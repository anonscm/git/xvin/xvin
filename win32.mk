########################################################
# makefile rules for Win32, using MinGW32 or Cygwin    #
########################################################


# below is the define to be passed to all XVin source files:
XV_TARGET      = XV_WIN32


# library type (static or dynamic):
ifeq ($(LINKING),DYNAMIC)
    ifeq ($(PLATFORM),MVSC)
	LINKING_OPTION = /DBUILDING_DLL #-mdll
	#PLUGIN_LINKING_OPTION = -mdll
        # extension for filenames of libraries:
	LIB_EXT    =dll
	LIB_PREFIX =bin/
    else ifeq ($(PLATFORM),CLANG)	
	LINKING_OPTION = -DBUILDING_DLL -I/c/msys64/mingw32/include
	PLUGIN_LINKING_OPTION = 
        # extension for filenames of libraries:
	LIB_EXT    =dll
	LIB_PREFIX =bin/
    else	
	LINKING_OPTION = -DBUILDING_DLL -mdll
	PLUGIN_LINKING_OPTION = -mdll
        # extension for filenames of libraries:
	LIB_EXT    =dll
	LIB_PREFIX =bin/
    endif
else
ifeq ($(LINKING),STATIC)
	LINKING_OPTION = -DXVIN_STATICLINK
        # extension for filenames of libraries:
	LIB_EXT    =a
	LIB_PREFIX =lib/lib
else 
	@echo "Library type (static or dynamic) not defined... aborting".
	return 1
endif
endif


DLL_FLAGS       = -mwindows -shared



####################
# archive creation #
####################
ifeq ($(PLATFORM),CYGWIN)
ZIPEXE = zip 
ZIP_EXT = .zip
endif

ifeq ($(PLATFORM),MINGW)
# define your disk drive letter : c: etc 
DRIVE=c:
# command line zip archiver
ZIPEXE = $(DRIVE)'/Program Files'/7-zip/7z a -tzip 
ZIP_EXT = .zip
endif




ifeq ($(PLATFORM),MINGW)
PLATFORM_FLAGS = -DGSL_DLL
endif

ifeq ($(PLATFORM),CYGWIN)
PLATFORM_FLAGS = -mno-cygwin 
endif




