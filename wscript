#! /usr/bin/env python
# encoding: utf-8

from waflib import Task, Utils, Context, Logs, Configure, Build, Scripting
from waflib.TaskGen import extension
from pathlib import Path
import os
import glob
import sys
import subprocess
import locale
import script.GitInfo as GitInfo
from script.BuildUtils import *
import time
import fileinput


VERSION='0.0.1'
APPNAME='xvin'

top = '.'
out = 'build'

##################Windows option############################
# USE ONLY ABSOLUTE and SHORT PATH NAME
msyspath="C:/msys64/"

if not os.path.exists(msyspath):
    msyspath="D:/msys64/"

if not os.path.exists(msyspath):
    msyspath="C:/msys32/"
if not os.path.exists(msyspath):
    msyspath="D:/msys32/"
mingwpath= msyspath + "mingw32/"
local_path = mingwpath




#TODO find ids path automatically using register
ueye_include="C:/Progra~1/IDS/uEye/Develop/include/"
ueye_bin="C:/Progra~1/IDS/uEye/Develop/"
ueye_lib="C:/Progra~1/IDS/uEye/Develop/Lib/"

#TODO find cuda path automatically using register
opencl_include= "C:/Progra~1/NVIDIA~2/CUDA/v6.5/include/"
opencl_bin= "C:/Progra~1/NVIDIA~1/OpenCL/"
opencl_lib="C:/Progra~1/NVIDIA~2/CUDA/v6.5/lib/Win32/"

boost_lib = mingwpath + "lib/"
boost_include = mingwpath + "include/"


local_lib = local_path + "lib/"
local_include = local_path + "include/"
local_bin = local_path + "bin/"

pi_gcs_bin = "C:ProgramData/PI/GCSTranslator"

PY_MODULES = dict( xlsxwriter = "ver >= num(0,8,0)"
                 , numpy      = "ver >= num(1,11,0)"
                 , pandas     = "ver >= num(0,19,0)"
                 , dateutil   = "ver >= num(2,5,3)" # pandas dependency
                 , pytz       = "ver >= num(2016,7)"# pandas dependency
                 , six        = "ver >= num(1,10,0)"# pandas dependency
                 , openpyxl   = "ver >= num(2,3,2)"
                 , jdcal      = "ver >= num(1,2)"   # openpyxl dependency
                 , et_xmlfile = "ver >= num(1,0,1)" # openpyxl dependency
                 , json       = "" # openpyxl dependency
                 , xml        = "" # openpyxl dependency
                 , urllib     = "" # openpyxl dependency
                 , distutils  = "" # pandas dependency
                 , http       = "" # pandas dependency
                 , email      = "" # pandas dependency
                 , skimage    = "ver >= num(0,12,3)" #  this is scikit-image
                 , scipy      = "ver >= num(0,18,1)" # scikit-image dependency
                 , matplotlib = "" # scikit-image dependency
                 , pyparsing  = "" # scikit-image dependency
                 , cycler     = "" # scikit-image dependency
                 )

################## FLAGS ############################
cflags = ['-std=gnu11', '-Werror=implicit-function-declaration']
cxxflags = ['-std=c++14', '-Werror=implicit-function-declaration']
generalflags = ['-fstack-protector-all', '-ftrapv'] # -fsanitize=memory, -lefence
cwarningflags = [
'-W', '-Wall', '-Wextra','-Wno-write-strings', '-Wunused',
'-Wmaybe-uninitialized', '-fno-common', '-Winit-self',  '-Wpacked',
'-Wpointer-arith', '-Wmissing-format-attribute',
'-Wmissing-noreturn',
'-Wundef',
'-Wunreachable-code',
'-Wmissing-include-dirs',
'-Wparentheses',
'-Wsequence-point',
'-Wshadow',
'-Wnested-externs', '-Wold-style-definition',
'-Wmissing-declarations', '-Wmissing-prototypes',
#'-Wswitch-enum',
#'-Wformat-security', '-Wformat=2',
#'-Wcast-qual',
#'-Wfloat-equal',
#'-Wredundant-decls',
#'-Wno-mismatched-tags',
#'-Winline',
]

cxxwarningflags = [
'-W', '-Wall', '-Wextra','-Wno-write-strings', '-Wunused',
'-Wmaybe-uninitialized', '-fno-common', '-Winit-self', '-Wpacked',
'-Wpointer-arith', '-Wmissing-format-attribute',
'-Wmissing-noreturn',
'-Wundef',
'-Wunreachable-code',
'-Wmissing-include-dirs',
#'-Wcast-qual',
'-Wparentheses',
'-Wsequence-point',
'-Wno-ignored-attributes', # EIGEN
'-Wno-deprecated-declarations', # FOR EIGEN WARNING TRY TO REMOVE IT REGULARY TO CHECK
'-Wno-misleading-indentation',
#'-Wswitch-enum',
#,'-Winline'
]

linkflags =  ['-W']#,'-z','relro','-z','now'] #il ya vait un l avant

winflags = ['-msse2', '-mwindows', '-mthreads', '-Wl,--export-all-symbols'] #'', ]
unixflags = ['-fPIC']
macflags_c = ['-arch','i386','-pthread','-fPIC']



modeflags = {
    'release': ['-O3'],
    'debug-opti': ['-O3', '-g', '-ggdb'],
    'debug': ['-g', '-ggdb'],
    'sanitize': ['-fsanitize=leak',
         '-fsanitize=address',
         '-fno-omit-frame-pointer',
         '-g',
         '-ggdb',
        #'-fsanitize=undefined',
        #'-fsanitize=thread'
        ],
    'profile': ['-g', '-ggdb', '-pg'],
    }
copy_rule = ''

def options(opt):
    loads = 'compiler_c compiler_cxx doxygen boost python'

    if "linux" in get_platform(opt):
        loads += ' color_gcc'

    opt.load(loads)

    opt.add_option('-m', '--mode', dest = 'mode', default = 'debug', action = 'store',
            help = 'build mode: *debug*, debug-opti, release, sanitize')

    opt.add_option('-p', '--platform', dest = 'platform', action = 'store',
            help = 'platform: *linux*, macos, win')

    opt.add_option('-c', '--camera', dest = 'camera', default = 'sim', action = 'store',
            help = 'camera: *sim*, movie, ueye, jai, cvb')

    opt.add_option('-s', '--serial',dest = 'serial', default = True, action = 'store',
            help = 'Compile with serial support, if false, pico while be with Dummy interface')

    opt.add_option('-z', '--focus', dest = 'focus', default = 'pifoc', action = 'store',
            help = 'Compile with special piezo objective stepper lib (*pifoc* or madcity, pigcs)')

    opt.add_option('--sdi',dest = 'sdi', default = False, action = 'store_true',
            help = 'Compile with SDI support')

    opt.add_option('--sdi2',dest = 'sdi2', default = False, action = 'store_true',
                    help = 'Compile with SDI support')

    opt.add_option('--sdipico',dest = 'sdipico', default = False, action = 'store_true',
                    help = 'Compile with SDI support pico mode')

    opt.add_option('--picotwist',dest = 'picotwist', default = False, action = 'store_false',
            help = 'Special flag for PicoTwist(TM) features')

    opt.add_option('--no-openmp',dest = 'noopenmp', default = False, action = 'store_true',
            help = 'disable OpenMP')

    opt.add_option('--no-opencl',dest = 'noopencl', default = True, action = 'store_true',
            help = 'disable OpenCL')

    opt.add_option('--no-python',dest = 'nopython', default = False, action = 'store_true',
            help = 'disable Python')

    opt.add_option('--no-warning', dest = 'nowarning', default = False, action = 'store_true',
            help = 'disable Warning')

    opt.add_option('--no-gtkdialog', dest = 'nogtkdialog', default = False, action = 'store_true',
            help = 'disable all gtk dialogs')

    opt.add_option('--no-gtkprintf', dest = 'nogtkprintf', default = False, action = 'store_true',
            help = 'disable GTK for win_printf and win_scanf')

    opt.add_option(
            '--cxx-only', dest = 'cxxonly',default = False, action = 'store_true',
            help = 'C++ compilation only')

    opt.add_option('--use-msvc', dest = 'msvc', default = False, action = 'store_true',
            help = 'On windows : use msvc compiler')

def configure(conf):
    #conf.setenv('x86')
    #conf.env['MSVC_VERSIONS'] = ['msvc 12.0']
    #conf.env['MSVC_TARGETS'] = ['x86']

    global cwarningflags
    global cxxwarningflags
    global generalflags
    global modeflags
    global linkflags

    conf.setenv('xvin', conf.get_env())

    # LOADING OPTIONS
    conf.env.OPENMP = not conf.options.noopenmp
    conf.env.OPENCL = not conf.options.noopencl
    conf.env.MODE = conf.options.mode
    conf.env.CAMERA = conf.options.camera
    conf.env.SERIAL = conf.options.serial
    conf.env.WARNING = not conf.options.nowarning
    conf.env.CXXONLY = conf.options.cxxonly
    conf.env.MSVC = conf.options.msvc
    conf.env.FOCUS = conf.options.focus
    conf.env.NOGTKDIALOG = conf.options.nogtkdialog
    conf.env.NOGTKPRINTF = conf.options.nogtkprintf
    conf.env.PYTHON = not conf.options.nopython
    conf.env.SDI = conf.options.sdi
    conf.env.SDI2 = conf.options.sdi2
    conf.env.SDIPICO = conf.options.sdipico
    conf.env.PICOTWIST = conf.options.picotwist

    # CONFIG PLATFORM
    conf.env.PLATFORM = get_platform(conf)
    print("Platform detected : "+conf.env.PLATFORM)

    if "win" in conf.env.PLATFORM:
        if not conf.options.boost_includes:
            conf.options.boost_includes = boost_include
        if not conf.options.boost_libs:
            conf.options.boost_libs = boost_lib

    loads = 'boost'
    if conf.env.PYTHON:
        loads = ' python '

    if "win" in conf.env.PLATFORM:
        loads += ' winres '
        if conf.env.MSVC:
            loads += ' msvc '
        else:
            loads += ' gxx '
            if not conf.env.CXXONLY:
                loads +=' gcc '
    else:
        loads += ' compiler_cxx doxygen color_gcc '
        if not conf.env.CXXONLY:
            loads +=' compiler_c '

    conf.load(loads)

    # ADDING FLAGS
    if not conf.env.WARNING:
        cwarningflags = '-w'
        cxxwarningflags = '-w'

    conf.env.append_unique('CFLAGS', generalflags)
    conf.env.append_unique('CFLAGS', modeflags[conf.env.MODE])
    conf.env.append_unique('CFLAGS', cwarningflags)
    conf.env.append_unique('CFLAGS', cflags)

    conf.env.append_unique('CXXFLAGS', generalflags)
    conf.env.append_unique('CXXFLAGS', modeflags[conf.env.MODE])
    conf.env.append_unique('CXXFLAGS', cxxwarningflags)
    conf.env.append_unique('CXXFLAGS', cxxflags)

    conf.env.append_unique('LINKFLAGS', linkflags)
    conf.env.append_unique('LINKFLAGS', modeflags[conf.env.MODE])

    #PLATFORM SPECIFIC FLAGS
    if "macos" in conf.env.PLATFORM:

        conf.env.append_unique('CFLAGS', macflags_c)
        conf.env.append_unique('CXXFLAGS', macflags_c)
        conf.env.append_unique('LINKFLAGS', macflags_c)

    elif "win" in conf.env.PLATFORM:
        conf.env.append_unique('CFLAGS', winflags)
        conf.env.append_unique('CXXFLAGS', winflags)
        conf.env.append_unique('LINKFLAGS', winflags)

    else:
        conf.env.append_unique('CFLAGS', unixflags)
        conf.env.append_unique('CXXFLAGS', unixflags)
        conf.env.append_unique('LINKFLAGS', unixflags)


    flgs = conf.cmd_and_log('h5cc -show')
    flgs = ' '.join('-l'+Path(i).with_suffix('').stem.replace('lib', '')
                    if i.endswith('.a') else i
                    for i in flgs.split())
    conf.parse_flags(flgs, uselib_store='HDF5')

    if "macos" in conf.env.PLATFORM:

        conf.env.append_unique('INCLUDES', ['/usr/local/include/','/opt/local/include/','/opt/local/include/boost/','/opt/local/include/gtk-3.0/gtk/',
        '/opt/local/include/gtk-3.0/','/opt/local/lib/glib-2.0/include/',
        '/opt/local/include/pango-1.0/','/opt/local/include/cairo/','/opt/local/include/gdk-pixbuf-2.0/','/opt/local/include/atk-1.0/'])
        conf.env.append_unique('LIBPATH', ['/usr/local/lib/','/opt/local/lib/','/opt/local/include/','/opt/local/include/boost/','/opt/local/include/gtk-3.0/gtk/',
        '/opt/local/include/gtk-3.0/','/opt/local/include/glnib-2.0/','/opt/local/lib/glib-2.0/include/',
        '/opt/local/include/pango-1.0/','/opt/local/include/cairo/','/opt/local/include/gdk-pixbuf-2.0/','/opt/local/include/atk-1.0/'])


    #conf.check_doxygen(mandatory=False)
    if conf.env.PYTHON:
        conf.check_python_version((3,5))
        conf.check_python_headers()

        for name, ver in PY_MODULES.items():
            conf.check_python_module(name, mandatory = False, condition = ver)

        cmd = conf.env.PYTHON[0]                                    \
            + ' -c "from numpy.distutils import misc_util as n;'    \
            + ' print(\'-I\'.join([\'\']+n.get_numpy_include_dirs()))"'
        flg = subprocess.check_output(cmd, shell=True).decode("utf-8")
        conf.parse_flags(flg, uselib_store='python')

    #OTHER PLATFORM SPECIFIC CONFIGURATION
    if "win" in conf.env.PLATFORM:
        #conf.env.append_unique('PREFIX',[local_path])
        conf.env.append_unique('LIBPATH', [local_lib, ueye_lib, opencl_lib])
        conf.env.append_unique('INCLUDES', [local_include, ueye_include, opencl_include])
        conf.env.append_unique('BINPATH', [local_bin, ueye_bin, opencl_bin, pi_gcs_bin])

    ######## PACKAGE CONFIGURATION
    conf.check_boost(lib        = 'serialization system filesystem', mandatory  = True)

    if conf.env.PYTHON:
        conf.check_boost(uselib_store = 'boost_python', lib = 'python-py35', mandatory = False)
        conf.check_boost(uselib_store = 'boost_python', lib = 'python3', mandatory = False)

    if conf.env.SDI or conf.env.SDI2 or conf.env.SDIPICO:
        conf.check_cfg(package='libusb',uselib_store='libusb',args='--cflags --libs')
        conf.check_cfg(package='libftdi1', uselib_store='ftdi', args='--cflags --libs')

    conf.check_cfg(package='hdf5', uselib_store='hdf5', args='--cflags --libs')
    conf.check_cfg(package='nlopt', uselib_store='nlopt', args='--cflags --libs')
    conf.check_cfg(package='glib-2.0', uselib_store='glib', args='--cflags --libs')
    conf.check_cfg(package='gtk+-3.0', uselib_store='gtk', args='--cflags --libs')
    conf.check_cfg(package='freetype2', args='--cflags --libs', uselib_store='freetype')
    conf.check_cfg(package='gsl', args='--cflags --libs', uselib_store='gsl')
    conf.check_cfg(package='eigen3', args='--cflags --libs', uselib_store='eigen')
    conf.check_cfg(package='libtiff-4',uselib_store='libtiff', args='--cflags --libs',mandatory=True)

    conf.check_cfg(package='fftw3', uselib_store='fftw3', args='--cflags --libs')

    #not using --libs of pkg-config because opencv_highgui depends on gtk+2...
    #conf.check(package='opencv', args= '--cflags', lib = ['opencv_core', 'opencv_imgproc'],
    #    uselib_store='opencv')

#conf.find_program('ldd')
    conf.find_program('cut')

    conf.check(uselib_store='dl', lib='dl',header_name='dlfcn.h')

    if "darwin" in conf.env.PLATFORM or "macos" in conf.env.PLATFORM:

        conf.env.CP_RULE="cp ${SRC} ${TGT}"

        conf.check_cfg( path='allegro-config', uselib_store='alleg', args=['--cflags', '--libs'],package='')
        conf.check(uselib_store = 'cl', includes = '/usr/include/CL', lib=['OpenCL' ],
                        header_name = 'CL/cl.h', mandatory = conf.env.OPENCL)


    elif "win" in conf.env.PLATFORM:
        conf.env.CP_RULE="cp ${SRC} ${TGT} -v"

        conf.check(uselib_store = 'cl', lib=['OpenCL'], header_name = 'CL/cl.h', static=True,
                mandatory = conf.env.OPENCL,)
        try:
            conf.check_cxx(uselib_store='comdlg32', lib=['comdlg32'],)
            conf.check_cxx(uselib_store='kernel32',lib=['kernel32'],)
            conf.check_cxx(
                    uselib_store='alleg',
                   fragment='#include <allegro.h> \n\
                           int main(int argc, char **argv) { \
                           (void)argc; (void)argv; return 0; } END_OF_MAIN();',
                    lib=['alleg44'],
                    )
        except:
            Logs.error('\n\nConfiguration process fail\nTry to disable "Real-time" anti-virus '
                'scan.\nSome of them are stupid...\n\n')
            return 1



    else:
        conf.env.CP_RULE="cp ${SRC} ${TGT}"

        conf.check_cfg( package='allegro', uselib_store='alleg',
                args=['allegro < 5', 'allegro >= 4.4', '--cflags', '--libs'],)
        conf.check(uselib_store = 'cl', includes = '/usr/include/CL', lib=['OpenCL' ],
                header_name = 'CL/cl.h', mandatory = conf.env.OPENCL)



    if conf.env.OPENMP:
        conf.check_openmp_cflags()#(uselib_store='omp', lib='gomp', cflags='-fopenmp', cxxflags='-fopenmp')

    if "madcity" in conf.env.FOCUS:
        conf.check(uselib_store='madlib', header_name='Madlib.h', lib='madlib')

    if "pie509" in conf.env.FOCUS:
        conf.check(uselib_store='pigcs', lib='PI_GCS2_DLL')

    if "ueye" in conf.env.CAMERA:
        if "win" in conf.env.PLATFORM:
            elib = ['ueye_api', 'ueye_tools']
        else:
            elib = ['ueye_api']
        conf.check(uselib_store='ueye', header_name='ueye.h', lib=elib)
    if "pleora" in conf.env.CAMERA:
        conf.check(uselib_store='pleora', header_name='PvDevice.h',
                lib=['PvBase', 'PvDevice','PvBuffer','PvGenICam','PvStream'])
                #includes='/opt/pleora/ebus_sdk/RHEL-6-x86_64/include',
                #libpath='/opt/pleora/ebus_sdk/RHEL-6-x86_64/lib',

    if "jai" in conf.env.CAMERA:
        conf.check(uselib_store='jai', header_name='JAI/Jai_Factory.h', lib=['JAIFactory'],
                includes=['/usr/local/include/JAI'],
                libpath=['/usr/local/lib'])

    # ADDING CONSTANTS
    conf.define('INSTRUMENT', 1)
    conf.define('BUILDING_DLL', 1)
    conf.define('DPIC', 1)
    conf.define('XV_NO_SSE', 1)
    conf.define('NO_WARNING_WINDOWS', 1)
    conf.define('UNIQUE_CONFIG_DIR', 1)
    conf.define('_FILE_OFFSET_BITS', 64)
    conf.define("_LARGEFILE_SOURCE", 1);
    conf.define("_LARGEFILE64_SOURCE", 1);
    conf.define("_GLIBCXX_USE_CXX11_ABI", 0) # Compilation issue on arch 2017-07-05 -- to be removed

    if conf.env.PICOTWIST:
        conf.define('PICOTWIST', 1)
    if not conf.env.NOGTKDIALOG:
        conf.define('XV_GTK_DIALOG', 1);

    if conf.env.NOGTKPRINTF:
        conf.define('XV_NO_GTK_WIN_PRINTF', 1);

    if "release" in conf.env.MODE:
        conf.define('NDEBUG', 1)

    if conf.env.LIB_opencv:
        conf.define('XV_OPENCV', 1)
    if conf.env.SDI:
            conf.define('SDI_VERSION',1)
            if "movie" not in conf.env.CAMERA:
                conf.define('ZMAG_APT',1)
                conf.define('ZMAG_FACTOR',1)
                conf.define('RAWHID_V1',1)
                conf.define('APT_MOT_TRANSLATION',1)
                conf.define('ZMAG_THREAD',1)

    if conf.env.SDI2:
            conf.define('SDI_VERSION_2',1)
            if "movie" not in conf.env.CAMERA:
                conf.define('ZMAG_APT',1)
                conf.define('ZMAG_FACTOR',1)
                conf.define('RAWHID_V1',1)
                conf.define('APT_MOT_TRANSLATION',1)
                conf.define('ZMAG_THREAD',1)


    if conf.env.SDIPICO:
            conf.define('SDIPICO',1)
            conf.define('ZMAG_APT',1)
            conf.define('ZMAG_FACTOR',1)
            conf.define('RAWHID_V1',1)
            conf.define('APT_MOT_TRANSLATION',1)
            conf.define('ZMAG_THREAD',1)


    if "darwin" in conf.env.PLATFORM or "mac" in conf.env.PLATFORM:
        conf.define("XV_MACOS", 1)
        conf.define('NO_STD_HELP', 1)
        conf.define("XV_MAC", 1)

    elif "win" in conf.env.PLATFORM:
        conf.define('XV_WIN32', 1)
        conf.define('ALFONT_DLL', 1)
        conf.define('ALFONT_DLL_EXPORTS', 1)
    else:

        conf.define('NO_STD_HELP', 1)
        conf.define("XV_UNIX", 1)
        conf.define("_XOPEN_SOURCE", 700);

    if conf.env.OPENCL:
        conf.define('OPENCL', 1)

    conf.write_config_header('xvin/include/config.h', remove = False);

    xvin_env = conf.env;

    # Configuring pico
    conf.setenv('pico', xvin_env)


    conf.define('TRACKING_BUFFER_SIZE', 1024);
    conf.define('XVIN_STATICLINK', 1)
    if "movie" in conf.env.CAMERA:
            conf.define('MOVIE', 1)
            conf.env.SERIAL = False
    if not conf.env.SERIAL:
        conf.define('DUMMY', 1)
    else:
        if conf.env.SDI==0 and conf.env.SDI2==0 and conf.env.SDIPICO==0:
            conf.define('SERIAL_PICO', 1)
            conf.define('RT_DEBUG', 1)
    if "madcity" in conf.env.FOCUS:
        conf.define('MAD_CITY_LAB', 1)
        conf.define('PI_C843', 1)
    if "pigcs" in conf.env.FOCUS:
        conf.define('PI_E709', 1)
        conf.check(uselib_store='pigcs', header_name='PI/PI_GCS2_DLL.h', lib=['pi_pi_gcs2'],
                includes=['/usr/local/include/'],
                libpath=['/usr/local/lib'])

    if "sim" in conf.env.CAMERA:
        conf.define('GAME', 1)
        #conf.define('FLOW_CONTROL_DUMMY', 1)


    if "ueye" in conf.env.CAMERA:
        conf.define('SDK_4_6_1', 1)
        conf.define('GIGE', 1)
        conf.define('UEYE', 1)
    #if "pleora" in conf.env.CAMERA:

    conf.write_config_header('pico/include/config.h')

    # Configuring PlayItAgainSam
    conf.setenv('pias', xvin_env)

    conf.define('PIAS', 1)


    conf.write_config_header('pias/include/config.h')

    status(conf)

def _build_xvin(bld):
    status(bld);
    bld.post_mode = Build.POST_LAZY
    bld.define('GIT_COMMIT_HASH', GitInfo.lastCommitHash())
    bld.define('GIT_COMMIT_DATE', GitInfo.lastCommitDate())
    bld.define('GIT_COMMIT_AUTHOR', GitInfo.lastCommitAuthor())
    bld.define('GIT_COMMIT_VERSION', GitInfo.version())
    bld.define('GIT_COMMIT_CLEAN',  "no" if GitInfo.isDirty() else "yes")
    bld.define('GIT_COMMIT_BRANCH', GitInfo.currentBranch())
    bld.write_config_header('./include/gitinfo.h')
    bld(features = 'subst',
         source   = 'script/gitinfo.template',
         target   = './include/gitinfo.hh',
         BRANCH   = GitInfo.currentBranch(),
         VERSION  = GitInfo.version(),
         HASHTAG  = GitInfo.lastCommitHash(),
         DATE     = GitInfo.lastCommitDate(),
         AUTHOR   = GitInfo.lastCommitAuthor(),
         CLEAN    = str(not GitInfo.isDirty()).lower())


    if (bld.options.nowarning):
        bld.env.append_unique('CFLAGS', ['-w']);
        bld.env.append_unique('CXXFLAGS', ['-w']);

    # Building alfont
    bld.shlib(
            target='allfont',
            source=bld.path.ant_glob(['plug-ins-src/allfont/alfont.c', ]),
            use=['freetype', 'alleg'])

    #Building icon
    if "win" in bld.env.PLATFORM:
        icon = "icons/xvin.rc"
    else:
        icon = "icons/X11_icon.c"

    #Adding gui sources,
    xvingui_source = bld.path.ant_glob(
            ['src/*.c', 'src/**/*.c'],
            excl=['src/*main*','src/*test*','src/menus/plot/p_treat_math.c','src/xv_pico.c',
                'src/xv_main.c', 'src/xv_scope.c'])

    #Adding gui dependency
    xvingui_use = ['allfont', 'alleg', 'freetype', 'gsl', 'fftw3', 'dl','gtk', 'comdlg32',
            'opencv','kernel32']

    # Building xvin library
    bld.shlib(
            #features="cxxshlib",
            target='xvingui',
            source=xvingui_source,
            includes=['include', 'include/xvin', 'src/menus/plot/treat', 'plug-ins-src/allfont'],
            use = xvingui_use)

    bld.shlib(
            #features="cxxshlib",
            target='xvincore',
            source=['src/xv_main.c'],
            includes=['include', 'include/xvin', 'src/menus/plot/treat', 'plug-ins-src/allfont'],
            use = ['xvingui'])

    # XVin binary
    if "xvin" in bld.variant:
        bld.program(
            source= ['src/main.c', icon],
            includes=['include', 'include/xvin'],
            target='xvin', use=['xvincore'], rpath=".")

    misc_copy = ['arial.ttf', 'fonts.dat', 'bead000quater.gr', 'MyOne.gr' ]

    for cfile in misc_copy:
        bld(rule=bld.env.CP_RULE, source='misc/' + cfile, target= cfile)
    bld(rule=bld.env.CP_RULE, source='res/gtk/ds_viewer.glade', target= 'ds_viewer.glade')
    #_build_module(bld, name='xvuEye',use=['ueye'])
    _build_modules(bld)

    bld.add_group()

    if "win" in bld.env.PLATFORM:
        windeploy(bld)

def build_xvin(bld):
    _build_xvin(bld)

def build_pico(bld):
    _build_xvin(bld)

    #Building icon
    if "win" in bld.env.PLATFORM:
        icon = "icons/xvin.rc"
    else:
        icon = "icons/X11_icon.c"

    trackbead_source = []
    trackbead_copy = []

    pico_source = ['src/main_pico.c', icon];
    pico_use = ['fft2d','cardinal','wlc','stat', 'opencl_utils', 'opencv', 'xvingui','cordrift',
        'FitInterference', 'BOOST', 'HDF5','ftdi','libusb','polymerWLC']
    pico_target = "pico"

    if bld.env.OPENCL:
        trackbead_source.extend(['gpu/cpu_track_utils.c', 'gpu/gpu_track_utils.c', 'gpu/track_beads.c',
            'gpu/struct_convert.c', 'gpu/debug_tools.c', 'gpu/track_bead_movie.c'])
        trackbead_copy.extend(["gpu/kernels/z_tracking.cl", "gpu/kernels/xy_tracking.cl",
                        "gpu/kernels/profile_utils.cl"]);
    if bld.env.SDI2:
        trackbead_source.extend(["SDI_2_functions.c","SDI_2_utils.c","SDI_2_bead_menu.c"])

    if (bld.env.SDI or bld.env.SDI2 or bld.env.SDIPICO) :
        if "movie" not in bld.env.CAMERA:

            if "darwin" in bld.env.PLATFORM or "macos" in bld.env.PLATFORM or "linux" in bld.env.PLATFORM:

                trackbead_source.extend(["thorlabs/apt_magnets_unix.c"])
                trackbead_source.extend(["thorlabs/apt_translation_unix.c","translation_control.c"])
                trackbead_source.extend(["rawhid_general/hid_LINUX.c","rawhid_general/rawhid_general.c"])
                trackbead_source.extend(["../thorlabs_apt_api/thorlabs_apt_api.c"])

            else:
                trackbead_source.extend(["thorlabs/apt_magnets.c"])
                trackbead_source.extend(["rawhid_general/rawhid-o.c","rawhid_general/rawhid_general.c"])


    if bld.env.SDI:
        filestoadd=[]
        folder_to_add="plug-ins-src/trackBead/SDITracking_v2/"
        flist=os.listdir(folder_to_add)
        for f in flist:
            if f.endswith(".c") and f not in ["sdi_v2_proc.c","sdi_v2_hdf5_record.c"]:
                filestoadd.append("SDITracking_v2/"+f)
        print(filestoadd)
        trackbead_source.extend(filestoadd)


    if "win" in bld.env.PLATFORM:
        trackbead_source.extend(['pico_serial/pico_serial.c'])
    else:
        trackbead_source.extend(['pico_serial/pico_serial_linux.c'])

    if "madcity" in bld.env.FOCUS:
        pico_use.extend(['madlib'])
        trackbead_source.extend(['mad_city_focus/MCL_focus.c'])
        #TODO separate focus from magnet
        #trackbead_source.extend(['C843_magnets/C843_magnets.c'])
    elif "pigcs" in bld.env.FOCUS:
        pico_use.extend(['pigcs'])
        trackbead_source.extend(['PI_E709/PI_E709_focus.c'])

    elif (bld.env.SDI2==0 and bld.env.SDI==0 and bld.env.SDIPICO==0):
        trackbead_source.extend(['pico_focus/pico_focus.c'])





    if ("movie" in bld.env.CAMERA) or (bld.env.SDI2==0 and bld.env.SDI==0 and bld.env.SDIPICO==0):
        trackbead_source.extend(['pico_magnets/pico_magnets.c'])
        trackbead_source.extend(['pico_temperature/pico_temperature.c'])
        if bld.env.PYTHON :
            trackbead_source.extend(['indexing.cc'])


    if bld.env.LIB_opencv and bld.env.LIB_cl:
        trackbead_source.extend(['count_beads_cv.cc'])
    trackbead_source.extend(['trackBead.c', 'brown_util.c',
        'track_util.c', 'focus.c', 'magnetscontrol.c', 'scan_zmag.c', 'action.c', 'calibration.c',
        'hat.c', 'force.c', 'record.c', 'draw_track_event_phase_average.cc', 'zdet.c', 'save_movie_util.c',
        'zdet_topo.c', 'fillibbt.c', 'timer_thread.c', 'thermal_z/thermal_z.c', '../cfg_file/microscope.c',
        '../cfg_file/cfg_file.c', 'gui.c', ])


    if "sim" in bld.env.CAMERA:
        trackbead_source.append('game_source.c')
    elif "ueye" in bld.env.CAMERA:
        pico_use.extend(['ueye'])
        trackbead_source.append('uEye_source.c')
        pico_target = "pico-ueye"
    elif "jai" in bld.env.CAMERA:
        pico_use.extend(['jai'])
        trackbead_source.append('JAI_source.cc')
        pico_target = "pico-jai"
    elif "pleora" in bld.env.CAMERA:
        pico_use.extend(['pleora'])
        trackbead_source.append('pleora_source.cc')
        pico_target = "pico-pleora"
    elif "movie" in bld.env.CAMERA:
        trackbead_source.extend(['movie_source.c'])
        pico_use.append('xvtiff')

    trackbead_source.extend(['../filters/filter_core.cc', '../filters/stattests.cc'])
    trackbead_source = ['plug-ins-src/trackBead/' + x for x in trackbead_source]
    trackbead_source.extend(['src/xv_pico.c'])
    if bld.env.SDI2 or bld.env.SDI or bld.env.SDIPICO:
        _build_module(bld, name='thorlabs_apt_api', use=['ftdi']);


    bld.shlib(
        source= trackbead_source,
        includes = ['include', 'include/xvin', 'src/'],
        target=pico_target + 'core',
        use=pico_use,
            )

    # Pico binary
    bld.program(
        source=pico_source,
        target=pico_target,
        includes = ['include', 'include/xvin', 'src/'],
        use=[pico_target + 'core'],
        rpath=".")

    if bld.env.PYTHON:
        _build_pywrapper( pico_target+'core', bld)

        #_build_module(bld, name='beaddetector',
        #          use=['BOOST', pico_target+'core', 'pywrapper'],
        #          features = ['pyembed'], pymodule = True,
        #          copy = {'bead': 'beaddetector/bead'})

    _build_module(bld, name='shortcuts', use=['cordrift',pico_target+'core']);
    _build_module(bld,name='polymerWLC',use=['nrutil'])
    _build_module(bld,name='NoiseInImage');

    #File copy
    for cfile in trackbead_copy:
        bld(rule=bld.env.CP_RULE, source='plug-ins-src/trackBead/'+ cfile, target= cfile)

    if "win" in bld.env.PLATFORM:
        windeploy(bld)


def build_pias(bld):
    print(bld)
    _build_xvin(bld)

    trackbead_source = []
    trackbead_copy = []

    #Building icon
    if "win" in bld.env.PLATFORM:
        icon = "icons/xvin.rc"
    else:
        icon = "icons/X11_icon.c"

    pias_source = ['src/main_pico.c', icon];
    pias_use = ['fft2d','cardinal','wlc','stat', 'opencl_utils', 'opencv', 'xvingui',
                'FitInterference', 'BOOST', 'cordrift','polymerWLC','FindSteps']

    pias_target = "pias"

    if bld.env.OPENCL:
        trackbead_source.extend(['gpu/cpu_track_utils.c', 'gpu/gpu_track_utils.c', 'gpu/track_beads.c',
            'gpu/struct_convert.c', 'gpu/debug_tools.c', 'gpu/track_bead_movie.c'])
        trackbead_copy.extend(["gpu/kernels/z_tracking.cl", "gpu/kernels/xy_tracking.cl",
                        "gpu/kernels/profile_utils.cl"]);

    trackbead_source.extend(['scan_zmag_rec.c', 'hat_rec.c', 'record.c',
         'draw_track_event_phase_average.cc', 'force_rec.c', 'select_rec.c',
         'fillibbt.c', 'brown_util.c', 'PlayItAgainSam.c', 'timer_thread.c',
         '../cfg_file/cfg_file.c', '../cfg_file/microscope.c', 'gui.c'    ]);
    if bld.env.PYTHON :
        trackbead_source.extend(['indexing.cc'])
    trackbead_source.extend(['../filters/filter_core.cc', '../filters/stattests.cc'])

    trackbead_source = ['plug-ins-src/trackBead/' + x for x in trackbead_source]
    trackbead_source.extend(['src/xv_pico.c'])


    bld.shlib(
        source= trackbead_source,
        includes = ['include', 'include/xvin', 'src/', ],
        target=pias_target + 'core',
        use=pias_use,
            )


    bld.program(
        source=pias_source,
        target=pias_target,
        includes = ['include', 'include/xvin', 'src/'],
        use=[pias_target + 'core'],
        rpath=".")

    #File copy
    for cfile in trackbead_copy:
        bld(rule=bld.env.CP_RULE, source='plug-ins-src/trackBead/'+ cfile, target= cfile)

    if bld.env.PYTHON:
        _build_pywrapper('piascore', bld)
        _build_module(bld, name='hybridstat',
                  use=['stat', 'BOOST', 'piascore', 'nlopt', 'pywrapper'],
                  features = ['pyembed'], pymodule = True)
        _build_module(bld, use=['piascore'], name='beadselection')

    _build_module(bld, name='shortcuts', use=['cordrift','piascore']);
    _build_module(bld,name='SeqHeli')
    _build_module(bld,name='derive_analysis')
    _build_module(bld,name='xvtiff',use=['libtiff'])
    _build_module(bld,name='polymerWLC',use=['nrutil'])

    #_build_module(bld, name='methylstat', use=['stat', 'piascore'])
    #_build_module(bld, name='oligoseq', use=['fingerprint', 'methylstat', 'eigen']);

    if "win" in bld.env.PLATFORM:
        windeploy(bld)

def _build_modules(bld):

    if bld.env.OPENCL:
        _build_module(bld, name='testOpenCl', use = ['cl'])
        _build_module(bld, name='opencl_utils', use = ['cl'])


    #_build_module(bld, name='profil_radial')
    _build_module(bld, name='xvplot')
    _build_module(bld, name='boxplot')
    _build_module(bld, name='fft2d')
    _build_module(bld, name='nrutil')
    #_build_module(bld, name='GifAvg')
    _build_module(bld, name='TrkMax')
    _build_module(bld, name='shift_data')
    _build_module(bld, name='FitInterference', source=["FitInterference.c",
    "fillibbt.c", "brown_util.c"], use = ['nrutil', 'fft2d', 'xvincore','gsl'])
    _build_module(bld, name='wlc', use = ['xvplot', 'nrutil'])
    _build_module(bld, name='cardinal', use = ['xvplot'])
    _build_module(bld, name='stat', use = ['nrutil'])
    _build_module(bld, name='oligo', use = ['nrutil'])
    _build_module(bld, name='talbot')
    _build_module(bld, name='dna_utils')
    _build_module(bld, name='Tm_DNA', source=['Tm_DNA.c'])
    _build_module(bld, name='findSeq', source=['findSeq.c'], use=['Tm_DNA', 'nrutil', 'dna_utils',
        'fasta_utils'])
    _build_module(bld, name='fasta_utils', use=['dna_utils', 'gtk'])
    _build_module(bld, name='histo_utils', use=['fft2d'])
    _build_module(bld, name='ImOliSeq')
    _build_module(bld, name='talbot')
    #if bld.env.BOOST_VERSION >= "1_60":
    #    _build_module(bld, name='fingerprint', use=['findSeq', 'fasta_utils', 'dna_utils',
    #        'histo_utils',
    #    'BOOST', 'gtk', 'gsl', 'eigen', 'ImOliSeq', 'boxplot', 'stat', 'cordrift'],
    ##    flags="-Werror")
    _build_module(bld, name='histo2fmr');
    _build_module(bld, name='FindRepeats');
    _build_module(bld, name='cordrift');
    _build_module(bld, name='dna_manipulation', use=['dna_utils', 'fasta_utils']);
    #_build_module(bld, name='thorlabs_apt_api', use=['ftdi']);
    _build_module(bld,name='NoiseInImage');
    _build_module(bld,name='FindSteps');
    #_build_module(bld,name='loadsnd');
    _build_module(bld, name='shortcuts', use=['cordrift']);
    _build_module(bld, name='xvtiff',use=['libtiff']);


    #_build_module(bld, name='SDITracking', use=['fft2d']);
    #if "win" in bld.env.PLATFORM:
    #    _build_module(bld, name='xvuEye', use=['ueye']);

def build(bld):
    print("You are build all programs. To build on only one, type ./waf build_xvin or ./waf build_pias"
          " or ./waf build_pico");

    Scripting.run_command('build_xvin');
    Scripting.run_command('build_pico');
    Scripting.run_command('build_pias');

def clean(ctx):

    Scripting.run_command('clean_xvin');
    Scripting.run_command('clean_pico');
    Scripting.run_command('clean_pias');



def windeploy(bld):
    bld.add_group()
    # FIXME : corrected dll deployment function

    # deploy dll declared in the program
    #bld(source = [ 'xvin.exe'], use = ['xvin', pico_target] )

    # deploy all dll from mingw 32 (brutforce technique...)
    dll = bld.root.ant_glob(local_bin + '/*.dll',
                            remove=False,
                            excl  = [local_bin+'/Qt5*.dll'])
    for x in dll:
        bld(rule=bld.env.CP_RULE, source=x , target=x.name)

    # deploy some gtk assets
    shareglibshema = bld.root.ant_glob(local_path + 'share/glib-2.0/schemas/*', remove=False)
    shareicons = bld.root.ant_glob(local_path + 'share/icons/**/*', remove=False)

    for x in shareglibshema:
        print(x.abspath().replace(local_path, '', 1))
        bld(rule=bld.env.CP_RULE, source=x , target=x.abspath().replace(local_path, '', 1))

    # Disable because very very long on windows
    #for x in shareicons:
    #    bld(rule=bld.env.CP_RULE, source=x , target=x.abspath().replace(local_path, '', 1))


@bld_command
def doc(bld):
    bld(
        features='doxygen',
        doxyfile='doxygen.conf',
        doxy_tar='docs.tar.bz2')

@bld_command
def status(conf):


    if hasattr(conf, "MODE"):
        curenv = conf.env
    else:
        curenv = conf.all_envs["xvin"]


    switch_flags = ""

    if (not curenv.WARNING):
        switch_flags += " --no-warning "
    if (not curenv.OPENMP):
        switch_flags += " --no-openmp "

    if (not curenv.OPENCL):
        switch_flags += " --no-opencl "

    if (not curenv.OPENCV):
        switch_flags += " --no-opencv "

    if (curenv.NOGTKPRINTF):
        switch_flags += " --nogtkprintf "

    if (curenv.NOGTKDIALOG):
        switch_flags += " --nogtkdialog "

    if (curenv.CXXONLY):
        switch_flags += " --cxx-only "

    if (curenv.MSVC):
        switch_flags += " --use-msvc "

    print ("---- Configuration command ----")
    print ("./waf configure --mode={mode} --platform={platform}  --camera={camera} --focus={focus}"
           " --serial={serial} {switch_flags}".format(mode=curenv.MODE,
        platform=curenv.PLATFORM, camera=curenv.CAMERA,
        focus=curenv.FOCUS, serial=curenv.SERIAL, switch_flags=switch_flags))
    print("\n")
    print ("---- Compilation Flags ----")
    print("\n")
    print("cflags: ", curenv.CFLAGS)
    print("\n")
    print("cxxflags: ", curenv.CXXFLAGS)
    print("\n")
    print("linkflags: ", curenv.LINKFLAGS)
    print("\n")



class PyCopy(Task.Task):
    def run(self):
        out  = str(self.bld.bldnode.abspath())+"/python"
        pdir = self.env['PYTHONDIR']
        cmd  = "cp -r "+pdir + "/{} " + out+"/."

        if not os.path.exists(out):
            subprocess.check_output("mkdir "+out)
            subprocess.check_output(cmd.format("../*.py"))
            ver = ".".join(self.env["PYTHON_VERSION"].split(".")[:2])
            subprocess.check_output(cmd.format("../../libpython"+ver+"*"))

        for sd in ('collections', 'ctypes', 'encodings', 'importlib',
                   'lib-dynload', 'logging', 'unittest', 'multiprocessing'
                  ):
            if not os.path.exists(out+"/"+sd):
                subprocess.check_output(cmd.format("../"+sd))

        for sd in PY_MODULES:
            print(out+"/"+sd)
            if os.path.exists(out+"/"+sd):
                continue
            if os.path.exists(out+"/"+sd+".py"):
                continue
            print("copying python module: "+sd)
            if os.path.exists(pdir+"/../"+sd):
                subprocess.check_output(cmd.format("../"+sd))
            elif os.path.exists(pdir+"/../"+sd+".py"):
                subprocess.check_output(cmd.format("../"+sd+".py"))
            elif os.path.exists(pdir+"/"+sd+".py"):
                subprocess.check_output(cmd.format(sd+".py"))
            else:
                subprocess.check_output(cmd.format(sd))

def _build_pywrapper(dep, bld):
    name   = "pywrapper"
    path   = 'plug-ins-src/' + name + '/'
    items  = dict(use       = [dep, 'BOOST', "python", 'boost_python'])
    source = bld.path.ant_glob([path + '/*.'+ext for ext in ('c', 'cc')],
                               excl = [path+'/module.cc'])
    _build_module(bld,
                  name      = name,
                  features  = ["pyembed"],
                  source    = [str(x) for x in source],
                  **items
                 )

    bld.shlib(
        source      = [path+"/module.cc"],
        includes    = ['include', 'include/xvin', 'src', 'src/menus/plot/treat'],
        target      = "pyxvin",
        lib         = [],
        features    = ["pyext"],
        cflags      = [],
        use         = items["use"]+['pywrapper']
        )

    if "win" in bld.env.PLATFORM:
        bld.add_to_group(PyCopy(env = bld.env))

def _build_module(bld, name, use = [], source=[], lib=[], copy=[], cflags=[], test=False,
                  features = None, copypy = True, pymodule = False):
    path = 'plug-ins-src/' + name + '/'
    includes = ['include', 'include/xvin' ]

    if (not os.path.exists(path)):
        Logs.error('Plugin ' + name + ' not found')
        return

    #library used
    sh_use = use[:]
    sh_use.append('xvingui')

    #output name
    sh_targetname = name

    if (source == '' or source == []):
        good   = [path + '/*' + x for x in ('.c', '*/*.c', '*/*.cc', '.cc')]
        if pymodule:
            pysource = bld.path.ant_glob([path+'/module_*.cc'])
            source   = bld.path.ant_glob(good, excl = [path+'/module_*.cc'])
        else:
            source = bld.path.ant_glob(good, excl = [path+'/module_*.cc'])
    else:
        source = [(path + '/' + x if not x.startswith(path) else x)  for x in source]
        if pymodule:
            pysource = [x for x in source if '/module_'     in x]
            source   = [x for x in source if '/module_' not in x]

    if isinstance(copy, dict):
        for x, y in copy.items():
            bld(rule=bld.env.CP_RULE, source=path + x, target=y)
    else:
        for x in copy:
            bld(rule=bld.env.CP_RULE, source=path + x, target=x)

    if copypy:
        for x in bld.path.ant_glob([path + '/*.py', path + '/**/*.py']):
            bld(rule   = bld.env.CP_RULE,
                source = x,
                target = x.get_bld().relpath().replace(path, name+"/"))
        for x in bld.path.ant_glob(['test/*{}*.py'.format(name)]):
            bld(rule=bld.env.CP_RULE, source=x, target=x.get_bld().relpath())

    bld.shlib(
        source=source,
        includes=['include', 'include/xvin', 'src', 'src/menus/plot/treat'],
        target = sh_targetname,
        use= sh_use,
        lib=lib,
        features = features if features is not None else [],
        cflags=cflags,)
        #cxxflags=cflags)

    if pymodule and len(pysource):
        bld.shlib(
            source=pysource,
            includes=['include', 'include/xvin', 'src', 'src/menus/plot/treat'],
            target = name+"/_core",
            use= sh_use+[sh_targetname, 'BOOST', "python", 'boost_python', 'pywrapper'],
            lib=lib,
            features = ["pyext"],
            cflags=cflags,)

        pysrc = bld.path.ant_glob([path + '/*.py'])
        if not any(str(x).endswith("/__init__.py") for x in pysrc):
            bld(rule   = bld.env.CP_RULE,
                source = "script/pyinit.template",
                target = name+"/__init__.py")

    if test:
        bld(
           features = "subst",
           source= "test/test.template",
           target= 'test/' + name + '.c' ,
           MODULE_NAME = name,
            )

        bld.program(
           source = 'test/' + name + '.c',
           target = 'test/' + name,
           includes=['include', 'include/xvin'],
           use= [sh_targetname, 'xvingui'])
        if os.path.exists("test/test_{mod}.cc".format(mod=name)):
            bld.program(
               source = 'test/test_' + name + '.cc',
               target = 'test/unittest_' + name,
               includes=['include', 'include/xvin', "plug-ins-src/"+name],
               use= [sh_targetname, 'xvingui']+use, rpath=".")

def init(ctx):
    from waflib.Build import BuildContext, CleanContext, \
            InstallContext, UninstallContext
    for x in 'pico pias xvin'.split():
        for y in (BuildContext, CleanContext, InstallContext, UninstallContext):
            name = y.__name__.replace('Context','').lower()
            class tmp(y):
                cmd = name + '_' + x
                variant = x
                fun = name + '_' + x if "build" in name else name

@extension('.exe', '.dll')
def process_dlldeploy(self, node):
    self.create_task('dlldeploy', node)

class dlldeploy(Task.Task):
    ext_out = ['.dll']

    def fatal(self, str):
        Logs.debug(str)

    def run(self):
        file = Configure.find_file(self, self.inputs[0].name)
        rootfile = Configure.find_file(self, self.inputs[0].name, self.env.BINPATH)
        if file == None and not rootfile == None:
            self.deploy(self.inputs[0].name)
        else:
            cmd = '%s %s | %s -f -1 -d \' \' | %s -f 2-' % (self.env.LDD[0], self.inputs[0].abspath(), self.env.CUT[0], self.env.CUT[0])
            cwd = self.inputs[0].parent.get_bld()
            out = self.generator.bld.cmd_and_log(cmd, cwd=cwd.name, quiet=Context.STDOUT)
            out = Utils.to_list(out)

            for x in out:
                self.deploy(x)

    def deploy(self, x):
        file = Configure.find_file(self, x, self.env.BINPATH)
        cwd = self.inputs[0].parent.get_bld()
        if not file == None:
            cmd = 'cp -n %s %s' % ( file, x);
            self.generator.bld.cmd_and_log(cmd, cwd=cwd.name, quiet=Context.STDOUT)
            print("deployed " + x)
