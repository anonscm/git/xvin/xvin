# below is the system used:
SYSTEM = WIN32
# SYSTEM = LINUX
# SYSTEM = MACOS

# below is the platform (used only on win32 machines):
# PLATFORM = MVSC
PLATFORM = MINGW
# PLATFORM = CYGWIN

# below is the linking type:
LINKING = STATIC
#LINKING = DYNAMIC

# below the linking type of allegro:
#ALLEGRO_LINKING = STATIC
ALLEGRO_LINKING = DYNAMIC

ifeq ($(ALLEGRO_LINKING),DYNAMIC)
ALLEGRO_LIB_NAME = alleg44.dll
else
ALLEGRO_LIB_NAME = alleg
endif

UEYE_SDK = SDK_4_9_0
JAI_SDK = SDK_1_2_5
#FLOW_CONTROL = THORLABS
#

# below we eventually define a memory checker (comment it if none is used)
# MEMCHECK = FORTIFY
CXX_FLAGS      = -std=c++11
WARNING_FLAGS  = -W -Wall -Wextra -Wunused -Wmaybe-uninitialized -fno-common -Wformat=2 -Winit-self -Winline -Wpacked -Wpointer-arith -Wmissing-format-attribute -Wmissing-noreturn -Wnested-externs -Wold-style-definition -Wswitch-enum -Wundef -Wunreachable-code -Wmissing-include-dirs -Wshadow -Wformat-security -Wcast-qual -Wparentheses -Wsequence-point
DEBUG_FLAGS = -g -ggdb
#-Wmissing-prototypes -Wmissing-declarations
#'-Wredundant-decls',
#'-Wfloat-equal',

# below are the CPU flags, and eventually optimization flags:
ifeq ($(PLATFORM),MVSC)
CPU_FLAGS      = /Yd  /MT  /I./allegro-4.4.2-msvc-10.0/include/ /DALLEGRO_STATICLINK /DMVSC /D_CRT_SECURE_NO_WARNINGS  
else
CPU_FLAGS      = -Werror=implicit-function-declaration -std=gnu11 -msse2 -mthreads   # -march=pentium4 -msse2 -mfpmath=sse -DALLEGRO_STATICLINK
endif

#CPU_FLAGS      = -O2 -march=nocona -msse2 -mfpmath=sse
#CPU_FLAGS     = -O2 -DMAC_POWERPC # for MACOS with PowerPC / G4 / G5 processors
# for PowerPC Macs, bytes from files have to be swept before any use with XVin
# but this is not the case with new Intel Macs.

# below is the C compiler name (e.g. : gcc-3.0 on debian/woody, gcc on most systems)
ifeq ($(PLATFORM),MVSC)
CC	=	CL
else
CC	=	gcc
endif
CXX          = g++

# following line indicates where are the libraries required to build XVin
# those libraries are : Allegro/Freetype/FFTW/GSL
# you can write several directories, separated with spaces, but it is
# recommended to use only one directory, if you can.
#GNU_LIBS_DIR = $(HOME)/local            # typical for linux when no root
#GNU_LIBS_DIR = /opt/local               # typical for macos X 
#GNU_LIBS_DIR = /usr/local               # most typical 
#GNU_LIBS_DIR = /c/PROGRA~1/GnuWin32      # most typical on Win32 Mingw systems
GNU_LIBS_DIR = /mingw32

# below are lines to tell where your libraries are, if they are in different 
# directories then the one specified above. If you don't know, or if libs are
# in the directory above, please comment those definitions.
#ALLEGRO_DIR=
#FREETYPE_DIR=
#ifeq ($(PLATFORM),MVSC)
#GSL_DIR=c:\PROGRA~1\GnuWin32
#else
#GSL_DIR=/c/PROGRA~1/GnuWin32
#endif

NSIS_BIN = C:/Program\ Files/NSIS/makensis.exe

# below is a computation of the libraries include path
MY_INC = $(addprefix -I,$(addsuffix /include,$(strip $(GNU_LIBS_DIR))))
# following line may be usefull on mingw only, but I recommend using
#    GNU_LIBS_DIR = $(XVIN_DIR) 
# instead.
# MY_INC = -I./include/allegro -I./include/allfont -I../../include/pthread



# We write the plug'in names in a compact way:
#include $(XVIN_DIR)plugin-list-ens-lyon.mk
include $(XVIN_DIR)plugin-list-ens-lps.mk
# another possible way of giving a plugins list:
#PLUGINS = inout random hist



#############################################################################
# do not edit below this line
#############################################################################
include $(XVIN_DIR)defines.mk
