#ifndef _PARALLEL_PORT_H_
#define _PARALLEL_PORT_H_


PXV_FUNC(MENU*, parallel_port_plot_menu, (void));
PXV_FUNC(int, parallel_port_main, (int argc, char **argv));
#endif

