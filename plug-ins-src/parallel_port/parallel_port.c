/*
*    Plug-in program for parallel port control in Xvin.
 *
 *    E.Cavatore & F. Mosconi
  */
#ifndef _PARALLEL_PORT_C_
#define _PARALLEL_PORT_C_

# include "allegro.h"
# include "xvin.h"

/* If you include other regular header do it here*/ 

/* But not below this define */
# define BUILDING_PLUGINS_DLL
#define DATA_OUTPUT 0x378
#define STATE_INPUT 0x379

//place here all headers from plugins already compiled

#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "parallel_port.h"
//place here other headers of this plugin 

/* If you include other regular header do it here*/ 
// to open the port
short _stdcall Inp32(short PortAddress);
void _stdcall Out32(short PortAddress, short data);

int do_write(void)
{
  register int i;
  static unsigned char bit_number = 0;
	
  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine writes a bitword to data the parallel port");
    }
  i = win_scanf("define the 8bit_number you want to write as OUTPUT (DATA 0x378) %d",&bit_number);
  if (i == CANCEL)	return D_O_K;
  if ( bit_number > 128) return win_printf_OK("cannot write over 1 byte (=8bits)!!");
    
  Out32(DATA_OUTPUT, bit_number);
  return D_O_K;
}
  

int do_read(void)
{  
  register int i;
  unsigned char bit_number;
	
  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */	if (key[KEY_LSHIFT])
	  
    if (key[KEY_LSHIFT])	return win_printf_OK("This routine reads a bitword as input(STATE 0x379) of the parallel port");
	  
  bit_number = Inp32(STATE_INPUT);
  if ( bit_number > 128 ) return win_printf_OK("cannot read over 4bits!");
   
  i = win_printf("reading input (STATE 0x379) %d\n",bit_number);
  if (i == CANCEL)	return D_O_K;
   
  return D_O_K;
}  

int do_toggle(void)
{
  static int first_bit_on = 0;
  if(updating_menu_state != 0)	return D_O_K;
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT]) return win_printf_OK("This routine toggles the value of the first bit (pin 2 on the port)"); 
  if(first_bit_on){
    Out32(DATA_OUTPUT, 0);
    first_bit_on = 0;
  }
  else{
    Out32(DATA_OUTPUT, 1);
    first_bit_on = 1;
  }
  return D_O_K;
}

MENU *parallel_port_plot_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"write on parallel port", do_write,NULL,0,NULL);
  add_item_to_menu(mn,"read on parallel port", do_read,NULL,0,NULL);
  add_item_to_menu(mn,"toggle first bit", do_toggle,NULL,0,NULL);
  return mn;
}

int	parallel_port_main(int argc, char **argv)
{
  add_plot_treat_menu_item ( "parallel_port", NULL, parallel_port_plot_menu(), 0, NULL);
  return D_O_K;
}

int	parallel_port_unload(int argc, char **argv)
{
  remove_item_to_menu(plot_treat_menu, "parallel_port", NULL, NULL);
  return D_O_K;
}
#endif

