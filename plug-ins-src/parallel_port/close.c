/*
*    Standalone parallel port control.
 *
 *    F. Mosconi
  */
#ifndef _PARALLEL_PORT_CLOSE_C_
#define _PARALLEL_PORT_CLOSE_C_

# define BUILDING_PLUGINS_DLL
#define DATA_OUTPUT 0x378
#define STATE_INPUT 0x379

short _stdcall Inp32(short PortAddress);
void _stdcall Out32(short PortAddress, short data);

int main(void)
{  
  register int i;
  unsigned char bit_number;

  Out32(DATA_OUTPUT, 0);
  return 0;
}

#endif

