/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _SSDNA_C_
#define _SSDNA_C_

# include "allegro.h"
# include "xvin.h"

/* If you include other plug-ins header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "ssdna.h"


int delay = 50; // ms

int	free_chain(chain *chn)
{
	if(chn == NULL)		return 1;
	if(chn->im != NULL)
	{
		if (chn->im[0] != NULL)		free(chn->im[0]);
		free(chn->im);
	}
	free(chn->file);
	free(chn->path);
	free(chn->n);
	free(chn->linksize);
	free(chn);
	return 0;
}
int	alloc_im_of_chain(chain *chn)
{
	register int i;
	
	if(chn == NULL || chn->ni <= 0)		return 1;
	if(chn->im != NULL)
	{
		if (chn->im[0] != NULL)		free(chn->im[0]);
		free(chn->im);
	}
/*	fprintf(stderr,"allocating %d by %d image ...",chn->ni,chn->ni);*/
	chn->im = (short int**)calloc(chn->ni,sizeof(short int*));
	if (chn->im == NULL)	return 2;
	chn->im[0] = (short int*)calloc(chn->ni*chn->ni,sizeof(short int));
	if (chn->im[0] == NULL)	return 3;
	for (i = 1; i < chn->ni; i++)
		chn->im[i] = chn->im[i-1] + chn->ni;
/*	fprintf(stderr,"done\n");*/
	return 0;
}
int	translate_chain_to_im(chain *chn)
{
	register int i;
	int x, y, nx; 
	short int *ch;
	
	if (chn == NULL)	return 1;
	nx = chn->ni * chn->ni;
	if (chn->im == NULL)	alloc_im_of_chain(chn);
	chn->isubstep = chn->istep = 0;	
	for (i = 0, ch = chn->im[0]; i < nx; i++) 	ch[i] = NOT_BOUND;
/*	fprintf(stderr,"setting  %d pixel to not bound\n",nx); */

	nx = chn->ni;
	for (i = 0; i < nx; i++)
	{
		x = chn->n[i].x;
		y = chn->n[i].y;
		x = (x+nx)%nx;
		y = (y+nx)%nx;
		chn->im[y][x] = (short int)i;	/*	chn->n[i].type; */
	}
/*	fprintf(stderr,"setting  %d chain site in image \n",nx);
	x = chn->n[which].x;
	y = chn->n[which].y;
	t = chn->n[which].type;
	win_printf("site %d base %d \n at x = %d y = %d \n im = %d\n ",which,t,x,y,chn->im[(y+nx)%nx][(x+nx)%nx]);
*/
	
	return 0;
}
chain *read_mole_file(char *file)
{
	register int i, j;
	nucleotide *n = NULL;
	chain *chn = NULL;
	int  x, y, t, *linksize = NULL; /*,  which = 5, nx; */
	FILE *fp;

	if (file == NULL)	return NULL;
	fp = fopen(file,"r");
	if (fp == NULL)
	{
		win_printf("cannot open file");
		return NULL;
	}
	chn = (chain*)calloc(1,sizeof(chain));
	if (chn == NULL)	return NULL;
	chn->mi = 0;
	for (i = 0, j = fscanf(fp,"%d%d%d",&x,&y,&t); j == 3; i++)
	{
		if (n == NULL || chn->mi <= i)
		{
			chn->mi += 64;
			n = (nucleotide*)realloc(n,chn->mi*sizeof(nucleotide));
			linksize = (int*)realloc(linksize,chn->mi*sizeof(int));	
			if (n == NULL || linksize == NULL)
			{
				fclose(fp);
				return NULL;
			}
		}
		n[i].x = x;
		n[i].y = y;
		n[i].type = t;
		n[i].end = 0;		
		n[i].nb = 0;
		j = fscanf(fp,"%d%d%d",&x,&y,&t);
	}
	fclose(fp);
	chn->ni = i;
	chn->n = n;
	chn->linksize = linksize;
	chn->flip = 0;
	chn->im = NULL;
	n[0].end = n[(i>0)?i-1:0].end = END_OF_MOLECULE;
	translate_chain_to_im(chn);

	/*	
	nx = i;
	x = chn->n[which].x;
	y = chn->n[which].y;
	t = chn->n[which].type;
	win_printf("site %d base %d \n at x = %d y = %d \n im = %d\n ",which,t,x,y,chn->im[(y+nx)%nx][(x+nx)%nx]);
	*/
	chn->nstep = 100;
	chn->running = 0;
	chn->nsubstep = 100;
	chn->isubstep = chn->istep = 0;
	chn->T = 1;
	chn->EAT = 2; chn->EGC = 3;
	chn->force = 0.5;
	chn->E = -chn->force * (chn->n[i-1].x - chn->n[0].x);
	establish_bounds(chn, &chn->ngc, &chn->nat,-1);
	chn->flip = 0;	
	free(chn->file);
	chn->file = extract_file_name(NULL, 0, file);
	free(chn->path);
	chn->path = extract_file_path(NULL, 0, file);	
	/*
	x = chn->n[which].x;
	y = chn->n[which].y;
	t = chn->n[which].type;
	win_printf("site %d base %d \n at x = %d y = %d \n im = %d\n ",which,t,x,y,chn->im[(y+nx)%nx][(x+nx)%nx]);
	*/
	return chn;
}
chain *read_simple_mole_file(char *file)
{
	register int i, j;
	nucleotide *n = NULL;
	chain *chn = NULL;
	int   *linksize = NULL; /*,  which = 5, nx; */
	FILE *fp;
	char ch;

	if (file == NULL)	return NULL;
	fp = fopen(file,"r");
	if (fp == NULL)
	{
		win_printf("cannot open file");
		return NULL;
	}
	chn = (chain*)calloc(1,sizeof(chain));
	if (chn == NULL)	return NULL;
	chn->mi = 0;
	for (i = 0, j = fscanf(fp,"%c",&ch); j == 1; i++)
	{
		if (n == NULL || chn->mi <= i)
		{
			chn->mi += 64;
			n = (nucleotide*)realloc(n,chn->mi*sizeof(nucleotide));
			linksize = (int*)realloc(linksize,chn->mi*sizeof(int));	
			if (n == NULL || linksize == NULL)
			{
				fclose(fp);
				return NULL;
			}
		}
		n[i].x = i;
		n[i].y = 0;
		n[i].end = 0;		
		n[i].nb = 0;		
		if (ch == 'a' || ch == 'A')	n[i].type = 1;
		else if (ch == 't' || ch == 'T' )	n[i].type = 4;
		else if (ch == 'c' || ch == 'C' )	n[i].type = 3;
		else if (ch == 'g' || ch == 'G' )	n[i].type = 2;
		else if (ch == ' ' || ch == '\n' || ch == '\r' || ch == '\t')
		{
			i = (i > 0) ? i-1 : i;
		}
		else
		{
			n[i].type = 1;
			win_printf("unknown base type %c at %d",ch,i);
		}
		

		j = fscanf(fp,"%c",&ch);
	}
	fclose(fp);
	chn->ni = i;
	chn->n = n;
	chn->linksize = linksize;
	chn->flip = 0;
	chn->im = NULL;
	n[0].end = n[(i>0)?i-1:0].end = END_OF_MOLECULE;
	translate_chain_to_im(chn);


	chn->nstep = 400;
	chn->running = 0;
	chn->nsubstep = 400;
	chn->T = 1;
	chn->EAT = 2;  chn->EGC = 3;
	chn->force = 0.5;
	chn->E = -chn->force * (chn->n[i-1].x - chn->n[0].x);
	establish_bounds(chn, &chn->ngc, &chn->nat,-1);
	chn->flip = 0;	
	free(chn->file);
	chn->file = extract_file_name(NULL, 0, file);
	free(chn->path);
	chn->path = extract_file_path(NULL, 0, file);	

	return chn;
}
chain *create_random_chain(int nx, int nstep, int nsubstep, float T, float force, float EAT, float EGC)
{
	register int i;
	nucleotide *n;
	chain *chn;
	int  x, y, *linksize = NULL;

	srand(2);
	chn = (chain*)calloc(1,sizeof(chain));
	if (chn == NULL)	return NULL;
	chn->ni = chn->mi = nx;
	chn->im = NULL;
	n = (nucleotide*)calloc(nx,sizeof(nucleotide));
	linksize = (int*)calloc(nx,sizeof(int));	
	if (n == NULL || linksize == NULL) return NULL;
	chn->n = n;
	chn->linksize = linksize;
	for (i = 0, x = y = 0; i < nx; i++)
	{
		n[i].x = x;
		n[i].y = y;
		x = (i % 2) ? x+1 : x;
		y = (i % 2) ? y : y+1;
		n[i].type = (rand()%4) + 1;
		n[i].end = 0;		
		n[i].bound_to[0] = NOT_BOUND;
		n[i].bound_to[1] = NOT_BOUND;
		n[i].bound_to[2] = NOT_BOUND;
	}
	n[0].end = n[nx-1].end = END_OF_MOLECULE;
	translate_chain_to_im(chn);
	chn->nstep = nstep;
	chn->running = 0;
	chn->nsubstep = nsubstep;
	chn->T = T;
	chn->EAT = EAT;
	chn->EGC = EGC;
	chn->force = force;
	establish_bounds(chn, &chn->ngc, &chn->nat,-1);
	chn->E = chn->force * (chn->n[nx-1].x - chn->n[0].x);
	chn->E += chn->ngc * chn->EGC + chn->nat * chn->EAT;
	chn->flip = 0;	
	chn->file = chn->path = NULL;
	return chn;
}
int write_mole_to_file(char *file, chain *chn)
{
	register int i;
	FILE *fp;

	if (file == NULL)	return 1;
	fp = fopen(file,"w");
	if (fp == NULL)		return 1;	
	for (i = 0; i < chn->ni; i++)
		fprintf(fp,"%d\t%d\t%d\n",chn->n[i].x,chn->n[i].y,chn->n[i].type);
	fclose(fp);
	free(chn->file);
	chn->file = extract_file_name(NULL, 0, file);
	free(chn->path);
	chn->path = extract_file_path(NULL, 0, file);	
	return 0;
}


int		end_nucleotide_may_move(chain *chn, int which, int *dx1, int *dy1, int *dx2, int *dy2)
{
	int x, y, nx, i, i1, i2, n = 0;
	int	voisin[4];
	
	*dx1 = *dx2 = *dy1 = *dy2 = 0;
	if (chn == NULL)						return 0;
	if (which != 0 && which != chn->ni - 1)	return 0;
	nx = chn->ni;
	i = (which == 0) ? 1 : nx - 2;
	x = chn->n[i].x;
	y = chn->n[i].y;
	voisin[0] = (int)chn->im[(y+1+nx)%nx][(x+nx)%nx];
	voisin[1] = (int)chn->im[(y+nx)%nx][(x+1+nx)%nx];
	voisin[2] = (int)chn->im[(y-1+nx)%nx][(x+nx)%nx];
	voisin[3] = (int)chn->im[(y+nx)%nx][(x-1+nx)%nx];
	/* first possible spot */
	
	for (i = 0; i < 4 && voisin[i] >= 0 ; i++);
	i1 = i;	
	if (i < 4)
	{
		*dx1 = x - chn->n[which].x;
		*dy1 = y - chn->n[which].y;
		if (i == 0)			*dy1 += 1;
		else if (i == 1)	*dx1 += 1;
		else if (i == 2)	*dy1 += -1;
		else if (i == 3)	*dx1 += -1;
		n++;
	}
	/* second possible spot */
	for (i = i1 + 1; i < 4 && voisin[i] >= 0 ; i++);	
	i2 = i;
	if (i < 4)
	{
		*dx2 = x - chn->n[which].x;
		*dy2 = y - chn->n[which].y;	
		if (i == 0)			*dy2 += 1;
		else if (i == 1)	*dx2 += 1;
		else if (i == 2)	*dy2 += -1;
		else if (i == 3)	*dx2 += -1;
		n++;
	}
	for (i = i2 + 1; i < 4 && voisin[i] >= 0 ; i++);	
	if (i+1 < 4)
	{
		win_printf("error, 3 possible end motion !");
		n++;
	}
	return n; 
}
int		nucleotide_may_move(chain *chn, int which, int *dx, int *dy)
{
	if (chn == NULL)			return 0;
	if (which < 1 || which >= chn->ni - 1)	return 0;
	*dx = chn->n[which+1].x - chn->n[which].x;
	*dy = chn->n[which+1].y - chn->n[which].y;	
	*dx += chn->n[which-1].x - chn->n[which].x;
	*dy += chn->n[which-1].y - chn->n[which].y;
	return (*dx == 0 && *dy == 0) ? 0 : 1; 
}
int		segment_may_rotate(chain *chn, int which, int *dx, int *dy)
{
	register int j;
	int x, y, nx, nb = 0, dxn, dyn;

	if (chn == NULL)			return 0;
	if (which < 1 || which >= chn->ni - 2)	return 0;
	nx = chn->ni;
	x = chn->n[which].x;
	y = chn->n[which].y;
	dxn = x - chn->n[which+1].x;
	dyn = y - chn->n[which+1].y;
	
	j = chn->im[(y+nx)%nx][(x+1+nx)%nx];
	if (j == NOT_BOUND || j > which + 1)
	{
		dx[nb] = dxn + 1; dy[nb] = dyn; nb++;
	}
	j = chn->im[(y+nx)%nx][(x-1+nx)%nx];
	if (j == NOT_BOUND || j > which + 1)
	{
		dx[nb] = dxn - 1; dy[nb] = dyn; nb++;	
	}
	j = chn->im[(y+1+nx)%nx][(x+nx)%nx];
	if (j == NOT_BOUND || j > which + 1)
	{
		dx[nb] = dxn; dy[nb] = dyn + 1; nb++;
	}
	j = chn->im[(y-1+nx)%nx][(x+nx)%nx];
	if (j == NOT_BOUND || j > which + 1)
	{
		dx[nb] = dxn; dy[nb] = dyn - 1; nb++;	
	}
	return nb;
}
int		segment_may_rotate_ok(chain *chn, int which, int dx, int dy)
{
	register int i, j;
	int x, y, nx, no_contact = 0;

	if (chn == NULL)			return 0;
	if (which < 1 || which >= chn->ni - 2)	return 0;
	nx = chn->ni;
	
	if (which > 0) /* nx/2 */
	{
		for (i = which + 1, no_contact = 1; i < nx && no_contact == 1; i++)
		{
			x = chn->n[i].x;
			y = chn->n[i].y;
			j = chn->im[(y+dy+nx)%nx][(x+dx+nx)%nx];
			if (j <= which && j >= 0)
				no_contact = 0;
		}
	}
	else
	{
		for (i = which, no_contact = 1; i >=0 && no_contact == 1; i--)
		{
			x = chn->n[i].x;
			y = chn->n[i].y;
			if (chn->im[(y-dy+nx)%nx][(x-dx+nx)%nx] > which)
				no_contact = 0;
		}
	}
	return no_contact;
}

int 	nucleotide_new_move_ok(chain *chn, int which, int dx, int dy)
{
	int x, y, nx;
	
	if (chn == NULL)			return 0;
	if (which < 0 || which > chn->ni - 1)	return 0;
	nx = chn->ni;
	x = chn->n[which].x;
	y = chn->n[which].y;
	x = (x+dx+nx)%nx;
	y = (y+dy+nx)%nx;	
	return (chn->im[y][x] == NOT_BOUND) ? 1 : 0;
}
int		do_rotate_segment(chain *chn, int which, int dx, int dy)
{
	register int i;
	int x, y, nx;

	if (chn == NULL)			return 0;
	if (which < 1 || which >= chn->ni - 2)	return 0;
	nx = chn->ni;
	
	if (which > 0) /* nx/2 */
	{
		for (i = which + 1; i < nx; i++)
		{
			x = chn->n[i].x;
			y = chn->n[i].y;
			chn->im[(y+nx)%nx][(x+nx)%nx] = NOT_BOUND;
		}
		for (i = which + 1; i < nx; i++)
		{
			chn->n[i].x += dx;
			chn->n[i].y += dy;
			x = chn->n[i].x;
			y = chn->n[i].y;			
			chn->im[(y+nx)%nx][(x+nx)%nx] = i;
		}		
	}
	else	/* we should not get here */
	{
		for (i = which; i >=0 ; i--)
		{
			x = chn->n[i].x;
			y = chn->n[i].y;
			chn->im[(y+nx)%nx][(x+nx)%nx] = NOT_BOUND;
			chn->n[i].x -= dx;
			chn->n[i].y -= dy;
			x = chn->n[i].x;
			y = chn->n[i].y;			
			chn->im[(y+nx)%nx][(x+nx)%nx] = i;
		}
	}
	return 0;
}
int	establish_bounds(chain *chn, int *ngc, int *nat, int debug)
{
	register int i,j, k;
	int top, bot, right, left;	/* possible site */
	int nx, x, y, type;
	nucleotide *nuc;

	if (chn == NULL)				return -1;
	nx = chn->ni;
	*ngc = *nat = 0;
	for(i = 0; i < nx; i++)
	{
		nuc = &(chn->n[i]);
		nuc->nb = 0;
		nuc->nlink = i;	
		nuc->link = &nuc->nlink;
		chn->linksize[i] = 0;
	}
	/* I exclude both end so nore than two contact */	
	for(i = 1; i < nx-1; i++)
	{
		nuc = &(chn->n[i]);
		type = nuc->type;
		x = nuc->x;
		y = nuc->y;
		if (debug >= 0)	fprintf(stderr,"site %d x %d y %d\n",i,x,y);
	/* these are the four possible sites */
		top = chn->im[(nx+y+1)%nx][(nx+x)%nx];
		bot = chn->im[(nx+y-1)%nx][(nx+x)%nx];
		right = chn->im[(nx+y)%nx][(nx+x+1)%nx];
		left = chn->im[(nx+y)%nx][(nx+x-1)%nx];	
		if (debug >= 0)	fprintf(stderr,"top %d bot %d right %d left %d \n",top,bot,right,left);
	/* don't allow direct neibourg to bound ! */
		top = (((top-i) == 1) || ((top-i) == -1)) ? NEIGHBOUR : top;
		bot = (((bot-i) == 1) || ((bot-i) == -1)) ? NEIGHBOUR : bot;	
		right = (((right-i) == 1) || ((right-i) == -1)) ? NEIGHBOUR : right;
		left = (((left-i) == 1) || ((left-i) == -1)) ? NEIGHBOUR : left;
		if (debug >= 0)	fprintf(stderr,"top %d bot %d right %d left %d \n",top,bot,right,left);

		if (top != NEIGHBOUR)
			top = ((type+chn->n[top].type) == COMPLEMENT) ? top : NOT_BOUND;
		if (bot != NEIGHBOUR)
			bot = ((type+chn->n[bot].type) == COMPLEMENT) ? bot : NOT_BOUND;
		if (right != NEIGHBOUR)
			right = ((type+chn->n[right].type) == COMPLEMENT) ? right : NOT_BOUND;
		if (left != NEIGHBOUR)
			left = ((type+chn->n[left].type) == COMPLEMENT) ? left : NOT_BOUND;
		if (debug >= 0)	fprintf(stderr,"top %d bot %d right %d left %d \n",top,bot,right,left);
			
		j = nx;
		k = -1;
		if (top >= 0)	
		{
			nuc->bound_to[nuc->nb++] = k = top;
			if (k > i)
			{
				if (chn->n[k].link[0] < k)
				{
					nuc->link[0] = chn->n[k].link[0];
					if (debug >= 0)	fprintf(stderr,"site %d relinked to %d by top %d\n",i,chn->n[k].link[0],k);
				}
				else 
				{
					chn->n[k].link = nuc->link;
					if (debug >= 0)	fprintf(stderr,"site %d top %d linked\n",i,k);				
				}
			}
		}
		if (bot >= 0)	
		{
			nuc->bound_to[nuc->nb++] = k = bot;
			if (k > i)
			{
				if (chn->n[k].link[0] < k)
				{
					nuc->link[0] = chn->n[k].link[0];
					if (debug >= 0)	fprintf(stderr,"site %d relinked to %d by bot %d\n",i,chn->n[k].link[0],k);
				}
				else 
				{
					chn->n[k].link = nuc->link;
					if (debug >= 0)	fprintf(stderr,"site %d bot %d linked\n",i,k);				
				}
			}
		}
		if (right >= 0)	
		{
			nuc->bound_to[nuc->nb++] = k = right;
			if (k > i)
			{
				if (chn->n[k].link[0] < k)
				{
					nuc->link[0] = chn->n[k].link[0];
					if (debug >= 0)	fprintf(stderr,"site %d relinked to %d by right %d\n",i,chn->n[k].link[0],k);
				}
				else 
				{
					chn->n[k].link = nuc->link;
					if (debug >= 0)	fprintf(stderr,"site %d right %d linked\n",i,k);				
				}
			}
		}
		if (left >= 0)
		{
			nuc->bound_to[nuc->nb++] = k = left;
			if (k > i)
			{
				if (chn->n[k].link[0] < k)
				{
					nuc->link[0] = chn->n[k].link[0];
					if (debug >= 0)	fprintf(stderr,"site %d relinked to %d by left %d\n",i,chn->n[k].link[0],k); 
				}
				else 
				{
					chn->n[k].link = nuc->link;
					if (debug >= 0)	fprintf(stderr,"site %d left %d linked\n",i,k);				
				}
			}
		}
	}
	for(i = 0; i < nx; i++)
	{
		nuc = &(chn->n[i]);
		chn->linksize[*(nuc->link)]++;
	}
	for(i = 0, *ngc = *nat = 0; i < nx; i++)
	{
		nuc = &(chn->n[i]);
		j = chn->linksize[i];
		if (j > 1)
		{
			if (nuc->type == ADENINE || nuc->type == THYMINE) 
			{
				*nat += j/2;
				if (debug >= 0)	fprintf(stderr,"AT bound %d in length at %d\n",j/2,i);
			}
			if (nuc->type == GUANINE || nuc->type == CYTOSINE) 
			{
				*ngc += j/2;
				if (debug >= 0)	fprintf(stderr,"GC bound %d in length at %d\n",j/2,i);
				
			}
		}
	}
	return 0;
}


int 	move_nucleotide(chain *chn, int which, int dx, int dy)
{
	int nx, ix, iy;
	nucleotide *nuc;
	
	if (chn == NULL)						return 0;
	if (which < 0 || which > chn->ni - 1)	return 0;
	nx = chn->ni;
	nuc = chn->n + which;
	ix = (nuc->x+nx)%nx;
	iy = (nuc->y+nx)%nx;	
	chn->im[iy][ix] = NOT_BOUND;
	nuc->x += dx;
	nuc->y += dy;
	ix = (nuc->x+nx)%nx;
	iy = (nuc->y+nx)%nx;	
	chn->im[iy][ix] = which;
	return 0;
}
int		bound_nucleotide_to_neibourg(chain *chn, int nuc)
{
# ifdef	ENCOURS	
	register int i;
	int top, bot, right, left;	/* possible site */
	int nx, x, y, type;

	if (chn == NULL)				return -1;
	if (chn->n[nuc].bound_to[0] >= 0)		return -2;
	type = chn->n[nuc].type;
	x = chn->n[nuc].x;
	y = chn->n[nuc].y;	
	nx = chn->ni;
	/* these are the four possible sites */
	top = chn->im[(y+1)%nx][x%nx];
	bot = chn->im[(y-1)%nx][x%nx];
	right = chn->im[y%nx][(x+1)%nx];
	left = chn->im[y%nx][(x-1)%nx];	
	/* don't allow direct neibourg to bound ! */
	top = (((top - nuc) == 1) || ((top - nuc) == -1)) ? NEIGHBOUR : top;
	bot = (((bot - nuc) == 1) || ((bot - nuc) == -1)) ? NEIGHBOUR : bot;	
	right = (((right - nuc) == 1) || ((right - nuc) == -1)) ? NEIGHBOUR : right;	
	left = (((left - nuc) == 1) || ((left - nuc) == -1)) ? NEIGHBOUR : left;

	if (top >= 0)
		top = ((type + chn->n[top].type) == COMPLEMENT) ? top : NO_AFFINITY;
	if (bot >= 0)
		bot = ((type + chn->n[bot].type) == COMPLEMENT) ? bot : NO_AFFINITY;
	if (right >= 0)
		right = ((type + chn->n[right].type) == COMPLEMENT) ? right : NO_AFFINITY;
	if (left >= 0)
		left = ((type + chn->n[left].type) == COMPLEMENT) ? left : NO_AFFINITY;
	i = 0;
	if (top >= 0)	i++;
	if (bot >= 0)	i++;
	if (right >= 0)	i++;
	if (left >= 0)	i++;
	if (i == 0)		return 0;
	if (i == 1)		return 1;
# endif
	return 0;
}

int	type_of_nucleotide_on_spot(chain *chn, int start, int x, int y)
{
	register int i, j;
	int dx, dy, di = 0, dj = 0;
	nucleotide *n;
	
	if (chn == NULL)	return -2;
	n = chn->n;
	for (i = start, j = start; i < chn->ni || j >= 0; i+= di, j-= dj)
	{
		if (i < chn->ni)
		{
			dx = n[i].x - x;
			dy = n[i].y - y;
			dx = (dx < 0) ? -dx : dx;
			dy = (dy < 0) ? -dy : dy;
			if (dx == 0 && dy == 0)		return n[i].type;
			di = (dx > dy) ? dx : dy;
		}
		if (j >= 0)
		{
			dx = n[j].x - x;
			dy = n[j].y - y;
			dx = (dx < 0) ? -dx : dx;
			dy = (dy < 0) ? -dy : dy;
			if (dx == 0 && dy == 0)		return n[j].type;
			dj = (dx > dy) ? dx : dy;			
		}
	}
	return -1;
}

int  metropolis(void)
{
  register int i, k;
  static int dx[4], dy[4], iE, nx, np, ip, nf = 0, nt, stop_loop;
  static int nat = 0, ngc = 0, dis, nfs;
  static chain *chn = NULL;
  static pltreg *pr;
  static O_p *op;
  static d_s *ds = NULL, *dsb = NULL;
  static double L = 0, L2 = 0, X = 0, X2 = 0, tmp, natm = 0, ngcm = 0, E;
  static double Ls = 0, Ls2 = 0, Xs = 0, Xs2 = 0,  natms = 0, ngcms = 0, Es;
  static double La = 0, L2a = 0, Xa = 0, X2a = 0, RD, EX;
  clock_t t_start = 0, t_end;    

  if(updating_menu_state != 0)  return D_O_K;
  if (chn == NULL || chn->istep == 0)
  {
    if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)  return D_O_K;
    if ((chn = find_chain_in_pltreg(pr)) == NULL)  return D_O_K;  

    ds = find_source_specific_ds_in_pr(pr, "Single strand chain");
    dsb = find_source_specific_ds_in_pr(pr, "Single strand bounds");
    i = find_op_nb_of_source_specific_ds_in_pr(pr, "Single strand chain");
    if (i >= 0)   op = pr->o_p[i]; 
    /*  Initialisation du generateur de nombre aleatoire */  
    /* randomize();*/
    srand(15);
    nx = chn->ni;    
    nt = 0;
    L = 0; L2 = 0; X = 0; X2 = 0; natm = 0; ngcm = 0;
    Ls = 0; Ls2 = 0; Xs = 0; Xs2 = 0; natms = 0; ngcms = 0;
    La = 0; L2a = 0; Xa = 0; X2a = 0;
  }
  if (chn != find_chain_in_pltreg(NULL)) return D_O_K;
  if (chn->istep >= chn->nstep) return D_O_K;

  t_start = clock();
  t_end = t_start + (delay * CLOCKS_PER_SEC)/1000;
  /*  simulation starting point  */
  for (; clock() < t_end; )
  {
          if (chn->isubstep%chn->nsubstep == 0)
	    {
                Ls = Ls2 = Xs = Xs2 = natms = ngcms = Es = 0;
		nfs = 0;
	    }
    /*    take a nucleotide at random  */
    /* i = random()%nx; */
    i = rand();
/*    win_printf("random %d, nx %d -> %d",i, nx, i%nx);*/
    i %= nx;
    iE = 0;
    stop_loop = 0;
    /*    check whether nucleotide is a chain's end  */
    if (i == 0 || i == chn->ni - 1)
    {
      np = end_nucleotide_may_move(chn, i, dx, dy, dx + 1, dy + 1);
/*      win_printf("end move %d",np);*/
      if (np == 0)    stop_loop = 1;
      else if (np == 2)  /* two possible positions */
      {
        ip = rand()%2;
        if (ip == 1)
        {
          dx[0] = dx[1];
          dy[0] = dy[1];
        }
        iE = (i == 0) ? dx[0]: - dx[0];
      }
      else if (np == 1)
      {
        iE = (i == 0) ? dx[0]: - dx[0];
      }
      else stop_loop = 1;
    }
    else
    {
      /*    check whether it may move */
      /*
      if ((nucleotide_may_move(chn, i, dx, dy)) == 0) break;
      if ((nucleotide_new_move_ok(chn, i, dx[0], dy[0])) == 0) break;
      iE = 0;
      */
      np = segment_may_rotate(chn, i, dx, dy);
      if (np == 0) stop_loop = 1;
      else if (np == 2)  /* two possible positions */
      {
        ip = rand()%2;
        if (ip == 1)
        {
          dx[0] = dx[1];
          dy[0] = dy[1];
        }
        iE =  -dx[0];
      }
      else if (np == 1)
      {
        iE =  -dx[0];
      }
      else 
      {
        win_printf("abnormal move possibility %d\n"
             "which = %d dx = %d dy = %d",np,i, dx[0], dy[0]);
        break;
      }
      if (stop_loop == 0)
      {
        if ((segment_may_rotate_ok(chn, i, dx[0], dy[0])) == 0) 
          stop_loop = 1;
      }
    }
    
/*  Regle de Metropolis, ((double)random())/MAXINT : nb aleatoire ds [0,1] */
    if (iE > 2 || iE < -2)   
    {
      win_printf("energy out of range %d\n which = %d dx = %d dy = %d",
           iE,i, dx[0], dy[0]);
      break;
    }
    if (stop_loop == 0)
    {  /* we move to evaluate the bindings and thus energy */
      if (i == 0 || i == nx-1)
        move_nucleotide(chn, i, dx[0], dy[0]);
      else  do_rotate_segment(chn, i, dx[0], dy[0]);
      
      establish_bounds(chn, &ngc, &nat, -1);
      E = chn->force * (chn->n[nx-1].x - chn->n[0].x);
      E += ngc * chn->EGC + nat * chn->EAT;
      EX = exp((E - chn->E)/chn->T);
      RD = (((double)rand())/RAND_MAX) ;
      if (display_debug)  
      {
        dis = win_printf("old E = %g new E = %g\n"
             "exp\\Delta E/kT = %g rand = %g",chn->E,E,EX,RD);
             if (dis == CANCEL)
        display_debug = 0;
      }
      if (EX > RD)
      {  /* move is accepted */
        if (display_debug) win_printf("movde");
        chn->E = E;
        chn->nat = nat;
        chn->ngc = ngc;
        chn->L = chn->n[nx-1].x-chn->n[0].x;  
        chn->flip++;
	display_title_message("flip %d L %g E %g",chn->flip,L,E);
      }
      else  /* we get back in position */
      {
        if (i == 0 || i == nx-1)
          move_nucleotide(chn, i, -dx[0], -dy[0]);
        else  do_rotate_segment(chn, i, -dx[0], -dy[0]);
        establish_bounds(chn, &ngc, &nat, -1);
      }
      nf++;
      nfs++;
    }
    chn->L = chn->n[nx-1].x-chn->n[0].x;          
    L += chn->L;
    Ls += chn->L;
    L2 += chn->L*chn->L;
    tmp = chn->n[nx-1].y-chn->n[0].y;
    X += tmp;
    Xs += tmp;
    X2 += tmp*tmp;
    ngcm += chn->ngc;
    natm += chn->nat;
    ngcms += chn->ngc;
    natms += chn->nat;
    chn->isubstep++;
    /*
    else win_printf("%d,%d not moving %d, of %d in x %d in y energie %d",k,j,i, dx[0],dy[0], iE); */
    }
    if (chn->isubstep >= chn->nsubstep)
    {
      chn->isubstep = 0;
      La += chn->L;
      L2a += chn->L*chn->L;
      tmp = chn->n[nx-1].y-chn->n[0].y;
      Xa += tmp;
      X2a += tmp*tmp;
      translate_chain_to_ds(chn, ds, 1);
      translate_chain_to_bound_ds(chn, dsb, 1);
      k = chn->istep;
      nt = (chn->nsubstep*(chn->istep+1));
      if (nt != 0)  
        set_plot_title(op,"\\stack{{F = %g, L = %g ({+\\over -} %g), flip %d}"
         "{<X> = %g X2 = %g nf %d}{<Xa> = %g X2a = %g Fx^2/l %g}"
         "{n AT %f n GC %f}}",chn->force,L/nt,
         sqrt((L2/nt)-((L/nt)*(L/nt))),chn->flip,X/nt,(X2/nt),nf,
         Xa/(k+1),(X2a/(k+1)),chn->force*X2a/La,natm/nt,ngcm/nt);
      pr->one_p->need_to_refresh = 1;    
      op->need_to_refresh = 1;    
      refresh_plot(pr,pr->cur_op);
      if (ds_data_recording != NULL && k < ds_data_recording->nx)  
        {
          ds_data_recording->xd[k] = k*chn->nsubstep;
          ds_data_recording->yd[k] = (nfs !=0) ? Xs/nfs : Xs; //chn->n[nx-1].y-chn->n[0].y;
        } 
      if (ds_data_recording2 != NULL && k < ds_data_recording2->nx)  
        {
          ds_data_recording2->xd[k] = k*chn->nsubstep;
          ds_data_recording2->yd[k] = (nfs !=0) ? Ls/nfs : Ls; //chn->n[nx-1].x-chn->n[0].x;
        }     
      if (ds_data_recording3 != NULL && k < ds_data_recording3->nx)  
        {
          ds_data_recording3->xd[k] = k*chn->nsubstep;
          tmp = ngcms * chn->EGC + natms * chn->EAT;
          ds_data_recording3->yd[k] = (nfs !=0) ? tmp/nfs : tmp; //chn->n[nx-1].x-chn->n[0].x;
        }
      chn->istep++;
    }
    if (chn->istep >= chn->nstep)
    {
      //chn->istep = 0;       
      nt = chn->nsubstep * chn->nstep;
      if (nf != 0)
        {
          L /= nt;
          X /= nt;
          L2 /= nt;
          L2 -= L*L;
          L2 = (L2 >= 0) ? sqrt(L2) : L2;
          X2 /= nt;
          ngcm /= nt;
          natm /= nt;
        }
  
  /*
  if (record)
  {
    if (logfil == NULL)   
    {
      logfil = grab_specific_log_file();
      dump_to_specific_log_file_only(logfil, "%%index\tF\t{\\delta}x\t{\\delta}x^2\tL\t{\\sigma_L}\tnf\tF{\\delta}x^2/L\tnAT\tnGC");
    }
    dump_to_specific_log_file_only(logfil, "%d\t%g\t%g\t%g\t%g\t%g\t%d\t%g\t%g\t%g",nc++,chn->force,X,X2,L,L2,nf,(L!=0)?X2*chn->force/L:0,natm,ngcm);
  }  
  */
  //return (chn->flip);
  }
  return D_O_K;
}

int ssdna_plot_idle_action(pltreg *pr, O_p *op, DIALOG *d)
{
  metropolis();
}


int ssdna_op_idle_action(O_p *op, DIALOG *d)
{
  metropolis();
}


chain  *find_chain_in_pltreg(pltreg *pr) 
{
  if (pr == NULL) pr = find_pr_in_current_dialog(NULL);
  if (pr == NULL)  return NULL;
  if ( pr->use.to == IS_CHAIN)  return (chain *)pr->use.stuff;
  return NULL;
}
/*
int     on_yes_kill_chain_acreg(char ch)
{
  if (ch != CR)		return D_O_K;	
	answer_yes = 0;	
	if (win_printf("Do you really want \nto destroy this window ?") == OK)
		kill_chain_acreg(CR);
	return 0;	
}

int kill_chain_acreg(char ch)
{
	if (ch != CR) return D_O_K;
	free_chain(find_chain_in_acreg(cur_ac_reg));
	return kill_acreg(ch);
}
*/
int		translate_chain_to_ds(chain *chn, d_s *ds, int fixorigin)
{
	register int i;
	
	if(chn == NULL || ds == NULL)	return 1;
	
	for (i = 0; i < chn->ni && i < ds->nx; i++)
	{
		ds->xd[i] = (fixorigin) ? chn->n[i].x - chn->n[0].x : chn->n[i].x;
		ds->yd[i] = (fixorigin) ? chn->n[i].y - chn->n[0].y : chn->n[i].y;	
	}
	return 0;
}

int		translate_chain_to_bound_ds(chain *chn, d_s *ds, int fixorigin)
{
	register int i, j, k;
	int x, y, x0, y0;
	nucleotide *nuc, *neibourg;
	
	
	if(chn == NULL || ds == NULL)	return 1;
	x0 = chn->n[0].x;
	y0 = chn->n[0].y;
	for (i = 0, j = 0; i < chn->ni && j < ds->mx; i++)
	{
		nuc = &(chn->n[i]);
		x = nuc->x;
		y = nuc->y;
		if (nuc->nb > 0)
		{
			for (k = 0; k < nuc->nb; k++)
			{
				if (nuc->bound_to[k] > i)
				{
					ds->xd[j] = (fixorigin) ? x - x0 : x;
					ds->yd[j++] = (fixorigin) ? y - y0 : y;	
					neibourg = &(chn->n[nuc->bound_to[k]]);
					ds->xd[j] = (fixorigin) ? neibourg->x - x0 : neibourg->x;
					ds->yd[j++] = (fixorigin) ? neibourg->y - y0 : neibourg->y;
				}
			}
		}					
	}
	ds->nx = j;
	return 0;
}

int	set_force(void)
{
	register int i;
	chain *chn = NULL;
	float f, at, gc;
	
	if(updating_menu_state != 0)	return D_O_K;		
	chn = find_chain_in_pltreg(NULL); 
	if (chn == NULL) return D_O_K;
	f = chn->force;
	at = chn->EAT;
	gc = chn->EGC;
	i = win_scanf("New force %fE de AT %f E de GC %f",&f,&at,&gc);
	if (i == CANCEL)	return D_O_K;
	chn->force = f;
	chn->EAT = at;
	chn->EGC = gc;
	chn->E = chn->force * (chn->n[chn->ni-1].x - chn->n[0].x);
	chn->E += chn->ngc * chn->EGC + chn->nat * chn->EAT;
	return D_O_K;
}
int do_iterate_and_record(void)
{
	chain *chn = NULL;
	pltreg *pr;
	O_p *op;
	d_s *ds;
	
	if(updating_menu_state != 0)	return D_O_K;		
	chn = find_chain_in_pltreg(NULL); 
	if (chn == NULL) return D_O_K;
	if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)	return D_O_K;
	if ((op = create_and_attach_one_plot(pr, chn->nstep, chn->nstep, 0)) == NULL)
	  return D_O_K;
	ds = op->dat[0];	
	set_plot_x_title(op,"Nb d'iterations");
	set_plot_y_title(op,"Y");
	ds_data_recording = ds;
	if ((op = create_and_attach_one_plot(pr, chn->nstep, chn->nstep, 0)) == NULL)
	{
		win_printf("cannot create ds!");
		return D_O_K;
	}
	ds = op->dat[0];	
	set_plot_x_title(op,"Nb d'iterations");
	set_plot_y_title(op,"L");	
	ds_data_recording2 = ds;
	if ((op = create_and_attach_one_plot(pr, chn->nstep, chn->nstep, 0)) == NULL)
	{
		win_printf("cannot create ds!");
		return D_O_K;
	}
	ds = op->dat[0];	
	set_plot_x_title(op,"Nb d'iterations");
	set_plot_y_title(op,"E bound");	
	ds_data_recording3 = ds;
	chn->istep = 0;
	op->op_idle_action = ssdna_op_idle_action;
       	//plot_idle_action = ssdna_plot_idle_action;
	//	metropolis();
	//	ds_data_recording = NULL; 
	//ds_data_recording2 = NULL; 
	//ds_data_recording3 = NULL; 
	do_one_plot(pr);	
	return D_O_K;
}

int do_force_curve(void)
{
	register int i;
	chain *chn = NULL;
	float f;
	static float f0 = 5, df = 0.5;
	static int nx = 20;
	pltreg *pr;
	O_p *op;
	
	if(updating_menu_state != 0)	return D_O_K;		
	chn = find_chain_in_pltreg(NULL); 
	if (chn == NULL) return D_O_K;
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return D_O_K;
	f = chn->force;
	i = win_scanf("force max %f nb of points %d decrease factor%f",&f0,&nx,&df);
	if (i == CANCEL)	return D_O_K;
	/*
	if (logfil == NULL) 	
	{
		logfil = grab_specific_log_file();
		dump_to_specific_log_file_only(logfil, "%%index\tF\t{\\delta}x\t{\\delta}x^2\tL\t{\\sigma_L}\tnf\tF{\\delta}x^2/L\tnAT\tnGC");
	}
	*/
	for (i = 0, f = f0; i < nx; i++, f*=df)
	{
		chn->force = f;
		chn->E = chn->force * (chn->n[nx-1].x - chn->n[0].x);
		chn->E += chn->ngc * chn->EGC + chn->nat * chn->EAT;
		//plot_idle_action = ssdna_plot_idle_action;
		op->op_idle_action = ssdna_op_idle_action;
		record = 0;
		metropolis();
		record = 1;
		metropolis();	
	}
		
	return D_O_K;	
}
int	info_on_site(void)
{
	register int i;
	chain *chn = NULL;
	static int which = 5;
	int x, y, nx, t,nm,nmv,dx,dy;
	
	if(updating_menu_state != 0)	return D_O_K;		
	chn = find_chain_in_pltreg(NULL); 
	if (chn == NULL) return D_O_K;
	i = win_scanf("New site %d",&which);
	if (i == CANCEL)	return D_O_K;
	nx = chn->ni;
	x = chn->n[which].x;
	y = chn->n[which].y;
	t = chn->n[which].type;
	nm = nucleotide_may_move(chn, which, &dx, &dy);
	nmv = nucleotide_new_move_ok(chn, which, dx, dy);
	win_printf("site %d base %d \n at x = %d y = %d \n im = %d\n may move %d dx %d dy %d\n ok %d\nlinked at %d nb of bounds %d length of bounds %d",which,t,x,y,chn->im[(y+nx)%nx][(x+nx)%nx],nm,dx,dy,nmv,chn->n[which].link[0],chn->n[which].nb,chn->linksize[which]);
	return D_O_K;
}
int	info_on_im_site(void)
{
	register int i;
	chain *chn = NULL;
	static int x = 5, y = 10;
	int  nx;
	
	if(updating_menu_state != 0)	return D_O_K;		
	chn = find_chain_in_pltreg(NULL); 
	if (chn == NULL) return D_O_K;
	i = win_scanf("im site x %d y %d",&x,&y);
	if (i == CANCEL)	return D_O_K;
	nx = chn->ni;
	win_printf("im site  \n at x = %d y = %d \n im = %d",x,y,chn->im[(y+nx)%nx][(x+nx)%nx]);
	return D_O_K;
}

int	info_on_chain(void)
{
	chain *chn = NULL;
	
	if(updating_menu_state != 0)	return D_O_K;		
	chn = find_chain_in_pltreg(NULL); 
	if (chn == NULL) return D_O_K;
	win_printf("Chain has %d site\nEnergy = %g T = %g EAC = %g EGC = %g\nForce %g",chn->ni,chn->E,chn->T,chn->EAT,chn->EGC,chn->force);
	return D_O_K;
}

int	test_end(void)
{
	register int i;
	chain *chn = NULL;
	int nx, mx, dx1, dy1, dx2, dy2, ip;
	
	if(updating_menu_state != 0)	return D_O_K;		
	chn = find_chain_in_pltreg(NULL); 
	if (chn == NULL) return D_O_K;
	nx = chn->ni;
	mx = chn->mi;
	ip = end_nucleotide_may_move(chn, 0, &dx1, &dy1, &dx2, &dy2);
	i = win_printf("end 0 %d moves dx %d dy %d\n dx %d dy %d",ip,dx1,dy1,dx2,dy2);
	if (i == CANCEL)	return D_O_K;
	ip = end_nucleotide_may_move(chn, nx-1, &dx1, &dy1, &dx2, &dy2);
	i = win_printf("end %d %d moves dx %d dy %d\n dx %d dy %d",nx-1,ip,dx1,dy1,dx2,dy2);
	if (i == CANCEL)	return D_O_K;	
	return D_O_K;
}
int	set_T(void)
{
	register int i;
	chain *chn = NULL;
	float t;
	
	if(updating_menu_state != 0)	return D_O_K;		
	chn = find_chain_in_pltreg(NULL); 
	if (chn == NULL) return D_O_K;
	t = chn->T;
	i = win_scanf("New temperature %f",&t);
	if (i == CANCEL)	return D_O_K;
	chn->T = t;
	return D_O_K;
}
int	set_repeat(void)
{
	register int i;
	chain *chn = NULL;
	int ns, nss;
	
	if(updating_menu_state != 0)	return D_O_K;		
	chn = find_chain_in_pltreg(NULL); 
	if (chn == NULL) return D_O_K;
	ns = chn->nstep;
	nss = chn->nsubstep;
	i = win_scanf("nombre de pas entre affichage %dnombre de simulation %d",&nss,&ns);
	if (i == CANCEL)	return D_O_K;
	chn->nstep = ns;
	chn->nsubstep = nss;
	return D_O_K;
}
int	do_stop(void)
{
	chain *chn = NULL;
	pltreg *pr;
	O_p *op;
	
	if(updating_menu_state != 0)	return D_O_K;		
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return D_O_K;

	chn = find_chain_in_pltreg(NULL); 
	if (chn == NULL) return D_O_K;
	chn->running = 0;
       	plot_idle_action = NULL;
	op->op_idle_action = NULL;
	return D_O_K;
}

char *get_saved_chain_filename(chain *chn)
{
	register int i;
	char c[256], c_dir[256];
	static char *plt_dir = NULL, s[512];
	
	if ( chn == NULL) 		return NULL;

	if (chn->file != NULL)          strcpy(c,chn->file);
	else 				strcpy(c,"untitle.chn");
	for (i = 0; i< 255 && c[i] !=0; i++);
	for (i-- ; i>=0 && c[i] != '.'; i--);
	if (c[i] == '.')	strcpy(c+i,".chn"); 
	if (chn->path != NULL) 	strcpy(c_dir,chn->path);
	else 				
	{
		if (plt_dir == NULL)
		{
			my_getcwd(c_dir ,DIR_LEN);
			slash_to_backslash(c_dir);
			plt_dir = (char *)calloc(256,sizeof(char));
			strcpy(plt_dir,c_dir);
		}
		else strcpy(c_dir,plt_dir);
	}

	append_filename(s, (const char *)c_dir, (const char *)c, 512);
	switch_allegro_font(1);
	i = file_select_ex("Save chain", s, "chn", 512, 0, 0);
	switch_allegro_font(0);
	if (i != 0) 	
	{
		extract_file_path(c_dir, 512, s);
		strcat(c_dir,"\\");
		if (chn->path == NULL) 	strcpy(plt_dir,c_dir);
		return s;
	}
	return NULL;
}		
int	do_write_chain_to_file(void)
{
	int i;
	chain *chn = NULL;
	char *file;
	
	if(updating_menu_state != 0)	return D_O_K;		
	chn = find_chain_in_pltreg(NULL); 
	if (chn == NULL) return D_O_K;
	file = get_saved_chain_filename(chn);
	if (file == NULL)	return D_O_K;
	i = write_mole_to_file(file, chn);
	if (i) return win_printf("Cannot open\n %s",backslash_to_slash(file));
	return D_O_K;
}	

int	do_get_bounds(void)
{
	chain *chn = NULL;
	int ngc = 0, nat = 0;
	pltreg *pr;
	O_p *op;
	d_s *dsb;
	static int debug = -1;
			
	if(updating_menu_state != 0)	return D_O_K;		

	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return D_O_K;
	ngc = win_scanf("debug site (<0) no message %d",&debug);
	if (ngc == CANCEL)	return D_O_K;
	chn = find_chain_in_pltreg(NULL); 
	if (chn == NULL) return D_O_K;
	dsb = find_source_specific_ds_in_pr(pr, "Single strand bounds");
	establish_bounds(chn, &ngc, &nat, debug);
	win_printf("Chain as %d AT bounds\n and %d GC bounds",nat,ngc);
	if (dsb != NULL)		translate_chain_to_bound_ds(chn, dsb, 0);
	dsb = find_source_specific_ds_in_pr(pr, "Single strand chain");
	if (dsb != NULL) 		translate_chain_to_ds(chn, dsb, 0);		
	refresh_plot(pr,pr->cur_op);	
	return D_O_K;
}			

chain		*load_chain(void)
{
	register int i;
	char path[512], file[256];
	chain *chn;
	static int mode = 0;
	static char fullfile[512], *fui = NULL;


	if(updating_menu_state != 0)	return D_O_K;	
		
	switch_allegro_font(1);
	if (fui == NULL)
	{
		my_getcwd(fullfile, 512);
		strcat(fullfile,"\\");
	}	
	i = file_select_ex("Load chain file (*.chn)", fullfile, "chn", 512, 0, 0);

	switch_allegro_font(0);
	if (i != 0) 	
	{
		fui = fullfile;
		extract_file_name(file, 256, fullfile);
		extract_file_path(path, 512, fullfile);
		strcat(path,"\\");
		sprintf(fullfile,"%s%s",path,file);
		printf("file = %s\n",fui);
		i = win_scanf("is it a simple mole file (->1) or a real chain file (->0) %d",&mode);
		if (i == CANCEL)	return D_O_K;
		if (mode == 0)	chn = read_mole_file(fui);
		else	chn = read_simple_mole_file(fui);
		chn->file = file;
		chn->path = path;
		return chn;
	}
	return NULL;
}			
int do_read_chain(void)
{
	chain *chn = NULL;
	pltreg *pr;	

	if(updating_menu_state != 0)	return D_O_K;		
	chn = load_chain();
	if (chn == NULL)	return win_printf("null chain");
	pr = create_chain_pltreg(chn);
	pr->one_p->need_to_refresh = 1;    
	switch_project_to_this_pltreg(pr);
	return D_REDRAWME;
}	
int set_debug_on_off(void)
{
	chain *chn = NULL;
	
	if(updating_menu_state != 0)	return D_O_K;		
	chn = find_chain_in_pltreg(NULL); 
	if (chn == NULL) return D_O_K;
	display_debug = mn_index;
	return D_O_K;
}									

int	singlest_main(void)
{
	register int i;
	pltreg *pr;
	chain *chn = NULL, *chn1 = NULL;
	static float t = 1, f = .5, EAT = 2, EGC = 3;
	static int nx = 128, ns = 500, nss = 400;
	
	if(updating_menu_state != 0)	return D_O_K;		
	i = win_scanf("Simulation Monte Carlo d'un polymere\n"
		      "nombre de nucl�otides %d temperature %f force %f "
		      "energie AT %f �nergie GC %f nombre de pas entre affichage %d"
		      "nombre de simulation %d",&nx,&t,&f,&EAT,&EGC,&nss,&ns);
	if (i == CANCEL)	return D_O_K;
	chn = create_random_chain(nx, ns, nss, t, f, EAT, EGC);

	pr = create_chain_pltreg(chn);
	chn1 = find_chain_in_pltreg(pr);
	broadcast_dialog_message(MSG_DRAW,0);
	return D_O_K;	
}
		
	
pltreg		*create_chain_pltreg(chain *chn)
{
	pltreg *pr;
	O_p *op;
	d_s *ds, *ds2;


	if (!chn) return NULL;
	pr = create_and_register_new_plot_project(0,   32,  900,  668);
	if (pr == NULL)	
	{
	  win_printf("Could not find or allocte plot region!");
	  return D_O_K;	
	}	
	op = pr->one_p;
	ds = create_and_attach_one_ds(op,chn->ni, chn->ni, 0);
	ds->source = my_sprintf(ds->source,"Single strand chain");
	
	if ((ds2 = create_and_attach_one_ds(op, 2*chn->ni,2*chn->ni, 0)) == NULL)
	{
		win_printf("cannot create ds!");
		return NULL;
	}
	ds2->source = my_sprintf(ds2->source,"Single strand bounds");
	set_dash_line(ds2);
	translate_chain_to_ds(chn,ds, 0);
	pr->one_p->need_to_refresh = 1;    
	set_plot_x_fixed_range(op);
	set_plot_y_fixed_range(op);
	op->x_lo = - 0.25 * chn->ni;
	op->x_hi = 1.05 * chn->ni;
	op->y_lo = -.65 * chn->ni;
	op->y_hi = .65 * chn->ni;
	do_one_plot(pr);	
	pr->use.to = IS_CHAIN;
	pr->use.stuff = (void *)chn;
	return pr;
}
int do_iterate(void)
{
	chain *chn = NULL;
	pltreg *pr;
	O_p *op;
	
	if(updating_menu_state != 0)	return D_O_K;		
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return D_O_K;
	chn = find_chain_in_pltreg(NULL); 
	if (chn == NULL) return D_O_K;
	chn->istep = 0;
       	//plot_idle_action = ssdna_plot_idle_action;
	op->op_idle_action =ssdna_op_idle_action;
	chn->running = 1;
	return 0;
}
MENU *ssdna_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;


	add_item_to_menu(mn,"iterate",do_iterate,NULL,0,NULL);
	add_item_to_menu(mn,"new chain",singlest_main,NULL,0,NULL);
	add_item_to_menu(mn,"iter & record",do_iterate_and_record,NULL,0,NULL);
	add_item_to_menu(mn,"stop",do_stop,NULL,0,NULL);
	add_item_to_menu(mn,"force curve",do_force_curve,NULL,0,NULL);
	add_item_to_menu(mn,"set force",set_force,NULL,0,NULL);
	add_item_to_menu(mn,"set repeat",set_repeat,NULL,0,NULL);
	add_item_to_menu(mn,"set T",set_T,NULL,0,NULL);
	add_item_to_menu(mn,"check end",test_end,NULL,0,NULL);
	add_item_to_menu(mn,"find bounds",do_get_bounds,NULL,0,NULL);
	add_item_to_menu(mn,"Im site",info_on_im_site,NULL,0,NULL);
	add_item_to_menu(mn,"chaine site",info_on_site,NULL,0,NULL);
	add_item_to_menu(mn,"chaine info",info_on_chain,NULL,0,NULL);
	add_item_to_menu(mn,"write chaine",do_write_chain_to_file,NULL,0,NULL);
	add_item_to_menu(mn,"read chaine",do_read_chain,NULL,0,NULL);
	add_item_to_menu(mn,"debug on",set_debug_on_off,NULL,MENU_INDEX(1),NULL);
	add_item_to_menu(mn,"debug off",set_debug_on_off,NULL,MENU_INDEX(0),NULL);

	return mn;
}

int	ssdna_main(int argc, char **argv)
{
	add_plot_treat_menu_item ( "ssdna", NULL, ssdna_plot_menu(), 0, NULL);
	//	singlest_main(argc, argv);
	return D_O_K;
}

int	ssdna_unload(int argc, char **argv)
{
	remove_item_to_menu(plot_treat_menu, "ssdna", NULL, NULL);
	return D_O_K;
}
#endif

