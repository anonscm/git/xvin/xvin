#ifndef _SSDNA_H_
#define _SSDNA_H_


# define	END_OF_MOLECULE		0x80000000
# define	ADENINE			1
# define	THYMINE			4
# define	GUANINE			2
# define	CYTOSINE		3
# define	COMPLEMENT		5
# define	NOT_BOUND		-2
# define	N_TOP			16
# define	N_RIGHT			17
# define	N_BOTTOM		18
# define	N_LEFT			19
# define	NOT_POSSIBLE	20
# define	NEIGHBOUR		-3
# define	IS_CHAIN		-40320




int record = 0;
int display_debug = 0;
d_s *ds_data_recording = NULL; 
d_s *ds_data_recording2 = NULL; 
d_s *ds_data_recording3 = NULL; 
int nc = 0;
char *logfil = NULL;
extern char	file_pattern[];
typedef struct _nucleotide
{
	int 	x, y;		/* the position */
	int	type;		/* the nature of the base */	
	int	end;		/* wether or not the end */
	int	bound_to[3];	/* index of the nucleotide bound to */ 
	int nb;
	int *link;		/* a pointer to the linked element */
	int nlink;
} nucleotide;

typedef struct _chain
{
	int 	ni, ci, mi;	/* the number of element, current allocated */
	struct _nucleotide	*n;
	short int **im;
	int		istep, nstep;
	int		isubstep, nsubstep;
	float	T;
	float	force;
	float 	EAT;
	float	EGC;
	double	E;
	float	L;
	int		nat;
	int		ngc;
	int		flip;
	int		*linksize;
	char 	*file;
	char	*path;
  int running;
} chain;


int	free_chain(chain *chn);
int	alloc_im_of_chain(chain *chn);
int	translate_chain_to_im(chain *chn);
chain 	*read_mole_file(char *file);
chain 	*create_random_chain(int nx, int nstep, int nsubstep, float T, 
			float force, float EAT, float EGC);
int 	write_mole_to_file(char *file, chain *chn);
int	end_nucleotide_may_move(chain *chn, int which, int *dx1, int *dy1, 
			int *dx2, int *dy2);
int	nucleotide_may_move(chain *chn, int which, int *dx, int *dy);
int 	nucleotide_new_move_ok(chain *chn, int which, int dx, int dy);
int 	move_nucleotide(chain *chn, int which, int dx, int dy);
int	bound_nucleotide_to_neibourg(chain *chn, int nuc);
int	establish_bounds(chain *chn, int *ngc, int *nat, int debug);
int	type_of_nucleotide_on_spot(chain *chn, int start, int x, int y);
int	metropolis(void);
int	translate_chain_to_ds(chain *chn, d_s *ds, int fixorigin);
int	translate_chain_to_bound_ds(chain *chn, d_s *ds, int fixorigin);



pltreg	*create_chain_pltreg(chain *chn);
chain	*find_chain_in_pltreg(pltreg *pr) ;
/*
chain	*find_chain_in_acreg(acreg *ac);
int	on_yes_kill_chain_acreg(char ch);
int     kill_chain_acreg(char ch);
*/

PXV_FUNC(int, do_ssdna_rescale_plot, (void));
PXV_FUNC(MENU*, ssdna_plot_menu, (void));
PXV_FUNC(int, do_ssdna_rescale_data_set, (void));
PXV_FUNC(int, ssdna_main, (int argc, char **argv));

#endif

