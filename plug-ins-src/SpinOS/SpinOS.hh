#pragma once
#include "xvin.h"

PXV_FUNC(int, do_SpinOS_rescale_plot, (void));
PXV_FUNC(MENU*, SpinOS_plot_menu, (void));
PXV_FUNC(int, do_SpinOS_rescale_data_set, (void));
PXV_FUNC(int, SpinOS_main, (int argc, char **argv));
