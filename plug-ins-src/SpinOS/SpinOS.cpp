/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _SPINOS_C_
#define _SPINOS_C_

#include <allegro.h>
# include "xvin.h"

/* If you include other regular header do it here*/ 

#include <iostream>
#include <cmath>
#include <fstream>
#include <vector>
#include <complex>
#include <algorithm>

#include <Eigen/Dense>
#include <Eigen/Sparse>


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "SpinOS.h"

using namespace Eigen;

using namespace std;

#include <stdlib.h>
#include <stdio.h>
// #include <time.h>
// #include <assert.h>

typedef complex<double> complexg;
typedef complex<float> complexf;
typedef vector<double> vectorg;
const complexg iii(0,1);
inline double sq(double x) { return x*x; }
inline double min(double x, double y) { return (y < x) ? y : x; }
inline double scalar(complexg a, complexg b) { return real(a)*real(b)+imag(a)*imag(b); }
double myrand(void) { return (double) rand() / (double) RAND_MAX; }

typedef SparseMatrix<float> SparseMatrixXf;
typedef SparseMatrix<double> SparseMatrixXd;
typedef SparseMatrix<complexf> SparseMatrixXcf;
typedef SparseMatrix<complexg> SparseMatrixXcd;


// double get_runtime(void) { return ((double)clock())/((double)CLOCKS_PER_SEC); } 
double get_runtime(void) { return 0; }

class Lanczos { 
    double SQR( double a ) { return (a == 0.0) ? 0.0 : a*a; }
    double SIGN( double a, double b) { return ((b) >= 0.0 ? fabs(a) : -fabs(a)); }

    double pythag(double a, double b)
    {
       double absa,absb;
       absa=fabs(a);
       absb=fabs(b);
       if (absa > absb) return absa*sqrt(1.0+SQR(absb/absa));
       else return (absb == 0.0 ? 0.0 : absb*sqrt(1.0+SQR(absa/absb)));
    }


    void tqli(vectorg &d, vectorg &e, int n)
    {
        int m,l,iter,i;
	double s,r,p,g,f,dd,c,b;

	for (i=1;i<n;i++) e[i-1]=e[i];
	e[n-1]=0.0;
	for (l=0;l<n;l++) {
		iter=0;
		do {
			for (m=l;m<n-1;m++) {
				dd=fabs(d[m])+fabs(d[m+1]);
				if ((double)(fabs(e[m])+dd) == dd) break;
			}
			if (m != l) {
				g=(d[l+1]-d[l])/(2.0*e[l]);
				r=pythag(g,1.0);
				g=d[m]-d[l]+e[l]/(g+SIGN(r,g));
				s=c=1.0;
				p=0.0;
				for (i=m-1;i>=l;i--) {
					f=s*e[i];
					b=c*e[i];
					e[i+1]=(r=pythag(f,g));
					if (r == 0.0) {
						d[i+1] -= p;
						e[m]=0.0;
						break;
					}
					s=f/r;
					c=g/r;
					g=d[i+1]-p;
					r=(d[i]-g)*s+2.0*c*b;
					d[i+1]=g+(p=s*r);
					g=c*r-b;
 				}
				if (r == 0.0 && i >= l) continue;
				d[l] -= p;
				e[l]=g;
				e[m]=0.0;
			}
		} while (m != l);
	}
    }



    void eigen_tqli(VectorXd &d, VectorXd &e, int n)
    {
       int m,l,iter,i;
       double s,r,p,g,f,dd,c,b;

       for (i=1;i<n;i++) 
	  e(i-1) = e(i);
       e(n-1) = 0.0;
       for (l=0;l<n;l++) {
	 iter=0;
	 do {
	    for (m=l;m<n-1;m++) {
	       dd=fabs(d(m))+fabs(d(m+1));
	       if ((double)(fabs(e(m))+dd) == dd) break;
	    }
	    if (m != l) {
	       g = (d(l+1)-d(l))/(2.0*e(l));
	       r = pythag(g,1.0);
	       g = d(m)-d(l)+e(l)/(g+SIGN(r,g));
	       s = c =1.0;
	       p = 0.0;
	       for (i=m-1;i>=l;i--) {
		  f = s*e(i);
		  b = c*e(i);
		  e(i+1) = (r=pythag(f,g));
		  if (r == 0.0) {
		     d(i+1) -= p;
		     e(m)=0.0;
		     break;
		  }
		  s=f/r;
		  c=g/r;
		  g=d(i+1)-p;
		  r=(d(i)-g)*s+2.0*c*b;
		  d(i+1)=g+(p=s*r);
		  g=c*r-b;
	       }
	       if (r == 0.0 && i >= l) continue;
	       d(l) -= p;
	       e(l)=g;
	       e(m)=0.0;
	    }
	 } while (m != l);
       }
    }


    void eigen_load_rand_vector(VectorXd &v)
    {
       for (int i = 0; i < v.rows(); i++) { 
	  v(i) = myrand();
       }
       double norm = v.norm();
       v /= norm;
    }

    void eigen_load_rand_vector(VectorXcd &v)
    {
       for (int i = 0; i < v.rows(); i++) { 
	  v(i) = myrand() + iii * myrand();
       }
       double norm = v.norm();
       v /= norm;
    }

    complexg scalar_product(const VectorXcd &v1, const VectorXcd &v2) { 
       VectorXcd Z(1);
       Z =  v1.adjoint() * v2;
       return Z(0); 
    } 


    double scalar_product_real(const VectorXcd &v1, const VectorXcd &v2) { 
       double s = 0;
       for (int i=0; i < v1.size(); i++) { 
	  s += ( real(v1(i)) * real(v2(i)) + imag(v1(i)) * imag(v2(i)) );
       }
       return s; 
    } 

    double scalar_product(const VectorXd &v1, const VectorXd &v2) { 
       VectorXd Z(1);
       Z =  v1.transpose() * v2;
       return Z(0); 
    } 

public : 
  /*
   * Lanczos diagonalization function for symmetric matrixes ... 
   * Z   : pointer to a vector of lists containing the column number of all
   * non-zero elements for each line of the matrix.
   * Res : eigenvalue vector 
   * findration : minimal percentage of eigenvalues to be found 
   * returns: number of found eigenvales 
   */
    int eigenvalues(SparseMatrixXd &Z, vectorg &Res, double findratio, double epsilon) 
    {
       int n = Z.rows();


       VectorXd W(n); 
       eigen_load_rand_vector(W);
       VectorXd V = VectorXd::Zero(n);
       
       int phi = 4;
       int maxJ = phi * n;
       vectorg B (maxJ+1);
       B[0] = 1.0;
       vectorg A (maxJ+1);      
       vectorg d(maxJ+1);
       vectorg e(maxJ+1); 
       
       int j = 0;
       
       for (;;) {
	  while (j < maxJ) {
	     if (j) 
	        // W <- Q_{j+1} = r_j / \beta_j  and V <- \beta_j Q_{j}
	        for (int i = 0; i < n; i++) {
		   double t = W(i);
		   W(i) = V(i)/B[j];
		   V(i) = -B[j] * t;
		}
	     
	     // matrix vector multiplication : A Q_{j+1} 
	     // V <- A Q_{j+1} - \beta_j Q_{j}
	     V += Z * W;
	     
	     // \alpha_j = <Q_{j+1}, A Q_{j+1} - \beta_j Q_{j}> = <Q_{j+1}, A Q_{j+1}> 
	     A[j] = scalar_product(W,V);

	     // V <- (A - \alpha_j) Q_{j+1} - \beta_j Q_{j}
	     V -= A[j] * W;
	    	     
	     j++;

	     if (j >= B.size()) {
		 phi++;
		 A.resize(phi*n);
		 B.resize(phi*n);
	     }

	     if (j < B.size()) { 
	        B[j] = V.norm();
	     }
	  }

	  int finsize = j;
	  d.resize(finsize);
	  e.resize(finsize);
	  
	  for (int q=0; q<finsize; q++) { 
	     d[q] = A[q];
	     e[q] = B[q];
	  }
	  tqli (d , e, finsize);
	  
	  sort(d.begin(), d.begin()+finsize);

	  /*
	   * In the abscence of numerical errors the algorithm ends after one iteration  
	   * (see http://www.mat.uniroma1.it/~bertaccini/seminars/CS339/)
	   * However for large matrices the method is not stable, and has to be run several times. 
	   */ 

	  int found = 0;
	  double foundval;
	  for (int ic = 0; ic < finsize-1; ic++) {
	     if (!found && abs(d[ic]-d[ic+1]) < epsilon) { 
	        Res[found++] = foundval = d[ic];
	     } else if ( abs(d[ic]- foundval) > 10.0 * epsilon  && abs(d[ic]-d[ic+1]) < epsilon  ) {
	        if (found == n) cout << "Too many eigenvalues found !" << endl;
		Res[found++] = foundval = d[ic];
	     }
	  }

	  if ( (double) found / (double) n >= findratio || found == n ) { 
	     return found;
	  }
	  maxJ += 2 * n;

       }   
    }

    int eigenvalues(SparseMatrixXcd &Z, vectorg &Res, double findratio, double epsilon) 
    {
       int n = Z.rows();


       VectorXcd Vn(n); 
       eigen_load_rand_vector(Vn);
       VectorXcd Vp = VectorXcd::Zero(n);
       VectorXcd W(n); 
       
       int phi = 4;
       int maxJ = phi * n;
       vectorg B (maxJ+1);
       B[0] = 0.0;
       vectorg A (maxJ+1);      
       vectorg d(maxJ+1);
       vectorg e(maxJ+1); 
       
       int j = 0;
       
       for (;;) {
	  while (j < maxJ) {
	     W = Z * Vn;
	     A[j] = real( scalar_product(W, Vn) ); // the scalar product is real for self adjoint operators 	    	 	     
	     W -= A[j] * Vn;
	     W -= B[j] * Vp;

	     j++;

	     if (j >= B.size()) {
		 phi++;
		 A.resize(phi*n);
		 B.resize(phi*n);
	     }

	     B[j] = W.norm();
	     Vp = Vn;
	     Vn = W / B[j];
	  }

	  int finsize = j;
	  d.resize(finsize);
	  e.resize(finsize);
	  
	  for (int q=0; q<finsize; q++) { 
	     d[q] = A[q];
	     e[q] = B[q];
	  }
	  tqli (d , e, finsize);
	  
	  sort(d.begin(), d.begin()+finsize);

	  /*
	   * In the abscence of numerical errors the algorithm ends after one iteration  
	   * (see http://www.mat.uniroma1.it/~bertaccini/seminars/CS339/)
	   * However for large matrices the method is not stable, and has to be run several times. 
	   */ 

	  int found = 0;
	  double foundval;
	  for (int ic = 0; ic < finsize-1; ic++) {
	     if (!found && abs(d[ic]-d[ic+1]) < epsilon) { 
	        Res[found++] = foundval = d[ic];
	     } else if ( abs(d[ic]- foundval) > 10.0 * epsilon  && abs(d[ic]-d[ic+1]) < epsilon  ) {
	        if (found == n) cout << "Too many eigenvalues found !" << endl;
		Res[found++] = foundval = d[ic];
	     }
	  }

	  cerr << get_runtime() << " : lanczos.eigenvalues found " << found << " out of " << n << endl;

	  if ( (double) found / (double) n >= findratio || found == n ) { 
	     return found;
	  }
	  maxJ += 2 * n;

       }   
    }



};


typedef Triplet<double> TMatrixXd;
typedef Triplet<complexg> TMatrixXcd;

class TestLanczos { 

    SparseMatrixXcd Hsp;
    MatrixXcd Hfull;
    VectorXd Heval;
    Lanczos lanczos_diag;
    vectorg lanczos_eval;
public:
    void fill_matrix(int N) { 
       Hsp.resize(N, N);
       Hsp.setZero();
       Hfull = MatrixXcd::Zero(N, N);
       lanczos_eval.resize(N);

       int M = 5;
       std::vector< TMatrixXcd > coef; 
       coef.reserve(2 * M * N);
       

       for (int i = 0; i < N; i++) { 
	  for (int j = 0; j < M; j++) {
	     int k = ( rand() % N );

	     complexg entry = (myrand() - 0.5) + iii * (myrand() - 0.5);
	     Hfull(i, k) += entry;
	     Hfull(k, i) += conj(entry);

	     coef.push_back( TMatrixXcd(i, k, entry) );
	     coef.push_back( TMatrixXcd(k, i, conj(entry) ) );
	  }
       }

       Hsp.setFromTriplets(coef.begin(), coef.end());
    }


    void diag(void) { 
       cerr << get_runtime() << " find eigenvalues of dense matrix" << endl;
       SelfAdjointEigenSolver<MatrixXcd> eigensolver(Hfull);
       if (eigensolver.info() != Success) abort();
       Heval = eigensolver.eigenvalues();
       cerr << get_runtime() << " eigenvalues of dense matrix found" << endl;

       cerr << get_runtime() << " find eigenvalues of sparse matrix" << endl;
       int n = lanczos_diag.eigenvalues(Hsp, lanczos_eval, 0.999, 1e-10);
       cerr << get_runtime() << " eigenvalues of sparse matrix found" << endl;
       cerr << "Lanczos : " << n << " eigenvalues out of " << lanczos_eval.size() << endl;
       
       for (int i = 0; i < lanczos_eval.size(); i++) { 
	 cout << i << "   " << Heval(i) << "    " << lanczos_eval[i]-Heval[i] << endl;
       }
    }


};


class InverseArnoldi { 
    VectorXcd V;
    MatrixXcd Krylov;
    MatrixXcd sub_space_H;
    SparseMatrix<complexg, ColMajor> Hs;
    SparseLU<SparseMatrix<complexg, ColMajor>, COLAMDOrdering<int> > solver;
    VectorXd Heval;
    MatrixXcd Hevec;

    int k_iter;

    void precompute_inverse(void) { 
       solver.analyzePattern(Hs);
       solver.factorize(Hs);
    }

    void mult_by_inverse(const VectorXcd &vin, VectorXcd &vout) { 
       vout = solver.solve(vin);
    }

    complexg scalar_product(const VectorXcd &v1, const VectorXcd &v2) { 
       VectorXcd Z(1);
       Z =  v1.adjoint() * v2;
       return Z(0); 
    } 

    void init_kyrilov(void) { 
       Krylov.setZero();
       Krylov.col(0) = VectorXcd::Random( Krylov.rows() );
       double scale = Krylov.col(0).norm();
       Krylov.col(0) /= scale;
       k_iter = 0;
    }

    void inverse_arnoldi_iteration(void) { 
       mult_by_inverse(Krylov.col(k_iter), V);

       int N = Krylov.cols();
       int k_next = (k_iter + 1) % N;

       Krylov.col(k_next) = V;
       for (int q=0; q < N; q++) { 
	 if (q != k_next) { 
	    complexg alpha_q = scalar_product( Krylov.col(q), V );
	    Krylov.col(k_next) -= alpha_q * Krylov.col(q);
	 }
       }
       double scale = Krylov.col(k_next).norm();
       Krylov.col(k_next) /= scale;       
       k_iter = k_next;
    }

    void subspace_hamiltonian(void) {
       int N = Krylov.cols();
       for (int k=0; k < N; k++) { 
	  mult_by_inverse(Krylov.col(k), V);
	  for (int q=0; q <= k; q++) { 
	     sub_space_H(q, k) = scalar_product( Krylov.col(q), V );
	     sub_space_H(k, q) = conj(sub_space_H(q, k));
	  }
       }
    }
  
    void diag(void) {       
       SelfAdjointEigenSolver<MatrixXcd> eigensolver(sub_space_H);
       if (eigensolver.info() != Success) abort();
       Heval = eigensolver.eigenvalues();
       Hevec = eigensolver.eigenvectors();
    }

public : 
    void init(int N,  SparseMatrixXcd &m) { 
       Hs = m;
       Krylov.resize(m.cols(), N);
       sub_space_H.resize(N, N);
       V.resize(m.cols());
       precompute_inverse();
       init_kyrilov();
    }

    void diag(int n) { 
       for (int i = 0; i < n; i++) { 
	  inverse_arnoldi_iteration();
       }

       subspace_hamiltonian();
       diag();
    }

    double eval(int i) { 
       return 1.0/Heval(i);
    }
};




class TestInvArnoldi { 

    SparseMatrixXcd Hsp;
    MatrixXcd Hfull;
    VectorXd Heval;
    InverseArnoldi iarnoldi_diag;
public:
    void fill_matrix(int N) { 
       Hsp.resize(N, N);
       Hsp.setZero();
       Hfull = MatrixXcd::Zero(N, N);

       int M = 5;
       std::vector< TMatrixXcd > coef; 
       coef.reserve(2 * M * N);
       

       for (int i = 0; i < N; i++) { 
	  for (int j = 0; j < M; j++) {
	     int k = ( rand() % N );

	     complexg entry = (myrand() - 0.5) + iii * (myrand() - 0.5);
	     Hfull(i, k) += entry;
	     Hfull(k, i) += conj(entry);

	     coef.push_back( TMatrixXcd(i, k, entry) );
	     coef.push_back( TMatrixXcd(k, i, conj(entry) ) );
	  }
       }

       Hsp.setFromTriplets(coef.begin(), coef.end());
    }


    void diag(void) { 
       cerr << get_runtime() << " find eigenvalues of dense matrix" << endl;
       SelfAdjointEigenSolver<MatrixXcd> eigensolver(Hfull);
       if (eigensolver.info() != Success) abort();
       Heval = eigensolver.eigenvalues();
       cerr << get_runtime() << " eigenvalues of dense matrix found" << endl;

       cerr << get_runtime() << " find eigenvalues of sparse matrix" << endl;
       int N = Hsp.rows();
       iarnoldi_diag.init(N, Hsp);
       cerr << get_runtime() << " arnoldi diag " << endl;
       iarnoldi_diag.diag(N);

       cerr << get_runtime() << " eigenvalues of sparse matrix found" << endl;
       
       for (int i = 0; i < N; i++) { 
	 cout << i << "   " << Heval(i) << "    " << iarnoldi_diag.eval(i) << endl;
       }
       for (int i = N; i < Heval.size(); i++) {
	 cout << i << "   " << Heval(i) << endl;
       }
    }


};



class SNSsparse2 { 
public:
    double Delta; 
    double Ht; 
    double Phi;
    double W;
    double eigen_fraction;
    int NSupra;
private:


    int NY;
    int NX;
    int size; 
    int n_found;

  //    enum { UP, DN, UPCC, DNCC, NS };
    enum { UP, DNCC, NS };

    SparseMatrixXcd Hsp;
    SparseMatrixXcd idm;
    Lanczos lanczos_diag;
    vectorg lanczos_eval;

    // applies periodic boundary conditions 
    int enc_xys(int x, int y, int s) {
       return NS * (NY * (x % NX) + (y % NY)) + s;
    }

    int dec_x(int n) { 
       return n / (NS * NY);
    }

    int dec_y(int n) { 
       return (n / NS) % NY;
    }

    int dec_s(int n) { 
       return n % (NS);
    }

    complexg Delta_xy(int x, int y) { 
       if (x < NSupra/2) { return Delta; }
       else if (x >= NX - NSupra/2) { return Delta * exp(iii * 2.0 * M_PI * Phi); }
       else { return 0; } 
    }

public : 
  
    SNSsparse2(int NX_new, int NY_new) { 
       NX = NX_new;
       NY = NY_new;
       size = NX * NY * NS;

       Hsp.resize(size, size);
       idm.resize(size, size);
       Hsp.setZero();
       idm.setIdentity();
       lanczos_eval.resize(size);
       n_found = 0;
    }

    void fill_matrix(void) { 
       Hsp.setZero();

       std::vector< TMatrixXcd > coef; 
       coef.reserve(4 * NX * NY * NS);

       for (int x = 0; x < NX; x++) { 
	  for (int y = 0; y < NY; y++) { 
	     double Vxy = (myrand() - 0.5) * W;
	     for (int s = 0; s < NS; s++) { 
	        int n = enc_xys(x, y, s);
	        int n_1x = enc_xys(x+1, y, s);
	        int n_1y = enc_xys(x, y+1, s);

		double sign;
		if (s == UP) { sign = 1.0; }
		else { sign = -1.0; }

		complexg tx = sign * Ht;
		complexg ty = sign * Ht;

		coef.push_back( TMatrixXcd(n, n, sign * Vxy) );
		if (x+1<NX) { 
		   coef.push_back( TMatrixXcd(n, n_1x, tx) );
		   coef.push_back( TMatrixXcd(n_1x, n, conj(tx)) );		
		}
		if (y+1<NY) { 
		  coef.push_back( TMatrixXcd(n, n_1y, ty) );
		  coef.push_back( TMatrixXcd(n_1y, n, conj(ty)) );		
		}
	     }
	     
	     int n_up = enc_xys(x, y, UP);
	     int n_dncc = enc_xys(x, y, DNCC);
	     
	     complexg delta = Delta_xy(x, y);

	     if (norm(delta) != 0) {
	       coef.push_back( TMatrixXcd(n_up, n_dncc, delta) );
	       coef.push_back( TMatrixXcd(n_dncc, n_up, conj(delta)));
	     }
	  }
       }

       Hsp.setFromTriplets(coef.begin(), coef.end());
    }

    void diag(void) { 
       cerr << get_runtime() << " : launching lanczos_diag" << endl;
       n_found = lanczos_diag.eigenvalues(Hsp, lanczos_eval, eigen_fraction, 1e-10);
       cerr << get_runtime() << " : found " << n_found << " eigenvalues out of " << size << endl;
    }

    double eval(int i) { 
       return lanczos_eval[i];
    };


    void print_eval(void) { 
       for (int i = 0; i < n_found; i++) {
	 cout << Phi << "    " << lanczos_eval[i] << endl;
       }
       cout << endl;
    }

    void print_wavefunction(void) { 
       int count = 0;
       while (lanczos_eval[count] < 0) { 
	  count++;
       } 
       cout << "# eigenvalue : " << lanczos_eval[count] << endl;

       VectorXcd xxx(size); 
       VectorXcd yyy = VectorXcd::Random(size);

       /***
       SparseMatrixXcd Hdiff = Hsp - lanczos_eval[count] * idm;
       ConjugateGradient<SparseMatrixXcd> cg;
       cg.compute(Hdiff);
       for (int k=0; k<10; k++) { 
	 x = cg.solve(y);
	 cerr << cg.iterations() << endl;
	 cerr << cg.error() << endl;

	 double n = x.norm();
	 x /= n;
	 y = x;
       } 
       ***/

       SparseMatrix<complexg, ColMajor> Hdiff = Hsp - lanczos_eval[count] * idm;
       SparseLU<SparseMatrix<complexg, ColMajor>, COLAMDOrdering<int> > solver;
       solver.analyzePattern(Hdiff); 
       solver.factorize(Hdiff); 

       for (int k=0; k<10; k++) { 
	 xxx = solver.solve(yyy);
	 double n = xxx.norm();
	 xxx /= n;
	 yyy = xxx;
       } 

       yyy = Hdiff * xxx;
       cerr << "error : " << yyy.norm() << "    " << xxx.norm() << endl;

       for (int x=0; x < NX; x++) { 
	  for (int y=0; y < NY; y++) { 
	     cout << x << "    " << y << "    " << norm(xxx(enc_xys(x, y, UP))) << "    " << norm(xxx(enc_xys(x, y, DNCC))) << endl;
	  }
	  cout << endl;
       }

       exit(0);
    }


    void print_info(void) { 
       cout << "# NX " << NX << endl;
       cout << "# NY " << NY << endl;
       cout << "# NS " << NS << endl;
       cout << "# NSupra " << NSupra << endl;
       cout << "# Delta " << Delta << endl;
       cout << "# Ht " << Ht << endl;
       cout << "# Phi " << Phi << endl;
       cout << "# W " << W << endl;
       cout << "# eigen_fraction " << eigen_fraction << endl;
    }
};





class SNSsparseSO { 
public:
    double Delta; 
    double Ht; 
    double Phi;
    double W;
    double eigen_fraction;
    double lambda;
    double BzN;
    double BzS;
    int NSupra;
    bool phase_in_delta;
private:

    int NY;
    int NX;
    int size; 
    int n_found;

    enum { UP, DN, UPCC, DNCC, NS };
  //    enum { UP, DNCC, NS };

    SparseMatrixXcd Hsp;
    SparseMatrixXcd idm;
    Lanczos lanczos_diag;
    vectorg lanczos_eval;

    // applies periodic boundary conditions 
    int enc_xys(int x, int y, int s) {
       return NS * (NY * (x % NX) + (y % NY)) + s;
    }

    int dec_x(int n) { 
       return n / (NS * NY);
    }

    int dec_y(int n) { 
       return (n / NS) % NY;
    }

    int dec_s(int n) { 
       return n % (NS);
    }

    bool site_is_normal(int x) { 
       return (x >= NSupra/2) && (x < (NX - NSupra/2));
    }

    int normal_length(void) { 
       return NX - 2 * (NSupra/2);
    }

    complexg Delta_xy(int x, int y) { 
       if (x < NSupra/2) { return Delta; }
       else if (x >= NX - NSupra/2) { 
	  if (phase_in_delta) { 
	     return Delta * exp(iii * 2.0 * M_PI * Phi); 
	  } else { 
  	     return Delta; 
	  }
       } else { return 0; } 
    }

public : 
  
    SNSsparseSO(int NX_new, int NY_new) { 
       NX = NX_new;
       NY = NY_new;
       size = NX * NY * NS;

       Hsp.resize(size, size);
       idm.resize(size, size);
       Hsp.setZero();
       idm.setIdentity();
       lanczos_eval.resize(size);
       n_found = 0;
       phase_in_delta = false;
    }

    void fill_matrix(void) { 
       Hsp.setZero();

       std::vector< TMatrixXcd > coef; 
       coef.reserve(4 * NX * NY * NS);

       for (int x = 0; x < NX; x++) { 
	  for (int y = 0; y < NY; y++) { 
	     double Vxy = (myrand() - 0.5) * W;
	     for (int s = 0; s < NS; s++) { 
	        int n = enc_xys(x, y, s);
	        int n_1x = enc_xys(x+1, y, s);
	        int n_1y = enc_xys(x, y+1, s);

		double sign;
		if (s == UP || s == DN) { sign = 1.0; }
		else { sign = -1.0; }

		complexg tx = sign * Ht;
		if (!phase_in_delta) { 
		   double NN = normal_length();
		   if (site_is_normal(x)) { 
		       if (s == UP || s == DN) tx *= exp(iii * M_PI * Phi / NN);
		       else tx *= exp(-iii * M_PI * Phi / NN);
		   }
		}
		complexg ty = sign * Ht;

		double sigma_z;
		if (s == UP || s == UPCC) { sigma_z = 1.0; } 
		else { sigma_z = -1.0; }

		double Bz = (site_is_normal(x)) ? BzN : BzS;

		coef.push_back( TMatrixXcd(n, n, sign * (Vxy + sigma_z * Bz) ) );
		if (x+1<NX) { 
		   coef.push_back( TMatrixXcd(n, n_1x, tx) );
		   coef.push_back( TMatrixXcd(n_1x, n, conj(tx)) );		
		}
		if (y+1<NY) { 
		  coef.push_back( TMatrixXcd(n, n_1y, ty) );
		  coef.push_back( TMatrixXcd(n_1y, n, conj(ty)) );		
		}
	     }
	     
	     
	     complexg delta = Delta_xy(x, y);

	     if (norm(delta) != 0) { 
	        int n_up = enc_xys(x, y, UP);
		int n_dncc = enc_xys(x, y, DNCC);
		
		int n_dn = enc_xys(x, y, DN);
		int n_upcc = enc_xys(x, y, UPCC);
	        coef.push_back( TMatrixXcd(n_up, n_dncc, delta) );
		coef.push_back( TMatrixXcd(n_dncc, n_up, conj(delta)));

		coef.push_back( TMatrixXcd(n_dn, n_upcc, delta) );
		coef.push_back( TMatrixXcd(n_upcc, n_dn, conj(delta)));
	     }

	     
	     if (lambda != 0) { 
	        int up0 = enc_xys(x, y, UP);
	        int dn0 = enc_xys(x, y, DN);
	        int upcc0 = enc_xys(x, y, UPCC);
	        int dncc0 = enc_xys(x, y, DNCC);

	        int up_yp1 = enc_xys(x, y+1, UP);
	        int up_ym1 = enc_xys(x, y-1, UP);
	        int dn_yp1 = enc_xys(x, y+1, DN);
	        int dn_ym1 = enc_xys(x, y-1, DN);

	        int upcc_yp1 = enc_xys(x, y+1, UPCC);
	        int upcc_ym1 = enc_xys(x, y-1, UPCC);
	        int dncc_yp1 = enc_xys(x, y+1, DNCC);
	        int dncc_ym1 = enc_xys(x, y-1, DNCC);

	        int up_xp1 = enc_xys(x+1, y, UP);
	        int up_xm1 = enc_xys(x-1, y, UP);
	        int dn_xp1 = enc_xys(x+1, y, DN);
	        int dn_xm1 = enc_xys(x-1, y, DN);

	        int upcc_xp1 = enc_xys(x+1, y, UPCC);
	        int upcc_xm1 = enc_xys(x-1, y, UPCC);
	        int dncc_xp1 = enc_xys(x+1, y, DNCC);
	        int dncc_xm1 = enc_xys(x-1, y, DNCC);

		if (y-1 >= 0) { 
		  coef.push_back( TMatrixXcd(up0, dn_ym1, iii * lambda) );
		  coef.push_back( TMatrixXcd(dn0, up_ym1, iii * lambda) );
		  coef.push_back( TMatrixXcd(upcc0, dncc_ym1, iii * lambda) );
		  coef.push_back( TMatrixXcd(dncc0, upcc_ym1, iii * lambda) );
		}

		if (y+1 < NY) { 
		  coef.push_back( TMatrixXcd(up0, dn_yp1, -iii * lambda) );
		  coef.push_back( TMatrixXcd(dn0, up_yp1, -iii * lambda) );
		  coef.push_back( TMatrixXcd(upcc0, dncc_yp1, -iii * lambda) );
		  coef.push_back( TMatrixXcd(dncc0, upcc_yp1, -iii * lambda) );
		}

		complexg cphase = 1.0;
		if (phase_in_delta == false) { 
		   double NN = normal_length();
		   cphase = exp(iii * M_PI * Phi / NN);		      
		}
		
		if (x-1 >= 0) { 
		   complexg cphase_xm1 = 1.0;
		   if (site_is_normal(x-1)) cphase_xm1 = cphase;
		   
		   coef.push_back( TMatrixXcd(up0, dn_xm1, lambda * conj(cphase_xm1)) );
		   coef.push_back( TMatrixXcd(dn0, up_xm1, -lambda * conj(cphase_xm1)) );
		   coef.push_back( TMatrixXcd(upcc0, dncc_xm1, -lambda * cphase_xm1) );
		   coef.push_back( TMatrixXcd(dncc0, upcc_xm1, lambda * cphase_xm1) );
		}

		if (x+1 < NX) { 
		   complexg cphase_xp1 = 1.0;
		   if (site_is_normal(x)) cphase_xp1 = cphase;

		   coef.push_back( TMatrixXcd(up0, dn_xp1, -lambda * cphase_xp1) );
		   coef.push_back( TMatrixXcd(dn0, up_xp1, lambda * cphase_xp1) );
		   coef.push_back( TMatrixXcd(upcc0, dncc_xp1, lambda * conj(cphase_xp1)) );
		   coef.push_back( TMatrixXcd(dncc0, upcc_xp1, -lambda * conj(cphase_xp1)) );
		}
	     }
	  }
       }

       Hsp.setFromTriplets(coef.begin(), coef.end());
    }

    void diag(void) { 
       cerr << get_runtime() << " : launching lanczos_diag" << endl;
       n_found = lanczos_diag.eigenvalues(Hsp, lanczos_eval, eigen_fraction, 1e-10);
       cerr << get_runtime() << " : found " << n_found << " eigenvalues out of " << size << endl;
    }

    double eval(int i) { 
       return lanczos_eval[i];
    };


    void print_eval(void) { 
       for (int i = 0; i < n_found; i++) {
	 cout << Phi << "    " << lanczos_eval[i] << endl;
       }
       cout << endl;
    }

    void print_wavefunction(void) { 
       int count = 0;
       while (lanczos_eval[count] < 0) { 
	  count++;
       } 
       cout << "# eigenvalue : " << lanczos_eval[count] << endl;

       VectorXcd xxx(size); 
       VectorXcd yyy = VectorXcd::Random(size);

       /***
       SparseMatrixXcd Hdiff = Hsp - lanczos_eval[count] * idm;
       ConjugateGradient<SparseMatrixXcd> cg;
       cg.compute(Hdiff);
       for (int k=0; k<10; k++) { 
	 x = cg.solve(y);
	 cerr << cg.iterations() << endl;
	 cerr << cg.error() << endl;

	 double n = x.norm();
	 x /= n;
	 y = x;
       } 
       ***/

       SparseMatrix<complexg, ColMajor> Hdiff = Hsp - lanczos_eval[count] * idm;
       SparseLU<SparseMatrix<complexg, ColMajor>, COLAMDOrdering<int> > solver;
       solver.analyzePattern(Hdiff); 
       solver.factorize(Hdiff); 

       for (int k=0; k<10; k++) { 
	 xxx = solver.solve(yyy);
	 double n = xxx.norm();
	 xxx /= n;
	 yyy = xxx;
       } 

       yyy = Hdiff * xxx;
       cerr << "error : " << yyy.norm() << "    " << xxx.norm() << endl;

       for (int x=0; x < NX; x++) { 
	  for (int y=0; y < NY; y++) { 
	     cout << x << "    " << y << "    " << norm(xxx(enc_xys(x, y, UP))) << "    " << norm(xxx(enc_xys(x, y, DNCC))) << "    " << norm(xxx(enc_xys(x, y, DN))) << "    " << norm(xxx(enc_xys(x, y, UPCC))) << endl;
	  }
	  cout << endl;
       }

       exit(0);
    }

    double self_adjoint_check(void) { 
       double err = 0;

       for (int i = 0; i < size; i++) { 
	  for (int j = 0; j < size; j++) { 
	    complexg a = Hsp.coeffRef(i, j);
	    complexg b = Hsp.coeffRef(j, i);
	    err += norm(a - conj(b));
	  }
       }
       return err;
    }

    void print_info(void) { 
       cout << "# NX " << NX << endl;
       cout << "# NY " << NY << endl;
       cout << "# NS " << NS << endl;
       cout << "# NSupra " << NSupra << endl;
       cout << "# Delta " << Delta << endl;
       cout << "# Ht " << Ht << endl;
       cout << "# lambda " << lambda << endl;
       //cout << "# Bz " << Bz << endl;
       cout << "# Phi " << Phi << endl;
       cout << "# W " << W << endl;
       cout << "# eigen_fraction " << eigen_fraction << endl;
       cout << "# phase_in_delta " << phase_in_delta << endl;
    
    }
};







class NsparseSO { 
public:
    double Ht; 
    double Phi;
    double W;
    double eigen_fraction;
    double lambda;
    double Bz;
private:

    int NY;
    int NX;
    int size; 
    int n_found;

    enum { UP, DN, NS };
  //    enum { UP, DNCC, NS };

    SparseMatrixXcd Hsp;
    SparseMatrixXcd idm;
    Lanczos lanczos_diag;
    vectorg lanczos_eval;

    // applies periodic boundary conditions 
    int enc_xys(int x, int y, int s) {
       return NS * (NY * ((x+NX) % NX) + ((y+NY) % NY)) + s;
    }

    int dec_x(int n) { 
       return n / (NS * NY);
    }

    int dec_y(int n) { 
       return (n / NS) % NY;
    }

    int dec_s(int n) { 
       return n % (NS);
    }


public : 
  
    NsparseSO(int NX_new, int NY_new) { 
       NX = NX_new;
       NY = NY_new;
       size = NX * NY * NS;

       Hsp.resize(size, size);
       idm.resize(size, size);
       Hsp.setZero();
       idm.setIdentity();
       lanczos_eval.resize(size);
       n_found = 0;
    }

    void fill_matrix(void) { 
       Hsp.setZero();

       std::vector< TMatrixXcd > coef; 
       coef.reserve(4 * NX * NY * NS);

       for (int x = 0; x < NX; x++) { 
	  for (int y = 0; y < NY; y++) { 
	     double Vxy = (myrand() - 0.5) * W;
	     for (int s = 0; s < NS; s++) { 
	        int n = enc_xys(x, y, s);
	        int n_1x = enc_xys(x+1, y, s);
	        int n_1y = enc_xys(x, y+1, s);

		complexg tx = Ht;
		tx *= exp(iii * M_PI * Phi / (double) NX);
		complexg ty = Ht;

		double sigma_z;
		if (s == UP) { sigma_z = 1.0; } 
		else { sigma_z = -1.0; }

		coef.push_back( TMatrixXcd(n, n, (Vxy + sigma_z * Bz) ) );
		coef.push_back( TMatrixXcd(n, n_1x, tx) );
		coef.push_back( TMatrixXcd(n_1x, n, conj(tx)) );		
		if (y+1<NY) { 
		  coef.push_back( TMatrixXcd(n, n_1y, ty) );
		  coef.push_back( TMatrixXcd(n_1y, n, conj(ty)) );		
		}
	     }
	     
	     
	     if (lambda != 0) { 
	        int up0 = enc_xys(x, y, UP);
	        int dn0 = enc_xys(x, y, DN);

	        int up_yp1 = enc_xys(x, y+1, UP);
	        int up_ym1 = enc_xys(x, y-1, UP);
	        int dn_yp1 = enc_xys(x, y+1, DN);
	        int dn_ym1 = enc_xys(x, y-1, DN);

	        int up_xp1 = enc_xys(x+1, y, UP);
	        int up_xm1 = enc_xys(x-1, y, UP);
	        int dn_xp1 = enc_xys(x+1, y, DN);
	        int dn_xm1 = enc_xys(x-1, y, DN);

		if (y-1 >= 0) { 
		  coef.push_back( TMatrixXcd(up0, dn_ym1, iii * lambda) );
		  coef.push_back( TMatrixXcd(dn0, up_ym1, iii * lambda) );
		}

		if (y+1 < NY) { 
		  coef.push_back( TMatrixXcd(up0, dn_yp1, -iii * lambda) );
		  coef.push_back( TMatrixXcd(dn0, up_yp1, -iii * lambda) );
		}

		complexg cphase = exp(iii * M_PI * Phi / (double) NX);
		coef.push_back( TMatrixXcd(up0, dn_xm1, lambda * conj(cphase)) );
		coef.push_back( TMatrixXcd(dn_xm1, up0, lambda * cphase) );

		coef.push_back( TMatrixXcd(dn0, up_xm1, -lambda * conj(cphase)) );
		coef.push_back( TMatrixXcd(up_xm1, dn0, -lambda * cphase) );

	     }
	  }
       }

       Hsp.setFromTriplets(coef.begin(), coef.end());
    }

    void diag(void) { 
       cerr << get_runtime() << " : launching lanczos_diag" << endl;
       n_found = lanczos_diag.eigenvalues(Hsp, lanczos_eval, eigen_fraction, 1e-10);
       cerr << get_runtime() << " : found " << n_found << " eigenvalues out of " << size << endl;
    }

    double eval(int i) { 
       return lanczos_eval[i];
    };


    void print_eval(void) { 
       for (int i = 0; i < n_found; i++) {
	 cout << Phi << "    " << lanczos_eval[i] << endl;
       }
       cout << endl;
    }

    void print_wavefunction(void) { 
       int count = 0;
       while (lanczos_eval[count] < 0) { 
	  count++;
       } 
       cout << "# eigenvalue : " << lanczos_eval[count] << endl;

       VectorXcd xxx(size); 
       VectorXcd yyy = VectorXcd::Random(size);

       /***
       SparseMatrixXcd Hdiff = Hsp - lanczos_eval[count] * idm;
       ConjugateGradient<SparseMatrixXcd> cg;
       cg.compute(Hdiff);
       for (int k=0; k<10; k++) { 
	 x = cg.solve(y);
	 cerr << cg.iterations() << endl;
	 cerr << cg.error() << endl;

	 double n = x.norm();
	 x /= n;
	 y = x;
       } 
       ***/

       SparseMatrix<complexg, ColMajor> Hdiff = Hsp - lanczos_eval[count] * idm;
       SparseLU<SparseMatrix<complexg, ColMajor>, COLAMDOrdering<int> > solver;
       solver.analyzePattern(Hdiff); 
       solver.factorize(Hdiff); 

       for (int k=0; k<10; k++) { 
	 xxx = solver.solve(yyy);
	 double n = xxx.norm();
	 xxx /= n;
	 yyy = xxx;
       } 

       yyy = Hdiff * xxx;
       cerr << "error : " << yyy.norm() << "    " << xxx.norm() << endl;

       for (int x=0; x < NX; x++) { 
	  for (int y=0; y < NY; y++) { 
	     cout << x << "    " << y << "    " << norm(xxx(enc_xys(x, y, UP))) << "    " << norm(xxx(enc_xys(x, y, DN))) << "    " << endl;
	  }
	  cout << endl;
       }

       exit(0);
    }

    double self_adjoint_check(void) { 
       double err = 0;

       for (int i = 0; i < size; i++) { 
	  for (int j = 0; j < size; j++) { 
	    complexg a = Hsp.coeffRef(i, j);
	    complexg b = Hsp.coeffRef(j, i);
	    err += norm(a - conj(b));
	  }
       }
       return err;
    }

    void print_info(void) { 
       cout << "# NX " << NX << endl;
       cout << "# NY " << NY << endl;
       cout << "# NS " << NS << endl;
       cout << "# Ht " << Ht << endl;
       cout << "# lambda " << lambda << endl;
       cout << "# Bz " << Bz << endl;
       cout << "# Phi " << Phi << endl;
       cout << "# W " << W << endl;
       cout << "# eigen_fraction " << eigen_fraction << endl;
    }
};

/**
int  main(int argc, char **argv)
{
    TestInvArnoldi hello;
    hello.fill_matrix(30);
    hello.diag();

}
**/

int  main(int argc, char **argv)
{
  SNSsparseSO Hr(80, 20);
  Hr.NSupra = 30;
  Hr.Ht = 4.0;
  Hr.Delta = 1.0;
  Hr.W = 4.0;
  Hr.eigen_fraction = 0.9996;
  Hr.lambda = 0.0;
  Hr.BzS = 1e-6;
  Hr.BzN = 0.1;
  Hr.phase_in_delta = false;
  Hr.print_info();
  if (argc < 2) 
    {
      fprintf(stderr,"Usage %s phi_value\n",argv[0]);
      exit(0);
    }
  double Phi0 = atof(argv[1]);
  cerr << Phi0 << endl;
  double Phi = Phi0;
  for (double Phi = Phi0+1e-4; Phi < 1.01; Phi += 0.04) { 
    Hr.Phi = Phi;
    srand(1);
    Hr.fill_matrix();
    Hr.diag();
    Hr.print_eval();
    //    Hr.print_wavefunction();
  }
}


int do_SpinOS_hello(void)
{
  int i;

  if(updating_menu_state != 0)	return D_O_K;

  i = win_printf("Hello from SpinOS");
  if (i == CANCEL) win_printf_OK("you have press CANCEL");
  else win_printf_OK("you have press OK");
  return 0;
}

int do_SpinOS_rescale_data_set(void)
{
  int i, j;
  O_p *op = NULL;
  int nf;
  static float factor = 1;
  d_s *ds, *dsi;
  pltreg *pr = NULL;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  nf = dsi->nx;	/* this is the number of points in the data set */
  i = win_scanf("By what factor do you want to multiply y %f",&factor);
  if (i == CANCEL)	return OFF;

  if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");

  for (j = 0; j < nf; j++)
  {
    ds->yd[j] = factor * dsi->yd[j];
    ds->xd[j] = dsi->xd[j];
  }

  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"y multiplied by %f",factor);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}

int do_SpinOS_rescale_plot(void)
{
  int i, j;
  O_p *op = NULL, *opn = NULL;
  int nf;
  static float factor = 1;
  d_s *ds, *dsi;
  pltreg *pr = NULL;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number and place it in a new plot");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  nf = dsi->nx;	/* this is the number of points in the data set */
  i = win_scanf("By what factor do you want to multiply y %f",&factor);
  if (i == CANCEL)	return OFF;

  if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  ds = opn->dat[0];

  for (j = 0; j < nf; j++)
  {
    ds->yd[j] = factor * dsi->yd[j];
    ds->xd[j] = dsi->xd[j];
  }

  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"y multiplied by %f",factor);
  set_plot_title(opn, "Multiply by %f",factor);
  if (op->x_title != NULL) set_plot_x_title(opn, op->x_title);
  if (op->y_title != NULL) set_plot_y_title(opn, op->y_title);
  opn->filename = Transfer_filename(op->filename);
  uns_op_2_op(opn, op);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}

MENU *SpinOS_plot_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"Hello example", do_SpinOS_hello,NULL,0,NULL);
  add_item_to_menu(mn,"data set rescale in Y", do_SpinOS_rescale_data_set,NULL,0,NULL);
  add_item_to_menu(mn,"plot rescale in Y", do_SpinOS_rescale_plot,NULL,0,NULL);
  return mn;
}

int	SpinOS_main(int argc, char **argv)
{
  add_plot_treat_menu_item ( "SpinOS", NULL, SpinOS_plot_menu(), 0, NULL);
  return D_O_K;
}

int	SpinOS_unload(int argc, char **argv)
{
  remove_item_to_menu(plot_treat_menu, "SpinOS", NULL, NULL);
  return D_O_K;
}
#endif

