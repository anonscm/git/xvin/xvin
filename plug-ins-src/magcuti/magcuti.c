/*
*    Plug-in program for image treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _MAGCUTI_C_
#define _MAGCUTI_C_

# include "allegro.h"
#ifdef XV_WIN32
#include "winalleg.h"
#endif
# include "xvin.h"

/* If you include other regular header do it here*/


/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled

# include "../xvuEye/xvuEye.h"
#ifdef XV_WIN32
# include "../thorlabs_APT/APTAPI.h"
# include "../thorlabs_APT/thorlabs_APT.h"
#elif defined XV_UNIX
# include "../thorlabs_apt_api/thorlabs_apt_api.h"
#define POS_ENC_CONV_FACTOR 34304
#define SPEED_ENC_CONV_FACTOR 767367.490218667
#define ACC_ENS_CONV_FACTOR 261.93

tl_apt_context *g_context = NULL;
 float small_step = 0.05;

int do_thorlabs_APT_start(void)
{
    if(updating_menu_state != 0)
      {
	if (g_context == NULL) active_menu->flags &= ~D_DISABLED;
	else active_menu->flags |=  D_DISABLED;
	return D_O_K;
      }    

    if (g_context)
        return win_printf_OK("please close your device before init again");
    g_context = tl_apt_init(0x0403, 0xfaf0, NULL, NULL, NULL);
    if (g_context == NULL)
    {
        g_context = tl_apt_init(0x1569, 0x2001, NULL, NULL, NULL);
    }
    if (g_context == NULL)
      win_printf_OK("Did not int device !");
    do_update_menu();    
    return 0;
}


int do_thorlabs_APT_stop(void)
{

    if(updating_menu_state != 0)
      {
	if (g_context == NULL) active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }
    
    tl_apt_close(g_context);
    g_context = NULL;
    return 0;
}

int do_cleanup(void)
{
    if(updating_menu_state != 0)
      {
	if (g_context == NULL) active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }

    if (g_context == NULL)
    {
        return win_printf_OK("please init before use");
    }
    tl_apt_cleanup(g_context);
    return 0;
}

int do_identify(void)
{
    int ret = 0;

    if(updating_menu_state != 0)
      {
	if (g_context == NULL) active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }
    if (g_context == NULL)
        return win_printf_OK("please init before use");
    ret = tl_apt_mod_identify(g_context);
    if (ret != 0)        win_printf("can't load identify, err: %d", ret);
    return 0;

}
int do_thorlabs_APT_read_MOT_pos(void)
{
    int absolute_pos = 0;
    float pos = 0;

    if(updating_menu_state != 0)
      {
	if (g_context == NULL) active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }

    if (g_context == NULL)
      {        return win_printf_OK("please init before use");    }

    if (tl_apt_mot_get_poscounter(g_context, &absolute_pos) != 0)
      warning_message("unable to get\n");

    pos = (float)absolute_pos/POS_ENC_CONV_FACTOR;
    win_printf("absolute_pos: %5f (mm)", pos);
    return 0;
}

int apt_api_GetMotPosition(float *pfPosition)
{
    int absolute_pos = 0;
    float pos = 0;

    if (g_context == NULL) return 1;

    //if (tl_apt_mot_get_move_abspos(g_context, &absolute_pos) != 0)
    if (tl_apt_mot_get_poscounter(g_context, &absolute_pos) != 0)
      return 2;

    pos = (float)absolute_pos/POS_ENC_CONV_FACTOR;
    if (pfPosition) *pfPosition = pos;
    return 0;
}


float grab_apt_z_value(void)
{
#ifdef XV_WIN32  
  int j;
  j = MOT_GetPosition(plSerialNum[0], pfPosition);
  return (j == 0) ? pfPosition[0] : 0;
# elif defined XV_UNIX
  float pos = 0;
  apt_api_GetMotPosition(&pos);
  return pos;
# endif  
}

int do_thorlabs_APT_set_MOT_pos(void)
{
    int absolute_pos = 0, local_pos = 0;
    int ret = 0;
    float pos = 0, lpos = 0;
    char message[256] = {0};

    if(updating_menu_state != 0)
      {
	if (g_context == NULL) active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }

    if (g_context == NULL)
      {        win_printf_OK("please init before use");    }

    if (tl_apt_mot_get_poscounter(g_context, &local_pos) != 0)      
        warning_message("unable to get\n");
    
    if (tl_apt_mot_get_move_abspos(g_context, &absolute_pos) != 0)
        warning_message("unable to get\n");

    pos = (float)absolute_pos/POS_ENC_CONV_FACTOR;
    lpos = (float)local_pos/POS_ENC_CONV_FACTOR;
    snprintf(message,sizeof(message),"Motor position %g mm/s\n"
	    "new target position: %%5f (mm)",lpos);
    ret = win_scanf(message, &pos);
    if (ret == WIN_CANCEL)         return D_O_K;

    absolute_pos = (int)(0.5 + pos*POS_ENC_CONV_FACTOR);
    if (tl_apt_mot_move_absolute_with_pos(g_context, absolute_pos) != 0)
    {
        warning_message("unable to set \n");
    }
    return D_O_K;
}

int apt_api_SetMotPosition(float pfPosition)
{
    int absolute_pos = 0;

    if (g_context == NULL) return 1;

    absolute_pos = (int)(0.5 + pfPosition*POS_ENC_CONV_FACTOR);
    if (tl_apt_mot_move_absolute_with_pos(g_context, absolute_pos) != 0)
      return 2;
    return 0;
}


int do_set_APT_velocity_mm_s(void)
{
    int min_velocity = 0;
    int max_velocity = 0;
    int acceleration = 0;
    int ret = 0;
    float v_min = 0, v_max = 0, acc = 0;

    if(updating_menu_state != 0)
      {
	if (g_context == NULL) active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }    

    if (g_context == NULL)
    {
        win_printf("please init before use");
        return D_O_K;
    }

    if (tl_apt_mot_get_velocity(g_context, &min_velocity, &acceleration, &max_velocity) != 0)
        warning_message("unable to get state\n");

    v_min = (float)min_velocity/SPEED_ENC_CONV_FACTOR;
    v_max = (float)max_velocity/SPEED_ENC_CONV_FACTOR;
    acc = (float)acceleration/ACC_ENS_CONV_FACTOR;
    ret = win_scanf(" min velocity: %5f (mm/s)\n acceleration: %5f\n max velocity %5f (mm/s)\n"
		    , &v_min, &acc,&v_max);

    if (ret == WIN_CANCEL)        return D_O_K;

    min_velocity = (int)(0.5+(v_min*SPEED_ENC_CONV_FACTOR));
    max_velocity = (int)(0.5+(v_max*SPEED_ENC_CONV_FACTOR));
    acceleration = (int)(0.5+(acc*ACC_ENS_CONV_FACTOR));        

    if (tl_apt_mot_set_velocity(g_context, min_velocity, acceleration, max_velocity) != 0)
    {
        warning_message("unable to set velocity\n");
    }

    return D_O_K;
}

int do_apt_api_getVvelocity_mm_s(float *v_min, float *acc, float *v_max)
{
    int min_velocity = 0;
    int max_velocity = 0;
    int acceleration = 0;
    
    if (g_context == NULL) {      return 1;}    
    if (tl_apt_mot_get_velocity(g_context, &min_velocity, &acceleration, &max_velocity) != 0)
      return 1;

    if (v_min) *v_min = (float)min_velocity/SPEED_ENC_CONV_FACTOR;
    if(v_max) *v_max = (float)max_velocity/SPEED_ENC_CONV_FACTOR;
    if(acc) *acc = (float)acceleration/ACC_ENS_CONV_FACTOR;
    return 0;
}


int do_apt_api_setVvelocity_mm_s(float v_min, float acc, float v_max)
{
    int min_velocity = 0;
    int max_velocity = 0;
    int acceleration = 0;

    if (g_context == NULL) {      return 1;}
    min_velocity = (int)(0.5+(v_min*SPEED_ENC_CONV_FACTOR));
    max_velocity = (int)(0.5+(v_max*SPEED_ENC_CONV_FACTOR));
    acceleration = (int)(0.5+(acc*ACC_ENS_CONV_FACTOR));        

    if (tl_apt_mot_set_velocity(g_context, min_velocity, acceleration, max_velocity) != 0)
    {       return 2;    }
    return D_O_K;
}



int do_thorlabs_APT_small_inc_MOT_pos(void)
{
    int absolute_pos = 0;
    float pfMinVel = 0, pfAccn = 8000, pfMaxVel = 1;
    
    if(updating_menu_state != 0)
      {
	if (g_context == NULL) active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }    

    if (g_context == NULL)
      {         win_printf("please init before use"); return 0;    }

    if (tl_apt_mot_get_move_abspos(g_context, &absolute_pos) != 0)
        warning_message("unable to get\n");

    if (do_apt_api_getVvelocity_mm_s(&pfMinVel, &pfAccn, &pfMaxVel))
      warning_message("Error reading velocity params from apt api");
    
    if (do_apt_api_setVvelocity_mm_s(pfMinVel, pfAccn, 2.5))
      warning_message("Error setting velocity params apt api\n");
    
    absolute_pos += (int)(0.5 + (small_step*POS_ENC_CONV_FACTOR));
    if (tl_apt_mot_move_absolute_with_pos(g_context, absolute_pos) != 0)
    {
        warning_message("unable to set \n");
    }
    snprintf(xvueye_ti_message,64,"Set focus at %g",(float)absolute_pos/POS_ENC_CONV_FACTOR);    
    if (do_apt_api_setVvelocity_mm_s(pfMinVel, pfAccn, pfMaxVel))
    warning_message("Error setting velocity params apt api\n");
    
    return 0;
}

int do_thorlabs_APT_small_dec_MOT_pos(void)
{
    int absolute_pos = 0;
    float pfMinVel = 0, pfAccn = 8000, pfMaxVel = 1;
    
    if(updating_menu_state != 0)
      {
	if (g_context == NULL) active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }    

    if (g_context == NULL)
      {        win_printf("please init before use"); return 0;    }

    if (tl_apt_mot_get_move_abspos(g_context, &absolute_pos) != 0)
        warning_message("unable to get\n");

    if (do_apt_api_getVvelocity_mm_s(&pfMinVel, &pfAccn, &pfMaxVel))
      warning_message("Error reading velocity params from apt api");
    
    if (do_apt_api_setVvelocity_mm_s(pfMinVel, pfAccn, 2.5))
      warning_message("Error setting velocity params apt api\n");

    absolute_pos -= (int)(0.5 + (small_step*POS_ENC_CONV_FACTOR));
    if (tl_apt_mot_move_absolute_with_pos(g_context, absolute_pos) != 0)
    {
        warning_message("unable to set \n");
    }
    snprintf(xvueye_ti_message,64,"Set focus at %g",(float)absolute_pos/POS_ENC_CONV_FACTOR);
    if (do_apt_api_setVvelocity_mm_s(pfMinVel, pfAccn, pfMaxVel))
    warning_message("Error setting velocity params apt api\n");    
    return 0;    
}



#endif // end of thorlabs apt api stuff
#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "magcuti.h"
//place here other headers of this plugin


int     copy_one_frame_of_movie(O_i *dest, int dest_fr, O_i *ois, int src_fr)
{
    union pix *pd, *ps;
    int i, j, onx, ony, donx, dony;

    if (ois == NULL || dest == NULL)                       return 1;

    if (ois->im.data_type != dest->im.data_type)           return 2;



    switch_frame(dest,dest_fr);
    switch_frame(ois,src_fr);
    pd = dest->im.pixel;
    ps = ois->im.pixel;
    onx = ois->im.nx;
    ony = ois->im.ny;
    donx = dest->im.nx;
    dony = dest->im.ny;


    if ((donx != onx) || (dony != ony))  return 4;

    //win_printf("Desti -> %d %d\nori %d %d %d %d",posx, posy, x_start,  y_start, nx, ny);

    if (ois->im.data_type == IS_CHAR_IMAGE)
    {
        for (i = 0; i < ony; i++)
        {
            for (j = 0; j < onx; j++)
                pd[i].ch[j] = ps[i].ch[j];
        }
    }
    else if (ois->im.data_type == IS_RGB_PICTURE)
    {
        for (i = 0; i < ony; i++)
        {
            for (j = 0; j < onx; j++)
                pd[i].rgb[j] = ps[i].rgb[j];
        }
    }
    else if (ois->im.data_type == IS_RGBA_PICTURE)
    {
        for (i = 0; i < ony; i++)
        {
            for (j = 0; j < onx; j++)
                pd[i].rgba[j] = ps[i].rgba[j];
        }
    }
    else if (ois->im.data_type == IS_RGB16_PICTURE)
    {
        for (i = 0; i < ony; i++)
        {
            for (j = 0; j < onx; j++)
                pd[i].rgb16[j] = ps[i].rgb16[j];
        }
    }
    else if (ois->im.data_type == IS_RGBA16_PICTURE)
    {
        for (i = 0; i < ony; i++)
        {
            for (j = 0; j < onx; j++)
                pd[i].rgba16[j] = ps[i].rgba16[j];
        }
    }
    else if (ois->im.data_type == IS_INT_IMAGE)
    {
        for (i = 0; i < ony; i++)
        {
            for (j = 0; j < onx; j++)
                pd[i].in[j] = ps[i].in[j];
        }
    }
    else if (ois->im.data_type == IS_UINT_IMAGE)
    {
        for (i = 0; i < ony; i++)
        {
            for (j = 0; j < onx; j++)
                pd[i].ui[j] = ps[i].ui[j];
        }
    }
    else if (ois->im.data_type == IS_LINT_IMAGE)
    {
        for (i = 0; i < ony; i++)
        {
            for (j = 0; j < onx; j++)
                pd[i].li[j] = ps[i].li[j];
        }
    }
    else if (ois->im.data_type == IS_FLOAT_IMAGE)
    {
        for (i = 0; i < ony; i++)
        {
            for (j = 0; j < onx; j++)
                pd[i].fl[j] = ps[i].fl[j];
        }
    }
    else if (ois->im.data_type == IS_COMPLEX_IMAGE)
    {
        for (i = 0; i < ony; i++)
        {
            for (j = 0; j < onx; j++)
                pd[i].cp[j] = ps[i].cp[j];
        }
    }
    else if (ois->im.data_type == IS_DOUBLE_IMAGE)
    {
        for (i = 0; i < ony; i++)
        {
            for (j = 0; j < onx; j++)
                pd[i].db[j] = ps[i].db[j];
        }
    }
    else if (ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
    {
        for (i = 0; i < ony; i++)
        {
            for (j = 0; j < onx; j++)
                pd[i].dcp[j] = ps[i].dcp[j];
        }
    }

    return 0;
}



int do_magcuti_average_along_y(void)
{
	register int i, j;
	int l_s, l_e;
	O_i *ois = NULL;
	O_p *op = NULL;
	d_s *ds;
	imreg *imr = NULL;


	if(updating_menu_state != 0)	return D_O_K;

	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine average the image intensity"
			"of several lines of an image");
	}
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	return D_O_K;
	/* we grap the data that we need, the image and the screen region*/
	l_s = 0; l_e = ois->im.ny;
	/* we ask for the limit of averaging*/
	i = win_scanf("Average Between lines%d and %d",&l_s,&l_e);
	if (i == WIN_CANCEL)	return D_O_K;
	/* we create a plot to hold the profile*/
	op = create_one_plot(ois->im.nx,ois->im.nx,0);
	if (op == NULL) return (win_printf_OK("cant create plot!"));
	ds = op->dat[0]; /* we grab the data set */
	op->y_lo = ois->z_black;	op->y_hi = ois->z_white;
	op->iopt2 |= Y_LIM;	op->type = IM_SAME_X_AXIS;
	inherit_from_im_to_ds(ds, ois);
	op->filename = Transfer_filename(ois->filename);
	set_plot_title(op,"magcuti averaged profile from line %d to %d",l_s, l_e);
	set_formated_string(&ds->treatement,"magcutti averaged profile from line %d to %d",l_s, l_e);
	uns_oi_2_op(ois, IS_Z_UNIT_SET, op, IS_Y_UNIT_SET);
	uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
	for (i=0 ; i< ds->nx ; i++)	ds->yd[i] = 0;
	for (i=l_s ; i< l_e ; i++)
	{
		extract_raw_line(ois, i, ds->xd);
		for (j=0 ; j< ds->nx ; j++)	ds->yd[j] += ds->xd[j];
	}
	for (i=0, j = l_e - l_s ; i< ds->nx && j !=0 ; i++)
	{
		ds->xd[i] = i;
		ds->yd[i] /= j;
	}
	add_one_image (ois, IS_ONE_PLOT, (void *)op);
	ois->cur_op = ois->n_op - 1;
	broadcast_dialog_message(MSG_DRAW,0);
	return D_REDRAWME;
}

O_i	*magcuti_image_multiply_by_a_scalar(O_i *ois, float factor)
{
	register int i, j;
	O_i *oid;
	float tmp;
	int onx, ony, data_type;
	union pix *ps, *pd;

	onx = ois->im.nx;	ony = ois->im.ny;	data_type = ois->im.data_type;
	oid =  create_one_image(onx, ony, data_type);
	if (oid == NULL)
	{
		win_printf("Can't create dest image");
		return NULL;
	}
	if (data_type == IS_CHAR_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].ch[j];
				pd[i].ch[j] = (tmp < 0) ? 0 :
					((tmp > 255) ? 255 : (unsigned char)tmp);
			}
		}
	}
	else if (data_type == IS_INT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].in[j];
				pd[i].in[j] = (tmp < -32768) ? -32768 :
					((tmp > 32767) ? 32767 : (short int)tmp);
			}
		}
	}
	else if (data_type == IS_UINT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].ui[j];
				pd[i].ui[j] = (tmp < 0) ? 0 :
					((tmp > 65535) ? 65535 : (unsigned short int)tmp);
			}
		}
	}
	else if (data_type == IS_LINT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].li[j];
				pd[i].li[j] = (tmp < 0x10000000) ? 0x10000000 :
					((tmp > 0x7FFFFFFF) ? 0x7FFFFFFF : (int)tmp);
			}
		}
	}
	else if (data_type == IS_FLOAT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].fl[j] = factor * ps[i].fl[j];
			}
		}
	}
	else if (data_type == IS_DOUBLE_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].db[j] = factor * ps[i].db[j];
			}
		}
	}
	else if (data_type == IS_COMPLEX_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].fl[2*j] = factor * ps[i].fl[2*j];
				pd[i].fl[2*j+1] = factor * ps[i].fl[2*j+1];
			}
		}
	}
	else if (data_type == IS_COMPLEX_DOUBLE_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].db[2*j] = factor * ps[i].db[2*j];
				pd[i].db[2*j+1] = factor * ps[i].db[2*j+1];
			}
		}
	}
	else if (data_type == IS_RGB_PICTURE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< 3*onx; j++)
			{
				tmp = factor * ps[i].ch[j];
				pd[i].ch[j] = (tmp < 0) ? 0 :
					((tmp > 255) ? 255 : (unsigned char)tmp);
			}
		}
	}
	else if (data_type == IS_RGBA_PICTURE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< 4*onx; j++)
			{
				tmp = factor * ps[i].ch[j];
				pd[i].ch[j] = (tmp < 0) ? 0 :
					((tmp > 255) ? 255 : (unsigned char)tmp);
			}
		}
	}
	inherit_from_im_to_im(oid,ois);
	uns_oi_2_oi(oid,ois);
	oid->im.win_flag = ois->im.win_flag;
	if (ois->title != NULL)	set_im_title(oid, "%s rescaled by %g",
		 ois->title,factor);
	set_formated_string(&oid->im.treatement,
		"Image %s rescaled by %g", ois->filename,factor);
	return oid;
}

int do_magcuti_image_multiply_by_a_scalar(void)
{
	O_i *ois, *oid;
	imreg *imr;
	int i;
	static float factor = 2.0;


	if(updating_menu_state != 0)	return D_O_K;

	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine multiply the image intensity"
		"by a user defined scalar factor");
	}
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	{
		win_printf("Cannot find image");
		return D_O_K;
	}
	i = win_scanf("define the factor of mutiplication %f",&factor);
	if (i == WIN_CANCEL)	return D_O_K;
	oid = magcuti_image_multiply_by_a_scalar(ois,factor);
	if (oid == NULL)	return win_printf_OK("unable to create image!");
	add_image(imr, oid->type, (void*)oid);
	find_zmin_zmax(oid);
	return (refresh_image(imr, imr->n_oi - 1));
}
int do_magcuti_grab_images_vs_focus_0(void)
{
  O_i *ois;
  imreg *imr;
  int i, j;
  float start_pos = 0, dzt, dz, zpos;
  static int dt = 10;
  static int nf = 32;
#ifdef XV_WIN32
  int i_motor = -1;
  static long motor_id = -1;
  char sel[32] = {0};
#endif    
  static float bef_dz = -0.5, after_dz = 1.5;
  static int org = 1;
  char message[2048] = {0};

    if(updating_menu_state != 0)
      {
	if (g_context == NULL) active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }    

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the image intensity"
			   "by a user defined scalar factor");
    }
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      win_printf("Cannot find image");
      return D_O_K;
    }
# ifdef XV_WIN32  
  if (plNumUnits < 0)
    return win_printf_OK("No motor driver detected!\n"
			 "You probably need to start APT!");
# elif defined XV_UNIX
    if (g_context == NULL)
    {
        return win_printf_OK("please init before use");
    }
# endif    

  
  if (oi_xvuEye == NULL)
    return win_printf_OK("Your camera does not seems to be started!\n"
			 "You probably need to start uEye movie!");

  if (oi_xvuEye != ois)
    return win_printf_OK("Your image does not correspond to the live Video!\n"
			 "switch to uEye movie!");

#ifdef XV_WIN32
  
  if ( motor_id < 0)
    {
      if (plNumUnits > 1)
	{
	  int n_sel = 0;
	  snprintf(message+strlen(message),sizeof(message)-strlen(message),
		   "You have several motors attached to your PC, please select the one attached to focus:\n");
	  for (i = 0; i < plNumUnits && i < 64; i++)
	    {
	      j = MOT_GetPosition(plSerialNum[i], pfPosition + i);
	      if (i == 0) sprintf(sel,"%%%%R->");
	      else sprintf(sel,"%%%%r->");
	      if (j == 0)
		snprintf(message+strlen(message),sizeof(message)-strlen(message),
			 "%s->Cube %d with serial # %d at %g\n",sel,i,(int)plSerialNum[i],pfPosition[i]);
	      else snprintf(message+strlen(message),sizeof(message)-strlen(message),
			    "%s->error on Cube %d with serial # %d at %g\n",sel,i,(int)plSerialNum[i],pfPosition[i]);
	    }
	  i = win_scanf(message,&n_sel);
	  if (i == WIN_CANCEL)	return D_O_K;
	  motor_id = plSerialNum[n_sel];
	  i_motor = n_sel;
	}
      else if (plNumUnits == 1)
	{
	  win_printf("%d device\nSerial num %d\n",(int)plNumUnits,(int)plSerialNum[0]);
	  motor_id = plSerialNum[0];
	  i_motor = 0;
	}
    }

  win_printf("Device 0 serial # %d",(int)plSerialNum[0]);

  if (motor_id < 0)
    return win_printf_OK("No motor driver selected!\n"
			 "You probably need to select one!");


  for (i = 0, j = 1; i < plNumUnits && i < 64; i++)
    {
      if (plSerialNum[i] == motor_id)
	{
	  j = MOT_GetPosition(plSerialNum[i], pfPosition + i);
	  i_motor = i;
	  if (j == 0) start_pos = pfPosition[i];
	  else return win_printf_OK("Error on Cube %d with serial # %d\n at %g\n",i,(int)plSerialNum[i],pfPosition[i]);
	}
    }
  if (j != 0 || i_motor < 0)     return win_printf_OK("Cannot read motor position\n");

#elif defined XV_UNIX
  j = apt_api_GetMotPosition(&start_pos);
  if (j) return win_printf_OK("Error in grabbing position from apt api\n");
#endif

  snprintf(message,sizeof(message),"Grabbing a series of pictures at different focus\n"
	  "Specify the number of frames %%4d\n"
	  "Strarting from the present focus position %g\n"
	  "Define the extend before this position to start the focus scan %%6f\n"
	  "Define the extend after this position to end the focus scan %%6f\n"
	  "Settling wait time after focus motion %%5d (ms)\n"
	  "%%b click here to go back to present focus at the end of the scan\n"
	  ,start_pos);

  i = win_scanf(message,&nf,&bef_dz,&after_dz,&dt,&org);
  if (i == WIN_CANCEL)	return D_O_K;

  dzt = after_dz - bef_dz;
  dz = dzt/(nf-1);
  zpos = start_pos + bef_dz;
#ifdef XV_WIN32  
  j = MOT_MoveAbsoluteEx(motor_id, zpos, 1); // wait
  if (j) return win_printf_OK("Error setting position off Cube %d with serial # %d\nerror %f"
			      ,i_motor,(int)motor_id,j);  
#elif defined XV_UNIX
  float tpos = 0, lpos = 0;
  j =  apt_api_SetMotPosition(zpos);
  if (j) return win_printf_OK("Error setting position in apt api");
  for (lpos = tpos = start_pos, i = 0; fabs(lpos - zpos) > 0.01 && i < 512;i++)
    {
      psleep(20);
      j = apt_api_GetMotPosition(&tpos);
      if (j == 0) lpos = tpos;
    }
  
  if (fabs(lpos - zpos) > 0.01)
    return win_printf_OK("Error in setting position from apt api\n");
  
#endif   


  i = win_printf("Press OK to start scan");



  return 0;
}



int do_magcuti_grab_images_vs_focus(void)
{
  O_i *ois, *oim = NULL;
  O_p *op = NULL;
  d_s *ds = NULL;
  imreg *imr;
  int i, j;
  float start_pos = 0, dzt, dz, zpos;
  static int dt = 10;
  static int nf = 32;
#ifdef XV_WIN32
  int i_motor = -1;
  static long motor_id = -1;
  char sel[32] = {0};
#endif  
  static float bef_dz = -0.5, after_dz = 1.5;
  static int org = 1;
  char message[2048] = {0};
  float tpos = 0, lpos = 0;

    if(updating_menu_state != 0)
      {
	if (g_context == NULL) active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }    


  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the image intensity"
			   "by a user defined scalar factor");
    }
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      win_printf("Cannot find image");
      return D_O_K;
    }

# ifdef XV_WIN32  
  if (plNumUnits < 0)
    return win_printf_OK("No motor driver detected!\n"
			 "You probably need to start APT!");
# elif defined XV_UNIX
    if (g_context == NULL)
    {
	return win_printf_OK("No motor driver detected!\n"
			 "You probably need to start apt_api!");	
    }
# endif    

  if (oi_xvuEye == NULL)
    return win_printf_OK("Your camera does not seems to be started!\n"
			 "You probably need to start uEye movie!");

  if (oi_xvuEye != ois)
    return win_printf_OK("Your image does not correspond to the live Video!\n"
			 "switch to uEye movie!");

# ifdef XV_WIN32  
  if ( motor_id < 0)
    {
      if (plNumUnits > 1)
	{
	  int n_sel = 0;
	  snprintf(message+strlen(message),sizeof(message)-strlen(message),
		   "You have several motors attached to your PC, please select the one attached to focus:\n");
	  for (i = 0; i < plNumUnits && i < 64; i++)
	    {
	      j = MOT_GetPosition(plSerialNum[i], pfPosition + i);
	      if (i == 0) sprintf(sel,"%%%%R->");
	      else sprintf(sel,"%%%%r->");
	      if (j == 0)
		snprintf(message+strlen(message),sizeof(message)-strlen(message),
			 "%s->Cube %d with serial # %d at %g\n",sel,i,(int)plSerialNum[i],pfPosition[i]);
	      else snprintf(message+strlen(message),sizeof(message)-strlen(message),
			    "%s->error on Cube %d with serial # %d at %g\n",sel,i,(int)plSerialNum[i],pfPosition[i]);
	    }
	  i = win_scanf(message,&n_sel);
	  if (i == WIN_CANCEL)	return D_O_K;
	  motor_id = plSerialNum[n_sel];
	  i_motor = n_sel;
	}
      else if (plNumUnits == 1)
	{
	  win_printf("%d device\nSerial num %d\n",(int)plNumUnits,plSerialNum[0]);
	  motor_id = plSerialNum[0];
	  i_motor = 0;
	}
    }

  if (motor_id < 0)
    return win_printf_OK("No motor driver selected!\n"
			 "You probably need to select one!");


  for (i = 0, j = 1; i < plNumUnits && i < 64; i++)
    {
      if (plSerialNum[i] == motor_id)
	{
	  j = MOT_GetPosition(plSerialNum[i], pfPosition + i);
	  i_motor = i;
	  if (j == 0) start_pos = pfPosition[i];
	  else return win_printf_OK("Error on Cube %d with serial # %d\n at %g\n",i,(int)plSerialNum[i],pfPosition[i]);
	}
    }
  if (j != 0 || i_motor < 0)     return win_printf_OK("Cannot read motor position\n");
#elif defined XV_UNIX
  j = apt_api_GetMotPosition(&start_pos);
  if (j) return win_printf_OK("Error in grabbing position from apt api\n");
#endif

  
  
  snprintf(message,sizeof(message),"Grabbing a series of pictures at different focus\n"
	  "Specify the number of frames %%4d\n"
	  "Strarting from the present focus position %g\n"
	  "Define the extend before this position to start the focus scan %%6f\n"
	  "Define the extend after this position to end the focus scan %%6f\n"
	  "Settling wait time after focus motion %%5d (ms)\n"
	  "%%b click here to go back to present focus at the end of the scan\n"
	  ,start_pos);

  i = win_scanf(message,&nf,&bef_dz,&after_dz,&dt,&org);
  if (i == WIN_CANCEL)	return D_O_K;

  oim = create_and_attach_movie_to_imr(imr, ois->im.nx, ois->im.ny, ois->im.data_type, nf);
  if (oim == NULL) return win_printf_OK("cannot create movie of modulation");
  set_zmin_zmax_values(oim, 0, 255);
  set_z_black_z_white_values(oim, 0, 255);
  inherit_from_im_to_im(oim, ois);
  op = create_and_attach_op_to_oi(oim, nf, nf, 0, 0);
  //op = create_and_attach_op_to_oi(ois, nf, nf, 0, 0);
  if (op == NULL) return win_printf_OK("cannot create plot");
  ds = op->dat[0];

  dzt = after_dz - bef_dz;
  dz = dzt/(nf-1);
  zpos = start_pos + bef_dz;



#ifdef XV_WIN32  
  j = MOT_MoveAbsoluteEx(motor_id, zpos, 1); // wait
  if (j) return win_printf_OK("Error setting position off Cube %d with serial # %d\nerror %f"
			      ,i_motor,(int)motor_id,j);  
#elif defined XV_UNIX
  j =  apt_api_SetMotPosition(zpos);
  if (j) return win_printf_OK("Error setting position in apt api");
  for (lpos = tpos = start_pos, i = 0; fabs(lpos - zpos) > 0.01 && i < 512;i++)
    {
      j = apt_api_GetMotPosition(&tpos);
      if (j == 0) lpos = tpos;
      psleep(20);
    }
  
  if (fabs(lpos - zpos) > 0.01)
    return win_printf_OK("Error in setting position from apt api\n");
  
#endif   

  i = win_printf("Press OK to start scan");
  unsigned long t0, dtt;
  for (i = 0, j = 0, t0 = my_uclock(); i < nf && j == 0; i++)
    {
      switch_frame(oim,i);
#ifdef XV_WIN32      
      j = MOT_GetPosition(motor_id, pfPosition + i_motor);
      tpos = pfPosition[i_motor];
#elif defined XV_UNIX      
      j = apt_api_GetMotPosition(&tpos);
#endif      
      dtt = my_uclock() - t0;
      if (j == 0)
	{
	  ds->yd[i] = tpos; 
	  ds->xd[i] =  (float)dtt/get_my_uclocks_per_sec();
	  copy_one_frame_of_movie(oim, i, ois, ois->im.c_f);
	  zpos += dz;
#ifdef XV_WIN32      	  
	  j = MOT_MoveAbsoluteEx(motor_id, zpos, 1); // wait
	  Sleep(dt);
#elif defined XV_UNIX
	  j =  apt_api_SetMotPosition(zpos);
	  if (j) return win_printf_OK("Error setting position in apt api");
	  for (lpos = tpos = start_pos, i = 0; fabs(lpos - zpos) > 0.01 && i < 512;i++)
	    {
	      psleep(20);
	      j = apt_api_GetMotPosition(&tpos);
		if (j == 0) lpos = tpos;
	    }
	  if (fabs(lpos - zpos) > 0.01)
	    return win_printf_OK("Error in setting position from apt api\n");
	  psleep(dt);
#endif	  
	  refresh_image(imr, imr->n_oi - 1);
	}
    }
  if (org)
    {
      zpos = start_pos;
#ifdef XV_WIN32      
      j = MOT_MoveAbsoluteEx(motor_id, zpos, 0); // wait
      if (j) return win_printf_OK("Error setting position off Cube %d with serial # %d\n"
				  ,i_motor,(int)motor_id);
#elif defined XV_UNIX            
      j =  apt_api_SetMotPosition(zpos);
      if (j) return win_printf_OK("Error setting position at %g in apt api\n",zpos);
#endif            
    }
  return (refresh_image(imr, imr->n_oi - 1));
}
int do_small_inc_MOT_pos(void)
{
  if(updating_menu_state != 0)	
     {
	if (g_context == NULL) active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;       
        remove_short_cut_from_dialog(0, KEY_F3);
        add_keyboard_short_cut(0, KEY_F3, 0, do_small_inc_MOT_pos);
        return D_O_K;
     }
  do_thorlabs_APT_small_inc_MOT_pos();
  return 0;
}
int do_small_dec_MOT_pos(void)
{
  if(updating_menu_state != 0)	
     {
	if (g_context == NULL) active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;              
        remove_short_cut_from_dialog(0, KEY_F2);
        add_keyboard_short_cut(0, KEY_F2, 0, do_small_dec_MOT_pos);
        return D_O_K;
     }
  do_thorlabs_APT_small_dec_MOT_pos();
  return 0;
}

# ifdef NOT_USED

int do_zoom_in_im(void)
{
  imreg *imr;
  O_i *ois = NULL;
  int w0, h0, w, h, xc, yc;
  float rw, rh;

  if(updating_menu_state != 0)	
     {
        remove_short_cut_from_dialog(0, KEY_F8);
        add_keyboard_short_cut(0, KEY_F8, 0, do_zoom_in_im);
        return D_O_K;
     }
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      win_printf("Cannot find image");
      return D_O_K;
    }
  if (ois != oi_xvuEye) return win_printf_OK("Not the video image");

  w = ois->im.nxe - ois->im.nxs;
  xc = (ois->im.nxe + ois->im.nxs)/2;
  w0 = ois->im.nx;
  h = ois->im.nye - ois->im.nys;
  yc = (ois->im.nye + ois->im.nys)/2;
  h0 = ois->im.ny;
  rw = w0/w;
  rh = h0/h;
  //SCREEN_W, SCREEN_H;
  if (rw < 16 || rh < 16)
    {
            rw = 2*(int)rw;
            set_oi_region_of_interest_in_pixel(ois, xc - w0/(2*rw), yc - h0/(2*rw), xc + w0/(2*rw), yc + h0/(2*rw));
    }
  return refresh_image(imr, UNCHANGED);
}

int do_zoom_out_im(void)
{
  imreg *imr;
  O_i *ois = NULL;
  int w0, h0, w, h, xc, yc, nonx, nony;
  int xs, xe, ys, ye;
  float rw, rh;

  if(updating_menu_state != 0)	
     {
        remove_short_cut_from_dialog(0, KEY_F5);
        add_keyboard_short_cut(0, KEY_F5, 0, do_zoom_out_im);
        return D_O_K;
     }
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      win_printf("Cannot find image");
      return D_O_K;
    }
  if (ois != oi_xvuEye) return win_printf_OK("Not the video image");
  w = ois->im.nxe - ois->im.nxs;
  xc = (ois->im.nxe + ois->im.nxs)/2;
  w0 = ois->im.nx;
  h = ois->im.nye - ois->im.nys;
  yc = (ois->im.nye + ois->im.nys)/2;
  h0 = ois->im.ny;
  rw = w0/w;
  rh = h0/h;
  //SCREEN_W, SCREEN_H;
  if (rw > 1 || rh > 1)
    {
            rw = (int)(rw/2);
            rw = (rw < 1) ? 1 : rw;
            nonx = w0/(2*rw);
            nony = h0/(2*rw);

            xe = xc + nonx;
            xe = (xe > w0) ? w0 : xe;
            xs = xe - 2*nonx;
            xs = (xs < 0) ? 0 : xs;

            ye = yc + nony;
            ye = (ye > h0) ? h0 : ye;
            ys = ye - 2*nony;
            ys = (ys < 0) ? 0 : ys;

            set_oi_region_of_interest_in_pixel(ois, xs, ys, xe, ye);
    }
  return refresh_image(imr, UNCHANGED);
}

int do_zoom_mov_x_r_im(void)
{
  imreg *imr;
  O_i *ois = NULL;
  int w0, w, xc;
  int xs, xe, ys, ye;

  if(updating_menu_state != 0)	
     {
        remove_short_cut_from_dialog(0, KEY_RIGHT);
        add_keyboard_short_cut(0, KEY_RIGHT, 0, do_zoom_mov_x_r_im);
        return D_O_K;
     }
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      win_printf("Cannot find image");
      return D_O_K;
    }
  if (ois != oi_xvuEye) return win_printf_OK("Not the video image");
  w = ois->im.nxe - ois->im.nxs;
  xc = (ois->im.nxe + ois->im.nxs)/2;
  w0 = ois->im.nx;
  xc += w/4;
  xe = xc + w/2;
  xe = (xe > w0) ? w0 : xe;
  xs = xe - w;
  xs = (xs < 0) ? 0 : xs;
  ys = ois->im.nys;
  ye = ois->im.nye;
  set_oi_region_of_interest_in_pixel(ois, xs, ys, xe, ye);
  return refresh_image(imr, UNCHANGED);
}

int do_zoom_mov_x_l_im(void)
{
  imreg *imr;
  O_i *ois = NULL;
  int w0, w, xc;
  int xs, xe, ys, ye;

  if(updating_menu_state != 0)	
     {
        remove_short_cut_from_dialog(0, KEY_LEFT);
        add_keyboard_short_cut(0, KEY_LEFT, 0, do_zoom_mov_x_l_im);
        return D_O_K;
     }
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      win_printf("Cannot find image");
      return D_O_K;
    }
  if (ois != oi_xvuEye) return win_printf_OK("Not the video image");
  w = ois->im.nxe - ois->im.nxs;
  xc = (ois->im.nxe + ois->im.nxs)/2;
  w0 = ois->im.nx;
  xc -= w/4;
  xe = xc + w/2;
  xe = (xe > w0) ? w0 : xe;
  xs = xe - w;
  xs = (xs < 0) ? 0 : xs;
  ys = ois->im.nys;
  ye = ois->im.nye;
  set_oi_region_of_interest_in_pixel(ois, xs, ys, xe, ye);
  return refresh_image(imr, UNCHANGED);
}


int do_zoom_mov_y_u_im(void)
{
  imreg *imr;
  O_i *ois = NULL;
  int  h0, h, yc;
  int xs, xe, ys, ye;

  if(updating_menu_state != 0)	
     {
        remove_short_cut_from_dialog(0, KEY_UP);
        add_keyboard_short_cut(0, KEY_UP, 0, do_zoom_mov_y_u_im);
        return D_O_K;
     }
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      win_printf("Cannot find image");
      return D_O_K;
    }
  if (ois != oi_xvuEye) return win_printf_OK("Not the video image");

  h = ois->im.nye - ois->im.nys;
  yc = (ois->im.nye + ois->im.nys)/2;
  h0 = ois->im.ny;
  yc += h/4;
  ye = yc + h/2;
  ye = (ye > h0) ? h0 : ye;
  ys = ye - h;
  ys = (ys < 0) ? 0 : ys;
  xs = ois->im.nxs;
  xe = ois->im.nxe;
  set_oi_region_of_interest_in_pixel(ois, xs, ys, xe, ye);
  return refresh_image(imr, UNCHANGED);
}
int do_zoom_mov_y_d_im(void)
{
  imreg *imr;
  O_i *ois = NULL;
  int h0, h, yc;
  int xs, xe, ys, ye;

  if(updating_menu_state != 0)	
     {
        remove_short_cut_from_dialog(0, KEY_DOWN);
        add_keyboard_short_cut(0, KEY_DOWN, 0, do_zoom_mov_y_d_im);
        return D_O_K;
     }
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      win_printf("Cannot find image");
      return D_O_K;
    }
  if (ois != oi_xvuEye) return win_printf_OK("Not the video image");
  h = ois->im.nye - ois->im.nys;
  yc = (ois->im.nye + ois->im.nys)/2;
  h0 = ois->im.ny;
  yc -= h/4;
  ye = yc + h/2;
  ye = (ye > h0) ? h0 : ye;
  ys = ye - h;
  ys = (ys < 0) ? 0 : ys;
  xs = ois->im.nxs;
  xe = ois->im.nxe;
  set_oi_region_of_interest_in_pixel(ois, xs, ys, xe, ye);
  return refresh_image(imr, UNCHANGED);
}

#endif // NOT_USED
int grab_partial_image_im_movie(void)
{
    int i, j;
    imreg *imr = NULL;
#ifdef XV_WIN32
    int i_motor = -1;
    static long motor_id = -1;
    char sel[32] = {0};
#endif  
    static float bef_dz = 0.2, dz = 1;
    O_i *ois = NULL, *oim = NULL;
    O_p *op = NULL;
    static int xc = 512, yc = 256, w = 2048, h = 1024, nf = 200, start = 10, org = 1;
    static int usercenter = 0;
    char message[1024] = {0};
    float pfMinVel = 0.01, pfAccn, pfMaxVel = 2.5, vz;
    float zpos, start_pos, lpos, tpos;


    if(updating_menu_state != 0)
      {
	if (g_context == NULL) active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }    
    
    if(updating_menu_state != 0)	return D_O_K;
    
    if (key[KEY_LSHIFT])
      return win_printf_OK("This routine start the averaging of an image area");
    
    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
      return win_printf_OK("cannot find image region");
# ifdef XV_WIN32  
    if (plNumUnits < 0)
      return win_printf_OK("No motor driver detected!\n"
			   "You probably need to start APT!");
# elif defined XV_UNIX
    if (g_context == NULL)
      {
	return win_printf_OK("No motor driver detected!\n"
			     "You probably need to start apt api!");	
      }
# endif    
    
  

    if (oi_xvuEye == NULL)
      return win_printf_OK("Your camera does not seems to be started!\n"
			   "You probably need to start uEye movie!");
    
    if (oi_xvuEye != ois)
      return win_printf_OK("Your image does not correspond to the live Video!\n"
			   "switch to uEye movie!");
#ifdef XV_WIN32

    if (motor_id < 0)
      {
	if (plNumUnits > 1)
	  {
	    int n_sel = 0;
	    snprintf(message+strlen(message),sizeof(message)-strlen(message),
		     "You have several motors attached to your PC,"
		     " please select the one attached to focus:\n");
	    for (i = 0; i < plNumUnits && i < 64; i++)
	      {
		j = MOT_GetPosition(plSerialNum[i], pfPosition + i);
		if (i == 0) sprintf(sel,"%%%%R->");
		else sprintf(sel,"%%%%r->");
		if (j == 0)
		  snprintf(message+strlen(message)
			   ,sizeof(message)-strlen(message),
			   "%s->Cube %d with serial # %d at %g\n"
			   ,sel,i,(int)plSerialNum[i],pfPosition[i]);
		else snprintf(message+strlen(message)
			      ,sizeof(message)-strlen(message),
			      "%s->error on Cube %d with serial # %d at %g\n"
			      ,sel,i,(int)plSerialNum[i],pfPosition[i]);
	      }
	    i = win_scanf(message,&n_sel);
	    if (i == WIN_CANCEL)	return D_O_K;
	    motor_id = plSerialNum[n_sel];
	    i_motor = n_sel;
	}
	else if (plNumUnits == 1)
	  {
	    win_printf("%d device\nSerial num %d\n"
		       ,(int)plNumUnits,plSerialNum[0]);
	    motor_id = plSerialNum[0];
	    i_motor = 0;
	  }
      }
    
    if (motor_id < 0)
      return win_printf_OK("No motor driver selected!\n"
			   "You probably need to select one!");
    
    for (i = 0, j = 1; i < plNumUnits && i < 64; i++)
      {
	if (plSerialNum[i] == motor_id)
	  {
	    j = MOT_GetPosition(plSerialNum[i], pfPosition + i);
	    i_motor = i;
	    if (j == 0) start_pos = pfPosition[i];
	    else return win_printf_OK("Error on Cube %d with serial # %d\n"
				      " at %g\n",i,(int)plSerialNum[i]
				      ,pfPosition[i]);
	    j = MOT_GetVelParams(plSerialNum[i], &pfMinVel, &pfAccn, &pfMaxVel);
	    if (j) win_printf_OK("Error reading velocity params\n"
				 "on Cube %d with serial # %d\n"
				 ,i,(int)plSerialNum[i]);
	    
	  }
      }
    if (j != 0 || i_motor < 0)
      return win_printf_OK("Cannot read motor position\n");


#elif defined XV_UNIX
    j = apt_api_GetMotPosition(&start_pos);
    if (j) return win_printf_OK("Error in grabbing position from apt api\n");
    
    j = do_apt_api_getVvelocity_mm_s(&pfMinVel, &pfAccn, &pfMaxVel);
    if (j) return win_printf_OK("Error reading velocity params from apt api");
    
#endif
    
    vz = pfMaxVel;
    snprintf(message,sizeof(message)
	     ,"Grabbing a series of frames while focus moves\n"
	     "Specify the number of frames %%4d\n"
	     "Strarting from the present focus position Z=%g\n"
	     "retract focus by zr = %%6f before the ramp (Z - zr)\n"
	     "Define the extend aimed in focus scan Ez = %%6f\n"
	     "Scan wil go from (Z - zr) to (Z - zr + Ez)\n"
	     "Min velocity (%g), %%6f Max velocity (%g), %%6f \n"
	     "Acceleration (%g), %%6f \n"
	     "Camera center xc = %d, w = %d, yc = %d, h = %d\n"
	     "AOI center: %%R->camera center %%r->user defined\n"
	     "In the later case define center xc %%6d yc %%6d\n"
	     "Width %%6d Height %%6d\n"
	     "%%b click here to go back to present focus at the end of the scan\n"
	     ,start_pos, pfMinVel, vz, pfAccn
	     ,(ois->im.nxe+ois->im.nxs)/2,(ois->im.nxe-ois->im.nxs)
	     ,(ois->im.nye+ois->im.nys)/2,(ois->im.nye-ois->im.nys));
    
    i = win_scanf(message,&nf,&bef_dz,&dz,&pfMinVel, &pfMaxVel, &pfAccn,
		  &usercenter,&xc, &yc, &w, &h, &org);
    if (i == WIN_CANCEL)	return D_O_K;

    if (usercenter == 0)
      {
	xc = (ois->im.nxe+ois->im.nxs)/2;
	yc = (ois->im.nye+ois->im.nys)/2;
      }
		
	
#ifdef XV_WIN32  
    j = MOT_SetVelParams(plSerialNum[i_motor], pfMinVel, pfAccn, 2.5);
    if (j) win_printf_OK("Error setting velocity params\n"
			 "on Cube %d with serial # %d\n"
			 ,i_motor,(int)plSerialNum[i_motor]);
#elif defined XV_UNIX
    j = do_apt_api_setVvelocity_mm_s(pfMinVel, pfAccn, 2.5);
    if (j) return win_printf_OK("Error setting velocity params apt api\n");
#endif  
    
    oim = create_and_attach_movie_to_imr(imr, w, h, ois->im.data_type, nf);
    if (oim == NULL) return win_printf_OK("cannot create movie of modulation");
    set_oi_source(oim, "Data taken at xc = %d, yc = %d, h = %d w = %d "
		  "zfocus start %g dz %g speep %g"
		  , xc,yc,h,w,start_pos-bef_dz,dz,pfMaxVel);

    zpos = start_pos - bef_dz;

#ifdef XV_WIN32      	  
    j = MOT_MoveAbsoluteEx(motor_id, zpos, 1); // wait
    if (j) return win_printf_OK("Error setting position off Cube %d with "
				"serial # %d\n" ,i_motor,(int)motor_id);  
#elif defined XV_UNIX
    j =  apt_api_SetMotPosition(zpos);
    if (j) return win_printf_OK("Error setting position in apt api");
    for (lpos = tpos = start_pos, i = 0; fabs(lpos - zpos) > 0.01 && i < 512;i++)
      {
	psleep(20);
	j = apt_api_GetMotPosition(&tpos);
	if (j == 0) lpos = tpos;
      }
    win_printf("reached target %g after %d iter",zpos,i); 
    if (fabs(lpos - zpos) > 0.01)
      return win_printf_OK("Error in setting position from apt api\n");
#endif	  
    

    set_im_title(oim,"Going to %g from %g",start_pos - bef_dz + dz,start_pos - bef_dz);

  
    refresh_image(imr, UNCHANGED);
    
    smart_map_pixel_ratio_of_image_and_screen(oim);
    set_zmin_zmax_values(oim, 0, 255);
    set_z_black_z_white_values(oim, 0, 255);
    oim->im.src_parameter[0] = xc;
    oim->im.src_parameter[1] = yc;
    oim->im.src_parameter[2] = w;
    oim->im.src_parameter[3] = h;
    oim->im.src_parameter[4] = start;
    oim->im.n_f = 0;
    
    oim->oi_idle_action = oiuEye_idle_action;


#ifdef XV_WIN32  
    j = MOT_SetVelParams(plSerialNum[i_motor], pfMinVel, pfAccn, pfMaxVel);
    if (j) return win_printf_OK("Error setting velocity params\n"
				"on Cube %d with serial # %d\n"
				,i_motor,(int)plSerialNum[i_motor]);
#elif defined XV_UNIX
    j = do_apt_api_setVvelocity_mm_s(pfMinVel, pfAccn, pfMaxVel);
    if (j) return win_printf_OK("Error setting velocity params apt api\n");
#endif  
  
    grab_track_image_z_value = grab_apt_z_value;
    
    op = create_and_attach_op_to_oi(oim, nf, nf, 0, 0);
    if (op == NULL) return win_printf_OK("cannot create plot");    
    
    for (i = WIN_CANCEL; i != WIN_OK; i = win_printf("Press OK to start"));
    zpos = start_pos - bef_dz + dz;
#ifdef XV_WIN32  
    j = MOT_MoveAbsoluteEx(motor_id, zpos, 0); // wait
    if (j) return win_printf_OK("Error setting position off Cube %d with serial # %d\n"
				,i_motor,(int)motor_id);
#elif defined XV_UNIX
    j =  apt_api_SetMotPosition(zpos);
    if (j) return win_printf_OK("Error setting position in apt api");
#endif    
    data_ptr = (void*)oim;
    track_image = copy_partial_image_in_movie;
    
    //find_zmin_zmax(oim);
    
    ois->need_to_refresh |= ALL_NEED_REFRESH;
    return refresh_image(imr, imr->n_oi - 1);
 }


int do_init_APT_and_xvuEye(void)
{
    if(updating_menu_state != 0)
      {
	if (g_context == NULL) active_menu->flags &= ~D_DISABLED;
	else active_menu->flags |=  D_DISABLED;
	return D_O_K;
      }    
  
    do_thorlabs_APT_start();
    return do_xvuEye_init_rolling_buffer_movie_xvin();
}


MENU *magcuti_image_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	//add_item_to_menu(mn,"average profile", do_magcuti_average_along_y,NULL,0,NULL);
	//add_item_to_menu(mn,"image z rescaled", do_magcuti_image_multiply_by_a_scalar,NULL,0,NULL);
	add_item_to_menu(mn,"Init APT and uEye movie", do_init_APT_and_xvuEye,NULL,0,NULL);
	add_item_to_menu(mn,"chg Freq", do_set_camera_freq,NULL,0,NULL);
	add_item_to_menu(mn,"chg exposure", do_set_camera_exposure,NULL,0,NULL);

	add_item_to_menu(mn,"chg gain", do_set_camera_gain,NULL,0,NULL);
	add_item_to_menu(mn,"chg binning", do_set_camera_binning,NULL,0,NULL);
	add_item_to_menu(mn,"chg black level", adjust_black_level,NULL,0,NULL);

	add_item_to_menu(mn,"stop uEye movie", do_stop_camera,NULL,0,NULL);
	add_item_to_menu(mn,"freeze uEye movie", do_freeze_camera,NULL,0,NULL);
	add_item_to_menu(mn,"live uEye movie", do_live_camera,NULL,0,NULL);
	add_item_to_menu(mn,"Adjust AOI origin", adj_AOI_position,NULL,0,NULL);
	add_item_to_menu(mn,"Read GPIO 1",do_read_gpio ,NULL,0,NULL);

	add_item_to_menu(mn, "\0",  NULL,   NULL,   0, NULL);    
	add_item_to_menu(mn,"start APT", do_thorlabs_APT_start,NULL,0,NULL);
	add_item_to_menu(mn,"stop APT", do_thorlabs_APT_stop,NULL,0,NULL);
	add_item_to_menu(mn,"read motor position", do_thorlabs_APT_read_MOT_pos,NULL,0,NULL);
	add_item_to_menu(mn,"set motor position", do_thorlabs_APT_set_MOT_pos,NULL,0,NULL);
	add_item_to_menu(mn,"set motor speed",do_set_APT_velocity_mm_s,NULL,0,NULL);	
	add_item_to_menu(mn,"Inc Z",do_small_inc_MOT_pos ,NULL,0,NULL);
	add_item_to_menu(mn,"dec Z",do_small_dec_MOT_pos ,NULL,0,NULL);	
#ifdef XV_WIN32		
	add_item_to_menu(mn,"set motor position 2", do_thorlabs_APT_set_MOT_pos_relative,NULL,0,NULL);
	add_item_to_menu(mn,"set motor home", do_thorlabs_APT_set_MOT_home,NULL,0,NULL);
	add_item_to_menu(mn,"get motor home", do_thorlabs_APT_get_MOT_home,NULL,0,NULL);
#endif
	add_item_to_menu(mn, "\0",  NULL,   NULL,   0, NULL);    
	add_item_to_menu(mn,"Record focus scan",do_magcuti_grab_images_vs_focus ,NULL,0,NULL);
	add_item_to_menu(mn,"Record focus scan 0",do_magcuti_grab_images_vs_focus_0 ,NULL,0,NULL);
	add_item_to_menu(mn,"Record focus scan on the fly",grab_partial_image_im_movie ,NULL,0,NULL);



        /*
	add_item_to_menu(mn,"Zoom in",do_zoom_in_im ,NULL,0,NULL);
	add_item_to_menu(mn,"Zoom out",do_zoom_out_im ,NULL,0,NULL);
	add_item_to_menu(mn,"Move right",do_zoom_mov_x_r_im ,NULL,0,NULL);
	add_item_to_menu(mn,"Move left",do_zoom_mov_x_l_im ,NULL,0,NULL);
	add_item_to_menu(mn,"Move up",do_zoom_mov_y_u_im ,NULL,0,NULL);
	add_item_to_menu(mn,"Move down",do_zoom_mov_y_d_im ,NULL,0,NULL);
        */

	return mn;
}

int	magcuti_main(int argc, char **argv)
{
  (void)argc;
  (void)argv;

	add_image_treat_menu_item ( "magcuti", NULL, magcuti_image_menu(), 0, NULL);
	return D_O_K;
}

int	magcuti_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;

	remove_item_to_menu(image_treat_menu, "magcuti", NULL, NULL);
	return D_O_K;
}
#endif

