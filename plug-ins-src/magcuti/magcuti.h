#ifndef _MAGCUTI_H_
#define _MAGCUTI_H_

PXV_FUNC(int, do_magcuti_average_along_y, (void));
PXV_FUNC(MENU*, magcuti_image_menu, (void));
PXV_FUNC(int, magcuti_main, (int argc, char **argv));
#endif

