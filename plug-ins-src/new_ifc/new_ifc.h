# ifndef NEW_IFC_H
# define NEW_IFC_H

#define P2V_BOARD			1
#define PCD_BOARD			2
#define PCLink_BOARD		3
#define ICP_BOARD			4 
#define FRAMES_IN_HOSTBUFF 16
#define NUM_MAX_BOARDS 	   10


#define SAVE2FILE           1
#define PLAY				1
#define	PLAYBACK			-1
#define STOP				0
#define JUMP				2
#define NEXT_FRAME		    3
#define	OPEN				1
#define CLOSE				0
#define PREVIOUS_FRAME		4

# ifndef NEW_IFC_C
PXV_VAR(int, board_type);
PXV_VAR(pCICapMod, capmod);
PXV_VAR(pCICamera, cam);
PXV_VAR(CAM_ATTR, attr);
PXV_VAR(HIFCGRAB, grabID);
PXV_VAR(pCImgConn, pImg_Conn);
PXV_VAR(pCImgSink, pImg_Sink);
PXV_VAR(int, number_of_board_detected); 
PXV_VAR(int, num_board_detected);
PXV_VAR(int, board_list[NUM_MAX_BOARDS]);
PXV_VAR(int,list_board[NUM_MAX_BOARDS]);
PXV_VAR(int,nframes_in_buffer);
PXV_VAR(GRAB_EXT_ATTR,ExtendedAttr);
PXV_VAR(int , previous_board);
PXV_VAR(int , previous_image_for_saving);
PXV_VAR(IFC_GRAB_STATS, stats);

# else
int num_image_IFC = 0;
int previous_image_for_saving = 0;
GRAB_EXT_ATTR ExtendedAttr;
int nframes_in_buffer = 64;
int board_type =0 ;
pCICapMod capmod;
pCICamera cam;
CAM_ATTR attr;
HIFCGRAB grabID;
pCImgConn pImg_Conn;
pCImgSink pImg_Sink;
int number_of_board_detected = 1;
int num_board_detected = 0;
int board_list[NUM_MAX_BOARDS];
int list_board[NUM_MAX_BOARDS] = {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};
int previous_board = P2V_BOARD;
IFC_GRAB_STATS stats;


# endif
PXV_FUNC(int,do_load_im,(void));
PXV_FUNC(int,tape_play_simulation,(imreg *imr,O_i *oi));
PXV_FUNC(int,tape_function,(int *speed,int *previous_action));
PXV_FUNC(int,live_video,(int mode));
PXV_FUNC(int, measure_aquisition_period_in_ms,(O_i *oi));

PXV_FUNC(int, delete_IFC_stuff,(void));
PXV_FUNC(int, resize_image,(int i, imreg *imr,unsigned char **buf));
PXV_FUNC(int, set_all_IFC_for_aquisition,(int j,imreg *imr,HWND hWnd));
PXV_FUNC(int, IFC_live,(void));
PXV_FUNC(int, build_list_board,(int i,char *module_name));
PXV_FUNC(int, list_number_of_board_detected,(void));
PXV_FUNC(MENU *,IFC_image_menu,(void));
PXV_FUNC(int, newifc_main,(int argc, char **argv));
PXV_FUNC(int, wait_next_frame,(O_i *oi, int *fr_nb, int *fr_time));
PXV_FUNC(int,  freeze_video,(void));

# endif








	
