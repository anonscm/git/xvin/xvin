# ifndef NEW_IFC_C
# define NEW_IFC_C

# include "allegro.h"
# include "winalleg.h"
# include "ifcapi.h"
# include <windowsx.h>
# include "ctype.h"

# include "xvin.h"
# define BUILDING_PLUGINS_DLL
# include "new_ifc.h"
# ifndef NEW_IFC
# define NEW_IFC
# endif
#define SAVE


XV_ARRAY(MENU,  project_menu);
XV_FUNC(int, do_quit, (void));

int change_mouse_to_rect(int cl, int cw);
int reset_mouse_rect();
int aquisition_period = 0;
int is_live = 0;

int test_memory_available(void)
{	char *string=NULL;
	ULARGE_INTEGER  FreeBytesAvailable   ,TotalNumberOfBytes  ,TotalNumberOfFreeBytes ;

	if(updating_menu_state != 0)	return D_O_K;
	
    GetDiskFreeSpaceEx(string,&FreeBytesAvailable,&TotalNumberOfBytes,&TotalNumberOfFreeBytes);
    
    win_printf("FreeBytesAvailable %Ld \nTotalNumberOfBytes%Ld \n TotalNumberOfFreeBytes%Ld ",FreeBytesAvailable.QuadPart,TotalNumberOfBytes.QuadPart,TotalNumberOfFreeBytes.QuadPart);
    //else 
    return 0;
 }

 /* LPCTSTR lpDirectoryName,
  PULARGE_INTEGER lpFreeBytesAvailable,
  PULARGE_INTEGER lpTotalNumberOfBytes,
  PULARGE_INTEGER lpTotalNumberOfFreeBytes*/
  
 /* BOOL DeviceIoControl(
  (HANDLE) hDevice,              // handle to device
  IOCTL_DISK_GET_LENGTH_INFO,    // dwIoControlCode
  NULL,                          // lpInfer
  0,                             // nInBufferSize
  (LPVOID) lpOutBuffer,          // output buffer
  (DWORD) nOutBufferSize,        // size of output buffer
  (LPDWORD) lpBytesReturned,     // number of bytes returned
  (LPOVERLAPPED) lpOverlapped    // OVERLAPPED structure
);*/



int open_close_trigger(void)
{	int i;

	if(updating_menu_state != 0)	return D_O_K;
	/*check camlink is on*/
	if (list_board[PCLink_BOARD] == -1)
    {	return win_printf("CamlinK board is not active!");
    }
    
	i = RETRIEVE_MENU_INDEX ;
	//win_printf("i=%d \n(WORD)i %d",i,(WORD)i);
	CICapMod_OutportVal(capmod,(WORD)i,LNK_TTL0_IO_CHAN); 
	return 0;
}



int wait_n_millisecond(unsigned long time)
{
	DWORD time_in;
 	
  	time_in  = CIFCOS_GetSystimeMillisecs();
  	while ((CIFCOS_GetSystimeMillisecs()-time_in) < time);
   	return 0;
	
}


int play_movie_real_time(void)
{
	O_i *oi = NULL;
	imreg *imr = NULL;
	
	
	if(updating_menu_state != 0)	return D_O_K;
 
  
  
  	do_load_movie_on_disk();
  	broadcast_dialog_message(MSG_DRAW,0);
		

    imr = find_imr_in_current_dialog(NULL);//si squeeze raccourci! Il faudra alors passer aussi imr en parametre
	if (imr == NULL)	return FALSE;
	
	select_image_of_imreg_and_display(imr,imr->n_oi - 1);
	
	
	oi = imr->one_i ;
	win_printf("filename %s",oi->filename);
 
  		
	

	tape_play_simulation(imr,oi);
	return 0;
  	
}
	

			
int find_present_automatic_filenumber(char *file)
{
	int file_number,nchar;
	
	nchar= strlen(file);
	file_number = atoi(file+(nchar-7));
	
	return file_number;
}

char *find_present_automatic_string_filename(char *file)
{	char *common_name=NULL;
	int nchar = 0;
	
	nchar= strlen(file);
	common_name = (char *) calloc(nchar-6,sizeof(char));
	common_name = strncpy(common_name,file,nchar-7);
	return common_name;
}
	



int play_from_disk(O_i *oi, int speed, int *image_number, int action)
{
	char *common_name;
	static int file_number;
	char fullfilename[128],filenumb[128],file[128];
	
	
	
	fullfilename[0]='\0';
	file[0]='\0';
	filenumb[0]='\0';
 	
 	//wait_n_millisecond(5);
 	if (*image_number > (abs(oi->im.n_f)-1)) 
  		{	
    		common_name = find_present_automatic_string_filename(oi->filename);
    		
    		file_number = find_present_automatic_filenumber(oi->filename);
    		//win_printf("file_number %d \n o-inf %d",file_number,oi->im.n_f);
    		file_number ++;
    		
    	    
    	    strcat( fullfilename,oi->dir);         	
         	strcat(file,common_name);
         	
         	sprintf(filenumb,"%04d.gr",file_number);
         	strcat(file,filenumb);
         	strcat(fullfilename,file);
         	
          	if (imreadfile(oi, fullfilename) == MAX_ERROR) 
           	{
            	win_printf("patate");
      			return -3;
   			}
		   oi->filename = strdup(file);
		   set_oi_t_unit_set(oi, oi->n_tu-1);
		    *image_number = 0;
    		return switch_frame(oi,*image_number); 
    		
    		
 	
  		}
	else if (*image_number < 0) 
  		{
    		common_name = find_present_automatic_string_filename(oi->filename);
    		file_number = find_present_automatic_filenumber(oi->filename);
    		file_number --;
    		
    	    strcat( fullfilename,oi->dir);         	
         	strcat(file,common_name);
         	sprintf(filenumb,"%04d.gr",file_number);
         	strcat(file,filenumb);
         	strcat(fullfilename,file);
         	
    		if (imreadfile(oi, fullfilename) == MAX_ERROR) {win_printf("patate");
      														return -3;
      														}
	        //oi->filename = strdup(file);
	        oi->filename = strdup(file);
	        //win_printf("Filename %s \n num %d \n image number %d",backslash_to_slash(oi->filename),oi->im.n_f,*image_number);
    		*image_number = abs(oi->im.n_f) -1;
    		set_oi_t_unit_set(oi, oi->n_tu-1);
    		return switch_frame(oi,*image_number); 
 	
  		}
	else {	set_oi_t_unit_set(oi, oi->n_tu-1);
 			//win_printf("file_number %d \n oi->nf %d",file_number,oi->im.n_f);
    		return switch_frame(oi,*image_number); 
  		}

}

int tape_play_simulation(imreg *imr,O_i *oi)
{
	int speed = 1;
	int action = PLAY;
	int out_variable = 0;
	int image_number = 0;
	int playfromdisk = 0;
	char question[128];
	int /*jump_frame = 1,*/desired_image_number;
	unsigned long frame_time = 0 ;
	
	if (oi == NULL) return win_printf("Oi NULL!!!!");
	set_oi_t_unit_set(oi, oi->n_tu-1);
	
	while (out_variable != -1)
   {
        if (keypressed() == TRUE)     out_variable = tape_function(&speed,&action);
        
        if ((action != STOP) || (out_variable != -1))
                {
                			if (action == PREVIOUS_FRAME) image_number-=speed;
                           if (action == JUMP)
                           {
                                      sprintf(question,"This movie has %d images \n ",oi->im.n_f);   
                                      strcat(question,"You want to jump to image number %d ");
                                                 
                                                                      
                                      win_scanf(question,&desired_image_number);
                                      if (desired_image_number > (abs(oi->im.n_f)-1) || desired_image_number < 0)
                                      {
                                                             action = PLAY;
                                                             win_printf("You do not understand!");
                                      }
                                      else
                                      {
                                          image_number = desired_image_number;
                                          action = STOP;
                                      }
                           }
                           playfromdisk = play_from_disk(oi,speed,&image_number,action);
                           refresh_image(imr, UNCHANGED);
                           
                           //win_printf("oi->im.time+image_number*(unsigned long)(oi->tu[oi->n_tu-1]->dx) %d",oi->im.time+image_number*(unsigned long)(oi->tu[oi->n_tu-1]->dx));
                           frame_time = oi->im.time+image_number*((oi->tu[oi->n_tu-1]->dx)/1000);
                           draw_bubble(screen,0,550,125,"Image %d ",image_number);
                           draw_bubble(screen,0,550,75,"filename %s ",oi->filename);
                           
                           draw_bubble(screen,0,550,150," Time %s,",ctime(&frame_time));
                           
                           
                           if (playfromdisk == -3) return win_printf("Filename not found sequence finished");
                           else if (playfromdisk != image_number) return win_printf("Reading problem!");
                           if (action == PLAY || action == NEXT_FRAME) image_number+=speed;
                           else if (action == PLAYBACK) image_number-=speed;
                           if (action == NEXT_FRAME) action = STOP;
                           if (action == PREVIOUS_FRAME) action = STOP;
                }
   }
    return 0;

}

int tape_function(int *speed,int *previous_action)
{
	
		
	
	switch(readkey()>>8) 
	{
      				
	           case  KEY_SPACE:
	      				
                               if (*previous_action == STOP) 
                               {
                			      	*previous_action = PLAY;
                			      	*speed = 1;
                			       	break;
                				
  			      	           } 
      			      	       if (*previous_action == PLAY) 
      			      	       {
                   			    	*previous_action = STOP;
                   			    	*speed = 1;
                       			    break;
                			   }
                			   if (*previous_action == PLAYBACK) 
                			   {
                   			    	*previous_action = STOP;
                   				    *speed = 1;
                       			    break;
                			   }
                			   break;
                   			
          		case KEY_F:	
              				if (*previous_action == PLAY)
                  			{
                     			*speed=(*speed)*4%33;

                  			}
                 			break;
          				
          		case KEY_B :
          					if (*previous_action == PLAYBACK)
          					{
                  				*speed=(*speed)*4%33;
 
              				}
              				else if (*previous_action == PLAY ||*previous_action == STOP) 
              				{	
                  				*speed = 1;
 
      				            *previous_action = PLAYBACK;
  				            }
  				             
  				            
              			              				
              				break;
               case KEY_N     : if (*previous_action == PLAY)
               					{                
        						               					*previous_action = NEXT_FRAME;
        						               					*speed = 1;
                                }
                                else if (*previous_action == STOP)
                                {
                                		*previous_action = NEXT_FRAME;
                                		*speed = 1;
                                }
                                break;
               case KEY_P     : if (*previous_action == PLAY)
               					{                
        						               					*previous_action = PREVIOUS_FRAME;
        						               					*speed = 1;
                                }
                                else if (*previous_action == STOP)
                                {
                                		*previous_action = PREVIOUS_FRAME;
                                		*speed = 1;
                                }
                                break;

               case KEY_J     :                 
             					*previous_action = JUMP;
                                *speed = 1;
                                 break;

              /* case KEY_A     :                 
           				       *previous_action = PLAY_AVERAGED;
                               *speed = 1;
                                break;*/
              			
              	case KEY_S:
              				return -1;
              				break;
  			
  			
     }  



     return 0;

}



int delete_IFC_stuff(void)
{
	IFC_IfxDeleteImgConn(pImg_Conn);
   	IFC_IfxDeleteCaptureModule(capmod);
   	return 0;
}

int resize_image(int i, imreg *imr, unsigned char **buf)
{	
	int k, j, nf, ny;
	O_i *oi;
	void *buf1;
	

	if (num_image_IFC != imr->cur_oi)	select_image_of_imreg_and_display(imr,num_image_IFC);

	oi = imr->one_i;
 	nf = imr->one_i->im.n_f = nframes_in_buffer+1;
 	num_image_IFC = imr->cur_oi;
   	//set_oi_source(imr->one_i, "IFC aquisition frame")
 	
	
	
  	if (attr.dwBytesPerPixel == 1)
         alloc_one_image (imr->one_i, attr.dwWidth, attr.dwHeight, IS_CHAR_IMAGE);
    else if (attr.dwBytesPerPixel == 2)
         alloc_one_image (imr->one_i, attr.dwWidth, attr.dwHeight, IS_UINT_IMAGE);
    else if (attr.dwBytesPerPixel == 3)
         alloc_one_image (imr->one_i, attr.dwWidth, attr.dwHeight, IS_RGB_PICTURE);
    ny = attr.dwHeight;
    for (k = 0,  *buf = imr->one_i->im.mem[0]; k < imr->one_i->im.n_f; k++)
    {
    	switch_frame(imr->one_i,k);
    	for (j = 0, buf1 = imr->one_i->im.mem[k]; j < attr.dwHeight; j++)
                  	imr->one_i->im.pixel[attr.dwHeight -1 -j].ch = buf1 + j * attr.dwWidth*attr.dwBytesPerPixel;

   	}
   	set_oi_source(imr->one_i, "IFC");
                  	
/*                  	
	buf1 = oi->im.mem[0];
	for (i=0 ; i< ny*nf ; i++)
	{
		switch (type)
		{
			case IS_CHAR_IMAGE:
			oi->im.pixel[ny -1-i].ch = (unsigned char*)buf1;
			break;
			case IS_RGB_PICTURE:
			oi->im.pixel[ny -1-i].ch = (unsigned char*)buf1;
			break;			
			case IS_UINT_IMAGE:
			oi->im.pixel[ny -1-i].ui = (unsigned short int *)buf1;
			break;
		};
		if ( (i % ny == 0) && (oi->im.n_f > 0))
		  {
			oi->im.pxl[i/ny] = oi->im.pixel + i;
		  }
		buf1 += nx * data_len;
	}                  	
*/                  	
                  	
                  	
                  	

    switch_frame(imr->one_i,0);
    if ((attr.dwWidth <1024) && (attr.dwHeight<1024))
    {
    	
    	imr->one_i->width  = ((float)attr.dwWidth)/512;
    	imr->one_i->height = ((float)attr.dwHeight)/512;
    }
    else
    {
    	imr->one_i->width  = ((float)attr.dwWidth)/1024;
    	imr->one_i->height = ((float)attr.dwHeight)/1024;
    }
    
    broadcast_dialog_message(MSG_DRAW,0);
    return 0;
}

BOOL what_to_do_before_display(void * parameter)
{	
	imreg *imr;
	O_i *oi = NULL;
	//static int i = 0;

    imr = find_imr_in_current_dialog(NULL);//si squeeze raccourci! Il faudra alors passer aussi imr en parametre
	if (imr == NULL)	return FALSE;
	oi = imr->one_i ;
	CICamera_GetGrabStats(cam,grabID,&stats);
 	memcpy(oi->im.mem[(stats.CurrentFrameSeqNum) %nframes_in_buffer+1],oi->im.mem[0],attr.dwHeight*attr.dwWidth*attr.dwBytesPerPixel);/*!= oi->im.mem[0]) return FALSE*/
	/*i++;
	if ((i%25) == 0) CICapMod_OutportVal(capmod,1,LNK_TTL0_IO_CHAN); 
	if ((i+12)%25 == 0) CICapMod_OutportVal(capmod,0,LNK_TTL0_IO_CHAN); */
	//draw_bubble(screen,0,550,10,"Image%d",stats.CurrentFrameSeqNum %nframes_in_buffer+1);

	return TRUE;

}


int set_all_IFC_for_aquisition(int j, imreg *imr, HWND hWnd)
{
	pCITIMods itimod;
	ITI_PARENT_MOD mod;
	char cam_file[128];	
	int i;
	float scx, scy;
	unsigned char * buf;
	BOOL what_to_do_before_display(void *received_frame_numb);
	
	
	if (j > number_of_board_detected) 
 		return win_printf_OK("You ask too much young Jedi!");
	
	itimod = IFC_IfxCreateITIMods();
	for (i = 0; i <= j; i++)
	{
    	if (i==0) CITIMods_GetFirst(itimod,&mod);
    	else CITIMods_GetNext(itimod,&mod);
    	
 	}
    
	sprintf(cam_file,"c:\\Program Files\\Xvin\\bin\\%scam.txt",mod.name);	
	//	win_printf("Name %s\n cam_file %s",mod.name,cam_file);
		


    if (!(capmod=IFC_IfxCreateCaptureModule(mod.name,0,cam_file))) //prevoir 2 cartes identiques
   	{
		win_printf("No Image Capture Module detected");
		exit(0);
	}
	
	//win_printf("hello patate");
 	cam = CICapMod_GetCam(capmod,0);
	if 	(cam == NULL) win_printf("Bad camera!");
 	//win_printf("file=%s",backslash_to_slash(cam_file));	
   	CICamera_GetAttr(cam,&attr,TRUE);
   	//win_printf("%s",CICamera_GetFirstCamType(cam));
   	resize_image(i,imr,&buf);
   	//win_printf("exit resize");
   	imr->one_i->filename = "IFC";
   	imr->one_i->need_to_refresh |= ALL_NEED_REFRESH;
   	
   	broadcast_dialog_message(MSG_DRAW,0);
   	pImg_Conn = IFC_IfxCreateImgConn_HostBuf(buf, attr.dwWidth, attr.dwHeight, (attr.dwBytesPerPixel == 2)?10:8,hWnd, IFC_MONO,
		IFC_LIVE_IMAGE/*IFC_DIB_SINK*/ ,cam, ICAP_INTR_EOF,1,&what_to_do_before_display,(void *) &ExtendedAttr);
	if 	(pImg_Conn == NULL) win_printf("Bad connection!");
   	pImg_Sink = CImgConn_GetSink(pImg_Conn);	
	if 	(pImg_Sink == NULL) win_printf("Bad Sink!");
	scx = (float)(get_oi_horizontal_extend(imr->one_i)*512)/attr.dwWidth;
	scy = (float)(get_oi_vertical_extend(imr->one_i)*512)/attr.dwHeight;
    CImgSink_SetAoiPos(pImg_Sink,imr->x_off , imr->y_off - scy*attr.dwHeight+15);
    CImgSink_SetZoom(pImg_Sink,scx,scy);
    
    aquisition_period = measure_aquisition_period_in_ms(imr->one_i);
     //win_printf("imr->one_i->n_tu %d",imr->one_i->n_tu);
    if (create_and_attach_unit_set_to_oi (imr->one_i,IS_SECOND, 0, (float)aquisition_period, -3, 0, "ms", IS_T_UNIT_SET) == NULL) 
    	win_printf("Failed");
    set_oi_t_unit_set(imr->one_i, imr->one_i->n_tu-1);
    //win_printf("imr->one_i->n_tu %d",imr->one_i->n_tu);
    //win_printf("oi->dx %f\n aquisition period %d",imr->one_i->tu[imr->one_i->c_tu ]->dx,aquisition_period);
    
    
    
    live_video(0);
    
    //freeze_video();
    return 0;
}   

int measure_aquisition_period_in_ms(O_i *oi)
{
	double period[10];
	DWORD acquiredDy;
	GRAB_EXT_ATTR ExtendedAttr;
	int i;
	double avg_time = 0;
	double t = 0;
	
	grabID = CICamera_Grab_HostBufEx(cam,0,oi->im.mem[0],1 ,IFC_INFINITE_FRAMES, 0, 0, attr.dwWidth, attr.dwHeight);
	for (i=0; i<10 ; i++)
	{
		CICamera_GrabWaitFrameEx(cam,grabID,oi->im.mem[0], IFC_WAIT_NEWER_FRAME,50,FALSE, &acquiredDy ,&ExtendedAttr );
		
		period[i] = ExtendedAttr.ArrivalTime;
		//draw_bubble(screen,0,550,75,"t %g ExtendedAttr.ArrivalTime %g period[i] %g",ExtendedAttr.ArrivalTime-t,ExtendedAttr.ArrivalTime,period[i]);
		t = ExtendedAttr.ArrivalTime;
	}
	for (i=0; i < 9; i++)
	{
		avg_time += (period[i+1]-period [i]);
	
	}
	
	return (int)(avg_time/i/1000);
}

int save_tape_mode(void)
{
	FILE *fp_images = NULL;
	int n_images_saved = 1;
	HWND hWnd;
	imreg *imr;
	O_i *oi;
    unsigned char *buf;
    int num_files = 0;
    
    char filenamend[16];
    char common_name[20]="Yokota";
    int previous_grab_stat = 0;  
   
    char fullfile[512];
    register int i;
	char path[512], file[256], *pa; 
	int n_images_to_be_saved = 0;
    
    if(updating_menu_state != 0)	return D_O_K;
	
    hWnd = win_get_window();
    imr = find_imr_in_current_dialog(NULL);
	if (imr == NULL)	return 1;
	
	if (num_image_IFC != imr->cur_oi)	select_image_of_imreg_and_display(imr,num_image_IFC);
	
	movie_buffer_on_disk = 1;
		
	switch_allegro_font(1);
	pa = (char*)get_config_string("IMAGE-GR-FILE","last_saved",NULL);	
	if (pa != NULL)		extract_file_path(fullfile, 512, pa);
	else				my_getcwd(fullfile, 512);
	strcat(fullfile,"\\");//Jusqu'ici c'est le path 
		
		
	num_files = get_config_int("TAPE_SAVE_MODE","filenumber",-1);
	win_scanf("Characteristic string for the files? %s\n Initial number for file%d?",common_name,&num_files);
		
  	strcat(fullfile,common_name);//on rajoute le debut du nom specifique
		
  	sprintf(filenamend,"%04d.gr",num_files);//on rajoute le nombre a incrementer et le format
	strcat(fullfile,filenamend);
    	
				
    i = file_select_ex("Save real time movie", fullfile, "gr", 512, 0, 0);
    switch_allegro_font(0);
    
     if (i != 0) 	
    	{
	    		set_config_string("IMAGE-GR-FILE","last_saved",fullfile);			
	    		extract_file_name(file, 256, fullfile);
	    		extract_file_path(path, 512, fullfile);
	    		strcat(path,"\\");
	    		
		}
	else 
 		{	
 			movie_buffer_on_disk = 1;
   			return 0;
		}
	
	set_config_int("TAPE_SAVE_MODE","filenumber",num_files);
	
	
 	oi = imr->one_i ;
 	
  	
 	
 	buf =  oi->im.mem[0];  	
 	LOCK_DATA( buf , oi->im.nx * oi->im.ny * attr.dwBytesPerPixel * nframes_in_buffer );
	grabID = CICamera_Grab_HostBufEx(cam,0,buf,1 ,IFC_INFINITE_FRAMES, 0, 0, attr.dwWidth, attr.dwHeight);
    if (grabID == NULL)  {
    								movie_buffer_on_disk = 0;
            						win_printf("GrabID failed");
            						return 0;
				         }
    //period = measure_aquisition_period(oi);
	
	
 	while (keypressed()!=TRUE)
 	{   if (mouse_b & 1)
  
        {     
        draw_bubble(screen,0,550,100,"%d",num_files);     
    	/*while (!(mouse_b & 1) )  //start saving with the mouse left button
          { 
                    if (keypressed()==TRUE) return freeze_video();
          } */
    	fullfile[0] = '\0';
    	//file = NULL;
    	num_files = get_config_int("TAPE_SAVE_MODE","filenumber",-1);
    	if (num_files < 0) num_files = 0;
    	sprintf(file,"%s%04d.gr",common_name,num_files++);//on rajoute le nombre a incrementer et le format
		strcat(fullfile,path);
		strcat(fullfile,file);
		
		oi->im.movie_on_disk = 1;
 		oi->dir = strdup(path);
 		oi->filename = strdup(file);
 		oi->im.time = 0;
 		oi->im.n_f= 0;
 		//oi->x_title = common_name;
 		set_image_starting_time(oi);
		
		
		set_image_ending_time (oi);
  		save_one_image(oi, fullfile);/***sauve en tete et les parametres**/
		
		set_config_int("TAPE_SAVE_MODE","filenumber",num_files);
    	//return 0;
    	
     	fp_images = fopen (fullfile, "ab");
    	
    	if ( fp_images == NULL ) 	
    	{        
                win_printf("Cannot open the file!");
                movie_buffer_on_disk = 0;
                return 1;
        }
        
        previous_image_for_saving = 0;
    	CICamera_GetGrabStats(cam,grabID,&stats);
    	previous_grab_stat = stats.CurrentFrameSeqNum; 
    	n_images_saved = 0;
		set_image_starting_time(oi);
        
        while (!(mouse_b & 2))//stop saving with the mouse right button 
        {
        	CICamera_GetGrabStats(cam,grabID,&stats);
        	n_images_to_be_saved = stats.CurrentFrameSeqNum - previous_grab_stat - n_images_saved;
        	
        
        	if (n_images_to_be_saved > 0)
        	{
 		      fwrite (oi->im.mem[(stats.CurrentFrameSeqNum - n_images_to_be_saved)%nframes_in_buffer + 1],attr.dwBytesPerPixel,oi->im.nx * oi->im.ny,fp_images);
 		      n_images_saved++;
 		      draw_bubble(screen,0,150,100,"Currently saving image %d",n_images_saved+1);
       		  /*draw_bubble(screen,0,50,100,"Saved %d",n_images_saved);
 		      draw_bubble(screen,0,550,100,"Image %d",stats.CurrentFrameSeqNum - previous_grab_stat);
 		      draw_bubble(screen,0,175,100,"Image to be saved %d",n_images_to_be_saved);*/
 		      if (n_images_to_be_saved > nframes_in_buffer)
 		      { 
         			freeze_video();
         			return win_printf("One image lost");
      		  }
      		}
   		}
   		
   		fclose(fp_images); 
   		
   		
   		set_image_ending_time (oi);
   		
   		
   		close_movie_on_disk(oi,n_images_saved);
   		}
	}
	movie_buffer_on_disk = 0;
	freeze_video();
    return 0;
}
int start_aquisition_tape(void)
{
	save_tape_mode();
	return 0;
}
int change_nb_images_in_buffer(void)
{
	HWND hWnd;
	imreg *imr;
	O_i *oi;
    unsigned char *buf;
    if(updating_menu_state != 0)	return D_O_K;
    
    win_scanf("How many frames in buffer %d",&nframes_in_buffer);
	
	
    hWnd = win_get_window();
    imr = find_imr_in_current_dialog(NULL);
	if (imr == NULL)	return 1;
	if (num_image_IFC != imr->cur_oi)	select_image_of_imreg_and_display(imr,num_image_IFC);
	oi = imr->one_i ;
 	buf =  oi->im.mem[0];
  	resize_image(previous_board,imr, &buf);
	
   	return 0;	
}

int wait_next_frame(O_i *oi, int *fr_nb, int *fr_time)
{
    DWORD acquiredDy;
	
    int seq_num = 0;
    static double t = 0;
         		
    seq_num = CICamera_GrabWaitFrameEx(cam,grabID,oi->im.mem[0], IFC_WAIT_NEWER_FRAME,50,FALSE, &acquiredDy ,&ExtendedAttr );
    *fr_nb = ExtendedAttr.frameSeqNum;
    *fr_time = ExtendedAttr.ArrivalTime;
    
    if (t!= 0) draw_bubble(screen,0,850,100,"SeqNum %d frame %d Time%g",seq_num,*fr_nb ,ExtendedAttr.ArrivalTime-t);
    t = ExtendedAttr.ArrivalTime;
    
    
    return seq_num;
}

int freeze_video(void)
{
	CICamera_Freeze(cam);  
	return 0;
}
int freeze_video_menu(void)
{
	imreg *imr;
	
    if(updating_menu_state != 0)	return D_O_K;
    is_live = 0;
	CICamera_Freeze(cam);  
	imr = find_imr_in_current_dialog(NULL);  
    imr->one_i->need_to_refresh &= BITMAP_NEED_REFRESH;  
    find_zmin_zmax(imr->one_i);
	refresh_image(imr, UNCHANGED);
	return 0;

}
int live_video(int mode)
{
	HWND hWnd;
	imreg *imr;
	O_i *oi;
    unsigned char *buf;

#ifdef MODIF        
    int seq_num = 0;
    DWORD acquiredDy;
	double t = 0;	
# endif
	int i;

	
	
	
    
    if(updating_menu_state != 0)	return D_O_K;
	
    hWnd = win_get_window();
    imr = find_imr_in_current_dialog(NULL);
	if (imr == NULL)	return 1;

	if (mode != 0)
 	{	
	 	i = RETRIEVE_MENU_INDEX ;
	
		if ((previous_board != i)||(pImg_Conn == NULL))
		{	
 			previous_board = i;
 			delete_IFC_stuff();
 			set_all_IFC_for_aquisition(list_board[i],imr,hWnd);
   			set_config_int("IFC","last_board",i); 			
		}
	}

	
    
    if (num_image_IFC != imr->cur_oi)	select_image_of_imreg_and_display(imr,num_image_IFC);
 	oi = imr->one_i ;
 	oi->filename = "IFC";
 	buf =  oi->im.mem[0]; 
 	
 	LOCK_DATA( buf , oi->im.nx * oi->im.ny * attr.dwBytesPerPixel * nframes_in_buffer );
	grabID = CICamera_Grab_HostBufEx(cam,0,buf,1,IFC_INFINITE_FRAMES, 0, 0, attr.dwWidth, attr.dwHeight);
    if (grabID == NULL) win_printf("GrabID failed");

	//while((keypressed()!=TRUE)) ;
    //CICamera_Freeze(cam);
	return 0;
}

int IFC_live(void)
{
    if(updating_menu_state != 0)	return D_O_K;
    

    is_live  = 1;
	return live_video(1);	
}

int image_ifc_got_mouse(imreg *imr, int xm_s, int ym_s, int mode)
{
	return live_video(0);
}
int image_ifc_lost_mouse(imreg *imr, int xm_s, int ym_s, int mode)
{
	return freeze_video();
}  


int build_list_board(int i,char *module_name)
{
	
	if (strncmp(module_name,"P2V",3) == 0)   		
     	{
      		board_list[i]  = P2V_BOARD;
      		list_board[P2V_BOARD] = i;
        }
	 else if (strncmp(module_name,"PCD",3) == 0)	
  		{
    		board_list[i]  = PCD_BOARD;
    		list_board[PCD_BOARD] = i;
      	}
   	else if (strncmp(module_name,"ICP",3) == 0)	
  		{
    		board_list[i]  = ICP_BOARD;
    		list_board[ICP_BOARD] = i;
      	}
 	else if (strncmp(module_name,"LNK",3) == 0)	
 		{
    		board_list[i]  = PCLink_BOARD;
    		list_board[PCLink_BOARD] = i;
      	}
    else win_printf("Unknown board!");
   	return 0;
}

int list_number_of_board_detected(void)
{
	pCITIMods itimod;
	ITI_PARENT_MOD mod[6];
	int num_board_detected = 0 ;
	int i;
	
	if(updating_menu_state != 0)	return D_O_K;
	
	itimod=IFC_IfxCreateITIMods();
	for (i = 0; i < 6 ; i++)
	{
		if (i == 0)
		{
 			if (CITIMods_GetFirst(itimod,&mod[0])) 
 	 			{
  		 	 			//win_printf("Board name number %d = %s\n ",i,mod[i].name);
  		 	 			build_list_board(i,mod[i].name);
  		 	 			num_board_detected = i+1 ;
 	 			}
  		}
  		else
  		{
    		if (CITIMods_GetNext(itimod,&mod[i] ))
        		{
    	        		//win_printf("Board name number %d = %s\n ",i,mod[i].name);
    	        		build_list_board(i,mod[i].name);
    	    	       	num_board_detected = i+1 ;
 	       	    }
 	    }
    }	
   	if (num_board_detected == 0 ) 
   	{
   		win_printf("No board detected, we leave the program");
   		exit(0);
	}
    IFC_IfxDeleteITIMods(itimod);
	return num_board_detected;
}


int	fill_avg_line_profiles_from_im(O_i *oi, int xc, int yc, int cl, int cw, int *x, O_i *oit)
{
	register int j, i;
	unsigned char *choi;	
	short int *inoi;
	unsigned short int *uioi;
	int ytt, xll, onx, ony, xco, yco, cl2, cw2;
	union pix *pd, *pt;
	
	if (oi == NULL || x == NULL)	return 1;
	onx = oi->im.nx;	ony = oi->im.ny;
	pd = oi->im.pixel;
	pt = oit->im.pixel;
	cl2 = cl >> 1;	cw2 = cw >> 1;
	yco = (yc - cw2 < 0) ? cw2 : yc;
	yco = (yco + cw2 <= ony) ? yco : ony - cw2;
	xco = (xc - cl2 < 0) ? cl2 : xc;
	xco = (xco + cl2 <= onx) ? xco : onx - cl2;
	for(i = 0; i < cl; i++)		x[i] = 0;	
	if (oi->im.data_type == IS_CHAR_IMAGE)
	{
		for(j = 0, ytt = yco - cw2, xll = xco - cl2; j< cw; j++)
		{	
			choi = pd[ytt+j].ch + xll;
			
			for (i = 0; i < cl; i++)
   			{
      			x[i] += (int)choi[i];	
      			pt[j].ch[i] = choi[i];	
   			}
		}
	}
	else if (oi->im.data_type == IS_INT_IMAGE)
	{
		for(j = 0, ytt = yco - cw2, xll = xco - cl2; j< cw; j++)
		{	
			inoi = pd[ytt+j].in + xll;
			for (i = 0; i < cl; i++)
   			{
      			x[i] += (int)inoi[i];
         		pt[j].in[i] = inoi[i];	
   			}
		}
	}
	else if (oi->im.data_type == IS_UINT_IMAGE)
	{
		for(j = 0, ytt = yco - cw2, xll = xco - cl2; j< cw; j++)
		{	
			uioi = pd[ytt+j].ui + xll;
			for (i = 0; i < cl; i++)		
   			{
      			x[i] += (int)uioi[i];
         		pt[j].ui[i] = uioi[i];	
   			}
		}
	}
	else return 1;	
	return 0;		
}



int test_frame(void)
{
	register int i, j;
	int   missed_frame = 0;
	static int onx = 512, ony = 512, line = - 1, col = - 1, width = 1, n_times = 1, dis = 1, per = 16;
	union pix *pd;
	O_i *ois, *oipci;
	O_p *op;
	d_s *ds;
	clock_t start = 0;
	int n_f, wait_min, zi[1024], fr_start,fr_cur;	
	imreg *imrs;
	int fr_time, im_mv;

    if(updating_menu_state != 0)	return D_O_K;


	if (ac_grep(cur_ac_reg,"%im%oi",&imrs,&oipci) != 2)
		return win_printf("cannot find image!");	
    for (i = 0; i < 500; i++)
    	n_f = wait_next_frame(oipci,&fr_start,&fr_time);
				
	return 0;
}


int space_time_icpci_float(void)
{
	register int i, j;
	float *fl, tmp, scx,scy;
	int  times;
	int   missed_frame = 0;
	static int onx = 200, ony = 128, line = 100, col = 50, width = 100, n_times = 1, dis = 1, per = 16, rec_sel_mouse = 1;
	int col2=0, line2=0, col3=0, line3=0;
	union pix *pd;
	O_i *ois, *oipci, *oit;
	O_p *op;
	d_s *ds;
	clock_t start = 0;
	int n_f, wait_min, zi[1024], fr_start,fr_cur;	
	imreg *imrs;
	int fr_time, im_mv, im;
	DIALOG *d;
	int set_z_max_range(void);

	if(updating_menu_state != 0)	return D_O_K;


	if (ac_grep(cur_ac_reg,"%im%oi",&imrs,&oipci) != 2)
		return win_printf("cannot find image!");	
	d = find_dialog_associated_to_imr(imrs, NULL);
	freeze_video();					
	//line = - 1, col = - 1;
	i = win_scanf("grab one line versus time  \n number of pts in x %d,"
		"number of time steps %d at line  %5d at column %5d or select by mouse %b\n"
		"averaging in the y direction over a width %d averaged in time over %d"
		"field period%ddiplay 1->yes 0->no %d",
		&onx,&ony,&line,&col,&rec_sel_mouse,&width,&n_times,&per,&dis);
	if (i == CANCEL)	return OFF;
	
	if (onx < 0 || ony < 0)	return win_printf("can't create negative dimension");
	
	win_printf("Acquisition time:\n %d lines averaged %d times is %f seconds\n%s", 
	ony, n_times, (float) ony*n_times/24, (line<0 || col<0)?"Use mouse to choose area":"");

	if (width < 2 || ((width + ((line >0) ? line : 0)) > oipci->im.ny) )	
		return win_printf("wrong width %d",width);

	ois = create_and_attach_oi_to_imr(imrs, onx, ony, IS_FLOAT_IMAGE);
	if (ois  == NULL) 	return win_printf("cannot create image!");	

   	set_oi_source(ois, "IFC space-time");
	op = create_and_attach_op_to_oi(ois, ony, ony, 0,0);
	if (op == NULL)	return win_printf("cannot create plot!");
	ds = op->dat[0];

	create_attach_select_y_un_to_op(op, IS_SECOND, 0, 1, 0, 0, "s");
	op->type = IM_SAME_Y_AXIS;	

	ois->filename = strdup("spaceti.gr");
	ois->im.source = strdup("CamLINK space time");
	pd = ois->im.pixel;

	oit = create_and_attach_oi_to_imr(imrs, onx, width, oipci->im.data_type);
	if (oit  == NULL) 	return win_printf("cannot create image!");	
   	set_oi_source(oit, "IFC debug");

	select_image_of_imreg(imrs,0);
	oipci->need_to_refresh |= BITMAP_NEED_REFRESH;
	if (d) d->proc(MSG_DRAW, d, 0);		

	live_video(0);

	if (rec_sel_mouse) 			
	{

		scx = (float)(get_oi_horizontal_extend(imrs->one_i)*512)/attr.dwWidth;
		scy = (float)(get_oi_vertical_extend(imrs->one_i)*512)/attr.dwHeight;
/*		win_printf("scx %g scy %g",scx,scy);*/
		change_mouse_to_rect(onx*scx, width*scy);
/*		change_mouse_to_rect(onx, width);		*/
/*	
		set_rectangle_in_mouse_position(&col, &line,  onx*scx, width*scy, imrs);	
		draw_rectangle_vga(col, line, onx+2, width+2, 1, imrs);
		*/
		
		for(; !key[KEY_ENTER]; )
  		{	
    		col = mouse_x;
    		line = mouse_y;
    		col2 = (int)x_imr_2_imdata(imrs, col);	
    		line2 = (int)y_imr_2_imdata(imrs, line);	
    		col3 = col2 - onx/2;
    		line3 = line2 - width/2;
    		draw_bubble(screen,0,850,100,"col %d , line %d, x_off %d, s_nx %d, s_ny %d",col,line, imrs->x_off, imrs->s_nx, imrs->s_ny);
      	}
		col = (int)x_imr_2_imdata(imrs, col);	
		line = (int)y_imr_2_imdata(imrs, line);	
		col -= onx/2;
		line -= width/2;	
		freeze_video();	
   		clear_keybuf();							
		win_printf ("acq at line %d col %d",line,col);
	}
	oipci->need_to_refresh |= BITMAP_NEED_REFRESH;
	if (d) d->proc(MSG_DRAW, d, 0);		
	live_video(0);	
	reset_mouse_rect();

	
	n_f = wait_next_frame(oipci,&fr_start,&fr_time);
	CICamera_GetGrabStats(cam,grabID,&stats);
    im_mv =   stats.CurrentFrameSeqNum%nframes_in_buffer;			
	for (i=im=0, wait_min = 1000, start = clock(); i< ony  ; i++ )
	{
		fl = pd[i].fl;
// modulation du champ
		if (i%per < per/2) 	
		{
			//set_low_control(0x0F);
			ds->xd[i] = 1;
		}
		else
		{
			//set_low_control(0x00);
			ds->xd[i] = 0;
		}

		for (j=0; j< onx ; j++)	zi[j] = 0;
		for (times=0 ; times< n_times ; times++)
		{
			n_f = wait_next_frame(oipci,&fr_cur,&fr_time);
			switch_frame(oipci,1+( (im+ im_mv)%nframes_in_buffer));
			fill_avg_line_profiles_from_im(oipci, col+onx/2, line+width/2, onx, width, zi, oit);
//			fill_from_im(oipci, line+onx/2, col+width/2, onx, width, oit);			
			im++;
		}
		for (j=0; j< onx  ; j++)	fl[j] = (float)zi[j];
		ds->yd[i] =  n_f;
	}
	
	freeze_video();	
	switch_frame(oipci,0);
	find_zmin_zmax(oit);
	set_z_max_range();
	oipci->need_to_refresh |= BITMAP_NEED_REFRESH;
	if (d) d->proc(MSG_DRAW, d, 0);	
	
	start =  clock() - start;
	win_printf("wait min %d \n%d im in %g S\nsoit %g im/s\nmissed frames %d",
		wait_min,ony*n_times,((float)start)/((float)CLOCKS_PER_SEC),
		((float)CLOCKS_PER_SEC*ony*n_times)/start,missed_frame);	
	win_printf("fr start %d  %d fr read",fr_start,fr_cur);
	set_im_title(ois,"space time at line %d, width %d, averaged %d periode %d",
		line,width, n_times,per);
	set_im_x_title(ois, "pixel in x");
	set_im_y_title(ois, "time (%d/25s)",n_times);
	create_attach_select_y_un_to_oi(ois, IS_SECOND, 0, .041666666*n_times, 0, 0, "s");	
	uns_oi_2_oi_by_type(oipci, IS_X_UNIT_SET, ois, IS_X_UNIT_SET);

	for (i=0, tmp = (float)1/(float)(width*n_times); i< ony  ; i++ )
	{
		fl = pd[i].fl;
		for (j=0; j< onx ; j++)	fl[j] *= tmp;
	}

 	time(&(ois->im.time));
	find_zmin_zmax(ois);
	freeze_video();	
	return D_REDRAWME;
}


int change_mouse_to_rect(int cl, int cw)
{
    BITMAP *ds_bitmap = NULL;
    int color, col;


    ds_bitmap = create_bitmap(cl+1,cw+1);
    col = Lightmagenta;
    color = makecol(EXTRACT_R(col), EXTRACT_G(col), EXTRACT_B(col));
    clear_to_color(ds_bitmap, color);

    color = makecol(255, 0, 0);                        

    line(ds_bitmap,0,0,cl,0,color);
    line(ds_bitmap,0,0,0,cw,color);
    line(ds_bitmap,0,cw,cl,cw,color);
    line(ds_bitmap,cl,0,cl,cw,color);
    scare_mouse();    
    set_mouse_sprite(ds_bitmap);
    set_mouse_sprite_focus(cl/2, cw/2);
    unscare_mouse();
    return 0;    
}
int reset_mouse_rect()
{
    scare_mouse();    
    set_mouse_sprite(NULL);
    set_mouse_sprite_focus(0, 0);
    unscare_mouse();
    return 0;
}
int	select_rect(void)
{
	static int cl = 128, cw = 16, i;
	
	if(updating_menu_state != 0)	return D_O_K;	
	i = win_scanf("Rectangle size cl %d cw %d",&cl,&cw);
	
 	if (i == CANCEL)	 return reset_mouse_rect();
 	return change_mouse_to_rect(cl, cw);
}



MENU *IFC_image_menu(void)
{
	static MENU mn[32];
	
	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"Detect Board",  list_number_of_board_detected, NULL ,  0, NULL);
	add_item_to_menu(mn,"Freeze",	freeze_video_menu, NULL ,  0, NULL );
	add_item_to_menu(mn,"Space-time",	space_time_icpci_float, NULL ,  0, NULL );
	//add_item_to_menu(mn,"Select rectangle", select_rect, NULL, 0, NULL);
	add_item_to_menu(mn,"Adjust buffer", change_nb_images_in_buffer, NULL, 0, NULL);
	add_item_to_menu(mn,"Save video tape", save_tape_mode, NULL, 0, NULL);
	add_item_to_menu(mn,"Play video tape", play_movie_real_time, NULL, 0, NULL);
	add_item_to_menu(mn,"Test memory available", test_memory_available, NULL, 0, NULL);
	add_item_to_menu(mn,"Test frame", test_frame, NULL, 0, NULL);
	
	
	if (list_board[P2V_BOARD] != -1) 	
 		add_item_to_menu(mn,"Live P2V",	IFC_live, NULL ,  MENU_INDEX(P2V_BOARD), NULL );
   	if (list_board[PCD_BOARD] != -1)	
    	add_item_to_menu(mn,"Live PCD_BOARD",IFC_live, NULL ,  MENU_INDEX(PCD_BOARD), NULL );
    if (list_board[PCLink_BOARD] != -1)	
    	add_item_to_menu(mn,"Live PCLink",IFC_live, NULL ,  MENU_INDEX(PCLink_BOARD), NULL );
    if (list_board[ICP_BOARD] != -1)	
    	add_item_to_menu(mn,"Live ICP",IFC_live, NULL ,  MENU_INDEX(ICP_BOARD), NULL );
   	if (list_board[PCLink_BOARD] != -1)
    {	
    	add_item_to_menu(mn,"OPEN shutter",open_close_trigger, NULL ,  MENU_INDEX(OPEN), NULL );
    	add_item_to_menu(mn,"CLOSE shutter",open_close_trigger, NULL ,  MENU_INDEX(CLOSE), NULL );
   	}
    
	return mn;
}

int newifc_main(int argc, char **argv)
{
	imreg *imr = NULL;
	HWND hWnd;
	int n;
	
	hWnd = win_get_window();
	
 	imr = create_and_register_new_image_project( 0,   32,  900,  668);
 	if (imr == NULL) win_printf("Patate t'es mal barre!");
   
	number_of_board_detected = list_number_of_board_detected();

	n = get_config_int("IFC","last_board",P2V_BOARD);
  	set_all_IFC_for_aquisition(list_board[n],imr,hWnd);
 	
 	//imr->image_got_mouse = image_ifc_got_mouse;
    //imr->image_lost_mouse = image_ifc_lost_mouse; 	
	
	add_image_treat_menu_item("IFC ", NULL, IFC_image_menu(), 0, NULL);
    broadcast_dialog_message(MSG_DRAW,0);
	return 0;    
}
# ifdef PBO

int main(int argc, char *argv[])
{
    xvin_main(argc, argv);
	newifc_main(argc, argv);	
}

END_OF_MAIN();

# endif	
#undef PB

# endif


