#pragma once
# include "../fft2d/fftbtl32n.h"
//# include "../cfg_file/Pico_cfg.h"
# include "xvin.h"
#include "rawhid-o.h"
#include <pthread.h>

# include <stdbool.h>
# include <unistd.h>
//# include <windows.h>
#include "hdf5.h"

#define PACKET_SIZE 64
#define RAWHID_RAW_BUFFER 128
#define ROLLING_BUFFER_SIZE 256

# define PTR_DATATYPE_INT 0
# define PTR_DATATYPE_FLOAT 1
# define PTR_DATATYPE_LLONG 2

# define N_TEMPERATURE_SENSOR 3

/*
typedef struct rawhid_general_atomic
{
    rawhid_atom *atom_list;
    int n_atom_list;
}general_atom;

//a couple between a ds and a float * to save data
typedef struct rawhid_saving_atomic
{
  d_s *ds;
  float *data;
}rawhid_atom;
//*/
/*
typedef struct rawhid_zmag_data
{

}

typedef struct rawhid_objective_data
{

}__attribute__((__packed__)) objective_data;

typedef struct rawhid_thermalization_data
{
  char error_code;
  char msg_type;
  float P_obj;
  float I_obj;
  float D_obj;

  //float P_mag;
  //float I_mag;
  //float D_mag;

}__attribute__((__packed__)) thermalization_data;
*/
typedef struct rawhid_report_data
{
  char error_code ;
  char msg_type ;
  char T0_status ;
  char T3_status ;
  char zmag_status ;
  char zobj_status ;
  float timestamp ;
  float freq ;
  float T0 ;//thermistance pid tbox
  float T1 ; //thermistante sample
  float T2 ;// temp sink (lm35?)
  float T3 ; //pid magnets
  float T4 ; //magnets
  float zmag ; //zmag
  float vmag ;
  float zobj ;
  float vobj ;
  char remain[18] ;
}__attribute__((__packed__)) report_data;

typedef struct rawhid_report_data_2
{
  unsigned char msg_type;
  unsigned long error_code; // 16 bits : be careful of endianess
  char index_Temperature;   //| command Mask (From DAQ)  2^0
  unsigned long timestamp;  //This is readonly from the DAQ point of view.
  char ZmagObj_status;      //Hi[0 Zmag, 1 Zobj] LOW[0 immobile, 1 moving, 2 arrived, 3 off]  . | command Mask (From DAQ)  2^1
  char xy_status ;          //HI[0 X, 1 Y ] [0 immobile, 1 moving, 2 arrived, 3 off]  | command Mask (From DAQ)  2^2
  float zmag;               //| command Mask (From DAQ)  2^3
  float vmag;               //| command Mask (From DAQ)  2^4
  float zobj;               //| command Mask (From DAQ)  2^5
  float x;                  //| command Mask (From DAQ)  2^6
  float y;                  //| command Mask (From DAQ)  2^7
  float TemperatureTbox;    //Tbox (index_temp 2^0 ), Tsample(index_temp 2^1 ),   | command Mask (From DAQ)  2^8
  float TemperatureMag;     //Tmagnet (index_temp 2^2 ), Tresse (index_temp 2^3 )  | command Mask (From DAQ)  2^9
  float TemperatureSink;    //TSink, Tmagsink, TAir, RH (relative humidity) //| command Mask (From DAQ)  2^10
  float intensity_led1;     //| command Mask (From DAQ)  2^11
  float intensity_led2;     //| command Mask (From DAQ)  2^12
  char remain[10] ;
}__attribute__((__packed__)) report_data_2;


typedef struct rawhid_general_data
{
  //report_data **r_data;
  report_data_2 **r_data;
  int n_report_data;
  int current_report_data;
}rawhid_g_data;

//union for ptr onto data of hdf5 1d data
typedef union hdf5_data_ptr
{
  int *idata;
  float *fdata;
  long long *lldata;
} data_ptr;

typedef struct hdf5_1d_data
{
  hid_t dataset, group, sub_group, file, dataspace;
  char filename[128];
  char datasetname[128];
  char groupname[128];
  char subgroupname[128];
  bool in_group;
  bool in_subgroup;
  hsize_t chunk_dims[1];//how much to allocate when necessary
  //hsize_t write_dims[1];//how much to write_dims
  hsize_t file_dims[1];//current dim of dataset
  hsize_t offset_file[1];
  int t0;
  int write_period;//when to write dims
  int mem_offset;
  int datatype;
  data_ptr data;
} hdf5_1d;

typedef struct hdf5_general
{
  char filename[512];
  hdf5_1d **hdf5_1d_list;
  int n_hdf5_1d;
} hdf5_gen;

//PXV_FUNC(int, do_SDITracking, (void));

PXV_FUNC(int, update_hdf5_1d, (hdf5_1d *hdf5_1d_in, int i, bool open_file, hid_t *file_id, bool close_file));
PXV_FUNC(int, attach_1d_float_to_hdf5_1d, (hdf5_1d *hdf5_1d_in, float *fdata));
PXV_FUNC(int, attach_1d_int_to_hdf5_1d, (hdf5_1d *hdf5_1d_in, int *idata));
PXV_FUNC(int, attach_1d_llong_to_hdf5_1d, (hdf5_1d *hdf5_1d_in, long long *lldata));
PXV_FUNC(int, init_hdf5_1d, (hdf5_1d *hdf5_1d_in, char filename[], char datasetname[], char groupname[], char subgroupname[], int datatype, int chunk_length));
PXV_FUNC(int, remove_one_1d_hdf5_from_manager, (int which_one));
PXV_FUNC(int, do_pause_record, (void));
PXV_FUNC(int, add_one_1d_hdf5_to_manager, (void));
PXV_FUNC(int, interpret_continous_msg_from_micro, (report_data *msg_out, char *msg_in));
PXV_FUNC(MENU*, SDITracking_plot_menu, (void));
PXV_FUNC(int, SDITracking_main, (int argc, char **argv));
