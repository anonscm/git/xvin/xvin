 #include "xvin.h"
 #include "SDITracking.h"
 #include "sdi_graphic.h"

int sdi_keyboard_shortcut(void)
{
  //int val;
  //val=readkey();
  //val= val & 0xff; ASCII of return
  //val = val >> 8; scancode of retunr
  add_keyboard_short_cut(0,  17, 0, do_sdi_display_config);//17 is the scancode for Ctrl-A

  return 0;
}

//this allocates time dependent fields of a fringe object
int allocate_sdi_fringes_t_fields(sdi_fringes *fringes, int buffer_size, int *which_data, int n_which_data)
{
  int i, which_data_i;
  d_s *data=NULL;
  for (i=0;i<n_which_data;i++)
  {
    which_data_i=*(which_data+i);
    switch_ptr_to_sdi_fringes_field(fringes, which_data_i, &data);
    data=build_adjust_data_set(data,buffer_size,buffer_size);
    data->nx=buffer_size;
    data->ny=buffer_size;
  }
  return 0;
}

//this allocates time dependent fields of a beaad object
int allocate_sdi_bead_t_fields(sdi_bead *bead, int buffer_size, int *which_data, int n_which_data)
{
  int i, which_data_i;
  d_s *data=NULL;
  for (i=0;i<n_which_data;i++)
  {
    which_data_i=*(which_data+i);
    switch_ptr_to_sdi_bead_field( bead, which_data_i, &data);
    data=build_adjust_data_set(data,buffer_size,buffer_size);
    data->nx=buffer_size;
    data->ny=buffer_size;
  }
  return 0;
}


 int switch_ptr_to_sdi_bead_field(sdi_bead *bead,int which_data,d_s **ds)
 {
   switch(which_data){
   case KEEP_Z_T :
   if (bead->z_t==NULL)
   {
     bead->z_t=build_adjust_data_set(bead->z_t,1,1);
     bead->z_t->nx=1;
     bead->z_t->ny=1;
   }
   *ds=bead->z_t;
   break;
   case KEEP_Y_T :
   if (bead->y_t==NULL)
   {
     bead->y_t=build_adjust_data_set(bead->y_t,1,1);
     bead->y_t->nx=1;
     bead->y_t->ny=1;
   }
   *ds=bead->y_t;
   break;
   case KEEP_X_T :
   if (bead->x_t==NULL)
   {
     bead->x_t=build_adjust_data_set(bead->x_t,1,1);
     bead->x_t->nx=1;
     bead->x_t->ny=1;
   }
   *ds=bead->x_t;
   break;
   }
   return 0;
 }

 int switch_ptr_to_sdi_y_profile_field(sdi_y_profile *y_profile,int which_data,d_s **ds)
 {
   switch(which_data){
   case KEEP_WINDOWED_Y_PROFILE :
   if (y_profile->windowed_y_profile==NULL)
   {
     y_profile->windowed_y_profile=build_adjust_data_set(y_profile->windowed_y_profile,1,1);
     y_profile->windowed_y_profile=1;
     y_profile->windowed_y_profile=1;
   }
   *ds=y_profile->windowed_y_profile;
   break;
   case KEEP_RAW_Y_PROFILE :
   if (y_profile->raw_y_profile==NULL)
   {
     y_profile->raw_y_profile=build_adjust_data_set(y_profile->raw_y_profile,1,1);
     y_profile->raw_y_profile=1;
     y_profile->raw_y_profile=1;
   }
   *ds=y_profile->raw_y_profile;
   break;
   case KEEP_Y_WINDOW :
   if (y_profile->y_window==NULL)
   {
     y_profile->y_window=build_adjust_data_set(y_profile->y_window,1,1);
     y_profile->y_window=1;
     y_profile->y_window=1;
   }
   *ds=y_profile->y_window;
   break;
   case KEEP_AUTOCONV :
   if (y_profile->y_profile_autoconv==NULL)
   {
     y_profile->y_profile_autoconv=build_adjust_data_set(y_profile->y_profile_autoconv,1,1);
     y_profile->y_profile_autoconv=1;
     y_profile->y_profile_autoconv=1;
   }
   *ds=y_profile->y_profile_autoconv;
   break;
   case KEEP_AUTOCONV_ROI :
   if (y_profile->y_profile_autoconv_roi==NULL)
   {
     y_profile->y_profile_autoconv_roi=build_adjust_data_set(y_profile->y_profile_autoconv_roi,1,1);
     y_profile->y_profile_autoconv_roi=1;
     y_profile->y_profile_autoconv_roi=1;
   }
   *ds=y_profile->y_profile_autoconv_roi;
   break;
   }
   return 0;
 }

 //switch to one field of a fringe object. when it is NULL it is allocated with on point
  int set_current_point_of_sdi_fringes_field(sdi_fringes *fringes,int which_data,int current_i,int current_t)
  {
    switch(which_data){
    case KEEP_NU0_T :
//    case KEEP_ORDER0_T :
//    case KEEP_ORDER1_T :
    fringes->current_i=current_i;
    fringes->current_t=current_t;
    break;
//    case KEEP_ORDER0_T_PROC1 :
//    case KEEP_ORDER1_T_PROC1 :
    case KEEP_CONTRAST_T :
    fringes->current_contrast_i=current_i;
    fringes->current_contrast_t=current_t;
    break;
    case KEEP_I_MAX_T :
    fringes->current_I_max_i=current_i;
    fringes->current_I_max_t=current_t;
    break;
    }
    return 0;
  }

//switch to one field of a fringe object. when it is NULL it is allocated with on point
 int switch_ptr_to_sdi_fringes_field(sdi_fringes *fringes,int which_data,d_s **ds)
 {
   switch(which_data){
   case KEEP_NU0_T :
   if (fringes->nu0_t==NULL)
   {
     fringes->nu0_t=build_adjust_data_set(fringes->nu0_t,1,1);
     fringes->nu0_t->nx=1;
     fringes->nu0_t->ny=1;
   }
   *ds=fringes->nu0_t;
   break;
   case KEEP_ORDER0_T :
   if (fringes->order0_t==NULL)
   {
     fringes->order0_t=build_adjust_data_set(fringes->order0_t,1,1);
     fringes->order0_t->nx=1;
     fringes->order0_t->ny=1;
   }
   *ds=fringes->order0_t;
   break;
   case KEEP_ORDER1_T :
   if (fringes->order1_t==NULL)
   {
     fringes->order1_t=build_adjust_data_set(fringes->order1_t,1,1);
     fringes->order1_t->nx=1;
     fringes->order1_t->ny=1;
   }
   *ds=fringes->order1_t;
   break;
   case KEEP_WINDOWED_X_PROFILE :
   if (fringes->windowed_x_profile==NULL)
   {
     fringes->windowed_x_profile=build_adjust_data_set(fringes->windowed_x_profile,1,1);
     fringes->windowed_x_profile->nx=1;
     fringes->windowed_x_profile->ny=1;
   }
   *ds=fringes->windowed_x_profile;
   break;
   case KEEP_RAW_X_PROFILE :
   if (fringes->raw_x_profile==NULL)
   {
     fringes->raw_x_profile=build_adjust_data_set(fringes->raw_x_profile,1,1);
     fringes->raw_x_profile->nx=1;
     fringes->raw_x_profile->ny=1;
   }
   *ds=fringes->raw_x_profile;
   break;
   case KEEP_X_WINDOW :
   if (fringes->x_window==NULL)
   {
     fringes->x_window=build_adjust_data_set(fringes->x_window,1,1);
     fringes->x_window->nx=1;
     fringes->x_window->ny=1;
   }
   *ds=fringes->x_window;
   break;
   case KEEP_SPECTRUM :
   if (fringes->spectrum==NULL)
   {
     fringes->spectrum=build_adjust_data_set(fringes->spectrum,1,1);
     fringes->spectrum->nx=1;
     fringes->spectrum->ny=1;
   }
   *ds=fringes->spectrum;
   break;
   case KEEP_SPECTRUM_LOG :
   if (fringes->spectrum_log==NULL)
   {
     fringes->spectrum_log=build_adjust_data_set(fringes->spectrum_log,1,1);
     fringes->spectrum_log->nx=1;
     fringes->spectrum_log->ny=1;
   }
   *ds=fringes->spectrum_log;
   break;
   case KEEP_AMPLITUDE :
   if (fringes->amplitude==NULL)
   {
     fringes->amplitude=build_adjust_data_set(fringes->amplitude,1,1);
     fringes->amplitude->nx=1;
     fringes->amplitude->ny=1;
   }
   *ds=fringes->amplitude;
   break;
   case KEEP_AMPLITUDE_ROI :
   if (fringes->amplitude_roi==NULL)
   {
     fringes->amplitude_roi=build_adjust_data_set(fringes->amplitude_roi,1,1);
     fringes->amplitude_roi->nx=1;
     fringes->amplitude_roi->ny=1;
   }
   *ds=fringes->amplitude_roi;
   break;
   case KEEP_PHASE :
   if (fringes->phase==NULL)
   {
     fringes->phase=build_adjust_data_set(fringes->phase,1,1);
     fringes->phase->nx=1;
     fringes->phase->ny=1;
   }
   *ds=fringes->phase;
   break;
   case KEEP_UNWRAPPED_PHASE_ROI :
   if (fringes->unwrapped_phase_roi==NULL)
   {
     fringes->unwrapped_phase_roi=build_adjust_data_set(fringes->unwrapped_phase_roi,1,1);
     fringes->unwrapped_phase_roi->nx=1;
     fringes->unwrapped_phase_roi->ny=1;
   }
   *ds=fringes->unwrapped_phase_roi;
   break;
   case KEEP_RAW_PHASE_ROI :
   if (fringes->raw_phase_roi==NULL)
   {
     fringes->raw_phase_roi=build_adjust_data_set(fringes->raw_phase_roi,1,1);
     fringes->raw_phase_roi->nx=1;
     fringes->raw_phase_roi->ny=1;
   }
   *ds=fringes->raw_phase_roi;
   break;
   case KEEP_ORDER0_T_PROC1 :
   if (fringes->order0_t_proc1==NULL)
   {
     fringes->order0_t_proc1=build_adjust_data_set(fringes->order0_t_proc1,1,1);
     fringes->order0_t_proc1->nx=1;
     fringes->order0_t_proc1->ny=1;
   }
   *ds=fringes->order0_t_proc1;
   break;
   case KEEP_ORDER1_T_PROC1 :
   if (fringes->order1_t_proc1==NULL)
   {
     fringes->order1_t_proc1=build_adjust_data_set(fringes->order1_t_proc1,1,1);
     fringes->order1_t_proc1->nx=1;
     fringes->order1_t_proc1->ny=1;
   }
   *ds=fringes->order1_t_proc1;
   break;
   case KEEP_CONTRAST_T :
   if (fringes->contrast_t==NULL)
   {
     fringes->contrast_t=build_adjust_data_set(fringes->contrast_t,1,1);
     fringes->contrast_t->nx=1;
     fringes->contrast_t->ny=1;
   }
   *ds=fringes->contrast_t;
   break;
   case KEEP_I_MAX_T :
   if (fringes->I_max_t==NULL)
   {
     fringes->I_max_t=build_adjust_data_set(fringes->I_max_t,1,1);
     fringes->I_max_t->nx=1;
     fringes->I_max_t->ny=1;
   }
   *ds=fringes->I_max_t;
   break;
   }
   return 0;
 }
 //remove a display only if it is possible (ie not the last dataset of the last plot of a plot project)
 //otherwise it returns 1
 int remove_one_sdi_display(sdi_disp *display)
 {
   int error,count;
   if (display==NULL) return D_O_K;
   if (display->data ==NULL || display->plr==NULL || display->op==NULL) return D_O_K;
   error=1;
   count=0;
   if (display->op->n_dat>1)
   {
     error=0;
     remove_ds_from_op_without_free_ds(display->op, display->data);
   }
   else
   {
     if (display->plr->n_op>1)
     {
       remove_plot_from_plr_without_free_ds(display->plr,display->op);
       error=0;
     }
   }
   return error;
 }



//like remove_ds_from_op, except that it does not free the ds
//to be used when one ds is plotted in several different plots and then you want to remove one of those plot
int	remove_ds_from_op_without_free_ds(O_p *op, d_s *ds)
{
  int i,found,k;
	if (op == NULL || ds == NULL)	return D_O_K;
  i=0;
  found=0;
  while (i<op->n_dat && found==0)
  {
    found=(int) (ds==op->dat[i]);
    i++;
  }
  op->n_dat--;
  for (k=i-1;k<op->n_dat;k++) op->dat[k] = op->dat[k+1];
  return 0;
}

int remove_plot_from_plr_without_free_ds(pltreg *pr, O_p *op)
{
  int i, found, k;
  if ( pr == NULL ) return D_O_K;
  i=0;
  found=0;
  while (i<pr->n_op && found==0)
  {
    found =(int) (pr->o_p[i]==op);
    i++;
  }
  free_one_plot_without_free_ds(pr->o_p[i-1]);
  pr->n_op--;
  for (k=i-1 ; k< pr->n_op ; k++)  pr->o_p[k] = pr->o_p[k+1];
  pr->one_p = (pr->cur_op >= 0) ? pr->o_p[pr->cur_op] : NULL;
  pr->stack = (pr->cur_op >= 0) ? &(pr->one_p->bplt) : NULL;
  return 0;
}

int free_one_plot_without_free_ds(O_p *op)
{
	int i;
	if (op == NULL) 	return xvin_error(Wrong_Argument);
	for (i=0 ; i< op->n_lab ; i++)
	{
		if (op->lab[i]->text != NULL) 	free(op->lab[i]->text);
		if (op->lab[i] != NULL) 	free(op->lab[i]);
	}
	if (op->lab)	{free(op->lab);	   op->lab = NULL;}
	if (op->filename != NULL)	free(op->filename);
	if (op->dir != NULL)		free(op->dir);
	if (op->title != NULL)		free(op->title);
	if (op->x_title != NULL)	free(op->x_title);
	if (op->y_title != NULL)	free(op->y_title);
	if (op->x_prime_title != NULL)	free(op->x_prime_title);
	if (op->y_prime_title != NULL)	free(op->y_prime_title);
	if (op->y_prefix != NULL)	free(op->y_prefix);
	if (op->x_prefix != NULL)	free(op->x_prefix);
	if (op->t_prefix != NULL)	free(op->t_prefix);
	if (op->x_unit != NULL)		free(op->x_unit);
	if (op->y_unit != NULL)		free(op->y_unit);
	if (op->t_unit != NULL)		free(op->t_unit);
	if (op->xu != NULL)
	{
		for (i=0 ; i< op->n_xu ; i++)	free_unit_set(op->xu[i]);
		free(op->xu);
	}
	if (op->yu != NULL)
	{
		for (i=0 ; i< op->n_yu ; i++)	free_unit_set(op->yu[i]);
		free(op->yu);
	}
	if (op->tu != NULL)
	{
		for (i=0 ; i< op->n_tu ; i++)	free_unit_set(op->tu[i]);
		free(op->tu);
	}
	if (&op->bplt)  free_box(&op->bplt);
	free(op);	op = NULL;
	return 0;
}


// pre-allocated
int sdi_extract_char_subimage_from_char_image(O_i *roi,O_i *src,int roi_xc, int roi_yc, int roi_size_x, int roi_size_y, int roi_size_x_2, int roi_size_y_2)
{
  int j,k;
  if (roi==NULL || src==NULL) return D_O_K;
  if (src->im.data_type != IS_CHAR_IMAGE) return D_O_K;
  if (roi->im.nx != roi_size_x || roi->im.ny != roi_size_y) return D_O_K;

  for (j=0 ; j < roi_size_y ; j++)
  {
    for (k=0 ; k < roi_size_x ; k++)
      {
        roi->im.pixel[j].ch[k]=src->im.pixel[j+roi_yc-roi_size_y_2].ch[k+roi_xc-roi_size_x_2];
      }
  }
  return 0;
}

//draw stuff in color depending on ok or not
//red if not
int sdi_fringes_color_choice(int *color, sdi_fringes *fringes)
{
  if (fringes==NULL) return D_O_K;
  if (fringes->v_0 >0) *color=makecol(0,255,255);
  if (fringes->v_0 <0) *color=makecol(238,0,238);
  if (fringes->v_0 == 999 || fringes->v_1 == 999) *color=makecol(255,64,64);
  return 0;
}


int display_one_sdi_fringes_data(sdi_fringes *fringes)
 {
   if (fringes==NULL) return D_O_K;
   win_printf_OK("Coordinates of the centrum of the pattern (in pixels) : x=%d, y=%d \n"
         "ROI along x in the image : xmin=%d, xmax=%d \n"
         "ROI along y in the image : ymin=%d, ymax=%d \n"
         "Phase velocity : %f rad per micron, group velocity : %f pix per micron \n"
         "Frequency of the fringes : %f (in 1 over pix) \n"
         "Background (in CCD counts) : %f\n"
         "Mode nu0 (should be 0 only during the calibration) : %d \n"
         "Maximum intensity of the signal (in CCD counts)[0] : %f \n"
         "Contrast[0] : %f \n"
         ,fringes->xc,fringes->yc,fringes->x_roi_min,fringes->x_roi_max,fringes->y_roi_min,fringes->y_roi_max,fringes->v_0,fringes->v_1,
         fringes->nu0,fringes->background, fringes->mode_nu0,9,9);// fringes->I_max_t->yd[0], fringes->contrast_t->yd[0]);

   win_printf_OK("Memory allocated to nu0(t) : %d \n"
                 "Memory allocated to order0(t) : %d \n"
                 "Memory allocated to order1(t) : %d \n"
                 "Memory allocated to the profile along x : %d \n"
                 "Memory allocated to the raw profile along x (before -bgd and *windowd) : %d \n"
                 "Memory allocated to the window : %d \n"
                 "Memory allocated to the spectrum : %d \n"
                 "Memory allocated to the log of the spectrum : %d \n"
                 "Memory allocated to the amplitude : %d \n"
                 "Memory allocated to the normalized amplitude : %d \n"
                 "Memory allocated to the spectral phase : %d \n"
                 "Memory allocated to the unwrapped roi of the spectral phase : %d \n"
                 "Memory allocated to the raw roi of the spectral phase : %d \n"
                 "Memory allocated to the result of proc1 on order0(t) : %d \n"
                 "Memory allocated to the record of the contrast : %d \n"
                 "Memory allocated to the record of the max Intensity : %d \n"
                 ,((fringes->nu0_t==NULL) ? 0 : fringes->nu0_t->mx),((fringes->order0_t==NULL) ? 0 : fringes->order0_t->mx)
                 ,((fringes->order1_t==NULL) ? 0 : fringes->order1_t->mx),((fringes->windowed_x_profile==NULL) ? 0 : fringes->windowed_x_profile->mx)
                 ,((fringes->raw_x_profile==NULL) ? 0 : fringes->raw_x_profile->mx),((fringes->x_window==NULL) ? 0 : fringes->x_window->mx)
                 ,((fringes->spectrum==NULL) ? 0 : fringes->spectrum->mx),((fringes->spectrum_log==NULL) ? 0 : fringes->spectrum_log->mx)
                 ,((fringes->amplitude==NULL) ? 0 : fringes->amplitude->mx),((fringes->amplitude_roi==NULL) ? 0 : fringes->amplitude_roi->mx)
                 ,((fringes->phase==NULL) ? 0 : fringes->phase->mx),((fringes->unwrapped_phase_roi==NULL) ? 0 : fringes->unwrapped_phase_roi->mx)
                 ,((fringes->raw_phase_roi==NULL) ? 0 : fringes->raw_phase_roi->mx),((fringes->order0_t_proc1==NULL) ? 0 : fringes->order0_t_proc1->mx)
                 ,((fringes->contrast_t==NULL) ? 0 : fringes->contrast_t->mx),((fringes->I_max_t==NULL) ? 0 : fringes->I_max_t->mx));

   return 0;
 }

int display_one_bead_data(sdi_bead *bead)
{
  if (bead==NULL) return D_O_K;
  win_printf_OK("The bead number is composed of the patterns of fringes whose are located in x=%d, y=%d and x=%d, y=%d \n"
                "The y profile of the bead is located in x=%d, y=%d \n"
                "The sensitivites along z are %f rad per microns and %f pixels per micron"
     , bead->half_beads[0]->xc, bead->half_beads[0]->yc, bead->half_beads[1]->xc, bead->half_beads[1]->yc
     , bead->y_bead->xc, bead->y_bead->yc, bead->v_phi1_z, bead->v_phi0_z);
   win_printf_OK("Memory allocated to z(t) : %d \n"
                 "Memory allocated to y(t) : %d \n"
                 "Memory allocated to x(t) : %d \n"
                 ,((bead->z_t==NULL) ? 0 : bead->z_t->mx),((bead->y_t==NULL) ? 0 : bead->y_t->mx)
                 ,((bead->x_t==NULL) ? 0 : bead->x_t->mx));
  return 0;
}

int display_one_y_profile_data(sdi_y_profile *y_profile)
{
   if (y_profile==NULL) return D_O_K;
   win_printf_OK("Coordinates of the centrum of the y profile :  x=%d, y=%d \n"
                 "ROI along x %d %d, along y %d %d \n"
                 "Background %f (in CCD units) \n"
                 "Lateral magnification %f"
         , y_profile->xc, y_profile->yc, y_profile->x_roi_min, y_profile->x_roi_max, y_profile->y_roi_min, y_profile->y_roi_max, y_profile->background
         , y_profile->gy);
  win_printf_OK("Memory allocated to the profile along y : %d \n"
                "Memory allocated to the raw profile along y (before -bgd and *windowd) : %d \n"
                "Memory allocated to the window : %d \n"
                "Memory allocated to the autoconvolution : %d \n"
                "Memory allocated to the roi of the autoconvolution : %d \n"
      ,((y_profile->windowed_y_profile==NULL) ? 0 : y_profile->windowed_y_profile->mx),((y_profile->raw_y_profile==NULL) ? 0 : y_profile->raw_y_profile->mx)
      ,((y_profile->y_window==NULL) ? 0 : y_profile->y_window->mx), ((y_profile->y_profile_autoconv==NULL) ? 0 : y_profile->y_profile_autoconv->mx)
      ,((y_profile->y_profile_autoconv_roi==NULL) ? 0 : y_profile->y_profile_autoconv_roi->mx));
   return 0;
}

int add_sdi_one_display(d_s *ds,pltreg **plr,O_p **op,char *name)
 {
   int flag_ini;
   flag_ini=0;
   if (ds == NULL)  return D_O_K;

   if (*plr==NULL)
   {
     if (*op==NULL)
     {
       flag_ini=1;
       init_one_plot(*op);
     }
     *plr=create_hidden_pltreg_with_op(op,1,1,0,name);
   }

   else
   {
     if (*op ==NULL)
     {
        *op=create_and_attach_one_plot(*plr,1,1,0);
         flag_ini=1;
     }
   }

   add_data_to_one_plot(*op,IS_DATA_SET,ds);
   if (flag_ini==1) remove_ds_from_op(*op, (*op)->dat[0]);//remove the first dataset put during the initialization of a plot
   return 0;
 }


int add_sdi_bead_sdi_one_display(sdi_bead *bead, int which_data, sdi_disp *display)
{
  d_s *data=NULL;
  switch_ptr_to_sdi_bead_field(bead,which_data,&data);
  add_sdi_one_display(data,&display->plr,&display->op,NULL);
  display->data = data;
  return 0;
  return 0;
}
int add_sdi_y_profile_sdi_one_display(sdi_y_profile *y_profile, int which_data, sdi_disp *display)
{
  d_s *data=NULL;
  switch_ptr_to_sdi_y_profile_field(y_profile,which_data,&data);
  add_sdi_one_display(data,&display->plr,&display->op,NULL);
  display->data = data;
  return 0;
}
int add_sdi_fringes_sdi_one_display(sdi_fringes *fringes, int which_data, sdi_disp *display)
{
  d_s *data=NULL;
  switch_ptr_to_sdi_fringes_field(fringes,which_data,&data);
  add_sdi_one_display(data,&display->plr,&display->op,NULL);
  display->data = data;
  return 0;
}

//from pix unit (imr) coordinates it places a rect on right position in the image
int draw_one_fast_rect_with_user_pix(int xc,int yc,int half_length, int half_width,int color, imreg *imr, O_i *oi, BITMAP *imb)
{
  int xc2,yc2,half_length2,half_width2;
  xc2=x_user_pix_2_vga_screen_unit(imr,oi,xc);
  half_length2=dx_user_pix_2_vga_screen_unit(imr,oi,half_length);
  yc2=y_user_pix_2_vga_screen_unit(imr,oi,yc,imb);
  half_width2=dy_user_pix_2_vga_screen_unit(imr,oi,half_width);
  draw_one_fast_rect_vga_screen_unit(xc2,yc2,half_length2,half_width2,color,imb);
  return 0;
}
//blit(imb,screen,0,0,imr->x_off+dlg->x,imr->y_off-imb->h+dlg->y,imb->w,imb->h);
//stretch_blit(imb,screen,0,0,imb->w,imb->h,0,0,SCREEN_W,SCREEN_H);pour faire un zoom un jour

//int y_user_pix_2_vga_screen_unit(imreg *imr,O_i *oi,int y,BITMAP *imb)
//int dy_user_pix_2_vga_screen_unit(imreg *imr,O_i *oi,int dy)

int place_multiple_double_fast_rect_with_mouse(int *xc, int *yc, int half_length,int half_width, int x_dist, int y_dist, int color, imreg *imr, O_i *oi, BITMAP *imb)
{
  int  x_m, y_m,count;
  int pos_m;
  int one_time;
  int xc_tmp,yc_tmp;
  int y_offset_1, y_offset_2, y_offset_1_vga, y_offset_2_vga;
  int x_offset_1, x_offset_2, x_m_tmp, y_m_tmp, x_offset_1_vga, x_offset_2_vga;
  y_offset_1=(int) (y_dist / 2);
  y_offset_2=(int) (y_dist / 2) + y_dist % 2;
  x_offset_1=(int) (x_dist / 2);
  x_offset_2=(int) (x_dist / 2) + x_dist % 2;
  y_offset_1_vga=dy_user_pix_2_vga_screen_unit(imr,oi,y_offset_1);
  y_offset_2_vga=dy_user_pix_2_vga_screen_unit(imr,oi,y_offset_2);
  x_offset_1_vga=dx_user_pix_2_vga_screen_unit(imr,oi,x_offset_1);
  x_offset_2_vga=dx_user_pix_2_vga_screen_unit(imr,oi,x_offset_2);
  pos_m = mouse_pos;
  x_m = pos_m >> 16;
  y_m = pos_m & 0x0000ffff;
  count=0;

  do
  {
     while( key[KEY_F]==false )
        {
           if (pos_m != mouse_pos)
	            {
	              pos_m = mouse_pos;
                x_m = pos_m >> 16;
                y_m = pos_m & 0x0000ffff;
                one_time=0;
                refresh_image(imr,UNCHANGED);
                acquire_screen();
	              draw_one_fast_rect_vga_screen_unit(x_m-x_offset_1_vga,y_m-y_offset_1_vga,half_length,half_width,color,screen);
                draw_one_fast_rect_vga_screen_unit(x_m+x_offset_2_vga,y_m+y_offset_2_vga,half_length,half_width,color,screen);
                release_screen();
	             }
          }

      if (one_time==0)
           {
               pos_m = mouse_pos;
               x_m = pos_m >> 16;
               y_m = pos_m & 0x0000ffff;
               x_m_tmp = x_imr_2_imdata(imr, x_m);
               y_m_tmp = y_imr_2_imdata(imr, y_m);
               *(xc+count) = x_m_tmp ;
               *(yc+count) = y_m_tmp;
               xc_tmp=x_user_pix_2_vga_screen_unit(imr,oi,*(xc+count));
               yc_tmp=y_user_pix_2_vga_screen_unit(imr,oi,*(yc+count),imb);
               acquire_bitmap(imb);
               draw_one_fast_rect_vga_screen_unit(xc_tmp-x_offset_1_vga,yc_tmp-y_offset_1_vga,half_length,half_width,color,imb);
               draw_one_fast_rect_vga_screen_unit(xc_tmp+x_offset_2_vga,yc_tmp+y_offset_2_vga,half_length,half_width,color,imb);
               release_bitmap(imb);
               count+=1;
               if (count == SDI_MAX_NUMBER_OF_PATTERN)
                  {
                      win_printf_OK("You have reached the maximum number of patterns");
                      return 1;
                  }
             }
      one_time+=1;
  } while( key[KEY_D] ==false );
  refresh_image(imr,UNCHANGED);
  return count;
}



int place_multiple_fast_rect_with_mouse(int *xc, int *yc,int half_length,int half_width,int color, imreg *imr, O_i *oi, BITMAP *imb)
{
  int  x_m, y_m,count;
  int pos_m;
  int one_time;
  int xc_tmp,yc_tmp;
  pos_m = mouse_pos;
  x_m = pos_m >> 16;
  y_m = pos_m & 0x0000ffff;
  count=0;

  do
  {
     while( key[KEY_F]==false )
        {
           if (pos_m != mouse_pos)
	            {
	              pos_m = mouse_pos;
                x_m = pos_m >> 16;
                y_m = pos_m & 0x0000ffff;
                one_time=0;
                refresh_image(imr,UNCHANGED);
                acquire_screen();
	              draw_one_fast_rect_vga_screen_unit(x_m,y_m,half_length,half_width,color,screen);
                release_screen();
	             }
          }

      if (one_time==0)
           {
               pos_m = mouse_pos;
               x_m = pos_m >> 16;
               y_m = pos_m & 0x0000ffff;
               *(xc+count) = x_imr_2_imdata(imr, x_m);
               *(yc+count) = y_imr_2_imdata(imr, y_m);
               xc_tmp=x_user_pix_2_vga_screen_unit(imr,oi,*(xc+count));
               yc_tmp=y_user_pix_2_vga_screen_unit(imr,oi,*(yc+count),imb);
               acquire_bitmap(imb);
               draw_one_fast_rect_vga_screen_unit(xc_tmp,yc_tmp,half_length,half_width,color,imb);
               release_bitmap(imb);
               count+=1;
               if (count == SDI_MAX_NUMBER_OF_PATTERN)
                  {
                      win_printf_OK("You have reached the maximum number of patterns");
                      return 1;
                  }
             }
      one_time+=1;
  } while( key[KEY_D] ==false );
  refresh_image(imr,UNCHANGED);
  return count;
}


int place_one_fast_rect_with_mouse(int *xc, int *yc,int half_length,int half_width,int color, imreg *imr, O_i *oi, BITMAP *imb)
{
  int  x_m, y_m;
  int pos_m;
  int xc_tmp,yc_tmp;
  pos_m = mouse_pos;
  x_m = pos_m >> 16;
  y_m = pos_m & 0x0000ffff;
  while( key[KEY_F]==false )
  {
    if (pos_m != mouse_pos)
	    {
	       pos_m = mouse_pos;
         x_m = pos_m >> 16;
         y_m = pos_m & 0x0000ffff;
	       refresh_image(imr,UNCHANGED);
         acquire_screen();
	       draw_one_fast_rect_vga_screen_unit(x_m,y_m,half_length,half_width,color,screen);
         release_screen();
	    }
  }
  *xc =1+ x_imr_2_imdata(imr, x_m);
  *yc =1+ y_imr_2_imdata(imr, y_m);
  xc_tmp=x_user_pix_2_vga_screen_unit(imr,oi,*xc);
  yc_tmp=y_user_pix_2_vga_screen_unit(imr,oi,*yc,imb);
  acquire_bitmap(imb);
  draw_one_fast_rect_vga_screen_unit(xc_tmp,yc_tmp,half_length,half_width,color,imb);
  release_bitmap(imb);
  refresh_image(imr,UNCHANGED);
  return 0;
}

//different from x_imr_2_imdata & cie because x is the coordinate in the image not on screen
//set of 4 functions that translate position and distance in pixel units into vga screen unit
//to be used to put something in the bmp hooked to a oi
int y_user_pix_2_vga_screen_unit(imreg *imr,O_i *oi,int y,BITMAP *imb)
{
  return((int) ((float)imb->h - (float)(y-oi->im.nys)*(float)(oi->height*imr->s_ny)/(float)(oi->im.nye-oi->im.nys)));
}
int dy_user_pix_2_vga_screen_unit(imreg *imr,O_i *oi,int dy)
{
  return((int)(dy*(float)(oi->height*imr->s_ny)/(float)(oi->im.nye-oi->im.nys)));
}
int x_user_pix_2_vga_screen_unit(imreg *imr,O_i *oi,int x)
{
  return((int)((x-oi->im.nxs)*(float)(oi->width*imr->s_nx)/(float)(oi->im.nxe-oi->im.nxs)));
}
int dx_user_pix_2_vga_screen_unit(imreg *imr,O_i *oi,int dx)
{
  return((int)(dx*(float)(oi->width*imr->s_nx)/(float)(oi->im.nxe-oi->im.nxs)));
}

int add_text_vga_screen_unit(BITMAP *bmp, int xc, int yc, const char *format, ...)
{//int	set_plot_title(O_p *op, const char *format, ...)
  va_list ap;
  imreg *imr=NULL;
  O_i *oi=NULL;
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)	return OFF;
  BITMAP *imb=NULL;
  int color,i;
  int x_txt,y_txt;
  char txt[128];
  snprintf(txt,128,"test");
  //sprintf(txt,32,'test');

  if ((oi->bmp.to == IS_BITMAP) &&  (oi->bmp.stuff != NULL))
  {
    imb = (BITMAP*)oi->bmp.stuff;
    color=makecol(255,64,64);
  //  for (i=0;i<sdi_gen->n_fringes;i++)
  //  {
  i=1;
      x_txt= x_user_pix_2_vga_screen_unit(imr,oi,xc);
      y_txt= y_user_pix_2_vga_screen_unit(imr,oi,yc,imb);
      //va_start(ap, format);
      //vsnprintf(txt,2048,format,ap);
      //va_end(ap);
      textprintf_centre_ex(imb,large_font,x_txt,y_txt,color,-1,txt);
    //}
  }
  refresh_image(imr,UNCHANGED);
  return 0;

}

int	draw_one_fast_rect_vga_screen_unit(int xc, int yc, int half_length, int half_width, int color, BITMAP* bm)
{
  int x1,x2,y1,y2;
  int x01,x02,y01,y02;
  x1=xc-half_length;
  x2=xc+half_length;
  y1=yc-half_width;
  y2=yc+half_width;
  y01=yc-4*half_width;
  y02=yc+4*half_width;
  x01=xc-half_length-10;
  x02=xc+half_length+10;
  fastline(bm, x1, y1, x2, y1, color);
  fastline(bm, x2, y1, x2, y2, color);
  fastline(bm, x2, y2, x1, y2, color);
  fastline(bm, x1, y2, x1, y1, color);
  fastline(bm, xc, y01, xc, y02, color);
  fastline(bm, x01, yc, x02, yc, color);
  return 0;
}


int	draw_one_ellipse_vga_screen_unit(int xc, int yc, int radius_x, int radius_y, int color, BITMAP* bmp)
{
  ellipse(bmp,xc,yc,radius_x,radius_y,color);
  return 0;
}
