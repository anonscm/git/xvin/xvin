#pragma once

//functions whose input is an sdi_fringes structure : one level below than SDITracking

#include "SDITracking.h"


PXV_FUNC(int, compute_sdi_bead_order0_t_proc1, (sdi_bead *bead,sdi_parameter *param));
PXV_FUNC(int, compute_sdi_bead_order1_t_proc1, (sdi_bead *bead,sdi_parameter *param));
PXV_FUNC(int, compute_sdi_fringes_order0_t_proc1, (sdi_fringes *fringes,sdi_parameter *param));


PXV_FUNC(int, set_sdi_bead_sensitivity, (sdi_bead *bead));

PXV_FUNC(int, compute_center_of_autoconv_of_y_profile, (sdi_y_profile *y_profile, sdi_parameter *param, float *position));
PXV_FUNC(int, ds_find_center_by_polynomial_3, (sdi_y_profile *y_profile, float *center, int x_i_min, int roi_interp_size));
PXV_FUNC(int, compute_center_by_autoconv_of_sdi_y_profile, (O_i *oi,sdi_y_profile *y_profile, sdi_parameter *param, float *center));
//handling of fields of fringe object
PXV_FUNC(int, set_sdi_fringes_x_roi, (sdi_fringes *fringes, int x_roi_size_2));
PXV_FUNC(int, set_sdi_fringes_y_roi, (sdi_fringes *fringes, int y_roi_size_2));
PXV_FUNC(int, set_sdi_fringes_mode_nu0, (sdi_fringes *fringes, int mode));
PXV_FUNC(int, set_sdi_fringes_background, (sdi_fringes *fringes, float background));
PXV_FUNC(int, set_one_sdi_fringes_v0, (sdi_fringes *fringes, float v_0));
PXV_FUNC(int, set_one_sdi_fringes_v1, (sdi_fringes *fringes, float v_1));
PXV_FUNC(int, set_one_sdi_fringes_nu0, (sdi_fringes *fringes,float nu0));
PXV_FUNC(int, set_sdi_fringes_keep_data, (sdi_fringes *fringes, int which_data, bool choice));
PXV_FUNC(int, set_one_sdi_fringes_centrum, (sdi_fringes *fringes,int xc, int yc));

//handling of fields of y_profile object
PXV_FUNC(int, set_sdi_y_profile_gy, (sdi_y_profile *y_profile, float gy));
PXV_FUNC(int, set_sdi_y_profile_y_roi, (sdi_y_profile *y_profile, int y_roi_size_2_Y));
PXV_FUNC(int, set_sdi_y_profile_x_roi, (sdi_y_profile *y_profile, int x_roi_size_2_Y));
PXV_FUNC(int, set_sdi_y_profile_background, (sdi_y_profile *y_profile, float background));
PXV_FUNC(int, set_sdi_y_profile_keep_data, (sdi_y_profile *y_profile, int which_data, bool choice));
PXV_FUNC(int, set_one_sdi_y_profile_centrum, (sdi_y_profile *y_profile,int xc, int yc));


//wrapps the operation made by functions in sdi_basic
PXV_FUNC(int, find_z0_of_one_bead_from_movie, (sdi_bead *bead));
PXV_FUNC(int, set_offset_sdi_fringes, (sdi_fringes *fringes, float order1_z_offset,float order0_z_offset));
PXV_FUNC(int, compute_z_coupled_of_sdi_fringes_at_current_i, (sdi_fringes *fringes));
PXV_FUNC(int, set_sdi_fringes_last_ok, (sdi_fringes *fringes,float order0_new_ok, float order1_new_ok));
PXV_FUNC(int, compute_phase_coeff_of_sdi_fringes, (O_i *oi,sdi_fringes *fringes, sdi_parameter *param));
PXV_FUNC(int, frequency_calibration_of_sdi_fringes, (sdi_fringes *fringes,d_s *nu0_z);)
PXV_FUNC(int, linear_calibration_of_sdi_fringes, (sdi_fringes *fringes,d_s *order_0,d_s *order_1));
PXV_FUNC(int, ds_phase_fit_omega, (sdi_fringes *fringes, double **phase_coeff,float nu0, int x_i_frac_min,int x_i_frac_max));
PXV_FUNC(int, compute_phase_coeff_of_x_profile, (sdi_fringes *fringes, sdi_parameter *param,double **phase_coeff));
PXV_FUNC(int, extract_sdi_y_profile_y_windowed_profile_from_image, (O_i *oi,sdi_y_profile *y_profile,sdi_parameter *param));
PXV_FUNC(int, extract_sdi_fringes_x_windowed_profile_from_image, (O_i *oi,sdi_fringes *fringes,sdi_parameter *param));
