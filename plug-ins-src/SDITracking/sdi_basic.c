# include "sdi_basic.h"
#include "float.h"

int estimate_mean(float *tab, int n, float *mean)
{
  int i;
  double mean_tmp; //in case of we add a lot of values and/or very different one
  mean_tmp=0;
  for (i=0;i<n;i++)
  {
    mean_tmp+=*(tab+i);
    //win_printf_OK("n %d debug %f",n,*(tab+i));
  }
  mean_tmp = mean_tmp / n;
  *mean = (float) mean_tmp;
  return 0;
}

//the classic std with with bias
int estimate_std(float *tab, int n, float *std)
{
  int i;
  float mean;
  double var_tmp;
  var_tmp=0;
  mean=0;
  estimate_mean(tab,n,&mean);
  for (i=0;i<n;i++)
  {
    var_tmp += (*(tab+i) - mean) * (*(tab+i) - mean);
  }
  var_tmp = var_tmp / (n-1) ;
  *std = (float) sqrt(var_tmp);
  return 0;
}

 //the core of the sdi tracking algorithm :
 //the absolute value of the position of the pattern of the finrges is given by the slope of the spectral phase.
 //this value is not precise, but enough to find the correct fringe : this information is then used to find the correct
 //multiple of 2*pi to be added to the constant value of the spectral phase between two measurements.
 //This determination is done by using the two velocities previously determined during the calibration step of the device :
 //the phase velocity is in rad per µm, and the groupd velocity is in pixel per µm.
 int find_correct_2_pi_multiple(float *phi0_1,float phi1_1,float phi0_0,float phi1_0,float v_0,float v_1)
 {
   double tmp;
   int k;
   tmp=((double) *phi0_1-(double) phi0_0- (double) v_0 / (double)v_1 * (phi1_1-phi1_0)) / 2 / M_PI;
   //win_printf_OK("phi00 %f phi10 %f phi01 %f phi11 %f v0 %f v1 %f diff %f",phi0_0,phi1_0,*phi0_1,phi1_1,v_0,v_1,tmp);
   k=(int) (round(tmp));
   *phi0_1=*phi0_1- 2 * k *M_PI ;
   return 0;
 }

 //puts an offset and then a scale factor to an array
 int offset_then_scale(float *tab,int tab_size,float offset,float scale)
 {
   int i;
   double scale_double;
   double offset_double;
   offset_double=(double) offset;
   scale_double=(double) scale;
   for (i=0;i<tab_size;i++)
   {
     *(tab+i)=(float) (((double)*(tab+i) - offset_double) * scale_double);
   }
   return 0;
 }

 //unwrap the phase of a complex signal
 int sdi_unwrap(float *phase,int i_min,int i_max, float tol)
 {
   int k;
   int i;
   double tmp;
   int tmp_k;
   double tol_double;
   int *local_jump;
   k=0;
   tmp_k=0;
   tol_double=(double) tol;
   local_jump=(int *)(calloc(i_max-i_min,sizeof(int)));
   for (i=i_min ; i<i_max-1 ; i++)
   {
     tmp= (double) *(phase+i+1)-(double) *(phase+i);
     tmp=tmp/tol_double;
     tmp=tmp/M_PI;
     tmp_k =(int) tmp;
     //if (tmp_k == 2) tmp_k=1;
     //if (tmp_k == -2) tmp_k=-1;
     *(local_jump+i-i_min)=tmp_k;
   }
   for (i=i_min+1;i<i_max;i++)
   {
     k+=local_jump[i-i_min-1];
     *(phase+i)=(float) ( (double) *(phase+i) + (double) (- (double) k * (double) 2 * M_PI) );
   }
   free(local_jump);
   local_jump=NULL;
   return k;
 }
 //computes the spectral phase of the fft of a signal (for VC's fftbt result)
 int raw_spectral_phase_fftbt(float *tf_complex,float *phase, int i_min, int i_max)
 {
   int i;
   float *re;
   float *im;
   for (i=i_min;i<i_max;i++)
   {
     re=tf_complex+2*i;
     im=tf_complex+2*i+1;
     *(phase+i-i_min)=(float) atan2(*im , *re);
   }
   return 0;
 }



 //basic analysis of the peak of one ds
 //search for position and width defined by a fractional of (max-min) of a ds
 //also returns the last index before crossing the line and  the closest index of the fractional of dynamic
 int ds_peak_width(const d_s *ds,float frac,float *x_frac_min,int *x_i_frac_min,int *x_i_frac_closest_min,
                                       float *x_frac_max,int *x_i_frac_max,int *x_i_frac_closest_max,float *width)
 {
   int ds_x_i_max,ds_x_i_min;
   float ds_x_max,ds_y_max,ds_x_min,ds_y_min,seuil;
   float tmp1, tmp2;
   ds_max(ds,&ds_y_max,&ds_x_max,&ds_x_i_max);
   ds_min(ds,&ds_y_min,&ds_x_min,&ds_x_i_min);
   *x_i_frac_min=ds_x_i_max;
   *x_i_frac_max=ds_x_i_max;
   seuil=ds_y_max-ds_y_min;
   seuil=seuil * frac+ds_y_min;

   //before the max
   while  (ds->yd[*x_i_frac_min-1]>seuil)
   {
     *x_i_frac_min=*x_i_frac_min-1;
   }
   //linear interpolation
   tmp1=ds->yd[*x_i_frac_min]-ds->yd[*x_i_frac_min-1];
   tmp2=ds->xd[*x_i_frac_min]-ds->xd[*x_i_frac_min-1];
   *x_frac_min=ds->xd[*x_i_frac_min-1]+(seuil-ds->yd[*x_i_frac_min-1])*tmp2/tmp1;
   //return nearest index
   *x_i_frac_closest_min=*x_i_frac_min;
   if ((ds->yd[*x_i_frac_min]-seuil)<(seuil-ds->yd[*x_i_frac_min-1])) (*x_i_frac_closest_min)--;

   //after the max
   while (ds->yd[*x_i_frac_max+1]>seuil)
   {
     *x_i_frac_max=*x_i_frac_max+1;
   }
   //linear interpolation
   tmp1=ds->yd[*x_i_frac_max+1]-ds->yd[*x_i_frac_max];
   tmp2=ds->xd[*x_i_frac_max+1]-ds->xd[*x_i_frac_max];
   *x_frac_max=ds->xd[*x_i_frac_max]+(seuil-ds->yd[*x_i_frac_max])*tmp2/tmp1;
   //nearest index
   *x_i_frac_closest_max=*x_i_frac_max;
   if ((ds->yd[*x_i_frac_max]-seuil)>(seuil-ds->yd[*x_i_frac_max+1])) (*x_i_frac_closest_max)++;

   *width=*x_frac_max-*x_frac_min;
   return 0;

 }
//computes the mean frequency of a signal : working with the spectrum,
//it determines the part of the xd axis where one wants to extract a distribution,
//normalizes the extracted distribution and finally computes the first momentum
//spectrum : dataset
//nu0 : the mean frequency
//x_i_frac_closest_min, x_i_frac_closest_max : from a previous analysis, these are the indexes of the two
//points in the xd axis of the spectrum which are the closest from the threshold used to analyse the peak
//dx_i_fac is a factor (typically 2) used to determine the part of xd over which the mean is computed
int ds_measure_frequency(d_s *spectrum,float *nu0,int x_i_frac_closest_min,int x_i_frac_closest_max,int dx_i_fac)
{
  d_s *spectrum_tmp=NULL;
  int i,x_i_inf,x_i_sup;
  x_i_inf=(int) (x_i_frac_closest_min - (x_i_frac_closest_max-x_i_frac_closest_min) * dx_i_fac);
  x_i_sup=(int) (x_i_frac_closest_max + (x_i_frac_closest_max-x_i_frac_closest_min) * dx_i_fac);
  spectrum_tmp=duplicate_data_set(spectrum,spectrum_tmp);
  for (i=0;i<x_i_inf;i++)
  {
    spectrum_tmp->yd[i]=0;
  }
  for (i=x_i_sup;i<spectrum->nx;i++)
  {
    spectrum_tmp->yd[i]=0;
  }
  ds_normalize_by_sum(spectrum_tmp);
  ds_mu_1(spectrum_tmp,nu0);
  free_data_set(spectrum_tmp);
  spectrum_tmp=NULL;
  return 0;
}
//computes the first moment of a dataset view as a distribution
int ds_mu_1(const d_s *ds, float *mu1)
{
  int i;
  *mu1=0;
  for (i = 0 ; i < ds->nx; i++)
  {
    *mu1 +=(float) ((double) (ds->xd[i]) * (double) (ds->yd[i]));
  }
  return 0;
}
//normalizes a dataset by its cumulative sum
int ds_normalize_by_sum(d_s *ds)
{
  int i;
  float sum=0;
  double sum_double=0;
  cumulative_sum(ds->yd,ds->nx,&sum);
  sum_double=(double) sum;
  for (i=0;i<ds->nx;i++)
  {
    ds->yd[i] = (float) ((double) (ds->yd[i]) / sum_double);
  }
  return 0;
}
//computes the cumulative sum of the values of an array
int cumulative_sum(const float *tab,int size_tab,float *sum)
{
  int i;
  *sum=0;
  for (i=0;i<size_tab;i++)
  {
    *sum = (float) ((double)(*sum) + (double)(*(tab+i)));
  }
  return 0;
}

//find the extremum value of a function by fitting a 3order polynom and then
//looking for its null of derivative
int ds_max_interpolation_polynom_3(const d_s *ds,float *ds_y_max,float *ds_x_max)
{
    int i;
    double a, b, c, d, tmp;
    double **coeff=NULL;
    coeff=(double **)calloc(4,sizeof(double *));
    fit_ds_to_xn_polynome(ds, 4, -FLT_MAX, FLT_MAX, -FLT_MAX, FLT_MAX, coeff);
    a=*(*coeff+3);
    b=*(*coeff+2);
    c=*(*coeff+1);
    d=*(*coeff);
    tmp = (-b - sqrt(b * b - 3 * a * c)) / (3 * a);//zeros of the derivative
    *ds_x_max = (float) tmp;
    *ds_y_max = (float)((a*tmp*tmp*tmp)+(b*tmp*tmp)+(c*tmp)+d);
    free(coeff);
    coeff=NULL;
    return 0;
}

//find the extremum value of a function by fitting a 2order polynom and then
//looking for its null of derivative
int ds_max_interpolation_polynom_2(const d_s *ds,float *ds_y_max,float *ds_x_max)
{
    int i;
    double a, b, c, tmp;
    double **coeff=NULL;
    coeff=(double **)calloc(3,sizeof(double *));
    fit_ds_to_xn_polynome(ds, 3, -FLT_MAX, FLT_MAX, -FLT_MAX, FLT_MAX, coeff);
    a=*(*coeff+2);
    b=*(*coeff+1);
    c=*(*coeff);
    tmp = (-b)  / (2 * a);//zeros of the derivative
    *ds_x_max = (float) tmp;
    *ds_y_max = (float)((a*tmp*tmp)+(b*tmp)+c);
    free(coeff);
    coeff=NULL;
    return 0;
}

int ds_max_local(const d_s *ds,float *ds_y_max,float *ds_x_max,int *ds_x_i_max, int x_i_roi_min, int x_i_roi_max)
{
    int i;
    *ds_x_i_max=0;
    *ds_x_max = ds->xd[x_i_roi_min];
    *ds_y_max = ds->yd[x_i_roi_min];
    for (i = 1 + x_i_roi_min ; i < x_i_roi_max ; i++)
    {
        if (ds->yd[i] > *ds_y_max)
        {
            *ds_y_max = ds->yd[i];
            *ds_x_max = ds->xd[i];
            *ds_x_i_max=i;
        }
    }
    return 0;
}

int ds_min_local(const d_s *ds,float *ds_y_min,float *ds_x_min,int *ds_x_i_min, int x_i_roi_min, int x_i_roi_max)
{
    int i;
    *ds_x_i_min=0;
    *ds_x_min = ds->xd[x_i_roi_min];
    *ds_y_min = ds->yd[x_i_roi_min];
    for (i = 1 + x_i_roi_min ; i < x_i_roi_max ; i++)
    {
        if (ds->yd[i] < *ds_y_min)
        {
            *ds_y_min = ds->yd[i];
            *ds_x_min = ds->xd[i];
            *ds_x_i_min=i;
        }
    }
    return 0;
}

int ds_max(const d_s *ds,float *ds_y_max,float *ds_x_max,int *ds_x_i_max)
{
    int i;
    *ds_x_i_max=0;
    *ds_x_max = ds->xd[0];
    *ds_y_max = ds->yd[0];
    for (i = 1; i < ds->nx; i++)
    {
        if (ds->yd[i] > *ds_y_max)
        {
            *ds_y_max = ds->yd[i];
            *ds_x_max = ds->xd[i];
            *ds_x_i_max=i;
        }
    }
    return 0;
}
int ds_min(const d_s *ds,float *ds_y_min,float *ds_x_min,int *ds_x_i_min)
{
    int i;
    *ds_x_i_min=0;
    *ds_x_min = ds->xd[0];
    *ds_y_min = ds->yd[0];
    for (i = 1; i < ds->nx; i++)
    {
        if (ds->yd[i] < *ds_y_min)
        {
            *ds_y_min = ds->yd[i];
            *ds_x_min = ds->xd[i];
            *ds_x_i_min=i;
        }
    }
    return 0;
}
int ds_log(d_s *ds)
{
  int i;
  for (i=0;i<ds->nx;i++)
  {
    if (ds->yd[i] != 0)  ds->yd[i]=log(ds->yd[i]);
  }
  return 0;
}


//compute the autoconvolution of a signal (not normalized)
int ds_fft_then_autoconv_then_ifft_then_ifftshift(d_s *profile, fft_plan *fp, d_s *auto_conv)
{
  int i;
  float tmp;
  tmp=0;
  auto_conv=duplicate_data_set(profile,auto_conv);
  realtr1bt(fp,auto_conv->yd);
  fftbt(fp,auto_conv->yd,1);
  realtr2bt(fp,auto_conv->yd,1);
  for (i=0;i<auto_conv->nx;i+=2)
  {
    tmp=auto_conv->yd[i]*auto_conv->yd[i]-auto_conv->yd[i+1]*auto_conv->yd[i+1];
    auto_conv->yd[i+1]=2*auto_conv->yd[i]*auto_conv->yd[i+1];
    auto_conv->yd[i]=tmp;
  }
  realtr2bt(fp,auto_conv->yd,-1);
  fftbt(fp,auto_conv->yd,-1);
  realtr1bt(fp,auto_conv->yd);
  fftshift(auto_conv->yd,fp->n_2);
  return 0;
}

//compute the spectrum, amplitude and spectral phase of a signal
//it returns datasets whose axis are the frequency in pix-1
int ds_fftshift_then_fft(d_s *profile, fft_plan *fp,d_s *ds_amplitude,d_s *ds_phase,d_s *spectrum)
{
  d_s *ds_tf=NULL;
  double tmp;
  int i;
  fftshift(profile->yd,fp->n_2);
  ds_tf=duplicate_data_set(profile,ds_tf);
  realtr1bt(fp,ds_tf->yd);
  fftbt(fp,ds_tf->yd,1);
  realtr2bt(fp,ds_tf->yd,1);
  for (i=0;i<fp->n_2;i++)
  {
    ds_phase->xd[i]=(float) ((double) i / (double) fp->nx);//frequence en pixel
    ds_amplitude->xd[i]=ds_phase->xd[i];
    spectrum->xd[i]=ds_phase->xd[i];
    tmp=(double) (ds_tf->yd[2*i+1]*ds_tf->yd[2*i+1]+ds_tf->yd[2*i]*ds_tf->yd[2*i]);
    spectrum->yd[i]=(float) (tmp);
    ds_amplitude->yd[i]=(float) (sqrt(tmp));
  }
  ds_amplitude->nx=fp->n_2;
  ds_phase->nx=fp->n_2;
  spectrum->nx=fp->n_2;
  ds_amplitude->ny=fp->n_2;
  ds_phase->ny=fp->n_2;
  spectrum->ny=fp->n_2;
  raw_spectral_phase_fftbt(ds_tf->yd,ds_phase->yd,0,fp->n_2);
  free_data_set(ds_tf);
  ds_tf=NULL;
  return 0;
}
int fftshift(float *tab,int size_2)
{
  int i;
  float tmp;
  for (i=0;i<size_2;i++)
  {
    tmp=*(tab+i);
    *(tab+i)=*(tab+size_2+i);
    *(tab+size_2+i)=tmp;
  }
  return 0;
}
//fills a previously allocated dataset with an hypergaussian funcion
int generate_hypergaussian(d_s* ds,float fwhm_min,float fwhm_max,int ordre)
{
  int i;
  float centre,fwhm;
  fwhm=fwhm_max-fwhm_min;
  centre=fwhm_max+fwhm_min;
  centre=centre / 2;
  for (i=0;i<ds->nx;i++)
  {
    ds->yd[i]=exp(-pow((ds->xd[i]-centre)/fwhm,2*ordre)*0.5);
  }
  return 0;
}


//from an image, extracts the averaged profiles along x
//the profile must have been allocated before
int extract_x_profile_from_image( O_i *oi, d_s *profile,int x_i_min,int x_i_max,int y_i_min,int y_i_max)
{
  int i,j;
  int x_size,y_size;
  x_size=x_i_max-x_i_min;
  y_size=y_i_max-y_i_min;
  for (j=0;j<x_size;j++)
  {
    profile->xd[j]=0;
    profile->yd[j]=0;
  }
  if (y_i_min==y_i_max)
  {
    extract_raw_partial_line(oi, y_i_min, profile->yd, x_i_min, x_size);
  }
  for (i=y_i_min;i<y_i_max;i++)
  {
    extract_raw_partial_line(oi, i, profile->xd, x_i_min, x_size);
    for (j=0;j<x_size;j++)
    {
      profile->yd[j]+=profile->xd[j];
    }
  }
  for (i=0;i<x_size;i++)
  {
    profile->xd[i]=i;
    if (y_size!=0) profile->yd[i]=profile->yd[i] / y_size;
  }
  profile->nx=x_size;
  profile->ny=x_size;
  return 0;
}

//from an image, extracts the averaged profiles along y
//the profile must have been allocated before
int extract_y_profile_from_image( O_i *oi, d_s *profile,int x_i_min,int x_i_max,int y_i_min,int y_i_max)
{
  int i,j;
  int x_size,y_size;
  x_size=x_i_max-x_i_min;
  y_size=y_i_max-y_i_min;
  for (j=0;j<y_size;j++)
  {
    profile->xd[j]=0;
    profile->yd[j]=0;
  }
  for (i=x_i_min;i<x_i_max;i++)
  {
    extract_raw_partial_row(oi, i, profile->xd, y_i_min, y_size);
    for (j=0;j<y_size;j++)
    {
      profile->yd[j]+=profile->xd[j];
    }
  }
  for (i=0;i<y_size;i++)
  {
    profile->xd[i]=i;
    profile->yd[i]=profile->yd[i] / x_size;
  }
  profile->nx=y_size;
  profile->ny=y_size;
  return 0;
}
