//this contain the functions wrapping the sdi operations on the sdi structures
#include "sdi_treatment.h"
#include "sdi_basic.h"
#include "float.h"
# include "xvin.h"

int set_sdi_bead_sensitivity(sdi_bead *bead)
{
  if(bead==NULL) return D_O_K;
  if(bead->half_beads[0]==NULL || bead->half_beads[1]==NULL) return D_O_K;
  if(bead->half_beads[0]->v_0==999 || bead->half_beads[0]->v_1==999 ||bead->half_beads[1]->v_0==999 ||bead->half_beads[1]->v_1==999) return D_O_K;
  bead->v_phi0_z=bead->half_beads[0]->v_0-bead->half_beads[1]->v_0;
  bead->v_phi1_z=bead->half_beads[0]->v_1-bead->half_beads[1]->v_1;
  return 0;
}

int find_z0_of_one_bead_from_movie(sdi_bead *bead)
{
  int i,xc,dxc1,dxc2;
  float y_min,x_min;
  int x_min_i;
  d_s *order1_diff=NULL;
  if (bead==NULL) return D_O_K;
  /*if (bead->half_beads[0]->v_0 == 999 ||  bead->half_beads[0]->v_1 == 999 || bead->half_beads[1]->v_0 == 999 || bead->half_beads[1]->v_1 == 999)
  {
    win_printf_OK("need for calibration before");
    return D_O_K;
  }
  if (bead->half_beads[0]->order0_t==NULL || bead->half_beads[0]->order1_t==NULL || bead->half_beads[1]->order0_t==NULL || bead->half_beads[1]->order1_t==NULL)
  {
    win_printf_OK("Some data of the patterns are empty");
    return D_O_K;
  }
  /*if (bead->half_beads[0]->xc == bead->half_beads[1]->xc)
  {
    xc=bead->half_beads[0];
    dxc1=0;
    dxc2=0;
  }
  else
  {
    xc=(int) (((float) bead->half_beads[0]->xc + (float) bead->half_beads[1]->xc ) /2) ;
    dxc1=bead->half_beads[0]->xc-xc;
    dxc2=bead->half_beads[1]->xc-xc;
  }
  */
  win_printf_OK("check1");
  d_s *half_bead_0_ordre1_t_abs=NULL;
  d_s *half_bead_1_ordre1_t_abs=NULL;
  half_bead_0_ordre1_t_abs=duplicate_data_set(bead->half_beads[0]->order1_t,half_bead_0_ordre1_t_abs);
  half_bead_1_ordre1_t_abs=duplicate_data_set(bead->half_beads[0]->order1_t,half_bead_1_ordre1_t_abs);
  for (i=0;i<half_bead_0_ordre1_t_abs->nx;i++)
  {
    half_bead_0_ordre1_t_abs->yd[i]=fabs(bead->half_beads[0]->order1_t->yd[i]);
    half_bead_1_ordre1_t_abs->yd[i]=fabs(bead->half_beads[1]->order1_t->yd[i]);
  }

  win_printf_OK("check2");
  pltreg *plr=NULL;
  O_p *op=NULL;
  plr=create_hidden_pltreg_with_op(&op,1,1,0,"debug");
  add_data_to_one_plot(op,IS_DATA_SET,half_bead_0_ordre1_t_abs);
  add_data_to_one_plot(op,IS_DATA_SET,half_bead_1_ordre1_t_abs);
  add_data_to_one_plot(op,IS_DATA_SET,bead->half_beads[0]->order1_t);
  add_data_to_one_plot(op,IS_DATA_SET,bead->half_beads[1]->order1_t);
  float y_min_1, y_min_2, x_min_1, x_min_2;
  int x_min_i_1, x_min_i_2;
  ds_min(half_bead_0_ordre1_t_abs,&y_min_1,&x_min_1,&x_min_i_1);
  win_printf_OK("test xmini %d xmin %f",x_min_i_1,x_min_1);
  ds_min(half_bead_1_ordre1_t_abs,&y_min_2,&x_min_2,&x_min_i_2);
  win_printf_OK("test xmini %d xmin %f",x_min_i_2,x_min_2);
  win_printf_OK("centre des deux %d et %d",bead->half_beads[0]->xc,bead->half_beads[1]->xc);

  //set_offset_sdi_fringes(bead->half_beads[0],99,99);
  return 0;
}

//asservi à clock current_i des vraies données
//computes the result of the proc1 on the pre-given value of current_i
//and put it in the pre given value of current_i_proc1
//if impossible returns copy
int compute_sdi_fringes_order0_t_proc1(sdi_fringes *fringes,sdi_parameter *param)
{
  int i, work_i_min, work_i_max;
  work_i_min=fringes->current_i - param->proc1_i_delay - param->proc1_i_min;
  work_i_max=fringes->current_i - param->proc1_i_delay + param->proc1_i_max;
  if (work_i_min <0 ) work_i_min=0;
  if (work_i_max <0 ) work_i_max=0;
  if (work_i_min >= work_i_max)
  {
    fringes->order0_t_proc1->xd[fringes->current_i_proc1]=fringes->current_t_proc1;
    fringes->order0_t_proc1->yd[fringes->current_i_proc1]=fringes->order0_t->yd[fringes->current_i];
  }
  else
  {
    fringes->order0_t_proc1->xd[fringes->current_i_proc1]=fringes->current_t_proc1;
    fringes->order0_t_proc1->yd[fringes->current_i_proc1]=0;
    for (i= work_i_min ; i< work_i_max ; i++)
    {
      fringes->order0_t_proc1->yd[fringes->current_i_proc1] += fringes->order0_t->yd[i];
    }
    fringes->order0_t_proc1->yd[fringes->current_i_proc1] /= (float) (param->proc1_i_max + param->proc1_i_min);
  }
  return 0;
}

int compute_sdi_fringes_order1_t_proc1(sdi_fringes *fringes,sdi_parameter *param)
{
  int i, work_i_min, work_i_max;
  work_i_min=fringes->current_i - param->proc1_i_delay - param->proc1_i_min;
  work_i_max=fringes->current_i - param->proc1_i_delay + param->proc1_i_max;
  if (work_i_min <0 ) work_i_min=0;
  if (work_i_max <0 ) work_i_max=0;
  if (work_i_min >= work_i_max)
  {
    fringes->order1_t_proc1->xd[fringes->current_i_proc1]=fringes->current_t_proc1;
    fringes->order1_t_proc1->yd[fringes->current_i_proc1]=fringes->order1_t->yd[fringes->current_i];
  }
  else
  {
    fringes->order1_t_proc1->xd[fringes->current_i_proc1]=fringes->current_t_proc1;
    fringes->order1_t_proc1->yd[fringes->current_i_proc1]=0;
    for (i= work_i_min ; i< work_i_max ; i++)
    {
      fringes->order1_t_proc1->yd[fringes->current_i_proc1] += fringes->order1_t->yd[i];
    }
    fringes->order1_t_proc1->yd[fringes->current_i_proc1] /= (float) (param->proc1_i_max + param->proc1_i_min);
  }
  return 0;
}


//expects positive values of proc1_i_min, proc1_i_max
int compute_sdi_bead_order0_t_proc1(sdi_bead *bead,sdi_parameter *param)
{
  int i, work_i;
  if (param->proc1_i_max == -param->proc1_i_min) return D_O_K;//if the interval is empyt then does nothing
  work_i=bead->current_i - param->proc1_i_delay ;
  bead->z_t_proc1->xd[bead->current_i_proc1]=bead->current_t_proc1;
  bead->z_t_proc1->yd[bead->current_i_proc1]=0;
  for (i= work_i - param->proc1_i_min ; i< work_i + param->proc1_i_max ; i++)
  {
    bead->z_t_proc1->yd[bead->current_i_proc1] += bead->z_t->yd[i];
  }
  bead->z_t_proc1->yd[bead->current_i_proc1] /= (float) (param->proc1_i_max + param->proc1_i_min);
  return 0;
}


int set_offset_sdi_fringes(sdi_fringes *fringes, float order1_z_offset,float order0_z_offset)
{
  if(fringes->order1_last_ok != 999 && fringes->order0_last_ok != 999)
  {
    fringes->order1_last_ok = fringes->order1_last_ok - order1_z_offset;
    fringes->order0_last_ok=fringes->order0_last_ok - order0_z_offset;
  }
  return 0;
}

//input : fringes structure
//it add to the tracked position at current_i the correct 2pi multiple
int compute_z_coupled_of_sdi_fringes_at_current_i(sdi_fringes *fringes)
{
  if (fringes->order0_last_ok != 999 && fringes->order1_last_ok != 999)
  {
  find_correct_2_pi_multiple(&fringes->order0_t->yd[fringes->current_i], fringes->order1_t->yd[fringes->current_i],
                       fringes->order0_last_ok * fringes->v_0, fringes->order1_last_ok * fringes->v_1, fringes->v_0,fringes->v_1);
  }
  fringes->order0_t->yd[fringes->current_i] = fringes->order0_t->yd[fringes->current_i] / fringes->v_0;
  fringes->order1_t->yd[fringes->current_i] = fringes->order1_t->yd[fringes->current_i] / fringes->v_1;
  return 0;
}

int set_sdi_fringes_last_ok(sdi_fringes *fringes,float order0_new_ok, float order1_new_ok)
{
  fringes->order0_last_ok=order0_new_ok;
  fringes->order1_last_ok=order1_new_ok;
  return 0;
}

//input : image, fringes structures, the index current_i and the value current_t (or z depending on the experiment)
/*int compute_phase_coeff_of_sdi_fringes_at_current_i(O_i *oi,sdi_fringes *fringes, sdi_parameter *param)
{

  compute_phase_coeff_of_sdi_fringes(oi,fringes,param,phase_coeff);

  return 0;
}
*/

//input : image and fringes structures
//output : coeff of dl of spectral phase, without calibration
int compute_phase_coeff_of_sdi_fringes(O_i *oi,sdi_fringes *fringes, sdi_parameter *param)
{
  double **phase_coeff=NULL;
  float ds_y_max, ds_y_min_local, ds_x_max, contrast, ds_x_min_local;
  int dx_i, ds_x_i_max, ds_x_i_min_local;
  phase_coeff=(double **)calloc(2,sizeof(double *));
  if (fringes==NULL || param==NULL)
  {
    win_printf_OK("The parameter or the pattern of fringes is empty");
    return 1;
  }
  else
  {
  //alloc or realloc if necessary
  if (fringes->windowed_x_profile==NULL)
  {
    fringes->windowed_x_profile=build_adjust_data_set(fringes->windowed_x_profile, param->x_roi_size, param->x_roi_size);
  }
  else
  {
  if (fringes->windowed_x_profile->mx != param->x_roi_size)  fringes->windowed_x_profile=build_adjust_data_set(fringes->windowed_x_profile, param->x_roi_size, param->x_roi_size);
  }

  //prepares the one-dimensional profile
  extract_sdi_fringes_x_windowed_profile_from_image(oi, fringes, param);

  win_printf_OK("current nu0 %d %f",fringes->current_i,fringes->current_t);
  win_printf_OK("current Imax %d %f",fringes->current_I_max_i,fringes->current_I_max_t);
  win_printf_OK("current contrast %d %f",fringes->current_contrast_i,fringes->current_contrast_t);
  //computes the phase coeff
  compute_phase_coeff_of_x_profile(fringes, param, phase_coeff);
  if (fringes)
  fringes->order0_t->xd[fringes->current_i]=fringes->current_t;
  fringes->order0_t->yd[fringes->current_i]=(float) *(*phase_coeff);
  fringes->order1_t->xd[fringes->current_i]=fringes->current_t;
  fringes->order1_t->yd[fringes->current_i]=(float) *(*phase_coeff+1);

  //computes the contrast quickly
  ds_max(fringes->windowed_x_profile, &ds_y_max, &ds_x_max, &ds_x_i_max);
  if (fringes->nu0 == 999) dx_i = 10 ; //before calibration of nu0
  else dx_i = (int) (1 / fringes->nu0) ; //look over around one fringe
  ds_min_local(fringes->windowed_x_profile, &ds_y_min_local, &ds_x_min_local, &ds_x_i_min_local, ds_x_i_max - dx_i, ds_x_i_max + dx_i);
  contrast = (ds_y_max- ds_y_min_local) / (ds_y_max + ds_y_min_local);

  fringes->contrast_t->xd[fringes->current_contrast_i]=fringes->current_i;
  fringes->contrast_t->yd[fringes->current_contrast_i]=contrast;
  fringes->I_max_t->xd[fringes->current_I_max_i]=fringes->current_i;
  fringes->I_max_t->yd[fringes->current_I_max_i]=ds_y_max;

  //to erase the undesired data in real space
  if (fringes->keep_windowed_x_profile==false)
  {
    free_data_set(fringes->windowed_x_profile);
    fringes->windowed_x_profile=NULL;
  }
  }
  free(phase_coeff);
  phase_coeff=NULL;
  return 0;
}


int compute_center_by_autoconv_of_sdi_y_profile(O_i *oi,sdi_y_profile *y_profile, sdi_parameter *param, float *center)
{

  //alloc or realloc if necessary
  if (y_profile->windowed_y_profile==NULL)
  {
    y_profile->windowed_y_profile=build_adjust_data_set(y_profile->windowed_y_profile, param->y_roi_size_Y, param->y_roi_size_Y);
  }
  else
  {
  if (y_profile->windowed_y_profile->mx != param->y_roi_size_Y)  y_profile->windowed_y_profile=build_adjust_data_set(y_profile->windowed_y_profile, param->y_roi_size_Y, param->y_roi_size_Y);
  }

  extract_sdi_y_profile_y_windowed_profile_from_image(oi,y_profile,param);

  compute_center_of_autoconv_of_y_profile(y_profile, param, center);

  //to erase the undesired data in real space
  if (y_profile->keep_windowed_y_profile==false)
  {
    free_data_set(y_profile->windowed_y_profile);
    y_profile->windowed_y_profile=NULL;
  }

  return 0;
}

//computes the phase and group velocity
//expects that the xd axis contains the z knmown values
//v_0 is the sensitivity of the absolute phase (0 order coefficient of the taylor dl) in rad.µm-1 (phase velocity)
//v_1 is the sentivity of the group position (1 order) in pix.µm-1 (group velocity)
int linear_calibration_of_sdi_fringes(sdi_fringes *fringes,d_s *order0_z,d_s *order1_z)
{
  double **coeff;
  coeff=(double **)calloc(2,sizeof(double *));
  fit_ds_to_xn_polynome(order0_z, 2, -FLT_MAX, FLT_MAX, -FLT_MAX, FLT_MAX, coeff);
  fringes->v_0=(float)(*(*coeff+1));
  fit_ds_to_xn_polynome(order1_z, 2, -FLT_MAX, FLT_MAX, -FLT_MAX, FLT_MAX, coeff);
  fringes->v_1=(float)(*(*coeff+1));
  fringes->which_half=(fringes->v_0 < 0) ? 0 : 1;
  free(coeff);
  coeff=NULL;
  return 0;
}

//computes the averaged frequency of the fringes over the calibration data
int frequency_calibration_of_sdi_fringes(sdi_fringes *fringes,d_s *nu0_z)
{
  float nu0_avg;
  nu0_avg=0;
  cumulative_sum(nu0_z->yd,nu0_z->nx,&nu0_avg);
  nu0_avg = (float)((double) nu0_avg / (double) (nu0_z->nx)) ;
  set_one_sdi_fringes_nu0(fringes, nu0_avg);
  fringes->mode_nu0=1;
  return 0;
}


// computes the 0 and 1 orders coefficients of the taylor dl of the spectral phase in (omega-omega0)
//it first extracts the roi from the phase, unwrapped it (if necessary, otherwise this step does nothing) and then performs a linear fit in (omega-omega0)
//if asked, it also plots the phase and unwrapped phase to check int the plots pointed *op and *(op+1)
int ds_phase_fit_omega(sdi_fringes *fringes, double **phase_coeff,float nu0, int x_i_frac_min,int x_i_frac_max)
{
  float scale;
  float x_nu_min,x_nu_max;
  int roi_fit_size,i;
  roi_fit_size=x_i_frac_max-x_i_frac_min+1;

  if (fringes->unwrapped_phase_roi==NULL)  fringes->unwrapped_phase_roi=build_adjust_data_set(fringes->unwrapped_phase_roi,roi_fit_size,roi_fit_size);
  else
  {
    if (fringes->unwrapped_phase_roi->mx <= roi_fit_size) fringes->unwrapped_phase_roi=build_adjust_data_set(fringes->unwrapped_phase_roi,roi_fit_size,roi_fit_size);
  }

  for (i=0;i<roi_fit_size;i++)
  {
    *(fringes->unwrapped_phase_roi->xd+i)=*(fringes->phase->xd+i+x_i_frac_min);
    *(fringes->unwrapped_phase_roi->yd+i)=*(fringes->phase->yd+i+x_i_frac_min);
  }
  fringes->unwrapped_phase_roi->nx=roi_fit_size;
  fringes->unwrapped_phase_roi->ny=roi_fit_size;

  if (fringes->keep_raw_phase_roi==true)
  {
    fringes->raw_phase_roi=duplicate_data_set(fringes->unwrapped_phase_roi,fringes->raw_phase_roi);
  }
  //security to prevent fit from jumps : unwrapps the phase before linear fit (most of time this line does nothing)
  sdi_unwrap(fringes->unwrapped_phase_roi->yd,0,fringes->unwrapped_phase_roi->nx,1);
  //nu0 centered
  x_nu_min=*(fringes->unwrapped_phase_roi->xd)-nu0;
  x_nu_max=*(fringes->unwrapped_phase_roi->xd+roi_fit_size-1)-nu0;

  //scale x axis to prevent very different values between x and y
  x_nu_min = x_nu_min*2*M_PI;
  x_nu_max= x_nu_max*2*M_PI;
  scale=2 * M_PI / (x_nu_max-x_nu_min);
  offset_then_scale(fringes->unwrapped_phase_roi->xd,roi_fit_size,nu0,scale);

  if (fringes->keep_raw_phase_roi==true)
  {
    offset_then_scale(fringes->raw_phase_roi->xd,fringes->raw_phase_roi->nx,nu0,scale);//to have the same axis
  }

  fit_ds_to_xn_polynome(fringes->unwrapped_phase_roi, 2, -FLT_MAX, FLT_MAX, -FLT_MAX, FLT_MAX, phase_coeff);

  //back to physical omega, then result in pixel
  *(*phase_coeff+1)=*(*phase_coeff+1) * scale / 2 / M_PI;

  if (fringes->keep_unwrapped_phase_roi==false)
  {
    free_data_set(fringes->unwrapped_phase_roi);
    fringes->unwrapped_phase_roi=NULL;
  }
  if (fringes->keep_raw_phase_roi==false && fringes->raw_phase_roi !=NULL)
  {
    free_data_set(fringes->raw_phase_roi);
    fringes->raw_phase_roi=NULL;
  }
  return 0;
}

int compute_center_of_autoconv_of_y_profile(sdi_y_profile *y_profile, sdi_parameter *param, float *center)
{
  int autoconv_x_i_max;
  float autoconv_y_max, autoconv_x_max;
  if (y_profile->y_profile_autoconv==NULL) y_profile->y_profile_autoconv=duplicate_data_set(y_profile->windowed_y_profile,y_profile->y_profile_autoconv);
  else
  {
     if (y_profile->y_profile_autoconv->mx != param->y_roi_size_Y) y_profile->y_profile_autoconv=duplicate_data_set(y_profile->windowed_y_profile,y_profile->y_profile_autoconv);
  }
  ds_fft_then_autoconv_then_ifft_then_ifftshift(y_profile->windowed_y_profile, param->fp_y,y_profile->y_profile_autoconv);

  ds_max(y_profile->y_profile_autoconv,&autoconv_y_max,&autoconv_x_max,&autoconv_x_i_max);

  ds_find_center_by_polynomial_3(y_profile, center, autoconv_x_i_max - 2 , 5);
  *center = (*center - param->y_roi_size_2_Y) /2 ;
  *center = (*center) * param->pixel_size / y_profile->gy ;
  if (y_profile->keep_y_profile_autoconv==false)
  {
    free_data_set(y_profile->y_profile_autoconv);
    y_profile->y_profile_autoconv=NULL;
  }
  return 0;
}

int ds_find_center_by_polynomial_3(sdi_y_profile *y_profile, float *center, int x_i_min, int roi_interp_size)
{
  int i;
  float autoconv_y_max;

  if (y_profile->y_profile_autoconv_roi==NULL) y_profile->y_profile_autoconv_roi=build_adjust_data_set(y_profile->y_profile_autoconv_roi,roi_interp_size,roi_interp_size);
  else
  {
     if (y_profile->y_profile_autoconv_roi->mx != roi_interp_size) y_profile->y_profile_autoconv_roi=build_adjust_data_set(y_profile->y_profile_autoconv_roi,roi_interp_size,roi_interp_size);
  }

  for (i=0;i<roi_interp_size;i++)
  {
    *(y_profile->y_profile_autoconv_roi->xd+i)=*(y_profile->y_profile_autoconv->xd+i+x_i_min);
    *(y_profile->y_profile_autoconv_roi->yd+i)=*(y_profile->y_profile_autoconv->yd+i+x_i_min);
  }

  ds_max_interpolation_polynom_3(y_profile->y_profile_autoconv_roi, &autoconv_y_max, center);

  if (y_profile->keep_y_profile_autoconv_roi==false)
  {
    free_data_set(y_profile->y_profile_autoconv_roi);
    y_profile->y_profile_autoconv_roi=NULL;
  }
  return 0;
}

//wrapps the operations done on a profile to get the 2 first coefficients of the taylor dl of the phase
//starting from a prepared real profile (windowed and background subtracted), it computes the signal in the
//Fourier space with VC's fft (the size of the profile must be a power of 2), then it finds the peak in the spectrum
//wich corresponds to the fringes, and computes the average frequency of this peak and the roi on wich the phase will be fitted
//finally it fits the phase on this roi with a linear polynom
//mode_nu0 : if 0 we don't the average frequency of the fringes so we compute it : this is for the calibration step.
//mode_nu0 : if 1 we know it from the calibration so we use this constant value : this is the usual mode for the experiments
//mode_nu0 : if 2 we know it from the calibration and use this constant value but we still compute the frequency
//for diagnostic (for future use)
int compute_phase_coeff_of_x_profile(sdi_fringes *fringes, sdi_parameter *param,double **phase_coeff)
{
  float x_frac_min,x_frac_max,width,nu0_tmp,nu0;
  int i,x_i_frac_min,x_i_frac_max,x_i_frac_closest_min,x_i_frac_closest_max;
  d_s *x_profile_copy=NULL;

  //if keep_amplitude is true and this is not the first call, then fringes->amplitude is already allocated
  if (fringes->amplitude==NULL)  fringes->amplitude=build_adjust_data_set(fringes->amplitude,param->fp_x->n_2,param->fp_x->n_2);
  else
  {
     if (fringes->amplitude->mx != param->fp_x->n_2)  fringes->amplitude=build_adjust_data_set(fringes->amplitude,param->fp_x->n_2,param->fp_x->n_2);
  }
  if (fringes->phase==NULL) fringes->phase=build_adjust_data_set(fringes->phase,param->fp_x->n_2,param->fp_x->n_2);
  else
  {
    if (fringes->phase->mx != param->fp_x->n_2) fringes->phase=build_adjust_data_set(fringes->phase,param->fp_x->n_2,param->fp_x->n_2);
  }
  if (fringes->spectrum==NULL) fringes->spectrum=build_adjust_data_set(fringes->spectrum,param->fp_x->n_2,param->fp_x->n_2);
  else
  {
    if (fringes->spectrum->mx != param->fp_x->n_2) fringes->spectrum=build_adjust_data_set(fringes->spectrum,param->fp_x->n_2,param->fp_x->n_2);
  }
  x_profile_copy=duplicate_data_set(fringes->windowed_x_profile,x_profile_copy);
  ds_fftshift_then_fft(x_profile_copy,param->fp_x,fringes->amplitude,fringes->phase,fringes->spectrum);

  //analysis of the peak of the spectrum
  fringes->amplitude_roi=duplicate_data_set(fringes->amplitude,fringes->amplitude_roi);

  for (i=0;i<param->dc_width;i++)
  {
    fringes->amplitude_roi->yd[i]=0;
  }

  ds_peak_width(fringes->amplitude_roi,param->nu_threshold,&x_frac_min,&x_i_frac_min,&x_i_frac_closest_min,
                                     &x_frac_max,&x_i_frac_max,&x_i_frac_closest_max,&width);
  //case 0  : during the calibration step, computation of the frequency to find the average frequency.
  //case 1 is the normal case
  //case 2 is used if one wants to plot the frequency variation during the experiment
  switch (fringes->mode_nu0) {
    case 0 :
    {
      ds_measure_frequency(fringes->spectrum,&nu0_tmp,x_i_frac_closest_min,x_i_frac_closest_max,1);
      nu0=nu0_tmp;
      fringes->nu0_t->yd[fringes->current_i]=nu0;
      fringes->nu0_t->xd[fringes->current_i]=fringes->current_t;
      break;
    }
    case 1 :
    {
      nu0=fringes->nu0;
      break;
    }
    case 2 :
    {
      ds_measure_frequency(fringes->spectrum,&nu0_tmp,x_i_frac_closest_min,x_i_frac_closest_max,1);
      nu0=fringes->nu0;
      break;
    }
  }

  //linear fit of a part of the phase
  ds_phase_fit_omega(fringes, phase_coeff, nu0, x_i_frac_min, x_i_frac_max);

  if (fringes->keep_spectrum_log==true)
  {
    fringes->spectrum_log=duplicate_data_set(fringes->spectrum,fringes->spectrum_log);
    ds_log(fringes->spectrum_log);
  }
  else
  {
     if (fringes->spectrum_log != NULL)
     {
        free_data_set(fringes->spectrum_log);
        fringes->spectrum_log=NULL;
     }
  }

  if (fringes->keep_spectrum==false)
  {
    free_data_set(fringes->spectrum);
    fringes->spectrum=NULL;
  }
  if (fringes->keep_amplitude==false)
  {
    free_data_set(fringes->amplitude);
    fringes->amplitude=NULL;
  }
  if (fringes->keep_phase==false)
  {
    free_data_set(fringes->phase);
    fringes->phase=NULL;
  }
  if (fringes->keep_amplitude_roi==false)
  {
    free_data_set(fringes->amplitude_roi);
    fringes->amplitude_roi=NULL;
  }

  free_data_set(x_profile_copy);
  x_profile_copy=NULL;
  return 0;
}


int extract_sdi_y_profile_y_windowed_profile_from_image(O_i *oi,sdi_y_profile *y_profile,sdi_parameter *param)
{
  int i,x_profile_max_index;
  float x_profile_max_y,x_profile_max_x;

  //at this point of the treatment, y_profile->windowed_y_profile must have been allocated previously
  extract_y_profile_from_image(oi,y_profile->windowed_y_profile,y_profile->x_roi_min,y_profile->x_roi_max,y_profile->y_roi_min,y_profile->y_roi_max);

  //to keep a copy of the raw profile in memory if asked. duplicate_data_set can allocate if raw_x_profile is initially =NULL
  if (y_profile->keep_raw_y_profile==true)
  {
    y_profile->raw_y_profile=duplicate_data_set(y_profile->windowed_y_profile,y_profile->raw_y_profile);
  }

  //windowing the signal by an hypergaussian function
  if (y_profile->y_window==NULL || y_profile->y_window->mx != param->y_roi_size_Y)
  {
    y_profile->y_window=duplicate_data_set(y_profile->windowed_y_profile,y_profile->y_window);
  }
  ds_max(y_profile->windowed_y_profile,&x_profile_max_y,&x_profile_max_x,&x_profile_max_index);
  generate_hypergaussian(y_profile->y_window, param->y_roi_size_2_Y - param->y_window_fwhm, param->y_roi_size_2_Y + param->y_window_fwhm,param->y_window_order);
  for (i=0;i<param->y_roi_size_Y;i++)
  {
      y_profile->windowed_y_profile->yd[i]= (y_profile->windowed_y_profile->yd[i]-y_profile->background) * y_profile->y_window->yd[i] ;
  }

  if (y_profile->keep_y_window==false && y_profile->y_window !=NULL)
  {
    free_data_set(y_profile->y_window);
    y_profile->y_window=NULL;
  }
  else
  {
    offset_then_scale(y_profile->y_window->yd,y_profile->y_window->nx,0,x_profile_max_y);
  }
  //if previously there was something that we want to erase now
  if (y_profile->keep_raw_y_profile==false && y_profile->raw_y_profile !=NULL)
  {
    free_data_set(y_profile->raw_y_profile);
    y_profile->raw_y_profile=NULL;
  }

  return 0;
}


//this wrapps the sdi operations in the real space : it first extracts the averaged over several lines profile of fringes along x
//then it substrcts the background, windows it with an hypergaussian
//all datasets can be kept in memory if asked
//it also computes the contrast with a simple direct calculation
int extract_sdi_fringes_x_windowed_profile_from_image(O_i *oi,sdi_fringes *fringes,sdi_parameter *param)
{
  int i,x_profile_max_index;
  float x_profile_max_y,x_profile_max_x;

  //at this point of the treatment, fringes->windowed_x_profile must have been allocated previously
  extract_x_profile_from_image(oi,fringes->windowed_x_profile,fringes->x_roi_min,fringes->x_roi_max,fringes->y_roi_min,fringes->y_roi_max);

  //keep a copy of the raw profile in memory if asked. duplicate_data_set can allocate if raw_x_profile is initially =NULL
  if (fringes->keep_raw_x_profile==true)
  {
    fringes->raw_x_profile=duplicate_data_set(fringes->windowed_x_profile,fringes->raw_x_profile);
  }

  //windowing the signal by an hypergaussian function
  if (fringes->x_window==NULL || fringes->x_window->mx != param->x_roi_size)
  {
    fringes->x_window=duplicate_data_set(fringes->windowed_x_profile,fringes->x_window);
  }
  ds_max(fringes->windowed_x_profile,&x_profile_max_y,&x_profile_max_x,&x_profile_max_index);
  generate_hypergaussian(fringes->x_window,x_profile_max_index - param->x_window_fwhm,x_profile_max_index + param->x_window_fwhm,param->x_window_order);

  for (i=0;i<param->x_roi_size;i++)
  {
      fringes->windowed_x_profile->yd[i]= (fringes->windowed_x_profile->yd[i]-fringes->background) * fringes->x_window->yd[i] ;
  }

  if (fringes->keep_x_window==false && fringes->x_window !=NULL)
  {
    free_data_set(fringes->x_window);
    fringes->x_window=NULL;
  }
  else
  {
    offset_then_scale(fringes->x_window->yd,fringes->x_window->nx,0,x_profile_max_y);
  }
  //if previously there was something that we want to erase now
  if (fringes->keep_raw_x_profile==false && fringes->raw_x_profile !=NULL)
  {
    free_data_set(fringes->raw_x_profile);
    fringes->raw_x_profile=NULL;
  }

  return 0;
}


//group of
int set_sdi_y_profile_gy(sdi_y_profile *y_profile, float gy)
{
  if (y_profile==NULL) return D_O_K;
    y_profile->gy=gy;
  return 0;}
int set_sdi_y_profile_x_roi(sdi_y_profile *y_profile, int x_roi_size_2_Y)
{
  if (y_profile==NULL) return D_O_K;
    y_profile->x_roi_min=y_profile->xc-x_roi_size_2_Y;
    y_profile->x_roi_max=y_profile->xc+x_roi_size_2_Y;
  return 0;
}
int set_sdi_y_profile_y_roi(sdi_y_profile *y_profile, int y_roi_size_2_Y)
{
  if (y_profile==NULL) return D_O_K;
    y_profile->y_roi_min=y_profile->yc-y_roi_size_2_Y;
    y_profile->y_roi_max=y_profile->yc+y_roi_size_2_Y;
  return 0;
}
int set_one_sdi_y_profile_centrum(sdi_y_profile *y_profile,int xc, int yc)
{
  if (y_profile==NULL) return D_O_K;
  y_profile->xc=xc;
  y_profile->yc=yc;
  return 0;
}
int set_sdi_y_profile_background(sdi_y_profile *y_profile, float background)
{
  if (y_profile==NULL) return D_O_K;
  y_profile->background=background;
  return 0;
}
int set_sdi_y_profile_keep_data(sdi_y_profile *y_profile, int which_data, bool choice)
{
  if (y_profile==NULL)  return D_O_K;
  switch(which_data)
   {
     case KEEP_RAW_Y_PROFILE :
     y_profile->keep_raw_y_profile=choice;
     break;
     case KEEP_WINDOWED_Y_PROFILE :
     y_profile->keep_windowed_y_profile=choice;
     break;
     case KEEP_Y_WINDOW :
     y_profile->keep_y_window=choice;
     break;
     case KEEP_AUTOCONV :
     y_profile->keep_y_profile_autoconv=choice;
     break;
     case KEEP_AUTOCONV_ROI :
     y_profile->keep_y_profile_autoconv_roi=choice;
     break;
   }
   return 0;
}

//update some data of the sdi fringes with the sdi parameters
int set_sdi_fringes_x_roi(sdi_fringes *fringes, int x_roi_size_2)
{
  if (fringes==NULL) return D_O_K;
    fringes->x_roi_min=fringes->xc-x_roi_size_2;
    fringes->x_roi_max=fringes->xc+x_roi_size_2;
  return 0;
}
int set_sdi_fringes_y_roi(sdi_fringes *fringes, int y_roi_size_2)
{
  if (fringes==NULL) return D_O_K;
    fringes->y_roi_min=fringes->yc-y_roi_size_2;
    fringes->y_roi_max=fringes->yc+y_roi_size_2;
  return 0;
}
int set_sdi_fringes_mode_nu0(sdi_fringes *fringes, int mode)
{
  if (fringes==NULL) return D_O_K;
  fringes->mode_nu0=mode;
  return 0;
}
int set_sdi_fringes_background(sdi_fringes *fringes, float background)
{
  if (fringes==NULL) return D_O_K;
  fringes->background=background;
  return 0;
}
int set_one_sdi_fringes_v0(sdi_fringes *fringes, float v_0)
{
  if (fringes==NULL) return D_O_K;
  fringes->v_0=v_0;
  return 0;
}
int set_one_sdi_fringes_v1(sdi_fringes *fringes, float v_1)
{
  if (fringes==NULL) return D_O_K;
  fringes->v_1=v_1;
  return 0;
}
int set_one_sdi_fringes_centrum(sdi_fringes *fringes,int xc, int yc)
{
  if (fringes==NULL) return D_O_K;
  fringes->xc=xc;
  fringes->yc=yc;
  return 0;
}
int set_one_sdi_fringes_nu0(sdi_fringes *fringes,float nu0)
{
  if (fringes==NULL) return D_O_K;
  fringes->nu0=nu0;
  return 0;
}

int set_sdi_fringes_keep_data(sdi_fringes *fringes, int which_data, bool choice)
{
  if (fringes==NULL)  return D_O_K;

 switch(which_data)
  {
    case KEEP_NU0_T :
    fringes->keep_nu0_t=choice;
    break;
    case KEEP_ORDER0_T :
    fringes->keep_order0_t=choice;
    break;
    case KEEP_ORDER1_T :
    fringes->keep_order1_t=choice;
    break;
    case KEEP_WINDOWED_X_PROFILE :
    fringes->keep_windowed_x_profile=choice;
    break;
    case KEEP_RAW_X_PROFILE :
    fringes->keep_raw_x_profile=choice;
    break;
    case KEEP_X_WINDOW :
    fringes->keep_x_window=choice;
    break;
    case KEEP_SPECTRUM :
    fringes->keep_spectrum=choice;
    break;
    case KEEP_SPECTRUM_LOG :
    fringes->keep_spectrum_log=choice;
    break;
    case KEEP_AMPLITUDE :
    fringes->keep_amplitude=choice;
    break;
    case KEEP_AMPLITUDE_ROI :
    fringes->keep_amplitude_roi=choice;
    break;
    case KEEP_PHASE :
    fringes->keep_phase=choice;
    break;
    case KEEP_UNWRAPPED_PHASE_ROI :
    fringes->keep_unwrapped_phase_roi=choice;
    break;
    case KEEP_RAW_PHASE_ROI :
    fringes->keep_raw_phase_roi=choice;
    break;
    case KEEP_ORDER0_T_PROC1 :
    fringes->keep_order0_t_proc1=choice;
    break;
    case KEEP_ORDER1_T_PROC1 :
    fringes->keep_order1_t_proc1=choice;
    break;

  }
  return 0;
}
