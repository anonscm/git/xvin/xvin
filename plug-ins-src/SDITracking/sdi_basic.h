#pragma once
# include "../fft2d/fft2d.h"*/
# include "../fft2d/fftbtl32n.h"
# include "xvin.h"

//this declares the basic functions, ie whose input is not a strcture, for sdi treatments

PXV_FUNC(int, estimate_mean, (float *tab, int n, float *mean));
PXV_FUNC(int, estimate_std, (float *tab, int n, float *std));

//work on the result of the linear fit of the spectral phase
PXV_FUNC(int, find_correct_2_pi_multiple, (float *phi0_1,float phi1_1,float phi0_0,float phi1_0,float v_0,float v_1));

//thise works on the spectral phase
PXV_FUNC(int, offset_then_scale, (float *tab,int tab_size,float offset,float scale));
PXV_FUNC(int, sdi_unwrap, (float *phase, int i_min, int i_max, float tol));
PXV_FUNC(int, raw_spectral_phase_fftbt, ( float *tf_complex, float *phase, int i_min, int i_max));

//analysis of the peak of the spectrum
PXV_FUNC(int, ds_peak_width, (const d_s *ds,float frac,float *x_frac_min,int *x_i_frac_min,int *x_i_frac_closest_min,
                       float *x_frac_max,int *x_i_frac_max,int *x_i_frac_closest_max,float *width));
PXV_FUNC(int, ds_measure_frequency,(d_s *spectrum,float *nu0,int x_i_frac_closest_min,int x_i_frac_closest_max,int dx_i_fac));
PXV_FUNC(int, ds_mu_1, (const d_s *ds, float *mu1));
PXV_FUNC(int, ds_normalize_by_sum,(d_s *spectrum));
PXV_FUNC(int, cumulative_sum, (const float *tab,int size_tab,float *sum));
PXV_FUNC(int, ds_log,(d_s *ds));
PXV_FUNC(int, ds_max_interpolation_polynom_3, (const d_s *ds,float *ds_y_max,float *ds_x_max));
PXV_FUNC(int, ds_max_interpolation_polynom_2, (const d_s *ds,float *ds_y_max,float *ds_x_max));
PXV_FUNC(int, ds_max,(const d_s *ds,float *ds_y_max,float *ds_x_max,int *ds_x_i_max));
PXV_FUNC(int, ds_min,(const d_s *ds,float *ds_y_min,float *ds_x_min,int *ds_x_i_min));
PXV_FUNC(int, ds_min_local, (const d_s *ds,float *ds_y_min,float *ds_x_min,int *ds_x_i_min, int x_i_roi_min, int x_i_roi_max));
PXV_FUNC(int, ds_max_local, (const d_s *ds,float *ds_y_max,float *ds_x_max,int *ds_x_i_max, int x_i_roi_min, int x_i_roi_max));

//works on the real profile
PXV_FUNC(int, ds_fft_then_autoconv_then_ifft_then_ifftshift, (d_s *profile, fft_plan *fp, d_s *auto_conv));
PXV_FUNC(int, ds_fftshift_then_fft,(d_s *profile, fft_plan *fp,d_s *amplitude,d_s *phase,d_s *spectrum));
PXV_FUNC(int, fftshift, (float *tab,int size_2));
PXV_FUNC(int, generate_hypergaussian, (d_s* ds,float fwhm_min,float fwhm_max,int ordre));
PXV_FUNC(int, extract_x_profile_from_image, (O_i *oi, d_s *profile,int x_i_min,int x_i_max,int y_i_min,int y_i_max));
PXV_FUNC(int, extract_y_profile_from_image, (O_i *oi, d_s *profile,int x_i_min,int x_i_max,int y_i_min,int y_i_max));
