#pragma once

#define SDI_MAX_NUMBER_OF_PATTERN 10000

PXV_FUNC(int, remove_one_sdi_display, (sdi_disp *display));
PXV_FUNC(int, remove_plot_from_plr_without_free_ds, (pltreg *pr, O_p *op));
PXV_FUNC(int, free_one_plot_without_free_ds, (O_p *op));
PXV_FUNC(int,	remove_ds_from_op_without_free_ds, (O_p *op, d_s *ds));
PXV_FUNC(int, sdi_extract_char_subimage_from_char_image, (O_i *roi,O_i *src,int roi_xc, int roi_yc, int roi_size_x, int roi_size_y, int roi_size_x_2, int roi_size_y_2));
PXV_FUNC(int, sdi_fringes_color_choice, (int *color, sdi_fringes *fringes));
PXV_FUNC(int, allocate_sdi_fringes_t_fields, (sdi_fringes *fringes, int buffer_size, int *which_data, int n_which_data));

PXV_FUNC(int, display_one_sdi_fringes_data, (sdi_fringes *fringes));
PXV_FUNC(int, display_one_bead_data, (sdi_bead *bead));
PXV_FUNC(int, display_one_y_profile_data, (sdi_y_profile *y_profile));
PXV_FUNC(int, set_current_point_of_sdi_fringes_field, (sdi_fringes *fringes,int which_data,int current_i,int current_t));
PXV_FUNC(int, switch_ptr_to_sdi_fringes_field, (sdi_fringes *fringes,int which_data,d_s **ds));
PXV_FUNC(int, switch_ptr_to_sdi_y_profile_field, (sdi_y_profile *y_profile,int which_data,d_s **ds));
PXV_FUNC(int, switch_ptr_to_sdi_bead_field, (sdi_bead *bead,int which_data,d_s **ds));
PXV_FUNC(int, allocate_sdi_bead_t_fields, (sdi_bead *bead, int buffer_size, int *which_data, int n_which_data));

PXV_FUNC(int, add_sdi_y_profile_sdi_one_display, (sdi_y_profile *y_profile, int which_data, sdi_disp *display));
PXV_FUNC(int, add_sdi_bead_sdi_one_display, (sdi_bead *bead, int which_data, sdi_disp *display));
PXV_FUNC(int, add_sdi_fringes_sdi_one_display, (sdi_fringes *fringes, int which_data, sdi_disp *display));
PXV_FUNC(int, add_sdi_one_display, (d_s *ds, pltreg **plr, O_p **op, char *name));
PXV_FUNC(int, place_multiple_double_fast_rect_with_mouse, (int *xc, int *yc,int half_length,int half_width,int x_dist,int y_dist, int color, imreg *imr, O_i *oi, BITMAP *imb));
PXV_FUNC(int, place_multiple_fast_rect_with_mouse, (int *xc, int *yc,int half_length,int half_width,int color, imreg *imr, O_i *oi, BITMAP *imb));
PXV_FUNC(int, draw_one_fast_rect_with_user_pix, (int xc,int yc,int half_length, int half_width,int color, imreg *imr, O_i *oi, BITMAP *imb));
PXV_FUNC(int, place_one_fast_rect_with_mouse, (int *xc, int *yc,int half_length,int half_width,int color, imreg *imr, O_i *oi, BITMAP *imb));

PXV_FUNC(int, sdi_keyboard_shortcut,(void));
PXV_FUNC(int, add_text_vga_screen_unit, (BITMAP *bmp, int xc, int yc, const char *format, ...));
PXV_FUNC(int, x_user_pix_2_vga_screen_unit, (imreg *imr,O_i *oi,int x));
PXV_FUNC(int, dx_user_pix_2_vga_screen_unit, (imreg *imr,O_i *oi,int dx));
PXV_FUNC(int, y_user_pix_2_vga_screen_unit, (imreg *imr,O_i *oi,int y, BITMAP *imb));
PXV_FUNC(int, dy_user_pix_2_vga_screen_unit, (imreg *imr,O_i *oi,int dy));
PXV_FUNC(int, draw_one_fast_rect_vga_screen_unit, (int xc, int yc, int half_length, int half_width, int color, BITMAP* bm));
PXV_FUNC(int, draw_one_ellipse_vga_screen_unit, (int xc, int yc, int radius_x, int radius_y, int color, BITMAP* bmp));
