# include "SDITracking.h"

# include <allegro.h>
# include "xvin.h"
# include <float.h>
# include <unitset.h>

/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "SDITracking.h"

//dump in the same dir as the working one
#define FILE "debug.h5"
hdf5_gen *hdf5_manager=NULL;
bool record_hdf5 = false;

pthread_t rawhid_thread_id;
bool continous_listening = false;
rawhid_g_data *g_data=NULL;

typedef struct toto_test
{
  char a;
  float b;
}__attribute__((__packed__)) toto;

int do_test_cast(void)
{
  if (updating_menu_state!=0) return D_O_K;
  toto dat;
  dat.a=13;
  dat.b=5.6789;
  char c[9];
  memcpy(c,&dat,sizeof(dat));
  toto deb;
  float *tmp;
  tmp = (float *)&c[3];
  *tmp = 6.891;
  //memcpy(&deb,&dat,sizeof(dat));
  memcpy(&deb,c,9*sizeof(char));
  win_printf_OK("%d %f",(deb.a),(deb.b));
  return 0;
}

int rawhid_init(void)
{
  int r;
  r = rawhid_open(1, 0x16C0, 0x0480, 0xFFAB, 0x0200);
  if (r <= 0)
  {
    r = rawhid_open(1, 0x16C0, 0x0486, 0xFFAB, 0x0200);
    if (r <= 0)
    {
      win_printf_OK("no rawhid device found\n");
      return -1;
    }
  }
  //win_printf_OK("rawhid channel opened");
  return 0;
}
int do_rawhid_init(void)
{
  if (updating_menu_state!=0) return D_O_K;
  int success = -1;
  success = rawhid_init();
  return 0;
}
int do_rawhid_close(void)
{
  if (updating_menu_state!=0) return D_O_K;
  rawhid_close(0);
  return 0;
}


int convert_report_to_char_2(report_data_2 *msg_in, char *msg_out)
{
  //memcpy(msg_out, msg_in, 64*sizeof(char));
  int ci=0;
  unsigned long *tmp_lu;
  float *tmp_fl;
  char *tmp_c;
  msg_out[0] = msg_in->msg_type;
  tmp_lu = (unsigned long *) &msg_out[1];
  tmp_lu[0] = msg_in->error_code;
  tmp_c = (char *) &tmp_lu[1];
  tmp_c[0] = msg_in->index_Temperature;
  tmp_lu = (unsigned long *) &tmp_c[1];
  tmp_lu[0] = msg_in->timestamp;
  tmp_c = (char *) &tmp_lu[1];
  tmp_c[0] = msg_in->ZmagObj_status;
  tmp_c[1] = msg_in->xy_status;
  tmp_fl = (float *) &tmp_c[2];
  tmp_fl[0] = msg_in->zmag;
  tmp_fl[1] = msg_in->vmag;
  tmp_fl[2] = msg_in->zobj;
  tmp_fl[3] = msg_in->x;
  tmp_fl[4] = msg_in->y;
  tmp_fl[5] = msg_in->TemperatureTbox;
  tmp_fl[6] = msg_in->TemperatureMag;
  tmp_fl[7] = msg_in->TemperatureSink;
  tmp_fl[8] = msg_in->intensity_led1;
  tmp_fl[9] = msg_in->intensity_led2;
  return 0;
}
int convert_report_to_char(report_data *msg_in, char *msg_out)
{
  //if ((msg_in->msg_type != 'R') && (msg_in->msg_type != 'W')) return -2;
  int ci=0;
  float *tmp;
  msg_out[0] = msg_in->error_code;
  msg_out[1] = msg_in->msg_type;
  msg_out[2] = msg_in->T0_status;
  msg_out[3] = msg_in->T3_status;
  msg_out[4] = msg_in->zmag_status;
  msg_out[5] = msg_in->zobj_status;
  ci = 6;
  tmp = (float *) &msg_out[ci];
  ci = 0;
  tmp[ci] = msg_in->timestamp;
  tmp[++ci] = msg_in->freq;
  tmp[++ci] = msg_in->T0;
  tmp[++ci] = msg_in->T1;
  tmp[++ci] = msg_in->T2;
  tmp[++ci] = msg_in->T3;
  tmp[++ci] = msg_in->T4;
  tmp[++ci] = msg_in->zmag;
  tmp[++ci] = msg_in->vmag;
  tmp[++ci] = msg_in->zobj;
  tmp[++ci] = msg_in->vobj;
  return 0;
}
int convert_char_to_report_2(report_data_2 *msg_out, char *msg_in)
{
  //memcpy( msg_out, msg_in, sizeof(msg_in);//bordel je comprends pas pourquoi ca marche pas!!!
  int ci=0;
  unsigned long *tmp_lu;
  float *tmp_fl;
  msg_out->msg_type =(unsigned char) msg_in[ci];
  ci += sizeof(unsigned char);
  tmp_lu = (unsigned long *)(msg_in + ci);
  msg_out->error_code  = tmp_lu[0];
  ci+=sizeof(unsigned long);
  msg_out->index_Temperature =(unsigned char) msg_in[ci];
  ci+=sizeof(char);
  tmp_lu = (unsigned long *)(msg_in + ci);
  msg_out->timestamp  = tmp_lu[0];
  ci+=sizeof(unsigned long);
  msg_out->ZmagObj_status = (char) msg_in[ci];
  ci+=sizeof(char);
  msg_out->xy_status = (char) msg_in[ci];
  ci+=sizeof(char);
  tmp_fl = (float *)(msg_in + ci);
  msg_out->zmag = (float) tmp_fl[0];
  msg_out->vmag = (float) tmp_fl[1];
  msg_out->zobj = (float) tmp_fl[2];
  msg_out->x = (float) tmp_fl[3];
  msg_out->y = (float) tmp_fl[4];
  msg_out->TemperatureTbox = (float) tmp_fl[5];
  msg_out->TemperatureMag = (float) tmp_fl[6];
  msg_out->TemperatureSink = (float) tmp_fl[7];
  msg_out->intensity_led1 = (float) tmp_fl[8];
  msg_out->intensity_led2 = (float) tmp_fl[9];
  return 0;
}
int convert_char_to_report(report_data *msg_out, char *msg_in)
{
  //if ((msg_in[1] != 'R') && (msg_in[1] != 'W')) return -2;
  float *tmp = NULL;
  int ci=0;
  msg_out->error_code = msg_in[0];

  if (msg_out->error_code < 0) return -1;
  msg_out->msg_type = msg_in[1];
  if (msg_out->msg_type != 'R') return -2;
  msg_out->T0_status = msg_in[2];
  msg_out->T3_status = msg_in[3];
  msg_out->zmag_status = msg_in[4];
  msg_out->zobj_status = msg_in[5];
  ci=6;
  tmp = (float *) (msg_in+ci);
  ci=0;
  msg_out->timestamp = tmp[ci];
  msg_out->freq = tmp[++ci];
  msg_out->T0 = tmp[++ci];
  msg_out->T1 = tmp[++ci];
  msg_out->T2 = tmp[++ci];
  msg_out->T3 = tmp[++ci];
  msg_out->T4 = tmp[++ci];
  msg_out->zmag = tmp[++ci];
  msg_out->vmag = tmp[++ci];
  msg_out->zobj = tmp[++ci];
  return 0;
}
int copy_report_data_2(report_data_2 *out, report_data_2 *in)
{
  out = in;
  return 0;
}
int copy_report_data(report_data *out, report_data *in)
{
  out->error_code = in->error_code ;
  out->msg_type = in->msg_type;
  out->T0_status = in->T0_status;
  out->T3_status = in->T3_status;
  out->zmag_status = in->zmag_status;
  out->zobj_status = in->zobj_status;
  out->timestamp = in->timestamp;
  out->freq = in->freq;
  out->T0 = in->T0;
  out->T1 = in->T1;
  out->T2 = in->T2;
  out->T3 = in->T3;
  out->T4 = in->T4;
  out->zmag = in->zmag;
  out->vmag = in->vmag;
  out->zobj = in->zobj;
  out->vobj = in->vobj;
  return 0;
}
int do_rawhid_manual_async_cmd(void)
{
  if(updating_menu_state != 0)	return D_O_K;
  char msg[PACKET_SIZE] = {0};
  //report_data *async_cmd = NULL;
  report_data_2 *async_cmd = NULL;
  //async_cmd = (report_data *)calloc(1, sizeof(report_data));
  async_cmd = (report_data_2 *)calloc(1, sizeof(report_data_2));
  int i;
  static float zmag_cmd = 0;
  static float zobj_cmd = 0;
  static float vmag_cmd = 1;
  //copy_report_data(async_cmd, g_data->r_data[g_data->current_report_data]);
  copy_report_data_2(async_cmd, g_data->r_data[g_data->current_report_data]);
  if ( (i=win_scanf("zmag%f vmag%f zobj%f", &zmag_cmd, &vmag_cmd, &zobj_cmd))==WIN_CANCEL ) return D_O_K;
  async_cmd->msg_type = 'W';
  async_cmd->zmag = zmag_cmd;
  async_cmd->vmag = vmag_cmd;
  async_cmd->zobj = zobj_cmd;

  async_cmd->ZmagObj_status = 0x10;//zmag
  async_cmd->ZmagObj_status |= 0x20;//zobj change
  async_cmd->ZmagObj_status |= 0x40;//vmag change
  //convert_report_to_char(async_cmd, msg);
  convert_report_to_char_2(async_cmd, msg);
  i = rawhid_send(0, (char *)msg, PACKET_SIZE, 5);
  if (i != PACKET_SIZE)  win_printf("not right cmd length, %d", i);
  free(async_cmd);
  async_cmd = NULL;
  return 0;
}


int init_rawhid_data(void)
{
  int i;
  if (g_data==NULL)
  {
    g_data = (rawhid_g_data *)calloc(1, sizeof(rawhid_g_data));
    g_data->current_report_data = 0;
    g_data->n_report_data = RAWHID_RAW_BUFFER;
    g_data->current_report_data = 0;
    //g_data->r_data = (report_data **)calloc(g_data->n_report_data, sizeof(report_data *));
    g_data->r_data = (report_data_2 **)calloc(g_data->n_report_data, sizeof(report_data_2 *));
    //for (i=0;i<g_data->n_report_data;i++) g_data->r_data[i] = (report_data *)calloc(1, sizeof(report_data));
    for (i=0;i<g_data->n_report_data;i++) g_data->r_data[i] = (report_data_2 *)calloc(1, sizeof(report_data_2));
  }
  /*
  //to record hdf5 data
  herr_t status;
  char name[32];
  snprintf(name,32,"teensy_data.h5");
  //manager of hdf5 objects
  hdf5_manager = (hdf5_gen *)calloc(1, sizeof(hdf5_gen));
  hdf5_manager->n_hdf5_1d = 0;
  snprintf(hdf5_manager->filename, 512, name);
  hid_t       file_id;
  file_id = H5Fcreate(name, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
  status = H5Fclose(file_id);*/
  return 0;
}

int rawhid_launch_continous_listening(void)
{
  int success = -1;
  int n_byte;
  char msg_buffer[PACKET_SIZE];
  int rawhid_timeout = 10;
  int ret;
  static int count = 0;
  int refresh_period = 2048;
  int index_prev;
  int i;
  hid_t file_id;
  //creation of datasets + saving
  /*add_one_1d_hdf5_to_manager();
  init_hdf5_1d(hdf5_manager->hdf5_1d_list[hdf5_manager->n_hdf5_1d-1], hdf5_manager->filename, "index", NULL, NULL, PTR_DATATYPE_FLOAT, (int) (ROLLING_BUFFER_SIZE / 2) );
  attach_1d_float_to_hdf5_1d(hdf5_manager->hdf5_1d_list[hdf5_manager->n_hdf5_1d-1], timestamp->xd);
  add_one_1d_hdf5_to_manager();
  init_hdf5_1d(hdf5_manager->hdf5_1d_list[hdf5_manager->n_hdf5_1d-1], hdf5_manager->filename, "timestamp", NULL, NULL, PTR_DATATYPE_FLOAT, (int) (ROLLING_BUFFER_SIZE / 2) );
  attach_1d_float_to_hdf5_1d(hdf5_manager->hdf5_1d_list[hdf5_manager->n_hdf5_1d-1], timestamp->yd);
*/
  O_p *op_timestamp = NULL;
  pltreg *plr = NULL;
  d_s *timestamp = NULL;
  plr = create_hidden_pltreg_with_op(&op_timestamp, 1, 1, 0, "teensy data");
  timestamp = build_adjust_data_set(timestamp, 1, 1);
  add_data_to_one_plot(op_timestamp, IS_DATA_SET, timestamp);
  remove_ds_from_op(op_timestamp, op_timestamp->dat[0]);
  set_plot_title(op_timestamp, "timestamp");

  O_p *op_dt_timestamp = NULL;
  d_s *dt_timestamp = NULL;
  dt_timestamp = build_adjust_data_set(dt_timestamp, 1, 1);
  op_dt_timestamp = create_and_attach_one_plot(plr,1,1,0);
  add_data_to_one_plot(op_dt_timestamp, IS_DATA_SET, dt_timestamp);
  remove_ds_from_op(op_dt_timestamp, op_dt_timestamp->dat[0]);
  set_plot_title(op_dt_timestamp, "difference of consecutive timestamps");

  O_p *op_zmag = NULL;
  d_s *zmag = NULL;
  op_zmag = create_and_attach_one_plot(plr,1,1,0);
  zmag = build_adjust_data_set(zmag, 1, 1);
  add_data_to_one_plot(op_zmag, IS_DATA_SET, zmag);
  remove_ds_from_op(op_zmag, op_zmag->dat[0]);
  set_plot_title(op_zmag, "zmag");

  O_p *op_vmag = NULL;
  d_s *vmag = NULL;
  op_vmag = create_and_attach_one_plot(plr,1,1,0);
  vmag = build_adjust_data_set(vmag, 1, 1);
  add_data_to_one_plot(op_vmag, IS_DATA_SET, vmag);
  remove_ds_from_op(op_vmag, op_vmag->dat[0]);
  set_plot_title(op_vmag, "vmag");

  O_p *op_zobj = NULL;
  d_s *zobj = NULL;
  op_zobj = create_and_attach_one_plot(plr,1,1,0);
  zobj = build_adjust_data_set(zobj, 1, 1);
  add_data_to_one_plot(op_zobj, IS_DATA_SET, zobj);
  remove_ds_from_op(op_zobj, op_zobj->dat[0]);
  set_plot_title(op_zobj, "zobj");

  O_p *op_zmagobj_status = NULL;
  d_s *zmagobj_status = NULL;
  op_zmagobj_status = create_and_attach_one_plot(plr,1,1,0);
  zmagobj_status = build_adjust_data_set(zmagobj_status, 1, 1);
  add_data_to_one_plot(op_zmagobj_status, IS_DATA_SET, zmagobj_status);
  remove_ds_from_op(op_zmagobj_status, op_zmagobj_status->dat[0]);
  set_plot_title(op_zmagobj_status, "zmagzobj_status");

  if ( (success=rawhid_init()) == -1 ) return D_O_K;
  while (true)
  {
    if ( (continous_listening == true) )
    {
      n_byte = rawhid_recv(0, msg_buffer, PACKET_SIZE, rawhid_timeout);
      if (n_byte == PACKET_SIZE)
      {
        if (msg_buffer[0] == 'R')
        {
          //ret = convert_char_to_report(g_data->r_data[g_data->current_report_data], msg_buffer);
          ret = convert_char_to_report_2(g_data->r_data[g_data->current_report_data], msg_buffer);
          g_data->current_report_data = (g_data->current_report_data)%g_data->n_report_data;
          count++;
          my_set_window_title("count %d %f", count, (float) g_data->r_data[g_data->current_report_data]->timestamp);
        }
        /*timestamp->xd[count%ROLLING_BUFFER_SIZE] = count;
        timestamp->yd[count%ROLLING_BUFFER_SIZE] = g_data->r_data[g_data->current_report_data]->timestamp;
        zmag->xd[count%ROLLING_BUFFER_SIZE] = count;
        zmag->yd[count%ROLLING_BUFFER_SIZE] = g_data->r_data[g_data->current_report_data]->zmag;
        dt_timestamp->xd[count%ROLLING_BUFFER_SIZE] = count;
        index_prev = (count%ROLLING_BUFFER_SIZE == 0) ? (ROLLING_BUFFER_SIZE-1) : (count - 1);
        dt_timestamp->yd[count%ROLLING_BUFFER_SIZE] = timestamp->yd[count] - timestamp->yd[index_prev];*/
        if (count==0)
        {
          timestamp->xd[0] = (float) count;
          timestamp->yd[0] = (float) g_data->r_data[g_data->current_report_data]->timestamp;
          dt_timestamp->xd[0] = (float) count;
          dt_timestamp->yd[0] = 0;
          zmag->xd[0] = (float) count;
          zmag->yd[0] = g_data->r_data[g_data->current_report_data]->zmag;
          vmag->xd[0] = (float) count;
          vmag->yd[0] = g_data->r_data[g_data->current_report_data]->vmag;
          zobj->xd[0] = (float) count;
          zobj->yd[0] = g_data->r_data[g_data->current_report_data]->zobj;
          zmagobj_status->xd[0] = (float) count;
          zmagobj_status->yd[0] = g_data->r_data[g_data->current_report_data]->ZmagObj_status;
        }
        else
        {
          add_new_point_to_ds(timestamp, (float) count, (float) g_data->r_data[g_data->current_report_data]->timestamp);
          add_new_point_to_ds(dt_timestamp, (float) count, timestamp->yd[count] - timestamp->yd[count - 1]);
          add_new_point_to_ds(zmag, (float) count, g_data->r_data[g_data->current_report_data]->zmag);
          add_new_point_to_ds(vmag, (float) count, g_data->r_data[g_data->current_report_data]->vmag);
          add_new_point_to_ds(zobj, (float) count, g_data->r_data[g_data->current_report_data]->zobj);
          add_new_point_to_ds(zmagobj_status, (float) count, g_data->r_data[g_data->current_report_data]->ZmagObj_status);
        }
        //if (count == 0) add_new_point_to_ds(dt_timestamp, (float) count, 0);
        if( (count%refresh_period) == 0)
        {
          op_timestamp->need_to_refresh = 1;
          op_zmag->x_lo = (count - 1) * refresh_period;
          op_zmag->x_hi = count * refresh_period;
          op_zmag->need_to_refresh = 1;
          op_dt_timestamp->need_to_refresh = 1;
          op_zobj->need_to_refresh = 1;
          op_zmagobj_status->need_to_refresh = 1;
          op_vmag->need_to_refresh =  1;
          refresh_plot(plr, UNCHANGED);
          op_timestamp->x_lo = (count - 1) * refresh_period;
          op_timestamp->x_hi = count * refresh_period;
        }
      }
      if ((record_hdf5 == true) && (count%((int) (ROLLING_BUFFER_SIZE/2))) )
      {
        my_set_window_title("save");
        for (i=0;i<hdf5_manager->n_hdf5_1d;i++)
        {
          if ( (ret = update_hdf5_1d(hdf5_manager->hdf5_1d_list[i], count, true, &file_id, true) ) != 0) ret=0;//my_set_window_title("pb with data saving %d", ret);
        }
      }
    }
  }
  return 0;
}
int	do_rawhid_launch_continous_listening()
{
  if (updating_menu_state!=0) return D_O_K;
  int ret = 0;
  init_rawhid_data();
  ret = pthread_create (&rawhid_thread_id, NULL, rawhid_launch_continous_listening, NULL);
  if (ret) win_printf_OK("Could not start HID thread!");
  return 0;
}
int do_rawhid_pause_continous_listening()
{
  if (updating_menu_state!=0) return D_O_K;
  continous_listening = (continous_listening == true) ? false : true;
  return 0;
}
int do_rawhid_stop_continous_listening()
{
  if (updating_menu_state!=0) return D_O_K;
  void * lpExitCode;
  pthread_join(rawhid_thread_id, &lpExitCode);
  return 0;
}
/////////////////////////////////////////////////////////////////////////////////


int do_extract_i_z_from_image(void)
{
  if (updating_menu_state!=0) return D_O_K;
  imreg *imr=NULL;
  O_i *oi=NULL;
  d_s *profile = NULL;
  O_p *op=NULL;
  if (ac_grep(cur_ac_reg,"%im%oi", &imr, &oi)!=2) return D_O_K;
  int n_pts = imr->o_i[imr->cur_oi]->im.ny;
  int i, k;
  win_printf_OK("nx %d, ny %d", imr->o_i[imr->cur_oi]->im.nx, imr->o_i[imr->cur_oi]->im.ny);
  profile = build_adjust_data_set(profile, n_pts, n_pts);
  for (i=0;i<n_pts;i++)
  {
    profile->xd[i] = i;
    for (k=0;k<imr->o_i[imr->cur_oi]->im.nx;k++) profile->yd[i] += imr->o_i[imr->cur_oi]->im.pixel[i].ch[k];
    //profile->yd[i] /=  (float) (imr->o_i[cur_oi]->im.nx);
  }
  create_hidden_pltreg_with_op(&op, 1,1,0, "avg profile");
  add_data_to_one_plot(op, IS_DATA_SET, profile);
  return 0;
}

//if asked, it opens the file and returns file_id. also the file can be closed or not
//if it is just to dump one array, init with total size and call update function only once with i=0
int update_hdf5_1d(hdf5_1d *hdf5_1d_in, int i, bool open_file, hid_t *file_id, bool close_file)
{
  //if ( i%hdf5_1d_in->write_period !=0 ) return -1;//nothing to do, je laisse tomber ça pour moment
  bool trigger=false;
  static int count_debug=0;
  if ( hdf5_1d_in->offset_file[0] == 0)
  {
    trigger = true;
    hdf5_1d_in->t0 = i;
    my_set_window_title("bon t0 %d i %d offset %d file id %d", hdf5_1d_in->t0, i,  hdf5_1d_in->offset_file[0], (int)(*file_id));
  }
  else
  {
    if ( (i>= (hdf5_1d_in->t0 + hdf5_1d_in->offset_file[0])) && (i<( hdf5_1d_in->t0 + hdf5_1d_in->offset_file[0] +  hdf5_1d_in->chunk_dims[0]) ) )
    {
        my_set_window_title("bon t0 %d i %d offset %d file id %d", hdf5_1d_in->t0, i,  hdf5_1d_in->offset_file[0], (int)(*file_id));
      trigger = true;
    }
  }
  if (trigger == false) return -1;
  hid_t memspace, dataspace, dataset, group_id, subgroup_id;
  herr_t status;
  if (open_file == true)
  {
    if ( (*file_id = H5Fopen (hdf5_1d_in->filename, H5F_ACC_RDWR, H5P_DEFAULT)) < 0) return 1;
    else hdf5_1d_in->file = *file_id;
  }
  else
  {
    if (*file_id < 0) {return 2;}
    else hdf5_1d_in->file = *file_id;
  } //means the given file_id is not valid
  if (hdf5_1d_in->in_group == true)
  {
    if ( (group_id = H5Gopen2(*file_id, hdf5_1d_in->groupname, H5P_DEFAULT)) < 0 ){ return 3;}
    if (hdf5_1d_in->in_subgroup == true)
    {
      if ( (subgroup_id = H5Gopen2(group_id, hdf5_1d_in->subgroupname, H5P_DEFAULT)) < 0 ){ return 6;}
      if ( (dataset = H5Dopen2 (subgroup_id, hdf5_1d_in->datasetname, H5P_DEFAULT)) < 0 ){return 4;}
    }
    else
    {
      if ( (dataset = H5Dopen2 (group_id, hdf5_1d_in->datasetname, H5P_DEFAULT)) < 0 ){return 5;}
    }
  }
  else {if ( (dataset = H5Dopen2 (*file_id, hdf5_1d_in->datasetname, H5P_DEFAULT)) < 0 ) return 7;}
  if ( (status = H5Dset_extent (dataset, hdf5_1d_in->file_dims)) < 0) return 8;
  if ( (dataspace = H5Dget_space (dataset)) < 0) return 9;
  if ( (status = H5Sselect_hyperslab(dataspace, H5S_SELECT_SET, hdf5_1d_in->offset_file, NULL, hdf5_1d_in-> chunk_dims, NULL)) < 0)  return 10;
  memspace = H5Screate_simple (1, hdf5_1d_in->chunk_dims, NULL);

  switch (hdf5_1d_in->datatype)
  {
    case PTR_DATATYPE_FLOAT:
    if ( (status = H5Dwrite (dataset, H5T_NATIVE_FLOAT, memspace, dataspace, H5P_DEFAULT, hdf5_1d_in->data.fdata + hdf5_1d_in->mem_offset )) < 0) return 11;
    break;
    case PTR_DATATYPE_INT:
    if ( (status = H5Dwrite (dataset, H5T_NATIVE_INT, memspace, dataspace, H5P_DEFAULT, hdf5_1d_in->data.idata + hdf5_1d_in->mem_offset )) < 0) return 12;
    break;
    case PTR_DATATYPE_LLONG:
    if ( (status = H5Dwrite (dataset, H5T_NATIVE_LLONG, memspace, dataspace, H5P_DEFAULT, hdf5_1d_in->data.lldata + hdf5_1d_in->mem_offset )) < 0) return 13;
    break;
  }

  if ( (status = H5Dclose (dataset)) < 0) return 14;
  if ( (status = H5Sclose(memspace)) < 0) return 15;
  if ( (status = H5Sclose(dataspace)) < 0) return 16;
  if (hdf5_1d_in->in_subgroup == true) {if ( (status = H5Gclose (subgroup_id)) < 0 ) return 17;}
  if (hdf5_1d_in->in_group == true) {if ( (status = H5Gclose (group_id)) < 0 ) return 18;}
  static count =0;
  if (close_file == true)
  {
    if ( (status = H5Fclose(*file_id)) < 0) return 19;
    *file_id = -1;
    count++;
    my_set_window_title("close file %d no %d", status, count);
  }
  //my_set_window_title("count %d mem offset%d  file dim %d file offset %d",i, hdf5_1d_in->mem_offset, hdf5_1d_in->file_dims[0], hdf5_1d_in->offset_file[0] );
  hdf5_1d_in->mem_offset = (int) (( hdf5_1d_in->mem_offset == 0) ? hdf5_1d_in->chunk_dims[0] : 0);
  hdf5_1d_in->file_dims[0] += hdf5_1d_in->chunk_dims[0];
  hdf5_1d_in->offset_file[0] += hdf5_1d_in->chunk_dims[0];

  return 0;
}
int attach_1d_float_to_hdf5_1d(hdf5_1d *hdf5_1d_in, float *fdata)
{
  hdf5_1d_in->data.fdata = fdata;
  return 0;
}
int attach_1d_int_to_hdf5_1d(hdf5_1d *hdf5_1d_in, int *idata)
{
  hdf5_1d_in->data.idata = idata;
  return 0;
}
int attach_1d_llong_to_hdf5_1d(hdf5_1d *hdf5_1d_in, long long *lldata)
{
  hdf5_1d_in->data.lldata = lldata;
  return 0;
}
int init_hdf5_1d(hdf5_1d *hdf5_1d_in, char filename[], char datasetname[], char groupname[], char subgroupname[], int datatype, int chunk_length)
{
  hid_t file_id, status, group_id, subgroup_id, dataspace, dataset, prop, parent_id;
  hsize_t dims[1] = {chunk_length};
  hsize_t maxdims[1] = {H5S_UNLIMITED};
  hsize_t chunk_dims[1] = {chunk_length};
  snprintf(hdf5_1d_in->datasetname, 128, datasetname);
  snprintf(hdf5_1d_in->filename, 128, filename);
  hdf5_1d_in->chunk_dims[0] = chunk_length ;
  hdf5_1d_in->file_dims[0] = chunk_length;
  hdf5_1d_in->offset_file[0] = 0;
  hdf5_1d_in->write_period = chunk_length;
  hdf5_1d_in->mem_offset = 0;
  hdf5_1d_in->datatype = datatype;
  hdf5_1d_in->in_group = false;
  hdf5_1d_in->in_subgroup = false;
  if ( (file_id = H5Fopen (filename, H5F_ACC_RDWR, H5P_DEFAULT)) < 0) return 1;
  if ( (dataspace = H5Screate_simple (1, dims, maxdims)) < 0) return 2;
  if ( (prop = H5Pcreate (H5P_DATASET_CREATE)) < 0) return 3;
  if ( (status = H5Pset_chunk (prop, 1, chunk_dims)) < 0) return 4;
  if (groupname == NULL)
  {
    hdf5_1d_in->in_group = false;
    parent_id = file_id;
  }
  else
  {
    snprintf(hdf5_1d_in->groupname, 128, groupname);
    hdf5_1d_in->in_group = true;
    //if group already exists or not
    if ( (group_id = H5Gopen2(file_id, groupname, H5P_DEFAULT)) < 0 )
    {
      group_id = H5Gcreate2(file_id, groupname, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    }
    if (subgroupname != NULL)
    {
      hdf5_1d_in->in_subgroup = true;
      snprintf(hdf5_1d_in->subgroupname, 128, subgroupname);
      if ( (subgroup_id = H5Gopen2(group_id, subgroupname, H5P_DEFAULT)) < 0 )
      {
        subgroup_id = H5Gcreate2(group_id, subgroupname, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
      }
      parent_id = subgroup_id;
    }
    else parent_id = group_id;
  }
  switch (datatype)
  {
    case PTR_DATATYPE_INT:
    dataset = H5Dcreate2 (parent_id, datasetname, H5T_NATIVE_INT, dataspace, H5P_DEFAULT, prop, H5P_DEFAULT);
    break;
    case PTR_DATATYPE_FLOAT:
    dataset = H5Dcreate2 (parent_id, datasetname, H5T_NATIVE_FLOAT, dataspace, H5P_DEFAULT, prop, H5P_DEFAULT);
    break;
    case PTR_DATATYPE_LLONG:
    dataset = H5Dcreate2 (parent_id, datasetname, H5T_NATIVE_LLONG, dataspace, H5P_DEFAULT, prop, H5P_DEFAULT);
    break;
  }
  status = H5Dclose (dataset);
  if (hdf5_1d_in->in_subgroup == true) status = H5Gclose(subgroup_id);
  if (hdf5_1d_in->in_group == true) status = H5Gclose (group_id);
  status = H5Pclose (prop);
  status = H5Sclose (dataspace);
  status = H5Fclose (file_id);
  return 0;
}
//to add :zmag, zmag_cmd,  (float), imi (int), action_status (int)
int remove_one_1d_hdf5_from_manager(int which_one)
{
  if (hdf5_manager == NULL) return D_O_K;
  if (hdf5_manager->n_hdf5_1d == 0) return D_O_K;
  if (which_one >= hdf5_manager->n_hdf5_1d) return D_O_K;
  int i;
  free(hdf5_manager->hdf5_1d_list[which_one]->datasetname);
  free(hdf5_manager->hdf5_1d_list[which_one]->filename);
  free(hdf5_manager->hdf5_1d_list[which_one]->groupname);
  free(hdf5_manager->hdf5_1d_list[which_one]);
  hdf5_manager->n_hdf5_1d--;
  for (i=which_one;i<hdf5_manager->n_hdf5_1d;i++) hdf5_manager->hdf5_1d_list[i] = hdf5_manager->hdf5_1d_list[i+1];
  return 0;
}

int add_one_1d_hdf5_to_manager(void)
{
  if (hdf5_manager->n_hdf5_1d==0)
  {
    if (hdf5_manager->hdf5_1d_list == NULL) hdf5_manager->hdf5_1d_list = (hdf5_1d **)calloc(1, sizeof(hdf5_1d *));
    hdf5_manager->hdf5_1d_list[0] = (hdf5_1d *)calloc(1, sizeof(hdf5_1d));
    hdf5_manager->n_hdf5_1d = 1;
  }
  else
  {
    hdf5_manager->hdf5_1d_list = (hdf5_1d **)realloc(hdf5_manager->hdf5_1d_list, (hdf5_manager->n_hdf5_1d + 1)*sizeof(hdf5_1d *));
    hdf5_manager->hdf5_1d_list[hdf5_manager->n_hdf5_1d] = (hdf5_1d *)calloc(1, sizeof(hdf5_1d));
    hdf5_manager->n_hdf5_1d ++;
  }
  return 0;
}
int do_pause_record(void)
{
  if (updating_menu_state!=0) return D_O_K;

  return 0;
}


//////saving of ds data onto hdf5 files
//one file for all ds (erase with same name)
int dump_all_ds_to_one_hdf5_file(d_s *ds_in, char filename[], char groupname[], char subgroupname[])
{
  hid_t file_id;
  herr_t status;
  hdf5_1d *x_hdf5;
  x_hdf5 = (hdf5_1d *)calloc(1, sizeof(hdf5_1d));
  init_hdf5_1d(x_hdf5, filename, "xd", groupname, subgroupname, PTR_DATATYPE_FLOAT, ds_in->nx);
  attach_1d_float_to_hdf5_1d(x_hdf5, ds_in->xd);
  hdf5_1d *y_hdf5;
  y_hdf5 = (hdf5_1d *)calloc(1, sizeof(hdf5_1d));
  init_hdf5_1d(y_hdf5, filename, "yd", groupname, subgroupname, PTR_DATATYPE_FLOAT, ds_in->nx);
  attach_1d_float_to_hdf5_1d(y_hdf5, ds_in->yd);
  update_hdf5_1d(x_hdf5, 0, true, &file_id, true);
  update_hdf5_1d(y_hdf5, 0, true, &file_id, true);
  free(x_hdf5);
  x_hdf5=NULL;
  free(y_hdf5);
  y_hdf5=NULL;
  return 0;
}

//new file for each ds (erase with same name)
int dump_one_ds_to_one_hdf5_file(d_s *ds_in, char filename[])
{
  hid_t file_id;
  herr_t status;
  if ( (file_id = H5Fcreate(filename, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT))<0 ) win_printf_OK("file not created");
  status = H5Fclose(file_id);
  hdf5_1d *x_hdf5;
  x_hdf5 = (hdf5_1d *)calloc(1, sizeof(hdf5_1d));
  init_hdf5_1d(x_hdf5, filename, "xd", NULL, NULL, PTR_DATATYPE_FLOAT, ds_in->nx);
  attach_1d_float_to_hdf5_1d(x_hdf5, ds_in->xd);
  hdf5_1d *y_hdf5;
  y_hdf5 = (hdf5_1d *)calloc(1, sizeof(hdf5_1d));
  init_hdf5_1d(y_hdf5, filename, "yd", NULL, NULL, PTR_DATATYPE_FLOAT, ds_in->nx);
  attach_1d_float_to_hdf5_1d(y_hdf5, ds_in->yd);
  update_hdf5_1d(x_hdf5, 0, true, &file_id, true);
  update_hdf5_1d(y_hdf5, 0, true, &file_id, true);
  free(x_hdf5);
  x_hdf5=NULL;
  free(y_hdf5);
  y_hdf5=NULL;
  return 0;
}
int do_dump_ds_of_current_op_to_hdf5(void)
{
  if (updating_menu_state!=0) return D_O_K;
  hid_t file_id;
  herr_t status;
  char filename[256];
  char groupname[256];
  char subgroupname[256];
  pltreg *plr=NULL;
  O_p *op=NULL;
  if ((ac_grep(cur_ac_reg, "%op", &op))!=1) return D_O_K;
  if (op->title != NULL) snprintf(filename, 256, op->title);
  else snprintf(filename, 256, FILE);
  win_printf_OK("from the title of the plot (no space please), one h5 per ds");
  int i;
  snprintf(filename+strlen(op->title), 256 - strlen(op->title), ".h5");
  win_printf_OK("dump ds to file : %s", filename);
  if ( (file_id = H5Fcreate(filename, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT))<0 ) win_printf_OK("file not created");
  status = H5Fclose(file_id);

  /*int         attr_data[2];
  herr_t      status;
  attr_data[0] = 100;
  attr_data[1] = 200;
  dataset_id = H5Dopen2(file_id, "/dset", H5P_DEFAULT);
  attribute_id = H5Acreate2 (dataset_id, "Units", H5T_STD_I32BE, dataspace_id,
                            H5P_DEFAULT, H5P_DEFAULT);
  status = H5Awrite(attribute_id, H5T_NATIVE_INT, attr_data);
  status = H5Aclose(attribute_id);
  */
  for (i=0;i<op->n_dat;i++)
  {
    //snprintf(filename+strlen(op->title), 256 - strlen(op->title), "-ds%d.h5",i);
    //dump_one_ds_to_one_hdf5_file(op->dat[i], filename);
    snprintf(groupname, 256, "ds%d",i);
    dump_all_ds_to_one_hdf5_file(op->dat[i], filename, groupname, NULL);
  }
  return 0;
}

MENU *SDITracking_plot_menu(void)
{
  static MENU mn[32];
  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"debug cast", do_test_cast, NULL, 0, NULL);
  add_item_to_menu(mn, "dump ds of current op to hdf5", do_dump_ds_of_current_op_to_hdf5, NULL, 0, NULL);
  add_item_to_menu(mn, "init rawhid comm", do_rawhid_init, NULL, 0, NULL);
  add_item_to_menu(mn, "close rawhid comm", do_rawhid_close, NULL, 0, NULL);
  add_item_to_menu(mn, "send one async cmd", do_rawhid_manual_async_cmd, NULL, 0, NULL);
  add_item_to_menu(mn, "launch continous listening", do_rawhid_launch_continous_listening, NULL, 0, NULL);
  add_item_to_menu(mn, "pause continous listening", do_rawhid_pause_continous_listening, NULL, 0, NULL);
  add_item_to_menu(mn, "close continous listening", do_rawhid_stop_continous_listening, NULL, 0, NULL);
  add_item_to_menu(mn, "record continous data on hdf5", do_pause_record, NULL, 0, NULL);
  return mn;
}

MENU *SDITracking_image_menu(void)
{
  static MENU mn[32];
  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn, "extract avg Intensity from calibim", do_extract_i_z_from_image, NULL, 0, NULL);
  return mn;
}

int	SDITracking_main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  add_plot_treat_menu_item ( "SDITracking", NULL, SDITracking_plot_menu(), 0, NULL);
  add_image_treat_menu_item( "SDITracking", NULL, SDITracking_image_menu(), 0, NULL);
  return D_O_K;
}
int	SDITracking_unload(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  remove_item_to_menu(plot_treat_menu, "SDITracking", NULL, NULL);
  return D_O_K;
}
