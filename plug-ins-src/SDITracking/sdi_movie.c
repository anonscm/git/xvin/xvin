#pragma once
#include "xvin.h"

#include "sdi_movie.h"


int do_extract_submovie(void)
{
  if(updating_menu_state != 0)	return D_O_K;
  imreg *imr=NULL;
  O_i *oi=NULL;
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) < 1)	return OFF;
  O_i *oi_extract=NULL;
  static int frame_selection_period=1, frame_selection_offset=0, frame_selection_subset=1, frame_selection_n=1;
  int oi_nx, oi_ny, nfi, frame_selection_i, frame_selection_current ;
  int j,k,i;//index for loops
  nfi=0;
  if (imr->n_oi > 0) nfi=imr->o_i[0]->im.n_f;
  win_printf_OK("This plug-in allow you to construct a movie by extracting frames of initial movies\n"
                "There are %3d movies in the current image region, the first contains %5d frames",imr->n_oi,nfi);
  if (win_scanf("Specify the periodic selection of frames by the period %4d, the offset %4d, the subset length %4d\n"
                "how many %4d"
               ,&frame_selection_period,&frame_selection_offset, &frame_selection_subset, &frame_selection_n)==CANCEL) return D_O_K;
  oi_ny=imr->o_i[0]->im.ny;
  oi_nx= imr->o_i[0]->im.nx;//total number of pixels along x of the final movie
  oi_extract=create_and_attach_movie_to_imr(imr, oi_nx, oi_ny, IS_CHAR_IMAGE, frame_selection_n);
  frame_selection_i=0;
  for (i=0;i<nfi;i++)
  {
    frame_selection_current =  (int) (frame_selection_i / frame_selection_subset) * frame_selection_period   +
                                           frame_selection_i % frame_selection_subset + frame_selection_offset;
    if(frame_selection_current == i && frame_selection_i<frame_selection_n)
    {
      switch_frame(imr->o_i[0], frame_selection_current);
      switch_frame(oi_extract,frame_selection_i);
      for(j=0;j<oi_ny;j++)
      {
        for(k=0;k<oi_nx;k++)
        {
          oi_extract->im.pixel[j].ch[k] +=  imr->o_i[0]->im.pixel[j].ch[k];
        }
      }
      //win_printf_OK("%d", frame_selection_current);
      frame_selection_i++;
    }
  }
  find_zmin_zmax(oi_extract);
  return 0;
}

int do_simulate_pattern(void)
{
if(updating_menu_state != 0)	return D_O_K;
imreg *imr=NULL;
O_i *oi=NULL;
if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) < 1)	return OFF;
O_i *oi_union=NULL;
int oi_nx, oi_ny, oi_nf;
int nb_im_i, nb_im_f;//number of the initial movies, ond total number of movies
static int n_pix_y, n_pix_y_2, n_pix_x, n_pix_x_2;//about the siez of images
static int nb_x, nb_y, yc_pix_i;//about the number of lines and rows of the final movie
static int shift_y,squeeze_x, n_repeat;
static float bgd;
int j,k,i,l,m;//index for loops
win_printf_OK("This plug-in allow you to construct a movie by assemblying and periodically repeating the list\n"
              "of movies loaded in the current image region (must have the same size). There are %d movies in the current image region",imr->n_oi);
if (win_scanf("Specify how many times you want to repeat the current list of movies : %d\n"
              "How many beads per lines ? %d and how many pixels (from the center) in y %d and x %d you want to keep from the original ones \n"
              "specify a shift along y of %d and a squeeze offset along x of %d to optimize the density of beads\n"
              "and the background threshold %f"
              ,&n_repeat, &nb_x, &n_pix_y, &n_pix_x, &shift_y, &squeeze_x, &bgd)==CANCEL) return D_O_K;
nb_im_i=imr->n_oi;
nb_im_f= nb_im_i * n_repeat;
n_pix_y=imr->o_i[0]->im.ny;
yc_pix_i=(int) (imr->o_i[0]->im.ny / 2);
n_pix_y_2 = (int) (n_pix_y / 2);
nb_y = (int) (nb_im_f / nb_x);//number of lines in the final movie
if (nb_im_f % nb_x !=0) nb_y++;//add one line in the final movie if the total number of beads is not a multiply of nb_x
oi_nx= imr->o_i[0]->im.nx + (nb_x-1) * (imr->o_i[0]->im.nx - squeeze_x);//total number of pixels along x of the final movie
oi_ny= nb_y * n_pix_y;
oi_nf=imr->o_i[0]->im.n_f;
oi_union=create_and_attach_movie_to_imr(imr, oi_nx, oi_ny+shift_y, IS_CHAR_IMAGE, oi_nf);
win_printf_OK("for a total number of %d patterns, nb lines %d nb columns %d, put on %d pixels along x and %d along y %d",nb_im_f,nb_y,nb_x,oi_nx,oi_ny+shift_y);
for (m=0 ; m< nb_y ; m++)
{
for(l=0;l<nb_x;l++)
{
  if(((l+m*nb_x)%nb_im_i)<imr->n_oi-1)
  {
  for (i=0;i<oi_nf;i++)
  {
    switch_frame(imr->o_i[(l+m*nb_x)%nb_im_i],i);
    switch_frame(oi_union,i);
    for(j=0;j<n_pix_y;j++)
    {
      for(k=0;k<imr->o_i[0]->im.nx;k++)
      {
        oi_union->im.pixel[j + m * n_pix_y + shift_y * (int)(l % 2)].ch[k + l * imr->o_i[0]->im.nx - squeeze_x * l] +=
                ((imr->o_i[(l+m*nb_x)%nb_im_i]->im.pixel[j-n_pix_y_2+yc_pix_i].ch[k] > (char) bgd ) ? imr->o_i[(l+m*nb_x)%nb_im_i]->im.pixel[j-n_pix_y_2+yc_pix_i].ch[k] : 0);
      }
    }
  }
  }
}
}
for (j=0;j<oi_ny+shift_y;j++)
{
  for(i=0;i<oi_nx;i++) if (oi_union->im.pixel[j].ch[i]==0) oi_union->im.pixel[j].ch[i]=(char) bgd;
}

find_zmin_zmax(oi_union);
return 0;
}
