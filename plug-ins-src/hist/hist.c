/**-name: hist
  *-author: N. Garnier, V. Croquette
  *-function: histograms creation and operation.
	the gsl structure gsl_histogram is used, and functions to convert such structures 
	from/to datasets are provided.
  */
   
#ifndef _HIST_C_
#define _HIST_C_

#include "allegro.h"
#include "xvin.h"
#include "xv_tools_lib.h"

#include <gsl/gsl_histogram.h>
#include <gsl/gsl_histogram2d.h>
#include <gsl/gsl_statistics_float.h> // to compute mean and variance

/* If you include other plug-ins header do it here*/ 
#include "../inout/inout.h"
/* But not below this define */
#define BUILDING_PLUGINS_DLL

#include "hist.h"



int do_hist_build_from_ds(void)
{
register int 	i;
	O_p 	*op2, *op1 = NULL;
static	int	nf=400+1;
	gsl_histogram *hist;
	float	h_min, h_max, mean_x, var_x, range=5.;
	d_s 	*ds2, *ds1;
	pltreg	*pr = NULL;
	int	ny, tau, n_tau;
	int	*tau_index=NULL;
static	int	shift=1;
	int	index;
static	char	tau_string[128]="1,2,5,10:5:50";

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	if (index==HIST_AVERAGE)
			return win_printf_OK("This routine builds an histogram\n"
				"out of the current dataset, and place it in a new plot\n\n"
				"data a(t) is first averaged using a sliding average:\n\n"
				"a_\\tau(t) = {1 \\over \\tau} \\int_t^{t+\\tau} a(t') dt'\n\n"
				"(this is for example used in the study of FTs)");
		if (index==HIST_NO_AVERAGE)
			return win_printf_OK("This routine builds an histogram\n"
				"out of the current dataset, and place it in a new plot");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)		return win_printf_OK("cannot plot region");
	ny=ds1->nx;	

	mean_x = gsl_stats_float_mean(ds1->yd, 1, ny);
	var_x  = gsl_stats_float_sd_m(ds1->yd, 1, ny, mean_x);
	h_max = mean_x + range*var_x;
    h_min = mean_x - range*var_x;
    
	/* old version was : (changed 2007/10/23, NG)
    h_max = op1->y_hi;
    h_min = op1->y_lo;
    */
    
	if (index==HIST_NO_AVERAGE)
	{	i=win_scanf("{\\color{yellow}\\pt14 building histograms}\n\n"
                    "number of bins in histogram? %5d\n"
                    "range : [min = %8f ,\n            max = %8f[\n",&nf, &h_min, &h_max);
	}
	else if (index==HIST_AVERAGE)
	{	i=win_scanf("{\\color{yellow}\\pt14 building histograms}\n\n"
                    "number of bins in histogram? %5d\n"
                    "range : [min = %8f ,\n            max = %8f[\n\n"
			"using averaged data a_\\tau(t) = {1 \\over \\tau} \\int_t^{t+\\tau} a(t') dt' \n\n"
			"sampled every N points (N>=1) N=%3d\n \\tau (integer, nb of pts) ? %s",
			&nf,&h_min,&h_max,&shift,&tau_string);
	}
	else return OFF;
	if (i==CANCEL) return OFF;

	if (index==HIST_NO_AVERAGE)
		tau_index = str_to_index("1", &n_tau);
	else	tau_index = str_to_index(tau_string, &n_tau);	// malloc is done by str_to_index
	if ( (tau_index==NULL) || (n_tau<1) )	return(win_printf_OK("bad values for \\tau !"));

	if ((op2 = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
		return(win_printf_OK("cannot create plot !"));
	ds2 = op2->dat[0];
	
	for (i=0; i<n_tau; i++)
	{	tau=tau_index[i];

		if (i!=0)
		if ((ds2 = create_and_attach_one_ds(op2, nf, nf, 0)) == NULL)	
			return win_printf_OK("cannot create dataset !");

		hist = (gsl_histogram *)gsl_histogram_alloc(nf);
		gsl_histogram_set_ranges_uniform (hist, (double)h_min, (double)h_max);

        fill_histogram_with_averaged_float_data(hist, ds1->yd, ny, tau, shift);

		histogram_to_ds(hist, ds2, nf);
		gsl_histogram_free(hist);

		inherit_from_ds_to_ds(ds2, ds1);
		ds2->treatement = my_sprintf(ds2->treatement,"histogram, averaged data over \\tau=%d points", tau);
	
	}
	free(tau_index);

	set_plot_title(op2,"histogram");
	set_plot_x_title(op2, "x (%d values)",ny);
	set_plot_y_title(op2, "pdf");
	op2->filename = Transfer_filename(op1->filename);
    op2->dir = Mystrdup(op1->dir);
    op2->iopt |= YLOG;

	return refresh_plot(pr, UNCHANGED);
} // end of "do_hist_build_from_ds(void)"





// hist must be allocated!
int fill_histogram_with_averaged_float_data(gsl_histogram *hist, float *x, int nx, int tau, int shift)
{   register int j, j2;
    double x_tau;

    for (j=0; j<nx-tau+1; j+=shift)
	{	x_tau=(double)0.0;
		for (j2=0; j2<tau; j2++)
		{	x_tau += (double)x[(j+j2)];		// average over tau points
		}
		gsl_histogram_increment(hist, (double)(x_tau/tau)); // histogram of averaged data
	}
    return(nx-tau+1); // returns the number of points added to the histogram
}	



/************************************************************************/
/* nt  : number of points in file					*/
/* tau : how long to average						*/
/* shift : between two averaged						*/
/************************************************************************/
int fill_histogram_from_1D_file(gsl_histogram *hist, char *data_filename, int nt, int tau, int shift)
{	FILE		*f_input;
	register int	j;
	float		*x;

	if (win_scanf("number of points in file ? %d (0 to auto-detect)",&nt) == CANCEL) return(0);
	if (nt==0)
	{	nt=load_data_bin(data_filename, &x);
	}
	else
	{	f_input=fopen(data_filename,"rb");
		if (f_input==NULL) return(win_printf_OK("cannot open file %s",data_filename));
		x=(float*)calloc(nt,sizeof(float));
		j=fread(x, sizeof(float), nt, f_input);
		fclose(f_input);
		if (j!=nt) win_printf_OK("you asked for %d points, but I found %d ones, so I will use %d points", nt,j,j);
		nt=j;
	}
	
    j=fill_histogram_with_averaged_float_data(hist, x, nt, tau, shift);

	return(j);	// returns the number of points added to the histogram
}



int normalize_histogram_Y(gsl_histogram *hist)
{//	register int i;
	int nh;
	double scale;
	double N, lower, upper, width;

	nh = gsl_histogram_bins(hist); // number of bins;
	N  = gsl_histogram_sum (hist); // sum of all the bins;
	gsl_histogram_get_range (hist, 1, &lower, &upper); // lower and upper bound of bin #1
	width = upper-lower;
	scale = 1/(N*width);

	gsl_histogram_scale(hist, (double)scale);	// multiplies histogram y-axis by scale

	return(nh);
}


int ds_to_histogram(d_s *ds, gsl_histogram **hist) // hist will be allocated perfectly
{	register int i;
	int nh;
	double h_min, h_max, width;

	nh=ds->nx;
	h_min=ds->xd[0];
	h_max=ds->xd[nh-1];
	width=(h_max - h_min)/(nh-1);
	h_min-=width/2;
	h_max+=width/2;

	*hist = (gsl_histogram *)gsl_histogram_alloc(nh);
	gsl_histogram_set_ranges_uniform (*hist, (double)h_min, (double)h_max);

	for (i=0; i<nh; i++)
	{	gsl_histogram_accumulate (*hist, ds->xd[i], ds->yd[i]);
	}

	return(nh);
}


int histogram_to_ds(gsl_histogram *hist, d_s *ds, int nf) // ds must already be allocated!!
{	register int j;
	double x1, x2, y;
	
	for (j = 0; j < nf; j++)
	{
		gsl_histogram_get_range( hist, j, &x1, &x2);
		y = gsl_histogram_get( hist, j);
		ds->yd[j] = (float)(y);
		ds->xd[j] = (float)( (x1+x2)/2 );
	}

	return(nf);
}




int do_hist_normalize(void)
{	register int i, j;
	d_s 	*ds2, *ds1;
	O_p	*op;
	pltreg *pr = NULL;
	gsl_histogram *hist;
	int	index, nh, n;
static	int	bool_Y=1;	// boolean for normalization in Y
static	int	bool_X=0;
	double  mean, sigma;
	
	if(updating_menu_state != 0)	return D_O_K;		
       	index = active_menu->flags & 0xFFFFFF00;
	if ( (index!=HIST_ONE) && (index!=HIST_ALL) ) return(win_printf_OK("bad call!"));

	if (key[KEY_LSHIFT])	return win_printf_OK("This routine normalizes one or all histogram(s)"
		"(the current dataset), and place it in a new dataset\n\nmean and variance are attached to the history");
	
	if (win_scanf("{\\color{yellow}\\pt14 Normalize histogram(s)}\n\n"
		"%b in Y such that sum \\int p(x) dx = 1\n\n"
		"%R no normalization in X\n"
		"%r multiply X such that variance \\sigma = \\sqrt{<x^2>-<x>^2} = 1\n"
		"%r multiply X such that mean <x> = 1\n"
		"%r shift X such that mean <x> = 0",	&bool_Y, &bool_X) == CANCEL) return(D_O_K);

	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds1) != 3)		return win_printf_OK("cannot plot region");

	if (index==HIST_ONE) 
	{	nh=ds1->nx;

		if ((ds2 = create_and_attach_one_ds(op, nh, nh, 0)) == NULL)	return win_printf_OK("cannot create dataset !");
	
		ds_to_histogram(ds1,&hist);
		mean  = gsl_histogram_mean(hist);
		sigma = gsl_histogram_sigma(hist);
		if (bool_Y==1)	normalize_histogram_Y(hist);
		histogram_to_ds(hist, ds2, nh);
		free(hist);

		if (bool_X!=0)
		for (i=0; i<nh; i++)
		{	if (bool_X==1)	ds2->xd[i] /= sigma;
			if (bool_X==2)	ds2->xd[i] /= mean;
			if (bool_X==3)	ds2->xd[i] -= mean;
		}

		inherit_from_ds_to_ds(ds2, ds1);
		
		ds2->treatement = my_sprintf(ds2->treatement,"normalized histogram %s %s\nmean = %g   sigma = %g",
			(bool_Y==1) ? "in Y," : "not in Y,",
			(bool_X==0) ? "not in X" : 
				( (bool_X==1) ? "in X dividing by variance" : 
				( (bool_X==2) ? "in X dividing by mean" :
						"in X substracting mean") ),
			mean, sigma);

	}
	else
	{ n = op->n_dat;
	  for (j=0; j<n; j++)
	{	ds1 = op->dat[j];
		nh  = ds1->nx;

		if ((ds2 = create_and_attach_one_ds(op, nh, nh, 0)) == NULL)	return win_printf_OK("cannot create dataset !");
	
		ds_to_histogram(ds1,&hist);
		mean  = gsl_histogram_mean(hist);
		sigma = gsl_histogram_sigma(hist);
		if (bool_Y==1)	normalize_histogram_Y(hist);
		histogram_to_ds(hist, ds2, nh);
		free(hist);

		if (bool_X!=0)
		for (i=0; i<nh; i++)
		{	if (bool_X==1)	ds2->xd[i] /= sigma;
			if (bool_X==2)	ds2->xd[i] /= mean;
			if (bool_X==3)	ds2->xd[i] -= mean;
		}

		inherit_from_ds_to_ds(ds2, ds1);

		ds2->treatement = my_sprintf(ds2->treatement,"normalized histogram %s %s\nmean = %g   sigma = %g",
			(bool_Y==1) ? "in Y," : "not in Y,",
			(bool_X==0) ? "not in X" : 
				( (bool_X==1) ? "in X dividing by variance" : 
				( (bool_X==2) ? "in X dividing by mean" :
						"in X substracting mean") ),
			mean, sigma);

	}

	if (win_scanf("Remove old histograms ?")!=CANCEL)
	for (j=0; j<n; j++)
	{	remove_ds_n_from_op (op, 0);	// always remove the 0th because counting is shifted after any one is deleted
	}

	}

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
}










// loi des grands nombres
// la variance de la pdf de la somme de N termes decroit en \sqrt(N)
// cette fonction normalise les histogrammes en X par sqrt(N) pour retrouver cela.
int do_normalize_histogram_by_tau(void)
{	int	index;
	register int i, k;
	pltreg	*pr;
	O_p	*op;
	d_s	*ds, *ds_test=NULL;
	int	tau; 
	int	*ind, n_ds;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;
	if (!( (index&HIST_ONE) || (index&HIST_ALL) )) return(win_printf_OK("? work on one or all dataset(s) ?"));

	if (key[KEY_LSHIFT]) 
	{ 	return(win_printf_OK("This function normalizes histograms in X by \\tau ,"
				"the number of point\n on which data has been averaged before building the histograms\n"
				"A multiplication by \\sqrt{\\tau}  is performed on the x-axis.\n"
				"The value of \\tau  is searched for in dataset information"));
	}
        if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)  return win_printf_OK("cannot find data!");

	if (index&HIST_ONE)
	{ 	n_ds  = 1;
		ind   = (int*)calloc(n_ds,sizeof(int));
		ind[0]= op->cur_dat;
	}
	else
	{	n_ds  = op->n_dat;
		ind   = (int*)calloc(n_ds,sizeof(int));
		for (i=0; i<n_ds; i++) ind[i]=i;
	}

	for (k=0; k<n_ds; k++)
	{	ds =  op->dat[ ind[k] ];
		if (ds == NULL)   return (win_printf_OK("can't find data set"));

		tau = grep_int_in_string(ds->treatement, "tau=");
		if (tau==-1) 	tau = grep_int_in_string(ds->history, "tau=");
		
		if (tau>0)
		{	ds_test = create_and_attach_one_ds(op, ds->nx, ds->ny, 0);
			if (ds_test==NULL) return(win_printf_OK("cannot allocate dataset"));

			for (i=0; i<ds->nx; i++)
			{	ds_test->xd[i] = ds->xd[i]*sqrt(tau);
				ds_test->yd[i] = ds->yd[i];
			}		

			inherit_from_ds_to_ds(ds_test, ds);
			ds_test->treatement = my_sprintf(ds_test->treatement,"x values multiplied by \\sqrt{\\tau=%d}", tau);

			if ( (k==0) || (k==n_ds-1)) 
			set_ds_plot_label(ds_test, ds_test->xd[ds->nx*2/3-1], ds_test->xd[ds->ny-1], USR_COORD, 
				"\\tau = %d", tau);
		}
	}

	if (n_ds>1)	// if we worked on all datasets...
	if (win_scanf("Remove old datasets ?")!=CANCEL)
	for (k=0; k<n_ds; k++)
	{	remove_ds_n_from_op (op, 0);	// always remove the 0th because counting is shifted after any one is deleted
	}

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
}//end of "do_normalize_histogram_by_tau"







int do_hist_build_from_file(void)
{
	register int i, j;
	O_p 	*opn = NULL;
	static int nf=2048+1;
	int	ny;
	char	data_filename[512];
	gsl_histogram *hist;
	float	h_min=-1, h_max=3;	
	double	x1,x2,y;
	d_s 	*ds;
	pltreg *pr = NULL;
	int	index;
	static int	tau=1, shift=1;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	if (index==HIST_AVERAGE)
			return win_printf_OK("This routine builds an histogram\n"
				"out of a 1D-data file, and place it in a new plot\n\n"
				"data a(t) is first averaged using a sliding average:\n\n"
				"a_\\tau(t) = {1 \\over \\tau} \\int_t^{t+\\tau} a(t') dt'\n\n"
				"(this is for example used in the study of FTs)");
		if (index==HIST_NO_AVERAGE)
			return win_printf_OK("This routine builds an histogram\n"
				"out of a 1D-data file, and place it in a new plot");
	}

	i = file_select_ex("Load histogram .dat file", data_filename, "dat", 512, 0, 0);
	if (i==0) return OFF;
	
	if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)		return win_printf_OK("cannot plot region");

	h_max = (pr->one_p)->y_hi;
        h_min = (pr->one_p)->y_lo;

	if (index==HIST_NO_AVERAGE)
	{	i=win_scanf("number of bins in histogram? %d min %f max %f",&nf,&h_min,&h_max);
	}
	else if (index==HIST_AVERAGE)
	{	i=win_scanf("number of bins in histogram? %d min %f max %f\n"
			"using averaged data a_\\tau(t) = {1 \\over \\tau} \\int_t^{t+\\tau} a(t') dt' \n\n"
			"sampled every N points\n\n \\tau (integer, nb of pts) ? %d N (integer, N>=1) %d",
			&nf,&h_min,&h_max,&tau,&shift);
	}
	else return OFF;
	if (i==CANCEL) 	return OFF;
	if (tau<1) 	return OFF;
	if (shift<1) 	return OFF;

	hist = (gsl_histogram *)gsl_histogram_alloc(nf);
	gsl_histogram_set_ranges_uniform (hist, h_min, h_max);
	ny=fill_histogram_from_1D_file(hist, data_filename, 0, tau, shift);

	if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	ds = opn->dat[0];

	for (j = 0; j < nf; j++)
	{
		gsl_histogram_get_range( hist, j, &x1, &x2);
		y = gsl_histogram_get( hist, j);
		ds->yd[j] = (float)(y/ny);
		ds->xd[j] = (float)( (x1+x2)/2 );
	}

	gsl_histogram_free(hist);

	opn->filename	= extract_file_name( opn->filename, 1, data_filename);
	opn->dir 	= extract_file_path( opn->dir, 	    1, data_filename);
	set_plot_title(opn,"histogram of file %s",opn->filename);
	set_plot_x_title(opn, "x (%d values)",ny);
	set_plot_y_title(opn, "pdf");

	set_ds_source(ds, "histogram, built from %s\nin [ %g  ;  %g ]", opn->filename, h_min, h_max);
	
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
}








/************************************************************************/
/* returns some infos on the histogram that is in the active dataset	*/
/************************************************************************/
int do_hist_measurement(void)
{	double max=0, mean=0, sigma=0, skewness=0, kurtosis=0, sum=0, x=0;
	char	message[512]="what did you want ???";
	pltreg	*pr;
	O_p	*op;
	d_s	*ds;
	gsl_histogram	*hist;
	int 	index, nh, j;
	double	pxc3;

	if(updating_menu_state != 0)	return D_O_K;	
	if (key[KEY_LSHIFT]) 	return win_printf_OK("This routine gives the mean, the variance, the max ... of a pdf\n");

        if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)  return win_printf_OK("cannot find data!");
	if (ds == NULL)   return (win_printf_OK("can't find data set"));

	index = active_menu->flags & 0xFFFFFF00;
    nh = ds->nx;

	ds_to_histogram(ds, &hist);

	max = gsl_histogram_max_val (hist);
	x=(double)ds->xd[gsl_histogram_max_bin(hist)];
// This function returns the maximum value contained in the histogram bins. 
	
	mean = gsl_histogram_mean ( hist);
// This function returns the mean of the histogrammed variable, 
// where the histogram is regarded as a probability distribution. 
// Negative bin values are ignored for the purposes of this calculation. 
// The accuracy of the result is limited by the bin width. 
	
	sigma = gsl_histogram_sigma ( hist);
// This function returns the standard deviation of the histogrammed variable, 
// where the histogram is regarded as a probability distribution. 
// Negative bin values are ignored for the purposes of this calculation. 
// The accuracy of the result is limited by the bin width. 
	
	sum = gsl_histogram_sum ( hist);
// This function returns the sum of all bin values. Negative bin values are included in the sum.
	
	skewness=0.;
	kurtosis=0.;
	for (j=0; j<nh; j++)
	{	x         = (double)(ds->xd[j]-mean);
		pxc3 	  = x*x*x*(double)(ds->yd[j]);
		skewness += pxc3;
		kurtosis += pxc3*x;
	}
	skewness /= sum;
	kurtosis /= sum;
	
	sprintf(message,"for the current histogram, with %d bins\n"
					"max  is %g for x = %g\n"
					"mean value <x> = %g\n"
					"std \\sigma = %g\n\n"
					"where \\sigma^2 = <(x-<x>)^2> is %g\n"
					"sum is %g\n"
					"<(x-<x>)^3> = %g   <(x-<x>)^3>/\\sigma^{3} = %g\n"
					"<(x-<x>)^4> = %g   <(x-<x>)^4>/\\sigma^{4} = %g\n",
					nh, max, x, mean, sigma, sigma*sigma, sum, 
					skewness, skewness/(sigma*sigma*sigma), kurtosis, kurtosis/(sigma*sigma*sigma*sigma));
	
	gsl_histogram_free(hist);
	return(win_printf_OK(message));
} /* end of "do_hist_measurement function" */





/************************************************************************/
/* returns one info on several histograms that are in the active plot	*/
/************************************************************************/
int do_multi_hist_measurement(void)
{	double measurement=0, x=0, mean=0, sum=1, sigma=0;
	pltreg	*pr;
	O_p	*op=NULL, *op_out=NULL;
	d_s	*ds=NULL, *ds_out=NULL;
	gsl_histogram	*hist;
	int 	index, nh, n_ds, k, j, tau;
	char    s[64]="something";

	if(updating_menu_state != 0)					
							return D_O_K;
	index = active_menu->flags & 0xFFFFFF00;
	if ( ! ( (index&HIST_MAX) || (index&HIST_MEAN)) ) 		
							return(win_printf_OK("bad call..."));	// 1 or all datasets

	if (key[KEY_LSHIFT]) 	return win_printf_OK("This routine measures something from a set of pdf\n");

    if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)  
    						return win_printf_OK("cannot find data!");
	
    n_ds  = op->n_dat;
	
	op_out = create_and_attach_one_plot(pr, n_ds, n_ds, 0);
    if (op_out == NULL)		return(win_printf_OK("Cannot allocate plot"));
    ds_out = op_out->dat[0];
    if (ds_out == NULL)		return(win_printf_OK("Cannot allocate dataset"));

   	for (k=0; k<n_ds; k++)
	{	ds = op->dat[ k ];
		if (ds == NULL)   return (win_printf_OK("can't find data set"));
    	nh = ds->nx;
    	ds_to_histogram(ds, &hist);

    	tau = grep_int_in_string(ds->treatement, "tau=");
		if (tau==-1) tau = grep_int_in_string(ds->history, "tau=");
		if (tau==-1) win_printf_OK("dataset %d has no valid value of \\tau...", k);
		
    	switch (index)
    	{	case HIST_MAX : measurement = gsl_histogram_max_val (hist);
        			   // x=(double)ds->xd[gsl_histogram_max_bin(hist)];
        			   // sprintf(message,"max value pdf(x)=%g for x=%g", measurement, x);
        			  	break;
        	case HIST_MEAN : measurement = gsl_histogram_mean ( hist);
        				break;
        	case HIST_SIGMA : measurement = gsl_histogram_sigma ( hist);
        				break;
        	case HIST_SUM : measurement = gsl_histogram_sum ( hist);
        				break;
        	case HIST_SKEWNESS : 
        				mean = gsl_histogram_mean (hist);
        				sum  = gsl_histogram_sum  (hist);
        				sigma= gsl_histogram_sigma(hist);
						measurement = 0.;
        				for (j=0; j<nh; j++)
						{	x            = (double)(ds->xd[j]-mean);
							measurement += x*x*x*(double)(ds->yd[j]);
						}
						measurement /= (sum*sigma*sigma*sigma);
        				break;
        	case HIST_KURTOSIS : 
        				mean = gsl_histogram_mean (hist);
        				sum  = gsl_histogram_sum  (hist);
        				sigma= gsl_histogram_sigma(hist);
						measurement = 0.;
        				for (j=0; j<nh; j++)
						{	x            = (double)(ds->xd[j]-mean);
							measurement += x*x*x*x*(double)(ds->yd[j]);
						}
						measurement /= sum;
						measurement /= (sigma*sigma*sigma*sigma);
        				break;
        	case HIST_GC : 
        				mean = gsl_histogram_mean (hist);
        				sigma= gsl_histogram_sigma(hist);
						measurement = (double)2.*mean/(sigma*sigma*tau);
        				break;
        	case HIST_GC4 : 
        				mean = gsl_histogram_mean (hist);
        				sigma= gsl_histogram_sigma(hist);
						measurement = (double)2.*mean/sigma/tau;
        				break;
        	default  : measurement = -1;
     	}		
		gsl_histogram_free(hist);
		
		ds_out->xd[k] = (float)tau;
		ds_out->yd[k] = (float)measurement;

	}
	
	switch (index)
    {	case HIST_MAX   : sprintf(s,"max");
       			  	break;
       	case HIST_MEAN  : sprintf(s,"mean <x>");
       				break;
       	case HIST_SIGMA : sprintf(s,"\\sigma = \\sqrt{<x^2>-<x>^2}");
       				break;
       	case HIST_SUM   : sprintf(s,"sum \\int p(x)dx");
       				break;
       	case HIST_SKEWNESS  : sprintf(s,"skewness \\frac{<(x-<x>)^3>}{\\sigma^{3}}");
       				break;
       	case HIST_KURTOSIS  : sprintf(s,"kurtosis \\frac{<(x-<x>)^4>}{\\sigma^{4}}");
       				break;
       	case HIST_GC  : sprintf(s,"GC estimate 2<x>/\\sigma^2");
       				break;
       	case HIST_GC4 : sprintf(s,"GC estimate at order 4");
       				break;
       	default  : measurement = -1;
    }		

	set_plot_title  (op_out, "%s of histograms", s);
	set_plot_x_title(op_out, "\\tau (points, 1/f_{acq})"); 
	set_plot_y_title(op_out, "%s", s);
    	op_out->filename = Transfer_filename(op->filename);
    	op_out->dir = Mystrdup(op->dir);

	inherit_from_ds_to_ds(ds_out, ds);
	ds_out->treatement = my_sprintf(ds_out->treatement,"%s of set of histograms", s);

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} /* end of function "do_multi_hist_measurement" */







/****************************************************************************/
/* concatenate two sets of histograms, located in two consecutive plots,	*/
/* into one set of histograms 												*/
/****************************************************************************/
int do_concatenate_histograms(void)
{	register int i, j;
	d_s 	*ds2, *ds1, *ds3;
	O_p	*op1, *op2=NULL, *op3=NULL;
	pltreg *pr = NULL;
	int	index, nh, n;
	
	if(updating_menu_state != 0)	return D_O_K;		
       	index = active_menu->flags & 0xFFFFFF00;

	if (key[KEY_LSHIFT])	return win_printf_OK("This routine concatenates datasets in two different plots\n"
		"it uses the current plot, and the previous one.\nA new plot is created.");
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)		return win_printf_OK("cannot plot region");
	if (pr->n_op<2) return win_printf_OK("not enough plots");
	if (pr->cur_op>0) op2 = pr->o_p[pr->cur_op-1];
	else 		  op2 = pr->o_p[pr->n_op-1];

	if (op1->n_dat != op2->n_dat) return(win_printf_OK("plots have not the same number of datasets !"));

	n  = op1->n_dat;
	nh = op1->dat[0]->nx; 
	op3 = create_and_attach_one_plot (pr, nh, nh, 0);

	for (j=0; j<n; j++)
	{	ds1 = op1->dat[j];
		ds2 = op2->dat[j];
		if ( (ds1->nx != nh) || (ds2->nx!=nh) )	return(win_printf_OK("datasets do not have the same number of points!"));

		if (j!=0)
		{ 	if ((ds3 = create_and_attach_one_ds(op3, nh, nh, 0)) == NULL)	
				return win_printf_OK("cannot create dataset !");
		}
		else 
		{	ds3 = op3->dat[0];
		}

		for (i=0; i<nh; i++)
		{	ds3->xd[i] = ds1->xd[i];
			ds3->yd[i] = ds1->yd[i] + ds2->yd[i];
		}

		inherit_from_ds_to_ds(ds3, ds1);

	}

    	op3->iopt |= YLOG;	
	set_plot_title(op2,"histograms, cumulated");
	set_plot_x_title(op2, "x");
	set_plot_y_title(op2, "pdf");

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
}








// fit a pdf by a Gaussian pdf
int do_fit_Gaussian_pdf(void)
{	pltreg	*pr;
	O_p	*op;
	d_s	*ds, *ds_fit=NULL;
	gsl_histogram	*hist;
	float mean, sigma, sum, x;
	int	n_bins;
	int i;
	
	if(updating_menu_state != 0)	return D_O_K;	
	
	if (key[KEY_LSHIFT]) 
	{ 	return(win_printf_OK("This function fits an histogram or a pdf by a Gaussian pdf"
				"a new dataset is created"));
	}
        if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)  return win_printf_OK("cannot find data!");
        
	ds_to_histogram(ds, &hist);
	mean  = (float)gsl_histogram_mean (hist);
	sigma = (float)gsl_histogram_sigma(hist);
	sum 	 = (float)gsl_histogram_sum  (hist);
	gsl_histogram_free(hist);
	
	n_bins = ds->nx;
     ds_fit = create_and_attach_one_ds(op, ds->nx, ds->ny, 0);
	if (ds_fit==NULL) return(win_printf_OK("cannot allocate dataset"));

	for (i=0; i<n_bins; i++)
	{	x 		    = ds->xd[i];
		ds_fit->xd[i] = x;
		ds_fit->yd[i] = (sum*(ds->xd[n_bins-1]-ds->xd[0])/(float)(n_bins))*1./(sqrt(2*M_PI)*sigma) * exp( -(x-mean)*(x-mean)/(2.*sigma*sigma) );
	}
		

	inherit_from_ds_to_ds(ds_fit, ds);
	ds_fit->color = ds->color;
	ds_fit->treatement = my_sprintf(ds_fit->treatement,"Gaussian fit, mean = %f, \\sigma = %f", mean, sigma);

	set_ds_plot_label(ds_fit, ds_fit->yd[ds_fit->nx/2]/n_bins, ds_fit->xd[ds_fit->nx/2], USR_COORD, 
				"\\stack{{mean = %f}{\\sigma = %f}}", mean, sigma);

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
}//end of "do_fit_Gaussien_pdf"





int do_save_pdf(void)
{	d_s 	*ds1;
	O_p	*op;
	pltreg  *pr = NULL;
	gsl_histogram *hist;
	double  mean, sigma;
static int 	bool_normalize=1;
static char 	pdf_filename[512];
	FILE	*file;
	
	if(updating_menu_state != 0)	return D_O_K;		
       	
	if (key[KEY_LSHIFT])	return win_printf_OK("This routine saves the active dataset in a file.\n"
		"a special (3 columns - ascii) format is used, as specified in the GSL.\n"
		"saved pdf can be further used in XVin statistical functions...");
	
	if (win_scanf("{\\color{yellow}\\pt14 Save pdf}\n\n"
		"%b normalize pdf in Y (pdf) before saving\n"
		"filename %s",
		&bool_normalize, &pdf_filename) == CANCEL) return(D_O_K);

	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds1) != 3)		return win_printf_OK("cannot find dataset!");

	ds_to_histogram(ds1,&hist);
	mean  = gsl_histogram_mean(hist);
	sigma = gsl_histogram_sigma(hist);
	if (bool_normalize==1)	normalize_histogram_Y(hist);

	file=fopen(pdf_filename,"wt");
	gsl_histogram_fprintf (file, hist, "%g", "%g");
	// see docs: http://www.gnu.org/software/gsl/manual/html_node/Reading-and-writing-histograms.html
	fclose(file);
	gsl_histogram_free(hist);

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // do_save_pdf




MENU *hist_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"build histogram from dataset", 	do_hist_build_from_ds,			NULL, HIST_NO_AVERAGE,	NULL);
	add_item_to_menu(mn,"build histogram from .dat file", 	do_hist_build_from_file,		NULL, HIST_NO_AVERAGE,	NULL);
	add_item_to_menu(mn,"average and build histograms from dataset",  do_hist_build_from_ds, NULL, HIST_AVERAGE,	NULL);	
	add_item_to_menu(mn,"average and build histogram from .dat file", do_hist_build_from_file, NULL, HIST_AVERAGE,	NULL);
	add_item_to_menu(mn,"\0", 								NULL, NULL, 0, NULL);
	add_item_to_menu(mn,"normalize histogram",				do_hist_normalize,				NULL,HIST_ONE,	NULL);
	add_item_to_menu(mn,"normalize all histograms",	 		do_hist_normalize,				NULL,HIST_ALL,	NULL);
	add_item_to_menu(mn,"normalize all histograms by tau", 	do_normalize_histogram_by_tau,	NULL,HIST_ALL,	NULL);
	add_item_to_menu(mn,"\0", 								NULL, NULL, 0, NULL);
	add_item_to_menu(mn,"info on one pdf",					do_hist_measurement,			NULL, HIST_MAX&HIST_MEAN, NULL);
	add_item_to_menu(mn,"fit Gaussian pdf",					do_fit_Gaussian_pdf,			NULL, 0,				NULL);
	add_item_to_menu(mn,"\0", 								NULL, NULL, 0, NULL);
	add_item_to_menu(mn,"mean of all pdfs",					do_multi_hist_measurement,		NULL, HIST_MEAN,		NULL);
	add_item_to_menu(mn,"sigma of all pdfs",				do_multi_hist_measurement,		NULL, HIST_SIGMA,		NULL);
	add_item_to_menu(mn,"skewness of all pdfs",				do_multi_hist_measurement,		NULL, HIST_SKEWNESS,	NULL);
	add_item_to_menu(mn,"kurtosis of all pdfs",				do_multi_hist_measurement,		NULL, HIST_KURTOSIS,	NULL);
	add_item_to_menu(mn,"max of all pdfs",					do_multi_hist_measurement,		NULL, HIST_MAX,			NULL);
	add_item_to_menu(mn,"sum of all pdfs",					do_multi_hist_measurement,		NULL, HIST_SUM,			NULL);
	add_item_to_menu(mn,"GC estimate",						do_multi_hist_measurement,		NULL, HIST_GC,			NULL);
	add_item_to_menu(mn,"\0", 								NULL, NULL, 0, NULL);
	add_item_to_menu(mn,"concatenate hist. from 2 plots",	do_concatenate_histograms,		NULL,HIST_MAX,	NULL);	
	add_item_to_menu(mn,"save active ds as pdf",		    do_save_pdf,		            NULL, 0, NULL);	
   
//	add_item_to_menu(mn,"\0", 				NULL,NULL,0,NULL);
//	add_item_to_menu(mn,"string to index",	 		do_try_str_to_index,		NULL,0,	NULL);
		
	return mn;
}

int	hist_main(int argc, char **argv)
{
	add_plot_treat_menu_item ( "Histograms", NULL, hist_plot_menu(), 0, NULL);
	return D_O_K;
}


int	hist_unload(int argc, char **argv)
{ 
	remove_item_to_menu(plot_treat_menu, "hist", NULL, NULL);
   	return D_O_K;
}


#endif

