#ifndef _HIST_H_
#define _HIST_H_


// definitions for measures on histograms/pdfs:
#define HIST_MAX        0x0600 /* 2058 */  
#define HIST_MEAN       0x1600 
#define HIST_SIGMA      0x2600 
#define HIST_SUM        0x3600 
#define HIST_SKEWNESS	0x4600 
#define HIST_KURTOSIS	0x5600
#define HIST_GC			0x6600
#define HIST_GC4		0x7600

// definitions for averaging or not before building an histogram:
#define HIST_AVERAGE	0x0100
#define HIST_NO_AVERAGE	0x0200

// definitions for normalizing histogram(s):
#define HIST_ONE		0x0100
#define HIST_ALL		0x0200

PXV_FUNC(MENU*, hist_plot_menu, (void));
PXV_FUNC(int, hist_main,        (int argc, char **argv));
PXV_FUNC(int, hist_unload,      (int argc, char **argv));

PXV_FUNC(int, fill_histogram_with_averaged_float_data, (gsl_histogram *hist, float *x, int nx, int tau, int shift) );
PXV_FUNC(int, fill_histogram_from_1D_file, (gsl_histogram *hist, char *data_filename, int nt, int tau, int shift) );
PXV_FUNC(int, do_hist_build_from_file,     (void));
PXV_FUNC(int, do_hist_build_from_ds,       (void));
PXV_FUNC(int, do_hist_measurement,         (void));
PXV_FUNC(int, do_multi_hist_measurement,   (void));
PXV_FUNC(int, do_hist_normalize,           (void) );
PXV_FUNC(int, ds_to_histogram, (d_s *ds, gsl_histogram **hist) );
PXV_FUNC(int, histogram_to_ds, (gsl_histogram *hist, d_s *ds, int nf) );// ds must already be allocated!!
PXV_FUNC(int, do_fit_Gaussian_pdf,            (void) );

PXV_FUNC(int, normalize_histogram_Y,          (gsl_histogram *hist) );

PXV_FUNC(int, do_save_pdf,	                  (void));

#endif

