
/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    E.Cavatore
  */
#ifndef _PHASEINDEX_C_
#define _PHASEINDEX_C_

# include "allegro.h"
# include "xvin.h"

/* If you include other regular header do it here*/ 
#include <stdio.h>
#include <stdlib.h>

/* But not below this define */
# define BUILDING_PLUGINS_DLL
#define DATA_OUTPUT 0x378
#define STATE_INPUT 0x379

//place here all headers from plugins already compiled

#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "phaseindex.h"
//place here other headers of this plugin 
# include "allegro.h"
# include "xvin.h"

/* If you include other regular header do it here*/ 
// to open the port
//short _stdcall Inp32(short PortAddress);
//void _stdcall Out32(short PortAddress, short data);
 short _stdcall Inp32(short PortAddress);
void _stdcall Out32(short PortAddress, short data);
/*  int do_write(void) */
/* { */

/*   register int i; */
/*   static short int  bit_number=0; */
	
/*   if(updating_menu_state != 0)	return D_O_K; */

/*   /\* display routine action if SHIFT is pressed *\/ */

/*   if (key[KEY_LSHIFT]) */
/*     { */
/*       return win_printf_OK("This routine writes a bitword to data the parallel port"); */
/*     } */
/*   i = win_scanf("define the 8bit_number you want to write as OUTPUT (DATA 0x378) %d",&bit_number); */
/*   if (i == CANCEL)	return D_O_K; */
/*   if ( bit_number > 128) return win_printf_OK("cannot write over 1 byte (=8bits)!!"); */
   
/*   Out32(DATA_OUTPUT, bit_number); */
/*   return D_O_K; */
/* } */
 
//short _stdcall Inp32(short PortAddress);
int do_read_db25_phi(void)
  {
    int phi, in;
    //short _stdcall Inp32(short PortAddress);
    in = (int)Inp32(STATE_INPUT);

    if (in == 191 || in == 159)             phi = 0;
    else if (in == 63 || in == 31) 	    phi = 90;
    else if (in == 255 || in ==  223) 	    phi = 180;
    else if (in == 127 || in == 95)         phi = 270;
    else phi = -90;
    return phi;
 }

int do_read_db25_TTL(void) // return  TTL state applied on pin 12 (Paper End)
  {
    
    int in, TTL_state;
   
    in = (int)Inp32(STATE_INPUT);

    if (in == 191 || in == 63 || in == 255 || in == 127)            TTL_state = 1;
    else if (in == 159 || in == 31 || in == 223 || in == 95 )       TTL_state = 0;
    else TTL_state = -1;
    return TTL_state;
 }
/* int do_read_db25(void)  */
/*   { */
/*     int in, i; */
/*     if(updating_menu_state != 0)	return D_O_K; */
/*     short _stdcall Inp32(short PortAddress); */
/*     in = (int)Inp32(STATE_INPUT); */
/*     i = win_printf("in %8d\n", in); */
/*     if (i == CANCEL)	return D_O_K; */
/*     return D_O_K; */
/*  } */
// 
// int do_static_A_high(void)
//
// {
//	if(updating_menu_state != 0)	return D_O_K;
//	/* display routine action if SHIFT is pressed */
//	if (key[KEY_LSHIFT]) return win_printf_OK("This routine pushes A to high level"); 
//	
//    Out32(DATA_OUTPUT, 3);
//	return D_O_K;
// }  
// 
// int do_static_A_low(void)
// {
//	if(updating_menu_state != 0)	return D_O_K;
//	/* display routine action if SHIFT is pressed */
//	if (key[KEY_LSHIFT]) return win_printf_OK("This routine pushes A to low level");
//	
//    Out32(DATA_OUTPUT, 1);
//	return D_O_K;
// }  
// 
// int do_run_A(void)
// {
//	
//	if(updating_menu_state != 0)	return D_O_K;
//	/* display routine action if SHIFT is pressed */
//	if (key[KEY_LSHIFT]) return win_printf_OK("This routine pushes A to run");
//    
//    Out32(DATA_OUTPUT, 0);
//	return D_O_K;
// }  
//
// int do_static_B_high(void)
//
// {
//	if(updating_menu_state != 0)	return D_O_K;
//	/* display routine action if SHIFT is pressed */
//	if (key[KEY_LSHIFT]) return win_printf_OK("This routine pushes B to high level"); 
//	
//    Out32(DATA_OUTPUT, 12);
//	return D_O_K;
// }  
// 
// int do_static_B_low(void)
// {
//	if(updating_menu_state != 0)	return D_O_K;
//	/* display routine action if SHIFT is pressed */
//	if (key[KEY_LSHIFT]) return win_printf_OK("This routine pushes B to low level");
//	
//    Out32(DATA_OUTPUT, 4);
//	return D_O_K;
// }  
// 
// int do_run_B(void)
// {
//	
//	if(updating_menu_state != 0)	return D_O_K;
//	/* display routine action if SHIFT is pressed */
//	if (key[KEY_LSHIFT]) return win_printf_OK("This routine pushes B to run");
// 
//    Out32(DATA_OUTPUT, 0);
//	return D_O_K;
// }   
 
/*  int do_asynch(void) */
/*  { */
/* 	register int i; */
/* 	if(updating_menu_state != 0)	return D_O_K; */

/* 	static int mode1 = 0, mode2 = 0, mode3 = 0, mode4 = 0; */
/* 	/\* display routine action if SHIFT is pressed *\/ */
/* 	if (key[KEY_LSHIFT]) return win_printf_OK("This routine applies a given phase shifting between modulation (without phase changement in time"); */
/*     i = win_scanf("phi = 0 %b\n" */
/*                   "phi = pi/2 %b\n" */
/*                   "phi = pi %b\n" */
/*                   "phi = 3pi/2 %b\n"                  , &mode1, &mode2, &mode3, &mode4); */
/*     if (i == CANCEL)	return D_O_K; */
    
/*     if (mode1 == 1) Out32(DATA_OUTPUT,54); */
/*     if (mode2 == 1) Out32(DATA_OUTPUT,55); */
/*     if (mode3 == 1) Out32(DATA_OUTPUT,62); */
/*     if (mode4 == 1) Out32(DATA_OUTPUT,63); */
    
/* 	return D_O_K; */
/*  }    */
 
 int do_phase_control(void)

 {
	register int i;
	if(updating_menu_state != 0)	return D_O_K;
 
	static int mode1 = 0, mode2 = 0, mode3 = 0, mode4 = 0, mode5 = 0, mode6 = 0, mode7 = 0, mode8 = 0, mode9 = 0, mode10 = 0, mode11 =0, mode12 = 0, mode13 = 0;
	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	  {
		return win_printf_OK("This routine control a multiplexer command in 4bits via parallele port and define phase beetween modulation");
	  }
    i = win_scanf("asynchronous modulation:given phase shifting between modulations (without phase changement in time)\n"
                  "phi = 0 %b\n"
                  "phi = pi/2 %b\n"
                  "phi = pi %b\n"
                  "phi = 3pi/2 %b\n"
                  "synchronous modulation : free-running modes\n"
                  "mode 2 phases pushed every frame\n"
                  "phi = 0, phi = pi/2 %b\n"
                  "phi = 0, phi = pi %b\n"
                  "phi = pi/2, phi = 3pi/2 %b\n"
                  "phi = pi, phi = 3pi/2 %b\n"
                  "mode 2 phases pushed every 2frames\n"
                  "phi = 0, phi = pi/2 %b\n"
                  "phi = 0, phi = pi %b\n"
                  "phi = pi/2, phi = 3pi/2 %b\n"
                  "phi = pi, phi = 3pi/2 %b\n"
                  "mode 4 phases pushed every frame\n"
                  "0, pi/2, pi, 3pi/2 %b\n"
                  , &mode1, &mode2, &mode3, &mode4, &mode5, &mode6, &mode7, &mode8, &mode9, &mode10, &mode11, &mode12, &mode13);
    if (i == CANCEL)	return D_O_K;

    if (mode1 == 1) Out32(DATA_OUTPUT,54);
    if (mode2 == 1) Out32(DATA_OUTPUT,55);
    if (mode3 == 1) Out32(DATA_OUTPUT,62);
    if (mode4 == 1) Out32(DATA_OUTPUT,63);
    if (mode5 == 1) Out32(DATA_OUTPUT,48);
    if (mode6 == 1) Out32(DATA_OUTPUT,6);
    if (mode7 == 1) Out32(DATA_OUTPUT,7);
    if (mode8 == 1) Out32(DATA_OUTPUT,56);
    if (mode9 == 1) Out32(DATA_OUTPUT,52);
    if (mode10 == 1) Out32(DATA_OUTPUT,38);
    if (mode11 == 1) Out32(DATA_OUTPUT,39);
    if (mode12 == 1) Out32(DATA_OUTPUT,60);
    if (mode13 == 1) Out32(DATA_OUTPUT,32);
    
    return D_O_K;
 }
 
 
 
  
MENU *phaseindex_plot_menu(void)

 {
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	//add_item_to_menu(mn,"write on parallele port", do_write,NULL,0,NULL);
	//add_item_to_menu(mn,"read on parallele port", do_read_db25,NULL,0,NULL);
	//add_item_to_menu(mn,"A to high level", do_static_A_high,NULL,0,NULL);
	//add_item_to_menu(mn,"A to low level", do_static_A_low,NULL,0,NULL);
	//add_item_to_menu(mn,"A to run", do_run_A,NULL,0,NULL);
	//add_item_to_menu(mn,"B to high level", do_static_B_high,NULL,0,NULL);
	//add_item_to_menu(mn,"B to low level", do_static_B_low,NULL,0,NULL);
	//add_item_to_menu(mn,"B to run", do_run_B,NULL,0,NULL);
	//add_item_to_menu(mn,"asynchronous modulation (no phase changement)", do_asynch,NULL,0,NULL);
	add_item_to_menu(mn,"phase control", do_phase_control,NULL,0,NULL);
	
	return mn;
 }

int	phaseindex_main(int argc, char **argv)
 {
	add_plot_treat_menu_item ( "phaseindex", NULL, phaseindex_plot_menu(), 0, NULL);
 	return D_O_K;
 }

int	phaseindex_unload(int argc, char **argv)
 {
	remove_item_to_menu(plot_treat_menu, "phaseindex", NULL, NULL);
	return D_O_K;
 }
#endif

