#ifndef _PHASEINDEX_H_
#define _PHASEINDEX_H_

PXV_FUNC(int, do_phaseindex_rescale_plot, (void));
PXV_FUNC(MENU*, phaseindex_plot_menu, (void));
PXV_FUNC(int, do_phaseindex_rescale_data_set, (void));
PXV_FUNC(int, phaseindex_main, (int argc, char **argv));
#endif

