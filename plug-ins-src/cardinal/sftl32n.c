#ifndef _SFTL32N_C_
#define _SFTL32N_C_
/*   fftl32n.c
 *
 *	24 Juillet. 2001	VC
 *
 *	A collection of routines for performing  Fourier Transforms on
 *	real and complex data.
 */
# include <stdlib.h>
# include <stdio.h>
# include <math.h>
# include <malloc.h>
# include "sftl32n.h"



# ifdef FORTIFY
# include "fortify.h"
# endif

static int oldnpts = 0, nptsmax = 0;

/*	sft_init()
 *	DESCRIPTION	Calculates the array of sines and cosines that will be needed
 *			for the sft. This routine is called by sft(). The
 *			user may call this routine explicitly before calling
 *			sft() for the sake of using the array sftsin[] and sftcos[] for
 *			windowing prior to calculating the sft.
 *
 *	RETURNS		0 on success, 1 on npts which is not a power of two,
 *			2 on array memory could not be allocated.
 */
int sft_init(int npts)
{
	register int i;
	int n_2;
	double tmp;

#ifdef DEBUG
	fprintf(stderr,"Initialisation: sftsin[i]\n");
#endif


	n_2 = npts / 2;
	if (npts != oldnpts)
	{
		if (n_2 > nptsmax)
		{
			sftsin = (float*)realloc(sftsin,(n_2+1)*sizeof(float));
			sftcos = (float*)realloc(sftcos,(n_2+1)*sizeof(float));
			if (sftsin == NULL || sftcos == NULL)	return 2;
			nptsmax = n_2;
		}
		for (i = 0 ; i <= n_2 ; i ++)
		{
			tmp = (2* M_PI * i) / npts;
			sftsin[i] =  sin (tmp);
			sftcos[i] =  cos (tmp);
		}
		oldnpts = npts;
	}
	return 0;
}

int sftmixing(int npts, float *x)
{
  (void)x;
  (void)npts;
	return 0;
}

/*	sft()
 *	DESCRIPTION	Caluculates a Fourier Transform on the array x. result in ak
 *			A forward transform is calculated for df=1, backward
 *			for df=-1. The array x is real data, the ak complex data
 *			is placed in alternating real and imaginary
 *			terms. npts is the number of real points. The ak array order
 *			depends upon npts parity :
			npts even -> ak[0] = Re(0), ak[1] = Re(npts/2), 
			ak[2i] = Re(i), ak[2i+1] = Im(i) like in FFT
			npts odd -> ak[0] = Re(0), ak[1] = Re(1), ak[2] = Im(1), 
			ak[2i-1] = Re(i), ak[2i] = Im(i).
			In all cases, the number of elements in ak[] equals those of x[].
			Only positive k modes are computed from k = 0 to k = npts/2
 *
 *	RETURNS		0 on success, non-0 on failure (see sft_init() above).
 */
int sft_real(int npts, float *x, int df, float *ak)
{
	register int j, k;
	int n_2, kr, ki, ik, odd;
	float co, si ;

#ifdef DEBUG
	fprintf (stderr, "SFT()...\n");
#endif

	if (npts != oldnpts)
	{
		if ((j = sft_init(npts)) != 0)
			return j;
	}
	n_2 = npts/2;
	odd = npts%2;
	if (df > 0)
	{
		for ( j = 0, ak[0] = 0; j < npts; j++)	ak[0] += x[j]; /* mode 0 */
		ak[0] /= npts;
		for ( k = 1; k < (n_2+odd); k++)
		{
			kr = 2*k - odd;
			ki = kr + 1;
			for ( j = 0, ak[kr] = ak[ki] = 0; j < npts; j++)
			{
				ik = k*j;
				ik %= npts;
				if (ik > n_2)
				{
					co = sftcos[npts - ik];
					si = -sftsin[npts - ik];
				}
				else
				{
					co = sftcos[ik];
					si = sftsin[ik];
				}
				ak[kr] += x[j] * co;
				ak[ki] -= x[j] * si;
			}
			ak[kr] = (2*ak[kr])/npts;	
			ak[ki] = (2*ak[ki])/npts;	
		}
		if (odd == 0)
		{
			for (j = 0, ak[1] = 0; j < npts; j+=2)	
				ak[1] += x[j] - x[j+1]; /* mode n/2 */
			ak[1] /= npts;
		}
/*		win_printf("odd %d ak[1] = %g",odd,ak[1]);*/
		
	}
	else if (df < 0)
	{
/*		win_printf("odd %d ak[1] = %g",odd,ak[1]);*/
		for (j = 0; j < npts; j++)	
		{
			for (x[j] = ak[0], k = 1; k < (n_2+odd); k++)
			{
				kr = 2*k - odd;
				ki = kr + 1;	
				ik = k*j;
				ik %= npts;
				if (ik > n_2)
				{
					co = sftcos[npts - ik];
					si = -sftsin[npts - ik];
				}
				else
				{
					co = sftcos[ik];
					si = sftsin[ik];
				}							
				x[j] += ak[kr] * co - ak[ki] * si;
			}
			if (odd == 0) x[j] += (j%2) ? -ak[1] : ak[1];
		}
	}
	return 0;
}
/*	sft()
 *	DESCRIPTION	Caluculates a Fourier Transform on the array x. result in ak
 *			A forward transform is calculated for df=1, backward
 *			for df=-1. The array x is complex data, (re im, re im) the ak 
 *			complex data is placed in alternating real and imaginary
 *			terms. npts is the number of complex points
			The number of elements of x and ak are equals to 2npts.
			ak[] range from k = 0 to k = npts - 1. The mode with k > npts/2
			correspond to negative k : c[k] = c^* [npts - k]
 *
 *	RETURNS		0 on success, non-0 on failure (see sft_init() above).
 */
int sft_complex(int npts, float *x, int df, float *ak)
{
	register int i, j, k;
	int n_2, kr, ki, ik; //, odd;
	float co, si ;

#ifdef DEBUG
	fprintf (stderr, "SFT()...\n");
#endif

	if (npts != oldnpts)
	{
		if ((j = sft_init(npts)) != 0)
			return j;
	}
	n_2 = npts/2;
	//odd = npts%2;
	if (df > 0)
	{
		for (k = 0; k < npts; k++)
		{
			kr = 2*k;
			ki = kr + 1;
			for ( j = 0, ak[kr] = ak[ki] = 0; j < npts; j++)
			{
				ik = k*j;
				ik %= npts;
				if (ik > n_2)
				{
					co = sftcos[npts - ik];
					si = -sftsin[npts - ik];
				}
				else
				{
					co = sftcos[ik];
					si = sftsin[ik];
				}
				i = j<<1;
				ak[kr] += x[i] * co + si * x[i+1];
				ak[ki] -= x[i] * si - co * x[i+1];
			}
			ak[kr] /= npts;
			ak[ki] /= npts;			
		}
	}
	else if (df < 0)
	{
		for (k = 0; k < npts; k++)
		{
			kr = 2*k;
			ki = kr + 1;
			for ( j = 0, x[kr] = x[ki] = 0; j < npts; j++)
			{
				ik = k*j;
				ik %= npts;
				if (ik > n_2)
				{
					co = sftcos[npts - ik];
					si = -sftsin[npts - ik];
				}
				else
				{
					co = sftcos[ik];
					si = sftsin[ik];
				}
				i = j<<1;
				x[kr] += ak[i] * co - si * ak[i+1];
				x[ki] += ak[i] * si + co * ak[i+1];
			}
		}	
	}
	return 0;
}



/*	sftwindow()
 *	DESCRIPTION	Performs a standard cosine window on an entire real
 *			data set. The window does not work for complex data.
 *			sft_init() need not be called before this routine
 *			and the sine table is filled by it. The invet function is provided
			but cannot inverte the 0 point.
 *
 *	RETURNS		0 on success, non-0 on failure (see sft_init()).
 */
int sftwindow(int npts, float *x)
{
	register int i, j;
	int n_2, odd;

#ifdef DEBUG
	fprintf(stderr,"Windowing...\n");
#endif
	if (npts != oldnpts)
	{
		if ((j = sft_init(npts)) != 0)
			return j;
	}

	n_2 = npts/2;
	odd = npts%2;

	for (i=0; i< (n_2+odd); i++)
		x[i] *= (1.0 - sftcos[i]);
	for (i=1; i< (n_2+odd); i++)
		x[npts-i] *= (1.0 - sftcos[i]);
	return 0;
}


int desftwindow(int npts, float *x)
{
	register int i, j;
	int n_2, odd;

#ifdef DEBUG
	fprintf(stderr,"Windowing...\n");
#endif
	if (npts != oldnpts)
	{
		if ((j = sft_init(npts)) != 0)
			return j;
	}

	n_2 = npts/2;
	odd = npts%2;

	for (i=1; i< (n_2+odd); i++)
		x[i] /= (1.0 - sftcos[i]);
	for (i=1; i< (n_2+odd); i++)
		x[npts-i] /= (1.0 - sftcos[i]);
	return 0;
}


/*	sftwindow1()
 *	DESCRIPTION	Performs a standard cosine window on an entire real
 *			data set. This window does not work for complex data.
 *			sft_init() need not be called before this routine
 *			and the sine table is filled by it. This windows does not
			reach 0 but smp (typically 0.05) at 0 and npts, thus it may be
			inverted with desftwindow1()
 *
 *	RETURNS		0 on success, non-0 on failure (see sft_init()).
 */

int sftwindow1(int npts, float *x, float smp)
{
	register int i, j;
	int n_2, odd;

#ifdef DEBUG
	fprintf(stderr,"Windowing...\n");
#endif
	if (npts != oldnpts)
	{
		if ((j = sft_init(npts)) != 0)
			return j;
	}

	n_2 = npts/2;
	odd = npts%2;

	if (odd)
	{
		for (i=0; i< (n_2+1); i++)
			x[i] *= (1.0+smp - sftcos[i]);
		for (i=1; i< (n_2+1); i++)
			x[npts-i] *= (1.0+smp - sftcos[i]);
	}
	else
	{
		for (i=0; i<= n_2; i++)
			x[i] *= (1.0+smp - sftcos[i]);
		for (i=1; i< n_2; i++)
			x[npts-i] *= (1.0+smp - sftcos[i]);	
	}
	return 0;
}

int desftwindow1(int npts, float *x, float smp)
{
	register int i, j;
	int n_2, odd;

#ifdef DEBUG
	fprintf(stderr,"Windowing...\n");
#endif
	if (npts != oldnpts)
	{
		if ((j = sft_init(npts)) != 0)
			return j;
	}

	n_2 = npts/2;
	odd = npts%2;

	if (odd)
	{
		for (i=0; i< (n_2+1); i++)
			x[i] /= (1.0+smp - sftcos[i]);
		for (i=1; i< (n_2+1); i++)
			x[npts-i] /= (1.0+smp - sftcos[i]);
	}
	else
	{
		for (i=0; i<= n_2; i++)
			x[i] /= (1.0+smp - sftcos[i]);
		for (i=1; i< n_2; i++)
			x[npts-i] /= (1.0+smp - sftcos[i]);	
	}
	return 0;
}

/*	sftwc()
 *	DESCRIPTION	Performs a standard cosine window on an entire complex
 *			data set. 
 *			sft_init() need not be called before this routine
 *			and the sine table is filled by it. The invert function is provided
			but cannot invert the 0 point.
 *
 *	RETURNS		0 on success, non-0 on failure (see sft_init()).
 */


int sftwc(int npts, float *x)
{
	register int i, j;
	int n_2, odd;
	float tmp;

#ifdef DEBUG
	fprintf(stderr,"Windowing...\n");
#endif
	if (npts != oldnpts)
	{
		if ((j = sft_init(npts)) != 0)
			return j;
	}

	n_2 = npts/2;
	odd = npts%2;
	for (i=0; i< (n_2+odd); i++)
	{
		j = i << 1;
		tmp = (1.0 - sftcos[i]);
		x[j++] *= tmp;
		x[j] *= tmp;
	}
	for (i=1; i< (n_2+odd); i++)
	{
		j = (npts-i) << 1;
		tmp = (1.0 - sftcos[i]);
		x[j++] *= tmp;
		x[j] *= tmp;
	}	
	return 0;
}

int desftwc(int npts, float *x)
{
	register int i, j;
	int n_2, odd;
	float tmp;

#ifdef DEBUG
	fprintf(stderr,"Windowing...\n");
#endif
	if (npts != oldnpts)
	{
		if ((j = sft_init(npts)) != 0)
			return j;
	}

	n_2 = npts/2;
	odd = npts%2;
	for (i=1; i< (n_2+odd); i++)
	{
		j = i << 1;
		tmp = (1.0 - sftcos[i]);
		x[j++] /= tmp;
		x[j] /= tmp;
	}
	for (i=1; i< (n_2+odd); i++)
	{
		j = (npts-i) << 1;
		tmp = (1.0  - sftcos[i]);
		x[j++] /= tmp;
		x[j] /= tmp;
	}	
	return 0;
}

/*	sftwc1()
 *	DESCRIPTION	Performs a standard cosine window on an entire complex
 *			data set. sft_init() need not be called before this routine
 *			and the sine table is filled by it. This windows does not
			reach 0 but smp (typically 0.05) at 0 and npts, thus it may be
			inverted with desftwc1()
 *
 *	RETURNS		0 on success, non-0 on failure (see sft_init()).
 */

int sftwc1(int npts, float *x, float smp)
{
	register int i, j;
	int n_2, odd;
	float tmp;

#ifdef DEBUG
	fprintf(stderr,"Windowing...\n");
#endif
	if (npts != oldnpts)
	{
		if ((j = sft_init(npts)) != 0)
			return j;
	}

	n_2 = npts/2;
	odd = npts%2;
	for (i=0; i< (n_2+odd); i++)
	{
		j = i << 1;
		tmp = (1.0 + smp - sftcos[i]);
		x[j++] *= tmp;
		x[j] *= tmp;
	}
	for (i=1; i< (n_2+odd); i++)
	{
		j = (npts-i) << 1;
		tmp = (1.0 + smp - sftcos[i]);
		x[j++] *= tmp;
		x[j] *= tmp;
	}	
	return 0;
}




int desftwc1(int npts, float *x, float smp)
{
	register int i, j;
	int n_2, odd;
	float tmp;

#ifdef DEBUG
	fprintf(stderr,"Windowing...\n");
#endif
	if (npts != oldnpts)
	{
		if ((j = sft_init(npts)) != 0)
			return j;
	}

	n_2 = npts/2;
	odd = npts%2;
	for (i=0; i< (n_2+odd); i++)
	{
		j = i << 1;
		tmp = (1.0 + smp - sftcos[i]);
		x[j++] /= tmp;
		x[j] /= tmp;
	}
	for (i=1; i< (n_2+odd); i++)
	{
		j = (npts-i) << 1;
		tmp = (1.0 + smp - sftcos[i]);
		x[j++] /= tmp;
		x[j] /= tmp;
	}	
	return 0;
}


/*	spec_real_slow()
 *
 *	Compute the spectrum for the Fourier transform of real data.
 *	If data was npts points then the spectrum has npts/2 + 1 points
 *
 *	RETURNS		nothing	 
 */
void spec_real_slow (int npts, float *x, float *spe)
{
	register int i, k;
	int n_2, odd;
	float tmp = 0;
	
	n_2 = npts/2;
	odd = npts%2;
	spe[0] =  x[0]*x[0];
	if (odd == 0)	tmp = x[1]*x[1];
	for (i = 1; i < (n_2+odd) ; i++)
	{
		k = 2*i - odd;
		spe[i] = x[k] *x[k] + x[k+1]*x[k+1];
	}
	if (odd == 0)	spe[n_2] = tmp; 
}

/*	spec_comp_slow()
 *
 *	Compute the spectrum for the Fourier transform of complex data.
 *	If data was npts/2 complex points then the spectrum has npts/2
 *	real points
 *
 *	RETURNS		nothing	 
 */
void spec_comp_slow (int npts, float *x, float *spe)
{
	register int i, k, j;
	//int n_2;
	
	//n_2 = npts/2;
	for (i = 0; i < npts ; i++)
	{
		k = 2*i;
		j = k+1;
		spe[i] = x[k] * x[k] + x[j] * x[j];
	}
}
/*	demodulate_slow a spectrum
*	the initial array is the sft of real data, the output is the sft
*	of complex data, npts is the number of memory boxes neaded
*/
int demodulate_slow (int npts, float *x, int freq)
{
	register int i;
	int n_2;
	float *x1, *x2, tmp;
	
	
	n_2 = npts/2;
	if ( 2*freq > n_2)
		return 3;
	
	x1 = x + 2*freq;
	x2 = x +npts -2*freq;

	
	for (i = 2*freq -1; i >= 0 ; i--)
	{
		tmp=x[i];
		x[i]=x1[i];
		x2[i]=-tmp;
		i--;
		tmp=x[i];
		x[i]=x1[i];
		x2[i]=tmp;
	}
	for (i = 2*freq; i < npts -2*freq; i++)
		x[i]=0;
	return 0;
}
/*	derive a spectrum
*	the initial array is the sft of real data, the output is the sft
*	of real data, npts is the number of memory boxes neaded
*/
void dderive_real_slow (int npts, float *x, float *y)
{
	register int i,k;
	float tmp;
	
	y[0]=0;
	y[1]=0;

	
	for (i = 2; i <npts ; i++, i++)
	{
		k = i/2;
		tmp=x[i];
		y[i]=k*x[i+1];
		y[i+1]=-k*tmp;
	}
}

void amp_slow(int npts, float *x, float *y)
{
	register int i,j;
	int n_2;
	float tmp;
	
	n_2 = npts /2;

	for (i = 0; i < n_2; i++)
	{
		j=2*i;
		tmp = x[j]*x[j]+x[j+1]*x[j+1];
		y[i] = sqrt (tmp);
	}
}

# ifdef EXE
# include"xvin.h"
# include"xvin2.h"
# include"xvadd.h"


int	do_sft_real(char ch)
{
	register int  j;
	O_p *op, *opn;
	d_s *ds, *dsi;
	pltreg *pr;
	int nf, index = mn_index;


	if (ch != CR) 		return OFF;		/* always do that */
	if (last_m_event.kbstat & GR_KB_SHIFT)	/* display routine action if SHIFT is pressed */
		return win_printf("This routine multiply the y coordinate\n of a data set by a number and place it in a new plot");
	/* we first find the data that we need to transform */
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
		return win_printf("cannot find data");

	nf = dsi->nx;	/* this is the number of points in the data set */


	if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
		return win_printf("cannot create plot !");
	ds = opn->dat[0];

	if (index > 2)
	{
		for (j = 0; j < nf; j++)
			ds->xd[j] = j;

		sft_init(nf);
		sft_real(nf, dsi->yd, 1, ds->yd);
/*		sft_real_phase(nf, dsi->yd, ds->yd);*/

	}
	else if (index > 0) /* we do spectrum */
	{
		for (j = 0; j < nf; j++)
			ds->xd[j] = j;

		sft_init(nf);
		sft_real(nf, dsi->yd, 1, ds->yd);
		if (index > 1)
		{
			spec_real_slow (nf, ds->yd, ds->yd);
			ds->nx = ds->ny = 1 + nf/2;
		}
	}
	else
	{
		for (j = 0; j < nf; j++)
			ds->xd[j] = j;

		sft_init(nf);
		sft_real(nf, ds->yd, -1, dsi->yd);
	}

	/* now we must do some house keeping */
	inherit_from_ds_to_ds(ds, dsi);
	ds->treatement = my_sprintf(ds->treatement,"sft over %d",nf);
	set_plot_title(opn, "sft over %d",nf);
	if (op->x_title != NULL) set_plot_x_title(opn, op->x_title);
	if (op->y_title != NULL) set_plot_y_title(opn, op->y_title);
	opn->filename = Transfer_filename(op->filename);
	uns_op_2_op(opn, op);
	/* refisplay the entire plot */
	refresh_plot(pr, UNCHANGED);
	switch_plot(0);
	switch_data_set(0);
	return 0;
}

# ifdef ENCOURS
int	do_sft_real_noise(char ch)
{
	register int  j;
	O_p *op, *opn;
	d_s *ds, *dsi;
	pltreg *pr;
	int nf, index = mn_index;
	float dt, dy;
    static int w_flag = 1;
 	un_s *un;


	if (ch != CR) 		return OFF;		/* always do that */
	if (last_m_event.kbstat & GR_KB_SHIFT)	/* display routine action if SHIFT is pressed */
		return win_printf("This routine compute the noise spectrum of a data set using slow fft");
	/* we first find the data that we need to transform */
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
		return win_printf("cannot find data");

	nf = dsi->nx;	/* this is the number of points in the data set */


	un = op->yu[op->c_yu];
	if (un->type != IS_METER || un->decade != IS_MICRO)
		win_printf("warning the y unit is not \\mu m !\nnumerical value will be wrong");
	dy = un->dx;
	un = op->xu[op->c_xu];
	if (un->type != IS_SECOND || un->decade != 0)
		win_printf("warning the x unit is not seconds !\nnumerical value will be wrong");
	dt = un->dx;


    i = win_scanf("do you want Hanning data window \n(1-> yes  0->non) %d",w_flag);

	if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
		return win_printf("cannot create plot !");
	ds = opn->dat[0];


	if (index > 2)
	{
		for (j = 0; j < nf; j++)
			ds->xd[j] = j;

		sft_init(nf);
		sft_real(nf, dsi->yd, 1, ds->yd);
/*		sft_real_phase(nf, dsi->yd, ds->yd); */

	}
	else if (index > 0) /* we do spectrum */
	{
		sft_init(nf);
		for (j = 0; j < nf; j++)       		ds->xd[j] = ds->yd[j];
		if (w_flag)
		{
            sftwindow(nf, ds->xd);
			for (i=0 ,mean = 0; i< nf ; i++)	mean += ds->xd[i];
			for (i=0 ; i< nf ; i++) 			ds->xd[i] = ds->yd[j];
		}
		else
		{
			for (i=0 ,mean = 0; i< nf ; i++)	mean += ds->xd[i];
		}
		mean /= nsp;
		for (i=0 ,mean = 0; i< nsp ; i++)		ds->xd[i] -= mean;
		if ( w_flag)							sftwindow(nf, ds->xd);
		sft_real(nf, dsi->xd, 1, ds->xd);
		spec_real_slow (nf, ds->xd, ds->yd);
		ds->nx = ds->ny = 1 + nf/2;
		for (j = 0; j < nf; j++) 				ds->xd[j] = j;

		for (i=0 ; i<= nf/2; i++)
		{
			ds->yd[i] *= (w_flag & 1) ? (dy *dy)/3 : (dy * dy)/2;
			ds->yd[i] *=  (dt * nf); 
			ds->xd[i] /=  (dt * nf); 
		}		
		pr->one_p->iopt |= YLOG;
		pr->one_p->iopt |= XLOG;
	
		set_plot_y_title(opd,"<x^2> (\\mu m^2.Hz^{-1})");
		set_plot_x_title(opd,"frequency (Hz)");

		
	}
	else
	{
		for (j = 0; j < nf; j++)
			ds->xd[j] = j;

		sft_init(nf);
		sft_real(nf, ds->yd, -1, dsi->yd);
	}

	/* now we must do some house keeping */
	inherit_from_ds_to_ds(ds, dsi);
	ds->treatement = my_sprintf(ds->treatement,"sft over %d",nf);
	set_plot_title(opn, "sft over %d",nf);
	if (op->x_title != NULL) set_plot_x_title(opn, op->x_title);
	if (op->y_title != NULL) set_plot_y_title(opn, op->y_title);
	opn->filename = Transfer_filename(op->filename);
	uns_op_2_op(opn, op);
	/* refisplay the entire plot */
	refresh_plot(pr, UNCHANGED);
	switch_plot(0);
	switch_data_set(0);
	return 0;
}

# endif


int	do_sft_comp(char ch)
{
	register int  j;
	O_p *op, *opn;
	d_s *ds, *dsi;
	pltreg *pr;
	int nf, index = mn_index;


	if (ch != CR) 		return OFF;		/* always do that */
	if (last_m_event.kbstat & GR_KB_SHIFT)	/* display routine action if SHIFT is pressed */
		return win_printf("This routine compute de fourier transform\n of a complex data set and place it in a new plot");
	/* we first find the data that we need to transform */
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
		return win_printf("cannot find data");

	nf = dsi->nx;	/* this is the number of points in the data set */

	if (nf%2)	return win_printf("complex data should be even nf %d",nf);
	if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
		return win_printf("cannot create plot !");
	ds = opn->dat[0];

	if (index > 0)
	{
		for (j = 0; j < nf; j++)
			ds->xd[j] = j;

		sft_init(nf/2);
		sft_complex(nf/2, dsi->yd, 1, ds->yd);
		if (index > 1)
		{
			spec_comp_slow (nf/2, ds->yd, ds->yd);
		}
	}
	else
	{
		for (j = 0; j < nf; j++)
			ds->xd[j] = j;

		sft_init(nf/2);
		sft_complex(nf/2, ds->yd, -1, dsi->yd);
	}
	if (index > 1)	ds->nx = ds->ny = nf;
	/* now we must do some house keeping */
	inherit_from_ds_to_ds(ds, dsi);
	ds->treatement = my_sprintf(ds->treatement,"sft comp over %d",nf/2);
	set_plot_title(opn, "sft comp over %d",nf/2);
	if (op->x_title != NULL) set_plot_x_title(opn, op->x_title);
	if (op->y_title != NULL) set_plot_y_title(opn, op->y_title);
	opn->filename = Transfer_filename(op->filename);
	uns_op_2_op(opn, op);
	/* refisplay the entire plot */
	refresh_plot(pr, UNCHANGED);
	switch_plot(0);
	switch_data_set(0);
	return 0;
}
int	do_merge_to_comp(char ch)
{
	register int  j;
	O_p *op, *opn;
	d_s *ds, *dsr, *dsi;
	pltreg *pr;
	int nf;


	if (ch != CR) 		return OFF;		/* always do that */
	if (last_m_event.kbstat & GR_KB_SHIFT)	/* display routine action if SHIFT is pressed */
		return win_printf("This routine construct a complex data set from two separated one (Re, Im)");
	/* we first find the data that we need to transform */
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsr) != 3)
		return win_printf("cannot find data");

	j = op->cur_dat + 1;
	j %= op->n_dat;
	if (j == op->cur_dat)
		return win_printf("you need at least a Re and Im data sets!");

	dsi = op->dat[j];

	if (dsr->nx != dsi->nx || dsr->ny != dsi->ny)
		return win_printf("data sets have different points numbers\n dsr->nx %d dsi->nx %d\n dsr->ny %d dsi->ny %d",dsr->nx,dsi->nx,dsr->ny,dsi->ny);

	nf = dsr->nx;	/* this is the number of points in the data set */

	if ((opn = create_and_attach_one_plot(pr, 2*nf, 2*nf, 0)) == NULL)
		return win_printf("cannot create plot !");
	ds = opn->dat[0];

	for (j = 0; j < nf; j++)
	{
		ds->xd[2*j] = j;
		ds->xd[2*j+1] = j;
		ds->yd[2*j] = dsr->yd[j];
		ds->yd[2*j+1] = dsi->yd[j];
	}
	/* now we must do some house keeping */
	inherit_from_ds_to_ds(ds, dsi);
	ds->treatement = my_sprintf(ds->treatement,"comp data set ");
	set_plot_title(opn, "comp data set ");
	if (op->x_title != NULL) set_plot_x_title(opn, op->x_title);
	if (op->y_title != NULL) set_plot_y_title(opn, op->y_title);
	opn->filename = Transfer_filename(op->filename);
	uns_op_2_op(opn, op);
	/* refisplay the entire plot */
	refresh_plot(pr, UNCHANGED);
	switch_plot(0);
	switch_data_set(0);
	return 0;
}
int	do_window(char ch)
{
	register int  j;
	O_p *op;
	d_s *ds, *dsi;
	pltreg *pr;
	int nf, index = mn_index;
	float smp = 0.05;

	if (ch != CR) 		return OFF;		/* always do that */
	if (last_m_event.kbstat & GR_KB_SHIFT)	/* display routine action if SHIFT is pressed */
		return win_printf("This routine multiply the y coordinate\n of a data set by a hanning window");
	/* we first find the data that we need to transform */
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
		return win_printf("cannot find data");

	nf = dsi->nx;	/* this is the number of points in the data set */


	if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
		return win_printf("cannot create plot !");

	for (j = 0; j < nf; j++)
	{
		ds->yd[j] = dsi->yd[j];
		ds->xd[j] = dsi->xd[j];
	}
	if (index == 1)			sftwindow(nf, ds->yd);
	else if (index == 2)		sftwindow1(nf, ds->yd, smp);
	else if (index == -1)	desftwindow(nf, ds->yd);
	else if (index == -2)	desftwindow1(nf, ds->yd, smp);

	/* now we must do some house keeping */
	inherit_from_ds_to_ds(ds, dsi);
	ds->treatement = my_sprintf(ds->treatement,"y multiplied by window");
	/* refisplay the entire plot */
	refresh_plot(pr, UNCHANGED);
	switch_plot(0);
	switch_data_set(0);
	return 0;
}
int	do_window_comp(char ch)
{
	register int  j;
	O_p *op;
	d_s *ds, *dsi;
	pltreg *pr;
	int nf, index = mn_index;
	float smp = 0.05;

	if (ch != CR) 		return OFF;		/* always do that */
	if (last_m_event.kbstat & GR_KB_SHIFT)	/* display routine action if SHIFT is pressed */
		return win_printf("This routine multiply the y coordinate\n of a complex data set by a hanning window");
	/* we first find the data that we need to transform */
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
		return win_printf("cannot find data");

	nf = dsi->nx;	/* this is the number of points in the data set */


	if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
		return win_printf("cannot create plot !");

	for (j = 0; j < nf; j++)
	{
		ds->yd[j] = dsi->yd[j];
		ds->xd[j] = dsi->xd[j];
	}
	if (index == 1)			sftwc(nf/2, ds->yd);
	else if (index == 2)	sftwc1(nf/2, ds->yd, smp);
	else if (index == -1)	desftwc(nf/2, ds->yd);
	else if (index == -2)	desftwc1(nf/2, ds->yd, smp);

	/* now we must do some house keeping */
	inherit_from_ds_to_ds(ds, dsi);
	ds->treatement = my_sprintf(ds->treatement,"y multiplied by hanning window");
	/* refisplay the entire plot */
	refresh_plot(pr, UNCHANGED);
	switch_plot(0);
	switch_data_set(0);
	return 0;
}

acreg *sft_menu(void)
{
	static acreg *acj = NULL, *acp = NULL;
	int		rm_poly(char ch);

	if (acp != NULL)	return acp;

	acp = prepare_plot_menu_0();
	clear_menu(acp);


	acj = build_menu("\\stack{{sft test}{plot}}",1);
	add_menu_item(acj,"\\stack{{sft real}{direct}}",do_sft_real,ONE_BOUNCE,1);
	add_menu_item(acj,"\\stack{{spe real}{direct}}",do_sft_real,ONE_BOUNCE,2);

	add_menu_item(acj,"\\stack{{sft real}{inverse}}",do_sft_real,ONE_BOUNCE,-1);	add_menu_item(acj,"window 0",do_window,ONE_BOUNCE,1);
	add_menu_item(acj,"window 1",do_window,ONE_BOUNCE,2);
	add_menu_item(acj,"dewindow 0",do_window,ONE_BOUNCE,-1);
	add_menu_item(acj,"dewindow 1",do_window,ONE_BOUNCE,-2);

	add_menu_item(acj,"merge comp",do_merge_to_comp,ONE_BOUNCE,-2);


	add_menu_item(acj,"sft comp dir.",do_sft_comp,ONE_BOUNCE,1);
	add_menu_item(acj,"spe comp dir.",do_sft_comp,ONE_BOUNCE,2);

	add_menu_item(acj,"sft comp inv.",do_sft_comp,ONE_BOUNCE,-1);
	add_menu_item(acj,"win. comp 0",do_window_comp,ONE_BOUNCE,1);
	add_menu_item(acj,"win. comp 1",do_window_comp,ONE_BOUNCE,2);
	add_menu_item(acj,"dewin. comp 0",do_window_comp,ONE_BOUNCE,-1);
	add_menu_item(acj,"dewin. comp 1",do_window_comp,ONE_BOUNCE,-2);

	add_sub_menu(acp,"sft",acj);


	return (acp);
}

int sft_main(int argc, char **argv)
{
	sft_menu();
	user_plot_menu = sft_menu;

	return 0;
}


# endif

# endif
