#ifndef _SFTL32N_H_
#define _SFTL32N_H_
/*	sftl32n.h  
 *	
 *	
 *
 *	header for sftl32n.c
 *		set of routines for fast Fourier transforms
 *		in 32 bits with the DOS EXTENDER
 */
#ifndef M_PI
#    define M_PI 3.14159265358979323846
#endif

# ifndef _SFTL32N_C_
extern float *sftsin;
extern float *sftcos;
# else
float 	*sftsin = NULL;
float 	*sftcos = NULL;

# endif

int 	sft_init(int npts);
int 	sftmixing(int npts, float *x);
int 	sft_real(int npts, float *x, int df, float *ak);
int 	sft_complex(int npts, float *x, int df, float *ak);

int 	sftwindow(int npts, float *x);
int 	sftwindow1(int npts, float *x, float smp);
int 	desftwindow1(int npts, float *x, float smp);
int 	sftwc(int npts, float *x);
int 	sftwc1(int npts, float *x, float smp);
int 	desftwc(int npts, float *x);
int 	desftwc1(int npts, float *x, float smp);
void    spec_real (int npts, float *x, float *spe);
void 	spec_comp (int npts, float *x, float *spe);
int 	demodulate (int npts, float *x, int freq);
void 	derive_real (int npts, float *x, float *y);
void 	amp(int npts, float *x, float *y);
void 	spec_real_slow (int npts, float *x, float *spe);
# endif

