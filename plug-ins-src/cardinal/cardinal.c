/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _CARDINAL_C_
#define _CARDINAL_C_

# include "xvin.h"

# include "allegro.h"
# include "fftl32n.h"
# include "fillib.h"
# include "log_file.h"
# include "sftl32n.h"

/* If you include other plug-ins header do it here*/
# include "xvplot.h"


/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "cardinal.h"

# include "stdlib.h"
# include "string.h"
# include "ctype.h"

cam_cor cam;

int	refold = -1, last_refold = 0;
float accu = .001;
int	log_cardinal = -1;

int grab_camera_and_spectrum_parameters(cam_cor *caml, int *w_flag, int *low_freq_cutoff, float *high_freq_cutoff_ratio)
{
  int i, frame, cam_fil, win_flag, fbt;
  float factor, fmr;

  if (caml == NULL)  return 1;

    frame = get_config_int("camera", "mode", 0);
    cam_fil = get_config_int("camera", "correction", 1);
    factor = get_config_float("camera", "openning", 1);
    win_flag = get_config_int("spectrum", "windowing", 1);
    fbt = get_config_int("spectrum", "lower_frequency", 10);
    fmr = get_config_float("spectrum", "high_frequency_cutoff_ratio", 1);
    i = win_scanf("Low freaquency cutoff below which spectrum is not considered\n"
		  "(in order to remove drift) fb in fft mode %5d\n"
		  "High frequency cutoff above which spectrum is considered to be\n"
		  "distorted by aliasing and filtering and discard fm (ratio to the\n"
		  "Nyquist frequency) %5f\n"
		  "Apply a hanning window before FFT No %R yes %r\n"
		  "Aplly the camera correction No %R  Yes %r\n"
		  "Camera integration mode Field mode %R Frame mode %r\n"
		  "Ratio of the integration window to the vertical frequency %5f\n"
		  ,&fbt,&fmr,&win_flag,&cam_fil,&frame,&factor);

    if (i == WIN_CANCEL)    return OFF;
    set_config_int("camera", "mode", caml->frame = frame);
    set_config_int("camera", "correction", caml->cam_fil = cam_fil);
    set_config_float("camera", "openning", caml->factor = factor);
    set_config_int("spectrum", "windowing", *w_flag = win_flag);
    set_config_int("spectrum", "lower_frequency", *low_freq_cutoff = fbt);
    set_config_float("spectrum", "high_frequency_cutoff_ratio", *high_freq_cutoff_ratio = fmr);
    return 0;
}
int retrieve_basename_and_index(char *filename, int n_file, char *basename, int n_base)
{
  int k, index;

  if (filename == NULL || basename == NULL)  return -1;
  for (k = 0; k < n_file && filename[k] != '.' && filename[k] != 0 ; k++);
  basename[(k < n_base) ? k : n_base-1] = 0;
  k = (k > 0) ? k-1 : k;
  for ( ; k >= 0 && isdigit(filename[k]); k--);
  if (sscanf(filename+k+1,"%d",&index) != 1) index = -1;
  basename[(k+1 < n_base) ? k+1 : n_base-1] = 0;
  for ( ; k >= 0; k--)
    basename[(k < n_base) ? k : n_base-1] = filename[k];
  return index;
}

int	sft_real_noise_op(pltreg *pr, O_p *op, d_s *dsi, O_p **ops, int start, int end,
	int cam_fil, int frame, float factor, int w_flag, int verbose)
{
	int i, j;
	d_s *ds = NULL;
	int nf, ret = 0;
	float dt, dy, tmp, mean;
 	un_s *un;


	nf = end - start;	/* this is the number of points in the data set */
	if (start > dsi->nx || end > dsi->nx)	return 1;

	un = op->yu[op->c_yu];
	if (un->type != IS_METER || un->decade != IS_NANO)
	{
		ret = 1;
		if (verbose)
			win_printf("warning the y unit is not \\mu m !\n"
			"numerical value will be wrong");
	}
	dy = un->dx;
	un = op->xu[op->c_xu];
	if (un->type != IS_SECOND || un->decade != 0)
	{
		ret = 1;
		if (verbose)
			win_printf("warning the x unit is not seconds !\n"
			"numerical value will be wrong");
	}
/*	dt = un->dx;*/
	dt = un->dx * (dsi->xd[dsi->nx-1] - dsi->xd[0]);
	dt = (dsi->nx > 1) ? dt/(dsi->nx-1) : dt;

	if (*ops == NULL)
	{
		*ops = create_and_attach_one_plot(pr, nf, nf, 0);
		if (*ops == NULL)
		{
			if (verbose) 	win_printf("cannot create plot !");
			return 2;
		}
		ds = (*ops)->dat[0];
		set_plot_x_log(*ops);
		set_plot_y_log(*ops);
		set_plot_y_title(*ops,"<x^2> (nm^2.Hz^{-1})");
		set_plot_x_title(*ops,"frequency (Hz)");
		set_plot_title(*ops, "sft over %d",nf);
		(*ops)->filename = Transfer_filename(op->filename);
	}
	else
	{
		ds = create_and_attach_one_ds(*ops, nf, nf, 0);
		if (ds == NULL)
		{
			if (verbose) 	win_printf("cannot create plot !");
			return 2;
		}
	}



	sft_init(nf);
	for (j = 0; j < nf; j++)       		ds->xd[j] = dsi->yd[start+j];
	if (w_flag)
	{
		sftwindow(nf, ds->xd);
		for (i=0 ,mean = 0; i< nf ; i++)	mean += ds->xd[i];
		for (i=0 ; i< nf ; i++) 			ds->xd[i] = dsi->yd[start+i];
	}
	else
	{
		for (i=0 ,mean = 0; i< nf ; i++)	mean += ds->xd[i];
	}
	mean /= nf;
	for (i=0 ,mean = 0; i< nf ; i++)		ds->xd[i] -= mean;
	if ( w_flag)							sftwindow(nf, ds->xd);
	sft_real(nf, ds->xd, 1, ds->yd);
	spec_real_slow (nf, ds->yd, ds->xd);
	ds->nx = ds->ny = 1 + nf/2;
	for (j = 0; j <= nf/2; j++)
	{
		ds->yd[j] = ds->xd[j];
		ds->xd[j] = j;
	}
	for (i=0 ; i<= nf/2; i++)
	{
		ds->yd[i] *= (w_flag & 1) ? (dy *dy)/3 : (dy * dy)/2;
		ds->yd[i] *=  (dt * nf);
		ds->xd[i] /=  (dt * nf);
	}
	for (i=1 ; (cam_fil != 0) && (i<= nf/2); i++)	/* correct camera */
	{
		tmp = camera_response((float)i/nf, frame, factor);
		tmp += camera_response(1.0 - ((float)i/nf), frame, factor);
		ds->yd[i] /= (tmp != 0) ? tmp : 1;
	}
	for (j = 1; j <= nf/2; j++) /* we suppress the mode 0 */
	{
		ds->yd[j-1] = ds->yd[j];
		ds->xd[j-1] = ds->xd[j];
	}
	ds->nx = ds->ny = nf/2;
	/* now we must do some house keeping */
	inherit_from_ds_to_ds(ds, dsi);
	ds->treatement = my_sprintf(ds->treatement,"sft over %d",nf);

	/* refisplay the entire plot */
	//switch_plot(0);
	//switch_data_set(0);
	return ret;
}


d_s *sft_low_pass_filer_real(O_p *op, d_s *dsi, int filter, int w_flag)
{
	int i, j;
	d_s *ds = NULL;
	int odd, nf;
	float mean, smp = 0.05, tmp;

	nf = dsi->nx;
	odd = nf%2;

	if (op != NULL)	ds = create_and_attach_one_ds(op, nf+odd, nf+odd, 0);
	else 			ds = build_data_set(nf+odd, nf+odd);

	if (ds == NULL)		return NULL;

/*
	ds->nx = ds->ny = nf;
	for (j = 0; j < nf; j++)
	{
		ds->yd[j] = dsi->yd[j];
		ds->xd[j] = dsi->xd[j];
	}
	return ds;
*/
	sft_init(nf);
/*	filter_init(nf); */
	for (j = 0; j < nf; j++)       		ds->xd[j] = dsi->yd[j];
	if (w_flag)
	{
        sftwindow(nf, ds->xd);
		for (i=0 ,mean = 0; i< nf ; i++)	mean += ds->xd[i];
		for (i=0 ; i< nf ; i++) 			ds->xd[i] = dsi->yd[i];
	}
	else
	{
		for (i=0 ,mean = 0; i< nf ; i++)	mean += ds->xd[i];
	}
	mean /= nf;
	for (i=0; i< nf ; i++)					ds->xd[i] -= mean;
	if ( w_flag)							sftwindow1(nf, ds->xd, smp);
	sft_real(nf, ds->xd, 1, ds->yd);

	if (odd)
	{
		for (i=nf; i > 1 ; i--)	 ds->yd[i] = ds->yd[i-1];
		ds->yd[1] = 0;
	}

	for (i=4*filter; i < nf+odd; i++)	 ds->yd[i] = 0;
	ds->yd[1] = 0;

	for (i=1; (i < 2*filter) && (2*i < nf); i++)
	{
		tmp = (1 + cos (i * M_PI_2/filter))/2;

		ds->yd[2*i] *= tmp;
		ds->yd[2*i+1] *= tmp;
	}

/*	lowpass_smooth_half (nf+odd, ds->yd, filter);*/
	if (odd)
	{
		for (i=1; i < nf ; i++)	 ds->yd[i] = ds->yd[i+1];
	}
	sft_real(nf, ds->xd, -1, ds->yd);
	ds->nx = ds->ny = nf;
	if ( w_flag)							desftwindow1(nf, ds->xd, smp);
	for (i=0; i< nf ; i++)
	{
		ds->yd[i] = mean + ds->xd[i];
		ds->xd[i] = dsi->xd[i];
	}

	/* now we must do some house keeping */
	inherit_from_ds_to_ds(ds, dsi);
	ds->treatement = my_sprintf(NULL,"low pass filter at %d over %d",filter,nf);
	return ds;
}



int	do_sft_real_noise(void)
{
  int i, j;
  O_p *op = NULL, *opn = NULL;
  d_s *ds = NULL, *dsi = NULL;
  pltreg *pr = NULL;
  char c[1024] = {0};
  int nf, index           ;
  float dt, dy, tmp, mean;
  static int  frame = 1, cam_fil = 0, w_flag = 1;
  static float factor = 1;

  un_s *un;

  if(updating_menu_state != 0)	return D_O_K;
  if (key[KEY_LSHIFT])
    return win_printf("This routine compute the noise spectrum of a data set using slow fft");
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf("cannot find data");

  nf = dsi->nx;	/* this is the number of points in the data set */
  index = RETRIEVE_MENU_INDEX;

  un = op->yu[op->c_yu];
  if (un->type != IS_METER || un->decade != IS_MICRO)
    win_printf("warning the y unit is not \\mu m !\nnumerical value will be wrong");
  dy = un->dx;
  un = op->xu[op->c_xu];
  if (un->type != IS_SECOND || un->decade != 0)
    win_printf("warning the x unit is not seconds !\nnumerical value will be wrong");
  /*	dt = un->dx;*/
  dt = un->dx * (dsi->xd[dsi->nx-1] - dsi->xd[0]);
  dt = (dsi->nx > 1) ? dt/(dsi->nx-1) : dt;


  frame = get_config_int("camera", "mode", 0);
  cam_fil = get_config_int("camera", "correction", 1);
  factor = get_config_float("camera", "openning", 1);
  w_flag = get_config_int("spectrum", "windowing", 1);


  sprintf(c,"spectrum along signal\n"
	  "having %d points\n"
	  "camera correction No->%%R yes ->%%r \n"
	  "Field integration-> %%R frame int. ->%%r\n"
	  "opening factor %%8f\nwindowing No->%%R yes->%%r\nindex %%8d\n",nf);
  i = win_scanf(c,&cam_fil,&frame,&factor,&w_flag,&index);
  if (i == WIN_CANCEL)	return OFF;


  set_config_int("camera", "mode",  frame);
  set_config_int("camera", "correction",  cam_fil);
  set_config_float("camera", "openning", factor);
  set_config_int("spectrum", "windowing", w_flag);

  if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
    return win_printf("cannot create plot !");
  ds = opn->dat[0];


  if (index > 2)
    {
      for (j = 0; j < nf; j++)
	ds->xd[j] = j;

      sft_init(nf);
      sft_real(nf, dsi->yd, 1, ds->yd);
      /*		sft_real_phase(nf, dsi->yd, ds->yd);*/

    }
  else if (index > 0) /* we do spectrum */
    {
      sft_init(nf);
      for (j = 0; j < nf; j++)       		ds->xd[j] = dsi->yd[j];
      if (w_flag)
	{
	  sftwindow(nf, ds->xd);
	  for (i=0 ,mean = 0; i< nf ; i++)	mean += ds->xd[i];
	  for (i=0 ; i< nf ; i++) 	        ds->xd[i] = dsi->yd[i];
	}
      else
	{
	  for (i=0 ,mean = 0; i< nf ; i++)	mean += ds->xd[i];
	}
      mean /= nf;
      for (i=0; i< nf ; i++)			ds->xd[i] -= mean;
      if ( w_flag)				sftwindow(nf, ds->xd);
      sft_real(nf, ds->xd, 1, ds->yd);
      spec_real_slow (nf, ds->yd, ds->xd);
      ds->nx = ds->ny = 1 + nf/2;
      for (j = 0; j <= nf/2; j++)
	{
	  ds->yd[j] = ds->xd[j];
	  ds->xd[j] = j;
	}
      for (i=0 ; i<= nf/2; i++)
	{
	  ds->yd[i] *= (w_flag & 1) ? (dy *dy)/3 : (dy * dy)/2;
	  ds->yd[i] *=  (dt * nf);
	  ds->xd[i] /=  (dt * nf);
	}
      for (i=1 ; (cam_fil != 0) && (i<= nf/2); i++)	/* correct camera */
	{
	  tmp = camera_response((float)i/nf, frame, factor);
	  tmp += camera_response(1.0 - ((float)i/nf), frame, factor);
	  ds->yd[i] /= (tmp != 0) ? tmp : 1;
	}
      set_plot_x_log(opn);
      set_plot_y_log(opn);
      set_plot_y_title(opn,"<x^2> (\\mu m^2.Hz^{-1})");
      set_plot_x_title(opn,"frequency (Hz)");
    }
  else
    {
      for (j = 0; j < nf; j++)
	ds->xd[j] = j;

      sft_init(nf);
      sft_real(nf, ds->yd, -1, dsi->yd);
    }
  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
  ds->treatement = my_sprintf(ds->treatement,"sft over %d",nf);
  set_plot_title(opn, "sft over %d",nf);
  if (op->x_title != NULL) set_plot_x_title(opn, "%s", op->x_title);
  if (op->y_title != NULL) set_plot_y_title(opn, "%s", op->y_title);
  opn->filename = Transfer_filename(op->filename);
  /* refisplay the entire plot */
  return refresh_plot(pr, UNCHANGED);
}

# ifdef KEEP
d_s	*find_cur_data_set_in_op(O_p *ops)
{
	d_s *ds = NULL;

	if (ops == NULL || ops->n_dat <= 0 || ops->cur_dat >= ops->n_dat)
		return NULL;
	ds = ops->dat[ops->cur_dat];
	return (ds);
}
#endif

int mean_y2(void)
{
    int i, j, k;
	char s[128] = {0}, u[128] = {0}, u1[128] = {0}, u4[128] = {0};
	pltreg *pr = NULL;
	O_p *op = NULL;
	d_s *dsi = NULL;
	int  nx, min, start, end, nl;
	double tmp, dx, y2, mean, y4, r, y2u, y4u, kx, y2bu, kxb;
	int win_flag;
	static int n_bin = 32;
	double y2b, meanb, y4b;

	if(updating_menu_state != 0)	return D_O_K;
	win_flag = RETRIEVE_MENU_INDEX;
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
		return(win_printf("can't find plot"));
	nx = dsi->nx;
	min = (dsi->nx > dsi->ny) ? dsi->nx : dsi->ny;
	for (start = i = min-1; i >= 0 ; i--)
	{
		if (dsi->xd[i] < op->x_hi && dsi->xd[i] >= op->x_lo)
		{
			start = (i < start) ? i : start;
		}
	}
	for ( end = i = 0; i < min ; i++)
	{
		if (dsi->xd[i] < op->x_hi && dsi->xd[i] >= op->x_lo)
		{
			end = (i > end) ? i : end;
		}
	}
	end += 1;
	nx = end - start;
	if (nx == 0)	return win_printf("No points to average !");

	for (i = start, nl = 0, k = 0, y2b = y4b = 0; k < nx/n_bin; i+= n_bin, k++)
	  {
	    for (j = 0, meanb = 0; (j < n_bin) && ((i+j) < end); j++)
	      meanb += dsi->yd[i+j];
	    meanb /= (j > 0) ? j : 1;
	    for (j = 0, y2 = y4 = 0; (j < n_bin) && ((i+j) < end); j++)
	      {
		tmp = dsi->yd[i+j] - meanb;
		y2b += tmp * tmp;
		y2 +=  tmp * tmp;
		y4b += tmp * tmp * tmp * tmp;
	      }
	    //win_printf("i %d j %d meanb = %g y2/n-1 %g",i,j, meanb,y2/(n_bin-1));
	    /*
	    y2 /= (j > 1) ? j-1 : 1;
	    y4 /= (j > 1) ? j-1 : 1;
	    */
	    //y2b += y2;
	    //y4b += y4;
	    nl += (j > 1) ? j-1 : 1;
	  }
	if (nl > 0)
	  {
	    y2b /= nl;
	    y4b /= nl;
	  }
	//win_printf("y2b %g nx %d k %d nl %d",y2b, end-start,k, nl);
	if (win_flag)
	{
		dx = 2*M_PI/nx;
		for (i=start ,mean = 0; i< end ; i++)
			mean += (1-cos(i*dx))*dsi->yd[i];
	}
	else
	{
		for (i=start ,mean = 0; i< end ; i++)		mean += dsi->yd[i];
	}
	mean /= nx;

	if (win_flag)
	{
		dx = 2*M_PI/nx;
		sprintf(s,"Using a Hanning window\n starting at %d ending at %d",start,end);
		for (i=start ,y4 = y2 = 0; i< end ; i++)
		{
			tmp = dsi->yd[i] - mean;
			y2 += (1-cos(i*dx)) * tmp * tmp;
			y4 += (1-cos(i*dx)) * tmp * tmp * tmp * tmp;
		}
	}
	else
	{
		sprintf(s,"Using no window\n starting at %d ending at %d",start,end);
		for (i=start ,y4 = y2 = 0; i< end ; i++)
		{
			tmp = dsi->yd[i] - mean;
			y2 += tmp * tmp;
			y4 +=  tmp * tmp * tmp * tmp;
		}
	}
	y2 /= (nx > 1) ? nx-1 : 1;
	y4 /= (nx > 1) ? nx-1 : 1;
	u[0] = 0;
	if (op->y_unit != NULL)	sprintf(u,"(%s)^2",op->y_unit);
	u4[0] = 0;
	if (op->y_unit != NULL)	sprintf(u4,"(%s)^4",op->y_unit);
	strcpy (u1, (op->y_unit != NULL)?op->y_unit:"pixel");
	y2u = y2*op->dy*op->dy;
	kx = (1.38e-11 * 298)/y2u;
	y2bu = y2b*op->dy*op->dy;
	kxb = (1.38e-11 * 298)/y2bu;
	y4u = y4*op->dy*op->dy*op->dy*op->dy;
	r = (y2 != 0) ? y4/(y2*y2) : 0;
	//win_printf("bef dis");
	i = win_printf("Data set %d with %d points Y component\n"
	"Mean value %g(pixel) %g %s \n"
	"Mean square fluctuations \\sigma^2 = \\Sigma_{i = n0}^{i < n1} (y_i - <y>)^2"
	" \n= %g (pixel^2) = %g %s\n\\sigma  = %g (pixel) = %g %s\n"
	"Stiffnes %g N/m\n"
	"fouth momentum \\gamma^4 = \\Sigma_{i = n0}^{i < n1} (y_i - <y>)^4\n"
	"= %g (pixel^4) = %g %s\n\\delta^4/\\sigma^4 = %g\n%s\n"
	"Computing variance using a small window of %d points leads to:\n"
	"Mean square fluctuations \\sigma_{%d}^2 = <\\Sigma_{i = n0}^{i < n0+%d} (y_i - <y>)^2 >_{n0,n1}\n"
		       "= %g (pixel^2) = %g %s\n"
		       "\\sigma_{%d}  = %g (pixel) = %g %s\n"
		       "Stiffnes k_{%d} %g N/m\n"
		       "To change bin size click on WIN_CANCEL\n"
		       ,pr->one_p->cur_dat,nx,
		       mean,mean*op->dy,u1,y2,y2u,u,sqrt(y2),sqrt(y2u),
		       (op->y_unit != NULL)?op->y_unit:" ",kx,y4, y4u,u4,r,s,
		       n_bin,n_bin,n_bin,y2b,y2bu,u,n_bin,sqrt(y2b),sqrt(y2bu),
		       (op->y_unit != NULL)?op->y_unit:" ",n_bin,kxb);
	if (i == WIN_CANCEL)
	  {
	    i = win_scanf("New bin size for next measure %d",&n_bin);
	  }
	return D_O_K;
}

O_p *x_ou_y_de_t_on_op(O_p *ops, int axis)
{
	int i;
	float ts = 0.04, ftmp;
	d_s *dsi = NULL, *dss = NULL;
	int  nx;
	un_s *un = NULL;
	O_p *op = NULL;

	if (ops == NULL)	return NULL;
	dsi = find_cur_data_set_in_op(ops);
	if (dsi == NULL || dsi->source == NULL)
	{
		win_printf("bad data set");
		return NULL;
	}
	nx = dsi->nx;


	if(strncmp(dsi->source,"Bead tracking at",16) != 0)
	{
		win_printf("bad source");
		return NULL;
	}
	if (sscanf(dsi->source + 16,"%f",&ftmp) != 1)
	{
		win_printf("bad freq ");
		return NULL;
	}

	ts = ftmp;
	ts = (float)1/ts;
	op = create_one_plot( nx, nx, 0);
	if (op == NULL)		return NULL;
	dss = op->dat[0];
	if (dss == NULL)		return NULL;
	inherit_from_ds_to_ds(dss, dsi);
	un = build_unit_set(IS_SECOND, 0, ts, 0, 0, "s");
	if (un == NULL)		return NULL;
	if (axis == X_AXIS)
	{
		for (i=0 ; i< nx ; i++)
		{
			dss->xd[i] = i;
			dss->yd[i] = dsi->xd[i];
		}
		op->title = strdup("x(t)");
		add_data_to_one_plot (op, IS_X_UNIT_SET, (void *)un);
		set_op_x_unit_set(op, op->n_xu-1);
		uns_op_2_op_by_type(ops, IS_X_UNIT_SET, op, IS_Y_UNIT_SET);
		}
	else
	{
		for (i=0 ; i< nx ; i++)
		{
			dss->xd[i] = i;
			dss->yd[i] = dsi->yd[i];
		}
		op->title = strdup("y(t)");
		un = build_unit_set(IS_SECOND, 0, ts, 0, 0, "s");
		if (un == NULL)		return NULL;
		add_data_to_one_plot (op, IS_X_UNIT_SET, (void *)un);
		set_op_x_unit_set(op, op->n_xu-1);
		uns_op_2_op_by_type(ops, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
	}
	op->filename = Transfer_filename(ops->filename);
	op->dir = Mystrdup(ops->dir);
	return op;
}

int y_de_t_et_x_t_new(void)
{
	pltreg *pr = NULL;
	O_p *ops = NULL, *op = NULL;

	if(updating_menu_state != 0)	return D_O_K;
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&ops) != 2)
		return win_printf("cannot find data");
	op = x_ou_y_de_t_on_op(ops, X_AXIS);
	if (op == NULL)		return win_printf("cannot create x(t)");
	add_data_to_pltreg (pr, IS_ONE_PLOT,(void *)op);
	op = x_ou_y_de_t_on_op(ops, Y_AXIS);
	if (op == NULL)		return win_printf("cannot create y(t)");
	add_data_to_pltreg (pr, IS_ONE_PLOT,(void *)op);
	refresh_plot(pr, pr->cur_op);
	return D_O_K;
}

int correct_simple_glitch(void)
{
  int i;
  pltreg *pr = NULL;
  O_p *op = NULL, *opd = NULL;
  d_s *dsi = NULL, *dsd = NULL;
  int  nx, ids, idsc;
  static float st = 2;
  static int mode = 0, all = 0, alter = 0;
  float tmp, bef, aft, st1;

  if(updating_menu_state != 0)	return D_O_K;
  i = ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi);
  if ( i != 3)		return win_printf("cannot find data");

  i = win_scanf("Correct points if they differ more than %10f\n"
		"of the mean value %R or in derivate %r\n"
		"Proceed current data set %R or all %r\n"
		"Put result in a new plot %R or alter present data %r\n"
		,&st,&mode,&all,&alter);
  if (i == WIN_CANCEL)	return OFF;
  st1 = st/op->dy;
  for (ids = 0, idsc = op->cur_dat; ids < op->n_dat; ids++)
    {
      if (all == 0 && ids != idsc) continue;
      {
	dsi = op->dat[ids];
	nx = dsi->nx;
	if (alter == 0)
	  {
	    if (opd == NULL)
	      {
		opd = create_and_attach_one_plot(pr, nx,nx, 0);
		if (opd == NULL)  return win_printf_OK("Cannot allocate plot");
		dsd = opd->dat[0];
		set_plot_title(opd, "\\stack{{simple de-glictched}{%s on %s}}",
			       (op->title == NULL)?"?":op->title,(mode)?"derivate":"mean");
		opd->filename = Transfer_filename(op->filename);
		uns_op_2_op(opd, op);
		opd->dir = Mystrdup(op->dir);
		opd->need_to_refresh = 1;
	      }
	    else
	      {
		dsd = create_and_attach_one_ds(opd, nx,nx, 0);
		if (dsd == NULL)  return win_printf_OK("Cannot allocate plot");
	      }
	    inherit_from_ds_to_ds(dsd, dsi);
	    dsd->treatement = my_sprintf(dsd->treatement,"simple de-glictched on %s",(mode)?"derivate":"mean");
	  }
	else dsd = build_data_set(nx,nx);
	if (dsd == NULL)		return 1;

	for (i = 0, tmp = 0; i < nx; i++)
	  tmp += dsi->yd[i];
	tmp /= nx;

	for (i = 0; i < nx; i++)
	  {
	    if (mode == 0) dsd->xd[i] = (fabs(dsi->yd[i] - tmp) > st1) ? 1 : 0;
	    else
	      {
		bef = (i == 0) ? dsi->yd[0] : dsi->yd[i-1];
		aft = (i < nx-1) ? dsi->yd[i+1] : dsi->yd[nx-1];
		dsd->xd[i] = (fabs(dsi->yd[i] - (bef + aft)/2) > st1) ? 1 : 0 ;
	      }
	    dsd->yd[i] = (dsd->xd[i] == 1) ? tmp : dsi->yd[i];
	  }
	for (i = 0; i < nx; i++)
	  {
	    if (dsd->xd[i] == 1)
	      {
		bef = (i == 0) ? tmp : dsd->yd[i-1];
		aft = (i < nx-1) ? dsd->yd[i+1] : tmp;
		dsd->yd[i] = (bef + aft)/2;
	      }
	    dsd->xd[i] = dsi->xd[i];
	  }
	if (alter != 0)
	  {
	    for (i = 0; i < nx; i++)
	      {
		dsi->xd[i] = dsd->xd[i];
		dsi->yd[i] = dsd->yd[i];
	      }
	    dsi->treatement = my_sprintf(dsi->treatement,"simple de-glictched on %s",(mode)?"derivate":"mean");
	    free_data_set(dsd);
	    dsd = NULL;
	    op->need_to_refresh = 1;
	  }
      }
    }
  refresh_plot(pr, UNCHANGED);
  return 0;
}

float camera_response(float freq_over_fech, int frame, float a)
{
	float x, tmp;

	x = a * freq_over_fech * M_PI;
	tmp = (sin(x) == 0) ? 1 : sin(x)/x;
	x = freq_over_fech * M_PI;
	if (frame)	tmp *= cos(x/2);
	tmp *= tmp;
	return tmp;
}

float compute_r_for_acq_data(int nx, int fb, int fm, float fc, int frame,
	float opening, float *rap)
{
	int i, j, k;
	float tmp, s, p, q;
	int  l_refold;


	if (refold <= 0)
	{
		for ( l_refold = 1, s = 1; s > accu ; l_refold++)
		{
			i = (l_refold * 2 * nx) + nx;
			tmp = 1 + ((float)i*(float)i)/(fc*fc);
			tmp = 1/tmp;
			tmp = tmp * camera_response(l_refold + (float)nx/(2*nx), frame,
				opening);
			s = tmp;
			i = ((l_refold + 1) * 2 * nx) - nx;
			tmp = 1 + ((float)i*(float)i)/(fc*fc);
			tmp = 1/tmp;
			tmp = tmp * camera_response(l_refold + 1.0 - ((float)nx/(2*nx)),
				frame, opening);
			s += tmp;
		}
/*		i = win_printf("fc = %g nx = %d\naccu = %g refold = %d",fc,nx,accu,
						l_refold); */
		last_refold = l_refold;
		l_refold = (l_refold < 2) ? 2 : l_refold;
	}
	l_refold = (refold <= 0) ? l_refold : refold;
	for (j=0, s = p = q = 0; j< l_refold; j++)
	{
		for (k = fb ; k <= fm; k++)
		{
			i = (j * 2 * nx) + k;
			tmp = 1 + ((float)i*(float)i)/(fc*fc);
			tmp = 1/tmp;
			q += (k == fb || k == fm) ? tmp/2 : tmp;
			tmp = tmp * camera_response(j + (float)k/(2*nx), frame, opening);
			tmp = (k == fb || k == fm) ? tmp/2 : tmp;
			s += tmp;
			p += k*tmp;

			i = ((j + 1) * 2 * nx) - k;
			tmp = 1 + ((float)i*(float)i)/(fc*fc);
			tmp = 1/tmp;
			q += (k == fb || k == fm) ? tmp/2 : tmp;
			tmp = tmp * camera_response(j + 1.0 - ((float)k/(2*nx)), frame,
				opening);
			tmp = (k == fb || k == fm) ? tmp/2 : tmp;
			s += tmp;
			p += k*tmp;

		}
	}
	q = fc;
	if (rap != NULL) *rap = (q != 0) ? s/q : 0;
		/* should be M_PI_2 for no correction */
	return (s != 0) ? p/s : -1;
}
float	delta_f_de(float fmin, float fmax, float fc)
{
	float xb, xm, n, d;

	if (fc == 0)	return 0;
	xb = fmin/fc;
	xm = fmax/fc;
	n = log(1+xm*xm) - log(1+xb*xb);
	d = atan(xm) - atan(xb);
	return ((fc/2) * n/d);

}
int invert_delta_f(float fb, float fm, float r, float *fc)
{
	int i;
	float x0, x1, y0, y1, x, yd;

	for (x0 = fb, y0 = delta_f_de(fb, fm, x0), i = 0;( r < y0) && (i < 16); i++)		{
		x0 /= 2;
		y0 = delta_f_de(fb, fm, x0);
	}
	if (i == 16)
	  {
	    my_set_window_title("Pb in invert_delta_f i = 16"); // to do
	    return 1;
	    //return win_printf("Pb in invert_delta_f i = 16"); // to do
	  }
	for (x1 = fm, y1 = delta_f_de(fb, fm, x1), i = 0;( r > y1) && (i < 16); i++)		{
		x1 *= 2;
		y1 = delta_f_de(fb, fm, x1);
	}
	if (i == 16)
	  {
	    my_set_window_title("Pb in invert_delta_f i = 16"); // to do
	    return 2;
	    //return win_printf("Pb in invert_delta_f i = 16");  // to do
	  }
	//	win_printf("x1 %g y1 %g x0 %g y0 %g",x1,y1,x0,y0);
	for (i = 0, x = x0; i < 32; i++)
	{
	  if (y1 - y0 == 0) my_set_window_title("Div by 0 in invert during iter");  // to do win_printf("Div by 0 in invert during iter");  // to do
		else x = x0 + (x1-x0) * (r - y0)/(y1 - y0);
		yd = delta_f_de(fb, fm, x);
		if (yd > r)
		{
			y1 = yd; x1 = x;
		}
		if (yd <= r)
		{
			y0 = yd; x0 = x;
		}
	}
	if (y1 - y0 == 0) win_printf("Div by 0 in invert");
	x = x0 + (x1-x0) * (r - y0)/(y1 - y0);
	*fc = x;
	return 0;
}
int invert_delta_f_for_acq_data(int nx, int fb, int fm, float r, float *fc,
	int frame, float opening, float *dfcdr)
{
  int i;
  int ret = 0;
  float x0, x1, y0, y1, x, yd, accur = .001, dr;


	invert_delta_f((float)fb,  (float)fm, r, &x0);
	y0 = compute_r_for_acq_data( nx, fb, fm, x0, frame,  opening ,NULL);
	x1 = x0 *(r/y0);
	y1 = compute_r_for_acq_data( nx, fb, fm, x1, frame,  opening, NULL);
	//win_printf("iter %d, x0 = %g, x1-x0 = %g\n r = %g, yd = %g",-1,x0,x1-x0,r,y1);
	for (i = 0; ( r > y1) && (i < 16); i++)
	{
		x0 = x1;
		y0 = y1;
		x1 *= 1.2;
		y1 = compute_r_for_acq_data(nx, fb, fm, x1, frame, opening ,NULL);
		//win_printf("iter %d, x1 = %g\n r = %g, y1 = %g",i,x1,r,y1);
	}
	if (i == 16)
	  {
	    my_set_window_title("return after %d iter",i);
	    return 1;
	    //return win_printf("return after %d iter",i);  // to do
	  }
	for (i = 0, yd = r + 10*accur; (i < 32) && (fabs(yd-r) > accur); i++)
	{
		x = x0 + (x1-x0) * (r - y0)/(y1 - y0);
		yd = compute_r_for_acq_data(nx, fb, fm, x, frame, opening, NULL);
		//win_printf("iter %d, x = %g, \n r = %g, yd = %g\ny0 %g y1 %g",i,x,r,yd,y0,y1);
		if (yd > r)
		{
			y1 = yd; x1 = x;
		}
		if (yd <= r)
		{
			y0 = yd; x0 = x;
		}
	}
	if (y1 - y0 == 0)
	  {
	    ret = 2;
	    win_printf("Div by 0 in invert");
	  }
	x = x0 + (x1-x0) * (r - y0)/(y1 - y0);
	yd = compute_r_for_acq_data(nx, fb, fm, x, frame, opening, NULL);
	dr = compute_r_for_acq_data(nx, fb, fm, 1.01*x, frame, opening, NULL) - yd;
	*fc = x;
	*dfcdr = (dr != 0) ? x*.01/dr : 0;
	return ret;
}
int	alternate_lorentian_fit_acq_spe_with_error(float *y, int ny, float dy,
	int fb, int fm, int w_flag, int cam_fil, float factor, int frame, float *fc,
	float *a, float *dfc, float *da, float *chisq, float *dy0)
{
	int i;
	float s, p, r, x, mean, ax, tmp, yt, dfcdr;
	float ds, dp, dr, xm, xp, df, rap; // , rm, rp
	int	 nx2 = 0;
	static float *fy = NULL, chi2;
	static int	nx = 0;

	(void)cam_fil;
	if (fy == NULL || nx != ny)
	{
		fy = (float*)realloc(fy,ny*sizeof(float));
		nx = ny;
		if (fy == NULL)
		  {
		    win_printf("Cannot allocate array");
		    return 1;
		  }
	}
	for (i = 0; i < nx; i++)  fy[i] = y[i]*dy;
	if (fft_init(nx))
	  {
	    win_printf("Cannot init fft");
	    return 2;
	  }
	if (w_flag)
	{
		ax = 2*M_PI/nx;
		for (i=0 ,mean = 0; i< nx ; i++)
			mean += (1-cos(i*ax))*fy[i];
	}
	else
	{
		for (i=0 ,mean = 0; i< nx ; i++)		mean += fy[i];
	}
	mean /= nx;
	for (i=0 ; i< nx ; i++)		fy[i] -= mean;
	if ( w_flag)		fftwindow(nx, fy);
	realtr1(nx, fy);
	fft(nx, fy, 1);
	realtr2(nx, fy, 1);
	spec_real (nx, fy, fy);

	nx2 = nx/2;
	for (i = 1, s =  0; i < fb && i < nx2; i++)
		s += fy[i];
	s = (fb > 1) ? s /(fb-1) : s;
	s /= (w_flag) ? 3 : 2;
	s = (s>= 0)	? sqrt(s) : s;
	if (dy0 != NULL)	*dy0 = s;
	for (i = fb, s = p = ds = dp = 0; i <= fm && i < nx2; i++)
	{
		tmp = (i == fb || i == fm) ? fy[i]/2 : fy[i];
		s += tmp;
		ds += tmp * tmp;
		p += tmp*i;
		dp += tmp*i * tmp*i;
	}
	if (s == 0)    win_printf("Division by zero");
	r = p/s;
	ds = sqrt(ds);
	ds = (s != 0) ? ds/s : ds;
	dp = sqrt(dp);
	dp = (p != 0) ? dp/p : dp;
	dr = ds + dp;
	invert_delta_f_for_acq_data(nx2, fb, fm, r, &x, frame, factor, &dfcdr);
	compute_r_for_acq_data( nx2, fb, fm, x, frame, factor, &rap);
	if (rap == 0)
	  {
	    *a = s;
	    my_set_window_title("rap Division by zero");
	  }
	else *a = s /rap;
	//win_printf("s %g rap %g a %g, fc %g",s,rap,*a, x);
	*a /= (w_flag) ? 3 : 2;
	*fc = x;
	df = dfcdr * r * dr;
	//rp = r * (1 + dr);
	//rm = r * (1 - dr);
	xp = x + df;
	xm = x - df;
	df = (x != 0) ? df/x : 0;
	*dfc = df * *fc;
	compute_r_for_acq_data( nx2, fb, fm, xm, frame, factor, &rap);
	if (rap == 0)
	  {
	    tmp = s*(1+ds);
	    my_set_window_title("rap 2 Division by zero");
	  }
	else tmp = (s*(1+ds)) /rap;
	tmp /= (w_flag) ? 3 : 2;
	*da = fabs(tmp - *a);
	tmp = (s*(1-ds)) /rap;
	tmp /= (w_flag) ? 3 : 2;
	if (fabs(*a - tmp) > *da)	*da = fabs(*a - tmp);
	compute_r_for_acq_data( nx2, fb, fm, xp, frame, factor, &rap);
	if (rap == 0)
	  {
	    tmp = s*(1-ds);
	    my_set_window_title("rap 3 Division by zero");
	  }
	else tmp = (s*(1-ds)) /rap;
	tmp /= (w_flag) ? 3 : 2;
	if (fabs(*a - tmp) > *da)	*da = fabs(*a - tmp);
	tmp = (s*(1+ds)) /rap;
	tmp /= (w_flag) ? 3 : 2;
	if (fabs(*a - tmp) > *da)	*da = fabs(*a - tmp);

	for (i = fb, chi2 = 0; i < fm && i < nx2; i++)
	{
		tmp = *a/(1 + ((float)(i)*(float)(i))/(x*x));
		yt = fy[i] - tmp;
		yt = (tmp != 0) ? yt/tmp : yt;
		yt *= yt;
		chi2 += yt;
	}
	/*	win_printf ("ds = %g, dp = %g, dr = %g\n a = %g da = %g\nfc = %g"
		" dfc = %g\n xm = %g x = %g xp = %g",ds,dp,dr,*a,*da,*fc,*dfc,xm,x,xp);*/
	*chisq = chi2;
	return 0;
}
float *compute_spe_for_acq_data(int nx, float fc, int frame, float opening,
		float *spe)
{
	int i, j, k;
	float tmp,  s;
	int l_refold;

	if (refold <= 0)
	{
		for ( l_refold = 1, s = 1; s > accu ; l_refold++)
		{
			i = (l_refold * 2 * nx) + nx;
			tmp = 1 + ((float)i*(float)i)/(fc*fc);
			tmp = 1/tmp;
			tmp = tmp * camera_response(l_refold + (float)nx/(2*nx), frame,
						opening);
			s = tmp;
			i = ((l_refold + 1) * 2 * nx) - nx;
			tmp = 1 + ((float)i*(float)i)/(fc*fc);
			tmp = 1/tmp;
			tmp = tmp * camera_response(l_refold + 1.0 - ((float)nx/(2*nx)),
						frame, opening);
			s += tmp;
		}
	/*		i = win_printf("fc = %g nx = %d\naccu = %g refold = %d",fc,nx,accu,
			l_refold);*/
		last_refold = l_refold;
		l_refold = (l_refold < 2) ? 2 : l_refold;
	}
	l_refold = (refold <= 0) ? l_refold : refold;


	for (k = 0 ; k <= nx; k++) spe[k] = 0;
	for (j=0; j< l_refold; j++)
	{
		for (k = 0 ; k <= nx; k++)
		{
			i = (j * 2 * nx) + k;
			tmp = 1 + ((float)i*(float)i)/(fc*fc);
			tmp = 1/tmp;
			tmp = tmp * camera_response(j + (float)k/(2*nx), frame, opening);
			spe[k] += tmp;

			i = ((j + 1) * 2 * nx) - k;
			tmp = 1 + ((float)i*(float)i)/(fc*fc);
			tmp = 1/tmp;
			tmp = tmp * camera_response(j + 1.0 - ((float)k/(2*nx)), frame,
						opening);
			spe[k] += tmp;

		}
	}
	return spe;
}
O_p *do_fft_and_derivate_on_op_2(O_p *op, int fb, int fd, int w_flag, float fc,
		int frame, float factor, float *etar, float *detar)
{
	int i;
	float df, x1, tmp, dx1, xy, yy, xt, yt;
	O_p  *opd = NULL;
	d_s *dsi = NULL, *dss = NULL;
	int  nx, xs = 0, n2;
	char c[1024] = {0};
	float dx, mean, su, dt;
	un_s *un = NULL;

	dsi = find_cur_data_set_in_op(op);
	if (dsi == NULL)		win_printf_ptr("bad data set");

	un = op->yu[op->c_yu];
	if (un->type != IS_METER || un->decade != IS_MICRO)
		win_printf("warning the y unit is not \\mu m !\n"
		"numerical value will be wrong");
	un = op->xu[op->c_xu];
	if (un->type != IS_SECOND || un->decade != 0)
		win_printf("warning the x unit is not seconds !\n"
		"numerical value will be wrong");

	//dt = un->dx;

	dt = un->dx * (dsi->xd[dsi->nx-1] - dsi->xd[0]);
	dt = (dsi->nx > 1) ? dt/(dsi->nx-1) : dt;



	xs = 0;
	n2 = nx = dsi->nx;
	if (fft_init(nx))
	{
		for (n2 = 1; n2 < nx; n2 <<= 1);
		n2 >>= 1;
		sprintf(c,"Cannot init fft\n%d not a power of 2!\n"
					" I propose to treat the %%d first point",nx);
		i = win_scanf(c, &n2);
		if (i == WIN_CANCEL)	return OFF;
		nx = n2;
		if (fft_init(nx))
		{
			win_printf("Cannot init fft");
			return NULL;
		}
	}

	if (fd <= 0 || fd > nx/2 || fb < 0 || fb > nx/2 || fb > fd)
	{
		win_printf("Improper frequency setting f_b = %d f_d = %d nx = %d",
				fb,fd,nx/2);
		return NULL;
	}

	opd = create_one_plot( nx, nx, 0);
	dss = opd->dat[0];
	if (dss == NULL)		return NULL;

	if (w_flag & 1)
	{
		dx = 2*M_PI/nx;
		for (i=xs ,mean = 0; i< nx ; i++)
			mean += (1-cos(i*dx))*dsi->yd[i];
	}
	else
	{
		for (i=xs ,mean = 0; i< nx ; i++)		mean += dsi->yd[i];
	}
	mean /= nx;
	for (i=0 ; i< nx ; i++)
	{
		dss->xd[i] = i;
		dss->yd[i] = dsi->yd[i+xs] - mean;
	}
	if (w_flag & 1)		fftwindow(nx, dss->yd);
	realtr1(nx, dss->yd);
	fft(nx, dss->yd, 1);
	realtr2(nx, dss->yd, 1);
	spec_real (nx, dss->yd, dss->yd);

	for (i=0 ; i<= nx/2; i++)
	{
		dss->yd[i] *= (w_flag & 1) ? (op->dy * op->dy)/3 : (op->dy * op->dy)/2;
		dss->yd[i] *= i * 2 * M_PI /(nx*dt);//(nx*op->dx);
		dss->yd[i] *= i * 2 * M_PI /(nx*dt);//(nx*op->dx);
	}

	dss->ny = nx/2;
	dss->nx = nx/2;
	set_plot_y_title(opd,"<v^2> (\\mu m^2.s^{-2}.Hz^{-1})");
	set_plot_x_title(opd,"frequency (Hz)");

	set_plot_x_log(opd);
	set_plot_y_log(opd);
	set_plot_title(opd, "spectre %s", (op->title == NULL)?"?":op->title);

	create_attach_select_x_un_to_op(opd, 0, 0, 1/(nx*dt), 0, 0, "no_name"); // 1/(nx*op->dx)

/*	fc = fc_in_hz * nx * op->dx/(M_PI * 2);*/
	compute_spe_for_acq_data(nx/2, fc, frame, factor, dss->xd);
	for (i=0 ; i<= nx/2; i++)
	{
		dss->xd[i] *= ((float)i/fc);
		dss->xd[i] *= ((float)i/fc);
	}
	for (i=fd, xy = yy = dx1 = x1 = tmp = 0 ; i >= fb ; i--)
	{
		xt = (i == fd || i == fb) ? dss->yd[i]/2 : dss->yd[i];
		x1 += xt;
		dx1 += xt*xt;
		yt = (i == fd || i == fb) ? dss->xd[i]/2 : dss->xd[i];
		tmp += yt;
		xy += xt*yt;
		yy += yt*yt;
	}

	for (dss->xd[0] = 0, i=1 ; i<= nx/2; i++)
	{
		dss->yd[i] /= dss->xd[i];
		dss->xd[i] = i;
	}

	dx1 = sqrt(dx1);
	dx1 = (x1 != 0) ? dx1/x1 : dx1;
	/*	win_printf("simple ratio = %g\n least square = %g",
		4*(1.38e-23 * tmp *298)/(6*M_PI* x1*1e-12* op->dx * nx),
		4*(1.38e-23 * yy *298)/(6*M_PI* xy*1e-12* op->dx * nx)  );*/
	x1 = (tmp != 0) ? x1/tmp : x1;
	su = x1*1e-12;
	df = ((float)1)/(dt * nx); /*op->dx * nx, (fd-fb) /(op->dx * nx);  */
	su = su/df;

	*etar = 4*(1.38e-23 * 298)/(6*M_PI*su);
	*detar = *etar * dx1;
  	sprintf(c,"\\fbox{\\stack{{fb = %g (Hz)}{fd = %g (Hz)}"
				"{v^2 = %g (\\mu m^2.s^{-2}.Hz^{-1})}"
				"{\\eta r = %g poise.m +/- %g %%}{f_b %d f_m %d df %g (Hz)}"
				"{fc = %g (Hz)}{window %d}{frame %d factor %f}}}",
				(float)fb/(nx*op->dx),(float)fd/(nx*op->dx),1e12*su,*etar,
				100*dx1,fb,fd,df,fc*2*M_PI/(nx*op->dx),w_flag,frame,factor);
	push_plot_label(opd, dss->xd[fd], dss->yd[fd], c, USR_COORD);
	return opd;
}
int	draw_delta_de_fc_4_iter_torque(void)
{

	int i;
	d_s *ds = NULL;
	O_p *op = NULL, *opd = NULL;
	pltreg *pr = NULL;
	int flow, index;
	float fc, a, su, kx, etar, etar2, detar2, dy, dt, da, dfc, chi2, dy0, mean;
	static int	nx = 1024, w_flag = 0, xv_sav = 0, next = 1;
	static int fm = 1024, fb = 10, cam_fil = 1, frame = 0;
	static float factor = 1;
	//	static char *log = NULL;
	int file_index = -1, n_warn = 0;
	static int auto_file_index = 0;
	char *filename = NULL, *ctmp = NULL, buf[2048], warning[1024], file[256], zmags[128];
	un_s *un = NULL;
	float zmag, rot;
	int nxo, nyo, nx_2;

	if(updating_menu_state != 0)	return D_O_K;
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
		return win_printf_OK("cannot find data");
	nxo = nx = ds->nx;
	nyo = ds->ny;
	warning[0] = 0;
	for (nx_2 = 2; nx_2 <= nx; nx_2 *= 2);
	nx_2 /= 2;
	if (nx_2 != nx)
	  {
	    //	    win_printf("Your data set size %d is not a power of 2!\n"
	    //         "I am going to analyse only the %d first points",nx,nx_2);
	    sprintf(warning+strlen(warning),
		    "{\\color{Yellow}warning:Your data set size %d is not a power of 2!}\n"
                       "{\\color{Yellow}I am going to analyse only the %d first points}\n",nx,nx_2);
	    n_warn++;
	    nx = ds->nx = ds->ny = nx_2;
	  }

	fm = nx/2;
	index = RETRIEVE_MENU_INDEX;


	frame = get_config_int("camera", "mode", 0);
	cam_fil = get_config_int("camera", "correction", 1);
	factor = get_config_float("camera", "openning", 1);
	w_flag = get_config_int("spectrum", "windowing", 1);
	fb = get_config_int("spectrum", "lower_frequency", 10);

	un = op->yu[op->c_yu];
	dy = un->dx;
	un = op->xu[op->c_xu];
	if (un->type != IS_SECOND || un->decade != 0)
	  {
	    sprintf(warning+strlen(warning),"{\\color{Yellow}warning the x unit is not seconds !\n"
		    "numerical value will be wrong}\n");
	    n_warn++;
	  }
	//		win_printf("warning the x unit is not seconds !\n"
	//"numerical value will be wrong");
	dt = un->dx * (ds->xd[ds->nx-1] - ds->xd[0]);
	dt = (ds->nx > 1) ? dt/(ds->nx-1) : dt;

	sprintf(buf,"%s\n\n\\frac{f_c [log(1+x^2)]_{fb/fc}^{fm/fc}}"
		"{2 [arctang(x)]_{fb/fc}^{fm/fc}}\n\nData set %d/%d fb %%6d fm %%6d nx %%6d\n"
		"windowing  no%%R yes%%r\ncamera correction no%%R yes%%r\n"
		"Camera integration mode: field ->%%R frame ->%%r\nopening factor %%6f\n"
		,warning,op->cur_dat,op->n_dat);
	i = win_scanf(buf,&fb,&fm,&nx,&w_flag,&cam_fil, &frame,&factor);
	if (i == WIN_CANCEL)
	  {
	    ds->nx = nxo; ds->ny = nyo;
	    return 0;
	  }

	for (i = 0, mean = 0; i < ds->nx; i++)
	  mean += ds->yd[i];
	mean /= ds->nx;
	mean *= dy;
	set_config_int("camera", "mode", frame);
	set_config_int("camera", "correction", cam_fil);
	set_config_float("camera", "openning", factor);
	set_config_int("spectrum", "windowing", w_flag);
	set_config_int("spectrum", "lower_frequency", fb);
	//	write_config(config);


	alternate_lorentian_fit_acq_spe_with_error(ds->yd, ds->ny, dy, fb, fm,
		w_flag, cam_fil, factor, frame, &fc, &a, &dfc, &da, &chi2, &dy0);
	su = a * M_PI_2;
	kx = (1.38e-2 * 298)/su;
	etar = kx*nx*dt/(2*M_PI*M_PI*fc);
	flow = (int)(2*fc);
	flow = (flow > nx/4) ? nx/4 : flow;
	flow = (flow < 2*fb) ? 2*fb : flow;
	opd = do_fft_and_derivate_on_op_2(op, flow, fm, w_flag+(cam_fil<<1), fc,
		frame, factor, &etar2, &detar2);
	free_one_plot(opd);
	detar2 = (etar2 != 0) ? detar2/etar2 : detar2;

	if ((current_write_open_xvfile == NULL) ||
	    (extract_file_name(file, 256, current_write_open_xvfile) == NULL))
	      sprintf(file,"a new xvplot file");

	zmag = -1;
	rot = -10000;
	zmags[0] = 0;
	if (ds->source != NULL)
	  {
	    ctmp = strstr(ds->source,"Zmag =");
	    if (ctmp != NULL)
		sscanf(ctmp,"Zmag = %g Rot %g",&zmag,&rot);
	    else
	      {
		ctmp = strstr(ds->source,"Z magnets ");
		if (ctmp != NULL)
		    sscanf(ctmp,"Z magnets %g",&zmag);
		ctmp = strstr(ds->source,"Rotation ");
		if (ctmp != NULL)
		  sscanf(ctmp,"Rotation %g",&rot);
	      }
	    sprintf(zmags,"Zmag = %g Rot = %g\n",zmag, rot);
	  }
	filename = op->filename;
	for (i = 0; filename[i] && filename[i] != '.'; i++);
	sscanf(filename + (((i - 3) >= 0) ? i - 3 : 0),"%d",&file_index);


	sprintf(buf,"analysing angular fluctuations of:\n%s%s\n data set %d/%d with nx %d fb %d fm %d %swindowing \n"
		"%s camera correction %s factor %f\n%ssu  %g  rad^2 kx = %g pN.nm \n"
		"a = %g +/- %g %%\nfc %g (raw) +/- %g %%%% %g (Hz) \\eta r = %g\n"
		" \\eta_1 = %g +/- %g %% fb = %d \\chi^2 = %g n = %d\n"
		"mean %g error on mean %g \\mu m\n"
		"do you want to dump these result in %s No->%%R Yes->%%r\n"
		"treat next data set%%R stop%%r previous%%r"
	,backslash_to_slash(op->dir),backslash_to_slash(op->filename),op->cur_dat,op->n_dat,nx,fb,fm,
	(w_flag)?" ":"no ",(cam_fil)? " ":"no ",(frame)?"frame int":"field int",
	factor,zmags,su,kx,a,(a!=0)?100*da/a:da,fc,(fc!=0)?100*dfc/fc:dfc,fc/(nx*dt),
	etar, etar2,100*detar2,flow,chi2,fm-fb-2,mean,dy0,file);
	i = win_scanf(buf,&xv_sav,&next);
	if (xv_sav)
	{
		if (log_cardinal == -1 || index == 1)
		{
			if (do_create_xvplot_file(NULL) == NULL)
			  {
			    ds->nx = nxo; ds->ny = nyo;
			    return 0;
			  }
			log_cardinal++;
			auto_file_index = 0;
			dump_to_current_xvplot_file(
			"%%index\t{\\delta}x^2\td{\\delta}x^2\tk_x\tdk_x\tf_c(Hz)\tdf_c/f_c"
			"\t{\\eta}r\t{\\eta}r2\td{\\eta}r2\td<x>\t<x>\tZmag\tRot");
		}
		if (file_index < 0)    file_index = auto_file_index++;
		dump_to_current_xvplot_file( "%d\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g"
		"\t%g\t%g\t%g\t%g\t%g",file_index,su,(a!=0)?su*da/a:su,kx,(a!=0)?kx*da/a:kx,
		fc/(nx*dt),(fc!=0)?100*dfc/fc:dfc,etar,etar2,etar2*detar2,dy0,mean,zmag,rot);
	}
	ds->nx = nxo; ds->ny = nyo;
	if (next == 0)
	  {
	    op->cur_dat++;
	    if (op->cur_dat >= op->n_dat) op->cur_dat = 0;
	    return 1;
	  }
	if (next == 2)
	  {
	    op->cur_dat--;
	    if (op->cur_dat < 0) op->n_dat =  op->cur_dat;
	    return 1;
	  }
	return 0;
}

int	draw_delta_de_fc_4_iter(void)
{

	int i;
	d_s *ds = NULL;
	O_p *op = NULL, *opd = NULL;
	pltreg *pr = NULL;
	int flow, index;
	float fc, a, su, kx, etar, etar2, detar2, dy, dt, da, dfc, chi2, dy0, mean;
	static int	nx = 1024, w_flag = 0, xv_sav = 0, next = 1;
	static int fm = 1024, fb = 10, cam_fil = 1, frame = 0;
	static float factor = 1;
	//	static char *log = NULL;
	int file_index = -1, n_warn = 0;
	static int auto_file_index = 0;
	char *filename = NULL, *ctmp = NULL, buf[2048], warning[1024], file[256], zmags[128];
	un_s *un = NULL;
	float zmag, rot;
	int nxo, nyo, nx_2;

	if(updating_menu_state != 0)	return D_O_K;
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
		return win_printf_OK("cannot find data");
	nxo = nx = ds->nx;
	nyo = ds->ny;
	warning[0] = 0;
	for (nx_2 = 2; nx_2 <= nx; nx_2 *= 2);
	nx_2 /= 2;
	if (nx_2 != nx)
	  {
	    //	    win_printf("Your data set size %d is not a power of 2!\n"
	    //         "I am going to analyse only the %d first points",nx,nx_2);
	    sprintf(warning+strlen(warning),
		    "{\\color{Yellow}warning:Your data set size %d is not a power of 2!}\n"
                       "{\\color{Yellow}I am going to analyse only the %d first points}\n",nx,nx_2);
	    n_warn++;
	    nx = ds->nx = ds->ny = nx_2;
	  }

	fm = nx/2;
	index = RETRIEVE_MENU_INDEX;


	frame = get_config_int("camera", "mode", 0);
	cam_fil = get_config_int("camera", "correction", 1);
	factor = get_config_float("camera", "openning", 1);
	w_flag = get_config_int("spectrum", "windowing", 1);
	fb = get_config_int("spectrum", "lower_frequency", 10);

	un = op->yu[op->c_yu];
	if (un->type != IS_METER || un->decade != IS_MICRO)
	  {
	    sprintf(warning+strlen(warning),"{\\color{Yellow}warning the y unit is not \\mu m !\n"
		    "numerical value will be wrong}\n");
	    n_warn++;
	//win_printf("warning the y unit is not \\mu m !\n"
	//"numerical value will be wrong");
	  }
	dy = un->dx;
	un = op->xu[op->c_xu];
	if (un->type != IS_SECOND || un->decade != 0)
	  {
	    sprintf(warning+strlen(warning),"{\\color{Yellow}warning the x unit is not seconds !\n"
		    "numerical value will be wrong}\n");
	    n_warn++;
	  }
	//		win_printf("warning the x unit is not seconds !\n"
	//"numerical value will be wrong");
	dt = un->dx * (ds->xd[ds->nx-1] - ds->xd[0]);
	dt = (ds->nx > 1) ? dt/(ds->nx-1) : dt;

	sprintf(buf,"%s\n\n\\frac{f_c [log(1+x^2)]_{fb/fc}^{fm/fc}}"
		"{2 [arctang(x)]_{fb/fc}^{fm/fc}}\n\nData set %d/%d fb %%6d fm %%6d nx %%6d\n"
		"windowing  no%%R yes%%r\ncamera correction no%%R yes%%r\n"
		"Camera integration mode: field ->%%R frame ->%%r\nopening factor %%6f\n"
		,warning,op->cur_dat,op->n_dat);
	i = win_scanf(buf,&fb,&fm,&nx,&w_flag,&cam_fil, &frame,&factor);
	if (i == WIN_CANCEL)
	  {
	    ds->nx = nxo; ds->ny = nyo;
	    return 0;
	  }

	for (i = 0, mean = 0; i < ds->nx; i++)
	  mean += ds->yd[i];
	mean /= ds->nx;
	mean *= dy;
	set_config_int("camera", "mode", frame);
	set_config_int("camera", "correction", cam_fil);
	set_config_float("camera", "openning", factor);
	set_config_int("spectrum", "windowing", w_flag);
	set_config_int("spectrum", "lower_frequency", fb);
	//	write_config(config);


	alternate_lorentian_fit_acq_spe_with_error(ds->yd, ds->ny, dy, fb, fm,
		w_flag, cam_fil, factor, frame, &fc, &a, &dfc, &da, &chi2, &dy0);
	su = a * M_PI_2;
	kx = (1.38e-11 * 298)/su;
	etar = kx*nx*dt/(12*M_PI*M_PI*fc);
	flow = (int)(2*fc);
	flow = (flow > nx/4) ? nx/4 : flow;
	flow = (flow < 2*fb) ? 2*fb : flow;
	opd = do_fft_and_derivate_on_op_2(op, flow, fm, w_flag+(cam_fil<<1), fc,
		frame, factor, &etar2, &detar2);
	free_one_plot(opd);
	detar2 = (etar2 != 0) ? detar2/etar2 : detar2;

	if ((current_write_open_xvfile == NULL) ||
	    (extract_file_name(file, 256, current_write_open_xvfile) == NULL))
	      sprintf(file,"a new xvplot file");

	zmag = -1;
	rot = -10000;
	zmags[0] = 0;
	if (ds->source != NULL)
	  {
	    ctmp = strstr(ds->source,"Zmag =");
	    if (ctmp != NULL)
		sscanf(ctmp,"Zmag = %g Rot %g",&zmag,&rot);
	    else
	      {
		ctmp = strstr(ds->source,"Z magnets ");
		if (ctmp != NULL)
		    sscanf(ctmp,"Z magnets %g",&zmag);
		ctmp = strstr(ds->source,"Rotation ");
		if (ctmp != NULL)
		  sscanf(ctmp,"Rotation %g",&rot);
	      }
	    sprintf(zmags,"Zmag = %g Rot = %g\n",zmag, rot);
	  }
	if (op->filename != NULL)
	  {
	    filename = op->filename;
	    for (i = 0; filename[i] && filename[i] != '.'; i++);
	    sscanf(filename + (((i - 3) >= 0) ? i - 3 : 0),"%d",&file_index);
	  }



	sprintf(buf,"analysing %s%s\n data set %d/%d with nx %d fb %d fm %d %swindowing \n"
		"%s camera correction %s factor %f\n%ssu  %g \\mu m^2 kx = %g N/m \n"
		"a = %g +/- %g %%\nfc %g (raw) +/- %g %%%% %g (Hz) \\eta r = %g\n"
		" \\eta_1 = %g +/- %g %% fb = %d \\chi^2 = %g n = %d\n"
		"mean %g error on mean %g \\mu m\n"
		"do you want to dump these result in %s No->%%R Yes->%%r\n"
		"treat next data set%%R stop%%r previous%%r"
	,backslash_to_slash(op->dir),backslash_to_slash(op->filename),op->cur_dat,op->n_dat,nx,fb,fm,
	(w_flag)?" ":"no ",(cam_fil)? " ":"no ",(frame)?"frame int":"field int",
	factor,zmags,su,kx,a,(a!=0)?100*da/a:da,fc,(fc!=0)?100*dfc/fc:dfc,fc/(nx*dt),
	etar, etar2,100*detar2,flow,chi2,fm-fb-2,mean,dy0,file);
	i = win_scanf(buf,&xv_sav,&next);
	if (xv_sav)
	{
		if (log_cardinal == -1 || index == 1)
		{
			if (do_create_xvplot_file(NULL) == NULL)
			  {
			    ds->nx = nxo; ds->ny = nyo;
			    return 0;
			  }
			log_cardinal++;
			auto_file_index = 0;
			dump_to_current_xvplot_file(
			"%%index\t{\\delta}x^2\td{\\delta}x^2\tk_x\tdk_x\tf_c(Hz)\tdf_c/f_c"
			"\t{\\eta}r\t{\\eta}r2\td{\\eta}r2\td<x>\t<x>\tZmag\tRot");
		}
		if (file_index < 0)    file_index = auto_file_index++;
		dump_to_current_xvplot_file( "%d\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g"
		"\t%g\t%g\t%g\t%g\t%g",file_index,su,(a!=0)?su*da/a:su,kx,(a!=0)?kx*da/a:kx,
		fc/(nx*dt),(fc!=0)?100*dfc/fc:dfc,etar,etar2,etar2*detar2,dy0,mean,zmag,rot);
	}
	ds->nx = nxo; ds->ny = nyo;
	if (next == 0)
	  {
	    op->cur_dat++;
	    if (op->cur_dat >= op->n_dat) op->cur_dat = 0;
	    return 1;
	  }
	if (next == 2)
	  {
	    op->cur_dat--;
	    if (op->cur_dat < 0) op->n_dat =  op->cur_dat;
	    return 1;
	  }
	return 0;
}

int	draw_delta_de_fc_4(void)
{

  while (draw_delta_de_fc_4_iter() == 1);
  return D_O_K;
}
int	draw_delta_de_fc_4_torque(void)
{

  while (draw_delta_de_fc_4_iter_torque() == 1);
  return D_O_K;
}







int	delta_de_fc_4_auto(O_p *op, d_s *ds,
			   int fb,             // low frequency cutoff
			   float fmr,          // high frequency ratio cutoff (normally 0.5)
			   int w_flag,         // hanning window on spectrum
			   int cam_fil,        // if set correct for camera filtering
			   int frame,          // 0 field mode, 1 frame mode for camera
			   float factor,       // integration ratio for the camera
			   float b_radius,     // expected bead radius in microns
			   int *nxeff,         // the nb of points treated
			   float *x2,          // square of fluctuations
			   float *dx2,         // variance of square of fluctuations
			   float *k_x,          // stiffness
			   float *f_c,          // cutoff frequency
			   float *df_c,         // error on cutoff frequency
			   float *eta_r,        // viscous drag \eta * r
			   float *eta_r2,       // viscous drag \eta * r by derivative
			   float *deta_r2,      // viscous drag \eta * r by derivative
			   float *dmean        // error on mean value
			   )
{

	int i;
	O_p  *opd;
	float fc = 0, a = 0, su = 0, kx = 0, etar = 0, etar2 = 0, detar2 = 0, dy = 0;
	float dt = 0, da = 0, dfc = 0, chi2, dy0 = 0, mean = 0;
	int nx = 1024, fm = 1024,  nxo, nyo, nx_2, flow, ret = 0, dsi, n_dat_save = 0;
	un_s *un;
	char k_l[32] = {0}, kxs[1024] = {0}, fcs[1024] = {0}, etas[1024] = {0};

	if (op == NULL || ds == NULL) return 1;

	n_dat_save = op->n_dat;
	for (dsi = 0; dsi < op->n_dat; dsi++)
	    if (op->dat[dsi] == ds)  break;

	if (dsi == 0)      strncpy(k_l,"k_x",32);
	else if (dsi == 1) strncpy(k_l,"k_y",32);
	else if (dsi == 2) strncpy(k_l,"k_z",32);
	else  strncpy(k_l,"k_?",32);
	if (dsi < op->n_dat)     switch_plot_data_set(op, dsi);

	nxo = nx = ds->nx;
	nyo = ds->ny;
	for (nx_2 = 2; nx_2 <= nx; nx_2 *= 2);
	nx_2 /= 2;
	if (nx_2 != nx)	     nx = ds->nx = ds->ny = nx_2;
	fm = (int)(fmr*nx);

	un = op->yu[op->c_yu];
	if (un->type != IS_METER || un->decade != IS_MICRO) 	    ret |= 2;
	dy = un->dx;
	un = op->xu[op->c_xu];
	if (un->type != IS_SECOND || un->decade != 0)               ret |= 4;
	dt = un->dx * (ds->xd[ds->nx-1] - ds->xd[0]);
	dt = (ds->nx > 1) ? dt/(ds->nx-1) : dt;

	for (i = 0, mean = 0; i < ds->nx; i++)	  mean += ds->yd[i];
	mean /= ds->nx;
	mean *= dy;
	ret = alternate_lorentian_fit_acq_spe_with_error(ds->yd, ds->ny, dy, fb, fm,
		w_flag, cam_fil, factor, frame, &fc, &a, &dfc, &da, &chi2, &dy0);
	su = a * M_PI_2;
	kx = (1.38e-11 * 298)/su;
	etar = kx*nx*dt/(12*M_PI*M_PI*fc);
	flow = (int)(2*fc);
	flow = (flow > nx/4) ? nx/4 : flow;
	flow = (flow < 2*fb) ? 2*fb : flow;
	opd = do_fft_and_derivate_on_op_2(op, flow, fm, w_flag+(cam_fil<<1), fc,
		frame, factor, &etar2, &detar2);
	for (i = ds->n_lab-1; i >= 0; i--)
	  remove_plot_label_from_ds(ds, ds->lab[i]);


	if (da > a/4)	snprintf(kxs,sizeof(kxs),"{Inaccurate measure of %s = %5.3g N/m (+/- %2.0f %%)}"
				 "{Increase integration time !}",k_l,kx,(a!=0)?100*da/a:da);
	else		snprintf(kxs,sizeof(kxs),"{\\color{lightgreen} %s = %5.3g N/m (+/- %2.0f %%)}",k_l,
				 kx,(a!=0)?100*da/a:da);
	if (fc > nx/2)	snprintf(fcs,sizeof(fcs),"{Cut-off frequency overrrun ! f_c = %5.1f Hz}",fc/(nx*dt));
	else if (fc > nx/4)	snprintf(fcs,sizeof(fcs),"{Cut-off frequency high ! f_c = %5.1f Hz}",fc/(nx*dt));
	else                    snprintf(fcs,sizeof(fcs),"{\\color{lightgreen} f_c = %5.1f Hz}",fc/(nx*dt));
	if ((1e9*etar > 10 * b_radius) || (1e9*etar2 > 10 * b_radius) || (1e9*etar <  b_radius/2) || (1e9*etar2 <  b_radius/2))
	  snprintf(etas,sizeof(etas),"{Anomallous bead radius \\eta r = %5.2f \\mu m and %5.2f}",1e9*etar,1e9*etar2);
	else  snprintf(etas,sizeof(etas),"{\\color{lightgreen} Bead radius \\eta r = %5.2f \\mu m and %5.2f}",1e9*etar,1e9*etar2);


	if (ret == 0)
	  {
		set_ds_plot_label(ds, (ds->xd[0]+ds->xd[nx-1])/2, (ds->yd[0]+ds->yd[nx-1])/2,
				  USR_COORD, "\\pt7\\fbox{\\stack{%s%s%s}}",kxs,fcs,etas);
	  }
	else
	  {
	    set_ds_plot_label(ds, (ds->xd[0]+ds->xd[nx-1])/2, (ds->yd[0]+ds->yd[nx-1])/2,
			      USR_COORD, "\\pt7\\fbox{\\stack{{Treatment error !}%s%s%s}}",kxs,fcs,etas);
	  }
	free_one_plot(opd);

	if (nxeff != NULL)      *nxeff = nx;
	if (x2 != NULL)         *x2 = su;
	if (dx2 != NULL)        *dx2 = da * M_PI_2;
	if (k_x != NULL)        *k_x = kx;
	if (f_c != NULL)        *f_c = fc;
	if (df_c != NULL)       *df_c = dfc;
	if (eta_r != NULL)      *eta_r = etar;
	if (eta_r2 != NULL)     *eta_r2 = etar2;
	if (deta_r2 != NULL)    *deta_r2 = detar2;
	if (dmean != NULL)      *dmean = dy0;
	ds->nx = nxo; ds->ny = nyo;
	switch_plot_data_set(op, n_dat_save);
	return ret;
}






int	mean_y2bin_slidding_average_in_microns(O_p *op,      // plot of ds
					       d_s *ds,      // dataset to treat
					       int bin,      // # of points from dataset to be binnned before treatment
					       int n_avg,    // # of binned points used for the slidding mean
					       float *meany, // pointer to mean value
					       float *my2,   // pointer to varaiance
					       float *my4)   // pointer to fouth momentum
{
  int k, i, j;
  int ret = 0, n_s, nl, np;
  un_s *un = NULL;
  float dy, my, meanb, y2b, y4b, tmp;

  if (op == NULL || ds == NULL) return 1;

  un = op->yu[op->c_yu];
  if (un->type != IS_METER || un->decade != IS_MICRO) 	    ret |= 2;
  dy = un->dx;

  //n_s = n_avg * bin;
  n_s = n_avg;
  for (i = 0, nl = np = 0, k = 0, my = y2b = y4b = 0; k < ds->nx/n_s; i+= n_s, k++)
    {
      for (j = 0, meanb = 0; (j < n_s) && ((i+j) < ds->nx); j++)
	{
	  meanb += ds->yd[i+j];
	  my += ds->yd[i+j];
	  np++;
	}
      meanb /= (j > 0) ? j : 1;
      for (j = 0, tmp = 0; (j < n_s) && ((i+j) < ds->nx); j++)
	{
	  tmp += ds->yd[i+j] - meanb;
	  if (j%bin == (bin - 1))
	    {
	      tmp /= bin;
	      y2b += tmp * tmp;
	      y4b += tmp * tmp * tmp * tmp;
	      tmp = 0;
	    }
	}
      nl += ((j/bin) > 1) ? (j/bin)-1 : 1;
    }
  if (nl > 0)
    {
      y2b /= nl;
      y4b /= nl;
    }
  if (np > 0) my /= np;

  my *= dy;
  y2b *= dy*dy;
  y4b *= dy*dy*dy*dy;
  if (meany != NULL)      *meany = my;
  if (my2 != NULL)        *my2 = y2b;
  if (my4 != NULL)        *my4 = y4b;
  return ret;
}




int	mean_y2_in_microns(O_p *op, d_s *ds, int win_flag, float *meany, float *my2, float *my4)
{
  int ret = 0;
  un_s *un = NULL;
  float dy, my, y2, y4;

  if (op == NULL || ds == NULL) return 1;

  un = op->yu[op->c_yu];
  if (un->type != IS_METER || un->decade != IS_MICRO) 	    ret |= 2;
  dy = un->dx;
  mean_y2_on_array(ds->yd, ds->nx, win_flag, &my, &y2, &y4);
  my *= dy;
  y2 *= dy*dy;
  y4 *= dy*dy*dy*dy;
  if (meany != NULL)         *meany = my;
  if (my2 != NULL)        *my2 = y2;
  if (my4 != NULL)        *my4 = y4;
  return ret;
}

int do_lin_log_spectrum_avg(void)
{
	int i, j, k;
	int nsp;
	pltreg *pr = NULL;
	static int n_avg = 16;
	float tmp, dx;
	static float reg = .95;//, reg2;
	O_p *op = NULL, *opd = NULL;
	d_s *dsi = NULL, *dsd = NULL;
	int  nx;
	float  mean, meanx;

	if(updating_menu_state != 0)	return D_O_K;
	i = ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi);
	if ( i != 3)		return win_printf("cannot find data");

	nx = dsi->nx;


	i = win_scanf("This routine average between {\\it reg} and 1\n"
		"regression factor%f\n"
		"when the number of points is larger than  %d",&reg,&n_avg);
	if (i == WIN_CANCEL)	return OFF;
	if (reg >= 1 || reg <= 0)
		return win_printf_OK("regression must be < 1 and >0");

	reg = 1/reg;
	//reg2 = sqrt(reg);
	for (tmp = 0, nsp = 0; (int)tmp < nx; nsp++)
	{
		dx = tmp * (reg - 1);
		tmp += ((int)dx < n_avg) ? n_avg : dx;
	}

	opd = create_and_attach_one_plot(pr, nsp,nsp, 0);
	if (opd == NULL)		return win_printf_OK("Cannot allocate plot");
	dsd = opd->dat[0];
	if (dsd == NULL)		return 1;
	if ((alloc_data_set_y_error(dsd) == NULL) || (dsd->ye == NULL))
		return win_printf_OK("I can't create errors 2!");

	for (tmp = 0, k = 0; (int)tmp < nx; k++)
	{
		dx = tmp * (reg - 1);
		if ((int)dx < n_avg)
		{
			for (mean = meanx = 0, i= (int)tmp, j = 0; j < n_avg && i < nx; i++, j++)
			{
				mean += dsi->yd[i];
				meanx += dsi->xd[i];
			}
			if (j != 0) mean /= j;
			dsd->yd[k] = mean;
			dsd->xd[k] = meanx/j;
			for (meanx = 0, i= (int)tmp, j = 0; j < n_avg && i < nx; i++, j++)
			{
				meanx += (dsi->yd[i] - mean)*(dsi->yd[i] - mean);
			}
			i = j;
			i = (i > 1) ? i - 1 : i;
			meanx = (i != 0 && j != 0) ? meanx/(i*j) : meanx;
			meanx = sqrt(meanx);
			dsd->ye[k] = meanx;
			tmp += n_avg;
		}
		else
		{
			for (mean = meanx = 0, i= (int)tmp, j = 0; j < (int)dx && i < nx; i++, j++)
			{
				mean += dsi->yd[i];
				meanx += dsi->xd[i];
			}
			if (j != 0) mean /= j;
			dsd->yd[k] = mean;
			dsd->xd[k] = meanx/j;
			for (meanx = 0, i= (int)tmp, j = 0; j < (int)dx && i < nx; i++, j++)
			{
				meanx += (dsi->yd[i] - mean)*(dsi->yd[i] - mean);
			}

			i = j;
			i = (i > 1) ? i - 1 : i;
			meanx = (i != 0 && j != 0) ? meanx/(i*j) : meanx;
			meanx = sqrt(meanx);
			dsd->ye[k] = meanx;
			tmp *= reg;
		}
	}
	reg = 1/reg;
	set_plot_title(opd, "lin_Log Averaged Spectrum");
	dsd->treatement = my_sprintf(dsd->treatement,"lin-log average fft");
	opd->iopt = op->iopt;
	opd->filename = Transfer_filename(op->filename);
	uns_op_2_op(opd, op);
	inherit_from_ds_to_ds(dsd, dsi);
	refresh_plot(pr, UNCHANGED);
	return 0;
}


d_s *sliding_spectrum(d_s *dsi, d_s *dsd, int nsp, int nshift, int cam_fil, int frame, int factor, int w_flag, int rm_dc, float dy, float dt)
{
  float *sp = NULL;
  int i = 0;
  int ns = 0;
  int nsi = 0;
  float mean = 0;
  float tmp = 0;
  int nx = dsi->nx;

  if (fft_init(nsp))
  		return win_printf("Cannot init fft\n%d not a power of 2!",nsp);
  sp = (float*)calloc(nsp,sizeof(float));
  if (sp == NULL) 		return NULL;
  dsd = build_adjust_data_set(dsd,nsp/2+1,nsp/2+1);
  sp = (float*)calloc(nsp,sizeof(float));
  if (sp == NULL) 		return NULL;
  if (dsd == NULL)		return NULL;
  for (i=0 ; i<= nsp/2 ; i++) 	dsd->yd[i] = 0;
  for (i=0 ; i<= nsp/2 ; i++) 	dsd->xd[i] = i;
  for (ns = nsi = 0; (ns + nsp) <= nx ;nsi++, ns += nshift)
  {
    for (i=0 ; i< nsp ; i++) sp[i] = dsi->yd[ns + i];
    if (w_flag)
    {
      fftwindow(nsp, sp);
      for (i=0 ,mean = 0; i< nsp ; i++)	mean += sp[i];
      for (i=0 ; i< nsp ; i++) 		sp[i] = dsi->yd[ns + i];
    }
    else
    {
      for (i=0 ,mean = 0; i< nsp ; i++)	mean += sp[i];
    }
    mean /= nsp;
    for (i=0; rm_dc > 0 && i< nsp ; i++)				sp[i] -= mean;
    if ( w_flag)					fftwindow(nsp, sp);
    realtr1(nsp, sp);
    fft(nsp, sp, 1);
    realtr2(nsp, sp, 1);
    spec_real (nsp, sp, sp);
    sp[0] *= 2;
    sp[nsp/2] *= 2;
    for (i=0 ; i<= nsp/2 ; i++) 	dsd->yd[i] += sp[i];
  }
  free(sp);
  ns = nsi;
  for (i=0 ; i<= nsp/2 ; i++) 	dsd->yd[i] /= ns;
  for (i=0 ; i<= nsp/2; i++)
  {
    dsd->yd[i] *= (w_flag & 1) ? (dy *dy)/3 : (dy * dy)/2;
    dsd->yd[i] *=  (dt * nsp);
    dsd->xd[i] /=  (dt * nsp);
  }
  for (i=1 ; (cam_fil != 0) && (i<= nsp/2); i++)	/* correct camera */
  {
    tmp = camera_response((float)i/nsp, frame, factor);
    tmp += camera_response(1.0 - ((float)i/nsp), frame, factor);
    dsd->yd[i] /= (tmp != 0) ? tmp : 1;
  }
  return dsd;
}

int do_slidding_spectrum(void)
{
	int i;
	int ns, nsi;
	pltreg *pr = NULL;
	float  *sp = NULL;
	static int nsp = 1024, nshift = 128;
	O_p *op = NULL, *opd = NULL;
	d_s *dsi = NULL, *dsd = NULL;
	int  nx;
	char c[1024] = {0};
	static int  frame = 1, cam_fil = 0, w_flag = 1, rm_dc = 1;
	static float factor = 1;
	float  mean, tmp, dt, dy;
	un_s *un = NULL;

	if(updating_menu_state != 0)	return D_O_K;
	i = ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi);
	if ( i != 3)		return win_printf("cannot find data");
	nx = dsi->nx;


	un = op->yu[op->c_yu];
	if (un->type != IS_METER || un->decade != IS_MICRO)
		win_printf("warning the y unit is not \\mu m !\nnumerical value will be wrong");
	dy = un->dx;
	un = op->xu[op->c_xu];
	if (un->type != IS_SECOND || un->decade != 0)
		win_printf("warning the x unit is not seconds !\nnumerical value will be wrong");
/*	dt = un->dx;*/
	dt = un->dx * (dsi->xd[dsi->nx-1] - dsi->xd[0]);
	dt = (dsi->nx > 1) ? dt/(dsi->nx-1) : dt;


	frame = get_config_int("camera", "mode", 0);
	cam_fil = get_config_int("camera", "correction", 1);
	factor = get_config_float("camera", "openning", 1);
	nsp = get_config_int("spectrum", "slidding_size", 256);
	nshift = get_config_int("spectrum", "slidding_offset", 128);
	w_flag = get_config_float("spectrum", "windowing", 1);

	sprintf(c,"slidding spectrum along signal\n"
		"having %d points\nselect fft number of points %%d"
		"and shift between sample %%d"
		"camera correction  No-> %%R yes-> %%r\n"
		"frame integration mode: field -> %%R frame -> %%r\n"
		"opening factor %%f"
		"windowing: no %%R yes ->%%r"
		"remove DC no %%R yes %%r\n",nx);
	i = win_scanf(c,&nsp,&nshift,&cam_fil,&frame,&factor,&w_flag,&rm_dc);
	if (i == WIN_CANCEL)	return OFF;
	set_config_int("camera", "mode", frame);
	set_config_int("camera", "correction", cam_fil);
	set_config_float("camera", "openning", factor);
	set_config_float("spectrum", "windowing", w_flag);
	set_config_int("spectrum", "slidding_size", nsp);
	set_config_int("spectrum", "slidding_offset", nshift);


	if (nshift <= 0)
	{
		i = win_scanf("improper shift between fft,\n"
			      "the shift must be a positive number %d",&nshift);
		if (i == WIN_CANCEL)	return OFF;
	}
	opd = create_and_attach_one_plot(pr, nsp/2+1,nsp/2+1, 0);
	if (opd == NULL)		return win_printf("Cannot allocate plot");
	dsd = opd->dat[0];
	dsd = sliding_spectrum(dsi,dsd,nsp,  nshift,  cam_fil,  frame,  factor,  w_flag,  rm_dc,  dy,  dt);
  if (dsd == NULL) return(win_printf_OK("Erreur"));
  opd->dat[0] = dsd;

	pr->one_p->iopt |= YLOG;
	pr->one_p->iopt |= XLOG;

	set_plot_title(opd, "Averaged Spectrum");
	set_plot_y_title(opd,"<x^2> (\\mu m^2.Hz^{-1})");
	set_plot_x_title(opd,"frequency (Hz)");
	set_ds_treatement(dsd,"slidding average fft");


	refresh_plot(pr, UNCHANGED);
	return 0;
}
# ifdef OLD
int find_locale_rotate_plot(void)
{
	int i, j;
	pltreg *pr = NULL;
	float the, yx = 1, co, si;
	d_s *dsi = NULL, *dss = NULL, *dso = NULL;
	O_p *ops = NULL, *op = NULL;
	int  nx;
	int win_flag;
	float xm = 0, ym = 0, x2 = 0, zmin = 0;
	int  fit_poly_local_op(O_p *op, int n_data_set, float wfit, int n, float *zmin);

	if(updating_menu_state != 0)	return D_O_K;

	win_flag = RETRIEVE_MENU_INDEX;

	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&ops,&dsi) != 3)
		return win_printf("cannot find data");

	nx = dsi->nx;
	if (ops->dx == 0)	return win_printf("unit factor null !\ncannot treate");
	if (ops->dx != ops->dy)
	{
		win_printf("unit factor differs betwwen x and y!\n"
			   "converting both so that they correspond to the x one");
		yx = ops->dy/ops->dx;
	}
	op = create_and_attach_one_plot(pr, 45, 45, 0);
	dso = op->dat[0];
	if (dso == NULL)		return win_printf("can't create ds");
	dss = build_data_set( nx, nx);
	if (dss == NULL)		return win_printf("can't create ds");
	dss->nx = dss->nx = nx;
	mean_y2_on_array(dsi->xd, dsi->nx, win_flag, &xm, NULL, NULL);
	mean_y2_on_array(dsi->yd, dsi->ny, win_flag, &ym, NULL, NULL);
	for (j = 0; j < 45 ; j++)
	{
		the = j * M_PI/36;
		co = cos(the);
		si = sin(the);
		for (i=0 ; i< nx ; i++)
		{
			dss->xd[i] = co * (dsi->xd[i] - xm) + si * yx * (dsi->yd[i] - ym);
			dss->yd[i] = -si * (dsi->xd[i] - xm) + co * yx * (dsi->yd[i] - ym);
		}
		mean_y2_on_array(dss->xd, dss->nx, win_flag, NULL, &x2, NULL);
		dso->xd[j] = 5*j;
		dso->yd[j] = x2*ops->dx*ops->dx;
	}
	inherit_from_ds_to_ds(dso, dsi);
	set_plot_title(op, "x^2 versus \\theta");
	op->filename = Transfer_filename(ops->filename);
	op->dir = Mystrdup(ops->dir);
	free_data_set(dss);
	refresh_plot(pr, pr->cur_op);
	fit_poly_local_op(op, 0, 30, 4, &zmin);
	win_printf("the minimum is at %g degree",zmin);
	refresh_plot(pr, pr->cur_op);
	return 0;
}
# endif

#ifdef KEEP
/*		find anisotropy of a brownian motion, return a new ds with
 *		x axis corresponding to the maximum of variance
 *		anisitropy in %, angle in degrees
 */
d_s *find_locale_rotate_ds(O_p *ops, int nds, float *anisotropy, float *angle, int win_flag)
{
	int i;
	float the, yx = 1, co, si;
	d_s *dsi = NULL, *dss = NULL;
	int  nx;
	float xm = 0, ym = 0, x2 = 0, y2 = 0, x21, y21;


	if (ops == NULL)	return NULL;
	dsi = ops->dat[(nds < ops->n_dat) ? nds : ops->cur_dat];

	nx = dsi->nx;
	if (ops->dx == 0)
	{
		win_printf("unit factor null !\ncannot treate");
		return NULL;
	}
	if (ops->dx != ops->dy)
	{
		win_printf("unit factor differs betwwen x and y!\n"
			   "converting both so that they correspond to the x one");
		yx = ops->dy/ops->dx;
	}
	dss = build_data_set( nx, nx);
	if (dss == NULL)
	{
		win_printf("can't create ds");
		return NULL;
	}
	dss->nx = dss->ny = nx;
	mean_y2_on_array(dsi->xd, dsi->nx, win_flag, &xm, &x2, NULL);
	mean_y2_on_array(dsi->yd, dsi->ny, win_flag, &ym, &y2, NULL);
	the = M_PI/4;
	co = cos(the);		si = sin(the);
	for (i=0 ; i< nx ; i++)
	{
		dss->xd[i] = co * (dsi->xd[i] - xm) + si * yx * (dsi->yd[i] - ym);
		dss->yd[i] =  -si * (dsi->xd[i] - xm) + co * yx * (dsi->yd[i] - ym);
	}
	mean_y2_on_array(dss->xd, dss->nx, win_flag, NULL, &x21, NULL);
	mean_y2_on_array(dss->yd, dss->ny, win_flag, NULL, &y21, NULL);
	co = (x2 - y2)/2;
	si = (x21 - y21)/2;
	x21 = (x2 + y2)/2;
	if (x21 != 0) y21 = sqrt(co*co+si*si)/x21;
	the = atan2(si,co)/2;
	*anisotropy = 100*y21;
	*angle = 180*the/M_PI;
	co = cos(the);		si = sin(the);
	for (i=0 ; i< nx ; i++)
	{
		dss->xd[i] = xm + co * (dsi->xd[i] - xm) + si * yx * (dsi->yd[i] - ym);
		dss->yd[i] = ym -si * (dsi->xd[i] - xm) + co * yx * (dsi->yd[i] - ym);
	}
	inherit_from_ds_to_ds(dss, dsi);
	return(dss);
}
# endif
int find_anisotropy_and_rotate_plot(void)
{
	pltreg *pr = NULL;
	d_s *dsi = NULL, *ds = NULL;
	O_p *ops = NULL;
	int win_flag;
	float anisotropy = 0, angle = 0;

	if(updating_menu_state != 0)	return D_O_K;

	win_flag = RETRIEVE_MENU_INDEX;

	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&ops,&dsi) != 3)
		return win_printf("cannot find data");

	ds = find_locale_rotate_ds(ops, ops->cur_dat, &anisotropy, &angle, win_flag);
	if (ds == NULL)		return win_printf("cannot create anisotropic ds");
	inherit_from_ds_to_ds(ds, dsi);
	add_data_to_one_plot(ops, IS_DATA_SET, (void*)ds);
	win_printf("Anisotropy %g angle %g",anisotropy, angle);
	refresh_plot(pr, pr->cur_op);
	return 0;

}


/*		find anisotropy of a brownian motion, return a new ds with
 *		x axis corresponding to the maximum of variance
 *		anisitropy in %, angle in degrees
 */
d_s *find_locale_rotate_in_2_ds(d_s *dsx, d_s *dsy, float *anisotropy, float *angle, int win_flag)
{
	int i;
	float the, co, si;
	d_s  *dss = NULL;
	int  nx;
	float xm = 0, ym = 0, x2 = 0, y2 = 0, x21, y21;

	nx = dsx->nx;
	dss = build_data_set( nx, nx);
	if (dss == NULL)
	{
		win_printf("can't create ds");
		return NULL;
	}
	dss->nx = dss->ny = nx;
	mean_y2_on_array(dsx->yd, dsx->nx, win_flag, &xm, &x2, NULL);
	mean_y2_on_array(dsy->yd, dsy->ny, win_flag, &ym, &y2, NULL);
	the = M_PI/4;
	co = cos(the);		si = sin(the);
	for (i=0 ; i< nx ; i++)
	{
		dss->xd[i] = co * (dsx->yd[i] - xm) + si *  (dsy->yd[i] - ym);
		dss->yd[i] =  -si * (dsx->xd[i] - xm) + co  * (dsy->yd[i] - ym);
	}
	mean_y2_on_array(dss->xd, dss->nx, win_flag, NULL, &x21, NULL);
	mean_y2_on_array(dss->yd, dss->ny, win_flag, NULL, &y21, NULL);
	co = (x2 - y2)/2;
	si = (x21 - y21)/2;
	x21 = (x2 + y2)/2;
	if (x21 != 0) y21 = sqrt(co*co+si*si)/x21;
	the = atan2(si,co)/2;
	*anisotropy = 100*y21;
	*angle = 180*the/M_PI;
	co = cos(the);		si = sin(the);
	for (i=0 ; i< nx ; i++)
	{
		dss->xd[i] = xm + co * (dsx->yd[i] - xm) + si * (dsy->yd[i] - ym);
		dss->yd[i] = ym -si * (dsx->yd[i] - xm) + co * (dsy->yd[i] - ym);
	}
	inherit_from_ds_to_ds(dss, dsx);
	return(dss);
}

# ifdef PB
/*		find anisotropy of a brownian motion, return a new ds with
 *		x axis corresponding to the maximum of variance
 *		anisitropy in %, angle in degrees
 */
d_s *find_locale_rotate_in_2_ds_fourier(d_s *dsx, d_s *dsy, float *anisotropy, float *angle, int win_flag, float su2x, float su2y, int fb, int fm)
{
	int i;
	float the, co, si;
	d_s  *dss = NULL;
	int  nx;
	float xm = 0, ym = 0, x2 = 0, y2 = 0, x21, y21;

	nx = dsx->nx;
	dss = build_data_set( nx, nx);
	if (dss == NULL)
	{
		win_printf("can't create ds");
		return NULL;
	}
	dss->nx = dss->ny = nx;
	mean_y2_on_array(dsx->yd, dsx->nx, win_flag, &xm, &x2, NULL);
	mean_y2_on_array(dsy->yd, dsy->ny, win_flag, &ym, &y2, NULL);
	the = M_PI/4;
	co = cos(the);		si = sin(the);
	for (i=0 ; i< nx ; i++)
	{
		dss->xd[i] = co * (dsx->yd[i] - xm) + si *  (dsy->yd[i] - ym);
		dss->yd[i] =  -si * (dsx->xd[i] - xm) + co  * (dsy->yd[i] - ym);
	}

	alternate_lorentian_fit_acq_spe_with_error(dss->xd, dss->nx, opq->dy, fb, fm,
					       w_flag, cam->cam_fil, cam->factor, cam->frame,
					       &xfc, &a, &dxfc, &da, &chisq, &dx0);
    x2s = a * M_PI_2;
    xfhz = xfc /(nxx*dt);
    kxs = (1.38e-11 * 298)/x2s;



	mean_y2_on_array(dss->xd, dss->nx, win_flag, NULL, &x21, NULL);
	mean_y2_on_array(dss->yd, dss->ny, win_flag, NULL, &y21, NULL);
	co = (x2 - y2)/2;
	si = (x21 - y21)/2;
	x21 = (x2 + y2)/2;
	if (x21 != 0) y21 = sqrt(co*co+si*si)/x21;
	the = atan2(si,co)/2;
	*anisotropy = 100*y21;
	*angle = 180*the/M_PI;
	co = cos(the);		si = sin(the);
	for (i=0 ; i< nx ; i++)
	{
		dss->xd[i] = xm + co * (dsx->yd[i] - xm) + si * (dsy->yd[i] - ym);
		dss->yd[i] = ym -si * (dsx->yd[i] - xm) + co * (dsy->yd[i] - ym);
	}
	inherit_from_ds_to_ds(dss, dsx);
	return(dss);
}

# endif
int        do_full_new_treat_x_y_z_acq_spe(void)
{
    int i, j;
    char c[256], *file = NULL, *title = NULL, *unit_x = NULL, *unit_y = NULL, *cs = NULL;
    pltreg  *pri = NULL;
    d_s  *dsqx = NULL, *dsqy = NULL, *dsqz = NULL, *dsqzo = NULL; //, *ds;
    O_p *opq = NULL;
    static int fm = 1024, fb = 10, cam_fil = 1, frame = 0, w_flag = 1, new_pos = -1, is_cycl = 0;
    static float factor = 1;
    int nxx, nxy, nxz, is_cyclope_acq = 0;
    float xm, x2, x4, kxd, rx, dx0;
    float ym, y2, y4, kyd, ry, dy0;
    float zm, z2, z4, kzd, rz, dz0;
    float x2s, xfc, xfhz, kxs;
    float y2s, yfc, yfhz, kys;
    float z2s, zfc, zfhz, kzs;
    float zmin; //, zmin_ref;
    float xbr = 0 , ybr = 0, zbr = 0, xpr = 0, ypr = 0, zpr = 0;
    float xb0 = 0 , yb0 = 0, zb0 = 0, xp0 = 0, yp0 = 0, zp0 = 0;
    float a = .133, rot = 0, fox, foy;
    //int file_index = 0;//     file_index_found = 0,
    float l, anisotropy = 0, angle = 0, dt, dz;
    float zmag, zobj = 0;
    FILE *fpi = NULL;
    char f_line[256], s1[128], s2[1024], s3[128];
    float bd_x0 = 0,bd_y0 = 0,bd_z0 = 0;
    static float bd_x0_n = 0,bd_y0_n = 0,bd_z0_n = 0;
    float du_x0 = 0,du_y0 = 0,du_z0 = 0;
    float bd_x = 0,bd_y = 0,bd_z = 0;
    float du_x = 0,du_y = 0,du_z = 0;
    float dfc, da, chisq;


    if(updating_menu_state != 0)    return D_O_K;
    if (ac_grep(cur_ac_reg,"%pr%op",&pri,&opq) != 2)
    {
        win_printf("cannot find data");
        return OFF;
    }

    dsqx = opq->dat[0];    dsqy = opq->dat[1];    dsqz = opq->dat[2];
    if (opq->n_dat > 3)
    {
        dsqzo = opq->dat[3];
        cs = strstr(dsqx->source,"Cyclope");
         is_cyclope_acq = (cs != NULL) ? 1 : 0;
        if (is_cyclope_acq == 0)
        {
            cs = strstr(dsqx->source,"Zobj coordinate");
             is_cyclope_acq = (cs != NULL) ? 1 : 0;
        }
        if (is_cyclope_acq == 0)
        {
            i = win_scanf("Is it a Cyclope acquisition ? (1-> yes, 0 -> non) %d",&is_cycl);
            if (i != WIN_CANCEL)    is_cyclope_acq = is_cycl;
        }
    }
    dt = opq->dx;
    dz = opq->dy;

    sprintf(c,"%s\\force.gr",opq->dir);
    fpi = fopen(c,"r");
    if (fpi == NULL)
    {
        win_printf("cannot open %s !\n",c);
        return 1;
    }
    for (j = 0; fgets(f_line,256,fpi) != NULL && j == 0;  )
    {
        if ((sscanf(f_line,"%s%s%s",s1,s2,s3) == 3)
            && (strcmp(s1,"-src") == 0) && (strcmp(s3,"force") == 0))
        {
            i = fscanf(fpi," xbr = %f ybr = %f zbr = %f",&bd_x0,&bd_y0,&bd_z0);
            i += fscanf(fpi," xpr = %f ypr = %f zpr = %f",&du_x0,&du_y0,&du_z0);
            if (i != 6)
            {
                win_scanf("bead x0 %f y0 %f z0 %f dust x0 %f y0 %f z0 %f",
			  &bd_x0,&bd_y0,&bd_z0,&du_x0,&du_y0,&du_z0);
            }
            else j = 1;
        }
    }
    fclose(fpi);

    i = 0;
    cs = strstr(dsqx->source,"Bead xc =");  /* we don't care */
     if (cs != NULL)
     {
         i = sscanf(cs,"Bead xc = %f yc = %f zc = %f",&bd_x,&bd_y,&bd_z);
     }
    cs = strstr(dsqx->source,"dust xc =");
     if (cs != NULL)
     {
         i = sscanf(cs,"dust xc = %f yc = %f zc = %f",&du_x,&du_y,&du_z);

     }

    if (i != 3 || cs == NULL)
    {
        win_scanf("bead x0 %f y0 %f z0 %f dust x %f y %f z %f",
		  &bd_x,&bd_y,&bd_z,&du_x,&du_y,&du_z);
        if (i == WIN_CANCEL)    return OFF;

    }
    cs = strstr(dsqx->source,"Rotation");
     if (cs != NULL)
     {
         i = sscanf(cs,"Rotation %f",&rot);
        if (i != 1)    win_printf("cannot find rot");
     }
    cs = strstr(dsqx->source,"Z magnets ");
     if (cs != NULL)
     {
         i = sscanf(cs,"Z magnets %f",&zmag);
        if (i != 1)    win_printf("cannot find zmag");
     }
    cs = strstr(dsqx->source,"Z_{obj} = ");
     if (cs != NULL)
     {
         i = sscanf(cs,"Z_{obj} = %f",&zobj);
        if (i != 1)    win_printf("cannot find zobj");
     }
    zobj /= dz;
    win_printf("Z_{obj} = %f",zobj);

    if (new_pos == -1)
    {
        bd_x0_n = bd_x0;
        bd_y0_n = bd_y0;
        bd_z0_n = bd_z0;
        new_pos = 0;
    }
    fm = dsqx->nx/2;


    frame = get_config_int("camera", "mode", 0);
    cam_fil = get_config_int("camera", "correction", 1);
    factor = get_config_float("camera", "openning", 1);
    w_flag = get_config_int("spectrum", "windowing", 1);
    fb = get_config_int("spectrum", "lower_frequency", 10);
    i = win_scanf("fb %d fm %dwindowing -> 0 no 1 -> yes %dcamera correction ->1 yes ->0 no %d"
		  "frame int. ->1 field -> 0%dopening factor %f"
		  "new ref bead pos (1) correct present dust (2)\n"
		  " no correction (0)%d",&fb,&fm,&w_flag,&cam_fil,&frame,&factor,&new_pos);

    if (i == WIN_CANCEL)    return OFF;
    set_config_int("camera", "mode", frame);
    set_config_int("camera", "correction", cam_fil);
    set_config_float("camera", "openning", factor);
    set_config_int("spectrum", "windowing", w_flag);
    set_config_int("spectrum", "lower_frequency", fb);





    if (new_pos & 1)
    {
        bd_x0 = bd_x0_n;
        bd_y0 = bd_y0_n;
        bd_z0 = bd_z0_n;
        i = win_scanf("bead new x_0 %f bead new y_0 %fbead new z_0 %f",&bd_x0,&bd_y0,&bd_z0);                if (i == WIN_CANCEL)    return OFF;
    }
    if (new_pos & 2)
    {
        i = win_scanf("correct present dust new x %f dust new y %fdust new z %f",
		      &du_x,&du_y,&du_z);
	if (i == WIN_CANCEL)    return OFF;
    }
    file = opq->filename;
    title = opq->title;
    for (i = 0; file[i] && file[i] != '.'; i++);
    //file_index_found = sscanf(file + (((i - 3) >= 0) ? i - 3 : 0),"%d",&file_index);
    /*
    ds = find_locale_rotate_in_2_ds(dsqx, dsqy, &anisotropy, &angle);
    if (ds == NULL)        return win_printf("cannot create anisotropic ds");
    else free_data_set(ds);
    */
    switch_plot_data_set(opq, 0);
    mean_y2_on_op(opq, w_flag, &xm, &x2, &x4, &kxd, &nxx);
    rx = (x2 != 0) ? x4/(x2*x2) : 0;

    switch_plot_data_set(opq, 1);
    mean_y2_on_op(opq, w_flag, &ym, &y2, &y4, &kyd, &nxy);
    ry = (y2 != 0) ? y4/(y2*y2) : 0;

    for (j = 0; is_cyclope_acq == 1 && j < dsqz->nx; j++)
        dsqz->yd[j] = dsqzo->yd[j] + dsqz->yd[j] - zobj;


    switch_plot_data_set(opq, 2);
    mean_y2_on_op(opq, w_flag, &zm, &z2, &z4, &kzd, &nxz);
    rz = (z2 != 0) ? z4/(z2*z2) : 0;

    switch_plot_data_set(opq, 0);
    alternate_lorentian_fit_acq_spe_with_error(dsqx->yd, dsqx->ny, opq->dy, fb, fm,
					       w_flag, cam_fil, factor, frame, &xfc,
					       &a, &dfc, &da, &chisq, &dx0);
    x2s = a * M_PI_2;
    xfhz = xfc /(nxx*dt);
    kxs = (1.38e-11 * 298)/x2s;

    alternate_lorentian_fit_acq_spe_with_error(dsqy->yd, dsqy->ny, opq->dy, fb, fm,
					       w_flag, cam_fil, factor, frame, &yfc,
					       &a, &dfc, &da, &chisq, &dy0);
    y2s = a * M_PI_2;
    yfhz = yfc /(nxy*dt);
    kys = (1.38e-11 * 298)/y2s;


    alternate_lorentian_fit_acq_spe_with_error(dsqz->yd, dsqz->ny, opq->dy, fb, fm,
					       w_flag, cam_fil, factor, frame, &zfc,
					       &a, &dfc, &da, &chisq, &dz0);
    z2s = a * M_PI_2;
    zfhz = zfc /(nxz*dt);
    kzs = (1.38e-11 * 298)/z2s;

    for (j = 0; is_cyclope_acq == 1 && j < dsqz->nx; j++)
        dsqz->yd[j] =  dsqz->yd[j] + zobj - dsqzo->yd[j];


    xb0 = xm;
    yb0 = ym;
    zmin = zb0 = zm;
    xbr = bd_x0;
    ybr = bd_y0;
    //zmin_ref = zbr = 0;
    xp0 = du_x;
    yp0 = du_y;
    zp0 = du_z;
    xpr = du_x0;
    ypr = du_y0;
    zpr = du_z0;

    a =   .878;

    l = sqrt((xb0-xbr-xp0+xpr)*(xb0-xbr-xp0+xpr)+(yb0-ybr-yp0+ypr)*
	     (yb0-ybr-yp0+ypr)+((zb0-zbr-zp0+zpr)*(zb0-zbr-zp0+zpr)*a*a));
    fox = l * kxs * 1e6;
    foy = l * kys * 1e6;

    unit_x = "\\mu m";
    unit_y = "\\mu m";

    c[0] = 0;
    if (opq->dir != NULL)    strcpy(c,opq->dir);
    backslash_to_slash(c);

    dump_to_log_file_only("{\\bf Analyse spectrale de %s fichier %s in %s}\n\n",
	 		  (title == NULL)?"?":title,file,c);
    dump_to_log_file_only("\\noindent\\begin{tabular}{|l|c|c|c|c|c|c|c|c|}\n\\hline\n "
			  "file & axe & $<mean>$ & $k$ & $k_s$ & r\\'ef\\'erence & $f_c$  "
			  "& $f_c$ & nx/r  \\\\\n rot %g Zm %g& & $%s$ & $\\times 10^{-8} N/m"
			  "$ & $\\times 10^{-8} N/m$ & $%s$ & \\# & (Hz) & %d \\\\\n"
			  "\\hline\n\\small %s & x(t) & %g & %g & %g & %g & %g & %g & %g  \\\\\n"
			  "\\hline\n %s  & y(t) & %g & %g & %g & %g & %g & %g & %g  \\\\\n"
			  " \\hline\n   & z(t) & %g & %g & %g & %g & %g & %g & %g  \\\\\n"
			  " \\hline\n   & L & %g & $f_0$ %g & $f_1$ %g & \\multicolumn{2}{|c|}"
			  "{ani. %g \\%%} & \\multicolumn{2}{|c|}{angle %g ${}^\\circ$} \\\\\n"
			  "  \\hline\n\\end{tabular} \n",rot,zmag,unit_x,unit_y,nxx,c,xm,kxd*1e8,
			  kxs*1e8,du_x,xfc,xfhz,rx,file,ym,kyd*1e8,kys*1e8,du_y,yfc,yfhz,ry,zmin,
			  kzd*1e8,kzs*1e8,zp0,zfc,zfhz,rz,l,fox,foy,anisotropy,angle);

    return D_O_K;
}

int        do_full_new_treat_x_y_z_acq_spe_pico(void)
{
    int i;
    char c[256], *file = NULL, *title = NULL, *unit_x = NULL, *unit_y = NULL, *cs = NULL;
    pltreg  *pri = NULL;
    d_s  *dsqx = NULL, *dsqy = NULL, *dsqz = NULL; //, *ds;
    O_p *opq = NULL;
    static int fm = 1024, fb = 10, cam_fil = 1, frame = 0, w_flag = 1, new_pos = -1;
    static float factor = 1;
    int nxx, nxy, nxz, n_bd = 0, force_n = 0;
    float xm, x2, x4, kxd, rx, dx0;
    float ym, y2, y4, kyd, ry, dy0;
    float zm, z2, z4, kzd, rz, dz0;
    float x2s, xfc, xfhz, kxs;
    float y2s, yfc, yfhz, kys;
    float z2s, zfc, zfhz, kzs;
    float zmin; //, zmin_ref;
    float xbr = 0 , ybr = 0, zbr = 0, xpr = 0, ypr = 0, zpr = 0;
    float xb0 = 0 , yb0 = 0, zb0 = 0, xp0 = 0, yp0 = 0, zp0 = 0;
    float a = .133, rot = 0, fox, foy;
    int   file_index = 0;
    float l, anisotropy = 0, angle = 0, dt;//, dz;
    float zmag;
    //FILE *fpi;
    //char f_line[256], s1[128], s2[1024], s3[128];
    //float bd_x0 = 0,bd_y0 = 0,bd_z0 = 0;
    //static float bd_x0_n = 0,bd_y0_n = 0,bd_z0_n = 0;
    float du_x0 = 0,du_y0 = 0,du_z0 = 0;
    //float bd_x = 0,bd_y = 0,bd_z = 0;
    float du_x = 0,du_y = 0,du_z = 0;
    float dfc, da, chisq;


    if(updating_menu_state != 0)    return D_O_K;
    if (ac_grep(cur_ac_reg,"%pr%op",&pri,&opq) != 2)
    {
        win_printf("cannot find data");
        return OFF;
    }

    dsqx = opq->dat[0];    dsqy = opq->dat[1];    dsqz = opq->dat[2];
    dt = opq->dx;
    //dz = opq->dy;

    cs = strstr(dsqx->source,"Rotation");
     if (cs != NULL)
     {
         i = sscanf(cs,"Rotation %f",&rot);
        if (i != 1)    win_printf("cannot find rot");
     }
    cs = strstr(dsqx->source,"Z magnets ");
     if (cs != NULL)
     {
         i = sscanf(cs,"Z magnets %f",&zmag);
        if (i != 1)    win_printf("cannot find zmag");
     }
    fm = dsqx->nx/2;


    frame = get_config_int("camera", "mode", 0);
    cam_fil = get_config_int("camera", "correction", 1);
    factor = get_config_float("camera", "openning", 1);
    w_flag = get_config_int("spectrum", "windowing", 1);
    fb = get_config_int("spectrum", "lower_frequency", 10);
    i = win_scanf("fb %d fm %dwindowing -> 0 no 1 -> yes %dcamera correction ->1 yes ->0 no %d"
		  "frame int. ->1 field -> 0%dopening factor %f"
		  "new ref bead pos (1) correct present dust (2)\n"
		  " no correction (0)%d",&fb,&fm,&w_flag,&cam_fil,&frame,&factor,&new_pos);

    if (i == WIN_CANCEL)    return OFF;
    set_config_int("camera", "mode", frame);
    set_config_int("camera", "correction", cam_fil);
    set_config_float("camera", "openning", factor);
    set_config_int("spectrum", "windowing", w_flag);
    set_config_int("spectrum", "lower_frequency", fb);


    file = opq->filename;
    title = opq->title;

    sscanf(file,"X(t)Y(t)Z(t)bd%dforce%d-%d.gr",&n_bd,&force_n,&file_index);
    switch_plot_data_set(opq, 0);
    mean_y2_on_op(opq, w_flag, &xm, &x2, &x4, &kxd, &nxx);
    rx = (x2 != 0) ? x4/(x2*x2) : 0;

    switch_plot_data_set(opq, 1);
    mean_y2_on_op(opq, w_flag, &ym, &y2, &y4, &kyd, &nxy);
    ry = (y2 != 0) ? y4/(y2*y2) : 0;


    switch_plot_data_set(opq, 2);
    mean_y2_on_op(opq, w_flag, &zm, &z2, &z4, &kzd, &nxz);
    rz = (z2 != 0) ? z4/(z2*z2) : 0;

    switch_plot_data_set(opq, 0);
    alternate_lorentian_fit_acq_spe_with_error(dsqx->yd, dsqx->ny, opq->dy, fb, fm,
					       w_flag, cam_fil, factor, frame, &xfc,
					       &a, &dfc, &da, &chisq, &dx0);
    x2s = a * M_PI_2;
    xfhz = xfc /(nxx*dt);
    kxs = (1.38e-11 * 298)/x2s;

    alternate_lorentian_fit_acq_spe_with_error(dsqy->yd, dsqy->ny, opq->dy, fb, fm,
					       w_flag, cam_fil, factor, frame, &yfc,
					       &a, &dfc, &da, &chisq, &dy0);
    y2s = a * M_PI_2;
    yfhz = yfc /(nxy*dt);
    kys = (1.38e-11 * 298)/y2s;


    alternate_lorentian_fit_acq_spe_with_error(dsqz->yd, dsqz->ny, opq->dy, fb, fm,
					       w_flag, cam_fil, factor, frame, &zfc,
					       &a, &dfc, &da, &chisq, &dz0);
    z2s = a * M_PI_2;
    zfhz = zfc /(nxz*dt);
    kzs = (1.38e-11 * 298)/z2s;


    xb0 = xm;
    yb0 = ym;
    zmin = zb0 = zm;
    xbr = 0;
    ybr = 0;
    //zmin_ref = zbr = 0;
    xp0 = du_x;
    yp0 = du_y;
    zp0 = du_z;
    xpr = du_x0;
    ypr = du_y0;
    zpr = du_z0;

    a =   .878;

    l = sqrt((xb0-xbr-xp0+xpr)*(xb0-xbr-xp0+xpr)+(yb0-ybr-yp0+ypr)*
	     (yb0-ybr-yp0+ypr)+((zb0-zbr-zp0+zpr)*(zb0-zbr-zp0+zpr)*a*a));
    fox = l * kxs * 1e6;
    foy = l * kys * 1e6;

    unit_x = "\\mu m";
    unit_y = "\\mu m";

    c[0] = 0;
    if (opq->dir != NULL)    strcpy(c,opq->dir);
    backslash_to_slash(c);

    dump_to_log_file_only("{\\bf Analyse spectrale de %s fichier %s in %s}\n\n",
	 		  (title == NULL)?"?":title,file,c);
    dump_to_log_file_only("\\noindent\\begin{tabular}{|l|c|c|c|c|c|c|c|c|}\n\\hline\n "
			  "file & axe & $<mean>$ & $k$ & $k_s$ & r\\'ef\\'erence & $f_c$  "
			  "& $f_c$ & nx/r  \\\\\n rot %g Zm %g& & $%s$ & $\\times 10^{-8} N/m"
			  "$ & $\\times 10^{-8} N/m$ & $%s$ & \\# & (Hz) & %d \\\\\n"
			  "\\hline\n\\small %s & x(t) & %g & %g & %g & %g & %g & %g & %g  \\\\\n"
			  "\\hline\n %s  & y(t) & %g & %g & %g & %g & %g & %g & %g  \\\\\n"
			  " \\hline\n   & z(t) & %g & %g & %g & %g & %g & %g & %g  \\\\\n"
			  " \\hline\n   & L & %g & $f_0$ %g & $f_1$ %g & \\multicolumn{2}{|c|}"
			  "{ani. %g \\%%} & \\multicolumn{2}{|c|}{angle %g ${}^\\circ$} \\\\\n"
			  "  \\hline\n\\end{tabular} \n",rot,zmag,unit_x,unit_y,nxx,c,xm,kxd*1e8,
			  kxs*1e8,du_x,xfc,xfhz,rx,file,ym,kyd*1e8,kys*1e8,du_y,yfc,yfhz,ry,zmin,
			  kzd*1e8,kzs*1e8,zp0,zfc,zfhz,rz,l,fox,foy,anisotropy,angle);

    return D_O_K;
}



multi_d_s *create_mds_retreat_acq(char *basename)
{
  int i = 0;
  multi_d_s *mds = NULL;

  mds = create_multi_d_s(77);
  if (mds == NULL)  return NULL;
  mds->name[i++] = strdup("# index");
  mds->name[i++] = strdup(basename);
  mds->name[i++] = strdup("Mag rot");
  mds->name[i++] = strdup("Mag Z");
  mds->name[i++] = strdup("# pts.");
  mds->name[i++] = strdup("Bead <x>");
  mds->name[i++] = strdup("Error on <x>");
  mds->name[i++] = strdup("\\sigma_x^2");
  mds->name[i++] = strdup("Error on \\sigma_x^2");
  mds->name[i++] = strdup("gaussian x");
  mds->name[i++] = strdup("Ref. <x>");
  mds->name[i++] = strdup("Ref. shift \\delta x");
  mds->name[i++] = strdup("k_x");
  mds->name[i++] = strdup("error on k_x");
  mds->name[i++] = strdup("f_{cx} (mode)");
  mds->name[i++] = strdup("f_{cx} (Hz)");
  mds->name[i++] = strdup("df_{cx}/f_{cx} (%)");
  mds->name[i++] = strdup("\\eta_x .r (k/6\\pi\\omega_c)");
  mds->name[i++] = strdup("\\eta_x .r (dx/dt)");
  mds->name[i++] = strdup("Error \\eta_x .r (dx/dt)");
  mds->name[i++] = strdup("Bead <y>");
  mds->name[i++] = strdup("Error on <y>");
  mds->name[i++] = strdup("gaussian y");
  mds->name[i++] = strdup("\\sigma_y^2");
  mds->name[i++] = strdup("Error on \\sigma_y^2");
  mds->name[i++] = strdup("Ref. <y>");
  mds->name[i++] = strdup("Ref. shift \\delta y");
  mds->name[i++] = strdup("k_y");
  mds->name[i++] = strdup("error on k_y");
  mds->name[i++] = strdup("f_{cy} (mode)");
  mds->name[i++] = strdup("f_{cy} (Hz)");
  mds->name[i++] = strdup("df_{cy}/f_{cy} (%)");
  mds->name[i++] = strdup("\\eta_y .r (k/6\\pi\\omega_c)");
  mds->name[i++] = strdup("\\eta_y .r (dx/dt)");
  mds->name[i++] = strdup("Error \\eta_y .r (dx/dt)");
  mds->name[i++] = strdup("Bead <z>");
  mds->name[i++] = strdup("Error on <z>");
  mds->name[i++] = strdup("gaussian z");
  mds->name[i++] = strdup("\\sigma_z^2");
  mds->name[i++] = strdup("Error on \\sigma_z^2");
  mds->name[i++] = strdup("Ref. <Z>");
  mds->name[i++] = strdup("Ref. shift \\delta z");
  mds->name[i++] = strdup("k_z");
  mds->name[i++] = strdup("error on k_z");
  mds->name[i++] = strdup("f_{cz} (mode)");
  mds->name[i++] = strdup("f_{cz} (Hz)");
  mds->name[i++] = strdup("df_{cz}/f_{cz} (%)");
  mds->name[i++] = strdup("\\eta_z .r (k/6\\pi\\omega_c)");
  mds->name[i++] = strdup("\\eta_z .r (dx/dt)");
  mds->name[i++] = strdup("Error \\eta_z .r (dx/dt)");
  mds->name[i++] = strdup("l=\\sqrt{x^2+y^2+z^2}");
  mds->name[i++] = strdup("f_{ox}");
  mds->name[i++] = strdup("f_{oy}");
  mds->name[i++] = strdup("anisotropy");
  mds->name[i++] = strdup("angle");
  mds->name[i++] = strdup("l = z");
  mds->name[i++] = strdup("f_{1x}");
  mds->name[i++] = strdup("f_{1y}");


  mds->name[i++] = strdup("\\sigma_{xa}^2");
  mds->name[i++] = strdup("Error on \\sigma_{xa}^2");
  mds->name[i++] = strdup("k_{xa}");
  mds->name[i++] = strdup("error on k_{xa}");
  mds->name[i++] = strdup("f_{cxa} (mode)");
  mds->name[i++] = strdup("f_{cxa} (Hz)");
  mds->name[i++] = strdup("df_{cxa}/f_{cxa} (%)");
  mds->name[i++] = strdup("\\eta_{xa} .r (k/6\\pi\\omega_c)");
  mds->name[i++] = strdup("\\eta_{xa} .r (dx/dt)");
  mds->name[i++] = strdup("Error \\eta_{xa} .r (dx/dt)");

  mds->name[i++] = strdup("\\sigma_{ya}^2");
  mds->name[i++] = strdup("Error on \\sigma_{ya}^2");
  mds->name[i++] = strdup("k_{ya}");
  mds->name[i++] = strdup("error on k_{ya}");
  mds->name[i++] = strdup("f_{cya} (mode)");
  mds->name[i++] = strdup("f_{cya} (Hz)");
  mds->name[i++] = strdup("df_{cya}/f_{cya} (%)");
  mds->name[i++] = strdup("\\eta_{ya} .r (k/6\\pi\\omega_c)");
  mds->name[i++] = strdup("\\eta_{ya} .r (dy/dt)");
  mds->name[i++] = strdup("Error \\eta_{ya} .r (dy/dt)");





  /*
		dump_to_current_xvplot_file( "%d\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g"
		"\t%g\t%g",file_index,su,(a!=0)?su*da/a:su,kx,(a!=0)?kx*da/a:kx,
		fc/(nx*dt),(fc!=0)?100*dfc/fc:dfc,etar,etar2,etar2*detar2,dy0);
  */
  return mds;
}


multi_d_s *do_full_new_treat_x_y_z_acq_spe_op(O_p *opq, multi_d_s *mds, cam_cor *caml, int w_flag, float high_fc_ratio, int fbm, ref_pos *bd_du)
{
    int i, j;
    char *file = NULL, *cs = NULL, basename[256] = {0};
    d_s  *dsqx = NULL, *dsqy = NULL, *dsqz = NULL, *dsqzo = NULL, *dsqxa = NULL, *dsqya = NULL;
    O_p *opd = NULL;
    static int fm = 1024, fb = 10, is_cycl = 0, nx, n_dsqxa, n_dsqya;
    int nxx, nxy, nxz, is_cyclope_acq = 0, flow;
    float xm, x2, x4, kxd, rx, dx0;
    float ym, y2, y4, kyd, ry, dy0;
    float zm, z2, z4, kzd, rz, dz0;
    float x2s, xfc, xfhz, kxs, dxfc;
    float y2s, yfc, yfhz, kys, dyfc;
    float z2s, zfc, zfhz, kzs, dzfc;
    float x2as, xafc, xafhz, kxas, dxafc, dxa0;
    float y2as, yafc, yafhz, kyas, dyafc, dya0;
    //float zmin, zmin_ref;
    double co, si, the, tmp, y21;
    float xbr = 0 , ybr = 0, zbr = 0, xpr = 0, ypr = 0, zpr = 0;
    float xb0 = 0 , yb0 = 0, zb0 = 0, xp0 = 0, yp0 = 0, zp0 = 0;
    float a = .133, rot = 0, fox, foy;
    int  file_index = 0; //    file_index_found = 0,
    float l, anisotropy = 0, angle = 0, dt, dz;
    float zmag, zobj = 0;
    float bd_x = 0,bd_y = 0,bd_z = 0;
    float du_x = 0,du_y = 0,du_z = 0;
    float da, chisq, val[128] = {0}, etar, etar2, detar2;




    dsqx = opq->dat[0];    dsqy = opq->dat[1];    dsqz = opq->dat[2];
    if (opq->n_dat > 3)
    {
      //win_printf("more than 3 dsfor %f",opq->filename);
        dsqzo = opq->dat[3];
        cs = strstr(dsqx->source,"Cyclope");
	is_cyclope_acq = (cs != NULL) ? 1 : 0;
        if (is_cyclope_acq == 0)
        {
            cs = strstr(dsqx->source,"Zobj coordinate");
             is_cyclope_acq = (cs != NULL) ? 1 : 0;
        }
        if (is_cyclope_acq == 0)
        {
            i = win_scanf("Is it a Cyclope acquisition ? (1-> yes, 0 -> non) %d",&is_cycl);
            if (i != WIN_CANCEL)    is_cyclope_acq = is_cycl;
        }
    }
    dt = opq->dx;
    dz = opq->dy;


    i = 0;
    cs = strstr(dsqx->source,"Bead xc =");  /* we don't care */
     if (cs != NULL)
     {
         i = sscanf(cs,"Bead xc = %f yc = %f zc = %f",&bd_x,&bd_y,&bd_z);
     }
    cs = strstr(dsqx->source,"dust xc =");
     if (cs != NULL)
     {
         i = sscanf(cs,"dust xc = %f yc = %f zc = %f",&du_x,&du_y,&du_z);

     }

    if (i != 3 || cs == NULL)
    {
        win_scanf("bead x0 %f y0 %f z0 %f dust x %f y %f z %f",
		  &bd_x,&bd_y,&bd_z,&du_x,&du_y,&du_z);
        if (i == WIN_CANCEL)    return OFF;

    }
    cs = strstr(dsqx->source,"Rotation");
     if (cs != NULL)
     {
         i = sscanf(cs,"Rotation %f",&rot);
        if (i != 1)    win_printf("cannot find rot");
     }
    cs = strstr(dsqx->source,"Z magnets ");
     if (cs != NULL)
     {
         i = sscanf(cs,"Z magnets %f",&zmag);
        if (i != 1)    win_printf("cannot find zmag");
     }
    cs = strstr(dsqx->source,"Z_{obj} = ");
     if (cs != NULL)
     {
         i = sscanf(cs,"Z_{obj} = %f",&zobj);
        if (i != 1)    win_printf("cannot find zobj");
     }
    zobj /= dz;
    display_title_message("%s Z_{obj} = %f",opq->filename,zobj);

    fm = (high_fc_ratio <= 1 && high_fc_ratio > 0) ? (int)(high_fc_ratio*dsqx->nx/2) : dsqx->nx/2;
    fb = (fbm < dsqx->nx/2) ? fbm : 8;
    file = opq->filename;
    //title = opq->title;
    for (i = 0; file[i] && file[i] != '.'; i++);
    //file_index_found = sscanf(file + (((i - 3) >= 0) ? i - 3 : 0),"%d",&file_index);
    strcpy(basename,file);
    basename[(((i - 3) >= 0) ? i - 3 : 0)] = 0;

    if (mds == NULL)
      mds = create_mds_retreat_acq(basename);
    if (mds == NULL)  return NULL;

    val[0] = mds->n;
    val[1] = file_index;
    val[2] = rot;
    val[3] = zmag;
    val[4] = dsqx->nx;


    switch_plot_data_set(opq, 0);
    mean_y2_on_op(opq, w_flag, &xm, &x2, &x4, &kxd, &nxx);
    rx = (x2 != 0) ? x4/(x2*x2) : 0;


    switch_plot_data_set(opq, 1);
    mean_y2_on_op(opq, w_flag, &ym, &y2, &y4, &kyd, &nxy);
    ry = (y2 != 0) ? y4/(y2*y2) : 0;

    for (j = 0; is_cyclope_acq == 1 && j < dsqz->nx; j++)
        dsqz->yd[j] = dsqzo->yd[j] + dsqz->yd[j] - zobj;


    switch_plot_data_set(opq, 2);
    mean_y2_on_op(opq, w_flag, &zm, &z2, &z4, &kzd, &nxz);
    rz = (z2 != 0) ? z4/(z2*z2) : 0;

    switch_plot_data_set(opq, 0);
    alternate_lorentian_fit_acq_spe_with_error(dsqx->yd, dsqx->ny, opq->dy, fb, fm,
					       w_flag, caml->cam_fil, caml->factor, caml->frame,
					       &xfc, &a, &dxfc, &da, &chisq, &dx0);
    x2s = a * M_PI_2;
    xfhz = xfc /(nxx*dt);
    kxs = (1.38e-11 * 298)/x2s;

    etar = kxs*nxx*dt/(12*M_PI*M_PI*xfc);
    flow = (int)(2*xfc);
    flow = (flow > nxx/4) ? nxx/4 : flow;
    opd = do_fft_and_derivate_on_op_2(opq, flow, fm, w_flag+(caml->cam_fil<<1), xfc,
				      caml->frame, caml->factor, &etar2, &detar2);
    free_one_plot(opd);
    detar2 = (etar2 != 0) ? detar2/etar2 : detar2;

    val[5] = xm;
    val[6] = dx0;
    val[7] = x2s;
    val[8] = da * M_PI_2;
    val[9] = rx;
    val[10] = du_x; //ref x;
    val[11] = du_x - bd_du->du_x0; //ref shift;
    val[12] = kxs;
    val[13] = (a != 0) ? (kxs * da)/a : 0;
    val[14] = xfc;
    val[15] = xfhz;
    val[16] = (xfc != 0) ? 100*dxfc/xfc : 0;
    val[17] = etar;
    val[18] = etar2;
    val[19] = detar2;

    switch_plot_data_set(opq, 1);
    alternate_lorentian_fit_acq_spe_with_error(dsqy->yd, dsqy->ny, opq->dy, fb, fm,
					       w_flag,  caml->cam_fil, caml->factor, caml->frame,
					       &yfc, &a, &dyfc, &da, &chisq, &dy0);
    y2s = a * M_PI_2;
    yfhz = yfc /(nxy*dt);
    kys = (1.38e-11 * 298)/y2s;

    etar = kys*nxy*dt/(12*M_PI*M_PI*yfc);
    flow = (int)(2*yfc);
    flow = (flow > nxy/4) ? nxy/4 : flow;
    opd = do_fft_and_derivate_on_op_2(opq, flow, fm, w_flag+(caml->cam_fil<<1), xfc,
				      caml->frame, caml->factor, &etar2, &detar2);
    free_one_plot(opd);
    detar2 = (etar2 != 0) ? detar2/etar2 : detar2;

    val[20] = ym;
    val[21] = dy0;
    val[22] = y2s;
    val[23] = da * M_PI_2;
    val[24] = ry;
    val[25] = du_y; //ref x;
    val[26] = du_y - bd_du->du_y0; //ref shift;
    val[27] = kys;
    val[28] = (a != 0) ? (kys * da)/a : 0;
    val[29] = yfc;
    val[30] = yfhz;
    val[31] = (yfc != 0) ? 100*dyfc/yfc : 0;
    val[32] = etar;
    val[33] = etar2;
    val[34] = detar2;


    switch_plot_data_set(opq, 2);
    alternate_lorentian_fit_acq_spe_with_error(dsqz->yd, dsqz->ny, opq->dy, fb, fm,
					       w_flag, caml->cam_fil, caml->factor, caml->frame,
					       &zfc, &a, &dzfc, &da, &chisq, &dz0);
    z2s = a * M_PI_2;
    zfhz = zfc /(nxz*dt);
    kzs = (1.38e-11 * 298)/z2s;

    etar = kzs*nxz*dt/(12*M_PI*M_PI*zfc);
    flow = (int)(2*zfc);
    flow = (flow > nxz/4) ? nxz/4 : flow;
    opd = do_fft_and_derivate_on_op_2(opq, flow, fm, w_flag+(caml->cam_fil<<1), xfc,
				      caml->frame, caml->factor, &etar2, &detar2);
    free_one_plot(opd);
    detar2 = (etar2 != 0) ? detar2/etar2 : detar2;

    val[35] = zm;
    val[36] = dz0;
    val[37] = z2s;
    val[38] = da * M_PI_2;
    val[39] = rz;
    val[40] = du_z; //ref x;
    val[41] = du_z - bd_du->du_z0; //ref shift;
    val[42] = kzs;
    val[43] = (a != 0) ? (kzs * da)/a : 0;
    val[44] = zfc;
    val[45] = zfhz;
    val[46] = (zfc != 0) ? 100*dzfc/zfc : 0;
    val[47] = etar;
    val[48] = etar2;
    val[49] = detar2;



    for (j = 0; is_cyclope_acq == 1 && j < dsqz->nx; j++)
        dsqz->yd[j] =  dsqz->yd[j] + zobj - dsqzo->yd[j];


    xb0 = xm;
    yb0 = ym;
    //zmin = zb0 = zm;
    xbr = bd_du->bd_x0;
    ybr = bd_du->bd_y0;
    //zmin_ref = zbr = 0;
    xp0 = du_x;
    yp0 = du_y;
    zp0 = du_z;
    xpr = bd_du->du_x0;
    ypr = bd_du->du_y0;
    zpr = bd_du->du_z0;

    a =   .878;

    l = sqrt((xb0-xbr-xp0+xpr)*(xb0-xbr-xp0+xpr)+(yb0-ybr-yp0+ypr)*
	     (yb0-ybr-yp0+ypr)+((zb0-zbr-zp0+zpr)*(zb0-zbr-zp0+zpr)*a*a));
    fox = l * kxs * 1e6;
    foy = l * kys * 1e6;

    val[50] = l;
    val[51] = fox;
    val[52] = foy;


    nx = dsqx->nx;
    if ((dsqxa = create_and_attach_one_ds(opq, nx, nx, 0)) == NULL)
    {
         win_printf("cannot create ds!");
        return NULL;
    }
    n_dsqxa = opq->n_dat;

    if ((dsqya = create_and_attach_one_ds(opq, nx, nx, 0)) == NULL)
    {
         win_printf("cannot create ds!");
        return NULL;
    }
    n_dsqya = opq->n_dat;

    the = M_PI/4;
    co = cos(the);		si = sin(the);
    for (i=0 ; i< nx ; i++)
      {
	dsqxa->yd[i] = co * (dsqx->yd[i] - xm) + si *  (dsqy->yd[i] - ym);
	dsqya->yd[i] =  -si * (dsqx->yd[i] - xm) + co  * (dsqy->yd[i] - ym);
      }

    alternate_lorentian_fit_acq_spe_with_error(dsqxa->yd, nx, opq->dy, fb, fm,
					       w_flag, caml->cam_fil, caml->factor, caml->frame,
					       &xafc, &a, &dxafc, &da, &chisq, &dxa0);
    x2as = a * M_PI_2;
    xafhz = xafc /(nxx*dt);
    kxas = (1.38e-11 * 298)/x2as;


    alternate_lorentian_fit_acq_spe_with_error(dsqya->yd, nx, opq->dy, fb, fm,
					       w_flag, caml->cam_fil, caml->factor, caml->frame,
					       &yafc, &a, &dyafc, &da, &chisq, &dya0);
    y2as = a * M_PI_2;
    yafhz = yafc /(nxx*dt);
    kxas = (1.38e-11 * 298)/y2as;


    co = (x2s - y2s)/2;
    si = (x2as - y2as)/2;
    tmp = (x2s + y2s)/2;
    y21 = (tmp != 0) ? sqrt(co*co+si*si)/tmp : 0;
    the = atan2(si,co)/2;
    anisotropy = 100*y21;
    angle = 180*the/M_PI;
    co = cos(the);		si = sin(the);
    for (i=0 ; i< nx ; i++)
      {
	dsqxa->xd[i] = xm + co * (dsqx->yd[i] - xm) + si * (dsqy->yd[i] - ym);
	dsqya->yd[i] = ym -si * (dsqx->yd[i] - xm) + co * (dsqy->yd[i] - ym);
      }

    val[53] = anisotropy;
    val[54] = angle;

    a =   .878;
    l = (zb0-zbr-zp0+zpr)*a;
    fox = l * kxs * 1e6;
    foy = l * kys * 1e6;

    val[55] = l;
    val[56] = fox;
    val[57] = foy;

    switch_plot_data_set(opq, n_dsqxa);
    alternate_lorentian_fit_acq_spe_with_error(dsqxa->yd, nx, opq->dy, fb, fm,
					       w_flag, caml->cam_fil, caml->factor, caml->frame,
					       &xafc, &a, &dxafc, &da, &chisq, &dxa0);
    x2as = a * M_PI_2;
    xafhz = xafc /(nxx*dt);
    kxas = (1.38e-11 * 298)/x2as;


    opd = do_fft_and_derivate_on_op_2(opq, flow, fm, w_flag+(caml->cam_fil<<1), xfc,
				      caml->frame, caml->factor, &etar2, &detar2);
    free_one_plot(opd);
    detar2 = (etar2 != 0) ? detar2/etar2 : detar2;

    val[58] = x2as;
    val[59] = da * M_PI_2;
    val[60] = kxas;
    val[61] = (a != 0) ? (kxas * da)/a : 0;
    val[62] = xafc;
    val[63] = xafhz;
    val[64] = (xafc != 0) ? 100*dxafc/xafc : 0;
    val[65] = etar;
    val[66] = etar2;
    val[67] = detar2;


    switch_plot_data_set(opq, n_dsqya);

    alternate_lorentian_fit_acq_spe_with_error(dsqya->yd, nx, opq->dy, fb, fm,
					       w_flag, caml->cam_fil, caml->factor, caml->frame,
					       &yafc, &a, &dyafc, &da, &chisq, &dya0);
    y2as = a * M_PI_2;
    yafhz = yafc /(nxx*dt);
    kyas = (1.38e-11 * 298)/y2as;


    opd = do_fft_and_derivate_on_op_2(opq, flow, fm, w_flag+(caml->cam_fil<<1), yfc,
				      caml->frame, caml->factor, &etar2, &detar2);
    free_one_plot(opd);
    detar2 = (etar2 != 0) ? detar2/etar2 : detar2;

    val[68] = y2as;
    val[69] = da * M_PI_2;
    val[70] = kyas;
    val[71] = (a != 0) ? (kyas * da)/a : 0;
    val[72] = yafc;
    val[73] = yafhz;
    val[74] = (yafc != 0) ? 100*dyafc/yafc : 0;
    val[75] = etar;
    val[76] = etar2;
    val[77] = detar2;

    remove_ds_from_op(opq, dsqxa);
    remove_ds_from_op(opq, dsqya);

    add_data_to_multi_d_s(mds, val);

    return mds;
}


multi_d_s *do_full_new_treat_x_y_z_acq_spe_op_pico(O_p *opq, multi_d_s *mds, cam_cor *caml, int w_flag, float high_fc_ratio, int fbm, ref_pos *bd_du)
{
    int i, j;
    char *file = NULL, *cs = NULL, basename[256] = {0};
    d_s  *dsqx = NULL, *dsqy = NULL, *dsqz = NULL, *dsqzo = NULL, *dsqxa = NULL, *dsqya = NULL;
    O_p *opd = NULL;
    static int fm = 1024, fb = 10, nx, n_dsqxa, n_dsqya;
    int nxx, nxy, nxz, is_cyclope_acq = 0, flow;
    float xm, x2, x4, kxd, rx, dx0;
    float ym, y2, y4, kyd, ry, dy0;
    float zm, z2, z4, kzd, rz, dz0;
    float x2s, xfc, xfhz, kxs, dxfc;
    float y2s, yfc, yfhz, kys, dyfc;
    float z2s, zfc, zfhz, kzs, dzfc;
    float x2as, xafc, xafhz, kxas, dxafc, dxa0;
    float y2as, yafc, yafhz, kyas, dyafc, dya0;
    //float zmin, zmin_ref;
    double co, si, the, tmp, y21;
    float xbr = 0 , ybr = 0, zbr = 0, xpr = 0, ypr = 0, zpr = 0;
    float xb0 = 0 , yb0 = 0, zb0 = 0, xp0 = 0, yp0 = 0, zp0 = 0;
    float a = .133, rot = 0, fox, foy;
    int  file_index = 0; //    file_index_found = 0,
    float l, anisotropy = 0, angle = 0, dt, dz;
    float zmag, zobj = 0;
    float bd_x = 0,bd_y = 0,bd_z = 0;
    float du_x = 0,du_y = 0,du_z = 0;
    float da, chisq, val[128], etar, etar2, detar2;




    dsqx = opq->dat[0];    dsqy = opq->dat[1];    dsqz = opq->dat[2];
    dt = opq->dx;
    dz = opq->dy;


    i = 0;
    cs = strstr(dsqx->source,"Bead xc =");  /* we don't care */
     if (cs != NULL)
     {
         i = sscanf(cs,"Bead xc = %f yc = %f zc = %f",&bd_x,&bd_y,&bd_z);
     }
    cs = strstr(dsqx->source,"dust xc =");
     if (cs != NULL)
     {
         i = sscanf(cs,"dust xc = %f yc = %f zc = %f",&du_x,&du_y,&du_z);

     }

    cs = strstr(dsqx->source,"Rotation");
     if (cs != NULL)
     {
         i = sscanf(cs,"Rotation %f",&rot);
        if (i != 1)    win_printf("cannot find rot");
     }
    cs = strstr(dsqx->source,"Z magnets ");
     if (cs != NULL)
     {
         i = sscanf(cs,"Z magnets %f",&zmag);
        if (i != 1)    win_printf("cannot find zmag");
     }
    cs = strstr(dsqx->source,"Z_{obj} = ");
     if (cs != NULL)
     {
         i = sscanf(cs,"Z_{obj} = %f",&zobj);
        if (i != 1)    win_printf("cannot find zobj");
     }
    zobj /= dz;
    display_title_message("%s Z_{obj} = %f",opq->filename,zobj);

    fm = (high_fc_ratio <= 1 && high_fc_ratio > 0) ? (int)(high_fc_ratio*dsqx->nx/2) : dsqx->nx/2;
    fb = (fbm < dsqx->nx/2) ? fbm : 8;
    file = opq->filename;
    //title = opq->title;
    for (i = 0; file[i] && file[i] != '.'; i++);
    //file_index_found = sscanf(file + (((i - 3) >= 0) ? i - 3 : 0),"%d",&file_index);
    strcpy(basename,file);
    basename[(((i - 3) >= 0) ? i - 3 : 0)] = 0;

    if (mds == NULL)
      mds = create_mds_retreat_acq(basename);
    if (mds == NULL)  return NULL;

    val[0] = mds->n;
    val[1] = file_index;
    val[2] = rot;
    val[3] = zmag;
    val[4] = dsqx->nx;


    switch_plot_data_set(opq, 0);
    mean_y2_on_op(opq, w_flag, &xm, &x2, &x4, &kxd, &nxx);
    rx = (x2 != 0) ? x4/(x2*x2) : 0;


    switch_plot_data_set(opq, 1);
    mean_y2_on_op(opq, w_flag, &ym, &y2, &y4, &kyd, &nxy);
    ry = (y2 != 0) ? y4/(y2*y2) : 0;

     switch_plot_data_set(opq, 2);
    mean_y2_on_op(opq, w_flag, &zm, &z2, &z4, &kzd, &nxz);
    rz = (z2 != 0) ? z4/(z2*z2) : 0;

    switch_plot_data_set(opq, 0);
    alternate_lorentian_fit_acq_spe_with_error(dsqx->yd, dsqx->ny, opq->dy, fb, fm,
					       w_flag, caml->cam_fil, caml->factor, caml->frame,
					       &xfc, &a, &dxfc, &da, &chisq, &dx0);
    x2s = a * M_PI_2;
    xfhz = xfc /(nxx*dt);
    kxs = (1.38e-11 * 298)/x2s;

    etar = kxs*nxx*dt/(12*M_PI*M_PI*xfc);
    flow = (int)(2*xfc);
    flow = (flow > nxx/4) ? nxx/4 : flow;
    opd = do_fft_and_derivate_on_op_2(opq, flow, fm, w_flag+(caml->cam_fil<<1), xfc,
				      caml->frame, caml->factor, &etar2, &detar2);
    free_one_plot(opd);
    detar2 = (etar2 != 0) ? detar2/etar2 : detar2;

    val[5] = xm;
    val[6] = dx0;
    val[7] = x2s;
    val[8] = da * M_PI_2;
    val[9] = rx;
    val[10] = du_x; //ref x;
    val[11] = du_x - bd_du->du_x0; //ref shift;
    val[12] = kxs;
    val[13] = (a != 0) ? (kxs * da)/a : 0;
    val[14] = xfc;
    val[15] = xfhz;
    val[16] = (xfc != 0) ? 100*dxfc/xfc : 0;
    val[17] = etar;
    val[18] = etar2;
    val[19] = detar2;

    switch_plot_data_set(opq, 1);
    alternate_lorentian_fit_acq_spe_with_error(dsqy->yd, dsqy->ny, opq->dy, fb, fm,
					       w_flag,  caml->cam_fil, caml->factor, caml->frame,
					       &yfc, &a, &dyfc, &da, &chisq, &dy0);
    y2s = a * M_PI_2;
    yfhz = yfc /(nxy*dt);
    kys = (1.38e-11 * 298)/y2s;

    etar = kys*nxy*dt/(12*M_PI*M_PI*yfc);
    flow = (int)(2*yfc);
    flow = (flow > nxy/4) ? nxy/4 : flow;
    opd = do_fft_and_derivate_on_op_2(opq, flow, fm, w_flag+(caml->cam_fil<<1), xfc,
				      caml->frame, caml->factor, &etar2, &detar2);
    free_one_plot(opd);
    detar2 = (etar2 != 0) ? detar2/etar2 : detar2;

    val[20] = ym;
    val[21] = dy0;
    val[22] = y2s;
    val[23] = da * M_PI_2;
    val[24] = ry;
    val[25] = du_y; //ref x;
    val[26] = du_y - bd_du->du_y0; //ref shift;
    val[27] = kys;
    val[28] = (a != 0) ? (kys * da)/a : 0;
    val[29] = yfc;
    val[30] = yfhz;
    val[31] = (yfc != 0) ? 100*dyfc/yfc : 0;
    val[32] = etar;
    val[33] = etar2;
    val[34] = detar2;


    switch_plot_data_set(opq, 2);
    alternate_lorentian_fit_acq_spe_with_error(dsqz->yd, dsqz->ny, opq->dy, fb, fm,
					       w_flag, caml->cam_fil, caml->factor, caml->frame,
					       &zfc, &a, &dzfc, &da, &chisq, &dz0);
    z2s = a * M_PI_2;
    zfhz = zfc /(nxz*dt);
    kzs = (1.38e-11 * 298)/z2s;

    etar = kzs*nxz*dt/(12*M_PI*M_PI*zfc);
    flow = (int)(2*zfc);
    flow = (flow > nxz/4) ? nxz/4 : flow;
    opd = do_fft_and_derivate_on_op_2(opq, flow, fm, w_flag+(caml->cam_fil<<1), xfc,
				      caml->frame, caml->factor, &etar2, &detar2);
    free_one_plot(opd);
    detar2 = (etar2 != 0) ? detar2/etar2 : detar2;

    val[35] = zm;
    val[36] = dz0;
    val[37] = z2s;
    val[38] = da * M_PI_2;
    val[39] = rz;
    val[40] = du_z; //ref x;
    val[41] = du_z - bd_du->du_z0; //ref shift;
    val[42] = kzs;
    val[43] = (a != 0) ? (kzs * da)/a : 0;
    val[44] = zfc;
    val[45] = zfhz;
    val[46] = (zfc != 0) ? 100*dzfc/zfc : 0;
    val[47] = etar;
    val[48] = etar2;
    val[49] = detar2;



    for (j = 0; is_cyclope_acq == 1 && j < dsqz->nx; j++)
        dsqz->yd[j] =  dsqz->yd[j] + zobj - dsqzo->yd[j];


    xb0 = xm;
    yb0 = ym;
    //zmin = zb0 = zm;
    xbr = bd_du->bd_x0;
    ybr = bd_du->bd_y0;
    //zmin_ref = zbr = 0;
    xp0 = du_x;
    yp0 = du_y;
    zp0 = du_z;
    xpr = bd_du->du_x0;
    ypr = bd_du->du_y0;
    zpr = bd_du->du_z0;

    a =   .878;

    l = sqrt((xb0-xbr-xp0+xpr)*(xb0-xbr-xp0+xpr)+(yb0-ybr-yp0+ypr)*
	     (yb0-ybr-yp0+ypr)+((zb0-zbr-zp0+zpr)*(zb0-zbr-zp0+zpr)*a*a));
    l = zb0*a;  // (zb0-zbr-zp0+zpr)*a;
    fox = l * kxs * 1e6;
    foy = l * kys * 1e6;

    val[50] = l;
    val[51] = fox;
    val[52] = foy;


    nx = dsqx->nx;
    if ((dsqxa = create_and_attach_one_ds(opq, nx, nx, 0)) == NULL)
    {
         win_printf("cannot create ds!");
        return NULL;
    }
    n_dsqxa = opq->n_dat;

    if ((dsqya = create_and_attach_one_ds(opq, nx, nx, 0)) == NULL)
    {
         win_printf("cannot create ds!");
        return NULL;
    }
    n_dsqya = opq->n_dat;

    the = M_PI/4;
    co = cos(the);		si = sin(the);
    for (i=0 ; i< nx ; i++)
      {
	dsqxa->yd[i] = co * (dsqx->yd[i] - xm) + si *  (dsqy->yd[i] - ym);
	dsqya->yd[i] =  -si * (dsqx->yd[i] - xm) + co  * (dsqy->yd[i] - ym);
      }

    alternate_lorentian_fit_acq_spe_with_error(dsqxa->yd, nx, opq->dy, fb, fm,
					       w_flag, caml->cam_fil, caml->factor, caml->frame,
					       &xafc, &a, &dxafc, &da, &chisq, &dxa0);
    x2as = a * M_PI_2;
    xafhz = xafc /(nxx*dt);
    kxas = (1.38e-11 * 298)/x2as;


    alternate_lorentian_fit_acq_spe_with_error(dsqya->yd, nx, opq->dy, fb, fm,
					       w_flag, caml->cam_fil, caml->factor, caml->frame,
					       &yafc, &a, &dyafc, &da, &chisq, &dya0);
    y2as = a * M_PI_2;
    yafhz = yafc /(nxx*dt);
    kxas = (1.38e-11 * 298)/y2as;


    co = (x2s - y2s)/2;
    si = (x2as - y2as)/2;
    tmp = (x2s + y2s)/2;
    y21 = (tmp != 0) ? sqrt(co*co+si*si)/tmp : 0;
    the = atan2(si,co)/2;
    anisotropy = 100*y21;
    angle = 180*the/M_PI;
    co = cos(the);		si = sin(the);
    for (i=0 ; i< nx ; i++)
      {
	dsqxa->xd[i] = xm + co * (dsqx->yd[i] - xm) + si * (dsqy->yd[i] - ym);
	dsqya->yd[i] = ym -si * (dsqx->yd[i] - xm) + co * (dsqy->yd[i] - ym);
      }

    val[53] = anisotropy;
    val[54] = angle;

    a =   .878;
    l = zb0*a;  // (zb0-zbr-zp0+zpr)*a;
    fox = l * kxs * 1e6;
    foy = l * kys * 1e6;

    val[55] = l;
    val[56] = fox;
    val[57] = foy;

    switch_plot_data_set(opq, n_dsqxa);
    alternate_lorentian_fit_acq_spe_with_error(dsqxa->yd, nx, opq->dy, fb, fm,
					       w_flag, caml->cam_fil, caml->factor, caml->frame,
					       &xafc, &a, &dxafc, &da, &chisq, &dxa0);
    x2as = a * M_PI_2;
    xafhz = xafc /(nxx*dt);
    kxas = (1.38e-11 * 298)/x2as;


    opd = do_fft_and_derivate_on_op_2(opq, flow, fm, w_flag+(caml->cam_fil<<1), xfc,
				      caml->frame, caml->factor, &etar2, &detar2);
    free_one_plot(opd);
    detar2 = (etar2 != 0) ? detar2/etar2 : detar2;

    val[58] = x2as;
    val[59] = da * M_PI_2;
    val[60] = kxas;
    val[61] = (a != 0) ? (kxas * da)/a : 0;
    val[62] = xafc;
    val[63] = xafhz;
    val[64] = (xafc != 0) ? 100*dxafc/xafc : 0;
    val[65] = etar;
    val[66] = etar2;
    val[67] = detar2;


    switch_plot_data_set(opq, n_dsqya);

    alternate_lorentian_fit_acq_spe_with_error(dsqya->yd, nx, opq->dy, fb, fm,
					       w_flag, caml->cam_fil, caml->factor, caml->frame,
					       &yafc, &a, &dyafc, &da, &chisq, &dya0);
    y2as = a * M_PI_2;
    yafhz = yafc /(nxx*dt);
    kyas = (1.38e-11 * 298)/y2as;


    opd = do_fft_and_derivate_on_op_2(opq, flow, fm, w_flag+(caml->cam_fil<<1), yfc,
				      caml->frame, caml->factor, &etar2, &detar2);
    free_one_plot(opd);
    detar2 = (etar2 != 0) ? detar2/etar2 : detar2;

    val[68] = y2as;
    val[69] = da * M_PI_2;
    val[70] = kyas;
    val[71] = (a != 0) ? (kyas * da)/a : 0;
    val[72] = yafc;
    val[73] = yafhz;
    val[74] = (yafc != 0) ? 100*dyafc/yafc : 0;
    val[75] = etar;
    val[76] = etar2;
    val[77] = detar2;

    remove_ds_from_op(opq, dsqxa);
    remove_ds_from_op(opq, dsqya);

    add_data_to_multi_d_s(mds, val);

    return mds;
}


O_p  *prepare_L_versus_Zmag(pltreg  *prn, multi_d_s *mds, ref_pos *bd_du)
{
  int i;
  O_p *op = NULL;
  d_s *ds = NULL;
  char path[512];
  int ix, iy, nx = 1024;
  char *buf = NULL;

    if ((op = create_and_attach_one_plot(prn, nx, nx, 0)) == NULL)
      {
	win_printf("cannot create plot !");
        return NULL;
      }


    ds = op->dat[0];
    ds->source = my_sprintf(ds->source,"DNA extension versus Z magnet curve \n"
			    "xbr = %g ybr = %g zbr = %g \n"
			    " xpr = %g ypr = %g zpr = %g ",
			    bd_du->bd_x0,bd_du->bd_y0,bd_du->bd_z0,
			    bd_du->du_x0,bd_du->du_y0,bd_du->du_z0 );
    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_plot_symb(ds, "\\pt6\\oc");


  ix = retrieve_and_check_mds_index_for(mds, "l = z");
  iy = retrieve_and_check_mds_index_for(mds, "Mag Z");

  for (i = 0 ; i < mds->n; i++)
    {
      ds->yd[i] = mds->x[iy][i];
      ds->xd[i] = mds->x[ix][i];
    }
  ds->nx = ds->ny = i;
  if ((alloc_data_set_x_error(ds) == NULL) || (ds->xe == NULL))
    {
      win_printf_OK("I can't create errors 3!");
      return NULL;
    }

  ix = retrieve_and_check_mds_index_for(mds, "Error on <z>");

  for (i = 0 ; i < mds->n; i++)
      ds->xe[i] = mds->x[ix][i];


    extract_file_path(path, 512, mds->source);
    set_plot_title(op, "Sample");
    set_plot_x_title(op, "Allongement (\\mu m)");
    set_plot_y_title(op, "Magnets distance");
    set_plot_file(op,"lzmag.gr");
    set_plot_path(op,"%s",path);
    find_x_limits(op);
    find_y_limits(op);
  buf = create_description_label(op, 8);
  set_op_plot_label(op, op->x_lo + (op->x_hi - op->x_lo)/4,
		    op->y_hi - (op->y_hi - op->y_lo)/4, USR_COORD, "%s", buf);
  free(buf);

    return op;
}
O_p  *prepare_fc_versus_L(pltreg  *prn, multi_d_s *mds, ref_pos *bd_du)
{
  int i = 0;
  O_p *op = NULL;
  d_s *ds = NULL, *ds2 = NULL;
  int ix, iy, nx = 1024;
  char *buf = NULL;

  if ((op = create_and_attach_one_plot(prn, nx, nx, 0)) == NULL)
    {
      win_printf("cannot create plot !");
      return NULL;
    }


  ds = op->dat[0];
  ds->source = my_sprintf(ds->source,"fcx versus DNA extension l = lz\n"
			    "xbr = %g ybr = %g zbr = %g \n"
			    " xpr = %g ypr = %g zpr = %g ",
			    bd_du->bd_x0,bd_du->bd_y0,bd_du->bd_z0,
			    bd_du->du_x0,bd_du->du_y0,bd_du->du_z0);
  ds->nx = ds->ny = 0;
  set_dot_line(ds);
  set_plot_symb(ds, "\\pt6\\oc");

  ix = retrieve_and_check_mds_index_for(mds, "l = z");
  iy = retrieve_and_check_mds_index_for(mds, "f_{cx} (Hz)");


  mds->name[i++] = strdup("df_{cx}/f_{cx} (%)");


  for (i = 0 ; i < mds->n; i++)
    {
      ds->yd[i] = mds->x[iy][i];
      ds->xd[i] = mds->x[ix][i];
    }
  ds->nx = ds->ny = i;

  if ((alloc_data_set_y_error(ds) == NULL) || (ds->ye == NULL))
    {
      win_printf_OK("I can't create errors 4!");
      return NULL;
    }

  ix = retrieve_and_check_mds_index_for(mds,"df_{cx}/f_{cx} (%)");

  for (i = 0 ; i < mds->n; i++)
      ds->ye[i] = (mds->x[iy][i]*ds->yd[i])/100;



  ds2 = create_and_attach_one_ds(op, nx, nx, 0);
  if (ds2 == NULL)
    return (O_p *)win_printf_ptr("cannot find stuff in acreg!");

  ds2->source = my_sprintf(ds2->source,"fcy versus DNA extension l = lz\n"
			   "xbr = %g ybr = %g zbr = %g \n"
			   " xpr = %g ypr = %g zpr = %g ",
			   bd_du->bd_x0,bd_du->bd_y0,bd_du->bd_z0,
			   bd_du->du_x0,bd_du->du_y0,bd_du->du_z0);
  ds2->nx = ds2->ny = 0;
  set_dot_line(ds2);
  set_plot_symb(ds2, "\\pt6\\oc");

  ix = retrieve_and_check_mds_index_for(mds, "l = z");
  iy = retrieve_and_check_mds_index_for(mds, "f_{cy} (Hz)");

  for (i = 0 ; i < mds->n; i++)
    {
      ds2->yd[i] = mds->x[iy][i];
      ds2->xd[i] = mds->x[ix][i];
    }
  ds2->nx = ds2->ny = i;
  if ((alloc_data_set_y_error(ds2) == NULL) || (ds2->ye == NULL))
    {
      win_printf_OK("I can't create errors 5!");
      return NULL;
    }

  iy = retrieve_and_check_mds_index_for(mds,"df_{cy}/f_{cy} (%)");

  for (i = 0 ; i < mds->n; i++)
      ds2->ye[i] = (mds->x[iy][i]*ds2->yd[i])/100;


  ds2 = create_and_attach_one_ds(op, nx, nx, 0);
  if (ds2 == NULL)
    return (O_p *)win_printf_ptr("cannot find stuff in acreg!");

  ds2->source = my_sprintf(ds2->source,"fcz versus DNA extension l = lz\n"
			   "xbr = %g ybr = %g zbr = %g \n"
			   " xpr = %g ypr = %g zpr = %g ",
			   bd_du->bd_x0,bd_du->bd_y0,bd_du->bd_z0,
			   bd_du->du_x0,bd_du->du_y0,bd_du->du_z0);
  ds2->nx = ds2->ny = 0;
  set_dot_line(ds2);
  set_plot_symb(ds2, "\\pt6\\oc");

  ix = retrieve_and_check_mds_index_for(mds, "l = z");
  iy = retrieve_and_check_mds_index_for(mds, "f_{cz} (Hz)");

  for (i = 0 ; i < mds->n; i++)
    {
      ds2->yd[i] = mds->x[iy][i];
      ds2->xd[i] = mds->x[ix][i];
    }
  ds2->nx = ds2->ny = i;
  if ((alloc_data_set_y_error(ds2) == NULL) || (ds2->ye == NULL))
    {
      win_printf_OK("I can't create errors 6!");
      return NULL;
    }

  iy = retrieve_and_check_mds_index_for(mds,"df_{cz}/f_{cz} (%)");

  for (i = 0 ; i < mds->n; i++)
      ds2->ye[i] = (mds->x[iy][i]*ds2->yd[i])/100;



    set_plot_title(op, "Sample");
    set_plot_x_title(op, "Allongement l = l_z (\\mu m)");
    set_plot_y_title(op, "Cuttoff frequency f_c");
    set_plot_file(op,"fcvsl.gr");
    set_plot_path(op,"%s", prn->path);
    find_x_limits(op);
    find_y_limits(op);
  buf = create_description_label(op, 8);
  set_op_plot_label(op, op->x_lo + (op->x_hi - op->x_lo)/4,
		    op->y_hi - (op->y_hi - op->y_lo)/4, USR_COORD, "%s", buf);
  free(buf);


    return op;
}
O_p  *prepare_etar_versus_L(pltreg  *prn, multi_d_s *mds, ref_pos *bd_du)
{
  int i = 0;
  O_p *op = NULL;
  d_s *ds = NULL, *ds2 = NULL;
  int ix, iy, nx = 1024;
  char *buf = NULL;

  if ((op = create_and_attach_one_plot(prn, nx, nx, 0)) == NULL)
    {
      win_printf("cannot create plot !");
      return NULL;
    }


  ds = op->dat[0];
  ds->source = my_sprintf(ds->source,"\\eta.r_x versus DNA extension l = l_z\n"
			    "xbr = %g ybr = %g zbr = %g \n"
			    " xpr = %g ypr = %g zpr = %g ",
			    bd_du->bd_x0,bd_du->bd_y0,bd_du->bd_z0,
			    bd_du->du_x0,bd_du->du_y0,bd_du->du_z0);
  ds->nx = ds->ny = 0;
  set_dot_line(ds);
  set_plot_symb(ds, "\\pt6\\oc");

  ix = retrieve_and_check_mds_index_for(mds, "l = z");
  iy = retrieve_and_check_mds_index_for(mds, "\\eta_x .r (k/6\\pi\\omega_c)");



  for (i = 0 ; i < mds->n; i++)
    {
      ds->yd[i] = 1000*mds->x[iy][i];
      ds->xd[i] = mds->x[ix][i];
    }
  ds->nx = ds->ny = i;

  if ((alloc_data_set_y_error(ds) == NULL) || (ds->ye == NULL))
    {
      win_printf_OK("I can't create errors 7!");
      return NULL;
    }

  ix = retrieve_and_check_mds_index_for(mds,"df_{cx}/f_{cx} (%)");

  for (i = 0 ; i < mds->n; i++)
      ds->ye[i] = (mds->x[iy][i]*ds->yd[i])/100;


  ds2 = create_and_attach_one_ds(op, nx, nx, 0);
  if (ds2 == NULL)
    return (O_p *) win_printf_ptr("cannot find stuff in acreg!");

  ds2->source = my_sprintf(ds2->source,"\\eta.r_y versus DNA extension l = l_z\n"
			   "xbr = %g ybr = %g zbr = %g \n"
			   " xpr = %g ypr = %g zpr = %g ",
			   bd_du->bd_x0,bd_du->bd_y0,bd_du->bd_z0,
			   bd_du->du_x0,bd_du->du_y0,bd_du->du_z0);
  ds2->nx = ds2->ny = 0;
  set_dot_line(ds2);
  set_plot_symb(ds2, "\\pt6\\oc");

  ix = retrieve_and_check_mds_index_for(mds, "l = z");
  iy = retrieve_and_check_mds_index_for(mds, "\\eta_y .r (k/6\\pi\\omega_c)");

  for (i = 0 ; i < mds->n; i++)
    {
      ds2->yd[i] = 1000 * mds->x[iy][i];
      ds2->xd[i] = mds->x[ix][i];
    }
  ds2->nx = ds2->ny = i;
  if ((alloc_data_set_y_error(ds2) == NULL) || (ds2->ye == NULL))
    {
      win_printf_OK("I can't create errors 8!");
      return NULL;
    }

  iy = retrieve_and_check_mds_index_for(mds,"df_{cy}/f_{cy} (%)");

  for (i = 0 ; i < mds->n; i++)
      ds2->ye[i] = (mds->x[iy][i]*ds2->yd[i])/100;


  ds2 = create_and_attach_one_ds(op, nx, nx, 0);
  if (ds2 == NULL)
    return (O_p *) win_printf_ptr("cannot find stuff in acreg!");

  ds2->source = my_sprintf(ds2->source,"\\eta.r_z versus DNA extension l = l_z\n"
			   "xbr = %g ybr = %g zbr = %g \n"
			   " xpr = %g ypr = %g zpr = %g ",
			   bd_du->bd_x0,bd_du->bd_y0,bd_du->bd_z0,
			   bd_du->du_x0,bd_du->du_y0,bd_du->du_z0);
  ds2->nx = ds2->ny = 0;
  set_dot_line(ds2);
  set_plot_symb(ds2, "\\pt6\\oc");

  ix = retrieve_and_check_mds_index_for(mds, "l = z");
  iy = retrieve_and_check_mds_index_for(mds,  "\\eta_z .r (k/6\\pi\\omega_c)");

  for (i = 0 ; i < mds->n; i++)
    {
      ds2->yd[i] = 1000*mds->x[iy][i];
      ds2->xd[i] = mds->x[ix][i];
    }
  ds2->nx = ds2->ny = i;
  if ((alloc_data_set_y_error(ds2) == NULL) || (ds2->ye == NULL))
    {
      win_printf_OK("I can't create errors 9!");
      return NULL;
    }

  iy = retrieve_and_check_mds_index_for(mds,"df_{cz}/f_{cz} (%)");

  for (i = 0 ; i < mds->n; i++)
      ds2->ye[i] = (mds->x[iy][i]*ds2->yd[i])/100;


  ds = create_and_attach_one_ds(op, nx, nx, 0);
  if (ds == NULL)
    return (O_p *) win_printf_ptr("cannot find stuff in acreg!");

  ds->source = my_sprintf(ds->source,"\\eta.r_x versus DNA extension spe l = l_z\n"
			    "xbr = %g ybr = %g zbr = %g \n"
			    " xpr = %g ypr = %g zpr = %g ",
			    bd_du->bd_x0,bd_du->bd_y0,bd_du->bd_z0,
			    bd_du->du_x0,bd_du->du_y0,bd_du->du_z0);
  ds->nx = ds->ny = 0;
  set_dot_line(ds);
  set_plot_symb(ds, "\\pt6\\oc");

  ix = retrieve_and_check_mds_index_for(mds, "l = z");
  iy = retrieve_and_check_mds_index_for(mds, "\\eta_x .r (dx/dt)");



  for (i = 0 ; i < mds->n; i++)
    {
      ds->yd[i] = 1000*mds->x[iy][i];
      ds->xd[i] = mds->x[ix][i];
    }
  ds->nx = ds->ny = i;

  if ((alloc_data_set_y_error(ds) == NULL) || (ds->ye == NULL))
    {
      win_printf_OK("I can't create errors 10!");
      return NULL;
    }

  iy = retrieve_and_check_mds_index_for(mds,"Error \\eta_x .r (dx/dt)");

  for (i = 0 ; i < mds->n; i++)
      ds->ye[i] = ds->yd[i]*mds->x[iy][i];



  ds2 = create_and_attach_one_ds(op, nx, nx, 0);
  if (ds2 == NULL)
    return (O_p *) win_printf_ptr("cannot find stuff in acreg!");

  ds2->source = my_sprintf(ds2->source,"\\eta.r_y versus DNA extension spe l = l_z\n"
			   "xbr = %g ybr = %g zbr = %g \n"
			   " xpr = %g ypr = %g zpr = %g ",
			   bd_du->bd_x0,bd_du->bd_y0,bd_du->bd_z0,
			   bd_du->du_x0,bd_du->du_y0,bd_du->du_z0);
  ds2->nx = ds2->ny = 0;
  set_dot_line(ds2);
  set_plot_symb(ds2, "\\pt6\\oc");

  ix = retrieve_and_check_mds_index_for(mds, "l = z");
  iy = retrieve_and_check_mds_index_for(mds, "\\eta_y .r (dx/dt)");

  for (i = 0 ; i < mds->n; i++)
    {
      ds2->yd[i] = 1000*mds->x[iy][i];
      ds2->xd[i] = mds->x[ix][i];
    }
  ds2->nx = ds2->ny = i;
  if ((alloc_data_set_y_error(ds2) == NULL) || (ds2->ye == NULL))
    {
      win_printf_OK("I can't create errors 11!");
      return NULL;
    }

  iy = retrieve_and_check_mds_index_for(mds,"Error \\eta_y .r (dx/dt)");

  for (i = 0 ; i < mds->n; i++)
      ds2->ye[i] = ds2->yd[i]*mds->x[iy][i];



  ds2 = create_and_attach_one_ds(op, nx, nx, 0);
  if (ds2 == NULL)
    return (O_p *) win_printf_ptr("cannot find stuff in acreg!");

  ds2->source = my_sprintf(ds2->source,"\\eta.r_z versus DNA extension spe\n"
			   "xbr = %g ybr = %g zbr = %g \n"
			   " xpr = %g ypr = %g zpr = %g ",
			   bd_du->bd_x0,bd_du->bd_y0,bd_du->bd_z0,
			   bd_du->du_x0,bd_du->du_y0,bd_du->du_z0);
  ds2->nx = ds2->ny = 0;
  set_dot_line(ds2);
  set_plot_symb(ds2, "\\pt6\\oc");

  ix = retrieve_and_check_mds_index_for(mds, "l = z");
  iy = retrieve_and_check_mds_index_for(mds, "\\eta_z .r (dx/dt)");

  for (i = 0 ; i < mds->n; i++)
    {
      ds2->yd[i] = 1000*mds->x[iy][i];
      ds2->xd[i] = mds->x[ix][i];
    }
  ds2->nx = ds2->ny = i;
  if ((alloc_data_set_y_error(ds2) == NULL) || (ds2->ye == NULL))
    {
      win_printf_OK("I can't create errors 12!");
      return NULL;
    }

  iy = retrieve_and_check_mds_index_for(mds,"Error \\eta_z .r (dx/dt)");

  for (i = 0 ; i < mds->n; i++)
      ds2->ye[i] = ds2->yd[i]*mds->x[iy][i];

    set_plot_title(op, "Sample");
    set_plot_x_title(op, "Allongement (\\mu m)");
    set_plot_y_title(op, "Viscous drag \\eta r (x 1000)");
    set_plot_file(op,"etarvsl.gr");
    set_plot_path(op,"%s",prn->path);
    find_x_limits(op);
    find_y_limits(op);
    buf = create_description_label(op, 8);
    set_op_plot_label(op, op->x_lo + (op->x_hi - op->x_lo)/4,
		      op->y_hi - (op->y_hi - op->y_lo)/4, USR_COORD, "%s", buf);
    free(buf);
   return op;
}
O_p  *prepare_anisotropy_versus_L(pltreg  *prn, multi_d_s *mds, ref_pos *bd_du)
{
  int i = 0;
  O_p *op = NULL;
  d_s *ds = NULL;
  int ix, iy, nx = 1024;
  char *buf = NULL;

  if ((op = create_and_attach_one_plot(prn, nx, nx, 0)) == NULL)
    {
      win_printf("cannot create plot !");
      return NULL;
    }


  ds = op->dat[0];
  ds->source = my_sprintf(ds->source,"anisotropy versus DNA extension \n"
			    "xbr = %g ybr = %g zbr = %g \n"
			    " xpr = %g ypr = %g zpr = %g ",
			    bd_du->bd_x0,bd_du->bd_y0,bd_du->bd_z0,
			    bd_du->du_x0,bd_du->du_y0,bd_du->du_z0);
  ds->nx = ds->ny = 0;
  set_dot_line(ds);
  set_plot_symb(ds, "\\pt6\\oc");

  ix = retrieve_and_check_mds_index_for(mds, "l = z");
  iy = retrieve_and_check_mds_index_for(mds, "anisotropy");



  for (i = 0 ; i < mds->n; i++)
    {
      ds->yd[i] = mds->x[iy][i];
      ds->xd[i] = mds->x[ix][i];
    }
  ds->nx = ds->ny = i;

    set_plot_title(op, "Sample %s",prn->filename);
    set_plot_x_title(op, "Allongement (\\mu m)");
    set_plot_y_title(op, "Anisotropy");
    set_plot_file(op,"anisotropy.gr");
    set_plot_path(op,"%s",prn->path);
    find_x_limits(op);
    find_y_limits(op);
  buf = create_description_label(op, 8);
  set_op_plot_label(op, op->x_lo + (op->x_hi - op->x_lo)/4,
		    op->y_hi - (op->y_hi - op->y_lo)/4, USR_COORD, "%s", buf);
  free(buf);
    return op;
}
O_p  *prepare_angle_versus_L(pltreg  *prn, multi_d_s *mds, ref_pos *bd_du)
{
  int i = 0;
  O_p *op = NULL;
  d_s *ds = NULL;
  int ix, iy, nx = 1024;
  char *buf = NULL;

  if ((op = create_and_attach_one_plot(prn, nx, nx, 0)) == NULL)
    {
      win_printf("cannot create plot !");
      return NULL;
    }


  ds = op->dat[0];
  ds->source = my_sprintf(ds->source,"angle versus DNA extension \n"
			    "xbr = %g ybr = %g zbr = %g \n"
			    " xpr = %g ypr = %g zpr = %g ",
			    bd_du->bd_x0,bd_du->bd_y0,bd_du->bd_z0,
			    bd_du->du_x0,bd_du->du_y0,bd_du->du_z0);
  ds->nx = ds->ny = 0;
  set_dot_line(ds);
  set_plot_symb(ds, "\\pt6\\oc");

  ix = retrieve_and_check_mds_index_for(mds, "l = z");
  iy = retrieve_and_check_mds_index_for(mds, "angle");



  for (i = 0 ; i < mds->n; i++)
    {
      ds->yd[i] = mds->x[iy][i];
      ds->xd[i] = mds->x[ix][i];
    }
  ds->nx = ds->ny = i;

    set_plot_title(op, "Sample %s",prn->filename);
    set_plot_x_title(op, "Allongement (\\mu m)");
    set_plot_y_title(op, "Angle");
    set_plot_file(op,"angle.gr");
    set_plot_path(op,"%s",prn->path);
    find_x_limits(op);
    find_y_limits(op);
    buf = create_description_label(op, 8);
    set_op_plot_label(op, op->x_lo + (op->x_hi - op->x_lo)/4,
		      op->y_hi - (op->y_hi - op->y_lo)/4, USR_COORD, "%s",buf);
  free(buf);

    return op;
}

O_p  *prepare_hat_curve(pltreg  *prn, multi_d_s *mds, ref_pos *bd_du)
{
  int i;
  O_p *op = NULL;
  d_s *ds = NULL, *ds2 = NULL;
  int ix, iy, nx = 1024;
  char *buf = NULL;

  if ((op = create_and_attach_one_plot(prn, nx, nx, 0)) == NULL)
    {
      win_printf("cannot create plot !");
      return NULL;
    }


  ds = op->dat[0];
  ds->source = my_sprintf(ds->source,"DNA extension versus sigma curve l = 3D length\n"
			    "xbr = %g ybr = %g zbr = %g \n"
			    " xpr = %g ypr = %g zpr = %g ",
			    bd_du->bd_x0,bd_du->bd_y0,bd_du->bd_z0,
			    bd_du->du_x0,bd_du->du_y0,bd_du->du_z0);
  ds->nx = ds->ny = 0;
  set_dot_line(ds);
  set_plot_symb(ds, "\\pt6\\oc");

  iy = retrieve_and_check_mds_index_for(mds, "l=\\sqrt{x^2+y^2+z^2}");
  ix = retrieve_and_check_mds_index_for(mds, "Mag rot");

  for (i = 0 ; i < mds->n; i++)
    {
      ds->yd[i] = mds->x[iy][i];
      ds->xd[i] = mds->x[ix][i];
    }
  ds->nx = ds->ny = i;




  ds2 = create_and_attach_one_ds(op, nx, nx, 0);
  if (ds2 == NULL)
    return (O_p *) win_printf_ptr("cannot find stuff in acreg!");

  ds2->source = my_sprintf(ds2->source,"DNA extension versus sigma curve l = z\n"
			   "xbr = %g ybr = %g zbr = %g \n"
			   " xpr = %g ypr = %g zpr = %g ",
			   bd_du->bd_x0,bd_du->bd_y0,bd_du->bd_z0,
			   bd_du->du_x0,bd_du->du_y0,bd_du->du_z0);
  ds2->nx = ds2->ny = 0;
  set_dot_line(ds2);
  set_plot_symb(ds2, "\\pt6\\oc");

  iy = retrieve_and_check_mds_index_for(mds, "l = z");
  ix = retrieve_and_check_mds_index_for(mds, "Mag rot");

  for (i = 0 ; i < mds->n; i++)
    {
      ds2->yd[i] = mds->x[iy][i];
      ds2->xd[i] = mds->x[ix][i];
    }
  ds2->nx = ds2->ny = i;
  if ((alloc_data_set_y_error(ds2) == NULL) || (ds2->ye == NULL))
    {
      win_printf_OK("I can't create errors 13!");
      return NULL;
    }

  iy = retrieve_and_check_mds_index_for(mds, "Error on <z>");

  for (i = 0 ; i < mds->n; i++)
      ds2->ye[i] = mds->x[iy][i];

  set_plot_title(op, "Sample");
  set_plot_y_title(op, "Allongement (\\mu m)");
  set_plot_x_title(op, "mag rot");
  set_plot_file(op,"chapeaux.gr");
  set_plot_path(op,"%s",prn->path);
    find_x_limits(op);
    find_y_limits(op);
  buf = create_description_label(op, 8);
  set_op_plot_label(op, 1, .8, ABS_COORD, "%s", buf);
  free(buf);

  return op;
}

O_p  *prepare_k_versus_Zmag(pltreg  *prn, multi_d_s *mds, ref_pos *bd_du)
{
  int i;
  O_p *op = NULL;
  d_s *ds = NULL, *ds2 = NULL, *ds3 = NULL;
  int ix, iy, nx = 1024;
  char *buf = NULL;

  if ((op = create_and_attach_one_plot(prn, nx, nx, 0)) == NULL)
    {
      win_printf("cannot create plot !");
      return NULL;
    }


  ds = op->dat[0];
  ds->source = my_sprintf(ds->source,"DNA stiffness versus Z mag along X\n"
			  "xbr = %g ybr = %g zbr = %g \n"
			  " xpr = %g ypr = %g zpr = %g ",
			  bd_du->bd_x0,bd_du->bd_y0,bd_du->bd_z0,
			  bd_du->du_x0,bd_du->du_y0,bd_du->du_z0 );
  ds->nx = ds->ny = 0;
  set_dot_line(ds);
  set_plot_symb(ds, "\\pt6\\oc");

  ix = retrieve_and_check_mds_index_for(mds, "k_x");
  iy = retrieve_and_check_mds_index_for(mds, "Mag Z");

  for (i = 0 ; i < mds->n; i++)
    {
      ds->yd[i] = mds->x[iy][i];
      ds->xd[i] = mds->x[ix][i];
    }
  ds->nx = ds->ny = i;
  if ((alloc_data_set_x_error(ds) == NULL) || (ds->xe == NULL))
    {
      win_printf_OK("I can't create errors 14!");
      return NULL;
    }

  ix = retrieve_and_check_mds_index_for(mds, "error on k_x");

  for (i = 0 ; i < mds->n; i++)
      ds->xe[i] = mds->x[ix][i];

  ds2 = create_and_attach_one_ds(op, nx, nx, 0);
  if (ds2 == NULL)
    return (O_p *) win_printf_ptr("cannot find stuff in acreg!");

  ds2->source = my_sprintf(ds2->source,"DNA stiffness versus Z mag along Y\n"
			   "xbr = %g ybr = %g zbr = %g \n"
			   " xpr = %g ypr = %g zpr = %g ",
			   bd_du->bd_x0,bd_du->bd_y0,bd_du->bd_z0,
			   bd_du->du_x0,bd_du->du_y0,bd_du->du_z0);
  ds2->nx = ds2->ny = 0;
  set_dot_line(ds2);
  set_plot_symb(ds2, "\\pt6\\oc");

  ix = retrieve_and_check_mds_index_for(mds, "k_y");
  iy = retrieve_and_check_mds_index_for(mds, "Mag Z");

  for (i = 0 ; i < mds->n; i++)
    {
      ds2->yd[i] = mds->x[iy][i];
      ds2->xd[i] = mds->x[ix][i];
    }
  ds2->nx = ds2->ny = i;
  if ((alloc_data_set_x_error(ds2) == NULL) || (ds2->xe == NULL))
    {
      win_printf_OK("I can't create errors 15!");
      return NULL;
    }

  ix = retrieve_and_check_mds_index_for(mds, "error on k_y");

  for (i = 0 ; i < mds->n; i++)
      ds2->xe[i] = mds->x[ix][i];


  ds3 = create_and_attach_one_ds(op, nx, nx, 0);
  if (ds3 == NULL)
    return (O_p *) win_printf_ptr("cannot find stuff in acreg!");

  ds3->source = my_sprintf(ds3->source,"DNA stiffness versus Z mag curve along Z\n"
			   "xbr = %g ybr = %g zbr = %g \n"
			   " xpr = %g ypr = %g zpr = %g ",
			   bd_du->bd_x0,bd_du->bd_y0,bd_du->bd_z0,
			   bd_du->du_x0,bd_du->du_y0,bd_du->du_z0);
  ds3->nx = ds3->ny = 0;
  set_dot_line(ds3);
  set_plot_symb(ds3, "\\pt6\\oc");

  ix = retrieve_and_check_mds_index_for(mds, "k_z");
  iy = retrieve_and_check_mds_index_for(mds, "Mag Z");

  for (i = 0 ; i < mds->n; i++)
    {
      ds3->yd[i] = mds->x[iy][i];
      ds3->xd[i] = mds->x[ix][i];
    }
  ds3->nx = ds3->ny = i;
  if ((alloc_data_set_x_error(ds3) == NULL) || (ds3->xe == NULL))
    {
      win_printf_OK("I can't create errors 16!");
      return NULL;
    }

  ix = retrieve_and_check_mds_index_for(mds, "error on k_z");

  for (i = 0 ; i < mds->n; i++)
      ds3->xe[i] = mds->x[ix][i];



  set_plot_title(op, "Stiffness in x, y, z vs Z_{mag}");
  set_plot_y_title(op, "Z mag");
  set_plot_x_title(op, "Stiffness (N/m)");
  set_plot_file(op,"ldekz.gr");
  set_plot_path(op,"%s",prn->path);
  set_plot_x_log(op);
  find_x_limits(op);
  find_y_limits(op);
  buf = create_description_label(op, 8);
  set_op_plot_label(op, op->x_lo + (op->x_hi - op->x_lo)/4,
		    op->y_hi - (op->y_hi - op->y_lo)/4, USR_COORD, "%s", buf);
  free(buf);

  //  set_plot_path(op,"%s/%s/",cur_path,name);
  return op;
}
O_p  *prepare_F_of_L(pltreg  *prn, multi_d_s *mds, ref_pos *bd_du)
{
  int i;
  O_p *op = NULL;
  d_s *ds = NULL, *ds2 = NULL;
  int ix, iy, nx = 1024;
  float tmp, tmp1;
  char *buf = NULL;


  if ((op = create_and_attach_one_plot(prn, nx, nx, 0)) == NULL)
    {
      win_printf("cannot create plot !");
      return NULL;
    }


  ds = op->dat[0];
  ds->source = my_sprintf(ds->source,"DNA force extension curve f_x 3D length\n"
			  "xbr = %g ybr = %g zbr = %g \n"
			  " xpr = %g ypr = %g zpr = %g ",
			  bd_du->bd_x0,bd_du->bd_y0,bd_du->bd_z0,
			  bd_du->du_x0,bd_du->du_y0,bd_du->du_z0 );
  ds->nx = ds->ny = 0;
  set_dot_line(ds);
  set_plot_symb(ds, "\\pt6\\oc");

  iy = retrieve_and_check_mds_index_for(mds, "f_{ox}");
  ix = retrieve_and_check_mds_index_for(mds, "l=\\sqrt{x^2+y^2+z^2}");

  for (i = 0 ; i < mds->n; i++)
    {
      ds->yd[i] = mds->x[iy][i];
      ds->xd[i] = mds->x[ix][i];
    }
  ds->nx = ds->ny = i;
  if ((alloc_data_set_x_error(ds) == NULL) || (ds->xe == NULL))
    {
      win_printf_OK("I can't create errors 17!");
      return NULL;
    }

  ix = retrieve_and_check_mds_index_for(mds, "Error on <z>");

  for (i = 0 ; i < mds->n; i++)
      ds->xe[i] = mds->x[ix][i];

  ix = retrieve_and_check_mds_index_for(mds, "k_x");
  iy = retrieve_and_check_mds_index_for(mds, "error on k_x");

  if ((alloc_data_set_y_error(ds) == NULL) || (ds->ye == NULL))
    {
      win_printf_OK("I can't create errors 18!");
      return NULL;
    }
  for (i = 0 ; i < mds->n; i++)
    {
      tmp = mds->x[ix][i];
      tmp = (tmp != 0) ? 1/tmp : 1;
      tmp *= mds->x[iy][i];
      tmp1 = ds->xd[i];
      tmp1 = (tmp1 != 0) ? 1/tmp1 : 1;
      tmp1 *= ds->xe[i];
      tmp = tmp*tmp + tmp1*tmp1;
      tmp = (tmp >= 0) ? sqrt(tmp) : 1;
      ds->ye[i] = ds->yd[i]*tmp;
    }


  ds2 = create_and_attach_one_ds(op, nx, nx, 0);
  if (ds2 == NULL)
    return (O_p *) win_printf_ptr("cannot find stuff in acreg!");

  ds2->source = my_sprintf(ds2->source,"DNA force extension curve f_y 3D length\n"
			   "xbr = %g ybr = %g zbr = %g \n"
			   " xpr = %g ypr = %g zpr = %g ",
			   bd_du->bd_x0,bd_du->bd_y0,bd_du->bd_z0,
			   bd_du->du_x0,bd_du->du_y0,bd_du->du_z0);
  ds2->nx = ds2->ny = 0;
  set_dot_line(ds2);
  set_plot_symb(ds2, "\\pt6\\oc");


  iy = retrieve_and_check_mds_index_for(mds, "f_{oy}");
  ix = retrieve_and_check_mds_index_for(mds, "l=\\sqrt{x^2+y^2+z^2}");

  for (i = 0 ; i < mds->n; i++)
    {
      ds2->yd[i] = mds->x[iy][i];
      ds2->xd[i] = mds->x[ix][i];
    }
  ds2->nx = ds2->ny = i;
  if ((alloc_data_set_x_error(ds2) == NULL) || (ds2->xe == NULL))
    {
      win_printf_OK("I can't create errors 19!");
      return NULL;
    }

  ix = retrieve_and_check_mds_index_for(mds, "Error on <z>");

  for (i = 0 ; i < mds->n; i++)
      ds2->xe[i] = mds->x[ix][i];


  ix = retrieve_and_check_mds_index_for(mds, "k_y");
  iy = retrieve_and_check_mds_index_for(mds, "error on k_y");

  if ((alloc_data_set_y_error(ds2) == NULL) || (ds2->ye == NULL))
    {
      win_printf_OK("I can't create errors 20!");
      return NULL;
    }
  for (i = 0 ; i < mds->n; i++)
    {
      tmp = mds->x[ix][i];
      tmp = (tmp != 0) ? 1/tmp : 1;
      tmp *= mds->x[iy][i];
      tmp1 = ds2->xd[i];
      tmp1 = (tmp1 != 0) ? 1/tmp1 : 1;
      tmp1 *= ds2->xe[i];
      tmp = tmp*tmp + tmp1*tmp1;
      tmp = (tmp >= 0) ? sqrt(tmp) : 1;
      ds2->ye[i] = ds2->yd[i]*tmp;
    }


  ds = create_and_attach_one_ds(op, nx, nx, 0);
  if (ds == NULL)
    return (O_p *) win_printf_ptr("cannot find stuff in acreg!");


  ds->source = my_sprintf(ds->source,"DNA force extension curve f_{1x} l = l_z\n"
			  "xbr = %g ybr = %g zbr = %g \n"
			  " xpr = %g ypr = %g zpr = %g ",
			  bd_du->bd_x0,bd_du->bd_y0,bd_du->bd_z0,
			  bd_du->du_x0,bd_du->du_y0,bd_du->du_z0 );
  ds->nx = ds->ny = 0;
  set_dot_line(ds);
  set_plot_symb(ds, "\\pt6\\oc");

  iy = retrieve_and_check_mds_index_for(mds, "f_{1x}");
  ix = retrieve_and_check_mds_index_for(mds, "l = z");

  for (i = 0 ; i < mds->n; i++)
    {
      ds->yd[i] = mds->x[iy][i];
      ds->xd[i] = mds->x[ix][i];
    }
  ds->nx = ds->ny = i;
  if ((alloc_data_set_x_error(ds) == NULL) || (ds->xe == NULL))
    {
      win_printf_OK("I can't create errors 21!");
      return NULL;
    }

  ix = retrieve_and_check_mds_index_for(mds, "Error on <z>");

  for (i = 0 ; i < mds->n; i++)
      ds->xe[i] = mds->x[ix][i];

  ix = retrieve_and_check_mds_index_for(mds, "k_x");
  iy = retrieve_and_check_mds_index_for(mds, "error on k_x");

  if ((alloc_data_set_y_error(ds) == NULL) || (ds->ye == NULL))
    {
      win_printf_OK("I can't create errors 22!");
      return NULL;
    }
  for (i = 0 ; i < mds->n; i++)
    {
      tmp = mds->x[ix][i];
      tmp = (tmp != 0) ? 1/tmp : 1;
      tmp *= mds->x[iy][i];
      tmp1 = ds->xd[i];
      tmp1 = (tmp1 != 0) ? 1/tmp1 : 1;
      tmp1 *= ds->xe[i];
      tmp = tmp*tmp + tmp1*tmp1;
      tmp = (tmp >= 0) ? sqrt(tmp) : 1;
      ds->ye[i] = ds->yd[i]*tmp;
    }


  ds2 = create_and_attach_one_ds(op, nx, nx, 0);
  if (ds2 == NULL)
    return (O_p *) win_printf_ptr("cannot find stuff in acreg!");

  ds2->source = my_sprintf(ds2->source,"DNA force extension curve f_{1y} l = l_z\n"
			   "xbr = %g ybr = %g zbr = %g \n"
			   " xpr = %g ypr = %g zpr = %g ",
			   bd_du->bd_x0,bd_du->bd_y0,bd_du->bd_z0,
			   bd_du->du_x0,bd_du->du_y0,bd_du->du_z0);
  ds2->nx = ds2->ny = 0;
  set_dot_line(ds2);
  set_plot_symb(ds2, "\\pt6\\oc");


  iy = retrieve_and_check_mds_index_for(mds, "f_{1y}");
  ix = retrieve_and_check_mds_index_for(mds, "l = z");

  for (i = 0 ; i < mds->n; i++)
    {
      ds2->yd[i] = mds->x[iy][i];
      ds2->xd[i] = mds->x[ix][i];
    }
  ds2->nx = ds2->ny = i;
  if ((alloc_data_set_x_error(ds2) == NULL) || (ds2->xe == NULL))
    {
      win_printf_OK("I can't create errors 23!");
      return NULL;
    }

  ix = retrieve_and_check_mds_index_for(mds, "Error on <z>");

  for (i = 0 ; i < mds->n; i++)
      ds2->xe[i] = mds->x[ix][i];


  ix = retrieve_and_check_mds_index_for(mds, "k_y");
  iy = retrieve_and_check_mds_index_for(mds, "error on k_y");

  if ((alloc_data_set_y_error(ds2) == NULL) || (ds2->ye == NULL))
    {
      win_printf_OK("I can't create errors 24!");
      return NULL;
    }
  for (i = 0 ; i < mds->n; i++)
    {
      tmp = mds->x[ix][i];
      tmp = (tmp != 0) ? 1/tmp : 1;
      tmp *= mds->x[iy][i];
      tmp1 = ds2->xd[i];
      tmp1 = (tmp1 != 0) ? 1/tmp1 : 1;
      tmp1 *= ds2->xe[i];
      tmp = tmp*tmp + tmp1*tmp1;
      tmp = (tmp >= 0) ? sqrt(tmp) : 1;
      ds2->ye[i] = ds2->yd[i]*tmp;
    }
    set_plot_title(op, "Force versus extension");
    set_plot_x_title(op, "Allongement (\\mu m)");
    set_plot_y_title(op, "Force (pN)");
    set_plot_file(op,"force.gr");
    set_plot_path(op,"%s",prn->path);
    find_x_limits(op);
    find_y_limits(op);
  buf = create_description_label(op, 8);
  set_op_plot_label(op, op->x_lo + (op->x_hi - op->x_lo)/4,
		    op->y_hi - (op->y_hi - op->y_lo)/4, USR_COORD, "%s", buf);
  free(buf);

    return op;
}
O_p  *prepare_bead_position(pltreg  *prn, multi_d_s *mds, ref_pos *bd_du)
{
  int i;
  O_p *op = NULL;
  d_s *ds = NULL;
  int ix, iy, nx = 1024;
  char *buf = NULL;

  if ((op = create_and_attach_one_plot(prn, nx, nx, 0)) == NULL)
    {
      win_printf("cannot create plot !");
      return NULL;
    }


  ds = op->dat[0];
  ds->source = my_sprintf(ds->source,"DNA bead position \n"
			  "xbr = %g ybr = %g zbr = %g \n"
			  " xpr = %g ypr = %g zpr = %g ",
			  bd_du->bd_x0,bd_du->bd_y0,bd_du->bd_z0,
			  bd_du->du_x0,bd_du->du_y0,bd_du->du_z0 );
  ds->nx = ds->ny = 0;
  set_dot_line(ds);
  set_plot_symb(ds, "\\pt6\\oc");

  iy = retrieve_and_check_mds_index_for(mds, "Bead <y>");
  ix = retrieve_and_check_mds_index_for(mds, "Bead <x>");

  for (i = 0 ; i < mds->n; i++)
    {
      ds->yd[i] = mds->x[iy][i];
      ds->xd[i] = mds->x[ix][i];
    }
  ds->nx = ds->ny = i;
  if ((alloc_data_set_x_error(ds) == NULL) || (ds->xe == NULL))
    {
      win_printf_OK("I can't create errors 25!");
      return NULL;
    }
  if ((alloc_data_set_y_error(ds) == NULL) || (ds->ye == NULL))
    {
      win_printf_OK("I can't create errors 26!");
      return NULL;
    }
  ix = retrieve_and_check_mds_index_for(mds, "Error on <x>");
  iy = retrieve_and_check_mds_index_for(mds, "Error on <y>");

  for (i = 0 ; i < mds->n; i++)
    {
      ds->xe[i] = mds->x[ix][i];
      ds->ye[i] = mds->x[iy][i];
    }
    set_plot_title(op, "Sample %s",prn->filename);
    set_plot_x_title(op, "Mean X position (\\mu m)");
    set_plot_y_title(op, "Mean Y position (\\mu m)");
    set_plot_file(op,"Beadxy.gr");
    set_plot_path(op,"%s", prn->path);
    find_x_limits(op);
    find_y_limits(op);
  buf = create_description_label(op, 8);
  set_op_plot_label(op, op->x_lo + (op->x_hi - op->x_lo)/4,
		    op->y_hi - (op->y_hi - op->y_lo)/4, USR_COORD, "%s", buf);
  free(buf);

    return op;
}
O_p  *prepare_F_of_Z_mag(pltreg  *prn, multi_d_s *mds, ref_pos *bd_du)
{
  int i;
  O_p *op = NULL;
  d_s *ds = NULL, *ds2 = NULL;
  int ix, iy, iz, it, nx = 1024;
  float tmp, tmp1;
  char *buf = NULL;

  if ((op = create_and_attach_one_plot(prn, nx, nx, 0)) == NULL)
    {
      win_printf("cannot create plot !");
      return NULL;
    }


  ds = op->dat[0];
  ds->source = my_sprintf(ds->source,"Force versus Z magnet curve f_x l = 3D length\n"
			  "xbr = %g ybr = %g zbr = %g \n"
			  " xpr = %g ypr = %g zpr = %g ",
			  bd_du->bd_x0,bd_du->bd_y0,bd_du->bd_z0,
			  bd_du->du_x0,bd_du->du_y0,bd_du->du_z0 );
  ds->nx = ds->ny = 0;
  set_dot_line(ds);
  set_plot_symb(ds, "\\pt6\\oc");

  iy = retrieve_and_check_mds_index_for(mds, "f_{ox}");
  ix = retrieve_and_check_mds_index_for(mds, "Mag Z");
  if (ix == -1 || iy == -1)
    return (O_p *) win_printf_ptr("Did not retrieve \"f_{ox}\" or \"Mag Z\" in mds");
  for (i = 0 ; i < mds->n; i++)
    {
      ds->yd[i] = mds->x[iy][i];
      ds->xd[i] = mds->x[ix][i];
    }
  ds->nx = ds->ny = i;

  iz = retrieve_and_check_mds_index_for(mds, "Error on <z>");
  it = retrieve_and_check_mds_index_for(mds, "l=\\sqrt{x^2+y^2+z^2}");
  ix = retrieve_and_check_mds_index_for(mds, "k_x");
  iy = retrieve_and_check_mds_index_for(mds, "error on k_x");
  if (ix < 0 || iy < 0 || it < 0 || iz < 0)
    return (O_p *) win_printf_ptr("Did not retrieve \"Error on <z>\" or \"l=\\sqrt{x^2+y^2+z^2}\"\n"
			  "or \"k_x\" or \"error on k_x\" in mds");
  if ((alloc_data_set_y_error(ds) == NULL) || (ds->ye == NULL))
    {
      win_printf_OK("I can't create errors 27!");
      return NULL;
    }
  for (i = 0 ; i < mds->n; i++)
    {
      tmp = mds->x[ix][i];
      tmp = (tmp != 0) ? 1/tmp : 1;
      tmp *= mds->x[iy][i];
      tmp1 = mds->x[it][i];
      tmp1 = (tmp1 != 0) ? 1/tmp1 : 1;
      tmp1 *= mds->x[iz][i];
      tmp = tmp*tmp + tmp1*tmp1;
      tmp = (tmp >= 0) ? sqrt(tmp) : 1;
      ds->ye[i] = ds->yd[i]*tmp;
    }


  ds2 = create_and_attach_one_ds(op, nx, nx, 0);
  if (ds2 == NULL)
    return (O_p *) win_printf_ptr("cannot find stuff in acreg!");

  ds2->source = my_sprintf(ds2->source,"Force versus Z magnet curve f_y l = 3D length\n"
			   "xbr = %g ybr = %g zbr = %g \n"
			   " xpr = %g ypr = %g zpr = %g ",
			   bd_du->bd_x0,bd_du->bd_y0,bd_du->bd_z0,
			   bd_du->du_x0,bd_du->du_y0,bd_du->du_z0);
  ds2->nx = ds2->ny = 0;
  set_dot_line(ds2);
  set_plot_symb(ds2, "\\pt6\\oc");


  iy = retrieve_and_check_mds_index_for(mds, "f_{oy}");
  ix = retrieve_and_check_mds_index_for(mds, "Mag Z");
  if (ix == -1 || iy == -1)
    return (O_p *) win_printf_ptr("Did not retrieve \"f_{oy}\" or \"Mag Z\" in mds");

  for (i = 0 ; i < mds->n; i++)
    {
      ds2->yd[i] = mds->x[iy][i];
      ds2->xd[i] = mds->x[ix][i];
    }
  ds2->nx = ds2->ny = i;

  iz = retrieve_and_check_mds_index_for(mds, "Error on <z>");
  it = retrieve_and_check_mds_index_for(mds, "l=\\sqrt{x^2+y^2+z^2}");
  ix = retrieve_and_check_mds_index_for(mds, "k_y");
  iy = retrieve_and_check_mds_index_for(mds, "error on k_y");
  if (ix < 0 || iy < 0 || it < 0 || iz < 0)
    return (O_p *) win_printf_ptr("Did not retrieve \"Error on <z>\" or \"l=\\sqrt{x^2+y^2+z^2}\"\n"
			  "or \"k_y\" or \"error on k_y\" in mds");
  if ((alloc_data_set_y_error(ds2) == NULL) || (ds2->ye == NULL))
    {
      win_printf_OK("I can't create errors 27!");
      return NULL;
    }
  for (i = 0 ; i < mds->n; i++)
    {
      tmp = mds->x[ix][i];
      tmp = (tmp != 0) ? 1/tmp : 1;
      tmp *= mds->x[iy][i];
      tmp1 = mds->x[it][i];
      tmp1 = (tmp1 != 0) ? 1/tmp1 : 1;
      tmp1 *= mds->x[iz][i];
      tmp = tmp*tmp + tmp1*tmp1;
      tmp = (tmp >= 0) ? sqrt(tmp) : 1;
      ds2->ye[i] = ds2->yd[i]*tmp;
    }


  ds = create_and_attach_one_ds(op, nx, nx, 0);
  if (ds == NULL)
    return (O_p *) win_printf_ptr("cannot find stuff in acreg!");


  ds->source = my_sprintf(ds->source,"Force versus Z magnet curve f_{1x} l = l_z\n"
			  "xbr = %g ybr = %g zbr = %g \n"
			  " xpr = %g ypr = %g zpr = %g ",
			  bd_du->bd_x0,bd_du->bd_y0,bd_du->bd_z0,
			  bd_du->du_x0,bd_du->du_y0,bd_du->du_z0 );
  ds->nx = ds->ny = 0;
  set_dot_line(ds);
  set_plot_symb(ds, "\\pt6\\oc");

  iy = retrieve_and_check_mds_index_for(mds, "f_{1x}");
  ix = retrieve_and_check_mds_index_for(mds, "Mag Z");
  if (ix == -1 || iy == -1)
    return (O_p *) win_printf_ptr("Did not retrieve \"f_{1x}\" or \"Mag Z\" in mds");

  for (i = 0 ; i < mds->n; i++)
    {
      ds->yd[i] = mds->x[iy][i];
      ds->xd[i] = mds->x[ix][i];
    }
  ds->nx = ds->ny = i;

  iz = retrieve_and_check_mds_index_for(mds, "Error on <z>");
  it = retrieve_and_check_mds_index_for(mds, "l = z");
  ix = retrieve_and_check_mds_index_for(mds, "k_x");
  iy = retrieve_and_check_mds_index_for(mds, "error on k_x");
  if (ix < 0 || iy < 0 || it < 0 || iz < 0)
    return (O_p *) win_printf_ptr("Did not retrieve \"Error on <z>\" or \"l = z\"\n"
			  "or \"k_x\" or \"error on k_x\" in mds");

  if ((alloc_data_set_y_error(ds) == NULL) || (ds->ye == NULL))
    {
      win_printf_OK("I can't create errors 28!");
      return NULL;
    }
  for (i = 0 ; i < mds->n; i++)
    {
      tmp = mds->x[ix][i];
      tmp = (tmp != 0) ? 1/tmp : 1;
      tmp *= mds->x[iy][i];
      tmp1 = mds->x[it][i];
      tmp1 = (tmp1 != 0) ? 1/tmp1 : 1;
      tmp1 *= mds->x[iz][i];
      tmp = tmp*tmp + tmp1*tmp1;
      tmp = (tmp >= 0) ? sqrt(tmp) : 1;
      ds->ye[i] = ds->yd[i]*tmp;
    }


  ds2 = create_and_attach_one_ds(op, nx, nx, 0);
  if (ds2 == NULL)
    return (O_p *) win_printf_ptr("cannot find stuff in acreg!");

  ds2->source = my_sprintf(ds2->source,"Force versus Z magnet curve f_{1y} l = l_z\n"
			   "xbr = %g ybr = %g zbr = %g \n"
			   " xpr = %g ypr = %g zpr = %g ",
			   bd_du->bd_x0,bd_du->bd_y0,bd_du->bd_z0,
			   bd_du->du_x0,bd_du->du_y0,bd_du->du_z0);
  ds2->nx = ds2->ny = 0;
  set_dot_line(ds2);
  set_plot_symb(ds2, "\\pt6\\oc");


  iy = retrieve_and_check_mds_index_for(mds, "f_{1y}");
  ix = retrieve_and_check_mds_index_for(mds, "Mag Z");
  if (ix == -1 || iy == -1)
    return (O_p *) win_printf_ptr("Did not retrieve \"f_{1y}\" or \"Mag Z\" in mds");

  for (i = 0 ; i < mds->n; i++)
    {
      ds2->yd[i] = mds->x[iy][i];
      ds2->xd[i] = mds->x[ix][i];
    }
  ds2->nx = ds2->ny = i;

  iz = retrieve_and_check_mds_index_for(mds, "Error on <z>");
  it = retrieve_and_check_mds_index_for(mds, "l = z");
  ix = retrieve_and_check_mds_index_for(mds, "k_y");
  iy = retrieve_and_check_mds_index_for(mds, "error on k_y");
  if (ix < 0 || iy < 0 || it < 0 || iz < 0)
    return (O_p *) win_printf_ptr("Did not retrieve \"Error on <z>\" or \"l = z\"\n"
			  "or \"k_y\" or \"error on k_y\" in mds");


  if ((alloc_data_set_y_error(ds2) == NULL) || (ds2->ye == NULL))
    {
      win_printf_OK("I can't create errors 29!");
      return NULL;
    }
  for (i = 0 ; i < mds->n; i++)
    {
      tmp = mds->x[ix][i];
      tmp = (tmp != 0) ? 1/tmp : 1;
      tmp *= mds->x[iy][i];
      tmp1 = mds->x[it][i];
      tmp1 = (tmp1 != 0) ? 1/tmp1 : 1;
      tmp1 *= mds->x[iz][i];
      tmp = tmp*tmp + tmp1*tmp1;
      tmp = (tmp >= 0) ? sqrt(tmp) : 1;
      ds2->ye[i] = ds2->yd[i]*tmp;
    }


    set_plot_title(op, "Sample");
    set_plot_y_title(op, "Force (pN)");
    set_plot_x_title(op, "Magnets distance");
    set_plot_file(op,"forcez.gr");
    set_plot_path(op,"%s", prn->path);
    find_x_limits(op);
    find_y_limits(op);

  buf = create_description_label(op, 8);
  set_op_plot_label(op, op->x_lo + (op->x_hi - op->x_lo)/4,
		    op->y_hi - (op->y_hi - op->y_lo)/4, USR_COORD, "%s", buf);
  free(buf);

    return op;
}

pltreg  *build_plots_for_acq(multi_d_s *mds, ref_pos *bd_du)
{
  int i;
  pltreg  *prn = NULL;
  O_p *op = NULL;
  d_s *ds = NULL, *ds2 = NULL;
  int ix, iy, nx = 1024;
  char path[512] = {0};



  if (mds == NULL || bd_du == NULL)   return NULL;

  prn = create_and_register_new_plot_project(0,     32,    900,    668);
  if (prn == NULL)
    return (pltreg *) win_printf_ptr("cannot create acreg !");


  extract_file_path(path, 512, mds->source);
  prn->path = strdup(path);
  extract_file_name(path, 512, mds->source);
  prn->filename = strdup(path);


  ds = create_and_attach_one_ds(op = prn->one_p, nx, nx, 0);
  if (ds == NULL)
    return (pltreg *) win_printf_ptr("cannot find stuff in acreg!");

  ds->source = my_sprintf(ds->source,"DNA extension versus Z magnet curve \n"
			    "xbr = %g ybr = %g zbr = %g \n"
			    " xpr = %g ypr = %g zpr = %g ",
			    bd_du->bd_x0,bd_du->bd_y0,bd_du->bd_z0,
			    bd_du->du_x0,bd_du->du_y0,bd_du->du_z0);
  ds->nx = ds->ny = 0;
  set_dot_line(ds);
  set_plot_symb(ds, "\\pt6\\oc");

  ix = retrieve_and_check_mds_index_for(mds, "l=\\sqrt{x^2+y^2+z^2}");
  iy = retrieve_and_check_mds_index_for(mds, "Mag Z");

  for (i = 0 ; i < mds->n; i++)
    {
      ds->xd[i] = mds->x[ix][i];
      ds->yd[i] = mds->x[iy][i];
    }
  ds->nx = ds->ny = i;




  ds2 = create_and_attach_one_ds(op, nx, nx, 0);
  if (ds2 == NULL)
    return (pltreg *) win_printf_ptr("cannot find stuff in acreg!");

  ds2->source = my_sprintf(ds2->source,"DNA extension versus Z magnet curve l = z\n"
			   "xbr = %g ybr = %g zbr = %g \n"
			   " xpr = %g ypr = %g zpr = %g ",
			   bd_du->bd_x0,bd_du->bd_y0,bd_du->bd_z0,
			   bd_du->du_x0,bd_du->du_y0,bd_du->du_z0);
  ds2->nx = ds2->ny = 0;
  set_dot_line(ds2);
  set_plot_symb(ds2, "\\pt6\\oc");

  ix = retrieve_and_check_mds_index_for(mds, "l = z");
  iy = retrieve_and_check_mds_index_for(mds, "Mag Z");

  for (i = 0 ; i < mds->n; i++)
    {
      ds2->xd[i] = mds->x[ix][i];
      ds2->yd[i] = mds->x[iy][i];
    }
  ds2->nx = ds2->ny = i;
  if ((alloc_data_set_x_error(ds2) == NULL) || (ds2->xe == NULL))
    {
      win_printf_OK("I can't create errors 1!");
      return NULL;
    }

  iy = retrieve_and_check_mds_index_for(mds, "Error on <z>");

  for (i = 0 ; i < mds->n; i++)
      ds2->xe[i] = mds->x[iy][i];



    set_plot_title(op, "Z_{mag} versus L");
    set_plot_x_title(op, "Allongement (\\mu m)");
    set_plot_y_title(op, "Z mag (mm)");
    set_op_filename(op, "lzmag.gr");

    prepare_F_of_L(prn, mds, bd_du);
    prepare_k_versus_Zmag(prn, mds, bd_du);
    prepare_hat_curve(prn, mds, bd_du);
    prepare_fc_versus_L(prn, mds, bd_du);
    //prepare_L_versus_Zmag(prn, mds, bd_du);
    prepare_F_of_Z_mag(prn, mds, bd_du);
    prepare_etar_versus_L(prn, mds, bd_du);
    prepare_angle_versus_L(prn, mds, bd_du);
    prepare_anisotropy_versus_L(prn, mds, bd_du);
    prepare_bead_position(prn, mds, bd_du);

  prn->use.to = IS_MULTI_D_S;
  prn->use.stuff = (void *)mds;

  do_one_plot(prn);

  refresh_plot(prn, UNCHANGED);
  return prn;
}


int do_full_new_treat_x_y_z_acq_spe_2(void)
{
  int i, j;
  multi_d_s *mds = NULL;
  cam_cor caml;
  ref_pos bd_du;
  int w_flag = 1, fbm = 10, index, index_end;
  float high_fc_ratio;
  pltreg *pr = NULL, *prn = NULL;
  O_p *op = NULL, *opn = NULL;
  char c[512] = {0}, basename[256] = {0};
  char f_line[256] = {0}, s1[128] = {0}, s2[1024] = {0}, s3[128] = {0}, dir[512] = {0};
  FILE *fpi = NULL;

  if(updating_menu_state != 0)    return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
      return win_printf_OK("cannot find data");

  strcpy(dir, op->dir);
  put_backslash(dir);


  sprintf(c,"%s\\force.gr",dir);
  fpi = fopen(c,"r");
  if (fpi == NULL)
    {
      win_printf("cannot open %s !\n",c);
      return 1;
    }
  for (j = 0; fgets(f_line,256,fpi) != NULL && j == 0;  )
    {
      if ((sscanf(f_line,"%s%s%s",s1,s2,s3) == 3)
	  && (strcmp(s1,"-src") == 0) && (strcmp(s3,"force") == 0))
        {
	  i = fscanf(fpi," xbr = %f ybr = %f zbr = %f",&bd_du.bd_x0,&bd_du.bd_y0,&bd_du.bd_z0);
	  i += fscanf(fpi," xpr = %f ypr = %f zpr = %f",&bd_du.du_x0,&bd_du.du_y0,&bd_du.du_z0);
	  if (i != 6)
            {
	      win_scanf("bead x0 %f y0 %f z0 %f dust x0 %f y0 %f z0 %f",
			&bd_du.bd_x0,&bd_du.bd_y0,&bd_du.bd_z0,&bd_du.du_x0,&bd_du.du_y0,&bd_du.du_z0);
            }
	  else j = 1;
        }
    }
  fclose(fpi);
  grab_camera_and_spectrum_parameters(&caml, &w_flag, &fbm, &high_fc_ratio);
  index = retrieve_basename_and_index(op->filename, strlen(op->filename), basename, 256);


  for (index_end = index; ;index_end++)
    {
      sprintf(f_line,"%s%s%03d.gr",dir,basename,index_end);
      fpi = fopen(f_line,"r");
      if (fpi == NULL) break;
      fclose(fpi);
    }
  index_end--;


  sprintf(c,"I am going to retreate acquisition files starting at %s%03d\n"
	  " until final index %%d",basename,index);
  win_scanf(c,&index_end);


  for (i = index ; i <= index_end; i++)
    {
      sprintf(f_line,"%s%03d.gr",basename,i);
      opn = create_plot_from_gr_file(f_line, dir);
      if (opn == NULL)  return win_printf_OK("could not load %s",f_line);
      else display_title_message("loaded %s",f_line);
      mds = do_full_new_treat_x_y_z_acq_spe_op(opn, mds, &caml, w_flag, high_fc_ratio, fbm, &bd_du);
      free_one_plot(opn);
    }
  mds->source = my_sprintf(NULL,"%s%s%03d-%03d.xv",op->dir,basename,index,index_end);


  prn = build_plots_for_acq(mds, &bd_du);
  prn->filename = my_sprintf(NULL,"%s%03d-%03d.xv",basename,index,index_end);
  prn->path = strdup(dir);

    return D_O_K;
}




int do_full_new_treat_x_y_z_acq_spe_2_pico(void)
{
  int i;
  multi_d_s *mds = NULL;
  cam_cor caml;
  ref_pos bd_du;
  int w_flag = 1, fbm = 10, index = 0, index_end = 0, n_bd = 0, force_n = 0;
  float high_fc_ratio;
  pltreg *pr = NULL, *prn = NULL;
  O_p *op = NULL, *opn = NULL;
  char c[512] = {0}; //, basename[256];
  char f_line[256] = {0}, dir[512] = {0};
  FILE *fpi = NULL;

  if(updating_menu_state != 0)    return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
      return win_printf_OK("cannot find data");

  strcpy(dir, op->dir);
  put_backslash(dir);

  sscanf(op->filename,"X(t)Y(t)Z(t)bd%dforce%d-%d.gr",&n_bd,&force_n,&index);
  //win_printf("index %d force %d bd %d",index,force_n,n_bd);
  grab_camera_and_spectrum_parameters(&caml, &w_flag, &fbm, &high_fc_ratio);



  //index = retrieve_basename_and_index(op->filename, strlen(op->filename), basename, 256);


  for (index_end = index; ;index_end++)
    {
      sprintf(f_line,"%sX(t)Y(t)Z(t)bd%dforce%d-%d.gr",dir,n_bd,force_n,index_end);
      fpi = fopen(f_line,"r");
      if (fpi == NULL) break;
      fclose(fpi);
    }
  index_end--;


  sprintf(c,"I am going to retreate acquisition files starting at X(t)Y(t)Z(t)bd%dforce%d-%d\n"
	  " until final index X(t)Y(t)Z(t)bd%dforce%d-%%d",n_bd,force_n,index,n_bd,force_n);
  win_scanf(c,&index_end);


  for (i = index ; i <= index_end; i++)
    {
      sprintf(f_line,"X(t)Y(t)Z(t)bd%dforce%d-%d.gr",n_bd,force_n,i);
      opn = create_plot_from_gr_file(f_line, dir);
      if (opn == NULL)  return win_printf_OK("could not load %s",f_line);
      else display_title_message("loaded %s",f_line);
      mds = do_full_new_treat_x_y_z_acq_spe_op_pico(opn, mds, &caml, w_flag, high_fc_ratio, fbm, &bd_du);
      free_one_plot(opn);
    }
  mds->source = my_sprintf(NULL,"%sX(t)Y(t)Z(t)bd%dforce%d-%03d-%03d.xv",op->dir,n_bd,force_n,index,index_end);


  prn = build_plots_for_acq(mds, &bd_du);
  prn->filename = my_sprintf(NULL,"X(t)Y(t)Z(t)bd%dforce%d-%03d-%03d.xv",n_bd,force_n,index,index_end);
  prn->path = strdup(dir);

    return D_O_K;
}



int    load_cardinal_xvplot_file(void)
{
    int i, j;
    char path[512] = {0}, file[256] = {0};
    char fullfile[512] = {0}, *fu = NULL;
    multi_d_s *mds = NULL;
    pltreg* pr = NULL;
    ref_pos bd_du;
    char f_line[256] = {0}, s1[128] = {0}, s2[1024] = {0}, s3[128] = {0};
    FILE *fpi = NULL;
    char c[512] = {0};
    const char *fuc = NULL;

    pr = find_pr_in_current_dialog(NULL);
    mds = find_multi_d_s_in_pltreg(pr);

    if(updating_menu_state != 0)
    {
        xvplot_plot_menu(mds);
        return D_O_K;
    }
    switch_allegro_font(1);
    if (fu == NULL)
    {
        fuc = get_config_string("CARDINAL-RETREAT","last_loaded",NULL);
        if (fuc != NULL)
        {
            strcpy(fullfile,fuc);
            fu = fullfile;
        }
        else
        {
            my_getcwd(fullfile, 512);
            strcat(fullfile,"\\");
        }
    }

        i = file_select_ex("Load cardinal xv-plot", fullfile, "xv;mgr;up;dn;dat", 512, 0, 0);
    switch_allegro_font(0);
    if (i != 0)
    {
        fu = fullfile;
        set_config_string("CARDINAL-RETREAT","last_loaded",fu);
        extract_file_name(file, 256, fullfile);
        extract_file_path(path, 512, fullfile);
        strcat(path,"\\");
        xvplot_file(fullfile,0);
	pr = find_pr_in_current_dialog(NULL);
	mds = find_multi_d_s_in_pltreg(pr);

	sprintf(c,"%s\\force.gr",path);
	fpi = fopen(c,"r");
	if (fpi == NULL)
	  {
	    win_printf("cannot open %s !\n",c);
	    return 1;
	  }
	for (j = 0; fgets(f_line,256,fpi) != NULL && j == 0;  )
	  {
	    if ((sscanf(f_line,"%s%s%s",s1,s2,s3) == 3)
		&& (strcmp(s1,"-src") == 0) && (strcmp(s3,"force") == 0))
	      {
		i = fscanf(fpi," xbr = %f ybr = %f zbr = %f",&bd_du.bd_x0,&bd_du.bd_y0,&bd_du.bd_z0);
		i += fscanf(fpi," xpr = %f ypr = %f zpr = %f",&bd_du.du_x0,&bd_du.du_y0,&bd_du.du_z0);
		if (i != 6)
		  {
		    win_scanf("bead x0 %f y0 %f z0 %f dust x0 %f y0 %f z0 %f",
			      &bd_du.bd_x0,&bd_du.bd_y0,&bd_du.bd_z0,&bd_du.du_x0,&bd_du.du_y0,&bd_du.du_z0);
		  }
		else j = 1;
	      }
	  }
	fclose(fpi);

	prepare_F_of_L(pr, mds, &bd_du);
	prepare_k_versus_Zmag(pr, mds, &bd_du);
	prepare_hat_curve(pr, mds, &bd_du);
	prepare_fc_versus_L(pr, mds, &bd_du);
	prepare_L_versus_Zmag(pr, mds, &bd_du);
	prepare_F_of_Z_mag(pr, mds, &bd_du);
	prepare_etar_versus_L(pr, mds, &bd_du);
	prepare_angle_versus_L(pr, mds, &bd_du);
	prepare_anisotropy_versus_L(pr, mds, &bd_du);
	prepare_bead_position(pr, mds, &bd_du);
	broadcast_dialog_message(MSG_DRAW,0);

    }
    return 0;
}








int do_load_one_ds_from_multiple_op(void)
{
  int i, j;
  int index, index_end;
  int index_jump=10;
  int color_code=1;
  static int ds_wanted = 0;
  pltreg *pr = NULL;
  O_p *op = NULL, *opn = NULL, *opm = NULL;
  d_s *ds = NULL, *dsn = NULL;
  char c[512] = {0}, basename[256] = {0};
  char f_line[256] = {0}, dir[512] = {0};
  FILE *fpi = NULL;

  if(updating_menu_state != 0)    return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
      return win_printf_OK("cannot find data");

  strcpy(dir, op->dir);
  put_backslash(dir);

  index = retrieve_basename_and_index(op->filename, strlen(op->filename), basename, 256);

  sprintf(c,"I am going to load a series of plots with increasing index numbers\n"
	  "present index %d basename %s\n"
                "maximum size of index jump allowed : %%d"
                "color of the loaded datasets (-1 auto) %%d",
                index,basename);
  i = win_scanf(c,&index_jump,&color_code);
  if (i == WIN_CANCEL) return D_O_K;


  for (index_end = index, j = 0; j < index_jump ;index_end++)
    {
      sprintf(f_line,"%s%s%03d.gr",dir,basename,index_end);
      fpi = fopen(f_line,"r");
      if (fpi == NULL) j++;
      else
	{
	  j = 0;
	  fclose(fpi);
	}
    }
  index_end -= index_jump;



  sprintf(c,"I am going to load a series of plots,\n grabbing only one data set\n"
	  "and placing them in a new plot\n"
	  "The acquisition files starts at %s%03d\n"
	  " until final index %%d indicate the data set number desired %%d",basename,index);
  i = win_scanf(c,&index_end,&ds_wanted);
  if (i == WIN_CANCEL)  return D_O_K;
  if (ds_wanted > op->n_dat)    return win_printf_OK("Gros con!");
  ds = op->dat[ds_wanted];
  if ((opm = create_and_attach_one_plot(pr, ds->nx, ds->nx, 0)) == NULL)
      return win_printf_OK("cannot create plot !");
  dsn = opm->dat[ds_wanted];
  for (i = index ; i <= index_end; i++)
    {
      //ds_number = 0;
      sprintf(f_line,"%s%03d.gr",basename,i);
      opn = create_plot_from_gr_file(f_line,dir);
         if (opn != NULL)
	{
       	  display_title_message("loaded %s",f_line);
	  ds = opn->dat[ds_wanted];
	  dsn = duplicate_data_set(ds, (i == index) ? dsn : NULL);
	  if (i != index)
	    {
	      if (add_one_plot_data(opm, IS_DATA_SET, (void*)dsn))
		return win_printf_OK("Out_Of_Memory");
	      else  if (color_code >= 0)
		set_ds_line_color(opm->dat[opm->n_dat-1],data_color[color_code%max_data_color]);
	    }
	  free_one_plot(opn);
	}
    }
  opm->need_to_refresh = 1;
  switch_plot(pr, pr->n_op - 1);
  refresh_plot(pr, pr->n_op - 1);
  return D_REDRAWME;
}

int	fit_force_versus_zmag(void)
{
	int i, j, k;
	O_p *op = NULL;
	d_s *dsf = NULL, *ds = NULL, *dsd = NULL, *dsn = NULL;
	pltreg *pr = NULL;
	int nop, nx, np, icp;
	static float zmin = 13, zmax = 20, zlow = -1;
	static int n = 2;
	float  y_lo , y_hi, x_lo, x_hi, er, weight, dx;
	double *a = NULL, x, y, x1, yt;


       if(updating_menu_state != 0)    return D_O_K;
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
		return D_O_K;
	dsf = find_source_specific_ds_in_pr(pr,"Force versus Z magnet curve");
	if (dsf == NULL)
		return win_printf_OK("cannot find \nForce versus Z magnet curve ds!");
	nop = find_op_nb_of_source_specific_ds_in_pr(pr,"Force versus Z magnet curve");
	if (nop < 0)
		return win_printf_OK("cannot find \nForce versus Z magnet curve ds!");


        zmin = get_config_float("F-de-Zmag", "zmin", 13);
	n = get_config_int("F-de-Zmag", "fit-order", 2);
	zmax = get_config_float("F-de-Zmag", "zmax", 20);
	zlow = get_config_float("F-de-Zmag", "zlow", 11);


	i = win_scanf("I am going to replace the Zmag vertical coordinate \n"
		      "of this force curve by its corresponding force\n"
		      "determined using the F=f(Z_{mag} plot and a \n"
		      "polynomial fit of log F \n"
		      "polynomial order %4d minimum zmag to use %6f\n"
		      "maximum zmag%6f\n"
		      "low force zmag to set weight (-1 forget)%6f\n",&n,&zmin,&zmax,&zlow);
	if (i == WIN_CANCEL)	return OFF;
	set_config_float("F-de-Zmag", "zmin", zmin);
	set_config_int("F-de-Zmag", "fit-order", n);
	set_config_float("F-de-Zmag", "zmax", zmax);
	set_config_float("F-de-Zmag", "zlow", zlow);


	dsd = build_data_set(dsf->nx, dsf->nx);
	dsd->nx = dsd->ny = dsf->nx;
	if (dsd == NULL)	   return win_printf_OK("cannot create data set");
	for (i = 0, j = 0, x_lo = x_hi = dsf->xd[0], y_lo = y_hi = dsf->yd[0] ; i < dsd->nx; i++)
	{
		x_lo = (x_lo > dsf->xd[i]) ? dsf->xd[i] : x_lo;
		x_hi = (x_hi < dsf->xd[i]) ? dsf->xd[i] : x_hi;
		y_lo = (y_lo > dsf->yd[i]) ? dsf->yd[i] : y_lo;
		y_hi = (y_hi < dsf->yd[i]) ? dsf->yd[i] : y_hi;
		if (dsf->xd[i] <= zmax && dsf->xd[i] >= zmin)
		{
			dsd->xd[j] = dsf->xd[i];
			dsd->yd[j] = (dsf->yd[i] > 0) ? log(dsf->yd[i]) : 0;
			if (dsf->yd[i] <= 0)
			  win_printf("Force is negative!\ncannot take log!\n%d point f = %g",i,dsf->yd[i]);
			j++;
		}
	}
	dsd->nx = dsd->ny = j;
	if (j <=2)	return win_printf_OK("not enough points for the line fit!");
	else win_printf("I have found %d points to fit",j);
	y_lo = (y_lo > 0) ? log(y_lo) : __FLT_MIN_EXP__;
	y_hi = (y_hi > 0) ? log(y_hi) : __FLT_MAX_EXP__;
	nx = fit_ds_to_xn_polynome(dsd, n+1, zmin, zmax, y_lo, y_hi, &a);
	if (nx < 0)		return win_printf_OK("something went wrong!");
	for (i = 0, j = -1 ; i < dsf->nx; i++)
	{
		if (fabs(dsf->xd[i] - zlow) < 0.01)		j = i;
	}
	for (k = 0, yt = 0; k <= n && j != -1; k++)
	{
		for (np = 0, x1 = a[k]; np < k; np++)
			x1 *= zlow;
		yt += x1;
	}
	weight = (j != -1) ? exp(yt) - dsf->yd[j] : 0;
	er = error_in_fit_ds_to_xn_polynome(dsd, n+1, zmin, zmax, y_lo, y_hi, a);
	win_printf("weight %g er %g",weight,er);
	free_data_set(dsd);

	for (i = 0, icp = -1; i < pr->o_p[nop]->n_dat && icp == -1; i++)
	{
		if (pr->o_p[nop]->dat[i] != NULL)
		{
			dsn = pr->o_p[nop]->dat[i];
			icp = ((dsn->treatement != NULL) &&
			       strncmp(dsn->treatement,"Force vs Zmag fit",17) == 0) ? i : -1;
		}
	}
	nx = 512;
	if (icp == -1)
	{
		i = pr->o_p[nop]->cur_dat;
		dsn = create_and_attach_one_ds(pr->o_p[nop], nx, nx, 0);
		dsn->treatement = my_sprintf(dsn->treatement,"Force vs Zmag fit");
		pr->o_p[nop]->cur_dat = i;
	}
	if (dsn == NULL)	return win_printf_OK("can't create data set");
	if ( nx != dsn->nx || nx != dsn->ny)
		return win_printf_OK("data set has not the right size");
	if (nx == 512)
	{
		for (i=0, dx = (x_hi - x_lo) / nx ; i< nx ; i++)
		{
			x = x_lo + dx * i;
			dsn->xd[i] = (float)x;
			yt = 0;
			for (j = 0; j <= n; j++)
			{
				for (np = 0, x1 = a[j]; np < j; np++)
					x1 *= x;
				yt += x1;
			}
			dsn->yd[i] = exp(yt) - weight;
		}
	}
	for (j = 0 ;  j < ds->nx ; j++)
	{
		x = ds->xd[j];
		y = ds->yd[j];
		for (k = 0, yt = 0; k <= n; k++)
		{
			for (np = 0, x1 = a[k]; np < k; np++)
				x1 *= y;
			yt += x1;
		}
		ds->yd[j] = exp(yt) - weight;
	}
	return refresh_plot(pr, UNCHANGED);
}


int average_x_y_over_step(void)
{
	int i, j , k;
	pltreg *pr = NULL;
	static int np = 1024, nd = 128, nn;
	O_p *op = NULL;
	d_s *dsi = NULL, *dsd = NULL;
	int  nx;

	if(updating_menu_state != 0)    return D_O_K;
	i = ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi);
	if ( i != 3)		return win_printf_OK("cannot find data");

	nx = dsi->nx;

	i = win_scanf("Average signal\n nb of steps in sample %8d\n"
		      "dead perid nb of points %d",&np,&nd);
	if (i == WIN_CANCEL)	return D_O_K;
	nn = nx / np;
	dsd = create_and_attach_one_ds(op, np,np, 0);
	if (dsd == NULL)     return win_printf_OK("Cannot allocate plot");
	for (i=0 ; i< np ; i++)
	{
	 	dsd->xd[i] = dsd->yd[i] = 0;
		k = (i * nx)/np;
		for (j= nd/2 ; j< nn-nd/2 ; j++)
		{
			dsd->yd[i] += dsi->yd[k + j];
			dsd->xd[i] += dsi->xd[k + j];
		}
		dsd->yd[i] /= (nn-nd);
		dsd->xd[i] /= (nn-nd);
	}
	dsd->treatement = my_sprintf(dsd->treatement,"step averaged signal over %d periods dead per %d"
				     ,np,nd);
	inherit_from_ds_to_ds(dsd, dsi);
	refresh_plot(pr, UNCHANGED);
	return 0;
}

int	average_modulation(void)
{
	int i, j;
	O_p  *op = NULL, *opi = NULL;
	d_s *ds = NULL, *dsi = NULL;
	pltreg *pr = NULL;
	static int nstep = 256, start = 0, over_nx = 0, start1 = 64, nx1 = 64, start2 = 128;
	int nf, exclu = -1;
	float avg1 = 0, y21, avg2 = 0, y22;

	if(updating_menu_state != 0)    return D_O_K;
	i = ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&opi,&dsi);
	if ( i != 3)		return win_printf_OK("cannot find data");

	nf = dsi->nx;
	over_nx = (over_nx == 0) ? nf/nstep : over_nx;
	i = win_scanf("This routine average a periodic signal having %8d points per period\n"
		      "you can skip points and start averaging at pt index %8d\n"
		      "you can define averaging over a number of cycle %8d \n"
		      "In a cycle averaging value 0 starting at %8d\n"
		      "extending over %8d second starting at (half period + dead period) %8d\n"
		      "same extend, excluding period ( < 0 no exclusion) %8d",
		      &nstep,&start,&over_nx,&start1,&nx1,&start2,&exclu);
	if (i == WIN_CANCEL)	return D_O_K;

	over_nx *= nstep;
	if ((op = create_and_attach_one_plot(pr, nstep, nstep, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	ds = op->dat[0];
	over_nx = (over_nx  + start >= nf) ? nf - start : over_nx;
	for (i = start; i < start + over_nx; i+= nstep)
	{
	  if (((i - start)/nstep) == exclu)  continue;
	  for (j = 0; j < nstep; j++)
	    {
	      ds->yd[j] += dsi->yd[(i+j < nf)?i+j:nf-1];
	      ds->xd[j] += 1;
	    }
	}
	for (j = 0; j < nstep; j++)
	{
		ds->yd[j] = (ds->xd[j] != 0) ? ds->yd[j]/ds->xd[j] : ds->yd[j];
		ds->xd[j] = j;
	}
	mean_y2_on_array(ds->yd + start1, nx1, 0, &avg1, &y21, NULL);
	mean_y2_on_array(ds->yd + start2, nx1, 0, &avg2, &y22, NULL);
	avg1 *= opi->dy;
	avg2 *= opi->dy;
	y21 *= opi->dy*opi->dy;
	y22 *= opi->dy*opi->dy;
	set_plot_title(op, "\\stack{{averaged modulation with period %d}"
		       "{\\pt8 start %d over %d}{\\pt8 averaging over %d start avg1 %d start avg2 %d}"
		       "{\\delta Z = %g \\stack{+-} %g %s}}",nstep,start,over_nx,nx1,start1,start2,
		       avg2-avg1,sqrt(y21+y22)/nx1,(opi->y_unit!= NULL)?opi->y_unit:" ");
	ds->treatement = my_sprintf(ds->treatement,"averaged modulation");
	op->filename = Transfer_filename(opi->filename);
	inherit_from_ds_to_ds(ds, dsi);
	over_nx /= nstep;
	uns_op_2_op(op, opi);
	refresh_plot(pr, UNCHANGED);
	return 0;
}


int extract_z_scan(void)
{
	int i, j , k;
	pltreg *pr = NULL;
	static int np = 1024, nd = 128, nn;
	O_p *op = NULL, *opd = NULL;
	d_s *dsi = NULL, *dsd = NULL;
	int  nx;

	if(updating_menu_state != 0)    return D_O_K;
	i = ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi);
	if ( i != 3)		return win_printf_OK("cannot find data");

	nx = dsi->nx;


	i = win_scanf("This routine averages signal over a time window\n"
		      "it expect a periodic modulation and extract the mean of"
		      "this signal skipping a transient at the beginning\n"
		      "The output will have a number of ponts equal the the number\n"
		      "of cycle in the signal. Define the N the nb. of points\n"
		      " corrsponding to one step including transient %8d\n"
		      "N_s nb. of points skipped corresponding to transient %8d\n"
		      "the averaging occurs on N - N_s points",&np,&nd);
	if (i == WIN_CANCEL)	return D_O_K;
	nn = nx / np;
	opd = create_and_attach_one_plot(pr, nn,nn, 0);
	if (opd == NULL)	 return win_printf_OK("Cannot allocate plot");
	dsd = opd->dat[0];
	if (dsd == NULL)	 return D_O_K;
	for (i=0 ; i< nn ; i++)
	{
	 	dsd->xd[i] = dsd->yd[i] = 0;
		k = i * np;
		for (j= nd ; j< np-1 ; j++)
		{
			dsd->yd[i] += dsi->yd[k + j];
			dsd->xd[i] += dsi->xd[k + j];
		}
		dsd->yd[i] /= (np-nd-1);
		dsd->xd[i] /= (np-nd-1);
	}
	set_plot_title(opd, "Z scan extract");
	dsd->treatement = my_sprintf(dsd->treatement,"Z scan extract");
	opd->filename = Transfer_filename(op->filename);
	uns_op_2_op(opd, op);
	inherit_from_ds_to_ds(dsd, dsi);
	refresh_plot(pr, UNCHANGED);
	return 0;
}
int extract_two_step_modulation(void)
{
	int i, j , k;
	pltreg *pr = NULL;
	static int np = 1024, nd = 128, nn;
	O_p *op = NULL, *opd = NULL;
	d_s *dsi = NULL, *dsd = NULL;
	int  nx;

	if(updating_menu_state != 0)    return D_O_K;
	i = ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi);
	if ( i != 3)		return win_printf_OK("cannot find data");

	nx = dsi->nx;


	i = win_scanf("This routine extract a periodic two states signal modulation.\n"
		      "It expects a periodic modulation and extract difference between \n"
		      "the mean signal of the two phases skipping a transient occuring \n"
		      "at the beginning of each phase. Both phases are assume symmetrical\n"
		      "The output will have a number of points equals the the number\n"
		      "of modulation cycles in the signal. Define the N the nb. of points\n"
		      "corrsponding to one cycle (having two states) including the "
		      "transients %8d\n"
		      "N_s nb. of points skipped corresponding to transient %8d\n"
		      "the averaging occurs on N/2 - N_s points",&np,&nd);
	if (i == WIN_CANCEL)	return D_O_K;
	nn = nx / np;
	opd = create_and_attach_one_plot(pr, nn,nn, 0);
	if (opd == NULL)	 return win_printf_OK("Cannot allocate plot");
	dsd = opd->dat[0];
	if (dsd == NULL)	 return D_O_K;
	for (i=0 ; i< nn ; i++)
	{
	 	dsd->xd[i] = dsd->yd[i] = 0;
		k = i * np;
		for (j= nd ; j< np/2-1 ; j++)
		  {
			dsd->yd[i] += dsi->yd[k + j];
			dsd->xd[i] += dsi->xd[k + j];
		  }
		for (j= np/2+nd ; j< np-1 ; j++)
		  {
			dsd->yd[i] -= dsi->yd[k + j];
			dsd->xd[i] += dsi->xd[k + j];
		  }
		dsd->yd[i] /= ((np/2)-nd-1);
		dsd->xd[i] /= (np-nd-1);
	}
	set_plot_title(opd, "\\stack{{Two states modulation extracted}}{period %d pts. skipping %d pts}}",np,nd);
	dsd->treatement = my_sprintf(dsd->treatement,"2 states modulation extracted");
	opd->filename = Transfer_filename(op->filename);
	uns_op_2_op(opd, op);
	inherit_from_ds_to_ds(dsd, dsi);
	refresh_plot(pr, UNCHANGED);
	return 0;
}


O_p *do_fft_and_derivate_on_op(O_p *op, int fb, int fd, int w_flag, float fc_in_hz, int frame, float factor, float *etar)
{
	int i;
	float df, x1, x, tmp;
	O_p  *opd = NULL;
	d_s *dsi = NULL, *dss = NULL;
	int  nx, xs = 0, n2;
	char c[1024] = {0};
	float dx, mean, su, fc, f;
	un_s *un = NULL;

	dsi = find_cur_data_set_in_op(op);
	if (dsi == NULL)		win_printf_ptr("bad data set");

	un = op->yu[op->c_yu];
	if (un->type != IS_METER || un->decade != IS_MICRO)
		win_printf("warning the y unit is not \\mu m !\nnumerical value will be wrong");
	un = op->xu[op->c_xu];
	if (un->type != IS_SECOND || un->decade != 0)
		win_printf("warning the x unit is not seconds !\nnumerical value will be wrong");
	//dt = un->dx;

	xs = 0;
	n2 = nx = dsi->nx;
	if (fft_init(nx))
	{
		for (n2 = 1; n2 < nx; n2 <<= 1);
		n2 >>= 1;
		sprintf(c,"Cannot init fft\n%d not a power of 2!\n I propose to treat the %%d first point",nx);
		i = win_scanf(c, &n2);
		if (i == WIN_CANCEL)	return OFF;
		nx = n2;
		if (fft_init(nx))
		{
			win_printf("Cannot init fft");
			return NULL;
		}
	}

	if (fd <= 0 || fd > nx/2 || fb < 0 || fb > nx/2 || fb > fd)
	{
		win_printf("Improper frequency setting f_b = %d f_d = %d nx = %d",fb,fd,nx/2);
		return NULL;
	}

	opd = create_one_plot( nx, nx, 0);
	dss = opd->dat[0];
	if (dss == NULL)		return NULL;

	if (w_flag & 1)
	{
		dx = 2*M_PI/nx;
		for (i=xs ,mean = 0; i< nx ; i++)
			mean += (1-cos(i*dx))*dsi->yd[i];
	}
	else
	{
		for (i=xs ,mean = 0; i< nx ; i++)		mean += dsi->yd[i];
	}
	mean /= nx;
	for (i=0 ; i< nx ; i++)
	{
		dss->xd[i] = i;
		dss->yd[i] = dsi->yd[i+xs] - mean;
	}
	if (w_flag & 1)		fftwindow(nx, dss->yd);
	realtr1(nx, dss->yd);
	fft(nx, dss->yd, 1);
	realtr2(nx, dss->yd, 1);
	spec_real (nx, dss->yd, dss->yd);

	for (i=0 ; i<= nx/2; i++)
	{
		dss->yd[i] *= (w_flag & 1) ? (op->dy * op->dy)/3 : (op->dy * op->dy)/2;
		dss->yd[i] *= i * 2 * M_PI ; /* /(nx*op->dx) */
		dss->yd[i] *= i * 2 * M_PI /(nx*op->dx);
	}
	if (w_flag & 2)		/* correct camera */
	{
		for (i=1 ; i<= nx/2; i++)
		{
			x = (float)i/nx;
			tmp = camera_response(x, frame, factor);
			/* this correction assume 1/f^2 spe and aliasing */
			tmp += x*x*camera_response(1-x, frame, factor)/((1-x)*(1-x));
			dss->yd[i] /= (tmp != 0) ? tmp : 1;
		}
	}
	dss->ny = nx/2;
	dss->nx = nx/2;
	set_plot_y_title(opd,"<v^2> (\\mu m^2.s^{-2}.Hz^{-1})");
	set_plot_x_title(opd,"frequency (Hz)");

	set_plot_x_log(opd);
	set_plot_y_log(opd);
	set_plot_title(opd, "spectre %s", (op->title == NULL)?"?":op->title);

	create_attach_select_x_un_to_op(opd, 0, 0, 1/(nx*op->dx), 0, 0, "no_name");

	fc = fc_in_hz * nx * op->dx/(M_PI * 2);
	for (i=fd-1, x1 = 0 ; i >= fb ; i--)
	{
		f = ((float)i)/fc;
		f *= f;
		x1 += (1+f)*dss->yd[i]/f;
	}

	su = x1*1e-12;
	df = (fd-fb);  /*  /(op->dx * nx) */
	su = su/df;

	*etar = 4*(1.38e-23 * 298)/(6*M_PI*su);

  	sprintf(c,"\\fbox{\\stack{{fb = %g (Hz)}{fd = %g (Hz)}{v^2 = %g (\\mu m^2.s^{-2}.Hz^{-1})}{\\eta r = %g poise.m}{f_b %d f_m %d df %g (Hz)}{fc = %g (Hz)}{window %d}{frame %d factor %f}}}",(float)fb/(nx*op->dx),(float)fd/(nx*op->dx),1e12*su,*etar,fb,fd,df,fc_in_hz,w_flag,frame,factor);
	push_plot_label(opd, dss->xd[fd], dss->yd[fd], c, USR_COORD);
	return opd;
}

int do_fft_and_derivate(void)
{
	int i;
	pltreg *pr = NULL;
	static int fb = 10, fd = 0, w_flag = 1, frame = 1;
	float etar;
	static float fc_in_hz = 1, factor = 1;
	O_p *op = NULL, *opd = NULL;
	d_s *dsi = NULL;


	if(updating_menu_state != 0)    return D_O_K;
	i = ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi);
	if ( i != 3)		return win_printf_OK("cannot find data");

	fd = dsi->nx/4;
	frame = get_config_int("camera", "mode", 0);
	w_flag |= (get_config_int("camera", "correction", 1) == 1) ? 0x2 : 0x0;
	factor = get_config_float("camera", "openning", 1);
	w_flag |= (get_config_float("spectrum", "windowing", 1) == 1) ? 0x1 : 0x0;
	i = win_scanf("low freq %d high freq %dwindow flag(1) camera correct (2) %dcut off freq (Hz) %fframe 1->yes 0-> field %dopenning factor %f",&fb,&fd,&w_flag,&fc_in_hz,&frame,&factor);
	if (i == WIN_CANCEL)	return OFF;
	set_config_int("camera", "mode", frame);
	set_config_int("camera", "correction", (w_flag&0x2)?1:0);
	set_config_float("camera", "openning", factor);
	set_config_float("spectrum", "windowing",  w_flag&0x1);

	//write_cfg(cfg);

	opd = do_fft_and_derivate_on_op(op, fb, fd, w_flag, fc_in_hz, frame, factor, &etar);
	add_data_to_pltreg (pr, IS_ONE_PLOT,(void *)opd);
	refresh_plot(pr, pr->n_op-1);
	//switch_plot(0);
	//switch_data_set(0);
	return 0;
}
int do_fft_and_derivate_2(void)
{
	int i;
	pltreg *pr = NULL;
	static int fb = 10, fd = 0, w_flag = 2, frame = 0;
	float etar, detar;
	static float fc = 100, factor = 1;
	O_p *op = NULL, *opd = NULL;
	d_s *dsi = NULL;

	if(updating_menu_state != 0)    return D_O_K;
	i = ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi);
	if ( i != 3)		return win_printf_OK("cannot find data");

	fd = dsi->nx/4;

	frame = get_config_int("camera", "mode", 0);
	w_flag |= (get_config_int("camera", "correction", 1) == 1) ? 0x2 : 0x0;
	factor = get_config_float("camera", "openning", 1);
	w_flag |= (get_config_float("spectrum", "windowing", 1) == 1) ? 0x1 : 0x0;

	i = win_scanf("low freq %d high freq %dwindow flag(1) camera correct (2) %dcut off freq (mod#) %fframe 1->yes 0-> field %dopenning factor %f",&fb,&fd,&w_flag,&fc,&frame,&factor);
	if (i == WIN_CANCEL)	return OFF;

	set_config_int( "camera", "mode", frame);
	set_config_int("camera", "correction", (w_flag&0x2)?1:0);
	set_config_float("camera", "openning", factor);
	set_config_float("spectrum", "windowing",  w_flag&0x1);


	//	write_cfg(cfg);

	opd = do_fft_and_derivate_on_op_2(op, fb, fd, w_flag, fc, frame, factor, &etar, &detar);
	add_data_to_pltreg (pr, IS_ONE_PLOT,(void *)opd);
	refresh_plot(pr, pr->n_op-1);
	//switch_plot(0);
	//switch_data_set(0);
	return 0;
}

O_p *do_fft_and_fit_high_stiffness_on_op_acq_data(O_p *op, int fb, int fd, int w_flag, float etar, float *fchz, float *stiff, int cam_fil, int frame, float factor, float T)
{
	int i, j;
	float df, x1;
	O_p  *opd = NULL;
	d_s *dsi = NULL, *dss = NULL;
	int  nx, xs = 0, xe = 0, n2;
	char c[1024] = {0};
	float dx, mean, fcu, su, kx;
	un_s *un = NULL;

	dsi = find_cur_data_set_in_op(op);
	if (dsi == NULL)		win_printf_ptr("bad data set");


	un = op->yu[op->c_yu];
	if (un->type != IS_METER || un->decade != IS_MICRO)
		win_printf("warning the y unit is not \\mu m !\nnumerical value will be wrong");
	un = op->xu[op->c_xu];
	if (un->type != IS_SECOND || un->decade != 0)
		win_printf("warning the x unit is not seconds !\nnumerical value will be wrong");
	//dt = un->dx;

	xs = 0;
	n2 = nx = xe = dsi->nx;
	if (fft_init(nx))
	{
		for (n2 = 1; n2 < nx; n2 <<= 1);
		n2 >>= 1;
		sprintf(c,"Cannot init fft\n%d not a power of 2!\n I propose to treat the %%d first point",nx);
		i = win_scanf(c, &n2);
		if (i == WIN_CANCEL)	return OFF;
		nx = n2;
		if (fft_init(nx))
		{
			win_printf("Cannot init fft");
			return NULL;
		}
	}

	if (fd <= 0 || fd > nx/2 || fb < 0 || fb > nx/2 || fb > fd)
	{
		win_printf("Improper frequency setting f_b = %d f_d = %d nx = %d",fb,fd,nx/2);
		return NULL;
	}

	opd = create_one_plot( nx, nx, 0);
	dss = opd->dat[0];
	if (dss == NULL)		return NULL;

	if (w_flag)
	{
		dx = 2*M_PI/nx;
		for (i=xs ,mean = 0; i< nx ; i++)
			mean += (1-cos(i*dx))*dsi->yd[i];
	}
	else
	{
		for (i=xs ,mean = 0; i< nx ; i++)		mean += dsi->yd[i];
	}
	mean /= nx;
	for (i=0 ; i< nx ; i++)
	{
		dss->xd[i] = i;
		dss->yd[i] = dsi->yd[i+xs] - mean;
	}
	if ( w_flag)		fftwindow(nx, dss->yd);
	realtr1(nx, dss->yd);
	fft(nx, dss->yd, 1);
	realtr2(nx, dss->yd, 1);
	spec_real (nx, dss->yd, dss->yd);
	dss->ny = nx/2;
	dss->nx = nx/2;
	set_plot_x_log(opd);
	set_plot_title(opd, "spectre %s", (op->title == NULL)?"?":op->title);





	for (i=fd, x1 = 0 ; i >= fb ; i--)
		x1 += (i == fd || i == fb) ? dss->yd[i]/2 : dss->yd[i];



	su = (x1 * op->dy * op->dy)/3;
	su *= 1e-12;
	df = (fd-fb) /(op->dx * nx);
	su = su/df;

	kx = sqrt(4*(1.38e-23 * T)*6*M_PI*etar/su);

	fcu = kx / (2*6*M_PI*M_PI*etar);

	for (j = 0; j < 5 && cam_fil != 0; j++)
	{
		compute_spe_for_acq_data(dss->nx, fcu * op->dx * nx, frame, factor, dss->xd);
		win_printf("\\fbox{\\stack{{\\sl y = a \\frac{1}{1 +(f/fc)^2}}{fc = %g (Hz)}{%g (\\mu m^2/Hz)}{k_x = %g N/m}{f_b %d f_m %d df %g (Hz) %d }}}",fcu,1e12*su,kx,fb,fd,df,j);
		for (i=fd, x1 = 0 ; i >= fb ; i--)
			x1 += (i == fd || i == fb) ? dss->yd[i]/(dss->xd[i]*2) : dss->yd[i]/dss->xd[i];

		su = (x1 * op->dy * op->dy)/3;
		su *= 1e-12;
		df = (fd-fb) /(op->dx * nx);
		su = su/df;

		kx = sqrt(4*(1.38e-23 * T)*6*M_PI*etar/su);

		fcu = kx / (2*6*M_PI*M_PI*etar);


	}

	if (cam_fil)	compute_spe_for_acq_data(dss->nx, fcu * op->dx * nx, frame, factor, dss->xd);
	for (i=0 ; i< dss->nx ; i++)
	{
		dss->yd[i] /= (cam_fil) ? dss->xd[i] : 1;
		dss->xd[i] = i;
	}

  	sprintf(c,"\\fbox{\\stack{{\\sl y = a \\frac{1}{1 +(f/fc)^2}}{fc = %g (Hz)}{%g (\\mu m^2/Hz)}{k_x = %g N/m}{f_b %d f_m %d df %g (Hz)}}}",fcu,1e12*su,kx,fb,fd,df);
	push_plot_label(opd, dss->xd[fd], dss->yd[fd], c, USR_COORD);
	if (fchz != NULL)	*fchz = fcu;
	if (stiff != NULL)	*stiff = kx;
	return opd;
}

O_p *do_fft_and_fit_high_stiffness_on_op(O_p *op, int fb, int fd, int w_flag, float etar, float *fchz, float *stiff, int cam_fil, int frame, float factor, float T)
{
	int i;
	float df, x1;
	O_p  *opd = NULL;
	d_s *dsi = NULL, *dss = NULL;
	int  nx, xs = 0, xe = 0, n2;
	char c[1024]  = {0};
	float dx, mean, fcu, su, kx, tmp; // , x
	un_s *un = NULL;

	dsi = find_cur_data_set_in_op(op);
	if (dsi == NULL)		win_printf_ptr("bad data set");


	un = op->yu[op->c_yu];
	if (un->type != IS_METER || un->decade != IS_MICRO)
		win_printf("warning the y unit is not \\mu m !\nnumerical value will be wrong");
	un = op->xu[op->c_xu];
	if (un->type != IS_SECOND || un->decade != 0)
		win_printf("warning the x unit is not seconds !\nnumerical value will be wrong");
	//dt = un->dx;

	xs = 0;
	n2 = nx = xe = dsi->nx;
	if (fft_init(nx))
	{
		for (n2 = 1; n2 < nx; n2 <<= 1);
		n2 >>= 1;
		sprintf(c,"Cannot init fft\n%d not a power of 2!\n I propose to treat the %%d first point",nx);
		i = win_scanf(c, &n2);
		if (i == WIN_CANCEL)	return OFF;
		nx = n2;
		if (fft_init(nx))
		{
			win_printf("Cannot init fft");
			return NULL;
		}
	}

	if (fd <= 0 || fd > nx/2 || fb < 0 || fb > nx/2 || fb > fd)
	{
		win_printf("Improper frequency setting f_b = %d f_d = %d nx = %d",fb,fd,nx/2);
		return NULL;
	}

	opd = create_one_plot( nx, nx, 0);
	dss = opd->dat[0];
	if (dss == NULL)		return NULL;

	if (w_flag)
	{
		dx = 2*M_PI/nx;
		for (i=xs ,mean = 0; i< nx ; i++)
			mean += (1-cos(i*dx))*dsi->yd[i];
	}
	else
	{
		for (i=xs ,mean = 0; i< nx ; i++)		mean += dsi->yd[i];
	}
	mean /= nx;
	for (i=0 ; i< nx ; i++)
	{
		dss->xd[i] = i;
		dss->yd[i] = dsi->yd[i+xs] - mean;
	}
	if ( w_flag)		fftwindow(nx, dss->yd);
	realtr1(nx, dss->yd);
	fft(nx, dss->yd, 1);
	realtr2(nx, dss->yd, 1);
	spec_real (nx, dss->yd, dss->yd);
	for (i=1 ; (cam_fil != 0) && (i<= nx/2); i++)	/* correct camera */
	{
	  //for (i=1 ; i<= nx/2; i++)
	  //{
	  //x = i * M_PI/nx;
	  tmp = camera_response((float)i/nx, frame, factor);
	  tmp += camera_response(1.0 - ((float)i/nx), frame, factor);
	  dss->yd[i] /= (tmp != 0) ? tmp : 1;
	  //}
	}

	dss->ny = nx/2;
	dss->nx = nx/2;
	set_plot_x_log(opd);
	set_plot_title(opd, "spectre %s", (op->title == NULL)?"?":op->title);



	for (i=fd-1, x1 = 0 ; i >= fb ; i--)
		x1 += dss->yd[i];



	su = (x1 * op->dy * op->dy)/3;
	su *= 1e-12;
	df = (fd-fb) /(op->dx * nx);
	su = su/df;

	kx = sqrt(4*(1.38e-23 * T)*6*M_PI*etar/su);

	fcu = kx / (2*6*M_PI*M_PI*etar);

  	sprintf(c,"\\fbox{\\stack{{\\sl y = a \\frac{1}{1 +(f/fc)^2}}{fc = %g (Hz)}{%g (\\mu m^2/Hz)}{k_x = %g N/m}{f_b %d f_m %d df %g (Hz)}}}",fcu,1e12*su,kx,fb,fd,df);
	push_plot_label(opd, dss->xd[fd], dss->yd[fd], c, USR_COORD);
	if (fchz != NULL)	*fchz = fcu;
	if (stiff != NULL)	*stiff = kx;
	return opd;
}


int do_fft_and_fit_high_stiffness(void)
{
	int i;
	pltreg *pr = NULL;
	static int fb = 10, fd = 0, w_flag = 1, frame = 1, cam_fil = 0;
	static float etar = 1.5*1e-9, factor = 1, T = 298;
	float stiff = 0, fchz = 0;
	O_p *op = NULL, *opd = NULL;
	d_s *dsi = NULL;

	if(updating_menu_state != 0)    return D_O_K;
	i = ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi);
	if ( i != 3)		return win_printf_OK("cannot find data");

	fd = dsi->nx/4;
	frame = get_config_int( "camera", "mode", 0);
	cam_fil = get_config_int( "camera", "correction", 1);
	factor = get_config_float( "camera", "openning", 1);
	fb = get_config_float( "spectrum", "lower_frequency", 10);
	w_flag = get_config_float( "spectrum", "windowing", 1);
	T = get_config_float( "spectrum", "temperature", 298);
	etar = get_config_float( "spectrum", "etar", 1.5*1e-9);


	i = win_scanf("low freq %d high freq %dflag %d etar %fcamera correction ->1 yes ->0 no %dframe int. ->1 field -> 0%dopening factor %fTemperature %f",&fb,&fd,&w_flag,&etar,&cam_fil,&frame,&factor,&T);
	if (i == WIN_CANCEL)	return OFF;
	set_config_int( "camera", "mode", frame);
	set_config_int( "camera", "correction", cam_fil);
	set_config_float( "camera", "openning", factor);
	set_config_float( "spectrum", "lower_frequency", fb);
	set_config_float( "spectrum", "windowing", w_flag);
	set_config_float( "spectrum", "temperature", T);
	set_config_float( "spectrum", "etar", etar);
	//write_cfg(cfg);
	opd = do_fft_and_fit_high_stiffness_on_op_acq_data(op, fb, fd, w_flag, etar, &fchz, &stiff, cam_fil, frame, factor,T);
	add_data_to_pltreg (pr, IS_ONE_PLOT,(void *)opd);
	refresh_plot(pr, pr->n_op-1);
	return 0;
}

int remove_mean_ds(void)
{
	int i;
	pltreg *pr = NULL;
	O_p *op = NULL;
	d_s *dsi = NULL;
	int  nx;
	float tmp, dx;

	if(updating_menu_state != 0)    return D_O_K;
	i = ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi);
	if ( i != 3)	return win_printf_OK("cannot find data");
	nx = dsi->nx;
	dx = 2*M_PI/nx;
	for (i=0 ,tmp = 0; i< nx ; i++)
		tmp += (1-cos(i*dx))*dsi->yd[i];
	tmp /= nx;
	for (i=0; i< nx ; i++)		dsi->yd[i] -= tmp;
	op->need_to_refresh = 1;
	return refresh_plot(pr, UNCHANGED);
}



int	draw_delta_de_fc_4_and_force(void)
{

	int i;
	d_s *ds = NULL;
	O_p *op = NULL, *opd = NULL;
	pltreg *pr = NULL;
	int flow, index;
	float fc, a, su, kx, etar, etar2, detar2, dy, dt, da, dfc, chi2, dy0, mean;
	static int	nx = 1024, w_flag = 0, xv_sav = 0, next = 1;
	static int fm = 1024, fb = 10, cam_fil = 1, frame = 0;
	static float factor = 1;
	//	static char *log = NULL;
	int file_index = -1, n_warn = 0;
	static int auto_file_index = 0;
	char *filename = NULL, *ctmp = NULL, buf[2048] = {0}, warning[1024] = {0}, file[256] = {0}, zmags[128] = {0};
	un_s *un = NULL;
	float zmag, rot;
	int nxo, nyo, nx_2;

	if(updating_menu_state != 0)	return D_O_K;
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
		return win_printf_OK("cannot find data");
	nxo = nx = ds->nx;
	nyo = ds->ny;
	warning[0] = 0;
	for (nx_2 = 2; nx_2 <= nx; nx_2 *= 2);
	nx_2 /= 2;
	if (nx_2 != nx)
	  {
	    //	    win_printf("Your data set size %d is not a power of 2!\n"
	    //         "I am going to analyse only the %d first points",nx,nx_2);
	    sprintf(warning+strlen(warning),
		    "{\\color{Yellow}warning:Your data set size %d is not a power of 2!}\n"
                       "{\\color{Yellow}I am going to analyse only the %d first points}\n",nx,nx_2);
	    n_warn++;
	    nx = ds->nx = ds->ny = nx_2;
	  }

	fm = nx/2;
	index = RETRIEVE_MENU_INDEX;


	frame = get_config_int("camera", "mode", 0);
	cam_fil = get_config_int("camera", "correction", 1);
	factor = get_config_float("camera", "openning", 1);
	w_flag = get_config_int("spectrum", "windowing", 1);
	fb = get_config_int("spectrum", "lower_frequency", 10);

	un = op->yu[op->c_yu];
	if (un->type != IS_METER || un->decade != IS_MICRO)
	  {
	    sprintf(warning+strlen(warning),"{\\color{Yellow}warning the y unit is not \\mu m !\n"
		    "numerical value will be wrong}\n");
	    n_warn++;
	//win_printf("warning the y unit is not \\mu m !\n"
	//"numerical value will be wrong");
	  }
	dy = un->dx;
	un = op->xu[op->c_xu];
	if (un->type != IS_SECOND || un->decade != 0)
	  {
	    sprintf(warning+strlen(warning),"{\\color{Yellow}warning the x unit is not seconds !\n"
		    "numerical value will be wrong}\n");
	    n_warn++;
	  }
	//		win_printf("warning the x unit is not seconds !\n"
	//"numerical value will be wrong");
	dt = un->dx * (ds->xd[ds->nx-1] - ds->xd[0]);
	dt = (ds->nx > 1) ? dt/(ds->nx-1) : dt;

	sprintf(buf,"%s\n\n\\frac{f_c [log(1+x^2)]_{fb/fc}^{fm/fc}}"
		"{2 [arctang(x)]_{fb/fc}^{fm/fc}}\n\nData set %d/%d fb %%6d fm %%6d nx %%6d\n"
		"windowing  no%%R yes%%r\ncamera correction no%%R yes%%r\n"
		"Camera integration mode: field ->%%R frame ->%%r\nopening factor %%6f\n"
		,warning,op->cur_dat,op->n_dat);
	i = win_scanf(buf,&fb,&fm,&nx,&w_flag,&cam_fil, &frame,&factor);
	if (i == WIN_CANCEL)
	  {
	    ds->nx = nxo; ds->ny = nyo;
	    return 0;
	  }

	for (i = 0, mean = 0; i < ds->nx; i++)
	  mean += ds->yd[i];
	mean /= ds->nx;
	mean *= dy;
	set_config_int("camera", "mode", frame);
	set_config_int("camera", "correction", cam_fil);
	set_config_float("camera", "openning", factor);
	set_config_int("spectrum", "windowing", w_flag);
	set_config_int("spectrum", "lower_frequency", fb);
	//	write_config(config);


	alternate_lorentian_fit_acq_spe_with_error(ds->yd, ds->ny, dy, fb, fm,
		w_flag, cam_fil, factor, frame, &fc, &a, &dfc, &da, &chi2, &dy0);
	su = a * M_PI_2;
	kx = (1.38e-11 * 298)/su;
	etar = kx*nx*dt/(12*M_PI*M_PI*fc);
	flow = (int)(2*fc);
	flow = (flow > nx/4) ? nx/4 : flow;
	flow = (flow < 2*fb) ? 2*fb : flow;
	opd = do_fft_and_derivate_on_op_2(op, flow, fm, w_flag+(cam_fil<<1), fc,
		frame, factor, &etar2, &detar2);
	free_one_plot(opd);
	detar2 = (etar2 != 0) ? detar2/etar2 : detar2;

	if ((current_write_open_xvfile == NULL) ||
	    (extract_file_name(file, 256, current_write_open_xvfile) == NULL))
	      sprintf(file,"a new xvplot file");

	zmag = -1;
	rot = -10000;
	zmags[0] = 0;
	if (ds->source != NULL)
	  {
	    ctmp = strstr(ds->source,"Zmag =");
	    if (ctmp != NULL)
		sscanf(ctmp,"Zmag = %g Rot %g",&zmag,&rot);
	    else
	      {
		ctmp = strstr(ds->source,"Z magnets ");
		if (ctmp != NULL)
		    sscanf(ctmp,"Z magnets %g",&zmag);
		ctmp = strstr(ds->source,"Rotation ");
		if (ctmp != NULL)
		  sscanf(ctmp,"Rotation %g",&rot);
	      }
	    sprintf(zmags,"Zmag = %g Rot = %g\n",zmag, rot);
	  }
	filename = op->filename;
	for (i = 0; filename[i] && filename[i] != '.'; i++);
	sscanf(filename + (((i - 3) >= 0) ? i - 3 : 0),"%d",&file_index);


	sprintf(buf,"analysing %s%s\n data set %d/%d with nx %d fb %d fm %d %swindowing \n"
		"%s camera correction %s factor %f\n%ssu  %g \\mu m^2 kx = %g N/m \n"
		"a = %g +/- %g %%\nfc %g (raw) +/- %g %%%% %g (Hz) \\eta r = %g\n"
		" \\eta_1 = %g +/- %g %% fb = %d \\chi^2 = %g n = %d\n"
		"mean %g error on mean %g \\mu m\n"
		"do you want to dump these result in %s No->%%R Yes->%%r\n"
		"treat next data set%%R stop%%r previous%%r"
	,backslash_to_slash(op->dir),backslash_to_slash(op->filename),op->cur_dat,op->n_dat,nx,fb,fm,
	(w_flag)?" ":"no ",(cam_fil)? " ":"no ",(frame)?"frame int":"field int",
	factor,zmags,su,kx,a,(a!=0)?100*da/a:da,fc,(fc!=0)?100*dfc/fc:dfc,fc/(nx*dt),
	etar, etar2,100*detar2,flow,chi2,fm-fb-2,mean,dy0,file);
	i = win_scanf(buf,&xv_sav,&next);
	if (xv_sav)
	{
		if (log_cardinal == -1 || index == 1)
		{
			if (do_create_xvplot_file(NULL) == NULL)
			  {
			    ds->nx = nxo; ds->ny = nyo;
			    return 0;
			  }
			log_cardinal++;
			auto_file_index = 0;
			dump_to_current_xvplot_file(
			"%%index\t{\\delta}x^2\td{\\delta}x^2\tk_x\tdk_x\tf_c(Hz)\tdf_c/f_c"
			"\t{\\eta}r\t{\\eta}r2\td{\\eta}r2\td<x>\t<x>\tZmag\tRot");
		}
		if (file_index < 0)    file_index = auto_file_index++;
		dump_to_current_xvplot_file( "%d\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g"
		"\t%g\t%g\t%g\t%g\t%g",file_index,su,(a!=0)?su*da/a:su,kx,(a!=0)?kx*da/a:kx,
		fc/(nx*dt),(fc!=0)?100*dfc/fc:dfc,etar,etar2,etar2*detar2,dy0,mean,zmag,rot);
	}
	ds->nx = nxo; ds->ny = nyo;
	if (next == 0)
	  {
	    op->cur_dat++;
	    if (op->cur_dat >= op->n_dat) op->cur_dat = 0;
	    return 1;
	  }
	if (next == 2)
	  {
	    op->cur_dat--;
	    if (op->cur_dat < 0) op->n_dat =  op->cur_dat;
	    return 1;
	  }
	return 0;
}



int reload_hat_data_and_force(void)
{
  char *st = NULL, basename[256] = {0}, warning[1024] = {0}, file[1024] = {0}, buf[2048] = {0}, zmags[128] = {0}, path[512] = {0};
  int findex;
  int flow;
  pltreg *pr = NULL;
  O_p *op = NULL, *opn = NULL, *op2 = NULL, *opd = NULL;
  d_s *ds, *dsx = NULL, *dsy = NULL, *dsz = NULL, *dsn = NULL, *dsm = NULL, *dsm2 = NULL, *dsz2 = NULL, *dsf = NULL, *dsfc = NULL;
  int nf, nstep, i, j, k, ki;
  float rots, step, zmag;
  float fc, a, su, kx, etar, etar2, detar2, dy, dt, da, dfc, chi2, dy0, mean;
  static int	nx = 1024, w_flag = 0, x_or_y = 0;
  static int fm = 1024, fb = 10, cam_fil = 1, frame = 0;
  static float factor = 1, z_offset;
  un_s *un = NULL;
  int  nx_2;
  int n_warn = 0;

 if(updating_menu_state != 0)	return D_O_K;

  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
    return win_printf("cannot find data");

  if (ds->source == NULL) return win_printf_OK("this is not a hat curve");
  if (strstr(ds->source,"Chapeau 2 beads") != NULL)
    {
      st = strstr(ds->source,"Z mag start");
      if (st == NULL)
	return win_printf_OK("this is not a good hat curve");
      if (sscanf(st,"Z mag start = %f for %d frames scan by steps of %f for %d"
		 ,&rots,&nf,&step,&nstep) != 4)
	return win_printf_OK("problem recovering parameters from hat curve");

    }
  st = strstr(op->title,", rot");
  if (st == NULL)
    return win_printf_OK("this is not a good hat curve title");
  if (sscanf(st,", rot %f",&zmag) != 1)
    return win_printf_OK("problem recovering zmag from hat curve");
  findex = retrieve_basename_and_index(op->filename, strlen(op->filename), basename, 256);

  strcpy(path,op->dir);
  put_backslash(path);

  sprintf(file,"traca%03d.gr",findex);
  opn = create_plot_from_gr_file(file, path);
  if (opn == NULL) win_printf_OK("Could not loaded %s\n from %s",
				 backslash_to_slash(file), backslash_to_slash(op->dir));
  //add_data_to_pltreg (pr, IS_ONE_PLOT, (void *)opn);
  for (i = 0; i < opn->n_dat; i++)
    {
      dsx = opn->dat[i];
      if (dsx->source == NULL) continue;
      st = strstr(dsx->source,"X coordinate");
      if (st != NULL) break;
    }
  if (st == NULL || dsx == NULL)
    return win_printf_OK("problem recovering X coordinate");
  for (i = 0; i < opn->n_dat; i++)
    {
      dsy = opn->dat[i];
      if (dsy->source == NULL) continue;
      st = strstr(dsy->source,"Y coordinate");
      if (st != NULL) break;
    }
  if (st == NULL || dsy == NULL)
    return win_printf_OK("problem recovering Y coordinate");

  for (i = 0; i < opn->n_dat; i++)
    {
      dsz = opn->dat[i];
      if (dsz->source == NULL) continue;
      st = strstr(dsz->source,"Z coordinate");
      if (st != NULL) break;
    }
  if (st == NULL || dsz == NULL)
    return win_printf_OK("problem recovering Z coordinate");


  sprintf(file,"tracb%03d.gr",findex);
  op2 = create_plot_from_gr_file(file, path);
  if (op2 == NULL) win_printf_OK("Could not loaded %s\n from %s",
				 backslash_to_slash(file), backslash_to_slash(op->dir));
  for (i = 0; i < op2->n_dat; i++)
    {
      dsz2 = op2->dat[i];
      if (dsz2->source == NULL) continue;
      st = strstr(dsz2->source,"Z coordinate");
      if (st != NULL) break;
    }
  if (st == NULL || dsz2 == NULL)
    return win_printf_OK("problem recovering Z coordinate");


  i = win_scanf("which horizontal direction\ndo you want to use to\n"
		"measure forece %R X %r Y\n",&x_or_y);

  if (i == WIN_CANCEL) return D_O_K;
  if (x_or_y == 0) dsy = dsx;


  nx = nf; // nxo =
  warning[0] = 0;
  for (nx_2 = 2; nx_2 <= nx; nx_2 *= 2);
  nx_2 /= 2;
  if (nx_2 != nx)
    {
      sprintf(warning+strlen(warning),
	      "{\\color{Yellow}warning:Your data set size %d is not a power of 2!}\n"
	      "{\\color{Yellow}I am going to analyse only the %d first points}\n",nx,nx_2);
      n_warn++;
      nx = nx_2;
    }
  fm = nx/2;


  frame = get_config_int("camera", "mode", 0);
  cam_fil = get_config_int("camera", "correction", 1);
  factor = get_config_float("camera", "openning", 1);
  w_flag = get_config_int("spectrum", "windowing", 1);
  fb = get_config_int("spectrum", "lower_frequency", 10);

  un = opn->yu[opn->c_yu];
  if (un->type != IS_METER || un->decade != IS_MICRO)
    {
      sprintf(warning+strlen(warning),"{\\color{Yellow}warning the y unit is not \\mu m !\n"
	      "numerical value will be wrong}\n");
      n_warn++;
    }
  dy = un->dx;
  un = opn->xu[opn->c_xu];
  if (un->type != IS_SECOND || un->decade != 0)
    {
      sprintf(warning+strlen(warning),"{\\color{Yellow}warning the x unit is not seconds !\n"
	      "numerical value will be wrong}\n");
	    n_warn++;
    }
  dt = un->dx;

  sprintf(buf,"%s\n\n\\frac{f_c [log(1+x^2)]_{fb/fc}^{fm/fc}}"
	  "{2 [arctang(x)]_{fb/fc}^{fm/fc}}\n\nData sets %d fb %%6d fm %%6d nx %%6d\n"
	  "windowing  no%%R yes%%r\ncamera correction no%%R yes%%r\n"
	  "Camera integration mode: field ->%%R frame ->%%r\nopening factor %%6f\n"
	  "Offset to substract to molecule extension %%8f\n"
	  ,warning,ds->nx);
  i = win_scanf(buf,&fb,&fm,&nx,&w_flag,&cam_fil, &frame,&factor,&z_offset);
  if (i == WIN_CANCEL)
    {
      return 0;
    }


  set_config_int("camera", "mode", frame);
  set_config_int("camera", "correction", cam_fil);
  set_config_float("camera", "openning", factor);
  set_config_int("spectrum", "windowing", w_flag);
  set_config_int("spectrum", "lower_frequency", fb);



  if (do_create_xvplot_file(NULL) == NULL)
    {
      return 0;
    }
  log_cardinal++;
  dump_to_current_xvplot_file( "%%index\t{\\delta}x^2\td{\\delta}x^2\tk_x\tdk_x\tf_c(Hz)\tdf_c/f_c"
			      "\t{\\eta}r\t{\\eta}r2\td{\\eta}r2\td<x>\t<x>\tZmag\tRot\tBead_Z\tFix_Z\tZ_offset");



  dsm = create_and_attach_one_ds(op, ds->nx, ds->nx, 0);
  if (dsm == NULL) 	 return win_printf("cannot create ds !");
  set_ds_source(dsm,"Mean Z value for bead");
  //set_plot_symb(dsm,"\\pt5\\oc ");
  //set_ds_dot_line(dsm);

  dsm2 = create_and_attach_one_ds(op, ds->nx, ds->nx, 0);
  if (dsm2 == NULL) 	 return win_printf("cannot create ds !");
  set_ds_source(dsm2,"Mean Z value for fix bead");
  //set_plot_symb(dsm2,"\\pt5\\oc ");
  //set_ds_dot_line(dsm2);

  dsfc = create_and_attach_one_ds(op, ds->nx, ds->nx, 0);
  if (dsfc == NULL) 	 return win_printf("cannot create ds !");
  set_ds_source(dsfc,"Cutoff frequency");

  dsf = create_and_attach_one_ds(op, ds->nx, ds->nx, 0);
  if (dsf == NULL) 	 return win_printf("cannot create ds !");
  set_plot_symb(dsf,"\\pt5\\oc ");
  alloc_data_set_y_error(dsf);
  set_ds_dot_line(dsf);
  set_ds_source(dsf,"Force versus rotation");

  for (i = 0, opn->cur_dat = opn->n_dat-1; i < ds->nx; i++)
    {
      j = (int)(0.5 + (ds->xd[i] - rots)/step);
      dsn = create_and_attach_one_ds(opn, nf, nf, 0);
      if (dsn == NULL) 	 return win_printf("cannot create ds !");
      grey = 224-(j*192)/nstep;
      set_ds_line_color(dsn, makecol(grey,grey,grey));
      set_ds_source(dsn,"Partial trajectory at Zmag = %g Rot %g fileindex %d basename %s",
		    zmag,ds->xd[i],findex,basename);
      set_ds_dot_line(dsn);
      for(k = 0, ki = j * nf; k < nx && ki < dsy->nx ; k++, ki++)
	{
	  dsn->xd[k] = dsy->xd[ki];
	  dsn->yd[k] = dsy->yd[ki];
	  dsm->yd[i] += dsz->yd[ki];
	  dsm2->yd[i] += dsz2->yd[ki];
	}
      dsm->yd[i] /= nx;
      dsm->yd[i] *= dy  * 0.878;
      dsm->xd[i] = ds->xd[i];
      dsm2->yd[i] /= nx;
      dsm2->yd[i] *= dy * 0.878;
      dsm2->xd[i] = ds->xd[i];
      dsf->xd[i] = ds->xd[i];
      dsfc->xd[i] = ds->xd[i];

      for (k = 0, mean = 0; k < dsn->nx; k++)
	mean += dsn->yd[i];
      mean /= dsn->nx;
      mean *= dy;
      dsn->nx = dsn->ny = nx;
      alternate_lorentian_fit_acq_spe_with_error(dsn->yd, nx, dy, fb, fm,
		w_flag, cam_fil, factor, frame, &fc, &a, &dfc, &da, &chi2, &dy0);
      su = a * M_PI_2;
      kx = (1.38e-11 * 298)/su;
      etar = kx*nx*dt/(12*M_PI*M_PI*fc);
      flow = (int)(2*fc);
      flow = (flow > nx/4) ? nx/4 : flow;
      flow = (flow < 2*fb) ? 2*fb : flow;
      opn->cur_dat = opn->n_dat-1;
      opd = do_fft_and_derivate_on_op_2(opn, flow, fm, w_flag+(cam_fil<<1), fc,
		frame, factor, &etar2, &detar2);
      free_one_plot(opd);
      detar2 = (etar2 != 0) ? detar2/etar2 : detar2;

      sprintf(zmags,"Zmag = %g Rot = %g\n",zmag, ds->xd[i]);
      /*
      if (fabs(ds->xd[i]) < 20) win_printf("analysing %s%s\n data set %d/%d with nx %d fb %d fm %d %swindowing \n"
		"%s camera correction %s factor %f\n%ssu  %g \\mu m^2 kx = %g N/m \n"
		"a = %g +/- %g %%\nfc %g (raw) +/- %g %% %g (Hz) \\eta r = %g\n"
		" \\eta_1 = %g +/- %g %% fb = %d \\chi^2 = %g n = %d\n"
		"mean %g error on mean %g \\mu m\n"
	,backslash_to_slash(opn->dir),backslash_to_slash(opn->filename),op->cur_dat,op->n_dat,nx,fb,fm,
	(w_flag)?" ":"no ",(cam_fil)? " ":"no ",(frame)?"frame int":"field int",
	factor,zmags,su,kx,a,(a!=0)?100*da/a:da,fc,(fc!=0)?100*dfc/fc:dfc,fc/(nx*dt),
	etar, etar2,100*detar2,flow,chi2,fm-fb-2,mean,dy0);
      */


      dump_to_current_xvplot_file( "%d\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g",
				   findex,su,(a!=0)?su*da/a:su,kx,(a!=0)?kx*da/a:kx,fc/(nx*dt),
				   (fc!=0)?100*dfc/fc:dfc,etar,etar2,etar2*detar2,dy0,mean,zmag,ds->xd[i],
				   dsm->yd[i],dsm2->yd[i],z_offset);
      dsfc->yd[i] = fc/(nx*dt);

      dsf->yd[i] = kx * 1000000 * ((dsm->yd[i] - dsm2->yd[i]) - z_offset);
      dsf->ye[i] = (a!=0)?dsf->yd[i]*da/a:0;

    }
  free_one_plot(opn);
  free_one_plot(op2);
  opn->need_to_refresh = 1;
  refresh_plot(pr, UNCHANGED);
  return 0;
}


// new tracking
int reload_hat_data_and_force_2(void)
{
  char *st = NULL, basename[256] = {0}, warning[1024] = {0}, file[1024] = {0}, buf[2048] = {0}, zmags[128] = {0}, path[512] = {0};
  int findex;
  int flow;
  pltreg *pr = NULL;
  O_p *op = NULL, *opn = NULL, *op2 = NULL, *opd = NULL, *oprot = NULL, *opk = NULL, *opf = NULL;
  d_s *ds = NULL, *dsx = NULL, *dsy = NULL, *dsz = NULL, *dsn = NULL, *dska = NULL, *dskr = NULL;
  d_s *dsma = NULL, *dsmr = NULL, *dsm2a = NULL, *dsm2r = NULL, *dsz2 = NULL, *dsfa, *dsfr = NULL;
  d_s *dsfca = NULL, *dsfcr = NULL, *dsrot = NULL, *dsrot2 = NULL;
  int nf, nstep, i, j, k, ki, dead, bd_index, bdfix_index = 0;
  float rots, step, zmag, min, max;
  float fc, a, su, kx, etar, etar2, detar2, dy, dt, da, dfc, chi2, dy0, mean;
  static int	nx = 1024, w_flag = 0, x_or_y = 0;
  static int fm = 1024, fb = 10, cam_fil = 1, frame = 0;
  static float factor = 1, z_offset;
  un_s *un = NULL;
  int   nx_2, min_i, max_i;
  int n_warn = 0, up_dwn = 0, n_size;

 if(updating_menu_state != 0)	return D_O_K;

  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
    return win_printf("cannot find data");

  /*
       set_ds_source(ds,"Hat Z\nZmag %g Rot start = %g for %d frames hat by steps of %g"
		     " for %d steps \ndead period %d\n"
		     "Bead  calbration %s \n"
		     ,zmag,rot_start,nper,rot_step,nstep
		     ,dead,bt->calib_im->filename);

  */
  for(i = min_i = max_i = 1, min = max = 0; i < ds->nx; i++)
    {
      if ((ds->xd[i] - ds->xd[i-1]) > max)
	  max = ds->xd[max_i = i] - ds->xd[i-1];
      if ((ds->xd[i] - ds->xd[i-1]) < min)
	min = ds->xd[min_i = i] - ds->xd[i-1];
    }
  up_dwn = ((max * min) < 0) ? 1 : 0;
  if (ds->source == NULL) return win_printf_OK("this is not a hat curve");
  if (strstr(ds->source,"Hat Z") != NULL)
    {
       i = sscanf(ds->source,"Hat Z\nZmag %f Rot start = %f for %d frames hat by steps of %f"
		     " for %d steps \ndead period %d\n",&zmag,&rots,&nf,&step,&nstep,&dead);
       if (i != 6)
	return win_printf_OK("problem recovering parameters from hat curve");
    }
  findex = retrieve_basename_and_index(op->filename, strlen(op->filename), basename, 256);


  //return win_printf_OK("file index = %d basename %s",findex,basename);
  bd_index = findex/1000;
  findex = findex%1000;
  strcpy(path,op->dir);
  put_backslash(path);
  sprintf(file,"X(t)Y(t)Z(t)bd%dhat%d.gr",bd_index,findex);
  opn = create_plot_from_gr_file(file, path);
  if (opn == NULL) win_printf_OK("Could not loaded %s\n from %s",
				 backslash_to_slash(file), backslash_to_slash(op->dir));
  add_data_to_pltreg (pr, IS_ONE_PLOT, (void *)opn);
  for (i = 0; i < opn->n_dat; i++)
    {
      dsx = opn->dat[i];
      if (dsx->source == NULL) continue;
      st = strstr(dsx->source,"X coordinate");
      if (st != NULL) break;
    }
  if (st == NULL || dsx == NULL)
    return win_printf_OK("problem recovering X coordinate");
  for (i = 0; i < opn->n_dat; i++)
    {
      dsy = opn->dat[i];
      if (dsy->source == NULL) continue;
      st = strstr(dsy->source,"Y coordinate");
      if (st != NULL) break;
    }
  if (st == NULL || dsy == NULL)
    return win_printf_OK("problem recovering Y coordinate");

  for (i = 0; i < opn->n_dat; i++)
    {
      dsz = opn->dat[i];
      if (dsz->source == NULL) continue;
      st = strstr(dsz->source,"Z coordinate");
      if (st != NULL) break;
    }
  if (st == NULL || dsz == NULL)
    return win_printf_OK("problem recovering Z coordinate");

  i = win_scanf("Indicate the index of the fixed bead %d \n < 0 skip fixed bead",&bdfix_index);
  if (i == WIN_CANCEL) return D_O_K;

  if (bdfix_index >= 0)
    {
      sprintf(file,"X(t)Y(t)Z(t)bd%dhat%d.gr",bdfix_index,findex);

      //sprintf(file,"tracb%03d.gr",findex);
      op2 = create_plot_from_gr_file(file, path);
      if (op2 == NULL) win_printf_OK("Could not loaded %s\n from %s",
				     backslash_to_slash(file), backslash_to_slash(op->dir));
      for (i = 0; i < op2->n_dat; i++)
	{
	  dsz2 = op2->dat[i];
	  if (dsz2->source == NULL) continue;
	  st = strstr(dsz2->source,"Z coordinate");
	  if (st != NULL) break;
	}
      if (st == NULL || dsz2 == NULL)
	return win_printf_OK("problem recovering Z coordinate");
    }
  sprintf(file,"bddzr%03d.gr",findex);
  oprot = create_plot_from_gr_file(file, path);
  if (oprot == NULL) win_printf_OK("Could not loaded %s\n from %s",
				 backslash_to_slash(file), backslash_to_slash(op->dir));
  add_data_to_pltreg (pr, IS_ONE_PLOT, (void *)oprot);
  dsrot = oprot->dat[0];


  if (up_dwn)
    {
      i = win_scanf("which horizontal direction\ndo you want to use to\n"
		    "measure forece %R X %r Y\n"
		    "Do you want to separate back and forth scan\n"
		    "No %R yes %r\n",&x_or_y,&up_dwn);
    }
  else
    {
      i = win_scanf("which horizontal direction\ndo you want to use to\n"
		    "measure forece %R X %r Y\n",&x_or_y);
    }

  if (i == WIN_CANCEL) return D_O_K;
  if (x_or_y == 0) dsy = dsx;


  nx = nf; // nxo =
  warning[0] = 0;
  for (nx_2 = 2; nx_2 <= nx; nx_2 *= 2);
  nx_2 /= 2;
  if (nx_2 != nx)
    {
      sprintf(warning+strlen(warning),
	      "{\\color{Yellow}warning:Your data set size %d is not a power of 2!}\n"
	      "{\\color{Yellow}I am going to analyse only the %d first points}\n",nx,nx_2);
      n_warn++;
      nx = nx_2;
    }
  fm = nx/2;


  frame = get_config_int("camera", "mode", 0);
  cam_fil = get_config_int("camera", "correction", 1);
  factor = get_config_float("camera", "openning", 1);
  w_flag = get_config_int("spectrum", "windowing", 1);
  fb = get_config_int("spectrum", "lower_frequency", 10);

  un = opn->yu[opn->c_yu];
  if (un->type != IS_METER || un->decade != IS_MICRO)
    {
      sprintf(warning+strlen(warning),"{\\color{Yellow}warning the y unit is not \\mu m !\n"
	      "numerical value will be wrong}\n");
      n_warn++;
    }
  dy = un->dx;
  un = opn->xu[opn->c_xu];
  if (un->type != IS_SECOND || un->decade != 0)
    {
      sprintf(warning+strlen(warning),"{\\color{Yellow}warning the x unit is not seconds !\n"
	      "numerical value will be wrong}\n");
	    n_warn++;
    }
  dt = un->dx;

  sprintf(buf,"%s\n\n\\frac{f_c [log(1+x^2)]_{fb/fc}^{fm/fc}}"
	  "{2 [arctang(x)]_{fb/fc}^{fm/fc}}\n\nData sets %d fb %%6d fm %%6d nx %%6d\n"
	  "windowing  no%%R yes%%r\ncamera correction no%%R yes%%r\n"
	  "Camera integration mode: field ->%%R frame ->%%r\nopening factor %%6f\n"
	  "Offset to substract to molecule extension %%8f\n"
	  ,warning,ds->nx);
  i = win_scanf(buf,&fb,&fm,&nx,&w_flag,&cam_fil, &frame,&factor,&z_offset);
  if (i == WIN_CANCEL)
    {
      return 0;
    }


  set_config_int("camera", "mode", frame);
  set_config_int("camera", "correction", cam_fil);
  set_config_float("camera", "openning", factor);
  set_config_int("spectrum", "windowing", w_flag);
  set_config_int("spectrum", "lower_frequency", fb);



  if (do_create_xvplot_file(NULL) == NULL)
    {
      return 0;
    }
  log_cardinal++;
  dump_to_current_xvplot_file( "%%index\t{\\delta}x^2\td{\\delta}x^2\tk_x\tdk_x\tf_c(Hz)\tdf_c/f_c"
			      "\t{\\eta}r\t{\\eta}r2\td{\\eta}r2\td<x>\t<x>\tZmag\tRot\tBead_Z\tFix_Z\tZ_offset");


  n_size = (up_dwn == 0) ? ds->nx : (ds->nx+1)/2;

  dsma = create_and_attach_one_ds(op, n_size, n_size, 0);
  if (dsma == NULL) 	 return win_printf("cannot create ds !");
  set_ds_source(dsma,"Mean Z value for bead");
  if (up_dwn == 1)
  {
    dsmr = create_and_attach_one_ds(op, n_size, n_size, 0);
    if (dsmr == NULL) 	 return win_printf("cannot create ds !");
    set_ds_source(dsmr,"Mean Z value for bead return");
  }

  dsm2a = create_and_attach_one_ds(op, n_size, n_size, 0);
  if (dsm2a == NULL) 	 return win_printf("cannot create ds !");
  set_ds_source(dsm2a,"Mean Z value for fix bead");
  if (up_dwn == 1)
  {
    dsm2r = create_and_attach_one_ds(op, n_size, n_size, 0);
    if (dsm2r == NULL) 	 return win_printf("cannot create ds !");
    set_ds_source(dsm2r,"Mean Z value for fix bead return");
  }

  dsfca = create_and_attach_one_ds(op, n_size, n_size, 0);
  if (dsfca == NULL) 	 return win_printf("cannot create ds !");
  set_ds_source(dsfca,"Cutoff frequency");
  if (up_dwn == 1)
  {
    dsfcr = create_and_attach_one_ds(op, n_size, n_size, 0);
    if (dsfcr == NULL) 	 return win_printf("cannot create ds !");
    set_ds_source(dsfcr,"Cutoff frequency return");
  }

  opf = create_and_attach_one_plot(pr, n_size, n_size, 0);
  if (opf == NULL) 	 return win_printf("cannot create ds !");
  dsfa = opf->dat[0];
  set_plot_symb(dsfa,"\\pt5\\oc ");
  alloc_data_set_y_error(dsfa);
  set_ds_dot_line(dsfa);
  set_ds_source(dsfa,"Force versus rotation");
  set_plot_x_title(opf,"Rotation");
  set_plot_y_title(opf,"Force_{%s} (pN)",(x_or_y)?"y":"x");
  if (up_dwn == 1)
  {
    dsfr = create_and_attach_one_ds(opf, n_size, n_size, 0);
    if (dsfr == NULL) 	 return win_printf("cannot create ds !");
    set_plot_symb(dsfr,"\\pt5\\oc ");
    alloc_data_set_y_error(dsfr);
    set_ds_dot_line(dsfr);
    set_ds_source(dsfr,"Force versus rotation return");
  }

  opk = create_and_attach_one_plot(pr, n_size, n_size, 0);
  if (opk == NULL) 	 return win_printf("cannot create ds !");
  dska = opk->dat[0];
  set_plot_symb(dska,"\\pt5\\di ");
  alloc_data_set_y_error(dska);
  set_ds_dot_line(dska);
  set_ds_source(dska,"10^6 x Ky versus rotation");
  set_plot_x_title(opk,"Rotation");
  set_plot_y_title(opk,"10^6 x k_{%s}",(x_or_y)?"y":"x");
  if (up_dwn == 1)
  {
    dskr = create_and_attach_one_ds(opk, n_size, n_size, 0);
    if (dskr == NULL) 	 return win_printf("cannot create ds !");
    set_plot_symb(dskr,"\\pt5\\di ");
    alloc_data_set_y_error(dskr);
    set_ds_dot_line(dskr);
    set_ds_source(dskr,"10^6 x Ky versus rotation return");
  }
  dsrot2 = create_and_attach_one_ds(oprot, 2*ds->nx, 2*ds->nx, 0);
  if (dsrot2 == NULL) 	 return win_printf("cannot create ds !");
  set_plot_symb(dsrot2,"|");
  alloc_data_set_y_error(dsrot2);
  set_ds_dash_line(dsrot2);
  set_ds_source(dsrot2,"Rotation marker");



  for (i = 0, j = 0, opn->cur_dat = opn->n_dat-1; i < n_size; i++)
    {
      //j = (int)(0.5 + (ds->xd[i] - rots)/step);
      j = i;
      dsn = create_and_attach_one_ds(opn, nf, nf, 0);
      if (dsn == NULL) 	 return win_printf("cannot create ds !");
      grey = 224-(j*192)/nstep;
      set_ds_line_color(dsn, makecol(grey,grey,grey));
      set_ds_source(dsn,"Partial trajectory at Zmag = %g Rot %g fileindex %d basename %s",
		    zmag,ds->xd[i],findex,basename);
      set_ds_dot_line(dsn);
      for(k = 0, ki = (j * (dead + nf)) + dead; k < nx && ki < dsy->nx ; k++, ki++)
	{
	  dsn->xd[k] = dsy->xd[ki];
	  dsn->yd[k] = dsy->yd[ki];
	  dsma->yd[i] += dsz->yd[ki];
	  dsm2a->yd[i] += (bdfix_index >= 0) ? dsz2->yd[ki] : 0;
	}
      dsrot2->xd[2*j] = dsrot->xd[(j * (dead + nf)) + dead];
      dsrot2->yd[2*j] = dsrot->yd[(j * (dead + nf)) + dead];
      dsrot2->xd[2*j+1] = dsrot->xd[(j * (dead + nf)) + dead + nx - 1];
      dsrot2->yd[2*j+1] = dsrot->yd[(j * (dead + nf)) + dead + nx - 1];

      dsma->yd[i] /= nx;
      dsma->yd[i] *= dy  * 0.878;
      dsma->xd[i] = ds->xd[i];
      dsm2a->yd[i] /= nx;
      dsm2a->yd[i] *= dy * 0.878;
      dsm2a->xd[i] = ds->xd[i];
      dsfa->xd[i] = ds->xd[i];
      dsfca->xd[i] = ds->xd[i];
      dska->xd[i] = ds->xd[i];

      for (k = 0, mean = 0; k < dsn->nx; k++)
	mean += dsn->yd[k];
      mean /= dsn->nx;
      mean *= dy;
      dsn->nx = dsn->ny = nx;
      alternate_lorentian_fit_acq_spe_with_error(dsn->yd, nx, dy, fb, fm,
		w_flag, cam_fil, factor, frame, &fc, &a, &dfc, &da, &chi2, &dy0);
      su = a * M_PI_2;
      kx = (1.38e-11 * 298)/su;
      etar = kx*nx*dt/(12*M_PI*M_PI*fc);
      flow = (int)(2*fc);
      flow = (flow > nx/4) ? nx/4 : flow;
      flow = (flow < 2*fb) ? 2*fb : flow;
      opn->cur_dat = opn->n_dat-1;
      opd = do_fft_and_derivate_on_op_2(opn, flow, fm, w_flag+(cam_fil<<1), fc,
		frame, factor, &etar2, &detar2);
      free_one_plot(opd);
      detar2 = (etar2 != 0) ? detar2/etar2 : detar2;

      sprintf(zmags,"Zmag = %g Rot = %g\n",zmag, ds->xd[i]);
      /*
      if (fabs(ds->xd[i]) < 20) win_printf("analysing %s%s\n data set %d/%d with nx %d fb %d fm %d %swindowing \n"
		"%s camera correction %s factor %f\n%ssu  %g \\mu m^2 kx = %g N/m \n"
		"a = %g +/- %g %%\nfc %g (raw) +/- %g %% %g (Hz) \\eta r = %g\n"
		" \\eta_1 = %g +/- %g %% fb = %d \\chi^2 = %g n = %d\n"
		"mean %g error on mean %g \\mu m\n"
	,backslash_to_slash(opn->dir),backslash_to_slash(opn->filename),op->cur_dat,op->n_dat,nx,fb,fm,
	(w_flag)?" ":"no ",(cam_fil)? " ":"no ",(frame)?"frame int":"field int",
	factor,zmags,su,kx,a,(a!=0)?100*da/a:da,fc,(fc!=0)?100*dfc/fc:dfc,fc/(nx*dt),
	etar, etar2,100*detar2,flow,chi2,fm-fb-2,mean,dy0);
      */


      dump_to_current_xvplot_file( "%d\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g",
				   findex,su,(a!=0)?su*da/a:su,kx,(a!=0)?kx*da/a:kx,fc/(nx*dt),
				   (fc!=0)?100*dfc/fc:dfc,etar,etar2,etar2*detar2,dy0,mean,zmag,ds->xd[i],
				   dsma->yd[i],dsm2a->yd[i],z_offset);
      dsfca->yd[i] = fc/(nx*dt);
      dska->yd[i] = kx*1000000;
      dska->ye[i] = (a!=0)?dska->yd[i]*da/a:0;

      dsfa->yd[i] = kx * 1000000 * ((dsma->yd[i] - dsm2a->yd[i]) - z_offset);
      dsfa->ye[i] = (a!=0)?dsfa->yd[i]*da/a:0;

    }


  for (i = n_size, j = 0, opn->cur_dat = opn->n_dat-1; i < ds->nx && up_dwn == 1; i++, j++)
    {
      //j = (int)(0.5 + (ds->xd[i] - rots)/step);
      //win_printf("doing return %d",i);
      dsn = create_and_attach_one_ds(opn, nf, nf, 0);
      if (dsn == NULL) 	 return win_printf("cannot create ds !");
      grey = 224-(i*192)/nstep;
      set_ds_line_color(dsn, makecol(grey,grey,grey));
      set_ds_source(dsn,"Partial trajectory at Zmag = %g Rot %g fileindex %d basename %s",
		    zmag,ds->xd[i],findex,basename);
      set_ds_dot_line(dsn);
      //win_printf("bef trans");
      for(k = 0, ki = (i * (dead + nf)) + dead; k < nx && ki < dsy->nx ; k++, ki++)
	{
	  dsn->xd[k] = dsy->xd[ki];
	  dsn->yd[k] = dsy->yd[ki];
	  dsmr->yd[j] += dsz->yd[ki];
	  dsm2r->yd[j] += (bdfix_index >= 0) ? dsz2->yd[ki] : 0;
	}
      dsrot2->xd[2*i] = dsrot->xd[(i * (dead + nf)) + dead];
      dsrot2->yd[2*i] = dsrot->yd[(i * (dead + nf)) + dead];
      dsrot2->xd[2*i+1] = dsrot->xd[(i * (dead + nf)) + dead + nx - 1];
      dsrot2->yd[2*i+1] = dsrot->yd[(i * (dead + nf)) + dead + nx - 1];

      dsmr->yd[j] /= nx;
      dsmr->yd[j] *= dy  * 0.878;
      dsmr->xd[j] = ds->xd[i];
      dsm2r->yd[j] /= nx;
      dsm2r->yd[j] *= dy * 0.878;
      dsm2r->xd[j] = ds->xd[i];
      dsfr->xd[j] = ds->xd[i];
      dsfcr->xd[j] = ds->xd[i];
      dskr->xd[j] = ds->xd[i];
      //win_printf("bef mean");
      for (k = 0, mean = 0; k < dsn->nx; k++)
	mean += dsn->yd[k];
      mean /= dsn->nx;
      mean *= dy;
      dsn->nx = dsn->ny = nx;
      alternate_lorentian_fit_acq_spe_with_error(dsn->yd, nx, dy, fb, fm,
		w_flag, cam_fil, factor, frame, &fc, &a, &dfc, &da, &chi2, &dy0);
      su = a * M_PI_2;
      kx = (1.38e-11 * 298)/su;
      etar = kx*nx*dt/(12*M_PI*M_PI*fc);
      flow = (int)(2*fc);
      flow = (flow > nx/4) ? nx/4 : flow;
      flow = (flow < 2*fb) ? 2*fb : flow;
      opn->cur_dat = opn->n_dat-1;
      //win_printf("bef deriv");
      opd = do_fft_and_derivate_on_op_2(opn, flow, fm, w_flag+(cam_fil<<1), fc,
		frame, factor, &etar2, &detar2);
      free_one_plot(opd);
      detar2 = (etar2 != 0) ? detar2/etar2 : detar2;

      sprintf(zmags,"Zmag = %g Rot = %g\n",zmag, ds->xd[i]);


      dump_to_current_xvplot_file( "%d\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g",
				   findex,su,(a!=0)?su*da/a:su,kx,(a!=0)?kx*da/a:kx,fc/(nx*dt),
				   (fc!=0)?100*dfc/fc:dfc,etar,etar2,etar2*detar2,dy0,mean,zmag,
				   ds->xd[i],dsmr->yd[j],dsm2r->yd[j],z_offset);
      dsfcr->yd[j] = fc/(nx*dt);
      dskr->yd[j] = kx*1000000;
      dskr->ye[j] = (a!=0)?dskr->yd[j]*da/a:0;

      dsfr->yd[j] = kx * 1000000 * ((dsmr->yd[j] - dsm2r->yd[j]) - z_offset);
      dsfr->ye[j] = (a!=0)?dsfr->yd[j]*da/a:0;

    }


  //  free_one_plot(opn);
    if (bdfix_index >= 0)
      free_one_plot(op2);
  opn->need_to_refresh = 1;
  refresh_plot(pr, UNCHANGED);
  return 0;
}


int reload_scan_data_and_force(void)
{
  char *st = NULL, basename[256] = {0}, warning[1024] = {0}, file[1024] = {0}, buf[2048] = {0}, zmags[128] = {0}, path[512] = {0};
  int findex;
  int flow;
  pltreg *pr = NULL;
  O_p *op = NULL, *opn = NULL, *op2 = NULL, *opd = NULL, *opf = NULL;
  d_s *ds = NULL, *dsx = NULL, *dsy = NULL, *dsz = NULL, *dsn = NULL, *dsm = NULL;
  d_s *dsm2 = NULL, *dsz2 = NULL, *dsf = NULL, *dsfc = NULL, *dsf2 = NULL;
  int nf, nstep, i, j, k, ki;
  float rots, step, zmag, *n_z, tmp, tmpn, tmp2, tmp2n;
  float fc, a, su, kx, etar, etar2, detar2, dy, dt, da, dfc, chi2, dy0, mean;
  static int	nx = 1024, w_flag = 0, x_or_y = 0;
  static int fm = 1024, fb = 10, cam_fil = 1, frame = 0;
  static float factor = 1, z_offset;
  un_s *un = NULL;
  int   nx_2, *n_nf;
  int n_warn = 0, found;

 if(updating_menu_state != 0)	return D_O_K;


  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
    return win_printf("cannot find data");

  if (ds->source == NULL) return win_printf_OK("this is not a hat curve");
  if (strstr(ds->source,"Scan Z 2 beads") != NULL)
    {
      st = strstr(ds->source,"Z mag start");
      if (st == NULL)
	return win_printf_OK("this is not a good hat curve");
      if (sscanf(st,"Z mag start = %f for %d frames scan by steps of %f for %d steps\nrot = %f "
		 ,&zmag,&nf,&step,&nstep,&rots) != 5)
	return win_printf_OK("problem recovering parameters from scan curve");

    }
  else return win_printf_OK("This is not a scan Z data set");

  findex = retrieve_basename_and_index(op->filename, strlen(op->filename), basename, 256);

  sprintf(file,"traca%03d.gr",findex);
  strcpy(path,op->dir);
  put_backslash(path);

  opn = create_plot_from_gr_file(file, path);
  if (opn == NULL) win_printf_OK("Could not loaded %s\n from %s",
				 backslash_to_slash(file), backslash_to_slash(path));
  add_data_to_pltreg (pr, IS_ONE_PLOT, (void *)opn);
  for (i = 0; i < opn->n_dat; i++)
    {
      dsx = opn->dat[i];
      if (dsx->source == NULL) continue;
      st = strstr(dsx->source,"X coordinate");
      if (st != NULL) break;
    }
  if (st == NULL || dsx == NULL)
    return win_printf_OK("problem recovering X coordinate");

  for (i = 0; i < opn->n_dat; i++)
    {
      dsy = opn->dat[i];
      if (dsy->source == NULL) continue;
      st = strstr(dsy->source,"Y coordinate");
      if (st != NULL) break;
    }
  if (st == NULL || dsy == NULL)
    return win_printf_OK("problem recovering Y coordinate");

  for (i = 0; i < opn->n_dat; i++)
    {
      dsz = opn->dat[i];
      if (dsz->source == NULL) continue;
      st = strstr(dsz->source,"Z coordinate");
      if (st != NULL) break;
    }
  if (st == NULL || dsz == NULL)
    return win_printf_OK("problem recovering Z coordinate");


  sprintf(file,"tracb%03d.gr",findex);
  op2 = create_plot_from_gr_file(file, path);
  if (op2 == NULL) win_printf_OK("Could not loaded %s\n from %s",
				 backslash_to_slash(file), backslash_to_slash(op->dir));
  for (i = 0; i < op2->n_dat; i++)
    {
      dsz2 = op2->dat[i];
      if (dsz2->source == NULL) continue;
      st = strstr(dsz2->source,"Z coordinate");
      if (st != NULL) break;
    }
  if (st == NULL || dsz2 == NULL)
    return win_printf_OK("problem recovering Z coordinate");


  nx = nf; // nxo =
  warning[0] = 0;
  for (nx_2 = 2; nx_2 <= nx; nx_2 *= 2);
  nx_2 /= 2;
  if (nx_2 != nx)
    {
      sprintf(warning+strlen(warning),
	      "{\\color{Yellow}warning:Your data set size %d is not a power of 2!}\n"
	      "{\\color{Yellow}I am going to analyse only the %d first points}\n",nx,nx_2);
      n_warn++;
      nx = nx_2;
    }
  fm = nx/2;


  un = opn->yu[opn->c_yu];
  if (un->type != IS_METER || un->decade != IS_MICRO)
    {
      sprintf(warning+strlen(warning),"{\\color{Yellow}warning the y unit is not \\mu m !\n"
	      "numerical value will be wrong}\n");
      n_warn++;
    }
  dy = un->dx;
  un = opn->xu[opn->c_xu];
  if (un->type != IS_SECOND || un->decade != 0)
    {
      sprintf(warning+strlen(warning),"{\\color{Yellow}warning the x unit is not seconds !\n"
	      "numerical value will be wrong}\n");
	    n_warn++;
    }
  dt = un->dx;


  n_nf = (int*)calloc(ds->nx,sizeof(int));
  j = dsz->nx / nf;

  n_z = (float*)calloc(j,sizeof(int));
  if (n_nf == NULL || n_z == NULL)
    return win_printf_OK("cannot allocate memory!");

  if (j == ds->nx)
    {
      for (i = 0; i < ds->nx; i++)
	n_z[i] = 1;
      win_printf("No pb");
    }
  else
    {
      for (i = 0; i < j; i++)
	{
	  for (k = 0; k < nf; k++)
	    n_z[i] += dsz->yd[i*nf + k] - dsz2->yd[i*nf + k];
	  n_z[i] /= nf;
	  n_z[i] *= dy * 0.878;
	}
      for (i = 0, ki = 0; i < ds->nx/2; i++)
	{
	  if (i == (ds->nx/2) - 1 && ki >= j/2)
	    win_printf("something went wrong in data size recovering");
	  for (k = ki, found = 0, tmp = tmp2 = 0; found == 0 && k < j/2; k++)
	    {
	      tmp += n_z[k];
	      tmp2 += n_z[j-k-1];
	      tmpn = tmp /(1+k-ki);
	      tmp2n = tmp2 /(1+k-ki);
	      found = (fabs(tmpn - ds->xd[i]) < 0.00003
		       && fabs(tmp2n - ds->xd[ds->nx-i-1]) < 0.00003) ? 1 : 0;
	      //win_printf ("i = %d k %d tmpn = %g ds %g",i,k,tmpn,ds->xd[i]);
	    }
	  n_nf[i] = k - ki;
	  n_nf[i] *= nf;
	  n_nf[ds->nx-i-1] = n_nf[i];
	  if (found == 0)
	    win_printf("Pb recovering trajectory size at i %d k %d",i,k);
	  //else win_printf("For i = %d k %d ki %d trajectory size is %d",i,k, ki,n_nf[i]*nf);
	  ki = k;
	}

    }

  //return win_printf_OK("end of data recovering");


  i = win_scanf("which horizontal direction\ndo you want to use to\n"
		"measure forece %R X %r Y\n",&x_or_y);

  if (i == WIN_CANCEL) return D_O_K;
  if (x_or_y == 0) dsy = dsx;

  frame = get_config_int("camera", "mode", 0);
  cam_fil = get_config_int("camera", "correction", 1);
  factor = get_config_float("camera", "openning", 1);
  w_flag = get_config_int("spectrum", "windowing", 1);
  fb = get_config_int("spectrum", "lower_frequency", 10);


  sprintf(buf,"%s\n\n\\frac{f_c [log(1+x^2)]_{fb/fc}^{fm/fc}}"
	  "{2 [arctang(x)]_{fb/fc}^{fm/fc}}\n\nData sets %d fb %%6d fm %%6d nx %%6d\n"
	  "windowing  no%%R yes%%r\ncamera correction no%%R yes%%r\n"
	  "Camera integration mode: field ->%%R frame ->%%r\nopening factor %%6f\n"
	  "Offset to substract to molecule extension %%8f\n"
	  ,warning,ds->nx);
  i = win_scanf(buf,&fb,&fm,&nx,&w_flag,&cam_fil, &frame,&factor,&z_offset);
  if (i == WIN_CANCEL)
    {
      return 0;
    }


  set_config_int("camera", "mode", frame);
  set_config_int("camera", "correction", cam_fil);
  set_config_float("camera", "openning", factor);
  set_config_int("spectrum", "windowing", w_flag);
  set_config_int("spectrum", "lower_frequency", fb);



  if (do_create_xvplot_file(NULL) == NULL)
    {
      return 0;
    }
  log_cardinal++;
  dump_to_current_xvplot_file( "%%index\t{\\delta}x^2\td{\\delta}x^2\tk_x\tdk_x\tf_c(Hz)\tdf_c/f_c"
			      "\t{\\eta}r\t{\\eta}r2\td{\\eta}r2\td<x>\t<x>\tZmag\tRot\tBead_Z\tFix_Z\tZ_offset");



  dsm = create_and_attach_one_ds(op, ds->nx, ds->nx, 0);
  if (dsm == NULL) 	 return win_printf("cannot create ds !");
  set_ds_source(dsm,"Mean Z value for bead");
  //set_plot_symb(dsm,"\\pt5\\oc ");
  //set_ds_dot_line(dsm);

  dsm2 = create_and_attach_one_ds(op, ds->nx, ds->nx, 0);
  if (dsm2 == NULL) 	 return win_printf("cannot create ds !");
  set_ds_source(dsm2,"Mean Z value for fix bead");
  //set_plot_symb(dsm2,"\\pt5\\oc ");
  //set_ds_dot_line(dsm2);



  dsf = create_and_attach_one_ds(op, ds->nx, ds->nx, 0);
  if (dsf == NULL) 	 return win_printf("cannot create ds !");
  set_plot_symb(dsf,"\\pt5\\oc ");
  alloc_data_set_x_error(dsf);
  set_ds_dot_line(dsf);
  set_ds_source(dsf,"Zmag vs force");


  opf = create_and_attach_one_plot(pr,ds->nx,ds->nx, 0);
  if (opf == NULL)	 return win_printf_OK("Cannot allocate plot");
  dsf2 = opf->dat[0];
  if (dsf2 == NULL)	 return D_O_K;
  set_plot_symb(dsf2,"\\pt5\\oc ");
  alloc_data_set_y_error(dsf2);
  set_ds_dot_line(dsf2);
  set_ds_source(dsf2,"Force extension");

  dsfc = create_and_attach_one_ds(opf, ds->nx, ds->nx, 0);
  if (dsfc == NULL) 	 return win_printf("cannot create ds !");
  set_ds_source(dsfc,"Cutoff frequency");
  set_plot_symb(dsfc,"\\pt5\\di ");
  set_ds_dot_line(dsfc);

  set_plot_y_title(opf,"Force & frequency (Hz)");
  set_plot_x_title(opf,"Extension ");
  set_plot_title(opf, "Force curve");

  for (i = 0, opn->cur_dat = opn->n_dat-1, j = 0; i < ds->nx; i++)
    {

      nx = n_nf[i];
      for (nx_2 = 2; nx_2 <= nx; nx_2 *= 2);
      nx_2 /= 2;
      if (nx_2 != nx)
	nx = nx_2;
      fm = nx/2;


      //j = (int)(0.5 + (ds->xd[i] - rots)/step);
      dsn = create_and_attach_one_ds(opn, n_nf[i], n_nf[i], 0);
      if (dsn == NULL) 	 return win_printf("cannot create ds !");
      grey = 224-(i*192)/(2*nstep);
      set_ds_line_color(dsn, makecol(grey,grey,grey));
      set_ds_source(dsn,"Partial trajectory at Zmag = %g Rot %g fileindex %d basename %s",
		    ds->yd[i],rots,findex,basename);
      set_ds_dot_line(dsn);
      for (j = 0, ki = 0; j < i; j++)
	ki += n_nf[j];
      for(k = 0; k < nx && ki < dsy->nx ; k++, ki++)
	{
	  dsn->xd[k] = dsy->xd[ki];
	  dsn->yd[k] = dsy->yd[ki];
	  dsm->xd[i] += dsz->yd[ki];
	  dsm2->xd[i] += dsz2->yd[ki];
	}
      dsm->xd[i] /= nx;
      dsm->xd[i] *= dy  * 0.878;
      dsm->yd[i] = ds->yd[i];
      dsm2->xd[i] /= nx;
      dsm2->xd[i] *= dy * 0.878;
      dsm2->yd[i] = ds->yd[i];
      dsf->yd[i] = ds->yd[i];
      dsf2->xd[i] = ds->xd[i] - z_offset;
      dsfc->xd[i] = ds->xd[i] - z_offset;

      for (k = 0, mean = 0; k < dsn->nx; k++)
	mean += dsn->yd[k];
      mean /= dsn->nx;
      mean *= dy;
      dsn->nx = dsn->ny = nx;
      alternate_lorentian_fit_acq_spe_with_error(dsn->yd, nx, dy, fb, fm,
		w_flag, cam_fil, factor, frame, &fc, &a, &dfc, &da, &chi2, &dy0);
      su = a * M_PI_2;
      kx = (1.38e-11 * 298)/su;
      etar = kx*nx*dt/(12*M_PI*M_PI*fc);
      flow = (int)(2*fc);
      flow = (flow > nx/4) ? nx/4 : flow;
      flow = (flow < 2*fb) ? 2*fb : flow;
      opn->cur_dat = opn->n_dat-1;
      opd = do_fft_and_derivate_on_op_2(opn, flow, fm, w_flag+(cam_fil<<1), fc,
		frame, factor, &etar2, &detar2);
      free_one_plot(opd);
      detar2 = (etar2 != 0) ? detar2/etar2 : detar2;

      sprintf(zmags,"Zmag = %g Rot = %g\n",ds->yd[i],rots);
      /*
      if (fabs(ds->xd[i]) < 20) win_printf("analysing %s%s\n data set %d/%d with nx %d fb %d fm %d %swindowing \n"
		"%s camera correction %s factor %f\n%ssu  %g \\mu m^2 kx = %g N/m \n"
		"a = %g +/- %g %%\nfc %g (raw) +/- %g %% %g (Hz) \\eta r = %g\n"
		" \\eta_1 = %g +/- %g %% fb = %d \\chi^2 = %g n = %d\n"
		"mean %g error on mean %g \\mu m\n"
	,backslash_to_slash(opn->dir),backslash_to_slash(opn->filename),op->cur_dat,op->n_dat,nx,fb,fm,
	(w_flag)?" ":"no ",(cam_fil)? " ":"no ",(frame)?"frame int":"field int",
	factor,zmags,su,kx,a,(a!=0)?100*da/a:da,fc,(fc!=0)?100*dfc/fc:dfc,fc/(nx*dt),
	etar, etar2,100*detar2,flow,chi2,fm-fb-2,mean,dy0);
      */


      dump_to_current_xvplot_file( "%d\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g",
				   findex,su,(a!=0)?su*da/a:su,kx,(a!=0)?kx*da/a:kx,fc/(nx*dt),
				   (fc!=0)?100*dfc/fc:dfc,etar,etar2,etar2*detar2,dy0,mean,ds->yd[i],rots,
				   dsm->xd[i],dsm2->xd[i],z_offset);
      dsfc->yd[i] = fc/(nx*dt);

      dsf2->yd[i] = dsf->xd[i] = kx * 1000000 * ((dsm->xd[i] - dsm2->xd[i]) - z_offset);
      dsf2->ye[i] = dsf->xe[i] = (a!=0)?dsf->xd[i]*da/a:0;
    }
  //free_one_plot(opn);
  free_one_plot(op2);
  opn->need_to_refresh = 1;
  refresh_plot(pr, UNCHANGED);
  return 0;
}


int reload_scan_data_and_force_2(void)
{
  char *st = NULL, basename[256], warning[1024], file[1024], buf[2048], zmags[128], path[512];
  int findex;
  int flow;
  pltreg *pr = NULL;
  O_p *op = NULL, *opn = NULL, *op2 = NULL, *opd = NULL, *opf = NULL, *opzmag = NULL;
  d_s *ds = NULL, *dsx = NULL, *dsy = NULL, *dsz = NULL, *dsn = NULL, *dsm = NULL, *dsk = NULL;
  d_s *dsm2 = NULL, *dsz2 = NULL, *dsf = NULL, *dsfc = NULL, *dsf2 = NULL, *dszmag = NULL, *dszmag2 = NULL;
  int nf, i, j, k, ki;
  float rots;
  float fc, a, su, kx, etar, etar2, detar2, dy, dt, da, dfc, chi2, dy0, mean, zmag_m;
  static int	nx = 1024, w_flag = 0, x_or_y = 0, zmag_n;
  static int fm = 1024, fb = 10, cam_fil = 1, frame = 0;
  static float factor = 1, z_offset;
  un_s *un = NULL;
  int  nx_2, *n_nf = NULL;
  int n_warn = 0, bd_index = 0, bdfix_index = 0;
  char buf1[256] = {0};

 if(updating_menu_state != 0)	return D_O_K;


  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
    return win_printf("cannot find data");

  /*
  if (ds->source == NULL) return win_printf_OK("this is not a hat curve");
  if (strstr(ds->source,"Scan Z 2 beads") != NULL)
    {
      st = strstr(ds->source,"Z mag start");
      if (st == NULL)
	return win_printf_OK("this is not a good hat curve");
      if (sscanf(st,"Z mag start = %f for %d frames scan by steps of %f for %d steps\nrot = %f "
		 ,&zmag,&nf,&step,&nstep,&rots) != 5)
	return win_printf_OK("problem recovering parameters from scan curve");

    }
  else return win_printf_OK("This is not a scan Z data set");
  */
  if (op->title == NULL) return win_printf_OK("this is not a scanz curve");
  st = strstr(op->title,"rot ");
  if (st != NULL)    sscanf(st,"rot %g",&rots);

  findex = retrieve_basename_and_index(op->filename, strlen(op->filename), basename, 256);


  //return win_printf_OK("file index = %d basename %s",findex,basename);
  bd_index = findex/1000;
  findex = findex%1000;
  strcpy(path,op->dir);
  put_backslash(path);
  sprintf(file,"X(t)Y(t)Z(t)bd%dscan%d.gr",bd_index,findex);
  opn = create_plot_from_gr_file(file, path);
  if (opn == NULL) win_printf_OK("Could not loaded %s\n from %s",
				 backslash_to_slash(file), backslash_to_slash(op->dir));
  add_data_to_pltreg (pr, IS_ONE_PLOT, (void *)opn);
  for (i = 0; i < opn->n_dat; i++)
    {
      dsx = opn->dat[i];
      if (dsx->source == NULL) continue;
      st = strstr(dsx->source,"X coordinate");
      if (st != NULL) break;
    }
  if (st == NULL || dsx == NULL)
    return win_printf_OK("problem recovering X coordinate");

  for (i = 0; i < opn->n_dat; i++)
    {
      dsy = opn->dat[i];
      if (dsy->source == NULL) continue;
      st = strstr(dsy->source,"Y coordinate");
      if (st != NULL) break;
    }
  if (st == NULL || dsy == NULL)
    return win_printf_OK("problem recovering Y coordinate");

  for (i = 0; i < opn->n_dat; i++)
    {
      dsz = opn->dat[i];
      if (dsz->source == NULL) continue;
      st = strstr(dsz->source,"Z coordinate");
      if (st != NULL) break;
    }
  if (st == NULL || dsz == NULL)
    return win_printf_OK("problem recovering Z coordinate");



  i = win_scanf("Indicate the index of the fixed bead %d",&bdfix_index);
  if (i == WIN_CANCEL) return D_O_K;

  sprintf(file,"X(t)Y(t)Z(t)bd%dscan%d.gr",bdfix_index,findex);

  op2 = create_plot_from_gr_file(file, path);
  if (op2 == NULL) win_printf_OK("Could not loaded %s\n from %s",
				 backslash_to_slash(file), backslash_to_slash(op->dir));
  add_data_to_pltreg (pr, IS_ONE_PLOT, (void *)op2);
  for (i = 0; i < op2->n_dat; i++)
    {
      dsz2 = op2->dat[i];
      if (dsz2->source == NULL) continue;
      st = strstr(dsz2->source,"Z coordinate");
      if (st != NULL) break;
    }
  if (st == NULL || dsz2 == NULL)
    return win_printf_OK("problem recovering Z coordinate");


  sprintf(file,"bddzr%03d.gr",findex);
  opzmag = create_plot_from_gr_file(file, path);
  if (opzmag == NULL) win_printf_OK("Could not loaded %s\n from %s",
				 backslash_to_slash(file), backslash_to_slash(op->dir));
  add_data_to_pltreg (pr, IS_ONE_PLOT, (void *)opzmag);
  dszmag = opzmag->dat[0];




  un = opn->yu[opn->c_yu];
  if (un->type != IS_METER || un->decade != IS_MICRO)
    {
      sprintf(warning+strlen(warning),"{\\color{Yellow}warning the y unit is not \\mu m !\n"
	      "numerical value will be wrong}\n");
      n_warn++;
    }
  dy = un->dx;
  un = opn->xu[opn->c_xu];
  if (un->type != IS_SECOND || un->decade != 0)
    {
      sprintf(warning+strlen(warning),"{\\color{Yellow}warning the x unit is not seconds !\n"
	      "numerical value will be wrong}\n");
	    n_warn++;
    }
  dt = un->dx;

  dszmag2 = create_and_attach_one_ds(opzmag, 2*ds->nx, 2*(ds->nx+1), 0);
  if (dszmag2 == NULL) 	 return win_printf("cannot create ds !");
  set_ds_source(dszmag2,"Zmag recover");
  set_plot_symb(dszmag2,"|");
  set_ds_dash_line(dszmag2);

  nf = dszmag->nx;
  n_nf = (int*)calloc(2*(ds->nx),sizeof(int));
  for (i = 0, j = 1; i < ds->nx &&  j < dszmag->nx ; i++)
    {
      for ( ; j < dszmag->nx && fabs(dszmag->yd[j] - dszmag->yd[j-1]) > 0.005 ; j++);
      n_nf[2*i] = j;
      dszmag2->xd[2*i] = dszmag->xd[j];
      dszmag2->yd[2*i] = dszmag->yd[j];
      j+=128;
      for ( ; j < dszmag->nx && fabs(dszmag->yd[j] - dszmag->yd[j-1]) < 0.005 ; j++);
      if (j >= dszmag->nx) j = dszmag->nx-1;
      else j--;
      n_nf[2*i+1] = j;
      nf = (n_nf[2*i+1] - n_nf[2*i] < nf) ? (n_nf[2*i+1] - n_nf[2*i]) : nf;
      dszmag2->xd[2*i+1] = dszmag->xd[j];
      dszmag2->yd[2*i+1] = dszmag->yd[j];
      if (j < dszmag->nx) j++;
    }

  //return win_printf_OK("end of data recovering");




  nx = nf; // nxo =
  warning[0] = 0;
  for (nx_2 = 2; nx_2 <= nx; nx_2 *= 2);
  nx_2 /= 2;
  if (nx_2 != nx)
    {
      sprintf(warning+strlen(warning),
	      "{\\color{Yellow}warning:Your data set size %d is not a power of 2!}\n"
	      "{\\color{Yellow}I am going to analyse only the %d first points}\n",nx,nx_2);
      n_warn++;
      nx = nx_2;
    }
  fm = nx/2;




  i = win_scanf("which horizontal direction\ndo you want to use to\n"
		"measure forece %R X %r Y\n",&x_or_y);

  if (i == WIN_CANCEL) return D_O_K;
  if (x_or_y == 0) dsy = dsx;

  frame = get_config_int("camera", "mode", 0);
  cam_fil = get_config_int("camera", "correction", 1);
  factor = get_config_float("camera", "openning", 1);
  w_flag = get_config_int("spectrum", "windowing", 1);
  fb = get_config_int("spectrum", "lower_frequency", 10);


  sprintf(buf,"%s\n\n\\frac{f_c [log(1+x^2)]_{fb/fc}^{fm/fc}}"
	  "{2 [arctang(x)]_{fb/fc}^{fm/fc}}\n\nData sets %d fb %%6d fm %%6d nx %%6d\n"
	  "windowing  no%%R yes%%r\ncamera correction no%%R yes%%r\n"
	  "Camera integration mode: field ->%%R frame ->%%r\nopening factor %%6f\n"
	  "Offset to substract to molecule extension %%8f\n"
	  ,warning,ds->nx);
  i = win_scanf(buf,&fb,&fm,&nx,&w_flag,&cam_fil, &frame,&factor,&z_offset);
  if (i == WIN_CANCEL)
    {
      return 0;
    }


  set_config_int("camera", "mode", frame);
  set_config_int("camera", "correction", cam_fil);
  set_config_float("camera", "openning", factor);
  set_config_int("spectrum", "windowing", w_flag);
  set_config_int("spectrum", "lower_frequency", fb);



  if (do_create_xvplot_file(NULL) == NULL)
    {
      return 0;
    }
  log_cardinal++;
  dump_to_current_xvplot_file( "%%index\t{\\delta}x^2\td{\\delta}x^2\tk_x\tdk_x\tf_c(Hz)\tdf_c/f_c"
			      "\t{\\eta}r\t{\\eta}r2\td{\\eta}r2\td<x>\t<x>\tZmag\tRot\tBead_Z\tFix_Z\tZ_offset");



  dsm = create_and_attach_one_ds(op, ds->nx, ds->nx, 0);
  if (dsm == NULL) 	 return win_printf("cannot create ds !");
  set_ds_source(dsm,"Mean Z value for bead");
  //set_plot_symb(dsm,"\\pt5\\oc ");
  //set_ds_dot_line(dsm);

  dsm2 = create_and_attach_one_ds(op, ds->nx, ds->nx, 0);
  if (dsm2 == NULL) 	 return win_printf("cannot create ds !");
  set_ds_source(dsm2,"Mean Z value for fix bead");
  //set_plot_symb(dsm2,"\\pt5\\oc ");
  //set_ds_dot_line(dsm2);



  dsf = create_and_attach_one_ds(op, ds->nx, ds->nx, 0);
  if (dsf == NULL) 	 return win_printf("cannot create ds !");
  set_plot_symb(dsf,"\\pt5\\oc ");
  alloc_data_set_x_error(dsf);
  set_ds_dot_line(dsf);
  set_ds_source(dsf,"Force_{%s} vs Zmag",(x_or_y)?"y":"x");

  dsk = create_and_attach_one_ds(op, ds->nx, ds->nx, 0);
  if (dsk == NULL) 	 return win_printf("cannot create ds !");
  set_plot_symb(dsk,"\\pt5\\di ");
  alloc_data_set_x_error(dsk);
  set_ds_dot_line(dsk);
  set_ds_source(dsk,"10^6 x k_{%s} vs Zmag",(x_or_y)?"y":"x");


  opf = create_and_attach_one_plot(pr,ds->nx,ds->nx, 0);
  if (opf == NULL)	 return win_printf_OK("Cannot allocate plot");
  dsf2 = opf->dat[0];
  if (dsf2 == NULL)	 return D_O_K;
  set_plot_symb(dsf2,"\\pt5\\oc ");
  alloc_data_set_y_error(dsf2);
  set_ds_dot_line(dsf2);
  set_ds_source(dsf2,"Force extension");





  dsfc = create_and_attach_one_ds(opf, ds->nx, ds->nx, 0);
  if (dsfc == NULL) 	 return win_printf("cannot create ds !");
  set_ds_source(dsfc,"Cutoff frequency");
  set_plot_symb(dsfc,"\\pt5\\di ");
  set_ds_dot_line(dsfc);

  set_plot_y_title(opf,"Force & frequency (Hz)");
  set_plot_x_title(opf,"Extension ");
  set_plot_title(opf, "Force curve");

  for (i = 0, opn->cur_dat = opn->n_dat-1, j = 0; i < ds->nx; i++)
    {
      nx = n_nf[2*i+1] - n_nf[2*i];
      for (nx_2 = 2; nx_2 <= nx; nx_2 *= 2);
      nx_2 /= 2;
      if (nx_2 != nx)
	nx = nx_2;
      //if (nx > 32768) nx = 32768;
      fm = nx/2;
      sprintf(buf1,"Treating step %d",i);
      set_window_title(buf1);
      dsn = create_and_attach_one_ds(opn, nx, nx, 0);
      if (dsn == NULL) 	 return win_printf("cannot create ds !");
      grey = 224-(i*192)/(ds->nx);
      set_ds_line_color(dsn, makecol(grey,grey,grey));
      set_ds_dot_line(dsn);
      ki = (n_nf[2*i+1] + n_nf[2*i])/2;
      ki -= nx/2;
      zmag_n = 0;
      zmag_m = 0;

      //win_printf("i %d nx %d zmag %g ki %d ke %d max %d",i,nx,ds->yd[i],ki,ki+nx,dszmag->nx);
      for(k = 0; k < nx && ki < dsy->nx && ki < dsz->nx && ki < dsz2->nx; k++, ki++)
	{
	  dsn->xd[k] = dsy->xd[ki];
	  dsn->yd[k] = dsy->yd[ki];
	  dsm->xd[i] += dsz->yd[ki];
	  dsm2->xd[i] += dsz2->yd[ki];
	  zmag_m += dszmag->yd[ki];
	  zmag_n++;
	}
      if (ki >= dsy->nx || ki >= dsz->nx || ki >= dsz2->nx)
	win_printf("You have reach plot limits");
      //win_printf("i %d after garbing data",i);
      if (zmag_n) zmag_m /= zmag_n;
      dsm->xd[i] /= nx;
      dsm->xd[i] *= dy  * 0.878;
      dsm->yd[i] = zmag_m; //ds->yd[i];
      dsm2->xd[i] /= nx;
      dsm2->xd[i] *= dy * 0.878;
      dsm2->yd[i] = zmag_m; //ds->yd[i];
      dsk->yd[i] = dsf->yd[i] = zmag_m; //ds->yd[i];
      //dsf2->xd[i] = ds->xd[i] - z_offset;
      //dsfc->xd[i] = ds->xd[i] - z_offset;




      set_ds_source(dsn,"Partial trajectory at Zmag = %g Rot %g fileindex %d basename %s",
		    zmag_m,rots,findex,basename);


      for (k = 0, mean = 0; k < dsn->nx; k++)
	mean += dsn->yd[k];
      mean /= dsn->nx;
      mean *= dy;
      dsn->nx = dsn->ny = nx;
      //win_printf("i %d nx %d zmag %g ki %d ke %d max %d\nzm %g zm2 %g",
      //i,nx,ds->yd[i],ki,ki+nx,dszmag->nx,dsm->xd[i],dsm2->xd[i]);
      //win_printf("before alternate");
      alternate_lorentian_fit_acq_spe_with_error(dsn->yd, nx, dy, fb, fm,
		w_flag, cam_fil, factor, frame, &fc, &a, &dfc, &da, &chi2, &dy0);
      //win_printf("after alternate");
      su = a * M_PI_2;
      kx = (1.38e-11 * 298)/su;
      etar = kx*nx*dt/(12*M_PI*M_PI*fc);
      flow = (int)(2*fc);
      flow = (flow > nx/4) ? nx/4 : flow;
      flow = (flow < 2*fb) ? 2*fb : flow;
      opn->cur_dat = opn->n_dat-1;
      //win_printf("before derivate");

      opd = do_fft_and_derivate_on_op_2(opn, flow, fm, w_flag+(cam_fil<<1), fc,
		frame, factor, &etar2, &detar2);
      //win_printf("after derivate");

      if (opd != NULL) free_one_plot(opd);
      //win_printf("after free one plot");
      detar2 = (etar2 != 0) ? detar2/etar2 : detar2;

      sprintf(zmags,"Zmag = %g Rot = %g\n",zmag_m,rots);

      dump_to_current_xvplot_file( "%d\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g",
				   findex,su,(a!=0)?su*da/a:su,kx,(a!=0)?kx*da/a:kx,fc/(nx*dt),
				   (fc!=0)?100*dfc/fc:dfc,etar,etar2,etar2*detar2,dy0,mean,zmag_m,rots,
				   dsm->xd[i],dsm2->xd[i],z_offset);
      //win_printf("after dump");
      dsfc->yd[i] = fc/(nx*dt);

      //win_printf("after dump 2");
      dsf2->yd[i] = dsf->xd[i] = kx * 1000000 * ((dsm->xd[i] - dsm2->xd[i]) - z_offset);
      dsf2->ye[i] = dsf->xe[i] = (a!=0)?dsf->xd[i]*da/a:0;
      dsk->xd[i] = kx * 1000000;
      //win_printf("after dump 3");
      dsk->xe[i] = (a!=0)?dsk->xd[i]*da/a:0;
      dsf2->xd[i] = (dsm->xd[i] - dsm2->xd[i]) - z_offset;
      dsfc->xd[i] = (dsm->xd[i] - dsm2->xd[i]) - z_offset;
      //win_printf("i %d nx %d zmag %g kx %g",i,nx,ds->yd[i],kx);
    }
  //free_one_plot(opn);
  //free_one_plot(op2);
  opn->need_to_refresh = 1;
  refresh_plot(pr, UNCHANGED);
  return 0;
}



int reload_hat_data(void)
{
  char *st = NULL, basename[256] = {0}, file[1024] = {0};
  int findex;
  static int x_or_y = 0;
  pltreg *pr = NULL;
  O_p *op = NULL, *opn = NULL, *op2 = NULL;
  d_s *ds = NULL, *dsx = NULL, *dsy = NULL, *dsz = NULL, *dsn = NULL, *dsm = NULL, *dsm2 = NULL, *dsz2 = NULL;
  int nf, nstep, i, j, k, ki;
  float rots, step, zmag;

 if(updating_menu_state != 0)	return D_O_K;


  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
    return win_printf("cannot find data");

  if (ds->source == NULL) return win_printf_OK("this is not a hat curve");
  if (strstr(ds->source,"Chapeau 2 beads") != NULL)
    {
      st = strstr(ds->source,"Z mag start");
      if (st == NULL)
	return win_printf_OK("this is not a good hat curve");
      if (sscanf(st,"Z mag start = %f for %d frames scan by steps of %f for %d"
		 ,&rots,&nf,&step,&nstep) != 4)
	return win_printf_OK("problem recovering parameters from hat curve");

    }
  st = strstr(op->title,", rot");
  if (st == NULL)
    return win_printf_OK("this is not a good hat curve title");
  if (sscanf(st,", rot %f",&zmag) != 1)
    return win_printf_OK("problem recovering zmag from hat curve");
  findex = retrieve_basename_and_index(op->filename, strlen(op->filename), basename, 256);

  sprintf(file,"traca%03d.gr",findex);
  opn = create_plot_from_gr_file(file, op->dir);
  if (opn == NULL) win_printf_OK("Could not loaded %s\n from %s",
				 backslash_to_slash(file), backslash_to_slash(op->dir));
  add_data_to_pltreg (pr, IS_ONE_PLOT, (void *)opn);
  for (i = 0; i < opn->n_dat; i++)
    {
      dsx = opn->dat[i];
      if (dsx->source == NULL) continue;
      st = strstr(dsx->source,"X coordinate");
      if (st != NULL) break;
    }
  if (st == NULL || dsx == NULL)
    return win_printf_OK("problem recovering X coordinate");
  for (i = 0; i < opn->n_dat; i++)
    {
      dsy = opn->dat[i];
      if (dsy->source == NULL) continue;
      st = strstr(dsy->source,"Y coordinate");
      if (st != NULL) break;
    }
  if (st == NULL || dsy == NULL)
    return win_printf_OK("problem recovering Y coordinate");

  for (i = 0; i < opn->n_dat; i++)
    {
      dsz = opn->dat[i];
      if (dsz->source == NULL) continue;
      st = strstr(dsz->source,"Z coordinate");
      if (st != NULL) break;
    }
  if (st == NULL || dsz == NULL)
    return win_printf_OK("problem recovering Z coordinate");


  sprintf(file,"tracb%03d.gr",findex);
  op2 = create_plot_from_gr_file(file, op->dir);
  if (op2 == NULL) win_printf_OK("Could not loaded %s\n from %s",
				 backslash_to_slash(file), backslash_to_slash(op->dir));
  for (i = 0; i < op2->n_dat; i++)
    {
      dsz2 = op2->dat[i];
      if (dsz2->source == NULL) continue;
      st = strstr(dsz2->source,"Z coordinate");
      if (st != NULL) break;
    }
  if (st == NULL || dsz2 == NULL)
    return win_printf_OK("problem recovering Z coordinate");

  i = win_scanf("which horizontal direction\ndo you want to use to\n"
		"measure forece %R X %r Y\n",&x_or_y);

  if (i == WIN_CANCEL) return D_O_K;
  if (x_or_y == 0) dsy = dsx;


  dsm = create_and_attach_one_ds(opn, ds->nx, ds->nx, 0);
  if (dsm == NULL) 	 return win_printf("cannot create ds !");
  alloc_data_set_x_error(dsm);
  set_plot_symb(dsm,"\\pt5\\oc ");
  set_ds_dot_line(dsm);

  dsm2 = create_and_attach_one_ds(opn, ds->nx, ds->nx, 0);
  if (dsm2 == NULL) 	 return win_printf("cannot create ds !");
  alloc_data_set_x_error(dsm2);
  set_plot_symb(dsm2,"\\pt5\\oc ");
  set_ds_dot_line(dsm2);

  for (i = 0; i < ds->nx; i++)
    {
      j = (int)(0.5 + (ds->xd[i] - rots)/step);
      dsn = create_and_attach_one_ds(opn, nf, nf, 0);
      if (dsn == NULL) 	 return win_printf("cannot create ds !");
      grey = 224-(j*192)/nstep;
      set_ds_line_color(dsn, makecol(grey,grey,grey));
      set_ds_source(dsn,"Partial trajectory at Zmag = %g Rot %g fileindex %d basename %s",
		    zmag,ds->xd[i],findex,basename);
      set_ds_dot_line(dsn);
      for(k = 0, ki = j * nf; k < nf && ki < dsy->nx ; k++, ki++)
	{
	  dsn->xd[k] = dsy->xd[ki];
	  dsn->yd[k] = dsy->yd[ki];
	  dsm->yd[i] += dsz->yd[ki];
	  dsm->xd[i] += dsz->xd[ki];
	  dsm2->yd[i] += dsz2->yd[ki];
	  dsm2->xd[i] += dsz2->xd[ki];
	}
      dsm->yd[i] /= nf;
      dsm->xd[i] /= nf;
      dsm2->yd[i] /= nf;
      dsm2->xd[i] /= nf;
      dsm2->xe[i] = dsm->xe[i] = (dsz->xd[j*nf] - dsz->xd[((j+1)*nf)-1])/2;
    }
  free_one_plot(op2);
  opn->need_to_refresh = 1;
  refresh_plot(pr, UNCHANGED);
  return 0;
}


int 	fit_affine(void)
{
  int i, j;
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *dsi1 = NULL, *dsi2 = NULL, *dss = NULL;
  int  nx1, nx2, nxn;
  double a,b,s1=0,s2=0,sp=0,sd1=0;



  if(updating_menu_state != 0)
    {
      if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      if (op == NULL && op->n_dat < 2)
	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("These routines perform find affine product between\n"
			 "2 the datasets of a plot\n");
    }
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi1) != 3)
      return win_printf_OK("cannot find data!");
    if (op->n_dat < 2)
      return (win_printf_OK("No enought data set!\n2 datasets are required!"));

    i = op->cur_dat;
    i = (i+1 >= op->n_dat) ? 0 : i + 1;    dsi2 = op->dat[i];
    if (dsi1 == NULL || dsi2 == NULL)
		return (win_printf_OK("can't find data set"));
    nx1 = dsi1->nx;
    nx2 = dsi2->nx;
    nxn = (nx1 > nx2) ? nx2 : nx1;

    if (nx1 != nx2)
      i =  win_printf("data sets have different size %d  and %d!\n I propose to add them over %d",nx1,nx2,nxn);
    if (i == WIN_CANCEL)	return 1;
    for (i=0 ,j = 0; i< nxn ; i++)
      j += (dsi1->xd[i] == dsi2->xd[i]) ? 0 : 1;
    if (j)	return win_printf("the X axis differ, cant add them !");
    dss = create_and_attach_one_ds(op, nxn, nxn, 0);
    if (dss == NULL)	return (win_printf("can't create data set"));

    for (i=0 ; i< nxn ; i++)
      {
	s1 += dsi1->yd[i];
	s2 += dsi2->yd[i];
	sp += (dsi1->yd[i])*(dsi2->yd[i]);
	sd1 += (dsi1->yd[i])*(dsi1->yd[i]);
      }
    b = (nxn*sd1-(s1*s1));
    a = (b != 0) ? (nxn*sp-(s1*s2))/b : (nxn*sp-(s1*s2));
    b = (s2-(a*s1))/nxn;
    for (i=0 ; i< nxn ; i++)
      {
	dss->xd[i] = dsi1->xd[i];
	dss->yd[i] = dsi2->yd[i] - a*( dsi1->yd[i]);
      }
    win_printf("a= %g b= %g",a,b);
    dss->ny = nxn;
    dss->nx = nxn;
    inherit_from_ds_to_ds(dss, dsi1);
    dss->treatement = my_sprintf(dss->treatement,"fit affine y2 = a y1 +b with a = %g b = %g\n",a,b);

    return refresh_plot(pr, UNCHANGED);
}



int 	fit_2_bds_rotation(void)
{
  int i;
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *dsi1 = NULL, *dsi2 = NULL, *dss1 = NULL, *dss2 = NULL, *dsd = NULL, *dsb = NULL;
  int  nx1, nx2, nxn, start;
  static float a;
  double b = 0;
  double x1, y1, x2, y2, tmp, x2m = 0;
  //double axx1, axx2, axy1, axy2, ayx1, ayx2, ayy1, ayy2;


  if(updating_menu_state != 0)
    {
      if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      if (op->n_dat < 2)
	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("These routines perform find affine product between\n"
			   "2 the datasets of a plot\n");
    }

    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi1) != 3)
      return win_printf_OK("cannot find data!");
    if (op->n_dat < 2)
      return (win_printf_OK("No enought data set!\n2 datasets are required!"));

    i = op->cur_dat;
    i = (i+1 >= op->n_dat) ? 0 : i + 1;    dsi2 = op->dat[i];
    if (dsi1 == NULL || dsi2 == NULL)
		return (win_printf_OK("can't find data set"));
    nx1 = dsi1->nx;
    nx2 = dsi2->nx;
    nxn = (nx1 > nx2) ? nx2 : nx1;

    if (nx1 != nx2)
      i =  win_printf("data sets have different size %d  and %d!\n I propose to add them over %d",nx1,nx2,nxn);
    if (i == WIN_CANCEL)	return 1;


    for (i=0, x1 = y1 = x2 = y2 = 0; i< nxn ; i++)
      {
	x1 += dsi1->xd[i];
	y1 += dsi1->yd[i];
	x2 += dsi2->xd[i];
	y2 += dsi2->yd[i];
      }
    x1 /= nxn; y1 /= nxn; x2 /= nxn; y2 /= nxn;
    dss1 = create_and_attach_one_ds(op, nxn, nxn, 0);
    if (dss1 == NULL)	return (win_printf("can't create data set"));
    dss2 = create_and_attach_one_ds(op, nxn, nxn, 0);
    if (dss2 == NULL)	return (win_printf("can't create data set"));

    dsd = create_and_attach_one_ds(op, 2*nxn +1, 2*nxn+1, 0);
    if (dsd == NULL)	return (win_printf("can't create data set"));

    dsb = create_and_attach_one_ds(op, nxn, nxn, 0);
    if (dsb == NULL)	return (win_printf("can't create data set"));


    for (a = -2.0, start = 1; a < 2.0; a += 0.05, start = 0)
      {
	for (i=0, x1 = y1 = 0; i < nxn; i++)
	  {
	    dss1->xd[i] = dsi1->xd[i] + a * (dsi2->xd[i] - dsi1->xd[i]);
	    dss1->yd[i] = dsi1->yd[i] + a * (dsi2->yd[i] - dsi1->yd[i]);
	    x1 += dss1->xd[i];
	    y1 += dss1->yd[i];
	  }
	x1 /= nxn; y1 /= nxn;
	for (i=0, x2 = y2 = 0; i < nxn; i++)
	  {
	    tmp = dss1->xd[i] - x1;
	    x2 += tmp*tmp;
	    tmp = dss1->yd[i] - y1;
	    y2 += tmp*tmp;
	  }
	x2 /= nxn; y2 /= nxn;
	if (start != 0 || (x2 + y2) < x2m )
	  {
	    b = a;
	    x2m = x2 + y2;
	  }
      }
    for (i=0; i < nxn; i++)
      {
	dsb->xd[i] = dss1->xd[i] = dsi1->xd[i] + b * (dsi2->xd[i] - dsi1->xd[i]);
	dsb->yd[i] = dss1->yd[i] = dsi1->yd[i] + b * (dsi2->yd[i] - dsi1->yd[i]);
      }


    win_printf("b %g",b);


    for (i=0, x1 = y1 = x2 = y2 = 0; i< nxn ; i++)
      {
	x1 += dsi1->xd[i];
	y1 += dsi1->yd[i];
	x2 += dsi2->xd[i];
	y2 += dsi2->yd[i];
      }
    x1 /= nxn; y1 /= nxn; x2 /= nxn; y2 /= nxn;

    for (i=0; i < nxn; i++)
      {
	dss2->xd[i] = dsi2->xd[i] - dss1->xd[i];
	dss2->yd[i] = dsi2->yd[i] - dss1->yd[i];
	dss1->xd[i] = dsi1->xd[i] - dss1->xd[i];
	dss1->yd[i] = dsi1->yd[i] - dss1->yd[i];
      }

    for (i=0, tmp = 0; i < nxn; i++)
      tmp += dss1->xd[i];
    for (i=0, tmp /= nxn; i < nxn; i++)
      dsd->xd[2*i] = dss1->xd[i] -= tmp - x1;

    dsd->xd[2*nxn] = x1;
    for (i=0, tmp = 0; i < nxn; i++)
      tmp += dss1->yd[i];
    for (i=0, tmp /= nxn; i < nxn; i++)
      dsd->yd[2*i] = dss1->yd[i] -= tmp - y1;
    dsd->yd[2*nxn] = y1;

    for (i=0, tmp = 0; i < nxn; i++)
      tmp += dss2->xd[i];
    for (i=0, tmp /= nxn; i < nxn; i++)
      dsd->xd[2*i +1] = dss2->xd[i] -= tmp - x2;
    for (i=0, tmp = 0; i < nxn; i++)
      tmp += dss2->yd[i];
    for (i=0, tmp /= nxn; i < nxn; i++)
      dsd->yd[2*i+1] = dss2->yd[i] -= tmp - y2;



    /*
    for (i=0, sp = 0, sd1 = 0; i< nxn; i++)
      {
	tmp = dsi2->yd[i] - dsi1->yd[i] - y2 + y1;
	sp += (dsi1->yd[i] - y1)* tmp;
	sd1 += tmp*tmp;
      }
    ayy1 = (sd1 != 0) ? sp/sd1 : sp;

    for (i=0, sp = 0, sd1 = 0; i< nxn; i++)
      {
	tmp = dsi2->xd[i] - dsi1->xd[i] - x2 + x1;
	sp += (dsi1->yd[i] - y1)* tmp;
	sd1 += tmp*tmp;
      }
    ayx1 = (sd1 != 0) ? sp/sd1 : sp;

    for (i=0; i < nxn; i++)
      dss1->yd[i] = dsi1->yd[i] - ayy1 * (dsi2->yd[i] - dsi1->yd[i]) - ayx1 * (dsi2->xd[i] - dsi1->xd[i]);


    for (i=0, sp = 0, sd1 = 0; i< nxn; i++)
      {
	tmp = dsi2->yd[i] - dsi1->yd[i] - y2 + y1;
	sp += (dsi1->xd[i] - x1)* tmp;
	sd1 += tmp*tmp;
      }
    axy1 = (sd1 != 0) ? sp/sd1 : sp;

    for (i=0, sp = 0, sd1 = 0; i< nxn; i++)
      {
	tmp = dsi2->xd[i] - dsi1->xd[i] - x2 + x1;
	sp += (dsi1->xd[i] - x1)* tmp;
	sd1 += tmp*tmp;
      }
    axx1 = (sd1 != 0) ? sp/sd1 : sp;

    for (i=0; i < nxn; i++)
      dss1->xd[i] = dsi1->xd[i] - axy1 * (dsi2->yd[i] - dsi1->yd[i]) - axx1 * (dsi2->xd[i] - dsi1->xd[i]);


    for (i=0, sp = 0, sd1 = 0; i< nxn; i++)
      {
	tmp = dsi2->yd[i] - dsi1->yd[i] - y2 + y1;
	sp += (dsi2->yd[i] - y2)* tmp;
	sd1 += tmp*tmp;
      }
    ayy2 = (sd1 != 0) ? sp/sd1 : sp;

    for (i=0, sp = 0, sd1 = 0; i< nxn; i++)
      {
	tmp = dsi2->xd[i] - dsi1->xd[i] - x2 + x1;
	sp += (dsi2->yd[i] - y2)* tmp;
	sd1 += tmp*tmp;
      }
    ayx2 = (sd1 != 0) ? sp/sd1 : sp;

    for (i=0; i < nxn; i++)
      dss2->yd[i] = dsi2->yd[i] - ayy2 * (dsi2->yd[i] - dsi1->yd[i]) - ayx2 * (dsi2->xd[i] - dsi1->xd[i]);




    for (i=0, sp = 0, sd1 = 0; i< nxn; i++)
      {
	tmp = dsi2->yd[i] - dsi1->yd[i] - y2 + y1;
	sp += (dsi2->xd[i] - x2)* tmp;
	sd1 += tmp*tmp;
      }
    axy2 = (sd1 != 0) ? sp/sd1 : sp;

    for (i=0, sp = 0, sd1 = 0; i< nxn; i++)
      {
	tmp = dsi2->xd[i] - dsi1->xd[i] - x2 + x1;
	sp += (dsi2->xd[i] - x2)* tmp;
	sd1 += tmp*tmp;
      }
    axx2 = (sd1 != 0) ? sp/sd1 : sp;

    for (i=0; i < nxn; i++)
      dss2->xd[i] = dsi2->xd[i] - axy2 * (dsi2->yd[i] - dsi1->yd[i]) - axx2 * (dsi2->xd[i] - dsi1->xd[i]);

    win_printf("axx1= %g axx2= %g\naxy1= %g axy2= %g\n"
	       "ayx1= %g ayx2= %g\nayy1= %g ayy2= %g",axx1, axx2, axy1, axy2, ayx1, ayx2, ayy1, ayy2);
    */
    dss1->ny = nxn;
    dss1->nx = nxn;
    dss2->ny = nxn;
    dss2->nx = nxn;
    inherit_from_ds_to_ds(dss1, dsi1);
    inherit_from_ds_to_ds(dss2, dsi2);
    /*
    dss1->treatement = my_sprintf(dss1->treatement,"axx1= %g axx2= %g\naxy1= %g axy2= %g\n"
	       "ayx1= %g ayx2= %g\nayy1= %g ayy2= %g",axx1, axx2, axy1, axy2, ayx1, ayx2, ayy1, ayy2);
    dss2->treatement = my_sprintf(dss2->treatement,"axx1= %g axx2= %g\naxy1= %g axy2= %g\n"
	       "ayx1= %g ayx2= %g\nayy1= %g ayy2= %g",axx1, axx2, axy1, axy2, ayx1, ayx2, ayy1, ayy2);
    */
    return refresh_plot(pr, UNCHANGED);
}





int show_camera_corr_2(void)
{
  int i;
  pltreg *pr = NULL;
  static int n_pts = 256, frame = 1;
  O_p *op = NULL, *opd = NULL;
  d_s *dsi = NULL, *dsd = NULL, *dsd1 = NULL;
  int  nx;
  static float a = 1;

  if(updating_menu_state != 0)	return 0;

  i = ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi);
  if ( i != 3)		return win_printf("cannot find data");

  n_pts =  dsi->nx;


  i = win_scanf("This routine display camera spectrum correction \n"
		"due to frame integration nb of points %8d\n"
		"field ->%R frame -> %r %window ratio %8f\n",&n_pts,&frame,&a);
  if (i == WIN_CANCEL)	return OFF;

  nx = n_pts;
  opd = create_and_attach_one_plot(pr, n_pts+1,n_pts+1, 0);
  if (opd == NULL)		return win_printf("Cannot allocate plot");
  dsd = opd->dat[0];
  if (dsd == NULL)		return 1;
  if ((dsd1 = create_and_attach_one_ds(opd, n_pts+1,n_pts+1, 0)) == NULL)
      return win_printf_OK("cannot create ds!");
  dsd->yd[0] = 1;
  dsd1->yd[0] = 0;

  for (i=1 ; i<= nx; i++)
    {
      dsd1->yd[i] = camera_response((float)i/(2*nx), frame, a);

      dsd1->yd[i] += camera_response(1.0 - ((float)i/(2*nx)), frame, a);

      dsd->yd[i] = camera_response((float)i/(2*nx), frame, a);
      dsd->yd[i] += camera_response(1.0 - ((float)i/(2*nx)), frame, a);
      dsd->yd[i] += camera_response(1+(float)i/(2*nx), frame, a);
      dsd->yd[i] += camera_response(2.0 - ((float)i/(2*nx)), frame, a);
      dsd->xd[i] = dsd1->xd[i] = i;
    }
  if ((dsd1 = create_and_attach_one_ds(opd, 4*n_pts,4*n_pts, 0)) == NULL)
      return win_printf_OK("cannot create ds!");
  for (i=1 ; i< 4*nx; i++)
    {
      dsd1->yd[i] = camera_response(((float)i/(2*nx)), frame, a);
      dsd1->xd[i] = i;
    }
  set_plot_title(opd, "Spectrum correction factor");
  opd->filename = Transfer_filename(op->filename);
  uns_op_2_op(opd, op);
  inherit_from_ds_to_ds(dsd, dsi);
  dsd->treatement = my_sprintf(dsd->treatement,"correction factor");
  refresh_plot(pr, pr->n_op-1);
  return 0;
}
int show_camera_corr_on_lorenzien_spe(void)
{
  int i, j, k;
  pltreg *pr = NULL;
  static int n_pts = 256, frame = 1, l_refold = 4;
  O_p *op = NULL, *opd = NULL;
  float tmp, s;
  d_s *dsi = NULL, *dsd = NULL;
  int  nx;
  static float a = 1, fc = 128;

  if(updating_menu_state != 0)	return 0;
  i = ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi);
  if ( i != 3)		return win_printf("cannot find data");

  n_pts =  dsi->nx;


  i = win_scanf("This routine display a lorenzian modified by \n"
		"camera spectrum correction due \to frame integration \n"
		"Nb. of points %12d field ->%R frame -> %r \n"
		"window ratio %12f cut off freq %12f\n"
		"number of refolding %12d\n",&n_pts,&frame,&a,&fc,&refold);
  if (i == WIN_CANCEL)	return OFF;

  nx = n_pts;
  opd = create_and_attach_one_plot(pr, n_pts+1,n_pts+1, 0);
  if (opd == NULL)		return win_printf("Cannot allocate plot");
  dsd = opd->dat[0];
  if (dsd == NULL)		return 1;
  dsd->yd[0] = 1;
  if (refold <= 0)
    {
      for ( l_refold = 1, s = 1; s > accu ; l_refold++)
	{
	  i = (l_refold * 2 * nx) + nx;
	  tmp = 1 + ((float)i*(float)i)/(fc*fc);
	  tmp = 1/tmp;
	  tmp = tmp * camera_response(l_refold + (float)nx/(2*nx), frame, a);
	  s = tmp;
    //win_printf_OK("%d=i %d=lrefold %f=s %dnx tmp=%f %ffregccd", i, l_refold, s, nx, tmp, (float)(l_refold + (float)nx/(2*nx)));
	  i = ((l_refold + 1) * 2 * nx) - nx;
	  tmp = 1 + ((float)i*(float)i)/(fc*fc);
	  tmp = 1/tmp;
    //win_printf_OK("%d=i %d=lrefold %f=s %dnx", i, l_refold, s, nx);
	  tmp = tmp * camera_response(l_refold + 1.0 - ((float)nx/(2*nx)), frame, a);
	  s += tmp;
    //win_printf_OK("%f=s %ftmp %ffreqccdover", s, tmp, (float)(l_refold + 1.0 - ((float)nx/(2*nx))) );
	}
      /*		i = win_printf("fc = %g nx = %d\naccu = %g refold = %d",fc,nx,accu,l_refold); */
      last_refold = l_refold;
      l_refold = (l_refold < 2) ? 2 : l_refold;
    }
  for (j=0 ; j< l_refold; j++)
    {
      for (k=1 ; k<= nx; k++)
	{
	  i = (j * 2 * nx) + k;
	  tmp = 1 + ((float)i*(float)i)/(fc*fc);
	  tmp = 1/tmp;
	  dsd->yd[k] += tmp * camera_response(j + (float)k/(2*nx), frame, a);
	  i = ((j + 1) * 2 * nx) - k;
	  tmp = 1 + ((float)i*(float)i)/(fc*fc);
	  tmp = 1/tmp;
	  dsd->yd[k] += tmp * camera_response(j + 1.0 - ((float)k/(2*nx)), frame, a);

	  dsd->xd[k] = k;
	}
    }
  set_plot_title(opd, "Lorenzienne spectrum fc = %g refold %d",fc,l_refold);
  opd->filename = Transfer_filename(op->filename);
  uns_op_2_op(opd, op);
  inherit_from_ds_to_ds(dsd, dsi);
  dsd->treatement = my_sprintf(dsd->treatement,"correction factor");
  refresh_plot(pr, pr->n_op-1);
  return 0;
}


int	do_compute_deltaZ2_versus_dt(void)
{
  int i, j;
  pltreg *pr = NULL;
  O_p *op = NULL, *opd = NULL;
  d_s *dsi = NULL, *dsd = NULL;
  float  y, dy, ay, y1, y2, at, dt;
  int nx, imin, imax;
  static float maxdt = 1;
  static int rm_line = 1, disp_nm = 1, dump_in_op_copied = 0;
  un_s *un = NULL;
  double a = 0, b = 0, sy2 = 0, sy = 0, sx = 0, sx2 = 0, sxy = 0;

  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");

  for (i = imax = 0, imin = dsi->nx - 1; i < dsi->nx ; i++)
    {
      if (dsi->xd[i] >= op->x_lo  && dsi->xd[i] < op->x_hi)
	{  // visible points
	  imin = (i < imin) ? i : imin;
	  imax = (i > imax) ? i : imax;
	}
    }
  if ((imax == 0) || (imin == (dsi->nx - 1)))
    return win_printf_OK("cannot find visible data to treat!");


  un = op->yu[op->c_yu];
  if (un->type != IS_METER)
    win_printf("warning the y unit is not a length!\n"
	       "numerical value will be wrong");
  un = op->xu[op->c_xu];
  if (un->type != IS_SECOND)
    win_printf("warning the x unit is not a time!\n"
	       "numerical value will be wrong");


  i = win_scanf("This routine test for a diffusive behavior\n"
		"To do so it compute <\\Delta Z^2(\\dela t)> of visible points\n"
		"Do you want result in %R\\mu m^2 or %rnm^2\n"
		"For bias diffusion do you want to substract a linear fit\n"
		"to points before computation %b\n"
		"Maximum \\Delta t %10f\n"
		"%R Generate result in a new plot\n"
		"%r or in the Copied plot\n"
		"%r or add data set to Copied plot\n",
		&disp_nm,&rm_line,&maxdt,&dump_in_op_copied);

  un = op->xu[op->c_xu];
  get_afine_param_from_unit(un, &at, &dt);

  un = op->yu[op->c_yu];
  get_afine_param_from_unit(un, &ay, &dy);
  dy *= 1000000; // we work in microns
  if (disp_nm)   dy *= 1000; // we work in microns



  if (rm_line)
    {
      for (i=imin; i< imax ; i++)
    	{
	  sy += dsi->yd[i];
	  sy2 += dsi->yd[i] * dsi->yd[i];
	  sx += dsi->xd[i];
	  sx2 += dsi->xd[i] * dsi->xd[i];
	  sxy += dsi->xd[i] * dsi->yd[i];
	}
      b = (sx2 * (imax - imin)) - sx * sx;
      if (b == 0) return win_printf_OK("\\Delta  = 0 \n can't fit line to remove!");
      a = sxy * (imax - imin) - sx * sy;
      a /= b;
      b = (sx2 * sy - sx * sxy)/b;
    }


  nx = imax - imin;
  if (dump_in_op_copied == 1)
    {
      if (op_copied != NULL)
	{
	  free_one_plot(op_copied);
	  op_copied = NULL;
	}
      op_copied = opd = create_one_plot( nx, nx, 0);
      if (opd == NULL) return win_printf("cannot create plot !");
      dsd = opd->dat[0];
    }
  else if (dump_in_op_copied == 2)
    {
      if (op_copied != NULL)
	{
	  opd = op_copied;
	  dsd = create_and_attach_one_ds(opd, nx, nx, 0);
	  if (dsd == NULL) return win_printf("cannot create ds !");
	}
      else
	{
	  op_copied = opd = create_one_plot( nx, nx, 0);
	  if (opd == NULL) return win_printf("cannot create plot !");
	  dsd = opd->dat[0];
	}
    }
  else
    {
      opd = create_and_attach_one_plot(pr, nx, nx, 0);
      if (opd == NULL) return win_printf("cannot create plot !");
      dsd = opd->dat[0];
    }

  for(i = 1; i < nx; i++)
    {
      for(j = i; j < nx && ((1+j-i)*dt < maxdt); j++)
	{
	  y1 = dsi->yd[imin+i-1] - a*dsi->xd[imin+i-1] + b;
	  y2 = dsi->yd[imin+j] - a*dsi->xd[imin+j] + b;
	  y = dy*(y1 - y2);
	  dsd->yd[1+j-i] += y*y;
	  dsd->xd[1+j-i] += 1;
	}
    }
  for(i = 0; i < nx; i++)
    {
      dsd->yd[i] = (dsd->xd[i] > 0) ? dsd->yd[i]/dsd->xd[i] : dsd->yd[i];
      dsd->xd[i] = i;
    }
  i = (int)(0.5+(maxdt/dt));
  if (i < dsd->nx) dsd->nx = dsd->ny = i;
  inherit_from_ds_to_ds(dsd, dsi);
  set_ds_treatement(dsd,"diffusion plot between points %d and %d",imin,imax);
  uns_op_2_op_by_type(op, IS_X_UNIT_SET, opd, IS_X_UNIT_SET);
  if (disp_nm) set_plot_y_title(opd,"<\\Delta Z^2> (nm^2)");
  else   set_plot_y_title(opd,"<\\Delta Z^2> (\\mu m^2)");
  set_plot_x_title(opd,"\\Delta t");
  if (dump_in_op_copied) return refresh_plot(pr, UNCHANGED);
  else return refresh_plot(pr,pr->n_op-1);
}


MENU *cardinal_plot_menu(void)
{
  static MENU mn[32] = {0}, mn1[32] = {0}, mn2[32] = {0};

    if (mn[0].text != NULL)    return mn;





    add_item_to_menu(mn,"(y - <y>)^2", mean_y2,NULL,MENU_INDEX(0),NULL);
    add_item_to_menu(mn,"(y - <y>)^2 Hanning", mean_y2,NULL,MENU_INDEX(1),NULL);
    add_item_to_menu(mn,"remove DC", remove_mean_ds,NULL,MENU_INDEX(1),NULL);
    add_item_to_menu(mn,"x(t) et y(t)", y_de_t_et_x_t_new,NULL,0,NULL);
    add_item_to_menu(mn,"Fit spectrum", draw_delta_de_fc_4,NULL,0,NULL);
    add_item_to_menu(mn,"Fit spectrum in a new file", draw_delta_de_fc_4,NULL,MENU_INDEX(1),NULL);



    add_item_to_menu(mn,"Fit torque spectrum", draw_delta_de_fc_4_torque,NULL,0,NULL);
    add_item_to_menu(mn,"Fit torque spectrum in a new file", draw_delta_de_fc_4_torque,NULL,MENU_INDEX(1),NULL);

    add_item_to_menu(mn,"lin/log avg", do_lin_log_spectrum_avg,NULL,0,NULL);
    add_item_to_menu(mn,"Slidding spectrum", do_slidding_spectrum,NULL,0,NULL);
    add_item_to_menu(mn,"Remove glitch", correct_simple_glitch,NULL,0,NULL);

    //    add_item_to_menu(mn,"Find anisotry angle", find_locale_rotate_plot,NULL,1,NULL);
    add_item_to_menu(mn,"Rotate ds min. anisotropy", find_anisotropy_and_rotate_plot,NULL,1,NULL);
    add_item_to_menu(mn,"Retreat", do_full_new_treat_x_y_z_acq_spe,NULL,1,NULL);
    add_item_to_menu(mn,"Retreat pico", do_full_new_treat_x_y_z_acq_spe_pico,NULL,1,NULL);
    add_item_to_menu(mn,"Retreat 2", do_full_new_treat_x_y_z_acq_spe_2,NULL,1,NULL);
    add_item_to_menu(mn,"Retreat 2 pico", do_full_new_treat_x_y_z_acq_spe_2_pico,NULL,1,NULL);
    add_item_to_menu(mn,"Load cardinal xv file", load_cardinal_xvplot_file,NULL,1,NULL);
    add_item_to_menu(mn,"Extract ds from multiple op",do_load_one_ds_from_multiple_op,NULL,1,NULL);
    add_item_to_menu(mn,"Convert Zmag to F in scan",fit_force_versus_zmag,NULL,1,NULL);
    add_item_to_menu(mn,"Average step over X and Y",average_x_y_over_step,NULL,1,NULL);
    add_item_to_menu(mn,"Average modulation",average_modulation,NULL,1,NULL);
    add_item_to_menu(mn,"Average step with transient",extract_z_scan,NULL,1,NULL);
    add_item_to_menu(mn,"extract 2 states mod.",extract_two_step_modulation,NULL,1,NULL);
    add_item_to_menu(mn,"deriv noise",do_fft_and_derivate,NULL,1,NULL);
    add_item_to_menu(mn,"deriv noise 2",do_fft_and_derivate_2,NULL,1,NULL);
    add_item_to_menu(mn,"High stiffness",do_fft_and_fit_high_stiffness,NULL,MENU_INDEX(1),NULL);
    add_item_to_menu(mn,"Diffusion test",do_compute_deltaZ2_versus_dt,NULL,0,NULL);
    add_item_to_menu(mn,"SFT noise",do_sft_real_noise,NULL,MENU_INDEX(1),NULL);




    add_item_to_menu(mn1,"Reload Hat data",reload_hat_data,NULL,1,NULL);
    add_item_to_menu(mn1,"Hat data -> Force",reload_hat_data_and_force,NULL,1,NULL);
    add_item_to_menu(mn1,"Scan Z data -> Force",reload_scan_data_and_force,NULL,1,NULL);
    add_item_to_menu(mn1,"Hat data -> Force new acq",reload_hat_data_and_force_2,NULL,1,NULL);
    add_item_to_menu(mn1,"Scan Z data -> Force new acq",reload_scan_data_and_force_2,NULL,1,NULL);
    add_item_to_menu(mn1,"Camera correction",show_camera_corr_2,NULL,1,NULL);
    add_item_to_menu(mn1,"Lorenz correction",show_camera_corr_on_lorenzien_spe,NULL,1,NULL);

    add_item_to_menu(mn,"more", NULL, mn1, 0, NULL);
    add_item_to_menu(mn2,"fit affine",fit_affine,NULL,0,NULL);
    add_item_to_menu(mn2,"Find 2bds rotation center",fit_2_bds_rotation,NULL,0,NULL);


    add_item_to_menu(mn,"more 2", NULL, mn2, 0, NULL);



    return mn;
}

int    cardinal_main(int argc, char **argv)
{
   (void)argc;
   (void)argv;
    xvplot_main(0, NULL);
    add_plot_treat_menu_item ( "Brownian motion analysis", NULL, cardinal_plot_menu(), 0, NULL);
    return D_O_K;
}

int    cardinal_unload(int argc, char **argv)
{
   (void)argc;
   (void)argv;
        remove_item_to_menu(image_treat_menu, "cardinal", NULL, NULL);
    return D_O_K;
}


#endif
