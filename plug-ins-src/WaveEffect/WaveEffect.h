#ifndef _WAVEEFFECT_H_
#define _WAVEEFFECT_H_

PXV_FUNC(int, do_WaveEffect_rescale_plot, (void));
PXV_FUNC(MENU*, WaveEffect_plot_menu, (void));
PXV_FUNC(int, do_WaveEffect_rescale_data_set, (void));
PXV_FUNC(int, WaveEffect_main, (int argc, char **argv));
#endif

