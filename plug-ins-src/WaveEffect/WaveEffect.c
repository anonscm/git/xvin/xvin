/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _WAVEEFFECT_C_
#define _WAVEEFFECT_C_

# include "allegro.h"
# include "xvin.h"

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
//place here other headers of this plugin 
# include "allegro.h"
# include "xvin.h"

/* If you include other regular header do it here*/ 

// nous avons besoin des fonctions reliees � la fft
# include "fft_pow2.h"
# include "waveop.h"


/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "WaveEffect.h"




int do_WaveEffect_hello(void)
{
  register int i;

  if(updating_menu_state != 0)	return D_O_K;

  i = win_printf("Hello from WaveEffect");
  if (i == CANCEL) win_printf_OK("you have press CANCEL");
  else win_printf_OK("you have press OK");
  return 0;
}



int save_wave(void)
{
  pltreg *pr;
  O_p *op;
  d_s *dsi;
  int i;
  static char file[1024];
  static int first = 1;
  int NbCanaux, Frequence = 0, BitsPerSample;
  float  freqf, dt;
  un_s *un;

  if(updating_menu_state != 0)	return D_O_K;

  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");

  if (first)   file[first = 0] = 0;

  if (Frequence == 0)
    {
      un = op->xu[op->c_xu];
      if (un->type != IS_SECOND || un->decade != 0)
	win_printf("warning the x unit is not seconds !\nnumerical value will be wrong");
      dt = un->dx * (dsi->xd[dsi->nx-1] - dsi->xd[0]);
      dt = (dsi->nx > 1) ? dt/(dsi->nx-1) : dt;
      freqf = (dt > 0) ? ((float)1)/dt : 44100;
      Frequence = (int)freqf;
      NbCanaux = op->n_dat;
      BitsPerSample = 16;
    }


  if (do_select_file(file, sizeof(file), "WAV_FILE", "wav", "Wav Files (*.wav)\0*.wav\0All Files (*.*)\0*.*\0\0" , 0))
    return win_printf_OK("cannot save file");

  i = win_scanf("Nb de canaux %8d frequence %10d\n"
	    "Bits per sample %8d\n"
	    ,&NbCanaux, &Frequence, &BitsPerSample);
  if (i == CANCEL) return 0;

  i = save_wave_from_op(file, op, NbCanaux, Frequence, BitsPerSample);
  if (i) win_printf("Error at %d in saving file %s",i,backslash_to_slash(file));
  return D_O_K;
}

int read_wave(void)
{
  pltreg *pr;
  O_p *op;
  int i;
  static char file[1024];
  static int first = 1, verbose = 0;

  if(updating_menu_state != 0)	return D_O_K;

  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return win_printf_OK("cannot find data");

  if (first)   file[first = 0] = 0;

  if (do_select_file(file, sizeof(file), "WAV_FILE", "wav", "Wav Files (*.wav)\0*.wav\0All Files (*.*)\0*.*\0\0" , 1))
    return win_printf_OK("cannot load file");

  i = win_scanf("Read wave file Verbose %b",&verbose);
  if (i == CANCEL) return 0;

  op = read_wave_create_op(file,verbose);
  if (op == NULL) 
    win_printf("Error in reading file %s",backslash_to_slash(file));
  if (add_data_to_pltreg(pr, IS_ONE_PLOT, (void*)op))	
    free_one_plot(op);
  refresh_plot(pr, pr->n_op - 1);
  return D_O_K;
}

int	do_expand(void)
{
  register int i;
  O_p *op = NULL;
  d_s *dsi, *dsd, *dst;
  pltreg *pr = NULL;
  static int size = 512, show = 0;
  static float expand = 1.5;
  int nxe, dx, ns, ne;


  if(updating_menu_state != 0)	return D_O_K;
  
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine expand a sound sample ");
    }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");

  // we ask to the operator what he wants
  i = win_scanf("This routine expands a sound signal, without modifying pitch\n"
		"Specify the desired expnsion factor %8f\n"
		"specify the size of the block use to treat %8d\n"
		"show the process No %R Yes %r all %r\n"
		,&expand,&size,&show);

  if (i == CANCEL) return D_O_K;
  // we compute the size of the output
  nxe = (int)(expand*dsi->nx);
  dx = (int)(((float)size)/(expand*2));

  dsd = create_and_attach_one_ds(op, nxe, nxe, 0);
  if (dsd == NULL)       return win_printf_OK("could not create ds");
  set_ds_source(dsd,"signal expanded by %g using block size %d",expand,size); 

  dst = build_data_set(size, size);
  if (dst == NULL)       return win_printf_OK("could not create ds");
  if (show) add_one_plot_data (op, IS_DATA_SET, (void *)dst);

  //////////////////////////////////////////////////////////////////////////////////
  // partie du programme a analyser
  // nous modifions le signal pour l'allonger
  for (ns = ne = 0; (ns + size) <= dsi->nx && (ne + size) < dsd->nx; ns += dx, ne += size/2)
    { // nous allons traiter le signal par bloc ayant la taille size
      if (dst == NULL)
	{
	  dst = create_and_attach_one_ds(op, size, size, 0);
	  if (dst == NULL)       return win_printf_OK("could not create ds");
	}
      for (i=0 ; i< size ; i++) 
	{  // nous recopions le signal dans un bloc ayant "size" points
	  dst->yd[i] = dsi->yd[ns + i];
	  dst->xd[i] = dsi->xd[ns + i];
	}
      // nous multiplions le signal par une fonction fenetre 1-cos(M_PI*i/size) 
      // qui est maximum au centre et s'annule � chaque bord   
      fftwindow(size, dst->yd);
      // nous ajoutons ce bloc au signal total final 
      for (i=0 ; i< size ; i++) 
	{	     
	  dsd->yd[ne + i] += dst->yd[i];
	  dsd->xd[ne + i] = ne + i;
	}
      if (show) 
	{
	  op->need_to_refresh = 1;    
	  refresh_plot(pr, UNCHANGED);
	  if (show == 2) dst = NULL;
	}
    }
  ////////////////////////////////////////////////////////////////////////////////////
  if (show == 0) free_data_set(dst);


  return refresh_plot(pr, UNCHANGED);
}







MENU *WaveEffect_plot_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"Hello example", do_WaveEffect_hello,NULL,0,NULL);
  add_item_to_menu(mn,"save wave",save_wave ,NULL,0,NULL);
  add_item_to_menu(mn,"read wave",read_wave ,NULL,0,NULL);
  add_item_to_menu(mn,"expand",do_expand ,NULL,0,NULL);


  return mn;
}

int	WaveEffect_main(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  add_plot_treat_menu_item ( "WaveEffect", NULL, WaveEffect_plot_menu(), 0, NULL);
  return D_O_K;
}

int	WaveEffect_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  remove_item_to_menu(plot_treat_menu, "WaveEffect", NULL, NULL);
  return D_O_K;
}
#endif

