/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _WAVEEFFECT_C_
#define _WAVEEFFECT_C_

# include "allegro.h"
# include "xvin.h"

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
//place here other headers of this plugin 
# include "allegro.h"
# include "xvin.h"

/* If you include other regular header do it here*/ 

// nous avons besoin des fonctions reliees � la fft
# include "fft_pow2.h"
# include "waveop.h"


/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "WaveEffect.h"




int do_WaveEffect_hello(void)
{
  register int i;

  if(updating_menu_state != 0)	return D_O_K;

  i = win_printf("Hello from WaveEffect");
  if (i == CANCEL) win_printf_OK("you have press CANCEL");
  else win_printf_OK("you have press OK");
  return 0;
}



int save_wave(void)
{
  pltreg *pr;
  O_p *op;
  d_s *dsi;
  int i;
  static char file[1024];
  static int first = 1;
  int NbCanaux, Frequence = 0, BitsPerSample;
  float  freqf, dt;
  un_s *un;

  if(updating_menu_state != 0)	return D_O_K;

  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");

  if (first)   file[first = 0] = 0;

  if (Frequence == 0)
    {
      un = op->xu[op->c_xu];
      if (un->type != IS_SECOND || un->decade != 0)
	win_printf("warning the x unit is not seconds !\nnumerical value will be wrong");
      dt = un->dx * (dsi->xd[dsi->nx-1] - dsi->xd[0]);
      dt = (dsi->nx > 1) ? dt/(dsi->nx-1) : dt;
      freqf = (dt > 0) ? ((float)1)/dt : 44100;
      Frequence = (int)freqf;
      NbCanaux = op->n_dat;
      BitsPerSample = 16;
    }


  if (do_select_file(file, sizeof(file), "WAV_FILE", "wav", "Wav Files (*.wav)\0*.wav\0All Files (*.*)\0*.*\0\0" , 0))
    return win_printf_OK("cannot save file");

  i = win_scanf("Nb de canaux %8d frequence %10d\n"
	    "Bits per sample %8d\n"
	    ,&NbCanaux, &Frequence, &BitsPerSample);
  if (i == CANCEL) return 0;

  i = save_wave_from_op(file, op, NbCanaux, Frequence, BitsPerSample);
  if (i) win_printf("Error at %d in saving file %s",i,backslash_to_slash(file));
  return D_O_K;
}

int read_wave(void)
{
  pltreg *pr;
  O_p *op;
  int i;
  static char file[1024];
  static int first = 1, verbose = 0;

  if(updating_menu_state != 0)	return D_O_K;

  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return win_printf_OK("cannot find data");

  if (first)   file[first = 0] = 0;

  if (do_select_file(file, sizeof(file), "WAV_FILE", "wav", "Wav Files (*.wav)\0*.wav\0All Files (*.*)\0*.*\0\0" , 1))
    return win_printf_OK("cannot load file");

  i = win_scanf("Read wave file Verbose %b",&verbose);
  if (i == CANCEL) return 0;

  op = read_wave_create_op(file,verbose);
  if (op == NULL) 
    win_printf("Error in reading file %s",backslash_to_slash(file));
  if (add_data_to_pltreg(pr, IS_ONE_PLOT, (void*)op))	
    free_one_plot(op);
  refresh_plot(pr, pr->n_op - 1);
  return D_O_K;
}


int	do_sono_gram(void)
{
  register int i;
  O_p *op = NULL;
  O_i *oi = NULL;
  d_s *dsi;
  pltreg *pr = NULL;
  imreg *imr;
  union pix *pd;
  static int onx, ony = 256, nsi, ns;
  float *sp, freqf, dt;
  un_s *un;
  //float min = 0, max = 0; 


  if(updating_menu_state != 0)	return D_O_K;
  
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine convert a sound wave in a \n"
			   "sonogram ");
    }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");

  i = win_scanf("Enter slidding window size (a power of 2) %d ",&ony);
  if ( i == CANCEL) return D_O_K;
  if (fft_init(ony)) return win_printf("Cannot init fft");
  //  onx = (int)((2*(dsi->nx - ony))/ony);


  un = op->xu[op->c_xu];
  if (un->type != IS_SECOND || un->decade != 0)
    win_printf("warning the x unit is not seconds !\nnumerical value will be wrong");
  dt = un->dx * (dsi->xd[dsi->nx-1] - dsi->xd[0]);
  dt = (dsi->nx > 1) ? dt/(dsi->nx-1) : dt;
  freqf = (dt > 0) ? ((float)1)/dt : 44100;

  for (ns = nsi = 0; (ns + ony) <= dsi->nx; nsi++, ns += ony/4);
  onx = nsi;    

 
  imr = find_imr_in_current_dialog(NULL);
  if (imr == NULL)	imr = create_and_register_new_image_project( 0,   32,  900,  668);
  if (imr == NULL)	return win_printf("could not create imreg");		
  oi = create_one_image(onx, ony/2, IS_COMPLEX_IMAGE);
  sp = (float*)calloc(ony,sizeof(float));
  if (oi == NULL || sp == NULL) return win_printf("Cannot alloc mem");
  pd = oi->im.pixel;
  set_oi_horizontal_extend(oi, 2.5);
  set_oi_vertical_extend(oi, 1.5);

  for (ns = nsi = 0; (ns + ony) <= dsi->nx && nsi < onx; nsi++, ns += ony/4)
    {
      for (i=0 ; i< ony ; i++) sp[i] = dsi->yd[ns + i];
      fftwindow(ony, sp);
      fft_forward_real(ony, sp);
      for (i=0 ; i< ony/2 ; i++) 	
	{
	  pd[i].fl[2*nsi] = sp[2*i];
	  pd[i].fl[2*nsi+1] = sp[2*i+1];
	}
    }

      //inherit_from_ds_to_im(dsi,oi);
  oi->im.mode = LOG_AMP;
  oi->need_to_refresh = ALL_NEED_REFRESH;		
  set_im_x_title(oi,"time");
  set_im_y_title(oi,"frequency");
  free(sp);
  add_image(imr, oi->type, (void*)oi);
  remove_from_image(imr, imr->o_i[0]->im.data_type, (void*)imr->o_i[0]);
  find_zmin_zmax(oi);
  create_attach_select_y_un_to_oi(oi, 0, 0, freqf/ony, 0, 0, "Hz");	
  create_attach_select_x_un_to_oi(oi, IS_SECOND, 0, (float)ony/(4*freqf), 0, 0, "s");	
  return (refresh_image(imr, imr->n_oi - 1));
}

int	do_expand(void)
{
  register int i;
  O_p *op = NULL;
  d_s *dsi, *dsd, *dst;
  pltreg *pr = NULL;
  static int size = 512, show = 0;
  static float expand = 1.5;
  int nxe, dx, ns, ne;


  if(updating_menu_state != 0)	return D_O_K;
  
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine expand a sound sample ");
    }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");

  // we ask to the operator what he wants
  i = win_scanf("This routine expands a sound signal, without modifying pitch\n"
		"Specify the desired expnsion factor %8f\n"
		"specify the size of the block use to treat %8d\n"
		"show the process No %R Yes %r all %r\n"
		,&expand,&size,&show);

  if (i == CANCEL) return D_O_K;
  // we compute the size of the output
  nxe = (int)(expand*dsi->nx);
  dx = (int)(((float)size)/(expand*2));

  dsd = create_and_attach_one_ds(op, nxe, nxe, 0);
  if (dsd == NULL)       return win_printf_OK("could not create ds");
  set_ds_source(dsd,"signal expanded by %g using block size %d",expand,size); 

  dst = build_data_set(size, size);
  if (dst == NULL)       return win_printf_OK("could not create ds");
  if (show) add_one_plot_data (op, IS_DATA_SET, (void *)dst);

  //////////////////////////////////////////////////////////////////////////////////
  // partie du programme a analyser
  // nous modifions le signal pour l'allonger
  for (ns = ne = 0; (ns + size) <= dsi->nx && (ne + size) < dsd->nx; ns += dx, ne += size/2)
    { // nous allons traiter le signal par bloc ayant la taille size
      if (dst == NULL)
	{
	  dst = create_and_attach_one_ds(op, size, size, 0);
	  if (dst == NULL)       return win_printf_OK("could not create ds");
	}
      for (i=0 ; i< size ; i++) 
	{  // nous recopions le signal dans un bloc ayant "size" points
	  dst->yd[i] = dsi->yd[ns + i];
	  dst->xd[i] = dsi->xd[ns + i];
	}
      // nous multiplions le signal par une fonction fenetre 1-cos(M_PI*i/size) 
      // qui est maximum au centre et s'annule � chaque bord   
      fftwindow(size, dst->yd);
      // nous ajoutons ce bloc au signal total final 
      for (i=0 ; i< size ; i++) 
	{	     
	  dsd->yd[ne + i] += dst->yd[i];
	  dsd->xd[ne + i] = ne + i;
	}
      if (show) 
	{
	  op->need_to_refresh = 1;    
	  refresh_plot(pr, UNCHANGED);
	  if (show == 2) dst = NULL;
	}
    }
  ////////////////////////////////////////////////////////////////////////////////////
  if (show == 0) free_data_set(dst);


  return refresh_plot(pr, UNCHANGED);
}


int	do_vocodeur(void)
{
  register int i;
  O_p *op = NULL;
  d_s *dsi, *dso, *dsd;
  pltreg *pr = NULL;
  static int ony = 256, nsi, ns;
  float *sp, *spo, tmp;
  char message[512];
  int ids1, ids2;
  char question[1024];
  //float min = 0, max = 0; 


  if(updating_menu_state != 0)	return D_O_K;
  
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine make a vocodeur effect ");
    }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");

  if (op->n_dat < 2) return win_printf_OK("not enough data sets");
  i = op->cur_dat + 1;
  i = (i < op->n_dat) ? i : i - op->n_dat;


  ids1 = op->cur_dat;                   
  ids2 = (ids1+1 < op->n_dat) ? ids1 + 1 : 0;        
  if (op->n_dat > 24) 
    {
      i = win_scanf("Operation on two data sets\nData set 1 %8d data set 2 %8d\n",&ids1,&ids2);
      if (i == CANCEL) return D_O_K;
    }
  else
    {
      snprintf(question,sizeof(question),"Operation on two data sets\n"
	       "First Data set %d\nchoose Data set 2:\n",ids1);
      for(i = 0; i < op->n_dat; i++)
	{
	  if (i == 0)    snprintf(question+strlen(question),sizeof(question)-strlen(question),"%d->%%R ",i);
	  else           snprintf(question+strlen(question),sizeof(question)-strlen(question),"%d->%%r ",i);
	  if (i%10 == 9) snprintf(question+strlen(question),sizeof(question)-strlen(question),"\n");
	}
      i = win_scanf(question,&ids2);
      if (i == CANCEL)       return D_O_K;
    }
  if (ids1 < 0 || ids1 >= op->n_dat)  
    return win_printf_OK("Data set 1 index %d out of range [0, %d[!",ids1,op->n_dat);
  if (ids2 < 0 || ids2 >= op->n_dat)  
    return win_printf_OK("Data set 2 index %d out of range [0, %d[!",ids2,op->n_dat);
  

  dso = op->dat[ids2];
  if (dsi->nx != dso->nx)     return win_printf_OK("data sets have not the same number of points!");

  snprintf(message,sizeof(message),"Vocodeur between data set %d and %d\n"
		"Enter slidding window size (a power of 2) %%8d\n ",op->cur_dat,i);
  i = win_scanf(message,&ony);
  if ( i == CANCEL) return D_O_K;
  if (fft_init(ony)) return win_printf_OK("Cannot init fft");
  dsd = create_and_attach_one_ds(op, dsi->nx, dsi->nx, 0);
  if (dsd == NULL)       return win_printf_OK("could not create ds");
  set_ds_source(dsd,"vocodeur size %d",ony); 

  sp = (float*)calloc(ony,sizeof(float));
  spo = (float*)calloc(ony,sizeof(float));
  if (sp == NULL || spo == NULL) return win_printf_OK("Cannot alloc mem");
  for (ns = nsi = 0; (ns + ony) <= dsi->nx; nsi++, ns += ony/2)
    {
      for (i=0 ; i< ony ; i++) 
	{
	  sp[i] = dsi->yd[ns + i];
	  spo[i] = dso->yd[ns + i];
	}
      fftwindow(ony, sp);
      fftwindow(ony, spo);
      fft_forward_real(ony, sp);
      fft_forward_real(ony, spo);
      for (i=1 ; i< ony/2 ; i++) 	
	{
	  tmp = sqrt(sp[2*i] * sp[2*i] + sp[2*i+1] * sp[2*i+1]);
	  spo[2*i] *= tmp;
	  spo[2*i+1] *= tmp;
	}
      fft_backward_to_real(ony,spo);
      for (i=0 ; i< ony ; i++) 	     
	{
	  dsd->yd[ns + i] += 2*spo[i]/ony;
	  dsd->xd[ns + i] = dsi->xd[ns + i];
	}
    }
  free(sp);
  free(spo);
  return refresh_plot(pr, UNCHANGED);
}






MENU *WaveEffect_plot_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"Hello example", do_WaveEffect_hello,NULL,0,NULL);
  add_item_to_menu(mn,"save wave",save_wave ,NULL,0,NULL);
  add_item_to_menu(mn,"read wave",read_wave ,NULL,0,NULL);
  add_item_to_menu(mn,"sonogram",do_sono_gram ,NULL,0,NULL);
  add_item_to_menu(mn,"vocodeur",do_vocodeur ,NULL,0,NULL);
  add_item_to_menu(mn,"expand",do_expand ,NULL,0,NULL);


  return mn;
}

int	WaveEffect_main(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  add_plot_treat_menu_item ( "WaveEffect", NULL, WaveEffect_plot_menu(), 0, NULL);
  return D_O_K;
}

int	WaveEffect_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  remove_item_to_menu(plot_treat_menu, "WaveEffect", NULL, NULL);
  return D_O_K;
}
#endif

