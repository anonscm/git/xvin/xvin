char fullfile[16384];



/*   select a file with a file grabber using config info
 *   char *fpath            a string where to save the selected file path, it may contain a starting path to search
 *      if the first character is set to zero, search will be done from last save file_type_cfg
 *   int fpath_size         the size of fpath
 *   char *file_type_cfg    the file type save in config file example GR-FILE, TRACKING-FILE
 *   char *extensions        "gr", "trk" or "xv,txt" 
 *   char *loading_description  the help message ending with "\0\0" example : "Wav Files (*.wav)\0*.wav\0All Files (*.*)\0*.*\0\0"
 *   int loading if 1 loading otherwise saving
 */

int do_select_file(char *fpath, int fpath_size, char *file_type_cfg, char *extensions, char *loading_description, int loading)
{
  register int i = 0;
  char path[512], file[256], title[512];
  char *fu = NULL;  
  
  if (fpath == NULL || file_type_cfg == NULL || extensions == NULL) return 1;
  if (fpath[0] != 0)     strncpy(fullfile,fpath,sizeof(fullfile));
  else     
    {
      fu = (char*)strdup((char*)get_config_string(file_type_cfg,(loading)?"last_loaded":"last_saved",NULL));
      if (fu != NULL)	
	{
	  strncpy(fullfile,fu,sizeof(fullfile));
	  free(fu);
	  fu = fullfile;
	}
      else
	{
	  my_getcwd(fullfile, 512);
	  strncat(fullfile,"\\",(sizeof(fullfile) > strlen(fullfile)) ? sizeof(fullfile)-strlen(fullfile) : 0);
	}
    }
  if (loading) snprintf(title,sizeof(title),"Load file (*.%s)",extensions);
  else snprintf(title,sizeof(title),"Save file (*.%s)",extensions);
		 
#ifdef XV_WIN32
  if (full_screen)
    {
#endif
      switch_allegro_font(1);
      i = file_select_ex(title, fullfile, extensions, 512, 0, 0);
      switch_allegro_font(0);
#ifdef XV_WIN32
    }
  else
    {
      extract_file_path(path, 512, fullfile);
      put_backslash(path);
      if (extract_file_name(file, 256, fullfile) != NULL)
	strncpy(fullfile,file,sizeof(fullfile));
      else fullfile[0] = file[0] = 0;
      if (loading) fu = DoFileOpen(title, path, fullfile, sizeof(fullfile), loading_description, extensions);
      else fu = DoFileSave(title, path, fullfile, sizeof(fullfile), loading_description, extensions);
      i = (fu == NULL) ? 0 : 1;
      //win_printf("Path %s",backslash_to_slash(path));
    }
#endif
  if (i != 0) 	
    {
      fu = fullfile;
      strncpy(fpath,fullfile,fpath_size);
      set_config_string(file_type_cfg,(loading)?"last_loaded":"last_saved",fullfile);	
      reorder_visited_path_list(path);
      return 0;
    }
  return 2;	
}


