#ifndef _WAVEOP_H_
#define _WAVEOP_H_

int do_select_file(char *fpath, int fpath_size, char *file_type_cfg, char *extensions, char *loading_description, int loading);
int save_wave_from_op(char *file, O_p *op, int NbCanaux, int Frequence, int BitsPerSample);
O_p* read_wave_create_op(char *file, int verbose);

#endif

