/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _WAVEOP_C_
#define _WAVEOP_C_

# include "allegro.h"
# include "xvin.h"

char fullfile[16384];



/*   select a file with a file grabber using config info
 *   char *fpath            a string where to save the selected file path, it may contain a starting path to search
 *      if the first character is set to zero, search will be done from last save file_type_cfg
 *   int fpath_size         the size of fpath
 *   char *file_type_cfg    the file type save in config file example GR-FILE, TRACKING-FILE
 *   char *extensions        "gr", "trk" or "xv,txt" 
 *   char *loading_description  the help message ending with "\0\0" example : "Wav Files (*.wav)\0*.wav\0All Files (*.*)\0*.*\0\0"
 *   int loading if 1 loading otherwise saving
 */

int do_select_file(char *fpath, int fpath_size, char *file_type_cfg, char *extensions, char *loading_description, int loading)
{
  register int i = 0;
  char path[512], file[256], title[512];
  char *fu = NULL;  
  
  if (fpath == NULL || file_type_cfg == NULL || extensions == NULL) return 1;
  if (fpath[0] != 0)     strncpy(fullfile,fpath,sizeof(fullfile));
  else     
    {
      fu = (char*)strdup((char*)get_config_string(file_type_cfg,(loading)?"last_loaded":"last_saved",NULL));
      if (fu != NULL)	
	{
	  strncpy(fullfile,fu,sizeof(fullfile));
	  free(fu);
	  fu = fullfile;
	}
      else
	{
	  my_getcwd(fullfile, 512);
	  strncat(fullfile,"\\",(sizeof(fullfile) > strlen(fullfile)) ? sizeof(fullfile)-strlen(fullfile) : 0);
	}
    }
  if (loading) snprintf(title,sizeof(title),"Load file (*.%s)",extensions);
  else snprintf(title,sizeof(title),"Save file (*.%s)",extensions);
		 
#ifdef XV_WIN32
  if (full_screen)
    {
#endif
      switch_allegro_font(1);
      i = file_select_ex(title, fullfile, extensions, 512, 0, 0);
      switch_allegro_font(0);
#ifdef XV_WIN32
    }
  else
    {
      extract_file_path(path, 512, fullfile);
      put_backslash(path);
      if (extract_file_name(file, 256, fullfile) != NULL)
	strncpy(fullfile,file,sizeof(fullfile));
      else fullfile[0] = file[0] = 0;
      if (loading) fu = DoFileOpen(title, path, fullfile, sizeof(fullfile), loading_description, extensions);
      else fu = DoFileSave(title, path, fullfile, sizeof(fullfile), loading_description, extensions);
      i = (fu == NULL) ? 0 : 1;
      //win_printf("Path %s",backslash_to_slash(path));
    }
#endif
  if (i != 0) 	
    {
      fu = fullfile;
      strncpy(fpath,fullfile,fpath_size);
      set_config_string(file_type_cfg,(loading)?"last_loaded":"last_saved",fullfile);	
      reorder_visited_path_list(path);
      return 0;
    }
  return 2;	
}






int save_wave_from_op(char *file, O_p *op, int NbCanaux, int Frequence, int BitsPerSample)
{
  FILE *fp;
  unsigned char *uch;
  int i, j, FileSize, tmp, BytePerSec, DataSize, nx, data, noctets;
  short int AudioFormat = 1, NbrCanaux, BytePerBloc, BitsPerSamp;

  if (file == NULL || op == NULL) return -1;
  if (op->n_dat < NbCanaux)     
    {win_printf("op->n_dat %d < Nbcanaux %d",op->n_dat,NbCanaux);return -2;}
  for (i = 0, nx = 0; i < NbCanaux; i++)
    {
      nx = (nx == 0) ? op->dat[i]->nx : nx; 
      nx = (op->dat[i]->nx < nx) ? op->dat[i]->nx : nx; 
    }

  NbrCanaux = (short int)NbCanaux;

  noctets = BitsPerSample/8;
  BytePerBloc = (NbrCanaux * BitsPerSample/8);
  DataSize = nx * BytePerBloc; 
  FileSize = DataSize + 44;
  BytePerSec = Frequence * BytePerBloc;  
  BitsPerSamp = (short int)BitsPerSample;
  fp = fopen(file,"wb");
  if (fp == NULL) {win_printf("could not open file to save wav");return 1;}
  // [Bloc de d�claration d'un fichier au format WAVE] 12 octets
  if (fwrite("RIFF",sizeof(char),4,fp) != 4)    {fclose(fp); return 2;}
  if (fwrite(&FileSize,sizeof(int),1,fp) != 1)  {fclose(fp); return 3;}
  if (fwrite("WAVE",sizeof(char),4,fp) != 4)    {fclose(fp); return 4;}
  // [Bloc d�crivant le format audio]   24 octets
  if (fwrite("fmt ",sizeof(char),4,fp) != 4)    {fclose(fp); return 5;}
  tmp = 16; //Nombre d'octets du bloc - 8  (0x10) 
  if (fwrite(&tmp,sizeof(int),1,fp) != 1)       {fclose(fp); return 6;}
  if (fwrite(&AudioFormat,sizeof(short int),1,fp) != 1)      
    {fclose(fp); return 7;}
  if (fwrite(&NbrCanaux,sizeof(short int),1,fp) != 1)      
    {fclose(fp); return 8;}
  if (fwrite(&Frequence,sizeof(int),1,fp) != 1) {fclose(fp); return 9;}
  if (fwrite(&BytePerSec,sizeof(int),1,fp) != 1) {fclose(fp); return 10;}
  if (fwrite(&BytePerBloc,sizeof(short int),1,fp) != 1)      
    {fclose(fp); return 11;}
  if (fwrite(&BitsPerSamp,sizeof(short int),1,fp) != 1)      
    {fclose(fp); return 12;}
  // [Bloc des donn�es] 8 octets + datasize
  if (fwrite("data",sizeof(char),4,fp) != 4)    {fclose(fp); return 13;}
  if (fwrite(&DataSize,sizeof(int),1,fp) != 1) {fclose(fp); return 14;}
  for (j = 0, uch = (unsigned char*)&data; j < nx; j++)
    {
      for (i = 0; i < NbCanaux; i++)
	{
	  data = (int)op->dat[i]->yd[j];
	  if (fwrite(uch,sizeof(unsigned char),noctets,fp) != noctets)    
	    {fclose(fp); return i*j;}
	}
    }
  fclose(fp);
  return 0;
}


O_p* read_wave_create_op(char *file, int verbose)
{
  FILE *fp;
  O_p *op;
  d_s *ds;
  char ch[16];
  unsigned char uch[4];
  int i, j, FileSize, BytePerSec, DataSize, nx, data, noctets;
  short int AudioFormat = 1, NbrCanaux, BytePerBloc, BitsPerSamp, si;
  int Frequence, offset; 

  if (file == NULL) return NULL;

  fp = fopen(file,"rb");
  if (fp == NULL) return win_printf_ptr("could not open file to read wav");
  // [Bloc de d�claration d'un fichier au format WAVE] 12 octets
  if (fread(ch,sizeof(char),4,fp) != 4)    // read 4
    {      fclose(fp);       return win_printf_ptr("could not read RIFF label");    }
  if (strncmp(ch,"RIFF",4) != 0)
    {      fclose(fp);       return win_printf_ptr("could not read RIFF tag");    }
  if (fread(&FileSize,sizeof(int),1,fp) != 1)      // read 4
    {      fclose(fp);       return win_printf_ptr("could not read File size");    }
  if (verbose)  win_printf("file size %d",FileSize);
  if (fread(ch,sizeof(char),4,fp) != 4)        // read 4
    {      fclose(fp);       return win_printf_ptr("could not read WAVE label");    }
  if (strncmp(ch,"WAVE",4) != 0)         
    {      fclose(fp);       return win_printf_ptr("could not read WAVE tag");    }

  // [Bloc d�crivant le format audio]   24 octets
  if (fread(ch,sizeof(char),4,fp) != 4)   // read 4  
    {      fclose(fp);       return win_printf_ptr("could not read fmt label");    }
  if (strncmp(ch,"fmt ",4) != 0)  
    {      fclose(fp);       return win_printf_ptr("could not read fmt tag");    }
  if (fread(&offset,sizeof(int),1,fp) != 1) // read 4  
    {      fclose(fp);       return win_printf_ptr("could not read offset");    }
  if (verbose) win_printf("offset %d",offset); // read 4
  if (fread(&AudioFormat,sizeof(short int),1,fp) != 1) // read 2  
    {      fclose(fp);       return win_printf_ptr("could not read Audio format");    }
  if (fread(&NbrCanaux,sizeof(short int),1,fp) != 1)   // read 2
    {      fclose(fp);       return win_printf_ptr("could not read Nb. of canaux");    }
  if (verbose) win_printf("NbrCanaux %d",(int)NbrCanaux); // read 4
  if (fread(&Frequence,sizeof(int),1,fp) != 1)  
    {      fclose(fp);       return win_printf_ptr("could not read Frequence");    }
  if (verbose)  win_printf("Frequence %d",Frequence);  // read 4
  if (fread(&BytePerSec,sizeof(int),1,fp) != 1)  
    {      fclose(fp);       return win_printf_ptr("could not read BytePerSec");    }
  if (verbose) win_printf("BytePerSec %d",BytePerSec);
  if (fread(&BytePerBloc,sizeof(short int),1,fp) != 1) // read 2  
    {      fclose(fp);       return win_printf_ptr("could not read BytePerSec");     }
  if (verbose) win_printf("BytePerBloc %d",(int)BytePerBloc);
  if (fread(&BitsPerSamp,sizeof(short int),1,fp) != 1)  // read 2
    {      fclose(fp);       return win_printf_ptr("could not read BitsPerSamp");    }
  if (verbose) win_printf("BitsPerSamp %d",(int)BitsPerSamp);
  // [Bloc des donn�es] 8 octets + datasize
  if (fread(ch,sizeof(char),4,fp) != 4)    // read 4
    {      fclose(fp);       return win_printf_ptr("could not read data label");    }

  if (strncmp(ch,"LIST",4) == 0)
    {
      if (fread(&offset,sizeof(int),1,fp) != 1) // read 4  
	{      fclose(fp);       return win_printf_ptr("could not read offset");    }
      if (verbose) win_printf("found list block skipping %d bytes",offset);
      for (i = 0; i < offset; i++)
	{
	  if (fread(ch,sizeof(char),1,fp) != 1)    
	    {      fclose(fp);       return win_printf_ptr("could not read data");    }
	}
      if (fread(ch,sizeof(char),4,fp) != 4)    // read 4
	{      fclose(fp);       return win_printf_ptr("could not read data label");    }
    }
  if (strncmp(ch,"data",4) != 0)
    {
      ch[4] = 0;
      i = win_printf("could not read data tag!\n read %s instead",ch);
      if (i == CANCEL) 
	{
	  fclose(fp); 
	  return NULL;
	}
    }
  if (fread(&DataSize,sizeof(int),1,fp) != 1)  // read 4
    {
      fclose(fp); 
      return win_printf_ptr("could not read datasize");
    }
  noctets = BitsPerSamp/8;
  nx = DataSize/BytePerBloc;
  if (verbose) win_printf("DataSize %d nx %d noctets %d" ,DataSize,nx,noctets);

  if (BytePerBloc != (NbrCanaux * BitsPerSamp/8))
    {
      fclose(fp); 
      return win_printf_ptr("Byte per bloc pb!\n"
			    "Byte per bloc %d != NbrCanaux %d x Bitspersample %d/8"
			    ,BytePerBloc,(int)NbrCanaux,(int)BitsPerSamp);
    }
  if (FileSize != DataSize + 44)
    {
      i = win_printf("File size pb!\n"
		     "File size %d != datasize %d + 44"
		     ,FileSize,DataSize);
      if (i == CANCEL) 
	{
	  fclose(fp); 
	  return NULL;
	}
    }

  if (BytePerSec != Frequence * BytePerBloc)
    {
      i = win_printf("Byte count pb!\n"
		     "Byte per sec %d != Frequence %d x Byte per bloc %d"
		     ,BytePerSec,Frequence,BytePerBloc);
      if (i == CANCEL)
	{
	  fclose(fp); 
	  return NULL;
	}
    }  

  op = create_one_plot(nx, nx, 0);
  if (op == NULL) 
    {
      fclose(fp); 
      return win_printf_ptr("could not create plot");
    }
  ds = op->dat[0];
  set_ds_source(ds,"Canal 0/%d of %s, Frequence = %d, BitsPerSample = %d"
		,NbrCanaux,file,Frequence,(int)BitsPerSamp); 
  create_attach_select_x_un_to_op(op, IS_SECOND, 0,(float)1/Frequence, 0, 0, "s");
  
  for (i = 1; i < NbrCanaux; i++)
    {
      ds = create_and_attach_one_ds(op, nx, nx, 0);
      if (ds == NULL) 
	{
	  fclose(fp); 
	  return win_printf_ptr("could not create ds");
	}  
      set_ds_source(ds,"Canal %d/%d of %s, Frequence = %d, BitsPerSample = %d"
		    ,i,NbrCanaux,file,Frequence,(int)BitsPerSamp); 
    }
  for (j = 0; j < nx; j++)
    {
      for (i = 0; i < NbrCanaux; i++)
	{
	  uch[0] = uch[1] = uch[2] = uch[3] = 0;
	  data = 0;
	  if (noctets == 1)      
	    {
	      if (fread(uch,sizeof(unsigned char),noctets,fp) != noctets)  
		{
		  fclose(fp); 
		  return win_printf_ptr("could not read unsigned int data at %d %d",i,j);
		}
	      data = uch[0];
	    }
	  if (noctets == 2)
	    {
	      if (fread(&si,sizeof(short int),1,fp) != 1)  
		{
		  fclose(fp); 
		  return win_printf_ptr("could not read short int data at %d %d",i,j);
		}
	      data = (int)si;
	    }
	  if (noctets == 3)
	    { // to be checked
	      if (fread(uch,sizeof(unsigned char),noctets,fp) != noctets)  
		{
		  fclose(fp); 
		  return win_printf_ptr("could not read data at %d %d",i,j);
		}
	      data = (uch[0] << 8) | (uch[1] << 16) | (uch[2] << 24);
	      data /= 256;
	    }
	  /*
	  if (j < 5)
	    win_printf("uc 0 -> %d 1 -> %d 2 -> %d 3 ->%d\n data %d"
		       ,(int)uch[0],(int)uch[1],(int)uch[2],(int)uch[3],data);
	  */
	  op->dat[i]->yd[j] = (float)data;
	  op->dat[i]->xd[j] = (float)j;
	}
    }
  fclose(fp);
  set_plot_title(op, "\\stack{{Wave file : %s}"
		 "{\\pt8 Frequence = %d, BitsPerSample = %d}}"
		  ,backslash_to_slash(file),Frequence,(int)BitsPerSamp);

  return op;
}

#endif
