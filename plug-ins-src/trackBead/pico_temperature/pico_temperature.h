#ifndef _PICO_TEMPERATURE_H_
#define _PICO_TEMPERATURE_H_


# ifndef _PICO_TEMPERATURE_C_

# endif

# ifdef _PICO_TEMPERATURE_C_

# endif
PXV_FUNC(MENU*, serialw_menu, (void));
PXV_FUNC(int, serialw32_main, (int argc, char **argv));

PXV_FUNC(MENU*, pico_temperature_plot_menu, (void));
PXV_FUNC(int, pico_temperature_main, (int argc, char **argv));

PXV_FUNC(int, _request_read_temperature_value, (int image_n, int t, unsigned long *t0));
PXV_FUNC(float, _read_temperature_from_request_answer, (char *answer, int im_n, float val, int *error, int *channel));

PXV_FUNC(int, _set_T0_pid, (int num, int val));
PXV_FUNC(int,  _get_T0_pid, (int num));

PXV_FUNC(int, _set_temperature,(float z));
PXV_FUNC(float, _get_temperature,(int t));
PXV_FUNC(int, _set_light,(float z));
PXV_FUNC(float, _get_light,());

#endif
