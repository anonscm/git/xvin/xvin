/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _PICO_TEMPERATURE_C_
#define _PICO_TEMPERATURE_C_

# include "allegro.h"
#ifdef XV_WIN32
# include "winalleg.h"
#endif
# include "xvin.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// these rotines all work in the Real time timer thread
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

/* If you include other plug-ins header do it here*/
# include "../pico_serial/pico_serial.h"

/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "pico_temperature.h"
# include "../action.h"
# include "../../fft2d/fftbtl32n.h"
# include "../fillibbt.h"

# include "../trackBead.h"
# include "../track_util.h"

int _request_read_temperature_value(int image_n, int t, unsigned long *t0)
{
    char command[128];

    if (rs232_register(READ_TEMPERATURE_REQUEST, image_n, (float)t))
    {
        return 1;    // rs232 buisy
    }

    if (t0 != NULL)
    {
        *t0 = my_uclock();
    }

    if (t < 0 || t > 3)
    {
        return 2;
    }

    snprintf(command, 128, "T%d?\r", t);
    Write_serial_port(hCom, command, strlen(command));

    if (t0 != NULL)
    {
        *t0 = my_uclock() - *t0;
    }

    return 0;
}

float _read_temperature_from_request_answer(char *answer, int im_n, float val, int *error, int *channel)
{
    float t = 0;
    *error = 0;

    (void)val;
    (void)im_n;
    if (answer != NULL)
    {
        if (sscanf(answer, "T%d = %f", channel, &t) == 2)
        {
            return t;
        }
        else
        {
            *error |= PICO_COM_ERROR;
        }
    }
    else
    {
        *error =  PICO_COM_ERROR;
    }

    return t;
}


int _set_temperature(float z)
{
    char command[128] = {0};
    char answer[128] = {0};
    int  ret = 0;
    float set = -1;
    unsigned long len = 0;
    unsigned long dwErrors;
    snprintf(command, 128, "R0=%g\r", z);
    /*Write_serial_port(hCom,command,strlen(command));
    for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
      ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
      answer[len] = 0; */

    ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);

    if (ret)
    {
        return (ret - 5);
    }

    if (sscanf(answer, "set R0 at %g", &set) != 1)
    {
        return -1;
    }

    return (z = set) ? 0 : -1;
}

float _get_temperature(int t)
{
    char command[128] = {0};
    char answer[128] = {0};
    int   ret = 0;
    int Tn = -1;
    float set = -1;
    //static float last[3] = {0,0,0};
    unsigned long len = 0;
    unsigned long dwErrors;

    if (t < 0 || t > 3)
    {
        return 0.;
    }

    //if (t == 3)        snprintf(command,128,"A3?\r"); else
    snprintf(command, 128, "T%d?\r", t);
    /*Write_serial_port(hCom,command,strlen(command));
    for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
    answer[len] = 0;*/

    ret = sent_cmd_and_read_answer(hCom, command, answer, sizeof(answer), &len, &dwErrors, NULL);

    if (ret)
    {
        return (float)(ret - 5);
    }

    if (sscanf(answer, "T%d is at %f", &Tn, &set) != 2)
    {
        my_set_window_title("Can't read temperature from RS232 received message: \"%s\" interpreted as: T%d is at %f", answer,
                            Tn, set);
        debug_message("Can't read temperature from RS232 received message: \"%s\" interpreted as: T%d is at %f", answer, Tn,
                      set);
        return -1.;
    }

    return set;
}

int _set_T0_pid(int num, int val)
{
    char command[128], answer[128], ison[32];
    unsigned long len = 0;
    int  set = -1, ret, iret = 0;
    unsigned long dwErrors;

    if (num < 0 || num > 6)
    {
        return 1;
    }

    if (num < 3 && (val < 0 || val > 255))
    {
        return 2;
    }
    else if (num == 4 && (val < 0 || val > 2))
    {
        return 3;
    }
    else if (num > 4 && num < 7 && (val < 0 || val > 65535))
    {
        return 4;
    }

    snprintf(command, 128, "tpid%d=%d\r", num, val);
    ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);

    if (ret != 0)
    {
        return ret;
    }

    iret = 0;

    if (num == 0)
    {
        iret = sscanf(answer, "Temp. P %d", &set);
    }
    else if (num == 1)
    {
        iret = sscanf(answer, "Temp. I %d", &set);
    }
    else if (num == 2)
    {
        iret = sscanf(answer, "Temp. D %d", &set);
    }
    else if (num == 3)
    {
        iret = sscanf(answer, "max PWM %d", &set);
    }
    else if (num == 5)
    {
        iret = sscanf(answer, "Temp. max positive PWM %d", &set);
    }
    else if (num == 6)
    {
        iret = sscanf(answer, "Temp. max negative PWM %d", &set);
    }
    else if (num == 4)
    {
        iret = sscanf(answer, "Temp. PID %s", ison);
        set = (strstr(ison, "On")) ? 1 : 0;
    }

    if (iret == 0)
    {
        return 5;
    }

    return (val == set) ? 0 : 1;
}
int  _get_T0_pid(int num)
{
    char command[128], answer[128], ison[32];
    unsigned long len = 0;
    int  set = -1, ret, iret = 0;
    unsigned long dwErrors;

    if (num < 0 || num > 9)
    {
        return -1;
    }

    snprintf(command, 128, "tpid%d?\r", num);
    /*Write_serial_port(hCom,command,strlen(command));
    for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
      ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
      answer[len] = 0;*/

    ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);

    if (ret != 0)
    {
        return (ret - 5);
    }

    if (num == 0)
    {
        iret = sscanf(answer, "Temp. P %d", &set);
    }
    else if (num == 1)
    {
        iret = sscanf(answer, "Temp. I %d", &set);
    }
    else if (num == 2)
    {
        iret = sscanf(answer, "Temp. D %d", &set);
    }
    else if (num == 3)
    {
        iret = sscanf(answer, "max PWM %d", &set);
    }
    else if (num == 5)
    {
        iret = sscanf(answer, "Temp. max positive PWM %d", &set);
    }
    else if (num == 6)
    {
        iret = sscanf(answer, "Temp. max negative PWM %d", &set);
    }
    else if (num == 4)
    {
        iret = sscanf(answer, "Temp. PID %s", ison);
        set = (strstr(ison, "On")) ? 1 : 0;
    }

    if (iret == 0)
    {
        return -5;
    }

    return set;
}


// z 0 to 100
int _set_light(float z)
{
    char command[128], answer[128];
    int  set = -1, ret, dac = 0;
    unsigned long len = 0;
    unsigned long dwErrors;

    if (z < 0)
    {
        dac = 0;
    }
    else if (z >= 100)
    {
        dac = 4095;
    }
    else
    {
        dac = (int)(40.96 * z);
    }

    if (dac > 4095)
    {
        dac = 4095;
    }

    snprintf(command, 128, "light=%d\r", dac);
    /* Write_serial_port(hCom,command,strlen(command));
    for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
      ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
      answer[len] = 0;*/

    ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);

    if (ret)
    {
        return (ret - 5);
    }

    if (sscanf(answer, "Dac12 set to %d", &set) == 1)
    {
        if (set != dac)
        {
            return -1;
        }
        else
        {
            return 0;
        }
    }
    else
    {
        return -1;
    }
}

// return from 0 to 100
float _get_light(void)
{
    char command[128], answer[128];
    int  set = 0, ret;
    unsigned long len = 0;
    unsigned long dwErrors;
    snprintf(command, 128, "light?\r");
    /*Write_serial_port(hCom,command,strlen(command));
    for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
      ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
      answer[len] = 0; */

    ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);

    if (ret)
    {
        return (ret - 5);
    }

    if (sscanf(answer, "Dac12 is at %d", &set) != 1)
    {
        return -1;
    }

    return ((float)set) / 40.96;
}

/*
float _get_I_peltier()
{
  char command[128], answer[128];
  int  set = 0, ret;
  unsigned long len = 0;
  unsigned long dwErrors;

  snprintf(command,128,"A6?\r");
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret) return (ret - 5);
  if (sscanf(answer,"I sense %d",&set) != 1)    return -1;
  return ((float)set)/40.96;
}
*/


MENU *pico_temperature_plot_menu(void)
{
    static MENU mn[32];

    if (mn[0].text != NULL)
    {
        return mn;
    }

    //	add_item_to_menu(mn,"Read Objective position", pico_read_Z,NULL,0,NULL);
    //add_item_to_menu(mn,"Set Objective position", pico_set_Z,NULL,0,NULL);
    return mn;
}

int	pico_temperature_main(int argc, char **argv)
{
  (void)argc;
  (void)argv;  
    pico_serial_main(0, NULL);

    if (init_pico_serial())
    {
        win_printf_OK("Serial port initialisation failed !");
        return D_O_K;
    }

    add_plot_treat_menu_item("pico_temperature", NULL, pico_temperature_plot_menu(), 0, NULL);
    add_image_treat_menu_item("pico_temperature", NULL, pico_temperature_plot_menu(), 0, NULL);
    return D_O_K;
}

int	pico_temperature_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;
    remove_item_to_menu(plot_treat_menu, "pico_temperature", NULL, NULL);
    return D_O_K;
}
#endif
