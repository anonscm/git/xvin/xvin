/*
*    Plug-in program for image treatement in Xvin.
 *
 *    V. Croquette
 *      JF Allemand
  */
#ifndef _IFC_SOURCE_C_
#define _IFC_SOURCE_C_


# include "allegro.h"
# include "winalleg.h"
# include "ifcapi.h"
# include <windowsx.h>
# include "ctype.h"
# include "xvin.h"


# define BUILDING_PLUGINS_DLL
#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "trackBead.h"
# include "ifc_source.h"

float IFC_delay = 0.0001;

# define THIS_IMAGE_HAS_NOT_ARRIVED_YET(imp)  ((*stat_nf-1) < (imp)) ? 1 : 0  




////float read_Z_value(void)
//{
//  return 0.0;
//}
//int set_Z_value(float z)
//{
//  return 0;
//}

int prepare_image_overlay(O_i* oi)
{
  BITMAP *imb = NULL;
  if ((oi->bmp.to == IS_BITMAP) &&  (oi->bmp.stuff != NULL))
    {
      imb = (BITMAP*)oi->bmp.stuff;
      clear_to_color(imb, makecol(255,0,255)); // set the magic overlay color
    }
  return 0;
}

int kill_source(void)
{
  imreg *imr;
  if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
    {
      active_menu->flags |= D_DISABLED;
      return D_O_K;	
    }
  //else if (go_track == TRACK_ON) active_menu->flags |=  D_DISABLED;
  else active_menu->flags &= ~D_DISABLED;
  if(updating_menu_state != 0)	return D_O_K;
  go_track = TRACK_STOP;
  return D_O_K;
}


long long MyAcquisitionPeriod(void)
{
    register int i;
    long long t0;

    i = stats.CurrentFrameSeqNum  + 1;         // the next frame
    while(THIS_IMAGE_HAS_NOT_ARRIVED_YET(i));  
    t0 = my_ulclock();
    i += 10;
    while(THIS_IMAGE_HAS_NOT_ARRIVED_YET(i));  
    t0 = my_ulclock() - t0;
    return t0/10; 
}



int open_close_P2V_trigger(void)
{	int i;

	if(updating_menu_state != 0)	return D_O_K;
	/*check camlink is on*/
	if (get_pci_slot_for_IFCboard[P2V_BOARD] == -1)
    {	return win_printf("P2V board is not active!");
    }
    
	i = RETRIEVE_MENU_INDEX ;
	//win_printf("i=%d \n(WORD)i %d",i,(WORD)i);
	CICapMod_OutportVal(capmod,(WORD)i,0); 
	return 0;
}


char	*do_load_cam_file(void)
{
	register int i = 0;
	char path[512], file[256];
	static char fullfile[512], *fu = NULL;
	int full=1;

	if (fu == NULL)
	  {
	    fu = (char*)get_config_string("CAM-FILE","last_loaded",NULL);
	    if (fu != NULL)	
	      {
		strcpy(fullfile,fu);
		fu = fullfile;
	      }
	    else
	      {
		my_getcwd(fullfile, 512);
		strcat(fullfile,"\\");
	      }
	  }
#ifdef XV_WIN32
 	if (full) // full_screen
	  {
#endif
	    switch_allegro_font(1);
	    i = file_select_ex("Load camer (*.txt)", fullfile, "txt", 512, 0, 0);
	    switch_allegro_font(0);
#ifdef XV_WIN32
	  }
	else
	  {
	    extract_file_path(path, 512, fullfile);
	    put_backslash(path);
	    if (extract_file_name(file, 256, fullfile) == NULL)
	      fullfile[0] = file[0] = 0;
	    else strcpy(fullfile,file);
	    fu = DoFileOpen("Load cam file (*.txt)", path, fullfile, 512, "Cam Files (*.txt)\0*.txt\0All Files (*.*)\0*.*\0\0", "txt");
	    i = (fu == NULL) ? 0 : 1;
	    win_printf("loading %d\n%s",backslash_to_slash(fullfile));
	    slash_to_backslash(fullfile);
	  }
#endif
	if (i != 0) 	
	{
		fu = fullfile;
		set_config_string("CAM-FILE","last_loaded",fu);
		return fullfile;;
	}
	return NULL;
}

int delete_IFC_stuff(void)
{
  if(updating_menu_state != 0)	return D_O_K;
  IFC_IfxDeleteImgConn(pImg_Conn);
  IFC_IfxDeleteCaptureModule(capmod);
  return 0;
}

int retrieve_IFC_image_index(imreg *imr)
{
  register int i;
  
  if (imr == NULL)   imr = imr_TRACK;
  if (imr == NULL || oi_TRACK == NULL)  return -1;
  for (i = 0; i < imr->n_oi; i++)
    if (imr->o_i[i] == oi_TRACK) return i;
  return -1;
}

/*     This function resize the oi_IFC image to the the camera size, this is needed when
 *     whe change camera or select anothe video mode
 */

int resize_image(int i, imreg *imr, unsigned char **buf)
{	
  int k, j, ny, num_image_IFC;
  O_i *oi;
	
  num_image_IFC = retrieve_IFC_image_index(imr);
  if (num_image_IFC != imr->cur_oi)	select_image_of_imreg_and_display(imr,num_image_IFC);
  
  oi = imr->one_i;
  //nf = 
  imr->one_i->im.n_f = nframes_in_buffer;
  num_image_IFC = imr->cur_oi;
  //set_oi_source(imr->one_i, "IFC aquisition frame")
 	
  if (attr.dwBytesPerPixel == 1)
    alloc_one_image (imr->one_i, attr.dwWidth, attr.dwHeight, IS_CHAR_IMAGE);
  else if (attr.dwBytesPerPixel == 2)
    alloc_one_image (imr->one_i, attr.dwWidth, attr.dwHeight, IS_UINT_IMAGE);
  else if (attr.dwBytesPerPixel == 3)
    alloc_one_image (imr->one_i, attr.dwWidth, attr.dwHeight, IS_RGB_PICTURE);
  ny = attr.dwHeight;
  for (k = 0; k < nframes_in_buffer; k++)
    {
      switch_frame(imr->one_i,k);
      for (j = 0, *buf = imr->one_i->im.mem[k]; j < attr.dwHeight; j++)
	imr->one_i->im.pixel[attr.dwHeight -1 -j].ch = *buf + j * attr.dwWidth*attr.dwBytesPerPixel;
    }
  set_oi_source(imr->one_i, "IFC");
  if ((attr.dwWidth <1024) && (attr.dwHeight<1024))
    {
      imr->one_i->width  = ((float)attr.dwWidth)/512;
      imr->one_i->height = ((float)attr.dwHeight)/512;
    }
  else
    {
      imr->one_i->width  = ((float)attr.dwWidth)/1024;
      imr->one_i->height = ((float)attr.dwHeight)/1024;
    }
  broadcast_dialog_message(MSG_DRAW,0);
  imr_TRACK = imr;
  oi_TRACK = imr->one_i;
  d_TRACK = find_dialog_associated_to_imr(imr_TRACK, NULL);
  return 0;
}

BOOL what_to_do_before_display(void *parameter)
{	
  int nf;
  long long t;
  static unsigned long dt, dt_1 = 0;
  struct before_image_display *p;
  int bufn;
  /*static int i = 0;
  static unsigned long acqtime = 0 , start = 0;*/

  if (imr_TRACK == NULL || oi_TRACK == NULL)	return FALSE;
  /////////////////////////JF MODIF
   //next buffer to display
  CICamera_GetGrabStats(cam,grabID,&stats);
  bufn = (stats.CurrentFrameSeqNum -1/*- init*/) % nframes_in_buffer;
//  //draw_bubble(screen,0,550,100,"bufn %f",bufn);

  CImgSrc_SetBufferAddr(CImgConn_GetSrc(pImg_Conn),(BYTE *)oi_TRACK->im.mem[bufn]);
// 
 return TRUE;
  //////////////////////////////////
  CICamera_GetGrabStats(cam,grabID,&stats);
  nf = stats.CurrentFrameSeqNum;
  stat_nf = (int*)&(stats.CurrentFrameSeqNum);
  if (parameter == NULL)	return TRUE;
  t /*= start*/ = my_ulclock();
  dt = my_uclock();
  p = (struct before_TRACK_display*)parameter;
  if (p->to_do == NULL) return TRUE;
  for (p->to_do(imr_TRACK, oi_TRACK, nf, d_TRACK, t ,dt_1 ,p->param) ; p->to_do != NULL && p->next != NULL; p = p->next)
    p->to_do(imr_TRACK, oi_TRACK,nf, d_TRACK, t, dt_1, p->param);
  dt_1 = my_uclock() - dt;
    
  if ( nf%2 == 0 ) return TRUE; // why that ?
  return TRUE;
}

int get_acq_time(void)
{
    if(updating_menu_state != 0)	return D_O_K;
	bid.to_do = determine_time_between_frames;
	bid.param = NULL;
	return 0;
}


int determine_time_between_frames(imreg *imr, O_i *oi, int n,  DIALOG *d, long long t, unsigned long dt, void *p) 
{
  //unsigned long acq_time = 0, previous_time = 0;
  static int i = 0;
  int numb_images = 125;
  static double IFC_acq_time =0, IFC_acq_timed = 0, IFC_previous_time =0, IFC_dt = 0; //  acq_timed = 0,
  
  IFC_dt = CIFCOS_GetSystimeMicrosecs();
  //acq_timed += (i == 0) ? 0 : (double) (dt - previous_time);
  //acq_time += (i == 0) ? 0 :  (dt - previous_time);
  IFC_acq_time += (i == 0) ? 0 : (IFC_dt - IFC_previous_time);
  
  //previous_time = dt;
  IFC_previous_time = IFC_dt;
  draw_bubble(screen,0,550,100,"IFCtime %g",IFC_acq_time/1000);// acqtimed %g time %ul n = %d", acq_timed,acq_time,i);
  
  i++;
  
  if (i == numb_images+1)
    {
      IFC_acq_timed = IFC_acq_time / (double)(numb_images);//(numb_images == 0) ? 1 : numb_images;
      aquisition_period = (int)IFC_acq_time; 
      draw_bubble(screen,0,550,50,"IFC acqtimed %g ",IFC_acq_timed/*(double)MY_UCLOCKS_PER_SEC 1000*/);
      if (create_and_attach_unit_set_to_oi (oi,IS_SECOND, 0, (float)IFC_acq_timed, -3, 0, "ms", 
					    IS_T_UNIT_SET) == NULL) 
	win_printf("Failed");
      set_oi_t_unit_set(oi, oi->n_tu-1);
      bid.to_do = NULL; 
      i = 0;
      
    }
  return 0;
}
void CALLBACK get_frame_number(HWND hwnd, UINT uMsg, UINT_PTR idEvent,  DWORD dwTime)
{
    CICamera_GetGrabStats(cam,grabID,&stats);
    //    tim_im[n_tim%4096] = stats.CurrentFrameSeqNum;
    //n_tim++;
   return;
}


int IFC_live(void)
{
    if(updating_menu_state != 0)	return D_O_K;

    is_live  = 1;
    return live_video(-1);	
}

int set_all_IFC_for_aquisition(int boardtype, imreg *imr, HWND hWnd)
{
	pCITIMods itimod;
	ITI_PARENT_MOD mod;
	//char cam_file[256];	
	int i, j;
	float scx, scy;
	unsigned char * buf;
	BOOL what_to_do_before_display(void *received_frame_numb);
	//BITMAP *imb,*ds_bitmap;
	DIALOG *d;
	//HDC dc;
	//pCOverlay pC_Overlay = NULL;
	DWORD ColorKeyRep;

	
	j = get_pci_slot_for_IFCboard[boardtype];
	//win_printf("Board type %d slot %d LNK %d",boardtype,j,get_pci_slot_for_IFCboard[P2V_BOARD]);
	
	if (j > number_of_board_detected) 
 		return win_printf_OK("You ask too much young Jedi!");

	d = find_dialog_associated_to_imr(imr, NULL);
	itimod = IFC_IfxCreateITIMods();
	for (i = 0; i <= j; i++)
	{
	  if (i==0) CITIMods_GetFirst(itimod,&mod);
	  else CITIMods_GetNext(itimod,&mod);
    	
 	}
 	
	//win_printf("Bard type %d slot %d name %s",boardtype,j,mod.name);
	//sprintf(cam_file,"c:\\Program Files\\Xvin\\bin\\%scam.txt",mod.name);	
	if (!(capmod = IFC_IfxCreateCaptureModule(mod.name,0,do_load_cam_file()))) //prevoir 2 cartes identiques
   	{
		win_printf("No Image Capture Module detected");
		exit(0);
	}
 	cam = CICapMod_GetCam(capmod,0);
	if 	(cam == NULL) win_printf("Bad camera!");
   	CICamera_GetAttr(cam,&attr,TRUE);
   	resize_image(i,imr,&buf);
   	imr->one_i->filename = "IFC";
   	imr->one_i->need_to_refresh |= ALL_NEED_REFRESH;

   	pImg_Conn = IFC_IfxCreateImgConn_HostBuf(buf, attr.dwWidth, attr.dwHeight, 
						 (attr.dwBytesPerPixel == 2)?10:8, hWnd, 
						 IFC_MONO, IFC_LIVE_IMAGE|IFC_YCRCB_SINK/*|IFC_HW_OVERLAY*/ 
						 ,cam, ICAP_INTR_EOF,1,
						 what_to_do_before_display,(void *) &bid);
	if 	(pImg_Conn == NULL) win_printf("Bad connection!");
   	pImg_Sink = CImgConn_GetSink(pImg_Conn);	
	if 	(pImg_Sink == NULL) win_printf("Bad Sink!");
	ColorKeyRep = CImgSink_GetColorKeyRep(pImg_Sink, 0x00FF00FF) ;
	
	if ( CImgSink_SetDestColorKey(pImg_Sink,ColorKeyRep) == FALSE) win_printf("Set color failed");
	scx = (float)(get_oi_horizontal_extend(imr->one_i)*512)/attr.dwWidth;
	scy = (float)(get_oi_vertical_extend(imr->one_i)*512)/attr.dwHeight;
	CImgSink_SetAoiPos(pImg_Sink,imr->x_off + d->x , imr->y_off - scy*attr.dwHeight+d->y);

	CImgSink_SetZoom(pImg_Sink,scx,scy);
	previous_board = boardtype;    
	set_config_int("IFC","last_board",boardtype);
	//win_scanf("prev set to %d",&boardtype);
	live_video(boardtype);
	//	SetTimer( win_get_window() , 25 , 15 , get_frame_number );/*sets a background saving*/
	return 0;
}   



int freeze_video(void)
{
	CICamera_Freeze(cam);  
	return 0;
}
int freeze_source(void)
{
	imreg *imr;
	
	if(updating_menu_state != 0)	return D_O_K;
	is_live = 0;
	CICamera_Freeze(cam);  
	imr = find_imr_in_current_dialog(NULL);  
	imr->one_i->need_to_refresh &= BITMAP_NEED_REFRESH;  
	find_zmin_zmax(imr->one_i);
	refresh_image(imr, UNCHANGED);
	return 0;

}
int live_video(int mode)
{
  HWND hWnd;
  imreg *imr;
  O_i *oi;
  unsigned char *buf;
  int i;
  DIALOG *d;
  BITMAP *imb;
  unsigned long dt = 0;

#ifdef MODIF        
    int seq_num = 0;
    DWORD acquiredDy;
    double t = 0;	
# endif
	
    if(updating_menu_state != 0)	return D_O_K;
	
    hWnd = win_get_window();
    imr = find_imr_in_current_dialog(NULL);
    if (imr == NULL)	return 1;
    d = find_dialog_associated_to_imr(imr, NULL);

    if (mode != 0)
      {	
	if (mode < 0) i = RETRIEVE_MENU_INDEX ;// board is defined by menu
	else i = mode; 	
	if ((previous_board != i)||(pImg_Conn == NULL))
	  {
	    win_printf("prev %d asked %d",previous_board,i);
	    delete_IFC_stuff();
	    set_all_IFC_for_aquisition(i,imr,hWnd);
	  }
      }
    if (imr->one_i != oi_TRACK)
      {
	for(i = 0; i < imr->n_oi; i++)
	  if (imr->o_i[i] == oi_TRACK)
	    {
	      select_image_of_imreg_and_display(imr,i);
	      break;
	    }
	if (i == imr->n_oi) 
	  return win_printf_OK("No IFC image found");
      }
    oi = imr->one_i ;
    oi->filename = "IFC";
    buf =  oi->im.mem[0]; 
    
    if ((imr->one_i->bmp.to == IS_BITMAP) &&  (imr->one_i->bmp.stuff != NULL))
      {
	imb = (BITMAP*)imr->one_i->bmp.stuff;
	if (is_video_bitmap(imb) != TRUE)
	  {
	    imb = create_video_bitmap(imb->w,imb->h);
	    if (imb != NULL)
	      {
		destroy_bitmap((BITMAP*)imr->one_i->bmp.stuff);
		imr->one_i->bmp.stuff = (void*)imb;
	      }
	  }
	imr->one_i->need_to_refresh &= ~BITMAP_NEED_REFRESH;
	dt = my_uclock();
	clear_to_color(imb, makecol(255,0,255));
	blit(imb,screen,0,0,imr->x_off + d->x, imr->y_off - 
	     imb->h + d->y, imb->w, imb->h); 
	dt = my_uclock() -dt;
	//destroy_bitmap(imb);

      }  
    else   win_printf("bitmap not allocated yet");

	broadcast_dialog_message(MSG_DRAW,0);   	
	draw_bubble(screen,0,550,75,"t  %g ",((double)1000000*dt)/get_my_uclocks_per_sec());
	grabID = CICamera_Grab_HostBufEx(cam,0,buf,nframes_in_buffer,IFC_INFINITE_FRAMES, 0, 0, attr.dwWidth, attr.dwHeight);
	if (grabID == NULL) win_printf("GrabID failed");
	CICamera_GetGrabStats(cam,grabID,&stats);
	stat_nf = (int*)&(stats.CurrentFrameSeqNum);
	bid.first_im = bid.previous_fr_nb = stats.CurrentFrameSeqNum ;
	return 0;
}

int live_source(void)
{
    if(updating_menu_state != 0)	return D_O_K;

    is_live  = 1;
    return live_video(-1);	
}

int image_ifc_got_mouse(imreg *imr, int xm_s, int ym_s, int mode)
{
	return live_video(0);
}
int image_ifc_lost_mouse(imreg *imr, int xm_s, int ym_s, int mode)
{
	return freeze_video();
}  


int build_list_board(int i,char *module_name)
{
  
  if (strncmp(module_name,"P2V",3) == 0)   		
    {
      board_list[i]  = P2V_BOARD;
      get_pci_slot_for_IFCboard[P2V_BOARD] = i;
      //win_printf("Board P2V name number %d = %s\n ",i,module_name);
    }
  else if (strncmp(module_name,"PCD",3) == 0)	
    {
      board_list[i]  = PCD_BOARD;
      get_pci_slot_for_IFCboard[PCD_BOARD] = i;
      //win_printf("Board PCD name number %d = %s\n ",i,module_name);
    }
  else if (strncmp(module_name,"ICP",3) == 0)	
    {
      board_list[i]  = ICP_BOARD;
      get_pci_slot_for_IFCboard[ICP_BOARD] = i;
      //win_printf("Board ICP name number %d = %s\n ",i,module_name);
    }
  else if (strncmp(module_name,"LNK",3) == 0)	
    {
      board_list[i]  = PCLink_BOARD;
      get_pci_slot_for_IFCboard[PCLink_BOARD] = i;
      //win_printf("Board LNK name number %d = %s\n ",i,module_name);
    }
  else win_printf("Unknown board!");
  return 0;
}

int list_number_of_board_detected(void)
{
  pCITIMods itimod;
  ITI_PARENT_MOD mod[6];
  int num_board_detected = 0 ;
  int i, j;
  
  if(updating_menu_state != 0)	return D_O_K;
  
  itimod = IFC_IfxCreateITIMods();
  for (i = 0; i < 6 ; i++)
    {
      j = (i == 0) ? CITIMods_GetFirst(itimod,&mod[0]) : CITIMods_GetNext(itimod,&mod[i]); 
      if (j)
	{
	  //win_printf("Board name number %d = %s\n ",i,mod[i].name);
	  build_list_board(i,mod[i].name);
	  num_board_detected = i+1 ;
	}
    }	
  if (num_board_detected == 0 ) 
    {
      win_printf("No board detected, we leave the program");
      exit(0);
    }
  //win_printf("Board LNK name number %d \n ",get_pci_slot_for_IFCboard[PCLink_BOARD]);
  IFC_IfxDeleteITIMods(itimod);
  return num_board_detected;
}


int change_IFC_delay(void)
{
  int i;
  float dt = IFC_delay;

  if(updating_menu_state != 0)	return D_O_K;
  dt *= 1000000;
  i = win_scanf("delay in micro sec %f",&dt);
  if (i == CANCEL)  return D_O_K;
  dt /= 1000000;
  IFC_delay = dt;
  return D_O_K;
}

int IFC_lost_fr(void)
{
  if(updating_menu_state != 0)	return D_O_K;
  win_printf("lost frames %d",bid.lost_fr);
  return D_O_K;
}
int say_image_nb(void)
{
  if(updating_menu_state != 0)	return D_O_K;
  win_printf("frames %d\nn %d \nlost %d\nmissed",
	     (int)stats.CurrentFrameSeqNum,*stat_nf,
	     (int)stats.NumberOfFramesLost,(int)stats.NumberOfFramesMissed);
  return D_O_K;
}


MENU *track_ifc_image_menu(void)
{
  static MENU mn[32];
  int follow_bead_in_x_y(void);
  int draw_bead_in_x_y(void);
  int get_tracking_points(void);
  int aquire_and_save_data_in_real_time(void);
  int test_background_saving(void);
  int start_save_acq_ds(void);
  int plot_acqplt(void);
  int do_load_track_data(void);
  int get_acq_time(void);
  int change_nb_images_in_buffer(void);
  int save_tape_mode(void);
  int play_movie_real_time(void);
  int set_background_transfer_image_data(void);
    int get_movie(void);
  int get_small_movie(void);
  int average_real_time(void);
  int start_servo(void);
  int get_tracking_profile(void);

  if (mn[0].text != NULL)	return mn;
  //add_item_to_menu(mn,"Servo Obj.", start_servo,NULL,0,NULL);
    // debug : display bead position
    //add_item_to_menu(mn,"display x y", draw_bead_in_x_y,NULL,0,NULL);
    //old stuff to retrieve data from rolling buffer
    //add_item_to_menu(mn,"get previous points of tracking",get_tracking_points,NULL,0,NULL);
    //    add_item_to_menu(mn,"Save data",start_save_acq_ds,NULL,0,NULL);
    //add_item_to_menu(mn,"get_acq_time",get_acq_time,NULL,0,NULL);
    //add_item_to_menu(mn,"Kill timer",kill_timer1,NULL,0,NULL);
    //add_item_to_menu(mn,"Set delay",change_IFC_delay,NULL,0,NULL);
    //add_item_to_menu(mn,"lost frames",IFC_lost_fr,NULL,0,NULL);
    //add_item_to_menu(mn,"say frame nb.",say_image_nb,NULL,0,NULL);

  //add_item_to_menu(mn,"Read acq data",read_acq_file,NULL,0,NULL);
    //add_item_to_menu(mn,"plot_acqplt",plot_acqplt,NULL,0,NULL);
    //  add_item_to_menu(mn,"Load Track",do_load_track_data,NULL,0,NULL);
    // add_item_to_menu(mn,"Adjust buffer", change_nb_images_in_buffer, NULL, 0, NULL);
    // add_item_to_menu(mn,"Save video tape", save_tape_mode, NULL, 0, NULL);
    //add_item_to_menu(mn,"Play video tape", play_movie_real_time, NULL, 0, NULL);
    //add_item_to_menu(mn,"Background task",set_background_transfer_image_data, NULL, 0, NULL);
    //add_item_to_menu(mn,"Get movie",get_movie, NULL, 0, NULL);
    //add_item_to_menu(mn,"Get SMALLmovie",get_small_movie, NULL, 0, NULL);
    //add_item_to_menu(mn,"Average_real_time",average_real_time, NULL, 0, NULL);
  
  add_item_to_menu(mn,"Detect Board",  list_number_of_board_detected, NULL ,  0, NULL);
  //add_item_to_menu(mn,"Freeze",	freeze_video_menu, NULL ,  0, NULL );
  //add_item_to_menu(mn,"grab profile",get_tracking_profile, NULL ,  0, NULL );

  //	add_item_to_menu(mn,"idle stuff",	active_idle_plot, NULL ,  0, NULL );
  if (get_pci_slot_for_IFCboard[P2V_BOARD] != -1) 	
    add_item_to_menu(mn,"Live P2V",	IFC_live, NULL ,  MENU_INDEX(P2V_BOARD), NULL );
  if (get_pci_slot_for_IFCboard[PCD_BOARD] != -1)	
    add_item_to_menu(mn,"Live PCD_BOARD",IFC_live, NULL ,  MENU_INDEX(PCD_BOARD), NULL );
  if (get_pci_slot_for_IFCboard[PCLink_BOARD] != -1)	
    add_item_to_menu(mn,"Live PCLink",IFC_live, NULL ,  MENU_INDEX(PCLink_BOARD), NULL );
  if (get_pci_slot_for_IFCboard[ICP_BOARD] != -1)	
    add_item_to_menu(mn,"Live ICP",IFC_live, NULL ,  MENU_INDEX(ICP_BOARD), NULL );


    
  if (get_pci_slot_for_IFCboard[P2V_BOARD] != -1)
    {	
    	add_item_to_menu(mn,"OPEN shutter",open_close_P2V_trigger, NULL ,  MENU_INDEX(OPEN), NULL );
    	add_item_to_menu(mn,"CLOSE shutter",open_close_P2V_trigger, NULL ,  MENU_INDEX(CLOSE), NULL );
    }
  
  return mn;
}


VOID CALLBACK TimerAPCProc(LPVOID lpArgToCompletionRoutine,
  DWORD dwTimerLowValue,  DWORD dwTimerHighValue)
{
  long long t;
  static unsigned long dt, dt_1 = 0, dt2;
  int im_n, bufn, i;
  tid *ltid;
  imreg *imr;
  DIALOG *d;
  O_i *oi;
  Bid *p;


  ltid = (tid*)lpArgToCompletionRoutine;

  imr = ltid->imr;
  d = ltid->dimr;
  oi = ltid->oi;
  p = ltid->dbid;



  dt = my_uclock();
  im_n = p->previous_fr_nb + 1; // the next frame
  if (THIS_IMAGE_HAS_NOT_ARRIVED_YET(im_n))  // we are in advance
    {
      while(THIS_IMAGE_HAS_NOT_ARRIVED_YET(im_n));
      p->previous_in_time_fr = t = my_ulclock();
      dt2 = my_uclock() - dt;
      p->previous_in_time_fr_nb = im_n;
    }
  else   
    {
      dt2 = 0;
      //CICamera_GetGrabStats(cam,grabID,&stats);
      //p->lost_fr += stats.CurrentFrameSeqNum  - p->previous_fr_nb; 
    }
  p->timer_dt = dt2;
  p->previous_fr_nb = im_n;


  //CICamera_GetGrabStats(cam,grabID,&stats);
  //bufn  = p->first_im + stats.CurrentFrameSeqNum ; // = p->previous_fr_nb
  //bufn %= nframes_in_buffer;
  // next buffer to display
//  CImgSrc_SetBufferAddr(CImgConn_GetSrc(pImg_Conn),(BYTE *)oi->im.mem[bufn]);



  if (p->timer_do == NULL) return;
  for (i = 0 ;im_n  < *stat_nf ; im_n++, i++)
    {
      t = my_ulclock();
      dt = my_uclock();
      ////////////////JF MODIF
      //switch_frame(oi,(p->first_im+im_n)%nframes_in_buffer); pas de sens avec IFC
      //oi->need_to_refresh &= ~BITMAP_NEED_REFRESH;
      //////////////////////////////////////////////////
      p->previous_fr_nb = im_n;      
      for (p->timer_do(imr, oi, im_n, d, t ,dt ,p->param, i) ; 
	   p->timer_do != NULL && p->next != NULL; p = p->next)
	p->timer_do(imr, oi,im_n, d, t, dt, p->param, i);
    }
  //display_title_message("nb process %d",i); 
  dt_1 = my_uclock() - dt;
  return;
}
# define AcquisitionPeriodInHundredsOfNanoSeconds   \
(int)((((double)AcquisitionPeriod())*10000000)/get_my_ulclocks_per_sec())



DWORD WINAPI TrackingThreadProc(LPVOID lpParam) 
{
    HANDLE hTimer = NULL;
    LARGE_INTEGER liDueTime;
    imreg *imr;
    //O_i *oi;
    int j, im_n, nf; //,n;
    long long t;
    long long AcqPer;
    int AcqPerNano;
    //HWND hWnd;
    tid *ltid;
    DIALOG *d;
    O_i *oi;
    //unsigned long dt = 0, t0 = 0, t1 = 0, dtmax = 0;
    Bid *p;


    if (imr_TRACK == NULL || oi_TRACK == NULL)	return 1;
    liDueTime.QuadPart=-300000; // 30 ms
    ltid = (tid*)lpParam;

    imr = ltid->imr;
    d = ltid->dimr;
    oi = ltid->oi;
    p = ltid->dbid;
    nf = abs(oi->im.n_f);		


    AcqPer = MyAcquisitionPeriod();
    AcqPerNano = (int)((((double)AcqPer)*10000000)/get_my_ulclocks_per_sec());
    //win_printf("Period in nanosec %d",AcqPerNano);
    //Sleep(1000);
    hTimer = CreateWaitableTimer(NULL, TRUE, "WaitableTimer");
    if (!hTimer)   win_printf_OK("CreateWaitableTimer failed (%d)\n", GetLastError());

    CICamera_GetGrabStats(cam,grabID,&stats);
    stat_nf = (int*)&(stats.CurrentFrameSeqNum);
    /////JF*****************************///////////
    p->first_im = p->previous_fr_nb = stats.CurrentFrameSeqNum - 1;
    im_n = p->previous_fr_nb + 1;
    while(THIS_IMAGE_HAS_NOT_ARRIVED_YET(im_n));
    p->previous_in_time_fr = my_ulclock();
    p->previous_in_time_fr_nb = im_n;
    //display_title_message("entering the infinite loop "); 
    while (go_track)
      {
	//CICamera_GetGrabStats(cam,grabID,&stats);
	im_n = p->previous_fr_nb + 1;
	if (*stat_nf > im_n)  // the image has arrived
	  {
	    t = 500;
	    j = 0;
	  }
	else
	  {
	    j = im_n - p->previous_in_time_fr_nb;
	    t = j*AcqPer;
	    t -= (my_ulclock() - p->previous_in_time_fr);
	    t = (long long)((((double)t)*10000000)/get_my_ulclocks_per_sec());
	    t -= 10000; // we aim 1000 micro second before image flip
	    t = (t < 0) ? 1000 : t;
	    t = (t > 150000) ? 150000 : t;
	  }
	//display_title_message("                            "); 
	
	//display_title_message("wait j %d %d p %f",j,(int)t,(float)(AcqPer*1000)/get_my_ulclocks_per_sec()); 
	//n_tim++;
	liDueTime.QuadPart= -t; // 30 ms
	//liDueTime.QuadPart= -30000; // 5 ms
	// Set a timer to wait for 10 seconds.
	if (!SetWaitableTimer(hTimer, &liDueTime, 0, TimerAPCProc, lpParam, 0))
	  win_printf_OK("SetWaitableTimer failed (%d)\n", GetLastError());

	// Wait for the timer.
	SleepEx (INFINITE, TRUE);
      }
      //if (WaitForSingleObject(hTimer, INFINITE) != WAIT_OBJECT_0)
      // win_printf_OK("WaitForSingleObject failed (%d)\n", GetLastError());
      //else 
      win_printf_OK("Timer was signaled.\n");
      return 0;
}

int init_image_source(void)
{
  imreg *imr; 
  HWND hWnd;
  int n;

  imr = create_and_register_new_image_project( 0,   32,  900,  668);
  if (imr == NULL) return win_printf_OK("Patate t'es mal barre!");
  add_image_treat_menu_item ( "IFC", NULL, track_ifc_image_menu(), 0, NULL);
  hWnd = win_get_window(); // we grab WINDOWS context
  number_of_board_detected = list_number_of_board_detected();
  n = get_config_int("IFC","last_board",P2V_BOARD);
  set_all_IFC_for_aquisition(n,imr,hWnd);
  remove_from_image (imr, imr->o_i[0]->im.data_type, (void *)imr->o_i[0]);
  imr_TRACK = NULL;
  return 0;
}
int start_data_movie(imreg *imr)
{




  return 0;
}


# endif
