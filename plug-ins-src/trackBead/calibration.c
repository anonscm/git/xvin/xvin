/*
 * @addtogroup trackBead
 * @{
 */

#ifndef _CALIBRATION_C_
#define _CALIBRATION_C_

# include "allegro.h"
#ifdef XV_WIN32
# include "winalleg.h"
#endif

# include "xvin.h"
# include "fftl32n.h"
# include "fillib.h"





/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "../cfg_file/Pico_cfg.h"
# include "../fft2d/fft2d.h"
#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "../fft2d/fftbtl32n.h"
# include "fillibbt.h"
# include "focus.h"
# include "magnetscontrol.h"
# include "trackBead.h"
# include "brown_util.h"
# include "track_util.h"
# include "calibration.h"
# include "scan_zmag.h"
# include "action.h"
# include "record.h"

int n_calib = 0;

int ref_method = 0;

int link_calibration(void);
int remove_calibration(void);
int remove_all_calibration(void);
int set_black_circle(void);
int unset_black_circle(void);
int link_beads_to_generic_calibration(void);
int calibration_oi_post_display(O_i *oi, DIALOG *d);




MENU bead_menu[32] =
{
    { "load calibration",     link_calibration,   NULL,      0, NULL  },
    { "remove calibration",   remove_calibration,     NULL,      0, NULL  },
    { "remove all calibration",   remove_all_calibration,     NULL,      0, NULL  },
    { "Set black circle",     set_black_circle,     NULL,      0, NULL  },
    { "Unset black circle",   unset_black_circle,     NULL,      0, NULL  },
    { NULL,                    NULL,             NULL,       0, NULL  }
};

char calibration_state_disp[4096];
char *generate_calibration_title(void)
{
    return calibration_state_disp;
}


int position_cross_at_fence_limit(b_track *bt)
{
    int xci, yci, ri, dr;

    if (bt == NULL)
    {
        return 1;    // || bt->calib_im == NULL
    }

    if (bt->fence_x < 0 || bt->fence_y < 0)
    {
        return 1;
    }

    bt->fence_xi = xci = (int)(0.5 + (bt->fence_x / oi_TRACK->dx));
    bt->fence_yi = yci = (int)(0.5 + (bt->fence_y / oi_TRACK->dy));
    ri = (int)(0.5 + (track_info->bead_fence / oi_TRACK->dy));
    dr = sqrt((bt->xc - xci) * (bt->xc - xci) + (bt->yc - yci) * (bt->yc - yci));

    if (dr > ri)
    {
        //my_set_window_title("fence xci %d yci %d ri %d dr %d dx %d dy %d"
        //                    ,xci,yci,ri,dr,(int)((float)((bt->xc - xci)*ri)/dr),(int)((float)((bt->yc - yci)*ri)/dr));
        bt->mx = bt->xc = bt->x0 = xci;// + (int)((float)((bt->xc - xci)*ri)/dr);
        bt->my = bt->yc = bt->y0 = yci;// + (int)((float)((bt->yc - yci)*ri)/dr);
    }

    return 0;
}



int position_cross_according_to_calibration_image(b_track *bt)
{
    float xb, yb, zb;
    int bnx = 128, bcw = 16;

    if (bt == NULL || bt->calib_im == NULL)
    {
        return 1;
    }

    if (grab_bead_info_from_calibration(bt->calib_im, &xb, &yb, &zb, &bnx, &bcw))
    {
        return 1;
    }

    bt->mx = bt->xc = bt->x0 = (int)(0.5 + (xb /
                                            oi_TRACK->dx)); //x_imdata_2_imr_nl(imr_TRACK, (int) (0.5+(xb/oi_TRACK->dx)),d_TRACK);
    bt->my = bt->yc = bt->y0 = (int)(0.5 + (yb /
                                            oi_TRACK->dy)); //y_imdata_2_imr_nl(imr_TRACK, (int) (0.5+(yb/oi_TRACK->dy)),d_TRACK)
    bt->ncl = bnx;
    bt->ncw = bcw;

    // Add fence when resetting position
    bt->fence_xi = bt->xc;
    bt->fence_yi = bt->yc;
    bt->fence_x = xb;
    bt->fence_y = yb;


    //  win_printf("xb %g yb %g \\mu m\n pxl %d %d\ny_off %d ys %d ye %d\nsny %d yim %d"
    //     ,xb,yb,bt->xc,bt->yc,imr_TRACK->y_off,imr_TRACK->one_i->im.nys,imr_TRACK->one_i->im.nye
    //     ,imr_TRACK->s_ny,(int)(0.5+(yb/oi_TRACK->dy)));
    return 0;
}

int save_cross_position(b_track *bt)
{
    if (bt == NULL)
    {
        return 1;
    }

    bt->saved_x = bt->x[track_info->c_i];  // last_x_not_lost
    bt->saved_y = bt->y[track_info->c_i];  // last_y_not_lost
    return 0;
}

int position_cross_according_to_last_saved(b_track *bt)
{
    if (bt == NULL)
    {
        return 1;
    }

    bt->mx = bt->xc = bt->x0 = (int)bt->saved_x; //last_x_not_lost;
    bt->my = bt->yc = bt->y0 = (int)bt->saved_y; // last_y_not_lost;
    return 0;
}


int put_back_cross(void)
{
    if (mouse_selected_bead < 0 || mouse_selected_bead >= track_info->n_b)
    {
        return D_O_K;
    }

    position_cross_according_to_calibration_image(track_info->bd[mouse_selected_bead]);
    return D_O_K;
}


int put_back_all_cross(void)
{
    int i;

    if (mouse_selected_bead < 0 || mouse_selected_bead >= track_info->n_b)
    {
        return D_O_K;
    }

    for (i = 0; i < track_info->n_b; i++)
    {
        position_cross_according_to_calibration_image(track_info->bd[i]);
    }

    return D_O_K;
}


int put_back_cross_save(void)
{
    int i;
    i = immediate_next_available_action(BEAD_CROSS_RESTORE, 0);

    if (i < 0)
    {
        win_printf_OK("Could not add pending action!");
    }

    /*
       if (mouse_selected_bead < 0 || mouse_selected_bead >= track_info->n_b)    return D_O_K;
       position_cross_according_to_last_saved(track_info->bd[mouse_selected_bead]);
       */
    return D_O_K;
}

int do_save_cross_position(void)
{
    int i;
    i = immediate_next_available_action(BEAD_CROSS_SAVED, 0);

    if (i < 0)
    {
        win_printf_OK("Could not add pending action!");
    }

    /*
       if (mouse_selected_bead < 0 || mouse_selected_bead >= track_info->n_b)    return D_O_K;
       save_cross_position(track_info->bd[mouse_selected_bead]);
       */
    return D_O_K;
}

int change_cross_avg_width(void)
{
    int i, bcw;
    b_track *bt = NULL;

    if (mouse_selected_bead < 0 || mouse_selected_bead >= track_info->n_b)
    {
        return D_O_K;
    }

    bt = track_info->bd[mouse_selected_bead];
    bcw = bt->cw;
    i = win_scanf("Define cross averaging depth %6d\n", &bcw);

    if (i == WIN_CANCEL)
    {
        return D_O_K;
    }

    bt->ncw = bcw;
    return D_O_K;
}


int change_cross_avg_filter(void)
{
    int i, bcw, bp_center, bp_width, rc, max_limit, all_bd = 0, cl = 0;
    float fence;
    b_track *bt = NULL;

    if (mouse_selected_bead < 0 || mouse_selected_bead >= track_info->n_b)
    {
        return D_O_K;
    }

    bt = track_info->bd[mouse_selected_bead];
    cl = bt->cl;
    bcw = bt->dis_filter;
    bp_center = bt->bp_center;
    bp_width = bt->bp_width;
    rc = bt->rc;
    max_limit = bt->max_limit;
    fence = track_info->bead_fence;
    i = win_scanf("You can specify a fence to prevent bead to escape too far\n"
                  "Specify the fence radius %6f\\mu m (<0) -> disable fence\n"
                  "For X, Y tracking: Define cross averaging filter %6d\n"
                  "For Z tracking: Define band-pass filter center %6d\n"
                  "band-pass filter width %6d; Cut-off radius %6d\n"
                  "Search maximum limit %6d\n"
                  "%R->modify only this bead\n"
                  "%r->modify all beads having the same size\n"
                  , &fence, &bcw, &bp_center, &bp_width, &rc, &max_limit, &all_bd);

    if (i == WIN_CANCEL)
    {
        return D_O_K;
    }

    if (all_bd == 1)
    {
        for (i = 0; i < track_info->n_b; i++)
        {
            bt = track_info->bd[i];

            if (bt == NULL)
            {
                continue;
            }

            if (bt->cl != cl)
            {
                continue;
            }

            bt->dis_filter = bcw;
            bt->bp_center = bp_center;
            bt->bp_width = bp_width;
            bt->rc = rc;
            bt->max_limit = max_limit;
        }
    }
    else
    {
        bt->dis_filter = bcw;
        bt->bp_center = bp_center;
        bt->bp_width = bp_width;
        bt->rc = rc;
        bt->max_limit = max_limit;
    }

    track_info->bead_fence = fence;

    if (oi_TRACK)
    {
        track_info->bead_fence_in_pixel = (int)(0.5 + (track_info->bead_fence / oi_TRACK->dy));
    }

    return D_O_K;
}


int change_cross_size(void)
{
    int i, bcl;
    b_track *bt = NULL;

    if (mouse_selected_bead < 0 || mouse_selected_bead >= track_info->n_b)
    {
        return D_O_K;
    }

    bt = track_info->bd[mouse_selected_bead];
    bcl = bt->cl;

    if (bt->calib_im != NULL)
    {
        return win_printf_OK("Cross size %d You must first remove the \nassociated calibration image!", bt->ncl);
    }

    i = win_scanf("Define cross size %6d\n", &bcl);

    if (i == WIN_CANCEL)
    {
        return D_O_K;
    }

    bt->ncl = bcl;
    return D_O_K;
}

int change_cross_angle(void)
{
    b_track *bt = NULL;

    if (mouse_selected_bead < 0 || mouse_selected_bead >= track_info->n_b)
    {
        return D_O_K;
    }

    bt = track_info->bd[mouse_selected_bead];
    bt->cross_45 = (bt->cross_45) ? 0 : 1;
    //win_printf("cross angle %d",bt->cross_45);
    return D_O_K;
}


int set_diff_track(void)
{
    track_info->do_diff_track = (track_info->do_diff_track) ? 0 : 1;
    //win_printf("cross angle %d",bt->cross_45);
    return D_O_K;
}

typedef struct _count_data
{
    bd_sc *bdsc;
    d_s *ds;
} count_data;



int job_recount(int im, struct future_job *job)
{
    count_data *c_d  = NULL;
    im_ext_ar *iea = NULL;
    int nstep, i;
    float zmag;
    d_s *ds = NULL, *ds2 = NULL;
    (void)im;

    if (job == NULL)
    {
        return 0;
    }

    c_d = (count_data *)job->more_data;
    iea = do_auto_bead_find_from_sym_profile(oi_TRACK, c_d->ds, c_d->bdsc, NULL);

    if (iea != NULL)
    {
        track_info->last_nb_of_bd_found = iea->n_ie;
    }

    if (job->op != NULL)
    {
        nstep = job->in_progress;
        ds = job->op->dat[0];
        ds2 = job->op->dat[1];
        i = ds->nx;
        job->op->need_to_refresh = 1;

        if (i == 0)
        {
            ds->user_ispare[0] = ds->user_ispare[1] = track_info->last_nb_of_bd_found;
            set_ds_source(ds, "Count bead versus estimated force (Starting from %d beads)", track_info->last_nb_of_bd_found);
            ds->yd[0] = 0;
            ds2->yd[0] = track_info->last_nb_of_bd_found;
        }
        else if (i < nstep)
        {
            ds->yd[i] = ds->user_ispare[1] - track_info->last_nb_of_bd_found;
            ds->user_ispare[1] = track_info->last_nb_of_bd_found;
            ds2->yd[i] = track_info->last_nb_of_bd_found;
        }

        my_set_window_title("counting bead point %d # of beads %d f %g", ds->nx, iea->n_ie, ds->xd[ds->nx]);
        ds->nx++;
        i = ds->nx;

        if (i < ds->mx)
        {
            zmag = ds->yd[i];

            if (immediate_next_available_action(MV_ZMAG_ABS, zmag) < 0)
            {
                win_printf_OK("Could not add pending action!");
            }
        }

        if (i >= nstep)
        {
            job->in_progress = 0;
        }
    }

    job->imi = track_info->imi[track_info->c_i] + job->local; // next count schedule
    free_im_ext_array(iea);
    return 0;
}





int count_beads_like_this_one(void)
{
    int i, nstep = 1, xbc = -1, ybc = -1, ib;
    static b_track *bt = NULL;
    float mean = 0;
    O_p *op = NULL;
    d_s *dsd = NULL, *ds2 = NULL;
    static d_s *ds = NULL;
    static int size = 256, scanzf = 0, count_file, crosses = 1, cl = 64, cw = 12;
    static float lp = 4, lw = 2, hp = -1, hw = -1, dx = 1, fmax = 18, fstep = 1;
    static int nmax = 256, tolerence = 3, useROI = 0, nf_recount = 60, pending_recount = -1;
    int last_c_i = -1, mode = 0, imean;
    static float thres = 0.35, mindist = 32, peaksize = 6;
    static bd_sc bdsc;
    float f, tmp;
    im_ext_ar *iea = NULL;
    static count_data c_d;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    mode = RETRIEVE_MENU_INDEX;

    if (mode == 0 && mouse_selected_bead >= 0 && mouse_selected_bead < track_info->n_b)
    {
        bt = track_info->bd[mouse_selected_bead];
        xbc = bt->xc;
        ybc = bt->yc;

        if (ds != NULL)
        {
            free_data_set(ds);
        }

        ds = NULL;
        ds = build_data_set(bt->cl / 2, bt->cl / 2);
        last_c_i = track_info->c_i - 1;
        last_c_i = (last_c_i < 0) ? TRACKING_BUFFER_SIZE - 1 : last_c_i;

        for (i = 0, mean = 0, imean = 0; i < (bt->cl / 2) && i < ds->nx ; i++)
        {
            ds->xd[i] = i;
            ds->yd[i] = bt->rad_prof[last_c_i][i + bt->cl / 2];

            if (i == (bt->cl / 2) - 1)
            {
                mean += ds->yd[i];
                imean++;
            }
        }

        for (i = 0, mean = (imean > 0) ? mean / imean : mean; i < bt->cl / 2 && i < ds->nx ; i++)
        {
            ds->yd[i] -= mean;
        }
    }

    if (bt == NULL || ds == NULL)
    {
        return D_O_K;
    }

    if (mode == 0 && mouse_selected_bead >= 0 && mouse_selected_bead < track_info->n_b)
    {
        i = bt->cl / 2;

        for (size = 32; size < i; size *= 2);

        size *= 2;
        lp = get_config_float("CALIBRATION", "count_beads_bp_lp_cutoff", lp);
        lw = get_config_float("CALIBRATION", "count_beads_bp_lp_width", lw);
        hp = get_config_float("CALIBRATION", "count_beads_bp_hp_cutoff", hp);
        hw = get_config_float("CALIBRATION", "count_beads_bp_hp_width", hw);
        dx = get_config_float("CALIBRATION", "count_beads_pixel_extends", dx);
        thres = get_config_float("CALIBRATION", "count_beads_threshold", thres);
        tolerence = get_config_int("CALIBRATION", "count_beads_tolerence", tolerence);
        nmax = get_config_int("CALIBRATION", "count_beads_max_extremum_found", nmax);
        mindist = get_config_float("CALIBRATION", "count_beads_min_dist_between_beads", mindist);
        peaksize = get_config_float("CALIBRATION", "count_beads_autocorrelation_beads_peaksize", peaksize);
        crosses = get_config_int("CALIBRATION", "count_beads_do_create_crosses", crosses);
        cl = get_config_int("CALIBRATION", "count_beads_crosses_length", cl);
        cw = get_config_int("CALIBRATION", "count_beads_crosses_width", cw);
        nf_recount =  get_config_int("CALIBRATION", "count_beads_recount_rate", nf_recount);
        scanzf =  get_config_int("CALIBRATION", "count_beads_do_scan_force", scanzf);
        fmax =  get_config_float("CALIBRATION", "count_beads_max_force", fmax);
        fstep =  get_config_float("CALIBRATION", "count_beads_step_force", fstep);

        if (hp < 0)
        {
            hp = size / 4;
        }

        if (hw < 0)
        {
            hw = size / 8;
        }

        i = win_scanf("This routine will cut the ROI of an image in smaller ones with covering edge\n"
                      "and compute correlation with the image of a single bead described by its profile\n"
                      "- Define the size of the small image (a power of 2) %8d\n"
                      "- Define the Band-pass filter cutoff lowpass  %10flowpass width %10f\n"
                      "- cutoff highpass  %10fhighpass width %10f\n"
                      "- Define the extend of one spot pixel in image pixel %8f\n"
                      "looking for maximum in image exceeding a threshold\n"
                      "- Define threshold %8f the typical peak width %8d\n"
                      "and max number of extremum found %8d\n"
                      "- Define minimum distance between beads %10f\n"
                      "The peaksize of autocorrelation bead peak %6f\n"
                      "%b Create crosses for each maximum length %6d width %6d\n"
                      "Nb. of frames between to recount %8d (<0 no auto recount)\n"
                      "%b Scan force and count bead leaving\n"
                      "If scan start from present force to maximum force %8f with steps of %8f\n"
                      , &size, &lp, &lw, &hp, &hw, &dx, &thres, &tolerence, &nmax, &mindist, &peaksize, &crosses, &cl, &cw, &nf_recount,
                      &scanzf, &fmax, &fstep);

        if (i == WIN_CANCEL)
        {
            return 0;
        }

        set_config_float("CALIBRATION", "count_beads_bp_lp_cutoff", lp);
        set_config_float("CALIBRATION", "count_beads_bp_lp_width", lw);
        set_config_float("CALIBRATION", "count_beads_bp_hp_cutoff", hp);
        set_config_float("CALIBRATION", "count_beads_bp_hp_width", hw);
        set_config_float("CALIBRATION", "count_beads_pixel_extends", dx);
        set_config_float("CALIBRATION", "count_beads_threshold", thres);
        set_config_int("CALIBRATION", "count_beads_tolerence", tolerence);
        set_config_int("CALIBRATION", "count_beads_max_extremum_found", nmax);
        set_config_float("CALIBRATION", "count_beads_min_dist_between_beads", mindist);
        set_config_float("CALIBRATION", "count_beads_autocorrelation_beads_peaksize", peaksize);
        set_config_int("CALIBRATION", "count_beads_do_create_crosses", crosses);
        set_config_int("CALIBRATION", "count_beads_crosses_length", cl);
        set_config_int("CALIBRATION", "count_beads_crosses_width", cw);
        set_config_int("CALIBRATION", "count_beads_recount_rate", nf_recount);
        set_config_int("CALIBRATION", "count_beads_do_scan_force", scanzf);
        set_config_float("CALIBRATION", "count_beads_max_force", fmax);
        set_config_float("CALIBRATION", "count_beads_step_force", fstep);

        if (scanzf)
        {
            for (nstep = 0, f = convert_only_zmag_to_force(what_is_present_z_mag_value(), 0); (f <= fmax + 0.005);
                    f += fstep, nstep++);

            nstep++;

            if ((op = create_and_attach_one_plot(pr_TRACK, nstep, nstep, 0)) == NULL)
            {
                return win_printf_OK("I can't create plot !");
            }

            set_op_filename(op, "CountBead%03d.gr", count_file++);
            dsd = op->dat[0]; // the first data set was already created
            set_ds_source(dsd, "Count bead versus estimated force");

            if ((ds2 = create_and_attach_one_ds(op, nstep, nstep, 0)) == NULL)
            {
                return win_printf_OK("I can't create plot !");
            }

            set_ds_source(ds2, "Zmag versus estimated force");

            for (i = 0, f = convert_only_zmag_to_force(what_is_present_z_mag_value(), 0); i < dsd->nx
                    && (f <= fmax + 0.005); f += fstep, i++)
            {
                dsd->xd[i] = ds2->xd[i] = f;
                dsd->yd[i] = ds2->yd[i] = convert_force_to_zmag(f,  0);
                //win_printf("point i %d f = %f zmag %g",i,dsd->xd[i],dsd->yd[i]);
            }

            if (i < dsd->nx)
            {
                dsd->xd[i] = ds2->xd[i] = dsd->xd[0];
                dsd->yd[i] = ds2->yd[i] = what_is_present_z_mag_value();
                //win_printf("return point i %d f = %f zmag %g",i,dsd->xd[i],dsd->yd[i]);
            }

            set_plot_x_title(op, "Estimated force");
            set_plot_y_title(op, "Nb. of bead lost");
            dsd->nx = 0;
        }
    }

    bdsc.sim_nx = size;
    bdsc.lpr = (float)lp / size;
    bdsc.lwr = (float)lw / size;
    bdsc.hpr = (float)hp / size;
    bdsc.hwr = (float)hw / size;
    bdsc.useROI = useROI;
    bdsc.thres = thres;
    bdsc.tolerence = tolerence;
    bdsc.nmax = nmax;
    bdsc.mindist = mindist;
    bdsc.peaksize = peaksize;
    iea = do_auto_bead_find_from_sym_profile(oi_TRACK, ds, &bdsc, NULL); // imr_TRACK

    if (iea != NULL)
    {
        track_info->last_nb_of_bd_found = iea->n_ie;
    }

    convert_bead_search_info_in_plot(iea, oi_TRACK, &bdsc);

    if (crosses)
    {
        for (i = 0, ib = 0; i < (int)iea->n_ie; i++)
        {
            if (iea->ie[i].dnb > mindist)
            {
                if (ib == mouse_selected_bead)
                {
                    ib++;
                }

                tmp = (iea->ie[i].xpos - xbc) * (iea->ie[i].xpos - xbc);
                tmp += (iea->ie[i].ypos - ybc) * (iea->ie[i].ypos - ybc);

                if (tmp > 25) // we exclude the starting bead
                {
                    if (ib < track_info->n_b)
                    {
                        track_info->bd[ib]->saved_x = iea->ie[i].xpos * oi_TRACK->dx;
                        track_info->bd[ib]->saved_y = iea->ie[i].ypos * oi_TRACK->dy;
                        track_info->bd[ib]->fence_x = iea->ie[i].xpos * oi_TRACK->dx;
                        track_info->bd[ib]->fence_y = iea->ie[i].ypos * oi_TRACK->dy;
                        track_info->bd[ib]->fence_xi = (int)(0.5 + (iea->ie[i].xpos));
                        track_info->bd[ib]->fence_yi = (int)(0.5 + (iea->ie[i].ypos));
                        ib++;
                    }
                    else
                    {
#ifdef SDI_VERSION
                      do_add_bead_in_x_y(cl, cw, iea->ie[i].xpos, iea->ie[i].ypos);
#else
                        do_add_bead_in_x_y(cl, cw,
					   (calib_im_nx_equal_cross_arm_length)
					   ? cl : calib_im_nx_set,
					   dy_fringes, dy_fringes_tolerence, fringes_ny,
					   iea->ie[i].xpos, iea->ie[i].ypos,0,0,0,0,0,0,0,0,0,0,0,0);
#endif
                        track_info->bd[track_info->n_b - 1]->saved_x = iea->ie[i].xpos * oi_TRACK->dx;
                        track_info->bd[track_info->n_b - 1]->fence_x = iea->ie[i].xpos * oi_TRACK->dy;
                        track_info->bd[track_info->n_b - 1]->saved_y = iea->ie[i].ypos * oi_TRACK->dx;
                        track_info->bd[track_info->n_b - 1]->fence_y = iea->ie[i].ypos * oi_TRACK->dy;
                        track_info->bd[track_info->n_b - 1]->fence_xi = (int)(0.5 + (iea->ie[i].xpos));
                        track_info->bd[track_info->n_b - 1]->fence_yi = (int)(0.5 + (iea->ie[i].ypos));
                    }
                }
            }
        }

        put_back_cross_save();
    }

    free_im_ext_array(iea);

    if (nf_recount <= 0 && pending_recount >= 0)
    {
        // we stop recounting job
        job_pending[pending_recount].in_progress = 0;
        pending_recount = -1;
    }

    if (nf_recount > 0 && pending_recount >= 0)
    {
        // we eventually change the rate of counting
        if (job_pending[pending_recount].in_progress == 0)
        {
            // we create a new job
            c_d.bdsc = &bdsc;
            c_d.ds = ds;
            pending_recount = fill_next_available_job_spot(((nstep > 0) ? nstep : 1),
                              track_info->imi[track_info->c_i] + nf_recount,
                              RECOUNT_BEAD, mouse_selected_bead, imr_TRACK, oi_TRACK, pr_TRACK,
                              op, (void *)(&c_d), job_recount , nf_recount);
        }

        job_pending[pending_recount].local = nf_recount;
    }

    if (nf_recount > 0 && pending_recount < 0)
    {
        // we start recounting job
        c_d.bdsc = &bdsc;
        c_d.ds = ds;
        pending_recount = fill_next_available_job_spot(((nstep > 0) ? nstep : 1),
                          track_info->imi[track_info->c_i] + nf_recount,
                          RECOUNT_BEAD, mouse_selected_bead, imr_TRACK, oi_TRACK, pr_TRACK,
                          op, (void *)(&c_d), job_recount , nf_recount);
    }

    return D_REDRAWME;
}


int adj_calibration_by_hand(void)
{
    int i;
    char buf[1024] = {0};
    b_track *bt = NULL;
    float zoff = 0;
    //  char fullfile[1024];
    O_i *oi = NULL;

    if (mouse_selected_bead < 0 || mouse_selected_bead >= track_info->n_b)
    {
        return D_O_K;
    }

    if (updating_menu_state != 0)
    {
        bt = track_info->bd[mouse_selected_bead];

        if (bt->calib_im == NULL)
        {
            bead_menu[0].flags &= ~D_DISABLED;
        }
        else
        {
            bead_menu[0].flags |=  D_DISABLED;
        }

        return D_O_K;
    }

    bt = track_info->bd[mouse_selected_bead];
    oi = bt->calib_im;

    if (oi == NULL || strncmp(oi->im.source, "equally spaced reference profile", 31) != 0)
    {
        win_printf_OK("This is not a calibration image");
    }

    zoff = oi->yu[oi->c_yu]->ax;
    snprintf(buf, 1024, "Manual adjustment of calibration image\n"
             "for bead %d the z position is set in objective \\mu m"
             "without correction. Present offset %g\nNew value %%8f\n", mouse_selected_bead, zoff);
    i = win_scanf(buf, &zoff);

    if (i == WIN_CANCEL)
    {
        return D_O_K;
    }

    if (oi->n_yu > 2)
    {
        oi->yu[2]->ax = zoff;
    }

    oi->ay = zoff;
    oi = bt->calib_im_fil;

    if (oi != NULL)
    {
        if (oi->n_yu > 2)
        {
            oi->yu[2]->ax = zoff;
        }

        oi->ay = zoff;
    }

    snprintf(buf, 1024, " Bead %d Manual calibration adjustment %6.3f microns ", mouse_selected_bead, zoff);
    my_set_window_title("%s",buf);

    if (current_write_open_path_picofile != NULL)
    {
        dump_to_html_log_file_with_date_and_time(current_write_open_picofile,
                "Calibration image %d manually offseted new length adjustment %6.3f microns\n%s",
                mouse_selected_bead,  zoff, oi->filename);
    }

    return 0;
}






O_i	*do_load_calibration(int n)
{
    int i = 0;
    char path[512] = { 0 };
    char file[256] = { 0 };
    char name[256] = { 0 };
    char *fullfile = NULL;
    O_i *oi = NULL;
    snprintf(name, sizeof(name), "last_calibration_%d", n);
    fullfile = open_one_file_config("Load calibration image (*.gr)", NULL,
                                    "Calib Files (*.gr)\0*.gr\0All Files (*.*)\0*.*\0\0", "IMAGE-GR-FILE", name);

    if (fullfile)
    {
        oi = load_im_file_in_oi(fullfile, path);

        if (oi == NULL)
        {
            extract_file_path(path, sizeof(path), fullfile);
            win_printf("could not load image\n%s\n"
                       "from path %s", file, backslash_to_slash(path));
            return NULL;
        }

        i++;
    }

    //win_printf("loaded image\n%s\n"
    //	     "from path %s",file, backslash_to_slash(path));
    return oi;
}





O_i	*do_load_calibration_no_question(int n)
{
    char path[512] = {0}, file[256] = {0};
    static char fullfile[512]  = {0}, name[128] = {0};
    const char *fu = NULL;
    O_i *oi = NULL;

    snprintf(name, 128, "last_calibration_%d", n);
    fu = (const char *)get_config_string("IMAGE-GR-FILE", name, NULL);

    if (fu == NULL)
    {
        return NULL;
    }

    strcpy(fullfile, fu);
    extract_file_name(file, sizeof(file), fullfile);
    extract_file_path(path, sizeof(path), fullfile);
    put_path_separator(path, sizeof(path));
    oi = load_im_file_in_oi(file, path);

    if (oi == NULL)
    {
        return NULL;
    }

    return oi;
}





int grab_bead_info_from_calibration(O_i *oi, float *xb, float *yb, float *zb, int *nx, int *cw)
{
    int nb;
    char *co = NULL, *cs = NULL;

    if (oi == NULL)
    {
        return 1;
    }

    co = cs = (oi->im.source != NULL) ? oi->im.source : oi->im.treatement;

    if (cs == NULL)
    {
        win_printf("wrong souce!");
        return 1;
    }

    //win_printf(co);
    if (strncmp(cs, "equally spaced reference profile", 31) != 0)
    {
        win_printf("image must be :\\sl  equally spaced reference profile!\n");
    }

    cs = strstr(co, "Bead");

    if (cs != NULL && xb != NULL)
    {
        //if (sscanf(cs+4,"%d",&nb) != 1)
        //win_printf("cannot read bead bead nb!");
        sscanf(cs + 4, "%d", &nb);
    }

    cs = strstr(co, "xcb");

    if (cs != NULL && xb != NULL)
    {
        if (sscanf(cs + 3, "%f", xb) != 1)
        {
            win_printf("cannot read bead xc!");
        }
    }

    cs = strstr(cs, "ycb");

    if (cs != NULL && yb != NULL)
    {
        if (sscanf(cs + 3, "%f", yb) != 1)
        {
            win_printf("cannot read bead yc!");
        }
    }

    cs = strstr(cs, "zcb");

    if (cs != NULL && zb != NULL)
    {
        if (sscanf(cs + 3, "%f", zb) != 1)
        {
            win_printf("cannot read bead zc!");
        }
    }

    cs = strstr(cs, "nxb");

    if (cs != NULL && nx != NULL)
    {
        if (sscanf(cs + 3, "%d", nx) != 1)
        {
            win_printf("cannot read bead ncd!");
        }
    }

    cs = strstr(cs, "cwb");

    if (cs != NULL && cw != NULL)
    {
        if (sscanf(cs + 3, "%d", cw) != 1)
        {
            win_printf("cannot read bead cwd!");
        }
    }

    return 0;
}



int grab_bead_filter_info_from_calibration(O_i *oi, int *rc, int *fc, int *fw)
{
    char *co = NULL, *cs = NULL;

    if (oi == NULL)
    {
        return 1;
    }

    co = cs = (oi->im.source != NULL) ? oi->im.source : oi->im.treatement;

    if (cs == NULL)
    {
        win_printf("wrong souce!");
        return 1;
    }

    //win_printf(co);
    if (strncmp(cs, "equally spaced reference profile", 31) != 0)
    {
        win_printf("image must be :\\sl  equally spaced reference profile!\n");
    }

    cs = strstr(co, "Forget circle");

    if (cs != NULL && rc != NULL)
    {
        if (sscanf(cs + 14, "%d", rc) != 1)
        {
            win_printf("cannot read forget circle!");
        }
    }

    cs = strstr(co, "bp_filter");

    if (cs != NULL && fc != NULL)
    {
        if (sscanf(cs + 9, "%d", fc) != 1)
        {
            win_printf("cannot read bp filter!");
        }
    }

    cs = strstr(cs, "bp_width");

    if (cs != NULL && fw != NULL)
    {
        if (sscanf(cs + 8, "%d", fw) != 1)
        {
            win_printf("cannot read filter width!");
        }
    }

    return 0;
}



//# ifdef ENCOURS



O_i	*do_load_generic_calibration(int n)
{
    int i = 0;
    char path[512] = { 0 };
    char file[256] = { 0 };
    char name[256] = {0};
    char *fullfile = NULL;
    O_i *oi = NULL;
    snprintf(name, sizeof(name), "last_generic_calibration_%d", n);
    fullfile = open_one_file_config("Load generic calibration image (*.gr)", NULL,
                                    "Calib Files (*.gr)\0*.gr\0All Files (*.*)\0*.*\0\0", "IMAGE-GR-FILE", name);

    if (fullfile)
    {
        //fu = fullfile;
        oi = load_im_file_in_oi(fullfile, path);

        if (oi == NULL)
        {
            win_printf("could not load image\n%s\n"
                       "from path %s", file, backslash_to_slash(path));
            return NULL;
        }

        i++;
        free(fullfile);
    }

    //win_printf("loaded image\n%s\n"
    //	     "from path %s",file, backslash_to_slash(path));
    return oi;
}


int attach_generic_calibration_image(O_i *oi)
{
    float xb, yb, zb;
    int bnx = 128, bcw = 16, rc = 12, fw = 12, fc = 16;
    O_p *op = NULL;
    d_s *ds = NULL;
    fft_plan *ft = NULL;
    filter_bt *fil = NULL;

    if (oi == NULL)
    {
        return win_printf_OK("Could not load calibration image");
    }

    if (grab_bead_info_from_calibration(oi, &xb, &yb, &zb, &bnx, &bcw))
    {
        return win_printf_OK("error in retrieving bead data");
    }

    if (grab_bead_filter_info_from_calibration(oi, &rc, &fc, &fw))
    {
        return win_printf_OK("error in retrieving bead data");
    }

    //win_printf("rc = %d fc = %d fw %d ",rc, fc, fw);
    //win_printf("xcb = %g ycb = %g nx %d cw %d",xb, yb, bnx, bcw);
    op = create_and_attach_op_to_oi(oi, bnx, bnx, 0, IM_SAME_Y_AXIS | IM_SAME_X_AXIS);

    if (op == NULL)
    {
        return win_printf_OK("cannot create calibration plot !");
    }

    ds = op->dat[0];
    set_ds_source(ds, "Calibration nearest profile -1       ");

    if ((ds = create_and_attach_one_ds(op, bnx, bnx, 0)) == NULL)
    {
        return win_printf_OK("I can't create plot !");
    }

    set_ds_source(ds, "Bead profile");
    oi->cur_op = 1;
    add_image(imr_TRACK, oi->im.data_type, (void *)oi);

    if (track_info->n_oi >= track_info->m_oi)
    {
        track_info->generic_calib_im = (O_i **)realloc(track_info->generic_calib_im, track_info->m_oi * sizeof(O_i *));
        track_info->generic_calib_im_fil = (O_i **)realloc(track_info->generic_calib_im_fil, track_info->m_oi * sizeof(O_i *));

        if (track_info->generic_calib_im == NULL || track_info->generic_calib_im_fil == NULL)
        {
            return win_printf_OK("cannot alloc o_i!");
        }
    }

    track_info->generic_calib_im[track_info->n_oi] = oi;
    //while(fft_used_in_tracking);             // we avoid threads pb
    //fft_used_in_dialog = 1;
    ft = fftbt_init(NULL, bnx);
    fil = filter_init_bt(NULL, bnx);
    track_info->generic_calib_im_fil[track_info->n_oi] = image_band_pass_in_real_out_complex(NULL, oi, 0, fc, fw, rc, ft,
            fil);
    //fft_used_in_dialog = 0;
    oi->oi_idle_action = calibration_oi_idle_action;
    oi->oi_post_display = calibration_oi_post_display;

    if (track_info->generic_calib_im_fil[track_info->n_oi] == NULL)
    {
        return win_printf_OK("Can't compute calibration image");
    }

    ask_to_update_menu();//do_update_menu();
    free_fft_plan_bt(ft);
    free_filter_bt(fil);
    track_info->n_oi++;
    return 0;
}



O_i	*do_reload_generic_calibration(int n)
{
    char path[512] = { 0 };
    char file[256] = { 0 };
    char name[128] = { 0 };
    static char fullfile[512] = { 0 };
    const char *fu = NULL;
    O_i *oi = NULL;
    snprintf(name, 128, "last_generic_calibration_%d", n);
    fu = (const char *)get_config_string("IMAGE-GR-FILE", name, NULL);

    if (fu == NULL)
    {
        return NULL;
    }

    strncpy(fullfile, fu, sizeof(fullfile));
    fu = fullfile;
    extract_file_name(file, sizeof(file), fullfile);
    extract_file_path(path, sizeof(path), fullfile);
    put_path_separator(path, sizeof(path));
    oi = load_im_file_in_oi(file, path);

    if (oi != NULL)
    {
        attach_generic_calibration_image(oi);
    }

    return oi;
}

int link_beads_to_generic_calibration(void)
{
    int i, j;
    b_track *bd = NULL;
    int rc, fc, fw, bnx, bcw;
    float xb, yb, zb;

    for (j = 0; j < track_info->n_oi; j++)
    {
        if (grab_bead_info_from_calibration(track_info->generic_calib_im[j], &xb, &yb, &zb, &bnx, &bcw))
        {
            return win_printf_OK("error in retrieving bead data");
        }

        if (grab_bead_filter_info_from_calibration(track_info->generic_calib_im[j], &rc, &fc, &fw))
        {
            return win_printf_OK("error in retrieving bead data");
        }

        for (i = 0; i < track_info->n_b; i++)
        {
            bd = track_info->bd[i];

            if (bd->bead_generic_index == j && bd->cl == bnx)
            {
                bd->generic_calib_im = track_info->generic_calib_im[j];
                bd->generic_calib_im_fil = track_info->generic_calib_im_fil[j];
                bd->rc = rc;
                bd->bp_center = fc;
                bd->bp_width = fw;
                bd->ncl = bnx;
                bd->ncw = bcw;
            }
        }
    }

    oi_TRACK->need_to_refresh |= PLOTS_NEED_REFRESH;
    ask_to_update_menu();
    return 0;
}


int load_generic_calibration_image(void)
{
    O_i *oi = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    oi = do_load_generic_calibration(0);

    if (oi == NULL)
    {
        return win_printf_OK("Could not load generic calibration image");
    }

    attach_generic_calibration_image(oi);
    link_beads_to_generic_calibration();
    ask_to_update_menu();
    return D_REDRAWME;
}


//# endif




int link_calibration_to_cross(int cross, O_i *oi)
{
    float xb, yb, zb;
    int i = 0 , j = 0, bnx = 128, bcw = 16;
    O_p *op = NULL;
    d_s *ds = NULL;
    b_track *bt = NULL;

    for (j = 0; j < track_info->n_oi; j++)
    {
        if (track_info->generic_calib_im[j] == NULL
                || track_info->generic_calib_im[j]->filename == NULL)
        {
            break;
        }
    }

    j = (j > 0) ? j - 1 : 0;

    for (i = track_info->n_b; i <= cross; i++)
    {
        do_follow_bead_in_x_y(GENERIC_IMAGE_CAL + j); // +POSITION_SET_BY_MOUSE
        //win_printf("bead %d created",cross);
    }

    bt = track_info->bd[cross];

    if (bt->calib_im != NULL)
    {
        free_one_image(bt->calib_im);
    }

    if (bt->calib_im_fil != NULL)
    {
        free_one_image(bt->calib_im_fil);
    }

    //  if (oi == NULL)  oi = do_load_calibration_no_question(cross);
    if (oi == NULL)
    {
        return D_REDRAWME;
    }

    if (grab_bead_info_from_calibration(oi, &xb, &yb, &zb, &bnx, &bcw))
    {
        return win_printf_OK("error in retrieving bead data");
    }

    //win_printf("xcb = %g ycb = %g nx %d cw %d",xb, yb, bnx, bcw);
    op = create_and_attach_op_to_oi(oi, bnx, bnx, 0, IM_SAME_Y_AXIS | IM_SAME_X_AXIS);

    if (op == NULL)
    {
        return win_printf_OK("cannot create calibration plot !");
    }

    ds = op->dat[0];
    set_ds_source(ds, "Calibration nearest profile -1       ");

    if ((ds = create_and_attach_one_ds(op, bnx, bnx, 0)) == NULL)
    {
        return win_printf_OK("I can't create plot !");
    }

    set_ds_source(ds, "Bead profile");
    oi->cur_op = 0;
    bt->cl = bt->ncl = bnx;
    bt->cw = bt->ncw = bcw;

    //win_printf("bead %d size %d->%d %d->%d",cross,bt->ncl,bnx,bt->ncw,bcw);
    if (cross == 0)
    {
        my_set_window_title("bead %d size %d->%d %d->%d", cross, bt->ncl, bnx, bt->ncw, bcw);
    }

    if (bt->in_image == 0)
    {
        bt->mx = bt->xc = bt->x0 = x_imdata_2_imr_nl(imr_TRACK, (int)(0.5 + xb / oi_TRACK->dx), d_TRACK);
        bt->my = bt->yc = bt->y0 = y_imdata_2_imr_nl(imr_TRACK, (int)(0.5 + yb / oi_TRACK->dy), d_TRACK);
        bt->last_x_not_lost = (float)bt->xc;
        bt->last_y_not_lost = (float)bt->yc;
    }

    add_image(imr_TRACK, oi->im.data_type, (void *)oi);
    bt->calib_im = oi;

    if (grab_bead_info_from_calibration(bt->calib_im, &xb, &yb, &zb, &bnx, &bcw) == 0 && xb > 0 && bt->in_image)
    {
        bt->fence_x = bt->calibration_im_x = xb;
        bt->fence_y = bt->calibration_im_y = yb;
        bt->fence_xi = bt->calibration_im_xi = (int)(0.5 + (bt->calibration_im_x / oi_TRACK->dx));
        bt->fence_yi = bt->calibration_im_yi = (int)(0.5 + (bt->calibration_im_y / oi_TRACK->dy));
    }

    //while(fft_used_in_tracking);             // we avoid threads pb
    //fft_used_in_dialog = 1;
    bt->calib_im_fil = image_band_pass_in_real_out_complex(NULL, oi, 0, bt->bp_center, bt->bp_width, bt->rc, bt->z_trp,
                       bt->z_fil);
    //fft_used_in_dialog = 0;
    oi->oi_idle_action = calibration_oi_idle_action;
    oi->oi_post_display = calibration_oi_post_display;

    //win_printf("filtererd im");
    if (bt->calib_im_fil == NULL)
    {
        return win_printf_OK("Can't compute calibration image");
    }

    ask_to_update_menu(); //do_update_menu();
    return D_REDRAWME;
}

int relink_all_calibrations(void)
{
    int n, i, nb;
    char name[128] = {0};
    const char *fu = NULL;
    O_i *oi = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    nb = get_config_int("IMAGE-GR-FILE", "last_calibration_n", 256);

    for (i = n = 0; i < nb; i++)
    {
        // we scan config file to find the max of beads
        snprintf(name, sizeof(name), "last_calibration_%d", i);
        fu = (const char *)get_config_string("IMAGE-GR-FILE", name, NULL);

        if (fu != NULL)
        {
            n = i;
        }
    }

    for (i = 0; i <= n; i++)
    {
        snprintf(name, 128, "last_calibration_%d", i);
        fu = (const char *)get_config_string("IMAGE-GR-FILE", name, NULL);

        if (fu != NULL)
        {
            oi = do_load_calibration_no_question(i);
            link_calibration_to_cross(i, oi);
            position_cross_according_to_calibration_image(track_info->bd[i]);
        }
    }

    ask_to_update_menu();
    return D_REDRAWME;
}




int reload_calibrations_from_trk(char *fullfile)
{
    int i;
    O_i *oi = NULL;
    char path[512] = {0};
    g_record *g_r = NULL;
    extract_file_path(path, sizeof(path), fullfile);
    g_r = read_record_file_header(fullfile, 0);

    if (g_r == NULL)
    {
        return win_printf_OK("Pb loading tracking data from :\n%s", backslash_to_slash(fullfile));
    }

    //win_printf("Loading tracking data from :\n%s",backslash_to_slash(fullfile));
    for (i = 0; i < g_r->n_bead; i++)
    {
        oi = load_calib_im_file_from_record(g_r, i);

        if (oi == NULL)
        {
            //win_printf_OK("Pb loading calibration image %d",i);
            link_calibration_to_cross(i, NULL);
        }
        else
        {
            //win_printf("linking cal %d",i);
            link_calibration_to_cross(i, oi);
            position_cross_according_to_calibration_image(track_info->bd[i]);
        }
    }

    //win_printf_OK("before End loading calibration images");

    if (current_write_open_path_picofile != NULL)
    {
        dump_to_html_log_file_with_date_and_time(current_write_open_picofile,
                "Reloaded %d calibration image from TRK %s\n", g_r->n_bead, fullfile);
    }

    free_gen_record(g_r);
    //win_printf_OK("End loading calibration images");
    return D_O_K;
}

int reload_calibrations_from_last_trk_old(void)
{
    const char *pa = NULL;
    char pal[1024] = {0};

    pa = (const char *)get_config_string("TRACKING-FILE", "last_saved", NULL);

    if (updating_menu_state != 0)
    {
        if (pa == NULL)
        {
            bead_menu[0].flags &= ~D_DISABLED;
        }
        else
        {
            bead_menu[0].flags |=  D_DISABLED;
        }

        return D_O_K;
    }

    if (pa != NULL)
    {
        strncpy(pal,pa,sizeof(pal));
        reload_calibrations_from_trk(pal);
    }

    ask_to_update_menu();
    return D_O_K;
}

int reload_calibrations_from_last_trk(void)
{
    register int i = 0;
    char path[512] = {0}, fullfile[512] = {0};
    const char *fu = NULL;
#ifdef XV_WIN32
    char file[256] = {0};
#endif
    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (fu == NULL)
    {
        Open_Pico_config_file();
        fu = (const char *)get_config_string("TRACKING-FILE", "last_saved", NULL);

        if (fu == NULL)
        {
            fu = (const char *)get_config_string("TRACKING-FILE", "last_loaded", NULL);
        }

        Close_Pico_config_file();

        if (fu != NULL)
        {
            strcpy(fullfile, fu);
            fu = fullfile;
        }
        else
        {
            my_getcwd(fullfile, sizeof(fullfile));
            put_path_separator(path, sizeof(path));
        }
    }

#ifdef XV_WIN32

    if (full_screen)
    {
#endif
        switch_allegro_font(1);
        i = file_select_ex("Load tracking record (*.trk) in a new project", fullfile, "trk", 512, 0, 0);
        switch_allegro_font(0);
#ifdef XV_WIN32
    }
    else
    {
        extract_file_path(path, sizeof(path), fullfile);
        put_backslash(path);

        if (extract_file_name(file, sizeof(file), fullfile) != NULL)
        {
            strcpy(fullfile, file);
        }
        else
        {
            fullfile[0] = file[0] = 0;
        }

        fu = DoFileOpen("Load tracking record (*.trk) in a new project", path, fullfile, 512,
                        "Plot Files (*.trk)\0*.trk\0All Files (*.*)\0*.*\0\0", "trk");
        i = (fu == NULL) ? 0 : 1;
        //win_printf("Path %s",backslash_to_slash(path));
    }

#endif

    if (i != 0)
    {
        fu = fullfile;
        reload_calibrations_from_trk(fullfile);
        Open_Pico_config_file();
        set_config_string("TRACKING-FILE", "last_loaded", fullfile);
        write_Pico_config_file();
        Close_Pico_config_file();
    }

    return 0;
}

int link_calibration(void)
{
    float xb, yb, zb;
    int bnx = 128, bcw = 16;
    O_i *oi = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    b_track *bt = NULL;

    if (mouse_selected_bead < 0 || mouse_selected_bead >= track_info->n_b)
    {
        return D_O_K;
    }

    if (updating_menu_state != 0)
    {
        bt = track_info->bd[mouse_selected_bead];

        if (bt->calib_im == NULL)
        {
            bead_menu[0].flags &= ~D_DISABLED;
        }
        else
        {
            bead_menu[0].flags |=  D_DISABLED;
        }

        return D_O_K;
    }

    bt = track_info->bd[mouse_selected_bead];
    oi = do_load_calibration(mouse_selected_bead);

    if (oi == NULL)
    {
        return win_printf_OK("Could not load calibration image");
    }

    if (grab_bead_info_from_calibration(oi, &xb, &yb, &zb, &bnx, &bcw))
    {
        return win_printf_OK("error in retrieving bead data");
    }

    //win_printf("xcb = %g ycb = %g nx %d cw %d",xb, yb, bnx, bcw);
    op = create_and_attach_op_to_oi(oi, bnx, bnx, 0, IM_SAME_Y_AXIS | IM_SAME_X_AXIS);

    if (op == NULL)
    {
        return win_printf_OK("cannot create calibration plot !");
    }

    ds = op->dat[0];
    set_ds_source(ds, "Calibration nearest profile -1       ");

    if ((ds = create_and_attach_one_ds(op, bnx, bnx, 0)) == NULL)
    {
        return win_printf_OK("I can't create plot !");
    }

    set_ds_source(ds, "Bead profile");
    oi->cur_op = 1;

    if (bt->in_image == 0)
    {
        bt->mx = bt->xc = bt->x0 = x_imdata_2_imr_nl(imr_TRACK, (int)(0.5 + xb / oi_TRACK->dx), d_TRACK);
        bt->my = bt->yc = bt->y0 = y_imdata_2_imr_nl(imr_TRACK, (int)(0.5 + yb / oi_TRACK->dy), d_TRACK);
        bt->last_x_not_lost = (float)bt->xc;
        bt->last_y_not_lost = (float)bt->yc;
    }

    add_image(imr_TRACK, oi->im.data_type, (void *)oi);
    bt->calib_im = oi;

    if (grab_bead_info_from_calibration(bt->calib_im, &xb, &yb, &zb, &bnx, &bcw) == 0)
    {
        bt->fence_x = bt->calibration_im_x = xb;
        bt->fence_x = bt->calibration_im_y = yb;
        bt->fence_xi = bt->calibration_im_xi = (int)(0.5 + (bt->calibration_im_x / oi_TRACK->dx));
        bt->fence_yi = bt->calibration_im_yi = (int)(0.5 + (bt->calibration_im_y / oi_TRACK->dy));
    }

    //while(fft_used_in_tracking);             // we avoid threads pb
    //fft_used_in_dialog = 1;
    bt->calib_im_fil = image_band_pass_in_real_out_complex(NULL, oi, 0, bt->bp_center, bt->bp_width, bt->rc, bt->z_trp,
                       bt->z_fil);
    //fft_used_in_dialog = 0;
    oi->oi_idle_action = calibration_oi_idle_action;
    oi->oi_post_display = calibration_oi_post_display;

    //win_printf("filtererd im");
    if (bt->calib_im_fil == NULL)
    {
        return win_printf_OK("Can't compute calibration image");
    }

    ask_to_update_menu(); //do_update_menu();
    return D_REDRAWME;
}

int remove_calibration(void)
{
    register int i, j;
    O_i *oi = NULL;
    b_track *bt = NULL;

    if (mouse_selected_bead < 0 || mouse_selected_bead >= track_info->n_b)
    {
        return D_O_K;
    }

    if (updating_menu_state != 0)
    {
        bt = track_info->bd[mouse_selected_bead];

        if (bt->calib_im == NULL)
        {
            bead_menu[1].flags |=  D_DISABLED;
        }
        else
        {
            bead_menu[1].flags &= ~D_DISABLED;
        }

        return D_O_K;
    }

    bt = track_info->bd[mouse_selected_bead];
    oi = bt->calib_im;

    for (i = 0, j = -1; i < imr_TRACK->n_oi && j < 0; i++)
    {
        j = (imr_TRACK->o_i[i] == oi) ? i : j;
    }

    bt->calibration_im_x = -1;
    bt->calibration_im_y = -1;
    bt->calibration_im_xi = -1;
    bt->calibration_im_yi = -1;
    bt->calib_im = NULL;

    if (j > 0)
    {
        remove_from_image(imr_TRACK, IS_ONE_IMAGE, (void *)imr_TRACK->o_i[j]);
    }
    else
    {
        free_one_image(oi);
    }

    oi = bt->calib_im_fil;
    bt->calib_im_fil = NULL;
    free_one_image(oi);
    ask_to_update_menu();// do_update_menu();
    imr_TRACK_title_change_asked++;
    return D_O_K;
}


int remove_all_calibration(void)
{
    register int i, j, im;
    O_i *oi = NULL;
    b_track *bt = NULL;

    if (updating_menu_state != 0)
    {
        for (im = 0, i = 0; im < track_info->n_b; im++)
        {
            bt = track_info->bd[im];

            if (bt->calib_im != NULL)
            {
                i++;
            }
        }

        if (i == 0)
        {
            bead_menu[1].flags |=  D_DISABLED;
        }
        else
        {
            bead_menu[1].flags &= ~D_DISABLED;
        }

        return D_O_K;
    }

    for (im = 0; im < track_info->n_b; im++)
    {
        bt = track_info->bd[im];
        oi = bt->calib_im;

        if (oi == NULL)
        {
            continue;
        }

        bt->calibration_im_x = -1;
        bt->calibration_im_y = -1;
        bt->calibration_im_xi = -1;
        bt->calibration_im_yi = -1;

        for (i = 0, j = -1; i < imr_TRACK->n_oi && j < 0; i++)
        {
            j = (imr_TRACK->o_i[i] == oi) ? i : j;
        }

        bt->calib_im = NULL;

        if (j > 0)
        {
            remove_from_image(imr_TRACK, IS_ONE_IMAGE, (void *)imr_TRACK->o_i[j]);
        }
        else
        {
            free_one_image(oi);
        }

        oi = bt->calib_im_fil;
        bt->calib_im_fil = NULL;
        free_one_image(oi);
    }

    ask_to_update_menu();// do_update_menu();
    imr_TRACK_title_change_asked++;
    return D_O_K;
}



int set_black_circle(void)
{
    b_track *bt = NULL;

    if (mouse_selected_bead < 0 || mouse_selected_bead >= track_info->n_b)
    {
        return D_O_K;
    }

    if (updating_menu_state != 0)
    {
        bt = track_info->bd[mouse_selected_bead];

        if (bt->black_circle == 0)
        {
            bead_menu[2].flags &= ~D_DISABLED;
        }
        else
        {
            bead_menu[2].flags |=  D_DISABLED;
        }

        return D_O_K;
    }

    bt = track_info->bd[mouse_selected_bead];
    bt->black_circle = 1;
    return D_O_K;
}

int unset_black_circle(void)
{
    b_track *bt = NULL;

    if (mouse_selected_bead < 0 || mouse_selected_bead >= track_info->n_b)
    {
        return D_O_K;
    }

    if (updating_menu_state != 0)
    {
        bt = track_info->bd[mouse_selected_bead];

        if (bt->black_circle == 1)
        {
            bead_menu[3].flags &= ~D_DISABLED;
        }
        else
        {
            bead_menu[3].flags |=  D_DISABLED;
        }

        return D_O_K;
    }

    bt = track_info->bd[mouse_selected_bead];
    bt->black_circle = 0;
    return D_O_K;
}

int put_fixed(void)
{
    b_track *bt = NULL;

    if (mouse_selected_bead < 0 || mouse_selected_bead >= track_info->n_b)
    {
        return D_O_K;
    }

    if (updating_menu_state != 0)
    {
        bt = track_info->bd[mouse_selected_bead];

        if (bt->fixed_bead == 0)
        {
            bead_menu[2].flags &= ~D_DISABLED;
        }
        else
        {
            bead_menu[2].flags |=  D_DISABLED;
        }

        return D_O_K;
    }

    bt = track_info->bd[mouse_selected_bead];
    track_info->generic_reference = track_info->generic_reference - bt->z_avgd;
    bt->fixed_bead = 1;
    return D_O_K;
}



int set_coilable(void)
{
    b_track *bt = NULL;

    if (mouse_selected_bead < 0 || mouse_selected_bead >= track_info->n_b)
    {
        return D_O_K;
    }

    if (updating_menu_state != 0)
    {
        bt = track_info->bd[mouse_selected_bead];

        if (bt->coilable_bead == 0)
        {
            bead_menu[2].flags &= ~D_DISABLED;
        }
        else
        {
            bead_menu[2].flags |=  D_DISABLED;
        }

        return D_O_K;
    }

    bt = track_info->bd[mouse_selected_bead];
    bt->coilable_bead = (bt->coilable_bead) ? 0 : 1;
    return D_O_K;
}


int set_generic_zero(void)
{
    b_track *bt = NULL;

    if (mouse_selected_bead < 0 || mouse_selected_bead >= track_info->n_b)
    {
        return D_O_K;
    }

    if (updating_menu_state != 0)
    {
        bt = track_info->bd[mouse_selected_bead];

        if (bt->fixed_bead == 0)
        {
            bead_menu[2].flags &= ~D_DISABLED;
        }
        else
        {
            bead_menu[2].flags |=  D_DISABLED;
        }

        return D_O_K;
    }

    bt = track_info->bd[mouse_selected_bead];
    track_info->generic_reference = -bt->z_avgd;
    return D_O_K;
}

int reset_generic_zero(void)
{
    b_track *bt = NULL;

    if (mouse_selected_bead < 0 || mouse_selected_bead >= track_info->n_b)
    {
        return D_O_K;
    }

    if (updating_menu_state != 0)
    {
        bt = track_info->bd[mouse_selected_bead];

        if (bt->fixed_bead == 0)
        {
            bead_menu[2].flags &= ~D_DISABLED;
        }
        else
        {
            bead_menu[2].flags |=  D_DISABLED;
        }

        return D_O_K;
    }

    bt = track_info->bd[mouse_selected_bead];
    track_info->generic_reference = 0;
    return D_O_K;
}

int unput_fixed(void)
{
    b_track *bt = NULL;

    if (mouse_selected_bead < 0 || mouse_selected_bead >= track_info->n_b)
    {
        return D_O_K;
    }

    if (updating_menu_state != 0)
    {
        bt = track_info->bd[mouse_selected_bead];

        if (bt->fixed_bead == 1)
        {
            bead_menu[3].flags &= ~D_DISABLED;
        }
        else
        {
            bead_menu[3].flags |=  D_DISABLED;
        }

        return D_O_K;
    }

    bt = track_info->bd[mouse_selected_bead];
    bt->fixed_bead = 0;
    return D_O_K;
}


int    do_bead_menu(int item)
{
    (void)item;
    updating_menu_state = 1;
    remove_calibration();
    remove_all_calibration();
    link_calibration();
    relink_all_calibrations();
    unset_black_circle();
    set_black_circle();
    put_fixed();
    unput_fixed();
    set_coilable();
    set_generic_zero();
    reset_generic_zero();
    updating_menu_state = 0;
    return D_O_K;
}


MENU *prepare_calib_menu(b_track *bt)
{
    register int i, j;
    i = 0;
    char ch[256] = {0};
    const char *patm = NULL;
    const char *pa = NULL;
    static char pat[1024] = {0};

    if (bt != NULL)
    {
        bead_menu[i].text = "Change averaging width";
        bead_menu[i].proc = change_cross_avg_width;
        bead_menu[i].child = NULL;
        bead_menu[i].flags = 0;
        bead_menu[i++].dp = NULL;
        bead_menu[i].text = "Change cross size";
        bead_menu[i].proc = change_cross_size;
        bead_menu[i].child = NULL;
        bead_menu[i].flags = 0;
        bead_menu[i++].dp = NULL;
        bead_menu[i].text = "Turn cross by 45 degrees";
        bead_menu[i].proc = change_cross_angle;
        bead_menu[i].child = NULL;
        bead_menu[i].flags = 0;
        bead_menu[i++].dp = NULL;
        bead_menu[i].text = "Averaging filter And fence";
        bead_menu[i].proc = change_cross_avg_filter;
        bead_menu[i].child = NULL;
        bead_menu[i].flags = 0;
        bead_menu[i++].dp = NULL;

        if (bt->black_circle == 0)
        {
            bead_menu[i].text = "Set black circle";
            bead_menu[i].proc = set_black_circle;
            bead_menu[i].child = NULL;
            bead_menu[i].flags = 0;
            bead_menu[i++].dp = NULL;
        }
        else
        {
            bead_menu[i].text = "Unset black circle";
            bead_menu[i].proc = unset_black_circle;
            bead_menu[i].child = NULL;
            bead_menu[i].flags = 0;
            bead_menu[i++].dp = NULL;
        }

        bead_menu[i].text = "\0";
        bead_menu[i].proc = NULL;
        bead_menu[i].child = NULL;
        bead_menu[i].flags = 0;
        bead_menu[i++].dp = NULL;

        if (bt->fixed_bead == 0)
        {
            bead_menu[i].text = "Set as fixed bead";
            bead_menu[i].proc = put_fixed;
            bead_menu[i].child = NULL;
            bead_menu[i].flags = 0;
            bead_menu[i++].dp = NULL;
        }
        else
        {
            bead_menu[i].text = "Set as bead of interest";
            bead_menu[i].proc = unput_fixed;
            bead_menu[i].child = NULL;
            bead_menu[i].flags = 0;
            bead_menu[i++].dp = NULL;
        }

        if (bt->coilable_bead == 0)
        {
            bead_menu[i].text = "Insensitive to torsion";
            bead_menu[i].proc = set_coilable;
            bead_menu[i].child = NULL;
            bead_menu[i].flags = 0;
            bead_menu[i++].dp = NULL;
        }
        else
        {
            bead_menu[i].text = "Sensitive to torsion";
            bead_menu[i].proc = set_coilable;
            bead_menu[i].child = NULL;
            bead_menu[i].flags = 0;
            bead_menu[i++].dp = NULL;
        }

        if (track_info->do_diff_track == 0)
        {
            bead_menu[i].text = "Use differential tracking";
            bead_menu[i].proc = set_diff_track;
            bead_menu[i].child = NULL;
            bead_menu[i].flags = 0;
            bead_menu[i++].dp = NULL;
        }
        else
        {
            bead_menu[i].text = "Use normal tracking";
            bead_menu[i].proc = set_diff_track;
            bead_menu[i].child = NULL;
            bead_menu[i].flags = 0;
            bead_menu[i++].dp = NULL;
        }

        bead_menu[i].text = "\0";
        bead_menu[i].proc = NULL;
        bead_menu[i].child = NULL;
        bead_menu[i].flags = 0;
        bead_menu[i++].dp = NULL;
        bead_menu[i].text = "Save position of all beads";
        bead_menu[i].proc = do_save_cross_position;
        bead_menu[i].child = NULL;
        bead_menu[i].flags = 0;
        bead_menu[i++].dp = NULL;
        bead_menu[i].text = "Restore position of all beads";
        bead_menu[i].proc = put_back_cross_save;
        bead_menu[i].child = NULL;
        bead_menu[i].flags = 0;
        bead_menu[i++].dp = NULL;
        bead_menu[i].text = "Count beads";
        bead_menu[i].proc = count_beads_like_this_one;
        bead_menu[i].child = NULL;
        bead_menu[i].flags = 0;
        bead_menu[i++].dp = NULL;

        if (bt->calib_im != NULL)
        {
            bead_menu[i].text = "Reset cross to cal. position";
            bead_menu[i].proc = put_back_cross;
            bead_menu[i].child = NULL;
            bead_menu[i].flags = 0;
            bead_menu[i++].dp = NULL;
            bead_menu[i].text = "Reset all beads to cal. position";
            bead_menu[i].proc = put_back_all_cross;
            bead_menu[i].child = NULL;
            bead_menu[i].flags = 0;
            bead_menu[i++].dp = NULL;
            bead_menu[i].text = "Adjust Z cal. position";
            bead_menu[i].proc = adj_calibration_by_hand;
            bead_menu[i].child = NULL;
            bead_menu[i].flags = 0;
            bead_menu[i++].dp = NULL;
        }

        bead_menu[i].text = "\0";
        bead_menu[i].proc = NULL;
        bead_menu[i].child = NULL;
        bead_menu[i].flags = 0;
        bead_menu[i++].dp = NULL;

        if (bt->calib_im == NULL)
        {
            bead_menu[i].text = "Load calibration";
            bead_menu[i].proc = link_calibration;
            bead_menu[i].child = NULL;
            bead_menu[i].flags = 0;
            bead_menu[i++].dp = NULL;
            bead_menu[i].text = "Record calibration";
            bead_menu[i].proc = record_calibration;
            bead_menu[i].child = NULL;
            bead_menu[i].flags = 0;
            bead_menu[i++].dp = NULL;
        }
        else
        {
            bead_menu[i].text = "Remove calibration";
            bead_menu[i].proc = remove_calibration;
            bead_menu[i].child = NULL;
            bead_menu[i].flags = 0;
            bead_menu[i++].dp = NULL;
        }

        bead_menu[i].text = "Remove all calibrations";
        bead_menu[i].proc = remove_all_calibration;
        bead_menu[i].child = NULL;
        bead_menu[i].flags = 0;
        bead_menu[i++].dp = NULL;
        bead_menu[i].text = "Reload all calibrations";
        bead_menu[i].proc = relink_all_calibrations;
        bead_menu[i].child = NULL;
        bead_menu[i].flags = 0;
        bead_menu[i++].dp = NULL;
        pa = (const char *)get_config_string("TRACKING-FILE", "last_saved", NULL);

        if (pa != NULL)
        {
            for (j = 0; pa[j] != 0; j++);

            for (j = (j > 0) ? j - 1 : 0; j >= 0 && is_path_separator(pa[j]); j--);

            patm = (j > 0) ? pa + j + 1 : pa;
            snprintf(pat, 1024, "Reload calibration from %s", patm);
            bead_menu[i].text = pat;
            bead_menu[i].proc = reload_calibrations_from_last_trk;
            bead_menu[i].child = NULL;
            bead_menu[i].flags = 0;
            bead_menu[i++].dp = NULL;
        }
        else
        {
            bead_menu[i].text = "Reload calibration from TRK";
            bead_menu[i].proc = reload_calibrations_from_last_trk;
            bead_menu[i].child = NULL;
            bead_menu[i].flags = 0;
            bead_menu[i++].dp = NULL;
        }

        if (bt->generic_calib_im != NULL && bt->calib_im == NULL)
        {
            if (track_info->generic_reference == 0)
            {
                bead_menu[i].text = "Set generic reference";
                bead_menu[i].proc = set_generic_zero;
            }
            else
            {
                bead_menu[i].text = "Reset generic reference";
                bead_menu[i].proc = reset_generic_zero;
            }

            bead_menu[i].child = NULL;
            bead_menu[i].flags = 0;
            bead_menu[i++].dp = NULL;
        }
    }
    else
    {
        for (j = 0; j < track_info->n_oi; j++)
        {
            if (track_info->generic_calib_im[j] == NULL
                    || track_info->generic_calib_im[j]->filename == NULL)
            {
                continue;
            }

            snprintf(ch, 256, "Generic %d %s ", j, track_info->generic_calib_im[j]->filename);
            bead_menu[i].text = strdup(ch);
            bead_menu[i].proc = follow_bead_in_x_y;
            bead_menu[i].child = NULL;
            bead_menu[i].flags = MENU_INDEX(GENERIC_IMAGE_CAL + j + POSITION_SET_BY_MOUSE);
            bead_menu[i++].dp = NULL;
            snprintf(ch, 256, "Multiple Generic %d %s ", j, track_info->generic_calib_im[j]->filename);
            bead_menu[i].text = strdup(ch);
            bead_menu[i].proc = follow_bead_in_x_y;
            bead_menu[i].child = NULL;
            bead_menu[i].flags = MENU_INDEX(GENERIC_IMAGE_CAL + j + POSITION_SET_BY_MOUSE + MULTIPLE_BEADS);
            bead_menu[i++].dp = NULL;
        }

        bead_menu[i].text = "New bead";
        bead_menu[i].proc = follow_bead_in_x_y;
        bead_menu[i].child = NULL;
        bead_menu[i].flags = MENU_INDEX(CHOOSE_SIZE + POSITION_SET_BY_MOUSE);
        bead_menu[i++].dp = NULL;
        bead_menu[i].text = "Multiple new beads";
        bead_menu[i].proc = follow_bead_in_x_y;
        bead_menu[i].child = NULL;
        bead_menu[i].flags = MENU_INDEX(CHOOSE_SIZE + POSITION_SET_BY_MOUSE + MULTIPLE_BEADS);
        bead_menu[i++].dp = NULL;
        bead_menu[i].text = "Configure create cross by shortcut";
        bead_menu[i].proc = do_configure_create_cross_by_shortcut;
        bead_menu[i].child = NULL;
        bead_menu[i].flags = 0;
        bead_menu[i++].dp = NULL;
    }

    bead_menu[i].text = NULL;
    bead_menu[i].proc = NULL;
    //do_bead_menu(0);
    return bead_menu;
}


int calibration_oi_post_display(O_i *oi, DIALOG *d)
{
    int i, xc, yc, oi_c;
    BITMAP *imb = NULL;
    imreg *imr = NULL;
    return 0;
    imr = (imreg *)d->dp;

    for (i = 0; i < track_info->n_b; i++)
    {
        if (track_info->bd[i]->calib_im == oi)
        {
            imb = (BITMAP *)oi_TRACK->bmp.stuff;

            if (oi_TRACK->need_to_refresh & BITMAP_NEED_REFRESH)
            {
                oi_c = imr->cur_oi;
                imr->one_i = oi_TRACK;
                display_image_stuff_16M(imr_TRACK, d_TRACK);
                oi_TRACK->need_to_refresh |= INTERNAL_BITMAP_NEED_REFRESH;
                show_bead_cross(imb, imr_TRACK, d_TRACK);
                imr->one_i = imr->o_i[oi_c];
            }

            xc = track_info->bd[i]->x0 - 100;
            yc = oi_TRACK->im.ny - track_info->bd[i]->y0 - 100;
            acquire_bitmap(plt_buffer);
            blit(imb, plt_buffer, xc, yc, SCREEN_W - 300, 80, 200, 200);
            release_bitmap(plt_buffer);
            oi_TRACK->need_to_refresh &= ~INTERNAL_BITMAP_NEED_REFRESH;
        }
    }

    return 0;
}

int calibration_oi_idle_action(struct  one_image *oi, DIALOG *d)
{
    // we put in this routine all the screen display stuff
    imreg *imr = NULL;
    //BITMAP *imb;
    b_track *bt = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    int bead_n = -1, i;
    int last_c_i = -1;
    float tmp;//, zm;

    if (d->dp == NULL)
    {
        return 1;
    }

    imr = (imreg *)d->dp;       /* the image region is here */

    if (imr->one_i == NULL || imr->n_oi == 0)
    {
        return 1;
    }

    if (oi->im.source != NULL && sscanf(oi->im.source, "equally spaced reference profile\nBead%d", &bead_n) == 1)
    {
        bt = track_info->bd[bead_n];
    }

    last_c_i = track_info->c_i - 1;
    last_c_i = (last_c_i < 0) ? TRACKING_BUFFER_SIZE - 1 : last_c_i;
    op = find_oi_cur_op(oi);

    if (op != NULL && bt != NULL && op->n_dat > 1 && bt->calib_im_fil != NULL)
    {
        ds = op->dat[0];

        /*
           if (ds != NULL && sscanf(ds->source,"Calibration nearest profile %d",&prof_n) == 1)
           {
           if (bt->profile_index != prof_n)
           {
           }
           }
           set_ds_source(ds, "Calibration nearest profile -1       ");
           */
        //zm = (bt->z[last_c_i] - bt->calib_im_fil->ay) / bt->calib_im_fil->dy;
        for (i = 0; i < ds->nx; i++)
        {
            ds->xd[i] = i;
            ds->yd[i] = bt->profile_index;
        }

        ds = op->dat[1];

        for (i = 0, tmp = 0; i < ds->nx && i < bt->cl; i++)
        {
            if (i < bt->cl / 4 || i >= 3 * bt->cl / 4)
            {
                tmp += bt->rad_prof[last_c_i][i];
            }
        }

        tmp /= bt->cl / 2;

        for (i = 0; i < ds->nx && i < bt->cl; i++)
        {
            ds->xd[i] = i;
            ds->yd[i] = bt->profile_index + (bt->rad_prof[last_c_i][i] - tmp) / 32;
            //ds->yd[i] = zm + (bt->rad_prof[last_c_i][i] - tmp)/32;
        }

        do_one_image(imr);
        oi->need_to_refresh |= PLOTS_NEED_REFRESH;
    }

    if (oi->need_to_refresh & BITMAP_NEED_REFRESH)
    {
        display_image_stuff_16M(imr, d);
        oi->need_to_refresh |= INTERNAL_BITMAP_NEED_REFRESH;
    }

    //imb = (BITMAP*)oi->bmp.stuff;

    if (oi->need_to_refresh & (INTERNAL_BITMAP_NEED_REFRESH | PLOTS_NEED_REFRESH))
    {
        //screen_used = 1;
        write_and_blit_im_box(plt_buffer, d, imr);
        /*
           SET_BITMAP_IN_USE(oi);
           acquire_bitmap(screen);
           blit(imb,screen,0,0,imr->x_off + d->x, imr->y_off -
           imb->h + d->y, imb->w, imb->h);
           release_bitmap(screen);
        //screen_used = 0;
        oi->need_to_refresh &= ~INTERNAL_BITMAP_NEED_REFRESH;
        SET_BITMAP_NO_MORE_IN_USE(oi);
        */
    }

    /*
       for (i = 0; i < track_info->n_b; i++)
       {
       if (track_info->bd[i]->calib_im == oi)
       {
       imb = (BITMAP*)oi_TRACK->bmp.stuff;
       if (oi_TRACK->need_to_refresh & BITMAP_NEED_REFRESH)
       {
       display_image_stuff_16M(imr_TRACK,d_TRACK);
       oi_TRACK->need_to_refresh |= INTERNAL_BITMAP_NEED_REFRESH;
       show_bead_cross(imb, imr_TRACK, d_TRACK);
       }
       xc = track_info->bd[i]->x0 - 100;
       yc = oi_TRACK->im.ny - track_info->bd[i]->y0 - 100;
       acquire_bitmap(screen);
       blit(imb,screen,xc,yc,SCREEN_W - 300, 80, 200, 200);
       release_bitmap(screen);
       oi_TRACK->need_to_refresh &= ~INTERNAL_BITMAP_NEED_REFRESH;
       }
       }
       */
    return 0;
}

int calibration_image_job(int im, struct future_job *job)
{
    register int i, k;
    int onx, ony, i_line, ci;
    float xb, yb, zb;
    int bnx, bcw;
    union pix  *pd = NULL;
    b_track *bt = NULL;
    calib_param *sp = NULL;
    char buf[1024] = {0};

    if (job == NULL || job->oi == NULL || job->in_progress == 0)
    {
        return 0;
    }

    if (im < job->imi)
    {
        return 0;
    }

    ci = track_info->c_i;
    bt = track_info->bd[job->bead_nb];
    sp = (calib_param *)job->more_data;
    onx = job->oi->im.nx;
    ony = job->oi->im.ny;
    i_line = job->local;
    pd = job->oi->im.pixel;

    for (k = 0; (track_info->imi[ci] >= sp->im[i_line]) && (k < TRACKING_BUFFER_SIZE); k++)
    {
        ci--;

        if (ci < 0)
        {
            ci += TRACKING_BUFFER_SIZE;
        }
    }

    if (i_line < 0 || i_line >= job->oi->im.ny)
    {
        return 0;
    }

    for (i = 0; i < onx; i++)
    {
        pd[i_line].fl[i] = 0;
    }

    for (k = 0; (track_info->imi[ci] <= job->imi) && (i_line < ony) && (k < TRACKING_BUFFER_SIZE); k++)
    {
        if ((track_info->imi[ci] > (sp->im[i_line] + sp->dead))//detecte intervalle d'indice de frame qu'on veut moyenner
                && (track_info->imi[ci] < sp->im[i_line + 1]))
        {
            for (i = 0; i < onx; i++)
            {
                pd[i_line].fl[i] += bt->rad_prof[ci][i];
            }
        }
        else if (track_info->imi[ci] >= sp->im[i_line + 1])
        {
            for (i = 0; i < onx; i++)
            {
                pd[i_line].fl[i] /= (sp->nper - sp->dead - 1);
            }

            snprintf(buf, 1024, "calibration %d %d/%d Zobj %6.3f Zmag %6.3f mm ", n_calib, job->local, ony, sp->zo[i_line],
                     sp->zmag_cal);
            job->local++;
            my_set_window_title("%s",buf);
            snprintf(calibration_state_disp, sizeof(calibration_state_disp), "calibration %d %d/%d Zobj %6.3f Zmag %6.3f mm ",
                     n_calib, job->local, ony, sp->zo[i_line], sp->zmag_cal);
            imr_TRACK_title_change_asked++;
        }

        ci++;

        if (ci >= TRACKING_BUFFER_SIZE)
        {
            ci -= TRACKING_BUFFER_SIZE;
        }
    }

    job->oi->need_to_refresh |= BITMAP_NEED_REFRESH;

    //mise a jour du frame ou on declenche la prochaine acquisition
    if (job->local < ony)
    {
        job->imi = sp->im[job->local + 1];
    }
    else
    {
        job->in_progress = 0;
        bt->calib_im = job->oi;   // this is it

        if (grab_bead_info_from_calibration(bt->calib_im, &xb, &yb, &zb, &bnx, &bcw) == 0)
        {
            bt->calibration_im_x = xb;
            bt->calibration_im_y = yb;
            bt->calibration_im_xi = (int)(0.5 + (bt->calibration_im_x / oi_TRACK->dx));
            bt->calibration_im_yi = (int)(0.5 + (bt->calibration_im_y / oi_TRACK->dy));
        }

        //      while(fft_used_in_tracking);             // we avoid threads pb
        //fft_used_in_dialog = 1;
        bt->calib_im_fil = image_band_pass_in_real_out_complex(NULL, job->oi, 0, bt->bp_center, bt->bp_width, bt->rc, bt->z_trp,
                           bt->z_fil);

        //fft_used_in_dialog = 0;
        if (bt->calib_im_fil == NULL)
        {
            return win_printf_OK("Can't compute calibration image");
        }

        // new we adjust after calibration
        // fill_next_available_job_spot(im + 2, im+2, GRAB_CALIBRATION_IMAGE, job->bead_nb,job->imr, job->oi, NULL, NULL, (void*)sp, adj_calibration_image_job, 0);
        snprintf(calibration_state_disp, sizeof(calibration_state_disp),
                 "Adjusting zero reference of calibration %d Zmag %6.3f mm ", n_calib, sp->zmag_ground);
        imr_TRACK_title_change_asked++;
    }

    return 0;
}

//fill_next_available_job_spot(im+nf+32+sp->zero_dead, im+nf+32+sp->zero_dead+n_zerof, GRAB_CALIBRATION_IMAGE, i, imr, oic, NULL, NULL, (void*)sp, adj_calibration_image_job, 0);

int adj_calibration_image_job(int im, struct future_job *job)
{
    int nmean, ci, ci_m, k, i_zero = 0, i_op;
    b_track *bt = NULL;
    calib_param *sp = NULL;
    char buf[1024] = {0}, name[128] = {0};
    float zmean = 0;
    char fullfile[1024] = {0};
    O_p *op = NULL;
    d_s *ds = NULL, *dso = NULL;
    //extern int QuickSort_double(float * xd, float * yd, int l, int r);

    if (job == NULL || job->oi == NULL || job->in_progress == 0)
    {
        return 0;
    }

    if (im < job->imi)
    {
        return 0;
    }

    ci_m = ci = track_info->c_i;
    bt = track_info->bd[job->bead_nb];
    sp = (calib_param *)job->more_data;
    i_zero = job->local;

    if (bt->calib_im_fil == NULL)
    {
        return 0;    // we make sure that calibration job is finished
    }

    i_op = find_op_nb_of_source_specific_ds_in_oi(job->oi, "Calibration zero phase adjustment");

    if (i_op < 0)
    {
        op = create_and_attach_op_to_oi(job->oi, sp->n_zerof, sp->n_zerof, 0, 0);

        if (op == NULL)
        {
            return win_printf_OK("cannot create calibration plot !");
        }

        ds = op->dat[0];
        ds->nx = ds->ny = 0;
        set_ds_source(ds, "Calibration zero phase adjustment");
        create_attach_select_y_un_to_op(op, IS_METER, 0, 1, -6, 0, "\\mu m");
        create_attach_select_x_un_to_op(op, IS_SECOND, 0, (float)1 / Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");
        job->oi->cur_op = 0;
    }
    else
    {
        op = job->oi->o_p[i_op];
        ds = find_source_specific_ds_in_op(op, "Calibration zero phase adjustment");

        if (ds == NULL)
        {
            ds = op->dat[0];
        }
    }

    for (k = 0; (track_info->imi[ci] > (sp->im_start_zerof + i_zero)) && (k < TRACKING_BUFFER_SIZE); k++)
    {
        // we go back in buffer to grab point that need to be averaged
        ci--;

        if (ci < 0)
        {
            ci += TRACKING_BUFFER_SIZE;
        }
    }

    for (k = 0; ds->nx < sp->n_zerof && (k < TRACKING_BUFFER_SIZE); k++)
    {
        // we save points
        add_new_point_to_ds(ds, track_info->imi[ci], bt->z[ci]);

        if (track_info->imi[ci] == track_info->imi[ci_m])
        {
            break;
        }

        ci++;

        if (ci >= TRACKING_BUFFER_SIZE)
        {
            ci -= TRACKING_BUFFER_SIZE;
        }
    }

    i_zero = ds->nx;
    job->local = i_zero;

    if (i_zero < sp->n_zerof)
    {
        //snprintf(buf,1024,"calibration adjustment %d over %d",i_zero,sp->n_zerof);
        //my_set_window_title(buf);
        snprintf(calibration_state_disp, sizeof(calibration_state_disp), "\\pt7 cal. adj. %d over %d start %d k %d ims %d"
                 , i_zero, sp->n_zerof, sp->im_start_zerof, k, track_info->imi[ci]);
        imr_TRACK_title_change_asked++;
        set_plot_title(op, "\\pt7 cal. adj. %d over %d start %d k %d ims %d"
                       , i_zero, sp->n_zerof, sp->im_start_zerof, k, track_info->imi[ci]);
        job->oi->need_to_refresh |= PLOT_NEED_REFRESH;
        job->imi += 32;
        return 0; // we have not yet finish
    }

    for (k = 0, nmean = 0, zmean = 0; k < ds->nx; k++)
    {
        zmean += ds->yd[k];
        nmean++;
    }

    if (nmean)
    {
        zmean /= nmean;
    }

    zmean = -zmean;

    if (ref_method == 1)
    {
        dso = duplicate_data_set(ds, NULL);

        if (dso != NULL && dso->nx > 1)
        {
            QuickSort_double(dso->xd, dso->yd, 0, dso->nx - 1);
            zmean = -dso->yd[(2 * dso->nx) / 100];
            free_data_set(dso);
        }
    }

    snprintf(buf, 1024, "calibration adjustment %6.3f microns (cor)", track_info->focus_cor * zmean);
    my_set_window_title("%s",buf);
    snprintf(calibration_state_disp, sizeof(calibration_state_disp),
             "Bead %d calibration adj. %6.3f \\mu m (cor) im %d start %d",
             job->bead_nb, track_info->focus_cor * zmean, track_info->imi[ci_m], sp->im_start_zerof);
    imr_TRACK_title_change_asked++;
    set_plot_title(op, "\\stack{{Bead %d calibration}{Z adjustment of %6.3f microns (cor)}}", job->bead_nb,
                   track_info->focus_cor * zmean);
    create_attach_select_y_un_to_oi(job->oi, IS_METER, job->oi->ay - zmean, sp->zobj_step, -6, 0, "\\mu m");
    create_attach_select_y_un_to_oi(bt->calib_im_fil, IS_METER, bt->calib_im_fil->ay - zmean, sp->zobj_step, -6, 0,
                                    "\\mu m");

    if (current_write_open_path_picofile != NULL)
    {
        set_oi_path(job->oi, current_write_open_path_picofile);
        build_full_file_name(fullfile, sizeof(fullfile), current_write_open_path_picofile, job->oi->filename);
        save_one_image(job->oi, fullfile);
        set_config_int("IMAGE-GR-FILE", "last_calibration_n", track_info->n_b);
        snprintf(name, 128, "last_calibration_%d", job->bead_nb);
        set_config_string("IMAGE-GR-FILE", name, fullfile);
        dump_to_html_log_file_with_date_and_time(current_write_open_picofile,
                "Calibration image recorded for Bead %d length adjustment %6.3f microns (cor)\n%s",
                job->bead_nb, track_info->focus_cor * zmean, backslash_to_slash(fullfile));
    }

    job->in_progress = 0;
    win_title_used = 0;
    return 0;
}

int ramp_objective(void)
{
    int i, j, im;
    static int nstep_up = 20,  nstep_dwn = 20, nper = 12;
    static float zobj_start, zobj_step = 0.3, zo;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    zobj_start = prev_focus;
    Open_Pico_config_file();
    zobj_step = get_config_float("Obj_modulation", "zobj_step", 0.3);
    nstep_up = get_config_int("Obj_modulation", "nstep_up", 20);
    nstep_dwn = get_config_int("Obj_modulation", "nstep_dwn", 20);
    nper = get_config_int("Obj_modulation", "nper", 24);
    i = win_scanf("This routine modulate the objective position with a saw-tooth shape\n"
                  "Define the objective starting/ending position %8f\n"
                  "Define the objective step size %8f\n"
                  "Define the number of steps upwards %8d\n"
                  "Define the number of steps downwards %8d\n"
                  "Define the number of frame for each step %8d\n"
                  , &zobj_start, &zobj_step, &nstep_up, &nstep_dwn, &nper);

    if (i == WIN_CANCEL)
    {
        return OFF;
    }

    Open_Pico_config_file();
    set_config_float("Obj_modulation", "zobj_step", zobj_step);
    set_config_int("Obj_modulation", "nstep_up", nstep_up);
    set_config_int("Obj_modulation", "nstep_dwn", nstep_dwn);
    set_config_int("Obj_modulation", "nper", nper);
    write_Pico_config_file();
    Close_Pico_config_file();
    im = track_info->imi[track_info->c_i] + 10;

    for (i = 0, zo = zobj_start; i <= nstep_up; i++)
    {
        j = fill_next_available_action(im, MV_OBJ_ABS, zo);

        if (j < 0)
        {
            win_printf_OK("Could not add pending action!");
        }

        im += nper;
        zo += zobj_step;
    }

    for (i = 0, zo = zobj_start + nstep_up * zobj_step; i <= (nstep_up + nstep_dwn); i++)
    {
        j = fill_next_available_action(im, MV_OBJ_ABS, zo);

        if (j < 0)
        {
            win_printf_OK("Could not add pending action!");
        }

        im += nper;
        zo -= zobj_step;
    }

    for (i = 0, zo = zobj_start - nstep_dwn * zobj_step; i <= nstep_dwn; i++)
    {
        j = fill_next_available_action(im, MV_OBJ_ABS, zo);

        if (j < 0)
        {
            win_printf_OK("Could not add pending action!");
        }

        im += nper;
        zo += zobj_step;
    }

    return D_O_K;
}
int record_calibration(void)
{
    int i, j, im, k, last_c_i;
    float  tmp, xcb, ycb, zcb;
    imreg *imr;
    O_i *oi, *oic = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    b_track *bt = NULL;
    DIALOG *d = NULL;
    static int nf, nstep = 32,  dead = 2, nper = 12, n_zerof = 128, zero_dead = 10;
    static float zobj_start, zobj_step = 0.3, zmag_cal = 9, zmag_ground = 4;
    static float zmag_after = 8, rot;
    //static float z_cor = 0.878;
    float molecule_length, z_dwn;
    calib_param *sp = NULL;
    char question[2048] = {0};

    if (updating_menu_state != 0)
    {
        if (track_info->n_b == 0)
        {
            active_menu->flags |=  D_DISABLED;
        }
        else
        {
            for (i = 0, k = 0; i < track_info->n_b; i++)
            {
                bt = track_info->bd[i];
                k += (bt->calib_im == NULL) ? 1 : 0;
            }

            if (k)
            {
                active_menu->flags &= ~D_DISABLED;
            }
            else
            {
                active_menu->flags |=  D_DISABLED;
            }
        }

        return D_O_K;
    }

    if (ac_grep(cur_ac_reg, "%im%oi", &imr, &oi) != 2)
    {
        return OFF;
    }

    for (i = 0, k = 1; i < track_info->n_b; i++)
    {
        bt = track_info->bd[i];

        if ((bt->calib_im == NULL) && (bt->not_lost > 0) && (bt->in_image > 0))
        {
            k = (bt->fixed_bead == 0) ? 0 : k;
        }
    }

    zobj_start = tmp = prev_focus; //read_Z_value_accurate_in_micron();
    rot = prev_mag_rot; //what_is_present_rot_value();
    zmag_cal = prev_zmag; // read_magnet_z_value();
    Open_Pico_config_file();
    n_calib = get_config_int("Calibration", "n_calib", n_calib);
    zobj_step = get_config_float("Calibration", "zobj_step", 0.1);
    nstep = get_config_int("Calibration", "n_step", 32);
    nper = get_config_int("Calibration", "nper", 24);
    dead = get_config_int("Calibration", "dead", 8);
    zmag_cal = get_config_float("Calibration", "zmag_cal", -0.5);
    zmag_ground = get_config_float("Calibration", "zmag_ground", 0);
    zero_dead = get_config_int("Calibration", "zero_dead", 128);
    n_zerof = get_config_int("Calibration", "n_zerof", 512);
    zmag_after = get_config_float("Calibration", "zmag_after", -0.6);
    ref_method = get_config_int("Calibration", "ref_method", 0);
    Close_Pico_config_file();
    molecule_length = Pico_param.dna_molecule.dsDNA_molecule_extension;
    z_dwn = zmag_ground - zmag_cal;
    n_calib++;

    if (k)
    {
        molecule_length = 0;
    }

    snprintf(question, sizeof(question), "{\\pt14 Recording calibration images } %%6d \n"
             "Z_{mag} present val %g angular position %g\n"
             "\\fbox{{\\color{yellow}Defining focus travel}\n"
             "starting objective at %%6f with %%4d steps of %%6f (\\mu m)\n"
             "complete period including dead %%4d dead period %%4d in video frames\n"
             "Z_{mag} for calibration %%6f molecule length %%6f}\n"
             "{\\color{yellow}Defining the Z reference}\n"
             "\\Delta Z_{mag} shift to reach F = 0 %%6f settling time to reach F = 0 %%4d\n"
             "averaging time %%4d in video frames\n"
             "Bead referencing method %%R->mean value %%r->value corresponding to 2%%%% of smallest Z\n"
             "Z_{mag} once calibration is finished %%6f\n"
             , zmag_cal, rot);
    i = win_scanf(question, &n_calib, &zobj_start, &nstep, &zobj_step, &nper, &dead,
                  &zmag_cal, &molecule_length, &z_dwn, &zero_dead, &n_zerof, &ref_method, &zmag_after);

    if (i == WIN_CANCEL)
    {
        return OFF;
    }

    zmag_ground = z_dwn + zmag_cal;
    Open_Pico_config_file();
    set_config_int("Calibration", "n_calib", n_calib);
    set_config_float("Calibration", "zobj_step", zobj_step);
    set_config_int("Calibration", "n_step", nstep);
    set_config_int("Calibration", "nper", nper);
    set_config_int("Calibration", "dead", dead);
    set_config_float("Calibration", "zmag_cal", zmag_cal);
    set_config_float("Calibration", "zmag_ground", zmag_ground);
    set_config_int("Calibration", "zero_dead", zero_dead);
    set_config_int("Calibration", "n_zerof", n_zerof);
    set_config_float("Calibration", "zmag_after", zmag_after);
    set_config_int("Calibration", "ref_method", ref_method);
    write_Pico_config_file();
    Close_Pico_config_file();
    sp = (calib_param *)calloc(1, sizeof(calib_param));

    if (sp == NULL)
    {
        win_printf_OK("Could no allocte calibration  parameters!");
    }

    sp->im = (int *)calloc(nstep + 1, sizeof(int));
    sp->zo = (float *)calloc(nstep + 1, sizeof(float));

    if (sp->im == NULL || sp->zo == NULL)
    {
        win_printf_OK("Could no allocte calibration parameters!");
    }

    sp->zobj_start = zobj_start;
    sp->zobj_step = zobj_step;
    sp->nstep = nstep;
    sp->nper = nper;
    sp->dead = dead;
    sp->n_zerof = n_zerof;
    sp->zero_dead = zero_dead;
    sp->zmag_cal = zmag_cal;
    sp->zmag_ground = zmag_ground;
    sp->zmag_after = zmag_after;
    sp->rot_pos = prev_mag_rot;//read_rot_value();

    if (current_write_open_path_picofile != NULL)
    {
        dump_to_html_log_file_with_date_and_time(current_write_open_picofile,
                "<h2>Calibration image recording</h2>"
                "<br>Focus start %g, %d sets of %g lasting %d frames after %d frames to settle<br>"
                "Zmag calibration %g Zmag zero %g recorded %d frames<br>"
                "Rotation %g<br>\n%s",
                zobj_start, nstep, zobj_step, nper, dead,
                zmag_cal, zmag_ground, zero_dead, prev_mag_rot,
                backslash_to_slash(current_write_open_path_picofile));
    }

    im = track_info->imi[track_info->c_i] + 10;
    k = fill_next_available_action(im + 4, MV_ZMAG_ABS, sp->zmag_cal);

    if (k < 0)
    {
        win_printf_OK("Could not add pending action!");
    }

    im += 64;

    for (i = 0, nf = 0; i < nstep ; i++)
    {
        sp->zo[i] = sp->zobj_start + (i * sp->zobj_step);
        sp->im[i] = im + nf;
        nf += nper;
    }

    sp->zo[i] = sp->zobj_start + (nstep / 3) * sp->zobj_step - molecule_length;
    sp->im[i] = im + nf;
    sp->im_start_zerof = im + nf + 42 + sp->zero_dead;

    for (i = 0, k = 0; i < track_info->n_b; i++)
    {
        bt = track_info->bd[i];

        if ((bt->calib_im == NULL) && (bt->not_lost > 0) && (bt->in_image > 0 || track_info->is_using_gpu))
        {
            oic = create_and_attach_oi_to_imr(imr, bt->cl, sp->nstep, IS_FLOAT_IMAGE);

            if (oic == NULL)
            {
                return win_printf_OK("cannot create calibration image !");
            }

            oic->filename = my_sprintf(oic->filename, "Calib%04dbd%01d.gr", n_calib, i);
            last_c_i = (track_info->c_i > 0) ? track_info->c_i - 1 : 0;  // c_i is not yet valid
            xcb = bt->x[last_c_i];
            ycb = bt->y[last_c_i];
            zcb = bt->z[last_c_i];
            set_oi_source(oic, "equally spaced reference profile\n"
                          "Bead%d xcb %g ycb %g zcb %g nxb %d cwb %d zstepb %g nstepb %d\n"
                          "nxprofile %d ratio in x %g ratio in y %g \n"
                          "objective %g, zoom factor %f micro_factor %f\n"
                          "field_factor %f x_over_y %f reflo %d refhi %d lamp %d\n"
                          "sample %s Rotation %g\n"
                          "ICPCI Acq_freq %g Hz Z magnets hi %g low %g mm\n"
                          "Forget circle %d bp_filter %d bp_width %d\n"
                          "Ver : %s",
                          i, xcb * oi->dx, ycb * oi->dy, zcb, bt->cl, track_info->cw, sp->zobj_step, sp->nstep,
                          bt->npc, bt->apx, bt->apy,
                          Pico_param.obj_param.objective_magnification,
                          Pico_param.micro_param.zoom_factor,
                          Pico_param.micro_param.microscope_factor,
                          Pico_param.micro_param.field_factor,
                          Pico_param.camera_param.pixel_w_in_microns / Pico_param.camera_param.pixel_h_in_microns,
                          0, 255, 255,
                          pico_sample, sp->rot_pos,
                          Pico_param.camera_param.camera_frequency_in_Hz, sp->zmag_cal, sp->zmag_ground,
                          bt->rc, bt->bp_center, bt->bp_width,
                          "TrackBead");
            create_attach_select_y_un_to_oi(oic, IS_METER, tmp, sp->zobj_step, -6, 0, "\\mu m");
            create_attach_select_x_un_to_oi(oic, IS_METER, -(oic->im.nx / 2)*oi->dy,
                                            oi->dy, -6, 0, "\\mu m");
            //create_attach_select_x_un_to_oi(oic, IS_METER, -(oidp->im.nx/2)*oi->dy/ay,
            //oi->dy/ay, -6, 0,"\\mu m");
            op = create_and_attach_op_to_oi(oic, bt->cl, bt->cl, 0, IM_SAME_Y_AXIS | IM_SAME_X_AXIS);

            if (oic == NULL)
            {
                return win_printf_OK("cannot create calibration image !");
            }

            ds = op->dat[0];
            set_ds_source(ds, "Calibration nearest profile -1       ");

            if ((ds = create_and_attach_one_ds(op, bt->cl, bt->cl, 0)) == NULL)
            {
                return win_printf_OK("I can't create plot !");
            }

            set_ds_source(ds, "Bead profile");
            set_zmin_zmax_values(oic, oi->z_min, oi->z_max);
            set_z_black_z_white_values(oic, oi->z_black, oi->z_white);
            oic->oi_idle_action = calibration_oi_idle_action;
            oic->oi_post_display = calibration_oi_post_display;
            fill_next_available_job_spot(im, sp->im[1], GRAB_CALIBRATION_IMAGE, i, imr, oic, NULL, NULL, (void *)sp,
                                         calibration_image_job, 0);
            oic->cur_op = 0;
            // moved to the end of calibration to be sure that the filtered imaged has been allocated
            //fill_next_available_job_spot(im+nf+32+sp->zero_dead, im+nf+32+sp->zero_dead+n_zerof, GRAB_CALIBRATION_IMAGE, i, imr, oic, NULL, NULL, (void*)sp, adj_calibration_image_job, 0);
            fill_next_available_job_spot(sp->im_start_zerof , sp->im_start_zerof + i % 32, GRAB_CALIBRATION_IMAGE, i, imr, oic,
                                         NULL, NULL, (void *)sp, adj_calibration_image_job, 0);
            k++;
        }
    }

    for (i = 0; i <= nstep; i++)
    {
        j = fill_next_available_action(sp->im[i], MV_OBJ_ABS, sp->zo[i]);

        if (j < 0)
        {
            win_printf_OK("Could not add pending action!");
        }
    }

    d = find_dialog_associated_to_imr(imr, NULL);
    write_and_blit_im_box(plt_buffer, d, imr);
    k = fill_next_available_action(im + nf + 30, BEAD_CROSS_SAVED, 0);

    if (k < 0)
    {
        win_printf_OK("Could not add pending action!");
    }

    k = fill_next_available_action(im + nf + 32, MV_ZMAG_ABS, sp->zmag_ground);

    if (k < 0)
    {
        win_printf_OK("Could not add pending action!");
    }

    j = fill_next_available_action(im + nf + 52 + sp->zero_dead + n_zerof, MV_OBJ_ABS, sp->zo[3]);

    if (j < 0)
    {
        win_printf_OK("Could not add pending action!");
    }

    k = fill_next_available_action(im + nf + 60 + sp->zero_dead + n_zerof, BEAD_CROSS_RESTORE, 0);

    if (k < 0)
    {
        win_printf_OK("Could not add pending action!");
    }

    k = fill_next_available_action(im + nf + 64 + sp->zero_dead + n_zerof, MV_ZMAG_ABS, sp->zmag_after);

    if (k < 0)
    {
        win_printf_OK("Could not add pending action!");
    }

    win_title_used = 1;
    generate_imr_TRACK_title = generate_calibration_title;
    return D_O_K;
}


# ifdef NOTUSED

int record_calibration_with_generic(void)
{
    int i, j, im, k, last_c_i;
    float tmp, xcb, ycb, zcb;
    imreg *imr = NULL;
    O_i *oi = NULL, *oic = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    b_track *bt = NULL;
    DIALOG *d = NULL;
    static int nf, nstep = 32,  dead = 2, nper = 12, n_zerof = 128, zero_dead = 10;
    static float zobj_start, zobj_step = 0.3, zmag_cal = 9, zmag_ground = 4;
    static float zmag_after = 8, rot;
    //static float z_cor = 0.878;
    calib_param *sp = NULL;
    char question[4096] = {0};

    if (updating_menu_state != 0)
    {
        if (track_info->n_b == 0)
        {
            active_menu->flags |=  D_DISABLED;
        }
        else
        {
            for (i = 0, k = 0; i < track_info->n_b; i++)
            {
                bt = track_info->bd[i];
                k += (bt->calib_im == NULL && bt->generic_calib_im != NULL) ? 1 : 0;
            }

            if (k)
            {
                active_menu->flags &= ~D_DISABLED;
            }
            else
            {
                active_menu->flags |=  D_DISABLED;
            }
        }

        return D_O_K;
    }

    if (ac_grep(cur_ac_reg, "%im%oi", &imr, &oi) != 2)
    {
        return OFF;
    }

    zobj_start = tmp = prev_focus; //read_Z_value_accurate_in_micron();
    rot = prev_mag_rot; //what_is_present_rot_value();
    zmag_cal = prev_zmag; // read_magnet_z_value();
    Open_Pico_config_file();
    n_calib = get_config_float("Calibration", "n_calib", n_calib);
    zobj_step = get_config_float("Calibration", "zobj_step", 0.3);
    nstep = get_config_int("Calibration", "n_step", 32);
    nper = get_config_int("Calibration", "nper", 24);
    zmag_cal = get_config_float("Calibration", "zmag_cal", 8.5);
    zmag_ground = get_config_float("Calibration", "zmag_ground", 5);
    zero_dead = get_config_int("Calibration", "zero_dead", 32);
    n_zerof = get_config_int("Calibration", "n_zerof", 128);
    zmag_after = get_config_float("Calibration", "zmag_after", 8);
    Close_Pico_config_file();
    snprintf(question, sizeof(question), "{\\pt14 Recording calibration images}\n"
             "Z_{mag} present val %g angular position %g\n"
             "objective step between two profiles %%8f nb of steps %%8d\n"
             "number of frames averaged at each step %%8d\n"
             "Z_{mag} for calibration %%8f\n"
             "Z_{mag} for F = 0 %%8f settling time %%8d recoding during %%8d\n"
             "zmag finish %%8f\n"
             , rot, zmag_cal);
    i = win_scanf(question, &zobj_step, &nstep, &nper,
                  &zmag_cal, &zmag_ground, &zero_dead, &n_zerof, &zmag_after);

    if (i == WIN_CANCEL)
    {
        return OFF;
    }

    Open_Pico_config_file();
    set_config_float("Calibration", "zobj_step", zobj_step);
    set_config_int("Calibration", "n_step", nstep);
    set_config_int("Calibration", "nper", nper);
    set_config_float("Calibration", "zmag_cal", zmag_cal);
    set_config_float("Calibration", "zmag_ground", zmag_ground);
    set_config_int("Calibration", "zero_dead", zero_dead);
    set_config_int("Calibration", "n_zerof", n_zerof);
    set_config_float("Calibration", "zmag_after", zmag_after);
    write_Pico_config_file();
    Close_Pico_config_file();
    sp = (calib_param *)calloc(1, sizeof(calib_param));

    if (sp == NULL)
    {
        win_printf_OK("Could no allocte calibration  parameters!");
    }

    sp->im = (int *)calloc(nstep + 1, sizeof(int));
    sp->zo = (float *)calloc(nstep + 1, sizeof(float));

    if (sp->im == NULL || sp->zo == NULL)
    {
        win_printf_OK("Could no allocte calibration parameters!");
    }

    sp->zobj_start = zobj_start;
    sp->zobj_step = zobj_step;
    sp->nstep = nstep;
    sp->nper = nper;
    sp->dead = dead;
    sp->n_zerof = n_zerof;
    sp->zero_dead = zero_dead;
    sp->zmag_cal = zmag_cal;
    sp->zmag_ground = zmag_ground;
    sp->zmag_after = zmag_after;
    sp->rot_pos = prev_mag_rot; //read_rot_value();
    im = track_info->imi[track_info->c_i] + 10;
    k = fill_next_available_action(im + 4, MV_ZMAG_ABS, sp->zmag_cal);

    if (k < 0)
    {
        win_printf_OK("Could not add pending action!");
    }

    im += 64;

    for (i = 0, nf = 0; i < nstep ; i++)
    {
        sp->zo[i] = sp->zobj_start + (i * sp->zobj_step);
        sp->im[i] = im + nf;
        nf += nper;
    }

    sp->zo[i] = sp->zobj_start + (nstep / 3) * sp->zobj_step - Pico_param.dna_molecule.dsDNA_molecule_extension;
    sp->im[i] = im + nf;

    for (i = 0, k = 0; i < track_info->n_b; i++)
    {
        bt = track_info->bd[i];

        if ((bt->calib_im == NULL) && (bt->not_lost > 0) && (bt->in_image > 0))
        {
            oic = create_and_attach_oi_to_imr(imr, bt->cl, sp->nstep, IS_FLOAT_IMAGE);

            if (oic == NULL)
            {
                return win_printf_OK("cannot create calibration image !");
            }

            oic->filename = my_sprintf(oic->filename, "Calib%04dbd%01d.gr", n_calib, i);
            last_c_i = (track_info->c_i > 0) ? track_info->c_i - 1 : 0;  // c_i is not yet valid
            xcb = bt->x[last_c_i];
            ycb = bt->y[last_c_i];
            zcb = bt->z[last_c_i];
            set_oi_source(oic, "equally spaced reference profile\n"
                          "Bead%d xcb %g ycb %g zcb %g nxb %d cwb %d zstepb %g nstepb %d\n"
                          "nxprofile %d ratio in x %g ratio in y %g \n"
                          "objective %g, zoom factor %f micro_factor %f\n"
                          "field_factor %f x_over_y %f reflo %d refhi %d lamp %d\n"
                          "sample %s Rotation %g\n"
                          "ICPCI Acq_freq %g Hz Z magnets hi %g low %g mm\n"
                          "Forget circle %d bp_filter %d bp_width %d\n"
                          "Ver : %s",
                          i, xcb * oi->dx, ycb * oi->dy, zcb, bt->cl, bt->cw, sp->zobj_step, sp->nstep,
                          bt->npc, bt->apx, bt->apy,
                          Pico_param.obj_param.objective_magnification,
                          Pico_param.micro_param.zoom_factor,
                          Pico_param.micro_param.microscope_factor,
                          Pico_param.micro_param.field_factor,
                          Pico_param.camera_param.pixel_w_in_microns / Pico_param.camera_param.pixel_h_in_microns,
                          0, 255, 255,
                          pico_sample, sp->rot_pos,
                          Pico_param.camera_param.camera_frequency_in_Hz, sp->zmag_cal, sp->zmag_ground,
                          bt->rc, bt->bp_center, bt->bp_width,
                          "TrackBead");
            create_attach_select_y_un_to_oi(oic, IS_METER, tmp, sp->zobj_step, -6, 0, "\\mu m");
            create_attach_select_x_un_to_oi(oic, IS_METER, -(oic->im.nx / 2)*oi->dy,
                                            oi->dy, -6, 0, "\\mu m");
            //create_attach_select_x_un_to_oi(oic, IS_METER, -(oidp->im.nx/2)*oi->dy/ay,
            //oi->dy/ay, -6, 0,"\\mu m");
            op = create_and_attach_op_to_oi(oic, bt->cl, bt->cl, 0, IM_SAME_Y_AXIS | IM_SAME_X_AXIS);

            if (oic == NULL)
            {
                return win_printf_OK("cannot create calibration image !");
            }

            ds = op->dat[0];
            set_ds_source(ds, "Calibration nearest profile -1       ");

            if ((ds = create_and_attach_one_ds(op, bt->cl, bt->cl, 0)) == NULL)
            {
                return win_printf_OK("I can't create plot !");
            }

            set_ds_source(ds, "Bead profile");
            set_zmin_zmax_values(oic, oi->z_min, oi->z_max);
            set_z_black_z_white_values(oic, oi->z_black, oi->z_white);
            oic->oi_idle_action = calibration_oi_idle_action;
            oic->oi_post_display = calibration_oi_post_display;
            fill_next_available_job_spot(im, sp->im[1], GRAB_CALIBRATION_IMAGE, i, imr, oic, NULL, NULL, (void *)sp,
                                         calibration_image_job, 0);
            oic->cur_op = 0;
            fill_next_available_job_spot(im + nf + 32 + sp->zero_dead, im + nf + 32 + sp->zero_dead + n_zerof,
                                         GRAB_CALIBRATION_IMAGE, i, imr, oic, NULL, NULL, (void *)sp, adj_calibration_image_job, 0);
            k++;
        }
    }

    for (i = 0; i <= nstep; i++)
    {
        j = fill_next_available_action(sp->im[i], MV_OBJ_ABS, sp->zo[i]);

        if (j < 0)
        {
            win_printf_OK("Could not add pending action!");
        }
    }

    d = find_dialog_associated_to_imr(imr, NULL);
    write_and_blit_im_box(plt_buffer, d, imr);
    k = fill_next_available_action(im + nf + 30, BEAD_CROSS_SAVED, 0);

    if (k < 0)
    {
        win_printf_OK("Could not add pending action!");
    }

    k = fill_next_available_action(im + nf + 32, MV_ZMAG_ABS, sp->zmag_ground);

    if (k < 0)
    {
        win_printf_OK("Could not add pending action!");
    }

    j = fill_next_available_action(im + nf + 52 + sp->zero_dead + n_zerof, MV_OBJ_ABS, sp->zo[3]);

    if (j < 0)
    {
        win_printf_OK("Could not add pending action!");
    }

    k = fill_next_available_action(im + nf + 60 + sp->zero_dead + n_zerof, BEAD_CROSS_RESTORE, 0);

    if (k < 0)
    {
        win_printf_OK("Could not add pending action!");
    }

    k = fill_next_available_action(im + nf + 64 + sp->zero_dead + n_zerof, MV_ZMAG_ABS, sp->zmag_after);

    if (k < 0)
    {
        win_printf_OK("Could not add pending action!");
    }

    win_title_used = 1;
    return D_O_K;
}

# endif

# endif

/* @} */
