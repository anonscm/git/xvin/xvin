# name of the plug'in
NAME	    		= PlayItSam
# specific plugin flags
LCFLAGS 		=  -DJAI -msse2
# extra file to zip
ZIPALSO			= 
# name of additional source files (.c and .h are already assumed)
OTHER_FILES 		=  JAI_source JAI_2_XVIN PlayUtil action save_movie_util 
# name of libraries this plug'in depends on
LIB_DEPS    		=  Jai_Factory XVcfg alfont
# name of the dll to convert in lib.a      
OTHER_LIB_FROM_DLL 	= Jai_Factory 
XVIN_DIR    		= ../../
##################################################################################################
include $(XVIN_DIR)platform.mk	# platform specific definitions
include $(XVIN_DIR)plugin.mk 		# plugin construction rules
