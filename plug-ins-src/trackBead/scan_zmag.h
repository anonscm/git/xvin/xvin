
#ifndef _SCAN_ZMAG_H_
#define _SCAN_ZMAG_H_


typedef struct _scan_param
{
  int nf;
  int nstep;
  int dead;
  int setling;          // define the time skipped after rotation to allow bead to settle
  int nper;
  int maxnper;
  int one_way;
  int c_step;
  int adj_focus;           // 0 -> do not change focus, > 0 adjust focus to this profile
  int *im;
  int *grim;            // the general record image index
  int *rec_n;            // the general record image index
  float zstart;
  float zstep;
  float zlow;
  float z_cor;
  float lambdazmag;
  float zmag0;
  float rotation;
  float *zm;
  float *fm;
  float *zo;// the array of zobj
  float *rot;
  int is_rot;
  int do_rot;
  int fir;              // the nb of points for averaging
  int repeat; // nb of repeated cycles
  int i_repeat; // nb of repeated cycles
} scan_param;




# ifndef _SCAN_ZMAG_C_

PXV_VAR(char, pico_sample[]);
PXV_VAR(int, n_scan);
PXV_FUNC(MENU *, scan_menu,(void));
PXV_FUNC(MENU *, scan_plot_menu,(void));

# endif

# ifdef _SCAN_ZMAG_C_

char pico_sample[] = "test"; // _PXV_DLL
int n_scan = 0;// _PXV_DLL

# endif

# endif
