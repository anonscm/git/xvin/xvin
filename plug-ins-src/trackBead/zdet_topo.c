
#ifndef _ZDET_TOPO_C_
#define _ZDET_TOPO_C_

# include "allegro.h"
#ifdef XV_WIN32
# include "winalleg.h"
#endif
# include "xvin.h"





/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "../cfg_file/Pico_cfg.h"
#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "../fft2d/fftbtl32n.h"
# include "fillibbt.h"
# include "focus.h"
# include "magnetscontrol.h"
# include "trackBead.h"
# include "brown_util.h"
# include "track_util.h"
# include "scan_zmag.h"
# include "action.h"
# include "record.h"
# include "zdet.h"
# include "zdet_topo.h"

/*
  series of assay intented to check the DNA rotation state
 */

# define ENCOURS
zdet_topo_param *zdet_p = NULL;
zdet_oligo_param *zoli_p = NULL;
char ti_debug[256];



int zdet_job_z_fil_avg(int im, struct future_job *job)
{
    int i, j, k, im_start = 0, im_n, avg_stat;
    int  ims, nf, page_n, i_page, max_pos, min_pos, lastzm = -1, data_valid = 0; // , imi ci
    //b_track *bt = NULL;
    b_record *br = NULL;
    d_s *dsz = NULL, *dszf = NULL, *dszm = NULL, *dsza = NULL;
    float min = 0, max = 0, minz = 0, maxz = 0, tmp, tmp2, mean = 0;
    pltreg *pr = NULL;
    g_record *g_r = NULL;

    ac_grep(cur_ac_reg, "%pr", &pr);
    g_r = find_g_record_in_pltreg(pr);

    if (g_r == NULL || g_r != zdet_r) return 0;

    if (job == NULL || job->oi == NULL || job->op->n_dat < 3) return 0;

    //if (working_gen_record != zdet_r)  return 0;
    if (im < job->imi)  return 0;

    //if (zdet_r == NULL || zdet_r->abs_pos < 0)  return 0;

    //ci = track_info->c_i;
    dsz = job->op->dat[0];
    dszf = job->op->dat[1];
    dszm = job->op->dat[2];

    if (job->op->n_dat > 3)
        dsza = job->op->dat[3];

    //bt = track_info->bd[job->bead_nb];
    br = g_r->b_r[job->bead_nb];


    min_pos = g_r->starting_in_page_index + (g_r->starting_page * g_r->page_size);
    j = max_pos = (g_r->abs_pos < 0) ? 0 : g_r->abs_pos;

    if (job->op->user_id == 4096 && job->op->user_ispare[ISPARE_TIME_OUT] > (int)(clock() / CLOCKS_PER_SEC))
    {
        j = job->op->user_ispare[ISPARE_BUF_POSITION];

        if (j > max_pos) j = max_pos;

        if (j < min_pos) j = min_pos;
    }
    else  job->op->user_ispare[ISPARE_BUF_POSITION] = j;

    max_pos = j;

    //imi = g_r->imi[j/g_r->page_size][j%g_r->page_size]; //track_info->imi[ci];
    ims = g_r->last_starting_pos;

    j = max_pos - scope_buf_size; //dsz->mx;
    j = (j < min_pos) ? min_pos : j; //512*(1+(j/512));

    nf = max_pos - j;
    nf = (nf < dsz->mx) ? nf : dsz->mx;
    dsza->nx = dsza->ny = 0;

    for (i = 0, im_n = 0, avg_stat = 0; i < nf; i++, j++)
    {
        page_n = j / g_r->page_size;
        i_page = j % g_r->page_size;
        //if (page_n <= g_r->n_page && i_page <= g_r->in_page_index)
        dsz->xd[i] = dszm->xd[i] = g_r->imi[page_n][i_page] - ims;
        dsz->yd[i] = g_r->z_cor * br->z[page_n][i_page];

        if (zmag_or_f < 2) dszm->yd[i] = g_r->zmag[page_n][i_page];
        else dszm->yd[i] = g_r->rot_mag[page_n][i_page];

        if (g_r->action_status[page_n][i_page] & DATA_AVERAGING)
        {
            if (avg_stat == 0)
            {
                mean = 0;
                im_start = g_r->imi[page_n][i_page] - ims;
                im_n = 0;
                avg_stat = DATA_AVERAGING;
            }

            mean += g_r->z_cor * br->z[page_n][i_page];
            im_n++;
        }
        else if (avg_stat == DATA_AVERAGING)
        {
            if (im_n) mean /= im_n;

            dsza->yd[dsza->nx] = mean;
            dsza->xd[dsza->nx] = (float)(g_r->imi[page_n][i_page] - ims + im_start) / 2;
            dsza->xe[dsza->nx] = (float)(g_r->imi[page_n][i_page] - ims - im_start) / 2;
            dsza->nx++;
            dsza->ny = dsza->nx;
            avg_stat = 0;
        }

        if (zmag_or_f == 1)
        {
            if (i == 0 || lastzm != dszm->yd[i])
            {
                lastzm = dszm->yd[i];
                dszm->yd[i] = zmag_to_force(0, dszm->yd[i]);
            }
            else dszm->yd[i] = dszm->yd[i - 1];
        }

        if (i == 0)
        {
            min = max = dsz->yd[i];
            minz = maxz = dszm->yd[i];
        }

        if (g_r->status_flag[page_n][i_page] == 0)
            data_valid++;
        else data_valid = 0;

        if (data_valid > 10)
        {
            min = (dsz->yd[i] < min) ? dsz->yd[i] : min;
            minz = (dszm->yd[i] < minz) ? dszm->yd[i] : minz;
            max = (dsz->yd[i] > max) ? dsz->yd[i] : max;
            maxz = (dszm->yd[i] > maxz) ? dszm->yd[i] : maxz;
        }
    }

    dsz->nx = dsz->ny =  dszm->nx = dszm->ny = nf;

    j = ((int)(8 * dsz->xd[0])) / scope_buf_size;
    job->op->x_lo = j * scope_buf_size / 8;
    job->op->x_hi = (j + 9) * scope_buf_size / 8;
    set_plot_x_fixed_range(job->op);


    tmp = (max - min) * 0.05;
    tmp = (tmp < min_scale_z) ? min_scale_z : tmp;

    for (i = 0, tmp2 = min_scale_z; tmp > tmp2; i++)
    {
        if (i % 3 == 0) tmp2 += tmp2;
        else if (i % 3 == 1) tmp2 += tmp2;
        else tmp2 *= 2.5;
    }

    tmp = tmp2 / 2;
    min -=  tmp;
    max +=  tmp;

    if (job->op->iopt2 & Y_LIM)  // fixed range
    {
        min = job->op->y_lo;
        max = job->op->y_hi;
    }


    tmp = (maxz - minz) * 0.05;
    minz -=  6 * tmp;
    maxz +=  tmp;


    if (minz > min && maxz < max)
    {
        job->op->yu[job->op->c_yu_p]->ax = 0;
        job->op->yu[job->op->c_yu_p]->dx = 1;
    }
    else
    {
        if ((maxz - minz) < min_scale_zmag)
        {
            minz = (maxz + minz) / 2;

            if (zmag_or_f == 0)
            {
                maxz = minz + min_scale_zmag / 2;
                minz = minz - min_scale_zmag / 2;
            }
            else if (zmag_or_f == 1)
            {
                maxz = minz + min_scale_force / 2;
                minz = minz - min_scale_force / 2;
            }
            else
            {
                maxz = minz + min_scale_rot / 2;
                minz = minz - min_scale_rot / 2;
            }
        }

        tmp = (max - min) / (maxz - minz);

        for (i = 0; i < nf; i++)
        {
            dszm->yd[i] = min + (dszm->yd[i] - minz) * tmp;
        }

        if (tmp > 0)
        {
            job->op->yu[job->op->c_yu_p]->ax = minz - min / tmp;
            job->op->yu[job->op->c_yu_p]->dx = (float)1 / tmp;
        }
    }

    job->op->y_lo = min;
    job->op->y_hi = max;
    //set_plot_y_fixed_range(job->op);

    for (i = 0, j = 0, k = 0, dszf->yd[0] = dszf->xd[0] = 0; i < nf; i++)
    {
        dszf->yd[j] += dsz->yd[i];
        dszf->xd[j] += dsz->xd[i];
        k++;

        if (k >= fir)
        {
            dszf->yd[j] /= fir;
            dszf->xd[j] /= fir;
            j++;
            dszf->yd[j] = dszf->xd[j] = 0;
            k = 0;
        }
    }

    dszf->nx = dszf->ny = j;

    if (zmag_or_f == 0)
    {
        set_plot_y_prime_title(job->op, "Zmag");
        job->op->c_yu_p = 1;
    }
    else if (zmag_or_f == 1)
    {
        set_plot_y_prime_title(job->op, "Estimated force");
        job->op->c_yu_p = 2;
    }
    else
    {
        set_plot_y_prime_title(job->op, "Rotation");
        job->op->c_yu_p = 3;
    }


    //set_plot_title(job->op,"nf  %d fir %d j %d",nf,fir,j);
    job->op->need_to_refresh = 1;
    job->imi += 32;
    //else job->in_progress = 0;
    return 0;
}

/*
  This is a record action handling zmag motion
  on first step it ask for zmag motion
  then wait for zmag reaching target
  then it wait for an extra setling time
 */

int zmag_action_and_wait(int im, int c_i, float zmag, int setling)
{
    int k;
    static int status = 0, end = 0, duration = 0;
    static float z_mag;

    /*
    k = is_there_pending_action_of_type(MV_ZMAG_ABS);
    if (k >= 0)    my_set_window_title("Zdet topo pending action zmag -> %g", action_pending[k].value);
    else my_set_window_title("                                          Zdet topo zmag NO MV ZMAG pending zmag cmd %g dur %d",track_info->zmag_cmd[c_i],duration);
    */
    if (status == 0 || fabs(track_info->zmag_cmd[c_i] - zmag) > 0.005)
    {
        k = fill_next_available_action(im + 1, MV_ZMAG_ABS, z_mag = zmag);

        if (k < 0)    my_set_window_title("Could not add pending action!");

        //else my_set_window_title("Zdet topo zmag -> %g", z_mag);
        end = setling;
        duration = 0;
        status = 1;
    }
    else if (status == 1)
    {
        duration++;

        if (duration < (int)(3 * Pico_param.camera_param.camera_frequency_in_Hz))
        {
            if (fabs(track_info->zmag[c_i] - z_mag) > 0.005)
                return 0;  // we are not yet arrived
        }
        else
        {
            my_set_window_title("Pb zmag");
            status = 0;
            return 0;
        }

        end += im;
        status++;
    }
    else if (status == 2)
    {
        if (im < end)     return 0;  // we are not yet done

        status = 0;
        imr_TRACK_title_change_asked++;
        return 1;
    }

    return 0;
}

int zmag_action_with_velocity_and_wait(int im, int c_i, float zmag, float speed, int setling, int init_to_zero)
{
    int k, vzi;
    static int status = 0, end = 0, duration = 0, speed_changed = 0, duration_expected = 0, im_max = 0, transient = 8;
    static float z_mag;

    if(init_to_zero)
    {
		  status = 0; end = 0; duration = 0; speed_changed = 0; duration_expected = 0; im_max = 0, transient = 8;
		  return 0;
    }
    /*
    k = is_there_pending_action_of_type(MV_ZMAG_ABS);
    if (k >= 0) my_set_window_title("Zdet topo pending action zmag -> %g", action_pending[k].value);
    else my_set_window_title("Zdet topo zmag NO MV ZMAG pending zmag cmd %g dur %d",track_info->zmag_cmd[c_i],duration);
    */
    speed = fabs(speed);
    vzi = (int)(0.5 + 8 * fabs(speed));
    vzi = (vzi > 120) ? 120 : vzi;
    vzi = (vzi < 1) ? 1 : vzi; // motors do not like 0 velocity !
	//my_set_window_title("status %d", status);
    if (status == 0)
    {
        // we change speed if necessary
        end = setling;
        duration = 0;
        im_max = im + 5;
        if (fabs(Pico_param.translation_motor_param.translation_velocity - speed) > 0.124)
        {
			     last_zmag_pid.proceeded_ask_bo_be_clear = im;
#ifdef RAWHID_V2
//TV 120518: conversion of speed onto vzi depends on motor model
          speed = fabs(speed);
          speed = (speed>10)? 10: speed;
          k = fill_next_available_action_with_spare(im + 1, CHG_ZMAG_PID, speed, 7);
#elif defined ZMAG_APT
//TV 120518: conversion of speed onto vzi depends on motor model
          speed = fabs(speed);
          speed = (speed>2)? 2: speed;
          k = fill_next_available_action_with_spare(im + 1, CHG_ZMAG_PID, speed, 7);
#else
			     k = fill_next_available_action_with_spare(im + 1, CHG_ZMAG_PID, (float)vzi, 7);
#endif
			     if (k < 0) my_set_window_title("Could not add pending action!");
			     RT_debug("zm_a_w_w 0 changed speed to %f \n", speed);
			     status = 1;
			     speed_changed = 1;
        }
        else
        {
			    RT_debug("zm_a_w_w 0 speed is fine %f \n", speed);
			    status = 2;
			    speed_changed = 0;
        }
    }
    else if (status == 1)
    {
        if (last_zmag_pid.proceeded_RT || im > im_max) // we wait for speed change
	      {
	        status = 2;
	        RT_debug("zm_a_w_w 1 speed changed asked \n");
	      }
    }
    else if (status == 2)
    {
        // we ask change in zmag
      RT_debug("zm_a_w_w 2 zmag chg changed ack \n");
      end = setling;
      if (fabs(track_info->zmag_cmd[c_i] - zmag) > 0.005)
	    {
	      k = fill_next_available_action(im + 1, MV_ZMAG_ABS, z_mag = zmag);
	      if (k < 0)    my_set_window_title("Could not add pending action!");
        //else my_set_window_title("Zdet topo zmag -> %g", z_mag);
	      if (speed > 0) duration_expected = (int)Pico_param.camera_param.camera_frequency_in_Hz * fabs(track_info->zmag_cmd[c_i] - zmag) /speed;
	      duration_expected += 2;
	      end = duration_expected + setling;
	      duration = 0;
	     }
      status = 3;
    }
    else if (status == 3) // we wait to reach position
    {
        duration++;
	      RT_debug("zm_a_w_w 3 waiting %d/%d\n",duration,end);
        if (duration < end)
        {
	        if (fabs(track_info->zmag[c_i] - z_mag) > 0.005)
	        {
		        transient = 8;
            return 0;  // we are not yet arrived
	         }
	        else if (transient-- > 0)
          {
            return 0;
          }
        }
        else
        {
            my_set_window_title("Pb zmag in zmag with vel and wait : too long, duration %d end %d spped %3.2f steling %3.2f duration %3.2f", duration, end,(float)speed, (float)setling,(float)duration_expected);
            status = 4;
            return 0;
        }
        status = (speed_changed == 0) ? 6 : 4;
    }
    else if (status == 4)
    {
        // we ask for zmag speed
        //to do : the same when apt driven
	      if (find_action_to_be_done_by_type(READ_ZMAG_PID) < 0)
	      {
	        last_zmag_pid.proceeded_ask_bo_be_clear = im;
          //tv add-on : read vel from apt driven zmag
	        k = fill_next_available_action_with_spare(im + 1, READ_ZMAG_PID, (float)vzi, 7);
          if (k < 0)
	        {
		          my_set_window_title("Could not add pending action!");
		          RT_debug("zm_a_w_w 4 could not ask speed\n");
	        }
	        else RT_debug("zm_a_w_w 4 asking speed\n");
	        im_max = im + 5;
	        status = 5;
	       }
	      else RT_debug("zm_a_w_w 4 already a read zmag in action\n");
    }
    else if (status == 5)
    {
        // we get the zmag speed
      RT_debug("zm_a_w_w 5 waiting speed\n");
      if (last_zmag_pid.proceeded_RT)
      {
	      Pico_param.translation_motor_param.translation_velocity = (float)last_zmag_pid.value / 8;
#ifdef ZMAG_APT
        Pico_param.translation_motor_param.translation_velocity = (float)last_zmag_pid.value;
#endif
	      speed_changed = 0;
	      status = 6;
      }
      if (last_zmag_pid.proceeded_RT || im > im_max) status = 6;
    }
    else if (status == 6)
    {
      RT_debug("zm_a_w_w 6 finished\n");
      if (im < end)     return 0;  // we are not yet done
      status = 0;
      //imr_TRACK_title_change_asked++;
      return 1;
    }
    else
    {
        status = 0;
        return 1;
    }

    return 0;
 }


# ifdef OLD

int zmag_speed_action_for_next_move(int im, int c_i, float speed)
{
    int k, vzi;
    static int status = -2, pb_reading_vzmag = 0, im_max = 0; // , speed_changed = 0

    (void)c_i;
    speed = fabs(speed);
    vzi = (int)(0.5 + 8 * fabs(speed));
    vzi = (vzi < 1) ? 1 : vzi; // motors do not like 0 velocity !
#ifdef ZMAG_APT
    speed = (speed > 0) ? speed : 1;
#endif
    if (status == -2)
    {
        im_max = im + 5;
	im_max = (im > last_zmag_pid.acknowledge) ? im : last_zmag_pid.acknowledge + 1;
        last_zmag_pid.proceeded_ask_bo_be_clear = im_max;
	status = -1;
    }
    else if (status == -1)
    {
      if ((volatile int)last_zmag_pid.proceeded_RT == 0)
	status = 0;
    }
    else if (status == 0)
    {
        im_max = im + 5;
        k = fill_next_available_action_with_spare(im + 1, READ_ZMAG_PID, (float)vzi, 7);

        if (k < 0)    my_set_window_title("Could not add pending action!");

        im_max = im + 5;
        status = 1;
    }
    else if (status == 1)
    {
        // we get the zmag speed
        if ((volatile int)last_zmag_pid.proceeded_RT)
        {
            Pico_param.translation_motor_param.translation_velocity = (float)last_zmag_pid.value / 8;
            //speed_changed = 0;
            pb_reading_vzmag = 0;
            status = 2;
        }

        if (last_zmag_pid.proceeded_RT || im > im_max)
        {
            pb_reading_vzmag = 1;
            status = -2;
        }
    }
    else if (status == 2)
    {
        if ((pb_reading_vzmag) || (fabs(Pico_param.translation_motor_param.translation_velocity - speed) > 0.124))
        {
            last_zmag_pid.proceeded_RT = 0;
            k = fill_next_available_action_with_spare(im + 1, CHG_ZMAG_PID, (float)vzi, 7);
            if (k < 0)    my_set_window_title("Could not add pending action!");
            status = 3;
        }
        else
        {
            status = 4;
            //speed_changed = 0;
        }
    }
    else if (status == 3)
    {
        // we wait for speed change
        if (last_zmag_pid.proceeded_RT || im > im_max) status = 2;
    }
    else if (status == 4)
    {
        last_zmag_pid.proceeded_ask_bo_be_clear = im;
        k = fill_next_available_action_with_spare(im + 1, READ_ZMAG_PID, (float)vzi, 7);

        if (k < 0)    my_set_window_title("Could not add pending action!");

        im_max = im + 5;
        status = 5;
    }
    else if (status == 5)
    {
        if (last_zmag_pid.proceeded_RT)
        {
            Pico_param.translation_motor_param.translation_velocity = (float)last_zmag_pid.value / 8;
#ifdef ZMAG_APT
            Pico_param.translation_motor_param.translation_velocity = (float)last_zmag_pid.value ;
#endif
            //speed_changed = 0;
            status = 6;
        }

        if (last_zmag_pid.proceeded_RT || im > im_max) status = 6;
    }
    else if (status >= 6)
    {
        status = -2;
        return 1;
    }
    else
    {
        status = -2;
        return 1;
    }

    return 0;
}

# endif

int zmag_speed_action_for_next_move(int im, int c_i, float speed, int init_to_zero)
{
  int k, vzi, l;
    static int status = 0, pb_reading_vzmag = 0, im_max = 0; // , speed_changed = 0

    if (init_to_zero)
    {
	    status = 0; pb_reading_vzmag = 0; im_max = 0;
	    return 0;
    }
    (void)c_i;
    speed = fabs(speed);
    vzi = (int)(0.5 + 8 * fabs(speed));
    vzi = (vzi < 1) ? 1 : vzi; // motors do not like 0 velocity !
#if defined(ZMAG_APT) || defined(RAWHID_V2)
    speed = (speed > 0) ? speed : 0.4;
#endif
    if (status ==  0)
    {
      if (fabs(Pico_param.translation_motor_param.translation_velocity - speed) < 0.124)
	    {
	      RT_debug("zm_s_a_f_n_m 0 the speed is already set\n");
	      return 1;  // the speed is already set
	    }
      else   // we need to change it and start a cycle
	    { // we insure that we get a new read
        //to do : with apt driven motor
	      if ((l = find_action_to_be_done_by_type(READ_ZMAG_PID)) < 0)
	      {
	         im_max = (im > last_zmag_pid.acknowledge) ? im : last_zmag_pid.acknowledge + 1;
	         last_zmag_pid.proceeded_ask_bo_be_clear = im_max;
	         status = 1;
	         im_max = im + 5;
	         RT_debug("zm_s_a_f_n_m 0 clear last_zmag_pid\n");
	        }
	        else
	         {
	            RT_debug("zm_s_a_f_n_m 0 waiting for read_zmag_pid at im %dstatus %d \n",action_pending[l].imi,action_pending[l].proceeded);
	             status = 3;
	            }
	    }
    }
    else if (status == 1)
      {   // we wait for clearing flag
	RT_debug("zm_s_a_f_n_m 1 wait  last_zmag_pid cleared\n");
	if ((volatile int)last_zmag_pid.proceeded_RT == 0)
	  status = 2;
      }
    else if (status == 2)
    {
      im_max = im + 5;
      if ((l = find_action_to_be_done_by_type(READ_ZMAG_PID)) < 0)
	{
	  k = fill_next_available_action_with_spare(im + 1, READ_ZMAG_PID, (float)vzi, 7);

	  if (k < 0)
	    {
	      my_set_window_title("Could not add pending action!");
	      RT_debug("zm_s_a_f_n_m 2 Could not add pending action to initiate speed read\n");

	      status = 2;
	    }
	  else
	    {
	      status = 3;
	      RT_debug("zm_s_a_f_n_m 2 initiate speed read\n");
	    }
	  im_max = im + 5;
	}
    }
    else if (status == 3)
    {
        // we get the zmag speed
      	RT_debug("zm_s_a_f_n_m 3 wait speed read\n");
        if ((volatile int)last_zmag_pid.proceeded_RT)
        {
            Pico_param.translation_motor_param.translation_velocity = (float)last_zmag_pid.value / 8;
#ifdef ZMAG_APT
            Pico_param.translation_motor_param.translation_velocity = (float)last_zmag_pid.value;
#endif
            //speed_changed = 0;
            pb_reading_vzmag = 0;
            status = 4;
        }
        if (last_zmag_pid.proceeded_RT && im > im_max)
        {
            pb_reading_vzmag = 1;
            status = 4;
        }
    }
    else if (status == 4)
    {
        if ((pb_reading_vzmag) || (fabs(Pico_param.translation_motor_param.translation_velocity - speed) > 0.124))
        {
	  if ((l = find_action_to_be_done_by_type(CHG_ZMAG_PID)) < 0)
	    {
	      last_zmag_pid.proceeded_RT = 0;
	      RT_debug("zm_s_a_f_n_m 4 change speed to %f\n",speed);
#if defined(ZMAG_APT) || defined(RAWHID_V2)
            k = fill_next_available_action_with_spare(im + 1, CHG_ZMAG_PID, speed, 7);
            my_set_window_title("zmag vel change asked by zmag speed action for next move %f", speed );
#else
	      k = fill_next_available_action_with_spare(im + 1, CHG_ZMAG_PID, (float)vzi, 7);
#endif

	      if (k < 0)    my_set_window_title("Could not add pending action!");

	      status = 5;
            //speed_changed = 1;
	    }
        }
        else
        {
	  RT_debug("zm_s_a_f_n_m 4 we ask for speed read\n");
            status = 0;
            //speed_changed = 0;
        }
    }
    else if (status == 5)
    {
        // we wait for speed change
      	  RT_debug("zm_s_a_f_n_m 5 we wait for speed change\n");
        if (last_zmag_pid.proceeded_RT) status = 0;
    }
    else if (status >= 6)
    {
        status = 0;
        return 1;
    }
    else
    {
        status = 0;
        return 1;
    }

    return 0;
}




/*
  This is a record action handling relative rotation motion
  on first step it ask for  relative rotation motion
  then wait for rotation reaching target
  restore bead position
  then it wait for an extra setling time
 */
int rotation_relative_action_and_wait(int im, int c_i, float dr, int setling)
{
    int k;
    static int status = 1, end = 0;
    static float rot;

    if (status == 1)
    {
        rot = track_info->rot_mag[c_i] + dr;
        k = fill_next_available_action(im + 1, MV_ROT_ABS, rot);

        if (k < 0)    my_set_window_title("Could not add pending action!");

        end = setling;
        status++;
    }
    else if (status == 2)
    {
        if (fabs(track_info->rot_mag[c_i] - rot) > 0.005)
            return 0;  // we are not yet arrived

        status++;
    }
    else if (status == 3)
    {
        k = fill_next_available_action(im + 1, BEAD_CROSS_RESTORE, 0);

        if (k < 0)    my_set_window_title("Could not add pending action!");

        end = im + 2 + setling;
        status++;
    }
    else if (status == 4)
    {
        if (im < end)     return 0;  // we are not yet done

        status = 1;
        imr_TRACK_title_change_asked++;
        return 1;
    }

    return 0;
}

/*
  This is a record action handling rotation motion
  on first step it ask for rotation motion
  then wait for rotation reaching target
  restore bead position
  then it wait for an extra setling time
 */

int rotation_action_and_wait(int im, int c_i, float r, int setling, int init_to_zero)
{
    int k;
    static int status = 1, end = 0;
    static float rot;

    /*
    if (status == 0)
      {
        k = fill_next_available_action(im+1, BEAD_CROSS_SAVED, 0);
        if (k < 0)    my_set_window_title("Could not add pending action!");
        end = im+2;
        status++;
      }
    */
    //else
    if (init_to_zero)
      {
	status = 1;
	end = 0;
	return 0;
      }
    if (status == 1)
    {
        //if (im < end)   return 0;  // we are not yet done
        k = fill_next_available_action(im + 1, MV_ROT_ABS, rot = r);

        if (k < 0)    my_set_window_title("Could not add pending action!");

        //else my_set_window_title("Zdet topo rotation -> %g", rot);
        end = setling;
        status++;
    }
    else if (status == 2)
    {
        if (fabs(track_info->rot_mag[c_i] - rot) > 0.005)
            return 0;  // we are not yet arrived

        status++;
    }
    else if (status == 3)
    {
        k = fill_next_available_action(im + 1, BEAD_CROSS_RESTORE, 0);

        if (k < 0)    my_set_window_title("Could not add pending action!");

        end = im + 2 + setling;
        status++;
    }
    else if (status == 4)
    {
        if (im < end)     return 0;  // we are not yet done

        status = 1;
        imr_TRACK_title_change_asked++;
        return 1;
    }

    return 0;
}


int average_Z_action(int im, int c_i, int n_bead, zdet_avg *z_avg, g_record *g_r, int init_to_zero)
{
    static int status = 0;
    int  page_n, i_page, abs_pos = 0;


    (void)c_i;
    if (init_to_zero)
      {
	status = 0;
	return 0;
      }
    abs_pos = (g_r->abs_pos < 0) ? 0 : g_r->abs_pos;
    page_n = abs_pos / g_r->page_size;
    i_page = abs_pos % g_r->page_size;


    if (status == 0)
    {
        z_avg->ending_im = im + z_avg->nb_frames_in_avg + 2;
        z_avg->rec_start = abs_pos;
        g_r->action_status[page_n][i_page] |= DATA_AVERAGING;
        status++;
    }
    else if (status == 1)
    {
        if (im < z_avg->ending_im)    return 0;  // we are not yet done

        z_avg->rec_end = abs_pos;
        g_r->action_status[page_n][i_page] &= ~DATA_AVERAGING;
        z_avg->zbead_valid = 0;

        if (average_bead_z_during_part_trk(g_r, n_bead, z_avg->rec_start, z_avg->nb_frames_in_avg,
                                           &(z_avg->xbead), &(z_avg->ybead), &(z_avg->zbead),
                                           &(z_avg->zobj), &(z_avg->profile_invalid)) < 0)
            my_set_window_title("Could not grab bead ref position im %d !", z_avg->ending_im);
        else
        {
            z_avg->zbead_valid = 1;
            my_set_window_title("Data averaging Z = %g", z_avg->zbead);
        }

        imr_TRACK_title_change_asked++;
        status = 0;
        return 1;
    }

    return 0;
}

int zdet_stay_in_range_action(int im, int c_i,  zdet_stay_in_range *z_dt, g_record *g_r, zdet_avg *z_ref)
{
    int  page_n, i_page, abs_pos = 0, end = 0;
    b_record *b_r = NULL;

    (void)c_i;
    abs_pos = (g_r->abs_pos < 0) ? 0 : g_r->abs_pos;
    page_n = abs_pos / g_r->page_size;
    i_page = abs_pos % g_r->page_size;
    b_r = g_r->b_r[z_dt->n_bead];

    if (z_dt->status == 0)
    {
        z_dt->ending_im = im;
        z_dt->rec_start = abs_pos;
        g_r->action_status[page_n][i_page] |= DATA_AVERAGING;
        z_dt->status++;
    }
    else if (z_dt->status == 1)
    {
        if (z_dt->direction >= 0)
        {
            if (g_r->z_cor * ((b_r->z[page_n][i_page] - z_ref->zbead)) > z_dt->z_threshold)     end = 1;
        }
        else
        {
            if (g_r->z_cor * ((b_r->z[page_n][i_page]  - z_ref->zbead)) < z_dt->z_threshold)        end = 1;
        }

        if (end == 0)     return 0;  // we are not yet done

        z_dt->rec_end = abs_pos;
        z_dt->nb_frames_in_avg = z_dt->rec_end - z_dt->rec_start;
        g_r->action_status[page_n][i_page] &= ~DATA_AVERAGING;
        z_dt->zbead_valid = 0;

        if (z_dt->nb_frames_in_avg <= 0)
        {
            z_dt->nb_frames_in_avg = 1;
            z_dt->rec_end = z_dt->rec_start + 1;
        }

        z_dt->ending_im += z_dt->nb_frames_in_avg;

        if (average_bead_z_during_part_trk(g_r, z_dt->n_bead, z_dt->rec_start, z_dt->nb_frames_in_avg,
                                           &(z_dt->xbead), &(z_dt->ybead), &(z_dt->zbead),
                                           &(z_dt->zobj), &(z_dt->profile_invalid)) < 0)
            my_set_window_title("Could not grab bead ref position im %d !", z_dt->ending_im);
        else
        {
            z_dt->zbead_valid = 1;
            my_set_window_title("Data averaging Z = %g", g_r->z_cor * z_dt->zbead);
        }

        imr_TRACK_title_change_asked++;
        z_dt->status = 0;
        return 1;
    }

    return 0;
}

int add_new_point_to_resu(int plot_n, zdet_avg *z_avg)
{
    if (zdet_p->resu != NULL && zdet_p->resu->n_dat > plot_n && zdet_p->resu->dat[plot_n] != NULL)
    {
        add_new_point_to_ds(zdet_p->resu->dat[plot_n], zdet_p->resu->dat[plot_n]->nx,
                            zdet_p->z_cor * z_avg->zbead);
        zdet_p->resu->need_to_refresh = 1;
        return 0;
    }

    return 0;
}


int add_new_point_to_oli_resu(int plot_n, zdet_avg *z_avg)
{
    if (zoli_p->resu != NULL && zoli_p->resu->n_dat > plot_n && zoli_p->resu->dat[plot_n] != NULL)
    {
        add_new_point_to_ds(zoli_p->resu->dat[plot_n], zoli_p->resu->dat[plot_n]->nx,
                            zoli_p->z_cor * z_avg->zbead);
        zoli_p->resu->need_to_refresh = 1;
        return 0;
    }

    return 0;
}






int add_new_point_to_oli_time_lapse(int plot_n, zdet_stay_in_range *z_dt)
{
    if (zoli_p->resu_dt != NULL && zoli_p->resu_dt->n_dat > plot_n && zoli_p->resu_dt->dat[plot_n] != NULL)
    {
        add_new_point_to_ds(zoli_p->resu_dt->dat[plot_n], zoli_p->resu_dt->dat[plot_n]->nx,
                            z_dt->nb_frames_in_avg);
        zoli_p->resu_dt->need_to_refresh = 1;
        return 0;
    }

    return 0;
}
int add_new_point_to_oli_resu_dt(int plot_n, zdet_stay_in_range *z_dt)
{
    if (zoli_p->resu != NULL && zoli_p->resu->n_dat > plot_n && zoli_p->resu->dat[plot_n] != NULL)
    {
        add_new_point_to_ds(zoli_p->resu->dat[plot_n], zoli_p->resu->dat[plot_n]->nx,
                            zoli_p->z_cor * z_dt->zbead);
        zoli_p->resu->need_to_refresh = 1;
        return 0;
    }

    return 0;
}



int add_new_point_to_resu_valid(int plot_n, float rot)
{
    if (zdet_p->resu_valid != NULL && zdet_p->resu_valid->n_dat > plot_n && zdet_p->resu_valid->dat[plot_n] != NULL
            && zdet_p->za_after.profile_invalid == 0 && zdet_p->za_after.zbead_valid
            && zdet_p->za_hat.profile_invalid == 0 && zdet_p->za_hat.zbead_valid)
    {
        add_new_point_to_ds(zdet_p->resu_valid->dat[plot_n], rot,
                            zdet_p->z_cor * (zdet_p->za_after.zbead - zdet_p->za_hat.zbead));
        zdet_p->resu_valid->need_to_refresh = 1;
        return 0;
    }

    return 0;
}



int add_new_point_to_resu_valid_2(int plot_n, float rot, float dr)
{
    if (zdet_p->resu_valid != NULL && zdet_p->resu_valid->n_dat > plot_n && zdet_p->resu_valid->dat[plot_n] != NULL
            && zdet_p->za_after.profile_invalid == 0 && zdet_p->za_after.zbead_valid
            && zdet_p->za_hat.profile_invalid == 0 && zdet_p->za_hat.zbead_valid)
    {
        add_new_point_to_ds(zdet_p->resu_valid->dat[plot_n], rot, dr);
        zdet_p->resu_valid->need_to_refresh = 1;
        return 0;
    }

    return 0;
}

int add_new_point_to_resu_hat(int plot_n, float rot, float dr)
{
    if (zdet_p->resu_hat != NULL && zdet_p->resu_hat->n_dat > plot_n && zdet_p->resu_hat->dat[plot_n] != NULL
            && zdet_p->za_after.profile_invalid == 0 && zdet_p->za_after.zbead_valid
            && zdet_p->za_hat.profile_invalid == 0 && zdet_p->za_hat.zbead_valid)
    {
        add_new_point_to_ds(zdet_p->resu_hat->dat[plot_n], rot, dr);
        zdet_p->resu_hat->need_to_refresh = 1;
        return 0;
    }

    return 0;
}

/*
  save in the recod status the number of the event and its phase
 */

int record_event_number_and_phase(g_record *g_r, int im, int nb, int phase)
{
    int  page_n, i_page, abs_pos;

    if (g_r == NULL)  return 1;

    abs_pos = (g_r->abs_pos < 0) ? 0 : g_r->abs_pos;

    if (im < 0 || im >  abs_pos)  return 1;

    page_n = im / zdet_r->page_size;
    i_page = im % zdet_r->page_size;

    g_r->action_status[page_n][i_page] &= 0xff000000;
    g_r->action_status[page_n][i_page] |= ((0xffff & nb) << 8) + (0xff & phase);
    return 0;
}


char *generate_zdet_topo_title(void)
{
    static char mes[4096] = {0}, state[64] = {0};

    if (zdet_p != NULL)
    {
        if (zdet_p->state == ZDET_SUSPENDED)              snprintf(state, 64, "suspended");
        else if (zdet_p->state == ZDET_RUNNING)           snprintf(state, 64, "running");
        else if (zdet_p->state == ZDET_SINGLE_TEST_DUMMY) snprintf(state, 64, "single test");
        else if (zdet_p->state == ZDET_SINGLE_TEST)       snprintf(state, 64, "single test");
        else        snprintf(state, 64, "stopped");

        snprintf(mes, sizeof(mes), "\\stack{{Zdet Topo %s}{Test nb. %d phase %d/11}{debug: %s}}",
                 state, zdet_p->n_test, zdet_p->test_progress, ti_debug);
    }
    else if (zoli_p != NULL)
    {
        if (zoli_p->state == ZDET_SUSPENDED)              snprintf(state, 64, "suspended");
        else if (zoli_p->state == ZDET_RUNNING)           snprintf(state, 64, "running");
        else if (zoli_p->state == ZDET_SINGLE_TEST_DUMMY) snprintf(state, 64, "single test");
        else if (zoli_p->state == ZDET_SINGLE_TEST)       snprintf(state, 64, "single test");
        else        snprintf(state, 64, "stopped");

        snprintf(mes, sizeof(mes), "\\stack{{Zdet Oligo %s}{Test nb. %d phase %d/6 %d oligos bound/%d}{debug: %s}}",
                 state, zoli_p->n_test, zoli_p->test_progress, zoli_p->n_left, zoli_p->n_bead, ti_debug);
    }

    return mes;
}




int topo_action(int im, int c_i, int aci, int init_to_zero)
{
    static float zmag, zobj_start = -1, zcalib;
    static int suspended = 0;
    int  page_n, i_page, abs_pos, r_step, tmpi, j;
    float rot_mod, tmpf, zobj;

    (void)aci;
    if (init_to_zero)
      {
	zobj_start = -1;
	suspended = 0;
	average_Z_action(0, 0, 0, NULL, NULL, 1);
	rotation_action_and_wait(0, 0, 0, 0, 1);
	return 0;
      }


    if (zdet_r == NULL || zdet_p == NULL)  return 0;

    if (zdet_p->state == ZDET_SUSPENDED) suspended = 1;

    if (zdet_p->state != ZDET_RUNNING && zdet_p->state != ZDET_SINGLE_TEST_DUMMY
            && zdet_p->state != ZDET_SINGLE_TEST)  return 0;

    if (suspended) return 0;

    abs_pos = (zdet_r->abs_pos < 0) ? 0 : zdet_r->abs_pos;
    page_n = abs_pos / zdet_r->page_size;
    i_page = abs_pos % zdet_r->page_size;

    r_step = (int)(zdet_p->rot_hat_max - zdet_p->rot_hat + 0.5);
    r_step += 1;
    rot_mod = zdet_p->rot_hat + zdet_p->n_test % r_step;
    record_event_number_and_phase(zdet_r, abs_pos, zdet_p->n_test, zdet_p->test_progress);

    //  zdet_r->action_status[page_n][i_page] &= 0xff000000;
    //zdet_r->action_status[page_n][i_page] |= ((0xffff&zdet_p->n_test)<<8) + zdet_p->test_progress;
    if (zdet_p->test_progress == 0)   // we start the test by moving zmag to test value
    {
        zmag = (zdet_p->state == ZDET_SINGLE_TEST_DUMMY) ? zdet_p->zmag_rot : zdet_p->zmag_test;
        tmpi = zmag_action_and_wait(im, c_i, zmag, zdet_p->setling_nb_frames_in_zmag);
        zdet_p->test_progress += tmpi;

        if (tmpi)
        {
            zdet_p->n_test++;
        }
    }
    else if (zdet_p->test_progress == 1)   // we move rotation
      zdet_p->test_progress += rotation_action_and_wait(im, c_i, zdet_p->rot_to_test, 0,0);
    else if (zdet_p->test_progress == 2)   // we record test
    {
      if (average_Z_action(im, c_i, zdet_p->bead_nb, &zdet_p->za_test, zdet_r, 0))
        {
            zdet_p->test_progress++;
            add_new_point_to_resu(0, &zdet_p->za_test);
        }
    }
    else if (zdet_p->test_progress == 3)   // we set zmag to the rot value
        zdet_p->test_progress += zmag_action_and_wait(im, c_i, zdet_p->zmag_rot, zdet_p->setling_nb_frames_in_zmag);
    else if (zdet_p->test_progress == 4)   // we record ref0
    {
      if (average_Z_action(im, c_i, zdet_p->bead_nb, &zdet_p->za_ref0, zdet_r,0))
        {
            zdet_p->test_progress++;
            add_new_point_to_resu(1, &zdet_p->za_ref0);

            if (zobj_start < 0)
            {
                zobj_start = zdet_p->za_ref0.zobj;
                zcalib = zdet_p->za_ref0.zobj - zdet_p->za_ref0.zbead;
            }
            else if (zdet_p->za_ref0.profile_invalid == 0 && zdet_p->za_ref0.zbead_valid)
            {
                // we keep profile at the same place in the calibrationimage
                tmpf = zdet_p->za_ref0.zobj - zdet_p->za_ref0.zbead;
                zobj = zdet_p->za_ref0.zobj - tmpf  + zcalib;

                if (zobj > zobj_start + 3) zobj = zobj_start + 3;
                else if (zobj < zobj_start - 3) zobj = zobj_start - 3;

                j = fill_next_available_action(im, MV_OBJ_ABS, zobj);

                if (j < 0)    win_printf_OK("Could not add pending action!");

            }
        }
    }
    else if (zdet_p->test_progress == 5)   // we move rotation to rot_hat
      zdet_p->test_progress += rotation_action_and_wait(im, c_i, rot_mod, 0, 0);
    else if (zdet_p->test_progress == 6)   // we record ref
    {
      if (average_Z_action(im, c_i, zdet_p->bead_nb, &zdet_p->za_ref, zdet_r,0))
        {
            zdet_p->test_progress++;
            add_new_point_to_resu(2, &zdet_p->za_ref);
        }
    }
    else if (zdet_p->test_progress == 7)   // we set zmag to the hat value
        zdet_p->test_progress += zmag_action_and_wait(im, c_i, zdet_p->zmag_hat_measure, zdet_p->setling_nb_frames_in_zmag);
    else if (zdet_p->test_progress == 8)   // we record hat measure
    {
      if (average_Z_action(im, c_i, zdet_p->bead_nb, &zdet_p->za_hat, zdet_r,0))
        {
            zdet_p->test_progress++;
            add_new_point_to_resu(3, &zdet_p->za_hat);
        }
    }
    else if (zdet_p->test_progress == 9)   // we set zmag back to the rot value
        zdet_p->test_progress += zmag_action_and_wait(im, c_i, zdet_p->zmag_rot, zdet_p->setling_nb_frames_in_zmag);
    else if (zdet_p->test_progress == 10)   // we record after
    {
      if (average_Z_action(im, c_i, zdet_p->bead_nb, &zdet_p->za_after, zdet_r,0))
        {
            zdet_p->test_progress++;
            add_new_point_to_resu(4, &zdet_p->za_after);
            add_new_point_to_resu_valid(0, rot_mod);
        }
    }
    else if (zdet_p->test_progress == 11)   // we move rotation
    {
      if (rotation_action_and_wait(im, c_i, zdet_p->rot_to_test, 0, 0))
        {
            zdet_p->test_progress = 0;
            zdet_r->action_status[page_n][i_page] = 0;

            if (zdet_p->state == ZDET_SINGLE_TEST_DUMMY
                    || zdet_p->state == ZDET_SINGLE_TEST)  zdet_p->state = 0;
        }
    }

    return 0;
}


int topo_action_feedback(int im, int c_i, int aci, int init_to_zero)
{
    static float zmag = 0, zobj_start = -1, zcalib;
    static float rot_target = -1000000, dr = 0;
    static int suspended = 0;
    int  page_n, i_page, abs_pos, tmpi, j;
    static float rot_mod = 0;
    float  tmpf, zobj;

    (void)aci;
    if (init_to_zero)
      {
	zmag = 0; zobj_start = -1;
	rot_target = -1000000; dr = 0;
	suspended = 0;
        rot_mod = 0;
	return 0;
      }

    if (zdet_r == NULL || zdet_p == NULL)  return 0;

    if (zdet_p->state == ZDET_SUSPENDED) suspended = 1;

    if (zdet_p->state != ZDET_RUNNING && zdet_p->state != ZDET_SINGLE_TEST_DUMMY
            && zdet_p->state != ZDET_SINGLE_TEST)
    {
        rot_target = -1000000;
        return 0;
    }

    if (suspended) return 0;

    abs_pos = (zdet_r->abs_pos < 0) ? 0 : zdet_r->abs_pos;
    page_n = abs_pos / zdet_r->page_size;
    i_page = abs_pos % zdet_r->page_size;

    if (rot_target == -1000000)  rot_target = prev_mag_rot;

    zdet_r->action_status[page_n][i_page] &= 0xff000000;
    zdet_r->action_status[page_n][i_page] |= ((0xffff & zdet_p->n_test) << 8) + zdet_p->test_progress;

    if (zdet_p->test_progress == 0)   // we move rotation
    {
      zdet_p->test_progress += rotation_action_and_wait(im, c_i, rot_target, 0, 0);
        snprintf(ti_debug, 256, "rotation %f mod %f", rot_target, rot_mod);
    }
    else   if (zdet_p->test_progress == 1)   // we start the test by moving zmag to test value
    {
        if (zdet_p->state == ZDET_SINGLE_TEST_DUMMY) zmag = zdet_p->zmag_rot;
        else if (zdet_p->state == ZDET_SINGLE_TEST) zmag = zdet_p->zmag_test;
        else if (zdet_p->state == ZDET_RUNNING)
            zmag = ((zdet_p->n_test / 2) % 2 == 0) ? zdet_p->zmag_rot : zdet_p->zmag_test;

        tmpi = zmag_action_and_wait(im, c_i, zmag, zdet_p->setling_nb_frames_in_zmag);
        snprintf(ti_debug, 256, "rotation %f mod %f zmag %f", rot_target, rot_mod, zmag);

        if (tmpi)
        {
            zdet_p->test_progress++;
        }
    }
    else if (zdet_p->test_progress == 2)   // we record test
    {
      if (average_Z_action(im, c_i, zdet_p->bead_nb, &zdet_p->za_test, zdet_r,0))
        {
            add_new_point_to_resu(0, &zdet_p->za_test);

            if (zdet_p->n_test > 2)
                add_new_point_to_resu_hat((zdet_p->n_test / 2) % 2, rot_target,
                                          zdet_p->z_cor * (zdet_p->za_test.zbead - zdet_p->za_ref.zbead));

            zdet_p->test_progress++;
        }
    }
    else if (zdet_p->test_progress == 3)   // we set zmag to the rot value
        zdet_p->test_progress += zmag_action_and_wait(im, c_i, zdet_p->zmag_rot, zdet_p->setling_nb_frames_in_zmag);
    else if (zdet_p->test_progress == 4)   // we record ref0
    {
      if (average_Z_action(im, c_i, zdet_p->bead_nb, &zdet_p->za_ref0, zdet_r,0))
        {
            add_new_point_to_resu(1, &zdet_p->za_ref0);

            if (zdet_p->n_test > 2)
                add_new_point_to_resu_hat((zdet_p->n_test / 2) % 2, rot_target,
                                          zdet_p->z_cor * (zdet_p->za_ref0.zbead - zdet_p->za_ref.zbead));

            if (zobj_start < 0)
            {
                zobj_start = zdet_p->za_ref0.zobj;
                zcalib = zdet_p->za_ref0.zobj - zdet_p->za_ref0.zbead;
            }
            else if (zdet_p->za_ref0.profile_invalid == 0 && zdet_p->za_ref0.zbead_valid)
            {
                // we keep profile at the same place in the calibrationimage
                tmpf = zdet_p->za_ref0.zobj - zdet_p->za_ref0.zbead;
                zobj = zdet_p->za_ref0.zobj - tmpf  + zcalib;

                if (zobj > zobj_start + 3) zobj = zobj_start + 3;
                else if (zobj < zobj_start - 3) zobj = zobj_start - 3;

                j = fill_next_available_action(im, MV_OBJ_ABS, zobj);

                if (j < 0)    win_printf_OK("Could not add pending action!");

                snprintf(ti_debug, 256, "focus %f", zobj);
            }

            rot_target += zdet_p->turns_to_increment;
            dr = 0.0;
            zdet_p->test_progress++;
        }
    }
    else if (zdet_p->test_progress == 5)   // we move rotation to rot_hat
    {
      zdet_p->test_progress += rotation_action_and_wait(im, c_i, rot_target, 0, 0);
        snprintf(ti_debug, 256, "rotation %f", rot_target);
    }
    else if (zdet_p->test_progress == 6)   // we record ref
    {
      if (average_Z_action(im, c_i, zdet_p->bead_nb, &zdet_p->za_ref, zdet_r,0))
        {
            if (zdet_p->za_ref.profile_invalid == 1 || zdet_p->za_ref.zbead_valid == 0)
                return 0;  // we wait for a good reference

            if (zdet_p->n_test > 2)
                add_new_point_to_resu_hat((zdet_p->n_test / 2) % 2, rot_target,
                                          zdet_p->z_cor * (zdet_p->za_ref.zbead - zdet_p->za_ref.zbead));

            zdet_p->test_progress++;
        }
    }
    else if (zdet_p->test_progress == 7)   // we set zmag to the hat value
        zdet_p->test_progress += zmag_action_and_wait(im, c_i, zdet_p->zmag_hat_measure, zdet_p->setling_nb_frames_in_zmag);
    else if (zdet_p->test_progress == 8)   // we record hat measure
    {
      if (average_Z_action(im, c_i, zdet_p->bead_nb, &zdet_p->za_hat, zdet_r,0))
        {
            if (zdet_p->za_hat.profile_invalid == 1 || zdet_p->za_hat.zbead_valid == 0)
                return 0;

            if (zdet_p->n_test > 2)
                add_new_point_to_resu_hat((zdet_p->n_test / 2) % 2, rot_target,
                                          zdet_p->z_cor * (zdet_p->za_hat.zbead - zdet_p->za_ref.zbead));

            zdet_p->test_progress++;
        }
    }
    else if (zdet_p->test_progress == 9)   // we set zmag back to the rot value
    {
        if (zmag_action_and_wait(im, c_i, zdet_p->zmag_rot, zdet_p->setling_nb_frames_in_zmag))
        {
            tmpf = (zdet_p->z_cor * (zdet_p->za_ref.zbead - zdet_p->za_hat.zbead  - zdet_p->dz_servo)) / (zdet_p->dz_one_turn * 2);
            tmpf += 50.5;
            tmpi = (int)tmpf;
            tmpi -= 50;
            tmpi *= 2;
            tmpi = (tmpi > 4) ? 4 : tmpi;
            tmpi = (tmpi < -4) ? -4 : tmpi;
            dr -= (float)tmpi;
            snprintf(ti_debug, 256, "dz found %g dr %g", zdet_p->z_cor * (zdet_p->za_ref.zbead - zdet_p->za_hat.zbead), dr);

            if (zdet_p->feedback == 2 && abs(tmpi))
            {
                rot_target -= tmpi;
                zdet_p->test_progress = 5;
                return 0;
            }

            add_new_point_to_resu(2, &zdet_p->za_ref);
            add_new_point_to_resu(3, &zdet_p->za_hat);
            zdet_p->test_progress++;
        }
    }
    else if (zdet_p->test_progress == 10)   // we record after
    {
      if (average_Z_action(im, c_i, zdet_p->bead_nb, &zdet_p->za_after, zdet_r,0))
        {
            add_new_point_to_resu(4, &zdet_p->za_after);
            add_new_point_to_resu_valid_2((zdet_p->n_test / 2) % 2, rot_mod, dr);

            if (zdet_p->n_test > 2)
                add_new_point_to_resu_hat((zdet_p->n_test / 2) % 2, rot_target,
                                          zdet_p->z_cor * (zdet_p->za_hat.zbead - zdet_p->za_ref.zbead));

            rot_mod = 0;

            if (zdet_p->feedback)
            {
                rot_mod = (zdet_p->n_test % 2) ? zdet_p->turns_to_decrement_pos : zdet_p->turns_to_decrement_neg;
            }

            rot_target -= zdet_p->turns_to_increment;
            rot_target += rot_mod;
            zdet_p->test_progress++;
        }
    }
    else if (zdet_p->test_progress == 11)   // we move rotation
    {
      if (rotation_action_and_wait(im, c_i, rot_target, 0, 0))
        {
            zdet_p->test_progress = 0;
            zdet_p->n_test++;

            zdet_r->action_status[page_n][i_page] = 0;

            if (zdet_p->state == ZDET_SINGLE_TEST_DUMMY
                    || zdet_p->state == ZDET_SINGLE_TEST)  zdet_p->state = 0;
        }
    }

    return 0;
}

# ifdef EN_COURS
int topo_action_feedback_simple(int im, int c_i, int aci)
{
    static float zmag = 0, zobj_start = -1, zcalib;
    static float rot_target = -1000000, dr = 0;
    static int suspended = 0, rot_int = 0;
    int  page_n, i_page, abs_pos, tmpi, j;
    //static float rot_mod = 0;
    float  tmpf, zobj;


    if (zdet_r == NULL || zdet_p == NULL)  return 0;

    if (zdetc_p->state == ZDET_SUSPENDED) suspended = 1;

    if (zdetc_p->state != ZDET_RUNNING && zdetc_p->state != ZDET_SINGLE_TEST_DUMMY
            && zdet_pc->state != ZDET_SINGLE_TEST)
    {
        rot_target = -1000000;
        return 0;
    }


    abs_pos = (zdet_r->abs_pos < 0) ? 0 : zdet_r->abs_pos;
    page_n = abs_pos / zdet_r->page_size;
    i_page = abs_pos % zdet_r->page_size;

    rot_int = (int)(rot_target + 0.5);

    if (rot_target == -1000000)
    {
        rot_int = (int)(prev_mag_rot + 0.5);
        rot_target = (float)rot_int + zdetc_p->rot_to_ref;
    }

    zdet_r->action_status[page_n][i_page] &= 0xff000000;
    zdet_r->action_status[page_n][i_page] |= ((0xffff & zdet_p->n_test) << 8) + zdet_p->test_progress;

    if (zdet_p->test_progress == 0)   // we move rotation to ref
    {
      zdet_p->test_progress += rotation_action_and_wait(im, c_i, rot_target, 0, 0);
        snprintf(ti_debug, 256, "Phase 0 => rotation %f", rot_target);
    }
    else   if (zdet_p->test_progress == 1)   // we start the test by moving zmag to test value
    {
      if (average_Z_action(im, c_i, zdet_p->bead_nb, &zdet_p->za_ref, zdet_r,0))
        {
            add_new_point_to_resu(0, &zdet_p->za_ref);
            zdet_p->test_progress++;
        }
    }
    else if (zdet_p->test_progress == 2)   // we record test
    {
        rot_target = rot_int + zdetc_p->rot_to_test;
        zdet_p->test_progress += rotation_action_and_wait(im, c_i, rot_target, 0, 0);
        snprintf(ti_debug, 256, "Phase 2 => rotation %f", rot_target);
    }
    else if (zdet_p->test_progress == 3)   // we set zmag to the rot value
    {
      if (average_Z_action(im, c_i, zdet_p->bead_nb, &zdet_p->za_test, zdet_r,0))
        {
            add_new_point_to_resu(0, &zdet_p->za_test);

            if (zdet_p->n_test > 2)
                add_new_point_to_resu_hat((zdet_p->n_test / 2) % 2, rot_target,
                                          zdet_p->z_cor * (zdet_p->za_test.zbead - zdet_p->za_ref.zbead));

            zdet_p->test_progress++;
        }
    }
    else if (zdet_p->test_progress == 3)   // we set zmag to the rot value
        zdet_p->test_progress += zmag_action_and_wait(im, c_i, zdet_p->zmag_rot, zdet_p->setling_nb_frames_in_zmag);
    else if (zdet_p->test_progress == 4)   // we record ref0
    {
      if (average_Z_action(im, c_i, zdet_p->bead_nb, &zdet_p->za_ref0, zdet_r,0))
        {
            add_new_point_to_resu(1, &zdet_p->za_ref0);

            if (zdet_p->n_test > 2)
                add_new_point_to_resu_hat((zdet_p->n_test / 2) % 2, rot_target,
                                          zdet_p->z_cor * (zdet_p->za_ref0.zbead - zdet_p->za_ref.zbead));

            if (zobj_start < 0)
            {
                zobj_start = zdet_p->za_ref0.zobj;
                zcalib = zdet_p->za_ref0.zobj - zdet_p->za_ref0.zbead;
            }
            else if (zdet_p->za_ref0.profile_invalid == 0 && zdet_p->za_ref0.zbead_valid)
            {
                // we keep profile at the same place in the calibrationimage
                tmpf = zdet_p->za_ref0.zobj - zdet_p->za_ref0.zbead;
                zobj = zdet_p->za_ref0.zobj - tmpf  + zcalib;

                if (zobj > zobj_start + 3) zobj = zobj_start + 3;
                else if (zobj < zobj_start - 3) zobj = zobj_start - 3;

                j = fill_next_available_action(im, MV_OBJ_ABS, zobj);

                if (j < 0)    win_printf_OK("Could not add pending action!");

                snprintf(ti_debug, 256, "focus %f", zobj);
            }

            rot_target += zdet_p->turns_to_increment;
            dr = 0.0;
            zdet_p->test_progress++;
        }
    }
    else if (zdet_p->test_progress == 5)   // we move rotation to rot_hat
    {
      zdet_p->test_progress += rotation_action_and_wait(im, c_i, rot_target, 0, 0);
        snprintf(ti_debug, 256, "rotation %f", rot_target);
    }
    else if (zdet_p->test_progress == 6)   // we record ref
    {
      if (average_Z_action(im, c_i, zdet_p->bead_nb, &zdet_p->za_ref, zdet_r,0))
        {
            if (zdet_p->za_ref.profile_invalid == 1 || zdet_p->za_ref.zbead_valid == 0)
                return 0;  // we wait for a good reference

            if (zdet_p->n_test > 2)
                add_new_point_to_resu_hat((zdet_p->n_test / 2) % 2, rot_target,
                                          zdet_p->z_cor * (zdet_p->za_ref.zbead - zdet_p->za_ref.zbead));

            zdet_p->test_progress++;
        }
    }
    else if (zdet_p->test_progress == 7)   // we set zmag to the hat value
        zdet_p->test_progress += zmag_action_and_wait(im, c_i, zdet_p->zmag_hat_measure, zdet_p->setling_nb_frames_in_zmag);
    else if (zdet_p->test_progress == 8)   // we record hat measure
    {
      if (average_Z_action(im, c_i, zdet_p->bead_nb, &zdet_p->za_hat, zdet_r,0))
        {
            if (zdet_p->za_hat.profile_invalid == 1 || zdet_p->za_hat.zbead_valid == 0)
                return 0;

            if (zdet_p->n_test > 2)
                add_new_point_to_resu_hat((zdet_p->n_test / 2) % 2, rot_target,
                                          zdet_p->z_cor * (zdet_p->za_hat.zbead - zdet_p->za_ref.zbead));

            zdet_p->test_progress++;
        }
    }
    else if (zdet_p->test_progress == 9)   // we set zmag back to the rot value
    {
        if (zmag_action_and_wait(im, c_i, zdet_p->zmag_rot, zdet_p->setling_nb_frames_in_zmag))
        {
            tmpf = (zdet_p->z_cor * (zdet_p->za_ref.zbead - zdet_p->za_hat.zbead  - zdet_p->dz_servo)) / (zdet_p->dz_one_turn * 2);
            tmpf += 50.5;
            tmpi = (int)tmpf;
            tmpi -= 50;
            tmpi *= 2;
            tmpi = (tmpi > 4) ? 4 : tmpi;
            tmpi = (tmpi < -4) ? -4 : tmpi;
            dr -= (float)tmpi;
            snprintf(ti_debug, 256, "dz found %g dr %g", zdet_p->z_cor * (zdet_p->za_ref.zbead - zdet_p->za_hat.zbead), dr);

            if (zdet_p->feedback == 2 && abs(tmpi))
            {
                rot_target -= tmpi;
                zdet_p->test_progress = 5;
                return 0;
            }

            add_new_point_to_resu(2, &zdet_p->za_ref);
            add_new_point_to_resu(3, &zdet_p->za_hat);
            zdet_p->test_progress++;
        }
    }
    else if (zdet_p->test_progress == 10)   // we record after
    {
      if (average_Z_action(im, c_i, zdet_p->bead_nb, &zdet_p->za_after, zdet_r,0))
        {
            add_new_point_to_resu(4, &zdet_p->za_after);
            add_new_point_to_resu_valid_2((zdet_p->n_test / 2) % 2, rot_mod, dr);

            if (zdet_p->n_test > 2)
                add_new_point_to_resu_hat((zdet_p->n_test / 2) % 2, rot_target,
                                          zdet_p->z_cor * (zdet_p->za_hat.zbead - zdet_p->za_ref.zbead));

            rot_mod = 0;

            if (zdet_p->feedback)
            {
                rot_mod = (zdet_p->n_test % 2) ? zdet_p->turns_to_decrement_pos : zdet_p->turns_to_decrement_neg;
            }

            rot_target -= zdet_p->turns_to_increment;
            rot_target += rot_mod;
            zdet_p->test_progress++;
        }
    }
    else if (zdet_p->test_progress == 11)   // we move rotation
    {
      if (rotation_action_and_wait(im, c_i, rot_target, 0, 0))
        {
            zdet_p->test_progress = 0;
            zdet_p->n_test++;

            zdet_r->action_status[page_n][i_page] = 0;

            if (zdet_p->state == ZDET_SINGLE_TEST_DUMMY
                    || zdet_p->state == ZDET_SINGLE_TEST)  zdet_p->state = 0;
        }
    }

    return 0;
}


# endif

int oligo_action(int im, int c_i, int aci, int init_to_zero)
{
    static float zmag;
    static int suspended = 0;
    int  page_n, i_page, abs_pos, tmpi;

    (void)aci;

    if (init_to_zero)
      {
	suspended = 0;
	return 0;
      }
    if (zdet_r == NULL || zoli_p == NULL)  return 0;

    if (zoli_p->state == ZDET_SUSPENDED) suspended = 1;

    if (zoli_p->state != ZDET_RUNNING && zoli_p->state != ZDET_SINGLE_TEST_DUMMY
            && zoli_p->state != ZDET_SINGLE_TEST)  return 0;

    if (suspended) return 0;

    abs_pos = (zdet_r->abs_pos < 0) ? 0 : zdet_r->abs_pos;
    page_n = abs_pos / zdet_r->page_size;
    i_page = abs_pos % zdet_r->page_size;


    record_event_number_and_phase(zdet_r, abs_pos, zoli_p->n_test, zoli_p->test_progress);

    if (zoli_p->test_progress == 0)   // we start the test by moving zmag to test value
    {
        zmag = zoli_p->zmag_test;
        tmpi = zmag_action_and_wait(im, c_i, zmag, zoli_p->setling_nb_frames_in_zmag);
        zoli_p->test_progress += tmpi;

        if (tmpi)
        {
            zoli_p->n_test++;
        }
    }
    else if (zoli_p->test_progress == 1)   // we average ref value
    {
      if (average_Z_action(im, c_i, zoli_p->bead_nb, &zoli_p->za_ref0, zdet_r,0))
        {
            zoli_p->test_progress++;
            add_new_point_to_oli_resu(0, &zoli_p->za_ref0);
        }
    }
    else if (zoli_p->test_progress == 2)   // we record test
    {
        zoli_p->test_progress += zmag_action_and_wait(im, c_i, zoli_p->zmag_unzip, zoli_p->setling_nb_frames_in_zmag);
    }
    else if (zoli_p->test_progress == 3)   // we record unzip state
    {
      if (average_Z_action(im, c_i, zoli_p->bead_nb, &zoli_p->za_unzip, zdet_r,0))
        {
            zoli_p->test_progress++;
            add_new_point_to_oli_resu(1, &zoli_p->za_unzip);
        }
    }
    else if (zoli_p->test_progress == 4)   // we set zmag to the measurement value
        zoli_p->test_progress += zmag_action_and_wait(im, c_i, zoli_p->zmag_test, 2);
    else if (zoli_p->test_progress == 5)   // we record ref0
    {
        if (zdet_stay_in_range_action(im,  c_i,  &zoli_p->z_dt, zdet_r, &zoli_p->za_ref0))
        {
            zoli_p->test_progress++;
            add_new_point_to_oli_time_lapse(0, &zoli_p->z_dt);
            add_new_point_to_oli_resu_dt(2, &zoli_p->z_dt);
        }
    }
    else if (zoli_p->test_progress == 6)   // we record unzip state
    {
      if (average_Z_action(im, c_i, zoli_p->bead_nb, &zoli_p->za_after, zdet_r,0))
        {
            zoli_p->test_progress = 0;
            //add_new_point_to_resu(3, &zoli_p->za_after);
            zdet_r->action_status[page_n][i_page] = 0;

            if (zoli_p->state == ZDET_SINGLE_TEST_DUMMY
                    || zoli_p->state == ZDET_SINGLE_TEST)  zoli_p->state = 0;
        }
    }

    return 0;
}






int record_zdet_topo(void)
{
    static int first = 1;
    int i, k, im, li;
    imreg *imr = NULL;
    O_i *oi = NULL;
    b_track *bt = NULL;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    static int nf = 8192, with_radial_profiles = 0, streaming = 1, bead_nb = 0;
    static float rot_start = 10, zmag; //, zmag_start
    char name[64] = {0}, basename[256] = {0};
    char question[4096] = {0};

    if (updating_menu_state != 0)
    {
        if (working_gen_record != NULL || track_info->n_b == 0)
            active_menu->flags |=  D_DISABLED;
        else
        {
           if (track_info->SDI_mode == 0)
              {
                  for (i = 0, k = 0; i < track_info->n_b; i++)
                     {
                         bt = track_info->bd[i];
                         k += (bt->calib_im != NULL) ? 1 : 0;
                     }
              }
           else k = 1;
            if (k) active_menu->flags &= ~D_DISABLED;
            else active_menu->flags |=  D_DISABLED;
        }

        return D_O_K;
    }

    if (ac_grep(cur_ac_reg, "%im%oi", &imr, &oi) != 2)  return OFF;

    heap_check();
    rot_start = prev_mag_rot;
    //zmag_start = prev_zmag;
    //rot_start = rot = read_rot_value();
    //zmag_start = zmag = read_magnet_z_value();
    zdet_p = (zdet_topo_param *)calloc(1, sizeof(zdet_topo_param));

    Open_Pico_config_file();

    if (first)
    {
        n_zdet = get_config_float("Zdet_Curve", "n_zdet", n_zdet);
        first = 0;
    }

    zdet_p->za_test.time_avg = get_config_float("Zdet_Topo_Curve", "test_time_avg", 5);
    zdet_p->za_ref0.time_avg = get_config_float("Zdet_Topo_Curve", "ref0_time_avg", 1);
    zdet_p->za_hat.time_avg = get_config_float("Zdet_Topo_Curve", "hat_time_avg", 3);
    zdet_p->zmag_setling_time = get_config_float("Zdet_Topo_Curve", "zmag_setling_time", 0.5);
    //zdet_p->z_cor = get_config_float("Zdet_Topo_Curve", "z_cor", 0.878);
    zdet_p->rot_to_test = get_config_float("Zdet_Topo_Curve", "rot_to_test", 0);
    zdet_p->rot_hat = get_config_float("Zdet_Topo_Curve", "rot_hat", 12);
    zdet_p->rot_hat_max = get_config_float("Zdet_Topo_Curve", "rot_hat_max", 13);
    zdet_p->zmag_rot = get_config_float("Zdet_Topo_Curve", "zmag_rot", 8);
    // the roration where we measure the molecule extension
    zdet_p->zmag_test = get_config_float("Zdet_Topo_Curve", "zmag_test", 6.5);
// the magnet zmag value where we want to test the torsional state
    zdet_p->zmag_hat_measure = get_config_float("Zdet_Topo_Curve", "zmag_hat_measure", 7.5);
    Close_Pico_config_file();
    // this is the rotation measuring phase parameters
    zdet_p->bead_nb = bead_nb;
    heap_check();
    n_zdet = find_trk_index_not_used("Zdet_", n_zdet);

    snprintf(question, sizeof(question), "Zdet curve %%5d for topoisomerase characterisation\n"
             "This test involve 3 zmag(forces) states and 2 rotation states\n"
             "The purpose is to test the rotation state of bead %%4d in the so-called\n"
             "test phase with a rotation r_0 %%6f near 0 and a small force F_0 zmag_0 = %%6f\n"
             "this phase will last for \\tau_0 %%6f seconds \n"
             "Then the force is increased to F_{rot} zmag_{rot} %%6f\n"
             "then Z bead position is averaged for \\tau_{rot} %%6f seconds\n"
             "Rotation is modulated between R_{hat} = %%6f and R_{hat_max} = %%6f\n"
             " then Z bead position is averaged\n"
             "for \\tau_{rot} seconds before force reduced to F_{hat} with zmag %%6f \n"
             "The Z position of the bead is then averaged for \\tau_{hat} %%6f seconds\n"
             "Force is then increased again at F_{rot} the Z position of the bead is then\n"
             "averaged for \\tau_{rot} seconds. Rotation is then brought back\n"
             "present val %g angular position %g\n"
             "Number of points in the displayed plot %%8d\nNumber of points for averaging %%8d\n"
             "display zmag %%R or estimated force %%r\n"
             "Do you want to record bead profiles %%b\n"
             "Do you want real time saving %%b\n"
             , rot_start, zmag);

    i = win_scanf(question, &n_zdet, &zdet_p->bead_nb, &zdet_p->rot_to_test, &zdet_p->zmag_test,
                  &zdet_p->za_test.time_avg, &zdet_p->zmag_rot, &zdet_p->za_ref0.time_avg,
                  &zdet_p->rot_hat, &zdet_p->rot_hat_max, &zdet_p->zmag_hat_measure,
                  &zdet_p->za_hat.time_avg, &scope_buf_size, &fir,
                  &zmag_or_f, &with_radial_profiles, &streaming);

    if (i == WIN_CANCEL) return 0;

    bead_nb = zdet_p->bead_nb;
    Open_Pico_config_file();
    set_config_int("Zdet_Curve", "n_zdet", n_zdet);

    set_config_float("Zdet_Topo_Curve", "test_time_avg", zdet_p->za_test.time_avg);
    set_config_float("Zdet_Topo_Curve", "ref0_time_avg", zdet_p->za_ref0.time_avg);
    set_config_float("Zdet_Topo_Curve", "hat_time_avg", zdet_p->za_hat.time_avg);
    set_config_float("Zdet_Topo_Curve", "zmag_setling_time", zdet_p->zmag_setling_time);
    //set_config_float("Zdet_Topo_Curve", "z_cor", zdet_p->z_cor);
    set_config_float("Zdet_Topo_Curve", "rot_to_test", zdet_p->rot_to_test);
    set_config_float("Zdet_Topo_Curve", "rot_hat", zdet_p->rot_hat);
    set_config_float("Zdet_Topo_Curve", "rot_hat_max", zdet_p->rot_hat_max);
    set_config_float("Zdet_Topo_Curve", "zmag_rot", zdet_p->zmag_rot);
    set_config_float("Zdet_Topo_Curve", "zmag_test", zdet_p->zmag_test);
    set_config_float("Zdet_Topo_Curve", "zmag_hat_measure", zdet_p->zmag_hat_measure);
    write_Pico_config_file();
    Close_Pico_config_file();



    zdet_p->setling_nb_frames_in_zmag = (int)(0.5 + (zdet_p->zmag_setling_time
                                        * Pico_param.camera_param.camera_frequency_in_Hz));


    zdet_p->za_test.nb_frames_in_avg = (int)(0.5 + (zdet_p->za_test.time_avg
                                       * Pico_param.camera_param.camera_frequency_in_Hz));

    zdet_p->za_ref0.nb_frames_in_avg = (int)(0.5 + (zdet_p->za_ref0.time_avg
                                       * Pico_param.camera_param.camera_frequency_in_Hz));

    zdet_p->za_hat.nb_frames_in_avg = (int)(0.5 + (zdet_p->za_hat.time_avg
                                            * Pico_param.camera_param.camera_frequency_in_Hz));

    zdet_p->za_ref.nb_frames_in_avg = zdet_p->za_ref0.nb_frames_in_avg;
    zdet_p->za_after.nb_frames_in_avg = zdet_p->za_ref0.nb_frames_in_avg;
    zdet_p->za_ref.time_avg = zdet_p->za_ref0.time_avg;
    zdet_p->za_after.time_avg = zdet_p->za_ref0.time_avg;

    zdet_p->n_test = 1;

    /*
    win_printf("setling_nb_frames_in_zmag %d nb_frames_in_test_phase %d\n"
          "nb_frames_in_ref_phase %d nb_frames_in_hat_measure_phase %d",
          zdet_p->setling_nb_frames_in_zmag,zdet_p->za_test.nb_frames_in_avg,
          zdet_p->za_ref.nb_frames_in_avg,zdet_p->za_hat.nb_frames_in_avg);
    */


    nf = (scope_buf_size > 8192) ? 2 * scope_buf_size : 8192;

    snprintf(name, 64, "Zdet topo curve %d", n_zdet);

    // We create a plot region to display bead position, zdet  etc.
    pr = create_hidden_pltreg_with_op(&op, nf, nf, 0, name);

    if (pr == NULL)  win_printf_OK("Could not find or allocte plot region!");

    //op = pr->one_p;
    ds = op->dat[0];


    zdet_r = create_gen_record(track_info, 1, with_radial_profiles, 0, 1, 0, 1.0, 1.0, 1);
    duplicate_Pico_parameter(&Pico_param, &(zdet_r->Pico_param_record));

    if (attach_g_record_to_pltreg(pr, zdet_r))
        win_printf("Could not attach grecord to plot region!");

    //   if (extract_file_name(zdet_r->filename, 512, fullfilename) == NULL)
    snprintf(zdet_r->filename, 512, "Zdet_%d.trk", n_zdet);
    snprintf(zdet_r->name, 512, "Zdet curve %d", n_zdet);
    zdet_r->n_rec = n_zdet;
    zdet_r->data_type |= ZDET_RECORD;
    zdet_r->record_action = topo_action;
    topo_action(0,0,0,1);
    zdet_p->z_cor = zdet_r->z_cor;

    if (streaming)
    {
        if (do_save_track(zdet_r))
            win_printf("Pb writing tracking header");
    }

    li = grab_basename_and_index(zdet_r->filename, strlen(zdet_r->filename) + 1, basename, 256);
    n_zdet = (li >= 0) ? li : n_zdet;

    Open_Pico_config_file();
    set_config_int("Zdet_Curve", "n_zdet", n_zdet);
    write_Pico_config_file();
    Close_Pico_config_file();



    im = track_info->imi[track_info->c_i];

    im += 64;

    //we create one plot for each bead
    for (i = li = 0; i < track_info->n_b; i++)
    {
        bt = track_info->bd[i];

        if ((bt->calib_im == NULL) || (bt->not_lost <= 0) || (bt->in_image == 0))
            continue;

        if (li)
        {
            op = create_and_attach_one_plot(pr, nf, nf, 0);
            ds = op->dat[0];
        }

        li++;
        bt->opt = op;
        ds->nx = ds->ny = 0;
        set_dot_line(ds);
        set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                      "Z coordinate l = %d, w = %d, nim = %d\n"
                      "objective %f, zoom factor %f, sample %s Rotation %g\n"
                      "Z magnets %g mm\n"
                      "Calibration from %s"
                      , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, i
                      , track_info->cl, track_info->cw, nf
                      , Pico_param.obj_param.objective_magnification
                      , Pico_param.micro_param.zoom_factor
                      , pico_sample
                      , rot_start
                      , zmag
                      , bt->calib_im->filename);

        if ((ds = create_and_attach_one_ds(op, nf / fir, nf / fir, 0)) == NULL)
            return win_printf_OK("I can't create plot !");

        ds->nx = ds->ny = 0;
        set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                      "filtred Z over %d coordinate l = %d, w = %d, nim = %d\n"
                      "objective %f, zoom factor %f, sample %s Rotation %g\n"
                      "Z magnets %g mm\n"
                      "Calibration from %s"
                      , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, i
                      , fir, track_info->cl, track_info->cw, nf
                      , Pico_param.obj_param.objective_magnification
                      , Pico_param.micro_param.zoom_factor
                      , pico_sample
                      , rot_start
                      , zmag
                      , bt->calib_im->filename);

        if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
            return win_printf_OK("I can't create plot !");

        ds->nx = ds->ny = 0;
        set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                      "Zmag l = %d, w = %d, nim = %d\n"
                      "objective %f, zoom factor %f, sample %s Rotation %g\n"
                      "Z magnets %g mm\n"
                      "Calibration from %s"
                      , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, i
                      , track_info->cl, track_info->cw, nf
                      , Pico_param.obj_param.objective_magnification
                      , Pico_param.micro_param.zoom_factor
                      , pico_sample
                      , rot_start
                      , zmag
                      , bt->calib_im->filename);


        if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
            return win_printf_OK("I can't create plot !");

        alloc_data_set_x_error(ds);
        ds->nx = ds->ny = 0;
        set_ds_dot_line(ds);
        set_ds_point_symbol(ds, "\\oc");
        set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                      "averaged Z over %d coordinate l = %d, w = %d, nim = %d\n"
                      "objective %f, zoom factor %f, sample %s Rotation %g\n"
                      "Z magnets %g mm\n"
                      "Calibration from %s"
                      , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, i
                      , fir, track_info->cl, track_info->cw, nf
                      , Pico_param.obj_param.objective_magnification
                      , Pico_param.micro_param.zoom_factor
                      , pico_sample
                      , rot_start
                      , zmag
                      , bt->calib_im->filename);
        create_attach_select_y_un_to_op(op, IS_METER, 0 , (float)1, -3, 0, "mm");
        create_attach_select_y_un_to_op(op, IS_NEWTON, 0 , (float)1, -12, 0, "pN");
        create_attach_select_y_un_to_op(op, 0, 0 , (float)1, 0, 0, "no_name");


        //uns_oi_2_op(oi, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
        if (zmag_or_f == 0)
        {
            set_plot_y_prime_title(op, "Zmag");
            op->c_yu_p = 1;
        }
        else if (zmag_or_f == 0)
        {
            set_plot_y_prime_title(op, "Estimated force");
            op->c_yu_p = 2;
        }
        else
        {
            set_plot_y_prime_title(op, "Rotation");
            op->c_yu_p = 3;
        }

        //op->c_yu_p = op->c_yu;
        create_attach_select_y_un_to_op(op, IS_METER, 0 , (float)1, -6, 0, "\\mu m");
        create_attach_select_x_un_to_op(op, IS_SECOND, 0
                                        , (float)1 / Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");

        op->op_idle_action = zdet_op_idle_action;
        op->op_post_display = zdet_op_post_display;
        set_op_filename(op, "Z(t)fil-bd%dzdet%d.gr", i, n_zdet);
        set_plot_title(op, "Bead %d Z(t) %d zmag %g", i, n_zdet, zmag);
        set_plot_x_title(op, "Time");
        set_plot_y_title(op, "Molecule extension");


        op->x_lo = (-nf / 64); ///Pico_param.camera_param.camera_frequency_in_Hz;
        op->x_hi = (nf + nf / 64); ///Pico_param.camera_param.camera_frequency_in_Hz;
        set_plot_x_fixed_range(op);
        fill_next_available_job_spot(im, im + 32 + i, COLLECT_DATA, i, imr, oi, pr, op, NULL, zdet_job_z_fil_avg, 0);
        op->user_id = 4096;
        op->user_ispare[ISPARE_BEAD_NB] = i;
        //win_printf("bd %d",i);
    }

    bt = track_info->bd[zdet_p->bead_nb];
    op = create_and_attach_one_plot(pr, nf, nf, 0);
    ds = op->dat[0];
    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_ds_point_symbol(ds, "\\oc");
    set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                  "averaged Z coordinate test point mean value at rot %g zmag %g\n "
                  "l = %d, w = %d, nim = %d\n"
                  "objective %f, zoom factor %f, sample %s Rotation %g\n"
                  "Z magnets %g mm\n"
                  "Calibration from %s"
                  , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, zdet_p->bead_nb
                  , zdet_p->rot_to_test, zdet_p->zmag_test
                  , track_info->cl, track_info->cw, nf
                  , Pico_param.obj_param.objective_magnification
                  , Pico_param.micro_param.zoom_factor
                  , pico_sample
                  , rot_start
                  , zmag
                  , bt->calib_im->filename);


    if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("I can't create plot !");

    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_ds_point_symbol(ds, "\\fs");
    set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                  "averaged Z coordinate ref0 point mean value at rot %g zmag %g\n "
                  "l = %d, w = %d, nim = %d\n"
                  "objective %f, zoom factor %f, sample %s Rotation %g\n"
                  "Z magnets %g mm\n"
                  "Calibration from %s"
                  , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, zdet_p->bead_nb
                  , zdet_p->rot_to_test, zdet_p->zmag_rot
                  , track_info->cl, track_info->cw, nf
                  , Pico_param.obj_param.objective_magnification
                  , Pico_param.micro_param.zoom_factor
                  , pico_sample
                  , rot_start
                  , zmag
                  , bt->calib_im->filename);


    if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("I can't create plot !");

    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_ds_point_symbol(ds, "\\cr");
    set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                  "averaged Z coordinate ref point mean value at rot %g zmag %g\n "
                  "l = %d, w = %d, nim = %d\n"
                  "objective %f, zoom factor %f, sample %s Rotation %g\n"
                  "Z magnets %g mm\n"
                  "Calibration from %s"
                  , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, zdet_p->bead_nb
                  , zdet_p->rot_hat, zdet_p->zmag_rot
                  , track_info->cl, track_info->cw, nf
                  , Pico_param.obj_param.objective_magnification
                  , Pico_param.micro_param.zoom_factor
                  , pico_sample
                  , rot_start
                  , zmag
                  , bt->calib_im->filename);


    if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("I can't create plot !");

    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_ds_point_symbol(ds, "\\fd");
    set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                  "averaged Z coordinate measure point mean value at rot %g zmag %g\n "
                  "l = %d, w = %d, nim = %d\n"
                  "objective %f, zoom factor %f, sample %s Rotation %g\n"
                  "Z magnets %g mm\n"
                  "Calibration from %s"
                  , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, zdet_p->bead_nb
                  , zdet_p->rot_hat, zdet_p->zmag_hat_measure
                  , track_info->cl, track_info->cw, nf
                  , Pico_param.obj_param.objective_magnification
                  , Pico_param.micro_param.zoom_factor
                  , pico_sample
                  , rot_start
                  , zmag
                  , bt->calib_im->filename);


    if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("I can't create plot !");

    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_ds_point_symbol(ds, "\\fd");
    set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                  "averaged Z coordinate after point mean value at rot %g zmag %g\n "
                  "l = %d, w = %d, nim = %d\n"
                  "objective %f, zoom factor %f, sample %s Rotation %g\n"
                  "Z magnets %g mm\n"
                  "Calibration from %s"
                  , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, zdet_p->bead_nb
                  , zdet_p->rot_hat, zdet_p->zmag_rot
                  , track_info->cl, track_info->cw, nf
                  , Pico_param.obj_param.objective_magnification
                  , Pico_param.micro_param.zoom_factor
                  , pico_sample
                  , rot_start
                  , zmag
                  , bt->calib_im->filename);



    op->op_idle_action = zdet_op_idle_action;
    op->op_post_display = zdet_op_post_display;
    set_op_filename(op, "Z(t)Avg-bd%dzdet%d.gr", zdet_p->bead_nb, n_zdet);
    set_plot_title(op, "Bead %d Averaged Z(t) %d ", zdet_p->bead_nb, n_zdet);
    set_plot_x_title(op, "Test number");
    set_plot_y_title(op, "Molecule extension change");

    zdet_p->resu = op;


    op = create_and_attach_one_plot(pr, nf, nf, 0);
    ds = op->dat[0];
    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_ds_point_symbol(ds, "\\oc");
    set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                  "averaged Z coordinate test point mean value at rot %g zmag %g\n "
                  "l = %d, w = %d, nim = %d\n"
                  "objective %f, zoom factor %f, sample %s Rotation %g\n"
                  "Z magnets %g mm\n"
                  "Calibration from %s"
                  , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, zdet_p->bead_nb
                  , zdet_p->rot_to_test, zdet_p->zmag_test
                  , track_info->cl, track_info->cw, nf
                  , Pico_param.obj_param.objective_magnification
                  , Pico_param.micro_param.zoom_factor
                  , pico_sample
                  , rot_start
                  , zmag
                  , bt->calib_im->filename);



    op->op_idle_action = zdet_op_idle_action;
    op->op_post_display = zdet_op_post_display;
    zdet_p->resu_valid = op;

    im = track_info->imi[track_info->c_i];
    working_gen_record = zdet_r;
    zdet_r->last_starting_pos = im;
    win_title_used = 1;
    ask_to_update_menu();
    k = fill_next_available_action(im + 1, BEAD_CROSS_SAVED, 0);

    if (k < 0)   my_set_window_title("Could not add pending action!");

    if (imr_and_pr) switch_project_to_this_pltreg(pr);

    heap_check();
    generate_imr_TRACK_title = generate_zdet_topo_title;
    return D_O_K;
}
# ifdef EN_COURS

int record_zdet_topo_feedback_simple(void)
{
    static int first = 1, feedback = 1;
    int i, k, im, li;
    imreg *imr = NULL;
    O_i *oi = NULL;
    b_track *bt = NULL;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    static int nf = 8192, with_radial_profiles = 0, streaming = 1, bead_nb = 0;
    static float rot_start = 10, rot, zmag_start, zmag;
    char name[64] = {0}, basename[256] = {0};
    char question[4096] = {0};

    if (updating_menu_state != 0)
    {
        if (working_gen_record != NULL || track_info->n_b == 0)
            active_menu->flags |=  D_DISABLED;
        else
        {
           if (track_info->SDI_mode == 0)
              {
                  for (i = 0, k = 0; i < track_info->n_b; i++)
                     {
                         bt = track_info->bd[i];
                         k += (bt->calib_im != NULL) ? 1 : 0;
                     }
              }
           else k = 1;
            if (k) active_menu->flags &= ~D_DISABLED;
            else active_menu->flags |=  D_DISABLED;
        }

        return D_O_K;
    }

    if (ac_grep(cur_ac_reg, "%im%oi", &imr, &oi) != 2)  return OFF;

    heap_check();

    rot_start = rot = prev_mag_rot; //read_rot_value();
    zmag_start = zmag = prev_zmag; //read_magnet_z_value();
    zdet_p = (zdet_topo_param *)calloc(1, sizeof(zdet_topo_param));

    Open_Pico_config_file();

    if (first)
    {
        n_zdet = get_config_float("Zdet_Curve", "n_zdet", n_zdet);
        first = 0;
    }

    zdet_p->za_test.time_avg = get_config_float("Zdet_Topo_Curve", "test_time_avg", 5);
    zdet_p->za_ref0.time_avg = get_config_float("Zdet_Topo_Curve", "ref0_time_avg", 1);
    zdet_p->za_hat.time_avg = get_config_float("Zdet_Topo_Curve", "hat_time_avg", 3);
    zdet_p->zmag_setling_time = get_config_float("Zdet_Topo_Curve", "zmag_setling_time", 0.5);
    //zdet_p->z_cor = get_config_float("Zdet_Topo_Curve", "z_cor", 0.878);
    zdet_p->turns_to_increment = get_config_float("Zdet_Topo_Curve", "turns_to_increment", 10);
    zdet_p->turns_to_decrement_pos = get_config_float("Zdet_Topo_Curve", "turns_to_decrement_pos", 2);
    zdet_p->turns_to_decrement_neg = get_config_float("Zdet_Topo_Curve", "turns_to_decrement_neg", -2);
    zdet_p->zmag_rot = get_config_float("Zdet_Topo_Curve", "zmag_rot", 8);
    zdet_p->dz_servo = get_config_float("Zdet_Topo_Curve", "dz_servo", 1);
    zdet_p->dz_one_turn = get_config_float("Zdet_Topo_Curve", "dz_one_turn", 0.045);
    // the roration where we measure the molecule extension
    zdet_p->zmag_test = get_config_float("Zdet_Topo_Curve", "zmag_test", 6.5);
// the magnet zmag value where we want to test the torsional state
    zdet_p->zmag_hat_measure = get_config_float("Zdet_Topo_Curve", "zmag_hat_measure", 7.5);
    Close_Pico_config_file();
    // this is the rotation measuring phase parameters
    zdet_p->bead_nb = bead_nb;
    heap_check();


    snprintf(question, sizeof(question), "Zdet curve %%5d for topoisomerase characterisation\n"
             "This test involve a rotation step each time the bead moves up above a define threshold\n"
             "The purpose is to test the rotation state of bead %%4d in the so-called\n"
             "test phase with a rotation r_- %%6f  r_+ %%6f near 0 and a small force F_0 zmag_0 = %%6f\n"
             "this phase will last for \\tau_0 %%6f seconds \n"
             "Then the force is increased to F_{rot} zmag_{rot} %%6f\n"
             "then Z bead position is averaged for \\tau_{rot} %%6f seconds\n"
             "Rotation is incremented by  \\Delta R_{hat} = %%6f\n"
             " then Z bead position is averaged\n"
             "for \\tau_{rot} seconds before force reduced to F_{hat} with zmag %%6f \n"
             "The Z position of the bead is then averaged for \\tau_{hat} %%6f seconds\n"
             "Force is then increased again at F_{rot} the Z position of the bead is then\n"
             "averaged for \\tau_{rot} seconds. Rotation is then brought back\n"
             "\\delta z for servo in rotation %%6f \\delta z for 1 turn in hat %%6f\n"
             "present val %g angular position %g\n"
             "Rotation feedback no %%R simulate rot %%r compete %%r\n"
             "Number of points in the displayed plot %%8d\nNumber of points for averaging %%8d\n"
             "display zmag %%R or estimated force %%r\n"
             "Do you want to record bead profiles %%b\n"
             "Do you want real time saving %%b\n"
             , rot_start, zmag);

    i = win_scanf(question, &n_zdet, &zdet_p->bead_nb, &zdet_p->turns_to_decrement_neg
                  , &zdet_p->turns_to_decrement_pos, &zdet_p->zmag_test,
                  &zdet_p->za_test.time_avg, &zdet_p->zmag_rot, &zdet_p->za_ref0.time_avg,
                  &zdet_p->turns_to_increment, &zdet_p->zmag_hat_measure,
                  &zdet_p->za_hat.time_avg, &zdet_p->dz_servo, &zdet_p->dz_one_turn,
                  &feedback, &scope_buf_size, &fir,
                  &zmag_or_f, &with_radial_profiles, &streaming);

    if (i == WIN_CANCEL) return 0;

    bead_nb = zdet_p->bead_nb;

    Open_Pico_config_file();
    set_config_int("Zdet_Curve", "n_zdet", n_zdet);

    set_config_float("Zdet_Topo_Curve", "test_time_avg", zdet_p->za_test.time_avg);
    set_config_float("Zdet_Topo_Curve", "ref0_time_avg", zdet_p->za_ref0.time_avg);
    set_config_float("Zdet_Topo_Curve", "hat_time_avg", zdet_p->za_hat.time_avg);
    set_config_float("Zdet_Topo_Curve", "zmag_setling_time", zdet_p->zmag_setling_time);
    //set_config_float("Zdet_Topo_Curve", "z_cor", zdet_p->z_cor);
    set_config_float("Zdet_Topo_Curve", "zmag_rot", zdet_p->zmag_rot);
    set_config_float("Zdet_Topo_Curve", "zmag_test", zdet_p->zmag_test);
    set_config_float("Zdet_Topo_Curve", "zmag_hat_measure", zdet_p->zmag_hat_measure);
    set_config_float("Zdet_Topo_Curve", "dz_servo", zdet_p->dz_servo);
    set_config_float("Zdet_Topo_Curve", "dz_one_turn", zdet_p->dz_one_turn);
    set_config_float("Zdet_Topo_Curve", "turns_to_increment", zdet_p->turns_to_increment);
    set_config_float("Zdet_Topo_Curve", "turns_to_decrement_pos", zdet_p->turns_to_decrement_pos);
    set_config_float("Zdet_Topo_Curve", "turns_to_decrement_neg", zdet_p->turns_to_decrement_neg);
    write_Pico_config_file();
    Close_Pico_config_file();

    zdet_p->feedback = feedback;


    zdet_p->setling_nb_frames_in_zmag = (int)(0.5 + (zdet_p->zmag_setling_time
                                        * Pico_param.camera_param.camera_frequency_in_Hz));


    zdet_p->za_test.nb_frames_in_avg = (int)(0.5 + (zdet_p->za_test.time_avg
                                       * Pico_param.camera_param.camera_frequency_in_Hz));

    zdet_p->za_ref0.nb_frames_in_avg = (int)(0.5 + (zdet_p->za_ref0.time_avg
                                       * Pico_param.camera_param.camera_frequency_in_Hz));

    zdet_p->za_hat.nb_frames_in_avg = (int)(0.5 + (zdet_p->za_hat.time_avg
                                            * Pico_param.camera_param.camera_frequency_in_Hz));

    zdet_p->za_ref.nb_frames_in_avg = zdet_p->za_ref0.nb_frames_in_avg;
    zdet_p->za_after.nb_frames_in_avg = zdet_p->za_ref0.nb_frames_in_avg;
    zdet_p->za_ref.time_avg = zdet_p->za_ref0.time_avg;
    zdet_p->za_after.time_avg = zdet_p->za_ref0.time_avg;

    zdet_p->n_test = 1;

    /*
    win_printf("setling_nb_frames_in_zmag %d nb_frames_in_test_phase %d\n"
          "nb_frames_in_ref_phase %d nb_frames_in_hat_measure_phase %d",
          zdet_p->setling_nb_frames_in_zmag,zdet_p->za_test.nb_frames_in_avg,
          zdet_p->za_ref.nb_frames_in_avg,zdet_p->za_hat.nb_frames_in_avg);
    */


    nf = (scope_buf_size > 8192) ? 2 * scope_buf_size : 8192;

    snprintf(name, 64, "Zdet topo curve %d", n_zdet);

    // We create a plot region to display bead position, zdet  etc.
    pr = create_hidden_pltreg_with_op(&op, nf, nf, 0, name);

    if (pr == NULL)  win_printf_OK("Could not find or allocte plot region!");

    //op = pr->one_p;
    ds = op->dat[0];


    zdet_r = create_gen_record(track_info, 1, with_radial_profiles, 0, 1, 0, 1.0, 1.0, 1);
    duplicate_Pico_parameter(&Pico_param, &(zdet_r->Pico_param_record));

    if (attach_g_record_to_pltreg(pr, zdet_r))
        win_printf("Could not attach grecord to plot region!");

    snprintf(zdet_r->filename, 512, "Zdet_%d", n_zdet);
    snprintf(zdet_r->name, 512, "Zdet curve %d", n_zdet);
    zdet_r->n_rec = n_zdet;
    zdet_r->data_type |= ZDET_RECORD;
    zdet_r->record_action = topo_action_feedback;
    topo_action_feedback(0,0,0,1);
    zdet_p->z_cor = zdet_r->z_cor;

    if (streaming)
    {
        if (do_save_track(zdet_r))
            win_printf("Pb writing tracking header");
    }

    li = grab_basename_and_index(zdet_r->filename, strlen(zdet_r->filename) + 1, basename, 256);
    n_zdet = (li >= 0) ? li : n_zdet;
    //win_printf("index %d",n_zdet);
    Open_Pico_config_file();
    set_config_int("Zdet_Curve", "n_zdet", n_zdet);
    write_Pico_config_file();
    Close_Pico_config_file();


    im = track_info->imi[track_info->c_i];

    im += 64;

    //we create one plot for each bead
    for (i = li = 0; i < track_info->n_b; i++)
    {
        bt = track_info->bd[i];

        if ((bt->calib_im == NULL) || (bt->not_lost <= 0) || (bt->in_image == 0))
            continue;

        if (li)
        {
            op = create_and_attach_one_plot(pr, nf, nf, 0);
            ds = op->dat[0];
        }

        li++;
        bt->opt = op;
        ds->nx = ds->ny = 0;
        set_dot_line(ds);
        set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                      "Z coordinate l = %d, w = %d, nim = %d\n"
                      "objective %f, zoom factor %f, sample %s Rotation %g\n"
                      "Z magnets %g mm\n"
                      "Calibration from %s"
                      , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, i
                      , track_info->cl, track_info->cw, nf
                      , Pico_param.obj_param.objective_magnification
                      , Pico_param.micro_param.zoom_factor
                      , pico_sample
                      , rot_start
                      , zmag
                      , bt->calib_im->filename);

        if ((ds = create_and_attach_one_ds(op, nf / fir, nf / fir, 0)) == NULL)
            return win_printf_OK("I can't create plot !");

        ds->nx = ds->ny = 0;
        set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                      "filtred Z over %d coordinate l = %d, w = %d, nim = %d\n"
                      "objective %f, zoom factor %f, sample %s Rotation %g\n"
                      "Z magnets %g mm\n"
                      "Calibration from %s"
                      , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, i
                      , fir, track_info->cl, track_info->cw, nf
                      , Pico_param.obj_param.objective_magnification
                      , Pico_param.micro_param.zoom_factor
                      , pico_sample
                      , rot_start
                      , zmag
                      , bt->calib_im->filename);

        if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
            return win_printf_OK("I can't create plot !");

        ds->nx = ds->ny = 0;
        set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                      "Zmag l = %d, w = %d, nim = %d\n"
                      "objective %f, zoom factor %f, sample %s Rotation %g\n"
                      "Z magnets %g mm\n"
                      "Calibration from %s"
                      , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, i
                      , track_info->cl, track_info->cw, nf
                      , Pico_param.obj_param.objective_magnification
                      , Pico_param.micro_param.zoom_factor
                      , pico_sample
                      , rot_start
                      , zmag
                      , bt->calib_im->filename);


        if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
            return win_printf_OK("I can't create plot !");

        alloc_data_set_x_error(ds);
        ds->nx = ds->ny = 0;
        set_ds_dot_line(ds);
        set_ds_point_symbol(ds, "\\oc");
        set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                      "averaged Z over %d coordinate l = %d, w = %d, nim = %d\n"
                      "objective %f, zoom factor %f, sample %s Rotation %g\n"
                      "Z magnets %g mm\n"
                      "Calibration from %s"
                      , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, i
                      , fir, track_info->cl, track_info->cw, nf
                      , Pico_param.obj_param.objective_magnification
                      , Pico_param.micro_param.zoom_factor
                      , pico_sample
                      , rot_start
                      , zmag
                      , bt->calib_im->filename);
        create_attach_select_y_un_to_op(op, IS_METER, 0 , (float)1, -3, 0, "mm");
        create_attach_select_y_un_to_op(op, IS_NEWTON, 0 , (float)1, -12, 0, "pN");
        create_attach_select_y_un_to_op(op, 0, 0 , (float)1, 0, 0, "no_name");


        //uns_oi_2_op(oi, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
        if (zmag_or_f == 0)
        {
            set_plot_y_prime_title(op, "Zmag");
            op->c_yu_p = 1;
        }
        else if (zmag_or_f == 0)
        {
            set_plot_y_prime_title(op, "Estimated force");
            op->c_yu_p = 2;
        }
        else
        {
            set_plot_y_prime_title(op, "Rotation");
            op->c_yu_p = 3;
        }

        //op->c_yu_p = op->c_yu;
        create_attach_select_y_un_to_op(op, IS_METER, 0 , (float)1, -6, 0, "\\mu m");
        create_attach_select_x_un_to_op(op, IS_SECOND, 0
                                        , (float)1 / Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");

        op->op_idle_action = zdet_op_idle_action;
        op->op_post_display = zdet_op_post_display;
        set_op_filename(op, "Z(t)fil-bd%dzdet%d.gr", i, n_zdet);
        set_plot_title(op, "Bead %d Z(t) %d zmag %g", i, n_zdet, zmag);
        set_plot_x_title(op, "Time");
        set_plot_y_title(op, "Molecule extension");


        op->x_lo = (-nf / 64); ///Pico_param.camera_param.camera_frequency_in_Hz;
        op->x_hi = (nf + nf / 64); ///Pico_param.camera_param.camera_frequency_in_Hz;
        set_plot_x_fixed_range(op);
        fill_next_available_job_spot(im, im + 32 + i, COLLECT_DATA, i, imr, oi, pr, op, NULL, zdet_job_z_fil_avg, 0);
        op->user_id = 4096;
        op->user_ispare[ISPARE_BEAD_NB] = i;
        //win_printf("bd %d",i);
    }

    bt = track_info->bd[zdet_p->bead_nb];
    op = create_and_attach_one_plot(pr, nf, nf, 0);
    ds = op->dat[0];
    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_ds_point_symbol(ds, "\\oc");
    set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                  "averaged Z coordinate test point mean value at rot %g zmag %g\n "
                  "l = %d, w = %d, nim = %d\n"
                  "objective %f, zoom factor %f, sample %s Rotation %g\n"
                  "Z magnets %g mm\n"
                  "Calibration from %s"
                  , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, zdet_p->bead_nb
                  , zdet_p->rot_to_test, zdet_p->zmag_test
                  , track_info->cl, track_info->cw, nf
                  , Pico_param.obj_param.objective_magnification
                  , Pico_param.micro_param.zoom_factor
                  , pico_sample
                  , rot_start
                  , zmag
                  , bt->calib_im->filename);


    if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("I can't create plot !");

    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_ds_point_symbol(ds, "\\fs");
    set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                  "averaged Z coordinate ref0 point mean value at rot %g zmag %g\n "
                  "l = %d, w = %d, nim = %d\n"
                  "objective %f, zoom factor %f, sample %s Rotation %g\n"
                  "Z magnets %g mm\n"
                  "Calibration from %s"
                  , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, zdet_p->bead_nb
                  , zdet_p->rot_to_test, zdet_p->zmag_rot
                  , track_info->cl, track_info->cw, nf
                  , Pico_param.obj_param.objective_magnification
                  , Pico_param.micro_param.zoom_factor
                  , pico_sample
                  , rot_start
                  , zmag
                  , bt->calib_im->filename);


    if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("I can't create plot !");

    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_ds_point_symbol(ds, "\\cr");
    set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                  "averaged Z coordinate ref point mean value at rot %g zmag %g\n "
                  "l = %d, w = %d, nim = %d\n"
                  "objective %f, zoom factor %f, sample %s Rotation %g\n"
                  "Z magnets %g mm\n"
                  "Calibration from %s"
                  , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, zdet_p->bead_nb
                  , zdet_p->rot_hat, zdet_p->zmag_rot
                  , track_info->cl, track_info->cw, nf
                  , Pico_param.obj_param.objective_magnification
                  , Pico_param.micro_param.zoom_factor
                  , pico_sample
                  , rot_start
                  , zmag
                  , bt->calib_im->filename);


    if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("I can't create plot !");

    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_ds_point_symbol(ds, "\\fd");
    set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                  "averaged Z coordinate measure point mean value at rot %g zmag %g\n "
                  "l = %d, w = %d, nim = %d\n"
                  "objective %f, zoom factor %f, sample %s Rotation %g\n"
                  "Z magnets %g mm\n"
                  "Calibration from %s"
                  , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, zdet_p->bead_nb
                  , zdet_p->rot_hat, zdet_p->zmag_hat_measure
                  , track_info->cl, track_info->cw, nf
                  , Pico_param.obj_param.objective_magnification
                  , Pico_param.micro_param.zoom_factor
                  , pico_sample
                  , rot_start
                  , zmag
                  , bt->calib_im->filename);


    if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("I can't create plot !");

    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_ds_point_symbol(ds, "\\fd");
    set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                  "averaged Z coordinate after point mean value at rot %g zmag %g\n "
                  "l = %d, w = %d, nim = %d\n"
                  "objective %f, zoom factor %f, sample %s Rotation %g\n"
                  "Z magnets %g mm\n"
                  "Calibration from %s"
                  , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, zdet_p->bead_nb
                  , zdet_p->rot_hat, zdet_p->zmag_rot
                  , track_info->cl, track_info->cw, nf
                  , Pico_param.obj_param.objective_magnification
                  , Pico_param.micro_param.zoom_factor
                  , pico_sample
                  , rot_start
                  , zmag
                  , bt->calib_im->filename);



    op->op_idle_action = zdet_op_idle_action;
    op->op_post_display = zdet_op_post_display;
    set_op_filename(op, "Z(t)Avg-bd%dzdet%d.gr", zdet_p->bead_nb, n_zdet);
    set_plot_title(op, "Bead %d Averaged Z(t) %d ", zdet_p->bead_nb, n_zdet);
    set_plot_x_title(op, "Test number");
    set_plot_y_title(op, "Molecule extension change");

    zdet_p->resu = op;


    op = create_and_attach_one_plot(pr, nf, nf, 0);
    ds = op->dat[0];
    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_ds_point_symbol(ds, "\\oc");
    set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                  "averaged Z coordinate test point mean value at rot %g zmag %g\n "
                  "l = %d, w = %d, nim = %d\n"
                  "objective %f, zoom factor %f, sample %s Rotation %g\n"
                  "Z magnets %g mm\n"
                  "Calibration from %s"
                  , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, zdet_p->bead_nb
                  , zdet_p->rot_to_test, zdet_p->zmag_test
                  , track_info->cl, track_info->cw, nf
                  , Pico_param.obj_param.objective_magnification
                  , Pico_param.micro_param.zoom_factor
                  , pico_sample
                  , rot_start
                  , zmag
                  , bt->calib_im->filename);

    if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("I can't create plot !");

    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_ds_point_symbol(ds, "\\oc");
    set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                  "averaged Z coordinate test point mean value at rot %g zmag %g\n "
                  "l = %d, w = %d, nim = %d\n"
                  "objective %f, zoom factor %f, sample %s Rotation %g\n"
                  "Z magnets %g mm\n"
                  "Calibration from %s"
                  , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, zdet_p->bead_nb
                  , zdet_p->rot_to_test, zdet_p->zmag_test
                  , track_info->cl, track_info->cw, nf
                  , Pico_param.obj_param.objective_magnification
                  , Pico_param.micro_param.zoom_factor
                  , pico_sample
                  , rot_start
                  , zmag
                  , bt->calib_im->filename);


    op->op_idle_action = zdet_op_idle_action;
    op->op_post_display = zdet_op_post_display;


    set_op_filename(op, "Resu-bd%dzdet%d.gr", zdet_p->bead_nb, n_zdet);
    set_plot_title(op, "Bead %d resu %d ", zdet_p->bead_nb, n_zdet);
    set_plot_x_title(op, "Rot modulation");
    set_plot_y_title(op, "turns added");


    zdet_p->resu_valid = op;

    op = create_and_attach_one_plot(pr, nf, nf, 0);
    ds = op->dat[0];
    ds->nx = ds->ny = 0;

    set_ds_point_symbol(ds, "\\oc");
    set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                  "averaged Z coordinate test point mean value at rot %g zmag %g\n "
                  "l = %d, w = %d, nim = %d\n"
                  "objective %f, zoom factor %f, sample %s Rotation %g\n"
                  "Z magnets %g mm\n"
                  "Calibration from %s"
                  , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, zdet_p->bead_nb
                  , zdet_p->rot_to_test, zdet_p->zmag_test
                  , track_info->cl, track_info->cw, nf
                  , Pico_param.obj_param.objective_magnification
                  , Pico_param.micro_param.zoom_factor
                  , pico_sample
                  , rot_start
                  , zmag
                  , bt->calib_im->filename);

    if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("I can't create plot !");

    ds->nx = ds->ny = 0;
    set_ds_point_symbol(ds, "\\oc");
    set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                  "averaged Z coordinate test point mean value at rot %g zmag %g\n "
                  "l = %d, w = %d, nim = %d\n"
                  "objective %f, zoom factor %f, sample %s Rotation %g\n"
                  "Z magnets %g mm\n"
                  "Calibration from %s"
                  , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, zdet_p->bead_nb
                  , zdet_p->rot_to_test, zdet_p->zmag_test
                  , track_info->cl, track_info->cw, nf
                  , Pico_param.obj_param.objective_magnification
                  , Pico_param.micro_param.zoom_factor
                  , pico_sample
                  , rot_start
                  , zmag
                  , bt->calib_im->filename);


    op->op_idle_action = zdet_op_idle_action;
    op->op_post_display = zdet_op_post_display;


    set_op_filename(op, "ResuHat-bd%dzdet%d.gr", zdet_p->bead_nb, n_zdet);
    set_plot_title(op, "Bead %d resu %d ", zdet_p->bead_nb, n_zdet);
    set_plot_x_title(op, "Rotation");
    set_plot_y_title(op, "\\Delta Z");


    zdet_p->resu_hat = op;

    im = track_info->imi[track_info->c_i];
    working_gen_record = zdet_r;
    zdet_r->last_starting_pos = im;
    win_title_used = 1;
    ask_to_update_menu();
    k = fill_next_available_action(im + 1, BEAD_CROSS_SAVED, 0);

    if (k < 0)   my_set_window_title("Could not add pending action!");

    if (imr_and_pr) switch_project_to_this_pltreg(pr);

    heap_check();
    generate_imr_TRACK_title = generate_zdet_topo_title;
    return D_O_K;
}

# endif

int record_zdet_topo_feedback(void)
{
    static int first = 1, feedback = 1;
    int i, k, im, li;
    imreg *imr = NULL;
    O_i *oi = NULL;
    b_track *bt = NULL;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    static int nf = 8192, with_radial_profiles = 0, streaming = 1, bead_nb = 0;
    static float rot_start = 10, rot, zmag; // , zmag_start
    char name[64] = {0}, basename[256] = {0};
    char question[4096] = {0};

    if (updating_menu_state != 0)
    {
        if (working_gen_record != NULL || track_info->n_b == 0)
            active_menu->flags |=  D_DISABLED;
        else
        {
           if (track_info->SDI_mode == 0)
              {
                  for (i = 0, k = 0; i < track_info->n_b; i++)
                     {
                         bt = track_info->bd[i];
                         k += (bt->calib_im != NULL) ? 1 : 0;
                     }
              }
           else k = 1;
            if (k) active_menu->flags &= ~D_DISABLED;
            else active_menu->flags |=  D_DISABLED;
        }

        return D_O_K;
    }

    if (ac_grep(cur_ac_reg, "%im%oi", &imr, &oi) != 2)  return OFF;

    heap_check();

    rot_start = rot = prev_mag_rot; //read_rot_value();
    //zmag_start = zmag = prev_zmag; //read_magnet_z_value();
    zdet_p = (zdet_topo_param *)calloc(1, sizeof(zdet_topo_param));

    Open_Pico_config_file();

    if (first)
    {
        n_zdet = get_config_float("Zdet_Curve", "n_zdet", n_zdet);
        first = 0;
    }

    zdet_p->za_test.time_avg = get_config_float("Zdet_Topo_Curve", "test_time_avg", 5);
    zdet_p->za_ref0.time_avg = get_config_float("Zdet_Topo_Curve", "ref0_time_avg", 1);
    zdet_p->za_hat.time_avg = get_config_float("Zdet_Topo_Curve", "hat_time_avg", 3);
    zdet_p->zmag_setling_time = get_config_float("Zdet_Topo_Curve", "zmag_setling_time", 0.5);
    //zdet_p->z_cor = get_config_float("Zdet_Topo_Curve", "z_cor", 0.878);
    zdet_p->turns_to_increment = get_config_float("Zdet_Topo_Curve", "turns_to_increment", 10);
    zdet_p->turns_to_decrement_pos = get_config_float("Zdet_Topo_Curve", "turns_to_decrement_pos", 2);
    zdet_p->turns_to_decrement_neg = get_config_float("Zdet_Topo_Curve", "turns_to_decrement_neg", -2);
    zdet_p->zmag_rot = get_config_float("Zdet_Topo_Curve", "zmag_rot", 8);
    zdet_p->dz_servo = get_config_float("Zdet_Topo_Curve", "dz_servo", 1);
    zdet_p->dz_one_turn = get_config_float("Zdet_Topo_Curve", "dz_one_turn", 0.045);
    // the roration where we measure the molecule extension
    zdet_p->zmag_test = get_config_float("Zdet_Topo_Curve", "zmag_test", 6.5);
// the magnet zmag value where we want to test the torsional state
    zdet_p->zmag_hat_measure = get_config_float("Zdet_Topo_Curve", "zmag_hat_measure", 7.5);
    Close_Pico_config_file();
    // this is the rotation measuring phase parameters
    zdet_p->bead_nb = bead_nb;
    heap_check();


    snprintf(question, sizeof(question), "Zdet curve %%5d for topoisomerase characterisation\n"
             "This test involve 3 zmag(forces) states and 2 rotation states\n"
             "The purpose is to test the rotation state of bead %%4d in the so-called\n"
             "test phase with a rotation r_- %%6f  r_+ %%6f near 0 and a small force F_0 zmag_0 = %%6f\n"
             "this phase will last for \\tau_0 %%6f seconds \n"
             "Then the force is increased to F_{rot} zmag_{rot} %%6f\n"
             "then Z bead position is averaged for \\tau_{rot} %%6f seconds\n"
             "Rotation is incremented by  \\Delta R_{hat} = %%6f\n"
             " then Z bead position is averaged\n"
             "for \\tau_{rot} seconds before force reduced to F_{hat} with zmag %%6f \n"
             "The Z position of the bead is then averaged for \\tau_{hat} %%6f seconds\n"
             "Force is then increased again at F_{rot} the Z position of the bead is then\n"
             "averaged for \\tau_{rot} seconds. Rotation is then brought back\n"
             "\\delta z for servo in rotation %%6f \\delta z for 1 turn in hat %%6f\n"
             "present val %g angular position %g\n"
             "Rotation feedback no %%R simulate rot %%r compete %%r\n"
             "Number of points in the displayed plot %%8d\nNumber of points for averaging %%8d\n"
             "display zmag %%R or estimated force %%r\n"
             "Do you want to record bead profiles %%b\n"
             "Do you want real time saving %%b\n"
             , rot_start, zmag);

    i = win_scanf(question, &n_zdet, &zdet_p->bead_nb, &zdet_p->turns_to_decrement_neg
                  , &zdet_p->turns_to_decrement_pos, &zdet_p->zmag_test,
                  &zdet_p->za_test.time_avg, &zdet_p->zmag_rot, &zdet_p->za_ref0.time_avg,
                  &zdet_p->turns_to_increment, &zdet_p->zmag_hat_measure,
                  &zdet_p->za_hat.time_avg, &zdet_p->dz_servo, &zdet_p->dz_one_turn,
                  &feedback, &scope_buf_size, &fir,
                  &zmag_or_f, &with_radial_profiles, &streaming);

    if (i == WIN_CANCEL) return 0;

    bead_nb = zdet_p->bead_nb;

    Open_Pico_config_file();
    set_config_int("Zdet_Curve", "n_zdet", n_zdet);

    set_config_float("Zdet_Topo_Curve", "test_time_avg", zdet_p->za_test.time_avg);
    set_config_float("Zdet_Topo_Curve", "ref0_time_avg", zdet_p->za_ref0.time_avg);
    set_config_float("Zdet_Topo_Curve", "hat_time_avg", zdet_p->za_hat.time_avg);
    set_config_float("Zdet_Topo_Curve", "zmag_setling_time", zdet_p->zmag_setling_time);
    //set_config_float("Zdet_Topo_Curve", "z_cor", zdet_p->z_cor);
    set_config_float("Zdet_Topo_Curve", "zmag_rot", zdet_p->zmag_rot);
    set_config_float("Zdet_Topo_Curve", "zmag_test", zdet_p->zmag_test);
    set_config_float("Zdet_Topo_Curve", "zmag_hat_measure", zdet_p->zmag_hat_measure);
    set_config_float("Zdet_Topo_Curve", "dz_servo", zdet_p->dz_servo);
    set_config_float("Zdet_Topo_Curve", "dz_one_turn", zdet_p->dz_one_turn);
    set_config_float("Zdet_Topo_Curve", "turns_to_increment", zdet_p->turns_to_increment);
    set_config_float("Zdet_Topo_Curve", "turns_to_decrement_pos", zdet_p->turns_to_decrement_pos);
    set_config_float("Zdet_Topo_Curve", "turns_to_decrement_neg", zdet_p->turns_to_decrement_neg);
    write_Pico_config_file();
    Close_Pico_config_file();

    zdet_p->feedback = feedback;


    zdet_p->setling_nb_frames_in_zmag = (int)(0.5 + (zdet_p->zmag_setling_time
                                        * Pico_param.camera_param.camera_frequency_in_Hz));


    zdet_p->za_test.nb_frames_in_avg = (int)(0.5 + (zdet_p->za_test.time_avg
                                       * Pico_param.camera_param.camera_frequency_in_Hz));

    zdet_p->za_ref0.nb_frames_in_avg = (int)(0.5 + (zdet_p->za_ref0.time_avg
                                       * Pico_param.camera_param.camera_frequency_in_Hz));

    zdet_p->za_hat.nb_frames_in_avg = (int)(0.5 + (zdet_p->za_hat.time_avg
                                            * Pico_param.camera_param.camera_frequency_in_Hz));

    zdet_p->za_ref.nb_frames_in_avg = zdet_p->za_ref0.nb_frames_in_avg;
    zdet_p->za_after.nb_frames_in_avg = zdet_p->za_ref0.nb_frames_in_avg;
    zdet_p->za_ref.time_avg = zdet_p->za_ref0.time_avg;
    zdet_p->za_after.time_avg = zdet_p->za_ref0.time_avg;

    zdet_p->n_test = 1;

    /*
    win_printf("setling_nb_frames_in_zmag %d nb_frames_in_test_phase %d\n"
          "nb_frames_in_ref_phase %d nb_frames_in_hat_measure_phase %d",
          zdet_p->setling_nb_frames_in_zmag,zdet_p->za_test.nb_frames_in_avg,
          zdet_p->za_ref.nb_frames_in_avg,zdet_p->za_hat.nb_frames_in_avg);
    */


    nf = (scope_buf_size > 8192) ? 2 * scope_buf_size : 8192;

    snprintf(name, 64, "Zdet topo curve %d", n_zdet);

    // We create a plot region to display bead position, zdet  etc.
    pr = create_hidden_pltreg_with_op(&op, nf, nf, 0, name);

    if (pr == NULL)  win_printf_OK("Could not find or allocte plot region!");

    //op = pr->one_p;
    ds = op->dat[0];


    zdet_r = create_gen_record(track_info, 1, with_radial_profiles, 0, 1, 0, 1.0, 1.0, 1);
    duplicate_Pico_parameter(&Pico_param, &(zdet_r->Pico_param_record));

    if (attach_g_record_to_pltreg(pr, zdet_r))
        win_printf("Could not attach grecord to plot region!");

    snprintf(zdet_r->filename, 512, "Zdet_%d", n_zdet);
    snprintf(zdet_r->name, 512, "Zdet curve %d", n_zdet);
    zdet_r->n_rec = n_zdet;
    zdet_r->data_type |= ZDET_RECORD;
    zdet_r->record_action = topo_action_feedback;
    topo_action_feedback(0,0,0,1);
    zdet_p->z_cor = zdet_r->z_cor;

    if (streaming)
    {
        if (do_save_track(zdet_r))
            win_printf("Pb writing tracking header");
    }

    li = grab_basename_and_index(zdet_r->filename, strlen(zdet_r->filename) + 1, basename, 256);
    n_zdet = (li >= 0) ? li : n_zdet;
    //win_printf("index %d",n_zdet);
    Open_Pico_config_file();
    set_config_int("Zdet_Curve", "n_zdet", n_zdet);
    write_Pico_config_file();
    Close_Pico_config_file();


    im = track_info->imi[track_info->c_i];

    im += 64;

    //we create one plot for each bead
    for (i = li = 0; i < track_info->n_b; i++)
    {
        bt = track_info->bd[i];

        if ((bt->calib_im == NULL) || (bt->not_lost <= 0) || (bt->in_image == 0))
            continue;

        if (li)
        {
            op = create_and_attach_one_plot(pr, nf, nf, 0);
            ds = op->dat[0];
        }

        li++;
        bt->opt = op;
        ds->nx = ds->ny = 0;
        set_dot_line(ds);
        set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                      "Z coordinate l = %d, w = %d, nim = %d\n"
                      "objective %f, zoom factor %f, sample %s Rotation %g\n"
                      "Z magnets %g mm\n"
                      "Calibration from %s"
                      , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, i
                      , track_info->cl, track_info->cw, nf
                      , Pico_param.obj_param.objective_magnification
                      , Pico_param.micro_param.zoom_factor
                      , pico_sample
                      , rot_start
                      , zmag
                      , bt->calib_im->filename);

        if ((ds = create_and_attach_one_ds(op, nf / fir, nf / fir, 0)) == NULL)
            return win_printf_OK("I can't create plot !");

        ds->nx = ds->ny = 0;
        set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                      "filtred Z over %d coordinate l = %d, w = %d, nim = %d\n"
                      "objective %f, zoom factor %f, sample %s Rotation %g\n"
                      "Z magnets %g mm\n"
                      "Calibration from %s"
                      , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, i
                      , fir, track_info->cl, track_info->cw, nf
                      , Pico_param.obj_param.objective_magnification
                      , Pico_param.micro_param.zoom_factor
                      , pico_sample
                      , rot_start
                      , zmag
                      , bt->calib_im->filename);

        if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
            return win_printf_OK("I can't create plot !");

        ds->nx = ds->ny = 0;
        set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                      "Zmag l = %d, w = %d, nim = %d\n"
                      "objective %f, zoom factor %f, sample %s Rotation %g\n"
                      "Z magnets %g mm\n"
                      "Calibration from %s"
                      , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, i
                      , track_info->cl, track_info->cw, nf
                      , Pico_param.obj_param.objective_magnification
                      , Pico_param.micro_param.zoom_factor
                      , pico_sample
                      , rot_start
                      , zmag
                      , bt->calib_im->filename);


        if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
            return win_printf_OK("I can't create plot !");

        alloc_data_set_x_error(ds);
        ds->nx = ds->ny = 0;
        set_ds_dot_line(ds);
        set_ds_point_symbol(ds, "\\oc");
        set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                      "averaged Z over %d coordinate l = %d, w = %d, nim = %d\n"
                      "objective %f, zoom factor %f, sample %s Rotation %g\n"
                      "Z magnets %g mm\n"
                      "Calibration from %s"
                      , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, i
                      , fir, track_info->cl, track_info->cw, nf
                      , Pico_param.obj_param.objective_magnification
                      , Pico_param.micro_param.zoom_factor
                      , pico_sample
                      , rot_start
                      , zmag
                      , bt->calib_im->filename);
        create_attach_select_y_un_to_op(op, IS_METER, 0 , (float)1, -3, 0, "mm");
        create_attach_select_y_un_to_op(op, IS_NEWTON, 0 , (float)1, -12, 0, "pN");
        create_attach_select_y_un_to_op(op, 0, 0 , (float)1, 0, 0, "no_name");


        //uns_oi_2_op(oi, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
        if (zmag_or_f == 0)
        {
            set_plot_y_prime_title(op, "Zmag");
            op->c_yu_p = 1;
        }
        else if (zmag_or_f == 0)
        {
            set_plot_y_prime_title(op, "Estimated force");
            op->c_yu_p = 2;
        }
        else
        {
            set_plot_y_prime_title(op, "Rotation");
            op->c_yu_p = 3;
        }

        //op->c_yu_p = op->c_yu;
        create_attach_select_y_un_to_op(op, IS_METER, 0 , (float)1, -6, 0, "\\mu m");
        create_attach_select_x_un_to_op(op, IS_SECOND, 0
                                        , (float)1 / Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");

        op->op_idle_action = zdet_op_idle_action;
        op->op_post_display = zdet_op_post_display;
        set_op_filename(op, "Z(t)fil-bd%dzdet%d.gr", i, n_zdet);
        set_plot_title(op, "Bead %d Z(t) %d zmag %g", i, n_zdet, zmag);
        set_plot_x_title(op, "Time");
        set_plot_y_title(op, "Molecule extension");


        op->x_lo = (-nf / 64); ///Pico_param.camera_param.camera_frequency_in_Hz;
        op->x_hi = (nf + nf / 64); ///Pico_param.camera_param.camera_frequency_in_Hz;
        set_plot_x_fixed_range(op);
        fill_next_available_job_spot(im, im + 32 + i, COLLECT_DATA, i, imr, oi, pr, op, NULL, zdet_job_z_fil_avg, 0);
        op->user_id = 4096;
        op->user_ispare[ISPARE_BEAD_NB] = i;
        //win_printf("bd %d",i);
    }

    bt = track_info->bd[zdet_p->bead_nb];
    op = create_and_attach_one_plot(pr, nf, nf, 0);
    ds = op->dat[0];
    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_ds_point_symbol(ds, "\\oc");
    set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                  "averaged Z coordinate test point mean value at rot %g zmag %g\n "
                  "l = %d, w = %d, nim = %d\n"
                  "objective %f, zoom factor %f, sample %s Rotation %g\n"
                  "Z magnets %g mm\n"
                  "Calibration from %s"
                  , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, zdet_p->bead_nb
                  , zdet_p->rot_to_test, zdet_p->zmag_test
                  , track_info->cl, track_info->cw, nf
                  , Pico_param.obj_param.objective_magnification
                  , Pico_param.micro_param.zoom_factor
                  , pico_sample
                  , rot_start
                  , zmag
                  , bt->calib_im->filename);


    if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("I can't create plot !");

    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_ds_point_symbol(ds, "\\fs");
    set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                  "averaged Z coordinate ref0 point mean value at rot %g zmag %g\n "
                  "l = %d, w = %d, nim = %d\n"
                  "objective %f, zoom factor %f, sample %s Rotation %g\n"
                  "Z magnets %g mm\n"
                  "Calibration from %s"
                  , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, zdet_p->bead_nb
                  , zdet_p->rot_to_test, zdet_p->zmag_rot
                  , track_info->cl, track_info->cw, nf
                  , Pico_param.obj_param.objective_magnification
                  , Pico_param.micro_param.zoom_factor
                  , pico_sample
                  , rot_start
                  , zmag
                  , bt->calib_im->filename);


    if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("I can't create plot !");

    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_ds_point_symbol(ds, "\\cr");
    set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                  "averaged Z coordinate ref point mean value at rot %g zmag %g\n "
                  "l = %d, w = %d, nim = %d\n"
                  "objective %f, zoom factor %f, sample %s Rotation %g\n"
                  "Z magnets %g mm\n"
                  "Calibration from %s"
                  , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, zdet_p->bead_nb
                  , zdet_p->rot_hat, zdet_p->zmag_rot
                  , track_info->cl, track_info->cw, nf
                  , Pico_param.obj_param.objective_magnification
                  , Pico_param.micro_param.zoom_factor
                  , pico_sample
                  , rot_start
                  , zmag
                  , bt->calib_im->filename);


    if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("I can't create plot !");

    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_ds_point_symbol(ds, "\\fd");
    set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                  "averaged Z coordinate measure point mean value at rot %g zmag %g\n "
                  "l = %d, w = %d, nim = %d\n"
                  "objective %f, zoom factor %f, sample %s Rotation %g\n"
                  "Z magnets %g mm\n"
                  "Calibration from %s"
                  , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, zdet_p->bead_nb
                  , zdet_p->rot_hat, zdet_p->zmag_hat_measure
                  , track_info->cl, track_info->cw, nf
                  , Pico_param.obj_param.objective_magnification
                  , Pico_param.micro_param.zoom_factor
                  , pico_sample
                  , rot_start
                  , zmag
                  , bt->calib_im->filename);


    if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("I can't create plot !");

    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_ds_point_symbol(ds, "\\fd");
    set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                  "averaged Z coordinate after point mean value at rot %g zmag %g\n "
                  "l = %d, w = %d, nim = %d\n"
                  "objective %f, zoom factor %f, sample %s Rotation %g\n"
                  "Z magnets %g mm\n"
                  "Calibration from %s"
                  , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, zdet_p->bead_nb
                  , zdet_p->rot_hat, zdet_p->zmag_rot
                  , track_info->cl, track_info->cw, nf
                  , Pico_param.obj_param.objective_magnification
                  , Pico_param.micro_param.zoom_factor
                  , pico_sample
                  , rot_start
                  , zmag
                  , bt->calib_im->filename);



    op->op_idle_action = zdet_op_idle_action;
    op->op_post_display = zdet_op_post_display;
    set_op_filename(op, "Z(t)Avg-bd%dzdet%d.gr", zdet_p->bead_nb, n_zdet);
    set_plot_title(op, "Bead %d Averaged Z(t) %d ", zdet_p->bead_nb, n_zdet);
    set_plot_x_title(op, "Test number");
    set_plot_y_title(op, "Molecule extension change");

    zdet_p->resu = op;


    op = create_and_attach_one_plot(pr, nf, nf, 0);
    ds = op->dat[0];
    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_ds_point_symbol(ds, "\\oc");
    set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                  "averaged Z coordinate test point mean value at rot %g zmag %g\n "
                  "l = %d, w = %d, nim = %d\n"
                  "objective %f, zoom factor %f, sample %s Rotation %g\n"
                  "Z magnets %g mm\n"
                  "Calibration from %s"
                  , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, zdet_p->bead_nb
                  , zdet_p->rot_to_test, zdet_p->zmag_test
                  , track_info->cl, track_info->cw, nf
                  , Pico_param.obj_param.objective_magnification
                  , Pico_param.micro_param.zoom_factor
                  , pico_sample
                  , rot_start
                  , zmag
                  , bt->calib_im->filename);

    if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("I can't create plot !");

    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_ds_point_symbol(ds, "\\oc");
    set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                  "averaged Z coordinate test point mean value at rot %g zmag %g\n "
                  "l = %d, w = %d, nim = %d\n"
                  "objective %f, zoom factor %f, sample %s Rotation %g\n"
                  "Z magnets %g mm\n"
                  "Calibration from %s"
                  , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, zdet_p->bead_nb
                  , zdet_p->rot_to_test, zdet_p->zmag_test
                  , track_info->cl, track_info->cw, nf
                  , Pico_param.obj_param.objective_magnification
                  , Pico_param.micro_param.zoom_factor
                  , pico_sample
                  , rot_start
                  , zmag
                  , bt->calib_im->filename);


    op->op_idle_action = zdet_op_idle_action;
    op->op_post_display = zdet_op_post_display;


    set_op_filename(op, "Resu-bd%dzdet%d.gr", zdet_p->bead_nb, n_zdet);
    set_plot_title(op, "Bead %d resu %d ", zdet_p->bead_nb, n_zdet);
    set_plot_x_title(op, "Rot modulation");
    set_plot_y_title(op, "turns added");


    zdet_p->resu_valid = op;

    op = create_and_attach_one_plot(pr, nf, nf, 0);
    ds = op->dat[0];
    ds->nx = ds->ny = 0;

    set_ds_point_symbol(ds, "\\oc");
    set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                  "averaged Z coordinate test point mean value at rot %g zmag %g\n "
                  "l = %d, w = %d, nim = %d\n"
                  "objective %f, zoom factor %f, sample %s Rotation %g\n"
                  "Z magnets %g mm\n"
                  "Calibration from %s"
                  , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, zdet_p->bead_nb
                  , zdet_p->rot_to_test, zdet_p->zmag_test
                  , track_info->cl, track_info->cw, nf
                  , Pico_param.obj_param.objective_magnification
                  , Pico_param.micro_param.zoom_factor
                  , pico_sample
                  , rot_start
                  , zmag
                  , bt->calib_im->filename);

    if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("I can't create plot !");

    ds->nx = ds->ny = 0;
    set_ds_point_symbol(ds, "\\oc");
    set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                  "averaged Z coordinate test point mean value at rot %g zmag %g\n "
                  "l = %d, w = %d, nim = %d\n"
                  "objective %f, zoom factor %f, sample %s Rotation %g\n"
                  "Z magnets %g mm\n"
                  "Calibration from %s"
                  , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, zdet_p->bead_nb
                  , zdet_p->rot_to_test, zdet_p->zmag_test
                  , track_info->cl, track_info->cw, nf
                  , Pico_param.obj_param.objective_magnification
                  , Pico_param.micro_param.zoom_factor
                  , pico_sample
                  , rot_start
                  , zmag
                  , bt->calib_im->filename);


    op->op_idle_action = zdet_op_idle_action;
    op->op_post_display = zdet_op_post_display;


    set_op_filename(op, "ResuHat-bd%dzdet%d.gr", zdet_p->bead_nb, n_zdet);
    set_plot_title(op, "Bead %d resu %d ", zdet_p->bead_nb, n_zdet);
    set_plot_x_title(op, "Rotation");
    set_plot_y_title(op, "\\Delta Z");


    zdet_p->resu_hat = op;

    im = track_info->imi[track_info->c_i];
    working_gen_record = zdet_r;
    zdet_r->last_starting_pos = im;
    win_title_used = 1;
    ask_to_update_menu();
    k = fill_next_available_action(im + 1, BEAD_CROSS_SAVED, 0);

    if (k < 0)   my_set_window_title("Could not add pending action!");

    if (imr_and_pr) switch_project_to_this_pltreg(pr);

    heap_check();
    generate_imr_TRACK_title = generate_zdet_topo_title;
    return D_O_K;
}

int zdet_topo_test(void)
{
    if (updating_menu_state != 0)
    {
        if (working_gen_record == NULL)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }

    if (working_gen_record != NULL)
    {
        if (zdet_p != NULL)
            zdet_p->state = ZDET_SINGLE_TEST;

        if (zoli_p != NULL)
            zoli_p->state = ZDET_SINGLE_TEST;
    }

    return D_O_K;
}

int zdet_topo_run(void)
{
    if (updating_menu_state != 0)
    {
        if (working_gen_record == NULL)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }

    if (working_gen_record != NULL)
    {
        if (zdet_p != NULL)
            zdet_p->state = ZDET_RUNNING;

        if (zoli_p != NULL)
            zoli_p->state = ZDET_RUNNING;

        if (current_write_open_path_picofile != NULL)
            dump_to_html_log_file_with_date_and_time(current_write_open_picofile, "Z(t) recording started<br/>");
    }

    return D_O_K;
}


int zdet_topo_stop(void)
{
    if (updating_menu_state != 0)
    {
        if (working_gen_record == NULL)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }

    if (working_gen_record != NULL)
    {
        if (zdet_p != NULL && zdet_p->state > 0)
            zdet_p->state = ZDET_SINGLE_TEST;

        if (zoli_p != NULL && zoli_p->state > 0)
            zoli_p->state = ZDET_SINGLE_TEST;;

        if (current_write_open_path_picofile != NULL)
            dump_to_html_log_file_with_date_and_time(current_write_open_picofile, "Z(t) recording stopped<br>");
    }

    return D_O_K;
}


int zdet_topo_dummy_test(void)
{
    if (updating_menu_state != 0)
    {
        if (working_gen_record == NULL)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }

    if (working_gen_record != NULL)
    {
        if (zdet_p != NULL)
            zdet_p->state = ZDET_SINGLE_TEST_DUMMY;

        if (zoli_p != NULL)
            zoli_p->state = ZDET_SINGLE_TEST_DUMMY;

        if (current_write_open_path_picofile != NULL)
            dump_to_html_log_file_with_date_and_time(current_write_open_picofile, "Z(t) recording single test<br>");
    }

    return D_O_K;
}


int zdet_topo_param_select(void)
{
    if (updating_menu_state != 0)
    {
        if (working_gen_record == NULL)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }

    if (working_gen_record != NULL)
    {
        zmag_or_f = RETRIEVE_MENU_INDEX;
    }

    return D_O_K;
}


int record_zdet_oligo(void)
{
    static int first = 1;
    int i, k, im, li;
    imreg *imr;
    O_i *oi;
    b_track *bt = NULL;
    pltreg *pr;
    O_p *op;
    d_s *ds;
    static int nf = 8192, with_radial_profiles = 0, streaming = 1, bead_nb = 0, file_selection = 1;
    static float rot_start = 10, zmag_start, zmag;
    char name[64] = {0}, basename[256] = {0}, fullfilename[512] = {0};
    char question[4096] = {0};

    if (updating_menu_state != 0)
    {
        if (working_gen_record != NULL || track_info->n_b == 0)
            active_menu->flags |=  D_DISABLED;
        else
        {
           if (track_info->SDI_mode == 0)
              {
                  for (i = 0, k = 0; i < track_info->n_b; i++)
                     {
                         bt = track_info->bd[i];
                         k += (bt->calib_im != NULL) ? 1 : 0;
                     }
              }
           else k = 1;
            if (k) active_menu->flags &= ~D_DISABLED;
            else active_menu->flags |=  D_DISABLED;
        }

        return D_O_K;
    }

    if (ac_grep(cur_ac_reg, "%im%oi", &imr, &oi) != 2)  return OFF;

    heap_check();
    rot_start = prev_mag_rot;
    zmag_start = prev_zmag;
    //rot_start = rot = read_rot_value();
    //zmag_start = zmag = read_magnet_z_value();
    zoli_p = (zdet_oligo_param *)calloc(1, sizeof(zdet_oligo_param));

    Open_Pico_config_file();

    if (first)
    {
        n_zdet = get_config_float("Zdet_Curve", "n_zdet", n_zdet);
        first = 0;
    }

    zoli_p->za_ref0.time_avg = get_config_float("Zdet_Oligo", "test_time_avg", 2);
    zoli_p->zmag_setling_time = get_config_float("Zdet_Oligo", "zmag_setling_time", 0.5);
    zoli_p->zmag_test = get_config_float("Zdet_Oligo", "zmag_test", 21);
    zoli_p->z_dt.z_threshold = get_config_float("Zdet_Oligo", "z_threshold", 0.3);
    // the roration where we measure the molecule extension
    zoli_p->zmag_unzip = get_config_float("Zdet_Oligo", "zmag_unzip", 21.83);
// the magnet zmag value where we want to test the torsional state
    Close_Pico_config_file();
    // this is the rotation measuring phase parameters
    zoli_p->bead_nb = bead_nb;
    heap_check();
    n_zdet = find_trk_index_not_used("Zdet_", n_zdet);

    if (current_write_open_path_picofile != NULL)
    {
        snprintf(fullfilename, 512, "%s\\Zdet_%d.trk", current_write_open_path_picofile, n_zdet);
        backslash_to_slash(fullfilename);
        file_selection = 0;
    }
    else
    {
        snprintf(fullfilename, 512, "Zdet_%d.trk", n_zdet);
        backslash_to_slash(fullfilename);
        file_selection = 1;
    }


    snprintf(question, sizeof(question), "Zdet curve %%5d for oligo hybridization characterisation\n"
             "This test involve 2 zmag(forces) states\n"
             "The purpose is to test the hybridization state of bead %%4d in the so-called\n"
             "test phase force F_0 zmag_0 = %%6f\n"
             "this phase will last for \\tau_0 %%6f seconds \n"
             "Then the force is increased to F_{unzip} zmag_{unzip} %%6f\n"
             "then Z bead position is averaged for \\tau_0\n"
             "seconds before force is reduced back to to F_{test} \n"
             "The duration of the hybridization is define by the time the bead Z position\n"
             "stays above a threshold value (compared to the ref0) threshold %%6f microns\n"
             "present val %g angular position %g\n"
             "Number of points in the displayed plot %%8d\n"
             "Number of points for averaging %%8d\n"
             "display zmag %%R or estimated force %%r\n"
             "\\fbox{Track will be saved in %s\n"
             "Do you want to select a new file location %%R No %%r Yes\n"
             "Do you want to record bead profiles %%b\n"
             "Do you want real time saving %%b\n}"
             , rot_start, zmag, fullfilename);

    i = win_scanf(question, &n_zdet, &zoli_p->bead_nb, &zoli_p->zmag_test,
                  &zoli_p->za_ref0.time_avg, &zoli_p->zmag_unzip,
                  &zoli_p->z_dt.z_threshold, &scope_buf_size, &fir,
                  &zmag_or_f, &file_selection, &with_radial_profiles, &streaming);

    if (i == WIN_CANCEL) return 0;

    bead_nb = zoli_p->bead_nb;
    Open_Pico_config_file();
    set_config_int("Zdet_Curve", "n_zdet", n_zdet);

    set_config_float("Zdet_Oligo", "test_time_avg", zoli_p->za_ref0.time_avg);
    set_config_float("Zdet_Oligo", "zmag_setling_time", zoli_p->zmag_setling_time);
    set_config_float("Zdet_Oligo", "zmag_test", zoli_p->zmag_test);
    set_config_float("Zdet_Oligo", "z_threshold", zoli_p->z_dt.z_threshold);
    // the roration where we measure the molecule extension
    set_config_float("Zdet_Oligo", "zmag_unzip", zoli_p->zmag_unzip);
    write_Pico_config_file();
    Close_Pico_config_file();

    zoli_p->z_dt.direction = -1;


    zoli_p->setling_nb_frames_in_zmag = (int)(0.5 + (zoli_p->zmag_setling_time
                                        * Pico_param.camera_param.camera_frequency_in_Hz));


    zoli_p->za_ref0.nb_frames_in_avg = (int)(0.5 + (zoli_p->za_ref0.time_avg
                                       * Pico_param.camera_param.camera_frequency_in_Hz));

    zoli_p->za_unzip.nb_frames_in_avg = zoli_p->za_after.nb_frames_in_avg = zoli_p->za_ref0.nb_frames_in_avg;
    zoli_p->za_unzip.time_avg = zoli_p->za_after.time_avg = zoli_p->za_ref0.time_avg;

    zoli_p->n_test = 1;

    /*
    win_printf("setling_nb_frames_in_zmag %d nb_frames_in_test_phase %d\n"
          "nb_frames_in_ref_phase %d nb_frames_in_hat_measure_phase %d",
          zoli_p->setling_nb_frames_in_zmag,zoli_p->za_test.nb_frames_in_avg,
          zoli_p->za_ref.nb_frames_in_avg,zoli_p->za_hat.nb_frames_in_avg);
    */


    nf = (scope_buf_size > 8192) ? 2 * scope_buf_size : 8192;

    snprintf(name, 64, "Zdet oligo curve %d", n_zdet);

    // We create a plot region to display bead position, zdet  etc.
    pr = create_hidden_pltreg_with_op(&op, nf, nf, 0, name);

    if (pr == NULL)  win_printf_OK("Could not find or allocte plot region!");

    //op = pr->one_p;
    ds = op->dat[0];


    zdet_r = create_gen_record(track_info, 1, with_radial_profiles, 0, 1, 0, 1.0, 1.0, 1);
    duplicate_Pico_parameter(&Pico_param, &(zdet_r->Pico_param_record));

    if (attach_g_record_to_pltreg(pr, zdet_r))
        win_printf("Could not attach grecord to plot region!");

    snprintf(zdet_r->filename, 512, "Zdet_%d", n_zdet);
    snprintf(zdet_r->name, 512, "Zdet curve %d", n_zdet);
    zdet_r->n_rec = n_zdet;
    zdet_r->data_type |= ZDET_RECORD;
    zdet_r->record_action = oligo_action;
    oligo_action(0,0,0,1);
    zoli_p->z_cor = zdet_r->z_cor;

    if (streaming)
    {
        if (current_write_open_path_picofile != NULL && file_selection == 0)
        {
            strncpy(zdet_r->path, current_write_open_path_picofile, 512);
            write_record_file_header(zdet_r, oi_TRACK);
            zdet_r->real_time_saving = 1;
        }
        else if (do_save_track(zdet_r))
            win_printf("Pb writing tracking header");
    }



    if (current_write_open_path_picofile != NULL)
    {
        dump_to_html_log_file_with_date_and_time(current_write_open_picofile,
                "<h2>Z(t) Oligo recording</h2>"
                "<br>in file %s in directory %s"
                "<br>zmag start %g, rot start %g <br>\n"
                , backslash_to_slash(zdet_r->filename), backslash_to_slash(zdet_r->path)
                , zmag_start, rot_start);
    }




    li = grab_basename_and_index(zdet_r->filename, strlen(zdet_r->filename) + 1, basename, 256);
    n_zdet = (li >= 0) ? li : n_zdet;

    Open_Pico_config_file();
    set_config_int("Zdet_Curve", "n_zdet", n_zdet);
    write_Pico_config_file();
    Close_Pico_config_file();



    im = track_info->imi[track_info->c_i];

    im += 64;

    //we create one plot for each bead
    for (i = li = 0; i < track_info->n_b; i++)
    {
        bt = track_info->bd[i];

        if ((bt->calib_im == NULL) || (bt->not_lost <= 0) || (bt->in_image == 0))
            continue;

        if (li)
        {
            op = create_and_attach_one_plot(pr, nf, nf, 0);
            ds = op->dat[0];
        }

        li++;
        bt->opt = op;
        ds->nx = ds->ny = 0;
        set_dot_line(ds);
        set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                      "Z coordinate l = %d, w = %d, nim = %d\n"
                      "objective %f, zoom factor %f, sample %s Rotation %g\n"
                      "Z magnets %g mm\n"
                      "Calibration from %s"
                      , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, i
                      , track_info->cl, track_info->cw, nf
                      , Pico_param.obj_param.objective_magnification
                      , Pico_param.micro_param.zoom_factor
                      , pico_sample
                      , rot_start
                      , zmag
                      , bt->calib_im->filename);

        if ((ds = create_and_attach_one_ds(op, nf / fir, nf / fir, 0)) == NULL)
            return win_printf_OK("I can't create plot !");

        ds->nx = ds->ny = 0;
        set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                      "filtred Z over %d coordinate l = %d, w = %d, nim = %d\n"
                      "objective %f, zoom factor %f, sample %s Rotation %g\n"
                      "Z magnets %g mm\n"
                      "Calibration from %s"
                      , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, i
                      , fir, track_info->cl, track_info->cw, nf
                      , Pico_param.obj_param.objective_magnification
                      , Pico_param.micro_param.zoom_factor
                      , pico_sample
                      , rot_start
                      , zmag
                      , bt->calib_im->filename);

        if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
            return win_printf_OK("I can't create plot !");

        ds->nx = ds->ny = 0;
        set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                      "Zmag l = %d, w = %d, nim = %d\n"
                      "objective %f, zoom factor %f, sample %s Rotation %g\n"
                      "Z magnets %g mm\n"
                      "Calibration from %s"
                      , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, i
                      , track_info->cl, track_info->cw, nf
                      , Pico_param.obj_param.objective_magnification
                      , Pico_param.micro_param.zoom_factor
                      , pico_sample
                      , rot_start
                      , zmag
                      , bt->calib_im->filename);


        if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
            return win_printf_OK("I can't create plot !");

        alloc_data_set_x_error(ds);
        ds->nx = ds->ny = 0;
        set_ds_dot_line(ds);
        set_ds_point_symbol(ds, "\\oc");
        set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                      "averaged Z over %d coordinate l = %d, w = %d, nim = %d\n"
                      "objective %f, zoom factor %f, sample %s Rotation %g\n"
                      "Z magnets %g mm\n"
                      "Calibration from %s"
                      , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, i
                      , fir, track_info->cl, track_info->cw, nf
                      , Pico_param.obj_param.objective_magnification
                      , Pico_param.micro_param.zoom_factor
                      , pico_sample
                      , rot_start
                      , zmag
                      , bt->calib_im->filename);
        create_attach_select_y_un_to_op(op, IS_METER, 0 , (float)1, -3, 0, "mm");
        create_attach_select_y_un_to_op(op, IS_NEWTON, 0 , (float)1, -12, 0, "pN");
        create_attach_select_y_un_to_op(op, 0, 0 , (float)1, 0, 0, "no_name");


        //uns_oi_2_op(oi, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
        if (zmag_or_f == 0)
        {
            set_plot_y_prime_title(op, "Zmag");
            op->c_yu_p = 1;
        }
        else if (zmag_or_f == 0)
        {
            set_plot_y_prime_title(op, "Estimated force");
            op->c_yu_p = 2;
        }
        else
        {
            set_plot_y_prime_title(op, "Rotation");
            op->c_yu_p = 3;
        }

        //op->c_yu_p = op->c_yu;
        create_attach_select_y_un_to_op(op, IS_METER, 0 , (float)1, -6, 0, "\\mu m");
        create_attach_select_x_un_to_op(op, IS_SECOND, 0
                                        , (float)1 / Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");

        op->op_idle_action = zdet_op_idle_action;
        op->op_post_display = zdet_op_post_display;
        set_op_filename(op, "Z(t)fil-bd%dzdet%d.gr", i, n_zdet);
        set_plot_title(op, "Bead %d Z(t) %d zmag %g", i, n_zdet, zmag);
        set_plot_x_title(op, "Time");
        set_plot_y_title(op, "Molecule extension");


        op->x_lo = (-nf / 64); ///Pico_param.camera_param.camera_frequency_in_Hz;
        op->x_hi = (nf + nf / 64); ///Pico_param.camera_param.camera_frequency_in_Hz;
        set_plot_x_fixed_range(op);
        fill_next_available_job_spot(im, im + 32 + i, COLLECT_DATA, i, imr, oi, pr, op, NULL, zdet_job_z_fil_avg, 0);
        op->user_id = 4096;
        op->user_ispare[ISPARE_BEAD_NB] = i;
        //win_printf("bd %d",i);
    }

    bt = track_info->bd[zoli_p->bead_nb];
    op = create_and_attach_one_plot(pr, nf, nf, 0);
    ds = op->dat[0];
    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_ds_point_symbol(ds, "\\oc");
    set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                  "averaged Z coordinate ref0 point mean value at zmag %g\n "
                  "l = %d, w = %d, nim = %d\n"
                  "objective %f, zoom factor %f, sample %s Rotation %g\n"
                  "Z magnets %g mm\n"
                  "Calibration from %s"
                  , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, zoli_p->bead_nb
                  , zoli_p->zmag_test
                  , track_info->cl, track_info->cw, nf
                  , Pico_param.obj_param.objective_magnification
                  , Pico_param.micro_param.zoom_factor
                  , pico_sample
                  , rot_start
                  , zmag
                  , bt->calib_im->filename);


    if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("I can't create plot !");

    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_ds_point_symbol(ds, "\\fs");
    set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                  "averaged Z coordinate unzip point mean value a zmag %g\n "
                  "l = %d, w = %d, nim = %d\n"
                  "objective %f, zoom factor %f, sample %s Rotation %g\n"
                  "Z magnets %g mm\n"
                  "Calibration from %s"
                  , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, zoli_p->bead_nb
                  , zoli_p->zmag_unzip
                  , track_info->cl, track_info->cw, nf
                  , Pico_param.obj_param.objective_magnification
                  , Pico_param.micro_param.zoom_factor
                  , pico_sample
                  , rot_start
                  , zmag
                  , bt->calib_im->filename);


    if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("I can't create plot !");

    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_ds_point_symbol(ds, "\\cr");
    set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                  "averaged Z coordinate after point mean value at zmag %g\n "
                  "l = %d, w = %d, nim = %d\n"
                  "objective %f, zoom factor %f, sample %s Rotation %g\n"
                  "Z magnets %g mm\n"
                  "Calibration from %s"
                  , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, zoli_p->bead_nb
                  , zoli_p->zmag_test
                  , track_info->cl, track_info->cw, nf
                  , Pico_param.obj_param.objective_magnification
                  , Pico_param.micro_param.zoom_factor
                  , pico_sample
                  , rot_start
                  , zmag
                  , bt->calib_im->filename);


    op->op_idle_action = zdet_op_idle_action;
    op->op_post_display = zdet_op_post_display;
    set_op_filename(op, "Z(t)Avg-bd%dzdet%d.gr", zoli_p->bead_nb, n_zdet);
    set_plot_title(op, "Bead %d Averaged Z(t) %d ", zoli_p->bead_nb, n_zdet);
    set_plot_x_title(op, "Test number");
    set_plot_y_title(op, "Molecule extension change");
    create_attach_select_y_un_to_op(op, IS_METER, 0 , (float)1, -6, 0, "\\mu m");
    zoli_p->resu = op;


    op = create_and_attach_one_plot(pr, nf, nf, 0);
    ds = op->dat[0];
    ds->nx = ds->ny = 0;

    set_dot_line(ds);
    set_ds_point_symbol(ds, "\\fd");
    set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
                  "mean time of hybridization between zmag %g and zmag %g\n "
                  "l = %d, w = %d, nim = %d\n"
                  "objective %f, zoom factor %f, sample %s Rotation %g\n"
                  "Z magnets %g mm\n"
                  "Calibration from %s"
                  , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, zoli_p->bead_nb
                  , zoli_p->zmag_unzip, zoli_p->zmag_test
                  , track_info->cl, track_info->cw, nf
                  , Pico_param.obj_param.objective_magnification
                  , Pico_param.micro_param.zoom_factor
                  , pico_sample
                  , rot_start
                  , zmag
                  , bt->calib_im->filename);

    op->op_idle_action = zdet_op_idle_action;
    op->op_post_display = zdet_op_post_display;
    set_op_filename(op, "Z(t)dt-bd%dzdet%d.gr", zoli_p->bead_nb, n_zdet);
    set_plot_title(op, "Bead %d Hybridization time %d ", zoli_p->bead_nb, n_zdet);
    set_plot_x_title(op, "Test number");
    set_plot_y_title(op, "Time");

    create_attach_select_y_un_to_op(op, IS_SECOND, 0
                                    , (float)1 / Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");

    zoli_p->resu_dt = op;



    im = track_info->imi[track_info->c_i];
    working_gen_record = zdet_r;
    zdet_r->last_starting_pos = im;
    win_title_used = 1;
    ask_to_update_menu();
    k = fill_next_available_action(im + 1, BEAD_CROSS_SAVED, 0);

    if (k < 0)   my_set_window_title("Could not add pending action!");

    if (imr_and_pr) switch_project_to_this_pltreg(pr);

    heap_check();
    generate_imr_TRACK_title = generate_zdet_topo_title;
    return D_O_K;
}



# ifdef ENCOURS




int add_new_point_to_oli_time_lapse_n_bead(int plot_n, int i_bead)
{
    if (zoli_p->aresu_dt != NULL && zoli_p->aresu_dt[i_bead] != NULL && zoli_p->aresu_dt[i_bead]->n_dat > plot_n
            && zoli_p->aresu_dt[i_bead]->dat[plot_n] != NULL)
    {
        add_new_point_to_ds(zoli_p->aresu_dt[i_bead]->dat[plot_n], zoli_p->n_test,
                            zoli_p->az_dt[i_bead]->nb_frames_in_avg);
        zoli_p->aresu_dt[i_bead]->need_to_refresh = 1;
    }

    if (zoli_p->resu_dt != NULL && zoli_p->resu_dt->n_dat > plot_n
            && zoli_p->resu_dt->dat[plot_n] != NULL)
    {
        add_new_point_to_ds(zoli_p->resu_dt->dat[plot_n], zoli_p->n_test,
                            zoli_p->az_dt[i_bead]->nb_frames_in_avg);
        zoli_p->resu_dt->need_to_refresh = 1;
    }

    return 0;
}



int add_new_point_to_oli_resu_n_bead(int plot_n)
{
    int i;
    float z = 0;


    for (i = 0; i < zoli_p->n_bead; i++)
    {
        if (zoli_p->aresu != NULL && zoli_p->aresu[i] != NULL && zoli_p->aresu[i]->n_dat > plot_n
                && zoli_p->aresu[i]->dat[plot_n] != NULL)
        {
            if (plot_n == 0)       z = zoli_p->aza_ref0[i]->zbead;
            else if (plot_n == 1)  z = zoli_p->aza_unzip[i]->zbead;
            else if (plot_n == 2)  z = zoli_p->az_dt[i]->zbead;
            else if (plot_n == 2)  z = zoli_p->aza_after[i]->zbead;
            else if (plot_n == 3)  z = zoli_p->aza_lowf[i]->zbead;
            else return 0;

            add_new_point_to_ds(zoli_p->aresu[i]->dat[plot_n], zoli_p->n_test,
                                zoli_p->z_cor * z);
            zoli_p->aresu[i]->need_to_refresh = 1;
        }
    }

    return 0;
}



int average_Z_action_n_bead(int im, int c_i, zdet_avg **z_avg, int n_zavg, g_record *g_r, int init_to_zero)
{
    static int status = 0;
    int  i, page_n, i_page, abs_pos = 0;

    (void)c_i;
    if (init_to_zero)
      {
	status = 0;
	return 0;
      }
    abs_pos = (g_r->abs_pos < 0) ? 0 : g_r->abs_pos;
    page_n = abs_pos / g_r->page_size;
    i_page = abs_pos % g_r->page_size;


    if (status == 0)
    {
        for (i = 0; i < n_zavg; i++)
        {
            z_avg[i]->ending_im = im + z_avg[i]->nb_frames_in_avg + 2;
            z_avg[i]->rec_start = abs_pos;
        }

        g_r->action_status[page_n][i_page] |= DATA_AVERAGING;
        status++;
    }
    else if (status == 1)
    {
        if (im < z_avg[0]->ending_im)     return 0;  // we are not yet done

        g_r->action_status[page_n][i_page] &= ~DATA_AVERAGING;

        for (i = 0; i < n_zavg; i++)
        {
            z_avg[i]->rec_end = abs_pos;
            z_avg[i]->zbead_valid = 0;

            if (track_info->bd[i]->calib_im != NULL)
            {
                if (average_bead_z_during_part_trk(g_r, i, z_avg[i]->rec_start, z_avg[i]->nb_frames_in_avg,
                                                   &(z_avg[i]->xbead), &(z_avg[i]->ybead), &(z_avg[i]->zbead),
                                                   &(z_avg[i]->zobj), &(z_avg[i]->profile_invalid)) < 0)
                    my_set_window_title("Could not grab bead %d ref position im %d !", i, z_avg[i]->ending_im);
                else
                {
                    z_avg[i]->zbead_valid = 1;
                    //my_set_window_title("Data averaging Z = %g", z_avg[i]->zbead);
                }
            }
            else z_avg[i]->zbead_valid = 1;
        }

        imr_TRACK_title_change_asked++;
        status = 0;
        return 1;
    }

    return 0;
}


int zdet_stay_in_range_action_n_bead(int im, int c_i,  zdet_stay_in_range **z_dt, int n_bead, g_record *g_r,
                                     zdet_avg **z_ref)
{
    int  i, page_n, i_page, abs_pos = 0, end = 0, n_end = 0, n_left = 0;
    b_record *b_r = NULL;
    //  static int debug_sta = 0;

    if (zoli_p == NULL)  return 0;

    (void)c_i;

    abs_pos = (g_r->abs_pos < 0) ? 0 : g_r->abs_pos;
    page_n = abs_pos / g_r->page_size;
    i_page = abs_pos % g_r->page_size;

    /*
    # ifdef RT_DEBUG
    debug_sta++;
    if (debug_sta > 80)
      {
          debug_sta = 0;
    return 1;
      }
    else return 0;
    # endif
    */

    for (i = 0; i < n_bead; i++)
    {
        if (z_dt[i]->status != 2) n_left++;
    }

    for (i = 0; i < n_bead; i++)
    {
        if (z_dt[i]->status == 0)
        {
            g_r->action_status[page_n][i_page] |= DATA_AVERAGING;
            z_dt[i]->ending_im = im;
            z_dt[i]->rec_start = abs_pos;
            z_dt[i]->status++;
            //      debug_sta = 0;
        }
        else if (z_dt[i]->status == 2)
        {
            n_end++;
        }
        else if (z_dt[i]->status == 1)
        {
            end = 0;
            b_r = g_r->b_r[i];

            if ( (track_info->bd[i]->calib_im == NULL) && (track_info->SDI_mode != 1) )
            {
                z_dt[i]->status = 2;
                continue;
            }

            if (track_info->bd[i]->fixed_bead)   end = 1;
            else if (z_dt[i]->direction >= 0)
            {
                if (g_r->z_cor * ((b_r->z[page_n][i_page] - z_ref[i]->zbead)) < z_dt[i]->z_threshold)
                {
                    end = 1;
                }

                snprintf(ti_debug, 256, "{dir > 0 Z %f ref %f thres %f}{bead %d end %d}", //  %f unzip
                         g_r->z_cor * b_r->z[page_n][i_page], g_r->z_cor * z_ref[i]->zbead,
                         //g_r->z_cor*zoli_p->aza_unzip[i]->zbead,
                         z_dt[i]->z_threshold,
                         i, end);
                //RT_debug("%s\n", ti_debug);
            }
            else
            {
                if (g_r->z_cor * ((b_r->z[page_n][i_page]  - z_ref[i]->zbead)) > z_dt[i]->z_threshold)
                {
                    end = 1;
                }

                snprintf(ti_debug, 256, "{dir < 0 Z %f ref %f thres %f}{bead %d end %d}", // unzip %f
                         g_r->z_cor * b_r->z[page_n][i_page], g_r->z_cor * z_ref[i]->zbead,
                         //g_r->z_cor*zoli_p->aza_unzip[i]->zbead,
                         z_dt[i]->z_threshold,
                         i, end);
                //RT_debug("%s\n", ti_debug);
            }

            if (abs_pos - z_dt[i]->rec_start > z_dt[i]->nb_frames_max)       end = 1;

            if (end == 1)
            {
                z_dt[i]->status = 2;
                n_end++;
                n_left--;
                z_dt[i]->rec_end = abs_pos;
                z_dt[i]->nb_frames_in_avg = z_dt[i]->rec_end - z_dt[i]->rec_start;
                z_dt[i]->zbead_valid = 0;

                if (z_dt[i]->nb_frames_in_avg <= 0)
                {
                    z_dt[i]->nb_frames_in_avg = 1;
                    z_dt[i]->rec_end = z_dt[i]->rec_start + 1;
                }

                z_dt[i]->ending_im += z_dt[i]->nb_frames_in_avg;

                if (average_bead_z_during_part_trk(g_r, i, z_dt[i]->rec_start, z_dt[i]->nb_frames_in_avg,
                                                   &(z_dt[i]->xbead), &(z_dt[i]->ybead), &(z_dt[i]->zbead),
                                                   &(z_dt[i]->zobj), &(z_dt[i]->profile_invalid)) < 0)
                    my_set_window_title("Could not grab bead %d ref position im %d !", z_dt[i]->n_bead, z_dt[i]->ending_im);
                else
                {
                    z_dt[i]->zbead_valid = 1;
                    my_set_window_title("%d oligos bound Data averaging Z[%d] = %g", n_left - 1, i, g_r->z_cor * z_dt[i]->zbead);


                    if (zoli_p->aresu != NULL && zoli_p->aresu[i] != NULL && zoli_p->aresu[i]->n_dat > 2
                            && zoli_p->aresu[i]->dat[2] != NULL)
                    {
                        add_new_point_to_ds(zoli_p->aresu[i]->dat[2], zoli_p->n_test,
                                            zoli_p->z_cor * zoli_p->az_dt[i]->zbead);
                        zoli_p->aresu[i]->need_to_refresh = 1;

                    }

                    if (zoli_p->aresu_dt != NULL && zoli_p->aresu_dt[i] != NULL && zoli_p->aresu_dt[i]->n_dat > 0
                            && zoli_p->aresu_dt[i]->dat[0] != NULL)
                    {
                        add_new_point_to_ds(zoli_p->aresu_dt[i]->dat[0], zoli_p->n_test,
                                            zoli_p->az_dt[i]->nb_frames_in_avg);
                        zoli_p->aresu_dt[i]->need_to_refresh = 1;
                    }

                    if (zoli_p->resu_dt != NULL && zoli_p->resu_dt->n_dat > 0
                            && zoli_p->resu_dt->dat[0] != NULL)
                    {
                        add_new_point_to_ds(zoli_p->resu_dt->dat[0], ((float)i / n_bead) + zoli_p->n_test,
                                            zoli_p->az_dt[i]->nb_frames_in_avg);
                        zoli_p->resu_dt->need_to_refresh = 1;
                    }

                }

                imr_TRACK_title_change_asked++;
            }
        }
    }

    zoli_p->n_left = n_bead - n_end;

#ifdef SDI_VERSION //tv debug
	static int count=0;
	count++;
  //my_set_window_title("rec start %d max fr%d", z_dt[0]->rec_start, z_dt[0]->nb_frames_max);
  //zoli_p->z_dt.nb_frames_ma
  //my_set_window_title("count wait %d",count);
  if (count > z_dt[0]->nb_frames_max)
	{
		count=0;
		return 1;
	}
#else
    if (n_end >= n_bead)
    {
        for (i = 0; i < n_bead; i++)
            z_dt[i]->status = 0;

        g_r->action_status[page_n][i_page] &= ~DATA_AVERAGING;
        return 1;
    }
#endif
    return 0;
}

# ifdef KEIR

int do_phase_action(int im, int c_i, zdet_phase *zph)
{
}


int gerenic_phase_action_n_bead(int im, int c_i, int aci)
{
    static float zmag;
    static int suspended = 0;
    int  page_n, i_page, abs_pos, tmpi;

    if (zdet_r == NULL || zphase_p == NULL)  return 0;

    if (zphase_p->state == ZDET_SUSPENDED) suspended = 1;

    if (zphase_p->state != ZDET_RUNNING && zphase_p->state != ZDET_SINGLE_TEST_DUMMY
            && zphase_p->state != ZDET_SINGLE_TEST)  return 0;



    abs_pos = (zdet_r->abs_pos < 0) ? 0 : zdet_r->abs_pos;
    page_n = abs_pos / zdet_r->page_size;
    i_page = abs_pos % zdet_r->page_size;


    record_event_number_and_phase(zdet_r, abs_pos, zphase_p->n_test, zphase_p->test_progress);

    //tmpi = do_phase_action(im, c_i, zphase_p->zph[zphase_p->test_progress]);

    if (zphase_p->zph[zphase_p->test_progress] & ZMAG_SET_PHASE)
    {
        tmpi = zmag_action_and_wait(im, c_i, zphase_p->zmag_aimed, zphase_p->nb_frames_in_avg);
    }
    else if (zphase_p->zph[zphase_p->test_progress] & ROT_SET_PHASE)
    {
    }
    else if (zphase_p->zph[zphase_p->test_progress] & OBJ_SET_PHASE)
    {
    }
    else if (zphase_p->zph[zphase_p->test_progress] & Z_MEASURE_PHASE)
    {
      tmpi = average_Z_action_n_bead(im, c_i, zphase_p->aza_ref0, zphase_p->n_bead, zdet_r,0));
    }
    else if (zphase_p->zph[zphase_p->test_progress] & WAIT_SINGLE_BEAD_Z_CHANGE)
    {

    }
    else if (zphase_p->zph[zphase_p->test_progress] & WAIT_MULTI_BEADs_Z_CHANGE)
    {
        if (zdet_stay_in_range_action_n_bead(im,  c_i,  zphase_p->az_dt, zphase_p->n_bead, zdet_r,
                                             zphase_p->aza_unzip)) // _ref0
        }



    if (tmpi)
    {

        zphase_p->test_progress++;

        if (zphase_p->test_progress >= zphase_p->n_phase)
            zphase_p->n_test++;

        RT_debug("Phasego-0 %d-%d\n", zphase_p->n_test, zphase_p->test_progress);
    }




    zphase_p->test_progress += tmpi;

    if (zphase_p->test_progress >= zphase_p->n_phase)
    {
        zphase_p->test_progress = 0;
        RT_debug("Phasego-8 %d-%d avg end unzip z %f dur %d\n", zphase_p->n_test, zphase_p->test_progress,
                 zphase_p->aza_after[0]->zbead, zphase_p->aza_after[0]->nb_frames_in_avg);
        zdet_r->action_status[page_n][i_page] = 0;

        if (zphase_p->state == ZDET_SINGLE_TEST_DUMMY
                || zphase_p->state == ZDET_SINGLE_TEST)  zphase_p->state = 0;
    }


    if (zphase_p->test_progress == 0)   // we start the test by moving zmag to test value
    {

    }
    else if (zphase_p->test_progress == 1)   // we average ref value
    {
        my_set_window_title("phase 1 im %d averaging", im);

        if (average_Z_action_n_bead(im, c_i, zphase_p->aza_ref0, zphase_p->n_bead, zdet_r, 0))
        {
            zphase_p->test_progress++;
            add_new_point_to_phase_resu_n_bead(0);
            RT_debug("Phasego-1 %d-%d avg end z0 %f\n", zphase_p->n_test, zphase_p->test_progress, zphase_p->aza_ref0[0]->zbead);
        }
    }
    else if (zphase_p->test_progress == 2)   // we record test
    {
        //      my_set_window_title("                                                  phase 2 im %d zmag %f",im, zphase_p->zmag_unzip);
        if (zmag_action_and_wait(im, c_i, zphase_p->zmag_unzip, zphase_p->setling_nb_frames_in_zmag))
        {
            zphase_p->test_progress++;
            RT_debug("Phasego-2 %d-%d\n", zphase_p->n_test, zphase_p->test_progress);
        }
    }
    else if (zphase_p->test_progress == 3)   // we record unzip state
    {
      if (average_Z_action_n_bead(im, c_i, zphase_p->aza_unzip, zphase_p->n_bead, zdet_r, 0))
        {
            zphase_p->test_progress++;
            my_set_window_title("phase 3 im %d averaging  val %f dur %d", im, zphase_p->aza_unzip[0]->zbead,
                                zphase_p->aza_unzip[0]->nb_frames_in_avg);
            RT_debug("Phasego-3 %d-%d avg end unzip z %f dur %d\n", zphase_p->n_test, zphase_p->test_progress,
                     zphase_p->aza_unzip[0]->zbead, zphase_p->aza_unzip[0]->nb_frames_in_avg);

            add_new_point_to_phase_resu_n_bead(1);
        }
    }
    else if (zphase_p->test_progress == 4)   // we set zmag to the measurement value
    {
        my_set_window_title("phase 2 im %d zmag %f", im, zphase_p->zmag_test);

        if (zmag_action_and_wait(im, c_i, zphase_p->zmag_test, 2))
        {
            zphase_p->test_progress++;
            RT_debug("Phasego-4 %d-%d\n", zphase_p->n_test, zphase_p->test_progress);
        }
    }
    else if (zphase_p->test_progress == 5)   // we record ref0
    {
        my_set_window_title("phase 5 im %d waiting", im);

        if (zdet_stay_in_range_action_n_bead(im,  c_i,  zphase_p->az_dt, zphase_p->n_bead, zdet_r,
                                             zphase_p->aza_unzip)) // _ref0
        {
            zphase_p->test_progress++;
            RT_debug("Phasego-5 %d-%d avg end z0 %f\n", zphase_p->n_test, zphase_p->test_progress, zphase_p->aza_ref0[0]->zbead);
        }
    }
    else if (zphase_p->test_progress == 6)   // we gow to low force
    {
        //      my_set_window_title("                                                  phase 2 im %d zmag %f",im, zphase_p->zmag_unzip);
        if (zmag_action_and_wait(im, c_i, zphase_p->zmag_lowf, zphase_p->setling_nb_frames_in_zmag))
        {
            zphase_p->test_progress++;
            RT_debug("Phasego-6 %d-%d\n", zphase_p->n_test, zphase_p->test_progress);
        }
    }
    else if (zphase_p->test_progress == 7)   // we record unzip state
    {
      if (average_Z_action_n_bead(im, c_i, zphase_p->aza_lowf, zphase_p->n_bead, zdet_r, 0))
        {
            zphase_p->test_progress++;
            my_set_window_title("phase 7 im %d averaging  val %f dur %d", im, zphase_p->aza_lowf[0]->zbead,
                                zphase_p->aza_lowf[0]->nb_frames_in_avg);
            RT_debug("Phasego-7 %d-%d avg end lowf z %f dur %d\n", zphase_p->n_test, zphase_p->test_progress,
                     zphase_p->aza_lowf[0]->zbead, zphase_p->aza_lowf[0]->nb_frames_in_avg);
            add_new_point_to_phase_resu_n_bead(3);
        }
    }
    else if (zphase_p->test_progress == 8)   // we record unzip state
    {
        my_set_window_title("phase 6 im %d averaging", im);

        if (average_Z_action_n_bead(im, c_i, zphase_p->aza_after, zphase_p->n_bead, zdet_r, 0))
        {
            zphase_p->test_progress = 0;
            RT_debug("Phasego-8 %d-%d avg end unzip z %f dur %d\n", zphase_p->n_test, zphase_p->test_progress,
                     zphase_p->aza_after[0]->zbead, zphase_p->aza_after[0]->nb_frames_in_avg);
            //add_new_point_to_resu(3, &zphase_p->za_after);
            zdet_r->action_status[page_n][i_page] = 0;

            if (zphase_p->state == ZDET_SINGLE_TEST_DUMMY
                    || zphase_p->state == ZDET_SINGLE_TEST)  zphase_p->state = 0;
        }
    }

    return 0;
}

# endif


int oligo_action_n_bead(int im, int c_i, int aci, int init_to_zero)
{
    static float zmag;
    static int  vchg = 0, disp = 0;//, suspended = 0;
    int  page_n, i_page, abs_pos, tmpi, k;

    (void)aci;
    if (init_to_zero)
    {
		  vchg = 0;
		  disp = 0;
		  zmag_action_with_velocity_and_wait(0, 0, 0, 0, 0, 1);
		  zmag_speed_action_for_next_move(0, 0, 0, 1);
		  average_Z_action_n_bead(0, 0, NULL, 0, NULL, 1);
		  return 0;
	  }

    if (zdet_r == NULL || zoli_p == NULL)  return 0;

    if (zoli_p->state != ZDET_RUNNING && zoli_p->state != ZDET_SINGLE_TEST_DUMMY && zoli_p->state != ZDET_SINGLE_TEST)  return 0;

    abs_pos = (zdet_r->abs_pos < 0) ? 0 : zdet_r->abs_pos;
    page_n = abs_pos / zdet_r->page_size;
    i_page = abs_pos % zdet_r->page_size;
    record_event_number_and_phase(zdet_r, abs_pos, zoli_p->n_test, zoli_p->test_progress);

    if (zoli_p->test_progress == 0)   // we start the test by moving zmag to test value
    {
      if (zoli_p->ramp_obj && ((zoli_p->n_test % zoli_p->obj_change_period) == 0) && (zoli_p->obj_moved == 0))
      {
        k = fill_next_available_action(im+1, MV_OBJ_REL, zoli_p->ramp_obj);
        if (k < 0)	win_printf_OK("Could not add pending action!");
        zoli_p->obj_moved = 1;

      }
      else{

      zmag = convert_force_to_zmag(zmag_to_force(0,zoli_p->zmag_test) +zoli_p->ramp_test*((zoli_p->n_test)/zoli_p->ramp_test_period),0);
      if (zmag == -25) zmag = zoli_p->zmag_test;
		  if (zoli_p->test_progress == disp)
		  {
			   RT_debug("Oligo-0 zm_a_w_w zmag=%f, vz=%f \n", zmag, zoli_p->v_zmag_lowf_to_test);
			   disp++;
		  }
      my_set_window_title("phase 0 im %d", im);

		  tmpi = zmag_action_with_velocity_and_wait(im, c_i, zmag, zoli_p->v_zmag_lowf_to_test, zoli_p->setling_nb_frames_in_zmag, 0);
        //tmpi = zmag_action_and_wait( im, c_i, zmag, zoli_p->setling_nb_frames_in_zmag);
		      vchg = 0;
		  zoli_p->test_progress += tmpi;
      if (tmpi)
      {
        zoli_p->n_test++;
        zoli_p->obj_moved = 0;
        RT_debug("Oligo-0 %d-%d\n", zoli_p->n_test, zoli_p->test_progress);
      }
    }
    }
    else if (zoli_p->test_progress == 1)   // we average ref value
    {
      my_set_window_title("phase 1 im %d", im);
	      if (zoli_p->test_progress == disp)
	      {
	        RT_debug("Oligo-1 zm_s_a_f_n_m  vz=%f \n", zoli_p->v_zmag_test_to_unzip);
	        disp++;
	      }
        if (vchg == 0) vchg = zmag_speed_action_for_next_move(im, c_i, zoli_p->v_zmag_test_to_unzip, 0);
        if (average_Z_action_n_bead(im, c_i, zoli_p->aza_ref0, zoli_p->n_bead, zdet_r, 0))
        {
            zoli_p->test_progress++;
            add_new_point_to_oli_resu_n_bead(0);
            RT_debug("Oligo-1 %d-%d avg end z0 %f\n", zoli_p->n_test, zoli_p->test_progress, zoli_p->aza_ref0[0]->zbead);
        }
    }
    else if (zoli_p->test_progress == 2)   // we record test
    {
      my_set_window_title("phase 2 im %d",im);
        vchg = 0;
	      if (zoli_p->test_progress == disp)
	      {
	        RT_debug("Oligo-2 zm_a_w_w zmag=%f, vz=%f \n", zoli_p->zmag_unzip, zoli_p->v_zmag_test_to_unzip);
	        disp++;
	      }
        if (zmag_action_with_velocity_and_wait(im, c_i, zoli_p->zmag_unzip, zoli_p->v_zmag_test_to_unzip, zoli_p->setling_nb_frames_in_zmag,0))
        {
            zoli_p->test_progress++;
            RT_debug("Oligo-2 %d-%d\n", zoli_p->n_test, zoli_p->test_progress);
        }
    }
    else if (zoli_p->test_progress == 3)   // we record unzip state and prepare velocity for next phase
    {
      my_set_window_title("phase 3 im %d",im);
      if (zoli_p->test_progress == disp)
	    {
	      RT_debug("Oligo-3 zm_s_a_f_n_m  vz=%f \n", zoli_p->v_zmag_unzip_to_test);
	      disp++;
	    }
      if (vchg == 0) vchg = zmag_speed_action_for_next_move(im, c_i, zoli_p->v_zmag_unzip_to_test,0);
      if (average_Z_action_n_bead(im, c_i, zoli_p->aza_unzip, zoli_p->n_bead, zdet_r, 0))
        {
            zoli_p->test_progress++;
            my_set_window_title("phase 3 im %d averaging  val %f dur %d", im, zoli_p->aza_unzip[0]->zbead,
                                zoli_p->aza_unzip[0]->nb_frames_in_avg);
            RT_debug("Oligo-3 %d-%d avg end unzip z %f dur %d\n", zoli_p->n_test, zoli_p->test_progress,
                     zoli_p->aza_unzip[0]->zbead, zoli_p->aza_unzip[0]->nb_frames_in_avg);

            add_new_point_to_oli_resu_n_bead(1);
        }
    }
    else if (zoli_p->test_progress == 4)   // we set zmag to the measurement value
    {

        vchg = 0;
        my_set_window_title("phase 4 im %d",im);
        zmag = convert_force_to_zmag(zmag_to_force(0,zoli_p->zmag_test) +zoli_p->ramp_test*((zoli_p->n_test)/zoli_p->ramp_test_period),0);
        if (zmag == -25) zmag = zoli_p->zmag_test;
	if (zoli_p->test_progress == disp)
	  {
	    RT_debug("Oligo-4 zm_a_w_w zmag=%f, vz=%f \n", zmag);
	    disp++;
	  }
        if (zmag_action_with_velocity_and_wait(im, c_i, zmag, zoli_p->v_zmag_unzip_to_test, 2,0))
        {
            zoli_p->test_progress++;
            RT_debug("Oligo-4 %d-%d\n", zoli_p->n_test, zoli_p->test_progress);
        }
    }
    else if (zoli_p->test_progress == 5)   // we record ref0
    {
      if (zoli_p->test_progress == disp)
	     {
	        RT_debug("Oligo-5 zm_s_a_f_n_m  vz=%f \n", zoli_p->v_zmag_test_to_lowf);
	         disp++;
	      }
      if (vchg == 0) vchg = zmag_speed_action_for_next_move(im, c_i, zoli_p->v_zmag_test_to_lowf, 0);

        my_set_window_title("phase 5 im %d waiting", im);
        if (zdet_stay_in_range_action_n_bead(im,  c_i,  zoli_p->az_dt, zoli_p->n_bead, zdet_r, zoli_p->aza_unzip)) // _ref0
        {
            zoli_p->test_progress ++ ;
            RT_debug("Oligo-5 %d-%d avg end z0 %f\n", zoli_p->n_test, zoli_p->test_progress, zoli_p->aza_ref0[0]->zbead);
        }
    }
    else if (zoli_p->test_progress == 6)   // we gow to low force
    {
        vchg = 0;
	if (zoli_p->test_progress == disp)
	  {
	    RT_debug("Oligo-6 zm_a_w_w zmag=%f, vz=%f \n", zoli_p->zmag_lowf, zoli_p->v_zmag_test_to_lowf);
	    disp++;
	  }

    if (zoli_p->low_force_period == 0 || ((zoli_p->n_test % zoli_p->low_force_period) == 0))
    {
        //      my_set_window_title("                                                  phase 2 im %d zmag %f",im, zoli_p->zmag_unzip);
        if (zmag_action_with_velocity_and_wait(im, c_i, zoli_p->zmag_lowf, zoli_p->v_zmag_test_to_lowf,zoli_p->setling_nb_frames_in_zmag,0))
        {
            zoli_p->test_progress++;
            RT_debug("Oligo-6 %d-%d\n", zoli_p->n_test, zoli_p->test_progress);
        }
      }
        else {
          zmag = convert_force_to_zmag(zmag_to_force(0,zoli_p->zmag_test) +zoli_p->ramp_test*((zoli_p->n_test)/zoli_p->ramp_test_period),0);
          if (zmag == -25) zmag = zoli_p->zmag_test;
          if (zmag_action_with_velocity_and_wait(im, c_i, zmag, zoli_p->v_zmag_unzip_to_test, 2,0))
          {
            zoli_p->test_progress++;
            RT_debug("Oligo-6 %d-%d\n", zoli_p->n_test, zoli_p->test_progress);
          }
        }

  }
    else if (zoli_p->test_progress == 7)   // we record unzip state
    {
      if (zoli_p->test_progress == disp)
	{
	  RT_debug("Oligo-7 zm_s_a_f_n_m  vz=%f \n", zoli_p->v_zmag_lowf_to_test);
	  disp++;
	}
      //if (vchg == 0) vchg = zmag_speed_action_for_next_move(im, c_i, zoli_p->v_zmag_lowf_to_test, 0);
      if (zoli_p->low_force_period == 0 || ((zoli_p->n_test % zoli_p->low_force_period) == 0))
      {
        if (average_Z_action_n_bead(im, c_i, zoli_p->aza_lowf, zoli_p->n_bead, zdet_r, 0))
          {
              zoli_p->test_progress++;
              //my_set_window_title("phase 7 im %d averaging  val %f dur %d", im, zoli_p->aza_lowf[0]->zbead,
              //                    zoli_p->aza_lowf[0]->nb_frames_in_avg);
              RT_debug("Oligo-7 %d-%d avg end lowf z %f dur %d\n", zoli_p->n_test, zoli_p->test_progress, zoli_p->aza_lowf[0]->zbead,
                       zoli_p->aza_lowf[0]->nb_frames_in_avg);
              add_new_point_to_oli_resu_n_bead(3);
          }
        }
          else {
            zmag = convert_force_to_zmag(zmag_to_force(0,zoli_p->zmag_test) +zoli_p->ramp_test*((zoli_p->n_test)/zoli_p->ramp_test_period),0);
            if (zmag == -25) zmag = zoli_p->zmag_test;
            if (zmag_action_with_velocity_and_wait(im, c_i, zmag, zoli_p->v_zmag_unzip_to_test, 2,0))
            {
              zoli_p->test_progress++;
              //my_set_window_title("phase 7 im %d averaging  val %f dur %d", im, zoli_p->aza_lowf[0]->zbead,
              //                    zoli_p->aza_lowf[0]->nb_frames_in_avg);
              RT_debug("Oligo-7 %d-%d avg end lowf z %f dur %d\n", zoli_p->n_test, zoli_p->test_progress, zoli_p->aza_lowf[0]->zbead,
                       zoli_p->aza_lowf[0]->nb_frames_in_avg);
              add_new_point_to_oli_resu_n_bead(3);
            }
          }

    }
    else if (zoli_p->test_progress == 8)   // we record unzip state
    {
        my_set_window_title("phase 8 im %d averaging", im);
        if (average_Z_action_n_bead(im, c_i, zoli_p->aza_after, zoli_p->n_bead, zdet_r, 0))
        {
            zoli_p->test_progress = 0;
	           disp = 0;
            RT_debug("Oligo-8 %d-%d avg end unzip z %f dur %d\n", zoli_p->n_test, zoli_p->test_progress,
                     zoli_p->aza_after[0]->zbead, zoli_p->aza_after[0]->nb_frames_in_avg);
            //add_new_point_to_resu(3, &zoli_p->za_after);
            zdet_r->action_status[page_n][i_page] = 0;

            if (zoli_p->state == ZDET_SINGLE_TEST_DUMMY || zoli_p->state == ZDET_SINGLE_TEST)
            {
                if (zoli_p->zmag_speed_origin > 0)
                {
                    // we restore zmag speed
                    //tv add-on : possibility of changing vel of zmag without serial interface
                    k = immediate_next_available_action(CHG_ZMAG_PID, zoli_p->zmag_speed_origin);
                    //if (k < 0)    my_set_window_title("Could not add pending action!");
                    action_pending[k].spare = 7; //speed pid
                }

                zoli_p->state = 0;
            }
        }
    }
    return 0;
}



int record_zdet_oligo_n_bead(void)
{
    static int first = 1;
    int i, k, im, li;
    imreg *imr = NULL;
    O_i *oi = NULL;
    b_track *bt = NULL;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    static int nf = 8192, with_radial_profiles = 0, streaming = 1, bead_nb = 0, file_selection = 1, keep_result = 1;
    static float rot_start = 10, zmag_start, zmag;
    char name[64] = {0}, basename[256] = {0}, fullfilename[2048] = {0}, fullconfigfile[2048] = {0};
    char question[4096] = {0};
    float vz_org;
    int add_ramp = 0;
    int enter_force ;
    int focus_change = 0;
    int exceptionnal_low_force = 0;

    if (updating_menu_state != 0)
    {
        if (working_gen_record != NULL || track_info->n_b == 0) active_menu->flags |=  D_DISABLED;
        else
        {
           if (track_info->SDI_mode == 0) //only if non sdi tracking we care about non empty calibration image
              {
                  for (i = 0, k = 0; i < track_info->n_b; i++)
                     {
                         bt = track_info->bd[i];
                         k += (bt->calib_im != NULL) ? 1 : 0;
                     }
              }
           else k = 1;
            if (k) active_menu->flags &= ~D_DISABLED;
            else active_menu->flags |=  D_DISABLED;
        }
        return D_O_K;
    }

    if (ac_grep(cur_ac_reg, "%im%oi", &imr, &oi) != 2)  return OFF;

    heap_check();
    rot_start = prev_mag_rot;
    zmag_start = prev_zmag;
    //rot_start = rot = read_rot_value();
    //zmag_start = zmag = read_magnet_z_value();
    zoli_p = (zdet_oligo_param *)calloc(1, sizeof(zdet_oligo_param));

    if (set_zmag_motor_speed != NULL || get_zmag_motor_speed != NULL)
    {
  #if defined(SDI_VERSION_2)
  vz_org = -1;

# elif defined(PI_C843) || defined(PI_C843_TRS) || defined(ZMAG_APT) || defined(RAWHID_V2)
        vz_org = get_zmag_motor_speed();
# else
        vz_org = get_zmag_motor_speed_from_pid();
# endif
        zoli_p->zmag_speed_origin = vz_org;
    }
    else   zoli_p->zmag_speed_origin = -1;
    //win_printf_OK("zdet oligo action vz_org %f", vz_org);
    Open_Pico_config_file();
    if (first)
    {
        n_zdet = get_config_float("Zdet_Curve", "n_zdet", n_zdet);
        first = 0;
    }

    zoli_p->za_ref0.time_avg = get_config_float("Zdet_Oligo", "test_time_avg", 2);
    zoli_p->za_unzip.time_avg = get_config_float("Zdet_Oligo", "unzip_time_avg", 1);
    zoli_p->z_dt.max_duration = get_config_float("Zdet_Oligo", "hyb_max_duration", 20);
    zoli_p->zmag_setling_time = get_config_float("Zdet_Oligo", "zmag_setling_time", 0.5);
    zoli_p->zmag_test = get_config_float("Zdet_Oligo", "zmag_test", 21);
    zoli_p->v_zmag_lowf_to_test = get_config_float("Zdet_Oligo", "v_zmag_lowf_to_test", 10);
    zoli_p->z_dt.z_threshold = get_config_float("Zdet_Oligo", "z_threshold", 0.3);
    // the roration where we measure the molecule extension
    zoli_p->zmag_unzip = get_config_float("Zdet_Oligo", "zmag_unzip", 21.83);
    zoli_p->v_zmag_test_to_unzip = get_config_float("Zdet_Oligo", "v_zmag_test_to_unzip", 10);
    zoli_p->v_zmag_unzip_to_test = get_config_float("Zdet_Oligo", "v_zmag_unzip_to_test", 10);
    zoli_p->zmag_lowf = get_config_float("Zdet_Oligo", "zmag_lowf", 21.83);
    zoli_p->v_zmag_test_to_lowf = get_config_float("Zdet_Oligo", "v_zmag_test_to_lowf", 10);
    zoli_p->za_lowf.time_avg = get_config_float("Zdet_Oligo", "lowf_time_avg", 1);
    zoli_p->ramp_test = 0;
    zoli_p->ramp_test_period = 1;
    zoli_p->ramp_obj = 0;
    zoli_p->obj_change_period = 1;
    zoli_p->obj_moved = 0;
    zoli_p->low_force_period = 0;
// the magnet zmag value where we want to test the torsional state
    Close_Pico_config_file();
    // this is the rotation measuring phase parameters
    zoli_p->bead_nb = bead_nb;
    heap_check();
    n_zdet = find_trk_index_not_used("Zdet_", n_zdet);

# ifdef PICOTWIST
    if (current_write_open_path_picofile != NULL)
      {
	snprintf(fullfilename, sizeof(fullfilename), "%s\\_Zdet_%d.trk", current_write_open_path_picofile,
		 n_zdet);
        backslash_to_slash(fullfilename);
        file_selection = 0;
      }
    else
      {
	snprintf(fullfilename, sizeof(fullfilename), "Zdet_%d.trk",  n_zdet);
        backslash_to_slash(fullfilename);
        file_selection = 1;
      }
# else
    change_expt_config();
    if (current_write_open_path_picofile != NULL)
      {
	snprintf(fullfilename, sizeof(fullfilename), "%s\\%s_Zdet_%d.trk", current_write_open_path_picofile,
                 Pico_param.expt_param.experiment_filename, n_zdet);
        backslash_to_slash(fullfilename);
        file_selection = 0;
      }
    else
      {
	snprintf(fullfilename, sizeof(fullfilename), "%s_Zdet_%d.trk", Pico_param.expt_param.experiment_filename, n_zdet);
        backslash_to_slash(fullfilename);
        file_selection = 1;
      }
# endif

win_scanf("Do you want to enter zmag -> %R or force -> %r\n",&enter_force);

float force_test = zmag_to_force(0,zoli_p->zmag_test);
float force_unzip = zmag_to_force(0,zoli_p->zmag_unzip);
float force_lowf = zmag_to_force(0,zoli_p->zmag_lowf);

if (enter_force)
{
      snprintf(question, sizeof(question), "Zdet curve %%5d for oligo hybridization characterisation\n"
             "The purpose is to test the hybridization state of multiple beads\n"
             "This test involve 3 zmag(forces) states.\n"
             "In the so-called test phase force F_0 = %%6f pN reached with a speed %%6f\n"
             "this phase will last for \\tau_0 %%6f seconds to record a reference.\n"
             "Then the force is increased to F_{unzip} %%6f pN with a\n"
             "speed %%6f. The Z bead position is averaged for \\tau_u %%6f seconds\n"
             "to measure the extended molecule. The force is then reduced back to to F_{test},\n"
             "with a speed %%6f, the hybridizations block the rezipping, this phase is define as\n"
             "long as the molecule extension stays above a threshold value (compared to the ref0)\n"
             "threshold %%6f microns. The maximum duration of the hybridization is define by \n"
             "\\tau_{test} %%8f this time should be long enough so that all oligo detach\n"
             "Specify a low force to remove oligos %%6f pN going there with speed %%6f\n"
             "Specify the time spent at low force  \\tau_{low] = %%6f\n"
             "present Zmag value %g mm and angular position %g tr.\n"
             "Number of points in the displayed plot %%8d\n"
             "Number of points for averaging %%8d\n"
             "display zmag %%R or estimated force %%r\n"
             "display result plot %%b\n"
             "\\fbox{Track will be saved in %s\n"
             "Do you want to select a new file location %%R No %%r Yes\n"
             "Do you want to add focus change %%b\n"
             "Do you want to record bead profiles %%b\n"
             "Do you want real time saving %%b\n"
             "Do you want to add a ramp for F_test %%b\n"
              "Do you want to make low force exceptionnal ? %%b\n"
             , rot_start, zmag, fullfilename);

    i = win_scanf(question, &n_zdet, &force_test, &zoli_p->v_zmag_lowf_to_test,
                  &zoli_p->za_ref0.time_avg, &force_unzip, &zoli_p->v_zmag_test_to_unzip,
                  &zoli_p->za_unzip.time_avg, &zoli_p->v_zmag_unzip_to_test,
                  &zoli_p->z_dt.z_threshold, &zoli_p->z_dt.max_duration,
                  &force_lowf, &zoli_p->v_zmag_test_to_lowf, &zoli_p->za_lowf.time_avg,
                  &scope_buf_size, &fir,
                  &zmag_or_f, &keep_result, &file_selection,&focus_change, &with_radial_profiles, &streaming,&add_ramp,&exceptionnal_low_force);




    zoli_p->zmag_test = convert_force_to_zmag(force_test,0);
    zoli_p->zmag_unzip = convert_force_to_zmag(force_unzip,0);
    zoli_p->zmag_lowf = convert_force_to_zmag(force_lowf,0);


}



else {

          snprintf(question, sizeof(question), "Zdet curve %%5d for oligo hybridization characterisation\n"
               "The purpose is to test the hybridization state of multiple beads\n"
               "This test involve 3 zmag(forces) states.\n"
               "In the so-called test phase force F_0 zmag = %%6f reached with a speed %%6f\n"
               "this phase will last for \\tau_0 %%6f seconds to record a reference.\n"
               "Then the force is increased to F_{unzip} zmag  %%6f with a\n"
               "speed %%6f. The Z bead position is averaged for \\tau_u %%6f seconds\n"
               "to measure the extended molecule. The force is then reduced back to to F_{test},\n"
               "with a speed %%6f, the hybridizations block the rezipping, this phase is define as\n"
               "long as the molecule extension stays above a threshold value (compared to the ref0)\n"
               "threshold %%6f microns. The maximum duration of the hybridization is define by \n"
               "\\tau_{test} %%8f this time should be long enough so that all oligo detach\n"
               "Specify a low force to remove oligos zmag %%6f going there with speed %%6f\n"
               "Specify the time spent at low force  \\tau_{low] = %%6f\n"
               "present Zmag value %g mm and angular position %g tr.\n"
               "Number of points in the displayed plot %%8d\n"
               "Number of points for averaging %%8d\n"
               "display zmag %%R or estimated force %%r\n"
               "display result plot %%b\n"
               "\\fbox{Track will be saved in %s\n"
               "Do you want to select a new file location %%R No %%r Yes\n"
               "Do you want to add focus change %%b\n"
               "Do you want to record bead profiles %%b\n"
               "Do you want real time saving %%b\n"
               "Do you want to add a ramp for F_test %%b\n"
               "Do you want to make low force exceptionnal ? %%b\n"
               , rot_start, zmag, fullfilename);

      i = win_scanf(question, &n_zdet, &zoli_p->zmag_test, &zoli_p->v_zmag_lowf_to_test,
                    &zoli_p->za_ref0.time_avg,&zoli_p->zmag_unzip, &zoli_p->v_zmag_test_to_unzip,
                    &zoli_p->za_unzip.time_avg, &zoli_p->v_zmag_unzip_to_test,
                    &zoli_p->z_dt.z_threshold, &zoli_p->z_dt.max_duration,
                    &zoli_p->zmag_lowf, &zoli_p->v_zmag_test_to_lowf, &zoli_p->za_lowf.time_avg,
                    &scope_buf_size, &fir,
                    &zmag_or_f, &keep_result, &file_selection, &focus_change,&with_radial_profiles, &streaming,&add_ramp,&exceptionnal_low_force);

  }

    if (add_ramp)
    {
      i = win_scanf("F_test will change of the following amount : %6f pN every %6d cycles\n",&zoli_p->ramp_test,&zoli_p->ramp_test_period);
    }
    if (i == WIN_CANCEL) return 0;

    if (focus_change)
    {
      i = win_scanf("Focus will change of the following amount : %6f um every %6d cycles\n",&zoli_p->ramp_obj,&zoli_p->obj_change_period);

    }
    if (exceptionnal_low_force)
    {
      i = win_scanf("Low force step will only happen very %d cycles\n",&zoli_p->low_force_period);

    }
    if (i == WIN_CANCEL) return 0;




    bead_nb = zoli_p->bead_nb;
    Open_Pico_config_file();
    set_config_int("Zdet_Curve", "n_zdet", n_zdet);

    set_config_float("Zdet_Oligo", "test_time_avg", zoli_p->za_ref0.time_avg);
    set_config_float("Zdet_Oligo", "unzip_time_avg", zoli_p->za_unzip.time_avg);
    set_config_float("Zdet_Oligo", "zmag_setling_time", zoli_p->zmag_setling_time);
    set_config_float("Zdet_Oligo", "zmag_test", zoli_p->zmag_test);
    set_config_float("Zdet_Oligo", "v_zmag_lowf_to_test", zoli_p->v_zmag_lowf_to_test);
    set_config_float("Zdet_Oligo", "z_threshold", zoli_p->z_dt.z_threshold);
    // the roration where we measure the molecule extension
    set_config_float("Zdet_Oligo", "zmag_unzip", zoli_p->zmag_unzip);
    set_config_float("Zdet_Oligo", "v_zmag_test_to_unzip", zoli_p->v_zmag_test_to_unzip);
    set_config_float("Zdet_Oligo", "v_zmag_unzip_to_test", zoli_p->v_zmag_unzip_to_test);
    set_config_float("Zdet_Oligo", "hyb_max_duration", zoli_p->z_dt.max_duration);


    set_config_float("Zdet_Oligo", "zmag_lowf", zoli_p->zmag_lowf);
    set_config_float("Zdet_Oligo", "v_zmag_test_to_lowf", zoli_p->v_zmag_test_to_lowf);
    set_config_float("Zdet_Oligo", "lowf_time_avg", zoli_p->za_lowf.time_avg);
    set_config_float("Zdet_Oligo", "ramp_test", zoli_p->ramp_test);
    set_config_int("Zdet_Oligo", "ramp_test_period", zoli_p->ramp_test_period);
    set_config_float("Zdet_Oligo", "ramp_test", zoli_p->ramp_obj);
    set_config_int("Zdet_Oligo", "ramp_test_period", zoli_p->obj_change_period);
    write_Pico_config_file();
    Close_Pico_config_file();
    zoli_p->z_dt.direction = (zoli_p->z_dt.z_threshold < 0) ? 1 : -1;
    zoli_p->setling_nb_frames_in_zmag = (int)(0.5 + (zoli_p->zmag_setling_time
                                        * Pico_param.camera_param.camera_frequency_in_Hz));
    zoli_p->za_ref0.nb_frames_in_avg = (int)(0.5 + (zoli_p->za_ref0.time_avg
                                       * Pico_param.camera_param.camera_frequency_in_Hz));
    zoli_p->za_unzip.nb_frames_in_avg = (int)(0.5 + (zoli_p->za_unzip.time_avg
                                        * Pico_param.camera_param.camera_frequency_in_Hz));
    zoli_p->za_lowf.nb_frames_in_avg = (int)(0.5 + (zoli_p->za_lowf.time_avg
                                       * Pico_param.camera_param.camera_frequency_in_Hz));
    zoli_p->za_after.nb_frames_in_avg = zoli_p->za_ref0.nb_frames_in_avg;
    zoli_p->za_after.time_avg = zoli_p->za_ref0.time_avg;
    zoli_p->z_dt.nb_frames_max = (int)(0.5 + (zoli_p->z_dt.max_duration
                                       * Pico_param.camera_param.camera_frequency_in_Hz));

    zoli_p->n_test = 1;
    zoli_p->aza_ref0 = (zdet_avg **)calloc(track_info->n_b, sizeof(zdet_avg *));
    zoli_p->aza_unzip = (zdet_avg **)calloc(track_info->n_b, sizeof(zdet_avg *));
    zoli_p->aza_after = (zdet_avg **)calloc(track_info->n_b, sizeof(zdet_avg *));
    zoli_p->aza_lowf = (zdet_avg **)calloc(track_info->n_b, sizeof(zdet_avg *));
    zoli_p->az_dt = (zdet_stay_in_range **)calloc(track_info->n_b, sizeof(zdet_stay_in_range *));
    zoli_p->aresu = (O_p **)calloc(track_info->n_b, sizeof(O_p *));
    zoli_p->aresu_dt = (O_p **)calloc(track_info->n_b, sizeof(O_p *));



    if (zoli_p->aza_ref0 == NULL || zoli_p->aza_unzip == NULL || zoli_p->aza_after == NULL ||
            zoli_p->az_dt == NULL || zoli_p->aresu == NULL || zoli_p->aresu_dt == NULL)
        return win_printf_OK("Memory allocation pb!");

    for (i = 0; i < track_info->n_b; i++)
    {
        zoli_p->aza_ref0[i] = (zdet_avg *)calloc(1, sizeof(zdet_avg));
        zoli_p->aza_unzip[i] = (zdet_avg *)calloc(1, sizeof(zdet_avg));
        zoli_p->aza_after[i] = (zdet_avg *)calloc(1, sizeof(zdet_avg));
        zoli_p->aza_lowf[i] = (zdet_avg *)calloc(1, sizeof(zdet_avg));
        zoli_p->az_dt[i] = (zdet_stay_in_range *)calloc(1, sizeof(zdet_stay_in_range));

        if (zoli_p->aza_ref0[i] == NULL || zoli_p->aza_unzip[i] == NULL || zoli_p->aza_lowf[i] == NULL
                || zoli_p->aza_after[i] == NULL || zoli_p->az_dt[i] == NULL)
            return win_printf_OK("Memory allocation pb!");

        zoli_p->aza_ref0[i]->nb_frames_in_avg = zoli_p->za_ref0.nb_frames_in_avg;
        zoli_p->aza_unzip[i]->nb_frames_in_avg = zoli_p->za_unzip.nb_frames_in_avg;
        zoli_p->aza_after[i]->nb_frames_in_avg = zoli_p->za_ref0.nb_frames_in_avg;
        zoli_p->aza_lowf[i]->nb_frames_in_avg = zoli_p->za_lowf.nb_frames_in_avg;
        zoli_p->aza_unzip[i]->time_avg = zoli_p->za_unzip.time_avg;
        zoli_p->aza_after[i]->time_avg = zoli_p->za_ref0.time_avg;
        zoli_p->aza_lowf[i]->time_avg = zoli_p->za_lowf.time_avg;
        zoli_p->aza_ref0[i]->i_bead = i;
        zoli_p->aza_unzip[i]->i_bead = i;
        zoli_p->aza_after[i]->i_bead = i;
        zoli_p->aza_lowf[i]->i_bead = i;
        zoli_p->az_dt[i]->n_bead = i;
        zoli_p->az_dt[i]->direction = zoli_p->z_dt.direction;
        zoli_p->az_dt[i]->z_threshold = zoli_p->z_dt.z_threshold;
        zoli_p->az_dt[i]->nb_frames_max = zoli_p->z_dt.nb_frames_max;
        zoli_p->az_dt[i]->max_duration = zoli_p->z_dt.max_duration;
    }
    zoli_p->c_bead = 0;
    zoli_p->n_bead = zoli_p->m_bead = track_info->n_b;
    nf = (scope_buf_size > 8192) ? 2 * scope_buf_size : 8192;
    snprintf(name, 64, "Zdet oligo curve %d", n_zdet);

    // We create a plot region to display bead position, zdet  etc.
    pr = create_hidden_pltreg_with_op(&op, nf, nf, 0, name);
    if (pr == NULL)  win_printf_OK("Could not find or allocte plot region!");
    //op = pr->one_p;
    ds = op->dat[0];
    zdet_r = create_gen_record(track_info, 1, with_radial_profiles, 0, 1, 0, 1.0, 1.0, 1);
    duplicate_Pico_parameter(&Pico_param, &(zdet_r->Pico_param_record));

    if (attach_g_record_to_pltreg(pr, zdet_r))
        win_printf("Could not attach grecord to plot region!");

    snprintf(zdet_r->filename, 512, "%s_Zdet_%d.trk", Pico_param.expt_param.experiment_filename, n_zdet);
    snprintf(zdet_r->name, 512, "Zdet curve %d - %s", n_zdet, Pico_param.expt_param.experiment_filename);
    zdet_r->n_rec = n_zdet;
    zdet_r->data_type |= ZDET_RECORD;
    zdet_r->record_action = oligo_action_n_bead;
    oligo_action_n_bead(0,0,0,1);
    zoli_p->z_cor = zdet_r->z_cor;

    if (streaming)
    {
        if (current_write_open_path_picofile != NULL && file_selection == 0)
        {
            strncpy(zdet_r->path, current_write_open_path_picofile, 512);
            write_record_file_header(zdet_r, oi_TRACK);
            zdet_r->real_time_saving = 1;
        }
        else if (do_save_track(zdet_r))
            win_printf("Pb writing tracking header");
    }

    if (current_write_open_path_picofile != NULL)
    {
        char *expt_str = print_html_pico_expt_params(&Pico_param);
        dump_to_html_log_file_with_date_and_time(current_write_open_picofile,
                "<h2>Z(t) Oligo recording n beads</h2>"
                "<br />in file %s in directory %s"
                "<br />zmag start %g, rot start %g <br />\n,%s"
                , backslash_to_slash(zdet_r->filename), backslash_to_slash(zdet_r->path)
                , zmag_start, rot_start, expt_str);
        free(expt_str);
    }

    li = grab_basename_and_index(zdet_r->filename, strlen(zdet_r->filename) + 1, basename, 256);
    n_zdet = (li >= 0) ? li : n_zdet;

    Open_Pico_config_file();
    set_config_int("Zdet_Curve", "n_zdet", n_zdet);
    write_Pico_config_file();
    Close_Pico_config_file();

    snprintf(fullfilename, sizeof(fullfilename), "%s/%s", zdet_r->path, zdet_r->filename);
    snprintf(fullconfigfile, sizeof(fullfilename), "%s/%s", config_dir, pico_config_filename);
    replace_extension(fullfilename, fullfilename, "cfg", sizeof(fullfilename));

    if (my_cp(fullfilename, fullconfigfile) != 0)
    {
        warning_message("unable to write config file %s along track at: ", fullfilename);
    }

    im = track_info->imi[track_info->c_i];

    im += 64;

    //we create one plot for each bead
    if (track_info->SDI_mode == 1)
    {
      for (i = li = 0; i < track_info->n_b; i++)
      {
        bt = track_info->bd[i];
        if (li)
        {
          op = create_and_attach_one_plot(pr, nf, nf, 0);
          ds = op->dat[0];
        }
        li++;
        bt->opt = op;
        ds->nx = ds->ny = 0;
        set_dot_line(ds);
        set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
        "Z coordinate l = %d, w = %d, nim = %d\n"
        "objective %f, zoom factor %f, sample %s Rotation %g\n"
        "Z magnets %g mm\n", Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, i, track_info->cl, track_info->cw, nf
        , Pico_param.obj_param.objective_magnification, Pico_param.micro_param.zoom_factor, pico_sample, rot_start, zmag);
        if ((ds = create_and_attach_one_ds(op, nf / fir, nf / fir, 0)) == NULL) return win_printf_OK("I can't create plot !");
        ds->nx = ds->ny = 0;
        set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
        "filtred Z over %d coordinate l = %d, w = %d, nim = %d\n"
        "objective %f, zoom factor %f, sample %s Rotation %g\n"
        "Z magnets %g mm\n", Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, i, fir, track_info->cl, track_info->cw, nf
        , Pico_param.obj_param.objective_magnification, Pico_param.micro_param.zoom_factor, pico_sample, rot_start, zmag);
        if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)  return win_printf_OK("I can't create plot !");
        ds->nx = ds->ny = 0;
        set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
        "Zmag l = %d, w = %d, nim = %d\n"
        "objective %f, zoom factor %f, sample %s Rotation %g\n"
        "Z magnets %g mm\n", Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, i , track_info->cl, track_info->cw, nf
        , Pico_param.obj_param.objective_magnification, Pico_param.micro_param.zoom_factor, pico_sample, rot_start, zmag);
        if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL) return win_printf_OK("I can't create plot !");
        alloc_data_set_x_error(ds);
        ds->nx = ds->ny = 0;
        set_ds_dot_line(ds);
        set_ds_point_symbol(ds, "\\oc");
        set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
        "averaged Z over %d coordinate l = %d, w = %d, nim = %d\n"
        "objective %f, zoom factor %f, sample %s Rotation %g\n"
        "Z magnets %g mm\n", Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, i, fir, track_info->cl, track_info->cw, nf, Pico_param.obj_param.objective_magnification
        , Pico_param.micro_param.zoom_factor, pico_sample, rot_start, zmag);
        create_attach_select_y_un_to_op(op, IS_METER, 0 , (float)1, -3, 0, "mm");
        create_attach_select_y_un_to_op(op, IS_NEWTON, 0 , (float)1, -12, 0, "pN");
        create_attach_select_y_un_to_op(op, 0, 0 , (float)1, 0, 0, "no_name");
        if (zmag_or_f == 0)
        {
          set_plot_y_prime_title(op, "Zmag");
          op->c_yu_p = 1;
        }
        else if (zmag_or_f == 0)
        {
          set_plot_y_prime_title(op, "Estimated force");
          op->c_yu_p = 2;
        }
        else
        {
          set_plot_y_prime_title(op, "Rotation");
          op->c_yu_p = 3;
        }

        create_attach_select_y_un_to_op(op, IS_METER, 0 , (float)1, -6, 0, "\\mu m");
        create_attach_select_x_un_to_op(op, IS_SECOND, 0 , (float)1 / Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");
        op->op_idle_action = zdet_op_idle_action;
        op->op_post_display = zdet_op_post_display;
        set_op_filename(op, "Z(t)fil-bd%dzdet%d.gr", i, n_zdet);
        set_plot_title(op, "Bead %d Z(t) %d zmag %g", i, n_zdet, zmag);
        set_plot_x_title(op, "Time");
        set_plot_y_title(op, "Molecule extension");
        op->x_lo = (-nf / 64); ///Pico_param.camera_param.camera_frequency_in_Hz;
        op->x_hi = (nf + nf / 64); ///Pico_param.camera_param.camera_frequency_in_Hz;
        set_plot_x_fixed_range(op);
        fill_next_available_job_spot(im, im + 32 + i, COLLECT_DATA, i, imr, oi, pr, op, NULL, zdet_job_z_fil_avg, 0);
        op->user_id = 4096;
        op->user_ispare[ISPARE_BEAD_NB] = i;
        if (keep_result)
        {
          op = create_and_attach_one_plot(pr, nf, nf, 0);
          ds = op->dat[0];
          ds->nx = ds->ny = 0;
          set_dot_line(ds);
          set_ds_point_symbol(ds, "\\oc");
          set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
          "averaged Z coordinate ref0 point mean value at zmag %g\n "
          "l = %d, w = %d, nim = %d\n"
          "objective %f, zoom factor %f, sample %s Rotation %g\n"
          "Z magnets %g mm\n", Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, i, zoli_p->zmag_test
          , track_info->cl, track_info->cw, nf, Pico_param.obj_param.objective_magnification
          , Pico_param.micro_param.zoom_factor, pico_sample, rot_start, zmag);
          if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)  return win_printf_OK("I can't create plot !");
          ds->nx = ds->ny = 0;
          set_dot_line(ds);
          set_ds_point_symbol(ds, "\\fs");
          set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
          "averaged Z coordinate unzip point mean value a zmag %g\n "
          "l = %d, w = %d, nim = %d\n"
          "objective %f, zoom factor %f, sample %s Rotation %g\n"
          "Z magnets %g mm\n", Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, i, zoli_p->zmag_unzip
          , track_info->cl, track_info->cw, nf, Pico_param.obj_param.objective_magnification, Pico_param.micro_param.zoom_factor
          , pico_sample, rot_start, zmag);
          if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)  return win_printf_OK("I can't create plot !");
          ds->nx = ds->ny = 0;
          set_dot_line(ds);
          set_ds_point_symbol(ds, "\\cr");
          set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
          "averaged Z coordinate after point mean value at zmag %g\n "
          "l = %d, w = %d, nim = %d\n"
          "objective %f, zoom factor %f, sample %s Rotation %g\n"
          "Z magnets %g mm\n", Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, i, zoli_p->zmag_test
          , track_info->cl, track_info->cw, nf, Pico_param.obj_param.objective_magnification, Pico_param.micro_param.zoom_factor
          , pico_sample, rot_start, zmag);
          if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL) return win_printf_OK("I can't create plot !");

          ds->nx = ds->ny = 0;
          set_dot_line(ds);
          set_ds_point_symbol(ds, "\\cr");
          set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
          "averaged Z coordinate after point mean value at zmag %g\n "
          "l = %d, w = %d, nim = %d\n"
          "objective %f, zoom factor %f, sample %s Rotation %g\n"
          "Z magnets %g mm\n", Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, i, zoli_p->zmag_test
          , track_info->cl, track_info->cw, nf, Pico_param.obj_param.objective_magnification, Pico_param.micro_param.zoom_factor
          , pico_sample, rot_start, zmag);
          op->op_idle_action = zdet_op_idle_action;
          op->op_post_display = zdet_op_post_display;
          set_op_filename(op, "Z(t)Avg-bd%dzdet%d.gr", i, n_zdet);
          set_plot_title(op, "Bead %d Averaged Z(t) %d ", i, n_zdet);
          set_plot_x_title(op, "Test number");
          set_plot_y_title(op, "Molecule extension change");
          create_attach_select_y_un_to_op(op, IS_METER, 0 , (float)1, -6, 0, "\\mu m");
          op->user_ispare[ISPARE_BEAD_NB] = i;
          zoli_p->aresu[i] = op;
          op = create_and_attach_one_plot(pr, nf, nf, 0);
          ds = op->dat[0];
          ds->nx = ds->ny = 0;
          set_dot_line(ds);
          set_ds_point_symbol(ds, "\\fd");
          set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
            "mean time of hybridization between zmag %g and zmag %g\n "
            "l = %d, w = %d, nim = %d\n"
            "objective %f, zoom factor %f, sample %s Rotation %g\n"
            "Z magnets %g mm\n", Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, i, zoli_p->zmag_unzip, zoli_p->zmag_test
            , track_info->cl, track_info->cw, nf, Pico_param.obj_param.objective_magnification, Pico_param.micro_param.zoom_factor
            , pico_sample, rot_start, zmag);
          op->op_idle_action = zdet_op_idle_action;
          op->op_post_display = zdet_op_post_display;
          set_op_filename(op, "Z(t)dt-bd%dzdet%d.gr", i, n_zdet);
          set_plot_title(op, "Bead %d Hybridization time %d ", i, n_zdet);
          set_plot_x_title(op, "Test number");
          set_plot_y_title(op, "Time");
          create_attach_select_y_un_to_op(op, IS_SECOND, 0, (float)1 / Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");
          op->user_ispare[ISPARE_BEAD_NB] = i;
          zoli_p->aresu_dt[i] = op;
        }
      }

    }
    else
    {
      for (i = li = 0; i < track_info->n_b; i++)
      {
        bt = track_info->bd[i];
        if (bt->calib_im == NULL)// || (bt->not_lost <= 0) || (bt->in_image == 0))
        continue;
        if (li)
        {
          op = create_and_attach_one_plot(pr, nf, nf, 0);
          ds = op->dat[0];
        }
        li++;
        bt->opt = op;
        ds->nx = ds->ny = 0;
        set_dot_line(ds);
        set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
        "Z coordinate l = %d, w = %d, nim = %d\n"
        "objective %f, zoom factor %f, sample %s Rotation %g\n"
        "Z magnets %g mm\n"
        "Calibration from %s" , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, i, track_info->cl, track_info->cw, nf
        , Pico_param.obj_param.objective_magnification, Pico_param.micro_param.zoom_factor, pico_sample, rot_start, zmag, bt->calib_im->filename);
        if ((ds = create_and_attach_one_ds(op, nf / fir, nf / fir, 0)) == NULL) return win_printf_OK("I can't create plot !");
        ds->nx = ds->ny = 0;
        set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
        "filtred Z over %d coordinate l = %d, w = %d, nim = %d\n"
        "objective %f, zoom factor %f, sample %s Rotation %g\n"
        "Z magnets %g mm\n"
        "Calibration from %s", Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, i, fir, track_info->cl, track_info->cw, nf
        , Pico_param.obj_param.objective_magnification, Pico_param.micro_param.zoom_factor, pico_sample, rot_start, zmag, bt->calib_im->filename);
        if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)  return win_printf_OK("I can't create plot !");
        ds->nx = ds->ny = 0;
        set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
        "Zmag l = %d, w = %d, nim = %d\n"
        "objective %f, zoom factor %f, sample %s Rotation %g\n"
        "Z magnets %g mm\n"
        "Calibration from %s", Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, i , track_info->cl, track_info->cw, nf
        , Pico_param.obj_param.objective_magnification, Pico_param.micro_param.zoom_factor, pico_sample, rot_start, zmag, bt->calib_im->filename);
        if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL) return win_printf_OK("I can't create plot !");
        alloc_data_set_x_error(ds);
        ds->nx = ds->ny = 0;
        set_ds_dot_line(ds);
        set_ds_point_symbol(ds, "\\oc");
        set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
        "averaged Z over %d coordinate l = %d, w = %d, nim = %d\n"
        "objective %f, zoom factor %f, sample %s Rotation %g\n"
        "Z magnets %g mm\n"
        "Calibration from %s", Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, i, fir, track_info->cl, track_info->cw, nf, Pico_param.obj_param.objective_magnification
        , Pico_param.micro_param.zoom_factor, pico_sample, rot_start, zmag, bt->calib_im->filename);
        create_attach_select_y_un_to_op(op, IS_METER, 0 , (float)1, -3, 0, "mm");
        create_attach_select_y_un_to_op(op, IS_NEWTON, 0 , (float)1, -12, 0, "pN");
        create_attach_select_y_un_to_op(op, 0, 0 , (float)1, 0, 0, "no_name");
        if (zmag_or_f == 0)
        {
          set_plot_y_prime_title(op, "Zmag");
          op->c_yu_p = 1;
        }
        else if (zmag_or_f == 0)
        {
          set_plot_y_prime_title(op, "Estimated force");
          op->c_yu_p = 2;
        }
        else
        {
          set_plot_y_prime_title(op, "Rotation");
          op->c_yu_p = 3;
        }

        create_attach_select_y_un_to_op(op, IS_METER, 0 , (float)1, -6, 0, "\\mu m");
        create_attach_select_x_un_to_op(op, IS_SECOND, 0 , (float)1 / Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");
        op->op_idle_action = zdet_op_idle_action;
        op->op_post_display = zdet_op_post_display;
        set_op_filename(op, "Z(t)fil-bd%dzdet%d.gr", i, n_zdet);
        set_plot_title(op, "Bead %d Z(t) %d zmag %g", i, n_zdet, zmag);
        set_plot_x_title(op, "Time");
        set_plot_y_title(op, "Molecule extension");
        op->x_lo = (-nf / 64); ///Pico_param.camera_param.camera_frequency_in_Hz;
        op->x_hi = (nf + nf / 64); ///Pico_param.camera_param.camera_frequency_in_Hz;
        set_plot_x_fixed_range(op);
        fill_next_available_job_spot(im, im + 32 + i, COLLECT_DATA, i, imr, oi, pr, op, NULL, zdet_job_z_fil_avg, 0);
        op->user_id = 4096;
        op->user_ispare[ISPARE_BEAD_NB] = i;
        if (keep_result)
        {
          op = create_and_attach_one_plot(pr, nf, nf, 0);
          ds = op->dat[0];
          ds->nx = ds->ny = 0;
          set_dot_line(ds);
          set_ds_point_symbol(ds, "\\oc");
          set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
          "averaged Z coordinate ref0 point mean value at zmag %g\n "
          "l = %d, w = %d, nim = %d\n"
          "objective %f, zoom factor %f, sample %s Rotation %g\n"
          "Z magnets %g mm\n"
          "Calibration from %s", Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, i, zoli_p->zmag_test
          , track_info->cl, track_info->cw, nf, Pico_param.obj_param.objective_magnification
          , Pico_param.micro_param.zoom_factor, pico_sample, rot_start, zmag, bt->calib_im->filename);
          if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)  return win_printf_OK("I can't create plot !");
          ds->nx = ds->ny = 0;
          set_dot_line(ds);
          set_ds_point_symbol(ds, "\\fs");
          set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
          "averaged Z coordinate unzip point mean value a zmag %g\n "
          "l = %d, w = %d, nim = %d\n"
          "objective %f, zoom factor %f, sample %s Rotation %g\n"
          "Z magnets %g mm\n"
          "Calibration from %s", Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, i, zoli_p->zmag_unzip
          , track_info->cl, track_info->cw, nf, Pico_param.obj_param.objective_magnification, Pico_param.micro_param.zoom_factor
          , pico_sample, rot_start, zmag, bt->calib_im->filename);
          if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)  return win_printf_OK("I can't create plot !");
          ds->nx = ds->ny = 0;
          set_dot_line(ds);
          set_ds_point_symbol(ds, "\\cr");
          set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
          "averaged Z coordinate after point mean value at zmag %g\n "
          "l = %d, w = %d, nim = %d\n"
          "objective %f, zoom factor %f, sample %s Rotation %g\n"
          "Z magnets %g mm\n"
          "Calibration from %s", Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, i, zoli_p->zmag_test
          , track_info->cl, track_info->cw, nf, Pico_param.obj_param.objective_magnification, Pico_param.micro_param.zoom_factor
          , pico_sample, rot_start, zmag, bt->calib_im->filename);
          if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL) return win_printf_OK("I can't create plot !");

          ds->nx = ds->ny = 0;
          set_dot_line(ds);
          set_ds_point_symbol(ds, "\\cr");
          set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
          "averaged Z coordinate after point mean value at zmag %g\n "
          "l = %d, w = %d, nim = %d\n"
          "objective %f, zoom factor %f, sample %s Rotation %g\n"
          "Z magnets %g mm\n"
          "Calibration from %s", Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, i, zoli_p->zmag_test
          , track_info->cl, track_info->cw, nf, Pico_param.obj_param.objective_magnification, Pico_param.micro_param.zoom_factor
          , pico_sample, rot_start, zmag, bt->calib_im->filename);
          op->op_idle_action = zdet_op_idle_action;
          op->op_post_display = zdet_op_post_display;
          set_op_filename(op, "Z(t)Avg-bd%dzdet%d.gr", i, n_zdet);
          set_plot_title(op, "Bead %d Averaged Z(t) %d ", i, n_zdet);
          set_plot_x_title(op, "Test number");
          set_plot_y_title(op, "Molecule extension change");
          create_attach_select_y_un_to_op(op, IS_METER, 0 , (float)1, -6, 0, "\\mu m");
          op->user_ispare[ISPARE_BEAD_NB] = i;
          zoli_p->aresu[i] = op;
          op = create_and_attach_one_plot(pr, nf, nf, 0);
          ds = op->dat[0];
          ds->nx = ds->ny = 0;
          set_dot_line(ds);
          set_ds_point_symbol(ds, "\\fd");
          set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for bead %d \n "
            "mean time of hybridization between zmag %g and zmag %g\n "
            "l = %d, w = %d, nim = %d\n"
            "objective %f, zoom factor %f, sample %s Rotation %g\n"
            "Z magnets %g mm\n"
            "Calibration from %s", Pico_param.camera_param.camera_frequency_in_Hz, n_zdet, i, zoli_p->zmag_unzip, zoli_p->zmag_test
            , track_info->cl, track_info->cw, nf, Pico_param.obj_param.objective_magnification, Pico_param.micro_param.zoom_factor
            , pico_sample, rot_start, zmag, bt->calib_im->filename);


          op->op_idle_action = zdet_op_idle_action;
          op->op_post_display = zdet_op_post_display;
          set_op_filename(op, "Z(t)dt-bd%dzdet%d.gr", i, n_zdet);
          set_plot_title(op, "Bead %d Hybridization time %d ", i, n_zdet);
          set_plot_x_title(op, "Test number");
          set_plot_y_title(op, "Time");
          create_attach_select_y_un_to_op(op, IS_SECOND, 0, (float)1 / Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");
          op->user_ispare[ISPARE_BEAD_NB] = i;
          zoli_p->aresu_dt[i] = op;
        }
      }

    }

    op = create_and_attach_one_plot(pr, nf, nf, 0);
    ds = op->dat[0];
    ds->nx = ds->ny = 0;

    set_dot_line(ds);
    set_ds_point_symbol(ds, "\\fd");
    if (track_info->SDI_mode == 0)
    {
      set_ds_source(ds, "Bead tracking at %g Hz Acquistion %d for all beads \n "
      "mean time of hybridization between zmag %g and zmag %g\n "
      "l = %d, w = %d, nim = %d\n"
      "objective %f, zoom factor %f, sample %s Rotation %g\n"
      "Z magnets %g mm\n"
		    //"Calibration from %s"
      , Pico_param.camera_param.camera_frequency_in_Hz, n_zdet
      , zoli_p->zmag_unzip, zoli_p->zmag_test
      , track_info->cl, track_info->cw, nf
      , Pico_param.obj_param.objective_magnification
      , Pico_param.micro_param.zoom_factor
      , pico_sample
      , rot_start
		    , zmag);

    }

    op->op_idle_action = zdet_op_idle_action;
    op->op_post_display = zdet_op_post_display;
    set_op_filename(op, "Z(t)dt-bdXXzdet%d.gr", n_zdet);
    set_plot_title(op, "All Beads Hybridization time %d ", n_zdet);
    set_plot_x_title(op, "Test number");
    set_plot_y_title(op, "Time");

    create_attach_select_y_un_to_op(op, IS_SECOND, 0
                                    , (float)1 / Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");

    zoli_p->resu_dt = op;



    im = track_info->imi[track_info->c_i];
    working_gen_record = zdet_r;
    zdet_r->last_starting_pos = im;
    win_title_used = 1;
    ask_to_update_menu();
    k = fill_next_available_action(im + 1, BEAD_CROSS_SAVED, 0);
    if (k < 0)   my_set_window_title("Could not add pending action!");

    if (imr_and_pr) switch_project_to_this_pltreg(pr);

    heap_check();
    generate_imr_TRACK_title = generate_zdet_topo_title;
    return D_O_K;
}


# endif



MENU *zdet_topo_menu(void)
{
    static MENU *zdt = NULL;

    if (zdt != NULL)    return zdt;

    zdt = zdet_menu();
#ifndef PICOTWIST
    add_item_to_menu(zdt, "record Z(t) topo", record_zdet_topo, NULL, 0, NULL);
    add_item_to_menu(zdt, "record Z(t) topo feedback", record_zdet_topo_feedback, NULL, 0, NULL);
#endif
    add_item_to_menu(zdt, "Single test", zdet_topo_test, NULL, 0, NULL);
    add_item_to_menu(zdt, "single dummy test", zdet_topo_dummy_test, NULL, 0, NULL);
    add_item_to_menu(zdt, "Run test", zdet_topo_run, NULL, 0, NULL);
    add_item_to_menu(zdt, "Finish test", zdet_topo_stop, NULL, 0, NULL);
    add_item_to_menu(zdt, "record Z(t) oligo", record_zdet_oligo, NULL, 0, NULL);
    add_item_to_menu(zdt, "record Z(t) 3 states cycles", record_zdet_oligo_n_bead, NULL, 0, NULL);

    add_item_to_menu(zdt, "stop recording", stop_zdet, NULL, 0, NULL);
    add_item_to_menu(zdt, "\0",  NULL,   NULL,   0, NULL);

    zdt = zdet_plot_menu();
    add_item_to_menu(zdt, "\0",  NULL,   NULL,   0, NULL);
    add_item_to_menu(zdt, "Single test", zdet_topo_test, NULL, 0, NULL);
    add_item_to_menu(zdt, "single dummy test", zdet_topo_dummy_test, NULL, 0, NULL);
    add_item_to_menu(zdt, "Run test", zdet_topo_run, NULL, 0, NULL);
    add_item_to_menu(zdt, "Finish test", zdet_topo_stop, NULL, 0, NULL);


    add_item_to_menu(zdt, "\0",  NULL,   NULL,   0, NULL);
    add_item_to_menu(zdt, "Display zmag", zdet_topo_param_select, NULL, MENU_INDEX(0), NULL);
    add_item_to_menu(zdt, "Display force", zdet_topo_param_select, NULL, MENU_INDEX(1), NULL);
    add_item_to_menu(zdt, "Display rot", zdet_topo_param_select, NULL, MENU_INDEX(2), NULL);
    add_item_to_menu(zdt, "\0",  NULL,   NULL,   0, NULL);

    add_item_to_menu(zdt, "Experiment",  NULL,   NULL,   0, NULL);
    add_item_to_menu(quicklink_menu, "record Z(t) 3 states cycles", record_zdet_oligo_n_bead, NULL, 0, NULL);
    add_item_to_menu(quicklink_menu, "Run test", zdet_topo_run, NULL, 0, NULL);
    add_item_to_menu(quicklink_menu, "Finish test", zdet_topo_stop, NULL, 0, NULL);
    add_item_to_menu(quicklink_menu, "Stop recording now", stop_zdet, NULL, 0, NULL);
    return zdt;
}

int zdet_topo_main(int argc, char **argv)
{
    (void)argc;
    (void)argv;
    zdet_topo_menu();
    return 0;
}



# endif
