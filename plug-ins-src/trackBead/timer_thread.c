#include "timer_thread.h" 
#include <stdio.h>
#include <stdlib.h>
#ifdef XV_WIN32
    #include <windows.h>

    void psleep(unsigned long int milliseconds)
    {
        Sleep(milliseconds);
    }
#else
    #include <unistd.h>

    void psleep(unsigned long int milliseconds)
    {
        usleep(milliseconds * 1000); // takes microseconds
    }
#endif

void *wait_thread_func(void *param)
{
    unsigned long int dueTime = (unsigned long int) param;
    psleep(dueTime);
    return NULL;
}

void *wait_and_call_thread_func(void *vargs)
{
    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);

    call_timer_t *args = (call_timer_t *) vargs;
    int res = 0;

    while (res == 0)
    {
        pthread_testcancel();

        psleep(args->time_interval);

        pthread_testcancel();

        res = args->callback();
    }
    return (void *) 0;
}

int create_timer(timer_thread_t *hTimer, unsigned long int dueTime)
{
    // Create a waitable timer.
    return pthread_create(hTimer, NULL, wait_thread_func, (void *) dueTime);
}

int create_call_timer (timer_thread_t *hTimer, unsigned long int time_interval, int (*callback)(void))
{
    call_timer_t *args = (call_timer_t *)malloc(sizeof(call_timer_t));
    args->time_interval = time_interval;
    args->callback = callback;

    return pthread_create(hTimer, NULL, wait_thread_func, (void *) args);
}

int wait_timer(timer_thread_t *hTimer)
{
    return pthread_join(*hTimer, NULL);
}

int kill_timer(timer_thread_t *hTimer)
{
    return pthread_cancel(*hTimer);
}
