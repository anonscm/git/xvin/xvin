/** \file MCL_FOCUS.c
    \brief Plug-in program for pifoc control in Xvin. 
    \author JF
*/
#ifndef _MCL_FOCUS_C_ 
#define _MCL_FOCUS_C_

#include "allegro.h"
#ifdef XV_WIN32
# include "winalleg.h"
# include <windowsx.h>
#endif
#include "ctype.h"

#ifdef XV_WIN32
#include "MCL_header.h"
#else
#include "Madlib.h"
#endif
#include "MCL_focus.h"


#define BUILDING_PLUGINS_DLL


int _has_focus_memory(void)
{
 return 0;
}
/*
int _set_light(float z)  
{ return D_O_K;}

float _get_light()  
{ return 0.5;}

int _set_temperature(float z)  
{ return D_O_K;}

float _get_temperature()  
{return 0.5;}
*/
int  _init_focus_OK(void)
{
  if(updating_menu_state != 0)	return D_O_K;

  MCL_ReleaseAllHandles();

 /* Prior to calling any other device function MCL_InitHandle() should be called */
  handle = MCL_InitHandle();
  if(handle == 0) {
    win_printf("Cannot get a handle to the device\n");
    win_printf("Enter any key to continue\n");
    return 1;
  }

  //Cal = MCL_GetCalibration(3, handle); /* the range of motion possible*/

  return 0;   
}



float _read_Z_value_accurate_in_micron(void)
{
  MCL_obj_position = (float) MCL_SingleReadZ(handle);
  return MCL_obj_position;
}

int _read_Z_value(void)
{
  return (int)(_read_Z_value_accurate_in_micron());//ask meaninig 10*	
}

float _read_Z_value_OK(int *error) /* in microns */
{
  return _read_Z_value_accurate_in_micron();
}
float _read_last_Z_value(void) /* in microns */
{
  return MCL_obj_position;//why not asking the driver?	
}



int _set_Z_step(float z)  /* in micron */
{
	MCL_Z_step = z;
	return 0;
}




int _set_Z_value(float z)  /* in microns */
{
  if ((z > 200) || (z < 0)) return D_O_K;
  if (MCL_SingleWriteZ((double) z , handle) == MCL_SUCCESS)
    MCL_obj_position = z;
  else win_printf("Error in setting z");
  //MCL_MonitorZ((double) z, handle);
  return D_O_K;
}


int _set_Z_value_MCL(void)
{
  float goal = 0;

  if(updating_menu_state != 0)	return D_O_K;
  win_scanf("Where do you want to go ?%f",goal);
  _set_Z_value(goal);
  return D_O_K;
}


int _set_Z_obj_accurate(float zstart)
{
  return _set_Z_value(zstart);
}
int _set_Z_value_OK(float z) 
{
  return _set_Z_value(z);
}

int _inc_Z_value(int step)  /* in micron */
{
  _set_Z_value(MCL_obj_position + MCL_Z_step*step);
  return 0;
}

int _small_inc_Z_value(int up)  
{
  int nstep;
  
  _set_Z_step(0.1);
  nstep = (up>=0)? 1:-1;
  _set_Z_value(MCL_obj_position + MCL_Z_step*nstep);
  
  return 0;
}

int _big_inc_Z_value(int up)  
{
  int nstep;
  
  _set_Z_step(0.1);
  nstep = (up>=0)? 10:-10;
  _set_Z_value(MCL_obj_position + MCL_Z_step*nstep);
  
  return 0;
}



MENU *MCL_focus_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"Read Objective position", _read_Z_value,NULL,0,NULL);
	add_item_to_menu(mn,"Set Objective position",_set_Z_value_MCL,NULL,0,NULL);

	return mn;
}


int MCL_focus_main (int argc, char **argv)
{
 
  //  struct ProductInformation pi;

  _init_focus_OK();
  add_plot_treat_menu_item ( "MCL focus", NULL, MCL_focus_plot_menu(), 0, NULL);
  add_image_treat_menu_item ( "MCL focus", NULL, MCL_focus_plot_menu(), 0, NULL);    
  return 0;
}



int	MCL_focus_unload(int argc, char **argv)
{
	remove_item_to_menu(plot_treat_menu, "MCL_focus", NULL, NULL);
	return D_O_K;
}

#endif
