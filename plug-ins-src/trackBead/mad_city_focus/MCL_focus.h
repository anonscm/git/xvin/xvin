#ifndef _MCL_FOCUS_H
#define MCL_FOCUS_H

#include "xvin.h"

# ifndef _MCL_FOCUS_C_

PXV_VAR(float ,  MCL_obj_position);
PXV_VAR(float , MCL_Z_step);
PXV_VAR(int , handle);
PXV_VAR(double, Cal);

# endif

# ifdef _MCL_FOCUS_C_
float   MCL_obj_position = 0;
float  MCL_Z_step = 0.1;
int handle = -1;
double Cal = 200;

# endif

PXV_FUNC(int, MCL_focus_main, (int argc, char **argv));





#endif
