
#ifndef _ZDET_TOPO_H_
#define _ZDET_TOPO_H_



# define ZDET_TOPO_RECORD  32769

# define ZMAG_SET_PHASE 1
# define ROT_SET_PHASE 2
# define ROT_FRAC_SET_PHASE 4
# define ROT_INC_SET_PHASE 8
# define FOCUS_SET_PHASE 16
# define Z_MEASURE_PHASE 32
# define WAIT_SINGLE_BEAD_Z_CHANGE 64
# define WAIT_MULTI_BEADs_Z_CHANGE 128


typedef struct _zdet_phase
{
  int type;                  // may be move Zmag, move rot, grab reference value, wait for Zbead change
  float zmag_aimed;
  float rot_aimed;
  float obj_aimed;
  float time_avg;            // in seconds
  int nb_frames_in_avg;      // in frames
  int zbead_valid;           // the flag indicating if the z bead position is valid
  int ending_im;
  int n_avg;
  int n_rec;
  float zobj;
  int i_bead;
  int n_bead;
  zdet_stay_in_range **az_dt;
}zdet_phase;


typedef struct _zdet_macro_param
{
  int state;                     // define what action is going on
  int n_test;
  int c_test;                    // the current testing number
  int bead_nb;                    // the bead of interest (only one at a time!)
  int test_or_dummy;             // flag indicating if
  int test_progress;             // indicates in which phase we are
  float rot_setling_time;       // in seconds
  int setling_nb_frames_in_rot; // in frames
  float zmag_setling_time;       // in seconds
  int setling_nb_frames_in_zmag; // in frames
  int n_phases;                  // the number of phases
  int c_phases;                  // the current phase
  zdet_phase **zph;
} zdet_macro_param;


typedef struct _zdet_topo_chirality_param
{
  int state;                     // define what action is going on
  int n_test;
  int c_test;                    // the current testing number
  int bead_nb;                    // the bead of interest (only one at a time!)
  int test_or_dummy;             // flag indicating if
  int test_progress;             // indicates in which phase we are
  float rot_setling_time;       // in seconds
  int setling_nb_frames_in_rot; // in frames
      // this is the test phase parameters
  zdet_avg za_test;
  zdet_avg za_ref;
  float rot_to_ref;             // the magnet fractional rotation where we want to test
  float zmag_ref;               // the magnet zmag value where we want to test
  float force_ref;              // the force value where we want to test

  float rot_to_test;             // the magnet fractional rotation where we want to test
  float zmag_test;               // the magnet zmag value where we want to test
  float force_test;              // the force value where we want to test

  O_p *resu;
  O_p *resu_valid;
  O_p *resu_hat;
  float dz_servo;
  float dz_one_turn;
  float dr_max;
  float rot_ref;
  int feedback;
  int one_way;
  float z_cor;
  float zmag_finish;
  int c_step;
  int fir;              // the nb of points for averaging
} zdet_topo_chirality_param;




typedef struct _zdet_topo_param
{
  int state;                     // define what action is going on
  int n_test;
  int c_test;                    // the current testing number
  int bead_nb;                    // the bead of interest (only one at a time!)
  int test_or_dummy;             // flag indicating if
  int test_progress;             // indicates in which phase we are
  float zmag_setling_time;       // in seconds
  int setling_nb_frames_in_zmag; // in frames
      // this is the test phase parameters
  zdet_avg za_test;
  zdet_avg za_ref0;
  zdet_avg za_ref;
  zdet_avg za_after;
  zdet_avg za_hat;
  float rot_to_test;             // the magnet rotation where we want to test the torsional state
  float zmag_test;               // the magnet zmag value where we want to test the torsional state
  float force_test;              // the force value where we want to test the torsional state

     // this is the rotation phase parameters
  float rot_hat;                 // the roration where we measure the molecule extension
  float rot_hat_max;             // the roration is modulate from rot_hat t rot_hat_max
  float zmag_rot;                // the magnet zmag value where we want to test the torsional state
  float force_rot;               // the force value where we want to test the torsional state

     // this is the rotation measuring phase parameters
  float zmag_hat_measure;        // the zmag choosen to measure rotation using hat curve
  float force_hat_measure;       // the force choosen to measure rotation using hat curve
  O_p *resu;
  O_p *resu_valid;
  O_p *resu_hat;
  float turns_to_increment;
  float turns_to_decrement_neg;
  float turns_to_decrement_pos;
  float dz_servo;
  float dz_one_turn;
  float dr_max;
  float rot_ref;
  int feedback;
  int one_way;
  float z_cor;
  float zmag_finish;
  int c_step;
  int fir;              // the nb of points for averaging
} zdet_topo_param;



typedef struct _zdet_oligo_param
{
  int state;                     // define what action is going on
  int n_test;
  int c_test;                    // the current testing number
  int bead_nb;                    // the bead of interest (only one at a time!)
  int test_or_dummy;             // flag indicating if
  int test_progress;             // indicates in which phase we are
  float zmag_setling_time;       // in seconds
  int setling_nb_frames_in_zmag; // in frames
      // this is the test phase parameters
  zdet_avg za_ref0;
  zdet_avg za_unzip;
  zdet_avg za_after;
  zdet_avg za_lowf;
  zdet_stay_in_range z_dt;
  float zmag_test;               // the magnet zmag value where we want to test the torsional state
  float force_test;              // the force value where we want to test the torsional state
  float v_zmag_lowf_to_test;     // the magnet zmag velocity
  float v_zmag_unzip_to_test;    // the magnet zmag velocity
     // this is the rotation phase parameters
  float zmag_unzip;              // the magnet zmag value where we want to test the torsional state
  float force_unzip;             // the force value where we want to test the torsional state
  float v_zmag_test_to_unzip;    // the magnet zmag velocity
  float zmag_lowf;               // the magnet zmag value where to sit to remove all oligos
  float force_lowf;              // the force value where to sit to remove all oligos
  float v_zmag_test_to_lowf;     // the magnet zmag velocity from test to lowf
  float zmag_speed_origin;      // specify the original speed to restore
  int ramp_test_period; // period to modify test force
  float ramp_test; // increment to modify test_force
  int obj_change_period; //
  float ramp_obj;
  int low_force_period;
  int obj_moved; //
  O_p *resu;
  O_p *resu_dt;
  float z_cor;
  float zmag_finish;
  int c_step;
  int fir;              // the nb of points for averaging
  zdet_avg **aza_ref0;   // array to save average of multiple beads
  zdet_avg **aza_unzip;
  zdet_avg **aza_after;
  zdet_avg **aza_lowf;
  zdet_stay_in_range **az_dt;
  int c_bead, n_bead, m_bead;           // the number of beads
  int n_left;
  O_p **aresu;
  O_p **aresu_dt;
} zdet_oligo_param;






# define ZDET_RUNNING  1
# define ZDET_STOPPED  2
# define ZDET_SUSPENDED  4
# define ZDET_SINGLE_TEST  8
# define ZDET_SINGLE_TEST_DUMMY  16



# ifndef _ZDET_TOPO_C_

//PXV_VAR(char, sample[]);
PXV_VAR(int, n_zdet_topo);
PXV_FUNC(MENU *, zdet_topo_menu, (void));

# else


# endif

# endif
