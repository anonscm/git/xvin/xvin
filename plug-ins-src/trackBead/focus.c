# ifndef _FOCUS_C_
# define _FOCUS_C_


# include <pthread.h>
# include <stdbool.h>
# include <unistd.h>
# include "xvin.h"

# include "ctype.h"
# include "allegro.h"
#ifdef XV_WIN32
    # include "winalleg.h"
#endif


# include "xv_plugins.h"	// for inclusions of libraries that control dynamic loading of libraries.


# define BUILDING_PLUGINS_DLL
# include "../cfg_file/Pico_cfg.h"


# undef _PXV_DLL
# define _PXV_DLL   __declspec(dllexport)
# include "../fft2d/fftbtl32n.h"
# include "fillibbt.h"
# include "focus.h"
# include "action.h"
# include "magnetscontrol.h"

# include "trackBead.h"
# include "track_util.h"



PXV_VAR(float, prev_focus);

float   dummy_obj_position = 0, dummy_Z_step = 0.1;

float temp_sample = 25,  led_light = 0.25;

unsigned long last_focus_set, focus_moving_time;
int focus_moving = 0;
float last_dummy_obj_position = 0;
float v_focus = 100;  // microns per sec
int device_has_memory = 1;

/*
extern FILE *fp_ldebug;
extern char *ldebug_file;
int im_ci(void);
*/
int   dummy_init_focus_OK(void);
int   dummy_read_Z_value(void);
int   dummy_set_Z_step(float z);  /* in micron */
float dummy_read_Z_value_accurate_in_micron(void);

float dummy_read_Z_value_OK(int *error); /* in microns */
float dummy_read_last_Z_value(void); /* in microns */
int   dummy_set_Z_value(float z);  /* in microns */
int   dummy_set_Z_obj_accurate(float zstart);
int   dummy_set_Z_value_OK(float z);
int   dummy_inc_Z_value(int step);  /* in micron */

int   dummy_small_inc_Z_value(int up);
int   dummy_big_inc_Z_value(int up);

int   dummy_set_light(float val);
float   dummy_get_light(void);

int   dummy_set_temperature(float val);
float   dummy_get_temperature(int);
int dummy_has_focus_memory(void);


# ifdef XVIN_STATICLINK

int  _init_focus_OK(void);
float _read_Z_value_accurate_in_micron(void);
int _read_Z_value(void);
int _set_Z_step(float z);  /* in micron */
float _read_Z_value_OK(int *error); /* in microns */
int _set_Z_value(float z);  /* in microns */
float _read_last_Z_value(void); /* in microns */
int	_set_Z_obj_accurate(float zstart);
int _set_Z_value_OK(float z);
int _inc_Z_value(int step);
int _small_inc_Z_value(int up);
int _big_inc_Z_value(int up);
int _set_light(float z) ;
float _get_light();
int _set_temperature(float z);
float _get_temperature(int t);
int _has_focus_memory(void);

PXV_FUNC(int, _request_read_temperature_value, (int image_n, int t, unsigned long *t0));
PXV_FUNC(float, _read_temperature_from_request_answer, (char *answer, int im_n, float val, int *error, int *channel));
PXV_FUNC(int, _request_read_focus_value, (int image_n, unsigned long *t0));
PXV_FUNC(float, _read_focus_raw_from_request_answer, (char *answer, int im_n, float val, int *error));
PXV_FUNC(int, _request_set_focus_value_raw, (int image_n, float z, unsigned long *t0));
PXV_FUNC(int, _check_set_focus_request_answer, (char *answer, int im_n, float z));

PXV_FUNC(int, _set_T0_pid, (int num, int val));
PXV_FUNC(int,  _get_T0_pid, (int num));


#endif

#ifdef RAWHID_V1
  int _rawhid_init(int verbose);
  int _launch_rawhid_thread(void);
  PXV_FUNC(int, _rawhid_init, (int verbose));
  PXV_FUNC(int, _launch_rawhid_thread, (void));
#endif

PXV_FUNC(int, grab_zmag_pid_param, (int param));
PXV_FUNC(int, chg_zmag_pid_param, (int param, int value));

int   dummy_set_light(float val)
{
  led_light = val;

# ifdef RT_MV_DEBUG
  RT_debug("light%d\n",(int)(40.96 * val));
#endif
  return 0;
}
float   dummy_get_light(void)
{
# ifdef RT_MV_DEBUG
  RT_debug("light?\n\tdac12=%d\n",(int)(40.96 * led_light));
#endif
  return led_light;
}

int   dummy_set_temperature(float val)
{
  temp_sample = val;

# ifdef RT_MV_DEBUG
  RT_debug("R0=%f\n",val);
#endif
  return 0;
}
float   dummy_get_temperature(int t)
{
# ifdef RT_MV_DEBUG
  RT_debug("T%d?\n\tT%d=%f\n",t,t,temp_sample);
#else
  (void)t;
#endif
  return temp_sample;
}





int    set_focus_device(Focus_param *f_p)
{
  int ret = 0, i;
# ifndef XVIN_STATICLINK
  int abort_loading = 0;
  char *argvp[2] = {0};
  void*	hinstLib;
  char error[2048] = {0};
# endif
  char pl_name[256] = {0};

  snprintf(pl_name,256,"%s_focus",f_p->Focus_driver);
  for (i=0; pl_name[i]; i++) pl_name[i] = tolower(pl_name[i]);

# ifndef DUMMY
#if defined(PI_E709) || defined(RAWHID_V1) //if pie709 you do not care about dummy
  //win_printf_OK("load rawhid focus");
  if (true)
#else
  if ( strcmp(pl_name,"dummy_focus") != 0)
#endif
    {
# ifdef XVIN_STATICLINK
      //win_printf("Before init focus OK");
      if (_init_focus_OK() == 0)
	{

	  init_focus_OK = _init_focus_OK;
	  read_Z_value_raw = _read_Z_value;
	  set_Z_step = _set_Z_step;
	  read_Z_value_accurate_in_micron_raw = _read_Z_value_accurate_in_micron;
	  read_Z_value_OK_raw = _read_Z_value_OK;
	  read_last_Z_value_raw = _read_last_Z_value;
	  set_Z_value_raw = _set_Z_value;
	  set_Z_obj_accurate_raw = _set_Z_obj_accurate;
	  set_Z_value_OK_raw = _set_Z_value_OK;
	  inc_Z_value_raw = _inc_Z_value;

	  small_inc_Z_value = _small_inc_Z_value;
	  big_inc_Z_value = _big_inc_Z_value;
	  has_focus_memory = _has_focus_memory;
	  set_light = _set_light;
	  get_light = _get_light;
	  set_temperature = _set_temperature;
	  get_temperature = _get_temperature;
#ifdef RAWHID_V1
    rawhid_init = _rawhid_init;
    launch_rawhid_thread = _launch_rawhid_thread;
//#else
//set_light = dummy_set_light;
//get_light = dummy_get_light;
//set_temperature = dummy_set_temperature;
//get_temperature = dummy_get_temperature;
#endif

# if !defined(MAD_CITY_LAB) && !defined(PIFOC_DAS1602) && !defined(PI_E761) && !defined(PI_E709) && !defined(FOCUS_6503) && !defined(FOCUS_6229) && !defined(RAWHID_V1)
	  request_read_temperature_value = _request_read_temperature_value;
	  read_temperature_from_request_answer = _read_temperature_from_request_answer;
	  request_read_focus_value = _request_read_focus_value;
	  read_focus_raw_from_request_answer = _read_focus_raw_from_request_answer;
	  request_set_focus_value_raw = _request_set_focus_value_raw;
	  check_set_focus_request_answer = _check_set_focus_request_answer;
	  get_T0_pid = _get_T0_pid;
	  set_T0_pid = _set_T0_pid;
# endif

#ifdef RAWHID_V1
request_read_temperature_value = _request_read_temperature_value;
read_temperature_from_request_answer = _read_temperature_from_request_answer;
get_T0_pid = _get_T0_pid;
set_T0_pid = _set_T0_pid;
#endif
	  return 0;
	}
    }
# else
  win_printf("loading %s for focus",pl_name);
      //argvp[0] = "dummy_focus";
      argvp[0] = pl_name;
      argvp[1] = NULL;
      load_plugin(1, argvp);
      hinstLib = grep_plug_ins_hinstLib(argvp[0]);

      abort_loading = 0;
      error[0] = 0;
      if (hinstLib != NULL)
	{
	  init_focus_OK = (int (*)(void))GetProcAddress(hinstLib,"_init_focus_OK");
	  if (init_focus_OK != NULL)
	    {
	      if (init_focus_OK())
		{
		  abort_loading++;
		  snprintf(error+strlen(error),2048,"Bad focus device initialization\nin%s",argvp[0]);
		}
	      else
		{
		  read_Z_value_raw = (int(*)(void))GetProcAddress(hinstLib, "_read_Z_value");
		  if (read_Z_value_raw == NULL)
		    {
		      abort_loading++;
		      snprintf(error+strlen(error),2048-+strlen(error),"Could not find: read_Z_value()\nin%s",argvp[0]);
		    }
		  read_Z_value_accurate_in_micron_raw = (float (*)(void))GetProcAddress(hinstLib,"_read_Z_value_accurate_in_micron");
		  if (read_Z_value_accurate_in_micron_raw == NULL)
		    {
		      abort_loading++;
		      snprintf(error+strlen(error),"Could not find: read_Z_value_accurate_in_micron()\nin%s",argvp[0]);
		    }
		  read_Z_value_OK_raw = (float (*)(int *error))GetProcAddress(hinstLib,"_read_Z_value_OK");
		  if (read_Z_value_OK_raw == NULL)
		    {
		      abort_loading++;
		      snprintf(error+strlen(error),2048-+strlen(error),"Could not find: read_Z_value_OK()\nin%s",argvp[0]);
		    }
		  read_last_Z_value_raw = (float(*)(void))GetProcAddress(hinstLib, "_read_last_Z_value");
		  if (read_last_Z_value_raw == NULL)
		    {
		      abort_loading++;
		      snprintf(error+strlen(error),2048-+strlen(error),"Could not find: read_last_Z_value()\nin%s",argvp[0]);
		    }
		  set_Z_value_raw = (int (*)(float z))GetProcAddress(hinstLib,"_set_Z_value");
		  if (set_Z_value_raw == NULL)
		    {
		      abort_loading++;
		      snprintf(error+strlen(error),2048-+strlen(error),"Could not find: set_Z_value()\nin%s",argvp[0]);
		    }
		  set_Z_obj_accurate_raw = (int	(*)(float zstart))GetProcAddress(hinstLib,"_set_Z_obj_accurate");
		  if (set_Z_obj_accurate_raw == NULL)
		    {
		      abort_loading++;
		      snprintf(error+strlen(error),2048-+strlen(error),"Could not find: set_Z_obj_accurate()\nin%s",argvp[0]);
		    }
		  set_Z_value_OK_raw = (int (*)(float z))GetProcAddress(hinstLib,"_set_Z_value_OK");
		  if (set_Z_value_OK_raw == NULL)
		    {
		      abort_loading++;
		      snprintf(error+strlen(error),2048-+strlen(error),"Could not find: set_Z_value_OK()\nin%s",argvp[0]);
		    }
		  set_Z_step = (int (*)(float z))GetProcAddress(hinstLib,"_set_Z_step");
		  if (set_Z_step == NULL)
		    {
		      abort_loading++;
		      snprintf(error+strlen(error),2048-+strlen(error),"Could not find: set_Z_step()\nin%s",argvp[0]);
		    }
		  inc_Z_value_raw = (int (*)(int step))GetProcAddress(hinstLib,"_inc_Z_value");
		  if (inc_Z_value_raw == NULL)
		    {
		      abort_loading++;
		      snprintf(error+strlen(error),2048-+strlen(error),"Could not find: inc_Z_value()\nin%s",argvp[0]);
		    }
		  small_inc_Z_value = (int (*)(int up))GetProcAddress(hinstLib,"_small_inc_Z_value");
		  if (small_inc_Z_value == NULL)
		    {
		      abort_loading++;
		      snprintf(error+strlen(error),2048-+strlen(error),"Could not find: small_inc_Z_value()\nin%s",argvp[0]);
		    }
		  big_inc_Z_value = (int (*)(int up))GetProcAddress(hinstLib,"_big_inc_Z_value");
		  if (big_inc_Z_value == NULL)
		    {
		      abort_loading++;
		      snprintf(error+strlen(error),2048-+strlen(error),"Could not find: big_inc_Z_value()\nin%s",argvp[0]);
		    }
		}
	    }
	  else
	    {
	      abort_loading++;
	      snprintf(error+strlen(error),2048-+strlen(error),"Could not find: _init_focus_OK\nin%s",argvp[0]);
	    }
	}
      else
	{
	  abort_loading++;
	  snprintf(error+strlen(error),2048-+strlen(error),"Could not find plugins %s",argvp[0]);
	}
      if (abort_loading == 0)
	{
	  has_focus_memory = (int (*)(void))GetProcAddress(hinstLib,"_has_focus_memory");
	  if (has_focus_memory == NULL)  has_focus_memory = dummy_has_focus_memory;
	  set_light = (int (*)(float up))GetProcAddress(hinstLib,"_set_light");
	  if (set_light == NULL)  set_light = dummy_set_light;
	  get_light = (float (*)(void))GetProcAddress(hinstLib,"_get_light");
	  if (get_light == NULL)  get_light = dummy_get_light;
	  set_temperature = (int (*)(float up))GetProcAddress(hinstLib,"_set_temperature");
	  if (set_temperature == NULL)  set_temperature = dummy_set_temperature;
	  get_temperature = (float (*)(int))GetProcAddress(hinstLib,"_get_temperature");
	  if (get_temperature == NULL)  get_temperature = dummy_get_temperature;

	  request_read_temperature_value = (int (*)(int image_n, int t, unsigned long *t0))
	    GetProcAddress(hinstLib,"_request_read_temperature_value");
	  read_temperature_from_request_answer = (float (*) (char *answer, int im_n, float val, int *error, int *channel))
	    GetProcAddress(hinstLib,"_read_temperature_from_request_answer");
	  request_read_focus_value = (int (*)(int image_n, unsigned long *t0))
	    GetProcAddress(hinstLib,"_request_read_focus_value");
	  read_focus_raw_from_request_answer = (float (*)(char *answer, int im_n, float val, int *error))
	    GetProcAddress(hinstLib,"_read_focus_raw_from_request_answer");
	  request_set_focus_value_raw = (int (*) (int image_n, float z, unsigned long *t0))
	    GetProcAddress(hinstLib,"_request_set_focus_value_raw");
	  check_set_focus_request_answer = (int (*) (char *answer, int im_n, float z))
	    GetProcAddress(hinstLib,"_check_set_focus_request_answer");

	  get_T0_pid = (int (*) (int num))GetProcAddress(hinstLib,"_get_T0_pid");
	  set_T0_pid = (int (*) (int num, int val))GetProcAddress(hinstLib,"_set_T0_pid");
	  return 0;
	}
      win_printf("Aborting specific focus device,\n"
		 "loading encountered %d errors:\n%s\n"
		 "Using the dummy magnets device instead\n",abort_loading, error);
    }
# endif
# endif // not DUMMY
#ifdef RAWHID_V1
  init_focus_OK = _init_focus_OK;
  read_Z_value_raw = _read_Z_value;
  set_Z_step = _set_Z_step;
  read_Z_value_accurate_in_micron_raw = _read_Z_value_accurate_in_micron;
  read_Z_value_OK_raw = _read_Z_value_OK;
  read_last_Z_value_raw = _read_last_Z_value;
  set_Z_value_raw = _set_Z_value;
  set_Z_obj_accurate_raw = _set_Z_obj_accurate;
  set_Z_value_OK_raw = _set_Z_value_OK;
  inc_Z_value_raw = _inc_Z_value;

  small_inc_Z_value = _small_inc_Z_value;
  big_inc_Z_value = _big_inc_Z_value;
  has_focus_memory = _has_focus_memory;
#else
  init_focus_OK = dummy_init_focus_OK;
  read_Z_value_raw = dummy_read_Z_value;
  set_Z_step = dummy_set_Z_step;
  read_Z_value_accurate_in_micron_raw = dummy_read_Z_value_accurate_in_micron;
  read_Z_value_OK_raw = dummy_read_Z_value_OK;
  read_last_Z_value_raw = dummy_read_last_Z_value;
  set_Z_value_raw = dummy_set_Z_value;
  set_Z_obj_accurate_raw = dummy_set_Z_obj_accurate;
  set_Z_value_OK_raw = dummy_set_Z_value_OK;
  inc_Z_value_raw = dummy_inc_Z_value;

  small_inc_Z_value = dummy_small_inc_Z_value;
  big_inc_Z_value = dummy_big_inc_Z_value;
  has_focus_memory = dummy_has_focus_memory;
#endif
  //debug tv : dummy mode for focus (discarded when pie709) but rawhid_v1 for temperature + ligth
  init_focus_OK();
  return ret;
}


int dummy_init_focus_OK(void)
{
  return 0;
}
int dummy_has_focus_memory(void)
{
 return 0;
}

int dummy_read_Z_value(void)
{
  return (int)(10*dummy_read_last_Z_value());
}
int dummy_set_Z_step(float z)  /* in micron */
{
  dummy_Z_step = z;
  return 0;
}
float dummy_read_Z_value_accurate_in_micron(void)
{
  unsigned long dt;
  float tmp;
my_set_window_title("dummy obj read");

  if (focus_moving_time == 0)
    {
# ifdef RT_MV_DEBUG
      RT_debug("dac16?\n\t dac16=%d\n",(int)((dummy_obj_position*65536)/250));
#endif
      return dummy_obj_position;	// magnets have stopped
    }
  dt = my_uclock();
  if (last_focus_set + focus_moving_time < dt) // in fact translating is finished
    {
      focus_moving_time = 0;
      tmp = dummy_obj_position;	// magnets have stopped
      last_dummy_obj_position = dummy_obj_position;
    }
  else
    {
      tmp = (dummy_obj_position - last_dummy_obj_position) * ((float)(dt - last_focus_set))/focus_moving_time;
      tmp += last_dummy_obj_position; // this is where we are
    }
# ifdef RT_MV_DEBUG
  RT_debug("dac16?\n\t dac16=%d\n",(int)((tmp*65536)/250));
#endif
  return tmp;
}

float dummy_read_Z_value_OK(int *error) /* in microns */
{
  (void)error;
  return dummy_read_Z_value_accurate_in_micron();

}

float dummy_read_last_Z_value(void) /* in microns */
{
  return dummy_obj_position;
}

int dummy_set_Z_value(float z)  /* in microns */
{
  unsigned long dt;
  float tmp;
//win_printf_OK("obj dummy set %f",z);
  if (focus_moving_time == 0)   // rotation has stopped
    {
      focus_moving_time = get_my_uclocks_per_sec()*fabs((z - dummy_obj_position)/v_focus);
      last_focus_set = my_uclock();
      last_dummy_obj_position = dummy_obj_position;
      dummy_obj_position = z;
    }
  else
    {
      dt = my_uclock();
      if (last_focus_set + focus_moving_time < dt) // in fact rotating is finished
	{
	  focus_moving_time = get_my_uclocks_per_sec()*fabs((z - dummy_obj_position)/v_focus);
	  last_focus_set = my_uclock();
	  last_dummy_obj_position = dummy_obj_position;
	  dummy_obj_position = z;
	}
      else  // we are still rotating !
	{
	  tmp = (dummy_obj_position - last_dummy_obj_position) * ((float)(dt - last_focus_set))/focus_moving_time;
	  tmp += last_dummy_obj_position; // this is where we are
	  focus_moving_time = get_my_uclocks_per_sec()*fabs((z - tmp)/v_focus);
	  last_focus_set = my_uclock();
	  last_dummy_obj_position = tmp;
	  dummy_obj_position = z;
	}
    }
# ifdef RT_MV_DEBUG
  RT_debug("dac16=%d&%d\n",(int)((65536*z)/250),(int)((65536*z)/250));
#endif
  return 0;
}

int	dummy_set_Z_obj_accurate(float zstart)
{
  dummy_set_Z_step(0.045);
  return dummy_set_Z_value(zstart);
}
int dummy_set_Z_value_OK(float z)
{
  return dummy_set_Z_value(z);
}

int dummy_inc_Z_value(int step)  /* in micron */
{
  return dummy_set_Z_value(dummy_obj_position + dummy_Z_step*step);
}

int dummy_small_inc_Z_value(int up)
{
  int nstep;

  dummy_set_Z_step(0.1);
  nstep = (up>=0)? 1:-1;
  dummy_set_Z_value(dummy_obj_position + dummy_Z_step*nstep);

  //display_title_message("Z obj = %g \\mu m Rot %g Z mag %g mm",
  //  (float)dummy_read_Z_value()/10,n_rota,n_magnet_z);
  return 0;
}

int dummy_big_inc_Z_value(int up)
{
  int nstep;

  dummy_set_Z_step(0.1);
  nstep = (up>=0)? 10:-10;
  dummy_set_Z_value(dummy_obj_position + dummy_Z_step*nstep);
  //	display_title_message("Z obj = %g \\mu m Rot %g Z mag %g mm",
  //  (float)dummy_read_Z_value()/10,n_rota,n_magnet_z);

  return 0;
}




int	set_Z_value(float r)
{

#ifndef RAWHID_V1
  r = (r < 0) ? 0 : r;
#endif
  last_focus_target = (fabs(r) <= Pico_param.focus_param.Focus_max_expansion) ? r :
    Pico_param.focus_param.Focus_max_expansion;
  if (Pico_param.focus_param.Focus_direction == 0)  set_Z_value_raw(r);
  else      set_Z_value_raw(Pico_param.focus_param.Focus_max_expansion - r);
  return 0;
}
int	set_Z_value_OK(float r)
{
#ifndef RAWHID_V1
  r = (r < 0) ? 0 : r;
#endif
  last_focus_target = (fabs(r) <= Pico_param.focus_param.Focus_max_expansion) ? r :
    Pico_param.focus_param.Focus_max_expansion;

  if (Pico_param.focus_param.Focus_direction == 0)  set_Z_value_OK_raw(r);
  else      set_Z_value_OK_raw(Pico_param.focus_param.Focus_max_expansion - r);
  return 0;
}
int set_Z_obj_accurate(float r)
{
#ifndef RAWHID_V1
  r = (r < 0) ? 0 : r;
#endif
  last_focus_target = (r <= Pico_param.focus_param.Focus_max_expansion) ? r :
    Pico_param.focus_param.Focus_max_expansion;

  if (Pico_param.focus_param.Focus_direction == 0)  set_Z_obj_accurate_raw(r);
  else      set_Z_obj_accurate_raw(Pico_param.focus_param.Focus_max_expansion - r);
  return 0;
}

int 	read_Z_value(void)
{
  if (Pico_param.focus_param.Focus_direction == 0)  return read_Z_value_raw();
  else     return Pico_param.focus_param.Focus_max_expansion - read_Z_value_raw();
}
float 	read_Z_value_accurate_in_micron(void)
{
  if (Pico_param.focus_param.Focus_direction == 0)
    return read_Z_value_accurate_in_micron_raw();
  else
    return Pico_param.focus_param.Focus_max_expansion - read_Z_value_accurate_in_micron_raw();
}
float 	read_Z_value_OK(int *error)
{
  if (Pico_param.focus_param.Focus_direction == 0)
    return read_Z_value_OK_raw(error);
  else     return Pico_param.focus_param.Focus_max_expansion -
	     read_Z_value_OK_raw(error);
}
float 	read_last_Z_value(void)
{
  if (Pico_param.focus_param.Focus_direction == 0)  return read_last_Z_value_raw();
  else     return Pico_param.focus_param.Focus_max_expansion - read_last_Z_value_raw();
}

int	inc_Z_value(int st)
{
  float z = prev_focus;//read_last_Z_value();
  inc_Z_value_raw(st);
  last_focus_target = z +st;
  return 0;
}


int	request_set_focus_value (int image_n, float r, unsigned long *t0)
{
  r = (r < 0) ? 0 : r;
  last_focus_target = (r <= Pico_param.focus_param.Focus_max_expansion) ? r :
    Pico_param.focus_param.Focus_max_expansion;

  if (Pico_param.focus_param.Focus_direction == 0)  request_set_focus_value_raw (image_n, r, t0);
  else      request_set_focus_value_raw (image_n, Pico_param.focus_param.Focus_max_expansion - r, t0);
  return 0;
}


float 	read_focus_from_request_answer(char *answer, int im_n, float val, int *error)
{
  if (Pico_param.focus_param.Focus_direction == 0)
    return read_focus_raw_from_request_answer(answer, im_n, val, error);
  else     return Pico_param.focus_param.Focus_max_expansion
	     - read_focus_raw_from_request_answer(answer, im_n, val, error);
}

int read_Z(void)
{
  float z;
  if(updating_menu_state != 0)	return D_O_K;

  z = prev_focus; //read_Z_value_accurate_in_micron();
  win_printf("The objective position in z is %7.3f \\mu m\n",z);
  return D_O_K;
}
int set_Z(void)
{
  register int i;
  static float z = 0;
  char buf[256] = {0};

  if(updating_menu_state != 0)	return D_O_K;

  z = prev_focus;//read_Z_value_accurate_in_micron();
  i = win_scanf("Where do you want to go in \\mu m %f",&z);
  if (i == WIN_CANCEL)	return D_O_K;
  immediate_next_available_action(MV_OBJ_ABS, z);
  snprintf(buf,256,"Obj : %g Zmag %g rotation %g",z,n_magnet_z,n_rota);
  my_set_window_title("%s",buf);
  return D_O_K;
}

int move_obj_small_up(void)
{
  float z = 0;
  char buf[256] = {0};

  if(updating_menu_state != 0)	      return D_O_K;

  z = 0.5 + 10 * prev_focus; //read_Z_value_accurate_in_micron();
  z = 0.1*((int)(z)+1);
  if (is_there_pending_action_of_type(MV_OBJ_ABS) >= 0)
    {
      my_set_window_title("MV objective already pending");
      return D_O_K;
    }
  if (track_info->status_flag[track_info->c_i] & OBJ_MOVING)
    {
      my_set_window_title("Objective already moving");
      return D_O_K;
    }
  immediate_next_available_action(MV_OBJ_ABS, z);
  snprintf(buf,256,"Obj : %g Zmag %g rotation %g",z,n_magnet_z,n_rota);
  my_set_window_title("%s",buf);
  return D_O_K;
}

int move_obj_up(void)
{
  float z;
  char buf[256] = {0};

  if(updating_menu_state != 0)
  {
    add_keyboard_short_cut(0, KEY_F1, 0, move_obj_up);
    add_keyboard_short_cut(0, KEY_F2, 0, move_obj_small_up);
    return D_O_K;
  }
  z = 0.5 + prev_focus;//read_Z_value_accurate_in_micron();
  z = (float)((int)(z)+1);
  if (is_there_pending_action_of_type(MV_OBJ_ABS) >= 0)
    {
      my_set_window_title("MV objective already pending");
      return D_O_K;
    }
  if (track_info->status_flag[track_info->c_i] & OBJ_MOVING)
    {
      my_set_window_title("Objective already moving");
      return D_O_K;
    }
  immediate_next_available_action(MV_OBJ_ABS, z);
  snprintf(buf,256,"Obj : %g Zmag %g rotation %g",z,n_magnet_z,n_rota);
  my_set_window_title("%s",buf);
  return D_O_K;
}

int move_obj_small_dwn(void)
{
  float z;
  char buf[256] = {0};

  if(updating_menu_state != 0)
    {
      return D_O_K;
    }
  z = 0.5 + 10 * prev_focus;//read_Z_value_accurate_in_micron();
  z = 0.1*((int)(z)-1);
  if (is_there_pending_action_of_type(MV_OBJ_ABS) >= 0)
    {
      my_set_window_title("MV objective already pending");
      return D_O_K;
    }
  if (track_info->status_flag[track_info->c_i] & OBJ_MOVING)
    {
      my_set_window_title("Objective already moving");
      return D_O_K;
    }
  immediate_next_available_action(MV_OBJ_ABS, z);
  snprintf(buf,256,"Obj : %g Zmag %g rotation %g",z,n_magnet_z,n_rota);
  my_set_window_title("%s",buf);
  return D_O_K;
}

int move_obj_dwn(void)
{
  float z;
  char buf[256] = {0};
  if(updating_menu_state != 0)
    {
      add_keyboard_short_cut(0, KEY_F4, 0, move_obj_dwn);
      add_keyboard_short_cut(0, KEY_F3, 0, move_obj_small_dwn);
      return D_O_K;
    }
  z = 0.5 + prev_focus; //read_Z_value_accurate_in_micron();
  z = (float)((int)(z)-1);
  if (is_there_pending_action_of_type(MV_OBJ_ABS) >= 0)
    {
      my_set_window_title("MV objective already pending");
      return D_O_K;
    }
  if (track_info->status_flag[track_info->c_i] & OBJ_MOVING)
    {
      my_set_window_title("Objective already moving");
      return D_O_K;
    }
  immediate_next_available_action(MV_OBJ_ABS, z);
  snprintf(buf,256,"Obj : %g Zmag %g rotation %g",z,n_magnet_z,n_rota);
  my_set_window_title("%s",buf);
  return D_O_K;
}


float trk_get_light(void)
{
  int i, im, done;

  im = track_info->imi[track_info->c_i] + 1;
  i = (im > last_led_read.acknowledge) ? im : last_led_read.acknowledge + 1;
  last_led_read.proceeded_ask_bo_be_clear = i;
  for (done = 1, i = im; done == 1 && i < im + 100;)
    {  // we wait proceeded_RT to go down
      i = track_info->imi[(volatile int)track_info->c_i];
      done = (volatile int)last_led_read.proceeded_RT;
      my_set_window_title("waiting proceeded_RT to go down im %d state %d!", i - im, done);
    }
  im += 2;
  i = immediate_next_available_action(READ_LED_VAL, 0);
  if (i < 0)
    {
      win_printf("cannot fill next action");
      return -1;
    }
  for (done = 0, i = im; done == 0 && i < im + 100;)
    {
      i = track_info->imi[(volatile int)track_info->c_i];
      done = (volatile int)last_led_read.proceeded_RT;
      my_set_window_title("read T0 pid im %d state %d!",i-im,done);
    }
  if (i >= im + 100)
    {
      win_printf("could not read led");
      return -2;
    }

  return last_led_read.value;
}

int trk_set_light(float val)
{
  int i;//, im;

  //im = track_info->imi[track_info->c_i] + 1;
  i = immediate_next_available_action(CHG_LED_VAL, val);
  if (i < 0)
    {
      win_printf("cannot fill next action");
      return -1;
    }
  return 0;
}

int set_led(void)
{
  register int i;
  float z = 0;

  if(updating_menu_state != 0)	return D_O_K;

  //z = (float)get_light();
  z = trk_get_light();
  if (z < 0) z = trk_get_light(); // we retry
  if (z < 0) return win_printf_OK("cannot set led level");
  i = win_scanf("adjust light to %f %%",&z);
  if (i == WIN_CANCEL)	return D_O_K;
  if (trk_set_light(z) < 0)
    i = trk_set_light(z);
  if (i < 0) win_printf("cannot set led level");
  //set_light(z);
  return D_O_K;
}

int set_temp(void)
{
  register int i;
  float z = 0;

  if(updating_menu_state != 0)	return D_O_K;

  z = track_info->T0; //get_temperature(0);
  i = win_scanf("adjust temperature to %f degrees",&z);
  if (i == WIN_CANCEL)	return D_O_K;

  i = immediate_next_available_action(CHG_TEMPERATURE, z);
  if (i < 0)	win_printf_OK("Could not add pending action!");
  //set_temperature(z);
  return D_O_K;
}


int dialog_set_focus(char *focs)
{
  register int i;
  float fo = 0;

  if (sscanf(focs,"%f",&fo) != 1)
      return 0;
  i = immediate_next_available_action(MV_OBJ_ABS, fo);
  if (i < 0)	win_printf_OK("Could not add pending action!");
  return D_O_K;;
}



int grab_T0_pid_param(int param)
{
  int i, im, done;

  im = track_info->imi[track_info->c_i] + 1;
  i = (im > last_T0_pid.acknowledge) ? im : last_T0_pid.acknowledge + 1;
  last_T0_pid.proceeded_ask_bo_be_clear = i;
  for (done = 1, i = im; done == 1 && i < im + 100;)
    {  // we wait proceeded_RT to go down
      i = track_info->imi[(volatile int)track_info->c_i];
      done = (volatile int)last_T0_pid.proceeded_RT;
      my_set_window_title("waiting proceeded_RT to go down im %d state %d!", i - im, done);
    }
  im += 2;
  i = fill_next_available_action_with_spare(im, READ_T0_PID, 0, param);
  if (i < 0)
    {
      win_printf("cannot fill next action");
      return -1;
    }
  for (done = 0, i = im; done == 0 && i < im + 100;)
    {
      i = track_info->imi[(volatile int)track_info->c_i];
      done = (volatile int)last_T0_pid.proceeded_RT;
      my_set_window_title("read T0 pid im %d state %d!",i-im,done);
    }
  if (i >= im + 100)
    {
      return win_printf("could not read param %d",param);
      return -2;
    }
  //last_T0_pid.proceeded_RT = 2;
  return (int)last_T0_pid.value;
}


int chg_T0_pid_param(int param, int value)
{
  int i, im, done;

  im = track_info->imi[track_info->c_i] + 1;
  i = (im > last_T0_pid.acknowledge) ? im : last_T0_pid.acknowledge + 1;
  last_T0_pid.proceeded_ask_bo_be_clear = i;

  for (done = 1, i = im; done == 1 && i < im + 100;)
    {  // we wait proceeded_RT to go down
      i = track_info->imi[(volatile int)track_info->c_i];
      done = (volatile int)last_T0_pid.proceeded_RT;
      my_set_window_title("waiting proceeded_RT to go down im %d state %d!", i - im, done);
    }
  im += 2;
  i = fill_next_available_action_with_spare(im, CHG_T0_PID, value, param);
  if (i < 0)
    {
      win_printf("cannot fill next action");
      return -1;
    }
  for (done = 0, i = im; done == 0 && i < im + 100;)
    {
      i = track_info->imi[(volatile int)track_info->c_i];
      done = (volatile int)last_T0_pid.proceeded_RT;
      my_set_window_title("read T0 pid im %d state %d!",i-im,done);
    }
  if (i >= im + 100)
    {
      return win_printf("could not read param %d",param);
      return -2;
    }

  //  last_T0_pid.proceeded_RT = 2;
  return (int)last_T0_pid.value;
}





int	do_set_T0_pid(void)
{
  register int i;
  int T_P, T_I, T_D, Ton, Bon;
  int T_Pc, T_Ic, T_Dc, Tonc, Bonc;

  if(updating_menu_state != 0)	 return D_O_K;

  if (set_T0_pid == NULL || get_T0_pid == NULL)
    return win_printf_OK("No PID setting available on T0");

  T_Pc = T_P = grab_T0_pid_param(0);
  if (T_P < 0) return D_O_K;
  T_Ic = T_I = grab_T0_pid_param(1);
  if (T_I < 0) return D_O_K;
  T_Dc = T_D = grab_T0_pid_param(2);
  if (T_D < 0) return D_O_K;
  Tonc = Ton = grab_T0_pid_param(4);
  if (Ton < 0) return D_O_K;
  Bonc = Bon = grab_zmag_pid_param(9);
  if (Bon < 0) return D_O_K;


  i = win_scanf("Temperature PID parameters setting\n"
		"Entering wrong parameters will produce\n"
		"instable temperatures! \n"
		"Proportional (default 64)% 6d\n"
		"Integral (default 16) %6d\n"
  		"Derivative (default 0) %6d\n"
		"Temperature PID (%R->Off)   (%r->On)\n"
		"General brake (%R->Off)   (%r->On)\n"
		,&T_P,&T_I,&T_D,&Ton,&Bon);
  if (i == WIN_CANCEL) return OFF;
  if (T_P != T_Pc || T_I != T_Ic || T_D != T_Dc || Ton != Tonc || Bon != Bonc)
    {
      if (T_P != T_Pc)
	{
	  i = chg_T0_pid_param(0, T_P);
	  if (i < 0) return win_printf_OK("Rot PID problem!");
	}
      if (T_I != T_Ic)
	{
	  i = chg_T0_pid_param(1, T_I);
	  if (i < 0) return win_printf_OK("Rot PID problem!");
	}
      if (T_D != T_Dc)
	{
	  i = chg_T0_pid_param(2, T_D);
	  if (i < 0) return win_printf_OK("Rot PID problem!");
	}
      if (Bon != Bonc)
	{
	  i = chg_zmag_pid_param(9, Bon);
	  if (i < 0) return win_printf_OK("Zmag PID problem!");
	}
      if (Ton != Tonc)
	{
	  i = chg_T0_pid_param(4, Ton); // we switch back PID
	  if (i < 0) return win_printf_OK("Rot PID problem!");
	}
    }
  if (Bon != 0)
    {
      win_printf("Brake is ON, both motors will not work\n"
		      "neither the temperature control!");
    }
  if (Ton == 0)
    {
      win_printf("Temperature PID is OFF\n Temperature will not be regulated!");
    }
  return D_O_K;
}




MENU *micro_menu(void)
{
	static MENU mn[32] = {0};

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"Adjust LED", set_led,NULL,0,NULL);
	add_item_to_menu(mn,"Adjust Temperature", set_temp,NULL,0,NULL);
	add_item_to_menu(mn,"Adjust Temperature PID", do_set_T0_pid,NULL,0,NULL);


	return mn;
}


MENU *focus_plot_menu(void)
{
	static MENU mn[32] = {0};

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"Read Objective position", read_Z,NULL,0,NULL);
	add_item_to_menu(mn,"Set Objective position", set_Z,NULL,0,NULL);
	add_item_to_menu(mn,"Move up", move_obj_up,NULL,0,NULL);
	add_item_to_menu(mn,"Move down", move_obj_dwn,NULL,0,NULL);

	return mn;
}

int	focus_main(int argc, char **argv)
{
  //PXV_VAR(MENU*, plot_experiment_menu);
  float foc;
  //win_printf("entering focus main");
    (void)argc;
    (void)argv;
	set_focus_device(&(Pico_param.focus_param));
	if (has_focus_memory() == 0)
	  {
	    Open_Pico_config_file();
	    foc = get_config_float("STATE", "ObjZ", 5);
	    Close_Pico_config_file();
#ifndef RAWHID_V2
	    set_Z_value(foc);
#endif
	    device_has_memory = 0;
	  }
	last_focus_target = read_Z_value_accurate_in_micron();
# ifdef DYNAMIC
	add_image_treat_menu_item ( "focus", NULL, focus_plot_menu(), 0, NULL);
# endif

#ifdef RAWHID_V1
  if (rawhid_init(1)!=0) win_printf_OK("Rawhid comm failed. Check your cable and retry with menu");
  else launch_rawhid_thread();
#endif
	return D_O_K;
}

int shut_down_focus(void)
{
  if (device_has_memory == 0)
    {
      Open_Pico_config_file();
      set_config_float("STATE", "ObjZ", prev_focus);
      write_Pico_config_file();
      Close_Pico_config_file();
      //win_printf("Saving focus position");
    }
  return 0;
}


# endif /* _ZOBJ_C_ */
