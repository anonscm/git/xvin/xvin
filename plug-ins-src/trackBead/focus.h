#ifndef _FOCUS_H_
#define _FOCUS_H_
#include "../cfg_file/Pico_cfg.h"
//# include "Pico_cfg.h"

#ifdef _FOCUS_C_

//float   *focus_position = NULL, *focus_Z_step = NULL;


int     (*init_focus_OK)(void);
int	(*what_is_objective_device)(void);
char	*(*describe_objective_device)(void);

int 	(*read_Z_value_raw)(void);
int 	read_Z_value(void);
/* this function reads the Z position of the objective it return an integer in tenth of
microns example 235 corresponds to 23.5 microns */

float 	(*read_Z_value_accurate_in_micron_raw)(void);
float 	read_Z_value_accurate_in_micron(void);
/* this function reads the Z position of the objective it return a float in
microns. This function is replacing read_Z_value() */

float 	(*read_Z_value_OK_raw)(int *error);
float 	read_Z_value_OK(int *error);
/* this function reads the Z position of the objective it return a float in
microns an error flag is provided. This function is replacing read_Z_value() */


float 	(*read_last_Z_value_raw)(void);
float 	read_last_Z_value(void);
/* this function reads the last Z position of the objective it return a float in
microns it just grab the last position and return very fast, it does not ask physically for the position */



/* setting the objective position */

int 	(*set_Z_value_raw)(float z);
int 	set_Z_value(float z);
/* 	move the objective to the position z in micron, the return value corresponds to an error
flag : 0 -> no error , RS232_ERROR	in case of rs232 error */

int	(*set_Z_obj_accurate_raw)(float zstart);
int	set_Z_obj_accurate(float zstart);
/* 	move the objective to the position z in micron with improved accuracy, the return value
corresponds to an error flag : 0 -> no error , RS232_ERROR	in case of rs232 error */

int 	(*set_Z_value_OK_raw)(float z);
int 	set_Z_value_OK(float z);
/* 	move the objective to the position z in micron, the return value corresponds to an error
flag : 0 -> no error , RS232_ERROR	in case of rs232 error */


/* setting the objective position by quatized step (for stepper motors) */


int 	(*set_Z_step)(float z);
/* define the stepping size to move the objective */

int 	inc_Z_value(int step);
/* move the objective by a certain number of steps. The stepping size is defined
by set_Z_step(float z) */
int 	(*inc_Z_value_raw)(int step);

/* functions used to move the objective with arrows on the keyboard */

int 	(*small_inc_Z_value)(int up);

int 	(*big_inc_Z_value)(int up);

int (*set_light)(float val);
float (*get_light)(void);

int (*set_temperature)(float val);
float (*get_temperature)(int t);

int (*has_focus_memory)(void);




int (*request_read_temperature_value)(int image_n, int t, unsigned long *t0);
float (*read_temperature_from_request_answer) (char *answer, int im_n, float val, int *error, int *channel);

int (*request_read_focus_value)(int image_n, unsigned long *t0);
float read_focus_from_request_answer(char *answer, int im_n, float val, int *error);
float (*read_focus_raw_from_request_answer)(char *answer, int im_n, float val, int *error);

int request_set_focus_value (int image_n, float z, unsigned long *t0);
int (*request_set_focus_value_raw) (int image_n, float z, unsigned long *t0);
int (*check_set_focus_request_answer) (char *answer, int im_n, float z);

int     (*set_T0_pid) (int num, int val);
int     (*get_T0_pid) (int num);



/* function available on the LEICA only */

//acreg   *(*z_objective_menu)(void);

float last_focus_target = 0;
int focus_transient = 2;

#ifdef RAWHID_V1
int (*rawhid_init)(int verbose);
int (*launch_rawhid_thread)(void);
#endif

# else
//PXV_VAR(float*, focus_position);
//PXV_VAR(float*, focus_Z_step);

PXV_VAR(int, focus_transient);
PXV_FUNCPTR(int, what_is_objective_device,(void));
PXV_FUNCPTR(char*, describe_objective_device,(void));
PXV_FUNCPTR(int, read_Z_value_raw,(void));
PXV_FUNCPTR(float, read_last_Z_value_raw,(void));
PXV_FUNCPTR(float, read_Z_value_accurate_in_micron_raw,(void));
PXV_FUNCPTR(float, read_Z_value_OK_raw, (int *error));


PXV_FUNC(int, read_Z_value,(void));
PXV_FUNC(float, read_last_Z_value,(void));
PXV_FUNC(float, read_Z_value_accurate_in_micron,(void));
PXV_FUNC(float, read_Z_value_OK, (int *error));

PXV_FUNC(int, set_Z_value, (float z));
PXV_FUNC(int, set_Z_obj_accurate, (float zstart));
PXV_FUNC(int, set_Z_value_OK, (float z));

PXV_FUNCPTR(int, set_Z_value_raw, (float z));
PXV_FUNCPTR(int, set_Z_obj_accurate_raw, (float zstart));
PXV_FUNCPTR(int, set_Z_value_OK_raw, (float z));

PXV_FUNCPTR(int, set_Z_step, (float z));
PXV_FUNC(int, inc_Z_value, (int step));
PXV_FUNCPTR(int, inc_Z_value_raw, (int step));
PXV_FUNCPTR(int, small_inc_Z_value, (int up));
PXV_FUNCPTR(int, big_inc_Z_value, (int up));


PXV_FUNCPTR(int, set_light, (float val));
PXV_FUNCPTR(float, get_light, (void));

PXV_FUNCPTR(int, set_temperature, (float val));
PXV_FUNCPTR(float, get_temperature, (int));
PXV_VAR(float, last_focus_target);


#ifdef RAWHID_V1
PXV_FUNCPTR(int, rawhid_init, (int verbose));
PXV_FUNCPTR(int, launch_rawhid_thread, (void));
#endif

PXV_FUNCPTR(int, request_read_temperature_value, (int image_n, int t, unsigned long *t0));
PXV_FUNCPTR(float, read_temperature_from_request_answer, (char *answer, int im_n, float val, int *error, int *channel));

PXV_FUNCPTR(int, request_read_focus_value, (int image_n, unsigned long *t0));
PXV_FUNC(float, read_focus_from_request_answer, (char *answer, int im_n, float val, int *error));
PXV_FUNCPTR(float, read_focus_raw_from_request_answer, (char *answer, int im_n, float val, int *error));

PXV_FUNC(int, request_set_focus_value, (int image_n, float z, unsigned long *t0));
PXV_FUNCPTR(int, request_set_focus_value_raw, (int image_n, float z, unsigned long *t0));
PXV_FUNCPTR(int, check_set_focus_request_answer, (char *answer, int im_n, float z));

PXV_FUNCPTR(int, set_T0_pid, (int num, int val));
PXV_FUNCPTR(int,  get_T0_pid, (int num));



# endif

PXV_FUNC(int,read_Z,(void));
PXV_FUNC(int,set_Z,(void));
PXV_FUNC(int,focus_main,(int argc, char **argv));
PXV_FUNC(int, set_focus_device, (Focus_param *f_p));
PXV_FUNC(int, dialog_set_focus, (char *focs));
PXV_FUNC(MENU*, focus_plot_menu, (void));
PXV_FUNC(MENU*, micro_menu, (void));
PXV_FUNC(int, shut_down_focus, (void));

# endif
