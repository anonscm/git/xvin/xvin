
#ifndef _SDI_2__FUNCTIONS_C_
#define _SDI_2__FUNCTIONS_C_

# include "SDI_2_functions_real.h"
# include <stdlib.h>
# include <string.h>
# include <math.h>
# include <time.h>

static float yratio=3; //pour la sélectino en y
static bool move_franges=1;
static int nb_init_cycles=128;

//static int k_averaging_frame_nb=1024;
//static int k_buffer_active=0;


int SDI_2_get_tracking_parameters( float *ext_yratio)
{

  *ext_yratio=yratio;
  return 0;
}

int SDI_2_initialize_phase(b_track *bt)
{
  if (SDI_2_initialize_phase_fringe(bt,bt->frange_top)) return 1;;
  if (SDI_2_initialize_phase_fringe(bt,bt->frange_bottom)) return 1;

  return 0;
}

int SDI_2_initialize_phase_fringe(b_track *bt,sdi_fr *f1)
{
  if (f1==NULL) return 1;

  f1->r0=0;
  f1->phi0=0;
  f1->init_cycles=0;
  f1->yg=0;
  f1->yd=0;
  f1->buffer_full = 0;
  f1->c_buff = 0;



  return 0;
}




int SDI_2_do_set_tracking_parameters(void)
{
  int i;
  if (updating_menu_state!=0) return 0;
  i=win_scanf("Please indicate the value of the tracking parameters : \n"
              "Indicate the  ratio signal/ maximum amplitude to take in account y -lines : %f\n"
              "Choose the number of frames on which initialize the phase : %d \n"
              "Click to activate frange tracking (if not, they will stay fixed) : %b\n",
              //"Click to activate k buffer : %b \n",
          &yratio,&nb_init_cycles,&move_franges);//,&k_buffer_active);

  return 0;

}




int SDI_2_update_frange_position(b_track *bt,O_i *oic,int length, int width, sdi_fr *f1)

{

  int yg,yd,ymax=0;
  float ymax_real;
  float dummy;
  float dummy2;
  double pos_x;
  double Max_pos=0;
  int i=0;
  double low,high;
  double k=0,phif=0;
  double k_av=0;
  int yco,yend,xco,xend;
  int onx = oic->im.nx;
  int ony = oic->im.ny;
  int cw2 = width >> 1; //divide by 2
  int cl2 = length >> 1; // divide by 2

  float total_amplitude = 0;


  int ifxmax,ifxmin,ifymax,ifymin;

  ifxmin = cl2;
  ifxmax = onx - cl2;

  ifymin = cw2;
  ifymax = ony - cw2;

  //adjust beginning and ending of the franges so that they don't go outside the image
  if (f1->ifx < ifxmin) f1->ifx = ifxmin;
  if (f1->ifx > ifxmax) f1->ifx = ifxmax;

  if (f1->ify < ifymin) f1->ify = ifymin;
  if (f1->ify > ifymax) f1->ify = ifymax;

  yco = f1->ify-cw2;
  yend =  f1->ify+cw2-1 ;
  xco = f1->ifx-cl2;
  xend = f1->ifx+cl2-1;
  int ecart_x = 0, ecart_y = 0, new_ifx = 0,new_ify=0;
  int moving = 1;
  if (xco>=xend || xco>=onx || xend <=0) return 1;
  if (yco>=yend || yco>=ony || yend <=0) return 1;


  while (moving == 1)
  {

    yco = f1->ify-cw2;
    yend =  f1->ify+cw2-1 ;
    xco = f1->ifx-cl2;
    xend = f1->ifx+cl2-1;

  if (SDI_2_fill_x_av_y_profile_from_im(oic, xco,  xend,  yco,  yend,  f1->yi,&ymax,&yg,&yd)==1) return 1;
  find_max_around(f1->yi,width,ymax,&ymax_real,&dummy, &dummy2);
  if (SDI_2_fill_y_av_x_profile_from_im(oic, xco,  xend,yco,  yend,  f1->xi,f1->fit_max_coeffs,&Max_pos)==1) return 1;


  if (move_franges && bt->moving_forbidden == 0)
  {

     new_ifx=(int)(0.5+ f1->ifx + Max_pos - length/2);
    f1->fy = f1->ify - width/2 + ymax_real;
     new_ify=(int)( 0.5+ f1->fy);


    if (new_ifx <ifxmin) new_ifx = ifxmin;
    if (new_ifx >ifxmax) new_ifx = ifxmax;
    if (new_ify <ifymin) new_ify = ifymin;
    if (new_ify >ifymax) new_ify = ifymax;
     ecart_x = new_ifx - f1->ifx;

     ecart_y = new_ify - f1->ify;

    if (abs(ecart_x)>0)
    {
      f1->ifx=new_ifx;

    }
    if (abs(ecart_y)>0)
    {
     f1->ify=new_ify;

    }

    if (ecart_x == 0 && ecart_y == 0) moving = 0;

  }

}
yco = f1->ify-cw2;
yend =  f1->ify+cw2-1 ;
xco = f1->ifx-cl2;
xend = f1->ifx+cl2-1;
if (SDI_2_fill_x_av_y_profile_from_im(oic, xco,  xend,  yco,  yend,  f1->yi,&ymax,&yg,&yd)==1) return 1;
if (SDI_2_fill_y_av_x_profile_from_im(oic, xco,  xend,yco,  yend,  f1->xi,f1->fit_max_coeffs,&Max_pos)==1) return 1;

  if (f1->init_cycles == 0)
  {
    f1->best_align = 0;
    SDI_2_interpolate_profile(f1);
    SDI_2_update_mean_profile(f1);
    pos_x = 0;
    f1->init_cycles = 1;
  }
  else
  {
  SDI_2_align_profile(f1,&pos_x);
  SDI_2_interpolate_profile(f1);
  SDI_2_update_mean_profile(f1);
}



  f1->fy = f1->ify - width/2 + ymax_real;
  f1->fx = f1->ifx - pos_x ;




  return 0;

}


int SDI_2_project_profile_on_profile(O_i *oic, int xco, int xend, int yco, int yend, float *xi, float *yi)
{
    int i,j;
    int lx=xend-xco+1;
    int ly=yend-yco+1;
    float temp_mr[lx];

    for (i=0;i<ly;i++)
    {
      yi[i]=0;
      extract_raw_partial_line(oic, yco+i, temp_mr,xco,lx);
      for (j=0;j<lx;j++)
      {
        yi[i]+=temp_mr[j]*xi[j];
      }

    }

  return 0;


}






int SDI_2_fill_x_av_y_profile_from_im(O_i *oi, int xco, int xend, int yco, int yend, float *y, int *ym,int *yg, int *yd)

{
    int j, i;
    int ly=yend-yco+1;
    int lx=xend-xco+1;

    float ymax=0;
    union pix *pd=NULL;


    pd  = oi->im.pixel;


    if (oi == NULL  || y == NULL)   return 1;


    for (i = 0; i < ly; i++)      y[i] = 0;

    if (oi->im.data_type == IS_CHAR_IMAGE)
    {

        for (i = 0; i < ly; i++)
        {
            for (j = 0; j < lx; j++)
                y[i] += (float) pd[yco+i].ch[xco+j];
            y[i]=y[i]/lx;
        }



    }
    else if (oi->im.data_type == IS_INT_IMAGE)
    {
      for (i = 0; i < ly; i++)
      {
          for (j = 0; j < lx; j++)
              y[i] += (float) pd[yco+i].in[xco+j];
          y[i]=y[i]/lx;
      }


    }
    else if (oi->im.data_type == IS_UINT_IMAGE)
    {
      for (i = 0; i < ly; i++)
      {
          for (j = 0; j < lx; j++)
              y[i] += (float) pd[yco+i].ui[xco+j];
          y[i]=y[i]/lx;
      }

    }

    else return 1;

    for (i=0;i<ly;i++)
    {
      if (y[i]>ymax)
      {
        *ym=i;
        ymax=y[i];
      }
    }

    (*yg)=(*ym);
    (*yd)=(*ym);
    //find yg limit given yratio
    while ( (y[*yg]>ymax/yratio) && (*yg>0))
    {
      *yg-=1;
    }

    //find yd limit given y ratio
    while ( (y[*yd]>ymax/yratio) && (*yd<ly-1))
    {
      *yd+=1;
    }

    return 0;
}

int SDI_2_fill_y_av_x_profile_from_im(O_i *oi, int xco, int xend, int yco, int yend, float *x, double *fit_param,double *xm)

{
    int j, i;
    int ly=yend-yco+1;
    int lx=xend-xco+1;
    double *a = NULL;


    union pix *pd=NULL;


    pd  = oi->im.pixel;


    if (oi == NULL  || x == NULL)   return 1;


    for (i = 0; i < lx; i++)      x[i] = 0;

    if (oi->im.data_type == IS_CHAR_IMAGE)
    {

        for (i = 0; i < lx; i++)
        {
            for (j = 0; j < ly; j++)
                x[i] += (float) pd[yco+j].ch[xco+i];
            x[i]=x[i]/ly;
        }



    }
    else if (oi->im.data_type == IS_INT_IMAGE)
    {

              for (i = 0; i < lx; i++)
              {
                  for (j = 0; j < ly; j++)
                      x[i] += (float) pd[yco+j].in[xco+i];
                  x[i]=x[i]/ly;
              }



    }
    else if (oi->im.data_type == IS_UINT_IMAGE)
    {

              for (i = 0; i < lx; i++)
              {
                  for (j = 0; j < ly; j++)
                      x[i] += (float) pd[yco+j].ui[xco+i];
                  x[i]=x[i]/ly;
              }


    }
    else return 1;




    d_s *ds = build_data_set(5,5);
    ds->nx=ds->ny=5;

    for (i=lx/2-10;i<lx/2+10;i++)
    {
      if (x[i]>ds->yd[2])
      {
        ds->xd[2]=(double)i;
        ds->yd[2]=x[i];
      }
    }

    int deb =(int) (ds->xd[2]+5);
    int fin =(int) (ds->xd[2]+15);
    ds->yd[3] = 0;
    for (i=deb;i<fin;i++)
    {
      if (x[i]>ds->yd[3])
      {
        ds->xd[3]=(double)i;
        ds->yd[3]=x[i];
      }
    }


    deb =(int) (ds->xd[3]+5);
    fin =(int) (ds->xd[3]+15);
    ds->yd[4] = 0;
    for (i=deb;i<fin;i++)
    {
      if (x[i]>ds->yd[4])
      {
        ds->xd[4]=(double)i;
        ds->yd[4]=x[i];
      }
    }

    deb =(int) (ds->xd[2]-5);
    fin =(int) (ds->xd[2]-15);
    ds->yd[1] = 0;
    for (i=deb;i>=fin;i--)
    {
      if (x[i]>ds->yd[1])
      {
        ds->xd[1]=(double)i;
        ds->yd[1]=x[i];
      }
    }

    deb =(int) (ds->xd[1]-5);
    fin =(int) (ds->xd[1]-15);
    ds->yd[0] = 0;
    for (i=deb;i>=fin;i--)
    {
      if (x[i]>ds->yd[0])
      {
        ds->xd[0]=(double)i;
        ds->yd[0]=x[i];
      }
    }


    if (fit_ds_to_xn_polynome(ds, 3, ds->xd[0]-1, ds->xd[4]+1, 0, 256, &a) < 0) *xm = ds->xd[2];
    else *xm = -a[1]/(2*a[2]);

    if (!((*xm>ds->xd[0]) && (*xm<ds->xd[4])))  *xm = ds->xd[2];
    free_data_set(ds);


    if (a!=NULL)
    {
      fit_param[0] = a[0];
      fit_param[1] = a[1];
      fit_param[2] = a[2];
      free(a);
    }





    return 0;
}


int SDI_2_activate_tracking(void)
{
  move_franges = !move_franges;
  return 0;

}



MENU *SDI_2_tracking_parameters_menu(void)
{
    static MENU mn[32] = {0};

    if (mn[0].text != NULL) return mn;
    add_item_to_menu(mn, "Change tracking parameters", SDI_2_do_set_tracking_parameters, NULL, 0, NULL);

    return mn;
}

 #endif
