
#ifndef _SCAN_ZMAG_C_
#define _SCAN_ZMAG_C_

# include "allegro.h"
#ifdef XV_WIN32
    # include "winalleg.h"
#endif

# include "xvin.h"
# include "../wlc/wlc.h"

/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "../cfg_file/Pico_cfg.h"
#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "../fft2d/fftbtl32n.h"
# include "fillibbt.h"
# include "focus.h"
# include "magnetscontrol.h"
# include "trackBead.h"
# include "brown_util.h"
# include "track_util.h"
# include "scan_zmag.h"
# include "action.h"
# include "record.h"
int trajectories_op_idle_action(O_p *op, DIALOG *d);


g_record *scan_r = NULL;
# define SCAN_RUNNING  1
# define SCAN_STOPPED  2
# define SCAN_SUSPENDED  4
int scan_state = 0;

static int last_bead = -1;
//static float z_cor = 0.878;
//static float min_scale_rot = 1;
static float min_scale_zmag = 1;
static float min_scale_z = 0.5;

static scan_param *sp = NULL;
char scan_state_disp[4096];


int scan_op_post_display(struct one_plot *op, DIALOG *d)
{
  register int i, j;
  BITMAP *imb = NULL;
  d_s *ds = NULL;
  //int na = 0, nb = 0, xc, yc;
  int  nb, xc, yc;
  //float f;

  if (d == NULL || d->dp == NULL || d->proc == NULL)    return 1;
  if (imr_and_pr) return 0;
  ds = op->dat[0];
  if (ds == NULL || ds->source == NULL)       return 0;

  //  if (sscanf(ds->source,"Bead tracking at %f Hz Acquistion %d for bead %d"
  //     ,&f,&na,&nb) != 3) return 0;
  nb = op->user_ispare[ISPARE_BEAD_NB];

  if (nb >= 0 && nb  < track_info->n_b)
    {
      imb = (BITMAP*)oi_TRACK->bmp.stuff;
      if (oi_TRACK->need_to_refresh & BITMAP_NEED_REFRESH)
	{
	  display_image_stuff_16M(imr_TRACK,d_TRACK);
	  oi_TRACK->need_to_refresh |= INTERNAL_BITMAP_NEED_REFRESH;
	  show_bead_cross(imb, imr_TRACK, d_TRACK);
	}
      for(i = nb - 1, j = 0; (i < nb + 2) && (j < track_info->n_b) && (j < 3); i++, j++)
	{
	  xc = track_info->bd[(i+track_info->n_b)%track_info->n_b]->x0 - 100;
	  yc = oi_TRACK->im.ny - track_info->bd[(i+track_info->n_b)%track_info->n_b]->y0 - 100;
	  acquire_bitmap(plt_buffer);
	  blit(imb,plt_buffer,xc,yc,SCREEN_W - 300, 60 + 256*j, 200, 200);
	  release_bitmap(plt_buffer);
	}
    }
  return 0;
}

int scan_op_idle_action_old(O_p *op, DIALOG *d)
{
  if (d == NULL || d->dp == NULL || d->proc == NULL)    return 1;
  if (op->need_to_refresh)      d->proc(MSG_DRAW,d,0);
  return 0;
}

int scan_op_idle_action(O_p *op, DIALOG *d)
{
  register int i, j;
  BITMAP *imb = NULL;
  d_s *ds = NULL;
  //int na = 0, nb = 0,
  int nb, xc, yc;
  //float f;
  //char buf[512];

  if (d == NULL || d->dp == NULL || d->proc == NULL)    return 1;
  if (op->need_to_refresh)      d->proc(MSG_DRAW,d,0);
  if (imr_and_pr) return 0;
  ds = op->dat[0];
  if (ds == NULL || ds->source == NULL)       return 0;

  //if (sscanf(ds->source,"Bead tracking at %f Hz Acquistion %d for bead %d"
  //	     ,&f,&na,&nb) != 3) return 0;
  nb = op->user_ispare[ISPARE_BEAD_NB];

  if (nb >= 0 && nb  < track_info->n_b)
    {
      imb = (BITMAP*)oi_TRACK->bmp.stuff;
      if (oi_TRACK->need_to_refresh & BITMAP_NEED_REFRESH)
	{
	  display_image_stuff_16M(imr_TRACK,d_TRACK);
	  oi_TRACK->need_to_refresh |= INTERNAL_BITMAP_NEED_REFRESH;
	  show_bead_cross(imb, imr_TRACK, d_TRACK);
	}

      for(i = nb - 1, j = 0; (i < nb + 2) && (j < track_info->n_b) && (j < 3); i++, j++)
	{
	  xc = track_info->bd[(i+track_info->n_b)%track_info->n_b]->x0 - 100;
	  yc = oi_TRACK->im.ny - track_info->bd[(i+track_info->n_b)%track_info->n_b]->y0 - 100;
	  for(;screen_acquired;); // we wait for screen_acquired back to 0;
	  screen_acquired = 1;

	  acquire_bitmap(screen);
	  blit(imb,screen,xc,yc,SCREEN_W - 300, 60 + 256*j, 200, 200);
	  release_bitmap(screen);
	  screen_acquired = 0;
	  oi_TRACK->need_to_refresh &= ~INTERNAL_BITMAP_NEED_REFRESH;
	}
    }
  return 0;
}

# ifdef OLD

int scan_job_xyz(int im, struct future_job *job)
{
  register int j, i;
  int ci;
  b_track *bt = NULL;
  d_s *dsx = NULL, *dsy = NULL, *dsz = NULL;
  float ax, dx, ay, dy, y_over_x, y_over_z;
  un_s *un = NULL;

  if (job == NULL || job->oi == NULL || job->in_progress == 0 || job->op->n_dat < 3) return 0;
  if (im < job->imi)  return 0;

  ci = track_info->c_i;
  dsx = job->op->dat[0];
  dsy = job->op->dat[1];
  dsz = job->op->dat[2];
  bt = track_info->bd[job->bead_nb];


  un = job->oi->xu[job->oi->c_xu];
  get_afine_param_from_unit(un, &ax, &dx);
  un = job->oi->yu[job->oi->c_yu];
  get_afine_param_from_unit(un, &ay, &dy);
  y_over_x = (dy != 0) ? dx/dy : 1;
  y_over_z = track_info->focus_cor*1e-6;
  y_over_z = (dy != 0) ? y_over_z/dy : 1;


  for (j = 0; track_info->imi[ci] > job->imi && j <TRACKING_BUFFER_SIZE ; j++)
    {
      ci--;
      if (ci < 0) ci += TRACKING_BUFFER_SIZE;
    }

  if (track_info->imi[ci] > job->imi) return 0;

  for (i = 0; track_info->imi[ci] >= (job->imi-32) && i < TRACKING_BUFFER_SIZE; i++)
    {
      j = track_info->imi[ci] - job->in_progress;
      if (j < dsx->mx)
	{
	  dsx->yd[j] = y_over_x * bt->x[ci];
	  dsy->yd[j] = bt->y[ci];
	  dsz->yd[j] = y_over_z * bt->z[ci];
	  dsx->xd[j] = dsy->xd[j] = dsz->xd[j] = j;
	}
      ci--;
      if (ci < 0) ci += TRACKING_BUFFER_SIZE;
    }
  j = job->imi - job->in_progress;
  if (j <= dsx->mx)
    dsx->nx = dsx->ny = dsy->nx = dsy->ny = dsz->nx = dsz->ny = j;
  if (j >= dsx->mx)
        dsx->nx = dsx->ny = dsy->nx = dsy->ny = dsz->nx = dsz->ny = dsx->mx;
  job->op->need_to_refresh = 1;
  if (dsx->nx < dsx->mx) job->imi += 32;
  else job->in_progress = 0;
  return 0;
}




int scan_job_zmag(int im, struct future_job *job)
{
  register int j, i;
  int ci;
  b_track *bt = NULL;
  d_s *ds = NULL;

  if (job == NULL || job->oi == NULL || job->in_progress == 0 || job->op->n_dat < 1) return 0;
  if (im < job->imi)  return 0;

  ci = track_info->c_i;

  for (j = 0; track_info->imi[ci] > job->imi && j <TRACKING_BUFFER_SIZE ; j++)
    {
      ci--;
      if (ci < 0) ci += TRACKING_BUFFER_SIZE;
    }
  if (track_info->imi[ci] > job->imi) return 0;
  ds = job->op->dat[0];

  bt = track_info->bd[job->bead_nb];

  for (i = 0; track_info->imi[ci] >= (job->imi-32) && i < TRACKING_BUFFER_SIZE; i++)
    {
      j = track_info->imi[ci] - job->in_progress;
      if (j < ds->mx)
	{
	  ds->yd[j] = track_info->zmag[ci];
	  ds->xd[j] = j;
	}
      ci--;
      if (ci < 0) ci += TRACKING_BUFFER_SIZE;
    }
  j = job->imi - job->in_progress;
  if (j <= ds->mx)
    ds->nx = ds->ny = j;
  if (j >= ds->mx)
        ds->nx = ds->ny = ds->mx;
  job->op->need_to_refresh = 1;
  if (ds->nx < ds->mx) job->imi += 32;
  else job->in_progress = 0;
  return 0;
}


int scan_job_scan(int im, struct future_job *job)
{
  int  ci,  ims, ime, i, k;
  b_track *bt = NULL;
  d_s *ds = NULL, *dsx = NULL, *dsy = NULL, *dsz = NULL;
  scan_param *sp = NULL;
  un_s *un = NULL;
  float ay, dy, axl, dxl;

  if (job == NULL || job->oi == NULL || job->in_progress == 0 || job->more_data == NULL) return 0;
  if (im < job->imi)  return 0;

  ci = track_info->c_i;
  ds = job->op->dat[0];
  sp = (scan_param *)job->more_data;
  bt = track_info->bd[job->bead_nb];
  if (bt->opt == NULL)   return 0;
  dsx = bt->opt->dat[0];
  dsy = bt->opt->dat[1];
  dsz = bt->opt->dat[2];
  un = bt->opt->yu[bt->opt->c_yu];
  get_afine_param_from_unit(un, &ay, &dy);
  un = job->op->xu[job->op->c_xu];
  get_afine_param_from_unit(un, &axl, &dxl);
  dy /= dxl;

  ims = sp->im[ds->nx] - sp->im[0] + sp->dead;
  if (ds->nx + 1 >= ds->mx)   return 0;
  ime = sp->im[ds->nx + 1] - sp->im[0];

  for(i = ims, k = 0, ds->xd[ds->nx] = 0; i < ime; i++, k++)
    ds->xd[ds->nx] += dsz->yd[i];
  if (k) ds->xd[ds->nx] /= k;
  ds->xd[ds->nx] = ay + dy * ds->xd[ds->nx];
  ds->yd[ds->nx] = sp->zm[ds->nx];
  ds->ny = ds->nx = ds->nx + 1;

  job->op->need_to_refresh = 1;
  if (ds->nx < ds->mx) job->imi = sp->im[ds->nx+1] + 40;
  else job->in_progress = 0;
  return 0;
}

int scan_job_scanf(int im, struct future_job *job)
{
  int  ci, ims, ime, i, k;
  b_track *bt = NULL;
  d_s *ds = NULL, *dsx = NULL, *dsy = NULL, *dsz = NULL;
  scan_param *sp = NULL;
  char buf[1024] = {0};
  un_s *un = NULL;
  float ay, dy, axl, dxl;

  if (job == NULL || job->oi == NULL || job->in_progress == 0 || job->more_data == NULL) return 0;
  if (im < job->imi)  return 0;

  ci = track_info->c_i;
  ds = job->op->dat[0];
  sp = (scan_param *)job->more_data;
  bt = track_info->bd[job->bead_nb];
  if (bt->opt == NULL)   return 0;
  dsx = bt->opt->dat[0];
  dsy = bt->opt->dat[1];
  dsz = bt->opt->dat[2];
  un = bt->opt->yu[bt->opt->c_yu];
  get_afine_param_from_unit(un, &ay, &dy);
  un = job->op->xu[job->op->c_xu];
  get_afine_param_from_unit(un, &axl, &dxl);
  dy /= dxl;

  ims = sp->im[ds->nx] - sp->im[0] + sp->dead;
  if (ds->nx>= ds->mx)   return 0;
  ime = sp->im[ds->nx + 1] - sp->im[0];

  for(i = ims, k = 0, ds->xd[ds->nx] = 0; i < ime; i++, k++)
    ds->xd[ds->nx] += dsz->yd[i];
  if (k) ds->xd[ds->nx] /= k;
  ds->xd[ds->nx] = ay + dy * ds->xd[ds->nx];
  ds->yd[ds->nx] = sp->fm[ds->nx];
  snprintf(buf,1024,"Scan %d %03d/%d Zmag %6.3f mm Fexp %6.3f pN <z> %6.3f microns",
	  n_scan,ds->nx,ds->mx,sp->zm[ds->nx],sp->fm[ds->nx],ds->xd[ds->nx]);


  ds->ny = ds->nx = ds->nx + 1;
 my_set_window_title(buf);
  job->op->need_to_refresh = 1;
  if (ds->nx < ds->mx) job->imi = sp->im[ds->nx+1] + 40;
  else
    {
      job->in_progress = 0;
      win_title_used = 0;
      n_scan++;
      last_im_who_asked_menu_update = im;
      Open_Pico_config_file();
      set_config_int("ZScan", "n_scan", n_scan);
      write_Pico_config_file();
      Close_Pico_config_file();
    }
  return 0;
}


int record_scan(void)
{
   int i, j, k, im, li;
   imreg *imr = NULL;
   O_i *oi = NULL;
   b_track *bt = NULL;
   pltreg *pr = NULL;
   O_p *op = NULL;
   d_s *ds = NULL;
   static int nf = 2048, nstep = 33,  dead = 5, nper = 32, maxnper = 8192;
   static float zstart = 9, zstep = -.125, zlow = 9, lambdazmag = 1, zmag0 = 6;
   char question[1024] = {0}, name[64] = {0};
   scan_param *sp = NULL;
   int n_maxnper;

   if(updating_menu_state != 0)
     {
       if (track_info->n_b == 0)
	 active_menu->flags |=  D_DISABLED;
       else
      {
       if (track_info->SDI_mode == 0)
   {
	 	   for (i = 0, k = 0; i < track_info->n_b; i++)
	     {
	       bt = track_info->bd[i];
	       k += (bt->calib_im != NULL) ? 1 : 0;
	     }
	   if (k) active_menu->flags &= ~D_DISABLED;
	   else active_menu->flags |=  D_DISABLED;
	 }
   else active_menu->flags &= ~D_DISABLED;
 }
       return D_O_K;
     }

   /*
   if(updating_menu_state != 0)
     {
       if (track_info->n_b == 0)
	 active_menu->flags |=  D_DISABLED;
       else active_menu->flags &= ~D_DISABLED;
       return D_O_K;
     }
   */
   if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)	return OFF;

   Open_Pico_config_file();
   n_scan = get_config_int("ZScan", "n_scan", n_scan);
   zstart = get_config_float("ZScan", "zstart", 9);
   zstep = get_config_float("ZScan", "zstep", -0.125);
   nstep = get_config_int("ZScan", "n_step", 9);
   nper = get_config_int("ZScan", "nper", 64);
   maxnper = get_config_int("ZScan", "maxnper", 8192);
   dead = get_config_int("ZScan", "dead", 32);
   zmag0 = get_config_float("ZScan", "zmag0", 7.5);
   lambdazmag = get_config_float("ZScan", "lambdazmag", 3);
   //z_cor = get_config_float("ZScan", "correction", 0.878);
   zlow = get_config_float("ZScan", "zmag_after", 8);
   Close_Pico_config_file();

   snprintf(question,1024,"Scan in  Z_{mag} present val %g angular position %g\n"
	   "starting at %%8f with step of %%8f nb of step %%8d\n"
	   "Minimun number of frames to record bead position at each step  %%8d\n"
	   "dead period (nb. of frames skipped during magnets translation) %%8d\n"
	   "Z_{mag0} (value below which integration time increases) %%8f\n"
	   "\\tau  = mper * 2^{(\\lambda Z_{mag0} - Z_{mag})} %%8f\n"
	   "Maximun number of frames to record bead position at each step  %%8d\n"
	   "zmag low %%8f\n"
	   ,prev_mag_rot,prev_zmag);



   i = win_scanf(question,&zstart,&zstep,&nstep,&nper,&dead,&zmag0,&lambdazmag,&maxnper,&zlow);

   if (i == WIN_CANCEL)	return OFF;

   Open_Pico_config_file();
   set_config_float("ZScan", "zstart", zstart);
   set_config_float("ZScan", "zstep", zstep);
   set_config_int("ZScan", "n_step", nstep);
   set_config_int("ZScan", "nper", nper);
   set_config_int("ZScan", "maxnper", maxnper);
   set_config_int("ZScan", "dead", dead);
   set_config_float("ZScan", "zmag0", zmag0);
   set_config_float("ZScan", "lambdazmag", lambdazmag);
   //set_config_float("ZScan", "correction", z_cor);
   set_config_float("ZScan", "zmag_after", zlow);
   write_Pico_config_file();
   Close_Pico_config_file();

   for (n_maxnper = 1; (2<<n_maxnper) < maxnper; n_maxnper++);

   sp = (scan_param*)calloc(1,sizeof(scan_param));
   if (sp == NULL)  win_printf_OK("Could no allocte scan parameters!");
   sp->im = (int*)calloc(2*nstep+1,sizeof(int));
   sp->zm = (float*)calloc(2*nstep+1,sizeof(float));
   sp->fm = (float*)calloc(2*nstep+1,sizeof(float));
  sp->is_rot = 0;
   if (sp->im == NULL || sp->zm == NULL || sp->fm == NULL)  win_printf_OK("Could no allocte scan parameters!");

   for (i = 0, nf = 0; i < 2*nstep ; i++)
     {
       j = i;
       j %= 2*nstep;
       j = (j < nstep) ? j : 2 * nstep -1 - j;
       j = (int)(lambdazmag * (zmag0 - zstart - j * zstep));
       j = (j <= n_maxnper) ? j : n_maxnper;
       nf += (j > 0) ?  nper<<j : nper;
       nf += dead;
     }

   sp->nf = nf;
   sp->nstep = nstep;
   sp->dead = dead;
   sp->nper = nper;

   sp->zstart = zstart;
   sp->zstep = zstep;
   sp->zlow = zlow;
   sp->z_cor = track_info->focus_cor;
   sp->lambdazmag = lambdazmag;
   sp->zmag0 = zmag0;
   snprintf(name,64,"Scan Zmag %d",n_scan);

   // We create a plot region to display bead position, scan  etc.
   pr = create_hidden_pltreg_with_op(&op, nf, nf, 0,name);
   if (pr == NULL)  win_printf_OK("Could not find or allocte plot region!");
   //op = pr->one_p;
   ds = op->dat[0];

   //win_printf("nf %d",nf);


   im = track_info->imi[track_info->c_i];
   im += 25;

   //we create one plot for each bead
   for (i = li = 0; i < track_info->n_b; i++)
     {
       bt = track_info->bd[i];
       if ((bt->calib_im == NULL) || (bt->not_lost <= 0) || (bt->in_image == 0))
	 continue;

       if (li)
	 {
	   op = create_and_attach_one_plot(pr, nf, nf, 0);
	   ds = op->dat[0];
	 }
       li++;
       bt->opt = op;
       ds->nx = ds->ny = 0;
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
		     "X coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_scan,i
		     ,track_info->cl,track_info->cw,nf
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,pico_sample
		     ,what_is_present_rot_value()
		     ,what_is_present_z_mag_value()
		     ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");
       if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
	 return win_printf_OK("I can't create plot !");
       ds->nx = ds->ny = 0;
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
		     "Y coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_scan,i
		     ,track_info->cl,track_info->cw,nf
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,pico_sample
		     ,what_is_present_rot_value()
		     ,what_is_present_z_mag_value()
		     ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");
       if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
	 return win_printf_OK("I can't create plot !");
       ds->nx = ds->ny = 0;
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
		     "Z coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_scan,i
		     ,track_info->cl,track_info->cw,nf
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,pico_sample
		     ,what_is_present_rot_value()
		     ,what_is_present_z_mag_value()
		     ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");

       uns_oi_2_op(oi, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
       create_attach_select_x_un_to_op(op, IS_SECOND, 0
				       ,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");

       op->op_idle_action = scan_op_idle_action; //xyz_scan_idle_action;
       op->op_post_display = scan_op_post_display;
       set_op_filename(op, "X(t)Y(t)Z(t)bd%dscan%d.gr",i,n_scan);
       set_plot_title(op, "Bead %d trajectory %d", i,n_scan);
       set_plot_x_title(op, "Time");
       set_plot_y_title(op, "Position");

       op->x_lo = (-nf/64); ///Pico_param.camera_param.camera_frequency_in_Hz;
       op->x_hi = (nf + nf/64); ///Pico_param.camera_param.camera_frequency_in_Hz;
       set_plot_x_fixed_range(op);
       op->user_ispare[ISPARE_BEAD_NB] = i;
       fill_next_available_job_spot(im, im+32, COLLECT_DATA, i, imr, oi, pr, op, (void*)sp, scan_job_xyz, 0);
       //win_printf("bd %d",i);
     }



   op = create_and_attach_one_plot(pr, nf, nf, 0);
   ds = op->dat[0];
   set_plot_title(op, "Dz and Zmag %d", n_scan);
   set_plot_file(op,"bddzr%03d.gr",n_scan);
   set_plot_x_title(op, "Real time");
   set_plot_y_title(op, "dZ and Zmag");

   set_ds_source(ds,"Bead tracking at %d Hz Acquistion %d \n "
		 "Magnet Z position l = %d, w = %d, nim = %d\n"
		 "objective %g, zoom factor %f, sample %s Rotation %g\n"
		 "Calibration from %s"
		 ,Pico_param.camera_param.camera_frequency_in_Hz,n_scan
		 ,track_info->cl,track_info->cw,nf
		 ,Pico_param.obj_param.objective_magnification
		 ,Pico_param.micro_param.zoom_factor
		 ,sample
		 ,what_is_present_rot_value()
		 ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");


   create_attach_select_x_un_to_op(op, IS_SECOND, 0
				       ,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");

   op->x_lo = (-nf/64); //*op->dx;
   op->x_hi = (nf + nf/64);//*op->dx;
   set_plot_x_fixed_range(op);
   ds->nx = ds->ny = 0;
   op->op_idle_action = scan_op_idle_action;
   op->op_post_display = scan_op_post_display;
   op->user_ispare[ISPARE_BEAD_NB] = 0;
   fill_next_available_job_spot(im, im+32, COLLECT_DATA, i, imr, oi, pr, op, NULL, scan_job_zmag, 0);

   for (i = 0, nf = 0; i < 2*nstep ; i++)
     {
       j = i;
       j %= 2*nstep;
       j = (j < nstep) ? j : 2 * nstep -1 - j;
       sp->zm[i] = zstart + (j * zstep);
       sp->fm[i] = zmag_to_force(0, sp->zm[i]);
       sp->im[i] = im + nf;
       j = (int)(lambdazmag * (zmag0 - zstart - j * zstep));
       j = (j <= n_maxnper) ? j : n_maxnper;
       nf += (j > 0) ?  nper<<j : nper;
       nf += dead;
     }
   sp->zm[i] = zstart;
   sp->im[i] = im + nf;




   for (i = 0; i < track_info->n_b; i++)
     {
       bt = track_info->bd[i];

       if ((bt->calib_im == NULL) || (bt->not_lost <= 0) || (bt->in_image == 0))
	 continue;
       op = create_and_attach_one_plot(pr, 2*nstep,  2*nstep, 0);
       ds = op->dat[0];
       ds->nx = ds->ny = 0;
       //set_dot_line(ds);
       set_plot_symb(ds, "\\pt5\\oc");
       set_plot_title(op, "Beads %d Zmag Scan %d, rot %f", i, n_scan,what_is_present_rot_value());
       set_plot_x_title(op,"Length (\\mu m)");
       set_plot_y_title(op,"Z_{mag} (mm)");
       set_plot_file(op,"scanzbd%01d%03d%s",i,n_scan,".gr");
       create_attach_select_x_un_to_op(op, IS_METER, 0, 1, -6, 0, "\\mu m");
       /*
       set_ds_source(ds,"Scan Z\nZ mag start = %g for %d frames scan by steps of %g"
		     " for %d steps \nrot = %g dead period %d\n"
		     "Bead  calbration %s \n"
		     ,zstart,nper,zstep,nstep,what_is_present_rot_value()
		     ,dead,bt->calib_im->filename);
       */
       op->op_idle_action = scan_op_idle_action;
       op->op_post_display = scan_op_post_display;
       op->user_ispare[ISPARE_BEAD_NB] = i;
       j = (int)(lambdazmag * (zmag0 - zstart));
       j = (j > 0) ?  nper<<j : nper;
       fill_next_available_job_spot(im, sp->im[1]+40, COLLECT_DATA, i, imr, oi, pr, op, (void*)sp, scan_job_scan,0);
     }



   //# ifdef ENCOURS

   for (i = 0; i < track_info->n_b; i++)
     {
       bt = track_info->bd[i];
       if ((bt->calib_im == NULL) || (bt->not_lost <= 0) || (bt->in_image == 0))
	 continue;

       op = create_and_attach_one_plot(pr, 2*nstep,  2*nstep, 0);
       ds = op->dat[0];
       ds->nx = ds->ny = 0;
       set_dot_line(ds);
       set_plot_symb(ds, "\\pt5\\oc");

       set_plot_title(op, "Beads %d Zmag Scan %d, rot %f", i, n_scan,what_is_present_rot_value());
       set_plot_x_title(op,"Length (\\mu m)");
       set_plot_y_title(op,"Estimated Force (pN)");
       set_plot_file(op,"scanfbd%01d%03d%s",i,n_scan,".gr");
       create_attach_select_x_un_to_op(op, IS_METER, 0, 1, -6, 0, "\\mu m");
       /*
       set_ds_source(ds,"Scan Z\nZ mag start = %g for %d frames scan by steps of %g"
		     " for %d steps \nrot = %g dead period %d\n"
		     "Bead  calbration %s \n"
		     ,zstart,nper,zstep,nstep,what_is_present_rot_value()
		     ,dead,bt->calib_im->filename);
       */
       op->op_idle_action = scan_op_idle_action;
       op->op_post_display = scan_op_post_display;
       op->user_ispare[ISPARE_BEAD_NB] = i;
       j = (int)(lambdazmag * (zmag0 - zstart));
       j = (j > 0) ?  nper<<j : nper;
       fill_next_available_job_spot(im, sp->im[1]+40, COLLECT_DATA, i, imr, oi, pr, op, (void*)sp, scan_job_scanf, 0);
     }

   //# endif




   for (i = 0, nf = 0; i < 2*nstep ; i++)
     {
       j = i;
       j %= 2*nstep;
       j = (j < nstep) ? j : 2 * nstep -1 - j;
       sp->zm[i] = zstart + (j * zstep);
       sp->fm[i] = zmag_to_force(0, sp->zm[i]);
       sp->im[i] = im + nf;
       k = fill_next_available_action(sp->im[i], MV_ZMAG_ABS, sp->zm[i]);
       if (k < 0)	win_printf_OK("Could not add pending action!");
       j = (int)(lambdazmag * (zmag0 - zstart - j * zstep));
       j = (j <= n_maxnper) ? j : n_maxnper;
       nf += (j > 0) ?  nper<<j : nper;
       nf += dead;
     }
   sp->zm[i] = zstart;
   sp->im[i] = im + nf;
   win_title_used = 1;
 return D_O_K;
}

# endif // OLD

int scan_record_job_zmag(int im, struct future_job *job)
{
  register int j, k;
  int min_pos, max_pos, page_n, i_page;
  d_s *ds = NULL;

  if (scan_r == NULL)  return 0;
  if (job == NULL || job->oi == NULL || job->in_progress == 0 || job->op->n_dat < 1) return 0;
  if (im < job->imi)  return 0;

  ds = job->op->dat[0];

  max_pos = j = scan_r->abs_pos;
  min_pos = scan_r->starting_in_page_index + (scan_r->starting_page * scan_r->page_size);

  for (k = ds->nx, j = min_pos + k; j < max_pos; k++, j++)
    {
      page_n = j/scan_r->page_size;
      i_page = j%scan_r->page_size;
      add_new_point_to_ds(ds, scan_r->imi[page_n][i_page], scan_r->zmag[page_n][i_page]);

    }
  job->op->need_to_refresh = 1;
  job->imi += 32;
  if (working_gen_record == NULL)
      job->in_progress = 0;

  return 0;
}



int scan_record_job_scan(int im, struct future_job *job)
{
  register int i, j, k;
  int  max_pos, page_n, i_page, np, nd;
  static float min = 0, max = 0, tmp, tmp2;
  b_record *br = NULL;
  d_s *ds = NULL;

  if (scan_r == NULL)  return 0;
  if (job == NULL || job->oi == NULL || job->in_progress == 0 || job->op->n_dat < 1) return 0;
  if (im < job->imi)  return 0;

  ds = job->op->dat[0];
  br = scan_r->b_r[job->bead_nb];

  max_pos = j = scan_r->abs_pos;

  np = sp->nstep;
  np += (sp->one_way) ? 0 : np;

  nd = (sp->c_step/2);
  if (sp->c_step%2 == 0) nd--;

  nd = (nd <= ds->mx) ? nd : ds->mx;
  ds->nx = ds->ny = nd;

  //for (i = (ds->nx) ? ds->nx - 1 : 0; i < nd; i++)
  for (i = 0; i < nd; i++)
    {
      ds->xd[i] = ds->yd[i] = 0;
      for (j = sp->grim[i], k = 0; (j < sp->grim[i] + sp->nper) && (j < max_pos); j++, k++)
	{
	  page_n = j/scan_r->page_size;
	  i_page = j%scan_r->page_size;
	  ds->yd[i] += scan_r->zmag[page_n][i_page];
	  ds->xd[i] += scan_r->z_cor * br->z[page_n][i_page];
	}
      if (k)
	{
	  ds->xd[i] /= k;
	  ds->yd[i] /= k;
	  if (i == 0)  	  min = max = ds->xd[i];
	  min = (ds->xd[i] < min) ? ds->xd[i] : min;
	  max = (ds->xd[i] > max) ? ds->xd[i] : max;
	}
      else if (i == nd-1)
	  ds->nx = ds->ny = nd-1;
      //if ((i == np - 1) && (k == sp->nper)) job->in_progress = 0;
    }

  tmp = (max - min)*0.05;
  tmp = (tmp < min_scale_z) ? min_scale_z : tmp;
  for (i = 0, tmp2 = min_scale_z;tmp > tmp2; i++)
    {
      if (i%3 == 0) tmp2 += tmp2;
      else if (i%3 == 1) tmp2 += tmp2;
      else tmp2 *= 2.5;
    }
  tmp = tmp2/2;
  min -=  tmp;
  max +=  tmp;

  job->op->x_lo = min;
  job->op->x_hi = max;
  set_plot_x_fixed_range(job->op);

  job->op->need_to_refresh = 1;

  job->imi += 32;
  if (working_gen_record == NULL)
    {
      job->in_progress = 0;
      win_title_used = 0;
    }
  return 0;
}

int scan_record_job_scan_f(int im, struct future_job *job)
{
  register int i, j, k;
  int   max_pos, page_n, i_page, np, nd;
  static float min = 0, max = 0, tmp, tmp2;
  b_record *br = NULL;
  d_s *ds = NULL;

  if (scan_r == NULL)  return 0;
  if (job == NULL || job->oi == NULL || job->in_progress == 0 || job->op->n_dat < 1) return 0;
  if (im < job->imi)  return 0;

  ds = job->op->dat[0];
  br = scan_r->b_r[job->bead_nb];

  max_pos = j = scan_r->abs_pos;
  //min_pos = scan_r->starting_in_page_index + (scan_r->starting_page * scan_r->page_size);

  np = sp->nstep;
  np += (sp->one_way) ? 0 : np;

  nd = (sp->c_step/2);
  if (sp->c_step%2 == 0) nd--;

  nd = (nd <= ds->mx) ? nd : ds->mx;
  ds->nx = ds->ny = nd;
  //for (i = (ds->nx) ? ds->nx - 1 : 0; i < nd; i++)
  for (i = 0; i < nd; i++)
    {
      ds->xd[i] = ds->yd[i] = 0;
      for (j = sp->grim[i], k = 0; (j < sp->grim[i] + sp->nper) && (j < max_pos); j++, k++)
	{
	  page_n = j/scan_r->page_size;
	  i_page = j%scan_r->page_size;
	  ds->yd[i] += scan_r->zmag[page_n][i_page];
	  ds->xd[i] += scan_r->z_cor * br->z[page_n][i_page];
	}
      if (k)
	{
	  ds->xd[i] /= k;
	  ds->yd[i] /= k;
	  if (i == 0)  	  min = max = ds->xd[i];
	  min = (ds->xd[i] < min) ? ds->xd[i] : min;
	  max = (ds->xd[i] > max) ? ds->xd[i] : max;
	}
      else if (i == nd-1)
	  ds->nx = ds->ny = nd-1;
      ds->yd[i] = zmag_to_force(0, ds->yd[i]);
      //if ((i == np - 1) && (k == sp->nper)) job->in_progress = 0;
    }

  tmp = (max - min)*0.05;
  tmp = (tmp < min_scale_z) ? min_scale_z : tmp;
  for (i = 0, tmp2 = min_scale_z;tmp > tmp2; i++)
    {
      if (i%3 == 0) tmp2 += tmp2;
      else if (i%3 == 1) tmp2 += tmp2;
      else tmp2 *= 2.5;
    }
  tmp = tmp2/2;
  min -=  tmp;
  max +=  tmp;
  job->op->x_lo = min;
  job->op->x_hi = max;
  set_plot_x_fixed_range(job->op);

  job->op->need_to_refresh = 1;
  job->imi += 32;
  if (working_gen_record == NULL)
    {
      job->in_progress = 0;
      win_title_used = 0;
    }
  return 0;
}




int scan_record_job_z(int im, struct future_job *job)
{
  int i, j, k;
  int  nf, page_n, i_page, max_pos, min_pos, fir;
  b_record *br = NULL;
  d_s *dsz = NULL, *dszf = NULL, *dszm = NULL;
  float min = 0, max = 0, minz = 0, maxz = 0, tmp, tmp2;

  if (job == NULL || job->oi == NULL || job->in_progress == 0 || job->op->n_dat < 3) return 0;
  if ((void*)sp != job->more_data) return 0;
  if (im < job->imi)  return 0;

  dsz = job->op->dat[0];
  dszf = job->op->dat[1];
  dszm = job->op->dat[2];
  br = scan_r->b_r[job->bead_nb];

  max_pos = j = scan_r->abs_pos;
  min_pos = scan_r->starting_in_page_index + (scan_r->starting_page * scan_r->page_size);

  nf = max_pos - min_pos;
  dsz->nx = dsz->ny =  dszm->nx = dszm->ny = 0;
  for (i = 0, j = min_pos; i < nf && j < max_pos; i++, j++)
    {
      page_n = j/scan_r->page_size;
      i_page = j%scan_r->page_size;
      add_new_point_to_ds(dsz, scan_r->imi[page_n][i_page], scan_r->z_cor * br->z[page_n][i_page]);
      add_new_point_to_ds(dszm, scan_r->imi[page_n][i_page], scan_r->zmag[page_n][i_page]);
      if (i == 0)
	{
	  min = max = dsz->yd[i];
	  minz = maxz = dszm->yd[i];
	}
      min = (dsz->yd[i] < min) ? dsz->yd[i] : min;
      minz = (dszm->yd[i] < minz) ? dszm->yd[i] : minz;
      max = (dsz->yd[i] > max) ? dsz->yd[i] : max;
      maxz = (dszm->yd[i] > maxz) ? dszm->yd[i] : maxz;
    }

  j = (int)(dsz->xd[0]);
  job->op->x_lo = 128*(j/128);
  j = 128+(int)(dsz->xd[nf-1]);
  job->op->x_hi = 128*(j/128);
  set_plot_x_fixed_range(job->op);

  fir = sp->fir;
  tmp = (max - min)*0.05;
  tmp = (tmp < min_scale_z) ? min_scale_z : tmp;
  for (i = 0, tmp2 = min_scale_z;tmp > tmp2; i++)
    {
      if (i%3 == 0) tmp2 += tmp2;
      else if (i%3 == 1) tmp2 += tmp2;
      else tmp2 *= 2.5;
    }
  tmp = tmp2/2;
  min -=  tmp;
  max +=  tmp;

  if (job->op->iopt2 & Y_LIM)  // fixed range
    {
      min = job->op->y_lo;
      max = job->op->y_hi;
    }


  tmp = (maxz - minz)*0.05;
  minz -=  6*tmp;
  maxz +=  tmp;


  if (minz > min && maxz < max)
    {
      job->op->yu[job->op->c_yu_p]->ax = 0;
      job->op->yu[job->op->c_yu_p]->dx = 1;
    }
  else
    {
      if ((maxz - minz) < min_scale_zmag)
	{
	  minz = (maxz + minz)/2;
	  maxz = minz + min_scale_zmag/2;
	  minz = minz - min_scale_zmag/2;
	}
      tmp = (max-min)/(maxz - minz);
      for (i = 0; i < nf; i++)
	{
	  dszm->yd[i] = min + (dszm->yd[i] - minz)*tmp;
	}
      if (tmp > 0)
	{
	  job->op->yu[job->op->c_yu_p]->ax = minz - min/tmp;
	  job->op->yu[job->op->c_yu_p]->dx = (float)1/tmp;
	}
    }
  job->op->y_lo = min;
  job->op->y_hi = max;
  set_plot_y_fixed_range(job->op);

  for (i = 0, j = 0, k = 0, dszf->yd[0] = dszf->xd[0] = 0; i < nf; i++)
    {
      if (k == 0) add_new_point_to_ds(dszf, 0, 0);
      dszf->yd[j] += dsz->yd[i];
      dszf->xd[j] += dsz->xd[i];
      k++;
      if (k >= fir)
	{
	  dszf->yd[j] /= fir;
	  dszf->xd[j] /= fir;
	  j++;
	  dszf->yd[j] = dszf->xd[j] = 0;
	  k = 0;
	}
    }
  dszf->nx = dszf->ny = j;
  job->op->need_to_refresh = 1;
  job->imi += 32;

  if (working_gen_record == NULL)
      job->in_progress = 0;
  return 0;
}


char *generate_scan_title(void)
{
  return scan_state_disp;
}



int scan_action(int im, int c_i, int aci, int init_to_zero)
{
  int k, i, page_n, i_page, np, abs_pos;
  static int suspended = 0;

  if (init_to_zero)
    {
      suspended = 0;
      return 0;
    }
  (void)aci;
  if (scan_r == NULL || sp == NULL)  return 0;
  np = sp->nstep;
  np += (sp->one_way) ? 0 : np;


  i = (sp->c_step/2) - 1;
  if (i >= np)
    {
      sp->c_step -= 2*np;
      sp->c_step = (sp->c_step) < 0 ? 0 : sp->c_step;
      sp->i_repeat++;
    }
  i = (sp->c_step/2) - 1;
  if (scan_state == SCAN_SUSPENDED)
    {
      snprintf(scan_state_disp,sizeof(scan_state_disp),"Vertical scan %d supended at point %d",n_scan,i);
      imr_TRACK_title_change_asked++;
      suspended = 1;
    }

  if (scan_state != SCAN_RUNNING)  return 0;
  abs_pos = (scan_r->abs_pos < 0) ? 0 : scan_r->abs_pos;

  page_n = abs_pos/scan_r->page_size;
  i_page = abs_pos%scan_r->page_size;

  scan_r->action_status[page_n][i_page] &= 0xff000000;
  scan_r->action_status[page_n][i_page] |= (0xffff&i<<8) + sp->c_step%2;
  if (sp->i_repeat >= sp->repeat) // i >= np
    {
      scan_r->action_status[page_n][i_page] &= ~DATA_AVERAGING;
      if (im < sp->im[np-1] +  sp->rec_n[np - 1] + 512)
	return 0;  // we are nearly finished
      else
	{
	  scan_r->action_status[page_n][i_page] &= ~DATA_AVERAGING;
	  snprintf(scan_state_disp,sizeof(scan_state_disp),"Verical scan %d finished",n_scan);
	  imr_TRACK_title_change_asked++;
	  working_gen_record = NULL;
	  if (scan_r != NULL)	      scan_r->n_record++;
	  n_scan++;
	  Open_Pico_config_file();
	  set_config_int("ZScan", "n_scan", n_scan);
	  write_Pico_config_file();
	  Close_Pico_config_file();
	  return 0;  // we have finished
	}
    }
  if (sp->c_step == 0)   // we move zmag
    {
      k = fill_next_available_action(im+1, MV_ROT_ABS, sp->rotation);
      if (k < 0)	win_printf_OK("Could not add pending action!");
      sp->c_step++;
    }
  if (sp->c_step == 1)   // we wait for rotation to stop moving and then start zmag
    {
      if (fabs(track_info->rot_mag[c_i] - sp->rotation) > 0.005)
	return 0;  // we are not yet arrived
      k = fill_next_available_action(im+1, MV_ZMAG_ABS, sp->zstart);
      if (k < 0)	win_printf_OK("Could not add pending action!");
      sp->c_step++;
    }


  if (sp->do_rot && sp->is_rot && sp->rot != NULL)
  {
    if (fabs(track_info->zmag[c_i] - sp->zm[i]) > 0.005)
    {
        return 0;
    }
    k = fill_next_available_action(im+1, MV_ROT_ABS, sp->rot[i]);
    if (k < 0)	win_printf_OK("Could not add pending action!");
    sp->do_rot = 0;
  }



  if (sp->c_step >= 2 && sp->c_step%2 == 0)
    { // we wait for zmag to stop moving and then start acquisition
      snprintf(scan_state_disp,sizeof(scan_state_disp),"Scan %d going to point %d",n_scan,i);
      imr_TRACK_title_change_asked++;
      if (fabs(track_info->zmag[c_i] - sp->zm[i]) > 0.005)
      {
          return 0;


      }


  if (sp->do_rot && sp->is_rot && sp->rot != NULL)
  {
  if (fabs(track_info->rot_mag[c_i] - sp->rot[i]) > 0.01)
  {
    return 0;


  }
}
   // we are not yet arrived

      if (sp->adj_focus)
	{
	  k = fill_next_available_action(im+1, MV_OBJ_ABS, sp->zo[i]);
	  if (k < 0)	win_printf_OK("Could not add pending action!");
	}
      sp->im[i] = im  + sp->setling; //scan_r->abs_pos + sp->setling;
      sp->grim[i] = scan_r->abs_pos + sp->setling;
      sp->c_step++;
    }
  if (sp->c_step >= 2 && sp->c_step%2 == 1)
    {
// we wait for the end of acquisition and start zmag
      if (suspended == 1) // we restart point averaging
	{
	  sp->im[i] = im  + sp->setling; //scan_r->abs_pos + sp->setling;
	  sp->grim[i] = scan_r->abs_pos + sp->setling;
	  suspended = 0;
	}
      if (im >= sp->im[i])
	{
	  scan_r->action_status[page_n][i_page] |= DATA_AVERAGING;
	  snprintf(scan_state_disp,sizeof(scan_state_disp),"Scan %d averaging point %d/%d (%3.1f%%) Zmag %5.3f",
		  n_scan,i,np,(float)(100*(im - sp->im[i]))/sp->rec_n[i],sp->zm[i]);
	  imr_TRACK_title_change_asked++;
	}

      if (im < sp->im[i] + sp->rec_n[i])   { return 0; } // we are not yet done
      scan_r->action_status[page_n][i_page] &= ~DATA_AVERAGING;
      if (i+1 < np)
	{
	  k = fill_next_available_action(im+1, MV_ZMAG_ABS, sp->zm[i+1]);
	  if (k < 0)	win_printf_OK("Could not add pending action!");

    if (sp->is_rot && sp->rot != NULL) sp->do_rot = 1;

	}
      else if (sp->i_repeat >= sp->repeat)
	{
	  k = fill_next_available_action(im+1, MV_ZMAG_ABS, sp->zlow);
	  if (k < 0)	win_printf_OK("Could not add pending action!");
	}


      sp->c_step++;
    }

  return 0;
}

int record_scan_rot_noise_force(void)
{
  int i, j, k, im,  tmpi, li = 0;
   imreg *imr = NULL;
   O_i *oi = NULL;
   b_track *bt = NULL;
   pltreg *pr = NULL;
   O_p *op = NULL;
   d_s *ds = NULL;
   static int nf = 2048, nstep = 33,  dead = 5, nper = 32, maxnper = 8192, setling = 16, fir = 16, adj_focus = 0, n_repeat = 1;
   static float rotation_vel = 0.25,zstart = 9, zstep = -.125, zlow = 9, lambdazmag = 1, zmag0 = 6, movie_rw = 1.0, movie_rh = 1.0;
   char question[4096] = {0}, name[64] = {0}, basename[512] = {0}, fullfilename[512] = {0};
   int nw;
   float rotation, z_tmp, tmp;
   static int with_radial_profiles = 0, n_maxnper, one_way = 0, streaming = 1, file_selection = 0, ortho_profiles = 0, movie_track = 0, rotation_nb = 10;
   static int special_saving = 0, rec_xy_prof = 0, rec_xy_diff_prof = 0, rec_small_image = 0, xyz_error = 1, xy_tracking_type;

   if(updating_menu_state != 0)
     {
       if (working_gen_record != NULL || track_info->n_b == 0)
	 active_menu->flags |=  D_DISABLED;
       else
	 {
           if (track_info->SDI_mode == 0)
	     {
	       for (i = 0, k = 0; i < track_info->n_b; i++)
		 {
		   bt = track_info->bd[i];
		   k += (bt->calib_im != NULL) ? 1 : 0;
		 }

	   if (k) active_menu->flags &= ~D_DISABLED;
	   else active_menu->flags |=  D_DISABLED;
   }
    else active_menu->flags &= ~D_DISABLED;
	 }
       return D_O_K;
     }

   if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)	return OFF;

   Open_Pico_config_file();
   n_scan = get_config_int("ZScanRotNoise", "n_scan", n_scan);
   n_repeat = get_config_int("ZScanRotNoise", "n_repeat", n_repeat);
   zstart = get_config_float("ZScanRotNoise", "zstart", -0.5);
   zstep = get_config_float("ZScanRotNoise", "zstep", -0.125);
   nstep = get_config_int("ZScanRotNoise", "n_step", 9);
   nper = get_config_int("ZScanRotNoise", "nper", 64);
   one_way = get_config_int("ZScanRotNoise", "one_way", 0);
   maxnper = get_config_int("ZScanRotNoise", "maxnper", 8192);
   setling = get_config_int("ZScanRotNoise", "setling", 16);
   zmag0 = get_config_float("ZScanRotNoise", "zmag0", -0.9);
   lambdazmag = get_config_float("ZScanRotNoise", "lambdazmag", 4);
   rotation_vel = get_config_float("ZScanRotNoise", "rotation_vel", 0.25);
   rotation_nb = get_config_int("ZScanRotNoise", "rotation_nb",10);
   //z_cor = get_config_float("ZScan", "correction", 0.878);
   zlow = get_config_float("ZScanRotNoise", "zmag_after", -1);
   Close_Pico_config_file();

   n_scan = find_trk_index_not_used("Zscan_", n_scan);

   rotation = what_is_present_rot_value();

   if (current_write_open_path_picofile != NULL)
     {
       snprintf(fullfilename,sizeof(fullfilename),"%s\\Zscanrotnoise_%d.trk",current_write_open_path_picofile,n_scan);
       backslash_to_slash(fullfilename);
       file_selection = 0;
     }
   else
     {
       snprintf(fullfilename,sizeof(fullfilename),"Zscanrotnoise_%d.trk",n_scan);
       backslash_to_slash(fullfilename);
       file_selection = 1;
     }



   snprintf(question,sizeof(question),"Scan Rot Noise in  Z_{mag} \n"
     "Rotation to begin with %%8f\n"
	   "Number of turn at each step %%8d\n"
     "Rotation velocity %%8f\n"
	   "starting Zmag at %%8f with step of %%8f nb of step %%8d\n"
	   "one way and back %%R or only one way %%r\n"
	    "Repeat this %%4d times\n"
	    "Adjust focus according to estimated force and %5.2f \\mu m mol NO%%R Yes%%r\n"
	   "Minimun number of frames to record bead position at each step  %%8d\n"
	   "setling period (nb. of frames skipped after magnets translation) %%8d\n"
	   "Z_{mag0} (value below which integration time increases) %%8f\n"
	   "\\tau  = mper * 2^{(\\lambda Z_{mag0} - Z_{mag})} %%8f\n"
	   "Maximun number of frames to record bead position at each step  %%8d\n"
	   "zmag finish %%8f averaging Z over %%8d\n"
	    "\\fbox{Track will be saved in %s\n"
	    "Do you want to select a new file location %%R No %%r Yes\n"
	    "Click here to select special data saving options %%b\n}"
	   ,Pico_param.dna_molecule.dsDNA_molecule_extension,fullfilename);

   i = win_scanf(question,&rotation,&rotation_nb,&rotation_vel,&zstart,&zstep,&nstep,&one_way,&n_repeat,&adj_focus,&nper,
		 &setling, &zmag0,&lambdazmag,&maxnper,&zlow,&fir,&file_selection,&special_saving);

   if (i == WIN_CANCEL)	return OFF;

   if (special_saving)
     {
       snprintf(question,sizeof(question),"You can select here special saving options (increasing trk file size)\n"
		"Do you want to record bead profiles %%b\n"
		"Do you want to record bead orthoradial profiles %%b\n"
		"Do you want real time saving %%b\n"
		"Do you want to record bead xyz tracking error %%b\n"
		"Do you want to record bead xy tracking profiles %%b\n"
		"Do you want to record bead xy differential tracking profiles %%b\n"
		"Do you want to record bead image %%b (generates big trk file!)\n"
		"In this case define :\n"
		" \\oc  the ratio of bead image width with the cross arm %%6f\n"
		" \\oc  the ratio of bead image heigh with the cross arm %%6f\n"
		" \\oc  does the image center track the bead %%b\n");
       i = win_scanf(question,&with_radial_profiles,&ortho_profiles, &streaming, &xyz_error, &rec_xy_prof,
		     &rec_xy_diff_prof, &rec_small_image, &movie_rw, &movie_rh, &movie_track);
       if (i == WIN_CANCEL)	 return OFF;
     }


   z_tmp = zstart + (nstep - 1) * zstep;
   if (z_tmp > absolute_maximum_zmag)
     {
       i = win_printf("You have specified zmag start = %g, with %d steps of %g\n"
		     "with these values your final zmag %g exceeds the safe limit %g mm\n"
		     "You might have made a mistake. Are you sure you want to persue ?\n"
		      ,zstart ,nstep ,zstep, z_tmp,absolute_maximum_zmag );
	 if (i == WIN_CANCEL)	return OFF;
     }





   Open_Pico_config_file();
   set_config_float("ZScanRotNoise", "zstart", zstart);
   set_config_int("ZScanRotNoise", "n_repeat", n_repeat);
   set_config_float("ZScanRotNoise", "zstep", zstep);
   set_config_int("ZScanRotNoise", "n_step", nstep);
   set_config_int("ZScanRotNoise", "nper", nper);
   set_config_int("ZScanRotNoise", "one_way", one_way);
   set_config_int("ZScanRotNoise", "maxnper", maxnper);
   set_config_int("ZScanRotNoise", "setling", setling);
   set_config_float("ZScanRotNoise", "zmag0", zmag0);
   set_config_float("ZScanRotNoise", "lambdazmag", lambdazmag);
   //set_config_float("ZScan", "correction", z_cor);
   set_config_float("ZScanRotNoise", "zmag_after", zlow);

   set_config_float("ZScanRotNoise", "rotation_vel", rotation_vel);
   set_config_int("ZScanRotNoise", "rotation_nb",rotation_nb);
   write_Pico_config_file();
   Close_Pico_config_file();

   dead = (int)((Pico_param.camera_param.camera_frequency_in_Hz*fabs(zstep))/get_motors_speed());
   dead *= 2;

   nw = (one_way) ? 1 : 2;
   for (n_maxnper = 0; (nper*(1<<n_maxnper)) < maxnper; n_maxnper++);


   for (i = 0, nf = 0; i < nw*nstep ; i++)
     {
       j = i;
       j = (j < nstep) ? j : 2 * nstep -1 - j;
       j = (int)(lambdazmag * (zmag0 - zstart - j * zstep));
       j = (j <= n_maxnper) ? j : n_maxnper;
       tmpi = (j > 0) ?  nper<<j : nper;
       tmpi = (tmpi < maxnper) ? tmpi : maxnper;

       nf += tmpi;
       nf += dead;
       nf += (int) (rotation_nb * Pico_param.camera_param.camera_frequency_in_Hz / rotation_vel) ;
     }
   nf += 1024;


   i = win_printf("ZScanRotNoise scan curve will last %6.2f seconds (%d frames)\n"
		  "Pressing Cancel will abord acquisition",
		  (float)nf/Pico_param.camera_param.camera_frequency_in_Hz,nf);
   if (i == WIN_CANCEL) return D_O_K;


   sp = (scan_param*)calloc(1,sizeof(scan_param));
   if (sp == NULL)  win_printf_OK("Could no allocte scan parameters!");
   sp->im = (int*)calloc(nw*nstep+1,sizeof(int));
   sp->grim = (int*)calloc(nw*nstep+1,sizeof(int));
   sp->rec_n = (int*)calloc(nw*nstep+1,sizeof(int));
   sp->zm = (float*)calloc(nw*nstep+1,sizeof(float));
   sp->fm = (float*)calloc(nw*nstep+1,sizeof(float));
   sp->zo = (float*)calloc(nw*nstep+1,sizeof(float));
   sp->rot = (float*)calloc(nw*nstep+1,sizeof(float));
   sp->is_rot = 1;
   sp->do_rot = 0;
   if (sp->im == NULL || sp->zm == NULL || sp->fm == NULL || sp->fm == NULL || sp->rot == NULL)
     win_printf_OK("Could no allocte scan parameters!");

   im = track_info->imi[track_info->c_i];
   tmp = Pico_param.dna_molecule.dsDNA_molecule_extension
     * extension_versus_alpha((zmag_to_force(0,zstart) * 50)/ (2.98 * 1.38));
   tmp /= track_info->focus_cor;

   for (i = 0, nf = 0; i < nw*nstep ; i++)
     {
       j = i;
       j = (j < nstep) ? j : 2 * nstep -1 - j;
       sp->zm[i] = zstart + (j * zstep);
       sp->fm[i] = zmag_to_force(0, sp->zm[i]);
       sp->im[i] = im + nf;
       sp->zo[i] = prev_focus - tmp + (Pico_param.dna_molecule.dsDNA_molecule_extension
	       * extension_versus_alpha((sp->fm[i] * 50)/ (2.98 * 1.38)))/track_info->focus_cor;
       sp->rot[i] = rotation + j*rotation_nb;
       j = (int)(lambdazmag * (zmag0 - zstart - j * zstep));
       j = (j <= n_maxnper) ? j : n_maxnper;
       sp->rec_n[i] = (j > 0) ?  nper<<j : nper;
       sp->rec_n[i] = (sp->rec_n[i] < maxnper) ? sp->rec_n[i] : maxnper;
       nf += (int) (rotation_nb * Pico_param.camera_param.camera_frequency_in_Hz / rotation_vel) ;
       nf += sp->rec_n[i];
       nf += dead;
     }
   sp->zm[i] = (one_way) ? (zstart + (nstep * zstep)) : zstart;
   sp->fm[i] = zmag_to_force(0, sp->zm[i]);
   sp->im[i] = im + nf;
   sp->rec_n[i] = nper;
   nf += 1024;
   nf *= n_repeat;
   sp->nf = nf;
   sp->nstep = nstep;
   sp->dead = dead;
   sp->nper = nper;
   sp->setling = setling;
   sp->one_way = one_way;
   sp->c_step = 0;
   sp->fir = fir;
   sp->repeat = n_repeat;
   sp->i_repeat = 0;
   sp->rotation = rotation;

   sp->zstart = zstart;
   sp->zstep = zstep;
   sp->zlow = zlow;
   sp->z_cor = track_info->focus_cor;
   sp->lambdazmag = lambdazmag;
   sp->zmag0 = zmag0;
   sp->adj_focus = adj_focus;
   snprintf(name,64,"Scan Zmag %d",n_scan);

   // We create a plot region to display bead position, scan  etc.
   pr = create_hidden_pltreg_with_op(&op, nf, nf, 0,name);
   if (pr == NULL)  win_printf_OK("Could not find or allocte plot region!");
   ds = op->dat[0];
   //ds->xd[1] = ds->yd[1] = 0.1;
   im = track_info->imi[track_info->c_i];
   im += 25;

   sp->fir = fir;

   im = track_info->imi[track_info->c_i];
   xy_tracking_type = (rec_xy_prof) ? XY_BEAD_PROFILE_RECORDED : 0;
   xy_tracking_type |= (rec_xy_diff_prof) ? XY_BEAD_DIFF_PROFILE_RECORDED : 0;
   xy_tracking_type |= (rec_small_image) ? RECORD_BEAD_IMAGE : 0;
   xy_tracking_type |= (xyz_error) ? XYZ_ERROR_RECORDED : 0;

   scan_r = create_gen_record(track_info, 1, with_radial_profiles,0,1,xy_tracking_type, movie_rw, movie_rh, movie_track);
   duplicate_Pico_parameter(&Pico_param, &(scan_r->Pico_param_record));
   if (extract_file_name(scan_r->filename, sizeof(scan_r->filename), fullfilename) == NULL)
     snprintf(scan_r->filename,sizeof(scan_r->filename), "Zscan_%d.trk",n_scan);
   scan_r->record_action = scan_action;
   scan_action(0,0,0,1);
   scan_r->data_type |= SCAN_ZMAG;


   if (streaming)
     {
       if (current_write_open_path_picofile != NULL && file_selection == 0)
	 {
	   strncpy(scan_r->path,current_write_open_path_picofile,sizeof(scan_r->path));
	   write_record_file_header(scan_r, oi_TRACK);
	   scan_r->real_time_saving = 1;
	 }
       else if (do_save_track(scan_r))
	 win_printf("Pb writing tracking header");
     }


   if (current_write_open_path_picofile != NULL)
     {
       dump_to_html_log_file_with_date_and_time(current_write_open_picofile,
						"<h2>Scan Zmag curve recording</h2>"
						"<br>in file %s in directory %s"
						"<br>zmag start %g, %d sets of %g lasting %d frames after %d frames to settle <br>"
						"<br>%d way(s)"
						"Zmag0 %g lambdazmag %g maxnper %d<br>"
						"Rotation %g<br>\n"
						,backslash_to_slash(scan_r->filename),backslash_to_slash(scan_r->path)
						,zstart,nstep,zstep,nper,dead,(one_way) ? 1 : 2,zmag0,lambdazmag
						,maxnper,rotation);
    }


   li = grab_basename_and_index(scan_r->filename, strlen(scan_r->filename)+1, basename, 256);
   n_scan = (li >= 0) ? li : n_scan;
   scan_r->n_rec = n_scan;

   scan_state = SCAN_RUNNING;
   //we create one plot for each bead
   if (attach_g_record_to_pltreg(pr, scan_r))
     win_printf("Could not attach grecord to plot region!");


   for (i = li = 0; i < track_info->n_b; i++)
     {
       bt = track_info->bd[i];
       if ((bt->calib_im == NULL) || (bt->not_lost <= 0) || (bt->in_image == 0))
	 continue;
       if (li)
	 {
	   op = create_and_attach_one_plot(pr, nf, nf, 0);
	   ds = op->dat[0];
	 }
       li++;
       bt->opt = op;
       ds->nx = ds->ny = 0;
       //ds->xd[1] = ds->yd[1] = 0.1;
       set_dot_line(ds);
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
		     "Z coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_scan,i
		     ,track_info->cl,track_info->cw,nf
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,pico_sample
		     ,rotation
		     ,zstart
		     ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");
       if ((ds = create_and_attach_one_ds(op, nf/fir, nf/fir, 0)) == NULL)
	 return win_printf_OK("I can't create plot !");
       ds->nx = ds->ny = 0;
       //ds->xd[1] = ds->yd[1] = 0.1;
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
		     "filtred Z over %d coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_scan,i
		     ,fir,track_info->cl,track_info->cw,nf
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,pico_sample
		     ,rotation
		     ,zstart
		     ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");

       if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
	 return win_printf_OK("I can't create plot !");
       ds->nx = ds->ny = 0;
       //ds->xd[1] = ds->yd[1] = 0.1;
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
		     "Zmag l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Zmagnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_scan,i
		     ,track_info->cl,track_info->cw,nf
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,pico_sample
		     ,rotation
		     ,zstart
		     ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");


       create_attach_select_y_un_to_op(op, 0, 0 ,(float)1, 0, 0, "no_name");
       set_plot_y_prime_title(op, "Z Magnets (mm)");

       op->c_yu_p = op->c_yu;
       //       uns_oi_2_op(oi, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
       create_attach_select_y_un_to_op(op, IS_METER, 0 ,(float)1, -6, 0, "\\mu m");
       create_attach_select_x_un_to_op(op, IS_SECOND, 0
				       ,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");

       op->op_idle_action = scan_op_idle_action; //xyz_hat_idle_action;
       op->op_post_display = scan_op_post_display;
       set_op_filename(op, "Z(t)bd%dZscan%d.gr",i,n_scan);
       set_plot_title(op, "Bead %d trajectory %d rotation %g", i,n_scan,rotation);
       set_plot_x_title(op, "Time");
       set_plot_y_title(op, "Z position");
       op->user_ispare[ISPARE_BEAD_NB] = i;
       op->x_lo = im + (-nf/64);
       op->x_hi = im + (nf + nf/64);
       set_plot_x_fixed_range(op);
       fill_next_available_job_spot(im, im+32, COLLECT_DATA, i, imr, oi, pr, op, (void*)sp, scan_record_job_z, 0);
       //win_printf("bd %d",i);
     }



   op = create_and_attach_one_plot(pr, nf, nf, 0);
   ds = op->dat[0];
   //ds->xd[1] = ds->yd[1] = 0.1;
   set_plot_title(op, "Zscan %d at rotation %g", n_scan,rotation);
   set_plot_file(op,"bddzzmag%03d.gr",n_scan);
   set_plot_x_title(op, "Real time");
   set_plot_y_title(op, "Zmag");

   set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d \n "
		 "Magnet zmag position l = %d, w = %d, nim = %d\n"
		 "objective %g, zoom factor %f, sample %s Rotation %g\n"
		 "Zmag = %g Calibration from %s"
		 ,Pico_param.camera_param.camera_frequency_in_Hz, n_scan
		 ,track_info->cl, track_info->cw, nf
		 ,Pico_param.obj_param.objective_magnification
		 ,Pico_param.micro_param.zoom_factor
		 ,sample
		 ,rotation,zstart
		 ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");

   create_attach_select_x_un_to_op(op, IS_SECOND, 0
				       ,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");
   /*
   op->x_lo = im - (nf/64);
   op->x_hi = im + nf + (nf/64);
   set_plot_x_fixed_range(op);
   */
   ds->nx = ds->ny = 0;
   op->op_idle_action = scan_op_idle_action;
   op->op_post_display = scan_op_post_display;
   fill_next_available_job_spot(im, im+32, COLLECT_DATA, i, imr, oi, pr, op, NULL, scan_record_job_zmag, 0);
   op->user_ispare[ISPARE_BEAD_NB] = 0;
   for (i = 0; i < track_info->n_b; i++)
     {
       bt = track_info->bd[i];
       if ((bt->calib_im == NULL) || (bt->not_lost <= 0) || (bt->in_image == 0))
	 continue;

       op = create_and_attach_one_plot(pr, nw*nstep,  nw*nstep, 0);
       ds = op->dat[0];
       //ds->xd[1] = ds->yd[1] = 0.1;
       ds->nx = ds->ny = 0;
       //set_dot_line(ds);
       set_plot_symb(ds, "\\pt5\\oc");
       set_plot_title(op, "\\stack{{Beads %d rotation %g Scan %d}{zmag %g + %d x %g}}", i, rotation, n_scan,zstart,nstep,zstep);
       set_plot_x_title(op,"Length (\\mu m)");
       set_plot_y_title(op,"Z mag ");
       set_plot_file(op,"scanzbd%01d%03d%s",i,n_scan,".gr");
       create_attach_select_x_un_to_op(op, IS_METER, 0, 1, -6, 0, "\\mu m");

       set_ds_source(ds,"Scan Z\nRot %g zscan start = %g for %d frames scan by steps of %g"
		     " for %d steps \ndead period %d\n"
		     "Bead  calbration %s \n"
		     ,rotation,zstart,nper,zstep,nstep
		     ,dead,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");

       op->op_idle_action = scan_op_idle_action;
       op->op_post_display = scan_op_post_display;
       op->user_ispare[ISPARE_BEAD_NB] = i;
       j =  nper;
       // there is a bug in scan_job_scan
       fill_next_available_job_spot(im, sp->im[1] + 40, COLLECT_DATA, i, imr, oi, pr, op, (void*)sp, scan_record_job_scan, 0);
       last_bead = i;
     }

   for (i = 0; i < track_info->n_b; i++)
     {
       bt = track_info->bd[i];
       if ((bt->calib_im == NULL) || (bt->not_lost <= 0) || (bt->in_image == 0))
	 continue;

       op = create_and_attach_one_plot(pr, nw*nstep,  nw*nstep, 0);
       ds = op->dat[0];
       //ds->xd[1] = ds->yd[1] = 0.1;
       ds->nx = ds->ny = 0;
       //set_dot_line(ds);
       set_plot_symb(ds, "\\pt5\\oc");
       set_plot_title(op, "\\stack{{Beads %d rotation %g Scan %d}{zmag %g + %d x %g}}", i, rotation, n_scan,zstart,nstep,zstep);
       set_plot_x_title(op,"Length (\\mu m)");
       set_plot_y_title(op,"Estimated force (pN)");
       set_plot_file(op,"scanzfbd%01d%03d%s",i,n_scan,".gr");
       create_attach_select_x_un_to_op(op, IS_METER, 0, 1, -6, 0, "\\mu m");
       create_attach_select_y_un_to_op(op, IS_NEWTON, 0, 1, -12, 0, "pN");

       set_ds_source(ds,"Scan Z\nRot %g zscan start = %g for %d frames scan by steps of %g"
		     " for %d steps \ndead period %d\n"
		     "Bead  calbration %s \n"
		     ,rotation,zstart,nper,zstep,nstep
		     ,dead,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");

       op->op_idle_action = scan_op_idle_action;
       op->op_post_display = scan_op_post_display;
       op->user_ispare[ISPARE_BEAD_NB] = i;
       j =  nper;
       // there is a bug in scan_job_scan
       fill_next_available_job_spot(im, sp->im[1] + 40, COLLECT_DATA, i, imr, oi, pr, op, (void*)sp, scan_record_job_scan_f, 0);
       last_bead = i;
     }

   im = track_info->imi[track_info->c_i];
   working_gen_record = scan_r;
   scan_r->last_starting_pos = im;
   win_title_used = 1;
   if (imr_and_pr) switch_project_to_this_pltreg(pr);
   generate_imr_TRACK_title = generate_scan_title;
 return D_O_K;
}







int record_scan_record(void)
{
  int i, j, k, im,  tmpi, li = 0;
   imreg *imr = NULL;
   O_i *oi = NULL;
   b_track *bt = NULL;
   pltreg *pr = NULL;
   O_p *op = NULL;
   d_s *ds = NULL;
   static int nf = 2048, nstep = 33,  dead = 5, nper = 32, maxnper = 8192, setling = 16, fir = 16, adj_focus = 0, n_repeat = 1;
   static float zstart = 9, zstep = -.125, zlow = 9, lambdazmag = 1, zmag0 = 6, movie_rw = 1.0, movie_rh = 1.0;
   char question[4096] = {0}, name[64] = {0}, basename[512] = {0}, fullfilename[512] = {0};
   int nw;
   float rotation, z_tmp, tmp;
   static int with_radial_profiles = 0, n_maxnper, one_way = 0, streaming = 1, file_selection = 0, ortho_profiles = 0, movie_track = 0;
   static int special_saving = 0, rec_xy_prof = 0, rec_xy_diff_prof = 0, rec_small_image = 0, xyz_error = 1, xy_tracking_type;

   if(updating_menu_state != 0)
     {
       if (working_gen_record != NULL || track_info->n_b == 0)
	 active_menu->flags |=  D_DISABLED;
       else
	 {
           if (track_info->SDI_mode == 0)
	     {
	       for (i = 0, k = 0; i < track_info->n_b; i++)
		 {
		   bt = track_info->bd[i];
		   k += (bt->calib_im != NULL) ? 1 : 0;
		 }

	   if (k) active_menu->flags &= ~D_DISABLED;
	   else active_menu->flags |=  D_DISABLED;
   }
    else active_menu->flags &= ~D_DISABLED;
	 }
       return D_O_K;
     }

   if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)	return OFF;

   Open_Pico_config_file();
   n_scan = get_config_int("ZScan", "n_scan", n_scan);
   n_repeat = get_config_int("ZScan", "n_repeat", n_repeat);
   zstart = get_config_float("ZScan", "zstart", -0.5);
   zstep = get_config_float("ZScan", "zstep", -0.125);
   nstep = get_config_int("ZScan", "n_step", 9);
   nper = get_config_int("ZScan", "nper", 64);
   one_way = get_config_int("ZScan", "one_way", 0);
   maxnper = get_config_int("ZScan", "maxnper", 8192);
   setling = get_config_int("ZScan", "setling", 16);
   zmag0 = get_config_float("ZScan", "zmag0", -0.9);
   lambdazmag = get_config_float("ZScan", "lambdazmag", 4);
   //z_cor = get_config_float("ZScan", "correction", 0.878);
   zlow = get_config_float("ZScan", "zmag_after", -1);
   Close_Pico_config_file();

   n_scan = find_trk_index_not_used("Zscan_", n_scan);

   rotation = what_is_present_rot_value();

   if (current_write_open_path_picofile != NULL)
     {
       snprintf(fullfilename,sizeof(fullfilename),"%s\\Zscan_%d.trk",current_write_open_path_picofile,n_scan);
       backslash_to_slash(fullfilename);
       file_selection = 0;
     }
   else
     {
       snprintf(fullfilename,sizeof(fullfilename),"Zscan_%d.trk",n_scan);
       backslash_to_slash(fullfilename);
       file_selection = 1;
     }



   snprintf(question,sizeof(question),"Scan in  Z_{mag} present val %g angular position %g\n"
	   "Do scan at rotation %%8f\n"
	   "starting Zmag at %%8f with step of %%8f nb of step %%8d\n"
	   "one way and back %%R or only one way %%r\n"
	    "Repeat this %%4d times\n"
	    "Adjust focus according to estimated force and %5.2f \\mu m mol NO%%R Yes%%r\n"
	   "Minimun number of frames to record bead position at each step  %%8d\n"
	   "setling period (nb. of frames skipped after magnets translation) %%8d\n"
	   "Z_{mag0} (value below which integration time increases) %%8f\n"
	   "\\tau  = mper * 2^{(\\lambda Z_{mag0} - Z_{mag})} %%8f\n"
	   "Maximun number of frames to record bead position at each step  %%8d\n"
	   "zmag finish %%8f averaging Z over %%8d\n"
	    "\\fbox{Track will be saved in %s\n"
	    "Do you want to select a new file location %%R No %%r Yes\n"
	    "Click here to select special data saving options %%b\n}"
	   ,prev_mag_rot,prev_zmag,Pico_param.dna_molecule.dsDNA_molecule_extension,fullfilename);

   i = win_scanf(question,&rotation,&zstart,&zstep,&nstep,&one_way,&n_repeat,&adj_focus,&nper,
		 &setling, &zmag0,&lambdazmag,&maxnper,&zlow,&fir,&file_selection,&special_saving);

   if (i == WIN_CANCEL)	return OFF;

   if (special_saving)
     {
       snprintf(question,sizeof(question),"You can select here special saving options (increasing trk file size)\n"
		"Do you want to record bead profiles %%b\n"
		"Do you want to record bead orthoradial profiles %%b\n"
		"Do you want real time saving %%b\n"
		"Do you want to record bead xyz tracking error %%b\n"
		"Do you want to record bead xy tracking profiles %%b\n"
		"Do you want to record bead xy differential tracking profiles %%b\n"
		"Do you want to record bead image %%b (generates big trk file!)\n"
		"In this case define :\n"
		" \\oc  the ratio of bead image width with the cross arm %%6f\n"
		" \\oc  the ratio of bead image heigh with the cross arm %%6f\n"
		" \\oc  does the image center track the bead %%b\n");
       i = win_scanf(question,&with_radial_profiles,&ortho_profiles, &streaming, &xyz_error, &rec_xy_prof,
		     &rec_xy_diff_prof, &rec_small_image, &movie_rw, &movie_rh, &movie_track);
       if (i == WIN_CANCEL)	 return OFF;
     }


   z_tmp = zstart + (nstep - 1) * zstep;
   if (z_tmp > absolute_maximum_zmag)
     {
       i = win_printf("You have specified zmag start = %g, with %d steps of %g\n"
		     "with these values your final zmag %g exceeds the safe limit %g mm\n"
		     "You might have made a mistake. Are you sure you want to persue ?\n"
		      ,zstart ,nstep ,zstep, z_tmp,absolute_maximum_zmag );
	 if (i == WIN_CANCEL)	return OFF;
     }





   Open_Pico_config_file();
   set_config_float("ZScan", "zstart", zstart);
   set_config_int("ZScan", "n_repeat", n_repeat);
   set_config_float("ZScan", "zstep", zstep);
   set_config_int("ZScan", "n_step", nstep);
   set_config_int("ZScan", "nper", nper);
   set_config_int("ZScan", "one_way", one_way);
   set_config_int("ZScan", "maxnper", maxnper);
   set_config_int("ZScan", "setling", setling);
   set_config_float("ZScan", "zmag0", zmag0);
   set_config_float("ZScan", "lambdazmag", lambdazmag);
   //set_config_float("ZScan", "correction", z_cor);
   set_config_float("ZScan", "zmag_after", zlow);
   set_config_float("ZScan", "rotation", rotation);
   write_Pico_config_file();
   Close_Pico_config_file();

   dead = (int)((Pico_param.camera_param.camera_frequency_in_Hz*fabs(zstep))/get_motors_speed());
   dead *= 2;

   nw = (one_way) ? 1 : 2;
   for (n_maxnper = 0; (nper*(1<<n_maxnper)) < maxnper; n_maxnper++);


   for (i = 0, nf = 0; i < nw*nstep ; i++)
     {
       j = i;
       j = (j < nstep) ? j : 2 * nstep -1 - j;
       j = (int)(lambdazmag * (zmag0 - zstart - j * zstep));
       j = (j <= n_maxnper) ? j : n_maxnper;
       tmpi = (j > 0) ?  nper<<j : nper;
       tmpi = (tmpi < maxnper) ? tmpi : maxnper;
       nf += tmpi;
       nf += dead;
     }
   nf += 512;


   i = win_printf("Zmaq scan curve will last %6.2f seconds (%d frames)\n"
		  "Pressing Cancel will abord acquisition",
		  (float)nf/Pico_param.camera_param.camera_frequency_in_Hz,nf);
   if (i == WIN_CANCEL) return D_O_K;


   sp = (scan_param*)calloc(1,sizeof(scan_param));
   if (sp == NULL)  win_printf_OK("Could no allocte scan parameters!");
   sp->im = (int*)calloc(nw*nstep+1,sizeof(int));
   sp->grim = (int*)calloc(nw*nstep+1,sizeof(int));
   sp->rec_n = (int*)calloc(nw*nstep+1,sizeof(int));
   sp->zm = (float*)calloc(nw*nstep+1,sizeof(float));
   sp->fm = (float*)calloc(nw*nstep+1,sizeof(float));
   sp->zo = (float*)calloc(nw*nstep+1,sizeof(float));
   sp->is_rot = 0;

   if (sp->im == NULL || sp->zm == NULL || sp->fm == NULL || sp->fm == NULL)
     win_printf_OK("Could no allocte scan parameters!");

   im = track_info->imi[track_info->c_i];
   tmp = Pico_param.dna_molecule.dsDNA_molecule_extension
     * extension_versus_alpha((zmag_to_force(0,zstart) * 50)/ (2.98 * 1.38));
   tmp /= track_info->focus_cor;

   for (i = 0, nf = 0; i < nw*nstep ; i++)
     {
       j = i;
       j %= 2*nstep;
       j = (j < nstep) ? j : 2 * nstep -1 - j;
       sp->zm[i] = zstart + (j * zstep);
       sp->fm[i] = zmag_to_force(0, sp->zm[i]);
       sp->im[i] = im + nf;
       sp->zo[i] = prev_focus - tmp + (Pico_param.dna_molecule.dsDNA_molecule_extension
	       * extension_versus_alpha((sp->fm[i] * 50)/ (2.98 * 1.38)))/track_info->focus_cor;
       j = (int)(lambdazmag * (zmag0 - zstart - j * zstep));
       j = (j <= n_maxnper) ? j : n_maxnper;
       sp->rec_n[i] = (j > 0) ?  nper<<j : nper;
       sp->rec_n[i] = (sp->rec_n[i] < maxnper) ? sp->rec_n[i] : maxnper;
       nf += (j > 0) ?  nper<<j : nper;
       nf += sp->rec_n[i];
       nf += dead;
     }
   sp->zm[i] = (one_way) ? (zstart + (nstep * zstep)) : zstart;
   sp->fm[i] = zmag_to_force(0, sp->zm[i]);
   sp->im[i] = im + nf;
   sp->rec_n[i] = nper;
   nf += 512;
   nf *= n_repeat;
   sp->nf = nf;
   sp->nstep = nstep;
   sp->dead = dead;
   sp->nper = nper;
   sp->setling = setling;
   sp->one_way = one_way;
   sp->c_step = 0;
   sp->fir = fir;
   sp->repeat = n_repeat;
   sp->i_repeat = 0;
   sp->rotation = rotation;

   sp->zstart = zstart;
   sp->zstep = zstep;
   sp->zlow = zlow;
   sp->z_cor = track_info->focus_cor;
   sp->lambdazmag = lambdazmag;
   sp->zmag0 = zmag0;
   sp->adj_focus = adj_focus;
   snprintf(name,64,"Scan Zmag %d",n_scan);

   // We create a plot region to display bead position, scan  etc.
   pr = create_hidden_pltreg_with_op(&op, nf, nf, 0,name);
   if (pr == NULL)  win_printf_OK("Could not find or allocte plot region!");
   ds = op->dat[0];
   //ds->xd[1] = ds->yd[1] = 0.1;
   im = track_info->imi[track_info->c_i];
   im += 25;

   sp->fir = fir;

   im = track_info->imi[track_info->c_i];
   xy_tracking_type = (rec_xy_prof) ? XY_BEAD_PROFILE_RECORDED : 0;
   xy_tracking_type |= (rec_xy_diff_prof) ? XY_BEAD_DIFF_PROFILE_RECORDED : 0;
   xy_tracking_type |= (rec_small_image) ? RECORD_BEAD_IMAGE : 0;
   xy_tracking_type |= (xyz_error) ? XYZ_ERROR_RECORDED : 0;

   scan_r = create_gen_record(track_info, 1, with_radial_profiles,0,1,xy_tracking_type, movie_rw, movie_rh, movie_track);
   duplicate_Pico_parameter(&Pico_param, &(scan_r->Pico_param_record));
   if (extract_file_name(scan_r->filename, sizeof(scan_r->filename), fullfilename) == NULL)
     snprintf(scan_r->filename,sizeof(scan_r->filename), "Zscan_%d.trk",n_scan);
   scan_r->record_action = scan_action;
   scan_action(0,0,0,1);
   scan_r->data_type |= SCAN_ZMAG;


   if (streaming)
     {
       if (current_write_open_path_picofile != NULL && file_selection == 0)
	 {
	   strncpy(scan_r->path,current_write_open_path_picofile,sizeof(scan_r->path));
	   write_record_file_header(scan_r, oi_TRACK);
	   scan_r->real_time_saving = 1;
	 }
       else if (do_save_track(scan_r))
	 win_printf("Pb writing tracking header");
     }


   if (current_write_open_path_picofile != NULL)
     {
       dump_to_html_log_file_with_date_and_time(current_write_open_picofile,
						"<h2>Scan Zmag curve recording</h2>"
						"<br>in file %s in directory %s"
						"<br>zmag start %g, %d sets of %g lasting %d frames after %d frames to settle <br>"
						"<br>%d way(s)"
						"Zmag0 %g lambdazmag %g maxnper %d<br>"
						"Rotation %g<br>\n"
						,backslash_to_slash(scan_r->filename),backslash_to_slash(scan_r->path)
						,zstart,nstep,zstep,nper,dead,(one_way) ? 1 : 2,zmag0,lambdazmag
						,maxnper,rotation);
    }


   li = grab_basename_and_index(scan_r->filename, strlen(scan_r->filename)+1, basename, 256);
   n_scan = (li >= 0) ? li : n_scan;
   scan_r->n_rec = n_scan;

   scan_state = SCAN_RUNNING;
   //we create one plot for each bead
   if (attach_g_record_to_pltreg(pr, scan_r))
     win_printf("Could not attach grecord to plot region!");


   for (i = li = 0; i < track_info->n_b; i++)
     {
       bt = track_info->bd[i];
       if ((bt->calib_im == NULL) || (bt->not_lost <= 0) || (bt->in_image == 0))
	 continue;
       if (li)
	 {
	   op = create_and_attach_one_plot(pr, nf, nf, 0);
	   ds = op->dat[0];
	 }
       li++;
       bt->opt = op;
       ds->nx = ds->ny = 0;
       //ds->xd[1] = ds->yd[1] = 0.1;
       set_dot_line(ds);
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
		     "Z coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_scan,i
		     ,track_info->cl,track_info->cw,nf
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,pico_sample
		     ,rotation
		     ,zstart
		     ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");
       if ((ds = create_and_attach_one_ds(op, nf/fir, nf/fir, 0)) == NULL)
	 return win_printf_OK("I can't create plot !");
       ds->nx = ds->ny = 0;
       //ds->xd[1] = ds->yd[1] = 0.1;
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
		     "filtred Z over %d coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_scan,i
		     ,fir,track_info->cl,track_info->cw,nf
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,pico_sample
		     ,rotation
		     ,zstart
		     ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");

       if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
	 return win_printf_OK("I can't create plot !");
       ds->nx = ds->ny = 0;
       //ds->xd[1] = ds->yd[1] = 0.1;
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
		     "Zmag l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Zmagnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_scan,i
		     ,track_info->cl,track_info->cw,nf
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,pico_sample
		     ,rotation
		     ,zstart
		     ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");


       create_attach_select_y_un_to_op(op, 0, 0 ,(float)1, 0, 0, "no_name");
       set_plot_y_prime_title(op, "Z Magnets (mm)");

       op->c_yu_p = op->c_yu;
       //       uns_oi_2_op(oi, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
       create_attach_select_y_un_to_op(op, IS_METER, 0 ,(float)1, -6, 0, "\\mu m");
       create_attach_select_x_un_to_op(op, IS_SECOND, 0
				       ,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");

       op->op_idle_action = scan_op_idle_action; //xyz_hat_idle_action;
       op->op_post_display = scan_op_post_display;
       set_op_filename(op, "Z(t)bd%dZscan%d.gr",i,n_scan);
       set_plot_title(op, "Bead %d trajectory %d rotation %g", i,n_scan,rotation);
       set_plot_x_title(op, "Time");
       set_plot_y_title(op, "Z position");
       op->user_ispare[ISPARE_BEAD_NB] = i;
       op->x_lo = im + (-nf/64);
       op->x_hi = im + (nf + nf/64);
       set_plot_x_fixed_range(op);
       fill_next_available_job_spot(im, im+32, COLLECT_DATA, i, imr, oi, pr, op, (void*)sp, scan_record_job_z, 0);
       //win_printf("bd %d",i);
     }



   op = create_and_attach_one_plot(pr, nf, nf, 0);
   ds = op->dat[0];
   //ds->xd[1] = ds->yd[1] = 0.1;
   set_plot_title(op, "Zscan %d at rotation %g", n_scan,rotation);
   set_plot_file(op,"bddzzmag%03d.gr",n_scan);
   set_plot_x_title(op, "Real time");
   set_plot_y_title(op, "Zmag");

   set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d \n "
		 "Magnet zmag position l = %d, w = %d, nim = %d\n"
		 "objective %g, zoom factor %f, sample %s Rotation %g\n"
		 "Zmag = %g Calibration from %s"
		 ,Pico_param.camera_param.camera_frequency_in_Hz, n_scan
		 ,track_info->cl, track_info->cw, nf
		 ,Pico_param.obj_param.objective_magnification
		 ,Pico_param.micro_param.zoom_factor
		 ,sample
		 ,rotation,zstart
		 ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");

   create_attach_select_x_un_to_op(op, IS_SECOND, 0
				       ,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");
   /*
   op->x_lo = im - (nf/64);
   op->x_hi = im + nf + (nf/64);
   set_plot_x_fixed_range(op);
   */
   ds->nx = ds->ny = 0;
   op->op_idle_action = scan_op_idle_action;
   op->op_post_display = scan_op_post_display;
   fill_next_available_job_spot(im, im+32, COLLECT_DATA, i, imr, oi, pr, op, NULL, scan_record_job_zmag, 0);
   op->user_ispare[ISPARE_BEAD_NB] = 0;
   for (i = 0; i < track_info->n_b; i++)
     {
       bt = track_info->bd[i];
       if ((bt->calib_im == NULL) || (bt->not_lost <= 0) || (bt->in_image == 0))
	 continue;

       op = create_and_attach_one_plot(pr, nw*nstep,  nw*nstep, 0);
       ds = op->dat[0];
       //ds->xd[1] = ds->yd[1] = 0.1;
       ds->nx = ds->ny = 0;
       //set_dot_line(ds);
       set_plot_symb(ds, "\\pt5\\oc");
       set_plot_title(op, "\\stack{{Beads %d rotation %g Scan %d}{zmag %g + %d x %g}}", i, rotation, n_scan,zstart,nstep,zstep);
       set_plot_x_title(op,"Length (\\mu m)");
       set_plot_y_title(op,"Z mag ");
       set_plot_file(op,"scanzbd%01d%03d%s",i,n_scan,".gr");
       create_attach_select_x_un_to_op(op, IS_METER, 0, 1, -6, 0, "\\mu m");

       set_ds_source(ds,"Scan Z\nRot %g zscan start = %g for %d frames scan by steps of %g"
		     " for %d steps \ndead period %d\n"
		     "Bead  calbration %s \n"
		     ,rotation,zstart,nper,zstep,nstep
		     ,dead,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");

       op->op_idle_action = scan_op_idle_action;
       op->op_post_display = scan_op_post_display;
       op->user_ispare[ISPARE_BEAD_NB] = i;
       j =  nper;
       // there is a bug in scan_job_scan
       fill_next_available_job_spot(im, sp->im[1] + 40, COLLECT_DATA, i, imr, oi, pr, op, (void*)sp, scan_record_job_scan, 0);
       last_bead = i;
     }

   for (i = 0; i < track_info->n_b; i++)
     {
       bt = track_info->bd[i];
       if ((bt->calib_im == NULL) || (bt->not_lost <= 0) || (bt->in_image == 0))
	 continue;

       op = create_and_attach_one_plot(pr, nw*nstep,  nw*nstep, 0);
       ds = op->dat[0];
       //ds->xd[1] = ds->yd[1] = 0.1;
       ds->nx = ds->ny = 0;
       //set_dot_line(ds);
       set_plot_symb(ds, "\\pt5\\oc");
       set_plot_title(op, "\\stack{{Beads %d rotation %g Scan %d}{zmag %g + %d x %g}}", i, rotation, n_scan,zstart,nstep,zstep);
       set_plot_x_title(op,"Length (\\mu m)");
       set_plot_y_title(op,"Estimated force (pN)");
       set_plot_file(op,"scanzfbd%01d%03d%s",i,n_scan,".gr");
       create_attach_select_x_un_to_op(op, IS_METER, 0, 1, -6, 0, "\\mu m");
       create_attach_select_y_un_to_op(op, IS_NEWTON, 0, 1, -12, 0, "pN");

       set_ds_source(ds,"Scan Z\nRot %g zscan start = %g for %d frames scan by steps of %g"
		     " for %d steps \ndead period %d\n"
		     "Bead  calbration %s \n"
		     ,rotation,zstart,nper,zstep,nstep
		     ,dead,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");

       op->op_idle_action = scan_op_idle_action;
       op->op_post_display = scan_op_post_display;
       op->user_ispare[ISPARE_BEAD_NB] = i;
       j =  nper;
       // there is a bug in scan_job_scan
       fill_next_available_job_spot(im, sp->im[1] + 40, COLLECT_DATA, i, imr, oi, pr, op, (void*)sp, scan_record_job_scan_f, 0);
       last_bead = i;
     }

   im = track_info->imi[track_info->c_i];
   working_gen_record = scan_r;
   scan_r->last_starting_pos = im;
   win_title_used = 1;
   if (imr_and_pr) switch_project_to_this_pltreg(pr);
   generate_imr_TRACK_title = generate_scan_title;
 return D_O_K;
}


int stop_scan(void)
{
  pltreg *pr = NULL;
  g_record *g_r = NULL;

  ac_grep(cur_ac_reg,"%pr",&pr);
  g_r = (pr != NULL) ? find_g_record_in_pltreg(pr) : working_gen_record;

  if(updating_menu_state != 0)
    {
      if (working_gen_record == NULL || g_r != working_gen_record)
	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }
  if (working_gen_record != NULL)
    {
      working_gen_record = NULL;
      snprintf(scan_state_disp,sizeof(scan_state_disp),"Zmag scan %d stoped",n_scan);
      imr_TRACK_title_change_asked++;
      if (current_write_open_path_picofile != NULL)
	dump_to_html_log_file_with_date_and_time(current_write_open_picofile,"Scan Zmag recording stopped");
      if (scan_r != NULL)
	{
	  scan_r->n_record++;
	}
    }
  ask_to_update_menu();
  return D_O_K;
}


int suspend_scan(void)
{
  pltreg *pr = NULL;
  g_record *g_r = NULL;

  ac_grep(cur_ac_reg,"%pr",&pr);
  g_r = (pr != NULL) ? find_g_record_in_pltreg(pr) : working_gen_record;

  if(updating_menu_state != 0)
    {
      if (scan_state != SCAN_RUNNING && scan_state != SCAN_SUSPENDED)
	active_menu->flags |=  D_DISABLED;
      else if (working_gen_record == NULL || g_r != working_gen_record)
	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }
  if (working_gen_record != NULL)
    {
      if (scan_state == SCAN_RUNNING)
	{
	  scan_state = SCAN_SUSPENDED;
	  active_menu->text = "Restart";
	  if (current_write_open_path_picofile != NULL)
	      dump_to_html_log_file_with_date_and_time(current_write_open_picofile,"Scan Zmag recording suspended");
	}
      else if (scan_state == SCAN_SUSPENDED)
	{
	  scan_state = SCAN_RUNNING;
	  active_menu->text = "Suspend";
	}
    }
  ask_to_update_menu();
  return D_O_K;
}




int draw_scan_XYZ_trajectories(void)
{
  int i, j, page_n, i_page;
  b_track *bt = NULL;
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *ds = NULL;
  int nf;
  float rot_start = 10,  zmag;
  b_record *br = NULL;
  g_record *g_r = NULL;

  ac_grep(cur_ac_reg,"%pr",&pr);
  g_r = find_g_record_in_pltreg(pr);
  if(updating_menu_state != 0)
    {  // we wait for recording finish
      if (working_gen_record != NULL || scan_r == NULL)
	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }
  if (pr == NULL)return 0;

  if (g_r == NULL || g_r->n_record == 0) return OFF;
  nf = g_r->abs_pos;
  if (nf <= 0) return OFF;

  rot_start = g_r->rot_mag[0][0];
  zmag = g_r->zmag[0][0];

  //we create one plot for each bead
  for (i = 0; i < g_r->n_bead; i++)
    {
      br = g_r->b_r[i];
      bt = br->b_t;
      if (bt->calib_im == NULL)	continue;
      op = create_and_attach_one_plot(pr, nf, nf, 0);
      ds = op->dat[0];
      set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
		    "X coordinate l = %d, w = %d, nim = %d\n"
		    "objective %f, zoom factor %f, sample %s Rotation %g\n"
		    "Z magnets %g mm\n"
		    "Calibration from %s"
		    ,Pico_param.camera_param.camera_frequency_in_Hz,n_scan,i
		    ,track_info->cl,track_info->cw,nf
		    ,Pico_param.obj_param.objective_magnification
		    ,Pico_param.micro_param.zoom_factor
		    ,pico_sample
		    ,rot_start
		    ,zmag
		    ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");
      for (j = 0; j < nf; j++)
	{
	  page_n = j/g_r->page_size;
	  i_page = j%g_r->page_size;
	  ds->xd[j] = g_r->imi[page_n][i_page];
	  ds->yd[j] = g_r->ax + g_r->dx * br->x[page_n][i_page];
	}

       if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
	 return win_printf_OK("I can't create plot !");
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
		     "Y coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_scan,i
		     ,track_info->cl,track_info->cw,nf
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,pico_sample
		     ,rot_start
		     ,zmag
		     ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");
      for (j = 0; j < nf; j++)
	{
	  page_n = j/g_r->page_size;
	  i_page = j%g_r->page_size;
	  ds->xd[j] = g_r->imi[page_n][i_page];
	  ds->yd[j] = g_r->ay + g_r->dy * br->y[page_n][i_page];
	}
       if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
	 return win_printf_OK("I can't create plot !");
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
		     "Z coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_scan,i
		     ,track_info->cl,track_info->cw,nf
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,pico_sample
		     ,rot_start
		     ,zmag
		     ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");
      for (j = 0; j < nf; j++)
	{
	  page_n = j/g_r->page_size;
	  i_page = j%g_r->page_size;
	  ds->xd[j] = g_r->imi[page_n][i_page];
	  ds->yd[j] = g_r->z_cor * br->z[page_n][i_page];
	}
       create_attach_select_y_un_to_op(op, IS_METER, 0 ,(float)1, -6, 0, "\\mu m");
       create_attach_select_x_un_to_op(op, IS_SECOND, 0
				       ,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");


       set_op_filename(op, "X(t)Y(t)Z(t)bd%dscan%d.gr",i,g_r->n_rec);
       set_plot_title(op, "Bead %d Z(t) %d zmag %g", i,g_r->n_rec,zmag);
       set_plot_x_title(op, "Time");
       set_plot_y_title(op, "Position");
       op->need_to_refresh = 1;
     }
  return refresh_plot(pr, pr->n_op - 1);
}

int draw_scan_trajectories_params(void)
{
  int  j,  page_n, i_page;
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *ds = NULL;
  int nf;
  g_record *g_r = NULL;

  ac_grep(cur_ac_reg,"%pr",&pr);
  g_r = find_g_record_in_pltreg(pr);
  if(updating_menu_state != 0)
    {  // we wait for recording finish
      if (g_r == NULL || working_gen_record == g_r)
	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }

  if (pr == NULL || g_r == NULL || g_r->n_record == 0) return OFF;
  nf = g_r->abs_pos;
  if (nf <= 0) return OFF;

  //we create one plot for each bead
  op = create_and_attach_one_plot(pr, nf, nf, 0);
  ds = op->dat[0];
  set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d  \n "
		"Zmag l = %d, w = %d, nim = %d\n"
		"objective %f, zoom factor %f, sample %s\n"
		,Pico_param.camera_param.camera_frequency_in_Hz,n_scan
		,track_info->cl,track_info->cw,nf
		,Pico_param.obj_param.objective_magnification
		,Pico_param.micro_param.zoom_factor
		,pico_sample);


  for (j = 0; j < nf; j++)
    {
      page_n = j/g_r->page_size;
      i_page = j%g_r->page_size;
      ds->xd[j] = g_r->imi[page_n][i_page];
      ds->yd[j] = g_r->zmag[page_n][i_page];
    }

  if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("I can't create plot !");
  set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d  \n "
		"magnets rotation l = %d, w = %d, nim = %d\n"
		"objective %f, zoom factor %f, sample %s\n"
		,Pico_param.camera_param.camera_frequency_in_Hz,n_scan
		,track_info->cl,track_info->cw,nf
		,Pico_param.obj_param.objective_magnification
		,Pico_param.micro_param.zoom_factor
		,pico_sample);

  for (j = 0; j < nf; j++)
    {
      page_n = j/g_r->page_size;
      i_page = j%g_r->page_size;
      ds->xd[j] = g_r->imi[page_n][i_page];
      ds->yd[j] = g_r->rot_mag[page_n][i_page];
    }
  if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("I can't create plot !");
  set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d \n "
		"Objective position l = %d, w = %d, nim = %d\n"
		"objective %f, zoom factor %f, sample %s\n"
		,Pico_param.camera_param.camera_frequency_in_Hz,n_scan
		,track_info->cl,track_info->cw,nf
		,Pico_param.obj_param.objective_magnification
		,Pico_param.micro_param.zoom_factor
		,pico_sample);


  for (j = 0; j < nf; j++)
    {
      page_n = j/g_r->page_size;
      i_page = j%g_r->page_size;
      ds->xd[j] = g_r->imi[page_n][i_page];
      ds->yd[j] = g_r->obj_pos[page_n][i_page];
    }
  create_attach_select_x_un_to_op(op, IS_SECOND, 0
				  ,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");


  set_op_filename(op, "ZmagRotFocusscan%d.gr",g_r->n_rec);
  set_plot_title(op, "Scan %d zmag, rotation & focus ", g_r->n_rec);
  set_plot_x_title(op, "Time");
  set_plot_y_title(op, "Zmag, rot, focus");

  op->need_to_refresh = 1;
  return refresh_plot(pr, pr->n_op - 1);
}




MENU *scan_menu(void)
{
	static MENU mn[32] = {0};

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn, "record scan", record_scan_record, NULL, 0, NULL);
  	add_item_to_menu(mn, "record scan with rot noise", record_scan_rot_noise_force, NULL, 0, NULL);
	add_item_to_menu(mn, "stop scan", stop_scan, NULL, 0, NULL);
	add_item_to_menu(mn, "suspend scan", suspend_scan, NULL, 0, NULL);
	add_item_to_menu(mn,"\0",	NULL,	NULL,	0, NULL);

	return mn;
}

MENU *scan_plot_menu(void)
{
	static MENU mn[32] = {0};

	if (mn[0].text != NULL)	return mn;
	//	add_item_to_menu(mn, "record Z(t)", record_zdet, NULL, 0, NULL);
	add_item_to_menu(mn, "stop scan", stop_scan, NULL, 0, NULL);
	add_item_to_menu(mn, "suspend scan", suspend_scan, NULL, 0, NULL);


	add_item_to_menu(mn,"\0",	NULL,	NULL,	0, NULL);
	add_item_to_menu(mn, "Dump XYZ", draw_scan_XYZ_trajectories, NULL, 0, NULL);
	add_item_to_menu(mn, "Dump Zmag Rot Focus", draw_scan_trajectories_params, NULL, 0, NULL);


	return mn;
}




# endif
