#ifndef _REAL_TIME_AVG_C_
#define _REAL_TIME_AVG_C_



# include "allegro.h"
# include "winalleg.h"
# include "ctype.h"
# include "xvin.h"
# include "fftl32n.h"
# include "fillib.h"
# include <fftw3.h>		// include the fftw v3.0.1 or v3.1.1 library for computing spectra:
# include "../fft2d/fft2d.h"


/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "../cfg_file/Pico_cfg.h"
//# include "../phaseindex/phaseindex.h"

#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)

# include "PlayItSam.h"
# include "real_time_avg_TL.h"
# include "action_live_video.h"
//# include "focus.h"

int check_heap_raw(char *file, unsigned long line)
{
  int out;

  out = _heapchk();
  if (out != -2)  win_printf("File %s at line %d\nheap satus %d (-2 =>OK)\n",file,(int)line,out);
  return (out == -2) ? 0 : out;
}

// phase timer for real-time screening of phase

void phase_timer(void)
{
  int ci, phi1, phi2;
  
  ci = ph_lock.c_i;
  ci++;
  if (ci >= ph_lock.n_i) ci = 0;
  phi1 = ph_lock.phase2[ci] = do_read_db25_phi();  
  ph_lock.imt[ci] = my_ulclock(); 
  phi2 = do_read_db25_phi();  
  if (ph_lock.ac_i == 0) 
    {
      ph_lock.phase2[0] = phi1;
      ph_lock.imt[0] = ph_lock.imt[1];
      ph_lock.imt0 = ph_lock.imt[ci];
    }
  if (phi1 == phi2)
    {
      ph_lock.c_i = ci;
      ph_lock.ac_i++;
    }
   
}
END_OF_FUNCTION(phase_timer)


int phase_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  register int i, j   ;
  static int last_c_i = -1, immax = -1;
  d_s *dsx/*, *dsy, *dsz*/;
  int nf, b_size;
  double tmpd;
    
  if (real_time_info == NULL) return D_O_K;
 
  dsx = find_source_specific_ds_in_op(op,"Modulation phase measured");
  if (dsx == NULL) return D_O_K;
  //dsy = find_source_specific_ds_in_op(op,"Modulation phase simulated"); 
  //if (dsy == NULL) return D_O_K; 
  // dsz = find_source_specific_ds_in_op(op,"Error");
  //if (dsz == NULL) return D_O_K;
 
  i = ph_lock.c_i;
  if (ph_lock.ac_i <= immax) return D_O_K;   // we are uptodate
  immax = ph_lock.ac_i;   

  b_size = 50*REAL_TIME_BUFFER_SIZE/3;            
  nf = (ph_lock.ac_i > b_size) ? b_size : ph_lock.ac_i;      
  nf = (nf > (b_size>>4)) ? (b_size>>4) : nf;
  dsx->nx = dsx->ny = nf;
  //dsy->nx = dsy->ny = nf;
  //dsz->nx = dsz->ny = nf;
  
  last_c_i = (ph_lock.c_i > 0) ? ph_lock.c_i - 1 : 0;  // c_i is not yet valid
  for (i = nf-1, j = last_c_i; i >= 0; i--, j--) 
    {
      j = (j < 0) ? b_size + j: j;
      tmpd = 1000*((double)(ph_lock.imt[j] - ph_lock.imt0))/get_my_ulclocks_per_sec();
      dsx->xd[i] = (float)tmpd;
      dsx->yd[i] = (float)ph_lock.phase2[j];
      //dsy->xd[i] = (float)tmpd;
      //dsy->yd[i] = (float)ph_lock.phase1[j];
      //dsz->xd[i] = (float)tmpd;
      //dsz->yd[i] = (float)ph_lock.dephi[j];
    }
  op->x_lo = dsx->xd[0] - (dsx->xd[dsx->nx-1] - dsx->xd[0])/20;  
  op->x_hi = dsx->xd[dsx->nx-1] + (dsx->xd[dsx->nx-1] - dsx->xd[0])/20;
  set_plot_x_fixed_range(op);
   
  op->need_to_refresh = 1; 
  //set_plot_title(op, "\\stack{{image %d}{Period %d}{Period in ms %8f}{d\\phi %g}}"
  //		 ,ph_lock.ac_i, lockin_info->per_i,(float)(1000*(double)lockin_info->per_i/get_my_ulclocks_per_sec()),lockin_info->dphi_t);
  return refresh_plot(pr_LIVE_VIDEO, UNCHANGED);
}


int timing_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  register int i, j, k;
  static int  immax = -1; // last_c_i = -1,
  d_s *dsx, *dsy, *dsz;
  int nf, lost_fr, lf, jstart = 0, j1 = 0;
  unsigned long   dtm, dtmax, dts;
  float   fdtm, fdtmax;
  double min_delay_camera_thread_in_ms, tmp, tmp2;
  long long llt;

  if (real_time_info == NULL) return D_O_K;
  dts = get_my_uclocks_per_sec();
  dsx = find_source_specific_ds_in_op(op,"Timing rolling buffer");
  dsy = find_source_specific_ds_in_op(op,"Period rolling buffer");
  dsz = find_source_specific_ds_in_op(op,"Timer rolling buffer");
  if (dsx == NULL || dsy == NULL || dsz == NULL) return D_O_K;	

  i = real_time_info->c_i;
 /*  if (real_time_info->imi[i] <= immax) */
/*     { */
/*         my_set_window_title("waiting %d %d",real_time_info->imi[i], immax); */
/*         // my_set_window_title("waiting %d %d",lockin_info->imi[i], immax); */
/* 	return D_O_K;   // we are up todate */
/*     } */

  min_delay_camera_thread_in_ms = bid.min_delay_camera_thread_in_ms;

  i = real_time_info->c_i;
  immax = real_time_info->imi[i];               
  lf = immax - real_time_info->ac_i;
  if (real_time_info->ac_i > (REAL_TIME_BUFFER_SIZE>>1)) 
    {
      nf = REAL_TIME_BUFFER_SIZE>>1;
      j = jstart = (real_time_info->ac_i - (REAL_TIME_BUFFER_SIZE>>1))%REAL_TIME_BUFFER_SIZE;  //last_c_i;
    }
  else
    {
      nf = real_time_info->ac_i-1;
      j = jstart = 1;
    }
  dsx->nx = dsx->ny = nf;
  dsy->nx = dsy->ny = nf;
  dsz->nx = dsz->ny = nf;
  
  j = jstart + nf - 1;
  j = (j < REAL_TIME_BUFFER_SIZE) ? j : j - REAL_TIME_BUFFER_SIZE;
  llt = real_time_info->imt[j];
  j1 = j;
  j = jstart;
  j = (j < REAL_TIME_BUFFER_SIZE) ? j : j - REAL_TIME_BUFFER_SIZE;
  llt -= real_time_info->imt[j];
  llt /= (nf - 1);
  
  for (i = 0, j = jstart, lost_fr = 0, dtm = dtmax = 0, fdtm = fdtmax = 0, k = 0, tmp2 = 0, lf = 0; i < nf; i++, j++, k++)
    {
      j = (j < REAL_TIME_BUFFER_SIZE) ? j : j - REAL_TIME_BUFFER_SIZE;
      dsx->xd[i] = dsy->xd[i] = dsz->xd[i] = real_time_info->imi[j];
      if (i > 0)
	lf += real_time_info->imi[j] - real_time_info->imi[j1] - 1;
      dsx->yd[i] = ((float)(1000*real_time_info->imdt[j]))/dts;
      tmp = real_time_info->imt[j];
      tmp -= (i == 0) ? real_time_info->imt[j] : real_time_info->imt[j1];
      tmp = 1000*(double)(tmp)/get_my_ulclocks_per_sec();
      //tmp -= bid.image_per_in_ms * i;
      tmp2 += tmp;
      dsy->yd[i] = (float)tmp;
      dsz->yd[i] = real_time_info->imit[j];

      dtm += real_time_info->imdt[j];
      fdtm += dsx->yd[i];
      dtmax = (real_time_info->imdt[j] > dtmax) ? real_time_info->imdt[j] : dtmax;
      fdtmax = (dsx->yd[i] > fdtmax) ? dsx->yd[i] : fdtmax;
      j1 = j;
    }
  dsy->yd[0] =  dsy->yd[1];
  op->need_to_refresh = 1; 
  if (k) set_plot_title(op, "\\stack{{fr position lac_i %d }{%d missed images}"
			"{\\pt8 dt mean %6.3f ms (%6.3f ms) max %6.3f (%6.3f)}}"
			,bid.first_im +real_time_info->ac_i 
			,lf,fdtm/k,
			1000*((double)(dtm/k))/dts,
			fdtmax, 1000*((double)(dtmax))/dts);
  i = REAL_TIME_BUFFER_SIZE/8;
  j = ((int)dsx->xd[1])/i;
  op->x_lo = j*i;
  op->x_hi = (j+5)*i;
  set_plot_x_fixed_range(op);
  return refresh_plot(pr_LIVE_VIDEO, UNCHANGED);
}

/* int thermal_modulation_buffer_idle_action(O_p *op, DIALOG *d) */
/* { */
/*   register int i, j; */
/*   static int last_c_i = -1, immax = -1; */
/*   d_s *dsx; */
/*   int nf; */
/*   //int _get_camera_BM141GE_TTLIN(void); */

/*   if (real_time_info == NULL) return D_O_K; */

/*   dsx = find_source_specific_ds_in_op(op,"Thermal modulation signal"); */
/*   if (dsx == NULL) return D_O_K;	 */

/*   i = real_time_info->c_i; */
/*   if (real_time_info->imi[i] <= immax) return D_O_K;   // we are uptodate */
/*   immax = real_time_info->imi[i];                */
/*   nf = (real_time_info->ac_i > REAL_TIME_BUFFER_SIZE) ? REAL_TIME_BUFFER_SIZE : real_time_info->ac_i; */
/*   nf = (nf > (REAL_TIME_BUFFER_SIZE>>1)) ? REAL_TIME_BUFFER_SIZE>>1 : nf; */
  
/*   dsx->nx = dsx->ny = nf; */
/*   last_c_i = (real_time_info->c_i > 0) ? real_time_info->c_i - 1 : 0;  // c_i is not yet valid */
/*   for (i = nf-1, j = last_c_i; i >= 0; i--, j--) */
/*     { */
/*       j = (j < 0) ? REAL_TIME_BUFFER_SIZE + j: j; */
/*       j = (j < REAL_TIME_BUFFER_SIZE) ? j : REAL_TIME_BUFFER_SIZE; */
/*       dsx->xd[i] = real_time_info->imi[j]; */
/*       //dsx->yd[i] = _get_camera_BM141GE_TTLIN(); */
/*       // dsx->yd[i] = do_read_db25_TTL(); */
/*       dsx->yd[i] = real_time_info->th_mod[j]; */
/*     } */
 
/*   i = REAL_TIME_BUFFER_SIZE/8; */
/*   j = ((int)dsx->xd[i])/i; */
/*   op->x_lo = (j-1)*i; */
/*   op->x_hi = (j+4)*i; */
/*   set_plot_x_fixed_range(op); */
/*   op->need_to_refresh = 1;  */
/*   return refresh_plot(pr_LIVE_VIDEO, UNCHANGED); */
/* } */
	
int obj_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  register int i, j;
  static int last_c_i = -1, immax = -1;
  d_s *dsx;
  int nf;
  
  if (real_time_info == NULL) return D_O_K;

  dsx = find_source_specific_ds_in_op(op,"Obj. rolling buffer");
  if (dsx == NULL) return D_O_K;	

  i = real_time_info->c_i;
  if (real_time_info->imi[i] <= immax) return D_O_K;   // we are uptodate
  immax = real_time_info->imi[i];               
  nf = (real_time_info->ac_i > REAL_TIME_BUFFER_SIZE) ? REAL_TIME_BUFFER_SIZE : real_time_info->ac_i;
  nf = (nf > (REAL_TIME_BUFFER_SIZE>>1)) ? REAL_TIME_BUFFER_SIZE>>1 : nf;

  dsx->nx = dsx->ny = nf;
  last_c_i = (real_time_info->c_i > 0) ? real_time_info->c_i - 1 : 0;  // c_i is not yet valid
  for (i = nf-1, j = last_c_i; i >= 0; i--, j--)
    {
      j = (j < 0) ? REAL_TIME_BUFFER_SIZE + j: j;
      j = (j < REAL_TIME_BUFFER_SIZE) ? j : REAL_TIME_BUFFER_SIZE;
      dsx->xd[i] = real_time_info->imi[j];
      dsx->yd[i] = real_time_info->obj_pos[j];
    }
  i = REAL_TIME_BUFFER_SIZE/8;
  j = ((int)dsx->xd[i])/i;
  op->x_lo = (j-1)*i;
  op->x_hi = (j+4)*i;
  set_plot_x_fixed_range(op);

  op->need_to_refresh = 1; 
  set_plot_title(op, "\\stack{{image %d}{Obj %g obj}}"
		 ,real_time_info->ac_i, real_time_info->obj_pos[last_c_i]);
  return refresh_plot(pr_LIVE_VIDEO, UNCHANGED);
}




//commands to operate on the averaged movies////////////////////////


int oi_std_idle_action(O_i *oi, DIALOG *d)
{
  if (d == NULL || d->dp == NULL || d->proc == NULL)    return 1;
  if (oi->need_to_refresh)      d->proc(MSG_DRAW,d,0);	
  return 0;
}

//////

// this running in the timer thread
int do_at_all_image(imreg *imr, O_i *oi, int n,  DIALOG *d, long long t, unsigned long dt, void *p, int n_inarow) 
{
  register int i, j, k;
  int ci, ci_1, ac_i/*, im, im_i*/;
  g_real_time *gt = NULL;

  BITMAP *imb = NULL;
  O_i *oia;
  unsigned char *p_char;
  unsigned short int *p_uint;
  int *p_lint;
  
  int n_per, n_avg, n_tot, i_per, i_start, i_stop;
  //static unsigned long t_c, to = 0;
  
  gt = (g_real_time*)p;

  ac_i = real_time_info->ac_i;
  ac_i++;
  ci_1 = ci = real_time_info->c_i;
  ci++;
  ci = (ci < real_time_info->n_i) ? ci : 0;
  
  real_time_info->imi[ci] = n;                                 // we save image" number
  real_time_info->imit[ci] = n_inarow;                                 
  real_time_info->imt[ci] = t;                                 // image time
  if (real_time_info->ac_i == 0)  real_time_info->imt0 = real_time_info->imt[ci]; 

  real_time_info->th_mod[ci]= do_read_db25_TTL();
  
  
  n_per = real_time_info->n_per;
  n_avg = real_time_info->n_avg;
  n_tot = n_per * n_avg;

  if (oi->bmp.stuff != NULL) imb = (BITMAP*)oi->bmp.stuff;
  // we grab the video bitmap of the IFC image
  prepare_image_overlay(oi);
  oia = real_time_info->oi_avg;
  //t_c = my_uclock();
  // my_set_window_title("real_time_info->rec_start %d",real_time_info->rec_start);
  if (real_time_info->rec_start == 1 && real_time_info->th_mod[ci] == 1 && real_time_info->th_mod[ci-1] == 0) 
    {

      real_time_info->i_start_avg = ac_i +(3*real_time_info->n_per)/4; 
      real_time_info->rec_start -= 1;
      real_time_info->first_rec +=1;
    }
  i_start = real_time_info->i_start_avg;
  i_stop = i_start + n_tot;
  //my_set_window_title("aci %d istart %d istop %d",ac_i,i_start,i_stop);
  if (i_start > 0 && ac_i >=  i_start && ac_i < i_stop)
    {
      i_per = (ac_i - i_start)%n_per;
      if (oi->im.data_type == IS_CHAR_IMAGE)
	{
	  for (i = 0; i < oi->im.ny; i++)
	    {
	      p_char = oi->im.pixel[i].ch;
	      p_lint = oia->im.pxl[i_per][i].li;
	      for (j = 0; j < oi->im.nx; j++)
		p_lint[j] += p_char[j];
	    }
	}
      else if (oi->im.data_type == IS_UINT_IMAGE)
	{
	  for (i = 0; i < oi->im.ny; i++)
	    {
	      p_uint = oi->im.pixel[i].ui;
	      p_lint = oia->im.pxl[i_per][i].li;
	      for (j = 0; j < oi->im.nx; j++)
		p_lint[j] += p_uint[j];
	    }
	}
      if (i_per == 0)
	{
	  i = (ac_i - i_start)/n_per;
	  i = (i == 0) ? 1 : i;
	  set_z_black_z_white_values(oia, 0, (float)255*i);
	  set_zmin_zmax_values(oia, 0, (float)255*i);
	  my_set_window_title("i iter for averaging on a single period %d",i);
	}
      // recording plot to check phase synchronization
      O_p *op;
      op = oia->o_p[0];
      //my_set_window_title("first rec flag %d",real_time_info->first_rec);
      op->dat[0]->xd[ac_i-i_start] = ac_i-i_start;
      op->dat[0]->yd[ac_i-i_start] = real_time_info->th_mod[ci];
    }

  //t_c = my_uclock() - t_c;
  //if (to == 0) to = MY_UCLOCKS_PER_SEC;
  //my_set_window_title("FFT took %g ms",(double)(t_c*1000)/to)

  real_time_info->imdt[ci] = my_uclock() - dt;
  real_time_info->c_i = ci;
  real_time_info->ac_i = ac_i;
  /////////////////////////////////////
  /////////////////////////////////////

  i = find_remaining_action(n);
  real_time_info->status_flag[ci] = real_time_info->status_flag[ci_1];
  real_time_info->obj_pos_cmd[ci] = real_time_info->obj_pos_cmd[ci_1];
  while ((i = find_next_action(n)) >= 0)
    {
      action_pending[i].proceeded = 1;
    }

  return 0; 
}
END_OF_FUNCTION(do_at_all_image)

int record_avg_movie(void)
{
  int n_avg, i, j, k;
  static int rec_start = 0;
  O_p *op;
  int *p_lint;

  if (real_time_info == NULL)  return 0;
  n_avg = real_time_info->n_avg;

  if(updating_menu_state != 0)	return 0;
  i = win_scanf("For real time treatment and averaging\n"
                "You want to record averaged movie %b\n"
	        "Number of averaging modulation periods %8d\n"
	        , &rec_start, &n_avg);
  if (i == CANCEL) return 0;
  real_time_info->n_avg = n_avg;
  if (real_time_info->first_rec != 0) 
    {
      remove_from_one_image (real_time_info->oi_avg, IS_ONE_PLOT, real_time_info->oi_avg->o_p[0]);
      for (k = 0; k < real_time_info->n_per; k++)
	{
	  for (i = 0; i < real_time_info->oi_avg->im.ny; i++)
	    {
	      p_lint = real_time_info->oi_avg->im.pxl[k][i].li;
	      for (j = 0; j < real_time_info->oi_avg->im.nx; j++) p_lint[j] = 0;
	    }
	}
    }
  // plot to check phase synchronisation between recording and thermal modulation
  if ((op = create_and_attach_op_to_oi(real_time_info->oi_avg, n_avg*real_time_info->n_per, n_avg* real_time_info->n_per, 0, 0)) == NULL)
   	 return win_printf_ptr("I can't create plot!");
  real_time_info->rec_start = rec_start;
  rec_start = 0;

  return 0;  
}

MENU *real_time_avg_menu(void)
{
	static MENU mn[32];
	if (mn[0].text != NULL)	return mn;
        add_item_to_menu(mn,"record avg movie", record_avg_movie,NULL,MENU_INDEX(0),NULL);
	return mn;
}

int get_present_image_nb(void)
{
 return (real_time_info != NULL) ?  real_time_info->imi[real_time_info->c_i] : -1;
}

g_real_time *creating_real_time_info(int nper)
{
  O_p *op;
  d_s *ds;
  O_i *oi1;
  //int i, j;
  //int nx, ny;
  
  
  if (real_time_info == NULL)
    {
      real_time_info = (g_real_time*)calloc(1,sizeof(g_real_time));
      if (real_time_info == NULL) return win_printf_ptr("cannot alloc real_time info!");	

      real_time_info->im_focus_check = 16;
      real_time_info->user_real_time_action = NULL;

      real_time_info->first_rec = 0;
      real_time_info->rec_start = 0;
      real_time_info->n_avg = 16;

      // Allocate plots to check real_time
      op = pr_LIVE_VIDEO->o_p[0];
      set_op_filename(op, "Timing.gr");
      ds = op->dat[0]; // the first data set was already created
      set_ds_source(ds, "Timing rolling buffer");
      if ((ds = create_and_attach_one_ds(op, REAL_TIME_BUFFER_SIZE, REAL_TIME_BUFFER_SIZE, 0)) == NULL)
		return win_printf_ptr("I can't create plot !");
      set_ds_source(ds, "Period rolling buffer");
      if ((ds = create_and_attach_one_ds(op, REAL_TIME_BUFFER_SIZE, REAL_TIME_BUFFER_SIZE, 0)) == NULL)
		return win_printf_ptr("I can't create plot !");
      set_ds_source(ds, "Timer rolling buffer");
      op->op_idle_action = timing_rolling_buffer_idle_action;
      //      op->op_post_display = general_op_post_display;
      set_plot_x_title(op, "Time");
      set_plot_y_title(op, "{\\color{yellow} Real_Timing duration} {\\color{lightgreen} Frame duration} (ms)");
      
      /* // Plot for screening electric signal of thermal modulation */
/*       if ((op = create_and_attach_one_plot(pr_LIVE_VIDEO, REAL_TIME_BUFFER_SIZE, REAL_TIME_BUFFER_SIZE, 0)) == NULL) */
/* 	return win_printf_ptr("I can't create plot !"); */
/*       set_op_filename(op, "Thermalmodulation.gr"); */
/*       ds = op->dat[0]; // the first data set was already created */
/*       set_ds_source(ds, "Thermal modulation signal"); */
/*       op->op_idle_action = thermal_modulation_buffer_idle_action; */
/*       //set_plot_x_title(op, "Time"); */
/* 	    //set_plot_y_title(op, "{\\color{yellow} Lockining duration} {\\color{lightgreen} Frame duration} (ms)");			 */
/* 	    // create_attach_select_x_un_to_op(op, IS_SECOND, 0 */
/* 	    //		       ,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s"); */

      // plots to check phase index in phase-timer
      if ((op = create_and_attach_one_plot(pr_LIVE_VIDEO, 50*REAL_TIME_BUFFER_SIZE/3, 50*REAL_TIME_BUFFER_SIZE/3, 0)) == NULL)
	return win_printf_ptr("I can't create plot !");
      set_op_filename(op, "Lock_phase.gr");
      ds = op->dat[0]; // the first data set was already created
      set_ds_source(ds, "Modulation phase measured");
      //if ((ds = create_and_attach_one_ds(op, 50*REAL_TIME_BUFFER_SIZE/3, 50*REAL_TIME_BUFFER_SIZE/3, 0)) == NULL)
      // return win_printf_ptr("I can't create plot !");
      //set_ds_source(ds, "Modulation phase simulated");
      //if ((ds = create_and_attach_one_ds(op, 50*REAL_TIME_BUFFER_SIZE/3, 50*REAL_TIME_BUFFER_SIZE/3, 0)) == NULL)
      //return win_printf_ptr("I can't create plot !");
      //set_ds_source(ds, "Error");
      
      op->op_idle_action = phase_rolling_buffer_idle_action;
      set_plot_x_title(op, "Time");
      set_plot_y_title(op, "{\\color{yellow} Measured}{\\color{lightgreen} Simulated} phase");
      //create_attach_select_x_un_to_op(op, IS_SECOND, 0 ,1, 0, 0, "s");


       // Allocate images
      if ((oi1 = create_and_attach_movie_to_imr(imr_LIVE_VIDEO, oi_LIVE_VIDEO->im.nx, oi_LIVE_VIDEO->im.ny, IS_LINT_IMAGE, nper)) == NULL)
      	return win_printf_ptr("I can't create image !");
      /* // plot to check phase synchronisation between recording and thermal modulation */
/*       if ((op = create_and_attach_op_to_oi(oi1,real_time_info->n_avg * real_time_info->n_per,real_time_info->n_avg * real_time_info->n_per, 0, 0)) == NULL) */
/*      	 return win_printf_ptr("I can't create op!"); */
      set_oi_filename(oi1, "Im_avg.gr");
      real_time_info->oi_avg = oi1;
      oi1->oi_idle_action = oi_std_idle_action;
      map_pixel_ratio_of_image_and_screen(oi1, 1, 1);
      //set_z_black_z_white_values(oi1, 0, (float)255);
      //set_zmin_zmax_values(oi1, 0, (float)255);
      //smooth_interpol = 0;

      real_time_info->m_i = real_time_info->n_i = REAL_TIME_BUFFER_SIZE;
      real_time_info->c_i = real_time_info->ac_i = 0;
      LOCK_FUNCTION(do_at_all_image);
      LOCK_VARIABLE(real_time_info);

      ph_lock.ac_i = ph_lock.c_i = 0;
      ph_lock.n_i = 50*REAL_TIME_BUFFER_SIZE/3;
      LOCK_FUNCTION(phase_timer);
      LOCK_VARIABLE(ph_lock);
      install_int(phase_timer,1);

      bid.param = (void*)real_time_info;
      bid.to_do = NULL;
      bid.timer_do = do_at_all_image;

      add_image_treat_menu_item ( "record avg movie", NULL, real_time_avg_menu(), 0, NULL);
    }
  return real_time_info;
}


int init_live_video_info(void)
{
  int i;
  static int nper = 16;
  if (real_time_info == NULL) 
    {
      i = win_scanf("For real time treatment for averaging\n"
		    "Number of camera frame during one modulation period %8d\n",&nper);
      if (i == CANCEL) return 0;
      real_time_info = creating_real_time_info(nper);
      real_time_info->n_per = nper;

      m_action = 256;
      n_action = 0;
      action_pending = (f_action*)calloc(m_action,sizeof(f_action));
      if (action_pending == NULL) win_printf_OK("Could not allocate action_pending!");

      phaseindex_main(0, NULL);
    }
  return 0;
}


#endif
