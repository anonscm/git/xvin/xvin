/*
 * @addtogroup trackBead
 * @{
 */

#ifndef _SDI_2_BEADMENU_C_
#define _SDI_2_BEADMENU_C_

# include "allegro.h"
#ifdef XV_WIN32
# include "winalleg.h"
#endif

# include "xvin.h"
# include "fftl32n.h"
# include "fillib.h"
#include "SDI_2_bead_menu.h"

# include "SDI_2_functions.h"




/* But not below this define */
# define BUILDING_PLUGINS_DLL

#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)


MENU SDI_2_bead_menu[16]=
{
    { NULL,    NULL,   NULL,      0, NULL  }

};


MENU *prepare_SDI_2_bead_menu(b_track *bt)
{
    register int i, j;
    i = 0;
    char ch[256] = {0};
    const char *patm = NULL;
    const char *pa = NULL;
    static char pat[1024] = {0};

    if (bt != NULL)
    {
        SDI_2_bead_menu[i].text = "Reset phase";
        SDI_2_bead_menu[i].proc = do_SDI_2_initialize_phase;
        SDI_2_bead_menu[i].child = NULL;
        SDI_2_bead_menu[i].flags = 0;
        SDI_2_bead_menu[i++].dp = NULL;
        SDI_2_bead_menu[i].text = "Reset all phases";
        SDI_2_bead_menu[i].proc = do_SDI_2_initialize_phase_all;
        SDI_2_bead_menu[i].child = NULL;
        SDI_2_bead_menu[i].flags = 0;
        SDI_2_bead_menu[i++].dp = NULL;
        SDI_2_bead_menu[i].text = "Adjust bead on global maximum";
        SDI_2_bead_menu[i].proc = do_SDI_2_global_maximize;
        SDI_2_bead_menu[i].child = NULL;
        SDI_2_bead_menu[i].flags = 0;
        SDI_2_bead_menu[i++].dp = NULL;
        SDI_2_bead_menu[i].text = "Change bead tracking mode";
        SDI_2_bead_menu[i].proc = do_SDI_2_track_mode;
        SDI_2_bead_menu[i].child = NULL;
        SDI_2_bead_menu[i].flags = 0;
        SDI_2_bead_menu[i++].dp = NULL;

        if (bt->free_bead == 0)
        {
            SDI_2_bead_menu[i].text = "Set as free bead";
            SDI_2_bead_menu[i].proc = do_change_free;
            SDI_2_bead_menu[i].child = NULL;
            SDI_2_bead_menu[i].flags = 0;
        }
        else
        {
            SDI_2_bead_menu[i].text = "Set as non free bead";
            SDI_2_bead_menu[i].proc = do_change_free;
            SDI_2_bead_menu[i].child = NULL;
            SDI_2_bead_menu[i].flags = 0;
        }
        // SDI_2_bead_menu[i++].dp = NULL;
        // SDI_2_bead_menu[i].text = "Forbid move";
        // SDI_2_bead_menu[i].proc = do_SDI_2_forbid_move;
        // SDI_2_bead_menu[i].child = NULL;
        // SDI_2_bead_menu[i].flags = 0;
        // SDI_2_bead_menu[i++].dp = NULL;
        // SDI_2_bead_menu[i].text = "Reset k buffer";
        // SDI_2_bead_menu[i].proc = do_SDI_2_initialize_k_buffer;
        // SDI_2_bead_menu[i].child = NULL;
        // SDI_2_bead_menu[i].flags = 0;

      //do_bead_menu(0);
    }
    return SDI_2_bead_menu;
}

int do_SDI_2_initialize_phase_all(void)
{
  b_track *bt = NULL;
  int i;

i=win_printf("Before reseting all phases, you should be at high force in order to avoid large fluctuations. If it is not the case, you should cancel.\n");
if (i==WIN_CANCEL) return D_O_K;
if (updating_menu_state != 0)
{


    return D_O_K;
}
for (i=0;i<track_info->n_b;i++)
{
  bt = track_info->bd[i];
  SDI_2_initialize_phase(bt);
}
  return D_O_K;

}

int do_SDI_2_track_mode(void)
{
  b_track *bt = NULL;
  int tracking_mode = 0;

if (mouse_selected_bead < 0 || mouse_selected_bead >= track_info->n_b)
{
    return D_O_K;
}

if (updating_menu_state != 0)
{
    return D_O_K;
}
  bt = track_info->bd[mouse_selected_bead];

  char message[4096] = {0};
  tracking_mode = bt->do_minimize + 1;
  snprintf(message,sizeof(message),"Change tracking of bead %d \n"
"%%R -> Tracking deactivated\n"
"%%r -> Fourier tracking\n"
"%%r -> Multiplication tracking\n"
"%%r -> Barycentre tracking\n",mouse_selected_bead);

  int i = win_scanf(message,&tracking_mode);
  if (i==WIN_CANCEL) return OFF;

  bt->do_minimize = tracking_mode - 1;
  SDI_2_initialize_phase(bt);



  return D_O_K;

}


int do_SDI_2_forbid_move(void)
{
  b_track *bt = NULL;

if (mouse_selected_bead < 0 || mouse_selected_bead >= track_info->n_b)
{
    return D_O_K;
}

if (updating_menu_state != 0)
{


    return D_O_K;
}

  bt = track_info->bd[mouse_selected_bead];
  bt->moving_forbidden = 1;
  return D_O_K;

}

int do_change_free(void)
{
  b_track *bt = NULL;

if (mouse_selected_bead < 0 || mouse_selected_bead >= track_info->n_b)
{
    return D_O_K;
}

if (updating_menu_state != 0)
{


    return D_O_K;
}

  bt = track_info->bd[mouse_selected_bead];
  bt->free_bead=1-bt->free_bead;


  return D_O_K;

}

int do_SDI_2_global_maximize(void)
{
  b_track *bt = NULL;

if (mouse_selected_bead < 0 || mouse_selected_bead >= track_info->n_b)
{
    return D_O_K;
}

if (updating_menu_state != 0)
{


    return D_O_K;
}

  bt = track_info->bd[mouse_selected_bead];
  if (bt->do_minimize == 0) return D_O_K;
  bt->frange_top->buffer_full = 0;
  bt->frange_bottom->buffer_full = 0;

  return D_O_K;

}

int do_SDI_2_initialize_phase(void)
{
  b_track *bt = NULL;

if (mouse_selected_bead < 0 || mouse_selected_bead >= track_info->n_b)
{
    return D_O_K;
}

if (updating_menu_state != 0)
{


    return D_O_K;
}

  bt = track_info->bd[mouse_selected_bead];
  SDI_2_initialize_phase(bt);
  return D_O_K;

}

// int do_SDI_2_initialize_k_buffer(void)
// {
//   b_track *bt = NULL;
//
// if (mouse_selected_bead < 0 || mouse_selected_bead >= track_info->n_b)
// {
//     return D_O_K;
// }
//
// if (updating_menu_state != 0)
// {
//
//
//     return D_O_K;
// }
//
//   bt = track_info->bd[mouse_selected_bead];
//   //SDI_2_initialize_k_buffer(bt);
//   return D_O_K;
//
// }


#endif
