#include "xvin.h"
#include <allegro.h>
#ifdef XV_WIN32
#   include <winalleg.h>
#endif
#include "record.h"
#include "indexing.h"
namespace indexing
{
    namespace
    {
        template <int I>
        opt_t<int> _plotids(O_p const *op)
        {
            int ids[] = {-1, -1, -1};
            if (op == nullptr || op->filename == nullptr)
                return {};
            if(     sscanf(op->filename, CYCLE_PLOT_FILENAME,       ids+1, ids)        == 2
               ||   sscanf(op->filename, CYCLE_PHASE_PLOT_FILENAME, ids+1, ids, ids+2) == 3
               ||   sscanf(op->filename, HYB_PATTERN_PLOT_FILENAME, ids+1, ids, ids+2) == 3)
                return ids[I];
            return {};
        };

    }
    opt_t<int>  cycleid(data_set const *ds)
    {
        int id = -1;
        if (   ds         != nullptr
            && ds->source != nullptr
            && sscanf(ds->source, CYCLE_DATASET_SOURCE, &id) == 1
           )
            return id;
        return {};
    };

    opt_t<int> beadid (std::regex pattern, O_p const *op)
    {
        std::smatch val;
        std::string fname = op->filename;
        if(std::regex_match(fname, val, pattern) && val.size() == 2)
            try { return std::stoi(val[1].str()); } catch (...) {}

        fname = op->title;
        if(std::regex_match(fname, val, pattern) && val.size() == 2)
            try { return std::stoi(val[1].str()); } catch (...) {}

        return {};
    }

    opt_t<int> trackid(O_p const *op) { return _plotids<0>(op); }
    opt_t<int> beadid (O_p const *op) { return _plotids<1>(op); }
    opt_t<int> phaseid(O_p const *op) { return _plotids<2>(op); }

    namespace
    {
        template <typename T>
        bool current(T *&pr, char const * x)
        {
            pr = nullptr;
            char tmp[512];
            strcpy(tmp, x);
            return ac_grep(cur_ac_reg,tmp,&pr) == 1;
        }

        template <typename T>
        T * current(char const *x)
        {
            T * pr = nullptr;
            if (!current(pr, x))
            {
                win_printf_OK("cannot find data");
                return nullptr;
            }
            return pr;
        }
    }
    bool        current(pltreg   *&pr) { return current(pr, "%pr"); }
    bool        current(one_plot *&pr) { return current(pr, "%op"); }
    bool        current(data_set *&pr) { return current(pr, "%ds"); }
    pltreg*     currentpltreg()        { return current<pltreg>("%pr"); }
    one_plot*   currentplot  ()        { return current<one_plot>("%op"); }
    data_set*   currentds    ()        { return current<data_set>("%ds"); }

    gen_record const * record()        {   return record(currentpltreg()); }
    gen_record const * record(plot_region const *plt)
    {
        if(plt == nullptr)
            plt = currentpltreg();
        return find_g_record_in_pltreg(const_cast<plot_region*>(plt));
    }

    gen_record       * record(plot_region       *plt)
    {
        if(plt == nullptr)
            plt = currentpltreg();
        return find_g_record_in_pltreg(plt);
    }

    bool belongs(gen_record const *gr, O_p const * op)
    {
        if(gr == nullptr || op == nullptr)
            return false;
        auto id  = indexing::beadid(op);
        if(!id || (*id) >= gr->n_bead)
            return false;
        id = indexing::trackid(op);
        if(!id || (*id) != gr->n_rec)
            return false;
        return true;
    }
    bool        belongs (pltreg const *plt, O_p const * op) { return belongs(record(plt), op); }

    std::string filename(gen_record  const * gr)            { return gr->filename; }
    std::string filename()
    { return filename((plot_region const*) 0); }

    std::string filename(plot_region const * plt)
    {
        if(plt == nullptr)
            plt = currentpltreg();
        if(plt == nullptr)
            return "";
        return filename(record(plt));
    }

    std::string filename(one_plot const * plt)
    {
        if(plt == nullptr)
            plt = currentplot();
        if(plt == nullptr || plt->filename == nullptr)
            return "";
        return plt->filename;
    }

    opt_t<std::pair<int, int>> phaserange(data_set const * ds, int phase)
    {   
        auto cid = cycleid(ds);
        if(cid)
            return phaserange(*cid, phase); 
        else
            return {};
    }

    opt_t<std::pair<int, int>> phaserange(int cid, int phase)
    {   return phaserange(record(currentpltreg()), cid, phase); }

    opt_t<std::pair<int, int>> phaserange(gen_record const * cgt, int cid, int phase)
    {
        if(cgt == nullptr)
            cgt = record(currentpltreg());
        if(cgt == nullptr)
            return {};


        int inds[2] = { -1, -1};
        auto gt = const_cast<gen_record*>(cgt);
        if (retrieve_image_index_of_next_point(gt, cid, inds, inds+1) < 0)
            return {};

        int  _   = 0;
        auto ref = inds[0];
        if(retrieve_image_index_of_next_point_phase(gt, cid, phase, inds, inds+1, &_) < 0)
            return {};

        return std::make_pair(inds[0]-ref, inds[1]-ref);
    };

    opt_t<int> phasestart(gen_record const * cgt, int cid, int phase)
    {
        if(cgt == nullptr)
            cgt = record(currentpltreg());
        if(cgt == nullptr)
            return {};

        int inds[2] = { -1, -1};
        auto gt = const_cast<gen_record*>(cgt);
        if (retrieve_image_index_of_next_point(gt, cid, inds, inds+1) < 0)
            return {};

        int  _   = 0;
        if(retrieve_image_index_of_next_point_phase(gt, cid, phase, inds, inds+1, &_) < 0)
            return {};

        return inds[0];
    };

    opt_t<std::pair<int, int>> phaserange(plot_region const * plt, int cid, int phase)
    { return phaserange(record(plt), cid, phase); }

    PhaseRange::PhaseRange(gen_record const *g, int p, size_t sz)
        : base_t(sz, std::make_pair(std::numeric_limits<int>::max(), 0))
        , _gen  (g)
        , _phase(p)
    {}

    PhaseRange::PhaseRange(plot_region const *g, int p, size_t s)
        : PhaseRange(record(g), p , s)
    {}

    void PhaseRange::setup(plot_region const * g, int p, size_t sz)
    {   _gen = record(g); _phase = p; resize(0); resize(sz); }

    void PhaseRange::setup(gen_record  const * g, int p, size_t sz)
    {   _gen = g; _phase = p; resize(0); resize(sz); }

    PhaseRange::value_type PhaseRange::operator[](int cid)
    {
        constexpr std::pair<int,int> const miss = { std::numeric_limits<int>::max(), 0 };
        if(int(size()) < cid)
           resize(cid+1, miss);

        auto & cur = base_t::operator[](cid);
        if(cur == miss)
            cur = phaserange(_gen, cid, _phase);
        return cur;
    }

    std::set<int> cycles(plot_region const * curr)
    {
        if(curr == nullptr)
            curr = currentpltreg();
        if(curr == nullptr)
            return {};
        std::set<int> res;
        for(auto op = curr->o_p, oe = op+curr->n_op; op != oe; ++op)
        {
            if(op[0] == nullptr)
                continue;
            for(auto ds = op[0]->dat, de = ds+op[0]->n_dat; ds != de; ++ds)
            {
                if(ds[0] == nullptr || ds[0]->source == nullptr)
                    continue;
                int id = -1;
                if(sscanf(ds[0]->source, CYCLE_DATASET_SOURCE, &id) != 1)
                    continue;
                res.insert(id);
            }
        }

        return res;
    }

    std::map<int, cycleds_t> mapbycycle(plot_region * curr)
    {
        if(curr == nullptr)
            curr = currentpltreg();
        if(curr == nullptr)
            return {};

        decltype(mapbycycle(nullptr)) res;
        for(auto op = curr->o_p, oe = op+curr->n_op; op != oe; ++op)
        {
            if(op[0] == nullptr)
                continue;
            for(auto ds = op[0]->dat, de = ds+op[0]->n_dat; ds != de; ++ds)
            {
                auto id = cycleid(ds[0]);
                if(!id)
                    continue;

                res[id.value()].emplace_back(op[0], ds[0]);
            }
        }
        return res;
    }

    cycleds_t selectcycle(plot_region * curr, int cycle)
    {
        if(curr == nullptr)
            curr = currentpltreg();
        if(curr == nullptr)
            return {};

        decltype(selectcycle(nullptr, 0)) res;
        res.reserve(curr->n_op);
        for(auto op = curr->o_p, oe = op+curr->n_op; op != oe; ++op)
        {
            if(op[0] == nullptr)
                continue;
            for(auto ds = op[0]->dat, de = ds+op[0]->n_dat; ds != de; ++ds)
            {
                auto id = cycleid(ds[0]);
                if(id && id.value() == cycle)
                    res.emplace_back(op[0], ds[0]);
            }
        }
        return res;
    }

    std::vector<one_plot*>  plots(plot_region * plt, std::regex pattern)
    { return plots(pattern, plt); }

    std::vector<one_plot*>  plots(std::regex pattern, plot_region * plt)
    {
        if(plt == nullptr)
            plt = currentpltreg();
        if(plt == nullptr)
            return {};

        decltype(plots()) res;
        for(int i = 0; i < plt->n_op; ++i)
        {
            auto id = indexing::beadid(pattern, plt->o_p[i]);
            if(!id)
                continue;
            res.push_back(plt->o_p[i]);
        }
        return res;
    }

    std::vector<int>  plotindexes(plot_region * plt, std::regex pattern)
    { return plotindexes(pattern, plt); }

    std::vector<int>  plotindexes(std::regex pattern, plot_region * plt)
    {
        if(plt == nullptr)
            plt = currentpltreg();
        if(plt == nullptr)
            return {};

        decltype(plotindexes()) res;
        for(int i = 0; i < plt->n_op; ++i)
        {
            auto id = indexing::beadid(pattern, plt->o_p[i]);
            if(!id)
                continue;
            res.push_back(i);
        }
        return res;
    }

    int nphases  (gen_record const * gr)
    {
        if(gr == nullptr)
            gr = record(currentpltreg());
        if(gr == nullptr || gr->abs_pos < 0)
            return 0;
        int lmin = 0, lmax = 0, lphase = 0;
        retrieve_min_max_event_and_phases(const_cast<gen_record*>(gr), &lmin, &lmax, &lphase);
        return lphase;
    }

    int cyclemin    (gen_record  const * gr)
    {
        if(gr == nullptr)
            gr = record(currentpltreg());
        if(gr == nullptr || gr->abs_pos < 0)
            return 0;
        int lmin = 0, lmax = 0, lphase = 0;
        retrieve_min_max_event_and_phases(const_cast<gen_record*>(gr), &lmin, &lmax, &lphase);
        return lmin;
    }

    int  cyclemax    (gen_record  const *gr)
    {
        if(gr == nullptr)
            gr = record(currentpltreg());
        if(gr == nullptr || gr->abs_pos < 0)
            return 0;
        int lmin = 0, lmax = 0, lphase = 0;
        retrieve_min_max_event_and_phases(const_cast<gen_record*>(gr), &lmin, &lmax, &lphase);
        return lmax;
    }

    int  ncycles  (gen_record  const *gr)
    {
        if(gr == nullptr)
            gr = record(currentpltreg());
        if(gr == nullptr || gr->abs_pos < 0)
            return 0;
        int lmin = 0, lmax = 0, lphase = 0;
        retrieve_min_max_event_and_phases(const_cast<gen_record*>(gr), &lmin, &lmax, &lphase);
        return lmax-lmin+1;
    }
}
