/***********************************************************************plug-ins-src/trackBead/uEye_plug-ins-src/trackBead/uEye_plug-ins-src/trackBead/uEye_source.c
  LPS ENS
  --------------------------------------------------------------------------------

Program      : UEYE Genicam GigE_Source for bead tracking
Author       : JFA
Date         : 27.10.2007

Purpose      : UEYE Manager Functions for XVin
CVB          :
Hints        :

Updates      :
 ********************************************************************************/
#ifndef _UEYE_SOURCE_C_
#define _UEYE_SOURCE_C_

# include "uEye_source.h"

# include "../cfg_file/Pico_cfg.h"

MENU *UEYE_image_menu(void);
int do_set_camera_max_freq(void);

MENU *UEYE_source_image_menu(void);
int _get_camera_shutter_in_us(int *set, int *smin, int *smax);
int _set_camera_shutter_in_us(int exp, int *set, int *smin, int *smax);
int _get_camera_gain(float *gain, float *fmin, float *fmax, float *finc);
int _set_camera_gain(float gain, float *set, float *fmin, float *fmax, float *finc);

int UEYE_oi_idle_action(struct  one_image *oi, DIALOG *d)
{
    (void) oi;
    (void) d;
    return D_O_K;
}

/*
   int oi_data_length(O_i* oi)
   {
   int data_len = 1;

   if (oi == NULL) return 1;

   switch (oi->im.data_type)
   {
   case IS_CHAR_IMAGE:          data_len = 1;       break;
   case IS_RGB_PICTURE:     data_len = 3;       break;
   case IS_RGBA_PICTURE:        data_len = 4;       break;
   case IS_RGB16_PICTURE:       data_len = 6;       break;
   case IS_RGBA16_PICTURE:      data_len = 8;       break;
   case IS_INT_IMAGE:           data_len = 2;       break;
   case IS_UINT_IMAGE:          data_len = 2;       break;
   case IS_LINT_IMAGE:          data_len = 4;       break;
   case IS_FLOAT_IMAGE:     data_len = 4;       break;
   case IS_COMPLEX_IMAGE:       data_len = 8;       break;
   case IS_DOUBLE_IMAGE:        data_len = 8;       break;
   case IS_COMPLEX_DOUBLE_IMAGE:    data_len = 16;      break;
   default:                         data_len = 1;       break;
   };
   return data_len;
   }


   int set_oi_to_uEye_pointers(O_i* oi, void* data_pointer, int nx, int ny)
   {

   register int i = 0;
   int data_len = 1, nf=0;
   void *buf = NULL;
   int type = -1;

   if (oi == NULL) return win_printf_OK("Oi NULL");
   type = oi->im.data_type ;

   data_len = oi_data_length(oi);

   nf = (oi->im.n_f > 0) ? oi->im.n_f : 1;
   oi->im.ny = ny;
   oi->im.nx = nx;
   oi->im.pixel = (union pix *)realloc(oi->im.pixel, ny*nf * sizeof(union pix));
   if (oi->im.pixel == NULL) return xvin_error(Out_Of_Memory);
   if (nf > 1)
   {
   oi->im.pxl = (union pix **)realloc(oi->im.pxl, nf*sizeof(union pix*));
   oi->im.mem = (void **)realloc(oi->im.mem, nf*sizeof(void*));
   if (oi->im.pxl == NULL  || oi->im.mem == NULL) return xvin_error(Out_Of_Memory);
   }
   else oi->im.mem = (void **)realloc(oi->im.mem, sizeof(void*));
   oi->im.m_f = nf;
   buf = data_pointer; // already allocated by uEye
   if (oi->im.pixel == NULL || buf == NULL) return xvin_error(Out_Of_Memory);
   oi->im.mem[0] = buf;
   for (i=0 ; i< ny*nf ; i++)
   {
   switch (type)
   {
   case IS_CHAR_IMAGE:
   oi->im.pixel[i].ch = (unsigned char*)buf;

   break;
   case IS_RGB_PICTURE:
   oi->im.pixel[i].ch = (unsigned char*)buf;

   break;
   case IS_RGBA_PICTURE:
   oi->im.pixel[i].ch = (unsigned char*)buf;

case IS_RGB16_PICTURE:
oi->im.pixel[i].in = (short int*)buf;

break;
case IS_RGBA16_PICTURE:
oi->im.pixel[i].in = (short int*)buf;

break;
case IS_INT_IMAGE:
oi->im.pixel[i].in = (short int *)buf;

break;
case IS_UINT_IMAGE:
oi->im.pixel[i].ui = (unsigned short int *)buf;

break;
case IS_LINT_IMAGE:
oi->im.pixel[i].li = (int *)buf;

break;
case IS_FLOAT_IMAGE:
oi->im.pixel[i].fl = (float *)buf;

break;
case IS_COMPLEX_IMAGE:
oi->im.pixel[i].fl = (float *)buf;

break;
case IS_DOUBLE_IMAGE:
oi->im.pixel[i].db = (double *)buf;

break;
case IS_COMPLEX_DOUBLE_IMAGE:
oi->im.pixel[i].db = (double *)buf;

break;

};
if ( (i % ny == 0) && (oi->im.n_f > 0))
{
    oi->im.pxl[i/ny] = oi->im.pixel + i;
    oi->im.mem[i/ny] = (void*)buf;
}
buf += nx * data_len;
}
oi->im.nxs = oi->im.nys = 0;
oi->im.nxe = oi->im.nx = nx;
oi->im.nye = oi->im.ny = ny;

if (type == IS_COMPLEX_IMAGE  || type == IS_COMPLEX_DOUBLE_IMAGE)
    oi->im.mode = RE;
    oi->need_to_refresh |= ALL_NEED_REFRESH;
    return D_O_K;
    }


int attach_uEye_pointer_to_movie(O_i* oi, void* data_pointer, int nx, int ny, int fr)
{

    register int i = 0;
    int data_len = 1, nf=0;
    void *buf = NULL;
    int type = -1;

    if (oi == NULL) return win_printf_OK("Oi NULL");
    type = oi->im.data_type ;

    data_len = oi_data_length(oi);

    nf = (oi->im.n_f > 0) ? oi->im.n_f : 1;
    oi->im.ny = ny;
    oi->im.nx = nx;
    if (oi->im.pixel == NULL)
    {
        oi->im.pixel = (union pix *)realloc(oi->im.pixel, ny*nf * sizeof(union pix));
        if (oi->im.pixel == NULL) return xvin_error(Out_Of_Memory);
        if (nf > 1)
        {
            oi->im.pxl = (union pix **)realloc(oi->im.pxl, nf*sizeof(union pix*));
            oi->im.mem = (void **)realloc(oi->im.mem, nf*sizeof(void*));
            if (oi->im.pxl == NULL  || oi->im.mem == NULL) return xvin_error(Out_Of_Memory);
        }
        else oi->im.mem = (void **)realloc(oi->im.mem, sizeof(void*));
        oi->im.m_f = nf;
    }
    buf = data_pointer; // already allocated by uEye
    if (oi->im.pixel == NULL || buf == NULL)    return xvin_error(Out_Of_Memory);

    if (fr >= nf) return win_printf_OK("frame out of range");


    oi->im.mem[fr] = buf;
    for (i=fr*ny ; i< ny*(fr+1) ; i++)
    {
        switch (type)
        {
            case IS_CHAR_IMAGE:
                oi->im.pixel[i].ch = (unsigned char*)buf;

                break;
            case IS_RGB_PICTURE:
                oi->im.pixel[i].ch = (unsigned char*)buf;

                break;
            case IS_RGBA_PICTURE:
                oi->im.pixel[i].ch = (unsigned char*)buf;

            case IS_RGB16_PICTURE:
                oi->im.pixel[i].in = (short int*)buf;

                break;
            case IS_RGBA16_PICTURE:
                oi->im.pixel[i].in = (short int*)buf;

                break;
            case IS_INT_IMAGE:
                oi->im.pixel[i].in = (short int *)buf;

                break;
            case IS_UINT_IMAGE:
                oi->im.pixel[i].ui = (unsigned short int *)buf;

                break;
            case IS_LINT_IMAGE:
                oi->im.pixel[i].li = (int *)buf;

                break;
            case IS_FLOAT_IMAGE:
                oi->im.pixel[i].fl = (float *)buf;

                break;
            case IS_COMPLEX_IMAGE:
                oi->im.pixel[i].fl = (float *)buf;

                break;
            case IS_DOUBLE_IMAGE:
                oi->im.pixel[i].db = (double *)buf;

                break;
            case IS_COMPLEX_DOUBLE_IMAGE:
                oi->im.pixel[i].db = (double *)buf;

                break;

        };
        if ( (i % ny == 0) && (oi->im.n_f > 0))
        {
            oi->im.pxl[i/ny] = oi->im.pixel + i;
            oi->im.mem[i/ny] = (void*)buf;
        }
        buf += nx * data_len;
    }
    oi->im.nxs = oi->im.nys = 0;
    oi->im.nxe = oi->im.nx = nx;
    oi->im.nye = oi->im.ny = ny;

    if (type == IS_COMPLEX_IMAGE  || type == IS_COMPLEX_DOUBLE_IMAGE)
        oi->im.mode = RE;
    oi->need_to_refresh |= ALL_NEED_REFRESH;
    return D_O_K;
}

*/


# ifdef TRACK_WIN_THREAD
DWORD WINAPI TrackingThreadProc(LPVOID lpParam)
# else
void *TrackingThreadProc(void *lpParam)
# endif

{
    tid *ltid;
    INT ids_err = 0;
    IS_CHAR *ids_err_msg = NULL;
#ifdef XV_WIN32
    DWORD   iWaitResult;
    HANDLE hEventNewImagel = NULL;
    HANDLE  EventListl[4];
#else
    INT lastEventReturn = IS_SUCCESS;
#endif
    int first = 1;
    Bid *p = NULL;
    long long t, dt;//, lTimeStamp, llt;
    int i = 0, im_n = 0, j, fr_delivered, prev_fr_delivered = 0, i_todo; // , *pI
    //void *pBuffer;
    int NumberFramesDelivered, state = 0;
    long int ret;
    char message[1024], *buf;
    double fps;

    if (imr_TRACK == NULL || oi_TRACK == NULL)  return (void *)1;


#ifdef XV_WIN32

    if (hEventFreeze != NULL)    CloseHandle(hEventFreeze);

    // Create a new one
    hEventFreeze = CreateEvent(NULL, TRUE, FALSE, NULL);
    hEventNewImagel = CreateEvent(NULL, TRUE, FALSE, NULL);
    hEventRemoved = CreateEvent(NULL, TRUE, FALSE, NULL);
    hEventReplug = CreateEvent(NULL, TRUE, FALSE, NULL);

    // Make a list of events to be used in WaitForMultipleObjects
    EventListl[0] = hEventNewImagel;
    EventListl[1] = hEventFreeze;
    EventListl[2] = hEventRemoved;
    EventListl[3] = hEventReplug;

    ret = is_InitEvent(m_hCam, hEventNewImagel, IS_SET_EVENT_FRAME);

    if (ret != IS_SUCCESS)  return win_printf_ptr("could not init event!");

    ret = is_InitEvent(m_hCam, hEventRemoved, IS_SET_EVENT_REMOVE);

    if (ret != IS_SUCCESS)  return win_printf_ptr("could not init event!");

    ret = is_InitEvent(m_hCam, hEventReplug, IS_SET_EVENT_DEVICE_RECONNECTED);

    if (ret != IS_SUCCESS)  return win_printf_ptr("could not init event!");

#endif

    ret = is_EnableEvent(m_hCam, IS_SET_EVENT_FRAME);

    if (ret != IS_SUCCESS)  return (void *)win_printf_ptr("could not enable event!");

    ret = is_EnableEvent(m_hCam, IS_SET_EVENT_REMOVE);

    if (ret != IS_SUCCESS)  return (void *)win_printf_ptr("could not enable event!");

    ret = is_EnableEvent(m_hCam, IS_SET_EVENT_DEVICE_RECONNECTED);

    if (ret != IS_SUCCESS)  return (void *)win_printf_ptr("could not enable event!");

    //win_printf("before capture");
    is_CaptureVideo(m_hCam, 100);
    ltid = (tid *)lpParam;
    p = ltid->dbid;
    NumberFramesDelivered = 0;

    while (go_track)
    {
#ifdef XV_WIN32
        // Wait for New Buffer event (or Freeze event)
        iWaitResult = WaitForMultipleObjects(4, EventListl, FALSE, 10000/*00*/);

        if (iWaitResult == WAIT_OBJECT_0)
#else
            lastEventReturn = is_WaitEvent(m_hCam, IS_SET_EVENT_FRAME, 10000);

        if (lastEventReturn == IS_SUCCESS)
#endif
        {
            //printf("%u\n",pthread_self());
            is_GetActiveImageMem(m_hCam, &buf, &fr_delivered);
            fr_delivered -= 2;
            fr_delivered = (fr_delivered < 0) ? oi_TRACK->im.n_f + fr_delivered : fr_delivered;

            if (fr_delivered >= iImages_in_UEYE_Buffer)   my_set_window_title("out of range");

            if (first)
                prev_fr_delivered = (fr_delivered + iImages_in_UEYE_Buffer - 1) % iImages_in_UEYE_Buffer;

            for (i_todo = 0, j = prev_fr_delivered; j != fr_delivered; i_todo++)
            {
                j++;
                j = (j >= iImages_in_UEYE_Buffer) ? 0 : j;
            }

            //if (i_todo > 1)    my_set_window_title("Missed fr %d at %d", i, NumberFramesDelivered);

            if (p->first_im == 0)
                p->first_im = NumberFramesDelivered;

            t = my_ulclock();
            dt = my_uclock();
            p->image_per_in_ms = 1000 * (double)(t - p->previous_in_time_fr) / get_my_ulclocks_per_sec();

            if (first == 0) Pico_param.camera_param.camera_frequency_in_Hz = (float)1000 / p->image_per_in_ms;

            if (first == 0 && fr_delivered % 1024)
            {
                ret = is_SetFrameRate(m_hCam, IS_GET_FRAMERATE, &fps);

                if (ret != IS_SUCCESS)       win_printf("Could not get camera rate");
                else
		  {
		    Pico_param.camera_param.camera_frequency_in_Hz = fps;
		    //change_Freq_string(fps);
		  }
            }

            p->previous_in_time_fr = t;
            p->timer_dt = 0;
            p->previous_fr_nb = NumberFramesDelivered;
            j = prev_fr_delivered;

            //j = (j >= iImages_in_UEYE_Buffer) ? 0 : j;
            if (first)    first = 0;

            for (i = 0; i < i_todo ; im_n++, i++, j++, NumberFramesDelivered++)
            {
                //is_GetVsyncCount();
                switch_frame(oi_TRACK, j);
                t = my_ulclock();
                dt = my_uclock();
                p->previous_fr_nb = im_n = NumberFramesDelivered;

                do
                {
                    p->timer_do(imr_TRACK, oi_TRACK, im_n, d_TRACK, t, dt, p->param,  i_todo - i - 1);
                    p = p->next;
                }
                while (p && p->timer_do != NULL);

                p = ltid->dbid;
                //lTimeStamp = llt;
#ifdef XV_WIN32

                if (rec_AVI)
                {
                    isavi_AddFrame(nAviID, oi_TRACK->im.mem[j]);
                }

#endif
            }

            prev_fr_delivered = fr_delivered;
#ifdef XV_WIN32
            ResetEvent(hEventNewImagel);  // needed to get one event per frame
#endif

            /*
               if (state == 0)
               my_set_window_title("im id %d freq %g",
               fr_delivered, Pico_param.camera_param.camera_frequency_in_Hz);
               else
               */
            if (state != 0)  my_set_window_title("camera removed im id %d freq %g",
                                                 fr_delivered, Pico_param.camera_param.camera_frequency_in_Hz);
        }

#ifdef XV_WIN32
        else if (iWaitResult == WAIT_TIMEOUT)
#else
        else if (lastEventReturn == IS_TIMED_OUT)
#endif
        {
            ret = 2;
            win_printf("10s Timeout");
            bEnableThread = FALSE;

        }
        else
        {
            is_GetError(m_hCam, &ids_err, &ids_err_msg);
#ifdef XV_WIN32
            warning_message("ids: err: %d, msg: %s, last event return: %ld\n", ids_err, ids_err_msg, iWaitResult);
#else
            warning_message("ids: err: %d, msg: %s, last event return: %d\n", ids_err, ids_err_msg, lastEventReturn);
#endif

#ifdef XV_WIN32

            switch (iWaitResult)
            {
                case WAIT_OBJECT_0 + 2:  // camera removed
                    state = 1;
                    break;

                case WAIT_OBJECT_0 + 3:  // camera reconnected
                    state = 0;
                    break;

                    // Freeze event
                case    WAIT_OBJECT_0 + 1:
                    ret = 1;
                    bEnableThread = FALSE;
                    break;

                default:
                    ret = 3;
                    break;
            }

#else
            if (is_WaitEvent(m_hCam, IS_SET_EVENT_REMOVE, 100) == IS_SUCCESS)
            {
                state = 1;
            }
            else if (is_WaitEvent(m_hCam, IS_SET_EVENT_DEVICE_RECONNECTED, 100) == IS_SUCCESS)
            {
                state = 0;
            }
            else
            {
                ret = 3;
            }

#endif
        }
    }

    //win_printf("exiting thread loop");
    strncat(message, "- out loop", (sizeof(message) > strlen(message)) ? sizeof(message) - strlen(message) : 0);
    my_set_window_title("%s",message);

    strncat(message, "- closing camera", (sizeof(message) > strlen(message)) ? sizeof(message) - strlen(message) : 0);
    my_set_window_title("%s",message);

    is_StopLiveVideo(m_hCam, 100);  // IS_WAIT
    is_ClearSequence(m_hCam);
    is_ExitCamera(m_hCam);

    //Terminate_UEYE_Thread();     // Terminate the thread.

#ifdef XV_WIN32

    // Free the kill event
    if (hEventFreeze != NULL)
    {
        CloseHandle(hEventFreeze);
        hEventFreeze = NULL;
    }

    // Free the new image event object
    if (hEventNewImagel != NULL)
    {
        CloseHandle(hEventNewImagel);
        hEventNewImagel = NULL;
    }

#endif
    //WaitForThreadToTerminate();
    strncat(message, "- Out thread", (sizeof(message) > strlen(message)) ? sizeof(message) - strlen(message) : 0);
    my_set_window_title("%s",message);
    return (void *) ret;
}



HIDS hCam_Open_camera(void)
{
  PUEYE_CAMERA_LIST pucl =  (PUEYE_CAMERA_LIST) calloc(1,sizeof (DWORD) + 10 * sizeof (UEYE_CAMERA_INFO));
  is_GetCameraList (pucl);
  int ret;
  m_hCam = (HIDS) 0;
//  win_printf("found %d cameras\n ",pucl->dwCount);
  for (int iCamera = 0; iCamera < (int) pucl->dwCount; iCamera++){
  //  win_printf("trying to open camera with id %d\n",pucl->uci[iCamera].dwSensorID);
    m_hCam = (HIDS) pucl->uci[iCamera].dwDeviceID;
  ret = is_InitCamera(&m_hCam,NULL);
  if (ret == IS_SUCCESS) break;
//  win_printf("failed to open %d among %d\n",pucl->uci[iCamera].dwCameraID,pucl->dwCount);
}

if (ret != IS_SUCCESS)
  {
      win_printf("Could not open IDS camera");
      return 0;
  }

  is_GetSensorInfo(m_hCam, &sInfo);
  Pico_param.camera_param.nb_pxl_x = sInfo.nMaxWidth;
  Pico_param.camera_param.nb_pxl_y = sInfo.nMaxHeight;

  if (Pico_param.camera_param.camera_manufacturer)
      free(Pico_param.camera_param.camera_manufacturer);

  Pico_param.camera_param.camera_manufacturer = strdup("IDS-uEye");

  if (Pico_param.camera_param.camera_model)
      free(Pico_param.camera_param.camera_model);

  Pico_param.camera_param.camera_model = strdup(sInfo.strSensorName);


  if (sInfo.wPixelSize > 0)
    Pico_param.camera_param.pixel_w_in_microns = Pico_param.camera_param.pixel_h_in_microns = (float)sInfo.wPixelSize/100;
  return m_hCam;
}




int add_oi_UEYE_2_imr(imreg *imr, O_i *oi)
{

    double fps = 0, nfps;
    float ffps;
    int pixelclock = 0;
    int i;
    int x0, y0, width, height;
    int m_lMemoryId = 0, ret = 0;
    int nb_bits = Pico_param.camera_param.nb_bits;
    unsigned char *buf;
# ifdef SDK_4_0_0
    IS_RECT rectAOI;
# endif
# ifdef SDK_4_2_1
    IS_RECT rectAOI;
# endif
# ifdef SDK_4_2_2
    IS_RECT rectAOI;
# endif
# ifdef SDK_4_3_0
    IS_RECT rectAOI;
# endif
# ifdef SDK_4_6_0
    IS_RECT rectAOI;
# endif
# ifdef SDK_4_6_1
    IS_RECT rectAOI;
# endif
# ifdef SDK_4_7_0
    IS_RECT rectAOI;
# endif
# ifdef SDK_4_7_2
    IS_RECT rectAOI;
# endif
# ifdef SDK_4_8_0
    IS_RECT rectAOI;
# endif
# ifdef SDK_4_9_0
    IS_RECT rectAOI;
# endif
# ifdef SDK_4_9_1
    IS_RECT rectAOI;
# endif

    (void) oi;
    if (imr == NULL) return win_printf_OK("IMR NULL");

    //ret = is_SetFrameRate(m_hCam, IS_GET_FRAMERATE, &fps);

    //if (ret != IS_SUCCESS)       win_printf("Could not get camera rate");
    //else
    //

    fps = Pico_param.camera_param.camera_frequency_in_Hz;
    pixelclock = Pico_param.camera_param.pixel_clock;
    ffps = fps;
    x0 = 0;
    y0 = 0;
    width = sInfo.nMaxWidth;
    height = sInfo.nMaxHeight;

# if defined(SDK_3_8_0) || defined(SDK_3_5_0)
    ret = is_SetAOI(m_hCam, IS_GET_IMAGE_AOI, &x0, &y0, &width, &height);
# endif
# if defined(SDK_4_0_0) || defined(SDK_4_2_1) || defined(SDK_4_2_2) || defined(SDK_4_3_0) || defined(SDK_4_6_0) || defined(SDK_4_6_1) || defined(SDK_4_7_0) || defined(SDK_4_7_2) || defined(SDK_4_8_0) || defined(SDK_4_9_0) || defined(SDK_4_9_1)
    is_AOI(m_hCam, IS_AOI_IMAGE_GET_AOI, (void *)&rectAOI, sizeof(rectAOI));
    x0 = rectAOI.s32X;
    y0 = rectAOI.s32Y;
    width = rectAOI.s32Width;
    height = rectAOI.s32Height;
# endif

    if (ret != IS_SUCCESS)       win_printf("Could not get AOI");

    i = win_scanf("Your camera supports small AOI to increase acquisition speed\n"
                  "while reducing the field of view. The width and height can only\n"
                  "be set at starting time, position may be moved during acquisition\n"
                  "Define X0 %8d and width %8d\n"
                  "Define Y0 %8d height %8d\n"
                  "You can select the number of frames in the image rolling buffer\n"
                  "This number should correspond a a feww seconds of video\n"
                  "Nb. of frames in buffer %8d\n"
                  "Camera frame rate %8f\n"
                  "pixel clock %5d\n"
                  "Bit number %5d\n"
                  , &x0, &width, &y0, &height, &iImages_in_UEYE_Buffer, &ffps, &pixelclock,&nb_bits);

    if (i == WIN_CANCEL) return OFF;

    i = 4 * (width / 4);
    //if (i != width)
    //{   // we adjust size to a multiple of 4
    width = i;
# if defined(SDK_3_8_0) || defined(SDK_3_5_0)
    ret = is_SetAOI(m_hCam, IS_SET_IMAGE_AOI, &x0, &y0, &width, &height);

    if (ret != IS_SUCCESS)       win_printf("Could not set AOI");

# endif
# if defined(SDK_4_0_0) || defined(SDK_4_2_1) || defined(SDK_4_2_2) || defined(SDK_4_3_0) || defined(SDK_4_6_0) || defined(SDK_4_6_1) || defined(SDK_4_7_0) || defined(SDK_4_7_2) || defined(SDK_4_8_0) || defined(SDK_4_9_0) || defined(SDK_4_9_1)
    rectAOI.s32X     = x0;
    rectAOI.s32Y     = y0;
    rectAOI.s32Width = width;
    rectAOI.s32Height = height;
    is_AOI(m_hCam, IS_AOI_IMAGE_SET_AOI, (void *)&rectAOI, sizeof(rectAOI));
# endif

    // }
    //create image

    //win_printf("w %d h %d",width,height);
    Pico_param.camera_param.nb_pxl_x = width;
    Pico_param.camera_param.nb_pxl_y = height;

    oi_TRACK = create_and_attach_continuous_movie_to_imr(imr, width, height, nb_bits == 8 ? IS_CHAR_IMAGE : IS_INT_IMAGE, iImages_in_UEYE_Buffer);

    if (oi_TRACK == NULL)     return win_printf_OK("Can't create dest image");


    LOCK_DATA(oi_TRACK->im.mem[0], width * height * iImages_in_UEYE_Buffer);

    for (i = 0; i < iImages_in_UEYE_Buffer; i++)
    {
        buf = (unsigned char *) oi_TRACK->im.mem[i];
        is_SetAllocatedImageMem(m_hCam,  width, height, nb_bits, (char *)buf, &m_lMemoryId);
        is_AddToSequence(m_hCam, (char *)buf, m_lMemoryId);
    }

    /*
       if((oi_TRACK = (O_i *)calloc(1,sizeof(O_i))) == NULL)
       return win_printf_OK("Can't create dest image");
       if (init_one_image(oi_TRACK, IS_CHAR_IMAGE))
       return win_printf_OK("Can't create dest image");
       oi_TRACK->im.data_type = IS_CHAR_IMAGE;
       oi_TRACK->im.n_f = iImages_in_UEYE_Buffer;
       add_image(imr, IS_CHAR_IMAGE, (void*)oi_TRACK);

       for (i = 0; i < iImages_in_UEYE_Buffer; i++)
       {
       is_AllocImageMem( m_hCam, width, height, 8, &buf, &m_lMemoryId);
       attach_uEye_pointer_to_movie(oi_TRACK, buf,  width, height, i);
       is_AddToSequence(m_hCam,buf,m_lMemoryId);
       }
       */

    //win_printf("after create");
    map_pixel_ratio_of_image_and_screen(oi_TRACK, 1, 1);
    set_zmin_zmax_values(oi_TRACK, 0, pow(2,nb_bits));
    set_z_black_z_white_values(oi_TRACK, 0, pow(2,nb_bits));
    unsigned int pc = pixelclock;
# if defined(SDK_3_8_0) || defined(SDK_3_5_0)
    is_SetPixelClock(m_hCam, pc);
# endif
# if defined(SDK_4_0_0) || defined(SDK_4_2_1) || defined(SDK_4_2_2) || defined(SDK_4_3_0) || defined(SDK_4_6_0) || defined(SDK_4_6_1) || defined(SDK_4_7_0) || defined(SDK_4_7_2) || defined(SDK_4_8_0) || defined(SDK_4_9_0) || defined(SDK_4_9_1)
    ret = is_PixelClock(m_hCam, IS_PIXELCLOCK_CMD_SET, &pc, sizeof(pc));

    if (ret != IS_SUCCESS)       win_printf("Could not set camera pixel clock");
    else Pico_param.camera_param.pixel_clock = pixelclock;

# endif

    fps = ffps;
    //do_set_camera_max_freq();
    is_SetFrameRate(m_hCam, fps, &nfps);

    ret = is_SetFrameRate(m_hCam, IS_GET_FRAMERATE, &fps);

    if (ret != IS_SUCCESS)       win_printf("Could not get camera rate");
    else Pico_param.camera_param.camera_frequency_in_Hz = fps;
    Pico_param.camera_param.nb_bits = nb_bits;
    is_SetColorMode(m_hCam, nb_bits == 8 ?  IS_CM_MONO8:IS_CM_MONO12);
    write_Pico_config_file();
    Close_Pico_config_file();
    ask_to_update_menu();
    return 0;
}



int initialize_uEye(imreg *imr)
{
    int ret;

    //win_printf("bef hcam");
    my_set_window_title("Openning uEye camera");
    m_hCam = hCam_Open_camera();

    if (m_hCam == 0)
    {
        exit(0);
    }

    //win_printf("after hcam");
    my_set_window_title("Camera %s found seting image ", Pico_param.camera_param.camera_model);

    if ((ret = add_oi_UEYE_2_imr(imr, NULL))) return ret + 0x40;

    //win_printf("after add");
    my_set_window_title("Starting UEYE camera thread");
    //win_printf("after iopen");
    return 0;

}




int init_image_source(imreg *imr, int mode)
{
    int ret;
    O_i *oi = NULL;

    //if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
    if (imr == NULL)    imr = create_and_register_new_image_project(0,   32,  900,  668);

    if (imr == NULL)    return win_printf_OK("Patate t'es mal barre!");

    //imr_TRACK = imr;
    oi = imr->one_i;

    if ((ret = initialize_uEye(imr)))
    {
        win_printf_OK("Your uEye video camera did not start!");
        return ret; // Initializes UEYE
    }

    if (mode == 1)      remove_from_image(imr, oi->im.data_type, (void *)oi);
    //add_item_to_menu(plot_menu, "Experiment", NULL, UEYE_source_image_menu(), 0, NULL);
    //add_image_treat_menu_item("UEYE Genicam", NULL, UEYE_source_image_menu(), 0, NULL);
    get_camera_shutter_in_us = _get_camera_shutter_in_us;
    set_camera_shutter_in_us = _set_camera_shutter_in_us;
    get_camera_gain = _get_camera_gain;
    set_camera_gain = _set_camera_gain;
    specific_camera_image_menu = UEYE_image_menu;
    return 0;
}



int prepare_image_overlay(O_i *oi)
{
    (void)oi;
    return 0;
}




///////////////////BOF la suite////////////////////
int kill_source(void)
{
    imreg *imr;

    if (ac_grep(cur_ac_reg, "%im", &imr) != 1)
    {
        if (active_menu) active_menu->flags |= D_DISABLED;

        return D_O_K;
    }
    //else if (go_track == TRACK_ON) active_menu->flags |=  D_DISABLED;
    else
    {
        if (active_menu) active_menu->flags &= ~D_DISABLED;
    }

    if (updating_menu_state != 0)    return D_O_K;

    go_track = TRACK_STOP;
#ifdef XV_WIN32
    SetEvent(hEventFreeze);
#else
    is_FreezeVideo(m_hCam, IS_DONT_WAIT);
#endif
    return D_O_K;
}

int live_source(void)
{
    if (updating_menu_state != 0)    return D_O_K;

    //is_live  = 1;
    return 0;
}


int start_data_movie(imreg *imr)
{
  (void)imr;
    return 0;
}








int do_stop_camera(void)
{
    if (updating_menu_state != 0)    return D_O_K;

#ifdef XV_WIN32
    SetEvent(hEventFreeze);
#else
    is_FreezeVideo(m_hCam, IS_DONT_WAIT);
#endif
    bEnableThread = FALSE;
    return 0;
}



int freeze_source(void)
{
    if (updating_menu_state != 0)    return D_O_K;

#ifdef XV_WIN32
    SetEvent(hEventFreeze);
#else
    is_FreezeVideo(m_hCam, IS_DONT_WAIT);
#endif
    return 0;
}




int do_set_camera_gain(void)
{
    int i, ret;
    int gMaxRed, gMaxGreen, gMaxBlue, gMaxGain;//, ninpos;
    int gRed, gGreen, gBlue, gGain, gboost, gboostavail;
    char question[1024];


    if (updating_menu_state != 0)    return D_O_K;

    //Query the maximum gain factor for the red channel:
    gMaxRed = is_SetHWGainFactor(m_hCam, IS_INQUIRE_RED_GAIN_FACTOR, 100);
    gMaxGreen = is_SetHWGainFactor(m_hCam, IS_INQUIRE_GREEN_GAIN_FACTOR, 100);
    gMaxBlue = is_SetHWGainFactor(m_hCam, IS_INQUIRE_BLUE_GAIN_FACTOR, 100);
    gMaxGain = is_SetHWGainFactor(m_hCam, IS_INQUIRE_MASTER_GAIN_FACTOR, 100);


    gRed = is_SetHWGainFactor(m_hCam, IS_GET_RED_GAIN_FACTOR, 100);
    gGreen = is_SetHWGainFactor(m_hCam, IS_GET_GREEN_GAIN_FACTOR, 100);
    gBlue = is_SetHWGainFactor(m_hCam, IS_GET_BLUE_GAIN_FACTOR, 100);
    gGain = is_SetHWGainFactor(m_hCam, IS_GET_MASTER_GAIN_FACTOR, 100);

    gboostavail = is_SetGainBoost(m_hCam, IS_GET_SUPPORTED_GAINBOOST);
    gboost = is_SetGainBoost(m_hCam, IS_GET_GAINBOOST);
    /*
       win_printf("Camera has master gain %s current %d max %d\n"
       "Camera has Red gain %s current %d max %d\n"
       "Camera has Green gain %s current %d max %d\n"
       "Camera has Blue gain %s current %d max %d\n"
       "Camare support gain boost %s current val %d\n"
       ,((sInfo.bMasterGain)?"Yes":"No"),gGain,gMaxGain,
       ((sInfo.bRGain) ?"Yes":"No"),gRed,gMaxRed,
       ((sInfo.bGGain) ?"Yes":"No"),gGreen,gMaxGreen,
       ((sInfo.bBGain) ?"Yes":"No"),gBlue,gMaxBlue,
       ((gboostavail) ?"Yes":"No"),gboost
       );


*/
    i = gRed;
    i = gGreen;
    i = gBlue;
    i = gMaxRed;
    i = gMaxGreen;
    i = gMaxBlue;
    snprintf(question, 1024, "Camera %s %s \nmaster gain current value %%8d max %d\n"
             "Camera %s gain boost (No boost->%%R, boost %%r)\n"
             , sInfo.strSensorName, ((sInfo.bMasterGain) ? "has" : "does not have"), gMaxGain,
             ((gboostavail) ? "supports" : "does not support"));

    i = win_scanf(question, &gGain, &gboost);

    if (i == WIN_CANCEL) return OFF;

    ret = is_SetHWGainFactor(m_hCam, IS_SET_MASTER_GAIN_FACTOR, gGain);

    if (ret) return win_printf_OK("Cannot set gain");

    ret = is_SetGainBoost(m_hCam, (gboost) ? IS_SET_GAINBOOST_ON : IS_SET_GAINBOOST_OFF);

    if (ret) return win_printf_OK("Cannot set gain boost");

    return 0;
}

int _get_camera_gain(float *gain, float *fmin, float *fmax, float *finc)
{
    int gMaxGain;//, ninpos;
    int gGain;
    float fmaxg, fming;

    gMaxGain = is_SetHWGainFactor(m_hCam, IS_INQUIRE_MASTER_GAIN_FACTOR, 100);
    fmaxg = (float)gMaxGain / 100;
    fming = 1.0;
    gGain = is_SetHWGainFactor(m_hCam, IS_GET_MASTER_GAIN_FACTOR, 100);

    if (gain) *gain = ((float)gGain) / 100;

    if (fmin) *fmin = fming;

    if (fmax) *fmax = fmaxg;

    if (finc) *finc = ((float)1) / 100;

    return 0;
}

int _set_camera_gain(float gain, float *set, float *fmin, float *fmax, float *finc)
{
    int gMaxGain;//, ninpos;
    int gGain;
    float fmaxg, fming;

    gMaxGain = is_SetHWGainFactor(m_hCam, IS_INQUIRE_MASTER_GAIN_FACTOR, 100);
    fmaxg = (float)gMaxGain / 100;
    fming = 1.0;

    gGain = (int)(0.5 + (100 * gain));
    gGain = (gGain > gMaxGain) ? gMaxGain : gGain;
    gGain = (gGain < 100) ? 100 : gGain;
    is_SetHWGainFactor(m_hCam, IS_SET_MASTER_GAIN_FACTOR, gGain);

    gGain = is_SetHWGainFactor(m_hCam, IS_GET_MASTER_GAIN_FACTOR, 100);

    if (gain) *set = ((float)gGain) / 100;

    if (fmin) *fmin = fming;

    if (fmax) *fmax = fmaxg;

    if (finc) *finc = ((float)1) / 100;

    return 0;
}

int do_get_bad_correction_mode(void)
{
  if (updating_menu_state != 0)    return D_O_K;
  int nMode = 0;
  int r = is_HotPixel(m_hCam,IS_HOTPIXEL_GET_CORRECTION_MODE,(void*)&nMode, sizeof(nMode));
  if (r == IS_HOTPIXEL_DISABLE_CORRECTION) return win_printf_OK("IS_HOTPIXEL_DISABLE_CORRECTION\n");
  else if (r == IS_HOTPIXEL_ENABLE_CAMERA_CORRECTION) return win_printf_OK("IS_HOTPIXEL_ENABLE_CAMERA_CORRECTION\n");
  else if (r == (IS_HOTPIXEL_ENABLE_SENSOR_CORRECTION | IS_HOTPIXEL_ENABLE_CAMERA_CORRECTION)) return win_printf_OK("IS_HOTPIXEL_ENABLE_SENSOR_CORRECTION | IS_HOTPIXEL_ENABLE_CAMERA_CORRECTION\n");
  else if (r == IS_HOTPIXEL_ENABLE_SOFTWARE_USER_CORRECTION) return win_printf_OK("IS_HOTPIXEL_ENABLE_SOFTWARE_USER_CORRECTION\n");
  else if (r == (IS_HOTPIXEL_ENABLE_SENSOR_CORRECTION | IS_HOTPIXEL_ENABLE_SOFTWARE_USER_CORRECTION)) return win_printf_OK("(IS_HOTPIXEL_ENABLE_SENSOR_CORRECTION | IS_HOTPIXEL_ENABLE_SOFTWARE_USER_CORRECTION)\n");

  else return win_printf_OK("Erreur %d\n",r);

}

int do_set_camera_freq(void)
{
    double fr, nfr;
    float ffr;
    int pmin, pmax, pm, ret;
    double min, max, intervall;
    char question[1024];
    int i, optsn = 1;
    float gain, fmin, fmax, finc, g;
    int set = 0, smin = 0, smax = 0, exp;

# if defined(SDK_4_0_0) || defined(SDK_4_2_1) || defined(SDK_4_2_2) || defined(SDK_4_3_0) || defined(SDK_4_6_0) || defined(SDK_4_6_1) || defined(SDK_4_7_0) || defined(SDK_4_7_2) || defined(SDK_4_8_0) || defined(SDK_4_9_0) || defined(SDK_4_9_1)
    int nRet;
    UINT nPixelClock = 0, nRange[3] = {0, 0, 0};
# endif


    if (updating_menu_state != 0)    return D_O_K;

# if defined(SDK_3_8_0) || defined(SDK_3_5_0)
    ret = is_GetPixelClockRange(m_hCam, &pmin, &pmax);

    if (ret != IS_SUCCESS)       win_printf("Could not get camera rate");

    pm = is_SetPixelClock(m_hCam, IS_GET_PIXEL_CLOCK);
# endif
# if defined(SDK_4_0_0) || defined(SDK_4_2_1) || defined(SDK_4_2_2) || defined(SDK_4_3_0) || defined(SDK_4_6_0) || defined(SDK_4_6_1)  || defined(SDK_4_7_0) || defined(SDK_4_7_2) || defined(SDK_4_8_0) || defined(SDK_4_9_0) || defined(SDK_4_9_1)
     nRet = is_PixelClock(m_hCam, IS_PIXELCLOCK_CMD_GET_RANGE, (void *)nRange, sizeof(nRange));

    if (nRet == IS_SUCCESS)
    {
        pmin = nRange[0];
        pmax = nRange[1];
    }
    else return win_printf("Cannot get camera pixel clock range");

    ret = is_PixelClock(m_hCam, IS_PIXELCLOCK_CMD_GET, (void *)&nPixelClock, sizeof(nPixelClock));

    if (ret != IS_SUCCESS) return win_printf("Cannot get camera pixel clock");

    pm = nPixelClock;
# endif

    is_GetFrameTimeRange(m_hCam, &min, &max, &intervall);
    is_SetFrameRate(m_hCam, IS_GET_FRAMERATE, &fr);

    ffr = fr;
    //  fpsmin= 1/max ; fpsmax=1/min ; fpsn= 1/(min + n * intervall)
    snprintf(question, sizeof(question), "Camera frequency settings\n"
             "Pixel clock (%d min) < (%d present) < (%d max)\n"
             "New pixel clock %%8d\n"
             "Frame rate (%g min) < (%g present) < (%g max)\n"
             "New Frame rate %%8f\n"
	     "%%bOptimize S/N"
             , pmin, pm, pmax, (float)1 / max, (float)1 / fr, (float)1 / min);

    i = win_scanf(question, &pm, &ffr,&optsn);

    if (i == WIN_CANCEL)    return 0;

# if defined(SDK_3_8_0) || defined(SDK_3_5_0)
    is_SetPixelClock(m_hCam, pm);
# endif
# if defined(SDK_4_0_0) || defined(SDK_4_2_1) || defined(SDK_4_2_2) || defined(SDK_4_3_0 ) || defined(SDK_4_6_0) || defined(SDK_4_6_1)  || defined(SDK_4_7_0) || defined(SDK_4_7_2) || defined(SDK_4_8_0) || defined(SDK_4_9_0) || defined(SDK_4_9_1)
    nPixelClock = pm;
    ret = is_PixelClock(m_hCam, IS_PIXELCLOCK_CMD_SET, (void *)&nPixelClock, sizeof(nPixelClock));

    if (ret != IS_SUCCESS)       win_printf("Could not set camera pixel clock");
    else Pico_param.camera_param.pixel_clock = pm;

# endif

    fr = ffr;
    is_SetFrameRate(m_hCam, fr, &nfr);

    ret = is_SetFrameRate(m_hCam, IS_GET_FRAMERATE, &fr);

    if (ret != IS_SUCCESS)       win_printf("Could not get camera rate");
    else Pico_param.camera_param.camera_frequency_in_Hz = fr;

    if (optsn)
      {
	_get_camera_gain(&gain, &fmin, &fmax, &finc);
	_get_camera_shutter_in_us(&set, &smin, &smax);
	g = fmin;
	_set_camera_gain(g, &gain, &fmin, &fmax, &finc);
	exp = smax;
	_set_camera_shutter_in_us(exp, &set, &smin, &smax);
      }


    write_Pico_config_file();
    return 0;
}



int do_set_camera_max_freq(void)
{
    double fr, nfr;
# if defined(SDK_3_8_0) || defined(SDK_3_5_0)
    int pmin;
# endif

    int pmax, ret;
    double min, max, intervall;
# if defined(SDK_4_0_0) || defined(SDK_4_2_1) || defined(SDK_4_2_2) || defined(SDK_4_3_0) || defined(SDK_4_6_0) || defined(SDK_4_6_1)  || defined(SDK_4_7_0) || defined(SDK_4_7_2) || defined(SDK_4_8_0) || defined(SDK_4_9_0) || defined(SDK_4_9_1)
    int nRet;
    UINT nPixelClock = 0, nRange[3] = {0, 0, 0};
# endif

    if (updating_menu_state != 0)    return D_O_K;

# if defined(SDK_3_8_0) || defined(SDK_3_5_0)
    is_GetPixelClockRange(m_hCam, &pmin, &pmax);
    ret = is_SetPixelClock(m_hCam, pmax);

    if (ret != IS_SUCCESS)       win_printf("Could not get camera rate");

# endif
# if defined(SDK_4_0_0) || defined(SDK_4_2_1) || defined(SDK_4_2_2) || defined(SDK_4_3_0) || defined(SDK_4_6_0) || defined(SDK_4_6_1)  || defined(SDK_4_7_0) || defined(SDK_4_7_2) || defined(SDK_4_8_0) || defined(SDK_4_9_0) || defined(SDK_4_9_1)
    nRet = is_PixelClock(m_hCam, IS_PIXELCLOCK_CMD_GET_RANGE, (void *)nRange, sizeof(nRange));

    if (nRet == IS_SUCCESS)
    {
      //pmin = nRange[0];
        nPixelClock = pmax = nRange[1];
    }
    else return win_printf("Cannot get camera pixel clock range");

    nRet = is_PixelClock(m_hCam, IS_PIXELCLOCK_CMD_SET, (void *)&nPixelClock, sizeof(nPixelClock));

    if (nRet != IS_SUCCESS) return win_printf("Cannot set camera pixel clock");
    else Pico_param.camera_param.pixel_clock = pmax;

# endif

    is_GetFrameTimeRange(m_hCam, &min, &max, &intervall);
    is_SetFrameRate(m_hCam, (double)1 / min, &nfr);
    ret = is_SetFrameRate(m_hCam, IS_GET_FRAMERATE, &fr);

    if (ret != IS_SUCCESS) return win_printf("Cannot set camera pixel clock");
    else Pico_param.camera_param.camera_frequency_in_Hz = fr;

    return 0;
}



int adj_AOI_position(void)
{
# if defined(SDK_3_8_0) || defined(SDK_3_5_0)
    int ret;
# endif
    int i;
    int x0, y0, width, height;
# if defined(SDK_4_0_0) || defined(SDK_4_2_1) || defined(SDK_4_2_2) || defined(SDK_4_3_0) || defined(SDK_4_6_0) || defined(SDK_4_6_1)  || defined(SDK_4_7_0) || defined(SDK_4_7_2) || defined(SDK_4_8_0) || defined(SDK_4_9_0) || defined(SDK_4_9_1)
    IS_RECT rectAOI;
# endif


    if (updating_menu_state != 0)    return D_O_K;

    x0 = 0;
    y0 = 0;
    width = sInfo.nMaxWidth;
    height = sInfo.nMaxHeight;

# if defined(SDK_3_8_0) || defined(SDK_3_5_0)
    ret = is_SetAOI(m_hCam, IS_GET_IMAGE_AOI, &x0, &y0, &width, &height);

    if (ret != IS_SUCCESS)       win_printf("Could not get AOI");

# endif
# if defined(SDK_4_0_0) || defined(SDK_4_2_1) || defined(SDK_4_2_2) || defined(SDK_4_3_0) || defined(SDK_4_6_0) || defined(SDK_4_6_1)  || defined(SDK_4_7_0) || defined(SDK_4_7_2) || defined(SDK_4_8_0) || defined(SDK_4_9_0) || defined(SDK_4_9_1)
    is_AOI(m_hCam, IS_AOI_IMAGE_GET_AOI, (void *)&rectAOI, sizeof(rectAOI));
    x0 = rectAOI.s32X;
    y0 = rectAOI.s32Y;
    width = rectAOI.s32Width;
    height = rectAOI.s32Height;
# endif


    i = win_scanf("Defines AOI origin position\n"
                  "X0 %8d y0 %8d\n", &x0, &y0);

    if (i == WIN_CANCEL) return OFF;

# if defined(SDK_3_8_0) || defined(SDK_3_5_0)
    ret = is_SetAOI(m_hCam, IS_SET_IMAGE_AOI, &x0, &y0, &width, &height);

    if (ret != IS_SUCCESS)       win_printf("Could not set AOI");

# endif
# if defined(SDK_4_0_0) || defined(SDK_4_2_1) || defined(SDK_4_2_2) || defined(SDK_4_3_0) || defined(SDK_4_6_0)  || defined(SDK_4_6_1) || defined(SDK_4_7_0) || defined(SDK_4_7_2) || defined(SDK_4_8_0) || defined(SDK_4_9_0) || defined(SDK_4_9_1)
    rectAOI.s32X     = x0;
    rectAOI.s32Y     = y0;
    rectAOI.s32Width = width;
    rectAOI.s32Height = height;
    is_AOI(m_hCam, IS_AOI_IMAGE_SET_AOI, (void *)&rectAOI, sizeof(rectAOI));
# endif


    return 0;
}



int do_set_camera_exposure(void)
{
    int i;
    double min = 0, max = 0, intervall = 0, exp, nexp;
    float fexp;
    char question[1024];
# if defined(SDK_4_0_0) || defined(SDK_4_2_1) || defined(SDK_4_2_2) || defined(SDK_4_3_0) || defined(SDK_4_6_0) || defined(SDK_4_6_1) || defined(SDK_4_7_0) || defined(SDK_4_7_2) || defined(SDK_4_8_0) || defined(SDK_4_9_0) || defined(SDK_4_9_1)
    int nRet;
    double dblRange[3];
    UINT nCaps = 0;
# endif

    if (updating_menu_state != 0)    return D_O_K;

# if defined(SDK_3_8_0) || defined(SDK_3_5_0)
    is_GetExposureRange(m_hCam, &min, &max, &intervall);
    is_SetExposureTime(m_hCam, IS_GET_EXPOSURE_TIME, &nexp);
# endif
# if defined(SDK_4_0_0) || defined(SDK_4_2_1) || defined(SDK_4_2_2) || defined(SDK_4_3_0) || defined(SDK_4_6_0) || defined(SDK_4_6_1)  || defined(SDK_4_7_0) || defined(SDK_4_7_2) || defined(SDK_4_8_0) || defined(SDK_4_9_0) || defined(SDK_4_9_1)
    nRet = is_Exposure(m_hCam, IS_EXPOSURE_CMD_GET_CAPS, (void *)&nCaps, sizeof(nCaps));

    if (nRet == IS_SUCCESS)
    {
        // Fine increment supported
        if (nCaps & IS_EXPOSURE_CAP_FINE_INCREMENT)
        {
            nRet = is_Exposure(m_hCam, IS_EXPOSURE_CMD_GET_FINE_INCREMENT_RANGE
                               , (void *)dblRange, sizeof(dblRange));

            if (nRet == IS_SUCCESS)
            {
                min = (int)(dblRange[0]);
                max = (int)(dblRange[1]);
                intervall = (int)(dblRange[2]);
            }
        }
        else if (nCaps & IS_EXPOSURE_CAP_EXPOSURE)
        {
            nRet = is_Exposure(m_hCam, IS_EXPOSURE_CMD_GET_EXPOSURE_RANGE
                               , (void *)dblRange, sizeof(dblRange));

            if (nRet == IS_SUCCESS)
            {
                min = (int)(dblRange[0]);
                max = (int)(dblRange[1]);
                intervall = (int)(dblRange[2]);
            }
        }
    }

    nRet = is_Exposure(m_hCam, IS_EXPOSURE_CMD_GET_EXPOSURE, (void *)&nexp, sizeof(nexp));
# endif
    fexp = nexp;

    snprintf(question, sizeof(question), "Camera exposure setting\n"
             "Range min %g max %g dt %g"
             "new value %%12f", min, max, intervall);
    i = win_scanf(question, &fexp);

    if (i == WIN_CANCEL)    return 0;

    exp = fexp;

# if defined(SDK_3_8_0) || defined(SDK_3_5_0)
    is_SetExposureTime(m_hCam, exp, &nexp);
# endif
# if defined(SDK_4_0_0) || defined(SDK_4_2_1) || defined(SDK_4_2_2) || defined(SDK_4_3_0) || defined(SDK_4_6_0) || defined(SDK_4_6_1)  || defined(SDK_4_7_0) || defined(SDK_4_7_2) || defined(SDK_4_8_0) || defined(SDK_4_9_0) || defined(SDK_4_9_1)
    is_Exposure(m_hCam, IS_EXPOSURE_CMD_SET_EXPOSURE, (void *)&exp, sizeof(exp));
    is_Exposure(m_hCam, IS_EXPOSURE_CMD_GET_EXPOSURE, (void *)&nexp, sizeof(nexp));
# endif
    return 0;
}

int _get_camera_shutter_in_us(int *set, int *smin, int *smax)
{
# if defined(SDK_3_8_0) || defined(SDK_3_5_0)
    double dmin, dmax, dintervall;
# endif
    double dexp;
# if defined(SDK_4_0_0) || defined(SDK_4_2_1) || defined(SDK_4_2_2) || defined(SDK_4_3_0) || defined(SDK_4_6_0) || defined(SDK_4_6_1) || defined(SDK_4_7_0) || defined(SDK_4_7_2) || defined(SDK_4_8_0) || defined(SDK_4_9_0) || defined(SDK_4_9_1)
    int nRet;
    double dblRange[3];
    UINT nCaps = 0;
# endif

# if defined(SDK_3_8_0) || defined(SDK_3_5_0)
    is_GetExposureRange(m_hCam, &dmin, &dmax, &dintervall);
    is_SetExposureTime(m_hCam, IS_GET_EXPOSURE_TIME, &dexp);

    if (set) *set = (int)(0.5 + (1000 * dexp));

    if (smin) *smin = (int)(0.5 + (1000 * dmin));

    if (smax) *smax = (int)(0.5 + (1000 * dmax));

# endif
# if defined(SDK_4_0_0) || defined(SDK_4_2_1) || defined(SDK_4_2_2) || defined(SDK_4_3_0) || defined(SDK_4_6_0) || defined(SDK_4_6_1)  || defined(SDK_4_7_0) || defined(SDK_4_7_2) || defined(SDK_4_8_0) || defined(SDK_4_9_0) || defined(SDK_4_9_1)
    nRet = is_Exposure(m_hCam, IS_EXPOSURE_CMD_GET_CAPS, (void *)&nCaps, sizeof(nCaps));

    if (nRet == IS_SUCCESS)
    {
        // Fine increment supported
        if (nCaps & IS_EXPOSURE_CAP_FINE_INCREMENT)
        {
            nRet = is_Exposure(m_hCam, IS_EXPOSURE_CMD_GET_FINE_INCREMENT_RANGE
                               , (void *)dblRange, sizeof(dblRange));

            if (nRet == IS_SUCCESS)
            {
                if (smin) *smin = (int)(0.5 + (1000 * dblRange[0]));

                if (smax) *smax = (int)(0.5 + (1000 * dblRange[1]));
            }
        }
        else if (nCaps & IS_EXPOSURE_CAP_EXPOSURE)
        {
            nRet = is_Exposure(m_hCam, IS_EXPOSURE_CMD_GET_EXPOSURE_RANGE
                               , (void *)dblRange, sizeof(dblRange));

            if (nRet == IS_SUCCESS)
            {
                if (smin) *smin = (int)(0.5 + (1000 * dblRange[0]));

                if (smax) *smax = (int)(0.5 + (1000 * dblRange[1]));
            }
        }
    }

    nRet = is_Exposure(m_hCam, IS_EXPOSURE_CMD_GET_EXPOSURE, (void *)&dexp, sizeof(dexp));

    if (nRet == IS_SUCCESS)   if (set) *set = (int)(0.5 + (1000 * dexp));

# endif
    return 0;
}



int _set_camera_shutter_in_us(int exp, int *set, int *smin, int *smax)
{
# if defined(SDK_3_8_0) || defined(SDK_3_5_0)
    double dmin, dmax, dintervall;
# endif
    double dexp, nexp;
    int min = 0, max = 0;
# if defined(SDK_4_0_0) || defined(SDK_4_2_1) || defined(SDK_4_2_2) || defined(SDK_4_3_0) || defined(SDK_4_6_0) || defined(SDK_4_6_1) || defined(SDK_4_7_0) || defined(SDK_4_7_2) || defined(SDK_4_8_0) || defined(SDK_4_9_0) || defined(SDK_4_9_1)
    int nRet;
    double dblRange[3];
    UINT nCaps = 0;
# endif

# if defined(SDK_3_8_0) || defined(SDK_3_5_0)
    is_GetExposureRange(m_hCam, &dmin, &dmax, &dintervall);
    is_SetExposureTime(m_hCam, IS_GET_EXPOSURE_TIME, &dexp);
    min = (int)(0.5 + (1000 * dmin));
    max = (int)(0.5 + (1000 * dmax));
# endif

# if defined(SDK_4_0_0) || defined(SDK_4_2_1) || defined(SDK_4_2_2) || defined(SDK_4_3_0) || defined(SDK_4_6_0) || defined(SDK_4_6_1)  || defined(SDK_4_7_0) || defined(SDK_4_7_2) || defined(SDK_4_8_0) || defined(SDK_4_9_0) || defined(SDK_4_9_1)
    nRet = is_Exposure(m_hCam, IS_EXPOSURE_CMD_GET_CAPS, (void *)&nCaps, sizeof(nCaps));

    if (nRet == IS_SUCCESS)
    {
        // Fine increment supported
        if (nCaps & IS_EXPOSURE_CAP_FINE_INCREMENT)
        {
            nRet = is_Exposure(m_hCam, IS_EXPOSURE_CMD_GET_FINE_INCREMENT_RANGE
                               , (void *)dblRange, sizeof(dblRange));

            if (nRet == IS_SUCCESS)
            {
                min = (int)(0.5 + (1000 * dblRange[0]));
                max = (int)(0.5 + (1000 * dblRange[1]));
            }
        }
        else if (nCaps & IS_EXPOSURE_CAP_EXPOSURE)
        {
            nRet = is_Exposure(m_hCam, IS_EXPOSURE_CMD_GET_EXPOSURE_RANGE
                               , (void *)dblRange, sizeof(dblRange));

            if (nRet == IS_SUCCESS)
            {
                min = (int)(0.5 + (1000 * dblRange[0]));
                max = (int)(0.5 + (1000 * dblRange[1]));
            }
        }
    }

    is_Exposure(m_hCam, IS_EXPOSURE_CMD_GET_EXPOSURE, (void *)&dexp, sizeof(dexp));
# endif

    if (exp == 0) exp = max;  // no shutter means maximum exposure

    if (exp < min) exp = min;

    if (exp > max) exp = max;

    dexp = (double)exp / 1000;

# if defined(SDK_3_8_0) || defined(SDK_3_5_0)
    is_SetExposureTime(m_hCam, dexp, &nexp);
# endif
# if defined(SDK_4_0_0) || defined(SDK_4_2_1) || defined(SDK_4_2_2) || defined(SDK_4_3_0) || defined(SDK_4_6_0) || defined(SDK_4_6_1)  || defined(SDK_4_7_0) || defined(SDK_4_7_2) || defined(SDK_4_8_0) || defined(SDK_4_9_0) || defined(SDK_4_9_1)
    is_Exposure(m_hCam, IS_EXPOSURE_CMD_SET_EXPOSURE, (void *)&dexp, sizeof(dexp));
    is_Exposure(m_hCam, IS_EXPOSURE_CMD_GET_EXPOSURE, (void *)&nexp, sizeof(nexp));
# endif

    if (set) *set = (int)(0.5 + (1000 * nexp));

    if (smin) *smin = min;

    if (smax) *smax = max;

    return 0;
}



int adjust_black_level(void)
{
  int  i, m_Ret, nBlacklevelCaps, nOffset = 0;
  char question[1024];
 IS_RANGE_S32 nRange;


   if (updating_menu_state != 0)
      {
	if (m_hCam == 0)  active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }


  m_Ret = is_Blacklevel(m_hCam, IS_BLACKLEVEL_CMD_GET_CAPS,
			(void*)&nBlacklevelCaps, sizeof(nBlacklevelCaps));

  if (m_Ret == IS_SUCCESS)
    {
      win_printf("The user can changed the state of the auto blacklevel %d\n"
		 "The user can change the offset %d"
		 ,(nBlacklevelCaps & IS_BLACKLEVEL_CAP_SET_AUTO_BLACKLEVEL)
		 ,(nBlacklevelCaps & IS_BLACKLEVEL_CAP_SET_OFFSET));

      m_Ret = is_Blacklevel(m_hCam, IS_BLACKLEVEL_CMD_GET_OFFSET, (void*)&nOffset, sizeof(nOffset));


      m_Ret = is_Blacklevel(m_hCam, IS_BLACKLEVEL_CMD_GET_OFFSET_RANGE, (void*)&nRange, sizeof(nRange));

      snprintf(question,sizeof(question),"Black level min %d max %d inc %d\n"
	       "Present offset %d new one %%6d\n"
	       ,nRange.s32Min, nRange.s32Max, nRange.s32Inc,nOffset);
      i = win_scanf(question,&nOffset);
      if (i == WIN_CANCEL) return OFF;
      m_Ret = is_Blacklevel(m_hCam, IS_BLACKLEVEL_CMD_SET_OFFSET, (void*)&nOffset, sizeof(nOffset));
    }
  return 0;
}


char    *do_save_avi(void)
{
    register int i = 0;
    int win_file = 0;
    char path[512] = {0},
         file[256] = {0};
    static char fullfile[512] = {0};
    char *pa = NULL;
#ifdef XV_WIN32
    char *fu = NULL;
#endif

    Open_Pico_config_file();
    pa = (char *)strdup((const char *)get_config_string("AVI-FILE", "last_saved", ""));
    Close_Pico_config_file();

    if (pa != NULL)
    {
        strncpy(fullfile, pa, sizeof(fullfile));
        free(pa);
        pa = NULL;
    }
    else
    {
        my_getcwd(fullfile, sizeof(fullfile));
        put_path_separator(fullfile, sizeof(fullfile));
        my_strncat(fullfile, "test.avi", sizeof(fullfile));
    }

#ifdef XV_WIN32

    if (full_screen)
    {
#endif
        switch_allegro_font(1);
        i = file_select_ex("Save avi movie (*.avi)", fullfile, "avi", 512, 0, 0);
        switch_allegro_font(0);
#ifdef XV_WIN32
    }
    else
    {
        extract_file_path(path, sizeof(path), fullfile);

        if (extract_file_name(file, sizeof(file), fullfile) != NULL)    strncpy(fullfile, file, sizeof(fullfile));
        else fullfile[0] = file[0] = 0;

        fu = DoFileSave("Save AVI movie (*.avi)", path, fullfile, 512,
                        "AVI movie (*.avi)\0*.trk\0All Files (*.*)\0*.*\0\0", "avi");
        i = (fu == NULL) ? 0 : 1;
        win_file = 1;
    }

#endif

    if (i != 0)
    {
        Open_Pico_config_file();
        set_config_string("AVI-FILE", "last_saved", fullfile);
        write_Pico_config_file();
        Close_Pico_config_file();
        extract_file_name(file, sizeof(file), fullfile);
        extract_file_path(path, sizeof(path), fullfile);
        put_path_separator(path, sizeof(path));

        if (win_file == 1 || do_you_want_to_overwrite(fullfile, NULL, "Saving AVI File") == WIN_OK)
            return fullfile;
    }

    return NULL;
}



int start_ueye_avi(void)
{
#ifdef XV_WIN32
    int  ret, i;
    static int quality = 80;
    double fr;
    char *avi_file = NULL;
#endif

    if (updating_menu_state != 0)    return D_O_K;

    if (key[KEY_LSHIFT])
        return win_printf_OK("This routine record an AVI movie");

    if (m_hCam == IS_INVALID_HIDS)  return D_O_K;

#ifdef XV_WIN32
    ret = isavi_InitAVI(&nAviID, m_hCam);

    if (ret != IS_AVI_NO_ERR)    return win_printf_OK("Error initiating AVI %d", ret);

    ret = isavi_SetImageSize(nAviID, IS_AVI_CM_Y8, sInfo.nMaxWidth, sInfo.nMaxHeight, 0, 0, 0);  // sInfo.nMaxWidth

    if (ret != IS_AVI_NO_ERR)    return win_printf_OK("Error initiating AVI %d", ret);

    i = win_scanf("Jpeg quality(1->poor 100->excellent) %d", &quality);

    if (i == WIN_CANCEL) return D_O_K;

    avi_file = do_save_avi();

    if (avi_file == NULL)  return D_O_K;

    ret = isavi_SetImageQuality(nAviID, quality);

    if (ret != IS_AVI_NO_ERR)    return win_printf_OK("Error initiating AVI %d", ret);

    ret = isavi_OpenAVI(nAviID, avi_file);

    if (ret != IS_AVI_NO_ERR)    return win_printf_OK("Error initiating AVI %d", ret);


    is_SetFrameRate(m_hCam, IS_GET_FRAMERATE, &fr);


    ret = isavi_SetFrameRate(nAviID, fr);

    if (ret != IS_AVI_NO_ERR)    return win_printf_OK("Error initiating AVI %d", ret);

    ret = isavi_StartAVI(nAviID);

    if (ret != IS_AVI_NO_ERR)    return win_printf_OK("Error initiating AVI %d", ret);

#endif
    rec_AVI = 1;

    return D_O_K;
}


int stop_ueye_avi(void)
{
#ifdef XV_WIN32
    int ret;
#endif

    if (updating_menu_state != 0)    return D_O_K;

    if (key[KEY_LSHIFT])
        return win_printf_OK("This routine stop the recording of an AVI movie");

    rec_AVI = 0;
#ifdef XV_WIN32
    ret = isavi_StopAVI(nAviID);

    if (ret != IS_AVI_NO_ERR)    return win_printf_OK("Error initiating AVI %d", ret);

    ret = isavi_CloseAVI(nAviID);

    if (ret != IS_AVI_NO_ERR)    return win_printf_OK("Error initiating AVI %d", ret);

    ret = isavi_ExitAVI(nAviID);

    if (ret != IS_AVI_NO_ERR)    return win_printf_OK("Error initiating AVI %d", ret);

    nAviID = -1;
#endif
    return D_O_K;
}



MENU *UEYE_source_image_menu(void)
{
    static MENU mn[32];

    if (mn[0].text != NULL) return mn;

    add_item_to_menu(mn, "chg Freq", do_set_camera_freq, NULL, 0, NULL);
    add_item_to_menu(mn, "chg exposure", do_set_camera_exposure, NULL, 0, NULL);
    add_item_to_menu(mn, "Adjust AOI origin", adj_AOI_position, NULL, 0, NULL);
    add_item_to_menu(mn, "change gains", do_set_camera_gain, NULL, 0, NULL);
    add_item_to_menu(mn, "Adjust black level", adjust_black_level, NULL, 0, NULL);
    add_item_to_menu(mn, "stop uEye movie", do_stop_camera, NULL, 0, NULL);
    add_item_to_menu(mn, "freeze uEye movie", freeze_source, NULL, 0, NULL);
    add_item_to_menu(mn, "start AVI movie", start_ueye_avi, NULL, 0, NULL);
    add_item_to_menu(mn, "stop AVI movie", stop_ueye_avi, NULL, 0, NULL);



    return mn;

}

MENU *UEYE_image_menu(void)
{
    static MENU mn[32];

    if (mn[0].text != NULL) return mn;

    add_item_to_menu(mn, "Adjust black level", adjust_black_level, NULL, 0, NULL);
    add_item_to_menu(mn, "chg Freq", do_set_camera_freq, NULL, 0, NULL);
    add_item_to_menu(mn, "Adjust AOI origin", adj_AOI_position, NULL, 0, NULL);
    add_item_to_menu(mn, "start AVI movie", start_ueye_avi, NULL, 0, NULL);
    add_item_to_menu(mn, "stop AVI movie", stop_ueye_avi, NULL, 0, NULL);
    add_item_to_menu(mn, "get correction mode", do_get_bad_correction_mode, NULL, 0, NULL);
    return mn;

}


/** Unload function for the  UEYE.c menu.*/
int UEYE_unload(void)
{
    remove_item_to_menu(image_treat_menu, "uEye camera", NULL, NULL);
    return D_O_K;
}



#endif
