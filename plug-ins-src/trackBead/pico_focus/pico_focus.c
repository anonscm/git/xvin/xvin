/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _PICO_FOCUS_C_
#define _PICO_FOCUS_C_

# include "allegro.h"
#ifdef XV_WIN32
# include "winalleg.h"
#endif
# include "xvin.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// these rotines all work in the Real time timer thread
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

/* If you include other plug-ins header do it here*/ 
# include "../pico_serial/pico_serial.h"

/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "pico_focus.h"
# include "../action.h"
# include "../../fft2d/fftbtl32n.h"
# include "../fillibbt.h"

# include "../trackBead.h"
# include "../track_util.h"



int  _init_focus_OK(void)
{
  if(updating_menu_state != 0)	return D_O_K;
  if (init_pico_serial()) 
    {
      win_printf_OK("Serial port initialisation failed !");
      return D_O_K; 
    }
  return 0;   
}
int _has_focus_memory(void)
{
 return 1;
}


float _read_Z_value_accurate_in_micron(void)
{
  char command[128], answer[128];
  int  set = -1, ret, over = 0;
  unsigned long len = 0;
  //unsigned long t0;
  unsigned long dwErrors;
  
  snprintf(command,128,"dac16?\r");
  /*  Write_serial_port(hCom,command,strlen(command));
  //talk_serial_port(hCom, command, WRITE_READ, answer, strlen(command), 127);
  for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
  answer[len] = 0;  */
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return (ret -5);
  if (sscanf(answer,"Dac16 = %d",&set) == 1)
    { 
      if (set >= 0)       pico_dac = set;
      pico_obj_position = ((float)(pico_dac*250))/65535;
      over = (strstr(answer,"OVER") != NULL) ? 1 : 0; // we check for overflow
      if (over) 
	{
	  my_set_window_title("Pifoc Overflow ! %g",(pico_obj_position + 1000000));
	  return (pico_obj_position + 1000000); 
	}
      else return pico_obj_position;
    }
  else return -2;
}

int _read_Z_value(void)
{
	return (int)(10*_read_Z_value_accurate_in_micron());	
}
int _set_Z_step(float z)  /* in micron */
{
	pico_Z_step = z;
	return 0;
}

float _read_Z_value_OK(int *error) /* in microns */
{
  (void)*error;
  return _read_Z_value_accurate_in_micron();
}

int _set_Z_value(float z)  /* in microns */
{
  char command[128], answer[128];
  int dac, set = -1, ret, im = 0;
  unsigned long len = 0;
  unsigned long dwErrors;
  
  //purge_com();
  if (z < 0) dac = 0;
  else if (z >= 250) dac = 65535;
  else dac = (int)((65536 * z)/250);
  if (dac > 65535) dac = 65535;
  snprintf(command,128,"dac16=%d&%d\r\n",dac,dac);
  /*Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
    answer[len] = 0; */
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return (ret -5);
  if (sscanf(answer,"Dac16 aiming %d",&set) == 1)
    { 
      if (set != dac)
	{
	  if (debug)
	    {
	      fp_debug = fopen(debug_file,"a");
	      if (fp_debug != NULL)
		{
		  if (track_info != NULL) im = track_info->ac_i;
		  fprintf (fp_debug,"Im %d : focus pb set %d different from dac %d\n",im,set,dac);
		  fclose(fp_debug);
		}
	    }
	}
      else
	{
	  pico_dac = set;
	  pico_obj_position = ((float)(pico_dac*250))/65535;
	}
    }
  else return -1;
  return (dac == set) ? 0 : 1;
}



int _request_read_focus_value(int image_n, unsigned long *t0)
{
  char command[128];
  
  if (rs232_register(READ_FOCUS_REQUEST,image_n,0)) return 1; // rs232 buisy
  if (t0 != NULL) *t0 = my_uclock();
  snprintf(command,128,"dac16?\r");
  Write_serial_port(hCom,command,strlen(command));
  if (t0 != NULL) *t0 = my_uclock()- *t0;
  return 0;
}


float _read_focus_raw_from_request_answer(char *answer, int im_n, float val, int *error)
{
  int set = -1;
  float z = 0;
  char *over = NULL;

  (void)val;
  (void)im_n;
  *error = 0;
  if (answer != NULL)
    {
      over = strstr(answer,"OVER");
      if (over != NULL) *error |= PIFOC_OVERFLOW;
      if (sscanf(answer,"Dac16 = %d",&set) == 1)
	{

	  pico_dac = set;
	  z = pico_obj_position = ((float)(pico_dac*250))/65535;
	}
      else *error |= PICO_COM_ERROR;
    } 
  else *error =  PICO_COM_ERROR;
  return z;	
}

int _request_set_focus_value_raw(int image_n, float z, unsigned long *t0)
{
  char command[128];
  int dac;

  if (t0 != NULL) *t0 = my_uclock();
  if (rs232_register(SET_FOCUS_REQUEST,image_n,z)) return 1;
  if (z < 0) dac = 0;
  else if (z >= 250) dac = 65535;
  else dac = (int)((65536 * z)/250);
  if (dac > 65535) dac = 65535;
  snprintf(command,128,"dac16=%d&%d\r\n",dac,dac);
  Write_serial_port(hCom,command,strlen(command));
  if (t0 != NULL) *t0 = my_uclock()- *t0;
  return 0;
}
int _check_set_focus_request_answer(char *answer, int im_n, float z)
{
  int dac, set ;
  (void)im_n;
  if (answer == NULL)     return PICO_COM_ERROR;
  if (z < 0) dac = 0;
  else if (z >= 250) dac = 65535;
  else dac = (int)((65536 * z)/250);
  if (dac > 65535) dac = 65535;
  if (sscanf(answer,"Dac16 aiming %d",&set) == 1)
    {
      if (set == dac) return 0;
    }
  return PICO_COM_ERROR;
}


float _read_last_Z_value(void) /* in microns */
{
  return pico_obj_position;	
}

int	_set_Z_obj_accurate(float zstart)
{
  return _set_Z_value(zstart);
}
int _set_Z_value_OK(float z) 
{
  return _set_Z_value(z);
}

int _inc_Z_value(int step)  /* in micron */
{
  _set_Z_value(pico_obj_position + pico_Z_step*step);
  return 0;
}

int _small_inc_Z_value(int up)  
{
  int nstep;
  
  _set_Z_step(0.1);
  nstep = (up>=0)? 1:-1;
  _set_Z_value(pico_obj_position + pico_Z_step*nstep);
  
  //display_title_message("Z obj = %g \\mu m Rot %g Z mag %g mm",
  //  (float)dummy_read_Z_value()/10,n_rota,n_magnet_z);
  return 0;
}

int _big_inc_Z_value(int up)  
{
  int nstep;
  
  _set_Z_step(0.1);
  nstep = (up>=0)? 10:-10;
  _set_Z_value(pico_obj_position + pico_Z_step*nstep);
  //	display_title_message("Z obj = %g \\mu m Rot %g Z mag %g mm",
  //  (float)dummy_read_Z_value()/10,n_rota,n_magnet_z);
  
  return 0;
}


MENU *pico_focus_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	//	add_item_to_menu(mn,"Read Objective position", pico_read_Z,NULL,0,NULL);
	//add_item_to_menu(mn,"Set Objective position", pico_set_Z,NULL,0,NULL);

	return mn;
}

int	pico_focus_main(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  pico_serial_main(0, NULL);
  if (init_pico_serial()) 
    {
      win_printf_OK("Serial port initialisation failed !");
      return D_O_K; 
    }
  add_plot_treat_menu_item ( "pico_focus", NULL, pico_focus_plot_menu(), 0, NULL);
  add_image_treat_menu_item ( "pico_focus", NULL, pico_focus_plot_menu(), 0, NULL);
  return D_O_K;
}

int	pico_focus_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;
	remove_item_to_menu(plot_treat_menu, "pico_focus", NULL, NULL);
	return D_O_K;
}
#endif

