#ifndef _PICO_FOCUS_H_
#define _PICO_FOCUS_H_


# ifndef _PICO_FOCUS_C_

# endif

# ifdef _PICO_FOCUS_C_
float   pico_obj_position = 0, pico_Z_step = 0.1;
int pico_dac = 0;

# endif
PXV_FUNC(MENU*, serialw_menu, (void));
PXV_FUNC(int, serialw32_main, (int argc, char **argv));

PXV_FUNC(MENU*, pico_focus_plot_menu, (void));
PXV_FUNC(int, pico_focus_main, (int argc, char **argv));

PXV_FUNC(int, _request_read_temperature_value, (int image_n, int t, unsigned long *t0));
PXV_FUNC(float, _read_temperature_from_request_answer, (char *answer, int im_n, float val, int *error, int *channel));
PXV_FUNC(int, _request_read_focus_value, (int image_n, unsigned long *t0));
PXV_FUNC(float, _read_focus_raw_from_request_answer, (char *answer, int im_n, float val, int *error));
PXV_FUNC(int, _request_set_focus_value_raw, (int image_n, float z, unsigned long *t0));
PXV_FUNC(int, _check_set_focus_request_answer, (char *answer, int im_n, float z));

PXV_FUNC(int, _set_T0_pid, (int num, int val));
PXV_FUNC(int,  _get_T0_pid, (int num));

#endif

