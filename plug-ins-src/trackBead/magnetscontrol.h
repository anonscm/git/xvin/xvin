#ifndef _MAGNETSCONTROL_H_
#define _MAGNETSCONTROL_H_


# ifndef _MAGNETSCONTROL_C_

#ifdef ZMAG_THREAD
# include <pthread.h>
#ifdef XV_WIN32
#include <windows.h>
#endif
  typedef struct zmag_thread_data
  {
    long long *timestamp;
    float *zmag_value;
    int *limit;
    int zmag_thread_buffer;
    int period_zmag_thread;
    int current_i;
  }zmag_t_d;
  PXV_VAR(bool, zmag_continous_polling);
  PXV_VAR(pthread_t, zmag_thread);
  PXV_VAR(zmag_t_d*, zmag_data);
  PXV_FUNC(int, zmag_continous_thread, (void));
  PXV_FUNC(int, launch_zmag_thread, (void));
  PXV_FUNC(float, zmag_find_timestamp_match, (long long t));
  PXV_VAR(int, available_zmag_id);
  # define ZMAG_THREAD_GET_SPEED  1
  # define ZMAG_THREAD_SET_SPEED  2
  # define ZMAG_THREAD_GET_ZMAG_SPEED  4
  # define ZMAG_THREAD_SET_ZMAG_SPEED  8
  # define ZMAG_LOCKED  16

  //PXV_FUNC(int, , (unsigned long t));
#endif

PXV_VAR(int, bead_search);
PXV_VAR(float, bead_search_high);
PXV_VAR(float, bead_search_low);


PXV_VAR(float, n_rota);
PXV_VAR(float, n_rot_offset);
PXV_VAR(float, n_magnet_z);
PXV_VAR(float, n_magnet_offset);
PXV_VAR(float, v_rota);
PXV_VAR(float, v_mag);
PXV_VAR(float, absolute_maximum_zmag);
PXV_VAR(double, applied_force);
PXV_VAR(double, alpha);
PXV_VAR(double, mean_extension);
PXV_VAR(double, kx);
PXV_VAR(double, ky);
PXV_VAR(double, kz);
PXV_VAR(double, taux);
PXV_VAR(double, tauy);
PXV_VAR(double, tauz);

PXV_VAR(double, betax);             // exp (-dt/tau)
PXV_VAR(double, betay);
PXV_VAR(double, betaz);
PXV_VAR(double, xnoise);                // the magnitude of noise
PXV_VAR(double, ynoise);
PXV_VAR(double, znoise);

PXV_VAR(BITMAP*, search_bitmap);

PXV_VAR(float, n_rota_inst);
PXV_VAR(float, n_magnet_z_inst);
PXV_VAR(float, last_n_rota);
PXV_VAR(float, last_n_magnet_z);


PXV_VAR(int, Zmag_Ticks_Per_Mm);
PXV_VAR(int, Rot_Ticks_Per_Turn);

// this function check that the interface initialized fine (return 0 on sucess)
PXV_FUNCPTR(int, init_magnets_OK,(void));
PXV_FUNCPTR(int, set_rot_value_raw,(float rot));
PXV_FUNC(int, set_rot_value,(float rot));
PXV_FUNCPTR(int, set_rot_ref_value, (float rot));
PXV_FUNC(float, read_rot_value, (void));
PXV_FUNCPTR(float, read_rot_value_raw, (void));
PXV_FUNCPTR(int, set_magnet_z_value_raw, (float z));
PXV_FUNC(int, set_magnet_z_value, (float z));
PXV_FUNCPTR(int, set_magnet_z_ref_value, (float z));
PXV_FUNC(int, set_magnet_z_factor_value, (float fac));
PXV_FUNCPTR(float, read_magnet_z_value_raw, (void));
PXV_FUNC(float, read_magnet_z_value, (void));
PXV_FUNCPTR(int, set_magnet_z_value_and_wait_raw, (float pos));
PXV_FUNC(int, set_magnet_z_value_and_wait, (float pos));
PXV_FUNCPTR(int, set_rot_value_and_wait_raw, (float rot));
PXV_FUNC(int, set_rot_value_and_wait, (float rot));
PXV_FUNCPTR( int, set_motors_speed, (float v));
PXV_FUNCPTR(float, get_motors_speed, (void));


PXV_FUNCPTR(int, set_zmag_motor_speed, (float v));
PXV_FUNCPTR(float,  get_zmag_motor_speed, (void));
PXV_FUNCPTR(int, set_zmag_motor_pid, (int num, int val));
PXV_FUNCPTR(int,  get_zmag_motor_pid, (int num));
PXV_FUNCPTR(int, set_rotation_motor_pid, (int num, int val));
PXV_FUNCPTR(int,  get_rotation_motor_pid, (int num));


PXV_FUNCPTR(int, read_magnet_z_value_and_status_raw, (float *z, float *vcap, int *limit, int *vcap_servo));
PXV_FUNC(int, read_magnet_z_value_and_status, (float *z, float *vcap, int *limit, int *vcap_servo));

PXV_FUNCPTR(float,  get_Vcap_min, (void));
PXV_FUNCPTR(int,   set_Vcap_min, (float volts));



PXV_FUNCPTR(int, request_set_rot_value_raw, (int image_n, float rot,  unsigned long *t0));
PXV_FUNC(int, request_set_rot_value, (int image_n, float rot,  unsigned long *t0));
PXV_FUNCPTR(int, check_set_rot_request_answer, (char *answer, int im_n, float rot));

PXV_FUNCPTR(int, request_set_magnet_z_value_raw, (int image_n, float z, unsigned long *t0));
PXV_FUNC(int, request_set_magnet_z_value, (int image_n, float z, unsigned long *t0));
PXV_FUNCPTR(int, check_set_zmag_request_answer, (char *answer, int im_n, float pos));

PXV_FUNCPTR(int, request_read_rot_value, (int image_n, unsigned long *t0));
PXV_FUNC(float, read_rot_from_request_answer, (char *answer, int im_n, float val, int *error));
PXV_FUNCPTR(float, read_rot_raw_from_request_answer, (char *answer, int im_n, float val, int *error));

PXV_FUNCPTR(int, request_read_magnet_z_value, (int image_n, unsigned long *t0));
PXV_FUNC(float, read_zmag_from_request_answer, (char *answer, int im_n, float val, int *error, float *vacp));
PXV_FUNCPTR(float, read_zmag_raw_from_request_answer, (char *answer, int im_n, float val, int *error, float *vacp));

PXV_FUNCPTR(int, request_set_rot_ref_value, (int image_n, float rot,  unsigned long *t0));
PXV_FUNCPTR(int, check_set_rot_ref_request_answer, (char *answer, int im_n, float rot));

PXV_FUNCPTR(int, request_set_zmag_ref_value, (int image_n, float pos,  unsigned long *t0));
PXV_FUNCPTR(int, check_set_zmag_ref_request_answer, (char *answer, int im_n, float pos));




/*
PXV_FUNCPTR( int, go_and_dump_z_magnet, (float z));
PXV_FUNCPTR(int, go_wait_and_dump_z_magnet, (float zmag));
PXV_FUNCPTR(int, go_and_dump_rot, (float r));
PXV_FUNCPTR(int, go_wait_and_dump_rot, (float r));
PXV_FUNCPTR( int, go_wait_and_dump_log_specific_rot, (float r, char *log));
PXV_FUNCPTR(int, go_wait_and_dump_log_specific_z_magnet, (float zmag, char *log));
*/

PXV_FUNC(int, set_motor_speed_to_max, (void));
PXV_FUNC(int, set_motor_speed_to_min, (void));

PXV_FUNC( int, go_and_dump_z_magnet, (float z));
PXV_FUNC(int, go_wait_and_dump_z_magnet, (float zmag));
PXV_FUNC(int, go_and_dump_rot, (float r));
PXV_FUNC(int, go_wait_and_dump_rot, (float r));
PXV_FUNC( int, go_wait_and_dump_log_specific_rot, (float r, char *log));
PXV_FUNC(int, go_wait_and_dump_log_specific_z_magnet, (float zmag, char *log));

PXV_FUNC(int, magnetscontrol_main, (int argc, char **argv));

PXV_FUNC(int, move_mag, (void));

PXV_FUNC(float, zmag_to_force,(int nb, float zmag));
PXV_FUNC(float, force_to_tau_x_in_fr,(float force, int bead_type));

PXV_FUNC(float, convert_only_zmag_to_force, (float zmag, int nb));

PXV_FUNC(MENU*, magnetscontrol_plot_menu, (void));
PXV_FUNC(float, convert_zmag_to_force, (float zmag, float rot, int nb));
PXV_FUNC(float, convert_force_to_zmag, (float force,  int nb));
PXV_FUNC(int, dialog_set_magnets_rot, (char *rots));
PXV_FUNC(int, dialog_set_magnets_z, (char *zmags));
PXV_FUNC(int, dialog_set_magnets_z_to_force, (char *forces));
PXV_FUNC(int, shut_down_magnets, (void));

PXV_FUNC(float, get_zmag_motor_speed_from_pid, (void));
PXV_FUNC(int, switch_motors_pid,(int state));
PXV_VAR(int, zmag_moving);
PXV_VAR(int, rot_moving);
PXV_VAR(int, zmag_transient);
PXV_VAR(int, rot_transient);
PXV_VAR(float, Vcap_min);

# else

#ifdef ZMAG_THREAD
# include <pthread.h>
#ifdef XV_WIN32
#include <windows.h>
#endif
  typedef struct zmag_thread_data
  {
    long long *timestamp;
    float *zmag_value;
    int *limit;
    int zmag_thread_buffer;
    int period_zmag_thread;
    int current_i;
  }zmag_t_d;
  PXV_VAR(bool, zmag_continous_polling);
  PXV_VAR(pthread_t, zmag_thread);
  PXV_VAR(zmag_t_d*, zmag_data);
  PXV_FUNC(int, zmag_continous_thread, (void));
  PXV_FUNC(int, launch_zmag_thread, (void));
  PXV_FUNC(float, zmag_find_timestamp_match, (long long t));
  # define ZMAG_THREAD_GET_SPEED  1
  # define ZMAG_THREAD_SET_SPEED  2
  # define ZMAG_THREAD_GET_ZMAG_SPEED  4
  # define ZMAG_THREAD_SET_ZMAG_SPEED  8
  # define ZMAG_LOCKED  16
  //PXV_FUNC(int, , (unsigned long t));
#endif

int zmag_moving = 0, rot_moving = 0, zmag_transient = 5, rot_transient = 8;

int bead_search = 0;
float bead_search_high = 8.5;
float bead_search_low = 6;

BITMAP *search_bitmap = NULL;

float n_rota = 0;       // this is the target rotation position which may not be reached yet
float n_rot_offset = 0; // this allow to change rotation reference
float n_rota_inst = 0;  // this is the actual rotation position
float v_rota = 1;       // this is the rotation velocity in turns per sec

float n_magnet_z = 0, n_magnet_z_inst = 0, n_magnet_offset = 0;
float last_n_rota = 0;
float last_n_magnet_z = 0;

float Vcap_min = 0;

int Zmag_Ticks_Per_Mm = 8000;
int Rot_Ticks_Per_Turn = 8000;


float v_mag = 2;   // in mm per sec
float absolute_maximum_zmag = 20;
double applied_force = 0;
double alpha;
double mean_extension = 0;
double kx;                // stiffness in pN/microns
double ky;
double kz;
double taux;              // characteristic time in s
double tauy;
double tauz;
double betax = 0;             // exp (-dt/tau)
double betay = 0;
double betaz = 0;
double xnoise = 0;            // the magnitude of noise
double ynoise = 0;
double znoise = 0;

int     (*init_magnets_OK)(void);
int 	(*set_rot_value_raw)(float rot);
int 	set_rot_value(float rot);
int 	(*set_rot_ref_value)(float rot);
float 	(*read_rot_value_raw)(void);
float 	read_rot_value(void);

int 	(*set_magnet_z_value_raw)(float z);
int     set_magnet_z_value(float z);

int set_magnet_z_factor_value(float fac); // tv 090317 : translation factor in dummy mode

int 	(*set_magnet_z_ref_value)(float z);
float 	(*read_magnet_z_value_raw)(void);
float 	read_magnet_z_value(void);

int	set_magnet_z_value_and_wait(float pos);
int	(*set_magnet_z_value_and_wait_raw)(float pos);

int	(*set_rot_value_and_wait_raw)(float rot);
int	set_rot_value_and_wait(float rot);

int 	(*set_motors_speed)(float v);
float 	(*get_motors_speed)(void);

int     (*set_zmag_motor_speed) (float v);
float   (*get_zmag_motor_speed) (void);
int     (*set_zmag_motor_pid) (int num, int val);
int     (*get_zmag_motor_pid) (int num);
int     (*set_rotation_motor_pid) (int num, int val);
int     (*get_rotation_motor_pid) (int num);


/*
int	(*go_and_dump_z_magnet)(float z);
int	(*go_wait_and_dump_z_magnet)(float zmag);
int	(*go_and_dump_rot)(float r);
int	(*go_wait_and_dump_rot)(float r);

int	(*go_wait_and_dump_log_specific_rot)(float r, char *log);
int	(*go_wait_and_dump_log_specific_z_magnet)(float zmag, char *log);
*/

int     (*set_z_origin)(float val, int dir);

int     (*has_magnets_memory)(void);

int     (*read_magnet_z_value_and_status_raw)(float *z, float *vcap, int *limit, int *vcap_servo);
int     read_magnet_z_value_and_status(float *z, float *vcap, int *limit, int *vcap_servo);

float  (*get_Vcap_min)(void);
int   (*set_Vcap_min)(float volts);


int	go_and_dump_z_magnet(float z);
int	go_wait_and_dump_z_magnet(float zmag);
int	go_and_dump_rot(float r);
int	go_wait_and_dump_rot(float r);

int	go_wait_and_dump_log_specific_rot(float r, char *log);
int	go_wait_and_dump_log_specific_z_magnet(float zmag, char *log);



int 	(*request_set_rot_value_raw)(int image_n, float rot,  unsigned long *t0);
int 	request_set_rot_value(int image_n, float rot,  unsigned long *t0);
int     (*check_set_rot_request_answer) (char *answer, int im_n, float rot);

int 	(*request_set_magnet_z_value_raw)(int image_n, float z, unsigned long *t0);
int     request_set_magnet_z_value(int image_n, float z, unsigned long *t0);
int     (*check_set_zmag_request_answer) (char *answer, int im_n, float pos);

int 	(*request_read_rot_value)(int image_n, unsigned long *t0);
float   read_rot_from_request_answer(char *answer, int im_n, float val, int *error);
float   (*read_rot_raw_from_request_answer)(char *answer, int im_n, float val, int *error);

int 	(*request_read_magnet_z_value)(int image_n, unsigned long *t0);
float   read_zmag_from_request_answer(char *answer, int im_n, float val, int *error, float *vacp);
float   (*read_zmag_raw_from_request_answer)(char *answer, int im_n, float val, int *error, float *vacp);

int 	(*request_set_rot_ref_value)(int image_n, float rot,  unsigned long *t0);
int     (*check_set_rot_ref_request_answer)(char *answer, int im_n, float rot);

int 	(*request_set_zmag_ref_value)(int image_n, float pos,  unsigned long *t0);
int     (*check_set_zmag_ref_request_answer)(char *answer, int im_n, float pos);


int switch_motors_pid(int state);


PXV_FUNC(int, grab_zmag_pid_param, (int param));
PXV_FUNC(int, chg_zmag_pid_param, (int param, int value));


# endif

float 	what_is_present_rot_value(void);
float 	what_is_present_z_mag_value(void);

# endif
