#ifndef _REAL_TIME_AVG_H_
#define _REAL_TIME_AVG_H_


# define live_video_info real_time_info

# define REAL_TIME_BUFFER_SIZE 4096


# define BUF_FREEZED 2
# define BUF_CHANGING 1
# define BUF_UNLOCK 0


# define USER_MOVING_OBJ     0x00000010 
# define OBJ_MOVING          0x00000020 
# define H_LEVELS            2560
# define H_FACTOR            10
# define BP_LEVELS           640
# define BP_FACTOR            10

typedef struct phase_lockin
{
                
  int phase2[50*REAL_TIME_BUFFER_SIZE/3];
  int phase1[50*REAL_TIME_BUFFER_SIZE/3];
  //int dephi[50*REAL_TIME_BUFFER_SIZE/3];
  long long imt[50*REAL_TIME_BUFFER_SIZE/3]; // the image absolute time
  long long imt0;
  int c_i, n_i, m_i, ac_i;          // c_i, index of the current image, n_i size of buffer, m_i size allocated
  //int peri;
  //int n_phi;                        // n_phi time-steps number to lock phase
} ph_lockin;



typedef struct gen_real_time
{
  int (*user_real_time_action)(struct gen_real_time *gt, DIALOG *d, void *data);  // a user callback function       
  int c_i, n_i, m_i, lc_i;          // c_i, index of the current image, n_i size of buffer, m_i size allocated
  int ac_i, lac_i;                  // ac_i actual number of frame since acquisition started + local
  int ac_ri, a_ri;                  // the absolute and relative index of the last pending request
  int ac_pri, a_pri;                // the absolute and relative index of the previous pending request
  int imi[REAL_TIME_BUFFER_SIZE];                         // the image nb
  int imit[REAL_TIME_BUFFER_SIZE];                        // n_inarow
  long long imt[REAL_TIME_BUFFER_SIZE];                   // the image absolute time
  long long imt0;
  long long imtt[REAL_TIME_BUFFER_SIZE];                  // the image absolute time obtained by timer 
  unsigned long imdt[REAL_TIME_BUFFER_SIZE];              // the time spent in the previous function call
  unsigned long imtdt[REAL_TIME_BUFFER_SIZE];             // the time spent before image flip in timer mode
  float obj_pos[REAL_TIME_BUFFER_SIZE];                   // the objective position measured

  int status_flag[REAL_TIME_BUFFER_SIZE];
  float obj_pos_cmd[REAL_TIME_BUFFER_SIZE];               // the objective position command
  char temp_message[REAL_TIME_BUFFER_SIZE];               // a string containing temperature message 
  float focus_cor;                                       // the immersion correction factor in Z
  int im_focus_check;                                    // the number of images between two focus read per
  int com_skip_fr;                                       // communication rate may be different from camera
                                                         // this is the ratio between the two
  O_i *oi_avg;                                           // avg movie 
  // int mc_i;                                              // c_i, index of the current averaged image
  int n_per;                        // number of frames during a modulation period
  int n_avg;                        // number of modulation periods to average                       
  //int x0, y0, wx, wy;             // interest region
  int rec_start, first_rec;         // flag to start a record, to reset after first recording
  int i_start_avg;                  // if rec_start > 0 define the starting image fr averaging
  int th_mod[REAL_TIME_BUFFER_SIZE];
} g_real_time;



# ifdef _REAL_TIME_AVG_C_
struct phase_lockin ph_lock;
g_real_time *real_time_info = NULL;
int px0,px1,py0,py1;
float intensity[REAL_TIME_BUFFER_SIZE];
float prev_focus = 0;

float *mn, *s2;

int fft_used_in_real_timeing = 0;
int fft_used_in_dialog = 0;
# else
PXV_VAR(g_real_time*, real_time_info);
PXV_VAR(int, px0);
PXV_VAR(int, px1);
PXV_VAR(int, py0);
PXV_VAR(int, py1);
PXV_VAR(int,x_center);
PXV_VAR(int,y_center);
PXV_VAR(float, prev_focus);



# endif

PXV_FUNC(g_real_time *, creating_live_video_info, (void));
//PXV_FUNC(MENU *, real_time_avg_menu, (void));
# endif



