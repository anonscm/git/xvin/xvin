/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _OLYMPUS_FOCUS_C_
#define _OLYMPUS_FOCUS_C_

# include "allegro.h"
# include "winalleg.h"
# include "xvin.h"

/* If you include other plug-ins header do it here*/ 
# include "../olympus_serial/olympus_serial.h"

/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "olympus_focus.h"
# include "../action.h"
# include "../fftbtl32n.h"
# include "../fillibbt.h"

# include "../trackBead.h"
# include "../track_util.h"

static int olympus_obj_reference_pos = 0, olympus_obj_pos = 0; // in 10nm units 


int  _init_focus_OK(void)
{
  if(updating_menu_state != 0)	return D_O_K;
  add_plot_treat_menu_item ( "olympus_focus", NULL, olympus_focus_plot_menu(), 0, NULL);
  add_image_treat_menu_item ( "olympus_focus", NULL, olympus_focus_plot_menu(), 0, NULL);

  if (init_pico_serial()) 
    {
      win_printf_OK("Serial port initialisation failed !");
      return D_O_K; 
    }

  return 0;   
}
int _has_focus_memory(void)
{
 return 1;
}





float _read_Z_value_accurate_in_micron(void)
{
  char command[128], answer[128];
  int  set = -1000000000, ret;
  unsigned long len = 0;
  //unsigned long t0;
  DWORD dwErrors;

  
  snprintf(command,128,"2POS?\r\n");
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  //win_printf("Question %s\nAnswer %s",command,answer);
  if (ret != 0) return (ret -50000000);
  if (sscanf(answer,"2POS %d",&set) == 1)
    { 
      if (set != -1000000000)
	{
	  olympus_obj_pos = set;
	  pico_obj_position = (float)(olympus_obj_pos - olympus_obj_reference_pos)/100;
	  return pico_obj_position;
	}
    }
  return -2000000000;
}


int _set_Z_value(float z)  /* in microns */
{
  char command[128], answer[128];
  int dz, ret, im = 0;
  unsigned long len = 0;
  DWORD dwErrors;
  char mov[2];
  
  //purge_com();

  mov[0] = (z > pico_obj_position) ? 'N' : 'F';
  mov[1] = 0;
  dz = (int)(0.5+(100*z)) - olympus_obj_pos + olympus_obj_reference_pos;
  dz = abs(dz);
  snprintf(command,128,"2MOV %s,%d\r\n",mov,dz);
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return (ret -5);
  if (strstr(answer,"2MOV +") == NULL)
    { 
      if (debug)
	{
	  fp_debug = fopen(debug_file,"a");
	  if (fp_debug != NULL)
	    {
	      //if (track_info != NULL) im = track_info->ac_i;
	      fprintf (fp_debug,"Im %d : focus pb setting %g !\n",im,z);
	      fclose(fp_debug);
	    }
	}
      return -1;
    }
  else      
    {
      olympus_obj_pos += (mov[0] == 'N') ? dz : -dz; 
      pico_obj_position = (float)(olympus_obj_pos - olympus_obj_reference_pos)/100;
    }
  return 0;
}



int set_olympus_obj_ref(void)
{
  int i, zr;
  char question[1024];

  if(updating_menu_state != 0)	return D_O_K;
  
  snprintf(question,1024,"Olympus reference focus position setting\n"
	   "Present relative objective position %g microns\n"
	   "Present absolute objective position %d x 10 nm\n"
	   "New reference position (x 10 nm) %%12d\n",pico_obj_position,olympus_obj_pos);
  zr = olympus_obj_reference_pos;

  i = win_scanf(question,&zr);
  if (i == CANCEL) return OFF;
  olympus_obj_reference_pos = zr;
  win_printf("Focus is now at %g",_read_Z_value_accurate_in_micron());
  return D_O_K;
}




int _set_T0_pid(int num, int val)
{
  return 0;
}

int _get_T0_pid(int num)
{
  return 0;
}


int _read_Z_value(void)
{
	return (int)(10*_read_Z_value_accurate_in_micron());	
}
int _set_Z_step(float z)  /* in micron */
{
	pico_Z_step = z;
	return 0;
}

float _read_Z_value_OK(int *error) /* in microns */
{
  return _read_Z_value_accurate_in_micron();
}



int _request_read_temperature_value(int image_n, int t, unsigned long *t0)
{
  return 0;
}

float _read_temperature_from_request_answer(char *answer, int im_n, float val, int *error, int *channel)
{
  float t = 25;
  return t;	
}


int _request_read_focus_value(int image_n, unsigned long *t0)
{
  char command[128];
  
  if (rs232_register(READ_FOCUS_REQUEST,image_n,0)) return 1; // rs232 buisy
  if (t0 != NULL) *t0 = my_uclock();
  snprintf(command,128,"2POS?\r\n");
  Write_serial_port(hCom,command,strlen(command));
  if (t0 != NULL) *t0 = my_uclock()- *t0;
  return 0;
}


float _read_focus_raw_from_request_answer(char *answer, int im_n, float val, int *error)
{
  int set = -1;
  float z = 0;

  *error = 0;

  if (answer != NULL)
    {
      if (sscanf(answer,"2POS %d",&set) == 1)
	{ 
	  if (set != -1000000000)
	    {
	      olympus_obj_pos = set;
	      pico_obj_position = (float)(olympus_obj_pos - olympus_obj_reference_pos)/100;
	      return pico_obj_position;
	    }
	}
     else *error |= PICO_COM_ERROR;
    }
  else *error =  PICO_COM_ERROR;
  return z;	
}

int _request_set_focus_value_raw(int image_n, float z, unsigned long *t0)
{
  char command[128];
  int dz;
  char mov[2];
  
  //purge_com();
  if (t0 != NULL) *t0 = my_uclock();
  if (rs232_register(SET_FOCUS_REQUEST,image_n,z)) return 1;
  mov[0] = (z > pico_obj_position) ? 'N' : 'F';
  mov[1] = 0;
  dz = (int)(0.5+(100*z)) - olympus_obj_pos + olympus_obj_reference_pos;
  dz = abs(dz);
  snprintf(command,128,"2MOV %s,%d\r\n",mov,dz);
  Write_serial_port(hCom,command,strlen(command));
  if (t0 != NULL) *t0 = my_uclock()- *t0;
  return 0;
}
int _check_set_focus_request_answer(char *answer, int im_n, float z)
{

  if (answer == NULL)     return PICO_COM_ERROR;
  if (strstr(answer,"2MOV +") == NULL)
    { 
      if (debug)
	{
	  fp_debug = fopen(debug_file,"a");
	  if (fp_debug != NULL)
	    {
	      //if (track_info != NULL) im = track_info->ac_i;
	      fprintf (fp_debug,"Im %d : focus pb setting %g !\n",im_n,z);
	      fclose(fp_debug);
	    }
	}
      return -1;
    }
  else        return 0;
  return PICO_COM_ERROR;
}


float _read_last_Z_value(void) /* in microns */
{
  return pico_obj_position;	
}

int	_set_Z_obj_accurate(float zstart)
{
  return _set_Z_value(zstart);
}
int _set_Z_value_OK(float z) 
{
  return _set_Z_value(z);
}

int _inc_Z_value(int step)  /* in micron */
{
  _set_Z_value(pico_obj_position + pico_Z_step*step);
  return 0;
}

int _small_inc_Z_value(int up)  
{
  int nstep;
  
  _set_Z_step(0.1);
  nstep = (up>=0)? 1:-1;
  _set_Z_value(pico_obj_position + pico_Z_step*nstep);

  return 0;
}

int _big_inc_Z_value(int up)  
{
  int nstep;
  
  _set_Z_step(0.1);
  nstep = (up>=0)? 10:-10;
  _set_Z_value(pico_obj_position + pico_Z_step*nstep);
  
  return 0;
}

// z 0 to 100
int _set_light(float z)  
{
  return -1;
}

// return from 0 to 100
float _get_light()  
{
  return -1;
}


int _set_temperature(float z)  
{
  return 1;
}

float _get_temperature(int t)  
{
  return 25.0;
}


int read_focus(void)
{
    if(updating_menu_state != 0)	return D_O_K;

    win_printf("focus %g",_read_Z_value_accurate_in_micron());
    return D_O_K;
}


int set_focus(void)
{
  int i;
  float z = pico_obj_position;

  if(updating_menu_state != 0)	return D_O_K;
  
  i = win_scanf("New focus position %f",&z);
  if (i == CANCEL) return OFF;
  _set_Z_value(z);
  //win_printf("focus set at %g",_read_Z_value_accurate_in_micron());
  return D_O_K;
}








MENU *olympus_focus_plot_menu(void)
{
	static MENU mn[32];
	int n_write_read_serial_port(void);

	if (mn[0].text != NULL)	return mn;
	//	add_item_to_menu(mn,"Init serial port", init_port,NULL,0,NULL);
	add_item_to_menu(mn,"Write serial port", write_command_on_serial_port,NULL,0,NULL);
	add_item_to_menu(mn,"Read serial port", read_on_serial_port,NULL,0,NULL);
	//add_item_to_menu(mn,"Write read serial port", write_read_serial_port,NULL,0,NULL);
	add_item_to_menu(mn,"Write read n times", n_write_read_serial_port,NULL,0,NULL);
	add_item_to_menu(mn,"Close serial port", close_serial_port,NULL,0,NULL);
	add_item_to_menu(mn,"Read focus", read_focus,NULL,0,NULL);
	add_item_to_menu(mn,"set focus", set_focus,NULL,0,NULL);
	add_item_to_menu(mn,"set focus origine",set_olympus_obj_ref,NULL,0,NULL);

	return mn;
}

int	olympus_focus_main(int argc, char **argv)
{
  olympus_serial_main(0, NULL);
  if (init_pico_serial()) 
    {
      win_printf_OK("Serial port initialisation failed !");
      return D_O_K; 
    }
  add_plot_treat_menu_item ( "olympus_focus", NULL, olympus_focus_plot_menu(), 0, NULL);
  add_image_treat_menu_item ( "olympus_focus", NULL, olympus_focus_plot_menu(), 0, NULL);
  return D_O_K;
}

int	olympus_focus_unload(int argc, char **argv)
{
	remove_item_to_menu(plot_treat_menu, "olympus_focus", NULL, NULL);
	return D_O_K;
}
#endif

