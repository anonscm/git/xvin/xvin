#include "gui.h"
#include "xvin.h"
#include <allegro.h>
#include "calibration.h"
#include "focus.h"
#include "magnetscontrol.h"
#ifdef RAWHID_V2
#include "rawhid_general/rawhid_general.h"
#endif


extern FONT *small_font;
extern FONT *normalsize_font;
extern FONT *large_font;

char rotation_value[128] = {0};
DIALOG *rotation_dialog = NULL;



char zmag_label[128] = {0};
DIALOG *zmag_label_di = NULL;

char zmag_value[128] = {0};
DIALOG *zmag_dialog = NULL;

char force_value[128] = {0};
DIALOG *force_dialog = NULL;

char focus_value[128] = {0};
DIALOG *focus_dialog = NULL;

char focus_label[128] = {0};
DIALOG *focus_label_di = NULL;

char T0_value[128] = {0};
DIALOG *T0_dialog = NULL;

char Freq_value[128] = {0};
DIALOG *Freq_dialog = NULL;

char Load_value[128] = {0};
DIALOG *Load_dialog = NULL;

char X_value[128] = {0};
DIALOG *X_dialog = NULL;

char Y_value[128] = {0};
DIALOG *Y_dialog = NULL;


char param_label[256] = {0};
DIALOG *param_label_di = NULL;

DIALOG *slider_di = NULL;

/* d_edit_proc:
 *  An editable text object (the dp field points to the string). When it
 *  has the input focus (obtained by clicking on it with the mouse), text
 *  can be typed into this object. The d1 field specifies the maximum
 *  number of characters that it will accept, and d2 is the text cursor
 *  position within the string.
 */
int d_my_edit_proc(int msg, DIALOG *d, int c)
{
    static int ignore_next_uchar = FALSE;
    BITMAP *gui_bmp;
    int last_was_space, new_pos, i, k;
    int f, l, p, w, x, fg, b, scroll;
    char buf[16] = {0};
    char *s = NULL, *t = NULL;
    FONT *oldfont = font;
    ASSERT(d);

    if (d->dp2)
    {
        font = (FONT *)d->dp2;
    }

    gui_bmp = gui_get_screen();
    s = (char *)d->dp;
    l = ustrlen(s);

    if (d->d2 > l)
    {
        d->d2 = l;
    }

    /* calculate maximal number of displayable characters */
    if (d->d2 == l)
    {
        usetc(buf + usetc(buf, ' '), 0);
        x = text_length(font, buf);
    }
    else
    {
        x = 0;
    }

    b = 0;

    for (p = d->d2; p >= 0; p--)
    {
        usetc(buf + usetc(buf, ugetat(s, p)), 0);
        x += text_length(font, buf);
        b++;

        if (x > d->w)
        {
            break;
        }
    }

    if (x <= d->w)
    {
        b = l;
        scroll = FALSE;
    }
    else
    {
        b--;
        scroll = TRUE;
    }

    switch (msg)
    {
    case MSG_START:
        d->d2 = l;
        break;

    case MSG_DRAW:
        fg = (d->flags & D_DISABLED) ? gui_mg_color : d->fg;
        x = 0;

        if (scroll)
        {
            p = d->d2 - b + 1;
            b = d->d2;
        }
        else
        {
            p = 0;
        }

        for (; p <= b; p++)
        {
            f = ugetat(s, p);
            usetc(buf + usetc(buf, (f) ? f : ' '), 0);
            w = text_length(font, buf);

            if (x + w > d->w)
            {
                break;
            }

            f = ((p == d->d2) && (d->flags & D_GOTFOCUS));
            acquire_bitmap(gui_bmp);
            textout_ex(gui_bmp, font, buf, d->x + x, d->y, (f) ? d->bg : fg, (f) ? fg : d->bg);
            release_bitmap(gui_bmp);
            x += w;
        }

        if (x < d->w)
        {
            acquire_bitmap(gui_bmp);
            rectfill(gui_bmp, d->x + x, d->y, d->x + d->w - 1, d->y + text_height(font) - 1, d->bg);
            release_bitmap(gui_bmp);
        }

        break;

    case MSG_CLICK:
        x = d->x;

        if (scroll)
        {
            p = d->d2 - b + 1;
            b = d->d2;
        }
        else
        {
            p = 0;
        }

        for (; p < b; p++)
        {
            usetc(buf + usetc(buf, ugetat(s, p)), 0);
            x += text_length(font, buf);

            if (x > gui_mouse_x())
            {
                break;
            }
        }

        d->d2 = MID(0, p, l);
        object_message(d, MSG_DRAW, 0);
        break;

    case MSG_WANTFOCUS:
    case MSG_LOSTFOCUS:
    case MSG_KEY:
        font = oldfont;
        return D_WANTFOCUS;

    case MSG_CHAR:
        ignore_next_uchar = FALSE;

        if ((c >> 8) == KEY_LEFT)
        {
            if (d->d2 > 0)
            {
                if (key_shifts & KB_CTRL_FLAG)
                {
                    last_was_space = TRUE;
                    new_pos = 0;
                    t = s;

                    for (i = 0; i < d->d2; i++)
                    {
                        k = ugetx(&t);

                        if (uisspace(k))
                        {
                            last_was_space = TRUE;
                        }
                        else if (last_was_space)
                        {
                            last_was_space = FALSE;
                            new_pos = i;
                        }
                    }

                    d->d2 = new_pos;
                }
                else
                {
                    d->d2--;
                }
            }
        }
        else if ((c >> 8) == KEY_RIGHT)
        {
            if (d->d2 < l)
            {
                if (key_shifts & KB_CTRL_FLAG)
                {
                    t = s + uoffset(s, d->d2);

                    for (k = ugetx(&t); uisspace(k); k = ugetx(&t))
                    {
                        d->d2++;
                    }

                    for (; k && !uisspace(k); k = ugetx(&t))
                    {
                        d->d2++;
                    }
                }
                else
                {
                    d->d2++;
                }
            }
        }
        else if ((c >> 8) == KEY_HOME)
        {
            d->d2 = 0;
        }
        else if ((c >> 8) == KEY_END)
        {
            d->d2 = l;
        }
        else if ((c >> 8) == KEY_DEL)
        {
            if (d->d2 < l)
            {
                uremove(s, d->d2);
            }
        }
        else if ((c >> 8) == KEY_BACKSPACE)
        {
            if (d->d2 > 0)
            {
                d->d2--;
                uremove(s, d->d2);
            }
        }
        else if ((c >> 8) == KEY_ENTER)
        {
            if (d->flags & D_EXIT)
            {
                object_message(d, MSG_DRAW, 0);
                font = oldfont;
                return D_CLOSE;
            }
            else
            {
                change_magnets_param(d);
            }

            font = oldfont;
            return D_O_K;
        }
        else if ((c >> 8) == KEY_TAB)
        {
            ignore_next_uchar = TRUE;
            font = oldfont;
            return D_O_K;
        }
        else
        {
            /* don't process regular keys here: MSG_UCHAR will do that */
            break;
        }

        object_message(d, MSG_DRAW, 0);
        font = oldfont;
        return D_USED_CHAR;

    case MSG_UCHAR:
        if ((c >= ' ') && (uisok(c)) && (!ignore_next_uchar))
        {
            if (l < d->d1)
            {
                uinsert(s, d->d2, c);
                d->d2++;
                object_message(d, MSG_DRAW, 0);
            }

            font = oldfont;
            return D_USED_CHAR;
        }

        break;
    }

    font = oldfont;
    return D_O_K;
}


void dotted_rect(int x1, int y1, int x2, int y2, int fg, int bg)
{
    BITMAP *gui_bmp = gui_get_screen();
    int x = ((x1 + y1) & 1) ? 1 : 0;
    int c;
    /* two loops to avoid bank switches */
    acquire_bitmap(gui_bmp);

    for (c = x1; c <= x2; c++)
    {
        putpixel(gui_bmp, c, y1, (((c + y1) & 1) == x) ? fg : bg);
    }

    for (c = x1; c <= x2; c++)
    {
        putpixel(gui_bmp, c, y2, (((c + y2) & 1) == x) ? fg : bg);
    }

    for (c = y1 + 1; c < y2; c++)
    {
        putpixel(gui_bmp, x1, c, (((c + x1) & 1) == x) ? fg : bg);
        putpixel(gui_bmp, x2, c, (((c + x2) & 1) == x) ? fg : bg);
    }

    release_bitmap(gui_bmp);
}



# if !defined(PLAYITSAM) && !defined(PIAS)
/* d_button_proc:
 *  A button object (the dp field points to the text string). This object
 *  can be selected by clicking on it with the mouse or by pressing its
 *  keyboard shortcut. If the D_EXIT flag is set, selecting it will close
 *  the dialog, otherwise it will toggle on and off.
 */
int my_d_button_proc(int msg, DIALOG *d, int c)
{
    BITMAP *gui_bmp = NULL;
    int state1, state2;
    int blackl;
    int swap;
    int g;
    //int switch_motors_pid(int state);
    ASSERT(d);
    (void)c;

    gui_bmp = gui_get_screen();

    switch (msg)
    {
    case MSG_DRAW:
        if (d->flags & D_SELECTED)
        {
            g = 1;
            state1 = d->bg;
            state2 = (d->flags & D_DISABLED) ? gui_mg_color : d->fg;
            state1 = makecol32(255, 255, 255);
            state2 = makecol32(180, 0, 0);
            switch_motors_pid(0);
        }
        else
        {
            g = 0;
            state1 = (d->flags & D_DISABLED) ? gui_mg_color : d->fg;
            state2 = d->bg;
            state1 = makecol32(255, 255, 255);
            state2 = makecol32(0, 180, 0);
            switch_motors_pid(1);
        }

        acquire_bitmap(gui_bmp);
        rectfill(gui_bmp, d->x + 1 + g, d->y + 1 + g, d->x + d->w - 3 + g, d->y + d->h - 3 + g, state2);
        rect(gui_bmp, d->x + g, d->y + g, d->x + d->w - 2 + g, d->y + d->h - 2 + g, state1);

        if (d->flags & D_SELECTED)
        {
            gui_textout_ex(gui_bmp, "Stop", d->x + d->w / 2 + g, d->y + d->h / 2 - text_height(font) / 2 + g, state1, -1, TRUE);
        }
        else
        {
            gui_textout_ex(gui_bmp, "Run", d->x + d->w / 2 + g, d->y + d->h / 2 - text_height(font) / 2 + g, state1, -1, TRUE);
        }

        if (d->flags & D_SELECTED)
        {
            vline(gui_bmp, d->x, d->y, d->y + d->h - 2, d->bg);
            hline(gui_bmp, d->x, d->y, d->x + d->w - 2, d->bg);
        }
        else
        {
            blackl = makecol(0, 0, 0);
            vline(gui_bmp, d->x + d->w - 1, d->y + 1, d->y + d->h - 2, blackl);
            hline(gui_bmp, d->x + 1, d->y + d->h - 1, d->x + d->w - 1, blackl);
        }

        release_bitmap(gui_bmp);

        if ((d->flags & D_GOTFOCUS) &&
                (!(d->flags & D_SELECTED) || !(d->flags & D_EXIT)))
        {
            dotted_rect(d->x + 1 + g, d->y + 1 + g, d->x + d->w - 3 + g, d->y + d->h - 3 + g, state1, state2);
        }

        break;

    case MSG_WANTFOCUS:
        return D_WANTFOCUS;

    case MSG_KEY:

        /* close dialog? */
        if (d->flags & D_EXIT)
        {
            return D_CLOSE;
        }

        /* or just toggle */
        d->flags ^= D_SELECTED;
        object_message(d, MSG_DRAW, 0);
        break;

    case MSG_CLICK:
        /* what state was the button originally in? */
        state1 = d->flags & D_SELECTED;

        if (d->flags & D_EXIT)
        {
            swap = FALSE;
        }
        else
        {
            swap = state1;
        }

        /* track the mouse until it is released */
        while (gui_mouse_b())
        {
            state2 = ((gui_mouse_x() >= d->x) && (gui_mouse_y() >= d->y) &&
                      (gui_mouse_x() < d->x + d->w) && (gui_mouse_y() < d->y + d->h));

            if (swap)
            {
                state2 = !state2;
            }

            /* redraw? */
            if (((state1) && (!state2)) || ((state2) && (!state1)))
            {
                d->flags ^= D_SELECTED;
                state1 = d->flags & D_SELECTED;
                object_message(d, MSG_DRAW, 0);
            }

            /* let other objects continue to animate */
            broadcast_dialog_message(MSG_IDLE, 0);
        }

        /* should we close the dialog? */
        if ((d->flags & D_SELECTED) && (d->flags & D_EXIT))
        {
            d->flags ^= D_SELECTED;
            return D_CLOSE;
        }

        break;
    }

    return D_O_K;
}

# else


/* d_slider_proc:
 *  A slider control object. This object returns a value in d2, in the
 *  range from 0 to d1&0xFFFF. It will display as a vertical slider if h is
 *  greater than or equal to w; otherwise, it will display as a horizontal
 *  slider. dp can contain an optional bitmap to use for the slider handle;
 *  the width of the slider may be set in (d1&0xFFFF0000)>>16
 *  dp2 can contain an optional callback function, which is called each
 *  time d2 changes. The callback function should have the following
 *  prototype:
 *
 *  int function(void *dp3, int d2);
 *
 *  The d_slider_proc object will return the value of the callback function.
 */
int my_d_slider_proc(int msg, DIALOG *d, int c)
{
    BITMAP *gui_bmp = gui_get_screen();
    BITMAP *slhan = NULL;
    int oldpos, newpos;
    int sfg;                /* slider foreground color */
    int vert = TRUE;        /* flag: is slider vertical? */
    int hh = 7;             /* handle height (width for horizontal sliders) */
    int hmar;               /* handle margin */
    int slp;                /* slider position */
    int mp;                 /* mouse position */
    int irange;
    int slx, sly, slh, slw;
    int msx, msy;
    int retval = D_O_K;
    int upkey, downkey;
    int pgupkey, pgdnkey;
    int homekey, endkey;
    int delta;
    fixed slratio, slmax, slpos;
    int (*proc)(void *cbpointer, int d2value);
    int oldval;

    ASSERT(d);

    /* check for slider direction */
    if (d->h < d->w)
    {
        vert = FALSE;
    }

    irange = (vert) ? d->h : d->w;

    /* set up the metrics for the control */
    if (d->dp != NULL)
    {
        slhan = (BITMAP *)d->dp;

        if (vert)
        {
            hh = slhan->h;
        }
        else
        {
            hh = slhan->w;
        }
    }
    else
    {
        hh = (d->d1 & 0xFFFF0000) >> 16;
        hh = (hh < irange && hh >= 7) ? hh : 7;
    }

    hmar = hh / 2;
    irange = (vert) ? d->h : d->w;
    slmax = itofix(irange - hh);
    slratio = slmax / ((d->d1 & 0xFFFF));
    slpos = slratio * d->d2;
    slp = fixtoi(slpos);

    switch (msg)
    {
    case MSG_DRAW:
        sfg = (d->flags & D_DISABLED) ? gui_mg_color : d->fg;
        acquire_bitmap(gui_bmp);

        if (vert)
        {
            rectfill(gui_bmp, d->x, d->y, d->x + d->w / 2 - 2, d->y + d->h - 1, d->bg);
            rectfill(gui_bmp, d->x + d->w / 2 - 1, d->y, d->x + d->w / 2 + 1, d->y + d->h - 1, sfg);
            rectfill(gui_bmp, d->x + d->w / 2 + 2, d->y, d->x + d->w - 1, d->y + d->h - 1, d->bg);
        }
        else
        {
            rectfill(gui_bmp, d->x, d->y, d->x + d->w - 1, d->y + d->h / 2 - 2, d->bg);
            rectfill(gui_bmp, d->x, d->y + d->h / 2 - 1, d->x + d->w - 1, d->y + d->h / 2 + 1, sfg);
            rectfill(gui_bmp, d->x, d->y + d->h / 2 + 2, d->x + d->w - 1, d->y + d->h - 1, d->bg);
        }

        release_bitmap(gui_bmp);

        /* okay, background and slot are drawn, now draw the handle */
        if (slhan)
        {
            if (vert)
            {
                slx = d->x + (d->w / 2) - (slhan->w / 2);
                sly = d->y + (d->h - 1) - (hh + slp);
            }
            else
            {
                slx = d->x + slp;
                sly = d->y + (d->h / 2) - (slhan->h / 2);
            }

            acquire_bitmap(gui_bmp);
            draw_sprite(gui_bmp, slhan, slx, sly);
            release_bitmap(gui_bmp);
        }
        else
        {
            /* draw default handle */
            if (vert)
            {
                slx = d->x;
                sly = d->y + (d->h) - (hh + slp);
                slw = d->w - 1;
                slh = hh - 1;
            }
            else
            {
                slx = d->x + slp;
                sly = d->y;
                slw = hh - 1;
                slh = d->h - 1;
            }

            /* draw body */
            acquire_bitmap(gui_bmp);
            rectfill(gui_bmp, slx + 2, sly, slx + (slw - 2), sly + slh, sfg);
            vline(gui_bmp, slx + 1, sly + 1, sly + slh - 1, sfg);
            vline(gui_bmp, slx + slw - 1, sly + 1, sly + slh - 1, sfg);
            vline(gui_bmp, slx, sly + 2, sly + slh - 2, sfg);
            vline(gui_bmp, slx + slw, sly + 2, sly + slh - 2, sfg);
            vline(gui_bmp, slx + 1, sly + 2, sly + slh - 2, d->bg);
            hline(gui_bmp, slx + 2, sly + 1, slx + slw - 2, d->bg);
            putpixel(gui_bmp, slx + 2, sly + 2, d->bg);
            release_bitmap(gui_bmp);
        }

        if (d->flags & D_GOTFOCUS)
        {
            dotted_rect(d->x, d->y, d->x + d->w - 1, d->y + d->h - 1, sfg, d->bg);
        }

        break;

    case MSG_WANTFOCUS:
    case MSG_LOSTFOCUS:
        return D_WANTFOCUS;

    case MSG_KEY:
        if (!(d->flags & D_GOTFOCUS))
        {
            return D_WANTFOCUS;
        }
        else
        {
            return D_O_K;
        }

    case MSG_CHAR:
        /* handle movement keys to move slider */
        c >>= 8;

        if (vert)
        {
            upkey = KEY_UP;
            downkey = KEY_DOWN;
            pgupkey = KEY_PGUP;
            pgdnkey = KEY_PGDN;
            homekey = KEY_END;
            endkey = KEY_HOME;
        }
        else
        {
            upkey = KEY_RIGHT;
            downkey = KEY_LEFT;
            pgupkey = KEY_PGDN;
            pgdnkey = KEY_PGUP;
            homekey = KEY_HOME;
            endkey = KEY_END;
        }

        if (c == upkey)
        {
            delta = 1;
        }
        else if (c == downkey)
        {
            delta = -1;
        }
        else if (c == pgdnkey)
        {
            delta = -d->d1 / 16;
        }
        else if (c == pgupkey)
        {
            delta = d->d1 / 16;
        }
        else if (c == homekey)
        {
            delta = -d->d2;
        }
        else if (c == endkey)
        {
            delta = d->d1 - d->d2;
        }
        else
        {
            delta = 0;
        }

        if (delta)
        {
            oldpos = slp;
            oldval = d->d2;

            while (1)
            {
                d->d2 = d->d2 + delta;
                slpos = slratio * d->d2;
                slp = fixtoi(slpos);

                if ((slp != oldpos) || (d->d2 <= 0) || (d->d2 >= d->d1))
                {
                    break;
                }
            }

            if (d->d2 < 0)
            {
                d->d2 = 0;
            }

            if (d->d2 > d->d1)
            {
                d->d2 = d->d1;
            }

            retval = D_USED_CHAR;

            if (d->d2 != oldval)
            {
                /* call callback function here */
                if (d->dp2)
                {
                    proc = (int (*)(void *, int)) d->dp2;
                    retval |= (*proc)(d->dp3, d->d2);
                }

                object_message(d, MSG_DRAW, 0);
            }
        }

        break;

    case MSG_WHEEL:
        oldval = d->d2;
        d->d2 = MID(0, d->d2 + c, d->d1);

        if (d->d2 != oldval)
        {
            /* call callback function here */
            if (d->dp2)
            {
                proc = (int (*)(void *, int)) d->dp2;
                retval |= (*proc)(d->dp3, d->d2);
            }

            object_message(d, MSG_DRAW, 0);
        }

        break;

    case MSG_CLICK:
        /* track the mouse until it is released */
        mp = slp;

        while (gui_mouse_b())
        {
            msx = gui_mouse_x();
            msy = gui_mouse_y();
            oldval = d->d2;

            if (vert)
            {
                mp = (d->y + d->h - hmar) - msy;
            }
            else
            {
                mp = msx - (d->x + hmar);
            }

            if (mp < 0)
            {
                mp = 0;
            }

            if (mp > irange - hh)
            {
                mp = irange - hh;
            }

            slpos = itofix(mp);
            slmax = fixdiv(slpos, slratio);
            newpos = fixtoi(slmax);

            if (newpos != oldval)
            {
                d->d2 = newpos;

                /* call callback function here */
                if (d->dp2 != NULL)
                {
                    proc = (int (*)(void *, int)) d->dp2;
                    retval |= (*proc)(d->dp3, d->d2);
                }

                object_message(d, MSG_DRAW, 0);
            }

            /* let other objects continue to animate */
            broadcast_dialog_message(MSG_IDLE, 0);
        }

        break;
    }

    return retval;
}




# endif

int         change_magnets_param(DIALOG *d)
{
    (void)d;
# if !defined(PLAYITSAM) && !defined(PIAS)

    if (d == force_dialog)
    {
        dialog_set_magnets_z_to_force((char *) d->dp);
    }
    else if (d == zmag_dialog)
    {
        dialog_set_magnets_z((char *) d->dp);
    }
    else if (d == rotation_dialog)
    {
        dialog_set_magnets_rot((char *) d->dp);
    }
    else if (d == focus_dialog)
    {
        dialog_set_focus((char *) d->dp);
    }
    #ifdef RAWHID_V2
    else if (d == X_dialog)
    {
      dialog_set_X((char *) d->dp);
    }
    #endif
    else if (d == Y_dialog)
    {
      my_set_window_title("booh)à;");
        //dialog_set_X((char *) d->dp);
    }

# endif
    return 0;
}

DIALOG *get_time_slider(void)
{
    return slider_di;
}




int    generate_plot_dialog(int w, int h)
{
    DIALOG *di = NULL;
    int xpos = 0;
    int ypos = 0;
    int cur_text_len = 0;
//    int i;
    (void)h;
    (void)w;

    di = attach_new_item_to_dialog(d_clear_proc, 0, 0, 0, 0);

    if (di == NULL)
    {
        allegro_message("dialog pb");
        return 1;
    }

    set_dialog_size_and_color(di, 0, 0, 900, 700, 0, 0);
    di = attach_new_item_to_dialog(xvin_d_menu_proc, 0, 0, 0, 0);

    if (di == NULL)
    {
        allegro_message("dialog pb");
        return 1;
    }

    set_dialog_size_and_color(di, 0, 0, 0, normalsize_font_height + 10, 0, 32);
    set_dialog_properties(di, plot_menu, NULL, NULL);
    di = attach_new_item_to_dialog(d_draw_Op_proc, 0, 0, 0, 0);

    if (di == NULL)
    {
        allegro_message("dialog pb");
        return 1;
    }

    set_dialog_size_and_color(di, 0, normalsize_font_height + 10 + large_font_height * 1.2, 900, 668, 255, 0); //19
    ypos = normalsize_font_height + 10;
    xpos = 0;
# if !defined(PLAYITSAM) && !defined(PIAS)
    //start start/stop button
    di = attach_new_item_to_dialog(my_d_button_proc, 0, 0, 127, 0);

    if (di == NULL)
    {
        allegro_message("dialog pb");
        return 1;
    }

    cur_text_len = text_length(large_font, "Stop");
    set_dialog_size_and_color(di, xpos, ypos, cur_text_len, large_font_height,  makecol32(0, 0, 0), makecol32(0, 180, 0));
    set_dialog_properties(di, (char *) "Stop", large_font, NULL);
    xpos = cur_text_len;
# endif
    //start Zmag label
    di = attach_new_item_to_dialog(d_text_proc, 0, 0, 127, 0);

    if (di == NULL)
    {
        allegro_message("dialog pb");
        return 1;
    }
#ifdef RAWHID_V2
    snprintf(zmag_label, 128, "Zmag(mm)=");
#else
    snprintf(zmag_label, 128, "Magnet Z(mm)=");
#endif
    cur_text_len = text_length(large_font, zmag_label);
    set_dialog_size_and_color(di, xpos, ypos, cur_text_len, large_font_height,  makecol32(255, 40, 0), makecol32(180, 180,
                              180));
    set_dialog_properties(di, zmag_label, large_font, NULL);
    zmag_label_di = di;
    //start Zmag value
    snprintf(zmag_value, 128, "000.000 ");
    di = attach_new_item_to_dialog(d_my_edit_proc, 0, 0, 127, 0);

    if (di == NULL)
    {
        allegro_message("dialog pb");
        return 1;
    }

    //set_dialog_size_and_color(di, xpos+40, 0, 80, 19,  makecol32(225, 40, 0), makecol32(180, 180, 180));
    xpos += cur_text_len;
    cur_text_len = text_length(large_font, zmag_value);
    set_dialog_size_and_color(di, xpos, ypos, cur_text_len, large_font_height,  makecol32(40, 225, 40), makecol32(180, 180,
                              180));
    set_dialog_properties(di, zmag_value,  large_font, NULL);
    zmag_dialog = di;
    //start estimated force label
    di = attach_new_item_to_dialog(d_text_proc, 0, 0, 127, 0);

    if (di == NULL)
    {
        allegro_message("dialog pb");
        return 1;
    }

    //xpos = ((w - 4*120 - 130) > 680) ? w - 2*120 - 130 : w - 120;
    //xpos = (xpos < (680+240)) ? (680+240) : xpos;
    xpos += cur_text_len;
    cur_text_len = text_length(large_font, "Estimated Force(pN)=");
    set_dialog_size_and_color(di, xpos, ypos, cur_text_len, large_font_height,  makecol32(255, 40, 0), makecol32(180, 180,
                              180));
    set_dialog_properties(di, (char *) "Estimated Force(pN)=", large_font, NULL);
    //start estimated force value
    snprintf(force_value, 128, "000.000 ");
    di = attach_new_item_to_dialog(d_my_edit_proc, 0, 0, 127, 0);

    if (di == NULL)
    {
        allegro_message("dialog pb");
        return 1;
    }

    xpos += cur_text_len;
    cur_text_len = text_length(large_font, force_value);
    set_dialog_size_and_color(di, xpos, ypos, cur_text_len, large_font_height,  makecol32(225, 40, 0), makecol32(180, 180,
                              180));
    set_dialog_properties(di, force_value,  large_font, NULL);
    force_dialog = di;
    //start rotation label
    di = attach_new_item_to_dialog(d_text_proc, 0, 0, 127, 0);

    if (di == NULL)
    {
        allegro_message("dialog pb");
        return 1;
    }

    //xpos = ((w - 4*120 - 130) > 680) ? w - 3*120 - 130 : w - 2*120;
    //xpos = (xpos < (680+120)) ? (680+120) : xpos;
    //set_dialog_size_and_color(di, xpos, 0, 40, 19,  makecol32(255, 40, 0), makecol32(180, 180, 180));
#ifndef RAWHID_V2
    xpos += cur_text_len;
    cur_text_len = text_length(large_font, "Rotation =");
    set_dialog_size_and_color(di, xpos, ypos, cur_text_len, large_font_height,  makecol32(255, 40, 0), makecol32(180, 180,
                              180));
    set_dialog_properties(di, (char *) "Rotation =", large_font, NULL);
    //start rotation value
    snprintf(rotation_value, 128, "000.000 ");
    di = attach_new_item_to_dialog(d_my_edit_proc, 0, 0, 127, 0);

    if (di == NULL)
    {
        allegro_message("dialog pb");
        return 1;
    }

    xpos += cur_text_len;
    cur_text_len = text_length(large_font, rotation_value);
    set_dialog_size_and_color(di, xpos, ypos, cur_text_len, large_font_height,  makecol32(225, 40, 0), makecol32(180, 180,
                              180));
    set_dialog_properties(di, rotation_value,  large_font, NULL);
    rotation_dialog = di;
    //if ((w - 4*120 - 130) > 680)
    //{
    //start focus label
    di = attach_new_item_to_dialog(d_text_proc, 0, 0, 127, 0);

    if (di == NULL)
    {
        allegro_message("dialog pb");
        return 1;
    }
#endif
    //xpos =  w - 120 - 130;
    xpos += cur_text_len;
    snprintf(focus_label, 128, "Focus =");
    cur_text_len = text_length(large_font, focus_label);
    set_dialog_size_and_color(di, xpos, ypos, cur_text_len, large_font_height,  makecol32(255, 40, 0), makecol32(180, 180,
                              180));
    set_dialog_properties(di, focus_label, large_font, NULL);
    focus_label_di = di;
    //start focus value
    snprintf(focus_value, 128, "0000.000");
    di = attach_new_item_to_dialog(d_my_edit_proc, 0, 0, 127, 0);

    if (di == NULL)
    {
        allegro_message("dialog pb");
        return 1;
    }

    xpos += cur_text_len;
    cur_text_len = text_length(large_font, focus_value);
    set_dialog_size_and_color(di, xpos, ypos, cur_text_len, large_font_height,  makecol32(225, 40, 0), makecol32(180, 180,
                              180));
    set_dialog_properties(di, focus_value,  large_font, NULL);
    focus_dialog = di;
# if !defined(PLAYITSAM) && !defined(PIAS)
    //start Temp label
    di = attach_new_item_to_dialog(d_text_proc, 0, 0, 127, 0);

    if (di == NULL)
    {
        allegro_message("dialog pb");
        return 1;
    }

    //xpos =  w - 120;
    xpos += cur_text_len;
    cur_text_len = text_length(large_font, "Temp0 =");
    set_dialog_size_and_color(di, xpos, ypos, cur_text_len, large_font_height,  makecol32(255, 40, 0), makecol32(180, 180,
                              180));
    set_dialog_properties(di, (char *) "Temp0 =", large_font, NULL);
    snprintf(T0_value, 128, "000.000");
    //start Temp value
    di = attach_new_item_to_dialog(d_my_edit_proc, 0, 0, 127, 0);

    if (di == NULL)
    {
        allegro_message("dialog pb");
        return 1;
    }

    xpos += cur_text_len;
    cur_text_len = text_length(large_font, T0_value);
    set_dialog_size_and_color(di, xpos, ypos, cur_text_len, large_font_height,  makecol32(225, 40, 0), makecol32(180, 180,
                              180));
    set_dialog_properties(di, T0_value,  large_font, NULL);
    T0_dialog = di;
# else
    //start Temp label
    di = attach_new_item_to_dialog(d_text_proc, 0, 0, 127, 0);

    if (di == NULL)
    {
        allegro_message("dialog pb");
        return 1;
    }

    //xpos =  w - 120;
    xpos += cur_text_len;
    cur_text_len = text_length(large_font, "Temp. =");
    set_dialog_size_and_color(di, xpos, ypos, cur_text_len, large_font_height,  makecol32(255, 40, 0), makecol32(180, 180,
                              180));
    set_dialog_properties(di, (char *) "Temp. =", large_font, NULL);
    //start Temp value
    snprintf(T0_value, 128, "000.000 ");
    di = attach_new_item_to_dialog(d_my_edit_proc, 0, 0, 127, 0);

    if (di == NULL)
    {
        allegro_message("dialog pb");
        return 1;
    }

    xpos += cur_text_len;
    cur_text_len = text_length(large_font, T0_value);
    set_dialog_size_and_color(di, xpos, ypos, cur_text_len, large_font_height,  makecol32(225, 40, 0), makecol32(180, 180,
                              180));
    set_dialog_properties(di, T0_value,  large_font, NULL);
    T0_dialog = di;
#endif
    //start Freq display the title
    di = attach_new_item_to_dialog(d_text_proc, 0, 0, 127, 0);

    if (di == NULL)
    {
        allegro_message("dialog pb");
        return 1;
    }

    xpos += cur_text_len;
    cur_text_len = text_length(large_font, "Freq. (Hz)");
    set_dialog_size_and_color(di, xpos, ypos, cur_text_len, large_font_height,  makecol32(255, 40, 0), makecol32(180, 180,
                              180));
    set_dialog_properties(di, (char *) "Freq. (Hz)", large_font, NULL);
    //start Freq display the value
    snprintf(Freq_value, 128, "000.00 ");
    di = attach_new_item_to_dialog(d_my_edit_proc, 0, 0, 127, 0);

    if (di == NULL)
    {
        allegro_message("dialog pb");
        return 1;
    }

    xpos += cur_text_len;
    cur_text_len = text_length(large_font, Freq_value);
    set_dialog_size_and_color(di, xpos, ypos, cur_text_len, large_font_height,  makecol32(225, 40, 0), makecol32(180, 180,
                              180));
    set_dialog_properties(di, Freq_value,  large_font, NULL);
    Freq_dialog = di;


# if !defined(PLAYITSAM) && !defined(PIAS)
    //start load display title
    di = attach_new_item_to_dialog(d_text_proc, 0, 0, 127, 0);

    if (di == NULL)
    {
        allegro_message("dialog pb");
        return 1;
    }

    xpos += cur_text_len;
    cur_text_len = text_length(large_font, "Load (%)");
    set_dialog_size_and_color(di, xpos, ypos, cur_text_len, large_font_height,  makecol32(255, 40, 0), makecol32(180, 180,
                              180));
    set_dialog_properties(di, (char *) "Load (%)", large_font, NULL);
    //start load display value
    snprintf(Load_value, 128, "000.00%%");
    di = attach_new_item_to_dialog(d_my_edit_proc, 0, 0, 127, 0);

    if (di == NULL)
    {
        allegro_message("dialog pb");
        return 1;
    }

    xpos += cur_text_len;
    cur_text_len = text_length(large_font, Load_value);
    set_dialog_size_and_color(di, xpos, ypos, cur_text_len, large_font_height,  makecol32(225, 40, 0), makecol32(180, 180,
                              180));
    set_dialog_properties(di, Load_value,  large_font, NULL);
    Load_dialog = di;
    di = attach_new_item_to_dialog(d_text_proc, 0, 0, 127, 0);

    if (di == NULL)
    {
        allegro_message("dialog pb");
        return 1;
    }

#ifdef RAWHID_V2
  xpos += cur_text_len;
  cur_text_len = text_length(large_font, "X(mm)");
  set_dialog_size_and_color(di, xpos, ypos, cur_text_len, large_font_height,  makecol32(255, 40, 0), makecol32(180, 180, 180));
  set_dialog_properties(di, (char *) "X(mm)", large_font, NULL);
  snprintf(X_value, 128, "00.000");
  di = attach_new_item_to_dialog(d_my_edit_proc, 0, 0, 127, 0);
  if (di == NULL)
  {
      allegro_message("dialog pb");
      return 1;
  }
  xpos += cur_text_len;
  cur_text_len = text_length(large_font, X_value);
  set_dialog_size_and_color(di, xpos, ypos, cur_text_len, large_font_height,  makecol32(225, 40, 0), makecol32(180, 180, 180));
  set_dialog_properties(di, X_value,  large_font, NULL);
  X_dialog = di;
  di = attach_new_item_to_dialog(d_text_proc, 0, 0, 127, 0);
  if (di == NULL)
  {
      allegro_message("dialog pb");
      return 1;
  }

  xpos += cur_text_len;
  cur_text_len = text_length(large_font, "Y(mm)");
  set_dialog_size_and_color(di, xpos, ypos, cur_text_len, large_font_height,  makecol32(255, 40, 0), makecol32(180, 180, 180));
  set_dialog_properties(di, (char *) "Y(mm)", large_font, NULL);
  snprintf(Y_value, 128, "00.000");
  di = attach_new_item_to_dialog(d_my_edit_proc, 0, 0, 127, 0);
  if (di == NULL)
  {
      allegro_message("dialog pb");
      return 1;
  }
  xpos += cur_text_len;
  cur_text_len = text_length(large_font, Y_value);
  set_dialog_size_and_color(di, xpos, ypos, cur_text_len, large_font_height,  makecol32(225, 40, 0), makecol32(180, 180, 180));
  set_dialog_properties(di, Y_value,  large_font, NULL);
  Y_dialog = di;
  di = attach_new_item_to_dialog(d_text_proc, 0, 0, 127, 0);
  if (di == NULL)
  {
    allegro_message("dialog pb");
    return 1;
  }
#endif

    //xpos =  w - 120 - 130;
    //FIXME strange positioning, not modified for variable size font
    xpos += cur_text_len;
    set_dialog_size_and_color(di, xpos, ypos, w - xpos - 80, large_font_height,  makecol32(255, 40, 0), makecol32(180, 180,
                              180));
    snprintf(param_label, 128, "                       ");
    set_dialog_properties(di, param_label, large_font, NULL);
    param_label_di = di;
    /*
    di = attach_new_item_to_dialog(d_button_proc, 0, 0, ((256 << 16) | 1024), 512);

    if (di == NULL)
    {
        allegro_message("dialog pb");
        return 1;
    }
    xpos += 90;
    set_dialog_size_and_color(di, xpos, ypos, w - xpos - 10, large_font_height,  makecol32(255, 40, 0), makecol32(180, 180, 180));
    snprintf(param_label, 128, " ");
    set_dialog_properties(di, (char *)"test", NULL, NULL);
    slider_di = di;
    */
# endif
    //# endif
    //}
    di = attach_new_item_to_dialog(d_yield_proc, 0, 0, 0, 0);

    if (di == NULL)
    {
        allegro_message("dialog pb");
        return 1;
    }

    /*    i = retrieve_item_from_dialog(d_draw_Op_proc, NULL);
        allegro_message("plot in %d",i); */
    di = attach_new_item_to_dialog(d_keyboard_proc, 12, 0, 0, 0);

    if (di == NULL)
    {
        allegro_message("dialog pb");
        return 1;
    }

    set_dialog_properties(di, (void *)do_load, NULL, NULL);
    di = attach_new_item_to_dialog(general_idle, 0, 0, 0, 0);

    if (di == NULL)
    {
        allegro_message("dialog pb");
        return 1;
    }

    di = attach_new_item_to_dialog(d_yield_proc, 0, 0, 0, 0);

    if (di == NULL)
    {
        allegro_message("dialog pb");
        return 1;
    }

    set_dialog_size_and_color(di, 0, 0, 0, 0, 0, 0);
    return 0;
}

int change_rotation_string(float rot)
{
    snprintf(rotation_value, 128, "%6.3f", rot);

    if (rotation_dialog)
    {
        rotation_dialog->proc(MSG_DRAW, rotation_dialog, 0);
    }

    return 0;
}


int change_zmag_string(float rot)
{
    snprintf(zmag_value, 128, "%6.3f", rot);

    if (zmag_dialog)
    {
        zmag_dialog->proc(MSG_DRAW, zmag_dialog, 0);
    }

    return 0;
}


int change_zmag_label_string(char *label, int fg, int bg, int mode)
{
    static int nfg = 0, nbg = 0, uptodate = 0;
    static char ntext[128] = {0};

    if (zmag_label_di == NULL)
    {
        return 1;
    }

    if (mode == 0)
    {
        uptodate = 0;
        nfg = fg;
        nbg = bg;

        if (label == NULL)
        {
            ntext[0] = 0;
        }
        else
        {
            snprintf(ntext, 128, "%s", label);
        }

        return 0;
    }
    else if (uptodate == 0)  // we really display stuff
    {
        if (ntext[0] != 0)
        {
            zmag_label_di->fg = zmag_label_di->bg; // we erase text

            if (zmag_label_di)
            {
                zmag_label_di->proc(MSG_DRAW, zmag_label_di, 0);
            }

            snprintf(zmag_label, 128, "%s", ntext);
            zmag_label_di->fg = nfg;
            zmag_label_di->bg = nbg;
        }
        else
        {
            zmag_label_di->fg = zmag_label_di->bg; // we erase text

            if (zmag_label_di)
            {
                zmag_label_di->proc(MSG_DRAW, zmag_label_di, 0);
            }

            snprintf(zmag_label, 128, "Magnet Z (mm) =");
            zmag_label_di->fg = makecol32(0, 0, 0);
            zmag_label_di->bg = makecol32(240, 240, 240);
        }

        if (zmag_label_di)
        {
            zmag_label_di->proc(MSG_DRAW, zmag_label_di, 0);
        }

        uptodate = 1;
    }

    return 0;
}


int change_focus_label_string(char *label, int fg, int bg, int mode)
{
    static int nfg = 0, nbg = 0, uptodate = 0;
    static char ntext[128] = {0};

    if (focus_label_di == NULL)
    {
        return 1;
    }
    // new 2016-04-14
    if (focus_label_di->flags & D_GOTFOCUS) return 0; // we do not modify label during input
    if (mode == 0)
    {
        uptodate = 0;
        nfg = fg;
        nbg = bg;

        if (label == NULL)
        {
            ntext[0] = 0;
        }
        else
        {
            snprintf(ntext, 128, "%s", label);
        }

        return 0;
    }
    else if (uptodate == 0)  // we really display stuff
    {
        if (ntext[0] != 0)
        {
            focus_label_di->fg = focus_label_di->bg; // we erase text

            if (focus_label_di)
            {
                focus_label_di->proc(MSG_DRAW, focus_label_di, 0);
            }

            snprintf(focus_label, 128, "%s", ntext);
            focus_label_di->fg = nfg;
            focus_label_di->bg = nbg;
        }
        else
        {
            focus_label_di->fg = focus_label_di->bg = makecol32(240, 240, 240); // we erase text

            if (focus_label_di)
            {
                focus_label_di->proc(MSG_DRAW, focus_label_di, 0);
            }

            snprintf(focus_label, 128, "Focus =");
            focus_label_di->fg = makecol32(0, 0, 0);
            focus_label_di->bg = makecol32(240, 240, 240);
        }

        if (focus_label_di)
        {
            focus_label_di->proc(MSG_DRAW, focus_label_di, 0);
        }

        uptodate = 1;
    }

    return 0;
}


int change_param_label_string(char *label, int fg, int bg, int mode)
{
    static int nfg = 0, nbg = 0, uptodate = 0;
    static char ntext[128] = {0};

    if (param_label_di == NULL)
    {
        return 1;
    }

    if (mode == 0)
    {
        uptodate = 0;
        nfg = fg;
        nbg = bg;

        if (label == NULL)
        {
            ntext[0] = 0;
        }
        else
        {
            snprintf(ntext, 128, "%s", label);
        }

        return 0;
    }
    else if (uptodate == 0)  // we really display stuff
    {
        if (ntext[0] != 0)
        {
            param_label_di->fg = param_label_di->bg; // we erase text

            if (param_label_di)
            {
                param_label_di->proc(MSG_DRAW, param_label_di, 0);
            }

            snprintf(param_label, 128, "%s", ntext);
            param_label_di->fg = nfg;
            param_label_di->bg = nbg;
        }
        else
        {
            param_label_di->fg = param_label_di->bg = makecol32(240, 240, 240);

            if (param_label_di)
            {
                param_label_di->proc(MSG_DRAW, param_label_di, 0);
            }

            snprintf(param_label, 128, " ");
            param_label_di->fg = makecol32(0, 0, 0);
            param_label_di->bg = makecol32(240, 240, 240);
        }

        if (param_label_di->proc)
        {
            param_label_di->proc(MSG_DRAW, param_label_di, 0);
        }

        uptodate = 1;
    }

    return 0;
}



int change_force_string(float force)
{
    snprintf(force_value, 128, "%6.3f", force);

    if (force_dialog)
    {
        force_dialog->proc(MSG_DRAW, force_dialog, 0);
    }

    return 0;
}

int change_focus_string(float focus)
{
    snprintf(focus_value, 128, "%6.3f", focus);

    if (focus_dialog)
    {
        focus_dialog->proc(MSG_DRAW, focus_dialog, 0);
    }

    //else win_printf("No focus dialog %g",focus);
    return 0;
}

int change_X_string(float X_pos)
{
    snprintf(X_value, 128, "%6.3f", X_pos);
    if (X_dialog) X_dialog->proc(MSG_DRAW, X_dialog, 0);
    return 0;
}
int change_Y_string(float Y_pos)
{
    snprintf(Y_value, 128, "%6.3f", Y_pos);
    if (Y_dialog) Y_dialog->proc(MSG_DRAW, Y_dialog, 0);
    return 0;
}

int change_T0_string(float T0)
{
    snprintf(T0_value, 128, "%6.3f", T0);

    if (T0_dialog)
    {
        T0_dialog->proc(MSG_DRAW, T0_dialog, 0);
    }

    return 0;
}

int change_Freq_string(float Freq)
{
    snprintf(Freq_value, 128, "%5.2f", Freq);

    if (Freq_dialog)
    {
        Freq_dialog->proc(MSG_DRAW, Freq_dialog, 0);
    }

    return 0;
}


int change_Load_string(float Load)
{
    snprintf(Load_value, 128, "%4.1f", Load);

    if (Load_dialog)
    {
        Load_dialog->proc(MSG_DRAW, Load_dialog, 0);
    }

    return 0;
}
