#include "sdi_v2_parameter.h"
extern int *sdi_colormap=NULL;

//allocated outside
int set_sdi_v2_diagnose_gui(sdi_v2_diagnose *diagnose, bool from_cfg)
{
  static float I_min_limit=0, I_max_limit=255;
  static float z_min_limit=-5, z_max_limit=5, y_min_limit=-5, y_max_limit=5, x_min_limit=-5, x_max_limit=5;
  static float contrast_min_limit = 0.01;
  static int weird_count_limit = 10;
  static float std_threshold_x=0.01, std_threshold_y=0.01, std_threshold_z=0.01;
  static float z_expected_range=0.67;//1/1.5
  static float z_range_tol=0.2;
  if (from_cfg==true)
  {
    //parameters for quality of beads
    weird_count_limit = get_config_int("SDI", "weird_count_limit", 1);
    contrast_min_limit = get_config_float("SDI", "contrast_min_limit", 1.);
    I_min_limit = get_config_float("SDI", "I_min_limit", 1.);
    I_max_limit = get_config_float("SDI", "I_max_limit", 1.);
    z_min_limit = get_config_float("SDI", "z_min_limit", 1.);
    z_max_limit = get_config_float("SDI", "z_max_limit", 1.);
    y_min_limit = get_config_float("SDI", "y_min_limit", 1.);
    y_max_limit = get_config_float("SDI", "y_max_limit", 1.);
    x_max_limit = get_config_float("SDI", "x_max_limit", 1.);
    x_min_limit = get_config_float("SDI", "x_min_limit", 1.);
    std_threshold_z = get_config_float("SDI", "std_threshold_z", 1.);
    std_threshold_x = get_config_float("SDI", "std_threshold_x", 1.);
    std_threshold_y = get_config_float("SDI", "std_threshold_y", 1.);
    z_expected_range = get_config_float("SDI", "z_expected_range", 1.);
    z_range_tol = get_config_float("SDI", "z_range_tol", 1.);
  }
  win_scanf("This gui allows to parametrize the sdi_v2 diagnostic parameters.\n"
             "limits zmin=%2f zmax=%2f, ymin=%2f ymax=%2f xmin=%2f xmax=%2f\n"
             "in intensity Imin=%2f Imax=%2f, max weird count %2d, min accepted contrast %3f\n"
             "immobile beads : stdx=%3f, stdy=%3f, stdz=%3f\n"
             "opening hairpin : dz=%3f with tol%3f"
             ,&z_min_limit, &z_max_limit, &y_min_limit, &y_max_limit, &x_min_limit, &x_max_limit,
              &I_min_limit, &I_max_limit, &weird_count_limit, &contrast_min_limit,
              &std_threshold_x, &std_threshold_y, &std_threshold_z, &z_expected_range,&z_range_tol);
  sdi_v2_set_diagnose(diagnose, z_min_limit, z_max_limit, y_min_limit, y_max_limit, x_min_limit, x_max_limit, I_min_limit, I_max_limit, contrast_min_limit, weird_count_limit, std_threshold_x, std_threshold_y, std_threshold_z,  z_expected_range,  z_range_tol);
  if (from_cfg==true)
  {
    set_config_int("SDI", "weird_count_limit",  weird_count_limit);
    set_config_float("SDI", "contrast_min_limit",  contrast_min_limit);
    set_config_float("SDI", "I_min_limit",  I_min_limit);
    set_config_float("SDI", "I_max_limit",  I_max_limit);
    set_config_float("SDI", "z_min_limit",  z_min_limit);
    set_config_float("SDI", "z_max_limit",  z_max_limit);
    set_config_float("SDI", "y_min_limit",  y_min_limit);
    set_config_float("SDI", "y_max_limit",  y_max_limit);
    set_config_float("SDI", "x_min_limit",  x_min_limit);
    set_config_float("SDI", "x_max_limit",  x_max_limit);
    set_config_float("SDI", "std_threshold_z",  std_threshold_z);
    set_config_float("SDI", "std_threshold_x",  std_threshold_x);
    set_config_float("SDI", "std_threshold_y",  std_threshold_y);
    set_config_float("SDI", "z_expected_range",  z_expected_range);
    set_config_float("SDI", "z_range_tol",  z_range_tol);
  }
  return 0;
}

int set_sdi_v2_parameters_gui(sdi_v2_parameter *param, bool from_cfg)
{
  static int x_roi_size=128, y_roi_size=10, x_window_order=4, x_window_fwhm=15, dc_width=8;
  static int y_roi_size_Y=32, x_roi_size_Y=60, y_window_order=5, y_window_fwhm=5;
  static int dist_half_beads=22;
  static float nu_threshold=0.5, gy=1,pixel_size = 1;
  float index_factor=1;
  if (from_cfg==true)
  {
    x_roi_size= get_config_int("SDI", "x_roi_size_zx", 1);
    y_roi_size= get_config_int("SDI", "y_roi_size_zx", 1);
    x_window_order = get_config_int("SDI", "x_window_order", 1);
    x_window_fwhm = get_config_int("SDI", "x_window_fwhm", 1);
    dc_width = get_config_int("SDI", "dc_width", 1);
    nu_threshold = get_config_float("SDI", "nu_threshold", 1.);
    dist_half_beads = get_config_int("SDI", "dist_half_beads", 1.);
    x_roi_size_Y = get_config_int("SDI", "x_roi_size_y", 1);
    y_roi_size_Y = get_config_int("SDI", "y_roi_size_y", 1);
    y_window_order = get_config_int("SDI", "y_window_fwhm", 1);
    y_window_fwhm = get_config_int("SDI", "y_window_order", 1);
    index_factor = get_config_float("SDI", "refraction_index", 1.);
  }
  win_scanf("Setup of the sdi_v2 parameters (gy=pixel_size=indexfac=1 if you save trk files).\n"
             "Enter the dimensions of a pattern of fringes along x (must be a power of two) : %3d and along y : %3d\n"
             "Enter the half-width of the hypergaussian window for x,z tracking : %3d and its order %2d, \n"
             "Enter the (normalized) threshold for fit : %3f and the dc component width (in pix) : %3d\n"
             "Enter the size of a y profile along y : %4d and along x : %3d\n"
             "Enter the half-width of the hypergaussian window for y : %3d and its order %2d\n"
             "Enter the magnification of the setup : %6f, the distance between two patterns of fringes of one bead : %3d\n"
             "and the index factor %6f and pixels size %3f (micr)\n"
             "note : pixel size = gy = index factor = 1 : in pixel unit\n"
             ,&x_roi_size,&y_roi_size,&x_window_fwhm,&x_window_order,&nu_threshold,&dc_width,&y_roi_size_Y,&x_roi_size_Y,&y_window_fwhm,&y_window_order
             ,&gy,&dist_half_beads,&index_factor, &pixel_size);
  set_sdi_v2_parameters(param, x_roi_size, y_roi_size, dist_half_beads, x_window_fwhm,
                    nu_threshold, dc_width, x_window_order, x_roi_size_Y, y_roi_size_Y, y_window_fwhm, y_window_order, gy, index_factor, pixel_size);


  if (from_cfg==true)
  {
    set_config_int("SDI", "dist_half_beads", dist_half_beads);
    set_config_int("SDI", "x_roi_size_zx", x_roi_size);
    set_config_int("SDI", "y_roi_size_zx", y_roi_size);
    set_config_int("SDI", "x_window_order", x_window_order);
    set_config_int("SDI", "x_window_fwhm", x_window_fwhm);
    set_config_float("SDI", "nu_threshold", nu_threshold);
    set_config_int("SDI", "dc_width", dc_width);
    set_config_int("SDI", "x_roi_size_y", x_roi_size_Y);
    set_config_int("SDI", "y_roi_size_y", y_roi_size_Y);
    set_config_int("SDI", "y_window_fwhm", y_window_fwhm );
    set_config_int("SDI", "y_window_order", y_window_order);
    set_config_float("SDI", "index_factor", index_factor);
  }
  return 0;
}

int sdi_v2_set_diagnose(sdi_v2_diagnose *diagnose, float z_min_limit, float z_max_limit, float y_min_limit, float y_max_limit,
                        float x_min_limit, float x_max_limit, float I_min_limit, float I_max_limit, float contrast_min_limit, int weird_count_limit,
                        float std_threshold_x, float std_threshold_y, float std_threshold_z, float z_expected_range, float z_range_tol)
{
  diagnose->z_min_limit = z_min_limit;
  diagnose->z_max_limit = z_max_limit;
  diagnose->y_min_limit = y_min_limit;
  diagnose->y_max_limit = y_max_limit;
  diagnose->x_min_limit = x_min_limit;
  diagnose->x_max_limit = x_max_limit;
  diagnose->I_min_limit = I_min_limit;
  diagnose->I_max_limit = I_max_limit;
  diagnose->contrast_min_limit = contrast_min_limit;
  diagnose->weird_count_limit = weird_count_limit;
  diagnose->std_threshold_x = std_threshold_x;
  diagnose->std_threshold_y = std_threshold_y;
  diagnose->std_threshold_z = std_threshold_z;
  diagnose->z_expected_range = z_expected_range;
  diagnose->z_range_tol = z_range_tol;
  return 0;
}

//allocated previously
int set_sdi_v2_parameters( sdi_v2_parameter *param, int x_roi_size, int y_roi_size,int dist_half_beads,int x_window_fwhm,float nu_threshold,int dc_width,
          int x_window_order, int x_roi_size_Y, int y_roi_size_Y, int y_window_fwhm, int y_window_order, float gy, float index_factor, float pixel_size)
{
  int i;
  if (param->fp_x == NULL )  param->fp_x=(fft_plan *)calloc(1,sizeof(fft_plan));
  else  free_fft_plan_bt(param->fp_x);

  if (param->fp_y == NULL)   param->fp_y=(fft_plan *)calloc(1,sizeof(fft_plan));
  else  free_fft_plan_bt(param->fp_y);

  //parameters of the optical setup
  param->dist_half_beads=dist_half_beads;
  param->gy=gy;
  param->index_factor = index_factor;
  param->pixel_size = pixel_size;

  //parameters for computation of x and z
  param->fp_x=NULL;
  param->fp_x=(fft_plan *)calloc(1,sizeof(fft_plan));
  param->x_roi_size=x_roi_size;
  param->y_roi_size=y_roi_size;
  param->x_roi_size_2=(int)(x_roi_size/2);
  param->y_roi_size_2=(int)(y_roi_size/2);
  param->dc_width=dc_width;
  param->x_window_order=x_window_order;
  param->x_window_fwhm=x_window_fwhm;
  param->nu_threshold=nu_threshold;
  param->fp_x=fftbt_init(param->fp_x,x_roi_size);
  param->nu_x= (float *) calloc ( param->fp_x->n_2, sizeof(float));
  for (i=0 ; i<param->fp_x->n_2 ; i++) param->nu_x[i]=(float) ((double) i / (double) param->fp_x->nx); //half frequency axis (pix**-1)

  //parameters for computation of y
  param->fp_y=NULL;
  param->fp_y=(fft_plan *)calloc(1,sizeof(fft_plan));
  param->x_roi_size_Y=x_roi_size_Y;
  param->y_roi_size_Y=y_roi_size_Y;
  param->x_roi_size_2_Y=(int)(x_roi_size_Y/2);
  param->y_roi_size_2_Y=(int)(y_roi_size_Y/2);
  param->y_window_order=y_window_order;
  param->y_window_fwhm=y_window_fwhm;
  param->fp_y=fftbt_init(param->fp_y,y_roi_size_Y);
  param->nu_y= (float *) calloc ( param->fp_y->n_2, sizeof(float));
  for (i=0 ; i<param->fp_y->n_2 ; i++) param->nu_y[i]=(float) ((double) i / (double) param->fp_y->nx); //half frequency axis (pix**-1)


  return 0;
}

int sdi_v2_define_sdi_colormap(void)
{
  if (sdi_colormap == NULL)
  {
    sdi_colormap = (int *)calloc(32, sizeof(int));
    sdi_colormap[0] = makecol(99, 184, 255);//turquoise
    sdi_colormap[1] = makecol(160, 32, 240);//violet
    sdi_colormap[2] = makecol(255,255,0);//jaune
    sdi_colormap[3] = makecol(255,122,122);//rouge - orange
    sdi_colormap[4] = makecol(255, 127, 80);//??
    sdi_colormap[5] = makecol(255, 0, 0);//rouge rouge vc
    sdi_colormap[6] = makecol(0, 255, 0);//vert vc
    sdi_colormap[7] = makecol(0, 0, 255);//bleu vc
    sdi_colormap[8] = makecol(229, 204, 205);//un rose pale pour le fond des panels
    sdi_colormap[9] = makecol(153, 0, 0);//un rouge sombre pour le texte
  }
  return 0;
}
