#pragma once

#include "sdi_v2_fringes.h"
#include "sdi_v2_y_profile.h"
#include "sdi_v2_bead.h"
#include "xvin.h"

#define SDI_MAX_NUMBER_OF_PATTERN 10000

//handling all the display
typedef struct sdi_v2_display
{
  d_s *data;    //the ds plotted in op
  float **y;    //source of yd for ds
  int n_y, c_y; //to update the source yd from a 2d float (rolling) buffer
  float **x;     //the same for x
  int n_x, c_x;
  bool constant_x; // if false then the value of x is updated during time (rolling)
  float *l_axis;  //to update the length of the axis (may vary for)
  int n_axis, c_axis;
  pltreg *plr;
  O_p *op;
  char x_title[32];
  char y_title[32];
  char ds_source[32];
  //eventuellement remettre un petit bitmap ac des trucs
} sdi_v2_disp;

typedef struct panel_info
{
  BITMAP *bmp;
  int color_bgd;
  int color_txt;
  int w, h, x, y;//in screen
  char message[2048];
} sdi_v2_p_info;

//when removing display object one can end up with empty pltreg (cannot remove plotreg) so i keep the list of a available empyt pltreg
//to be re-used when needed, this is absolutely horrible
extern pltreg **available_pltreg;
extern int n_available_pltreg;

PXV_FUNC(int, sdi_v2_update_y_display, (sdi_v2_disp *disp, int cur));
PXV_FUNC(int, sdi_v2_update_x_display, (sdi_v2_disp *disp, int cur, float val));

PXV_FUNC(int, sdi_v2_idle_op_refresh, (O_p *op, DIALOG *d));
PXV_FUNC(int, sdi_v2_launch_display, (d_s *ds, pltreg **plr, O_p **op, char *name));
PXV_FUNC(int, sdi_v2_config_bead_1d_display, (sdi_v2_bead *bead, int which_data, sdi_v2_disp *display, int size, float x0));
PXV_FUNC(int, sdi_v2_config_fringes_1d_display, (sdi_v2_fringes *fringes, int which_data, sdi_v2_disp *display, int size));
PXV_FUNC(int, sdi_v2_config_y_profile_1d_display, (sdi_v2_y_profile *profile, int which_data, sdi_v2_disp *display, int size));
PXV_FUNC(int, sdi_v2_config_fringes_2d_display, (sdi_v2_fringes *fringes, int which_data, sdi_v2_disp *display, sdi_v2_parameter *param));
PXV_FUNC(int, sdi_v2_config_y_profile_2d_display, (sdi_v2_y_profile *y_profile, int which_data, sdi_v2_disp *display, sdi_v2_parameter *param));

PXV_FUNC(int, sdi_v2_draw_one_fast_rect_with_user_pix, (int xc,int yc,int half_length, int half_width,int color, O_i *oi, BITMAP *imb));
PXV_FUNC(int, sdi_v2_draw_one_fast_rect_vga_screen_unit, (int xc, int yc, int half_length, int half_width, int color, BITMAP* bm));
PXV_FUNC(int, sdi_v2_draw_one_circle_with_user_pix, (int xc, int yc, int radius, int color, O_i *oi, BITMAP *imb));
PXV_FUNC(int, sdi_v2_draw_one_circle_vga_screen_unit, (int xc, int yc, int radius, int color, BITMAP *bmp));
PXV_FUNC(int, sdi_v2_add_text_vga_screen_unit_in_oi, (O_i *oi, int xc, int yc, int color, const char *format, ...));
PXV_FUNC(int, sdi_v2_x_user_pix_2_vga_screen_unit, (BITMAP *bmp, O_i *oi, int x));
PXV_FUNC(int, sdi_v2_dx_user_pix_2_vga_screen_unit, (BITMAP *bmp, O_i *oi, int dx));
PXV_FUNC(int, sdi_v2_y_user_pix_2_vga_screen_unit, (BITMAP *bmp, O_i *oi, int y));
PXV_FUNC(int, sdi_v2_dy_user_pix_2_vga_screen_unit, (BITMAP *bmp, O_i *oi, int dy));
PXV_FUNC(int, sdi_v2_place_multiple_double_fast_rect_with_mouse, (int *xc, int *yc,int half_length,int half_width,int x_dist,int y_dist, int color, imreg *imr, O_i *oi, BITMAP *imb));


//remove
PXV_FUNC(int, sdi_v2_remove_one_sdi_display, (sdi_v2_disp *display));
PXV_FUNC(int,	sdi_v2_remove_ds_from_op_without_free_ds, (O_p *op, d_s *ds));
PXV_FUNC(int, sdi_v2_remove_plot_from_plr_without_free_ds, (pltreg *pr, O_p *op));
PXV_FUNC(int, sdi_v2_free_one_plot_without_free_ds, (O_p *op));

PXV_FUNC(int, sdi_v2_init_one_panel, (sdi_v2_p_info *panel, int width, int height, int x, int y));
PXV_FUNC(int, sdi_v2_draw_one_filled_rect_vga_screen_unit, (int xc, int yc, int half_length, int half_width, int color, BITMAP* bm));
PXV_FUNC(int, sdi_v2_add_text_vga_screen_unit, (BITMAP *bmp, int xc, int yc, int color, const char *format, ...));
