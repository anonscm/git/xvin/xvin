
#pragma once

#include "sdi_v2_basic.h"
#include "sdi_v2_parameter.h"
#include "float.h"
# include "xvin.h"

#define PTR_Y_PROFILE_Y_T 0

#define PTR_Y_PROFILE_Y_PROFILE 10
#define PTR_Y_PROFILE_RAW_Y_PROFILE 11
#define PTR_Y_PROFILE_Y_WINDOW 12
#define PTR_Y_PROFILE_Y_AUTOCONV 13
#define PTR_Y_PROFILE_Y_AUTOCONV_ROI 14

#define Y_PROFILE_AUTOCONV_ROI_SIZE 5

//functions whose input is an sdi_fringes structure : one level below than SDITracking

typedef struct sdi_version2_y_profile
{

  int xc, yc; //position of the center of the roi
  int x_roi_min,x_roi_max,y_roi_min,y_roi_max;//defines the roi in the image
  float background;
  float gy;//in case of distorsion when working outside the objective fov
  bool need_background, recenter_along_y, recenter_along_x;
  float y_offset_pixel;

  int c_output_t, n_output_t;
  float *y_t;

  float **raw_y_profile, **y_window, **windowed_y_profile;
  int n_raw_y_profile_buffer, c_raw_y_profile_buffer;
  int n_y_window_buffer, c_y_window_buffer;
  int n_windowed_y_profile_buffer, c_windowed_y_profile_buffer;

  float **y_profile_autoconv, **y_profile_autoconv_roi;
  int n_y_profile_autoconv_buffer, c_y_profile_autoconv_buffer;
  int n_y_profile_autoconv_roi_buffer, c_y_profile_autoconv_roi_buffer;

  d_s *ds_out1;

} sdi_v2_y_profile;

PXV_FUNC(int, sdi_v2_reset_all_intermediate_data_of_y_profile, (sdi_v2_y_profile *profile));
PXV_FUNC(int, sdi_v2_reset_data_of_y_profile, (sdi_v2_y_profile *profile, int which_data));

PXV_FUNC(int, sdi_v2_update_c_profile_y_profile_points, (sdi_v2_y_profile *y_profile, int ci));
PXV_FUNC(int, sdi_v2_compute_center_by_autoconv_of_sdi_v2_y_profile, (sdi_v2_y_profile *y_profile, sdi_v2_parameter *param));
PXV_FUNC(int, sdi_v2_process_y_profile_y_profile, ( sdi_v2_y_profile *y_profile, sdi_v2_parameter *param));
PXV_FUNC(int, sdi_v2_extract_sdi_y_profile_raw_y_profile_from_image, (O_i *oi, sdi_v2_y_profile *y_profile, sdi_v2_parameter *param));
PXV_FUNC(int, sdi_v2_compute_center_of_autoconv_of_y_profile, (sdi_v2_y_profile *y_profile, sdi_v2_parameter *param, float *center));
PXV_FUNC(int, sdi_v2_find_center_by_polynomial_3, (sdi_v2_y_profile *y_profile, sdi_v2_parameter *param, float *center, int x_i_min, int roi_interp_size));

PXV_FUNC(int, sdi_v2_switch_ptr_to_sdi_y_profile_1d_float_field, (sdi_v2_y_profile *y_profile, int which_data, float **field, float **ext_float));
PXV_FUNC(int, sdi_v2_switch_ptr_to_sdi_y_profile_2d_float_field, (sdi_v2_y_profile *y_profile, int which_data, float **field, int *n_field, int *c_field,
                                                                              float **ext_float, int n_ext_float));

PXV_FUNC(int, sdi_v2_y_profile_update_parameter, (sdi_v2_y_profile *y_profile, sdi_v2_parameter *param));
PXV_FUNC(int, sdi_v2_allocate_sdi_v2_y_profile_output_t_fields, (sdi_v2_y_profile *y_profile, int size));
PXV_FUNC(int, sdi_v2_allocate_sdi_v2_y_profile_1d_float_fields, (sdi_v2_y_profile *y_profile, int size, int which_data));
PXV_FUNC(int, sdi_v2_allocate_sdi_v2_y_profile_2d_float_fields, (sdi_v2_y_profile *y_profile, sdi_v2_parameter *param, int buffer_size, int which_data));
PXV_FUNC(int, sdi_v2_init_one_sdi_y_profile_by_position, (sdi_v2_y_profile *y_profile, int xc, int yc));
PXV_FUNC(int, sdi_v2_remove_one_sdi_y_profile, (sdi_v2_y_profile *y_profile));
