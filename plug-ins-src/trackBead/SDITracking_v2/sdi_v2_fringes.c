//this contain the functions wrapping the sdi operations on the sdi fringes structures
#include "sdi_v2_fringes.h"

# define  WAS_LIKE_THAT

//the z axis should be known
int sdi_v2_linear_calibration_of_sdi_fringes(sdi_v2_fringes *fringes, float *z, float *order0_z, float *order1_z, int n_points)
{
  d_s *tmp = NULL;
  int i;
  double *coeff = NULL;
  //coeff=(double **)calloc(2,sizeof(double *));

  if(fringes == NULL || z == NULL || order0_z == NULL || order1_z == NULL) return 1;
  if (tmp == NULL || n_points > tmp->nx) tmp = build_adjust_data_set(tmp, n_points, n_points);
  for(i=0 ; i<n_points ; i++)
  {
    tmp->xd[i] = z[i];
    tmp->yd[i] = order0_z[i];
  }
  fit_ds_to_xn_polynome(tmp, 2, -FLT_MAX, FLT_MAX, -FLT_MAX, FLT_MAX, &coeff);
  fringes->v_0=(float)(coeff[1]);
  for(i=0 ; i<n_points ; i++) tmp->yd[i] = order1_z[i];
  fit_ds_to_xn_polynome(tmp, 2, -FLT_MAX, FLT_MAX, -FLT_MAX, FLT_MAX, &coeff);
  fringes->v_1=(float)(coeff[1]);
  fringes->which_half=(fringes->v_0 < 0) ? 0 : 1;
  free(coeff);
  coeff=NULL;
  free_data_set(tmp);
  tmp = NULL;
  return 0;
}
//this one computes the frequency from the experiment
//it expects that nu0_t has been recorded ie that the data are taken in debug mode with n_phi_t!=0
int sdi_v2_frequency_calibration_of_fringes(sdi_v2_fringes *fringes)
{
  float nu0_avg = 0;
  if (fringes == NULL || fringes->n_phi_t == 0) return D_O_K;
  sdi_v2_estimate_mean(fringes->nu0_t, fringes->n_phi_t, &nu0_avg);
  fringes->nu0 = nu0_avg;
  return 0;
}
//computes the velocity only with mask & optical desing parameters
int sdi_v2_compute_v_x_of_fringes(sdi_v2_fringes *fringes, sdi_v2_parameter *param)
{
  if (fringes == NULL || param == NULL) return 1;
  fringes->v_x = 2 * M_PI * fringes->nu0 / param->pixel_size * param->gy;//during experiment, pix size & gy are=1 because the factor is in config file
  return 0;
}
//computes the phase velocity of the fringes with slits parameters
//x1/x2 is the interior/exterior
//be aware that this is valid only if correctly aligned!
int sdi_v2_compute_v_0_of_fringes(sdi_v2_fringes *fringes, int sign, float x_1, float x_2, float lambda, float gy, float ft, float n_o)
{
  if (fringes == NULL) return 1;
  float tmp0 = sqrt( 1 - gy * gy / n_o / n_o * x_2 * x_2 / ft /ft);
  float tmp1 = sqrt( 1 - gy * gy / n_o / n_o * x_1 * x_1 / ft /ft);
  fringes->v_0 = sign * 2 * M_PI * n_o / lambda * fabs(tmp0 - tmp1);//Fourier optics formula
  return 0;
}
//back to physical units
int sdi_v2_compute_x_z_coupled(sdi_v2_fringes *fringes)
{
  float tmp0, tmp1;
  if (fringes == NULL) return 1;
  tmp0 = fringes->raw_phi_0_t[fringes->c_phi_t];
  tmp1 = fringes->phase_jump_t[fringes->c_phi_t];
  //examines whether the fringes has ref points to do correct correction of 2pi jumps, or whether the current point mus t be taken as one ref pt
  if (fringes->n_ref_points != 0 && fringes->v_0 != 999 && fringes->v_1 != 999 && fringes->need_reference_point == false)
  {
    sdi_v2_find_correct_2_pi_multiple( &tmp0, fringes->raw_phi_1_t[fringes->c_phi_t], fringes->phi_0_ref_points,
                                                fringes->phi_1_ref_points, fringes->k_pi_ref_points, fringes->n_ref_points, fringes->v_0, fringes->v_1, &tmp1);
    fringes->corrected_phi_0_t[fringes->c_phi_t] = tmp0;
    fringes->phase_jump_t[fringes->c_phi_t] = tmp1;
  }
  if (fringes->need_reference_point == true)
  {
    //to update the ancient ref points
    if (fringes->n_ref_points != 0 && fringes->v_0 != 999 && fringes->v_1 != 999)
    {
      sdi_v2_find_correct_2_pi_multiple( &tmp0, fringes->raw_phi_1_t[fringes->c_phi_t], fringes->phi_0_ref_points,
                                                  fringes->phi_1_ref_points, fringes->k_pi_ref_points, fringes->n_ref_points, fringes->v_0, fringes->v_1, &tmp1);
      fringes->corrected_phi_0_t[fringes->c_phi_t] = tmp0;
      fringes->phase_jump_t[fringes->c_phi_t] = tmp1;
    }
    sdi_v2_add_current_point_as_ref_points(fringes);
  }

  if (fringes->n_output_t == 0)
  {
    if (fringes->z_coupled_t == NULL) fringes->z_coupled_t = (float *)calloc(1, sizeof(float));
    if (fringes->x_coupled_t == NULL) fringes->x_coupled_t = (float *)calloc(1, sizeof(float));
    if (fringes->contrast_t == NULL) fringes->contrast_t = (float *)calloc(1, sizeof(float));
    if (fringes->I_max_t == NULL) fringes->I_max_t = (float *)calloc(1, sizeof(float));
    fringes->c_output_t = 0;
  }
  if (fringes->v_0 == 999) fringes->z_coupled_t[fringes->c_output_t] = 999;
  else fringes->z_coupled_t[fringes->c_output_t] = tmp0  / fringes->v_0;
  if (fringes->v_x == 999) fringes->x_coupled_t[fringes->c_output_t] = 999;
  else fringes->x_coupled_t[fringes->c_output_t] = tmp0  / fringes->v_x;
/*
  if (fringes->n_phi_t == 0)
  {
    free(fringes->raw_phi_0_t);
    fringes->raw_phi_0_t = NULL;
    free(frainges->raw_phi_1_t);
    fringes->raw_phi_1_t = NULL;
    free(fringes->corrected_phi_0_t);
    fringes->corrected_phi_0_t = NULL;
    free(fringes->phase_jump_t);
    fringes->phase_jump_t = NULL;
  }*/
  return 0;
}

//contains all stuff in Fourier space & post-fourier
int sdi_v2_compute_phase_coeff_of_sdi_fringes(sdi_v2_fringes *fringes, sdi_v2_parameter *param)
{
  static double phase_coeff[2];
  float contrast, I_max;
  //computes the phase coeff
  //phase_coeff = (double *)calloc(2, sizeof(double));

  if (fringes == NULL || param == NULL) return 1;
  sdi_v2_compute_phase_coeff_of_x_profile(fringes, param, phase_coeff);

  if(fringes->n_phi_t == 0)
  {
    if (fringes->raw_phi_0_t==NULL) fringes->raw_phi_0_t = (float *)calloc(1, sizeof(float));
    if (fringes->raw_phi_1_t==NULL) fringes->raw_phi_1_t = (float *)calloc(1, sizeof(float));
    if (fringes->corrected_phi_0_t==NULL) fringes->corrected_phi_0_t = (float *)calloc(1, sizeof(float));
    if (fringes->phase_jump_t==NULL) fringes->phase_jump_t = (float *)calloc(1, sizeof(float));
    fringes->c_phi_t = 0;
  }
  fringes->raw_phi_0_t[fringes->c_phi_t] = (float)phase_coeff[0];
  fringes->raw_phi_1_t[fringes->c_phi_t] = (float)phase_coeff[1];
  fringes->corrected_phi_0_t[fringes->c_phi_t] = (float)phase_coeff[0]; //before 2pi jumps correction
  fringes->phase_jump_t[fringes->c_phi_t] = 0;      //before 2pi jumps correction

  //computes the contrast with a minimum number of operations
  int dx_i;
  if (fringes->nu0 == 999) dx_i = 8 ;
  else dx_i = (int) (2 / fringes->nu0) ; //look over around 2 fringes
  sdi_v2_quick_contrast( fringes->windowed_x_profile[fringes->c_windowed_x_profile_buffer], param->x_roi_size, dx_i, &contrast, &I_max);
  if (fringes->n_output_t == 0)
  {
    if (fringes->z_coupled_t == NULL) fringes->z_coupled_t = (float *)calloc(1, sizeof(float));
    if (fringes->x_coupled_t == NULL) fringes->x_coupled_t = (float *)calloc(1, sizeof(float));
    if (fringes->contrast_t == NULL) fringes->contrast_t = (float *)calloc(1, sizeof(float));
    if (fringes->I_max_t == NULL) fringes->I_max_t = (float *)calloc(1, sizeof(float));
    fringes->c_output_t = 0;
  }
  fringes->contrast_t[fringes->c_output_t]=contrast;
  fringes->I_max_t[fringes->c_output_t]=I_max;

  //to erase the undesired data in real space
  if (fringes->n_windowed_x_profile_buffer == 0) //en fait faut mettre une boucle sur n prev non nul...y a une fuite ici (mais pas continue donc ca va)
  {
    free(fringes->windowed_x_profile[0]);
    fringes->windowed_x_profile[0] = NULL;
    free(fringes->windowed_x_profile);
    fringes->windowed_x_profile = NULL;
  }
  //free(phase_coeff);
  //phase_coeff=NULL;
  return 0;
}

int sdi_v2_add_current_point_as_ref_points(sdi_v2_fringes *fringes)
{
  if (fringes == NULL) return D_O_K;
  if(fringes->raw_phi_0_t == NULL || fringes->raw_phi_1_t == NULL || fringes->phase_jump_t == NULL) return D_O_K;
  sdi_v2_add_one_ref_points(fringes, fringes->raw_phi_0_t[fringes->c_phi_t], fringes->raw_phi_1_t[fringes->c_phi_t], fringes->phase_jump_t[fringes->c_phi_t]);
  fringes->need_reference_point = false;
  return 0;
}

//1 suffit mais peut-être que des tests de fiabilité montreront que 3 seraient mieux
int sdi_v2_add_one_ref_points(sdi_v2_fringes *fringes, float phi0_r, float phi1_r, float k_pi_ref_point)
{
  if (fringes == NULL) return 1;
  if (fringes->n_ref_points == 0)
  {
    fringes->n_ref_points++;
    fringes->phi_0_ref_points = (float *)calloc(1, sizeof(float));
    fringes->phi_1_ref_points = (float *)calloc(1, sizeof(float));
    fringes->k_pi_ref_points = (float *)calloc(1, sizeof(float));
  }
  else
  {
    fringes->n_ref_points++;
    fringes->phi_0_ref_points = (float *)realloc(fringes->phi_0_ref_points, fringes->n_ref_points * sizeof(float));
    fringes->phi_1_ref_points = (float *)realloc(fringes->phi_1_ref_points, fringes->n_ref_points * sizeof(float));
    fringes->k_pi_ref_points = (float *)realloc(fringes->k_pi_ref_points, fringes->n_ref_points * sizeof(float));
  }
  fringes->phi_0_ref_points[fringes->n_ref_points - 1] = phi0_r;
  fringes->phi_1_ref_points[fringes->n_ref_points - 1] = phi1_r;
  fringes->k_pi_ref_points[fringes->n_ref_points - 1] = k_pi_ref_point;
  return 0;
}


//handles the sdi computation in Fourier space
int sdi_v2_compute_phase_coeff_of_x_profile(sdi_v2_fringes *fringes, sdi_v2_parameter *param, double *phase_coeff)
{
  float x_frac_min, x_frac_max, width, nu0_tmp;
  int i,x_i_frac_min,x_i_frac_max,x_i_frac_closest_min,x_i_frac_closest_max;
  float *x_profile_copy = NULL;
  static int n_x_profile_copy = 0;

  if (fringes == NULL || param == NULL) return 1;
  if (x_profile_copy == NULL)
  {
	   x_profile_copy = (float *)calloc(param->x_roi_size, sizeof(float));
	   if (x_profile_copy == NULL) return 2;
	   n_x_profile_copy = param->x_roi_size;
  }
  else if (n_x_profile_copy < param->x_roi_size)
  {
	   x_profile_copy = (float *)realloc(x_profile_copy, param->x_roi_size * sizeof(float));
	   if (x_profile_copy == NULL) return 2;
	   n_x_profile_copy = param->x_roi_size;
  }
  for (i=0 ; i<param->x_roi_size ; i++)  x_profile_copy[i] = fringes->windowed_x_profile[fringes->c_windowed_x_profile_buffer][i];
  if (fringes->n_amplitude_buffer == 0)
  {
	   fringes->c_amplitude_buffer = 0;
	   if (fringes->amplitude == NULL) fringes->amplitude = (float **)calloc(1, sizeof (float *));
	   fringes->amplitude[0] = (float *)realloc(fringes->amplitude[0],param->fp_x->n_2*sizeof(float));
  }

  if (fringes->n_phase_buffer == 0)
  {
	   fringes->c_phase_buffer = 0;
	   if (fringes->phase == NULL) fringes->phase = (float **)calloc(1, sizeof (float *));
	   fringes->phase[0] = (float *)realloc(fringes->phase[0],param->fp_x->n_2*sizeof(float));
  }
  if (fringes->n_spectrum_buffer == 0)
  {
	   fringes->c_spectrum_buffer = 0;
	   if (fringes->spectrum == NULL) fringes->spectrum = (float **)calloc(1, sizeof (float *));
	   fringes->spectrum[0] = (float *)realloc(fringes->spectrum[0], param->fp_x->n_2*sizeof(float));
  }

   sdi_v2_array_fftshift_then_fft( x_profile_copy, param->fp_x, fringes->amplitude[fringes->c_amplitude_buffer],
				    fringes->phase[fringes->c_phase_buffer], fringes->spectrum[fringes->c_spectrum_buffer]);
    //roi for fitting the spectral phase, dc_width is typically 5/7 pixels
    sdi_v2_peak_width( param->nu_x, fringes->amplitude[fringes->c_amplitude_buffer], param->dc_width, param->fp_x->n_2, param->nu_threshold,
		       &x_frac_min, &x_i_frac_min, &x_i_frac_closest_min, &x_frac_max, &x_i_frac_max, &x_i_frac_closest_max, &width);
    //the first time you get the frequency from one time measurement. For more precise stuff you can record it along time with n_phi_!=0
    if (fringes->nu0 == 999)
    {
      sdi_v2_measure_frequency(param->nu_x, fringes->spectrum[fringes->c_spectrum_buffer], param->fp_x->n_2, &nu0_tmp, x_i_frac_closest_min, x_i_frac_closest_max, 1);
      fringes->nu0 = nu0_tmp;
    }
    //as an option (ie n_phi_t !=0) one can record the mean frequency of the peak of the fringes
    //this is useful when hardware calibration, to record plots..this options also records raw phases, jumps etc
    if (fringes->n_phi_t != 0)
    {
      sdi_v2_measure_frequency(param->nu_x, fringes->spectrum[fringes->c_spectrum_buffer], param->fp_x->n_2, &nu0_tmp, x_i_frac_closest_min, x_i_frac_closest_max, 1);
      fringes->nu0_t[fringes->c_phi_t] = nu0_tmp;
    }
    //computes the linear fit of the phase over the roi defined by x_i_frac_min & x_i_frac_max
    sdi_v2_phase_fit_omega(fringes, param, phase_coeff, fringes->nu0, x_i_frac_min, x_i_frac_max);
    //to get more intermediate info about the Fourier step of the process : computes the log of spectrum if asked (mean size of buffer !=0)
    if (fringes->n_spectrum_log_buffer !=0)
    {
      if (fringes->n_spectrum_buffer != 0)
      {
        if (fringes->spectrum_log == NULL) fringes->spectrum_log = (float **)calloc(fringes->c_spectrum_log_buffer, sizeof( float *));
        fringes->spectrum_log[fringes->c_spectrum_log_buffer] = (float *)calloc( param->x_roi_size, sizeof(float));
        for (i=0 ; i<param->fp_x->n_2 ; i++) fringes->spectrum_log[fringes->c_spectrum_log_buffer][i] = fringes->spectrum[fringes->c_spectrum_buffer][i];
        sdi_v2_array_log(fringes->spectrum_log[fringes->c_spectrum_log_buffer], param->fp_x->n_2);
      }
    }

    //tuer profil reel window
    if (fringes->n_amplitude_buffer == 0)
    {
      free(fringes->amplitude[0]);
      fringes->amplitude[0] = NULL;
      free(fringes->amplitude);
      fringes->amplitude = NULL;
    }
    if (fringes->n_spectrum_buffer == 0)
    {
      free(fringes->spectrum[0]);
      fringes->spectrum[0] = NULL;
      free(fringes->spectrum);
      fringes->spectrum = NULL;
    }
    free(x_profile_copy);
    x_profile_copy = NULL;
    return 0;
}
/*
int compute_least_square_fit_on_ds(const d_s *src_ds, double *out_a, double *out_b)
{
    int nx;
    float tmp = 0;
    double sy2 = 0, sy = 0, sx = 0, sx2 = 0, sxy = 0;
    double a = 0, b = 0;

    for (int i = 0, nx = src_ds->nx; i < src_ds->nx ; i++)
      {
	sy += src_ds->yd[i];
	sy2 += src_ds->yd[i] * src_ds->yd[i];
	sx += src_ds->xd[i];
	sx2 += src_ds->xd[i] * src_ds->xd[i];
	sxy += src_ds->xd[i] * src_ds->yd[i];
      }
    b = (sx2 * nx) - sx * sx;
    if (b == 0)
      {
	//Delta  = 0 \n can't fit that!");
	return 1;
      }
    a = sxy * nx - sx * sy;
    a /= b;
    b = (sx2 * sy - sx * sxy) / b;
    if (out_a)
    {
        *out_a = a;
    }
    if (out_b)
    {
        *out_b = b;
    }
    return 0;
}

*/

//the core of the computation : linear fit of the spectral phase on a roi defined by x_i_frac_min/max
int sdi_v2_phase_fit_omega(sdi_v2_fringes *fringes, sdi_v2_parameter *param, double *phase_coeff, float nu0, int x_i_frac_min, int x_i_frac_max)
{
    float scale;
    float x_nu_min, x_nu_max;
    int i, roi_fit_size;
    double *a = NULL;
    d_s *tmp = NULL;

    if (fringes == NULL || param == NULL || phase_coeff == NULL) return 1;
    roi_fit_size = x_i_frac_max - x_i_frac_min + 1;
    //roi fit size change sometimes of 1 pixel (or if you change the nu threshold for fit obviously)
    //so recording it for diagnose purpose can be useful
    //and it can also be used as an easy way to check that the fitting part behaves as expected
    if (fringes->n_phi_t !=0 ) fringes->roi_fit_size_t[fringes->c_phi_t] = (float) roi_fit_size;

    //memory handling of unwrapped_phase_roi
    if (fringes->n_unwrapped_phase_roi_buffer == 0)
    {
	     fringes->c_unwrapped_phase_roi_buffer = 0;
	     if (fringes->unwrapped_phase_roi == NULL) fringes->unwrapped_phase_roi = (float **)calloc(1, sizeof (float *));
	     fringes->unwrapped_phase_roi[0] = (float *)realloc(fringes->unwrapped_phase_roi[0], roi_fit_size*sizeof(float));
    }
    for (i=0 ; i<roi_fit_size ; i++) fringes->unwrapped_phase_roi[fringes->c_unwrapped_phase_roi_buffer][i] = fringes->phase[fringes->c_phase_buffer][i + x_i_frac_min];
    if (fringes->n_raw_phase_roi_buffer != 0) //only to keep debug data when required
    {
	     for (i=0 ; i<roi_fit_size ; i++)  fringes->raw_phase_roi[fringes->c_raw_phase_roi_buffer][i] = fringes->phase[fringes->c_phase_buffer][i + x_i_frac_min];
    }

    //security to prevent fit from jumps : unwrapps the phase before linear fit (most of time this line does nothing)
    sdi_v2_unwrap( fringes->unwrapped_phase_roi[fringes->c_unwrapped_phase_roi_buffer], 0, roi_fit_size, 1);

    if (fringes->n_nu_roi_buffer == 0)
    {
      fringes->c_nu_roi_buffer = 0;
      if (fringes->nu_roi == NULL) fringes->nu_roi = (float **)calloc(1, sizeof(float *));
      fringes->nu_roi[0] = (float *)realloc(fringes->nu_roi[0],roi_fit_size*sizeof(float));
    }
    for (i=0 ; i<roi_fit_size ;i++) fringes->nu_roi[fringes->c_nu_roi_buffer][i] = param->nu_x[i+x_i_frac_min];
    x_nu_min = fringes->nu_roi[fringes->c_nu_roi_buffer][0];
    x_nu_max = fringes->nu_roi[fringes->c_nu_roi_buffer][roi_fit_size-1];
    //scale x axis to prevent possibly very different values between x and y
    x_nu_min = x_nu_min * 2 * M_PI;
    x_nu_max = x_nu_max * 2 * M_PI;
    scale = 2 * M_PI / (x_nu_max-x_nu_min);
    sdi_v2_offset_then_scale( fringes->nu_roi[fringes->c_nu_roi_buffer], roi_fit_size, nu0, scale);

    //a temporary ds to fit with expected input of fit_ds_to_xn_polynome
    tmp = build_adjust_data_set(tmp, roi_fit_size, roi_fit_size);
    for (i=0 ; i<roi_fit_size ; i++)
    {
	     tmp->xd[i] = fringes->nu_roi[fringes->c_nu_roi_buffer][i] ;
	     tmp->yd[i] = fringes->unwrapped_phase_roi[fringes->c_unwrapped_phase_roi_buffer][i];
    }
# ifdef WAS_LIKE_THAT
    fit_ds_to_xn_polynome( tmp, 2, -FLT_MAX, FLT_MAX, -FLT_MAX, FLT_MAX, &a);
    if (a != NULL)
    {
      phase_coeff[0] = a[0];
      phase_coeff[1] = a[1];
      free(a);
      a= NULL;
    }
#else
    //back to omega, to get slope in pixel
    compute_least_square_fit_on_ds(tmp, 1, phase_coeff+1, phase_coeff);
#endif
    phase_coeff[1] = phase_coeff[1] * scale / 2 / M_PI;

    //erases what one does not want to keep
    if (fringes->n_nu_roi_buffer == 0)
    {
	     free(fringes->nu_roi[0]);
	     fringes->nu_roi[0] = NULL;
       free(fringes->nu_roi);
       fringes->nu_roi = NULL;
    }
    if (fringes->n_unwrapped_phase_roi_buffer == 0)
    {
	     free(fringes->unwrapped_phase_roi[0]);
	     fringes->unwrapped_phase_roi[0] = NULL;
	     free(fringes->unwrapped_phase_roi);
	     fringes->unwrapped_phase_roi = NULL;
    }
    if (fringes->n_phase_buffer == 0)
    {
	     free(fringes->phase[0]);
	     fringes->phase[0] = NULL;
       free(fringes->phase);
       fringes->phase = NULL;
    }
  free_data_set(tmp);
  tmp = NULL;
  return 0;
}

//signal processing in real-space
int sdi_v2_process_sdi_fringes_x_profile( sdi_v2_fringes *fringes, sdi_v2_parameter *param)
{
  int max_i, j;
  float max;

  if (fringes == NULL || param == NULL)  return 1;
  if (fringes->n_x_window_buffer == 0)
  {
    fringes->c_x_window_buffer = 0;
    if (fringes->x_window == NULL) fringes->x_window = (float **)calloc(1, sizeof(float *));
    fringes->x_window[0] = (float *)realloc(fringes->x_window[0], param->x_roi_size *sizeof(float));  // possible memory leak !
  }

  //mettre une alarme sur position du max?
  sdi_v2_array_max( fringes->raw_x_profile[fringes->c_raw_x_profile_buffer], 0, param->x_roi_size, &max, &max_i);
  sdi_v2_generate_hypergaussian_array( fringes->x_window[fringes->c_x_window_buffer], param->x_roi_size, max_i - param->x_window_fwhm,
                                                          max_i + param->x_window_fwhm, param->x_window_order);


  if (fringes->n_windowed_x_profile_buffer == 0)
  {
    fringes->c_windowed_x_profile_buffer = 0;
    if (fringes->windowed_x_profile == NULL) fringes->windowed_x_profile = (float **)calloc(1, sizeof( float *));
    fringes->windowed_x_profile[0] = (float *)realloc( fringes->windowed_x_profile[0], param->x_roi_size*sizeof(float)); // possible memory leak !
  }

  //fills the current windowed profile
  for (j=0; j<param->x_roi_size; j++)
  {
    fringes->windowed_x_profile[fringes->c_windowed_x_profile_buffer][j] =
      (fringes->raw_x_profile[fringes->c_raw_x_profile_buffer][j]-fringes->background) * fringes->x_window[fringes->c_x_window_buffer][j] ;
  }

  //if wee keep the window in memory, it is useful to plot it with the same scale as the signal..
  if (fringes->n_x_window_buffer != 0)  sdi_v2_offset_then_scale(fringes->x_window[fringes->c_x_window_buffer], param->x_roi_size, 0, max);
  else
  {
    free(fringes->x_window[0]);
    fringes->x_window[0] = NULL;
    free(fringes->x_window);  // possible memory leak !
    fringes->x_window = NULL;
  }

  if (fringes->n_raw_x_profile_buffer == 0)
  {
    free(fringes->raw_x_profile[0]);
    fringes->raw_x_profile[0] = NULL;
    free(fringes->raw_x_profile);  // possible memory leak !
    fringes->raw_x_profile = NULL;
  }

  return 0;
}


//the buffer of raw_x_profile for fringe must have been previously allocated
//except if you do not want to keep a buffer of this quantity
int sdi_v2_extract_sdi_fringes_raw_x_profile_from_image(O_i *oi, sdi_v2_fringes *fringes, sdi_v2_parameter *param)
{
  if (oi == NULL || fringes == NULL || param == NULL)  return 1;
  //in case of nothing is allocated then all on the fly
  if (fringes->n_raw_x_profile_buffer == 0)
  {
    fringes->c_raw_x_profile_buffer = 0;
    if (fringes->raw_x_profile == NULL) fringes->raw_x_profile = (float **)calloc(1, sizeof(float *));
    fringes->raw_x_profile[0] = (float *)realloc(fringes->raw_x_profile[0], param->x_roi_size * sizeof(float));
  }
  sdi_v2_extract_x_profile_array_from_image(oi, fringes->raw_x_profile[fringes->c_raw_x_profile_buffer], fringes->x_roi_min, fringes->x_roi_max,
                                                                                      fringes->y_roi_min, fringes->y_roi_max);
  //to do some stuff on raw profile before it is erased (when no buffer asked)
  if (fringes->need_background == true) //one feature to substrat background by removing min of histogram
  {
    d_s *histo = NULL;
    histo = sdi_v2_compute_histo_of_tab(fringes->raw_x_profile[fringes->c_raw_x_profile_buffer], param->x_roi_size);
    fringes->background = (histo->xd[0] > 0) ? histo->xd[0] : 0;
    fringes->need_background = false;
    free_data_set(histo);
    histo = NULL;
  }
  return 0;
}





////////////allocation functions ///////////////////////
////////////the sdi v2 offers 2 ways of allocating the memory for the
///fields, an independant one and a one where you re-use existing
///ptr of arrays (to be integrated to track_util whose objects are allocated as arrays when declared)

//removes one pattern of fringes from the fringes list of general
//MUST NOT be called when linked with external ptr (in trackbead for example)
int sdi_v2_remove_one_sdi_fringes(sdi_v2_fringes *fringes)
{
  //int i;
  if (fringes==NULL) return D_O_K;
  if (fringes->hard_linked == true) return D_O_K;//impossible if linked to array used elsewhere

  free(fringes->z_coupled_t);
  fringes->z_coupled_t = NULL;
  free(fringes->x_coupled_t);
  fringes->x_coupled_t = NULL;
  free(fringes->contrast_t);
  fringes->contrast_t = NULL;
  free(fringes->I_max_t);
  fringes->I_max_t = NULL;

  free(fringes->raw_phi_0_t);
  fringes->raw_phi_0_t = NULL;
  free(fringes->raw_phi_1_t);
  fringes->raw_phi_1_t = NULL;
  free(fringes->corrected_phi_0_t);
  fringes->corrected_phi_0_t = NULL;
  free(fringes->phase_jump_t);
  fringes->phase_jump_t = NULL;
  free(fringes->roi_fit_size_t);
  fringes->roi_fit_size_t = NULL;
  free(fringes->nu0_t);
  fringes->nu0_t = NULL;

  int i;
  for (i=0;i<fringes->n_windowed_x_profile_buffer;i++)
  {
    free(fringes->windowed_x_profile[i]);
    fringes->windowed_x_profile[i] = NULL;
  }
  free(fringes->windowed_x_profile);
  fringes->windowed_x_profile = NULL;
  for (i=0;i<fringes->n_raw_x_profile_buffer;i++)
  {
    free(fringes->raw_x_profile[i]);
    fringes->raw_x_profile[i] = NULL;
  }
  free(fringes->raw_x_profile);
  fringes->raw_x_profile = NULL;
  for (i=0;i<fringes->n_x_window_buffer;i++)
  {
    free(fringes->x_window[i]);
    fringes->x_window[i] = NULL;
  }
  free(fringes->x_window);
  fringes->x_window = NULL;
  for (i=0;i<fringes->n_phase_buffer;i++)
  {
    free(fringes->phase[i]);
    fringes->phase[i] = NULL;
  }
  free(fringes->phase);
  fringes->phase = NULL;
  for (i=0;i<fringes->n_amplitude_buffer;i++)
  {
    free(fringes->amplitude[i]);
    fringes->amplitude[i] = NULL;
  }
  free(fringes->amplitude);
  fringes->amplitude = NULL;
  for (i=0;i<fringes->n_spectrum_buffer;i++)
  {
    free(fringes->spectrum[i]);
    fringes->spectrum[i] = NULL;
  }
  free(fringes->spectrum);
  fringes->spectrum = NULL;
  for (i=0;i<fringes->n_spectrum_log_buffer;i++)
  {
    free(fringes->spectrum_log[i]);
    fringes->spectrum_log[i] = NULL;
  }
  free(fringes->spectrum_log);
  fringes->spectrum_log = NULL;
  for (i=0;i<fringes->n_unwrapped_phase_roi_buffer;i++)
  {
    free(fringes->unwrapped_phase_roi[i]);
    fringes->unwrapped_phase_roi[i] = NULL;
  }
  free(fringes->unwrapped_phase_roi);
  fringes->unwrapped_phase_roi = NULL;
  for (i=0;i<fringes->n_raw_phase_roi_buffer;i++)
  {
    free(fringes->raw_phase_roi[i]);
    fringes->raw_phase_roi[i] = NULL;
  }
  free(fringes->raw_phase_roi);
  fringes->raw_phase_roi = NULL;
  for (i=0;i<fringes->n_nu_roi_buffer;i++)
  {
    free(fringes->nu_roi[i]);
    fringes->nu_roi[i] = NULL;
  }
  free(fringes->nu_roi);
  fringes->nu_roi = NULL;

  free(fringes);
  fringes = NULL;
  return 0;
}



//allocated outsied
int sdi_v2_init_one_sdi_fringes_by_position(sdi_v2_fringes *fringes, int xc, int yc)
{
  if (fringes == NULL) return 1;
  fringes->xc = xc;
  fringes->yc = yc;

  //key parameters defining fringes state
  fringes->v_0 = 999;
  fringes->v_1 = 999;
  fringes->which_half = 999;
  fringes->nu0 = 999;
  fringes->gy = 999;
  fringes->v_x = 999;
  fringes->background= 0 ;
  fringes->roi_fit_size0 = 1;
  fringes->hard_linked = false;
  fringes->need_reference_point = false;
  fringes->need_background = false;

  //essential 1d outputs
  fringes->n_output_t = 0;
  fringes->c_output_t = 0;
  fringes->z_coupled_t = NULL;
  fringes->x_coupled_t = NULL;
  fringes->contrast_t=NULL;
  fringes->I_max_t=NULL;

  //the 1d data in debug/expert mode
  fringes->n_phi_t = 0;
  fringes->c_phi_t = 0;
  fringes->nu0_t = NULL;
  fringes->raw_phi_0_t = NULL;
  fringes->raw_phi_1_t = NULL;
  fringes->corrected_phi_0_t = NULL;
  fringes->phase_jump_t = NULL;
  fringes->roi_fit_size_t = NULL;

  fringes->n_ref_points = 0;
  fringes->phi_0_ref_points = (float *)calloc(32, sizeof(float));
  fringes->phi_1_ref_points = (float *)calloc(32, sizeof(float));
  fringes->k_pi_ref_points = (float *)calloc(32, sizeof(float));

  //the 2d data
  int default_alloc = 0 ;
  fringes->windowed_x_profile=NULL;
  fringes->n_windowed_x_profile_buffer = default_alloc;
  fringes->c_windowed_x_profile_buffer = 0;
  fringes->raw_x_profile=NULL;
  fringes->n_raw_x_profile_buffer = default_alloc;
  fringes->c_raw_x_profile_buffer = 0;
  fringes->x_window=NULL;
  fringes->n_x_window_buffer = default_alloc;
  fringes->c_x_window_buffer = 0;

  //Fourier space data
  fringes->phase=NULL;
  fringes->n_phase_buffer = default_alloc;
  fringes->c_phase_buffer = 0;
  fringes->amplitude=NULL;
  fringes->n_amplitude_buffer = default_alloc;
  fringes->c_amplitude_buffer = 0;
  fringes->unwrapped_phase_roi=NULL;
  fringes->n_unwrapped_phase_roi_buffer = 0;
  fringes->c_unwrapped_phase_roi_buffer = 0;
  fringes->raw_phase_roi=NULL;
  fringes->n_raw_phase_roi_buffer = 0; //tv : bug corrected 31/05/17
  fringes->c_raw_phase_roi_buffer = 0;
  fringes->spectrum=NULL;
  fringes->n_spectrum_buffer = default_alloc;
  fringes->c_spectrum_buffer = 0;
  fringes->spectrum_log=NULL;
  fringes->n_spectrum_buffer = 0;
  fringes->c_spectrum_buffer = 0;
  fringes->nu_roi = NULL;
  fringes->n_nu_roi_buffer = 0;
  fringes->c_nu_roi_buffer = 0;

  return 0;
}
//current points
int sdi_v2_update_c_profile_fringes_points(sdi_v2_fringes *fringes, int ci)
{
  if (fringes == NULL) return 1;
  if (fringes->n_output_t !=0) fringes->c_output_t = ci % fringes->n_output_t;
  if (fringes->n_phi_t !=0 ) fringes->c_phi_t = ci % fringes->n_phi_t;
  if (fringes->n_raw_x_profile_buffer !=0 ) fringes->c_raw_x_profile_buffer = ci % fringes->n_raw_x_profile_buffer;
  if (fringes->n_x_window_buffer !=0 ) fringes->c_x_window_buffer = ci % fringes->n_x_window_buffer;
  if (fringes->n_windowed_x_profile_buffer !=0 ) fringes->c_windowed_x_profile_buffer = ci % fringes->n_windowed_x_profile_buffer;
  if (fringes->n_phase_buffer !=0 )  fringes->c_phase_buffer = ci % fringes->n_phase_buffer;
  if (fringes->n_amplitude_buffer !=0 ) fringes->c_amplitude_buffer = ci % fringes->n_amplitude_buffer;
  if (fringes->n_unwrapped_phase_roi_buffer !=0 ) fringes->c_unwrapped_phase_roi_buffer = ci % fringes->n_unwrapped_phase_roi_buffer;
  if (fringes->n_raw_phase_roi_buffer !=0 ) fringes->c_raw_phase_roi_buffer = ci % fringes->n_raw_phase_roi_buffer;
  if (fringes->n_spectrum_buffer !=0 ) fringes->c_spectrum_buffer = ci % fringes->n_spectrum_buffer;
  if (fringes->n_spectrum_log_buffer !=0 ) fringes->c_spectrum_log_buffer = ci % fringes->n_spectrum_log_buffer;
  if (fringes->n_nu_roi_buffer !=0 ) fringes->c_nu_roi_buffer = ci % fringes->n_nu_roi_buffer;
  return 0;
}
//this allocates the buffer of 2d curves
//please be careful with that when the 1d fields has been set up by linking
//with existing tab (as when it is plugged to track_util bead object fields which are array, not dynamic allocation)
//if the realloc requires to move the array then there will be an error when the trakc_util.c will use that array
int sdi_v2_allocate_sdi_v2_fringes_2d_float_fields(sdi_v2_fringes *fringes, sdi_v2_parameter *param, int buffer_size, int which_data)
{
  int j;
  int max_roi = param->fp_x->n_2;

  if (fringes == NULL || param == NULL) return 1;
  switch (which_data) {

    /////////////////////real-space profiles
    case PTR_FRINGES_WINDOWED_X_PROFILE :
    for (j=buffer_size ; j<fringes->n_windowed_x_profile_buffer ; j++)
    {
      free(fringes->windowed_x_profile[j]);
      fringes->windowed_x_profile[j]=NULL;
    }
    if (fringes->windowed_x_profile == NULL) fringes->windowed_x_profile = (float **)calloc(buffer_size, sizeof(float *));
    else fringes->windowed_x_profile = (float **)realloc(fringes->windowed_x_profile, buffer_size * sizeof(float *));
    for (j=0 ;j<buffer_size ;j++)
    {
      if ( j < fringes->n_windowed_x_profile_buffer)
      {
        fringes->windowed_x_profile[j] = (float *) realloc(fringes->windowed_x_profile[j], param->x_roi_size * sizeof(float));
      }
      else fringes->windowed_x_profile[j] = (float *)calloc(param->x_roi_size, sizeof(float));
    }
    fringes->c_windowed_x_profile_buffer = 0;
    fringes->n_windowed_x_profile_buffer = buffer_size;
    break;

    case PTR_FRINGES_RAW_X_PROFILE :
    for (j=buffer_size ; j<fringes->n_raw_x_profile_buffer ; j++)
    {
      free(fringes->raw_x_profile[j]);
      fringes->raw_x_profile[j]=NULL;
    }
    if (fringes->raw_x_profile == NULL)
      fringes->raw_x_profile = (float **)calloc(buffer_size, sizeof(float *));
    else fringes->raw_x_profile = (float **)realloc(fringes->raw_x_profile, buffer_size * sizeof(float *));
    for (j=0 ;j<buffer_size ;j++)
    {
      if ( j < fringes->n_raw_x_profile_buffer)
      {
        fringes->raw_x_profile[j] = (float *) realloc(fringes->raw_x_profile[j], param->x_roi_size * sizeof(float));
      }
      else fringes->raw_x_profile[j] = (float *)calloc(param->x_roi_size, sizeof(float));
    }
    fringes->c_raw_x_profile_buffer = 0;
    fringes->n_raw_x_profile_buffer = buffer_size;
    break;

    case PTR_FRINGES_X_WINDOW :
    for (j=buffer_size ; j<fringes->n_x_window_buffer ; j++)
    {
      free(fringes->x_window[j]);
      fringes->x_window[j]=NULL;
    }
    if (fringes->x_window == NULL) fringes->x_window = (float **)calloc(buffer_size, sizeof(float *));
    else fringes->x_window = (float **)realloc(fringes->x_window, buffer_size * sizeof(float *));
    for (j=0 ;j<buffer_size ;j++)
    {
      if ( j < fringes->n_x_window_buffer)
      {
        fringes->x_window[j] = (float *) realloc(fringes->x_window[j], param->x_roi_size * sizeof(float));
      }
      else fringes->x_window[j] = (float *)calloc(param->x_roi_size, sizeof(float));
    }
    fringes->c_x_window_buffer = 0;
    fringes->n_x_window_buffer = buffer_size;
    break;


    ////////////////////////////:fourier-space profiles
    case PTR_FRINGES_PHASE :
    for (j=buffer_size ; j<fringes->n_phase_buffer ; j++)
    {
      free(fringes->phase[j]);
      fringes->phase[j]=NULL;
    }
    if (fringes->phase == NULL) fringes->phase = (float **)calloc(buffer_size, sizeof(float *));
    else fringes->phase = (float **)realloc(fringes->phase, buffer_size * sizeof(float *));
    for (j=0 ;j<buffer_size ;j++)
    {
      if ( j < fringes->n_phase_buffer)
      {
        fringes->phase[j] = (float *) realloc(fringes->phase[j], param->fp_x->n_2 * sizeof(float));
      }
      else fringes->phase[j] = (float *)calloc(param->fp_x->n_2, sizeof(float));
    }
    fringes->c_phase_buffer = 0;
    fringes->n_phase_buffer = buffer_size;
    break;

    case PTR_FRINGES_AMPLITUDE :
    for (j=buffer_size ; j<fringes->n_amplitude_buffer ; j++)
    {
      free(fringes->amplitude[j]);
      fringes->amplitude[j]=NULL;
    }
    if (fringes->amplitude == NULL) fringes->amplitude = (float **)calloc(buffer_size, sizeof(float *));
    else fringes->amplitude = (float **)realloc(fringes->amplitude, buffer_size * sizeof(float *));
    for (j=0 ;j<buffer_size ;j++)
    {
      if ( j < fringes->n_amplitude_buffer)
      {
        fringes->amplitude[j] = (float *) realloc(fringes->amplitude[j], param->fp_x->n_2 * sizeof(float));
      }
      else fringes->amplitude[j] = (float *)calloc(param->fp_x->n_2, sizeof(float));
    }
    fringes->c_amplitude_buffer = 0;
    fringes->n_amplitude_buffer = buffer_size;
    break;

    case PTR_FRINGES_UNWRAPPED_PHASE_ROI :
    for (j=buffer_size ; j<fringes->n_unwrapped_phase_roi_buffer ; j++)
    {
      free(fringes->unwrapped_phase_roi[j]);
      fringes->unwrapped_phase_roi[j]=NULL;
    }
    if (fringes->unwrapped_phase_roi == NULL) fringes->unwrapped_phase_roi = (float **)calloc(buffer_size, sizeof(float *));
    else fringes->unwrapped_phase_roi = (float **)realloc(fringes->unwrapped_phase_roi, buffer_size * sizeof(float *));
    for (j=0 ;j<buffer_size ;j++)
    {
      if ( j < fringes->n_unwrapped_phase_roi_buffer)
      {
        fringes->unwrapped_phase_roi[j] = (float *) realloc(fringes->unwrapped_phase_roi[j], param->fp_x->n_2 * sizeof(float));
      }
      else fringes->unwrapped_phase_roi[j] = (float *)calloc(param->fp_x->n_2, sizeof(float));
    }
    fringes->c_unwrapped_phase_roi_buffer = 0;
    fringes->n_unwrapped_phase_roi_buffer = buffer_size;
    break;

    case PTR_FRINGES_RAW_PHASE_ROI :
    for (j=buffer_size ; j<fringes->n_raw_phase_roi_buffer ; j++)
    {
      free(fringes->raw_phase_roi[j]);
      fringes->raw_phase_roi[j]=NULL;
    }
    if (fringes->raw_phase_roi == NULL) fringes->raw_phase_roi = (float **)calloc(buffer_size, sizeof(float *));
    else fringes->raw_phase_roi = (float **)realloc(fringes->raw_phase_roi, buffer_size * sizeof(float *));
    for (j=0 ;j<buffer_size ;j++)
    {
      if ( j < fringes->n_raw_phase_roi_buffer)
      {
        fringes->raw_phase_roi[j] = (float *) realloc(fringes->raw_phase_roi[j], param->fp_x->n_2 * sizeof(float));
      }
      else fringes->raw_phase_roi[j] = (float *)calloc(param->fp_x->n_2, sizeof(float));
    }
    fringes->c_raw_phase_roi_buffer = 0;
    fringes->n_raw_phase_roi_buffer = buffer_size;
    break;

    case PTR_FRINGES_SPECTRUM :
    for (j=buffer_size ; j<fringes->n_spectrum_buffer ; j++)
    {
      free(fringes->spectrum[j]);
      fringes->spectrum[j]=NULL;
    }
    if (fringes->spectrum == NULL) fringes->spectrum = (float **)calloc(buffer_size, sizeof(float *));
    else fringes->spectrum = (float **)realloc(fringes->spectrum, buffer_size * sizeof(float *));
    for (j=0 ;j<buffer_size ;j++)
    {
      if ( j < fringes->n_spectrum_buffer)
      {
        fringes->spectrum[j] = (float *) realloc(fringes->spectrum[j], param->fp_x->n_2 * sizeof(float));
      }
      else fringes->spectrum[j] = (float *)calloc(param->fp_x->n_2, sizeof(float));
    }
    fringes->c_spectrum_buffer = 0;
    fringes->n_spectrum_buffer = buffer_size;
    break;

    case PTR_FRINGES_SPECTRUM_LOG :
    for (j=buffer_size ; j<fringes->n_spectrum_log_buffer ; j++)
    {
      free(fringes->spectrum_log[j]);
      fringes->spectrum_log[j]=NULL;
    }
    if (fringes->spectrum_log == NULL) fringes->spectrum_log = (float **)calloc(buffer_size, sizeof(float *));
    else fringes->spectrum_log = (float **)realloc(fringes->spectrum_log, buffer_size * sizeof(float *));
    for (j=0 ;j<buffer_size ;j++)
    {
      if ( j < fringes->n_spectrum_log_buffer)
      {
        fringes->spectrum_log[j] = (float *) realloc(fringes->spectrum_log[j], param->fp_x->n_2 * sizeof(float));
      }
      else fringes->spectrum_log[j] = (float *)calloc(param->fp_x->n_2, sizeof(float));
    }
    fringes->c_spectrum_log_buffer = 0;
    fringes->n_spectrum_log_buffer = buffer_size;
    break;

    case PTR_FRINGES_NU_ROI :
    for (j=buffer_size ; j<fringes->n_nu_roi_buffer ; j++)
    {
      free(fringes->nu_roi[j]);
      fringes->nu_roi[j]=NULL;
    }
    if (fringes->nu_roi == NULL) fringes->nu_roi = (float **)calloc(buffer_size, sizeof(float *));
    else fringes->nu_roi = (float **)realloc(fringes->nu_roi, buffer_size * sizeof(float *));
    for (j=0 ;j<buffer_size ;j++)
    {
      if ( j < fringes->n_nu_roi_buffer)
      {
        fringes->nu_roi[j] = (float *) realloc(fringes->nu_roi[j], param->fp_x->n_2 * sizeof(float));
      }
      else fringes->nu_roi[j] = (float *)calloc(param->fp_x->n_2, sizeof(float));
    }
    fringes->c_nu_roi_buffer = 0;
    fringes->n_nu_roi_buffer = buffer_size;
    break;
  }
  return 0;
}

int sdi_v2_allocate_sdi_v2_fringes_phi_t_fields(sdi_v2_fringes *fringes, int size)
{
  if(fringes == NULL) return 1;
  sdi_v2_allocate_sdi_v2_fringes_1d_float_fields(fringes, size, PTR_FRINGES_RAW_PHI_0_T);
  sdi_v2_allocate_sdi_v2_fringes_1d_float_fields(fringes, size, PTR_FRINGES_RAW_PHI_1_T);
  sdi_v2_allocate_sdi_v2_fringes_1d_float_fields(fringes, size, PTR_FRINGES_CORRECTED_PHI_0_T);
  sdi_v2_allocate_sdi_v2_fringes_1d_float_fields(fringes, size, PTR_FRINGES_NU0_T);
  sdi_v2_allocate_sdi_v2_fringes_1d_float_fields(fringes, size, PTR_FRINGES_PHASE_JUMP_T);
  sdi_v2_allocate_sdi_v2_fringes_1d_float_fields(fringes, size, PTR_FRINGES_NU_ROI_FIT_SIZE);
  fringes->n_phi_t = size;
  fringes->c_phi_t = 0;
  return 0;
}

//allocates the fields that are always outputs
//none of them being linked to pre-existing array
int sdi_v2_allocate_sdi_v2_fringes_output_t_fields(sdi_v2_fringes *fringes, int size)
{
  //win_printf_OK("boh");
  if(fringes == NULL) return 1;
  sdi_v2_allocate_sdi_v2_fringes_1d_float_fields(fringes, size, PTR_FRINGES_Z_COUPLED_T);
  sdi_v2_allocate_sdi_v2_fringes_1d_float_fields(fringes, size, PTR_FRINGES_X_COUPLED_T);
  sdi_v2_allocate_sdi_v2_fringes_1d_float_fields(fringes, size, PTR_FRINGES_CONTRAST_T);
  sdi_v2_allocate_sdi_v2_fringes_1d_float_fields(fringes, size, PTR_FRINGES_I_MAX_T);
  //sdi_v2_allocate_sdi_v2_fringes_1d_float_fields(fringes, size, PTR_FRINGES_QUALITY_FACTOR_T);
  fringes->n_output_t = size;
  fringes->c_output_t = 0;
  return 0;
}

//realloc forbidden when fringes field allocated with externel ptr
int sdi_v2_allocate_sdi_v2_fringes_1d_float_fields(sdi_v2_fringes *fringes, int size, int which_data)
{
  if(fringes == NULL) return 1;
  switch (which_data) {

    case PTR_FRINGES_Z_COUPLED_T :
    if (fringes->z_coupled_t == NULL) fringes->z_coupled_t = (float *)calloc(size, sizeof(float));
    else fringes->z_coupled_t = (float *)realloc(fringes->z_coupled_t, size * sizeof(float));
    break;
    case PTR_FRINGES_X_COUPLED_T :
    if (fringes->x_coupled_t == NULL) fringes->x_coupled_t = (float *)calloc(size, sizeof(float));
    else fringes->x_coupled_t = (float *)realloc(fringes->x_coupled_t, size * sizeof(float));
    break;
    case PTR_FRINGES_CONTRAST_T :
    if (fringes->contrast_t == NULL) fringes->contrast_t = (float *)calloc(size, sizeof(float));
    else fringes->contrast_t = (float *)realloc(fringes->contrast_t, size * sizeof(float));
    break;
    case PTR_FRINGES_I_MAX_T :
    if (fringes->I_max_t == NULL) fringes->I_max_t = (float *)calloc(size, sizeof(float));
    else fringes->I_max_t = (float *)realloc(fringes->I_max_t, size * sizeof(float));
    break;

    /*case PTR_FRINGES_QUALI :
    if (fringes->I_max_t == NULL) fringes->I_max_t = (float *)calloc(size, sizeof(float));
    else fringes->I_max_t = (float *)realloc(fringes->I_max_t, size * sizeof(float));
    break;*/

    //debugging purpose
    case PTR_FRINGES_RAW_PHI_0_T :
    if (fringes->raw_phi_0_t == NULL) fringes->raw_phi_0_t = (float *)calloc(size, sizeof(float));
    else fringes->raw_phi_0_t = (float *)realloc(fringes->raw_phi_0_t, size * sizeof(float));
    break;
    case PTR_FRINGES_RAW_PHI_1_T :
    if (fringes->raw_phi_1_t == NULL) fringes->raw_phi_1_t = (float *)calloc(size, sizeof(float));
    else fringes->raw_phi_1_t = (float *)realloc(fringes->raw_phi_1_t, size * sizeof(float));
    break;
    case PTR_FRINGES_NU0_T :
    if (fringes->nu0_t == NULL) fringes->nu0_t = (float *)calloc(size, sizeof(float));
    else fringes->nu0_t = (float *)realloc(fringes->nu0_t, size * sizeof(float));
    break;
    case PTR_FRINGES_CORRECTED_PHI_0_T :
    if (fringes->corrected_phi_0_t == NULL) fringes->corrected_phi_0_t = (float *)calloc(size, sizeof(float));
    else fringes->corrected_phi_0_t = (float *)realloc(fringes->corrected_phi_0_t, size * sizeof(float));
    break;
    case PTR_FRINGES_PHASE_JUMP_T :
    if (fringes->phase_jump_t == NULL) fringes->phase_jump_t = (float *)calloc(size, sizeof(float));
    else fringes->phase_jump_t = (float *)realloc(fringes->phase_jump_t, size * sizeof(float));
    break;
    case PTR_FRINGES_NU_ROI_FIT_SIZE :
    if (fringes->roi_fit_size_t == NULL) fringes->roi_fit_size_t = (float *)calloc(size, sizeof(float));
    else fringes->roi_fit_size_t = (float *)realloc(fringes->roi_fit_size_t, size * sizeof(float));
    break;

  }
  return 0;
}


//switch to 2d fields : as for 1d fields it switch ptr onto the existing 2d field of the fringes object
//if empty, one can define the 2d ptr with existing 2d ptr (allocated outside) then defining also the n buffer_size

//attention : si tu utilises des ptr préfixés, qui sont donc utilisés par ailleurs par d'autres fonctions
//(type les TABLEAUX de track_util.h), faut surtout pas bouger les choses (donc éviter les realloc, et tout ça)
int sdi_v2_switch_ptr_to_sdi_fringes_2d_float_field(sdi_v2_fringes *fringes, int which_data, float **field, int *n_field, int *c_field,
                                                                                              float **ext_float, int n_ext_float)
{
  int i;

  if(fringes == NULL) return 1;
  switch(which_data){

    case PTR_FRINGES_WINDOWED_X_PROFILE :
    if (fringes->windowed_x_profile == NULL)
    {
      if (n_ext_float != 0)
      {
        fringes->n_windowed_x_profile_buffer = n_ext_float;
        fringes->windowed_x_profile = (float **)calloc(n_ext_float, sizeof(float *));
        for ( i=0 ; i<n_ext_float ; i++) fringes->windowed_x_profile[i] = ext_float[i];
        fringes->hard_linked = true; //this forbids to remove the fringes if asked
      }
      else
      {
        fringes->windowed_x_profile = (float **)calloc(1, sizeof(float *));
        fringes->windowed_x_profile[0] = (float *)calloc(1, sizeof(float));
        fringes->n_windowed_x_profile_buffer = 0;
      }
      fringes->c_windowed_x_profile_buffer = 0;
    }
    for (i=0 ; i<fringes->n_windowed_x_profile_buffer ; i++) field[i] = fringes->windowed_x_profile[i];
    *n_field = fringes->n_windowed_x_profile_buffer;
    *c_field = fringes->c_windowed_x_profile_buffer;
    break;

    case PTR_FRINGES_RAW_X_PROFILE :
    if (fringes->raw_x_profile == NULL)
    {
      if (n_ext_float != 0)
      {
        fringes->n_raw_x_profile_buffer = n_ext_float;
        fringes->raw_x_profile = (float **)calloc(n_ext_float, sizeof(float *));
        for ( i=0 ; i<n_ext_float ; i++) fringes->raw_x_profile[i] = ext_float[i];
        fringes->hard_linked = true;
      }
      else
      {
        fringes->raw_x_profile = (float **)calloc(1, sizeof(float *));
        fringes->raw_x_profile[0] = (float *)realloc(fringes->raw_x_profile[0], sizeof(float));
        fringes->n_raw_x_profile_buffer = 0;
      }
      fringes->c_raw_x_profile_buffer = 0;
    }
    for (i=0 ; i<fringes->n_raw_x_profile_buffer ; i++) field[i] = fringes->raw_x_profile[i];
    *n_field = fringes->n_raw_x_profile_buffer;
    *c_field = fringes->c_raw_x_profile_buffer;
    break;

    case PTR_FRINGES_X_WINDOW :
    if (fringes->x_window == NULL)
    {
      if (n_ext_float != 0)
      {
        fringes->n_x_window_buffer = n_ext_float;
        fringes->x_window = (float **)calloc(n_ext_float, sizeof(float *));
        for ( i=0 ; i<n_ext_float ; i++) fringes->x_window[i] = ext_float[i];
        fringes->hard_linked = true;
      }
      else
      {
        fringes->x_window = (float **)calloc(1, sizeof(float *));
        fringes->x_window[0] = (float *)realloc(fringes->x_window[0], sizeof(float));
        fringes->n_x_window_buffer = 0;
      }
      fringes->c_x_window_buffer = 0;
    }
    for (i=0 ; i<fringes->n_x_window_buffer ; i++) field[i] = fringes->x_window[i];
    *n_field = fringes->n_x_window_buffer;
    *c_field = fringes->c_x_window_buffer;
    break;

    //Fourier space profile buffer
    case PTR_FRINGES_PHASE :
    if (fringes->phase == NULL)
    {
      if (n_ext_float != 0)
      {
        fringes->n_phase_buffer = n_ext_float;
        fringes->phase = (float **)calloc(n_ext_float, sizeof(float *));
        for ( i=0 ; i<n_ext_float ; i++) fringes->phase[i] = ext_float[i];
        fringes->hard_linked = true;
      }
      else
      {
        fringes->phase = (float **)calloc(1, sizeof(float *));
        fringes->phase[0] = (float *)realloc(fringes->phase[0], sizeof(float));
        fringes->n_phase_buffer = 0;
      }
      fringes->c_phase_buffer = 0;
    }
    for (i=0 ; i<fringes->n_phase_buffer ; i++) field[i] = fringes->phase[i];
    *n_field = fringes->n_phase_buffer;
    *c_field = fringes->c_phase_buffer;
    break;

    case PTR_FRINGES_AMPLITUDE :
    if (fringes->amplitude == NULL)
    {
      if (n_ext_float != 0)
      {
        fringes->n_amplitude_buffer = n_ext_float;
        fringes->amplitude = (float **)calloc(n_ext_float, sizeof(float *));
        for ( i=0 ; i<n_ext_float ; i++) fringes->amplitude[i] = ext_float[i];
        fringes->hard_linked = true;
      }
      else
      {
        fringes->amplitude = (float **)calloc(1, sizeof(float *));
        fringes->amplitude[0] = (float *)realloc(fringes->amplitude[0], sizeof(float));
        fringes->n_amplitude_buffer = 0;
      }
      fringes->c_amplitude_buffer = 0;
    }
    for (i=0 ; i<fringes->n_amplitude_buffer ; i++) field[i] = fringes->amplitude[i];
    *n_field = fringes->n_amplitude_buffer;
    *c_field = fringes->c_amplitude_buffer;
    break;

    case PTR_FRINGES_UNWRAPPED_PHASE_ROI :
    if (fringes->unwrapped_phase_roi == NULL)
    {
      if (n_ext_float != 0)
      {
        fringes->n_unwrapped_phase_roi_buffer = n_ext_float;
        fringes->unwrapped_phase_roi = (float **)calloc(n_ext_float, sizeof(float *));
        for ( i=0 ; i<n_ext_float ; i++) fringes->unwrapped_phase_roi[i] = ext_float[i];
        fringes->hard_linked = true;
      }
      else
      {
        fringes->unwrapped_phase_roi = (float **)calloc(1, sizeof(float *));
        fringes->unwrapped_phase_roi[0] = (float *)realloc(fringes->unwrapped_phase_roi[0], sizeof(float));
        fringes->n_unwrapped_phase_roi_buffer = 0;
      }
      fringes->c_unwrapped_phase_roi_buffer = 0;
    }
    for (i=0 ; i<fringes->n_unwrapped_phase_roi_buffer ; i++) field[i] = fringes->unwrapped_phase_roi[i];
    *n_field = fringes->n_unwrapped_phase_roi_buffer;
    *c_field = fringes->c_unwrapped_phase_roi_buffer;
    break;

    case PTR_FRINGES_RAW_PHASE_ROI :
    if (fringes->raw_phase_roi == NULL)
    {
      if (n_ext_float != 0)
      {
        fringes->n_raw_phase_roi_buffer = n_ext_float;
        fringes->raw_phase_roi = (float **)calloc(n_ext_float, sizeof(float *));
        for ( i=0 ; i<n_ext_float ; i++) fringes->raw_phase_roi[i] = ext_float[i];
        fringes->hard_linked = true;
      }
      else
      {
        fringes->raw_phase_roi = (float **)calloc(1, sizeof(float *));
        fringes->raw_phase_roi[0] = (float *)realloc(fringes->raw_phase_roi[0], sizeof(float));
        fringes->n_raw_phase_roi_buffer = 0;
      }
      fringes->c_raw_phase_roi_buffer = 0;
    }
    for (i=0 ; i<fringes->n_raw_phase_roi_buffer ; i++) field[i] = fringes->raw_phase_roi[i];
    *n_field = fringes->n_raw_phase_roi_buffer;
    *c_field = fringes->c_raw_phase_roi_buffer;
    break;

    case PTR_FRINGES_SPECTRUM :
    if (fringes->spectrum == NULL)
    {
      if (n_ext_float != 0)
      {
        fringes->n_spectrum_buffer = n_ext_float;
        fringes->spectrum = (float **)calloc(n_ext_float, sizeof(float *));
        for ( i=0 ; i<n_ext_float ; i++) fringes->spectrum[i] = ext_float[i];
        fringes->hard_linked = true;
      }
      else
      {
        fringes->spectrum = (float **)calloc(1, sizeof(float *));
        fringes->spectrum[0] = (float *)realloc(fringes->spectrum[0],sizeof(float));
        fringes->n_spectrum_buffer = 0;
      }
      fringes->c_spectrum_buffer = 0;
    }
    for (i=0 ; i<fringes->n_spectrum_buffer ; i++) field[i] = fringes->spectrum[i];
    *n_field = fringes->n_spectrum_buffer;
    *c_field = fringes->c_spectrum_buffer;
    break;

    case PTR_FRINGES_SPECTRUM_LOG :
    if (fringes->spectrum_log == NULL)
    {
      if (n_ext_float != 0)
      {
        fringes->n_spectrum_log_buffer = n_ext_float;
        fringes->spectrum_log = (float **)calloc(n_ext_float, sizeof(float *));
        for ( i=0 ; i<n_ext_float ; i++) fringes->spectrum_log[i] = ext_float[i];
        fringes->hard_linked = true;
      }
      else
      {
        fringes->spectrum_log = (float **)calloc(1, sizeof(float *));
        fringes->spectrum_log[0] = (float *)realloc(fringes->spectrum_log[0], sizeof(float));
        fringes->n_spectrum_log_buffer = 0;
      }
      fringes->c_spectrum_log_buffer = 0;
    }
    for (i=0 ; i<fringes->n_spectrum_log_buffer ; i++) field[i] = fringes->spectrum_log[i];
    *n_field = fringes->n_spectrum_log_buffer;
    *c_field = fringes->c_spectrum_log_buffer;
    break;

    case PTR_FRINGES_NU_ROI :
    if (fringes->nu_roi == NULL)
    {
      if (n_ext_float != 0)
      {
        fringes->n_nu_roi_buffer = n_ext_float;
        fringes->nu_roi = (float **)calloc(n_ext_float, sizeof(float *));
        for ( i=0 ; i<n_ext_float ; i++) fringes->nu_roi[i] = ext_float[i];
        fringes->hard_linked = true;
      }
      else
      {
        fringes->nu_roi = (float **)calloc(1, sizeof(float *));
        fringes->nu_roi[0] = (float *)realloc(fringes->nu_roi[0], sizeof(float));
        fringes->n_nu_roi_buffer = 0;
      }
      fringes->c_nu_roi_buffer = 0;
    }
    for (i=0 ; i<fringes->n_nu_roi_buffer ; i++) field[i] = fringes->nu_roi[i];
    *n_field = fringes->n_nu_roi_buffer;
    *c_field = fringes->c_nu_roi_buffer;
    break;

  }
  return 0;
}


//switch to 1d float fields of fringes structure
//if it is empty the last argument, if not empty, allows to chose the adress of the field (useful to link this field to an existing memory)
//if both inputs are empty, then it fills with one point to at least being able to link it to something else like a plot
int sdi_v2_switch_ptr_to_sdi_fringes_1d_float_field(sdi_v2_fringes *fringes, int which_data, float **field, float **ext_float)
{
  if(fringes == NULL) return 1;
  switch(which_data){
    //really essential for the following
    case PTR_FRINGES_Z_COUPLED_T :
    if (fringes->z_coupled_t == NULL)
    {
      if (ext_float == NULL) fringes->z_coupled_t = (float *)calloc(1, sizeof(float));
      else
      {
        fringes->z_coupled_t = *ext_float;
        fringes->hard_linked = true;
      }
    }
    *field = fringes->z_coupled_t;
    break;
    case PTR_FRINGES_X_COUPLED_T :
    if (fringes->x_coupled_t == NULL)
    {
      if (ext_float == NULL) fringes->x_coupled_t = (float *)calloc(1, sizeof(float));
      else
      {
        fringes->hard_linked = true;
        fringes->x_coupled_t = *ext_float;
      }
    }
    *field = fringes->x_coupled_t;
    break;
    case PTR_FRINGES_CONTRAST_T :
    if (fringes->contrast_t == NULL)
    {
      if (ext_float == NULL) fringes->contrast_t = (float *)calloc(1, sizeof(float));
      else
      {
        fringes->hard_linked = true;
        fringes->contrast_t = *ext_float;
      }
    }
    *field = fringes->contrast_t;
    break;
    case PTR_FRINGES_I_MAX_T :
    if (fringes->I_max_t == NULL)
    {
      if (ext_float == NULL) fringes->I_max_t = (float *)calloc(1, sizeof(float));
      else
      {
        fringes->hard_linked = true;
        fringes->I_max_t = *ext_float;
      }
    }
    *field = fringes->I_max_t;
    break;

    //for debugging, explaining etc
    case PTR_FRINGES_RAW_PHI_0_T :
    if (fringes->raw_phi_0_t == NULL)
    {
      if( ext_float == NULL) fringes->raw_phi_0_t = (float *)calloc(1, sizeof(float));
      else
      {
        fringes->hard_linked = true;
        fringes->raw_phi_0_t = *ext_float;
      }
    }
    *field = fringes->raw_phi_0_t;
    break;
    case PTR_FRINGES_RAW_PHI_1_T :
    if (fringes->raw_phi_1_t == NULL)
    {
      if (ext_float == NULL) fringes->raw_phi_1_t = (float *)calloc(1, sizeof(float));
      else
      {
        fringes->hard_linked = true;
        fringes->raw_phi_1_t = *ext_float;
      }
    }
    *field = fringes->raw_phi_1_t;
    break;
    case PTR_FRINGES_CORRECTED_PHI_0_T :
    if (fringes->corrected_phi_0_t == NULL)
    {
      if (ext_float == NULL) fringes->corrected_phi_0_t = (float *)calloc(1, sizeof(float));
      else
      {
        fringes->hard_linked = true;
        fringes->corrected_phi_0_t = *ext_float;
      }
    }
    *field = fringes->corrected_phi_0_t;
    break;
    case PTR_FRINGES_PHASE_JUMP_T :
    if (fringes->phase_jump_t == NULL)
    {
      if (ext_float == NULL) fringes->phase_jump_t = (float *)calloc(1, sizeof(float));
      else
      {
        fringes->hard_linked = true;
        fringes->phase_jump_t = *ext_float;
      }
    }
    *field = fringes->phase_jump_t;
    break;
    case PTR_FRINGES_NU0_T :
    if (fringes->nu0_t == NULL)
    {
      if (ext_float == NULL) fringes->nu0_t = (float *)calloc(1, sizeof(float));
      else
      {
        fringes->hard_linked = true;
        fringes->nu0_t = *ext_float;
      }
    }
    *field = fringes->nu0_t;
    break;
    /*
    case phase jump etc :
    if (fringes->nu0_t == NULL)
    {
      if (ext_float == NULL) fringes->nu0_t = (float *)calloc(1, sizeof(float));
      else
      {
        fringes->hard_linked = true;
        fringes->nu0_t = *ext_float;
      }
    }
    *field = fringes->nu0_t;*/
    break;
  }
  return 0;
}
int sdi_v2_fringes_parameter_update(sdi_v2_fringes *fringes, sdi_v2_parameter *param)
{
  if (fringes == NULL || param == NULL)  return 1;
  fringes->x_roi_min = fringes->xc - param->x_roi_size_2;
  fringes->x_roi_max = fringes->xc + param->x_roi_size_2;
  fringes->y_roi_min = fringes->yc - param->y_roi_size_2;
  fringes->y_roi_max = fringes->yc + param->y_roi_size_2;
  return 0;
}
int sdi_v2_reset_all_intermediate_data_of_fringes(sdi_v2_fringes *fringes)
{
  if (fringes == NULL)  return 1;
  sdi_v2_reset_data_of_fringes(fringes, PTR_FRINGES_Z_COUPLED_T);
  sdi_v2_reset_data_of_fringes(fringes, PTR_FRINGES_RAW_PHI_0_T);
  sdi_v2_reset_data_of_fringes(fringes, PTR_FRINGES_WINDOWED_X_PROFILE);
  sdi_v2_reset_data_of_fringes(fringes, PTR_FRINGES_RAW_X_PROFILE);
  sdi_v2_reset_data_of_fringes(fringes, PTR_FRINGES_X_WINDOW);
  sdi_v2_reset_data_of_fringes(fringes, PTR_FRINGES_AMPLITUDE);
  sdi_v2_reset_data_of_fringes(fringes, PTR_FRINGES_PHASE);
  sdi_v2_reset_data_of_fringes(fringes, PTR_FRINGES_UNWRAPPED_PHASE_ROI);
  sdi_v2_reset_data_of_fringes(fringes, PTR_FRINGES_RAW_PHASE_ROI);
  sdi_v2_reset_data_of_fringes(fringes, PTR_FRINGES_SPECTRUM);
  sdi_v2_reset_data_of_fringes(fringes, PTR_FRINGES_SPECTRUM_LOG);
  return 0;
}
//resets everything, buffer, go back to non-saving version of data treatment
int sdi_v2_reset_data_of_fringes(sdi_v2_fringes *fringes, int which_data)
{
  int i;
  if (fringes == NULL) return 1;
  switch (which_data) {
    case PTR_FRINGES_Z_COUPLED_T :
    free(fringes->z_coupled_t);
    fringes->z_coupled_t = NULL;
    free(fringes->x_coupled_t);
    fringes->x_coupled_t = NULL;
    free(fringes->contrast_t);
    fringes->contrast_t = NULL;
    free(fringes->I_max_t);
    fringes->I_max_t = NULL;
    fringes->n_output_t = 0;
    fringes->c_output_t = 0;
    break;
    case PTR_FRINGES_X_COUPLED_T:
    free(fringes->z_coupled_t);
    fringes->z_coupled_t = NULL;
    free(fringes->x_coupled_t);
    fringes->x_coupled_t = NULL;
    free(fringes->contrast_t);
    fringes->contrast_t = NULL;
    free(fringes->I_max_t);
    fringes->I_max_t = NULL;
    fringes->n_output_t = 0;
    fringes->c_output_t = 0;
    break;
    case PTR_FRINGES_CONTRAST_T:
    free(fringes->z_coupled_t);
    fringes->z_coupled_t = NULL;
    free(fringes->x_coupled_t);
    fringes->x_coupled_t = NULL;
    free(fringes->contrast_t);
    fringes->contrast_t = NULL;
    free(fringes->I_max_t);
    fringes->I_max_t = NULL;
    fringes->n_output_t = 0;
    fringes->c_output_t = 0;
    break;
    case PTR_FRINGES_I_MAX_T:
    free(fringes->z_coupled_t);
    fringes->z_coupled_t = NULL;
    free(fringes->x_coupled_t);
    fringes->x_coupled_t = NULL;
    free(fringes->contrast_t);
    fringes->contrast_t = NULL;
    free(fringes->I_max_t);
    fringes->I_max_t = NULL;
    fringes->n_output_t = 0;
    fringes->c_output_t = 0;
    break;
    case PTR_FRINGES_RAW_PHI_0_T:
    free(fringes->raw_phi_0_t);
    fringes->raw_phi_0_t = NULL;
    free(fringes->raw_phi_1_t);
    fringes->raw_phi_1_t = NULL;
    free(fringes->corrected_phi_0_t);
    fringes->corrected_phi_0_t = NULL;
    free(fringes->phase_jump_t);
    fringes->phase_jump_t = NULL;
    free(fringes->roi_fit_size_t);
    fringes->roi_fit_size_t = NULL;
    free(fringes->nu0_t);
    fringes->nu0_t = NULL;
    fringes->n_phi_t = 0;
    fringes->c_phi_t = 0;
    break;
    case PTR_FRINGES_RAW_PHI_1_T:
    free(fringes->raw_phi_0_t);
    fringes->raw_phi_0_t = NULL;
    free(fringes->raw_phi_1_t);
    fringes->raw_phi_1_t = NULL;
    free(fringes->corrected_phi_0_t);
    fringes->corrected_phi_0_t = NULL;
    free(fringes->phase_jump_t);
    fringes->phase_jump_t = NULL;
    free(fringes->roi_fit_size_t);
    fringes->roi_fit_size_t = NULL;
    free(fringes->nu0_t);
    fringes->nu0_t = NULL;
    fringes->n_phi_t = 0;
    fringes->c_phi_t = 0;
    break;
    case PTR_FRINGES_CORRECTED_PHI_0_T:
    free(fringes->raw_phi_0_t);
    fringes->raw_phi_0_t = NULL;
    free(fringes->raw_phi_1_t);
    fringes->raw_phi_1_t = NULL;
    free(fringes->corrected_phi_0_t);
    fringes->corrected_phi_0_t = NULL;
    free(fringes->phase_jump_t);
    fringes->phase_jump_t = NULL;
    free(fringes->roi_fit_size_t);
    fringes->roi_fit_size_t = NULL;
    free(fringes->nu0_t);
    fringes->nu0_t = NULL;
    fringes->n_phi_t = 0;
    fringes->c_phi_t = 0;
    break;
    case PTR_FRINGES_NU0_T:
    free(fringes->raw_phi_0_t);
    fringes->raw_phi_0_t = NULL;
    free(fringes->raw_phi_1_t);
    fringes->raw_phi_1_t = NULL;
    free(fringes->corrected_phi_0_t);
    fringes->corrected_phi_0_t = NULL;
    free(fringes->phase_jump_t);
    fringes->phase_jump_t = NULL;
    free(fringes->roi_fit_size_t);
    fringes->roi_fit_size_t = NULL;
    free(fringes->nu0_t);
    fringes->nu0_t = NULL;
    fringes->n_phi_t = 0;
    fringes->c_phi_t = 0;
    break;
    case PTR_FRINGES_PHASE_JUMP_T:
    free(fringes->raw_phi_0_t);
    fringes->raw_phi_0_t = NULL;
    free(fringes->raw_phi_1_t);
    fringes->raw_phi_1_t = NULL;
    free(fringes->corrected_phi_0_t);
    fringes->corrected_phi_0_t = NULL;
    free(fringes->phase_jump_t);
    fringes->phase_jump_t = NULL;
    free(fringes->roi_fit_size_t);
    fringes->roi_fit_size_t = NULL;
    free(fringes->nu0_t);
    fringes->nu0_t = NULL;
    fringes->n_phi_t = 0;
    fringes->c_phi_t = 0;
    break;
    case PTR_FRINGES_WINDOWED_X_PROFILE:
    for (i=0;i<fringes->n_windowed_x_profile_buffer;i++)
    {
      free(fringes->windowed_x_profile[i]);
      fringes->windowed_x_profile[i] = NULL;
    }
    free(fringes->windowed_x_profile);
    fringes->windowed_x_profile = NULL;
    fringes->n_windowed_x_profile_buffer = 0;
    fringes->c_windowed_x_profile_buffer = 0;
    break;
    case PTR_FRINGES_RAW_X_PROFILE:
    for (i=0;i<fringes->n_raw_x_profile_buffer;i++)
    {
      free(fringes->raw_x_profile[i]);
      fringes->raw_x_profile[i] = NULL;
    }
    free(fringes->raw_x_profile);
    fringes->raw_x_profile = NULL;
    fringes->n_raw_x_profile_buffer = 0;
    fringes->c_raw_x_profile_buffer = 0;
    break;
    case PTR_FRINGES_X_WINDOW:
    for (i=0;i<fringes->n_x_window_buffer;i++)
    {
      free(fringes->x_window[i]);
      fringes->x_window[i] = NULL;
    }
    free(fringes->x_window);
    fringes->x_window = NULL;
    fringes->n_x_window_buffer = 0;
    fringes->c_x_window_buffer = 0;
    break;
    case PTR_FRINGES_PHASE:
    for (i=0;i<fringes->n_phase_buffer;i++)
    {
      free(fringes->phase[i]);
      fringes->phase[i] = NULL;
    }
    free(fringes->phase);
    fringes->phase = NULL;
    fringes->n_phase_buffer = 0;
    fringes->c_phase_buffer = 0;
    break;
    case PTR_FRINGES_AMPLITUDE:
    for (i=0;i<fringes->n_amplitude_buffer;i++)
    {
      free(fringes->amplitude[i]);
      fringes->amplitude[i] = NULL;
    }
    free(fringes->amplitude);
    fringes->amplitude = NULL;
    fringes->n_amplitude_buffer = 0;
    fringes->c_amplitude_buffer = 0;
    break;
    case PTR_FRINGES_UNWRAPPED_PHASE_ROI:
    for (i=0;i<fringes->n_unwrapped_phase_roi_buffer;i++)
    {
      free(fringes->unwrapped_phase_roi[i]);
      fringes->unwrapped_phase_roi[i] = NULL;
    }
    free(fringes->unwrapped_phase_roi);
    fringes->unwrapped_phase_roi = NULL;
    fringes->c_unwrapped_phase_roi_buffer = 0;
    fringes->n_unwrapped_phase_roi_buffer = 0;
    break;
    case PTR_FRINGES_RAW_PHASE_ROI:
    for (i=0;i<fringes->n_raw_phase_roi_buffer;i++)
    {
      free(fringes->raw_phase_roi[i]);
      fringes->raw_phase_roi[i] = NULL;
    }
    free(fringes->raw_phase_roi);
    fringes->raw_phase_roi = NULL;
    fringes->n_raw_phase_roi_buffer = 0;
    fringes->c_raw_phase_roi_buffer = 0;
    break;
    case PTR_FRINGES_SPECTRUM:
    for (i=0;i<fringes->n_spectrum_buffer;i++)
    {
      free(fringes->spectrum[i]);
      fringes->spectrum[i] = NULL;
    }
    fringes->spectrum = NULL;
    fringes->n_spectrum_buffer = 0;
    fringes->c_spectrum_buffer = 0;
    break;
    case PTR_FRINGES_SPECTRUM_LOG:
    for (i=0;i<fringes->n_spectrum_log_buffer;i++)
    {
      free(fringes->spectrum_log[i]);
      fringes->spectrum_log[i] = NULL;
    }
    free(fringes->spectrum_log);
    fringes->spectrum_log = NULL;
    fringes->n_spectrum_log_buffer = 0;
    fringes->c_spectrum_log_buffer = 0;
    break;
    case PTR_FRINGES_NU_ROI:
    for (i=0;i<fringes->n_nu_roi_buffer;i++)
    {
      free(fringes->nu_roi[i]);
      fringes->nu_roi[i] = NULL;
    }
    free(fringes->nu_roi);
    fringes->nu_roi = NULL;
    fringes->n_nu_roi_buffer = 0;
    fringes->c_nu_roi_buffer = 0;
    break;
  }

  return 0;
}
