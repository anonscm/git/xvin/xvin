#pragma once

#include "sdi_v2_fringes.h"
#include "sdi_v2_y_profile.h"
#include "xvin.h"

#define PTR_BEAD_Z_T 0
#define PTR_BEAD_Y_T 1
#define PTR_BEAD_X_T 2
#define PTR_BEAD_QUALITY_FACTOR_T 3

# define SDI_FLOAT 100

//to classify the beads
# define SDI_UNINITIALIZED_BEAD  0x80
# define SDI_WEIRD_BEAD  0x01
# define SDI_BAD_BEAD 0x02
# define SDI_WORKING_BEAD 0x04
# define SDI_NO_FUNCTION_BEAD 0x08
# define SDI_REFERENCE_BEAD 0x10
# define SDI_NOT_OPENING_BEAD 0x20
# define SDI_OPENING_BEAD_1 0x40

//struture that contains the informations about one beadtypedef struct sdi_bead_data
typedef struct sdi_version2_bead_data
{
  int xc, yc;
  int mouse_draged;
  int index_in_fov; //used (with updates when events) when bead is added to a fov
  float *z_t, *x_t, *y_t, *quality_factor_t;
  int c_output_t, n_output_t;
  float v_phi0_z;//sensitivity of the absolute phase variation along z (rad per micron)
  float v_phi1_z;//sensitivity of the envelop position variation along z (pix per micron)
  float z_offset, y_offset, x_offset; //constant offset to align signals
  float index_factor;//copy of the index factor of the parameter (for z)
  int bead_class;//tells whether it is a good or not, logical combination of the define coding bead's classification
  float quality_factor;
  int weird_count;
  bool hard_linked;
  bool recenter_along_y, recenter_along_x;

  int n_fringes_bead; //number of fringe patterns (usually 2 but can be 1, 4 if 4 blades/mirrors & 8 slits etc...)
  sdi_v2_fringes **fringes_bead;//the ptr onto the sdi_fringes objects for measureing x and z
  //depending on your device you can 1, 2 etc fringes patterns
  int n_y_profile_bead; //number of y profile objects
  sdi_v2_y_profile **y_profile_bead;//the ptr onto the sdi_y object for measuring y, can be one big y profile or 2 half

  //when one wants this bead to apply the proc1 (current suitable job)
  bool apply_proc1;
  d_s *ds_out1;//a ds that can contain a in/out data for an algo called during experiment (for example a long snapshot for 0 detection, correlation with zmag ..)
  d_s *ds_out2;//a second one
  d_s *ds_out3;//a third one
} sdi_v2_bead;

PXV_FUNC(int, sdi_v2_recenter_y_of_one_bead, (sdi_v2_bead *bead, sdi_v2_parameter *param, int y_threshold));
PXV_FUNC(int, sdi_v2_center_y_roi_bead, (sdi_v2_parameter *sdi_param, sdi_v2_bead *bead));
PXV_FUNC(int, sdi_v2_diagnose_one_bead, (sdi_v2_bead *bead, sdi_v2_diagnose *diagnose));

PXV_FUNC(int, sdi_v2_dummy_xz_bead, (sdi_v2_bead *bead));
PXV_FUNC(int, sdi_v2_dummy_y_bead, (sdi_v2_bead *bead));
PXV_FUNC(int, sdi_v2_reset_data_of_bead, (sdi_v2_bead *bead));
PXV_FUNC(int, sdi_v2_center_x_roi_bead, (sdi_v2_parameter *sdi_param, sdi_v2_bead *bead));
PXV_FUNC(int, sdi_v2_build_image_with_2d_fringes_buffer, (sdi_v2_bead *bead, sdi_v2_parameter *param, O_i *oi));

PXV_FUNC(int, sdi_v2_update_bead_parameters, (sdi_v2_bead *bead, sdi_v2_parameter *param, int xc, int yc));
PXV_FUNC(int, sdi_v2_set_calibration_of_bead, (sdi_v2_bead *bead, float v_00, float v_10, float nu00, float v_01,
                                                float v_11, float nu01, float v_x0, float v_x1));
PXV_FUNC(int, sdi_v2_update_c_profile_bead_points, (sdi_v2_bead *bead, int ci));


PXV_FUNC(int, sdi_v2_compute_z_bead, (sdi_v2_bead *bead));
PXV_FUNC(int, sdi_v2_compute_x_bead, (sdi_v2_bead *bead));
PXV_FUNC(int, sdi_v2_compute_y_bead, (sdi_v2_bead *bead));

PXV_FUNC(int, sdi_v2_remove_one_sdi_bead, (sdi_v2_bead *bead));
PXV_FUNC(int, sdi_v2_switch_ptr_to_sdi_bead_1d_float_field, (sdi_v2_bead *bead, int which_data, float **field, float **ext_float, int n_ext_float));
PXV_FUNC(int, sdi_v2_allocate_sdi_v2_bead_output_t_fields, (sdi_v2_bead *bead, int size));
PXV_FUNC(int, sdi_v2_allocate_sdi_v2_bead_1d_float_fields, (sdi_v2_bead *bead, int buffer_size, int which_data));
PXV_FUNC(int, sdi_v2_init_bead_by_assembly, (sdi_v2_bead *bead, sdi_v2_parameter *sdi_param, sdi_v2_fringes *fringes1, sdi_v2_fringes *fringes2,
  sdi_v2_y_profile *y_profile1, sdi_v2_y_profile *y_profile2));

//PXV_FUNC(int, diagnose_bead, (sdi_bead *bead, sdi_diagnose *diagnose));
//PXV_FUNC(int, allocate_sdi_bead_t_movie, (sdi_bead *bead, int buffer_size));
/*
PXV_FUNC(int, center_x_roi_bead, (sdi_parameter *sdi_param, sdi_bead *bead));
PXV_FUNC(int, display_one_bead_data, (sdi_bead *bead));
*/
