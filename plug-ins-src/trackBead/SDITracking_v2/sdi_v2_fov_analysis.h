#pragma once

#include "sdi_v2_bead.h"
#include "sdi_v2_movie.h"
#include "sdi_v2_graphic.h"

#define SDI_ZMAG_MEASURED 0
#define SDI_ZMAG_COMMAND 1
#define SDI_ZOBJECTIVE_MEASURED 2

//parameters for fringes detection & filtering
typedef struct sdi_v2_fringes_detection
{
  int guess_dx; //rough expected half minimal distance between fringes (pixel)
  int guess_dy; //rough expected half minimal distance between y profiles (pixel unit)
  int intensity_threshold_detect; //threshold for intensity (int, acting on pixel)
  float intensity_min_filter; //to filter fringes with intensity threshold
  float intensity_max_filter;
  float contrast_threshold_filter; //
  int verbose_type;
} sdi_v2_f_d;

//parameters for fringes detection & filtering
typedef struct sdi_v2_y_bead_detection
{
  float correlation_min;
  int dist_y_min;
  int dist_y_max;
  int dist_x_max;
  int verbose;
  int replace;
  pltreg *plr;
} sdi_v2_y_b_d;

typedef struct sdi_v2_fov_data
{
  int current_index, current_time;//the foveral clock, in index and in real time, of the experiment
  int n_fringes, n_y_profile, n_display, n_beads;//n_proc;
  int buffer_size;
  int x_fov, y_fov;//coordinate of the fov (for scanning)
  int n_opening_hairpins, n_ref_beads;//for comparison with other fovs & scanning
  sdi_v2_parameter *sdi_param;
  sdi_v2_fringes **fringes_list;
  sdi_v2_disp **display_list;
  sdi_v2_y_profile **y_profile_list;
  sdi_v2_diagnose *diagnose;
  sdi_v2_bead **beads_list;
} sdi_v2_fov;

extern sdi_v2_fov *sdi_g_fov;

PXV_FUNC(int, sdi_v2_buffer_config, (sdi_v2_fov *fov, int length));
PXV_FUNC(int, sdi_v2_remove_all_data_of_beads, (sdi_v2_fov *fov));
PXV_FUNC(int, sdi_v2_remove_all_display_of_one_sdi_bead, (sdi_v2_fov *fov, int number));
PXV_FUNC(int, sdi_v2_remove_all_intermediate_signal_of_y_profiles, (sdi_v2_fov *fov));
PXV_FUNC(int, sdi_v2_remove_all_intermediate_signal_of_fringes, (sdi_v2_fov *fov));

PXV_FUNC(int, sdi_v2_fov_calibration_spatial_effect, (sdi_v2_fov *fov, pltreg **plr, int replace, float *z));
PXV_FUNC(int, sdi_v2_calibration_of_fringes_fov, (sdi_v2_fov *fov, O_i *oi, float *z));
PXV_FUNC(int, sdi_v2_fov_linearity_check, (sdi_v2_fov *fov, float *z, pltreg **plr, int replace));
//analysis of signal during time
PXV_FUNC(int, sdi_v2_classify_beads_dsout, (sdi_v2_fov *fov, d_s *zmag, int verbose, int replace, pltreg **plr));
PXV_FUNC(int, sdi_v2_compute_0_of_beads_dsout, (sdi_v2_fov *fov, int verbose, int replace, pltreg **plr));
PXV_FUNC(int, sdi_v2_compute_std_of_beads_dsout, (sdi_v2_fov *fov, int verbose, int replace, pltreg **plr));

//gui for bead detection parameters
PXV_FUNC(int, sdi_v2_y_bead_detection_gui, (sdi_v2_y_b_d *sdi_y_b_d, int verbose, bool from_cfg));
PXV_FUNC(int, sdi_v2_gather_y_profiles_into_bead_with_noise_correlation, (sdi_v2_fov *fov, sdi_v2_y_b_d *sdi_y_b_d, pltreg **plr));
PXV_FUNC(int, sdi_v2_compute_sdi_y_profile_correlation_of_fluctuation_ds1out, (sdi_v2_y_profile *profile1, sdi_v2_y_profile *profile2, float *corr_product));

//detection of fringes patterns on frames
PXV_FUNC(int, sdi_v2_fringes_detection_gui, (sdi_v2_f_d *sdi_f_d));
PXV_FUNC(int, sdi_v2_detect_fringes, (O_i *oi, sdi_v2_fov *fov, sdi_v2_f_d *sdi_f_d));
PXV_FUNC(int, sdi_v2_filter_bad_fringes, (sdi_v2_fov *fov, sdi_v2_f_d *sdi_f_d));

////////////////////functions to track or calibrate on recorded movie
PXV_FUNC(int, sdi_v2_track_bead_movie_fov, (sdi_v2_fov *fov, O_i *oi));
PXV_FUNC(int, sdi_v2_track_fringes_fov, (sdi_v2_fov *fov, O_i *oi));
PXV_FUNC(int, sdi_v2_track_y_profile_fov_movie, (sdi_v2_fov *fov, O_i *oi));
PXV_FUNC(int, sdi_v2_take_backgrounds, (sdi_v2_fov *fov));
PXV_FUNC(int, sdi_set_common_calibration_factors, (sdi_v2_fov *fov, int verbose, bool from_cfg));

PXV_FUNC(int, sdi_v2_clean_fov_from_bad_beads, (sdi_v2_fov *fov));
PXV_FUNC(int, sdi_v2_clean_fov_from_fringes_not_in_beads, (sdi_v2_fov *fov));
PXV_FUNC(int, sdi_v2_clean_fov_from_y_profile_not_in_beads, (sdi_v2_fov *fov));
PXV_FUNC(int, sdi_v2_center_roi_y_fringes, (O_i *oi, sdi_v2_parameter *sdi_param, sdi_v2_fringes *fringes));
PXV_FUNC(int, sdi_v2_center_roi_x_fringes, (O_i *oi, sdi_v2_parameter *sdi_param, sdi_v2_fringes *fringes));
PXV_FUNC(int, sdi_v2_create_y_profile_associated_with_fringes, (sdi_v2_fov *fov));
PXV_FUNC(int, sdi_v2_gather_fringes_into_bead_with_calibration_factors, (sdi_v2_fov *fov));


////allocation, remove, parameters for tracking etc
PXV_FUNC(int, sdi_v2_empty_fov, (sdi_v2_fov *fov));
PXV_FUNC(int, sdi_v2_add_current_point_as_ref_points_fov, (sdi_v2_fov *fov));
PXV_FUNC(int, sdi_v2_add_one_bead_to_fov_with_coordinates, (sdi_v2_fov *fov, int xc, int yc));
PXV_FUNC(int, sdi_v2_add_one_bead_to_fov_with_mouse, (sdi_v2_fov *fov));
PXV_FUNC(int, sdi_v2_add_one_bead_to_fov, (sdi_v2_fov *fov, sdi_v2_fringes *fringes1, sdi_v2_fringes *fringes2,
                                                          sdi_v2_y_profile *y_profile1, sdi_v2_y_profile *y_profile2));
PXV_FUNC(int, sdi_v2_update_all_sdi_y_profile_parameters, (sdi_v2_fov *fov, sdi_v2_parameter *param));
PXV_FUNC(int, sdi_v2_update_all_sdi_fringes_parameters, (sdi_v2_fov *fov, sdi_v2_parameter *param));
PXV_FUNC(int, sdi_v2_add_one_sdi_y_profile_to_fov_by_position, (sdi_v2_fov *fov, int xc, int yc));
PXV_FUNC(int, sdi_v2_add_one_sdi_fringes_to_fov_by_position, (sdi_v2_fov *fov, int xc, int yx));
PXV_FUNC(int, sdi_v2_init_one_fov, (sdi_v2_fov *fov));

///////////////////////////////graphical display of fringes data//////////////////
PXV_FUNC(int, sdi_v2_display_fringes_algo_real, (sdi_v2_fov *fov, sdi_v2_fringes *fringes, pltreg *plr, O_p *op));
PXV_FUNC(int, sdi_v2_display_fringes_algo_fourier, (sdi_v2_fov *fov, sdi_v2_fringes *fringes, pltreg *plr, O_p *op));
PXV_FUNC(int, sdi_v2_display_fringes_algo_raw_phase, (sdi_v2_fov *fov, sdi_v2_fringes *fringes, pltreg *plr, O_p *op, int size));
PXV_FUNC(int, sdi_v2_display_fringes_algo_z_result, (sdi_v2_fov *fov, sdi_v2_fringes *fringes, pltreg *plr, O_p *op, int size));
PXV_FUNC(int, sdi_v2_display_fringes_algo_contrast, (sdi_v2_fov *fov, sdi_v2_fringes *fringes, pltreg *plr, O_p *op, int size));
PXV_FUNC(int, sdi_v2_display_fringes_algo_Imax, (sdi_v2_fov *fov, sdi_v2_fringes *fringes, pltreg *plr, O_p *op, int size));
PXV_FUNC(int, sdi_v2_display_fringes_all_steps, (sdi_v2_fov *fov, sdi_v2_fringes *fringes, pltreg *plr, O_p *op, int size));
PXV_FUNC(int, sdi_v2_display_2_fringes_all_steps, (sdi_v2_fov *fov, sdi_v2_fringes *fringes1, sdi_v2_fringes *fringes2, pltreg *plr, O_p *op, int size));

//graphical display of y_profile data
PXV_FUNC(int, sdi_v2_set_y_profile_fourier_algo_display2, (sdi_v2_fov *fov, sdi_v2_y_profile *profile, pltreg *plr, O_p *op, int size));
PXV_FUNC(int, sdi_v2_set_y_profile_fourier_algo_display1, (sdi_v2_fov *fov, sdi_v2_y_profile *profile, pltreg *plr, O_p *op));

////////////////////////graphic for beads
PXV_FUNC(int, sdi_v2_display_bead_quality_factor, (sdi_v2_fov *fov, sdi_v2_bead *bead, pltreg *plr_parent, O_p *op_parent, int size, float t0));
PXV_FUNC(int, sdi_v2_display_bead_z, (sdi_v2_fov *fov, sdi_v2_bead *bead, pltreg *plr_parent, O_p *op_parent, int size, float t0));
PXV_FUNC(int, sdi_v2_display_bead_y, (sdi_v2_fov *fov, sdi_v2_bead *bead, pltreg *plr_parent, O_p *op_parent, int size, float t0));
PXV_FUNC(int, sdi_v2_display_bead_x, (sdi_v2_fov *fov, sdi_v2_bead *bead, pltreg *plr_parent, O_p *op_parent, int size, float t0));

PXV_FUNC(int, sdi_v2_draw_shape_of_bead, (BITMAP *imb, O_i *oi, int xc, int yc, sdi_v2_parameter *param));
PXV_FUNC(int, sdi_v2_label_fringes_of_fov, (BITMAP *imb, O_i *oi, sdi_v2_fov *fov));
PXV_FUNC(int, sdi_v2_label_one_fringes, (BITMAP *imb, O_i *oi, sdi_v2_fringes *fringes, sdi_v2_parameter *param));
PXV_FUNC(int, sdi_v2_draw_one_fringes, (BITMAP *imb, O_i *oi, sdi_v2_fringes *fringes, sdi_v2_parameter *param));
PXV_FUNC(int, sdi_v2_draw_fringes_of_current_fov, (BITMAP *imb, O_i *oi, sdi_v2_fov *fov));
PXV_FUNC(int, sdi_v2_draw_one_bead, (BITMAP *imb, O_i *oi, sdi_v2_bead *bead, sdi_v2_parameter *param));
PXV_FUNC(int, sdi_v2_draw_beads_of_current_fov, (BITMAP *imb, O_i *oi, sdi_v2_fov *fov));
PXV_FUNC(int, sdi_v2_add_one_display_to_fov, (sdi_v2_fov *fov));
PXV_FUNC(int, sdi_v2_label_beads_of_fov, (BITMAP *imb, O_i *oi, sdi_v2_fov *fov));
PXV_FUNC(int, sdi_v2_label_one_bead, (BITMAP *imb, O_i *oi, sdi_v2_bead *bead, sdi_v2_parameter *param));
//////////////////////graphical display of fov analysis///////////////////////////
////////to quickly examine the calibration factor spatial effects (to finalize the assembly)////////////
///////or others

//these are to find your stuff !
PXV_FUNC(int, sdi_v2_find_disp_linked_to_float, (sdi_v2_fov *fov, float *y_in, sdi_v2_disp **disp_out, int *index));
PXV_FUNC(int, sdi_v2_find_index_bead_in_fov, (sdi_v2_fov *fov, sdi_v2_bead *bead, int *index));
PXV_FUNC(int, sdi_v2_find_index_fringes_in_fov, (sdi_v2_fov *fov, sdi_v2_fringes *fringes, int *index));
PXV_FUNC(int, sdi_v2_find_index_y_profile_in_fov, (sdi_v2_fov *fov, sdi_v2_y_profile *y_profile, int *index));

//these are to remove objects
PXV_FUNC(int, sdi_v2_remove_all_display_of_fov, (sdi_v2_fov *fov));
PXV_FUNC(int, sdi_v2_remove_one_sdi_display_from_fov, (sdi_v2_fov *fov, int number));
PXV_FUNC(int, sdi_v2_remove_all_display_of_one_sdi_fringes_from_fov, (sdi_v2_fov *fov, int number));
PXV_FUNC(int, sdi_v2_remove_one_sdi_bead_from_fov, (sdi_v2_fov *fov, int number));
PXV_FUNC(int, sdi_v2_remove_one_sdi_y_profile_from_fov, (sdi_v2_fov *fov, int number));
PXV_FUNC(int, sdi_v2_remove_all_display_of_one_sdi_y_profile_from_fov, (sdi_v2_fov *fov, int number));
PXV_FUNC(int, sdi_v2_remove_one_sdi_fringes_from_fov, (sdi_v2_fov *fov, int number));
