#pragma once

#include "xvin.h"
PXV_FUNC(int, sdi_v2_extract_char_submovie_from_char_movie, (imreg * imr, O_i *submovie, O_i *movie, int period, int offset, int subset_length, int number));
PXV_FUNC(int, sdi_v2_oi_char_find_isolated_maxima, (O_i *oi, int *x_list, int *y_list, int *number, int threshold, int dx, int dy));
PXV_FUNC(int, sdi_v2_oi_char_find_all_maxima, (O_i *oi, int *x_list, int *y_list, int *count, int threshold, int dx));
PXV_FUNC(int, sdi_v2_filter_close_maxima, (int *x_list_tmp, int *y_list_tmp, int count_tmp, int *x_list, int *y_list, int *count, int dx, int dy));

/*
//PXV_FUNC(int, switch_frame_without_refresh, (O_i *oi, int n));
PXV_FUNC(int, oi_zoom, (int x, int y, imreg *imr, O_i *oi, float zoom_factor));
PXV_FUNC(int, construct_pattern_of_movies, (imreg *imr));
*/
