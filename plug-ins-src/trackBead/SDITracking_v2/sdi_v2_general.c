#include "../timer_thread.h"
#include "sdi_v2_general.h"
#include "../trackBead.h"
#include "../track_util.h"
#include "../record.h"//en gros, pour ces deux include, je suis obligé de les mettre là, ds le h ca marche pas
#include "../zdet.h"
#include "../action.h"
#include <hdf5/serial/hdf5.h>
//contains the interface between track_util, trackbead & sdi

//sdi variables
extern sdi_v2_parameter *sdi_g_param = NULL;
extern sdi_v2_diagnose *sdi_g_diagnose = NULL;
extern sdi_v2_fov *sdi_g_fov = NULL;
extern bool go_rt_track_xz = false;
extern bool go_rt_track_y = false;
extern bool go_rt_track_bead = false;
extern float dt_computation_ms_xz = 0;
extern float dt_computation_ms_y = 0;
sdi_v2_p_info *g_info = NULL;
sdi_v2_p_info *g_info_help = NULL;

bool sdi_par_from_cfg = false;//tells whether there is config files for sdi
bool sdi_diagnose_from_cfg = false;
bool sdi_calib_from_cfg = false;
bool sdi_y_b_from_cfg = false;
bool sdi_y_y_from_cfg = false;
bool sdi_xyz_bead_from_cfg = false;
//those comes from track_util & trackbead
extern int mouse_selected_bead;
extern O_i  *oi_TRACK;
extern g_track *track_info;
extern g_record *working_gen_record;

//for detection of beads i want to see fringes before beads
bool draw_beads = true;
bool draw_fringes = false;
extern pltreg **available_pltreg = NULL;
extern int n_available_pltreg = 0;
//tmp stuff
pltreg *plot_group_1 = NULL;
pltreg *plot_group_2 = NULL;

//different jobs that are regularly used : detection of beads, check linearity...
sdi_v2_oi_analysis *sdi_oi_a = NULL;
sdi_v2_y_y *sdi_y_y = NULL; //parameters for the job associated with y profiles objects
sdi_v2_f_d *sdi_f_d = NULL;//parameters to detect the fringes
sdi_v2_y_b_d *sdi_y_b_d = NULL;//parameters to detect beads by y profile correlation
sdi_v2_xyz_b *sdi_xyz_b = NULL;//one job on xyz of beads
sdi_v2_ramp *sdi_ramp = NULL; //ramp
extern sdi_v2_grab *sdi_grab = NULL; //grab frames during the movements of something : obj, zmag, etc. no average!


//hdf5 recording
#define FILE "F:\\Thibault\\dset.h5"
bool record_hdf5 = false;
hdf5_gen *hdf5_manager = NULL;
//////////////////////////////////////////////////////////////
/////////////////hdf5 saving
int do_save_beads_to_cfg_file(void)
{
  if (updating_menu_state!=0) return D_O_K;
  if (sdi_g_fov==NULL) return D_O_K;
  if (sdi_g_fov->n_beads == 0) return D_O_K;
  int j;
  char bead_name[16];
  bool go_rt_track_tmp_xz, go_rt_track_tmp_y, go_rt_track_tmp_bead;
  //puts tracking on pause, maybe useless but..
  /*go_rt_track_tmp_xz = go_rt_track_xz;
  go_rt_track_xz = false;
  go_rt_track_tmp_y = go_rt_track_y;
  go_rt_track_y = false;
  go_rt_track_tmp_bead = go_rt_track_bead;
  go_rt_track_bead = false;*/
  set_config_int("SDI-BEADS", "beads_n", sdi_g_fov->n_beads);
  for (j=0;j<sdi_g_fov->n_beads;j++)
  {
    snprintf(bead_name,16,"bead_%d_x",j);
    set_config_int("SDI-BEADS", bead_name, sdi_g_fov->beads_list[j]->xc);
    snprintf(bead_name,16,"bead_%d_y",j);
    set_config_int("SDI-BEADS", bead_name, sdi_g_fov->beads_list[j]->yc);
  }
  /*go_rt_track_xz = go_rt_track_tmp_xz;
  go_rt_track_y = go_rt_track_tmp_y;
  go_rt_track_bead = go_rt_track_tmp_bead;*/
  return D_O_K;
}

int do_load_beads_from_cfg_file(void)
{
  if (updating_menu_state!=0) return D_O_K;
  int n_bead=0;
  int i, j;
  char bead_name[16];
  int nf = TRACKING_BUFFER_SIZE;
  bool go_rt_track_tmp_xz, go_rt_track_tmp_y, go_rt_track_tmp_bead;
  int xc, yc;
  //puts tracking on pause, maybe useless but..
  go_rt_track_tmp_xz = go_rt_track_xz;
  go_rt_track_xz = false;
  go_rt_track_tmp_y = go_rt_track_y;
  go_rt_track_y = false;
  go_rt_track_tmp_bead = go_rt_track_bead;
  go_rt_track_bead = false;
  n_bead = get_config_int("SDI-BEADS", "beads_n", 0);
  for (j=0;j<n_bead;j++)
  {
    snprintf(bead_name,16,"bead_%d_x",j);
    xc=get_config_int("SDI-BEADS", bead_name,200);
    snprintf(bead_name,16,"bead_%d_y",j);
    yc=get_config_int("SDI-BEADS", bead_name,200);
    sdi_v2_add_one_bead_to_fov_with_coordinates(sdi_g_fov, xc, yc);
    //when not directly to b_t needs for allocation
    sdi_v2_allocate_sdi_v2_bead_output_t_fields( sdi_g_fov->beads_list[sdi_g_fov->n_beads-1], nf);//default size of output
    sdi_set_common_calibration_factors(sdi_g_fov, 0, sdi_calib_from_cfg); //puts calibration factors, not verbose
    for (i=0 ;i<2 ;i++) sdi_g_fov->beads_list[sdi_g_fov->n_beads-1]->fringes_bead[i]->need_background = true;//ask to ref point asap the tracking restart
    for (i=0 ;i<2 ;i++) sdi_g_fov->beads_list[sdi_g_fov->n_beads-1]->y_profile_bead[i]->need_background = true;//ask to ref point asap the tracking restart
    for (i=0 ;i<2 ;i++) sdi_g_fov->beads_list[sdi_g_fov->n_beads-1]->fringes_bead[i]->need_reference_point = true;//ask to ref point asap the tracking restart
  }
  go_rt_track_xz = go_rt_track_tmp_xz;
  go_rt_track_y = go_rt_track_tmp_y;
  go_rt_track_bead = go_rt_track_tmp_bead;
  return D_O_K;
}


//if asked, it opens the file and returns file_id. also the file can be closed or not
//if it is just to dump one array, init with total size and call update function only once with i=0
int update_hdf5_1d(hdf5_1d *hdf5_1d_in, int i, bool open_file, hid_t *file_id, bool close_file)
{
  //if ( i%hdf5_1d_in->write_period !=0 ) return -1;//nothing to do, je laisse tomber ça pour moment
  bool trigger=false;
  static int count_debug=0;
  if ( hdf5_1d_in->offset_file[0] == 0)
  {
    trigger = true;
    hdf5_1d_in->t0 = i;
    my_set_window_title("bon t0 %d i %d offset %d file id %d", hdf5_1d_in->t0, i,  hdf5_1d_in->offset_file[0], (int)(*file_id));
  }
  else
  {
    if ( (i>= (hdf5_1d_in->t0 + hdf5_1d_in->offset_file[0])) && (i<( hdf5_1d_in->t0 + hdf5_1d_in->offset_file[0] +  hdf5_1d_in->chunk_dims[0]) ) )
    {
        my_set_window_title("bon t0 %d i %d offset %d file id %d", hdf5_1d_in->t0, i,  hdf5_1d_in->offset_file[0], (int)(*file_id));
      trigger = true;
    }
  }
  if (trigger == false) return -1;
  hid_t memspace, dataspace, dataset, group_id, subgroup_id;
  herr_t status;
  if (open_file == true)
  {
    if ( (*file_id = H5Fopen (hdf5_1d_in->filename, H5F_ACC_RDWR, H5P_DEFAULT)) < 0) return 1;
    else hdf5_1d_in->file = *file_id;
  }
  else
  {
    if (*file_id < 0) {return 2;}
    else hdf5_1d_in->file = *file_id;
  } //means the given file_id is not valid
  if (hdf5_1d_in->in_group == true)
  {
    if ( (group_id = H5Gopen2(*file_id, hdf5_1d_in->groupname, H5P_DEFAULT)) < 0 ){ return 3;}
    if (hdf5_1d_in->in_subgroup == true)
    {
      if ( (subgroup_id = H5Gopen2(group_id, hdf5_1d_in->subgroupname, H5P_DEFAULT)) < 0 ){ return 6;}
      if ( (dataset = H5Dopen2 (subgroup_id, hdf5_1d_in->datasetname, H5P_DEFAULT)) < 0 ){return 4;}
    }
    else
    {
      if ( (dataset = H5Dopen2 (group_id, hdf5_1d_in->datasetname, H5P_DEFAULT)) < 0 ){return 5;}
    }
  }
  else {if ( (dataset = H5Dopen2 (*file_id, hdf5_1d_in->datasetname, H5P_DEFAULT)) < 0 ) return 7;}
  if ( (status = H5Dset_extent (dataset, hdf5_1d_in->file_dims)) < 0) return 8;
  if ( (dataspace = H5Dget_space (dataset)) < 0) return 9;
  if ( (status = H5Sselect_hyperslab(dataspace, H5S_SELECT_SET, hdf5_1d_in->offset_file, NULL, hdf5_1d_in-> chunk_dims, NULL)) < 0)  return 10;
  memspace = H5Screate_simple (1, hdf5_1d_in->chunk_dims, NULL);

  switch (hdf5_1d_in->datatype)
  {
    case PTR_DATATYPE_FLOAT:
    if ( (status = H5Dwrite (dataset, H5T_NATIVE_FLOAT, memspace, dataspace, H5P_DEFAULT, hdf5_1d_in->data.fdata + hdf5_1d_in->mem_offset )) < 0) return 11;
    break;
    case PTR_DATATYPE_INT:
    if ( (status = H5Dwrite (dataset, H5T_NATIVE_INT, memspace, dataspace, H5P_DEFAULT, hdf5_1d_in->data.idata + hdf5_1d_in->mem_offset )) < 0) return 12;
    break;
    case PTR_DATATYPE_LLONG:
    if ( (status = H5Dwrite (dataset, H5T_NATIVE_LLONG, memspace, dataspace, H5P_DEFAULT, hdf5_1d_in->data.lldata + hdf5_1d_in->mem_offset )) < 0) return 13;
    break;
  }

  if ( (status = H5Dclose (dataset)) < 0) return 14;
  if ( (status = H5Sclose(memspace)) < 0) return 15;
  if ( (status = H5Sclose(dataspace)) < 0) return 16;
  if (hdf5_1d_in->in_subgroup == true) {if ( (status = H5Gclose (subgroup_id)) < 0 ) return 17;}
  if (hdf5_1d_in->in_group == true) {if ( (status = H5Gclose (group_id)) < 0 ) return 18;}
  static count =0;
  if (close_file == true)
  {
    if ( (status = H5Fclose(*file_id)) < 0) return 19;
    *file_id = -1;
    count++;
    my_set_window_title("close file %d no %d", status, count);
  }
  //my_set_window_title("count %d mem offset%d  file dim %d file offset %d",i, hdf5_1d_in->mem_offset, hdf5_1d_in->file_dims[0], hdf5_1d_in->offset_file[0] );
  hdf5_1d_in->mem_offset = (int) (( hdf5_1d_in->mem_offset == 0) ? hdf5_1d_in->chunk_dims[0] : 0);
  hdf5_1d_in->file_dims[0] += hdf5_1d_in->chunk_dims[0];
  hdf5_1d_in->offset_file[0] += hdf5_1d_in->chunk_dims[0];

  return 0;
}
int attach_1d_float_to_hdf5_1d(hdf5_1d *hdf5_1d_in, float *fdata)
{
  hdf5_1d_in->data.fdata = fdata;
  return 0;
}
int attach_1d_int_to_hdf5_1d(hdf5_1d *hdf5_1d_in, int *idata)
{
  hdf5_1d_in->data.idata = idata;
  return 0;
}
int attach_1d_llong_to_hdf5_1d(hdf5_1d *hdf5_1d_in, long long *lldata)
{
  hdf5_1d_in->data.lldata = lldata;
  return 0;
}
int init_hdf5_1d(hdf5_1d *hdf5_1d_in, char filename[], char datasetname[], char groupname[], char subgroupname[], int datatype, int chunk_length)
{
  hid_t file_id, status, group_id, subgroup_id, dataspace, dataset, prop, parent_id;
  hsize_t dims[1] = {chunk_length};
  hsize_t maxdims[1] = {H5S_UNLIMITED};
  hsize_t chunk_dims[1] = {chunk_length};
  snprintf(hdf5_1d_in->datasetname, 128, datasetname);
  snprintf(hdf5_1d_in->filename, 128, filename);
  hdf5_1d_in->chunk_dims[0] = chunk_length ;
  hdf5_1d_in->file_dims[0] = chunk_length;
  hdf5_1d_in->offset_file[0] = 0;
  hdf5_1d_in->write_period = chunk_length;
  hdf5_1d_in->mem_offset = 0;
  hdf5_1d_in->datatype = datatype;
  hdf5_1d_in->in_group = false;
  hdf5_1d_in->in_subgroup = false;
  if ( (file_id = H5Fopen (filename, H5F_ACC_RDWR, H5P_DEFAULT)) < 0) return 1;
  if ( (dataspace = H5Screate_simple (1, dims, maxdims)) < 0) return 2;
  if ( (prop = H5Pcreate (H5P_DATASET_CREATE)) < 0) return 3;
  if ( (status = H5Pset_chunk (prop, 1, chunk_dims)) < 0) return 4;
  if (groupname == NULL)
  {
    hdf5_1d_in->in_group = false;
    parent_id = file_id;
  }
  else
  {
    snprintf(hdf5_1d_in->groupname, 128, groupname);
    hdf5_1d_in->in_group = true;
    //if group already exists or not
    if ( (group_id = H5Gopen2(file_id, groupname, H5P_DEFAULT)) < 0 )
    {
      group_id = H5Gcreate2(file_id, groupname, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    }
    if (subgroupname != NULL)
    {
      hdf5_1d_in->in_subgroup = true;
      snprintf(hdf5_1d_in->subgroupname, 128, subgroupname);
      if ( (subgroup_id = H5Gopen2(group_id, subgroupname, H5P_DEFAULT)) < 0 )
      {
        subgroup_id = H5Gcreate2(group_id, subgroupname, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
      }
      parent_id = subgroup_id;
    }
    else parent_id = group_id;
  }
  switch (datatype)
  {
    case PTR_DATATYPE_INT:
    dataset = H5Dcreate2 (parent_id, datasetname, H5T_NATIVE_INT, dataspace, H5P_DEFAULT, prop, H5P_DEFAULT);
    break;
    case PTR_DATATYPE_FLOAT:
    dataset = H5Dcreate2 (parent_id, datasetname, H5T_NATIVE_FLOAT, dataspace, H5P_DEFAULT, prop, H5P_DEFAULT);
    break;
    case PTR_DATATYPE_LLONG:
    dataset = H5Dcreate2 (parent_id, datasetname, H5T_NATIVE_LLONG, dataspace, H5P_DEFAULT, prop, H5P_DEFAULT);
    break;
  }
  status = H5Dclose (dataset);
  if (hdf5_1d_in->in_subgroup == true) status = H5Gclose(subgroup_id);
  if (hdf5_1d_in->in_group == true) status = H5Gclose (group_id);
  status = H5Pclose (prop);
  status = H5Sclose (dataspace);
  status = H5Fclose (file_id);
  return 0;
}
//to add :zmag, zmag_cmd,  (float), imi (int), action_status (int)
int remove_one_1d_hdf5_from_manager(int which_one)
{
  if (hdf5_manager == NULL) return D_O_K;
  if (hdf5_manager->n_hdf5_1d == 0) return D_O_K;
  if (which_one >= hdf5_manager->n_hdf5_1d) return D_O_K;
  int i;
  free(hdf5_manager->hdf5_1d_list[which_one]->datasetname);
  free(hdf5_manager->hdf5_1d_list[which_one]->filename);
  free(hdf5_manager->hdf5_1d_list[which_one]->groupname);
  free(hdf5_manager->hdf5_1d_list[which_one]);
  hdf5_manager->n_hdf5_1d--;
  for (i=which_one;i<hdf5_manager->n_hdf5_1d;i++) hdf5_manager->hdf5_1d_list[i] = hdf5_manager->hdf5_1d_list[i+1];
  return 0;
}

int add_one_1d_hdf5_to_manager(void)
{
  if (hdf5_manager->n_hdf5_1d==0)
  {
    if (hdf5_manager->hdf5_1d_list == NULL) hdf5_manager->hdf5_1d_list = (hdf5_1d **)calloc(1, sizeof(hdf5_1d *));
    hdf5_manager->hdf5_1d_list[0] = (hdf5_1d *)calloc(1, sizeof(hdf5_1d));
    hdf5_manager->n_hdf5_1d = 1;
  }
  else
  {
    hdf5_manager->hdf5_1d_list = (hdf5_1d **)realloc(hdf5_manager->hdf5_1d_list, (hdf5_manager->n_hdf5_1d + 1)*sizeof(hdf5_1d *));
    hdf5_manager->hdf5_1d_list[hdf5_manager->n_hdf5_1d] = (hdf5_1d *)calloc(1, sizeof(hdf5_1d));
    hdf5_manager->n_hdf5_1d ++;
  }
  return 0;
}
//un peu tarabistcoté, version d'essai
int link_track_info_to_hdf5(void)
{
  if (track_info==NULL) return D_O_K;
  //link time info : imi,
  add_one_1d_hdf5_to_manager();
  init_hdf5_1d(hdf5_manager->hdf5_1d_list[hdf5_manager->n_hdf5_1d-1], hdf5_manager->filename, "imi", "periph", NULL, PTR_DATATYPE_INT, (int) (TRACKING_BUFFER_SIZE / 2) );
  attach_1d_int_to_hdf5_1d(hdf5_manager->hdf5_1d_list[hdf5_manager->n_hdf5_1d-1], track_info->imi);
  add_one_1d_hdf5_to_manager();
  init_hdf5_1d(hdf5_manager->hdf5_1d_list[hdf5_manager->n_hdf5_1d-1], hdf5_manager->filename, "imt", "periph", NULL, PTR_DATATYPE_LLONG, (int) (TRACKING_BUFFER_SIZE / 2) );
  attach_1d_llong_to_hdf5_1d(hdf5_manager->hdf5_1d_list[hdf5_manager->n_hdf5_1d-1], track_info->imt);

  //link status_flag : info about periph
  //link zmag
  add_one_1d_hdf5_to_manager();
  init_hdf5_1d(hdf5_manager->hdf5_1d_list[hdf5_manager->n_hdf5_1d-1], hdf5_manager->filename, "zmag_cmd", "periph", NULL, PTR_DATATYPE_FLOAT, (int) (TRACKING_BUFFER_SIZE / 2) );
  attach_1d_float_to_hdf5_1d(hdf5_manager->hdf5_1d_list[hdf5_manager->n_hdf5_1d-1], track_info->zmag_cmd);
  add_one_1d_hdf5_to_manager();
  init_hdf5_1d(hdf5_manager->hdf5_1d_list[hdf5_manager->n_hdf5_1d-1], hdf5_manager->filename, "zmag", "periph", NULL, PTR_DATATYPE_FLOAT, (int) (TRACKING_BUFFER_SIZE / 2) );
  attach_1d_float_to_hdf5_1d(hdf5_manager->hdf5_1d_list[hdf5_manager->n_hdf5_1d-1], track_info->zmag);
  return 0;
}

//the most basic saving : onlyz,x,y,quality
int link_one_bead_to_hdf5_config1(sdi_v2_bead *bead)
{
  int index,l;
  char name[256];
  char bead_id[128];

  if (bead == NULL) return 1;
  sdi_v2_find_index_bead_in_fov(sdi_g_fov, bead, &index);
  //ss-gourp + good bead in different sub group?
  l=snprintf(name,256,"bead");
  snprintf(bead_id,128,"%d",index);//for now the id is the number
  snprintf(name+l,256-l,bead_id);
  add_one_1d_hdf5_to_manager();
  init_hdf5_1d(hdf5_manager->hdf5_1d_list[hdf5_manager->n_hdf5_1d-1], hdf5_manager->filename, "z", name, NULL, PTR_DATATYPE_FLOAT, (int) (TRACKING_BUFFER_SIZE / 2) );
  attach_1d_float_to_hdf5_1d(hdf5_manager->hdf5_1d_list[hdf5_manager->n_hdf5_1d-1], bead->z_t);
  add_one_1d_hdf5_to_manager();
  init_hdf5_1d(hdf5_manager->hdf5_1d_list[hdf5_manager->n_hdf5_1d-1], hdf5_manager->filename, "x", name, NULL, PTR_DATATYPE_FLOAT, (int) (TRACKING_BUFFER_SIZE / 2) );
  attach_1d_float_to_hdf5_1d(hdf5_manager->hdf5_1d_list[hdf5_manager->n_hdf5_1d-1], bead->x_t);
  add_one_1d_hdf5_to_manager();
  init_hdf5_1d(hdf5_manager->hdf5_1d_list[hdf5_manager->n_hdf5_1d-1], hdf5_manager->filename, "y", name, NULL, PTR_DATATYPE_FLOAT, (int) (TRACKING_BUFFER_SIZE / 2) );
  attach_1d_float_to_hdf5_1d(hdf5_manager->hdf5_1d_list[hdf5_manager->n_hdf5_1d-1], bead->y_t);

  return 0;
}

int test_hdf5_init(void)
{
  herr_t status;
  //manager of hdf5 objects
  hdf5_manager = (hdf5_gen *)calloc(1, sizeof(hdf5_gen));
  hdf5_manager->n_hdf5_1d = 0;
  snprintf(hdf5_manager->filename, 512, FILE);
  hid_t       file_id;
  file_id = H5Fcreate(FILE, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
  status = H5Fclose(file_id);
  link_track_info_to_hdf5();
  int i;
  if (sdi_g_fov==NULL) return D_O_K;
  for (i= 0;i<sdi_g_fov->n_beads ; i++) link_one_bead_to_hdf5_config1(sdi_g_fov->beads_list[i]);
  return 0;
}

int do_hdf5_init(void)
{
  if (updating_menu_state!=0) return D_O_K;
  test_hdf5_init();
  return 0;
}

int hdf5_record_job(int im, f_job *job)
{
  int i, cur_i, j;
  bool open_file=true;
  bool close_file=true;
  int ret;
  //static count = 0;
  static hid_t file_id;

  if (job == NULL) return 1;
  cur_i = job->local;
  //my_set_window_title("im %d jobimi %d", im, job->imi);
  if ( im>=job->imi )
  {
    for (i=0;i<hdf5_manager->n_hdf5_1d;i++)
    {
      /*if (i==0) open_file = true;
      else open_file = false;
      if (i==(hdf5_manager->n_hdf5_1d-1)) close_file = true;
      else close_file = false;*/
      if ( (ret = update_hdf5_1d(hdf5_manager->hdf5_1d_list[i], im, open_file, &file_id, close_file) ) != 0) ret=0;//my_set_window_title("pb with data saving %d", ret);
      //if (open_file == true ) open_file = false;
    }
    job->local ++;
    if (record_hdf5 == false)
    {
      job->in_progress = 0;
      win_printf_OK("end of record");
    }
    job->last_imi =  im;
    job->imi ++;
  }
  return 0;
}

//launches the sdi_y_y job+treatment
int hdf5_record_launch(int verbose, int which_treatment)
{
  int im_start;
  im_start = (int) (track_info->imi[track_info->c_i]/TRACKING_BUFFER_SIZE);
  im_start = im_start*TRACKING_BUFFER_SIZE+TRACKING_BUFFER_SIZE/2;
  fill_next_available_job_spot(1, im_start, SDI_SIGNAL_ANALYSIS, 0, NULL, NULL, NULL, NULL, NULL, hdf5_record_job, 0);
  return 0;
}

int do_hdf5_write(void)
{
  if(updating_menu_state!=0) return D_O_K;
  win_printf_OK("on/off write");
  //record_hdf5 = (record_hdf5 == true) ? false : true;
  if (record_hdf5==false)
  {
    hdf5_record_launch(0, 0);
    record_hdf5=true;
  }
  else record_hdf5=false;
  return D_O_K;
}


int do_filter_bad_fringes(void)
{
  if(updating_menu_state != 0) return D_O_K;
  if (sdi_g_fov == NULL) return D_O_K;
  if (sdi_f_d == NULL)
  {
    sdi_f_d = (sdi_v2_f_d *)calloc(1, sizeof(sdi_v2_f_d));
    sdi_v2_fringes_detection_gui(sdi_f_d);
  }
  //since i will delete some objects may be good to stop real-time tracking
  go_rt_track_y = false;
  go_rt_track_xz = false;
  go_rt_track_bead = false;
  sdi_v2_filter_bad_fringes(sdi_g_fov, sdi_f_d);
  return 0;
}
int sdi_v2_detect_fringes_general(void)
{
  if (sdi_g_fov == NULL) return D_O_K;
  if (sdi_g_fov->n_fringes !=0)
  {
    win_printf_OK("you already have fringes, please erase your FOV before");
    return D_O_K;
  }
  if (sdi_f_d == NULL)
  {
    sdi_f_d = (sdi_v2_f_d *)calloc(1, sizeof(sdi_v2_f_d));
    if (sdi_v2_fringes_detection_gui(sdi_f_d))  return 0;
  }
  go_rt_track_xz = false;
  go_rt_track_y = false;
  go_rt_track_bead = false;
  sdi_v2_detect_fringes(oi_TRACK, sdi_g_fov, sdi_f_d);
  draw_beads = false;
  draw_fringes = true;
  return 0;
}
int do_sdi_v2_detect_fringes(void)
{
  if (updating_menu_state !=0) return D_O_K;
  sdi_v2_detect_fringes_general();
  return 0;
}

int sdi_v2_job_on_y_y_profile(int im, f_job *job)
{
  int i, cur_i, j;

  if (job == NULL) return 1;
  cur_i = job->local;
  my_set_window_title("filling pt%d of job on y profiles", cur_i);
  for (i=0;i<sdi_g_fov->n_y_profile;i++)
  {
    sdi_g_fov->y_profile_list[i]->ds_out1->xd[cur_i] = im;
    sdi_g_fov->y_profile_list[i]->ds_out1->yd[cur_i] = sdi_g_fov->y_profile_list[i]->y_t[sdi_g_fov->y_profile_list[i]->c_output_t];
  }
  job->local ++;
  if (job->local == sdi_y_y->n_points)
  {
    job->in_progress = 0;
    go_rt_track_xz = false;
    go_rt_track_y = false;
    go_rt_track_bead = false;
    switch (sdi_y_y->which_treatment)
    {
      case SDI_FIND_BEADS_BY_CORRELATION:
      //find beads and allocate
      sdi_v2_gather_y_profiles_into_bead_with_noise_correlation(sdi_g_fov, sdi_y_b_d, &sdi_y_b_d->plr);
      //remove fringes objects and y profiles that are not part of beads
      sdi_v2_clean_fov_from_fringes_not_in_beads(sdi_g_fov);
      sdi_v2_clean_fov_from_y_profile_not_in_beads(sdi_g_fov);
      for (i=0;i<sdi_g_fov->n_beads;i++)
      {
        sdi_v2_center_x_roi_bead( sdi_g_fov->sdi_param, sdi_g_fov->beads_list[i]); //symmetrize the rois
        sdi_v2_allocate_sdi_v2_bead_output_t_fields( sdi_g_fov->beads_list[i], TRACKING_BUFFER_SIZE); //allocate rolling buffer for x(t), y and z(t)
        sdi_set_common_calibration_factors(sdi_g_fov, 0, sdi_calib_from_cfg); //puts automatic calibration factors
        for (j=0 ;j<2 ;j++) sdi_g_fov->beads_list[i]->fringes_bead[j]->need_background = true;//take background
        for (j=0 ;j<2 ;j++) sdi_g_fov->beads_list[i]->fringes_bead[j]->need_reference_point = true;//take ref points
        for (j=0 ;j<2 ;j++) sdi_g_fov->beads_list[i]->y_profile_bead[j]->need_background = true;//background
      }
      draw_fringes = false;
      draw_beads = true;
      go_rt_track_xz = true;
      go_rt_track_y = true;
      go_rt_track_bead = true;
      break;
    }
  }
  job->last_imi =  im;
  job->imi ++;
  return 0;
}

//launches the sdi_y_y job+treatment
int sdi_v2_job_y_y_profile_config(int verbose, int which_treatment)
{
  if (sdi_g_fov==NULL || sdi_y_y==NULL) return D_O_K;
  if (sdi_g_fov->n_beads > 0)
  {
    win_printf_OK("Please erase your beads before detection of new ones, and then re-detect fringes first");
    return D_O_K;
  }
  go_rt_track_xz = false;
  go_rt_track_bead = false;
  go_rt_track_y = true;
  int i, im_start;
  int n_points = TRACKING_BUFFER_SIZE;
  float zmag_y = track_info->zmag[track_info->c_i];
  if (which_treatment == -1) which_treatment = 0;
  if (sdi_y_y_from_cfg==true)
  {
    sdi_y_y->zmag_y_corr = get_config_float("SDI", "zmag_y_corr", 1.);
    sdi_y_y->n_points = get_config_int("SDI", "y_corr_n_points", 1);
    n_points = sdi_y_y->n_points;
    zmag_y = sdi_y_y->zmag_y_corr;
    sdi_y_y->which_treatment = which_treatment;
  }
  if (verbose==1)
  {
    if (win_scanf("This job records motion along y axis.\n"
    "Define the number of point of the record:%3d\n"
    "If, using this record, you want to detect beads by correlation of y motion,\n"
    "You should set a low force with zmag=%3f\n"
    "Choose the use of the record y(t):\n"
    "%R:nothing (just record), %r:bead detection"
    , &n_points, &zmag_y, &which_treatment) == WIN_CANCEL) return D_O_K;
    sdi_y_y->n_points = n_points;
    sdi_y_y->which_treatment = which_treatment + 20;
    sdi_y_y->zmag_y_corr = zmag_y;
  }
  if (sdi_y_y_from_cfg==true)
  {
    set_config_float("SDI", "zmag_y_corr", sdi_y_y->zmag_y_corr);
    set_config_int("SDI", "y_corr_n_points", sdi_y_y->n_points);
  }
  switch (sdi_y_y->which_treatment)
  {
    case SDI_FIND_BEADS_BY_CORRELATION :
    if (sdi_y_b_d == NULL) //just in case
    {
      sdi_y_b_d = (sdi_v2_y_b_d *)calloc(1, sizeof(sdi_v2_y_b_d));
      sdi_v2_y_bead_detection_gui(sdi_y_b_d, 1, sdi_y_b_from_cfg);
    }
    break;
  }
  if (sdi_g_fov->n_fringes == 0) return D_O_K;
  sdi_v2_create_y_profile_associated_with_fringes(sdi_g_fov);
  for (i=0;i<sdi_g_fov->n_y_profile;i++)
  {
    sdi_g_fov->y_profile_list[i]->need_background = true;
    sdi_g_fov->y_profile_list[i]->ds_out1 = build_adjust_data_set(sdi_g_fov->y_profile_list[i]->ds_out1, sdi_y_y->n_points, sdi_y_y->n_points);
  }
  fill_next_available_action(track_info->imi[track_info->c_i]+8, MV_ZMAG_ABS, sdi_y_y->zmag_y_corr);
  im_start = track_info->imi[track_info->c_i] + 64;
  fill_next_available_job_spot(1, im_start, SDI_SIGNAL_ANALYSIS, 0, NULL, NULL, NULL, NULL, NULL, sdi_v2_job_on_y_y_profile, 0);
  return 0;
}
//lauches the sdi_y_y job+treatment
int sdi_v2_job_y_y_profile_main(void)
{
  if (updating_menu_state!=0) return D_O_K;
  sdi_v2_job_y_y_profile_config(1,-1);
  return 0;
}


//////////////////////////////////////:jobs on z bead (t) //////////////////////////////////////////////////
int sdi_v2_job_on_xyz_bead(int im, f_job *job)
{
  int i, cur_i;
  cur_i = job->local;
  my_set_window_title("filling pt%d of bead job zmag=%2f", cur_i, track_info->zmag[track_info->c_i]);
  switch (sdi_xyz_b->which_treatment)
  {
    case SDI_Z_FIND_0_OF_MOLECULE:
    for (i=0;i<sdi_g_fov->n_beads;i++)
    {
      sdi_g_fov->beads_list[i]->ds_out1->xd[cur_i] = cur_i;
      sdi_g_fov->beads_list[i]->ds_out1->yd[cur_i] = sdi_g_fov->beads_list[i]->z_t[sdi_g_fov->beads_list[i]->c_output_t];
    }
    break;
    case SDI_Z_FIND_REF_BEADS:
    for (i=0;i<sdi_g_fov->n_beads;i++)
    {
      sdi_g_fov->beads_list[i]->ds_out1->xd[cur_i] = cur_i;
      sdi_g_fov->beads_list[i]->ds_out1->yd[cur_i] = sdi_g_fov->beads_list[i]->z_t[sdi_g_fov->beads_list[i]->c_output_t];
      sdi_g_fov->beads_list[i]->ds_out2->xd[cur_i] = cur_i;
      sdi_g_fov->beads_list[i]->ds_out2->yd[cur_i] = sdi_g_fov->beads_list[i]->x_t[sdi_g_fov->beads_list[i]->c_output_t];
      sdi_g_fov->beads_list[i]->ds_out3->xd[cur_i] = cur_i;
      sdi_g_fov->beads_list[i]->ds_out3->yd[cur_i] = sdi_g_fov->beads_list[i]->y_t[sdi_g_fov->beads_list[i]->c_output_t];
    }
    break;
    case SDI_Z_CLASSIFY_BEAD_FUNCTION:
    for (i=0;i<sdi_g_fov->n_beads;i++)
    {
      sdi_g_fov->beads_list[i]->ds_out1->xd[cur_i] = cur_i;
      sdi_g_fov->beads_list[i]->ds_out1->yd[cur_i] = sdi_g_fov->beads_list[i]->z_t[sdi_g_fov->beads_list[i]->c_output_t];
      sdi_g_fov->beads_list[i]->ds_out2->xd[cur_i] = cur_i;
      sdi_g_fov->beads_list[i]->ds_out2->yd[cur_i] = sdi_g_fov->beads_list[i]->x_t[sdi_g_fov->beads_list[i]->c_output_t];
      sdi_g_fov->beads_list[i]->ds_out3->xd[cur_i] = cur_i;
      sdi_g_fov->beads_list[i]->ds_out3->yd[cur_i] = sdi_g_fov->beads_list[i]->y_t[sdi_g_fov->beads_list[i]->c_output_t];
    }
    sdi_xyz_b->zmag_out->xd[cur_i] = cur_i;
	  int last_c_i;
	  last_c_i = (track_info->c_i > 0) ? track_info->c_i - 1 : 0;  // c_i is not yet valid
    sdi_xyz_b->zmag_out->yd[cur_i] = track_info->zmag[last_c_i];
    /*if (job->local % sdi_xyz_b->cycle_period)
    {
      float static where = sdi_xyz_b->z_mag_close;
      int im_start = track_info->imi[last_c_i ] + 4;
      where = (where==sdi_xyz_b->z_mag_close) ? sdi_xyz_b->z_mag_open : sdi_xyz_b->z_mag_close;
      fill_next_available_action(im_start, MV_ZMAG_ABS, where);
    }*/
    break;
  }
  job->local ++;
  if (job->local == sdi_xyz_b->n_points)
  {
    job->in_progress = 0;
    go_rt_track_xz = false;
    go_rt_track_y = false;
    go_rt_track_bead = false;
    switch (sdi_xyz_b->which_treatment)
    {
      case SDI_Z_FIND_0_OF_MOLECULE :
      sdi_v2_compute_0_of_beads_dsout(sdi_g_fov, sdi_xyz_b->verbose, sdi_xyz_b->replace, &sdi_xyz_b->plr);
      break;
      case SDI_Z_FIND_REF_BEADS :
      sdi_v2_compute_std_of_beads_dsout(sdi_g_fov, sdi_xyz_b->verbose, sdi_xyz_b->replace, &sdi_xyz_b->plr);
      break;
      case SDI_Z_CLASSIFY_BEAD_FUNCTION :
      sdi_v2_classify_beads_dsout(sdi_g_fov, sdi_xyz_b->zmag_out, sdi_xyz_b->verbose, sdi_xyz_b->replace, &sdi_xyz_b->plr);
      break;
    }
    go_rt_track_xz = true;
    go_rt_track_y = true;
    go_rt_track_bead = true;
  }
  job->last_imi =  im;
  job->imi ++;
  return 0;
}
int sdi_v2_job_xyz_bead_config(int verbose_gen, int which_treatment)
{
  if (sdi_g_fov == NULL) return D_O_K;
  if (sdi_xyz_b == NULL)//ust in case
  {
    sdi_xyz_b = (sdi_v2_xyz_b *)calloc(1, sizeof(sdi_v2_xyz_b));
    sdi_xyz_b->plr = NULL;
    if (sdi_xyz_b->zmag_out != NULL)
    {
      free_data_set(sdi_xyz_b->zmag_out);
      sdi_xyz_b->zmag_out = NULL;
    }
    if (sdi_g_fov->diagnose == NULL)
    {
      set_sdi_v2_diagnose_gui(sdi_g_diagnose, sdi_diagnose_from_cfg);
      sdi_g_fov->diagnose = sdi_g_diagnose;
    }
  }
  int i, im_start;
  static int n_points = TRACKING_BUFFER_SIZE;
  static float z_mag_lf = -2;
  static int replace = 0;
  static int verbose = 1;
  static float z_mag_close = -1, z_mag_open = -1;
  static int cycle_period = 16;
  static int continous_exp = 0;
  if (which_treatment==-1) which_treatment=0;
  if (sdi_xyz_bead_from_cfg == true)//eventually load from cfg
  {
    sdi_xyz_b->n_points = get_config_int("SDI", "xyz_n_points", 1);
    sdi_xyz_b->cycle_period = get_config_int("SDI", "zmag_cycle_period", 1);
    sdi_xyz_b->z_mag_lf = get_config_float("SDI", "z_mag_lf_l0", 1.);
    sdi_xyz_b->z_mag_open = get_config_float("SDI", "z_mag_open", 1.);
    sdi_xyz_b->z_mag_close =get_config_float("SDI", "z_mag_close", 1.);
    z_mag_lf = sdi_xyz_b->z_mag_lf;
    cycle_period = sdi_xyz_b->cycle_period;
    z_mag_close = sdi_xyz_b->z_mag_close;
    z_mag_open = sdi_xyz_b->z_mag_open;
    n_points = sdi_xyz_b->n_points;
    sdi_xyz_b->verbose = verbose;
    sdi_xyz_b->replace = replace;
    sdi_xyz_b->which_treatment = which_treatment;
  }
  if (verbose_gen==1)
  {
    if (win_scanf("This record of beads position, and peripherals, and apply processing to this record.\n"
    "Please define the duration (in frames) %2d\n"
    "Choose the processing %R:nothing (only recording),\n"
    "%r:find the origin of the molecule,\n"
    "%r:find immobile beads,\n"
    "%r:classify the beads (then the magnets will do on/off cycles))\n"
    "Do you want to have a graphical output of you treatment %R:no %r:yes\n"
    "If yes do you want to stack the plots%R or to replace%r?\n"
    "If you take the origin of the molecules, specify a low force zmag=%2f\n"
    "If you want to classify the beads, please enter zmagmin=%2f and max%2f and the period of cycles %d \n"
    "Do you want to apply your proc for once%R or periodically%r (not implemented yet)?"
    , &n_points, &which_treatment, &verbose, &replace, &z_mag_lf, &z_mag_close, &z_mag_open, &cycle_period, &continous_exp) == WIN_CANCEL) return D_O_K;
    sdi_xyz_b->n_points = n_points;
    sdi_xyz_b->which_treatment = which_treatment;
    sdi_xyz_b->verbose = verbose;
    sdi_xyz_b->replace = replace;
    sdi_xyz_b->z_mag_lf = z_mag_lf;
    sdi_xyz_b->z_mag_close = z_mag_close;
    sdi_xyz_b->z_mag_open = z_mag_open;
    sdi_xyz_b->cycle_period = cycle_period;
    sdi_xyz_b->continous_exp = continous_exp;
  }
  if (sdi_xyz_bead_from_cfg == true)//eventually load from cfg
  {
    set_config_int("SDI", "xyz_n_points", sdi_xyz_b->n_points);
    set_config_int("SDI", "zmag_cycle_period", sdi_xyz_b->cycle_period);
    set_config_float("SDI", "z_mag_lf_l0", sdi_xyz_b->z_mag_lf);
    set_config_float("SDI", "z_mag_open", sdi_xyz_b->z_mag_open);
    set_config_float("SDI", "z_mag_close", sdi_xyz_b->z_mag_close);
  }
  switch (sdi_xyz_b->which_treatment)
  {
    case SDI_Z_FIND_0_OF_MOLECULE:
    //only necessary to allocate ds_out1, a field in bead objects devoted to store, here, z(t), for a long time
    for (i=0;i<sdi_g_fov->n_beads;i++) sdi_g_fov->beads_list[i]->ds_out1 = build_adjust_data_set(sdi_g_fov->beads_list[i]->ds_out1, sdi_xyz_b->n_points, sdi_xyz_b->n_points);
    break;
    case SDI_Z_FIND_REF_BEADS: //this one only looks for immobile beads , not peripheral movements
    //necessary to allocate ds_out1/2/3, to store x(t), y(t), z(t)
    for (i=0;i<sdi_g_fov->n_beads;i++)
    {
      sdi_g_fov->beads_list[i]->ds_out1 = build_adjust_data_set(sdi_g_fov->beads_list[i]->ds_out1, sdi_xyz_b->n_points, sdi_xyz_b->n_points);
      sdi_g_fov->beads_list[i]->ds_out2 = build_adjust_data_set(sdi_g_fov->beads_list[i]->ds_out2, sdi_xyz_b->n_points, sdi_xyz_b->n_points);
      sdi_g_fov->beads_list[i]->ds_out3 = build_adjust_data_set(sdi_g_fov->beads_list[i]->ds_out3, sdi_xyz_b->n_points, sdi_xyz_b->n_points);
    }
    break;
    case SDI_Z_CLASSIFY_BEAD_FUNCTION: //this one does cylcles with magnets to classify beads (count the opening ones etc)
    //necessary to allocate ds_out1/2/3, to store x(t), y(t), z(t)
    for (i=0;i<sdi_g_fov->n_beads;i++)
    {
      sdi_g_fov->beads_list[i]->ds_out1 = build_adjust_data_set(sdi_g_fov->beads_list[i]->ds_out1, sdi_xyz_b->n_points, sdi_xyz_b->n_points);
      sdi_g_fov->beads_list[i]->ds_out2 = build_adjust_data_set(sdi_g_fov->beads_list[i]->ds_out2, sdi_xyz_b->n_points, sdi_xyz_b->n_points);
      sdi_g_fov->beads_list[i]->ds_out3 = build_adjust_data_set(sdi_g_fov->beads_list[i]->ds_out3, sdi_xyz_b->n_points, sdi_xyz_b->n_points);
    }
    sdi_xyz_b->zmag_out = build_adjust_data_set(sdi_xyz_b->zmag_out, sdi_xyz_b->n_points, sdi_xyz_b->n_points);
    break;
  }

  float where = sdi_xyz_b->z_mag_close;
  int last_c_i;
  last_c_i = (track_info->c_i > 0) ? track_info->c_i - 1 : 0;  // c_i is not yet valid
  im_start = track_info->imi[last_c_i ] + 32;
  //im_start = track_info->imi[track_info->c_i] + 32;
  if (sdi_xyz_b->which_treatment == SDI_Z_FIND_0_OF_MOLECULE) fill_next_available_action(im_start, MV_ZMAG_ABS, sdi_xyz_b->z_mag_lf);
  if (sdi_xyz_b->which_treatment == SDI_Z_CLASSIFY_BEAD_FUNCTION)
  {
    i=0;
    //while(i<sdi_xyz_b->n_points)
    //{
      //where = (where==sdi_xyz_b->z_mag_close) ? sdi_xyz_b->z_mag_open : sdi_xyz_b->z_mag_close;
      //fill_next_available_action(i + im_start, MV_ZMAG_ABS, where);
      //i+=sdi_xyz_b->cycle_period ;
    //}
  }
  fill_next_available_job_spot(1, im_start + 4, SDI_SIGNAL_ANALYSIS, 0, NULL, NULL, NULL, NULL, NULL, sdi_v2_job_on_xyz_bead, 0);
  //launches all the tracking if it was not done before
  if(go_rt_track_xz == false)
  {
    if(go_rt_track_y == false && go_rt_track_bead == false) sdi_v2_buffer_config(sdi_g_fov, TRACKING_BUFFER_SIZE);
    for (i=0;i<sdi_g_fov->n_fringes;i++)
    {
      sdi_g_fov->fringes_list[i]->need_background = true;//take background
      sdi_g_fov->fringes_list[i]->need_reference_point = true;//take ref points
    }
    sdi_g_fov->buffer_size = TRACKING_BUFFER_SIZE;
    go_rt_track_xz = true;
  }
  if(go_rt_track_y == false)
  {
    for (i=0;i<sdi_g_fov->n_y_profile;i++) sdi_g_fov->y_profile_list[i]->need_background = true;
    go_rt_track_y = true;
  }
  go_rt_track_bead = true;
  return D_O_K;
}

int sdi_v2_job_on_xyz_bead_main(void)
{
  if (updating_menu_state!=0) return D_O_K;
  sdi_v2_job_xyz_bead_config(1,-1);
  return 0;
}

int do_sdi_v2_grab_treatment(void)
{
  if(updating_menu_state!=0) return D_O_K;
  if(sdi_grab==NULL) return D_O_K;
  if(sdi_grab->oi==NULL) return D_O_K;
  if(sdi_g_fov==NULL) return D_O_K;
  switch (sdi_grab->which_treatment)
  {
    case SDI_RAMP_F_CALIBRATION:
    sdi_v2_calibration_of_fringes_fov(sdi_g_fov, sdi_grab->oi, sdi_grab->zobj_mes_grab);
    if (sdi_grab->verbose == 1)  sdi_v2_fov_calibration_spatial_effect(sdi_g_fov, &sdi_grab->plr, sdi_grab->replace, sdi_grab->zobj_mes_grab );
    break;
    case SDI_RAMP_F_LINEARITY_CHECK:
    sdi_v2_track_bead_movie_fov(sdi_g_fov, sdi_grab->oi);
    if (sdi_grab->verbose == 1)  sdi_v2_fov_linearity_check(sdi_g_fov, sdi_grab->zobj_mes_grab, &sdi_grab->plr, sdi_grab->replace);
    break;
  }
  free(sdi_grab->zmag_mes_grab);
  free(sdi_grab->zobj_mes_grab);
  return 0;
}

int sdi_v2_grab_function(int im)
{
  int j,k;
  static int count=0;
  if (count<sdi_grab->start_delay)
  {
    my_set_window_title("differed start count %d",count);
    count++;
  }
  else if ( (im%sdi_grab->period==0) && (sdi_grab->c_frame < (sdi_grab->n_frame)) )//grab this frame
  {
    switch_frame(sdi_grab->oi, sdi_grab->c_frame);
    for(j=0;j<sdi_grab->oi->im.ny;j++) for(k=0;k<sdi_grab->oi->im.nx;k++) sdi_grab->oi->im.pixel[j].ch[k] = (oi_TRACK->im.pixel[j].ch[k]);
    sdi_grab->zobj_mes_grab[sdi_grab->c_frame] = (float) track_info->obj_pos[track_info->c_i];
    sdi_grab->zmag_mes_grab[sdi_grab->c_frame] = (float) track_info->zmag[track_info->c_i];
    sdi_grab->c_frame++;
    my_set_window_title("frame %d", sdi_grab->c_frame);
    //not finished yet
    return 1;
  }
  else if ((sdi_grab->c_frame < (sdi_grab->n_frame)))
  {
    my_set_window_title("frame %d", -1);
    return 1;//nothing to do, but not done yet
  }
  else
  {
    my_set_window_title("frame %d done", sdi_grab->c_frame);
    find_zmin_zmax(sdi_grab->oi);
    sdi_grab->oi->need_to_refresh |= BITMAP_NEED_REFRESH;
    count=0;
    return 0;
  }
}
int do_sdi_v2_grab_movie(void)
{
    if (updating_menu_state != 0) return D_O_K;
    go_rt_track_y = false;
    go_rt_track_xz = false;
    go_rt_track_bead = false;
    int i, j, im;
    static int n_ramp=0;
    static float zobj_target=100;
    static float zmag_target=-1;
    static int which_treatment = 0;
    static int verbose = 0;
    static int replace = 1;
    static float z_mag_hf = -.3;
    static int n_frame = 1;
    static int delay = 0;
    static int period = 1;
    imreg *imr = NULL;
    if (ac_grep(cur_ac_reg,"%im",&imr) != 1)	return D_O_K;
    if (sdi_grab == NULL)
    {
      sdi_grab = (sdi_v2_grab *)calloc(1, sizeof(sdi_v2_grab));
      sdi_grab->oi = NULL;
      sdi_grab->plr = NULL;
      sdi_grab->grab = false;
    }
    if (win_scanf("This routine grabs a movie during the movement of one periph,\n"
    "Define the number of frame %4d (delay%2dd)\n"
    "Define the period of grabbing %4d\n"
    "Enter the target of the objective %3f\n"
    "Define the target of the magnets %3f (discarded now)\n"
    "Do you want to do a processing on that movie %R:nothing %r:calibration of fringes\n"
    "%r:check the linearity of the tracking\n"
    "Do you want to see graphical results?%R no %r yes\n"
    "if yes do you want to add plots%R or replace%r"
    , &n_frame, &delay, &period, &zobj_target, &zmag_target, &which_treatment, &verbose, &replace) == WIN_CANCEL) return D_O_K;
    if (sdi_grab->oi != NULL) remove_from_image(imr, IS_ONE_IMAGE, (void *)sdi_grab->oi);
    sdi_grab->zobj_target = zobj_target;
    sdi_grab->zmag_target = zmag_target;
    sdi_grab->n_frame = n_frame;
    sdi_grab->period = period;
    sdi_grab->start_delay = delay;
    sdi_grab->replace = replace;
    sdi_grab->verbose = verbose;
    sdi_grab->which_treatment = which_treatment + 40;
    sdi_grab->c_frame = 0;
    sdi_grab->zobj_mes_grab = (float *)calloc(sdi_grab->n_frame, sizeof(float));
    sdi_grab->zmag_mes_grab = (float *)calloc(sdi_grab->n_frame, sizeof(float));
    sdi_grab->oi = create_and_attach_movie_to_imr(imr, oi_TRACK->im.nx, oi_TRACK->im.ny, IS_CHAR_IMAGE, sdi_grab->n_frame);
    sdi_grab->oi->filename = my_sprintf(sdi_grab->oi->filename, "grab_movie%01d.gr", n_ramp);
    def_oi_scaling(sdi_grab->oi);//units & scales from cfg file of the microscope
    im = track_info->imi[track_info->c_i] + 4;
    sdi_grab->grab=true;
    go_rt_track_y = false;
    go_rt_track_xz = false;
    go_rt_track_bead = false;
    n_ramp ++;
    return D_O_K;
}


int sdi_v2_ramp_job(int im, f_job *job)
{
  int i_frame, j, k;
  i_frame = job->local;
  my_set_window_title("filling frame%d of movie ramp im%d, obj cmd at %2f", i_frame, im, track_info->obj_pos[track_info->c_i]);
  if ( (im >= (sdi_ramp->im[i_frame] + sdi_ramp->n_wait)) && (im < sdi_ramp->im[i_frame + 1]) )
  {
    switch_frame(sdi_ramp->oi, i_frame -1);
    for(j=0;j<sdi_ramp->oi->im.ny;j++) for(k=0;k<sdi_ramp->oi->im.nx;k++) sdi_ramp->oi->im.pixel[j].fl[k] += (float)(oi_TRACK->im.pixel[j].ch[k]);
    sdi_ramp->zo_mean_mes[i_frame -1] += (float) track_info->obj_pos[track_info->c_i];
    sdi_ramp->measurement_count[i_frame -1] ++;
  }
  if (im >= sdi_ramp->im[i_frame + 1])
  {
    for(j=0;j<sdi_ramp->oi->im.ny;j++) for(k=0;k<sdi_ramp->oi->im.nx;k++) sdi_ramp->oi->im.pixel[j].fl[k] /= (float) (sdi_ramp->measurement_count[i_frame - 1]);
    sdi_ramp->zo_mean_mes[i_frame - 1] /= (float) (sdi_ramp->measurement_count[i_frame - 1]);
    job->local ++;
    if (job->local == (sdi_ramp->n_step + 1))
    {
      job->in_progress = 0;
      find_zmin_zmax(sdi_ramp->oi);
      sdi_ramp->oi->need_to_refresh |= BITMAP_NEED_REFRESH;
      switch (sdi_ramp->which_treatment) {
        case SDI_RAMP_F_CALIBRATION:
        sdi_v2_calibration_of_fringes_fov(sdi_g_fov, sdi_ramp->oi, sdi_ramp->zo_mean_mes);
        if (sdi_ramp->verbose == 1)  sdi_v2_fov_calibration_spatial_effect(sdi_g_fov, &sdi_ramp->plr, sdi_ramp->replace, NULL);
        break;
        case SDI_RAMP_F_LINEARITY_CHECK:
        sdi_v2_track_bead_movie_fov(sdi_g_fov, sdi_ramp->oi);
        if (sdi_ramp->verbose == 1)  sdi_v2_fov_linearity_check(sdi_g_fov, sdi_ramp->zo_mean_mes, &sdi_ramp->plr, sdi_ramp->replace);
        break;
      }
      free(sdi_ramp->measurement_count);
      free(sdi_ramp->zo_mean_mes);
      free(sdi_ramp->zo_cmd);
      free(sdi_ramp->im);
    }
  }
  job->last_imi =  im;
  job->imi ++;
  return 0;
}
int do_sdi_v2_record_ramp_movie(void)
{
    if (updating_menu_state != 0) return D_O_K;
    go_rt_track_y = false;
    go_rt_track_xz = false;
    go_rt_track_bead = false;
    int i, j, im;
    static int n_ramp = 0;
    static int nstep = 5, nper = 20, nwait = 10;
    static float zobj_step = 0.1;
    static int which_treatment = 0;
    float zobj_start;
    static int verbose = 0;
    static int replace = 1;
    static float z_mag_hf = -.3;
    imreg *imr = NULL;
    if (ac_grep(cur_ac_reg,"%im",&imr) != 1)	return D_O_K;
    if (sdi_ramp == NULL)
    {
      sdi_ramp = (sdi_v2_ramp *)calloc(1, sizeof(sdi_v2_ramp));
      sdi_ramp->oi = NULL;
      sdi_ramp->plr = NULL;
    }
    zobj_start = track_info->obj_pos[track_info->c_i];
    if (win_scanf("This routine records a movie at different position of the objective\n"
    "followed by a signal processing on the recorded movie.\n"
    "I am going to erase the previous movie, if you want to save it, cancel and save.\n"
    "Define the objective starting position %3f\n"
    "Define the objective step size %3f\n"
    "Define the number of steps %3d\n"
    "Define the number of frame on which I average each step %8d\n"
    "Define the number of frame to wait before averaging %8d (to avoid moving objective)\n"
    "Define the position of the magnets during the all process %3f (high force is a good idea\n"
    "Do you want to do a processing on that movie %R:nothing %r:calibration of fringes\n"
    "%r:check the linearity of the tracking\n"
    "Do you want to see graphical results?%R no %r yes\n"
    "if yes do you want to add plots%R or replace%r"
    , &zobj_start, &zobj_step, &nstep, &nper, &nwait, &z_mag_hf, &which_treatment, &verbose, &replace) == WIN_CANCEL) return D_O_K;
    if (sdi_ramp->oi != NULL) remove_from_image(imr, IS_ONE_IMAGE, (void *)sdi_ramp->oi);

    sdi_ramp->n_step = nstep;
    sdi_ramp->zobj_start = zobj_start;
    sdi_ramp->zobj_step = zobj_step;
    sdi_ramp->n_per_frame = nper;
    sdi_ramp->n_wait = nwait;
    sdi_ramp->verbose = verbose;
    sdi_ramp->replace = replace;
    sdi_ramp->z_mag_hf = z_mag_hf;
    sdi_ramp->which_treatment = which_treatment + 40;
    sdi_ramp->im = (int *)calloc(sdi_ramp->n_step + 2, sizeof(int));
    sdi_ramp->measurement_count = (int *)calloc(sdi_ramp->n_step + 2, sizeof(int));
    sdi_ramp->zo_cmd = (float *)calloc(sdi_ramp->n_step + 2, sizeof(float));
    sdi_ramp->zo_mean_mes = (float *)calloc(sdi_ramp->n_step, sizeof(float));
    im = track_info->imi[track_info->c_i] + 64;
    fill_next_available_action(im, MV_ZMAG_ABS, sdi_ramp->z_mag_hf);
    im += 128;
    for (i = 0; i < nstep + 2 ; i++)//going +2 because i discard the first (obj )
    {
        sdi_ramp->zo_cmd[i] = sdi_ramp->zobj_start + (i * sdi_ramp->zobj_step);
        sdi_ramp->im[i] = im;
        im += sdi_ramp->n_per_frame ;
        if (fill_next_available_action(im, MV_OBJ_ABS, sdi_ramp->zo_cmd[i]) <0) return D_O_K;
    }
    sdi_ramp->oi = create_and_attach_movie_to_imr(imr, oi_TRACK->im.nx, oi_TRACK->im.ny, IS_FLOAT_IMAGE, nstep);
    def_oi_scaling(sdi_ramp->oi);//units & scales from cfg file of the microscope
    sdi_ramp->oi->filename = my_sprintf(sdi_ramp->oi->filename, "image_list%01d.gr", n_ramp);
    fill_next_available_job_spot(1, sdi_ramp->im[1], GRAB_SDI_RAMP_MOVIE, 0, imr, NULL, NULL, NULL, NULL, sdi_v2_ramp_job, 0);
    go_rt_track_y = false;
    go_rt_track_xz = false;
    go_rt_track_bead = false;
    n_ramp ++;
    return D_O_K;
}

///////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////ends of jobs//////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////setting up the sdi parameters/////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//load the parameters from config files
//if needed the user can change iwith the menus
int sdi_instrument_init(void)
{
  int x_roi_size, y_roi_size, x_window_order, x_window_fwhm, dc_width, dist_half_beads, x_roi_size_Y, y_roi_size_Y, y_window_fwhm, y_window_order;
  float index_factor;
  float nu_threshold;
  float z_min_limit, z_max_limit, y_min_limit, y_max_limit, x_min_limit, x_max_limit;
  float I_min_limit, I_max_limit, contrast_min_limit, std_threshold_x, std_threshold_y, std_threshold_z, z_expected_range, z_range_tol;
  int weird_count_limit;

  if (track_info == NULL) return 1;
  track_info->SDI_mode = 1;
  if (sdi_colormap == NULL) sdi_v2_define_sdi_colormap();
  sdi_calib_from_cfg = true;

  //tracking algo parameters loaded from config files
  sdi_par_from_cfg = true;
  if (sdi_g_param == NULL) sdi_g_param = (sdi_v2_parameter *)calloc(1, sizeof(sdi_v2_parameter));
  if (sdi_par_from_cfg == true)
  {
    x_roi_size = get_config_int("SDI", "x_roi_size_zx", 1);
    y_roi_size = get_config_int("SDI", "y_roi_size_zx", 1);
    x_window_order = get_config_int("SDI", "x_window_order", 1);
    x_window_fwhm = get_config_int("SDI", "x_window_fwhm", 1);
    dc_width = get_config_int("SDI", "dc_width", 1);
    dist_half_beads = get_config_int("SDI", "dist_half_beads", 1);
    x_roi_size_Y = get_config_int("SDI", "x_roi_size_y", 1);
    y_roi_size_Y = get_config_int("SDI", "y_roi_size_y", 1);
    y_window_order = get_config_int("SDI", "y_window_fwhm", 1);
    y_window_fwhm = get_config_int("SDI", "y_window_order", 1);
    nu_threshold = get_config_float("SDI", "nu_threshold", 1.);
    index_factor = get_config_float("SDI", "refraction_index", 1.);
    //tention au plantage ac ;auvais config file..
  }
  if (sdi_par_from_cfg==false) set_sdi_v2_parameters_gui(sdi_g_param, false);
  else set_sdi_v2_parameters(sdi_g_param, x_roi_size, y_roi_size, dist_half_beads, x_window_fwhm,
                   nu_threshold, dc_width, x_window_order, x_roi_size_Y, y_roi_size_Y, y_window_fwhm, y_window_order, 1, index_factor, 1);


  //diagnositc of bead quality and function
  sdi_diagnose_from_cfg = true;
  if (sdi_g_diagnose == NULL) sdi_g_diagnose = (sdi_v2_diagnose *)calloc(1, sizeof(sdi_v2_diagnose));
  if (sdi_diagnose_from_cfg == true)
  {
    weird_count_limit = get_config_int("SDI", "weird_count_limit", 1);
    contrast_min_limit = get_config_float("SDI", "contrast_min_limit", 1.);
    I_min_limit = get_config_float("SDI", "I_min_limit", 1.);
    I_max_limit = get_config_float("SDI", "I_max_limit", 1.);
    z_min_limit = get_config_float("SDI", "z_min_limit", 1.);
    z_max_limit = get_config_float("SDI", "z_max_limit", 1.);
    y_min_limit = get_config_float("SDI", "y_min_limit", 1.);
    y_max_limit = get_config_float("SDI", "y_max_limit", 1.);
    x_max_limit = get_config_float("SDI", "x_max_limit", 1.);
    x_min_limit = get_config_float("SDI", "x_min_limit", 1.);
    std_threshold_z = get_config_float("SDI", "std_threshold_z", 1.);
    std_threshold_x = get_config_float("SDI", "std_threshold_x", 1.);
    std_threshold_y = get_config_float("SDI", "std_threshold_y", 1.);
    z_expected_range = get_config_float("SDI", "z_expected_range", 1.);
    z_range_tol = get_config_float("SDI", "z_range_tol", 1.);
  }
  if (sdi_diagnose_from_cfg==false) set_sdi_v2_diagnose_gui(sdi_g_diagnose, false);
  else sdi_v2_set_diagnose(sdi_g_diagnose, z_min_limit, z_max_limit, y_min_limit, y_max_limit, x_min_limit, x_max_limit, I_min_limit, I_max_limit,
          contrast_min_limit, weird_count_limit, std_threshold_x, std_threshold_y, std_threshold_z,  z_expected_range,  z_range_tol);
  if (sdi_oi_a == NULL)
	{
	  sdi_oi_a = (sdi_v2_oi_analysis *)calloc(1, sizeof(sdi_v2_oi_analysis));
	  sdi_oi_a->sdi_f_d = (sdi_v2_f_d *)calloc(1, sizeof(sdi_v2_f_d));
	}

  //initializes the structures that handle jobs related to y profiles objects (bead detection)
  if (sdi_y_y == NULL)
  {
    sdi_y_y = (sdi_v2_y_y *)calloc(1, sizeof(sdi_v2_y_y));
    sdi_y_y->plr=NULL;
    sdi_y_y->op=NULL;
  }
  sdi_y_y_from_cfg = true;
  if (sdi_y_y_from_cfg==true)
  {
    sdi_y_y->zmag_y_corr = get_config_float("SDI", "zmag_y_corr", 1.);
    sdi_y_y->n_points = get_config_int("SDI", "y_corr_n_points", 1);
    sdi_y_y->which_treatment = SDI_FIND_BEADS_BY_CORRELATION;
  }
  //parameters to correlate the y brownian motion
  sdi_y_b_from_cfg = true;
  if (sdi_y_b_d == NULL)
  {
    sdi_y_b_d = (sdi_v2_y_b_d *)calloc(1, sizeof(sdi_v2_y_b_d));
    sdi_v2_y_bead_detection_gui(sdi_y_b_d, 0, sdi_y_b_from_cfg);
  }

  //parameters of job on xyz of beads
  sdi_xyz_bead_from_cfg = true;
  if(sdi_xyz_b == NULL)
  {
    sdi_xyz_b = (sdi_v2_xyz_b *)calloc(1, sizeof(sdi_v2_xyz_b));
    sdi_xyz_b->plr = NULL;
  }
  if (sdi_xyz_bead_from_cfg == true)
  {
    sdi_xyz_b->n_points = get_config_int("SDI", "xyz_n_points", 1);
    sdi_xyz_b->cycle_period = get_config_int("SDI", "zmag_cycle_period", 1);
    sdi_xyz_b->z_mag_lf = get_config_float("SDI", "z_mag_lf_l0", 1.);
    sdi_xyz_b->z_mag_open = get_config_float("SDI", "z_mag_open", 1.);
    sdi_xyz_b->z_mag_close =get_config_float("SDI", "z_mag_close", 1.);
  }


  if (sdi_g_fov == NULL)
  {
    sdi_g_fov = (sdi_v2_fov *)calloc(1, sizeof(sdi_v2_fov));
    sdi_v2_init_one_fov(sdi_g_fov);
    sdi_g_fov->sdi_param = sdi_g_param;
    sdi_g_fov->diagnose = sdi_g_diagnose;
  }
  track_info->sdi_param = sdi_g_fov->sdi_param;
  //initializes one info panel
  if (g_info == NULL)
  {
    g_info = (sdi_v2_p_info *)calloc(1,sizeof(sdi_v2_p_info));
    sdi_v2_init_one_panel(g_info, 210, 250, 300, 700);//width height x y
  }
  if (g_info_help == NULL)
  {
    g_info_help = (sdi_v2_p_info *)calloc(1,sizeof(sdi_v2_p_info));
    sdi_v2_init_one_panel(g_info_help, 210, 320, 300, 350);//width height x y
  }
  //replaces the track_util actions of oi_TRACK
  if(oi_TRACK != NULL)
  {
    oi_TRACK->oi_idle_action = sdi_v2_oi_idle_action;
    oi_TRACK->oi_post_display = sdi_v2_oi_idle_action;
    oi_TRACK->oi_mouse_action = sdi_v2_oi_mouse_action;
    sdi_g_fov->buffer_size = TRACKING_BUFFER_SIZE;
  }
  return 0;
}

//sued without SDI_VERSION flag : no config files, can be used with same SW as ring one, if one switc during the same experimenth for example
//by default nothing is loaded : no quality control, no fringes_detect parameters, no bead tedect parameters..
int do_switch_tracking_algo(void)
{
  if (updating_menu_state !=0) return D_O_K;
  if (track_info != NULL) track_info->SDI_mode = 1;
  if (sdi_colormap == NULL) sdi_v2_define_sdi_colormap();
  if (sdi_g_param == NULL) sdi_g_param = (sdi_v2_parameter *)calloc(1, sizeof(sdi_v2_parameter));
  set_sdi_v2_parameters_gui(sdi_g_param,sdi_par_from_cfg);
  if (sdi_g_diagnose == NULL) sdi_g_diagnose = (sdi_v2_diagnose *)calloc(1, sizeof(sdi_v2_diagnose));
  //initializes structure that handle image (not signal) processing like looking for fringes patterns
  if (sdi_oi_a == NULL)
  {
    sdi_oi_a = (sdi_v2_oi_analysis *)calloc(1, sizeof(sdi_v2_oi_analysis));
    sdi_oi_a->sdi_f_d = (sdi_v2_f_d *)calloc(1, sizeof(sdi_v2_f_d));
  }
  //initializes the structures that handle jobs related to y profiles objects (bead detection)
  if (sdi_y_y == NULL) sdi_y_y = (sdi_v2_y_y *)calloc(1, sizeof(sdi_v2_y_y));
  if (sdi_g_fov == NULL)
  {
    sdi_g_fov = (sdi_v2_fov *)calloc(1, sizeof(sdi_v2_fov));
    sdi_v2_init_one_fov(sdi_g_fov);
    sdi_g_fov->sdi_param = sdi_g_param;
    sdi_g_fov->diagnose = sdi_g_diagnose;
  }
  track_info->sdi_param = sdi_g_fov->sdi_param;
  //initializes one info panel
  if (g_info == NULL)
  {
    g_info = (sdi_v2_p_info *)calloc(1,sizeof(sdi_v2_p_info));
    sdi_v2_init_one_panel(g_info, 240, 350, 240, 900);//width height x y
  }
  if (g_info_help == NULL)
  {
    g_info_help = (sdi_v2_p_info *)calloc(1,sizeof(sdi_v2_p_info));
    sdi_v2_init_one_panel(g_info_help, 300, 480, 300, 480);//width height x y
  }
  //replaces the track_util actions of oi_TRACK
  if(oi_TRACK != NULL)
  {
    oi_TRACK->oi_idle_action = sdi_v2_oi_idle_action;
    oi_TRACK->oi_post_display = sdi_v2_oi_idle_action;
    oi_TRACK->oi_mouse_action = sdi_v2_oi_mouse_action;
    sdi_g_fov->buffer_size = TRACKING_BUFFER_SIZE;
  }
  return 0;
}

int do_switch_what_to_draw(void)
{
  if (updating_menu_state !=0) return D_O_K;
  draw_beads = (draw_beads == true) ? false : true;
  draw_fringes = (draw_beads == false) ? true : false;
  return 0;
}
int do_set_sdi_v2_parameters_gui(void)
{
  if (updating_menu_state !=0) return D_O_K;
  if (sdi_g_param == NULL) sdi_g_param = (sdi_v2_parameter *)calloc(1, sizeof(sdi_v2_parameter));
  set_sdi_v2_parameters_gui(sdi_g_param,sdi_par_from_cfg);
  if (sdi_g_fov == NULL)
  {
    sdi_g_fov = (sdi_v2_fov *)calloc(1, sizeof(sdi_v2_fov));
    sdi_v2_init_one_fov(sdi_g_fov);
    sdi_g_fov->sdi_param = sdi_g_param;
  }
  int i;
  for (i=0 ;i<sdi_g_fov->n_beads ;i++) sdi_v2_update_bead_parameters( sdi_g_fov->beads_list[i], sdi_g_param, sdi_g_fov->beads_list[i]->xc, sdi_g_fov->beads_list[i]->yc);
  sdi_v2_update_all_sdi_fringes_parameters(sdi_g_fov, sdi_g_param);
  sdi_v2_update_all_sdi_y_profile_parameters(sdi_g_fov, sdi_g_param);
  return 0;
}
int do_set_sdi_v2_diagnose_gui(void)
{
  if (updating_menu_state!=0) return D_O_K;
  if (sdi_g_fov==NULL) return D_O_K;
  set_sdi_v2_diagnose_gui(sdi_g_diagnose, sdi_diagnose_from_cfg);
  if (sdi_g_fov !=NULL ) sdi_g_fov->diagnose = sdi_g_diagnose;
  return 0;
}
//////////////////////////////sdi calculations on recorded movies/////////////////////
/////////////////////////////////////////////////////////////////////////////////
int do_calibrate_fov_from_movie(void)
{
  if(updating_menu_state != 0) return D_O_K;
  imreg *imr = NULL;
  O_i *oi = NULL;
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)	return D_O_K;
  if (win_scanf("you are going to calibrate your fringes with the current movie, you ok?")== WIN_CANCEL) return D_O_K;
  if (sdi_g_fov == NULL) return D_O_K;
  if (sdi_g_fov->sdi_param == NULL) return D_O_K;
  go_rt_track_xz = false;
  go_rt_track_y = false;
  go_rt_track_bead = false;
  int i;
  if (oi->im.n_f >1)
  {
    oi->oi_idle_action = sdi_v2_oi_idle_action;
    oi->oi_post_display = sdi_v2_oi_idle_action;
    oi->oi_mouse_action = sdi_v2_oi_mouse_action;
    sdi_v2_calibration_of_fringes_fov(sdi_g_fov, oi, NULL);
  }

  return 0;
}
int do_spatial_effect( void )
{
  if (updating_menu_state != 0) return D_O_K;
  if (sdi_g_fov == NULL) return D_O_K;
  if (sdi_g_fov->n_beads == 0) return D_O_K;
  pltreg *plr = NULL;
  win_printf_OK("disconnected");
  sdi_v2_fov_calibration_spatial_effect(sdi_g_fov, &plr, 0,NULL);
  return 0;
}
int do_track_fringes_of_fov_from_movie(void)
{
  if(updating_menu_state != 0) return D_O_K;
  imreg *imr = NULL;
  O_i *oi = NULL;
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)	return D_O_K;
  if (win_scanf("this track the fringes on the current movie, you ok?")== WIN_CANCEL) return D_O_K;
  if (sdi_g_fov == NULL) return D_O_K;
  if (sdi_g_fov->sdi_param == NULL) return D_O_K;
  go_rt_track_xz = false;
  go_rt_track_y = false;
  go_rt_track_bead = false;
  if (oi->im.n_f >1)
  {
    oi->oi_idle_action = sdi_v2_oi_idle_action;
    oi->oi_post_display = sdi_v2_oi_idle_action;
    oi->oi_mouse_action = sdi_v2_oi_mouse_action;
    sdi_v2_track_fringes_fov(sdi_g_fov, oi);
  }
  return 0;
}

int do_track_y_profiles(void)
{
  if (updating_menu_state != 0) return D_O_K;
  imreg *imr = NULL;
  O_i *oi = NULL;
  if (ac_grep(cur_ac_reg, "%im%oi", &imr, &oi) !=2 ) return D_O_K;
  if (sdi_g_fov == NULL) return D_O_K;
  if (sdi_g_param == NULL) return D_O_K;
  sdi_v2_track_y_profile_fov_movie(sdi_g_fov, oi);
  oi->oi_idle_action = sdi_v2_oi_idle_action;
  oi->oi_post_display = sdi_v2_oi_idle_action;
  oi->oi_mouse_action = sdi_v2_oi_mouse_action;
  return 0;
}
int do_track_fov_from_movie(void)
{
  if (updating_menu_state != 0) return D_O_K;
  if (sdi_g_fov == NULL) return D_O_K;
  if (sdi_g_fov->sdi_param == NULL) return D_O_K;
  imreg *imr = NULL;
  O_i *oi = NULL;
  if (ac_grep(cur_ac_reg, "%im%oi", &imr, &oi)!=2) return D_O_K;
  win_printf_OK("this track the current movie");
  sdi_v2_track_bead_movie_fov(sdi_g_fov, oi);
  return 0;
}
int do_extract_submovie_from_movie(void)
{
  if (updating_menu_state !=0) return D_O_K;
  imreg *imr = NULL;
  O_i *oi_in = NULL;
  O_i *oi_out = NULL;
  static int period =1;
  static int offset =0;
  static int subset_length=1;
  static int number=1;
  static int i_movie=0;
  if (ac_grep(cur_ac_reg, "%im", &imr)!=1) return D_O_K;
  if (win_scanf("this routine extract a submovie from the %d one with period %d, offset %d, subset_length %d number %d",
                            &i_movie, &period, &offset, &subset_length, &number) == WIN_CANCEL) return D_O_K;
  if (i_movie>=imr->n_oi) return D_O_K;
  oi_in = imr->o_i[i_movie];
  sdi_v2_extract_char_submovie_from_char_movie(imr, oi_out, oi_in, period, offset, subset_length, number);
  return 0;
}




//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////collection of routines working on the mouse_selected_bead of track_util
int sdi_v2_printf_bead_info(void)
{
  if (updating_menu_state != 0) return D_O_K;
  if (sdi_g_fov==NULL) return D_O_K;
  win_printf_OK("info about the mouse selected bead\n"
                "fringes x=%d y=%d, x=%d y=%d\n"
                "about the first fringe pattern : freq=%f, v0=%f, v1=%f\n"
                "about the second fringe pattern : freq=%f, v0=%f, v1=%f\n"
                "about the xphase vel  vx0=%f, vx1=%f\n"
                 , sdi_g_fov->beads_list[mouse_selected_bead]->fringes_bead[0]->xc, sdi_g_fov->beads_list[mouse_selected_bead]->fringes_bead[0]->yc
                , sdi_g_fov->beads_list[mouse_selected_bead]->fringes_bead[1]->xc, sdi_g_fov->beads_list[mouse_selected_bead]->fringes_bead[1]->yc
    , sdi_g_fov->beads_list[mouse_selected_bead]->fringes_bead[0]->nu0, sdi_g_fov->beads_list[mouse_selected_bead]->fringes_bead[0]->v_0 , sdi_g_fov->beads_list[mouse_selected_bead]->fringes_bead[0]->v_1 ,
    sdi_g_fov->beads_list[mouse_selected_bead]->fringes_bead[1]->nu0, sdi_g_fov->beads_list[mouse_selected_bead]->fringes_bead[1]->v_0 , sdi_g_fov->beads_list[mouse_selected_bead]->fringes_bead[1]->v_1,
    sdi_g_fov->beads_list[mouse_selected_bead]->fringes_bead[0]->v_x , sdi_g_fov->beads_list[mouse_selected_bead]->fringes_bead[0]->v_x);
  return 0;
}
int sdi_v2_remove_mouse_selected_bead(void)
{
  if (updating_menu_state != 0) return D_O_K;
  if (sdi_g_fov==NULL) return D_O_K;
  bool go_rt_track_tmp_xz, go_rt_track_tmp_y;
  bool go_rt_track_tmp_bead = go_rt_track_bead;
  go_rt_track_tmp_xz = go_rt_track_xz;
  go_rt_track_xz = false;
  go_rt_track_tmp_y = go_rt_track_y;
  go_rt_track_y = false;
  go_rt_track_tmp_bead = go_rt_track_bead;
  go_rt_track_bead = false;
  sdi_v2_remove_one_sdi_bead_from_fov(sdi_g_fov, mouse_selected_bead);
  go_rt_track_xz = go_rt_track_tmp_xz;
  go_rt_track_y = go_rt_track_tmp_y;
  go_rt_track_bead = go_rt_track_tmp_bead;
  return 0;
}
int do_display_xyz_of_mouse_selected_bead(void)
{
  if (updating_menu_state !=0) return D_O_K;
  if (sdi_g_fov == NULL) return D_O_K;
  bool go_rt_track_tmp_xz = go_rt_track_xz;
  bool go_rt_track_tmp_y = go_rt_track_y;
  bool go_rt_track_tmp_bead = go_rt_track_bead;
  go_rt_track_xz = false;
  go_rt_track_y = false;
  go_rt_track_bead = false;
  //allocate size adapted to the oi that you are tracking : TRACKING_BUFFER_SIZE if oi_TRACK, nf if movie
  O_i *oi_c = NULL;
  int nf;
  float t0=0;
  if (ac_grep(cur_ac_reg, "%oi", &oi_c) != 1) return D_O_K;
  if (oi_c == oi_TRACK) nf = TRACKING_BUFFER_SIZE;
  else nf = oi_c->im.n_f;
  if (track_info!=NULL) t0 = (float) (track_info->imi[track_info->c_i]);
  sdi_v2_display_bead_z(sdi_g_fov, sdi_g_fov->beads_list[mouse_selected_bead], NULL, NULL, nf, t0);
  sdi_v2_display_bead_y(sdi_g_fov, sdi_g_fov->beads_list[mouse_selected_bead], sdi_g_fov->display_list[sdi_g_fov->n_display-1]->plr, NULL, nf, t0);
  sdi_v2_display_bead_x(sdi_g_fov, sdi_g_fov->beads_list[mouse_selected_bead], sdi_g_fov->display_list[sdi_g_fov->n_display-1]->plr, NULL, nf, t0);
  sdi_v2_display_bead_quality_factor(sdi_g_fov, sdi_g_fov->beads_list[mouse_selected_bead], sdi_g_fov->display_list[sdi_g_fov->n_display-1]->plr, NULL, nf, t0);
  go_rt_track_xz = go_rt_track_tmp_xz;
  go_rt_track_y = go_rt_track_tmp_y;
  go_rt_track_bead = go_rt_track_tmp_bead;
  return 0;
}
int do_display_all_fringes_step_of_mouse_selected_bead(void)
{
  if (updating_menu_state !=0) return D_O_K;
  if (sdi_g_fov == NULL) return D_O_K;
  bool go_rt_track_tmp_xz = go_rt_track_xz;
  bool go_rt_track_tmp_y = go_rt_track_y;
  bool go_rt_track_tmp_bead = go_rt_track_bead;
  go_rt_track_xz = false;
  go_rt_track_y = false;
  go_rt_track_bead = false;
  O_i *oi_c = NULL;
  int nf;
  if (ac_grep(cur_ac_reg, "%oi", &oi_c) != 1) return D_O_K;
  if (oi_c == oi_TRACK) nf = TRACKING_BUFFER_SIZE;
  else nf = oi_c->im.n_f;
  sdi_v2_display_2_fringes_all_steps(sdi_g_fov, sdi_g_fov->beads_list[mouse_selected_bead]->fringes_bead[0],
                              sdi_g_fov->beads_list[mouse_selected_bead]->fringes_bead[1], NULL, NULL, nf);
  go_rt_track_xz = go_rt_track_tmp_xz;
  go_rt_track_y = go_rt_track_tmp_y;
  go_rt_track_bead = go_rt_track_tmp_bead;
  return 0;
}
int do_display_all_y_profiles_step_of_mouse_selected_bead(void)
{
  if (updating_menu_state !=0) return D_O_K;
  if (sdi_g_fov == NULL) return D_O_K;
  bool go_rt_track_tmp_xz = go_rt_track_xz;
  bool go_rt_track_tmp_y = go_rt_track_y;
  bool go_rt_track_tmp_bead = go_rt_track_bead;
  go_rt_track_xz = false;
  go_rt_track_y = false;
  go_rt_track_bead = false;
  O_i *oi_c = NULL;
  int nf;
  if (ac_grep(cur_ac_reg, "%oi", &oi_c) != 1) return D_O_K;
  if (oi_c == oi_TRACK) nf = TRACKING_BUFFER_SIZE;
  else nf = oi_c->im.n_f;
  sdi_v2_set_y_profile_fourier_algo_display1(sdi_g_fov, sdi_g_fov->beads_list[mouse_selected_bead]->y_profile_bead[0], NULL, NULL);
  go_rt_track_xz = go_rt_track_tmp_xz;
  go_rt_track_y = go_rt_track_tmp_y;
  go_rt_track_bead = go_rt_track_tmp_bead;
  return 0;
}
int do_set_calibration_factors_of_mouse_selected_bead(void)
{
  if (updating_menu_state !=0) return D_O_K;
  static float v_00 = 2.95;
  static float v_01 = -2.72;
  static float v_10 = 4.98;
  static float v_11 = -5.48;
  static float nu00 = 0.105;
  static float nu01 = 0.105;
  static float v_x0 = 6.3;
  static float v_x1 = 6.3;
  if (sdi_g_fov == NULL) return D_O_K;
  if (win_scanf("you can enter calibration factors manually (default are results for fringes of jfa setup-patent data) \n"
                "1st fringes : z phase velocity=%3f (rad.mic-1), z group velocity v10=%3f (pix.mic-1) frequency=%3f \n"
                "2nd fringes : z phase velocity=%3f (rad.mic-1), z group velocity v10=%3f (pix.mic-1) frequency=%3f \n"
                "phase velocity along x %3f %3f",
                        &v_00, &v_10, &nu00, &v_01, &v_11, &nu01, &v_x0, &v_x1) == WIN_CANCEL) return D_O_K;
  sdi_v2_set_calibration_of_bead(sdi_g_fov->beads_list[mouse_selected_bead], v_00, v_10, nu00, v_01, v_11, nu01, v_x0, v_x1);
  return 0;
}
int do_sdi_v2_duplicate_one_bead(void)
{
  if (updating_menu_state !=0) return D_O_K;
  if (sdi_g_fov == NULL) return D_O_K;
  int n_copy = 1;
  int i,j;
  go_rt_track_xz = go_rt_track_y = go_rt_track_bead = false;
  if (win_scanf("%d duplication of current bead",&n_copy) == WIN_CANCEL) return D_O_K;

  for (j=0;j<n_copy;j++)
  {
    sdi_v2_add_one_bead_to_fov_with_coordinates(sdi_g_fov, sdi_g_fov->beads_list[mouse_selected_bead]->xc, sdi_g_fov->beads_list[mouse_selected_bead]->yc);
    sdi_v2_allocate_sdi_v2_bead_output_t_fields( sdi_g_fov->beads_list[sdi_g_fov->n_beads-1], TRACKING_BUFFER_SIZE);//default size of output
    sdi_set_common_calibration_factors(sdi_g_fov, 0, sdi_calib_from_cfg); //puts calibration factors, not verbose
    for (i=0 ;i<2 ;i++) sdi_g_fov->beads_list[sdi_g_fov->n_beads-1]->fringes_bead[i]->need_background = true;//ask to ref point asap the tracking restart
    for (i=0 ;i<2 ;i++) sdi_g_fov->beads_list[sdi_g_fov->n_beads-1]->y_profile_bead[i]->need_background = true;//ask to ref point asap the tracking restart
    for (i=0 ;i<2 ;i++) sdi_g_fov->beads_list[sdi_g_fov->n_beads-1]->fringes_bead[i]->need_reference_point = true;//ask to ref point asap the tracking restart
  }
  win_printf_OK("added %d duplication of one bead", n_copy);
  return 0;
}


MENU *sdi_one_bead_menu(void)
{
  static MENU mn[16];
  if (mn[0].text != NULL) return mn;
  add_item_to_menu(mn, "plot x,y,z of bead", do_display_xyz_of_mouse_selected_bead, NULL, 0, NULL);
  add_item_to_menu(mn, "plot fringes algo steps of bead", do_display_all_fringes_step_of_mouse_selected_bead, NULL, 0, NULL);
  add_item_to_menu(mn, "plot y-profiles algo steps of bead", do_display_all_y_profiles_step_of_mouse_selected_bead, NULL, 0, NULL);
  add_item_to_menu(mn, "erase this bead (works only if not linked to trackbead bead)", sdi_v2_remove_mouse_selected_bead, NULL, 0, NULL);
  add_item_to_menu(mn, "set z calib factors", do_set_calibration_factors_of_mouse_selected_bead, NULL, 0, NULL);
  add_item_to_menu(mn, "printf bead info", sdi_v2_printf_bead_info, NULL, 0, NULL);
  add_item_to_menu(mn, "duplicate this bead", do_sdi_v2_duplicate_one_bead, NULL, 0, NULL);

  return mn;
}
///////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////end of bead-specific gui//////////////////////////////////////////////
int do_add_oi_to_source(void)
{
  if(updating_menu_state !=0) return D_O_K;
  if (track_info->SDI_mode != 1) return D_O_K;;
  imreg *imr=NULL;
  O_i *oi=NULL;
  if (ac_grep(cur_ac_reg, "%im%oi", &imr, &oi) !=2) return D_O_K;
  oi->oi_idle_action = sdi_v2_oi_idle_action;
  oi->oi_post_display = sdi_v2_oi_idle_action;
  oi->oi_mouse_action = sdi_v2_oi_mouse_action;
  return 0;
}

int do_purge_fov(void)
{
  if (updating_menu_state !=0) return D_O_K;
  if (sdi_g_fov==NULL) return D_O_K;
  bool go_rt_track_tmp_bead = go_rt_track_bead;
  bool go_rt_track_tmp_xz = go_rt_track_xz;
  bool go_rt_track_tmp_y = go_rt_track_y;
  go_rt_track_bead = false;
  go_rt_track_xz = false;
  go_rt_track_y = false;
  sdi_v2_clean_fov_from_fringes_not_in_beads(sdi_g_fov);
  sdi_v2_clean_fov_from_y_profile_not_in_beads(sdi_g_fov);
  go_rt_track_xz = go_rt_track_tmp_xz;
  go_rt_track_y = go_rt_track_tmp_y;
  go_rt_track_bead = go_rt_track_tmp_bead;
  return 0;
}
int do_sdi_v2_remove_all_display_of_fov(void)
{
  if (updating_menu_state !=0) return D_O_K;
  if (sdi_g_fov == NULL) return D_O_K;
  bool go_rt_track_tmp_bead = go_rt_track_bead;
  bool go_rt_track_tmp_xz = go_rt_track_xz;
  bool go_rt_track_tmp_y = go_rt_track_y;
  go_rt_track_bead = false;
  go_rt_track_xz = false;
  go_rt_track_y = false;
  sdi_v2_remove_all_display_of_fov(sdi_g_fov);
  go_rt_track_xz = go_rt_track_tmp_xz;
  go_rt_track_y = go_rt_track_tmp_y;
  go_rt_track_bead = go_rt_track_tmp_bead;
  return D_O_K;
}

int do_sdi_v2_fringes_detection_gui(void)
{
	if (updating_menu_state!=0) return D_O_K;
	if (sdi_f_d == NULL) sdi_f_d = (sdi_v2_f_d *)calloc(1, sizeof(sdi_v2_f_d));
	sdi_v2_fringes_detection_gui(sdi_f_d);
	return 0;
}
int do_sdi_v2_y_bead_detection_gui(void)
{
  if (updating_menu_state!=0) return D_O_K;
  if (sdi_y_b_d== NULL) sdi_y_b_d = (sdi_v2_y_b_d *)calloc(1, sizeof(sdi_v2_y_b_d));
  int verbose=1;
  sdi_v2_y_bead_detection_gui(sdi_y_b_d, verbose, sdi_y_b_from_cfg);
	return 0;
}

int do_clean_fov(void)
{
  if (updating_menu_state!=0) return D_O_K;
  if (sdi_g_fov==NULL) return D_O_K;

  //remove the fringes and y profiles that are not part of bead
  sdi_v2_clean_fov_from_y_profile_not_in_beads(sdi_g_fov);
  sdi_v2_clean_fov_from_fringes_not_in_beads(sdi_g_fov);
  return 0;
}

int do_find_active_job_per_type(void)
{
  if (updating_menu_state!=0) return D_O_K;
  win_printf_OK("active job sdi %d (T=%d)", check_active_job_per_type(SDI_SIGNAL_ANALYSIS), (int)(1==1));
  return 0;
}

int do_set_common_calibration_factor(void)
{
  if (updating_menu_state!=0) return D_O_K;
  go_rt_track_xz = false;
  go_rt_track_y = false;
  go_rt_track_bead = false;
  if (sdi_g_fov!=NULL) sdi_set_common_calibration_factors(sdi_g_fov, 1, sdi_calib_from_cfg);
  return 0;
}

int do_sdi_v2_clean_fov_from_bad_beads(void)
{
  if (updating_menu_state!=0) return D_O_K;
  go_rt_track_xz = false;
  go_rt_track_y = false;
  go_rt_track_bead = false;
  if (sdi_g_fov!=NULL) sdi_v2_clean_fov_from_bad_beads(sdi_g_fov);
  return 0;
}

//to do : flag inside between
MENU *sdi_menu(void)
{
  static MENU mn[32];
  if (mn[0].text != NULL) return mn;
  add_item_to_menu(mn, "Purge FoV from useless beads", do_sdi_v2_clean_fov_from_bad_beads, NULL, 0, NULL);
  add_item_to_menu(mn, "Save bead positions", do_save_beads_to_cfg_file, NULL, 0, NULL);
  add_item_to_menu(mn, "Load bead from config file", do_load_beads_from_cfg_file, NULL, 0, NULL);
  add_item_to_menu(mn, "Bead QC parameters",do_set_sdi_v2_diagnose_gui,NULL,0,NULL);
  add_item_to_menu(mn, "Fringes detection parameters", do_sdi_v2_fringes_detection_gui, NULL,0,NULL);
  add_item_to_menu(mn, "Tracking parameters", do_set_sdi_v2_parameters_gui, NULL, 0, NULL);

  //add_item_to_menu(mn, "test hdf5", do_hdf5_init, NULL, 0, NULL);
  //add_item_to_menu(mn, "write", do_hdf5_write, NULL, 0, NULL);
  //add_item_to_menu(mn, "switch to sdi tracking", do_switch_tracking_algo, NULL, 0, NULL);
  add_item_to_menu(mn, "add this oi to source", do_add_oi_to_source, NULL, 0, NULL);
  //add_item_to_menu(mn, "track fringes", do_track_fringes_of_fov_from_movie, NULL, 0, NULL);
  //add_item_to_menu(mn, "track y profiles of fov", do_track_y_profiles, NULL, 0, NULL);
  //add_item_to_menu(mn, "track beads", do_track_fov_from_movie, NULL, 0, NULL);
  add_item_to_menu(mn, "fringes calib from recorded movie", do_calibrate_fov_from_movie, NULL, 0, NULL);
  //add_item_to_menu(mn, "clean fov from useless obj", do_clean_fov, NULL, 0, NULL);
  //add_item_to_menu(mn, "remove all sdi display", do_sdi_v2_remove_all_display_of_fov, NULL, 0, NULL);
  //add_item_to_menu(mn, "purge fov from useless sub objects", do_purge_fov, NULL, 0, NULL);
  //add_item_to_menu(mn, "swith what to draw", do_switch_what_to_draw, NULL, 0, NULL);
  //add_item_to_menu(mn, "record averaged ramp movie + process", do_sdi_v2_record_ramp_movie, NULL, 0, NULL);
  add_item_to_menu(mn, "grab a movie during motion + process", do_sdi_v2_grab_movie, NULL, 0, NULL);
  add_item_to_menu(mn,"treat result of grab movie",do_sdi_v2_grab_treatment,NULL,0,NULL);
  add_item_to_menu(mn, "record z(t) of beads + process", sdi_v2_job_on_xyz_bead_main, NULL, 0, NULL);
  add_item_to_menu(mn, "record y(t) of y prof + process", sdi_v2_job_y_y_profile_main, NULL, 0, NULL);
  add_item_to_menu(mn, "detect fringes : step 1", do_sdi_v2_detect_fringes, NULL, 0, NULL);
  add_item_to_menu(mn, "filter fringes : step 2", do_filter_bad_fringes, NULL, 0, NULL);
  add_item_to_menu(mn, "set bead detection by correlation parameters", do_sdi_v2_y_bead_detection_gui, NULL,0,NULL);
  //add_item_to_menu(mn, "examine spatial effects in fringes factors", do_spatial_effect, NULL,0, NULL);
  //add_item_to_menu(mn, "test find active job per type", do_find_active_job_per_type, NULL, 0, NULL);
  return mn;
}


//specific to sdi : this is planned to be called for all source oi, not only oi_TRACK
//useful to study a fov with long operations (more than cam frequency typically) on saved movies
int sdi_v2_oi_mouse_action(O_i *oi, int x0, int y0, int mode, DIALOG *d)
{
    imreg *imr = NULL;
    if (d->dp == NULL) return D_O_K;
    imr = (imreg*)d->dp;
    int count = 0;
    int x_m, y_m, pos_m, x_m_im, y_m_im;
    bool bead_found = false;
    bool fringes_found = false;
    bool y_profile_found = false;
    if (sdi_g_fov == NULL) return D_O_K;
    pos_m = mouse_pos;
    x_m = pos_m >> 16;
    y_m = pos_m & 0x0000ffff;
    x_m_im = x_imr_2_imdata(imr, x_m);
    y_m_im = y_imr_2_imdata(imr, y_m);

    //check whether the mouse targets one bead
    while(bead_found==false && count<sdi_g_fov->n_beads)
    {
      if( x_m_im > (sdi_g_fov->beads_list[count]->xc - sdi_g_fov->sdi_param->x_roi_size_2/2)
      && x_m_im < (sdi_g_fov->beads_list[count]->xc + sdi_g_fov->sdi_param->x_roi_size_2/2)
      && y_m_im > (sdi_g_fov->beads_list[count]->yc - sdi_g_fov->sdi_param->y_roi_size_2_Y)
      && y_m_im < (sdi_g_fov->beads_list[count]->yc + sdi_g_fov->sdi_param->y_roi_size_2_Y))
      {
        bead_found = true;
        mouse_selected_bead = count;
        sdi_g_fov->beads_list[count]->mouse_draged = true;
      }
      else count ++;
    }
    //if no bead found then target one fringes? (typiclly in the middle of the process of finding beads with automatic algo)
    if (bead_found==false)
    {
      count = 0;
      while(fringes_found==false && count<sdi_g_fov->n_fringes)
      {
        if( x_m_im > (sdi_g_fov->fringes_list[count]->xc - sdi_g_fov->sdi_param->x_roi_size_2/2)
        && x_m_im < (sdi_g_fov->fringes_list[count]->xc + sdi_g_fov->sdi_param->x_roi_size_2/2)
        && y_m_im > (sdi_g_fov->fringes_list[count]->yc - sdi_g_fov->sdi_param->y_roi_size_2)
        && y_m_im < (sdi_g_fov->fringes_list[count]->yc + sdi_g_fov->sdi_param->y_roi_size_2))
        {
          fringes_found = true;
          mouse_selected_bead = count;//mouse_selected_bead used for also fringes and y profile
        }
        else count ++;
      }
    }
    //if no bead found then target one fringes? (typiclly in the middle of the process of finding beads with automatic algo)
    if (bead_found==false && fringes_found == false)
    {
      count = 0;
      while(y_profile_found==false && count<sdi_g_fov->n_y_profile)
      {
        if( x_m_im > (sdi_g_fov->y_profile_list[count]->xc - sdi_g_fov->sdi_param->x_roi_size_2_Y)
        && x_m_im < (sdi_g_fov->y_profile_list[count]->xc + sdi_g_fov->sdi_param->x_roi_size_2_Y)
        && y_m_im > (sdi_g_fov->y_profile_list[count]->yc - sdi_g_fov->sdi_param->y_roi_size_2_Y)
        && y_m_im < (sdi_g_fov->y_profile_list[count]->yc + sdi_g_fov->sdi_param->y_roi_size_2_Y))
        {
          y_profile_found = true;
          mouse_selected_bead = count;//mouse_selected_bead used for also fringes and y profile
        }
        else count ++;
      }
    }
    if(mouse_b == 2)
    {
      if (bead_found == true) do_menu(sdi_one_bead_menu(), mouse_x, mouse_y);
    }

    //what to do in case of left click
    while(mouse_b == 1)
    {
      //takes the current new mouse position
      pos_m = mouse_pos;
      x_m = pos_m >> 16;
      y_m = pos_m & 0x0000ffff;
      x_m_im = x_imr_2_imdata(imr, x_m);
      y_m_im = y_imr_2_imdata(imr, y_m);
      //update the mouse_selected_bead position if there is one
      if (bead_found == true)
      {
        sdi_v2_update_bead_parameters(sdi_g_fov->beads_list[mouse_selected_bead], sdi_g_fov->sdi_param, x_m_im, y_m_im);
        sdi_v2_fringes_parameter_update(sdi_g_fov->beads_list[mouse_selected_bead]->fringes_bead[0], sdi_g_fov->sdi_param);
        sdi_v2_fringes_parameter_update(sdi_g_fov->beads_list[mouse_selected_bead]->fringes_bead[1], sdi_g_fov->sdi_param);
        sdi_v2_y_profile_update_parameter(sdi_g_fov->beads_list[mouse_selected_bead]->y_profile_bead[0], sdi_g_fov->sdi_param);
        sdi_v2_y_profile_update_parameter(sdi_g_fov->beads_list[mouse_selected_bead]->y_profile_bead[1], sdi_g_fov->sdi_param);
      }
      //display
      refresh_image(imr,UNCHANGED);
    }
    if (general_idle_action) general_idle_action(d);
    for (count = 0; count<sdi_g_fov->n_beads ;count++) sdi_g_fov->beads_list[count]->mouse_draged = 0;
    return 0;
}

//oi_idle_action attached to the video thread : just display frames
int sdi_v2_oi_idle_action(O_i *oi, DIALOG *d)
{
  imreg *imr = NULL;
  BITMAP *imb = NULL;
  if (d->dp == NULL)    return D_O_K;
  imr = (imreg*)d->dp;
  bool go_rt_track_tmp_xz, go_rt_track_tmp_y, go_rt_track_tmp_bead;
  //constantly find the closest bead
  int pos_m, x_m, y_m, x_m_im, y_m_im, count;
  bool bead_found = false;
  int nf;
  if (oi == oi_TRACK) nf = TRACKING_BUFFER_SIZE;
  else nf = oi->im.n_f;
  count = 0;

  if ((IS_DATA_IN_USE(oi) == 0) && (oi->need_to_refresh & BITMAP_NEED_REFRESH))
  {
    display_image_stuff_16M(imr,d);
    oi->need_to_refresh |= INTERNAL_BITMAP_NEED_REFRESH;
  }
  imb = (BITMAP*)oi->bmp.stuff;

  if (sdi_g_fov != NULL)
  {
    pos_m = mouse_pos;
    x_m = pos_m >> 16;
    y_m = pos_m & 0x0000ffff;
    x_m_im = x_imr_2_imdata(imr, x_m);
    y_m_im = y_imr_2_imdata(imr, y_m);
    //check whether the mouse targets one bead
    while(bead_found==false && count<sdi_g_fov->n_beads)
    {
      if( x_m_im > (sdi_g_fov->beads_list[count]->xc - sdi_g_fov->sdi_param->x_roi_size_2/2)
      && x_m_im < (sdi_g_fov->beads_list[count]->xc + sdi_g_fov->sdi_param->x_roi_size_2/2)
      && y_m_im > (sdi_g_fov->beads_list[count]->yc - sdi_g_fov->sdi_param->y_roi_size_2_Y)
      && y_m_im < (sdi_g_fov->beads_list[count]->yc + sdi_g_fov->sdi_param->y_roi_size_2_Y))
      {
        bead_found = true;
        mouse_selected_bead = count;
        sdi_g_fov->beads_list[count]->mouse_draged = true;
      }
      else count ++;
    }

    if(draw_fringes == true) sdi_v2_draw_fringes_of_current_fov(imb, oi, sdi_g_fov);
    if(draw_beads == true)
    {
      sdi_v2_draw_beads_of_current_fov(imb, oi, sdi_g_fov);
      sdi_v2_label_beads_of_fov(imb, oi, sdi_g_fov);
    }
  }
  if (g_info != NULL)
  {
    clear_bitmap(g_info->bmp);
    sdi_v2_draw_one_filled_rect_vga_screen_unit( 0, 0, g_info->w, g_info->h, g_info->color_bgd, g_info->bmp);//nice background in bitmap
    sdi_v2_add_text_vga_screen_unit(g_info->bmp, 100, 10, g_info->color_txt, "trk xz:%d, y:%d, bd:%d", (int)go_rt_track_xz, (int)go_rt_track_y, (int)go_rt_track_bead);
    sdi_v2_add_text_vga_screen_unit(g_info->bmp, 100, 30, g_info->color_txt, "dtxz=%2.0f dty=%2.0f(ms)", dt_computation_ms_xz, dt_computation_ms_y);
    if (sdi_g_fov == NULL) sdi_v2_add_text_vga_screen_unit(g_info->bmp, 100, 50, g_info->color_txt, "no fov");
    else sdi_v2_add_text_vga_screen_unit(g_info->bmp, 100, 50, g_info->color_txt, "%d beads", sdi_g_fov->n_beads);
    if (sdi_g_fov == NULL) sdi_v2_add_text_vga_screen_unit(g_info->bmp, 100, 70, g_info->color_txt, "no fov");
    else sdi_v2_add_text_vga_screen_unit(g_info->bmp, 100, 70, g_info->color_txt, "%d fringes", sdi_g_fov->n_fringes);
    if (sdi_g_fov == NULL) sdi_v2_add_text_vga_screen_unit(g_info->bmp, 100, 90, g_info->color_txt, "no fov");
    else sdi_v2_add_text_vga_screen_unit(g_info->bmp, 100, 90, g_info->color_txt, "%d y profiles", sdi_g_fov->n_y_profile);
    if (working_gen_record == NULL) sdi_v2_add_text_vga_screen_unit(g_info->bmp, 100, 110, g_info->color_txt, "no record");
    else sdi_v2_add_text_vga_screen_unit(g_info->bmp, 100, 110, g_info->color_txt, "i record %d", working_gen_record->abs_pos);
    sdi_v2_add_text_vga_screen_unit(g_info->bmp, 100, 130, g_info->color_txt, "%d trk beads", track_info->n_b);
    if (sdi_g_fov == NULL) sdi_v2_add_text_vga_screen_unit(g_info->bmp, 100, 150, g_info->color_txt, "no fov");
    else sdi_v2_add_text_vga_screen_unit(g_info->bmp, 100, 150, g_info->color_txt, "%d hp opens", sdi_g_fov->n_opening_hairpins);
    if (sdi_g_fov == NULL) sdi_v2_add_text_vga_screen_unit(g_info->bmp, 100, 170, g_info->color_txt, "no fov");
    else sdi_v2_add_text_vga_screen_unit(g_info->bmp, 100, 170, g_info->color_txt, "%d ref bd", sdi_g_fov->n_ref_beads);
  }
  if (g_info_help != NULL)
  {
    clear_bitmap(g_info_help->bmp);
    sdi_v2_draw_one_filled_rect_vga_screen_unit( 0, 0, g_info_help->w, g_info_help->h, g_info_help->color_bgd, g_info_help->bmp);//nice background in bitmap
    sdi_v2_add_text_vga_screen_unit(g_info_help->bmp, 100, 10, g_info_help->color_txt, "  LCtr +");
    sdi_v2_add_text_vga_screen_unit(g_info_help->bmp, 100, 35, g_info_help->color_txt, "   B : add sdi bead");
    sdi_v2_add_text_vga_screen_unit(g_info_help->bmp, 100, 60, g_info_help->color_txt, "   F : track fringes");
    sdi_v2_add_text_vga_screen_unit(g_info_help->bmp, 100, 95, g_info_help->color_txt, "   Y : track y");
    sdi_v2_add_text_vga_screen_unit(g_info_help->bmp, 100, 120, g_info_help->color_txt, "   V : track beads");
    sdi_v2_add_text_vga_screen_unit(g_info_help->bmp, 100, 145, g_info_help->color_txt, "   T : erase FOV");
    sdi_v2_add_text_vga_screen_unit(g_info_help->bmp, 100, 170, g_info_help->color_txt, "   X : fringes detection");
    sdi_v2_add_text_vga_screen_unit(g_info_help->bmp, 100, 195, g_info_help->color_txt, "   N : link to .trk");
    sdi_v2_add_text_vga_screen_unit(g_info_help->bmp, 100, 220, g_info_help->color_txt, "   H : bd classify");
    sdi_v2_add_text_vga_screen_unit(g_info_help->bmp, 100, 255, g_info_help->color_txt, "   M : bd origin");
    //sdi_v2_add_text_vga_screen_unit(g_info_help->bmp, 100, 260, g_info_help->color_txt, "   1 : add z(t)");
    //sdi_v2_add_text_vga_screen_unit(g_info_help->bmp, 100, 290, g_info_help->color_txt, "   2 : add x(t)");
    //sdi_v2_add_text_vga_screen_unit(g_info_help->bmp, 100, 320, g_info_help->color_txt, "   I : analyze FoV");
	  //sdi_v2_add_text_vga_screen_unit(g_info_help->bmp, 100, 350, g_info_help->color_txt, "   D : y to bead");
  }

  if (oi->need_to_refresh & INTERNAL_BITMAP_NEED_REFRESH)
  {
    SET_BITMAP_IN_USE(oi);
    acquire_bitmap(screen);
    blit(imb,screen,0,0,imr->x_off, imr->y_off - imb->h + d->y, imb->w, imb->h);
    blit(g_info->bmp, screen, 0, 0, d->w-g_info->x, d->h-g_info->y, g_info->w, g_info->h);
    blit(g_info_help->bmp, screen, 0, 0, d->w-g_info_help->x, d->h-g_info_help->y, g_info_help->w, g_info_help->h);
    release_bitmap(screen);
    oi->need_to_refresh &= ~INTERNAL_BITMAP_NEED_REFRESH;
    SET_BITMAP_NO_MORE_IN_USE(oi);
  }

  //keyboard events low priority (then in the idle thread)
  int i;
  int user_length = 200;
  float t0=0;
  if (track_info!=NULL) t0 = (float) (track_info->imi[track_info->c_i]);
  if (key[KEY_LCONTROL])
  {
    //add one sdi bead in fov, add one b_track bead in track_info, and link sdi bead to
    if (key[KEY_B])
    {
      go_rt_track_tmp_xz = go_rt_track_xz;
      go_rt_track_xz = false;
      go_rt_track_tmp_y = go_rt_track_y;
      go_rt_track_y = false;
      go_rt_track_tmp_bead = go_rt_track_bead;
      go_rt_track_bead = false;
      sdi_v2_add_one_bead_to_fov_with_mouse(sdi_g_fov);
      sdi_v2_allocate_sdi_v2_bead_output_t_fields( sdi_g_fov->beads_list[sdi_g_fov->n_beads-1], nf);//default size of output
      sdi_set_common_calibration_factors(sdi_g_fov, 0, sdi_calib_from_cfg); //puts calibration factors, not verbose
      for (i=0 ;i<2 ;i++) sdi_g_fov->beads_list[sdi_g_fov->n_beads-1]->fringes_bead[i]->need_background = true;//ask to ref point asap the tracking restart
      for (i=0 ;i<2 ;i++) sdi_g_fov->beads_list[sdi_g_fov->n_beads-1]->y_profile_bead[i]->need_background = true;//ask to ref point asap the tracking restart
      for (i=0 ;i<2 ;i++) sdi_g_fov->beads_list[sdi_g_fov->n_beads-1]->fringes_bead[i]->need_reference_point = true;//ask to ref point asap the tracking restart
      //go_rt_track_xz = go_rt_track_tmp_xz;
      //go_rt_track_y = go_rt_track_tmp_y;
      //go_rt_track_bead = go_rt_track_tmp_bead;
      clear_keybuf();
      psleep(user_length);
    }
    if (key[KEY_X])
    {
      go_rt_track_tmp_xz = go_rt_track_xz;
      go_rt_track_xz = false;
      go_rt_track_tmp_y = go_rt_track_y;
      go_rt_track_y = false;
      go_rt_track_tmp_bead = go_rt_track_bead;
      go_rt_track_bead = false;
      sdi_v2_detect_fringes_general();
      if(sdi_g_fov!=NULL) sdi_v2_filter_bad_fringes(sdi_g_fov, sdi_f_d);
      go_rt_track_xz = go_rt_track_tmp_xz;
      go_rt_track_y = go_rt_track_tmp_y;
      go_rt_track_bead = go_rt_track_tmp_bead;
      clear_keybuf();
      psleep(user_length);
    }
    //attach sdi_bead onto track beads when not yet linked to one...this is really awful
    if (key[KEY_N])
    {
      go_rt_track_tmp_xz = go_rt_track_xz;
      go_rt_track_xz = false;
      go_rt_track_tmp_y = go_rt_track_y;
      go_rt_track_y = false;
      go_rt_track_tmp_bead = go_rt_track_bead;
      go_rt_track_bead = false;
      sdi_v2_remove_all_display_of_fov(sdi_g_fov);
      for (i=0 ; i<sdi_g_fov->n_beads ;i++)
      {
        if (sdi_g_fov->beads_list[i]->fringes_bead[0]->hard_linked == false)//this detects the sdi beads already linked to trk bead
        {
          do_add_bead_in_x_y( sdi_g_fov->sdi_param->x_roi_size, sdi_g_fov->sdi_param->y_roi_size_Y + sdi_g_fov->sdi_param->dist_half_beads,
                  (float)(sdi_g_fov->beads_list[i]->xc), (float)(sdi_g_fov->beads_list[i]->yc));
          sdi_v2_attach_sdi_bead_to_b_track(sdi_g_fov->beads_list[i], track_info->bd[track_info->n_b - 1]);
        }
      }
      //go_rt_track_xz = go_rt_track_tmp_xz;
      //go_rt_track_y = go_rt_track_tmp_y;
      //go_rt_track_bead = go_rt_track_tmp_bead;
      clear_keybuf();
      psleep(user_length);
    }
    if (key[KEY_T])//erase the sdi fov (the part not linked to a track bead)
    {
      go_rt_track_xz = go_rt_track_y = go_rt_track_bead = false;
      if (win_scanf("Erase FoV (when not attached to trk file)")==WIN_CANCEL) return D_O_K ;
      if (sdi_g_fov!=NULL) sdi_v2_empty_fov(sdi_g_fov);
      clear_keybuf();
      psleep(user_length);
    }
    //switch on/off real-time tracking of y (y profile objects processing)
    if (key[KEY_Y])
    {
      if(go_rt_track_y == false)
      {
        //change in nf sdi_v2_buffer_config(sdi_g_fov, nf);//this detects if one change the buffer length of fov, if yes, reset and reallocate only the minimum
        for (i=0;i<sdi_g_fov->n_y_profile;i++) sdi_g_fov->y_profile_list[i]->need_background = true;
        sdi_g_fov->buffer_size = TRACKING_BUFFER_SIZE;
        go_rt_track_y = true;
      }
      else go_rt_track_y = false;
      clear_keybuf();
      psleep(user_length);
    }
    //switch on/off real-time tracking of xz (fringes objects processing)
    if (key[KEY_F])
    {
      if(go_rt_track_xz == false)
      {
        //change in nf sdi_v2_buffer_config(sdi_g_fov, nf);//this detects if one change the buffer length of fov, if yes, reset and reallocate only the minimum
        for (i=0;i<sdi_g_fov->n_fringes;i++)
        {
          sdi_g_fov->fringes_list[i]->need_background = true;//take background
          sdi_g_fov->fringes_list[i]->need_reference_point = true;//take ref points
        }
        sdi_g_fov->buffer_size = TRACKING_BUFFER_SIZE;
        go_rt_track_xz = true;
      }
      else go_rt_track_xz = false;
      clear_keybuf();
      psleep(user_length);
    }
    if (key[KEY_V])
    {
      //change in nf to do
      if(go_rt_track_bead == false) go_rt_track_bead = true;
      else go_rt_track_bead = false;
      clear_keybuf();
      psleep(user_length);
    }
    if (key[KEY_J])
    {
      go_rt_track_tmp_xz = go_rt_track_xz;
      go_rt_track_xz = false;
      go_rt_track_tmp_y = go_rt_track_y;
      go_rt_track_y = false;
      go_rt_track_tmp_bead = go_rt_track_bead;
      go_rt_track_bead = false;
      if (sdi_g_fov!=NULL) sdi_v2_add_current_point_as_ref_points_fov(sdi_g_fov);
      go_rt_track_xz = go_rt_track_tmp_xz;
      go_rt_track_y = go_rt_track_tmp_y;
      go_rt_track_bead = go_rt_track_tmp_bead;
      clear_keybuf();
      psleep(user_length);
    }
    if (key[KEY_R])
    {
      go_rt_track_tmp_xz = go_rt_track_xz;
      go_rt_track_xz = false;
      go_rt_track_tmp_y = go_rt_track_y;
      go_rt_track_y = false;
      go_rt_track_tmp_bead = go_rt_track_bead;
      go_rt_track_bead = false;
	  //link to vc bd to do
      if (working_gen_record == NULL) record_zdet();
      else stop_zdet();
      go_rt_track_xz = go_rt_track_tmp_xz;
      go_rt_track_y = go_rt_track_tmp_y;
      go_rt_track_bead = go_rt_track_tmp_bead;
      clear_keybuf();
      psleep(user_length);
    }
    if (key[KEY_1])
    {
      go_rt_track_tmp_xz = go_rt_track_xz;
      go_rt_track_xz = false;
      go_rt_track_tmp_y = go_rt_track_y;
      go_rt_track_y = false;
      go_rt_track_tmp_bead = go_rt_track_bead;
      go_rt_track_bead = false;
      sdi_v2_display_bead_z(sdi_g_fov, sdi_g_fov->beads_list[mouse_selected_bead], plot_group_1, NULL, nf, t0);
      if (plot_group_1 == NULL) plot_group_1 = sdi_g_fov->display_list[sdi_g_fov->n_display - 1]->plr ;
      go_rt_track_xz = go_rt_track_tmp_xz;
      go_rt_track_y = go_rt_track_tmp_y;
      go_rt_track_bead = go_rt_track_tmp_bead;
      clear_keybuf();
      psleep(user_length);//to not call that as long as the user press the key and the idle is executed.
    }
    if (key[KEY_2])
    {
      go_rt_track_tmp_xz = go_rt_track_xz;
      go_rt_track_xz = false;
      go_rt_track_tmp_y = go_rt_track_y;
      go_rt_track_y = false;
      go_rt_track_tmp_bead = go_rt_track_bead;
      go_rt_track_bead = false;
      sdi_v2_display_bead_x(sdi_g_fov, sdi_g_fov->beads_list[mouse_selected_bead], plot_group_2, NULL, nf, t0);
      if (plot_group_2 == NULL) plot_group_2 = sdi_g_fov->display_list[sdi_g_fov->n_display - 1]->plr ;
      go_rt_track_xz = go_rt_track_tmp_xz;
      go_rt_track_y = go_rt_track_tmp_y;
      go_rt_track_bead = go_rt_track_tmp_bead;
      clear_keybuf();
      psleep(user_length);//to not call that as long as the user press the key and the idle is executed.
    }
    if (key[KEY_M])//zeros
    {
      sdi_v2_job_xyz_bead_config(0, SDI_Z_FIND_0_OF_MOLECULE);
      clear_keybuf();
      psleep(user_length);//to not call that as long as the user press the key and the idle is executed.
    }
    if (key[KEY_H])//oclassify the beads
    {
      sdi_v2_job_xyz_bead_config(0, SDI_Z_CLASSIFY_BEAD_FUNCTION);
      clear_keybuf();
      psleep(user_length);//to not call that as long as the user press the key and the idle is executed.
    }
   if (key[KEY_D])
   {
	    sdi_v2_job_y_y_profile_config(0, SDI_FIND_BEADS_BY_CORRELATION);
      clear_keybuf();
      psleep(user_length);
    }
  }
  return 0;
}
/*
//zmag_to_force(0, dszm->yd[i]);
int sdi_v2_config_peripheral_display(int which_data, sdi_v2_disp *display, int size)
{
  float **field;
  float *x, *y;
  int i;
  //y axis config
  display->n_y = 1;
  display->c_y = 0;
  display->y = (float **)calloc(1, sizeof(float *));
  switch (which_data) {
    case SDI_ZMAG_MEASURED:
    display->y[0] = &(track_info->zmag[0]);
    break;
    case SDI_ZMAG_COMMAND:
    display->y[0] = &(track_info->zmag_cmd[0]);
    break;
    case SDI_ZOBJECTIVE_MEASURED:
    display->y[0] = &(track_info->obj_pos[0]);
    break;
  }
  //x axis config : only 1 axis, with fixed length, to be updated at one point during time
  display->n_x = 1;
  display->x = (float **)calloc(1, sizeof(float));
  display->x[0] = (float *)calloc(size, sizeof(float));
  display->c_x = 0;
  display->l_axis = (float *)calloc(1, sizeof(float));
  display->c_axis = 0;
  display->l_axis[0] = size;
  display->n_axis = 1;
  display->constant_x = false;//for this we will update the value of x axis
  for (i=0 ;i<size ;i++) display->x[0][i] = i; //temporaire ca va etre ecraser par les timestamp
  display->data = sdi_v2_init_empty_ds();
  sdi_v2_link_arrays_to_ds(display->data, display->x[0], display->y[0], (int) (display->l_axis[display->c_axis]));
  return 0;
}
*/
