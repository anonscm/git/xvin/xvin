#include "sdi_v2_basic.h"


//compute the bin size of an histogram with Scott formula, optimal for normal
//should be used on a copy of the signal
int sdi_v2_bin_size_from_Scott(const float *y_in, int n, float *mean, float *bin_size)
{
  static float *tmp = NULL;
  static int ntmp = 0;
  float sum_2;
  int i;
  if (y_in == NULL || mean == NULL || bin_size == NULL || n <= 0) return 1;
  if (tmp == NULL || n > ntmp)
    {
      tmp = (float *)realloc(tmp,n*sizeof(float));
      if (tmp == NULL) return 2;
      ntmp = n;
    }

  for (i=0;i<n;i++) tmp[i] = y_in[i];
  sdi_v2_estimate_mean(tmp, n, mean);
  sdi_v2_offset_then_scale(tmp, n, *mean, 1);
  sdi_v2_cumulative_square_sum(tmp, 0, n, &sum_2);
  sum_2 /= sqrt(n) ;
  *bin_size = (3.5 * sum_2) / pow(n, 0.333333); // Rule formula
  return 0;
}

//compute an histogram of an histo with Rule formula for bin size
//it computes bin size, then in each bin it computes mean, std of x and y
d_s *sdi_v2_compute_histo_of_tab(const float *y_in, int n_y_in)
{
  int k;
  int i;
  float bin_size = 0, y_min, y_max, mean = 0;
  int bin_n, bin_0, bin_1, x_i_min, x_i_max;
  d_s *ds_histo = NULL;
  static float *tmp = NULL;
  static int ntmp = 0;
  float avg=0;
  if (y_in == NULL) return NULL;
  sdi_v2_bin_size_from_Scott(y_in, n_y_in, &mean, &bin_size);
  if (tmp == NULL || n_y_in > ntmp)
    {
      tmp = (float *)realloc(tmp,n_y_in*sizeof(float));
      if (tmp == NULL) return NULL;
      ntmp = n_y_in;
    }
  if (bin_size != 0)
  {
    //min and max bin y value
    for (i=0;i<n_y_in;i++) tmp[i] = y_in[i];
    sdi_v2_array_min(tmp, 0, n_y_in, &y_min, &x_i_min);
    sdi_v2_offset_then_scale(tmp, n_y_in, y_min, 1);
    bin_0 = 0;
    sdi_v2_array_max(tmp, 0, n_y_in, &y_max, &x_i_max);
    bin_1 = (int)floor( y_max / bin_size) + 1;
    bin_n = bin_1 - bin_0 ;
    ds_histo = build_adjust_data_set(ds_histo, bin_n, bin_n);
    k=0;
    //fills the histogram ds_histo itself
    for (i=0 ; i< n_y_in ; i++)
    {
      k = (int)floor(tmp[i] / bin_size) ;
      if (k < 0 || k >= ds_histo->nx) continue;
      ds_histo->yd[k] += 1;
      ds_histo->xd[k] += y_in[i];
    }
    for ( i=0 ; i<bin_1 && i < ds_histo->nx; i++) ds_histo->xd[i] = (ds_histo->yd[i] > 0) ? ds_histo->xd[i] / ds_histo->yd[i] : i*bin_size;
  }
  else
  {
      ds_histo = build_adjust_data_set(ds_histo, 1, 1);
      sdi_v2_estimate_mean(y_in, n_y_in, &avg);
      ds_histo->xd[0] = avg;
      ds_histo->yd[0] = n_y_in;
  }
  QuickSort_double(ds_histo->xd, ds_histo->yd, 0, ds_histo->nx - 1);
  return ds_histo;
}
int sdi_v2_find_correct_2_pi_multiple(float *phi0_1, float phi1_1, float *phi0_0, float *phi1_0, float *k_in, int n_prev, float v_0, float v_1, float *k_out)
{
  double  tmp; //tmpf
  int i, ktmp;
  if (phi0_1 == NULL || phi0_0 == NULL || phi1_0 == NULL || k_in == NULL || k_out == NULL) return 1;
  {
    i = n_prev-1;
    tmp = ((double) *phi0_1 - (double) phi0_0[i] - (double) v_0 / (double) v_1 * (phi1_1 - phi1_0[i]) ) / 2 / M_PI;
    ktmp=(int) (round(tmp));
  }
  *k_out = (float)(ktmp) + k_in[i];
  *phi0_1 = *phi0_1 - 2 * (int)(*k_out) * M_PI ;
  return 0;
}
int sdi_v2_quick_contrast(float *sig, int n_points, int delta_x_i, float *contrast, float *I_max)
{
  float y_max, y_min;
  int x_i_max, x_i_min;
  if (I_max == NULL || contrast == NULL) return 1;
  sdi_v2_array_max(sig, 0, n_points, &y_max, &x_i_max);
  sdi_v2_array_min(sig, x_i_max - delta_x_i, x_i_max + delta_x_i, &y_min, &x_i_min);
  *contrast = (y_max- y_min) / (y_max + y_min);
  *I_max = y_max;
  return 0;
}
//unwrap the phase of a complex signal
int sdi_v2_unwrap(float *phase,int i_min,int i_max, float tol)
{
  int k;
  int i;
  double tmp;
  int tmp_k;
  double tol_double;
  static int *local_jump = NULL;
  static int n_local_jump = 0;
  if (phase == NULL) return 1;
  k=0;
  tmp_k=0;
  tol_double=(double) tol;

  if ((local_jump == NULL) || ((i_max-i_min) > n_local_jump))
  {
    local_jump = (int *)realloc(local_jump,(i_max-i_min)*sizeof(int));
    if (local_jump == NULL) return 2;
    n_local_jump = (i_max-i_min);
  }
  for (i=i_min ; i<i_max-1 ; i++)
  {
    tmp= (double) *(phase+i+1)-(double) *(phase+i);
    tmp=tmp/tol_double;
    tmp=tmp/M_PI;
    tmp_k =(int) tmp;
    *(local_jump+i-i_min)=tmp_k;
  }
  for (i=i_min+1;i<i_max;i++)
  {
    k+=local_jump[i-i_min-1];
    *(phase+i)=(float) ( (double) *(phase+i) + (double) (- (double) k * (double) 2 * M_PI) );
  }
  return k;
}
int sdi_v2_ds_max_interpolation_polynom_3( d_s *ds, float *ds_y_max, float *ds_x_max)
{
    double a, b, c, d, tmp;
    double *coeff=NULL;
    if (ds == NULL || ds_y_max == NULL || ds_x_max == NULL) return 1;
    fit_ds_to_xn_polynome(ds, 4, -FLT_MAX, FLT_MAX, -FLT_MAX, FLT_MAX, &coeff);
    a = coeff[3];
    b = coeff[2];
    c = coeff[1];
    d = coeff[0];
    tmp = (a != 0) ? (-b - sqrt(b * b - 3 * a * c)) / (3 * a) : -c/(2*b);//zeros of the derivative to check zero in denominator
    *ds_x_max = (float) tmp;
    *ds_y_max = (float)((a*tmp*tmp*tmp)+(b*tmp*tmp)+(c*tmp)+d);
    free(coeff);
    coeff=NULL;
    return 0;
}
int sdi_v2_ds_max_interpolation_polynom_2( d_s *ds, float *ds_y_max, float *ds_x_max)
{
    double a, b, c, tmp;
    double *coeff=NULL;
    if (ds == NULL || ds_y_max == NULL || ds_x_max == NULL) return 1;
    fit_ds_to_xn_polynome(ds, 3, -FLT_MAX, FLT_MAX, -FLT_MAX, FLT_MAX, &coeff);
    a = coeff[2];
    b = coeff[1];
    c = coeff[0];
    tmp = (a != 0) ? (-b)  / (2 * a) : 0;//zeros of the derivative
    *ds_x_max = (float) tmp;
    *ds_y_max = (float)((a*tmp*tmp)+(b*tmp)+c);
    free(coeff);
    coeff=NULL;
    return 0;
}
int sdi_v2_raw_spectral_phase_fftbt( float *tf_complex, float *phase, int i_min, int i_max)
{
  int i;
  float re;
  float im;
  if (tf_complex == NULL || phase == NULL) return 1;
  for (i=i_min ; i<i_max ;  i++)
  {
    re = tf_complex[2 * i];
    im = tf_complex[2 * i + 1];
    phase[i-i_min] = (float) atan2(im , re);
  }
  return 0;
}
//compute the autoconvolution of a signal (in the future, normalization could help to diagnose SNR & then quality signal)
int sdi_v2_array_fft_then_autoconv_then_ifft_then_ifftshift(float *profile, fft_plan *fp, float *auto_conv)
{
  int i;
  float tmp = 0;
  if (profile == NULL || fp == NULL || auto_conv == NULL) return 1;
  tmp=0;
  for (i=0 ; i<fp->nx ; i++) auto_conv[i] = profile[i];
  realtr1bt(fp, auto_conv);
  fftbt(fp, auto_conv, 1);
  realtr2bt(fp, auto_conv, 1);
  for (i=0 ; i<fp->nx ; i+=2)
  {
    tmp = auto_conv[i] * auto_conv[i] - auto_conv[i+1] * auto_conv[i+1];
    auto_conv[i+1] = 2 * auto_conv[i] * auto_conv[i+1];
    auto_conv[i] = tmp;
  }
  realtr2bt(fp, auto_conv, -1);
  fftbt(fp, auto_conv, -1);
  realtr1bt(fp, auto_conv);
  sdi_v2_fftshift(auto_conv, fp->n_2);
  return 0;
}

//compute the spectrum, amplitude and spectral phase of a signal
int sdi_v2_array_fftshift_then_fft(float *profile, fft_plan *fp, float *amplitude, float *phase, float *spectrum)
{
  static float *tf = NULL;
  static int n_tf = 0;
  double tmp;
  int i;
  if (profile == NULL || fp == NULL || amplitude == NULL || phase == NULL || spectrum == NULL) return 1;
  sdi_v2_fftshift(profile, fp->n_2);
  if (tf == NULL)
  {
    tf = (float *)calloc(fp->nx, sizeof(float));
    if (tf == NULL) return 2;
    n_tf = fp->nx;
  }
  if (n_tf < fp->nx)
  {
    tf = (float *)realloc(tf,fp->nx*sizeof(float));
    if (tf == NULL) return 2;
    n_tf = fp->nx;
  }
  for (i=0 ; i<fp->nx ; i++) tf[i] = profile[i];
  realtr1bt(fp, tf);
  fftbt(fp, tf, 1);
  realtr2bt(fp, tf, 1);
  for (i=0; i<fp->n_2; i++)
  {
    tmp = (double) ( tf[2*i+1] * tf[2*i+1] + tf[2*i] * tf[2*i] );
    spectrum[i] = (float) (tmp);
    amplitude[i]= (float) (sqrt(tmp));
  }
  sdi_v2_raw_spectral_phase_fftbt( tf, phase, 0, fp->n_2);
  return 0;
}
int sdi_v2_generate_hypergaussian_array(float *array, int n_points, float fwhm_min, float fwhm_max, int order)
{
  int i;
  float center, fwhm;
  if (array == NULL) return 1;
  fwhm=fwhm_max-fwhm_min;
  center = (fwhm_max+fwhm_min) / 2;
  for (i=0; i<n_points ; i++) array[i]=exp(-pow((i-center)/fwhm, 2 * order) * 0.5);
  return 0;
}
int sdi_v2_extract_y_profile_array_from_image( O_i *oi, float *profile, int x_i_min, int x_i_max, int y_i_min, int y_i_max)
{
  int i, j, x_size, y_size, tmpx, tmpy;
  if (oi == NULL || profile == NULL) return 1;
  x_size=( (x_i_max-x_i_min) > 0) ? (x_i_max-x_i_min) : 1;
  y_size=( (y_i_max-y_i_min) > 0) ? (y_i_max-y_i_min) : 1;
  for (i=0 ; i<y_size ; i++) profile[i] = (float) 0;
  switch (oi->im.data_type) {
    case IS_CHAR_IMAGE:
    for (i=0 ; i<y_size ;i++)
    {
      tmpy = i + y_i_min;
      tmpy = (tmpy < 0) ? 0 : tmpy;
      tmpy = (tmpy >= oi->im.ny) ? oi->im.ny - 1: tmpy;
      for (j=0 ; j<x_size ; j++)
      {
        tmpx = j + x_i_min;
        tmpx = (tmpx < 0) ? 0 : tmpx;
        tmpx = (tmpx >= oi->im.nx) ? oi->im.nx - 1 : tmpx;
        profile[i]+= (float) (oi->im.pixel[tmpy].ch[tmpx]);
      }
    }
    break;
    case IS_INT_IMAGE:
      for (i=0 ; i<y_size ;i++)
      {
        tmpy = i + y_i_min;
        tmpy = (tmpy < 0) ? 0 : tmpy;
        tmpy = (tmpy >= oi->im.ny) ? oi->im.ny - 1: tmpy;
        for (j=0 ; j<x_size ; j++)
        {
          tmpx = j + x_i_min;
          tmpx = (tmpx < 0) ? 0 : tmpx;
          tmpx = (tmpx >= oi->im.nx) ? oi->im.nx - 1 : tmpx;
          profile[i]+= (float) (oi->im.pixel[tmpy].in[tmpx]);
        }
      }
      break;
      case IS_FLOAT_IMAGE:
      for (i=0 ; i<y_size ;i++)
      {
        tmpy = i + y_i_min;
        tmpy = (tmpy < 0) ? 0 : tmpy;
        tmpy = (tmpy >= oi->im.ny) ? oi->im.ny - 1 : tmpy;
        for (j=0 ; j<x_size ; j++)
        {
          tmpx = j + x_i_min;
          tmpx = (tmpx < 0) ? 0 : tmpx;
          tmpx = (tmpx >= oi->im.nx) ? oi->im.nx - 1 : tmpx;
          profile[i]+= (float) (oi->im.pixel[tmpy].fl[tmpx]);
        }
      }
      break;
      default:
      win_printf_OK("handles only char & float image");
      break;
    }
    for (i=0 ; i<y_size ; i++) if (i>=0) profile[i] = profile[i] / x_size;
  return 0;
}
int sdi_v2_extract_x_profile_array_from_image( O_i *oi, float *profile, int x_i_min, int x_i_max, int y_i_min, int y_i_max)
{
  int i, j, x_size, y_size, tmpx, tmpy;
  if (oi == NULL || profile == NULL) return 1;
  x_size=( (x_i_max-x_i_min) > 0) ? (x_i_max-x_i_min) : 1;
  y_size=( (y_i_max-y_i_min) > 0) ? (y_i_max-y_i_min) : 1;
  for (i=0 ; i<x_size ; i++) profile[i] = (float) 0;
  switch (oi->im.data_type)
  {
    case IS_CHAR_IMAGE:
    for (i=0 ; i<y_size ;i++)
    {
      tmpy = i + y_i_min;
      tmpy = (tmpy < 0) ? 0 : tmpy;
      tmpy = (tmpy >= oi->im.ny) ? oi->im.ny - 1: tmpy;
      for (j=0 ; j<x_size ; j++)
      {
        tmpx = j + x_i_min;
        tmpx = (tmpx < 0) ? 0 : tmpx;
        tmpx = (tmpx >= oi->im.nx) ? oi->im.nx - 1: tmpx;
        profile[j]+= (float) (oi->im.pixel[tmpy].ch[tmpx]);
      }
    }
    break;
    case IS_INT_IMAGE:
    for (i=0 ; i<y_size ;i++)
    {
      tmpy = i + y_i_min;
      tmpy = (tmpy < 0) ? 0 : tmpy;
      tmpy = (tmpy >= oi->im.ny) ? oi->im.ny - 1: tmpy;
      for (j=0 ; j<x_size ; j++)
      {
        tmpx = j + x_i_min;
        tmpx = (tmpx < 0) ? 0 : tmpx;
        tmpx = (tmpx >= oi->im.nx) ? oi->im.nx - 1: tmpx;
        profile[j]+= (float) (oi->im.pixel[tmpy].in[tmpx]);
      }
    }
    break;
    case IS_FLOAT_IMAGE:
    for (i=0 ; i<y_size ;i++)
    {
      tmpy = i + y_i_min;
      tmpy = (tmpy < 0) ? 0 : tmpy;
      tmpy = (tmpy >= oi->im.ny) ? oi->im.ny - 1: tmpy;
      for (j=0 ; j<x_size ; j++)
      {
        tmpx = j + x_i_min;
        tmpx = (tmpx < 0) ? 0 : tmpx;
        tmpx = (tmpx >= oi->im.nx) ? oi->im.nx -1: tmpx;
        profile[j]+= (float) (oi->im.pixel[tmpy].fl[tmpx]);
      }
    }
    break;
    default:
    win_printf_OK("handles only char & float image");
    return 1;
    break;
  }
  for (i=0 ; i<x_size ; i++) profile[i] = profile[i] / y_size;
  return 0;
}

//computes the 1st momentum of a distribution over a roi
//defined by x_i_frac_closest_min, x_i_frac_closest_max and dx_i_factor
int sdi_v2_measure_frequency(float *x, float *y, int n_points, float *nu0, int x_i_frac_closest_min, int x_i_frac_closest_max, int dx_i_factor)
{
  int x_i_inf, x_i_sup;
  if (x == NULL || y == NULL) return 1;
  x_i_inf = (int) (x_i_frac_closest_min - (x_i_frac_closest_max-x_i_frac_closest_min) * dx_i_factor);
  x_i_sup = (int) (x_i_frac_closest_max + (x_i_frac_closest_max-x_i_frac_closest_min) * dx_i_factor);
  sdi_v2_normalize_by_sum( y, x_i_inf, x_i_sup, n_points);
  sdi_v2_array_mu_1( x, y, x_i_inf, x_i_sup, nu0);
  return 0;
}
int sdi_v2_estimate_mean(float *tab, int n, float *mean)
{
  int i;
  double mean_tmp;
  if (tab == NULL || mean == NULL) return 1;
  mean_tmp=0;
  for (i=0;i<n;i++) mean_tmp+=*(tab+i);
  mean_tmp = mean_tmp / n;
  *mean = (float) mean_tmp;
  return 0;
}
int sdi_v2_estimate_std(float *tab, int n, float *std)
{
  int i;
  float mean;
  double var_tmp;
  if (tab == NULL || std == NULL) return 1;
  var_tmp=0;
  mean=0;
  sdi_v2_estimate_mean(tab,n,&mean);
  for (i=0;i<n;i++) var_tmp += (*(tab+i) - mean) * (*(tab+i) - mean);
  var_tmp = var_tmp / (n-1) ;
  *std = (float) sqrt(var_tmp);
  return 0;
}

//computes the first momentum of a previously normalized distribution
//be aware that the previous normalization must have been on the same roi
int sdi_v2_array_mu_1(const float *x, const float *y, int x_roi_min, int x_roi_max, float *mu1)
{
  int i;
  if (x == NULL || y == NULL || mu1 == NULL) return 1;
  *mu1 = 0;
  for (i = x_roi_min ; i < x_roi_max ; i++)  *mu1 += (float) ((double) (x[i]) * (double) (y[i]));
  return 0;
}

//correlation product of 2 arrays (possibly on roi)
//it substract the means of the signals, then normalize the signals by their norm2 then prod scalar (thus on normalized function space)
int sdi_v2_correlation_product(const float *x1, const float *x2, int x_roi_min, int x_roi_max, float *corr_product)
{
  int i;
  float avg1=0;
  float avg2=0;
  int n_points = x_roi_max - x_roi_min;
  static float *x1_copy = NULL, *x2_copy = NULL;
  static int n_cx = 0;
  if (x1 == NULL || x2 == NULL) return 1;
  if (x1_copy == NULL || x2_copy == NULL || n_cx < n_points)
  {
    x1_copy = (float *)realloc(x1_copy,n_points*sizeof(float));
    x2_copy = (float *)realloc(x2_copy,n_points* sizeof(float));
    if (x1_copy == NULL || x2_copy == NULL) return 2;
    n_cx = n_points;
  }
  for (i=0 ;i<n_points ;i++)
  {
    x1_copy[i] = x1[i+x_roi_min];
    x2_copy[i] = x2[i+x_roi_min];
  }
  sdi_v2_estimate_mean(x1_copy, n_points, &avg1);
  sdi_v2_offset_then_scale(x1_copy, n_points, avg1, 1);
  sdi_v2_normalize_by_norm2(x1_copy, 0, n_points, n_points);
  sdi_v2_estimate_mean(x2_copy, n_points, &avg2);
  sdi_v2_offset_then_scale(x2_copy, n_points, avg2, 1);
  sdi_v2_normalize_by_norm2(x2_copy, 0, n_points, n_points);
  sdi_v2_normalized_scal_prod(x1_copy, x2_copy, 0, n_points, corr_product);
  return 0;
}
//scalar product of 2 arrays, previously normalized ON THE SAME ROI
//careful there is no check of x values
int sdi_v2_normalized_scal_prod(const float *x1, const float *x2, int x_i_min, int x_i_max, float *prod)
{
  int i;
  double prod_tmp=0;
  if (x1 == NULL || x2 == NULL) return 1;
  for (i=x_i_min ; i < x_i_max ;i++) prod_tmp = prod_tmp + (double)(x1[i]) * (double)(x2[i]);
  *prod = (float)(prod_tmp);
  return 0;
}
//normalizes a distribution by its cumulative sum, be aware that the result is significant only over the roi
int sdi_v2_normalize_by_sum(float *array, int x_roi_min, int x_roi_max, int n_points)
{
  int i;
  float sum=0;
  double sum_double=0;
  if (array == NULL) return 1;
  sdi_v2_cumulative_sum(array, x_roi_min, x_roi_max, &sum);
  sum_double=(double) sum;
  for (i=0 ; i<n_points ; i++) array[i] = (float) ((double) (array[i]) / sum_double);
  return 0;
}
//normalizes an array by its norm2 over a roi (mean something only over the roi)
int sdi_v2_normalize_by_norm2(float *y, int x_roi_min, int x_roi_max, int n_points)
{
  int i;
  float sum=0;
  double sum_double=0;
  if (y == NULL) return 1;
  sdi_v2_cumulative_square_sum( y, x_roi_min, x_roi_max, &sum);
  sum_double=(double) sum;
  if (sum_double!=0) for (i=0 ; i<n_points ; i++) y[i] = (float) ((double) (y[i]) / sum_double);
  return 0;
}
//computes the cumulative sum of the values of an array over a roi
int sdi_v2_cumulative_sum(const float *tab, int x_roi_min, int x_roi_max, float *sum)
{
  int i;
  if (tab == NULL || sum  == NULL) return 1;
  *sum=0;
  for (i=x_roi_min ; i<x_roi_max ; i++) *sum = (float) ((double)(*sum) + (double)(*(tab+i)));
  return 0;
}
//computes the cumulative sum of the square of the values of an array
int sdi_v2_cumulative_square_sum(const float *tab, int x_roi_min, int x_roi_max, float *sum)
{
  int i;
  double sum_tmp=0;
  if (tab == NULL || sum  == NULL) return 1;
  for (i=x_roi_min ; i<x_roi_max ;i++) sum_tmp = sum_tmp + (double)(*(tab+i)) * (double)(*(tab+i));
  *sum = (float) sqrt(sum_tmp);
  return 0;
}
//basic analysis of a peak of a signal
//over a roi defined by x_roi_min/max, search for position and width of a peak defined by a fractional of (max-min) of an array
//also returns the last index before crossing the line and  the closest index of the fractional of dynamic
int sdi_v2_peak_width(const float *x, const float *y, int x_roi_min, int x_roi_max, float frac,
                float *x_frac_min, int *x_i_frac_min,int *x_i_frac_closest_min, float *x_frac_max,int *x_i_frac_max,int *x_i_frac_closest_max,float *width)
{
  int x_i_max, x_i_min;
  float y_max, y_min, threshold;
  float tmp1, tmp2;

//surout pas malheureux!
  //if (x == NULL || y == NULL || x_frac_min == NULL || x_i_frac_min == NULL || x_i_frac_closest_min == NULL || x_frac_max == NULL
  //    || x_i_frac_max == NULL || *x_i_frac_closest_max == NULL || width == NULL) return 1;

  sdi_v2_array_max( y, x_roi_min, x_roi_max, &y_max, &x_i_max);
  sdi_v2_array_min( y, x_roi_min, x_roi_max, &y_min, &x_i_min);
  *x_i_frac_min = x_i_max;
  *x_i_frac_max = x_i_max;
  threshold = (y_max - y_min) * frac + y_min;
  //go to threshold before the max
  while (y[*x_i_frac_min-1] > threshold) *x_i_frac_min=*x_i_frac_min-1;
  //linear interpolation
  tmp1 = y[*x_i_frac_min] -  y[*x_i_frac_min-1];
  tmp2 = x[*x_i_frac_min] - x[*x_i_frac_min-1];
  *x_frac_min = x[*x_i_frac_min-1] + (threshold-y[*x_i_frac_min-1]) * tmp2 / tmp1;
  //return nearest index
  *x_i_frac_closest_min= *x_i_frac_min;
  if ( (y[*x_i_frac_min]-threshold) < (threshold-y[*x_i_frac_min-1]) ) (*x_i_frac_closest_min)--;
  //the same on the rigth part
  while (y[*x_i_frac_max+1] > threshold) *x_i_frac_max=*x_i_frac_max+1;
  //linear interpolation
  tmp1 = y[*x_i_frac_max+1] - y[*x_i_frac_max];
  tmp2 = x[*x_i_frac_max+1]- x[*x_i_frac_max];
  *x_frac_max = x[*x_i_frac_max] + (threshold-y[*x_i_frac_max]) * tmp2 / tmp1;
  //nearest index
  *x_i_frac_closest_max = *x_i_frac_max;
  if ( (y[*x_i_frac_max]-threshold) > (threshold-y[*x_i_frac_max+1])) (*x_i_frac_closest_max)++;
  *width=*x_frac_max-*x_frac_min;
  return 0;
}
//puts an offset and then a scale factor to an array
int sdi_v2_offset_then_scale(float *tab,int tab_size,float offset,float scale)
{
  int i;
  double scale_double;
  double offset_double;
  offset_double=(double) offset;
  scale_double=(double) scale;
  for (i=0 ; i<tab_size ; i++) tab[i]=(float) (((double)tab[i] - offset_double) * scale_double);
  return 0;
 }
 //looks for the maximumm of an array over a roi
 int sdi_v2_array_max(const float *array, int roi_min, int roi_max, float *y_max, int *x_max)
 {
     int i;
     int n_points;
     if (array == NULL || y_max == NULL  || x_max == NULL ) return 1;
     n_points = roi_max - roi_min;
     *x_max = roi_min;
     *y_max = array[roi_min];
     for (i = 1+roi_min ; i < n_points ; i++)
     {
         if (array[i] > *y_max)
         {
           *y_max = array[i];
           *x_max = i;
         }
     }
     return 0;
 }

 //looks for the min of an array over a roi
 int sdi_v2_array_min(const float *array, int roi_min, int roi_max, float *y_min, int *x_min)
 {
     int i, n_points;
     if (array == NULL || y_min == NULL  || x_min == NULL ) return 1;
     n_points = roi_max - roi_min;
     *x_min = roi_min;
     *y_min = array[roi_min];
     for (i = 1+roi_min ; i < roi_max ; i++)
     {
         if (array[i] < *y_min)
         {
           *y_min = array[i];
           *x_min = i;
         }
     }
     return 0;
 }

 int sdi_v2_fftshift(float *tab, int size_2)
 {
   int i;
   float tmp;

   if (tab == NULL) return 1;
   for (i=0 ; i<size_2 ; i++)
   {
     tmp = *(tab+i);
     *(tab+i) = *(tab+size_2+i);
     *(tab+size_2+i) = tmp;
   }
   return 0;
 }


 int sdi_v2_array_log(float *data, int n_points)
 {
   int i;
   if (data == NULL) return 1;
   for (i=0 ; i<n_points ; i++) if (data[i] != 0)  data[i] = log(data[i]);
   return 0;
 }


 //links 2 1d arrays as xd & yd of a ds
 //with optionnallyy linking only one to update
int sdi_v2_link_arrays_to_ds(d_s *ds, float *x, float *y, int n_points)
{
  if (ds == NULL || x  == NULL || y == NULL) return 1;
  ds->xd = x;
  ds->yd = y;
  ds->mx = ds->nx = n_points;
  ds->my = ds->ny = n_points;
  return 0;
}
 //build a ds from 2 array, to use some ds function easily
 //ds allocated ()
 d_s *sdi_v2_init_empty_ds(void)
 {
      d_s *ds=NULL;
     int i;

     ds = (d_s *)calloc(1, sizeof(d_s));
     if (ds == NULL) return (d_s *) xvin_ptr_error(Out_Of_Memory);

     ds->mx = 0;
     ds->my = 0;
     ds->m = 1;
     ds->xe = ds->ye = NULL;
     ds->xed = ds->yed = NULL;
     ds->xbu = ds->ybu = NULL;
     ds->xbd = ds->ybd = NULL;
     //ds->color = DS_COLOR;
     ds->color = 1;
     ds->n_special = ds->m_special = 0;
     ds->special = NULL;
     ds->use.to = 0;
     ds->use.stuff = NULL;
     ds->symb = NULL;
     ds->source = NULL;
     ds->history = NULL;
     ds->treatement = NULL;
     ds->special = NULL;
     ds->lab = (p_l **)calloc(MAX_DATA, sizeof(p_l *));
     ds->boxplot_width = 1;
     if (ds->lab == NULL)
     {
         free_data_set(ds);
         return (d_s *) xvin_ptr_error(Out_Of_Memory);
     }
     ds->m_lab = MAX_DATA;
     set_ds_starting_time(ds);
     // following line added 2007/10/16, N.G.:
     ds->nx = 0;
     ds->ny = 0;

     for (i = 0 ; i < 8 ; i++)
     {
         ds->src_parameter_type[i] = NULL;
         ds->src_parameter[i] = 0;
     }
     ds->invisible = 0;
     return ds;
 }
