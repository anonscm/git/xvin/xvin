# include "sdi_hdf5_record.h"

//phylosophy :
//each object (bead/fringes/y_profile) is a group, which contains datasets with inidivual components (z...)
//each of these object are linked to some common others groups : magnets position, temperature...
int test_hdf5_model(void)
{
    hid_t fid, gid, did, did2, spid, pid; //les identités : file, groupd=, dataset, dataspace
    int data_buffer_size = 512, data_page_size = 4096, data_saving_period = 256;
    int video_buffer_size = 128, video_page_size = 256, video_saving_period = 64;
    int oi_nx = 2048, oi_ny = 2048;
    int n_bead, nx = 200, ny = 60;
    imreg *imr = NULL;
    O_i *oi = NULL;
    ac_grep(cur_ac_reg,"%im",&imr);
    oi =create_and_attach_movie_to_imr(imr, oi_nx, oi_ny, IS_CHAR_IMAGE, video_buffer_size);
    int t, i, j;
    for (t = 0 ; t<video_buffer_size ; t++)
    {
      switch_frame(oi, t);
      for (i = 0; i <oi_ny ; i ++) for (j = 0; j<oi_nx ; j++) oi->im.pixel[i].ch[j] = (unsigned char) (j+i*10+t*100) ;
    }
    refresh_image(imr, UNCHANGED);
    win_printf_OK("alloc movie done");
    //record initialization
    hdf5_sdi_r *g_record=NULL;
    g_record = (hdf5_sdi_r *)calloc(1, sizeof(hdf5_sdi_r));
    init_hdf5_sdi_record(g_record, "C:/Users/thibault/Desktop/data_exp/raw_data/sdi_trk_1.h5");
    //the one to save 1d float
    int mem_data_dim_1d[1] = {data_buffer_size};
    int file_data_dim_1d[1] = {data_page_size};
    add_one_mem_map_to_hdf5_sdi_record( g_record, 1, mem_data_dim_1d, 1, file_data_dim_1d, data_page_size, 0);
    //the list to save frames
    int mem_data_dim_movie[3] = {video_buffer_size * oi_nx * oi_ny};
    int file_data_dim_movie[3] = {video_page_size, ny, nx};
    //for(i=0;i<video_saving_period;i++)  add_one_mem_map_to_hdf5_sdi_record( g_record, 1, mem_data_dim_movie, 3, file_data_dim_movie, video_page_size, 0);

    //simulation of one bead
    sdi_bead *bead = NULL;
    sdi_fringes *f1 = NULL;
    sdi_fringes *f2 = NULL;
    sdi_y_profile *y1 = NULL;
    sdi_y_profile *y2 = NULL;
    f1 = (sdi_fringes *)calloc(1, sizeof(sdi_fringes));
    f2 = (sdi_fringes *)calloc(1, sizeof(sdi_fringes));
    y1 = (sdi_y_profile *)calloc(1, sizeof(sdi_y_profile));
    y2 = (sdi_y_profile *)calloc(1, sizeof(sdi_y_profile));
    bead = (sdi_bead *)calloc(1, sizeof(sdi_bead));
    init_one_sdi_fringes_by_position(f1, 100, 60);
    init_one_sdi_fringes_by_position(f2, 100, 40);
    init_one_sdi_y_profile_by_position(y1, 100, 40);
    init_one_sdi_y_profile_by_position(y2, 100, 60);
    init_one_sdi_bead_by_half(bead, f1, f2, y1, y2);
    int which_data[3] = {KEEP_Z_T, KEEP_Y_T, KEEP_X_T};
    allocate_sdi_bead_t_fields(bead, data_buffer_size, which_data, 3);
    int which_data_f[5] = {KEEP_NU0_T, KEEP_ORDER0_T, KEEP_ORDER1_T, KEEP_CONTRAST_T, KEEP_I_MAX_T};
    allocate_sdi_fringes_t_fields( f1, data_buffer_size, which_data_f, 5);
    allocate_sdi_fringes_t_fields( f2, data_buffer_size, which_data_f, 5);
    int which_data_y[1] ={KEEP_Y_PROFILE_T};
    allocate_sdi_y_profile_t_fields( y1, data_buffer_size, which_data_y, 1);
    allocate_sdi_y_profile_t_fields( y2, data_buffer_size, which_data_y, 1);

    float *time_exp;
    float *temp_exp;
    float *zmag_exp;
    unsigned long *index_exp;
    index_exp = (unsigned long *)calloc(data_buffer_size, sizeof(unsigned long));
    time_exp = (float *)calloc(data_buffer_size, sizeof(float));
    temp_exp = (float *)calloc(data_buffer_size, sizeof(float));
    zmag_exp = (float *)calloc(data_buffer_size, sizeof(float));
    for (t=0;t<data_buffer_size;t++)
    {
      bead->z_t->xd[t] = t;
      bead->z_t->yd[t] = t +1;
      bead->x_t->xd[t] = t;
      bead->x_t->yd[t] = t +2 ;
      bead->y_t->xd[t] = t;
      bead->y_t->yd[t] = t + 3;
      f1->contrast_t->xd[t] = t;
      f1->contrast_t->yd[t] = 0.8;
      f2->contrast_t->xd[t] = t;
      f2->contrast_t->yd[t] = 0.8;
      f1->I_max_t->xd[t] = t;
      f1->I_max_t->yd[t] = 200;
      f2->I_max_t->xd[t] = t;
      f2->I_max_t->yd[t] = 200;
      y1->y_t->xd[t] = t;
      y1->y_t->xd[t] = t *t;
      y2->y_t->xd[t] = t;
      y2->y_t->xd[t] = 1 + t *t;
      time_exp[t] = t;
      zmag_exp[t] = (t%10<6) ? 0 : 1;
      temp_exp[t] = 25;
      index_exp[t] = (unsigned long) t;
    }

    n_bead = 1;
    int i_bead;
    char g_name[32];
    snprintf( g_name, SDI_HDF5_MAX_NAME_LENGTH, "/peripherals");
    config_one_record_in_sdi_record(g_record, g_name);
    add_one_hdf5_data_to_record( g_record, 0, g_record->mem_map_list[0], SDI_RECORD_TYPE_FLOAT_1D, "time");
    attach_float_1d_to_hdf5_data( g_record->hdf5_data_all_list[g_record->n_hdf5_data_all-1], time_exp);
    add_one_hdf5_data_to_record( g_record, 0, g_record->mem_map_list[0], SDI_RECORD_TYPE_FLOAT_1D, "zmag");
    attach_float_1d_to_hdf5_data( g_record->hdf5_data_all_list[g_record->n_hdf5_data_all-1], zmag_exp);
    add_one_hdf5_data_to_record( g_record, 0, g_record->mem_map_list[0], SDI_RECORD_TYPE_FLOAT_1D, "temperature");
    attach_float_1d_to_hdf5_data( g_record->hdf5_data_all_list[g_record->n_hdf5_data_all-1], temp_exp);
    add_one_hdf5_data_to_record( g_record, 0, g_record->mem_map_list[0], SDI_RECORD_TYPE_UNSIGNED_LONG_1D, "index");
    attach_ulong_1d_to_hdf5_data( g_record->hdf5_data_all_list[g_record->n_hdf5_data_all-1], index_exp);


    for (i_bead = 0; i_bead<n_bead ; i_bead++) bead_record_config_1( g_record, bead, i_bead);
    for (i_bead = 0; i_bead<n_bead ; i_bead ++) record_bead_1d_float_basic_links(g_record, bead, i_bead, g_record->mem_map_list[0]);

    int datatype ;
    char bead_name[32];
    sdi_r *record_tmp;
    int index=0;
    int found=0;
    snprintf( g_name, SDI_HDF5_MAX_NAME_LENGTH, "/bead0");
    find_one_record_by_name( g_record, g_name, &record_tmp, &index, &found);
    //add_one_hdf5_data_to_record( g_record, index, g_record->mem_map_list[1], SDI_RECORD_TYPE_MOVIE, "movie_roi");
    //attach_oi_to_hdf5_data(g_record->hdf5_data_all_list[g_record->n_hdf5_data_all-1], oi);
    //for (i=2;i<video_saving_period+1;i++) add_one_mem_map_link_to_hdf5_data( g_record->record_list[index]->hdf5_data_list[g_record->record_list[index]->n_hdf5_data-1], g_record->mem_map_list[i]);

    int mem_offset[1] = {0};
    int mem_select[1] = {ny};
    int mem_stride[1] = {oi_nx};
    int mem_block[1] = {nx};
    int file_offset[3] = {0, 0, 0};
    int file_select[3] = {1, ny, nx};
    /*for(i=1;i<video_saving_period+1;i++)
    {
      config_hyperslabs_of_mem_map( g_record->mem_map_list[i], mem_offset, mem_select, mem_stride, mem_block, file_offset, file_select);
      g_record->mem_map_list[i]->expected_offset = video_saving_period;
    }*/
    int mem_offset2[1] = {0};
    int mem_select2[1] = {data_saving_period};
    int mem_stride2[1] = {1};
    int mem_block2[1] = {1};
    int file_offset2[1] = {0};
    int file_select2[1] = {data_saving_period};
    config_hyperslabs_of_mem_map( g_record->mem_map_list[0], mem_offset2, mem_select2, mem_stride2, mem_block2, file_offset2, file_select2);
    g_record->mem_map_list[0]->expected_offset = data_saving_period;
    int k;

    int period_min = video_saving_period;
    k = 0;
    win_printf_OK("start saving");

    clock_t t1, t0, t2, t3;
    long dt_clk=CLOCKS_PER_SEC;
    double dt;
    t0 = clock();

    bool record0 = false;
    bool record1 = false;
    int record_count = 0;
    int tmp1=0, tmp2=0;
    while(k<9000)
    {
      if ( (k % data_saving_period) == 0)
      {
        record0 = true;
        t2 = clock();
      }
      if ( (k % data_saving_period) == 0)
      {
        tmp1 = (record_count == 0) ? 0 : ( (tmp1 + data_saving_period) % data_buffer_size) ;
        tmp2 = (record_count == 0) ? 0 : (tmp2 + data_saving_period) ;
        record1 = true;
      }
      /*if ((k%period_min) == 0)
      {
        reload_hdf5_data_all_list(g_record);
        append_pages( g_record->mem_map_list, g_record->n_mem_map, g_record->hdf5_data_all_list, g_record->n_hdf5_data_all);
      }*/
      if (record0 == true)
      {
        reload_hdf5_data_all_list(g_record);
        append_pages( g_record->mem_map_list, g_record->n_mem_map, g_record->hdf5_data_all_list, g_record->n_hdf5_data_all);
      }
      if (record1 == true)
      {
        g_record->mem_map_list[0]->trigger = true;
        update_hyperslab_of_mem_map(g_record->mem_map_list[0],  tmp1, tmp2);
        record1 = false;
        record_count++;
      }
      //handling of the triggers of the mem map
      /*if ( (k%video_saving_period) == 0)
      {
        for (i=1;i<video_saving_period+1;i++)
        {
          g_record->mem_map_list[i]->trigger = true;
          //update_hyperslab_of_mem_map(g_record->mem_map_list[i], ((k+i-1) * oi_nx * oi_ny) %(video_buffer_size * oi_ny * oi_nx), k+i-1);
          update_hyperslab_of_mem_map(g_record->mem_map_list[i], 0, k+i-1);
        }
      }*/
      /*if ( (k%data_saving_period) == 0)
      {
        g_record->mem_map_list[0]->trigger = true;
        update_hyperslab_of_mem_map(g_record->mem_map_list[0], k%data_buffer_size, k);
      }*/

      //the data are written here
      handle_one_write_in_g_record(g_record);

      /*if (k%period_min == 0)
      {
        for (i=0;i<g_record->n_hdf5_data_all;i++) H5Dclose(g_record->hdf5_data_all_list[i]->d_id);
        for (i=0;i<g_record->n_mem_map;i++) g_record->mem_map_list[i]->trigger = false;
        H5Fclose(g_record->fid);
      }*/
      if (record0 == true)
      {
        for (i=0;i<g_record->n_hdf5_data_all;i++) H5Dclose(g_record->hdf5_data_all_list[i]->d_id);
        for (i=0;i<g_record->n_mem_map;i++) g_record->mem_map_list[i]->trigger = false;
        H5Fclose(g_record->fid);
        record0 = false;
        t3 = clock();
        //win_printf_OK("k %d duration %f",k, (double)(t3 -t2)/(double)dt_clk *1000);
      }
      k++;
    }
    //H5Lcreate_soft( "/peripherals", gid2, "/peripherals", H5P_LINK_CREATE, H5P_DEFAULT);
    //H5Lcreate_soft( const char *target_path, hid_t link_loc_id, const char*link_name, hid_t lcpl_id, hid_t lapl_id )

    t1=clock();
    dt=(double)(t1 -t0)/(double)dt_clk *1000;
    win_printf_OK("duration %f", dt);
    return 0;
}

//add a record config1 for one bead in g record
int init_one_bead_record_config_1(hdf5_sdi_r *g_record, sdi_bead *bead, int index, mem_map *mem_map_i)
{
  bead_record_config_1( g_record, bead, index);
  record_bead_1d_float_basic_links(g_record, bead, index, mem_map_i);
  return 0;
}

//expects opened datasets
//for every dataset, check whether the corresponding mem_map is triggered on
//if yes then write with the helo of the corresponding current mem map configuration
int handle_one_write_in_g_record(hdf5_sdi_r *g_record)
{
  int i, j;
  for (i=0;i<g_record->n_hdf5_data_all;i++)
  {
    for (j=0;j<g_record->hdf5_data_all_list[i]->n_mem_map_link;j++)
    {
      if (g_record->hdf5_data_all_list[i]->mem_map_link_list[j]->trigger == true)
      {
        switch (g_record->hdf5_data_all_list[i]->d_e->datatype)
        {
          case SDI_RECORD_TYPE_MOVIE :
          H5Dwrite( g_record->hdf5_data_all_list[i]->d_id, g_record->hdf5_data_all_list[i]->type_id, g_record->hdf5_data_all_list[i]->mem_map_link_list[j]->mem_sp,
            g_record->hdf5_data_all_list[i]->mem_map_link_list[j]->file_sp, H5P_DEFAULT, g_record->hdf5_data_all_list[i]->d_e->u_d->uch_1d);
          break;
          case SDI_RECORD_TYPE_FLOAT_1D :
          H5Dwrite( g_record->hdf5_data_all_list[i]->d_id, g_record->hdf5_data_all_list[i]->type_id, g_record->hdf5_data_all_list[i]->mem_map_link_list[j]->mem_sp,
              g_record->hdf5_data_all_list[i]->mem_map_link_list[j]->file_sp, H5P_DEFAULT, g_record->hdf5_data_all_list[i]->d_e->u_d->uch_1d);
          break;
          case SDI_RECORD_TYPE_UNSIGNED_LONG_1D :
          H5Dwrite( g_record->hdf5_data_all_list[i]->d_id, g_record->hdf5_data_all_list[i]->type_id, g_record->hdf5_data_all_list[i]->mem_map_link_list[j]->mem_sp,
              g_record->hdf5_data_all_list[i]->mem_map_link_list[j]->file_sp, H5P_DEFAULT, g_record->hdf5_data_all_list[i]->d_e->u_d->ul_1d);
          break;
        }
      }
    }
  }
  return 0;
}

int bead_record_config_1(hdf5_sdi_r *g_record, sdi_bead *bead, int index)
{
  char g_name[32];
  char tmp_name[32];

  snprintf( g_name, SDI_HDF5_MAX_NAME_LENGTH, "/bead%d", index);
  config_one_record_in_sdi_record(g_record, g_name);

  snprintf( tmp_name, SDI_HDF5_MAX_NAME_LENGTH, g_name);
  strcat( tmp_name, "/fringes0" );
  config_one_record_in_sdi_record(g_record, tmp_name);
  attach_fringes_attributes_to_record_1(g_record, bead->half_beads[0], tmp_name);

  snprintf( tmp_name, SDI_HDF5_MAX_NAME_LENGTH, g_name);
  strcat( tmp_name, "/fringes1" );
  config_one_record_in_sdi_record(g_record, tmp_name);
  attach_fringes_attributes_to_record_1(g_record, bead->half_beads[1], tmp_name);

  snprintf( tmp_name, SDI_HDF5_MAX_NAME_LENGTH, g_name);
  strcat( tmp_name, "/yp0" );
  config_one_record_in_sdi_record(g_record, tmp_name);

  snprintf( tmp_name, SDI_HDF5_MAX_NAME_LENGTH, g_name);
  strcat( tmp_name, "/yp1" );
  config_one_record_in_sdi_record(g_record, tmp_name);
  return 0;
}

//to set up quicl linkages between a mem_map for 1d float to bead data
int record_bead_1d_float_basic_links(hdf5_sdi_r *g_record, sdi_bead *bead, int index, mem_map * mem_map_i)
{
  int position = 99;
  char g_name[32];
  char tmp_name[32];
  sdi_r *record_tmp;
  int found = 1;

  //find the record object for the bead in the list
  snprintf( g_name, SDI_HDF5_MAX_NAME_LENGTH, "/bead%d", index);
  find_one_record_by_name( g_record, g_name, &record_tmp, &position, &found);
  //then link some data driven by the same mem_map_i
  add_one_hdf5_data_to_record( g_record, position, mem_map_i, SDI_RECORD_TYPE_FLOAT_1D, "z");
  attach_bead_data_to_hdf5_data(g_record->hdf5_data_all_list[g_record->n_hdf5_data_all-1], bead, KEEP_Z_T);
  add_one_hdf5_data_to_record( g_record, position, mem_map_i, SDI_RECORD_TYPE_FLOAT_1D, "y");
  attach_bead_data_to_hdf5_data(g_record->hdf5_data_all_list[g_record->n_hdf5_data_all-1], bead, KEEP_Y_T);
  add_one_hdf5_data_to_record( g_record, position, mem_map_i, SDI_RECORD_TYPE_FLOAT_1D, "x");
  attach_bead_data_to_hdf5_data(g_record->hdf5_data_all_list[g_record->n_hdf5_data_all-1], bead, KEEP_X_T);

  //the same process for the fringes
  snprintf( tmp_name, SDI_HDF5_MAX_NAME_LENGTH, g_name);
  strcat( tmp_name, "/fringes0" );
  find_one_record_by_name(g_record, tmp_name, &record_tmp, &position, &found);
  add_one_hdf5_data_to_record( g_record, position, mem_map_i, SDI_RECORD_TYPE_FLOAT_1D, "contrast");
  attach_fringes_data_to_hdf5_data(g_record->hdf5_data_all_list[g_record->n_hdf5_data_all-1], bead->half_beads[0], KEEP_CONTRAST_T);
  add_one_hdf5_data_to_record( g_record, position, mem_map_i, SDI_RECORD_TYPE_FLOAT_1D, "intensity");
  attach_fringes_data_to_hdf5_data(g_record->hdf5_data_all_list[g_record->n_hdf5_data_all-1], bead->half_beads[0], KEEP_I_MAX_T);

  snprintf( tmp_name, SDI_HDF5_MAX_NAME_LENGTH, g_name);
  strcat( tmp_name, "/fringes1" );
  find_one_record_by_name(g_record, tmp_name, &record_tmp, &position, &found);
  add_one_hdf5_data_to_record( g_record, position, mem_map_i, SDI_RECORD_TYPE_FLOAT_1D, "contrast");
  attach_fringes_data_to_hdf5_data(g_record->hdf5_data_all_list[g_record->n_hdf5_data_all-1], bead->half_beads[1], KEEP_CONTRAST_T);
  add_one_hdf5_data_to_record( g_record, position, mem_map_i, SDI_RECORD_TYPE_FLOAT_1D, "intensity");
  attach_fringes_data_to_hdf5_data(g_record->hdf5_data_all_list[g_record->n_hdf5_data_all-1], bead->half_beads[1], KEEP_I_MAX_T);

  //the same process for the fringes
  snprintf( tmp_name, SDI_HDF5_MAX_NAME_LENGTH, g_name);
  strcat( tmp_name, "/yp0" );
  find_one_record_by_name(g_record, tmp_name, &record_tmp, &position, &found);
  add_one_hdf5_data_to_record( g_record, position, mem_map_i, SDI_RECORD_TYPE_FLOAT_1D, "yp");
  attach_y_profile_data_to_hdf5_data(g_record->hdf5_data_all_list[g_record->n_hdf5_data_all-1], bead->y_bead[0], KEEP_Y_PROFILE_T);

  snprintf( tmp_name, SDI_HDF5_MAX_NAME_LENGTH, g_name);
  strcat( tmp_name, "/yp1" );
  find_one_record_by_name(g_record, tmp_name, &record_tmp, &position, &found);
  add_one_hdf5_data_to_record( g_record, position, mem_map_i, SDI_RECORD_TYPE_FLOAT_1D, "yp");
  attach_y_profile_data_to_hdf5_data(g_record->hdf5_data_all_list[g_record->n_hdf5_data_all-1], bead->y_bead[1], KEEP_Y_PROFILE_T);

  return 0;
}
//quick attachement of default attributes :
//position, sensitivities, bgd
int attach_fringes_attributes_to_record_1(hdf5_sdi_r *g_record, sdi_fringes *fringes, const char *name)
{
  sdi_r *record_tmp=NULL;
  int found=0, position=0;
  int *tmp1;
  float *tmp2;
  data_e *tmp3;

  find_one_record_by_name( g_record, name, &record_tmp, &position, &found);
  if (found !=1) return D_O_K;

  tmp3 = (data_e *)calloc(1, sizeof(data_e));
  tmp3->u_d = (u_data *)calloc(1, sizeof(u_data));

  tmp1 = (int *)calloc(1, sizeof(int));
  tmp1[0] = fringes->xc;
  tmp3->u_d->int_1d = tmp1;
  tmp3->datatype = SDI_RECORD_TYPE_INT_1D;
  attach_one_attribute_to_one_record( record_tmp, "xc", 1, tmp3);
  tmp1[0] = fringes->yc;
  attach_one_attribute_to_one_record( record_tmp, "yc", 1, tmp3);

  tmp2 = (float *)calloc(1, sizeof(float));
  tmp2[0] = fringes->v_0;
  tmp3->u_d->float_1d = tmp2;
  tmp3->datatype = SDI_RECORD_TYPE_FLOAT_1D;
  attach_one_attribute_to_one_record( record_tmp, "v0", 1, tmp3);
  tmp2[0] = fringes->v_1;
  attach_one_attribute_to_one_record( record_tmp, "v1", 1, tmp3);

  return 0;
}

int attach_one_attribute_to_one_record(sdi_r *record, char *name, int size, data_e *data)
{
  float attr_data_fl[size];
  int attr_data_int[size];
  unsigned long long attr_data_llu[size];
  hsize_t att_dims;
  hid_t dataspace_id, attribute_id, fid, gid, hdf5_type;
  char att_name[SDI_HDF5_MAX_NAME_LENGTH];
  snprintf(att_name, SDI_HDF5_MAX_NAME_LENGTH, name);
  att_dims = size;
  dataspace_id = H5Screate_simple(1, &att_dims, NULL);
  //compound_type = H5Tcreate(H5T_COMPOUND);
  //H5Tinsert( compound_type, "test", )
  fid = H5Fopen( record->filename, H5F_ACC_RDWR, H5P_DEFAULT);
  gid = H5Gopen2( fid, record->name, H5P_DEFAULT);
  switch (data->datatype) {
    case SDI_RECORD_TYPE_FLOAT_1D :
    {
      memcpy(attr_data_fl, data->u_d->float_1d, size*sizeof(float));
      hdf5_type = H5Tcopy(H5T_NATIVE_FLOAT);
      attribute_id = H5Acreate2 (gid, att_name, hdf5_type, dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
      H5Awrite(attribute_id, hdf5_type, attr_data_fl);
      break;
    }
    case SDI_RECORD_TYPE_INT_1D :
    {
      memcpy(attr_data_int, data->u_d->int_1d, size*sizeof(int));
      hdf5_type = H5Tcopy(H5T_NATIVE_INT);
      attribute_id = H5Acreate2 (gid, att_name, hdf5_type, dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
      H5Awrite(attribute_id, hdf5_type, attr_data_int);
      break;
    }
  }
  H5Aclose(attribute_id);
  H5Gclose(gid);
  H5Fclose(fid);
  return 0;
}


//H5D_FILL_TIME_ALLOC_EARLY, H5D_FILL_TIME, hp5_fill_set, hp5_fill_value
//hp5set_fill_time, hp5set_fill_value  : si H5D_FILL_TIME, fill d

//this function reloads the handles on all the datasets. typically called once one has previously close the file (and then released these handles)
//it opens the file, needs a close file after


int reload_hdf5_data_all_list(hdf5_sdi_r *g_record)
{
  hid_t fid, gid;
  int i, j;
  //each time you close the file (and then release all the group and dataset you must reload it in memory)
  g_record->fid = H5Fopen(g_record->filename, H5F_ACC_RDWR, H5P_DEFAULT);
  g_record->c_hdf5_data_all = 0;
  for (i=0;i<g_record->n_record;i++)
  {
    gid = H5Gopen2(g_record->fid, g_record->record_list[i]->name, H5P_DEFAULT);
    for (j=0; j<g_record->record_list[i]->n_hdf5_data;j++)
    {
      //reload the list of handles of dataset each time the file has been close
      g_record->record_list[i]->hdf5_data_list[j]->d_id = H5Dopen2(gid, g_record->record_list[i]->hdf5_data_list[j]->name, H5P_DEFAULT);
      g_record->hdf5_data_all_list[g_record->c_hdf5_data_all] = g_record->record_list[i]->hdf5_data_list[j]; //for making future call easier
      g_record->c_hdf5_data_all ++;
    }
    //close the groups : not needed any more
    H5Gclose(gid);
  }
  return 0;
}


int add_one_mem_map_to_hdf5_sdi_record(hdf5_sdi_r *g_record, int mem_rank, int *mem_dims, int file_rank, int *file_dims_chunk, int page_size, int dim_to_be_incr)
{
  if (g_record->n_mem_map == 0) g_record->mem_map_list = (mem_map **)calloc(1, sizeof(mem_map *));
  else g_record->mem_map_list = (mem_map **)realloc(g_record->mem_map_list, (g_record->n_mem_map+1) * sizeof(mem_map *));
  g_record->mem_map_list[g_record->n_mem_map] = (mem_map *)calloc(1, sizeof(mem_map));
  init_mem_map(g_record->mem_map_list[g_record->n_mem_map], mem_rank, mem_dims, file_rank, file_dims_chunk, page_size, dim_to_be_incr);
  g_record->n_mem_map++;
  return 0;
}

//pre-allocated mem_map object
//this creates dataspaces for mapping emmory data to saved data
//mem_dims is the buffer size for the datatype
//at the beginning the file contains file_dim[rank-1] chunks
int init_mem_map(mem_map *mem_map_i, int mem_rank, int *mem_dims, int file_rank, int *file_dims_chunk, int page_size, int dim_to_be_incr)
{
  int i;
  //memory part
  mem_map_i->mem_rank = mem_rank;
  mem_map_i->mem_dims = (hsize_t *)calloc(mem_rank, sizeof(hsize_t));
  for (i=0;i<mem_map_i->mem_rank;i++) mem_map_i->mem_dims[i] = (hsize_t) mem_dims[i];

  //file part
  mem_map_i->file_rank = file_rank;
  hsize_t maxdims[file_rank];
  mem_map_i->page_size = page_size;
  mem_map_i->dim_to_be_incr = dim_to_be_incr;
  mem_map_i->file_dims_chunk = (hsize_t *)calloc(file_rank, sizeof(hsize_t));
  mem_map_i->file_dims = (hsize_t *)calloc(file_rank, sizeof(hsize_t));
  for (i=0;i<mem_map_i->file_rank;i++)
  {
    maxdims[i] = H5S_UNLIMITED;
    mem_map_i->file_dims[i] = (hsize_t) 0;
    mem_map_i->file_dims_chunk[i] = (hsize_t) file_dims_chunk[i];
  }

  //creates the dataspace pair
  mem_map_i->file_sp = H5Screate_simple(file_rank, mem_map_i->file_dims, maxdims); //the size of the corresponding dataset is unlimited
  mem_map_i->mem_sp = H5Screate_simple(mem_rank, mem_map_i->mem_dims, NULL);
  mem_map_i->file_sp_offset = NULL;
  mem_map_i->file_sp_select = NULL;
  mem_map_i->mem_sp_offset = NULL;
  mem_map_i->mem_sp_select = NULL;
  mem_map_i->mem_sp_stride = NULL;
  mem_map_i->mem_sp_block = NULL;

  mem_map_i->trigger = false;
  return 0;
}

//config the hyperslabs at the beginning for a mem-map object (some elements, offset, will change during the experiment)
int config_hyperslabs_of_mem_map(mem_map *mem_map_i, int *mem_offset, int *mem_select, int *mem_stride, int *mem_block, int *file_offset, int *file_select)
{
  int i;
  if (mem_map_i == NULL) return D_O_K;

  mem_map_i->file_sp_offset = (hsize_t *)calloc(mem_map_i->file_rank, sizeof(hsize_t));
  mem_map_i->file_sp_select = (hsize_t *)calloc(mem_map_i->file_rank, sizeof(hsize_t));
  for (i=0;i<mem_map_i->file_rank;i++)
  {
    mem_map_i->file_sp_offset[i] =(hsize_t) file_offset[i];
    mem_map_i->file_sp_select[i] = (hsize_t) file_select[i];
  }

  mem_map_i->mem_sp_offset = (hsize_t *)calloc(mem_map_i->mem_rank, sizeof(hsize_t));
  mem_map_i->mem_sp_select = (hsize_t *)calloc(mem_map_i->mem_rank, sizeof(hsize_t));
  for (i=0;i<mem_map_i->mem_rank;i++)
  {
    mem_map_i->mem_sp_offset[i] = (hsize_t) mem_offset[i];
    mem_map_i->mem_sp_select[i] = (hsize_t) mem_select[i];
  }
  if( mem_stride !=NULL)
  {
    mem_map_i->mem_sp_stride = (hsize_t *)calloc(mem_map_i->mem_rank, sizeof(hsize_t));
    for (i=0; i<mem_map_i->mem_rank;i++)
    {
      mem_map_i->mem_sp_stride[i] = (hsize_t) mem_stride[i];
    }
  }
  if( mem_block !=NULL)
  {
    mem_map_i->mem_sp_block = (hsize_t *)calloc(mem_map_i->mem_rank, sizeof(hsize_t));
    for (i=0; i<mem_map_i->mem_rank;i++)
    {
      mem_map_i->mem_sp_block[i] = (hsize_t) mem_block[i];
    }
  }

  return 0;
}

//updates dataspaces element to save
//rolling buffer for memomry data : the buffer is divided into 2 parts
//when updated a memory map object also switch on its trigger to process one saving shot
int update_hyperslab_of_mem_map(mem_map *mem_map_in, int mem_offset, int file_offset)
{
  //H5Sselect_hyperslab (g_record->mem_map_list[0]->file_sp, H5S_SELECT_SET, file_off_tmp, NULL, file_select_tmp, NULL);
  mem_map_in->trigger = true;
  mem_map_in->file_sp_offset[mem_map_in->dim_to_be_incr] = (hsize_t) file_offset ;
  H5Sselect_hyperslab (mem_map_in->file_sp , H5S_SELECT_SET, mem_map_in->file_sp_offset, NULL, mem_map_in->file_sp_select, NULL);

  //iterates what part of buffer to save with an offset (for any kind of data, t must be the first dimension)
  mem_map_in->mem_sp_offset[0] = (hsize_t) mem_offset;
  H5Sselect_hyperslab (mem_map_in->mem_sp , H5S_SELECT_SET, mem_map_in->mem_sp_offset, mem_map_in->mem_sp_stride, mem_map_in->mem_sp_select, mem_map_in->mem_sp_block);

  return 0;
}


//comes after reload
//handles the synchronized pages allocation
//the first element of mem_map type detects whehter it is necessary, then implements page in all mem_map of the experiment
//then all the hdf5_data object adjust their dataset size in file to their cooresponding mem_map object that dictates how theyr aare filled
int append_pages(mem_map **mem_map_list, int n_mem_map, hdf5_data **hdf5_data_list, int n_hdf5_data)
{
  int i, j;

  //for all mem map objects check whether the dataspaces need to be extended
  for (i = 0; i<n_mem_map ; i++)
  {
    mem_map_list[i]->alloc = 0;
    if (mem_map_list[i]->file_dims[0] == 0)
    {
      for (j=0; j<mem_map_list[i]->file_rank; j++)  mem_map_list[i]->file_dims[j] = mem_map_list[i]->file_dims_chunk[j];
      mem_map_list[i]->alloc = 1;
    }
    else
    {
      if ( (mem_map_list[i]->file_sp_offset[mem_map_list[i]->dim_to_be_incr] + mem_map_list[i]->expected_offset ) >= mem_map_list[i]->file_dims[mem_map_list[i]->dim_to_be_incr] )
      {
        mem_map_list[i]->file_dims[mem_map_list[i]->dim_to_be_incr] += mem_map_list[i]->page_size;
        mem_map_list[i]->alloc = 1;
      }
    }

    if (mem_map_list[i]->alloc == 1)
    {
      H5Sset_extent_simple (mem_map_list[i]->file_sp, mem_map_list[i]->file_rank, mem_map_list[i]->file_dims, NULL);
      H5Sselect_hyperslab (mem_map_list[i]->file_sp , H5S_SELECT_SET, mem_map_list[i]->file_sp_offset, NULL, mem_map_list[i]->file_sp_select, NULL);
    }
  }
  //then extends the "real" dataset if its mem_map has alloc on
  for (i=0;i<n_hdf5_data;i++)
  {
    if (hdf5_data_list[i]->mem_map_link_list[0]->alloc == 1)
    {
      H5Dset_extent(hdf5_data_list[i]->d_id, hdf5_data_list[i]->mem_map_link_list[0]->file_dims);
    }
  }
  return 0;
}

//create a record for one bead in a hdf5 file already, and fills first stuff
//pour le moment y a rien mais faut rajouter les peripheriques et tout, qui sont tj la
int init_hdf5_sdi_record(hdf5_sdi_r *sdi_g_record, char *filename)
{
  hid_t fid;

  //record objects list
  sdi_g_record->n_record = 0;
  sdi_g_record->record_list = NULL;

  //dataspaces list
  sdi_g_record->n_mem_map=0;
  sdi_g_record->mem_map_list = NULL;

  //the total list of dataset handles (to be easy to manipulate after)
  sdi_g_record->c_hdf5_data_all = 0;
  sdi_g_record->n_hdf5_data_all = 0;
  sdi_g_record->hdf5_data_all_list = NULL;

  //file
  snprintf( sdi_g_record->filename, 1024, filename);
  // Create a new file using default properties, overwrite if non empty.
  sdi_g_record->fid = H5Fcreate( sdi_g_record->filename, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
  // Terminate access to the file
  H5Fclose(sdi_g_record->fid);
  return 0;
}

//allocate a record objects : a group
int alloc_one_record_in_hdf5_sdi_record(hdf5_sdi_r *sdi_g_record)
{
  if (sdi_g_record == NULL) return D_O_K;
  if (sdi_g_record->n_record == 0) sdi_g_record->record_list = (sdi_r **)calloc(1, sizeof(sdi_r *));
  else sdi_g_record->record_list = (sdi_r **) realloc(sdi_g_record->record_list, (1 + sdi_g_record->n_record) * sizeof(sdi_r *));
  sdi_g_record->record_list[sdi_g_record->n_record] = (sdi_r *)calloc(1, sizeof(sdi_r));
  sdi_g_record->n_record ++;
  return 0;
}

//config one bead record in the list
int config_one_record_in_sdi_record(hdf5_sdi_r *sdi_g_record, const char *name)
{
  hid_t fid, gid;
  //opens the file
  fid = H5Fopen(sdi_g_record->filename, H5F_ACC_RDWR, H5P_DEFAULT);

  alloc_one_record_in_hdf5_sdi_record(sdi_g_record);
  //compies the name of the file to be able to reload the id of the group of the bead after closing the files
  snprintf(sdi_g_record->record_list[sdi_g_record->n_record-1]->filename, SDI_HDF5_MAX_NAME_LENGTH, sdi_g_record->filename);

  snprintf(sdi_g_record->record_list[sdi_g_record->n_record-1]->name , SDI_HDF5_MAX_NAME_LENGTH, name);
  gid = H5Gcreate2(fid, sdi_g_record->record_list[sdi_g_record->n_record-1]->name, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  //data
  sdi_g_record->record_list[sdi_g_record->n_record-1]->n_hdf5_data = 0;
  sdi_g_record->record_list[sdi_g_record->n_record-1]->hdf5_data_list = NULL;

  //config_one_record_in_sdi_recordsdi_g_record, int index)
  H5Gclose(gid);
  H5Fclose(fid);
  return 0;
}

//finds a record object in the g_record list
//returns ptr onto it and its position in the list
int find_one_record_by_name(hdf5_sdi_r *g_record, const char *name, sdi_r **record, int *position, int *found)
{
  int i, found_tmp;
  found_tmp = 0;
  i=0;
  *position = 0;
  while (found_tmp ==0 && i<g_record->n_record)
  {
    if (strcmp(g_record->record_list[i]->name, name)==0)
    {
      found_tmp = 1;
      *record = g_record->record_list[i];
      *position = i;
    }
    i++;
  }
  *found = found_tmp;
  return 0;
}

int attach_bead_data_to_hdf5_data(hdf5_data *hdf5_d, sdi_bead *bead, int which_data)
{
  d_s *ds_tmp = NULL;
  switch_ptr_to_sdi_bead_field(bead, which_data, &ds_tmp);
  attach_float_1d_to_hdf5_data(hdf5_d, ds_tmp->yd);
  return 0;
}

int attach_fringes_data_to_hdf5_data(hdf5_data *hdf5_d, sdi_fringes *fringes, int which_data)
{
  d_s *ds_tmp = NULL;
  switch_ptr_to_sdi_fringes_field(fringes, which_data, &ds_tmp);
  attach_float_1d_to_hdf5_data(hdf5_d, ds_tmp->yd);
  return 0;
}

int attach_y_profile_data_to_hdf5_data(hdf5_data *hdf5_d, sdi_y_profile *y_profile, int which_data)
{
  d_s *ds_tmp = NULL;
  switch_ptr_to_sdi_y_profile_field(y_profile, which_data, &ds_tmp);
  attach_float_1d_to_hdf5_data(hdf5_d, ds_tmp->yd);
  return 0;
}

int attach_ulong_1d_to_hdf5_data(hdf5_data *hdf5_d, unsigned long *ul_1d)
{
  hdf5_d->d_e->datatype = SDI_RECORD_TYPE_UNSIGNED_LONG_1D;
  hdf5_d->d_e->u_d->ul_1d = ul_1d;
  return 0;
}

int attach_float_1d_to_hdf5_data(hdf5_data *hdf5_d, float *fl_1d)
{
  hdf5_d->d_e->datatype = SDI_RECORD_TYPE_FLOAT_1D;
  hdf5_d->d_e->u_d->float_1d = fl_1d;
  return 0;
}

int attach_oi_to_hdf5_data(hdf5_data *hdf5_d, O_i *oi)
{
  hdf5_d->d_e->datatype = SDI_RECORD_TYPE_MOVIE;
  hdf5_d->d_e->u_d->uch_1d = oi->im.mem[0];
  return 0;
}

//thus adds one more mem_map to an existing hdf5_data (with already one mem_map link)
//the new one must be compatible : meaning same format for the file space, and same rank for mem space
int add_one_mem_map_link_to_hdf5_data(hdf5_data *hdf5_d, mem_map *mem_map_i)
{
  hdf5_d->mem_map_link_list = (mem_map **)realloc(hdf5_d->mem_map_link_list, (hdf5_d->n_mem_map_link + 1) * sizeof(mem_map));
  hdf5_d->mem_map_link_list[hdf5_d->n_mem_map_link] = (mem_map *)calloc(1, sizeof(mem_map));
  hdf5_d->mem_map_link_list[hdf5_d->n_mem_map_link] = mem_map_i;
  hdf5_d->n_mem_map_link ++;
  return 0;
}

int add_one_hdf5_data_to_record(hdf5_sdi_r *g_record, int record_index, mem_map *mem_map_i, int datatype, const char *name)
{
  d_s *ds=NULL;
  hid_t did, pid, fid, gid;
  if (g_record==NULL) return D_O_K;
  if (g_record->n_record < record_index) return D_O_K;

  if (g_record->record_list[record_index]->n_hdf5_data == 0) g_record->record_list[record_index]->hdf5_data_list = (hdf5_data **)calloc(1, sizeof(hdf5_data *));
  else g_record->record_list[record_index]->hdf5_data_list = (hdf5_data **)realloc(g_record->record_list[record_index]->hdf5_data_list,
                                                                                    (1 + g_record->record_list[record_index]->n_hdf5_data) * sizeof(hdf5_data *));

  g_record->record_list[record_index]->hdf5_data_list[g_record->record_list[record_index]->n_hdf5_data] = (hdf5_data *)calloc(1, sizeof(hdf5_data));
  g_record->record_list[record_index]->hdf5_data_list[g_record->record_list[record_index]->n_hdf5_data]->d_e = (data_e *)calloc(1, sizeof(data_e));
  g_record->record_list[record_index]->hdf5_data_list[g_record->record_list[record_index]->n_hdf5_data]->d_e->u_d = (u_data *)calloc(1, sizeof(u_data));
  //creates property instance for the dataset, defined as a dataset ceation property
  pid = H5Pcreate(H5P_DATASET_CREATE); //property for dataset creation
  //property of dataset creation setted to chunked mode
  H5Pset_layout(pid, H5D_CHUNKED);
  //sets chunk size (in datatype units), with unlimited possible extension of the dataset
  H5Pset_chunk(pid, mem_map_i->file_rank, mem_map_i->file_dims_chunk);
  H5Pset_alloc_time( pid, H5D_ALLOC_TIME_INCR);

  fid = H5Fopen(g_record->record_list[record_index]->filename, H5F_ACC_RDWR, H5P_DEFAULT);
  gid = H5Gopen2(fid, g_record->record_list[record_index]->name, H5P_DEFAULT);

  switch (datatype) {
    case SDI_RECORD_TYPE_MOVIE :
    {
      g_record->record_list[record_index]->hdf5_data_list[g_record->record_list[record_index]->n_hdf5_data]->type_id = H5Tcopy(H5T_NATIVE_UCHAR);
      break;
    }
    case SDI_RECORD_TYPE_FLOAT_1D :
    {
      g_record->record_list[record_index]->hdf5_data_list[g_record->record_list[record_index]->n_hdf5_data]->type_id = H5Tcopy(H5T_NATIVE_FLOAT);
      break;
    }
    case SDI_RECORD_TYPE_UNSIGNED_LONG_1D:
    {
      g_record->record_list[record_index]->hdf5_data_list[g_record->record_list[record_index]->n_hdf5_data]->type_id = H5Tcopy(H5T_NATIVE_ULONG);
      break;
    }
  }
  snprintf(g_record->record_list[record_index]->hdf5_data_list[g_record->record_list[record_index]->n_hdf5_data]->name, SDI_HDF5_MAX_NAME_LENGTH, name);
  did = H5Dcreate2( gid, g_record->record_list[record_index]->hdf5_data_list[g_record->record_list[record_index]->n_hdf5_data]->name,
                         g_record->record_list[record_index]->hdf5_data_list[g_record->record_list[record_index]->n_hdf5_data]->type_id, mem_map_i->file_sp, H5P_DEFAULT, pid, H5P_DEFAULT);

  //fills the mem_map_list with the first element
  g_record->record_list[record_index]->hdf5_data_list[g_record->record_list[record_index]->n_hdf5_data]->mem_map_link_list = (mem_map **)calloc(1, sizeof(mem_map *));
  g_record->record_list[record_index]->hdf5_data_list[g_record->record_list[record_index]->n_hdf5_data]->mem_map_link_list[0] = (mem_map *)calloc(1, sizeof(mem_map));
  g_record->record_list[record_index]->hdf5_data_list[g_record->record_list[record_index]->n_hdf5_data]->mem_map_link_list[0] = mem_map_i;
  g_record->record_list[record_index]->hdf5_data_list[g_record->record_list[record_index]->n_hdf5_data]->n_mem_map_link = 1;

  //also adds in the more general list to use easily
  if (g_record->n_hdf5_data_all==0)  g_record->hdf5_data_all_list = (hdf5_data **)calloc(1, sizeof(hdf5_data *));
  else g_record->hdf5_data_all_list = (hdf5_data **)realloc(g_record->hdf5_data_all_list, (g_record->n_hdf5_data_all+1) * sizeof(hdf5_data *));
  g_record->hdf5_data_all_list[g_record->n_hdf5_data_all] = (hdf5_data *)calloc(1, sizeof(hdf5_data));
  g_record->hdf5_data_all_list[g_record->n_hdf5_data_all]->d_e = (data_e *)calloc(1, sizeof(data_e));
  g_record->hdf5_data_all_list[g_record->n_hdf5_data_all]->d_e->u_d = (u_data *)calloc(1, sizeof(u_data));
  g_record->hdf5_data_all_list[g_record->n_hdf5_data_all] = g_record->record_list[record_index]->hdf5_data_list[g_record->record_list[record_index]->n_hdf5_data];

  g_record->record_list[record_index]->n_hdf5_data ++;
  g_record->n_hdf5_data_all ++;
  H5Pclose(pid);
  H5Dclose (did);
  H5Gclose(gid);
  H5Fclose(fid);
  return 0;
}
