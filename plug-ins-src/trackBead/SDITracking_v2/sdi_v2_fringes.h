#pragma once

#include "sdi_v2_basic.h"
#include "sdi_v2_parameter.h"
#include "xvin.h"
#include "float.h"

#define PTR_FRINGES_Z_COUPLED_T 0
#define PTR_FRINGES_X_COUPLED_T 1
#define PTR_FRINGES_CONTRAST_T 2
#define PTR_FRINGES_I_MAX_T 3
#define PTR_FRINGES_RAW_PHI_0_T 4
#define PTR_FRINGES_RAW_PHI_1_T 5
#define PTR_FRINGES_CORRECTED_PHI_0_T 6
#define PTR_FRINGES_NU0_T 7
#define PTR_FRINGES_NU_ROI_FIT_SIZE 8
#define PTR_FRINGES_PHASE_JUMP_T 9
//#define PTR_FRINGES_QUALITY_FACTOR_T 11

#define PTR_FRINGES_WINDOWED_X_PROFILE 20
#define PTR_FRINGES_RAW_X_PROFILE 21
#define PTR_FRINGES_X_WINDOW 22
#define PTR_FRINGES_PHASE 23
#define PTR_FRINGES_AMPLITUDE 24
#define PTR_FRINGES_UNWRAPPED_PHASE_ROI 25
#define PTR_FRINGES_RAW_PHASE_ROI 26
#define PTR_FRINGES_SPECTRUM 27
#define PTR_FRINGES_SPECTRUM_LOG 28
#define PTR_FRINGES_NU_ROI 29

typedef struct sdi_version2_fringes
{
  int xc,yc; //position of the center of the roi for these fringes
  int x_roi_min,x_roi_max,y_roi_min,y_roi_max;//defines the roi to process in the image
  int which_half;//encodes if it is a first or second half of one bead
  float v_0;  //for z computation : phase velocity in rad.µm-1 (ie sentivity of the 0 order coefficient of the spectral phase)
  float v_1;  //group velocity in pix.µm-1 (ie sensitivy of the 1 order coeff of the spectral phase)
  float v_x;  //sensitivity along x
  float nu0;  //the (constant) frequency of the fringes (in pix **-1)
  float gy;
  float background; //in pixel count
  int weird_count;
  int roi_fit_size0; //the constant fit size
  bool hard_linked, need_reference_point, need_background; //bool that describe fringes state

  //the following fields are all the time used, with the same
  int n_output_t, c_output_t;
  float *contrast_t;//constrast of the fringes, used to discard some measurement when too much fluctuation
  float *I_max_t;//max of the signal, used to discard some patterns when too much fluctuation
  float *z_coupled_t;//position z (strongly coupled with x)
  float *x_coupled_t;// x (..z)
  float *quality_factor_t;//quality factor, function to be defined
  int fringes_state; //describes with an easy number how the fringe behaves (weird, bad, good..) for color&record issues

  //raw results for debugging, explanations etc
  int n_phi_t, c_phi_t;
  float *raw_phi_0_t, *raw_phi_1_t;//raw result from the fit of the spectral phase (in rad & pix)
  float *corrected_phi_0_t, *phase_jump_t;//the corrected phase after addition of the correct multiple of 2pi + the index of fringes
  float *nu0_t;
  float *roi_fit_size_t;  //this defines & keep in memory the roi for fit on phase (should be a int but it is simpler that way)

  //the intermediate steps of tracking algo, all arranged in buffers for debugging, generating iamges..
  float **windowed_x_profile, **raw_x_profile, **x_window;                  //in real space
  float **spectrum, **spectrum_log, **amplitude, **phase;                   //in Fourier space
  float **nu_roi, **raw_phase_roi, **unwrapped_phase_roi;  //roi, in Fourier space, for fitting the spectral phase
  //to handle with history buffers of the previous quantities
  int n_windowed_x_profile_buffer, c_windowed_x_profile_buffer;  //0 means you do not keep it in memory at the end of its use for computation
  int n_raw_x_profile_buffer, c_raw_x_profile_buffer;
  int n_x_window_buffer, c_x_window_buffer;
  int n_spectrum_buffer, c_spectrum_buffer;
  int n_spectrum_log_buffer, c_spectrum_log_buffer;
  int n_amplitude_buffer, c_amplitude_buffer;
  int n_phase_buffer, c_phase_buffer;
  int n_nu_roi_buffer, c_nu_roi_buffer;
  int n_raw_phase_roi_buffer, c_raw_phase_roi_buffer;
  int n_unwrapped_phase_roi_buffer, c_unwrapped_phase_roi_buffer;

  //the points to find the 2pi multiple to be added to current phi_0
  int n_ref_points;
  float *phi_0_ref_points, *phi_1_ref_points;
  float *k_pi_ref_points;
} sdi_v2_fringes;


PXV_FUNC(int, sdi_v2_reset_all_intermediate_data_of_fringes, (sdi_v2_fringes *fringes));
PXV_FUNC(int, sdi_v2_add_current_point_as_ref_points, (sdi_v2_fringes *fringes));
PXV_FUNC(int, sdi_v2_add_one_ref_points, (sdi_v2_fringes *fringes, float phi0_r, float phi1_r, float k_pi_ref_point));
PXV_FUNC(int, sdi_v2_compute_x_z_coupled, (sdi_v2_fringes *fringes));
PXV_FUNC(int, sdi_v2_compute_v_x_of_fringes, (sdi_v2_fringes *fringes, sdi_v2_parameter *param));
PXV_FUNC(int, sdi_v2_compute_v_0_of_fringes, (sdi_v2_fringes *fringes, int sign, float x_1, float x_2, float lambda, float gy, float ft, float n_o));
PXV_FUNC(int, sdi_v2_frequency_calibration_of_fringes, (sdi_v2_fringes *fringes));
PXV_FUNC(int, sdi_v2_linear_calibration_of_sdi_fringes, (sdi_v2_fringes *fringes, float *z, float *order0_z, float *order1_z, int n_points));

PXV_FUNC(int, sdi_v2_allocate_sdi_v2_fringes_phi_t_fields, (sdi_v2_fringes *fringes, int size));
PXV_FUNC(int, sdi_v2_allocate_sdi_v2_fringes_output_t_fields, (sdi_v2_fringes *fringes, int size));
PXV_FUNC(int, sdi_v2_update_c_profile_fringes_points, (sdi_v2_fringes *fringes, int ci));
PXV_FUNC(int, sdi_v2_compute_phase_coeff_of_sdi_fringes, (sdi_v2_fringes *fringes, sdi_v2_parameter *param));
PXV_FUNC(int, sdi_v2_compute_phase_coeff_of_x_profile, (sdi_v2_fringes *fringes, sdi_v2_parameter *param, double *phase_coeff));
PXV_FUNC(int, sdi_v2_phase_fit_omega, (sdi_v2_fringes *fringes, sdi_v2_parameter *param, double *phase_coeff, float nu0, int x_i_frac_min, int x_i_frac_max));
PXV_FUNC(int, sdi_v2_process_sdi_fringes_x_profile, (sdi_v2_fringes *fringes, sdi_v2_parameter *param));
PXV_FUNC(int, sdi_v2_extract_sdi_fringes_raw_x_profile_from_image, (O_i *oi, sdi_v2_fringes *fringes, sdi_v2_parameter *param));

//allocation stuff
PXV_FUNC(int, sdi_v2_fringes_parameter_update, (sdi_v2_fringes *fringes, sdi_v2_parameter *param));
PXV_FUNC(int, sdi_v2_remove_one_sdi_fringes, (sdi_v2_fringes *fringes));
PXV_FUNC(int, sdi_v2_reset_data_of_fringes, (sdi_v2_fringes *fringe, int which_data));
PXV_FUNC(int, sdi_v2_init_one_sdi_fringes_by_position, (sdi_v2_fringes *fringes, int xc, int yc));
PXV_FUNC(int, sdi_v2_allocate_sdi_v2_fringes_1d_float_fields, (sdi_v2_fringes *fringes, int size, int which_data));
PXV_FUNC(int, sdi_v2_allocate_sdi_v2_fringes_2d_float_fields, (sdi_v2_fringes *fringes, sdi_v2_parameter *param, int buffer_size, int which_data));
PXV_FUNC(int, sdi_v2_switch_ptr_to_sdi_fringes_2d_float_field, (sdi_v2_fringes *fringes, int which_data, float **field, int *n_field, int *c_field,
                                                                                float **ext_float, int n_ext_float));
PXV_FUNC(int, sdi_v2_switch_ptr_to_sdi_fringes_1d_float_field, (sdi_v2_fringes *fringes, int which_data, float **field, float **ext_float));
