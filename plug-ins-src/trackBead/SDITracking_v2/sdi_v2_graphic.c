 #include "sdi_v2_graphic.h"

 extern pltreg **available_pltreg;
 extern int n_available_pltreg;

 extern int *sdi_colormap;

 int sdi_v2_idle_op_refresh(O_p *op, DIALOG *d)
 {
     pltreg *pr = NULL;
     if (d == NULL || d->dp == NULL) return D_O_K;;
     pr = (pltreg *)d->dp;
     if (pr->one_p == NULL || pr->n_op == 0)  return D_O_K;
     op->need_to_refresh = 1;
     if (op->need_to_refresh) refresh_plot(pr, UNCHANGED);
     return 0;
 }

//this allows to update the value of x axis (and then we avoid add_new_point_to_ds)
int sdi_v2_update_x_display(sdi_v2_disp *disp, int cur, float val)
{
  int tmp;
  //when buffer of x axis
  if (disp->n_x > 1)
  {
    disp->c_x = cur;
    disp->c_x = disp->c_x % disp->n_x;
  }
  if (disp->n_axis >1)
  {
    tmp = (cur % disp->n_axis);
    disp->c_axis = tmp;
  }
  //update of value in x if not constant (time evolving)
  if (disp->constant_x == false) //first case : x a one dim axis which is updated during time
  {
    tmp = (cur % (int) (disp->l_axis[disp->c_axis]));
    disp->x[disp->c_x][tmp] = val;
  }
  sdi_v2_link_arrays_to_ds( disp->data, disp->x[disp->c_x], disp->y[disp->c_y], (int) (disp->l_axis[disp->c_axis]));
  return 0;
}

//this one allows to switch between element of 2d buffers
 int sdi_v2_update_y_display(sdi_v2_disp *disp, int cur)
 {
   if (disp->n_y > 1)
   {
     disp->c_y = cur;
     disp->c_y = disp->c_y % disp->n_y;
     sdi_v2_link_arrays_to_ds( disp->data, disp->x[disp->c_x], disp->y[disp->c_y], (int) disp->l_axis[disp->c_axis]);
   }
   return 0;
 }

//like remove_ds_from_op, except that it does not free the ds
//to be used when one ds is plotted in several different plots and then you want to remove one of those plot
int	sdi_v2_remove_ds_from_op_without_free_ds(O_p *op, d_s *ds)
{
  int i,found,k;
	if (op == NULL || ds == NULL)	return D_O_K;
  i=0;
  found=0;
  while (i<op->n_dat && found==0)
  {
    found=(int) (ds==op->dat[i]);
    i++;
  }
  op->n_dat--;
  for (k=i-1;k<op->n_dat;k++) op->dat[k] = op->dat[k+1];
  return 0;
}

int sdi_v2_remove_plot_from_plr_without_free_ds(pltreg *pr, O_p *op)
{
  int i, found, k;
  if ( pr == NULL ) return D_O_K;
  i=0;
  found=0;
  while (i<pr->n_op && found==0)
  {
    found =(int) (pr->o_p[i]==op);
    i++;
  }
  sdi_v2_free_one_plot_without_free_ds(pr->o_p[i-1]);
  pr->n_op--;
  for (k=i-1 ; k< pr->n_op ; k++)  pr->o_p[k] = pr->o_p[k+1];
  pr->cur_op = 0;
  pr->one_p = (pr->n_op >= 0) ? pr->o_p[pr->cur_op] : NULL;
  return 0;
}

int sdi_v2_free_one_plot_without_free_ds(O_p *op)
{
	int i;
	if (op == NULL) 	return xvin_error(Wrong_Argument);
	for (i=0 ; i< op->n_lab ; i++)
	{
		if (op->lab[i]->text != NULL) 	free(op->lab[i]->text);
		if (op->lab[i] != NULL) 	free(op->lab[i]);
	}
	if (op->lab)	{free(op->lab);	   op->lab = NULL;}
	if (op->filename != NULL)	free(op->filename);
	if (op->dir != NULL)		free(op->dir);
	if (op->title != NULL)		free(op->title);
	if (op->x_title != NULL)	free(op->x_title);
	if (op->y_title != NULL)	free(op->y_title);
	if (op->x_prime_title != NULL)	free(op->x_prime_title);
	if (op->y_prime_title != NULL)	free(op->y_prime_title);
	if (op->y_prefix != NULL)	free(op->y_prefix);
	if (op->x_prefix != NULL)	free(op->x_prefix);
	if (op->t_prefix != NULL)	free(op->t_prefix);
	if (op->x_unit != NULL)		free(op->x_unit);
	if (op->y_unit != NULL)		free(op->y_unit);
	if (op->t_unit != NULL)		free(op->t_unit);
	if (op->xu != NULL)
	{
		for (i=0 ; i< op->n_xu ; i++)	free_unit_set(op->xu[i]);
		free(op->xu);
	}
	if (op->yu != NULL)
	{
		for (i=0 ; i< op->n_yu ; i++)	free_unit_set(op->yu[i]);
		free(op->yu);
	}
	if (op->tu != NULL)
	{
		for (i=0 ; i< op->n_tu ; i++)	free_unit_set(op->tu[i]);
		free(op->tu);
	}
	//if (&op->bplt)  free_box(&op->bplt);
	free(op);	op = NULL;
	return 0;
}


int sdi_v2_remove_one_sdi_display(sdi_v2_disp *display)
{
  if (display == NULL) return D_O_K;
  d_s *safe = NULL;
  pltreg *plr = NULL;
  if (display->op->n_dat>1)  sdi_v2_remove_ds_from_op_without_free_ds(display->op, display->data);
  else
  {
    if (display->plr->n_op>1)  sdi_v2_remove_plot_from_plr_without_free_ds(display->plr,display->op);
    else //if it is the last dataset in the last plot of a plot project, it is replaced by a 1point dataset, then erased
    {
      safe = build_adjust_data_set(safe, 1, 1);
      safe->xd[0] = 0;
      safe->yd[0] = 0;
      add_data_to_one_plot(display->op, IS_DATA_SET, safe);
      set_plot_title(display->op, "");
      set_plot_x_title(display->op, "");
      set_plot_y_title(display->op, "");
      set_plot_y_prime_title(display->op, "");
      set_op_filename(display->op, "");
      sdi_v2_remove_ds_from_op_without_free_ds(display->op, display->data);
      //also, the plot project is added to the general list of empty plot proj to be re-used (bug xvin)
      if (n_available_pltreg == 0) available_pltreg = (pltreg **)calloc(64, sizeof(pltreg *));
      plr = display->plr;
      available_pltreg[n_available_pltreg] = plr;
      n_available_pltreg ++;
    }
  }
  free(display);
  display = NULL;
  return 0;
}


//if plr == nul check whether there is an available pltreg
int sdi_v2_launch_display(d_s *ds, pltreg **plr, O_p **op, char *name)
{
 int flag_ini;
 flag_ini=0;
 if (ds == NULL)  return D_O_K;
 if (*plr==NULL && n_available_pltreg==0)
 {
   if (*op==NULL)
   {
     flag_ini=1;
     init_one_plot(*op);
   }
   *plr=create_hidden_pltreg_with_op(op,1,1,0,name);
 }
 else
 {
   if (*plr == NULL  && n_available_pltreg!=0)
   {//i use previously used pltreg that have been released
     n_available_pltreg--;
     *plr = available_pltreg[n_available_pltreg];
     *op = (*plr)->o_p[0];
     flag_ini = 1;
   }
   if (*op == NULL)
   {
      *op=create_and_attach_one_plot(*plr,1,1,0);
       flag_ini = 1;
   }
 }
 add_data_to_one_plot(*op, IS_DATA_SET, ds);
 //set title, etc, avoiding conflicts overwriting previous names
 if (flag_ini==1) remove_ds_from_op(*op, (*op)->dat[0]);//remove the first dataset put during the initialization of a plot
 (*op)->op_idle_action = sdi_v2_idle_op_refresh;
 return 0;
}

//la super base : juste x, y, z(t)
//un axe x mis a jour dans le temps
int sdi_v2_config_bead_1d_display(sdi_v2_bead *bead, int which_data, sdi_v2_disp *display, int size, float x0)
{
  float **field;
  float *x, *y;
  int i;
  sdi_v2_switch_ptr_to_sdi_bead_1d_float_field(bead, which_data, &y, NULL, 0);
  //y axis config
  display->n_y = 1;
  display->c_y = 0;
  display->y = (float **)calloc(1, sizeof(float *));
  display->y[0] = y;
  //x axis config : only 1 axis, with fixed length, to be updated at one point during time
  display->n_x = 1;
  display->x = (float **)calloc(1, sizeof(float));
  display->x[0] = (float *)calloc(size, sizeof(float));
  display->c_x = 0;
  display->l_axis = (float *)calloc(1, sizeof(float));
  display->c_axis = 0;
  display->l_axis[0] = size;
  display->n_axis = 1;
  display->constant_x = false;//for this we will update the value of x axis
  for (i=0 ;i<size ;i++) display->x[0][i] = x0; //temporaire ca va etre ecraser par les timestamp
  display->data = sdi_v2_init_empty_ds();
  sdi_v2_link_arrays_to_ds(display->data, display->x[0], display->y[0], (int) (display->l_axis[display->c_axis]));
  return 0;
}

//un jour faudra vraiment ecrier des union avec fringes, y prof et bead pour plus joli...
//bon la fonction switch est vraiment pas fini et c'est pas beau
int sdi_v2_config_fringes_2d_display(sdi_v2_fringes *fringes, int which_data, sdi_v2_disp *display, sdi_v2_parameter *param)
{
  float **field_2d;
  int n_field, c_field, dim=0;
  int i;
  field_2d = (float **)calloc(8096, sizeof(float *));
  sdi_v2_switch_ptr_to_sdi_fringes_2d_float_field(fringes, which_data, field_2d, &n_field, &c_field, NULL, 0);
  //y data : rolling buffer of n_field curves
  display->n_y = n_field;
  display->c_y = c_field;
  display->y = field_2d;

  //settings of the associated x axis for plot : frequency, pixels etc
  if (which_data == PTR_FRINGES_WINDOWED_X_PROFILE || which_data == PTR_FRINGES_RAW_X_PROFILE || which_data == PTR_FRINGES_X_WINDOW)
  {
    //x axis config : only 1 axis, with fixed length, not updated
    display->x = (float **)calloc(1, sizeof(float *));
    display->x[0] = (float *)calloc(param->x_roi_size, sizeof(float));
    display->n_x = 1;
    display->c_x = 0;
    display->l_axis = (float *)calloc(1, sizeof(float));
    display->l_axis[0] = param->x_roi_size;
    display->c_axis = 0;
    display->n_axis = 1;
    display->constant_x = true;
    for (i=0;i<display->l_axis[0];i++) display->x[0][i] = i;//pixel axis (in future could be interesting to put in param a constant x axis in µm common to all)
  }
  if (which_data == PTR_FRINGES_PHASE || which_data == PTR_FRINGES_AMPLITUDE || which_data == PTR_FRINGES_SPECTRUM || which_data == PTR_FRINGES_SPECTRUM_LOG)
  {
    //x axis config : only 1 axis, with fixed length, not updated
    display->x = (float **)calloc(1, sizeof(float *));
    display->x[0] = (float *)calloc(param->fp_x->n_2, sizeof(float));
    display->n_x = 1;
    display->c_x = 0;
    display->l_axis = (float *)calloc(1, sizeof(float));
    display->l_axis[0] = param->fp_x->n_2;
    display->c_axis = 0;
    display->n_axis = 1;
    display->constant_x = true;
    for (i=0;i<display->l_axis[0];i++) display->x[0][i] = param->nu_x[i];//the common frequency axis of fft of fringes
  }
  if (which_data == PTR_FRINGES_RAW_PHASE_ROI || which_data == PTR_FRINGES_UNWRAPPED_PHASE_ROI)
  {
    float **field_2d_2;
    if (fringes->n_phi_t == 0)
    {
      sdi_v2_allocate_sdi_v2_fringes_phi_t_fields(fringes, 1);
      fringes->roi_fit_size_t[fringes->c_phi_t] = 1;//for the first point in time
    }
    int n_field_2, c_field_2;
    field_2d_2 = (float **)calloc(8096, sizeof(float *));
    sdi_v2_switch_ptr_to_sdi_fringes_2d_float_field(fringes, PTR_FRINGES_NU_ROI, field_2d_2, &n_field_2, &c_field_2, NULL, 0);
    display->x = field_2d_2;
    display->n_x = n_field_2;
    display->c_x = c_field_2;
    display->l_axis = fringes->roi_fit_size_t;
    display->c_axis = fringes->c_phi_t;
    display->n_axis = fringes->n_phi_t;
    display->constant_x = true;
  }
  display->data = sdi_v2_init_empty_ds();
  sdi_v2_link_arrays_to_ds(display->data, display->x[display->c_x], display->y[display->c_y], (int) display->l_axis[display->c_axis]);

  return 0;
}

//bon la fonction switch est vraiment pas fini et c'est nuuuul
int sdi_v2_config_fringes_1d_display(sdi_v2_fringes *fringes, int which_data, sdi_v2_disp *display, int size)
{
  float **field;
  float *x, *y;
  int i;
  sdi_v2_switch_ptr_to_sdi_fringes_1d_float_field(fringes, which_data, &y, NULL);
  //y axis config
  display->n_y = 1;
  display->c_y = 0;
  display->y = (float **)calloc(1, sizeof(float *));
  display->y[0] = y;
  //x axis config : only 1 axis, with fixed length, to be updated at one point during time
  display->n_x = 1;
  display->x = (float **)calloc(1, sizeof(float));
  display->x[0] = (float *)calloc(size, sizeof(float));
  display->c_x = 0;
  display->l_axis = (float *)calloc(1, sizeof(float));
  display->c_axis = 0;
  display->l_axis[0] = size;
  display->n_axis = 1;
  display->constant_x = false;//for this we will update the value of x axis
  for (i=0 ;i<size ;i++) display->x[0][i] = 0; //will be updated with timestamps of camera, index of movies, zmag (xyy(zmag)) etc
  display->data = sdi_v2_init_empty_ds();
  sdi_v2_link_arrays_to_ds(display->data, display->x[0], display->y[0], (int) display->l_axis[display->c_axis]);
 return 0;
}

//bon la fonction switch est vraiment pas fini et c'est nuuuul
int sdi_v2_config_y_profile_1d_display(sdi_v2_y_profile *profile, int which_data, sdi_v2_disp *display, int size)
{
  float **field;
  float *x, *y;
  int i;
  sdi_v2_switch_ptr_to_sdi_y_profile_1d_float_field(profile, which_data, &y, NULL);
  //y axis config
  display->n_y = 1;
  display->c_y = 0;
  display->y = (float **)calloc(1, sizeof(float *));
  display->y[0] = y;
  //x axis config : only 1 axis, with fixed length, to be updated at one point during time
  display->n_x = 1;
  display->x = (float **)calloc(1, sizeof(float));
  display->x[0] = (float *)calloc(size, sizeof(float));
  display->c_x = 0;
  display->l_axis = (float *)calloc(1, sizeof(float));
  display->c_axis = 0;
  display->l_axis[0] = size;
  display->n_axis = 1;
  display->constant_x = false;//for this we will update the value of x axis
  for (i=0 ;i<size ;i++) display->x[0][i] = 0; //temporaire ca va etre ecraser par les timestamp
  display->data = sdi_v2_init_empty_ds();
  sdi_v2_link_arrays_to_ds(display->data, display->x[0], display->y[0], (int) display->l_axis[display->c_axis]);
 return 0;
}

//bon la fonction switch est vraiment pas fini et c'est nuuuul
int sdi_v2_config_y_profile_2d_display(sdi_v2_y_profile *y_profile, int which_data, sdi_v2_disp *display, sdi_v2_parameter *param)
{
  float **field_2d;
  int n_field, c_field, dim=0;
  int i, n;
  float *x;
  field_2d = (float **)calloc(8096, sizeof(float *));
  sdi_v2_switch_ptr_to_sdi_y_profile_2d_float_field(y_profile, which_data, field_2d, &n_field, &c_field, NULL, 0);
  //setting of y curves
  display->n_y = n_field;
  display->c_y = c_field;
  display->y = field_2d;

  //setting of x curves
  if (which_data == PTR_Y_PROFILE_Y_PROFILE || which_data == PTR_Y_PROFILE_Y_WINDOW || which_data == PTR_Y_PROFILE_RAW_Y_PROFILE
                                            || which_data == PTR_Y_PROFILE_Y_AUTOCONV)
  {
    display->x = (float **)calloc(1, sizeof(float *));
    display->x[0] = (float *)calloc(param->y_roi_size_Y, sizeof(float));
    display->n_x = 1;
    display->c_x = 0;
    display->l_axis = (float *)calloc(1, sizeof(float));
    display->l_axis[0] = param->y_roi_size_Y;
    display->c_axis = 0;
    display->n_axis = 1;
    display->constant_x = true;
    for (i=0;i<display->l_axis[0];i++) display->x[0][i] = i;//the common frequency axis of fft of fringes
  }
  display->data = sdi_v2_init_empty_ds();
  sdi_v2_link_arrays_to_ds(display->data, display->x[0], display->y[display->c_y], (int) display->l_axis[0]);
 return 0;
}

//from pix unit (imr) coordinates it places a rect on right position in the image
int sdi_v2_draw_one_fast_rect_with_user_pix(int xc,int yc,int half_length, int half_width,int color, O_i *oi, BITMAP *imb)
{
  int xc2,yc2,half_length2,half_width2;
  xc2 = sdi_v2_x_user_pix_2_vga_screen_unit(imb,oi,xc);
  half_length2 = sdi_v2_dx_user_pix_2_vga_screen_unit(imb,oi,half_length);
  yc2 = sdi_v2_y_user_pix_2_vga_screen_unit(imb,oi,yc);
  half_width2 = sdi_v2_dy_user_pix_2_vga_screen_unit(imb,oi,half_width);
  sdi_v2_draw_one_fast_rect_vga_screen_unit(xc2,yc2,half_length2,half_width2,color,imb);
  return 0;
}

int sdi_v2_place_multiple_double_fast_rect_with_mouse(int *xc, int *yc, int half_length,int half_width, int x_dist, int y_dist, int color, imreg *imr, O_i *oi, BITMAP *imb)
{
  int  x_m, y_m,count;
  int pos_m;
  int one_time;
  int xc_tmp,yc_tmp;
  int y_offset_1, y_offset_2, y_offset_1_vga, y_offset_2_vga;
  int x_offset_1, x_offset_2, x_m_tmp, y_m_tmp, x_offset_1_vga, x_offset_2_vga;
  y_offset_1=(int) (y_dist / 2);
  y_offset_2=(int) (y_dist / 2) + y_dist % 2;
  x_offset_1=(int) (x_dist / 2);
  x_offset_2=(int) (x_dist / 2) + x_dist % 2;
  y_offset_1_vga=sdi_v2_dy_user_pix_2_vga_screen_unit(imb,oi,y_offset_1);
  y_offset_2_vga=sdi_v2_dy_user_pix_2_vga_screen_unit(imb,oi,y_offset_2);
  x_offset_1_vga=sdi_v2_dx_user_pix_2_vga_screen_unit(imb,oi,x_offset_1);
  x_offset_2_vga=sdi_v2_dx_user_pix_2_vga_screen_unit(imb,oi,x_offset_2);
  pos_m = mouse_pos;
  x_m = pos_m >> 16;
  y_m = pos_m & 0x0000ffff;
  count=0;

  do
  {
     while( key[KEY_F]==false )
        {
           if (pos_m != mouse_pos)
	            {
	              pos_m = mouse_pos;
                x_m = pos_m >> 16;
                y_m = pos_m & 0x0000ffff;
                one_time=0;
                refresh_image(imr,UNCHANGED);
                acquire_screen();
	              sdi_v2_draw_one_fast_rect_vga_screen_unit(x_m-x_offset_1_vga,y_m-y_offset_1_vga,half_length,half_width,color,screen);
                sdi_v2_draw_one_fast_rect_vga_screen_unit(x_m+x_offset_2_vga,y_m+y_offset_2_vga,half_length,half_width,color,screen);
                release_screen();
	             }
          }

      if (one_time==0)
           {
               pos_m = mouse_pos;
               x_m = pos_m >> 16;
               y_m = pos_m & 0x0000ffff;
               x_m_tmp = x_imr_2_imdata(imr, x_m);
               y_m_tmp = y_imr_2_imdata(imr, y_m);
               *(xc+count) = x_m_tmp ;
               *(yc+count) = y_m_tmp;
               xc_tmp=sdi_v2_x_user_pix_2_vga_screen_unit(imb,oi,*(xc+count));
               yc_tmp=sdi_v2_y_user_pix_2_vga_screen_unit(imb,oi,*(yc+count));
               acquire_bitmap(imb);
               sdi_v2_draw_one_fast_rect_vga_screen_unit(xc_tmp-x_offset_1_vga,yc_tmp-y_offset_1_vga,half_length,half_width,color,imb);
               sdi_v2_draw_one_fast_rect_vga_screen_unit(xc_tmp+x_offset_2_vga,yc_tmp+y_offset_2_vga,half_length,half_width,color,imb);
               release_bitmap(imb);
               count+=1;
               if (count == SDI_MAX_NUMBER_OF_PATTERN)
                  {
                      win_printf_OK("You have reached the maximum number of patterns");
                      return 1;
                  }
             }
      one_time+=1;
  } while( key[KEY_D] ==false );
  refresh_image(imr,UNCHANGED);
  return count;
}

//different from x_imr_2_imdata & cie because x is the coordinate in the image not on screen
//set of 4 functions that translate position and distance in pixel units into vga screen unit
//to be used to put something in the bmp hooked to a oi
int sdi_v2_y_user_pix_2_vga_screen_unit(BITMAP *bmp, O_i *oi, int y)
{
  return( (int) ( (float)(oi->im.nye - y) * (float)(bmp->h) / (float)(oi->im.nye-oi->im.nys) ));
}
int sdi_v2_dy_user_pix_2_vga_screen_unit(BITMAP *bmp, O_i *oi, int dy)
{
  return( (int) ( dy * (float)(bmp->h) / (float)(oi->im.nye-oi->im.nys) ));
}
int sdi_v2_x_user_pix_2_vga_screen_unit(BITMAP *bmp, O_i *oi, int x)
{
  return( (int) ( (float)(x-oi->im.nxs) * (float)(bmp->w) / (float)(oi->im.nxe-oi->im.nxs) ) );
}
int sdi_v2_dx_user_pix_2_vga_screen_unit(BITMAP *bmp, O_i *oi, int dx)
{
  return ( (int) ( dx * (float)(bmp->w) / (float)(oi->im.nxe-oi->im.nxs) ) );
}
int	sdi_v2_draw_one_fast_rect_vga_screen_unit(int xc, int yc, int half_length, int half_width, int color, BITMAP* bm)
{
  int x1,x2,y1,y2;
  int x01,x02,y01,y02;
  x1=xc-half_length;
  x2=xc+half_length;
  y1=yc-half_width;
  y2=yc+half_width;
  y01=yc-4*half_width;
  y02=yc+4*half_width;
  x01=xc-half_length-10;
  x02=xc+half_length+10;
  fastline(bm, x1, y1, x2, y1, color);
  fastline(bm, x2, y1, x2, y2, color);
  fastline(bm, x2, y2, x1, y2, color);
  fastline(bm, x1, y2, x1, y1, color);
  fastline(bm, xc, y01, xc, y02, color);
  fastline(bm, x01, yc, x02, yc, color);
  return 0;
}

//from pix unit (imr) coordinates it places a rect on right position in the image
int sdi_v2_draw_one_circle_with_user_pix(int xc,int yc,int radius, int color, O_i *oi, BITMAP *imb)
{
  int xc2, yc2, radius2;
  xc2 = sdi_v2_x_user_pix_2_vga_screen_unit(imb, oi, xc);
  radius2 = sdi_v2_dx_user_pix_2_vga_screen_unit(imb, oi, radius);
  yc2 = sdi_v2_y_user_pix_2_vga_screen_unit(imb, oi, yc);
  sdi_v2_draw_one_circle_vga_screen_unit(xc2, yc2, radius2, color, imb);
  return 0;
}

int sdi_v2_draw_one_circle_vga_screen_unit(int xc, int yc, int radius, int color, BITMAP *bmp)
{
    circle(bmp, xc, yc, radius, color);
    return 0;
}

//already allocated
//position will be counted from the opposite side : must be understood as shift from right-bottom corner of the futur dialog window
int sdi_v2_init_one_panel(sdi_v2_p_info *panel, int width, int height, int x, int y)
{
  if (sdi_colormap == NULL) sdi_v2_define_sdi_colormap();
  panel->color_bgd = sdi_colormap[8];
  panel->color_txt = sdi_colormap[9];
  panel->w = width;
  panel->h = height;
  panel->x = x ;
  panel->y = y ;
  panel->bmp = create_bitmap(panel->w, panel->h);
  return 0;
}

int sdi_v2_draw_one_filled_rect_vga_screen_unit(int xc, int yc, int half_length, int half_width, int color, BITMAP* bmp)
{
  int x1, x2, y1, y2;
  x1=xc-half_length;
  x2=xc+half_length;
  y1=yc-half_width;
  y2=yc+half_width;
  rectfill(bmp, x1, y1, x2, y2, color);
  return 0;
}

int sdi_v2_add_text_vga_screen_unit(BITMAP *bmp, int xc, int yc, int color, const char *format, ...)
{
va_list ap;
char vtxt[32];
va_start(ap, format);
vsnprintf(vtxt,2048,format,ap);
va_end(ap);
textprintf_centre_ex(bmp,large_font, xc, yc,color,-1,vtxt);
return 0;
}

int sdi_v2_add_text_vga_screen_unit_in_oi(O_i *oi, int xc, int yc, int color, const char *format, ...)
{
  va_list ap;
  BITMAP *imb=NULL;
  int x_txt,y_txt;
  char vtxt[32];

  va_start(ap, format);
  vsnprintf(vtxt,2048,format,ap);
  va_end(ap);
  if ((oi->bmp.to == IS_BITMAP) &&  (oi->bmp.stuff != NULL))
  {
    imb = (BITMAP*)oi->bmp.stuff;
    x_txt= sdi_v2_x_user_pix_2_vga_screen_unit(imb,oi,xc);
    y_txt= sdi_v2_y_user_pix_2_vga_screen_unit(imb,oi,yc);
    textprintf_centre_ex(imb,large_font,x_txt,y_txt,color,-1,vtxt);
  }
  return 0;
}

/*
int	draw_one_ellipse_vga_screen_unit(int xc, int yc, int radius_x, int radius_y, int color, BITMAP* bmp)
{
  ellipse(bmp,xc,yc,radius_x,radius_y,color);
  return 0;
}*/
