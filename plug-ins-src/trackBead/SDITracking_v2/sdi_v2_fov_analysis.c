# include "sdi_v2_fov_analysis.h"

extern int *sdi_colormap;


//used for hairpin blockage experiments
int sdi_v2_classify_beads_dsout(sdi_v2_fov *fov, d_s *zmag, int verbose, int replace, pltreg **plr)
{
  int i;
  static int count=0;
  fov->n_opening_hairpins = 0;
  fov->n_ref_beads = 0;
  O_p *op_tmp = NULL;
  O_p *op1 = NULL; //always at least one plot..
  //handles the stacking or not of plots from one job to the next one
  if (fov == NULL || plr == NULL || zmag == NULL) return 1;
  if (verbose > 0)
  {
    if (*plr == NULL) *plr = create_hidden_pltreg_with_op(&op1, 1, 1, 0, "beads proc results");
    else
    {
      op1 = create_and_attach_one_plot(*plr,1,1,0);
      if (replace == 1) while ( (*plr)->n_op > 1) remove_data (*plr, IS_ONE_PLOT, (void*)((*plr)->o_p[0]));
    }
    add_data_to_one_plot(op1, IS_DATA_SET, duplicate_data_set(zmag, NULL));
    set_ds_source(zmag, "zmag");
    set_plot_title(op_tmp, "Zmag.exp%d", count);
  }

  float std_y=0, std_x=0, std_z=0;
  d_s *tmp=NULL;
  float h_x_min, h_x_max, dist_to_expected;
  for (i=0;i<fov->n_beads;i++)
  {
    //detects if this molecule opens properly : computes the histogrom of z(t) during cycles with magnets and look if the distance between xmin and xma is the expected elongation when opening
    tmp = sdi_v2_compute_histo_of_tab(fov->beads_list[i]->ds_out1->yd, fov->beads_list[i]->ds_out1->nx);
    h_x_min = tmp->xd[0];
    h_x_max = tmp->xd[tmp->nx-1];
    dist_to_expected = fabs( fabs(h_x_max - h_x_min) - fov->diagnose->z_expected_range);
    //max min pour control? todo?
    //computes the sigma on x and y to eventually get ref beads when not opening
    sdi_v2_estimate_std(fov->beads_list[i]->ds_out1->yd, fov->beads_list[i]->ds_out1->nx, &std_z);
    sdi_v2_estimate_std(fov->beads_list[i]->ds_out2->yd, fov->beads_list[i]->ds_out2->nx, &std_x);
    sdi_v2_estimate_std(fov->beads_list[i]->ds_out3->yd, fov->beads_list[i]->ds_out3->nx, &std_y);

    if (fov->beads_list[i]->ds_out1->yd[0]!=999) //z(t)
    {
      if (dist_to_expected<=fov->diagnose->z_range_tol)
      {
        fov->beads_list[i]->bead_class |= SDI_OPENING_BEAD_1;
        fov->n_opening_hairpins ++;
      }
      else
      {
        fov->beads_list[i]->bead_class &= ~SDI_OPENING_BEAD_1;
        if (std_z <= fov->diagnose->std_threshold_z)
        {
          fov->beads_list[i]->bead_class |= SDI_REFERENCE_BEAD;
          fov->n_ref_beads ++;
        }
        else fov->beads_list[i]->bead_class &= ~SDI_REFERENCE_BEAD;
        /*if (fov->beads_list[i]->ds_out2->yd[0]!=999) //x(t)
        {
          if (std_x <= fov->diagnose->std_threshold_x) fov->beads_list[i]->bead_class |= SDI_REFERENCE_BEAD;
          else fov->beads_list[i]->bead_class &= ~SDI_REFERENCE_BEAD;
        }
        if (fov->beads_list[i]->ds_out3->yd[0]!=999) //y(t)
        {
          if (std_y <= fov->diagnose->std_threshold_y) fov->beads_list[i]->bead_class |= SDI_REFERENCE_BEAD;
          else fov->beads_list[i]->bead_class &= ~SDI_REFERENCE_BEAD;
        }*/
      }
    }
    if (verbose==1)
    {
      op_tmp = create_and_attach_one_plot(*plr,1,1,0);
      set_ds_source(fov->beads_list[i]->ds_out1, "z(t).bd%d", fov->beads_list[i]->index_in_fov);
      add_data_to_one_plot(op_tmp, IS_DATA_SET, duplicate_data_set(tmp,NULL));
      set_plot_title(op_tmp, "z(t).bd%d.exp%d.distz%f.sz%f.sx%f.sy%f", count, fov->beads_list[i]->index_in_fov,dist_to_expected,std_z,std_x,std_y);
    }
    free_data_set(fov->beads_list[i]->ds_out1);
    fov->beads_list[i]->ds_out1 = NULL;
    free_data_set(fov->beads_list[i]->ds_out2);
    fov->beads_list[i]->ds_out2 = NULL;
    free_data_set(fov->beads_list[i]->ds_out3);
    fov->beads_list[i]->ds_out3 = NULL;
  }
  count ++;
  return 0;
}


int sdi_v2_compute_0_of_beads_dsout(sdi_v2_fov *fov, int verbose, int replace, pltreg **plr)
{
  int i;
  static int count = 0;
  d_s *tmp = NULL;
  O_p *op_tmp = NULL;
  O_p *op1 = NULL; //always at least one plot..
  O_p *op_tmp2 = NULL;
  //handles the stacking or not of plots from one job to the next one
  if (fov == NULL || plr == NULL) return 1;
  if (verbose > 0)
  {
    if (*plr == NULL) *plr = create_hidden_pltreg_with_op(&op1, 1, 1, 0, "beads proc results");
    else
    {
      op1 = create_and_attach_one_plot(*plr,1,1,0);
      if (replace == 1) while ( (*plr)->n_op > 1) remove_data (*plr, IS_ONE_PLOT, (void*)((*plr)->o_p[0]));
    }
  }

  for (i=0;i<fov->n_beads;i++)
  {
    tmp = sdi_v2_compute_histo_of_tab(fov->beads_list[i]->ds_out1->yd, fov->beads_list[i]->ds_out1->nx);
    fov->beads_list[i]->z_offset += tmp->xd[0];
    if (verbose > 0)
    {
      op_tmp = create_and_attach_one_plot(*plr,1,1,0);
      set_ds_source(tmp, "Histo.z(t).bead%d", fov->beads_list[i]->index_in_fov);
      set_plot_title(op_tmp, "Histo.z(t).beads.exp%d", (int)count);
      set_plot_filename(op_tmp, "Histo.z(t).beads.exp%d : %d", (int)count);
      add_data_to_one_plot(op_tmp, IS_DATA_SET, duplicate_data_set(tmp, NULL));
      op_tmp2 = create_and_attach_one_plot(*plr,1,1,0);
      set_plot_title(op_tmp2, "z(t).beads.exp%d", count);
      set_plot_filename(op_tmp2, "z(t).beads.exp%d : %d", (int)count);
      add_data_to_one_plot(op_tmp2, IS_DATA_SET, duplicate_data_set(fov->beads_list[i]->ds_out1, NULL));
      add_data_to_one_plot(op_tmp, IS_DATA_SET, duplicate_data_set(tmp, NULL));
      add_data_to_one_plot(op1, IS_DATA_SET, duplicate_data_set(tmp, NULL));
    }
    free_data_set(fov->beads_list[i]->ds_out1);
    fov->beads_list[i]->ds_out1 = NULL;
  }
  count ++;
  return 0;
}

int sdi_v2_compute_std_of_beads_dsout(sdi_v2_fov *fov, int verbose, int replace, pltreg **plr)
{
  int i;
  static int count=0;
  O_p *op_tmp = NULL;
  O_p *op1 = NULL; //always at least one plot..
  if (fov == NULL || plr == NULL || fov->diagnose == NULL) return D_O_K;

  //handles the stacking or not of plots from one job to the next one
  if (verbose > 0)
  {
    if (*plr == NULL) *plr = create_hidden_pltreg_with_op(&op1, 1, 1, 0, "beads proc results");
    else
    {
      op1 = create_and_attach_one_plot(*plr,1,1,0);
      if (replace == 1) while ( (*plr)->n_op > 1) remove_data (*plr, IS_ONE_PLOT, (void*)((*plr)->o_p[0]));
    }
  }

  float std_z=0, std_y=0, std_x=0;

  for (i=0;i<fov->n_beads;i++)
  {
    sdi_v2_estimate_std(fov->beads_list[i]->ds_out1->yd, fov->beads_list[i]->ds_out1->nx, &std_z);
    sdi_v2_estimate_std(fov->beads_list[i]->ds_out2->yd, fov->beads_list[i]->ds_out2->nx, &std_x);
    sdi_v2_estimate_std(fov->beads_list[i]->ds_out3->yd, fov->beads_list[i]->ds_out3->nx, &std_y);
    if (verbose==1)
    {
      op_tmp = create_and_attach_one_plot(*plr,1,1,0);
      set_ds_source(fov->beads_list[i]->ds_out1, "z(t).bd%d", fov->beads_list[i]->index_in_fov);
      add_data_to_one_plot(op_tmp, IS_DATA_SET, duplicate_data_set(fov->beads_list[i]->ds_out1, NULL));
      set_ds_source(fov->beads_list[i]->ds_out2, "x(t).bd%d", fov->beads_list[i]->index_in_fov);
      add_data_to_one_plot(op_tmp, IS_DATA_SET, duplicate_data_set(fov->beads_list[i]->ds_out2, NULL));
      set_ds_source(fov->beads_list[i]->ds_out3, "y(t).bd%d", fov->beads_list[i]->index_in_fov);
      add_data_to_one_plot(op_tmp, IS_DATA_SET, duplicate_data_set(fov->beads_list[i]->ds_out3, NULL));
      set_plot_title(op_tmp, "xyz(t).beads.exp%d", count);
    }

    fov->beads_list[i]->bead_class &= ~SDI_OPENING_BEAD_1;
    //depending on what position (xz and/or y) is tracked one can detect the mobile or not with different test
    if (fov->beads_list[i]->ds_out1->yd[0]!=999)
    {
      if (std_z<fov->diagnose->std_threshold_z) fov->beads_list[i]->bead_class |= SDI_REFERENCE_BEAD;
      else fov->beads_list[i]->bead_class &= ~SDI_REFERENCE_BEAD;
    }
    if (fov->beads_list[i]->ds_out2->yd[0]!=999)
    {
      if (std_x<fov->diagnose->std_threshold_z) fov->beads_list[i]->bead_class |= SDI_REFERENCE_BEAD;
      else fov->beads_list[i]->bead_class &= ~SDI_REFERENCE_BEAD;
    }
    if (fov->beads_list[i]->ds_out3->yd[0]!=999)
    {
      if (std_y<fov->diagnose->std_threshold_z) fov->beads_list[i]->bead_class |= SDI_REFERENCE_BEAD;
      else fov->beads_list[i]->bead_class &= ~SDI_REFERENCE_BEAD;
    }

    free_data_set(fov->beads_list[i]->ds_out1);
    fov->beads_list[i]->ds_out1 = NULL;
    free_data_set(fov->beads_list[i]->ds_out2);
    fov->beads_list[i]->ds_out2 = NULL;
    free_data_set(fov->beads_list[i]->ds_out3);
    fov->beads_list[i]->ds_out3 = NULL;

  }
  count ++;
  return 0;
}

int sdi_v2_y_bead_detection_gui(sdi_v2_y_b_d *sdi_y_b_d,  int verbose, bool from_cfg)
{
  if (sdi_y_b_d == NULL) return D_O_K;
  static float correlation_min = 0.8;
  static int replace = 0;
  static int dist_y_min = 8;
  static int dist_y_max = 30;
  static int dist_x_max = 100;
  if (sdi_y_b_d == NULL) return 1;
  if (from_cfg== true)
  {
    correlation_min = get_config_float("SDI", "y_correlation_min", 1.);
    dist_y_min = get_config_int("SDI", "dist_y_min", 1);
    dist_y_max = get_config_int("SDI", "dist_y_max", 1);
    dist_x_max = get_config_int("SDI", "dist_x_max", 1);
    //verbose = get_config_int("SDI", "verbose", 1);
    replace = 0;
  }
  if (verbose==1)
  {
    if (win_scanf("this gui configures the detection of beads by correlation of brownian motion of y profiles\n"
    "do you want to see results %R:no %r:yes\n"
    "if yes do you want to %R:replace, %r:add plots\n"
    "enter the threshold (min) of accepted correlation %2f\n"
    "maximum subregion along y ymin:%d ymax:%d\n"
    "maximum subregion along x:%d", &verbose, &replace, &correlation_min, &dist_y_min, &dist_y_max, &dist_x_max) == WIN_CANCEL) return D_O_K;
  }

  if (from_cfg== true)
  {
    set_config_float("SDI", "y_correlation_min",correlation_min);
    set_config_int("SDI", "dist_y_min", dist_y_min);
    set_config_int("SDI", "dist_y_max", dist_y_max);
    set_config_int("SDI", "dist_x_max", dist_x_max);
    //verbose = get_config_int("SDI", "verbose", 1);
  }
  sdi_y_b_d->correlation_min = correlation_min;
  sdi_y_b_d->dist_y_min = dist_y_min;
  sdi_y_b_d->dist_y_max = dist_y_max;
  sdi_y_b_d->dist_x_max = dist_x_max;
  sdi_y_b_d->verbose = verbose;
  sdi_y_b_d->replace = replace;
  return 0;
}
int sdi_v2_fringes_detection_gui(sdi_v2_f_d *sdi_f_d)
{
  if (sdi_f_d == NULL) return D_O_K;
  static int guess_dx = 50, guess_dy = 13, intensity_threshold_detect = 80;
  static float intensity_min_filter = 20, intensity_max_filter = 230, contrast_threshold_filter = 0.3;
  if(win_scanf("this gui configures fringes detection parameters\n"
                "enter the intensity threshold for detection %3d\n"
                "and roughly the expected distance between fringes along x%3d and y %3d\n"
                "then config the parameters to filter the bad fringes\n"
                "contrast threshold %3f, intensity min %3f and max %3f",
                &intensity_threshold_detect, &guess_dx, &guess_dy, &contrast_threshold_filter, &intensity_min_filter,
                &intensity_max_filter) == WIN_CANCEL) return 1;
  sdi_f_d->intensity_threshold_detect = intensity_threshold_detect;
  sdi_f_d->guess_dx = guess_dx;
  sdi_f_d->guess_dy = guess_dy;
  sdi_f_d->intensity_min_filter = intensity_min_filter;
  sdi_f_d->intensity_max_filter = intensity_max_filter;
  sdi_f_d->contrast_threshold_filter = contrast_threshold_filter;
  return 0;
}
///////////////////////////detection of fringes////////////////////////////
///////////////////////////////////////////////////////////////////////////
int sdi_v2_detect_fringes(O_i *oi, sdi_v2_fov *fov, sdi_v2_f_d *sdi_f_d)
{
  //BITMAP *bmp;
  int *x_list=NULL;
  int *y_list=NULL;
  int number=0;
  int  i;
  if (fov == NULL || sdi_f_d == NULL || oi == NULL) return 1;
  if (fov->sdi_param == NULL)  return D_O_K;

  x_list=calloc(32768, sizeof(int));
  y_list=calloc(32768, sizeof(int));
  sdi_v2_oi_char_find_isolated_maxima(oi, x_list, y_list, &number, sdi_f_d->intensity_threshold_detect, sdi_f_d->guess_dx, sdi_f_d->guess_dy);
  //in a first step, one creates fringe patterns to check the quality of the signal and discard the bad ones
  int dx_safe = fov->sdi_param->x_roi_size_2+2;
  int dy_safe = fov->sdi_param->y_roi_size_2_Y+2;
  for(i=0 ; i<number ; i++)
  {
    if (dx_safe < x_list[i] && x_list[i] < (oi->im.nx - dx_safe) && dy_safe < y_list[i] && y_list[i] < (oi->im.ny - dy_safe)) sdi_v2_add_one_sdi_fringes_to_fov_by_position(fov, x_list[i], y_list[i]);
  }
  sdi_v2_update_all_sdi_fringes_parameters(fov, fov->sdi_param);
  for(i=0 ; i<fov->n_fringes ; i++)
  {
    sdi_v2_center_roi_y_fringes( oi, fov->sdi_param, fov->fringes_list[i]);
    sdi_v2_center_roi_x_fringes( oi, fov->sdi_param, fov->fringes_list[i]);//this contains one call to sdi algo
  }
  //remove some fringes whose position, after centrum shifted by sdi algo, is dangerous (close to image limit) or absurd
  i = 0;
  while( i< fov->n_fringes )
  {
    if ((fov->fringes_list[i]->xc <= dx_safe) || (fov->fringes_list[i]->xc >= (oi->im.nx - dx_safe))
            || (fov->fringes_list[i]->yc <= dy_safe) || (fov->fringes_list[i]->yc >= (oi->im.ny - dy_safe)))
    {
      sdi_v2_remove_one_sdi_fringes_from_fov(fov, i);
      i--;
    }
    i++;
  }
  free(x_list);
  x_list=NULL;
  free(y_list);
  y_list=NULL;
  return 0;
}
//filter bad fringes that the detection may have found :
//with too big/small intensity, low contrast
int sdi_v2_filter_bad_fringes(sdi_v2_fov *fov, sdi_v2_f_d *sdi_f_d)
{
  int i=0;
  if (fov == NULL || sdi_f_d == NULL) return 1;
  while (i< fov->n_fringes)
  {
    if (fov->fringes_list[i]->contrast_t[fov->fringes_list[i]->c_phi_t] < sdi_f_d->contrast_threshold_filter
        || fov->fringes_list[i]->I_max_t[fov->fringes_list[i]->c_phi_t] > sdi_f_d->intensity_max_filter
        || fov->fringes_list[i]->I_max_t[fov->fringes_list[i]->c_phi_t] < sdi_f_d->intensity_min_filter)
    {
      sdi_v2_remove_one_sdi_fringes_from_fov(fov, i);
      i--;
    }
    i++;
  }
  return 0;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
int sdi_v2_track_fringes_fov(sdi_v2_fov *fov, O_i *oi)
{
  int i, j , k;
  if (fov == NULL || oi == NULL) return D_O_K;
  if (fov->n_fringes == 0) return D_O_K;
  int nf = oi->im.n_f;
  //if (win_scanf("enter the number of frames to be processed %d", &nf)==WIN_CANCEL) return D_O_K;
  if (sdi_v2_buffer_config(fov, nf)==1)  for (i=0; i<fov->n_fringes ;i++) sdi_v2_allocate_sdi_v2_fringes_output_t_fields(fov->fringes_list[i], nf);
  for (i = 0 ; i<1 ; i++)
  {
    switch_frame(oi, i);
    for (j=0 ; j<fov->n_fringes ; j++)
    {
      //the first point is here the ref points for 2pi multiple correction
      if (i == 0)
      {
        fov->fringes_list[j]->need_background = true;
        fov->fringes_list[j]->need_reference_point = true;
      }
      else
      {
        fov->fringes_list[j]->need_background = true;
        fov->fringes_list[j]->need_reference_point = true;
      }
      sdi_v2_update_c_profile_fringes_points(fov->fringes_list[j], i);
      sdi_v2_extract_sdi_fringes_raw_x_profile_from_image(oi, fov->fringes_list[j], fov->sdi_param);
      sdi_v2_process_sdi_fringes_x_profile(fov->fringes_list[j], fov->sdi_param);
      sdi_v2_compute_phase_coeff_of_sdi_fringes(fov->fringes_list[j], fov->sdi_param);
      sdi_v2_compute_x_z_coupled(fov->fringes_list[j]);
    }
    for(k=0;k<fov->n_display;k++)//update the display object that are in fov
    {
      sdi_v2_update_x_display( fov->display_list[k], i, i);
      sdi_v2_update_y_display( fov->display_list[k], i);
    }
  }
  return 0;
}

//track a recorded movie with fringes output non empty to compare
int sdi_v2_track_bead_movie_fov(sdi_v2_fov *fov, O_i *oi)
{
  int i, j, k;
  int nf = oi->im.n_f;

  if (fov == NULL || oi == NULL) return 1;
  //if (win_scanf("enter the number of frames to be processed %d", &nf)==WIN_CANCEL) return D_O_K;
  if (sdi_v2_buffer_config(fov, nf)==1)
  {
    //allocates at least the output of fringes objects for linearity check of each fr of a bead
    for (i=0; i<fov->n_fringes ;i++) sdi_v2_allocate_sdi_v2_fringes_output_t_fields(fov->fringes_list[i], nf);
  }

  for (i = 0 ; i<nf ; i++)
  {
    switch_frame(oi, i);
    for (j=0 ; j<fov->n_beads ; j++)
    {
      if (i==0)
      {
        fov->beads_list[j]->fringes_bead[0]->need_reference_point = true;
        fov->beads_list[j]->fringes_bead[1]->need_reference_point = true;
        fov->beads_list[j]->fringes_bead[0]->need_background = true;
        fov->beads_list[j]->fringes_bead[1]->need_background = true;
      }
      else
      {
        fov->beads_list[j]->fringes_bead[0]->need_reference_point = false;
        fov->beads_list[j]->fringes_bead[1]->need_reference_point = false;
        fov->beads_list[j]->fringes_bead[0]->need_background = false;
        fov->beads_list[j]->fringes_bead[1]->need_background = false;
      }
      sdi_v2_update_c_profile_fringes_points(fov->beads_list[j]->fringes_bead[0], i);
      sdi_v2_extract_sdi_fringes_raw_x_profile_from_image(oi, fov->beads_list[j]->fringes_bead[0], fov->sdi_param);
      sdi_v2_process_sdi_fringes_x_profile(fov->beads_list[j]->fringes_bead[0], fov->sdi_param);
      sdi_v2_compute_phase_coeff_of_sdi_fringes(fov->beads_list[j]->fringes_bead[0], fov->sdi_param);
      sdi_v2_compute_x_z_coupled(fov->beads_list[j]->fringes_bead[0]);

      sdi_v2_update_c_profile_fringes_points(fov->beads_list[j]->fringes_bead[1], i);
      sdi_v2_extract_sdi_fringes_raw_x_profile_from_image(oi, fov->beads_list[j]->fringes_bead[1], fov->sdi_param);
      sdi_v2_process_sdi_fringes_x_profile(fov->beads_list[j]->fringes_bead[1], fov->sdi_param);
      sdi_v2_compute_phase_coeff_of_sdi_fringes(fov->beads_list[j]->fringes_bead[1], fov->sdi_param);
      sdi_v2_compute_x_z_coupled(fov->beads_list[j]->fringes_bead[1]);

      sdi_v2_update_c_profile_bead_points(fov->beads_list[j], i);
      sdi_v2_compute_z_bead(fov->beads_list[j]);

      for(k=0;k<fov->n_display;k++)//update the display object that are in fov
      {
        sdi_v2_update_x_display( fov->display_list[k], i, i);
        sdi_v2_update_y_display( fov->display_list[k], i);
      }
    }
  }
  return 0;
}
int sdi_v2_remove_all_intermediate_signal_of_y_profiles(sdi_v2_fov *fov)
{
  int i;

  if (fov == NULL) return D_O_K;
  for (i=0;i<fov->n_y_profile;i++)
  {
    sdi_v2_remove_all_display_of_one_sdi_y_profile_from_fov(fov, i);
    sdi_v2_reset_all_intermediate_data_of_y_profile(fov->y_profile_list[i]);
  }
  return 0;
}
int sdi_v2_remove_all_intermediate_signal_of_fringes(sdi_v2_fov *fov)
{
  int i;
  for (i=0;i<fov->n_fringes;i++)
  {
    sdi_v2_remove_all_display_of_one_sdi_fringes_from_fov(fov, i);
    sdi_v2_reset_all_intermediate_data_of_fringes(fov->fringes_list[i]);
  }
  return 0;
}

//when tracking on a recorded movie one must erase the different buffer and then reinitialize it with the correct sizes
int sdi_v2_buffer_config(sdi_v2_fov *fov, int length)
{
  int i, out=0;
  //detects whether it is new general buffer size
  if (fov == NULL) return D_O_K;
  if (length != fov->buffer_size) //then new buffer size for fov : previous buffer size must be discarded
  {
    sdi_v2_remove_all_intermediate_signal_of_fringes(fov);
    sdi_v2_remove_all_intermediate_signal_of_y_profiles(fov);
    sdi_v2_remove_all_data_of_beads(fov);
    fov->buffer_size = length;
    out = 1;
    //allocates strictly the minimum : output of beads
    for(i=0;i<fov->n_beads;i++) sdi_v2_allocate_sdi_v2_bead_output_t_fields( fov->beads_list[i], fov->buffer_size);
  }
  return out;
}

//ok : 1205 data_for_didier
int sdi_v2_calibration_of_fringes_fov(sdi_v2_fov *fov, O_i *oi, float *z)
{
  int i, j, k;
  bool z_initially_null = false;
  if (fov == NULL || oi == NULL) return D_O_K;
  if (fov->n_beads == 0)
  {
    win_printf_OK("no bead..");
    return D_O_K;
  }
  static float z_step = 0.1;
  int n_f = oi->im.n_f;
  int offset=0;
  if (z == NULL)  if (win_scanf("your z values are empty, you can specify a step %f and i will continue with a symetric vector of points", &z_step)== WIN_CANCEL) return D_O_K;
  win_printf_OK("length buf of fov %d", fov->buffer_size);
  if (sdi_v2_buffer_config(fov, n_f)==1)  for (i=0; i<fov->n_fringes ;i++) sdi_v2_allocate_sdi_v2_fringes_phi_t_fields(fov->fringes_list[i], n_f);
  if (z == NULL)
  {
    z_initially_null = true;
    z = (float *)calloc(n_f, sizeof(float));
    for (i=0;i<n_f;i++) z[i] = z_step * (i - (int)(oi->im.m_f / 2));
  }
  for (i = 0 ; i<n_f ; i++)
  {
    switch_frame(oi, i + offset);
    for (j=0 ; j<fov->n_fringes ; j++)
    {
      sdi_v2_update_c_profile_fringes_points(fov->fringes_list[j], i);
      sdi_v2_extract_sdi_fringes_raw_x_profile_from_image(oi, fov->fringes_list[j], fov->sdi_param);
      sdi_v2_process_sdi_fringes_x_profile(fov->fringes_list[j], fov->sdi_param);
      sdi_v2_compute_phase_coeff_of_sdi_fringes(fov->fringes_list[j], fov->sdi_param);
    }
  }
  //after the first range, computation of avg frequency
  for (i=0; i<fov->n_fringes; i++)
  {
    sdi_v2_frequency_calibration_of_fringes(fov->fringes_list[i]);
    sdi_v2_compute_v_x_of_fringes(fov->fringes_list[i], fov->sdi_param); //calibration in x
  }
  //then second one, collecting phase fitted around the right frequency
  for (i = 0 ; i<n_f ; i++)
  {
    switch_frame(oi, i);
    for (j=0 ; j<fov->n_fringes ; j++)
    {
      sdi_v2_update_c_profile_fringes_points(fov->fringes_list[j], i);
      sdi_v2_extract_sdi_fringes_raw_x_profile_from_image(oi, fov->fringes_list[j], fov->sdi_param);
      sdi_v2_process_sdi_fringes_x_profile(fov->fringes_list[j], fov->sdi_param);
      sdi_v2_compute_phase_coeff_of_sdi_fringes(fov->fringes_list[j], fov->sdi_param);
    }
    for(k=0;k<fov->n_display;k++)//update the display object that are in fov
    {
      sdi_v2_update_x_display( fov->display_list[k], i, i);
      sdi_v2_update_y_display( fov->display_list[k], i);
    }
  }
  //and then calibration of velocities
  for (i=0; i<fov->n_fringes; i++)
  {
    sdi_v2_unwrap( fov->fringes_list[i]->corrected_phi_0_t, 0, n_f, 1);
    sdi_v2_linear_calibration_of_sdi_fringes(fov->fringes_list[i], z, fov->fringes_list[i]->corrected_phi_0_t, fov->fringes_list[i]->raw_phi_1_t, n_f);
  }
  if (z_initially_null == true)
  {
    free(z);
    z = NULL;
  }
  return 0;
}

int sdi_v2_fov_calibration_spatial_effect(sdi_v2_fov *fov, pltreg **plr, int replace, float *z)
{
  int i, j;
  static int count=0;
  O_p *op1 = NULL; //always at least one plot..
  if (fov == NULL || z == NULL) return D_O_K;
  if (*plr == NULL) *plr = create_hidden_pltreg_with_op(&op1, 1, 1, 0, "ramp proc results");
  else
  {
    op1 = create_and_attach_one_plot(*plr,1,1,0);
    if (replace == 1) while ( (*plr)->n_op > 1) remove_data (*plr, IS_ONE_PLOT, (void*)((*plr)->o_p[0]));
  }
  ////////plots of distribution of the fringes calibration factors
  //phase velocity first (fringes->v_0, in rad per microns)
  d_s *v_0_x_pos = NULL;
  d_s *v_0_x_neg = NULL;
  v_0_x_pos = build_adjust_data_set(v_0_x_pos, fov->n_beads, fov->n_beads);
  v_0_x_neg = build_adjust_data_set(v_0_x_neg, fov->n_beads, fov->n_beads);
  for (i=0;i<fov->n_beads ;i++)
  {
    v_0_x_pos->xd[i] = fov->beads_list[i]->fringes_bead[0]->xc;
    v_0_x_pos->yd[i] = fov->beads_list[i]->fringes_bead[0]->v_0;
    v_0_x_neg->xd[i] = fov->beads_list[i]->fringes_bead[1]->xc;
    v_0_x_neg->yd[i] = -fov->beads_list[i]->fringes_bead[1]->v_0;
  }
  if (fov->n_beads > 1)
  {
    QuickSort_double(v_0_x_pos->xd, v_0_x_pos->yd, 0, v_0_x_pos->nx - 1);
    QuickSort_double(v_0_x_neg->xd, v_0_x_neg->yd, 0, v_0_x_neg->nx - 1);
  }
  //char result[64];
  float mean_pos, mean_neg, sigma_pos, sigma_neg;
  sdi_v2_estimate_mean(v_0_x_pos->yd, v_0_x_pos->nx, &mean_pos);
  sdi_v2_estimate_mean(v_0_x_neg->yd, v_0_x_neg->nx, &mean_neg);
  sdi_v2_estimate_std(v_0_x_pos->yd, v_0_x_pos->nx, &sigma_pos);
  sdi_v2_estimate_std(v_0_x_neg->yd, v_0_x_neg->nx, &sigma_neg);
  win_printf_OK("result v+=%f sig+=%f v-=%f sig-=%f", mean_pos, sigma_pos, mean_neg, sigma_neg);
  //snprintf(result, 64, "v0+ = %e, sigma+ = %e : v0- = %e, sigma- = %e", result, mean_pos, sigma_pos, mean_neg, sigma_neg);
  //push_plot_label(op1, op1->x_lo + (op1->x_hi - op1->x_lo) / 4, op1->y_hi - (op1->y_hi - op1->y_lo) / 4, result, USR_COORD);
  set_ds_source(v_0_x_pos, "phi vel +");
  set_ds_source(v_0_x_neg, "phi vel -");
  set_plot_title(op1, "phase velocity(x):job%d", count);
  set_op_filename(op1, "phase_velocity(x)_job%d.gr", count);
  set_plot_x_title(op1, "fringes position x (pix)");
  set_plot_y_title(op1, "phase velocity (rad per mic)");
  add_data_to_one_plot(op1, IS_DATA_SET, v_0_x_pos);
  add_data_to_one_plot(op1, IS_DATA_SET, v_0_x_neg);

  //group velocity (fringes->v_1, in pix per microns)
  d_s *v_1_x_pos = NULL;
  d_s *v_1_x_neg = NULL;
  v_1_x_pos = build_adjust_data_set(v_1_x_pos, fov->n_beads, fov->n_beads);
  v_1_x_neg = build_adjust_data_set(v_1_x_neg, fov->n_beads, fov->n_beads);
  for (i=0;i<fov->n_beads ;i++)
  {
    v_1_x_pos->xd[i] = fov->beads_list[i]->fringes_bead[0]->xc;
    v_1_x_pos->yd[i] = fov->beads_list[i]->fringes_bead[0]->v_1;
    v_1_x_neg->xd[i] = fov->beads_list[i]->fringes_bead[1]->xc;
    v_1_x_neg->yd[i] = -fov->beads_list[i]->fringes_bead[1]->v_1;
  }
  if (fov->n_beads > 1)
  {
    QuickSort_double(v_1_x_pos->xd, v_1_x_pos->yd, 0, v_1_x_pos->nx - 1);
    QuickSort_double(v_1_x_neg->xd, v_1_x_neg->yd, 0, v_1_x_neg->nx - 1);
  }
  O_p *op2 = NULL;
  op2 = create_and_attach_one_plot(*plr,1,1,0);
  set_ds_source(v_1_x_pos, "group vel +");
  set_ds_source(v_1_x_neg, "group vel -");
  set_plot_title(op2, "group velocity(x):job%d", count);
  set_op_filename(op2, "group velocity(x)%d.gr", count);
  set_plot_x_title(op2, "fringes position x (pix)");
  set_plot_y_title(op2, "group velocity (pix per mic)");
  add_data_to_one_plot(op2, IS_DATA_SET, v_1_x_pos);
  add_data_to_one_plot(op2, IS_DATA_SET, v_1_x_neg);

  //frequency (fringes->v0, in pix per microns)
  d_s *nu0_x_pos = NULL;
  d_s *nu0_x_neg = NULL;
  nu0_x_pos = build_adjust_data_set(nu0_x_pos, fov->n_beads, fov->n_beads);
  nu0_x_neg = build_adjust_data_set(nu0_x_neg, fov->n_beads, fov->n_beads);
  for (i=0;i<fov->n_beads ;i++)
  {
    nu0_x_pos->xd[i] = fov->beads_list[i]->fringes_bead[0]->xc;
    nu0_x_pos->yd[i] = fov->beads_list[i]->fringes_bead[0]->nu0;
    nu0_x_neg->xd[i] = fov->beads_list[i]->fringes_bead[1]->xc;
    nu0_x_neg->yd[i] = fov->beads_list[i]->fringes_bead[1]->nu0;
  }
  if (fov->n_beads > 1)
  {
    QuickSort_double(nu0_x_pos->xd, nu0_x_pos->yd, 0, nu0_x_pos->nx - 1);
    QuickSort_double(nu0_x_neg->xd, nu0_x_neg->yd, 0, nu0_x_neg->nx - 1);
  }
  O_p *op4 = NULL;
  op4 = create_and_attach_one_plot(*plr,1,1,0);
  set_ds_source(nu0_x_pos, "nu0 +");
  set_ds_source(nu0_x_neg, "nu0 -");
  set_plot_title(op4, "nu0(x):job%d", count);
  set_op_filename(op4, "nu0(x)%d.gr", count);
  set_plot_x_title(op4, "fringes position x (pix)");
  set_plot_y_title(op4, "nu0(pix-1c)");
  add_data_to_one_plot(op4, IS_DATA_SET, nu0_x_pos);
  add_data_to_one_plot(op4, IS_DATA_SET, nu0_x_neg);
  float mean_nu_pos, mean_nu_neg, sigma_nu_pos, sigma_nu_neg;
  sdi_v2_estimate_mean(nu0_x_pos->yd, nu0_x_pos->nx, &mean_nu_pos);
  sdi_v2_estimate_mean(nu0_x_neg->yd, nu0_x_neg->nx, &mean_nu_neg);
  sdi_v2_estimate_std(nu0_x_pos->yd, nu0_x_pos->nx, &sigma_nu_pos);
  sdi_v2_estimate_std(nu0_x_neg->yd, nu0_x_neg->nx, &sigma_nu_neg);
  win_printf_OK("result nu+=%f sig+=%f nu-=%f sig-=%f", mean_nu_pos, sigma_nu_pos, mean_nu_neg, sigma_nu_neg);

  //balance of fringes calibration factors for beads, along x
  d_s *delta_v0 = NULL;
  d_s *mean_v0 = NULL;
  delta_v0 = build_adjust_data_set(delta_v0, fov->n_beads, fov->n_beads);
  mean_v0 = build_adjust_data_set(mean_v0, fov->n_beads, fov->n_beads);
  for (i=0;i<fov->n_beads;i++)
  {
    delta_v0->xd[i] = fov->beads_list[i]->xc;
    delta_v0->yd[i] = fov->beads_list[i]->fringes_bead[0]->v_0 + fov->beads_list[i]->fringes_bead[1]->v_0;
    mean_v0->xd[i] = fov->beads_list[i]->xc;
    mean_v0->yd[i] = (fov->beads_list[i]->fringes_bead[0]->v_0 - fov->beads_list[i]->fringes_bead[1]->v_0) / 2;
  }
  if (fov->n_beads > 1)
  {
    QuickSort_double(delta_v0->xd, delta_v0->yd, 0, delta_v0->nx - 1);
    QuickSort_double(mean_v0->xd, mean_v0->yd, 0, mean_v0->nx - 1);
  }
  O_p *op3 = NULL;
  op3 = create_and_attach_one_plot(*plr,1,1,0);
  set_ds_source(delta_v0, "diff phi vel");
  set_ds_source(mean_v0, "mean phi vel");
  set_plot_title(op3, "velocity balance beads job%d", count);
  set_plot_x_title(op3, "bead position (pixel)");
  set_plot_y_title(op3, "delta and mean (rd per mic)");
  set_op_filename(op3, "beads_calibration_job%d.gr", count);
  add_data_to_one_plot(op3, IS_DATA_SET, delta_v0);
  add_data_to_one_plot(op3, IS_DATA_SET, mean_v0);

  //plot raw phase versus zobj for all fringes
  d_s *tmp_phi = NULL;
  win_printf_OK("actual fo buffer size %d", fov->buffer_size);
  tmp_phi = build_adjust_data_set(tmp_phi, fov->buffer_size, fov->buffer_size);
  O_p *op5 = NULL;
  op5 = create_and_attach_one_plot(*plr,1,1,0);
  set_plot_title(op5, "unwrapped raw phases job%d", count);
  set_op_filename(op5, "unwrapped_phase_job%d.gr", count);
  for (i=0;i<fov->n_beads;i++)
  {
    for (j=0;j<fov->buffer_size;j++)
    {
      if (z==NULL) tmp_phi->xd[j]=j;
      else tmp_phi->xd[j]=z[j];
      tmp_phi->yd[j]=fov->beads_list[i]->fringes_bead[0]->corrected_phi_0_t[j];
    }
    set_ds_source(tmp_phi, "bead%d_fr0_job",i);
    add_data_to_one_plot(op5, IS_DATA_SET, duplicate_data_set(tmp_phi,NULL));
    for (j=0;j<fov->buffer_size;j++)
    {
      tmp_phi->yd[j]=fov->beads_list[i]->fringes_bead[1]->corrected_phi_0_t[j];
    }
    set_ds_source(tmp_phi, "bead%d_fr1_job",i);
    add_data_to_one_plot(op5, IS_DATA_SET, duplicate_data_set(tmp_phi,NULL));
  }
  set_plot_x_title(op5, "z obj sensor");
  set_plot_y_title(op5, "phase (rad)");

  count ++;
  return 0;
}

//substract a known vector of z positions to check the linearity of tracking during instrument calibration
int sdi_v2_fov_linearity_check(sdi_v2_fov *fov, float *z, pltreg **plr, int replace)
{
  int i, j;
  static int count=0;
  O_p *op1 = NULL; //always at least one plot..
  if (fov == NULL || z == NULL) return D_O_K;
  if (*plr == NULL) *plr = create_hidden_pltreg_with_op(&op1, 1, 1, 0, "ramp proc results");
  else
  {
    op1 = create_and_attach_one_plot(*plr,1,1,0);
    if (replace == 1) while ( (*plr)->n_op > 1) remove_data (*plr, IS_ONE_PLOT, (void*)((*plr)->o_p[0]));
  }

  ////////plots of linearity error for all the beads in different plots
  O_p *op_tmp = NULL;
  d_s *diff_error = NULL;
  d_s *known_z = NULL;
  int force_buf_size=fov->buffer_size;
  win_scanf("actual fov buffer size of %d",&force_buf_size);
  known_z = build_adjust_data_set(known_z, force_buf_size, force_buf_size);
  diff_error = build_adjust_data_set(diff_error, force_buf_size, force_buf_size);
  float mean_tmp = 0;
  for (j=0 ; j<force_buf_size ; j++)
  {
    known_z->xd[j] = j;
    if (z==NULL) known_z->yd[j] = j;
    else known_z->yd[j]=z[j];
    diff_error->xd[j] = j;
  }
  for (i=0;i<fov->n_beads ;i++)
  {
    for (j=0 ; j<force_buf_size ; j++) diff_error->yd[j] = fov->beads_list[i]->z_t[j] - z[j];
    sdi_v2_estimate_mean(diff_error->yd, diff_error->nx, &mean_tmp);
    sdi_v2_offset_then_scale(diff_error->yd, diff_error->nx, mean_tmp, 1);
    op_tmp = create_and_attach_one_plot(*plr,1,1,0);
    set_ds_source(diff_error, "bead_z(t)_error %d", i);
    set_plot_title(op_tmp, "linearity bead%d, :job%d", i, count);
    set_op_filename(op_tmp, "linearity_bead%d_job%d.gr", i, count);
    set_plot_x_title(op_tmp, "frame index");
    set_plot_y_title(op_tmp, "z(t)");
    add_data_to_one_plot(op_tmp, IS_DATA_SET, duplicate_data_set(diff_error, NULL));
    add_data_to_one_plot(op_tmp, IS_DATA_SET, duplicate_data_set(known_z, NULL));
    add_data_to_one_plot(op1, IS_DATA_SET, duplicate_data_set(diff_error, NULL));
    if (i==0) add_data_to_one_plot(op1, IS_DATA_SET, duplicate_data_set(known_z, NULL));
  }
  set_plot_title(op1, "linearity check all beads:job%d", count);
  set_op_filename(op1, "linearity_all_beads_job%d.gr", count);
  set_plot_x_title(op1, "frame_index");
  set_plot_y_title(op1, "z(t)");

  count ++;
  return 0;
}


int sdi_v2_track_y_profile_fov_movie(sdi_v2_fov *fov, O_i *oi)
{
  if (win_printf_OK("this routine track the y profile of your fov of the current movie") == WIN_CANCEL) return D_O_K;
  int i, j;
  int nf = oi->im.n_f;
  int n_2d_buf = nf;
  if (fov == NULL || oi == NULL) return 1;
  for (i=0 ; i<fov->n_y_profile ;i++)
  {
    if(fov->y_profile_list[i]->n_output_t < nf) sdi_v2_allocate_sdi_v2_y_profile_output_t_fields(fov->y_profile_list[i], nf);
    if(fov->y_profile_list[i]->n_raw_y_profile_buffer == 0) sdi_v2_allocate_sdi_v2_y_profile_2d_float_fields(fov->y_profile_list[i], sdi_g_param, 1, PTR_Y_PROFILE_RAW_Y_PROFILE);
    if(fov->y_profile_list[i]->n_y_window_buffer == 0) sdi_v2_allocate_sdi_v2_y_profile_2d_float_fields(fov->y_profile_list[i], sdi_g_param, 1, PTR_Y_PROFILE_Y_WINDOW);
    if(fov->y_profile_list[i]->n_windowed_y_profile_buffer == 0) sdi_v2_allocate_sdi_v2_y_profile_2d_float_fields(fov->y_profile_list[i], sdi_g_param, 1, PTR_Y_PROFILE_Y_PROFILE);
    if(fov->y_profile_list[i]->n_y_profile_autoconv_buffer == 0) sdi_v2_allocate_sdi_v2_y_profile_2d_float_fields(fov->y_profile_list[i], sdi_g_param, 1, PTR_Y_PROFILE_Y_AUTOCONV);
    if(fov->y_profile_list[i]->n_y_profile_autoconv_roi_buffer == 0) sdi_v2_allocate_sdi_v2_y_profile_2d_float_fields(fov->y_profile_list[i], sdi_g_param, n_2d_buf, PTR_Y_PROFILE_Y_AUTOCONV_ROI);
  }
  for (i=0 ; i<nf ; i++)
  {
    switch_frame(oi, i);
    for (j=0 ;j<fov->n_y_profile ;j++)
    {
      sdi_v2_extract_sdi_y_profile_raw_y_profile_from_image(oi, fov->y_profile_list[j], sdi_g_param);
      sdi_v2_process_y_profile_y_profile(fov->y_profile_list[j], sdi_g_param);
      sdi_v2_compute_center_by_autoconv_of_sdi_v2_y_profile(fov->y_profile_list[j], fov->sdi_param);
      sdi_v2_update_c_profile_y_profile_points(fov->y_profile_list[j], i);
    }
  }
  return 0;
}

///////////////////////////////////graphical stuff//////////////////////:
//////////////////////////////////////////////////////////////////////////////////
/*
int sdi_v2_label_fringes_of_fov(BITMAP *imb, O_i *oi, sdi_v2_fov *fov)
{
  if (fov == NULL) return D_O_K;
  if (fov->sdi_param == NULL) return D_O_K;
  if (fov->n_fringes == 0) return D_O_K;
  int i;
  for (i=0 ; i<fov->n_fringes ; i++) sdi_v2_label_one_fringes(imb, oi, fov->fringes_list[i], fov->sdi_param);
  return 0;
}*/

int sdi_v2_draw_one_fringes(BITMAP *imb, O_i *oi, sdi_v2_fringes *fringes, sdi_v2_parameter *param)
{
  int xtmp, ytmp, dxtmpf, dytmpf, color1;
  if (fringes == NULL || oi == NULL || param == NULL) return D_O_K;
  dxtmpf = param->x_roi_size_2;
  dytmpf = param->y_roi_size_2;
  color1 = sdi_colormap[3];//red :
  //fringes patterns
  xtmp = fringes->xc;
  ytmp = fringes->yc;
  sdi_v2_draw_one_fast_rect_with_user_pix(xtmp, ytmp, dxtmpf, dytmpf, color1, oi, imb);
  return 0;
}

int sdi_v2_draw_one_bead(BITMAP *imb, O_i *oi, sdi_v2_bead *bead, sdi_v2_parameter *param)
{
  int xtmp, ytmp, dxtmpf, dytmpf, dxtmpy, dytmpy, color1, color2, color3, color4;
  dxtmpf = param->x_roi_size_2;
  dytmpf = param->y_roi_size_2;
  dxtmpy = param->x_roi_size_2_Y;
  dytmpy = param->y_roi_size_2_Y;

  if (bead->bead_class & SDI_WORKING_BEAD)
  {
    //default color : good bead
    color1 = sdi_colormap[0];//first pattern violet
    color2 = sdi_colormap[1];//second one turquoise
    color3 = sdi_colormap[0];
    color4 = sdi_colormap[1];
  }
  //functions
  if (bead->bead_class & SDI_REFERENCE_BEAD) color1 = color2 = color3 = color4 = sdi_colormap[7];//bead ok not moving : ref
  if (bead->bead_class & SDI_OPENING_BEAD_1) color1 = color2 = color3 = color4 = sdi_colormap[6];//moving beads that open

  //error signal and uninitialzed beads
  //if (bead->bead_class & SDI_WEIRD_BEAD) color1 = color2 = color3 = color4 = sdi_colormap[3];//orange
  //if (bead->bead_class & SDI_BAD_BEAD) color1 = color2 = color3 = color4 = sdi_colormap[5];//red
  if (bead->bead_class & SDI_UNINITIALIZED_BEAD) color1 = color2 = color3 = color4 = sdi_colormap[2];//not calibrated or no ref points


  //fringes patterns
  xtmp = bead->fringes_bead[0]->xc;
  ytmp = bead->fringes_bead[0]->yc;
  sdi_v2_draw_one_fast_rect_with_user_pix(xtmp, ytmp, dxtmpf, dytmpf, color1, oi, imb);
  //sdi_v2_draw_one_circle_with_user_pix(xtmp, ytmp, dxtmpf, sdi_colormap[6], oi, imb);//test
  xtmp = bead->fringes_bead[1]->xc;
  ytmp = bead->fringes_bead[1]->yc;
  sdi_v2_draw_one_fast_rect_with_user_pix(xtmp, ytmp, dxtmpf, dytmpf, color2, oi, imb);

  //y profiles
  xtmp = bead->y_profile_bead[0]->xc;
  ytmp = bead->y_profile_bead[0]->yc;
  sdi_v2_draw_one_fast_rect_with_user_pix(xtmp, ytmp, dxtmpy, dytmpy, color3, oi, imb);
  xtmp = bead->y_profile_bead[1]->xc;
  ytmp = bead->y_profile_bead[1]->yc;
  sdi_v2_draw_one_fast_rect_with_user_pix(xtmp, ytmp, dxtmpy, dytmpy, color4, oi, imb);
  return 0;
}

int sdi_v2_label_one_bead(BITMAP *imb, O_i *oi, sdi_v2_bead *bead, sdi_v2_parameter *param)
{
  (void)param;
  (void)imb;
  sdi_v2_add_text_vga_screen_unit_in_oi( oi, bead->xc, bead->yc, sdi_colormap[8], "bd%d z=%1.3f", bead->index_in_fov, bead->z_t[bead->c_output_t]);
  return 0;
}

int sdi_v2_label_beads_of_fov(BITMAP *imb, O_i *oi, sdi_v2_fov *fov)
{
  if (fov == NULL) return D_O_K;
  if (fov->sdi_param == NULL) return D_O_K;
  if (fov->n_beads == 0) return D_O_K;
  int i;
  for (i=0 ; i<fov->n_beads ; i++)
  {
    sdi_v2_label_one_bead(imb, oi, fov->beads_list[i], fov->sdi_param);
    //sdi_v2_find_index_fringes_in_fov(fov, fov->beads_list[i]->fringes_bead[0], &index);
    //sdi_v2_find_index_fringes_in_fov(fov, fov->beads_list[i]->fringes_bead[1], &index2);
    //sdi_v2_add_text_vga_screen_unit_in_oi( oi, fov->beads_list[i]->xc + 20, fov->beads_list[i]->yc, sdi_colormap[8], "f%d&f%d", index, index2);
  }
  return 0;
}
///////////////////////////////////graphical stuff//////////////////////:
//////////////////////////////////////////////////////////////////////////////////
int sdi_v2_draw_beads_of_current_fov(BITMAP *imb, O_i *oi, sdi_v2_fov *fov)
{
  if (fov == NULL) return D_O_K;
  if (fov->sdi_param == NULL) return D_O_K;
  if (fov->n_beads == 0) return D_O_K;
  int i;
  for (i=0 ; i<fov->n_beads ; i++) sdi_v2_draw_one_bead(imb, oi, fov->beads_list[i], fov->sdi_param);
  return 0;
}

int sdi_v2_draw_fringes_of_current_fov(BITMAP *imb, O_i *oi, sdi_v2_fov *fov)
{
  if (fov == NULL) return D_O_K;
  if (fov->sdi_param == NULL) return D_O_K;
  if (fov->n_fringes == 0) return D_O_K;
  int i;
  for (i=0 ; i<fov->n_fringes ; i++) sdi_v2_draw_one_fringes(imb, oi, fov->fringes_list[i], fov->sdi_param);
  return 0;
}

int sdi_v2_add_one_display_to_fov(sdi_v2_fov *fov)
{
  if (fov->n_display==0)
  {
    fov->display_list=(sdi_v2_disp **) calloc(1,sizeof(sdi_v2_disp *));
    fov->display_list[0] = (sdi_v2_disp *)calloc(1, sizeof(sdi_v2_disp));
  }
  else
  {
    fov->display_list=(sdi_v2_disp **) realloc(fov->display_list, (fov->n_display + 1) * sizeof(sdi_v2_disp *));
    fov->display_list[fov->n_display] = (sdi_v2_disp *)calloc(1, sizeof(sdi_v2_disp));
  }
  fov->display_list[fov->n_display]->plr = NULL;
  fov->display_list[fov->n_display]->op = NULL;
  fov->display_list[fov->n_display]->data=NULL;
  fov->display_list[fov->n_display]->y=NULL;
  fov->display_list[fov->n_display]->x=NULL;
  fov->display_list[fov->n_display]->n_y=0;
  fov->display_list[fov->n_display]->c_y=0;
  fov->display_list[fov->n_display]->n_x=0;
  fov->display_list[fov->n_display]->constant_x=true;
  fov->n_display+=1;
  return 0;
}

int sdi_v2_create_y_profile_associated_with_fringes(sdi_v2_fov *fov)
{
  int i;
  for(i=0; i < fov->n_fringes ;i++) sdi_v2_add_one_sdi_y_profile_to_fov_by_position(fov, fov->fringes_list[i]->xc, fov->fringes_list[i]->yc);
  sdi_v2_update_all_sdi_y_profile_parameters(fov, fov->sdi_param);
  return 0;
}



int sdi_set_common_calibration_factors(sdi_v2_fov *fov, int verbose, bool from_cfg)
{
  int i;
  static float v_00 = 3.52;
  static float v_01 = -3.52;
  static float v_10 = 3.;
  static float v_11 = -3.;
  static float nu00 = 0.225;
  static float nu01 = 0.225;
  static float v_x0 = 5.775;
  static float v_x1 = 5.775;
  if (from_cfg==true)
  {
    v_00 = get_config_float("SDI", "z_phase_velocity", 1.);
    v_x0 = get_config_float("SDI", "x_phase_velocity", 1.);
    v_10 = get_config_float("SDI", "group_velocity", 1.);
    nu00 = get_config_float("SDI", "frequency", 1.);
    v_01= - v_00;
    v_11 = - v_10;
    nu01 = nu00;
    v_x1 = v_x0;
  }
  if (verbose != 0)
  {
    if (win_scanf("Calibration factors of fringes\n"
    "top fringes : z phase velocity=%3f (rad.mic-1), z group velocity v10=%3f (pix.mic-1) frequency=%3f \n"
    "bottom fringes : z phase velocity=%3f (rad.mic-1), z group velocity v10=%3f (pix.mic-1) frequency=%3f \n"
    "phase velocity along x %3f %3f (in pix)", &v_00, &v_10, &nu00, &v_01, &v_11, &nu01, &v_x0, &v_x1) == WIN_CANCEL) return D_O_K;
  }
  for (i=0 ;i<fov->n_beads ;i++) sdi_v2_set_calibration_of_bead(fov->beads_list[i], v_00, v_10, nu00, v_01, v_11, nu01, v_x0, v_x1);
  if (from_cfg==true)
  {
    set_config_float("SDI", "z_phase_velocity", v_00);
    set_config_float("SDI", "x_phase_velocity", v_x0);
    set_config_float("SDI", "group_velocity", v_10);
    set_config_float("SDI", "frequency", nu00);
  }
  return 0;
}

int sdi_v2_add_current_point_as_ref_points_fov(sdi_v2_fov *fov)
{
  if (fov == NULL) return D_O_K;
  int i;
  for (i=0 ;i<fov->n_fringes ;i++) fov->fringes_list[i]->need_reference_point = true;
  return 0;
}

//ancient version : user gives the value he read on debug signals
//new one : bgd is computed by smallest value of histo of raw profile
int sdi_v2_take_backgrounds(sdi_v2_fov *fov)
{
  if (updating_menu_state !=0)  return D_O_K;
  if (fov==NULL) return D_O_K;
  int i;
  //static float bgdf = 0;
  //static float bgdy = 0;
  //if (win_scanf("this sets common background for %3f fringes and %3f y obj", &bgdf, &bgdy)==WIN_CANCEL) return D_O_K;
  //for(i=0; i<fov->n_fringes ;i++) fov->fringes_list[i]->background = bgdf;
  //for(i=0; i<fov->n_y_profile ;i++) fov->y_profile_list[i]->background = bgdy;
  for(i=0; i<fov->n_fringes ;i++) fov->fringes_list[i]->need_background = true;
  for(i=0; i<fov->n_y_profile ;i++) fov->y_profile_list[i]->need_background = true;
  return 0;
}

int sdi_v2_compute_sdi_y_profile_correlation_of_fluctuation_ds1out(sdi_v2_y_profile *profile1, sdi_v2_y_profile *profile2, float *corr_product)
{
  sdi_v2_correlation_product(profile1->ds_out1->yd, profile2->ds_out1->yd, 0, profile1->ds_out1->nx, corr_product);
  return 0;
}

//the following 2 functions are used in detection phase and directly process images
int sdi_v2_center_roi_y_fringes(O_i *oi, sdi_v2_parameter *sdi_param, sdi_v2_fringes *fringes)
{
  static float *tmp=NULL;
  static int ntmp = 0;
  int x_i_max;
  float y_max;

  if (oi == NULL || sdi_param == NULL || fringes == NULL) return 1;
  if (tmp == NULL || ntmp < sdi_param->y_roi_size)
    {
      tmp = (float *)realloc(tmp,sdi_param->y_roi_size*sizeof(float));
      if (tmp == NULL) return 1;
      ntmp = sdi_param->y_roi_size;
    }
  sdi_v2_update_c_profile_fringes_points(fringes, 0);
  sdi_v2_extract_y_profile_array_from_image(oi, tmp, fringes->xc - sdi_param->x_roi_size_2_Y, fringes->xc + sdi_param->x_roi_size_2_Y,
                                            fringes->yc - sdi_param->y_roi_size_2, fringes->yc + sdi_param->y_roi_size_2);
  sdi_v2_array_max( tmp, 0, sdi_param->y_roi_size, &y_max, &x_i_max);
  fringes->yc = fringes->yc + x_i_max - sdi_param->y_roi_size_2 + 1;
  fringes->y_roi_min = fringes->yc - sdi_param->y_roi_size_2;
  fringes->y_roi_max = fringes->yc + sdi_param->y_roi_size_2;
  //free(tmp);
  //tmp = NULL;
  return 0;
}
int sdi_v2_center_roi_x_fringes(O_i *oi, sdi_v2_parameter *sdi_param, sdi_v2_fringes *fringes)
{
  //static int debug = 0;
  sdi_v2_update_c_profile_fringes_points(fringes, 0);
  sdi_v2_extract_sdi_fringes_raw_x_profile_from_image(oi, fringes, sdi_param);
  sdi_v2_process_sdi_fringes_x_profile(fringes, sdi_param);
  sdi_v2_compute_phase_coeff_of_sdi_fringes(fringes, sdi_param);
  fringes->xc = fringes->xc-(int)(round(fringes->raw_phi_1_t[fringes->c_phi_t]));
  fringes->x_roi_min = fringes->xc - sdi_param->x_roi_size_2;
  fringes->x_roi_max = fringes->xc + sdi_param->x_roi_size_2;
  return 0;
}

int sdi_v2_clean_fov_from_bad_beads(sdi_v2_fov *fov)
{
  int i = 0;
  while( i<fov->n_beads )
  {
    if ( (fov->beads_list[i]->bead_class && SDI_WORKING_BEAD)
      && ((fov->beads_list[i]->bead_class && SDI_OPENING_BEAD_1)  || (fov->beads_list[i]->bead_class && SDI_REFERENCE_BEAD)) ) i++;
    else sdi_v2_remove_one_sdi_bead_from_fov(fov, i);
  }
  return 0;
}

//this erases all fringes that are not in beads
int sdi_v2_clean_fov_from_fringes_not_in_beads(sdi_v2_fov *fov)
{
  if (fov == NULL) return D_O_K;
  int i, j;//franchement je purrais faire plus joli mais bon
  i = 0;
  bool in_bead;
  while( i< fov->n_fringes )
  {
    in_bead = false;
    for (j=0 ; j<fov->n_beads ;j++)
    {
      if (fov->beads_list[j]->fringes_bead[0] == fov->fringes_list[i])
      {
        in_bead = true;
      }
      if (fov->beads_list[j]->fringes_bead[1] == fov->fringes_list[i]) in_bead = true;
    }
    if (in_bead == false)
    {
      sdi_v2_remove_one_sdi_fringes_from_fov(fov, i);
      i--;
    }
    i++;
  }
  return 0;
}

//this erases all y profiles that are not in beads
int sdi_v2_clean_fov_from_y_profile_not_in_beads(sdi_v2_fov *fov)
{
  if (fov == NULL) return D_O_K;
  int i, j;//franchement je purrais faire plus joli mais bon
  i = 0;
  bool in_bead;
  while( i< fov->n_y_profile )
  {
    in_bead = false;
    for (j=0 ; j<fov->n_beads ;j++)
    {
      if (fov->beads_list[j]->y_profile_bead[0] == fov->y_profile_list[i]) in_bead = true;
      if (fov->beads_list[j]->y_profile_bead[1] == fov->y_profile_list[i]) in_bead = true;
    }
    if (in_bead == false)
    {
      sdi_v2_remove_one_sdi_y_profile_from_fov(fov, i);
      i--;
    }
    i++;
  }
  return 0;
}

//from raw phase measurements, computes the correlation of noise between pair of fringes
//and creates the bead
int sdi_v2_gather_y_profiles_into_bead_with_noise_correlation(sdi_v2_fov *fov, sdi_v2_y_b_d *sdi_y_b_d, pltreg **plr)
{
  int i, j , k;
  int dist_x, dist_y;
  int found, founded, count;
  static int count_found = 0;
  float corr_product=0;
  count = 0;
  sdi_v2_y_profile *y_tmp;
  j=0;

  O_p *op1 = NULL;
  O_p *op2 = NULL;
  O_p *op3 = NULL;
  if(sdi_y_b_d->verbose == 1)
  {
    if ( *(plr) == NULL) *(plr) = create_hidden_pltreg_with_op(&op1, 1, 1, 0, "bead detection");
    else
    {
      op1 = create_and_attach_one_plot(*plr,1,1,0);
      if (sdi_y_b_d->replace == 1) while ( (*plr)->n_op > 1) remove_data (*plr, IS_ONE_PLOT, (void*)((*plr)->o_p[0]));
    }
    set_plot_title(op1, "y(t) of moving y profiles.exp%d", count_found);
    set_plot_filename(op1, "y(t) of moving y profiles %d", count_found);
    op2 = create_and_attach_one_plot(*plr,1,1,0);
    set_plot_title(op2, "y(t) of fixed y profiles.exp%d", count_found);
    set_plot_filename(op2, "y(t) of moving y profiles.exp%d", count_found);
    op3 = create_and_attach_one_plot(*plr,1,1,0);
    set_plot_title(op3, "correlation stat.exp%d", count_found);
    set_plot_filename(op3, "correlation stat.exp%d", count_found);
  }

  float offset = 0;
  float std_y_i = 0, std_y_j = 0;
  i=0;

  while( count< (int)(fov->n_y_profile / 2) && i < fov->n_y_profile )
  {
    sdi_v2_estimate_std(fov->y_profile_list[i]->ds_out1->yd, fov->y_profile_list[i]->ds_out1->nx, &std_y_i);
    found = 0;
    founded = 0;
    j = i+1;
    //because it is the best way to find moving beads, we start by searching beads using brownian motion correlation of y profiles
    //we will miss the reference beads in this first step
    //in the neighboorhood we find y profiles with good correlation of brownian motion
    while (j < fov->n_y_profile && found ==0)
    {
      dist_y = fabs(fov->y_profile_list[i]->yc-fov->y_profile_list[j]->yc);
      dist_x = fabs(fov->y_profile_list[i]->xc-fov->y_profile_list[j]->xc);
      if( dist_y<sdi_y_b_d->dist_y_max && dist_x<sdi_y_b_d->dist_x_max && dist_y>sdi_y_b_d->dist_y_min)
      {
        sdi_v2_compute_sdi_y_profile_correlation_of_fluctuation_ds1out(fov->y_profile_list[i], fov->y_profile_list[j], &corr_product);
        if (corr_product > sdi_y_b_d->correlation_min)
        {
          found = 1;
          for (k=0 ; k < fov->n_beads ; k++) //check that this one has not been found previously
          {
            y_tmp = fov->beads_list[k]->y_profile_bead[0];
            if ( fov->y_profile_list[i] == y_tmp || fov->y_profile_list[j] == y_tmp) founded = 1;
            y_tmp = fov->beads_list[k]->y_profile_bead[1];
            if ( fov->y_profile_list[i] == y_tmp || fov->y_profile_list[j] == y_tmp) founded = 1;
          }
          if (founded ==0)
          {
            sdi_v2_add_one_bead_to_fov( fov, fov->fringes_list[i], fov->fringes_list[j], fov->y_profile_list[i], fov->y_profile_list[j]);
            count++;
            if(sdi_y_b_d->verbose == 1)
            {
              sdi_v2_offset_then_scale(fov->y_profile_list[i]->ds_out1->yd, fov->y_profile_list[i]->ds_out1->nx, -offset, 1);
              sdi_v2_offset_then_scale(fov->y_profile_list[j]->ds_out1->yd, fov->y_profile_list[j]->ds_out1->nx, -offset, 1);
              set_ds_source(fov->y_profile_list[i]->ds_out1, "y.y(t)%d.exp%d", i, count_found);
              set_ds_source(fov->y_profile_list[j]->ds_out1, "y.y(t)%d.exp%d", j, count_found);
              add_data_to_one_plot(op1, IS_DATA_SET, duplicate_data_set(fov->y_profile_list[i]->ds_out1, NULL));
              add_data_to_one_plot(op1, IS_DATA_SET, duplicate_data_set(fov->y_profile_list[j]->ds_out1, NULL));
              offset += 5; //offset for plot
            }
          }
        }
      }
      j++;
    }

    //second step : if one y profile was not a half of a moving bead, then it can be a part of a ref (immobile) bead
    //thus : if std is very small and he has a neighboor with also a very small std, do a ref bead
    //i assume that I do not have a too big density  of ref bead (meaning not 2 more close than the dist max)
    if (found == 0 && std_y_i < 10)
    {
      j = i + 1;
      while (j < fov->n_y_profile && found ==0)
      {
        dist_y = fabs(fov->y_profile_list[i]->yc-fov->y_profile_list[j]->yc);
        dist_x = fabs(fov->y_profile_list[i]->xc-fov->y_profile_list[j]->xc);
        if( dist_y<sdi_y_b_d->dist_y_max && dist_x<sdi_y_b_d->dist_x_max && dist_y>sdi_y_b_d->dist_y_min)
        {
          sdi_v2_estimate_std(fov->y_profile_list[j]->ds_out1->yd, fov->y_profile_list[j]->ds_out1->nx, &std_y_j);
          if (std_y_j <  10)
          {
            found = 1;
            for (k=0 ; k < fov->n_beads ; k++) //check that this one has not been found previously
            {
              y_tmp = fov->beads_list[k]->y_profile_bead[0];
              if ( fov->y_profile_list[i] == y_tmp || fov->y_profile_list[j] == y_tmp) founded = 1;
              y_tmp = fov->beads_list[k]->y_profile_bead[1];
              if ( fov->y_profile_list[i] == y_tmp || fov->y_profile_list[j] == y_tmp) founded = 1;
            }
            if (founded ==0)
            {
              sdi_v2_add_one_bead_to_fov( fov, fov->fringes_list[i], fov->fringes_list[j], fov->y_profile_list[i], fov->y_profile_list[j]);
              count++;
              if(sdi_y_b_d->verbose == 1)
              {
                set_ds_source(fov->y_profile_list[i]->ds_out1, "y.y(t)%d.exp%d", i, count_found);
                set_ds_source(fov->y_profile_list[j]->ds_out1, "y.y(t)%d.exp%d", j, count_found);
                add_data_to_one_plot(op2, IS_DATA_SET, duplicate_data_set(fov->y_profile_list[i]->ds_out1, NULL));
                add_data_to_one_plot(op2, IS_DATA_SET, duplicate_data_set(fov->y_profile_list[j]->ds_out1, NULL));
              }
            }
          }
        }
        j++;
      }
    }
    i++;
  }
  //erase buffers
  for (i=0;i<fov->n_y_profile;i++)
  {
    free_data_set(fov->y_profile_list[i]->ds_out1);
    fov->y_profile_list[i]->ds_out1 = NULL;
  }
  count_found ++;
  return 0;
}

//not used
/*int sdi_v2_gather_fringes_into_bead_with_calibration_factors(sdi_v2_fov *fov)
{
  if(fov->n_fringes<2) return D_O_K;
  if(fov->sdi_param==NULL)return D_O_K;
  int found_couple=0;
  int i_fringes=0;
  int tol_dy=4, tol_dx=80;
  int dist_x, dist_y;
  int j,k, found;

  while( found_couple< (int)(fov->n_fringes / 2) )
  {
    found = 0;
    if(fov->fringes_list[i_fringes]->v_0 > 0)
    {
      k=0;
      while (k<fov->n_fringes && found==0 )
      {
        j = -tol_dy;//mais LOL quoi, la boucle sur j ne sert a RIEN
        while ( j<=tol_dy && found==0 )
        {
          dist_y = fabs(fov->fringes_list[k]->yc-fov->fringes_list[i_fringes]->yc);
          dist_x = fabs(fov->fringes_list[k]->xc-fov->fringes_list[i_fringes]->xc);
          if( dist_y<(fov->sdi_param->dist_half_beads+tol_dy) && dist_x<tol_dx && fov->fringes_list[k]->v_0 < 0)
          {
            sdi_v2_add_one_bead_to_fov(fov, fov->fringes_list[i_fringes] , fov->fringes_list[k], fov->y_profile_list[i_fringes], fov->y_profile_list[k]);
            found = 1;
          }
          j++;
        }
        k++;
      }

    }
    i_fringes++;
    if (found==1) found_couple++;
  }
  win_printf_OK("combien %d", found_couple);
  return 0;
}*/


////////////////allocation of sdi objects as new element of fov
//////////////////////////////////////////////////////////////////////////////////
int sdi_v2_add_one_bead_to_fov_with_coordinates(sdi_v2_fov *fov, int xc, int yc)
{
  //creation of a bead with fringes & y_profiles
  sdi_v2_add_one_sdi_fringes_to_fov_by_position( fov, xc, yc - (int) (( 1 + fov->sdi_param->dist_half_beads ) /2) );
  sdi_v2_fringes_parameter_update(fov->fringes_list[fov->n_fringes - 1], fov->sdi_param);
  sdi_v2_add_one_sdi_fringes_to_fov_by_position( fov, xc, yc + (int) (fov->sdi_param->dist_half_beads / 2) );
  sdi_v2_fringes_parameter_update(fov->fringes_list[fov->n_fringes - 1], fov->sdi_param);

  sdi_v2_add_one_sdi_y_profile_to_fov_by_position(fov, xc, yc - (int) (( 1 + fov->sdi_param->dist_half_beads ) /2) );
  sdi_v2_y_profile_update_parameter(fov->y_profile_list[fov->n_y_profile - 1], fov->sdi_param);
  sdi_v2_add_one_sdi_y_profile_to_fov_by_position(fov, xc, yc + (int) (fov->sdi_param->dist_half_beads / 2) );
  sdi_v2_y_profile_update_parameter(fov->y_profile_list[fov->n_y_profile - 1], fov->sdi_param);

  sdi_v2_add_one_bead_to_fov(fov, fov->fringes_list[fov->n_fringes - 2], fov->fringes_list[fov->n_fringes - 1],
                      fov->y_profile_list[fov->n_y_profile - 2], fov->y_profile_list[fov->n_y_profile - 1]);

  return 0;
}

int sdi_v2_add_one_bead_to_fov_with_mouse(sdi_v2_fov *fov)
{
  imreg *imr = NULL;
  if (ac_grep(cur_ac_reg,"%im", &imr) != 1)	return D_O_K;
  int x_m, y_m, pos_m;
  //takes the current new mouse position
  pos_m = mouse_pos;
  x_m = pos_m >> 16;
  y_m = pos_m & 0x0000ffff;
  x_m = x_imr_2_imdata(imr, x_m);
  y_m = y_imr_2_imdata(imr, y_m);

  //creation of a bead with fringes & y_profiles
  sdi_v2_add_one_sdi_fringes_to_fov_by_position( fov, x_m, y_m - (int) (( 1 + fov->sdi_param->dist_half_beads ) /2) );
  sdi_v2_fringes_parameter_update(fov->fringes_list[fov->n_fringes - 1], fov->sdi_param);
  sdi_v2_add_one_sdi_fringes_to_fov_by_position( fov, x_m, y_m + (int) (fov->sdi_param->dist_half_beads / 2) );
  sdi_v2_fringes_parameter_update(fov->fringes_list[fov->n_fringes - 1], fov->sdi_param);

  sdi_v2_add_one_sdi_y_profile_to_fov_by_position(fov, x_m, y_m - (int) (( 1 + fov->sdi_param->dist_half_beads ) /2) );
  sdi_v2_y_profile_update_parameter(fov->y_profile_list[fov->n_y_profile - 1], fov->sdi_param);
  sdi_v2_add_one_sdi_y_profile_to_fov_by_position(fov, x_m, y_m + (int) (fov->sdi_param->dist_half_beads / 2) );
  sdi_v2_y_profile_update_parameter(fov->y_profile_list[fov->n_y_profile - 1], fov->sdi_param);

  sdi_v2_add_one_bead_to_fov(fov, fov->fringes_list[fov->n_fringes - 2], fov->fringes_list[fov->n_fringes - 1],
                      fov->y_profile_list[fov->n_y_profile - 2], fov->y_profile_list[fov->n_y_profile - 1]);

  return 0;
}

int sdi_v2_add_one_bead_to_fov(sdi_v2_fov *fov, sdi_v2_fringes *fringes1, sdi_v2_fringes *fringes2,
                                                          sdi_v2_y_profile *y_profile1, sdi_v2_y_profile *y_profile2)
{
  int n;
  if (fov==NULL) return D_O_K;
  if (fov->n_beads==0)
  {
    fov->beads_list=(sdi_v2_bead **)calloc(1,sizeof(sdi_v2_bead *));
    n=1;
  }
  else
  {
    n=fov->n_beads;
    n+=1;
    fov->beads_list=realloc(fov->beads_list, n*sizeof(sdi_v2_bead *));
  }
  fov->beads_list[n-1] = (sdi_v2_bead *)calloc(1, sizeof(sdi_v2_bead));
  sdi_v2_init_bead_by_assembly( fov->beads_list[n-1], fov->sdi_param, fringes1, fringes2, y_profile1, y_profile2);
  fov->beads_list[n-1]->index_in_fov = n-1;
  fov->n_beads=n;
  return 0;
}
int sdi_v2_update_all_sdi_fringes_parameters(sdi_v2_fov *fov, sdi_v2_parameter *param)
{
  if (fov==NULL) return D_O_K;
  int i;
  for (i=0; i<fov->n_fringes ;i++)  sdi_v2_fringes_parameter_update(fov->fringes_list[i], param);
  return 0;
}

int sdi_v2_update_all_sdi_y_profile_parameters(sdi_v2_fov *fov, sdi_v2_parameter *param)
{
  if (fov==NULL) return D_O_K;
  int i;
  for (i=0; i<fov->n_y_profile ;i++) sdi_v2_y_profile_update_parameter(fov->y_profile_list[i], param);
  return 0;
}
int sdi_v2_add_one_sdi_y_profile_to_fov_by_position(sdi_v2_fov *fov, int xc, int yc)
{
  int n;
  if (fov==NULL) return D_O_K;
  if (fov->n_y_profile == 0)
  {
    fov->y_profile_list=(sdi_v2_y_profile **)calloc(1,sizeof(sdi_v2_y_profile *));
    n=1;
  }
  else
  {
    n=fov->n_y_profile;
    n+=1;
    fov->y_profile_list = realloc(fov->y_profile_list, n * sizeof(sdi_v2_y_profile *));
  }
  fov->y_profile_list[n-1] = (sdi_v2_y_profile *) calloc (1, sizeof(sdi_v2_y_profile));
  sdi_v2_init_one_sdi_y_profile_by_position( fov->y_profile_list[n-1], xc, yc);
  fov->n_y_profile = n;
  return 0;
}
int sdi_v2_add_one_sdi_fringes_to_fov_by_position(sdi_v2_fov *fov, int xc, int yc)
{
  int n;
  if (fov==NULL) return D_O_K;
  if (fov->n_fringes == 0)
  {
    fov->fringes_list=(sdi_v2_fringes **)calloc(1,sizeof(sdi_v2_fringes *));
    n=1;
  }
  else
  {
    n=fov->n_fringes;
    n+=1;
    fov->fringes_list = realloc(fov->fringes_list, n * sizeof(sdi_v2_fringes *));
  }
  fov->fringes_list[n-1] = (sdi_v2_fringes *) calloc (1, sizeof(sdi_v2_fringes));
  sdi_v2_init_one_sdi_fringes_by_position( fov->fringes_list[n-1], xc, yc);
  fov->n_fringes=n;
  return 0;
}
//allocated outside
int sdi_v2_init_one_fov(sdi_v2_fov *fov)
{
  fov->buffer_size = 0;
  fov->sdi_param = NULL;
  fov->n_fringes=0;
  fov->fringes_list=NULL;
  fov->n_display=0;
  fov->display_list=NULL;
  fov->n_y_profile=0;
  fov->y_profile_list=NULL;
  fov->n_beads=0;
  fov->beads_list=NULL;
  fov->diagnose=NULL;
  fov->n_opening_hairpins = 0;
  return 0;
}


//all intermediate steps for a fringes treatment
int sdi_v2_set_y_profile_fourier_algo_display2(sdi_v2_fov *fov, sdi_v2_y_profile *profile, pltreg *plr, O_p *op, int size)
{
  int display0;
  display0 = fov->n_display ;
  char name[32];
  snprintf(name, 32, "y profile %d", 0);

  sdi_v2_add_one_display_to_fov(fov);
  if (plr != NULL) fov->display_list[display0]->plr = plr;
  if (op != NULL) fov->display_list[display0]->op = op;
  if (profile->n_output_t == 0) sdi_v2_allocate_sdi_v2_y_profile_output_t_fields(profile, size);
  sdi_v2_config_y_profile_1d_display(profile, PTR_Y_PROFILE_Y_T, fov->display_list[display0], profile->n_output_t);
  sdi_v2_launch_display(fov->display_list[display0]->data, &fov->display_list[display0]->plr, &fov->display_list[display0]->op, "sdi display");
  set_plot_x_title(fov->display_list[display0]->op, "Time");
  set_plot_y_title(fov->display_list[display0]->op, "{\\color{lightgreen} y");
  set_ds_line_color(fov->display_list[display0]->data, Lightgreen); //Lightgreen, Lightred orange
  fov->display_list[display0]->op->y_lo = -4;
  fov->display_list[display0]->op->y_hi = 4;
  set_plot_y_fixed_range(fov->display_list[display0]->op);
  return 0;
}

//all intermediate steps for a fringes treatment
int sdi_v2_set_y_profile_fourier_algo_display1(sdi_v2_fov *fov, sdi_v2_y_profile *profile, pltreg *plr, O_p *op)
{
  int i_display, display0, n_display;
  i_display=0;
  n_display = 4;
  display0 = fov->n_display ;
  char name[32];
  snprintf(name, 32, "y profile %d", 0);

  for (i_display=0 ; i_display<n_display ; i_display++)   sdi_v2_add_one_display_to_fov(fov);
  if (plr != NULL) fov->display_list[display0]->plr = plr;
  if (op != NULL) fov->display_list[display0]->op = op;

  if (profile->n_raw_y_profile_buffer == 0) sdi_v2_allocate_sdi_v2_y_profile_2d_float_fields(profile, fov->sdi_param,1,PTR_Y_PROFILE_RAW_Y_PROFILE);
  sdi_v2_config_y_profile_2d_display(profile, PTR_Y_PROFILE_RAW_Y_PROFILE, fov->display_list[display0], fov->sdi_param);
  sdi_v2_launch_display(fov->display_list[display0]->data, &fov->display_list[display0]->plr, &fov->display_list[display0]->op, name);
  set_plot_x_title(fov->display_list[display0]->op, "Pixels");
  set_plot_y_title(fov->display_list[display0]->op, "{\\color{lightgreen} Averaged CCD pixels} {\\color{lightred} window} (ms)");
  set_ds_line_color(fov->display_list[display0]->data, Lightgreen); //Lightgreen, Lightred orange
  fov->display_list[display0]->op->x_lo = -1;
  fov->display_list[display0]->op->y_hi = 256;
  set_plot_y_fixed_range(fov->display_list[display0]->op);

  fov->display_list[display0+1]->plr = fov->display_list[display0]->plr;
  fov->display_list[display0+1]->op=fov->display_list[display0]->op;
  if (profile->n_y_window_buffer == 0) sdi_v2_allocate_sdi_v2_y_profile_2d_float_fields(profile, fov->sdi_param,1,PTR_Y_PROFILE_Y_WINDOW);
  sdi_v2_config_y_profile_2d_display(profile, PTR_Y_PROFILE_Y_WINDOW, fov->display_list[display0+1], fov->sdi_param);
  sdi_v2_launch_display(fov->display_list[display0+1]->data, &fov->display_list[display0+1]->plr, &fov->display_list[display0+1]->op, name);
  set_ds_line_color(fov->display_list[display0+1]->data, Lightred); //Lightgreen, Lightred orange

  fov->display_list[display0+2]->plr = fov->display_list[display0]->plr;
  fov->display_list[display0+2]->op=fov->display_list[display0]->op;
  if (profile->n_windowed_y_profile_buffer == 0) sdi_v2_allocate_sdi_v2_y_profile_2d_float_fields(profile, fov->sdi_param,1,PTR_Y_PROFILE_Y_PROFILE);
  sdi_v2_config_y_profile_2d_display(profile, PTR_Y_PROFILE_Y_PROFILE, fov->display_list[display0+2], fov->sdi_param);
  sdi_v2_launch_display(fov->display_list[display0+2]->data, &fov->display_list[display0+2]->plr, &fov->display_list[display0+2]->op, name);
  set_ds_line_color(fov->display_list[display0+2]->data, yellow); //Lightgreen, Lightred orange

  fov->display_list[display0+3]->plr = fov->display_list[display0]->plr;
  fov->display_list[display0+3]->op=fov->display_list[display0]->op;
  if (profile->n_y_profile_autoconv_buffer == 0) sdi_v2_allocate_sdi_v2_y_profile_2d_float_fields(profile, fov->sdi_param, 1,PTR_Y_PROFILE_Y_AUTOCONV);
  sdi_v2_config_y_profile_2d_display(profile, PTR_Y_PROFILE_Y_AUTOCONV, fov->display_list[display0+3], fov->sdi_param);
  sdi_v2_launch_display(fov->display_list[display0+3]->data, &fov->display_list[display0+3]->plr,&fov->display_list[display0+3]->op, NULL);

  return 0;
}


//removes all the display associated to this fringes then the fringes itself
int sdi_v2_remove_one_sdi_bead_from_fov(sdi_v2_fov *fov, int number)
{
  int i;
  if (fov == NULL) return D_O_K;
  if (fov->n_beads<number) return D_O_K;
  sdi_v2_disp *d_tmp = NULL;
  int index;

  //remove the bead signal display
  float *field;
  for (i=0 ;i<4;i++) //list of define of beads
  {
    sdi_v2_switch_ptr_to_sdi_bead_1d_float_field(fov->beads_list[number], i, &field, NULL, 0);
    sdi_v2_find_disp_linked_to_float( fov, field, &d_tmp, &index);//looking for only one of the buffer is sufficient
    if (index !=9999999 ) sdi_v2_remove_one_sdi_display_from_fov( fov, index);
  }
  //remove the fringes of the bead
  sdi_v2_find_index_fringes_in_fov(fov, fov->beads_list[number]->fringes_bead[0], &index);
  if(index!=888888) sdi_v2_remove_one_sdi_fringes_from_fov(fov, index);
  sdi_v2_find_index_fringes_in_fov(fov, fov->beads_list[number]->fringes_bead[1], &index);
  if(index!=888888) sdi_v2_remove_one_sdi_fringes_from_fov(fov, index);

  //remove the y profiles of the bead
  sdi_v2_find_index_y_profile_in_fov(fov, fov->beads_list[number]->y_profile_bead[0], &index);
  sdi_v2_remove_one_sdi_y_profile_from_fov(fov, index);
  sdi_v2_find_index_y_profile_in_fov(fov, fov->beads_list[number]->y_profile_bead[1], &index);
  sdi_v2_remove_one_sdi_y_profile_from_fov(fov, index);
  //then remove
  sdi_v2_remove_one_sdi_bead(fov->beads_list[number]);
  fov->n_beads--;
  for (i=number ; i<fov->n_beads ;i++) fov->beads_list[i] = fov->beads_list[i+1];
  return 0;
}

int sdi_v2_remove_all_data_of_beads(sdi_v2_fov *fov)
{
  int i;
  if (fov->n_beads == 0) return D_O_K;
  for (i=0;i<fov->n_beads;i++)
  {
    sdi_v2_remove_all_display_of_one_sdi_bead(fov, i);
    sdi_v2_reset_data_of_bead(fov->beads_list[i]);
  }
  return 0;
}

int sdi_v2_remove_all_display_of_one_sdi_bead(sdi_v2_fov *fov, int number)
{
  float *field;
  int i;
  sdi_v2_disp *d_tmp = NULL;
  int index;
  for (i=0 ;i<3;i++) //list of define
  {
    sdi_v2_switch_ptr_to_sdi_bead_1d_float_field(fov->beads_list[number], i, &field, NULL, 0);
    sdi_v2_find_disp_linked_to_float( fov, field, &d_tmp, &index);//looking for only one of the buffer is sufficient
    if (index !=9999999 ) sdi_v2_remove_one_sdi_display_from_fov( fov, index);
  }
  return 0;
}

//removes all the display associated to this fringes then the fringes itself
int sdi_v2_remove_all_display_of_one_sdi_y_profile_from_fov(sdi_v2_fov *fov, int number)
{
  int i;
  if (fov == NULL) return D_O_K;
  if (fov->n_y_profile<number) return D_O_K;
  sdi_v2_disp *d_tmp = NULL;
  int index;

  //check whether the fields are displayed somewhere and remove the associated display object if necessary
  int n_field, c_field;
  float **field_2d;
  field_2d = (float **)calloc(8096, sizeof(float *));
  for (i=10 ;i<14 ;i++) //list of define
  {
    sdi_v2_switch_ptr_to_sdi_y_profile_2d_float_field(fov->y_profile_list[number], i, field_2d, &n_field, &c_field, NULL, 0);
    sdi_v2_find_disp_linked_to_float( fov, field_2d[0], &d_tmp, &index);//looking for only one of the buffer is sufficient
    if (index !=9999999 ) sdi_v2_remove_one_sdi_display_from_fov( fov, index);
  }
  float *field;
  for (i=0 ;i<1 ;i++) //list of define
  {
    sdi_v2_switch_ptr_to_sdi_y_profile_1d_float_field(fov->y_profile_list[number], i, &field, NULL);
    sdi_v2_find_disp_linked_to_float( fov, field, &d_tmp, &index);//looking for only one of the buffer is sufficient
    if (index !=9999999 ) sdi_v2_remove_one_sdi_display_from_fov( fov, index);
  }
  free(field_2d);
  field_2d = NULL;
  return 0;
}

//removes all the display associated to this fringes then the fringes itself
int sdi_v2_remove_one_sdi_y_profile_from_fov(sdi_v2_fov *fov, int number)
{
  int i;
  //removes displays if exist
  sdi_v2_remove_all_display_of_one_sdi_y_profile_from_fov(fov, number);
  //then remove
  sdi_v2_remove_one_sdi_y_profile( fov->y_profile_list[number]);
  for (i=number ; i<fov->n_y_profile ; i++) fov->y_profile_list[i]=fov->y_profile_list[i+1];
  fov->n_y_profile--;
  return 0;
}

//removes all the display associated to this fringes
int sdi_v2_remove_all_display_of_one_sdi_fringes_from_fov(sdi_v2_fov *fov, int number)
{
  int i;
  if (fov == NULL) return D_O_K;
  if (fov->n_fringes<number) return D_O_K;
  sdi_v2_disp *d_tmp = NULL;
  int index;
  //check whether the fields are displayed somewhere and remove the associated display object if necessary
  int n_field, c_field;
  float **field_2d;
  field_2d = (float **)calloc(8096, sizeof(float *));
  for (i=20 ;i<29 ;i++) //list of define
  {
    sdi_v2_switch_ptr_to_sdi_fringes_2d_float_field(fov->fringes_list[number], i, field_2d, &n_field, &c_field, NULL, 0);
    sdi_v2_find_disp_linked_to_float( fov, field_2d[0], &d_tmp, &index);//looking for only one of the buffer is sufficient
    if (index !=9999999 ) sdi_v2_remove_one_sdi_display_from_fov( fov, index);
  }
  float *field;
  for (i=0 ;i<10 ;i++) //list of define
  {
    sdi_v2_switch_ptr_to_sdi_fringes_1d_float_field(fov->fringes_list[number], i, &field, NULL);
    sdi_v2_find_disp_linked_to_float( fov, field, &d_tmp, &index);//looking for only one of the buffer is sufficient
    if (index !=9999999 ) sdi_v2_remove_one_sdi_display_from_fov( fov, index);
  }
  free(field_2d);
  field_2d = NULL;
  return 0;
}

//removes all the display associated to this fringes then the fringes itself
int sdi_v2_remove_one_sdi_fringes_from_fov(sdi_v2_fov *fov, int number)
{
  int i;
  //removes the display associated to this one
  sdi_v2_remove_all_display_of_one_sdi_fringes_from_fov(fov, number);
  //then remove
  sdi_v2_remove_one_sdi_fringes( fov->fringes_list[number]);
  for (i=number ; i<fov->n_fringes ; i++) fov->fringes_list[i]=fov->fringes_list[i+1];
  fov->n_fringes--;
  return 0;
}

//remove all the beads of fov (when no linked to a track bead)
int sdi_v2_empty_fov(sdi_v2_fov *fov)
{
  if (fov == NULL) return D_O_K;
  if (fov->n_beads!=0)
  {
    while(fov->n_beads !=0)  sdi_v2_remove_one_sdi_bead_from_fov(fov, 0);
  }
  if (fov->n_fringes !=0 ) while (fov->n_fringes !=0) sdi_v2_remove_one_sdi_fringes_from_fov(fov, 0);
  if (fov->n_y_profile !=0 ) while (fov->n_y_profile !=0) sdi_v2_remove_one_sdi_y_profile_from_fov(fov, 0);
  fov->n_opening_hairpins = 0;
  fov->n_ref_beads = 0;
  return D_O_K;
}
/*
int sdi_v2_find_beads_in_neighborhood(sdi_v2_fov *fov, int xc, int yc, int dx, int dy, int *index_list)
{
  int i=0;
  while(*found==false && count<fov->n_beads)
  {
    if( x_m > (fov->beads_list[count]->xc - fov->sdi_param->x_roi_size_2)
    && x_m < (fov->beads_list[count]->xc + fov->sdi_param->x_roi_size_2)
    && y_m > (fov->beads_list[count]->yc - fov->sdi_param->y_roi_size_2_Y)
    && y_m < (fov->beads_list[count]->yc + fov->sdi_param->y_roi_size_2_Y))
    {
      found_one = true;
      mouse_selected_bead = count;
      fov->beads_list[count]->mouse_draged = true;
    }
    else count ++;
  }
  return 0;
}*/

int sdi_v2_find_index_bead_in_fov(sdi_v2_fov *fov, sdi_v2_bead *bead, int *index)
{
  if (fov == NULL) return D_O_K;
  if (bead == NULL) return D_O_K;
  int i = 0;
  bool found = false;
  while (i<fov->n_beads && found == false)
  {
    if (fov->beads_list[i] == bead)
    {
      found = true;
      *index = i;
    }
    i++;
  }
  return 0;
}

int sdi_v2_find_index_y_profile_in_fov(sdi_v2_fov *fov, sdi_v2_y_profile *y_profile, int *index)
{
  if (fov == NULL) return D_O_K;
  if (y_profile == NULL) return D_O_K;
  int i = 0;
  bool found = false;
  *index=777777;
  while (i<fov->n_y_profile && found == false)
  {
    if (fov->y_profile_list[i] == y_profile)
    {
      found = true;
      *index = i;
    }
    i++;
  }
  return 0;
}
int sdi_v2_find_index_fringes_in_fov(sdi_v2_fov *fov, sdi_v2_fringes *fringes, int *index)
{
  if (fov == NULL) return D_O_K;
  if (fringes == NULL) return D_O_K;
  int i = 0;
  *index = 888888;
  bool found = false;
  while (i<fov->n_fringes && found == false)
  {
    if (fov->fringes_list[i] == fringes)
    {
      found = true;
      *index = i;
    }
    i++;
  }
  return 0;
}

//check whether the float * is also used by one display object : used to find where an object os displayed
//careful : supposed to be one disp per signal : to be corrected
int sdi_v2_find_disp_linked_to_float(sdi_v2_fov *fov, float *y_in, sdi_v2_disp **disp_out, int *index)
{
  int i, j;
  bool found = false;
  *index = 9999999;
  i=0;
  while(i<fov->n_display && found == false)
  {
    j = 0;
    while(j<fov->display_list[i]->n_y && found == false)
    {
      if(fov->display_list[i]->y[j] == y_in)
      {
        found = true;
        *disp_out = fov->display_list[i];
        *index = i;
      }
      j++;
    }
    i++;
  }
  return 0;
}

//this one erases all fov display
int sdi_v2_remove_all_display_of_fov(sdi_v2_fov *fov)
{
  int i, n;
  n = fov->n_display;
  for (i=0;i<n;i++) sdi_v2_remove_one_sdi_display_from_fov(fov, 0);
  return 0;
}

//remove display : the plots & the list
//if it is the last one, since it is not possible to erase the last one, replace with a plot with one point
int sdi_v2_remove_one_sdi_display_from_fov(sdi_v2_fov *fov, int number)
{
  int i;
  sdi_v2_remove_one_sdi_display(fov->display_list[number]);
  if (number < fov->n_display-1) for(i=number; i < fov->n_display-1 ; i++)  fov->display_list[i]=fov->display_list[i+1];
  fov->n_display--;
  return 0;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////config the display of bead data : final results, intermediate stuff, etc
/////////////////minimal configuration of display for one bead : x, y, z(t) for the bead
////////////////////////does not use more memory than default configuration
//if the plr_parent is not NULL then you add in this pltreg
//the same for op_parent (for the first plot of your config)
int sdi_v2_display_bead_quality_factor(sdi_v2_fov *fov, sdi_v2_bead *bead, pltreg *plr_parent, O_p *op_parent, int size, float t0)
{
  int display0;

  display0 = fov->n_display ;
  sdi_v2_add_one_display_to_fov(fov);
  if (plr_parent != NULL) fov->display_list[display0]->plr = plr_parent;
  if (op_parent != NULL) fov->display_list[display0]->op = op_parent;

  //to plot you need at least a 1 element buffer
  if (bead->n_output_t == 0) sdi_v2_allocate_sdi_v2_bead_output_t_fields(bead, size);
  sdi_v2_config_bead_1d_display(bead, PTR_BEAD_QUALITY_FACTOR_T, fov->display_list[display0], size, t0);
  sdi_v2_launch_display(fov->display_list[display0]->data,&fov->display_list[display0]->plr,&fov->display_list[display0]->op, "sdi display");
  set_ds_source(fov->display_list[display0]->data, "bd%d.quality", bead->index_in_fov);

  char name[128], name2[12];
  if (fov->display_list[display0]->op->title == NULL) snprintf(name, 128, "bd%d.quality", bead->index_in_fov);
  else
  {
    snprintf(name, 128, "%s",fov->display_list[display0]->op->title);
    snprintf(name2, 12, "-bd%d.quality", bead->index_in_fov);
    strncat(name, name2, 128);
  }
  set_plot_title(fov->display_list[display0]->op, "%s",name);
  set_plot_x_title(fov->display_list[display0]->op, "Time");
  set_plot_y_title(fov->display_list[display0]->op, "quality");
  //create_attach_select_x_un_to_op(op, IS_SECOND, 0,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");
  set_ds_line_color(fov->display_list[display0]->data, Yellow); //Lightgreen, Lightred orange
  fov->display_list[display0]->op->y_lo = -1;
  fov->display_list[display0]->op->y_hi = 3;
  set_plot_y_fixed_range(fov->display_list[display0]->op);
  strncat(name, ".gr", 128);
  set_op_filename(fov->display_list[display0]->op, "%s", name);
  return 0;
}
//only z
int sdi_v2_display_bead_z(sdi_v2_fov *fov, sdi_v2_bead *bead, pltreg *plr_parent, O_p *op_parent, int size, float t0)
{
  int  display0;

  display0 = fov->n_display ;
  sdi_v2_add_one_display_to_fov(fov);
  if (plr_parent != NULL) fov->display_list[display0]->plr = plr_parent;
  if (op_parent != NULL) fov->display_list[display0]->op = op_parent;

  //to plot you need at least a 1 element buffer
  if (bead->n_output_t == 0) sdi_v2_allocate_sdi_v2_bead_output_t_fields(bead, size);
  sdi_v2_config_bead_1d_display(bead, PTR_BEAD_Z_T, fov->display_list[display0], size, t0);
  sdi_v2_launch_display(fov->display_list[display0]->data,&fov->display_list[display0]->plr,&fov->display_list[display0]->op, "sdi display");
  set_ds_source(fov->display_list[display0]->data, "bd%d.z", bead->index_in_fov);

  char name[128], name2[12];
  if (fov->display_list[display0]->op->title == NULL) snprintf(name, 128, "bd%d.z", bead->index_in_fov);
  else
  {
    snprintf(name, 128, "%s", fov->display_list[display0]->op->title);
    snprintf(name2, 12, "-bd%d.z", bead->index_in_fov);
    strncat(name, name2, 128);
  }
  set_plot_title(fov->display_list[display0]->op, "%s", name);
  set_plot_x_title(fov->display_list[display0]->op, "Time");
  create_attach_select_y_un_to_op(fov->display_list[display0]->op, IS_METER, 0 ,(float)1, -6, 0, "\\mu m");
  set_plot_y_title(fov->display_list[display0]->op, "z");
  //create_attach_select_x_un_to_op(op, IS_SECOND, 0,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");
  set_ds_line_color(fov->display_list[display0]->data, Yellow); //Lightgreen, Lightred orange
  fov->display_list[display0]->op->y_lo = -3;
  fov->display_list[display0]->op->y_hi = 3;
  set_plot_y_fixed_range(fov->display_list[display0]->op);
  strncat(name, ".gr", 128);
  set_op_filename(fov->display_list[display0]->op, "%s", name);
  return 0;
}
//only x
int sdi_v2_display_bead_x(sdi_v2_fov *fov, sdi_v2_bead *bead, pltreg *plr_parent, O_p *op_parent, int size, float t0)
{
  int  display0;

  display0 = fov->n_display ;
  sdi_v2_add_one_display_to_fov(fov);
  if (plr_parent != NULL) fov->display_list[display0]->plr = plr_parent;
  if (op_parent != NULL) fov->display_list[display0]->op = op_parent;

  //to plot you need at least a 1 element buffer
  if (bead->n_output_t == 0) sdi_v2_allocate_sdi_v2_bead_output_t_fields(bead, size);
  sdi_v2_config_bead_1d_display(bead, PTR_BEAD_X_T, fov->display_list[display0], size, t0);
  sdi_v2_launch_display(fov->display_list[display0]->data,&fov->display_list[display0]->plr,&fov->display_list[display0]->op, "sdi display");
  set_ds_source(fov->display_list[display0]->data, "bd%d.x", bead->index_in_fov);

  char name[128], name2[12];
  if (fov->display_list[display0]->op->title == NULL) snprintf(name, 128, "bd%d.x", bead->index_in_fov);
  else
  {
    snprintf(name, 128, "%s", fov->display_list[display0]->op->title);
    snprintf(name2, 12, "-bd%d.x", bead->index_in_fov);
    strncat(name, name2, 128);
  }
  set_plot_title(fov->display_list[display0]->op, "%s", name);
  set_plot_x_title(fov->display_list[display0]->op, "Time");
  create_attach_select_y_un_to_op(fov->display_list[display0]->op, IS_METER, 0 ,(float)1, -6, 0, "\\mu m");
  //create_attach_select_x_un_to_op(op, IS_SECOND, 0,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");
  set_ds_line_color(fov->display_list[display0]->data, Yellow); //Lightgreen, Lightred orange
  fov->display_list[display0]->op->y_lo = -3;
  fov->display_list[display0]->op->y_hi = 3;
  set_plot_y_fixed_range(fov->display_list[display0]->op);
  strncat(name, ".gr", 128);
  set_op_filename(fov->display_list[display0]->op, "%s",name);
  return 0;
}
//only y
int sdi_v2_display_bead_y(sdi_v2_fov *fov, sdi_v2_bead *bead, pltreg *plr_parent, O_p *op_parent, int size, float t0)
{
  int  display0;

  display0 = fov->n_display ;
  sdi_v2_add_one_display_to_fov(fov);
  if (plr_parent != NULL) fov->display_list[display0]->plr = plr_parent;
  if (op_parent != NULL) fov->display_list[display0]->op = op_parent;

  //to plot you need at least a 1 element buffer
  if (bead->n_output_t == 0) sdi_v2_allocate_sdi_v2_bead_output_t_fields(bead, size);
  sdi_v2_config_bead_1d_display(bead, PTR_BEAD_Y_T, fov->display_list[display0], size, t0);
  sdi_v2_launch_display(fov->display_list[display0]->data,&fov->display_list[display0]->plr,&fov->display_list[display0]->op, "sdi display");
  set_ds_source(fov->display_list[display0]->data, "bd%d.y", bead->index_in_fov);

  char name[128], name2[12];
  if (fov->display_list[display0]->op->title == NULL) snprintf(name, 128, "bd%d.y", bead->index_in_fov);
  else
  {
    snprintf(name, 128, "%s",fov->display_list[display0]->op->title);
    snprintf(name2, 12, "-bd%d.y", bead->index_in_fov);
    strncat(name, name2, 128);
  }
  set_plot_title(fov->display_list[display0]->op, "%s", name);
  set_plot_x_title(fov->display_list[display0]->op, "Time");
  create_attach_select_y_un_to_op(fov->display_list[display0]->op, IS_METER, 0 ,(float)1, -6, 0, "\\mu m");
  //create_attach_select_x_un_to_op(op, IS_SECOND, 0,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");
  set_ds_line_color(fov->display_list[display0]->data, Yellow); //Lightgreen, Lightred orange
  fov->display_list[display0]->op->y_lo = -3;
  fov->display_list[display0]->op->y_hi = 3;
  set_plot_y_fixed_range(fov->display_list[display0]->op);
  strncat(name, ".gr", 128);
  set_op_filename(fov->display_list[display0]->op, "%s", name);
  return 0;
}








////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////config the display of fringes data : intermediate steps, inputs, outputs...
//all intermediate steps for a fringes treatment
int sdi_v2_display_fringes_all_steps(sdi_v2_fov *fov, sdi_v2_fringes *fringes, pltreg *plr, O_p *op, int size)
{
  sdi_v2_display_fringes_algo_real(fov, fringes, plr, op);
  sdi_v2_display_fringes_algo_fourier(fov, fringes, fov->display_list[fov->n_display - 1]->plr, NULL);
  sdi_v2_display_fringes_algo_raw_phase(fov, fringes, fov->display_list[fov->n_display - 1]->plr, NULL, size);
  sdi_v2_display_fringes_algo_z_result(fov, fringes, fov->display_list[fov->n_display - 1]->plr, NULL, size);
  sdi_v2_display_fringes_algo_contrast(fov, fringes, fov->display_list[fov->n_display - 1]->plr, NULL, size);
  sdi_v2_display_fringes_algo_Imax(fov, fringes, fov->display_list[fov->n_display - 1]->plr, NULL, size);
  return 0;
}
//all intermediate steps for 2 fringes treatment
int sdi_v2_display_2_fringes_all_steps(sdi_v2_fov *fov, sdi_v2_fringes *fringes1, sdi_v2_fringes *fringes2, pltreg *plr, O_p *op, int size)
{
  win_printf_OK("SIZE %d", size);
  sdi_v2_display_fringes_algo_real(fov, fringes1, plr, op);
  sdi_v2_display_fringes_algo_real(fov, fringes2, fov->display_list[fov->n_display - 1]->plr, fov->display_list[fov->n_display - 1]->op);
  sdi_v2_display_fringes_algo_fourier(fov, fringes1, fov->display_list[fov->n_display - 1]->plr, NULL);
  sdi_v2_display_fringes_algo_fourier(fov, fringes2, fov->display_list[fov->n_display - 1]->plr, fov->display_list[fov->n_display - 1]->op);
  sdi_v2_display_fringes_algo_raw_phase(fov, fringes1, fov->display_list[fov->n_display - 1]->plr, NULL, size);
  sdi_v2_display_fringes_algo_raw_phase(fov, fringes2, fov->display_list[fov->n_display - 1]->plr, fov->display_list[fov->n_display - 1]->op, size);
  sdi_v2_display_fringes_algo_z_result(fov, fringes1, fov->display_list[fov->n_display - 1]->plr, NULL, size);
  sdi_v2_display_fringes_algo_z_result(fov, fringes2, fov->display_list[fov->n_display - 1]->plr, fov->display_list[fov->n_display - 1]->op, size);
  sdi_v2_display_fringes_algo_contrast(fov, fringes1, fov->display_list[fov->n_display - 1]->plr, NULL, size);
  sdi_v2_display_fringes_algo_contrast(fov, fringes2, fov->display_list[fov->n_display - 1]->plr, fov->display_list[fov->n_display - 1]->op, size);
  sdi_v2_display_fringes_algo_Imax(fov, fringes1, fov->display_list[fov->n_display - 1]->plr, NULL, size);
  sdi_v2_display_fringes_algo_Imax(fov, fringes2, fov->display_list[fov->n_display - 1]->plr, fov->display_list[fov->n_display - 1]->op, size);
  return 0;
}

int sdi_v2_display_fringes_algo_contrast(sdi_v2_fov *fov, sdi_v2_fringes *fringes, pltreg *plr, O_p *op, int size)
{
  int  display0, f_index=0;
  display0 = fov->n_display ;
  sdi_v2_find_index_fringes_in_fov(fov, fringes, &f_index);
  //add one display
  sdi_v2_add_one_display_to_fov(fov);
  if (plr != NULL) fov->display_list[display0]->plr = plr;
  if (op != NULL) fov->display_list[display0]->op = op;

  // buffer of intermediate results
  if (fringes->n_output_t == 0) sdi_v2_allocate_sdi_v2_fringes_output_t_fields(fringes, size);
  sdi_v2_config_fringes_1d_display(fringes, PTR_FRINGES_CONTRAST_T, fov->display_list[display0], fringes->n_output_t);
  sdi_v2_launch_display(fov->display_list[display0]->data, &fov->display_list[display0]->plr, &fov->display_list[display0]->op, "sdi display");
  set_ds_source(fov->display_list[display0]->data, "f%d.C", f_index);
  set_ds_line_color(fov->display_list[display0]->data, Lightgreen); //Lightgreen, Lightred orange

  //handles the title
  char name[128], name2[12];
  if (fov->display_list[display0]->op->title == NULL) snprintf(name, 128, "f%d.C", f_index);
  else
  {
    snprintf(name, 128, "%s",fov->display_list[display0]->op->title);
    snprintf(name2, 12, "-f%d.C", f_index);
    strncat(name, name2, 128);
  }
  set_plot_title(fov->display_list[display0]->op, "%s", name);
  set_plot_x_title(fov->display_list[display0]->op, "time");
  set_plot_y_title(fov->display_list[display0]->op, "{\\color{lightgreen} contrast}");
  //faudrait trouver la liste des couleurs deja utilises pour ce plot
  fov->display_list[display0]->op->y_lo = -.1;
  fov->display_list[display0]->op->y_hi = 1.1;
  set_plot_y_fixed_range(fov->display_list[display0]->op);
  strncat(name, ".gr", 128);
  set_op_filename(fov->display_list[display0]->op, "%s",name);
  return 0;
}

int sdi_v2_display_fringes_algo_Imax(sdi_v2_fov *fov, sdi_v2_fringes *fringes, pltreg *plr, O_p *op, int size)
{
  int  display0, f_index=0;
  display0 = fov->n_display ;
  sdi_v2_find_index_fringes_in_fov(fov, fringes, &f_index);
  sdi_v2_add_one_display_to_fov(fov);
  if (plr != NULL) fov->display_list[display0]->plr = plr;
  if (op != NULL) fov->display_list[display0]->op = op;
  // buffer of intermediate results
  if (fringes->n_output_t == 0) sdi_v2_allocate_sdi_v2_fringes_output_t_fields(fringes, size);
  sdi_v2_config_fringes_1d_display(fringes, PTR_FRINGES_I_MAX_T, fov->display_list[display0], fringes->n_output_t);
  sdi_v2_launch_display(fov->display_list[display0]->data, &fov->display_list[display0]->plr, &fov->display_list[display0]->op, "sdi display");
  set_ds_source(fov->display_list[display0]->data, "f%d.IMax", f_index);
  set_ds_line_color(fov->display_list[display0]->data, Lightred); //Lightgreen, Lightred orange

  //handles the title
  char name[128], name2[12];
  if (fov->display_list[display0]->op->title == NULL) snprintf(name, 128, "f%d.IM", f_index);
  else
  {
    snprintf(name, 128, "%s", fov->display_list[display0]->op->title);
    snprintf(name2, 12, "-f%d.IM", f_index);
    strncat(name, name2, 128);
  }
  set_plot_title(fov->display_list[display0]->op, "%s", name);
  set_plot_x_title(fov->display_list[display0]->op, "time");
  set_plot_y_title(fov->display_list[display0]->op, "{\\color{lightred} Imax}");
  //faudrait trouver la liste des couleurs deja utilises pour ce plot
  fov->display_list[display0]->op->y_lo = 0;
  fov->display_list[display0]->op->y_hi = 255;
  set_plot_y_fixed_range(fov->display_list[display0]->op);
  strncat(name, ".gr", 128);
  set_op_filename(fov->display_list[display0]->op, "%s", name);
  return 0;
}

int sdi_v2_display_fringes_algo_z_result(sdi_v2_fov *fov, sdi_v2_fringes *fringes, pltreg *plr, O_p *op, int size)
{
  int i_display, display0, n_display, f_index=0;
  display0 = fov->n_display ;
  n_display = 2;
  sdi_v2_find_index_fringes_in_fov(fov, fringes, &f_index);

  for (i_display=0 ; i_display<n_display ; i_display++) sdi_v2_add_one_display_to_fov(fov);
  if (plr != NULL) fov->display_list[display0]->plr = plr;
  if (op != NULL) fov->display_list[display0]->op = op;

  // buffer of intermediate results (in the case where a previous array has been allocated, to force the size of the plot,
  // you need to first erase it with empty)
  if (fringes->n_output_t == 0) sdi_v2_allocate_sdi_v2_fringes_output_t_fields(fringes, size);
  sdi_v2_config_fringes_1d_display(fringes, PTR_FRINGES_Z_COUPLED_T, fov->display_list[display0], fringes->n_output_t);
  sdi_v2_launch_display(fov->display_list[display0]->data, &fov->display_list[display0]->plr, &fov->display_list[display0]->op, "sdi display");
  set_ds_source(fov->display_list[display0]->data, "f%d.zx.z", f_index);
  set_ds_line_color(fov->display_list[display0]->data, Lightgreen); //Lightgreen, Lightred orange

  fov->display_list[display0+1]->plr = fov->display_list[display0]->plr;
  fov->display_list[display0+1]->op = fov->display_list[display0]->op;
  sdi_v2_config_fringes_1d_display(fringes, PTR_FRINGES_X_COUPLED_T, fov->display_list[display0+1], fringes->n_output_t);
  sdi_v2_launch_display(fov->display_list[display0+1]->data, &fov->display_list[display0+1]->plr, &fov->display_list[display0+1]->op, NULL);
  set_ds_line_color(fov->display_list[display0+1]->data, yellow); //Lightgreen, Lightred orange
  set_ds_source(fov->display_list[display0+1]->data, "f%d.zx.x", f_index);

  //handles the title
  char name[128], name2[12];
  if (fov->display_list[display0]->op->title == NULL) snprintf(name, 128, "f%d.zx", f_index);
  else
  {
    snprintf(name, 128, "%s", fov->display_list[display0]->op->title);
    snprintf(name2, 12, "-f%d.zx", f_index);
    strncat(name, name2, 128);
  }
  set_plot_title(fov->display_list[display0]->op, "%s", name);
  create_attach_select_y_un_to_op(fov->display_list[display0]->op, IS_METER, 0, (float)1, -6, 0, "\\mu m");
  set_plot_x_title(fov->display_list[display0]->op, "time");
  set_plot_y_prime_title(fov->display_list[display0]->op, "{\\color{yellow} x coupled}");
  set_plot_y_title(fov->display_list[display0]->op, "{\\color{lightgreen} z coupled}");
  //pourrait être super de mettre 2 unités y pour x et z (x doit pas bouger)
  fov->display_list[display0]->op->y_lo = -5;
  fov->display_list[display0]->op->y_hi = 5;
  set_plot_y_fixed_range(fov->display_list[display0]->op);
  strncat(name, ".gr", 128);
  set_op_filename(fov->display_list[display0]->op, "%s", name);
  return 0;
}

//this one displays the raw phases from the fringes : used to illustrate, and debug when not properly tuned, the correction of the 2pi jumps
int sdi_v2_display_fringes_algo_raw_phase(sdi_v2_fov *fov, sdi_v2_fringes *fringes, pltreg *plr, O_p *op, int size)
{
  int i_display, display0, n_display, f_index=0;
  display0 = fov->n_display ;
  n_display = 3;

  for (i_display=0 ; i_display<n_display ; i_display++) sdi_v2_add_one_display_to_fov(fov);
  if (plr != NULL) fov->display_list[display0]->plr = plr;
  if (op != NULL) fov->display_list[display0]->op = op;

  // buffer of intermediate results
  if (fringes->n_phi_t == 0) sdi_v2_allocate_sdi_v2_fringes_phi_t_fields(fringes, size);
  //raw phase, with 2pi jumps not yet corrected
  sdi_v2_config_fringes_1d_display(fringes, PTR_FRINGES_RAW_PHI_0_T, fov->display_list[display0], fringes->n_phi_t);
  sdi_v2_launch_display(fov->display_list[display0]->data, &fov->display_list[display0]->plr, &fov->display_list[display0]->op, "sdi display");
  set_ds_line_color(fov->display_list[display0]->data, Lightgreen); //Lightgreen, Lightred orange
  set_ds_source(fov->display_list[display0]->data, "f%d.phi.raw", f_index);

  //corrected phase
  fov->display_list[display0+1]->plr = fov->display_list[display0]->plr;
  fov->display_list[display0+1]->op = fov->display_list[display0]->op;
  sdi_v2_config_fringes_1d_display(fringes, PTR_FRINGES_CORRECTED_PHI_0_T, fov->display_list[display0+1], fringes->n_phi_t);
  sdi_v2_launch_display(fov->display_list[display0+1]->data, &fov->display_list[display0+1]->plr, &fov->display_list[display0+1]->op, NULL);
  set_ds_line_color(fov->display_list[display0+1]->data, yellow);
  set_ds_source(fov->display_list[display0+1]->data, "f%d.phi.corr", f_index);

  //slope
  fov->display_list[display0+2]->plr = fov->display_list[display0]->plr;
  fov->display_list[display0+2]->op = fov->display_list[display0]->op;
  sdi_v2_config_fringes_1d_display(fringes, PTR_FRINGES_RAW_PHI_1_T, fov->display_list[display0+2], fringes->n_phi_t);
  sdi_v2_launch_display(fov->display_list[display0+2]->data, &fov->display_list[display0+2]->plr, &fov->display_list[display0+2]->op, NULL);
  set_ds_line_color(fov->display_list[display0+2]->data, Lightred);
  set_ds_source(fov->display_list[display0+2]->data, "f%d.slope", f_index);

  //handles the title
  char name[128], name2[12];
  sdi_v2_find_index_fringes_in_fov(fov, fringes, &f_index);
  if (fov->display_list[display0]->op->title == NULL) snprintf(name, 128, "f%d.phi", f_index);
  else
  {
    snprintf(name, 128, "%s", fov->display_list[display0]->op->title);
    snprintf(name2, 12, "-f%d.phi", f_index);
    strncat(name, name2, 128);
  }
  set_plot_title(fov->display_list[display0]->op, "%s", name);
  set_plot_x_title(fov->display_list[display0]->op, "Time");
  set_plot_y_title(fov->display_list[display0]->op, "{\\color{Ligthtgreen} raw phases} {\\color{yellow} corrected phase}");
  fov->display_list[display0]->op->y_lo = -12;
  fov->display_list[display0]->op->y_hi = 12;
  set_plot_y_fixed_range(fov->display_list[display0]->op);
  strncat(name, ".gr", 128);
  set_op_filename(fov->display_list[display0]->op, "%s", name);
  return 0;
}

int sdi_v2_display_fringes_algo_fourier(sdi_v2_fov *fov, sdi_v2_fringes *fringes, pltreg *plr, O_p *op)
{
  int i_display, display0, n_display, f_index=0;
  i_display=0;
  n_display = 2;
  display0 = fov->n_display ;
  sdi_v2_find_index_fringes_in_fov(fov, fringes, &f_index);

  for (i_display=0 ; i_display<n_display ; i_display++)   sdi_v2_add_one_display_to_fov(fov);
  if (plr != NULL) fov->display_list[display0]->plr = plr;
  if (op != NULL) fov->display_list[display0]->op = op;

  if (fringes->n_phase_buffer == 0) sdi_v2_allocate_sdi_v2_fringes_2d_float_fields(fringes, fov->sdi_param, 1, PTR_FRINGES_PHASE);
  sdi_v2_config_fringes_2d_display(fringes, PTR_FRINGES_PHASE, fov->display_list[display0], fov->sdi_param);
  sdi_v2_launch_display(fov->display_list[display0]->data, &fov->display_list[display0]->plr, &fov->display_list[display0]->op, "sdi display");
  set_ds_line_color(fov->display_list[display0]->data, yellow); //Lightgreen, Lightred orange
  set_ds_source(fov->display_list[display0]->data, "f%d.FT.phase", f_index);

  fov->display_list[display0+1]->plr = fov->display_list[display0]->plr;
  fov->display_list[display0+1]->op = fov->display_list[display0]->op;
  if (fringes->n_amplitude_buffer == 0) sdi_v2_allocate_sdi_v2_fringes_2d_float_fields(fringes, fov->sdi_param, 1, PTR_FRINGES_AMPLITUDE);
  sdi_v2_config_fringes_2d_display(fringes, PTR_FRINGES_AMPLITUDE, fov->display_list[display0+1], fov->sdi_param);
  sdi_v2_launch_display(fov->display_list[display0+1]->data, &fov->display_list[display0+1]->plr, &fov->display_list[display0+1]->op, NULL);
  set_ds_source(fov->display_list[display0+1]->data, "f%d.FT.amp", f_index);
  set_ds_line_color(fov->display_list[display0+1]->data, Lightred); //Lightgreen, Lightred orange

  //handles the title
  char name[128], name2[12];
  if (fov->display_list[display0]->op->title == NULL) snprintf(name, 128, "f%d.FT", f_index);
  else
  {
    snprintf(name, 128, "%s", fov->display_list[display0]->op->title);
    snprintf(name2, 12, "-f%d.FT", f_index);
    strncat(name, name2, 128);
  }
  set_plot_x_title(fov->display_list[display0]->op, "Frequency (pix ^-1)");
  set_plot_y_title(fov->display_list[display0]->op, "{\\color{yellow} spectral phase (rad)}");
  set_plot_y_prime_title(fov->display_list[display0+1]->op, "{\\color{lightred} amplitude}");
  fov->display_list[display0]->op->y_lo = -5;
  fov->display_list[display0]->op->y_hi = 20;
  set_plot_y_fixed_range(fov->display_list[display0]->op);
  set_plot_title(fov->display_list[display0]->op, "%s", name);
  strncat(name, ".gr", 128);
  set_op_filename(fov->display_list[display0]->op, "%s", name);

  return 0;
}

//display real space stuff for fringes treatment : can be used to control the raw signal quality, the window shape, the background
int sdi_v2_display_fringes_algo_real(sdi_v2_fov *fov, sdi_v2_fringes *fringes, pltreg *plr, O_p *op)
{
  int i_display, display0, n_display, f_index=0;
  n_display = 3;
  display0 = fov->n_display ;
  sdi_v2_find_index_fringes_in_fov(fov, fringes, &f_index);
  //create the display objects
  for (i_display=0 ; i_display<n_display ; i_display++)   sdi_v2_add_one_display_to_fov(fov);
  if (plr != NULL) fov->display_list[display0]->plr = plr;
  if (op != NULL) fov->display_list[display0]->op = op;

  //to plot you need at least a 1 element buffer
  if (fringes->n_raw_x_profile_buffer == 0) sdi_v2_allocate_sdi_v2_fringes_2d_float_fields(fringes, fov->sdi_param, 1, PTR_FRINGES_RAW_X_PROFILE);
  sdi_v2_config_fringes_2d_display(fringes, PTR_FRINGES_RAW_X_PROFILE, fov->display_list[display0], fov->sdi_param);
  sdi_v2_launch_display(fov->display_list[display0]->data, &fov->display_list[display0]->plr, &fov->display_list[display0]->op, "sdi display");
  set_ds_source(fov->display_list[display0]->data, "f%d.R.raw", f_index);
  set_ds_line_color(fov->display_list[display0]->data, Lightgreen);

  fov->display_list[display0+1]->plr = fov->display_list[display0]->plr;
  fov->display_list[display0+1]->op = fov->display_list[display0]->op;
  if (fringes->n_windowed_x_profile_buffer==0) sdi_v2_allocate_sdi_v2_fringes_2d_float_fields(fringes,fov->sdi_param,1,PTR_FRINGES_WINDOWED_X_PROFILE);
  sdi_v2_config_fringes_2d_display(fringes, PTR_FRINGES_WINDOWED_X_PROFILE, fov->display_list[display0+1], fov->sdi_param);
  sdi_v2_launch_display(fov->display_list[display0+1]->data, &fov->display_list[display0+1]->plr, &fov->display_list[display0+1]->op, NULL);
  set_ds_source(fov->display_list[display0+1]->data, "f%d.R.wsig", f_index);
  set_ds_line_color(fov->display_list[display0+1]->data, yellow);

  fov->display_list[display0+2]->plr = fov->display_list[display0]->plr;
  fov->display_list[display0+2]->op=fov->display_list[display0]->op;
  if (fringes->n_x_window_buffer == 0) sdi_v2_allocate_sdi_v2_fringes_2d_float_fields(fringes, fov->sdi_param, 1, PTR_FRINGES_X_WINDOW);
  sdi_v2_config_fringes_2d_display(fringes, PTR_FRINGES_X_WINDOW, fov->display_list[display0+2], fov->sdi_param);
  sdi_v2_launch_display(fov->display_list[display0+2]->data, &fov->display_list[display0+2]->plr, &fov->display_list[display0+2]->op, NULL);
  set_ds_source(fov->display_list[display0+2]->data, "f%d.R.window", f_index);
  set_ds_line_color(fov->display_list[display0+2]->data, Lightred);

  //handles the title, names etc
  char name[128], name2[12];
  if (fov->display_list[display0]->op->title == NULL) snprintf(name, 128, "f%d.R", f_index);
  else
  {
    snprintf(name, 128, "%s", fov->display_list[display0]->op->title);
    snprintf(name2, 12, "-f%d.R", f_index);
    strncat(name, name2, 128);
  }
  set_plot_title(fov->display_list[display0]->op, "%s", name);
  set_plot_x_title(fov->display_list[display0]->op, "Pixel");
  set_plot_y_title(fov->display_list[display0]->op, "{\\color{lightgreen} raw sig}  {\\color{Lightred} window} {\\color{yellow} sig}");
  //unité  create_attach_select_y_un_to_op(op, IS_METER, 0 ,(float)1, -3, 0, "mm");
  //create_attach_select_y_un_to_op(O_p *op, int type, float ax, float dx, char decade, char mode, char *name)
  fov->display_list[display0]->op->x_lo = -1;
  fov->display_list[display0]->op->y_hi = 256;
  set_plot_y_fixed_range(fov->display_list[display0]->op);
  strncat(name, ".gr", 128);
  set_op_filename(fov->display_list[display0]->op, "%s", name);
  return 0;
}
/*to be debugged
//this display the fitted part of the spectral phase
int sdi_v2_set_fringes_fourier_algo_display3(sdi_v2_fov *fov, sdi_v2_fringes *fringes, pltreg *plr, O_p *op)
{
  int i_display, display0, n_display, f_index=0;
  i_display=0;
  n_display = 2;
  display0 = fov->n_display ;
  char name[32];
  sdi_v2_find_index_fringes_in_fov(fov, fringes, &f_index);
  snprintf(name, 32, "f%d : fit", f_index);

  for (i_display=0 ; i_display<n_display ; i_display++)   sdi_v2_add_one_display_to_fov(fov);
  if (plr != NULL) fov->display_list[display0]->plr = plr;
  if (op != NULL) fov->display_list[display0]->op = op;

  if (fringes->n_nu_roi_buffer == 0) sdi_v2_allocate_sdi_v2_fringes_2d_float_fields(fringes, fov->sdi_param, 1, PTR_FRINGES_NU_ROI);
  if (fringes->n_raw_phase_roi_buffer == 0) sdi_v2_allocate_sdi_v2_fringes_2d_float_fields(fringes, fov->sdi_param, 1, PTR_FRINGES_RAW_PHASE_ROI);
  sdi_v2_config_fringes_2d_display(fringes, PTR_FRINGES_RAW_PHASE_ROI, fov->display_list[display0], fov->sdi_param);
  sdi_v2_launch_display(fov->display_list[display0]->data, &fov->display_list[display0]->plr, &fov->display_list[display0]->op, name);
  fov->display_list[display0]->op->y_lo = -5;
  fov->display_list[display0]->op->y_hi = 5;
  set_plot_y_fixed_range(fov->display_list[display0]->op);
  set_ds_line_color(fov->display_list[display0]->data, Lightred); //Lightgreen, Lightred orange
  //unité  create_attach_select_y_un_to_op(op, IS_METER, 0 ,(float)1, -3, 0, "mm");
  //create_attach_select_y_un_to_op(O_p *op, int type, float ax, float dx, char decade, char mode, char *name)
  set_plot_x_title(fov->display_list[display0]->op, "Omega");
  set_plot_y_title(fov->display_list[display0]->op, "{\\color{Lightred} raw phase roi} {\\color{green} unwrapped phase roi}");
  //char *c;
  //c = (char *)calloc(1, sizeof(char));
  //c[0] = 'x';
  //set_ds_point_symbol(fov->display_list[display0 + i_display]->data, c);

  fov->display_list[display0+1]->plr = fov->display_list[display0]->plr;
  fov->display_list[display0+1]->op = fov->display_list[display0]->op;
  if (fringes->n_unwrapped_phase_roi_buffer == 0) sdi_v2_allocate_sdi_v2_fringes_2d_float_fields(fringes, fov->sdi_param, 1, PTR_FRINGES_UNWRAPPED_PHASE_ROI);
  sdi_v2_config_fringes_2d_display(fringes, PTR_FRINGES_UNWRAPPED_PHASE_ROI, fov->display_list[display0+1], fov->sdi_param);
  sdi_v2_launch_display(fov->display_list[display0+1]->data, &fov->display_list[display0+1]->plr,&fov->display_list[display0+1]->op, NULL);
  set_ds_line_color(fov->display_list[display0+1]->data, Lightgreen); //Lightgreen, Lightred orange
  return 0;
}
*/
