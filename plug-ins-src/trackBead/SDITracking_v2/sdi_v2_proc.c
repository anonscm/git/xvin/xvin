# include "sdi_proc.h"


//for T_roi type trigger of rt-proc handles whether the trigger in on/off
int update_sdi_T_select_trigger_opening(sdi_T_select *T_roi, int current_i)
{
  if (current_i == 0)
  {
    T_roi->current_index = 0;
    T_roi->current_i_select = T_roi->offset;
    T_roi->trigger = false;
  }
  if(current_i == T_roi->current_i_select)  T_roi->trigger = true;
  return 0;
}

int update_sdi_T_select_trigger_closing(sdi_T_select *T_roi)
{
  if (T_roi->trigger == true)
  {
    T_roi->current_index ++;
    T_roi->current_i_select = (int) (T_roi->current_index / T_roi->subset_length) * T_roi->period + T_roi->current_index % T_roi->subset_length + T_roi->offset;
    T_roi->trigger = false;
  }
  return 0;
}


/*
///////////////////////
//this adds one results dataset in d_s **proc_results
//the ds * itself is not allocated, this is done during the buffer size intialization
int set_fourier_filter_parameter(sdi_fbt_filter *fbt_filter, int size, int max_size, int hp, int wh, int lp, int wl)
{
  fl=0; //real data
  ft->n = n_2;
  fbt_filter =filter_init_bt( fbt_filter, size);
}
int n;		/*size of the filter array in use
int m;		/*max. size of the array, n<m
int hp;		/*high-pass freq.
int wh;		/*high-pass width
int lp;		/*low-pass freq./
int wl;		/*low-pass width/
int fl;		/*flag 0(1) indicating real(complex) data
*/

//allocated outside
int init_one_proc(sdi_proc *proc, int which_proc, int which_object, int which_data)
{
  proc->which_proc = which_proc;
  proc->which_object = which_object;
  proc->which_data = which_data;
  proc->which_output = -1;//said that not linked yet
  switch (proc->which_proc) {
    case PROC1:
    proc->T_roi = NULL;
    proc->T_roi = (sdi_T_select *)calloc(1, sizeof(sdi_T_select));
    break;
    case PROC2:
    proc->T_roi = NULL;
    proc->T_roi = (sdi_T_select *)calloc(1, sizeof(sdi_T_select));
    break;
  }
  return 0;
}


int set_T_roi_parameter(sdi_T_select *T_roi, int period, int offset, int subset_length, int i_min, int i_max, int delay)
{
  T_roi->period = period;
  T_roi->subset_length = subset_length;
  T_roi->i_min = i_min;
  T_roi->i_max = i_max;
  T_roi->offset = offset;
  T_roi->delay = delay;
  T_roi->current_index = 0;
  T_roi->current_i_select = 0;
  T_roi->trigger = false;
  return 0;
}


//current_index et current_time sont données de l'objet
int call_proc_on_ds(sdi_proc *proc, d_s *ds_in, d_s *ds_out, int current_index, float current_time)
{
  switch (proc->which_proc) {
    case PROC1:
    {
      if (proc->T_roi == NULL) return D_O_K;
      compute_proc1( proc->T_roi, ds_in, ds_out, current_index, current_time);
      break;
    }
    case PROC2:
    {
      if (proc->T_roi == NULL) return D_O_K;
      compute_proc2( proc->T_roi, ds_in, ds_out, current_index, current_time);
      break;
    }
  }
  return 0;
}

int allocate_sdi_fringes_proc_t_fields(sdi_fringes *fringes, int buffer_size, int *which_data, int n_which_data)
{
  int i, which_data_i;
  d_s *data=NULL;
  for (i=0;i<n_which_data;i++)
  {
    which_data_i=*(which_data+i);
    switch_ptr_to_sdi_fringes_proc_field(fringes, which_data_i, &data);
    data=build_adjust_data_set(data,buffer_size,buffer_size);
    data->nx=buffer_size;
    data->ny=buffer_size;
  }
  return 0;
}
//this allocates time dependent fields of an y_profile object
int allocate_sdi_y_profile_proc_t_fields(sdi_y_profile *y_profile, int buffer_size, int *which_data, int n_which_data)
{
  int i, which_data_i;
  d_s *data=NULL;
  for (i=0;i<n_which_data;i++)
  {
    which_data_i=*(which_data+i);
    switch_ptr_to_sdi_y_profile_proc_field(y_profile, which_data_i, &data);
    data=build_adjust_data_set(data,buffer_size,buffer_size);
    data->nx=buffer_size;
    data->ny=buffer_size;
  }
  return 0;
}
//this allocates time dependent fields of a fringe object
int allocate_sdi_bead_proc_t_fields(sdi_bead *bead, int buffer_size, int *which_data, int n_which_data)
{
  int i, which_data_i;
  d_s *data=NULL;
  for (i=0;i<n_which_data;i++)
  {
    which_data_i=*(which_data+i);
    switch_ptr_to_sdi_bead_proc_field(bead, which_data_i, &data);
    data=build_adjust_data_set(data,buffer_size,buffer_size);
    data->nx=buffer_size;
    data->ny=buffer_size;
  }
  return 0;
}


////////////////////
//this calls the proc itself : find the ptr and call the proc
/*
int compute_proc(sdi_general *gen, sdi_proc *proc, int object_index)
{
  d_s *in=NULL;
  d_s *out=NULL;
  int current_index=0;
  float current_time=0;
  switch_ptr_to_proc_fields(gen, proc, object_index, &in, &out, &current_index, &current_time)
  switch (proc->which_proc) {
    case PROC1:
    {
      if (proc->T_roi == NULL) return D_O_K;
      compute_proc1(proc->T_roi, in, out, current_index, current_time);
      break;
    }
    case PROC2:
    {
      if (proc->T_roi == NULL) return D_O_K;
      compute_proc2(proc->T_roi, in, out, current_index, current_time);
      break;
    }
  }
  return 0;
}
//this defines a switch just befaroe computation
int switch_ptr_to_proc_fields(sdi_general *gen, sdi_proc *proc, int object_index, d_s **in, d_s **out, int *current_index, float *current_time)
{
  switch (proc->which_object) {
    case IS_ONE_SDI_FRINGE :
    {
      switch_ptr_to_sdi_fringes_field(&gen->fringes_list[object_index], proc->which_data, in);
      switch_ptr_to_sdi_fringes_proc_field(&gen->fringes_list[object_index], proc->which_output, out);
      *current_index = &gen->fringes_list[object_index].current_index;
      *current_time = &gen->fringes_list[object_index].current_time;
      break;
    }
    case IS_ONE_BEAD :
    {
      switch_ptr_to_sdi_bead_field(&gen->beads_list[object_index], proc->which_data, in);
      switch_ptr_to_sdi_bead_proc_field(&gen->beads_list[object_index], proc->which_output, out);
      *current_index = &gen->bead_list[object_index].current_index;
      *current_time = &gen->bead_list[object_index].current_time;
      break;
    }
    case IS_ONE_Y_PROFILE :
    {
      switch_ptr_to_sdi_y_profile_field(&gen->y_profile_list[object_index], proc->which_data, in);
      switch_ptr_to_sdi_y_profile_proc_field(&gen->y_profile_list[object_index], proc->which_output, out);
      *current_index = &gen->y_profile_list[object_index].current_index;
      *current_time = &gen->y_profile_list[object_index].current_time;
      break;
    }
  }
  return 0;
}
*/
int add_one_proc_to_sdi_bead(sdi_bead *bead, sdi_proc *proc)
{
  if (bead == NULL || proc==NULL) return D_O_K;
  d_s *tmp=NULL;
  if(proc->which_output == -1) proc->which_output = bead->n_proc;
  switch_ptr_to_sdi_bead_proc_field(bead, proc->which_output, &tmp);
  return 0;
}

int add_one_proc_to_sdi_y_profile(sdi_y_profile *y_profile, sdi_proc *proc)
{
  if (y_profile == NULL || proc==NULL) return D_O_K;
  d_s *tmp=NULL;
  if(proc->which_output == -1) proc->which_output = y_profile->n_proc;
  switch_ptr_to_sdi_y_profile_proc_field(y_profile, proc->which_output, &tmp);
  return 0;
}

int add_one_proc_to_sdi_fringes(sdi_fringes *fringes, sdi_proc *proc)
{
  if (fringes == NULL || proc==NULL) return D_O_K;
  d_s *tmp=NULL;
  if(proc->which_output == -1) proc->which_output = fringes->n_proc;
  switch_ptr_to_sdi_fringes_proc_field(fringes, proc->which_output, &tmp);
  return 0;
}
//this function switch a ds ptr to one in the rt-proc list
//it also declares it and initialize it if its index is fringes-n_proc+1
int switch_ptr_to_sdi_fringes_proc_field(sdi_fringes *fringes, int proc_index, d_s **ds)
{
  if (proc_index <= fringes->n_proc)
  {
    if (fringes->n_proc ==0)
    {
      fringes->proc_results = (d_s **)calloc(1, sizeof(d_s *));
      fringes->n_proc = 1;
      fringes->proc_results[0] = NULL;
      fringes->proc_results[0] = build_adjust_data_set(fringes->proc_results[proc_index], 1, 1);
      fringes->proc_results[0]->nx=1;
      fringes->proc_results[0]->ny=1;
    }
    else
    {
      if (proc_index == fringes->n_proc)
      {
        fringes->n_proc ++;
        fringes->proc_results = (d_s **)realloc(fringes->proc_results, (fringes->n_proc) * sizeof(d_s *));
        fringes->proc_results[proc_index] = NULL;
        fringes->proc_results[proc_index] = build_adjust_data_set(fringes->proc_results[proc_index], 1, 1);
        fringes->proc_results[proc_index]->nx=1;
        fringes->proc_results[proc_index]->ny=1;
      }
    }
    *ds = fringes->proc_results[proc_index];
  }
  return 0;
}
int sdi_fringes_remove_one_proc(sdi_fringes *fringes, int proc_index)
{
  int i;
  if (proc_index >= fringes->n_proc) return D_O_K;
  free_data_set(fringes->proc_results[proc_index]);
  //not going back to NULL otherwise need to re declare it
  for (i=proc_index; i<fringes->n_proc-1; i++) fringes->proc_results[i] = fringes->proc_results[i+1];
  fringes->n_proc --;
  return 0;
}

int switch_ptr_to_sdi_y_profile_proc_field(sdi_y_profile *y_profile, int proc_index, d_s **ds)
{
  if (proc_index <= y_profile->n_proc)
  {
    if (y_profile->n_proc == 0)
    {
      y_profile->proc_results = (d_s **)calloc(1, sizeof(d_s *));
      y_profile->n_proc = 1;
      y_profile->proc_results[0] = NULL;
      y_profile->proc_results[0] = build_adjust_data_set(y_profile->proc_results[proc_index], 1, 1);
      y_profile->proc_results[0]->nx = 1;
      y_profile->proc_results[0]->ny = 1;
    }
    else
    {
      if (proc_index == y_profile->n_proc)
      {
        y_profile->n_proc ++;
        y_profile->proc_results = (d_s **)realloc(y_profile->proc_results, (y_profile->n_proc) * sizeof(d_s *));
        y_profile->proc_results[proc_index] = NULL;
        y_profile->proc_results[proc_index] = build_adjust_data_set(y_profile->proc_results[proc_index], 1, 1);
        y_profile->proc_results[proc_index]->nx = 1;
        y_profile->proc_results[proc_index]->ny = 1;
      }
    }
  *ds = y_profile->proc_results[proc_index];
  }
  return 0;
}

int switch_ptr_to_sdi_bead_proc_field(sdi_bead *bead, int proc_index, d_s **ds)
{
  if (proc_index <= bead->n_proc)
  {
    if (bead->n_proc == 0)
    {
      bead->proc_results = (d_s **)calloc(1, sizeof( d_s *));
      bead->n_proc = 1;
      bead->proc_results[0] = NULL;
      bead->proc_results[0] = build_adjust_data_set(bead->proc_results[proc_index], 1, 1);
      bead->proc_results[0]->nx = 1;
      bead->proc_results[0]->ny = 1;
    }
    else
    {
      if (proc_index == bead->n_proc)
      {
        bead->n_proc ++;
        bead->proc_results = (d_s **)realloc(bead->proc_results, (bead->n_proc) * sizeof( d_s *));
        bead->proc_results[proc_index] = NULL;
        bead->proc_results[proc_index] = build_adjust_data_set(bead->proc_results[proc_index], 1, 1);
        bead->proc_results[proc_index]->nx = 1;
        bead->proc_results[proc_index]->ny = 1;
      }
    }
  *ds = bead->proc_results[proc_index];
  }
  return 0;
}


///////////////////////////////////
//proc1 : moving average
int compute_proc1(sdi_T_select *T_roi, d_s *in, d_s *out, int current_i, float current_time)
{
  int i, work_i_min, work_i_max;
  work_i_min=current_i - T_roi->delay - T_roi->i_min;
  work_i_max=current_i - T_roi->delay + T_roi->i_max;
  //win_printf_OK("work min %d max %d",work_i_min,work_i_max);
  if (work_i_min <0 ) work_i_min=0;
  if (work_i_max <0 ) work_i_max=0;
  if (work_i_min >= work_i_max)
  {
    out->xd[T_roi->current_index]=current_time;
    out->yd[T_roi->current_index]=in->yd[current_i];
  }
  else
  {
    out->xd[T_roi->current_index]=current_time;
    out->yd[T_roi->current_index]=0;
    for (i= work_i_min ; i< work_i_max ; i++)
    {
      out->yd[T_roi->current_index] += in->yd[i];
    }
    out->yd[T_roi->current_index] /= (float) (T_roi->i_max + T_roi->i_min);
  }
  return 0;
}
//proc2 : moving sigma
int compute_proc2(sdi_T_select *T_roi, d_s *in, d_s *out, int current_i, float current_time)
{
  int i, work_i_min, work_i_max;
  double mean;
  work_i_min=current_i - T_roi->delay - T_roi->i_min;
  work_i_max=current_i - T_roi->delay + T_roi->i_max;
  if (work_i_min <0 ) work_i_min=0;
  if (work_i_max <0 ) work_i_max=0;
  if (work_i_min >= work_i_max)
  {
    out->xd[T_roi->current_index]=current_time;
    out->yd[T_roi->current_index]=in->yd[current_i];
  }
  else
  {
    out->xd[T_roi->current_index]=current_time;
    out->yd[T_roi->current_index]=0;
    mean=0;
    for (i= work_i_min ; i< work_i_max ; i++)
    {
      mean += in->yd[i];
      out->yd[T_roi->current_index] += in->yd[i] * in->yd[i];
    }
    out->yd[T_roi->current_index] -= (float) (mean * mean / (float) (T_roi->i_max + T_roi->i_min)) ;
    out->yd[T_roi->current_index] /= (float) (T_roi->i_max + T_roi->i_min -1);
    out->yd[T_roi->current_index] = (float) (sqrt(out->yd[T_roi->current_index]));
    //win_printf_OK("val proc 2, x roiT %d, x %d y %d", current_i, out->xd[T_roi->current_index], out->yd[T_roi->current_index]);
  }
  return 0;
}
/*
//this set the cutoff of abt_filter object
//this function takes an axis x in time-domain (can be x, t..), find the index where
//returns nu_bt the index of the cutoff frequency
int bt_convert_cutoff(float nu_c, d_s *sig, int *nu_bt)
{
  int i;
  float y_min;
  float *tmp=NULL;
  tmp = (float *)calloc(1,sig->nx * sizeof(float));
  for (i=0;i<sig->nx;i++) *(tmp+i)=fabs( sig->xd[i] - nu_c );
  tab_min(tmp, 0, sig->nx, &y_min, nu_bt);
  free (tmp);
  tmp=NULL;
  return 0;
}
//proc3 : filter the DC of a signal
int compute_proc3(sdi_)
//utiliser l'objet filtre de vc?
filter_init_bt
int lowpass_smooth_half_bt (filter *ft, int npts, float *x, int cutoff)
int hipass_smooth_half_bt (filter *ft, int npts, float *x, int cutoff) //calcule filtre que si change ou vide

realtr1bt(fp, dsc->yd);           fftbt(fp, dsc->yd, 1);      realtr2bt(fp, dsc->yd, 1);
// we fft-1 on n_expan * dsd->nx points
realtr2bt(fpe, dsc->yd, -1);      fftbt(fpe, dsc->yd, -1);      realtr1bt(fpe, dsc->yd);
*/
