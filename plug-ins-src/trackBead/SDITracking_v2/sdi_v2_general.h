#pragma once
///////////////interface between track_util (general bead object handled by record, trackbead...) & sdi_v2_fov_analysis (sdi beads)
#include "sdi_v2_fov_analysis.h"
#include "../trackBead.h"
# include "../../cfg_file/Pico_cfg.h"
#include "../../cfg_file/microscope.h"
# include <hdf5/serial/hdf5.h>

//job on beads recording, eventually with a peripheral movement
#define SDI_Z_B_NOTHING 0
#define SDI_Z_FIND_0_OF_MOLECULE   1
#define SDI_Z_FIND_REF_BEADS 2
#define SDI_Z_CLASSIFY_BEAD_FUNCTION 3

//jobs on y profile objects, allows to detect the bead
#define SDI_Y_Y_NOTHING 20
#define SDI_FIND_BEADS_BY_CORRELATION 21


#define SDI_RAMP_NOTHING 40
#define SDI_RAMP_F_CALIBRATION 41
#define SDI_RAMP_F_LINEARITY_CHECK 42

#define SDI_OI_NOTHING 60
#define SDI_OI_FRINGES_DETECTION 61

# define PTR_DATATYPE_INT 0
# define PTR_DATATYPE_FLOAT 1
# define PTR_DATATYPE_LLONG 2

extern bool go_rt_track_y;
extern bool go_rt_track_xz;
extern float dt_computation_ms_y;
extern float dt_computation_ms_xz;

//union for ptr onto data of hdf5 1d data
typedef union hdf5_data_ptr
{
  int *idata;
  float *fdata;
  long long *lldata;
} data_ptr;

//#ifdef HDF5_RECORD
typedef struct hdf5_1d_data
{
  hid_t dataset, group, sub_group, file, dataspace;
  char filename[128];
  char datasetname[128];
  char groupname[128];
  char subgroupname[128];
  bool in_group;
  bool in_subgroup;
  hsize_t chunk_dims[1];//how much to allocate when necessary
  //hsize_t write_dims[1];//how much to write_dims
  hsize_t file_dims[1];//current dim of dataset
  hsize_t offset_file[1];
  int write_period;//when to write dims
  int mem_offset;
  int datatype;
  int t0;
  data_ptr data;
} hdf5_1d;
typedef struct hdf5_general
{
  char filename[512];
  hdf5_1d **hdf5_1d_list;
  int n_hdf5_1d;
} hdf5_gen;
//#endif

///this ones is priority : not a job, directly in track util, action
//(jobs sometimes start later than expected im..)
typedef struct sdi_v2_grab_param
{
  bool grab;
  int n_frame;
  int c_frame;
  int start_delay;
  int period;
  float zobj_target;
  float *zobj_mes_grab;
  float zmag_target;
  float *zmag_mes_grab;
  int which_treatment;
  int verbose;
  int replace;
  pltreg *plr;
  O_i *oi;
}sdi_v2_grab;


//parameters to register a movie related to a treatment, like fringes calibration
typedef struct sdi_v2_ramp_param
{
  int n_step;               // the number of steps i.e. of lines in the calib image
  int n_wait;                // n frame to wait when pifoc is moving
  int n_per_frame;                // the number of frames averaged at each position of pifoc
  int *im;                 // the array of images number where each objecive step occurs
  int im_start_zerof;      // the image number defining the beginning of the zero adjustment phase

  float zobj_start;        // the starting objective position
  float zobj_step;         // the step size of focus step
  float *zo_cmd;               // the array of objective position command
  float *zo_mean_mes;       //the array of obj pos measured (averaged)
  float z_mag_hf; //high force magnetic tweezers position to record images
  int *measurement_count; //a counter array that count how many points you take for each value (sometimes we can miss frames)

  int which_treatment;
  int verbose;
  int replace;
  pltreg *plr;
  O_i *oi;
} sdi_v2_ramp;

//to make an image analysis, possibly on everal frames
typedef struct sdi_v2_image_analysis
{
  int n_points;
  O_i *oi;
  int which_treatment;
  sdi_v2_f_d *sdi_f_d; //used when image processing consist in looking for fringes pattern
} sdi_v2_oi_analysis;

//to record bead position with eventually a peripheral movement to and analyze the bead (opening/not, take reference etc)
typedef struct sdi_v2_position_bead_analysis
{
  int n_points;
  int which_treatment;//encodes what to do with this long z(t) record : 0of molecule detection, opening check
  d_s *zmag_out;    //copy of zmag if necessary (opening check)
  float z_mag_close;
  float z_mag_open;
  float z_mag_lf;
  int cycle_period; //in frame, when you want to do cycles with magnets
  int continous_exp;
  int verbose;
  int replace;
  pltreg *plr;
} sdi_v2_xyz_b;

//job that record y(t) of y profile objects and do a processing on it (for now detection of beads)
typedef struct sdi_v2_y_y_profile_analysis
{
  int n_points;
  int which_treatment;//encodes what to do with this long y(t) record : detection of beads by correlation of brownian motion etc
  float zmag_y_corr;
  pltreg *plr;
  O_p *op;
} sdi_v2_y_y;

//#ifdef HDF5_RECORD
PXV_FUNC(int, do_save_beads_to_cfg_file, (void));
PXV_FUNC(int, do_load_beads_from_cfg_file, (void));

PXV_FUNC(int, update_hdf5_1d, (hdf5_1d *hdf5_1d_in, int i, bool open_file, hid_t *file_id, bool close_file));
//#endif
PXV_FUNC(int, sdi_v2_grab_function, (int im));
//automatic detection of beads by correlation of brownian motion of y profile fluctuation
PXV_FUNC(int, sdi_instrument_init, (void));
PXV_FUNC(int, sdi_v2_calibration_fringes_job, (int im, f_job *job));
PXV_FUNC(MENU *, sdi_menu, (void));
PXV_FUNC(int, sdi_v2_count_opening_hp, (void));
PXV_FUNC(int, sdi_v2_correlate_bead_signal_with_peripheral, (sdi_v2_bead *bead, float *corr_product, bool see_result, int n_extract,
                    int which_data, int which_periph, pltreg *plr, O_p *op));
////////bead menus//////////////
PXV_FUNC(MENU *, sdi_one_bead_menu, (void));
PXV_FUNC(int, sdi_v2_remove_mouse_selected_bead, (void));
PXV_FUNC(int, sdi_v2_extract_peripheral_signal_from_track_info, (d_s *extract, int which_peripheral, int n_extract));
/////fringes only display/////////////
PXV_FUNC(int, do_display_fringe_1, (void));
PXV_FUNC(MENU *, sdi_one_fringes_menu, (void));

/////y profile only display///////////
PXV_FUNC(int, do_display_y_profile_1, (void));
PXV_FUNC(MENU *, sdi_one_y_profile_menu, (void));

/////handles the global fov
PXV_FUNC(int, do_set_sdi_v2_parameters_gui, (void));
PXV_FUNC(int, sdi_v2_add_one_bead_to_fov_with_mouse, (sdi_v2_fov *fov));
PXV_FUNC(int, do_calibrate_fov_from_movie, (void));

PXV_FUNC(int, sdi_v2_set_peripheral_display1, (int which_data, int size, pltreg *plr_parent, O_p *op_parent));
PXV_FUNC(int, sdi_v2_config_peripheral_display, (int which_data, sdi_v2_disp *display, int size));
//
PXV_FUNC(int, sdi_v2_oi_mouse_action, (O_i *oi, int x0, int y0, int mode, DIALOG *d));
PXV_FUNC(int, sdi_v2_oi_idle_action, (struct one_image *oi, DIALOG *d));
