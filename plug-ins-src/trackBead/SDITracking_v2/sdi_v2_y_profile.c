#include "sdi_v2_y_profile.h"

//this contain the functions wrapping the sdi operations on the sdi y_profile structures
int sdi_v2_switch_ptr_to_sdi_y_profile_1d_float_field(sdi_v2_y_profile *y_profile, int which_data, float **field, float **ext_float)
{
  if (y_profile == NULL) return 1;
  switch(which_data){

    //really essential for the following
    case PTR_Y_PROFILE_Y_T :
    if (y_profile->y_t == NULL)
    {
      if (ext_float == NULL) y_profile->y_t = (float *)calloc(1, sizeof(float));
      else y_profile->y_t = *ext_float;
    }
    *field = y_profile->y_t;
    break;
    //to be added : quality factor etc

  }
  return 0;
}

//standalone incrementation of current points
int sdi_v2_update_c_profile_y_profile_points(sdi_v2_y_profile *y_profile, int ci)
{
  if (y_profile == NULL) return 1;  
  if (y_profile->n_output_t !=0) y_profile->c_output_t = ci % y_profile->n_output_t;
  if (y_profile->n_raw_y_profile_buffer !=0 ) y_profile->c_raw_y_profile_buffer = ci % y_profile->n_raw_y_profile_buffer;
  if (y_profile->n_y_window_buffer !=0 ) y_profile->c_y_window_buffer = ci % y_profile->n_y_window_buffer;
  if (y_profile->n_windowed_y_profile_buffer !=0 ) y_profile->c_windowed_y_profile_buffer = ci % y_profile->n_windowed_y_profile_buffer;
  if (y_profile->n_y_profile_autoconv_buffer !=0 ) y_profile->c_y_profile_autoconv_buffer = ci % y_profile->n_y_profile_autoconv_buffer;
  if (y_profile->n_y_profile_autoconv_roi_buffer !=0 ) y_profile->c_y_profile_autoconv_roi_buffer = ci % y_profile->n_y_profile_autoconv_roi_buffer;
  return 0;
}

//allocates the fields that are always outputs
//none of them being linked to pre-existing array
int sdi_v2_allocate_sdi_v2_y_profile_output_t_fields(sdi_v2_y_profile *y_profile, int size)
{
  if (y_profile == NULL) return 1;  
  sdi_v2_allocate_sdi_v2_y_profile_1d_float_fields(y_profile, size, PTR_Y_PROFILE_Y_T);
  y_profile->n_output_t = size;
  y_profile->c_output_t = 0;
  return 0;
}

//realloc forbidden when y_profile field allocated with externel ptr
int sdi_v2_allocate_sdi_v2_y_profile_1d_float_fields(sdi_v2_y_profile *y_profile, int size, int which_data)
{
  if (y_profile == NULL) return 1;  
  switch (which_data) {

    case PTR_Y_PROFILE_Y_T :
    if (y_profile->y_t == NULL) y_profile->y_t = (float *)calloc(size, sizeof(float));
    else y_profile->y_t = (float *)realloc(y_profile->y_t, size * sizeof(float));
    break;
    //to be done : imax, quality_factor
  }
  return 0;
}
//this allocates the buffer of 2d curves
//please be careful with that when the 1d fields has been set up by linking
//with existing tab (as when it is plugged to track_util bead object fields which are array, not dynamic allocation)
//if the realloc requires to move the array then there will be an error when the trakc_util.c will use that array
int sdi_v2_allocate_sdi_v2_y_profile_2d_float_fields(sdi_v2_y_profile *y_profile, sdi_v2_parameter *param, int buffer_size, int which_data)
{
  int j;
  if (y_profile == NULL) return 1;
  switch (which_data) {

    /////////////////////real-space profiles
    case PTR_Y_PROFILE_Y_PROFILE :
    for (j=buffer_size ; j<y_profile->n_windowed_y_profile_buffer ; j++)
    {
      free(y_profile->windowed_y_profile[j]);
      y_profile->windowed_y_profile[j]=NULL;
    }
    if (y_profile->windowed_y_profile == NULL) y_profile->windowed_y_profile = (float **)calloc(buffer_size, sizeof(float *));
    else y_profile->windowed_y_profile = (float **)realloc(y_profile->windowed_y_profile, buffer_size * sizeof(float *));
    for (j=0 ;j<buffer_size ;j++)
    {
      if ( j < y_profile->n_windowed_y_profile_buffer)
      {
        y_profile->windowed_y_profile[j] = (float *) realloc(y_profile->windowed_y_profile[j], param->y_roi_size_Y * sizeof(float));
      }
      else y_profile->windowed_y_profile[j] = (float *)calloc(param->y_roi_size_Y, sizeof(float));
    }
    y_profile->c_windowed_y_profile_buffer = 0;
    y_profile->n_windowed_y_profile_buffer = buffer_size;
    break;

    case PTR_Y_PROFILE_RAW_Y_PROFILE :
    for (j=buffer_size ; j<y_profile->n_raw_y_profile_buffer ; j++)
    {
      free(y_profile->raw_y_profile[j]);
      y_profile->raw_y_profile[j]=NULL;
    }
    if (y_profile->raw_y_profile == NULL) y_profile->raw_y_profile = (float **)calloc(buffer_size, sizeof(float *));
    else y_profile->raw_y_profile = (float **)realloc(y_profile->raw_y_profile, buffer_size * sizeof(float *));
    for (j=0 ;j<buffer_size ;j++)
    {
      if ( j < y_profile->n_raw_y_profile_buffer)
      {
        y_profile->raw_y_profile[j] = (float *) realloc(y_profile->raw_y_profile[j], param->y_roi_size_Y * sizeof(float));
      }
      else y_profile->raw_y_profile[j] = (float *)calloc(param->y_roi_size_Y, sizeof(float));
    }
    y_profile->c_raw_y_profile_buffer = 0;
    y_profile->n_raw_y_profile_buffer = buffer_size;
    break;

    case PTR_Y_PROFILE_Y_WINDOW :
    for (j=buffer_size ; j<y_profile->n_y_window_buffer ; j++)
    {
      free(y_profile->y_window[j]);
      y_profile->y_window[j]=NULL;
    }
    if (y_profile->y_window == NULL) y_profile->y_window = (float **)calloc(buffer_size, sizeof(float *));
    else y_profile->y_window = (float **)realloc(y_profile->y_window, buffer_size * sizeof(float *));
    for (j=0 ;j<buffer_size ;j++)
    {
      if ( j < y_profile->n_y_window_buffer)
      {
        y_profile->y_window[j] = (float *) realloc(y_profile->y_window[j], param->y_roi_size_Y * sizeof(float));
      }
      else y_profile->y_window[j] = (float *)calloc(param->y_roi_size_Y, sizeof(float));
    }
    y_profile->c_y_window_buffer = 0;
    y_profile->n_y_window_buffer = buffer_size;
    break;


    ///fourier space
    case PTR_Y_PROFILE_Y_AUTOCONV :
    for (j=buffer_size ; j<y_profile->n_y_profile_autoconv_buffer ; j++)
    {
      free(y_profile->y_profile_autoconv[j]);
      y_profile->y_profile_autoconv[j]=NULL;
    }
    if (y_profile->y_profile_autoconv == NULL) y_profile->y_profile_autoconv = (float **)calloc(buffer_size, sizeof(float *));
    else y_profile->y_profile_autoconv = (float **)realloc(y_profile->y_profile_autoconv, buffer_size * sizeof(float *));
    for (j=0 ;j<buffer_size ;j++)
    {
      if ( j < y_profile->n_y_profile_autoconv_buffer)
      {
        y_profile->y_profile_autoconv[j] = (float *) realloc(y_profile->y_profile_autoconv[j], param->y_roi_size_Y * sizeof(float));
      }
      else y_profile->y_profile_autoconv[j] = (float *)calloc(param->y_roi_size_Y, sizeof(float));
    }
    y_profile->c_y_profile_autoconv_buffer = 0;
    y_profile->n_y_profile_autoconv_buffer = buffer_size;
    break;

    case PTR_Y_PROFILE_Y_AUTOCONV_ROI :
    for (j=buffer_size ; j<y_profile->n_y_profile_autoconv_roi_buffer ; j++)
    {
      free(y_profile->y_profile_autoconv_roi[j]);
      y_profile->y_profile_autoconv_roi[j]=NULL;
    }
    if (y_profile->y_profile_autoconv_roi == NULL) y_profile->y_profile_autoconv_roi = (float **)calloc(buffer_size, sizeof(float *));
    else y_profile->y_profile_autoconv_roi = (float **)realloc(y_profile->y_profile_autoconv_roi, buffer_size * sizeof(float *));
    for (j=0 ;j<buffer_size ;j++)
    {
      if ( j < y_profile->n_y_profile_autoconv_roi_buffer)
      {
        y_profile->y_profile_autoconv_roi[j] = (float *) realloc(y_profile->y_profile_autoconv_roi[j], Y_PROFILE_AUTOCONV_ROI_SIZE * sizeof(float));
      }
      else y_profile->y_profile_autoconv_roi[j] = (float *)calloc(Y_PROFILE_AUTOCONV_ROI_SIZE, sizeof(float));
    }
    y_profile->c_y_profile_autoconv_roi_buffer = 0;
    y_profile->n_y_profile_autoconv_roi_buffer = buffer_size;
    break;

  }
  return 0;
}
/*
 int display_one_y_profile_data(sdi_v2_y_profile *y_profile)
 {
    if (y_profile==NULL) return D_O_K;
    int i;
    win_printf_OK("Coordinates of the centrum of the y profile :  x=%d, y=%d \n"
                  "ROI along x %d %d, along y %d %d \n"
                  "Background %f (in CCD units) \n"
                  "Lateral magnification %f"
          , y_profile->xc, y_profile->yc, y_profile->x_roi_min, y_profile->x_roi_max, y_profile->y_roi_min, y_profile->y_roi_max, y_profile->background
          , y_profile->gy);
   win_printf_OK("Memory allocated to the y trajectory %d \n"
                "Memory allocated to the profile along y : %d \n"
                 "Memory allocated to the raw profile along y (before -bgd and *windowd) : %d \n"
                 "Memory allocated to the window : %d \n"
                 "Memory allocated to the autoconvolution : %d \n"
                 "Memory allocated to the roi of the autoconvolution : %d \n"
        ,((y_profile->y_t==NULL) ? 0 : y_profile->y_t->mx)
       ,((y_profile->windowed_y_profile==NULL) ? 0 : y_profile->windowed_y_profile->mx),((y_profile->raw_y_profile==NULL) ? 0 : y_profile->raw_y_profile->mx)
       ,((y_profile->y_window==NULL) ? 0 : y_profile->y_window->mx), ((y_profile->y_profile_autoconv==NULL) ? 0 : y_profile->y_profile_autoconv->mx)
       ,((y_profile->y_profile_autoconv_roi==NULL) ? 0 : y_profile->y_profile_autoconv_roi->mx));

  win_printf_OK("%d proc", y_profile->n_proc);
  for (i=0 ;i<y_profile->n_proc; i++)
  {
     win_printf_OK("this one %d uses %d points", i, y_profile->proc_results[i]->nx);
  }
  return 0;
 }
*/

///////////////////////////////////////////////////////////////////////////////:
////////////algorithm of tracking itself
int sdi_v2_compute_center_by_autoconv_of_sdi_v2_y_profile(sdi_v2_y_profile *y_profile, sdi_v2_parameter *param)
{
  float center=0;
  if (y_profile == NULL) return 1;
  sdi_v2_compute_center_of_autoconv_of_y_profile( y_profile, param, &center);
  if (y_profile->n_output_t == 0)
  {
    if (y_profile->y_t == NULL) y_profile->y_t = (float *)calloc(1, sizeof( float));
    y_profile->c_output_t = 0;
  }
  y_profile->y_t[y_profile->c_output_t] = center;
  if (y_profile->n_windowed_y_profile_buffer == 0)
  {
    free(y_profile->windowed_y_profile[0]);
    y_profile->windowed_y_profile[0] = NULL;
  }
  return 0;
}

int sdi_v2_compute_center_of_autoconv_of_y_profile(sdi_v2_y_profile *y_profile, sdi_v2_parameter *param, float *center)
{
  int autoconv_x_i_max;
  float autoconv_y_max;
  if (y_profile == NULL) return 1;
  if (y_profile->n_y_profile_autoconv_buffer == 0)
  {
    y_profile->c_y_profile_autoconv_buffer = 0;
    if (y_profile->y_profile_autoconv == NULL) y_profile->y_profile_autoconv = (float **)calloc(1, sizeof(float *));
    y_profile->y_profile_autoconv[0] = (float *)calloc(param->y_roi_size_Y, sizeof(float));
  }
  sdi_v2_array_fft_then_autoconv_then_ifft_then_ifftshift(y_profile->windowed_y_profile[y_profile->c_windowed_y_profile_buffer],
                                            param->fp_y, y_profile->y_profile_autoconv[y_profile->c_y_profile_autoconv_buffer]);
  //une secu sur pos du max ferait pas de aml ici?
  sdi_v2_array_max(y_profile->y_profile_autoconv[y_profile->c_y_profile_autoconv_buffer], 0, param->y_roi_size_Y, &autoconv_y_max, &autoconv_x_i_max);
  sdi_v2_find_center_by_polynomial_3(y_profile, param, center, autoconv_x_i_max - (int)(Y_PROFILE_AUTOCONV_ROI_SIZE/2) , Y_PROFILE_AUTOCONV_ROI_SIZE);

  //before eventual translation in µm we can recenter the y profile object
  if (y_profile->recenter_along_y == true)
  {
    y_profile->y_offset_pixel = (int) (*center / 2);
    y_profile->yc += (float) y_profile->y_offset_pixel ;
    sdi_v2_y_profile_update_parameter(y_profile, param);
    y_profile->recenter_along_y = false;
  }

  *center = *center / 2 * param->pixel_size / param->gy ;//translation of pixel to µm + factor 1/2 of autocon
  //during real experiments gy=pix_size=1 always to save in px in trk file because calib fac in config file of microscope(092016)
  if (y_profile->n_y_profile_autoconv_buffer == 0)
  {
    free(y_profile->y_profile_autoconv[0]);
    y_profile->y_profile_autoconv[0] = NULL;
  }
  return 0;
}

int sdi_v2_find_center_by_polynomial_3(sdi_v2_y_profile *y_profile, sdi_v2_parameter *param, float *center, int x_i_min, int roi_interp_size)
{
  int i;
  float autoconv_y_max;
  if (y_profile == NULL) return 1;
  if (y_profile->n_y_profile_autoconv_roi_buffer == 0)
  {
    y_profile->c_y_profile_autoconv_roi_buffer = 0;
    if (y_profile->y_profile_autoconv_roi == NULL) y_profile->y_profile_autoconv_roi = (float **)calloc(1, sizeof(float *));
    y_profile->y_profile_autoconv_roi[0] = (float *)calloc(roi_interp_size, sizeof(float *));
  }
  for (i=0;i<roi_interp_size;i++)  y_profile->y_profile_autoconv_roi[y_profile->c_y_profile_autoconv_roi_buffer][i] =
                          y_profile->y_profile_autoconv[y_profile->c_y_profile_autoconv_buffer][i+x_i_min];

  static d_s *tmp=NULL;
  if (tmp == NULL) tmp = build_adjust_data_set(tmp, roi_interp_size, roi_interp_size);
  for (i=0 ; i<roi_interp_size ;i++)
  {
    tmp->xd[i] = x_i_min - param->y_roi_size_2_Y + i; //better to fit on 0-centered axis
    tmp->yd[i] = y_profile->y_profile_autoconv_roi[y_profile->c_y_profile_autoconv_roi_buffer][i];
  }
  sdi_v2_ds_max_interpolation_polynom_3( tmp, &autoconv_y_max, center);
  if(y_profile->n_y_profile_autoconv_roi_buffer == 0)
  {
    free(y_profile->y_profile_autoconv_roi[0]);
    y_profile->y_profile_autoconv_roi[0] = NULL;
  }
  free_data_set(tmp);
  tmp = NULL;
  return 0;
}

//the buffer of raw_y_profile for fringe must have been previously allocated
//except if you do not want to keep a buffer of this quantity
int sdi_v2_extract_sdi_y_profile_raw_y_profile_from_image(O_i *oi, sdi_v2_y_profile *y_profile, sdi_v2_parameter *param)
{
  if (y_profile == NULL) return 1;
  //in case of nothing is allocated then all on the fly
  if (y_profile->n_raw_y_profile_buffer == 0)
  {
    y_profile->c_raw_y_profile_buffer = 0;
    if (y_profile->raw_y_profile == NULL) y_profile->raw_y_profile = (float **)calloc(1, sizeof(float *));
    y_profile->raw_y_profile[0] = (float *)calloc(param->y_roi_size_Y, sizeof(float));
  }
  sdi_v2_extract_y_profile_array_from_image(oi, y_profile->raw_y_profile[y_profile->c_raw_y_profile_buffer],
                                            y_profile->x_roi_min, y_profile->x_roi_max, y_profile->y_roi_min, y_profile->y_roi_max);
  //to do some stuff on raw profile before it is erased (when no buffer asked)
  if (y_profile->need_background == true) //one feature to substrat background by removing min of histogram
  {
    d_s *histo = NULL;
    histo = sdi_v2_compute_histo_of_tab(y_profile->raw_y_profile[y_profile->c_raw_y_profile_buffer], param->y_roi_size_Y);
    float min_histo=0;
    int x_i=0;
    sdi_v2_array_min(histo->xd, 0, histo->nx, &min_histo, &x_i);
    y_profile->background = min_histo;
    y_profile->need_background = false;
    free_data_set(histo);
    histo = NULL;
  }
  return 0;
}

//signal processing in real-space
int sdi_v2_process_y_profile_y_profile( sdi_v2_y_profile *y_profile, sdi_v2_parameter *param)
{
  int max_i, j;
  float max;

  if (y_profile == NULL) return 1;
  if (y_profile->n_y_window_buffer == 0)
  {
    y_profile->c_y_window_buffer = 0;
    if (y_profile->y_window == NULL) y_profile->y_window = (float **)calloc(1, sizeof(float *));
    y_profile->y_window[0] = (float *)calloc( param->y_roi_size_Y, sizeof(float));
  }

  //mettre une alarme sur position du max
  sdi_v2_array_max( y_profile->raw_y_profile[y_profile->c_raw_y_profile_buffer], 0, param->y_roi_size_Y, &max, &max_i);
  sdi_v2_generate_hypergaussian_array( y_profile->y_window[y_profile->c_y_window_buffer], param->y_roi_size_Y, max_i - param->y_window_fwhm,
                                                          max_i + param->y_window_fwhm, param->y_window_order);


  if (y_profile->n_windowed_y_profile_buffer == 0)
  {
    y_profile->c_windowed_y_profile_buffer = 0;
    if (y_profile->windowed_y_profile == NULL) y_profile->windowed_y_profile = (float **)calloc(1, sizeof( float *));
    y_profile->windowed_y_profile[y_profile->c_windowed_y_profile_buffer] = (float *)calloc( param->y_roi_size_Y, sizeof(float));
  }

  //fills the current windowed profile
  for (j=0; j<param->y_roi_size_Y; j++)
  {
    y_profile->windowed_y_profile[y_profile->c_windowed_y_profile_buffer][j] =
      (y_profile->raw_y_profile[y_profile->c_raw_y_profile_buffer][j]-y_profile->background) * y_profile->y_window[y_profile->c_y_window_buffer][j] ;
  }

  //if wee keep the window in memory, it is useful to plot it with the same scale as the signal..
  if (y_profile->n_y_window_buffer != 0)  sdi_v2_offset_then_scale(y_profile->y_window[y_profile->c_y_window_buffer], param->y_roi_size_Y, 0, max);
  else
  {
    free(y_profile->y_window[0]);
    y_profile->y_window[0] = NULL;
  }

  if (y_profile->n_raw_y_profile_buffer == 0)
  {
    free(y_profile->raw_y_profile[0]);
    y_profile->raw_y_profile[0] = NULL;
  }

  return 0;
}


//as for fringes :
//switch returns ptr onto the buffer profiles and also as an option can be added to use existing ptr to link
int sdi_v2_switch_ptr_to_sdi_y_profile_2d_float_field(sdi_v2_y_profile *y_profile, int which_data, float **field, int *n_field, int *c_field,
                                                                                          float **ext_float, int n_ext_float)
{
  int i;
  if (y_profile == NULL) return 1;
  switch(which_data){

    case PTR_Y_PROFILE_Y_PROFILE :
    if (y_profile->windowed_y_profile == NULL)
    {
      if (n_ext_float != 0)
      {
        y_profile->n_windowed_y_profile_buffer = n_ext_float;
        y_profile->windowed_y_profile = (float **)calloc(n_ext_float, sizeof(float *));
        for ( i=0 ; i<n_ext_float ; i++) y_profile->windowed_y_profile[i] = ext_float[i];
      }
      else
      {
        y_profile->windowed_y_profile = (float **)calloc(1, sizeof(float *));
        y_profile->windowed_y_profile[0] = (float *)calloc(1, sizeof(float));
        y_profile->n_windowed_y_profile_buffer = 0;
      }
      y_profile->c_windowed_y_profile_buffer = 0;
    }
    for (i=0 ; i<y_profile->n_windowed_y_profile_buffer ; i++) field[i] = y_profile->windowed_y_profile[i];
    *n_field = y_profile->n_windowed_y_profile_buffer;
    *c_field = y_profile->c_windowed_y_profile_buffer;
    break;

    case PTR_Y_PROFILE_RAW_Y_PROFILE :
    if (y_profile->raw_y_profile == NULL)
    {
      if (n_ext_float != 0)
      {
        y_profile->n_raw_y_profile_buffer = n_ext_float;
        y_profile->raw_y_profile = (float **)calloc(n_ext_float, sizeof(float *));
        for ( i=0 ; i<n_ext_float ; i++) y_profile->raw_y_profile[i] = ext_float[i];
      }
      else
      {
        y_profile->raw_y_profile = (float **)calloc(1, sizeof(float *));
        y_profile->raw_y_profile[0] = (float *)calloc(1, sizeof(float));
        y_profile->n_raw_y_profile_buffer = 0;
      }
      y_profile->c_raw_y_profile_buffer = 0;
    }
    for (i=0 ; i<y_profile->n_raw_y_profile_buffer ; i++) field[i] = y_profile->raw_y_profile[i];
    *n_field = y_profile->n_raw_y_profile_buffer;
    *c_field = y_profile->c_raw_y_profile_buffer;
    break;

    case PTR_Y_PROFILE_Y_WINDOW :
    if (y_profile->y_window == NULL)
    {
      if (n_ext_float != 0)
      {
        y_profile->n_y_window_buffer = n_ext_float;
        y_profile->y_window = (float **)calloc(n_ext_float, sizeof(float *));
        for ( i=0 ; i<n_ext_float ; i++) y_profile->y_window[i] = ext_float[i];
      }
      else
      {
        y_profile->y_window = (float **)calloc(1, sizeof(float *));
        y_profile->y_window[0] = (float *)calloc(1, sizeof(float));
        y_profile->n_y_window_buffer = 0;
      }
      y_profile->c_y_window_buffer = 0;
    }
    for (i=0 ; i<y_profile->n_y_window_buffer ; i++) field[i] = y_profile->y_window[i];
    *n_field = y_profile->n_y_window_buffer;
    *c_field = y_profile->c_y_window_buffer;
    break;

    case PTR_Y_PROFILE_Y_AUTOCONV :
    if (y_profile->y_profile_autoconv == NULL)
    {
      if (n_ext_float != 0)
      {
        y_profile->n_y_profile_autoconv_buffer = n_ext_float;
        y_profile->y_profile_autoconv = (float **)calloc(n_ext_float, sizeof(float *));
        for ( i=0 ; i<n_ext_float ; i++) y_profile->y_profile_autoconv[i] = ext_float[i];
      }
      else
      {
        y_profile->y_profile_autoconv = (float **)calloc(1, sizeof(float *));
        y_profile->y_profile_autoconv[0] = (float *)calloc(1, sizeof(float));
        y_profile->n_y_profile_autoconv_buffer = 0;
      }
      y_profile->c_y_profile_autoconv_buffer = 0;
    }
    for (i=0 ; i<y_profile->n_y_profile_autoconv_buffer ; i++) field[i] = y_profile->y_profile_autoconv[i];
    *n_field = y_profile->n_y_profile_autoconv_buffer;
    *c_field = y_profile->c_y_profile_autoconv_buffer;
    break;

    case PTR_Y_PROFILE_Y_AUTOCONV_ROI :
    if (y_profile->y_profile_autoconv_roi == NULL)
    {
      if (n_ext_float != 0)
      {
        y_profile->n_y_profile_autoconv_roi_buffer = n_ext_float;
        y_profile->y_profile_autoconv_roi = (float **)calloc(n_ext_float, sizeof(float *));
        for ( i=0 ; i<n_ext_float ; i++) y_profile->y_profile_autoconv_roi[i] = ext_float[i];
      }
      else
      {
        y_profile->y_profile_autoconv_roi = (float **)calloc(1, sizeof(float *));
        y_profile->y_profile_autoconv_roi[0] = (float *)calloc(1, sizeof(float));
        y_profile->n_y_profile_autoconv_roi_buffer = 0;
      }
      y_profile->c_y_profile_autoconv_roi_buffer = 0;
    }
    for (i=0 ; i<y_profile->n_y_profile_autoconv_roi_buffer ; i++) field[i] = y_profile->y_profile_autoconv_roi[i];
    *n_field = y_profile->n_y_profile_autoconv_roi_buffer;
    *c_field = y_profile->c_y_profile_autoconv_roi_buffer;
    break;

  }
  return 0;
}


//removes one pattern of y_profile from the y_profile list of general
//to be carefully rewritten for trackbead implmentation (not erase tab)
int sdi_v2_remove_one_sdi_y_profile(sdi_v2_y_profile *y_profile)
{
  if (y_profile == NULL) return D_O_K;

  free(y_profile->y_t);
  y_profile->y_t = NULL;

  int i;
  for (i=0;i<y_profile->n_windowed_y_profile_buffer;i++)
  {
    free(y_profile->windowed_y_profile[i]);
    y_profile->windowed_y_profile[i] = NULL;
  }
  y_profile->windowed_y_profile = NULL;
  for (i=0;i<y_profile->n_raw_y_profile_buffer;i++)
  {
    free(y_profile->raw_y_profile[i]);
    y_profile->raw_y_profile[i] = NULL;
  }
  y_profile->raw_y_profile = NULL;
  for (i=0;i<y_profile->n_y_window_buffer;i++)
  {
    free(y_profile->y_window[i]);
    y_profile->y_window[i] = NULL;
  }
  y_profile->y_window = NULL;
  for (i=0;i<y_profile->n_y_profile_autoconv_buffer;i++)
  {
    free(y_profile->y_profile_autoconv[i]);
    y_profile->y_profile_autoconv[i] = NULL;
  }
  y_profile->y_profile_autoconv = NULL;
  for (i=0;i<y_profile->n_y_profile_autoconv_roi_buffer;i++)
  {
    free(y_profile->y_profile_autoconv_roi[i]);
    y_profile->y_profile_autoconv_roi[i] = NULL;
  }
  y_profile->y_profile_autoconv_roi = NULL;

  free_data_set(y_profile->ds_out1);
  y_profile->ds_out1 = NULL;
  free(y_profile);
  y_profile = NULL;
  return 0;
}

int sdi_v2_reset_all_intermediate_data_of_y_profile(sdi_v2_y_profile *profile)
{
  if (profile == NULL) return 1;  
  sdi_v2_reset_data_of_y_profile(profile, PTR_Y_PROFILE_Y_T);
  sdi_v2_reset_data_of_y_profile(profile, PTR_Y_PROFILE_RAW_Y_PROFILE);
  sdi_v2_reset_data_of_y_profile(profile, PTR_Y_PROFILE_Y_PROFILE);
  sdi_v2_reset_data_of_y_profile(profile, PTR_Y_PROFILE_Y_WINDOW);
  sdi_v2_reset_data_of_y_profile(profile, PTR_Y_PROFILE_Y_AUTOCONV);
  sdi_v2_reset_data_of_y_profile(profile, PTR_Y_PROFILE_Y_AUTOCONV_ROI);
  return D_O_K;
}

//resets everything, buffer, go back to non-saving version of data treatment
int sdi_v2_reset_data_of_y_profile(sdi_v2_y_profile *profile, int which_data)
{
  int i;
  if (profile == NULL) return 1;  
  switch (which_data) {
    case PTR_Y_PROFILE_Y_T:
    free(profile->y_t);
    profile->y_t = NULL;
    profile->n_output_t = 0;
    profile->c_output_t = 0;
    break;
    case PTR_Y_PROFILE_RAW_Y_PROFILE:
    for (i=0;i<profile->n_raw_y_profile_buffer;i++)
    {
      free(profile->raw_y_profile[i]);
      profile->raw_y_profile[i] = NULL;
    }
    profile->raw_y_profile = NULL;
    profile->n_raw_y_profile_buffer = 0;
    profile->c_raw_y_profile_buffer = 0;
    break;
    case PTR_Y_PROFILE_Y_PROFILE:
    for (i=0;i<profile->n_windowed_y_profile_buffer;i++)
    {
      free(profile->windowed_y_profile[i]);
      profile->windowed_y_profile[i] = NULL;
    }
    profile->windowed_y_profile = NULL;
    profile->n_windowed_y_profile_buffer = 0;
    profile->c_windowed_y_profile_buffer = 0;
    break;
    case PTR_Y_PROFILE_Y_WINDOW:
    for (i=0;i<profile->n_y_window_buffer;i++)
    {
      free(profile->y_window[i]);
      profile->y_window[i] = NULL;
    }
    profile->y_window = NULL;
    profile->n_y_window_buffer = 0;
    profile->c_y_window_buffer = 0;
    break;
    case PTR_Y_PROFILE_Y_AUTOCONV:
    for (i=0;i<profile->n_y_profile_autoconv_buffer;i++)
    {
      free(profile->y_profile_autoconv[i]);
      profile->y_profile_autoconv[i] = NULL;
    }
    profile->y_profile_autoconv = NULL;
    profile->n_y_profile_autoconv_buffer = 0;
    profile->c_y_profile_autoconv_buffer = 0;
    break;
    case PTR_Y_PROFILE_Y_AUTOCONV_ROI:
    for (i=0;i<profile->n_y_profile_autoconv_roi_buffer;i++)
    {
      free(profile->y_profile_autoconv_roi[i]);
      profile->y_profile_autoconv_roi[i] = NULL;
    }
    profile->y_profile_autoconv_roi = NULL;
    profile->n_y_profile_autoconv_roi_buffer = 0;
    profile->c_y_profile_autoconv_roi_buffer = 0;
    break;
  }

  return 0;
}

//list of functions of the user interface for y_profile object
int sdi_v2_init_one_sdi_y_profile_by_position(sdi_v2_y_profile *y_profile, int xc, int yc)
{
  if(y_profile == NULL) y_profile = (sdi_v2_y_profile *)calloc(1,sizeof(sdi_v2_y_profile));

  y_profile->xc=xc;
  y_profile->yc=yc;

  y_profile->background=0;
  y_profile->need_background = false;
  y_profile->recenter_along_y = false;
  y_profile->y_offset_pixel = 0;
  y_profile->recenter_along_x = false;
  y_profile->gy=1;

  y_profile->n_output_t = 0;
  y_profile->c_output_t=0;
  y_profile->y_t = NULL;

  y_profile->raw_y_profile = NULL;
  y_profile->n_raw_y_profile_buffer = 0;
  y_profile->c_raw_y_profile_buffer = 0;
  y_profile->y_window = NULL;
  y_profile->n_y_window_buffer = 0;
  y_profile->c_y_window_buffer = 0;
  y_profile->windowed_y_profile = NULL;
  y_profile->c_windowed_y_profile_buffer = 0;
  y_profile->n_windowed_y_profile_buffer = 0;

  y_profile->y_profile_autoconv = NULL;
  y_profile->c_y_profile_autoconv_buffer = 0;
  y_profile->n_y_profile_autoconv_buffer = 0;
  y_profile->y_profile_autoconv_roi = NULL;
  y_profile->c_y_profile_autoconv_roi_buffer = 0;
  y_profile->n_y_profile_autoconv_roi_buffer = 0;

  y_profile->ds_out1 = NULL;
  return 0;
}

int sdi_v2_y_profile_update_parameter(sdi_v2_y_profile *y_profile, sdi_v2_parameter *param)
{
  if (y_profile == NULL) return 1;
  y_profile->x_roi_min = y_profile->xc - param->x_roi_size_2_Y;
  y_profile->x_roi_max = y_profile->xc + param->x_roi_size_2_Y;
  y_profile->y_roi_min = y_profile->yc - param->y_roi_size_2_Y;
  y_profile->y_roi_max = y_profile->yc + param->y_roi_size_2_Y;
  return 0;
}
