#include "sdi_v2_movie.h"

//allocation of submovie inside
//submovie in the same imr as the selected one
//char images
//extracts a submovie from a movie with a periodic selection
int sdi_v2_extract_char_submovie_from_char_movie(imreg *imr, O_i *submovie, O_i *movie, int period, int offset, int subset_length, int number)
{  // submovie is hanging in the air !
  int i, j, k, frame_to_be_selected, frame_selected_index, nfi, submovie_nx, submovie_ny ;
  if (movie == NULL || imr == NULL) return D_O_K;
  if (movie->im.data_type != IS_CHAR_IMAGE) return D_O_K;
  nfi = movie->im.n_f ;
  submovie_ny =movie->im.ny;
  submovie_nx = movie->im.nx;
  submovie=create_and_attach_movie_to_imr(imr, submovie_nx, submovie_ny, IS_CHAR_IMAGE, number);
  frame_selected_index = 0;
  frame_to_be_selected = offset;
  for (i=0;i<nfi;i++)
  {
    if(i == frame_to_be_selected && frame_selected_index < number)
    {
      switch_frame(submovie, frame_selected_index);
      switch_frame(movie, frame_to_be_selected);
      for(j=0;j<submovie_ny;j++)
      {
        for(k=0;k<submovie_nx;k++)
        {
          submovie->im.pixel[j].ch[k] =  movie->im.pixel[j].ch[k];
        }
      }
      frame_selected_index++;
      frame_to_be_selected =  (int) (frame_selected_index / subset_length) * period + frame_selected_index % subset_length + offset;
    }
  }
  find_zmin_zmax(submovie);
  return 0;
}

int sdi_v2_oi_char_find_isolated_maxima(O_i *oi, int *x_list, int *y_list, int *count, int threshold, int dx, int dy)
{
  static int *x_list_tmp=NULL;
  static int *y_list_tmp=NULL;
  static int n_list_tmp = 0;
  int count_tmp=0;
  int i;
  int tab_size=100000;

  if (oi == NULL || x_list == NULL || y_list == NULL  || count == NULL) return 1;
  if (oi->im.data_type!= IS_CHAR_IMAGE) return D_O_K;


  if (x_list_tmp == NULL || y_list_tmp == NULL || n_list_tmp < tab_size)
    {
      x_list_tmp=(int *)realloc(x_list_tmp,tab_size*sizeof(int));
      y_list_tmp=(int *)realloc(y_list_tmp,tab_size*sizeof(int));
      if (x_list_tmp == NULL || y_list_tmp == NULL) return 2;
      n_list_tmp = tab_size;
    }
  sdi_v2_oi_char_find_all_maxima(oi, x_list_tmp, y_list_tmp, &count_tmp, threshold, dx);
  sdi_v2_filter_close_maxima(x_list_tmp, y_list_tmp, count_tmp, x_list, y_list, count, dx, dy);
  //free(x_list_tmp);   x_list_tmp=NULL;
  //free(y_list_tmp);   y_list_tmp=NULL;
  return 0;
}

int sdi_v2_filter_close_maxima(int *x_list_tmp, int *y_list_tmp, int count_tmp, int *x_list, int *y_list, int *count, int dx, int dy)
{
  if (x_list_tmp==NULL || y_list_tmp==NULL) return D_O_K;
  if (x_list == NULL || y_list == NULL  || count == NULL) return 1;
  int iy, ix, dist_x, dist_y;
  int counted;
  int count_tmp_tmp=1;

  //second step : filter the one whose y value are too close
  x_list[0]=x_list_tmp[0];
  y_list[0]=y_list_tmp[0];
  counted=0;
  for(ix = 0 ; ix < count_tmp ; ix++)
  {
    for(iy = 0 ; iy < count_tmp_tmp ; iy++)
    {
      dist_y = (int) (fabs(y_list_tmp[ix] - y_list[iy]));
      dist_x = (int) (fabs(x_list_tmp[ix] - x_list[iy]));
      if(dist_y < dy && dist_x < dx) counted=1;
    }
    if (counted==0)
    {
      x_list[count_tmp_tmp]=x_list_tmp[ix];
      y_list[count_tmp_tmp]=y_list_tmp[ix];
      count_tmp_tmp++;
    }
    counted=0;
  }
  *count = count_tmp_tmp;
  return 0;
}

//this function looks for maxima in an image
//with jumps in x of dx to accelerate
int sdi_v2_oi_char_find_all_maxima(O_i *oi, int *x_list, int *y_list, int *count, int threshold, int dx)
{
  int iy, ix;
  int count_tmp=0;

  if (oi == NULL || x_list == NULL || y_list == NULL  || count == NULL) return 1;
  if (oi->im.data_type != IS_CHAR_IMAGE) return D_O_K;
  for(iy=0 ; iy < oi->im.ny ; iy++)
  {
    ix = 0;
    while (ix < oi->im.nx)
    {
      if(oi->im.pixel[iy].ch[ix] > threshold)
      {
        x_list[count_tmp]=ix;
        y_list[count_tmp]=iy;
        count_tmp++;
        ix += dx;
      }
      else ix++;
    }
  }
  *count=count_tmp;
  return 0;
}
