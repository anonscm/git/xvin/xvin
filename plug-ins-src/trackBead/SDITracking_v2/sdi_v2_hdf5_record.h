#pragma once

#ifndef XV_WIN32
# include "config.h"
#endif

# include "file_picker_gui.h"

# include "sdi_bead.h"
# include "xvin.h"
# include "hdf5.h"


# define SDI_HDF5_MAX_NAME_LENGTH 512


# define RECORD_Z_T 0
# define RECORD_MOVIE_ROI 1
# define RECORD_Y_T 2
# define RECORD_X_T 3

# define RECORD_Z_COUPLED 0
# define RECORD_CONTRAST 1
# define RECORD_I_MAX 2

//# define IS_BEAD_RECORD 0
//# define IS_FRINGES_RECORD 1
//# define IS_Y_PROFILE_RECORD 2

# define SDI_RECORD_TYPE_FLOAT_1D 0
# define SDI_RECORD_TYPE_MOVIE 1
# define SDI_RECORD_TYPE_INT_1D 2
# define SDI_RECORD_TYPE_UNSIGNED_LONG_1D 3


//the following describes the general architecture to save data in hdf5 files

//en gros comment ca marche

//une phase de setup ou tu ouvres tout et configure ton architecture de données
//puis tu fermes tout (donc faut reallouer les id (qui t'intéresse) en cours de manip)

//ensuite : une liste de dataspace qui eux sont toujours ouverts pour pas les perdre
//et chaque qu'on appelle une sauvegarde disque, on linke toues les objets qu'on vient de reouvrir vers

//tention avec les id : n' plus de sens des qu'on a ferme le truc associe, donc faut les reallouer a chq coup si on a ferme
//le schéma typipque :
//avant enregistrment : set up du record, on peut se permettre de fermer et de perdre le sens des id
//qd on commence le record, on reset tous les id et surtout on ne ferme rien. ensuite reocmmencer ca a chaque ouverture/fermeture du ficher record


//this object describes the mapping of memory between
//data in memory & in record file, as a couple of dataspaces
//the scheme is : 2 dataspaces (extensible for the file one) whose hyperslab are overwritten during time
//the ranks of the dataspaces can be different
typedef struct hdf5_data_map
{
  //when the mem_map is triggered on, by any external controler (user, clock from peripheral/cpu ...)
  //one saving process is launched. Then the next one is prepared
  bool trigger;
  int alloc; //tag for helping with the extension of corresponding datasets

  //memory part of the dataspace pair
  int mem_rank;
  hid_t mem_sp;
  hsize_t *mem_dims;  //dimension of the dataspace
  hsize_t *mem_sp_offset;//the following configures what to take from the data
  hsize_t *mem_sp_select;
  hsize_t *mem_sp_stride;
  hsize_t *mem_sp_block;

  //file part of the dataspace pair
  int file_rank;
  hid_t file_sp;
  int page_size; //increment of memory allocation in file..
  int dim_to_be_incr; //..along this dimension
  int expected_offset;
  hsize_t *file_dims_chunk;//dimensions of the chunk
  hsize_t *file_dims;//real diemnsions of the dataset in file (incremented during the experiment)
  hsize_t *file_sp_offset;
  hsize_t *file_sp_select;

} mem_map;

typedef union union_data
{
  float *float_1d;
  unsigned long *ul_1d;
  int *int_1d;
  unsigned char *uch_1d;
} u_data;

typedef struct data_exp
{
  int datatype;
  u_data *u_d;
} data_e;

//this object describes a link between a float array & its dataset where to be written
typedef struct hdf5_data_link
{
  hid_t d_id; //this needs to be reset after each closure
  hid_t type_id;//datatype
  hsize_t *d_space;  //space fo the dataset in file (to be incremented during time)
  char name[SDI_HDF5_MAX_NAME_LENGTH];
  data_e *d_e;
  int n_mem_map_link; //a dataset can be filled by several mem_map
  mem_map **mem_map_link_list;
} hdf5_data;

//a record object is a group which contains different data attibute & datasets
//to keep simple i stay at one generation set_formated_string
//so sdi_record object may be a finrge, y_profile, bead...
typedef struct sdi_record
{
  char filename[SDI_HDF5_MAX_NAME_LENGTH];
  int index;
  char name[SDI_HDF5_MAX_NAME_LENGTH];
  hid_t gr_id;

  //the float dataset list (z, y, x, results of proc on those, quality factor...)
  int n_hdf5_data;
  hdf5_data **hdf5_data_list;

} sdi_r;

//the core of the architecture :
//each bead has its own group, which contains :
//dataset with data (z(t), y(t), x(t) etc)
//groups of subobjects : fringes1&2, whith their datasets, y_profiles1&2 with datasets etc
typedef struct hdf5_sdi_record
{

  char filename[SDI_HDF5_MAX_NAME_LENGTH];
  hid_t fid;//handle to the record file

  int n_record;
  sdi_r **record_list;

  int n_mem_map;
  mem_map **mem_map_list;

  //this list contains ptr onto all the objects dataspace-dataset-memory data tp be called easily
  int n_hdf5_data_all, c_hdf5_data_all;
  hdf5_data **hdf5_data_all_list;

  //temporary
  int period_min; //temporary, to be remove
  int data_saving_period;

} hdf5_sdi_r;

PXV_FUNC(int, init_one_bead_record_config_1, (hdf5_sdi_r *g_record,  sdi_bead *bead, int index, mem_map *mem_map_i));
PXV_FUNC(int, handle_one_write_in_g_record, (hdf5_sdi_r *g_record));
PXV_FUNC(int, attach_ulong_1d_to_hdf5_data, (hdf5_data *hdf5_d, unsigned long *ul_1d));
PXV_FUNC(int, attach_float_1d_to_hdf5_data, (hdf5_data *hdf5_d, float *fl_1d));
PXV_FUNC(int, attach_oi_to_hdf5_data, (hdf5_data *hdf5_d, O_i *oi));
PXV_FUNC(int, attach_y_profile_data_to_hdf5_data, (hdf5_data *hdf5_d, sdi_y_profile *y_profile, int which_data));
PXV_FUNC(int, attach_fringes_data_to_hdf5_data, (hdf5_data *hdf5_d, sdi_fringes *fringes, int which_data));
PXV_FUNC(int, attach_bead_data_to_hdf5_data, (hdf5_data *hdf5_d, sdi_bead *bead, int which_data));
PXV_FUNC(int, add_one_mem_map_link_to_hdf5_data, (hdf5_data *hdf5_d, mem_map *mem_map_i));
PXV_FUNC(int, add_one_hdf5_data_to_record, (hdf5_sdi_r *g_record, int record_index, mem_map *mem_map_i, int datatype, const char *name));

PXV_FUNC(int, record_bead_1d_float_basic_links, (hdf5_sdi_r *g_record, sdi_bead *bead, int index, mem_map * mem_map_i));
PXV_FUNC(int, bead_record_config_1, (hdf5_sdi_r *g_record, sdi_bead *bead, int index));
PXV_FUNC(int, attach_fringes_attributes_to_record_1, (hdf5_sdi_r *g_record, sdi_fringes *fringes, const char *name));
PXV_FUNC(int, find_one_record_by_name, (hdf5_sdi_r *g_record, const char *name, sdi_r **record, int *position, int *found));
PXV_FUNC(int, attach_one_attribute_to_one_record, (sdi_r *record, char *name, int size, data_e *d_e));
PXV_FUNC(int, reload_hdf5_data_all_list, (hdf5_sdi_r *g_record));
PXV_FUNC(int, add_one_mem_map_to_hdf5_sdi_record, (hdf5_sdi_r *g_record, int mem_rank, int *mem_dims, int file_rank, int *file_dims_chunk, int page_size, int dim_to_be_incr));

PXV_FUNC(int, init_mem_map, (mem_map *mem_map_i, int mem_rank, int *mem_dims, int file_rank, int *file_dims_chunk, int page_size, int dim_to_be_incr));
PXV_FUNC(int, config_hyperslabs_of_mem_map, (mem_map *mem_map_i, int *mem_offset, int *mem_select, int *mem_stride, int *mem_block, int *file_offset, int *file_select));
PXV_FUNC(int, update_hyperslab_of_mem_map, (mem_map *mem_map_in, int mem_offset, int file_offset));
PXV_FUNC(int, append_pages, (mem_map **mem_map_list, int n_mem_map, hdf5_data **hdf5_data_list, int n_hdf5_data));

PXV_FUNC(int, init_hdf5_sdi_record, (hdf5_sdi_r *sdi_g_record, char *filename));
PXV_FUNC(int, alloc_one_record_in_hdf5_sdi_record, (hdf5_sdi_r *sdi_g_record));
PXV_FUNC(int, config_one_record_in_sdi_record, (hdf5_sdi_r *sdi_g_record, const char *name));

PXV_FUNC(int, test_hdf5_model, (void));
