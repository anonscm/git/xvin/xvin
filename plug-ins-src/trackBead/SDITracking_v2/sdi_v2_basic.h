#pragma once
//# include "../fft2d/fft2d.h"
# include "../../fft2d/fftbtl32n.h"
# include "xvin.h"
#include "float.h"
PXV_FUNC(int, sdi_v2_bin_size_from_Scott, (const float *y, int n, float *mean, float *bin_size));
PXV_FUNC(d_s *, sdi_v2_compute_histo_of_tab, (const float *y_in, int n_y_in));//this is tmp
PXV_FUNC(int, sdi_v2_quick_contrast, (float *sig, int n_points, int delta_x_i, float *contrast, float *I_max));
PXV_FUNC(int, sdi_v2_find_correct_2_pi_multiple, (float *phi0_1, float phi1_1, float *phi0_0, float *phi1_0, float *k_in, int n_prev, float v_0, float v_1, float *k_out));
PXV_FUNC(int, sdi_v2_unwrap, (float *phase,int i_min,int i_max, float tol));
PXV_FUNC(int, sdi_v2_ds_max_interpolation_polynom_3, (d_s *ds, float *ds_y_max, float *ds_x_max));
PXV_FUNC(int, sdi_v2_ds_max_interpolation_polynom_2, (d_s *ds, float *ds_y_max, float *ds_x_max));
PXV_FUNC(int, sdi_v2_raw_spectral_phase_fftbt, ( float *tf_complex, float *phase, int i_min, int i_max));
PXV_FUNC(int, sdi_v2_array_fft_then_autoconv_then_ifft_then_ifftshift, (float *profile, fft_plan *fp, float *auto_conv));
PXV_FUNC(int, sdi_v2_array_fftshift_then_fft, (float *profile, fft_plan *fp, float *amplitude, float *phase, float *spectrum));
PXV_FUNC(int, sdi_v2_generate_hypergaussian_array, (float *array, int n_points, float fwhm_min, float fwhm_max, int order));
PXV_FUNC(int, sdi_v2_extract_y_profile_array_from_image, ( O_i *oi, float *profile, int x_i_min, int x_i_max, int y_i_min, int y_i_max));
PXV_FUNC(int, sdi_v2_extract_x_profile_array_from_image, ( O_i *oi, float *profile, int x_i_min, int x_i_max, int y_i_min, int y_i_max));


PXV_FUNC(int, sdi_v2_measure_frequency, (float *x, float *y, int n_points, float *nu0,
                                        int x_i_frac_closest_min, int x_i_frac_closest_max, int dx_i_factor));
PXV_FUNC(int, sdi_v2_estimate_mean, (float *tab, int n, float *mean));
PXV_FUNC(int, sdi_v2_estimate_std, (float *tab, int n, float *std));
PXV_FUNC(int, sdi_v2_array_mu_1, (const float *x, const float *y, int x_roi_min, int x_roi_max, float *mu1));
PXV_FUNC(int, sdi_v2_correlation_product, (const float *x1, const float *x2, int x_roi_min, int x_roi_max, float *corr_product));
PXV_FUNC(int, sdi_v2_normalized_scal_prod, (const float *x1, const float *x2, int x_i_min, int x_i_max, float *prod));
PXV_FUNC(int, sdi_v2_normalize_by_sum, (float *array, int x_roi_min, int x_roi_max, int n_points));
PXV_FUNC(int, sdi_v2_normalize_by_norm2, (float *y, int x_roi_min, int x_roi_max, int n_points));
PXV_FUNC(int, sdi_v2_cumulative_sum, (const float *tab, int x_roi_min, int x_roi_max, float *sum));
PXV_FUNC(int, sdi_v2_cumulative_square_sum, (const float *tab, int x_roi_min, int x_roi_max, float *sum));
PXV_FUNC(int, sdi_v2_peak_width, (const float *x, const float *y, int x_roi_min, int x_roi_max, float frac,
                float *x_frac_min, int *x_i_frac_min,int *x_i_frac_closest_min, float *x_frac_max,int *x_i_frac_max,int *x_i_frac_closest_max,float *width));
PXV_FUNC(int, sdi_v2_offset_then_scale, (float *tab,int tab_size,float offset,float scale));
PXV_FUNC(int, sdi_v2_array_max, (const float *array, int roi_min, int roi_max, float *y_max, int *x_max));
PXV_FUNC(int, sdi_v2_array_min, (const float *array, int roi_min, int roi_max, float *y_min, int *x_min));
PXV_FUNC(int, sdi_v2_fftshift, (float *tab, int size_2));
PXV_FUNC(int, sdi_v2_array_log, (float *data, int n_points));
PXV_FUNC(int, sdi_v2_link_arrays_to_ds, (d_s *ds, float *x, float *y, int n_points));
PXV_FUNC(d_s *, sdi_v2_init_empty_ds, (void));
