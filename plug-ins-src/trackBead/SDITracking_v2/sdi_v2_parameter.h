#pragma once

#ifndef XV_WIN32
# include "config.h"
#endif

# include "../../fft2d/fftbtl32n.h"
# include "xvin.h"

//this contains the general parameters for diagnostoc of the raw tracking signal
typedef struct sdi_v2_tracking_diagnose
{
  //error handling : to diagnose whether the bead is good quality or not
  float contrast_min_limit, I_min_limit, I_max_limit; //on the raw signal examines the fringes quality
  float z_min_limit, z_max_limit, y_min_limit, y_max_limit, x_min_limit, x_max_limit; //ultimate test of the result
  int weird_count_limit;

  //parameters for diagnostic of the function of the bead : ref bead/opening hairpin etc
  float std_threshold_x, std_threshold_y, std_threshold_z; //thershold in fluctuation x/y for immobile beads

  //float corr_threshold_z; //threshold in correlation between z & something (zmag for example) obsolete
  float z_expected_range;//range of z when opening hp (in raw unt for now)
  float z_range_tol;

  //parameters for automatic detection of beads
} sdi_v2_diagnose;

typedef struct sdi_v2_general_parameter
{
  float index_factor; // real object space index to handle with immersion objective & object in solution
  float gy; //magnification of the imaging device (22.222 for 40X olympus objective with 1X 4f line and 100mm tube lens)
  float pixel_size; //in microns

  //parameters for x,z
  int x_roi_size,y_roi_size;//size of subset for x,z measurement caomputations
  int x_roi_size_2,y_roi_size_2;//half size of subset for x,z measurement compatutaions
  int dist_half_beads;//the distance, in pixels, between two patterns of fringes of one bead
  int x_window_order, x_window_fwhm;//order and fwhm of the hypergaussian filter for the x,z computations
  fft_plan *fp_x;//the fft plan along x, the axis along the fringes
  float *nu_x; //the frequency axis of fftplan (in pix**-1)
  float nu_threshold;//the threshold of the peak of the spectrum determining the interval of linear fitting
  int dc_width;//the width of DC component to be removed for x,z computations

  //parameters for y
  int x_roi_size_Y, x_roi_size_2_Y;
  int y_roi_size_Y, y_roi_size_2_Y;
  int y_window_order, y_window_fwhm;
  fft_plan *fp_y;
  float *nu_y;

} sdi_v2_parameter;

//to be used by track_util, record & trackbead
extern sdi_v2_parameter *sdi_g_param;
extern sdi_v2_diagnose *sdi_g_diagnose;
extern int *sdi_colormap;

PXV_FUNC(int, set_sdi_v2_diagnose_gui, (sdi_v2_diagnose *diagnose, bool from_cfg));
PXV_FUNC(int, sdi_v2_set_diagnose, (sdi_v2_diagnose *diagnose, float z_min_limit, float z_max_limit, float y_min_limit, float y_max_limit,
                                  float x_min_limit, float x_max_limit, float I_min_limit, float I_max_limit, float contrast_min_limit,
                                  int weird_count_limit, float std_threshold_x, float std_threshold_y, float std_threshold_z, float z_expected_range, float z_range_tol));

PXV_FUNC(int, set_sdi_v2_parameters_gui, (sdi_v2_parameter *param, bool from_cfg));
PXV_FUNC(int, set_sdi_v2_parameters, (sdi_v2_parameter *param, int x_roi_size, int y_roi_size,int dist_half_beads,
        int x_window_fwhm,float nu_threshold,int dc_width,int x_window_order, int x_roi_size_Y, int y_roi_size_Y,
        int y_window_fwhm, int y_window_order,float gy, float index_factor, float pixel_size));
PXV_FUNC(int, sdi_v2_define_sdi_colormap, (void));
