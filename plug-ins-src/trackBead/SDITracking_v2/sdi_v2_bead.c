# include "sdi_v2_bead.h"

int sdi_v2_diagnose_one_bead(sdi_v2_bead *bead, sdi_v2_diagnose *diagnose)
{
  int i;
  int local_count = 0;
  float q_factor = 1;
  int n_par = 6;

  if(bead == NULL ||diagnose == NULL)  return 1;

  bead->bead_class &= ~SDI_UNINITIALIZED_BEAD;
  for (i = 0; i<bead->n_fringes_bead ;i++)
  {
    if (fabs(bead->fringes_bead[i]->v_0) > 10) bead->bead_class |= SDI_UNINITIALIZED_BEAD;
    if (bead->fringes_bead[i]->n_ref_points == 0) bead->bead_class |= SDI_UNINITIALIZED_BEAD;
    if(bead->fringes_bead[i]->I_max_t[bead->fringes_bead[i]->c_output_t] > diagnose->I_max_limit) local_count++;
    if(bead->fringes_bead[i]->I_max_t[bead->fringes_bead[i]->c_output_t] < diagnose->I_min_limit) local_count++;
    if(bead->fringes_bead[i]->contrast_t[bead->fringes_bead[i]->c_output_t] < diagnose->contrast_min_limit) local_count++;
  }

  //q_factor -= (1 - bead->fringes_bead[0]->contrast_t[bead->fringes_bead[0]->c_output_t])  / n_par;
  //q_factor -= (1 - bead->fringes_bead[1]->contrast_t[bead->fringes_bead[1]->c_output_t]) / n_par;
  //q_factor -= (diagnose->I_max_limit - bead->fringes_bead[0]->I_max_t[bead->fringes_bead[0]->c_output_t])  / n_par / diagnose->I_max_limit;
  //q_factor -= (diagnose->I_max_limit - bead->fringes_bead[1]->I_max_t[bead->fringes_bead[1]->c_output_t]) / n_par / diagnose->I_max_limit;
  //q_factor -= (bead->fringes_bead[0]->background - 20)  / n_par ;
  //q_factor -= (bead->fringes_bead[1]->background - 20) / n_par ;

  if (bead->z_t[bead->c_output_t]!=999 && (bead->z_t[bead->c_output_t]<diagnose->z_min_limit || bead->z_t[bead->c_output_t]>diagnose->z_max_limit))
  {
    q_factor = 0;
    local_count++;
  }
  if (bead->y_t[bead->c_output_t]!=999 && (bead->y_t[bead->c_output_t]<diagnose->y_min_limit || bead->y_t[bead->c_output_t]>diagnose->y_max_limit))
  {
    q_factor = 0;
    local_count++;
  }
  if (bead->x_t[bead->c_output_t]!=999 && (bead->x_t[bead->c_output_t]<diagnose->x_min_limit || bead->x_t[bead->c_output_t]>diagnose->x_max_limit))
  {
    q_factor = 0;
    local_count++;
  }

  if (local_count != 0)
  {
    bead->weird_count++;
    if(bead->weird_count < diagnose->weird_count_limit) bead->bead_class |= SDI_WEIRD_BEAD;
    else
    {
      bead->bead_class |= SDI_BAD_BEAD;
      bead->bead_class &= ~SDI_WORKING_BEAD;
      bead->weird_count = diagnose->weird_count_limit + 5;//to avoid blinking
    }
  }
  else
  {
    bead->weird_count--;
    bead->weird_count = (bead->weird_count < 0) ? 0 : bead->weird_count;
    if (bead->weird_count < diagnose->weird_count_limit)
    {
      bead->bead_class &= ~SDI_BAD_BEAD;
      bead->bead_class &= ~SDI_WEIRD_BEAD;
      bead->bead_class |= SDI_WORKING_BEAD;
    }
  }
  bead->quality_factor_t[bead->c_output_t] = q_factor;

  return 0;
}

//recenter along y a bead, if more than y threshold
int sdi_v2_recenter_y_of_one_bead(sdi_v2_bead *bead, sdi_v2_parameter *param, int y_threshold)
{
  if(bead == NULL ||param == NULL)  return 1;
  if (fabs((bead->y_t[bead->c_output_t] * param->gy / param->pixel_size)) >= y_threshold) sdi_v2_center_y_roi_bead(param, bead);
  return 0;
}

int sdi_v2_dummy_xz_bead(sdi_v2_bead *bead)
{
  static int count=0;
  if(bead == NULL)  return 1;
  count++;
  bead->z_t[bead->c_output_t] = 99999;
  bead->x_t[bead->c_output_t] = 99999;
  bead->quality_factor_t[bead->c_output_t] = 99999;
  return 0;
}

int sdi_v2_dummy_y_bead(sdi_v2_bead *bead)
{
  if(bead == NULL)  return 1;
  bead->y_t[bead->c_output_t] = 99999;
  return 0;
}

//for 2 fringes patterns
int sdi_v2_center_x_roi_bead(sdi_v2_parameter *sdi_param, sdi_v2_bead *bead)
{
  if (bead==NULL || sdi_param == NULL) return D_O_K;
  bead->fringes_bead[0]->xc = (int)( round( (bead->fringes_bead[0]->xc + bead->fringes_bead[1]->xc) / 2) );
  bead->fringes_bead[1]->xc = bead->fringes_bead[0]->xc;
  bead->fringes_bead[0]->x_roi_min = bead->fringes_bead[0]->xc - sdi_param->x_roi_size_2;
  bead->fringes_bead[0]->x_roi_max = bead->fringes_bead[0]->xc + sdi_param->x_roi_size_2;
  bead->fringes_bead[1]->x_roi_min = bead->fringes_bead[1]->xc - sdi_param->x_roi_size_2;
  bead->fringes_bead[1]->x_roi_max = bead->fringes_bead[1]->xc + sdi_param->x_roi_size_2;

  bead->y_profile_bead[0]->xc = bead->y_profile_bead[1]->xc = bead->fringes_bead[0]->xc;
  bead->y_profile_bead[0]->x_roi_min = bead->y_profile_bead[0]->xc - sdi_param->x_roi_size_2_Y;
  bead->y_profile_bead[0]->x_roi_max = bead->y_profile_bead[0]->xc + sdi_param->x_roi_size_2_Y;
  bead->y_profile_bead[1]->x_roi_min = bead->y_profile_bead[1]->xc - sdi_param->x_roi_size_2_Y;
  bead->y_profile_bead[1]->x_roi_max = bead->y_profile_bead[1]->xc + sdi_param->x_roi_size_2_Y;
  return 0;
}


//this recenter the bead along y using the y positions of the y profile objects
//it also ask to retake ref points & background
int sdi_v2_center_y_roi_bead(sdi_v2_parameter *sdi_param, sdi_v2_bead *bead)
{
  int i;
  int offset_y_1, offset_y_2;
  static int count = 0;

  if (bead==NULL || sdi_param == NULL) return D_O_K;
  //handles couple of y profile objects and frignes associated
  //if the 2 y profiles moves not along the same value then nothing is done : something wrong in the image..
  offset_y_1 = (int) (bead->y_profile_bead[0]->y_t[bead->y_profile_bead[0]->c_output_t] * sdi_param->gy / sdi_param->pixel_size);
  offset_y_2 = (int) (bead->y_profile_bead[1]->y_t[bead->y_profile_bead[1]->c_output_t] * sdi_param->gy / sdi_param->pixel_size);

  if (offset_y_1==offset_y_2 || offset_y_2==(offset_y_1-1) || offset_y_2==(offset_y_1+1) && fabs(offset_y_1)<6 && fabs(offset_y_2)<6)
  {
      count ++;
      my_set_window_title("count %d", count);
      bead->y_profile_bead[0]->yc += (int)((offset_y_1+offset_y_2)/2);
      sdi_v2_y_profile_update_parameter(bead->y_profile_bead[0], sdi_param);
      bead->y_profile_bead[1]->yc += (int)((offset_y_1+offset_y_2)/2);
      sdi_v2_y_profile_update_parameter(bead->y_profile_bead[1], sdi_param);
      bead->y_profile_bead[i]->need_background = true;
      //bead->fringes_bead[i]->yc += (int)((offset_y_1+offset_y_2)/2);
      //sdi_v2_fringes_parameter_update(bead->fringes_bead[i], sdi_param);
      //bead->fringes_bead[i]->need_background = true;
      //bead->fringes_bead[i]->need_reference_point = true;
  }

  return 0;
}

//raw profile todo el reste ac switch la c urgent
int sdi_v2_build_image_with_2d_fringes_buffer(sdi_v2_bead *bead, sdi_v2_parameter *param, O_i *oi)
{
  if (bead == NULL || param == NULL || oi == NULL) return D_O_K;
  if (bead->fringes_bead[0]->n_raw_x_profile_buffer != bead->fringes_bead[1]->n_raw_x_profile_buffer) return D_O_K;
  int j, k, nx, ny ;
  imreg *imr = NULL;
  if (ac_grep(cur_ac_reg, "%im", &imr)!=1) return D_O_K;
  ny = bead->fringes_bead[0]->n_raw_x_profile_buffer ;
  nx = param->x_roi_size;
  oi = create_and_attach_oi_to_imr(imr, 2 * nx, ny, IS_FLOAT_IMAGE);
  d_s *deb;
  O_p *op;
  for(j=0;j<128;j++)
  {
    deb->xd[j] = j;
    deb->yd[j] = bead->fringes_bead[0]->raw_x_profile[0][j];
  }
  create_hidden_pltreg_with_op(&op, 1, 1, 0, "deb");
  add_data_to_one_plot(op, IS_DATA_SET, deb);
  for(j=0;j<ny;j++)
  {
    for(k=0;k<nx;k++)
    {
      oi->im.pixel[j].ch[k] = bead->fringes_bead[0]->raw_x_profile[j][k];
      oi->im.pixel[j].ch[k + nx] = bead->fringes_bead[1]->raw_x_profile[j][k];
    }
  }
  find_zmin_zmax(oi);
  return 0;
}

//compute z with possibility of different arrangement of image
int sdi_v2_compute_z_bead(sdi_v2_bead *bead)
{
  int i;
  if (bead==NULL) return D_O_K;
  bead->z_t[bead->c_output_t] = 0 ;
  for (i=0 ; i<bead->n_fringes_bead ;i++) bead->z_t[bead->c_output_t] += bead->fringes_bead[i]->z_coupled_t[bead->fringes_bead[i]->c_output_t];
  bead->z_t[bead->c_output_t] /= bead->n_fringes_bead;
  bead->z_t[bead->c_output_t] *= bead->index_factor;
  bead->z_t[bead->c_output_t] -= bead->z_offset;
  return 0;
}
int sdi_v2_compute_y_bead(sdi_v2_bead *bead)
{
  int i;
  if (bead==NULL) return D_O_K;
  bead->y_t[bead->c_output_t] = 0 ;
  for (i=0 ; i<bead->n_y_profile_bead ;i++) bead->y_t[bead->c_output_t] += bead->y_profile_bead[i]->y_t[bead->y_profile_bead[i]->c_output_t];
  bead->y_t[bead->c_output_t] /= bead->n_y_profile_bead;
  bead->y_t[bead->c_output_t] += bead->y_offset;
  return 0;
}
int sdi_v2_compute_x_bead(sdi_v2_bead *bead)
{
  int i;
  if (bead==NULL) return D_O_K;
  bead->x_t[bead->c_output_t] = 0 ;
  for (i=0 ; i<bead->n_fringes_bead ;i++) bead->x_t[bead->c_output_t] += bead->fringes_bead[i]->x_coupled_t[bead->fringes_bead[i]->c_output_t];
  bead->x_t[bead->c_output_t] /= bead->n_fringes_bead;
  bead->x_t[bead->c_output_t] += bead->x_offset;
  return 0;
}

int sdi_v2_remove_one_sdi_bead(sdi_v2_bead *bead)
{
  if (bead==NULL) return D_O_K;
  free(bead->y_t);
  bead->y_t=NULL;
  free(bead->z_t);
  bead->z_t=NULL;
  free(bead->y_t);
  bead->y_t=NULL;
  free(bead->quality_factor_t);
  bead->quality_factor_t=NULL;
  free_data_set(bead->ds_out1);
  bead->ds_out1 = NULL;
  free_data_set(bead->ds_out2);
  bead->ds_out2 = NULL;
  free_data_set(bead->ds_out3);
  bead->ds_out3 = NULL;
  free(bead);
  bead = NULL;
  return 0;
}

//useful to adjust the distance between the 2 subimages
int sdi_v2_update_bead_parameters(sdi_v2_bead *bead, sdi_v2_parameter *param, int xc, int yc)
{
  if (bead == NULL || param == NULL) return D_O_K;
  bead->xc = xc;
  bead->yc = yc;
  bead->x_offset = xc;
  bead->y_offset = yc;
  bead->index_factor = param->index_factor;

  //y
  bead->y_profile_bead[0]->xc = bead->xc;
  bead->y_profile_bead[0]->x_roi_min = bead->y_profile_bead[0]->xc - param->x_roi_size_2_Y;
  bead->y_profile_bead[0]->x_roi_max = bead->y_profile_bead[0]->xc + param->x_roi_size_2_Y;
  bead->y_profile_bead[0]->yc = bead->yc - (int)((param->dist_half_beads+1)/2);
  bead->y_profile_bead[0]->y_roi_min = bead->y_profile_bead[0]->yc - param->y_roi_size_2_Y;
  bead->y_profile_bead[0]->y_roi_max = bead->y_profile_bead[0]->yc + param->y_roi_size_2_Y;

  bead->y_profile_bead[1]->xc = bead->xc;
  bead->y_profile_bead[1]->x_roi_min = bead->y_profile_bead[1]->xc - param->x_roi_size_2_Y;
  bead->y_profile_bead[1]->x_roi_max = bead->y_profile_bead[1]->xc + param->x_roi_size_2_Y;
  bead->y_profile_bead[1]->yc = bead->yc + (int)((param->dist_half_beads)/2);
  bead->y_profile_bead[1]->y_roi_min = bead->y_profile_bead[1]->yc - param->y_roi_size_2_Y;
  bead->y_profile_bead[1]->y_roi_max = bead->y_profile_bead[1]->yc + param->y_roi_size_2_Y;

  //x, z
  bead->fringes_bead[0]->xc = bead->xc;
  bead->fringes_bead[0]->x_roi_min = bead->fringes_bead[0]->xc - param->x_roi_size_2;
  bead->fringes_bead[0]->x_roi_max = bead->fringes_bead[0]->xc + param->x_roi_size_2;
  bead->fringes_bead[0]->yc = bead->yc - (int)((param->dist_half_beads+1)/2);
  bead->fringes_bead[0]->y_roi_min = bead->fringes_bead[0]->yc - param->y_roi_size_2;
  bead->fringes_bead[0]->y_roi_max = bead->fringes_bead[0]->yc + param->y_roi_size_2;

  bead->fringes_bead[1]->xc = bead->xc;
  bead->fringes_bead[1]->x_roi_min = bead->fringes_bead[1]->xc - param->x_roi_size_2;
  bead->fringes_bead[1]->x_roi_max = bead->fringes_bead[1]->xc + param->x_roi_size_2;
  bead->fringes_bead[1]->yc = bead->yc + (int)((param->dist_half_beads)/2);
  bead->fringes_bead[1]->y_roi_min = bead->fringes_bead[1]->yc - param->y_roi_size_2;
  bead->fringes_bead[1]->y_roi_max = bead->fringes_bead[1]->yc + param->y_roi_size_2;

  return 0;
}

//v_00 is phase velocity for first pattern (rad per micr), v_10 is group velocity for first pattern (pix per micro)
int sdi_v2_set_calibration_of_bead(sdi_v2_bead *bead, float v_00, float v_10, float nu00, float v_01, float v_11, float nu01, float v_x0, float v_x1)
{
  if (bead==NULL) return D_O_K;
  bead->fringes_bead[0]->v_0 = v_00;
  bead->fringes_bead[0]->v_1 = v_10;
  bead->fringes_bead[0]->nu0 = nu00;
  bead->fringes_bead[0]->v_x = v_x0;

  bead->fringes_bead[1]->v_0 = v_01;
  bead->fringes_bead[1]->v_1 = v_11;
  bead->fringes_bead[1]->nu0 = nu01;
  bead->fringes_bead[1]->v_x = v_x1;
  return 0;
}

int sdi_v2_update_c_profile_bead_points(sdi_v2_bead *bead, int ci)
{
  if (bead==NULL) return D_O_K;
  if (bead->n_output_t !=0)  bead->c_output_t = ci % bead->n_output_t;
  return 0;
}

int sdi_v2_switch_ptr_to_sdi_bead_1d_float_field(sdi_v2_bead *bead, int which_data, float **field, float **ext_float, int n_ext_float)
{
  if (bead==NULL) return D_O_K;
  switch(which_data){

    //really essential for the following
    case PTR_BEAD_Z_T :
    if (bead->z_t == NULL)
    {
      if (n_ext_float == 0) bead->z_t = (float *)calloc(1, sizeof(float));
      else
      {
        bead->hard_linked = true;
        bead->z_t = *ext_float;
      }
    }
    *field = bead->z_t;
    break;
    case PTR_BEAD_Y_T :
    if (bead->y_t == NULL)
    {
      if (n_ext_float == 0) bead->y_t = (float *)calloc(1, sizeof(float));
      else
      {
        bead->hard_linked = true;
        bead->y_t = *ext_float;
      }
    }
    *field = bead->y_t;
    break;
    case PTR_BEAD_X_T :
    if (bead->x_t == NULL)
    {
      if (n_ext_float == 0) bead->x_t = (float *)calloc(1, sizeof(float));
      else
      {
        bead->hard_linked = true;
        bead->x_t = *ext_float;
      }
    }
    *field = bead->x_t;
    break;
    case PTR_BEAD_QUALITY_FACTOR_T :
    if (bead->quality_factor_t == NULL)
    {
      if (n_ext_float == 0) bead->quality_factor_t = (float *)calloc(1, sizeof(float));
      else
      {
        bead->hard_linked = true;
        bead->quality_factor_t = *ext_float;
      }
    }
    *field = bead->quality_factor_t;
    break;

  }
  return 0;
}

int sdi_v2_allocate_sdi_v2_bead_output_t_fields(sdi_v2_bead *bead, int size)
{
  if (bead==NULL) return D_O_K;
  sdi_v2_allocate_sdi_v2_bead_1d_float_fields(bead, size, PTR_BEAD_Z_T);
  sdi_v2_allocate_sdi_v2_bead_1d_float_fields(bead, size, PTR_BEAD_Y_T);
  sdi_v2_allocate_sdi_v2_bead_1d_float_fields(bead, size, PTR_BEAD_X_T);
  sdi_v2_allocate_sdi_v2_bead_1d_float_fields(bead, size, PTR_BEAD_QUALITY_FACTOR_T);
  bead->n_output_t = size;
  bead->c_output_t = 0;
  return 0;
}

//forbidden if allocated linking to record strucures
int sdi_v2_allocate_sdi_v2_bead_1d_float_fields(sdi_v2_bead *bead, int buffer_size, int which_data)
{
  if (bead==NULL) return D_O_K;
  switch (which_data) {
    case PTR_BEAD_Z_T:
    if (bead->z_t == NULL) bead->z_t = (float *)calloc(buffer_size, sizeof(float));
    else bead->z_t = (float *)realloc(bead->z_t, buffer_size * sizeof(float));
    break;
    case PTR_BEAD_Y_T:
    if (bead->y_t == NULL) bead->y_t = (float *)calloc(buffer_size, sizeof(float));
    else bead->y_t = (float *)realloc(bead->y_t, buffer_size * sizeof(float));
    break;
    case PTR_BEAD_X_T:
    if (bead->x_t == NULL) bead->x_t = (float *)calloc(buffer_size, sizeof(float));
    else bead->x_t = (float *)realloc(bead->x_t, buffer_size * sizeof(float));
    break;
    case PTR_BEAD_QUALITY_FACTOR_T:
    if (bead->quality_factor_t == NULL) bead->quality_factor_t = (float *)calloc(buffer_size, sizeof(float));
    else bead->quality_factor_t = (float *)realloc(bead->quality_factor_t, buffer_size * sizeof(float));
    break;
  }
  return 0;
}

// a reecrire pour nb var
int sdi_v2_init_bead_by_assembly(sdi_v2_bead *bead, sdi_v2_parameter *param, sdi_v2_fringes *fringes1, sdi_v2_fringes *fringes2,
                                                                  sdi_v2_y_profile *y_profile1, sdi_v2_y_profile *y_profile2)
{
  if (bead==NULL) bead = (sdi_v2_bead *)calloc(1,sizeof(sdi_v2_bead));

  bead->bead_class = 0;
  bead->bead_class |= SDI_UNINITIALIZED_BEAD;
  bead->weird_count = 0;
  bead->quality_factor = 1;
  bead->mouse_draged = 0;
  bead->index_in_fov = -1;//is not a member of a fov
  bead->recenter_along_y = false;
  bead->recenter_along_x = false;
  bead->apply_proc1 = false;
  bead->ds_out1 = NULL;
  bead->ds_out2 = NULL;
  bead->ds_out3 = NULL;

  bead->z_offset = 0;
  bead->index_factor = param->index_factor;
  bead->y_offset = 0;
  bead->x_offset = 0;
  bead->hard_linked = false;

  bead->y_t=NULL;
  bead->z_t=NULL;
  bead->x_t=NULL;
  bead->quality_factor_t=NULL;
  bead->n_output_t = 0;
  bead->c_output_t = 0;

  if (fringes2 == NULL)
  {
    bead->n_fringes_bead = 1;
    bead->fringes_bead = (sdi_v2_fringes **) calloc(1, sizeof(sdi_v2_fringes *));
    bead->fringes_bead[0]=fringes1;
    bead->xc = fringes1->xc;
    bead->yc = fringes1->yc;
  }
  else
  {
    bead->n_fringes_bead = 2;
    bead->fringes_bead = (sdi_v2_fringes **) calloc(2, sizeof(sdi_v2_fringes *));
    bead->fringes_bead[0]=fringes1;
    bead->fringes_bead[1]=fringes2;
    bead->xc = (int) ((fringes1->xc + fringes2->xc)/2) ;
    bead->yc = (int) ((fringes1->yc + fringes2->yc)/2) ;
  }
  bead->v_phi0_z=999;
  bead->v_phi1_z=999;

  if (y_profile2==NULL)
  {
    bead->n_y_profile_bead = 1;
    bead->y_profile_bead = (sdi_v2_y_profile **)calloc(1, sizeof(sdi_v2_y_profile *));
    bead->y_profile_bead[0] = y_profile1;
  }
  else
  {
    bead->n_y_profile_bead = 2;
    bead->y_profile_bead = (sdi_v2_y_profile **)calloc(2, sizeof(sdi_v2_y_profile *));
    bead->y_profile_bead[0] = y_profile1;
    bead->y_profile_bead[1] = y_profile2;
  }

  bead->x_offset =  bead->xc;
  bead->y_offset =  bead->yc;
  return 0;
}

int sdi_v2_reset_data_of_bead(sdi_v2_bead *bead)
{
  if (bead==NULL) return D_O_K;
  free(bead->z_t);
  bead->z_t = NULL;
  free(bead->y_t);
  bead->y_t = NULL;
  free(bead->x_t);
  bead->x_t = NULL;
  free(bead->quality_factor_t);
  bead->quality_factor_t = NULL;
  bead->n_output_t = 0;
  bead->c_output_t = 0;
  return 0;
}
