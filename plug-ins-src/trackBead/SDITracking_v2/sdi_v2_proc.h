#pragma once
//this codes aimes at doing basic signal processing
//a signal processing module is triggered by either the user
//a periodic software trigger of an event (like bad fringes)
//the treatment are statistical (use of boost::accumulator in the future cc++)
//or mapping (scalar product)

/*#include "sdi_basic.h"
#include "sdi_parameter.h"
#include "sdi_fringes.h"
#include "sdi_y_profile.h"
#include "sdi_bead.h"
*/
//encodes the type of objects
# define IS_ONE_SDI_FRINGE 0
# define IS_ONE_SDI_BEAD 1
# define IS_ONE_SDI_Y_PROFILE 2

//list of existing proc : when one wants to add one,
# define PROC1 0 //moving average
# define PROC2 1 //moving sigma
# define PROC3 2 //linear BF filter
# define PROC4 3 //substract linear fit

//defines a periodic trigger (in index) to do some signal processing during the experiment
//it is defined by a periodicity of the selected interesting points
//with delay, offset, period, subset_length...
typedef struct sdi_periodic_trigger
{
  int period;
  int subset_length;
  int offset;

  //the memory, counted from general index of experiment
  int delay;
  int i_min;
  int i_max;

  int current_index;
  int current_i_select;//the next to be extracted

  bool trigger;//boolean that triggers what this selection is tuning (processing, refreshing, saving...)
} sdi_T_trigger;

/*
typedef struct sdi_fillibbt_filter
{
  int size;
  float nu_c;//cutoff frequency, in Hz (then translated in pixel for bt_filter)
  fft_plan *fp;
  filter *bt_filter;
  bool trigger;//boolean that triggers the computation
} sdi_fbt_filter;


typedef struct sdi_signal_processing
{
  int which_proc;//defines the nature of the proc (1 for moving average, 2 for sigma...)
  //this defines who is the input (z of bead, y of one y profile, contrast of fringes...)
  int which_object;//type of object
  int which_data;//wich data in the object
  //this defines where is the output : ie index in the proc array in the same object
  //during the intialization, the software ask the object how many it already has and then add one
  int which_output;

  sdi_T_select *T_roi;//if needed, contains the parameters for proc1, proc2
  //sdi_fbt_filter *f_filter;//if needed, contains th parameters for a Fourier filter proc
} sdi_proc;
*/

//handles the call to the proc
//PXV_FUNC(int, update_proc_opening,(sdi_general *gen, sdi_proc *proc));
//PXV_FUNC(int, update_proc_closing, (sdi_proc *proc));
PXV_FUNC(int, update_sdi_T_select_trigger_closing, (sdi_T_select *T_roi));
PXV_FUNC(int, update_sdi_T_select_trigger_opening, (sdi_T_select *T_roi, int current_i));
/*
PXV_FUNC(int, init_one_proc, (sdi_proc *proc, int which_proc, int which_object, int which_data));

//initialization and allocationint add_one_proc_to_sdi_gen(sdi_general *sdi_gen, sdi_proc *proc)
PXV_FUNC(int, set_T_roi_parameter, (sdi_T_select *T_roi, int period, int offset, int subset_length, int i_min, int i_max, int delay));
//PXV_FUNC(int, init_output_of_one_proc, (sdi_general *sdi_gen, sdi_proc *proc, int object_index));
PXV_FUNC(int, allocate_sdi_fringes_proc_t_fields, (sdi_fringes *fringes, int buffer_size, int *which_data, int n_which_data));
PXV_FUNC(int, allocate_sdi_y_profile_proc_t_fields, (sdi_y_profile *y_profile, int buffer_size, int *which_data, int n_which_data));
PXV_FUNC(int, allocate_sdi_bead_proc_t_fields, (sdi_bead *bead, int buffer_size, int *which_data, int n_which_data));
PXV_FUNC(int, add_one_proc_to_sdi_bead, (sdi_bead *bead, sdi_proc *proc));
PXV_FUNC(int, add_one_proc_to_sdi_y_profile, (sdi_y_profile *y_profile, sdi_proc *proc));
PXV_FUNC(int, add_one_proc_to_sdi_fringes, (sdi_fringes *fringes, sdi_proc *proc));
PXV_FUNC(int, call_proc_on_ds, (sdi_proc *proc, d_s *ds_in, d_s *ds_out, int current_index, float current_time));
//switch input & output datasets of proc between the different fields of the objects
//PXV_FUNC(int, compute_proc, (sdi_general *gen, sdi_proc *proc, int object_index));
//PXV_FUNC(int, switch_ptr_to_proc_fields, (sdi_general *gen, sdi_proc *proc, int object_index, d_s **in, d_s **out, int *current_index, float *current_time));
PXV_FUNC(int, switch_ptr_to_sdi_fringes_proc_field, (sdi_fringes *fringes, int proc_index, d_s **ds));
PXV_FUNC(int, sdi_fringes_remove_one_proc, (sdi_fringes *fringes, int proc_index));
PXV_FUNC(int, switch_ptr_to_sdi_y_profile_proc_field, (sdi_y_profile *y_profile, int proc_index, d_s **ds));
PXV_FUNC(int, switch_ptr_to_sdi_bead_proc_field, (sdi_bead *bead, int proc_index, d_s **ds));

//the proc functions
PXV_FUNC(int, compute_proc1, (sdi_T_select *T_roi, d_s *in, d_s *out, int current_i, float current_time));
PXV_FUNC(int, compute_proc2, (sdi_T_select *T_roi, d_s *in, d_s *out, int current_i, float current_time));
