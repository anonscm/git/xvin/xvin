
#define IS_PH_OLIGO 1
#define IS_PH_G4  2
#define IS_PH_STRANGE_PICK  4
#define G4_BAD_CYCLE 8
#define IS_PH_NO_BLOCK 16

#ifndef __cplusplus // keep this here: see draw_track_event_phase_average.hh


int draw_track_event_phase_average(void)
#endif
{
    int i, j, k, page_n, i_page;
    //b_track *bt = NULL;
    pltreg *pr = NULL;
    O_p *op = NULL, *opt = NULL, *optt = NULL, *oph = NULL, *opdt = NULL, *ophdt = NULL, *opopen = NULL, *fopen_op = NULL, *op_g4 = NULL;
    d_s *ds = NULL, *dst = NULL, *dsc = NULL, *dstd = NULL, *ds_fopen = NULL, *ds_fclose = NULL, *ds_fopens = NULL, *ds_fcloses = NULL, *dsg41 = NULL, *dsg42 = NULL,*dsg43 = NULL,*dsg44 = NULL,*ds_block_times= NULL,*ds_g4_times = NULL, *ds_beads_g4 = NULL, *ds_beads_oligo = NULL, *ds_removed_beads = NULL;//, *dsfit;
    int nf,  param_cst, ph1s, ph1e, no = 0, navg, i_ph, phi0s, phi0e;//,tmp_j;
    float tmp, xavg1, yavg1, zavg1, zobj, tmpx = 0, zd[3], md[3], dzmin, dzmax; //, tmpx_1
    int start_index, ending, done, ph_max = 0, npt, profile_invalid, ji, verbose = 0, lmin, lmax, dmin, dmax;
    static char all_fixed[512] = {0};
    static char local_set[1024] = {0};
    char temp_set[1024] = {0};
    char *token = NULL;
    static int add_subset=0;
    static int light_mode = 3;

    int  fixed_beads[128];
    static int i_bead = 0, all_bead = 0, fixed_bd = -1, z_or_f = 0, ref_ph = 3, ph_nx[32], skip_disp_s = 0,
               bigger_or_smaller = 0, filter_fixed = 1;
    static int skip_disp_e = 0, tolerant = 1, discri_low = 1,  discri_high = 3, same_op = 0, find_bump = 0;
    static int  skip_bef = 5, skip_after = 5, same_ds = 1, restrict_mode = 0, z_or_zmag = 0, dzdtst = 0, min = -1, max = -1,
                do_adj_f_unzip = 0;
    static float dzph_bump = 0.1, dzph2_bump = -0.05, r_max = 0, r_min = 0, hi_low_discri = 1.2, activ_thres = 0.05,
                 adj_f_unzip = 15, biny = 3;
    static int phase_align = 5;
    //int i_phase_align = 0;
    b_record *br = NULL, *brf = NULL;
    b_record filtered;
    float * filtdata = NULL, ** addresses = NULL;
    g_record *g_r = NULL;
    int ***im_array_k_ph = 0;
    char st[32], question[4096], phs[1024] = {0}, phs_pos[2048] = {0};
    static char do_bead[128], do_ph[32];
    float f_unzip_mean = 0, x_last_bump, y_last_bump, x_last_bump2, y_last_bump2, Max_pos, Max_val, Max_pos2, Max_val2,
          Max_open, mol_ext = 0;
    int f_unzip_n = 0, unzip_ds_s = 0, last_bump, last_bump2, new_plot, n_csv, kon_file_ok = 0, i_csv, last_treated_bd;
    static int analyze_b = 0, T_ph[32], nT_ph[32], gen_fopen_op = 0, cyselect = 0, phselect = 0;
    static float block_high = 1.0, block_low = 0.5, oligo = 50;
    double af, tau, af2, taui, csv_data[32];
    char csv_title[32][32], kon_file[2048];
    float zmagt, zmag_open, zmag_test, zmag_low, Max_close, rot, Max_posa, Max_valc, Max_w, Max_w2, Max_wc;//, zmol_hi, zmol_lo;
    float vcap_open = 0, vcap_test = 0, vcap_low = 0;
    FILE *fp = NULL;
    unsigned int t0; // long
    int ph_str[32] = {0}, ph_end[32] = {0};
    int allegro_res = WIN_OK;
    static int g4_analysis = 0,bead_s = 0, bead_range = 0, bead_e = 0;
    //int pos_max = 0;
    //int pos_min = 0;
    int ph_k_nb_points = 0;
    static float g4_significance = 0.05;
    int g4_status = 0;



    static float tol_g4 = 0.1;
    static float tol_oligo = 0.1;
    static float oligo_pos =  -0.3;
    static float g4_pos =  -0.5;
    static int dump_g4_plots = 1;
    static int all_block_above_g4 = 1;
    int oligo_pts = 0;
    int g4_pts = 0;
    int no_block_pts = 0;

    //variables g4
    //static int g4_dump_times = 0;
    static int take_last_g4 = 1, take_last_oligo = 1;
    int block_points_bf_g4 = 0;
    int is_g4 = 0;
    int begin_g4 = 0;
    int end_g4 = 0;
    int last_seen_g4 = 0, last_seen_g4_in_phase = 0;
    bool first_g4_time_for_this_bead = 1;
    bool first_block_time_for_this_bead = 1;



    char cctmp[2048] = "%pr%op";
    ac_grep(cur_ac_reg, cctmp, &pr, &op);
    g_r = find_g_record_in_pltreg(pr);

    if (updating_menu_state != 0)
    {
        // we wait for recording finish
        if (g_r == NULL || working_gen_record == g_r)
        {
            active_menu->flags |=  D_DISABLED;
        }
        else
        {
            active_menu->flags &= ~D_DISABLED;
        }

        return D_O_K;
    }

    if (pr == NULL)
    {
        return 0;
    }

    if (op != NULL && op->user_id == 4097)
    {
        i_bead = op->user_ispare[ISPARE_BEAD_NB];
        op = NULL;
    }

    //if (g_r == NULL || g_r->n_record == 0) return OFF;
    if (g_r == NULL)
    {
        return OFF;
    }

    nf = g_r->abs_pos;

    if (nf <= 0)
    {
        return OFF;
    }



    fixed_bd = (do_substract_fixed_bead) ? fixed_bead_nb : fixed_bd;

    bead_e = (bead_e == 0) ? g_r->n_bead - 1 : bead_e;

    //zmag = g_r->zmag[0][0];
    i = 0;
    snprintf(csv_title[i], sizeof(csv_title[i]), "trk nb");
    snprintf(csv_title[++i], sizeof(csv_title[i]), "bead nb");
    snprintf(csv_title[++i], sizeof(csv_title[i]), "[oligo]");
    snprintf(csv_title[++i], sizeof(csv_title[i]), "Zmag_{open}");
    snprintf(csv_title[++i], sizeof(csv_title[i]), "F_{open}");
    snprintf(csv_title[++i], sizeof(csv_title[i]), "T_{open}");
    snprintf(csv_title[++i], sizeof(csv_title[i]), "Zmag_{test}");
    snprintf(csv_title[++i], sizeof(csv_title[i]), "F_{test}");
    snprintf(csv_title[++i], sizeof(csv_title[i]), "T_{test}");
    snprintf(csv_title[++i], sizeof(csv_title[i]), "Zmag_{low}");
    snprintf(csv_title[++i], sizeof(csv_title[i]), "F_{low}");
    snprintf(csv_title[++i], sizeof(csv_title[i]), "T_{low}");
    snprintf(csv_title[++i], sizeof(csv_title[i]), "T_{on}");
    snprintf(csv_title[++i], sizeof(csv_title[i]), "N_{cycles} Ok");
    snprintf(csv_title[++i], sizeof(csv_title[i]), "Z_{open}(nm)");
    snprintf(csv_title[++i], sizeof(csv_title[i]), "error Z_{open}(nm)");
    snprintf(csv_title[++i], sizeof(csv_title[i]), "Z_{close}(nm)");
    snprintf(csv_title[++i], sizeof(csv_title[i]), "error Z_{close}(nm)");
    snprintf(csv_title[++i], sizeof(csv_title[i]), "Z_{block}(nm)");
    snprintf(csv_title[++i], sizeof(csv_title[i]), "error Z_{block}(nm)");
    snprintf(csv_title[++i], sizeof(csv_title[i]), "N_{block}");
    snprintf(csv_title[++i], sizeof(csv_title[i]), "P_{block}");
    snprintf(csv_title[++i], sizeof(csv_title[i]), "Error P_{block}");
    snprintf(csv_title[++i], sizeof(csv_title[i]), "T_{off}(s)");
    snprintf(csv_title[++i], sizeof(csv_title[i]), "error T_{off}");
    snprintf(csv_title[++i], sizeof(csv_title[i]), "a de l'expo");
    snprintf(csv_title[++i], sizeof(csv_title[i]), "T_{off} cumul(s)");
    snprintf(csv_title[++i], sizeof(csv_title[i]), "error T_{off} cumul");
    snprintf(csv_title[++i], sizeof(csv_title[i]), "a de l'expo cumul");
    snprintf(csv_title[++i], sizeof(csv_title[i]), "<T_{off}>(s)");
    n_csv = ++i;
    //win_printf("i %d n_csv %d",i,n_csv);
    lmin = lmax = -1;
    //retrieve_min_max_event(g_r, &min, &max);
    retrieve_min_max_event_and_phases(g_r, &lmin, &lmax, &ph_max);

    for (done = -1; done < 0; )
    {
        phi0s = 0;
        done = retrieve_image_index_of_next_point_phase(g_r, lmin, 1, &phi0s, &phi0e, &param_cst);
        if (done <0 )lmin++;
    }

    for (done = -1; done < 0; )
    {
        phi0s = 0;
        done = retrieve_image_index_of_next_point_phase(g_r, lmax, 1, &phi0s, &phi0e, &param_cst);
        if (done <0 ) lmax--;
    }

    if (min < 0 || min > max || min > lmax || max < 0 || max > lmax)
    {
        min = lmin;
        max = lmax;
    }



#   ifndef __cplusplus // keep this here: see draw_track_event_phase_average.hh
    char key[] = "draw_track_event_phase_average";
#   endif
    all_bead = get_config_int(key, "all_bead", 0);
    filter_fixed = get_config_int(key, "filter_fixed", 1);
    same_op = get_config_int(key, "same_op", 0);

    for (i = 0; i <= ph_max; i++)
    {
        snprintf(st, sizeof(st), "do_phase_%02d", i);
        do_ph[i] = get_config_int(key, st, 0);
    }

    //min = get_config_int("draw_track_event_phase_average", "min_cycle", 0);
    //max = get_config_int("draw_track_event_phase_average", "max_cycle", 0);
    add_subset = get_config_int(key,"add_subset",0);
    all_block_above_g4 = get_config_int(key,"all_block_above_g4",1);

    oligo_pos = get_config_float(key,"oligo_pos",-0.3);
    g4_pos = get_config_float(key,"g4_pos",-0.5);
    g4_significance = get_config_float(key,"g4_significance",0.05);
    tol_g4 = get_config_float(key,"tol_g4",0.08);
    tol_oligo = get_config_float(key,"tol_oligo",0.08);
    dump_g4_plots = get_config_int(key,"dump_g4_plots",1);
    z_or_zmag = get_config_int(key, "z_or_zmag", 0);
    z_or_f = get_config_int(key, "z_or_f", 0);
    same_ds = get_config_int(key, "same_ds", 1);
    tolerant = get_config_int(key, "tolerant", 1);
    skip_disp_s = get_config_int(key, "skip_disp_s", 0);
    skip_disp_e = get_config_int(key, "skip_disp_e", 0);
    hi_low_discri = get_config_float(key, "hi_low_discri", 1.2);
    discri_low = get_config_int(key, "discri_low", 1);
    discri_high = get_config_int(key, "discri_high", 3);
    bigger_or_smaller = get_config_int(key, "bigger_or_smaller", 0);
    activ_thres = get_config_float(key, "activ_thres", 0.05);
    ref_ph = get_config_int(key, "ref_ph", 3);
    restrict_mode = get_config_int(key, "restrict_mode", 0);
    skip_bef = get_config_int(key, "skip_bef", 5);
    skip_after = get_config_int(key, "skip_after", 5);
    r_max = get_config_float(key, "r_max", 0);
    r_min = get_config_float(key, "r_min", 0);
    find_bump = get_config_int(key, "find_bump", 0);
    dzdtst = get_config_int(key, "dzdtst", 0);
    dzph_bump = get_config_float(key, "dzph_bump", 0.1);
    dzph2_bump = get_config_float(key, "dzph2_bump", -0.05);
    do_adj_f_unzip = get_config_int(key, "do_adj_f_unzip", 0);
    adj_f_unzip = get_config_float(key, "adj_f_unzip", 15);
    analyze_b = get_config_int(key, "analyze_b", 0);
    block_low = get_config_float(key, "block_low", 0.5);
    block_high = get_config_float(key, "block_high", 1.0);
    oligo = get_config_float(key, "oligo", 50);
    cyselect = get_config_int(key, "all_cycles", 1);
    cyselect = (cyselect > 0) ? 0 : 1;
    phselect = get_config_int(key, "all_phases", 1);
    phselect = (phselect > 0) ? 0 : 1;

    snprintf(question, sizeof(question),
             "Dump phases signal for all %d beads-> or select beads: \n ", g_r->n_bead);

    for (k = 0, i = 0; k < g_r->n_bead && (size_t)k < sizeof(do_bead); k++)
    {
      br = g_r->b_r[k];

      if (((g_r->b_r[k]->completely_losted & COMPLETELY_LOST) == 0) && (g_r->b_r[k]->calib_im != NULL  && g_r->SDI_mode == 0))
        {
            do_bead[k] = 0;

            if (i == 0)
            {
                snprintf(question + strlen(question), sizeof(question) - strlen(question), "%d->%%Q ", k);
            }
            else
            {
                snprintf(question + strlen(question), sizeof(question) - strlen(question), "%d->%%q ", k);
            }

            if (i % 10 == 9)
            {
                snprintf(question + strlen(question), sizeof(question) - strlen(question), "\n");
            }

            if (k == i_bead)
            {
                do_bead[k] = 1;
            }

            i++;
        }
        else
        {
            do_bead[k] = -1;
        }
    }

    snprintf(question, sizeof(question),
             "{\\color{yellow}Dump phase signal(s)} for all %d beads ->%%b \n"
             "Or %%b beads in range [%%d,%%d]\n"
            "Or  select beads to discard %%R or to select %%r, separated by comas \n"
             "%%50s \n"
             "In the same plot %%b.\n", g_r->n_bead);
    snprintf(question + strlen(question), sizeof(question) - strlen(question),
             "{\\color{yellow}Treat} %%R->all cycles %%r or in range [%%4d, %%4d]"
             "\nFixed bead(s) substracted %%30s \n"
	     "(N for no beads, X to substract bead X; X,Y,Z to sunstract X,Y and Z)\n"
#ifdef PIAS // compilation issue with xvnonlin_run
             " Filter their average %%b"
#endif
             "\n"
             "Display Z%%R or Z_{mag}%%r vs Time%%R Z_{mag}%%r E-Force%%r index in phase%%r in cycle%%r\n"
             "Treat [%%R->all %d phases]\n [%%r->or select among] : ", ph_max);

    for (k = 0, i = 0; k <= ph_max && (size_t)k < sizeof(do_ph); k++)
    {
        if (do_ph[k] < 0 || do_ph[k] > 1)
        {
            do_ph[k] = 0;
        }

        if (i == 0)
        {
            snprintf(question + strlen(question), sizeof(question) - strlen(question), "%d->%%Q ", k);
        }
        else
        {
            snprintf(question + strlen(question), sizeof(question) - strlen(question), "%d->%%q ", k);
        }

        if (i % 10 == 9)
        {
            snprintf(question + strlen(question), sizeof(question) - strlen(question), "\n");
        }

        i++;
    }

    snprintf(question + strlen(question), sizeof(question) - strlen(question),
             "\n%%R->separate ds for each phase %%rfor each cycle %%rfor each bead %%rsame ds for all bead\n"
             "Tolerate bad z profile %%b. Align data on start of phase %%5d\n"
             "skip point before %%6d after %%6d\n"
             "{\\color{yellow}Keeps data if} \\oc molecule extension is greater than %%10f\n"
             "between low phase %%6d and high phase %%6d (<0 do not check)\n"
             "Using a gaussian histogram with a size %%8f (nm)\n"
             "\\oc  Keeps data if molecule extension between phase 1 and beginning of phase 5\n"
             "exceeds->%%R or is smaller->%%r or %%r-> do not apply this threshold %%6f\n"
             "{\\color{yellow}adjust Z to a reference} taken in phase %%8d (-1 no) restricted \n"
             "by: points number%%R Zmag range%%r Force Range%%r\n"
             "If points number skipping %%4d pts at beginning and %%4d at the end\n"
             "If zmag or force range max:%%6f  and min:%%6f \n"
             "{\\color{yellow}For points with strong dz/dt}\n"
             "Do nothing->%%R mark (label)->%%r erase ->%%r find (max dz/dt)->%%r Keep last only->%%r\n"
             "And do not keep points->%%R construct associated plot->%%r or add datasets %%r\n"
             "for > 0 bump detect dz/dt > %%6f (\\mu m)\n"
             "for < 0 bump detect dz/dt < %%6f (\\mu m)\n"
             "If you display expected force adjust it to unzipping transition %%b\n"
             "Force to adjust at unzipping transition %%8f\n"
             "Analyze single blockage statistics %%b if yes define range\n"
             "Z_{block high} %%8f Z_{block low} %%8f; [oligo] %%8f nM\n "
             "%%b->gerenate a plot with the opening force of hairpins vs bead number\n"
             "%%b->Click here to go to G4 analysis menu\n");
    // ead
#   ifdef __cplusplus // keep this here: see draw_track_event_phase_average.hh
    allegro_res = trackrenorm::do_win_scanf(script,
#   else
    allegro_res = win_scanf(
#   endif
			    question, &all_bead,&bead_range,&bead_s,&bead_e, &add_subset,&local_set, &same_op, &cyselect, &min, &max, all_fixed,
#ifdef PIAS // compilation issue with xvnonlin_run
			    &filter_fixed,
#endif
			    &z_or_zmag, &z_or_f, &phselect, do_ph
			    , &same_ds, &tolerant, &phase_align, &skip_disp_s, &skip_disp_e, &hi_low_discri, &discri_low
			    , &discri_high, &biny, &bigger_or_smaller, &activ_thres, &ref_ph, &restrict_mode, &skip_bef
			    , &skip_after, &r_max, &r_min, &find_bump, &dzdtst, &dzph_bump, &dzph2_bump, &do_adj_f_unzip
			    , &adj_f_unzip, &analyze_b, &block_high, &block_low, &oligo, &gen_fopen_op, &g4_analysis);


      if (g4_analysis)
      {

        i = win_scanf("%b Click to analyse g4\n"
        "%b click if you want to consider all blockages above the G4 \n"
                                 "if not, indicate the position (um) of oligo from the top %f\n"
                                "in any case, indicate the position (um) of g4 from the top %f\n"
                              "Indicate the tolerance on the position of the oligo (um) %f\n"
                              "Indicate the tolerance on the position of the G4 (um) %f\n"
                              "Indicate significance threshold  %f\n"
                              "%b Click to take in account last G4 even if not finished\n"
                              "%b Click to take in account last oligo blocking time even if not finished\n"
                              "Do you want to use light mode (0 : no, 3: very light) %d\n"
                              "%b Dump status plots \n",&g4_analysis,&all_block_above_g4,&oligo_pos,&g4_pos,&tol_oligo,&tol_g4,&g4_significance,&take_last_g4,&take_last_oligo,&light_mode,&dump_g4_plots);
         if (i == WIN_CANCEL) return 0;
         if (light_mode < 0) light_mode = 0;
         if (light_mode >3) light_mode = 3;



         }

    for(i = 0; i < (int)(sizeof(fixed_beads)/sizeof(int)); ++i)
        fixed_beads[i] = -1;

    no = -1; navg = 1;
    for(i = 0; all_fixed[i] != '\0' && i < (int)(sizeof(all_fixed)); ++i)
    {
        j = (int) (all_fixed[i]-'0');
        if(j >= 0 && j < 10)
        {
            if(navg == 1)
                fixed_beads[++no] = 0;

            fixed_beads[no] = fixed_beads[no]*10 + j;
            navg            = 0;
        }
        else
            navg = 1;
    }
    fixed_bd = no == -1 ? -1 : fixed_beads[0];

    if (allegro_res == WIN_CANCEL)
    {
        return D_O_K;
    }
					    set_config_int(key, "all_bead", all_bead);
    set_config_int(key, "filter_fixed", filter_fixed);
    set_config_int(key, "same_op", same_op);

    for (i = 0; i <= ph_max; i++)
    {
        snprintf(st, sizeof(st), "do_phase_%02d", i);
        set_config_int(key, st, do_ph[i]);
    }
    //set_config_int("draw_track_event_phase_average", "min_cycle",min);
    //set_config_int("draw_track_event_phase_average", "max_cycle",max);
    set_config_int(key, "z_or_zmag", z_or_zmag);
    set_config_int(key,"add_subset",add_subset);
    set_config_string(key,"local_set",local_set);
    set_config_float(key,"oligo_pos",oligo_pos);
    set_config_float(key,"g4_pos",g4_pos);
    set_config_float(key,"g4_significance",g4_significance);
    set_config_float(key,"tol_g4",tol_g4);
    set_config_float(key,"tol_oligo",tol_oligo);
    set_config_int(key,"dump_g4_plots",dump_g4_plots);

    set_config_int(key, "z_or_f", z_or_f);
    set_config_int(key, "same_ds", same_ds);
    set_config_int(key, "tolerant", tolerant);
    set_config_int(key, "skip_disp_s", skip_disp_s);
    set_config_int(key, "skip_disp_e", skip_disp_e);
    set_config_float(key, "hi_low_discri", hi_low_discri);
    set_config_int(key, "discri_low", discri_low);
    set_config_int(key, "discri_high", discri_high);
    set_config_int(key, "bigger_or_smaller", bigger_or_smaller);
    set_config_float(key, "activ_thres", activ_thres);
    set_config_int(key, "ref_ph", ref_ph);
    set_config_int(key, "restrict_mode", restrict_mode);
    set_config_int(key, "skip_bef", skip_bef);
    set_config_int(key, "skip_after", skip_after);
    set_config_float(key, "r_max", r_max);
    set_config_float(key, "r_min", r_min);
    set_config_int(key, "find_bump", find_bump);
    set_config_int(key, "dzdtst", dzdtst);
    set_config_float(key, "dzph_bump", dzph_bump);
    set_config_float(key, "dzph2_bump", dzph2_bump);
    set_config_int(key,"all_block_above_g4",all_block_above_g4);

    set_config_int(key, "do_adj_f_unzip", do_adj_f_unzip);
    set_config_float(key, "adj_f_unzip", adj_f_unzip);
    set_config_int(key, "analyze_b", analyze_b);
    set_config_float(key, "block_low", block_low);
    set_config_float(key, "block_high", block_high);
    set_config_float(key, "oligo", oligo);
    set_config_int(key, "all_cycles", (cyselect > 0) ? 0 : 1);
    set_config_int(key, "all_phases", (phselect > 0) ? 0 : 1);
    if (phselect == 0)
      {
	for (i = 0; i <= ph_max; i++)
	  do_ph[i] = 1;
      }
    if (cyselect == 0)
      {
	min = lmin;
	max = lmax;
      }

      im_array_k_ph = (int ***) malloc((max+1)*sizeof(int**));
      for (int ind = 0;ind<=max;ind++)
      {

        im_array_k_ph[ind] = (int **) malloc((ph_max+1)*sizeof(int*));


        for (int ind2 = 0;ind2<=ph_max;ind2++)
        {
          im_array_k_ph[ind][ind2]= (int *) malloc(2*sizeof(int));
          im_array_k_ph[ind][ind2][0] = -1;
          im_array_k_ph[ind][ind2][1] = -1;
        }
      }
      fill_phase_k_position_array(g_r,im_array_k_ph,min,max,ph_max);

    if (g4_analysis)
    {
         op_g4 = create_and_attach_one_plot(pr, 64,64, 0);
        ds_block_times = create_and_attach_one_ds(op_g4,64,64,0);
        ds_g4_times = op_g4->dat[0];
        ds_beads_g4 = create_and_attach_one_ds(op_g4,64,64,0);
        ds_beads_oligo = create_and_attach_one_ds(op_g4,64,64,0);
        ds_removed_beads = create_and_attach_one_ds(op_g4,64,64,0);


        op_g4->user_ispare[4] = 28476;



        ds_g4_times->nx = ds_g4_times->ny = ds_block_times->nx = ds_block_times->ny = ds_beads_g4->nx = ds_beads_g4->ny =  ds_beads_oligo->nx = ds_beads_oligo->ny = ds_removed_beads->nx = ds_removed_beads -> ny = 0;
        strcpy(cctmp, "s");
        create_attach_select_y_un_to_op(op_g4, IS_SECOND, 0, (float)1 / g_r->Pico_param_record.camera_param.camera_frequency_in_Hz, 0, 0, cctmp);
set_ds_source(ds_block_times, "Oligo blocking time before G4");
if (all_block_above_g4)    set_ds_treatement(ds_block_times,"all_block_above_g4, g4_pos = %f,tol_g4=%f,g4_significance=%f,take_last_g4=%d,take_last_oligo=%d,light_mode=%d",g4_pos,tol_g4,g4_significance,take_last_g4,take_last_oligo,light_mode);
else set_ds_treatement(ds_block_times,"oligo_pos=%f,g4_pos = %f,tol_oligo=%f,tol_g4=%f,g4_significance=%f,take_last_g4=%d,take_last_oligo=%d,light_mode=%d",oligo_pos,g4_pos,tol_oligo,tol_g4,g4_significance,take_last_g4,take_last_oligo,light_mode);

char message_par[128] = {0};
snprintf(message_par,sizeof(message_par),"Zmag open => Force = %g pN"
,trk_zmag_to_force(0, zmag_open, g_r));
ds_g4_times->src_parameter_type[0] = strdup(message_par);
        ds_g4_times->src_parameter[0] = zmag_open;
snprintf(message_par,sizeof(message_par),"Zmag test => Force = %g pN"
,trk_zmag_to_force(0, zmag_test, g_r));
ds_g4_times->src_parameter_type[1] = strdup(message_par);
        ds_g4_times->src_parameter[1] = zmag_test;
snprintf(message_par,sizeof(message_par),"Zmag low => Force = %g pN"
,trk_zmag_to_force(0, zmag_low, g_r));
ds_g4_times->src_parameter_type[2] = strdup(message_par);
        ds_g4_times->src_parameter[2] = zmag_low;
        ds_g4_times->src_parameter_type[3] = strdup("Vcap open");
        ds_g4_times->src_parameter[3] = vcap_open;
        ds_g4_times->src_parameter_type[4] = strdup("Vcap test");
        ds_g4_times->src_parameter[4] = vcap_test;
        ds_g4_times->src_parameter_type[5] = strdup("Vcap low");
        ds_g4_times->src_parameter[5] = vcap_low;
set_ds_source(ds_g4_times, "G4 life time");
if (all_block_above_g4)    set_ds_treatement(ds_g4_times,"all_block_above_g4, g4_pos = %f,tol_g4=%f,g4_significance=%f,take_last_g4=%d,take_last_oligo=%d,light_mode=%d",g4_pos,tol_g4,g4_significance,take_last_g4,take_last_oligo,light_mode);
else set_ds_treatement(ds_g4_times,"oligo_pos=%f,g4_pos = %f,tol_oligo=%f,tol_g4=%f,g4_significance=%f,take_last_g4=%d,take_last_oligo=%d,light_mode=%d",oligo_pos,g4_pos,tol_oligo,tol_g4,g4_significance,take_last_g4,take_last_oligo,light_mode);

snprintf(message_par,sizeof(message_par),"Zmag open => Force = %g pN"
,trk_zmag_to_force(0, zmag_open, g_r));
ds_block_times->src_parameter_type[0] = strdup(message_par);
        ds_block_times->src_parameter[0] = zmag_open;
snprintf(message_par,sizeof(message_par),"Zmag test => Force = %g pN"
,trk_zmag_to_force(0, zmag_test, g_r));
ds_block_times->src_parameter_type[1] = strdup(message_par);
        ds_block_times->src_parameter[1] = zmag_test;
snprintf(message_par,sizeof(message_par),"Zmag low => Force = %g pN"
,trk_zmag_to_force(0, zmag_low, g_r));
ds_block_times->src_parameter_type[2] = strdup(message_par);
        ds_block_times->src_parameter[2] = zmag_low;
        ds_block_times->src_parameter_type[3] = strdup("Vcap open");
        ds_block_times->src_parameter[3] = vcap_open;
        ds_block_times->src_parameter_type[4] = strdup("Vcap test");
        ds_block_times->src_parameter[4] = vcap_test;
        ds_block_times->src_parameter_type[5] = strdup("Vcap low");
        ds_block_times->src_parameter[5] = vcap_low;

        snprintf(message_par,sizeof(message_par),"Zmag open => Force = %g pN"
        ,trk_zmag_to_force(0, zmag_open, g_r));
        ds_beads_g4->src_parameter_type[0] = strdup(message_par);
                ds_beads_g4->src_parameter[0] = zmag_open;
        snprintf(message_par,sizeof(message_par),"Zmag test => Force = %g pN"
        ,trk_zmag_to_force(0, zmag_test, g_r));
        ds_beads_g4->src_parameter_type[1] = strdup(message_par);
                ds_beads_g4->src_parameter[1] = zmag_test;
        snprintf(message_par,sizeof(message_par),"Zmag low => Force = %g pN"
        ,trk_zmag_to_force(0, zmag_low, g_r));
        ds_beads_g4->src_parameter_type[2] = strdup(message_par);
                ds_beads_g4->src_parameter[2] = zmag_low;
                ds_beads_g4->src_parameter_type[3] = strdup("Vcap open");
                ds_beads_g4->src_parameter[3] = vcap_open;
                ds_beads_g4->src_parameter_type[4] = strdup("Vcap test");
                ds_beads_g4->src_parameter[4] = vcap_test;
                ds_beads_g4->src_parameter_type[5] = strdup("Vcap low");
                ds_beads_g4->src_parameter[5] = vcap_low;
        set_ds_source(ds_beads_g4, "G4 event off by bead");

        if (all_block_above_g4)    set_ds_treatement(ds_beads_g4,"all_block_above_g4, g4_pos = %f,tol_g4=%f,g4_significance=%f,take_last_g4=%d,take_last_oligo=%d,light_mode=%d",g4_pos,tol_g4,g4_significance,take_last_g4,take_last_oligo,light_mode);
        else set_ds_treatement(ds_beads_g4,"oligo_pos=%f,g4_pos = %f,tol_oligo=%f,tol_g4=%f,g4_significance=%f,take_last_g4=%d,take_last_oligo=%d,light_mode=%d",oligo_pos,g4_pos,tol_oligo,tol_g4,g4_significance,take_last_g4,take_last_oligo,light_mode);



        snprintf(message_par,sizeof(message_par),"Zmag open => Force = %g pN"
        ,trk_zmag_to_force(0, zmag_open, g_r));
        ds_beads_oligo->src_parameter_type[0] = strdup(message_par);
                ds_beads_oligo->src_parameter[0] = zmag_open;
        snprintf(message_par,sizeof(message_par),"Zmag test => Force = %g pN"
        ,trk_zmag_to_force(0, zmag_test, g_r));
        ds_beads_oligo->src_parameter_type[1] = strdup(message_par);
                ds_beads_oligo->src_parameter[1] = zmag_test;
        snprintf(message_par,sizeof(message_par),"Zmag low => Force = %g pN"
        ,trk_zmag_to_force(0, zmag_low, g_r));
        ds_beads_oligo->src_parameter_type[2] = strdup(message_par);
                ds_beads_oligo->src_parameter[2] = zmag_low;
                ds_beads_oligo->src_parameter_type[3] = strdup("Vcap open");
                ds_beads_oligo->src_parameter[3] = vcap_open;
                ds_beads_oligo->src_parameter_type[4] = strdup("Vcap test");
                ds_beads_oligo->src_parameter[4] = vcap_test;
                ds_beads_oligo->src_parameter_type[5] = strdup("Vcap low");
                ds_beads_oligo->src_parameter[5] = vcap_low;
        set_ds_source(ds_beads_oligo, "G4 event on by bead");
        if (all_block_above_g4)    set_ds_treatement(ds_beads_oligo,"all_block_above_g4, g4_pos = %f,tol_g4=%f,g4_significance=%f,take_last_g4=%d,take_last_oligo=%d,light_mode=%d",g4_pos,tol_g4,g4_significance,take_last_g4,take_last_oligo,light_mode);
        else set_ds_treatement(ds_beads_oligo,"oligo_pos=%f,g4_pos = %f,tol_oligo=%f,tol_g4=%f,g4_significance=%f,take_last_g4=%d,take_last_oligo=%d,light_mode=%d",oligo_pos,g4_pos,tol_oligo,tol_g4,g4_significance,take_last_g4,take_last_oligo,light_mode);



        set_ds_source(ds_removed_beads, "Removed beads");
        if (all_block_above_g4)    set_ds_treatement(ds_removed_beads,"all_block_above_g4, g4_pos = %f,tol_g4=%f,g4_significance=%f,take_last_g4=%d,take_last_oligo=%d,light_mode=%d",g4_pos,tol_g4,g4_significance,take_last_g4,take_last_oligo,light_mode);
        else set_ds_treatement(ds_removed_beads,"oligo_pos=%f,g4_pos = %f,tol_oligo=%f,tol_g4=%f,g4_significance=%f,take_last_g4=%d,take_last_oligo=%d,light_mode=%d",oligo_pos,g4_pos,tol_oligo,tol_g4,g4_significance,take_last_g4,take_last_oligo,light_mode);







    }

    //  win_printf("z_or_f %d",z_or_f);

    for (i = 0; all_bead > 0 && i < g_r->n_bead; i++)
    {
        do_bead[i] = 1;
    }

    bead_s = (bead_s < 0) ? 0:bead_s;
    bead_e = (bead_e >= g_r->n_bead) ? g_r->n_bead-1 :bead_e;

    for (i = bead_s; (bead_range == 1) && (all_bead == 0) && (i < bead_e + 1); i++)
    {
        do_bead[i] = 1;
    }

    //all_bead = 0;
    for (i = 0; (all_bead == 0) && (bead_range == 0) && (i < g_r->n_bead); i++)
    {
      i_bead=-1;
      if (add_subset) do_bead[i]=0;
      else do_bead[i]=1;
      strcpy(temp_set,local_set);
      for (token = strtok(temp_set, ","); token != NULL; token = strtok(NULL, ","))
          {

              i_bead = atoi(token);
          if (i_bead==i)
          {
            if (add_subset) do_bead[i]=1;
            else do_bead[i]=0;
          }

          }
    }
    if (i_bead==-1) return(win_printf_OK("You didn't choose any beads\n"));

      for (i = 0; all_bead == 0 && i < g_r->n_bead; i++) printf("do_bead[%d]=%d\n",i,do_bead[i]);

    for (i = 0, npt = 0; i <= ph_max; i++)
    {
        if (do_ph[i] != 1)
        {
            continue;
        }


        for (k = min, start_index = 0, ph_nx[i] = 0; k <= max; k++)
        {
            ending = 0;  // phase 5 is hybridization
            done = start_index = im_array_k_ph[k][i][0];
            ending = im_array_k_ph[k][i][1];

            if ((done >= 0) && ((ending - start_index) > ph_nx[i]))
            {
                ph_nx[i] = ending - start_index;
		ph_str[i] = start_index;
		ph_end[i] = ending;
            }
        }

        npt += ph_nx[i];
        if (phs[0] == '\0')
        {
            snprintf(phs, sizeof(phs), "[%d", i);
        }
        else
        {
            snprintf(phs + strlen(phs), sizeof(phs) - strlen(phs), ",%d", i);
        }

    }

    npt += 5;
    snprintf(phs + strlen(phs), sizeof(phs) - strlen(phs), "]");
    snprintf(phs_pos + strlen(phs_pos), sizeof(phs_pos) - strlen(phs_pos), "; ");
    //  win_printf("total %d start %d end %d",npt,start_index, ending);
    //we create one plot for each bead
    last_treated_bd = -2;
    t0 = my_uclock() + (get_my_uclocks_per_sec() / 10);
    if (gen_fopen_op)
    {
        fopen_op = create_and_attach_one_plot(pr, g_r->n_bead, g_r->n_bead, 0);
        ds_fopen = fopen_op->dat[0];
	ds_fclose = create_and_attach_one_ds(fopen_op, g_r->n_bead, g_r->n_bead, 0);
	ds_fopens = create_and_attach_one_ds(fopen_op, 16, 16, 0);
	if (ds_fopens) ds_fopens->nx = ds_fopens->ny = 0;
	ds_fcloses = create_and_attach_one_ds(fopen_op, 16, 16, 0);
	if (ds_fcloses) ds_fcloses->nx = ds_fcloses->ny = 0;
    }

    brf = (fixed_bd < 0 && fixed_bd < g_r->n_bead) ? NULL : g_r->b_r[fixed_bd];
    if(brf != NULL && (filter_fixed == 1 || fixed_beads[1] != -1))
    {
        memcpy(&filtered, brf, sizeof(b_record));
        addresses = (float**) malloc(g_r->n_page*sizeof(float*));
        filtdata  = (float*)  calloc(g_r->n_page*g_r->page_size, sizeof(float));

        filtered.z = addresses;
        for(i = 0; i < g_r->n_page; ++i)
            filtered.z[i] = filtdata+i*g_r->page_size;

        for(no = 0; no < (int) (sizeof(fixed_beads)/sizeof(int)) && fixed_beads[no] != -1; ++no)
        {
            brf = g_r->b_r[fixed_beads[no]];
            for(i = 0; i < g_r->n_page; ++i)
                for(j = 0; j < g_r->page_size; ++j)
                    filtered.z[i][j] += brf->z[i][j];
        }

        if(no > 1)
        {
            tmp = 1.0f/no;
            for(i = 0; i < g_r->abs_pos; ++i)
                filtdata[i] *= tmp;
        }

#ifdef OLD // compilation issue with xvnonlin_run
        if(filter_fixed)
            xvnonlin_run(g_r->abs_pos, filtdata);
#endif
        brf = &filtered;
    }

   if (g4_analysis)  set_plot_title(op_g4, "\\stack{{G4 Plot}{\\pt7 Cycles:[%d,%d]  phase(s) %s}}",min,max,phs);


    for (i = 0, op = NULL; i < g_r->n_bead; i++)
    {
        // start loop on beads
        if (all_bead == 0 && do_bead[i] != 1)
        {
            continue;
        }



        if (g4_analysis)
        {

          first_g4_time_for_this_bead = 1;
          first_block_time_for_this_bead = 1;
          g4_status = 0;
          is_g4 = 0;
          block_points_bf_g4 = 0;
          begin_g4 = 0;
          end_g4 = 0;
          last_seen_g4 = 0;
          add_new_point_to_ds(ds_beads_oligo,i+g_r->start_bead,0);
          add_new_point_to_ds(ds_beads_g4,i+g_r->start_bead,0);
          add_new_point_to_ds(ds_removed_beads,i+g_r->start_bead,0);

          //nb_g4_off_for_this_bead = 0;
          //nb_g4_on_for_this_bead = 0;

        }
#       ifdef __cplusplus
        if(on_do_one_bead(script, i) == WIN_CANCEL)
            continue;
#       endif

        br = g_r->b_r[i];

        //win_printf("doing bead %d",i);
        if (g_r->b_r[i]->completely_losted & COMPLETELY_LOST)
        {
            continue;
        }

        if (g_r->b_r[i]->calib_im == NULL && g_r->SDI_mode == 0)
        {
            continue;
        }
        if (i == fixed_bd)
        {
            continue;
        }

        //bt = br->b_t;
        //if (bt->calib_im == NULL)   continue;
        f_unzip_mean = 0;
        f_unzip_n = 0;

        //win_printf("starting bead %d",i);
        if (op == NULL)
        {
            new_plot = 1;
        }
        else if (op->n_dat == 1 && op->dat[0]->nx == 0)
        {
            new_plot = 0;
        }
        else if (same_op == 0)
        {
            new_plot = 1;
        }
        else
        {
            new_plot = 0;
        }

        if (new_plot)
        {
            npt = (npt) ? npt : 1;
            op = create_and_attach_one_plot(pr, npt, npt, 0);
            if (op == NULL) return win_printf_OK("Cannot allocate plot of size %d",npt);
            ds = op->dat[0];
            if (g4_analysis){
            op->user_vspare[0] = (void *) ds_g4_times;
            op->user_vspare[1] = (void *) ds_block_times;
            op->user_vspare[2] = (void *) op_g4;
            op->user_vspare[3] = (void *) ds_beads_g4;
            op->user_vspare[4] = (void *) ds_beads_oligo;
            op->user_vspare[5] = (void *) ds_removed_beads;
            op->user_ispare[4] = 42567;

          }
            ds->nx = ds->ny = 0;
            strcpy(cctmp, "\\mu m");
            create_attach_select_y_un_to_op(op, IS_METER, 0 , (float)1, -6, 0, cctmp);

            if (z_or_f < 1 || z_or_f > 2)
            {
                strcpy(cctmp, "s"); // "\\mu m"
                create_attach_select_x_un_to_op(op, IS_SECOND, 0
                                        , (float)1 / g_r->Pico_param_record.camera_param.camera_frequency_in_Hz, 0, 0, cctmp);
            }
        }

        unzip_ds_s = op->n_dat - 1;
        unzip_ds_s = (unzip_ds_s < 0) ? 0 : unzip_ds_s;

        if (dzdtst == 1) // construct a separate plot with the bump positions
        {
            if (opt == NULL || same_op == 0)
            {
                opt = create_and_attach_one_plot(pr, 16, 16, 0);
                dst = opt->dat[0];
                dst->nx = dst->ny = 0;
                dstd = create_and_attach_one_ds(opt, 16, 16, 0);
                dstd->nx = dstd->ny = 0;
            }

            if (dst == NULL && opt != NULL)
            {
                dst = create_and_attach_one_ds(opt, 16, 16, 0);
                dst->nx = dst->ny = 0;
                strcpy(cctmp,"\\pt5\\oc");
                set_plot_symb(dst, cctmp);
            }

            if (dstd == NULL && opt != NULL)
            {
                dstd = create_and_attach_one_ds(opt, 16, 16, 0);
                dstd->nx = dstd->ny = 0;
                strcpy(cctmp,"\\pt5\\di");
                set_plot_symb(dstd, cctmp);
            }
        }

        if (dzdtst == 2)  // construct datasets for bump positions
        {
            dst = build_data_set(16, 16);
            dst->nx = dst->ny = 0;
            strcpy(cctmp,"\\pt5\\oc");
            set_plot_symb(dst, cctmp);
            dstd = build_data_set(16, 16);
            dstd->nx = dstd->ny = 0;
            strcpy(cctmp,"\\pt5\\di");
            set_plot_symb(dstd, cctmp);
        }

        for (k = 0; k < 32; k++)
        {
            T_ph[k] = nT_ph[k] = 0;
        }



        for (k = min, start_index = 0; k <= max; k++)
        {
            // loop start on cycles
            phi0s = 0;

            done = phi0s = im_array_k_ph[k][1][0];
            phi0e = im_array_k_ph[k][1][1];

            //if (k == (min+1))
            //win_printf("k = %d discri_low %d Phi0s %d done %d",k,discri_low,phi0s,done);
            if (done >= 0)
            {
                T_ph[1] += phi0e - phi0s;
                nT_ph[1]++;
            }
            mol_ext = 0;
            start_index = 0;
            done = start_index = im_array_k_ph[k][discri_low][0];
            ending = im_array_k_ph[k][discri_low][1];
            // Max_close = 0; i think this is if useless {
              /* code */

            //
            // if (done >= 0)
            //     find_bead_z_by_max_gaussian_histo_during_part_trk_tolerant(g_r, i, start_index,  ending - start_index,
            //             tolerant, 0.001 * biny, &xavg1, &yavg1, &zavg1,
            //             &zobj, &profile_invalid);
            //
            // Max_close = g_r->z_cor * zavg1;
            start_index = 0;
            done = start_index = im_array_k_ph[k][discri_high][0];
            ending = im_array_k_ph[k][discri_high][1];
            if (done >= 0)
            {
                average_param_during_part_trk(g_r, start_index, ending - start_index, &zmag_open, &rot, &zobj, &param_cst);
		average_Vcap_during_part_trk(g_r, start_index, ending - start_index, &zmagt, &vcap_open, &param_cst);
            }

            start_index = 0;
            done = start_index = im_array_k_ph[k][5][0];
            ending = im_array_k_ph[k][5][1];

            if (done >= 0)
            {
                average_param_during_part_trk(g_r, start_index, ending - start_index, &zmag_test, &rot, &zobj, &param_cst);
		average_Vcap_during_part_trk(g_r, start_index, ending - start_index, &zmagt, &vcap_test, &param_cst);
            }

            if (done >= 0)
            {
                T_ph[5] += ending - start_index;
                nT_ph[5]++;
            }

            if (ph_max > 7)
            {
                start_index = 0;
                done = start_index = im_array_k_ph[k][7][0];
                ending = im_array_k_ph[k][7][1];
                zmag_low = 0;

                if (done >= 0)
                {
                    T_ph[7] += ending - start_index;
                    nT_ph[7]++;
                }

                if (done >= 0)
                {
                    average_param_during_part_trk(g_r, start_index, ending - start_index, &zmag_low, &rot, &zobj, &param_cst);
		    average_Vcap_during_part_trk(g_r, start_index, ending - start_index, &zmagt, &vcap_low, &param_cst);
                }
            }

            if (discri_low >= 0 && discri_low <= ph_max && discri_high >= 0 && discri_high <= ph_max)
            {
                // test the molecule extension between phases high and low phases
                done = start_index = im_array_k_ph[k][discri_low][0];
                ending = im_array_k_ph[k][discri_low][1];

                if (done < 0)
                {
                    continue;    // we grab ref phase
                }

                /*
                average_bead_z_during_part_trk_tolerant(g_r, i, start_index, ending - start_index,tolerant,
                                 &xavg1, &yavg1, &zavg1, &zobj, &profile_invalid);
                */
  //find_bead_z_by_max_gaussian_histo_during_part_trk_tolerant(g_r, i, start_index,  ending - start_index,
    //                    tolerant, 0.001 * biny, &xavg1, &yavg1, &zavg1,
      //Ò                  &zobj, &profile_invalid);
                average_bead_z_during_part_trk_tolerant(g_r, i, start_index,  ending - start_index,
                                    tolerant, &xavg1, &yavg1, &zavg1,
                                      &zobj, &profile_invalid);
                //if (k == (min+1))
                //win_printf("k = %d discri_low %d Phi0s %d start_index %d",k,discri_low,phi0s,start_index);
                Max_close = tmp = zavg1; // we should use an histogramme averaging // zmol_hi =
                done = start_index = im_array_k_ph[k][discri_high][0];
                ending = im_array_k_ph[k][discri_high][1];

                if (done < 0)
                {
                    continue;    // we grab ref phase
                }


                average_bead_z_during_part_trk_tolerant(g_r, i, start_index, ending - start_index,tolerant,
                                 &xavg1, &yavg1, &zavg1, &zobj, &profile_invalid);

                //find_bead_z_by_max_gaussian_histo_during_part_trk_tolerant(g_r, i, start_index,  ending - start_index,
                    //    tolerant, 0.001 * biny, &xavg1, &yavg1, &zavg1,
                      //  &zobj, &profile_invalid);
                //zmol_lo = zavg1;
                tmp = g_r->z_cor * (zavg1 - tmp);
                mol_ext = tmp;

                if (tmp < hi_low_discri)
                {
                    // if (g4_analysis) {
                    //   for (int i_ph2 = 0; i_ph2 <= ph_max ; i_ph2 ++) g4_status[k][i_ph2] |= G4_BAD_CYCLE;
                    // }
                    continue;    // if the molecule extension between phases high and low is not sufficient skip this cycle
                }
            }

            if (bigger_or_smaller < 2)
            {
                // we test if the extension between phase 5 and 1 exceeds a theshold
                average_bead_z_during_part_trk_tolerant(g_r, i, phi0s, phi0e - phi0s, tolerant,
                                                        &xavg1, &yavg1, &zavg1, &zobj, &profile_invalid);
                tmp = zavg1; // the beginning of phase 5 is not properly defined
                done = start_index = im_array_k_ph[k][5][0];
                ending = im_array_k_ph[k][5][1];
                average_bead_z_during_part_trk_tolerant(g_r, i, start_index, phi0e - phi0s, tolerant,
                                                        &xavg1, &yavg1, &zavg1, &zobj, &profile_invalid);
                tmp = zavg1 - tmp;

                if ((bigger_or_smaller == 0) && (tmp < activ_thres))
                {
                    continue;    // if condition not fullfil
                }

                if ((bigger_or_smaller == 1) && (tmp > activ_thres))
                {
                    continue;    // we skip the cycle
                }
            }
            if (ref_ph >= 0)
            {
                // we compute the reference Z position
                done = start_index = im_array_k_ph[k][ref_ph][0];
                ending = im_array_k_ph[k][ref_ph][1];

                if (done < 0)
                {
                    continue;    // we grab ref phase
                }

                if (restrict_mode == 0)  // ref phase is defined on a restricted number of points
                {
                    ph1s = start_index + skip_bef;
                    ph1e = ending - skip_after;

                    if ((k == min) && (ph1e - ph1s <= 0))
                    {
                        error_message("Pb in averaring window = start %d end %d", ph1s, ph1e);
                    }

                    // we average the ref signal
                    if (fixed_bd >= 0)   average_bead_z_minus_fixed_during_part_trk_tolerant(g_r, i, fixed_bd, ph1s,
                                ph1e - ph1s, tolerant, &xavg1,
                                &yavg1, &zavg1, &zobj, &profile_invalid);
                    else
                    {

                        average_bead_z_during_part_trk_tolerant(g_r, i, ph1s, ph1e - ph1s,tolerant,
                                          &xavg1, &yavg1, &zavg1, &zobj, &profile_invalid);

                        //find_bead_z_by_max_gaussian_histo_during_part_trk_tolerant(g_r, i, ph1s,  ph1e - ph1s,
                          //      tolerant, 0.001 * biny, &xavg1, &yavg1, &zavg1,
                            //    &zobj, &profile_invalid);
                    }

                    zavg1 *= g_r->z_cor;
                }
                else     // ref phase is defined on a restriccted Zmag or Force
                {
                    ph1s = start_index;
                    ph1e = ending;

                    for (j = ph1s, zavg1 = 0, navg = 0; j < ph1e; j++)
                    {
                        page_n = j / g_r->page_size;
                        i_page = j % g_r->page_size;

                        if ((tolerant == 0) && (is_bead_profile_good(g_r,br->profile_index[page_n][i_page]) == 0))
                        {
                          win_printf_OK("not good...");
                            continue;
                        }

                        tmp = g_r->z_cor * br->z[page_n][i_page];

                        if (brf != NULL)
                        {
                            tmp -= g_r->z_cor * brf->z[page_n][i_page];
                        }

                        tmpx = zmag_valid(g_r, j);//g_r->zmag[page_n][i_page];

                        if (restrict_mode == 2)
                        {
                            tmpx = trk_zmag_to_force(0, tmpx, g_r);
                        }

                        if (tmpx > r_max || tmpx < r_min)
                        {
                            continue;
                        }

                        zavg1 += tmp;
                        navg++;
                    }

                    if (navg)
                    {
                        zavg1 /= navg;
                    }
                    else
                    {
                        if (verbose != WIN_CANCEL)
                        {
                            verbose = win_printf("Zero averaging in cycle %d", k);
                        }
                    }
                }
            }
            else
            {
                zavg1 = 0;
            }
	    int last_phi = 0;
	    for (i_ph = 0; i_ph <= ph_max; i_ph++)
	      {
		if (do_ph[i_ph] != 1)
		  {
		    continue;
		  }
		ending = 0;  // phase 5 is hybridization
    done = start_index = im_array_k_ph[k][i_ph][0];
    ending = im_array_k_ph[k][i_ph][1];
		if (done >= 0)
		  {
		    //ph_nx[i_ph] = ending - start_index;
		    ph_str[i_ph] = start_index;
		    ph_end[i_ph] = ending;
		  }
		last_phi = i_ph;
	      }

	    int first_ph = 0, first_phi = 0;
	    for (i_ph = 0; i_ph <= ph_max; i_ph++)
	      {
		if (do_ph[i_ph] != 1)
		  {
		    continue;
		  }
		if (first_ph == 0)
		  {
		    first_phi = i_ph;
		    snprintf(phs_pos, sizeof(phs_pos), "starting = %d extending = %d with reference = %d\nPhase[%d] = [0,%d]"
			     ,ph_str[i_ph],ph_end[last_phi]-ph_str[first_phi],phi0s,i_ph,ph_end[i_ph]-ph_str[i_ph]-1);
		  }
		else
		  {
		    snprintf(phs_pos + strlen(phs_pos), sizeof(phs_pos) - strlen(phs_pos)
			     ,"%sPhase[%d] = [%d,%d]",(first_ph == 4)?"\n":" ,",i_ph
			     , ph_str[i_ph]-ph_str[first_phi]
			     , ph_end[i_ph]-ph_str[first_phi]-1);
		  }
		first_ph++;

	      }

	    snprintf(phs_pos + strlen(phs_pos), sizeof(phs_pos) - strlen(phs_pos), ";\n");

            for (i_ph = 0, last_bump = 0, last_bump2 = 0; i_ph <= ph_max; i_ph++)
            {
                // start loop on phases
                if (do_ph[i_ph] != 1)
                {
                    continue;    // we keep only selected phases
                }
                g4_pts = 0;
                oligo_pts = 0;
                no_block_pts = 0;
                g4_status = 0;
                last_seen_g4_in_phase = 0;

                ending = 0;
                done = start_index = im_array_k_ph[k][i_ph][0];
                ending = im_array_k_ph[k][i_ph][1];

                if (done < 0)
                {
                    continue;    // we grab ref phase
                }

                done = is_bead_profile_in_im_range_good(g_r, i, start_index, ending - start_index);

                if (3 * done < 2 * (ending - start_index))
                {
                    continue;    // bead lost
                }

                if (my_uclock() > t0)
                {
		  //strcpy(cctmp,"bead %d evt %d ph %d done %d start %d end %d");
                    my_set_window_title("bead %d evt %d ph %d done %d start %d end %d", i, k, i_ph, done, start_index, ending);
                    t0 = my_uclock() + (get_my_uclocks_per_sec() / 10);
                }

                if (ds == NULL)
                {
                    if ((ds = create_and_attach_one_ds(op, ph_nx[i_ph], ph_nx[i_ph], 0)) == NULL)
                    {
                        return win_printf_OK("I can't create plot !");
                    }

                    ds->nx = ds->ny = 0;
                }

                //strcpy(cctmp,"Bead Cycle %d phase(s): %s %s tracking at xx Hz Acquistion %d for bead %d \n "
		//            "Z coordinate l = xx, w = xx, nim = xx\n");
                set_ds_source(ds, "Bead Cycle %d phase(s): %s %s tracking at xx Hz Acquistion %d for bead %d \n "
                              "Z coordinate l = xx, w = xx, nim = xx\n"
			      , k, phs, phs_pos, g_r->n_rec, i);
                ds->src_parameter_type[0] = strdup("Cycle nb.");;
                ds->src_parameter[0] = k;
                ds->src_parameter_type[1] = strdup("Bead nb.");;
                ds->src_parameter[1] = i;
		char message_par[128] = {0};
		snprintf(message_par,sizeof(message_par),"Zmag open => Force = %g pN"
			 ,trk_zmag_to_force(0, zmag_open, g_r));
			 ds->src_parameter_type[2] = strdup(message_par);
                ds->src_parameter[2] = zmag_open;
		snprintf(message_par,sizeof(message_par),"Zmag test => Force = %g pN"
			 ,trk_zmag_to_force(0, zmag_test, g_r));
			 ds->src_parameter_type[3] = strdup(message_par);
                ds->src_parameter[3] = zmag_test;
		snprintf(message_par,sizeof(message_par),"Zmag low => Force = %g pN"
			 ,trk_zmag_to_force(0, zmag_low, g_r));
		ds->src_parameter_type[4] = strdup(message_par);
                ds->src_parameter[4] = zmag_low;
                ds->src_parameter_type[5] = strdup("Vcap open");
                ds->src_parameter[5] = vcap_open;
                ds->src_parameter_type[6] = strdup("Vcap test");
                ds->src_parameter[6] = vcap_test;
                ds->src_parameter_type[7] = strdup("Vcap low");
                ds->src_parameter[7] = vcap_low;



                if (dump_g4_plots && g4_analysis)
                {
                  if (dsg41 == NULL)
                  {
                    if ((dsg41 = create_and_attach_one_ds(op, ph_nx[i_ph], ph_nx[i_ph], 0)) == NULL)
                  {
                      return win_printf_OK("I can't create plot !");

                  }

                  set_ds_source(dsg41, "Oligo status : Bead Cycle %d phase(s): %s %s tracking at xx Hz Acquistion %d for bead %d \n "
                                "Z coordinate l = xx, w = xx, nim = xx\n"
  			      , k, phs, phs_pos, g_r->n_rec, i);

                  dsg41->nx = dsg41->ny = 0;
                }

                  if (dsg42 == NULL)
                  {
                  if ((dsg42 = create_and_attach_one_ds(op, ph_nx[i_ph], ph_nx[i_ph], 0)) == NULL)
                  {
                      return win_printf_OK("I can't create plot !");
                  }
                  set_ds_source(dsg42, "G4 status : Bead Cycle %d phase(s): %s %s tracking at xx Hz Acquistion %d for bead %d \n "
                                "Z coordinate l = xx, w = xx, nim = xx\n"
  			      , k, phs, phs_pos, g_r->n_rec, i);


                  dsg42->nx = dsg42->ny = 0;
                }
                if (dsg43 == NULL)
                {
                  if ((dsg43 = create_and_attach_one_ds(op, ph_nx[i_ph], ph_nx[i_ph], 0)) == NULL)
                  {
                      return win_printf_OK("I can't create plot !");
                  }
                  set_ds_source(dsg43, "Bad cycle : Bead Cycle %d phase(s): %s %s tracking at xx Hz Acquistion %d for bead %d \n "
                                "Z coordinate l = xx, w = xx, nim = xx\n"
              , k, phs, phs_pos, g_r->n_rec, i);

                  dsg43->nx = dsg43->ny = 0;
                }

                if (dsg44 == NULL)
                {
                  if ((dsg44 = create_and_attach_one_ds(op, ph_nx[i_ph], ph_nx[i_ph], 0)) == NULL)
                  {
                      return win_printf_OK("I can't create plot !");
                  }

                  dsg44->nx = dsg44->ny = 0;
                }
              }




                zd[0] =   zd[1] =   zd[2] = 0;
                md[0] =   md[1] =   md[2] = 0;

                for (j = start_index + skip_disp_s, ji = 0, no = 0; done >= 0 && j < ending - skip_disp_e; j++)
                {

                    // TO BE CHECKED skip_disp_s/e are used for all phases !

                    page_n = j / g_r->page_size;
                    i_page = j % g_r->page_size;
                    tmp = g_r->z_cor * br->z[page_n][i_page];

                    if (brf != NULL)
                    {
                        tmp -= g_r->z_cor * brf->z[page_n][i_page];
                    }

                    tmp -= zavg1;

                    if (z_or_f == 0) tmpx = (g_r->timing_mode == 1) ? (g_r->imi[page_n][i_page] - g_r->imi_start) :
                                                g_r->imi[page_n][i_page];
                    else if (z_or_f == 1)
                    {
                        tmpx = zmag_valid(g_r, j);    //g_r->zmag[page_n][i_page];
                    }
                    else if (z_or_f == 2)
                    {
                        tmpx = trk_zmag_to_force(0, zmag_valid(g_r, j), g_r);
                    }
                    else if (z_or_f == 3)
                    {
                        tmpx = j - start_index;
                    }
                    else if (z_or_f == 4)
                    {
                        tmpx = (float)(j - ph_str[phase_align]);//phi0s);
                    }

                    if (z_or_zmag == 1)
                    {
                        tmp = zmag_valid(g_r, j);    //g_r->zmag[page_n][i_page];
                    }

                    if ((z_or_zmag == 0) && (tolerant == 0)
                            && (is_bead_profile_good(g_r,br->profile_index[page_n][i_page]) == 0))
                    {
                        continue;
                    }

                    if ((z_or_zmag == 0) && (tolerant == 0) && (brf != NULL)
                            && (is_bead_profile_good(g_r,brf->profile_index[page_n][i_page]) == 0))
                    {
                        continue;
                    }

                    if (g4_analysis)
                     {
                       if ((i_ph == 5) || (i_ph == 1))
                       {

                         if (all_block_above_g4) // take all points above the g4
                         {
                         if (tmp > g4_pos + tol_g4)
                        {
                         oligo_pts += 1;
                         if (is_g4 == 0)  block_points_bf_g4 += 1;
                         }
                        }


                        else { // take blockages around predefined oligo pos
                         if ((oligo_pos-tol_oligo<tmp) &&  (tmp<oligo_pos+tol_oligo))
                         {
                           oligo_pts += 1;
                           if (is_g4 == 0) block_points_bf_g4 += 1;
                         }
                       }

                         if ((g4_pos-tol_g4<tmp) &&  (tmp<g4_pos+tol_g4))
                         {
                           g4_pts += 1;
                           last_seen_g4_in_phase = j;
                         }

                         if ((i_ph == 5)&&(tmp<g4_pos-tol_g4)) no_block_pts ++;
                       }
                     }



                    if (light_mode == 0) add_new_point_to_ds(ds, tmpx , tmp);
                    else if ((light_mode == 1) && (j%2 == 0)) add_new_point_to_ds(ds, tmpx , tmp);
                    else if ((light_mode == 2) && (j%3 == 0)) add_new_point_to_ds(ds, tmpx , tmp);
                    else if ((light_mode == 3) && (j%4 == 0)) add_new_point_to_ds(ds, tmpx , tmp);


//
  //                  if (ji == 0) pos_min = ds->nx -1;

                    if (ds->nx > 3)
                    {
                        zd[2] = ds->yd[ds->nx - 1] - ds->yd[ds->nx - 2];
                        md[2] = (ds->xd[ds->nx - 1] + ds->xd[ds->nx - 2]) / 2; //(tmpx + tmpx_1)/2;
                        zd[1] = ds->yd[ds->nx - 2] - ds->yd[ds->nx - 3];
                        md[1] = (ds->xd[ds->nx - 2] + ds->xd[ds->nx - 3]) / 2; //(tmpx + tmpx_1)/2;
                        zd[0] = ds->yd[ds->nx - 3] - ds->yd[ds->nx - 4];
                        md[0] = (ds->xd[ds->nx - 3] + ds->xd[ds->nx - 4]) / 2; //(tmpx + tmpx_1)/2;




                        //md[2] = (tmpx + tmpx_1)/2;
                        if (find_bump < 3) // simple bump detection
                        {
                            if (((dzph_bump * zd[1]) > (dzph_bump * dzph_bump))
                                    && ((zd[1] - zd[0]) * (zd[1] - zd[2]) > 0))
                            {
                                // rising edge
                                last_bump = 1;
                                x_last_bump = md[1];
                                y_last_bump = (ds->yd[ds->nx - 4] + ds->yd[ds->nx - 1]) / 2;

                                if (dzdtst)
                                {
                                    add_new_point_to_ds(dst, md[1] , (ds->yd[ds->nx - 4] + ds->yd[ds->nx - 1]) / 2);    //dzph_bump/fabs(dzph_bump));
                                }

                                sprintf(st, "\\pt7\\center{%d}", no++);

                                if (find_bump == 1)
                                {
				  set_ds_plot_label(ds, md[1], (ds->yd[ds->nx - 4] + ds->yd[ds->nx - 1]) / 2, USR_COORD, "%s", st);
                                } //"\\center{\\oc}");

                                if (z_or_f == 2)
                                {
                                    f_unzip_mean += md[1];
                                    f_unzip_n++;
                                }
                            }

                            if (((dzph2_bump * zd[1]) > (dzph2_bump * dzph2_bump))
                                    && ((zd[1] - zd[0]) * (zd[1] - zd[2]) > 0))
                            {
                                // falling edge
                                last_bump2 = 1;
                                x_last_bump2 = md[1];
                                y_last_bump2 = (ds->yd[ds->nx - 4] + ds->yd[ds->nx - 1]) / 2;

                                if (dzdtst)
                                {
                                    add_new_point_to_ds(dstd, md[1] , (ds->yd[ds->nx - 4] + ds->yd[ds->nx - 1]) / 2);    //dzph_bump/fabs(dzph_bump));
                                }

                                sprintf(st, "\\pt7\\center{%d}", no++);

                                if (find_bump == 1)
				  set_ds_plot_label(ds, md[1], (ds->yd[ds->nx - 4] + ds->yd[ds->nx - 1]) / 2, USR_COORD, "%s",st); //"\\center{\\oc}");
                            }
                          }



                    if (find_bump == 4)
                    {
                        // we handle the last jump
                        if (((dzph_bump * zd[1]) > (dzph_bump * dzph_bump))
                                && ((zd[1] - zd[0]) * (zd[1] - zd[2]) > 0))
                        {
                            last_bump = 1;
                            x_last_bump = md[1];
                            y_last_bump = (ds->yd[ds->nx - 4] + ds->yd[ds->nx - 1]) / 2;
                        }

                        if (((dzph2_bump * zd[1]) > (dzph2_bump * dzph2_bump))
                                && ((zd[1] - zd[0]) * (zd[1] - zd[2]) > 0))
                        {
                            last_bump2 = 1;
                            x_last_bump2 = md[1];
                            y_last_bump2 = (ds->yd[ds->nx - 4] + ds->yd[ds->nx - 1]) / 2;
                        }
                    }

                    if (ji > 1)
                    {
                        md[0] = md[1];
                    }

                    if (ji > 2)
                    {
                        md[1] = md[2];
                    }

                    ji++;
                  }
                    //tmp_1 = tmp;
                    //tmpx_1 = tmpx;
                    //if (same_ds == 0) ds = NULL;




                } // end loop on the points of the current phase

                if (g4_analysis)
                {
            //    pos_max = ds->nx -1;
                    //tmp_j = j;
                ph_k_nb_points = j - start_index - skip_disp_s;

                if (oligo_pts > ph_k_nb_points*g4_significance) g4_status |= IS_PH_OLIGO;
                if (g4_pts > ph_k_nb_points*g4_significance) g4_status |= IS_PH_G4;
                if (no_block_pts >  ph_k_nb_points*g4_significance) g4_status |= IS_PH_NO_BLOCK;

                if (g4_status & IS_PH_G4) {

                        if (is_g4 == 0)
                            {
                                is_g4 = 1;
                                j = start_index;
                                page_n = j / g_r->page_size;
                                i_page = j % g_r->page_size;
                                begin_g4 = (g_r->timing_mode == 1) ? (g_r->imi[page_n][i_page] - g_r->imi_start) :
                                              g_r->imi[page_n][i_page];


                                if (first_block_time_for_this_bead)
                                {
                                  op->user_ispare[2] = ds_block_times->nx;
                                  first_block_time_for_this_bead = 0;
                                }
                                op->user_ispare[3] = ds_block_times->nx;
                                if (1) {
                                add_new_point_to_ds(ds_block_times,i+g_r->start_bead,block_points_bf_g4);

                                ds_beads_oligo->yd[ds_beads_oligo->ny-1] += 1;
                              }
                                block_points_bf_g4 = 0;
                                  }
                        if (is_g4 == 1)
                        {

                          page_n = last_seen_g4_in_phase / g_r->page_size;
                          i_page = last_seen_g4_in_phase % g_r->page_size;
                          last_seen_g4 = (g_r->timing_mode == 1) ? (g_r->imi[page_n][i_page] - g_r->imi_start) :
                                        g_r->imi[page_n][i_page];

                        }
                  }

                  if (i_ph == 5) {
                  if ((is_g4 == 1) && (g4_status & IS_PH_NO_BLOCK) && !(g4_status & IS_PH_G4))  {
                      block_points_bf_g4 = 0;
                      is_g4 = 0;
                      end_g4 = last_seen_g4;
                      if (first_g4_time_for_this_bead)
                      {
                        op->user_ispare[0] = ds_g4_times->nx;
                        first_g4_time_for_this_bead = 0;
                      }
                      op->user_ispare[1] = ds_g4_times->nx;
                      add_new_point_to_ds(ds_g4_times,i+g_r->start_bead,(end_g4-begin_g4));

                      ds_beads_g4->yd[ds_beads_g4->ny-1] += 1;
                      begin_g4 = 0;
                      end_g4 = 0;

                    }
                  }
                // dsg4temp=duplicate_partial_data_set(ds,pos_min,pos_max);
                // ds_histo=histo_background(dsg4temp,bin_g4_analysis);

                // g4_ki |= IS_PH_STRANGE_PICK;


                if ( dump_g4_plots && (z_or_f == 0) )
                {
                  j = start_index + skip_disp_s;
                    {
                      page_n = j / g_r->page_size;
                      i_page = j % g_r->page_size;
                      tmpx = (g_r->timing_mode == 1) ? (g_r->imi[page_n][i_page] - g_r->imi_start) :
                                                  g_r->imi[page_n][i_page];
                      add_new_point_to_ds(dsg41,tmpx,(g4_status & IS_PH_OLIGO));
                      add_new_point_to_ds(dsg42,tmpx,(3.0/IS_PH_G4)*(float)(g4_status & IS_PH_G4));
                      add_new_point_to_ds(dsg43,tmpx,(2.0/IS_PH_NO_BLOCK)*(float)(g4_status & IS_PH_NO_BLOCK));


                    }



                  j = ending - skip_disp_e;
                    {
                      page_n = j / g_r->page_size;
                      i_page = j % g_r->page_size;
                      tmpx = (g_r->timing_mode == 1) ? (g_r->imi[page_n][i_page] - g_r->imi_start) :
                                                  g_r->imi[page_n][i_page];
                      add_new_point_to_ds(dsg41,tmpx,(g4_status & IS_PH_OLIGO));
                      add_new_point_to_ds(dsg42,tmpx,(3.0/IS_PH_G4)*(float)(g4_status & IS_PH_G4));
                      add_new_point_to_ds(dsg43,tmpx,(2.0/IS_PH_NO_BLOCK)*(float)(g4_status & IS_PH_NO_BLOCK));


                    }


              }
            }



            if (ds->nx > 4)
            {

                zd[1] = ds->yd[0] - ds->yd[ds->nx - 1];

                if ((dzph2_bump * zd[1]) > (dzph2_bump * dzph2_bump))
                {
                    last_bump2 = 1;
                    x_last_bump2 = ds->xd[ds->nx - 1];
                    y_last_bump2 = (ds->yd[ds->nx - 1] + ds->yd[0]) / 2;
                }

                if (find_bump == 2)  // we treat bump by computing derivative
                {
                    dsc = duplicate_data_set(ds, NULL);

                    for (j = 1; j < ds->nx; j++)
                    {
                        dsc->xd[j - 1] = dsc->yd[j] - dsc->yd[j - 1];
                    }

                    for (j = 0; j < ds->nx; j++)
                    {
                        dsc->yd[j] = 1;
                    }

                    for (j = 0; j < dsc->nx - 1; j++)
                    {
                        if (((dzph_bump * dsc->xd[j]) > (dzph_bump * dzph_bump)) ||
                                ((dzph2_bump * dsc->xd[j]) > (dzph2_bump * dzph2_bump)))
                        {
                            dsc->yd[j] = dsc->yd[j + 1] = 0;

                            if (j > 0)
                            {
                                dsc->yd[j - 1] = 0;
                            }

                            if (j < dsc->nx - 2)
                            {
                                dsc->yd[j + 2] = 0;
                            }
                        }
                    }

                    for (j = 0, ds->nx = 0; j < dsc->nx; j++)
                    {
                        if (dsc->yd[j] == 1)
                        {
                            ds->xd[ds->nx] = ds->xd[j];
                            ds->yd[ds->nx] = ds->yd[j];
                            ds->nx++;
                            ds->ny = ds->nx;
                        }
                    }

                    free_data_set(dsc);
                }
            }
          }



            if (find_bump == 3 && ds != NULL)
            {
              if (ds->nx > 4)
              {
                dsc = duplicate_data_set(ds, NULL);

                for (j = 3; j < ds->nx - 3; j++)
                {
                    dsc->xd[j] = (dsc->yd[j + 2] - dsc->yd[j - 2]);
                }

                for (j = dmin = dmax = 5, dzmin = dzmax = dsc->xd[5]; j < dsc->nx - 6; j++)
                {
                    if (dsc->xd[j] > dzmax)
                    {
                        dmax = j;
                        dzmax = dsc->xd[j];
                    }

                    if (dsc->xd[j] < dzmin)
                    {
                        dmin = j;
                        dzmin = dsc->xd[j];
                    }
                }

                if ((dzph_bump * dzmax) > (dzph_bump * dzph_bump))
                {
                    if (dzdtst)
                    {
                        add_new_point_to_ds(dst, ds->xd[dmax] , (ds->yd[dmax + 1] + ds->yd[dmax - 1]) / 2);
                    }

                    sprintf(st, "\\pt7\\center{%d}", no++);
                    set_ds_plot_label(ds, ds->xd[dmax], (ds->yd[dmax + 1] + ds->yd[dmax - 1]) / 2, USR_COORD, "%s",st);

                    if (z_or_f == 2)
                    {
                        f_unzip_mean += ds->xd[dmax];
                        f_unzip_n++;
                    }
                }

                if ((dzph2_bump * dzmin) > (dzph2_bump * dzph2_bump))
                {
                    if (dzdtst)
                    {
                        add_new_point_to_ds(dstd, ds->xd[dmin] , (ds->yd[dmin - 1] + ds->yd[dmin + 1]) / 2);
                    }

                    sprintf(st, "\\pt7\\center{%d}", no++);
                    set_ds_plot_label(ds, ds->xd[dmin], (ds->yd[dmin - 1] + ds->yd[dmin + 1]) / 2, USR_COORD, "%s", st);
                }

                free_data_set(dsc);
            }
          }

            if (find_bump == 4)
            {
                if (last_bump)
                {
                    if (dzdtst)
                    {
                        add_new_point_to_ds(dst, x_last_bump , y_last_bump);
                    }
                }

                if (last_bump2)
                {
                    if (dzdtst)
                    {
                        add_new_point_to_ds(dstd,  x_last_bump2 , y_last_bump2);
                    }
                }
            }

            if (same_ds < 2)
            {
                ds = NULL;
            }






            //ds = NULL;
        } // loop end on cycles

        if (g4_analysis){
          if (take_last_g4 && is_g4 == 1)  {
            if (first_g4_time_for_this_bead)
            {
              op->user_ispare[0] = ds_g4_times->nx;
              first_g4_time_for_this_bead = 0;
            }
            op->user_ispare[1] = ds_g4_times->nx;
            add_new_point_to_ds(ds_g4_times,i+g_r->start_bead,(last_seen_g4-begin_g4));
          }




          if (take_last_oligo && is_g4 == 0 && block_points_bf_g4 > 0)  {
            if (first_block_time_for_this_bead)
                      {
                          op->user_ispare[2] = ds_block_times->nx;
                          first_block_time_for_this_bead = 0;
                            }
                op->user_ispare[3] = ds_block_times->nx;
                add_new_point_to_ds(ds_block_times,i+g_r->start_bead,block_points_bf_g4);
              }





        }





        if (dzdtst == 2) // add dataset with the bump positions to the signal plot
        {
            set_ds_dot_line(dst);
            add_one_plot_data(op, IS_DATA_SET, (void *)dst);
            set_ds_dot_line(dstd);
            add_one_plot_data(op, IS_DATA_SET, (void *)dstd);
        }

        if (ds_fopen != NULL && i < ds_fopen->nx && dst && dst->nx > (max - min)/2)
        {
            ds_fopen->xd[i] = i;

            dst->xe = dst->xd;
            dst->xd = dst->yd;
            dst->yd = dst->xe;
            dst->xe = NULL;
	    for (j = 0; ds_fopens != NULL && j < dst->nx; j++)
	      add_new_point_to_ds(ds_fopens, dst->xd[j], dst->yd[j]);
            /*
            for (j = 0, ds_fopen->yd[i] = 0; j < dst->nx; j++)
              ds_fopen->yd[i] += dst->yd[j];
            if (j > 0) ds_fopen->yd[i] /= j;
            */
            j = find_max_of_distribution_by_histo(dst, NULL, (float)1.0, ds_fopen->yd + i);
            if(j != 0)
              {
            win_printf("Error in find max by histo %d",j);
            //ds_fopen->yd[i] = 0;
              }

            dst->xe = dst->xd;
            dst->xd = dst->yd;
            dst->yd = dst->xe;
            dst->xe = NULL;
            /*
            ds_fopen->yd[i] = f_unzip_mean;
            if (f_unzip_n > 0) ds_fopen->yd[i] /= f_unzip_n;
            if (dst != NULL)
              {
            for (j = 0, ds_fopen->yd[i] = 0; j < dst->nx; j++)
              ds_fopen->yd[i] += dst->xd[j];
            if (j > 0) ds_fopen->yd[i] /= j;
              }
            */
        }
        if (ds_fclose != NULL && i < ds_fclose->nx && dstd && dstd->nx > (max - min)/2)
        {
            ds_fclose->xd[i] = i;

            dstd->xe = dstd->xd;
            dstd->xd = dstd->yd;
            dstd->yd = dstd->xe;
            dstd->xe = NULL;
	    for (j = 0; ds_fcloses != NULL && j < dstd->nx; j++)
	      add_new_point_to_ds(ds_fcloses, dstd->xd[j], dstd->yd[j]);
            j = find_max_of_distribution_by_histo(dstd, NULL, (float)1.0, ds_fclose->yd + i);
            if(j != 0)
              {
		win_printf("Error in find max by histo %d",j);
              }

            dstd->xe = dstd->xd;
            dstd->xd = dstd->yd;
            dstd->yd = dstd->xe;
            dstd->xe = NULL;
        }

        if (do_adj_f_unzip && f_unzip_n > 0)
        {
            f_unzip_mean /= f_unzip_n;

            for (op->cur_dat = unzip_ds_s; op->cur_dat < op->n_dat; op->cur_dat++)
            {
                ds = op->dat[op->cur_dat];

                for (j = 0; j < ds->nx && f_unzip_mean > 0; j++)
                {
                    ds->xd[j] *= adj_f_unzip / f_unzip_mean;
                }
            }

            op->cur_dat = 0;

            if (dzdtst == 1 && dst != NULL)
            {
                for (j = 0; j < dst->nx && f_unzip_mean > 0; j++)
                {
                    dst->xd[j] *= adj_f_unzip / f_unzip_mean;
                }
            }

            if (dzdtst == 1 && dstd != NULL)
            {
                for (j = 0; j < dstd->nx && f_unzip_mean > 0; j++)
                {
                    dstd->xd[j] *= adj_f_unzip / f_unzip_mean;
                }
            }
        }

        if (dzdtst == 1)
        {
            dst = dstd = NULL;
        }
	char *tmpfile = strdup(g_r->filename);
	for (j = 0; tmpfile[j] > 0; j++);
	for (; j >= 0 && tmpfile[j] != '.'; j--);
	j = (j < 0) ? 0 : j;
	tmpfile[j] = 0;
        set_op_filename(op, "%s-Z(t)bd%dtrack%d.gr",tmpfile, i, g_r->n_rec);
        if (do_adj_f_unzip && f_unzip_n > 0)
        {
            set_plot_title(op, "\\stack{{Bead %d Z(t) %d}{\\pt7 Cycles:[%d,%d] phase(s) %s}{Force adjusted by %g}}"
                           , i +  g_r->start_bead, g_r->n_rec, min, max, phs, adj_f_unzip / f_unzip_mean);
            if (g4_analysis)   op->user_ispare[6] = i + g_r->start_bead;
          }
        else
        {
            set_plot_title(op, "\\stack{{Bead %d Z(t) %d}{\\pt7 Cycles:[%d,%d] phase(s) %s}}", i + g_r->start_bead, g_r->n_rec, min, max, phs);
            if (g4_analysis)   op->user_ispare[6] = i + g_r->start_bead;
        }

        if (z_or_f == 0)
        {
            set_plot_x_title(op, "Time");
        }
        else if (z_or_f == 1)
        {
            set_plot_x_title(op, "Zmag");
        }
        else if (z_or_f == 2)
        {
            set_plot_x_title(op, "Estimated Force");
        }
        else if (z_or_f == 3)
        {
            set_plot_x_title(op, "Index in phase");
        }
        else if (z_or_f == 4)
        {
            set_plot_x_title(op, "Index in cycle");
        }

        set_plot_y_title(op, "Extension");
        op->need_to_refresh = 1;

        /*win_printf("check tmpx %g, phi0s %d, phi0e %d",tmpx,phi0s,phi0e);
        retrieve_image_index_of_next_point_phase(g_r,min+1, 1,&phi0s, &phi0e, &param_cst);
        win_printf("phase %d start at phi0s %d phi0e %d",min+1,phi0s,phi0e);*/
        //win_printf("bef fitting bead %d with %d pts in ds 0",i,op->dat[0]->nx);

        if (analyze_b && (op->n_dat > 1 || op->dat[0]->nx != 0) && same_op == 0)
        {
            //win_printf("fitting bead %d",i)
            refresh_plot(pr, pr->n_op - 1);    ;
            opopen = x_openning_extend_of_pts_crossing_y_all_ds_same_op(pr, op, hi_low_discri);

            if (opopen)
            {
                /*
                opopenh = build_histo_with_exponential_convolution(pr, opopen, opopen->dat[0]
                                   ,1.0/g_r->Pico_param_record.camera_param.camera_frequency_in_Hz);
                find_max_in_y_in_x_range(opopenh, opopenh->dat[0], FLT_MIN, FLT_MAX, &Max_open, &Max_val);
                */
                mean_y2_on_array(opopen->dat[0]->yd, opopen->dat[0]->nx, 0, &Max_open, NULL, NULL);

                if (opopen->dat[0]->nx <= 0)
                {
                    Max_open = 0;
                }

                Max_open = opopen->ay + opopen->dy * Max_open;
                refresh_plot(pr, pr->n_op - 1);
                //mean_y2_on_op(opopen, 0, &Max_open, NULL, NULL, NULL, NULL);
            }
            else
            {
                Max_open = -1;
            }

            optt = merge_all_ds_in_op(pr, op);
            refresh_plot(pr, pr->n_op - 1);
            oph = (optt) ? build_histo_with_exponential_convolution(pr, optt, optt->dat[0], 0.0005 * biny) : NULL;
            refresh_plot(pr, pr->n_op - 1);
            Max_pos = -1;

            if (oph)
            {
                find_max_in_y_in_x_range(oph, oph->dat[0], block_low, block_high, &Max_pos, &Max_val, &Max_w);
            }

            opdt = x_extend_of_pts_in_y_range_all_ds_same_op(pr, op, Max_pos - 0.01 * biny, Max_pos + 0.01 * biny);
            refresh_plot(pr, pr->n_op - 1);
            mean_y2_on_array(opdt->dat[0]->yd, opdt->dat[0]->nx, 0, &Max_posa, NULL, NULL);

            if (opdt->dat[0]->nx <= 0)
            {
                Max_posa = 0;
            }

            Max_posa = opdt->ay + opdt->dy * Max_posa;
            ophdt = (opdt) ? histogram_vc(pr, opdt, NULL, -1, 0, 0, 2) : NULL;
            tau = 1.0;

            if (ophdt && ophdt->n_dat > 1 && opdt->dat[0]->nx > 9)
            {
                if (ophdt->dat[1]->nx > 5)
                {
                    // saturated histogram last point are removed
                    if ((5 * ophdt->dat[1]->yd[ophdt->dat[1]->nx - 2]) > ophdt->dat[2]->yd[0])
                    {
                        ophdt->dat[1]->nx -= 2;
                    }

                    if ((5 * ophdt->dat[1]->yd[ophdt->dat[1]->nx - 1]) > ophdt->dat[2]->yd[0])
                    {
                        ophdt->dat[1]->nx -= 1;
                    }
                }

                fit_a_exp_t_over_tau_with_er(ophdt, ophdt->dat[1], &af, &tau, 0.001, 0);//dsfit =
            }
            else
            {
                af = tau = -1;
            }

            taui = 1.0;

            if (ophdt && ophdt->n_dat > 1 && opdt->dat[0]->nx > 9)
            {
                fit_a_exp_t_over_tau_with_er(ophdt, ophdt->dat[2], &af2, &taui, 0.001, 0);    // dsfit =
            }
            else
            {
                af2 = taui = -1;
            }

            Max_pos2 = -1;

            if (oph)
            {
                find_max_in_y_in_x_range(oph, oph->dat[0], hi_low_discri, hi_low_discri + 0.5, &Max_pos2, &Max_val2, &Max_w2);
            }

            Max_close = -1000;

            if (oph)
            {
                find_max_in_y_in_x_range(oph, oph->dat[0], -0.1, 0.1, &Max_close, &Max_valc, &Max_wc);
            }

            refresh_plot(pr, pr->n_op - 1);

            if (all_bead == 0)
                win_printf("bead %d: T_{open} = %6.3f s\n max pos %6.3f \\mu m molecule extension %6.3f (%6.3f)\\mu m\n"
                           "N_{block} %d N_{cycles} %d P_{block} %6.3f \\tau_{off} %6.3f s \\tau_{off}(cumul) %6.3f s"
                           , i + g_r->start_bead, Max_open, Max_pos, Max_pos2, mol_ext, opdt->dat[0]->nx, op->n_dat
                           , ((op->n_dat) ? (float)opdt->dat[0]->nx / op->n_dat : 0)
                           , tau, taui);

            i_csv = 0;
            csv_data[i_csv++] = g_r->n_rec;//"trk nb");
            csv_data[i_csv++] = i;//"bead nb");
            csv_data[i_csv++] = oligo;//"[oligo]");
            csv_data[i_csv++] = zmag_open;//"Zmag_{open}");
            csv_data[i_csv++] = trk_zmag_to_force(0, zmag_open, g_r) ; //"F_{open}");
            csv_data[i_csv++] = Max_open;//"T_{open}");
            csv_data[i_csv++] = zmag_test;//"Zmag_{test}");
            csv_data[i_csv++] = trk_zmag_to_force(0, zmag_test, g_r); //"F_{test}");
            csv_data[i_csv++] = (nT_ph[5]) ? ((double)(T_ph[5]) / nT_ph[5]) /
                                g_r->Pico_param_record.camera_param.camera_frequency_in_Hz : 0;//"T_{test}");
            csv_data[i_csv++] = zmag_low;//"Zmag_{low}");
            csv_data[i_csv++] = trk_zmag_to_force(0, zmag_low, g_r); //"F_{low}");
            csv_data[i_csv++] = (nT_ph[7]) ? ((double)(T_ph[7]) / nT_ph[7]) /
                                g_r->Pico_param_record.camera_param.camera_frequency_in_Hz : 0;//"T_{low}");
            csv_data[i_csv++] = 0;//"T_{on}");

            if (all_bead)
            {
                if (last_treated_bd == -2)
                {
                    replace_extension(kon_file, g_r->fullname, "kon-koff.csv", sizeof(kon_file));
                    fp = fopen(kon_file, "w");

                    if (fp == NULL)
                    {
                        return win_printf_OK("cannot open file:\n%s", backslash_to_slash(kon_file));
                    }
                    else
                    {
                        for (i_csv = 0; i_csv < n_csv; i_csv++)
                        {
                            fprintf(fp, " %s%c", csv_title[i_csv], ((i_csv < n_csv - 1) ? ';' : ' '));
                        }

                        fprintf(fp, "\n");
                        //win_printf("%d labels last %s",n_csv,csv_title[n_csv-1]);
                        kon_file_ok = 1;
                        fclose(fp);
                    }

                    last_treated_bd = -1;
                }

                for (; last_treated_bd < (i - 1); last_treated_bd++)
                {
                    fp = fopen(kon_file, "a");

                    if (fp == NULL)
                    {
                        return win_printf_OK("cannot open file:\n%s", backslash_to_slash(kon_file));
                    }
                    else
                    {
                        csv_data[1] = last_treated_bd + 1;//"bead nb");

                        for (i_csv = 0; i_csv < 13; i_csv++)
                        {
                            fprintf(fp, " %g;", csv_data[i_csv]);
                        }

                        for (; i_csv < n_csv; i_csv++)
                        {
                            fprintf(fp, " %c", ((i_csv < n_csv - 1) ? ';' : ' '));
                        }

                        fprintf(fp, "\n");
                        fclose(fp);
                    }
                }
            }

            i_csv = 0;
            csv_data[i_csv++] = g_r->n_rec;//"trk nb");
            csv_data[i_csv++] = i;//"bead nb");
            csv_data[i_csv++] = oligo;//"[oligo]");
            csv_data[i_csv++] = zmag_open;//"Zmag_{open}");
            csv_data[i_csv++] = trk_zmag_to_force(0, zmag_open, g_r) ; //"F_{open}");
            csv_data[i_csv++] = Max_open;//"T_{open}");
            csv_data[i_csv++] = zmag_test;//"Zmag_{test}");
            csv_data[i_csv++] = trk_zmag_to_force(0, zmag_test, g_r); //"F_{test}");
            csv_data[i_csv++] = (nT_ph[5]) ? ((double)(T_ph[5]) / nT_ph[5]) /
                                g_r->Pico_param_record.camera_param.camera_frequency_in_Hz : 0;//"T_{test}");
            csv_data[i_csv++] = zmag_low;//"Zmag_{low}");
            csv_data[i_csv++] = trk_zmag_to_force(0, zmag_low, g_r); //"F_{low}");
            csv_data[i_csv++] = (nT_ph[7]) ? ((double)(T_ph[7]) / nT_ph[7]) /
                                g_r->Pico_param_record.camera_param.camera_frequency_in_Hz : 0;//"T_{low}");
            csv_data[i_csv++] = 0;//"T_{on}");
            csv_data[i_csv++] = op->n_dat;//"N_{cycles} Ok");
            csv_data[i_csv++] = (Max_pos2 > 0) ? 1000 * Max_pos2 : 0; //"Z_{open}");
            csv_data[i_csv++] = (Max_pos2 > 0) ? 1000 * Max_w2 : 0; //"error Z_{open}");
            csv_data[i_csv++] = (Max_close > -0.1) ? 1000 * Max_close : 0; //"Z_{close}");
            csv_data[i_csv++] = (Max_close > -0.1) ? 1000 * Max_wc : 0; //"error Z_{close}");
            csv_data[i_csv++] = (Max_pos > 0) ? 1000 * Max_pos : 0; //"Z_{block}");
            csv_data[i_csv++] = (Max_pos > 0) ? 1000 * Max_w : 0; //"error Z_{block}");
            csv_data[i_csv++] = opdt->dat[0]->nx;//"N_{block}");
            csv_data[i_csv++] = ((op->n_dat) ? (float)opdt->dat[0]->nx / op->n_dat : 0); //"P_{block}");
            csv_data[i_csv++] = ((op->n_dat) ? sqrt(opdt->dat[0]->nx) / op->n_dat : 0); //"Error P_{block}");
            csv_data[i_csv++] = tau;//"T_{off}");
            csv_data[i_csv++] = Max_posa / sqrt(opdt->dat[0]->nx + 1); //"error T_{off}");
            csv_data[i_csv++] = af;//"a de l'expo");
            csv_data[i_csv++] = taui;//"T_{off} cumul");
            csv_data[i_csv++] = Max_posa / sqrt(opdt->dat[0]->nx + 1); //"error T_{off} cumul");
            csv_data[i_csv++] = af2;//"a de l'expo cumul");
            csv_data[i_csv++] = Max_posa;//"a de l'expo cumul");

            //win_printf("printing bead %d",i);
            if (all_bead && kon_file_ok)
            {
                fp = fopen(kon_file, "a");

                if (fp == NULL)
                {
                    return win_printf_OK("cannot open file:\n%s", backslash_to_slash(kon_file));
                }
                else
                {
                    for (i_csv = 0; i_csv < n_csv; i_csv++)
                    {
                        fprintf(fp, " %g%c", csv_data[i_csv], ((i_csv < n_csv - 1) ? ';' : ' '));
                    }

                    fprintf(fp, "\n");
                    fclose(fp);
                  }

            }

            last_treated_bd = i;
            //free_one_plot(optt);
        }

        if (g4_analysis)  {
          dsg41 = dsg42 = dsg43 = dsg44 = NULL;
        }



        refresh_plot(pr, pr->n_op - 1);
        if (g4_analysis) op_g4->need_to_refresh = 1;
#       ifdef __cplusplus // keep this here: see draw_track_event_phase_average.hh
        if(on_done_one_bead(script, pr, op) == WIN_CANCEL)
            break;
#       endif //__cplusplus // keep this here: see draw_track_event_phase_average.hh
} // end loop on beads


    for (int ind = 0;ind<=max;ind++)
    {

      for (int ind2 = 0;ind2<=ph_max;ind2++)
      {
        free(im_array_k_ph[ind][ind2]);

      }
      free(im_array_k_ph[ind]);
    }
    free(im_array_k_ph);



    if(addresses != 0)
        free(addresses);
    if(filtdata != 0)
        free(filtdata);
    return refresh_plot(pr, pr->n_op - 1);
}
