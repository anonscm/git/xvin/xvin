#ifndef _UEYE_SOURCE_H_
#define _UEYE_SOURCE_H_


#define BIT_PER_PIX_8  0x01080001
#define BIT_PER_PIX_10  0x01100003
#define ACQUIRE_INFINITY 0xFFFFFFFFFFFFFFFF

#define D_BAD 1

#include "allegro.h"
#ifdef XV_WIN32
#include "winalleg.h"
#endif
#include "xvin.h"

/* If you include other plug-ins header do it here*/
/* But not below this define */

# define BUILDING_PLUGINS_DLL
#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)

# include "../fft2d/fftbtl32n.h"
# include "fillibbt.h"
# include "trackBead.h"
# include "track_util.h"
#undef OK
#undef CANCEL
#include <PvDevice.h>
#include <PvStream.h>
# define	CANCEL		129

PXV_FUNC(bool, is_camera_available, (void));
PXV_FUNC(int, do_select_camera,(void));
PXV_FUNC(int, retrieve_camera_basic_parameter, (void));
PXV_FUNC(int, init_source, (void));
PXV_FUNC(int, freeze_source, (void));
PXV_FUNC(int, live_source, (void));
PXV_FUNC(int, kill_source, (void));
PXV_FUNC(PvStream, *OpenStream, (const PvDeviceInfo *aDeviceInfo));
PXV_FUNC(void, ConfigureStream, (PvDevice *aDevice, PvStream *aStream));
PXV_FUNC(void, CreateStreamBuffers, (PvStream *aStream, O_i *oi));
PXV_FUNC(MENU, *pleora_source_image_menu, (void));


#endif

