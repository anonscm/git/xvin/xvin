
#ifndef _SDI_2__UTILS_C_
#define _SDI_2__UTILS_C_

# include "SDI_2_utils.h"
# include "SDI_2_functions.h"
# include <stdlib.h>
# include <string.h>
# include <math.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_min.h>

// int correct_phases_in_buffer(fdcmp* fpb1, int buffer_size)
// {
//   for (int i =1;i<buffer_size;i++)
//   {
//     double fpd=fpb1[i]->phase[0]-fpb1[0]->phase[0];
//     for (j=1; j<fpb1[i]->number_of_modes &&  j<fpb1[0]->number_of_modes; j++)
//     {
//       while (fpb1[i]->phase[j] - fpb1[0]->phase[j] - fpd > M_PI)
//       {
//         fpb1[i]->phase[j] -= 2*M_PI;
//       }
//
//       while (fpb1[i]->phase[j] - fpb1[0]->phase[j] - fpd < -M_PI)
//       {
//         fpb1[i]->phase[j] += 2*M_PI;
//       }
//
//     }
//   }
//
// return 0;
//
// }
//
//
//
// int check_phases_in_buffer(fdcmp* fpb1, int buffer_size)
// {
//   for (int i =0;i<buffer_size;i++)
//   for (int ii=i+1;ii<buffer_size;i++)
//   {
//   {
//     fpd=fpb1[ii]->phase[0]-fpb1[i]->phase[0];
//     for (j=1; j<fpb1[ii]->number_of_modes &&  j<fpb1[i]->number_of_modes; j++)
//     {
//       if (fpb1[ii]->phase[j] - fpb1[i]->phase[j] - fpd > M_PI) return(win_printf_OK("No good translation between mode %d and mode %d\n",i,ii));
//       if (fpb1[ii]->phase[j] - fpb1[i]->phase[j] - fpd < -M_PI) return(win_printf_OK("No good translation between mode %d and mode %d\n",i,ii));
//     }
//   }
//   }
//
// return 0;
//
// }

double SDI_2_func_min(double x, int cl2, float sig, float om)
{
  return exp(-(cl2-x)*(cl2-x)/(2*sig*sig)) * (1+cos(om*(cl2-x)));
}

double SDI_2_minimized_func (double x, void * params)
{
  struct fun_params * param = (struct fun_params *)params;
  b_track *bt = param->bt;
  sdi_fr *f1 = param->f1;
  double r = 0;
  float sig = bt->sigma_g;
  float om = bt->om_cos;
  int cl = bt->cl;
  int cl2 = cl /2 ;

  for (int i = 0;i<bt->cl;i++)
   r += (double)(f1->xi[i] * SDI_2_func_min( i-x, cl2, sig, om));


  return -r;
}

int SDI_2_minimize_distance_local(b_track *bt,sdi_fr *f1, double *pos_x, double *score,double a, double b)
{
  int status;
  int iter = 0, max_iter = bt->min_max_iter;
  const gsl_min_fminimizer_type *T;
  gsl_min_fminimizer *s;
  double m = (a+b)/2;

  gsl_function F;
  struct fun_params params = {bt,f1};

  F.function = &SDI_2_minimized_func;
  F.params = &params;

  //*pos_x = 0;
  //return 0;

  gsl_set_error_handler_off();

  T = gsl_min_fminimizer_brent;
  s = gsl_min_fminimizer_alloc (T);
  status = gsl_min_fminimizer_set (s, &F,m , a, b);
  //printf ("error0: %s\n", gsl_strerror (status));

  do
    {
      iter++;
      status = gsl_min_fminimizer_iterate (s);
      //printf ("error1: %s\n", gsl_strerror (status));


      m = gsl_min_fminimizer_x_minimum (s);
      a = gsl_min_fminimizer_x_lower (s);
      b = gsl_min_fminimizer_x_upper (s);

      status
        = gsl_min_test_interval (a, b, bt->min_epsabs, bt->min_epsrel);
        //printf ("error2: %s\n", gsl_strerror (status));
      //
      // if (status == GSL_SUCCESS)
      //   printf ("Converged:\n");

    }
  while (status == GSL_CONTINUE && iter < max_iter);
  *score = gsl_min_fminimizer_f_minimum(s);
  gsl_min_fminimizer_free (s);
  *pos_x = m;
  return status;
}

int SDI_2_minimize_distance_global(b_track *bt,sdi_fr *f1, double *pos_x)
{
  float T = 2 * M_PI / bt->om_cos;
  double score;
  double newscore;
  double pos_m;
  double new_pos_m;
  SDI_2_minimize_distance_local(bt,f1, &new_pos_m, &newscore,-T/2,T/2);
  pos_m = new_pos_m;
  int i = 0;
  score = newscore;
  while (1)
  {
    i += 1;

    SDI_2_minimize_distance_local(bt,f1, &new_pos_m, &newscore,i*T-T/2,i*T+T/2);
    if (newscore > score) break;
    else
    {
      pos_m = new_pos_m;
      score = newscore;
    }
  }


    i = 0;
    while (1)
    {
      i -= 1;
      SDI_2_minimize_distance_local(bt,f1, &new_pos_m, &newscore,i*T-T/2,i*T+T/2);
      if (newscore > score) break;
      else
      {
        pos_m = new_pos_m;
        score = newscore;
      }
    }


  *pos_x = pos_m;

  return 0;
}

int SDI_2_correct_phase(fdcmp* fbp0, fdcmp *fbp1, int nb_of_modes, int low_mode, int fixed_mode)
{
  d_s* fit_ds;
  int jfixed = fixed_mode - low_mode;
  double pd0 = fbp1->phase[jfixed] - fbp0->phase[jfixed];
  double *res = NULL;
  double b =0 ,a=0;
  int n = 0;

  fit_ds = build_adjust_data_set(NULL,nb_of_modes,nb_of_modes);
  if (fit_ds==NULL) return 1;
  alloc_data_set_y_error(fit_ds);
  //
  //
  // for (int j=jfixed-1;j<jfixed+2;j+=2)
  // {
  //   while (fbp1->phase[j] - fbp0->phase[j] - pd0 > M_PI) fbp1->phase[j] -= 2*M_PI;
  //   while (fbp1->phase[j] - fbp0->phase[j] - pd0 < -M_PI) fbp1->phase[j] += 2*M_PI;
  //
  //   fit_ds->xd[j] = (float) (low_mode + j);
  //   fit_ds->yd[j] = (float) (fbp1->phase[j] - fbp0->phase[j]);
  //   fit_ds->ye[j] = (float) (fbp1->amplitude[j]);
  // }
  //
  // compute_least_square_fit_on_ds_between_indexes(fit_ds, 1, jfixed-1, jfixed+2,&a,&b);


  for (int j=0;j<nb_of_modes;j++)
  {
    while (fbp1->phase[j] - fbp0->phase[j] - pd0 - a*(j-jfixed) > M_PI) fbp1->phase[j] -= 2*M_PI;
    while (fbp1->phase[j] - fbp0->phase[j] - pd0 - a*(j-jfixed) < -M_PI) fbp1->phase[j] += 2*M_PI;

    fit_ds->xd[j] = (float) (low_mode + j);
    fit_ds->yd[j] = (float) (fbp1->phase[j] - fbp0->phase[j]);
    fit_ds->ye[j] = (float) (fbp1->amplitude[j]);
  }



  //compute_least_square_fit_on_ds(fit_ds,1,&a,&b);
  fit_ds_to_xn_polynome_er(fit_ds, 2, 0, 1e6, -1e6, 1e6, &res);
  b = res[0];


  b = (b + M_PI) / (2* M_PI);
  n = b > 0 ? (int) b : ((int) b) - 1;

  for (int j = 0; j<nb_of_modes;j++)
  {
    fbp1->phase[j] -= 2*n*M_PI;
  }

  free_data_set(fit_ds);
  free(res);

  return 0;

}


int SDI_2_correct_phase_N(fdcmp* fbp0, int nb_of_modes, int low_mode, int fixed_mode)
{

  int jfixed = fixed_mode - low_mode;
  d_s *fit_ds = build_data_set(3,3);
  double a=0,b=0;

 //  for (int j=jfixed-1;j<jfixed+2;j+=2)
 //  {
 //    while (fbp0->phase[j] - fbp0->phase[jfixed]  > M_PI) fbp0->phase[j] -= 2*M_PI;
 //    while (fbp0->phase[j] - fbp0->phase[jfixed] < -M_PI) fbp0->phase[j] += 2*M_PI;
 //
 //  }
 //
 //  fit_ds->xd[0] = jfixed-1;
 //  fit_ds->xd[1] = jfixed;
 //  fit_ds->xd[2] = jfixed+1;
 //
 //  fit_ds->yd[0] = fbp0->phase[jfixed-1];
 //  fit_ds->yd[1] = fbp0->phase[jfixed];
 //  fit_ds->yd[2] = fbp0->phase[jfixed+1];
 //
 // compute_least_square_fit_on_ds(fit_ds, 1,&a,&b);

  for (int j=0;j<nb_of_modes;j++)
  {
    while (fbp0->phase[j] - fbp0->phase[jfixed] - a*(j-jfixed) > M_PI) fbp0->phase[j] -= 2*M_PI;
    while (fbp0->phase[j] - fbp0->phase[jfixed] - a*(j-jfixed) < -M_PI) fbp0->phase[j] += 2*M_PI;

  }

  free_data_set(fit_ds);

  return 0;

}




int SDI_2_correct_and_find_phase(b_track *bt,float push, float *phase0, float *phase0_er,float *phase1, int nb_of_modes, int low_mode, double *fit_coeffs, d_s *fit_ds,double *pos_x,float* quality)
{
    int jfixed = bt->fixed_mode - low_mode;

  float pd0 = phase1[jfixed] - phase0[jfixed];
  double a = 0,b = 0;
  int n = 0;
  if (fit_ds==NULL) return 1;




  //first find an approximation of the set_dialog_properties


  // for (int j=jfixed-1;j<jfixed+2;j+=2)
  // {
  //
  //   while (phase1[j] - phase0[j] - pd0 > M_PI) phase1[j] -= 2*M_PI;
  //   while (phase1[j] - phase0[j] - pd0 < -M_PI) phase1[j] += 2*M_PI;
  //
  //   fit_ds->xd[j] = (float) (low_mode + j);
  //   fit_ds->yd[j] = (float) (phase1[j] - phase0[j]);
  //   fit_ds->ye[j] = (float) phase0_er[j];
  // }
  //
  // compute_least_square_fit_on_ds_between_indexes(fit_ds, 1, jfixed-1, jfixed+2,&a,&b );



  for (int j=0;j<nb_of_modes;j++)
  {

    while (phase1[j] - phase0[j] - pd0 - (float) (a*(j-jfixed)) > M_PI) phase1[j] -= 2*M_PI;
    while (phase1[j] - phase0[j] - pd0 - (float) (a*(j-jfixed)) < -M_PI) phase1[j] += 2*M_PI;

    fit_ds->xd[j] = (float) (low_mode + j);
    fit_ds->yd[j] = (float) (phase1[j] - phase0[j]);
    fit_ds->ye[j] = (float) phase0_er[j];
  }


  compute_least_square_fit_on_ds(fit_ds,1,pos_x,&b);


  b = (b + M_PI) / (2* M_PI);
  n = b > 0 ? (int) b : ((int) b) - 1;

  for (int j = 0; j<nb_of_modes;j++)
  {
    if (push != 0) printf("push = %f\n",j,push);
    phase1[j] -= 2*(n-push)*M_PI ;
    fit_ds->yd[j] -= 2*(n-push)*M_PI;
  }

  compute_least_square_fit_on_ds(fit_ds,0,pos_x,&b);
  *quality = 0;
  for (int j=0; j<fit_ds->nx;j++)
  {
    *quality += (fit_ds->yd[j]-*pos_x*fit_ds->xd[j])*(fit_ds->yd[j]-*pos_x*fit_ds->xd[j])/((fit_ds->ye[2])*(fit_ds->ye[2]));
  }
  fit_coeffs[0] = *pos_x;
  fit_coeffs[1] = b;
  *pos_x = - *pos_x * ((double) bt->cl / (2*M_PI)) ;


  return 0;

}


int SDI_2_initiate_matrix(b_track *bt,sdi_fr *f1)
{

  int signum = 0;
  int i,j;
  int nb_of_modes =  bt->filter_high - bt->filter_low + 1;

  double *mean_amplitude = NULL;
  int jfixed = bt->fixed_mode - bt->filter_low;


  mean_amplitude = (double*) calloc(nb_of_modes,sizeof(double));
  gsl_matrix_set_zero(bt->w);
  gsl_matrix_set_zero(bt->matx);
  gsl_matrix_set_zero(f1->matxt);
  gsl_matrix_set_zero(bt->temp);
  gsl_matrix_set_zero(bt->temp2);
  gsl_matrix_set_zero(bt->temp3);
  gsl_matrix_set_zero(bt->temp4);
  gsl_vector_set_zero(bt->maty);
  gsl_vector_set_zero(bt->beta);
  gsl_permutation_init(bt->perm);

  for (i=0;i<nb_of_modes;i++)
  {
    for (j=0;j<bt->im_buffer_size;j++)  mean_amplitude[i] += f1->fpb[j]->amplitude[i] / bt->im_buffer_size;
  }

  for (i = 0 ; i < bt->im_buffer_size; i++)
  {
  	for (j = 0 ; j < nb_of_modes; j++)
  	{
  		gsl_matrix_set(bt->w,i*nb_of_modes+j,i*nb_of_modes+j,mean_amplitude[j]);
  		gsl_vector_set(bt->maty,i*nb_of_modes+j,f1->fpb[i]->phase[j]);
  		if (j != jfixed) gsl_matrix_set(bt->matx,i*nb_of_modes+j,(j < jfixed) ? j : j-1,1.0);
  		gsl_matrix_set(bt->matx,i*nb_of_modes+j,nb_of_modes -1 + i, (double) j+bt->filter_low);
  		if (j != jfixed) gsl_matrix_set(f1->matxt,(j < jfixed) ? j : j-1 ,i*nb_of_modes+j,1.0);
  		gsl_matrix_set(f1->matxt,nb_of_modes - 1 + i,i*nb_of_modes+j, (double) j+bt->filter_low);

  	}
  }

  gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1,bt->w,bt->matx,0,bt->temp);  //wx->x
  gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1,f1->matxt,bt->temp,0,bt->temp2); //xt w x -> temp
  gsl_linalg_LU_decomp (bt->temp2, bt->perm, &signum);
  gsl_linalg_LU_invert(bt->temp2,bt->perm,bt->temp3);
  gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1,bt->temp3,f1->matxt,0,bt->temp4);
  gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1,bt->temp4,bt->w,0,f1->matxt);
  free(mean_amplitude);

  return 0;


}


int SDI_2_find_average_profile(b_track *bt,sdi_fr *f1)
{

  int signum = 0;
  int i,j,k;
  int nb_of_modes =  bt->filter_high - bt->filter_low + 1;

  int jfixed = bt->fixed_mode -  bt->filter_low;
  double *mean_amplitude = NULL;




  mean_amplitude = calloc(nb_of_modes,sizeof(double));
  gsl_matrix_set_zero(bt->w);
  gsl_matrix_set_zero(bt->matx);
  gsl_matrix_set_zero(f1->matxt);
  gsl_matrix_set_zero(bt->temp);
  gsl_matrix_set_zero(bt->temp2);
  gsl_matrix_set_zero(bt->temp3);
  gsl_matrix_set_zero(bt->temp4);
  gsl_vector_set_zero(bt->maty);
  gsl_vector_set_zero(bt->beta);
  gsl_permutation_init(bt->perm);

  for (i=0;i<nb_of_modes;i++)
  {
    for (j=0;j<bt->im_buffer_size;j++)
    {
      mean_amplitude[i] += f1->fpb[j]->amplitude[i] / bt->im_buffer_size;
  }
  }

  for (i = 0 ; i < bt->im_buffer_size; i++)
  {
    for (j = 0 ; j < nb_of_modes; j++)
    {
      gsl_matrix_set(bt->w,i*nb_of_modes+j,i*nb_of_modes+j,mean_amplitude[j]);
      gsl_vector_set(bt->maty,i*nb_of_modes+j,f1->fpb[i]->phase[j]);
      if (j != jfixed) gsl_matrix_set(bt->matx,i*nb_of_modes+j,(j < jfixed) ? j : j-1,1.0);
      gsl_matrix_set(bt->matx,i*nb_of_modes+j,nb_of_modes -1 + i, (double) j+bt->filter_low);
      if (j != jfixed) gsl_matrix_set(f1->matxt,(j < jfixed) ? j : j-1 ,i*nb_of_modes+j,1.0);
      gsl_matrix_set(f1->matxt,nb_of_modes - 1 + i,i*nb_of_modes+j, (double) j+bt->filter_low);

    }
  }

  gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1,bt->w,bt->matx,0,bt->temp);  //wx->x
  gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1,f1->matxt,bt->temp,0,bt->temp2); //xt w x -> temp
  gsl_linalg_LU_decomp (bt->temp2, bt->perm, &signum);
  gsl_linalg_LU_invert(bt->temp2,bt->perm,bt->temp3);
  gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1,bt->temp3,f1->matxt,0,bt->temp4);
  gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1,bt->temp4,bt->w,0,f1->matxt);


  for ( k = 0 ; k < nb_of_modes; k++)
  {
    f1->avg_profile_er[k]=(float) 1/mean_amplitude[k];
    f1->avg_profile[k] = 0;


  for ( i = 0 ; i < bt->im_buffer_size; i++)
  {
  	for ( j = 0 ; j < nb_of_modes ; j++)
  	{
      if (k < jfixed){
      f1->avg_profile[k] += (float) (f1->matxt->data[k * f1->matxt->tda + i*nb_of_modes + j]*f1->fpb[i]->phase[j]);
      }
      else if (k > jfixed){
      f1->avg_profile[k] += (float) (f1->matxt->data[(k-1) * f1->matxt->tda + i*nb_of_modes + j]*f1->fpb[i]->phase[j]) ;
      }

  	}
  }
}


free(mean_amplitude);
return 0;
}


int SDI_2_fit_position_from_buffer(b_track *bt, sdi_fr *f1,double *pos_x, float *quality)
  {

int nb_of_modes =  bt->filter_high - bt->filter_low +1;
int c_middle = f1->c_buff;
int jfixed = bt->fixed_mode - bt->filter_low;
*pos_x = 0;
float fitted_phases[nb_of_modes];

// c_middle = f1->c_buff - bt->im_buffer_size / 2;
// if (c_middle < 0) c_middle += bt->im_buffer_size;

for (int i = 0 ; i < bt->im_buffer_size; i++)
{
	for (int j = 0 ; j < nb_of_modes ; j++)
	{
    *pos_x += f1->matxt->data[(nb_of_modes - 1 + c_middle) * f1->matxt->tda + i*nb_of_modes + j]*f1->fpb[i]->phase[j] ;

	}
}
*quality = 0;
for (int k=0;k<nb_of_modes;k++)
{
  if (k<jfixed){
  for (int i=0;i<bt->im_buffer_size;i++){

     for (int j=0;j<nb_of_modes;j++)
     {
         fitted_phases[k] += f1->matxt->data[k * f1->matxt->tda + i*nb_of_modes + j]*f1->fpb[i]->phase[j] ;
}
}
}

if (k>jfixed){
for (int i=0;i<bt->im_buffer_size;i++){

   for (int j=0;j<nb_of_modes;j++)
   {
       fitted_phases[k] += f1->matxt->data[(k-1) * f1->matxt->tda + i*nb_of_modes + j]*f1->fpb[i]->phase[j] ;
}
}
}

}
fitted_phases[jfixed] = 0;
for (int j=0; j<nb_of_modes;j++)
{
  *quality += (f1->fpb[c_middle]->phase[j] - *pos_x*(bt->filter_low + j) - fitted_phases[j])  ;
}

//uncomment the following to compute the position in the middle of the buffer;

//
// c_middle = f1->c_buff - bt->im_buff_size / 2;
// if (c_middle < 0) c_middle += bt->im_buffer_size;


*pos_x = - *pos_x * ((double) bt->cl / (2*M_PI)) ;


return 0;



  }


int  SDI_2_fill_amplitude_phase(float *xi,int filter_low, fft_plan *fp,int filter_high,fdcmp *fpb, int lx, float *fourier, float *bandpassed)
{

  float dsr[lx];
  int i,j;
  int filter_length = filter_high - filter_low +1;

  for (i=0;i<lx;i++) dsr[i]=xi[i];


  realtr1bt(fp, dsr);  fftbt(fp, dsr, 1);  realtr2bt(fp, dsr, 1);


  for (i=1; i < lx/2 -1;i++)
  {
    fourier[i]=dsr[2*i]*dsr[2*i]+dsr[2*i+1]*dsr[2*i+1];
  }
  fourier[0]=dsr[0]*dsr[0];

  for (i=filter_low;i<=filter_high && i < lx/2 -1;i++)
  {
    fpb->amplitude[i-filter_low]=(double) fourier[i];

    if (dsr[2*i] == 0) fpb->phase[i-filter_low] = 0;
    else
    {
      fpb->phase[i-filter_low] = (double) (atan2(dsr[2*i+1],dsr[2*i]));
    }
  }



  for (j = 0;j < lx/2 - 1; j++)
  {
    if ((j >= filter_low) && (j <= filter_high)) continue;
    dsr[2*j] = 0;
    dsr[2*j+1] = 0;
  }


  realtr2bt(fp, dsr, -1);  fftbt(fp, dsr, -1); realtr1bt(fp, dsr);



  for (i=0;i<lx;i++) bandpassed[i]=dsr[i];

  return 0;




}

int  SDI_2_fill_phase_only(float *xi,int filter_low, fft_plan *fp,int filter_high,float *phasefit,int lx, float *fourier, float *bandpassed)
{

  float dsr[lx];
  int i,j;
  int filter_length = filter_high - filter_low +1;

  for (i=0;i<lx;i++) dsr[i]=xi[i];


  realtr1bt(fp, dsr);  fftbt(fp, dsr, 1);  realtr2bt(fp, dsr, 1);


  for (i=1; i < lx/2 -1;i++)
  {
    fourier[i]=dsr[2*i]*dsr[2*i]+dsr[2*i+1]*dsr[2*i+1];
  }
  fourier[0]=dsr[0]*dsr[0];

  for (i=filter_low;i<=filter_high && i < lx/2 -1;i++)
  {
    if (dsr[2*i] == 0) phasefit[i-filter_low] = 0;
    else phasefit[i-filter_low] = atan2(dsr[2*i+1],dsr[2*i]);
  }



  for (j = 0;j < lx/2 - 1; j++)
  {
    if ((j >= filter_low) && (j <= filter_high)) continue;
    dsr[2*j] = 0;
    dsr[2*j+1] = 0;
  }


  realtr2bt(fp, dsr, -1);  fftbt(fp, dsr, -1); realtr1bt(fp, dsr);



  for (i=0;i<lx;i++) bandpassed[i]=dsr[i];

  return 0;




}


int SDI_2_real_max_pos(float *x ,int nx, int bp_low, int bp_high, fft_plan *fp,double *Max_pos)
{

register int i, j;
float dsr[nx], dsi[nx], dsfr[nx], dsfi[nx];
int imax;
float tmp, f0, amp, Max_val;
double re, im, phi;
float Max_pos_float;



//copy dst to dsr




for (j = 0;j < nx; j++)
{
  dsr[j] = x[j];

}

realtr1bt(fp, dsr);  fftbt(fp, dsr, 1);  realtr2bt(fp, dsr, 1);

for (j = 0;j < nx/2; j++)
{
  if ((j >= bp_low) && (j <= bp_high)) continue;
  dsr[2*j] = 0;
  dsr[2*j+1] = 0;

}
//
// duplicate_data_set(dsr,ds3);
// set_ds_source(ds3,"fourier transform");

j = bp_low;

for (j = 2;j < nx; j+=2)
{  // we prepare imaginary part
  dsi[j+1] = dsr[j];
  dsi[j] = -dsr[j+1];
}

dsi[1] = dsi[0] = 0;
dsr[1] = dsr[0] = 0;
/*	get back to complex world */
realtr2bt(fp, dsr, -1);  fftbt(fp, dsr, -1); realtr1bt(fp, dsr);
realtr2bt(fp, dsi, -1);  fftbt(fp, dsi, -1); realtr1bt(fp, dsi);


for (j = 0;j < nx; j++)
{  // we find the amplitude maximum and place the amplitude in dsfr
  tmp = sqrt(dsr[j] * dsr[j] + dsi[j] * dsi[j]);
  dsfr[j] = tmp;
} // the amplitude is stored in dsfr

for (j = 4, imax = 4;j < nx-4; j++)
{  // we skip the border that are poluted by dewindowing
  if (dsfr[j] > dsfr[imax]) imax = j;
} // the amplitude is stored in dsfr
j = find_max_around(dsfr, nx, imax, &Max_pos_float, &Max_val, NULL);

*Max_pos= (double) Max_pos_float;

return 0;


}




int SDI_2_find_centroid(float *yi, int ly, float *cen, float *er)
{
  float r = 0;
  float t = 0;
  float amp=0;
  for (int i = 0;i<ly;i++)
  {
    r += i*yi[i];
    t+=yi[i];
    amp += yi[i];
  }
if (t!=0) *cen =  r/t;
*er = 1/(amp*amp);
return 0;
}

int SDI_2_find_max_parabole(float *y, int ly, float *cen, float *er)
{
  d_s *dstofit;
  dstofit = build_data_set(ly,ly);
  alloc_data_set_y_error(dstofit);
  double *a = NULL;
  float amp = 0;
  int ret = 0;

  for (int i = 0;i<ly;i++)
  {
    dstofit->xd[i] = i;
    dstofit->ye[i] = sqrt(y[i]);
    dstofit->yd[i] = y[i];
    amp+=y[i];
  }
  dstofit->nx = dstofit->ny = ly;
  ret =  fit_ds_to_xn_polynome_er(dstofit, 3, 0, 1e6, -1e6, 1e6, &a);
  *cen = (float) (-a[1]/(2*a[2]));
  *er = sqrt(amp);
  free_data_set(dstofit);

  free(a);
}


int SDI_2_find_yslope(O_i *oic,int xco,int xend,int yco,int yend,float *slope,float *ymax)
{
  d_s *dscentroids;
  int dum = 0;
  int dum1 = 0;
  int dum2 = 0;
  float *yi;
  int ly = yend-yco+1;
  int lx = xend-xco+1;
  double *a = NULL;
  yi = calloc(ly,sizeof(float));
  dscentroids = build_data_set(lx,lx);
  alloc_data_set_y_error(dscentroids);
  for (int x = xco; x<=xend;x++)
  {
      SDI_2_fill_x_av_y_profile_from_im(oic, x, x, yco, yend,yi, &dum,&dum1, &dum2);
    SDI_2_find_centroid(yi,ly,dscentroids->yd+x-xco,dscentroids->ye+x-xco);
    // SDI_2_find_max_parabole(yi,ly,dscentroids->yd+x-xco,dscentroids->ye+x-xco);

      dscentroids->xd[x-xco] = x;
  }
  dscentroids->nx=dscentroids->ny = lx;
  fit_ds_to_xn_polynome_er(dscentroids, 2, 0, 1e6, -1e3, 1e3, &a);
  *slope = a[1];
  *ymax = a[0];
  free_data_set(dscentroids);
  free(a);
  free(yi);
  return 0;
}



int SDI_2_find_y(O_i *oic,int xco,int xend,int yco,int yend,float slope,float *ymax)
{
  d_s *dscentroids;
  int dum = 0;
  int dum1 = 0;
  int dum2 = 0;
  float *yi;
  int ly = yend-yco+1;
  int lx = xend-xco+1;
  float mean = 0;
  float weight = 0;
  yi = calloc(ly,sizeof(float));
  dscentroids = build_data_set(lx,lx);
  alloc_data_set_y_error(dscentroids);
  for (int x = xco; x<=xend;x++)
  {
      SDI_2_fill_x_av_y_profile_from_im(oic, x, x, yco, yend,yi, &dum,&dum1, &dum2);
     SDI_2_find_centroid(yi,ly,dscentroids->yd+x-xco,dscentroids->ye+x-xco);
  //   SDI_2_find_max_parabole(yi,ly,dscentroids->yd+x-xco,dscentroids->ye+x-xco);

      //dscentroids->yd[x-xco] -= slope*x;
      mean +=   dscentroids->yd[x-xco] /(dscentroids->ye[x-xco]*dscentroids->ye[x-xco]);
      weight += 1/(dscentroids->ye[x-xco]*dscentroids->ye[x-xco]);
  }
  *ymax = mean/weight;
  free_data_set(dscentroids);
  free(yi);

  return 0;


}

int SDI_2_find_x_from_centro(O_i *oic,int xco,int xend,int yco,int yend,float slope,double *xmax)
{
  d_s *dscentroids;
  int dum = 0;
  int dum1 = 0;
  double dum2 = 0;
  float *xi;
  int ly = yend-yco+1;
  int lx = xend-xco+1;
  float mean = 0;
  float weight = 0;
  xi = calloc(lx,sizeof(float));
  dscentroids = build_data_set(ly,ly);
  alloc_data_set_y_error(dscentroids);
  for (int y = yco; y<=yend;y++)
  {
      SDI_2_fill_y_av_x_profile_from_im(oic, xco, xend, y, y,xi,NULL, &dum2);
     SDI_2_find_centroid(xi,lx,dscentroids->yd+y-yco,dscentroids->ye+y-yco);
  //   SDI_2_find_max_parabole(yi,ly,dscentroids->yd+x-xco,dscentroids->ye+x-xco);

      //dscentroids->yd[x-xco] -= slope*x;
      mean +=   dscentroids->yd[y-yco] /(dscentroids->ye[y-yco]*dscentroids->ye[y-yco]);
      weight += 1/(dscentroids->ye[y-yco]*dscentroids->ye[y-yco]);
  }
  *xmax = mean/weight;
  free_data_set(dscentroids);
  free(xi);

  return 0;


}








#endif
