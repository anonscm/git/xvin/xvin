#pragma once

#ifndef XV_WIN32
# include "config.h"
#endif

# include <allegro.h>
#include <xvin.h>

PXV_FUNC(int, count_beads_hough,(O_i *im,
                      int distinction_radius,
                      int nearness_radius,
                      int cannythreashold,
                      int accumulator_val,
                      int min_radius,
                      int max_radius,
                      int create_cross,
                      int cl,
                      int cw));
PXV_FUNC(int, do_count_beads_hough, (void));
