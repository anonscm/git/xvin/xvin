/*
 *    Plug-in module allowing to track bead from a movie
 *
 *    V. Croquette
 *    JF Allemand
 */
#ifndef _MOVIE_SOURCE_C_
#define _MOVIE_SOURCE_C_


# include "allegro.h"
#ifdef XV_WIN32
# include "winalleg.h"
# include <windowsx.h>
#endif
# include "ctype.h"
# include "xvin.h"
# include "../xvtiff/xvtiff.h"


XV_FUNC(int, do_load_im, (void));


float g_movie_fps = 30;




# define BUILDING_PLUGINS_DLL
#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)

//# include "movie_source.h"
# include "trackBead.h"

# ifndef TRACK_WIN_THREAD
# include <pthread.h>
# include "timer_thread.h"
# endif

#include "count_beads_cv.hh"

/*
   float read_Z_value(void)
   {
   return 0.0;
   }
   int set_Z_value(float z)
   {
   return 0;
   }
   */



int freeze_source(void)
{
    imreg *imr = NULL;

    if (ac_grep(cur_ac_reg, "%im", &imr) != 1)
    {
        active_menu->flags |=  D_DISABLED;
        return D_O_K;
    }
    //else if (go_track == TRACK_FREEZE) active_menu->flags |=  D_DISABLED;
    else active_menu->flags &=  ~D_DISABLED;

    if (updating_menu_state != 0)    return D_O_K;

    go_track = TRACK_FREEZE;
    return D_O_K;
}


int live_source(void)
{
    imreg *imr = NULL;

    if (ac_grep(cur_ac_reg, "%im", &imr) != 1)
    {
        active_menu->flags |= D_DISABLED;
        return D_O_K;
    }
    //else if (go_track == TRACK_ON) active_menu->flags |=  D_DISABLED;
    else active_menu->flags &= ~D_DISABLED;

    if (updating_menu_state != 0)    return D_O_K;

    go_track = TRACK_ON;
    return D_O_K;
}
int kill_source(void)
{
    imreg *imr = NULL;

    if (ac_grep(cur_ac_reg, "%im", &imr) != 1)
    {
        active_menu->flags |= D_DISABLED;
        return D_O_K;
    }
    //else if (go_track == TRACK_ON) active_menu->flags |=  D_DISABLED;
    else active_menu->flags &= ~D_DISABLED;

    if (updating_menu_state != 0)    return D_O_K;

    go_track = TRACK_STOP;
    return D_O_K;
}

int prepare_image_overlay(O_i *oi)
{
    return 0;
}

// function executed in the tracking thread
# ifdef TRACK_WIN_THREAD
// function executed in the tracking thread
VOID CALLBACK dummyAPCProc(LPVOID lpArgToCompletionRoutine,
                           DWORD dwTimerLowValue,  DWORD dwTimerHighValue)
{
    (void)lpArgToCompletionRoutine;
    (void)dwTimerLowValue;
    (void)dwTimerHighValue;
    return;
}


DWORD WINAPI TrackingThreadProc(LPVOID lpParam)
# else
void *TrackingThreadProc(void *lpParam)
# endif
{
# ifdef TRACK_WIN_THREAD
    HANDLE hTimer = NULL;
    LARGE_INTEGER liDueTime;
    tid *ltid = NULL;
# else
    timer_thread_t hTimer;
    long int liDueTime = 0;
    tid *ltid = (tid *) lpParam;
# endif
    imreg *imr = NULL;
    DIALOG *d = NULL;
    O_i *oi = NULL;
    int nf, im_n = 0;;
    unsigned long dt = 0, t0 = 0, t1 = 0, t2 = 0, dtmax = 0;
    long long t;
    DIALOG *starting_one = NULL;
    Bid *p = NULL;

# ifdef TRACK_WIN_THREAD

    if (imr_TRACK == NULL || oi_TRACK == NULL)  return 1;

    ltid = (tid *)lpParam;
# else

    if (imr_TRACK == NULL || oi_TRACK == NULL)  return (void *) 1;

# endif

    imr = ltid->imr;
    d = ltid->dimr;
    oi = ltid->oi;
    p = ltid->dbid;
    nf = abs(oi->im.n_f);
    // Create a waitable timer.
# ifdef TRACK_WIN_THREAD
    hTimer = CreateWaitableTimer(NULL, TRUE, "WaitableTimer2");

    if (!hTimer)   win_printf_OK("CreateWaitableTimer failed (%d)\n", GetLastError());

# else

    if (create_timer(&hTimer, liDueTime) != 0)
        win_printf_OK("CreateTimer failed\n");

# endif
    t0 = my_uclock();
    starting_one = active_dialog; // to check if windows change
    source_running = 1;

    bool first = true;

    while (go_track)
    {
#ifdef XV_GTK_DIALOG

        if (first)
        {

            //do_count_beads_hough();
            first = false;
        }

#endif

        psleep(1);
        t1 = my_uclock();
        dt = t1 - t0;
        float num_frame_todo_fraq = (dt / (float)(my_uclocks_per_sec)) * g_movie_fps;
        int num_frame_todo = (int) num_frame_todo_fraq;

        //printf("%d, %d, %f, %f\n",im_n, num_frame_todo, num_frame_todo_fraq, dt);

        p->timer_dt = 0;
        p->previous_fr_nb = im_n;
        p->previous_in_time_fr = t0 - (1 / g_movie_fps) * my_uclocks_per_sec;

        if (num_frame_todo >= 1)
        {

            t0 = t1 - ((num_frame_todo_fraq - num_frame_todo) * my_uclocks_per_sec) / g_movie_fps + 1;
        }

        if (go_track == TRACK_FREEZE) continue;

        if (oi_TRACK->im.movie_on_disk == 16) num_frame_todo = 1;

        //tim[oi->im.c_f] = t1;
        //display_image_stuff_16M(imr, d);
        //d->d1 = (int)dt;



        for (int i = 0; i < num_frame_todo; ++i)
        {

          if (oi_TRACK->im.movie_on_disk != -16) im_n = switch_frame(oi, im_n);
          else im_n = 0;
          t = my_ulclock();
          dt = my_uclock();
          p->previous_fr_nb = im_n;

            do
            {
                p->timer_do(imr_TRACK, oi_TRACK, im_n, d_TRACK, t , dt , p->param, num_frame_todo - i - 1);
                p = p->next;

            }
            while (p && p->timer_do != NULL);

            im_n++;
            p = ltid->dbid;
        }

        t2 = my_uclock();

        oi->need_to_refresh |= INTERNAL_BITMAP_NEED_REFRESH;

        //dtim[oi->im.c_f] = my_uclock() - t1;
    }

    //dt_max = 1000*((double)dtmax)/get_my_uclocks_per_sec();
    source_running = 0;
    return 0;
}

int do_choose_fps(void)
{
    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    win_scanf("Movie FPS %f", &g_movie_fps);

}

MENU *movie_source_image_menu(void)
{
    static MENU mn[32] = {0};

    if (mn[0].text != NULL)
    {
        return mn;
    }

    add_item_to_menu(mn, "Movie FPS%f\n", do_choose_fps, NULL, 0, NULL);
    return mn;
}

MENU *zero_menu(void)
{
  return 0;
}

int init_image_source(imreg *imr1, int mode)
{
    O_i *oi = NULL;
    O_i *oibg = NULL;
    imreg *imrnow=NULL;
    int format = 0;
    char path[512] = {0};
    char file[256] = {0};
    static int load_bg = 0;
    static int staticc = 0;
    win_scanf("Load a movie that will be used as a tracking source and choose format %R -> .gr %r -> .tif\n"
              "Load a background %b \n"
              "Static %b \n",&format,&load_bg,&staticc);

    if (format) do_load_tiff_stack_on_disk();
    else  do_load_movie_on_disk();

    imrnow = find_imr_in_current_dialog(NULL);
    oi = imrnow->one_i;

    if (load_bg)
    {
      file_list_t *file_list = open_files_config("Load background image(*.gr)", fui,
                               "Plot Files (*.gr)\0*.gr\0All Files (*.*)\0*.*\0\0", "IMAGE-GR-FILE", "last_loaded");
      file_list_t *cursor = file_list;

      while (cursor != NULL)
      {
          set_config_string("IMAGE-GR-FILE", "last_loaded", cursor->data);
          extract_file_path(path, sizeof(path), cursor->data);
          extract_file_name(file, sizeof(file), cursor->data);
          oibg = load_im_file_in_oi(file, path);

          if (oibg == NULL)
             win_printf_OK("could not background image image\n%s\n"
                                   "from path %s", file, backslash_to_slash(path));
           else
           {
             oi->rm_background = 1;
             oi->oi_bg = oibg;
             if (staticc) oi->im.movie_on_disk = -16;
           }
           cursor = cursor->next;
        }
      }

    map_pixel_ratio_of_image_and_screen(oi, 1, 1);
    set_zmin_zmax_values(oi, 0, 255);
    set_z_black_z_white_values(oi, 0, 255);
    add_image_treat_menu_item("Movie Source Settings", NULL, movie_source_image_menu(), 0, NULL);

    specific_camera_image_menu=zero_menu;

    do_choose_fps();
    return broadcast_dialog_message(MSG_DRAW, 0);
}



int start_data_movie(imreg *imr)
{
    return 0;
}


# endif
