#ifndef _TRANSLATIONCONTROL_C_
#define _TRANSLATIONCONTROL_C_


#include "ctype.h"
#include "allegro.h"
#ifdef XV_WIN32
#include "winalleg.h"
#endif
#include "xvin.h"
#include "xv_plugins.h"    // for inclusions of libraries that control dynamic loading of libraries.
#include "timer_thread.h"
#include <pthread.h>
#include "translation_control.h"



tl_apt_context *mot_contexts_translation[2]={NULL}; //tableau de deux pointeurs vers x (mot_context[0] et y (mot_context[1]))
static char motSerialNum_x[25]="-1";
static char motSerialNum_y[25]="-1";
char *motSerials_translation[2]= {motSerialNum_x,motSerialNum_y};
float step_value_apt_translation=0.1;
float small_step_value_apt_translation=0.01;
static float xpos_apt=-1;
static float ypos_apt=-1;


int move_step_translation(int channel, float step)
{
  if (mot_contexts_translation[channel]==NULL)
  {
  if (motSerials_translation[channel][0]=='-')
    {

    if (channel==0) strcpy(motSerials_translation[channel],get_config_string("SDI","x_motor_serial","-1"));
    if (channel==1) strcpy(motSerials_translation[channel],get_config_string("SDI","y_motor_serial","-1"));
    }
  if (motSerials_translation[channel][0]=='-') return win_printf("unable to find the serial number of the motor in the config gile.\n");

  _init_apt_motors_OK(motSerials_translation[channel], &mot_contexts_translation[channel]);

  }

  if (mot_contexts_translation[channel]==NULL) return win_printf("No connection to the motor.\n");

  _set_apt_motor_rel_value(mot_contexts_translation[channel],step);

}




int do_y_translation_apt_up(void)
  {

    if (key[KEY_LSHIFT])   move_step_translation(1, small_step_value_apt_translation);
    else    move_step_translation(1, step_value_apt_translation);
  }

int do_x_translation_apt_right(void)
  {
    if (key[KEY_LSHIFT]) move_step_translation(0, small_step_value_apt_translation);
    else move_step_translation(0, step_value_apt_translation);
  }

int do_y_translation_apt_down(void)
  {
    if (key[KEY_LSHIFT]) move_step_translation(1, -small_step_value_apt_translation);
    else move_step_translation(1, -step_value_apt_translation);
  }

int do_x_translation_apt_left(void)
  {
    if (key[KEY_LSHIFT]) move_step_translation(0, -small_step_value_apt_translation);
    else  move_step_translation(0, -step_value_apt_translation);
  }


int do_change_step_translation_value(void)
{
    int i;
    static int init=0;

    if (updating_menu_state != 0 )
    {
        add_keyboard_short_cut(0, KEY_UP, 0, do_y_translation_apt_up);
        add_keyboard_short_cut(0, KEY_RIGHT, 0, do_x_translation_apt_right);
        add_keyboard_short_cut(0, KEY_LEFT, 0, do_x_translation_apt_left);
        add_keyboard_short_cut(0, KEY_DOWN, 0, do_y_translation_apt_down);
	      return D_O_K;
    }

    if (updating_menu_state==0)
    {
    i = win_scanf("Enter the new step value \n for translation %f \n"
    "Enter the new small step value \n for translation %f \n", &step_value_apt_translation,&small_step_value_apt_translation);

    if (i == WIN_CANCEL)  return OFF;

    }
    return D_O_K;

}


MENU *translation_control_plot_menu(void)
{
    static MENU mn[32] = {0};

    if (mn[0].text != NULL) return mn;
    add_item_to_menu(mn, "Change translation step", do_change_step_translation_value, NULL, 0, NULL);
    //add_item_to_menu(mn, "Try reinitialise translation motor", do_try_reinitialize_apt_translation_motor,NULL,0,NULL);
    //add_item_to_menu(mn, "Change velocity parameters", do_change_velocity_parameters_apt_translation,NULL,0,NULL);
    //add_item_to_menu(mn, "Try reenable overloaded motor", do_try_reenable_overloaded_motor, NULL,0,NULL)

    return mn;
}


#endif
