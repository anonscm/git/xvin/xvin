/** \file MCL_FOCUS.c
    \brief Plug-in program for pifoc control in Xvin.
    \author JF
*/
#ifndef _PIFOC_DAS1602_C_
#define _PIFOC_DAS1602_C_

#include "allegro.h"
#include "winalleg.h"
#include <windowsx.h>
#include "ctype.h"
#include "xvin.h"

# include "../../cfg_file/Pico_cfg.h"

#include "cbw.h"
#include "PIFOC_das1602.h"

# ifndef DAS1602_H
# include "das1602.h"
# endif

#define BUILDING_PLUGINS_DLL

int _has_focus_memory(void)
{return 0;}

int _set_light(float z)
{return D_O_K;}

float _get_light()
{ return 0.5;}

int _set_temperature(float z)
{return D_O_K;}

float _get_temperature()
{return 0.5;}

int  _init_focus_OK(void)
{
  if(updating_menu_state != 0)	return D_O_K;

 	int err=0;
	int BoardNum=0;
	char *BoardnName=NULL,*ErrMsg=NULL;
	float    RevLevel = (float)CURRENTREVNUM;
	int  BoardType;
	int  NumBoards;

	if(updating_menu_state != 0)	return D_O_K;

	BoardnName=(char *)malloc(BOARDNAMELEN*sizeof(char));
	ErrMsg=(char *)malloc(ERRSTRLEN*sizeof(char));

  /* Declare UL Revision Level Must be the first function used*/
   	err = cbDeclareRevision(&RevLevel);
   	if (err!=0)
   	{
   		cbGetErrMsg( err, ErrMsg );
   		win_printf("Error %s", ErrMsg);
   	 	return 0;
	}

	cbErrHandling( DONTPRINT, DONTSTOP );

 /* Get the number of boards installed in system */
    cbGetConfig (GLOBALINFO, 0, 0, GINUMBOARDS, &NumBoards);

    /* Print out board type name of each installed board */
    for (BoardNum=0; BoardNum<NumBoards; BoardNum++)
        {
        /* Get board type of each board */
        cbGetConfig (BOARDINFO, BoardNum, 0, BIBOARDTYPE, &BoardType);

        /* If a board is installed */
        if (BoardType > 0)
           {
            cbGetBoardName (BoardNum, BoardnName);
       //     win_printf ("    Board #%d = %s\n", BoardNum, BoardnName);
           }
        }

    BoardNum=0;//Change and put a real selection
 	err=cbGetBoardName(BoardNum, BoardnName);

	if (err!=0) win_printf("No card on 0");
//	else win_printf("The card installed is %s",BoardnName);
	init_parameters();
	return 0;
}

float _read_Z_value_accurate_in_micron(void)
{
  return PIFOC_position;
}

int _read_Z_value(void)
{

  return (int)(_read_Z_value_accurate_in_micron());//ask meaninig 10*
}

float _read_Z_value_OK(int *error) /* in microns */
{
  return _read_Z_value_accurate_in_micron();
}

float _read_last_Z_value(void) /* in microns */
{
  return PIFOC_position;
}

int _set_Z_step(float z)  /* in micron */
{
	PIFOC_Z_step = z;
	return 0;
}

int _set_Z_value(float z)  /* in microns */
{
    unsigned short *datavalue;

    datavalue=(unsigned short *)malloc(sizeof(unsigned short));
    *datavalue=0;
    float temp;

    if( (z > Pico_param.focus_param.Focus_max_expansion) || (z<0) ) return D_O_K;

	cbFromEngUnits(parameters.BoardNum, parameters.range_out,(z*10/Pico_param.focus_param.Focus_max_expansion), datavalue);
	        my_set_window_title("z is %f, datavalue %d",z,*datavalue);
	if(cbAOut(parameters.BoardNum,parameters.LowChan_out,parameters.range_out,*datavalue)==0)
        PIFOC_position = z;
    else win_printf("Error in setting z");

  return D_O_K;
}


int _set_Z_value_PIFOC(void)
{
  float goal = 0;

  if(updating_menu_state != 0)	return D_O_K;

  win_scanf("Where do you want to go ?%f",goal);

  _set_Z_value(goal);
  return D_O_K;
}


int _set_Z_obj_accurate(float zstart)
{
  return _set_Z_value(zstart);
}

int _set_Z_value_OK(float z)
{
  return _set_Z_value(z);
}

int _inc_Z_value(int step)  /* in micron */
{
  _set_Z_value(PIFOC_position + PIFOC_Z_step*step);
  return 0;
}

int _small_inc_Z_value(int up)
{
  int nstep;

  _set_Z_step(0.1);
  nstep = (up>=0)? 1:-1;
  _set_Z_value(PIFOC_position + PIFOC_Z_step*nstep);

  return 0;
}

int _big_inc_Z_value(int up)
{
  int nstep;

  _set_Z_step(0.1);
  nstep = (up>=0)? 10:-10;
  _set_Z_value(PIFOC_position + PIFOC_Z_step*nstep);

  return 0;
}


int do_set_out(void)
{   static float data=0;

    if(updating_menu_state != 0)	return D_O_K;

    win_scanf("Value to write?%f (0,+10V)",&data);
    _set_Z_value(data);

    return 0;
}

int do_get_in(void)
{

    if(updating_menu_state != 0)	return D_O_K;

    win_printf("On est de retour %f",PIFOC_position);

    return 0;
}


MENU *PIFOC_das1602_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn, "&Set point on line 0",  do_set_out,      NULL,0,NULL);
    add_item_to_menu(mn, "Read &Single data",    do_get_in,    	   NULL,0,NULL);
	add_item_to_menu(mn,"Read Objective position", _read_Z_value,  NULL,0,NULL);
	add_item_to_menu(mn,"Set Objective position",_set_Z_value_PIFOC, NULL,0,NULL);
    add_item_to_menu(mn, "Initialize", _init_focus_OK,    	       NULL,0,NULL);
	return mn;
}

int PIFOC_das1602_main (int argc, char **argv)
{
  _init_focus_OK();
  win_printf("Hello, main");
  add_plot_treat_menu_item ( "PIFOC das1602", NULL, PIFOC_das1602_plot_menu(), 0, NULL);
  add_image_treat_menu_item ( "PIFOC das1602", NULL, PIFOC_das1602_plot_menu(), 0, NULL);
  return 0;
}

int	PIFOC_das1602_unload(int argc, char **argv)
{
	remove_item_to_menu(plot_treat_menu, "PIFOC das1602", NULL, NULL);
	return D_O_K;
}


int init_parameters(void)
{
                             parameters.HighChan_in=1;
                             parameters.BoardNum=0;
                             parameters.HighChan_out=1;
                             parameters.LowChan_in=1;
                             parameters.LowChan_out=1;
                             parameters.rate_in=10000;
                             parameters.rate_out=1000;
                             parameters.range_in=UNI10VOLTS;
                             parameters.range_out=UNI10VOLTS;
                             parameters.trigger=TRIGABOVE;
                             parameters.count_in=8192;
                             parameters.options_in=BACKGROUND+CONTINUOUS;
                             parameters.options_out=BACKGROUND+CONTINUOUS;
                             parameters.count_out=8192;
                             parameters.HighThreshold=5.0;
                             parameters.LowThreshold=0;
                             //win_printf("Acquisition parameters are set to default value");
                             return 0;
}

#endif
