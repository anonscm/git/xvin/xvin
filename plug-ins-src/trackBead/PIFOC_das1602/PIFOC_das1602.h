#ifndef _PIFOC_DAS1602_H
#define _PIFOC_DAS1602_H


typedef struct {
		
long 	count_in;
long 	count_out;
long 	rate_in;
long 	rate_out;
float 	HighThreshold;
float 	LowThreshold;
int		trigger;
int 	range_in;
int 	range_out;
int 	options_in;
int 	options_out;
int 	BoardNum;
int 	LowChan_in;
int 	HighChan_in;
int 	LowChan_out;
int 	HighChan_out;

}das_param;

das_param parameters;

# ifndef _PIFOC_DAS1602_C_
PXV_VAR(float ,  PIFOC_position);
PXV_VAR(float , PIFOC_Z_step);
//PXV_VAR(das_param , parameters);
# endif

# ifdef _PIFOC_DAS1602_C_
float   PIFOC_position = 0;
float  PIFOC_Z_step = 0.1;

# endif


PXV_FUNC(float, _read_Z_value_OK, (int *error));
PXV_FUNC(float, _read_last_Z_value, (void));
PXV_FUNC(int, _set_Z_step, (float z));
PXV_FUNC(int, _set_Z_value, (float z));
PXV_FUNC(int, _set_Z_value_PIFOC, (void));



PXV_FUNC(int, _set_Z_obj_accurate, (float zstart));
PXV_FUNC(int, _set_Z_value_OK,(float z));
PXV_FUNC(int, _inc_Z_value,(int step));
PXV_FUNC(int, _small_inc_Z_value, (int up));
PXV_FUNC(int, _big_inc_Z_value, (int up));
PXV_FUNC(int, do_set_out     , (void));
PXV_FUNC(int, do_get_in,       (void));
PXV_FUNC(int, init_parameters, (void));
PXV_FUNC(int, PIFOC_das1602_unload, (int argc, char **argv));
PXV_FUNC(int, PIFOC_das1602_main, (int argc, char **argv));


#endif
