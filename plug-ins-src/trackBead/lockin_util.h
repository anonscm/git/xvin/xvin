#ifndef _LOCKIN_UTIL_H_
#define _LOCKIN_UTIL_H_

# include "fftbtl32n.h"
# include "fillibbt.h"

# define live_video_info lockin_info

# define LOCKIN_BUFFER_SIZE 4096
# define PROFILE_BUFFER_SIZE 128
# define MAX_RADIAL_PROFILE_SIZE 256


# define BUF_FREEZED 2
# define BUF_CHANGING 1
# define BUF_UNLOCK 0


# define USER_MOVING_OBJ     0x00000010 
# define OBJ_MOVING          0x00000020 

typedef struct bead_tracking
{
  int xc, yc, x0, y0, not_lost, im_prof_ref;    // bead pixel position
  int cl, cw, dis_filter, bd_mul, ncl, ncw, cross_45;   // cross size, filter def, and bd_mul define the loss of bead condition 
  int in_image, mouse_draged, mx, my;
  float last_x_not_lost;           // the bead x position saved before a rotation for instance 
  float last_y_not_lost;           // the bead y position saved before a rotation for instance 
  int start_im;                    // im number for tracking start
  int black_circle;                // flag modifying the bead tracking extend
  fft_plan *xy_trp;                // the fft plan for x, y tracking
  filter *xy_fil;                  // the filter paln for x, y tracking
  int bead_number;                 // a unique bead identifier
  int bp_center;                   // the bp center frequency
  int bp_width;                    // the bp widt frequency
  int rc;                          // the critical radius 
  int npc;                         // the number of points in radial profile 
  float apx;                       // the ratio between image pixel size in x and in radial profile 
  float apy;                       // the ratio between image pixel size in y and in radial profile 
  fft_plan *z_trp;                 // the fft plan for x, y tracking
  filter *z_fil;                   // the filter plan for x, y tracking
  float x[LOCKIN_BUFFER_SIZE], y[LOCKIN_BUFFER_SIZE], z[LOCKIN_BUFFER_SIZE];     // bead position
  int xi[256], yi[256];             // tmp int buffers to avg profile
  float xf[256], yf[256];           // tmp float buffers to do fft
  float xcor[256], ycor[256];
  float amp[LOCKIN_BUFFER_SIZE], ampds_rel[LOCKIN_BUFFER_SIZE], ampds_im[LOCKIN_BUFFER_SIZE],  amp_async1[LOCKIN_BUFFER_SIZE], amp_async2[LOCKIN_BUFFER_SIZE];//amp_2phase[LOCKIN_BUFFER_SIZE]
  char n_l[LOCKIN_BUFFER_SIZE];                       // not lost idicator
  float rad_prof_ref[PROFILE_BUFFER_SIZE];             // reference radial profile
  float rad_prof_ref_fft[PROFILE_BUFFER_SIZE];         // reference radial profile fourrier transform
  float rad_prof_fft[PROFILE_BUFFER_SIZE];             // radial profile fourrier transform
  float rad_prof[LOCKIN_BUFFER_SIZE][PROFILE_BUFFER_SIZE];                        // radial profile
  O_i *calib_im;		   // calibration image
  O_i *calib_im_fil;	           // filtered calibration image
  int profile_index;               // indicate the nearest profile in calibration image
  float min_xi2;                   // minimum found for profile 
  float min_phi;                   // phase at minimum 
  int bead_generic_index;          // specify the generic calibration type. -1 mean no generic calibration
  O_i *generic_calib_im;	   // calibration image
  O_i *generic_calib_im_fil;	   // filtered generic calibration image
  int generic_profile_index;       // indicate the nearest profile in generic calibration image
  float generic_min_xi2;           // minimum found for profile 
  float generic_min_phi;           // phase at minimum 
  O_p *opt;                        // trajectory of the last force point just acquired
  int fixed_bead;                  // specify if bead is a reference fixed beadd
  int z_track;                     // = 1 if zposition found
  float z_avgd;                   // the bead averaged z position displayed
  float z_avg;                     // the bead averaged z position
  int iz_avg;                      // the nb of bead averaged z position

} b_track;


typedef struct phase_lockin
{
                
  int phase2[50*LOCKIN_BUFFER_SIZE/3];
  int phase1[50*LOCKIN_BUFFER_SIZE/3];
  int dephi[50*LOCKIN_BUFFER_SIZE/3];
  long long imt[50*LOCKIN_BUFFER_SIZE/3]; // the image absolute time
  long long imt0;
  int c_i, n_i, m_i, ac_i;          // c_i, index of the current image, n_i size of buffer, m_i size allocated
  int peri;
  int n_phi;                        // n_phi time-steps number to lock phase
} ph_lockin;

typedef struct gen_lockin
{
  int (*user_lockin_action)(struct gen_lockin *gt, DIALOG *d, void *data);  // a user callback function       
  int c_i, n_i, m_i, lc_i;          // c_i, index of the current image, n_i size of buffer, m_i size allocated
  int ac_i, lac_i;                  // ac_i actual number of frame since acquisition started + local
  int ac_ri, a_ri;                  // the absolute and relative index of the last pending request
  int ac_pri, a_pri;                // the absolute and relative index of the previous pending request
  int x0, y0, x1, y1;               // interest region
  int cl, cw, dis_filter, bd_mul;   // cross size, filter def, and bd_mul define the loss of bead condition 
  int c_b, n_b, m_b;                // the current bead nb and the max bead nb
  int n_bead_display;               // the bead number corresponding to the plot display
  int imi[LOCKIN_BUFFER_SIZE];                         // the image nb
  int imit[LOCKIN_BUFFER_SIZE];                        // n_inarow
  long long imt[LOCKIN_BUFFER_SIZE];                   // the image absolute time
  long long imt0;
  long long imtt[LOCKIN_BUFFER_SIZE];                  // the image absolute time obtained by timer 
  unsigned long imdt[LOCKIN_BUFFER_SIZE];              // the time spent in the previous function call
  unsigned long imtdt[LOCKIN_BUFFER_SIZE];             // the time spent before image flip in timer mode
  float obj_pos[LOCKIN_BUFFER_SIZE];                   // the objective position measured

  int phase[LOCKIN_BUFFER_SIZE];                       // the modulation phase
  int ph_start;
  //long long imitphi[LOCKIN_BUFFER_SIZE];               // the image phase starting time
  //float phi[LOCKIN_BUFFER_SIZE];
  int status_flag[LOCKIN_BUFFER_SIZE];
  float obj_pos_cmd[LOCKIN_BUFFER_SIZE];               // the objective position command
  char temp_message[LOCKIN_BUFFER_SIZE];               // a string containing temperature message 
  float focus_cor;                                       // the immersion correction factor in Z
  int im_focus_check;                                    // the number of images between two ocus read per
  int xi[256], yi[256];             // tmp int buffers to avg profile
  float xf[256], yf[256];           // tmp float buffers to do fft
  b_track **bd;                    // the bead pointer

  float T0;                                              // the sensor temperature 
  float T1;                                              // the sample temperature 
  float T2;                                              // the thermal sink temperature 
  float T3;                                              // the outside temperature 
  float I0;                                              // the peltier current
  int im_temperature_check;                              // the number of images between two temperature read
  int temp_cycle;                                        // the temperature cycle position
  char temp_mes[32];                                     // the string used to save temperature
  int itemp_mes;                                         // the position of byte in string used to save temperature
  int ntemp_mes;                                         // the max position of byte in string used to save temperature
  int com_skip_fr;                                       // communication rate may be different from camera
                                                         // this is the ratio between the two
  O_i  *oi_avg_async_1, *oi_avg_async_2, *oi_avg1, *oi_avg2, *oid;//*oi_avg
  int navg;
  //int odd_even;
  int toogle_frame;
  
  int per_i;
  double dphi_t, per_t, phi_t;
  float alpha;
  double delta[LOCKIN_BUFFER_SIZE];
} g_lockin;



# ifdef _LOCKIN_UTIL_C_
struct phase_lockin ph_lock;
g_lockin *lockin_info = NULL;
int px0,px1,py0,py1;
float intensity[LOCKIN_BUFFER_SIZE];
float prev_focus = 0;


int fft_used_in_lockining = 0;
int fft_used_in_dialog = 0;
# define ISPARE_BEAD_NB 0
# else
PXV_VAR(g_lockin*, lockin_info);
PXV_VAR(int, px0);
PXV_VAR(int, px1);
PXV_VAR(int, py0);
PXV_VAR(int, py1);
PXV_VAR(int,x_center);
PXV_VAR(int,y_center);
PXV_VAR(float, prev_focus);

PXV_VAR(int, fft_used_in_lockin);
PXV_VAR(int, fft_used_in_dialog);


# endif

PXV_FUNC(g_lockin *, creating_live_video_info, (void));
PXV_FUNC(int, do_follow_bead_in_x_y,(int mode));
//PXV_FUNC(MENU*, phase_in_live_video_image_menu, (void));
# endif



