/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
 * http://www.yov408.com/html/tutorials.php?s=41
http://msdn.microsoft.com/en-us/library/ms810467.aspx
  */
# ifndef _OLYMPUS_SERIAL_C_
# define _OLYMPUS_SERIAL_C_

# include "allegro.h"
# include "winalleg.h"
# include "xvin.h"




/* If you include other plug-ins header do it here*/ 
# include "../trackBead.h"


/* But not below this define */
# define BUILDING_PLUGINS_DLL

# define STAND_ALONE

int rs232_started_fine = 0;

# ifdef STAND_ALONE
# define RS232_STARTED_FINE rs232_started_fine
# elif
# define RS232_STARTED_FINE pico_param.serial_param.rs232_started_fine
# endif

# undef _PXV_DLL
# define _PXV_DLL   __declspec(dllexport)
# include "olympus_serial.h"

# include "../../cfg_file/Pico_cfg.h"
# include "../action.h"
# include "../fftbtl32n.h"
# include "../fillibbt.h"

# include "../trackBead.h"
# include "../track_util.h"
unsigned long t_rs = 0, dt_rs;

UINT timeout = 200;  // ms

char last_answer_2[128];


HANDLE init_serial_port(unsigned short port_number, int baudrate, int n_bits, int parity, int stops, int RTSCTS)
{
    HANDLE hCom_port;


    //win_printf("entering init ");

    sprintf( str_com, "\\\\.\\COM%d\0", port_number);
    hCom_port = CreateFile(str_com,GENERIC_READ|GENERIC_WRITE,0,NULL,
        OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED,NULL);
    PurgeComm(hCom_port, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);
    if (GetCommState( hCom_port, &lpCC.dcb) == 0) 
    {
        win_printf("GetCommState FAILED\n port %d baudrate %d \nbits %d parity %d stops %d RTSCTS %d\n"
		   ,port_number, baudrate, n_bits, parity, stops, RTSCTS);
        return NULL;
    }    

    /* Initialisation des parametres par defaut */
    /*http://msdn.microsoft.com/library/default.asp?url=/library/en-us/devio/base/dcb_str.asp*/
    //win_printf("lpCC.dcb.BaudRate = %d",lpCC.dcb.BaudRate);
    lpCC.dcb.BaudRate = baudrate;//CBR_57600;//CBR_115200;
    //lpCC.dcb.BaudRate = CBR_115200;
    //win_printf("lpCC.dcb.BaudRate = %d",lpCC.dcb.BaudRate);
    lpCC.dcb.ByteSize = n_bits; //8;
    lpCC.dcb.StopBits = (stops == 1) ? ONESTOPBIT : TWOSTOPBITS;//stops;//ONESTOPBIT;
    lpCC.dcb.Parity = parity;//NOPARITY;
    lpCC.dcb.fOutX = FALSE;
    lpCC.dcb.fInX = FALSE;


    if (RTSCTS == 0)
      {
	lpCC.dcb.fDtrControl = DTR_CONTROL_DISABLE;//DTR_CONTROL_HANDSHAKE or DTR_CONTROL_DISABLE;
	lpCC.dcb.fRtsControl = RTS_CONTROL_DISABLE;//RTS_CONTROL_HANDSHAKE
      }
    else
      {
	lpCC.dcb.fDtrControl = DTR_CONTROL_HANDSHAKE;// or DTR_CONTROL_DISABLE;
	lpCC.dcb.fRtsControl = RTS_CONTROL_HANDSHAKE;
	lpCC.dcb.fOutxCtsFlow = TRUE;
      }
 
 
    if (SetCommState( hCom_port, &lpCC.dcb )== 0) 
    {
        win_printf("SetCommState FAILED\n port %d baudrate %d \nbits %d parity %d stops %d RTSCTS %d\n"
		   ,port_number, baudrate, n_bits, parity, stops, RTSCTS);
        return NULL;
    }    

    //Delay(60);

    if (GetCommTimeouts(hCom_port,&lpTo)== 0) 
    {
        win_printf("GetCommTimeouts FAILED\n port %d baudrate %d \nbits %d parity %d stops %d RTSCTS %d\n"
		   ,port_number, baudrate, n_bits, parity, stops, RTSCTS);
        return NULL;
    }
        
    lpTo.ReadIntervalTimeout = timeout;   // 100 ms max entre characters
    lpTo.ReadTotalTimeoutMultiplier = 1;
    lpTo.ReadTotalTimeoutConstant = 1;
    lpTo.WriteTotalTimeoutMultiplier = 1;
    lpTo.WriteTotalTimeoutConstant = 1;
    
    if (SetCommTimeouts(hCom_port,&lpTo)== 0) 
    {
        win_printf("SetCommTimeouts FAILED\n port %d baudrate %d \nbits %d parity %d stops %d RTSCTS %d\n"
		   ,port_number, baudrate, n_bits, parity, stops, RTSCTS);
        return NULL;
    }    
    
    if (SetupComm(hCom_port,2048,2048) == 0) 
    {
        win_printf("Init Serial port %d FAILED",port_number);
        return NULL;
    }
    my_set_window_title("Init Serial port %d OK",port_number);
    return hCom_port;
}


int Write_serial_port_tmp(HANDLE handle, char *data, DWORD length)
{
  int success = 0, i, nt = 0;
  char tmpch[2];
  DWORD dwWritten = 0;
  OVERLAPPED o= {0};
  unsigned long t0;


  for (i = 0; i < length; i++)
    {
      tmpch[0] = data[i];
      tmpch[1] = 0;
      success = 0;
      o.hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

      t0 = get_my_uclocks_per_sec()/20; // we cannot wait more than 1000 ms
      t0 += my_uclock();
      if (!WriteFile(handle, (LPCVOID)tmpch, 1, &dwWritten, &o))
	{
	  if (GetLastError() == ERROR_IO_PENDING)
	    if (WaitForSingleObject(o.hEvent, timeout) == WAIT_OBJECT_0)
	      if (GetOverlappedResult(handle, &o, &dwWritten, FALSE))
		{
		  success = 1;
		  nt++;
		}
	}
      else    
	{
	  success = 1;
	  nt++;
	}
      CloseHandle(o.hEvent);
      if (debug)
	{
	  fp_debug = fopen(debug_file,"a");
	  if (fp_debug != NULL)
	    {
	      if (success)    
		fprintf (fp_debug,">pb writting char\n");
	      fprintf (fp_debug,">\t %s\n",tmpch);
	      fclose(fp_debug);
	    }
	}

      for ( ;my_uclock() < t0; );
    }
  if (nt != length)     success = 0;

  return (success == 0) ? -1 : dwWritten;
}




int Write_serial_port(HANDLE handle, char *data, DWORD length)
{
  int success = 0;
  DWORD dwWritten = 0;
  OVERLAPPED o= {0};

  o.hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
  if (!WriteFile(handle, (LPCVOID)data, length, &dwWritten, &o))
    {
      if (GetLastError() == ERROR_IO_PENDING)
	if (WaitForSingleObject(o.hEvent, timeout) == WAIT_OBJECT_0)
	  if (GetOverlappedResult(handle, &o, &dwWritten, FALSE))
	    success = 1;
    }
  else    success = 1;
  if (dwWritten != length)     success = 0;
  CloseHandle(o.hEvent);
  if (debug)
    {
      fp_debug = fopen(debug_file,"a");
      if (fp_debug != NULL)
	{
	  if (dwWritten != length)    
	    fprintf (fp_debug,">pb writting %d char instead of %d\n",(int)dwWritten,(int)length);
	  fprintf (fp_debug,">\t %s\n",data);
	  fclose(fp_debug);
	}
    }
  return (success == 0) ? -1 : dwWritten;
}


int ReadData(HANDLE handle, BYTE* data, DWORD length, DWORD* dwRead)
{
  int success = 0;
  OVERLAPPED o ={0};

  o.hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
  if (!ReadFile(handle, data, length, dwRead, &o))
    {
      if (GetLastError() == ERROR_IO_PENDING)
	if (WaitForSingleObject(o.hEvent, timeout) == WAIT_OBJECT_0)
	  success = 1;
      GetOverlappedResult(handle, &o, dwRead, FALSE);
    }
  else    success = 1;
  CloseHandle(o.hEvent);
  return success;
}


int Read_serial_port(HANDLE handle, char *stuff, DWORD max_size, DWORD *dwRead)
{
  register int i, j;
  unsigned long t0;
  char lch[2];
  int timeout = 0;
  
  if (handle == NULL) return -2;


  t0 = get_my_uclocks_per_sec(); // we cannot wait more than 1000 ms
  t0 += my_uclock();
  for (i = j = 0; j == 0 && i < max_size-1 && timeout == 0; )
    {
      *dwRead = 0;
      if (ReadData(handle, lch, 1, dwRead) == 0)
	{
	  stuff[i] = 0;
	  *dwRead = i;
	  if (debug)
	    {
	      fp_debug = fopen(debug_file,"a");
	      if (fp_debug != NULL)
		{
		  fprintf (fp_debug,"<-- ReadData error\n");
		  fprintf (fp_debug,"<\t %s (t = %g)\n",stuff,(double)(my_uclock()-t_rs)/dt_rs);
		  fclose(fp_debug);
		}
	    }
	  return -1;
	}
      /* 
      if (*dwRead < 1) 
	{
	  stuff[i] = 0;
	  *dwRead = i;
	  if (debug)
	    {
	      fp_debug = fopen(debug_file,"a");
	      if (fp_debug != NULL)
		{
		  fprintf (fp_debug,"<-- ReadData 0 char read\n");
		  fprintf (fp_debug,"<\t %s\n",stuff);
		  fclose(fp_debug);
		}
	    }
	  return -2;
	}
      */
      if (*dwRead == 1) 
	{
	  stuff[i] = lch[0];
	  j = (lch[0] == '\n') ? 1 : 0;
	  i++;
	}
      timeout = (t0 > my_uclock()) ? 0 : 1;
    }
  stuff[i] = 0;
  *dwRead = i;

  if (debug)
    {
      fp_debug = fopen(debug_file,"a");
      if (fp_debug != NULL)
	{
	  if (timeout) fprintf (fp_debug,"<\t timeout %s (t = %g)\n",stuff,(double)(my_uclock()-t_rs)/dt_rs);
	  else fprintf (fp_debug,"<\t %s (t = %g)\n",stuff,(double)(my_uclock()-t_rs)/dt_rs);
	  fclose(fp_debug);
	}
    }
  return (timeout) ? -2 : i;
}




int sent_cmd_and_read_answer(HANDLE hCom, char *Command, char *answer, DWORD n_answer, DWORD *n_written, DWORD *dwErrors, unsigned long *t0)
{
  int rets, ret, i, j;
  COMSTAT comStat;
  char l_command[128], chtmp[128]; // , ch[2]
  DWORD len = 0;

  if (hCom == NULL || Command == NULL || answer == NULL || strlen(Command) < 1) return 1;

    for (i = 0, j = 1; i < 5 && j != 0; i++)
      {
	j = ClearCommError(hCom, dwErrors, &comStat);
	if (j && comStat.cbInQue)   Read_serial_port(hCom,chtmp,127,&len);
	//if (j && comStat.fCtsHold)  my_set_window_title(" Tx waiting for CTS signal");
	//if (j && comStat.fDsrHold)  my_set_window_title(" Tx waiting for DSR signal");
	//if (j && comStat.fRlsdHold) my_set_window_title(" Tx waiting for RLSD signal");
	//if (j && comStat.fXoffHold) my_set_window_title(" Tx waiting, XOFF char rec'd");
	//if (j && comStat.fXoffSent) my_set_window_title(" Tx waiting, XOFF char sent");
	//if (j && comStat.fEof)      my_set_window_title(" EOF character received");
	if (j && comStat.fTxim)     my_set_window_title(" Character waiting for Tx; char queued with TransmitCommChar");
	//if (j && comStat.cbInQue)   my_set_window_title(" comStat.cbInQue bytes have been received, but not read");
	if (j && comStat.cbOutQue)  my_set_window_title(" comStat.cbOutQue bytes are awaiting transfer");
      }


  for (i = 0; i < 126 && Command[i] != 0 && Command[i] != 10; i++)
    l_command[i] = Command[i]; // we copy until LF
  l_command[i++] = 10;         // we add it
  l_command[i] = 0;            // we end string
  if (t0) *t0 = my_uclock();
  /*
  for (i = 0; i < 126 && l_command[i] != 0; i++)
    {
      ch[0] = l_command[i];
      ch[1] = 0;
      rets = Write_serial_port(hCom,ch,1);
      if (rets <= 0)
	{
	  if (t0) *t0 = my_uclock() - *t0;
	  // Get and clear current errors on the port.
	  if (ClearCommError(hCom, dwErrors, &comStat)) 	
	    {
	      if (comStat.cbInQue)  Read_serial_port(hCom,chtmp,127,&len);
	      return -2;
	    }
	  return -1;
	}
    }
  */

  rets = Write_serial_port(hCom,l_command,strlen(l_command));
  if (rets <= 0)
    {
      if (t0) *t0 = my_uclock() - *t0;
      // Get and clear current errors on the port.
      if (ClearCommError(hCom, dwErrors, &comStat)) 	
	{
	  if (comStat.cbInQue)  Read_serial_port(hCom,chtmp,127,&len);
	  //if (comStat.fCtsHold)  my_set_window_title(" Tx waiting for CTS signal");
	  //if (comStat.fDsrHold)  my_set_window_title(" Tx waiting for DSR signal");
	  //if (comStat.fRlsdHold) my_set_window_title(" Tx waiting for RLSD signal");
	  //if (comStat.fXoffHold) my_set_window_title(" Tx waiting, XOFF char rec'd");
	  //if (comStat.fXoffSent) my_set_window_title(" Tx waiting, XOFF char sent");
	  //if (comStat.fEof)      my_set_window_title(" EOF character received");
	  if (comStat.fTxim)     my_set_window_title(" Character waiting for Tx; char queued with TransmitCommChar");
	  //if (comStat.cbInQue)   my_set_window_title(" comStat.cbInQue bytes have been received, but not read");
	  if (comStat.cbOutQue)  my_set_window_title(" comStat.cbOutQue bytes are awaiting transfer");
	  return -2;
	}
      return -1;
    }

  *n_written = 0;
  ret = Read_serial_port(hCom,answer,n_answer,n_written);
  for  (; ret == 0; )
    ret = Read_serial_port(hCom,answer,n_answer,n_written);
  if (t0) *t0 = my_uclock() - *t0;
  answer[*n_written] = 0;
  strncpy(last_answer_2,answer,127);
  if (ret < 0)
    {
      // Get and clear current errors on the port.
      if (ClearCommError(hCom, dwErrors, &comStat)) 	
	{
	  if (comStat.cbInQue)  Read_serial_port(hCom,chtmp,127,&len);
	  //if (comStat.fCtsHold)  my_set_window_title(" Tx waiting for CTS signal");
	  //if (comStat.fDsrHold)  my_set_window_title(" Tx waiting for DSR signal");
	  //if (comStat.fRlsdHold) my_set_window_title(" Tx waiting for RLSD signal");
	  //if (comStat.fXoffHold) my_set_window_title(" Tx waiting, XOFF char rec'd");
	  //if (comStat.fXoffSent) my_set_window_title(" Tx waiting, XOFF char sent");
	  //if (comStat.fEof)      my_set_window_title(" EOF character received");
	  if (comStat.fTxim)     my_set_window_title(" Character waiting for Tx; char queued with TransmitCommChar");
	  //if (comStat.cbInQue)   my_set_window_title(" comStat.cbInQue bytes have been received, but not read");
	  if (comStat.cbOutQue)  my_set_window_title(" comStat.cbOutQue bytes are awaiting transfer");
	  return 2;
	}
      return 1;      
    }
  return 0;
}

int talk_serial_port(HANDLE hCom_port, char *command, int wait_answer, char *answer, DWORD NumberOfBytesToWrite, DWORD nNumberOfBytesToRead)
{
    int NumberOfBytesWrittenOnPort = -1;
    unsigned long NumberOfBytesWritten = -1;
    

    if (hCom == NULL) return win_printf_OK("No serial port init�");
    if (wait_answer != READ_ONLY)
    {
        NumberOfBytesWrittenOnPort = Write_serial_port(hCom_port, command, NumberOfBytesToWrite);
    }    
		
    if (wait_answer != WRITE_ONLY) 
    {
        Read_serial_port(hCom_port,answer,nNumberOfBytesToRead,&NumberOfBytesWritten);
    
    }	
    return 0;
}

/*
Attemp to read the pico response from serial port
if a full answer is available then transfer it
otherwise grab characters and respond that full message is not yet there

*/

char *catch_pending_request_answer(int *size, int im_n, int *error, unsigned long *t0)
{
  static char answer[128], request_answer[128];
  unsigned long dwRead = 0;
  static int i = 0;
  int j, max_size = 128, lf_found = 0;
  char lch[2];

  if (t0 != NULL) *t0 = my_uclock();
  if (hCom == NULL) 
   {
     *error = -3;
     if (t0 != NULL) *t0 = my_uclock()- *t0;
     return NULL;   // no serial port !
   }

  for (j = 0; j == 0 && i < max_size-1; )
    {// we grab rs232 char already arrived
      dwRead = 0;
      if (ReadData(hCom, lch, 1, &dwRead) == 0)
	{  // something wrong
	  answer[i] = 0;
	  dwRead = i;
	  if (debug)
	    {
	      fp_debug = fopen(debug_file,"a");
	      if (fp_debug != NULL)
		{
		  fprintf (fp_debug,"<-- ReadData error at im %d\n", im_n);
		  fprintf (fp_debug,"<\t %s\n",answer);
		  fclose(fp_debug);
		}
	    }
	  *error = -2;
	  if (t0 != NULL) *t0 = my_uclock()- *t0;
	  return NULL;    // serial port read error !
	}
      if (dwRead == 1) 
	{
	  answer[i] = lch[0];
	  j = lf_found = (lch[0] == '\n') ? 1 : 0;
	  i++;
	}
      else if (dwRead == 0) j = 1; 
    }
  answer[i] = 0;
  *size = i;
  if (lf_found)
    {
      *error = 0;
      strncpy(request_answer, answer,i);
      if (debug)
	{
	  fp_debug = fopen(debug_file,"a");
	  if (fp_debug != NULL)
	    {
	      fprintf (fp_debug,"Im %d : <\t %s\n",im_n,request_answer);
	      fclose(fp_debug);
	    }
	}
      if (t0 != NULL) *t0 = my_uclock()- *t0;
      i = 0;
      return request_answer;
    }
  *error = 1;
  if (debug)
    {
      fp_debug = fopen(debug_file,"a");
      if (fp_debug != NULL)
	{
	  fprintf (fp_debug,"Im %d : <\t %s...\n",im_n,answer);
	  fclose(fp_debug);
	}
    }
  if (t0 != NULL) *t0 = my_uclock()- *t0;
  return NULL;
}




int n_write_read_serial_port(void)
{
    static char Command[128]="dac16?";
    static int ntimes = 1, i;
    unsigned long t0;
    double dt, dtm, dtmax;
    char resu[128], *ch;
    int ret = 0;
    unsigned long lpNumberOfBytesWritten = 0;
    DWORD nNumberOfBytesToRead = 127;
    
    if(updating_menu_state != 0)	return D_O_K;

    if (hCom == NULL) return win_printf_OK("No serial port init�");    
    for (ch = Command; *ch != 0; ch++) 
      if (*ch == '\r') *ch = 0;
    win_scanf("Command to send? %snumber of times %d",&Command,&ntimes);
       
    strncat(Command,"\r\n",(sizeof(Command) > strlen(Command)) ? sizeof(Command)-strlen(Command) : 0);    

    for(i = 0, dtm = dtmax = 0; i < ntimes; i++)
      {
	t0 = my_uclock();    
	Write_serial_port(hCom,Command,strlen(Command));

	ret = Read_serial_port(hCom,resu,nNumberOfBytesToRead,&lpNumberOfBytesWritten);
	t0 = my_uclock() - t0;
	dt = 1000*(double)(t0)/get_my_uclocks_per_sec();
	resu[lpNumberOfBytesWritten] = 0;
	dtm += dt;
	if (dt > dtmax) dtmax = dt;
      }
    dtm /= ntimes;
    win_printf("command read = %s \n ret %d lpNumberOfBytesWritten = %d\nin %g mS in avg %g max",resu,ret,lpNumberOfBytesWritten,dtm, dtmax);
    return D_O_K;
}

int purge_com(void)
{
   if (hCom == NULL) return 1; 
  return PurgeComm(hCom, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);
}
int init_pico_serial(void)
{
  int i, j;
  static int port_number = 1, sbaud = 1, hand = 0, parity = 2, stops = 2, n_bits = 8;
  char command[128], answer[128], *gr;
  int  ret, iret;
  unsigned long len = 0;
  

  if (hCom != NULL) return 0;


 # ifndef STAND_ALONE

  port_number = Pico_param.serial_param.serial_port;
  parity = Pico_param.serial_param.serial_Parity;
  stops = Pico_param.serial_param.serial_StopBits;
  n_bits = Pico_param.serial_param.serial_ByteSize;
  sbaud = Pico_param.serial_param.serial_BaudRate;

  sbaud /= 9600;
  sbaud--;
  hand = Pico_param.serial_param.serial_fDtrControl;

# endif


  if (state_is_pico() == 0)
    {
      i = win_scanf("Your interface device has been switched to a dummy mode\n"
		    "in the previous experiment. This is a debugging mode\n"
		    "which does not allow normal operation. Do you want to\n"
		    "switch back your configuration to the normal state ?");
      if (i == OK)  switch_from_pico_serial_to_dummy(1); 
    }



  if (RS232_STARTED_FINE == 0)
    {
      for ( ; hCom == NULL; )
	{
	  i = win_scanf("{\\color{yellow}Your Picotwist serial interface did not open properly on last trial!} Check:\n"
			" - that another program is not using the same serial port\n"
			" - Another instance of Picojai.exe may still be running\n"
			"    If this is the case open the Windows task manager and kill the process\n"
			" - Last but not least your configuration may not be correct\n"
			"If this is the case select a new one. {\\color{yellow}Port number:} ?%5d\n"
			"Dump in and out in a debug file No %R yes %r\n"
			"{\\color{yellow}Baudrate:} 9.6k %R 19.2k %r 38.4 %r (default 19.2k)\n"
			"{\\color{yellow}Number of bits:} %4d  (default 8)\n"
			"{\\color{yellow}Parity:} No %R odd %r even %r (default No)\n"
			"{\\color{yellow}Number of stop bits:} 0->%R 1->%r 2->%r (default 2)\n"
			"{\\color{yellow}HandShaking:} None %R Cts/Rts %r (default None)\n"
			"{\\color{lightred}If you hit cancel a dummy interface will be used\n"
			"This only allows you to check your camera}\n"
			,&port_number, &debug,&sbaud,&n_bits,&parity,&stops,&hand);
	  if (i == CANCEL) 
	    {
	      RS232_STARTED_FINE = 0;
 # ifndef STAND_ALONE
	      switch_from_pico_serial_to_dummy(0);
	      write_Pico_config_file();
# endif
	      return 1;
	    }
	  hCom = init_serial_port(port_number, (sbaud+1)*9600,n_bits,parity ,stops, hand);
	}
    }
  if (hCom == NULL)  hCom = init_serial_port(port_number, (sbaud+1)*9600,n_bits,parity ,stops, hand);
  for ( ; hCom == NULL; )
    {
      i = win_scanf("{\\color{yellow}Your Picotwist serial interface is not opening properly!} Check:\n"
		    " - that another program is not using the same serial port\n"
		    " - Another instance of Picojai.exe may still be running\n"
		    "    If this is the case open the Windows task manager and kill the process\n"
		    " - Last but not least your configuration may not be correct\n"
		    "If this is the case select a new one. {\\color{yellow}Port number:} ?%5d\n"
		    "Dump in and out in a debug file No %R yes %r\n"
		    "{\\color{yellow}Baudrate:} 9.6k %R 19.2k %r 38.4 %r (default 19.2k)\n"
		    "{\\color{yellow}Number of bits:} %4d  (default 8)\n"
		    "{\\color{yellow}Parity:} No %R odd %r even %r (default No)\n"
		    "{\\color{yellow}Number of stop bits:} 0->%R 1->%r 2->%r (default 2)\n"
		    "{\\color{yellow}HandShaking:} None %R Cts/Rts %r (default None)\n"
		    "{\\color{lightred}If you hit cancel a dummy interface will be used\n"
		    "This only allows you to check your camera}\n"
		    ,&port_number, &debug,&sbaud,&n_bits,&parity,&stops,&hand);
      if (i == CANCEL) 
	{
	  RS232_STARTED_FINE = 0;
 # ifndef STAND_ALONE
	  switch_from_pico_serial_to_dummy(0);
	  write_Pico_config_file();
# endif
	  return 1;
	}
      hCom = init_serial_port(port_number, (sbaud+1)*9600,n_bits,parity ,stops, hand);
    }
  if (hCom == NULL)
    {
      RS232_STARTED_FINE = 0;
 # ifndef STAND_ALONE
      switch_from_pico_serial_to_dummy(0);
      write_Pico_config_file();
# endif
      return 1;
    }
  if (debug)
    {
      fp_debug = fopen(debug_file,"w");
      if (fp_debug != NULL) fclose(fp_debug);
    }
  purge_com();
  
  sprintf(command,"1LOG IN\r\n");
  for (j = Write_serial_port(hCom,command,strlen(command)), i = 0; j < 0 && i < 2; i++)
    {
      win_printf("Communication iteration");
      j = Write_serial_port(hCom,command,strlen(command));
    }
  for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);
  answer[len] = 0;
  //win_printf("init answer %s len %d",answer,len);
  gr = strstr(answer,"1LOG +");
  if (gr == NULL)
      {
	win_printf("Bad microscope initialization %s",answer);
	return 1;
      }

  sprintf(command,"2LOG IN\r\n");
  for (j = Write_serial_port(hCom,command,strlen(command)), i = 0; j < 0 && i < 2; i++)
    {
      win_printf("Communication iteration");
      j = Write_serial_port(hCom,command,strlen(command));
    }
  for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);
  answer[len] = 0;
  gr = strstr(answer,"2LOG +");
  //win_printf("init 2 answer %s",answer);
  if (gr == NULL)
      {
	win_printf("Bad microscope initialization %s",answer);
	return 1;
      }
  
  if (gr != NULL) 
    {
      my_set_window_title("Olympus init");
      RS232_STARTED_FINE = 1;
 # ifndef STAND_ALONE
      Pico_param.serial_param.rs232_started_fine = 1;
      Pico_param.serial_param.serial_port = port_number;
      Pico_param.serial_param.serial_Parity = parity;
      Pico_param.serial_param.serial_StopBits = stops;
      Pico_param.serial_param.serial_ByteSize = n_bits;
      Pico_param.serial_param.serial_BaudRate = (sbaud+1)*9600;
      Pico_param.serial_param.serial_fDtrControl = hand;
      Pico_param.serial_param.serial_fRtsControl = hand;
      write_Pico_config_file();
# endif
    }
  else
    {
      RS232_STARTED_FINE = 0;
 # ifndef STAND_ALONE
      Pico_param.serial_param.rs232_started_fine = 0;
      write_Pico_config_file();
 # endif
    }
  return (gr != NULL) ? 0 : 1;
}
int init_port(void)
{
    if(updating_menu_state != 0)	return D_O_K;
  if (hCom == NULL) 
    {

      add_plot_treat_menu_item ( "Olympus serial", NULL, olympus_serial_plot_menu(), 0, NULL);
      add_image_treat_menu_item ( "Olympus serial", NULL, olympus_serial_plot_menu(), 0, NULL);
    }
    init_pico_serial();
    return D_O_K;
}



int write_command_on_serial_port(void)
{
    static char Command[128]="test";
    //char *test="\r";
    
    if(updating_menu_state != 0)	return D_O_K;
    
    win_scanf("Command to send? %s",&Command);
       
    strncat(Command,"\r\n",(sizeof(Command) > strlen(Command)) ? sizeof(Command)-strlen(Command) : 0);      
     //win_printf("deja cela %d\n Command %s",strlen(Command), Command);
    Write_serial_port(hCom,Command,strlen(Command));
   //snprintf(Command,128,"?nm\r");
   //strcat(Command,'\r');
    //win_printf("deja cela %d\n Command %s",strlen(Command), Command);
    
    //win_printf("%s \n lpNumberOfBytesWritten = %d",Command,lpNumberOfBytesWritten);
    return D_O_K;
}


int read_on_serial_port(void)
{
    char Command[128];
    unsigned long lpNumberOfBytesWritten = 0;
    int ret = 0;
    DWORD nNumberOfBytesToRead = 127;
    unsigned long t0;
    
    if(updating_menu_state != 0)	return D_O_K;

    t0 = my_uclock();        
    //win_scanf("Command to send? %s",Command);
    ret = Read_serial_port(hCom,Command,nNumberOfBytesToRead,&lpNumberOfBytesWritten);
    t0 = my_uclock() - t0;
    Command[lpNumberOfBytesWritten] = 0;

    win_printf("command read = %s \n ret %d lpNumberOfBytesWritten = %d\n in %g mS",Command,ret,lpNumberOfBytesWritten,1000*(double)(t0)/get_my_uclocks_per_sec());

    //win_printf("command read = %s \n lpNumberOfBytesWritten = %d",Command,lpNumberOfBytesWritten);
    return D_O_K;
}



int write_read_serial_port(void)
{
        if(updating_menu_state != 0)	return D_O_K;
    
        write_command_on_serial_port();
        read_on_serial_port();
        return D_O_K;
}



        
int CloseSerialPort(HANDLE hCom)
{
    if (CloseHandle(hCom) == 0) return win_printf("Close Handle FAILED!");
    return D_O_K;
}

int close_serial_port(void)
{
        if(updating_menu_state != 0)	return D_O_K;
        CloseSerialPort(hCom);
	hCom = NULL;
        return D_O_K;
}

/////////////////////


# ifdef FORTEST

static float pico_obj_position = 0;
static int olympus_obj_reference_pos = 0, olympus_obj_pos = 0; // in 10nm units 

float _read_Z_value_accurate_in_micron(void)
{
  char command[128], answer[128];
  int  set = -1000000000, ret;
  unsigned long len = 0;
  //unsigned long t0;
  DWORD dwErrors;

  
  snprintf(command,128,"2POS?\r\n");
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  //win_printf("Question %s\nAnswer %s",command,answer);
  if (ret != 0) return (ret -50000000);
  if (sscanf(answer,"2POS %d",&set) == 1)
    { 
      if (set != -1000000000)
	{
	  olympus_obj_pos = set;
	  pico_obj_position = (float)(olympus_obj_pos - olympus_obj_reference_pos)/100;
	  return pico_obj_position;
	}
    }
  return -2000000000;
}


int _set_Z_value(float z)  /* in microns */
{
  char command[128], answer[128];
  int dz, ret, im = 0;
  unsigned long len = 0;
  DWORD dwErrors;
  char mov[2];
  
  //purge_com();

  mov[0] = (z > pico_obj_position) ? 'N' : 'F';
  mov[1] = 0;
  dz = (int)(0.5+(100*z)) - olympus_obj_pos + olympus_obj_reference_pos;
  dz = abs(dz);
  snprintf(command,128,"2MOV %s,%d\r\n",mov,dz);
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return (ret -5);
  if (strstr(answer,"2MOV +") == NULL)
    { 
      if (debug)
	{
	  fp_debug = fopen(debug_file,"a");
	  if (fp_debug != NULL)
	    {
	      //if (track_info != NULL) im = track_info->ac_i;
	      fprintf (fp_debug,"Im %d : focus pb setting %g !\n",im,z);
	      fclose(fp_debug);
	    }
	}
      return -1;
    }
  else      
    {
      olympus_obj_pos += (mov[0] == 'N') ? dz : -dz; 
      pico_obj_position = (float)(olympus_obj_pos - olympus_obj_reference_pos)/100;
    }
  return 0;
}



int _set_Z_origin(void)  // in microns 
{
  char command[128], answer[128];
  int  ret, im = 0;
  unsigned long len = 0;
  DWORD dwErrors;
  
  //purge_com();

  snprintf(command,128,"2INITORG\r\n");
  win_printf("command: %s",command);
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return (ret -5);
  if (strstr(answer,"2INITORG +") == NULL)
    { 
      if (debug)
	{
	  fp_debug = fopen(debug_file,"a");
	  if (fp_debug != NULL)
	    {
	      //if (track_info != NULL) im = track_info->ac_i;
	      fprintf (fp_debug,"Im %d : focus pb init org !\n",im);
	      fclose(fp_debug);
	    }
	}
      win_printf("Init org pb!");
      return -1;
    }
  else      pico_obj_position = 0;
  return 0;
}


int set_olympus_obj_ref(void)
{
  int i, zr;
  char question[1024];

  if(updating_menu_state != 0)	return D_O_K;
  
  snprintf(question,1024,"Olympus reference focus position setting\n"
	   "Present relative objective position %g microns\n"
	   "Present absolute objective position %d x 10 nm\n"
	   "New reference position (x 10 nm) %%12d\n",pico_obj_position,olympus_obj_pos);
  zr = olympus_obj_reference_pos;

  i = win_scanf(question,&zr);
  if (i == CANCEL) return OFF;
  olympus_obj_reference_pos = zr;
  win_printf("Focus is now at %g",_read_Z_value_accurate_in_micron());
  return D_O_K;
}


int read_focus(void)
{
    if(updating_menu_state != 0)	return D_O_K;

    win_printf("focus %g",_read_Z_value_accurate_in_micron());
    return D_O_K;
}


int set_focus(void)
{
  int i;
  float z = pico_obj_position;

  if(updating_menu_state != 0)	return D_O_K;
  
  i = win_scanf("New focus position %f",&z);
  if (i == CANCEL) return OFF;
  _set_Z_value(z);
  //win_printf("focus set at %g",_read_Z_value_accurate_in_micron());
  return D_O_K;
}

# endif

///////////////////////





MENU *olympus_serial_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"Init serial port", init_port,NULL,0,NULL);
	add_item_to_menu(mn,"Write serial port", write_command_on_serial_port,NULL,0,NULL);
	add_item_to_menu(mn,"Read serial port", read_on_serial_port,NULL,0,NULL);
	add_item_to_menu(mn,"Write read serial port", write_read_serial_port,NULL,0,NULL);
	add_item_to_menu(mn,"Write read n times", n_write_read_serial_port,NULL,0,NULL);
	add_item_to_menu(mn,"Close serial port", close_serial_port,NULL,0,NULL);
	/*
	add_item_to_menu(mn,"Read focus", read_focus,NULL,0,NULL);
	add_item_to_menu(mn,"set focus", set_focus,NULL,0,NULL);
	add_item_to_menu(mn,"set focus origine",set_olympus_obj_ref,NULL,0,NULL);
	*/
	return mn;
}

int	olympus_serial_main(int argc, char **argv)
{
  init_port();
  if (hCom != NULL) return D_O_K; 
  add_plot_treat_menu_item ( "olympus_serial", NULL, olympus_serial_plot_menu(), 0, NULL);
  add_image_treat_menu_item ( "olympus_serial", NULL, olympus_serial_plot_menu(), 0, NULL);
  return D_O_K;
}

int	olympus_serial_unload(int argc, char **argv)
{
	remove_item_to_menu(plot_treat_menu, "olympus_serial", NULL, NULL);
	return D_O_K;
}
#endif

