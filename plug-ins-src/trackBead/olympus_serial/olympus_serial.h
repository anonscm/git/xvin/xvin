#ifndef _OLYMPUS_SERIAL_H_
#define _OLYMPUS_SERIAL_H_


#define READ_ONLY 0
#define WRITE_ONLY 1
#define WRITE_READ 2

# ifndef _OLYMPUS_SERIAL_C_
PXV_VAR(HANDLE, hCom);
PXV_VAR(COMMTIMEOUTS, lpTo);
PXV_VAR(COMMCONFIG, lpCC);
PXV_VAR(char, str_com[32]);

# endif

# ifdef _OLYMPUS_SERIAL_C_
HANDLE hCom = NULL;
COMMTIMEOUTS lpTo;
COMMCONFIG lpCC;
char str_com[32];

char *debug_file = "c:\\serial_debug.txt";
int debug = 0;
FILE *fp_debug = NULL;

# endif
PXV_FUNC(int,Write_serial_port,(HANDLE hCom_port, char *ch, DWORD NumberOfBytesToWrite));
PXV_FUNC(int,Read_serial_port,(HANDLE hCom_port, char *ch, DWORD nNumberOfBytesToRead, unsigned long *lpNumberOfBytesWritten));
PXV_FUNC(int,talk_serial_port,(HANDLE hCom_port, char *command, int wait_answer, char * answer, DWORD NumberOfBytesToWrite, DWORD nNumberOfBytesToRead));
PXV_FUNC(int, CloseSerialPort,(HANDLE hCom));
//PXV_FUNC(HANDLE,init_serial_port,(unsigned short port_number));
PXV_FUNC(HANDLE, init_serial_port, (unsigned short port_number, int baudrate, int n_bits, int parity, int stops, int RTSCTS));
PXV_FUNC(int, write_command_on_serial_port,(void));
PXV_FUNC(int, read_on_serial_port,(void));
PXV_FUNC(int, purge_com,(void));
PXV_FUNC(int, close_serial_port,(void));
PXV_FUNC(MENU*, serialw_menu, (void));
PXV_FUNC(int, serialw32_main, (int argc, char **argv));

PXV_FUNC(int, init_pico_serial, (void));

PXV_FUNC(MENU*, olympus_serial_plot_menu, (void));
PXV_FUNC(int, olympus_serial_main, (int argc, char **argv));

PXV_FUNC(int, sent_cmd_and_read_answer, (HANDLE hCom, char *Command, char *answer, DWORD n_answer, 
					 DWORD *n_written, DWORD *dwErrors, unsigned long *t0));


PXV_VAR(char*, debug_file);
PXV_VAR(int, debug);
PXV_VAR(FILE *, fp_debug);



#endif

