#ifndef _PI_E761_FOCUS_H
#define PI_E761_FOCUS_H


# ifndef _PI_E761_FOCUS_C_

PXV_VAR(float ,  PI_E761_obj_position);
PXV_VAR(float , PI_E761_Z_step);
PXV_VAR(float ,  PI_E761_X_position);
PXV_VAR(float ,  PI_E761_Y_position);
PXV_VAR(int , handle);
PXV_VAR(double, Cal);

# endif

# ifdef _PI_E761_FOCUS_C_
float   PI_E761_obj_position = 0;
float  PI_E761_Z_step = 0.1;
float   PI_E761_X_position = 0;
float   PI_E761_Y_position = 0;
int handle = -1;
double Cal = 100;

# endif

PXV_FUNC(int, PI_E761_focus_main, (int argc, char **argv));





#endif
