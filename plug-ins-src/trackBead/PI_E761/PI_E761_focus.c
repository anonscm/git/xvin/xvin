/** \file PI_E761_FOCUS.c
    \brief Plug-in program for pifoc control in Xvin. 
    \author JF
*/
#ifndef _PI_E761_FOCUS_C_ 
#define _PI_E761_FOCUS_C_

#include "allegro.h"
#include "winalleg.h"
#include "ctype.h"
#include "xvin.h"

# include "E7XX_GCS_DLL.h"
# include "PI_E761_focus.h"

char PI_E761_axes[10];
int PI_E761_ID = -1;


#define BUILDING_PLUGINS_DLL


int _has_focus_memory(void)
{
 return 1;
}

int _set_light(float z)  
{ return D_O_K;}

float _get_light()  
{ return 0.5;}

int _set_temperature(float z)  
{ return D_O_K;}

float _get_temperature()  
{return 0.5;}




float _read_Z_value_accurate_in_micron(void)
{
  double pos[2];

  if (PI_E761_ID<0) PI_E761_ID = E7XX_ConnectPciBoard (1); 
  if (PI_E761_ID<0)     return -1;
  E7XX_qPOS(PI_E761_ID, "3", pos);
  PI_E761_obj_position = (float) pos[0];
  return PI_E761_obj_position;
}

int _read_Z_value(void)
{
  return (int)(_read_Z_value_accurate_in_micron());//ask meaninig 10*	
}



int _read_Z_value_PI_E761(void)
{
  int z;
  if(updating_menu_state != 0)	return D_O_K;
  z = _read_Z_value_accurate_in_micron();
  win_printf("Z obj = %f",z);
  return z;
}

float _read_Z_value_OK(int *error) /* in microns */
{
  return _read_Z_value_accurate_in_micron();
}
float _read_last_Z_value(void) /* in microns */
{
  return PI_E761_obj_position;//why not asking the driver?	
}



int _set_Z_step(float z)  /* in micron */
{
	PI_E761_Z_step = z;
	return 0;
}




int _set_Z_value(float z)  /* in microns */
{
  double pos[2];
  if (z > 100) z = 100;
  if (z < 0) z = 0;
  if (PI_E761_ID<0) PI_E761_ID = E7XX_ConnectPciBoard (1); 
  if (PI_E761_ID<0)     return -1;
  pos[0] = (double)z;
  E7XX_MOV(PI_E761_ID, "3", pos);
  PI_E761_obj_position = (float) pos[0];
  return 0;
}


int _set_Z_value_PI_E761(void)
{
  float goal = PI_E761_obj_position;

  if(updating_menu_state != 0)	return D_O_K;
  win_scanf("Where do you want to go ?%f",&goal);
  _set_Z_value(goal);
  return D_O_K;
}


int _set_Z_obj_accurate(float zstart)
{
  return _set_Z_value(zstart);
}
int _set_Z_value_OK(float z) 
{
  return _set_Z_value(z);
}

int _inc_Z_value(int step)  /* in micron */
{
  _set_Z_value(PI_E761_obj_position + PI_E761_Z_step*step);
  return 0;
}

int _small_inc_Z_value(int up)  
{
  int nstep;
  
  _set_Z_step(0.1);
  nstep = (up>=0)? 1:-1;
  _set_Z_value(PI_E761_obj_position + PI_E761_Z_step*nstep);
  
  return 0;
}

int _big_inc_Z_value(int up)  
{
  int nstep;
  
  _set_Z_step(0.1);
  nstep = (up>=0)? 10:-10;
  _set_Z_value(PI_E761_obj_position + PI_E761_Z_step*nstep);
  
  return 0;
}



int set_XY_value(float x, float y)  /* in microns */
{
  double pos[3];
  if (x > 100) x = 100;
  if (x < 0) x = 0;
  if (y > 100) y = 100;
  if (y < 0) y = 0;
  if (PI_E761_ID<0) PI_E761_ID = E7XX_ConnectPciBoard (1); 
  if (PI_E761_ID<0)     return -1;
  pos[0] = (double)x;
  pos[1] = (double)y;
  E7XX_MOV(PI_E761_ID, "12", pos);
  PI_E761_X_position = (float) pos[0];
  PI_E761_Y_position = (float) pos[1];
  return 0;
}

int read_XY_value(void)
{
  double pos[3];

  if (PI_E761_ID<0) PI_E761_ID = E7XX_ConnectPciBoard (1); 
  if (PI_E761_ID<0)     return -1;
  E7XX_qPOS(PI_E761_ID, "12", pos);
  PI_E761_X_position = (float) pos[0];
  PI_E761_Y_position = (float) pos[1];
  return 0;
}



int read_XY_value_PI_E761(void)
{
  if(updating_menu_state != 0)	return D_O_K;
  read_XY_value();
  win_printf("X obj = %f\nY obj = %f",PI_E761_X_position,PI_E761_Y_position);
  return 0;
}




int set_XY_value_PI_E761(void)
{
  float x, y;
  int i;

  if(updating_menu_state != 0)	return D_O_K;
  read_XY_value();
  x = PI_E761_X_position;
  y = PI_E761_Y_position;
  i = win_scanf("New X obj %12f\nNew Y obj = %f",&x,&y);
  if (i == CANCEL) return D_O_K;
  set_XY_value(x, y);  /* in microns */
  return D_O_K;
}


int inc_X_value_PI_E761(void)
{
  float x, y;
  int i, is = 10;

  if(updating_menu_state != 0)	
    {
      add_keyboard_short_cut('x',KEY_X, 0, inc_X_value_PI_E761);
      return D_O_K;
    }
  if (key[KEY_LSHIFT]) is = 1;
  read_XY_value();
  x = PI_E761_X_position;
  y = PI_E761_Y_position;
  i = (int)(10*x + 0.05);
  i += is;
  if (i < 0) i = 0; 
  if (i > 1000) i = 1000;
  x = ((float)i)/10;
  set_XY_value(x, y);  /* in microns */
  return D_O_K;
}


int dec_X_value_PI_E761(void)
{
  float x, y;
  int i, is = 10;

  if(updating_menu_state != 0)	
    {
      add_keyboard_short_cut('w',KEY_W, 0, dec_X_value_PI_E761);
      return D_O_K;
    }
  if (key[KEY_LSHIFT]) is = 1;
  read_XY_value();
  x = PI_E761_X_position;
  y = PI_E761_Y_position;
  i = (int)(10*x + 0.05);
  i -= is;
  if (i < 0) i = 0; 
  if (i > 1000) i = 1000;
  x = ((float)i)/10;
  set_XY_value(x, y);  /* in microns */
  return D_O_K;
}



int inc_Y_value_PI_E761(void)
{
  float x, y;
  int i, is = 10;

  if(updating_menu_state != 0)	
    {
      add_keyboard_short_cut('y',KEY_Y, 0, inc_Y_value_PI_E761);
      return D_O_K;
    }
  if (key[KEY_LSHIFT]) is = 1;
  read_XY_value();
  x = PI_E761_X_position;
  y = PI_E761_Y_position;
  i = (int)(10*y + 0.05);
  i += is;
  if (i < 0) i = 0; 
  if (i > 1000) i = 1000;
  y = ((float)i)/10;
  set_XY_value(x, y);  /* in microns */
  return D_O_K;
}



int dec_Y_value_PI_E761(void)
{
  float x, y;
  int i, is = 10;

  if(updating_menu_state != 0)	
    {
      add_keyboard_short_cut('h',KEY_H, 0, dec_Y_value_PI_E761);
      return D_O_K;
    }
  if (key[KEY_LSHIFT]) is = 1;
  read_XY_value();
  x = PI_E761_X_position;
  y = PI_E761_Y_position;
  i = (int)(10*y + 0.05);
  i -= is;
  if (i < 0) i = 0; 
  if (i > 1000) i = 1000;
  y = ((float)i)/10;
  set_XY_value(x, y);  /* in microns */
  return D_O_K;
}




MENU *PI_E761_focus_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"Read Objective position", _read_Z_value_PI_E761,NULL,0,NULL);
	add_item_to_menu(mn,"Set Objective position",_set_Z_value_PI_E761,NULL,0,NULL);
	add_item_to_menu(mn,"Read XY position",read_XY_value_PI_E761,NULL,0,NULL);
	add_item_to_menu(mn,"Set XY position",set_XY_value_PI_E761,NULL,0,NULL);
	add_item_to_menu(mn,"Inc X position",inc_X_value_PI_E761,NULL,0,NULL);
	add_item_to_menu(mn,"Dec X position",dec_X_value_PI_E761,NULL,0,NULL);
	add_item_to_menu(mn,"Inc Y position",inc_Y_value_PI_E761,NULL,0,NULL);
	add_item_to_menu(mn,"Dec Y position",dec_Y_value_PI_E761,NULL,0,NULL);

	return mn;
}

int  _init_focus_OK(void)
{
  //win_printf("init PI761");
  if (PI_E761_ID >= 0) return 0;
  PI_E761_ID = E7XX_ConnectPciBoard (1); 
  if (PI_E761_ID<0)     return 1;
    
  if (!E7XX_qSAI(PI_E761_ID, PI_E761_axes, 9))      return 1;
  add_plot_treat_menu_item ( "PI_E761 focus", NULL, PI_E761_focus_plot_menu(), 0, NULL);
  add_image_treat_menu_item ( "PI_E761 focus", NULL, PI_E761_focus_plot_menu(), 0, NULL);    
  return 0;   
}



int PI_E761_focus_main (int argc, char **argv)
{
 
  //  struct ProductInformation pi;

  _init_focus_OK();
  add_plot_treat_menu_item ( "PI_E761 focus", NULL, PI_E761_focus_plot_menu(), 0, NULL);
  add_image_treat_menu_item ( "PI_E761 focus", NULL, PI_E761_focus_plot_menu(), 0, NULL);    
  return 0;
}



int	PI_E761focus_unload(int argc, char **argv)
{
	remove_item_to_menu(plot_treat_menu, "PI_E761_focus", NULL, NULL);
	return D_O_K;
}

#endif
