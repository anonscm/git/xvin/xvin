/*******************************************************************************
                         LPS ENS
--------------------------------------------------------------------------------

  Program      : CVB_Source for bead tracking 
  Author       : JFA,VC
  Date         : 27.10.2006

  Purpose      : CVB Image Manager Functions for XVin
  CVB          : 8.1.0
  Hints        : 
      
  Updates      : 
********************************************************************************/
#ifndef _CVB_SOURCE_C_
#define _CVB_SOURCE_C_
//#define XV_WIN32
#define CVB_TRUE 1



#include "allegro.h"
#include "winalleg.h"
//#include <windowsx.h>
#define GetPixel GetPixelW
#define SetPixel SetPixelW
#include "wingdi.h"
#undef GetPixel
#undef SetPixel

#define INCLUDE_PIXEL_GET_SET
#include "icvcimg.h"
#undef INCLUDE_PIXEL_GET_SET

#include "xvin.h"

/* If you include other plug-ins header do it here*/ 


/* But not below this define */

# define BUILDING_PLUGINS_DLL
#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "trackBead.h"

#include "XVCVB.h"

imreg   	*imr_CVB = NULL;
pltreg  	*pr_CVB = NULL;
//O_i     	*oi_TRACK = NULL;
DIALOG  	*d_CVB = NULL;
IMG     	Img_CVB;
IMG     	Old_Img_CVB;
IMG     	New_Img_CVB;
DISPLAY 	CVB_Display;
long  	CVB_number_images_buffer = 64;
int 		num_image_CVB = -1;
BOOL 		allegro_display_on = TRUE;
long		lNumBuff = 128;
double	absolute_image_number = 0;
BOOL 		overlay = TRUE;
//int 		do_refresh_overlay = 1;

#define CVB_FALSE 0


/* float read_Z_value(void) */
/* { */
/*   return 0.0; */
/* } */
/* int set_Z_value(float z) */
/* { */
/*   return 0; */
/* } */

int prepare_image_overlay(O_i* oi)
{
  return 0;
}

int freeze_video(void);

int CVB_before_menu(int msg, DIALOG *d, int c)
{
  /* this routine switches the display flag so that no magic color is drawn 
     when menu are activated */
   
  if (msg == MSG_GOTMOUSE)
    {
      do_refresh_overlay = 0;
      //display_title_message("menu got mouse");
    }
    
  return 0;
}
int CVB_after_menu(int msg, DIALOG *d, int c)
{

  return 0;
}

int CVB_oi_got_mouse(struct  one_image *oi, int xm_s, int ym_s, int mode)
{
  /* this routine switches the display flag so that the magic color is drawn again
     after menu are activated */
  do_refresh_overlay = 1;
  //display_title_message("Image got mouse");
  return 0;
}
//int CVB_oi_idle_action(struct  one_image *oi, DIALOG *d)
//{
// this routine switches supress the image idle action 
//  return 1;
//}

/* //////////////////////////////////////////////////////////////////////////////// */
/* // New file saving functions */

/* //////////////////////////////////////////////////////////////////////////////// */

/* /\** Modified .gr header saver. */

/* IMPORTANT : superseds all previous (bugged) versions of save_image_header. */
/* \param O_i The oi containing the information. */
/* \param char A header text ?? */
/* \param int ?? */
/* \return int Error code. */
/* \author JF Allemand */
/* *\/ */
/* int save_image_header_JF(O_i* oi, char *head, int size) */
/* { */
/*   FILE *fp; */
/*   int nh; */
/*   char filename[512]; */

/*   if (oi == NULL || oi->dir == NULL || oi->filename == NULL)	return 1; */
/*   build_full_file_name(filename,512,oi->dir,oi->filename); */

/*   fp = fopen(filename,"r+b"); */
/*   if (fp == NULL) */
/*     { */
/*       error_in_file("%s\nfile not found!...",filename); */
/*       return 1; */
/*     } */
/*   nh = find_CTRL_Z_offset(oi,fp); */

/*   if (nh != size) */
/*     { */
/*       win_printf("the size of headers disagree!"); */
/*       return 1; */
/*     } */
	  
/*   if (fp == NULL) */
/*     { */
/*       error_in_file("%s\nfile not found!...",filename); */
/*       return 1; */
/*     } */
	
/*   fseek(fp,0,SEEK_SET); */
/*   if (fwrite (head,sizeof(char),size,fp) != size) */
/*     { */
/*       win_printf("Pb while writing header!"); */
/*       return 1; */
/*     } */
/*   fclose(fp); */
/*   return 0; */
/* } */

/* //////////////////////////////////////////////////////////////////////////////// */
/* /\** Modified file saver. */

/* IMPORTANT : superseds all previous (bugged) versions of close_movie_on_disk. */
/* \param O_i The oi containing the information. */
/* \param int The number of frames in the movie. */
/* \return int Error code. */
/* \author JF Allemand */
/* *\/ */
/* int  close_movie_on_disk_JF(O_i *oi, int nf) */
/* { */
/*   char *head, *found, tmp[256]; */
/*   int size = 0, nff = -1, cff = -1, i; */
/*   extern char *read_image_header(O_i* oi, int *size); */
/*   head = read_image_header(oi, &size); */
/*   if (head == NULL)  return 1; */
/*   found = strstr(head,"-nf"); */
/*   if (found != NULL)     */
/*     { */
/*       snprintf(tmp,256,"-nf %010d %010d\r\n",nf, oi->im.c_f); */
/*       for (i = 0; (i < 256) && (tmp[i] != 0); i++) found[i] = tmp[i];  */
/*     } */
/*   else win_printf("did not found -nf symbol"); */
/*   sscanf(found,"-nf %d %d",&nff,&cff); */

/*   if (oi->im.record_duration != 0) */
/*     { */
/*       found = strstr(head,"-duration"); */
/*       if (found != NULL) 	 */
/* 	{ */
/*       	  snprintf(tmp,256,"-duration %016.6f \r\n",oi->im.record_duration); */
/*       	  for (i = 0; (i < 256) && (tmp[i] != 0); i++) found[i] = tmp[i];  */
/* 	} */
/*       else win_printf("did not found -duration symbol"); */
/*     } */
/*   save_image_header_JF(oi, head,  size); */
/*   if (head) {free(head); head = NULL;} */
/*   return 0; */
/* } */


/* int save_tape_mode(void) */
/* { */
/*   FILE *fp_images = NULL; */
/*   int n_images_saved = 1; */
/*   HWND hWnd; */
/*   imreg *imr; */
/*   O_i *oi_buffer_tape_CVB = NULL; */
/*   int num_files = 0; */
/*   char filenamend[16]; */
/*   char common_name[20]="CVB"; */
/*   int previous_grab_stat = 0;   */
/*   char fullfile[512]; */
/*   register int i; */
/*   char path[512], file[256], *pa;  */
/*   int n_images_to_be_saved = 0; */
    
/*   if(updating_menu_state != 0)	return D_O_K; */
	
/*   overlay = FALSE; */
/*   Sleep(500); */
	
	
/*   hWnd = win_get_window(); */
/*   imr = find_imr_in_current_dialog(NULL); */
/*   if (imr == NULL)	return 1; */

/*   movie_buffer_on_disk = 1; */
/*   switch_allegro_font(1); */
/*   pa = (char*)get_config_string("IMAGE-GR-FILE","last_saved",NULL);	 */
/*   if (pa != NULL)		extract_file_path(fullfile, 512, pa); */
/*   else				my_getcwd(fullfile, 512); */
/*   strcat(fullfile,"\\");//Jusqu'ici c'est le path  */
/*   num_files = get_config_int("TAPE_SAVE_MODE","filenumber",-1); */
/*   win_scanf("Characteristic string for the files? %s\n Initial number for file%d?",common_name,&num_files); */
/*   strcat(fullfile,common_name);//on rajoute le debut du nom specifique */
/*   snprintf(filenamend,16,"%04d.gr",num_files);//on rajoute le nombre a incrementer et le format */
/*   strcat(fullfile,filenamend); */
/*   i = file_select_ex("Save real time movie", fullfile, "gr", 512, 0, 0); */
/*   switch_allegro_font(0); */
    
/*   if (i != 0) 	 */
/*     { */
/*       set_config_string("IMAGE-GR-FILE","last_saved",fullfile);			 */
/*       extract_file_name(file, 256, fullfile); */
/*       extract_file_path(path, 512, fullfile); */
/*       strcat(path,"\\"); */
	    		
/*     } */
/*   else  */
/*     {	 */
/*       movie_buffer_on_disk = 1; */
/*       return 0; */
/*     } */
	
/*   set_config_int("TAPE_SAVE_MODE","filenumber",num_files); */
	
	
	
	
/*   if (oi_buffer_tape_CVB == NULL) */
/*     { */
/*       if((oi_buffer_tape_CVB = (O_i *)calloc(1,sizeof(O_i))) == NULL)	 */
/* 	return win_printf("Our are out of memory guy"); */
/*       if (init_one_image(oi_buffer_tape_CVB, IS_CHAR_IMAGE)) 		return win_printf("You have a little problem guy"); */

/*       uns_oi_2_oi(oi_buffer_tape_CVB, oi_TRACK); */
/*       inherit_from_im_to_im(oi_buffer_tape_CVB, oi_TRACK); */
/*     } */
	
/*   while (keypressed()!=TRUE) */
/*     {   if (mouse_b & 1) */
  
/*       {      */
/* 	draw_bubble(screen,0,550,100,"%d",num_files);      */
    		
/* 	fullfile[0] = '\0'; */
/* 	num_files = get_config_int("TAPE_SAVE_MODE","filenumber",-1); */
/* 	if (num_files < 0) num_files = 0; */
/* 	snprintf(file,256,"%s%04d.gr",common_name,num_files++);//on rajoute le nombre a incrementer et le format */
/* 	strcat(fullfile,path); */
/* 	strcat(fullfile,file); */
		
/* 	oi_buffer_tape_CVB->im.movie_on_disk = 1; */
/* 	oi_buffer_tape_CVB->dir = strdup(path); */
/* 	oi_buffer_tape_CVB->filename = strdup(file); */
/* 	oi_buffer_tape_CVB->im.time = 0; */
/* 	oi_buffer_tape_CVB->im.n_f= 0; */
/* 	oi_buffer_tape_CVB->im.data_type = IS_CHAR_IMAGE; */
/* 	oi_buffer_tape_CVB->im.nx = oi_TRACK->im.nx; */
/* 	oi_buffer_tape_CVB->im.ny = oi_TRACK->im.ny; */
 		
 		
/* 	set_image_starting_time(oi_buffer_tape_CVB); */
/* 	set_image_ending_time (oi_buffer_tape_CVB); */
		
/* 	save_one_image(oi_buffer_tape_CVB, fullfile);/\***sauve en tete et les parametres**\/ */
/* 	//win_printf("After image"); */
/* 	set_config_int("TAPE_SAVE_MODE","filenumber",num_files); */
    		
    	
/* 	fp_images = fopen (fullfile, "ab"); */
    	
/* 	if ( fp_images == NULL ) 	 */
/* 	  {         */
/* 	    win_printf("Cannot open the file!"); */
/* 	    movie_buffer_on_disk = 0; */
/* 	    return 1; */
/* 	  } */
        
/* 	//    	previous_image_for_tape_saving = 0; */
        	
/* 	G2GetGrabStatus (Img_CVB, GRAB_INFO_NUMBER_IMAGES_AQCUIRED, &absolute_image_number); */
/* 	//RBBufferSeq ( Img_CVB, lSequenceIndex, &lBufferIndex);  */
/* 	//switch_frame(oi_TRACK,(int) error%lNumBuff);  */
/* 	//draw_bubble(screen,0,550,75,"error  %f  i %d index %d",absolute_image_number ,i,(int)lBufferIndex); */
            
/* 	previous_grab_stat = (int) absolute_image_number; //A VOIR */
/* 	n_images_saved = 0; */
    		
/* 	set_image_starting_time(oi_buffer_tape_CVB); */
		
/* 	overlay = TRUE; */
/* 	Sleep(50); */
/* 	while (!(mouse_b & 2))//stop saving with the mouse right button  */
/* 	  { */
/* 	    G2GetGrabStatus (Img_CVB, GRAB_INFO_NUMBER_IMAGES_AQCUIRED, &absolute_image_number); */
/* 	    n_images_to_be_saved = (int)absolute_image_number - previous_grab_stat - n_images_saved; */
        	
        
/* 	    if (n_images_to_be_saved > 0) */
/* 	      { */
/* 		//Passer avec RBBufferSeq ( IMG Image, long lSequenceIndex, long &lBufferIndex);  */

/* 		//win_printf("stats.CurrentFrameSeqNum %d n_images_to_be_saved %d nframes_in_IFC_buffer %d",stats.CurrentFrameSeqNum,n_images_to_be_saved,nframes_in_IFC_buffer); */
/* 		fwrite (oi_TRACK->im.mem[((int)absolute_image_number - n_images_to_be_saved)%((int)lNumBuff) ],ImageDimension (Img_CVB) * ImageDatatype(Img_CVB, 0)/8,oi_TRACK->im.nx * oi_TRACK->im.ny,fp_images); */
/* 		n_images_saved++; */
/* 		draw_bubble(screen,0,150,100,"Currently saving image %d",n_images_saved+1); */
/* 		if (n_images_to_be_saved > lNumBuff) */
/* 		  {  */
/* 		    freeze_video(); */
/* 		    return win_printf("Could not go fast enough!"); */
/* 		  } */
/* 	      } */
/* 	  } */
   		
/* 	fclose(fp_images);  */
/* 	set_image_ending_time (oi_buffer_tape_CVB); */
/* 	overlay = FALSE; */
/* 	Sleep(50); */
/* 	win_printf("Saved %d",n_images_saved); */
/* 	overlay = TRUE; */
/* 	Sleep(50); */
/* 	close_movie_on_disk_JF(oi_buffer_tape_CVB,n_images_saved); */
/*       } */
/*     } */
/*   movie_buffer_on_disk = 0; */
/*   return 0; */
/* } */

/*************************************************END OF RECORD PART*********************************************************************************/


int set_oi_pointers(O_i *oi, void * p_CVB_data_pointer, int x_inc, int y_inc)
{
	
  register int i = 0;
  int data_len = 1, nf=0;
  void *buf = NULL;
  int nx = -1; 
  int ny = -1;
  int type = -1;
	
  if (oi == NULL) return win_printf_OK("Oi NULL");
  data_len = ImageDimension (Img_CVB) * x_inc; //CHECK valid even for rgb
	
  nx = ImageWidth (Img_CVB);
  ny = ImageHeight (Img_CVB);

  type = oi->im.data_type ; 
	
  if (oi->im.pixel != NULL && oi->im.ny != 0 && oi->im.nx != 0) 		buf = oi->im.mem[0]; //oi->im.pixel[0].ch;
	
  nf = (oi->im.n_f > 0) ? oi->im.n_f : 1;
  oi->im.pixel = (union pix *)realloc(oi->im.pixel, ny*nf * sizeof(union pix));
  if (oi->im.pixel == NULL) return xvin_error(Out_Of_Memory);
  //win_printf("Oi  pixel NULL");
  if (nf > 1) 
    {
      //win_printf_OK("nf %d \n data length %d \n Type %d", oi->im.n_f , data_len , oi->im.data_type);	
      oi->im.pxl = (union pix **)realloc(oi->im.pxl, nf*sizeof(union pix*));
      oi->im.mem = (void **)realloc(oi->im.mem, nf*sizeof(void*));
      if (oi->im.pxl == NULL  || oi->im.mem == NULL) return xvin_error(Out_Of_Memory);
      //	win_printf_OK("Oi CVB not NULL");		
		  
    }
	
  else oi->im.mem = (void **)realloc(oi->im.mem, sizeof(void*));
  //win_printf_OK("Oi CVB mem NULL");
  oi->im.m_f = nf;
  buf = p_CVB_data_pointer; // already allocated by CVB realloc(buf, nf*nx*ny * data_len * sizeof(char));
  if (oi->im.pixel == NULL || buf == NULL)	return xvin_error(Out_Of_Memory);
  oi->im.mem[0] = buf;
  for (i=0 ; i< ny*nf ; i++)
    {
      switch (type)
	{
	case IS_CHAR_IMAGE:
	  oi->im.pixel[i].ch = (unsigned char*)buf;
			
	  break;
	case IS_RGB_PICTURE:
	  oi->im.pixel[i].ch = (unsigned char*)buf;
			
	  break;			
	case IS_RGBA_PICTURE:
	  oi->im.pixel[i].ch = (unsigned char*)buf;
			
	case IS_RGB16_PICTURE:
	  oi->im.pixel[i].in = (short int*)buf;
			
	  break;			
	case IS_RGBA16_PICTURE:
	  oi->im.pixel[i].in = (short int*)buf;
			
	  break;			
	case IS_INT_IMAGE:
	  oi->im.pixel[i].in = (short int *)buf;
			
	  break;
	case IS_UINT_IMAGE:
	  oi->im.pixel[i].ui = (unsigned short int *)buf;
			
	  break;
	case IS_LINT_IMAGE:
	  oi->im.pixel[i].li = (int *)buf;
			
	  break;
	case IS_FLOAT_IMAGE:
	  oi->im.pixel[i].fl = (float *)buf;
			
	  break;
	case IS_COMPLEX_IMAGE:
	  oi->im.pixel[i].fl = (float *)buf;
			
	  break;			
	case IS_DOUBLE_IMAGE:
	  oi->im.pixel[i].db = (double *)buf;
			
	  break;
	case IS_COMPLEX_DOUBLE_IMAGE:
	  oi->im.pixel[i].db = (double *)buf;
			
	  break;			

	};
      if ( (i % ny == 0) && (oi->im.n_f > 0))
	{
	  oi->im.pxl[i/ny] = oi->im.pixel + i;
	  oi->im.mem[i/ny] = (void*)buf;
	}
      buf += nx * data_len;
    }
  oi->im.nxs = oi->im.nys = 0;
		
  if (type == IS_COMPLEX_IMAGE  || type == IS_COMPLEX_DOUBLE_IMAGE)	
    oi->im.mode = RE;

  return D_O_K;
}

	
	
	

int add_CVB_oi_2_imr (imreg *imr,O_i* oi)
{
  long lInfo = -1;
  void* CVB_linear_pointer = NULL;
  long XInc = -1; 
  long YInc = -1;
  long pix;
  int type;
  
  
  //win_printf ("IN");
  if ( imr == NULL ) return win_printf_OK("IMR NULL");//xvin_error(Wrong_Argument);
  //if (CVB_linear_pointer) return xvin_error(Wrong_Argument);
  
  
  
  if (AnalyseXVPAT(Img_CVB, 0, &pix) == XVPAT_LINEAR_WITH_DATATYPE ) 
    {
      //win_printf("PAT perfect with increment %d",(int)pix); 
      if (GetLinearAccess(Img_CVB, 0, &CVB_linear_pointer, &XInc, &YInc)!= CVB_TRUE) return win_printf_OK("Linear acces failed"); 
      //win_printf("ImageBits %d",CVB_linear_pointer);
      //win_printf("XInc %d \n YInc %d...to be exploited",XInc,YInc);
    }
  else return win_printf_OK("NOT LINEAR ACCESS IMAGE");
  
  
  if 	  (ImageDimension (Img_CVB) == 1 && ImageDatatype(Img_CVB, 0) == 8) type = IS_CHAR_IMAGE;
  else if (ImageDimension (Img_CVB) == 3 && ImageDatatype(Img_CVB, 0) == 8) type = IS_RGB_PICTURE;
  else if (ImageDimension (Img_CVB) == 1 && ImageDatatype(Img_CVB, 0) == 16) type = IS_UINT_IMAGE;
  else if (ImageDimension (Img_CVB) == 1 && ImageDatatype(Img_CVB, 0) == 32) type = IS_INT_IMAGE;
  else return win_printf_OK("FORMAT NOT IMPLEMENTED YET");
	
  if (RBNumBuffer ( Img_CVB , RINGBUFFER_NUMBUFFER_CMD_GET, &lInfo, NULL) < 0)return win_printf_OK("Ring Numb pb! %d",(int)lInfo);; 
  //win_printf("before oi_track creation");
  if (oi == NULL)
    {
      if((oi = (O_i *)calloc(1,sizeof(O_i))) == NULL)	return win_printf_OK("Calloc oi pb!");
    }
  //win_printf("Init NOT done \n buffer size %d",(int)lInfo);
  if (init_one_image(oi, type)) 		return win_printf_OK("Init pb!");
  oi->im.nx = ImageWidth (Img_CVB);
  oi->im.ny = ImageHeight (Img_CVB);
  oi->im.nxs = oi->im.nys = 0;
  oi->im.nxe = oi->im.nx;
  oi->im.nye = oi->im.ny;


  set_oi_source(oi , "CVB");
	
  oi->im.n_f = (int)lInfo;
  //win_printf("Init done \n buffer size %d",(int)lInfo);
  oi->im.data_type = type ;
  //win_printf("CVB Type %d \n Type %d \n dimension %d \n type %d",oi->im.data_type,type, ImageDimension (Img_CVB),ImageDatatype(Img_CVB, 0));
  //win_printf("Source %s",get_oi_source(oi_TRACK));
  if (add_image(imr, type, (void*)oi) != 0) 
    {
      //remove_image (imr_TRACK, IS_CHAR_IMAGE, (void *) imr_TRACK->o_i[0]);
      win_printf("Pb in Image added");
    }
  set_oi_pointers(oi,CVB_linear_pointer, XInc , YInc);
  if ((ImageWidth (Img_CVB) <1024) && (ImageHeight (Img_CVB)<1024))
    {
      oi->width  = ((float)ImageWidth (Img_CVB))/512;
      oi->height = ((float)ImageHeight (Img_CVB))/512;
    }
  else
    {
      oi->width  = ((float)ImageWidth (Img_CVB))/1024;
      oi->height = ((float)ImageHeight (Img_CVB))/1024;
    }
  oi->iopt2 &= ~X_NUM;		oi->iopt2 &= ~Y_NUM;		
  set_zmin_zmax_values(oi, 0, 255);
  set_z_black_z_white_values(oi, 0, 255);
	
  //oi_TRACK->need_to_refresh |= ALL_NEED_REFRESH;
  //win_printf("XInc %d \n YInc %d...to be exploited",XInc,YInc);	
  return 0;
}

char	*do_load_grabber_driver(void)
{
  register int i = 0;
  char path[512], file[256];
  static char fullfile[512], *fu = NULL;
  int full=1;

  if (fu == NULL)
    {
      fu = (char*)get_config_string("Grabberdriver","last_loaded",NULL);
      if (fu != NULL)	
	{
	  strcpy(fullfile,fu);
	  fu = fullfile;
	}
      else
	{
	  my_getcwd(fullfile, 512);
	  strcat(fullfile,"\\");
	}
    }
#ifdef XV_WIN32
  if (full) // full_screen
    {
#endif
      switch_allegro_font(1);
      i = file_select_ex("Load driver (*.vin)", fullfile, "vin", 512, 0, 0);
      switch_allegro_font(0);
#ifdef XV_WIN32
    }
  else
    {
      extract_file_path(path, 512, fullfile);
      put_backslash(path);
      if (extract_file_name(file, 256, fullfile) == NULL)
	fullfile[0] = file[0] = 0;
      else strcpy(fullfile,file);
      fu = DoFileOpen("Load Grabber Driver (*.vin)", path, fullfile, 512, "Driver Files (*.vin)\0*.vin\0All Files (*.*)\0*.*\0\0", "vin");
      i = (fu == NULL) ? 0 : 1;
      win_printf("loading %d\n%s",backslash_to_slash(fullfile));
      slash_to_backslash(fullfile);
    }
#endif
  if (i != 0) 	
    {
      fu = fullfile;
      set_config_string("Grabberdriver","last_loaded",fu);
      return fullfile;;
    }
  return NULL;
}



int initialize_CVB(void)
{   
  long value = -1;
  char message[256];

  if(updating_menu_state != 0)	return D_O_K;
 
  value = GetLicenseInfo ();
    
  if (value == LI_NOLICENSE ) 
    {
      //MessageBox(GetActiveWindow(),"CVB not licensed!", "XVin Warning", MB_ICONINFORMATION);
    }    
  if (value == LI_COMMONVISIONBLOX) 
    {
      //MessageBox(GetActiveWindow(),"CVB licensed!", "XVin Warning", MB_ICONINFORMATION);
    }    
  value = GetSerialNumber();
  {
    snprintf(message,256,"CVB Serial Number %ld",value);
    //MessageBox(GetActiveWindow(),message, "XVin Warning", MB_ICONINFORMATION);
  }    
  GetCVBVersion (message, 256); 
  //MessageBox(GetActiveWindow(),message, "CVB version", MB_ICONINFORMATION);
    
  if (LoadImageFile (do_load_grabber_driver(), &Img_CVB) == FALSE) win_printf("This board is not in the computer"); 
  if (RBNumBuffer ( Img_CVB, RINGBUFFER_NUMBUFFER_CMD_SET , &lNumBuff,&Img_CVB) < 0) return win_printf_OK("Number images in buffer failed"); 
 
  snprintf(message,256,"Number of boards %ld",value);
  //MessageBox(GetActiveWindow(),message, "Board", MB_ICONINFORMATION); 
  if (CanCameraSelect (Img_CVB) == CVB_TRUE) ;//MessageBox(GetActiveWindow(),"Camera Select OK", "Cam Select", MB_ICONINFORMATION);  


  return D_O_K;   
} 

long long MyAcquisitionPeriod(void)
{
  //   register int i;
  long long t0;
  double dTotalImageNumber = 0;
  long lBufferIndex = 0;
  double dNumberOfPendingImages = -1;

  G2GetGrabStatus (Img_CVB, GRAB_INFO_NUMBER_IMAGES_PENDIG, &dNumberOfPendingImages);
  //if (RBBufferSeq ( Img_CVB, 0 , &lBufferIndex) < 0) return win_printf_OK("Error"); 
  //draw_bubble(screen,0,550,75,"index %f",lBufferIndex);
  dTotalImageNumber = dNumberOfPendingImages + 1;
	
		
  // wait the next frame acquired
  while(dTotalImageNumber > dNumberOfPendingImages)
    {G2GetGrabStatus (Img_CVB, GRAB_INFO_NUMBER_IMAGES_PENDIG, &dNumberOfPendingImages);
    //draw_bubble(screen,0,550,125,"index %f",dNumberOfPendingImages);
    }
  t0 = my_ulclock();
    
  while(dTotalImageNumber +10 > dNumberOfPendingImages)
    {G2GetGrabStatus (Img_CVB, GRAB_INFO_NUMBER_IMAGES_PENDIG, &dNumberOfPendingImages);
    //draw_bubble(screen,0,550,125,"index %f",dNumberOfPendingImages);
      
    }
  //draw_bubble(screen,0,550,75,"index %f",lBufferIndex);
  t0 = my_ulclock() - t0;
  return t0/10; 
}
///////////////////BOF la suite////////////////////
int kill_source(void)
{
  imreg *imr;
  if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
    {
      active_menu->flags |= D_DISABLED;
      return D_O_K;	
    }
  //else if (go_track == TRACK_ON) active_menu->flags |=  D_DISABLED;
  else active_menu->flags &= ~D_DISABLED;
  if(updating_menu_state != 0)	return D_O_K;
  go_track = TRACK_STOP;
  return D_O_K;
}
//////////////////////////////////////////////////

int freeze_video(void)
{
  imreg *imr;
  if(updating_menu_state != 0)	return D_O_K;
  allegro_display_on = FALSE;
  Sleep(100);
  if (G2Freeze (Img_CVB, CVB_TRUE) <0) return win_printf_OK("Freeze failed");
  imr = find_imr_in_current_dialog(NULL);  
  imr->one_i->need_to_refresh &= BITMAP_NEED_REFRESH;  
  find_zmin_zmax(imr->one_i);
  refresh_image(imr, UNCHANGED); 
  return 0;
}

int freeze_source(void)
{
  //imreg *imr;
	
  if(updating_menu_state != 0)	return D_O_K;
  freeze_video();
  return 0;

}




DWORD WINAPI DisplayThreadProc(LPVOID lpParam) 
{
	
  int i = 0;
  BITMAP * imb = NULL;
  //    	long lSequenceIndex = 0;
  long lBufferIndex;
  double test = 0;
	
#ifdef MODIF        
  int seq_num = 0;
  DWORD acquiredDy;
  double t = 0;	
# endif
  //win_printf("BeforeG2");
  G2Grab (Img_CVB); 
  //	before_menu_proc = CVB_before_menu;
  //        after_menu_proc = CVB_after_menu;
  //    	oi_TRACK->oi_got_mouse = CVB_oi_got_mouse;
  //    	oi_TRACK->oi_idle_action = CVB_oi_idle_action;
 
   	
  G2GetGrabStatus (Img_CVB, GRAB_INFO_NUMBER_IMAGES_AQCUIRED, &absolute_image_number);
   

  while(allegro_display_on == TRUE )
    {
      G2Wait(Img_CVB);
      if (overlay == TRUE && do_refresh_overlay == 1)
	{
			
	  set_zmin_zmax_values(oi_TRACK, 0, 255);
	  set_z_black_z_white_values(oi_TRACK, 0, 255);
	  oi_TRACK->need_to_refresh |= BITMAP_NEED_REFRESH;            
	  display_image_stuff_16M(imr_TRACK,  d_TRACK);
	  imb = (BITMAP*)oi_TRACK->bmp.stuff;
	  acquire_bitmap(screen);
	  blit(imb,screen,0,0,imr_TRACK->x_off + d_TRACK->x, imr_TRACK->y_off - imb->h + d_TRACK->y, imb->w, imb->h); 
	  release_bitmap(screen);
	  G2GetGrabStatus (Img_CVB, GRAB_INFO_NUMBER_IMAGES_AQCUIRED, &absolute_image_number);
	  G2GetGrabStatus (Img_CVB, GRAB_INFO_NUMBER_IMAGES_PENDIG, &test);
	  //RBBufferSeq ( Img_CVB, lSequenceIndex, &lBufferIndex); 
	  switch_frame(oi_TRACK,(int) absolute_image_number%lNumBuff); 
	  draw_bubble(screen,0,550,75,"error  %f  i %d index %d test %f",absolute_image_number ,i,(int)lBufferIndex,test);
	}
    }
	  
  return D_O_K;

}

VOID CALLBACK TimerAPCProc(LPVOID lpArgToCompletionRoutine,
			   DWORD dwTimerLowValue,  DWORD dwTimerHighValue)
{
  long long t;
  static unsigned long dt, dt_1 = 0, dt2;
  int im_n,  i = 0;
  static int j=0;
  tid *ltid;
  imreg *imr;
  DIALOG *d;
  O_i *oi;
  Bid *p = NULL;
  int nf;
  double dTreatedImageNumber;
  double dPendigImageNumber;
  long lBufferIndex = -1;

  ltid = (tid*)lpArgToCompletionRoutine;

  imr = ltid->imr;
  d = ltid->dimr;
  oi = ltid->oi;
  p = ltid->dbid;
  nf = abs(oi->im.n_f);

  if (p == NULL) win_printf("MERDE");


  //draw_bubble(screen,0,550,75,"PENDING  %f  ",dPendigImageNumber);
  
  dt = my_uclock();
  im_n = p->previous_fr_nb + 1; // the next frame
  
  G2GetGrabStatus (Img_CVB, GRAB_INFO_NUMBER_IMAGES_PENDIG , &dPendigImageNumber);
  
  if (dPendigImageNumber == 0)  // we are in advance
    {
      
      G2Wait(Img_CVB);
      p->previous_in_time_fr = t = my_ulclock();
      dt2 = my_uclock() - dt;
      p->previous_in_time_fr_nb = im_n;
      //draw_bubble(screen,0,250,95,"j  %d  ",j);
    }
  else   
    {
      G2Wait(Img_CVB);
      //display_title_message("Late %f",dPendigImageNumber); 
      dt2 = 0;
    }
  
  p->timer_dt = dt2;
  p->previous_fr_nb = im_n;
  //draw_bubble(screen,0,250,75,"im_n  %d  ",im_n);

  if (p->timer_do == NULL) 
    {
      win_printf("timer_do NULL");
      return;
    }
  //  G2GetGrabStatus (Img_CVB, GRAB_INFO_NUMBER_IMAGES_PENDIG , &dPendigImageNumber);
  for (i = 0 ; i < dPendigImageNumber +1/* im_n  < dTreatedImageNumber*/ ; im_n++, i++)
    {
      if (i) G2Wait(Img_CVB);
      RBBufferSeq ( Img_CVB, 0 , &lBufferIndex); 

      switch_frame(oi_TRACK,lBufferIndex );
      t = my_ulclock();
      dt = my_uclock();
      p->previous_fr_nb = im_n;
      //draw_bubble(screen,0,550,75,"YOP %d j %d",im_n,j++);      
      for (p->timer_do(imr, oi, im_n, d, t ,dt ,p->param, dPendigImageNumber - i) ; 
	   p->timer_do != NULL && p->next != NULL; p = p->next)
	{
	  //draw_bubble(screen,0,550,95,"YOP");
	  p->timer_do(imr, oi,im_n, d, t, dt, p->param, dPendigImageNumber - i);
        }
    }
  //display_title_message("nb process %d",i); 
  dt_1 = my_uclock() - dt;
  return;
}
# define AcquisitionPeriodInHundredsOfNanoSeconds   \
(int)((((double)AcquisitionPeriod())*10000000)/get_my_ulclocks_per_sec())



DWORD WINAPI TrackingThreadProc(LPVOID lpParam) 
{
  HANDLE hTimer = NULL;
  LARGE_INTEGER liDueTime;
  imreg *imr;
  int j, im_n, nf; //,n;
  long long t;
  long long AcqPer;
  int AcqPerNano;
  tid *ltid;
  DIALOG *d;
  O_i *oi;
  Bid *p;
  double dTreatedImageNumber = 0;
  long lBufferIndex = -1;
  double dPendigImageNumber;
  BITMAP* imb = NULL;
    
  if (imr_TRACK == NULL || oi_TRACK == NULL)	return 1;
  liDueTime.QuadPart=-300000; // 30 ms
  ltid = (tid*)lpParam;

  imr = ltid->imr;
  d = ltid->dimr;
  oi = ltid->oi;
  p = ltid->dbid;
  //nf = abs(oi->im.n_f);		
    
  if (G2Grab (Img_CVB) < 0) return win_printf_OK("Failure"); 
    
  AcqPer = MyAcquisitionPeriod();
    
  AcqPerNano = (int)((((double)AcqPer)*10000000)/get_my_ulclocks_per_sec());
  //draw_bubble(screen,0,550,125,"AcqPerNano  %d  ",AcqPerNano);
  hTimer = CreateWaitableTimer(NULL, TRUE, "WaitableTimer");
  if (!hTimer)   win_printf_OK("CreateWaitableTimer failed (%d)\n", GetLastError());

  /*     set_zmin_zmax_values(oi_TRACK, 0, 255); */
  /*     set_z_black_z_white_values(oi_TRACK, 0, 255); */
  /*     oi_TRACK->need_to_refresh |= BITMAP_NEED_REFRESH; */
  /*     display_image_stuff_16M(imr_TRACK,  d_TRACK); */
  /*     imb = (BITMAP*)oi_TRACK->bmp.stuff; */
  /*     acquire_bitmap(screen); */
  /*     blit(imb,screen,0,0,imr_TRACK->x_off + d_TRACK->x, imr_TRACK->y_off - imb->h + d_TRACK->y, imb->w, imb->h); */
  /*     release_bitmap(screen); */
  /*     refresh_image(imr_TRACK, UNCHANGED);  */
  /*     broadcast_dialog_message(MSG_DRAW,0);   */

  if (G2Freeze (Img_CVB, CVB_FALSE) < 0) return win_printf_OK("Failure"); 


  if (G2Grab (Img_CVB) < 0) return win_printf_OK("Failure"); 
  G2GetGrabStatus (Img_CVB, GRAB_INFO_NUMBER_IMAGES_AQCUIRED , &dPendigImageNumber);
  //draw_bubble(screen,0,250,175,"PENDING  %f  ",dPendigImageNumber);
  //bid.first_im = bid.previous_fr_nb = (int) dPendigImageNumber;
  p->first_im = p->previous_fr_nb = (int) dPendigImageNumber;
  im_n = p->previous_fr_nb + 1;
  //draw_bubble(screen,0,350,105,"im_n %d PENDING  %f  ",im_n,dPendigImageNumber);
  while( im_n > (int) dPendigImageNumber)
    {
      G2GetGrabStatus (Img_CVB, GRAB_INFO_NUMBER_IMAGES_PENDIG , &dPendigImageNumber);
	      
      //draw_bubble(screen,0,150,115,"im_n top %d PENDING  %f  ",im_n,dPendigImageNumber);
    }
  p->previous_in_time_fr = my_ulclock();
  p->previous_in_time_fr_nb = im_n;
    
  while (1)
    {
      im_n = p->previous_fr_nb + 1;
		
      G2GetGrabStatus (Img_CVB, GRAB_INFO_NUMBER_IMAGES_PENDIG , &dPendigImageNumber);
      //draw_bubble(screen,0,150,85,"PENDING  %f  im_n %d",dPendigImageNumber,im_n);
 
      if (dPendigImageNumber > 0)  // Images are stored in the buffer
	{
	  t = 500;
	  j = 0;
	}
      else
	{
	  j = im_n - p->previous_in_time_fr_nb;
	  t = j*AcqPer;
	  t -= (my_ulclock() - p->previous_in_time_fr);
	  t = (long long)((((double)t)*10000000)/get_my_ulclocks_per_sec());
	  t -= 10000; // we aim 1000 micro second before image flip
	  t = (t < 0) ? 1000 : t;
	  t = (t > 150000) ? 150000 : t;
    	}
	
      liDueTime.QuadPart= -t; // 30 ms
      //liDueTime.QuadPart= -30000; // 5 ms
      // Set a timer to wait for 10 seconds.
      if (!SetWaitableTimer(hTimer, &liDueTime, 0, TimerAPCProc, lpParam, 0))
	win_printf_OK("SetWaitableTimer failed (%d)\n", GetLastError());

      // Wait for the timer.
      SleepEx (INFINITE, TRUE);
    }
      
  win_printf_OK("Timer was signaled.\n");
  return 0;
}


int create_display_thread(void)
{
  HANDLE hThread = NULL;
  DWORD dwThreadId;

  hThread = CreateThread( 
			 NULL,             // default security attributes
			 0,                // use default stack size  
			 DisplayThreadProc,// thread function 
			 NULL,       	// argument to thread function 
			 0,                // use default creation flags 
			 &dwThreadId);     // returns the thread identifier 

  if (hThread == NULL) 	win_printf_OK("No thread created");
  SetThreadPriority(hThread,  THREAD_PRIORITY_TIME_CRITICAL);
  return 0;
}

int CVB_live(void)
{	int is_live = 0;
 if(updating_menu_state != 0)	return D_O_K;

 is_live  = 1;
 allegro_display_on = TRUE;
 //create_display_thread();
 return D_O_K;//live_video(-1);	
}

int live_source(void)
{
  if(updating_menu_state != 0)	return D_O_K;

  //is_live  = 1;
  return CVB_live();	
}


int start_data_movie(imreg *imr)
{
  //  HWND hWnd;
  //  int n;

  //  hWnd = win_get_window(); // we grab WINDOWS context
  
  return 0;
}


MENU *CVB_image_menu(void)
{
  static MENU mn[32];
    
  if (mn[0].text != NULL)	return mn;
  // lauch bead tracking in 2D or 3D
  add_item_to_menu(mn,"Init CVB", initialize_CVB ,NULL,0,NULL);
  add_item_to_menu(mn,"Live CVB", CVB_live ,NULL,0,NULL);
  add_item_to_menu(mn,"Freeze CVB", freeze_video ,NULL,0,NULL);
  //  add_item_to_menu(mn,"Tape CVB", save_tape_mode ,NULL,0,NULL);
    
  return mn;
    
}


int init_image_source(imreg *imr, int mode)
{
  int ret;


  if (imr == NULL)    imr = create_and_register_new_image_project( 0,   32,  900,  668);
  if (imr == NULL) return win_printf_OK("Patate t'es mal barre!");
  add_image_treat_menu_item ( "CVB", NULL, CVB_image_menu(), 0, NULL);
  //win_printf("Number of images %d",imr->n_oi);
  initialize_CVB(); // Initializes CVB 
  //win_printf("Number of images bis %d",imr->n_oi);	
  add_CVB_oi_2_imr (imr,NULL);
  //win_printf("Number of images %d",imr->n_oi);
  if (mode == 1)      remove_from_image (imr, imr->o_i[0]->im.data_type, (void *)imr->o_i[0]);
  imr_TRACK = NULL;
  
	
  return 0;
}
int	XVCVB_main(void)
{
  imreg *imr = NULL;
  //pltreg *pr = NULL;
  //O_p *op;
  HWND hWnd;
  //double pixel = -1;
    
    
  hWnd = win_get_window(); // we grant WINDOWS context
    
  if (CanCVBDirectDraw (hWnd) != DDP_NOPROBLEM ) win_printf("Direct Draw OK"); 
  // Image region initialization
  initialize_CVB(); // Initializes CVB
  imr = create_and_register_new_image_project( 0,   32,  900,  668);
  if (imr == NULL) win_printf("Patate t'es mal barre!");
  //Sleep(500);
  //win_printf("Before CVB"); 
  imr_CVB = imr;
  d_CVB = find_dialog_associated_to_imr(imr_CVB, NULL);
  if (imr_CVB == NULL) win_printf("NO GOOD");
  add_CVB_oi_2_imr (imr_CVB,NULL);
  //broadcast_dialog_message(MSG_DRAW,0);
  refresh_image(imr, UNCHANGED);
  add_image_treat_menu_item ( "CVB", NULL, CVB_image_menu(), 0, NULL); // CVB user menus
       
  return D_O_K;
}

/** Unload function for the  CVB.c menu.*/
int	CVB_unload(void)
{
  remove_item_to_menu(image_treat_menu, "CVB", NULL, NULL);
  return D_O_K;
}


  
#endif

