
#ifndef _HAT_C_
#define _HAT_C_

# include "allegro.h"
#ifdef XV_WIN32
    # include "winalleg.h"
#endif
# include "float.h"
# include "xvin.h"





/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "../cfg_file/Pico_cfg.h"
#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "../fft2d/fftbtl32n.h"
# include "fillibbt.h"
# include "focus.h"
# include "magnetscontrol.h"
# include "trackBead.h"
# include "brown_util.h"
# include "track_util.h"
# include "hat.h"
# include "record.h"
# include "scan_zmag.h"
# include "action.h"
int trajectories_op_idle_action(O_p *op, DIALOG *d);


g_record *hat_r = NULL;
# define HAT_RUNNING  1
# define HAT_STOPPED  2
# define HAT_SUSPENDED  4
int hat_state = 0;

int last_bead = -1;
//static float z_cor = 0.878;
static float min_scale_rot = 1;
static float min_scale_z = 0.5;

int hat_fixed_bead_nb = 0;
int do_hat_substract_fixed_bead = 0;
hat_param *sp = NULL; 
char hat_state_disp[4096];

int hat_op_post_display(struct one_plot *op, DIALOG *d)
{
  register int i, j;
  BITMAP *imb = NULL;
  d_s *ds = NULL;
  //int na = 0, nb = 0, xc, yc;
  int  nb, xc, yc;
  //float f;

  if (d == NULL || d->dp == NULL || d->proc == NULL)    return 1;
  if (imr_and_pr) return 0;
  ds = op->dat[0];
  if (ds == NULL || ds->source == NULL)       return 0;

  //if (sscanf(ds->source,"Bead tracking at %f Hz Acquistion %d for bead %d"
  //     ,&f,&na,&nb) != 3) return 0;

  nb = op->user_ispare[ISPARE_BEAD_NB];
  if (nb >= 0 && nb  < track_info->n_b)
    {
      imb = (BITMAP*)oi_TRACK->bmp.stuff;
      if (oi_TRACK->need_to_refresh & BITMAP_NEED_REFRESH)
	{
	  display_image_stuff_16M(imr_TRACK,d_TRACK);
	  oi_TRACK->need_to_refresh |= INTERNAL_BITMAP_NEED_REFRESH;
	  show_bead_cross(imb, imr_TRACK, d_TRACK);
	}
      for(i = nb - 1, j = 0; (i < nb + 2) && (j < track_info->n_b) && (j < 3); i++, j++) 
	{
	  xc = track_info->bd[(i+track_info->n_b)%track_info->n_b]->x0 - 100;
	  yc = oi_TRACK->im.ny - track_info->bd[(i+track_info->n_b)%track_info->n_b]->y0 - 100;
	  acquire_bitmap(plt_buffer);
	  blit(imb,plt_buffer,xc,yc,SCREEN_W - 300, 60 + 256*j, 200, 200); 
	  release_bitmap(plt_buffer);
	}
    }
  return 0;
}


int hat_op_idle_action_old(O_p *op, DIALOG *d)
{
  if (d == NULL || d->dp == NULL || d->proc == NULL)    return 1;
  if (op->need_to_refresh)      d->proc(MSG_DRAW,d,0);	
  return 0;
}


int hat_op_idle_action(O_p *op, DIALOG *d)
{
  register int i, j;
  BITMAP *imb = NULL;
  d_s *ds = NULL;
  //int na = 0, nb = 0, xc, yc;
  int  nb, xc, yc;
  //float f;
  //char buf[512];

  if (d == NULL || d->dp == NULL || d->proc == NULL)    return 1;
  if (op->need_to_refresh)      d->proc(MSG_DRAW,d,0);	
  if (imr_and_pr) return 0;
  ds = op->dat[0];
  if (ds == NULL || ds->source == NULL)       return 0;

  //if (sscanf(ds->source,"Bead tracking at %f Hz Acquistion %d for bead %d"
  //	     ,&f,&na,&nb) != 3) return 0;

  nb = op->user_ispare[ISPARE_BEAD_NB];

  if (nb >= 0 && nb  < track_info->n_b)
    {
      imb = (BITMAP*)oi_TRACK->bmp.stuff;
      if (oi_TRACK->need_to_refresh & BITMAP_NEED_REFRESH)
	{
	  display_image_stuff_16M(imr_TRACK,d_TRACK);
	  oi_TRACK->need_to_refresh |= INTERNAL_BITMAP_NEED_REFRESH;
	  show_bead_cross(imb, imr_TRACK, d_TRACK);
	}

      for(i = nb - 1, j = 0; (i < nb + 2) && (j < track_info->n_b) && (j < 3); i++, j++) 
	{
	  xc = track_info->bd[(i+track_info->n_b)%track_info->n_b]->x0 - 100;
	  yc = oi_TRACK->im.ny - track_info->bd[(i+track_info->n_b)%track_info->n_b]->y0 - 100;

	  for(;screen_acquired;); // we wait for screen_acquired back to 0;     
	  screen_acquired = 1;	  
	  acquire_bitmap(screen);
	  blit(imb,screen,xc,yc,SCREEN_W - 300, 60 + 256*j, 200, 200); 
	  release_bitmap(screen);
	  screen_acquired = 0;	  	  
	  oi_TRACK->need_to_refresh &= ~INTERNAL_BITMAP_NEED_REFRESH;
	}
    }
  return 0;
}

# ifdef OLD

int hat_job_xyz(int im, struct future_job *job)
{
  register int j, k;
  int ci;
  b_track *bt = NULL;
  d_s *dsx = NULL, *dsy = NULL, *dsz = NULL;
  float ax, dx, ay, dy, y_over_x, y_over_z;
  un_s *un = NULL;

  if (job == NULL || job->oi == NULL || job->in_progress == 0 || job->op->n_dat < 3) return 0;
  if (im < job->imi)  return 0;

  ci = track_info->c_i;
  dsx = job->op->dat[0];
  dsy = job->op->dat[1];
  dsz = job->op->dat[2];
  bt = track_info->bd[job->bead_nb];

  un = job->oi->xu[job->oi->c_xu];
  get_afine_param_from_unit(un, &ax, &dx);
  un = job->oi->yu[job->oi->c_yu];
  get_afine_param_from_unit(un, &ay, &dy);
  y_over_x = (dy != 0) ? dx/dy : 1;
  y_over_z = track_info->focus_cor*1e-6;
  y_over_z = (dy != 0) ? y_over_z/dy : 1;


  for (k = 0; (track_info->imi[ci] > job->imi) && (k < TRACKING_BUFFER_SIZE); k++)
    {
      ci--;
      if (ci < 0) ci += TRACKING_BUFFER_SIZE;
    }

  for (k = 0; (track_info->imi[ci] >= (job->imi-32)) && (k < TRACKING_BUFFER_SIZE); k++)
    {
      j = track_info->imi[ci] - job->in_progress;
      if (j < dsx->mx)
	{
	  dsx->yd[j] = y_over_x * bt->x[ci];
	  dsy->yd[j] = bt->y[ci];
	  dsz->yd[j] = y_over_z * bt->z[ci];
	  dsx->xd[j] = dsy->xd[j] = dsz->xd[j] = j;
	}
      ci--;
      if (ci < 0) ci += TRACKING_BUFFER_SIZE;
    }
  j = job->imi - job->in_progress;
  if (j <= dsx->mx)
    dsx->nx = dsx->ny = dsy->nx = dsy->ny = dsz->nx = dsz->ny = j;
  if (j >= dsx->mx)
        dsx->nx = dsx->ny = dsy->nx = dsy->ny = dsz->nx = dsz->ny = dsx->mx;
  job->op->need_to_refresh = 1;
  if (dsx->nx < dsx->mx) job->imi += 32;
  else job->in_progress = 0; 
  return 0;
}




int hat_job_rot(int im, struct future_job *job)
{
  register int j, k;
  int ci;
  b_track *bt = NULL;
  d_s *ds = NULL;

  if (job == NULL || job->oi == NULL || job->in_progress == 0 || job->op->n_dat < 1) return 0;
  if (im < job->imi)  return 0;

  ci = track_info->c_i;
  for (k = 0; (track_info->imi[ci] > job->imi) && (k < TRACKING_BUFFER_SIZE); k++)
    {
      ci--;
      if (ci < 0) ci += TRACKING_BUFFER_SIZE;
    }
  ds = job->op->dat[0];

  bt = track_info->bd[job->bead_nb];

  for (k = 0; (track_info->imi[ci] >= (job->imi-32)) && (k < TRACKING_BUFFER_SIZE); k++)
    {
      j = track_info->imi[ci] - job->in_progress;
      if (j < ds->mx)
	{
	  ds->yd[j] = track_info->rot_mag[ci];
	  ds->xd[j] = j;
	}
      ci--;
      if (ci < 0) ci += TRACKING_BUFFER_SIZE;
    }
  j = job->imi - job->in_progress;
  if (j <= ds->mx)
    ds->nx = ds->ny = j;
  if (j >= ds->mx)
        ds->nx = ds->ny = ds->mx;
  job->op->need_to_refresh = 1;
  if (ds->nx < ds->mx) job->imi += 32;
  else job->in_progress = 0; 
  return 0;
}


int hat_job_hat(int im, struct future_job *job)
{
  int  ci, ims, ime, i, k;
  b_track *bt = NULL;
  d_s *ds = NULL, *dsx = NULL, *dsy = NULL, *dsz = NULL;
  hat_param *sp = NULL; 
  char buf[1024] = {0};
  un_s *un = NULL;
  float ay, dy, ayl, dyl;


  if (job == NULL || job->oi == NULL || job->in_progress == 0 || job->more_data == NULL) return 0;
  if (im < job->imi)  return 0;

  ci = track_info->c_i;
  ds = job->op->dat[0];
  sp = (hat_param *)job->more_data;
  bt = track_info->bd[job->bead_nb];
  //  snprintf(buf,1024,"Hat %d coucou %03d/%d ",n_hat,ds->nx,ds->mx);
  /my_set_window_title(buf);
  if (bt->opt == NULL)   return 0;
  dsx = bt->opt->dat[0];
  dsy = bt->opt->dat[1];
  dsz = bt->opt->dat[2];
  un = bt->opt->yu[bt->opt->c_yu];
  get_afine_param_from_unit(un, &ay, &dy);
  un = job->op->yu[job->op->c_yu];
  get_afine_param_from_unit(un, &ayl, &dyl);
  dy /= dyl;

  ims = sp->im[ds->nx] - sp->im[0] + sp->dead;
  if (ds->nx >= ds->mx)   return 0;
  ime = sp->im[ds->nx + 1] - sp->im[0];

  for(i = ims, k = 0, ds->yd[ds->nx] = 0; i < ime; i++, k++)
    ds->yd[ds->nx] += dsz->yd[i];
  if (k) ds->yd[ds->nx] /= k;
  ds->yd[ds->nx] = dy * ds->yd[ds->nx];
  ds->xd[ds->nx] = sp->rot[ds->nx];
  snprintf(buf,1024,"Hat %d %03d/%d Rot %6.3f tr. <z> %6.3f microns",
	  n_hat,ds->nx,ds->mx,sp->rot[ds->nx],ds->yd[ds->nx]);
  ds->ny = ds->nx = ds->nx + 1;
 my_set_window_title(buf);
  job->op->need_to_refresh = 1;
  if (ds->nx < ds->mx) job->imi = sp->im[ds->nx+1] + 40;
  else 
    {
      job->in_progress = 0; 
      win_title_used = 0;
      if (job->bead_nb == last_bead)
	{
	  n_hat++;
	  last_im_who_asked_menu_update = im;
	  Open_Pico_config_file();
	  set_config_float("Hat_Curve", "n_hat", n_hat);
	  write_Pico_config_file();
	  Close_Pico_config_file();
	}
    }
  return 0;
}



int record_hat(void)
{
  static int first = 1;
   int i, j, k, im, li, nw;
   imreg *imr = NULL;
   O_i *oi = NULL;
   b_track *bt = NULL;
   pltreg *pr = NULL;
   O_p *op = NULL;
   d_s *ds = NULL;
   static int nf = 2048, nstep = 33,  dead = 10, nper = 64, one_way = 0;
   static float rot_start = 10, rot_step = 5, zmag,  zmag_finish = 8;
   char question[1024] = {0}, name[64] = {0};
   hat_param *sp = NULL; 

   if(updating_menu_state != 0)	
     {
       if (track_info->n_b == 0) 
	 active_menu->flags |=  D_DISABLED;
       else
	 {
           if (track_info->SDI_mode == 0)
              {
                  for (i = 0, k = 0; i < track_info->n_b; i++)
                     {
                         bt = track_info->bd[i];
                         k += (bt->calib_im != NULL) ? 1 : 0;
                     }
              }
           else k = 1;
	   if (k) active_menu->flags &= ~D_DISABLED;	
	   else active_menu->flags |=  D_DISABLED;
	 }
       return D_O_K;
     }

   /*
   if(updating_menu_state != 0)	
     {
       if (track_info->n_b == 0) 
	 active_menu->flags |=  D_DISABLED;
       else active_menu->flags &= ~D_DISABLED;			
       return D_O_K;
     }
   */
   if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)	return OFF;

  heap_check();
  Open_Pico_config_file();
  if (first)
    {
      n_hat = get_config_float("Hat_Curve", "n_hat", n_hat);
      first = 0;
    }
  //win_printf("next n_hat %d ",n_hat);
  rot_step = get_config_float("Hat_Curve", "rot_step", 5);
  nstep = get_config_int("Hat_Curve", "n_step", 9);
  nper = get_config_int("Hat_Curve", "nper", 128);
  one_way = get_config_int("Hat_Curve", "one_way", 0);
  dead = get_config_int("Hat_Curve", "dead", 160);
  zmag = get_config_float("Hat_Curve", "zmag", 8.5);
  //z_cor = get_config_float("Hat_Curve", "correction", 0.878);
  zmag_finish = get_config_float("Hat_Curve", "zmag_after", 8);
  Close_Pico_config_file();

   rot_start = prev_mag_rot;
   zmag = prev_zmag; 

   //rot_start = read_rot_value();
   //zmag = read_magnet_z_value();
   snprintf(question,1024,"Hat curve at Z_{mag} present val %g angular position %g\n" 
	   "starting rotation at %%8f with step of %%8f nb of step %%8d\n"
	   "one way and back %%R or only one way %%r\n"
	   "Number of frames to record bead position at each step  %%8d\n"
	   "dead period (nb. of frames skipped during magnets rotation) %%8d\n" 
	   "Z_{mag}  %%8f\n" 
	   "zmag finish %%8f\n"
	   ,rot_start,zmag);

  heap_check();
   i = win_scanf(question,&rot_start,&rot_step,&nstep,&one_way,&nper,&dead,&zmag,&zmag_finish);	
  heap_check();
   if (i == WIN_CANCEL)	return OFF; 
  heap_check();

  Open_Pico_config_file();
  set_config_float("Hat_Curve", "rot_step", rot_step);
  set_config_int("Hat_Curve", "n_step", nstep);
  set_config_int("Hat_Curve", "nper", nper);
  set_config_int("Hat_Curve", "one_way", one_way);
  set_config_int("Hat_Curve", "dead", dead);
  set_config_float("Hat_Curve", "zmag", zmag);
  //set_config_float("Hat_Curve", "correction", z_cor);
  set_config_float("Hat_Curve", "zmag_after", zmag_finish);
  write_Pico_config_file();
  Close_Pico_config_file();

  heap_check();
   nw = (one_way) ? 1 : 2;
   sp = (hat_param*)calloc(1,sizeof(hat_param));
   if (sp == NULL)  win_printf_OK("Could no allocte hat parameters!");
   sp->im = (int*)calloc(nw*nstep+1,sizeof(int));
   sp->rot = (float*)calloc(nw*nstep+1,sizeof(float));
   if (sp->im == NULL || sp->rot == NULL)  win_printf_OK("Could no allocte hat parameters!");



   for (i = 0, nf = 0; i < nw*nstep ; i++)
     {
       j = i;
       j %= 2*nstep;
       j = (j < nstep) ? j : 2 * nstep -1 - j;
       nf += nper + dead;
     }

   sp->nf = nf;
   sp->nstep = nstep;
   sp->dead = dead;
   sp->nper = nper;
   sp->one_way = one_way;

   sp->rot_start = rot_start;
   sp->rot_step = rot_step;
   sp->zmag_finish = zmag_finish;
   sp->z_cor = track_info->focus_cor;
   sp->zmag = zmag;
   heap_check();
   snprintf(name,64,"Hat curve %d",n_hat);

   // We create a plot region to display bead position, hat  etc.
   pr = create_hidden_pltreg_with_op(&op, nf, nf, 0,name);
   if (pr == NULL)  win_printf_OK("Could not find or allocte plot region!");
   //op = pr->one_p;
   ds = op->dat[0];   

   //win_printf("nf %d",nf);


   im = track_info->imi[track_info->c_i];                                 

  heap_check();

   k = fill_next_available_action(im+4, MV_ZMAG_ABS, sp->zmag);
   if (k < 0)	win_printf_OK("Could not add pending action!");

   k = fill_next_available_action(im+8, MV_ROT_ABS, sp->rot_start);
   if (k < 0)	win_printf_OK("Could not add pending action!");


   im = track_info->imi[track_info->c_i];                                 

   im += 64;


   for (i = 0, nf = 0; i < nw*nstep ; i++)
     {
       j = i;
       j %= 2*nstep;
       j = (j < nstep) ? j : 2 * nstep -1 - j;
       sp->rot[i] = rot_start + (j * rot_step);
       sp->im[i] = im + nf;
       nf += nper + dead;
     }
   sp->rot[i] = (one_way) ? (rot_start + (nstep * rot_step)) : rot_start;
   sp->im[i] = im + nf;


  heap_check();

   //we create one plot for each bead
   for (i = li = 0; i < track_info->n_b; i++)
     {
       bt = track_info->bd[i];
       if ((bt->calib_im == NULL) || (bt->not_lost <= 0) || (bt->in_image == 0))
	 continue;
       if (li)
	 {
	   op = create_and_attach_one_plot(pr, nf, nf, 0);
	   ds = op->dat[0];
	 }
       li++;
       bt->opt = op;
       ds->nx = ds->ny = 0;
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
		     "X coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_hat,i
		     ,track_info->cl,track_info->cw,nf
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,pico_sample
		     ,rot_start
		     ,zmag
		     ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");
       if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
	 return win_printf_OK("I can't create plot !");
       ds->nx = ds->ny = 0;
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
		     "Y coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_hat,i
		     ,track_info->cl,track_info->cw,nf
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,pico_sample
		     ,rot_start
		     ,zmag
		     ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");
       if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
	 return win_printf_OK("I can't create plot !");
       ds->nx = ds->ny = 0;
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
		     "Z coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_hat,i
		     ,track_info->cl,track_info->cw,nf
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,pico_sample
		     ,rot_start
		     ,zmag
		     ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");

       uns_oi_2_op(oi, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
       create_attach_select_x_un_to_op(op, IS_SECOND, 0
				       ,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");
       
       op->op_idle_action = hat_op_idle_action; 
       op->op_post_display = hat_op_post_display;
       set_op_filename(op, "X(t)Y(t)Z(t)bd%dhat%d.gr",i,n_hat);
       set_plot_title(op, "Bead %d trajectory %d zmag %g", i,n_hat,zmag);
       set_plot_x_title(op, "Time");
       set_plot_y_title(op, "Position");			
       op->user_ispare[ISPARE_BEAD_NB] = i;
       op->x_lo = (-nf/64); ///Pico_param.camera_param.camera_frequency_in_Hz;
       op->x_hi = (nf + nf/64); ///Pico_param.camera_param.camera_frequency_in_Hz;
       set_plot_x_fixed_range(op);
         fill_next_available_job_spot(im, im+32, COLLECT_DATA, i, imr, oi, pr, op, (void*)sp, hat_job_xyz, 0);
       //win_printf("bd %d",i);
     }
   


   op = create_and_attach_one_plot(pr, nf, nf, 0);
   ds = op->dat[0];
   set_plot_title(op, "Dz and rot %d at Zmag %g", n_hat,zmag);
   set_plot_file(op,"bddzr%03d.gr",n_hat);
   set_plot_x_title(op, "Real time");
   set_plot_y_title(op, "dZ and rotation");			

   set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d \n "
		 "Magnet rot position l = %d, w = %d, nim = %d\n"
		 "objective %g, zoom factor %f, sample %s Rotation %g\n"
		 "Zmag = %g Calibration from %s"
		 ,Pico_param.camera_param.camera_frequency_in_Hz, n_hat
		 ,track_info->cl, track_info->cw, nf
		 ,Pico_param.obj_param.objective_magnification
		 ,Pico_param.micro_param.zoom_factor
		 ,sample
		 ,rot_start,zmag
		 ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined"); 

   create_attach_select_x_un_to_op(op, IS_SECOND, 0
				       ,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");

   op->x_lo = (-nf/64); //*op->dx;
   op->x_hi = (nf + nf/64);//*op->dx;
   set_plot_x_fixed_range(op);
   ds->nx = ds->ny = 0;
   op->op_idle_action = hat_op_idle_action; 
   op->op_post_display = hat_op_post_display;
   op->user_ispare[ISPARE_BEAD_NB] = 0;
   fill_next_available_job_spot(im, im+32, COLLECT_DATA, i, imr, oi, pr, op, NULL, hat_job_rot, 0);

   for (i = 0; i < track_info->n_b; i++)
     {
       bt = track_info->bd[i];
       if ((bt->calib_im == NULL) || (bt->not_lost <= 0) || (bt->in_image == 0))
	 continue;

       op = create_and_attach_one_plot(pr, nw*nstep,  nw*nstep, 0);
       ds = op->dat[0];
       ds->nx = ds->ny = 0;       
       //set_dot_line(ds);
       set_plot_symb(ds, "\\pt5\\oc");
       set_plot_title(op, "\\stack{{Beads %d Zmag %g Hat %d}{rot %g + %d x %g}}", i, zmag, n_hat,rot_start,nstep,rot_step);
       set_plot_y_title(op,"Length (\\mu m)");
       set_plot_x_title(op,"Rotation (turn)");
       set_plot_file(op,"hatzbd%01d%03d%s",i,n_hat,".gr");
       create_attach_select_y_un_to_op(op, IS_METER, 0, 1, -6, 0, "\\mu m");              

       set_ds_source(ds,"Hat Z\nZmag %g Rot start = %g for %d frames hat by steps of %g"
		     " for %d steps \ndead period %d\n"
		     "Bead  calbration %s \n"
		     ,zmag,rot_start,nper,rot_step,nstep
		     ,dead,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");	

       op->op_idle_action = hat_op_idle_action; 
       op->op_post_display = hat_op_post_display;
       op->user_ispare[ISPARE_BEAD_NB] = i;
       j =  nper;
       // there is a bug in hat_job_hat
       fill_next_available_job_spot(im, sp->im[1] + 40, COLLECT_DATA, i, imr, oi, pr, op, (void*)sp, hat_job_hat, 0);
       last_bead = i;
     }
   for (i = 0, nf = 0; i <= nw*nstep ; i++)
     {
       k = fill_next_available_action(sp->im[i], MV_ROT_ABS, sp->rot[i]);
       if (k < 0)	win_printf_OK("Could not add pending action!");
     }

   k = fill_next_available_action(sp->im[nw*nstep]+nper, MV_ZMAG_ABS, sp->zmag_finish);
   if (k < 0)	win_printf_OK("Could not add pending action!");
   win_title_used = 1;

 return D_O_K;
}

# endif

int hat_record_job_rot(int im, struct future_job *job)
{
  register int j, k;
  int min_pos, max_pos, page_n, i_page;
  d_s *ds = NULL;

  if (hat_r == NULL)  return 0;
  if (job == NULL || job->oi == NULL || job->in_progress == 0 || job->op->n_dat < 1) return 0;
  if (im < job->imi)  return 0;
  if (hat_r->abs_pos < 0)  return 0;

  ds = job->op->dat[0];


  max_pos = j = hat_r->abs_pos;
  min_pos = hat_r->starting_in_page_index + (hat_r->starting_page * hat_r->page_size);

  for (k = ds->nx, j = min_pos + k; j < max_pos; k++, j++)
    {
      page_n = j/hat_r->page_size;
      i_page = j%hat_r->page_size;
      add_new_point_to_ds(ds, hat_r->imi[page_n][i_page], hat_r->rot_mag[page_n][i_page]);
    } 
  job->op->need_to_refresh = 1;
  job->imi += 32;
  if (working_gen_record == NULL) 
      job->in_progress = 0;

  return 0;
}



int hat_record_job_hat(int im, struct future_job *job)
{
  register int i, j, k;
  int  max_pos, page_n, i_page, np, nd;// min_pos
  static float min = 0, max = 0, tmp, tmp2, zm;
  b_record *br = NULL, *brf = NULL;
  d_s *ds = NULL;

  if (hat_r == NULL)  return 0;
  if (job == NULL || job->oi == NULL || job->in_progress == 0 || job->op->n_dat < 1) return 0;
  if (im < job->imi)  return 0;
  if (hat_r->abs_pos < 0)  return 0;

  ds = job->op->dat[0];
  br = hat_r->b_r[job->bead_nb];


  if (do_hat_substract_fixed_bead > 0 && hat_fixed_bead_nb >= 0 && hat_fixed_bead_nb < hat_r->n_bead)
    {
      brf = hat_r->b_r[hat_fixed_bead_nb];
       set_plot_y_title(job->op, "\\Delta(Z_%d - Z_%d)",job->bead_nb,hat_fixed_bead_nb);
    }
  else        set_plot_y_title(job->op, "Z position");


  max_pos = j = hat_r->abs_pos;
  //min_pos = hat_r->starting_in_page_index + (hat_r->starting_page * hat_r->page_size);

  np = sp->nstep; 
  np += (sp->one_way) ? 0 : np;

  nd = (sp->c_step/4);
  if (sp->c_step%4 == 0) nd--;
 
  nd = (nd <= ds->mx) ? nd : ds->mx;
  ds->nx = ds->ny = nd;
  //for (i = (ds->nx) ? ds->nx - 1 : 0; i < nd; i++)
  for (i = 0; i < nd; i++)
    {
      ds->xd[i] = ds->yd[i] = 0;
      for (j = sp->grim[i], k = 0; (j < sp->grim[i] + sp->nper) && (j < max_pos); j++, k++)
	{
	  page_n = j/hat_r->page_size;
	  i_page = j%hat_r->page_size;

	  ds->xd[i] += hat_r->rot_mag[page_n][i_page];
	  zm = br->z[page_n][i_page];
	  if (brf != NULL) zm -= brf->z[page_n][i_page];
	  ds->yd[i] += hat_r->z_cor * zm;
	}
      if (k)
	{
	  ds->xd[i] /= k;
	  ds->yd[i] /= k;
	  if (i == 0)  	  min = max = ds->yd[i];
	  min = (ds->yd[i] < min) ? ds->yd[i] : min;
	  max = (ds->yd[i] > max) ? ds->yd[i] : max;
	}
      else if (i == nd-1)
	  ds->nx = ds->ny = nd-1;

      //if ((i == np - 1) && (k == sp->nper)) job->in_progress = 0;
    }



  tmp = (max - min)*0.05;
  tmp = (tmp < min_scale_z) ? min_scale_z : tmp;
  for (i = 0, tmp2 = min_scale_z;tmp > tmp2; i++)
    {
      if (i%3 == 0) tmp2 += tmp2;
      else if (i%3 == 1) tmp2 += tmp2;
      else tmp2 *= 2.5;
    }
  tmp = tmp2/2;
  min -=  tmp;
  max +=  tmp;


  if (job->op->iopt2 & Y_LIM)  // fixed range
    {
      min = job->op->y_lo;
      max = job->op->y_hi;
    }

  job->op->y_lo = min;
  job->op->y_hi = max;
  //set_plot_y_fixed_range(job->op);

  /*
  if (nd < 2)
    {
      ds->xd[0] = ds->yd[0] = 0;
      ds->xd[1] = ds->yd[1] = 0.1;
      ds->nx = ds->ny = 2;
    }
  */
  job->op->need_to_refresh = 1;
  job->imi += 32;
  if (working_gen_record == NULL) 
    {
      job->in_progress = 0;
      win_title_used = 0;
    }
  return 0;
}




int hat_record_job_z(int im, struct future_job *job)
{
  int i, j, k, i_mean, np;
  int  nf, page_n, i_page, max_pos, min_pos, fir, mean_n = 0, imas = 0, imae = 0;//  imi, ims,
  b_record *br = NULL, *brf = NULL;
  d_s *dsz = NULL, *dszf = NULL, *dszm = NULL, *dsza = NULL;
  float min = 0, max = 0, minz = 0, maxz = 0, tmp, tmp2, zm;
  double mean = 0;

  if (job == NULL || job->oi == NULL || job->in_progress == 0 || job->op->n_dat < 3) return 0;
  if (im < job->imi)  return 0;
  if (hat_r == NULL || hat_r->abs_pos < 0)  return 0;

  dsz = job->op->dat[0];
  dszf = job->op->dat[1];
  dszm = job->op->dat[2];
  if (job->op->n_dat > 3)
    dsza = job->op->dat[3];

  br = hat_r->b_r[job->bead_nb];

  if (do_hat_substract_fixed_bead > 0 && hat_fixed_bead_nb >= 0 && hat_fixed_bead_nb < hat_r->n_bead)
    {
      brf = hat_r->b_r[hat_fixed_bead_nb];
       set_plot_y_title(job->op, "\\Delta(Z_%d - Z_%d)",job->bead_nb,hat_fixed_bead_nb);
    }
  else        set_plot_y_title(job->op, "Z position");



  max_pos = j = (hat_r->abs_pos < 0) ? 0 : hat_r->abs_pos;
  min_pos = hat_r->starting_in_page_index + (hat_r->starting_page * hat_r->page_size);
  //imi = hat_r->imi[j/hat_r->page_size][j%hat_r->page_size]; //track_info->imi[ci];
  //ims = hat_r->last_starting_pos;

  nf = max_pos - min_pos;
  /*
  if (nf > dsz->mx)
    {
      win_printf("adjusting data set size");
      build_adjust_data_set(dsz, nf+256, nf+256);
      build_adjust_data_set(dszm, nf+256, nf+256);
      build_adjust_data_set(dszf, (nf+256)/sp->fir, (nf+256)/sp->fir);
    }
  nf = (nf < dsz->mx) ? nf : dsz->mx;
  */
  dsz->nx = dsz->ny =  dszm->nx = dszm->ny = 0;
  if (dsza) dsza->nx = dsza->ny = 0;
  np = sp->nstep; 
  np += (sp->one_way) ? 0 : np;

  for (i_mean = 0; i_mean < np; i_mean++)
    {
      if (sp->grim[i_mean] >= min_pos) break;
    }

  for (i = 0, j = min_pos; i < nf && j < max_pos; i++, j++)
    {
      page_n = j/hat_r->page_size;
      i_page = j%hat_r->page_size;
      //if (page_n <= hat_r->n_page && i_page <= hat_r->in_page_index)
      zm =  br->z[page_n][i_page];
      if (brf != NULL) zm -=  brf->z[page_n][i_page];
      add_new_point_to_ds(dsz, hat_r->imi[page_n][i_page], hat_r->z_cor * zm);
      add_new_point_to_ds(dszm, hat_r->imi[page_n][i_page], hat_r->rot_mag[page_n][i_page]);
      //dsz->xd[i] = dszm->xd[i] = hat_r->imi[page_n][i_page];
      //dsz->yd[i] = hat_r->z_cor * br->z[page_n][i_page];
      //dszm->yd[i] = hat_r->rot_mag[page_n][i_page];
      if (i_mean < np)
	{
	  if (j == sp->grim[i_mean])
	    {
	      mean = 0;
	      mean_n = 0;
	      imas = hat_r->imi[page_n][i_page];
	    }
	  if (j >= sp->grim[i_mean] && j < sp->grim[i_mean] + sp->nper)
	    {
	      mean += hat_r->z_cor * br->z[page_n][i_page];
	      mean_n++;
	      imae = hat_r->imi[page_n][i_page];
	    }
  
	  if ((i_mean <= (sp->c_step/4) - 1) && (j == (sp->grim[i_mean] + sp->nper - 1)))
	    {
	      if (mean_n) mean/=mean_n;
	      dsza->yd[dsza->nx] = mean;
	      dsza->xd[dsza->nx] = (float)(imae + imas)/2;
	      dsza->xe[dsza->nx] = (float)(imae - imas)/2;
	      dsza->nx++;
	      dsza->ny = dsza->nx;
	      i_mean++;
	    }

	}
      if (i == 0)  
	{
	  min = max = dsz->yd[i];
	  minz = maxz = dszm->yd[i];
	}
      min = (dsz->yd[i] < min) ? dsz->yd[i] : min;
      minz = (dszm->yd[i] < minz) ? dszm->yd[i] : minz;
      max = (dsz->yd[i] > max) ? dsz->yd[i] : max;
      maxz = (dszm->yd[i] > maxz) ? dszm->yd[i] : maxz;
    }
  //dsz->nx = dsz->ny =  dszm->nx = dszm->ny = nf;
  /*
  j = ((int)(8*dsz->xd[0]))/dsz->mx;
  job->op->x_lo = j*dsz->mx/8;
  job->op->x_hi = (j+9)*dsz->mx/8;
  set_plot_x_fixed_range(job->op);
  */

  j = (int)(dsz->xd[0]);
  job->op->x_lo = 128*(j/128);
  j = 128+(int)(dsz->xd[nf-1]);
  job->op->x_hi = 128*(j/128);
  set_plot_x_fixed_range(job->op);

  fir = sp->fir;
  tmp = (max - min)*0.05;
  tmp = (tmp < min_scale_z) ? min_scale_z : tmp;
  for (i = 0, tmp2 = min_scale_z;tmp > tmp2; i++)
    {
      if (i%3 == 0) tmp2 += tmp2;
      else if (i%3 == 1) tmp2 += tmp2;
      else tmp2 *= 2.5;
    }
  tmp = tmp2/2;
  min -=  tmp;
  max +=  tmp;

  if (job->op->iopt2 & Y_LIM)  // fixed range
    {
      min = job->op->y_lo;
      max = job->op->y_hi;
    }

  tmp = (maxz - minz)*0.05;
  minz -=  6*tmp;
  maxz +=  tmp;


  if (minz > min && maxz < max)
    {
      job->op->yu[job->op->c_yu_p]->ax = 0;
      job->op->yu[job->op->c_yu_p]->dx = 1;
    }
  else
    {
      if ((maxz - minz) < min_scale_rot)
	{
	  minz = (maxz + minz)/2;
	  maxz = minz + min_scale_rot/2;
	  minz = minz - min_scale_rot/2;
	}
      tmp = (max-min)/(maxz - minz);
      for (i = 0; i < nf; i++)
	{
	  dszm->yd[i] = min + (dszm->yd[i] - minz)*tmp;
	}
      if (tmp > 0) 
	{
	  job->op->yu[job->op->c_yu_p]->ax = minz - min/tmp;
	  job->op->yu[job->op->c_yu_p]->dx = (float)1/tmp;
	}
    }
  job->op->y_lo = min;
  job->op->y_hi = max;
  //set_plot_y_fixed_range(job->op);

  for (i = 0, j = 0, k = 0, dszf->yd[0] = dszf->xd[0] = 0; i < nf; i++)
    {
      if (k == 0) add_new_point_to_ds(dszf, 0, 0);
      dszf->yd[j] += dsz->yd[i];
      dszf->xd[j] += dsz->xd[i];
      k++;
      if (k >= fir)
	{
	  dszf->yd[j] /= fir;
	  dszf->xd[j] /= fir;
	  j++;
	  dszf->yd[j] = dszf->xd[j] = 0;
	  k = 0;
	}
    }
  dszf->nx = dszf->ny = j;
  job->op->need_to_refresh = 1;
  job->imi += 32;

  if (working_gen_record == NULL) 
      job->in_progress = 0;
  return 0;
}


char *generate_hat_title(void)
{
  return hat_state_disp;
}



int hat_action(int im, int c_i, int aci, int init_to_zero)
{
  int k, i, np;
  static int suspended = 0;
  int  page_n, abs_pos, i_page;

  if (init_to_zero)
    {
      suspended = 0;
      return 0;
    }
  (void)aci;
  if (hat_r == NULL || sp == NULL)  return 0;
  i = (sp->c_step/4) - 1;
  np = sp->nstep; 
  np += (sp->one_way) ? 0 : np;
  if (hat_state == HAT_SUSPENDED) 
    {
      snprintf(hat_state_disp,sizeof(hat_state_disp),"Hat %d supended at point %d/%d",n_hat,i,np);
      imr_TRACK_title_change_asked++;
      suspended = 1;
    }
  if (hat_state != HAT_RUNNING)  
    {
      snprintf(hat_state_disp,sizeof(hat_state_disp),"Hat %d completed with %d points",n_hat,i);
      imr_TRACK_title_change_asked++;
      return 0;
    }


  abs_pos = (hat_r->abs_pos < 0) ? 0 : hat_r->abs_pos;
  page_n = abs_pos/hat_r->page_size;
  i_page = abs_pos%hat_r->page_size;

  hat_r->action_status[page_n][i_page] &= 0xff000000;
  hat_r->action_status[page_n][i_page] |= (0xffff&i<<8) + sp->c_step%4;
  if (i >= np)   
    {
      if (im < sp->im[np-1] + sp->nper + 512) 	
	return 0;  // we are nearly finished    
      else
	{
	  hat_r->action_status[page_n][i_page] &= ~DATA_AVERAGING; 
	  snprintf(hat_state_disp,sizeof(hat_state_disp),"Hat %d completed with %d points",n_hat,i);
	  imr_TRACK_title_change_asked++;
	  working_gen_record = NULL;
	  if (hat_r != NULL)	      hat_r->n_record++;
	  n_hat++;
	  Open_Pico_config_file();
	  set_config_int("Hat_Curve", "n_hat", n_hat);
	  write_Pico_config_file();
	  Close_Pico_config_file();
	  return 0;  // we have finished
	}
    }
  if (sp->c_step == 0)   // we move to zmag_rotate 
    {
      snprintf(hat_state_disp,sizeof(hat_state_disp),"moving Zmag to %6.3f Hat %d ",sp->zmag_rotate,n_hat);
      imr_TRACK_title_change_asked++;
      k = fill_next_available_action(im+1, MV_ZMAG_ABS, sp->zmag_rotate);
      if (k < 0)	win_printf_OK("Could not add pending action!");
      sp->c_step+=2;
    }
  else if (sp->c_step == 2)   // we wait for zmag to stop moving and then start rotation
    {
      if (fabs(track_info->zmag[c_i] - sp->zmag_rotate) > 0.005) 
	return 0;  // we are not yet arrived     
      k = fill_next_available_action(im+1, BEAD_CROSS_SAVED, 0);
      if (k < 0)	win_printf_OK("Could not add pending action!");

      k = fill_next_available_action(im+5, MV_ROT_ABS, sp->rot_start);
      if (k < 0)	win_printf_OK("Could not add pending action!");
      sp->c_step+=2;
    }
  else if (sp->c_step >= 4 && sp->c_step%4 == 0)   
    { // we wait for rotation to stop moving and then adapt zmag
      snprintf(hat_state_disp,sizeof(hat_state_disp),"Hat %d rot %5.2f going to point %d/%d (rot = %5.2f)",
	      n_hat,track_info->rot_mag[c_i],i,np,sp->rot[i]);
      imr_TRACK_title_change_asked++;
      if (fabs(track_info->rot_mag[c_i] - sp->rot[i]) > 0.005) 
	return 0;  // we are not yet arrived at rotation position
      // we go to the zmag position to measure 
      k = fill_next_available_action(im+5, MV_ZMAG_ABS, sp->zmag);
      if (k < 0)	win_printf_OK("Could not add pending action!");
      sp->c_step++;      
    }
  else if (sp->c_step >= 4 && sp->c_step%4 == 1)   
    { // we wait for rotation to stop moving and then start acquisition
      snprintf(hat_state_disp,sizeof(hat_state_disp),"Hat %d rot %5.2f going to point %d/%d (rot = %5.2f)",
	      n_hat,track_info->rot_mag[c_i],i,np,sp->rot[i]);
      imr_TRACK_title_change_asked++;
      if (fabs(track_info->zmag[c_i] - sp->zmag) > 0.005) 
	return 0;  // we are not yet arrived at zmag measure   
      k = fill_next_available_action(im+1, BEAD_CROSS_RESTORE, 0);
      if (k < 0)	win_printf_OK("Could not add pending action!");
      sp->im[i] = im  + sp->setling; //hat_r->abs_pos + sp->setling;
      sp->grim[i] = abs_pos + sp->setling;
      sp->c_step++;      
    }
  else if (sp->c_step >= 4 && sp->c_step%4 == 2)   
    { // we wait for the end of acquisition and reposition zmag 
      if (suspended == 1) // we restart point averaging
	{
	  sp->im[i] = im  + sp->setling; //hat_r->abs_pos + sp->setling;
	  sp->grim[i] = abs_pos + sp->setling;
	  suspended = 0;
	}
      if (im >= sp->im[i])    hat_r->action_status[page_n][i_page] |= DATA_AVERAGING;
      snprintf(hat_state_disp,sizeof(hat_state_disp),"Hat %d going to point %d/%d (%3.1f%%) (rot = %5.2f)",
	       n_hat,i,np,(float)(100*(im - sp->im[i]))/sp->nper,sp->rot[i]);
      if (im < sp->im[i] + sp->nper) 	return 0;  // we are not yet done     
      hat_r->action_status[page_n][i_page] &= ~DATA_AVERAGING;
      k = fill_next_available_action(im+5, MV_ZMAG_ABS, sp->zmag_rotate);
      if (k < 0)	win_printf_OK("Could not add pending action!");
      sp->c_step++;      
    }
  else if (sp->c_step >= 4 && sp->c_step%4 == 3)   
    { // we wait for zmag reaching zmag_rotate and then start rotation
      if (fabs(track_info->zmag[c_i] - sp->zmag_rotate) > 0.005) 
	return 0;  // we are not yet arrived     
      if (i+1 < np)
	{
	  snprintf(hat_state_disp,sizeof(hat_state_disp),"Hat %d going to point %d/%d (%3.1f%%) (rot = %5.2f)",
	      n_hat,i,np,(float)(100*(im - sp->im[i]))/sp->nper,sp->rot[i]);
	  imr_TRACK_title_change_asked++;
	  k = fill_next_available_action(im+1, BEAD_CROSS_SAVED, 0);
	  if (k < 0)	win_printf_OK("Could not add pending action!");
	  k = fill_next_available_action(im+5, MV_ROT_ABS, sp->rot[i+1]);
	  if (k < 0)	win_printf_OK("Could not add pending action!");
	}
      else
	{
	  k = fill_next_available_action(im+1, MV_ZMAG_ABS, sp->zmag_finish);
	  if (k < 0)	win_printf_OK("Could not add pending action!");
	}
      sp->c_step++;
    }

  // in the process of adding zmag to rotate
  return 0;
}


int record_hat_record(void)
{
  static int first = 1;
   int i, j, k, im, li, nw;
   imreg *imr = NULL;
   O_i *oi = NULL;
   b_track *bt = NULL;
   pltreg *pr = NULL;
   O_p *op = NULL;
   d_s *ds = NULL;
   int dead;
   static int nf = 2048, nstep = 33, nper = 64, one_way = 0, setling = 16, fir = 16;
   static float rot_start = 10, rot_step = 5, zmag,  zmag_finish = 8, movie_rw = 1.0, movie_rh = 1.0, zmag_rotate = FLT_MAX;
   char question[4096] = {0}, name[64] = {0}, fullfilename[512] = {0};
   static int with_radial_profiles = 0, streaming = 1, file_selection = 0, fixed_bead_nbl = -1, ortho_profiles = 0, movie_track = 0;
   static int special_saving = 0, rec_xy_prof = 0, rec_xy_diff_prof = 0, rec_small_image = 0, xyz_error = 1, xy_tracking_type;

   if(updating_menu_state != 0)	
     {
       if (working_gen_record != NULL || track_info->n_b == 0) 
	 active_menu->flags |=  D_DISABLED;
       else
	 {
           if (track_info->SDI_mode == 0)
              {
                  for (i = 0, k = 0; i < track_info->n_b; i++)
                     {
                         bt = track_info->bd[i];
                         k += (bt->calib_im != NULL) ? 1 : 0;
                     }
              }
           else k = 1;
	   if (k) active_menu->flags &= ~D_DISABLED;	
	   else active_menu->flags |=  D_DISABLED;
	 }
       return D_O_K;
     }
   heap_check();
   if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)	return OFF;


   Open_Pico_config_file();
   if (first)
     {
       n_hat = get_config_int("Hat_Curve", "n_hat", n_hat);
       first = 0;
     }
   //win_printf("next n_hat %d ",n_hat);
   rot_step = get_config_float("Hat_Curve", "rot_step", 5);
   nstep = get_config_int("Hat_Curve", "n_step", 9);
   nper = get_config_int("Hat_Curve", "nper", 128);
   setling = get_config_int("Hat_Curve", "setling", 16);
   one_way = get_config_int("Hat_Curve", "one_way", 0);
   zmag = get_config_float("Hat_Curve", "zmag", -1);
   zmag_rotate = get_config_float("Hat_Curve", "zmag_rotate", -1);
   //z_cor = get_config_float("Hat_Curve", "correction", 0.878);
   zmag_finish = get_config_float("Hat_Curve", "zmag_after", -1);
   Close_Pico_config_file();
   n_hat = find_trk_index_not_used("Hat_", n_hat);
   
   heap_check();
   rot_start = prev_mag_rot;
   zmag = prev_zmag; 
   if (zmag_rotate == FLT_MAX)    zmag_rotate = zmag; 
   
   if (current_write_open_path_picofile != NULL) 
     {
       snprintf(fullfilename,sizeof(fullfilename),"%s\\Hat_%d.trk",current_write_open_path_picofile,n_hat);
       backslash_to_slash(fullfilename);
       file_selection = 0;
     }
   else
     {
       snprintf(fullfilename,sizeof(fullfilename),"Hat_%d.trk",n_hat);
       backslash_to_slash(fullfilename);
       file_selection = 1;
     }
   
   
   for (i = 0, fixed_bead_nbl = -1; i < track_info->n_b; i++)
     {
       if (track_info->bd[i]->fixed_bead)
	 {
	   fixed_bead_nbl = i;
	   break;
	 }
     }


   //   rot_start = read_rot_value();
   //zmag = read_magnet_z_value();
   snprintf(question,sizeof(question),"Hat curve %%5d at Z_{mag} present val %g angular position %g\n" 
	    "starting rotation at %%8f with step of %%8f nb of step %%8d\n"
	    "one way and back %%R or only one way %%r\n"
	    "You can specify a fixed bead to suppress drifts \n"
	    "Indicate the fixed bead number %%6d (set to negative value if not needed)\n"
	    "Number of frames to record bead position at each step  %%8d\n"
	    "setling time (nb. of frames skipped after magnets rotation) %%8d\n" 
	    "Z_{mag} test %%8f zmag value to perform rotation %%8f \nzmag finish %%8f averaging Z over %%8d\n"
	    "\\fbox{Track will be saved in %s\n"
	    "Do you want to select a new file location %%R No %%r Yes\n"
	    "Click here to select special data saving options %%b\n}"
	    ,rot_start,zmag,fullfilename);

   i = win_scanf(question,&n_hat,&rot_start,&rot_step,&nstep,&one_way,&fixed_bead_nbl,&nper,&setling,&zmag,
		 &zmag_rotate,&zmag_finish,&fir,&file_selection,&special_saving);	
   heap_check();
   if (i == WIN_CANCEL)	return OFF; 
   heap_check();

   if (special_saving)
     {
       snprintf(question,sizeof(question),"You can select here special saving options (increasing trk file size)\n"
		"Do you want to record bead profiles %%b\n"
		"Do you want to record bead orthoradial profiles %%b\n"
		"Do you want real time saving %%b\n"
		"Do you want to record bead xyz tracking error %%b\n"
		"Do you want to record bead xy tracking profiles %%b\n"
		"Do you want to record bead xy differential tracking profiles %%b\n"
		"Do you want to record bead image %%b (generates big trk file!)\n"
		"In this case define :\n"
		" \\oc  the ratio of bead image width with the cross arm %%6f\n"
		" \\oc  the ratio of bead image heigh with the cross arm %%6f\n"
		" \\oc  does the image center track the bead %%b\n");
       i = win_scanf(question,&with_radial_profiles,&ortho_profiles, &streaming, &xyz_error, &rec_xy_prof, 
		     &rec_xy_diff_prof, &rec_small_image, &movie_rw, &movie_rh, &movie_track);	
       if (i == WIN_CANCEL)	 return OFF;  
     }


   Open_Pico_config_file();
   set_config_float("Hat_Curve", "rot_start", rot_start);
   set_config_float("Hat_Curve", "rot_step", rot_step);
   set_config_int("Hat_Curve", "n_step", nstep);
   set_config_int("Hat_Curve", "nper", nper);
   set_config_int("Hat_Curve", "one_way", one_way);
   set_config_int("Hat_Curve", "setling", setling);
   set_config_float("Hat_Curve", "zmag", zmag);
   set_config_float("Hat_Curve", "zmag_rotate", zmag_rotate);
   //set_config_float("Hat_Curve", "correction", z_cor);
   set_config_float("Hat_Curve", "zmag_after", zmag_finish);
   set_config_int("Hat_Curve", "fixed_bead_nb", fixed_bead_nbl);
   
   write_Pico_config_file();
   Close_Pico_config_file();
   
   if (fixed_bead_nbl >= 0 && fixed_bead_nbl < track_info->n_b)
     {
       do_hat_substract_fixed_bead = 1;
       hat_fixed_bead_nb = fixed_bead_nbl;
     }
   else do_hat_substract_fixed_bead = 0;
   
   
   heap_check();
   dead = (int)((Pico_param.camera_param.camera_frequency_in_Hz*fabs(rot_step))/get_motors_speed());
   
   nw = (one_way) ? 1 : 2;
  
  
   for (i = 0, nf = 0; i < nw*nstep ; i++)
     {
       j = i;
       j %= 2*nstep;
       j = (j < nstep) ? j : 2 * nstep -1 - j;
       nf += nper + dead + setling;
     }
   nf += 512;
   

   i = win_printf("Rotation scan curve will last %6.2f seconds (%d frames)\n"
		  "Pressing Cancel will abord acquisition",(float)nf/Pico_param.camera_param.camera_frequency_in_Hz,nf);
   if (i == WIN_CANCEL) return D_O_K;
   
   
   sp = (hat_param*)calloc(1,sizeof(hat_param));
   if (sp == NULL)  win_printf_OK("Could no allocte hat parameters!");
   sp->im = (int*)calloc(nw*nstep+1,sizeof(int));
   sp->grim = (int*)calloc(nw*nstep+1,sizeof(int));
   sp->rot = (float*)calloc(nw*nstep+1,sizeof(float));
   if (sp->im == NULL || sp->rot == NULL)  win_printf_OK("Could no allocte hat parameters!");
   
   heap_check();
   
   for (i = 0, nf = 0; i < nw*nstep ; i++)
     {
       j = i;
       j %= 2*nstep;
       j = (j < nstep) ? j : 2 * nstep -1 - j;
       nf += nper + dead + setling;
     }
   nf += 512;
   sp->nf = nf;
   sp->nstep = nstep;
   sp->dead = dead;
   sp->setling = setling;
   sp->nper = nper;
   sp->one_way = one_way;
   sp->c_step = 0;

   sp->rot_start = rot_start;
   sp->rot_step = rot_step;
   sp->zmag_rotate = zmag_rotate;
   sp->zmag_finish = zmag_finish;
   sp->z_cor = track_info->focus_cor;
   sp->zmag = zmag;
   sp->fir = fir;
   snprintf(name,64,"Hat curve %d",n_hat);

   // We create a plot region to display bead position, hat  etc.
   pr = create_hidden_pltreg_with_op(&op, nf, nf, 0,name);
   if (pr == NULL)  win_printf_OK("Could not find or allocte plot region!");
   //op = pr->one_p;
   ds = op->dat[0];   
   ds->xd[1] = ds->yd[1] = 0.1; 
   im = track_info->imi[track_info->c_i];                                 
  heap_check();
   for (i = 0, nf = 0; i < nw*nstep ; i++)
     {
       j = i;
       j %= 2*nstep;
       j = (j < nstep) ? j : 2 * nstep -1 - j;
       sp->rot[i] = rot_start + (j * rot_step);
       sp->im[i] = im + nf;
       nf += nper + dead + setling;
     }
   sp->rot[i] = (one_way) ? (rot_start + (nstep * rot_step)) : rot_start;
   sp->im[i] = im + nf;
   nf += 512;

   xy_tracking_type = (rec_xy_prof) ? XY_BEAD_PROFILE_RECORDED : 0;
   xy_tracking_type |= (rec_xy_diff_prof) ? XY_BEAD_DIFF_PROFILE_RECORDED : 0;
   xy_tracking_type |= (rec_small_image) ? RECORD_BEAD_IMAGE : 0;
   xy_tracking_type |= (xyz_error) ? XYZ_ERROR_RECORDED : 0;


   hat_r = create_gen_record(track_info, 1, with_radial_profiles, 0,1,xy_tracking_type,movie_rw, movie_rh, movie_track);
   duplicate_Pico_parameter(&Pico_param, &(hat_r->Pico_param_record));
   if (extract_file_name(hat_r->filename, sizeof(hat_r->filename), fullfilename) == NULL)
     snprintf(hat_r->filename, sizeof(hat_r->filename),"Hat_%d.trk",n_hat);
   if (attach_g_record_to_pltreg(pr, hat_r))
     win_printf("Could not attach grecord to plot region!");
   snprintf(hat_r->name,sizeof(hat_r->name),"Hat curve %d",n_hat);
   hat_r->n_rec = n_hat;
   hat_r->data_type |= HAT_CURVE;



  heap_check();
   if (streaming) 
     {
       if (current_write_open_path_picofile != NULL && file_selection == 0) 
	 {
	   strncpy(hat_r->path,current_write_open_path_picofile,sizeof(hat_r->path));
	   write_record_file_header(hat_r, oi_TRACK);
	   hat_r->real_time_saving = 1;
	 }
       else if (do_save_track(hat_r))
	 win_printf("Pb writing tracking header");
     }


   if (current_write_open_path_picofile != NULL)
     {
       dump_to_html_log_file_with_date_and_time(current_write_open_picofile, 
						"<h2>Hat curve recording</h2>"
						"<br>in file %s in directory %s"
						"<br>rot start %g, %d sets of %g lasting %d frames after %d frames to settle <br>"
						"<br<%d way(s)"
						"Zmag %g<br>\n"
						,backslash_to_slash(hat_r->filename),backslash_to_slash(hat_r->path)
						,rot_start,nstep,rot_step,nper,dead,(one_way) ? 1 : 2,
						zmag);
    }




   //li = grab_basename_and_index(hat_r->filename, strlen(hat_r->filename)+1, basename, 256);
   //n_hat = (li >= 0) ? li : n_hat;



   heap_check();
   hat_r->record_action = hat_action;
   hat_action(0,0,0,1);
   hat_state = HAT_RUNNING;
   //we create one plot for each bead
   for (i = li = 0; i < track_info->n_b; i++)
     {
       bt = track_info->bd[i];
       if ((bt->calib_im == NULL) || (bt->not_lost <= 0) || (bt->in_image == 0))
	 continue;
       if (li)
	 {
	   op = create_and_attach_one_plot(pr, nf, nf, 0);
	   ds = op->dat[0];
	 }
       li++;
       bt->opt = op;
       ds->nx = ds->ny = 0;
       //ds->xd[1] = ds->yd[1] = 0.1; 
       set_dot_line(ds);
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d  substracting fixed bead %d\n "
		     "Z coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_hat,i,fixed_bead_nbl
		     ,track_info->cl,track_info->cw,nf
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,pico_sample
		     ,rot_start
		     ,zmag
		     ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");
       if ((ds = create_and_attach_one_ds(op, nf/fir, nf/fir, 0)) == NULL)
	 return win_printf_OK("I can't create plot !");
       ds->nx = ds->ny = 0;
       //ds->xd[1] = ds->yd[1] = 0.1; 
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d  substracting fixed bead %d\n "
		     "filtred Z over %d coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_hat,i,fixed_bead_nbl
		     ,fir,track_info->cl,track_info->cw,nf
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,pico_sample
		     ,rot_start
		     ,zmag
		     ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");

       if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
	 return win_printf_OK("I can't create plot !");
       ds->nx = ds->ny = 0;
       //ds->xd[1] = ds->yd[1] = 0.1; 
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d  substracting fixed bead %d\n "
		     "Zmag l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Zmagnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_hat,i,fixed_bead_nbl
		     ,track_info->cl,track_info->cw,nf
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,pico_sample
		     ,rot_start
		     ,zmag
		     ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");

       if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
	 return win_printf_OK("I can't create plot !");
       alloc_data_set_x_error(ds);
       ds->nx = ds->ny = 0;
       set_ds_dot_line(ds);
       set_ds_point_symbol(ds, "\\oc");
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d substracting fixed bead %d\n "
		     "averaged Z over %d coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_hat,i,fixed_bead_nbl
		     ,fir,track_info->cl,track_info->cw,nf
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,pico_sample
		     ,rot_start
		     ,zmag
		     ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");


       create_attach_select_y_un_to_op(op, 0, 0 ,(float)1, 0, 0, "no_name");
       set_plot_y_prime_title(op, "Magnet rotation (tr)");			

       op->c_yu_p = op->c_yu;
       //       uns_oi_2_op(oi, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
       create_attach_select_y_un_to_op(op, IS_METER, 0 ,(float)1, -6, 0, "\\mu m");
       create_attach_select_x_un_to_op(op, IS_SECOND, 0
				       ,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");
       op->op_idle_action = hat_op_idle_action; 
       op->op_post_display = hat_op_post_display;
       set_op_filename(op, "Z(t)bd%dRothat%d.gr",i,n_hat);
       if (fixed_bead_nbl < 0)	 set_plot_title(op, "Bead %d Z(t) %d zmag %g", i,n_hat,zmag);
       else set_plot_title(op, "Bead %d Z(t) %d zmag %g - fixed bead %d", i,n_hat,zmag,fixed_bead_nbl);

       set_plot_x_title(op, "Time");
       if (fixed_bead_nbl < 0)	set_plot_y_title(op, "Z position");			
       else   set_plot_y_title(op, "Z_{%d} - Z_{%d} position",i,fixed_bead_nbl);			
       op->user_ispare[ISPARE_BEAD_NB] = i;
       op->x_lo = im + (-nf/64); ///Pico_param.camera_param.camera_frequency_in_Hz;
       op->x_hi = im + (nf + nf/64); ///Pico_param.camera_param.camera_frequency_in_Hz;
       set_plot_x_fixed_range(op);
       //op->y_lo = -0.1;
       //op->y_hi = 0.1;
       //set_plot_y_fixed_range(op);
       j = fill_next_available_job_spot(im, im+32, COLLECT_DATA, i, imr, oi, pr, op, (void*)sp, hat_record_job_z, 0);
       if (j >= 0) job_pending[j].fixed_bead_nb = fixed_bead_nbl;
       //win_printf("bd %d",i);
     }
   
  heap_check();
   op = create_and_attach_one_plot(pr, nf, nf, 0);
   ds = op->dat[0];
   ds->xd[1] = ds->yd[1] = 0.1; 
   set_plot_title(op, "Dz and rot %d at Zmag %g", n_hat,zmag);
   set_plot_file(op,"bddzrot%03d.gr",n_hat);
   set_plot_x_title(op, "Real time");
   set_plot_y_title(op, "dZ and rotation");			

   set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d \n "
		 "Magnet rot position l = %d, w = %d, nim = %d\n"
		 "objective %g, zoom factor %f, sample %s Rotation %g\n"
		 "Zmag = %g Calibration from %s"
		 ,Pico_param.camera_param.camera_frequency_in_Hz, n_hat
		 ,track_info->cl, track_info->cw, nf
		 ,Pico_param.obj_param.objective_magnification
		 ,Pico_param.micro_param.zoom_factor
		 ,sample
		 ,rot_start,zmag
		 ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined"); 
   create_attach_select_x_un_to_op(op, IS_SECOND, 0
				       ,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");

   op->x_lo = im - (nf/64);
   op->x_hi = im + nf + (nf/64);
   set_plot_x_fixed_range(op);
   //op->y_lo = -0.1;
   //op->y_hi = 0.1;
   //set_plot_y_fixed_range(op);

   ds->nx = ds->ny = 0;
   op->op_idle_action = hat_op_idle_action; 
   op->op_post_display = hat_op_post_display;
   op->user_ispare[ISPARE_BEAD_NB] = 0;
   fill_next_available_job_spot(im, im+32, COLLECT_DATA, i, imr, oi, pr, op, NULL, hat_record_job_rot, 0);
   for (i = 0; i < track_info->n_b; i++)
     {
       bt = track_info->bd[i];
       if ((bt->calib_im == NULL) || (bt->not_lost <= 0) || (bt->in_image == 0))
	 continue;

       op = create_and_attach_one_plot(pr, nw*nstep,  nw*nstep, 0);
       ds = op->dat[0];
       //ds->xd[1] = ds->yd[1] = 0.1; 
       ds->nx = ds->ny = 0;       
       //set_dot_line(ds);
       set_plot_symb(ds, "\\pt5\\oc");
       set_plot_title(op, "\\stack{{Beads %d Zmag %g Hat %d}{rot %g + %d x %g}}", i, zmag, n_hat,rot_start,nstep,rot_step);
       set_plot_y_title(op,"Length (\\mu m)");
       set_plot_x_title(op,"Rotation (turn)");
       set_plot_file(op,"hatzbd%01d%03d%s",i,n_hat,".gr");
       create_attach_select_y_un_to_op(op, IS_METER, 0, 1, -6, 0, "\\mu m");              

       set_ds_source(ds,"Hat Z\nZmag %g Rot start = %g for %d frames hat by steps of %g"
		     " for %d steps \ndead period %d\n"
		     "Bead  calbration %s \n"
		     ,zmag,rot_start,nper,rot_step,nstep
		     ,dead,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");	

       op->op_idle_action = hat_op_idle_action; 
       op->op_post_display = hat_op_post_display;
       op->user_ispare[ISPARE_BEAD_NB] = i;
       set_plot_y_auto_range(op);
       j =  nper;
       /*
       op->x_lo = -0.1; ///Pico_param.camera_param.camera_frequency_in_Hz;
       op->x_hi = 0.1; ///Pico_param.camera_param.camera_frequency_in_Hz;
       set_plot_x_fixed_range(op);
       op->y_lo = -0.1;
       op->y_hi = 0.1;
       set_plot_y_fixed_range(op);
       */
       // there is a bug in hat_job_hat
       fill_next_available_job_spot(im, sp->im[1] + 40 + 20*i, COLLECT_DATA, i, imr, oi, pr, op, (void*)sp, hat_record_job_hat, 0);
       last_bead = i;
     }
  heap_check();
   im = track_info->imi[track_info->c_i];                                 
   working_gen_record = hat_r;
   hat_r->last_starting_pos = im;
   win_title_used = 1;
   if (imr_and_pr) switch_project_to_this_pltreg(pr);
  heap_check();
   generate_imr_TRACK_title = generate_hat_title;
   return D_O_K;
}



int draw_hat_XYZ_trajectories(void)
{
  int i, j, page_n, i_page;
  b_track *bt = NULL;
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *ds = NULL;
  int nf;
  float rot_start = 10,  zmag;
  b_record *br = NULL;
  g_record *g_r = NULL;
  

  ac_grep(cur_ac_reg,"%pr",&pr);
  g_r = find_g_record_in_pltreg(pr);     

  if(updating_menu_state != 0)	
    {  // we wait for recording finish
      if (g_r == NULL || working_gen_record == g_r) 
	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;	
      return D_O_K;
    }

  if (g_r == NULL || g_r->n_record == 0 || pr == NULL) return OFF;
  nf = g_r->abs_pos;
  if (nf <= 0) return OFF;

  rot_start = g_r->rot_mag[0][0];
  zmag = g_r->zmag[0][0];
  
  //we create one plot for each bead
  for (i = 0; i < g_r->n_bead; i++)
    {
      br = g_r->b_r[i];
      bt = br->b_t;
      if ((bt->calib_im == NULL) || (bt->not_lost <= 0) || (bt->in_image == 0))
	continue;
      op = create_and_attach_one_plot(pr, nf, nf, 0);
      ds = op->dat[0];
      set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
		    "X coordinate l = %d, w = %d, nim = %d\n"
		    "objective %f, zoom factor %f, sample %s Rotation %g\n"
		    "Z magnets %g mm\n"
		    "Calibration from %s"
		    ,Pico_param.camera_param.camera_frequency_in_Hz,n_hat,i
		    ,track_info->cl,track_info->cw,nf
		    ,Pico_param.obj_param.objective_magnification
		    ,Pico_param.micro_param.zoom_factor
		    ,pico_sample
		    ,rot_start
		    ,zmag
		    ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");
      for (j = 0; j < nf; j++)
	{
	  page_n = j/g_r->page_size;
	  i_page = j%g_r->page_size;
	  ds->xd[j] = g_r->imi[page_n][i_page];
	  ds->yd[j] = g_r->ax + g_r->dx * br->x[page_n][i_page];
	}
      
       if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
	 return win_printf_OK("I can't create plot !");
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
		     "Y coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_hat,i
		     ,track_info->cl,track_info->cw,nf
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,pico_sample
		     ,rot_start
		     ,zmag
		     ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");
      for (j = 0; j < nf; j++)
	{
	  page_n = j/g_r->page_size;
	  i_page = j%g_r->page_size;
	  ds->xd[j] = g_r->imi[page_n][i_page];
	  ds->yd[j] = g_r->ay + g_r->dy * br->y[page_n][i_page];
	}
       if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
	 return win_printf_OK("I can't create plot !");
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
		     "Z coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_hat,i
		     ,track_info->cl,track_info->cw,nf
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,pico_sample
		     ,rot_start
		     ,zmag
		     ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");
      for (j = 0; j < nf; j++)
	{
	  page_n = j/g_r->page_size;
	  i_page = j%g_r->page_size;
	  ds->xd[j] = g_r->imi[page_n][i_page];
	  ds->yd[j] = g_r->z_cor * br->z[page_n][i_page];
	}
       create_attach_select_y_un_to_op(op, IS_METER, 0 ,(float)1, -6, 0, "\\mu m");
       create_attach_select_x_un_to_op(op, IS_SECOND, 0
				       ,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");


       set_op_filename(op, "X(t)Y(t)Z(t)bd%dhat%d.gr",i,g_r->n_rec);
       set_plot_title(op, "Bead %d Z(t) %d zmag %g", i,g_r->n_rec,zmag);
       set_plot_x_title(op, "Time");
       set_plot_y_title(op, "Position");			
       op->need_to_refresh = 1;
     }
  return refresh_plot(pr, pr->n_op - 1);    
}

int draw_hat_trajectories_params(void)
{
  int  j,  page_n, i_page;
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *ds = NULL;
  int nf;
  g_record *g_r = NULL;  
  
  ac_grep(cur_ac_reg,"%pr",&pr);
  g_r = find_g_record_in_pltreg(pr);   

  if(updating_menu_state != 0)	
    {  // we wait for recording finish
      if (g_r == NULL || working_gen_record == g_r) 
	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;	
      return D_O_K;
    }

  if (pr == NULL)	return OFF;
  if (g_r == NULL || g_r->n_record == 0) return OFF;
  nf = g_r->abs_pos;
  if (nf <= 0) return OFF;
  
  
  //we create one plot for each bead
  op = create_and_attach_one_plot(pr, nf, nf, 0);
  ds = op->dat[0];
  set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d  \n "
		"Zmag l = %d, w = %d, nim = %d\n"
		"objective %f, zoom factor %f, sample %s\n"
		,Pico_param.camera_param.camera_frequency_in_Hz,n_hat
		,track_info->cl,track_info->cw,nf
		,Pico_param.obj_param.objective_magnification
		,Pico_param.micro_param.zoom_factor
		,pico_sample);

  
  for (j = 0; j < nf; j++)
    {
      page_n = j/g_r->page_size;
      i_page = j%g_r->page_size;
      ds->xd[j] = g_r->imi[page_n][i_page];
      ds->yd[j] = g_r->zmag[page_n][i_page];
    }
      
  if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("I can't create plot !");
  set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d  \n "
		"magnets rotation l = %d, w = %d, nim = %d\n"
		"objective %f, zoom factor %f, sample %s\n"
		,Pico_param.camera_param.camera_frequency_in_Hz,n_hat
		,track_info->cl,track_info->cw,nf
		,Pico_param.obj_param.objective_magnification
		,Pico_param.micro_param.zoom_factor
		,pico_sample);

  for (j = 0; j < nf; j++)
    {
      page_n = j/g_r->page_size;
      i_page = j%g_r->page_size;
      ds->xd[j] = g_r->imi[page_n][i_page];
      ds->yd[j] = g_r->rot_mag[page_n][i_page];
    }
  if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("I can't create plot !");
  set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d \n "
		"Objective position l = %d, w = %d, nim = %d\n"
		"objective %f, zoom factor %f, sample %s\n"
		,Pico_param.camera_param.camera_frequency_in_Hz,n_hat
		,track_info->cl,track_info->cw,nf
		,Pico_param.obj_param.objective_magnification
		,Pico_param.micro_param.zoom_factor
		,pico_sample);


  for (j = 0; j < nf; j++)
    {
      page_n = j/g_r->page_size;
      i_page = j%g_r->page_size;
      ds->xd[j] = g_r->imi[page_n][i_page];
      ds->yd[j] = g_r->obj_pos[page_n][i_page];
    }
  create_attach_select_x_un_to_op(op, IS_SECOND, 0
				  ,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");
  
  
  set_op_filename(op, "ZmagRotFocushat%d.gr",g_r->n_rec);
  set_plot_title(op, "Hat %d zmag, rotation & focus ", g_r->n_rec);
  set_plot_x_title(op, "Time");
  set_plot_y_title(op, "Zmag, rot, focus");			
  
  
  op->need_to_refresh = 1;
  return refresh_plot(pr, pr->n_op - 1); 
}


int stop_hat(void)
{
  pltreg *pr = NULL;
  g_record *g_r = NULL;  
  
  ac_grep(cur_ac_reg,"%pr",&pr);
  g_r = (pr != NULL) ? find_g_record_in_pltreg(pr) : working_gen_record;   

  if(updating_menu_state != 0)	
    {
      if (g_r == NULL || working_gen_record != g_r) 
	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;	
      return D_O_K;
    }
  if (working_gen_record != NULL)
    {
      working_gen_record = NULL;
      snprintf(hat_state_disp,sizeof(hat_state_disp),"Hat curve %d stopped",n_hat);
      imr_TRACK_title_change_asked++;
	  if (current_write_open_path_picofile != NULL)
	    dump_to_html_log_file_with_date_and_time(current_write_open_picofile,"Hat recording stopped by user<br>");
      if (hat_r != NULL)
	{
	  hat_r->n_record++;
	}
    } 
  ask_to_update_menu();
  return D_O_K;  
}


int suspend_hat(void)
{
  pltreg *pr = NULL;
  g_record *g_r = NULL;  
  
  ac_grep(cur_ac_reg,"%pr",&pr);
  g_r = (pr != NULL) ? find_g_record_in_pltreg(pr) : working_gen_record;   

  if(updating_menu_state != 0)	
    {
      if (hat_state != HAT_RUNNING && hat_state != HAT_SUSPENDED)
	active_menu->flags |=  D_DISABLED;
      else if (working_gen_record != g_r) 
	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;	
      return D_O_K;
    }
  if (working_gen_record != NULL)
    {
      if (hat_state == HAT_RUNNING)
	{
	  hat_state = HAT_SUSPENDED;
	  active_menu->text = "Restart";
	  if (current_write_open_path_picofile != NULL)
	    dump_to_html_log_file_with_date_and_time(current_write_open_picofile,"Hat recording restarted<br>");
	}
      else if (hat_state == HAT_SUSPENDED)
	{
	  hat_state = HAT_RUNNING;
	  active_menu->text = "Suspend";
	  if (current_write_open_path_picofile != NULL)
	    dump_to_html_log_file_with_date_and_time(current_write_open_picofile,"Hat recording suspended<br>");
	}
    } 
  ask_to_update_menu();
  return D_O_K;  
}


int set_hat_fixed_bead(void)
{
  int  i,  nb, dt;
  pltreg *pr = NULL;
  g_record *g_r = NULL;  

  ac_grep(cur_ac_reg,"%pr",&pr);
  g_r = find_g_record_in_pltreg(pr);   

  if(updating_menu_state != 0)	
    {  // we wait for recording finish
      if (g_r == NULL)  	active_menu->flags |=  D_DISABLED; //  || working_gen_record == g_r
      else active_menu->flags &= ~D_DISABLED;	
      return D_O_K;
    }
  dt = do_hat_substract_fixed_bead;
  nb = hat_fixed_bead_nb;
  if (nb < 0) nb = g_r->n_bead;
  if (nb > g_r->n_bead) nb = g_r->n_bead;

  i = win_scanf("Do you want to perform fifferential tracking %b\n"
		"using a fixed bead, specify its number %d ",&dt,&nb);
  if (i == WIN_CANCEL) return 0;
  if (nb >= g_r->n_bead) nb = -1;
  hat_fixed_bead_nb = nb;
  do_hat_substract_fixed_bead = dt;
  if (do_hat_substract_fixed_bead > 0 && hat_fixed_bead_nb < 0)
    win_printf("Inproper differential tracking setting");
  return 0;
}



MENU *hat_menu(void)
{
	static MENU mn[32] = {0};

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn, "record hat", record_hat_record, NULL, 0, NULL);
	add_item_to_menu(mn, "stop hat", stop_hat, NULL, 0, NULL);
	add_item_to_menu(mn, "suspend hat", suspend_hat, NULL, 0, NULL);
	add_item_to_menu(mn,"\0",	NULL,	NULL,	0, NULL);

	return mn;
}

MENU *hat_plot_menu(void)
{
	static MENU mn[32] = {0};

	if (mn[0].text != NULL)	return mn;
	//	add_item_to_menu(mn, "record Z(t)", record_zdet, NULL, 0, NULL);
	add_item_to_menu(mn, "stop hat", stop_hat, NULL, 0, NULL);
	add_item_to_menu(mn, "suspend hat", suspend_hat, NULL, 0, NULL);


	add_item_to_menu(mn,"\0",	NULL,	NULL,	0, NULL);


	add_item_to_menu(mn, "Substract fixed bead", set_hat_fixed_bead, NULL, 0, NULL);
	add_item_to_menu(mn, "Dump XYZ", draw_hat_XYZ_trajectories, NULL, 0, NULL);
	add_item_to_menu(mn, "Dump Zmag Rot Focus", draw_hat_trajectories_params, NULL, 0, NULL);


	return mn;
}



# endif
