
#ifndef _FORCE_C_
#define _FORCE_C_

# include "allegro.h"
#ifdef XV_WIN32
    # include "winalleg.h"
#endif

# include "xvin.h"
# include "fftl32n.h"
# include "fillib.h"





/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "../cfg_file/Pico_cfg.h"
# include "../cardinal/cardinal.h"
# include "../wlc/wlc.h"


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "../fft2d/fftbtl32n.h"
# include "fillibbt.h"
# include "focus.h"
# include "magnetscontrol.h"
# include "trackBead.h"
# include "brown_util.h"
# include "track_util.h"
# include "force.h"
# include "scan_zmag.h"
# include "action.h"
# include "record.h"
int trajectories_op_idle_action(O_p *op, DIALOG *d);


g_record *force_r = NULL;
# define FORCE_RUNNING  1
# define FORCE_STOPPED  2
# define FORCE_SUSPENDED  4
int force_state = 0;

static force_param *sp = NULL;
char force_state_disp[4096];

//static float min_scale_zmag = 1;
static float min_scale_z = 0.5;

int keep_all_plots = 1;



int force_op_post_display(struct one_plot *op, DIALOG *d)
{
  register int i, j;
  BITMAP *imb = NULL;
  d_s *ds = NULL;
  //int na = 0, nb = 0, xc, yc;
  int  nb, xc, yc;
  //float f;

  if (d == NULL || d->dp == NULL || d->proc == NULL)    return 1;
  if (imr_and_pr) return 0;
  ds = op->dat[0];
  if (ds == NULL || ds->source == NULL)       return 0;

  //  if (sscanf(ds->source,"Bead tracking at %f Hz Acquistion %d for bead %d"
  //     ,&f,&na,&nb) != 3) return 0;
  nb = op->user_ispare[ISPARE_BEAD_NB];

  if (nb >= 0 && nb  < track_info->n_b)
    {
      imb = (BITMAP*)oi_TRACK->bmp.stuff;
      if (oi_TRACK->need_to_refresh & BITMAP_NEED_REFRESH)
	{
	  display_image_stuff_16M(imr_TRACK,d_TRACK);
	  oi_TRACK->need_to_refresh |= INTERNAL_BITMAP_NEED_REFRESH;
	  show_bead_cross(imb, imr_TRACK, d_TRACK);
	}
      for(i = nb - 1, j = 0; (i < nb + 2) && (j < track_info->n_b) && (j < 3); i++, j++)
	{
	  xc = track_info->bd[(i+track_info->n_b)%track_info->n_b]->x0 - 100;
	  yc = oi_TRACK->im.ny - track_info->bd[(i+track_info->n_b)%track_info->n_b]->y0 - 100;
	  acquire_bitmap(plt_buffer);
	  blit(imb,plt_buffer,xc,yc,SCREEN_W - 300, 60 + 256*j, 200, 200);
	  release_bitmap(plt_buffer);
	}
    }
  return 0;
}


int force_op_idle_action(O_p *op, DIALOG *d)
{
  register int i, j;
  BITMAP *imb = NULL;
  d_s *ds = NULL;
  //int na = 0, nb = 0,
  int nb, xc, yc;
  //float f;
  //char buf[512];

  if (d == NULL || d->dp == NULL || d->proc == NULL)    return 1;
  if (op->need_to_refresh)      d->proc(MSG_DRAW,d,0);
  if (imr_and_pr) return 0;
  ds = op->dat[0];
  if (ds == NULL || ds->source == NULL)       return 0;

  //if (sscanf(ds->source,"Bead tracking at %f Hz Acquistion %d for bead %d"
  //	     ,&f,&na,&nb) != 3) return 0;
  nb = op->user_ispare[ISPARE_BEAD_NB];

  if (nb >= 0 && nb  < track_info->n_b)
    {
      imb = (BITMAP*)oi_TRACK->bmp.stuff;
      if (oi_TRACK->need_to_refresh & BITMAP_NEED_REFRESH)
	{
	  display_image_stuff_16M(imr_TRACK,d_TRACK);
	  oi_TRACK->need_to_refresh |= INTERNAL_BITMAP_NEED_REFRESH;
	  show_bead_cross(imb, imr_TRACK, d_TRACK);
	}

      for(i = nb - 1, j = 0; (i < nb + 2) && (j < track_info->n_b) && (j < 3); i++, j++)
	{
	  xc = track_info->bd[(i+track_info->n_b)%track_info->n_b]->x0 - 100;
	  yc = oi_TRACK->im.ny - track_info->bd[(i+track_info->n_b)%track_info->n_b]->y0 - 100;

	  for(;screen_acquired;); // we wait for screen_acquired back to 0;
	  screen_acquired = 1;
	  acquire_bitmap(screen);
	  blit(imb,screen,xc,yc,SCREEN_W - 300, 60 + 256*j, 200, 200);
	  release_bitmap(screen);
	  screen_acquired = 0;
	  oi_TRACK->need_to_refresh &= ~INTERNAL_BITMAP_NEED_REFRESH;
	}
    }
  return 0;
}


int force_op_idle_action_old(O_p *op, DIALOG *d)
{
  if (d == NULL || d->dp == NULL || d->proc == NULL)    return 1;
  if (op->need_to_refresh)      d->proc(MSG_DRAW,d,0);
  return 0;
}

# ifdef OLD

int force_job_xyz(int im, struct future_job *job)
{
  register int j, k;
  int ci, nf1, istep;
  b_track *bt = NULL;
  d_s *dsx = NULL, *dsy = NULL, *dsz = NULL, *ds = NULL, *dsm = NULL;
  O_p *op = NULL;
  force_param *sp = NULL;
  char name[256] = {0};
  float ax, dx, ay, dy, y_over_x, y_over_z;
  un_s *un = NULL;


  if (job == NULL || job->oi == NULL || job->in_progress == 0 || job->op->n_dat < 3) return 0;
  if (im < job->imi)  return 0;

  ci = track_info->c_i;
  sp = (force_param *)job->more_data;
  dsx = job->op->dat[0];
  dsy = job->op->dat[1];
  dsz = job->op->dat[2];
  dsm = job->op->dat[3];
  bt = track_info->bd[job->bead_nb];
  istep = sp->istep;

  un = job->oi->xu[job->oi->c_xu];
  get_afine_param_from_unit(un, &ax, &dx);
  un = job->oi->yu[job->oi->c_yu];
  get_afine_param_from_unit(un, &ay, &dy);
  y_over_x = (dy != 0) ? dx/dy : 1;
  y_over_z = track_info->focus_cor * 1e-6;
  y_over_z = (dy != 0) ? y_over_z/dy : 1;



  for (k = 0; (track_info->imi[ci] > job->imi) && (k < TRACKING_BUFFER_SIZE); k++)
    {
      ci--;
      if (ci < 0) ci += TRACKING_BUFFER_SIZE;
    }


  for (k = 0; (track_info->imi[ci] >= (job->imi-32)) && (k < TRACKING_BUFFER_SIZE); k++)
    {
      j = track_info->imi[ci] - job->in_progress;
      if (j < dsx->mx)
	{
	  dsx->yd[j] = y_over_x * bt->x[ci];
	  dsy->yd[j] = bt->y[ci];
	  dsz->yd[j] = y_over_z * bt->z[ci];
	  dsm->yd[j] = track_info->zmag[ci];
	  dsx->xd[j] = dsy->xd[j] = dsz->xd[j] = dsm->xd[j] = j;
	}
      ci--;
      if (ci < 0) ci += TRACKING_BUFFER_SIZE;
    }
  j = job->imi - job->in_progress;
  if (j <= dsx->mx)
    dsx->nx = dsx->ny = dsy->nx = dsy->ny = dsz->nx = dsz->ny = dsm->nx = dsm->ny = j;
  if (j >= dsx->mx)
        dsx->nx = dsx->ny = dsy->nx = dsy->ny = dsz->nx = dsz->ny = dsm->nx = dsm->ny = dsx->mx;
  job->op->need_to_refresh = 1;
  if (dsx->nx < dsx->mx) job->imi += 32;
  else
    {
      istep++;
      bt->opt = job->op;
      if (istep > sp->nstep) job->in_progress = 0;
      else if ((sp->im[istep+1] - sp->im[istep] - sp->dead) > 0)
	{
	  snprintf(name,256,"Force curve %d bead %d step %d",n_force,job->bead_nb, istep);
	  nf1 =  sp->im[istep+1] - sp->im[istep] - sp->dead;
	  op = create_and_attach_one_plot(job->pr, nf1, nf1, 0);
	  op->data_changing = 1;
	  ds = op->dat[0];
	  ds->nx = ds->ny = 0;
	  set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
			"X coordinate l = %d, w = %d, nim = %d\n"
			"objective %f, zoom factor %f, sample %s Rotation %g\n"
			"Z magnets %g mm\n"
			"Calibration from %s"
			,Pico_param.camera_param.camera_frequency_in_Hz,n_force,job->bead_nb
			,track_info->cl,track_info->cw,nf1
			,Pico_param.obj_param.objective_magnification
			,Pico_param.micro_param.zoom_factor
			,pico_sample
			,sp->rotation
			,sp->zm[istep]
			,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");
	  if ((ds = create_and_attach_one_ds(op, nf1, nf1, 0)) == NULL)
	    return win_printf_OK("I can't create plot !");
	  ds->nx = ds->ny = 0;
	  set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
			"Y coordinate l = %d, w = %d, nim = %d\n"
			"objective %f, zoom factor %f, sample %s Rotation %g\n"
			"Z magnets %g mm\n"
			"Calibration from %s"
			,Pico_param.camera_param.camera_frequency_in_Hz,n_force,job->bead_nb
			,track_info->cl,track_info->cw,nf1
			,Pico_param.obj_param.objective_magnification
			,Pico_param.micro_param.zoom_factor
			,pico_sample
			,sp->rotation
			,sp->zm[istep]
			,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");
	  if ((ds = create_and_attach_one_ds(op, nf1, nf1, 0)) == NULL)
	    return win_printf_OK("I can't create plot !");
	  ds->nx = ds->ny = 0;
	  set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
			"Z coordinate l = %d, w = %d, nim = %d\n"
			"objective %f, zoom factor %f, sample %s Rotation %g\n"
			"Z magnets %g mm\n"
			"Calibration from %s"
			,Pico_param.camera_param.camera_frequency_in_Hz,n_force,job->bead_nb
			,track_info->cl,track_info->cw,nf1
			,Pico_param.obj_param.objective_magnification
			,Pico_param.micro_param.zoom_factor
			,pico_sample
			,sp->rotation
			,sp->zm[istep]
			,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");

	  uns_op_2_op(op, job->op);
	  op->op_idle_action = force_op_idle_action; //xyz_scan_idle_action;
	  op->op_post_display = force_op_post_display;
	  set_op_filename(op, "X(t)Y(t)Z(t)bd%dforce%d-%d.gr",job->bead_nb,n_force,istep);
	  set_plot_title(op, "Bead %d trajectory %d-%d Zmag %g", job->bead_nb,n_force,istep,sp->zm[istep]);
	  set_plot_x_title(op, "Time");
	  set_plot_y_title(op, "Position");
	  op->user_ispare[ISPARE_BEAD_NB] = job->bead_nb;

	  if ((ds = create_and_attach_one_ds(op, nf1, nf1, 0)) == NULL)
	    return win_printf_OK("I can't create plot !");
	  ds->nx = ds->ny = 0;

	  op->x_lo = (-nf1/64);
	  op->x_hi = (nf1 + nf1/64);
	  set_plot_x_fixed_range(op);
	  op->data_changing = 0;
	  job->op = op;
	  job->imi = sp->im[istep] + sp->dead + 32;
	  job->in_progress = sp->im[istep] + sp->dead;
	  if (job->bead_nb == track_info->n_b - 1) sp->istep = istep;
	}
      else
	{
	  win_printf("istep %d, im[istep+1] %d,\n im[istep] %d, dead %d", istep, sp->im[istep+1], sp->im[istep], sp->dead);
	}
    }
  return 0;
}


int force_job_scankx(int im, struct future_job *job)
{
  register int i;
  int  ci;
  b_track *bt = NULL;
  d_s *dsx = NULL, *dsy = NULL, *dsz = NULL, *dsfx = NULL, *dsfy = NULL;
  force_param *sp = NULL;
  double mx = 0, my = 0, mz = 0, sx = 0, sy = 0, sz = 0, tmp;
  float ay, dy;
  un_s *un = NULL;

  if (job == NULL || job->oi == NULL || job->in_progress == 0 || job->more_data == NULL) return 0;
  if (im < job->imi)  return 0;

  ci = track_info->c_i;
  dsfx = job->op->dat[0];
  dsfy = job->op->dat[1];
  sp = (force_param *)job->more_data;
  bt = track_info->bd[job->bead_nb];

  if (bt->opt == NULL)   return 0;
  dsx = bt->opt->dat[0];
  dsy = bt->opt->dat[1];
  dsz = bt->opt->dat[2];
  un = bt->opt->yu[bt->opt->c_yu];
  get_afine_param_from_unit(un, &ay, &dy);



  for (i = 0; i < dsx->nx; i++)
    {
      mx += dsx->yd[i];
      my += dsy->yd[i];
      mz += dsz->yd[i];
    }
  if (dsx->nx > 0)
    {
      mx /= dsx->nx;
      my /= dsx->nx;
      mz /= dsx->nx;
    }
  for (i = 0; i < dsx->nx; i++)
    {
      tmp = dsx->yd[i] - mx;
      tmp *= tmp;
      sx += tmp;
      tmp = dsy->yd[i] - my;
      tmp *= tmp;
      sy += tmp;
      tmp = dsz->yd[i] - mz;
      tmp *= tmp;
      sz += tmp;
    }
  if ((dsx->nx - 1) > 0)
    {
      sx /= (dsx->nx - 1);
      sy /= (dsx->nx - 1);
      sz /= (dsx->nx - 1);
    }

  dy *= 1e6;

  bt->mzm = dsfx->xd[dsfx->nx] = dsfy->xd[dsfx->nx] = dy * mz;
  bt->mxm = dy * mx;
  bt->mym = dy * my;

  dsfx->yd[dsfx->nx] = dy * sqrt(sx);
  bt->sx2 = dy * dy * sx;

  dsfx->yd[dsfx->nx] = (1.38e-11 * 298)/bt->sx2;
  dsfx->yd[dsfx->nx] *= bt->mzm * 1e6;

  dsfy->yd[dsfx->nx] = dy * sqrt(sy);
  bt->sy2 = dy * dy * sy;

  dsfy->yd[dsfx->nx] = (1.38e-11 * 298)/bt->sy2;
  dsfy->yd[dsfx->nx] *= bt->mzm * 1e6;

  if (dsfx->nx < dsfx->mx)
    dsfx->nx = dsfx->ny = dsfy->nx = dsfy->ny = dsfx->nx + 1;

  for (i = 2; i <= dsx->nx; i *= 2);
  i /= 2;
  /*  pb using fft ....
  if (i == dsx->nx)
    {
      alternate_lorentian_fit_acq_spe_with_error(dsx->yd, dsx->ny, dy, 10, dsx->ny/2,
					     1, 1, 0.97, 0, &(bt->fcx), &(bt->ax),
						 &(bt->dfcx), &(bt->dax), &(bt->chisqx), &(bt->dmxm));
      alternate_lorentian_fit_acq_spe_with_error(dsy->yd, dsy->ny, dy, 10, dsy->ny/2,
					     1, 1, 0.97, 0, &(bt->fcy), &(bt->ay),
						 &(bt->dfcy), &(bt->day), &(bt->chisqy), &(bt->dmym));
      alternate_lorentian_fit_acq_spe_with_error(dsz->yd, dsz->ny, dy, 10, dsz->ny/2,
					     1, 1, 0.97, 0, &(bt->fcz), &(bt->az),
						 &(bt->dfcz), &(bt->daz), &(bt->chisqz), &(bt->dmzm));
    }
  */







  job->op->need_to_refresh = 1;
  if (dsfx->nx < dsfx->mx) job->imi = sp->im[dsfx->nx+1];
  else
    {
      job->in_progress = 0;
    }
  return 0;
}

int force_job_scanz(int im, struct future_job *job)
{
  int  ci;
  b_track *bt = NULL;
  d_s *dsz = NULL;
  force_param *sp = NULL;

  if (job == NULL || job->oi == NULL || job->in_progress == 0 || job->more_data == NULL) return 0;
  if (im < job->imi)  return 0;

  ci = track_info->c_i;
  dsz = job->op->dat[0];
  sp = (force_param *)job->more_data;
  bt = track_info->bd[job->bead_nb];
  if (bt->opt == NULL)   return 0;


  dsz->xd[dsz->nx] = bt->mzm;
  dsz->yd[dsz->nx] = sp->zm[dsz->nx];

  if (dsz->nx < dsz->mx)
    dsz->nx = dsz->ny = dsz->nx + 1;

  job->op->need_to_refresh = 1;
  if (dsz->nx < dsz->mx) job->imi = sp->im[dsz->nx+1] + 1;
  else
    {
      job->in_progress = 0;
      n_force++;
      Open_Pico_config_file();
      set_config_int("Force_curve", "n_force", n_force);
      write_Pico_config_file();
      Close_Pico_config_file();

    }
  return 0;
}




int record_force_curve(void)
{
   int i, j, k, im, nf1;
   imreg *imr = NULL;
   O_i *oi = NULL;
   b_track *bt = NULL;
   pltreg *pr = NULL;
   O_p *op = NULL;
   d_s *ds = NULL;
   static int nf = 2048, nstep = 8,  dead = 10, mper = 2048, maxnper = 8192;
   static float fbr = ((float)10)/4096, fdr = 0.5, lambdazmag = 2, zmag0 = 5;
   static float zmag_start = 9, zmag_step = -.5, rot_start, zmag,  zmag_finish = 8;
   char question[1024] = {0}, name[256] = {0};
   int n_maxnper;
   force_param *sp = NULL;

   if(updating_menu_state != 0)
     {
       if (track_info->n_b == 0)
	 active_menu->flags |=  D_DISABLED;
       else
	 {
           if (track_info->SDI_mode == 0)
              {
                  for (i = 0, k = 0; i < track_info->n_b; i++)
                     {
                         bt = track_info->bd[i];
                         k += (g_r->SDI_mode || bt->calib_im != NULL) ? 1 : 0;
                     }
              }
           else k = 1;
	   if (k) active_menu->flags &= ~D_DISABLED;
	   else active_menu->flags |=  D_DISABLED;
	 }
       return D_O_K;
     }
   if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)	return OFF;


   Open_Pico_config_file();
   n_force = get_config_int("Force_curve", "n_force", n_force);
   zmag_start = get_config_float("Force_curve", "zstart", 9);
   zmag_step = get_config_float("Force_curve", "zstep", 0.125);
   nstep = get_config_int("Force_curve", "n_step", 9);
   mper = get_config_int("Force_curve", "nper", 1024);
   maxnper = get_config_int("Force_curve", "maxnper", 32768);
   dead = get_config_int("Force_curve", "dead", 32);
   zmag0 = get_config_float("Force_curve", "zmag0", 7.5);
   lambdazmag = get_config_float("Force_curve", "lambdazmag", 3);
   //z_cor = get_config_float("Force_curve", "correction", 0.878);
   zmag_finish = get_config_float("Force_curve", "zmag_after", 8);
   Close_Pico_config_file();


   rot_start = prev_mag_rot;
   zmag = prev_zmag;

   //  rot_start = what_is_present_rot_value();
   //zmag = read_magnet_z_value();
   snprintf(question,1024,"{\\pt12 Starting a new Force curve recording}\n"
	   "This function will create two new projets per active bead, one containing force\n"
	   "curves and related plots, the second containing the x,y,z trajectories for each force\n"
	   "Z_{mag} present value is %g rotation position is %g\n"
	   "Starting zmag %%6f with step of %%6f nb of step %%6d\n"
	   "Rotation position for curve %%8f\n"
	   "number of frames for recording at high force  %%8d\n"
	   "Z_{mag0} (value below which integration time increases) %%8f\n"
	   "\\tau  = mper * 2^{(\\lambda Z_{mag0} - Z_{mag})}, \\lambda %%8f\n"
	   "Maximun number of frames to record bead position at each step  %%8d\n"
	   "Number of frames skipped after each force change %%8d\n"
	   "zmag to stand when acquisition(s) are finished %%8f\n"
	   ,zmag,rot_start);

   i = win_scanf(question,&zmag_start,&zmag_step,&nstep,&rot_start,&mper,&zmag0,&lambdazmag,&maxnper,&dead,&zmag_finish);



   if (i == WIN_CANCEL)	return OFF;


   Open_Pico_config_file();
   set_config_float("Force_curve", "zstart", zmag_start);
   set_config_float("Force_curve", "zstep", zmag_step);
   set_config_int("Force_curve", "n_step", nstep);
   set_config_int("Force_curve", "nper", mper);
   set_config_int("Force_curve", "maxnper", maxnper);
   set_config_int("Force_curve", "dead", dead);
   set_config_float("Force_curve", "zmag0", zmag0);
   set_config_float("Force_curve", "lambdazmag", lambdazmag);
   //set_config_float("Force_curve", "correction", z_cor);
   set_config_float("Force_curve", "zmag_after", zmag_finish);
   write_Pico_config_file();
   Close_Pico_config_file();

   for (n_maxnper = 1; (2<<n_maxnper) < maxnper; n_maxnper++);

   //  win_printf("1");
  my_set_window_title("force 1");
   sp = (force_param*)calloc(1,sizeof(force_param));
   if (sp == NULL)  win_printf_OK("Could no allocte force parameters!");
   sp->im = (int*)calloc(nstep+2,sizeof(int));
   sp->ro = (float*)calloc(nstep+2,sizeof(float));
   sp->zm = (float*)calloc(nstep+2,sizeof(float));
   sp->fm = (float*)calloc(nstep+2,sizeof(float));
   if (sp->im == NULL || sp->ro == NULL || sp->zm == NULL || sp->fm == NULL)
     win_printf_OK("Could no allocte force parameters!");

  sp->nstep = nstep;
  sp->istep = 0;
  sp->dead = dead;
  sp->zmag_start = zmag_start;
  sp->zmag_step = zmag_step;
  sp->rot_start = rot_start;
  sp->fbr = fbr;
  sp->fdr = fdr;
  sp->accuracy =  10;

  //win_printf("1a");
  im = track_info->imi[track_info->c_i];

  //  win_printf("1b");
      /*
  k = fill_next_available_action(im+4, MV_ZMAG_ABS, sp->zmag_start);
  if (k < 0)	win_printf_OK("Could not add pending action!");

  im += 64;

  k = fill_next_available_action(im, MV_ROT_ABS, sp->rot_start);
  if (k < 0)	win_printf_OK("Could not add pending action!");
      */
  im += 64;
  //win_printf("1c");
 my_set_window_title("force 2");
  // win_printf("2");
  for (i = 0, nf = 0; i <= nstep ; i++)
    {
      sp->ro[i] = rot_start;
      sp->zm[i] = zmag_start + (i * zmag_step);
      sp->fm[i] = zmag_to_force(0, sp->zm[i]);
      sp->im[i] = im + nf;

      k = (int)(lambdazmag * (zmag0 - zmag_start - i * zmag_step));
      k = (k <= n_maxnper) ? k : n_maxnper;
      k = (k > 0) ? k*mper : mper;

      for (j = 2; j < k; j *= 2);
      //win_printf(" zmag %f nf %d",sp->zm[i],j);
      nf += j;
      nf += sp->dead;
    }
  sp->ro[i] = rot_start;
  sp->zm[i] = zmag_start + (nstep * zmag_step);
  sp->im[i] = im + nf;

  //win_printf("i-1 %d n %d, i %d n %d",i-1,sp->im[i-1],i,sp->im[i]);

  /my_set_window_title("force 3");
  //#ifdef BUG
   for (i = 0; i < track_info->n_b; i++)
     {
       bt = track_info->bd[i];
       if ((g_r->SDI_mode == 0 && bt->calib_im == NULL) || (bt->not_lost <= 0) || (bt->in_image == 0))
	 continue;
       snprintf(name,256,"Force recordings %d bead %d",n_force,i);
       nf1 =  sp->im[1] - im - sp->dead;
       pr = create_hidden_pltreg_with_op(&op, nf1, nf1, 0,name);
       if (pr == NULL)  win_printf_OK("Could not find or allocte plot region!");
       op = pr->one_p;
       ds = op->dat[0];
       ds->nx = ds->ny = 0;
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
		     "X coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_force,i
		     ,track_info->cl,track_info->cw,nf1
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,pico_sample
		     ,what_is_present_rot_value()
		     ,what_is_present_z_mag_value()
		     ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");


       if ((ds = create_and_attach_one_ds(op, nf1, nf1, 0)) == NULL)
	 return win_printf_OK("I can't create plot !");
       ds->nx = ds->ny = 0;
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
		     "Y coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_force,i
		     ,track_info->cl,track_info->cw,nf1
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,pico_sample
		     ,what_is_present_rot_value()
		     ,what_is_present_z_mag_value()
		     ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");
       if ((ds = create_and_attach_one_ds(op, nf1, nf1, 0)) == NULL)
	 return win_printf_OK("I can't create plot !");
       ds->nx = ds->ny = 0;
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
		     "Z coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_force,i
		     ,track_info->cl,track_info->cw,nf1
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,pico_sample
		     ,what_is_present_rot_value()
		     ,what_is_present_z_mag_value()
		     ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");

       uns_oi_2_op(oi, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
       create_attach_select_x_un_to_op(op, IS_SECOND, 0
				       ,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");

       op->op_idle_action = force_op_idle_action; //xyz_scan_idle_action;
       op->op_post_display = force_op_post_display;
       set_op_filename(op, "X(t)Y(t)Z(t)bd%dforce%d-0.gr",i,n_force,what_is_present_z_mag_value());
       set_plot_title(op, "Bead %d trajectory %d-0", i,n_force);
       set_plot_x_title(op, "Time");
       set_plot_y_title(op, "Position");
       op->user_ispare[ISPARE_BEAD_NB] = i;

       op->x_lo = (-nf1/64);
       op->x_hi = (nf1 + nf1/64);
       set_plot_x_fixed_range(op);

       if ((ds = create_and_attach_one_ds(op, nf1, nf1, 0)) == NULL)
	 return win_printf_OK("I can't create plot !");
       ds->nx = ds->ny = 0;

       fill_next_available_job_spot(im + sp->dead, im+sp->dead+32, COLLECT_DATA, i, imr, oi, pr, op, (void*)sp, force_job_xyz, 0);

       // We create a plot region to display bead position, force  etc.
       snprintf(name,256,"Force curve %d bead %d",n_force,i);
       pr = create_hidden_pltreg_with_op(&op, nstep+1, nstep+1, 0,name);
       if (pr == NULL)  win_printf_OK("Could not find or allocte plot region!");
       if (bt->fixed_bead == 0)
	 {
	   set_plot_title(op, "Beads %d Force curve %d, rot %f Zmag", i, n_force,sp->ro[0]);
	   set_plot_x_title(op,"Length (\\mu m)");
	   set_plot_y_title(op,"Expected Force (pN)");
	   set_plot_file(op,"Forcebd%01d%03d%s",i,n_force,".gr");
	   ds = op->dat[0];
	   ds->nx = ds->ny = 0;
	   set_plot_symb(ds, "\\pt5\\oc");
	   set_dot_line(ds);
	   set_ds_source(ds,"DNA force extension curve f_x 3D length bead %d\n",i);
	   ds = create_and_attach_one_ds(op, nstep+1, nstep+1, 0);
	   if (ds == NULL)  win_printf_OK("cannot allocate ds!");
	   ds->nx = ds->ny = 0;
	   set_plot_symb(ds, "\\pt5\\oc");
	   set_dot_line(ds);
	   set_ds_source(ds,"DNA force extension curve f_y 3D length bead %d\n",i);
	   op->op_idle_action = force_op_idle_action;
	   op->op_post_display = force_op_post_display;
	   op->user_ispare[ISPARE_BEAD_NB] = i;
	   fill_next_available_job_spot(1, sp->im[1], COLLECT_DATA, i, imr, oi, pr, op, (void*)sp, force_job_scankx,0);



	   op = create_and_attach_one_plot(pr, nstep + 1,  nstep + 1, 0);
	 }
       ds = op->dat[0];
       ds->nx = ds->ny = 0;
       //set_dot_line(ds);
       set_plot_symb(ds, "\\pt5\\oc");
       set_plot_title(op, "Beads %d Force %d, rot %f", i, n_force,sp->ro[0]);
       set_plot_x_title(op,"Length (\\mu m)");
       set_plot_y_title(op,"Z_{mag} (mm)");
       set_plot_file(op,"scanzbd%01d%03d%s",i,n_force,".gr");

       /*
       set_ds_source(ds,"Scan Z\nZ mag start = %g for %d frames scan by steps of %g"
		     " for %d steps \nrot = %g dead period %d\n"
		     "Bead  calbration %s \n"
		     ,zstart,nper,zstep,nstep,what_is_present_rot_value()
		     ,dead,bt->calib_im->filename);
       */
       op->op_idle_action = force_op_idle_action;
       op->op_post_display = force_op_post_display;
       fill_next_available_job_spot(1, sp->im[1]+1, COLLECT_DATA, i, imr, oi, pr, op, (void*)sp, force_job_scanz,0);
       op->user_ispare[ISPARE_BEAD_NB] = i;



       //       op = create_and_attach_one_plot(pr, 2*nstep,  2*nstep, 0);
     }
   //#endif
   //win_printf("3");
 my_set_window_title("force 4");

   for (i = 0, nf = 0; i <= nstep ; i++)
     {
       k = fill_next_available_action(sp->im[i], MV_ZMAG_ABS, sp->zm[i]);
       if (k < 0)	win_printf_OK("Could not add pending action!");
     }
   //win_title_used = 1;
   // win_printf("4");
 my_set_window_title("force 4");
   return D_O_K;

}

int record_auto_force_curve(void)
{
   int i, j, k, im, nf1;
   imreg *imr = NULL;
   O_i *oi = NULL;
   b_track *bt = NULL;
   pltreg *pr = NULL;
   O_p *op = NULL;
   d_s *ds = NULL;
   static int nf = 2048, nstep = 8,  dead = 10;
   static float fbr = ((float)10)/4096, fdr = 0.5, accuracy = 10;
   static float zmag_start = 9, zmag_step = -.5, rot_start, zmag,  zmag_finish = 8;
   char question[1024] = {0}, name[256] = {0};
   force_param *sp = NULL;

   if(updating_menu_state != 0)
     {
       if (track_info->n_b == 0)
	 active_menu->flags |=  D_DISABLED;
       else
	 {
           if (track_info->SDI_mode == 0)
              {
                  for (i = 0, k = 0; i < track_info->n_b; i++)
                     {
                         bt = track_info->bd[i];
                         k += (g_r->SDI_mode || bt->calib_im != NULL) ? 1 : 0;
                     }
              }
           else k = 1;
	   if (k) active_menu->flags &= ~D_DISABLED;
	   else active_menu->flags |=  D_DISABLED;
	 }
       return D_O_K;
     }
   if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)	return OFF;

   Open_Pico_config_file();
   n_force = get_config_int("Force_curve", "n_force", n_force);
   zmag_start = get_config_float("Force_curve", "zstart", 9);
   zmag_step = get_config_float("Force_curve", "zstep", 0.125);
   nstep = get_config_int("Force_curve", "n_step", 9);
   dead = get_config_int("Force_curve", "dead", 32);
   accuracy = get_config_float("Force_curve", "accuracy", 10);
   //z_cor = get_config_float("Force_curve", "correction", 0.878);
   zmag_finish = get_config_float("Force_curve", "zmag_after", 8);
   Close_Pico_config_file();


   rot_start = prev_mag_rot;
   zmag = prev_zmag;

   //   rot_start = what_is_present_rot_value();
   //zmag = read_magnet_z_value();
   snprintf(question,1024,"{\\pt12 Starting a new Force curve recording}\n"
	   "This function will create two new projets per active bead, one containing force\n"
	   "curves and related plots, the second containing the x,y,z trajectories for each force\n"
	   "Z_{mag} present value is %g rotation position is %g\n"
	   "Starting zmag %%6f with step of %%6f nb of step %%6d\n"
	   "Rotation position for curve %%8f\n"
	   "typical accuracy %%8f(%%%%)\n"
	   "Number of frames skipped after each force change %%8d\n"
	   "zmag to stand when acquisition(s) are finished %%8f\n"
	   ,zmag,rot_start);

   i = win_scanf(question,&zmag_start,&zmag_step,&nstep,&rot_start,&accuracy,&dead,&zmag_finish);

   if (i == WIN_CANCEL)	return OFF;

   Open_Pico_config_file();
   set_config_float("Force_curve", "zstart", zmag_start);
   set_config_float("Force_curve", "zstep", zmag_step);
   set_config_int("Force_curve", "n_step", nstep);
   set_config_int("Force_curve", "dead", dead);
   set_config_float("Force_curve", "accuracy", accuracy);
   //set_config_float("Force_curve", "correction", z_cor);
   set_config_float("Force_curve", "zmag_after", zmag_finish);
   write_Pico_config_file();
   Close_Pico_config_file();

   //  win_printf("1");
  my_set_window_title("force 1");
   sp = (force_param*)calloc(1,sizeof(force_param));
   if (sp == NULL)  win_printf_OK("Could no allocte force parameters!");
   sp->im = (int*)calloc(nstep+2,sizeof(int));
   sp->ro = (float*)calloc(nstep+2,sizeof(float));
   sp->zm = (float*)calloc(nstep+2,sizeof(float));
   sp->fm = (float*)calloc(nstep+2,sizeof(float));
   if (sp->im == NULL || sp->ro == NULL || sp->zm == NULL || sp->fm == NULL)
     win_printf_OK("Could no allocte force parameters!");

  sp->nstep = nstep;
  sp->istep = 0;
  sp->dead = dead;
  sp->zmag_start = zmag_start;
  sp->zmag_step = zmag_step;
  sp->rot_start = rot_start;
  sp->fbr = fbr;
  sp->fdr = fdr;
  sp->accuracy = (accuracy > 0) ? accuracy : 10;

  //win_printf("1a");
  im = track_info->imi[track_info->c_i];

  //  win_printf("1b");
      /*
  k = fill_next_available_action(im+4, MV_ZMAG_ABS, sp->zmag_start);
  if (k < 0)	win_printf_OK("Could not add pending action!");

  im += 64;

  k = fill_next_available_action(im, MV_ROT_ABS, sp->rot_start);
  if (k < 0)	win_printf_OK("Could not add pending action!");
      */
  im += 64;
  //win_printf("1c");
 my_set_window_title("force 2");
  // win_printf("2");
  for (i = 0, nf = 0; i <= nstep ; i++)
    {
      sp->ro[i] = rot_start;
      sp->zm[i] = zmag_start + (i * zmag_step);
      sp->fm[i] = zmag_to_force(0, sp->zm[i]);
      sp->im[i] = im + nf;
      k = (int)(((2*M_PI*force_to_tau_x_in_fr(sp->fm[i], 0))*10000/(sp->accuracy*sp->accuracy))+0.5);
      for (j = 2; j < k; j *= 2);
      //win_printf(" zmag %f nf %d",sp->zm[i],j);
      nf += j;
      nf += sp->dead;
    }
  sp->ro[i] = rot_start;
  sp->zm[i] = zmag_start + (nstep * zmag_step);
  sp->im[i] = im + nf;

  //win_printf("i-1 %d n %d, i %d n %d",i-1,sp->im[i-1],i,sp->im[i]);

  //my_set_window_title("force 3");
  //#ifdef BUG
   for (i = 0; i < track_info->n_b; i++)
     {
       bt = track_info->bd[i];
       if ((g_r->SDI_mode && bt->calib_im == NULL) || (bt->not_lost <= 0) || (bt->in_image == 0))
	 continue;
       snprintf(name,256,"Force recordings %d bead %d",n_force,i);
       nf1 =  sp->im[1] - im - sp->dead;
       pr = create_hidden_pltreg_with_op(&op, nf1, nf1, 0,name);
       if (pr == NULL)  win_printf_OK("Could not find or allocte plot region!");
       op = pr->one_p;
       ds = op->dat[0];
       ds->nx = ds->ny = 0;
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
		     "X coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_force,i
		     ,track_info->cl,track_info->cw,nf1
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,pico_sample
		     ,rot_start   //what_is_present_rot_value()
		     ,zmag_start //what_is_present_z_mag_value()
		     ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");


       if ((ds = create_and_attach_one_ds(op, nf1, nf1, 0)) == NULL)
	 return win_printf_OK("I can't create plot !");
       ds->nx = ds->ny = 0;
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
		     "Y coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_force,i
		     ,track_info->cl,track_info->cw,nf1
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,pico_sample
		     ,rot_start   //what_is_present_rot_value()
		     ,zmag_start //what_is_present_z_mag_value()
		     ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");
       if ((ds = create_and_attach_one_ds(op, nf1, nf1, 0)) == NULL)
	 return win_printf_OK("I can't create plot !");
       ds->nx = ds->ny = 0;
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
		     "Z coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_force,i
		     ,track_info->cl,track_info->cw,nf1
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,pico_sample
		     ,rot_start   //what_is_present_rot_value()
		     ,zmag_start //what_is_present_z_mag_value()
		     ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");

       uns_oi_2_op(oi, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
       create_attach_select_x_un_to_op(op, IS_SECOND, 0
				       ,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");

       op->op_idle_action = force_op_idle_action; //xyz_scan_idle_action;
       op->op_post_display = force_op_post_display;
       set_op_filename(op, "X(t)Y(t)Z(t)bd%dforce%d-0.gr",i,n_force,what_is_present_z_mag_value());
       set_plot_title(op, "Bead %d trajectory %d-0", i,n_force);
       set_plot_x_title(op, "Time");
       set_plot_y_title(op, "Position");

       op->x_lo = (-nf1/64);
       op->x_hi = (nf1 + nf1/64);
       set_plot_x_fixed_range(op);
       op->user_ispare[ISPARE_BEAD_NB] = i;
       if ((ds = create_and_attach_one_ds(op, nf1, nf1, 0)) == NULL)
	 return win_printf_OK("I can't create plot !");
       ds->nx = ds->ny = 0;

       fill_next_available_job_spot(im + sp->dead, im+sp->dead+32, COLLECT_DATA, i, imr, oi, pr, op, (void*)sp, force_job_xyz, 0);

       // We create a plot region to display bead position, force  etc.
       snprintf(name,256,"Force curve %d bead %d",n_force,i);
       pr = create_hidden_pltreg_with_op(&op, nstep+1, nstep+1, 0,name);
       if (pr == NULL)  win_printf_OK("Could not find or allocte plot region!");
       if (bt->fixed_bead == 0)
	 {
	   set_plot_title(op, "Beads %d Force curve %d, rot %f Zmag", i, n_force,sp->ro[0]);
	   set_plot_x_title(op,"Length (\\mu m)");
	   set_plot_y_title(op,"Expected Force (pN)");
	   set_plot_file(op,"Forcebd%01d%03d%s",i,n_force,".gr");
	   ds = op->dat[0];
	   ds->nx = ds->ny = 0;
	   set_plot_symb(ds, "\\pt5\\oc");
	   set_dot_line(ds);
	   set_ds_source(ds,"DNA force extension curve f_x 3D length bead %d\n",i);
	   ds = create_and_attach_one_ds(op, nstep+1, nstep+1, 0);
	   if (ds == NULL)  win_printf_OK("cannot allocate ds!");
	   ds->nx = ds->ny = 0;
	   set_plot_symb(ds, "\\pt5\\oc");
	   set_dot_line(ds);
	   set_ds_source(ds,"DNA force extension curve f_y 3D length bead %d\n",i);
	   op->op_idle_action = force_op_idle_action;
	   op->op_post_display = force_op_post_display;
	   fill_next_available_job_spot(1, sp->im[1], COLLECT_DATA, i, imr, oi, pr, op, (void*)sp, force_job_scankx,0);


	   op->user_ispare[ISPARE_BEAD_NB] = i;
	   op = create_and_attach_one_plot(pr, nstep + 1,  nstep + 1, 0);
	 }
       ds = op->dat[0];
       ds->nx = ds->ny = 0;
       //set_dot_line(ds);
       set_plot_symb(ds, "\\pt5\\oc");
       set_plot_title(op, "Beads %d Force %d, rot %f", i, n_force,sp->ro[0]);
       set_plot_x_title(op,"Length (\\mu m)");
       set_plot_y_title(op,"Z_{mag} (mm)");
       set_plot_file(op,"scanzbd%01d%03d%s",i,n_force,".gr");

       /*
       set_ds_source(ds,"Scan Z\nZ mag start = %g for %d frames scan by steps of %g"
		     " for %d steps \nrot = %g dead period %d\n"
		     "Bead  calbration %s \n"
		     ,zstart,nper,zstep,nstep,what_is_present_rot_value()
		     ,dead,bt->calib_im->filename);
       */
       op->op_idle_action = force_op_idle_action;
       op->op_post_display = force_op_post_display;
       fill_next_available_job_spot(1, sp->im[1]+1, COLLECT_DATA, i, imr, oi, pr, op, (void*)sp, force_job_scanz,0);

       op->user_ispare[ISPARE_BEAD_NB] = i;


       //       op = create_and_attach_one_plot(pr, 2*nstep,  2*nstep, 0);
     }
   //#endif
   //win_printf("3");
 my_set_window_title("force 4");

   for (i = 0, nf = 0; i <= nstep ; i++)
     {
       k = fill_next_available_action(sp->im[i], MV_ZMAG_ABS, sp->zm[i]);
       if (k < 0)	win_printf_OK("Could not add pending action!");
     }
   //win_title_used = 1;
   // win_printf("4");
 my_set_window_title("force 4");
   return D_O_K;

}

# endif // OLD


int force_record_job_zmag(int im, struct future_job *job)
{
  register int j, k;
  int min_pos, max_pos, page_n, i_page;
  d_s *ds = NULL;

  if (force_r == NULL)  return 0;
  if (job == NULL || job->oi == NULL || job->in_progress == 0 || job->op->n_dat < 1) return 0;
  if (im < job->imi)  return 0;

  ds = job->op->dat[0];

  max_pos = j = force_r->abs_pos;
  min_pos = force_r->starting_in_page_index + (force_r->starting_page * force_r->page_size);

  for (k = ds->nx, j = min_pos + k; j < max_pos; k++, j++)
    {
      page_n = j/force_r->page_size;
      i_page = j%force_r->page_size;
      add_new_point_to_ds(ds, force_r->imi[page_n][i_page], force_r->zmag[page_n][i_page]);
    }
  job->op->need_to_refresh = 1;
  job->imi += 32;
  if (working_gen_record == NULL)
      job->in_progress = 0;

  return 0;
}



int force_record_job_scan(int im, struct future_job *job)
{
  register int i, j, k;
  int  max_pos, page_n, i_page, nd;
  static float min = 0, max = 0, tmp, tmp2;
  b_record *br = NULL;
  d_s *ds = NULL;

  if (force_r == NULL)  return 0;
  if (job == NULL || job->oi == NULL || job->in_progress == 0 || job->op->n_dat < 1) return 0;
  if (im < job->imi)  return 0;

  ds = job->op->dat[0];
  br = force_r->b_r[job->bead_nb];

  max_pos = j = force_r->abs_pos;

  nd = (sp->c_step/2);
  if (sp->c_step%2 == 0) nd--;

  nd = (nd <= ds->mx) ? nd : ds->mx;
  ds->nx = ds->ny = nd;

  //for (i = (ds->nx) ? ds->nx - 1 : 0; i < nd; i++)
  for (i = 0; i < nd; i++)
    {
      ds->xd[i] = ds->yd[i] = 0;
      for (j = sp->grim[i], k = 0; (j < sp->grim[i] + sp->nper) && (j < max_pos); j++, k++)
	{
	  page_n = j/force_r->page_size;
	  i_page = j%force_r->page_size;
	  ds->yd[i] += force_r->zmag[page_n][i_page];
	  ds->xd[i] += force_r->z_cor * br->z[page_n][i_page];
	}
      if (k)
	{
	  ds->xd[i] /= k;
	  ds->yd[i] /= k;
	  if (i == 0)  	  min = max = ds->xd[i];
	  min = (ds->xd[i] < min) ? ds->xd[i] : min;
	  max = (ds->xd[i] > max) ? ds->xd[i] : max;
	}
      else if (i == nd-1)
	  ds->nx = ds->ny = nd-1;
      //if ((i == np - 1) && (k == sp->nper)) job->in_progress = 0;
    }

  tmp = (max - min)*0.05;
  tmp = (tmp < min_scale_z) ? min_scale_z : tmp;
  for (i = 0, tmp2 = min_scale_z;tmp > tmp2; i++)
    {
      if (i%3 == 0) tmp2 += tmp2;
      else if (i%3 == 1) tmp2 += tmp2;
      else tmp2 *= 2.5;
    }
  tmp = tmp2/2;
  min -=  tmp;
  max +=  tmp;

  job->op->x_lo = min;
  job->op->x_hi = max;
  set_plot_x_fixed_range(job->op);

  job->op->need_to_refresh = 1;

  job->imi += 32;
  if (working_gen_record == NULL)
    {
      job->in_progress = 0;
      win_title_used = 0;
    }
  return 0;
}

int force_record_job_force_f(int im, struct future_job *job)
{
  register int i, j, k;
  int   max_pos, page_n, i_page, nd;
  static float min = 0, max = 0, tmp, tmp2;
  b_record *br = NULL;
  d_s *ds = NULL;

  if (force_r == NULL)  return 0;
  if (job == NULL || job->oi == NULL || job->in_progress == 0 || job->op->n_dat < 1) return 0;
  if (im < job->imi)  return 0;

  ds = job->op->dat[0];
  br = force_r->b_r[job->bead_nb];

  max_pos = j = force_r->abs_pos;

  //np = sp->nstep;
  //np += (sp->one_way) ? 0 : np;

  nd = (sp->c_step/2);
  if (sp->c_step%2 == 0) nd--;

  nd = (nd <= ds->mx) ? nd : ds->mx;
  ds->nx = ds->ny = nd;
  //for (i = (ds->nx) ? ds->nx - 1 : 0; i < nd; i++)
  for (i = 0; i < nd; i++)
    {
      ds->xd[i] = ds->yd[i] = 0;
      for (j = sp->grim[i], k = 0; (j < sp->grim[i] + sp->nper) && (j < max_pos); j++, k++)
	{
	  page_n = j/force_r->page_size;
	  i_page = j%force_r->page_size;
	  ds->yd[i] += force_r->zmag[page_n][i_page];
	  ds->xd[i] += force_r->z_cor * br->z[page_n][i_page];
	}
      if (k)
	{
	  ds->xd[i] /= k;
	  ds->yd[i] /= k;
	  if (i == 0)  	  min = max = ds->xd[i];
	  min = (ds->xd[i] < min) ? ds->xd[i] : min;
	  max = (ds->xd[i] > max) ? ds->xd[i] : max;
	}
      else if (i == nd-1)
	  ds->nx = ds->ny = nd-1;
      ds->yd[i] = zmag_to_force(0, ds->yd[i]);
      //if ((i == np - 1) && (k == sp->nper)) job->in_progress = 0;
    }

  tmp = (max - min)*0.05;
  tmp = (tmp < min_scale_z) ? min_scale_z : tmp;
  for (i = 0, tmp2 = min_scale_z;tmp > tmp2; i++)
    {
      if (i%3 == 0) tmp2 += tmp2;
      else if (i%3 == 1) tmp2 += tmp2;
      else tmp2 *= 2.5;
    }
  tmp = tmp2/2;
  min -=  tmp;
  max +=  tmp;
  job->op->x_lo = min;
  job->op->x_hi = max;
  set_plot_x_fixed_range(job->op);

  job->op->need_to_refresh = 1;
  job->imi += 32;
  if (working_gen_record == NULL)
    {
      job->in_progress = 0;
      win_title_used = 0;
    }
  return 0;
}




int force_record_job_xyz(int im, struct future_job *job)
{
  int i, j;
  int page_n, i_page, max_pos, min_pos;
  b_record *br = NULL;
  d_s *dsz = NULL, *dsx = NULL, *dsy = NULL;
  //float min = 0, max = 0, minz = 0, maxz = 0, tmp, tmp2;

  if (job == NULL || job->oi == NULL || job->in_progress == 0 || job->op->n_dat < 3) return 0;
  if (im < job->imi)  return 0;

  dsx = job->op->dat[0];
  dsy = job->op->dat[1];
  dsz = job->op->dat[2];
  //dszm = job->op->dat[3];
  br = force_r->b_r[job->bead_nb];

  max_pos = j = force_r->abs_pos;
  min_pos = sp->im[0];//force_r->last_starting_pos;

  max_pos = j = force_r->abs_pos;
  min_pos = force_r->starting_in_page_index + (force_r->starting_page * force_r->page_size);

  //  set_plot_title(job->op, "Bead %d trajectory %d-0 pts %d-%d", i,n_force,min_pos+dsx->nx,max_pos);

  //dsz->nx = dsz->ny =  dszm->nx = dszm->ny = 0;
  for (i = dsx->nx, j = min_pos + i; j < max_pos; i++, j++)
    {
      page_n = j/force_r->page_size;
      i_page = j%force_r->page_size;
      add_new_point_to_ds(dsx, force_r->imi[page_n][i_page], force_r->ax + force_r->dx * br->x[page_n][i_page]);
      add_new_point_to_ds(dsy, force_r->imi[page_n][i_page], force_r->ay + force_r->dy * br->y[page_n][i_page]);
      add_new_point_to_ds(dsz, force_r->imi[page_n][i_page], force_r->z_cor * br->z[page_n][i_page]);
      //add_new_point_to_ds(dszm, force_r->imi[page_n][i_page], force_r->zmag[page_n][i_page]);
    }

  job->op->need_to_refresh = 1;
  job->imi += 32;

  if (working_gen_record == NULL)
      job->in_progress = 0;
  return 0;
}

int force_job_record_scanf(int im, struct future_job *job)
{
  register int i, k;
  int n_pt = 0;
  float kxl, dkx;
  b_record *br = NULL;
  d_s *dskx = NULL, *dsky = NULL;


  if (force_r == NULL || sp == NULL)  return 0;
  if ((void*)sp != job->more_data) return 0;
  if (job == NULL || job->oi == NULL || job->in_progress == 0 || job->op->n_dat < 2) return 0;
  if (im < job->imi)  return 0;

  dskx = job->op->dat[0];
  dsky = job->op->dat[1];

  br = force_r->b_r[job->bead_nb];

  for (k = n_pt = 0; k < br->c_s_r; k++)
    n_pt = (sp->i_zm[k] > n_pt) ? sp->i_zm[k] : n_pt;


  for (k = dskx->nx; k <= n_pt; k++)
    {
      for (i = 0; i < br->c_s_r; i++)
	{
	  if (sp->i_zm[i] != k) continue;
	  if (br->s_r[i].kx > 0)
	    {
	      kxl = br->s_r[i].kx*1e6;
	      dkx = br->s_r[i].dax;
	      dkx = (br->s_r[i].ax > 0) ? dkx/br->s_r[i].ax : 0;
	      dkx *= kxl;
	      kxl *= br->s_r[i].mzm;
	      dkx *= br->s_r[i].mzm;
	      add_new_point_with_xy_error_to_ds(dskx, br->s_r[i].mzm, br->s_r[i].dmzm, kxl, dkx );
	    }
	  if (br->s_r[i].ky > 0)
	    {
	      kxl = br->s_r[i].ky*1e6;
	      dkx = br->s_r[i].day;
	      dkx = (br->s_r[i].ay > 0) ? dkx/br->s_r[i].ay : 0;
	      dkx *= kxl;
	      kxl *= br->s_r[i].mzm;
	      dkx *= br->s_r[i].mzm;
	      add_new_point_with_xy_error_to_ds(dsky, br->s_r[i].mzm, br->s_r[i].dmzm,kxl, dkx );
	    }
	}
    }
  job->op->need_to_refresh = 1;
  job->imi += 32;
  if (working_gen_record == NULL)       job->in_progress = 0;
  return 0;
}


int force_record_job_comp_f(int im, struct future_job *job)
{
  register int i, k;
  int n_pt = 0;
  float kxl, dkx, fe;
  b_record *br = NULL;
  d_s *dskx = NULL, *dsky = NULL;

  if (force_r == NULL || sp == NULL)  return 0;
  if ((void*)sp != job->more_data) return 0;
  if (job == NULL || job->oi == NULL || job->in_progress == 0 || job->op->n_dat < 2) return 0;
  if (im < job->imi)  return 0;

  dskx = job->op->dat[0];
  dsky = job->op->dat[1];

  br = force_r->b_r[job->bead_nb];

  for (k = n_pt = 0; k < br->c_s_r; k++)
    n_pt = (sp->i_zm[k] > n_pt) ? sp->i_zm[k] : n_pt;

  for (k = dskx->nx; k <= n_pt; k++)
    {
      for (i = 0; i < br->c_s_r; i++)
	{
	  if (sp->i_zm[i] != k) continue;
	  if (br->s_r[i].kx > 0)
	    {
	      fe = zmag_to_force(0, br->s_r[i].zmag);
	      kxl = br->s_r[i].kx*1e6;
	      dkx = br->s_r[i].dax;
	      dkx = (br->s_r[i].ax > 0) ? dkx/br->s_r[i].ax : 0;
	      dkx *= kxl;
	      kxl *= br->s_r[i].mzm;
	      dkx *= br->s_r[i].mzm;
	      add_new_point_with_y_error_to_ds(dskx, fe, kxl, dkx );
	    }
	  if (br->s_r[i].ky > 0)
	    {
	      fe = zmag_to_force(0, br->s_r[i].zmag);
	      kxl = br->s_r[i].ky*1e6;
	      dkx = br->s_r[i].day;
	      dkx = (br->s_r[i].ay > 0) ? dkx/br->s_r[i].ay : 0;
	      dkx *= kxl;
	      kxl *= br->s_r[i].mzm;
	      dkx *= br->s_r[i].mzm;
	      add_new_point_with_y_error_to_ds(dsky, fe, kxl, dkx );
	    }
	}
    }
  job->op->need_to_refresh = 1;
  job->imi += 32;
  if (working_gen_record == NULL)   job->in_progress = 0;
  return 0;
}


int force_job_kxky(int im, struct future_job *job)
{
  register int i, k;
  int n_pt = 0;
  float kxl, dkx;
  b_record *br = NULL;
  d_s *dskx = NULL, *dsky = NULL;


  if (force_r == NULL || sp == NULL)  return 0;
  if ((void*)sp != job->more_data) return 0;
  if (job == NULL || job->oi == NULL || job->in_progress == 0 || job->op->n_dat < 2) return 0;
  if (im < job->imi)  return 0;

  dskx = job->op->dat[0];
  dsky = job->op->dat[1];

  br = force_r->b_r[job->bead_nb];

  for (k = n_pt = 0; k < br->c_s_r; k++)
    n_pt = (sp->i_zm[k] > n_pt) ? sp->i_zm[k] : n_pt;


  for (k = dskx->nx; k <= n_pt; k++)
    {
      for (i = 0; i < br->c_s_r; i++)
	{
	  if (sp->i_zm[i] != k) continue;
	  if (br->s_r[i].kx > 0)
	    {
	      kxl = br->s_r[i].kx*1e6;
	      dkx = br->s_r[i].dax;
	      dkx = (br->s_r[i].ax > 0) ? dkx/br->s_r[i].ax : 0;
	      dkx *= kxl;
	      add_new_point_with_y_error_to_ds(dskx, dskx->nx, kxl, dkx );
	    }
	  if (br->s_r[i].ky > 0)
	    {
	      kxl = br->s_r[i].ky*1e6;
	      dkx = br->s_r[i].day;
	      dkx = (br->s_r[i].ay > 0) ? dkx/br->s_r[i].ay : 0;
	      dkx *= kxl;
	      add_new_point_with_y_error_to_ds(dsky, dsky->nx, kxl, dkx );
	    }
	}
    }
  job->op->need_to_refresh = 1;
  job->imi += 32;
  if (working_gen_record == NULL)       job->in_progress = 0;
  return 0;
}


int force_job_fcxy(int im, struct future_job *job)
{
  register int i, k;
  int n_pt = 0;
  float kxl, dkx;
  b_record *br = NULL;
  d_s *dskx = NULL, *dsky = NULL;


  if (force_r == NULL || sp == NULL)  return 0;
  if ((void*)sp != job->more_data) return 0;
  if (job == NULL || job->oi == NULL || job->in_progress == 0 || job->op->n_dat < 2) return 0;
  if (im < job->imi)  return 0;

  dskx = job->op->dat[0];
  dsky = job->op->dat[1];

  br = force_r->b_r[job->bead_nb];

  for (k = n_pt = 0; k < br->c_s_r; k++)
    n_pt = (sp->i_zm[k] > n_pt) ? sp->i_zm[k] : n_pt;


  for (k = dskx->nx; k <= n_pt; k++)
    {
      for (i = 0; i < br->c_s_r; i++)
	{
	  if (sp->i_zm[i] != k) continue;
	  if (br->s_r[i].kx > 0)
	    {
	      kxl = (float)br->s_r[i].fcx/br->s_r[i].nxeff;
	      kxl *= Pico_param.camera_param.camera_frequency_in_Hz;
	      dkx = (float)br->s_r[i].dfcx/br->s_r[i].nxeff;
	      dkx *= Pico_param.camera_param.camera_frequency_in_Hz;
	      add_new_point_with_y_error_to_ds(dskx, dskx->nx, kxl, dkx );
	    }
	  if (br->s_r[i].ky > 0)
	    {
	      kxl = (float)br->s_r[i].fcy/br->s_r[i].nyeff;
	      kxl *= Pico_param.camera_param.camera_frequency_in_Hz;
	      dkx = (float)br->s_r[i].dfcy/br->s_r[i].nyeff;
	      dkx *= Pico_param.camera_param.camera_frequency_in_Hz;
	      add_new_point_with_y_error_to_ds(dsky, dsky->nx, kxl, dkx );
	    }
	}
    }
  job->op->need_to_refresh = 1;
  job->imi += 32;
  if (working_gen_record == NULL)       job->in_progress = 0;
  return 0;
}


double compute_chi2_and_fit_wong( double tau_0, // characteristime in micro s
				 double *a,    // amplitude fitted
				 d_s *ds)      // data with error
{
  register int i, j = 0;
  double n, d, tmp, sig2, fita = 0, alphal;

  if (ds == NULL || ds->nx < 3 || ds->ye == NULL || (tau_0 == 0)) return -1.0;
  for(i = 0, n = d = 0; i < ds->nx; i++)
    {
      alphal = (double)ds->xd[i]/ tau_0;
      tmp = 1 - exp(-alphal);
      tmp /= alphal * alphal;
      tmp = ((double)1.0/alphal) - tmp;
      tmp *= 2;
      sig2 = ds->ye[i]*ds->ye[i];
      if (sig2 == 0 && j != WIN_CANCEL)
	j = my_set_window_title("\\sigma = 0 ,division by zero at pt %d/%d\n"
				"x %g y %g er y %g\n"
				"src %s\n"
				,i,ds->nx,ds->xd[i],ds->yd[i],ds->ye[i]
				,ds->source);
      else if (tmp == 0 && j != WIN_CANCEL)
	j = my_set_window_title("tmp = 0 division by zero at pt %d",i);
      else
	{
	  n += (tmp * ds->yd[i])/sig2;
	  d += tmp*tmp/sig2;
	}
    }
  if (d == 0) 	my_set_window_title("division by zero in fit");
  else	fita = n/d;
  for(i = 0, n = 0; i < ds->nx; i++)
    {
      alphal = (double)ds->xd[i]/ tau_0;
      tmp = 1 - exp(-alphal);
      tmp /= alphal * alphal;
      tmp = ((double)1.0/alphal) - tmp;
      tmp *= 2;
      sig2 = ds->ye[i]*ds->ye[i];
      if (sig2 == 0 && j != WIN_CANCEL) my_set_window_title("Loop 2 \\sigma = 0 ,division by zero at pt %d",i);
      else if (tmp == 0 && j != WIN_CANCEL)	my_set_window_title("Loop 2 tmp = 0 division by zero at pt %d",i);
      else
	{
	  tmp = ds->yd[i] - (fita*tmp);
	  n += (tmp*tmp)/sig2;
	}
    }
  if (a != NULL)	*a = fita;
  return n;
}



int fit_wong_ds(O_p *op, d_s *ds, float *x2, float *tau, float *chisq)
{
  register int i;
  d_s *dsfit = NULL;
  double mint, maxt, chi2 = 0, a, t, chi20, a0, t0, chi21, a1, t1, tmp, alphal, tm;
  char st[1024] = {0};
  float my = 0, sy2 = 0;

  if (ds == NULL) return 1;

  for (i = 0, my = 0; i < ds->nx; i++) my += ds->yd[i];
  my = (ds->nx == 0) ? 0 : my/ds->nx;
  for (i = 0, sy2 = 0; i < ds->nx; i++) sy2 += (ds->yd[i]-my)*(ds->yd[i]-my);
  sy2 = (ds->nx < 2) ? 0 : sy2/(ds->nx-1);

  if (sy2 == 0)
    {
      my_set_window_title("wong fit, zero variance! %d pts y0 %g for %s",ds->nx,ds->yd[0],(ds->source)?ds->source :"no source" );
      return 1;
    }
  if ((dsfit = create_and_attach_one_ds(op, 256, 256, 0)) == NULL)
    return win_printf_OK("cannot create plot");

  if (ds->source != NULL)
    {
      set_ds_source(dsfit,"Wong fit to %s",ds->source);
      //win_printf("ds src %s\n dsfit src %s\n",ds->source,dsfit->source);
    }
  else set_ds_source(dsfit,"Wong fit");

  for (i = 0, t0 = t1 = ds->xd[0]; i < ds->nx; i++)
    {
      if (t1 < ds->xd[i]) t1 = ds->xd[i];
      if (t0 > ds->xd[i]) t0 = ds->xd[i];
    }
  mint = t0; maxt = t1;
  //win_printf("min %g max %g",mint, maxt);
  t0 /= 4;
  if (t0 <= 0) t0 = 1;
  //win_printf("first chi2 t0 %g ds src%s",t0, ds->source);
  chi2 = chi20 = compute_chi2_and_fit_wong(t0, &a0, ds);
  t1 *= 4;
  for (t = t0, chi20 = compute_chi2_and_fit_wong(tm = t, &a, ds); t <= t1; t *=2)
    {
      chi2 = compute_chi2_and_fit_wong(t, &a, ds);
      //win_printf("\\chi^2 = %g n0 = %g a %g",chi2,t,a);
      if (chi2 < chi20)
	{
	  chi20 = chi2;
	  tm = t;
	}
    }
  t = tm;
  //win_printf("first found %g chi2",t,chi20);
  chi21 = compute_chi2_and_fit_wong(2*t, &a1, ds);
  chi2 = compute_chi2_and_fit_wong(t/2, &a1, ds);
  if (chi21 < chi2)
    {
      chi2 = chi20;
      chi20 = compute_chi2_and_fit_wong((t0 = t), &a0, ds);
      chi21 = compute_chi2_and_fit_wong((t1 = 2*t), &a1, ds);
    }
  else
    {
      chi2 = chi20;
      chi20 = compute_chi2_and_fit_wong((t0 = t/2), &a0, ds);
      chi21 = compute_chi2_and_fit_wong((t1 = t), &a1, ds);
    }

  for (t = exp((log(t0)+log(t1))/2); fabs(t1/t0) > 1.00005 ; )
    {
      t = exp((log(t0)+log(t1))/2);
      chi2 = compute_chi2_and_fit_wong(t, &a, ds);
      if (chi20 < chi21)
	{
	  chi21 = chi2;
	  t1 = t;
	  a1 = a;
	}
      else
	{
	  chi20 = chi2;
	  t0 = t;
	  a0 = a;
	}
    }

  tmp = log(maxt/mint);
  for (i = 0; i < dsfit->nx; i++)
    {
      dsfit->xd[i] = exp((i*tmp)/dsfit->nx);
      dsfit->xd[i] *= mint;
      alphal = (double)dsfit->xd[i]/ t;
      dsfit->yd[i] = 1 - exp(-alphal);
      dsfit->yd[i] /= alphal * alphal;
      dsfit->yd[i] = ((double)1.0/alphal) - dsfit->yd[i];
      dsfit->yd[i] *= 2 * a;
    }
  snprintf(st,sizeof(st),"\\fbox{\\pt8\\stack{{\\sl \\delta x^2(w) = "
	  "x_0^2 \\frac{2}{\\alpha} [1 - \\frac{1-exp^{-\\alpha}}{\\alpha}]}"
	  " {with \\alpha = w/\\tau_0}"
	  "{x_0^2 = %e%s}"
	  "{\\tau_0 = %g  %s}"
	  "{\\chi^2 = %g n = %d}}}",
	  a*op->dy,(op->y_unit != NULL)?op->y_unit:" ",
	  t*op->dx,(op->x_unit != NULL)?op->x_unit:" ",
	  chi2,ds->nx);
  push_plot_label_in_ds(dsfit, dsfit->xd[dsfit->nx/2], a, st, USR_COORD);
  if (x2) *x2 = a*op->dy;
  if (tau) *tau = t*op->dx;
  if (chisq) *chisq = chi2;
  return 0;
}



int force_job_wong_x(int im, struct future_job *job)
{
  register int k, j;
  float x2, dx2;
  b_record *br = NULL;
  d_s *ds = NULL, *dst = NULL;
  int i, last_pt;
  char pattern[256] = {0};
  float tau = 0, chisq = 0;

  if (force_r == NULL || sp == NULL)  return 0;
  if ((void*)sp != job->more_data) return 0;
  if (job == NULL || job->oi == NULL || job->in_progress == 0 || job->op->n_dat < 1) return 0;
  if (im < job->imi)  return 0;
  job->imi += 64;
  br = force_r->b_r[job->bead_nb];

  for (i = 0, last_pt = -1; last_pt < 0 && i < br->c_s_r && i < sp->nb_acq_pt; i++)
    {  // we find the last acquisition treated for this bead
      if (sp->i_mod_shut[i] < 0) continue;
      snprintf(pattern,sizeof(pattern), "X variance point series %d bead",sp->i_zm[i]);// sp->i_mod_shut[i]);
      ds = find_source_specific_ds_in_op(job->op,pattern);
      if (ds == NULL)
	{
	  last_pt = i;
	  break;
	}
      if (sp->is_zm[i] >= ds->nx)
	{
	  last_pt = i;
	  break;
	}
    }

  for (k = last_pt; k < br->c_s_r && k < sp->nb_acq_pt; k++)
    {
      if (k < 0 || sp->i_mod_shut[k] < 0 || br->s_r == NULL) continue;
      snprintf(pattern,sizeof(pattern), "X variance point series %d bead",sp->i_zm[k]);//sp->i_mod_shut[k]);
      ds = find_source_specific_ds_in_op(job->op,pattern);
      if (ds == NULL)
	{
	  if ((ds = create_and_attach_one_ds(job->op, sp->n_expo_div+4,  sp->n_expo_div+4, 0)) == NULL)
	    return win_printf_OK("I can't create plot !");
	  ds->nx = ds->ny = 0;
	  set_dot_line(ds);
	  set_plot_symb(ds, "\\pt5\\oc");
	  set_ds_source(ds,"X variance point series %d bead %d\nZ mag = %g rot %g "
			,sp->i_zm[k],job->bead_nb,sp->zm[k],sp->ro[k]);   // ,sp->i_mod_shut[k],sp->zm[k],sp->ro[k]);
	}
      if (ds == NULL) break;
      if (sp->is_zm[k] >= ds->nx)
	{
	  x2 = br->s_r[k].sx2*1e6;
	  dx2 = (br->s_r[k].nxeff/2 > 1) ? x2/sqrt((br->s_r[k].nxeff)/2-1) : x2;
	  add_new_point_with_y_error_to_ds(ds, sp->exp[k], x2, dx2);
	}
      if (sp->is_zm[k] == (sp->nis_zm[k]-1))
	{
	  for (j = 0; j < br->c_s_r && j < sp->nb_acq_pt; j++)
	    if (sp->i_zm[j] == sp->i_zm[k] && sp->is_zm[j] == 0) break;
	  for ( ;ds->nx < (sp->nis_zm[k]+3); )
	    {
	      if ((ds->nx  - sp->nis_zm[k]) == 0)
		{
		  x2 = br->s_r[j].sx2_m2*1e6;
		  dx2 = (br->s_r[j].nxeff/4 > 1) ? x2/sqrt((br->s_r[j].nxeff)/4-1) : x2;
		  add_new_point_with_y_error_to_ds(ds, 2*sp->exp[j], x2, dx2);
		}
	      if ((ds->nx  - sp->nis_zm[k]) == 1)
		{
		  x2 = br->s_r[j].sx2_m4*1e6;
		  dx2 = (br->s_r[j].nxeff/8 > 1) ? x2/sqrt((br->s_r[j].nxeff/8)-1) : x2;
		  add_new_point_with_y_error_to_ds(ds, 4*sp->exp[j], x2, dx2);
		}
	      if ((ds->nx  - sp->nis_zm[k]) == 2)
		{
		  x2 = br->s_r[j].sx2_m8*1e6;
		  dx2 = (br->s_r[j].nxeff/16 > 1) ? x2/sqrt((br->s_r[j].nxeff)/16-1) : x2;
		  add_new_point_with_y_error_to_ds(ds, 8*sp->exp[j], x2, dx2);
		}
	    }
	  snprintf(pattern,sizeof(pattern), "Wong fit to X variance point series %d bead",sp->i_zm[k]);//sp->i_mod_shut[k]);
	  dst = find_source_specific_ds_in_op(job->op,pattern);
	  if (dst == NULL)
	    {
	      fit_wong_ds(job->op, ds, &x2, &tau, &chisq);//;NULL, NULL, NULL);
	      //for (j = 0, t = ds->xd[0]; j < ds->nx; j++)
	      //t = (ds->xd[i] < t) ?  ds->xd[i] : t;
	      br->s_r[k].ax = x2*1e-6;
	      dx2 = (br->s_r[k].nxeff/2 > 1) ? x2/sqrt((br->s_r[k].nxeff)/2-1) : x2;
	      br->s_r[k].dax = dx2*1e-6;
	      if (x2 > 0) br->s_r[k].kx = (1.38e-5 * 298)/x2;
	      if (tau > 0)
		{
		  br->s_r[k].fcx = 1e6/(tau*2*M_PI);
		  br->s_r[k].fcx /= (Pico_param.camera_param.camera_frequency_in_Hz > 0)
		    ? Pico_param.camera_param.camera_frequency_in_Hz : 1;
		  br->s_r[k].fcx *=br->s_r[i].nxeff;
		}
	    }
	}
    }
  set_plot_y_log(job->op);
  set_plot_x_log(job->op);
  job->op->need_to_refresh = 1;
  job->imi += (k < 0) ? 64 : br->s_r[k].nxeff/4;
  if (working_gen_record == NULL)       job->in_progress = 0;
  return 0;
}




int force_job_wong_y(int im, struct future_job *job)
{
  register int k, j;
  float y2, dy2;
  b_record *br = NULL;
  d_s *ds = NULL, *dst = NULL;
  int i, last_pt;
  char pattern[256] = {0};
  float tau = 0, chisq = 0;

  if (force_r == NULL || sp == NULL)  return 0;
  if ((void*)sp != job->more_data) return 0;
  if (job == NULL || job->oi == NULL || job->in_progress == 0 || job->op->n_dat < 1) return 0;
  if (im < job->imi)  return 0;
  job->imi += 64;
  br = force_r->b_r[job->bead_nb];

  for (i = 0, last_pt = -1; last_pt < 0 && i < br->c_s_r && i < sp->nb_acq_pt; i++)
    {  // we find the last acquisition treated for this bead
      if (sp->i_mod_shut[i] < 0) continue;
      snprintf(pattern,sizeof(pattern), "Y variance point series %d bead",sp->i_zm[i]);// sp->i_mod_shut[i]);
      ds = find_source_specific_ds_in_op(job->op,pattern);
      if (ds == NULL)
	{
	  last_pt = i;
	  break;
	}
      if (sp->is_zm[i] >= ds->nx)
	{
	  last_pt = i;
	  break;
	}
    }

  for (k = last_pt; k < br->c_s_r && k < sp->nb_acq_pt; k++)
    {
      if (k < 0 || sp->i_mod_shut[k] < 0 || br->s_r == NULL) continue;
      snprintf(pattern,sizeof(pattern), "Y variance point series %d bead",sp->i_zm[k]);//sp->i_mod_shut[k]);
      ds = find_source_specific_ds_in_op(job->op,pattern);
      if (ds == NULL)
	{
	  if ((ds = create_and_attach_one_ds(job->op, sp->n_expo_div+4,  sp->n_expo_div+4, 0)) == NULL)
	    return win_printf_OK("I can't create plot !");
	  ds->nx = ds->ny = 0;
	  set_dot_line(ds);
	  set_plot_symb(ds, "\\pt5\\oc");
	  set_ds_source(ds,"Y variance point series %d bead %d\nZ mag = %g rot %g"
			,sp->i_zm[k],job->bead_nb,sp->zm[k],sp->ro[k]);   // ,sp->i_mod_shut[k],sp->zm[k],sp->ro[k]);
	}
      if (ds == NULL) break;
      if (sp->is_zm[k] >= ds->nx)
	{
	  y2 = br->s_r[k].sy2*1e6;
	  dy2 = (br->s_r[k].nxeff/2 > 1) ? y2/sqrt((br->s_r[k].nxeff)/2-1) : y2;
	  add_new_point_with_y_error_to_ds(ds, sp->exp[k], y2, dy2);
	}
      if (sp->is_zm[k] == (sp->nis_zm[k]-1))
	{
	  for (j = 0; j < br->c_s_r && j < sp->nb_acq_pt; j++)
	    if (sp->i_zm[j] == sp->i_zm[k] && sp->is_zm[j] == 0) break;
	  for ( ;ds->nx < (sp->nis_zm[k]+3); )
	    {
	      if ((ds->nx  - sp->nis_zm[k]) == 0)
		{
		  y2 = br->s_r[j].sy2_m2*1e6;
		  dy2 = (br->s_r[j].nxeff/4 > 1) ? y2/sqrt((br->s_r[j].nxeff)/4-1) : y2;
		  add_new_point_with_y_error_to_ds(ds, 2*sp->exp[j], y2, dy2);
		}
	      if ((ds->nx  - sp->nis_zm[k]) == 1)
		{
		  y2 = br->s_r[j].sy2_m4*1e6;
		  dy2 = (br->s_r[j].nxeff/8 > 1) ? y2/sqrt((br->s_r[j].nxeff)/8-1) : y2;
		  add_new_point_with_y_error_to_ds(ds, 4*sp->exp[j], y2, dy2);
		}
	      if ((ds->nx  - sp->nis_zm[k]) == 2)
		{
		  y2 = br->s_r[j].sy2_m8*1e6;
		  dy2 = (br->s_r[j].nxeff/16 > 1) ? y2/sqrt((br->s_r[j].nxeff)/16-1) : y2;
		  add_new_point_with_y_error_to_ds(ds, 8*sp->exp[j], y2, dy2);
		}
	    }
	  snprintf(pattern,sizeof(pattern), "Wong fit to Y variance point series %d bead",sp->i_zm[k]);//sp->i_mod_shut[k]);
	  dst = find_source_specific_ds_in_op(job->op,pattern);
	  if (dst == NULL)
	    {
	      fit_wong_ds(job->op, ds, &y2, &tau, &chisq);//;NULL, NULL, NULL);
	      //for (j = 0, t = ds->xd[0]; j < ds->nx; j++)
	      //t = (ds->xd[i] < t) ?  ds->xd[i] : t;
	      br->s_r[k].ay = y2*1e-6;
	      dy2 = (br->s_r[k].nxeff/2 > 1) ? y2/sqrt((br->s_r[k].nxeff)/2-1) : y2;
	      br->s_r[k].day = dy2*1e-6;
	      if (y2 > 0) br->s_r[k].ky = (1.38e-5 * 298)/y2;
	      if (tau > 0)
		{
		  br->s_r[k].fcy = 1e6/(tau*2*M_PI);
		  br->s_r[k].fcy /= (Pico_param.camera_param.camera_frequency_in_Hz > 0)
		    ? Pico_param.camera_param.camera_frequency_in_Hz : 1;
		  br->s_r[k].fcy *=br->s_r[i].nyeff;
		}

	    }
	}
    }
  set_plot_y_log(job->op);
  set_plot_x_log(job->op);
  job->op->need_to_refresh = 1;
  job->imi += (k < 0) ? 64 : br->s_r[k].nxeff/4;
  if (working_gen_record == NULL)       job->in_progress = 0;
  return 0;
}





int force_record_job_single_xyz(int im, struct future_job *job)
{
  int i, j ;
  int nf, page_n, i_page, max_pos, min_pos, np, iter, itreat;
  float l_zmag = 0, l_rot = 0;
  b_track *bt = NULL;
  b_record *br = NULL;
  O_p *op = NULL;
  d_s *dsz = NULL, *dsx = NULL, *dsy = NULL, *ds = NULL;
  stiffness_resu *sr = NULL;
  float b_radius = 0;
  //float min = 0, max = 0, minz = 0, maxz = 0, tmp, tmp2;

  if (force_r == NULL || sp == NULL)  return 0;
  if ((void*)sp != job->more_data) return 0;
  if (job == NULL || job->oi == NULL || job->in_progress == 0 || job->op->n_dat < 3) return 0;
  if (im < job->imi)  return 0;

  dsx = job->op->dat[0];
  dsy = job->op->dat[1];
  dsz = job->op->dat[2];
  //dszm = job->op->dat[3];
  bt = track_info->bd[job->bead_nb];
  br = force_r->b_r[job->bead_nb];

  itreat = br->c_s_r;

  max_pos = j = force_r->abs_pos;
  min_pos = sp->im[0];//force_r->last_starting_pos;

  iter = (sp->c_step/2) - 1;
  //np = sp->nstep;
  //np += (sp->one_way) ? 0 : np;
  np = sp->nb_acq_pt;

  itreat = (itreat < iter) ? itreat : iter;
  if (itreat >= np)
    {
      job->in_progress = 0;
      return 0;
    }
  if (sp->grim[itreat] < 0)
    {
      job->imi += 32;
      return 0; // data point no yet define
    }
  min_pos = j = sp->grim[itreat];
  max_pos = sp->grim[itreat] + sp->rec_n[itreat];



  set_op_filename(job->op, "X(t)Y(t)Z(t)bd%dforce%d-%d.gr",job->bead_nb,n_force,itreat);
  nf = max_pos - min_pos;

  for (i = dsx->nx, j = min_pos + i; j < max_pos && j < force_r->abs_pos; i++, j++)
    {
      page_n = j/force_r->page_size;
      i_page = j%force_r->page_size;
      add_new_point_to_ds(dsx, force_r->imi[page_n][i_page], force_r->ax + force_r->dx * br->x[page_n][i_page]);
      add_new_point_to_ds(dsy, force_r->imi[page_n][i_page], force_r->ay + force_r->dy * br->y[page_n][i_page]);
      add_new_point_to_ds(dsz, force_r->imi[page_n][i_page], force_r->z_cor * br->z[page_n][i_page]);
      l_zmag = force_r->zmag[page_n][i_page];
      l_rot = force_r->rot_mag[page_n][i_page];
    }
  if (itreat > 0)
    {
      sr = br->s_r + (itreat - 1);
      set_plot_title(job->op, "\\stack{{Bead %d single trajectory %d-%d}"
		     "{\\pt8 Zmag %g shutter %d \\mu s gain %6.2f led %6.2f}{\\pt8 y2 %g ky %g}}",
		     job->bead_nb,n_force,itreat,sp->zm[itreat],sp->exp[itreat]
		     ,sp->gain[itreat],sp->led[itreat],sr->sx2, sr->kx);
    }
  /*
  else
      set_plot_title(job->op, "\\stack{{Bead %d single trajectory %d-%d}{Zmag %g %5.2f%%}}",
		     job->bead_nb,n_force,iter,sp->zm[iter],sr->sx2, sr->kx, (float)(100*dsx->nx)/sp->rec_n[itreat]);
  */
  job->op->need_to_refresh = 1;
  b_radius = Pico_param.bead_param.radius;
  if (max_pos < force_r->abs_pos)  // we have finished recording this point
    {
      if (itreat < br->n_s_r)
	{
	  sr = br->s_r + itreat;
	  sr->zmag = l_zmag;
	  sr->rot = l_rot;
	  sr->nxeff = dsx->nx;
	  //mean_y2_in_microns(job->op, dsx, 1, &sr->mxm, &sr->sx2, &sr->sx4);
	  mean_y2bin_slidding_average_in_microns(job->op, dsx, 1, 32, &sr->mxm, &sr->sx2, &sr->sx4);
	  mean_y2bin_slidding_average_in_microns(job->op, dsx, 2, 32, NULL, &sr->sx2_m2, NULL);
	  mean_y2bin_slidding_average_in_microns(job->op, dsx, 4, 32, NULL, &sr->sx2_m4, NULL);
	  mean_y2bin_slidding_average_in_microns(job->op, dsx, 8, 32, NULL, &sr->sx2_m8, NULL);

	  if ((sp->mod_expo_enable == 0 || sp->zmag_hi_fc > l_zmag) && sr->sx2 > MIN_DX2)
	    {
	      sr->x_cardinal_er = delta_de_fc_4_auto(job->op, dsx, 10, 0.5, 1, 1, 0, 0.97, b_radius, &sr->nxeff,
						     &sr->ax, &sr->dax, &sr->kx, &sr->fcx, &sr->dfcx,
						     &sr->etarx, &sr->etar2x, &sr->detar2x, &sr->dmxm);
	      ky = sr->kx;
	    }
	  else	set_ds_plot_label(dsx, (dsx->xd[0]+dsx->xd[dsx->nx-1])/2, 19*(dsx->yd[0]+dsx->yd[dsx->nx-1])/40,
				  USR_COORD, "\\pt7\\fbox{\\stack{{x^2 = %6.4f expo = %d \\mu s}}}"
				  ,sr->sx2*1e6,sp->exp[itreat]);
	  sr->nyeff = dsy->nx;
	  //mean_y2_in_microns(job->op, dsy, 1, &sr->mym, &sr->sy2, &sr->sy4);
	  mean_y2bin_slidding_average_in_microns(job->op, dsy, 1, 32, &sr->mym, &sr->sy2, &sr->sy4);
	  mean_y2bin_slidding_average_in_microns(job->op, dsy, 2, 32, NULL, &sr->sy2_m2, NULL);
	  mean_y2bin_slidding_average_in_microns(job->op, dsy, 4, 32, NULL, &sr->sy2_m4, NULL);
	  mean_y2bin_slidding_average_in_microns(job->op, dsy, 8, 32, NULL, &sr->sy2_m8, NULL);
	  if ((sp->mod_expo_enable == 0 || sp->zmag_hi_fc > l_zmag) && sr->sy2 > MIN_DX2)
	    {
	      sr->y_cardinal_er = delta_de_fc_4_auto(job->op, dsy, 10, 0.5, 1, 1, 0, 0.97, b_radius, &sr->nyeff,
						     &sr->ay, &sr->day, &sr->ky, &sr->fcy, &sr->dfcy,
						     &sr->etary, &sr->etar2y, &sr->detar2y, &sr->dmym);
	    }
	  else	set_ds_plot_label(dsy, (dsy->xd[0]+dsy->xd[dsy->nx-1])/2, 19*(dsy->yd[0]+dsy->yd[dsy->nx-1])/40,
				  USR_COORD, "\\pt7\\fbox{\\stack{{y^2 = %6.4f expo = %d \\mu s}}}"
				  ,sr->sy2*1e6,sp->exp[itreat]);
	  sr->nzeff = dsz->nx;
	  //mean_y2_in_microns(job->op, dsz, 1, &sr->mzm, &sr->sz2, &sr->sz4);
	  mean_y2bin_slidding_average_in_microns(job->op, dsz, 1, 32, &sr->mzm, &sr->sz2, &sr->sz4);
	  mean_y2bin_slidding_average_in_microns(job->op, dsz, 2, 32, NULL, &sr->sz2_m2, NULL);
	  mean_y2bin_slidding_average_in_microns(job->op, dsz, 4, 32, NULL, &sr->sz2_m4, NULL);
	  mean_y2bin_slidding_average_in_microns(job->op, dsz, 8, 32, NULL, &sr->sz2_m8, NULL);
	  if ((sp->mod_expo_enable == 0 || sp->zmag_hi_fc > l_zmag) && sr->sz2 > MIN_DX2)
	    {
	      sr->z_cardinal_er = delta_de_fc_4_auto(job->op, dsz, 10, 0.5, 1, 1, 0, 0.97, b_radius, &sr->nzeff,
						     &sr->az, &sr->daz, &sr->kz, &sr->fcz, &sr->dfcz,
						     &sr->etarz, &sr->etar2z, &sr->detar2z, &sr->dmzm);
	    }
	  else	set_ds_plot_label(dsz, (dsz->xd[0]+dsz->xd[dsz->nx-1])/2, 19*(dsz->yd[0]+dsz->yd[dsz->nx-1])/40,
				  USR_COORD, "\\pt7\\fbox{\\stack{{z^2 = %6.4f expo = %d \\mu s}}}"
				  ,sr->sz2*1e6,sp->exp[itreat]);
	  itreat++;
	  br->c_s_r = itreat;
	  if (itreat < br->n_s_r)
	    {
	      if (keep_all_plots)
		{
		  op = create_and_attach_one_plot(job->pr, sp->nper,  sp->nper, 0);
		  if (op == NULL)  return win_printf("Cannot allocate plot");
		  ds = op->dat[0];
		  ds->nx = ds->ny = 0;
		  set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
				"X coordinate l = %d, w = %d, nim = %d\n"
				"objective %f, zoom factor %f, sample %s Rotation %g\n"
				"Z magnets %g mm\n"
				,Pico_param.camera_param.camera_frequency_in_Hz,n_force,i
				,bt->cl,bt->cw,nf
				,Pico_param.obj_param.objective_magnification
				,Pico_param.micro_param.zoom_factor
				,pico_sample
				,what_is_present_rot_value()
				,what_is_present_z_mag_value());


		  if ((ds = create_and_attach_one_ds(op, sp->nper, sp->nper, 0)) == NULL)
		    return win_printf_OK("I can't create plot !");
		  ds->nx = ds->ny = 0;
		  set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
				"Y coordinate l = %d, w = %d, nim = %d\n"
				"objective %f, zoom factor %f, sample %s Rotation %g\n"
				"Z magnets %g mm\n"
				,Pico_param.camera_param.camera_frequency_in_Hz,n_force,i
				,bt->cl,bt->cw,nf
				,Pico_param.obj_param.objective_magnification
				,Pico_param.micro_param.zoom_factor
				,pico_sample
				,what_is_present_rot_value()
				,what_is_present_z_mag_value());
		  if ((ds = create_and_attach_one_ds(op, sp->nper, sp->nper, 0)) == NULL)
		    return win_printf_OK("I can't create plot !");
		  ds->nx = ds->ny = 0;
		  set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
				"Z coordinate l = %d, w = %d, nim = %d\n"
				"objective %f, zoom factor %f, sample %s Rotation %g\n"
				"Z magnets %g mm\n"
				,Pico_param.camera_param.camera_frequency_in_Hz,n_force,i
				,bt->cl,bt->cw,nf
				,Pico_param.obj_param.objective_magnification
				,Pico_param.micro_param.zoom_factor
				,pico_sample
				,what_is_present_rot_value()
				,what_is_present_z_mag_value());


		  create_attach_select_y_un_to_op(op, IS_METER, 0, 1, -6, 0, "\\mu m");
		  create_attach_select_x_un_to_op(op, IS_SECOND, 0
						  ,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");

		  op->op_idle_action = force_op_idle_action; //xyz_scan_idle_action;
		  op->op_post_display = force_op_post_display;
		  set_op_filename(op, "X(t)Y(t)Z(t)bd%dforce%d-0.gr",job->bead_nb,n_force);
		  set_plot_title(op, "Bead %d single trajectory %d-0 Zmag %g",job->bead_nb,n_force,sp->zm[0]);
		  set_plot_x_title(op, "Time");
		  set_plot_y_title(op, "Position");
		  job->op = op;
		  job->op->user_ispare[ISPARE_BEAD_NB] = job->bead_nb;
		}
	      else dsy->nx = dsy->ny = dsz->nx = dsz->ny = dsx->nx = dsx->ny = 0;
	    }
	  else job->in_progress = 0;
	}
    }
  job->op->need_to_refresh = 1;
  job->imi += 32;
  //if (working_gen_record == NULL)       job->in_progress = 0;
  return 0;
}


char *generate_force_title(void)
{
  return force_state_disp;
}



int force_action(int im, int c_i, int aci, int init_to_zero)
{
  int k, i, page_n, i_page, np, abs_pos;
  static int suspended = 0, prof_index_avg = 0, n_avg = 0;

  if (init_to_zero)
    {
      suspended = 0; prof_index_avg = 0; n_avg = 0;
      return 0;
    }

  (void)aci;
  if (force_r == NULL || sp == NULL)  return 0;
  i = (sp->c_step/2) - 1;
  //  np = sp->nstep;
  //  np += (sp->one_way) ? 0 : np;
  np = sp->nb_acq_pt;

  if (force_state == FORCE_SUSPENDED)
    {
      snprintf(force_state_disp,sizeof(force_state_disp),"Force scan %d supended at point %d",n_force,i);
      imr_TRACK_title_change_asked++;
      suspended = 1;
    }

  if (force_state != FORCE_RUNNING)  return 0;
  abs_pos = (force_r->abs_pos < 0) ? 0 : force_r->abs_pos;

  page_n = abs_pos/force_r->page_size;
  i_page = abs_pos%force_r->page_size;

  force_r->action_status[page_n][i_page] &= 0xff000000;
  force_r->action_status[page_n][i_page] |= (0xffff&i<<8) + sp->c_step%2;
  if (i >= np)
    {
      force_r->action_status[page_n][i_page] &= ~DATA_AVERAGING;
      if (im < sp->im[np-1] + sp->rec_n[np-1] + 512)
	return 0;  // we are nearly finished
      else
	{
	  fill_next_available_action(im+2, CHG_LED_VAL, sp->led_0);
	  fill_next_available_action(im+3, CHG_CAMERA_EXPO, sp->exp_0);
	  fill_next_available_action(im+4, CHG_CAMERA_GAIN, sp->gain_0);
	  snprintf(force_state_disp,sizeof(force_state_disp),"Force scan %d finished",n_force);
	  imr_TRACK_title_change_asked++;
	  force_r->action_status[page_n][i_page] &= ~DATA_AVERAGING;
	  working_gen_record = NULL;
	  if (force_r != NULL)	      force_r->n_record++;
	  n_force++;
	  Open_Pico_config_file();
	  set_config_float("Force_curve", "n_force", n_force);
	  write_Pico_config_file();
	  Close_Pico_config_file();
	  return 0;  // we have finished
	}
    }
  if (sp->c_step == 0)   // we move rotation
    {
      k = fill_next_available_action(im+1, MV_ROT_ABS, sp->rotation);
      if (k < 0)	win_printf_OK("Could not add pending action!");
      sp->c_step++;
    }
  if (sp->c_step == 1)   // we wait for rotation to stop moving and then start zmag
    {
      if (fabs(track_info->rot_mag[c_i] - sp->rotation) > 0.005)
	return 0;  // we are not yet arrived
      k = fill_next_available_action(im+1, MV_ZMAG_ABS, sp->zmag_start);
      if (k < 0)	win_printf_OK("Could not add pending action!");
      sp->c_step++;
    }
  if (sp->c_step >= 2 && sp->c_step%2 == 0)
    { // we wait for zmag to stop moving and then start acquisition
      snprintf(force_state_disp,sizeof(force_state_disp),"Force scan %d going to point %d Zmag %g",n_force,i,sp->zm[i]);
      imr_TRACK_title_change_asked++;
      if (fabs(track_info->zmag[c_i] - sp->zm[i]) > 0.005)
	return 0;  // we are not yet arrived
      if (sp->adj_focus)
	{
	  k = fill_next_available_action(im+1, MV_OBJ_ABS, sp->zo[i]);
	  if (k < 0)	win_printf_OK("Could not add pending action!");
	}
      fill_next_available_action(im+2, CHG_LED_VAL, sp->led[i]);
      fill_next_available_action(im+3, CHG_CAMERA_EXPO, sp->exp[i]);
      fill_next_available_action(im+4, CHG_CAMERA_GAIN, sp->gain[i]);

      sp->im[i] = im  + sp->setling; //force_r->abs_pos + sp->setling;
      sp->grim[i] = force_r->abs_pos + sp->setling;
      prof_index_avg = 0; n_avg = 0;
      sp->c_step++;
    }
  if (sp->c_step >= 2 && sp->c_step%2 == 1)
    { // we wait for the end of acquisition and start zmag
      if (suspended == 1) // we restart point averaging
	{
	  sp->im[i] = im  + sp->setling; //force_r->abs_pos + sp->setling;
	  sp->grim[i] = force_r->abs_pos + sp->setling;
	  suspended = 0;
	}
      if (im >= sp->im[i])
	{
	  force_r->action_status[page_n][i_page] |= DATA_AVERAGING;

	  //prof_index_avg += force_r->profile_index[page_n][i_page]; n_avg++;
	  snprintf(force_state_disp,sizeof(force_state_disp),"\\stack{{Force scan %d averaging point %d/%d}"
		   "{Zmag %6.3f Rot %6.3f Z_{obj} %6.3f}{(Pt. %d over %d)}{shutter %d \\mu s gain %g led %g}}",
		   n_force,i,np,sp->zm[i],sp->rotation, sp->zo[i],(im - sp->im[i]),sp->rec_n[i]
		   ,sp->exp[i],sp->gain[i],sp->led[i]);
	  imr_TRACK_title_change_asked++;
	}
      if (im < sp->im[i] + sp->rec_n[i]) 	return 0;  // we are not yet done
      force_r->action_status[page_n][i_page] &= ~DATA_AVERAGING;
      if (n_avg)  prof_index_avg /= n_avg;
      if (i+1 < np)
	{
	  k = fill_next_available_action(im+1, MV_ZMAG_ABS, sp->zm[i+1]);
	  if (k < 0)	win_printf_OK("Could not add pending action!");
	}
      else
	{
	  k = fill_next_available_action(im+1, MV_ZMAG_ABS, sp->zlow);
	  if (k < 0)	win_printf_OK("Could not add pending action!");
	}
      sp->c_step++;
    }

  return 0;
}




int record_force_record(void)
{
  int i, j, k, im, li, tmpi;
   imreg *imr = NULL;
   O_i *oi = NULL;
   b_track *bt = NULL;
   pltreg *pr = NULL;
   O_p *op = NULL;
   d_s *ds = NULL;
   static int nf = 2048, nstep = 33,  dead = 5, nper = 512, maxnper = 32768, n_nper, mod_expo_enable = 0;
   static int setling = 16, fir = 16, adj_focus = 0, min_expo = 250, n_expo_div , n_expo_div0;
   static float zmag_start = -0.2, zmag_step = -.125, zmag_finish = 9, lambdazmag = 1, zmag0 = 6,
     zmag_expo = -0.5, movie_rw = 1.0, movie_rh = 1.0;
   char question[4096] = {0}, name[64] = {0}, basename[512] = {0}, fullfilename[512] = {0};
   int nw, mod_expo = 0, i_expo = 0;
   float rot_start, z_tmp, tmp, gain, gled, zmag_tmp;
   static int with_radial_profiles = 0, n_maxnper, one_way = 0, streaming = 1, file_selection = 0, ortho_profiles = 0, movie_track = 0;
   static int special_saving = 0, rec_xy_prof = 0, rec_xy_diff_prof = 0, rec_small_image = 0, xyz_error = 1, xy_tracking_type;

   if(updating_menu_state != 0)
     {
       if (working_gen_record != NULL || track_info->n_b == 0)
	 active_menu->flags |=  D_DISABLED;
       else
	 {
           if (track_info->SDI_mode == 0) //tv : to record force curve in sdi config, ie without calib im
              {
                  for (i = 0, k = 0; i < track_info->n_b; i++)
                     {
                         bt = track_info->bd[i];
                         k += ( bt->calib_im != NULL) ? 1 : 0;//g_r->SDI_mode == 0 ||
                     }
              }
           else k = 1;
	   if (k) active_menu->flags &= ~D_DISABLED;
	   else active_menu->flags |=  D_DISABLED;
	 }
       return D_O_K;
     }

   if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)	return OFF;



   Open_Pico_config_file();
   n_force = get_config_int("Force_curve", "n_force", n_force);
   zmag_start = get_config_float("Force_curve", "zstart", -.5);
   zmag_step = get_config_float("Force_curve", "zstep", -0.125);
   nstep = get_config_int("Force_curve", "n_step", 9);
   nper = get_config_int("Force_curve", "nper", 1024);
   setling = get_config_int("Force_curve", "setling", 120);
   one_way = get_config_int("Force_curve", "one_way", 0);
   maxnper = get_config_int("Force_curve", "maxnper", 32768);
   dead = get_config_int("Force_curve", "dead", 32);
   zmag0 = get_config_float("Force_curve", "zmag0", -.9);
   zmag_expo = get_config_float("Force_curve", "zmag_expo",-0.5);
   mod_expo_enable = get_config_int("Force_curve", "mod_expo_enable",0);
   min_expo = get_config_int("Force_curve", "min_expo",250);
   lambdazmag = get_config_float("Force_curve", "lambdazmag", 4);
   zmag_finish = get_config_float("Force_curve", "zmag_after", -1);
   Close_Pico_config_file();


   rot_start = prev_mag_rot;
   zmag_start = prev_zmag;

   n_force = find_trk_index_not_used("Force_", n_force);


   if (current_write_open_path_picofile != NULL)
     {
       snprintf(fullfilename,sizeof(fullfilename),"%s\\Force_%d.trk",current_write_open_path_picofile,n_force);
       backslash_to_slash(fullfilename);
       file_selection = 0;
     }
   else
     {
       snprintf(fullfilename,sizeof(fullfilename),"Force_%d.trk",n_force);
       backslash_to_slash(fullfilename);
       file_selection = 1;
     }

   immediate_next_available_action(READ_GAIN_EXPO, 0);
   immediate_next_available_action(READ_LED_VAL, 0);


   static int ccc =0;
   for (i = j = k = im = 0; i < track_info->n_b; i++)
  {
       bt = track_info->bd[i];
       if (track_info->SDI_mode == 0)
       {
         if ((bt->calib_im == NULL) || (bt->not_lost <= 0) || (bt->in_image == 0)) continue;//g_r->SDI_mode == 0 &&
         if (bt->calib_im != NULL && bt->calib_im->n_yu > 2)
         {
           tmp = track_info->focus_cor*(bt->calib_im->yu[1]->ax - bt->calib_im->yu[2]->ax);
           if (fabs(tmp) < 0.5) j++; // a fixed bead
           else if (fabs(tmp - Pico_param.dna_molecule.dsDNA_molecule_extension)
           < (0.15 * Pico_param.dna_molecule.dsDNA_molecule_extension)) k++; // a good molecule bead
           else im++;
           if (k == 0 || im > 0)
           {
             win_printf("Among the %d beads to record, I have found :\n"
             "%d bead(s) attached by a molecule of correct length\n"
             "%d bead(s) attached by a molecule of incorrect length\n"
             "%d stuck bead(s). the force curves might not be pertinent for all beads!\n"
             "or you the molecule length entered in the present config file %g \\mu m\n"
             "might be incorrect!\n"
             ,track_info->n_b,k,im,j,Pico_param.dna_molecule.dsDNA_molecule_extension);
           }
         }
       }
       else
       {
         if (ccc==0) win_printf_OK("Force calibration requires zeros of molecules. \n In SDI instrument, please make sure this is done before");
         ccc++;
       }
  }

   min_expo = track_info->camera_max_exposure;
   if (track_info->camera_gain > 0) min_expo = (int) ((track_info->camera_max_gain*min_expo)/track_info->camera_gain);
   if (track_info->led_level > 0)   min_expo = (int)  ((100.0*min_expo)/track_info->led_level);

   //win_printf("Min expo = %d \\mu s", min_expo);
   for (n_expo_div = 0, i = 1; (track_info->camera_max_exposure/i) > min_expo; n_expo_div++, i *= 2);
   min_expo = track_info->camera_max_exposure/(2*i);
   n_expo_div++;

   //win_printf("camera max exposure %d actual gain %f actual led %f\nnb of exposition %d",track_info->camera_max_exposure,
   //      track_info->camera_gain,track_info->led_level, n_expo_div);

   for (n_nper = 0; (512 << n_nper) < nper; n_nper++);
   for (n_maxnper = 0; (4096 << n_maxnper) < maxnper; n_maxnper++);

   //  rot_start = what_is_present_rot_value();
   //zmag = read_magnet_z_value();
   snprintf(question,sizeof(question), "{\\pt12\\color{yellow} Starting a new Force curve recording by scanning Zmag by steps}\n"
	    "This function will create two new plot projets per active bead, containing :\n"
	    "\\oc  the x,y,z trajectories for each step \\oc  the force curves and related plots, \n"
	    "Z_{mag} present value is %g. Rotation position is %g optional new position%%6f \n"
	    "{\\color{yellow} Define Zmag Scanning} Starting at %%6f with %%6d steps of %%6f mm\n"
	    "Scan one way and back %%R or only one way %%r. Zmag to stand at the end %%8f\n"
	    "For each step, after Zmag has changed we wait %%6d frames to allow transient to vanish.\n"
	    //"Number of frames skipped after each force change %%8d\n"
	    "Adjust focus according to estimated force and to the %5.2f \\mu m length, NO%%R Yes%%r\n"
	    "{\\color{yellow} Optional Camera exposure modulation} For each high force point, you can use this to avoid camera\n"
	    "averaging. Do you want to modulate exposure No%%R by x2 steps%%r or just min and max %%r\n"
	    "   if yes, define Zmag threshold below which exposure is modulated %%8f\n"
	    "   and the Minimum exposure time in \\mu s %%8d possible.\n"
	    "{\\color{yellow} Step duration:} At high force, indicate the number of frames in one recording :\n"
	    "512%%R  1024%%r 2048%%r 4096%%r 8192%%r 16384%%r\n"
	    "To improve accuracy the recording time increases as force decreases.\n"
	    "When zmag becomes smaller than Z_{mag0}%%8f integration time increases\n"
	    "Following \\tau  = nper * 2^{(\\lambda Z_{mag0} - Z_{mag})}, \\lambda %%8f\n"
	    "However the integration time may be limited to a maximun value\n"
	    "define the maximum number of frames to record bead position at each step: \n"
	    "4096%%8R 8192%%r 16384%%r 32768%%r 65536%%r 131072%%r 262144%%r\n"
	    "{\\color{yellow} File Saving infos.} Track will be saved in :\n"
	    "\\fbox{%s\n"
	    "Do you want to select a new file location %%R No %%r Yes}\n"
	    "Do you want to keep plots corresponding to each acquisition No %%R Yes %%r\n"
	    "Click here %%b to access special saving options\n"
	    ,zmag_start,rot_start,Pico_param.dna_molecule.dsDNA_molecule_extension, fullfilename);

   i = win_scanf(question,&rot_start,&zmag_start,&nstep,&zmag_step,&one_way,&zmag_finish,&setling,&adj_focus
		 ,&mod_expo_enable,&zmag_expo,&min_expo,&n_nper,&zmag0,&lambdazmag,&n_maxnper,&file_selection
		 ,&keep_all_plots,&special_saving);	// ,&dead

   nper = 512 << n_nper;
   maxnper = 4096 << n_maxnper;

   if (i == WIN_CANCEL)	return OFF;

   if (special_saving)
     {
       snprintf(question,sizeof(question),"You can select here special saving options (increasing trk file size)\n"
		"Do you want to record bead profiles %%b\n"
		"Do you want to record bead orthoradial profiles %%b\n"
		"Do you want real time saving %%b\n"
		"Do you want to record bead xyz tracking error %%b\n"
		"Do you want to record bead xy tracking profiles %%b\n"
		"Do you want to record bead xy differential tracking profiles %%b\n"
		"Do you want to record bead image %%b (generates big trk file!)\n"
		"In this case define :\n"
		" \\oc  the ratio of bead image width with the cross arm %%6f\n"
		" \\oc  the ratio of bead image heigh with the cross arm %%6f\n"
		" \\oc  does the image center track the bead %%b\n");
       i = win_scanf(question,&with_radial_profiles,&ortho_profiles, &streaming, &xyz_error, &rec_xy_prof,
		     &rec_xy_diff_prof, &rec_small_image, &movie_rw, &movie_rh, &movie_track);
       if (i == WIN_CANCEL)	 return OFF;
     }


   for (n_expo_div = 0, i = 1; (track_info->camera_max_exposure/i) > min_expo; n_expo_div++, i *= 2);
   min_expo = track_info->camera_max_exposure/(2*i);
   n_expo_div++;

   n_expo_div0 = n_expo_div-1;
   if (mod_expo_enable == 2) n_expo_div = 2; // we only modulate between mean and max

   //win_printf("cam max expo %d gain %f led %f\nn expo %d",track_info->camera_max_exposure,
   //      track_info->camera_gain,track_info->led_level, n_expo_div);

   z_tmp = zmag_start + (nstep - 1) * zmag_step;
   if (z_tmp > absolute_maximum_zmag)
     {
       i = win_printf("You have specified zmag start = %g, with %d steps of %g\n"
		      "with these values your final zmag %g exceeds the safe limit %g mm\n"
		      "You might have made a mistake. Are you sure you want to persue ?\n"
		      ,zmag_start ,nstep ,zmag_step, z_tmp,absolute_maximum_zmag);
       if (i == WIN_CANCEL)	return OFF;
     }


   Open_Pico_config_file();
   set_config_float("Force_curve", "zstart", zmag_start);
   set_config_float("Force_curve", "zstep", zmag_step);
   set_config_int("Force_curve", "n_step", nstep);
   set_config_int("Force_curve", "nper", nper);
   set_config_int("Force_curve", "setling", setling);
   set_config_int("Force_curve", "one_way", one_way);
   set_config_int("Force_curve", "maxnper", maxnper);
   set_config_int("Force_curve", "dead", dead);
   set_config_float("Force_curve", "zmag0", zmag0);
   set_config_float("Force_curve", "lambdazmag", lambdazmag);
   set_config_float("Force_curve", "zmag_after", zmag_finish);
   set_config_float("Force_curve", "zmag_expo", zmag_expo);
   set_config_int("Force_curve", "mod_expo_enable",mod_expo_enable);
   set_config_int("Force_curve", "min_expo", min_expo);
   set_config_int("Force_curve", "n_expo", n_expo_div);
   set_config_float("Force_curve", "rotation", rot_start);
   write_Pico_config_file();
   Close_Pico_config_file();

   for (n_maxnper = 1; (2<<n_maxnper) < maxnper; n_maxnper++);




   #ifndef ZMAG_THREAD
   dead = (int)((Pico_param.camera_param.camera_frequency_in_Hz*zmag_step)/get_motors_speed());
   #else
   dead = (int)((Pico_param.camera_param.camera_frequency_in_Hz*zmag_step)/1);
   #endif

   nw = (one_way) ? 1 : 2;
   for (n_maxnper = 1; (2<<n_maxnper) < maxnper; n_maxnper++);



   for (i = 0, li = 0, k = 0, nf = 0; i < nw*nstep ; i++)
     {
       j = i;
       j %= 2*nstep;
       j = (j < nstep) ? j : 2 * nstep -1 - j;
       zmag_tmp = zmag_start + (j * zmag_step);
       for (k = 0, gled = 1.0, gain = track_info->camera_gain; k < n_expo_div; k++) // sp->gain_0
	 {
	   j = (int)(lambdazmag * (zmag0 - zmag_start - j * zmag_step));
	   j = (j <= n_maxnper) ? j : n_maxnper;
	   tmpi = (j > 0) ?  nper<<j : nper;
	   tmpi = (tmpi < maxnper) ? tmpi : maxnper;
	   nf += tmpi;
	   nf += dead;
	   li++;
	   if (k > 0) mod_expo++;
	   if ((mod_expo_enable == 0) || (zmag_tmp < zmag_expo))
	     {
	       break;
	     }
	 }
     }
   nf += 512;
   i = win_printf("Force curve will last %6.2f seconds (%d frames)\n"
		  "Pressing Cancel will abord acquisition",(float)nf/Pico_param.camera_param.camera_frequency_in_Hz,nf);
   if (i == WIN_CANCEL) return D_O_K;

   sp = (force_param*)calloc(1,sizeof(force_param));
   if (sp == NULL)  win_printf_OK("Could no allocte force parameters!");
   sp->im = (int*)calloc(n_expo_div*nw*nstep+1,sizeof(int));
   sp->grim = (int*)calloc(n_expo_div*nw*nstep+1,sizeof(int));
   sp->rec_n = (int*)calloc(n_expo_div*nw*nstep+1,sizeof(int));
   sp->zm = (float*)calloc(n_expo_div*nw*nstep+1,sizeof(float));
   sp->fm = (float*)calloc(n_expo_div*nw*nstep+1,sizeof(float));
   sp->zo = (float*)calloc(n_expo_div*nw*nstep+1,sizeof(float));
   sp->ro = (float*)calloc(n_expo_div*nw*nstep+1,sizeof(float));
   sp->gain = (float*)calloc(n_expo_div*nw*nstep+1,sizeof(float));
   sp->led = (float*)calloc(n_expo_div*nw*nstep+1,sizeof(float));
   sp->exp = (int*)calloc(n_expo_div*nw*nstep+1,sizeof(int));
   sp->i_zm = (int*)calloc(n_expo_div*nw*nstep+1,sizeof(int));
   sp->is_zm = (int*)calloc(n_expo_div*nw*nstep+1,sizeof(int));
   sp->nis_zm = (int*)calloc(n_expo_div*nw*nstep+1,sizeof(int));
   sp->i_mod_shut = (int*)calloc(n_expo_div*nw*nstep+1,sizeof(int));

   if (sp->im == NULL || sp->zm == NULL || sp->fm == NULL)
     win_printf_OK("Could no allocte force parameters!");


   //   win_printf_OK("f 1!");

   im = track_info->imi[track_info->c_i];
   tmp = Pico_param.dna_molecule.dsDNA_molecule_extension
     * extension_versus_alpha((zmag_to_force(0,zmag_start) * 50)/ (2.98 * 1.38));
   tmp /= track_info->focus_cor;
   //   win_printf("Mol ext at %g pN = %g microns/%g",zmag_to_force(0,zmag_start),tmp,
   //      Pico_param.dna_molecule.dsDNA_molecule_extension );

   sp->gain_0 = track_info->camera_gain;
   sp->led_0 = track_info->led_level;
   sp->exp_0 = track_info->camera_max_exposure;

   //   win_printf_OK("f 2!");

   for (i = 0, li = 0, k = 0, nf = 0, mod_expo = 0; i < nw*nstep ; i++)
     {
       j = i;
       j %= 2*nstep;
       j = (j < nstep) ? j : 2 * nstep -1 - j;
       zmag_tmp = zmag_start + (j * zmag_step);
       for (k = 0, gled = 1.0, gain = sp->gain_0; k < n_expo_div; k++)
	 {
	   sp->zm[li] = zmag_tmp;         // this is the Zmag position desired
	   sp->ro[li] = rot_start;        // this is the rotation position desired
	   sp->i_zm[li] = i;              // this is the zmag index
	   sp->is_zm[li] = k;             // this is the shutter index
	   sp->nis_zm[li] = n_expo_div;   // this is the number of shutter index at one zmag

	   sp->fm[li] = zmag_to_force(0, sp->zm[li]);
	   sp->im[li] = im + nf;
	   sp->zo[li] = prev_focus - tmp + (Pico_param.dna_molecule.dsDNA_molecule_extension
					   * extension_versus_alpha((sp->fm[li] * 50)/ (2.98 * 1.38)))/track_info->focus_cor;
	   sp->grim[li] = -1;
	   j = (int)(lambdazmag * (zmag0 - zmag_start - j * zmag_step));
	   j = (j <= n_maxnper) ? j : n_maxnper;
	   sp->rec_n[li] = (j > 0) ?  nper<<j : nper;
	   sp->rec_n[li] = (sp->rec_n[li] < maxnper) ? sp->rec_n[li] : maxnper;  // this is the number of points of the acq
	   nf += sp->rec_n[li];
	   nf += dead;
	   sp->exp[li] = track_info->camera_max_exposure/(1<<k);
	   sp->led[li] = gled * track_info->led_level;
	   sp->gain[li] = gain/gled;
	   if (sp->gain[li] > track_info->camera_max_gain)
	     sp->gain[li] = track_info->camera_max_gain;
	   gled *= 2;
	   if ((gled * track_info->led_level) > 100.0) gled = 100.0/track_info->led_level;
	   gain *= 2;
	   if (mod_expo_enable == 2 && k > 0)
	     {
	       sp->exp[li] = track_info->camera_max_exposure/(1<<n_expo_div0);
	       sp->led[li] = 100.0;
	       sp->gain[li] = track_info->camera_max_gain;
	     }
	   sp->i_mod_shut[li] = mod_expo;   // a verifier il faut peut etre mod_expo = 0 en d�but de boucle
	   if (k > 0) mod_expo++;
	   li++;
	   if ((mod_expo_enable == 0) || (zmag_tmp < zmag_expo))
	     {
	       if (li > 0)
		 {
		   sp->nis_zm[li-1] = k+1;
		   sp->i_mod_shut[li-1] = -1;
		 }
	       //win_printf("exiting expo loop zmagtmp %g zmagexpo %g li %d k %d",zmag_tmp, zmag_expo, li, k);
	       break;
	     }
	 }
     }
   /*
   sp->zm[li] = (one_way) ? (zmag_start + (nstep * zmag_step)) : zmag_start;
   sp->fm[li] = zmag_to_force(0, sp->zm[i]);
   sp->im[li] = im + nf;
   sp->zo[li] = prev_focus;
   sp->exp[li] = sp->exp_0;
   sp->led[li] = sp->led_0;
   sp->gain[li] = sp->gain_0;

   sp->rec_n[li] = nper;
   sp->nb_acq_pt = li+1;
   */
   sp->nb_acq_pt = li;
   //win_printf("Nb of points %d",sp->nb_acq_pt);
   nf += 512;
   sp->nf = nf;
   sp->nstep = nstep;
   sp->dead = dead;
   sp->nper = nper;
   sp->maxnper = maxnper;
   sp->setling = setling;
   sp->one_way = one_way;
   sp->c_step = 0;
   sp->fir = fir;
   sp->rotation = rot_start;
   sp->zmag_hi_fc = zmag_expo;
   sp->mod_expo_enable = mod_expo_enable;
   sp->zmag_start = zmag_start;
   sp->zmag_step = zmag_step;
   sp->zlow = zmag_finish;
   //   sp->z_cor = track_info->focus_cor;
   sp->lambdazmag = lambdazmag;
   sp->zmag0 = zmag0;
   sp->adj_focus = adj_focus;

   //   win_printf_OK("f 3!");

   snprintf(name,64,"Force scan %d",n_force);

   // We create a plot region to display bead position, scan  etc.
   //pr = create_hidden_pltreg_with_op(&op, nf, nf, 0,name);
   //if (pr == NULL)  win_printf_OK("Could not find or allocte plot region!");
   //ds = op->dat[0];
   //ds->xd[1] = ds->yd[1] = 0.1;
   im = track_info->imi[track_info->c_i];
   im += 25;

   sp->fir = fir;

   xy_tracking_type = (rec_xy_prof) ? XY_BEAD_PROFILE_RECORDED : 0;
   xy_tracking_type |= (rec_xy_diff_prof) ? XY_BEAD_DIFF_PROFILE_RECORDED : 0;
   xy_tracking_type |= (rec_small_image) ? RECORD_BEAD_IMAGE : 0;
   xy_tracking_type |= (xyz_error) ? XYZ_ERROR_RECORDED : 0;

   im = track_info->imi[track_info->c_i];
   force_r = create_gen_record(track_info, 1, with_radial_profiles,0,1,xy_tracking_type,movie_rw, movie_rh, movie_track);
   duplicate_Pico_parameter(&Pico_param, &(force_r->Pico_param_record));
   if (extract_file_name(force_r->filename, sizeof(force_r->filename), fullfilename) == NULL)
     snprintf(force_r->filename, sizeof(force_r->filename), "Force_%d.trk",n_force);
   force_r->record_action = force_action;
   force_action(0,0,0,1);
   force_r->data_type |= FORCE_CURVE;

   for (i = 0; i < force_r->n_bead; i++)
     {
       force_r->b_r[i]->s_r = (stiffness_resu*)calloc(sp->nb_acq_pt,sizeof(stiffness_resu));
       if (force_r->b_r[i]->s_r == NULL)
	 return win_printf_OK("Could allocate space for stiffness result!");
       force_r->b_r[i]->n_s_r = force_r->b_r[i]->m_s_r = sp->nb_acq_pt;
       force_r->b_r[i]->c_s_r = 0;
     }

   //   win_printf_OK("f 4!");
   if (streaming)
     {
       if (current_write_open_path_picofile != NULL && file_selection == 0)
	 {
	   strncpy(force_r->path,current_write_open_path_picofile,sizeof(force_r->path));
	   write_record_file_header(force_r, oi_TRACK);
	   force_r->real_time_saving = 1;
	 }
       else if (do_save_track(force_r))
	 win_printf("Pb writing tracking header");
     }

   //   win_printf_OK("f 5!");

   if (current_write_open_path_picofile != NULL)
     {
       dump_to_html_log_file_with_date_and_time(current_write_open_picofile,
						"<h2>Force curve recording</h2>"
						"<br>in file %s in directory %s"
						"<br>zmag start %g, %d sets of %g lasting %d frames after %d frames to settle <br>"
						"<br<%d way(s)"
						"Zmag0 %g lambdazmag %g maxnper %d<br>"
						"Rotation %g<br>\n"
						,backslash_to_slash(force_r->filename),backslash_to_slash(force_r->path)
						,zmag_start,nstep,zmag_step,nper,dead,(one_way) ? 1 : 2,zmag0,lambdazmag
						,maxnper,rot_start);
    }



   li = grab_basename_and_index(force_r->filename, strlen(force_r->filename)+1, basename, 256);
   n_force = (li >= 0) ? li : n_force;
   force_r->n_rec = n_force;


   force_state = FORCE_RUNNING;

   /////////////////////////////////


   //   win_printf_OK("f 6!");
   for (i = 0; i < track_info->n_b; i++)
     {
       bt = track_info->bd[i];
       if ((bt->calib_im == NULL) || (bt->not_lost <= 0) || (bt->in_image == 0))
	 continue;//g_r->SDI_mode == 0 &&
       snprintf(name,64,"Force recordings %d bead %d",n_force,i);
       pr = create_hidden_pltreg_with_op(&op, nf, nf, 0,name);
       if (pr == NULL)  win_printf_OK("Could not find or allocte plot region!");
       //we create one plot for each bead
       if (attach_g_record_to_pltreg(pr, force_r))
	 win_printf("Could not attach grecord to plot region!");

       op = pr->one_p;
       ds = op->dat[0];
       ds->nx = ds->ny = 0;
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
		     "X coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_force,i
		     ,bt->cl,bt->cw,nf
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,pico_sample
		     ,what_is_present_rot_value()
		     ,what_is_present_z_mag_value()
		     ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");


       if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
	 return win_printf_OK("I can't create plot !");
       ds->nx = ds->ny = 0;
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
		     "Y coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_force,i
		     ,bt->cl,bt->cw,nf
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,pico_sample
		     ,what_is_present_rot_value()
		     ,what_is_present_z_mag_value()
		     ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");
       if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
	 return win_printf_OK("I can't create plot !");
       ds->nx = ds->ny = 0;
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
		     "Z coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_force,i
		     ,bt->cl,bt->cw,nf
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,pico_sample
		     ,what_is_present_rot_value()
		     ,what_is_present_z_mag_value()
		     ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");

       //uns_oi_2_op(oi, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
       create_attach_select_y_un_to_op(op, IS_METER, 0, 1, -6, 0, "\\mu m");
       create_attach_select_x_un_to_op(op, IS_SECOND, 0
				       ,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");

       op->op_idle_action = force_op_idle_action; //xyz_scan_idle_action;
       op->op_post_display = force_op_post_display;
       set_op_filename(op, "X(t)Y(t)Z(t)bd%dforce%d-0%f.gr",i,n_force,what_is_present_z_mag_value());
       set_plot_title(op, "Bead %d trajectory %d-0", i,n_force);
       set_plot_x_title(op, "Time");
       set_plot_y_title(op, "Position");

       op->x_lo = (im-nf/64);
       op->x_hi = (im+nf + nf/64);
       set_plot_x_fixed_range(op);
       op->user_ispare[ISPARE_BEAD_NB] = i;
       /*
       if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
	 return win_printf_OK("I can't create plot !");
       ds->nx = ds->ny = 0;
       */
       // this is the global xyz recording during the entire process
       fill_next_available_job_spot(im, im + 32, COLLECT_DATA, i, imr, oi,
				    pr, op, (void*)sp, force_record_job_xyz, 0);


       op = create_and_attach_one_plot(pr, nper,  nper, 0);
       op = pr->one_p;
       ds = op->dat[0];
       ds->nx = ds->ny = 0;
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
		     "X coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_force,i
		     ,bt->cl,bt->cw,nf
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,pico_sample
		     ,what_is_present_rot_value()
		     ,what_is_present_z_mag_value()
		     ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");


       if ((ds = create_and_attach_one_ds(op, nper, nper, 0)) == NULL)
	 return win_printf_OK("I can't create plot !");
       ds->nx = ds->ny = 0;
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
		     "Y coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_force,i
		     ,bt->cl,bt->cw,nf
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,pico_sample
		     ,what_is_present_rot_value()
		     ,what_is_present_z_mag_value()
		     ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");
       if ((ds = create_and_attach_one_ds(op, nper, nper, 0)) == NULL)
	 return win_printf_OK("I can't create plot !");
       ds->nx = ds->ny = 0;
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
		     "Z coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_force,i
		     ,bt->cl,bt->cw,nf
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,pico_sample
		     ,what_is_present_rot_value()
		     ,what_is_present_z_mag_value()
		     ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");

       //uns_oi_2_op(oi, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
       create_attach_select_y_un_to_op(op, IS_METER, 0, 1, -6, 0, "\\mu m");
       create_attach_select_x_un_to_op(op, IS_SECOND, 0
				       ,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");

       op->op_idle_action = force_op_idle_action; //xyz_scan_idle_action;
       op->op_post_display = force_op_post_display;
       op->user_ispare[ISPARE_BEAD_NB] = i;
       set_op_filename(op, "X(t)Y(t)Z(t)bd%dforce%d-0.gr",i,n_force);
       set_plot_title(op, "\\stack{{Bead %d single trajectory %d-0}{Zmag %g shutter %d \\mu s}}",
		      i,n_force,sp->zm[0],sp->exp[0]);
       set_plot_x_title(op, "Time");
       set_plot_y_title(op, "Position");


       /*
       if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
	 return win_printf_OK("I can't create plot !");
       ds->nx = ds->ny = 0;
       */
       // this is the xyz recording of a ingle point
       fill_next_available_job_spot(im, im + 32, COLLECT_DATA, i, imr, oi,
				    pr, op, (void*)sp, force_record_job_single_xyz, 0);



       // We create a plot region to display bead position, force  etc.
       snprintf(name,64,"Force curve %d bead %d",n_force,i);
       pr = create_hidden_pltreg_with_op(&op, sp->nb_acq_pt+1, sp->nb_acq_pt+1, 0,name);
       if (pr == NULL)  win_printf_OK("Could not find or allocte plot region!");
       //we create one plot for each bead
       if (attach_g_record_to_pltreg(pr, force_r))
	 win_printf("Could not attach grecord to plot region!");

       if (bt->fixed_bead == 0)
	 {
	   set_plot_title(op, "Beads %d Force curve %d, rot %f Zmag", i, n_force,sp->rotation);
	   set_plot_x_title(op,"Extension (\\mu m)");
	   set_plot_y_title(op,"Force (pN)");
	   set_plot_file(op,"Forcebd%01d%03d%s",i,n_force,".gr");
	   ds = op->dat[0];
	   ds->nx = ds->ny = 0;
	   set_plot_symb(ds, "\\pt5\\oc");
	   set_dot_line(ds);
	   set_ds_source(ds,"DNA force extension curve f_x 3D length bead %d\n",i);
	   ds = create_and_attach_one_ds(op, sp->nb_acq_pt+1,sp->nb_acq_pt+1, 0);
	   if (ds == NULL)  win_printf_OK("cannot allocate ds!");
	   ds->nx = ds->ny = 0;
	   set_plot_symb(ds, "\\pt5\\oc");
	   set_dot_line(ds);
	   set_ds_source(ds,"DNA force extension curve f_y 3D length bead %d\n",i);
	   op->op_idle_action = force_op_idle_action;
	   op->op_post_display = force_op_post_display;
	   op->user_ispare[ISPARE_BEAD_NB] = i;
	   fill_next_available_job_spot(1, sp->im[1], COLLECT_DATA, i, imr, oi, pr, op, (void*)sp, force_job_record_scanf,0);

	   op = create_and_attach_one_plot(pr, sp->nb_acq_pt + 1,  sp->nb_acq_pt + 1, 0);
	   ds = op->dat[0];
	   ds->nx = ds->ny = 0;
	   set_dot_line(ds);
	   set_plot_symb(ds, "\\pt5\\oc");
	   set_plot_title(op, "Beads %d Stiffness %d, rot %f", i, n_force,sp->rotation);
	   set_plot_x_title(op,"Acquisition Nb.");
	   set_plot_y_title(op,"K_x and K_y (10^{-6}N/m)");
	   set_plot_file(op,"KxKybd%01d%03d%s",i,n_force,".gr");

	   set_ds_source(ds,"k_x Stiffnes\nZ mag start = %g for %d frames scan by steps of %g"
			 " for %d steps %d way(s)\nrot = %g dead period %d\n"
			 "Bead  calbration %s \n"
			 ,zmag_start,nper,zmag_step,nstep,nw,what_is_present_rot_value()
			 ,dead,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");

	   if ((ds = create_and_attach_one_ds(op, sp->nb_acq_pt + 1,  sp->nb_acq_pt + 1, 0)) == NULL)
	     return win_printf_OK("I can't create plot !");
	   ds->nx = ds->ny = 0;
	   set_dot_line(ds);
	   set_plot_symb(ds, "\\pt5\\oc");
	   set_ds_source(ds,"k_y Stiffnes\nZ mag start = %g for %d frames scan by steps of %g"
			 " for %d steps %d way(s)\nrot = %g dead period %d\n"
			 "Bead  calbration %s \n"
			 ,zmag_start,nper,zmag_step,nstep,nw,what_is_present_rot_value()
			 ,dead,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");


	   op->op_idle_action = force_op_idle_action;
	   op->op_post_display = force_op_post_display;
	   fill_next_available_job_spot(1, sp->im[1]+1, COLLECT_DATA, i, imr, oi, pr, op, (void*)sp, force_job_kxky,0);


	   op = create_and_attach_one_plot(pr, sp->nb_acq_pt + 1,  sp->nb_acq_pt + 1, 0);
	   ds = op->dat[0];
	   ds->nx = ds->ny = 0;
	   set_dot_line(ds);
	   set_plot_symb(ds, "\\pt5\\oc");
	   set_plot_title(op, "Beads %d F_c %d, rot %f", i, n_force,sp->rotation);
	   set_plot_x_title(op,"Acquisition Nb.");
	   set_plot_y_title(op,"F_c (Hz), x , y");
	   set_plot_file(op,"Fcbd%01d%03d%s",i,n_force,".gr");

	   set_ds_source(ds,"F_cx \nZ mag start = %g for %d frames scan by steps of %g"
			 " for %d steps %d way(s)\nrot = %g dead period %d\n"
			 "Bead  calbration %s \n"
			 ,zmag_start,nper,zmag_step,nstep,nw,what_is_present_rot_value()
			 ,dead,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");

	   if ((ds = create_and_attach_one_ds(op, sp->nb_acq_pt + 1,  sp->nb_acq_pt + 1, 0)) == NULL)
	     return win_printf_OK("I can't create plot !");
	   ds->nx = ds->ny = 0;
	   set_dot_line(ds);
	   set_plot_symb(ds, "\\pt5\\oc");
	   set_ds_source(ds,"F_cy \nZ mag start = %g for %d frames scan by steps of %g"
			 " for %d steps %d way(s)\nrot = %g dead period %d\n"
			 "Bead  calbration %s \n"
			 ,zmag_start,nper,zmag_step,nstep,nw,what_is_present_rot_value()
			 ,dead,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");


	   op->op_idle_action = force_op_idle_action;
	   op->op_post_display = force_op_post_display;
	   fill_next_available_job_spot(1, sp->im[1]+1, COLLECT_DATA, i, imr, oi, pr, op, (void*)sp, force_job_fcxy,0);




	   if (mod_expo)
	     {
	       for(i_expo = 0; i_expo < sp->nb_acq_pt; i_expo++)
		   if (sp->i_zm[i_expo] == 0) break;

	       op = create_and_attach_one_plot(pr, sp->n_expo_div+4,  sp->n_expo_div+4, 0);
	       ds = op->dat[0];
	       ds->nx = ds->ny = 0;
	       set_dot_line(ds);
	       set_plot_symb(ds, "\\pt5\\oc");
	       set_plot_title(op, "\\stack{{X Variance of fluctuations}{\\pt8 Beads %d curve %d, rot %f}}"
			      , i, n_force,sp->rotation);
	       set_plot_x_title(op,"Camera exposure (\\mu s)");
	       set_plot_y_title(op,"<x - <x>>^2 (nm^2)");
	       set_plot_file(op,"dx2bd%01d%03d%s",i,n_force,".gr");

	       set_ds_source(ds,"X variance point series %d bead %d\nZ mag = %g rot %g"
			     ,sp->i_zm[i_expo],i,sp->zm[i_expo],sp->ro[i_expo]);

	       op->op_idle_action = force_op_idle_action;
	       op->op_post_display = force_op_post_display;
	       fill_next_available_job_spot(1, sp->im[1]+1, COLLECT_DATA, i, imr, oi, pr, op, (void*)sp, force_job_wong_x,0);


	       op = create_and_attach_one_plot(pr, sp->n_expo_div+4,  sp->n_expo_div+4, 0);
	       ds = op->dat[0];
	       ds->nx = ds->ny = 0;
	       set_dot_line(ds);
	       set_plot_symb(ds, "\\pt5\\oc");
	       set_plot_title(op, "\\stack{{Y Variance of fluctuations}{\\pt8 Beads %d curve %d, rot %f}}"
			      , i, n_force,sp->rotation);
	       set_plot_x_title(op,"Camera exposure (\\mu s)");
	       set_plot_y_title(op,"<y - <y>>^2 (nm^2)");
	       set_plot_file(op,"dy2bd%01d%03d%s",i,n_force,".gr");
	       set_ds_source(ds,"Y variance point series %d bead %d\nZ mag = %g rot %g"
			     ,sp->i_zm[i_expo],i,sp->zm[i_expo],sp->ro[i_expo]);

	       op->op_idle_action = force_op_idle_action;
	       op->op_post_display = force_op_post_display;
	       fill_next_available_job_spot(1, sp->im[1]+1, COLLECT_DATA, i, imr, oi, pr, op, (void*)sp, force_job_wong_y,0);

	     }

	   op = create_and_attach_one_plot(pr, sp->nb_acq_pt+1,  sp->nb_acq_pt+1, 0);
	   ds = op->dat[0];
	   ds->nx = ds->ny = 0;
	   set_dot_line(ds);
	   set_plot_symb(ds, "\\pt5\\oc");
	   set_ds_source(ds,"F_x measured compared with estimated \nZ mag start = %g for %d frames scan by steps of %g"
			 " for %d steps %d way(s)\nrot = %g dead period %d\n"
			 "Bead  calbration %s \n"
			 ,zmag_start,nper,zmag_step,nstep,nw,what_is_present_rot_value()
			 ,dead,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");
	   if ((ds = create_and_attach_one_ds(op, sp->nb_acq_pt + 1,  sp->nb_acq_pt + 1, 0)) == NULL)
	     return win_printf_OK("I can't create plot !");
	   ds->nx = ds->ny = 0;
	   set_dot_line(ds);
	   set_plot_symb(ds, "\\pt5\\oc");
	   set_ds_source(ds,"F_y measured compared with estimated \nZ mag start = %g for %d frames scan by steps of %g"
			 " for %d steps %d way(s)\nrot = %g dead period %d\n"
			 "Bead  calbration %s \n"
			 ,zmag_start,nper,zmag_step,nstep,nw,what_is_present_rot_value()
			 ,dead,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");
	   set_plot_title(op, "\\stack{{Beads %d rotation %g Force comparison %d}}", i, sp->rotation, n_force);
	   set_plot_y_title(op,"Measured force F_x (pN)");
	   set_plot_x_title(op,"Estimated force (pN)");
	   set_plot_file(op,"ForceCompbd%01d%03d%s",i,n_force,".gr");


	   op->op_idle_action = force_op_idle_action;
	   op->op_post_display = force_op_post_display;
	   op->user_ispare[ISPARE_BEAD_NB] = i;
	   j =  nper;
	   // there is a bug in scan_job_scan
	   fill_next_available_job_spot(im, sp->im[1] + 40, COLLECT_DATA, i, imr, oi, pr, op, (void*)sp, force_record_job_comp_f, 0);




	   op->user_ispare[ISPARE_BEAD_NB] = i;
	   op = create_and_attach_one_plot(pr, sp->nb_acq_pt + 1,  sp->nb_acq_pt + 1, 0);
	 }
       ds = op->dat[0];
       ds->nx = ds->ny = 0;
       //set_dot_line(ds);
       set_plot_symb(ds, "\\pt5\\oc");
       set_plot_title(op, "Beads %d Force %d, rot %f", i, n_force,sp->rotation);
       set_plot_x_title(op,"Length (\\mu m)");
       set_plot_y_title(op,"Z_{mag} (mm)");
       set_plot_file(op,"scanzbd%01d%03d%s",i,n_force,".gr");

       set_ds_source(ds,"Scan Z\nZ mag start = %g for %d frames scan by steps of %g"
		     " for %d steps %d way(s)\nrot = %g dead period %d\n"
		     "Bead  calbration %s \n"
		     ,zmag_start,nper,zmag_step,nstep,nw,what_is_present_rot_value()
		     ,dead,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");
       op->op_idle_action = force_op_idle_action;
       op->op_post_display = force_op_post_display;
       op->user_ispare[ISPARE_BEAD_NB] = i;
       fill_next_available_job_spot(1, sp->im[1]+1, COLLECT_DATA, i, imr, oi, pr, op, (void*)sp, force_record_job_scan,0);








     }


   im = track_info->imi[track_info->c_i];
   working_gen_record = force_r;
   force_r->last_starting_pos = im;
   win_title_used = 1;
   if (imr_and_pr && pr != NULL) switch_project_to_this_pltreg(pr);
   generate_imr_TRACK_title = generate_force_title;
   //   win_printf_OK("f 7!");
 return D_O_K;
}


int stop_force(void)
{
  pltreg *pr = NULL;
  g_record *g_r = NULL;

  ac_grep(cur_ac_reg,"%pr",&pr);
  g_r = (pr != NULL) ? find_g_record_in_pltreg(pr) : working_gen_record;

  if(updating_menu_state != 0)
    {
      if (working_gen_record == NULL || g_r != working_gen_record)
	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }
  if (working_gen_record != NULL)
    {
      snprintf(force_state_disp,sizeof(force_state_disp),"Force scan %d stoped",n_force);
      imr_TRACK_title_change_asked++;
      working_gen_record = NULL;
      if (force_r != NULL)
	{
	  force_r->n_record++;
	}
    }
  ask_to_update_menu();
  return D_O_K;
}


int suspend_force(void)
{
  pltreg *pr = NULL;
  g_record *g_r = NULL;

  ac_grep(cur_ac_reg,"%pr",&pr);
  g_r = (pr != NULL) ? find_g_record_in_pltreg(pr) : working_gen_record;

  if(updating_menu_state != 0)
    {
      if (force_state != FORCE_RUNNING && force_state != FORCE_SUSPENDED)
	active_menu->flags |=  D_DISABLED;
      else if (working_gen_record == NULL || g_r != working_gen_record)
	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }
  if (working_gen_record != NULL)
    {
      if (force_state == FORCE_RUNNING)
	{
	  force_state = FORCE_SUSPENDED;
	  active_menu->text = "Restart";
	}
      else if (force_state == FORCE_SUSPENDED)
	{
	  force_state = FORCE_RUNNING;
	  active_menu->text = "Suspend";
	}
    }
  ask_to_update_menu();
  return D_O_K;
}




int draw_force_XYZ_trajectories(void)
{
  int i, j, page_n, i_page;
  b_track *bt = NULL;
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *ds = NULL;
  int nf;
  float rot_start = 10,  zmag;
  b_record *br = NULL;
  g_record *g_r = NULL;

  ac_grep(cur_ac_reg,"%pr",&pr);
  g_r = find_g_record_in_pltreg(pr);
  if(updating_menu_state != 0)
    {  // we wait for recording finish
      if (working_gen_record != NULL || force_r == NULL)
	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }
  if (pr == NULL)return 0;

  if (g_r == NULL || g_r->n_record == 0) return OFF;
  nf = g_r->abs_pos;
  if (nf <= 0) return OFF;

  rot_start = g_r->rot_mag[0][0];
  zmag = g_r->zmag[0][0];

  //we create one plot for each bead
  for (i = 0; i < g_r->n_bead; i++)
    {
      br = g_r->b_r[i];
      bt = br->b_t;
      if (g_r->SDI_mode == 0 && bt->calib_im == NULL)	continue;
      op = create_and_attach_one_plot(pr, nf, nf, 0);
      ds = op->dat[0];
      set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
		    "X coordinate l = %d, w = %d, nim = %d\n"
		    "objective %f, zoom factor %f, sample %s Rotation %g\n"
		    "Z magnets %g mm\n"
		    "Calibration from %s"
		    ,Pico_param.camera_param.camera_frequency_in_Hz,n_force,i
		    ,track_info->cl,track_info->cw,nf
		    ,Pico_param.obj_param.objective_magnification
		    ,Pico_param.micro_param.zoom_factor
		    ,pico_sample
		    ,rot_start
		    ,zmag
		    ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");
      for (j = 0; j < nf; j++)
	{
	  page_n = j/g_r->page_size;
	  i_page = j%g_r->page_size;
	  ds->xd[j] = g_r->imi[page_n][i_page];
	  ds->yd[j] = g_r->ax + g_r->dx * br->x[page_n][i_page];
	}

       if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
	 return win_printf_OK("I can't create plot !");
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
		     "Y coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_force,i
		     ,track_info->cl,track_info->cw,nf
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,pico_sample
		     ,rot_start
		     ,zmag
		     ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");
      for (j = 0; j < nf; j++)
	{
	  page_n = j/g_r->page_size;
	  i_page = j%g_r->page_size;
	  ds->xd[j] = g_r->imi[page_n][i_page];
	  ds->yd[j] = g_r->ay + g_r->dy * br->y[page_n][i_page];
	}
       if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
	 return win_printf_OK("I can't create plot !");
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
		     "Z coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_force,i
		     ,track_info->cl,track_info->cw,nf
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,pico_sample
		     ,rot_start
		     ,zmag
		     ,(bt->calib_im != NULL) ? bt->calib_im->filename : "Not defined");
      for (j = 0; j < nf; j++)
	{
	  page_n = j/g_r->page_size;
	  i_page = j%g_r->page_size;
	  ds->xd[j] = g_r->imi[page_n][i_page];
	  ds->yd[j] = g_r->z_cor * br->z[page_n][i_page];
	}
       create_attach_select_y_un_to_op(op, IS_METER, 0 ,(float)1, -6, 0, "\\mu m");
       create_attach_select_x_un_to_op(op, IS_SECOND, 0
				       ,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");


       set_op_filename(op, "X(t)Y(t)Z(t)bd%dforce%d.gr",i,g_r->n_rec);
       set_plot_title(op, "Bead %d Z(t) %d zmag %g", i,g_r->n_rec,zmag);
       set_plot_x_title(op, "Time");
       set_plot_y_title(op, "Position");
       op->need_to_refresh = 1;
     }
  return refresh_plot(pr, pr->n_op - 1);
}

int draw_force_trajectories_params(void)
{
  int  j,  page_n, i_page;
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *ds = NULL;
  int nf;
  g_record *g_r = NULL;

  ac_grep(cur_ac_reg,"%pr",&pr);
  g_r = find_g_record_in_pltreg(pr);
  if(updating_menu_state != 0)
    {  // we wait for recording finish
      if (g_r == NULL || working_gen_record == g_r)
	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }

  if (pr == NULL || g_r == NULL || g_r->n_record == 0) return OFF;
  nf = g_r->abs_pos;
  if (nf <= 0) return OFF;

  //we create one plot for each bead
  op = create_and_attach_one_plot(pr, nf, nf, 0);
  ds = op->dat[0];
  set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d  \n "
		"Zmag l = %d, w = %d, nim = %d\n"
		"objective %f, zoom factor %f, sample %s\n"
		,Pico_param.camera_param.camera_frequency_in_Hz,n_force
		,track_info->cl,track_info->cw,nf
		,Pico_param.obj_param.objective_magnification
		,Pico_param.micro_param.zoom_factor
		,pico_sample);


  for (j = 0; j < nf; j++)
    {
      page_n = j/g_r->page_size;
      i_page = j%g_r->page_size;
      ds->xd[j] = g_r->imi[page_n][i_page];
      ds->yd[j] = g_r->zmag[page_n][i_page];
    }

  if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("I can't create plot !");
  set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d  \n "
		"magnets rotation l = %d, w = %d, nim = %d\n"
		"objective %f, zoom factor %f, sample %s\n"
		,Pico_param.camera_param.camera_frequency_in_Hz,n_force
		,track_info->cl,track_info->cw,nf
		,Pico_param.obj_param.objective_magnification
		,Pico_param.micro_param.zoom_factor
		,pico_sample);

  for (j = 0; j < nf; j++)
    {
      page_n = j/g_r->page_size;
      i_page = j%g_r->page_size;
      ds->xd[j] = g_r->imi[page_n][i_page];
      ds->yd[j] = g_r->rot_mag[page_n][i_page];
    }
  if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("I can't create plot !");
  set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d \n "
		"Objective position l = %d, w = %d, nim = %d\n"
		"objective %f, zoom factor %f, sample %s\n"
		,Pico_param.camera_param.camera_frequency_in_Hz,n_force
		,track_info->cl,track_info->cw,nf
		,Pico_param.obj_param.objective_magnification
		,Pico_param.micro_param.zoom_factor
		,pico_sample);


  for (j = 0; j < nf; j++)
    {
      page_n = j/g_r->page_size;
      i_page = j%g_r->page_size;
      ds->xd[j] = g_r->imi[page_n][i_page];
      ds->yd[j] = g_r->obj_pos[page_n][i_page];
    }
  create_attach_select_x_un_to_op(op, IS_SECOND, 0
				  ,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");


  set_op_filename(op, "ZmagRotFocusforce%d.gr",g_r->n_rec);
  set_plot_title(op, "Force scan %d zmag, rotation & focus ", g_r->n_rec);
  set_plot_x_title(op, "Time");
  set_plot_y_title(op, "Zmag, rot, focus");

  op->need_to_refresh = 1;
  return refresh_plot(pr, pr->n_op - 1);
}




MENU *force_menu(void)
{
	static MENU mn[32] = {0};

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn, "record force", record_force_record, NULL, 0, NULL);
	add_item_to_menu(mn, "stop force", stop_force, NULL, 0, NULL);
	add_item_to_menu(mn, "suspend force", suspend_force, NULL, 0, NULL);
	add_item_to_menu(mn,"\0",	NULL,	NULL,	0, NULL);

	return mn;
}

MENU *force_plot_menu(void)
{
	static MENU mn[32] = {0};

	if (mn[0].text != NULL)	return mn;
	//	add_item_to_menu(mn, "record Z(t)", record_zdet, NULL, 0, NULL);
	add_item_to_menu(mn, "stop force", stop_force, NULL, 0, NULL);
	add_item_to_menu(mn, "suspend force", suspend_force, NULL, 0, NULL);


	add_item_to_menu(mn,"\0",	NULL,	NULL,	0, NULL);
	add_item_to_menu(mn, "Dump XYZ", draw_force_XYZ_trajectories, NULL, 0, NULL);
	add_item_to_menu(mn, "Dump Zmag Rot Focus", draw_force_trajectories_params, NULL, 0, NULL);


	return mn;
}


# endif
