#ifndef _SAVE_MOVIE_UTIL_C_
#define _SAVE_MOVIE_UTIL_C_

# include "allegro.h"
#ifdef XV_WIN32
# include "winalleg.h"
#endif

# include "xvin.h"


/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "../cfg_file/Pico_cfg.h"
#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "../fft2d/fftbtl32n.h"
# include "fillibbt.h"
# include "focus.h"
# include "magnetscontrol.h"
# include "trackBead.h"
# include "brown_util.h"
# include "track_util.h"
# include "action.h"
# include "calibration.h"
# include "save_movie_util.h"
# include "timer_thread.h"

# include <pthread.h>

# ifndef TRACK_WIN_THREAD
# include "timer_thread.h"
timer_thread_t g_auto_time_lapse_timer;
# endif

char title[512];
char fullfile_time_lapse[512];
int iNumber_of_images_to_save = 128, iNumber_of_s_between_frames = 60, images_saved_for_time_lapse = 0;
O_i *time_lapse_movie_oi = NULL;
////////////////////////////////////////////////////////////////////////////////////////////////////
/////                 Save finite movie synchronized in the tracking thread through bid          ///
///////////////////////////////////////////////////////////////////////////////////////////////////

int save_finite_movie_in_tracking_thread(imreg *imr, O_i *oi, int n, DIALOG *d, long long t, unsigned long dt, void *p,
        int n_inarow)
{
    static int i = 0;
    O_i *oi_movie = (O_i *)p;

  (void)imr;
  (void)oi;
  (void)n;
  (void)d;
  (void)t;
  (void)dt;
  (void)n_inarow;
    snprintf(title, 512, "saving image %d on %d", i, oi_movie->im.n_f);
    my_set_window_title("%s",title);

    if (i < oi_movie->im.n_f)
    {
        memcpy(oi_movie->im.mem[i], oi_TRACK->im.mem[track_info->imi[track_info->c_i] % oi_TRACK->im.n_f],
               oi_TRACK->im.nx * oi_TRACK->im.ny * ((oi_TRACK->im.data_type == IS_CHAR_IMAGE) ? 1 : 2));
        i++;
        snprintf(title, 512, "saving image %d on %d", i, oi_movie->im.n_f);
	my_set_window_title("%s",title);
    }
    else bid.next = NULL;

    return D_O_K;
}


int save_finite_movie(void)
{
    static int n_images_to_save = 256;
    O_i *oi = NULL;

    if (updating_menu_state != 0)    return D_O_K;

    win_scanf("You want to save how many images?%d\n"
              " Think about RAM size...\n"
              "Recording starts when mouse button is pressed", &n_images_to_save);

    oi = create_and_attach_movie_to_imr(imr_TRACK, oi_TRACK->im.nx , oi_TRACK->im.ny, oi_TRACK->im.data_type ,
                                        n_images_to_save);

    if (oi == NULL) return win_printf_OK("Oi could not be created");

    if (bid.timer_do == NULL) return win_printf_OK("Bid NULL");

    while (!(mouse_b & 0x3));

    if (bid.next == NULL)
    {
        bid.next = (Bid *)calloc(1, sizeof(Bid));
        bid.next->param = (void *)oi;
        bid.next->timer_do = save_finite_movie_in_tracking_thread;
    }

    return D_O_K;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////                Finite recording asynchronized                                                        /////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void *RecordMovieThreadProc(void *lpParam)
{
    O_i *movie_oi = NULL;
    int i, k , iStarting_image;
    int iImages_in_movie;
    O_p *op = NULL;
    int one_image_on = 1;  // was 2 !
    //  char title[64];
    int bit_per_pixel;


    movie_oi = (O_i *)lpParam;

    if (movie_oi == NULL)
    {
        win_printf_OK("Movie could not be created");
        return NULL;
    }

    iImages_in_movie = movie_oi->im.n_f;
    snprintf(title, 512, "Number of images %d", movie_oi->im.n_f);
    my_set_window_title("%s",title);
    op = movie_oi->o_p[0];
    iStarting_image = track_info->imi[track_info->c_i];
    bit_per_pixel = (oi_TRACK->im.data_type == IS_CHAR_IMAGE) ? 1 : 2;

    for (i = iStarting_image , k = 0; i < (iStarting_image + iImages_in_movie) ; i++ , k++)
    {
        while (track_info->imi[track_info->c_i] < iStarting_image + one_image_on * k)
            psleep(2 * one_image_on);

        memcpy(movie_oi->im.mem[k]/*i-iStarting_image]*/,
               oi_TRACK->im.mem[(iStarting_image + one_image_on * k) % oi_TRACK->im.n_f],
               oi_TRACK->im.nx * oi_TRACK->im.ny * bit_per_pixel); //Correct for data type
        op->dat[0]->xd[i - iStarting_image] = i - iStarting_image;
        op->dat[0]->yd[i - iStarting_image] = track_info->imi[track_info->c_i];
        snprintf(title, 512, "Saved image %d on %d Saving one every %d image", k, iImages_in_movie, one_image_on);
	my_set_window_title("%s",title);
    }

    find_zmin_zmax(movie_oi);
    return D_O_K;
}

int create_record_movie_thread(O_i *oi)
{
    pthread_t hThread;

    int err = pthread_create(&hThread, NULL, RecordMovieThreadProc, (void *) oi);

    if (err)
        return win_printf_OK("No thread created, error n°%d", err);

    return 0;
}

int record_movie__sep_thread(void)
{
    int i;
    static int iImages_in_movie = 128;
    O_i *movie_oi = NULL;

    if (updating_menu_state != 0)    return D_O_K;

    i = win_scanf("You want to save a movie of how many images?%d" , &iImages_in_movie);

    if (i == WIN_CANCEL) return D_O_K;

    if (oi_TRACK == NULL) return win_printf_OK("No tracking image");

    if (track_info == NULL)return win_printf_OK("No tracking info");

    movie_oi = create_and_attach_movie_to_imr(imr_TRACK, oi_TRACK->im.nx, oi_TRACK->im.ny, oi_TRACK->im.data_type,
               iImages_in_movie);

    if (movie_oi == NULL) return win_printf_OK("Movie could not be created");


    uns_oi_2_oi(movie_oi, oi_TRACK);
    inherit_from_im_to_im(movie_oi, oi_TRACK);

    create_and_attach_op_to_oi(movie_oi , iImages_in_movie , iImages_in_movie , 0 , 0);
    win_printf("Movie Created, starting recording");
    create_record_movie_thread(movie_oi);
    return D_O_K;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////                InFinite recording asynchronized                                                        ///////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// New file saving functions

////////////////////////////////////////////////////////////////////////////////
/** Modified .gr header saver.

IMPORTANT : superseds all previous (bugged) versions of save_image_header.
\param O_i The oi containing the information.
\param char A header text ??
\param int ??
\return int Error code.
\author JF Allemand
*/
int save_image_header_JF(O_i *oi, char *head, int size)
{
    FILE *fp;
    int nh;
    char filename[512] = {0};

    if (oi == NULL || oi->dir == NULL || oi->filename == NULL)  return 1;

    build_full_file_name(filename, 512, oi->dir, oi->filename);

    fp = fopen(oi->filename, "r+b");

    if (fp == NULL)
    {
        error_in_file("%s\nfile not found!...", filename);
        return 1;
    }

    nh = find_CTRL_Z_offset(oi, fp);

    if (nh != size)
    {
        win_printf("the size of headers disagree!");
        return 1;
    }

    if (fp == NULL)
    {
        error_in_file("%s\nfile not found!...", filename);
        return 1;
    }

    fseek(fp, 0, SEEK_SET);

    if (fwrite(head, sizeof(char), size, fp) != (size_t)size)
    {
        win_printf("Pb while writing header!");
        return 1;
    }

    fclose(fp);
    return 0;
}

////////////////////////////////////////////////////////////////////////////////
/** Modified file saver.

IMPORTANT : superseds all previous (bugged) versions of close_movie_on_disk.
\param O_i The oi containing the information.
\param int The number of frames in the movie.
\return int Error code.
\author JF Allemand
*/


char *read_image_header_JF(O_i *oi, int *size)
{
    FILE *fp = NULL;
    char filename[512] = {0}, *buf = NULL;



    if (oi == NULL || oi->dir == NULL || oi->filename == NULL)
    {
        snprintf(title, 512, "name namenamenamenamenamenamenamenamenamename");
	my_set_window_title("%s",title);
        return NULL;
    }

    build_full_file_name(filename, 512, oi->dir, oi->filename);
    fp = fopen(oi->filename, "rb");

    if (fp == NULL)
    {
        //error_in_file("%s\nfile not found!...",filename);
        snprintf(title, 512, "FilenameFilenameFilenameFilenameFilenameFilenameFilename");
	my_set_window_title("%s",title);
        return NULL;
    }

    *size = find_CTRL_Z_offset(oi, fp);
    fseek(fp, 0, SEEK_SET);
    buf = (char *)calloc(*size + 1, sizeof(char));

    if (buf == NULL)
    {
        snprintf(title, 512, "FilenameFilename %d", *size);
	my_set_window_title("%s",title);
        return NULL;
    }

    if (fread(buf, sizeof(char), *size, fp) != (size_t)*size)
    {
        //win_printf("Va donc chez speedy!",*size);
        if (buf)
        {
            free(buf);
            buf = NULL;
        }

        snprintf(title, 512, "FilenameFilename %d", *size);
	my_set_window_title("%s",title);

        return NULL;
    }

    // win_printf("%s",buf);
    return buf;
}

int  close_movie_on_disk_JF(O_i *oi, int nf)
{
    char *head = NULL, *found = NULL, tmp[256] = {0};
    int size = 0, nff = -1, cff = -1, i;


    head = read_image_header_JF(oi, &size);

    if (head == NULL)  return 1;

    found = strstr(head, "-nf");

    if (found != NULL)
    {
        snprintf(tmp, 256, "-nf %010d %010d\r\n", nf, oi->im.c_f);

        for (i = 0; (i < 256) && (tmp[i] != 0); i++) found[i] = tmp[i];
    }
    else win_printf("did not found -nf symbol");

    sscanf(found, "-nf %d %d", &nff, &cff);

    // win_printf("%s",found);
    if (oi->im.record_duration != 0)
    {
        found = strstr(head, "-duration");

        if (found != NULL)
        {
            snprintf(tmp, 256, "-duration %016.6f \r\n", oi->im.record_duration);

            for (i = 0; (i < 256) && (tmp[i] != 0); i++) found[i] = tmp[i];
        }
        else win_printf("did not found -duration symbol");
    }

    save_image_header_JF(oi, head,  size);

    if (head)
    {
        free(head);
        head = NULL;
    }

    return 0;
}



void *RecordSaveTapeModeThreadProc(void *lpParam)
{
char fullfile[512];
int num_files;
char path[512];
char file[512];
char common_name[64];
O_i *oi = NULL;
FILE *fp_images = NULL;
Save_Tape_Mode_Param SaveTapeModeParam;
int n_images_saved = 1;
int previous_grab_stat = 0;
int n_images_to_be_saved = 0;
int previous_image_for_saving;
char *head = NULL, *found = NULL;
int relpos = 0;

SaveTapeModeParam = *((Save_Tape_Mode_Param*)(lpParam));
num_files = SaveTapeModeParam.num_files;
snprintf ( path , 512,SaveTapeModeParam.path ) ;
snprintf ( common_name , 64,SaveTapeModeParam.common_name);
oi = create_one_image (oi_TRACK->im.nx, oi_TRACK->im.ny , oi_TRACK->im.data_type);
movie_buffer_on_disk = 1;

while (keypressed()!=TRUE)
  {   if (mouse_b & 1)
{
  fullfile[0] = '\0';
  num_files = get_config_int("TAPE_SAVE_MODE","filenumber",-1);
  if (num_files < 0) num_files = 0;
  snprintf(file,512,"%s%04d.gr",common_name,num_files++);//on rajoute le nombre a incrementer et le format
  strncat(fullfile,path,512-strlen(fullfile));
  strncat(fullfile,file,512-strlen(fullfile));

  oi->im.movie_on_disk = 1;
  oi->dir = strdup(path);
  oi->filename = strdup(file);
  oi->im.time = 0;
  oi->im.n_f= 0;
  set_image_starting_time(oi);
  uns_oi_2_oi(oi, oi_TRACK);
  previous_image_for_saving = 0;
  previous_grab_stat = track_info->imi[track_info->c_i];
  n_images_saved = 0;
  set_image_starting_time(oi);
  set_oi_source(oi,"started at frame %d\n",previous_grab_stat);

  set_image_ending_time (oi);
  save_one_image(oi, fullfile);/**sauve en tete et les parametres**/


  head = read_image_header(oi, &relpos);
  if (head == NULL)  return (win_printf_OK("no header in image"));
  found = strstr(head,"-nf");
  if (found == NULL)  return (win_printf_OK("no nf in image"));

  relpos = (int) (found - head);

  set_config_int("TAPE_SAVE_MODE","filenumber",num_files);
  fp_images = fopen (fullfile, "r+b");
  if ( fp_images == NULL )
    {
      win_printf("Cannot open the file!");
      movie_buffer_on_disk = 0;
      return 1;
    }
  fseek(fp_images, 0, SEEK_END);



  while (!(mouse_b & 2))//stop saving with the mouse right button
    {
      n_images_to_be_saved = track_info->imi[track_info->c_i] - previous_grab_stat - n_images_saved;
      if (n_images_to_be_saved > 0)
  {

    fwrite (oi_TRACK->im.mem[( track_info->imi[track_info->c_i] - n_images_to_be_saved)%oi_TRACK->im.n_f],(oi_TRACK->im.data_type == IS_CHAR_IMAGE) ? 1 : 2 ,oi_TRACK->im.nx * oi_TRACK->im.ny,fp_images);
    n_images_saved++;
    snprintf(title,512,"Currently saving image %d with %d images delayed compared to track",n_images_saved+1,n_images_to_be_saved);
    set_window_title(title);

    if (n_images_saved % 128 ==0)
    {
      fseek(fp_images, relpos, SEEK_SET);
      fprintf(fp_images,"-nf %010d %010d",n_images_saved,0);
      fseek(fp_images, 0, SEEK_END);


    }

    if (n_images_to_be_saved > oi_TRACK->im.n_f)
      {
        fseek(fp_images, relpos, SEEK_SET);
        fprintf(fp_images,"-nf %010d %010d",n_images_saved,0);
        fclose(fp_images);

        return win_printf("One image lost...Stop real time saving");
      }
  }
    }
  fseek(fp_images, relpos, SEEK_SET);
  fprintf(fp_images,"-nf %010d %010d",n_images_saved,0);

  fclose(fp_images);
}
  }
movie_buffer_on_disk = 0;
return D_O_K;



}


int create_save_tape_mode_track_thread(char path[512], char common_name[20], int num_files)
{
    pthread_t hThread;
    Save_Tape_Mode_Param *SaveTapeModeParam = (Save_Tape_Mode_Param *) malloc(sizeof(Save_Tape_Mode_Param));

    SaveTapeModeParam->num_files = num_files;
    snprintf(SaveTapeModeParam->path, 512, "%s", path);
    snprintf(SaveTapeModeParam->common_name, 64, "%s", common_name);

    if (pthread_create(&hThread, NULL, RecordSaveTapeModeThreadProc, (void *) SaveTapeModeParam))
        return win_printf_OK("No thread created");

    return 0;
}




int save_tape_mode_from_track(void)
{
    //  FILE *fp_images = NULL;
    //int n_images_saved = 1;
    //unsigned char *buf;
    int num_files = 0;
    char filenamend[64] = {0};
    char common_name[20] = "movie";
    //int previous_grab_stat = 0;
    char *fullfile = NULL;
    char prevfiles[512] = {0};
    int i;
    char path[512] = {0};
    char file[256] = {0};
    const char *pa = NULL;
    //int n_images_to_be_saved = 0;

    if (updating_menu_state != 0)    return D_O_K;

    if (imr_TRACK == NULL)  return 1;

    if (oi_TRACK == NULL) return 1;

    //imr = imr_TRACK;
    //oi = oi_TRACK;

    movie_buffer_on_disk = 1;
    switch_allegro_font(1);
    pa = (const char *)get_config_string("IMAGE-GR-FILE", "last_saved", NULL);

    if (pa != NULL)     extract_file_path(prevfiles, 512, pa);
    else                my_getcwd(prevfiles, 512);

    put_path_separator(prevfiles, sizeof(prevfiles));

    num_files = get_config_int("TAPE_SAVE_MODE", "filenumber", -1);
    win_scanf("Characteristic string for the files? %s\n Initial number for file%d?", common_name, &num_files);

    strncat(prevfiles, common_name, sizeof(prevfiles)-strlen(prevfiles)); //on rajoute le debut du nom specifique

    snprintf(filenamend, sizeof(filenamend), "%04d.gr", num_files); //on rajoute le nombre a incrementer et le format
    strncat(prevfiles, filenamend, sizeof(prevfiles) - strlen(prevfiles));


    //i = file_select_ex("Save real time movie", fullfile, "gr", 512, 0, 0);
    extract_file_name(file, sizeof(file), prevfiles);
    extract_file_path(path, sizeof(path), prevfiles);

    fullfile = save_one_file_config("Save tracking record (*.trk)", path, file,
                                    "Tracking data Files (*.trk)\0*.trk\0All Files (*.*)\0*.*\0\0", "TRACKING-FILE", "last_saved");

    if (fullfile != 0)
    {
        extract_file_name(file, sizeof(file), fullfile);
        extract_file_path(path, sizeof(path), fullfile);
        put_path_separator(path, sizeof(path));
        free(fullfile);
        i=1;
    }


    switch_allegro_font(0);

    if (i != 0)
    {
        set_config_string("IMAGE-GR-FILE", "last_saved", fullfile);
        extract_file_name(file, 256, fullfile);
        extract_file_path(path, 512, fullfile);
        put_path_separator(path, sizeof(path));
    }
    else
    {
        movie_buffer_on_disk = 1;
        return 0;
    }

    set_config_int("TAPE_SAVE_MODE", "filenumber", num_files);

    create_save_tape_mode_track_thread(path, common_name, num_files);

    return 0;
}


int start_aquisition_tape(void)
{
    if (updating_menu_state != 0)    return D_O_K;

    save_tape_mode_from_track();
    return 0;
}

# ifdef TRACK_WIN_THREAD
////////////////////////////////////////////////////////////////////////////////
/** A timer killer function.

  Specific to this application.
  \param None.
  \return int Error code.
  \author JF Allemand
  */
int Kill_timer(void)
{
    if (KillTimer(win_get_window(), 25) == FALSE) return win_printf_OK("Timer not killed"); //stops the background task

    return 0;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// ROM acquisition functions

////////////////////////////////////////////////////////////////////////////////
/** A timer killer function.

  Specific to this application.
  \param None.
  \return int Error code.
  \author JF Allemand
  */
int Kill_time_lapse_timer(void)
{
    if (KillTimer(win_get_window(), 26) == FALSE) return win_printf_OK("Timer not killed"); //stops the background task

    return D_O_K;//win_printf_OK("Timer killed");
}
#endif

#if defined(TRACK_WIN_THREAD)
void CALLBACK automatic_time_lapse_saving(HWND hwnd, UINT uMsg, UINT_PTR idEvent,  DWORD dwTime)
#else
int automatic_time_lapse_saving(void)
#endif
{
    FILE *fp = NULL;
    //int nw;

    fp = fopen(fullfile_time_lapse, "ab");

    if (fp == NULL)
    {

#if defined(TRACK_WIN_THREAD)
        Kill_time_lapse_timer();
        return ;
#else
        return 1;
#endif

    }

    snprintf(title, 512, "before write");
    my_set_window_title("%s",title);
    //nw =
    fwrite(oi_TRACK->im.mem[(oi_TRACK->im.n_f != 0) ? ((track_info->imi[track_info->c_i] - 1) % oi_TRACK->im.n_f) : 0],
                (oi_TRACK->im.data_type == IS_CHAR_IMAGE) ? 1 : 2 , oi_TRACK->im.nx * oi_TRACK->im.ny, fp);
    fclose(fp);

    images_saved_for_time_lapse++;
    close_movie_on_disk_JF(time_lapse_movie_oi , images_saved_for_time_lapse);
    snprintf(title, 512, "Currently saving image %d expecting %d", images_saved_for_time_lapse, iNumber_of_images_to_save);
    my_set_window_title("%s",title);

    if (images_saved_for_time_lapse >= iNumber_of_images_to_save)
    {
#ifdef TRACK_WIN_THREAD
        Kill_time_lapse_timer();
        return;
#else
        return 1;
#endif
    }

    // set_image_ending_time(time_lapse_movie_oi);

#ifdef TRACK_WIN_THREAD
    return;
#else
    return 0;
#endif
}

int time_lapse(void)
{
    int i;
    char common_name[20] = "movie";
    int num_files = 0;
    char path[512] = {0}, file[256] = {0};//, *pa; , filenamend[256]

    //  FILE *fp = NULL;
    //int nw;
    //char add_source[256];
    if (updating_menu_state != 0)    return D_O_K;

    i = win_scanf("You want to save a movie of how many images?%d \n Number of seconds between 2 frames? %d",
                  &iNumber_of_images_to_save, &iNumber_of_s_between_frames);

    if (i == WIN_CANCEL) return D_O_K;

    if (oi_TRACK == NULL) return win_printf_OK("No image to save");

    if (track_info == NULL)return win_printf_OK("No tracking info");

    ///////////////////////////SELECT FILENAME////////////////////////////////////////////
    movie_buffer_on_disk = 1;

    num_files = get_config_int("TIME_LAPSE_SAVE_MODE", "filenumber", 1);
    win_scanf("Name for the files? %s\n Initial number for file%d?", common_name, &num_files);

    //fullfile_time_lapse[0] = '\0';
    //strcat(fullfile_time_lapse,common_name);//on rajoute le debut du nom specifique
    //sprintf(filenamend,"%04d.gr",num_files);//on rajoute le nombre a incrementer et le format
    //strcat(fullfile_time_lapse,filenamend);

    snprintf(fullfile_time_lapse, 512, "%s%04d.gr", common_name, num_files);
    i = file_select_ex("Save time lapse movie", fullfile_time_lapse, "gr", 512, 0, 0);
    switch_allegro_font(0);

    if (i != 0)
    {
        set_config_string("IMAGE-GR-FILE", "last_saved", fullfile_time_lapse);
        extract_file_name(file, 256, fullfile_time_lapse);
        extract_file_path(path, 512, fullfile_time_lapse);
        put_path_separator(path, sizeof(path));
    }
    else
    {
        movie_buffer_on_disk = 1;
        return 0;
    }

    set_config_int("TIME_LAPSE_SAVE_MODE", "filenumber", ++num_files);

    /////////////////////////////////////////////////////////////////////////////////
    if (time_lapse_movie_oi == NULL)
    {
        if ((time_lapse_movie_oi = (O_i *)calloc(1, sizeof(O_i))) == NULL)
            return win_printf("Our are out of memory guy");

        if (init_one_image(time_lapse_movie_oi, IS_CHAR_IMAGE))
            return win_printf("You have a little problem guy");

        uns_oi_2_oi(time_lapse_movie_oi, oi_TRACK);
        inherit_from_im_to_im(time_lapse_movie_oi, oi_TRACK);
    }



    /*  set_image_starting_time(oi_buffer_tape_CVB); */
    /*  set_image_ending_time (oi_buffer_tape_CVB); */

    time_lapse_movie_oi->im.movie_on_disk = 1;
    time_lapse_movie_oi->dir = strdup(path);
    time_lapse_movie_oi->filename = strdup(fullfile_time_lapse);//pb here
    time_lapse_movie_oi->im.time = 0;
    time_lapse_movie_oi->im.n_f = 0; //iNumber_of_images_to_save;
    time_lapse_movie_oi->im.data_type = (oi_TRACK->im.data_type == IS_CHAR_IMAGE) ? IS_CHAR_IMAGE : IS_UINT_IMAGE;
    time_lapse_movie_oi->im.nx = oi_TRACK->im.nx;
    time_lapse_movie_oi->im.ny = oi_TRACK->im.ny;
    //set_image_starting_time(time_lapse_movie_oi);

    /*  sprintf(add_source," Time lapse interval %d s",iNumber_of_s_between_frames); */
    /*   strcat(time_lapse_movie_oi->im.source,add_source); */
    set_oi_source(time_lapse_movie_oi, " Time lapse interval %d s", iNumber_of_s_between_frames);
    //set_image_starting_time(time_lapse_movie_oi);
    //set_image_ending_time (time_lapse_movie_oi);
    save_one_image(time_lapse_movie_oi, fullfile_time_lapse);/**sauve en tete et les parametres**/

    set_config_int("TIME_LAPSE_SAVE_MODE", "filenumber", num_files);

    images_saved_for_time_lapse = 0;
    snprintf(title, 512, "before timer");
    my_set_window_title("%s",title);


#if defined(TRACK_WIN_THREAD)
    SetTimer(win_get_window(), 26, iNumber_of_s_between_frames * 1000,
             automatic_time_lapse_saving); /*sets a background saving*/
#else
    create_call_timer(&g_auto_time_lapse_timer, iNumber_of_s_between_frames * 1000, automatic_time_lapse_saving);
#endif
    return D_O_K;
}

MENU *save_movie_in_track_image_menu(void)
{
    static MENU mn[32] = {0};


    if (mn[0].text != NULL) return mn;

    add_item_to_menu(mn, "Record movie on disk", save_tape_mode_from_track, NULL, 0 , NULL);
    add_item_to_menu(mn, "Record movie on RAM using a separate thread", record_movie__sep_thread, NULL, 0 , NULL);
    add_item_to_menu(mn, "Record movie on RAM using Bid", save_finite_movie, NULL, 0 , NULL);
    add_item_to_menu(mn, "Record time lapse images", time_lapse, NULL, 0 , NULL);

    return mn;
}


#endif
