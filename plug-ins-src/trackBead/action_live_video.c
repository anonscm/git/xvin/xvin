
#ifndef _ACTION_LIVE_VIDEO_C_
#define _ACTION_LIVE_VIDEO_C_

# include "allegro.h"
# include "winalleg.h"

# include "xvin.h"
# include "fftl32n.h"
# include "fillib.h"





/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "../cfg_file/Pico_cfg.h"
#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
//# include "fftbtl32n.h"
//# include "fillibbt.h"
//# include "focus.h"
//# include "magnetscontrol.h"
# include "PlayItSam.h"
# include "fftbtl32n.h"
# include "fillibbt.h"

//# include <fftw3.h>		// include the fftw v3.0.1 or v3.1.1 library for computing spectra:



# if defined(UEYE) || defined(GIGE)
# include "real_time_avg.h"
# else
# include "lockin_util.h"
# endif


//# include "brown_util.h"
//# include "track_util.h"
# include "action_live_video.h"

int request_pending = 0;
float request_parameter = 0;
int request_image_n = 0;

int rs232_register(int type, int image_n, float val)
{
  if (request_pending)  return 1;
  request_pending = type;
  request_image_n = image_n;
  request_parameter = val;
  return 0;
}

int what_is_pending_request(int *type, int *image_n, float *val)
{
  if (request_pending == 0)  return 1;
  *type = request_pending ;
  *image_n = request_image_n;
  *val = request_parameter;
  return 0;
}




int find_next_available_action(void)
{
  int i;

  if (action_pending == NULL) return -1;
  for (i = 0; i < n_action; i++)  
    if (action_pending[i].proceeded == 1) break; // we look for used action to recycle 
  if (i == n_action)
    {
        n_action = (n_action < m_action) ? n_action + 1 : n_action;
    }
  if (i >= m_action) return -2;                 // the buffer is full
  return i;
}


int fill_next_available_action(int im, int type, float value)
{
  int i;

  i = find_next_available_action();
  if (i < 0) return -1;
  action_pending[i].proceeded = 0;
  action_pending[i].imi = action_pending[i].imi0 = im;
  action_pending[i].type = type;
  action_pending[i].value = value;
  action_pending[i].period = 0;
  return i;
}


int fill_next_available_periodic_action(int im, int type, float value, int period)
{
  int i;

  i = find_next_available_action();
  if (i < 0) return -1;
  action_pending[i].proceeded = 0;
  action_pending[i].imi = action_pending[i].imi0 = im;
  action_pending[i].type = type;
  action_pending[i].value = value;
  action_pending[i].period = period;
  return i;
}


int immediate_next_available_action(int type, float value)
{
  register int im;

  im = live_video_info->imi[live_video_info->c_i];                                 
  return fill_next_available_action(im+1, type, value);
}


int immediate_next_available_action_spare(int type, float value, int spare)
{
  register int im, i;

  im = live_video_info->imi[live_video_info->c_i];                                 
  i = fill_next_available_action(im+1, type, value);
  if (i >= 0)   action_pending[i].spare = spare;
  return i;
}

int find_next_action(int imi)
{
  int i, j;

  if (action_pending == NULL) return -1;
  for (i = 0, j = -1; i < n_action; i++)
    {
      if (action_pending[i].proceeded) continue;
      else j = i;
      if (action_pending[i].imi < 0) break;
      if (action_pending[i].imi <= imi) break;
    }
  if (i < n_action) return i;
  else  n_action = j+1;
  return -2;
}


int find_next_action_by_type(int imi, int type)
{
  int i, j, s_type;

  if (action_pending == NULL) return -1;
  for (i = 0, j = -1; i < n_action; i++)
    {
      if (action_pending[i].proceeded) continue;
      else j = i;
      s_type = (action_pending[i].type & type);
      if (s_type == 0)  continue;
      if (action_pending[i].imi < 0) break;
      if (action_pending[i].imi <= imi) break;
    }
  if (i < n_action) return i;
  else  n_action = j+1;
  return -2;
}

int is_there_pending_action_of_type(int type)
{
  register int im;

  im = live_video_info->imi[live_video_info->c_i];                                 
  return find_next_action_by_type(im, type);
}




int find_remaining_action(int imi)
{
  int i, j;

  if (action_pending == NULL) return -1;
  for (i = 0, j = 0; i < n_action; i++)
      j += (action_pending[i].proceeded == 0) ? 1 : 0;
  return j;
}



int ask_to_update_menu(void)
{
  if (live_video_info == NULL)  return 1;
  last_im_who_asked_menu_update = live_video_info->imi[live_video_info->c_i];
  return 0;
}


# endif
