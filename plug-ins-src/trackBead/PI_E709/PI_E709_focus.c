/*
 *    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
 */
#ifndef _PI_E709_FOCUS_C_
#define _PI_E709_FOCUS_C_

#include "allegro.h"
#ifdef XV_WIN32
#include "winalleg.h"
#endif
#include "xvin.h"


/* If you include other regular header do it here*/


/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
//# include "PI_E709.h"
//place here other headers of this plugin

/* If you include other regular header do it here*/


/* But not below this define */
# define BUILDING_PLUGINS_DLL

# include "PI_E709_focus.h"
# include "../focus.h" //tv 04/03/17 : PI709 HW interface implemented in focus_plot_menu (pico)
#ifdef XV_UNIX
#include <PI/PI_GCS2_DLL.h>
#endif
#ifdef XV_WIN32
#include "PI_GCS2_DLL.h"
#endif
#include "pthread.h"

char PI_E709_axes[10];
int PI_E709_ID = -1;
int Controller_ID = -1;
char szAxes[129];
double Piezzo_position = 0;
int iError;
char szErrorMessage[1025];
BOOL bTest = FALSE;
float actual_temperature=0;
int error=0;
char	errBuff[2048]={'\0'};
float   data_out[1] = {1.0};
#define BUFFER_SIZE 30000
#define PIFOC_RANGE 2500/*4000*/
double PI_obj_position = 0;
float PI_Z_step = 1.0;

float PI_read_aut(void)
{
  static double pos;
  PI_qPOS(Controller_ID, szAxes, &pos);
  return (float)pos;
}

int PI_E709_start(void)
{
  //char szDescription[1025];
  BOOL bReturn_SVO;
  char *szBuffer = NULL;
  szBuffer = malloc(100);
  int nb_controllers = 0;
  #ifdef XV_WIN32
  nb_controllers = PI_EnumerateUSB(szBuffer, 100,"E-709");
  if (nb_controllers <= 0)
  {
    win_printf_OK("The PI E-709 isn't connected");
    return 1;
  }
  Controller_ID = PI_ConnectUSB(szBuffer);
  #endif
  #ifdef XV_UNIX
  Controller_ID = PI_ConnectRS232ByDevName("/dev/ttyUSB0", 57600);
  #endif
  bTest = PI_IsConnected(Controller_ID);
  if (bTest == FALSE) win_printf ("PI E709 Not Connected") ;
  /////////////////////////////////////////
  // Get the name of the connected axis. //
  /////////////////////////////////////////
  if (!PI_qSAI(Controller_ID, szAxes, 16))
  {
    iError = PI_GetError(Controller_ID);
    PI_TranslateError(iError, szErrorMessage, 1024);
    win_printf("SAI?: ERROR %d: %s\n", iError, szErrorMessage);
    PI_CloseConnection(Controller_ID);
    return 1;
  }

  //default : servo on
  bReturn_SVO = TRUE;
  bTest = PI_SVO(Controller_ID, szAxes, &bReturn_SVO);
  if (bTest == FALSE) win_printf ("PI_SVO failed\n");
  bTest = PI_qSVO(Controller_ID, szAxes, &bReturn_SVO);
  if (bTest == FALSE) win_printf ("PI_qSVO failed\n");
  free(szBuffer);
  szBuffer = NULL;
  return 0;
}

//tv 04/03 to disconnect svo
int do_PI_set_SVO(void)
{
  if (updating_menu_state!=0) return D_O_K;
  static BOOL svo_cmd = TRUE;
  if (win_scanf("set PIE709 Servo on%R off%r", &svo_cmd) == WIN_CANCEL) return D_O_K;
  if (Controller_ID == -1) return D_O_K;
  if (PI_SVO(Controller_ID, szAxes, &svo_cmd) == FALSE) win_printf_OK("PI_SVO failed\n");
  return 0;
}
int do_PI_get_SVO(void)
{
  if (updating_menu_state!=0) return D_O_K;
  BOOL svo_state;
  if (Controller_ID == -1) return D_O_K;
  if (PI_qSVO(Controller_ID, szAxes, &svo_state) == FALSE) win_printf ("PI_SVO query failed\n");
  return 0;
}

//tv 04/03/17 : necessary to go to command level 1 (default 0) to tune notch frequency and PI parameters
int do_PI_set_command_level(void)
{
  if (updating_menu_state!=0) return D_O_K;
  static int command_level_i = 0;
  if (win_scanf("Go to command level %d", &command_level_i) == WIN_CANCEL) return D_O_K;
  if (Controller_ID == -1) return D_O_K;
  if (PI_CCL(Controller_ID, 1, "advanced") == FALSE) win_printf_OK("PI failed to change commande level");
  return 0;
}
int do_PI_get_command_level(void)
{
  if (updating_menu_state!=0) return D_O_K;
  int i=0;
  if (Controller_ID == -1) return D_O_K;
  if (PI_qCCL(Controller_ID, &i) == FALSE) win_printf_OK("PI failed to get command level");
  return 0;
}
//sets interesting parameters (doc PIE709)
//digital filter type : par ID 0x05000000, int 0/1/2/99 : no/lIR lowpass/avg filter/user filter
//notch freq 1 : ID 0x08000100, float
//notch freq 2 : ID 0x08000101, float
//notch rejection 1 : ID 0x08000200, float
//notch rejection 2 : ID 0x08000201, float
//notch bandwidth 1 : ID 0x08000300, float
//notch bandwidth 2 : ID 0x08000301, float
//servo loop slew rate : ID 0x07à00200,
//P of PID : ID 0x7000300,
//I of PID : ID 0x7000301,
//processing analog input in digital filter for closed-loop operation :
//filter type 0x05000000
//0 : no filter, 1 low-pass filter 2nd order, 2 moving-average filter
//freq of low-pass : 0x05000001
//nb de points ds moyenne glissante : 0x05000002
//HPV?, HPA?, RPA, RBT
//SPA?/SPA to read/write paramters in volatile memory
//SEP to write a parameter in non volatile memory, RPA to write it without rebooting the controler
int do_PI_get_parameter(void)
{
  if (updating_menu_state != 0) return D_O_K;
  //static int i_param;
  if (Controller_ID == -1) return D_O_K;
  //PI_qSPA(long ID, const char* szAxes, unsigned int* iParameterArray, double* pdValueArray, char* szStrings, int iMaxNameSize);
  /*int iParameterArray[];
  char szStrings[];
  int iMaxNameSize =
  if (PI_qSPA_String(Controller_ID, szAxes, const unsigned int* iParameterArray, char* szStrings, iMaxNameSize) == FALSE)
  {
    win_printf_OK("PI failed to set hw parameter");
    return 1;
  }
  else win_printf_OK("Parameter : ");*/
  return 0;
}

//HPA? : get the list of parameters
int do_PI_get_parameter_list(void)
{
  if (updating_menu_state!=0) return D_O_K;
  if (Controller_ID == -1) return D_O_K;
  int iBufferSize = 128;
  char *szBuffer = NULL;
  szBuffer = (char *)calloc(iBufferSize, sizeof(char));
  if (PI_qHPA(Controller_ID, szBuffer, iBufferSize) == FALSE) win_printf_OK("PI HPA failed at getting parameter list");
  else win_printf_OK("PIE709 answer : %s", szBuffer);
  return 0;
}

//IMP, IMP?, DDR : impulse + respons curve
//ONT?
//SSN?, IDN? : why not
//SVA (SVR relatif) : in open-loop, set voltage, then POS? gives the position : not controlled but better knowledge (noise of closed-loop)
//WPA to copy volatile state of param on non volatile (SPA then WPA when ok)
//RPA : reset volatile value to non volatile
//VEL, VEL? : velocity in closed-loop
//ERR?

int  _init_focus_OK(void)
{
  static int init = 0;
  //tv 04/03/17 : same as for magnets when apt driven : when initizalizing the focus, also adding a menu to interface gcs functions when piE709
  //purpose : work on mechanical notch filter, pid, servo on/off...
  if (init == 0)
  {
    add_item_to_menu(focus_plot_menu(), "PIE709 set SVO", do_PI_set_SVO, NULL, 0, NULL);
    add_item_to_menu(focus_plot_menu(), "PIE709 get SVO", do_PI_get_SVO, NULL, 0, NULL);
    add_item_to_menu(focus_plot_menu(), "PIE709 get list of parameters", do_PI_get_parameter_list, NULL, 0, NULL);
    add_item_to_menu(focus_plot_menu(), "PIE709 set command level (expert only)", do_PI_set_command_level, NULL, 0, NULL);
    add_item_to_menu(focus_plot_menu(), "PIE709 get command level (expert only)", do_PI_get_command_level, NULL, 0, NULL);
    add_item_to_menu(focus_plot_menu(), "PIE709 get parameters", do_PI_get_parameter, NULL, 0, NULL);
    init = 1;
  }
  return PI_E709_start();
}

int _has_focus_memory(void)
{
 return 0;
}




int PI_pos(double* pos)
{
 int bTestl = FALSE;
  if (Controller_ID == -1) PI_E709_start();

  if (Controller_ID == -1)
    {
      win_printf_OK("cannot init PI");
      return 0;
    }
  if (Controller_ID == -1) return 0;
  bTestl = PI_qPOS(Controller_ID, szAxes,pos);
  if (bTestl == FALSE)
    {
      win_printf_OK("PI_qPOS failed\n");
      return 0;
    }
  return 1;
}


float _read_Z_value_accurate_in_micron(void)
{
   PI_pos(&PI_obj_position);
   return (float)PI_obj_position;
}

int _read_Z_value(void)
{
  return (int)(_read_Z_value_accurate_in_micron());//ask meaning i10*
}

float _read_Z_value_OK(int *lerror) /* in microns */
{
  (void)lerror;
  return _read_Z_value_accurate_in_micron();
}
float _read_last_Z_value(void) /* in microns */
{
  return (float)PI_obj_position;//why not asking the driver?
}






int PI_bouge(double pos)
{
  int bTestl = FALSE;
  bTestl =  PI_MOV(Controller_ID, szAxes, (const double*)&pos);
  if (bTestl == FALSE)
    {
      win_printf_OK("PI_MOV failed\n");
      return 0;
    }
  return 1;
}


int PI_bouge_rel(double pos)
{
  int bTestl = FALSE;
  bTestl =  PI_MVR(Controller_ID, szAxes, (const double*)&pos);
  if (bTestl == FALSE)
    {
      win_printf_OK("PI_MVR failed\n");
      return 0;
    }
  return 1;
}



int _set_Z_step(float z)  /* in micron */
{
	PI_Z_step = z;
	return 0;
}




int _set_Z_value(float z)  /* in microns */
{
  double zd;
  if ((z > 250) || (z < 0)) return D_O_K;
  PI_bouge(zd = (double)z);
  return D_O_K;
}


int _set_Z_value_PI(void)
{
  float goal = 0;

  if(updating_menu_state != 0)	return D_O_K;
  win_scanf("Where do you want to go ?%f",goal);
  _set_Z_value(goal);
  return D_O_K;
}


int _set_Z_obj_accurate(float zstart)
{
  return _set_Z_value(zstart);
}
int _set_Z_value_OK(float z)
{
  return _set_Z_value(z);
}

int _inc_Z_value(int step)  /* in micron */
{
  _set_Z_value((float)PI_obj_position + PI_Z_step*step);
  return 0;
}

int _small_inc_Z_value(int up)
{
  int nstep;

  _set_Z_step(0.1);
  nstep = (up>=0)? 1:-1;
  _set_Z_value((float)PI_obj_position + PI_Z_step*nstep);

  return 0;
}

int _big_inc_Z_value(int up)
{
  int nstep;

  _set_Z_step(0.1);
  nstep = (up>=0)? 10:-10;
  _set_Z_value((float)PI_obj_position + PI_Z_step*nstep);

  return 0;
}




MENU *PI_E709_plot_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"Start PI_E709", PI_E709_start,NULL,0,NULL);
  //add_item_to_menu(mn,"Stop PI_E709", PI_E709_close,NULL,0,NULL);
  //add_item_to_menu(mn,"PI_E709 Read Z", PI_E709_read_Z,NULL,0,NULL);
  //add_item_to_menu(mn,"PI_E709 Set Z", PI_E709_set_Z,NULL,0,NULL);
  return mn;
}

int	PI_E709_main(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  add_plot_treat_menu_item ( "PI_E709", NULL, PI_E709_plot_menu(), 0, NULL);
  return D_O_K;
}

int	PI_E709_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  remove_item_to_menu(plot_treat_menu, "PI_E709", NULL, NULL);
  return D_O_K;
}
#endif
