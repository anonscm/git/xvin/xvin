#ifndef _PI_E709_FOCUS_H_
#define _PI_E709_FOCUS_H_


float PI_read_aut(void);
int PI_E709_start(void);
int  _init_focus_OK(void);
int _has_focus_memory(void);
int PI_pos(double* pos);
float _read_Z_value_accurate_in_micron(void);
int _read_Z_value(void);
float _read_Z_value_OK(int *error); /* in microns */
float _read_last_Z_value(void); /* in microns */
int PI_bouge(double pos);
int PI_bouge_rel(double pos);
int _set_Z_step(float z);  /* in micron */
int _set_Z_value(float z);  /* in microns */
int _set_Z_value_PI(void);
int _set_Z_obj_accurate(float zstart);
int _set_Z_value_OK(float z);
int _inc_Z_value(int step);  /* in micron */
int _small_inc_Z_value(int up);
int _big_inc_Z_value(int up);

# endif
