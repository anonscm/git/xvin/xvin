#ifndef _SDI_2_FUNCTIONS_H_
#define _SDI_2_FUNCTIONS_H_


#include "allegro.h"
#include "xvin.h"
#include "xv_main.h"
#include "fftl32n.h"
#include "trackBead.h"
#include "track_util.h"
#include "../fft2d/fftbtl32n.h"
#include "../cfg_file/Pico_cfg.h"
#include "SDI_2_utils_real.h"


XV_FUNC(int,SDI_2_update_frange_position,(b_track *bt,O_i *oic,int length, int width, sdi_fr *f1));
XV_FUNC(int,SDI_2_project_profile_on_profile,(O_i *oic, int xco, int xend, int yco, int yend, float *xi, float *yi));
XV_FUNC(int, SDI_2_window_profile,(float *x, int lx));
XV_FUNC(int,SDI_2_fill_y_av_x_profile_from_im,(O_i *oi, int xco, int xend, int yco, int yend, float *x, double* fit_param,double *xm));
XV_FUNC(int,SDI_2_fill_x_av_y_profile_from_im,(O_i *oi, int xco, int xend, int yco, int yend, float *y, int *ym,int *yg, int *yd));
XV_FUNC(int, SDI_2_vincent_displacement_function,(float *x ,int nx, int bp_low, int bp_high, double k_av, fft_plan *fp,double *phif, double *k, double *Max_pos, d_s *dsfit, float *xibp, float *xfourier));
XV_FUNC(int ,SDI_2_get_tracking_parameters,(float *ext_yratio));
XV_FUNC(int, SDI_2_do_set_tracking_parameters,(void));
XV_FUNC(int, SDI_2_do_choose_window_on_x,(void));
XV_FUNC(int, SDI_2_initialize_phase_fringe, (b_track *bt,sdi_fr *f1));
XV_FUNC(int, SDI_2_initialize_phase,(b_track *bt));
XV_FUNC(int, SDI_2_initialize_k_buffer,(b_track *bt));
XV_FUNC(int, SDI_2_initialize_k_buffer_fringe, (sdi_fr *f1));
XV_FUNC(int, SDI_2_activate_tracking, (void));

MENU *SDI_2_tracking_parameters_menu(void);








#endif
