/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
# ifndef _OLYMPUS_MAGNETS_C_
# define _OLYMPUS_MAGNETS_C_

# include "allegro.h"
# include "winalleg.h"
# include "xvin.h"


/* If you include other plug-ins header do it here*/ 
# include "../magnetscontrol.h"
# include "../olympus_serial/olympus_serial.h"
# include "../action.h"
//# include "../fftbtl32n.h"
//# include "../fillibbt.h"

//# include "../trackBead.h"
//# include "../track_util.h"

/* But not below this define */
# define BUILDING_PLUGINS_DLL

# undef _PXV_DLL
# define _PXV_DLL   __declspec(dllexport)
# include "olympus_magnets.h"

// local variables
/*
unsigned long last_zmag_set, zmag_moving_time = 0;
unsigned long last_rot_set, rot_moving_time;

int Pos_limit = 0, Neg_limit = 0;
char m_speed[512], max_pos[512], ref_pos[512];

float Mag_vcap_min = 0;


int rot_pid[10], zmag_pid[10];
*/
PXV_FUNC(int,     dummy_init_magnets_OK,(void));
PXV_FUNC(int,	dummy_set_rot_value,(float rot));
PXV_FUNC(int,     dummy_set_rot_ref_value,(float rot));  /* in tour */
PXV_FUNC(float,   dummy_read_rot_value,(void));
PXV_FUNC(int,     dummy_set_magnet_z_value,(float pos));
PXV_FUNC(float,   dummy_read_magnet_z_value,(void));
PXV_FUNC(int,     dummy_set_motors_speed,(float v));
PXV_FUNC(float,   dummy_get_motors_speed,(void));

PXV_FUNC(int,     dummy_set_zmag_motor_pid,(int param_nb, int val));
PXV_FUNC(int,     dummy_get_zmag_motor_pid,(int param_nb));

PXV_FUNC(int,     dummy_set_rotation_motor_pid,(int param_nb, int val));
PXV_FUNC(int,     dummy_get_rotation_motor_pid,(int param_nb));

PXV_FUNC(int,     dummy_set_magnet_z_ref_value,(float z));  /* in mm */
PXV_FUNC(int,     dummy_set_magnet_z_value_and_wait,(float pos));
PXV_FUNC(int,     dummy_set_rot_value_and_wait,(float rot));
PXV_FUNC(int,	dummy_go_and_dump_z_magnet,(float z));
PXV_FUNC(int,     dummy_go_wait_and_dump_z_magnet,(float zmag));
PXV_FUNC(int,     dummy_go_and_dump_rot,(float r));
PXV_FUNC(int,	dummy_go_wait_and_dump_rot,(float r));
PXV_FUNC(int,	dummy_go_wait_and_dump_log_specific_rot,(float r, char *log));
PXV_FUNC(int,	dummy_go_wait_and_dump_log_specific_z_magnet,(float zmag, char *log));
PXV_FUNC(int,     dummy_set_z_origin,(float z, int dir));
PXV_FUNC(int,     dummy_has_magnets_memory,(void));
PXV_FUNC(int,     dummy_read_magnet_z_value_and_status,(float *z, float *vcap, int *limit, int *vcap_servo));
PXV_FUNC(int,     dummy_set_Vcap_min,(float v));
PXV_FUNC(float,   dummy_get_Vcap_min ,(void));
PXV_FUNC(int,     dummy_set_zmag_motor_speed,(float v));
PXV_FUNC(float,   dummy_get_zmag_motor_speed,(void));


int grab_plot_from_serial_port(void);

char	*describe_magnets_device_olympus(void)
{
  return "The translation and rotation of the magnets\n"
    "is faked by olympus functions just for test...\n"; 
}


int _has_magnets_memory(void)
{
 return 0;
}


int _set_z_origin(float z, int dir)
{
 return 0;
}

int _init_magnets_OK(void)
{
  return 0;
}

int	_set_rot_value(float rot)
{
  return dummy_set_rot_value(rot);

}

int _set_rot_ref_value(float rot)  /* in tour */
{
  return dummy_set_rot_ref_value(rot);  /* in tour */
}

float _read_rot_value(void)
{
  return dummy_read_rot_value();
}  

int   _set_magnet_z_value(float pos)
{
  return dummy_set_magnet_z_value(pos);
}

float _read_magnet_z_value(void)
{
  return dummy_read_magnet_z_value();
}



int _read_magnet_z_value_and_status(float *z, float *vcap, int *limit, int *vcap_servo)
{
  return dummy_read_magnet_z_value_and_status(z, vcap, limit, vcap_servo);
}

float _get_Vcap_min(void)
{
  return dummy_get_Vcap_min();
}
int _set_Vcap_min(float v)
{
  return dummy_set_Vcap_min(v);
}

int _set_motors_speed(float v)
{
  return dummy_set_motors_speed(v);
}
float  _get_motors_speed()
{
  return dummy_get_motors_speed();
}




int _set_zmag_motor_pid(int n, int val)
{
  return dummy_set_zmag_motor_pid(n, val);
}
int  _get_zmag_motor_pid(int n)
{
  return dummy_get_zmag_motor_pid(n);
}


int _set_rotation_motor_pid(int n, int val)
{
  return dummy_set_rotation_motor_pid(n, val);	
}
int  _get_rotation_motor_pid(int n)
{
  return dummy_get_rotation_motor_pid(n);
}


int _set_zmag_motor_speed(float v)
{
  return dummy_set_zmag_motor_speed(v);
}
float  _get_zmag_motor_speed()
{
  return dummy_get_zmag_motor_speed();
}

int _set_magnet_z_ref_value(float z)  /* in mm */
{
  return dummy_set_magnet_z_ref_value(z);
}



int    _set_magnet_z_value_and_wait(float pos)
{
  return dummy_set_magnet_z_value_and_wait(pos);
}
int    _set_rot_value_and_wait(float rot)
{
  return dummy_set_rot_value_and_wait(rot);
}

int _request_set_rot_value (int image_n, float rot,  unsigned long *t0)
{
  return 0;
}
int _request_set_magnet_z_value(int image_n, float z, unsigned long *t0)
{
  return 0;
}

float _read_rot_from_request_answer(char *answer, int im_n, float val, int *error)
{
  return 0;
}
float _read_zmag_from_request_answer(char *answer, int im_n, float val, int *error, float *vacp)
{
  return 0;
}

float _read_rot_raw_from_request_answer(char *answer, int im_n, float val, int *error)
{
  return 0;
}
int _request_read_rot_value(int image_n, unsigned long *t0)
{
  return 0;
}
int _check_set_zmag_request_answer(char *answer, int im_n, float pos)
{
  return 0;
}
int _request_set_magnet_z_value_raw(int image_n, float pos,  unsigned long *t0)
{
  return 0;
}
int _check_set_rot_request_answer(char *answer, int im_n, float rot)
{
  return 0;
}
int _request_read_magnet_z_value(int image_n, unsigned long *t0)
{
  return 0;
}
float _read_zmag_raw_from_request_answer(char *answer, int im_n, float val, int *error, float *vcap)
{
  return 0;
}
int _request_set_rot_ref_value(int image_n, float rot,  unsigned long *t0)
{
  return 0;
}
int _check_set_rot_ref_request_answer(char *answer, int im_n, float rot)
{
  return 0;
}
int _request_set_zmag_ref_value(int image_n, float pos, unsigned long *t0)
{
  return 0;
}
int _request_set_rot_value_raw(int image_n, float rot, unsigned long *t0)
{
  return 0;
}
int _check_set_zmag_ref_request_answer(char *answer, int im_n, float pos)
{
  return 0;
}

MENU *olympus_magnets_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	//add_item_to_menu(mn,"data set rescale in Y", do_olympus_magnets_rescale_data_set,NULL,0,NULL);
	//add_item_to_menu(mn,"plot rescale in Y", do_olympus_magnets_rescale_plot,NULL,0,NULL);
	return mn;
}

int	olympus_magnets_main(int argc, char **argv)
{
  static int init = 0;
  //	add_plot_treat_menu_item ( "olympus_magnets", NULL, olympus_magnets_plot_menu(), 0, NULL);

  if (init == 0)
    {
      init = 1;
    }

	return D_O_K;
}

int	olympus_magnets_unload(int argc, char **argv)
{
  //remove_item_to_menu(plot_treat_menu, "olympus_magnets", NULL, NULL);
	return D_O_K;
}
#endif

