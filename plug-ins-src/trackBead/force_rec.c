
#ifndef _FORCE_REC_C_
#define _FORCE_REC_C_

# include "allegro.h"
#ifdef XV_WIN32
# include "winalleg.h"
#endif
# include "xvin.h"





/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "../cfg_file/Pico_cfg.h"
# include "../cardinal/cardinal.h"
# include "../wlc/wlc.h"

#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "../fft2d/fftbtl32n.h"
# include "fillibbt.h"
# include "focus.h"
# include "focus.h"
# include "magnetscontrol.h"
# include "trackBead.h"
# include "brown_util.h"
# include "track_util.h"

# include "force.h"
# include "record.h"

# include "force_rec.h"

# include "action.h"

#include "../../src/menus/plot/treat/interpol.h"






force_param *load_Force_param_from_trk(g_record *g_r)
{
    int cfg_size;
    force_param *S_p = NULL;
    char *cfg = NULL, *st = NULL, *st1 = NULL;
    FILE *fp = NULL;


    if (g_r == NULL || g_r->fullname == NULL) return NULL;
    cfg_size = g_r->header_size - g_r->config_file_position;
    if (cfg_size <= 0)                    return NULL;
    cfg = (char *)calloc(cfg_size,sizeof(char));
    if (cfg == NULL)                    return NULL;
    fp = fopen(g_r->fullname,"rb");
    if (fp == NULL)
        {
            free(cfg);
            return NULL;
        }
    fseek(fp, 0, SEEK_SET);
    fseek(fp, g_r->config_file_position, SEEK_SET);
    if (fread(cfg, sizeof(char),cfg_size,fp) != (size_t)cfg_size)
        {
            fclose(fp);
            free(cfg);
            return NULL;
        }
    st = strstr(cfg,"[Force_curve]");
    if (st == NULL)
        {
            fclose(fp);
            free(cfg);
            return NULL;
        }
    S_p = (force_param*)calloc(1,sizeof(force_param));
    if (S_p == NULL)
        {
            fclose(fp);
            free(cfg);
            return NULL;
        }

    st1 = strstr(st,"zstart");
    if (st1 != NULL)
        sscanf(st1,"zstart = %f",&(S_p->zmag_start));
    st1 = strstr(st,"zstep");
    if (st1 != NULL)
        sscanf(st1,"zstep = %f",&(S_p->zmag_step));
    st1 = strstr(st,"zmag0");
    if (st1 != NULL)
        sscanf(st1,"zmag0 = %f",&(S_p->zmag0));
    st1 = strstr(st,"lambdazmag");
    if (st1 != NULL)
        sscanf(st1,"lambdazmag = %f",&(S_p->lambdazmag));
    st1 = strstr(st,"zmag_after");
    if (st1 != NULL)
        sscanf(st1,"zmag_after = %f",&(S_p->zlow));
    st1 = strstr(st,"rotation");
    if (st1 != NULL)
        sscanf(st1,"rotation = %f",&(S_p->rotation));
    else	  S_p->rotation = g_r->rot_mag[0][0];


    st1 = strstr(st,"n_step");
    if (st1 != NULL)
        sscanf(st1,"n_step = %d",&(S_p->nstep));
    st1 = strstr(st,"nper");
    if (st1 != NULL)
        sscanf(st1,"nper = %d",&(S_p->nper));
    st1 = strstr(st,"dead");
    if (st1 != NULL)
        sscanf(st1,"dead = %d",&(S_p->dead));
    st1 = strstr(st,"one_way");
    if (st1 != NULL)
        sscanf(st1,"one_way = %d",&(S_p->one_way));
    st1 = strstr(st,"setling");
    if (st1 != NULL)
        sscanf(st1,"setling = %d",&(S_p->setling));
    st1 = strstr(st,"maxnper");
    if (st1 != NULL)
        sscanf(st1,"maxnper = %d",&(S_p->maxnper));
    st1 = strstr(st,"zmag_expo");
    if (st1 != NULL)
        sscanf(st1,"zmag_expo = %f",&(S_p->zmag_hi_fc));
    st1 = strstr(st,"\nn_expo");
    S_p->n_expo_div = 1;
    if (st1 != NULL)
        sscanf(st1,"\nn_expo = %d",&(S_p->n_expo_div));
    S_p->mod_expo_enable = 0;
    if (st1 != NULL)
        sscanf(st1,"\nmod_expo_enable = %d",&(S_p->mod_expo_enable));


    free(cfg);
    fclose(fp);
    return S_p;
}



int draw_extension_vs_kz_over_kx(void)
{
    int  i, i_s0, i_e0;
    //b_track *bt = NULL;
    static int i_s = -1, i_e = 0, i_sk, i_ek, keep_val = 1, kx_or_ky = 0;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    gb_record *gb = NULL;
    b_record *b_r = NULL;
    double r, dr, dr1, ex, dex;

    ac_grep(cur_ac_reg,"%pr",&pr);
    gb = find_gb_record_in_pltreg(pr);
    if(updating_menu_state != 0)
        {  // we wait for recording finish
            if (gb == NULL)
                active_menu->flags |=  D_DISABLED;
            else active_menu->flags &= ~D_DISABLED;
            return D_O_K;
        }
    if (pr == NULL)return 0;
    if (gb->g_r == NULL || gb->n_br < 0 || gb->n_br > gb->g_r->n_bead) return 0;
    b_r = gb->g_r->b_r[gb->n_br];
    if (b_r == NULL || b_r->s_r == NULL || b_r->c_s_r < 1) return 0;

    i_s0 = 0;
    i_e0 = b_r->c_s_r;
    if (i_s < 0 || i_s > i_e0 || i_e > i_e0)
        {
            i_s = i_s0;
            i_e = i_e0;
        }
    else
        {
            i_s = i_sk;
            i_e = i_ek;
        }

    i  = win_scanf("I am going to evaluate the molecule\n"
                   "relative extension based upon the WLC model\n"
                   "and the ratio between k_z and k{x or y}\n"
                   "Do you want to use k_x%R or k_y%r\n"
                   "Compute extention starting at acquisition point %6d\n"
                   "ending at %6d (remember those values %b)\n",&kx_or_ky,&i_s,&i_e,&keep_val);
    if (i == WIN_CANCEL) return 0;
    if (keep_val)
        {
            i_sk = i_s;
            i_ek = i_e;
        }

    op = create_and_attach_one_plot(pr, b_r->c_s_r, b_r->c_s_r, 0);
    ds = op->dat[0];
    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_plot_symb(ds, "\\pt5\\oc");
    set_plot_title(op, "Bead %d Stiffness force curve %d", gb->n_br, gb->g_r->n_rec);
    set_plot_x_title(op,"Extension (\\mu m)");
    if (kx_or_ky)
        {
            set_plot_y_title(op,"Relative extension from K_z/K_y");
            set_plot_file(op,"ZrFromKzOverKy.gr");
            set_ds_source(ds,"Relavie extension from kz/ky");
        }
    else
        {
            set_plot_y_title(op,"Relative extension from K_z/K_x");
            set_plot_file(op,"ZrFromKzOverKX.gr");
            set_ds_source(ds,"Relavie extension from kz/kX");
        }

    for (i = i_s; i <= i_e; i++)
        {
            dr = b_r->s_r[i].daz;
            dr = (b_r->s_r[i].az > 0) ? dr/b_r->s_r[i].az : 0;
            if (kx_or_ky)
                {
                    r = b_r->s_r[i].kz;
                    if (b_r->s_r[i].ky > 0)   r /= b_r->s_r[i].ky;
                    else r = 1;
                    dr1 = b_r->s_r[i].day;
                    dr1 = (b_r->s_r[i].ay > 0) ? dr1/b_r->s_r[i].ay : 0;
                    dr = sqrt(dr*dr + dr1*dr1);
                    dr *= r;
                }
            else
                {
                    r = b_r->s_r[i].kz;
                    if (b_r->s_r[i].kx > 0)   r /= b_r->s_r[i].kx;
                    else r = 1;
                    dr1 = b_r->s_r[i].dax;
                    dr1 = (b_r->s_r[i].ax > 0) ? dr1/b_r->s_r[i].ax : 0;
                    dr = sqrt(dr*dr + dr1*dr1);
                    dr *= r;
                }
            ex = extension_versus_kz_over_kx(r);
            dex = extension_versus_kz_over_kx(r+dr);
            dex -= extension_versus_kz_over_kx(r-dr);
            dex /= 2;
            add_new_point_with_xy_error_to_ds(ds, b_r->s_r[i].mzm, b_r->s_r[i].dmzm,  ex, dex);
        }
    switch_plot(pr, pr->n_op - 1);
    return refresh_plot(pr, pr->n_op - 1);
}


int draw_etar_versus_extension(void)
{
    int  i, i_s0, i_e0;
    //b_track *bt = NULL;
    static int i_s = -1, i_e = 0, i_sk, i_ek, keep_val = 1, kx_or_ky_or_kz = 0, from_hf = 0;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    gb_record *gb = NULL;
    b_record *b_r = NULL;
    double r, dr, dr1;

    ac_grep(cur_ac_reg,"%pr",&pr);
    gb = find_gb_record_in_pltreg(pr);
    if(updating_menu_state != 0)
        {  // we wait for recording finish
            if (gb == NULL)
                active_menu->flags |=  D_DISABLED;
            else active_menu->flags &= ~D_DISABLED;
            return D_O_K;
        }
    if (pr == NULL)return 0;
    if (gb->g_r == NULL || gb->n_br < 0 || gb->n_br > gb->g_r->n_bead) return 0;
    b_r = gb->g_r->b_r[gb->n_br];
    if (b_r == NULL || b_r->s_r == NULL || b_r->c_s_r < 1) return 0;

    i_s0 = 0;
    i_e0 = b_r->c_s_r;
    if (i_s < 0 || i_s > i_e0 || i_e > i_e0)
        {
            i_s = i_s0;
            i_e = i_e0;
        }
    else
        {
            i_s = i_sk;
            i_e = i_ek;
        }

    i  = win_scanf("I am going to evaluate the bead hydrodynamic radius\n"
                   "using the brownian fluctuations\n"
                   "Do you want to use x%R or y%r or z%r direction\n"
                   "Using cutoff frequency %R or high frequecies %r\n"
                   "Compute radius starting at acquisition point %6d\n"
                   "ending at %6d (remember those values %b)\n",&kx_or_ky_or_kz,&from_hf,&i_s,&i_e,&keep_val);
    if (i == WIN_CANCEL) return 0;
    if (keep_val)
        {
            i_sk = i_s;
            i_ek = i_e;
        }

    op = create_and_attach_one_plot(pr, b_r->c_s_r, b_r->c_s_r, 0);
    ds = op->dat[0];
    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_plot_symb(ds, "\\pt5\\oc");
    set_plot_title(op, "Bead %d  force curve %d", gb->n_br, gb->g_r->n_rec);
    set_plot_x_title(op,"Extension (\\mu m)");
    if (kx_or_ky_or_kz == 1)
        {
            set_plot_y_title(op,"\\eta r from Y fluctuations");
            set_plot_file(op,"etary_vs_z.gr");
            set_ds_source(ds,"\\eta r from Y fluctuations");
        }
    else if (kx_or_ky_or_kz == 0)
        {
            set_plot_y_title(op,"\\eta r from X fluctuations");
            set_plot_file(op,"etarx_vs_z.gr");
            set_ds_source(ds,"\\eta r from X fluctuations");
        }
    else
        {
            set_plot_y_title(op,"\\eta r from Z fluctuations");
            set_plot_file(op,"etarz_vs_z.gr");
            set_ds_source(ds,"\\eta r from Z fluctuations");
        }

    for (i = i_s; i <= i_e; i++)
        {
            if (kx_or_ky_or_kz == 1)
                {
                    if (from_hf == 0)
                        {
                            dr1 = b_r->s_r[i].day;
                            dr1 = (b_r->s_r[i].ay > 0) ? dr1/b_r->s_r[i].ay : 0;
                            dr = b_r->s_r[i].dfcy;
                            dr = (b_r->s_r[i].fcy > 0) ? dr/b_r->s_r[i].fcy : 0;
                            dr = sqrt(dr*dr + dr1*dr1);
                            r = b_r->s_r[i].etary;
                            dr *= r;
                        }
                    else
                        {
                            r = b_r->s_r[i].etar2y;
                            dr = b_r->s_r[i].detar2y;;
                        }
                }
            else if (kx_or_ky_or_kz == 0)
                {
                    if (from_hf == 0)
                        {
                            dr1 = b_r->s_r[i].dax;
                            dr1 = (b_r->s_r[i].ax > 0) ? dr1/b_r->s_r[i].ax : 0;
                            dr = b_r->s_r[i].dfcx;
                            dr = (b_r->s_r[i].fcx > 0) ? dr/b_r->s_r[i].fcx : 0;
                            dr = sqrt(dr*dr + dr1*dr1);
                            r = b_r->s_r[i].etarx;
                            dr *= r;
                        }
                    else
                        {
                            r = b_r->s_r[i].etar2x;
                            dr = b_r->s_r[i].detar2x;;
                        }
                }
            else
                {
                    if (from_hf == 0)
                        {
                            dr1 = b_r->s_r[i].daz;
                            dr1 = (b_r->s_r[i].az > 0) ? dr1/b_r->s_r[i].az : 0;
                            dr = b_r->s_r[i].dfcz;
                            dr = (b_r->s_r[i].fcz > 0) ? dr/b_r->s_r[i].fcz : 0;
                            dr = sqrt(dr*dr + dr1*dr1);
                            r = b_r->s_r[i].etarz;
                            dr *= r;
                        }
                    else
                        {
                            r = b_r->s_r[i].etar2z;
                            dr = b_r->s_r[i].detar2z;;
                        }
                }
            add_new_point_with_xy_error_to_ds(ds, b_r->s_r[i].mzm, b_r->s_r[i].dmzm, 1e9*r, 1e9*dr);
        }
    switch_plot(pr, pr->n_op - 1);
    return refresh_plot(pr, pr->n_op - 1);
}




int draw_mean_position(void)
{
    int  i;
    //b_track *bt = NULL;
    static int kx_or_ky = 0;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    gb_record *gb = NULL;
    b_record *b_r = NULL;
    double mean;

    ac_grep(cur_ac_reg,"%pr",&pr);
    gb = find_gb_record_in_pltreg(pr);
    if(updating_menu_state != 0)
        {  // we wait for recording finish
            if (gb == NULL)
                active_menu->flags |=  D_DISABLED;
            else active_menu->flags &= ~D_DISABLED;
            return D_O_K;
        }
    if (pr == NULL)return 0;
    if (gb->g_r == NULL || gb->n_br < 0 || gb->n_br > gb->g_r->n_bead) return 0;
    b_r = gb->g_r->b_r[gb->n_br];
    if (b_r == NULL || b_r->s_r == NULL || b_r->c_s_r < 1) return 0;

    i  = win_scanf("I am going to draw bead mean position\n"
                   "using the brownian fluctuations\n"
                   "Do you want to use x%R or y%r direction\n"
                   ,&kx_or_ky);
    if (i == WIN_CANCEL) return 0;

    op = create_and_attach_one_plot(pr, b_r->c_s_r, b_r->c_s_r, 0);
    ds = op->dat[0];
    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_plot_symb(ds, "\\pt5\\oc");
    set_plot_title(op, "Bead %d  force curve %d", gb->n_br, gb->g_r->n_rec);
    set_plot_y_title(op,"Z mean value (\\mu m)");

    if (kx_or_ky == 1)
        {
            set_plot_x_title(op,"Y mean value (\\mu m)");
            set_plot_file(op,"Z_vs_Y.gr");
            set_ds_source(ds,"Z vs Y mean values");
        }
    else
        {
            set_plot_x_title(op,"X mean value (\\mu m)");
            set_plot_file(op,"Z_vs_X.gr");
            set_ds_source(ds,"Z vs X mean values");
        }
    for (i = 0, mean = 0; i < b_r->c_s_r; i++)
        {
            if (kx_or_ky == 1)
                {
                    add_new_point_with_xy_error_to_ds(ds, b_r->s_r[i].mym, b_r->s_r[i].dmym,
                                                      b_r->s_r[i].mzm, b_r->s_r[i].dmzm);
                    mean += b_r->s_r[i].mym;
                }
            else
                {
                    add_new_point_with_xy_error_to_ds(ds, b_r->s_r[i].mxm, b_r->s_r[i].dmxm,
                                                      b_r->s_r[i].mzm, b_r->s_r[i].dmzm);
                    mean += b_r->s_r[i].mxm;
                }
        }
    mean /= b_r->c_s_r;
    op->y_lo = 0;
    op->y_hi = gb->g_r->Pico_param_record.dna_molecule.dsDNA_molecule_extension;
    set_plot_y_fixed_range(op);
    op->x_lo = mean - op->y_hi/2;
    op->x_hi = mean + op->y_hi/2;
    set_plot_x_fixed_range(op);
    switch_plot(pr, pr->n_op - 1);
    return refresh_plot(pr, pr->n_op - 1);
}

# ifdef ENCOURS


int draw_fc_versus_zmag(void)
{
    int  i, i_s0, i_e0;
    //b_track *bt = NULL;
    static int do_x = 1, do_y = 1, do_z = 1;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    gb_record *gb = NULL;
    b_record *b_r = NULL;
    double r, dr, dr1;

    ac_grep(cur_ac_reg,"%pr",&pr);
    gb = find_gb_record_in_pltreg(pr);
    if(updating_menu_state != 0)
        {  // we wait for recording finish
            if (gb == NULL)
                active_menu->flags |=  D_DISABLED;
            else active_menu->flags &= ~D_DISABLED;
            return D_O_K;
        }
    if (pr == NULL)return 0;
    if (gb->g_r == NULL || gb->n_br < 0 || gb->n_br > gb->g_r->n_bead) return 0;
    b_r = gb->g_r->b_r[gb->n_br];
    if (b_r == NULL || b_r->s_r == NULL || b_r->c_s_r < 1) return 0;

    i  = win_scanf("I am going to plot the cutoff frequency of acquisition points\n"
                   "obtained by fitting a Lorentzian to brownian fluctuation\n"
                   "Do you want to draw x%R or y%r or z%r direction\n",&do_x,&do_y,&do_z);
    if (i == WIN_CANCEL) return 0;

    op = create_and_attach_one_plot(pr, b_r->c_s_r, b_r->c_s_r, 0);
    ds = op->dat[0];
    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_plot_symb(ds, "\\pt5\\oc");
    set_plot_title(op, "Bead %d  force curve %d", gb->n_br, gb->g_r->n_rec);
    set_plot_x_title(op,"f_c");
    set_plot_y_title(op,"Zmag");

    if (kx_or_ky_or_kz == 1)
        {
            set_plot_file(op,"etary_vs_z.gr");
            set_ds_source(ds,"\\eta r from Y fluctuations");
        }
    else if (kx_or_ky_or_kz == 0)
        {
            set_plot_y_title(op,"\\eta r from X fluctuations");
            set_plot_file(op,"etarx_vs_z.gr");
            set_ds_source(ds,"\\eta r from X fluctuations");
        }
    else
        {
            set_plot_y_title(op,"\\eta r from Z fluctuations");
            set_plot_file(op,"etarz_vs_z.gr");
            set_ds_source(ds,"\\eta r from Z fluctuations");
        }

    for (i = i_s; i <= i_e; i++)
        {
            if (kx_or_ky_or_kz == 1)
                {
                    if (from_hf == 0)
                        {
                            dr1 = b_r->s_r[i].day;
                            dr1 = (b_r->s_r[i].ay > 0) ? dr1/b_r->s_r[i].ay : 0;
                            dr = b_r->s_r[i].dfcy;
                            dr = (b_r->s_r[i].fcy > 0) ? dr/b_r->s_r[i].fcy : 0;
                            dr = sqrt(dr*dr + dr1*dr1);
                            r = b_r->s_r[i].etary;
                            dr *= r;
                        }
                    else
                        {
                            r = b_r->s_r[i].etar2y;
                            dr = b_r->s_r[i].detar2y;;
                        }
                }
            else if (kx_or_ky_or_kz == 0)
                {
                    if (from_hf == 0)
                        {
                            dr1 = b_r->s_r[i].dax;
                            dr1 = (b_r->s_r[i].ax > 0) ? dr1/b_r->s_r[i].ax : 0;
                            dr = b_r->s_r[i].dfcx;
                            dr = (b_r->s_r[i].fcx > 0) ? dr/b_r->s_r[i].fcx : 0;
                            dr = sqrt(dr*dr + dr1*dr1);
                            r = b_r->s_r[i].etarx;
                            dr *= r;
                        }
                    else
                        {
                            r = b_r->s_r[i].etar2x;
                            dr = b_r->s_r[i].detar2x;;
                        }
                }
            else
                {
                    if (from_hf == 0)
                        {
                            dr1 = b_r->s_r[i].daz;
                            dr1 = (b_r->s_r[i].az > 0) ? dr1/b_r->s_r[i].az : 0;
                            dr = b_r->s_r[i].dfcz;
                            dr = (b_r->s_r[i].fcz > 0) ? dr/b_r->s_r[i].fcz : 0;
                            dr = sqrt(dr*dr + dr1*dr1);
                            r = b_r->s_r[i].etarz;
                            dr *= r;
                        }
                    else
                        {
                            r = b_r->s_r[i].etar2z;
                            dr = b_r->s_r[i].detar2z;;
                        }
                }
            add_new_point_with_xy_error_to_ds(ds, 1e9*r, 1e9*dr, b_r->s_r[i].mzm, b_r->s_r[i].dmzm);
        }
    switch_plot(pr, pr->n_op - 1);
    return refresh_plot(pr, pr->n_op - 1);
}

# endif

int draw_force_versus_extension_cor(void)
{
    int i, j, i_p, nw, i_n;
    //b_track *bt = NULL;
    static int  B_on_y = 1, fzmag = 0;
    static float er_level = .25, fc_max = 0.33, eta_min = 0.5, eta_max = 10, mol_off = 0;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    O_p *opz = NULL;
    d_s *dsz = NULL;
    gb_record *gb = NULL;
    b_record *b_r = NULL;
    force_param *sph = NULL;
    float b_radius, kxl = 0, dkx = 0;

    ac_grep(cur_ac_reg,"%pr",&pr);
    gb = find_gb_record_in_pltreg(pr);
    if(updating_menu_state != 0)
        {  // we wait for recording finish
            if (gb == NULL)
                active_menu->flags |=  D_DISABLED;
            else active_menu->flags &= ~D_DISABLED;
            return D_O_K;
        }
    if (pr == NULL)return 0;
    if (gb->g_r == NULL || gb->n_br < 0 || gb->n_br > gb->g_r->n_bead) return 0;
    b_r = gb->g_r->b_r[gb->n_br];
    if (b_r == NULL || b_r->s_r == NULL || b_r->c_s_r < 1) return 0;
    b_radius = gb->g_r->Pico_param_record.bead_param.radius;
    sph = load_Force_param_from_trk(gb->g_r);
    if (sph == NULL)    return win_printf_OK("cannot retrieve force info !");

    er_level *= 100;
    i  = win_scanf("I am going to redraw force curve with a possible length adaptation\n"
                   "and point selection upon acceptable values\n"
                   "I shall adjust the molecule length according to the magnetic field\n"
                   "direction. The magnetic field was along the x%R or y%r direction.\n"
                   "(If \\vec{B} along y, F_y = k_BT.<z+\\delta z>/<\\delta y^2> and F_x = k_BT.<z+r+\\delta z>/<\\delta x^2>)\n"
                   "Specify a possible offset \\delta z = %8f \n"
                   "I shall eliminate points which have one of the following properties :\n"
                   "\\oc If the error on the stiffness exceeds %8f %%\n\n"
                   "\\oc if the cutoff frequency exceeds %8f of the camera frequency\n"
                   "\\oc if the hydrodynamic radius of the bead is %8f smaller than expected\n"
                   "\\oc or if the hydrodynamic radius of the bead is %8f larger than expected\n"
                   "%b draw Force versus Zmag\n"
                   ,&B_on_y,&mol_off,&er_level, &fc_max, &eta_min, &eta_max,&fzmag);
    er_level /= 100;
    if (i == WIN_CANCEL) return 0;
    nw = (sph->one_way) ? 1 : 2;

    op = create_and_attach_one_plot(pr, b_r->c_s_r, b_r->c_s_r, 0);
    ds = op->dat[0];
    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_plot_symb(ds, "\\pt5\\oc");
    set_plot_title(op, "Force curve %d Bead %d (good points)", gb->g_r->n_rec,gb->n_br);
    set_plot_x_title(op,"Extension (\\mu m)");
    set_plot_y_title(op,"Force (pN)");
    set_plot_file(op,"Forcebd%01d%03d%s",i,gb->g_r->n_rec,".gr");

    set_ds_source(ds,"F_x Force\nZ mag start = %g for %d frames force curve by steps of %g"
                  " for %d steps %d way(s)\nrot = %g dead period %d\n"
                  "Bead nb %d force curve %s mol offset %g\n"
                  ,sph->zmag_start,sph->nper,sph->zmag_step,sph->nstep,nw,gb->g_r->rot_mag[0][0]
                  ,sph->dead,i,gb->g_r->filename,mol_off);


    if (fzmag)
        {
            opz = create_and_attach_one_plot(pr, b_r->c_s_r, b_r->c_s_r, 0);
            dsz = opz->dat[0];
            dsz->nx = dsz->ny = 0;
            set_dot_line(dsz);
            set_plot_symb(dsz, "\\pt5\\oc");
            set_plot_title(opz, "Force vs Zmag %d Bead %d (good points)", gb->g_r->n_rec,gb->n_br);
            set_plot_x_title(opz,"Zmag (mm)");
            set_plot_y_title(opz,"Force (pN)");
            set_plot_file(opz,"Force-Zmag-bd%01d%03d%s",i,gb->g_r->n_rec,".gr");

            set_ds_source(dsz,"F_x Force\nZ mag start = %g for %d frames force curve by steps of %g"
                          " for %d steps %d way(s)\nrot = %g dead period %d\n"
                          "Bead nb %d force curve %s mol offset %g\n"
                          ,sph->zmag_start,sph->nper,sph->zmag_step,sph->nstep,nw,gb->g_r->rot_mag[0][0]
                          ,sph->dead,i,gb->g_r->filename,mol_off);

        }


    for (j = 0, i_p = i_n = 0; j < b_r->c_s_r; j++)
        {
            if ((b_r->s_r[j].n_exp > 1) && (b_r->s_r[j].i_exp == 0)) //  (sr->n_exp - sr->i_exp) == 1))
                {
                    i_n++;
                    if (b_r->s_r[j].wong_x2 > 0)	    kxl = (13.8 * 298)/b_r->s_r[j].wong_x2;
                    kxl *= (b_r->s_r[j].mzm + mol_off);
                    if (b_r->s_r[j].nxeff/2 > 1)	    dkx = kxl/sqrt((b_r->s_r[j].nxeff/2)-1);
                    add_new_point_with_xy_error_to_ds(ds, b_r->s_r[j].mzm + mol_off, b_r->s_r[j].dmzm, kxl, dkx);
                    if (fzmag)
                        add_new_point_with_y_error_to_ds(dsz, b_r->s_r[j].zmag, kxl, dkx);
                    i_p++;
                }
            if (b_r->s_r[j].n_exp == 1)
                {
                    i_n++;
                    if (b_r->s_r[j].dax > b_r->s_r[j].ax * er_level)    continue; // we only keep accurate date
                    if (b_r->s_r[j].fcx > b_r->s_r[j].nxeff * fc_max) continue; // we only keep accurate date
                    if ((b_r->s_r[j].etarx > (1e-9 * eta_max * b_radius)) || (b_r->s_r[j].etarx < (1e-9 * b_radius * eta_min)))
                        continue; // we only keep accurate date
                    kxl = b_r->s_r[j].kx*1e6;
                    dkx = b_r->s_r[j].dax;
                    dkx = (b_r->s_r[j].ax > 0) ? dkx/b_r->s_r[j].ax : 0;
                    dkx *= kxl;
                    if (B_on_y)
                        {
                            kxl *= (b_r->s_r[j].mzm + b_radius + mol_off);
                            dkx *= (b_r->s_r[j].mzm + b_radius + mol_off);
                        }
                    else
                        {
                            kxl *= (b_r->s_r[j].mzm  + mol_off);
                            dkx *= (b_r->s_r[j].mzm + mol_off);
                        }
                    add_new_point_with_xy_error_to_ds(ds, b_r->s_r[j].mzm + mol_off, b_r->s_r[j].dmzm, kxl, dkx);
                    if (fzmag)
                        add_new_point_with_y_error_to_ds(dsz, b_r->s_r[j].zmag, kxl, dkx);
                    i_p++;
                }
        }


    if ((ds = create_and_attach_one_ds(op, b_r->c_s_r, b_r->c_s_r, 0)) == NULL)
        return win_printf_OK("I can't create plot !");
    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_plot_symb(ds, "\\pt5\\oc");
    set_ds_source(ds,"F_y force\nZ mag start = %g for %d frames force curve by steps of %g"
                  " for %d steps %d way(s)\nrot = %g dead period %d\n"
                  "Bead nb %d force curve %s mol offset %g\n"
                  ,sph->zmag_start,sph->nper,sph->zmag_step,sph->nstep,nw,gb->g_r->rot_mag[0][0]
                  ,sph->dead,i,gb->g_r->filename,mol_off);


    if (fzmag)
        {

            if ((dsz = create_and_attach_one_ds(opz, b_r->c_s_r, b_r->c_s_r, 0)) == NULL)
                return win_printf_OK("I can't create plot !");
            dsz->nx = dsz->ny = 0;
            set_dot_line(dsz);
            set_plot_symb(dsz, "\\pt5\\oc");
            set_ds_source(dsz,"F_y force\nZ mag start = %g for %d frames force curve by steps of %g"
                          " for %d steps %d way(s)\nrot = %g dead period %d\n"
                          "Bead nb %d force curve %s mol offset %g\n"
                          ,sph->zmag_start,sph->nper,sph->zmag_step,sph->nstep,nw,gb->g_r->rot_mag[0][0]
                          ,sph->dead,i,gb->g_r->filename,mol_off);

        }

    for (j = 0, i_n = i_p = 0; j < b_r->c_s_r; j++)
        {
            if ((b_r->s_r[j].n_exp > 1) && (b_r->s_r[j].i_exp == 0)) //  (sr->n_exp - sr->i_exp) == 1))
                {
                    i_n++;
                    if (b_r->s_r[j].wong_y2 > 0)	    kxl = (13.8 * 298)/b_r->s_r[j].wong_y2;
                    kxl *= (b_r->s_r[j].mzm + mol_off);
                    if (b_r->s_r[j].nxeff/2 > 1)	    dkx = kxl/sqrt((b_r->s_r[j].nxeff/2)-1);
                    add_new_point_with_xy_error_to_ds(ds, b_r->s_r[j].mzm + mol_off, b_r->s_r[j].dmzm, kxl, dkx);
                    if (fzmag)
                        add_new_point_with_y_error_to_ds(dsz, b_r->s_r[j].zmag, kxl, dkx);
                    i_p++;
                }
            if (b_r->s_r[j].n_exp == 1)
                {
                    i_n++;
                    if (b_r->s_r[j].day > b_r->s_r[j].ay * er_level)    continue; // we only keep accurate date
                    if (b_r->s_r[j].fcy > b_r->s_r[j].nyeff * fc_max) continue; // we only keep accurate date
                    if ((b_r->s_r[j].etary > (1e-9 * eta_max * b_radius)) || (b_r->s_r[j].etary < (1e-9 * b_radius * eta_min)))
                        continue; // we only keep accurate date
                    kxl = b_r->s_r[j].ky*1e6;
                    dkx = b_r->s_r[j].day;
                    dkx = (b_r->s_r[j].ay > 0) ? dkx/b_r->s_r[j].ay : 0;
                    dkx *= kxl;
                    if (B_on_y == 0)
                        {
                            kxl *= (b_r->s_r[j].mzm + b_radius + mol_off);
                            dkx *= (b_r->s_r[j].mzm + b_radius + mol_off);
                        }
                    else
                        {
                            kxl *= (b_r->s_r[j].mzm  + mol_off);
                            dkx *= (b_r->s_r[j].mzm + mol_off);
                        }
                    add_new_point_with_xy_error_to_ds(ds, b_r->s_r[j].mzm + mol_off, b_r->s_r[j].dmzm, kxl, dkx);
                    if (fzmag)
                        add_new_point_with_y_error_to_ds(dsz, b_r->s_r[j].zmag, kxl, dkx);
                    i_p++;
                }
        }
    if (i_n > i_p)
        set_plot_x_prime_title(op, "%d points were excluded from this curve",i_n - i_p);

    switch_plot(pr, pr->n_op - 1);
    free(sph);
    return refresh_plot(pr, pr->n_op - 1);
}


int draw_force_versus_extension_cor_2(void)
{
    int i, j, i_p, nw, i_n;
    //b_track *bt = NULL;
    static int  B_on_y = 1, fzmag = 0, calibrated_force_curve = 0, ssdnanb=0, dsdnanb=0;
    static float er_level = .25, fc_max = 0.33, eta_min = 0.5, eta_max = 10, mol_off = 0;
    int not_converged = 1;
    float difference = 0, difference_before = 0;
    float *force_arx;
    float *force_ary;

    pltreg *pr = NULL;
    O_p *op = NULL, *opDNA = NULL;
    d_s *ds = NULL, *dsssdna = NULL, *dsdsdna = NULL;
    O_p *opz = NULL;
    d_s *dsz = NULL;
    gb_record *gb = NULL;
    b_record *b_r = NULL;
    force_param *sph = NULL;
    float b_radius, kxl = 0, dkx = 0;
    char path[2048] = {0};
    char fname[2048] = {0};
    char *file = NULL;
    extern char *fu;
    float wss = 0, wds = 0;
    float accu = 1e-3;
    float conv_criterium = 1e-3;

    ac_grep(cur_ac_reg,"%pr",&pr);
    gb = find_gb_record_in_pltreg(pr);
    if(updating_menu_state != 0)
        {  // we wait for recording finish
            if (gb == NULL)
                active_menu->flags |=  D_DISABLED;
            else active_menu->flags &= ~D_DISABLED;
            return D_O_K;
        }
    if (pr == NULL)return 0;
    if (gb->g_r == NULL || gb->n_br < 0 || gb->n_br > gb->g_r->n_bead) return 0;
    b_r = gb->g_r->b_r[gb->n_br];
    if (b_r == NULL || b_r->s_r == NULL || b_r->c_s_r < 1) return 0;
    b_radius = gb->g_r->Pico_param_record.bead_param.radius;
    sph = load_Force_param_from_trk(gb->g_r);
    if (sph == NULL)    return win_printf_OK("cannot retrieve force info !");

    er_level *= 100;
    i  = win_scanf("I am going to redraw force curve with a possible length adaptation\n"
                   "and point selection upon acceptable values\n"
                   "I shall adjust the molecule length according to the magnetic field\n"
                   "direction. The magnetic field was along the x%R or y%r direction.\n"
                   "(If \\vec{B} along y, F_y = k_BT.<z+\\delta z>/<\\delta y^2> and F_x = k_BT.<z+r+\\delta z>/<\\delta x^2>)\n"
                   "Specify a possible offset \\delta z = %8f \n"
                   "I shall eliminate points which have one of the following properties :\n"
                   "\\oc If the error on the stiffness exceeds %8f %%\n\n"
                   "\\oc if the cutoff frequency exceeds %8f of the camera frequency\n"
                   "\\oc if the hydrodynamic radius of the bead is %8f smaller than expected\n"
                   "\\oc or if the hydrodynamic radius of the bead is %8f larger than expected\n"
                   "%b draw Force versus Zmag\n"
                   "%b Do you want to use a calibrated force curve ?\n"
                   ,&B_on_y,&mol_off,&er_level, &fc_max, &eta_min, &eta_max,&fzmag,&calibrated_force_curve);
    er_level /= 100;
    if (i == WIN_CANCEL) return 0;
    nw = (sph->one_way) ? 1 : 2;

    if (calibrated_force_curve)
    {
      i = win_scanf("Please enter the number of ssDNA bases %d\n"
                    "Please enter the number of dsDNA bases %d\n"
                    "Please enter the conv criterium %f \n", &ssdnanb, &dsdnanb,&conv_criterium);
      if (i == WIN_CANCEL) return 0;



    file = open_one_file_config("Load DNA calibration curve (*.gr)", fu,
                                "Plot Files (*.gr)\0*.gr\0All Files (*.*)\0*.*\0\0", "PLOT-GR-FILE", "last_loaded");

    if (file)
    {
        extract_file_path(path, sizeof(path), file);
        extract_file_name(fname, sizeof(fname), file);
        opDNA = create_plot_from_gr_file(fname, path);

        //win_printf("%s",op);
        if (opDNA == NULL) return(win_printf_OK("Could not loaded %s\n from %s",
                                          backslash_to_slash(fname), backslash_to_slash(path)));
     }

     dsssdna = find_source_specific_ds_in_op(opDNA, "ssDNA");
     dsdsdna = find_source_specific_ds_in_op(opDNA, "dsDNA");

     if (dsssdna == NULL) return(win_printf_OK("no ssdna dataset\n"));
     if (dsdsdna == NULL) return(win_printf_OK("no dsdna dataset\n"));
     wss = find_max_width_between_succesive_point(dsssdna->xd, dsssdna->nx);
     wds = find_max_width_between_succesive_point(dsdsdna->xd, dsdsdna->nx);
   }

    op = create_and_attach_one_plot(pr, b_r->c_s_r, b_r->c_s_r, 0);
    ds = op->dat[0];
    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_plot_symb(ds, "\\pt5\\oc");
    set_plot_title(op, "Force curve %d Bead %d (good points)", gb->g_r->n_rec,gb->n_br);
    set_plot_x_title(op,"Extension (\\mu m)");
    set_plot_y_title(op,"Force (pN)");
    set_plot_file(op,"Forcebd%01d%03d%s",i,gb->g_r->n_rec,".gr");

    set_ds_source(ds,"F_x Force\nZ mag start = %g for %d frames force curve by steps of %g"
                  " for %d steps %d way(s)\nrot = %g dead period %d\n"
                  "Bead nb %d force curve %s mol offset %g\n"
                  ,sph->zmag_start,sph->nper,sph->zmag_step,sph->nstep,nw,gb->g_r->rot_mag[0][0]
                  ,sph->dead,i,gb->g_r->filename,mol_off);


    if (fzmag)
        {
            opz = create_and_attach_one_plot(pr, b_r->c_s_r, b_r->c_s_r, 0);
            dsz = opz->dat[0];
            dsz->nx = dsz->ny = 0;
            set_dot_line(dsz);
            set_plot_symb(dsz, "\\pt5\\oc");
            set_plot_title(opz, "Force vs Zmag %d Bead %d (good points)", gb->g_r->n_rec,gb->n_br);
            set_plot_x_title(opz,"Zmag (mm)");
            set_plot_y_title(opz,"Force (pN)");
            set_plot_file(opz,"Force-Zmag-bd%01d%03d%s",i,gb->g_r->n_rec,".gr");

            set_ds_source(dsz,"F_x Force\nZ mag start = %g for %d frames force curve by steps of %g"
                          " for %d steps %d way(s)\nrot = %g dead period %d\n"
                          "Bead nb %d force curve %s mol offset %g\n"
                          ,sph->zmag_start,sph->nper,sph->zmag_step,sph->nstep,nw,gb->g_r->rot_mag[0][0]
                          ,sph->dead,i,gb->g_r->filename,mol_off);

        }

    difference_before=1000;

      force_arx = calloc(b_r->c_s_r,sizeof(float));
    for (j = 0, i_p = i_n = 0; j < b_r->c_s_r; j++) force_arx[j] = 20;

    not_converged = 1;
    while (not_converged)
    {

    difference = 0;
    for (j = 0, i_p = i_n = 0; j < b_r->c_s_r; j++)
        {
            if ((b_r->s_r[j].n_exp > 1) && (b_r->s_r[j].i_exp == 0)) //  (sr->n_exp - sr->i_exp) == 1))
                {
                    i_n++;
                    if (b_r->s_r[j].wong_x2 > 0)	    kxl = (13.8 * 298)/b_r->s_r[j].wong_x2;

                    if (calibrated_force_curve) kxl *=  0.001*(ssdnanb*interpolate_point_in_array(dsssdna->xd, dsssdna->yd, dsssdna->nx, force_arx[j], wss, accu) + dsdnanb*interpolate_point_in_array(dsdsdna->xd, dsdsdna->yd, dsdsdna->nx, force_arx[j], wds, accu));
                    else  kxl *= (b_r->s_r[j].mzm + mol_off);
                    if (b_r->s_r[j].nxeff/2 > 1)	    dkx = kxl/sqrt((b_r->s_r[j].nxeff/2)-1);
                    if (calibrated_force_curve == 1 && not_converged == 2 ) add_new_point_with_xy_error_to_ds(ds, 0.001 *(ssdnanb*interpolate_point_in_array(dsssdna->xd, dsssdna->yd, dsssdna->nx, force_arx[j], wss, accu) + dsdnanb*interpolate_point_in_array(dsdsdna->xd, dsdsdna->yd, dsdsdna->nx, force_arx[j], wds, accu)),b_r->s_r[j].dmzm,kxl, dkx);
                    if (calibrated_force_curve == 0) add_new_point_with_xy_error_to_ds(ds,b_r->s_r[j].mzm + mol_off, b_r->s_r[j].dmzm, kxl, dkx);
                    if (fzmag)
                        if (not_converged == 2 || calibrated_force_curve == 0) add_new_point_with_y_error_to_ds(dsz, b_r->s_r[j].zmag, kxl, dkx);
                    i_p++;
                    if (calibrated_force_curve)
                    {
                    difference += (force_arx[j] - kxl) * (force_arx[j] - kxl);
                    force_arx[j] = kxl;
                  }
                }
            if (b_r->s_r[j].n_exp == 1)
                {
                    i_n++;
                    if (b_r->s_r[j].dax > b_r->s_r[j].ax * er_level)    continue; // we only keep accurate date
                    if (b_r->s_r[j].fcx > b_r->s_r[j].nxeff * fc_max) continue; // we only keep accurate date
                    if ((b_r->s_r[j].etarx > (1e-9 * eta_max * b_radius)) || (b_r->s_r[j].etarx < (1e-9 * b_radius * eta_min)))
                        continue; // we only keep accurate date
                    kxl = b_r->s_r[j].kx*1e6;
                    dkx = b_r->s_r[j].dax;
                    dkx = (b_r->s_r[j].ax > 0) ? dkx/b_r->s_r[j].ax : 0;
                    dkx *= kxl;
                    if (B_on_y)
                        {
                          if (calibrated_force_curve) kxl *=  b_radius + 0.001*(ssdnanb*interpolate_point_in_array(dsssdna->xd, dsssdna->yd, dsssdna->nx, force_arx[j], wss, accu) + dsdnanb*interpolate_point_in_array(dsdsdna->xd, dsdsdna->yd, dsdsdna->nx, force_arx[j], wds, accu));
                          else  kxl *= b_radius + (b_r->s_r[j].mzm + mol_off);
                          if (calibrated_force_curve) dkx *=  b_radius + 0.001*(ssdnanb*interpolate_point_in_array(dsssdna->xd, dsssdna->yd, dsssdna->nx, force_arx[j], wss, accu) + dsdnanb*interpolate_point_in_array(dsdsdna->xd, dsdsdna->yd, dsdsdna->nx, force_arx[j], wds, accu));
                          else  dkx *= b_radius + (b_r->s_r[j].mzm + mol_off);

                        }
                    else
                        {
                          if (calibrated_force_curve) kxl *=  0.001*(ssdnanb*interpolate_point_in_array(dsssdna->xd, dsssdna->yd, dsssdna->nx, force_arx[j], wss, accu) + dsdnanb*interpolate_point_in_array(dsdsdna->xd, dsdsdna->yd, dsdsdna->nx, force_arx[j], wds, accu));
                          else  kxl *= b_radius + (b_r->s_r[j].mzm + mol_off);
                          if (calibrated_force_curve) dkx *=   0.001*(ssdnanb*interpolate_point_in_array(dsssdna->xd, dsssdna->yd, dsssdna->nx, force_arx[j], wss, accu) + dsdnanb*interpolate_point_in_array(dsdsdna->xd, dsdsdna->yd, dsdsdna->nx, force_arx[j], wds, accu));
                          else  dkx *= b_radius + (b_r->s_r[j].mzm + mol_off);

                        }
                    if (not_converged == 2)  add_new_point_with_xy_error_to_ds(ds, 0.001 *(ssdnanb*interpolate_point_in_array(dsssdna->xd, dsssdna->yd, dsssdna->nx, force_arx[j], wss, accu) + dsdnanb*interpolate_point_in_array(dsdsdna->xd, dsdsdna->yd, dsdsdna->nx, force_arx[j], wds, accu)) , b_r->s_r[j].dmzm, kxl, dkx);
                    if (calibrated_force_curve == 0) add_new_point_with_xy_error_to_ds(ds, b_r->s_r[j].mzm + mol_off, b_r->s_r[j].dmzm, kxl, dkx);
                    if (fzmag)
                      if (not_converged == 2 || calibrated_force_curve == 0) add_new_point_with_y_error_to_ds(dsz, b_r->s_r[j].zmag, kxl, dkx);
                    i_p++;
                    if (calibrated_force_curve)
                    {
                    difference += (force_arx[j] - kxl) * (force_arx[j] - kxl);
                    force_arx[j] = kxl;
                  }
                }

        }

        if (calibrated_force_curve)
        {
          difference = sqrt(difference/j);
          if (difference < conv_criterium)
          {
            if (not_converged == 2) not_converged = 0;
            else if (not_converged == 1) not_converged = 2;

          }
          difference_before = difference;
          difference = 0;
        }
        else not_converged = 0;

      }


    if ((ds = create_and_attach_one_ds(op, b_r->c_s_r, b_r->c_s_r, 0)) == NULL)
        return win_printf_OK("I can't create plot !");
    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_plot_symb(ds, "\\pt5\\oc");
    set_ds_source(ds,"F_y force\nZ mag start = %g for %d frames force curve by steps of %g"
                  " for %d steps %d way(s)\nrot = %g dead period %d\n"
                  "Bead nb %d force curve %s mol offset %g\n"
                  ,sph->zmag_start,sph->nper,sph->zmag_step,sph->nstep,nw,gb->g_r->rot_mag[0][0]
                  ,sph->dead,i,gb->g_r->filename,mol_off);


    if (fzmag)
        {

            if ((dsz = create_and_attach_one_ds(opz, b_r->c_s_r, b_r->c_s_r, 0)) == NULL)
                return win_printf_OK("I can't create plot !");
            dsz->nx = dsz->ny = 0;
            set_dot_line(dsz);
            set_plot_symb(dsz, "\\pt5\\oc");
            set_ds_source(dsz,"F_y force\nZ mag start = %g for %d frames force curve by steps of %g"
                          " for %d steps %d way(s)\nrot = %g dead period %d\n"
                          "Bead nb %d force curve %s mol offset %g\n"
                          ,sph->zmag_start,sph->nper,sph->zmag_step,sph->nstep,nw,gb->g_r->rot_mag[0][0]
                          ,sph->dead,i,gb->g_r->filename,mol_off);

        }

    difference_before=1000;

      force_ary = calloc(b_r->c_s_r,sizeof(float));
    for (j = 0, i_p = i_n = 0; j < b_r->c_s_r; j++) force_ary[j] = 20;

    not_converged = 1;
    while (not_converged)
    {
    difference = 0;

    for (j = 0, i_n = i_p = 0; j < b_r->c_s_r; j++)
        {
            if ((b_r->s_r[j].n_exp > 1) && (b_r->s_r[j].i_exp == 0)) //  (sr->n_exp - sr->i_exp) == 1))
                {
                    i_n++;
                    if (b_r->s_r[j].wong_y2 > 0)	    kxl = (13.8 * 298)/b_r->s_r[j].wong_y2;
                    if (calibrated_force_curve) kxl *=  0.001*(ssdnanb*interpolate_point_in_array(dsssdna->xd, dsssdna->yd, dsssdna->nx, force_ary[j], wss, accu) + dsdnanb*interpolate_point_in_array(dsdsdna->xd, dsdsdna->yd, dsdsdna->nx, force_ary[j], wds, accu));
                    else  kxl *= (b_r->s_r[j].mzm + mol_off);
                    if (b_r->s_r[j].nxeff/2 > 1)	    dkx = kxl/sqrt((b_r->s_r[j].nxeff/2)-1);
                    if (calibrated_force_curve == 1 && not_converged == 2 ) add_new_point_with_xy_error_to_ds(ds, 0.001 *(ssdnanb*interpolate_point_in_array(dsssdna->xd, dsssdna->yd, dsssdna->nx, force_ary[j], wss, accu) + dsdnanb*interpolate_point_in_array(dsdsdna->xd, dsdsdna->yd, dsdsdna->nx, force_ary[j], wds, accu)),b_r->s_r[j].dmzm, kxl, dkx);
                    if (calibrated_force_curve == 0) add_new_point_with_xy_error_to_ds(ds,b_r->s_r[j].mzm + mol_off, b_r->s_r[j].dmzm, kxl, dkx);
                    if (fzmag)
                        if (not_converged == 2 || calibrated_force_curve == 0) add_new_point_with_y_error_to_ds(dsz, b_r->s_r[j].zmag, kxl, dkx);
                    i_p++;
                    if (calibrated_force_curve)
                    {
                    difference += (force_ary[j] - kxl) * (force_ary[j] - kxl);
                    force_ary[j] = kxl;
                  }
                }
            if (b_r->s_r[j].n_exp == 1)
                {
                    i_n++;
                    if (b_r->s_r[j].day > b_r->s_r[j].ay * er_level)    continue; // we only keep accurate date
                    if (b_r->s_r[j].fcy > b_r->s_r[j].nyeff * fc_max) continue; // we only keep accurate date
                    if ((b_r->s_r[j].etary > (1e-9 * eta_max * b_radius)) || (b_r->s_r[j].etary < (1e-9 * b_radius * eta_min)))
                        continue; // we only keep accurate date
                    kxl = b_r->s_r[j].ky*1e6;
                    dkx = b_r->s_r[j].day;
                    dkx = (b_r->s_r[j].ay > 0) ? dkx/b_r->s_r[j].ay : 0;
                    dkx *= kxl;
                    if (B_on_y == 0)
                        {
                          if (calibrated_force_curve) kxl *=  b_radius + 0.001*(ssdnanb*interpolate_point_in_array(dsssdna->xd, dsssdna->yd, dsssdna->nx, force_ary[j], wss, accu) + dsdnanb*interpolate_point_in_array(dsdsdna->xd, dsdsdna->yd, dsdsdna->nx, force_ary[j], wds, accu));
                          else  kxl *= b_radius + (b_r->s_r[j].mzm + mol_off);
                          if (calibrated_force_curve) dkx *=  b_radius + 0.001*(ssdnanb*interpolate_point_in_array(dsssdna->xd, dsssdna->yd, dsssdna->nx, force_ary[j], wss, accu) + dsdnanb*interpolate_point_in_array(dsdsdna->xd, dsdsdna->yd, dsdsdna->nx, force_ary[j], wds, accu));
                          else  dkx *= b_radius + (b_r->s_r[j].mzm + mol_off);

                        }
                    else
                        {
                          if (calibrated_force_curve) kxl *=  0.001*(ssdnanb*interpolate_point_in_array(dsssdna->xd, dsssdna->yd, dsssdna->nx, force_ary[j], wss, accu) + dsdnanb*interpolate_point_in_array(dsdsdna->xd, dsdsdna->yd, dsdsdna->nx, force_ary[j], wds, accu));
                          else  kxl *= b_radius + (b_r->s_r[j].mzm + mol_off);
                          if (calibrated_force_curve) dkx *=   0.001*(ssdnanb*interpolate_point_in_array(dsssdna->xd, dsssdna->yd, dsssdna->nx, force_ary[j], wss, accu) + dsdnanb*interpolate_point_in_array(dsdsdna->xd, dsdsdna->yd, dsdsdna->nx, force_ary[j], wds, accu));
                          else  dkx *= b_radius + (b_r->s_r[j].mzm + mol_off);

                        }
                        if (not_converged == 2)  add_new_point_with_xy_error_to_ds(ds, 0.001 *(ssdnanb*interpolate_point_in_array(dsssdna->xd, dsssdna->yd, dsssdna->nx, force_ary[j], wss, accu) + dsdnanb*interpolate_point_in_array(dsdsdna->xd, dsdsdna->yd, dsdsdna->nx, force_ary[j], wds, accu)) , b_r->s_r[j].dmzm, kxl, dkx);
                        if (calibrated_force_curve == 0) add_new_point_with_xy_error_to_ds(ds, b_r->s_r[j].mzm + mol_off, b_r->s_r[j].dmzm, kxl, dkx);
                        if (fzmag)
                          if (not_converged == 2 || calibrated_force_curve == 0) add_new_point_with_y_error_to_ds(dsz, b_r->s_r[j].zmag, kxl, dkx);
                    i_p++;
                    if (calibrated_force_curve)
                    {
                    difference += (force_ary[j] - kxl) * (force_ary[j] - kxl);
                    force_ary[j] = kxl;
                  }
                }
        }

        if (calibrated_force_curve)
        {
          difference = sqrt(difference/j);
          if (difference < conv_criterium)
          {

            if (not_converged == 2) not_converged = 0;
            else if (not_converged == 1) not_converged = 2;
          }
          difference_before = difference;
          difference = 0;
        }
        else not_converged = 0;

      }
    if (i_n > i_p)
        set_plot_x_prime_title(op, "%d points were excluded from this curve",i_n - i_p);

    switch_plot(pr, pr->n_op - 1);
    free(sph);
    if (calibrated_force_curve )
    {
      if (force_arx != NULL) free(force_arx);
      if (force_ary != NULL) free(force_ary);
      free_one_plot(opDNA);

    }
    return refresh_plot(pr, pr->n_op - 1);
}


int draw_estimated_force_versus_extension(void)
{
    int i, j, nw;
    //b_track *bt = NULL;
    static float  mol_off = 0;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    gb_record *gb = NULL;
    b_record *b_r = NULL;
    force_param *sph = NULL;
    float kxl, dkx;

    ac_grep(cur_ac_reg,"%pr",&pr);
    gb = find_gb_record_in_pltreg(pr);
    if(updating_menu_state != 0)
        {  // we wait for recording finish
            if (gb == NULL) 	active_menu->flags |=  D_DISABLED;
            else active_menu->flags &= ~D_DISABLED;
            return D_O_K;
        }
    if (pr == NULL)return 0;
    if (gb->g_r == NULL || gb->n_br < 0 || gb->n_br > gb->g_r->n_bead) return 0;
    b_r = gb->g_r->b_r[gb->n_br];
    if (b_r == NULL || b_r->s_r == NULL || b_r->c_s_r < 1) return 0;
    //b_radius = gb->g_r->Pico_param_record.bead_param.radius;
    sph = load_Force_param_from_trk(gb->g_r);
    if (sph == NULL)    return win_printf_OK("cannot retrieve force info !");

    i  = win_scanf("I am going to draw estimated force curve with a possible length adaptation\n"
                   "and point selection upon acceptable values\n"
                   "Specify a possible offset \\delta z = %8f \n"
                   ,&mol_off);
    if (i == WIN_CANCEL) return 0;
    nw = (sph->one_way) ? 1 : 2;

    op = create_and_attach_one_plot(pr, b_r->c_s_r, b_r->c_s_r, 0);
    ds = op->dat[0];
    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_plot_symb(ds, "\\pt5\\oc");
    set_plot_title(op, "Estimated Force curve %d Bead %d (good points)", gb->g_r->n_rec,gb->n_br);
    set_plot_x_title(op,"Extension (\\mu m)");
    set_plot_y_title(op,"Estimated force (pN)");
    set_plot_file(op,"EstiForcebd%01d%03d%s",i,gb->g_r->n_rec,".gr");

    set_ds_source(ds,"Estimated Force\nZ mag start = %g for %d frames force curve by steps of %g"
                  " for %d steps %d way(s)\nrot = %g dead period %d\n"
                  "Bead nb %d force curve %s mol offset %g\n"
                  ,sph->zmag_start,sph->nper,sph->zmag_step,sph->nstep,nw,gb->g_r->rot_mag[0][0]
                  ,sph->dead,i,gb->g_r->filename,mol_off);
    for (j = 0; j < b_r->c_s_r; j++)
        {
            kxl = trk_zmag_to_force(0, b_r->s_r[j].zmag, gb->g_r);
            dkx = kxl* 0.15;
            add_new_point_with_xy_error_to_ds(ds, b_r->s_r[j].mzm + mol_off, b_r->s_r[j].dmzm, kxl, dkx);
        }


    switch_plot(pr, pr->n_op - 1);
    free(sph);
    return refresh_plot(pr, pr->n_op - 1);
}




int draw_estimated_force_versus_zmag(void)
{
    int j, nw;
    //b_track *bt = NULL;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    gb_record *gb = NULL;
    b_record *b_r = NULL;
    force_param *sph = NULL;
    float kxl, dkx;

    ac_grep(cur_ac_reg,"%pr",&pr);
    gb = find_gb_record_in_pltreg(pr);
    if(updating_menu_state != 0)
        {  // we wait for recording finish
            if (gb == NULL) 	active_menu->flags |=  D_DISABLED;
            else active_menu->flags &= ~D_DISABLED;
            return D_O_K;
        }
    if (pr == NULL)return 0;
    if (gb->g_r == NULL || gb->n_br < 0 || gb->n_br > gb->g_r->n_bead) return 0;
    b_r = gb->g_r->b_r[gb->n_br];
    if (b_r == NULL || b_r->s_r == NULL || b_r->c_s_r < 1) return 0;
    //b_radius = gb->g_r->Pico_param_record.bead_param.radius;
    sph = load_Force_param_from_trk(gb->g_r);
    if (sph == NULL)    return win_printf_OK("cannot retrieve force info !");

    nw = (sph->one_way) ? 1 : 2;

    op = create_and_attach_one_plot(pr, b_r->c_s_r, b_r->c_s_r, 0);
    ds = op->dat[0];
    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_plot_symb(ds, "\\pt5\\oc");
    set_plot_title(op, "Estimated Force curve %d Bead %d (good points)", gb->g_r->n_rec,gb->n_br);
    set_plot_x_title(op,"Zmag (mm)");
    set_plot_y_title(op,"Estimated force (pN)");
    set_plot_file(op,"ZmagForcebd%01d%03d%s",0,gb->g_r->n_rec,".gr");

    set_ds_source(ds,"Estimated Force\nZ mag start = %g for %d frames force curve by steps of %g"
                  " for %d steps %d way(s)\nrot = %g dead period %d\n"
                  "force curve %s\n"
                  ,sph->zmag_start,sph->nper,sph->zmag_step,sph->nstep,nw,gb->g_r->rot_mag[0][0]
                  ,sph->dead,gb->g_r->filename);
    for (j = 0; j < b_r->c_s_r; j++)
        {
            kxl = trk_zmag_to_force(0, b_r->s_r[j].zmag, gb->g_r);
            dkx = kxl* 0.15;
            add_new_point_with_y_error_to_ds(ds, b_r->s_r[j].zmag,  kxl, dkx);
        }


    switch_plot(pr, pr->n_op - 1);
    free(sph);
    return refresh_plot(pr, pr->n_op - 1);
}




int draw_zmag_versus_extension(void)
{
    int i, j, nw;
    //b_track *bt = NULL;
    static float  mol_off = 0;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    gb_record *gb = NULL;
    b_record *b_r = NULL;
    force_param *sph = NULL;

    ac_grep(cur_ac_reg,"%pr",&pr);
    gb = find_gb_record_in_pltreg(pr);
    if(updating_menu_state != 0)
        {  // we wait for recording finish
            if (gb == NULL) 	active_menu->flags |=  D_DISABLED;
            else active_menu->flags &= ~D_DISABLED;
            return D_O_K;
        }
    if (pr == NULL)return 0;
    if (gb->g_r == NULL || gb->n_br < 0 || gb->n_br > gb->g_r->n_bead) return 0;
    b_r = gb->g_r->b_r[gb->n_br];
    if (b_r == NULL || b_r->s_r == NULL || b_r->c_s_r < 1) return 0;
    //b_radius = gb->g_r->Pico_param_record.bead_param.radius;
    sph = load_Force_param_from_trk(gb->g_r);
    if (sph == NULL)    return win_printf_OK("cannot retrieve force info !");
    i  = win_scanf("I am going to draw zmag vs <z> curve\n"
                   "with a possible length adaptation\n"
                   "Specify a possible offset \\delta z = %8f \n"
                   ,&mol_off);
    if (i == WIN_CANCEL) return 0;

    nw = (sph->one_way) ? 1 : 2;

    op = create_and_attach_one_plot(pr, b_r->c_s_r, b_r->c_s_r, 0);
    ds = op->dat[0];
    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_plot_symb(ds, "\\pt5\\oc");
    set_plot_title(op, "Zmag vs <z> curve %d Bead %d (good points)", gb->g_r->n_rec,gb->n_br);
    set_plot_x_title(op,"Extension (\\mu m)");
    set_plot_y_title(op,"Zmag (mm)");
    set_plot_file(op,"Zmagbd%01d%03d%s",i,gb->g_r->n_rec,".gr");

    set_ds_source(ds,"Zmag vs <z>\nZ mag start = %g for %d frames force curve by steps of %g"
                  " for %d steps %d way(s)\nrot = %g dead period %d\n"
                  "Bead nb %d force curve %s mol offset %g\n"
                  ,sph->zmag_start,sph->nper,sph->zmag_step,sph->nstep,nw,gb->g_r->rot_mag[0][0]
                  ,sph->dead,i,gb->g_r->filename,mol_off);
    for (j = 0; j < b_r->c_s_r; j++)
        {
            add_new_point_with_x_error_to_ds(ds, b_r->s_r[j].mzm + mol_off, b_r->s_r[j].dmzm, b_r->s_r[j].zmag);
        }


    switch_plot(pr, pr->n_op - 1);
    free(sph);
    return refresh_plot(pr, pr->n_op - 1);
}






int draw_force_versus_estimated_force_cor(void)
{
    int i, j, i_p, nw;
    //b_track *bt = NULL;
    static int  B_on_y = 1;
    static float er_level = .25, fc_max = 0.33, eta_min = 0.5, eta_max = 10, mol_off = 0;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    gb_record *gb = NULL;
    b_record *b_r = NULL;
    force_param *sph = NULL;
    float b_radius, kxl, dkx, fe, dfe;

    ac_grep(cur_ac_reg,"%pr",&pr);
    gb = find_gb_record_in_pltreg(pr);
    if(updating_menu_state != 0)
        {  // we wait for recording finish
            if (gb == NULL)
                active_menu->flags |=  D_DISABLED;
            else active_menu->flags &= ~D_DISABLED;
            return D_O_K;
        }
    if (pr == NULL)return 0;
    if (gb->g_r == NULL || gb->n_br < 0 || gb->n_br > gb->g_r->n_bead) return 0;
    b_r = gb->g_r->b_r[gb->n_br];
    if (b_r == NULL || b_r->s_r == NULL || b_r->c_s_r < 1) return 0;
    b_radius = gb->g_r->Pico_param_record.bead_param.radius;
    sph = load_Force_param_from_trk(gb->g_r);
    if (sph == NULL)    return win_printf_OK("cannot retrieve force info !");

    er_level *= 100;
    i  = win_scanf("I am going to redraw force vs estimated force computed from zmag and\n"
                   "magnets properties with a possible length adaptation\n"
                   "and point selection upon acceptable values\n"
                   "I shall adjust the molecule length according to the magnetic field\n"
                   "direction. The magnetic field was along the x%R or y%r direction.\n"
                   "(If \\vec{B} along y, F_y = k_BT.<z+\\delta z>/<\\delta y^2> and F_x = k_BT.<z+r+\\delta z>/<\\delta x^2>)\n"
                   "Specify a possible offset \\delta z = %8f \n"
                   "I shall eliminate points which have one of the following properties :\n"
                   "\\oc If the error on the stiffness exceeds %8f %%\n\n"
                   "\\oc if the cutoff frequency exceeds %8f of the camera frequency\n"
                   "\\oc if the hydrodynamic radius of the bead is %8f smaller than expected\n"
                   "\\oc or if the hydrodynamic radius of the bead is %8f larger than expected\n"
                   ,&B_on_y,&mol_off,&er_level, &fc_max, &eta_min, &eta_max);
    er_level /= 100;
    if (i == WIN_CANCEL) return 0;
    nw = (sph->one_way) ? 1 : 2;

    op = create_and_attach_one_plot(pr, b_r->c_s_r, b_r->c_s_r, 0);
    ds = op->dat[0];
    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_plot_symb(ds, "\\pt5\\oc");
    set_plot_title(op, "Force curve %d Bead %d (good points)", gb->g_r->n_rec,gb->n_br);
    set_plot_x_title(op,"Estimated force (pN)");
    set_plot_y_title(op,"Force (pN)");
    set_plot_file(op,"ForcevsEstibd%01d%03d%s",i,gb->g_r->n_rec,".gr");

    set_ds_source(ds,"F_x Force vs estimated \nZ mag start = %g for %d frames force curve by steps of %g"
                  " for %d steps %d way(s)\nrot = %g dead period %d\n"
                  "Bead nb %d force curve %s mol offset %g\n"
                  ,sph->zmag_start,sph->nper,sph->zmag_step,sph->nstep,nw,gb->g_r->rot_mag[0][0]
                  ,sph->dead,i,gb->g_r->filename,mol_off);
    for (j = 0, i_p = 0; j < b_r->c_s_r; j++)
        {
            if (b_r->s_r[j].dax > b_r->s_r[j].ax * er_level)    continue; // we only keep accurate date
            if (b_r->s_r[j].fcx > b_r->s_r[j].nxeff * fc_max) continue; // we only keep accurate date
            if ((b_r->s_r[j].etarx > (1e-9 * eta_max * b_radius)) || (b_r->s_r[j].etarx < (1e-9 * b_radius * eta_min)))
                continue; // we only keep accurate date
            kxl = b_r->s_r[j].kx*1e6;
            dkx = b_r->s_r[j].dax;
            dkx = (b_r->s_r[j].ax > 0) ? dkx/b_r->s_r[j].ax : 0;
            dkx *= kxl;
            if (B_on_y)
                {
                    kxl *= (b_r->s_r[j].mzm + b_radius + mol_off);
                    dkx *= (b_r->s_r[j].mzm + b_radius + mol_off);
                }
            else
                {
                    kxl *= (b_r->s_r[j].mzm  + mol_off);
                    dkx *= (b_r->s_r[j].mzm + mol_off);
                }
            fe = trk_zmag_to_force(0, b_r->s_r[j].zmag, gb->g_r);
            dfe = fe* 0.15;
            add_new_point_with_xy_error_to_ds(ds, fe, dfe, kxl, dkx);
            i_p++;
        }


    if ((ds = create_and_attach_one_ds(op, b_r->c_s_r, b_r->c_s_r, 0)) == NULL)
        return win_printf_OK("I can't create plot !");
    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_plot_symb(ds, "\\pt5\\oc");
    set_ds_source(ds,"F_y force versus estimated \nZ mag start = %g for %d frames force curve by steps of %g"
                  " for %d steps %d way(s)\nrot = %g dead period %d\n"
                  "Bead nb %d force curve %s mol offset %g\n"
                  ,sph->zmag_start,sph->nper,sph->zmag_step,sph->nstep,nw,gb->g_r->rot_mag[0][0]
                  ,sph->dead,i,gb->g_r->filename,mol_off);
    for (j = 0; j < b_r->c_s_r; j++)
        {
            if (b_r->s_r[j].day > b_r->s_r[j].ay * er_level)    continue; // we only keep accurate date
            if (b_r->s_r[j].fcy > b_r->s_r[j].nyeff * fc_max) continue; // we only keep accurate date
            if ((b_r->s_r[j].etary > (1e-9 * eta_max * b_radius)) || (b_r->s_r[j].etary < (1e-9 * b_radius * eta_min)))
                continue; // we only keep accurate date
            kxl = b_r->s_r[j].ky*1e6;
            dkx = b_r->s_r[j].day;
            dkx = (b_r->s_r[j].ay > 0) ? dkx/b_r->s_r[j].ay : 0;
            dkx *= kxl;
            if (B_on_y == 0)
                {
                    kxl *= (b_r->s_r[j].mzm + b_radius + mol_off);
                    dkx *= (b_r->s_r[j].mzm + b_radius + mol_off);
                }
            else
                {
                    kxl *= (b_r->s_r[j].mzm  + mol_off);
                    dkx *= (b_r->s_r[j].mzm + mol_off);
                }
            fe = trk_zmag_to_force(0, b_r->s_r[j].zmag, gb->g_r);
            dfe = fe* 0.15;
            add_new_point_with_xy_error_to_ds(ds, fe, dfe, kxl, dkx);
            i_p++;
        }
    if (b_r->c_s_r > i_p)
        set_plot_x_prime_title(op, "%d points were excluded from this curve",b_r->c_s_r - i_p);

    switch_plot(pr, pr->n_op - 1);
    free(sph);
    return refresh_plot(pr, pr->n_op - 1);
}



double compute_chi2_and_fit_wong( double tau_0, // characteristime in micro s
                                  double *a,    // amplitude fitted
                                  d_s *ds)      // data with error
{
    register int i;
    double n, d, tmp, sig2, fita = 0, alphal;

    if (ds == NULL || ds->nx < 3 || ds->ye == NULL || (tau_0 == 0)) return -1.0;
    for(i = 0, n = d = 0; i < ds->nx; i++)
        {
            alphal = (double)ds->xd[i]/ tau_0;
            tmp = 1 - exp(-alphal);
            tmp /= alphal * alphal;
            tmp = ((double)1.0/alphal) - tmp;
            tmp *= 2;
            sig2 = ds->ye[i]*ds->ye[i];
            if (sig2 == 0 || tmp == 0)
                my_set_window_title("division by zero at pt %d",i);
            else
                {
                    n += (tmp * ds->yd[i])/sig2;
                    d += tmp*tmp/sig2;
                }
        }
    if (d == 0) 	my_set_window_title("division by zero in fit");
    else	fita = n/d;
    for(i = 0, n = 0; i < ds->nx; i++)
        {
            alphal = (double)ds->xd[i]/ tau_0;
            tmp = 1 - exp(-alphal);
            tmp /= alphal * alphal;
            tmp = ((double)1.0/alphal) - tmp;
            tmp *= 2;
            sig2 = ds->ye[i]*ds->ye[i];
            if (sig2 == 0 || tmp == 0)
                my_set_window_title("division by zero at pt %d",i);
            else
                {
                    tmp = ds->yd[i] - (fita*tmp);
                    n += (tmp*tmp)/sig2;
                }
        }
    if (a != NULL)	*a = fita;
    return n;
}



int fit_wong_ds(O_p *op, d_s *ds, float *x2, float *tau, float *chisq, float zmag)
{
    register int i;
    d_s *dsfit = NULL;
    double mint, maxt, chi2 = 0, a, t, chi20, a0, t0, chi21, a1, t1, tmp, alphal, tm;
    char st[1024] = {0};

    if ((dsfit = create_and_attach_one_ds(op, 256, 256, 0)) == NULL)
        return win_printf_OK("cannot create plot");

    if (ds->source != NULL) set_ds_source(dsfit,"Wong fit to %s",ds->source);
    else set_ds_source(dsfit,"Wong fit");

    for (i = 0, t0 = t1 = ds->xd[0]; i < ds->nx; i++)
        {
            if (t1 < ds->xd[i]) t1 = ds->xd[i];
            if (t0 > ds->xd[i]) t0 = ds->xd[i];
        }
    mint = t0; maxt = t1;
    //win_printf("min %g max %g",mint, maxt);
    t0 /= 4;
    if (t0 <= 0) t0 = 1;
    chi2 = chi20 = compute_chi2_and_fit_wong(t0, &a0, ds);
    t1 *= 4;
    for (t = t0, chi20 = compute_chi2_and_fit_wong(tm = t, &a, ds); t <= t1; t *=2)
        {
            chi2 = compute_chi2_and_fit_wong(t, &a, ds);
            //win_printf("\\chi^2 = %g n0 = %g a %g",chi2,t,a);
            if (chi2 < chi20)
                {
                    chi20 = chi2;
                    tm = t;
                }
        }
    t = tm;
    //win_printf("first found %g chi2",t,chi20);
    chi21 = compute_chi2_and_fit_wong(2*t, &a1, ds);
    chi2 = compute_chi2_and_fit_wong(t/2, &a1, ds);
    if (chi21 < chi2)
        {
            chi2 = chi20;
            chi20 = compute_chi2_and_fit_wong((t0 = t), &a0, ds);
            chi21 = compute_chi2_and_fit_wong((t1 = 2*t), &a1, ds);
        }
    else
        {
            chi2 = chi20;
            chi20 = compute_chi2_and_fit_wong((t0 = t/2), &a0, ds);
            chi21 = compute_chi2_and_fit_wong((t1 = t), &a1, ds);
        }

    for (t = exp((log(t0)+log(t1))/2); fabs(t1/t0) > 1.00005 ; )
        {
            t = exp((log(t0)+log(t1))/2);
            chi2 = compute_chi2_and_fit_wong(t, &a, ds);
            if (chi20 < chi21)
                {
                    chi21 = chi2;
                    t1 = t;
                    a1 = a;
                }
            else
                {
                    chi20 = chi2;
                    t0 = t;
                    a0 = a;
                }
        }

    tmp = log(maxt/mint);
    for (i = 0; i < dsfit->nx; i++)
        {
            dsfit->xd[i] = exp((i*tmp)/dsfit->nx);
            dsfit->xd[i] *= mint;
            alphal = (double)dsfit->xd[i]/ t;
            dsfit->yd[i] = 1 - exp(-alphal);
            dsfit->yd[i] /= alphal * alphal;
            dsfit->yd[i] = ((double)1.0/alphal) - dsfit->yd[i];
            dsfit->yd[i] *= 2 * a;
        }
    /*
      snprintf(st,sizeof(st),"\\fbox{\\pt8\\stack{{\\sl \\delta x^2(w) = "
	  "x_0^2 \\frac{2}{\\alphal} [1 - \\frac{1-exp^{-\\alphal}}{\\alphal}]}"
	  " {with \\alphal = w/\\tau_0}"
	  "{x_0^2 = %e%s}"
	  "{\\tau_0 = %g  %s}"
	  "{\\chi^2 = %g n = %d}}}",
	  a*op->dy,(op->y_unit != NULL)?op->y_unit:" ",
	  t*op->dx,(op->x_unit != NULL)?op->x_unit:" ",
	  chi2,ds->nx);
    */
    /*
      snprintf(st,sizeof(st),"\\fbox{\\pt8\\stack{{x_0^2 = %6.3e%s \\tau_0 = %g  %s}"
	  "{\\chi^2 = %g n = %d}}}",
	  a*op->dy,(op->y_unit != NULL)?op->y_unit:" ",
	  t*op->dx,(op->x_unit != NULL)?op->x_unit:" ",
	  chi2,ds->nx);
    */

    snprintf(st,sizeof(st),"\\fbox{\\pt6{Zmag %4.2f x_0^2 = %6.2fnm^2 \\tau_0 = %6.2fms \\chi^2 = %4.1f n = %d}}",
             zmag,a*op->dy,  1e-3*t*op->dx,chi2,ds->nx);

    push_plot_label_in_ds(dsfit, dsfit->xd[dsfit->nx/2], a, st, USR_COORD);
    if (x2) *x2 = a*op->dy;
    if (tau) *tau = t*op->dx;
    if (chisq) *chisq = chi2;
    return 0;
}




int redraw_force_from_trk(void)
{
    int  i, prev, j, page_n, i_page, k, i_er, i_p, l;
    //b_track *bt = NULL;
    pltreg *pr = NULL;
    O_p *op, *ops, *opn = NULL, *opx = NULL, *opy = NULL, *opz = NULL;
    d_s *ds, *dss, *dsx = NULL, *dsy = NULL, *dsz = NULL;
    static int c_plot = 1, one_bead = 0, bead_nb = 0, no_kx = 0, fixed_bead = -1, pr_inva_ok = 0;
    int nf, ims = 0, im0 = 0, ima = 0, ime = 0, nw, profile_invalid = 0, n_avg, first;
    float  zmag = 0, rot = 0, xavg, yavg, zavg, kxl = 0, dkx = 0, fc, dfc, xavgf, yavgf, zavgf;
    //b_record *br = NULL;
    g_record *g_r = NULL;
    b_record *b_r = NULL; //, *b_rf = NULL;
    force_param *sph = NULL;
    stiffness_resu *sr = NULL;
    float b_radius = 0, zmag_tmp = 0;
    char mes[256] = {0};
    int exp_min = -1, exp_max = -1, n_acq, li, i_zmag, i_ds, nstep = 0, tmpi, n_maxnper; // , sc_in_found = 0
    scan_info *sc_in = NULL;

    ac_grep(cur_ac_reg,"%pr",&pr);
    g_r = find_g_record_in_pltreg(pr);
    if(updating_menu_state != 0)
        {  // we wait for recording finish
            if (g_r == NULL || working_gen_record == g_r)
                active_menu->flags |=  D_DISABLED;
            else active_menu->flags &= ~D_DISABLED;
            return D_O_K;
        }
    if (pr == NULL)return 0;
    if (g_r == NULL) return OFF;
    nf = g_r->abs_pos;
    if (nf <= 0) return OFF;

    sph = load_Force_param_from_trk(g_r);
    if (sph == NULL)    return win_printf_OK("cannot retrieve force info !");

    for (j = 0, prev = 1; j < nf; j++)
        {
            page_n = j/g_r->page_size;
            i_page = j%g_r->page_size;
            ims = g_r->imi[page_n][i_page];
            if (g_r->status_flag[page_n][i_page] == 0 && prev != 0)
                {
                    im0 = j;
                    zmag = g_r->zmag[page_n][i_page];
                    rot = g_r->rot_mag[page_n][i_page];
                }
            if (g_r->status_flag[page_n][i_page] != 0 && prev == 0)
                {
                    ims = j;
                    if (ims - im0 > sph->nper) break;
                }
            prev = g_r->status_flag[page_n][i_page];
        }

    // win_printf("Force %d points way %d setling %d nper %d\n"
    //     "step %g zmag start %g zmag_finish %g\n "
    //     "zmag0 %g lamdbdazmag %g maxnper %d"
    //     "im0 = %d ims = %d zmag %g rot %g",
    //     sph->nstep,sph->one_way,sph->setling,sph->nper,
    //     sph->zmag_step,sph->zmag_start,sph->zlow,
    //     sph->zmag0, sph->lambdazmag,
    //     im0, ims, zmag, rot);

    im0 = retrieve_image_index_of_next_acquisition_point(g_r, 0, sph->nper+sph->setling, &ims, &zmag, &rot);

    //win_printf("im0 = %d ims = %d zmag %g rot %g",
    //     im0, ims, zmag, rot);

    i = win_scanf("Do you want to create plot \n"
                  "from acquisition point No %R yes %r\n"
                  "if yes, do you want to compute stiffness oui %R no %r \n"
                  "For all beads %R or only one %r\n"
                  "In that last case specify which bead %10d\n"
                  "Substract fixed bead on z %10d (-1 none)\n"
                  ,&c_plot,&no_kx,&one_bead,&bead_nb, &fixed_bead);
    if (i == WIN_CANCEL) return 0;

    nw = (sph->one_way) ? 1 : 2;




    ops = create_and_attach_one_plot(pr, nf, nf, 0);
    dss = ops->dat[0];
    set_ds_source(dss,"Status flag");

    for (j = 0; j < nf; j++)
        {
            page_n = j/g_r->page_size;
            i_page = j%g_r->page_size;
            dss->xd[j] = (g_r->timing_mode == 1) ? (g_r->imi[page_n][i_page] - g_r->imi_start)
                : g_r->imi[page_n][i_page];
            dss->yd[j] = g_r->status_flag[page_n][i_page];
        }

    create_attach_select_x_un_to_op(ops, IS_SECOND, 0
                                    ,(float)1/g_r->Pico_param_record.camera_param.camera_frequency_in_Hz, 0, 0, "s");


    set_op_filename(ops, "Status-%d.gr",g_r->n_rec);
    set_plot_title(ops, "Force %d status ", g_r->n_rec);
    set_plot_x_title(ops, "Time");
    set_plot_y_title(ops, "Status");

    ops->need_to_refresh = 1;



    op = create_and_attach_one_plot(pr, nf, nf, 0);
    ds = op->dat[0];
    set_ds_source(ds,"Zmag recording");
    for (j = 0; j < nf; j++)
        {
            page_n = j/g_r->page_size;
            i_page = j%g_r->page_size;
            ds->xd[j] = (g_r->timing_mode == 1) ? (g_r->imi[page_n][i_page] - g_r->imi_start)
                : g_r->imi[page_n][i_page];
            ds->yd[j] = g_r->zmag[page_n][i_page];
        }

    if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("I can't create plot !");
    set_ds_source(ds,"Rotation recording");

    for (j = 0; j < nf; j++)
        {
            page_n = j/g_r->page_size;
            i_page = j%g_r->page_size;
            ds->xd[j] = (g_r->timing_mode == 1) ? (g_r->imi[page_n][i_page] - g_r->imi_start)
                : g_r->imi[page_n][i_page];
            ds->yd[j] = g_r->rot_mag[page_n][i_page];
        }
    if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("I can't create plot !");
    set_ds_source(ds,"Focus recording");

    for (j = 0; j < nf; j++)
        {
            page_n = j/g_r->page_size;
            i_page = j%g_r->page_size;
            ds->xd[j] = (g_r->timing_mode == 1) ? (g_r->imi[page_n][i_page] - g_r->imi_start)
                : g_r->imi[page_n][i_page];
            ds->yd[j] = g_r->obj_pos[page_n][i_page];
        }
    create_attach_select_x_un_to_op(op, IS_SECOND, 0
                                    ,(float)1/g_r->Pico_param_record.camera_param.camera_frequency_in_Hz, 0, 0, "s");


    set_op_filename(op, "ZmagRotFocusforce%d.gr",g_r->n_rec);
    set_plot_title(op, "Force %d zmag, rotation & focus ", g_r->n_rec);
    set_plot_x_title(op, "Time");
    set_plot_y_title(op, "Zmag, rot, focus");

    op->need_to_refresh = 1;
    find_x_limits(op);
    find_y_limits(op);

    // we grab info from the trk averaging signals
    sc_in = retrieve_scan_info(g_r, &nstep, WIN_CANCEL);

    // we also retrieve info from config file incase no averaging data was found
    b_radius = g_r->Pico_param_record.bead_param.radius;

    //  for (j = 1; j < g_r->n_bead; j++)
    //  dss = create_and_attach_one_ds(ops, nw * sph->nstep, nw * sph->nstep, 0);

    sph->n_expo_div = (sph->n_expo_div < 1) ? 1 : sph->n_expo_div;
    for (i = 0, li = 0; i < nw * sph->nstep ; i++)
        {
            j = i;
            j %= 2*sph->nstep;
            j = (j < sph->nstep) ? j : 2 * sph->nstep -1 - j;
            zmag_tmp = sph->zmag_start + (j * sph->zmag_step);
            for (k = 0; k < sph->n_expo_div; k++)
                {
                    li++;
                    if (sph->mod_expo_enable == 0 || zmag_tmp < sph->zmag_hi_fc) 	       break;
                }
        }
    n_acq = li;
    for (n_maxnper = 0; (sph->nper*(1<<n_maxnper)) < sph->maxnper; n_maxnper++);
    if (sc_in == NULL || nstep < 1)
        {
            //sc_in_found = 0;  // we fill scan info from expected data
            sc_in = (scan_info*)calloc(n_acq,sizeof(scan_info));
            if (sc_in == NULL) return win_printf_OK("Cannot allocate scan info!");

            for (i = 0, li = 0; i < nw * sph->nstep ; i++)
                {
                    j = i;
                    j %= 2*sph->nstep;
                    j = (j < sph->nstep) ? j : 2 * sph->nstep -1 - j;
                    zmag_tmp = sph->zmag_start + (j * sph->zmag_step);

                    for (k = 0; k < sph->n_expo_div; k++)
                        {
                            j = (int)(sph->lambdazmag * (sph->zmag0 - sph->zmag_start - j * sph->zmag_step));
                            j = (j <= n_maxnper) ? j : n_maxnper;
                            tmpi = (j > 0) ?  sph->nper<<j : sph->nper;
                            tmpi = (tmpi < sph->maxnper) ? tmpi : sph->maxnper;
                            nf += tmpi;
                            nf += sph->dead;
                            sc_in[li].zmag = zmag_tmp;
                            sc_in[li].im_n = tmpi;
                            sc_in[li].i_zm = i;
                            sc_in[li].is_zm = k;
                            sc_in[li].nis_zm = sph->n_expo_div;
                            li++;
                            if (sph->mod_expo_enable == 0 || zmag_tmp < sph->zmag_hi_fc)     break;
                        }
                }
        }
    //else sc_in_found = 1;
    if (n_acq < 1) win_printf("nstep %d nw %d nexpo %d, n_acq %d\n zmag expo %f",nw, sph->nstep, sph->n_expo_div,n_acq,sph->zmag_hi_fc);
    //win_printf("nstep %d nw %d nexpo %d, n_acq %d\n zmag expo %f", sph->nstep, nw, sph->n_expo_div,n_acq,sph->zmag_hi_fc);

    if (sc_in != NULL && nstep > 0) n_acq = nstep;
    // to be done check that both info agree

    pr = create_pltreg_with_op(&ops, (n_acq > 0) ? n_acq : 1, (n_acq > 0) ? n_acq : 1, 0);
    //ops = create_and_attach_one_plot(pr, nw * sph->nstep, nw * sph->nstep, 0);
    dss = ops->dat[0];
    set_ds_source(dss,"Zmag vs extension of Force curve");
    dss->nx = dss->ny = 0;
    set_dot_line(dss);
    set_plot_symb(dss, "\\pt5\\oc");
    set_plot_title(ops, "Zmag /extension Bead %d force curve %d", i, g_r->n_rec);
    set_plot_x_title(ops,"Extension (\\mu m)");
    set_plot_y_title(ops,"Zmag (mm)");
    set_plot_file(ops,"Zmag-exbd%01d%03d%s",i,g_r->n_rec,".gr");

    set_ds_source(dss,"Zmag versus extension\nZ mag start = %g for %d frames force curve by steps of %g"
                  " for %d steps %d way(s)\nrot = %g dead period %d\n"
                  "Bead nb %d force curve %s \n"
                  ,sph->zmag_start,sph->nper,sph->zmag_step,sph->nstep,nw,g_r->rot_mag[0][0]
                  ,sph->dead,i,g_r->filename);



    for (i = 0, first = 1; i < g_r->n_bead; i++)
        {
            if (one_bead != 0 && bead_nb != i) continue;
            if (g_r->b_r[i]->completely_losted == 1)  continue;
            b_r = g_r->b_r[i];
            if (g_r->b_r[i]->calib_im == NULL && g_r->SDI_mode == 0) continue; //sdi condition
            //if (fixed_bead >= 0 && fixed_bead < g_r->n_bead)  b_rf = g_r->b_r[fixed_bead];
            if (first == 0)
                {
                    pr = create_pltreg_with_op(&ops, n_acq, n_acq, 0);
                    dss = ops->dat[0];
                    dss->nx = dss->ny = 0;
                    set_dot_line(dss);
                    set_plot_symb(dss, "\\pt5\\oc");
                    set_plot_title(ops, "Zmag /extension Bead %d (force curve %d)", i, g_r->n_rec);
                    set_plot_x_title(ops,"Extension (\\mu m)");
                    set_plot_y_title(ops,"Zmag (mm)");
                    set_plot_file(ops,"Zmag-exbd%01d%03d%s",i,g_r->n_rec,".gr");

                    set_ds_source(dss,"Zmag versus extension\nZ mag start = %g for %d frames force curve by steps of %g"
                                  " for %d steps %d way(s)\nrot = %g dead period %d\n"
                                  "Bead nb %d force curve %s \n"
                                  ,sph->zmag_start,sph->nper,sph->zmag_step,sph->nstep,nw,g_r->rot_mag[0][0]
                                  ,sph->dead,i,g_r->filename);

                    //dss = create_and_attach_one_ds(ops, nw * sph->nstep, nw * sph->nstep, 0);
                }
            snprintf(mes,sizeof(mes),"Force curve bead %d",i);
            change_menu_name_of_current_project(mes);
            if (b_r->s_r == NULL)
                {
                    b_r->s_r = (stiffness_resu*)calloc(n_acq,sizeof(stiffness_resu));
                    if (b_r->s_r == NULL)   return win_printf_OK("Could not allocate space for stiffness result!");
                    b_r->n_s_r = b_r->m_s_r = n_acq;
                    b_r->c_s_r = 0;
                }
            attach_gb_record_to_pltreg(pr, g_r, i);
#ifdef WASBEFORE
            for (j = 0, im0 = 0, i_zmag = 0; (i_zmag < (sph->nstep*nw)) && (j < dss->mx) && ((im0 + sph->setling) < nf);  i_zmag++)
                {
                    k = i_zmag;
                    k %= 2*sph->nstep;
                    k = (k < sph->nstep) ? k : 2 * sph->nstep -1 - k;
                    //zmag_expected = sph->zmag_start + (k * sph->zmag_step);
                    for (l = 0; l < sph->n_expo_div; l++)
                        {
                            //	      win_printf("i_zmag %d l %d",i_zmag,l);
                            k = (int)(sph->lambdazmag * (sph->zmag0 - sph->zmag_start - k * sph->zmag_step));
                            k = (k <= sph->maxnper) ? k : sph->maxnper;
                            nper_expected = (k > 0) ?  sph->nper<<k : sph->nper;
                            nper_expected = (nper_expected <= sph->maxnper) ? nper_expected : sph->maxnper;
#endif

                            for (j = 0, im0 = 0, i_zmag = 0; j < n_acq && (j < dss->mx) && ((im0 + sph->setling) < nf); j++)
                                {
                                    i_zmag = sc_in[j].i_zm;
#ifdef WASBEFORE
                                    zmag_expected = sc_in[j].zmag;
                                    nper_expected = sc_in[j].im_n;
                                    ret = retrieve_image_index_of_acquisition_point(g_r, j, im0, &ims, &ima, &ime, &zmag, &rot, NULL);
                                    if (ret < 0 || ima < 0)
                                        {
                                            win_printf("Pb retrieving force point j = %d im0 %d, ret %d ims %d ima %d ime %d nf %d"
                                                       ,j,im0,ret, ims, ima, ime, nf);
                                            break;
                                        }
                                    if (fabs(zmag - zmag_expected) > 0.005)
                                        win_printf("Pb retrieving data point %d\n"
                                                   "Zmag found %g zmag expected %g",j,zmag,zmag_expected);
                                    n_avg = nper_expected;
                                    if (ime - ima < nper_expected)
                                        {
                                            win_printf("Duration Pb  force point j = %d ima %d duration %d\n"
                                                       "while setling size %d and averaging size %d\n"
                                                       "maxnper %d nper %d"
                                                       ,j,ima,ime - ima,sph->setling, nper_expected,sph->maxnper,sph->nper);
                                            n_avg = ime - ima;
                                        }
#endif
                                    dss->yd[j] = zmag = sc_in[j].zmag;
                                    rot = sc_in[j].rot;
                                    dss->nx = dss->ny = j;
                                    im0 = ima = sc_in[j].ims;
                                    n_avg = sc_in[j].im_n;
                                    //win_printf("For bead %d data retrieved for point %d start %d end %d zmag %g",i,j,im0,ims,zmag);
                                    i_er = average_bead_z_during_part_trk(g_r, i, ima, n_avg, &xavg, &yavg, &zavg, NULL, &profile_invalid);
                                    if (i_er  == -5)		  b_r->s_r[j].error_message = strdup("Z Tracking excessive noise for all acquisition!");
                                    else if (i_er  <= 0) win_printf("Pb retrieving force avg point %d err code %d",j,i_er);
                                    if (profile_invalid && (pr_inva_ok != WIN_CANCEL))    pr_inva_ok = win_printf("Pb invalid profiles in force avg point %d\n"
                                                                                                                  "Press cancel to supress futher warning\n",j);
                                    b_r->s_r[j].zavg = dss->xd[j] = g_r->z_cor * zavg;

                                    if (fixed_bead >= 0 && fixed_bead < g_r->n_bead)
                                        {
                                            average_bead_z_during_part_trk(g_r, fixed_bead, ima, n_avg, &xavgf, &yavgf, &zavgf, NULL, &profile_invalid);
                                            b_r->s_r[j].zavg -= g_r->z_cor * zavgf;
                                            dss->xd[j] -= g_r->z_cor * zavgf;
                                        }

                                    sr = b_r->s_r + j;
                                    sr->zmag = zmag;
                                    sr->rot = rot;
                                    extract_shutter_expo_from_partial_trk(g_r, ima, ima + n_avg, &sr->exp, &sr->gain, &sr->led);
                                    if (exp_min == -1) exp_min = sr->exp;
                                    if (exp_max == -1) exp_max = sr->exp;
                                    if (sr->exp < exp_min) exp_min = sr->exp;
                                    if (sr->exp > exp_max) exp_max = sr->exp;
                                    sr->i_zmag = i_zmag;
#ifdef WASBEFORE
                                    sr->i_exp = l;
                                    sr->n_exp = (sph->mod_expo_enable == 0 || zmag_tmp < sph->zmag_hi_fc) ? 1 : sph->n_expo_div;
#endif
                                    sr->i_exp = sc_in[j].is_zm;
                                    sr->n_exp = sc_in[j].nis_zm;
                                    b_r->c_s_r = j;
                                    if (c_plot && n_avg > 0)
                                        {
                                            if ((sr->n_exp == 1) || (sr->i_exp == 0))
                                                {
                                                    opn = generate_bead_op_during_part_trk(g_r, i, j, ima, n_avg);
                                                    add_data_to_pltreg(pr, IS_ONE_PLOT, (void*)opn);
                                                }
                                            else generate_bead_ds_during_part_trk(opn, g_r, i, j, ima, n_avg);
                                            if (no_kx == 0)
                                                {
                                                    //win_printf("bef analysis at %d n_exp %d",j,sr->n_exp);
                                                    sr->n_lost = checking_bead_not_lost_during_part_trk(g_r, i, ima, n_avg);
                                                    if ((100*sr->n_lost < opn->dat[0]->nx) && (j < b_r->n_s_r)) // we accept 1% lost points
                                                        {

                                                            sr->nxeff = opn->dat[0]->nx;
                                                            i_ds = (opn->n_dat > 2) ? opn->n_dat - 3 : 0;
                                                            mean_y2_in_microns(opn, opn->dat[i_ds], 1, &sr->mxm, &sr->sx2, &sr->sx4);
                                                            mean_y2bin_slidding_average_in_microns(opn, opn->dat[i_ds], 1, 32, &sr->mxm, &sr->sx2, &sr->sx4);
                                                            mean_y2bin_slidding_average_in_microns(opn, opn->dat[i_ds], 2, 32, NULL, &sr->sx2_m2, NULL);
                                                            mean_y2bin_slidding_average_in_microns(opn, opn->dat[i_ds], 4, 32, NULL, &sr->sx2_m4, NULL);
                                                            mean_y2bin_slidding_average_in_microns(opn, opn->dat[i_ds], 8, 32, NULL, &sr->sx2_m8, NULL);

                                                            if (sr->sx2 > MIN_DX2 && opn->n_dat > 2 && (sr->n_exp == 1))
                                                                {
                                                                    //win_printf("performing analysis %d",j);
                                                                    sr->ax = sr->dax = sr->kx = sr->fcx = sr->dfcx = sr->etarx = sr->etar2x = sr->detar2x = sr->dmxm = 0;
                                                                    sr->x_cardinal_er = delta_de_fc_4_auto(opn, opn->dat[i_ds], 10, 0.5, 1, 1, 0, 0.97, b_radius, &sr->nxeff,
                                                                                                           &sr->ax, &sr->dax, &sr->kx, &sr->fcx, &sr->dfcx, &sr->etarx,
                                                                                                           &sr->etar2x, &sr->detar2x, &sr->dmxm);
                                                                    sr->dax = (sr->dax > sr->ax) ? sr->ax : sr->dax;
                                                                }
                                                            //win_printf("analysis at %d n_exp %d",j,sr->n_exp);
                                                            sr->nyeff = opn->dat[1]->nx;
                                                            i_ds = (opn->n_dat > 2) ? opn->n_dat - 2 : 1;
                                                            mean_y2_in_microns(opn, opn->dat[i_ds], 1, &sr->mym, &sr->sy2, &sr->sy4);
                                                            mean_y2bin_slidding_average_in_microns(opn, opn->dat[i_ds], 1, 32, &sr->mym, &sr->sy2, &sr->sy4);
                                                            mean_y2bin_slidding_average_in_microns(opn, opn->dat[i_ds], 2, 32, NULL, &sr->sy2_m2, NULL);
                                                            mean_y2bin_slidding_average_in_microns(opn, opn->dat[i_ds], 4, 32, NULL, &sr->sy2_m4, NULL);
                                                            mean_y2bin_slidding_average_in_microns(opn, opn->dat[i_ds], 8, 32, NULL, &sr->sy2_m8, NULL);
                                                            if (sr->sy2 > MIN_DX2 && opn->n_dat > 2 && (sr->n_exp == 1))
                                                                {
                                                                    sr->ay = sr->day = sr->ky = sr->fcy = sr->dfcy = sr->etary = sr->etar2y = sr->detar2y = sr->dmym = 0;
                                                                    sr->y_cardinal_er = delta_de_fc_4_auto(opn, opn->dat[i_ds], 10, 0.5, 1, 1, 0, 0.97, b_radius, &sr->nyeff,
                                                                                                           &sr->ay, &sr->day, &sr->ky, &sr->fcy, &sr->dfcy, &sr->etary,
                                                                                                           &sr->etar2y, &sr->detar2y, &sr->dmym);
                                                                    if (sr->day > sr->ay) sr->day = sr->ay;
                                                                }
                                                            sr->nzeff = opn->dat[2]->nx;
                                                            i_ds = (opn->n_dat > 1) ? opn->n_dat - 1 : 2;
                                                            mean_y2_in_microns(opn, opn->dat[i_ds], 1, &sr->mzm, &sr->sz2, &sr->sz4);
                                                            mean_y2bin_slidding_average_in_microns(opn, opn->dat[i_ds], 1, 32, &sr->mzm, &sr->sz2, &sr->sz4);
                                                            mean_y2bin_slidding_average_in_microns(opn, opn->dat[i_ds], 2, 32, NULL, &sr->sz2_m2, NULL);
                                                            mean_y2bin_slidding_average_in_microns(opn, opn->dat[i_ds], 4, 32, NULL, &sr->sz2_m4, NULL);
                                                            mean_y2bin_slidding_average_in_microns(opn, opn->dat[i_ds], 8, 32, NULL, &sr->sz2_m8, NULL);
                                                            if (sr->sz2 > MIN_DX2 && opn->n_dat > 2 && profile_invalid == 0  && (sr->n_exp == 1))
                                                                {
                                                                    sr->az = sr->daz = sr->kz = sr->fcz = sr->dfcz = sr->etarz = sr->etar2z = sr->detar2z = sr->dmzm = 0;
                                                                    sr->z_cardinal_er = delta_de_fc_4_auto(opn, opn->dat[i_ds], 10, 0.5, 1, 1, 0, 0.97, b_radius, &sr->nzeff,
                                                                                                           &sr->az, &sr->daz, &sr->kz, &sr->fcz, &sr->dfcz, &sr->etarz,
                                                                                                           &sr->etar2z, &sr->detar2z, &sr->dmzm);
                                                                    if (sr->daz > sr->az) sr->daz = sr->az;
                                                                }

                                                            my_set_window_title("Treating Bead %d acquisition %d",i,j);
                                                        }
                                                    //else  win_printf("analysis at %d not done bead lost %d",j,checking_bead_not_lost_during_part_trk(g_r, i, ima, n_avg));
                                                }
                                            if (b_r->s_r[j].error_message != NULL)
                                                set_plot_x_prime_title(opn, "%s", b_r->s_r[j].error_message);
                                        }

                                    //im0 += sph->setling +n_avg;
                                    im0 = ime;
#ifdef WASBEFORE

                                    if (sph->mod_expo_enable == 0 || zmag_tmp < sph->zmag_hi_fc)
                                        {
                                            sr->n_exp = 1;
                                            break;
                                        }

                                    j++;
                                }
#endif
                        }
                    //win_printf("Exit j loop %d/%d %d  %d < %d",j,n_acq,dss->mx,im0 + sph->setling,nf);
                    if (no_kx == 0)
                        {
                            for (k = 0; k < n_acq && b_r->s_r != NULL; k++)
                                {
                                    sr = b_r->s_r + k;
                                    //win_printf("k = %d i_exp %d n_exp %d",k,sr->i_exp,sr->n_exp);
                                    if (sr->n_exp > 1)
                                        {   // exposure modulation
                                            if (sr->i_exp == 0)
                                                {
                                                    if (opx == NULL)
                                                        {
                                                            opx = create_and_attach_one_plot(pr, sr->n_exp + 3, sr->n_exp + 3, 0);
                                                            dsx = opx->dat[0];
                                                            dsx->nx = dsx->ny = 0;
                                                            set_plot_y_log(opx);
                                                            set_plot_x_log(opx);
                                                            set_plot_title(opx, "Bead %d X variance (force curve %d) Zmag %6.3f", i, g_r->n_rec,sr->zmag);
                                                            set_plot_x_title(opx,"Shutter exposure.");
                                                            set_plot_y_title(opx,"<X - <X>>^2 (nm^2)");
                                                            set_plot_file(opx,"dx2bd%01d%03d%s",i,g_r->n_rec,".gr");
                                                        }
                                                    else
                                                        {
                                                            dsx = create_and_attach_one_ds(opx, sr->n_exp + 3, sr->n_exp + 3, 0);
                                                            dsx->nx = dsx->ny = 0;
                                                        }
                                                    set_dot_line(dsx);
                                                    set_plot_symb(dsx, "\\pt5\\oc");
                                                    set_ds_source(dsx,"X variance point series %d\nZ mag = %g rot %d",sr->i_zmag,sr->zmag,sr->exp);
                                                    add_new_point_with_y_error_to_ds(dsx, 2*sr->exp, 1e6*sr->sx2_m2, (sr->nxeff/4 > 1)
                                                                                     ? 1e6*sr->sx2_m2/sqrt((sr->nxeff/4)-1) : 1e6*sr->sx2_m2);
                                                    add_new_point_with_y_error_to_ds(dsx, 4*sr->exp, 1e6*sr->sx2_m4, (sr->nxeff/8 > 1)
                                                                                     ? 1e6*sr->sx2_m4/sqrt((sr->nxeff/8)-1) : 1e6*sr->sx2_m4);
                                                    add_new_point_with_y_error_to_ds(dsx, 8*sr->exp, 1e6*sr->sx2_m8, (sr->nxeff/16 > 1)
                                                                                     ? 1e6*sr->sx2_m8/sqrt((sr->nxeff/16)-1) : 1e6*sr->sx2_m8);

                                                }
                                            add_new_point_with_y_error_to_ds(dsx, sr->exp, 1e6*sr->sx2, (sr->nxeff/2 > 1)
                                                                             ? 1e6*sr->sx2/sqrt((sr->nxeff/2)-1) : 1e6*sr->sx2);
                                            if ((sr->n_exp - sr->i_exp) == 1)
                                                {
                                                    for (l = k; l >= 0 && b_r->s_r[l].i_exp != 0; l--);
                                                    fit_wong_ds(opx, dsx, &b_r->s_r[l].wong_x2, &b_r->s_r[l].wong_taux, &b_r->s_r[l].wong_chi2x,b_r->s_r[l].zmag);
                                                }
                                        }
                                    if (sr->n_exp > 1)
                                        {   // exposure modulation
                                            if (sr->i_exp == 0)
                                                {
                                                    if (opy == NULL)
                                                        {
                                                            opy = create_and_attach_one_plot(pr, sr->n_exp + 3, sr->n_exp + 3, 0);
                                                            dsy = opy->dat[0];
                                                            dsy->nx = dsy->ny = 0;
                                                            set_plot_y_log(opy);
                                                            set_plot_x_log(opy);
                                                            set_plot_title(opy, "Bead %d Y variance (force curve %d) Zmag %6.3f", i, g_r->n_rec,sr->zmag);
                                                            set_plot_x_title(opy,"Shutter exposure.");
                                                            set_plot_y_title(opy,"<Y - <Y>>^2 (nm^2)");
                                                            set_plot_file(opy,"dy2bd%01d%03d%s",i,g_r->n_rec,".gr");
                                                        }
                                                    else
                                                        {
                                                            dsy = create_and_attach_one_ds(opy, sr->n_exp + 3, sr->n_exp + 3, 0);
                                                            dsy->nx = dsy->ny = 0;
                                                        }
                                                    set_dot_line(dsy);
                                                    set_plot_symb(dsy, "\\pt5\\oc");
                                                    set_ds_source(dsy,"Y variance point series %d\nZ mag = %g rot %d",sr->i_zmag,sr->zmag,sr->exp);
                                                    add_new_point_with_y_error_to_ds(dsy, 2*sr->exp, 1e6*sr->sy2_m2, (sr->nyeff/4 > 1)
                                                                                     ? 1e6*sr->sy2_m2/sqrt((sr->nyeff/4)-1) : 1e6*sr->sy2_m2);
                                                    add_new_point_with_y_error_to_ds(dsy, 4*sr->exp, 1e6*sr->sy2_m4, (sr->nyeff/8 > 1)
                                                                                     ? 1e6*sr->sy2_m4/sqrt((sr->nyeff/8)-1) : 1e6*sr->sy2_m4);
                                                    add_new_point_with_y_error_to_ds(dsy, 8*sr->exp, 1e6*sr->sy2_m8, (sr->nyeff/16 > 1)
                                                                                     ? 1e6*sr->sy2_m8/sqrt((sr->nyeff/16)-1) : 1e6*sr->sy2_m8);
                                                }
                                            add_new_point_with_y_error_to_ds(dsy, sr->exp, 1e6*sr->sy2, (sr->nyeff/2 > 1)
                                                                             ? 1e6*sr->sy2/sqrt((sr->nyeff/2)-1) : 1e6*sr->sy2);
                                            if ((sr->n_exp - sr->i_exp) == 1)
                                                {
                                                    for (l = k; l >= 0 && b_r->s_r[l].i_exp != 0; l--);
                                                    fit_wong_ds(opy, dsy, &b_r->s_r[l].wong_y2, &b_r->s_r[l].wong_tauy, &b_r->s_r[l].wong_chi2y,b_r->s_r[l].zmag);
                                                }
                                        }
                                    if (sr->n_exp > 1)
                                        {   // exposure modulation
                                            if (sr->i_exp == 0)
                                                {
                                                    if (opz == NULL)
                                                        {
                                                            opz = create_and_attach_one_plot(pr, sr->n_exp + 3, sr->n_exp + 3, 0);
                                                            dsz = opz->dat[0];
                                                            dsz->nx = dsz->ny = 0;
                                                            set_plot_y_log(opz);
                                                            set_plot_x_log(opz);
                                                            set_plot_title(opz, "Bead %d Z variance (force curve %d) Zmag %6.3f", i, g_r->n_rec,sr->zmag);
                                                            set_plot_x_title(opz,"Shutter exposure.");
                                                            set_plot_y_title(opz,"<Z - <Z>>^2 (nm^2)");
                                                            set_plot_file(opz,"dz2bd%01d%03d%s",i,g_r->n_rec,".gr");
                                                        }
                                                    else
                                                        {
                                                            dsz = create_and_attach_one_ds(opz, sr->n_exp + 3, sr->n_exp + 3, 0);
                                                            dsz->nx = dsz->ny = 0;
                                                        }
                                                    set_dot_line(dsz);
                                                    set_plot_symb(dsz, "\\pt5\\oc");
                                                    set_ds_source(dsz,"Z variance point series %d\nZ mag = %g rot %d",sr->i_zmag,sr->zmag,sr->exp);
                                                    add_new_point_with_y_error_to_ds(dsz, 2*sr->exp, 1e6*sr->sz2_m2, (sr->nzeff/4 > 1)
                                                                                     ? 1e6*sr->sz2_m2/sqrt((sr->nzeff/4)-1) : 1e6*sr->sz2_m2);
                                                    add_new_point_with_y_error_to_ds(dsz, 4*sr->exp, 1e6*sr->sz2_m4, (sr->nzeff/8 > 1)
                                                                                     ? 1e6*sr->sz2_m4/sqrt((sr->nzeff/8)-1) : 1e6*sr->sz2_m4);
                                                    add_new_point_with_y_error_to_ds(dsz, 8*sr->exp, 1e6*sr->sz2_m8, (sr->nzeff/16 > 1)
                                                                                     ? 1e6*sr->sz2_m8/sqrt((sr->nzeff/16)-1) : 1e6*sr->sz2_m8);

                                                }
                                            add_new_point_with_y_error_to_ds(dsz, sr->exp, 1e6*sr->sz2, (sr->nzeff/2 > 1)
                                                                             ? 1e6*sr->sz2/sqrt((sr->nzeff/2)-1) : 1e6*sr->sz2);
                                            if ((sr->n_exp - sr->i_exp) == 1)
                                                {
                                                    for (l = k; l >= 0 && b_r->s_r[l].i_exp != 0; l--);
                                                    fit_wong_ds(opz, dsz, &b_r->s_r[l].wong_z2, &b_r->s_r[l].wong_tauz, &b_r->s_r[l].wong_chi2z,b_r->s_r[l].zmag);
                                                }
                                        }
                                }


                            op = create_and_attach_one_plot(pr, nw * sph->nstep, nw * sph->nstep, 0);
                            ds = op->dat[0];
                            ds->nx = ds->ny = 0;
                            set_dot_line(ds);
                            set_plot_symb(ds, "\\pt5\\oc");
                            set_plot_title(op, "Bead %d Stiffness (force curve %d)", i, g_r->n_rec);
                            set_plot_x_title(op,"Acquisition Nb.");
                            set_plot_y_title(op,"K_x and K_y (10^{-6}N/m)");
                            set_plot_file(op,"KxKySzbd%01d%03d%s",i,g_r->n_rec,".gr");

                            set_ds_source(ds,"k_x Stiffnes\nZ mag start = %g for %d frames force curve by steps of %g"
                                          " for %d steps %d way(s)\nrot = %g dead period %d\n"
                                          "Bead nb %d force curve %s \n"
                                          ,sph->zmag_start,sph->nper,sph->zmag_step,sph->nstep,nw,g_r->rot_mag[0][0]
                                          ,sph->dead,i,g_r->filename);
                            if (b_r->s_r == NULL)	    win_printf("No stiffness result bead %d",i);

                            for (j = 0; b_r->s_r != NULL && j < b_r->n_s_r && j < n_acq; j++)
                                {
                                    sr = b_r->s_r + j;
                                    if ((sr->n_exp > 1) && (sr->i_exp == 0)) //  (sr->n_exp - sr->i_exp) == 1))
                                        {
                                            if (sr->wong_x2 > 0)	    kxl = (13.8 * 298)/sr->wong_x2;
                                            if (sr->nxeff > 1)	    dkx = kxl/sqrt(sr->nxeff-1);
                                            add_new_point_with_y_error_to_ds(ds, sr->i_zmag, kxl, dkx);
                                        }
                                    if (sr->n_exp == 1)
                                        {
                                            kxl = b_r->s_r[j].kx*1e6;
                                            dkx = b_r->s_r[j].dax;
                                            dkx = (b_r->s_r[j].ax > 0) ? dkx/b_r->s_r[j].ax : 0;
                                            dkx *= kxl;
                                            add_new_point_with_y_error_to_ds(ds, sr->i_zmag, kxl, dkx );
                                        }
                                }


                            if ((ds = create_and_attach_one_ds(op, nw * sph->nstep,  nw * sph->nstep, 0)) == NULL)
                                return win_printf_OK("I can't create plot !");
                            ds->nx = ds->ny = 0;
                            set_dot_line(ds);
                            set_plot_symb(ds, "\\pt5\\oc");
                            set_ds_source(ds,"k_y Stiffnes\nZ mag start = %g for %d frames force curve by steps of %g"
                                          " for %d steps %d way(s)\nrot = %g dead period %d\n"
                                          "Bead nb %d force curve %s \n"
                                          ,sph->zmag_start,sph->nper,sph->zmag_step,sph->nstep,nw,g_r->rot_mag[0][0]
                                          ,sph->dead,i,g_r->filename);



                            for (j = 0; b_r->s_r != NULL && j < b_r->n_s_r && j < n_acq; j++)
                                {
                                    sr = b_r->s_r + j;
                                    if ((sr->n_exp > 1) && (sr->i_exp == 0)) //  (sr->n_exp - sr->i_exp) == 1))
                                        {
                                            if (sr->wong_y2 > 0)	    kxl = (13.8 * 298)/sr->wong_y2;
                                            if (sr->nyeff > 1)	    dkx = kxl/sqrt(sr->nyeff-1);
                                            add_new_point_with_y_error_to_ds(ds, sr->i_zmag, kxl, dkx);
                                        }
                                    if (sr->n_exp == 1)
                                        {
                                            kxl = b_r->s_r[j].ky*1e6;
                                            dkx = b_r->s_r[j].day;
                                            dkx = (b_r->s_r[j].ay > 0) ? dkx/b_r->s_r[j].ay : 0;
                                            dkx *= kxl;
                                            add_new_point_with_y_error_to_ds(ds, sr->i_zmag, kxl, dkx );
                                        }
                                }


                            if ((ds = create_and_attach_one_ds(op, nw * sph->nstep,  nw * sph->nstep, 0)) == NULL)
                                return win_printf_OK("I can't create plot !");
                            ds->nx = ds->ny = 0;
                            set_dot_line(ds);
                            set_plot_symb(ds, "\\pt5\\oc");
                            set_ds_source(ds,"k_z Stiffnes\nZ mag start = %g for %d frames force curve by steps of %g"
                                          " for %d steps %d way(s)\nrot = %g dead period %d\n"
                                          "Bead nb %d force curve %s \n"
                                          ,sph->zmag_start,sph->nper,sph->zmag_step,sph->nstep,nw,g_r->rot_mag[0][0]
                                          ,sph->dead,i,g_r->filename);

                            for (j = 0; b_r->s_r != NULL && j < b_r->n_s_r && j < n_acq; j++)
                                {
                                    sr = b_r->s_r + j;
                                    if ((sr->n_exp > 1) && (sr->i_exp == 0)) //  (sr->n_exp - sr->i_exp) == 1))
                                        {
                                            if (sr->wong_z2 > 0)	    kxl = (13.8 * 298)/sr->wong_z2;
                                            if (sr->nzeff > 1)	    dkx = kxl/sqrt(sr->nzeff-1);
                                            add_new_point_with_y_error_to_ds(ds, sr->i_zmag, kxl, dkx);
                                        }
                                    if (sr->n_exp == 1)
                                        {
                                            kxl = b_r->s_r[j].kz*1e6;
                                            dkx = b_r->s_r[j].daz;
                                            dkx = (b_r->s_r[j].az > 0) ? dkx/b_r->s_r[j].az : 0;
                                            dkx *= kxl;
                                            add_new_point_with_y_error_to_ds(ds, sr->i_zmag, kxl, dkx );
                                        }
                                }


                            op = create_and_attach_one_plot(pr, nw * sph->nstep, nw * sph->nstep, 0);
                            ds = op->dat[0];
                            ds->nx = ds->ny = 0;
                            set_dot_line(ds);
                            set_plot_symb(ds, "\\pt5\\oc");
                            set_plot_title(op, "Bead %d frequency cutoff (force curve %d)", i, g_r->n_rec);
                            set_plot_x_title(op,"Acquisition Nb.");
                            set_plot_y_title(op,"f_c (Hz)");
                            set_plot_file(op,"fcSzbd%01d%03d%s",i,g_r->n_rec,".gr");

                            set_ds_source(ds,"f_{cx} cutoof requency\nZ mag start = %g for %d frames force curve by steps of %g"
                                          " for %d steps %d way(s)\nrot = %g dead period %d\n"
                                          "Bead nb %d force curve %s \n"
                                          ,sph->zmag_start,sph->nper,sph->zmag_step,sph->nstep,nw,g_r->rot_mag[0][0]
                                          ,sph->dead,i,g_r->filename);




                            for (j = 0; b_r->s_r != NULL && j < b_r->n_s_r && j < n_acq; j++)
                                {
                                    sr = b_r->s_r + j;
                                    if ((sr->n_exp > 1) && (sr->i_exp == 0)) //  (sr->n_exp - sr->i_exp) == 1))
                                        {
                                            fc = sr->wong_taux;
                                            if (fc > 0) fc = ((float)1e6)/fc;
                                            fc /= (M_PI * 2);
                                            dfc = 0;
                                            add_new_point_with_y_error_to_ds(ds, sr->i_zmag, fc, dfc );
                                        }
                                    if (sr->n_exp == 1)
                                        {

                                            fc = sr->fcx;
                                            if (sr->nxeff) fc /= sr->nxeff;
                                            fc *= g_r->Pico_param_record.camera_param.camera_frequency_in_Hz;
                                            if (fc > g_r->Pico_param_record.camera_param.camera_frequency_in_Hz)
                                                fc = g_r->Pico_param_record.camera_param.camera_frequency_in_Hz;
                                            dfc = sr->dfcx;
                                            if (b_r->s_r[j].nxeff) dfc /= b_r->s_r[j].nxeff;
                                            dfc *= g_r->Pico_param_record.camera_param.camera_frequency_in_Hz;
                                            if (dfc > g_r->Pico_param_record.camera_param.camera_frequency_in_Hz)
                                                dfc = g_r->Pico_param_record.camera_param.camera_frequency_in_Hz;
                                            add_new_point_with_y_error_to_ds(ds, sr->i_zmag, fc, dfc );
                                        }
                                }

                            if ((ds = create_and_attach_one_ds(op, nw * sph->nstep,  nw * sph->nstep, 0)) == NULL)
                                return win_printf_OK("I can't create plot !");
                            ds->nx = ds->ny = 0;
                            set_dot_line(ds);
                            set_plot_symb(ds, "\\pt5\\oc");
                            set_ds_source(ds,"f_{cy} cutoff frequency\nZ mag start = %g for %d frames force curve by steps of %g"
                                          " for %d steps %d way(s)\nrot = %g dead period %d\n"
                                          "Bead nb %d force curve %s \n"
                                          ,sph->zmag_start,sph->nper,sph->zmag_step,sph->nstep,nw,g_r->rot_mag[0][0]
                                          ,sph->dead,i,g_r->filename);



                            for (j = 0; b_r->s_r != NULL && j < b_r->n_s_r && j < n_acq; j++)
                                {
                                    sr = b_r->s_r + j;
                                    if ((sr->n_exp > 1) && (sr->i_exp == 0)) //  (sr->n_exp - sr->i_exp) == 1))
                                        {
                                            fc = sr->wong_tauy;
                                            if (fc > 0) fc = ((float)1e6)/fc;
                                            fc /= (M_PI * 2);
                                            dfc = 0;
                                            add_new_point_with_y_error_to_ds(ds, sr->i_zmag, fc, dfc );
                                        }
                                    if (sr->n_exp == 1)
                                        {
                                            fc = sr->fcy;
                                            if (sr->nyeff) fc /= sr->nyeff;
                                            fc *= g_r->Pico_param_record.camera_param.camera_frequency_in_Hz;
                                            if (fc > g_r->Pico_param_record.camera_param.camera_frequency_in_Hz)
                                                fc = g_r->Pico_param_record.camera_param.camera_frequency_in_Hz;
                                            dfc = sr->dfcy;
                                            if (b_r->s_r[j].nyeff) dfc /= b_r->s_r[j].nyeff;
                                            dfc *= g_r->Pico_param_record.camera_param.camera_frequency_in_Hz;
                                            if (dfc > g_r->Pico_param_record.camera_param.camera_frequency_in_Hz)
                                                dfc = g_r->Pico_param_record.camera_param.camera_frequency_in_Hz;
                                            add_new_point_with_y_error_to_ds(ds, sr->i_zmag, fc, dfc );
                                        }
                                }

                            op = create_and_attach_one_plot(pr, nw * sph->nstep, nw * sph->nstep, 0);
                            ds = op->dat[0];
                            ds->nx = ds->ny = 0;
                            set_dot_line(ds);
                            set_plot_symb(ds, "\\pt5\\oc");
                            set_plot_title(op, "Bead %d extension (force curve %d)", i, g_r->n_rec);
                            set_plot_x_title(op,"Acquisition Nb.");
                            set_plot_y_title(op,"Extension (\\mu m)");
                            set_plot_file(op,"LengthSzbd%01d%03d%s",i,g_r->n_rec,".gr");

                            set_ds_source(ds,"Extension\nZ mag start = %g for %d frames force curve by steps of %g"
                                          " for %d steps %d way(s)\nrot = %g dead period %d\n"
                                          "Bead nb %d force curve %s \n"
                                          ,sph->zmag_start,sph->nper,sph->zmag_step,sph->nstep,nw,g_r->rot_mag[0][0]
                                          ,sph->dead,i,g_r->filename);
                            for (j = 0; b_r->s_r != NULL && j < b_r->n_s_r && j < n_acq; j++)
                                {
                                    sr = b_r->s_r + j;
                                    if ((sr->n_exp > 1) && (sr->i_exp == 0))
                                        add_new_point_with_y_error_to_ds(ds, sr->i_zmag, b_r->s_r[j].mzm, b_r->s_r[j].dmzm);
                                    else if (sr->n_exp == 1)
                                        add_new_point_with_y_error_to_ds(ds, sr->i_zmag, b_r->s_r[j].mzm, b_r->s_r[j].dmzm);
                                }



                            op = create_and_attach_one_plot(pr, nw * sph->nstep, nw * sph->nstep, 0);
                            ds = op->dat[0];
                            ds->nx = ds->ny = 0;
                            set_dot_line(ds);
                            set_plot_symb(ds, "\\pt5\\oc");
                            set_plot_title(op, "Force curve Bead %d (force curve %d)", i, g_r->n_rec);
                            set_plot_x_title(op,"Extension (\\mu m)");
                            set_plot_y_title(op,"Force (pN)");
                            set_plot_file(op,"Forcebd%01d%03d%s",i,g_r->n_rec,".gr");

                            set_ds_source(ds,"F_x Force\nZ mag start = %g for %d frames force curve by steps of %g"
                                          " for %d steps %d way(s)\nrot = %g dead period %d\n"
                                          "Bead nb %d force curve %s \n"
                                          ,sph->zmag_start,sph->nper,sph->zmag_step,sph->nstep,nw,g_r->rot_mag[0][0]
                                          ,sph->dead,i,g_r->filename);
                            for (j = 0; b_r->s_r != NULL && j < b_r->n_s_r && j < n_acq; j++)
                                {
                                    sr = b_r->s_r + j;
                                    if ((sr->n_exp > 1) && (sr->i_exp == 0)) //  (sr->n_exp - sr->i_exp) == 1))
                                        {
                                            if (sr->wong_x2 > 0)	    kxl = (13.8 * 298)/sr->wong_x2;
                                            kxl *= b_r->s_r[j].mzm;
                                            if (sr->nxeff > 1)	    dkx = kxl/sqrt(sr->nxeff-1);
                                            add_new_point_with_xy_error_to_ds(ds, b_r->s_r[j].mzm, b_r->s_r[j].dmzm, kxl, dkx);
                                        }
                                    if (sr->n_exp == 1)
                                        {
                                            kxl = b_r->s_r[j].kx*1e6;
                                            kxl *= b_r->s_r[j].mzm;
                                            dkx = b_r->s_r[j].dax;
                                            dkx = (b_r->s_r[j].ax > 0) ? dkx/b_r->s_r[j].ax : 0;
                                            dkx *= kxl;
                                            add_new_point_with_xy_error_to_ds(ds, b_r->s_r[j].mzm, b_r->s_r[j].dmzm, kxl, dkx);
                                        }
                                }


                            if ((ds = create_and_attach_one_ds(op, nw * sph->nstep,  nw * sph->nstep, 0)) == NULL)
                                return win_printf_OK("I can't create plot !");
                            ds->nx = ds->ny = 0;
                            set_dot_line(ds);
                            set_plot_symb(ds, "\\pt5\\oc");
                            set_ds_source(ds,"F_y force\nZ mag start = %g for %d frames force curve by steps of %g"
                                          " for %d steps %d way(s)\nrot = %g dead period %d\n"
                                          "Bead nb %d force curve %s \n"
                                          ,sph->zmag_start,sph->nper,sph->zmag_step,sph->nstep,nw,g_r->rot_mag[0][0]
                                          ,sph->dead,i,g_r->filename);

                            for (j = 0; b_r->s_r != NULL && j < b_r->n_s_r && j < n_acq; j++)
                                {
                                    sr = b_r->s_r + j;
                                    if ((sr->n_exp > 1) && (sr->i_exp == 0)) //  (sr->n_exp - sr->i_exp) == 1))
                                        {
                                            if (sr->wong_y2 > 0)	    kxl = (13.8 * 298)/sr->wong_y2;
                                            kxl *= b_r->s_r[j].mzm;
                                            if (sr->nyeff > 1)	    dkx = kxl/sqrt(sr->nyeff-1);
                                            add_new_point_with_xy_error_to_ds(ds, b_r->s_r[j].mzm, b_r->s_r[j].dmzm, kxl, dkx);
                                        }
                                    if (sr->n_exp == 1)
                                        {
                                            kxl = b_r->s_r[j].ky*1e6;
                                            kxl *= b_r->s_r[j].mzm;
                                            dkx = b_r->s_r[j].day;
                                            dkx = (b_r->s_r[j].ay > 0) ? dkx/b_r->s_r[j].ay : 0;
                                            dkx *= kxl;
                                            add_new_point_with_xy_error_to_ds(ds, b_r->s_r[j].mzm, b_r->s_r[j].dmzm, kxl, dkx);
                                        }
                                }




                            op = create_and_attach_one_plot(pr, nw * sph->nstep, nw * sph->nstep, 0);
                            ds = op->dat[0];
                            ds->nx = ds->ny = 0;
                            set_dot_line(ds);
                            set_plot_symb(ds, "\\pt5\\oc");
                            set_plot_title(op, "Force curve %d Bead %d (good points)", g_r->n_rec,i);
                            set_plot_x_title(op,"Extension (\\mu m)");
                            set_plot_y_title(op,"Force (pN)");
                            set_plot_file(op,"Forcebd%01d%03d%s",i,g_r->n_rec,".gr");

                            set_ds_source(ds,"F_x Force\nZ mag start = %g for %d frames force curve by steps of %g"
                                          " for %d steps %d way(s)\nrot = %g dead period %d\n"
                                          "Bead nb %d force curve %s \n"
                                          ,sph->zmag_start,sph->nper,sph->zmag_step,sph->nstep,nw,g_r->rot_mag[0][0]
                                          ,sph->dead,i,g_r->filename);

                            for (j = 0, i_p = 0; b_r->s_r != NULL && j < b_r->n_s_r && j < n_acq; j++)
                                {
                                    sr = b_r->s_r + j;
                                    if ((sr->n_exp > 1) && (sr->i_exp == 0)) //  (sr->n_exp - sr->i_exp) == 1))
                                        {
                                            if (sr->wong_x2 > 0)	    kxl = (13.8 * 298)/sr->wong_x2;
                                            kxl *= b_r->s_r[j].mzm;
                                            if (sr->nxeff > 1)	    dkx = kxl/sqrt(sr->nxeff-1);
                                            add_new_point_with_xy_error_to_ds(ds, b_r->s_r[j].mzm, b_r->s_r[j].dmzm, kxl, dkx);
                                            i_p++;
                                        }
                                    if (sr->n_exp == 1)
                                        {
                                            if (b_r->s_r[j].dax > b_r->s_r[j].ax/4)    continue; // we only keep accurate date
                                            if (b_r->s_r[j].fcx > b_r->s_r[j].nxeff/3) continue; // we only keep accurate date
                                            if ((b_r->s_r[j].etarx > 1e-8 * b_radius) || (b_r->s_r[j].etarx < (1e-9*b_radius)/2))
                                                continue; // we only keep accurate date
                                            kxl = b_r->s_r[j].kx*1e6;
                                            kxl *= b_r->s_r[j].mzm;
                                            dkx = b_r->s_r[j].dax;
                                            dkx = (b_r->s_r[j].ax > 0) ? dkx/b_r->s_r[j].ax : 0;
                                            dkx *= kxl;
                                            add_new_point_with_xy_error_to_ds(ds, b_r->s_r[j].mzm, b_r->s_r[j].dmzm, kxl, dkx);
                                            i_p++;
                                        }
                                }

                            if ((ds = create_and_attach_one_ds(op, nw * sph->nstep,  nw * sph->nstep, 0)) == NULL)
                                return win_printf_OK("I can't create plot !");
                            ds->nx = ds->ny = 0;
                            set_dot_line(ds);
                            set_plot_symb(ds, "\\pt5\\oc");
                            set_ds_source(ds,"F_y force\nZ mag start = %g for %d frames force curve by steps of %g"
                                          " for %d steps %d way(s)\nrot = %g dead period %d\n"
                                          "Bead nb %d force curve %s \n"
                                          ,sph->zmag_start,sph->nper,sph->zmag_step,sph->nstep,nw,g_r->rot_mag[0][0]
                                          ,sph->dead,i,g_r->filename);

                            for (j = 0, i_p = 0; b_r->s_r != NULL && j < b_r->n_s_r && j < n_acq; j++)
                                {
                                    sr = b_r->s_r + j;
                                    if ((sr->n_exp > 1) && (sr->i_exp == 0)) //  (sr->n_exp - sr->i_exp) == 1))
                                        {
                                            if (sr->wong_y2 > 0)	    kxl = (13.8 * 298)/sr->wong_y2;
                                            kxl *= b_r->s_r[j].mzm;
                                            if (sr->nyeff > 1)	    dkx = kxl/sqrt(sr->nyeff-1);
                                            add_new_point_with_xy_error_to_ds(ds, b_r->s_r[j].mzm, b_r->s_r[j].dmzm, kxl, dkx);
                                            i_p++;
                                        }
                                    if (sr->n_exp == 1)
                                        {
                                            if (b_r->s_r[j].day > b_r->s_r[j].ay/4)    continue; // we only keep accurate date
                                            if (b_r->s_r[j].fcy > b_r->s_r[j].nyeff/3) continue; // we only keep accurate date
                                            if ((b_r->s_r[j].etary > 1e-8 * b_radius) || (b_r->s_r[j].etary < (1e-9*b_radius)/2))
                                                continue; // we only keep accurate date
                                            kxl = b_r->s_r[j].ky*1e6;
                                            kxl *= b_r->s_r[j].mzm;
                                            dkx = b_r->s_r[j].day;
                                            dkx = (b_r->s_r[j].ay > 0) ? dkx/b_r->s_r[j].ay : 0;
                                            dkx *= kxl;
                                            add_new_point_with_xy_error_to_ds(ds, b_r->s_r[j].mzm, b_r->s_r[j].dmzm, kxl, dkx);
                                            i_p++;
                                        }
                                }
                            if (2* nw * sph->nstep > i_p)
                                set_plot_x_prime_title(op, "%d points were excluded from this curve",2* nw * sph->nstep - i_p);




# ifdef ZMAG_2_FORCE

                            op = create_and_attach_one_plot(pr, nw * sph->nstep, nw * sph->nstep, 0);
                            ds = op->dat[0];
                            ds->nx = ds->ny = 0;
                            set_dot_line(ds);
                            set_plot_symb(ds, "\\pt5\\oc");
                            set_plot_title(op, "Force curve Bead %d force curve %d", i, g_r->n_rec);
                            set_plot_x_title(op,"Force estimated (pN)");
                            set_plot_y_title(op,"Force measured (pN)");
                            set_plot_file(op,"Forcecompbd%01d%03d%s",i,g_r->n_rec,".gr");

                            set_ds_source(ds,"F_x Force comparison\nZ mag start = %g for %d frames force curve by steps of %g"
                                          " for %d steps %d way(s)\nrot = %g dead period %d\n"
                                          "Bead nb %d force curve %s \n"
                                          ,sph->zmag_start,sph->nper,sph->zmag_step,sph->nstep,nw,g_r->rot_mag[0][0]
                                          ,sph->dead,i,g_r->filename);
                            for (j = 0; j < nw * sph->nstep; j++)
                                {
                                    kxl = b_r->s_r[j].kx*1e6;
                                    dkx = b_r->s_r[j].dax;
                                    dkx = (b_r->s_r[j].ax > 0) ? dkx/b_r->s_r[j].ax : 0;
                                    dkx *= kxl;
                                    kxl *= b_r->s_r[j].mzm;
                                    dkx *= b_r->s_r[j].mzm;
                                    add_new_point_with_y_error_to_ds(ds, b_r->s_r[j].zmag, kxl, dkx);
                                }


                            if ((ds = create_and_attach_one_ds(op, nw * sph->nstep,  nw * sph->nstep, 0)) == NULL)
                                return win_printf_OK("I can't create plot !");
                            ds->nx = ds->ny = 0;
                            set_dot_line(ds);
                            set_plot_symb(ds, "\\pt5\\oc");
                            set_ds_source(ds,"F_y force\nZ mag start = %g for %d frames force curve by steps of %g"
                                          " for %d steps %d way(s)\nrot = %g dead period %d\n"
                                          "Bead nb %d force curve %s \n"
                                          ,sph->zmag_start,sph->nper,sph->zmag_step,sph->nstep,nw,g_r->rot_mag[0][0]
                                          ,sph->dead,i,g_r->filename);
                            for (j = 0; j < nw * sph->nstep; j++)
                                {
                                    kxl = b_r->s_r[j].ky*1e6;
                                    dkx = b_r->s_r[j].day;
                                    dkx = (b_r->s_r[j].ay > 0) ? dkx/b_r->s_r[j].ay : 0;
                                    dkx *= kxl;
                                    kxl *= b_r->s_r[j].mzm;
                                    dkx *= b_r->s_r[j].mzm;
                                    add_new_point_with_y_error_to_ds(ds, b_r->s_r[j].zmag, kxl, dkx);
                                }

# endif



                            op = create_and_attach_one_plot(pr, nw * sph->nstep, nw * sph->nstep, 0);
                            ds = op->dat[0];
                            ds->nx = ds->ny = 0;
                            set_dot_line(ds);
                            set_plot_symb(ds, "\\pt5\\oc");
                            set_plot_title(op, "Bead %d mean positions (force curve %d)", i, g_r->n_rec);
                            set_plot_y_title(op,"Extension (\\mu m)");
                            set_plot_x_title(op,"Mean X or Y position(\\mu m)");
                            set_plot_file(op,"ZvsYbd%01d%03d%s",i,g_r->n_rec,".gr");

                            set_ds_source(ds,"Z versus Y Force\nZ mag start = %g for %d frames force curve by steps of %g"
                                          " for %d steps %d way(s)\nrot = %g dead period %d\n"
                                          "Bead nb %d force curve %s \n"
                                          ,sph->zmag_start,sph->nper,sph->zmag_step,sph->nstep,nw,g_r->rot_mag[0][0]
                                          ,sph->dead,i,g_r->filename);

                            for (j = 0; b_r->s_r != NULL && j < b_r->n_s_r && j < n_acq; j++)
                                {
                                    sr = b_r->s_r + j;
                                    if ((sr->n_exp > 1) && (sr->i_exp == 0)) //  (sr->n_exp - sr->i_exp) == 1))
                                        add_new_point_with_xy_error_to_ds(ds, b_r->s_r[j].mxm, b_r->s_r[j].dmxm, b_r->s_r[j].mzm, b_r->s_r[j].dmzm);
                                    else if (sr->n_exp == 1)
                                        add_new_point_with_xy_error_to_ds(ds, b_r->s_r[j].mxm, b_r->s_r[j].dmxm, b_r->s_r[j].mzm, b_r->s_r[j].dmzm);
                                }

                            /*
                              for (j = 0; j < nw * sph->nstep; j++)
                              {
                              add_new_point_with_xy_error_to_ds(ds, b_r->s_r[j].mxm, b_r->s_r[j].dmxm, b_r->s_r[j].mzm, b_r->s_r[j].dmzm);
                              }
                            */

                            if ((ds = create_and_attach_one_ds(op, nw * sph->nstep,  nw * sph->nstep, 0)) == NULL)
                                return win_printf_OK("I can't create plot !");
                            ds->nx = ds->ny = 0;
                            set_dot_line(ds);
                            set_plot_symb(ds, "\\pt5\\oc");
                            set_ds_source(ds,"Z versus Y\nZ mag start = %g for %d frames force curve by steps of %g"
                                          " for %d steps %d way(s)\nrot = %g dead period %d\n"
                                          "Bead nb %d force curve %s \n"
                                          ,sph->zmag_start,sph->nper,sph->zmag_step,sph->nstep,nw,g_r->rot_mag[0][0]
                                          ,sph->dead,i,g_r->filename);
                            for (j = 0; b_r->s_r != NULL && j < b_r->n_s_r && j < n_acq; j++)
                                {
                                    sr = b_r->s_r + j;
                                    if ((sr->n_exp > 1) && (sr->i_exp == 0)) //  (sr->n_exp - sr->i_exp) == 1))
                                        add_new_point_with_xy_error_to_ds(ds, b_r->s_r[j].mym, b_r->s_r[j].dmym, b_r->s_r[j].mzm, b_r->s_r[j].dmzm);
                                    else if (sr->n_exp == 1)
                                        add_new_point_with_xy_error_to_ds(ds, b_r->s_r[j].mym, b_r->s_r[j].dmym, b_r->s_r[j].mzm, b_r->s_r[j].dmzm);
                                }

                            op = create_and_attach_one_plot(pr, nw * sph->nstep, nw * sph->nstep, 0);
                            ds = op->dat[0];
                            ds->nx = ds->ny = 0;
                            set_dot_line(ds);
                            set_plot_symb(ds, "\\pt5\\di");
                            set_plot_title(op, "Force vs Zmag Bead %d (force curve %d)", i, g_r->n_rec);
                            set_plot_y_title(op,"Force (pN)");
                            set_plot_x_title(op,"Zmag (mm)");
                            set_plot_file(op,"Force-Zmag-bd%01d-%03d%s",i,g_r->n_rec,".gr");

                            set_ds_source(ds,"Force f_{x} versus Zmag\nZ mag start = %g for %d frames force curve by steps of %g"
                                          " for %d steps %d way(s)\nrot = %g dead period %d\n"
                                          "Bead nb %d force curve %s \n"
                                          ,sph->zmag_start,sph->nper,sph->zmag_step,sph->nstep,nw,g_r->rot_mag[0][0]
                                          ,sph->dead,i,g_r->filename);

                            for (j = 0; b_r->s_r != NULL && j < b_r->n_s_r && j < n_acq; j++)
                                {
                                    sr = b_r->s_r + j;
                                    if ((sr->n_exp > 1) && (sr->i_exp == 0)) //  (sr->n_exp - sr->i_exp) == 1))
                                        {
                                            if (sr->wong_x2 > 0)	    kxl = (13.8 * 298)/sr->wong_x2;
                                            kxl *= b_r->s_r[j].mzm;
                                            if (sr->nxeff > 1)	    dkx = kxl/sqrt(sr->nxeff-1);
                                            add_new_point_with_y_error_to_ds(ds, b_r->s_r[j].zmag, kxl, dkx);
                                        }
                                    if (sr->n_exp == 1)
                                        {
                                            kxl = b_r->s_r[j].kx*1e6;
                                            kxl *= b_r->s_r[j].mzm;
                                            dkx = b_r->s_r[j].dax;
                                            dkx = (b_r->s_r[j].ax > 0) ? dkx/b_r->s_r[j].ax : 0;
                                            dkx *= kxl;
                                            add_new_point_with_y_error_to_ds(ds, b_r->s_r[j].zmag, kxl, dkx);
                                        }
                                }

                            if ((ds = create_and_attach_one_ds(op, nw * sph->nstep,  nw * sph->nstep, 0)) == NULL)
                                return win_printf_OK("I can't create plot !");
                            ds->nx = ds->ny = 0;
                            set_dot_line(ds);
                            set_plot_symb(ds, "\\pt5\\di");

                            set_ds_source(ds,"Force f_{y} versus Zmag\nZ mag start = %g for %d frames force curve by steps of %g"
                                          " for %d steps %d way(s)\nrot = %g dead period %d\n"
                                          "Bead nb %d force curve %s \n"
                                          ,sph->zmag_start,sph->nper,sph->zmag_step,sph->nstep,nw,g_r->rot_mag[0][0]
                                          ,sph->dead,i,g_r->filename);
                            for (j = 0; b_r->s_r != NULL && j < b_r->n_s_r && j < n_acq; j++)
                                {
                                    sr = b_r->s_r + j;
                                    if ((sr->n_exp > 1) && (sr->i_exp == 0)) //  (sr->n_exp - sr->i_exp) == 1))
                                        {
                                            if (sr->wong_y2 > 0)	    kxl = (13.8 * 298)/sr->wong_y2;
                                            kxl *= b_r->s_r[j].mzm;
                                            if (sr->nyeff > 1)	    dkx = kxl/sqrt(sr->nyeff-1);
                                            add_new_point_with_y_error_to_ds(ds, b_r->s_r[j].zmag, kxl, dkx);
                                        }
                                    if (sr->n_exp == 1)
                                        {
                                            kxl = b_r->s_r[j].ky*1e6;
                                            kxl *= b_r->s_r[j].mzm;
                                            dkx = b_r->s_r[j].day;
                                            dkx = (b_r->s_r[j].ay > 0) ? dkx/b_r->s_r[j].ay : 0;
                                            dkx *= kxl;
                                            add_new_point_with_y_error_to_ds(ds, b_r->s_r[j].zmag, kxl, dkx);
                                        }
                                }
                        }
                    first = 0;
                    opx = NULL;
                    opy = NULL;
                    opz = NULL;
                }



            switch_plot(pr, pr->n_op - 1);

            free(sph);
            return refresh_plot(pr, pr->n_op - 1);
        }


    int grab_force_steps_from_trk(void)
    {
        int  i, n_step, imin = 0, imax = 0;
        pltreg *pr = NULL;
        g_record *g_r = NULL;
        int nf;
        float min = 0, max = 0;
        scan_info *sc_in = NULL;

        ac_grep(cur_ac_reg,"%pr",&pr);
        g_r = find_g_record_in_pltreg(pr);
        if(updating_menu_state != 0)
            {  // we wait for recording finish
                if (g_r == NULL || working_gen_record == g_r)
                    active_menu->flags |=  D_DISABLED;
                else active_menu->flags &= ~D_DISABLED;
                return D_O_K;
            }
        if (pr == NULL)return 0;
        if (g_r == NULL) return OFF;
        nf = g_r->abs_pos;
        if (nf <= 0) return OFF;


        sc_in = retrieve_scan_info(g_r, &n_step, 0);
        if (sc_in == NULL) return 1;
        for (i = 0; i < n_step; i++)
            {
                if (i == 0) min = max = sc_in[i].zmag;
                if (min > sc_in[i].zmag) min = sc_in[i].zmag;
                if (max < sc_in[i].zmag) max = sc_in[i].zmag;
            }
        win_printf("%d steps zmag min %g max %g",n_step,min,max);
        for (i = 0; i < n_step; i++)
            {
                if (i == 0) min = max = sc_in[i].rot;
                if (min > sc_in[i].rot) min = sc_in[i].rot;
                if (max < sc_in[i].rot) max = sc_in[i].rot;
            }
        win_printf("%d steps rot min %g max %g",n_step,min,max);

        for (i = 0; i < n_step; i++)
            {
                if (i == 0) imin = imax = sc_in[i].shutter;
                if (imin > sc_in[i].shutter) imin = sc_in[i].shutter;
                if (imax < sc_in[i].shutter) imax = sc_in[i].shutter;
            }
        win_printf("%d steps shutter min %d max %d",n_step,imin,imax);

        for (i = 0; i < n_step; i++)
            {
                if (i == 0) min = max = sc_in[i].gain;
                if (min > sc_in[i].gain) min = sc_in[i].gain;
                if (max < sc_in[i].gain) max = sc_in[i].gain;
            }
        win_printf("%d steps gain min %g max %g",n_step,min,max);

        for (i = 0; i < n_step; i++)
            {
                if (i == 0) min = max = sc_in[i].led;
                if (min > sc_in[i].led) min = sc_in[i].led;
                if (max < sc_in[i].led) max = sc_in[i].led;
            }
        win_printf("%d steps led min %g max %g",n_step,min,max);

        return 0;
    }





    //# endif



    MENU *force_rec_plot_menu(void)
    {
        static MENU mn[32] = {0};

        if (mn[0].text != NULL)	return mn;
        add_item_to_menu(mn, "Extract <z>/L_0 from k_z/kx", draw_extension_vs_kz_over_kx, NULL, 0, NULL);
        add_item_to_menu(mn, "Hydrodynamic radius", draw_etar_versus_extension, NULL, 0, NULL);
        add_item_to_menu(mn, "Draw force curve corrected", draw_force_versus_extension_cor, NULL, 0, NULL);
        add_item_to_menu(mn, "Draw estimated force/<z>", draw_estimated_force_versus_extension, NULL, 0, NULL);
        add_item_to_menu(mn, "Compare force to estimated", draw_force_versus_estimated_force_cor, NULL, 0, NULL);
        add_item_to_menu(mn, "Zmag vs <z>", draw_zmag_versus_extension, NULL, 0, NULL);
        add_item_to_menu(mn, "<z> versus <X> or <Y>", draw_mean_position, NULL, 0, NULL);
        add_item_to_menu(mn, "Draw estimated force/zmag", draw_estimated_force_versus_zmag, NULL, 0, NULL);
        add_item_to_menu(mn, "Grab steps", grab_force_steps_from_trk, NULL, 0, NULL);


        return mn;
    }


# endif
