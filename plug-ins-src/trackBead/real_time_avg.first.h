#ifndef _REAL_TIME_AVG_H_
#define _REAL_TIME_AVG_H_

#define DELAY   // Add temporal filtering of bp image

# define live_video_info real_time_info

# define REAL_TIME_BUFFER_SIZE 4096


# define BUF_FREEZED 2
# define BUF_CHANGING 1
# define BUF_UNLOCK 0


# define USER_MOVING_OBJ     0x00000010 
# define OBJ_MOVING          0x00000020 
# define H_LEVELS            2560
# define H_FACTOR            10
# define BP_LEVELS           640
# define BP_FACTOR           10
# define BP_DSL_LEVELS       5000
# define BP_DSL_FACTOR       100

typedef struct gen_real_time
{
  int (*user_real_time_action)(struct gen_real_time *gt, DIALOG *d, void *data);  // a user callback function       
  int c_i, n_i, m_i, lc_i;          // c_i, index of the current image, n_i size of buffer, m_i size allocated
  int ac_i, lac_i;                  // ac_i actual number of frame since acquisition started + local
  int rc_i;                         // rc_i number of frame since last histo reset
  int ac_ri, a_ri;                  // the absolute and relative index of the last pending request
  int ac_pri, a_pri;                // the absolute and relative index of the previous pending request
  int imi[REAL_TIME_BUFFER_SIZE];                         // the image nb
  int imit[REAL_TIME_BUFFER_SIZE];                        // n_inarow
  long long imt[REAL_TIME_BUFFER_SIZE];                   // the image absolute time
  long long imt0;
  long long imtt[REAL_TIME_BUFFER_SIZE];                  // the image absolute time obtained by timer 
  unsigned long imdt[REAL_TIME_BUFFER_SIZE];              // the time spent in the previous function call
  unsigned long imtdt[REAL_TIME_BUFFER_SIZE];             // the time spent before image flip in timer mode
  float obj_pos[REAL_TIME_BUFFER_SIZE];                   // the objective position measured

  int status_flag[REAL_TIME_BUFFER_SIZE];
  float obj_pos_cmd[REAL_TIME_BUFFER_SIZE];               // the objective position command
  char temp_message[REAL_TIME_BUFFER_SIZE];               // a string containing temperature message 
  float avg_pxl[REAL_TIME_BUFFER_SIZE];                   // one pixel value versus time
  int x_pxl;
  int y_pxl;
  int **raw_single_histo;                                 // raw histo of all img in video buffer
  int raw_histo[256];                                     // raw histo of current video buffer
  int raw_histotmp[256];
  int raw_histo_update;
  int raw_min;
  int raw_wi;

  unsigned long *histo_beam_avg;                          // intensity profile of the beam
  int min_peak_thres;
  int inc_peak_thres;
  int ***single_histo;                                    // histo of averaged pixels
  int black_level;                                        // black level camera
  float black_level_avg;                                  // black level avg
  float black_level_bp;                                   // black level bp
  float w_avg;                                            // black width avg
  float w_bp;                                             // black width bp
  int histo[H_LEVELS];                                    // histo of averaged image
  int histotmp[H_LEVELS];
  int histo_update;
  int reset_histo;
  int histo_bp[BP_LEVELS];                                // histo of band-passed image
  int histo_bp_tmp[BP_LEVELS];                            // use this one in non rt thread
  int histo_bp_acc[BP_LEVELS];                            // histo of band-passed image
  int histo_bp_acc_tmp[BP_LEVELS];                        // use this one in non rt thread
  int histo_bp_update;
  int reset_histo_bp;
  long long *dur_since_last;                              // duration since last time this square was above bl_bp+w_bp (has length nf (nb of frame of oifb)
  int histo_dsl[BP_DSL_LEVELS];
  int histo_dsl_tmp[BP_DSL_LEVELS];
  int histo_dsl_update;
  int reset_histo_dsl;
/*   unsigned long t_last_im[BP_LEVELS/BP_FACTOR]; */
  float focus_cor;                                       // the immersion correction factor in Z
  int im_focus_check;                                    // the number of images between two ocus read per
  int com_skip_fr;                                       // communication rate may be different from camera
                                                         // this is the ratio between the two
  O_p *op_tl;
  O_i *oi_avg, *oim, *oif, *oifb, *oic, *oid, *oihbp, *oihpk;
  int mc_i;                                              // c_i, index of the current averaged image
  int rot;                                               // orientation of the beam
  int xc;                                                // beam center coordinates
  int yc;
  int w0;                                                // beam width
  int avg_bin;
  int avg_nx;
  int avg_ny;
/*   int avg_npx;                                           // nb of averaged pixels = nx * ny */
  int avg_nf;

  int fsize, fwidth;
  int f_nf;
  int x0, y0, wx, wy;                                   // interest region
  float lp, lw, hp, hw;

  int delay;                                            // delay between substracted image for bp

  int type;                                             // point measure or timelapse
  int period;                                           // time between 2 measures
  int duration;                                         // totalt duration in min
  int memory;                                           // proportion of accumulated histo kept at each measurments
  long long t_start;                                    // time at time lapse start
  long long t_last;                                     // time at last time lapse measurment
  int n_acq;

} g_real_time;



# ifdef _REAL_TIME_AVG_C_
g_real_time *real_time_info = NULL;
int px0,px1,py0,py1;
float intensity[REAL_TIME_BUFFER_SIZE];
float prev_focus = 0;

float *mn, *s2;

int fft_used_in_real_timeing = 0;
int fft_used_in_dialog = 0;
# else
PXV_VAR(g_real_time*, real_time_info);
PXV_VAR(int, px0);
PXV_VAR(int, px1);
PXV_VAR(int, py0);
PXV_VAR(int, py1);
PXV_VAR(int,x_center);
PXV_VAR(int,y_center);
PXV_VAR(float, prev_focus);

PXV_VAR(int, fft_used_in_real_time);
PXV_VAR(int, fft_used_in_dialog);


# endif

PXV_FUNC(g_real_time *, creating_live_video_info, (void));

PXV_FUNC(MENU *, real_time_avg_menu, (void));
# endif



