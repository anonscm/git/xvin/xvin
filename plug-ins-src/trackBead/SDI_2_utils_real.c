
#ifndef _SDI_2__UTILS_C_
#define _SDI_2__UTILS_C_

# include "SDI_2_utils_real.h"
# include <stdlib.h>
# include <string.h>
# include <math.h>

int SDI_2_align_profile(sdi_fr *f1, double *pos_x)
{

  //align current profile with time averaged signal
  float score = -1;
  float new_score = 0;
  for (int i = -f1->disp_range * f1->discretization ; i<f1->disp_range * f1->discretization ; i++)
  {
    new_score = SDI_2_compute_score_of_alignment(f1,i);
    f1->scores[i+f1->disp_range * f1->discretization] = new_score;

  if ((score == -1) || (new_score < score))
  {
    score=new_score;

    f1->best_align = i;
  }
}
  *pos_x = f1->best_align/(float)(f1->discretization);
  return 0;
}


float SDI_2_compute_score_of_alignment(sdi_fr *f1, int i)
{
  float score = 0;
  int nb_of_points_in_score =0;
  float delta = 0;
  int ind = 0;

  int align_start = 0, align_end = 0;
  if (i <= 0)
  {
    align_start = i;
    while (align_start<0)
    {
      align_start += f1->discretization;
      ind += 1;
    }
    align_end = f1->profile_size+i;
  }
  else
  {
    align_start = i;
    align_end = f1->profile_size + i ;
    while (align_end > f1->profile_size) align_end -= f1->discretization;
  }

  for (int j = align_start;j<align_end;j+=f1->discretization,ind++)
  {
      delta=f1->mean_profile[j] - f1->xi[ind];
      score += delta*delta*f1->photons_per_level/(f1->mean_profile[j]);
      nb_of_points_in_score ++;
    }

  score/=nb_of_points_in_score;

  return score;

}

int SDI_2_interpolate_profile(sdi_fr *f1)
{

  //interpolate current signal on discretized lattice

  float increment = 0;
  int ind = 0;

  if (f1->best_align <= 0)
  {
    f1->temp_start = f1->best_align;
    while (f1->temp_start<0)
    {
      f1->temp_start += f1->discretization;
      ind += 1;
    }
    f1->temp_end = f1->profile_size+f1->best_align;
  }
  else
  {
    f1->temp_start = f1->best_align;
    f1->temp_end = f1->profile_size + f1->best_align ;
    while (f1->temp_end > f1->profile_size) f1->temp_end -= f1->discretization;
  }
    for (int i = f1->temp_start; i<f1->temp_end;i++)
    {
      if ((i-f1->temp_start)%f1->discretization == 0)
      {
        f1->temp_dsignal[i] = f1->xi[ind];
        increment=(f1->xi[ind+1]-f1->xi[ind])/(f1->discretization);
        ind++;
        if (ind == f1->cl) break;
        continue;
      }
      else  f1->temp_dsignal[i] = f1->temp_dsignal[i-1] + increment;
    }

return 0;
}

int SDI_2_update_mean_profile(sdi_fr *f1)
{

  //update mean profile given the interpolation of the actual signal

  for (int i =f1->temp_start;i<f1->temp_end;i++)
  {
      f1->mean_profile[i] = f1->mean_profile[i]*f1->mean_profile_nb[i] + f1->temp_dsignal[i];
      f1->mean_profile_nb[i] += 1;
      f1->mean_profile[i] /=  f1->mean_profile_nb[i];
  }

  return 0;

}


#endif
