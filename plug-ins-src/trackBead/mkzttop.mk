# name of the plug'in
NAME	    		= zdet_topo
# specific plugin flags
LCFLAGS 		= -msse2 -g 
# extra file to zip
ZIPALSO			= 
# name of additional source files (.c and .h are already assumed)
OTHER_FILES 		= 
# name of libraries this plug'in depends on
LIB_DEPS    		= trackBead XVcfg 
# name of the dll to convert in lib.a      
OTHER_LIB_FROM_DLL 	=  	
XVIN_DIR    		=../../
##################################################################################################
include $(XVIN_DIR)platform.mk		# platform specific definitions
include $(XVIN_DIR)plugin.mk 		# plugin construction rules
