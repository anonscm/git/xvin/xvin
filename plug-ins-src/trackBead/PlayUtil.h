#ifndef _PLAYUTIL_H_
#define _PLAYUTIL_H_


# define TRACKING_BUFFER_SIZE 4096
# define PROFILE_BUFFER_SIZE 128

# define OBJ_MOVING          32

#define heap_check()                  check_heap_raw(__FILE__, __LINE__)


# define BUF_FREEZED 2
# define BUF_CHANGING 1
# define BUF_UNLOCK 0

typedef struct gen_tracking
{
  int (*user_tracking_action)(struct gen_tracking *gt, DIALOG *d, void *data);  // a user callback function       
  int c_i, n_i, m_i, lc_i;          // c_i, index of the current image, n_i size of buffer, m_i size allocated
  int ac_i, lac_i;                  // ac_i actual number of frame since acquisition started + local
  float max_obj_mov;                // Maximum relative objective displacement in servo mode
  float z_obj_servo_start;          // objective position at the start of servo mode
  int local_lock;                   // 0 unlock, 1 changing, 2 freezed
  int xi[256], yi[256];             // tmp int buffers to avg profile
  float xf[256], yf[256];           // tmp float buffers to do fft
  int imi[TRACKING_BUFFER_SIZE];                         // the image nb
  int limi[TRACKING_BUFFER_SIZE];                         // local buffer the image nb
  int imit[TRACKING_BUFFER_SIZE];                        // the image nb by timer
  int limit[TRACKING_BUFFER_SIZE];                        // the image nb by timer
  long long imt[TRACKING_BUFFER_SIZE];                   // the image absolute time
  long long limt[TRACKING_BUFFER_SIZE];                   // local the image absolute time
  long long imtt[TRACKING_BUFFER_SIZE];                  // the image absolute time obtained by timer 
  unsigned long imdt[TRACKING_BUFFER_SIZE];              // the time spent in the previous function call
  unsigned long limdt[TRACKING_BUFFER_SIZE];              // local the time spent in the previous function call
  unsigned long imtdt[TRACKING_BUFFER_SIZE];             // the time spent before image flip in timer mode
  float obj_pos[TRACKING_BUFFER_SIZE];                   // the objective position measured
  int status_flag[TRACKING_BUFFER_SIZE];
  float obj_pos_cmd[TRACKING_BUFFER_SIZE];               // the objective position command
} g_track;


# ifdef _PLAYUTIL_C_
g_track *track_info = NULL;
# else
PXV_VAR(g_track*, track_info);
# endif

PXV_FUNC(int, get_present_image_nb, (void));
PXV_FUNC(int, ask_to_update_menu,(void));
PXV_FUNC(int, check_heap_raw,(char *file, unsigned long line));
# endif



