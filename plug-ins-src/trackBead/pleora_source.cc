#pragma once
#include "pleora_source.h"
# include "../cfg_file/Pico_cfg.h"
#include <PvSystem.h>
#include <PvDevice.h>
#include <PvDeviceGEV.h>
#include <PvDeviceU3V.h>
#include <PvDeviceInfo.h>
#include <PvDeviceInfoGEV.h>
#include <PvDeviceInfoU3V.h>
#include <PvStream.h>
#include <PvStreamGEV.h>
#include <PvStreamU3V.h>
#include <PvPixelType.h>
#include <sstream>
#include <vector>
#include <iostream>
#include <iomanip>

PvSystem gSystem;
PvInterface *gInterface;
PvDevice *cameraDevice = NULL;
const PvDeviceInfo *cameraDeviceInfo = NULL;
PvStream *cameraStream = NULL;

int init_source(imreg *imr)
{
    if (!is_camera_available())
    {
        cameraStream = 0;
        do_select_camera();
        retrieve_camera_basic_parameter();
    }

    if (cameraDevice && !cameraStream)
    {
        cameraStream = OpenStream(cameraDeviceInfo);
        ConfigureStream(cameraDevice, cameraStream);
    }

    if (cameraStream)
    {
    }

    return D_O_K;
}
bool is_camera_available(void)
{
    return (cameraDevice && cameraDevice->IsConnected());
}
std::string getDeviceName(const PvDeviceInfo& deviceInfo)
{
    std::stringstream buffer;
    const PvInterface *interface = deviceInfo.GetInterface();
    const PvNetworkAdapter *lNIC = dynamic_cast<const PvNetworkAdapter *>(interface);

    if (lNIC)
    {
        buffer << "[" << lNIC->GetIPAddress().GetAscii() << "] ";
    }
    else
    {
        buffer << "[USB] ";
    }

    buffer << deviceInfo.GetVendorName().GetAscii() << "-" << deviceInfo.GetModelName().GetAscii()
        << "-" << deviceInfo.GetUserDefinedName().GetAscii()
        << std::endl;

    return buffer.str();

}

int do_select_camera(void)
{
    PvResult lResult;
    const PvDeviceInfo *lDeviceInfo = NULL;
    std::stringstream buffer;
    std::vector<const PvDeviceInfo *> deviceVector;
    int selectedDevice = 0;

    if(updating_menu_state != 0)	return D_O_K;

    // Find all GigE Vision devices on the network.
    gSystem.SetDetectionTimeout(200);
    lResult = gSystem.Find();

    if (!lResult.IsOK())
    {
        printf("PvSystem::Find Error: %s\n" , lResult.GetCodeString().GetAscii());
        return -1;
    }

    // Get the number of GEV Interfaces that were found using GetInterfaceCount.
    uint32_t lInterfaceCount = gSystem.GetInterfaceCount();
    // Display information about all found interfaces.
    // For each interface, display information about all devices.
    const PvInterface *lInterface = NULL;

    for (uint32_t x = 0; x < lInterfaceCount; x++)
    {
        // Get pointer to each of interface.
        lInterface = gSystem.GetInterface(x);
        // Get the number of GigE Vision devices that were found using GetDeviceCount.
        uint32_t lDeviceCount = lInterface->GetDeviceCount();

        for (uint32_t y = 0; y < lDeviceCount; y++)
        {
            if (deviceVector.size() == 0)
            {
                buffer << "Select a camera :" << std::endl;
                buffer << "%R";
            }
            else
            {
                buffer << "%r";
            }

            lDeviceInfo = lInterface->GetDeviceInfo(y);
            deviceVector.push_back(lDeviceInfo);
            buffer << getDeviceName(*lDeviceInfo);
        }
    }


    if (deviceVector.size() > 0)
    {
        win_scanf(buffer.str().c_str(), &selectedDevice);
        cameraDeviceInfo = deviceVector[selectedDevice];

        cameraDevice = PvDevice::CreateAndConnect(cameraDeviceInfo, &lResult);

        if (!lResult.IsOK())
        {
            win_printf("Unable to connect to %s : %s\n", getDeviceName(*lDeviceInfo).c_str(), lResult.GetDescription().GetAscii());
        }
    }
    else
    {
        win_printf("No device found\n");
        return 1;
    }
    return 0;
}
int retrieve_camera_basic_parameter(void)
{
    PvGenInteger *intParam;
    int64_t int64Value;

    if (!is_camera_available())
        return 1;

    Pico_param.camera_param.camera_manufacturer =  strdup(cameraDeviceInfo->GetVendorName().GetAscii());
    Pico_param.camera_param.camera_model = strdup(cameraDeviceInfo->GetModelName());

    intParam = cameraDevice->GetParameters()->GetInteger("Width");
    intParam->GetMax(int64Value);
    Pico_param.camera_param.nb_pxl_x = int64Value;

    intParam = cameraDevice->GetParameters()->GetInteger("Height");
    intParam->GetMax(int64Value);
    Pico_param.camera_param.nb_pxl_y = int64Value;

    cameraDevice->GetParameters()->GetEnumValue("PixelFormat", int64Value);
    Pico_param.camera_param.nb_bits = int64Value == PvPixelMono8 ? 8 : int64Value == PvPixelMono10 ? 10 : 12;
    return 0;

}

int configure_and_attach_camera_video_to_oi(imreg *imr)
{

    if (!is_camera_available())
        return 1;

    PvGenParameterArray *params;
    PvGenInteger *widthParam;
    PvGenInteger *heightParam;
    PvGenInteger *offsetXParam;
    PvGenInteger *offsetYParam;

    PvGenInteger *tLLockedParam;
    PvGenInteger *payloadSizeParam;
    PvGenCommand *startParam;
    PvGenCommand *stopParam;
    PvGenInteger *imageCountParam;
    PvGenFloat *FrameRateParam;
    PvGenFloat *BandwidthParam;

    int64_t minWidth, valWidth, maxWidth;
    int64_t minHeight, valHeight, maxHeight;
    int64_t minOffsetX, valOffsetX, maxOffsetX;
    int64_t minOffsetY, valOffsetY, maxOffsetY;

    params = cameraDevice->GetParameters();

    //tLLockedParam = params->GetInteger("TLParamsLocked");
    //payloadSizeParam = params->GetInteger("lPayloadSize");
    //startParam = params->GetCommand("AcquisitionStart");
    //stopParam = params->GetCommand("AcquisitionStop");

    widthParam = params->GetInteger("Width");
    heightParam = params->GetInteger("Height");
    offsetXParam = params->GetInteger("OffsetX");
    offsetYParam = params->GetInteger("OffsetY");

    widthParam->GetMin(minWidth);
    widthParam->GetValue(valWidth);
    widthParam->GetMax(maxWidth);

    heightParam->GetMin(minHeight);
    heightParam->GetValue(valHeight);
    heightParam->GetMax(maxHeight);

    offsetXParam->GetMin(minOffsetX);
    offsetXParam->GetValue(valOffsetX);
    offsetXParam->GetMax(maxOffsetX);

    offsetYParam->GetMin(minOffsetY);
    offsetYParam->GetValue(valOffsetY);
    offsetYParam->GetMax(maxOffsetY);

    int numImageRollingBuffer = cameraStream->GetQueuedBufferMaximum();

    std::stringstream buffer;

    buffer << "Your camera supports small AOI to increase acquisition speed\n"
        "while reducing the field of view. The width and height can only\n"
        "be set at starting time, position may be moved during acquisition\n"
        "Define X0 :"
        << minOffsetX << " < %8lld < " << maxOffsetX
        << "and width :" << minWidth << " < %8lld < " << maxWidth << "\n"
        "Define Y0 :"
        << minOffsetY << " < %8lld < " << maxOffsetY
        << "and height :" << minHeight << " < %8lld < " << maxHeight << "\n"
        "You can select the number of frames in the image rolling buffer\n"
        "This number should correspond a a feww seconds of video\n"
        "Nb. of frames in buffer %8lld\n";

    int win_res = win_scanf(buffer.str().c_str(), &valOffsetX, &valWidth, &valOffsetY, &valHeight, &numImageRollingBuffer);

    if (win_res == CANCEL)
        return -1;

    widthParam->SetValue(valWidth);
    heightParam->SetValue(valHeight);
    offsetXParam->SetValue(valOffsetX);
    offsetYParam->SetValue(valOffsetY);

    Pico_param.camera_param.nb_pxl_x = valWidth;
    Pico_param.camera_param.nb_pxl_y = valHeight;

    oi_TRACK = create_and_attach_movie_to_imr(imr, valWidth, valHeight, IS_CHAR_IMAGE, numImageRollingBuffer);
    CreateStreamBuffers(cameraStream, oi_TRACK);

}

PvStream *OpenStream(const PvDeviceInfo *deviceInfo)
{
    PvStream *lStream = NULL;
    PvResult lResult;

    if (!is_camera_available())
        return NULL;

    const PvDeviceInfoGEV *devgev = dynamic_cast<const PvDeviceInfoGEV * > (deviceInfo);
    const PvDeviceInfoU3V *devu3v = dynamic_cast<const PvDeviceInfoU3V * > (deviceInfo);

    if (devgev)
    {
        PvStreamGEV *stgev = new PvStreamGEV;
        char *ip = strdup(devgev->GetIPAddress().GetAscii());
        stgev->Open(ip);
        lStream = stgev;
    }
    else if (devu3v)
    {
        PvStreamU3V *stu3v = new PvStreamU3V;
        stu3v->Open(devu3v->GetDeviceGUID());
        lStream = stu3v;
    }


    if (lStream == NULL)
    {
        win_printf("Unable to stream from %s\n", cameraDeviceInfo->GetDisplayID().GetAscii());
    }

    return lStream;
}

void ConfigureStream(PvDevice *device, PvStream *aStream)
{

    if (!is_camera_available())
        return;
    // If this is a GigE Vision device, configure GigE Vision specific streaming parameters
    PvDeviceGEV *lDeviceGEV = dynamic_cast<PvDeviceGEV *>(device);

    if (lDeviceGEV != NULL)
    {
        PvStreamGEV *lStreamGEV = static_cast<PvStreamGEV *>(aStream);

        // Negotiate packet size
        lDeviceGEV->NegotiatePacketSize();

        // Configure device streaming destination
        lDeviceGEV->SetStreamDestination(lStreamGEV->GetLocalIPAddress(), lStreamGEV->GetLocalPort());
    }
}

void CreateStreamBuffers(PvStream *aStream, O_i *oi)
{

    oi->im.n_f = (aStream->GetQueuedBufferMaximum() < (uint32_t) oi->im.n_f) ?
        aStream->GetQueuedBufferMaximum() : oi->im.n_f;

    for (int i = 0; i < oi->im.n_f; i++)
    {
        PvBuffer *buffer = new PvBuffer;
        buffer->Attach(oi->im.mem[i], oi->im.nx * oi->im.ny);
        aStream->QueueBuffer(buffer);
    }
}

int do_set_param(void)
{
    char paramstr[1000] = {0};
    std::stringstream ss;
    int win_res = 0;

    if(updating_menu_state != 0)	return D_O_K;

    PvGenParameterArray *params = cameraDevice->GetParameters();

    win_res = win_scanf("parameter name : %s", paramstr);

    if (win_res == CANCEL)
        return 1;

    PvGenParameter *param = params->Get(paramstr);

    if (param == NULL)
    {
        win_printf("no parameter named %s", paramstr);
        return 1;
    }

    PvGenInteger *intparam = dynamic_cast<PvGenInteger *>(param);

    if (intparam)
    {
        int64_t maxval;
        int64_t minval;
        int64_t value;
        intparam->GetMax(maxval);
        intparam->GetMin(minval);
        intparam->GetValue(value);

        ss << paramstr << " Value (int) :" << minval << " <= " << "%lld" << " <= " << maxval;

        win_scanf(ss.str().c_str(), value);

        if (value >= minval && value <= maxval)
            intparam->SetValue(value);

    }

    PvGenFloat *floatparam = dynamic_cast<PvGenFloat *>(param);

    if (floatparam)
    {
        double maxval;
        double minval;
        double value;
        floatparam->GetMax(maxval);
        floatparam->GetMin(minval);
        floatparam->GetValue(value);

        ss << paramstr << " Value (floating point) :" << minval << " <= " << "%f" << " <= " << maxval;

        win_scanf(ss.str().c_str(), value);

        if (value >= minval && value <= maxval)
            floatparam->SetValue(value);
    }


    //TODO PvGenEnum
    //TODO PvGenString

}

template <typename PvGenType, typename ValueType>
int set_param(PvString *paramStr, ValueType value)
{
    PvGenParameterArray *params = cameraDevice->GetParameters();
    PvGenType *param = dynamic_cast<PvGenType>(params->Get(*paramStr));

    if (!param)
        return 1;

    ValueType maxval;
    ValueType minval;
    param->GetMax(maxval);
    param->GetMin(minval);

    if (value >= minval && value <= maxval)
    {
        param->SetValue(value);
        return 0;
    }

    return 1;

}

template <typename PvGenType, typename ValueType>
int set_param_max(PvString *paramStr)
{
    PvGenParameterArray *params = cameraDevice->GetParameters();
    PvGenType *param = dynamic_cast<PvGenType>(params->Get(*paramStr));
    ValueType maxval;

    param->GetMax(maxval);
    param->SetValue(maxval);
}

template <typename PvGenType, typename ValueType>
int set_param_min(PvString *paramStr)
{
    PvGenParameterArray *params = cameraDevice->GetParameters();
    PvGenType *param = dynamic_cast<PvGenType>(params->Get(*paramStr));
    ValueType minval;

    param->GetMin(minval);
    param->SetValue(minval);
}

template <typename PvGenType, typename ValueType>
int get_param(PvString *paramStr, ValueType& value, ValueType& minval, ValueType& maxval)
{

    PvGenParameterArray *params = cameraDevice->GetParameters();
    PvGenType *param = dynamic_cast<PvGenType>(params->Get(*paramStr));

    if (param == NULL)
        return 1;

    param->GetMax(maxval);
    param->GetMin(minval);
    param->GetValue(value);

    return 0;
}

int freeze_source(void)
{
}
int live_source(void)
{
}
int kill_source(void)
{
}

int _get_camera_shutter_in_us (int *set, int *smin, int *smax)
{

}
int _set_camera_shutter_in_us (int exp, int *set, int *smin, int *smax)
{

}
int _get_camera_gain (float *gain, float *fmin, float *fmax, float *finc)
{

}
int _set_camera_gain (float gain, float *set, float *fmin, float *fmax, float *finc)
{

}

void *TrackingThreadProc(void *lpParam)
{
    tid *ltid;
    Bid *p = NULL;
    int currentFrameNum = 0;
    long long t, dt;//, lTimeStamp, llt;

    // Get device parameters need to control streaming
    PvGenParameterArray *lDeviceParams = cameraDevice->GetParameters();

    // Map the GenICam AcquisitionStart and AcquisitionStop commands
    PvGenCommand *lStart = dynamic_cast<PvGenCommand *>(lDeviceParams->Get("AcquisitionStart"));
    PvGenCommand *lStop = dynamic_cast<PvGenCommand *>(lDeviceParams->Get("AcquisitionStop"));

    // Get stream parameters
    PvGenParameterArray *lStreamParams = cameraStream->GetParameters();

    // Map a few GenICam stream stats counters
    PvGenFloat *lFrameRate = dynamic_cast<PvGenFloat *>(lStreamParams->Get("AcquisitionRate"));
    PvGenFloat *lBandwidth = dynamic_cast<PvGenFloat *>(lStreamParams->Get("Bandwidth"));

    // Enable streaming and send the AcquisitionStart command
    std::cout << "Enabling streaming and sending AcquisitionStart command." << std::endl;
    cameraDevice->StreamEnable();
    lStart->Execute();

    char lDoodle[] = "|\\-|-/";
    int lDoodleIndex = 0;
    double lFrameRateVal = 0.0;
    double lBandwidthVal = 0.0;

    // Acquire images until the user instructs us to stop.
    while (go_track)
    {
        PvBuffer *lBuffer = NULL;
        PvResult lOperationResult;

        // Retrieve next buffer
        PvResult lResult = cameraStream->RetrieveBuffer(&lBuffer, &lOperationResult, 1000);

        if (lResult.IsOK())
        {
            if (lOperationResult.IsOK())
            {
                PvPayloadType lType;

                //
                // We now have a valid buffer. This is where you would typically process the buffer.
                // -----------------------------------------------------------------------------------------
                // ...

                lFrameRate->GetValue(lFrameRateVal);
                lBandwidth->GetValue(lBandwidthVal);

                // If the buffer contains an image, display width and height.
                uint32_t lWidth = 0, lHeight = 0;
                lType = lBuffer->GetPayloadType();

                std::cout << std::fixed << std::setprecision(1);
                std::cout << lDoodle[ lDoodleIndex ];
                std::cout << " BlockID: " << std::uppercase << std::hex << std::setfill('0') << std::setw(16) << lBuffer->GetBlockID();

                if (lType == PvPayloadTypeImage)
                {
                    // Get image specific buffer interface.
                    PvImage *lImage = lBuffer->GetImage();

                    // Read width, height.
                    lWidth = lBuffer->GetImage()->GetWidth();
                    lHeight = lBuffer->GetImage()->GetHeight();
                    std::cout << "  W: " << std::dec << lWidth << " H: " << lHeight;
                    switch_frame(oi_TRACK, currentFrameNum % oi_TRACK->im.n_f);
                    t = my_ulclock();
                    dt = my_uclock();
                    p->timer_do(imr_TRACK, oi_TRACK, currentFrameNum, d_TRACK, t , dt , p->param, currentFrameNum % oi_TRACK->im.n_f);
                }
                else
                {
                    std::cout << " (buffer does not contain image)";
                }

                std::cout << "  " << lFrameRateVal << " FPS  " << (lBandwidthVal / 1000000.0) << " Mb/s   \r";
            }
            else
            {
                // Non OK operational result
                std::cout << lDoodle[ lDoodleIndex ] << " " << lOperationResult.GetCodeString().GetAscii() << "\r";
            }

            // Re-queue the buffer in the stream object
            // FIXME : Queue only if buffer treatment finished!
            cameraStream->QueueBuffer(lBuffer);
        }
        else
        {
            // Retrieve buffer failure
            std::cout << lDoodle[ lDoodleIndex ] << " " << lResult.GetCodeString().GetAscii() << "\r";
        }

        ++lDoodleIndex %= 6;
    }

    //PvGetChar(); // Flush key buffer for next stop.
    std::cout << std::endl << std::endl;

    // Tell the device to stop sending images.
    std::cout << "Sending AcquisitionStop command to the device" << std::endl;
    lStop->Execute();

    // Disable streaming on the device
    std::cout << "Disable streaming on the controller." << std::endl;
    cameraDevice->StreamDisable();

    // Abort all buffers from the stream and dequeue
    std::cout << "Aborting buffers still in stream" << std::endl;
    cameraStream->AbortQueuedBuffers();

    while (cameraStream->GetQueuedBufferCount() > 0)
    {
        PvBuffer *lBuffer = NULL;
        PvResult lOperationResult;

        cameraStream->RetrieveBuffer(&lBuffer, &lOperationResult);
    }

}


int init_image_source(imreg *imr, int mode)
{
    int ret;
    O_i *oi = NULL;

    //if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
    if (imr == NULL)    imr = create_and_register_new_image_project( 0,   32,  900,  668);
    if (imr == NULL)    return win_printf_OK("Patate t'es mal barre!");
    //imr_TRACK = imr;
    oi = imr->one_i;
    if ((ret = init_source(imr))) 
    {
        win_printf_OK("Your uEye video camera did not start!");
        return ret; // Initializes UEYE 
    }
    if (mode == 1)      remove_from_image (imr, oi->im.data_type, (void *)oi);

    add_image_treat_menu_item ( "Pleora Camera", NULL, pleora_source_image_menu(), 0, NULL);
    get_camera_shutter_in_us = _get_camera_shutter_in_us;
    set_camera_shutter_in_us = _set_camera_shutter_in_us;
    get_camera_gain = _get_camera_gain;
    set_camera_gain = _set_camera_gain;
    return 0;
}
int start_data_movie(imreg *imr)
{
    return 0;
}

int prepare_image_overlay(O_i* oi)
{
    return 0;
}

MENU *pleora_source_image_menu(void)
{
    static MENU mn[32];

    if (mn[0].text != NULL)	return mn;


    add_item_to_menu(mn,"set parameter", do_set_param,NULL,0,NULL);
    //add_item_to_menu(mn,"chg Freq", do_set_camera_freq,NULL,0,NULL);
    //add_item_to_menu(mn,"chg exposure", do_set_camera_exposure,NULL,0,NULL);
    //add_item_to_menu(mn,"Adjust AOI origin", adj_AOI_position,NULL,0,NULL);
    //add_item_to_menu(mn,"change gains", do_set_camera_gain,NULL,0,NULL);
    //add_item_to_menu(mn,"stop uEye movie", do_stop_camera,NULL,0,NULL);
    //add_item_to_menu(mn,"freeze uEye movie", freeze_source,NULL,0,NULL);
    //add_item_to_menu(mn,"start AVI movie", start_ueye_avi,NULL,0,NULL);
    //add_item_to_menu(mn,"stop AVI movie", stop_ueye_avi,NULL,0,NULL);



    return mn;

}
