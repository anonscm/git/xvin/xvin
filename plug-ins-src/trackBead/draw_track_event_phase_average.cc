#include "draw_track_event_phase_average.hh"

#include "xvin.h"
#include "allegro.h"
#include "../filters/filter_core.h"
#ifdef XV_WIN32
#   include "winalleg.h"
#endif
#include "float.h"
#include "record.h"

#include <boost/filesystem.hpp>
#include <boost/array.hpp>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/median.hpp>
#include <locale.h>
#include "memorized_win_scan.hh"
XV_VAR(int, CAN_KEY_PRESS);

namespace  trackrenorm
{
    std::tuple<bool,int,float> _cleanup_cycles(SaveScript const & script, pltreg * pr, O_p *&op)
    {
        using namespace boost::accumulators;
        std::vector<float> vals;
        vals.reserve(op->n_dat);
        decltype(op->n_dat) cnt = 0;

        accumulator_set<float, stats<tag::median>> info;
        for(auto ds = op->dat, e = ds + op->n_dat; ds != e; ++ds)
        {
            if((*ds)->nx < script.mincount || (*ds)->nx <= 0)
            {
                vals.push_back(-1.f);
                ++cnt;
            }
            else
            {
                accumulator_set<float, stats<tag::median>> med;
                for(auto x = (*ds)->yd, y = x+(*ds)->nx-1; x != y; ++x)
                    med(x[1]-x[0]);
                auto m = median(med);

                accumulator_set<float, stats<tag::median>> meddev;
                for(auto x = (*ds)->yd, y = x+(*ds)->nx-1; x != y; ++x)
                    meddev(std::abs(x[1]-x[0]-m));

                auto md = median(meddev);
                vals.push_back(md);
                if(vals.back() > script.meddev)
                    ++cnt;
                else
                    info(md);
            }

            if(op->n_dat < cnt + script.mincycles)
                return std::make_tuple(true, 0, 0.);
        };



        if(cnt)
        {
            for(decltype(op->n_dat) i = op->n_dat-1; i >= 0; --i)
            {
                if(vals[i] > 0. && vals[i] < script.meddev)
                    continue;

                remove_ds_from_op(op, op->dat[i]);
            };

            op->need_to_refresh = 1;
            refresh_plot(pr, pr->n_op - 1);
        }

        return std::make_tuple(false, vals.size()-cnt, median(info));
    }

    int on_done_one_bead(SaveScript & script, pltreg * pr, O_p *& op)
    {
        namespace fs = boost::filesystem;
        int  quit = 0;
        auto info = _cleanup_cycles(script, pr, op);
        bool del  = std::get<0>(info);
        if(!del)
        {
            char tmp [2048];
            snprintf(tmp, sizeof(tmp), "{\\color{yellow}Key shortcuts on the graphic:}\n"
                                       "  - <Control-Left>  pan left\n"
                                       "  - <Control-Right> pan right\n"
                                       "  - <Control-Up>    zoom in\n"
                                       "  - <Control-Down>  zoom out\n\n"
                                       "{\\color{yellow} Statistics:}\n"
                                       "  - %d   cycles are selected\n"
                                       "  - %.5f is their median deviation\n"
                                       "\n"
                                       "Press OK to save this plot\n"
                                       "Press CANCEL to discard it\n"
                                       "\n"
                                       "Stop everything %%b\n",
                                       std::get<1>(info),
                                       std::get<2>(info));
            CAN_KEY_PRESS = 1;
            del = WIN_CANCEL == win_scanf(tmp, &quit);
            CAN_KEY_PRESS = 0;
        }

        if(!del && script.path.size() == 0)
        {
            fs::path dir;
            char const * path = get_config_string("TRACKING-FILE", "last_loaded", "");
            if(path == nullptr || strlen(path) == 0)
            {
                char * selected = select_folder_config(
                        "Select a directory for saving selected plots", nullptr,
                        script.key1.c_str(), script.key2.c_str());
                if(selected == nullptr || strlen(selected) == 0)
                {
                    del  = true;
                    quit = 1;
                }
                else
                {
                    dir         = selected;
                    script.path = dir.string();
                    set_config_string(script.key1.c_str(),
                                      script.key2.c_str(),
                                      script.path.c_str());
                }
            } else
            {
                dir = path;
                dir = dir.parent_path() / ("selected_" + dir.stem().string() + "/");
                script.path  = dir.string();
            }

        }

        if(del)
        {
            op_menu_kill_op_ask(0);
            op = nullptr;
        }
        else if(script.path.size() > 0)
        {
            fs::path ff    = script.path;
            fs::create_directories(ff);

            ff            /= get_op_filename(op);
            std::string x  = ff.string();

            char fullfile[2048];
            snprintf(fullfile, sizeof(fullfile), "%s", script.path.c_str());
            set_op_path(op, fullfile);

            snprintf(fullfile, sizeof(fullfile), "%s", x.c_str());
            save_one_plot_bin(op, fullfile);
            script.selected.push_back(op);
        }
        return quit != 0 ? WIN_CANCEL : D_O_K;
    }
}
