#pragma once
#ifndef XV_WIN32
# include "config.h"
#endif

#include "xvin.h"

PXV_FUNC(int, change_rotation_string, (float rot));
PXV_FUNC(int, change_zmag_string, (float zmag));
PXV_FUNC(int, change_force_string, (float force));
PXV_FUNC(int, change_Freq_string, (float Freq));
PXV_FUNC(int, change_Load_string, (float Load));
PXV_FUNC(int, change_zmag_label_string, (char *label, int fg, int bg, int mode));
PXV_FUNC(int, change_focus_label_string, (char *label, int fg, int bg, int mode));
PXV_FUNC(int, change_param_label_string, (char *label, int fg, int bg, int mode));
PXV_FUNC(int, generate_plot_dialog, (int w, int h));
PXV_FUNC(int, d_my_edit_proc, (int msg, DIALOG *d, int c));
PXV_FUNC(int, change_magnets_param, (DIALOG *d));
PXV_FUNC(int, change_T0_string, (float T0));
PXV_FUNC(int, change_focus_string, (float focus));
#ifdef RAWHID_V2
PXV_FUNC(int, change_X_string, (float x));
PXV_FUNC(int, change_Y_string, (float y));
#endif
