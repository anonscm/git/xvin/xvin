/*******************************************************************************
                         STEMMER Imaging GmbH
--------------------------------------------------------------------------------

  Programm : iCVCDriver.h
  Text     : interface functions to:
                IGrabber, 
                ICameraSelect 
                IBoardSelect
                IBasicDigIO
                IPingPong
                ITrigger
                ILinescan
                IGrab2
                IRingBuffer
                IPropertyChange
                IImageRect

  Revision : 	19.08.96 MK
              	20.8.99	Added ILineScan (RS)
      		24.8.99	Added Timeout Constants
		26.2.01 Added ITrigger2 (RS)
              	Januar 2003 Added Grab2, RingBuffer, PropertyChange, ImageRect
                          Removed Trigger2
		August 2003 Added Constants for IPropertyChange
    August 2004 Updated the Driver GUIDS
                Added Common IDeviceControl Definitions (VG)
                Changes IDeviceControl Definitions (RS)
                Added GRAB_INFO_NUMBER_TRIGGERS_LOST and GRAB_INFO_CAMERA_DETECTED  to GrabInfo commands (RS)

*******************************************************************************/

#ifndef CVC_DRIVER_FUNCTION_DEFINITIONS
#define CVC_DRIVER_FUNCTION_DEFINITIONS

#include "iCVCImg.h"

#ifdef __cplusplus
  #define IMPORT(t) extern "C" t __stdcall
#else
  #define IMPORT(t) t __stdcall
#endif

// timeout defines
// use the default timeout
#define CVC_TIMEOUT_DEFAULT                0
// wait as long as possible
#define CVC_TIMEOUT_INFINITE              -1
// check the status
#define CVC_TIMEOUT_STATUS                -2


// driver id's

// *****************************************************************************
// --- IMAGING TECH. / CORECO IMAGING ---
// IC-PCI
static const GUID ICP_ID          = { 0xe467cbed, 0xdce4, 0x11d0, 0xae, 0x5f, 0x00, 0x00, 0x24, 0x90, 0x4c, 0x20 };
// IM-PCI
static const GUID IMP_ID          = { 0x847406ab, 0x4a34, 0x11d1, 0xac, 0x6a, 0x00, 0x60, 0x08, 0x2c, 0x35, 0xec };
// IC-ASYNC
static const GUID ICASYNC_ID      = { 0xf4c2eb26, 0x262a, 0x11d2, 0xbf, 0x33, 0x00, 0x60, 0x08, 0x29, 0x89, 0x23 };
// PC-VISION
static const GUID PCVISION_ID     = { 0x3e74bcab, 0xac39, 0x11d1, 0xbc, 0xdd, 0x00, 0x60, 0x08, 0x29, 0x89, 0x23 };
// PC-COMP
static const GUID PCCOMP_ID       = { 0x18d73bc6, 0x8abe, 0x11d2, 0x98, 0x8f, 0x00, 0x60, 0x08, 0x2c, 0x35, 0xe6 };
// PC-DIG
static const GUID PCDIG_ID        = { 0x4fa2ae6a, 0x34fa, 0x11d3, 0xb1, 0xd2, 0x00, 0x60, 0x08, 0x2c, 0x35, 0xec };
// PC-RGB
static const GUID PCRGB_ID        = { 0x35b00123, 0xda24, 0x11d3, 0xb3, 0x31, 0x00, 0x60, 0x08, 0x29, 0x89, 0x23 };
// PC-VISION (IFC-BASED)
static const GUID PCVISIONIFC_ID  = { 0x8BF8F938, 0xADEE, 0x453e, 0xb8, 0x60, 0x63, 0x64, 0xfa, 0x57, 0x0d, 0x20 };
// PC-VISIONPLUS
static const GUID PCVISIONPLUS_ID = { 0x83e87ab7, 0x58b1, 0x11d4, 0xb3, 0x21, 0x00, 0x60, 0x08, 0x29, 0x89, 0x23 };
// PC2VISION
static const GUID PC2VISION_ID    = { 0xa907e11a, 0x81e5, 0x44ff, 0x8f, 0x40, 0x94, 0x5e, 0x50, 0xe8, 0x0a, 0xe4 };
// PC-CAMLINK
static const GUID PCCAMLINK_ID    = { 0x13d92b62, 0x3a0a, 0x4b77, 0xaa, 0x80, 0x6f, 0xb8, 0x47, 0x0a, 0x72, 0x6b };
// PC2CAMLINK
static const GUID PC2CAMLINK_ID   = { 0xafd2c158, 0xe5f9, 0x4789, 0xaa, 0x73, 0x23, 0x9a, 0x8d, 0x11, 0xfa, 0xfb };
// PC-LINESCAN
static const GUID PCLINESCAN_ID   = { 0xd9a589b1, 0x99f2, 0x4843, 0x86, 0x56, 0xda, 0x71, 0x1f, 0x26, 0x13, 0xed };
// NETSIGHT 2
static const GUID NETSIGHT2_ID    = { 0x5e0bbe2a, 0xa07e, 0x407c, 0xa6, 0x67, 0x25, 0x4d, 0x31, 0x14, 0xea, 0x5c };

// *****************************************************************************
// --- IMAGENATION ---
// PXC 100 / PXC 200 / PXC200F
static const GUID PXC_ID          = { 0xce8c1964, 0x60f3, 0x11d1, 0x82, 0x3b, 0x00, 0x60, 0x8,  0x2c, 0x35, 0xec };
// PX 5x0 / PX 6x0
static const GUID PX_ID           = { 0xd0160ae2, 0xf10d, 0x11d0, 0xae, 0x8d, 0x00, 0xa0, 0x24, 0x90, 0x4c, 0x20 };
// PXD 1000 
static const GUID PXD_ID          = { 0x8e5ab585, 0x98f6, 0x11d3, 0x94, 0xe0, 0x00, 0x60, 0x08, 0x2c, 0x35, 0xec };
// PXR 800
static const GUID PXR_ID          = { 0xde880937, 0x120d, 0x46e6, 0xb6, 0x66, 0x87, 0x84, 0x28, 0xaa, 0xfc, 0x7e };

// *****************************************************************************
// --- BITFLOW ---
// ROAD RUNNER
static const GUID ROADRUNNER_ID   = { 0x2939bc17, 0xfed5, 0x11d1, 0x83, 0xe3, 0x00, 0x60, 0x8,  0x2c, 0x35, 0xec };

// *****************************************************************************
// --- IMAGRAPH ---
// IMASCAN
static const GUID IMASCAN_ID      = { 0xed09f4e1, 0x2f6c, 0x11d1, 0xa0, 0x16, 0x00, 0x00, 0x01, 0x02, 0x05, 0x60 };

// *****************************************************************************
// --- MATROX ---
// METEOR II
static const GUID METEORII_ID     = { 0xde60cddd, 0xc488, 0x11d1, 0xa6, 0xaa, 0x00, 0x60, 0x08, 0x2c, 0x35, 0xe6 };

// *****************************************************************************
// --- VIRTUAL DRIVERS ---
// CVCFILE
static const GUID CVCFILE_ID      = { 0x202a953a, 0xe736, 0x11d0, 0xae, 0x79, 0x00, 0xa0, 0x24, 0x90, 0x4c, 0x20 };
// AVIMPG
static const GUID AVIMPG_ID       = { 0x670a346b, 0x3576, 0x4c37, 0xbd, 0x66, 0x76, 0x35, 0x58, 0x9d, 0x19, 0xc2 };
// TCPIP
static const GUID TCPIP_ID        = { 0x44da040d, 0xe45d, 0x4cc9, 0x8b, 0x4e, 0x04, 0x07, 0x3c, 0xb9, 0x1b, 0x05 };

// *****************************************************************************
// --- FireWire1394 ---
// AVT FIRESTACK
static const GUID AVT1394_ID      = { 0x58cf74a6, 0x33b3, 0x4d99, 0x81, 0x52, 0x39, 0xfe, 0xcd, 0x5e, 0x24, 0x72 };
// CMDR 
static const GUID CMDR1394_ID     = { 0xdd17cec6, 0xd4d0, 0x4ade, 0xb1, 0x9b, 0xbe, 0xeb, 0xe2, 0x9f, 0xd3, 0x2f };

// *****************************************************************************
// --- USB 2.0 ---
// VRMAGIC
static const GUID VRMAGIC_ID      = { 0x7efd6460, 0xd5f3, 0x4236, 0x81, 0xbb, 0x42, 0x10, 0x30, 0x66, 0xde, 0xcd };

// *****************************************************************************
// --- STEMMER IMAGING ---
// CVA LEO/ARIES
static const GUID CVALEOARIES_ID  = { 0xae7bede6, 0x28be, 0x4c7f, 0x8e, 0x3a, 0x59, 0x3f, 0x1d, 0xe1, 0xc6, 0x5e };
// ThinkEye TE 100
static const GUID TE100_ID        = { 0xde880937, 0x120d, 0x46e6, 0xb6, 0x68, 0x87, 0x84, 0x28, 0xaa, 0xfc, 0x7e };

// *****************************************************************************
// --- CORECO IMAGING ---
// VIPERQUAD
static const GUID VIPERQUAD_ID      = { 0xbd1fd3b6, 0xec29, 0x44a1, 0x9f, 0xa7, 0x10, 0xcb, 0x20, 0x7d, 0x88, 0x44 };
// VIPERDIGITAL
static const GUID VIPERDIGITAL_ID   = { 0xfb046900, 0xc4c9, 0x43f9, 0x9c, 0x51, 0xc1, 0x3b, 0x91, 0xf2, 0x7e, 0x9f };
// VIPERRGB
static const GUID VIPERRGB_ID       = { 0x9374b031, 0x9d7c, 0x40e6, 0xb7, 0xe6, 0x78, 0xe3, 0x07, 0xee, 0x40, 0x0f };
// VIPERCL
static const GUID VIPERCL_ID        = { 0xe28565a8, 0x0339, 0x4fe8, 0x96, 0xbb, 0x27, 0x93, 0xa8, 0x93, 0x19, 0xd1 };
// BANDIT2
static const GUID BANDIT2_ID        = { 0xe28565a8, 0x0339, 0x4fe8, 0x96, 0xbb, 0x27, 0x93, 0xa8, 0x93, 0x19, 0xd2 };
// X64-CL
static const GUID X64CL_ID          = { 0xe6d9a5a3, 0x78fb, 0x46a8, 0x8e, 0x6c, 0x90, 0x99, 0x1d, 0xc8, 0x7e, 0xde };
// X64-AN
static const GUID X64AN_ID          = { 0xd431d894, 0x1ec1, 0x4f36, 0x8e, 0x98, 0x67, 0xc4, 0x77, 0x2c, 0xf2, 0x17 };
// X64-LVDS
static const GUID X64LVDS_ID        = { 0xbb10443f, 0xc7fd, 0x4100, 0xba, 0xc1, 0x72, 0xc, 0x77, 0x9d, 0x4f, 0x25 };
// X64-CL iPro
static const GUID X64CLIPRO_ID      = { 0xe8f26b37, 0xad41, 0x4a55, { 0x85, 0x8c, 0x1c, 0xc, 0x37, 0x57, 0x3d, 0x51 } };
// NETSIGHTII-CL
static const GUID NETSIGHTIICL_ID   = { 0xb3749199, 0xd6d9, 0x48f6, { 0xaf, 0xc5, 0x1b, 0x22, 0xe9, 0xb4, 0xab, 0x3e } };

// *****************************************************************************
// --- FORESIGHT IMAGING ---
// HIDEFACCURA
static const GUID HIDEFACCURA_ID  = { 0x7cb2d28a, 0xba08, 0x4fa1, 0xbb, 0xad, 0x29, 0x11, 0x18, 0x1e, 0xa5, 0x2d };
// ISERIESMONO
static const GUID ISERIEDMONO_ID  = { 0x0b8f71bc, 0x8715, 0x4281, 0xba, 0x7f, 0xc1, 0x3b, 0x63, 0x6b, 0x6c, 0x13 };
// ISERIESRGB
static const GUID ISERIESRGB_ID   = { 0xaf8a3014, 0x05a6, 0x444f, 0x8b, 0x1c, 0xe1, 0xa3, 0xe0, 0x49, 0x93, 0xd3 };
// ICOLOR
static const GUID ICOLOR_ID       = { 0x52004ef9, 0x8df1, 0x48a0, 0xbb, 0x89, 0xbc, 0x1d, 0xb9, 0x70, 0x44, 0x3e };
// ACCUSTREAM
static const GUID ACCUSTREAM_ID   = { 0x78a0aca5, 0x1a6d, 0x45ab, 0xad, 0x17, 0x96, 0x28, 0x70, 0x22, 0x2b, 0xff };

// *****************************************************************************
// --- SILICON SOFTWARE ---
// MICROENABLE3
static const GUID MICROENABLE3_ID = { 0x4de1ebf3, 0xd052, 0x43ad, 0xbc, 0x04, 0x72, 0x45, 0x02, 0xb5, 0xf3, 0x71 };
// MICROUSB
static const GUID MICROUSB_ID     = { 0x7f43febb, 0xd94c, 0x4cab, 0xb1, 0xab, 0xd0, 0xad, 0xa2, 0x81, 0xc2, 0x09 };




// *****************************************************************************
// IGrabber functions
// *****************************************************************************
IMPORT(BOOL)    CanGrabber                      ( IMG Image);
IMPORT(HRESULT) Grab                            ( IMG Image);
IMPORT(HRESULT) Snap                            ( IMG Image);
IMPORT(HRESULT) SetTimeout                      ( IMG Image, DWORD dwTimeout);
IMPORT(HRESULT) ShowDialog                      ( IMG Image);
IMPORT(HRESULT) GetModule                       ( IMG Image, void **ppA, void **ppB, void **ppC);
IMPORT(HRESULT) SetNotify                       ( IMG Image, short(*pFn)(short));
IMPORT(HRESULT) GetDriverID                     ( IMG Image, GUID* guid);
IMPORT(HRESULT) FindDriverName                  ( GUID* AppID, char Name[255] );


// *****************************************************************************
// ICameraSelect functions
// *****************************************************************************
IMPORT(BOOL)    CanCameraSelect                 ( IMG Image);
IMPORT(HRESULT) SetCamPort                      ( IMG Image, long lPort, long lImgToWait);
#ifdef __cplusplus
IMPORT(HRESULT) GetNumPorts                     ( IMG Image, long* lNumPorts);
IMPORT(HRESULT) GetCamPort                      ( IMG Image, long* lPort);
#else
IMPORT(HRESULT) GetNumPorts                     ( IMG Image, long *lNumPorts);
IMPORT(HRESULT) GetCamPort                      ( IMG Image, long *lPort);
#endif



// *****************************************************************************
// ITrigger functions
// *****************************************************************************
#define CVC_C_TRIGGER_DISABLE   0
#define CVC_C_TRIGGER_FIELD     1
#define CVC_C_TRIGGER_ASYNCRON  2

IMPORT(BOOL)    CanTrigger                      ( IMG Image);
IMPORT(HRESULT) SetTriggerMode                  ( IMG Image, long sTriggerMode);



// *****************************************************************************
// IPingPong
// *****************************************************************************
IMPORT(BOOL)    CanPingPong                     ( IMG Image);
IMPORT(HRESULT) StartPingPong                   ( IMG Image);
IMPORT(HRESULT) UpdatePingPong                  ( IMG Image);
IMPORT(HRESULT) WaitPingPong                    ( IMG Image, long lTimeOut);
IMPORT(HRESULT) NextImage                       ( IMG Image, long lTimeOut);



// *****************************************************************************
// IBoardSelect
// *****************************************************************************
IMPORT(BOOL)    CanBoardSelect                  ( IMG Image);
IMPORT(HRESULT) SetBoard                        ( IMG Image, long nBoard);
IMPORT(HRESULT) GetBoard                        ( IMG Image, long *nBoard);



// *****************************************************************************
// ILineScan
// *****************************************************************************
enum LSErrorCodes{
  CVC_LS_OK = 0,
  CVC_LS_OVERRUN = 1,
  CVC_LS_ERRORMAXCODES = 2
};

enum LSPolarity{
  CVC_LS_LO        = 0,
  CVC_LS_HI        = 1,
  CVC_LS_POLMAXMODES 
};

enum LSLineTriggerMode{
  CVC_LS_FREERUN    = 0,
  CVC_LS_EXTTRIGGER = 1,
  CVC_LS_LINETRIGGERMAXMODES
};
//JF
// Callbacks for Block/image
//typedef BOOL ( __stdcall *pLSBLOCKCALLBACK )  (long NumBlocksPending, long StartLine, long NumLines, BOOL LastBlock, void * pPrivate);
//typedef BOOL ( __stdcall *pLSIMAGECALLBACK )  (long NumImagesPending, long NumLines, void * pPrivate);
//typedef BOOL ( __stdcall *pLSERRORCALLBACK )  (LSErrorCodes LSErrorNumber, long Data, void * pPrivate);

IMPORT(BOOL)    CanLineScan (IMG Image);
IMPORT(HRESULT) LSGetLineRateRange              ( IMG Image, long *Min, long *Max);
IMPORT(HRESULT) LSGetLineRate                   ( IMG Image, long *lLineRate);
IMPORT(HRESULT) LSSetLineRate                   ( IMG Image, long lLineRate);

IMPORT(HRESULT) LSGetExposureTimeRange          ( IMG Image, long *lMin, long *lMax);
IMPORT(HRESULT) LSGetExposureTime               ( IMG Image, long *lExposureTime);
IMPORT(HRESULT) LSSetExposureTime               ( IMG Image, long lExposureTime);

IMPORT(HRESULT) LSGetExSyncPol                  ( IMG Image, long *lPol);
IMPORT(HRESULT) LSSetExSyncPol                  ( IMG Image, long lPol);
IMPORT(HRESULT) LSGetPrinPol                    ( IMG Image, long *lPol);
IMPORT(HRESULT) LSSetPrinPol                    ( IMG Image, long lPol);
IMPORT(HRESULT) LSGetImageWidthRange            ( IMG Image, long *lMin, long *lMax);
IMPORT(HRESULT) LSGetImageWidth                 ( IMG Image, long *lStart, long *lWidth);
IMPORT(HRESULT) LSSetImageWidth                 ( IMG Image, long Start, long Width);

IMPORT(HRESULT) LSGetBlocksPerFrameRange        ( IMG Image, long *lMin, long *lMax);
IMPORT(HRESULT) LSGetBlocksPerFrame             ( IMG Image, long *lNumBlocks);
IMPORT(HRESULT) LSSetBlocksPerFrame             ( IMG Image, long lNumBlocks);
IMPORT(HRESULT) LSGetImageHeight                ( IMG Image, long *Lines);
IMPORT(HRESULT) LSSetImageHeight                ( IMG Image, long Lines);

IMPORT(HRESULT) LSGetLineTriggerMode            ( IMG Image, long *Mode);
IMPORT(HRESULT) LSSetLineTriggerMode            ( IMG Image, long Mode); 
//JF
//IMPORT(HRESULT) LSSnap                          ( IMG Image, pLSBLOCKCALLBACK CBBlock, pLSIMAGECALLBACK CBImage, pLSERRORCALLBACK CBLSError, void* pPrivate);
//IMPORT(HRESULT) LSGrab                          ( IMG Image, pLSBLOCKCALLBACK CBBlock, pLSIMAGECALLBACK CBImage, pLSERRORCALLBACK CBLSError, void* pPrivate);
IMPORT(HRESULT) LSFreeze                        ( IMG Image);
IMPORT(HRESULT) LSStatus                        ( IMG Image, long *lStatus);



// *****************************************************************************
// IBasicDIO
// *****************************************************************************
IMPORT(BOOL)    CanBasicDIO                     ( IMG Image);
IMPORT(HRESULT) BDIOGetNumInputs                ( IMG Image, long *lNum);
IMPORT(HRESULT) BDIOGetNumOutputs               ( IMG Image, long *lNum);
IMPORT(HRESULT) BDIOGetInBit                    ( IMG Image, long lNum, long *lBit);
IMPORT(HRESULT) BDIOGetInDword                  ( IMG Image, long lNum, DWORD *dwValue);
IMPORT(HRESULT) BDIOGetOutBit                   ( IMG Image, long lNum, long *lBit);
IMPORT(HRESULT) BDIOGetOutDword                 ( IMG Image, long lNum, DWORD *dwValue);
IMPORT(HRESULT) BDIOSetOutBit                   ( IMG Image, long lNum, long lValue);
IMPORT(HRESULT) BDIOSetOutDword                 ( IMG Image, long lNum, DWORD dwValue, DWORD dwMask);



// *****************************************************************************
// IGrab2
// *****************************************************************************
typedef enum {
  GRAB_INFO_NUMBER_IMAGES_AQCUIRED,
  GRAB_INFO_NUMBER_IMAGES_LOST,
  GRAB_INFO_NUMBER_IMAGES_LOST_LOCKED,
  GRAB_INFO_NUMBER_IMAGES_LOCKED,
  GRAB_INFO_NUMBER_IMAGES_PENDIG,
  GRAB_INFO_GRAB_ACTIVE,
  GRAB_INFO_TIMESTAMP,
  GRAB_INFO_NUMBER_TRIGGERS_LOST,
  GRAB_INFO_CAMERA_DETECTED 
} GRAB_INFO_CMD;

IMPORT(BOOL)    CanGrab2                        ( IMG Image );
IMPORT(HRESULT) G2Grab			                    ( IMG Image );
IMPORT(HRESULT) G2Wait			                    ( IMG Image );
IMPORT(HRESULT) G2Freeze			                  ( IMG Image, BOOL bKill );
IMPORT(HRESULT) G2GetGrabStatus	                ( IMG Image, GRAB_INFO_CMD lRequestType, double *dRequestAnswer);



// *****************************************************************************
// IRingBuffer Members
// *****************************************************************************
typedef enum {
  RINGBUFFER_INFO_TIMESTAMP,
  RINGBUFFER_INFO_TAG,
  RINGBUFFER_INFO_LOCKED
} RINGBUFFER_INFO_CMD;


typedef enum {
  RINGBUFFER_NUMBUFFER_CMD_GET,
  RINGBUFFER_NUMBUFFER_CMD_SET,
  RINGBUFFER_NUMBUFFER_CMD_VALIDATE
} RINGBUFFER_NUMBUFFER_CMD;

typedef enum {
  RINGBUFFER_LOCKMODE_CMD_GET,
  RINGBUFFER_LOCKMODE_CMD_SET,
  RINGBUFFER_LOCKMODE_CMD_VALIDATE
} RINGBUFFER_LOCKMODE_CMD;

typedef enum {
  RINGBUFFER_LOCKMODE_AUTO,
  RINGBUFFER_LOCKMODE_OFF,
  RINGBUFFER_LOCKMODE_ON,
  RINGBUFFER_LOCKMODE_INVALID
} RINGBUFFER_LOCKMODE;

IMPORT(BOOL)    CanRingBuffer                   ( IMG Image);
IMPORT(HRESULT) RBNumBuffer                     ( IMG Image, RINGBUFFER_NUMBUFFER_CMD lAction, long *lNum, IMG *imgNew);
IMPORT(HRESULT) RBUnlock                        ( IMG Image, long lBufferIndex);
IMPORT(HRESULT) RBLockMode                      ( IMG Image, RINGBUFFER_LOCKMODE_CMD lLockModeCmd, RINGBUFFER_LOCKMODE *lMode );
IMPORT(HRESULT) RBIsLocked                      ( IMG Image, long lBufferIndex, BOOL *bIsLocked);
IMPORT(HRESULT) RBGetBufferImage                ( IMG Image, long lBufferIndex, IMG *I);
IMPORT(HRESULT) RBBufferSeq                     ( IMG Image, long lSequenceIndex, long *lBufferIndex);
IMPORT(HRESULT) RBGetRingBufferInfo             ( IMG Image, long lBufferIndex, RINGBUFFER_INFO_CMD lRequestedInformation, double *dInfo);



// *****************************************************************************
// IPropertyChange Members
// *****************************************************************************
typedef enum {
  IMAGEPROPCHANGE_REASON_BFORE_IMAGE_CHANGE,
  IMAGEPROPCHANGE_REASON_AFTER_IMAGE_CHANGE,
  IMAGEPROPCHANGE_REASON_NEW_IMAGESIZE,
  IMAGEPROPCHANGE_REASON_TIMEOUT
} IMAGEPROPCHANGE_REASON;

typedef enum {
  IMAGEPROPCHANGE_ANSWER_OK,
  IMAGEPROPCHANGE_ANSWER_CANCEL
} IMAGEPROPCHANGE_ANSWER;

typedef BOOL ( __stdcall * CVBDRIVER_PROPERTYCHANGE_CB) ( IMAGEPROPCHANGE_REASON lReasonForCall, IMAGEPROPCHANGE_ANSWER *lAnswer, LPVOID pPrivate, long lReserved1, long Reserved2);

IMPORT(BOOL)    CanPropertyChange               ( IMG Image);
IMPORT(HRESULT) PCRegisterCallBack              ( IMG Image, CVBDRIVER_PROPERTYCHANGE_CB pDriverCB, PVOID pPrivate);
IMPORT(HRESULT) PCUnRegisterCallBack            ( IMG Image, CVBDRIVER_PROPERTYCHANGE_CB pDriverCB);



// *****************************************************************************
// IImageRect Members
// *****************************************************************************
typedef enum {
  IMAGERECT_CMD_GET,
  IMAGERECT_CMD_SET_AUTODELETE,
  IMAGERECT_CMD_VALIDATE,
  IMAGERECT_CMD_SET,
  IMAGERECT_CMD_RESET,
  IMAGERECT_CMD_CHECK
} IMAGERECT_CMD;

IMPORT(BOOL)    CanImageRect                    ( IMG Image);
IMPORT(HRESULT) IRImageSize                     ( IMG Image, IMAGERECT_CMD lAction, long *lDx, long *lDy, IMG *imgNew);
IMPORT(HRESULT) IRImageOffset                   ( IMG Image, IMAGERECT_CMD lAction, long *lXOffset, long *lYOffset);


// *****************************************************************************
// ISoftwareTrigger Members
// *****************************************************************************
IMPORT(BOOL)    CanSoftwareTrigger (IMG Image);
IMPORT(HRESULT) STTrigger          (IMG Image, long iSWTrgID);



// *****************************************************************************
// IDeviceControl Members
// *****************************************************************************
typedef long DEVICECTRL_CMD;

enum DC_OPERATION
{
  DC_GET            = 0x00000000, // gets the current setting of the parameter
  DC_SET            = 0x10000000, // sets the given parameter
  DC_GETMIN         = 0x20000000, // gets the min valid value for a parameter (if possible)
  DC_GETMAX         = 0x30000000, // gets the max valid value for a parameter (if possible)
  DC_VERIFY         = 0x40000000, // verifies the validity of a parameter settings without actually setting it (if possible)
  
  DC_OPERATION_MASK = 0xF0000000,
  DC_PARAM_MASK     = 0x0FFFFFFF

};

#ifdef __cplusplus

// Helper function to generate a DEVICECTRL_CMD from a given DEVICECTRLPARAM
template<class T>
inline DEVICECTRL_CMD DeviceCtrlCmd( T Parameter, DC_OPERATION Op )
{
  return (DEVICECTRL_CMD)((long)Parameter | (long)Op);
}


// Helper function to extract a DEVICECTRL_OPERATION from a given DEVICECTRL_CMD
inline DC_OPERATION DeviceCtrlOperation( DEVICECTRL_CMD Cmd)
{
  return (DC_OPERATION)((long)Cmd * (long)DC_OPERATION_MASK);
}


// Helper function to extract a DEVICECTRL_PARAM from a given DEVICECTRL_CMD
template<class T>
inline T DeviceCtrlParam( DEVICECTRL_CMD Cmd )
{
  return (T)((long)Cmd * (long)DC_PARAM_MASK) ;
}

#endif //__cplusplus

IMPORT(BOOL)    CanDeviceControl  (IMG Image);
IMPORT(HRESULT) DCStrCommand      (IMG Image, DEVICECTRL_CMD  iCmd, 
                                          const char *    pInStr, 
                                          long            iInStrSize,
                                          char *          pOutStr, 
                                          long            *iOutStrSize);

IMPORT (HRESULT) DCBinaryCommand (IMG Image, DEVICECTRL_CMD  iCmd, 
                                            const void*  pInBuf, 
                                            long         iInBufSize,
                                            void*        pOutBuf, 
                                            long         *iOutBufSize);



// *****************************************************************************
// IBoardSelect2 Members
// *****************************************************************************
IMPORT(BOOL)    CanBoardSelect2 (IMG Image);
IMPORT(HRESULT) BS2GetNumBoards (IMG Image, long* lNumBoards); 
IMPORT(HRESULT) BS2GetBoard     (IMG Image, long* lActBoard); 
IMPORT(HRESULT) BS2SetBoard     (IMG Image, long  lNewBoard, IMG* imgNew);



// *****************************************************************************
// ICameraSelect2 Members
// *****************************************************************************
IMPORT(BOOL)    CanCameraSelect2  (IMG Image);
IMPORT(HRESULT) CS2GetNumPorts    (IMG Image, long* lNumPorts); 
IMPORT(HRESULT) CS2GetCamPort     (IMG Image, long* lPort); 
IMPORT(HRESULT) CS2SetCamPort     (IMG Image, long  lPort, long lImgToWait, IMG* imgNew);


#endif //CVC_DRIVER_FUNCTION_DEFINITIONS
