#pragma once
#ifndef XV_WIN32
# include "config.h"
#endif
#include <boost/optional.hpp>
#include <map>
#include <set>
#include <vector>
#include <regex>
#include <utility>

#define CYCLE_PLOT_FILENAME         "Z(t)bd%dtrack%d.gr"
#define CYCLE_PHASE_PLOT_FILENAME   "Z(t)bd%dtrack%dphase%d.gr"
#define HYB_PATTERN_PLOT_FILENAME   "HybProb(Z)bd%dtrack%dphase%d-hyb-pattern.gr"
#define CYCLE_DATASET_SOURCE        "Bead Cycle %d phase(s):"
extern "C" { struct data_set; struct plot_region; struct one_plot; struct gen_record; }
namespace indexing
{
    template <typename T>
    using opt_t     = boost::optional<T>;
    using cycleds_t = std::vector<std::pair<one_plot*, data_set*>>;

    opt_t<int>                  cycleid     (data_set const *cycle_ds);
    opt_t<int>                  trackid     (one_plot const *op);
    opt_t<int>                  beadid      (std::regex, one_plot const *);
    opt_t<int>                  beadid      (one_plot const *op);
    opt_t<int>                  phaseid     (one_plot const *op);
    bool                        current     (plot_region *&);
    bool                        current     (one_plot    *&);
    bool                        current     (data_set    *&);
    plot_region*                currentpltreg  ();
    one_plot*                   currentplot    ();

    std::regex const BEADPATTERN = std::regex("Z\\(t\\)bd(\\d+)track\\d+\\.gr");
    std::vector<one_plot*>      plots(std::regex = BEADPATTERN, plot_region * = nullptr);
    std::vector<one_plot*>      plots(plot_region *, std::regex = BEADPATTERN);
    std::vector<int>            plotindexes(plot_region *, std::regex = BEADPATTERN);
    std::vector<int>            plotindexes(std::regex = BEADPATTERN, plot_region * = nullptr);

    data_set*                   currentds   ();
    gen_record const *          record      ();
    gen_record const *          record      (plot_region const *plt);
    gen_record       *          record      (plot_region       *plt);
    int                         cyclemin    (gen_record  const *);
    int                         cyclemax    (gen_record  const *);
    int                         ncycles     (gen_record  const *);
    int                         nphases     (gen_record  const *);
    opt_t<std::pair<int, int>>  phaserange  (data_set const *, int);
    opt_t<std::pair<int, int>>  phaserange  (int,              int);
    opt_t<std::pair<int, int>>  phaserange  (plot_region const *, int cid, int phase);
    opt_t<std::pair<int, int>>  phaserange  (gen_record  const *, int cid, int phase);
    opt_t<int>                  phasestart  (gen_record  const *, int cid, int phase);
    bool                        belongs     (plot_region const *, one_plot const *);
    bool                        belongs     (gen_record  const *, one_plot const *);
    std::string                 filename    (); // current plot_region
    std::string                 filename    (one_plot    const *);
    std::string                 filename    (plot_region const *);
    std::string                 filename    (gen_record  const *);

    /* PhaseRange: Memoized phaserange.
     * Thread-safe only if maxcycles is provided
     **/
    struct PhaseRange: private std::vector<opt_t<std::pair<int,int>>>
    {
        using value_type = opt_t<std::pair<int,int>>;
        using base_t     = std::vector<value_type>;
        PhaseRange()     = default;

        PhaseRange(plot_region const *, int, size_t maxcycles = 512);
        PhaseRange(gen_record  const *, int, size_t maxcycles = 512);
        value_type operator[](int);
        value_type at        (int i)        { return base_t::operator[](i); }
        value_type operator[](opt_t<int> i) { return !i ? value_type{} : (*this)[i.value()]; }
        value_type at        (opt_t<int> i) { return !i ? value_type{} : at(i.value()); }

        void setup(plot_region const *, int, size_t maxcycles = 512);
        void setup(gen_record  const *, int, size_t maxcycles = 512);

        private:
            gen_record const *  _gen;
            int                 _phase;
    };

    std::set<int>               cycles      (plot_region const * curr);
    std::map<int, cycleds_t>    mapbycycle  (plot_region * curr);
    cycleds_t                   selectcycle (plot_region * curr, int cycle);
}
