
#ifndef _SCAN_ZMAG_REC_C_
#define _SCAN_ZMAG_REC_C_

# include "allegro.h"
#ifdef XV_WIN32
# include "winalleg.h"
#endif

# include "xvin.h"


/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "../cfg_file/Pico_cfg.h"
# include "../cardinal/cardinal.h"

#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "../fft2d/fftbtl32n.h"
# include "fillibbt.h"
# include "focus.h"
# include "focus.h"
# include "magnetscontrol.h"
# include "trackBead.h"
# include "brown_util.h"
# include "track_util.h"
# include "select_rec.h"
# include "record.h"
# include "scan_zmag.h"
# include "action.h"

# define SIMPLE_SEGMENT 1024

//// bump stuff

// we call bump a signal presenting a flat start, an abrupt change and finaly recovering its starting level
// bump have 3 states : before bump, in bump, after bump
// two signal level : outside bump and inside bump
// four indexes start, start_bump, end_bump, end

// this compute the error of a bump

extern int zmag_f_or_rot;
extern int fixed_bead_nb;
extern int do_substract_fixed_bead;

#ifdef ENCOURS

int	find_best_y_half(d_s *data, d_s *poly, int i0, int i1, double *a, double *b, double *c)
  (g_record *g_r, b_record *b_r, b_record *b_rf, int i0, int i1, double *a, double *b, double *c)
{
  register int i;
  int *ipoly;
  double X1, Y1, X0, Y0, dX, dxX0, dxX1, tmp;
  int id0, id1, di;
  int page_n0, i_page1, page_n0, i_page1;

  if (i0 < 0 || i1 < 0 || i0 >= g_r->abs_pos || i1 >= g_r->abs_pos)	return 1;


  page_n0 = i0/g_r->page_size;
  i_page0 = i0%g_r->page_size;
  page_n1 = i1/g_r->page_size;
  i_page1 = i1%g_r->page_size;

  X0 = g_r->imi[page_n0][i_page0];
  Y0 = b_r->z[page_n0][i_page0];
  if (b_rf)  Y0 -= b_rf->z[page_n0][i_page0];
  X1 = g_r->imi[page_n1][i_page1];
  Y1 = b_r->z[page_n1][i_page1];
  if (b_rf)  Y1 -= b_rf->z[page_n1][i_page1];
  dX = X1 - X0;
  if (dX == 0)	return 1;
  *a = *b = *c = 0;
  for (i = i0, di = (i1-i0)/abs(i1-i0);i != i1; i += di)
    {
      dxX0 = data->xd[i] - X0;
      dxX1 = X1 - data->xd[i];
      tmp = dxX0/dX;
      *a += tmp*tmp;
      *b += data->yd[i]*tmp;
      *c += tmp*dxX1/dX;
    }

  *c *= Y0;
  return 0;
}


int		find_best_y(d_s *data, d_s *poly, int i0, int i1, double *a, double *b, double *c)
{
  register int i;
  int *ipoly;
  double X1, Y1, X0, Y0, dX, dxX0, dxX1, tmp;
  int id0, id1, di;

  if (i0 < 0 || i1 < 0 || i0 >= poly->nx || i1 >= poly->nx)	return 1;

  ipoly = (int*)poly->xd;
  X0 = data->xd[ipoly[i0]];
  Y0 = poly->yd[i0];
  X1 = data->xd[ipoly[i1]];
  Y1 = poly->yd[i1];
  dX = X1 - X0;
  if (dX == 0)	return 1;
  id0 = ipoly[i0];
  id1 = ipoly[i1];
  if (id0 == id1)	return 1;
  *a = *b = *c = 0;
  for (i = id0, di = (id1-id0)/abs(id1-id0);i != id1; i += di)
    {
      dxX0 = data->xd[i] - X0;
      dxX1 = X1 - data->xd[i];
      tmp = dxX0/dX;
      *a += tmp*tmp;
      *b += data->yd[i]*tmp;
      *c += tmp*dxX1/dX;
    }

  dxX0 = data->xd[id1] - X0;
  dxX1 = X1 - data->xd[id1];
  tmp = dxX0/dX;
  *a += tmp*tmp;
  *b += data->yd[id1]*tmp;
  *c += tmp*dxX1/dX;

  *c *= Y0;
  return 0;
}

double compute_chi2(d_s *data, d_s *poly)
{
  register int i, j;
  double  tmp, dX, chi2 = 0;
  int *ipoly, i0, i1;

  ipoly = (int*)poly->xd;
  for(i = 0; i < poly->nx-1; i++)
    {
      i0 = ipoly[i];
      i1 = ipoly[i+1];
      dX = data->xd[i1] - data->xd[i0];
      for (j = i0; j < i1; j++)
	{
	  tmp = data->yd[j] + poly->yd[i]*(data->xd[j]-data->xd[i1])/dX;
	  tmp -= poly->yd[i+1]*(data->xd[j]-data->xd[i0])/dX;
	  chi2 += tmp*tmp;
	}
    }
  i1 = ipoly[poly->nx-1];

  chi2 += ((data->yd[i1] - poly->yd[poly->nx-1])*(data->yd[i1] - poly->yd[poly->nx-1]));

  return chi2;
}

# endif

double	chi2_of_bump(d_s *dsi, int start, int bump0, int bump1, int end,
		     double *mean, double *bump)
{
  register int i, j, k;
  double chi2 = 0, m, mb, tmp;

  for (i = start, j = k = 0, m = mb = 0; i < end; i++)
    {
      if (i >= bump0 && i < bump1)
	{
	  mb += dsi->yd[i];
	  k++;
	}
      else
	{
	  m += dsi->yd[i];
	  j++;
	}
    }
  mb = (k != 0) ? mb/k : mb;
  m = (j != 0) ? m/j : m;
  if (mean != NULL)	*mean = m;
  if (bump != NULL)	*bump = mb;
  for (i = start, j = 0, chi2 = 0; i < end; i++, j++)
    {
      tmp = dsi->yd[i];
      tmp -= (i >= bump0 && i < bump1) ?  mb : m;
      tmp *= tmp;
      chi2 += tmp;
    }
  chi2 /= (j != 0) ? j : 1;
  return chi2;
}

/*

double 	adjust_bump(d_s *dsi, int first, int imin, int end, int *bumps, int *bumpe, double *sigs, double *sigb)
{
  register int i, j;
  int  icenter, wi, ibmi, ibma, ii, it, bump0, bump1, ib0b, ib1b;
  double chi2 = 0, mean, bump, meanb = 0, bumpb = 0, chi2b = 0;

  ibump0 = *ibumps;
  ibump1 = *ibumpe;
  if (ibump0 < imin || ibump0 >= imax)
    ibump0 = ((imin + imax)/2) - (imax - imin)/20;
  if (ibump1 < imin || ibump1 >= imax)
    ibump1 = ((imin + imax)/2) + (imax - imin)/20;

  icenter = (ibump0 + ibump1)/2;
  ibmi = imin + (imax - imin)/20;    // minimum position of bump0
  ibma = imax - (imax - imin)/20;    // maximum position of bump1
  wi = ibump1 - ibump0;              // bump size
  ib0b = imin + wi/2;
  if (first) //we find center of bump having a fixed size
    {
      for(i = imin + wi/2, j = 0; (i < (imax -(3*wi)/2)) && (i < imax); i++, j++)		{
	chi2 = chi2_of_bump(dsi, imin, i, i+wi, imax, &mean, &bump);
	if ((j == 0) || chi2 < chi2b)
	  {
	    chi2b = chi2;
	    ib0b = i;
	    meanb = mean;
	    bumpb = bump;
	  }
      }
      ibump0 = ib0b;
      ibump1 = ib0b + wi;
      icenter = (ibump0 + ibump1)/2;
    }
  it = ((imin+imax)/2) + 1;
  ii = icenter + (ibump1 - icenter)/2;
  ib1b = ii = (ii < it) ? it : ii;
  //we adjust ib1b (second edge)
  for (i = ii, j = 0; (i < (icenter + ibump1 - ibump0)) && (i < ibma); i++, j++)
    {
      chi2 = chi2_of_bump(dsi, imin, ibump0, i, imax, &mean, &bump);
      if ((j == 0) || chi2 < chi2b)
	{
	  chi2b = chi2;
	  ib1b = i;
	  meanb = mean;
	  bumpb = bump;
	}
    }
  ii = icenter - ibump1 + ibump0;
  ii = (ii < ibmi) ? ibmi : ii;
  it = icenter - (ibump1 - icenter)/2;
  //we adjust ib0b (first edge)
  for (ib0b = i = ii, j = 0; i < it && (i < ibma); i++, j++)
    {
      chi2 = chi2_of_bump(dsi, imin, i, ib1b, imax, &mean, &bump);
      if ((j == 0) || chi2 < chi2b)
	{
	  chi2b = chi2;
	  ib0b = i;
	  meanb = mean;
	  bumpb = bump;
	}
    }
  *ibumps = ib0b;
  *ibumpe = ib1b;
  *sigs = mean;
  *sigb = meanb;
  return chi2b;
}


*/

# ifdef OLD

int    do_simple_segment(pltreg *pr, O_p *op, d_s *dsi, float x0, float y0, float x1, float y1)
{
  register int i, j;
  int  imin, imax, dsn, op_n, bead_nb, page_n, i_page;
  double  min, max;
  float max_y, min_y;
  g_record *g_r = NULL;
  b_record *b_r = NULL;
  float zmag, rot, zobj, zavg;
  int param_cst, profile_invalid, n_ev;


  g_r = find_g_record_in_pltreg(pr);
  if (g_r == NULL)   win_printf_OK("Ne gen record found");
  bead_nb = op->user_ispare[ISPARE_BEAD_NB];
  if (bead_nb < 0 || bead_nb >= g_r->n_bead)  win_printf_OK("bead nb out of range");
  b_r = g_r->b_r[bead_nb];
  min = (x0 < x1) ? x0 : x1;
  max = (x0 < x1) ? x1 : x0;
  dsn = op->cur_dat;
  op_n = pr->cur_op;
  dsi = op->dat[0];
  for (imin = dsi->nx -1, imax = i = 0;i < dsi->nx; i++)
    {
      if (dsi->xd[i] <= max && i > imax) 	  imax = i;
      if (dsi->xd[i] >= min && i < imin) 	  imin = i;
    }
  if (imax <= imin)
    return win_printf("imin = %d imax = %d impossible",imin,imax);
  for (i = 1, min_y = max_y = dsi->yd[0]; i < dsi->nx; i++)
    {
      max_y = (dsi->yd[i] > max_y) ? dsi->yd[i] : max_y;
      min_y = (dsi->yd[i] < min_y) ? dsi->yd[i] : min_y;
    }
  // position of event
  j = op->user_ispare[ISPARE_STARTING_POSITION_SET] + imin;

  average_param_during_part_trk(g_r, j, imax - imin, &zmag, &rot, &zobj, &param_cst);
  if (param_cst != 0)
    win_printf_OK("Your experiment parameters\n were not constant during event %d\n",param_cst);
  average_bead_z_during_part_trk(g_r, bead_nb, j, imax - imin, NULL, NULL, &zavg, NULL, &profile_invalid);
  if (profile_invalid)
    {
      i = win_printf("Z tracking experience some troubles during this event\n"
		    "%d points over %d points may have bad Z value\n"
		    "Do you want to keep this event ?");
      if (i == WIN_CANCEL) return 0;
    }
  //g_r->n_events++;
  n_ev = g_r->n_events++;
  for (i = imin; i < imax; i++)
    {
      j = op->user_ispare[ISPARE_STARTING_POSITION_SET] + i;
      page_n = j/g_r->page_size;
      i_page = j%g_r->page_size;
      b_r->event_nb[page_n][i_page] = n_ev;
      b_r->event_index[page_n][i_page] = SIMPLE_SEGMENT;
    }
  //win_printf("Bead %d Simple segment ev %d\n starting at %d lasting %d",bead_nb,n_ev, op->user_ispare[ISPARE_STARTING_POSITION_SET]+imin,imax-imin);

  op->user_ispare[ISPARE_POSITION_UPDATE]--;
  return 0;
}

# endif

# ifdef ENCOURS
double	chi2_of_nhz_front(g_record *g_r, b_record *b_r, b_record *b_rf, int start, int pos, int end)
{
  register int i, j, k;
  int page_n, i_page;
  double chi2 = 0, m, mb, tmp;

  for (i = start, j = k = 0, m = mb = 0; i < end; i++)
    {
      if (i < pos)
	{
	  page_n = i/g_r->page_size;
	  i_page = i%g_r->page_size;
	  mb += b_r->z[page_n][i_page];
	  if (b_rf) mb -= b_rf->z[page_n][i_page];
	  k++;
	}
      else
	{
	  page_n = i/g_r->page_size;
	  i_page = i%g_r->page_size;
	  m += b_r->z[page_n][i_page];
	  if (b_rf) m -= b_rf->z[page_n][i_page];
	  j++;
	}
    }
  mb = (k != 0) ? mb/k : mb;
  m = (j != 0) ? m/j : m;
  if (mean0 != NULL)	*mean0 = mb;
  if (mean1 != NULL)	*mean1 = m;
  for (i = start, j = 0, chi2 = 0; i < end; i++, j++)
    {
      page_n = i/g_r->page_size;
      i_page = i%g_r->page_size;
      tmp = b_r->z[page_n][i_page];
      if (b_rf) tmp -= b_rf->z[page_n][i_page];
      tmp -= (i < pos) ?  mb : m;
      tmp *= tmp;
      chi2 += tmp;
    }
  chi2 /= (j != 0) ? j : 1;
  return chi2;
}

# endif

double	chi2_of_front(g_record *g_r, b_record *b_r, b_record *b_rf, int start, int pos, int end, double *mean0, double *mean1)
{
  register int i, j, k;
  int page_n, i_page;
  double chi2 = 0, m, mb, tmp;

  for (i = start, j = k = 0, m = mb = 0; i < end; i++)
    {
      if (i < pos)
	{
	  page_n = i/g_r->page_size;
	  i_page = i%g_r->page_size;
	  mb += b_r->z[page_n][i_page];
	  if (b_rf) mb -= b_rf->z[page_n][i_page];
	  k++;
	}
      else
	{
	  page_n = i/g_r->page_size;
	  i_page = i%g_r->page_size;
	  m += b_r->z[page_n][i_page];
	  if (b_rf) m -= b_rf->z[page_n][i_page];
	  j++;
	}
    }
  mb = (k != 0) ? mb/k : mb;
  m = (j != 0) ? m/j : m;
  if (mean0 != NULL)	*mean0 = mb;
  if (mean1 != NULL)	*mean1 = m;
  for (i = start, j = 0, chi2 = 0; i < end; i++, j++)
    {
      page_n = i/g_r->page_size;
      i_page = i%g_r->page_size;
      tmp = b_r->z[page_n][i_page];
      if (b_rf) tmp -= b_rf->z[page_n][i_page];
      tmp -= (i < pos) ?  mb : m;
      tmp *= tmp;
      chi2 += tmp;
    }
  chi2 /= (j != 0) ? j : 1;
  return chi2;
}

int find_front_position(g_record *gr, b_record *br, b_record *brf, int start, int pos_estimate, int end, int mov_allow)
{
  register int i, k;
  double tmp, chi2m = 0, mean0, mean1;


  if (gr == NULL || br == NULL || end < 0 || start < 0)   return -1;
  if (end < start)
    {
      i = start;
      start = end;
      end = i;
    }
  if (mov_allow < 0)                           mov_allow = (end - start)/2;
  else   if ((2*mov_allow) > (end - start))    mov_allow = (end - start)/2;
  for (i = pos_estimate - mov_allow, k = -1; i < pos_estimate + mov_allow; i++)
    {
      tmp = chi2_of_front(gr, br, brf, start, i, end, &mean0, &mean1);
      if (k < 0 || tmp < chi2m)
	{
	  k = i;
	  chi2m = tmp;
	}
    }

  //  win_printf("front adj k = %d start %d  \nend %d  %d < k < %d mov_allow %d"
  //     ,k,start, end, pos_estimate - mov_allow,pos_estimate + mov_allow, mov_allow);

  return k;
}

int    do_poly_segments(pltreg *pr, O_p *op, d_s *dsi, int n_seg, float *x, float *y, char *type)
{
  register int i, j, k;
  int  imin, imax, bead_nb, page_n, i_page;
  double  min, max;   // of segments data
  float max_yl, min_yl;
  g_record *g_r = NULL;
  b_record *b_r = NULL, *b_rf = NULL;
  float zmag = 0, rot = 0, zobj = 0, xavg = 0, yavg = 0, zavg = 0, zavg_s = 0, zavg_e = 0, chi2 = 0,  max_histo = 0;
  int param_cst = 0, profile_invalid = 0, n_ev = 0, xi[64], seg_s = 0, seg_e = 0;

  //win_printf("entering poly segs");
  (void)y;
  g_r = find_g_record_in_pltreg(pr);
  if (g_r == NULL)   win_printf_OK("Ne gen record found");
  bead_nb = op->user_ispare[ISPARE_BEAD_NB];
  if (bead_nb < 0 || bead_nb >= g_r->n_bead)  win_printf_OK("bead nb out of range");
  b_r = g_r->b_r[bead_nb];
  for (i = 0, j = 0; i <= n_seg; i++)
      j = (x[i] < x[j]) ? i : j;
  min = x[j];
  for (i = 0, j = 0; i <= n_seg; i++)
      j = (x[i] > x[j]) ? i : j;
  max = x[j];
  //dsn = op->cur_dat;
  //op_n = pr->cur_op;
  dsi = op->dat[0];
  for (imin = dsi->nx -1, imax = i = 0;i < dsi->nx; i++)
    {  // we find point index bracketting segments
      if (dsi->xd[i] <= max && i > imax) 	  imax = i;
      if (dsi->xd[i] >= min && i < imin) 	  imin = i;
    }

  for (j = 0; j < g_r->n_page && ((g_r->imi[j][0] - g_r->imi[0][0]) < (int)min) ; j++);
  j = (j > 0) ? j-1 : j;
  for (k = 0; k < g_r->page_size && ((g_r->imi[j][k] - g_r->imi[0][0]) < (int)min) ; k++);
  k = (k > 0) ? k-1 : k;
  imin = g_r->imi[j][k] - g_r->imi[0][0];

  for (j = 0; j < g_r->n_page && ((g_r->imi[j][0] - g_r->imi[0][0]) < (int)max) ; j++);
  j = (j > 0) ? j-1 : j;
  for (k = 0; k < g_r->page_size && ((g_r->imi[j][k] - g_r->imi[0][0]) < (int)max) ; k++);
  k = (k > 0) ? k-1 : k;
  imax = g_r->imi[j][k] - g_r->imi[0][0];

  if (imax <= imin)
    return win_printf("imin = %d imax = %d impossible",imin,imax);
  for (i = 1, min_yl = max_yl = dsi->yd[0]; i < dsi->nx; i++)
    {   // we find min and max in y of dsi data
      max_yl = (dsi->yd[i] > max_yl) ? dsi->yd[i] : max_yl;
      min_yl = (dsi->yd[i] < min_yl) ? dsi->yd[i] : min_yl;
    }
  for (i = 0; i <= n_seg && i < 64; i++)
    {

      for (j = 0; j < g_r->n_page && ((g_r->imi[j][0] - g_r->imi[0][0]) < (int)x[i]) ; j++);
      j = (j > 0) ? j-1 : j;
      for (k = 0; k < g_r->page_size && ((g_r->imi[j][k] - g_r->imi[0][0]) < (int)x[i]) ; k++);
      //k = (k > 0) ? k-1 : k;
      xi[i] = g_r->imi[j][k] - g_r->imi[0][0];
    }
/*
  for (i = 0; i <= n_seg && i < 64; i++)
    {
      for (j = 0, xi[i] = 0, tmp = fabs(x[i] - dsi->xd[0]); j < dsi->nx; j++)
	{
	  if (fabs(dsi->xd[j] - x[i]) < tmp)
	    {   // we convert segment x position to index store in xi
	      tmp = fabs(dsi->xd[j] - x[i]);
	      xi[i] = j;
	    }
	}
    }
*/
  /*
  if (n_seg > 3)
    {
      win_printf("xi 0 %d %c 1 %d %c 2 %d %c 3 %d",
		 xi[0],type[0],xi[1],type[1],xi[2],type[2],xi[3]);
    }
  */
  // this is the starting position of event
  j = op->user_ispare[ISPARE_STARTING_POSITION_SET];

  /*
  ims = (g_r->timing_mode == 1) ? (g_r->imi[j/g_r->page_size][j%g_r->page_size] - g_r->imi_start)
    : g_r->imi[j/g_r->page_size][j%g_r->page_size];

  j = ims;
  ims = j;
  */
  //j += imin;
  //win_printf("seg extends %d to %d",ims + imin,ims + imax);
  // we compute average values and check that params are ok
  average_param_during_part_trk(g_r, imin, imax - imin, &zmag, &rot, &zobj, &param_cst);
  if (param_cst != 0)
    win_printf_OK("Your experiment parameters\n were not constant during event %d\n",param_cst);

  if (do_substract_fixed_bead && fixed_bead_nb >= 0 &&  fixed_bead_nb < g_r->n_bead)
    {
      b_rf = g_r->b_r[fixed_bead_nb];
      average_bead_z_minus_fixed_during_part_trk_tolerant(g_r, bead_nb, fixed_bead_nb, imin, imax - imin, 1, NULL,
					       NULL, &zavg, NULL, &profile_invalid);
    }
  else average_bead_z_during_part_trk_tolerant(g_r, bead_nb, imin, imax - imin, 1, NULL, NULL, &zavg, NULL, &profile_invalid);
  if (profile_invalid)
    {
      i = win_printf("Z tracking experience some troubles during this event\n"
		    "%d points over %d points may have bad Z value\n"
		     "Do you want to keep this event ?",profile_invalid,imax-imin);
      if (i == WIN_CANCEL) return 0;
    }

  for (i = 0; i < n_seg; i++)
    {
      if (type[i] == 'H' || type[i] == 'R')  // a front
	{
	  if (i == 0)
	    return win_printf_OK("An event cannot start by a front!\n");
	  else if (i == n_seg - 1)
	    return win_printf_OK("An event cannot end by a front!\n");
	  else if (type[i-1] == 'N' && type[i+1] == 'N')
	    {  // need work here
	      j = find_front_position(g_r, b_r, b_rf, xi[i-1], (xi[i]+xi[i+1])/2, xi[i+2], 5+abs(xi[i+2]-xi[i-1])/7);
	      // win_printf("front adj j = %d start %d estimate %d \nend %d mov_allow %d ini %d"
	      //	 ,j
	      //	 ,ims  +xi[i-1]
	      //	 ,ims +(xi[i]+xi[i+1])/2
	      //	 ,ims+ xi[i+2]
	      //	 , 5+abs(xi[i+2]-xi[i-1])/7,ims);
	      //j -= ims;
	      if (j > 0)
		{
		  //win_printf("0-> %d j = %d 1-> %d",xi[i],j,xi[i+1]);
		  xi[i] = j;
		  xi[i+1] = j+2;
		}
	    }
	}
    }

  // we record the event
  //g_r->n_events++;
  n_ev = g_r->n_events;
  for (i = imin, k = 0; i < imax; i++)
    {
      if (k < n_seg && i >= xi[k+1]) k++;
      j = i; //op->user_ispare[ISPARE_STARTING_POSITION_SET] + i;
      page_n = j/g_r->page_size;
      i_page = j%g_r->page_size;
      b_r->event_nb[page_n][i_page] = n_ev;
      if (type[k] == 'H')        b_r->event_index[page_n][i_page] = H_SEGMENT;
      else if (type[k] == 'R')   b_r->event_index[page_n][i_page] = R_SEGMENT;
      else if (type[k] == 'Z')   b_r->event_index[page_n][i_page] = Z_SEGMENT;
      else if (type[k] == 'U')   b_r->event_index[page_n][i_page] = U_SEGMENT;
      else if (type[k] == 'N')   b_r->event_index[page_n][i_page] = N_SEGMENT;
    }
  // we record the event in the new structure

  i = (b_rf) ? fixed_bead_nb : -1;
  //win_printf("bef histo fixed = %d",i);
  find_bead_z_max_histo_during_part_trk(g_r, bead_nb, i,
					(imin+imax)/2 -256,
					512, 0.002, &max_histo, NULL, NULL, &profile_invalid);
  //op->user_ispare[ISPARE_STARTING_POSITION_SET] +
  //win_printf("after histo n_seg = %d",n_seg);
  for (;b_r->na_e >= b_r->ma_e; )
    {
      b_r->a_e = (analyzed_event*)realloc(b_r->a_e ,(b_r->ma_e+16)*sizeof(analyzed_event));
      if (b_r->a_e == NULL) return win_printf_OK("cannot allocate event data!\n");
      b_r->ma_e += 16;
    }
  b_r->a_e[b_r->na_e].e_s = (event_seg*)calloc(n_seg,sizeof(event_seg));
  if (b_r->a_e[b_r->na_e].e_s == NULL) return win_printf_OK("cannot allocate segments of event data!\n");
  b_r->ia_e = b_r->na_e;
  b_r->na_e++;
  b_r->a_e[b_r->ia_e].n_seg = n_seg;
  b_r->a_e[b_r->ia_e].i_ev = g_r->n_events;
  b_r->a_e[b_r->ia_e].ev_start = imin; // op->user_ispare[ISPARE_STARTING_POSITION_SET] +
  b_r->a_e[b_r->ia_e].ev_end = imax; // op->user_ispare[ISPARE_STARTING_POSITION_SET] +
  b_r->a_e[b_r->ia_e].zmag = zmag;
  b_r->a_e[b_r->ia_e].zobj = zobj;
  b_r->a_e[b_r->ia_e].rot = rot;
  b_r->a_e[b_r->ia_e].n_badz = profile_invalid;
  b_r->a_e[b_r->ia_e].type = EVENT_POLY_LINE;

  for (i = 0; i < n_seg; i++)
    {
      b_r->a_e[b_r->ia_e].e_s[i].i_seg = i;
      b_r->a_e[b_r->ia_e].e_s[i].i_ev = g_r->n_events;
      b_r->a_e[b_r->ia_e].e_s[i].seg_start = seg_s = xi[i]; // op->user_ispare[ISPARE_STARTING_POSITION_SET] +
      b_r->a_e[b_r->ia_e].e_s[i].seg_end = seg_e = xi[i+1]; // op->user_ispare[ISPARE_STARTING_POSITION_SET] +
      b_r->a_e[b_r->ia_e].e_s[i].dt = (float)(b_r->a_e[b_r->ia_e].e_s[i].seg_end -
					    b_r->a_e[b_r->ia_e].e_s[i].seg_start)
	/g_r->Pico_param_record.camera_param.camera_frequency_in_Hz;
      average_param_during_part_trk(g_r, seg_s, seg_e - seg_s, &zmag, &rot, &zobj, &param_cst);
      b_r->a_e[b_r->ia_e].e_s[i].par_chg = param_cst;//) ? 0 : 1;
      b_r->a_e[b_r->ia_e].e_s[i].zmag = zmag;
      float fmul = b_r->fparam[FPARAM_F_ADJUST];
      fmul = (fmul==0) ? 1 : fmul;
      b_r->a_e[b_r->ia_e].e_s[i].force=  fmul * trk_zmag_to_force(0,zmag,g_r);
      b_r->a_e[b_r->ia_e].e_s[i].rot = rot;
      b_r->a_e[b_r->ia_e].e_s[i].zobj = zobj;
      b_r->a_e[b_r->ia_e].e_s[i].z_ref = max_histo;
      if (g_r->evanescent_mode <= 0)
	b_r->a_e[b_r->ia_e].e_s[i].z_ref *= g_r->z_cor;
      b_r->a_e[b_r->ia_e].e_s[i].fixed_bead = (do_substract_fixed_bead && fixed_bead_nb >= 0
					       &&  fixed_bead_nb < g_r->n_bead) ? fixed_bead_nb + g_r->start_bead : -1;
      b_r->a_e[b_r->ia_e].e_s[i].n_bead = bead_nb + g_r->start_bead;

      if (do_substract_fixed_bead && fixed_bead_nb >= 0 &&  fixed_bead_nb < g_r->n_bead)
	{
	  average_bead_z_minus_fixed_during_part_trk_tolerant(g_r, bead_nb, fixed_bead_nb, seg_s, seg_e - seg_s, 1,
							   &xavg, &yavg, &zavg, NULL, &profile_invalid);
	}
      else  average_bead_z_during_part_trk_tolerant(g_r, bead_nb, seg_s, seg_e - seg_s, 1,
						 &xavg, &yavg, &zavg, NULL, &profile_invalid);

      b_r->a_e[b_r->ia_e].e_s[i].n_badz = profile_invalid;
      b_r->a_e[b_r->ia_e].e_s[i].zavg = zavg;
      if (g_r->evanescent_mode <= 0)
	b_r->a_e[b_r->ia_e].e_s[i].zavg *= g_r->z_cor;
      b_r->a_e[b_r->ia_e].e_s[i].xavg = g_r->ax + g_r->dx * xavg;
      b_r->a_e[b_r->ia_e].e_s[i].yavg = g_r->ay + g_r->dy * yavg;

      if (type[i] == 'Z'  || type[i] == 'U')
	{
	  fit_bead_z_to_line_during_part_trk_tolerant(g_r, bead_nb, ((do_substract_fixed_bead) ? fixed_bead_nb : -1),
						      seg_s, seg_e - seg_s, 1, &zavg_s, &zavg_e, &profile_invalid, &chi2);

	  b_r->a_e[b_r->ia_e].e_s[i].zstart = zavg_s;
	  b_r->a_e[b_r->ia_e].e_s[i].zend = zavg_e;
	  if (g_r->evanescent_mode <= 0)
	    {
	      b_r->a_e[b_r->ia_e].e_s[i].zstart *= g_r->z_cor;
	      b_r->a_e[b_r->ia_e].e_s[i].zend *= g_r->z_cor;
	    }
	  b_r->a_e[b_r->ia_e].e_s[i].sig_z = ((seg_e - seg_s) > 0) ? sqrt(chi2) /(seg_e - seg_s) : 0;
	}
      if (type[i] == 'H')        b_r->a_e[b_r->ia_e].e_s[i].type = H_SEGMENT;
      else if (type[i] == 'R')   b_r->a_e[b_r->ia_e].e_s[i].type = R_SEGMENT;
      else if (type[i] == 'Z')   b_r->a_e[b_r->ia_e].e_s[i].type = Z_SEGMENT;
      else if (type[i] == 'U')   b_r->a_e[b_r->ia_e].e_s[i].type = U_SEGMENT;
      else if (type[i] == 'N')
	{
	  b_r->a_e[b_r->ia_e].e_s[i].type = N_SEGMENT;
	  b_r->a_e[b_r->ia_e].e_s[i].zstart_vertex = b_r->a_e[b_r->ia_e].e_s[i].zavg;
	  b_r->a_e[b_r->ia_e].e_s[i].zend_vertex = b_r->a_e[b_r->ia_e].e_s[i].zavg;
	}
      b_r->a_e[b_r->ia_e].e_s[i].taux = 0;
      b_r->a_e[b_r->ia_e].e_s[i].tauy = 0;
      b_r->a_e[b_r->ia_e].e_s[i].tauz = 0;
    }
    for (i = 0; i < n_seg; i++)
    {
      if (type[i] == 'Z'  || type[i] == 'U')
	{
	  if (i > 0 && type[i-1] == 'N')
	    b_r->a_e[b_r->ia_e].e_s[i].zstart_vertex = b_r->a_e[b_r->ia_e].e_s[i-1].zavg;
	  else b_r->a_e[b_r->ia_e].e_s[i].zstart_vertex = b_r->a_e[b_r->ia_e].e_s[i].zstart;

	  if ((i+1) < n_seg && type[i+1] == 'N')
	    b_r->a_e[b_r->ia_e].e_s[i].zend_vertex = b_r->a_e[b_r->ia_e].e_s[i+1].zavg;
	  else b_r->a_e[b_r->ia_e].e_s[i].zend_vertex = b_r->a_e[b_r->ia_e].e_s[i].zend;
	}
    }
    for (i = 0; i < n_seg; i++)
    {
      if (type[i] == 'H'  || type[i] == 'R')
	{
	  if (i > 0)
	    b_r->a_e[b_r->ia_e].e_s[i].zstart_vertex = b_r->a_e[b_r->ia_e].e_s[i-1].zend_vertex;
	  else b_r->a_e[b_r->ia_e].e_s[i].zstart_vertex = b_r->a_e[b_r->ia_e].e_s[i].zstart;

	  if ((i+1) < n_seg)
	    b_r->a_e[b_r->ia_e].e_s[i].zend_vertex = b_r->a_e[b_r->ia_e].e_s[i+1].zstart_vertex;
	  else b_r->a_e[b_r->ia_e].e_s[i].zend_vertex = b_r->a_e[b_r->ia_e].e_s[i].zend;
	}
    }

    g_r->n_events++;

    //win_printf("%d Event %d recorded with %d seg max histo %f",b_r->ia_e,b_r->a_e[b_r->ia_e].i_ev,b_r->a_e[b_r->ia_e].n_seg,g_r->z_cor*max_histo);
  //win_printf("Bead %d Simple segment ev %d\n starting at %d lasting %d",bead_nb,n_ev, op->user_ispare[ISPARE_STARTING_POSITION_SET]+imin,imax-imin);

  op->user_ispare[ISPARE_POSITION_UPDATE]--;
  return 0;
}



int    do_poly_dash_segments(pltreg *pr, O_p *op, d_s *dsi, int n_seg, float *x, float *y, char *type)
{
  register int i, j, k;
  int  imin, imax, bead_nb, page_n, i_page;//, im0;
  double  min, max;   // of segments data
  float max_yl, min_yl;
  g_record *g_r = NULL;
  b_record *b_r = NULL, *b_rf = NULL;
  float zmag = 0, rot = 0, zobj = 0, xavg = 0, yavg = 0, zavg = 0, zavg_s = 0, zavg_e = 0, chi2 = 0,  max_histo = 0;
  int param_cst = 0, profile_invalid = 0, n_ev = 0, xi[64], seg_s = 0, seg_e = 0;

  (void)y;
  g_r = find_g_record_in_pltreg(pr);
  if (g_r == NULL)   win_printf_OK("Ne gen record found");
  bead_nb = op->user_ispare[ISPARE_BEAD_NB];
  if (bead_nb < 0 || bead_nb >= g_r->n_bead)  win_printf_OK("bead nb out of range");
  b_r = g_r->b_r[bead_nb];
  for (i = 0, j = 0; i <= n_seg; i++)
      j = (x[i] < x[j]) ? i : j;
  min = x[j];
  for (i = 0, j = 0; i <= n_seg; i++)
      j = (x[i] > x[j]) ? i : j;
  max = x[j];
  dsi = op->dat[0];
  for (imin = dsi->nx -1, imax = i = 0;i < dsi->nx; i++)
    {  // we find point index bracketting segments
      if (dsi->xd[i] <= max && i > imax) 	  imax = i;
      if (dsi->xd[i] >= min && i < imin) 	  imin = i;
    }

  for (j = 0; j < g_r->n_page && ((g_r->imi[j][0] - g_r->imi[0][0]) < (int)min) ; j++);
  j = (j > 0) ? j-1 : j;
  for (k = 0; k < g_r->page_size && ((g_r->imi[j][k] - g_r->imi[0][0]) < (int)min) ; k++);
  k = (k > 0) ? k-1 : k;
  imin = g_r->imi[j][k] - g_r->imi[0][0];

  for (j = 0; j < g_r->n_page && ((g_r->imi[j][0] - g_r->imi[0][0]) < (int)max) ; j++);
  j = (j > 0) ? j-1 : j;
  for (k = 0; k < g_r->page_size && ((g_r->imi[j][k] - g_r->imi[0][0]) < (int)max) ; k++);
  k = (k > 0) ? k-1 : k;
  imax = g_r->imi[j][k] - g_r->imi[0][0];


  if (imax <= imin)
    return win_printf("imin = %d imax = %d impossible",imin,imax);



  for (i = 1, min_yl = max_yl = dsi->yd[0]; i < dsi->nx; i++)
    {   // we find min and max in y of dsi data
      max_yl = (dsi->yd[i] > max_yl) ? dsi->yd[i] : max_yl;
      min_yl = (dsi->yd[i] < min_yl) ? dsi->yd[i] : min_yl;
    }

  for (i = 0; i <= n_seg && i < 64; i++)
    {

      for (j = 0; j < g_r->n_page && ((g_r->imi[j][0] - g_r->imi[0][0]) < (int)x[i]) ; j++);
      j = (j > 0) ? j-1 : j;
      for (k = 0; k < g_r->page_size && ((g_r->imi[j][k] - g_r->imi[0][0]) < (int)x[i]) ; k++);
      k = (k > 0) ? k-1 : k;
      xi[i] = g_r->imi[j][k] - g_r->imi[0][0];
    }


  //win_printf("Seg 0 x[0] = %g xi[0] = %d",x[0],xi[0]);
  /*
  if (n_seg > 3)
    {
      win_printf("xi 0 %d %c 1 %d %c 2 %d %c 3 %d",
		 xi[0],type[0],xi[1],type[1],xi[2],type[2],xi[3]);
    }
  */
  // this is the starting position of event
  j = op->user_ispare[ISPARE_STARTING_POSITION_SET];

  /*
  ims = (g_r->timing_mode == 1) ? (g_r->imi[j/g_r->page_size][j%g_r->page_size] - g_r->imi_start)
    : g_r->imi[j/g_r->page_size][j%g_r->page_size];

  j = ims;
  ims = j;

  */
  //j += imin;
  //win_printf("seg extends %d to %d",ims + imin,ims + imax);
  // we compute average values and check that params are ok
  //average_param_during_part_trk(g_r, ims + imin, imax - imin, &zmag, &rot, &zobj, &param_cst);
  average_param_during_part_trk(g_r, imin, imax - imin, &zmag, &rot, &zobj, &param_cst);
  if (param_cst != 0)
    win_printf_OK("Your experiment parameters\n were not constant during event %d\n",param_cst);

  if (do_substract_fixed_bead && fixed_bead_nb >= 0 &&  fixed_bead_nb < g_r->n_bead)
    {
      b_rf = g_r->b_r[fixed_bead_nb];
      /*
      average_bead_z_minus_fixed_during_part_trk(g_r, bead_nb, fixed_bead_nb, imin, imax - imin, NULL,
					       NULL, &zavg, NULL, &profile_invalid);
      */
      average_bead_z_minus_fixed_during_part_trk_tolerant(g_r, bead_nb, fixed_bead_nb, imin, imax - imin, 1, NULL,
					       NULL, &zavg, NULL, &profile_invalid);

    }
  else average_bead_z_during_part_trk_tolerant(g_r, bead_nb, imin, imax - imin, 1, NULL, NULL, &zavg, NULL, &profile_invalid);

  //average_bead_z_during_part_trk(g_r, bead_nb, imin, imax - imin, NULL, NULL, &zavg, NULL, &profile_invalid);
  if (profile_invalid)
    {
      i = win_printf("Z tracking experience some troubles during this event\n"
		    "%d points over %d points may have bad Z value\n"
		     "Do you want to keep this event ?",profile_invalid,imax-imin);
      if (i == WIN_CANCEL) return 0;
    }

  for (i = 0; i < n_seg; i++)
    {
      if (type[i] == ' ')  continue;
      if (type[i] == 'H' || type[i] == 'R')  // a front
	{
	  if (i == 0)
	    return win_printf_OK("An event cannot start by a front!\n");
	  else if (i == n_seg - 1)
	    return win_printf_OK("An event cannot end by a front!\n");
	}
    }

  // we record the event
  n_ev = g_r->n_events;
  for (i = imin, k = 0; i < imax; i++)
    {
      if (k < n_seg && i >= xi[k+1]) k++;
      j = i;
      page_n = j/g_r->page_size;
      i_page = j%g_r->page_size;
      b_r->event_nb[page_n][i_page] = n_ev;
      if (type[k] == ' ')        b_r->event_index[page_n][i_page] = 0;
      else if (type[k] == 'H')   b_r->event_index[page_n][i_page] = H_SEGMENT;
      else if (type[k] == 'R')   b_r->event_index[page_n][i_page] = R_SEGMENT;
      else if (type[k] == 'Z')   b_r->event_index[page_n][i_page] = Z_SEGMENT;
      else if (type[k] == 'U')   b_r->event_index[page_n][i_page] = U_SEGMENT;
      else if (type[k] == 'N')   b_r->event_index[page_n][i_page] = N_SEGMENT;
    }
  // we record the event in the new structure

  i = (b_rf) ? fixed_bead_nb : -1;
  //win_printf("bef histo fixed = %d",i);
  find_bead_z_max_histo_during_part_trk(g_r, bead_nb, i,
					 (imin+imax)/2 -256,
					512, 0.002, &max_histo, NULL, NULL, &profile_invalid);

  //win_printf("after histo");
  for (;b_r->na_e >= b_r->ma_e; )
    {
      b_r->a_e = (analyzed_event*)realloc(b_r->a_e ,(b_r->ma_e+16)*sizeof(analyzed_event));
      if (b_r->a_e == NULL) return win_printf_OK("cannot allocate event data!\n");
      b_r->ma_e += 16;
    }
  b_r->a_e[b_r->na_e].e_s = (event_seg*)calloc(n_seg,sizeof(event_seg));
  if (b_r->a_e[b_r->na_e].e_s == NULL) return win_printf_OK("cannot allocate segments of event data!\n");
  b_r->ia_e = b_r->na_e;
  b_r->na_e++;
  b_r->a_e[b_r->ia_e].n_seg = (n_seg%2) ? 1 + (n_seg/2) : (n_seg/2);
  b_r->a_e[b_r->ia_e].i_ev = g_r->n_events;
  b_r->a_e[b_r->ia_e].ev_start = imin;//op->user_ispare[ISPARE_STARTING_POSITION_SET] +
  b_r->a_e[b_r->ia_e].ev_end = imax;//op->user_ispare[ISPARE_STARTING_POSITION_SET] +
  b_r->a_e[b_r->ia_e].zmag = zmag;
  b_r->a_e[b_r->ia_e].zobj = zobj;
  b_r->a_e[b_r->ia_e].rot = rot;
  b_r->a_e[b_r->ia_e].n_badz = profile_invalid;
  b_r->a_e[b_r->ia_e].type = EVENT_DASH_LINE;

  //im0 = g_r->imi[0][0];
  for (i = 0; i < n_seg; i++)
    {
      if (type[i] == ' ') continue;
      b_r->a_e[b_r->ia_e].e_s[i/2].i_seg = i/2;
      b_r->a_e[b_r->ia_e].e_s[i/2].i_ev = g_r->n_events;
      b_r->a_e[b_r->ia_e].e_s[i/2].seg_start = seg_s = xi[i];
      b_r->a_e[b_r->ia_e].e_s[i/2].seg_end = seg_e = xi[i+1];
      b_r->a_e[b_r->ia_e].e_s[i/2].dt = (float)(b_r->a_e[b_r->ia_e].e_s[i].seg_end -
					    b_r->a_e[b_r->ia_e].e_s[i].seg_start)
	/g_r->Pico_param_record.camera_param.camera_frequency_in_Hz;
      /*
      if (i == 0) win_printf("seg 0 %d = x[0] %d + starting %d imi %d\n"
			     "im0 %d x -> %d\n"
			     ,seg_s,xi[0],op->user_ispare[ISPARE_STARTING_POSITION_SET]
			     , g_r->imi[seg_s/g_r->page_size][seg_s%g_r->page_size]
			     ,im0, g_r->imi[seg_s/g_r->page_size][seg_s%g_r->page_size] - im0 );
      */
      average_param_during_part_trk(g_r, seg_s, seg_e - seg_s, &zmag, &rot, &zobj, &param_cst);
      b_r->a_e[b_r->ia_e].e_s[i/2].par_chg = param_cst;//) ? 0 : 1;
      b_r->a_e[b_r->ia_e].e_s[i/2].zmag = zmag;
      float fmul = b_r->fparam[FPARAM_F_ADJUST];
      fmul = (fmul==0) ? 1 : fmul;
      b_r->a_e[b_r->ia_e].e_s[i/2].force=  fmul * trk_zmag_to_force(0,zmag,g_r);
      b_r->a_e[b_r->ia_e].e_s[i/2].rot = rot;
      b_r->a_e[b_r->ia_e].e_s[i/2].zobj = zobj;


      b_r->a_e[b_r->ia_e].e_s[i/2].z_ref = max_histo;
      if (g_r->evanescent_mode <= 0)
      b_r->a_e[b_r->ia_e].e_s[i/2].z_ref *= g_r->z_cor;
      b_r->a_e[b_r->ia_e].e_s[i/2].fixed_bead = (do_substract_fixed_bead && fixed_bead_nb >= 0
					       &&  fixed_bead_nb < g_r->n_bead) ? fixed_bead_nb + g_r->start_bead : -1;
      b_r->a_e[b_r->ia_e].e_s[i/2].n_bead = bead_nb + g_r->start_bead;

      if (do_substract_fixed_bead && fixed_bead_nb >= 0 &&  fixed_bead_nb < g_r->n_bead)
	{
	  /*
	  average_bead_z_minus_fixed_during_part_trk(g_r, bead_nb, fixed_bead_nb, seg_s, seg_e - seg_s,
							   &xavg, &yavg, &zavg, NULL, &profile_invalid);
	  */
	  average_bead_z_minus_fixed_during_part_trk_tolerant(g_r, bead_nb, fixed_bead_nb, seg_s, seg_e - seg_s, 1,
							   &xavg, &yavg, &zavg, NULL, &profile_invalid);

	}
      else  average_bead_z_during_part_trk_tolerant(g_r, bead_nb, seg_s, seg_e - seg_s, 1,
						 &xavg, &yavg, &zavg, NULL, &profile_invalid);

      //average_bead_z_during_part_trk(g_r, bead_nb, seg_s, seg_e - seg_s, &xavg, &yavg, &zavg, NULL, &profile_invalid);

      b_r->a_e[b_r->ia_e].e_s[i/2].n_badz = profile_invalid;
      b_r->a_e[b_r->ia_e].e_s[i/2].zavg = zavg;
      if (g_r->evanescent_mode <= 0)
	b_r->a_e[b_r->ia_e].e_s[i/2].zavg *= g_r->z_cor;

      b_r->a_e[b_r->ia_e].e_s[i/2].xavg = g_r->ax + g_r->dx * xavg;
      b_r->a_e[b_r->ia_e].e_s[i/2].yavg = g_r->ay + g_r->dy * yavg;

      if (type[i] == 'Z'  || type[i] == 'U')
	{
	  fit_bead_z_to_line_during_part_trk_tolerant(g_r, bead_nb, ((do_substract_fixed_bead) ? fixed_bead_nb : -1),
						      seg_s, seg_e - seg_s, 1, &zavg_s, &zavg_e, &profile_invalid, &chi2);

	  b_r->a_e[b_r->ia_e].e_s[i/2].zstart = zavg_s;
	  b_r->a_e[b_r->ia_e].e_s[i/2].zend = zavg_e;
	  if (g_r->evanescent_mode <= 0)
	    {
	      b_r->a_e[b_r->ia_e].e_s[i/2].zstart *= g_r->z_cor;
	      b_r->a_e[b_r->ia_e].e_s[i/2].zend *= g_r->z_cor;
	    }
	  b_r->a_e[b_r->ia_e].e_s[i/2].sig_z = ((seg_e - seg_s) > 0) ? sqrt(chi2) /(seg_e - seg_s) : 0;
	}
      //win_printf("Zavg %g",b_r->a_e[b_r->ia_e].e_s[i/2].zavg);
      if (type[i] == 'H')        b_r->a_e[b_r->ia_e].e_s[i/2].type = H_SEGMENT;
      else if (type[i] == 'R')   b_r->a_e[b_r->ia_e].e_s[i/2].type = R_SEGMENT;
      else if (type[i] == 'Z')   b_r->a_e[b_r->ia_e].e_s[i/2].type = Z_SEGMENT;
      else if (type[i] == 'U')   b_r->a_e[b_r->ia_e].e_s[i/2].type = U_SEGMENT;
      else if (type[i] == 'N')
	{
	  b_r->a_e[b_r->ia_e].e_s[i/2].type = N_SEGMENT;
	  b_r->a_e[b_r->ia_e].e_s[i/2].zstart_vertex = b_r->a_e[b_r->ia_e].e_s[i/2].zavg;
	  b_r->a_e[b_r->ia_e].e_s[i/2].zend_vertex = b_r->a_e[b_r->ia_e].e_s[i/2].zavg;
	  //win_printf("N seg Zavg %g",b_r->a_e[b_r->ia_e].e_s[i/2].zstart_vertex);
	}
      b_r->a_e[b_r->ia_e].e_s[i/2].taux = 0;
      b_r->a_e[b_r->ia_e].e_s[i/2].tauy = 0;
      b_r->a_e[b_r->ia_e].e_s[i/2].tauz = 0;
    }
    for (i = 0; i < n_seg; i++)
    {
      if (type[i] == 'Z'  || type[i] == 'U')
	{
	  b_r->a_e[b_r->ia_e].e_s[i/2].zstart_vertex = b_r->a_e[b_r->ia_e].e_s[i/2].zstart;
	  b_r->a_e[b_r->ia_e].e_s[i/2].zend_vertex = b_r->a_e[b_r->ia_e].e_s[i/2].zend;
	}
    }
    for (i = 0; i < n_seg; i++)
    {
      if (type[i] == 'H'  || type[i] == 'R')
	{
	  b_r->a_e[b_r->ia_e].e_s[i/2].zstart_vertex = b_r->a_e[b_r->ia_e].e_s[i/2].zstart;
	  b_r->a_e[b_r->ia_e].e_s[i/2].zend_vertex = b_r->a_e[b_r->ia_e].e_s[i/2].zend;
	}
    }
    /*
    char question[1024] = {0}, C;
    snprintf(question, sizeof(question),
             "Event %d for Bead %d has %d segment(s) recorded as %d event\n"
             , b_r->ia_e, bead_nb, b_r->a_e[b_r->ia_e].n_seg, b_r->a_e[b_r->ia_e].i_ev);


    for (i = 0; i < n_seg; i++)
    {
        if (b_r->a_e[b_r->ia_e].e_s[i].type == H_SEGMENT) C = 'H';
        else if (b_r->a_e[b_r->ia_e].e_s[i].type == R_SEGMENT) C = 'R';
        else if (b_r->a_e[b_r->ia_e].e_s[i].type == Z_SEGMENT) C = 'Z';
        else if (b_r->a_e[b_r->ia_e].e_s[i].type == U_SEGMENT) C = 'U';
        else if (b_r->a_e[b_r->ia_e].e_s[i].type == N_SEGMENT) C = 'N';

        snprintf(question + strlen(question), sizeof(question) - strlen(question),
                 "Segment %d type %c starts %d ends %d zmag %f\n"
                 "Z value zstart %g zend %g zavg %g\n"
                 , i, C, b_r->a_e[b_r->ia_e].e_s[i].seg_start, b_r->a_e[b_r->ia_e].e_s[i].seg_end
                 , b_r->a_e[b_r->ia_e].e_s[i].zmag, b_r->a_e[b_r->ia_e].e_s[i].zstart
                 , b_r->a_e[b_r->ia_e].e_s[i].zend, b_r->a_e[b_r->ia_e].e_s[i].zavg);
    }

    win_printf(question);
   win_printf("out N seg Zavg %g",b_r->a_e[b_r->ia_e].e_s[i/2].zstart_vertex);
    */
    g_r->n_events++;
   op->user_ispare[ISPARE_POSITION_UPDATE]--;

  return 0;
}


int 	  find_segment_from_point(pltreg *pr, O_p *op, float x, float y, int *i_ev, int *i_seg, int *end)
{
  register int i, j, k;
  int  bead_nb, page_n, i_page, found, imi;
  g_record *g_r = NULL;
  b_record *b_r = NULL;

  g_r = find_g_record_in_pltreg(pr);
  if (g_r == NULL)   win_printf_OK("Ne gen record found");
  bead_nb = op->user_ispare[ISPARE_BEAD_NB];
  if (bead_nb < 0 || bead_nb >= g_r->n_bead)  win_printf_OK("bead nb out of range");
  b_r = g_r->b_r[bead_nb];

  for (i = 0, found = 0; i < b_r->na_e && found == 0; i++)
    {
      for (j = 0; j < b_r->a_e[i].n_seg; j++)  //  && found == 0
	{
	  k = b_r->a_e[i].e_s[j].seg_start;
	  page_n = k/g_r->page_size;
	  i_page = k%g_r->page_size;
	  imi = (g_r->timing_mode == 1) ? (g_r->imi[page_n][i_page] - g_r->imi_start) : g_r->imi[page_n][i_page];
	  if ((b_r->a_e[i].e_s[j].zstart_vertex == y) && (imi == x))
	    found += 1;
	  *i_ev = i;
	  *i_seg = j;
	  *end = found;
	  //if (found) return (found) ? 0 : 1;
	  k = b_r->a_e[i].e_s[j].seg_end;
	  page_n = k/g_r->page_size;
	  i_page = k%g_r->page_size;
	  imi = (g_r->timing_mode == 1) ? (g_r->imi[page_n][i_page] - g_r->imi_start) : g_r->imi[page_n][i_page];
	  if ((b_r->a_e[i].e_s[j].zend_vertex == y) && (imi == x))
	    found += 2;
	  *i_ev = i;
	  *i_seg = j;
	  *end = found;
	  //if (found) return (found) ? 0 : 1;
	}
    }
  return (found) ? 0 : 1;
}

int free_event_for_bead(b_record *b_r, int i_ev)
{
  int i;;
  if (b_r == NULL) return 2;
  if (b_r->a_e[i_ev].e_s != NULL && i_ev < b_r->na_e && i_ev >= 0)
    {
      free(b_r->a_e[i_ev].e_s);
      b_r->a_e[i_ev].e_s = NULL;
      for (i = i_ev+1; i < b_r->na_e; i++)
	b_r->a_e[i-1] = b_r->a_e[i];
      b_r->na_e = (b_r->na_e) ? b_r->na_e - 1 : 0;
      b_r->ia_e = (b_r->ia_e >= b_r->na_e) ? b_r->na_e - 1 : b_r->ia_e;
      return 0;
    }
  return 1;
}
// this is the function called by the mouse action when analyzing an event in the scope plot

int find_selected_pr_poly_line(pltreg *pr, int x_0, int y_0, DIALOG *d)
{
  int   dx = 0, dy = 0, ldx, ldy, color, iseg = 0, i, i_1, k, page_n, i_page, iseg_chg = 0;
  //float  xa, ya; // x, y,
  int xp[64], yp[64], lx, ly, i_ev, i_seg, end, i_seg2, xpr, ypr, bead_nb;
  float xf[64], yf[64];
  char seg[64];
  //struct box *b;
  O_p *op;
  d_s *ds = NULL;
  g_record *g_r = NULL;
  b_record *b_r = NULL;

  if (pr->one_p == NULL)		return D_O_K;
  g_r = find_g_record_in_pltreg(pr);
  if (g_r == NULL)   win_printf_OK("No gen record found");
  op = pr->one_p;
  bead_nb = op->user_ispare[ISPARE_BEAD_NB];
  if (bead_nb < 0 || bead_nb >= g_r->n_bead)  win_printf_OK("bead nb out of range");
  b_r = g_r->b_r[bead_nb];
  op = pr->one_p;
  //sc = pr->screen_scale;
  //b = pr->stack;
  //ya = y_pr_2_pltdata_raw(pr, y_0);// y =
  //xa = x_pr_2_pltdata_raw(pr, x_0);//x =

  ds = find_source_specific_ds_in_op(op,"event polyline");
  for (i = 0, i_ev = i_seg = end = -1; ds != NULL && i < ds->nx; i++)
    {
      if (abs(x_pltdata_2_pr(pr, ds->xd[i])+d->x - x_0) < 5
	  && abs(y_pltdata_2_pr(pr, ds->yd[i])+d->y - y_0) < 5)
	{
	  if (find_segment_from_point(pr, op, ds->xd[i], ds->yd[i], &i_ev, &i_seg, &end))
	    win_printf("did not find Polyline modif i %d",i);

	  //win_printf("Found event %d segment %d end %d ds i %d",i_ev, i_seg, end,i);
	  break;
	}
      if (i == 0)
	{
	  display_title_message("x0 %d y0 %d -> x %d y %d", x_0,y_0,x_pltdata_2_pr(pr, ds->xd[0]),
		     y_pltdata_2_pr(pr, ds->yd[i]));
	}
    }

  if (i_ev >= 0 && i_seg >= 0)
    {  // we mofify an existing segment
      i_seg2 = -1;
      if (end == 2)	i_seg2 = ((i_seg + 1) < b_r->a_e[i_ev].n_seg) ? (i_seg + 1) : -1;
      else if (end == 1)  i_seg2 = ((i_seg - 1) >= 0) ? (i_seg - 1) : -1;

      //win_printf("Found 1");

      ldy = 0;
      ldx = 0;
      color = makecol(255, 64, 64);
      iseg_chg = 1;
      while ((mouse_b & 0x02) == 0)
	{     // as long as no right click
	  dx = mouse_x - x_0;
	  dy = mouse_y - y_0;
	  if  (ldy != dy || dx != ldx)
	    {   // if mouse moving we update line
	      blit_plot_reg(d);
	      //win_printf("Found 1.5");
	      if (end == 2)
		{
		  k = b_r->a_e[i_ev].e_s[i_seg].seg_start;
		  page_n = k/g_r->page_size;
		  i_page = k%g_r->page_size;
		  xpr = x_pltdata_2_pr(pr,(g_r->timing_mode == 1) ? (g_r->imi[page_n][i_page] - g_r->imi_start)
				       : g_r->imi[page_n][i_page]);
		  ypr = y_pltdata_2_pr(pr, b_r->a_e[i_ev].e_s[i_seg].zstart_vertex);
		  ypr =  d->y + ypr; // SCREEN_H
		}
	      else
		{
		  k = b_r->a_e[i_ev].e_s[i_seg].seg_end;
		  page_n = k/g_r->page_size;
		  i_page = k%g_r->page_size;
		  xpr = x_pltdata_2_pr(pr,(g_r->timing_mode == 1) ? (g_r->imi[page_n][i_page] - g_r->imi_start)
				       : g_r->imi[page_n][i_page]);
		  ypr = y_pltdata_2_pr(pr, b_r->a_e[i_ev].e_s[i_seg].zend_vertex);
		  ypr =  d->y + ypr; // SCREEN_H
		}
	      //win_printf("Found 2");
	      for(;screen_acquired;); // we wait for screen_acquired back to 0;
	      screen_acquired = 1;
	      acquire_bitmap(screen);
	      circle(screen, xpr,ypr, 2, color);
	      line(screen,xpr,ypr,x_0 + dx,y_0 + dy,color);
	      release_bitmap(screen);
	      screen_acquired = 0;
	      if (i_seg2 >= 0)
		{
		  if (end == 2)
		    {
		      k = b_r->a_e[i_ev].e_s[i_seg2].seg_end;
		      page_n = k/g_r->page_size;
		      i_page = k%g_r->page_size;
		      xpr = x_pltdata_2_pr(pr,(g_r->timing_mode == 1) ? (g_r->imi[page_n][i_page] - g_r->imi_start)
					   : g_r->imi[page_n][i_page]);
		      ypr = y_pltdata_2_pr(pr, b_r->a_e[i_ev].e_s[i_seg2].zend_vertex);
		      ypr =  d->y + ypr; // SCREEN_H
		      draw_bubble(screen, B_LEFT, 64, 64, "ev %d seg %d xs = %d ys %g"
				  ,i_ev,i_seg2,xpr, b_r->a_e[i_ev].e_s[i_seg2].zstart_vertex);


		    }
		  else
		    {
		      k = b_r->a_e[i_ev].e_s[i_seg2].seg_start;
		      page_n = k/g_r->page_size;
		      i_page = k%g_r->page_size;
		      xpr = x_pltdata_2_pr(pr,(g_r->timing_mode == 1) ? (g_r->imi[page_n][i_page] - g_r->imi_start)
					   : g_r->imi[page_n][i_page]);
		      ypr = y_pltdata_2_pr(pr, b_r->a_e[i_ev].e_s[i_seg2].zstart_vertex);
		      ypr =  d->y + ypr; // SCREEN_H
		      draw_bubble(screen, B_LEFT, 64, 64, "ev %d seg %d xs = %d ys %g"
				  ,i_ev,i_seg2,xpr, b_r->a_e[i_ev].e_s[i_seg].zend_vertex);

		    }
		  for(;screen_acquired;); // we wait for screen_acquired back to 0;
		  screen_acquired = 1;

		  acquire_bitmap(screen);
		  circle(screen, xpr,ypr, 2, color);
		  line(screen,xpr,ypr,x_0 + dx,y_0 + dy,color);
		  release_bitmap(screen);
		  screen_acquired = 0;
		}
	      draw_bubble(screen, B_RIGHT, SCREEN_W-64, 64, "x1 = %g y2 %g %d %d",
			  x_pr_2_pltdata(pr,x_0 + dx),y_pr_2_pltdata(pr,y_0 + dy),i_ev,i_seg);
	      //y = y_pr_2_pltdata_raw(pr, y_0 + dy);
	      //x = x_pr_2_pltdata_raw(pr, x_0 + dx);
	    }
	  if (mouse_b & 0x01)
	    {   // on left click we record the point as a new segment
	      if (iseg_chg == 0 && iseg < 64)
		{
		  xpr = x_0 + dx;
		  ypr = y_0 + dy;
		  iseg_chg = 1;
		}
	    }
	  else iseg_chg = 0;
	  if (key[KEY_ESC])	return D_REDRAWME;	// escape get out
	  if (key[KEY_DEL])
	    {
	      i = win_printf("Do you want to delete event\n %d at %d, n_ae %d seg %d"
			     ,b_r->a_e[i_ev].e_s[0].i_ev,i_ev,b_r->na_e,i_seg);
	      if (i == WIN_CANCEL) return D_O_K;
	      if (b_r->a_e[i_ev].e_s != NULL && i_ev < b_r->na_e && i_ev >= 0)
		{
		  free(b_r->a_e[i_ev].e_s);
		  b_r->a_e[i_ev].e_s = NULL;
		  for (i = i_ev+1; i < b_r->na_e; i++)
		    b_r->a_e[i-1] = b_r->a_e[i];
		  b_r->na_e = (b_r->na_e) ? b_r->na_e - 1 : 0;
		  b_r->ia_e = (b_r->ia_e >= b_r->na_e) ? b_r->na_e - 1 : b_r->ia_e;
		}
	      scope_z_fil(pr, op, g_r, bead_nb);
	      op->need_to_refresh = 1;
	      return refresh_plot(pr, UNCHANGED);
	    }
	  ldx = dx;
	  ldy = dy;
	}

      return 0; //win_printf_OK("Modif fini");


    }


  /*
    px = x_m - cur_ac_reg->area.x0;
    py = cur_ac_reg->area.y0 - y_m;
  */
  xp[0] = x_0;   // this is the starting point
  yp[0] = y_0;
  ldy = 0;
  ldx = 0;
  color = makecol(255, 64, 64);
  iseg_chg = 1;
  while ((mouse_b & 0x02) == 0)
    {     // as long as no right click
      dx = mouse_x - x_0;
      dy = mouse_y - y_0;
      if  (ldy != dy || dx != ldx)
	{   // if mouse moving we update line
	  blit_plot_reg(d);
	  for(;screen_acquired;); // we wait for screen_acquired back to 0;
	  screen_acquired = 1;
	  acquire_bitmap(screen);
	  circle(screen, xp[0],yp[0], 2, color);
	  for (i = 1; i <= iseg; i++)
	    {
	      line(screen,xp[i-1],yp[i-1],xp[i],yp[i],color);
	      circle(screen, xp[i],yp[i], 2, color);
	    }
	  line(screen,xp[iseg],yp[iseg],x_0 + dx,y_0 + dy,color);
	  release_bitmap(screen);
	  screen_acquired = 0;
	  // we display point values
	  draw_bubble(screen, B_LEFT, 20, 64, "x0 = %g y0 %g iseg %d",
		      x_pr_2_pltdata(pr,x_0),y_pr_2_pltdata(pr,y_0),iseg);
	  draw_bubble(screen, B_RIGHT, SCREEN_W-64, 64, "x1 = %g y2 %g",
		      x_pr_2_pltdata(pr,x_0 + dx),y_pr_2_pltdata(pr,y_0 + dy));
	  //y = y_pr_2_pltdata_raw(pr, y_0 + dy);
	  //x = x_pr_2_pltdata_raw(pr, x_0 + dx);
	}
      if (mouse_b & 0x01)
	{   // on left click we record the point as a new segment
	  if (iseg_chg == 0 && iseg < 64)
	    {
	      iseg++;
	      xp[iseg] = x_0 + dx;
	      yp[iseg] = y_0 + dy;
	      iseg_chg = 1;
	    }
	}
      else iseg_chg = 0;
      if (key[KEY_ESC])	return D_REDRAWME;	// escape get out
      ldx = dx;
      ldy = dy;
    }
  if ( abs(dy) > 5 || abs(dx) > 5)
    {
      if (iseg == 0) return 0;
      for (i = 1; i <= iseg; i++)
	{
	  i_1 = i - 1;
	  lx = xp[i] - xp[i_1];
	  ly = yp[i] - yp[i_1];
	  if (8*abs(ly) < abs(lx))         seg[i_1] = 'N';
	  else if (8*abs(lx) < ly)         seg[i_1] = 'H';
	  else if (8*abs(lx) < -ly)        seg[i_1] = 'R';
	  else if (8*ly > abs(lx))         seg[i_1] = 'Z';
	  else if (8*ly < -abs(lx))        seg[i_1] = 'U';
	  else
	    {
	      seg[i_1] = 'x';
	      win_printf("x seg %d \nx0 %d y0 %d\n lx %d ly %d ",i,xp[i_1],yp[i_1],lx,ly);
	    }
	}
      seg[i-1] = 0;
      //win_printf("poly %d seg val %s",iseg,seg);
      for (i = 0; i <= iseg; i++)
	{
	  yf[i] = y_pr_2_pltdata_raw(pr, yp[i]);
	  xf[i] = x_pr_2_pltdata_raw(pr, xp[i]);
	}
      //do_simple_segment(pr, op, op->dat[op->cur_dat], xa, ya, x, y);
      do_poly_segments(pr, op, op->dat[op->cur_dat], iseg, xf, yf, seg);
      return 0;
    }
  return D_O_K;
}



int find_selected_pr_dash_poly_line(pltreg *pr, int x_0, int y_0, DIALOG *d)
{
  int   dx = 0, dy = 0, ldx, ldy, color, iseg = 0, i, i_1, k, page_n, i_page, iseg_chg = 0;
  //float  xa, ya;// x, y,
  int xp[64], yp[64], lx, ly, i_ev, i_seg, end, i_seg2, xpr, ypr, bead_nb;
  float xf[64], yf[64];
  char seg[64];
  //struct box *b;
  O_p *op;
  d_s *ds = NULL;
  g_record *g_r = NULL;
  b_record *b_r = NULL;

  if (pr->one_p == NULL)		return D_O_K;
  g_r = find_g_record_in_pltreg(pr);
  if (g_r == NULL)   win_printf_OK("No gen record found");
  op = pr->one_p;
  bead_nb = op->user_ispare[ISPARE_BEAD_NB];
  if (bead_nb < 0 || bead_nb >= g_r->n_bead)  win_printf_OK("bead nb out of range");
  b_r = g_r->b_r[bead_nb];
  op = pr->one_p;
  //sc = pr->screen_scale;
  //b = pr->stack;
  //ya = y_pr_2_pltdata_raw(pr, y_0);// y =
  //xa = x_pr_2_pltdata_raw(pr, x_0);// x =

  ds = find_source_specific_ds_in_op(op,"event polyline");
  for (i = 0, i_ev = i_seg = end = -1; ds != NULL && i < ds->nx; i++)
    {
      if (abs(x_pltdata_2_pr(pr, ds->xd[i])+d->x - x_0) < 5
	  && abs(y_pltdata_2_pr(pr, ds->yd[i])+d->y - y_0) < 5)
	{
	  if (find_segment_from_point(pr, op, ds->xd[i], ds->yd[i], &i_ev, &i_seg, &end))
	    win_printf("did not find Polyline modif i %d",i);

	  //win_printf("Found event %d segment %d end %d ds i %d",i_ev, i_seg, end,i);
	  break;
	}
      if (i == 0)
	{
	  display_title_message("x0 %d y0 %d -> x %d y %d", x_0,y_0,x_pltdata_2_pr(pr, ds->xd[0]),
		     y_pltdata_2_pr(pr, ds->yd[i]));
	}
    }

  if (i_ev >= 0 && i_seg >= 0)
    {  // we mofify an existing segment
      i_seg2 = -1;
      if (end == 2)	i_seg2 = ((i_seg + 1) < b_r->a_e[i_ev].n_seg) ? (i_seg + 1) : -1;
      else if (end == 1)  i_seg2 = ((i_seg - 1) >= 0) ? (i_seg - 1) : -1;

      //win_printf("Found 1");

      ldy = 0;
      ldx = 0;
      color = makecol(255, 64, 64);
      iseg_chg = 1;
      while ((mouse_b & 0x02) == 0)
	{     // as long as no right click
	  dx = mouse_x - x_0;
	  dy = mouse_y - y_0;
	  if  (ldy != dy || dx != ldx)
	    {   // if mouse moving we update line
	      blit_plot_reg(d);
	      //win_printf("Found 1.5");
	      if (end == 2)
		{
		  k = b_r->a_e[i_ev].e_s[i_seg].seg_start;
		  page_n = k/g_r->page_size;
		  i_page = k%g_r->page_size;
		  xpr = x_pltdata_2_pr(pr,(g_r->timing_mode == 1) ? (g_r->imi[page_n][i_page] - g_r->imi_start)
				       : g_r->imi[page_n][i_page]);
		  ypr = y_pltdata_2_pr(pr, b_r->a_e[i_ev].e_s[i_seg].zstart_vertex);
		  ypr =  d->y + ypr; // SCREEN_H
		}
	      else
		{
		  k = b_r->a_e[i_ev].e_s[i_seg].seg_end;
		  page_n = k/g_r->page_size;
		  i_page = k%g_r->page_size;
		  xpr = x_pltdata_2_pr(pr,(g_r->timing_mode == 1) ? (g_r->imi[page_n][i_page] - g_r->imi_start)
				       : g_r->imi[page_n][i_page]);
		  ypr = y_pltdata_2_pr(pr, b_r->a_e[i_ev].e_s[i_seg].zend_vertex);
		  ypr =  d->y + ypr; // SCREEN_H
		}
	      //win_printf("Found 2");
	      for(;screen_acquired;); // we wait for screen_acquired back to 0;
	      screen_acquired = 1;
	      acquire_bitmap(screen);
	      circle(screen, xpr,ypr, 2, color);
	      line(screen,xpr,ypr,x_0 + dx,y_0 + dy,color);
	      release_bitmap(screen);
	      screen_acquired = 0;
	      if (i_seg2 >= 0)
		{
		  if (end == 2)
		    {
		      k = b_r->a_e[i_ev].e_s[i_seg2].seg_end;
		      page_n = k/g_r->page_size;
		      i_page = k%g_r->page_size;
		      xpr = x_pltdata_2_pr(pr,(g_r->timing_mode == 1) ? (g_r->imi[page_n][i_page] - g_r->imi_start)
					   : g_r->imi[page_n][i_page]);
		      ypr = y_pltdata_2_pr(pr, b_r->a_e[i_ev].e_s[i_seg2].zend_vertex);
		      ypr =  d->y + ypr; // SCREEN_H
		      draw_bubble(screen, B_LEFT, 64, 64, "ev %d seg %d xs = %d ys %g"
				  ,i_ev,i_seg2,xpr, b_r->a_e[i_ev].e_s[i_seg2].zstart_vertex);


		    }
		  else
		    {
		      k = b_r->a_e[i_ev].e_s[i_seg2].seg_start;
		      page_n = k/g_r->page_size;
		      i_page = k%g_r->page_size;
		      xpr = x_pltdata_2_pr(pr,(g_r->timing_mode == 1) ? (g_r->imi[page_n][i_page] - g_r->imi_start)
					   : g_r->imi[page_n][i_page]);
		      ypr = y_pltdata_2_pr(pr, b_r->a_e[i_ev].e_s[i_seg2].zstart_vertex);
		      ypr =  d->y + ypr; // SCREEN_H
		      draw_bubble(screen, B_LEFT, 64, 64, "ev %d seg %d xs = %d ys %g"
				  ,i_ev,i_seg2,xpr, b_r->a_e[i_ev].e_s[i_seg].zend_vertex);

		    }
		  for(;screen_acquired;); // we wait for screen_acquired back to 0;
		  screen_acquired = 1;

		  acquire_bitmap(screen);
		  circle(screen, xpr,ypr, 2, color);
		  line(screen,xpr,ypr,x_0 + dx,y_0 + dy,color);
		  release_bitmap(screen);
		  screen_acquired = 0;
		}
	      draw_bubble(screen, B_RIGHT, SCREEN_W-64, 64, "x1 = %g y2 %g %d %d",
			  x_pr_2_pltdata(pr,x_0 + dx),y_pr_2_pltdata(pr,y_0 + dy),i_ev,i_seg);
	      //y = y_pr_2_pltdata_raw(pr, y_0 + dy);
	      //x = x_pr_2_pltdata_raw(pr, x_0 + dx);
	    }
	  if (mouse_b & 0x01)
	    {   // on left click we record the point as a new segment
	      if (iseg_chg == 0 && iseg < 64)
		{
		  xpr = x_0 + dx;
		  ypr = y_0 + dy;
		  iseg_chg = 1;
		}
	    }
	  else iseg_chg = 0;
	  if (key[KEY_ESC])	return D_REDRAWME;	// escape get out
	  if (key[KEY_DEL])
	    {
	      i = win_printf("Do you want to delete event\n %d at %d, n_ae %d seg %d"
			     ,b_r->a_e[i_ev].e_s[0].i_ev,i_ev,b_r->na_e,i_seg);
	      if (i == WIN_CANCEL) return D_O_K;
	      if (b_r->a_e[i_ev].e_s != NULL && i_ev < b_r->na_e && i_ev >= 0)
		{
		  free(b_r->a_e[i_ev].e_s);
		  b_r->a_e[i_ev].e_s = NULL;
		  for (i = i_ev+1; i < b_r->na_e; i++)
		    b_r->a_e[i-1] = b_r->a_e[i];
		  b_r->na_e = (b_r->na_e) ? b_r->na_e - 1 : 0;
		  b_r->ia_e = (b_r->ia_e >= b_r->na_e) ? b_r->na_e - 1 : b_r->ia_e;
		}
	      scope_z_fil(pr, op, g_r, bead_nb);
	      op->need_to_refresh = 1;
	      return refresh_plot(pr, UNCHANGED);
	    }
	  ldx = dx;
	  ldy = dy;
	}

      return 0; //win_printf_OK("Modif fini");


    }

  xp[0] = x_0;   // this is the starting point
  yp[0] = y_0;
  ldy = 0;
  ldx = 0;
  color = makecol(255, 64, 64);
  iseg_chg = 1;
  while ((mouse_b & 0x02) == 0)
    {     // as long as no right click
      dx = mouse_x - x_0;
      dy = mouse_y - y_0;
      if  (ldy != dy || dx != ldx)
	{   // if mouse moving we update line
	  blit_plot_reg(d);
	  for(;screen_acquired;); // we wait for screen_acquired back to 0;
	  screen_acquired = 1;
	  acquire_bitmap(screen);
	  for (i = 1; i <= iseg; i+=2)
	    {
	      circle(screen, xp[i-1],yp[i-1], 2, color);
	      line(screen,xp[i-1],yp[i-1],xp[i],yp[i],color);
	      circle(screen, xp[i],yp[i], 2, color);
	    }
	  line(screen,xp[iseg],yp[iseg],x_0 + dx,y_0 + dy,color);
	  release_bitmap(screen);
	  screen_acquired = 0;
	  // we display point values
	  draw_bubble(screen, B_LEFT, 20, 64, "x0 = %g y0 %g iseg %d",
		      x_pr_2_pltdata(pr,x_0),y_pr_2_pltdata(pr,y_0),iseg);
	  draw_bubble(screen, B_RIGHT, SCREEN_W-64, 64, "x1 = %g y2 %g",
		      x_pr_2_pltdata(pr,x_0 + dx),y_pr_2_pltdata(pr,y_0 + dy));
	  //y = y_pr_2_pltdata_raw(pr, y_0 + dy);
	  //x = x_pr_2_pltdata_raw(pr, x_0 + dx);
	}
      if (mouse_b & 0x01)
	{   // on left click we record the point as a new segment
	  if (iseg_chg == 0 && iseg < 64)
	    {
	      iseg++;
	      xp[iseg] = x_0 + dx;
	      yp[iseg] = y_0 + dy;
	      iseg_chg = 1;
	    }
	}
      else iseg_chg = 0;
      if (key[KEY_ESC])	return D_REDRAWME;	// escape get out
      ldx = dx;
      ldy = dy;
    }
  if ( abs(dy) > 5 || abs(dx) > 5)
    {
      if (iseg == 0) return 0;
      for (i = 1; i <= iseg; i++)
	{
	  i_1 = i - 1;
	  lx = xp[i] - xp[i_1];
	  ly = yp[i] - yp[i_1];
	  if (i%2 == 0)                    seg[i_1] = ' ';
	  else if (8*abs(ly) < abs(lx))    seg[i_1] = 'N';
	  else if (8*abs(lx) < ly)         seg[i_1] = 'H';
	  else if (8*abs(lx) < -ly)        seg[i_1] = 'R';
	  else if (8*ly > abs(lx))         seg[i_1] = 'Z';
	  else if (8*ly < -abs(lx))        seg[i_1] = 'U';
	  else
	    {
	      seg[i_1] = 'x';
	      win_printf("x seg %d \nx0 %d y0 %d\n lx %d ly %d ",i,xp[i_1],yp[i_1],lx,ly);
	    }
	}
      seg[i-1] = 0;
      //win_printf("poly %d seg val %s",iseg,seg);
      for (i = 0; i <= iseg; i++)
	{
	  yf[i] = y_pr_2_pltdata_raw(pr, yp[i]);
	  xf[i] = x_pr_2_pltdata_raw(pr, xp[i]);
	}
      //do_simple_segment(pr, op, op->dat[op->cur_dat], xa, ya, x, y);
      do_poly_dash_segments(pr, op, op->dat[op->cur_dat], iseg, xf, yf, seg);
      return 0;
    }
  return D_O_K;
}



# ifdef OLD

int find_selected_pr_simple_segment(pltreg *pr, int x_0, int y_0, DIALOG *d)
{
  int   dx = 0, dy = 0, ldx, ldy, color, sc;
  float x, y, xa, ya;
  struct box *b;
  O_p *op;

  if (pr->one_p == NULL)		return D_O_K;
  op = pr->one_p;
  sc = pr->screen_scale;
  b = pr->stack;
  /*
	dxmax =  SCREEN_W - x_m - 32 ;
	dxmin =  -x_m;
	dymax = SCREEN_H - y_m - 32;
	dymin =  - y_m;
  */
  y = ya = y_pr_2_pltdata_raw(pr, y_0);
  x = xa = x_pr_2_pltdata_raw(pr, x_0);

  /*
    px = x_m - cur_ac_reg->area.x0;
    py = cur_ac_reg->area.y0 - y_m;
  */
  ldy = 0;
  ldx = 0;
  color = makecol(255, 64, 64);

  while (mouse_b & 0x03)
    {
      dx = mouse_x - x_0;
      dy = mouse_y - y_0;
      if  (ldy != dy || dx != ldx)
	{
	  blit_plot_reg(d);
	  for(;screen_acquired;); // we wait for screen_acquired back to 0;
	  screen_acquired = 1;
	  acquire_bitmap(screen);
	  line(screen,x_0,y_0,x_0 + dx,y_0 + dy,color);
	  release_bitmap(screen);
	  screen_acquired = 0;
	  draw_bubble(screen, B_LEFT, 20, 64, "x0 = %g y0 %g", x_pr_2_pltdata(pr,x_0),y_pr_2_pltdata(pr,y_0));
	  draw_bubble(screen, B_RIGHT, SCREEN_W-64, 64, "x1 = %g y2 %g",x_pr_2_pltdata(pr,x_0 + dx),
		      y_pr_2_pltdata(pr,y_0 + dy));
	  y = y_pr_2_pltdata_raw(pr, y_0 + dy);
	  x = x_pr_2_pltdata_raw(pr, x_0 + dx);
	}
      if (key[KEY_ESC])	return D_REDRAWME;
      ldx = dx;
      ldy = dy;
    }
  if ( abs(dy) > 5 || abs(dx) > 5)
    {
      do_simple_segment(pr, op, op->dat[op->cur_dat], xa, ya, x, y);
      return 0;
    }
  return D_O_K;
}

# endif

int 	do_select_pr_simple_segment(void)
{
  if(updating_menu_state != 0)
    {
      if ((general_pr_action == find_selected_pr_poly_line)
	  && strstr(active_menu->text, "Unselect Multi segments line") == NULL)
	  snprintf(active_menu->text,128,"Unselect Multi segments line");
      else  if ((general_pr_action != find_selected_pr_poly_line)
	  && strstr(active_menu->text, "Unselect Multi segments line") != NULL)
	  snprintf(active_menu->text,128,"Select Multi segments line");
      return D_O_K;
    }

  if (general_pr_action == find_selected_pr_poly_line)
    {
      general_pr_action = prev_general_pr_action;
      prev_general_pr_action = find_selected_pr_poly_line;
    }
  else
    {
      prev_general_pr_action = general_pr_action;
      general_pr_action = find_selected_pr_poly_line;
    }
  return 0;
}

int 	do_select_pr_dash_segment(void)
{
  if(updating_menu_state != 0)
    {
      if ((general_pr_action == find_selected_pr_dash_poly_line)
	  && strstr(active_menu->text, "Unselect Multi dash line") == NULL)
	  snprintf(active_menu->text,128,"Unselect Multi dash line");
      else  if ((general_pr_action != find_selected_pr_dash_poly_line)
	  && strstr(active_menu->text, "Unselect Multi dash line") != NULL)
	  snprintf(active_menu->text,128,"Select Multi dash line");
      return D_O_K;
    }

  if (general_pr_action == find_selected_pr_dash_poly_line)
    {
      general_pr_action = prev_general_pr_action;
      prev_general_pr_action = find_selected_pr_dash_poly_line;
    }
  else
    {
      prev_general_pr_action = general_pr_action;
      general_pr_action = find_selected_pr_dash_poly_line;
    }
  return 0;
}


MENU *select_plot_menu(void)
{
  static MENU mn[32];
  char *mnstr = NULL;
//  int display_analyzed_event(void);
//  int find_analyzed_event_for_slipage(void);
//  int do_write_analyzed_events(void);
//  int do_read_analyzed_events(void);
//  int find_analyzed_event_single_Z_or_U(void);
//  int histo_analyzed_event_single_segment(void);
//  int erase_bead_recorded_events(void);
//  int find_analyzed_event_ZN_or_UN(void);
//  int histo_analyzed_event_URNHU(void);
//  int histo_analyzed_hybrid_events(void);
//  int find_analyzed_event_for_translocation(void);
//  int histo_analyzed_event_NNN(void);

  if (mn[0].text != NULL)	return mn;


  mnstr = (char *) calloc(128,sizeof(char));
  if (mnstr == NULL) return NULL;
  snprintf(mnstr,128,"Select Multi segments line");
  add_item_to_menu(mn,mnstr, do_select_pr_simple_segment,NULL,0,NULL);
  mnstr = (char *) calloc(128,sizeof(char));
  if (mnstr == NULL) return NULL;
  snprintf(mnstr,128,"Select Multi dash line");
  add_item_to_menu(mn,mnstr, do_select_pr_dash_segment,NULL,0,NULL);
  add_item_to_menu(mn, "\0",   NULL,   NULL,   0, NULL);
  add_item_to_menu(mn, "Display event", display_analyzed_event,NULL,0, NULL);
  add_item_to_menu(mn, "Find UHU or ZHZ events", find_analyzed_event_for_slipage,NULL,0, NULL);
  add_item_to_menu(mn, "Find NHN or NRN events", find_analyzed_event_for_NRN_or_NHN,NULL,0, NULL);
  add_item_to_menu(mn,"Print Zmags",do_test_list_zmag_cmd_values_frame_list,NULL,0,NULL);



  add_item_to_menu(mn, "Find UN or ZN events", find_analyzed_event_ZN_or_UN,NULL,0, NULL);
  add_item_to_menu(mn, "Find single U or Z event", find_analyzed_event_single_Z_or_U,NULL,0, NULL);
  add_item_to_menu(mn, "Find single H or R event", find_analyzed_event_single_H_or_R,NULL,0, NULL);

  add_item_to_menu(mn, "Find NRNHN events", find_analyzed_event_for_translocation,NULL,0, NULL);
  add_item_to_menu(mn, "\0",   NULL,   NULL,   0, NULL);
  add_item_to_menu(mn, "statistic on single segment", histo_analyzed_event_single_segment,NULL,0, NULL);
  add_item_to_menu(mn, "statistic on evanescent hybrid", histo_analyzed_hybrid_events,NULL,0, NULL);

  add_item_to_menu(mn, "statistic on traslocation event NNN",histo_analyzed_event_NNN ,NULL,0, NULL);
  add_item_to_menu(mn, "statistic on URNHU event", histo_analyzed_event_URNHU,NULL,0, NULL);
  add_item_to_menu(mn,"statistics on two level",histo_analyzed_event_NRNHN_two_levels,NULL,0,NULL);
  add_item_to_menu(mn,"statistics on discrete levels",histo_analyzed_event_NRNHN_discrete_levels,NULL,0,NULL);
  add_item_to_menu(mn,"Sort events by starting time",sort_events_by_starting_time,NULL,0,NULL);
  add_item_to_menu(mn, "\0",   NULL,   NULL,   0, NULL);
  add_item_to_menu(mn, "Save events", do_write_analyzed_events,NULL,0, NULL);
  add_item_to_menu(mn, "Read events", do_read_analyzed_events,NULL,0, NULL);
  add_item_to_menu(mn, "Erase events", erase_bead_recorded_events,NULL,0, NULL);


  insert_item_to_menu_at_line(plot_file_menu, 3, "Save events", do_write_analyzed_events,NULL,0, NULL);
  insert_item_to_menu_at_line(plot_file_menu, 4, "Read events", do_read_analyzed_events,NULL,0, NULL);
  insert_item_to_menu_at_line(plot_file_menu, 5, "Read corrections", load_correction_from_file,NULL,0, NULL);
  insert_item_to_menu_at_line(plot_file_menu, 6,"\0",  NULL,   NULL,   0, NULL);



  return mn;
}


# endif
