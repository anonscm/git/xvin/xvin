#ifndef _REAL_TIME_AVG_C_
#define _REAL_TIME_AVG_C_



# include "allegro.h"
# include "winalleg.h"
# include "ctype.h"
# include "xvin.h"
# include "fftl32n.h"
# include "fillib.h"
# include <fftw3.h>		// include the fftw v3.0.1 or v3.1.1 library for computing spectra:
# include "../fft2d/fft2d.h"
# include "../fluobead/fluobead.h"


/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "../cfg_file/Pico_cfg.h"
//# include "../phaseindex/phaseindex.h"
#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)

# include "PlayItSam.h"
# include "real_time_avg.h"
# include "action_live_video.h"
//# include "focus.h"


int check_heap_raw(char *file, unsigned long line)
{
  int out;

  out = _heapchk();
  if (out != -2)  win_printf("File %s at line %d\nheap satus %d (-2 =>OK)\n",file,(int)line,out);
  return (out == -2) ? 0 : out;
}

int timing_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  register int i, j, k;
  static int  immax = -1; // last_c_i = -1,
  d_s *dsx, *dsy, *dsz;
  int nf, lost_fr, lf, jstart = 0, j1 = 0;
  unsigned long   dtm, dtmax, dts;
  float   fdtm, fdtmax;
  double min_delay_camera_thread_in_ms, tmp, tmp2;
  long long llt;

  if (real_time_info == NULL) return D_O_K;
  dts = get_my_uclocks_per_sec();
  dsx = find_source_specific_ds_in_op(op,"Timing rolling buffer");
  dsy = find_source_specific_ds_in_op(op,"Period rolling buffer");
  dsz = find_source_specific_ds_in_op(op,"Timer rolling buffer");
  if (dsx == NULL || dsy == NULL || dsz == NULL) return D_O_K;	

  i = real_time_info->c_i;
  if (real_time_info->imi[i] <= immax) 
    {
        my_set_window_title("waiting %d %d",real_time_info->imi[i], immax);
	return D_O_K;   // we are up todate
    }

  min_delay_camera_thread_in_ms = bid.min_delay_camera_thread_in_ms;

  i = real_time_info->c_i;
  immax = real_time_info->imi[i];               
  lf = immax - real_time_info->ac_i;
  if (real_time_info->ac_i > (REAL_TIME_BUFFER_SIZE>>1)) 
    {
      nf = REAL_TIME_BUFFER_SIZE>>1;
      j = jstart = (real_time_info->ac_i - (REAL_TIME_BUFFER_SIZE>>1))%REAL_TIME_BUFFER_SIZE;  //last_c_i;
    }
  else
    {
      nf = real_time_info->ac_i-1;
      j = jstart = 1;
    }
  dsx->nx = dsx->ny = nf;
  dsy->nx = dsy->ny = nf;
  dsz->nx = dsz->ny = nf;
  
  j = jstart + nf - 1;
  j = (j < REAL_TIME_BUFFER_SIZE) ? j : j - REAL_TIME_BUFFER_SIZE;
  llt = real_time_info->imt[j];
  j1 = j;
  j = jstart;
  j = (j < REAL_TIME_BUFFER_SIZE) ? j : j - REAL_TIME_BUFFER_SIZE;
  llt -= real_time_info->imt[j];
  llt /= (nf - 1);
  
  for (i = 0, j = jstart, lost_fr = 0, dtm = dtmax = 0, fdtm = fdtmax = 0, k = 0, tmp2 = 0, lf = 0; i < nf; i++, j++, k++)
    {
      j = (j < REAL_TIME_BUFFER_SIZE) ? j : j - REAL_TIME_BUFFER_SIZE;
      dsx->xd[i] = dsy->xd[i] = dsz->xd[i] = real_time_info->imi[j];
      if (i > 0)
	lf += real_time_info->imi[j] - real_time_info->imi[j1] - 1;
      dsx->yd[i] = ((float)(1000*real_time_info->imdt[j]))/dts;
      tmp = real_time_info->imt[j];
      tmp -= (i == 0) ? real_time_info->imt[j] : real_time_info->imt[j1];
      tmp = 1000*(double)(tmp)/get_my_ulclocks_per_sec();
      //tmp -= bid.image_per_in_ms * i;
      tmp2 += tmp;
      dsy->yd[i] = (float)tmp;
      dsz->yd[i] = real_time_info->imit[j];

      dtm += real_time_info->imdt[j];
      fdtm += dsx->yd[i];
      dtmax = (real_time_info->imdt[j] > dtmax) ? real_time_info->imdt[j] : dtmax;
      fdtmax = (dsx->yd[i] > fdtmax) ? dsx->yd[i] : fdtmax;
      j1 = j;
    }
  dsy->yd[0] =  dsy->yd[1];
  op->need_to_refresh = 1; 
  if (k) set_plot_title(op, "\\stack{{fr position lac_i %d }{%d missed images}"
			"{\\pt8 dt mean %6.3f ms (%6.3f ms) max %6.3f (%6.3f)}}"
			,bid.first_im +real_time_info->ac_i 
			,lf,fdtm/k,
			1000*((double)(dtm/k))/dts,
			fdtmax, 1000*((double)(dtmax))/dts);
  i = REAL_TIME_BUFFER_SIZE/8;
  j = ((int)dsx->xd[1])/i;
  op->x_lo = j*i;
  op->x_hi = (j+5)*i;
  set_plot_x_fixed_range(op);
  return refresh_plot(pr_LIVE_VIDEO, UNCHANGED);
}


	
int obj_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  register int i, j;
  static int last_c_i = -1, immax = -1;
  d_s *dsx;
  int nf;
  
  if (real_time_info == NULL) return D_O_K;

  dsx = find_source_specific_ds_in_op(op,"Obj. rolling buffer");
  if (dsx == NULL) return D_O_K;	

  i = real_time_info->c_i;
  if (real_time_info->imi[i] <= immax) return D_O_K;   // we are uptodate
  immax = real_time_info->imi[i];               
  nf = (real_time_info->ac_i > REAL_TIME_BUFFER_SIZE) ? REAL_TIME_BUFFER_SIZE : real_time_info->ac_i;
  nf = (nf > (REAL_TIME_BUFFER_SIZE>>1)) ? REAL_TIME_BUFFER_SIZE>>1 : nf;
  dsx->nx = dsx->ny = nf;
  last_c_i = (real_time_info->c_i > 0) ? real_time_info->c_i - 1 : 0;  // c_i is not yet valid
  for (i = nf-1, j = last_c_i; i >= 0; i--, j--)
    {
      j = (j < 0) ? REAL_TIME_BUFFER_SIZE + j: j;
      j = (j < REAL_TIME_BUFFER_SIZE) ? j : REAL_TIME_BUFFER_SIZE;
      dsx->xd[i] = real_time_info->imi[j];
      dsx->yd[i] = real_time_info->obj_pos[j];
    }
  i = REAL_TIME_BUFFER_SIZE/8;
  j = ((int)dsx->xd[i])/i;
  op->x_lo = (j-1)*i;
  op->x_hi = (j+4)*i;
  set_plot_x_fixed_range(op);

  op->need_to_refresh = 1; 
  set_plot_title(op, "\\stack{{image %d}{Obj %g obj}}"
		 ,real_time_info->ac_i, real_time_info->obj_pos[last_c_i]);
  return refresh_plot(pr_LIVE_VIDEO, UNCHANGED);
}

	
int raw_histo_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  register int i, j;
  d_s *dsx, *dsy;
/*   int nf, im, itmp; */
  static int immax = -1, wi, min;
  
  if (real_time_info == NULL) return D_O_K;

  i = real_time_info->c_i;
  if (real_time_info->imi[i] <= immax) return D_O_K;   // we are uptodate
  immax = real_time_info->imi[i];               

  dsx = find_source_specific_ds_in_op(op,"Raw bar histogram");
  if (dsx == NULL) return D_O_K;	

  dsy = find_source_specific_ds_in_op(op,"Raw histogram");
  if (dsx == NULL) return D_O_K;	

  if (op->title) 
    {
      if (sscanf(op->title, "\\stack{{Raw histo}{min %d width %d}}",&min,&wi) == 2)
	{
	  real_time_info->raw_min = min;
	  real_time_info->raw_wi = wi;
	}
    }

  if (real_time_info->raw_histo_update == 1) return D_O_K;   // we are uptodate

  for (i = 0; i < 1024; i+=4)
    {
      j = i/4;
      dsx->xd[i] = dsx->xd[i+1] = j - 0.4;
      dsx->xd[i+2] = dsx->xd[i+3] = j + 0.4;
      dsy->xd[j] = j;
      dsx->yd[i] = dsx->yd[i+3] = 0;
      dsx->yd[i+2] = dsx->yd[i+1] = real_time_info->raw_histotmp[j];
      dsy->yd[j] = real_time_info->raw_histotmp[j];
    }

  op->need_to_refresh = 1; 
  set_plot_title(op, "\\stack{{Raw histo}{min %d width %d}}", real_time_info->raw_min, real_time_info->raw_wi);
  real_time_info->raw_histo_update = 1;
  return refresh_plot(pr_LIVE_VIDEO, UNCHANGED);
}


int avg_pxl_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  register int i, j, jim;
  static int last_c_i = -1, immax = -1, itmp, x, y;
  d_s *dsx;
  int nf;
  
  if (real_time_info == NULL) return D_O_K;

  dsx = find_source_specific_ds_in_op(op,"Averaged pixel value versus time");
  if (dsx == NULL) return D_O_K;	

  i = real_time_info->c_i;
  if (real_time_info->imi[i] <= immax) return D_O_K;   // we are uptodate
  immax = real_time_info->imi[i];               

  
  if (op->title) 
    {
      if (sscanf(op->title,"Image %d  pixel displayed x %d y %d",&itmp,&x,&y) == 3)
	{
	  x = (x < real_time_info->avg_nx) ? x : real_time_info->avg_nx - 1;
	  x = (x < 0) ? 0 : x;
	  y = (y < real_time_info->avg_ny) ? y : real_time_info->avg_ny - 1;
	  y = (y < 0) ? 0 : y;
	  real_time_info->x_pxl = x;
	  real_time_info->y_pxl = y;
	}
    }


  nf = (real_time_info->ac_i > real_time_info->avg_nf) ? real_time_info->avg_nf : real_time_info->ac_i;
  dsx->nx = dsx->ny = nf;
  last_c_i = (real_time_info->c_i > 0) ? real_time_info->c_i - 1 : 0;  // c_i is not yet valid
  for (i = nf-1, j = last_c_i, jim = real_time_info->mc_i; i >= 0; i--, j--, jim--)
    {
      j = (j < 0) ? REAL_TIME_BUFFER_SIZE + j: j;
      j = (j < REAL_TIME_BUFFER_SIZE) ? j : REAL_TIME_BUFFER_SIZE;
      dsx->xd[i] = real_time_info->imi[j];
      jim = (jim < 0) ? real_time_info->avg_nf + jim: jim;
      jim = (jim < real_time_info->avg_nf) ? jim : real_time_info->avg_nf-1;
      dsx->yd[i] = real_time_info->oi_avg->im.pxl[jim][real_time_info->y_pxl].li[real_time_info->x_pxl];
      dsx->yd[i] /= real_time_info->avg_bin*real_time_info->avg_bin;
    }
  i = real_time_info->avg_nf/8;
  j = ((int)dsx->xd[dsx->nx-1])/i;
  op->x_lo = (j-1)*i;
  j = ((int)dsx->xd[0])/i;
  op->x_hi = (j+1)*i;
  set_plot_x_fixed_range(op);

  op->need_to_refresh = 1; 

  set_plot_title(op, "Image %d  pixel displayed x %d y %d" 
		 ,real_time_info->ac_i,real_time_info->x_pxl,real_time_info->y_pxl);

  return refresh_plot(pr_LIVE_VIDEO, UNCHANGED);
}


	
int avg_pxl_correlation_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  register int i, j, jim;
  static int last_c_i = -1, immax = -1;// itmp, x, y;
  d_s *dsx;
  int nf, ipx, ipy;
  double re, im, mean;
  static double  	*in1=NULL;
  static fftw_complex	*outc1=NULL;
  static fftw_plan     plan1;     // fft plan for fftw3
  static n_fftw = 0;
  
  if (real_time_info == NULL) return D_O_K;

  dsx = find_source_specific_ds_in_op(op,"Averaged pixel correlation");
  if (dsx == NULL) return D_O_K;	

  i = real_time_info->c_i;
  if (real_time_info->imi[i] <= immax) return D_O_K;   // we are uptodate
  immax = real_time_info->imi[i];               


  if (real_time_info->ac_i < real_time_info->avg_nf) return 0;
  nf = (real_time_info->ac_i > real_time_info->avg_nf) ? real_time_info->avg_nf : real_time_info->ac_i;

  for (i = 0; i < nf; i++)       dsx->xd[i] = 0; 
  for (ipy = 0; ipy < real_time_info->avg_ny; ipy++)
    {
      for (ipx = 0; ipx < real_time_info->avg_nx; ipx++)
	{
	  dsx->nx = dsx->ny = nf;
	  last_c_i = (real_time_info->c_i > 0) ? real_time_info->c_i - 1 : 0;  // c_i is not yet valid
	  for (i = nf-1, j = last_c_i, jim = real_time_info->mc_i; i >= 0; i--, j--, jim--)
	    {
	      j = (j < 0) ? REAL_TIME_BUFFER_SIZE + j: j;
	      j = (j < REAL_TIME_BUFFER_SIZE) ? j : REAL_TIME_BUFFER_SIZE;
	      //dsx->xd[i] = real_time_info->imi[j];
	      jim = (jim < 0) ? real_time_info->avg_nf + jim: jim;
	      jim = (jim < real_time_info->avg_nf) ? jim : real_time_info->avg_nf-1;
	      dsx->yd[i] = real_time_info->oi_avg->im.pxl[jim][ipy].li[ipx];
	      dsx->yd[i] /= real_time_info->avg_bin*real_time_info->avg_bin;
	    }
	  if (n_fftw < nf || in1 == NULL || outc1 == NULL)
	    {
	      in1   = fftw_malloc( nf * sizeof(double));
	      outc1 = fftw_malloc((nf/2+1)*sizeof(fftw_complex));
	      if (in1 == NULL || outc1 == NULL)  
		return win_printf_OK("cannot create fftw arrays !");
	      n_fftw = nf;
	    }
	  plan1 = fftw_plan_dft_r2c_1d(nf, in1, outc1, FFTW_ESTIMATE);
	  
	  
	  for (i = 0, mean = 0; i < nf; i++) 
	    {
	      in1[i] = dsx->yd[i]; 
	      mean += in1[i];
	    }
	  for (i = 0, mean /= nf; i < nf; i++) 
	    in1[i] -= mean; 
	  fftw_execute(plan1);
	  
	  outc1[0][0] = outc1[0][0]*outc1[0][0];
	  outc1[0][1] = 0;
	  for (i = 1; i < nf/2; i++) 
	    {
	      re = outc1[i][0]*outc1[i][0] + outc1[i][1]*outc1[i][1]; 
	      im = 0; //outc1[i][0]*outc1[i][1] - outc1[i][1]*outc1[i][0]; 
	      outc1[i][0] = re;
	      outc1[i][1] = im;
	    }
	  outc1[nf/2][0] = outc1[nf/2][0]*outc1[nf/2][0];
	  outc1[nf/2][1] = 0;
	  
	  fftw_destroy_plan(plan1);
	  plan1 = fftw_plan_dft_c2r_1d(nf, outc1, in1, FFTW_ESTIMATE);   
	  fftw_execute(plan1);
	  for (i = 0; i < nf; i++) 
	    {
	      dsx->yd[i] = (float)(in1[i]/nf);
	      dsx->xd[i] += dsx->yd[i]; 
	    }
	  fftw_destroy_plan(plan1);
	}
    }
  for (i = 0; i < nf; i++)       
    {
      dsx->yd[i] =  dsx->xd[i]; 
      dsx->xd[i] = (float)i; 
    }

  //fftw_free(in1); fftw_free(outc1);

  dsx->nx = dsx->ny = nf/2;

  //i = real_time_info->avg_nf/8;
  //op->x_lo = -(float)i/2;
  //op->x_hi = (float)(i+nf)/2;
  //  set_plot_x_fixed_range(op);

  op->need_to_refresh = 1; 

  set_plot_title(op, "Image %d  correlation of all pixels" ,real_time_info->ac_i);

  return refresh_plot(pr_LIVE_VIDEO, UNCHANGED);
}

int single_histo_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  register int i;
  d_s *ds0, *ds1, *ds2, *ds3;
  static int  ds0x, ds0y, ds1x, ds1y, ds2x, ds2y, ds3x, ds3y;
  static int immax = -1;
  
  if (real_time_info == NULL) return D_O_K;

  i = real_time_info->c_i;
  if (real_time_info->imi[i] <= immax) return D_O_K;   // we are uptodate
  immax = real_time_info->imi[i];               

  ds0 = find_source_specific_ds_in_op(op,"Histogram pixel 0");
  if (ds0 == NULL) return D_O_K;	
  ds1 = find_source_specific_ds_in_op(op,"Histogram pixel 1");
  if (ds1 == NULL) return D_O_K;	
  ds2 = find_source_specific_ds_in_op(op,"Histogram pixel 2");
  if (ds2 == NULL) return D_O_K;	
  ds3 = find_source_specific_ds_in_op(op,"Histogram pixel 3");
  if (ds3 == NULL) return D_O_K;	

  if (op->title) 
    {
      sscanf(op->title,"\\stack{{Position}{ds0: %d %d    ds1: %d %d}{ds2: %d %d    ds3: %d %d}}",
	     &ds0x,&ds0y,&ds1x,&ds1y,&ds2x,&ds2y,&ds3x,&ds3y) == 8;
    }
  else
    {
      if (real_time_info->avg_nx > real_time_info->avg_ny)
	{
	  ds0y = ds1y = ds2y = ds3y = 0;
	  ds0x = 0;
	  ds3x = real_time_info->avg_nx - 1;
	  ds1x = (int)(real_time_info->avg_nx / 3);
	  ds2x = (int)(real_time_info->avg_nx * 2/3);
	}
      else
	{
	  ds0x = ds1x = ds2x = ds3x = 0;
	  ds0y = 0;
	  ds3y = real_time_info->avg_ny - 1;
	  ds1y = (int)(real_time_info->avg_ny / 4);
	  ds2y = (int)(real_time_info->avg_ny * 3/4);
	}
    }

/*   if (real_time_info->histo_update == 1) return D_O_K;   // we are uptodate */


  for (i = 0; i < H_LEVELS; i++)
    {
      ds0->xd[i] = ds1->xd[i] = ds2->xd[i] = ds3->xd[i] = i;
      ds0->yd[i] = real_time_info->single_histo[ds0y][ds0x][i];
      ds1->yd[i] = real_time_info->single_histo[ds1y][ds1x][i];
      ds2->yd[i] = real_time_info->single_histo[ds2y][ds2x][i];
      ds3->yd[i] = real_time_info->single_histo[ds3y][ds3x][i];
    }

  op->need_to_refresh = 1; 
  set_plot_title(op, "\\stack{{Position}{ds0: %d %d    ds1: %d %d}{ds2: %d %d    ds3: %d %d}}",
		 ds0x, ds0y, ds1x, ds1y, ds2x, ds2y, ds3x, ds3y);
/*   real_time_info->histo_update = 1; */
  return refresh_plot(pr_LIVE_VIDEO, UNCHANGED);
}



int avg_along_y_idle_action(O_p *op, DIALOG *d)
{
  int x, y, i;
  d_s *ds;
  static int immax = -1;
  double nc = 0, dtmp;

  if (real_time_info == NULL) return D_O_K;

  i = real_time_info->c_i;
  if (real_time_info->imi[i] <= immax) return D_O_K;   // we are uptodate
  immax = real_time_info->imi[i];               

  ds = find_source_specific_ds_in_op(op,"Mean averaged value along the beam");
  if (ds == NULL) return D_O_K;	

  y = 0;
  for(x = 0; x < real_time_info->avg_nx; x++)
    {
      for (i = 0, mn[x] = 0, s2[x] = 0, nc = 0; i < H_LEVELS; i++)
	{
	  if ((i / H_FACTOR) >= real_time_info->black_level_avg)
	    {
	      mn[x] += ((i / H_FACTOR)-real_time_info->black_level_avg) * real_time_info->single_histo[y][x][i];
	      nc = nc + (double)real_time_info->single_histo[y][x][i];
	    }
	}
      if (nc > 0) mn[x] /= nc; 
      for (i = 0, s2[x] = 0; i < H_LEVELS; i++)
	{
	  dtmp = (i / H_FACTOR)-real_time_info->black_level_avg - mn[x];
	  dtmp *= dtmp;
	  dtmp *= real_time_info->single_histo[y][x][i];
	  s2[x] += dtmp; 
	}
      if (nc > 0) s2[x] /= nc; 
  
      ds->xd[x] = ds->ye[x] = x;
      ds->yd[x] = mn[x];
      ds->ye[x] = sqrt(s2[x]);
  }

  op->need_to_refresh = 1;  
  return refresh_plot(pr_LIVE_VIDEO, UNCHANGED);
}



int histo_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  register int i, j;
  d_s *dsx, *dsy;
  int itmp;
  float black_level = 0, wi;
  static int immax = -1;
/*   static float w = 2.3; */
  double mean = 0, sig2 = 0, bump = 0, lambda = 0, nc = 0, dtmp, nc0 = 0, lambda0 = 0;
  
  if (real_time_info == NULL) return D_O_K;

  i = real_time_info->c_i;
  if (real_time_info->imi[i] <= immax) return D_O_K;   // we are uptodate
  immax = real_time_info->imi[i];               

  dsx = find_source_specific_ds_in_op(op,"Bar Histogram all pixels");
  if (dsx == NULL) return D_O_K;	

  dsy = find_source_specific_ds_in_op(op,"Histogram all pixels");
  if (dsy == NULL) return D_O_K;	

  if (op->title) 
    {
      if (sscanf(op->title,"\\stack{{image %d  blacklevel %f width %f" ,&itmp,&black_level,&wi) == 3)
	{
	  real_time_info->black_level_avg = black_level;               
	  real_time_info->w_avg = wi;
	}
    }

  if (real_time_info->histo_update == 1) return D_O_K;   // we are uptodate

  for (i = 0, mean = 0, nc = 0; i < 4*H_LEVELS; i+=4)
    {
      j = i/4;
      dsx->xd[i] = dsx->xd[i+1] = (j - 0.4) / (float) H_FACTOR;
      dsx->xd[i+2] = dsx->xd[i+3] = (j + 0.4) / (float) H_FACTOR;
      dsy->xd[j] = j / (float) H_FACTOR;
      dsx->yd[i] = dsx->yd[i+3] = 0;
      dsx->yd[i+2] = dsx->yd[i+1] = real_time_info->histotmp[j];
      dsy->yd[j] = real_time_info->histotmp[j];
      if (dsy->xd[j] >= real_time_info->black_level_avg)
	{
	  mean += ((j / (float) H_FACTOR)-real_time_info->black_level_avg) * real_time_info->histotmp[j];
	  nc = nc + (double)real_time_info->histotmp[j];
	}
    }
  if (nc > 0) mean /= nc; 
  for (i = 0, sig2 = 0; i < H_LEVELS; i++)
    {
      dtmp = ((float) i / (float) H_FACTOR) - real_time_info->black_level_avg - mean;
      dtmp *= dtmp;
      dtmp *= real_time_info->histotmp[i];
      sig2 += dtmp; 
    }
  if (nc > 0) sig2 /= nc; 
  bump = sig2/mean;
  lambda = mean /bump;
  for (i = 0, nc0 = 0; i < H_LEVELS; i++)
    {
      if (((float) i / (float) H_FACTOR) >= real_time_info->black_level_avg)
	{
	  if ((((float) i / (float) H_FACTOR) - real_time_info->black_level_avg) < real_time_info->w_avg)   
	    nc0 = nc0 + (double)real_time_info->histotmp[i];
	}
    }
  if (nc0 > 0) lambda0 = log(nc/nc0); 
  op->need_to_refresh = 1; 
  set_plot_title(op, "\\stack{{image %d  blacklevel %1.2f width %1.2f}{Bump %g \\lambda = %g \\lambda_0 = %g}{nc %g nc0 %g}}" 
		 ,real_time_info->ac_i,real_time_info->black_level_avg,real_time_info->w_avg,bump,lambda,lambda0,nc,nc0);
  real_time_info->histo_update = 1;
  return refresh_plot(pr_LIVE_VIDEO, UNCHANGED);
}


int dsl_histo_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  register int i, j;
  int itmp;
  d_s *dsx, *dsy;
  float black_level = 0, wi;
  static int immax = -1;
  
  if (real_time_info == NULL) return D_O_K;

  i = real_time_info->c_i;
  if (real_time_info->imi[i] <= immax) return D_O_K;   // we are uptodate
  immax = real_time_info->imi[i];               

  dsx = find_source_specific_ds_in_op(op,"Bar Histogram of duration since last event");
  if (dsx == NULL) return D_O_K;	

  dsy = find_source_specific_ds_in_op(op,"Histogram of duration since last event");
  if (dsy == NULL) return D_O_K;

  if (op->title)
    {
      if (sscanf(op->title,"image %d blacklevel %f width %f" ,&itmp,&black_level,&wi) == 3)
	{
	  real_time_info->black_level_bp = black_level;
	  real_time_info->w_bp = wi;
	}
    }

  if (real_time_info->histo_dsl_update == 1) return D_O_K;   // we are uptodate

  for (i = 0; i < 4*BP_DSL_LEVELS; i+=4)
    {
      j = i/4;
      dsx->xd[i] = dsx->xd[i+1] = (j - 0.4) / (float) BP_DSL_FACTOR;
      dsx->xd[i+2] = dsx->xd[i+3] = (j + 0.4) / (float) BP_DSL_FACTOR;
      dsy->xd[j] = j / (float) BP_DSL_FACTOR;
      dsx->yd[i] = dsx->yd[i+3] = 0;
      dsx->yd[i+2] = dsx->yd[i+1] = real_time_info->histo_dsl_tmp[j];
      dsy->yd[j] = real_time_info->histo_dsl_tmp[j];
    }

  op->need_to_refresh = 1; 
  set_plot_title(op, "image %d  blacklevel %1.2f width %1.2f"
		 ,real_time_info->ac_i,real_time_info->black_level_bp,real_time_info->w_bp);
  real_time_info->histo_dsl_update = 1;
  return refresh_plot(pr_LIVE_VIDEO, UNCHANGED);
}


int bp_histo_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  register int i, j;
  d_s *dsx, *dsy;
  int itmp;
  float black_level = 0, wi;
  static int immax = -1;
  double mean = 0, sig2 = 0, bump = 0, lambda = 0, nc = 0, dtmp, nc0 = 0, lambda0 = 0;
  
  if (real_time_info == NULL) return D_O_K;

  i = real_time_info->c_i;
  if (real_time_info->imi[i] <= immax) return D_O_K;   // we are uptodate
  immax = real_time_info->imi[i];               

  dsx = find_source_specific_ds_in_op(op,"Bar Histogram all band-passed pixels");
  if (dsx == NULL) return D_O_K;	

  dsy = find_source_specific_ds_in_op(op,"Histogram all band-passed pixels");
  if (dsy == NULL) return D_O_K;	

  if (op->title)
    {
      if (sscanf(op->title,"\\stack{{image %d  blacklevel %f width %f" ,&itmp,&black_level,&wi) == 3)
	{
	  real_time_info->black_level_bp = black_level;
	  real_time_info->w_bp = wi;
	}
    }

  if (real_time_info->histo_bp_update == 1) return D_O_K;   // we are uptodate

  for (i = 0, mean = 0, nc = 0; i < 4*BP_LEVELS; i+=4)
    {
      j = i/4;
      dsx->xd[i] = dsx->xd[i+1] = (j - 0.4) / (float) BP_FACTOR;
      dsx->xd[i+2] = dsx->xd[i+3] = (j + 0.4) / (float) BP_FACTOR;
      dsy->xd[j] = j / (float) BP_FACTOR;
      dsx->yd[i] = dsx->yd[i+3] = 0;
      dsx->yd[i+2] = dsx->yd[i+1] = real_time_info->histo_bp_tmp[j];
      dsy->yd[j] = real_time_info->histo_bp_tmp[j];
      if (dsy->xd[j] >= real_time_info->black_level_bp)
	{
	  mean += j / (float) BP_FACTOR * real_time_info->histo_bp_tmp[j];
	  nc = nc + (double)real_time_info->histo_bp_tmp[j];
	}
    }
  if (nc > 0) mean /= nc; 

  for (i = 0, sig2 = 0; i < BP_LEVELS; i++)
    {
      dtmp = (float) i / (float) BP_FACTOR - mean;
      dtmp *= dtmp;
      dtmp *= real_time_info->histo_bp_tmp[i];
      sig2 += dtmp; 
    }
  if (nc > 0) sig2 /= nc; 
  bump = sig2/mean;
  lambda = mean / bump;

  for (i = 0, nc0 = 0; i < BP_LEVELS; i++)
    {
      if (((float) i / (float) BP_FACTOR) >= real_time_info->black_level_bp)
	{
	  if ((((float) i / (float) BP_FACTOR) - real_time_info->black_level_bp) < real_time_info->w_bp)
	    nc0 = nc0 + (double)real_time_info->histo_bp_tmp[i];
	}
    }
/*   nc0 = (double) real_time_info->histo_bp_tmp[0]; */

  if (nc0 > 0) lambda0 = log(nc/nc0);
  op->need_to_refresh = 1; 
  set_plot_title(op, "\\stack{{image %d  blacklevel %1.2f width %1.2f}{Bump %g \\lambda = %g \\lambda_0 = %g}{nc %g nc0 %g}}"
		 ,real_time_info->ac_i,real_time_info->black_level_bp,real_time_info->w_bp,bump,lambda,lambda0,nc,nc0);
  real_time_info->histo_bp_update = 1;
  return refresh_plot(pr_LIVE_VIDEO, UNCHANGED);
}


int time_lapse_idle_action(O_p *op, DIALOG *d)
{
  if (real_time_info == NULL) return D_O_K;

  op->need_to_refresh = 1;	      

  return refresh_plot(pr_LIVE_VIDEO, UNCHANGED);
}



// Menu functions

int change_averaging_area_place(void)
{
  int xc, yc, i;

  if(updating_menu_state != 0)	return D_O_K;
  if (real_time_info == NULL)
    return win_printf_OK("data structure not ready!");
  xc = real_time_info->xc;
  yc = real_time_info->yc;
  i = win_scanf("Define enter of averaging area\nxc %10d yd %10d\n",&xc,&yc);
  if (i == CANCEL) return D_O_K;

  if (yc + (real_time_info->avg_ny * real_time_info->avg_bin)/2 > oi_LIVE_VIDEO->im.ny)
    yc = oi_LIVE_VIDEO->im.ny - (real_time_info->avg_ny * real_time_info->avg_bin)/2;
  if (xc + (real_time_info->avg_nx * real_time_info->avg_bin)/2 > oi_LIVE_VIDEO->im.nx)
    xc = oi_LIVE_VIDEO->im.nx - (real_time_info->avg_nx * real_time_info->avg_bin)/2;

  if (yc - (real_time_info->avg_ny * real_time_info->avg_bin)/2 < 0)
    yc = (real_time_info->avg_ny * real_time_info->avg_bin)/2;
  if (xc - (real_time_info->avg_nx * real_time_info->avg_bin)/2 < 0)
    xc = (real_time_info->avg_nx * real_time_info->avg_bin)/2;

  real_time_info->xc = xc;
  real_time_info->yc = yc;

  // should be a function (also in init)
  real_time_info->x0 = (real_time_info->rot==0) ? 0 : (real_time_info->xc - real_time_info->fwidth/2);
  real_time_info->y0 = (real_time_info->rot==0) ? (real_time_info->yc - real_time_info->fwidth/2): 0;
  real_time_info->wx = (real_time_info->rot==0) ? oi_LIVE_VIDEO->im.nx : real_time_info->fwidth;
  real_time_info->wy = (real_time_info->rot==0) ? real_time_info->fwidth : oi_LIVE_VIDEO->im.ny;


  return D_O_K;
}

int zero_hist(void)
{
  if(updating_menu_state != 0)
    {
      add_keyboard_short_cut(0, KEY_R, 0, zero_hist);
      return D_O_K;
    }
  if (real_time_info == NULL)
    return win_printf_OK("data structure not ready!");

  real_time_info->reset_histo = 1;
  real_time_info->reset_histo_bp = 1;
  real_time_info->reset_histo_dsl = 1;

  return D_O_K;
}

//commands to operate on the averaged movies////////////////////////


int oi_std_idle_action(O_i *oi, DIALOG *d)
{
  if (d == NULL || d->dp == NULL || d->proc == NULL)    return 1;
  if (oi->need_to_refresh)      d->proc(MSG_DRAW,d,0);	
  return 0;
}

int oi_hbp_idle_action(O_i *oi, DIALOG *d)
{
  if (d == NULL || d->dp == NULL || d->proc == NULL)    return 1;
  if (oi->need_to_refresh)
    {
      d->proc(MSG_DRAW,d,0);
      //find_zmin_zmax(oi);
      refresh_image(imr_LIVE_VIDEO, UNCHANGED);
    }
  return 0;
}

int oi_hpk_idle_action(O_i *oi, DIALOG *d)
{
  int i, op_nb;
  O_p *op;

  if (d == NULL || d->dp == NULL || d->proc == NULL)    return 1;
  if (oi->need_to_refresh)
    {
      d->proc(MSG_DRAW,d,0);
      //find_zmin_zmax(oi);

      if ((op_nb = find_op_nb_of_source_specific_ds_in_oi(oi, "Mean intensity profile")) != -1)
	{
	  op = oi->o_p[op_nb];
	  for (i = 0; i < oi_LIVE_VIDEO->im.ny; i ++)
	    {
	      op->dat[0]->xd[i] = (float) real_time_info->histo_beam_avg[i]/real_time_info->rc_i;
	    }
	  oi->need_to_refresh |= PLOT_NEED_REFRESH;
	}
      refresh_image(imr_LIVE_VIDEO, UNCHANGED);
    }
  return 0;
}

//////

// this running in the timer thread

#ifndef _FLUOBEAD_H_
int 	copy_one_differential_subset_of_movie(O_i *dest, O_i *ois, int x_start, int y_start, int delay)
{
  union pix *pd, *ps, *psd; 
  int i, j, onx, ony, donx, dony;

  if (ois == NULL || dest == NULL)                       return 1;
  if (ois->im.data_type != IS_CHAR_IMAGE)           return 2;
  if (dest->im.data_type != IS_INT_IMAGE)           return 2;
  pd = dest->im.pixel;
  ps = ois->im.pixel;
  i = ois->im.c_f - delay;   
  for (;i < 0; i += ois->im.n_f);  // rolling buffer
  psd = ois->im.pxl[i];
  onx = ois->im.nx; 
  ony = ois->im.ny;
  donx = dest->im.nx; 
  dony = dest->im.ny;
  if (x_start < 0 || y_start < 0)                        return 3;                     
  if ((x_start + donx > onx) || (y_start + dony > ony))  return 4;                     

  if (ois->im.data_type == IS_CHAR_IMAGE)
    {
      for (i = 0; i< dony ; i++)
	{
	  for (j = 0; j < donx ; j++)
	      pd[i].in[j] = ps[i+y_start].ch[j+x_start] - psd[i+y_start].ch[j+x_start];
	}
    }

  return 0;
}
#endif



int do_at_all_image(imreg *imr, O_i *oi, int n,  DIALOG *d, long long t, unsigned long dt, void *p, int n_inarow) 
{
  register int k, j, l;
  int ci, i, im, ci_1, ac_i, mc_i;
  BITMAP *imb = NULL;
  g_real_time *gt = NULL;
  int *pi_avg;
  int xc, yc, itmp, ytmp;
  int x, y, size;
  int xb, yb, wb, hb, im_i;
  unsigned char *pc;
  static unsigned long t_c, to = 0;
  static unsigned long t_last_im[BP_LEVELS/BP_FACTOR];
  long long lltmp;
  float tmp;
  double nc, nc0;
  O_p *op;
  d_s *dsx, *dsy;
  O_i *oia;
  O_i *oim, *oif, *oifb, *oid, *oic, *oihbp, *oihpk;
  static O_i *apo=NULL, *desapo=NULL;
  int xmin, xmax, ymin, ymax;
   
  if (apo == NULL || desapo == NULL)
    {
      apo = create_appodisation_float_image(real_time_info->fsize, real_time_info->fsize, 0.05, 0);
      if (apo == NULL)	return win_printf_OK("Could not create apodisation image!");
      
      desapo = create_appodisation_float_image(real_time_info->fsize, real_time_info->fsize, 0.05, 1);
      if (desapo == NULL)	return win_printf_OK("Could not create apodisation image!");
    }

  gt = (g_real_time*)p;
  ac_i = real_time_info->ac_i;
  ac_i++;
  real_time_info->rc_i++;
  ci_1 = ci = real_time_info->c_i;
  ci++;
  ci = (ci < real_time_info->n_i) ? ci : 0;
  real_time_info->imi[ci] = n;                                 // we save image" number
  real_time_info->imit[ci] = n_inarow;                                 
  real_time_info->imt[ci] = t;                                 // image time
  im_i = oi->im.c_f;
  if (real_time_info->ac_i == 0)  real_time_info->imt0 = real_time_info->imt[ci]; 
  //real_time_info->delta[ci] = ac_i * real_time_info->per_t/4;   


////image treatment/////

  oia = real_time_info->oi_avg;
  mc_i = real_time_info->mc_i;
  mc_i++;
  if (mc_i >= real_time_info->avg_nf) mc_i = 0;
  
  if (oi->bmp.stuff != NULL) imb = (BITMAP*)oi->bmp.stuff;
  // we grab the video bitmap of the IFC image
  prepare_image_overlay(oi);


  //i_avg1 = (oia->im.c_f == 0) ? 1 : 0;

/// Find intensity peaks ///
  find_ecoli_in_cur_frame(oi, real_time_info->min_peak_thres, 0, 0);

//// Raw histo ////
  // substract first img histo in raw_histo (curr index)
  for (i = 0; i < 256; i++)
    {
      real_time_info->raw_histo[i] -= real_time_info->raw_single_histo[oi->im.c_f][i];
      real_time_info->raw_single_histo[oi->im.c_f][i] = 0;
    }
  // calculate curr histo
  for (i = 0; i < oi->im.nx; i++)
    {
      for (j = real_time_info->raw_min; j < real_time_info->raw_min+real_time_info->raw_wi; j++)
	//for (j = 0; j < oi->im.ny; j++)
	{
	  itmp = (int) oi->im.pixel[j].ch[i];
	  real_time_info->raw_single_histo[oi->im.c_f][0xff&itmp] += 1;
	}
    }
  // add curr img in raw_histo
  for (i = 0; i < 256; i++)
    real_time_info->raw_histo[i] += real_time_info->raw_single_histo[oi->im.c_f][i];


  if (real_time_info->raw_histo_update)
    {
      for (i = 0; i < 256; i++) 
	real_time_info->raw_histotmp[i] = real_time_info->raw_histo[i];
      real_time_info->raw_histo_update = 0;
    }

//// Reset histo ////
  oihbp = real_time_info->oihbp;
  oihpk = real_time_info->oihpk;
  if (real_time_info->reset_histo)
    {
      real_time_info->reset_histo = 0;
      real_time_info->rc_i = 0;
      for (i = 0; i < H_LEVELS; i++)
	real_time_info->histo[i] = 0;
      for (k = 0; k < real_time_info->avg_ny; k++)
	for (j = 0; j < real_time_info->avg_nx; j++)
	  for (i = 0; i < H_LEVELS; i++)
	    {
	      real_time_info->single_histo[k][j][i] = 0;	  
	    }

      // reset histo of peaks counts vs y
      for (i = 0; i < oihpk->im.nx; i++)
	{
	  for (j = 0; j < oihpk->im.ny; j++)
	    {
	      oihpk->im.pixel[j].db[i] = 0;
	    }
	}
      // reset histo of beam intensity profile
      for (i = 0; i < oi_LIVE_VIDEO->im.ny; i ++)
	{
	  real_time_info->histo_beam_avg[i] = 0;
	}

      // reset 2D histo of events vs y
      for (i = 0; i < oihbp->im.nx; i++)
	{
	  for (j = 0; j < oihbp->im.ny; j++)
	    {
	      oihbp->im.pixel[j].li[i] = 0;
	    }
	}
    }

//// Average beam profile  ////
  for (j = 0; j < oi_LIVE_VIDEO->im.ny; j++)
    {
      itmp = 0;
      for (i = 0; i < oi_LIVE_VIDEO->im.nx; i++)
	{
	  itmp += oi_LIVE_VIDEO->im.pixel[j].ch[i];
	}
      real_time_info->histo_beam_avg[j] += (unsigned long) itmp;
    }

//// Peaks counts vs y histo ////
  for (t = real_time_info->min_peak_thres; t < 255; t += real_time_info->inc_peak_thres)
    {
      for (l = 0; l < oi->im.n_sl[oi->im.c_f]; l++)
	{
	  if ((int) oi->im.s_l[oi->im.c_f][l]->user_val > t)
	    {
	      ytmp = (int) oi->im.s_l[oi->im.c_f][l]->yla;
	      if ((int) oi->im.s_l[oi->im.c_f][l]->user_val < 254) // no saturation
		{
		  itmp = (t - real_time_info->min_peak_thres) / real_time_info->inc_peak_thres;
		  oihpk->im.pixel[ytmp].db[itmp] += 1;
		}
	      else // saturation (store in last column)
		{
		  oihpk->im.pixel[ytmp].db[oihpk->im.nx-1] += 1;
		}
	    }
	}
    }
  oihpk->need_to_refresh = BITMAP_NEED_REFRESH;

//// Averaged image histo ////
  for (k = 0; k < real_time_info->avg_ny; k++)
    {
      yc = real_time_info->yc - (real_time_info->avg_ny * real_time_info->avg_bin)/2;
      yc += real_time_info->avg_bin * k;
      pi_avg = oia->im.pxl[mc_i][k].li;
      for (l = 0; l < real_time_info->avg_nx; l++)
	{
	  xc = real_time_info->xc - (real_time_info->avg_nx * real_time_info->avg_bin)/2;
	  xc += real_time_info->avg_bin * l;
	  for (i = yc, pi_avg[l] = 0; i < (yc + real_time_info->avg_bin); i++)
	    {
	      pc = oi->im.pixel[i].ch;
	      for (j = xc; j < xc + real_time_info->avg_bin; j++)
		pi_avg[l] += pc[j];
	    }
	  tmp = real_time_info->avg_bin * real_time_info->avg_bin;
	  tmp = ((float)pi_avg[l])/tmp*H_FACTOR;
	  itmp = (int)(tmp+0.5);
	  itmp = (itmp < 0) ? 0 : itmp;
	  itmp = (itmp > H_LEVELS-1) ? H_LEVELS-1 : itmp;
	  real_time_info->single_histo[k][l][itmp]++;
	  real_time_info->histo[itmp]++;
	  if (k == real_time_info->y_pxl && l == real_time_info->x_pxl)
	    real_time_info->avg_pxl[ci] = tmp;
        }
    }
  if (real_time_info->histo_update)
    {
      for (i = 0; i < H_LEVELS; i++) 
	real_time_info->histotmp[i] = real_time_info->histo[i];
      real_time_info->histo_update = 0;
    }


//// Fourier treatment ////
  if (real_time_info->ac_i > real_time_info->delay)
    {
      t_c = my_uclock();

      oim = real_time_info->oim;
      oif = real_time_info->oif;
      oifb = real_time_info->oifb;
      oid = real_time_info->oid;
      size = real_time_info->fsize;
      for (j = 0; j < real_time_info->f_nf; j++)
	{
	  switch_frame(oim,j);
	  switch_frame(oif,j);
	  find_min_subset_pos_ROI(oi, size, size, j, &x, &y, real_time_info->x0, real_time_info->y0, real_time_info->wx, real_time_info->wy);
#ifndef DELAY	  
	  copy_one_subset_of_image(oim, oi, x, y);
#else
	  copy_one_differential_subset_of_movie(oim, oi, x, y, real_time_info->delay);
#endif
	  //win_printf_OK("small image %d\nx %d y %d\n",j,x,y);
	}

      forward_fftbt_2d_im_appo(oif, oim, 0, screen, apo, 0);
      //my_set_window_title("Treating image %d after fft",ims);
      band_pass_filter_fft_2d_im(oif, oif,  0, real_time_info->lp,  real_time_info->lw,  real_time_info->hp,  real_time_info->hw, 0, 0);
      //my_set_window_title("Treating image %d after band pass ",ims);
      backward_fftbt_2d_im_desappo(oifb, oif, 0, screen, NULL, 0);
      //my_set_window_title("Treating image %d after back",ims);
      for (im = 0; im < real_time_info->f_nf; im++)
	{
	  switch_frame(oifb, im);
	  find_min_subset_pos_ROI(oi, size, size, im, &x, &y, 0, 0, real_time_info->wx, real_time_info->wy);
	  find_min_subset_good_pixel_pos_ROI(oi, size, size, im, &xb, &yb, &wb, &hb, real_time_info->wx, real_time_info->wy);
	  for (i = 0; i < hb; i++)
	    {
	      for (j = 0; j < wb; j++)
		{
		  oid->im.pxl[im_i][yb+i].fl[xb+j] = oifb->im.pixel[yb-y+i].fl[xb-x+j];
		}
	    }
	}
      t_c = my_uclock() - t_c;
      if (to == 0) to = MY_UCLOCKS_PER_SEC;
      //my_set_window_title("FFT took %g ms",(double)(t_c*1000)/to);


      // 2D histo of events vs y
      for (i = 0; i < oid->im.nx; i++)
	{
	  for (j = 0; j < oid->im.ny; j++)
	    {
	      tmp = fabs(oid->im.pxl[im_i][j].fl[i]); // RMS is absolute value for one pixel
	      tmp *= (float) BP_FACTOR;
	      itmp = (tmp < (float)BP_LEVELS) ? (int) tmp : (int) BP_LEVELS-1;
	      oihbp->im.pixel[j].li[itmp] += 1;
	    }
	}
      oihbp->need_to_refresh = BITMAP_NEED_REFRESH;
      


      // Band-passed image histo
      if (real_time_info->reset_histo_bp)
	{
	  real_time_info->reset_histo_bp = 0;
	  for (i = 0; i < BP_LEVELS; i++)
	    real_time_info->histo_bp[i] = 0;
	}
      if (real_time_info->reset_histo_dsl)
	{
	  real_time_info->reset_histo_dsl = 0;
	  for (i = 0; i < BP_DSL_LEVELS; i++)
	    real_time_info->histo_dsl[i] = 0;
	}

      xmin = oifb->im.nx / 8;
      xmax = 7 * oifb->im.nx / 8; 
      ymin = oifb->im.ny / 8;
      ymax = 7 * oifb->im.ny / 8;
      oic = real_time_info->oic;

      for (k = 0; k < oifb->im.n_f; k++)
	{
	  // compute rms of center of frame
	  tmp = 0;
	  itmp = 0;
	  for (j = ymin; j < ymax; j++)
	    {
	      for (i = xmin; i < xmax; i++)
		{
		  tmp += oifb->im.pxl[k][j].fl[i] * oifb->im.pxl[k][j].fl[i];
		  itmp++;
		}
	    }
	  tmp /= (float) itmp;
	  tmp = sqrt(tmp) * (float) BP_FACTOR;
	  itmp = (tmp < (float)BP_LEVELS) ? (int) tmp : (int) BP_LEVELS-1;
      
	  real_time_info->histo_bp[itmp]++;


	  // Duration since last event histo
	  if (tmp/(float)BP_FACTOR > (real_time_info->black_level_bp + real_time_info->w_bp))
	    {
	      if (real_time_info->dur_since_last[k] == 0)
		{
		  real_time_info->dur_since_last[k] = my_ulclock();
		}
	      else
		{
		  lltmp = my_ulclock() - real_time_info->dur_since_last[k];
		  lltmp *= (long long) BP_DSL_FACTOR;
		  lltmp /= get_my_ulclocks_per_sec();
		  real_time_info->dur_since_last[k] = my_ulclock();
		  lltmp = (lltmp < (long long)BP_DSL_LEVELS) ? lltmp : (long long) BP_DSL_LEVELS-1;
		  real_time_info->histo_dsl[(int)lltmp]++;
		}
	    }

	  // Save image for checking
	  itmp /= BP_FACTOR;
	  itmp = (itmp < BP_LEVELS/BP_FACTOR) ? itmp : (BP_LEVELS/BP_FACTOR) - 1;
	  if (my_uclock() - t_last_im[itmp] > to)  // update only after 1 sec
	    {
	      t_last_im[itmp] = my_uclock();
	      for (i = 0; i< oic->im.nx ; i++)
		{
		  for (j = 0; j < oic->im.ny ; j++)
		    oic->im.pxl[itmp][j].fl[i] =  oifb->im.pxl[k][j].fl[i];
		}
	      if (oic->im.c_f == itmp) oic->need_to_refresh = ALL_NEED_REFRESH;
	    }
	}

      // Update tmp histo used in non-RT thread
      if (real_time_info->histo_bp_update)
	{
	  for (i = 0; i < BP_LEVELS; i++)
	    {
	      real_time_info->histo_bp_tmp[i] = real_time_info->histo_bp[i];
	      real_time_info->histo_bp_acc_tmp[i] = real_time_info->histo_bp_acc[i];
	    }
	  real_time_info->histo_bp_update = 0;
	}      
      if (real_time_info->histo_dsl_update)
	{
	  for (i = 0; i < BP_DSL_LEVELS; i++)
	    real_time_info->histo_dsl_tmp[i] = real_time_info->histo_dsl[i];
	  real_time_info->histo_dsl_update = 0;
	}      
      
      switch_frame(oid, im_i);
      oim->need_to_refresh = ALL_NEED_REFRESH;
      oif->need_to_refresh = ALL_NEED_REFRESH;
    }

  real_time_info->imdt[ci] = my_uclock() - dt;
  real_time_info->c_i = ci;
  real_time_info->ac_i = ac_i;
  real_time_info->mc_i = mc_i;
  switch_frame(oia,mc_i);
  oia->need_to_refresh |= ALL_NEED_REFRESH;	


//// Manage time lapse ////
  // time lapse init
  if (real_time_info->n_acq == 0 && real_time_info->t_start == 0)
    {
      real_time_info->t_start = my_ulclock();
      real_time_info->t_last = my_ulclock();
    }

  // Update time lapse
  tmp = (float)(my_ulclock() - real_time_info->t_last);
  tmp /= (float) get_my_ulclocks_per_sec();
  if (real_time_info->type == 1 && tmp > (float)real_time_info->period && (int)(real_time_info->duration * 60 / real_time_info->period) > real_time_info->n_acq)
    {
      op = real_time_info->op_tl;
      if (op == NULL) return D_O_K;
      dsx = find_source_specific_ds_in_op(op,"Time lapse of bp measurments");
      if (dsx == NULL) return D_O_K;	
      dsy = find_source_specific_ds_in_op(op,"Time lapse of accumulated bp measurments");
      if (dsy == NULL) return D_O_K;	
	  
/*       set_plot_title(op, "\\stack{{tstart %ll}{tlast %ll}{my ulclock %ll}{per sec %ll}{tmp %f  nacq %d}}", */
/* 		     real_time_info->t_start, real_time_info->t_last, my_ulclock(), get_my_ulclocks_per_sec(), tmp, real_time_info->n_acq); */
      // Update acc histo
      for (i = 0; i < BP_LEVELS; i++) 
	{
	  real_time_info->histo_bp_acc[i] = (int) (real_time_info->memory * real_time_info->histo_bp_acc[i]);
	  real_time_info->histo_bp_acc[i] += (int) ((1-real_time_info->memory) * real_time_info->histo_bp[i]);
	}

      // Save in ds
      dsx->xd[real_time_info->n_acq] = dsy->xd[real_time_info->n_acq] = (float) (my_ulclock() - real_time_info->t_start) / get_my_ulclocks_per_sec();

      for (i = 0, nc = 0, nc0 = 0; i < BP_LEVELS; i++)
	if (((float) i / (float) BP_FACTOR) >= real_time_info->black_level_bp)
	  {
	    nc = nc + (double)real_time_info->histo_bp_tmp[i];
	    if ((((float) i / (float) BP_FACTOR) - real_time_info->black_level_bp) < real_time_info->w_bp)
	      nc0 = nc0 + (double)real_time_info->histo_bp_tmp[i];
	  }
      if (nc0 > 0) dsx->yd[real_time_info->n_acq] = (float) log(nc/nc0);
      else dsx->yd[real_time_info->n_acq] = 0;
      dsx->nx += 1;
      
      for (i = 0, nc = 0, nc0 = 0; i < BP_LEVELS; i++)
	if (((float) i / (float) BP_FACTOR) >= real_time_info->black_level_bp)
	  {
	    nc = nc + (double)real_time_info->histo_bp_acc_tmp[i];
	    if ((((float) i / (float) BP_FACTOR) - real_time_info->black_level_bp) < real_time_info->w_bp)
	      nc0 = nc0 + (double) real_time_info->histo_bp_acc_tmp[i];
	  }
      if (nc0 > 0) dsy->yd[real_time_info->n_acq] = (float) log(nc/nc0);
      else dsy->yd[real_time_info->n_acq] = 0;
      dsy->nx += 1;

      op->need_to_refresh = 1;	      

      // Reset current histo
      for (i = 0; i < BP_LEVELS; i++) 
	real_time_info->histo_bp[i] = 0;

      real_time_info->t_last = my_ulclock();
      real_time_info->n_acq += 1;
    }
 
  /////////////////////////////////////

  i = find_remaining_action(n);
  real_time_info->status_flag[ci] = real_time_info->status_flag[ci_1];  
  real_time_info->obj_pos_cmd[ci] = real_time_info->obj_pos_cmd[ci_1];
  while ((i = find_next_action(n)) >= 0)
    {
      action_pending[i].proceeded = 1;
    }

   
  return 0; 
}
END_OF_FUNCTION(do_at_all_image)



int get_present_image_nb(void)
{
 return (real_time_info != NULL) ?  real_time_info->imi[real_time_info->c_i] : -1;
}

g_real_time *creating_real_time_info(int rot, int z0, int w0, int min_pt, int inc_pt, int asize, int nfa, int fsize, int fwidth, float lp, float lw, float hp, float hw, int type, int period, int duration, float memory)
{
  O_p *op;
  d_s *ds;
  O_i *oi;
  int i, j, itmp, nf, nfi;
  int nx, ny;
  int x0, y0, wx, wy;
  int cx, cy, nx0, ny0;
  
  if (real_time_info == NULL)
    {
      nx = (rot==0) ? (int)(oi_LIVE_VIDEO->im.nx / asize) : (w0 / asize);
      ny = (rot==0) ? (w0 / asize) : (int)(oi_LIVE_VIDEO->im.ny / asize);
      nfi = oi_LIVE_VIDEO->im.n_f;

      real_time_info = (g_real_time*)calloc(1,sizeof(g_real_time));
      if (real_time_info == NULL) return win_printf_ptr("cannot alloc real_time info!");	

      real_time_info->raw_single_histo = (int**)calloc(nfi, sizeof(int*));
      if (real_time_info->raw_single_histo == NULL) return win_printf_ptr("cannot alloc raw_single_histo table info!");
      for (i = 0; i < oi_LIVE_VIDEO->im.n_f; i++)
	{
	  real_time_info->raw_single_histo[i] = (int*) calloc(256, sizeof(int));
	  if (real_time_info->raw_single_histo[i] == NULL) return win_printf_ptr("cannot alloc raw_single_histo info!");
	}

      real_time_info->single_histo = (int***)calloc(ny, sizeof(int**));
      if (real_time_info->single_histo == NULL) return win_printf_ptr("cannot alloc single_histo table info!");
      for (j = 0; j < ny; j++)
	{
	  real_time_info->single_histo[j] = (int**)calloc(nx, sizeof(int*));
	  if (real_time_info->single_histo[j] == NULL) return win_printf_ptr("cannot alloc single_histo table (2) info!");
	  for (i = 0; i < nx; i++)
	    {
	      real_time_info->single_histo[j][i] = (int*)calloc(H_LEVELS, sizeof(int));
	      if (real_time_info->single_histo[j][i] == NULL) return win_printf_ptr("cannot alloc single_histo info!");
	    }
	}

      real_time_info->im_focus_check = 16;
      real_time_info->user_real_time_action = NULL;
      real_time_info->rc_i = 0;

      real_time_info->avg_nx = nx;
      real_time_info->avg_ny = ny;
      real_time_info->avg_nf = nfa; 
      real_time_info->min_peak_thres = min_pt; 
      real_time_info->inc_peak_thres = inc_pt; 
      real_time_info->avg_bin = asize;
      real_time_info->rot = rot;
      real_time_info->w0 = w0;
      real_time_info->xc = (rot==0) ? (oi_LIVE_VIDEO->im.nx/2) : z0;
      real_time_info->yc = (rot==0) ? z0 : (oi_LIVE_VIDEO->im.ny/2);
      
      x0 = real_time_info->x0 = (rot==0) ? 0 : (real_time_info->xc - fwidth/2);
      y0 = real_time_info->y0 = (rot==0) ? (real_time_info->yc - fwidth/2) : 0;
      wx = real_time_info->wx = (rot==0) ? oi_LIVE_VIDEO->im.nx : fwidth;
      wy = real_time_info->wy = (rot==0) ? fwidth : oi_LIVE_VIDEO->im.ny;
      real_time_info->fsize = fsize;
      real_time_info->fwidth = fwidth;
      real_time_info->lp = lp;
      real_time_info->lw = lw;
      real_time_info->hp = hp;

      real_time_info->w_avg = 2.3;
      real_time_info->w_bp = 0.45;

      real_time_info->type = type;
      real_time_info->period = period;
      real_time_info->duration = duration;
      real_time_info->memory = memory;
     
 
     // Allocate images
      if ((oi = create_and_attach_movie_to_imr(imr_LIVE_VIDEO, nx, ny, IS_LINT_IMAGE, nfa)) == NULL)
      	return win_printf_ptr("I can't create image !");
      oi->oi_idle_action = oi_std_idle_action;
      map_pixel_ratio_of_image_and_screen(oi, (float)0.5/asize, (float)0.5/asize);
      set_z_black_z_white_values(oi, 0, (float)255*nx*ny);
      set_zmin_zmax_values(oi, 0, (float)255*nx*ny);
      smooth_interpol = 0;
      real_time_info->oi_avg = oi;


      real_time_info->f_nf = nf = compute_nb_of_min_subset_ROI(oi_LIVE_VIDEO, fsize, fsize, x0, y0, wx, wy);
      // display nb of small images
      cx = 4*(wx - fsize);
      nx0 = cx/(3*fsize);
      nx0 += (cx%(3*fsize)) ? 2 : 1;
      cy = 4*(wy - fsize);
      ny0 = cy/(3*fsize);
      ny0 += (cy%(3*fsize)) ? 2 : 1;
      win_printf_OK("Nb of small images %d (%d x %d)", nf, nx0, ny0);

      // Alloc table late because several params are needed
      real_time_info->dur_since_last = (long long*) calloc(nf, sizeof(long long));
      if (real_time_info->dur_since_last == NULL)
	return win_printf_ptr("cannot alloc dur_since_last info!");
      for (i = 0; i < nf; i++)
	real_time_info->dur_since_last[i] = 0;


#ifndef DELAY	  
      if ((oi = create_and_attach_movie_to_imr(imr_LIVE_VIDEO, fsize, fsize, IS_CHAR_IMAGE, nf)) == NULL)
      	return win_printf_ptr("I can't create image !");
#else
      if ((oi = create_and_attach_movie_to_imr(imr_LIVE_VIDEO, fsize, fsize, IS_INT_IMAGE, nf)) == NULL)
      	return win_printf_ptr("I can't create image !");
#endif
      set_zmin_zmax_values(oi, 0, 255);
      oi->oi_idle_action = oi_std_idle_action;
      real_time_info->oim = oi;

      if ((oi = create_and_attach_movie_to_imr(imr_LIVE_VIDEO, fsize, fsize/2, IS_COMPLEX_IMAGE, nf)) == NULL)
      	return win_printf_ptr("I can't create image !");
      oi->height *= 0.5;
      set_zmin_zmax_values(oi, -10, 0);
      oi->oi_idle_action = oi_std_idle_action;
      real_time_info->oif = oi;

      if ((oi = create_and_attach_movie_to_imr(imr_LIVE_VIDEO, fsize, fsize, IS_FLOAT_IMAGE, nf)) == NULL)
      	return win_printf_ptr("I can't create image !");
      oi->oi_idle_action = oi_std_idle_action;
      set_z_black_z_white_values(oi, -10, 40);
      set_zmin_zmax_values(oi, -10, 40);
      real_time_info->oifb = oi;

      if ((oi = create_and_attach_movie_to_imr(imr_LIVE_VIDEO, fsize, fsize, IS_FLOAT_IMAGE, (int) (BP_LEVELS/BP_FACTOR))) == NULL)
      	return win_printf_ptr("I can't create image !");
      oi->oi_idle_action = oi_std_idle_action;
      set_z_black_z_white_values(oi, -10, 40);
      set_zmin_zmax_values(oi, -10, 40);
      real_time_info->oic = oi;

      if ((oi = create_and_attach_movie_to_imr(imr_LIVE_VIDEO, wx, wy, IS_FLOAT_IMAGE, nfi)) == NULL)
      	return win_printf_ptr("I can't create image !");
      oi->oi_idle_action = oi_std_idle_action;
      map_pixel_ratio_of_image_and_screen(oi, 1, 1);
      set_z_black_z_white_values(oi, -10, 40);
      set_zmin_zmax_values(oi, -10, 40);
      real_time_info->oid = oi;

      if ((oi = create_and_attach_oi_to_imr(imr_LIVE_VIDEO, (255-min_pt)/inc_pt+1, oi_LIVE_VIDEO->im.ny, IS_DOUBLE_IMAGE)) == NULL)
      	return win_printf_ptr("I can't create image !");
      oi->oi_idle_action = oi_hpk_idle_action;
      //set_z_black_z_white_values(oi, -10, 40);
      //set_zmin_zmax_values(oi, 0, 1e4);
      create_attach_select_x_un_to_oi(oi, IS_RAW_U, min_pt, inc_pt, 0, 0, "");
      real_time_info->oihpk = oi;
      
      if ((op = create_and_attach_op_to_oi(oi, oi_LIVE_VIDEO->im.ny, oi_LIVE_VIDEO->im.ny, 0, IM_SAME_Y_AXIS)) == NULL)
	return win_printf_ptr("I can't attach plot to image !");
      set_ds_source(op->dat[0], "Mean intensity profile");
      for (i = 0; i < oi_LIVE_VIDEO->im.ny; i ++)
	{
	  op->dat[0]->yd[i] = (float) i;
	}

      real_time_info->histo_beam_avg = (unsigned long*) calloc(oi_LIVE_VIDEO->im.ny, sizeof(unsigned long));
      if (real_time_info->histo_beam_avg == NULL)
	return win_printf_ptr("cannot alloc histo_beam_avg info!");
      for (i = 0; i < oi_LIVE_VIDEO->im.ny; i++)
	real_time_info->histo_beam_avg[i] = 0;
      

      if ((oi = create_and_attach_oi_to_imr(imr_LIVE_VIDEO, (int)BP_LEVELS, wy, IS_LINT_IMAGE)) == NULL)
      	return win_printf_ptr("I can't create image !");
      oi->oi_idle_action = oi_hbp_idle_action;
      map_pixel_ratio_of_image_and_screen(oi, 1, 1);
      //set_z_black_z_white_values(oi, -10, 40);
      set_zmin_zmax_values(oi, 0, 1000);
      real_time_info->oihbp = oi;


      // Allocate plots to check real_time
      op = pr_LIVE_VIDEO->o_p[0];
      set_op_filename(op, "Timing.gr");
      ds = op->dat[0]; // the first data set was already created
      set_ds_source(ds, "Timing rolling buffer");
      if ((ds = create_and_attach_one_ds(op, REAL_TIME_BUFFER_SIZE, REAL_TIME_BUFFER_SIZE, 0)) == NULL)
		return win_printf_ptr("I can't create plot !");
      set_ds_source(ds, "Period rolling buffer");
      if ((ds = create_and_attach_one_ds(op, REAL_TIME_BUFFER_SIZE, REAL_TIME_BUFFER_SIZE, 0)) == NULL)
		return win_printf_ptr("I can't create plot !");
      set_ds_source(ds, "Timer rolling buffer");
      op->op_idle_action = timing_rolling_buffer_idle_action;
      //      op->op_post_display = general_op_post_display;
      set_plot_x_title(op, "Time");
      set_plot_y_title(op, "{\\color{yellow} Real_Timing duration} {\\color{lightgreen} Frame duration} (ms)");



      if ((op = create_and_attach_one_plot(pr_LIVE_VIDEO, 1024, 1024, 0)) == NULL)
	return win_printf_ptr("I can't create plot !");
      set_op_filename(op, "raw-histo.gr");
      ds = op->dat[0]; // the first data set was already created
      set_ds_source(ds, "Raw bar histogram");

      if ((ds = create_and_attach_one_ds(op, 256, 256, 0)) == NULL)
		return win_printf_ptr("I can't create plot !");
      set_ds_source(ds, "Raw histogram");

      op->op_idle_action = raw_histo_rolling_buffer_idle_action;

      set_plot_x_title(op, "Signal level");
      set_plot_y_title(op, "Counts");			



      if ((op = create_and_attach_one_plot(pr_LIVE_VIDEO, nfa, nfa, 0)) == NULL)
	return win_printf_ptr("I can't create plot !");
      set_op_filename(op, "pxl-vs-time.gr");
      ds = op->dat[0]; // the first data set was already created
      set_ds_source(ds, "Averaged pixel value versus time");
      op->op_idle_action = avg_pxl_rolling_buffer_idle_action;

      set_plot_x_title(op, "Time");
      set_plot_y_title(op, "Level");			

      create_attach_select_x_un_to_op(op, IS_SECOND, 0
				       ,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");



     if ((op = create_and_attach_one_plot(pr_LIVE_VIDEO, nfa, nfa, 0)) == NULL)
	return win_printf_ptr("I can't create plot !");
      set_op_filename(op, "autocorr.gr");
      ds = op->dat[0]; // the first data set was already created
      set_ds_source(ds, "Averaged pixel correlation");
      op->op_idle_action = avg_pxl_correlation_rolling_buffer_idle_action;

      set_plot_x_title(op, "Time");
      set_plot_y_title(op, "Level");			

      create_attach_select_x_un_to_op(op, IS_SECOND, 0
				       ,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");


      
      if ((op = create_and_attach_one_plot(pr_LIVE_VIDEO, H_LEVELS, H_LEVELS, 0)) == NULL)
	return win_printf_ptr("I can't create plot !");
      set_op_filename(op, "single-histo.gr");
      ds = op->dat[0]; // the first data set was already created
      set_ds_source(ds, "Histogram pixel 0");

      if ((ds = create_and_attach_one_ds(op, H_LEVELS, H_LEVELS, 0)) == NULL)
		return win_printf_ptr("I can't create plot !");
      set_ds_source(ds, "Histogram pixel 1");
      if ((ds = create_and_attach_one_ds(op, H_LEVELS, H_LEVELS, 0)) == NULL)
		return win_printf_ptr("I can't create plot !");
      set_ds_source(ds, "Histogram pixel 2");
      if ((ds = create_and_attach_one_ds(op, H_LEVELS, H_LEVELS, 0)) == NULL)
		return win_printf_ptr("I can't create plot !");
      set_ds_source(ds, "Histogram pixel 3");

      op->op_idle_action = single_histo_rolling_buffer_idle_action;

      set_plot_x_title(op, "Signal level");
      set_plot_y_title(op, "Counts");			



      itmp = (nx > ny) ? nx : ny;
      if ((op = create_and_attach_one_plot(pr_LIVE_VIDEO, itmp, itmp, 0)) == NULL)
	return win_printf_ptr("I can't create plot !");
      set_op_filename(op, "avg-along-y.gr");
      ds = op->dat[0]; // the first data set was already created
      set_ds_source(ds, "Mean averaged value along the beam");
      alloc_data_set_y_error(ds);

      op->op_idle_action = avg_along_y_idle_action;

      set_plot_x_title(op, "Position");
      set_plot_y_title(op, "Signal level");			

      mn = (float*)calloc(itmp, sizeof(float));
      if (mn == NULL) return win_printf_ptr("cannot alloc means table!");	
      s2 = (float*)calloc(itmp, sizeof(float));
      if (s2 == NULL) return win_printf_ptr("cannot alloc sig2 table!");	



     if ((op = create_and_attach_one_plot(pr_LIVE_VIDEO, 4*H_LEVELS, 4*H_LEVELS, 0)) == NULL)
	return win_printf_ptr("I can't create plot !");
      set_op_filename(op, "avg-histo.gr");
      ds = op->dat[0]; // the first data set was already created
      set_ds_source(ds, "Bar Histogram all pixels");

      if ((ds = create_and_attach_one_ds(op, H_LEVELS, H_LEVELS, 0)) == NULL)
		return win_printf_ptr("I can't create plot !");
      set_ds_source(ds, "Histogram all pixels");

      op->op_idle_action = histo_rolling_buffer_idle_action;

      set_plot_y_log(op);
      set_plot_x_title(op, "Signal level");
      set_plot_y_title(op, "Counts");			


      
      if ((op = create_and_attach_one_plot(pr_LIVE_VIDEO, BP_DSL_LEVELS*4, BP_DSL_LEVELS*4, 0)) == NULL)
	return win_printf_ptr("I can't create plot !");
      set_op_filename(op, "bp-duration-histo.gr");
      ds = op->dat[0]; // the first data set was already created
      set_ds_source(ds, "Bar Histogram of duration since last event");

      if ((ds = create_and_attach_one_ds(op, BP_DSL_LEVELS, BP_DSL_LEVELS, 0)) == NULL)
		return win_printf_ptr("I can't create plot !");
      set_ds_source(ds, "Histogram of duration since last event");

      op->op_idle_action = dsl_histo_rolling_buffer_idle_action;

      set_plot_y_log(op);
      set_plot_x_title(op, "Duration");
      set_plot_y_title(op, "Counts");			



     if ((op = create_and_attach_one_plot(pr_LIVE_VIDEO, BP_LEVELS*4, BP_LEVELS*4, 0)) == NULL)
	return win_printf_ptr("I can't create plot !");
      set_op_filename(op, "bp-histo.gr");
      ds = op->dat[0]; // the first data set was already created
      set_ds_source(ds, "Bar Histogram all band-passed pixels");

      if ((ds = create_and_attach_one_ds(op, BP_LEVELS, BP_LEVELS, 0)) == NULL)
		return win_printf_ptr("I can't create plot !");
      set_ds_source(ds, "Histogram all band-passed pixels");
      //if (real_time_info->type == 1)
	{
	  if ((ds = create_and_attach_one_ds(op, BP_LEVELS, BP_LEVELS, 0)) == NULL)
	    return win_printf_ptr("I can't create plot !");
	  set_ds_source(ds, "Bar Histogram accumulated band-passed pixels");
	  if ((ds = create_and_attach_one_ds(op, BP_LEVELS, BP_LEVELS, 0)) == NULL)
	    return win_printf_ptr("I can't create plot !");
	  set_ds_source(ds, "Histogram accumulated band-passed pixels");
	}
      op->op_idle_action = bp_histo_rolling_buffer_idle_action;

      set_plot_y_log(op);
      set_plot_x_title(op, "Signal level");
      set_plot_y_title(op, "Counts");			



      if (real_time_info->type == 1)
	{
	  itmp = (duration*60/period) + 1;
	  if ((op = create_and_attach_one_plot(pr_LIVE_VIDEO, itmp, itmp, 0)) == NULL)
	    return win_printf_ptr("I can't create plot !");
	  set_op_filename(op, "bp-timelapse.gr");
	  ds = op->dat[0]; // the first data set was already created
	  set_ds_source(ds, "Time lapse of bp measurments");
	  ds->nx = 0;

	  if ((ds = create_and_attach_one_ds(op, BP_LEVELS, BP_LEVELS, 0)) == NULL)
	    return win_printf_ptr("I can't create plot !");
	  set_ds_source(ds, "Time lapse of accumulated bp measurments");
	  ds->nx = 0;

	  op->op_idle_action = time_lapse_idle_action;

	  set_plot_y_log(op);
	  set_plot_x_title(op, "Time");
	  set_plot_y_title(op, "Signal level");			

	  real_time_info->op_tl = op;
	}


      real_time_info->delay = 10;

      real_time_info->t_start = 0;
      real_time_info->t_last = 0;
      real_time_info->n_acq = 0;

      real_time_info->raw_min = 100;
      real_time_info->raw_wi = 240;

      real_time_info->m_i = real_time_info->n_i = REAL_TIME_BUFFER_SIZE;
      real_time_info->c_i = real_time_info->ac_i = 0;
      real_time_info->mc_i = 0;
      LOCK_FUNCTION(do_at_all_image);
      LOCK_VARIABLE(real_time_info);

      bid.param = (void*)real_time_info;
      bid.to_do = NULL;
      bid.timer_do = do_at_all_image;

      //add_image_treat_menu_item ( "Avg", NULL, phase_in_live_video_image_menu(), 0, NULL);
    }
  return real_time_info;
}


int init_live_video_info(void)
{
  int i, rot=0, z0=240, w0=24, min_pt=15, inc_pt=10, asize=20, nf=1024, fsize=16, fwidth = 40, type = 0, period = 200, duration = 500;
  float lp = 3, lw = 1, hp = 6, hw = 2, memory = 0.8;

  if (real_time_info == NULL) 
    {
      do_set_camera_exposure();
      set_zmin_zmax_values(oi_LIVE_VIDEO, 0, 25);
      oi_LIVE_VIDEO->need_to_refresh = ALL_NEED_REFRESH;

      i = win_scanf("For real time treatment\nh/v (h:0 v:1) %10d\nbeam heigth %10d beam width %10d\n\n"
		    "Peak threshold  minimum %12d increment %12d\n"
		    "Average movie\nbin size %12d\nnumber of frames %12d\n\n"
		    "Fourier movie\nbin size %12d width %12d\n"
		    "Band-pass filter cutoff lowpass  %10flowpass width %10f\n"
		    "cutoff highpass  %10fhighpass width %10f"
		    ,&rot,&z0,&w0,&min_pt,&inc_pt,&asize,&nf,&fsize,&fwidth, &lp,&lw,&hp,&hw);
      if (i == CANCEL) return 0;

      if (fsize > fwidth) return win_printf_OK("Fourier treatment width (bin * width) must be larger than beam width");
      
      i = win_scanf("Type of measure:\n"
		    "%R Point measurement\n%r Time-lapse"
		    , &type);
      if (i == CANCEL) return 0;

      if (type != 0) // case of time lapse
	{
	  i = win_scanf("Acquisition period (in sec) %5d\n"
			"Acquisition duration (in min) %5d\n"
			"Factor of temporal memory %5f"
			, &period, &duration, &memory);
	  if (i == CANCEL) return 0;
	  
	  if ((memory > 1) || (memory < 0))
	    {
	      win_printf("memory (%2.2f) must be between 0 and 1", memory);
	      return 0;
	    }
	  //win_printf("Characteristic time of measurment is %2.2f", memory);
	}

      real_time_info = creating_real_time_info(rot,z0,w0,min_pt,inc_pt,asize,nf,fsize, fwidth,lp,lw,hp,hw,type,period,duration,memory);
      
      m_action = 256;
      n_action = 0;
      action_pending = (f_action*)calloc(m_action,sizeof(f_action));
      if (action_pending == NULL)
	     win_printf_OK("Could not allocate action_pending!");
    }
  return 0;
}


MENU *real_time_avg_menu(void)
{
  static MENU mn[32];
  extern int save_finite_movie(void);
  
  
  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"New averaging position", change_averaging_area_place,NULL,0,NULL);
  add_item_to_menu(mn,"Reset histo", zero_hist,NULL,0,NULL);


  return mn;
}



#endif
