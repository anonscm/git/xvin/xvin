#ifndef _SDI_2_UTILS_H_
#define _SDI_2_UTILS_H_


#include "allegro.h"
#include "xvin.h"
#include "xv_main.h"
#include "fftl32n.h"
#include "trackBead.h"
#include "track_util.h"
#include "../fft2d/fftbtl32n.h"
#include "../cfg_file/Pico_cfg.h"



XV_FUNC(int,SDI_2_fit_position_from_buffer,(b_track *bt,sdi_fr *f1,double *pos_x, float* quality));
XV_FUNC(int,SDI_2_fill_amplitude_phase,(float *xi,int filter_low,fft_plan *fp,int filter_high,fdcmp* fpb, int lx, float *xfourier, float *bandpassed));
XV_FUNC(int, SDI_2_correct_phase,(fdcmp* fbp1, fdcmp *fbp2, int nb_of_modes, int low_mode, int fixed_mode));
XV_FUNC(int,SDI_2_correct_phase_N,(fdcmp* fbp0, int nb_of_modes, int low_mode, int fixed_mode));
XV_FUNC(int, SDI_2_initiate_matrix,(b_track *bt,sdi_fr *f1));
XV_FUNC(int, SDI_2_real_max_pos,(float *x ,int nx, int bp_low, int bp_high, fft_plan *fp,double *Max_pos));
XV_FUNC(int,SDI_2_find_average_profile,(b_track *bt,sdi_fr *f1));
XV_FUNC(int,SDI_2_fill_phase_only,(float *xi,int filter_low, fft_plan *fp,int filter_high,float *phasefit,int lx, float *fourier, float *bandpassed));
XV_FUNC(int,SDI_2_correct_and_find_phase,(b_track *bt,float push,float *phase0, float *phase0_er,float *phase1, int nb_of_modes, int low_mode, double *fit_coeffs, d_s *fit_ds,double *pos_x, float *quality));
XV_FUNC(int, SDI_2_find_centroid,(float *yi, int ly, float *cen, float *er));
XV_FUNC(int, SDI_2_find_yslope,(O_i *oic,int xco,int xend,int yco,int yend,float *slope,float *ymax));
XV_FUNC(int, SDI_2_find_y,(O_i *oic,int xco,int xend,int yco,int yend,float slope,float *ymax));
XV_FUNC(int, SDI_2_find_x_from_centro,(O_i *oic,int xco,int xend,int yco,int yend,float slope,double *xmax));

XV_FUNC(int, SDI_2_minimize_distance_global,(b_track *bt,sdi_fr *f1, double *pos_x));
XV_FUNC(int, SDI_2_minimize_distance_local,(b_track *bt,sdi_fr *f1, double *pos_x, double *score,double a, double b));

XV_FUNC(double, SDI_2_func_min,(double x, int cl2, float sig, float om));

struct fun_params { b_track *bt; sdi_fr *f1;};











#endif
