#ifndef _TRACKBEAD_H_
#define _TRACKBEAD_H_

#ifndef XV_WIN32
# include "config.h"
#endif

#include "gui.h"

typedef struct before_image_display
{
  int (*to_do)(imreg *imr, O_i *oi, int n, DIALOG *d, long long t, unsigned long dt, void *p);
  void *param;
  int previous_fr_nb;
  unsigned long timer_dt;
  int previous_in_time_fr_nb;
  int first_im;                     // the nb of the first image
  long long previous_in_time_fr;
  int lost_fr;
  struct before_image_display *next;
  int (*timer_do)(imreg *imr, O_i *oi, int n, DIALOG *d, long long t, unsigned long dt, void *p, int n_inarow);
  double image_per_in_ms;
  double min_delay_camera_thread_in_ms;
} Bid;



typedef struct thread_image_data
{
  imreg *imr;
  pltreg *pr;
  O_p *op;
  O_i *oi;
  DIALOG *dpr, *dimr;
  Bid *dbid;
} tid;


# define TRACK_FREEZE   2
# define TRACK_ON       1
# define TRACK_STOP     0

// job type

# define GRAB_CALIBRATION_IMAGE     8
# define COLLECT_DATA   16
# define RECOUNT_BEAD  32
# define COUNT_BEAD_SCANZ  64
# define GRAB_SDI_RAMP_MOVIE 128
# define SDI_SIGNAL_ANALYSIS 256

// this is lower priority routine done in the general_idle_action
// typically grabbing the calibration image, force measurements etc
typedef struct future_job
{
  int imi;             // the image nb where to start action, if < 0 the next possible frame
  int last_imi;        // an internal index used to avoid calling several time this job for the same image
  int in_progress;    // 0 if finished, anything else if active
  int type;
  int bead_nb;
  int fixed_bead_nb;
  int local;          // local int variable
  float f_local[8];      // local variables
  imreg *imr;
  O_i *oi;
  pltreg *pr;
  O_p *op;
  void *more_data ;                       // data structure for function
  int (*job_to_do)(int im, struct future_job *job);
} f_job;




#define SET_CL_128_CW_16                0
#define IMAGE_CAL			1
#define CHOOSE_SIZE                     2
#define GENERIC_IMAGE_CAL	        256
#define POSITION_SET_BY_MOUSE		512
#define MULTIPLE_BEADS                  1024

#define MAX_RADIAL_PROFILE_SIZE        256


#define DEF_CROSS_SIZE			2


# define PROG_GAME 0
# define PROG_GIGE 1
# define PROG_CVB 2

# define RECOUNT 1


# ifndef _TRACKBEAD_C_
//extern  __declspec(dllexport) int go_track ;             // flag on means tracking

PXV_VAR(int, go_track);
PXV_VAR(tid, dtid);
PXV_VAR(int, do_refresh_overlay);
PXV_VAR(Bid, bid);
PXV_VAR(imreg  *,imr_TRACK);
PXV_VAR(pltreg  *,pr_TRACK);
PXV_VAR(O_i  *, oi_TRACK);
PXV_VAR(DIALOG *, d_TRACK);
PXV_VAR(int, source_running);
PXV_VAR(char , running_message[]);
PXV_VAR(unsigned long, dt_simul);

PXV_VAR(f_job*, job_pending);
PXV_VAR(int, m_job);
PXV_VAR(int, n_job);
PXV_VAR(int, mouse_selected_bead);

PXV_VAR(int, last_im_who_asked_menu_update);  // switch in the tracking thread
PXV_VAR(int, last_im_where_menu_was_updated); // done in the general idle action
PXV_VAR(int, imr_and_pr);  // image and plot display together
PXV_VAR(int, imr_TRACK_redraw_background);
PXV_VAR(int, imr_TRACK_title_change_asked);
PXV_FUNCPTR(char*, generate_imr_TRACK_title, (void));
PXV_VAR(char*, current_write_open_picofile);
PXV_VAR(char*, current_write_open_path_picofile);
PXV_VAR(int, interfaces_working);
PXV_ARRAY(MENU, quicklink_menu);

PXV_FUNCPTR(int, get_camera_shutter_in_us, (int *set, int *smin, int *smax));
PXV_FUNCPTR(int, set_camera_shutter_in_us, (int exp, int *set, int *smin, int *smax));
PXV_FUNCPTR(int, get_camera_gain, (float *gain, float *fmin, float *fmax, float *finc));
PXV_FUNCPTR(int, set_camera_gain, (float gain, float *set, float *fmin, float *fmax, float *finc));

PXV_FUNCPTR(MENU*, specific_camera_image_menu, (void));

# endif
# ifdef _TRACKBEAD_C_

int interfaces_working = 0;
char* 	(*generate_imr_TRACK_title)(void);
int imr_TRACK_title_change_asked = 0;
int imr_TRACK_title_displayed = 0;

int go_track = 0;             // flag on means tracking
tid dtid;                     // the data passed to the thread proc
int do_refresh_overlay = 0;
Bid bid={NULL,NULL,0,0,0,0,0,0,NULL,NULL,0.0,0.0};

imreg  *imr_TRACK = NULL;
pltreg  *pr_TRACK = NULL;
O_i  *oi_TRACK = NULL;
DIALOG *d_TRACK = NULL;
int source_running = 0;
char running_message[128];
unsigned long dt_simul = 0;

int imr_and_pr = 0;

f_job *job_pending = NULL;
int m_job = 0;
int n_job = 0;
int last_im_who_asked_menu_update = 0;  // switch in the tracking thread
int last_im_where_menu_was_updated = 0; // done in the general idle action


int imr_TRACK_redraw_background = 0;

char *current_write_open_picofile = NULL;
char *current_write_open_path_picofile = NULL;

MENU* plot_experiment_menu = NULL;
MENU quicklink_menu[32] = {0};

O_p *opmlfi = NULL;
int (*get_camera_shutter_in_us)(int *set, int *smin, int *smax);
int (*set_camera_shutter_in_us)(int exp, int *set, int *smin, int *smax);
int (*get_camera_gain)(float *gain, float *fmin, float *fmax, float *finc);
int (*set_camera_gain)(float gain, float *set, float *fmin, float *fmax, float *finc);

MENU* (*specific_camera_image_menu)(void);
# endif


PXV_FUNC(MENU*, trackBead_image_menu, (void));
PXV_FUNC(MENU*, trackBead_ex_image_menu, (void));
PXV_FUNC(MENU*, trackBead_plot_menu, (void));
PXV_FUNC(int, trackBead_main, (int argc, char **argv));
PXV_FUNC(int, init_image_source,(imreg *imr, int mode));
PXV_FUNC(int, start_data_movie, (imreg *imr));


# ifdef TRACK_WIN_THREAD
PXV_FUNC(DWORD WINAPI, TrackingThreadProc, (LPVOID lpParam));
# else
PXV_FUNC(void *, TrackingThreadProc, (void *lpParam));
# endif
PXV_FUNC(int, live_source, (void));
PXV_FUNC(int, freeze_source, (void));
PXV_FUNC(int, kill_source, (void));
PXV_FUNC(int, prepare_image_overlay, (O_i* oi));
PXV_FUNC(int, follow_bead_in_x_y,(void));
PXV_FUNC(int, track_oi_idle_action, (struct  one_image *oi, DIALOG *d));

//PXV_FUNC(float, read_Z_value, (void));
//PXV_FUNC(int, set_Z_value,(float z));


PXV_FUNC(int, fill_next_available_job_spot, (int in_progress, int im, int type, int bead_nb, imreg *imr, O_i *oi, pltreg *pr, O_p *op, void* more_data, int (*job_to_do)(int im, struct future_job *job),int local));
PXV_FUNC(int, check_active_job_per_type, (int type));
PXV_FUNC(f_job*, find_job_associated_to_image,(O_i *oi));
PXV_FUNC(f_job*, find_job_associated_to_plot,(O_p *op));
PXV_FUNC(int,    dump_to_html_log_file_with_date_and_time, (char *fullfilename, const char *fmt, ...) __attribute__((format(printf, 2, 3))));
PXV_FUNC(int,    dump_to_html_log_file_with_time, (char *fullfilename, const char *fmt, ...) __attribute__((format(printf, 2, 3))));
PXV_FUNC(int,    dump_to_html_log_file_only, (char *fullfilename, const char *fmt, ...) __attribute__((format(printf, 2, 3))));

PXV_VAR(MENU*, plot_experiment_menu);

XV_FUNC(int,do_load_movie_on_disk,(void));

XV_VAR(char*, fui);



#endif
