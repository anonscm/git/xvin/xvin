/** \file brown_util.c
  \brief Plug-in program for bead tracking

  This is a subprogram that creates, displays and interfaces the menus with the plug-in functions.

  \author V. Croquette, J-F. Allemand
  */

#ifndef _BROWN_UTIL_C_
#define _BROWN_UTIL_C_

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// these rotines all work in the Real time timer thread
///////////////////////////////////////////////////////////////////////////////////////////////////////////////



# include "xvin.h"
#include "allegro.h"
#ifdef XV_WIN32
# include "winalleg.h"
#endif
# include "../fft2d/fftbtl32n.h"
# include "fillibbt.h"
//# include <largeint.h>
# include "xv_main.h"
//# include <alfont.h>
//# include "box2alfn.h"

/* But not below this define */
# define BUILDING_PLUGINS_DLL
#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "brown_util.h"

# define PARA_DIFF

# if !defined(PLAYITSAM) && !defined(PIAS)
PXV_VAR(float, evanescent_avg_intensity);
PXV_VAR(float, evanescent_avg_n_appo);
PXV_VAR(float, evanescent_black_avg);
PXV_VAR(int, evanescent_black_n_avg);
PXV_VAR(float, evanescent_spot_weighting[]);
#endif

// required for SMID
//typedef int v4sf __attribute__ ((mode(V4SF))); // vector of four single floats
typedef float v4sf __attribute__((vector_size(16)));   // vector of four single floats
union f4vector
{
    v4sf v;
    float f[4];
};
union f4vector f4, fax, fay, f0, fx, fy, fxc, fyc, fy2, fr[64], fx2[64];
// debugging array
//float radial_dx[256];

float fx1[512]; //, dfx1[512];

float *radial_non_pixel_square_image_sym_profile_in_array_sse(float *rz, O_i *ois, float xc, float yc, int r_max,
        float ax, float ay);
float *radial_non_pixel_square_image_sym_profile_in_array(float *rz, O_i *ois, float xc, float yc, int r_max, float ax,
        float ay);

float *radial_non_pixel_square_image_sym_profile_in_array_apodise(float *rz, O_i *ois, float xc, float yc, int r_max, float ax,
								  float ay);

float *(*radial_non_pixel_square_image_sym_profile_in_array_ptr)(float *rz, O_i *ois, float xc, float yc, int r_max,
        float ax, float ay) = radial_non_pixel_square_image_sym_profile_in_array;


int switch_radial_sse(void)
{
#ifndef XV_NO_SSE

    if (updating_menu_state != 0)
    {

        if (radial_non_pixel_square_image_sym_profile_in_array_ptr ==
                radial_non_pixel_square_image_sym_profile_in_array_sse)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }

    radial_non_pixel_square_image_sym_profile_in_array_ptr = radial_non_pixel_square_image_sym_profile_in_array_sse;
#endif
    return 0;

}


int switch_radial_std(void)
{
    if (updating_menu_state != 0)
    {

        if (radial_non_pixel_square_image_sym_profile_in_array_ptr ==
                radial_non_pixel_square_image_sym_profile_in_array)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }

    radial_non_pixel_square_image_sym_profile_in_array_ptr = radial_non_pixel_square_image_sym_profile_in_array;
    return 0;

}


int switch_radial_apo(void)
{
    if (updating_menu_state != 0)
    {

        if (radial_non_pixel_square_image_sym_profile_in_array_ptr ==
                radial_non_pixel_square_image_sym_profile_in_array_apodise)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }

    radial_non_pixel_square_image_sym_profile_in_array_ptr = radial_non_pixel_square_image_sym_profile_in_array_apodise;
    return 0;

}


// was int  fill_avg_profiles_from_im(O_i *oi, int xc, int yc, int cl, int cw, int *x, int *y)
#ifdef NEW
int fill_avg_profiles_from_im(O_i *oi, int xc, int yc, int cl, int cw, int *x, int *y, float *x_var, float *y_var)
#else
int fill_avg_profiles_from_im(O_i *oi, int xc, int yc, int cl, int cw, int *x, int *y)
#endif
{
    int j, i;
    unsigned char *choi = NULL;
    short int *inoi = NULL;
    unsigned short int *inui = NULL;
    int ytt, xll, onx, ony, xco, yco, cl2, cw2;
    union pix *pd = NULL;
#ifdef NEW
    float tmp, mean[PROFILE_BUFFER_SIZE] = {0};
# endif

    if (oi == NULL || x == NULL || y == NULL)   return 1;

    onx = oi->im.nx;
    ony = oi->im.ny;
    pd = oi->im.pixel;
    cl2 = cl >> 1;
    cw2 = cw >> 1;
    yco = (yc - cl2 < 0) ? cl2 : yc;
    yco = (yco + cl2 <= ony) ? yco : ony - cl2;
    xco = (xc - cl2 < 0) ? cl2 : xc;
    xco = (xco + cl2 <= onx) ? xco : onx - cl2;

    for (i = 0; i < cl; i++)     x[i] = y[i] = 0;

    if (oi->im.data_type == IS_CHAR_IMAGE)
    {
        for (j = 0, ytt = yco - cw2, xll = xco - cl2; j < cw; j++)
        {
            choi = pd[ytt + j].ch + xll;

            for (i = 0; i < cl; i++)        x[i] += (int)choi[i];
        }

        for (j = 0, ytt = yco - cl2, xll = xco - cw2; j < cl; j++)
        {
            choi = pd[ytt + j].ch + xll;

            for (i = 0; i < cw; i++)        y[j] += (int)choi[i];
        }

#ifdef NEW

        if (x_var)
        {
            for (i = 0; i < cl; i++)
            {
                mean[i] = (float)x[i] / cw;
                x_var[i] = 0;
            }

            for (j = 0, ytt = yco - cw2, xll = xco - cl2; j < cw; j++)
            {
                choi = pd[ytt + j].ch + xll;

                for (i = 0; i < cl; i++)
                {
                    tmp = mean[i] - choi[i];
                    x_var[i] += tmp * tmp;
                }
            }

            for (i = 0; i < cl && cw > 1; i++)    x_var[i] /= (cw - 1);
        }

        if (y_var)
        {
            for (i = 0; i < cl; i++)
            {
                mean[i] = (float)y[i] / cw;
                y_var[i] = 0;
            }

            for (j = 0, ytt = yco - cl2, xll = xco - cw2; j < cl; j++)
            {
                choi = pd[ytt + j].ch + xll;

                for (i = 0; i < cw; i++)
                {
                    tmp = mean[i] - choi[i];
                    y_var[i] += tmp * tmp;
                }
            }

            for (i = 0; i < cl && cw > 1; i++)    y_var[i] /= (cw - 1);
        }

#endif
    }
    else if (oi->im.data_type == IS_INT_IMAGE)
    {
        for (j = 0, ytt = yco - cw2, xll = xco - cl2; j < cw; j++)
        {
            inoi = pd[ytt + j].in + xll;

            for (i = 0; i < cl; i++)        x[i] += (int)inoi[i];
        }

        for (j = 0, ytt = yco - cl2, xll = xco - cw2; j < cl; j++)
        {
            inoi = pd[ytt + j].in + xll;

            for (i = 0; i < cw; i++)        y[j] += (int)inoi[i];
        }

#ifdef NEW

        if (x_var)
        {
            for (i = 0; i < cl; i++)
            {
                mean[i] = (float)x[i] / cw;
                x_var[i] = 0;
            }

            for (j = 0, ytt = yco - cw2, xll = xco - cl2; j < cw; j++)
            {
                inoi = pd[ytt + j].in + xll;

                for (i = 0; i < cl; i++)
                {
                    tmp = mean[i] - inoi[i];
                    x_var[i] += tmp * tmp;
                }
            }

            for (i = 0; i < cl && cw > 1; i++)    x_var[i] /= (cw - 1);
        }

        if (y_var)
        {
            for (i = 0; i < cl; i++)
            {
                mean[i] = (float)y[i] / cw;
                y_var[i] = 0;
            }

            for (j = 0, ytt = yco - cl2, xll = xco - cw2; j < cl; j++)
            {
                inoi = pd[ytt + j].in + xll;

                for (i = 0; i < cw; i++)
                {
                    tmp = mean[i] - inoi[i];
                    y_var[i] += tmp * tmp;
                }
            }

            for (i = 0; i < cl && cw > 1; i++)    y_var[i] /= (cw - 1);
        }

#endif

        //my_set_window_title("       short int profile x0 %d",x[0]);
    }
    else if (oi->im.data_type == IS_UINT_IMAGE)
    {
        for (j = 0, ytt = yco - cw2, xll = xco - cl2; j < cw; j++)
        {
            inui = pd[ytt + j].ui + xll;

            for (i = 0; i < cl; i++)        x[i] += (int)inui[i];
        }

        for (j = 0, ytt = yco - cl2, xll = xco - cw2; j < cl; j++)
        {
            inui = pd[ytt + j].ui + xll;

            for (i = 0; i < cw; i++)        y[j] += (int)inui[i];
        }

        //my_set_window_title("       short int profile x0 %d",x[0]);
#ifdef NEW

        if (x_var)
        {
            for (i = 0; i < cl; i++)
            {
                mean[i] = (float)x[i] / cw;
                x_var[i] = 0;
            }

            for (j = 0, ytt = yco - cw2, xll = xco - cl2; j < cw; j++)
            {
                inui = pd[ytt + j].ui + xll;

                for (i = 0; i < cl; i++)
                {
                    tmp = mean[i] - inui[i];
                    x_var[i] += tmp * tmp;
                }
            }

            for (i = 0; i < cl && cw > 1; i++)    x_var[i] /= (cw - 1);
        }

        if (y_var)
        {
            for (i = 0; i < cl; i++)
            {
                mean[i] = (float)y[i] / cw;
                y_var[i] = 0;
            }

            for (j = 0, ytt = yco - cl2, xll = xco - cw2; j < cl; j++)
            {
                inui = pd[ytt + j].ui + xll;

                for (i = 0; i < cw; i++)
                {
                    tmp = mean[i] - inui[i];
                    y_var[i] += tmp * tmp;
                }
            }

            for (i = 0; i < cl && cw > 1; i++)    y_var[i] /= (cw - 1);
        }

#endif

    }

    else
    {
        //my_set_window_title("       not the right profile type x0 %d",x[0]);
        return 1;
    }

    return 0;
}


// _diff
#ifdef NEW
int fill_avg_profiles_from_im_diff(O_i *oi, int xc, int yc, int cl, int cw, int *x, int *y, int *xd, int *yd,
                                   int *x_var, int *y_var, int *xd_var, int *yd_var)
# else
int fill_avg_profiles_from_im_diff(O_i *oi, int xc, int yc, int cl, int cw, int *x, int *y, int *xd, int *yd)
# endif
{
    int j, i, k;
    unsigned char *choi = NULL;
    short int *inoi = NULL;
    unsigned short int *inui = NULL;
    int ytt, xll, onx, ony, xco, yco, cl2, cw2, cl22;
    union pix *pd = NULL;

    if (oi == NULL || x == NULL || y == NULL)   return 1;

    onx = oi->im.nx;
    ony = oi->im.ny;
    pd = oi->im.pixel;
    cl2 = cl >> 1;
    cw2 = cw >> 1;
    cl22 = cl2 * cl2;
    yco = (yc - cl2 < 0) ? cl2 : yc;
    yco = (yco + cl2 <= ony) ? yco : ony - cl2;
    xco = (xc - cl2 < 0) ? cl2 : xc;
    xco = (xco + cl2 <= onx) ? xco : onx - cl2;

    for (i = 0; i < cl; i++)     x[i] = y[i] = xd[i] = yd[i] = 0;

    if (oi->im.data_type == IS_CHAR_IMAGE)
    {
        for (j = 0, ytt = yco - cw2, xll = xco - cl2; j < cw; j++)
        {
            // we average the central band
            choi = pd[ytt + j].ch + xll;

            for (i = 0; i < cl; i++)        x[i] += (int)choi[i];
        }

#ifdef NEW

        if (x_var)
        {
            for (i = 0; i < cl; i++)       x_var[i] = 0;

            for (j = 0, ytt = yco - cw2, xll = xco - cl2; j < cw; j++)
            {
                for (i = 0, choi = pd[ytt + j].ch + xll; i < cl; i++)
                    x_var[i] += ((j & 0x01) == (i & 0x01)) ?  - choi[i] : choi[i];
            }
        }

# endif
# ifdef PARA_DIFF

        for (j = 0, ytt = yco, xll = xco - cl2; j < cw2; j++)
        {
            // we remove lateral bands on a parabola
            for (i = 0; i < cl; i++)
            {
                k = ((cl2 - cw) * (i - cl2) * (i - cl2)) / cl22;
                k = cl2 - k;
                xd[i] += (int)pd[ytt - k + j].ch[i + xll];
                xd[i] += (int)pd[ytt + k - j - 1].ch[i + xll];
            }
        }

# else

        for (j = 0, ytt = yco - cw, xll = xco - cl2; j < cw2; j++)
        {
            // we remove lateral bands
            choi = pd[ytt + j].ch + xll;

            for (i = 0; i < cl; i++)        xd[i] += (int)choi[i];
        }

        for (j = 0, ytt = yco + cw2, xll = xco - cl2; j < cw2; j++)
        {
            // we remove lateral bands
            choi = pd[ytt + j].ch + xll;

            for (i = 0; i < cl; i++)        xd[i] += (int)choi[i];
        }

# endif
#ifdef NEW

        if (xd_var)
        {
            for (i = 0; i < cl; i++)      xd_var[i] = 0;

            for (j = 0, ytt = yco - cw, xll = xco - cl2; j < cw2; j++)
            {
                // we remove lateral bands
                for (i = 0, choi = pd[ytt + j].ch + xll; i < cl; i++)
                    xd_var[i] += ((j & 0x01) == (i & 0x01)) ? - choi[i] : choi[i];
            }

            for (j = 0, ytt = yco + cw2, xll = xco - cl2; j < cw2; j++)
            {
                // we remove lateral bands
                for (i = 0, choi = pd[ytt + j].ch + xll; i < cl; i++)
                    xd_var[i] += ((j & 0x01) == (i & 0x01)) ?  - choi[i] : choi[i];
            }
        }

# endif

        for (j = 0, ytt = yco - cl2, xll = xco - cw2; j < cl; j++)
        {
            choi = pd[ytt + j].ch + xll;

            for (i = 0; i < cw; i++)        y[j] += (int)choi[i];
        }

#ifdef NEW

        if (y_var)
        {
            for (i = 0; i < cl; i++)      y_var[i] = 0;

            for (j = 0, ytt = yco - cl2, xll = xco - cw2; j < cl; j++)
            {
                for (i = 0, choi = pd[ytt + j].ch + xll; i < cw; i++)
                    y_var[j] += ((j & 0x01) == (i & 0x01)) ? -choi[i] : choi[i];
            }
        }

# endif
# ifdef PARA_DIFF

        for (j = 0, ytt = yco - cl2, xll = xco; j < cl; j++)
        {
            // we remove lateral bands on a parabola
            k = ((cl2 - cw) * (j - cl2) * (j - cl2)) / cl22;
            k = cl2 - k;

            for (i = 0; i < cw2; i++)
            {
                yd[j] += (int)pd[ytt + j].ch[xll - k + i];
                yd[j] += (int)pd[ytt + j].ch[xll + k - i - 1];
            }
        }

# else

        for (j = 0, ytt = yco - cl2, xll = xco - cw; j < cl; j++)
        {
            choi = pd[ytt + j].ch + xll;

            for (i = 0; i < cw2; i++)       yd[j] += (int)choi[i];
        }

        for (j = 0, ytt = yco - cl2, xll = xco + cw2; j < cl; j++)
        {
            choi = pd[ytt + j].ch + xll;

            for (i = 0; i < cw2; i++)       yd[j] += (int)choi[i];
        }

# endif
#ifdef NEW

        if (yd_var)
        {
            for (i = 0; i < cl; i++)           yd_var[i] = 0;

            for (j = 0, ytt = yco - cl2, xll = xco - cw; j < cl; j++)
            {
                for (i = 0, choi = pd[ytt + j].ch + xll; i < cw2; i++)
                    yd_var[j] += ((j & 0x01) == (i & 0x01)) ? - choi[i] : choi[i];
            }

            for (j = 0, ytt = yco - cl2, xll = xco + cw2; j < cl; j++)
            {
                for (i = 0, choi = pd[ytt + j].ch + xll; i < cw2; i++)
                    yd_var[j] += ((j & 0x01) == (i & 0x01)) ? - choi[i] : choi[i];
            }
        }

# endif
    }
    else if (oi->im.data_type == IS_INT_IMAGE)
    {
        for (j = 0, ytt = yco - cw2, xll = xco - cl2; j < cw; j++)
        {
            inoi = pd[ytt + j].in + xll;

            for (i = 0; i < cl; i++)        x[i] += (int)inoi[i];
        }

#ifdef NEW

        if (x_var)
        {
            for (i = 0; i < cl; i++)            x_var[i] = 0;

            for (j = 0, ytt = yco - cw2, xll = xco - cl2; j < cw; j++)
            {
                for (i = 0, inoi = pd[ytt + j].in + xll; i < cl; i++)
                    x_var[i] += ((j & 0x01) == (i & 0x01)) ? -inoi[i] : inoi[i];
            }
        }

# endif
# ifdef PARA_DIFF

        for (j = 0, ytt = yco, xll = xco - cl2; j < cw2; j++)
        {
            // we remove lateral bands on a parabola
            for (i = 0; i < cl; i++)
            {
                k = ((cl2 - cw) * (i - cl2) * (i - cl2)) / cl22;
                k = cl2 - k;
                xd[i] += (int)pd[ytt - k + j].in[i + xll];
                xd[i] += (int)pd[ytt + k - j - 1].in[i + xll];
            }
        }

# else

        for (j = 0, ytt = yco - cw, xll = xco - cl2; j < cw2; j++)
        {
            inoi = pd[ytt + j].in + xll;

            for (i = 0; i < cl; i++)        xd[i] += (int)inoi[i];
        }

        for (j = 0, ytt = yco + cw2, xll = xco - cl2; j < cw2; j++)
        {
            inoi = pd[ytt + j].in + xll;

            for (i = 0; i < cl; i++)        xd[i] += (int)inoi[i];
        }

# endif
#ifdef NEW

        if (xd_var)
        {
            for (i = 0; i < cl; i++)            xd_var[i] = 0;

            for (j = 0, ytt = yco - cw, xll = xco - cl2; j < cw2; j++)
            {
                // we remove lateral bands
                for (i = 0, inoi = pd[ytt + j].in + xll; i < cl; i++)
                    xd_var[i] += ((j & 0x01) == (i & 0x01)) ? - inoi[i] : inoi[i];
            }

            for (j = 0, ytt = yco + cw2, xll = xco - cl2; j < cw2; j++)
            {
                // we remove lateral bands
                for (i = 0, inoi = pd[ytt + j].in + xll; i < cl; i++)
                    xd_var[i] += ((j & 0x01) == (i & 0x01)) ? - inoi[i] : inoi[i];
            }
        }

# endif

        for (j = 0, ytt = yco - cl2, xll = xco - cw2; j < cl; j++)
        {
            inoi = pd[ytt + j].in + xll;

            for (i = 0; i < cw; i++)        y[j] += (int)inoi[i];
        }

#ifdef NEW

        if (y_var)
        {
            for (i = 0; i < cl; i++)            y_var[i] = 0;

            for (j = 0, ytt = yco - cl2, xll = xco - cw2; j < cl; j++)
            {
                for (i = 0, inoi = pd[ytt + j].in + xll; i < cw; i++)
                    y_var[j] += ((j & 0x01) == (i & 0x01)) ? - inoi[i] : inoi[i];
            }
        }

# endif
# ifdef PARA_DIFF

        for (j = 0, ytt = yco - cl2, xll = xco; j < cl; j++)
        {
            // we remove lateral bands on a parabola
            k = ((cl2 - cw) * (j - cl2) * (j - cl2)) / cl22;
            k = cl2 - k;

            for (i = 0; i < cw2; i++)
            {
                yd[j] += (int)pd[ytt + j].in[xll - k + i];
                yd[j] += (int)pd[ytt + j].in[xll + k - i - 1];
            }
        }

# else

        for (j = 0, ytt = yco - cl2, xll = xco - cw; j < cl; j++)
        {
            inoi = pd[ytt + j].in + xll;

            for (i = 0; i < cw2; i++)       yd[j] += (int)inoi[i];
        }

        for (j = 0, ytt = yco - cl2, xll = xco + cw2; j < cl; j++)
        {
            inoi = pd[ytt + j].in + xll;

            for (i = 0; i < cw2; i++)       yd[j] += (int)inoi[i];
        }

# endif
#ifdef NEW

        if (yd_var)
        {
            for (i = 0; i < cl; i++)            yd_var[i] = 0;

            for (j = 0, ytt = yco - cl2, xll = xco - cw; j < cl; j++)
            {
                for (i = 0, inoi = pd[ytt + j].in + xll; i < cw2; i++)
                    yd_var[j] += ((j & 0x01) == (i & 0x01)) ? - inoi[i] : inoi[i];
            }

            for (j = 0, ytt = yco - cl2, xll = xco + cw2; j < cl; j++)
            {
                for (i = 0, inoi = pd[ytt + j].in + xll; i < cw2; i++)
                    yd_var[j] += ((j & 0x01) == (i & 0x01)) ? - inoi[i] : inoi[i];
            }
        }

# endif
        //my_set_window_title("       short int profile x0 %d",x[0]);
    }
    else if (oi->im.data_type == IS_UINT_IMAGE)
    {
        for (j = 0, ytt = yco - cw2, xll = xco - cl2; j < cw; j++)
        {
            inui = pd[ytt + j].ui + xll;

            for (i = 0; i < cl; i++)        x[i] += (int)inui[i];
        }

#ifdef NEW

        if (x_var)
        {
            for (i = 0; i < cl; i++)            x_var[i] = 0;

            for (j = 0, ytt = yco - cw2, xll = xco - cl2; j < cw; j++)
            {
                for (i = 0, inui = pd[ytt + j].ui + xll; i < cl; i++)
                    x_var[i] += ((j & 0x01) == (i & 0x01)) ? -inui[i] : inui[i];
            }
        }

# endif
# ifdef PARA_DIFF

        for (j = 0, ytt = yco, xll = xco - cl2; j < cw2; j++)
        {
            // we remove lateral bands on a parabola
            for (i = 0; i < cl; i++)
            {
                k = ((cl2 - cw) * (i - cl2) * (i - cl2)) / cl22;
                k = cl2 - k;
                xd[i] += (int)pd[ytt - k + j].ui[i + xll];
                xd[i] += (int)pd[ytt + k - j - 1].ui[i + xll];
            }
        }

# else

        for (j = 0, ytt = yco - cw, xll = xco - cl2; j < cw2; j++)
        {
            inui = pd[ytt + j].ui + xll;

            for (i = 0; i < cl; i++)        xd[i] += (int)inui[i];
        }

        for (j = 0, ytt = yco + cw2, xll = xco - cl2; j < cw2; j++)
        {
            inui = pd[ytt + j].ui + xll;

            for (i = 0; i < cl; i++)        xd[i] += (int)inui[i];
        }

# endif
#ifdef NEW

        if (xd_var)
        {
            for (i = 0; i < cl; i++)            xd_var[i] = 0;

            for (j = 0, ytt = yco - cw, xll = xco - cl2; j < cw2; j++)
            {
                // we remove lateral bands
                for (i = 0, inui = pd[ytt + j].ui + xll; i < cl; i++)
                    xd_var[i] += ((j & 0x01) == (i & 0x01)) ? - inui[i] : inui[i];
            }

            for (j = 0, ytt = yco + cw2, xll = xco - cl2; j < cw2; j++)
            {
                // we remove lateral bands
                for (i = 0, inui = pd[ytt + j].ui + xll; i < cl; i++)
                    xd_var[i] += ((j & 0x01) == (i & 0x01)) ? - inui[i] : inui[i];
            }
        }

# endif

        for (j = 0, ytt = yco - cl2, xll = xco - cw2; j < cl; j++)
        {
            inui = pd[ytt + j].ui + xll;

            for (i = 0; i < cw; i++)        y[j] += (int)inui[i];
        }

#ifdef NEW

        if (y_var)
        {
            for (i = 0; i < cl; i++)            y_var[i] = 0;

            for (j = 0, ytt = yco - cl2, xll = xco - cw2; j < cl; j++)
            {
                for (i = 0, inui = pd[ytt + j].ui + xll; i < cw; i++)
                    y_var[i] += ((j & 0x01) == (i & 0x01)) ? - inui[i] : inui[i];
            }
        }

# endif
# ifdef PARA_DIFF

        for (j = 0, ytt = yco - cl2, xll = xco; j < cl; j++)
        {
            // we remove lateral bands on a parabola
            k = ((cl2 - cw) * (j - cl2) * (j - cl2)) / cl22;
            k = cl2 - k;

            for (i = 0; i < cw2; i++)
            {
                yd[j] += (int)pd[ytt + j].li[xll - k + i];
                yd[j] += (int)pd[ytt + j].li[xll + k - i - 1];
            }
        }

# else

        for (j = 0, ytt = yco - cl2, xll = xco - cw; j < cl; j++)
        {
            inui = pd[ytt + j].ui + xll;

            for (i = 0; i < cw2; i++)       yd[j] += (int)inui[i];
        }

        for (j = 0, ytt = yco - cl2, xll = xco + cw2; j < cl; j++)
        {
            inui = pd[ytt + j].ui + xll;

            for (i = 0; i < cw2; i++)       yd[j] += (int)inui[i];
        }

# endif
#ifdef NEW

        if (yd_var)
        {
            for (i = 0; i < cl; i++)            yd_var[i] = 0;

            for (j = 0, ytt = yco - cl2, xll = xco - cw; j < cl; j++)
            {
                for (i = 0, inui = pd[ytt + j].ui + xll; i < cw2; i++)
                    yd_var[j] += ((j & 0x01) == (i & 0x01)) ? - inui[i] : inui[i];
            }

            for (j = 0, ytt = yco - cl2, xll = xco + cw2; j < cl; j++)
            {
                for (i = 0, inui = pd[ytt + j].ui + xll; i < cw2; i++)
                    yd_var[j] += ((j & 0x01) == (i & 0x01)) ? - inui[i] : inui[i];
            }
        }

# endif
        //my_set_window_title("       short int profile x0 %d",x[0]);
    }

    else
    {
        //my_set_window_title("       not the right profile type x0 %d",x[0]);
        return 1;
    }

    return 0;
}


#ifdef NEW
int fill_X_avg_profiles_from_im(O_i *oi, int xc, int yc, int cl, int cw, int *x, int *y, float *x_var, float *y_var)
#else
int fill_X_avg_profiles_from_im(O_i *oi, int xc, int yc, int cl, int cw, int *x, int *y)
#endif
{
    int j, i;
    int ytt, xll, onx, ony, xco, yco, cl2, cw2, d2;
    union pix *pd;
#ifdef NEW
    float tmp, mean[PROFILE_BUFFER_SIZE] = {0};
# endif

    if (oi == NULL || x == NULL || y == NULL)   return 1;

    onx = oi->im.nx;
    ony = oi->im.ny;
    pd = oi->im.pixel;
    cl2 = cl >> 1;
    cw2 = cw >> 1;
    d2 = cl2 + cw2 - 1;
    yco = (yc - d2 < 0) ? d2 : yc;
    yco = (yco + d2 <= ony) ? yco : ony - d2 - 1;
    xco = (xc - d2 < 0) ? d2 : xc;
    xco = (xco + d2 <= onx) ? xco : onx - d2 - 1;

    for (i = 0; i < cl; i++)     x[i] = y[i] = 0;

    if (oi->im.data_type == IS_CHAR_IMAGE)
    {
        ytt = yco - d2;
        xll = xco - d2;
        xll += cw - 1;

        for (i = 0; i < cl; i++)
        {
            for (j = 0; j < cw; j++)
                x[i] += (int)pd[ytt + i + j].ch[xll + i - j];
        }

        ytt = yco - d2;
        ytt += cl + cw - 2;
        xll = xco - d2;
        xll += cw - 1;

        for (i = 0; i < cl; i++)
        {
            for (j = 0; j < cw; j++)
                y[i] += (int)pd[ytt - i - j].ch[xll + i - j];
        }

#ifdef NEW

        if (x_var)
        {
            for (i = 0; i < cl; i++)
            {
                mean[i] = (float)x[i] / cw;
                x_var[i] = 0;
            }

            ytt = yco - d2;
            xll = xco - d2;
            xll += cw - 1;

            for (i = 0; i < cl; i++)
            {
                for (j = 0; j < cw; j++)
                {
                    tmp = mean[i] - pd[ytt + i + j].ch[xll + i - j];
                    x_var[i] += tmp * tmp;
                }
            }

            for (i = 0; i < cl && cw > 1; i++)    x_var[i] /= (cw - 1);
        }

        if (y_var)
        {
            for (i = 0; i < cl; i++)
            {
                mean[i] = (float)y[i] / cw;
                y_var[i] = 0;
            }

            ytt = yco - d2;
            ytt += cl + cw - 2;
            xll = xco - d2;
            xll += cw - 1;

            for (i = 0; i < cl; i++)
            {
                for (j = 0; j < cw; j++)
                {
                    tmp = mean[i] - pd[ytt - i - j].ch[xll + i - j];
                    y_var[i] += tmp * tmp;
                }
            }

            for (i = 0; i < cl && cw > 1; i++)    y_var[i] /= (cw - 1);
        }

#endif
    }
    else if (oi->im.data_type == IS_INT_IMAGE)
    {
        ytt = yco - d2;
        xll = xco - d2;
        xll += cw - 1;

        for (i = 0; i < cl; i++)
        {
            for (j = 0; j < cw; j++)
                x[i] += (int)pd[ytt + i + j].in[xll + i - j];
        }

        ytt = yco - d2;
        ytt += cl + cw - 2;
        xll = xco - d2;
        xll += cw - 1;

        for (i = 0; i < cl; i++)
        {
            for (j = 0; j < cw; j++)
                y[i] += (int)pd[ytt - i - j].in[xll + i - j];
        }

#ifdef NEW

        if (x_var)
        {
            for (i = 0; i < cl; i++)
            {
                mean[i] = (float)x[i] / cw;
                x_var[i] = 0;
            }

            ytt = yco - d2;
            xll = xco - d2;
            xll += cw - 1;

            for (i = 0; i < cl; i++)
            {
                for (j = 0; j < cw; j++)
                {
                    tmp = mean[i] - pd[ytt + i + j].in[xll + i - j];
                    x_var[i] += tmp * tmp;
                }
            }

            for (i = 0; i < cl && cw > 1; i++)    x_var[i] /= (cw - 1);
        }

        if (y_var)
        {
            for (i = 0; i < cl; i++)
            {
                mean[i] = (float)y[i] / cw;
                y_var[i] = 0;
            }

            ytt = yco - d2;
            ytt += cl + cw - 2;
            xll = xco - d2;
            xll += cw - 1;

            for (i = 0; i < cl; i++)
            {
                for (j = 0; j < cw; j++)
                {
                    tmp = mean[i] - pd[ytt - i - j].in[xll + i - j];
                    y_var[i] += tmp * tmp;
                }
            }

            for (i = 0; i < cl && cw > 1; i++)    y_var[i] /= (cw - 1);
        }

#endif
    }
    else if (oi->im.data_type == IS_UINT_IMAGE)
    {
        ytt = yco - d2;
        xll = xco - d2;
        xll += cw - 1;

        for (i = 0; i < cl; i++)
        {
            for (j = 0; j < cw; j++)
                x[i] += (int)pd[ytt + i + j].ui[xll + i - j];
        }

        ytt = yco - d2;
        ytt += cl + cw - 2;
        xll = xco - d2;
        xll += cw - 1;

        for (i = 0; i < cl; i++)
        {
            for (j = 0; j < cw; j++)
                y[i] += (int)pd[ytt - i - j].ui[xll + i - j];
        }

#ifdef NEW

        if (x_var)
        {
            for (i = 0; i < cl; i++)
            {
                mean[i] = (float)x[i] / cw;
                x_var[i] = 0;
            }

            ytt = yco - d2;
            xll = xco - d2;
            xll += cw - 1;

            for (i = 0; i < cl; i++)
            {
                for (j = 0; j < cw; j++)
                {
                    tmp = mean[i] - pd[ytt + i + j].ui[xll + i - j];
                    x_var[i] += tmp * tmp;
                }
            }

            for (i = 0; i < cl && cw > 1; i++)    x_var[i] /= (cw - 1);
        }

        if (y_var)
        {
            for (i = 0; i < cl; i++)
            {
                mean[i] = (float)y[i] / cw;
                y_var[i] = 0;
            }

            ytt = yco - d2;
            ytt += cl + cw - 2;
            xll = xco - d2;
            xll += cw - 1;

            for (i = 0; i < cl; i++)
            {
                for (j = 0; j < cw; j++)
                {
                    tmp = mean[i] - pd[ytt - i - j].ui[xll + i - j];
                    y_var[i] += tmp * tmp;
                }
            }

            for (i = 0; i < cl && cw > 1; i++)    y_var[i] /= (cw - 1);
        }

#endif
    }
    else return 1;

    return 0;
}

#ifdef NEW
int fill_X_avg_profiles_from_im_diff(O_i *oi, int xc, int yc, int cl, int cw, int *x, int *y, int *xd, int *yd,
                                     int *x_var, int *y_var, int *xd_var, int *yd_var)
# else
int   fill_X_avg_profiles_from_im_diff(O_i *oi, int xc, int yc, int cl, int cw, int *x, int *y, int *xd, int *yd)
#endif
{
    int j, i;
    int ytt, xll, onx, ony, xco, yco, cl2, cw2, d, cw_2, cw_3, ytij, xlij;//, d2
    union pix *pd = NULL;

    if (oi == NULL || x == NULL || y == NULL) return 1;

    onx = oi->im.nx;
    ony = oi->im.ny;
    pd = oi->im.pixel;
    cl2 = cl >> 1;
    cw2 = cw >> 1;
    cw_2 = 2 * cw;
    cw_3 = cw + cw2;
    //d2 = cl2 + cw2 - 1;
    d = cl2 + cw - 1;
    yco = (yc - d < 0) ? d : yc;
    yco = (yco + d < ony) ? yco : ony - d - 1;
    xco = (xc - d < 0) ? d : xc;
    xco = (xco + d < onx) ? xco : onx - d - 1;

    for (i = 0; i < cl; i++)       x[i] = y[i] = xd[i] = yd[i] = 0;

    if (oi->im.data_type == IS_CHAR_IMAGE)
    {
        ytt = yco - d;
        xll = xco - d;
        xll += cw_2 - 1;

        for (i = 0; i < cl; i++)
        {
            for (j = 0; j < cw2; j++)
            {
                ytij = ytt + i + j;
                xlij = xll + i - j;
                ytij = (ytij < 0) ? 0 : ((ytij >= oi->im.ny) ? oi->im.ny - 1 : ytij);
                xlij = (xlij < 0) ? 0 : ((xlij >= oi->im.nx) ? oi->im.nx - 1 : xlij);
                xd[i] += (int)pd[ytij].ch[xlij];
            }

            for (j = cw2; j < cw_3; j++)
            {
                ytij = ytt + i + j;
                xlij = xll + i - j;
                ytij = (ytij < 0) ? 0 : ((ytij >= oi->im.ny) ? oi->im.ny - 1 : ytij);
                xlij = (xlij < 0) ? 0 : ((xlij >= oi->im.nx) ? oi->im.nx - 1 : xlij);
                x[i] += (int)pd[ytij].ch[xlij];
            }

            for (j = cw_3; j < cw_2; j++)
            {
                ytij = ytt + i + j;
                xlij = xll + i - j;
                ytij = (ytij < 0) ? 0 : ((ytij >= oi->im.ny) ? oi->im.ny - 1 : ytij);
                xlij = (xlij < 0) ? 0 : ((xlij >= oi->im.nx) ? oi->im.nx - 1 : xlij);
                xd[i] += (int)pd[ytij].ch[xlij];
            }
        }

#ifdef NEW

        if (x_var)
        {
            for (i = 0; i < cl; i++)
            {
                for (j = cw2, x_var[i] = 0; j < cw_3; j++)
                {
                    ytij = ytt + i + j;
                    xlij = xll + i - j;
                    ytij = (ytij < 0) ? 0 : ((ytij >= oi->im.ny) ? oi->im.ny - 1 : ytij);
                    xlij = (xlij < 0) ? 0 : ((xlij >= oi->im.nx) ? oi->im.nx - 1 : xlij);
                    x_var[i] += (j % 2) ? - pd[ytij].ch[xlij] : pd[ytij].ch[xlij];
                }
            }
        }

        if (xd_var)
        {
            for (i = 0; i < cl; i++)
            {
                xd_var[i] = 0;

                for (j = 0; j < cw2; j++)
                {
                    ytij = ytt + i + j;
                    xlij = xll + i - j;
                    ytij = (ytij < 0) ? 0 : ((ytij >= oi->im.ny) ? oi->im.ny - 1 : ytij);
                    xlij = (xlij < 0) ? 0 : ((xlij >= oi->im.nx) ? oi->im.nx - 1 : xlij);
                    xd_var[i] += (j % 2) ? - pd[ytij].ch[xlij] : pd[ytij].ch[xlij];
                }

                for (j = cw_3; j < cw_2; j++)
                {
                    ytij = ytt + i + j;
                    xlij = xll + i - j;
                    ytij = (ytij < 0) ? 0 : ((ytij >= oi->im.ny) ? oi->im.ny - 1 : ytij);
                    xlij = (xlij < 0) ? 0 : ((xlij >= oi->im.nx) ? oi->im.nx - 1 : xlij);
                    xd_var[i] += (j % 2) ? - pd[ytij].ch[xlij] : pd[ytij].ch[xlij];
                }
            }
        }

#endif
        ytt = yco - d;
        ytt += cl + cw_2 - 2;
        xll = xco - d;
        xll += cw_2 - 1;

        for (i = 0; i < cl; i++)
        {
            for (j = 0; j < cw2; j++)
            {
                ytij = ytt - i - j;
                xlij = xll + i - j;
                ytij = (ytij < 0) ? 0 : ((ytij >= oi->im.ny) ? oi->im.ny - 1 : ytij);
                xlij = (xlij < 0) ? 0 : ((xlij >= oi->im.nx) ? oi->im.nx - 1 : xlij);
                yd[i] += (int)pd[ytij].ch[xlij];
            }

            for (j = cw2; j < cw_3; j++)
            {
                ytij = ytt - i - j;
                xlij = xll + i - j;
                ytij = (ytij < 0) ? 0 : ((ytij >= oi->im.ny) ? oi->im.ny - 1 : ytij);
                xlij = (xlij < 0) ? 0 : ((xlij >= oi->im.nx) ? oi->im.nx - 1 : xlij);
                y[i] += (int)pd[ytij].ch[xlij];
            }

            for (j = cw_3; j < cw_2; j++)
            {
                ytij = ytt - i - j;
                xlij = xll + i - j;
                ytij = (ytij < 0) ? 0 : ((ytij >= oi->im.ny) ? oi->im.ny - 1 : ytij);
                xlij = (xlij < 0) ? 0 : ((xlij >= oi->im.nx) ? oi->im.nx - 1 : xlij);
                yd[i] += (int)pd[ytij].ch[xlij];
            }
        }

#ifdef NEW

        if (y_var)
        {
            for (i = 0; i < cl; i++)
            {
                for (j = cw2, y_var[i] = 0; j < cw_3; j++)
                {
                    ytij = ytt - i - j;
                    xlij = xll + i - j;
                    ytij = (ytij < 0) ? 0 : ((ytij >= oi->im.ny) ? oi->im.ny - 1 : ytij);
                    xlij = (xlij < 0) ? 0 : ((xlij >= oi->im.nx) ? oi->im.nx - 1 : xlij);
                    y_var[i] += (j % 2) ? - pd[ytij].ch[xlij] : pd[ytij].ch[xlij];
                }
            }
        }

        if (yd_var)
        {
            for (i = 0; i < cl; i++)
            {
                yd_var[i] = 0;

                for (j = 0; j < cw2; j++)
                {
                    ytij = ytt - i - j;
                    xlij = xll + i - j;
                    ytij = (ytij < 0) ? 0 : ((ytij >= oi->im.ny) ? oi->im.ny - 1 : ytij);
                    xlij = (xlij < 0) ? 0 : ((xlij >= oi->im.nx) ? oi->im.nx - 1 : xlij);
                    yd_var[i] += (j % 2) ? - pd[ytij].ch[xlij] : pd[ytij].ch[xlij];
                }

                for (j = cw_3; j < cw_2; j++)
                {
                    ytij = ytt - i - j;
                    xlij = xll + i - j;
                    ytij = (ytij < 0) ? 0 : ((ytij >= oi->im.ny) ? oi->im.ny - 1 : ytij);
                    xlij = (xlij < 0) ? 0 : ((xlij >= oi->im.nx) ? oi->im.nx - 1 : xlij);
                    yd_var[i] += (j % 2) ? - pd[ytij].ch[xlij] : pd[ytij].ch[xlij];
                }
            }
        }

#endif
    }
    else if (oi->im.data_type == IS_INT_IMAGE)
    {
        ytt = yco - d;
        xll = xco - d;
        xll += cw_2 - 1;

        for (i = 0; i < cl; i++)
        {
            for (j = 0; j < cw2; j++)
            {
                ytij = ytt + i + j;
                xlij = xll + i - j;
                ytij = (ytij < 0) ? 0 : ((ytij >= oi->im.ny) ? oi->im.ny - 1 : ytij);
                xlij = (xlij < 0) ? 0 : ((xlij >= oi->im.nx) ? oi->im.nx - 1 : xlij);
                xd[i] += (int)pd[ytij].in[xlij];
            }

            for (j = cw2; j < cw_3; j++)
            {
                ytij = ytt + i + j;
                xlij = xll + i - j;
                ytij = (ytij < 0) ? 0 : ((ytij >= oi->im.ny) ? oi->im.ny - 1 : ytij);
                xlij = (xlij < 0) ? 0 : ((xlij >= oi->im.nx) ? oi->im.nx - 1 : xlij);
                x[i] += (int)pd[ytij].in[xlij];
            }

            for (j = cw_3; j < cw_2; j++)
            {
                ytij = ytt + i + j;
                xlij = xll + i - j;
                ytij = (ytij < 0) ? 0 : ((ytij >= oi->im.ny) ? oi->im.ny - 1 : ytij);
                xlij = (xlij < 0) ? 0 : ((xlij >= oi->im.nx) ? oi->im.nx - 1 : xlij);
                xd[i] += (int)pd[ytij].in[xlij];
            }
        }

#ifdef NEW

        if (x_var)
        {
            for (i = 0; i < cl; i++)
            {
                for (j = cw2, x_var[i] = 0; j < cw_3; j++)
                {
                    ytij = ytt + i + j;
                    xlij = xll + i - j;
                    ytij = (ytij < 0) ? 0 : ((ytij >= oi->im.ny) ? oi->im.ny - 1 : ytij);
                    xlij = (xlij < 0) ? 0 : ((xlij >= oi->im.nx) ? oi->im.nx - 1 : xlij);
                    x_var[i] += (j % 2) ? - pd[ytij].in[xlij] : pd[ytij].in[xlij];
                }
            }
        }

        if (xd_var)
        {
            for (i = 0; i < cl; i++)
            {
                xd_var[i] = 0;

                for (j = 0; j < cw2; j++)
                {
                    ytij = ytt + i + j;
                    xlij = xll + i - j;
                    ytij = (ytij < 0) ? 0 : ((ytij >= oi->im.ny) ? oi->im.ny - 1 : ytij);
                    xlij = (xlij < 0) ? 0 : ((xlij >= oi->im.nx) ? oi->im.nx - 1 : xlij);
                    xd_var[i] += (j % 2) ? - pd[ytij].in[xlij] : pd[ytij].in[xlij];
                }

                for (j = cw_3; j < cw_2; j++)
                {
                    ytij = ytt + i + j;
                    xlij = xll + i - j;
                    ytij = (ytij < 0) ? 0 : ((ytij >= oi->im.ny) ? oi->im.ny - 1 : ytij);
                    xlij = (xlij < 0) ? 0 : ((xlij >= oi->im.nx) ? oi->im.nx - 1 : xlij);
                    xd_var[i] += (j % 2) ? - pd[ytij].in[xlij] : pd[ytij].in[xlij];
                }
            }
        }

#endif
        ytt = yco - d;
        ytt += cl + cw_2 - 2;
        xll = xco - d;
        xll += cw_2 - 1;

        for (i = 0; i < cl; i++)
        {
            for (j = 0; j < cw2; j++)
            {
                ytij = ytt - i - j;
                xlij = xll + i - j;
                ytij = (ytij < 0) ? 0 : ((ytij >= oi->im.ny) ? oi->im.ny - 1 : ytij);
                xlij = (xlij < 0) ? 0 : ((xlij >= oi->im.nx) ? oi->im.nx - 1 : xlij);
                yd[i] += (int)pd[ytij].in[xlij];
            }

            for (j = cw2; j < cw_3; j++)
            {
                ytij = ytt - i - j;
                xlij = xll + i - j;
                ytij = (ytij < 0) ? 0 : ((ytij >= oi->im.ny) ? oi->im.ny - 1 : ytij);
                xlij = (xlij < 0) ? 0 : ((xlij >= oi->im.nx) ? oi->im.nx - 1 : xlij);
                y[i] += (int)pd[ytij].in[xlij];
            }

            for (j = cw_3; j < cw_2; j++)
            {
                ytij = ytt - i - j;
                xlij = xll + i - j;
                ytij = (ytij < 0) ? 0 : ((ytij >= oi->im.ny) ? oi->im.ny - 1 : ytij);
                xlij = (xlij < 0) ? 0 : ((xlij >= oi->im.nx) ? oi->im.nx - 1 : xlij);
                yd[i] += (int)pd[ytij].in[xlij];
            }
        }

#ifdef NEW

        if (y_var)
        {
            for (i = 0; i < cl; i++)
            {
                for (j = cw2, y_var[i] = 0; j < cw_3; j++)
                {
                    ytij = ytt - i - j;
                    xlij = xll + i - j;
                    ytij = (ytij < 0) ? 0 : ((ytij >= oi->im.ny) ? oi->im.ny - 1 : ytij);
                    xlij = (xlij < 0) ? 0 : ((xlij >= oi->im.nx) ? oi->im.nx - 1 : xlij);
                    y_var[i] += (j % 2) ? - pd[ytij].in[xlij] : pd[ytij].in[xlij];
                }
            }
        }

        if (yd_var)
        {
            for (i = 0; i < cl; i++)
            {
                yd_var[i] = 0;

                for (j = 0; j < cw2; j++)
                {
                    ytij = ytt - i - j;
                    xlij = xll + i - j;
                    ytij = (ytij < 0) ? 0 : ((ytij >= oi->im.ny) ? oi->im.ny - 1 : ytij);
                    xlij = (xlij < 0) ? 0 : ((xlij >= oi->im.nx) ? oi->im.nx - 1 : xlij);
                    yd_var[i] += (j % 2) ? - pd[ytij].in[xlij] : pd[ytij].in[xlij];
                }

                for (j = cw_3; j < cw_2; j++)
                {
                    ytij = ytt - i - j;
                    xlij = xll + i - j;
                    ytij = (ytij < 0) ? 0 : ((ytij >= oi->im.ny) ? oi->im.ny - 1 : ytij);
                    xlij = (xlij < 0) ? 0 : ((xlij >= oi->im.nx) ? oi->im.nx - 1 : xlij);
                    yd_var[i] += (j % 2) ? - pd[ytij].in[xlij] : pd[ytij].in[xlij];
                }
            }
        }

#endif
    }
    else if (oi->im.data_type == IS_UINT_IMAGE)
    {
        ytt = yco - d;
        xll = xco - d;
        xll += cw_2 - 1;

        for (i = 0; i < cl; i++)
        {
            for (j = 0; j < cw2; j++)
            {
                ytij = ytt + i + j;
                xlij = xll + i - j;
                ytij = (ytij < 0) ? 0 : ((ytij >= oi->im.ny) ? oi->im.ny - 1 : ytij);
                xlij = (xlij < 0) ? 0 : ((xlij >= oi->im.nx) ? oi->im.nx - 1 : xlij);
                xd[i] += (int)pd[ytij].ui[xlij];
            }

            for (j = cw2; j < cw_3; j++)
            {
                ytij = ytt + i + j;
                xlij = xll + i - j;
                ytij = (ytij < 0) ? 0 : ((ytij >= oi->im.ny) ? oi->im.ny - 1 : ytij);
                xlij = (xlij < 0) ? 0 : ((xlij >= oi->im.nx) ? oi->im.nx - 1 : xlij);
                x[i] += (int)pd[ytij].ui[xlij];
            }

            for (j = cw_3; j < cw_2; j++)
            {
                ytij = ytt + i + j;
                xlij = xll + i - j;
                ytij = (ytij < 0) ? 0 : ((ytij >= oi->im.ny) ? oi->im.ny - 1 : ytij);
                xlij = (xlij < 0) ? 0 : ((xlij >= oi->im.nx) ? oi->im.nx - 1 : xlij);
                xd[i] += (int)pd[ytij].ui[xlij];
            }
        }

#ifdef NEW

        if (x_var)
        {
            for (i = 0; i < cl; i++)
            {
                for (j = cw2, x_var[i] = 0; j < cw_3; j++)
                {
                    ytij = ytt + i + j;
                    xlij = xll + i - j;
                    ytij = (ytij < 0) ? 0 : ((ytij >= oi->im.ny) ? oi->im.ny - 1 : ytij);
                    xlij = (xlij < 0) ? 0 : ((xlij >= oi->im.nx) ? oi->im.nx - 1 : xlij);
                    x_var[i] += (j % 2) ? - pd[ytij].ui[xlij] : pd[ytij].ui[xlij];
                }
            }
        }

        if (xd_var)
        {
            for (i = 0; i < cl; i++)
            {
                xd_var[i] = 0;

                for (j = 0; j < cw2; j++)
                {
                    ytij = ytt + i + j;
                    xlij = xll + i - j;
                    ytij = (ytij < 0) ? 0 : ((ytij >= oi->im.ny) ? oi->im.ny - 1 : ytij);
                    xlij = (xlij < 0) ? 0 : ((xlij >= oi->im.nx) ? oi->im.nx - 1 : xlij);
                    xd_var[i] += (j % 2) ? pd[ytij].ui[xlij] : pd[ytij].ui[xlij];
                }

                for (j = cw_3; j < cw_2; j++)
                {
                    ytij = ytt + i + j;
                    xlij = xll + i - j;
                    ytij = (ytij < 0) ? 0 : ((ytij >= oi->im.ny) ? oi->im.ny - 1 : ytij);
                    xlij = (xlij < 0) ? 0 : ((xlij >= oi->im.nx) ? oi->im.nx - 1 : xlij);
                    xd_var[i] += (j % 2) ? pd[ytij].ui[xlij] : pd[ytij].ui[xlij];
                }
            }
        }

#endif
        ytt = yco - d;
        ytt += cl + cw_2 - 2;
        xll = xco - d;
        xll += cw_2 - 1;

        for (i = 0; i < cl; i++)
        {
            for (j = 0; j < cw2; j++)
            {
                ytij = ytt - i - j;
                xlij = xll + i - j;
                ytij = (ytij < 0) ? 0 : ((ytij >= oi->im.ny) ? oi->im.ny - 1 : ytij);
                xlij = (xlij < 0) ? 0 : ((xlij >= oi->im.nx) ? oi->im.nx - 1 : xlij);
                yd[i] += (int)pd[ytij].ui[xlij];
            }

            for (j = cw2; j < cw_3; j++)
            {
                ytij = ytt - i - j;
                xlij = xll + i - j;
                ytij = (ytij < 0) ? 0 : ((ytij >= oi->im.ny) ? oi->im.ny - 1 : ytij);
                xlij = (xlij < 0) ? 0 : ((xlij >= oi->im.nx) ? oi->im.nx - 1 : xlij);
                y[i] += (int)pd[ytij].ui[xlij];
            }

            for (j = cw_3; j < cw_2; j++)
            {
                ytij = ytt - i - j;
                xlij = xll + i - j;
                ytij = (ytij < 0) ? 0 : ((ytij >= oi->im.ny) ? oi->im.ny - 1 : ytij);
                xlij = (xlij < 0) ? 0 : ((xlij >= oi->im.nx) ? oi->im.nx - 1 : xlij);
                yd[i] += (int)pd[ytij].ui[xlij];
            }
        }

#ifdef NEW

        if (y_var)
        {
            for (i = 0; i < cl; i++)
            {
                for (j = cw2, y_var[i] = 0; j < cw_3; j++)
                {
                    ytij = ytt - i - j;
                    xlij = xll + i - j;
                    ytij = (ytij < 0) ? 0 : ((ytij >= oi->im.ny) ? oi->im.ny - 1 : ytij);
                    xlij = (xlij < 0) ? 0 : ((xlij >= oi->im.nx) ? oi->im.nx - 1 : xlij);
                    y_var[i] += (j % 2) ? - pd[ytij].ui[xlij] : pd[ytij].ui[xlij];
                }
            }
        }

        if (yd_var)
        {
            for (i = 0; i < cl; i++)
            {
                yd_var[i] = 0;

                for (j = 0; j < cw2; j++)
                {
                    ytij = ytt - i - j;
                    xlij = xll + i - j;
                    ytij = (ytij < 0) ? 0 : ((ytij >= oi->im.ny) ? oi->im.ny - 1 : ytij);
                    xlij = (xlij < 0) ? 0 : ((xlij >= oi->im.nx) ? oi->im.nx - 1 : xlij);
                    yd_var[i] += (j % 2) ? - pd[ytij].ui[xlij] : pd[ytij].ui[xlij];
                }

                for (j = cw_3; j < cw_2; j++)
                {
                    ytij = ytt - i - j;
                    xlij = xll + i - j;
                    ytij = (ytij < 0) ? 0 : ((ytij >= oi->im.ny) ? oi->im.ny - 1 : ytij);
                    xlij = (xlij < 0) ? 0 : ((xlij >= oi->im.nx) ? oi->im.nx - 1 : xlij);
                    yd_var[i] += (j % 2) ? - pd[ytij].ui[xlij] : pd[ytij].ui[xlij];
                }
            }
        }

#endif
    }
    else return 1;

    return 0;
}

double  avg_im_on_square_area(O_i *oi, int frame, int xc, int yc, int cl)
{
    int j, i;
    unsigned char *choi = NULL;
    short int *inoi = NULL;
    unsigned short int *inui = NULL;
    int ytt, xll, onx, ony, xco, yco, cl2;
    union pix *pd = NULL;
    double avg = 0;

    if (oi == NULL) return -1;

    onx = oi->im.nx;
    ony = oi->im.ny;
    pd = oi->im.pxl[frame];
    cl2 = cl >> 1;
    yco = (yc - cl2 < 0) ? cl2 : yc;
    yco = (yco + cl2 <= ony) ? yco : ony - cl2;
    xco = (xc - cl2 < 0) ? cl2 : xc;
    xco = (xco + cl2 <= onx) ? xco : onx - cl2;

    if (oi->im.data_type == IS_CHAR_IMAGE)
    {
        for (j = 0, ytt = yco - cl2, xll = xco - cl2; j < cl; j++)
        {
            choi = pd[ytt + j].ch + xll;

            for (i = 0; i < cl; i++)    avg += choi[i];
        }
    }
    else if (oi->im.data_type == IS_INT_IMAGE)
    {
        for (j = 0, ytt = yco - cl2, xll = xco - cl2; j < cl; j++)
        {
            inoi = pd[ytt + j].in + xll;

            for (i = 0; i < cl; i++)    avg += inoi[i];
        }
    }
    else if (oi->im.data_type == IS_UINT_IMAGE)
    {
        for (j = 0, ytt = yco - cl2, xll = xco - cl2; j < cl; j++)
        {
            inui = pd[ytt + j].ui + xll;

            for (i = 0; i < cl; i++)    avg += inui[i];
        }
    }
    else      return -1;

    return (cl > 0) ? avg / (cl * cl) : avg;
}




int check_bead_not_lost(int *x, float *x1, int *y, float *y1, int cl, float bd_mul)
{
    int i, j;
    int minx, maxx, miny, maxy;

    for (i = 0, minx = maxx = x[0], miny = maxy = y[0]; i < cl; i++)
    {
        j = x[i];
        minx = (j < minx) ? j : minx;
        maxx = (j > maxx) ? j : maxx;
        x1[i] = (float)j;
        j = y[i];
        miny = (j < miny) ? j : miny;
        maxy = (j > maxy) ? j : maxy;
        y1[i] = (float)j;
    }

    //test image contrast, if it is too low, the bead is lost
    return !!(((maxx - minx) * bd_mul) < (maxx + minx) &&
              ((maxy - miny) * bd_mul) < (maxy + miny));
}


int check_bead_not_lost_diff(int *x, int *xd, float *x1, int *y, int *yd, float *y1, int cl, float bd_mul, int do_diff)
{
    int i, j;
    int minx, maxx, miny, maxy;
    int minxd, maxxd, minyd, maxyd;

    minxd = maxxd = x[0] - xd[0];
    minyd = maxyd = y[0] - yd[0];

    for (i = 0, minx = maxx = x[0], miny = maxy = y[0]; i < cl; i++)
    {
        j = x[i];
        minx = (j < minx) ? j : minx;
        maxx = (j > maxx) ? j : maxx;

        if (do_diff == 1)
        {
            j -= xd[i];
            minxd = (j < minxd) ? j : minxd;
            maxxd = (j > maxxd) ? j : maxxd;
        }

        x1[i] = (float)j;
        j = y[i];
        miny = (j < miny) ? j : miny;
        maxy = (j > maxy) ? j : maxy;

        if (do_diff == 1)
        {
            j -= yd[i];
            minyd = (j < minyd) ? j : minyd;
            maxyd = (j > maxyd) ? j : maxyd;
        }

        y1[i] = (float)j;
    }

    //test image contrast, if it is too low, the bead is lost
    if (do_diff == 0)
        return !!(((maxx - minx) * bd_mul) < (maxx + minx) && ((maxy - miny) * bd_mul) < (maxy + miny));
    else
        return !!(((maxxd - minxd) * bd_mul) < (maxx + minx) && ((maxyd - minyd) * bd_mul) < (maxy + miny));
}


int     erase_around_black_circle(int *x, int *y, int cl)
{
    int j;
    int xb0, xb1, zb0, zb1;

    /*  for good bead there is a definite black circle around the image
        for rotation with a small bead you can suppress the image around
        this black circle to avoid perturbation by the small bead
        */

    for (xb0 = j = 0, zb0 = x[0]; j < cl >> 1; j++)
        zb0 = (zb0 < x[j]) ? zb0 : x[(xb0 = j)];

    for (xb1 = j = cl - 1, zb1 = x[xb1]; j >= cl >> 1; j--)
        zb1 = (zb1 < x[j]) ? zb1 : x[(xb1 = j)];

    zb0 = (zb0 > zb1) ? zb0 : zb1;

    for (j = 0; j < cl; j++)
        x[j] = (j < xb0 || j > xb1) ? 0 : x[j] - zb0;

    for (xb0 = j = 0, zb0 = y[0]; j < cl >> 1; j++)
        zb0 = (zb0 < y[j]) ? zb0 : y[(xb0 = j)];

    for (xb1 = j = cl - 1, zb1 = y[xb1]; j >= cl >> 1; j--)
        zb1 = (zb1 < y[j]) ? zb1 : y[(xb1 = j)];

    zb0 = (zb0 > zb1) ? zb0 : zb1;

    for (j = 0; j < cl; j++)
        y[j] = (j < xb0 || j > xb1) ? 0 : y[j] - zb0;

    return 0;
}

int deplace(float *x, int nx)
{
    int i, j;
    float tmp;

    for (i = 0, j = nx / 2; j < nx ; i++, j++)
    {
        tmp = x[j];
        x[j] = x[i];
        x[i] = tmp;
    }

    return 0;
}
int fftwindow_flat_top1(fft_plan *fp, int npts, float *x, float smp)
{
    int i, j;
    //int n_2, n_4;
    float tmp; //, *offtsin;

    (void)npts;

    if (fp == NULL)  return 1;

    //offtsin = get_fftsin();

    //if ((j = fft_init(npts)) != 0)  return j;

    //n_2 = npts/2;
    //n_4 = n_2/2;
    x[0] *= smp;
    smp += 1.0;

    for (i = 1, j = fp->n_4 - 2; j >= 0; i++, j -= 2)
    {
        tmp = (smp - fp->fftbtsin[j]);
        x[i] *= tmp;
        x[fp->nx - i] *= tmp;
    }

    for (i = fp->n_4 / 2, j = 0; i < fp->n_4 ; i++, j += 2)
    {
        tmp = (smp + fp->fftbtsin[j]);
        x[i] *= tmp;
        x[fp->nx - i] *= tmp;
    }

    return 0;
}
int change_mouse_to_cross(int cl, int cw)
{
    BITMAP *ds_bitmap = NULL;
    int color, cw2, cl2;

    cl2 = cl / 2;
    cw2 = cw / 2;
    ds_bitmap = create_bitmap(cl + 1, cl + 1);
    cl = Lightmagenta;
    color = makecol(EXTRACT_R(cl), EXTRACT_G(cl), EXTRACT_B(cl));
    clear_to_color(ds_bitmap, color);

    color = makecol(255, 0, 0);
    acquire_bitmap(ds_bitmap);
    line(ds_bitmap, 0, cl2 - cw2, 0, cl2 + cw2, color);
    line(ds_bitmap, cl - 1, cl2 - cw2, cl - 1, cl2 + cw2, color);

    line(ds_bitmap, cl2 - cw2, 0, cl2 + cw2, 0, color);
    line(ds_bitmap, cl2 - cw2, cl - 1, cl2 + cw2, cl - 1, color);

    line(ds_bitmap, 0, cl2 - cw2, cl2 - cw2, cl2 - cw2, color);
    line(ds_bitmap, cl, cl2 - cw2, cl2 + cw2, cl2 - cw2, color);
    line(ds_bitmap, 0, cl2 + cw2, cl2 - cw2, cl2 + cw2, color);
    line(ds_bitmap, cl, cl2 + cw2, cl2 + cw2, cl2 + cw2, color);

    line(ds_bitmap, cl2 - cw2, 0, cl2 - cw2, cl2 - cw2, color);
    line(ds_bitmap, cl2 + cw2, 0, cl2 + cw2, cl2 - cw2, color);
    line(ds_bitmap, cl2 - cw2, cl, cl2 - cw2, cl2 + cw2, color);
    line(ds_bitmap, cl2 + cw2, cl, cl2 + cw2, cl2 + cw2, color);
    release_bitmap(ds_bitmap);
    scare_mouse();
    set_mouse_sprite(ds_bitmap);
    set_mouse_sprite_focus(cl2, cl2);
    unscare_mouse();
    return 0;
}
int reset_mouse(void)
{
    scare_mouse();
    set_mouse_sprite(NULL);
    unscare_mouse();
    return 0;
}


/*  Find maximum position in a 1d array, the array is supposed periodic
 *  return 0 -> everything fine, TOO_MUCH_NOISE or PB_WITH_EXTREMUM
 *
 */
int find_max1(float *x, int nx, float *Max_pos, float *Max_val, float *Max_deriv, int max_limit)
{
    register int i;
    int  nmax, ret = 0, delta, imin, imax;
    double a, b, c, d, f_2, f_1, f_0, f1, f2, xm = 0;
    int maxs_pos[PROFILE_BUFFER_SIZE];
    int nmaxs = 0;

    if (max_limit < 2) max_limit = nx / 2;

    imin = nx / 2 - max_limit;
    imin = (imin < 0) ? 0 : imin;
    imax = nx / 2 + max_limit;
    imax = (imax >= nx) ? nx : imax;

    maxs_pos[0] = 0;
    // find maximum, handle several points with the same maxima value.
    // TODO : handle correctly the case where maximums are not neighbours
    // find the first maximum
    for (i = imin, nmaxs = 1; i < imax; i++)
    {
      maxs_pos[0] = (i == imin || x[i] > x[maxs_pos[0]]) ? i : maxs_pos[0];
    }
    // then find points having the same value (saturation)
    for (i = imin; i < imax; i++)
    {
        if (i == maxs_pos[0]) continue;
        if (x[i] == x[maxs_pos[0]] && nmaxs < PROFILE_BUFFER_SIZE)
        {
            maxs_pos[nmaxs++] = i;
        }
    }
    // take the average position of the maximum
    for (i = 0, nmax = 0; i < nmaxs; ++i)
    {
        nmax += maxs_pos[i];
    }
    nmax /= nmaxs;

    // get neighbourhood
    f_2 = x[(nx + nmax - 2) % nx];
    f_1 = x[(nx + nmax - 1) % nx];
    f_0 = x[nmax];
    f1 = x[(nmax + 1) % nx];
    f2 = x[(nmax + 2) % nx];

    // polinomiale interpolation
    if (f_1 < f1)
    {
        a = -f_1 / 6 + f_0 / 2 - f1 / 2 + f2 / 6;
        b = f_1 / 2 - f_0 + f1 / 2;
        c = -f_1 / 3 - f_0 / 2 + f1 - f2 / 6;
        d = f_0;
        delta = 0;
    }
    else
    {
        a = b = c = 0;
        a = -f_2 / 6 + f_1 / 2 - f_0 / 2 + f1 / 6;
        b = f_2 / 2 - f_1 + f_0 / 2;
        c = -f_2 / 3 - f_1 / 2 + f_0 - f1 / 6;
        d = f_1;
        delta = -1;
    }


    if (fabs(a) < 1e-8)
    {
        if (b != 0)
        {
            xm =  - c / (2 * b) - (3 * a * c * c) / (4 * b * b * b);
        }
        else
        {
            xm = 0;
            ret |= PB_WITH_EXTREMUM;
        }
    }
    else if ((b * b - 3 * a * c) < 0)
    {
        ret |= PB_WITH_EXTREMUM;
        xm = 0;
    }
    else
        xm = (-b - sqrt(b * b - 3 * a * c)) / (3 * a);

    /*
       for (p = -2; p < 1; p++)
       {
       if (6*a*p+b > 0) ret |= TOO_MUCH_NOISE;
       }
       */
    *Max_pos = (float)(xm + nmax + delta);
    *Max_val = (float)((a * xm * xm * xm) + (b * xm * xm) + (c * xm) + d);

    if (Max_deriv)    *Max_deriv = (float)((6 * a * xm) + (2 * b));

    return ret;
}



/*      correlate "x1" with its invert over "nx" points, returns the correlation
 *      in "fx1". "fx1" will be used to compute fft and result
 *      setting them to NULL will allocate space. "window" will enable
 *      a windowing process if != 0 a double windowing if = to 2.
 */
int correlate_1d_sig_and_invert(float *x1, int nx, int window, float *fxl1, int lfilter, int remove_dc, fft_plan *fp,
                                filter_bt *fil, float *noise)
{
    int i, j;
    int n_2;
    float moy, smp = 0.05, tmpf;
    float m1, m2, ms, md, re1, re2, im1, im2;

    if (x1 == NULL)             return WRONG_ARGUMENT;

    if (fp == NULL || fil == NULL)          return FFT_NOT_SUPPORTED;

    if (fftbt_init(fp, nx) == NULL)         return FFT_NOT_SUPPORTED;

    //if (fft_init(nx) || filter_init(nx))  return FFT_NOT_SUPPORTED;
    if (fxl1 == NULL)   fxl1 = (float *)calloc(nx, sizeof(float));

    if (fxl1 == NULL)           return OUT_MEMORY;

    n_2 = nx / 2;

    /*  compute fft of x1 */
    for (i = 0; i < nx; i++)
        fxl1[i] = x1[i];

    for (i = 0, j = (3 * nx) / 4, moy = 0; i < nx / 4; i++, j++)
        moy += fxl1[i] + fxl1[j];

    moy = (2 * moy) / nx;

    for (i = 0 ; remove_dc && i < nx; i++)
        fxl1[i] = x1[i] - moy;

    if (window == 1)    fftbtwindow1(fp, fxl1, smp);
    else if (window == 2)   fftwindow_flat_top1(fp, nx, fxl1, smp);

    //for(i = 1, dfxl1[0] = fxl1[0] - fxl1[nx-1]; i < nx; i++)      dfxl1[i] = fxl1[i] - fxl1[i-1];


    realtr1bt(fp, fxl1);
    //for(i = 0; i < nx; i++)       x1[i] = fxl1[i];
    //return 0;
    //win_printf("bef fft fxl1[3] = %f moy %f nx %d filter %d",fxl1[3],moy,nx,filter);
    fftbt(fp, fxl1, 1);
    realtr2bt(fp, fxl1, 1);


    if (noise && (noise_from_auto_conv == 0))
    {
        // we estimate noise on convolution by using the high frequency modes
        for (j = 0, i = 3 * nx / 4, m1 = 0; i < nx; i++, j++)
            m1 += fxl1[i] * fxl1[i];

        m1 = (j) ? m1 / j : m1;
        m1 *= n_2;   // we extend the noise to all modes
        *noise = sqrt(m1);
    }

    //we filter low modes
    for (i = 1; i < rm_low_modes_in_auto_convolution; i++)
        fxl1[i] = 0;

    if (lfilter > 0 && filter_after_conv == 0)
        lowpass_smooth_half_bt(fil, nx, fxl1, lfilter);

    /*  compute normalization without DC and Nyquist*/
    for (i = 0, ms = md = 0; i < nx; i += 2)
    {
        tmpf = fxl1[i] * fxl1[i] + fxl1[i + 1] * fxl1[i + 1];
        ms += tmpf;
        md += tmpf * i * i;  // we compute derivative amplitude
    }

    md *= (M_PI * M_PI) / (nx * nx);
    //m1 /= 2;
    m1 = (ms == 0) ? 1 : ms;

    if (auto_conv_norm == 0)      m1 = 1;
    else if (auto_conv_norm == 1) m1 = (float)1 / m1;
    else if (auto_conv_norm == 2) m1 = (float)1 / sqrt(m1);

    /* if m1 = 0, the normalization still needs to multiply by nx/2 to reach \Sigma_i=1^N Si * Si'
       if we assume shot noise in the profile with variance \sigma_n, the error
       on the convolution error is \sigma_c^2 = 4 * \Sigma_i=1^N Si * Si' * \sigma_n^2

       To determine in the error on the position of the maximum, one needs to compute
       the derivative of the maximum and the noise of this derirative, the error on this
       derivatives equals \sigma_d^2 = 4 * \Sigma_i=1^N \Delta Si * \Delta Si' * \sigma_n^2
       without filtering.
       */

    /*  compute correlation in Fourier space */
    for (i = 2; i < nx; i += 2)
    {
        re1 = fxl1[i];
        re2 = fxl1[i];
        im1 = fxl1[i + 1];
        im2 = -fxl1[i + 1];
        fxl1[i]         = (re1 * re2 + im1 * im2) * m1;
        fxl1[i + 1]   = (im1 * re2 - re1 * im2) * m1;
    }

    fxl1[0] = 2 * (fxl1[0] * fxl1[0]) * m1; /* these too mode are special */
    fxl1[1]     = 0;//2*(fxl1[1] * fxl1[1])/m1;   may be to correct with remove_dc

    if (noise && noise_from_auto_conv ==  1)
    {
        // we estimate noise on convolution by using the high frequency modes
        for (j = 0, i = 3 * nx / 4, m1 = 0; i < nx; i++, j++)
            m1 += fxl1[i] * fxl1[i];

        m1 = (j) ? m1 / j : m1;
        m1 *= n_2;   // we extend the noise to all modes
    }

    if (noise && noise_from_auto_conv == 2)
    {
        // we estimate noise using the derivative of the autoconvolution
        if (auto_conv_norm == 0)      m1 = md;
        else if (auto_conv_norm == 1) m1 = md / ms;
        else if (auto_conv_norm == 2) m1 = md / sqrt(ms);

        m1 = 4 * m1 / n_2;
    }

    if (lfilter > 0 && filter_after_conv)
        lowpass_smooth_half_bt(fil, nx, fxl1, lfilter);

    if (noise && noise_from_auto_conv)
    {
        if (lfilter > 0)
        {
            for (i = 0, m2 = 0; i < n_2; i++)
                m2 += fil->f[i];// * fil->f[i];

            m2 /= n_2;
            m1 *= m2;           // we take in account the filter
        }

        *noise = sqrt(m1);
    }

    /*  get back to real world */
    realtr2bt(fp, fxl1, -1);
    //win_printf("bef 2 fft fxl1[2] = %f",fxl1[2]);
    fftbt(fp, fxl1, -1);
    realtr1bt(fp, fxl1);
    deplace(fxl1, nx);
    return 0;
}

# ifdef PIAS


/*		correlate "x1" with its invert over "nx" points, returns the correlation
 *		in "fx1". "fx1" will be used to compute fft and result
 *		setting them to NULL will allocate space. "window" will enable
 *		a windowing process if != 0 a double windowing if = to 2.
 */
int	correlate_1d_sig_and_invert_lp_and_bp(float *x1, int nx, int window, float *fxl1, int lp_filter, int bp_filter, int bp_width, int remove_dc, fft_plan *fp, filter_bt *fillp, filter_bt *filbp, float *noise)
{
    register int i, j;
    int n_2;
    float moy, smp = 0.05, tmpf;
    float m1, m2, ms, md, re1, re2, im1, im2;
    static float *cfx1 = NULL;
    static int n_cfx1 = 0;

    if (x1 == NULL)				return WRONG_ARGUMENT;
    if (fp == NULL || fillp == NULL || filbp == NULL)          return FFT_NOT_SUPPORTED;
    if (fftbt_init(fp, nx) == NULL)         return FFT_NOT_SUPPORTED;
    //if (fft_init(nx) || filter_init(nx))	return FFT_NOT_SUPPORTED;
    if (fxl1 == NULL)	fxl1 = (float*)calloc(nx,sizeof(float));
    if (fxl1 == NULL)			return OUT_MEMORY;
    if (n_cfx1 > 0 && n_cfx1 < nx)
      {
	if (cfx1 != NULL) free(cfx1);
	cfx1 = NULL;
	n_cfx1 = 0;
      }
    if (cfx1 == NULL)
      {
	cfx1 = (float*)calloc(nx,sizeof(float));
	if (cfx1 == NULL)			return OUT_MEMORY;
	n_cfx1 = nx;
      }


    n_2 = nx/2;

    // we cleam mean values
    for(i = 0; i < nx; i++)		fxl1[i] = x1[i];
    for(i = 0, j = (3*nx)/4, moy = 0; i < nx/4; i++, j++)
        moy += fxl1[i] + fxl1[j];
    for(i = 0, moy = (2*moy)/nx; remove_dc && i < nx; i++)
        fxl1[i] = x1[i] - moy;
    if (window == 1)	fftbtwindow1(fp, fxl1, smp);
    else if (window == 2)	fftwindow_flat_top1(fp, nx, fxl1, smp);

    /*	compute fft of x1 as real data */
    realtr1bt(fp, fxl1);
    fftbt(fp, fxl1, 1);
    realtr2bt(fp, fxl1, 1);


    if (noise && (noise_from_auto_conv == 0))
    {    // we estimate noise on convolution by using the high frequency modes
        for (j = 0, i=3*nx/4, m1 = 0; i< nx; i++, j++)
            m1 += fxl1[i] * fxl1[i];
        m1 = (j) ? m1/j : m1;
        m1 *= n_2;   // we extend the noise to all modes
        *noise = sqrt(m1);
    }


    //we filter low modes
    //for (i = 1; i < rm_low_modes_in_auto_convolution; i++)    fxl1[i] = 0;


    if (filter_after_conv == 0)
      {
	for (j = 0; j< nx; j++) cfx1[j] = fxl1[j];
	if (lp_filter > 0 )  lowpass_smooth_half_bt(fillp, nx, fxl1, lp_filter);
	if (bp_filter > 0 && bp_width > 0)
	  bandpass_smooth_half_bt (filbp, nx, cfx1, bp_filter, bp_width);
	if (lp_filter <= 0 ) for (j = 0; j< nx; j++) fxl1[j] = 0;
	if (bp_filter > 0 && bp_width > 0) for (j = 0; j< nx; j++) fxl1[j] += cfx1[j];
      }
    /*	compute normalization without DC and Nyquist*/
    for (i=0, ms = md = 0; i< nx; i+=2)
    {
        tmpf = fxl1[i] * fxl1[i] + fxl1[i+1] * fxl1[i+1];
        ms += tmpf;
        md += tmpf * i * i;  // we compute derivative amplitude
    }
    md *= (M_PI * M_PI)/(nx * nx);
    //m1 /= 2;
    m1 = (ms == 0) ? 1 : ms;
    if (auto_conv_norm == 0)      m1 = 1;
    else if (auto_conv_norm == 1) m1 = (float)1/m1;
    else if (auto_conv_norm == 2) m1 = (float)1/sqrt(m1);

    /* if m1 = 0, the normalization still needs to multiply by nx/2 to reach \Sigma_i=1^N Si * Si'
       if we assume shot noise in the profile with variance \sigma_n, the error
       on the convolution error is \sigma_c^2 = 4 * \Sigma_i=1^N Si * Si' * \sigma_n^2

       To determine in the error on the position of the maximum, one needs to compute
       the derivative of the maximum and the noise of this derirative, the error on this
       derivatives equals \sigma_d^2 = 4 * \Sigma_i=1^N \Delta Si * \Delta Si' * \sigma_n^2
       without filtering.
       */

    /*	compute correlation in Fourier space */
    for (i=2; i< nx; i+=2)
    {
        re1 = fxl1[i];		re2 = fxl1[i];
        im1 = fxl1[i+1];		im2 = -fxl1[i+1];
        fxl1[i] 		= (re1 * re2 + im1 * im2)*m1;
        fxl1[i+1] 	= (im1 * re2 - re1 * im2)*m1;
    }
    fxl1[0]	= 2*(fxl1[0] * fxl1[0])*m1;	/* these too mode are special */
    fxl1[1] 	= 0;//2*(fxl1[1] * fxl1[1])/m1;   may be to correct with remove_dc

    if (noise && noise_from_auto_conv ==  1)
    {    // we estimate noise on convolution by using the high frequency modes
        for (j = 0, i=3*nx/4, m1 = 0; i< nx; i++, j++)
            m1 += fxl1[i] * fxl1[i];
        m1 = (j) ? m1/j : m1;
        m1 *= n_2;   // we extend the noise to all modes
    }

    if (noise && noise_from_auto_conv == 2)
    {    // we estimate noise using the derivative of the autoconvolution
        if (auto_conv_norm == 0)      m1 = md;
        else if (auto_conv_norm == 1) m1 = md/ms;
        else if (auto_conv_norm == 2) m1 = md/sqrt(ms);
        m1 = 4*m1/n_2;
    }


    if (filter_after_conv)
      {
	for (j = 0; j< nx; j++) cfx1[j] = fxl1[j];
	if (lp_filter > 0 )  lowpass_smooth_half_bt(fillp, nx, fxl1, lp_filter);
	if (bp_filter > 0 && bp_width > 0)
	  bandpass_smooth_half_bt (filbp, nx, cfx1, bp_filter, bp_width);
	if (lp_filter <= 0 ) for (j = 0; j< nx; j++) fxl1[j] = 0;
      }

    if (noise && noise_from_auto_conv)
    {
      if (lp_filter > 0 )
	{
	  for (i = 0, m2 = 0; i < n_2; i++)
	    m2 += fillp->f[i];// * fil->f[i];
	}
      if (bp_filter > 0 && bp_width > 0)
	{
	  for (i = 0, m2 = 0; i < n_2; i++)
	    m2 += filbp->f[i];// * fil->f[i];
	}
      m2 /= n_2;
      m1 *= m2;           // we take in account the filter
      *noise = sqrt(m1);
    }
    /*	get back to real world */
    realtr2bt(fp, fxl1, -1);
    fftbt(fp, fxl1, -1);
    realtr1bt(fp, fxl1);
    deplace(fxl1, nx);
    return 0;
}





/*		correlate "x1" with its invert over "nx" points, returns the correlation
 *		in "fx1". "fx1" will be used to compute fft and result
 *		setting them to NULL will allocate space. "window" will enable
 *		a windowing process if != 0 a double windowing if = to 2.
 */
int	correlate_1d_sig_and_invert_bp_and_complexify(float *x1, int nx, int window, float *fxl1, float *fcx1, int bp_filter, int bp_width, int remove_dc, fft_plan *fp, filter_bt *filbp, float *noise)
{
    register int i, j;
    int n_2;
    float moy, smp = 0.05, tmpf;
    float m1, m2 = 0, ms, md, re1, re2, im1, im2;

    if (x1 == NULL)				return WRONG_ARGUMENT;
    if (fp == NULL || filbp == NULL)          return FFT_NOT_SUPPORTED;
    if (fftbt_init(fp, nx) == NULL)         return FFT_NOT_SUPPORTED;
    //if (fft_init(nx) || filter_init(nx))	return FFT_NOT_SUPPORTED;
    if (fxl1 == NULL)	fxl1 = (float*)calloc(nx,sizeof(float));
    if (fxl1 == NULL)			return OUT_MEMORY;
    if (fcx1 == NULL)	fcx1 = (float*)calloc(nx,sizeof(float));
    if (fcx1 == NULL)			return OUT_MEMORY;

    n_2 = nx/2;

    // we cleam mean values
    for(i = 0; i < nx; i++)		fxl1[i] = x1[i];
    for(i = 0, j = (3*nx)/4, moy = 0; i < nx/4; i++, j++)
        moy += fxl1[i] + fxl1[j];
    for(i = 0, moy = (2*moy)/nx; remove_dc && i < nx; i++)
        fxl1[i] = x1[i] - moy;
    if (window == 1)	fftbtwindow1(fp, fxl1, smp);
    else if (window == 2)	fftwindow_flat_top1(fp, nx, fxl1, smp);

    /*	compute fft of x1 as real data */
    realtr1bt(fp, fxl1);
    fftbt(fp, fxl1, 1);
    realtr2bt(fp, fxl1, 1);


    if (noise && (noise_from_auto_conv == 0))
    {    // we estimate noise on convolution by using the high frequency modes
        for (j = 0, i=3*nx/4, m1 = 0; i< nx; i++, j++)
            m1 += fxl1[i] * fxl1[i];
        m1 = (j) ? m1/j : m1;
        m1 *= n_2;   // we extend the noise to all modes
        *noise = sqrt(m1);
    }
    //we filter low modes
    //for (i = 1; i < rm_low_modes_in_auto_convolution; i++)    fxl1[i] = 0;
    if (filter_after_conv == 0)
      {
	if (bp_filter > 0 && bp_width > 0)
	  bandpass_smooth_half_bt (filbp, nx, fxl1, bp_filter, bp_width);
      }

    /*	compute normalization without DC and Nyquist*/
    for (i=0, ms = md = 0; i< nx; i+=2)
    {
        tmpf = fxl1[i] * fxl1[i] + fxl1[i+1] * fxl1[i+1];
        ms += tmpf;
        md += tmpf * i * i;  // we compute derivative amplitude
    }
    md *= (M_PI * M_PI)/(nx * nx);
    //m1 /= 2;
    m1 = (ms == 0) ? 1 : ms;
    if (auto_conv_norm == 0)      m1 = 1;
    else if (auto_conv_norm == 1) m1 = (float)1/m1;
    else if (auto_conv_norm == 2) m1 = (float)1/sqrt(m1);

    /* if m1 = 0, the normalization still needs to multiply by nx/2 to reach \Sigma_i=1^N Si * Si'
       if we assume shot noise in the profile with variance \sigma_n, the error
       on the convolution error is \sigma_c^2 = 4 * \Sigma_i=1^N Si * Si' * \sigma_n^2

       To determine in the error on the position of the maximum, one needs to compute
       the derivative of the maximum and the noise of this derirative, the error on this
       derivatives equals \sigma_d^2 = 4 * \Sigma_i=1^N \Delta Si * \Delta Si' * \sigma_n^2
       without filtering.
       */

    /*	compute correlation in Fourier space */
    for (i=2; i< nx; i+=2)
    {
        re1 = fxl1[i];		re2 = fxl1[i];
        im1 = fxl1[i+1];		im2 = -fxl1[i+1];
        fxl1[i] 		= (re1 * re2 + im1 * im2)*m1;
        fxl1[i+1] 	= (im1 * re2 - re1 * im2)*m1;
    }
    fxl1[0]	= 2*(fxl1[0] * fxl1[0])*m1;	/* these too mode are special */
    fxl1[1] 	= 0;//2*(fxl1[1] * fxl1[1])/m1;   may be to correct with remove_dc

    if (noise && noise_from_auto_conv ==  1)
    {    // we estimate noise on convolution by using the high frequency modes
        for (j = 0, i=3*nx/4, m1 = 0; i< nx; i++, j++)
            m1 += fxl1[i] * fxl1[i];
        m1 = (j) ? m1/j : m1;
        m1 *= n_2;   // we extend the noise to all modes
    }

    if (noise && noise_from_auto_conv == 2)
    {    // we estimate noise using the derivative of the autoconvolution
        if (auto_conv_norm == 0)      m1 = md;
        else if (auto_conv_norm == 1) m1 = md/ms;
        else if (auto_conv_norm == 2) m1 = md/sqrt(ms);
        m1 = 4*m1/n_2;
    }

    if (filter_after_conv)
      {
	if (bp_filter > 0 && bp_width > 0)
	  bandpass_smooth_half_bt (filbp, nx, fxl1, bp_filter, bp_width);
      }

    if (noise && noise_from_auto_conv)
    {
      if (bp_filter > 0 )
	{
	  for (i = 0, m2 = 0; i < n_2; i++)
	    m2 += filbp->f[i];// * fil->f[i];
	}
      m2 /= n_2;
      m1 *= m2;           // we take in account the filter
      *noise = sqrt(m1);
    }

    /*	get back to complex world */
    for (j = 2;j < nx; j+=2)
      {
	fcx1[j+1] = fxl1[j];
	fcx1[j] = -fxl1[j+1];
      }
    fcx1[1] = fcx1[0] = 0;
    /*	get back to real world */
    realtr2bt(fp, fxl1, -1);
    fftbt(fp, fxl1, -1);
    realtr1bt(fp, fxl1);
    deplace(fxl1, nx);
    realtr2bt(fp, fcx1, -1);
    fftbt(fp, fcx1, -1);
    realtr1bt(fp, fcx1);
    deplace(fcx1, nx);
    return 0;
}


/*		correlate "x1" with its invert over "nx" points, returns the correlation
 *		in "fx1". "fx1" will be used to compute fft and result
 *		setting them to NULL will allocate space. "window" will enable
 *		a windowing process if != 0 a double windowing if = to 2.
 */
int	correlate_1d_sig_and_invert_lp_and_complexify(float *x1, int nx, int window, float *fxl1, float *fcx1, int lp_filter, int remove_dc, fft_plan *fp, filter_bt *fillp, float *noise)
{
    register int i, j;
    int n_2;
    float moy, smp = 0.05, tmpf;
    float m1, m2 = 0, ms, md, re1, re2, im1, im2;


    if (x1 == NULL)				return WRONG_ARGUMENT;
    if (fp == NULL || fillp == NULL)          return FFT_NOT_SUPPORTED;
    if (fftbt_init(fp, nx) == NULL)         return FFT_NOT_SUPPORTED;
    //if (fft_init(nx) || filter_init(nx))	return FFT_NOT_SUPPORTED;
    if (fxl1 == NULL)	fxl1 = (float*)calloc(nx,sizeof(float));
    if (fxl1 == NULL)			return OUT_MEMORY;
    if (fcx1 == NULL)	fcx1 = (float*)calloc(nx,sizeof(float));
    if (fcx1 == NULL)			return OUT_MEMORY;


    n_2 = nx/2;

    // we cleam mean values
    for(i = 0; i < nx; i++)		fxl1[i] = x1[i];
    for(i = 0, j = (3*nx)/4, moy = 0; i < nx/4; i++, j++)
        moy += fxl1[i] + fxl1[j];
    for(i = 0, moy = (2*moy)/nx; remove_dc && i < nx; i++)
        fxl1[i] = x1[i] - moy;
    if (window == 1)	fftbtwindow1(fp, fxl1, smp);
    else if (window == 2)	fftwindow_flat_top1(fp, nx, fxl1, smp);

    /*	compute fft of x1 as real data */
    realtr1bt(fp, fxl1);
    fftbt(fp, fxl1, 1);
    realtr2bt(fp, fxl1, 1);


    if (noise && (noise_from_auto_conv == 0))
    {    // we estimate noise on convolution by using the high frequency modes
        for (j = 0, i=3*nx/4, m1 = 0; i< nx; i++, j++)
            m1 += fxl1[i] * fxl1[i];
        m1 = (j) ? m1/j : m1;
        m1 *= n_2;   // we extend the noise to all modes
        *noise = sqrt(m1);
    }


    //we filter low modes
    //for (i = 1; i < rm_low_modes_in_auto_convolution; i++)    fx1[i] = 0;


    if (filter_after_conv == 0)
      {
	if (lp_filter > 0 )  lowpass_smooth_half_bt(fillp, nx, fxl1, lp_filter);
      }
    /*	compute normalization without DC and Nyquist*/
    for (i=0, ms = md = 0; i< nx; i+=2)
    {
        tmpf = fxl1[i] * fxl1[i] + fxl1[i+1] * fxl1[i+1];
        ms += tmpf;
        md += tmpf * i * i;  // we compute derivative amplitude
    }
    md *= (M_PI * M_PI)/(nx * nx);
    //m1 /= 2;
    m1 = (ms == 0) ? 1 : ms;
    if (auto_conv_norm == 0)      m1 = 1;
    else if (auto_conv_norm == 1) m1 = (float)1/m1;
    else if (auto_conv_norm == 2) m1 = (float)1/sqrt(m1);

    /* if m1 = 0, the normalization still needs to multiply by nx/2 to reach \Sigma_i=1^N Si * Si'
       if we assume shot noise in the profile with variance \sigma_n, the error
       on the convolution error is \sigma_c^2 = 4 * \Sigma_i=1^N Si * Si' * \sigma_n^2

       To determine in the error on the position of the maximum, one needs to compute
       the derivative of the maximum and the noise of this derirative, the error on this
       derivatives equals \sigma_d^2 = 4 * \Sigma_i=1^N \Delta Si * \Delta Si' * \sigma_n^2
       without filtering.
       */

    /*	compute correlation in Fourier space */
    for (i=2; i< nx; i+=2)
    {
        re1 = fxl1[i];		re2 = fxl1[i];
        im1 = fxl1[i+1];		im2 = -fxl1[i+1];
        fxl1[i] 		= (re1 * re2 + im1 * im2)*m1;
        fxl1[i+1] 	= (im1 * re2 - re1 * im2)*m1;
    }
    fxl1[0]	= 2*(fxl1[0] * fxl1[0])*m1;	/* these too mode are special */
    fxl1[1] 	= 0;//2*(fxl1[1] * fxl1[1])/m1;   may be to correct with remove_dc

    if (noise && noise_from_auto_conv ==  1)
    {    // we estimate noise on convolution by using the high frequency modes
        for (j = 0, i=3*nx/4, m1 = 0; i< nx; i++, j++)
            m1 += fxl1[i] * fxl1[i];
        m1 = (j) ? m1/j : m1;
        m1 *= n_2;   // we extend the noise to all modes
    }

    if (noise && noise_from_auto_conv == 2)
    {    // we estimate noise using the derivative of the autoconvolution
        if (auto_conv_norm == 0)      m1 = md;
        else if (auto_conv_norm == 1) m1 = md/ms;
        else if (auto_conv_norm == 2) m1 = md/sqrt(ms);
        m1 = 4*m1/n_2;
    }


    if (filter_after_conv)
      {
	if (lp_filter > 0 )  lowpass_smooth_half_bt(fillp, nx, fxl1, lp_filter);
      }

    if (noise && noise_from_auto_conv)
    {
      if (lp_filter > 0 )
	{
	  for (i = 0, m2 = 0; i < n_2; i++)
	    m2 += fillp->f[i];// * fil->f[i];
	}
      m2 /= n_2;
      m1 *= m2;           // we take in account the filter
      *noise = sqrt(m1);
    }

    /*	get back to complex world */
    for (j = 2;j < nx; j+=2)
      {
	fcx1[j+1] = fxl1[j];
	fcx1[j] = -fxl1[j+1];
      }
    fcx1[1] = fcx1[0] = 0;
    /*	get back to real world */
    realtr2bt(fp, fxl1, -1);
    fftbt(fp, fxl1, -1);
    realtr1bt(fp, fxl1);
    deplace(fxl1, nx);
    realtr2bt(fp, fcx1, -1);
    fftbt(fp, fcx1, -1);
    realtr1bt(fp, fcx1);
    deplace(fcx1, nx);
    return 0;
}

# endif

float   find_distance_from_center(float *x1, int cl, int flag, int lfilter, int black_circle, float *corr, float *deriv,
                                  fft_plan *fp, filter_bt *fil, float *noise, int max_limit)
{
    float dx = 0;
    (void)black_circle;
    correlate_1d_sig_and_invert(x1, cl, flag, fx1, lfilter, 1, fp, fil, noise);//(black_circle)?0:1);
    find_max1(fx1, cl, &dx, corr, deriv, max_limit);
    dx -= cl / 2;
    dx /= 2;
    return dx;
}

float   find_distance_from_center_2(float *x1, float *lfx1, int cl, int flag, int lfilter, int black_circle,
                                    float *corr, float *deriv, fft_plan *fp, filter_bt *fil, float *noise, int max_limit)
{
    float dx = 0;
    (void)black_circle;
    correlate_1d_sig_and_invert(x1, cl, flag, lfx1, lfilter, 1, fp, fil, noise);//(black_circle)?0:1);
    find_max1(lfx1, cl, &dx, corr, deriv, max_limit);
    dx -= cl / 2;
    dx /= 2;
    return dx;
}


# ifndef PIAS


int	draw_moving_shear_rect_vga_screen_unit(int xt, int yt, // top fringes center
					       int xb, int yb, // bottom fringes center
					       int nfx, int nfy, // fringes size
					       int maxdx,    // maxium dx allowed
					       int dfy0, int dym,// fringes distance, typical and max dy
					       int color, int scale, int bead_nb,  BITMAP* bm, float *quality_factor)
{
    int x1, x2, x3, x4, y1, y2, y3, y4;
    //int tmp;
    int xcs, ycs;
    int l = (28 * (nfx >> 1)) / scale;
    int w = (28 * (nfy >> 1)) / scale;
    (void)maxdx;
    (void)dfy0;
    (void)dym;
  //
  //   if ((xt - xb) > maxdx)
  //     {
	// tmp = (xt + xb + maxdx)/2;
	// xb = (xt + xb - maxdx)/2;
	// xt = tmp;
  //     }
  //   else if ((xb - xt) > maxdx)
  //     {
	// tmp = (xt + xb + maxdx)/2;
	// xt = (xt + xb - maxdx)/2;
	// xb = tmp;
  //     }
  //   if ((yt - yb) > dfy0 + dym)
  //     {
	// tmp = (yt + yb + dfy0 + dym)/2;
	// yb = (yt + yb - dfy0 - dym)/2;
	// yt = tmp;
  //     }
  //   if ((yt - yb) < dfy0 - dym)
  //     {
	// tmp = (yt + yb + dfy0 - dym)/2;
	// yb = (yt + yb - dfy0 + dym)/2;
	// yt = tmp;
  //     }

    xcs = (28 * xt) / scale;
    ycs = bm->h - (28 * yt) / scale;
    x1 =  xcs - l;
    x2 =  xcs + l;
    y1 =  ycs - w;
    y2 =  ycs + w;

    line(bm, x1, y1, x2, y1, color);
    line(bm, x2, y1, x2, y2, color);
    line(bm, x2, y2, x1, y2, color);
    line(bm, x1, y2, x1, y1, color);
    line(bm, xcs, y1, xcs, y2, color);

    xcs = (28 * xb) / scale;
    ycs = bm->h - (28 * yb) / scale;
    x3 =  xcs - l;
    x4 =  xcs + l;
    y3 =  ycs - w;
    y4 =  ycs + w;

    line(bm, x3, y3, x4, y3, color);
    line(bm, x4, y3, x4, y4, color);
    line(bm, x2, y4, x3, y4, color);
    line(bm, x3, y4, x3, y3, color);
    line(bm, xcs, y3, xcs, y4, color);

    line(bm, (x1+x4)/2, y3, (x1+x4)/2, y2, color);
    textprintf_centre_ex(bm, large_font, xcs, ycs+45, color, -1, "%d", bead_nb);
    if (quality_factor)
    {
    textprintf_centre_ex(bm, large_font, xcs, ycs+55, color, -1, "%f", quality_factor[0]);
    textprintf_centre_ex(bm, large_font, xcs, ycs+70, color, -1, "%f", quality_factor[1]);
  }



    return 0;
}




int draw_cross_in_screen_unit(int xc, int yc, int length, int width, int color, BITMAP *bmp, int scale, int bead_nb,
                              int fixed, char *zpos, int zcolor)
{
    // bead pixel position

    int l = (28 * (length >> 1)) / scale;
    int w = (28 * (width >> 1)) / scale;
    int y = bmp->h - (28 * yc) / scale;
    int xcs = (28 * xc) / scale;
    int x1 =  xcs - l;
    int x4 =  xcs + l;
    int x2 =  xcs - w;
    int x3 =  xcs + w;
    int y1 =  y + l;
    int y4 =  y - l;
    int y2 =  y + w;
    int y3 =  y - w;
    //char bd[8];

    //sprintf(bd,"%d",bead_nb);
    acquire_bitmap(bmp);
    hline(bmp, x1, y2, x2, color);
    vline(bmp, x2, y2, y1, color);
    hline(bmp, x2, y1, x3, color);
    vline(bmp, x3, y1, y2, color);
    hline(bmp, x3, y2, x4, color);
    vline(bmp, x4, y2, y3, color);
    hline(bmp, x3, y3, x4, color);
    vline(bmp, x3, y3, y4, color);
    hline(bmp, x2, y4, x3, color);
    vline(bmp, x2, y3, y4, color);
    hline(bmp, x1, y3, x2, color);
    vline(bmp, x1, y2, y3, color);
    release_bitmap(bmp);
    //switch_allegro_font (big_font);
    textprintf_centre_ex(bmp, large_font, xcs, y, color, -1, "%d", bead_nb);

    if (fixed)
        textprintf_centre_ex(bmp, normalsize_font, xcs - 3 * w, y - 5 * w, color, -1, "F");

    if (zpos != NULL)
      textprintf_ex(bmp, normalsize_font, xcs + w + w, y + w + w, zcolor, -1, "%s",zpos);

    //alfont_textout(bmp,user_font,bd,xcs,y, color);
    return 0;
}




int draw_X_cross_in_screen_unit(int xc, int yc, int length, int width, int color,  BITMAP *bmp, int scale, int bead_nb,
                                int fixed, char *zpos, int zcolor)
{
    int l = (28 * ((length + width) >> 1)) / scale;
    int w = (28 * ((length - width) >> 1)) / scale;
    int w2 = (28 * width) / scale;
    int y = bmp->h - (28 * yc) / scale;
    int xcs = (28 * xc) / scale;

    int x1 = xcs - l;
    int x2 = xcs - w;
    int x3 = xcs - w2;
    int x4 = xcs;
    int x5 = xcs + w2;
    int x6 = xcs + w;
    int x7 = xcs + l;

    int y1 = y + l;
    int y2 = y + w;
    int y3 = y + w2;
    int y4 = y;
    int y5 = y - w2;
    int y6 = y - w;
    int y7 = y - l;
    acquire_bitmap(bmp);
    line(bmp, x5, y4, x7, y6, color);
    line(bmp, x7, y6, x6, y7, color);
    line(bmp, x6, y7, x4, y5, color);
    line(bmp, x4, y5, x2, y7, color);
    line(bmp, x2, y7, x1, y6, color);
    line(bmp, x1, y6, x3, y4, color);
    line(bmp, x3, y4, x1, y2, color);
    line(bmp, x1, y2, x2, y1, color);
    line(bmp, x2, y1, x4, y3, color);
    line(bmp, x4, y3, x6, y1, color);
    line(bmp, x6, y1, x7, y2, color);
    line(bmp, x7, y2, x5, y4, color);
    release_bitmap(bmp);
    //switch_allegro_font (big_font);
    textprintf_centre_ex(bmp, large_font, xcs, y + w2, color, -1, "%d", bead_nb);

    if (fixed)
        textprintf_centre_ex(bmp, normalsize_font, xcs, y - w2 - w2 - w2, color, -1, "F");

    if (zpos != NULL)
      textprintf_ex(bmp, normalsize_font, xcs + w2 + w2, y - w2, zcolor, -1, "%s",zpos);

    //alfont_textout(bmp,user_font,bd,xcs,y, color);

    return 0;
}






int draw_rectangle_in_screen_unit(int xc, int yc, int width, int height, int color, BITMAP *bmp, int scale, int bead_nb)
{
    // bead pixel position

    int h = (28 * (height >> 1)) / scale;
    int w = (28 * (width >> 1)) / scale;
    int y = bmp->h - (28 * yc) / scale;
    int xcs = (28 * xc) / scale;
    int x1 =  xcs - w;
    int x4 =  xcs + w;
    int y1 =  y + h;
    int y4 =  y - h;
    (void)bead_nb;
    acquire_bitmap(bmp);
    hline(bmp, x1, y1, x4, color);
    vline(bmp, x1, y1, y4, color);
    hline(bmp, x1, y4, x4, color);
    vline(bmp, x4, y1, y4, color);
    release_bitmap(bmp);
    return 0;
}





int draw_circle_in_screen_unit(int xc, int yc, int radius, int color, BITMAP *bmp, int scale)
{
    // bead pixel position

    int r = (28 * radius) / scale;
    int y = bmp->h - (28 * yc) / scale;
    int x = (28 * xc) / scale;
    acquire_bitmap(bmp);
    circle(bmp, x, y, r, color);
    release_bitmap(bmp);
    return 0;
}

# endif

int fft_band_pass_in_real_out_complex(float *x, int nx, int w_flag, int fc,
                                      int width , fft_plan *fp, filter_bt *fil)
{
    int i, j;
    float  moy, smp = 0.05;

    /* remove dc based upon the first and the last quarter of data */
    /*  we assume a peak in the middle !*/
    if (fp == NULL || fil == NULL || x == NULL) return 1;

    if (fftbt_init(fp, nx) == NULL)     return 2;

    for (i = 0, j = (3 * nx) / 4, moy = 0; i < nx / 4; i++, j++)   moy += x[i] + x[j];

    for (i = 0, moy = (2 * moy) / nx; i < nx; i++)       x[i] -= moy;

    if (w_flag)     fftbtwindow1(fp, x, smp);

    realtr1bt(fp, x);
    fftbt(fp, x, 1);
    realtr2bt(fp, x, 1);
    bandpass_smooth_half_bt(fil, nx, x, fc, width);
    fftbt(fp, x, -1);

    if (w_flag)     defftbtwc1(fp,  x, smp);

    return 0;
}


int prepare_filtered_profile(float *zp, int nx, int flag, int lfilter, int width, fft_plan *fp, filter_bt *fil)
{
    int j, i;
    float moy, re, im;//, amp, norm, tmp;

    (void)flag;

    /* remove dc based upon the first and the last quarter of data */
    /*  we assume a peak in the middle !*/
    if (fp == NULL || fil == NULL || zp == NULL) return 1;

    if (fftbt_init(fp, nx) == NULL)     return 2;

    for (i = 0, j = (3 * nx) / 4, moy = 0; i < nx / 4; i++, j++)   moy += zp[i] + zp[j];

    for (i = 0, moy = (2 * moy) / nx; i < nx; i++)       zp[i] -= moy;

    /* no windowing, profile must be symmetrical */
    realtr1bt(fp, zp);
    fftbt(fp, zp, 1);
    realtr2bt(fp, zp, 1);
    /* I take the amplitude of the cosine of the fist mode to normalize */
    //moy = zp[2];
    // I normalize in a new way multiplying by r/nx_2 and taking variance
    //for (j = 0, amp = 0, nx_2 = nx/2, norm = (float)1/nx_2; j < nx_2 ; j++)
    //{
    //	tmp = norm*(zp[nx_2+j] * j * (nx_2 - j));
    //	amp += tmp * tmp;
    //}
    //amp = moy/100;
    //amp /= nx_2;
    //moy = sqrt(amp);
    if (moy == 0) return 2; //win_printf("pb of normalisation zp[2] %f zp[3] %f",  zp[2],zp[3]);
    else
    {
        for (j = 0, moy = ((float)1) / moy; j < nx ; j++)   zp[j] *= moy;
    }

    zp[0] = zp[1] = 0;
    bandpass_smooth_half_bt(fil, nx, zp, lfilter, width);
    fftbt(fp,  zp, -1);

    /* first halh of profile ampliture, second half re im */
    for (j = 0 ; j < nx / 2 ; j += 2)
    {
        re = zp[j];
        im = zp[j + 1];
        zp[j] = sqrt(re * re + im * im);
        zp[j + 1] = 0;
    }

    return 0;
}


int prepare_filtered_profile_lphp(float *zp, int nx, int flag, int lpfilter, int lpw, int hpfilter, int hpw, fft_plan *fp, filter_bt *fil)
{
    int j, i;
    float moy, re, im;//, amp, norm, tmp;

    (void)flag;

    /* remove dc based upon the first and the last quarter of data */
    /*  we assume a peak in the middle !*/
    if (fp == NULL || fil == NULL || zp == NULL) return 1;

    if (fftbt_init(fp, nx) == NULL)     return 2;

    for (i = 0, j = (3 * nx) / 4, moy = 0; i < nx / 4; i++, j++)   moy += zp[i] + zp[j];

    for (i = 0, moy = (2 * moy) / nx; i < nx; i++)       zp[i] -= moy;

    /* no windowing, profile must be symmetrical */
    realtr1bt(fp, zp);
    fftbt(fp, zp, 1);
    realtr2bt(fp, zp, 1);
    /* I take the amplitude of the cosine of the fist mode to normalize */
    //moy = zp[2];
    // I normalize in a new way multiplying by r/nx_2 and taking variance
    //for (j = 0, amp = 0, nx_2 = nx/2, norm = (float)1/nx_2; j < nx_2 ; j++)
    //{
    //	tmp = norm*(zp[nx_2+j] * j * (nx_2 - j));
    //	amp += tmp * tmp;
    //}
    //amp = moy/100;
    //amp /= nx_2;
    //moy = sqrt(amp);
    if (moy == 0) return 2; //win_printf("pb of normalisation zp[2] %f zp[3] %f",  zp[2],zp[3]);
    else
    {
        for (j = 0, moy = ((float)1) / moy; j < nx ; j++)   zp[j] *= moy;
    }

    zp[0] = zp[1] = 0;
    lowpass_and_highpass_smooth_half_bt (fil, nx, zp, lpfilter, lpw, hpfilter, hpw);
    //bandpass_smooth_half_bt(fil, nx, zp, lfilter, width);
    fftbt(fp,  zp, -1);

    /* first halh of profile ampliture, second half re im */
    for (j = 0 ; j < nx / 2 ; j += 2)
    {
        re = zp[j];
        im = zp[j + 1];
        zp[j] = sqrt(re * re + im * im);
        zp[j + 1] = 0;
    }

    return 0;
}



float   find_simple_phase_shift_between_filtered_profile(const float *zr, const float *zp, int nx, int rc)
{
    int i, j;
    float im, re, amp, phi, w, ph;

    if (zr == NULL || zp == NULL)   return WRONG_ARGUMENT;

    for (j = 2, i = nx - j, phi = 0, w = 0; j < nx / 2 - rc; i -= 2, j += 2)
    {
        re = zr[i] * zp[i] + zp[i + 1] * zr[i + 1];
        im = zp[i + 1] * zr[i] - zp[i] * zr[i + 1];
        ph = (im == 0.0 && re == 0.0) ? 0.0 : atan2(im, re);
        amp = zr[j] * zp[j];
        w += amp;
        phi += amp * ph;
    }

    return (w != 0.0) ? phi / w : phi;
}


float   find_phase_shift_between_profile(float *zr, float *zp, int npc,
        int flag, int lfilter, int width, int rc, fft_plan *fp, filter_bt *fil)
{
    int i;
    float im, re, amp, phi, dphi, w;
    //static float *tmp = NULL, *tmp1 = NULL;
    //static int ntmp = 0;
    int nx, onx;
    float tmp[256] = {0},
                     *tmp1 = NULL;


    if (zr == NULL || zp == NULL)   return WRONG_ARGUMENT;

    nx = npc;

    if (fp == NULL || fil == NULL || zp == NULL) return 1;

    if (fftbt_init(fp, nx) == NULL)     return 2;

    //if (fft_init(nx) || filter_init(nx))          return FFT_NOT_SUPPORTED;
    /*
       if (tmp == NULL || ntmp != nx)
       {
       tmp = (float *)calloc(nx,sizeof(float));
       if (tmp == NULL) return -1;
       ntmp = nx;
       tmp1 = tmp + nx/2;
       }
       */
    tmp1 = tmp + nx / 2;
    fft_band_pass_in_real_out_complex(zp, nx, flag, lfilter, width, fp, fil);

    for (i = 0; i < nx / 2; i++)
    {
        re = zr[2 * i] * zp[2 * i] + zp[2 * i + 1] * zr[2 * i + 1];
        im = zp[2 * i + 1] * zr[2 * i] - zp[2 * i] * zr[2 * i + 1];
        tmp[i] = (im == 0.0 && re == 0.0) ? 0.0 : atan2(im, re);
    }

    onx = nx / 2;
    tmp1[onx / 2] = tmp[onx / 2];


    for (i = onx / 2 + rc / 2, phi = 0; i < onx; i++)
    {
        dphi = tmp[i] - tmp[i - 1];

        if (dphi > M_PI)
            phi -= 2 * M_PI;

        if (dphi < -M_PI)
            phi += 2 * M_PI;

        tmp1[i] = tmp[i] + phi;
    }

    for (i = onx / 2 - rc / 2, phi = 0; i >= 0; i--)
    {
        dphi = tmp[i] - tmp[i + 1];

        if (dphi > M_PI)
            phi -= 2 * M_PI;

        if (dphi < -M_PI)
            phi += 2 * M_PI;

        tmp1[i] = tmp[i] + phi;
    }

    for (i = onx / 2 + rc, phi = 0, w = 0; i < onx; i++)
    {
        amp = zp[2 * i] * zp[2 * i];
        amp += zp[2 * i + 1] * zp[2 * i + 1];
        w += amp;
        phi += amp * tmp1[i];
    }

    return (w != 0.0) ? phi / w : phi;
}

d_s *average_over_smooth_window_in_x(d_s *dsi, int n_out)
{
    register int i, j, k;
    float w, w_2, x, tmp, co;
    d_s *dso = NULL;

    if (dsi == NULL) return NULL;
    if (n_out < 2 || dsi->nx < 2 || n_out >= dsi->nx) return NULL;
    dso = build_data_set(n_out,n_out);
    if (dso == NULL) return NULL;
    w = ((float)(dsi->nx-1))/n_out;
    w_2 = w/2;
    for(i = 0; i < dsi->nx; i++)
      {
	j = (int)((float)i)/w;
	x = w_2 + j * w;
	if (i < w_2)
	  {
	    dso->xd[0] += dsi->xd[i];
	    dso->yd[0] += 1;
	  }
	else if (i >= dsi->nx - w_2)
	  {
	    dso->xd[dso->nx-1] += dsi->xd[i];
	    dso->yd[dso->nx-1] += 1;
	  }
	else
	  {
	    k = ((float)i >= x) ? j+1 : j - 1;
	    k = (k < 0) ? 0 : k;
	    k = (k < dso->nx) ? k : dso->nx-1;
	    co = (cos(M_PI*(x - i)/w))/2;
	    tmp = 0.5 + co;
	    dso->xd[j] += tmp*dsi->xd[i];
	    dso->yd[j] += tmp;
	    tmp = 1 - tmp;
	    dso->xd[k] += tmp*dsi->xd[i];
	    dso->yd[k] += tmp;
	  }
      }
    for(i = 0; i < dso->nx; i++)
      {
	dso->xd[i] /= (dso->yd[i] > 0) ? dso->yd[i] : 1;
	dso->yd[i] = w_2 + (i * w);
      }
    return dso;
}

O_i *image_band_pass_in_real_out_complex(O_i *dst, O_i *oi, int w_flag, int lfilter, int width, int rc, fft_plan *fp,
        filter_bt *fil)
{
    int i;
    int nx, ny;
    union pix *ps = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;


    if (oi == NULL || oi->im.data_type == IS_COMPLEX_IMAGE)     return NULL;

    ny = oi->im.ny;
    nx = oi->im.nx;

    if (dst == NULL)    dst = create_one_image(nx / 2,  ny, IS_COMPLEX_IMAGE);
    else if (dst->im.data_type != IS_COMPLEX_IMAGE || dst->im.nx != oi->im.nx / 2
             || dst->im.ny != oi->im.ny || dst->im.n_f != 1)
    {
        free_one_image(dst);
        dst = create_one_image(nx / 2,  ny, IS_COMPLEX_IMAGE);
    }

    if (dst == NULL)    return NULL;

    ps = dst->im.pixel;

    for (i = 0 ; i < ny ; i++)
    {
        //display_title_message("doing line %d",i);
        extract_raw_line(oi, i, ps[i].fl);
        prepare_filtered_profile(ps[i].fl, nx, w_flag, lfilter, width, fp, fil);
    }

    inherit_from_im_to_im(dst, oi);
    uns_oi_2_oi(dst, oi);
    dst->im.win_flag = oi->im.win_flag;

    if (oi->x_title != NULL)    set_im_x_title(dst, "%s", oi->x_title);

    if (oi->y_title != NULL)    set_im_y_title(dst, "%s", oi->y_title);

    set_im_title(dst, "\\stack{{Bandpass filter image}{%s}"
                 "{\\pt8 filter %d width %d flag %s}}", (oi->title != NULL) ? oi->title
                 : "untitled", lfilter, width, (w_flag) ? "periodic" : "not periodic");
    set_formated_string(&dst->im.treatement, "Bandpass filter image %s"
                        "filter %d width %d flag %s", (oi->title != NULL) ? oi->title
                        : "untitled", lfilter, width, (w_flag) ? "periodic" : "not periodic");
    op = create_and_attach_op_to_oi(dst, ny-1, ny-1, 0, 0);

    if (op == NULL)     return (O_i *) win_printf_ptr("cannot create plot!");

    ds = op->dat[0];
    op->type = IM_SAME_Y_AXIS;
    uns_oi_2_op(dst, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
    set_formated_string(&ds->treatement, "Simple phase difference from n and n-1 profile");

    for (i = 0 ; i < ny - 1 ; i++)
    {
        ds->xd[i] = find_simple_phase_shift_between_filtered_profile(ps[i].fl, ps[i + 1].fl, nx, rc);
        ds->yd[i] = (float)i;
    }
    float phitot;
    int n_out;
    d_s *dso = NULL, *dsd = NULL;
    for (i = 0, phitot = 0; i < ds->nx ; i++)
      phitot += ds->xd[i];
    phitot = fabs(phitot);
    phitot /= 0.5;  // a good practice is to have 0.5 rad between to succesive phase points
    n_out = (int)(0.5+phitot);
    n_out = (n_out < 4) ? 4 : n_out;
    n_out = (n_out > ds->nx/3) ? ds->nx/3 : n_out;
    dso = average_over_smooth_window_in_x(ds, n_out);
    if (dso == NULL) return dst;
    add_one_plot_data(op, IS_DATA_SET, (void*)dso);
    if ((dsd = create_and_attach_one_ds(op, ds->nx, ds->nx, 0)) == NULL)
      return dst;
    float tmp;
    for (i = 0; i < dsd->nx; i++)
      {
	dsd->xd[i] = interpolate_point_by_poly_3_in_array(dso->yd, dso->xd, dso->nx, ds->yd[i]);
	dsd->yd[i] = ds->yd[i];
	tmp = ds->xd[i];
	ds->xd[i] = dsd->xd[i];
	dsd->xd[i] = tmp;
      }
    return dst;
}


O_i *image_lowpass_and_highpass_in_real_out_complex(O_i *dst, O_i *oi, int w_flag, int lpfilter, int lw, int hpfilter, int hw, int rc, fft_plan *fp, filter_bt *fil)
{
    int i;
    int nx, ny;
    union pix *ps = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;


    if (oi == NULL || oi->im.data_type == IS_COMPLEX_IMAGE)     return NULL;

    ny = oi->im.ny;
    nx = oi->im.nx;

    if (dst == NULL)    dst = create_one_image(nx / 2,  ny, IS_COMPLEX_IMAGE);
    else if (dst->im.data_type != IS_COMPLEX_IMAGE || dst->im.nx != oi->im.nx / 2
             || dst->im.ny != oi->im.ny || dst->im.n_f != 1)
    {
        free_one_image(dst);
        dst = create_one_image(nx / 2,  ny, IS_COMPLEX_IMAGE);
    }

    if (dst == NULL)    return NULL;

    ps = dst->im.pixel;

    for (i = 0 ; i < ny ; i++)
    {
        //display_title_message("doing line %d",i);
        extract_raw_line(oi, i, ps[i].fl);
	prepare_filtered_profile_lphp(ps[i].fl, nx, w_flag, lpfilter, lw, hpfilter, hw, fp, fil);
    }

    inherit_from_im_to_im(dst, oi);
    uns_oi_2_oi(dst, oi);
    dst->im.win_flag = oi->im.win_flag;

    if (oi->x_title != NULL)    set_im_x_title(dst, "%s", oi->x_title);

    if (oi->y_title != NULL)    set_im_y_title(dst, "%s", oi->y_title);

    set_im_title(dst, "\\stack{{Bandpass filter 2 image}{%s}"
                 "{\\pt8 lowpass filter %d (w %d) highpass %d (w %d) flag %s}}", (oi->title != NULL) ? oi->title
                 : "untitled", lpfilter, lw, hpfilter, hw, (w_flag) ? "periodic" : "not periodic");
    set_formated_string(&dst->im.treatement, "Bandpass filter 2 image %s"
                        "lowpass filter %d (w %d) highpass %d (w %d) flag %s", (oi->title != NULL) ? oi->title
                        : "untitled", lpfilter, lw, hpfilter, hw, (w_flag) ? "periodic" : "not periodic");
    op = create_and_attach_op_to_oi(dst, ny-1, ny-1, 0, 0);

    if (op == NULL)     return (O_i *) win_printf_ptr("cannot create plot!");

    ds = op->dat[0];
    op->type = IM_SAME_Y_AXIS;
    uns_oi_2_op(dst, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
    set_formated_string(&ds->treatement, "Simple phase difference from n and n-1 profile");

    for (i = 0 ; i < ny - 1 ; i++)
    {
        ds->xd[i] = find_simple_phase_shift_between_filtered_profile(ps[i].fl, ps[i + 1].fl, nx, rc);
        ds->yd[i] = (float)i;
    }
    float phitot;
    int n_out;
    d_s *dso = NULL, *dsd = NULL;
    for (i = 0, phitot = 0; i < ds->nx ; i++)
      phitot += ds->xd[i];
    phitot = fabs(phitot);
    phitot /= 0.5;  // a good practice is to have 0.5 rad between to succesive phase points
    n_out = (int)(0.5+phitot);
    n_out = (n_out < 4) ? 4 : n_out;
    n_out = (n_out > ds->nx/3) ? ds->nx/3 : n_out;
    dso = average_over_smooth_window_in_x(ds, n_out);
    if (dso == NULL) return dst;
    add_one_plot_data(op, IS_DATA_SET, (void*)dso);
    if ((dsd = create_and_attach_one_ds(op, ds->nx, ds->nx, 0)) == NULL)
      return dst;
    float tmp;
    for (i = 0; i < dsd->nx; i++)
      {
	dsd->xd[i] = interpolate_point_by_poly_3_in_array(dso->yd, dso->xd, dso->nx, ds->yd[i]);
	dsd->yd[i] = ds->yd[i];
	tmp = ds->xd[i];
	ds->xd[i] = dsd->xd[i];
	dsd->xd[i] = tmp;
      }
    return dst;
}


int coarse_find_z_by_least_square_min(float *yt, int nxp, O_i *ois, float *min, int rc)
{
    int i, j, k;
    int nx, ny;
    union pix *ps = NULL;
    float  tmp, *yr = NULL, val, norm;

    if (ois->im.data_type != IS_COMPLEX_IMAGE)
        return win_printf_OK("image must be complex!");

    ny = ois->im.ny;
    nx = ois->im.nx;

    if (2 * nx != nxp)            return -1;

    ps = ois->im.pixel;

    for (i = k = 0; i < ny ; i++)
    {
        yr = ps[i].fl;

        /* first attemp pb !
           for (j = 0, val = 0; j < nx - rc/2; j +=2)
           {
           tmp = yr[j] -  yt[j];
           val += tmp * tmp;
           }
           */
        for (j = nx + rc, val = 0; j < nxp; j++)
        {
            tmp = yr[j] -  yt[j];
            val += tmp * tmp;
        }

        if (i == 0 || val < *min)
        {
            k = i;
            *min = val;
        }
    }

    for (j = nx + rc, norm = 0, yr = ps[k].fl; j < nxp; j++)
    {
        norm += yr[j] * yr[j];
    }

    //*min /= ((nx-rc)/2);  was working
    if (norm > 0) *min /= norm;

    return k;
}
float find_zero_of_3_points_polynome(float y_1, float y0, float y1)
{
    double a, b, c, x = 0, delta;

    a = ((y_1 + y1) / 2) - y0;
    b = (y1 - y_1) / 2;
    c = y0;
    delta = b * b - 4 * a * c;

    if (a == 0)
    {
        x = (b != 0) ? -c / b : 0;
    }
    else if (delta >= 0)
    {
        delta = sqrt(delta);
        x = (fabs(-b + delta) < fabs(-b - delta)) ? -b + delta : -b - delta ;
        x /= 2 * a;
    }

    return (float)x;
}

int find_z_profile_by_mean_square_and_phase(float *zp, int nx, O_i *oir, int w_flag, int lfilter, int width,
        int rc, float *z, int *profil_index, float *min_c, float *min_phi,
        fft_plan *fp, filter_bt *fil, float *z_tmp_prof)
{
    int k, j;
    int er = 0, ny;
    //  static int oldnx = 0;
    //  static float *yt = NULL; forbiden in multi thread !

    float yt[256];
    float min, tmp, ph_1, phi, ph1;
    union pix *psd;
    O_p *opz;
    d_s *dsrz;

    /*
       if (yt == NULL || oldnx != nx)
       {
       yt = (float*)realloc(yt,nx*sizeof(float));
       if (yt == NULL)      return -1;
       oldnx = nx;
       }
       */
    psd = oir->im.pixel;
    ny = oir->im.ny;
    opz = (oir->n_op != 0) ? oir->o_p[0] : NULL;
    dsrz = (opz != NULL) ? opz->dat[0] : NULL;

    for (j = 0; j < nx ; j++) yt[j] = zp[j];

    prepare_filtered_profile(yt, nx, w_flag, lfilter, width, fp, fil);

    if (z_tmp_prof != NULL)
    {
        for (int i = 0; i < nx; ++i)
            z_tmp_prof[i] = yt[i];
    }

    j = coarse_find_z_by_least_square_min(yt, nx, oir, &min,  rc);

    if (profil_index != NULL)(*profil_index) = j;
//    printf("prof_idx %d\n", j);

    j = (j == 0) ? j + 1 : j;
    j = (j == ny - 1) ? ny - 2 : j;
    *min_c = min;
    /*
     *z = oir->ay + oir->dy *  j;
     return er;
     */
    tmp = (float)j;
    phi = find_simple_phase_shift_between_filtered_profile(psd[j].fl, yt, nx, rc);
 //   printf("phi %f\n", phi);

    tmp = (dsrz != NULL) ? dsrz->xd[j] : 0;
    tmp = (tmp != 0) ? (float)j + phi / tmp : (float)j;
    k = (int)(tmp + .5);
    er = (abs(k - j) >= 2) ? 1 : 0;
    k = (k <= 0) ? 1 : k;
    k = (k >= ny - 1) ? ny - 2 : k;

    ph_1 = (k - 1 == j) ? phi :
           find_simple_phase_shift_between_filtered_profile(psd[k - 1].fl, yt, nx, rc);
    ph1 = (k + 1 == j) ? phi :
          find_simple_phase_shift_between_filtered_profile(psd[k + 1].fl, yt, nx, rc);
    phi = (k == j) ? phi :
          find_simple_phase_shift_between_filtered_profile(psd[k].fl, yt, nx, rc);

//    printf("phi-1 %f phi %f phi+1 %f\n", ph_1, phi, ph1);

    *z = oir->ay + oir->dy * (find_zero_of_3_points_polynome(ph_1, phi, ph1) + k);
    //printf("%f\n", *z);
    *min_phi = ((ph_1 - ph1) != 0) ? phi / (ph_1 - ph1) : phi; // erreur in phase normalized by profile distance
    return er;
}



int find_z_profile_by_mean_square_and_phase_lphp(float *zp, int nx, O_i *oir, int w_flag, int lfilter, int lw, int hpfilter, int hw,
        int rc, float *z, int *profil_index, float *min_c, float *min_phi,
        fft_plan *fp, filter_bt *fil, float *z_tmp_prof)
{
    int k, j;
    int er = 0, ny;
    //  static int oldnx = 0;
    //  static float *yt = NULL; forbiden in multi thread !

    float yt[256];
    float min, tmp, ph_1, phi, ph1;
    union pix *psd;
    O_p *opz;
    d_s *dsrz;

    /*
       if (yt == NULL || oldnx != nx)
       {
       yt = (float*)realloc(yt,nx*sizeof(float));
       if (yt == NULL)      return -1;
       oldnx = nx;
       }
       */
    psd = oir->im.pixel;
    ny = oir->im.ny;
    opz = (oir->n_op != 0) ? oir->o_p[0] : NULL;
    dsrz = (opz != NULL) ? opz->dat[0] : NULL;

    for (j = 0; j < nx ; j++) yt[j] = zp[j];

    prepare_filtered_profile_lphp(yt, nx, w_flag, lfilter, lw, hpfilter, hw, fp, fil);

    if (z_tmp_prof != NULL)
    {
        for (int i = 0; i < nx; ++i)
            z_tmp_prof[i] = yt[i];
    }

    j = coarse_find_z_by_least_square_min(yt, nx, oir, &min,  rc);

    if (profil_index != NULL)(*profil_index) = j;
//    printf("prof_idx %d\n", j);

    j = (j == 0) ? j + 1 : j;
    j = (j == ny - 1) ? ny - 2 : j;
    *min_c = min;
    /*
     *z = oir->ay + oir->dy *  j;
     return er;
     */
    tmp = (float)j;
    phi = find_simple_phase_shift_between_filtered_profile(psd[j].fl, yt, nx, rc);
 //   printf("phi %f\n", phi);

    tmp = (dsrz != NULL) ? dsrz->xd[j] : 0;
    tmp = (tmp != 0) ? (float)j + phi / tmp : (float)j;
    k = (int)(tmp + .5);
    er = (abs(k - j) >= 2) ? 1 : 0;
    k = (k <= 0) ? 1 : k;
    k = (k >= ny - 1) ? ny - 2 : k;

    ph_1 = (k - 1 == j) ? phi :
           find_simple_phase_shift_between_filtered_profile(psd[k - 1].fl, yt, nx, rc);
    ph1 = (k + 1 == j) ? phi :
          find_simple_phase_shift_between_filtered_profile(psd[k + 1].fl, yt, nx, rc);
    phi = (k == j) ? phi :
          find_simple_phase_shift_between_filtered_profile(psd[k].fl, yt, nx, rc);

//    printf("phi-1 %f phi %f phi+1 %f\n", ph_1, phi, ph1);

    *z = oir->ay + oir->dy * (find_zero_of_3_points_polynome(ph_1, phi, ph1) + k);
    //printf("%f\n", *z);
    *min_phi = ((ph_1 - ph1) != 0) ? phi / (ph_1 - ph1) : phi; // erreur in phase normalized by profile distance
    return er;
}




/*  take a radial image profile of an image with a different scaling in
 *  x and y, real x = i_image * ax,  y = i_image * ay
 *
 *
 *
 */


float *radial_non_pixel_square_image_sym_profile_in_array(float *rz, O_i *ois, float xc, float yc, int r_max, float ax,
        float ay)
{
    int i, j;
    union pix *ps = NULL;
    int onx, ony, ri, type;
    float z = 0, z1, x, y, r, p, p0 = 0, pr0 = 0;
    int imin, imax, jmin, jmax; //  xci, yci
# if !defined(PLAYITSAM) && !defined(PIAS)
    float eva_black = 0, eva_level = 0, eva_norm = 0, thres_w = 0.001, eva_w;
    int n_eva_black = 0;
# endif

    //for (i = 0; i < 2*r_max && i < 256; i++)  radial_dx[i] = 0;
    //for (i = 0; i < 4096; i++)      evanescent_black_level_histo[i] = 0;
    if (ois == NULL)    return NULL;

    if (rz == NULL)     rz = (float *)calloc(2 * r_max, sizeof(float));

    if (rz == NULL)     return NULL;

    ps = ois->im.pixel;
    ony = ois->im.ny;
    onx = ois->im.nx;
    type = ois->im.data_type;

    for (i = 0; i < 2 * r_max; i++)   rz[i] = 0;

    //xci = (int)xc;
    //yci = (int)yc;

    imin = (int)(yc - (r_max / ay));
    imax = (int)(0.5 + yc + (r_max / ay));
    jmin = (int)(xc -  r_max / ax);
    jmax = (int)(0.5 + xc + r_max / ax);
    imin = (imin < 0) ? 0 : imin;
    imax = (imax > ony) ? ony : imax;
    jmin = (jmin < 0) ? 0 : jmin;
    jmax = (jmax > onx) ? onx : jmax;

    if (type == IS_COMPLEX_IMAGE)
    {
        for (i = imin; i < imax; i++)
        {
            y = ay * ((float)i - yc);
            y *= y;

            for (j = jmin; j < jmax; j++)
            {
                x = ax * ((float)j - xc);
                x *= x;
                r = (float)sqrt(x + y);
                ri = (int)r;

                if (ri > r_max) continue;   /* bug break;*/

                if (ois->im.mode == RE)
                    z = ps[i].fl[2 * j];
                else if (ois->im.mode == IM)
                    z = ps[i].fl[2 * j + 1];
                else if (ois->im.mode == AMP)
                {
                    z = ps[i].fl[2 * j];
                    z1 = ps[i].fl[2 * j + 1];
                    z = sqrt(z * z + z1 * z1);
                }
                else if (ois->im.mode == AMP_2)
                {
                    z = ps[i].fl[2 * j];
                    z1 = ps[i].fl[2 * j + 1];
                    z = z * z + z1 * z1;
                }
                else if (ois->im.mode == LOG_AMP)
                {
                    z = ps[i].fl[2 * j];
                    z1 = ps[i].fl[2 * j + 1];
                    z = z * z + z1 * z1;
                    z = (z > 0) ? log10(z) : -40.0;
                }
                else
                {
                    win_printf("Unknown mode for complex image");
                    return NULL;
                }

                p = r - ri;

                if (ri == r_max)
                {
                    p0 += (1 - p) * z;
                    pr0 += (1 - p);
                    //radial_dx[r_max] -= 1-p;
                }
                else
                {
                    rz[ri] += (1 - p) * z;
                    rz[ri + r_max] += (1 - p);
                    //radial_dx[ri] += p;
                }

# if !defined(PLAYITSAM) && !defined(PIAS)
                eva_w = (1 - p) * evanescent_spot_weighting[ri];
# endif
                ri++;

                if (ri < r_max)
                {
                    rz[ri] += p * z;
                    rz[ri + r_max] += p;
                    //radial_dx[ri] -= 1-p;
                }

# if !defined(PLAYITSAM) && !defined(PIAS)
                eva_w += p * evanescent_spot_weighting[ri];

                if (eva_w < thres_w)
                {
                    eva_black += z;
                    n_eva_black++;
                }
                else
                {
                    eva_level += eva_w * z;
                    eva_norm += eva_w;
                }

# endif
            }
        }
    }
    else if (type == IS_FLOAT_IMAGE)
    {
        for (i = imin; i < imax; i++)
        {
            y = ay * ((float)i - yc);
            y *= y;

            for (j = jmin; j < jmax; j++)
            {
                x = ax * ((float)j - xc);
                x *= x;
                r = (float)sqrt(x + y);
                ri = (int)r;

                if (ri > r_max) continue;   /* bug break;*/

                z = ps[i].fl[j];
                p = r - ri;
# if !defined(PLAYITSAM) && !defined(PIAS)
                eva_w = (1 - p) * evanescent_spot_weighting[ri];
# endif

                if (ri == r_max)
                {
                    p0 += (1 - p) * z;
                    pr0 += (1 - p);
                    //radial_dx[r_max] -= 1-p;
                }
                else
                {
                    rz[ri] += (1 - p) * z;
                    rz[ri + r_max] += (1 - p);
                    //radial_dx[ri] += p;
                }

                ri++;

                if (ri < r_max)
                {
                    rz[ri] += p * z;
                    rz[ri + r_max] += p;
                    //radial_dx[ri] -= 1-p;
                }

# if !defined(PLAYITSAM) && !defined(PIAS)
                eva_w += p * evanescent_spot_weighting[ri];

                if (eva_w < thres_w)
                {
                    eva_black += z;
                    n_eva_black++;
                }
                else
                {
                    eva_level += eva_w * z;
                    eva_norm += eva_w;
                }

# endif
            }
        }
    }
    else if (type == IS_INT_IMAGE)
    {
        for (i = imin; i < imax; i++)
        {
            y = ay * ((float)i - yc);
            y *= y;

            for (j = jmin; j < jmax; j++)
            {
                x = ax * ((float)j - xc);
                x *= x;
                r = (float)sqrt(x + y);
                ri = (int)r;

                if (ri > r_max) continue;   /* bug break;*/

                z = (float)ps[i].in[j];
                p = r - ri;
# if !defined(PLAYITSAM) && !defined(PIAS)
                eva_w = (1 - p) * evanescent_spot_weighting[ri];
# endif

                if (ri == r_max)
                {
                    p0 += (1 - p) * z;
                    pr0 += (1 - p);
                    //radial_dx[r_max] -= 1-p;
                }
                else
                {
                    rz[ri] += (1 - p) * z;
                    rz[ri + r_max] += (1 - p);
                    //radial_dx[ri] += p;
                }

                ri++;

                if (ri < r_max)
                {
                    rz[ri] += p * z;
                    rz[ri + r_max] += p;
                    //radial_dx[ri] -= 1-p;
                }

# if !defined(PLAYITSAM) && !defined(PIAS)
                eva_w += p * evanescent_spot_weighting[ri];

                if (eva_w < thres_w)
                {
                    eva_black += z;
                    n_eva_black++;
                }
                else
                {
                    eva_level += eva_w * z;
                    eva_norm += eva_w;
                }

# endif
            }
        }
    }
    else if (type == IS_UINT_IMAGE)
    {
        for (i = imin; i < imax; i++)
        {
            y = ay * ((float)i - yc);
            y *= y;

            for (j = jmin; j < jmax; j++)
            {
                x = ax * ((float)j - xc);
                x *= x;
                r = (float)sqrt(x + y);
                ri = (int)r;

                if (ri > r_max) continue;   /* bug break;*/

                z = (float)ps[i].ui[j];
                p = r - ri;
# if !defined(PLAYITSAM) && !defined(PIAS)
                eva_w = (1 - p) * evanescent_spot_weighting[ri];
# endif

                if (ri == r_max)
                {
                    p0 += (1 - p) * z;
                    pr0 += (1 - p);
                    //radial_dx[r_max] -= 1-p;
                }
                else
                {
                    rz[ri] += (1 - p) * z;
                    rz[ri + r_max] += (1 - p);
                    //radial_dx[ri] += p;
                }

                ri++;

                if (ri < r_max)
                {
                    rz[ri] += p * z;
                    rz[ri + r_max] += p;
                    //radial_dx[ri] -= 1-p;
                }

# if !defined(PLAYITSAM) && !defined(PIAS)
                eva_w += p * evanescent_spot_weighting[ri];

                if (eva_w < thres_w)
                {
                    eva_black += z;
                    n_eva_black++;
                }
                else
                {
                    eva_level += eva_w * z;
                    eva_norm += eva_w;
                }

# endif
            }
        }
    }
    else if (type == IS_CHAR_IMAGE)
    {
        for (i = imin; i < imax; i++)
        {
            y = ay * ((float)i - yc);
            y *= y;

            for (j = jmin; j < jmax; j++)
            {
                x = ax * ((float)j - xc);
                x *= x;
                r = (float)sqrt(x + y);
                ri = (int)r;

                if (ri > r_max) continue;   /* bug break;*/

                z = (float)ps[i].ch[j];
                p = r - ri;
# if !defined(PLAYITSAM) && !defined(PIAS)
                eva_w = (1 - p) * evanescent_spot_weighting[ri];
# endif

                if (ri == r_max)
                {
                    p0 += (1 - p) * z;
                    pr0 += (1 - p);
                    //radial_dx[r_max] -= 1-p;
                }
                else
                {
                    rz[ri] += (1 - p) * z;
                    rz[ri + r_max] += (1 - p);
                    //radial_dx[ri] += p;
                }

                ri++;

                if (ri < r_max)
                {
                    rz[ri] += p * z;
                    rz[ri + r_max] += p;
                    //radial_dx[ri] -= 1-p;
                }

# if !defined(PLAYITSAM) && !defined(PIAS)
                eva_w += p * evanescent_spot_weighting[ri];

                if (eva_w < thres_w)
                {
                    eva_black += z;
                    n_eva_black++;
                }
                else
                {
                    eva_level += eva_w * z;
                    eva_norm += eva_w;
                }

# endif
            }
        }
    }

    for (i = 0; i < r_max; i++)
    {
        rz[i] = (rz[i + r_max] == 0) ? 0 : rz[i] / rz[i + r_max];
        //      radial_dx[i] = (rz[i+r_max] == 0) ? 0 : radial_dx[i]/rz[i+r_max];
    }

    p0 = (pr0 == 0) ? 0 : p0 / pr0;

    for (i = 0; i < r_max; i++)     rz[i + r_max] = rz[i];

    for (i = 1; i < r_max; i++)     rz[r_max - i] = rz[r_max + i];

    rz[0] = p0;
# if !defined(PLAYITSAM) && !defined(PIAS)

    if (n_eva_black > 0)    eva_black /= n_eva_black;

    evanescent_black_avg = eva_black;
    evanescent_black_n_avg = n_eva_black;

    if (eva_norm > 0.0) eva_level /= eva_norm;

    evanescent_avg_intensity = eva_level;
    evanescent_avg_n_appo = eva_norm;
# endif
    return rz;
}



/*  take a radial image profile of an image with a different scaling in
 *  x and y, real x = i_image * ax,  y = i_image * ay
 *
 *
 *
 */

float *mean_y_profile_in_array(float *rz, O_i *ois, float xc, float yc, int r_max, float ax, float ay)
{
    int i, j;
    union pix *ps = NULL;
    int onx, ony, ri, type;
    float z = 0, z1, y, r, p, p0 = 0, pr0 = 0;
    int imin, imax, jmin, jmax; //  xci, yci,

    //for (i = 0; i < 2*r_max && i < 256; i++)  radial_dx[i] = 0;
    if (ois == NULL)    return NULL;

    if (rz == NULL)     rz = (float *)calloc(2 * r_max, sizeof(float));

    if (rz == NULL)     return NULL;

    ps = ois->im.pixel;
    ony = ois->im.ny;
    onx = ois->im.nx;
    type = ois->im.data_type;

    for (i = 0; i < 2 * r_max; i++)   rz[i] = 0;

    //xci = (int)xc;
    //yci = (int)yc;

    imin = (int)(yc - (r_max / ay));
    imax = (int)(0.5 + yc + (r_max / ay));
    jmin = (int)(xc -  r_max / ax);
    jmax = (int)(0.5 + xc + r_max / ax);
    imin = (imin < 0) ? 0 : imin;
    imax = (imax > ony) ? ony : imax;
    jmin = (jmin < 0) ? 0 : jmin;
    jmax = (jmax > onx) ? onx : jmax;

    if (type == IS_COMPLEX_IMAGE)
    {
        for (i = imin; i < imax; i++)
        {
            y = ay * ((float)i - yc);

            //y *= y;
            for (j = jmin; j < jmax; j++)
            {
                //x = ax * ((float)j - xc);
                //x *= x;
                r = fabs(y);//(float)sqrt(x + y);
                ri = (int)r;

                if (ri > r_max) continue;   /* bug break;*/

                if (ois->im.mode == RE)
                    z = ps[i].fl[2 * j];
                else if (ois->im.mode == IM)
                    z = ps[i].fl[2 * j + 1];
                else if (ois->im.mode == AMP)
                {
                    z = ps[i].fl[2 * j];
                    z1 = ps[i].fl[2 * j + 1];
                    z = sqrt(z * z + z1 * z1);
                }
                else if (ois->im.mode == AMP_2)
                {
                    z = ps[i].fl[2 * j];
                    z1 = ps[i].fl[2 * j + 1];
                    z = z * z + z1 * z1;
                }
                else if (ois->im.mode == LOG_AMP)
                {
                    z = ps[i].fl[2 * j];
                    z1 = ps[i].fl[2 * j + 1];
                    z = z * z + z1 * z1;
                    z = (z > 0) ? log10(z) : -40.0;
                }
                else
                {
                    win_printf("Unknown mode for complex image");
                    return NULL;
                }

                p = r - ri;

                if (ri == r_max)
                {
                    p0 += (1 - p) * z;
                    pr0 += (1 - p);
                    //radial_dx[r_max] -= 1-p;
                }
                else
                {
                    rz[ri] += (1 - p) * z;
                    rz[ri + r_max] += (1 - p);
                    //radial_dx[ri] += p;
                }

                ri++;

                if (ri < r_max)
                {
                    rz[ri] += p * z;
                    rz[ri + r_max] += p;
                    //radial_dx[ri] -= 1-p;
                }
            }
        }
    }
    else if (type == IS_FLOAT_IMAGE)
    {
        for (i = imin; i < imax; i++)
        {
            y = ay * ((float)i - yc);

            //y *= y;
            for (j = jmin; j < jmax; j++)
            {
                //x = ax * ((float)j - xc);
                //x *= x;
                r = fabs(y);//(float)sqrt(x + y);
                ri = (int)r;

                if (ri > r_max) continue;   /* bug break;*/

                z = ps[i].fl[j];
                p = r - ri;

                if (ri == r_max)
                {
                    p0 += (1 - p) * z;
                    pr0 += (1 - p);
                    //radial_dx[r_max] -= 1-p;
                }
                else
                {
                    rz[ri] += (1 - p) * z;
                    rz[ri + r_max] += (1 - p);
                    //radial_dx[ri] += p;
                }

                ri++;

                if (ri < r_max)
                {
                    rz[ri] += p * z;
                    rz[ri + r_max] += p;
                    //radial_dx[ri] -= 1-p;
                }
            }
        }
    }
    else if (type == IS_INT_IMAGE)
    {
        for (i = imin; i < imax; i++)
        {
            y = ay * ((float)i - yc);

            //y *= y;
            for (j = jmin; j < jmax; j++)
            {
                //x = ax * ((float)j - xc);
                //x *= x;
                r = fabs(y);//(float)sqrt(x + y);
                ri = (int)r;

                if (ri > r_max) continue;   /* bug break;*/

                z = (float)ps[i].in[j];
                p = r - ri;

                if (ri == r_max)
                {
                    p0 += (1 - p) * z;
                    pr0 += (1 - p);
                    //radial_dx[r_max] -= 1-p;
                }
                else
                {
                    rz[ri] += (1 - p) * z;
                    rz[ri + r_max] += (1 - p);
                    //radial_dx[ri] += p;
                }

                ri++;

                if (ri < r_max)
                {
                    rz[ri] += p * z;
                    rz[ri + r_max] += p;
                    //radial_dx[ri] -= 1-p;
                }
            }
        }
    }
    else if (type == IS_UINT_IMAGE)
    {
        for (i = imin; i < imax; i++)
        {
            y = ay * ((float)i - yc);

            //y *= y;
            for (j = jmin; j < jmax; j++)
            {
                //x = ax * ((float)j - xc);
                //x *= x;
                r = fabs(y); //(float)sqrt(x + y);
                ri = (int)r;

                if (ri > r_max) continue;   /* bug break;*/

                z = (float)ps[i].ui[j];
                p = r - ri;

                if (ri == r_max)
                {
                    p0 += (1 - p) * z;
                    pr0 += (1 - p);
                    //radial_dx[r_max] -= 1-p;
                }
                else
                {
                    rz[ri] += (1 - p) * z;
                    rz[ri + r_max] += (1 - p);
                    //radial_dx[ri] += p;
                }

                ri++;

                if (ri < r_max)
                {
                    rz[ri] += p * z;
                    rz[ri + r_max] += p;
                    //radial_dx[ri] -= 1-p;
                }
            }
        }
    }
    else if (type == IS_CHAR_IMAGE)
    {
        for (i = imin; i < imax; i++)
        {
            y = ay * ((float)i - yc);

            //y *= y;
            for (j = jmin; j < jmax; j++)
            {
                //x = ax * ((float)j - xc);
                //x *= x;
                r = fabs(y);//(float)sqrt(x + y);
                ri = (int)r;

                if (ri > r_max) continue;   /* bug break;*/

                z = (float)ps[i].ch[j];
                p = r - ri;

                if (ri == r_max)
                {
                    p0 += (1 - p) * z;
                    pr0 += (1 - p);
                    //radial_dx[r_max] -= 1-p;
                }
                else
                {
                    rz[ri] += (1 - p) * z;
                    rz[ri + r_max] += (1 - p);
                    //radial_dx[ri] += p;
                }

                ri++;

                if (ri < r_max)
                {
                    rz[ri] += p * z;
                    rz[ri + r_max] += p;
                    //radial_dx[ri] -= 1-p;
                }
            }
        }
    }

    for (i = 0; i < r_max; i++)
    {
        rz[i] = (rz[i + r_max] == 0) ? 0 : rz[i] / rz[i + r_max];
        //radial_dx[i] = (rz[i+r_max] == 0) ? 0 : radial_dx[i]/rz[i+r_max];
    }

    p0 = (pr0 == 0) ? 0 : p0 / pr0;

    for (i = 0; i < r_max; i++)     rz[i + r_max] = rz[i];

    for (i = 1; i < r_max; i++)     rz[r_max - i] = rz[r_max + i];

    rz[0] = p0;
    return rz;
}



/*  take a radial image profile of an image with a different scaling in
 *  x and y, real x = i_image * ax,  y = i_image * ay
 *
 *
 *
 */

float *mean_x_profile_in_array(float *rz, O_i *ois, float xc, float yc, int r_max, int r_max_y, float ax, float ay)
{
    int i, j;
    union pix *ps = NULL;
    int onx, ony, ri, type;
    float z = 0, z1, x, r, p, p0 = 0, pr0 = 0;
    int imin, imax, jmin, jmax; // xci, yci,

    //for (i = 0; i < 2*r_max && i < 256; i++)  radial_dx[i] = 0;
    if (ois == NULL)    return NULL;

    if (rz == NULL)     rz = (float *)calloc(2 * r_max, sizeof(float));

    if (rz == NULL)     return NULL;

    ps = ois->im.pixel;
    ony = ois->im.ny;
    onx = ois->im.nx;
    type = ois->im.data_type;

    for (i = 0; i < 2 * r_max; i++)   rz[i] = 0;

    //xci = (int)xc;
    //yci = (int)yc;

    imin = (int)(yc - (r_max_y / ay));
    imax = (int)(0.5 + yc + (r_max_y / ay));
    jmin = (int)(xc -  r_max / ax);
    jmax = (int)(0.5 + xc + r_max / ax);
    imin = (imin < 0) ? 0 : imin;
    imax = (imax > ony) ? ony : imax;
    jmin = (jmin < 0) ? 0 : jmin;
    jmax = (jmax > onx) ? onx : jmax;

    if (type == IS_COMPLEX_IMAGE)
    {
        for (i = imin; i < imax; i++)
        {
            //y = ay * ((float)i - yc);
            //y *= y;
            for (j = jmin; j < jmax; j++)
            {
                x = ax * ((float)j - xc);
                //x *= x;
                r = fabs(x);//(float)sqrt(x + y);
                ri = (int)r;

                if (ri > r_max) continue;   /* bug break;*/

                if (ois->im.mode == RE)
                    z = ps[i].fl[2 * j];
                else if (ois->im.mode == IM)
                    z = ps[i].fl[2 * j + 1];
                else if (ois->im.mode == AMP)
                {
                    z = ps[i].fl[2 * j];
                    z1 = ps[i].fl[2 * j + 1];
                    z = sqrt(z * z + z1 * z1);
                }
                else if (ois->im.mode == AMP_2)
                {
                    z = ps[i].fl[2 * j];
                    z1 = ps[i].fl[2 * j + 1];
                    z = z * z + z1 * z1;
                }
                else if (ois->im.mode == LOG_AMP)
                {
                    z = ps[i].fl[2 * j];
                    z1 = ps[i].fl[2 * j + 1];
                    z = z * z + z1 * z1;
                    z = (z > 0) ? log10(z) : -40.0;
                }
                else
                {
                    win_printf("Unknown mode for complex image");
                    return NULL;
                }

                p = r - ri;

                if (ri == r_max)
                {
                    p0 += (1 - p) * z;
                    pr0 += (1 - p);
                    //radial_dx[r_max] -= 1-p;
                }
                else
                {
                    rz[ri] += (1 - p) * z;
                    rz[ri + r_max] += (1 - p);
                    //radial_dx[ri] += p;
                }

                ri++;

                if (ri < r_max)
                {
                    rz[ri] += p * z;
                    rz[ri + r_max] += p;
                    //radial_dx[ri] -= 1-p;
                }
            }
        }
    }
    else if (type == IS_FLOAT_IMAGE)
    {
        for (i = imin; i < imax; i++)
        {
            //y = ay * ((float)i - yc);
            //y *= y;
            for (j = jmin; j < jmax; j++)
            {
                x = ax * ((float)j - xc);
                //x *= x;
                r = fabs(x);//(float)sqrt(x + y);
                ri = (int)r;

                if (ri > r_max) continue;   /* bug break;*/

                z = ps[i].fl[j];
                p = r - ri;

                if (ri == r_max)
                {
                    p0 += (1 - p) * z;
                    pr0 += (1 - p);
                    //radial_dx[r_max] -= 1-p;
                }
                else
                {
                    rz[ri] += (1 - p) * z;
                    rz[ri + r_max] += (1 - p);
                    //radial_dx[ri] += p;
                }

                ri++;

                if (ri < r_max)
                {
                    rz[ri] += p * z;
                    rz[ri + r_max] += p;
                    //radial_dx[ri] -= 1-p;
                }
            }
        }
    }
    else if (type == IS_INT_IMAGE)
    {
        for (i = imin; i < imax; i++)
        {
            //y = ay * ((float)i - yc);
            //y *= y;
            for (j = jmin; j < jmax; j++)
            {
                x = ax * ((float)j - xc);
                //x *= x;
                r = fabs(x);//(float)sqrt(x + y);
                ri = (int)r;

                if (ri > r_max) continue;   /* bug break;*/

                z = (float)ps[i].in[j];
                p = r - ri;

                if (ri == r_max)
                {
                    p0 += (1 - p) * z;
                    pr0 += (1 - p);
                    //radial_dx[r_max] -= 1-p;
                }
                else
                {
                    rz[ri] += (1 - p) * z;
                    rz[ri + r_max] += (1 - p);
                    //radial_dx[ri] += p;
                }

                ri++;

                if (ri < r_max)
                {
                    rz[ri] += p * z;
                    rz[ri + r_max] += p;
                    //radial_dx[ri] -= 1-p;
                }
            }
        }
    }
    else if (type == IS_UINT_IMAGE)
    {
        for (i = imin; i < imax; i++)
        {
            //y = ay * ((float)i - yc);
            //y *= y;
            for (j = jmin; j < jmax; j++)
            {
                x = ax * ((float)j - xc);
                //x *= x;
                r = fabs(x); //(float)sqrt(x + y);
                ri = (int)r;

                if (ri > r_max) continue;   /* bug break;*/

                z = (float)ps[i].ui[j];
                p = r - ri;

                if (ri == r_max)
                {
                    p0 += (1 - p) * z;
                    pr0 += (1 - p);
                    //radial_dx[r_max] -= 1-p;
                }
                else
                {
                    rz[ri] += (1 - p) * z;
                    rz[ri + r_max] += (1 - p);
                    //radial_dx[ri] += p;
                }

                ri++;

                if (ri < r_max)
                {
                    rz[ri] += p * z;
                    rz[ri + r_max] += p;
                    //radial_dx[ri] -= 1-p;
                }
            }
        }
    }
    else if (type == IS_CHAR_IMAGE)
    {
        for (i = imin; i < imax; i++)
        {
            //y = ay * ((float)i - yc);
            //y *= y;
            for (j = jmin; j < jmax; j++)
            {
                x = ax * ((float)j - xc);
                //x *= x;
                r = fabs(x);//(float)sqrt(x + y);
                ri = (int)r;

                if (ri > r_max) continue;   /* bug break;*/

                z = (float)ps[i].ch[j];
                p = r - ri;

                if (ri == r_max)
                {
                    p0 += (1 - p) * z;
                    pr0 += (1 - p);
                    //radial_dx[r_max] -= 1-p;
                }
                else
                {
                    rz[ri] += (1 - p) * z;
                    rz[ri + r_max] += (1 - p);
                    //radial_dx[ri] += p;
                }

                ri++;

                if (ri < r_max)
                {
                    rz[ri] += p * z;
                    rz[ri + r_max] += p;
                    //radial_dx[ri] -= 1-p;
                }
            }
        }
    }

    for (i = 0; i < r_max; i++)
    {
        rz[i] = (rz[i + r_max] == 0) ? 0 : rz[i] / rz[i + r_max];
        //radial_dx[i] = (rz[i+r_max] == 0) ? 0 : radial_dx[i]/rz[i+r_max];
    }

    p0 = (pr0 == 0) ? 0 : p0 / pr0;

    for (i = 0; i < r_max; i++)     rz[i + r_max] = rz[i];

    for (i = 1; i < r_max; i++)     rz[r_max - i] = rz[r_max + i];

    rz[0] = p0;
    return rz;
}



/*  take a radial image profile of an image with a different scaling in
 *  x and y, real x = i_image * ax,  y = i_image * ay
 *
 *      use smid P4 instruction
 *
 */
#ifndef XV_NO_SSE
float *radial_non_pixel_square_image_sym_profile_in_array_sse(float *rz, O_i *ois, float xc, float yc, int r_max,
        float ax, float ay)
{
    int i, j;
    union pix *ps = NULL;
    int onx, ony, ri, type, onx_4;
    float z = 0, *r, p, p0 = 0, pr0 = 0, rmax2, tmp; // , z1, x, y
    int imin, imax, jmin, jmax, jmin_4, jmax_4;



    //for (i = 0; i < 2*r_max && i < 256; i++)  radial_dx[i] = 0;
    if (ois == NULL)    return NULL;

    if (rz == NULL)     rz = (float *)calloc(2 * r_max, sizeof(float));

    if (rz == NULL)     return NULL;

    ps = ois->im.pixel;
    ony = ois->im.ny;
    onx = ois->im.nx;
    onx_4 = onx >> 2;
    type = ois->im.data_type;

    for (i = 0; i < 2 * r_max; i++)   rz[i] = 0;

    rmax2 = r_max * r_max;
    imin = (int)(yc - (r_max / ay));
    imax = (int)(0.5 + yc + (r_max / ay));
    jmin = (int)(xc -  r_max / ax);
    jmax = (int)(0.5 + xc + r_max / ax);
    imin = (imin < 0) ? 0 : imin;
    imax = (imax > ony) ? ony : imax;
    jmin = (jmin < 0) ? 0 : jmin;
    jmax = (jmax > onx) ? onx : jmax;
    //  jmin_4 = jmin = jmin >> 2;  jmin <<= 2;
    //jmax_4 = jmax = jmax >> 2;    jmax++; jmax <<= 2;
    jmin_4 = jmin >> 2;
    jmax_4 = 1 + (jmax >> 2);
    jmax_4 = (jmax_4 > onx_4) ? onx_4 : jmax_4;


    /*win_printf("Imin %d imax %d, jmin %d jmax %d\njmin_4 %d jmax_4 %d"
      ,imin,imax,jmin,jmax,jmin_4,jmax_4); */

    fxc.f[0] = fxc.f[1] = fxc.f[2] = fxc.f[3] = xc;
    fyc.f[0] = fyc.f[1] = fyc.f[2] = fyc.f[3] = yc;
    fax.f[0] = fax.f[1] = fax.f[2] = fax.f[3] = ax;
    fay.f[0] = fay.f[1] = fay.f[2] = fay.f[3] = ay;
    f4.f[0] = f4.f[1] = f4.f[2] = f4.f[3] = 4;
    r = fr[0].f;

    if (type == IS_INT_IMAGE)
    {
        f0.f[0] = 0;
        f0.f[1] = 1;
        f0.f[2] = 2;
        f0.f[3] = 3;  // pixel index
        fx.v = __builtin_ia32_subps(f0.v, fxc.v);            // distance in pixel from center
        fx.v = __builtin_ia32_mulps(fax.v, fx.v);            // rescaling in x
        f4.v = __builtin_ia32_mulps(fax.v, f4.v);            // we prepare rescaled substraction

        for (j = 0; j < onx_4; j++)
        {
            fx2[j].v = __builtin_ia32_mulps(fx.v, fx.v);     // we compute X2
            fx.v = __builtin_ia32_addps(fx.v, f4.v);         // we move index
        }

        f0.f[0] = f0.f[1] = f0.f[2] = f0.f[3] = imin;        // starting y position
        fy.v = __builtin_ia32_subps(f0.v, fyc.v);            // distance in pixel to yc
        fy.v = __builtin_ia32_mulps(fay.v, fy.v);            // rescaled in y


        for (i = imin; i < imax; i++)
        {
            fy2.v = __builtin_ia32_mulps(fy.v, fy.v);        // we compute y2
            fy.v = __builtin_ia32_addps(fy.v, fay.v);        // move by one rescaled pixel in y
            tmp = rmax2 - fy2.f[0];

            if (tmp < 0) continue;

            tmp = sqrt(tmp);
            tmp = (tmp + 1) / ax;
            jmin = (int)(xc - tmp);
            jmax = (int)(xc + tmp + 1);
            jmin = (jmin < 0) ? 0 : jmin;
            jmax = (jmax > onx) ? onx : jmax;
            jmin_4 = jmin >> 2;
            jmax_4 = 1 + (jmax >> 2);
            jmax_4 = (jmax_4 > onx_4) ? onx_4 : jmax_4;

            for (j = jmin_4; j < jmax_4; j++)
            {
                fr[j].v = __builtin_ia32_addps(fy2.v, fx2[j].v);  // we compute x2 + y2
                fr[j].v = __builtin_ia32_sqrtps(fr[j].v);         // we get the distance
            }

            for (j = jmin; j < jmax; j++)
            {
                ri = (int)r[j];

                if (ri > r_max) continue;   /* bug break;*/

                z = (float)ps[i].in[j];
                p = r[j] - ri;

                if (ri == r_max)
                {
                    p0 += (1 - p) * z;
                    pr0 += (1 - p);
                    //radial_dx[r_max] -= 1-p;
                }
                else
                {
                    rz[ri] += (1 - p) * z;
                    rz[ri + r_max] += (1 - p);
                    //radial_dx[ri] += p;
                }

                ri++;

                if (ri < r_max)
                {
                    rz[ri] += p * z;
                    rz[ri + r_max] += p;
                    //radial_dx[ri] -= 1-p;
                }
            }
        }
    }
    else if (type == IS_CHAR_IMAGE)
    {
        f0.f[0] = 0;
        f0.f[1] = 1;
        f0.f[2] = 2;
        f0.f[3] = 3;  // pixel index
        fx.v = __builtin_ia32_subps(f0.v, fxc.v);            // distance in pixel from center
        fx.v = __builtin_ia32_mulps(fax.v, fx.v);            // rescaling in x
        f4.v = __builtin_ia32_mulps(fax.v, f4.v);            // we prepare rescaled substraction

        for (j = 0; j < onx_4; j++)
        {
            fx2[j].v = __builtin_ia32_mulps(fx.v, fx.v);     // we compute X2
            fx.v = __builtin_ia32_addps(fx.v, f4.v);         // we move index
        }

        f0.f[0] = f0.f[1] = f0.f[2] = f0.f[3] = imin;        // starting y position
        fy.v = __builtin_ia32_subps(f0.v, fyc.v);            // distance in pixel to yc
        fy.v = __builtin_ia32_mulps(fay.v, fy.v);            // rescaled in y

        for (i = imin; i < imax; i++)
        {
            fy2.v = __builtin_ia32_mulps(fy.v, fy.v);        // we compute y2
            fy.v = __builtin_ia32_addps(fy.v, fay.v);        // move by one rescaled pixel in y
            tmp = rmax2 - fy2.f[0];

            if (tmp < 0) continue;

            tmp = sqrt(tmp);
            tmp = (tmp + 1) / ax;
            jmin = (int)(xc - tmp);
            jmax = (int)(xc + tmp + 1);
            jmin = (jmin < 0) ? 0 : jmin;
            jmax = (jmax > onx) ? onx : jmax;
            jmin_4 = jmin >> 2;
            jmax_4 = 1 + (jmax >> 2);
            jmax_4 = (jmax_4 > onx_4) ? onx_4 : jmax_4;

            for (j = jmin_4; j < jmax_4; j++)
            {
                fr[j].v = __builtin_ia32_addps(fy2.v, fx2[j].v);  // we compute x2 + y2
                fr[j].v = __builtin_ia32_sqrtps(fr[j].v);         // we get the distance
                /*
                   if ((debug != CANCEL) && (j == (onx >> 3)) && (i%16 == 0))
                   debug = win_printf("y2 = %g i = %d j %d\n%g %g %g %g\n%g %g %g %g",
                   fy2.f[0],i,j,fr[j].f[0],fr[j].f[1],fr[j].f[2],fr[j].f[3],
                   r[4*j],r[(4*j)+1],r[(4*j)+2],r[(4*j)+3]);
                   */
            }

            for (j = jmin; j < jmax; j++)
            {
                ri = (int)r[j];

                if (ri > r_max) continue;   /* bug break;*/

                z = (float)ps[i].ch[j];
                p = r[j] - ri;

                if (ri == r_max)
                {
                    p0 += (1 - p) * z;
                    pr0 += (1 - p);
                    //radial_dx[r_max] -= 1-p;
                }
                else
                {
                    rz[ri] += (1 - p) * z;
                    rz[ri + r_max] += (1 - p);
                    //radial_dx[ri] += p;
                }

                ri++;

                if (ri < r_max)
                {
                    rz[ri] += p * z;
                    rz[ri + r_max] += p;
                    //radial_dx[ri] -= 1-p;
                }
            }
        }
    }
    else if (type == IS_UINT_IMAGE)
    {
        f0.f[0] = 0;
        f0.f[1] = 1;
        f0.f[2] = 2;
        f0.f[3] = 3;  // pixel index
        fx.v = __builtin_ia32_subps(f0.v, fxc.v);            // distance in pixel from center
        fx.v = __builtin_ia32_mulps(fax.v, fx.v);            // rescaling in x
        f4.v = __builtin_ia32_mulps(fax.v, f4.v);            // we prepare rescaled substraction

        for (j = 0; j < onx_4; j++)
        {
            fx2[j].v = __builtin_ia32_mulps(fx.v, fx.v);     // we compute X2
            fx.v = __builtin_ia32_addps(fx.v, f4.v);         // we move index
        }

        f0.f[0] = f0.f[1] = f0.f[2] = f0.f[3] = imin;        // starting y position
        fy.v = __builtin_ia32_subps(f0.v, fyc.v);            // distance in pixel to yc
        fy.v = __builtin_ia32_mulps(fay.v, fy.v);            // rescaled in y

        for (i = imin; i < imax; i++)
        {
            fy2.v = __builtin_ia32_mulps(fy.v, fy.v);        // we compute y2
            fy.v = __builtin_ia32_addps(fy.v, fay.v);        // move by one rescaled pixel in y
            tmp = rmax2 - fy2.f[0];

            if (tmp < 0) continue;

            tmp = sqrt(tmp);
            tmp = (tmp + 1) / ax;
            jmin = (int)(xc - tmp);
            jmax = (int)(xc + tmp + 1);
            jmin = (jmin < 0) ? 0 : jmin;
            jmax = (jmax > onx) ? onx : jmax;
            jmin_4 = jmin >> 2;
            jmax_4 = 1 + (jmax >> 2);
            jmax_4 = (jmax_4 > onx_4) ? onx_4 : jmax_4;

            for (j = jmin_4; j < jmax_4; j++)
            {
                fr[j].v = __builtin_ia32_addps(fy2.v, fx2[j].v);  // we compute x2 + y2
                fr[j].v = __builtin_ia32_sqrtps(fr[j].v);         // we get the distance
                /*
                   if ((debug != CANCEL) && (j == (onx >> 3)) && (i%16 == 0))
                   debug = win_printf("y2 = %g i = %d j %d\n%g %g %g %g\n%g %g %g %g",
                   fy2.f[0],i,j,fr[j].f[0],fr[j].f[1],fr[j].f[2],fr[j].f[3],
                   r[4*j],r[(4*j)+1],r[(4*j)+2],r[(4*j)+3]);
                   */
            }

            for (j = jmin; j < jmax; j++)
            {
                ri = (int)r[j];

                if (ri > r_max) continue;   /* bug break;*/

                z = (float)ps[i].ui[j];
                p = r[j] - ri;

                if (ri == r_max)
                {
                    p0 += (1 - p) * z;
                    pr0 += (1 - p);
                    //radial_dx[r_max] -= 1-p;
                }
                else
                {
                    rz[ri] += (1 - p) * z;
                    rz[ri + r_max] += (1 - p);
                    //radial_dx[ri] += p;
                }

                ri++;

                if (ri < r_max)
                {
                    rz[ri] += p * z;
                    rz[ri + r_max] += p;
                    //radial_dx[ri] -= 1-p;
                }
            }
        }
    }

    for (i = 0; i < r_max; i++)
    {
        rz[i] = (rz[i + r_max] == 0) ? 0 : rz[i] / rz[i + r_max];
        //radial_dx[i] = (rz[i+r_max] == 0) ? 0 : radial_dx[i]/rz[i+r_max];
    }

    p0 = (pr0 == 0) ? 0 : p0 / pr0;

    for (i = 0; i < r_max; i++)     rz[i + r_max] = rz[i];

    for (i = 1; i < r_max; i++)     rz[r_max - i] = rz[r_max + i];

    if (rz[r_max] == 0) rz[r_max] = rz[r_max + 1];

    rz[0] = p0;
    return rz;
}
#endif

/*


*/
float *orthoradial_non_pixel_square_image_sym_profile_in_array(float *orz,
        O_i *ois, float xc, float yc, int r_mean, int r_width, int orz_size, float ax, float ay)
{
    int i, j;
    union pix *ps = NULL;
    //static int last_size = 0;
    int onx, ony, ori, type;
    float z = 0, z1, x, y, x2, y2, r2, r, p, appo, pi_w;
    float r2f_max, r2f_min, theta, ora[256]; //*ora = NULL;
    int imin, imax, jmin, jmax, r_max, r_min, nx2;


    if (ois == NULL || orz_size == 0)   return NULL;

    /*
       if (last_size != orz_size)
       {
       if (ora) free(ora);
       ora = NULL;
       }
       if (ora == NULL) ora = (float*)calloc(last_size=orz_size,sizeof(float));
       if (ora == NULL) return NULL;
       */
    if (orz == NULL)    orz = (float *)calloc(orz_size, sizeof(float));

    if (orz == NULL)    return NULL;

    ps = ois->im.pixel;
    ony = ois->im.ny;
    onx = ois->im.nx;
    type = ois->im.data_type;

    for (i = 0; i < orz_size; i++)  orz[i] = ora[i] = 0;

    nx2 = orz_size / 2;
    r_max = r_mean + r_width;
    r2f_max = (float)r_max * (float)r_max;
    r_min = r_mean - r_width;
    r2f_min = (float)r_min * (float)r_min;
    pi_w = M_PI / r_width;

    imin = (int)(yc - (r_max / ay));
    imax = (int)(0.5 + yc + (r_max / ay));
    jmin = (int)(xc -  r_max / ax);
    jmax = (int)(0.5 + xc + r_max / ax);
    imin = (imin < 0) ? 0 : imin;
    imax = (imax > ony) ? ony : imax;
    jmin = (jmin < 0) ? 0 : jmin;
    jmax = (jmax > onx) ? onx : jmax;

    if (type == IS_COMPLEX_IMAGE)
    {
        for (i = imin; i < imax; i++)
        {
            y = ay * ((float)i - yc);
            y2 = y * y;

            for (j = jmin; j < jmax; j++)
            {
                x = ax * ((float)j - xc);
                x2 = x * x;
                r2 = x2 + y2;

                if (r2 > r2f_max || r2 < r2f_min)   continue;

                theta = atan2(y, x);
                theta /= M_PI;
                theta *= nx2;
                theta += nx2;
                ori = (int)theta;

                if (ois->im.mode == RE)
                    z = ps[i].fl[2 * j];
                else if (ois->im.mode == IM)
                    z = ps[i].fl[2 * j + 1];
                else if (ois->im.mode == AMP)
                {
                    z = ps[i].fl[2 * j];
                    z1 = ps[i].fl[2 * j + 1];
                    z = sqrt(z * z + z1 * z1);
                }
                else if (ois->im.mode == AMP_2)
                {
                    z = ps[i].fl[2 * j];
                    z1 = ps[i].fl[2 * j + 1];
                    z = z * z + z1 * z1;
                }
                else if (ois->im.mode == LOG_AMP)
                {
                    z = ps[i].fl[2 * j];
                    z1 = ps[i].fl[2 * j + 1];
                    z = z * z + z1 * z1;
                    z = (z > 0) ? log10(z) : -40.0;
                }
                else
                {
                    win_printf("Unknown mode for complex image");
                    return NULL;
                }

                p = theta - ori;

                if (ori < 0) win_printf("index < 0");

                ori %= orz_size;

                if (ori >= orz_size) win_printf("index out of range");

                orz[ori] += (1 - p) * z;
                ora[ori] += (1 - p);
                ori++;
                ori %= orz_size;
                orz[ori] += p * z;
                ora[ori] += p;
            }
        }
    }
    else if (type == IS_FLOAT_IMAGE)
    {
        for (i = imin; i < imax; i++)
        {
            y = ay * ((float)i - yc);
            y2 = y * y;

            for (j = jmin; j < jmax; j++)
            {
                x = ax * ((float)j - xc);
                x2 = x * x;
                r2 = x2 + y2;

                if (r2 > r2f_max || r2 < r2f_min)   continue;

                theta = atan2(y, x);
                theta /= M_PI;
                theta *= nx2;
                theta += nx2;
                ori = (int)theta;
                z = ps[i].fl[j];
                p = theta - ori;

                if (ori < 0) win_printf("index < 0");

                ori %= orz_size;

                if (ori >= orz_size) win_printf("index out of range");

                orz[ori] += (1 - p) * z;
                ora[ori] += (1 - p);
                ori++;
                ori %= orz_size;
                orz[ori] += p * z;
                ora[ori] += p;
            }
        }
    }
    else if (type == IS_INT_IMAGE)
    {
        for (i = imin; i < imax; i++)
        {
            y = ay * ((float)i - yc);
            y2 = y * y;

            for (j = jmin; j < jmax; j++)
            {
                x = ax * ((float)j - xc);
                x2 = x * x;
                r2 = x2 + y2;

                if (r2 > r2f_max || r2 < r2f_min)   continue;

                theta = atan2(y, x);
                theta /= M_PI;
                theta *= nx2;
                theta += nx2;
                ori = (int)theta;
                z = (float)ps[i].in[j];
                p = theta - ori;

                if (ori < 0) win_printf("index < 0");

                ori %= orz_size;

                if (ori >= orz_size) win_printf("index out of range");

                orz[ori] += (1 - p) * z;
                ora[ori] += (1 - p);
                ori++;
                ori %= orz_size;
                orz[ori] += p * z;
                ora[ori] += p;
            }
        }
    }
    else if (type == IS_UINT_IMAGE)
    {
        for (i = imin; i < imax; i++)
        {
            y = ay * ((float)i - yc);
            y2 = y * y;

            for (j = jmin; j < jmax; j++)
            {
                x = ax * ((float)j - xc);
                x2 = x * x;
                r2 = x2 + y2;

                if (r2 > r2f_max || r2 < r2f_min)   continue;

                theta = atan2(y, x);
                theta /= M_PI;
                theta *= nx2;
                theta += nx2;
                ori = (int)theta;
                z = (float)ps[i].ui[j];
                p = theta - ori;

                if (ori < 0) win_printf("index < 0");

                ori %= orz_size;

                if (ori >= orz_size) win_printf("index out of range");

                orz[ori] += (1 - p) * z;
                ora[ori] += (1 - p);
                ori++;
                ori %= orz_size;
                orz[ori] += p * z;
                ora[ori] += p;
            }
        }
    }
    else if (type == IS_CHAR_IMAGE)
    {
        for (i = imin; i < imax; i++)
        {
            y = ay * ((float)i - yc);
            y2 = y * y;

            for (j = jmin; j < jmax; j++)
            {
                x = ax * ((float)j - xc);
                x2 = x * x;
                r2 = x2 + y2;

                if (r2 > r2f_max || r2 < r2f_min)   continue;

                r = sqrt(r2);
                appo = r - r_mean;
                appo *= pi_w;
                appo = 1 + cos(appo);

                theta = atan2(y, x);
                //          win_printf("X %g Y %g \n\\theta  = %g -> %d",x,y,theta,(int)((orz_size*(theta+M_PI))/(2*M_PI)));
                theta /= M_PI;
                theta *= nx2;
                theta += nx2;
                ori = (int)theta;
                z = (float)ps[i].ch[j];
                p = theta - ori;

                if (ori < 0) win_printf("index < 0");

                ori %= orz_size;

                if (ori >= orz_size) win_printf("index out of range");

                orz[ori] += (1 - p) * z * appo;
                ora[ori] += (1 - p) * appo;
                ori++;
                ori %= orz_size;
                orz[ori] += p * z * appo;
                ora[ori] += p * appo;
            }
        }
    }

    for (i = 0; i < orz_size; i++)
    {
        orz[i] = (ora[i] == 0) ? 0 : orz[i] / ora[i];
    }

    return orz;
}




/*  take a radial image profile of an image with a different scaling in
 *  x and y, real x = i_image * ax,  y = i_image * ay
 *
 *
 *
 */
# define N0_APO 128
# define N_APO 4

float *radial_non_pixel_square_image_sym_profile_in_array_apodise(float *rz, O_i *ois, float xc, float yc, int r_max, float ax,
        float ay)
{
  register int i, j, k;
    union pix *ps = NULL;
    int onx, ony, ri, ril, type, iapo;
    float z = 0, z1, x, y, r, p, p0 = 0, pr0 = 0;
    static float apo[N_APO*N0_APO] = {0};
    static int apo_init = 0;
    int imin, imax, jmin, jmax, n_apo_2; //  xci, yci
# if !defined(PLAYITSAM) && !defined(PIAS)
    float eva_black = 0, eva_level = 0, eva_norm = 0, thres_w = 0.001, eva_w;
    int n_eva_black = 0;
# endif


    if (apo_init == 0)
      {
	apo_init = N_APO*N0_APO;
	for (i = 0; i < apo_init; i++)
	  apo[i] = 0.5*(1-cos((M_PI*2*i)/apo_init));
      }
    n_apo_2 = N_APO/2;
    //for (i = 0; i < 2*r_max && i < 256; i++)  radial_dx[i] = 0;
    //for (i = 0; i < 4096; i++)      evanescent_black_level_histo[i] = 0;
    if (ois == NULL)    return NULL;

    if (rz == NULL)     rz = (float *)calloc(2 * r_max, sizeof(float));

    if (rz == NULL)     return NULL;

    ps = ois->im.pixel;
    ony = ois->im.ny;
    onx = ois->im.nx;
    type = ois->im.data_type;

    for (i = 0; i < 2 * r_max; i++)   rz[i] = 0;

    //xci = (int)xc;
    //yci = (int)yc;

    imin = (int)(yc - (r_max / ay));
    imax = (int)(0.5 + yc + (r_max / ay));
    jmin = (int)(xc -  r_max / ax);
    jmax = (int)(0.5 + xc + r_max / ax);
    imin = (imin < 0) ? 0 : imin;
    imax = (imax > ony) ? ony : imax;
    jmin = (jmin < 0) ? 0 : jmin;
    jmax = (jmax > onx) ? onx : jmax;

    if (type == IS_COMPLEX_IMAGE)
    {
        for (i = imin; i < imax; i++)
        {
            y = ay * ((float)i - yc);
            y *= y;

            for (j = jmin; j < jmax; j++)
            {
                x = ax * ((float)j - xc);
                x *= x;
                r = (float)sqrt(x + y);
                ri = (int)r;

                if (ri > r_max) continue;   /* bug break;*/

                if (ois->im.mode == RE)
                    z = ps[i].fl[2 * j];
                else if (ois->im.mode == IM)
                    z = ps[i].fl[2 * j + 1];
                else if (ois->im.mode == AMP)
                {
                    z = ps[i].fl[2 * j];
                    z1 = ps[i].fl[2 * j + 1];
                    z = sqrt(z * z + z1 * z1);
                }
                else if (ois->im.mode == AMP_2)
                {
                    z = ps[i].fl[2 * j];
                    z1 = ps[i].fl[2 * j + 1];
                    z = z * z + z1 * z1;
                }
                else if (ois->im.mode == LOG_AMP)
                {
                    z = ps[i].fl[2 * j];
                    z1 = ps[i].fl[2 * j + 1];
                    z = z * z + z1 * z1;
                    z = (z > 0) ? log10(z) : -40.0;
                }
                else
                {
                    win_printf("Unknown mode for complex image");
                    return NULL;
                }

                p = r - ri;

                if (ri == r_max)
                {
                    p0 += (1 - p) * z;
                    pr0 += (1 - p);
                    //radial_dx[r_max] -= 1-p;
                }
                else
                {
                    rz[ri] += (1 - p) * z;
                    rz[ri + r_max] += (1 - p);
                    //radial_dx[ri] += p;
                }

# if !defined(PLAYITSAM) && !defined(PIAS)
                eva_w = (1 - p) * evanescent_spot_weighting[ri];
# endif
                ri++;

                if (ri < r_max)
                {
                    rz[ri] += p * z;
                    rz[ri + r_max] += p;
                    //radial_dx[ri] -= 1-p;
                }

# if !defined(PLAYITSAM) && !defined(PIAS)
                eva_w += p * evanescent_spot_weighting[ri];

                if (eva_w < thres_w)
                {
                    eva_black += z;
                    n_eva_black++;
                }
                else
                {
                    eva_level += eva_w * z;
                    eva_norm += eva_w;
                }

# endif
            }
        }
    }
    else if (type == IS_FLOAT_IMAGE)
    {
        for (i = imin; i < imax; i++)
        {
            y = ay * ((float)i - yc);
            y *= y;

            for (j = jmin; j < jmax; j++)
            {
                x = ax * ((float)j - xc);
                x *= x;
                r = (float)sqrt(x + y);
                ri = (int)r;

                if (ri > r_max) continue;   /* bug break;*/

                z = ps[i].fl[j];
                p = r - ri;
# if !defined(PLAYITSAM) && !defined(PIAS)
                eva_w = (1 - p) * evanescent_spot_weighting[ri];
# endif

                if (ri == r_max)
                {
                    p0 += (1 - p) * z;
                    pr0 += (1 - p);
                    //radial_dx[r_max] -= 1-p;
                }
                else
                {
                    rz[ri] += (1 - p) * z;
                    rz[ri + r_max] += (1 - p);
                    //radial_dx[ri] += p;
                }

                ri++;

                if (ri < r_max)
                {
                    rz[ri] += p * z;
                    rz[ri + r_max] += p;
                    //radial_dx[ri] -= 1-p;
                }

# if !defined(PLAYITSAM) && !defined(PIAS)
                eva_w += p * evanescent_spot_weighting[ri];

                if (eva_w < thres_w)
                {
                    eva_black += z;
                    n_eva_black++;
                }
                else
                {
                    eva_level += eva_w * z;
                    eva_norm += eva_w;
                }

# endif
            }
        }
    }
    else if (type == IS_INT_IMAGE)
    {
        for (i = imin; i < imax; i++)
        {
            y = ay * ((float)i - yc);
            y *= y;

            for (j = jmin; j < jmax; j++)
            {
                x = ax * ((float)j - xc);
                x *= x;
                r = (float)sqrt(x + y);
                ri = (int)r;

                if (ri > r_max) continue;   /* bug break;*/

                z = (float)ps[i].in[j];
                p = r - ri;
# if !defined(PLAYITSAM) && !defined(PIAS)
                eva_w = (1 - p) * evanescent_spot_weighting[ri];
# endif

                if (ri == r_max)
                {
                    p0 += (1 - p) * z;
                    pr0 += (1 - p);
                    //radial_dx[r_max] -= 1-p;
                }
                else
                {
                    rz[ri] += (1 - p) * z;
                    rz[ri + r_max] += (1 - p);
                    //radial_dx[ri] += p;
                }

                ri++;

                if (ri < r_max)
                {
                    rz[ri] += p * z;
                    rz[ri + r_max] += p;
                    //radial_dx[ri] -= 1-p;
                }

# if !defined(PLAYITSAM) && !defined(PIAS)
                eva_w += p * evanescent_spot_weighting[ri];

                if (eva_w < thres_w)
                {
                    eva_black += z;
                    n_eva_black++;
                }
                else
                {
                    eva_level += eva_w * z;
                    eva_norm += eva_w;
                }

# endif
            }
        }
    }
    else if (type == IS_UINT_IMAGE)
    {
        for (i = imin; i < imax; i++)
        {
            y = ay * ((float)i - yc);
            y *= y;

            for (j = jmin; j < jmax; j++)
            {
                x = ax * ((float)j - xc);
                x *= x;
                r = (float)sqrt(x + y);
                ri = (int)r;

                if (ri > r_max) continue;   /* bug break;*/

                z = (float)ps[i].ui[j];
                p = r - ri;
# if !defined(PLAYITSAM) && !defined(PIAS)
                eva_w = (1 - p) * evanescent_spot_weighting[ri];
# endif

                if (ri == r_max)
                {
                    p0 += (1 - p) * z;
                    pr0 += (1 - p);
                    //radial_dx[r_max] -= 1-p;
                }
                else
                {
                    rz[ri] += (1 - p) * z;
                    rz[ri + r_max] += (1 - p);
                    //radial_dx[ri] += p;
                }

                ri++;

                if (ri < r_max)
                {
                    rz[ri] += p * z;
                    rz[ri + r_max] += p;
                    //radial_dx[ri] -= 1-p;
                }

# if !defined(PLAYITSAM) && !defined(PIAS)
                eva_w += p * evanescent_spot_weighting[ri];

                if (eva_w < thres_w)
                {
                    eva_black += z;
                    n_eva_black++;
                }
                else
                {
                    eva_level += eva_w * z;
                    eva_norm += eva_w;
                }

# endif
            }
        }
    }
    else if (type == IS_CHAR_IMAGE)
    {
        for (i = imin; i < imax; i++)
        {
            y = ay * ((float)i - yc);
            y *= y;

            for (j = jmin; j < jmax; j++)
            {
                x = ax * ((float)j - xc);
                x *= x;
                r = (float)sqrt(x + y);
                ri = (int)r;

                if (ri > r_max) continue;   /* bug break;*/

                z = (float)ps[i].ch[j];
                p = r - ri;
# if !defined(PLAYITSAM) && !defined(PIAS)
                eva_w = (1 - p) * evanescent_spot_weighting[ri];
# endif

		for (k = 0; k < N_APO; k++)
		  {
		    iapo = (int)(0.5 + (p + k) * N0_APO);
		    ril = ri + k - n_apo_2 + 1;

		    if (ril == r_max)
		      {
			p0 += apo[iapo] * z;
			pr0 += apo[iapo];
			//radial_dx[r_max] -= 1-p;
		      }
		    else if (ril < r_max)
		      {
			ril = abs(ril);
			rz[ril] += apo[iapo] * z;
			rz[ril + r_max] += apo[iapo];
			//radial_dx[ri] += p;
		      }
		  }

# if !defined(PLAYITSAM) && !defined(PIAS)
                eva_w += p * evanescent_spot_weighting[ri];

                if (eva_w < thres_w)
                {
                    eva_black += z;
                    n_eva_black++;
                }
                else
                {
                    eva_level += eva_w * z;
                    eva_norm += eva_w;
                }

# endif
            }
        }
    }

    for (i = 0; i < r_max; i++)
    {
        rz[i] = (rz[i + r_max] == 0) ? 0 : rz[i] / rz[i + r_max];
        //      radial_dx[i] = (rz[i+r_max] == 0) ? 0 : radial_dx[i]/rz[i+r_max];
    }

    p0 = (pr0 == 0) ? 0 : p0 / pr0;

    for (i = 0; i < r_max; i++)     rz[i + r_max] = rz[i];

    for (i = 1; i < r_max; i++)     rz[r_max - i] = rz[r_max + i];

    rz[0] = p0;
# if !defined(PLAYITSAM) && !defined(PIAS)

    if (n_eva_black > 0)    eva_black /= n_eva_black;

    evanescent_black_avg = eva_black;
    evanescent_black_n_avg = n_eva_black;

    if (eva_norm > 0.0) eva_level /= eva_norm;

    evanescent_avg_intensity = eva_level;
    evanescent_avg_n_appo = eva_norm;
# endif
    return rz;
}


int defftwindow_flat_top1(fft_plan *fp, int npts, float *x, float smp)
{
    register int i, j;
    //int n_2, n_4;
    float tmp; //, *offtsin;

    (void)npts;
    if (fp == NULL )  return 1;
    //offtsin = get_fftsin();

    //if ((j = fft_init(npts)) != 0)  return j;

    //n_2 = npts/2;
    //n_4 = n_2/2;
    x[0] *= smp;
    smp += 1.0;
    for (i=1, j=fp->n_4-2; j >=0; i++, j-=2)
    {
        tmp = (smp - fp->fftbtsin[j]);
        x[i] /= tmp;
        x[fp->nx-i] /= tmp;
    }
    for (i=fp->n_4/2, j=0; i < fp->n_4 ; i++, j+=2)
    {
        tmp = (smp + fp->fftbtsin[j]);
        x[i] /= tmp;
        x[fp->nx-i] /= tmp;
    }
    return 0;
}


#endif
