
#ifndef _ACTION_LIVE_VIDEO_H_
#define _ACTION_LIVE_VIDEO_H_


# define MV_OBJ_ABS         0x00000001
# define MV_OBJ_REL         0x00000002
# define MV_ZMAG_ABS        0x00000004
# define MV_ZMAG_REL        0x00000008
# define MV_ROT_ABS         0x00000010
# define MV_ROT_REL         0x00000020
# define CHG_ROT_REF        0x00000040
# define CHG_ZMAG_REF       0x00000080

# define SAVE_BEAD_CROSS    0x00000100 
# define RESTORE_BEAD_CROSS 0x00000200 

# define GET_TEMERATURE     0x00000400
# define GET_FOCUS_POS      0x00000800
# define READ_ZMAG_VAL      0x00001000
# define READ_ROT_VAL       0x00002000
# define CHG_ZMAG_PID       0x00004000
# define CHG_ROT_PID        0x00008000
# define READ_TRANSIENT     0x00010000

PXV_FUNC(int, immediate_next_available_action, (int type, float value));

PXV_FUNC(int, immediate_next_available_action_spare,(int type, float value, int spare));
PXV_FUNC(int, fill_next_available_action, (int im, int type, float value));
PXV_FUNC(int, fill_next_available_periodic_action, (int im, int type, float value, int period));
PXV_FUNC(int, find_next_action, (int imi));
PXV_FUNC(int, find_next_action_by_type, (int imi, int type));
PXV_FUNC(int, find_remaining_action, (int imi));
PXV_FUNC(int, find_next_available_action, (void));
PXV_FUNC(int, is_there_pending_action_of_type, (int type));
PXV_FUNC(int, ask_to_update_menu,(void));

// request definition


# define SET_ROT_REQUEST             1
# define SET_ZMAG_REQUEST            2
# define READ_ROT_REQUEST            3
# define READ_ZMAG_REQUEST           4
# define SET_ZMAG_REF_REQUEST        5
# define SET_ROT_REF_REQUEST         6
# define READ_TEMPERATURE_REQUEST    7
# define READ_FOCUS_REQUEST          8
# define SET_FOCUS_REQUEST           9


// third group is errors state
# define PICO_COM_ERROR      0x01000000 
# define ZMAG_TOP_LIMIT      0x02000000 
# define ZMAG_BOTTOM_LIMIT   0x02000000 
# define ZMAG_VCAP_LIMIT     0x04000000 
# define PIFOC_OVERFLOW      0x08000000 
# define PIFOC_OUT_OF_RANGE  0x10000000 


PXV_FUNC(int, rs232_register, (int type, int image_n, float val));
PXV_FUNC(int, what_is_pending_request, (int *type, int *image_n, float *val));


// this describe real time action to perform like moving objective, magnets etc
typedef struct future_action
{
  int imi;                           // the image nb where to act, if < 0 the next possible frame
  int imi0;                           // the original image to start
  int proceeded;                     //  1 if treated, 0 if pending
  int type;
  float value;
  int period;                       // if > 0 means a periodic event
  int spare;                       // a spare int data for user
} f_action;


# ifndef _ACTION_LIVE_VIDEO_C_

# else
f_action *action_pending = NULL;
int m_action = 0;
int n_action = 0;

# endif
PXV_VAR(f_action*, action_pending);
PXV_VAR(int, m_action);
PXV_VAR(int, n_action);

# endif
