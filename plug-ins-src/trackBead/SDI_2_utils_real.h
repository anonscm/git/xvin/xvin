#ifndef _SDI_2_UTILS_H_
#define _SDI_2_UTILS_H_


#include "allegro.h"
#include "xvin.h"
#include "xv_main.h"
#include "fftl32n.h"
#include "trackBead.h"
#include "track_util.h"
#include "../fft2d/fftbtl32n.h"
#include "../cfg_file/Pico_cfg.h"



XV_FUNC(int,SDI_2_align_profile,(sdi_fr *f1, double *pos_x));
XV_FUNC(float,SDI_2_compute_score_of_alignment,(sdi_fr *f1, int i));
XV_FUNC(int,SDI_2_interpolate_profile,(sdi_fr *f1));
XV_FUNC(int,SDI_2_update_mean_profile,(sdi_fr *f1));






#endif
