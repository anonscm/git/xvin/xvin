/*******************************************************************************
                         LPS ENS
--------------------------------------------------------------------------------

  Program      : uEye live video_Source for bead live_videoing 
  Author       : VC
  Date         : 7.7.2009

  Purpose      : UEYE Manager Functions for XVin
  CVB          : 
  Hints        : 
      
  Updates      : 
********************************************************************************/
#ifndef _UEYE_LIVE_VIDEO_C_
#define _UEYE_LIVE_VIDEO_C_

#include "allegro.h"
#include "winalleg.h"
#include "xvin.h"

/* If you include other plug-ins header do it here*/ 
/* But not below this define */

# define BUILDING_PLUGINS_DLL
#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)

# undef IS_CHAR
# include "uEye.h"
# include "uEye_tools.h"


# include "fftbtl32n.h"
# include "fillibbt.h"
# include "PlayItSam.h"
//# include "track_util.h"
# include "uEye_live_video.h"

# include "../cfg_file/Pico_cfg.h"

MENU *UEYE_source_image_menu(void);





int uEye_oi_idle_action(struct  one_image *oi, DIALOG *d)
{
  return D_O_K;
}


int prepare_image_overlay(O_i* oi)
{
  return 0;
}

int kill_source(void)
{
  imreg *imr;
  if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
    {
      if (active_menu) active_menu->flags |= D_DISABLED;
      return D_O_K;	
    }
  //else if (go_track == TRACK_ON) active_menu->flags |=  D_DISABLED;
  else if (active_menu) active_menu->flags &= ~D_DISABLED;
  if(updating_menu_state != 0)	return D_O_K;
  go_live_video = LIVE_VIDEO_STOP;
  SetEvent(hEventFreeze);
  return D_O_K;
}

int live_source(void)
{
  if(updating_menu_state != 0)	return D_O_K;

  //is_live  = 1;
  return 0;	
}



int freeze_source(void)
{
  if(updating_menu_state != 0)	return D_O_K;
  SetEvent(hEventFreeze);
  return 0;
}



DWORD WINAPI LiveVideoThreadProc(LPVOID lpParam)
{
  tid *ltid;
  DWORD	iWaitResult;
  HANDLE hEventNewImage = NULL;
  HANDLE	EventList[4];
  int first = 1;
  Bid *p = NULL;
  long long t, dt, lTimeStamp, llt;
  int i = 0,im_n = 0, j, fr_delivered, prev_fr_delivered = 0, i_todo; // , *pI
  //void *pBuffer;
  int NumberFramesDelivered, ret, state = 0;
  char message[1024], *buf;

  if (imr_LIVE_VIDEO == NULL || oi_LIVE_VIDEO == NULL)	return 1;



  if (hEventFreeze != NULL)    CloseHandle(hEventFreeze);
  // Create a new one
  hEventFreeze = CreateEvent(NULL, TRUE, FALSE, NULL);
  hEventNewImage = CreateEvent(NULL, TRUE, FALSE, NULL);
  hEventRemoved = CreateEvent(NULL, TRUE, FALSE, NULL);
  hEventReplug = CreateEvent(NULL, TRUE, FALSE, NULL);

  // Make a list of events to be used in WaitForMultipleObjects
  
  EventList[0] = hEventNewImage;
  EventList[1] = hEventFreeze;
  EventList[2] = hEventRemoved;
  EventList[3] = hEventReplug;
  

  ret = is_InitEvent(m_hCam, hEventNewImage,IS_SET_EVENT_FRAME);
  if( ret != IS_SUCCESS )  return win_printf_OK("could not init event!");
  ret = is_EnableEvent(m_hCam, IS_SET_EVENT_FRAME);
  if( ret != IS_SUCCESS )  return win_printf_OK("could not init event!");

  ret = is_InitEvent(m_hCam, hEventRemoved,IS_SET_EVENT_REMOVE);
  if( ret != IS_SUCCESS )  return win_printf_OK("could not init event!");
  ret = is_EnableEvent(m_hCam, IS_SET_EVENT_REMOVE);
  if( ret != IS_SUCCESS )  return win_printf_OK("could not init event!");

  ret = is_InitEvent(m_hCam, hEventReplug,IS_SET_EVENT_DEVICE_RECONNECTED);
  if( ret != IS_SUCCESS )  return win_printf_OK("could not init event!");
  ret = is_EnableEvent(m_hCam, IS_SET_EVENT_DEVICE_RECONNECTED);
  if( ret != IS_SUCCESS )  return win_printf_OK("could not init event!"); 

  //win_printf("before capture");
  is_CaptureVideo (m_hCam, 100); 
  ltid = (tid*)lpParam;
  p = ltid->dbid;
  NumberFramesDelivered = 0;
  while(go_live_video)
    {
      // Wait for New Buffer event (or Freeze event)
      iWaitResult = WaitForMultipleObjects(4, EventList,FALSE, 10000/*00*/);
      if(iWaitResult == WAIT_OBJECT_0)
	{
	  is_GetActiveImageMem(m_hCam,&buf,&fr_delivered);
	  fr_delivered -= 2;
	  fr_delivered = (fr_delivered < 0) ? oi_LIVE_VIDEO->im.n_f + fr_delivered : fr_delivered;

	  if (fr_delivered >= iImages_in_UEYE_Buffer)   my_set_window_title("out of range");
	  if (first)
	      prev_fr_delivered = (fr_delivered + iImages_in_UEYE_Buffer - 1)%iImages_in_UEYE_Buffer;
	  for (i_todo = 0, j = prev_fr_delivered; j != fr_delivered; i_todo++)
	    {
	      j++;
	      j = (j >= iImages_in_UEYE_Buffer) ? 0 : j;
	    }
	  if (i_todo > 1)    my_set_window_title("Missed fr %d at %d", i, NumberFramesDelivered);

	  if (p->first_im == 0) p->first_im = NumberFramesDelivered;

	  t = my_ulclock();
	  dt = my_uclock();
	  p->image_per_in_ms = 1000*(double)(t -p->previous_in_time_fr)/get_my_ulclocks_per_sec();	  
	  if (first == 0) Pico_param.camera_param.camera_frequency_in_Hz = (float)1000/p->image_per_in_ms;
	  p->previous_in_time_fr = t;
	  p->timer_dt = 0;
	  p->previous_fr_nb = NumberFramesDelivered;
	  j = prev_fr_delivered;
	  //j = (j >= iImages_in_UEYE_Buffer) ? 0 : j;
	  if (first)	  first = 0;
	  for (i = 0; i < i_todo ; im_n++, i++, j++, NumberFramesDelivered++)
	    {
	      switch_frame(oi_LIVE_VIDEO,j);
	      t = my_ulclock();
	      dt = my_uclock();
	      p->previous_fr_nb = im_n = NumberFramesDelivered;
	      for (p->timer_do(imr_LIVE_VIDEO, oi_LIVE_VIDEO, im_n, d_LIVE_VIDEO, t ,dt ,p->param, i_todo - i - 1) ;
		   p->timer_do != NULL && p->next != NULL; p = p->next)
		{
		  p->timer_do(imr_LIVE_VIDEO, oi_LIVE_VIDEO,im_n, d_LIVE_VIDEO, t, dt, p->param,  i_todo - i - 1);
		}
	      lTimeStamp = llt;
	      if (rec_AVI)
		{
		  isavi_AddFrame (nAviID, oi_LIVE_VIDEO->im.mem[j]);
		}
	    }
	  prev_fr_delivered = fr_delivered;
	  ResetEvent(hEventNewImage);  // needed to get one event per frame
	  if (state == 0)
	    my_set_window_title("im id %d freq %g", 
				fr_delivered, Pico_param.camera_param.camera_frequency_in_Hz);
	  else 
	    my_set_window_title("camera removed im id %d freq %g", 
				fr_delivered, Pico_param.camera_param.camera_frequency_in_Hz);
	}
      else if(iWaitResult == WAIT_OBJECT_0 + 2)  // camera removed
	{
	  state = 1;
	}
      else if(iWaitResult == WAIT_OBJECT_0 + 3)  // camera reconnected
	{
	  state = 0;
	}
      else
        {
	  switch(iWaitResult)
            {
	      // Freeze event
	    case	WAIT_OBJECT_0 + 1:
	      ret = 1;
	      bEnableThread = FALSE;
	      break;
	      // Timeout
	    case	WAIT_TIMEOUT:
	      ret = 2;
	      win_printf("10s Timeout");
	      bEnableThread = FALSE;
	      break;
	      // Unknown?
	    default:
	      ret = 3;
	      break;
            }
        }
    }
  strncat(message,"- out loop",(sizeof(message) > strlen(message)) ? sizeof(message)-strlen(message) : 0);
  my_set_window_title(message);

 
  if( is_ExitEvent(m_hCam, IS_SET_EVENT_FRAME) != IS_SUCCESS )  
    return win_printf_OK("could not init event!");

  strncat(message,"- closing camera",(sizeof(message) > strlen(message)) ? sizeof(message)-strlen(message) : 0);
  my_set_window_title(message);

  is_StopLiveVideo (m_hCam, 100); // IS_WAIT
  is_ClearSequence (m_hCam);
  is_ExitCamera (m_hCam);

  //Terminate_UEYE_Thread();     // Terminate the thread.
  
  // Free the kill event
    if (hEventFreeze != NULL)
      {
        CloseHandle(hEventFreeze);
        hEventFreeze = NULL;
      }
    
    // Free the new image event object
    if (hEventNewImage != NULL)
      {
        CloseHandle(hEventNewImage);
        hEventNewImage = NULL;
      }
    //WaitForThreadToTerminate();
  strncat(message,"- Out thread",(sizeof(message) > strlen(message)) ? sizeof(message)-strlen(message) : 0);
  my_set_window_title(message);
  return ret;
}



HIDS hCam_Open_camera(void)
{
  int ret;

  m_hCam = (HIDS) 0; 	// open next camera
  // init camera - no window handle required
  ret = is_InitCamera( &m_hCam, NULL );
  if( ret != IS_SUCCESS ) 
    {
      win_printf("Could not open IDS camera");
      return NULL;
    }
  is_GetSensorInfo( m_hCam, &sInfo );
  Pico_param.camera_param.nb_pxl_x = sInfo.nMaxWidth;
  Pico_param.camera_param.nb_pxl_y = sInfo.nMaxHeight;
  if (Pico_param.camera_param.camera_manufacturer)      
    free(Pico_param.camera_param.camera_manufacturer);
  Pico_param.camera_param.camera_manufacturer = strdup("IDS-uEye");
  if (Pico_param.camera_param.camera_model)      
    free(Pico_param.camera_param.camera_model);
  Pico_param.camera_param.camera_model = strdup(sInfo.strSensorName);
  is_SetColorMode(m_hCam, IS_CM_MONO8);
  Pico_param.camera_param.nb_bits = 8;
  return m_hCam;
}


# ifdef OLD
int add_oi_UEYE_2_imr(imreg *imr, O_i* oi)
{

  double fps = 0;
  int i;
  int x0, y0, width, height;
  int m_lMemoryId = 0, ret;
  unsigned char *buf;
  
  if (imr == NULL) return win_printf_OK("IMR NULL");

  ret = is_SetFrameRate(m_hCam, IS_GET_FRAMERATE, &fps);
  if( ret != IS_SUCCESS )       win_printf("Could not get camera rate");
  else Pico_param.camera_param.camera_frequency_in_Hz = fps;

  x0 = 0; y0 = 0; width = sInfo.nMaxWidth;  height = sInfo.nMaxHeight;

  ret = is_SetAOI(m_hCam,IS_GET_IMAGE_AOI,&x0,&y0,&width,&height);
  if( ret != IS_SUCCESS )       win_printf("Could not get AOI");
  i = 4*(width/4);
  if (i != width)
    {   // we adjust size to a multiple of 4
      width = i;
      ret = is_SetAOI(m_hCam,IS_SET_IMAGE_AOI,&x0,&y0,&width,&height);
      if( ret != IS_SUCCESS )       win_printf("Could not set AOI");
    }
  //create image



  oi_LIVE_VIDEO = create_and_attach_movie_to_imr(imr, width, height, IS_CHAR_IMAGE, iImages_in_UEYE_Buffer);
  if (oi_LIVE_VIDEO == NULL)	  return win_printf_OK("Can't create dest image");


  LOCK_DATA(oi_LIVE_VIDEO->im.mem[0], width * height * iImages_in_UEYE_Buffer);
  for (i = 0; i < iImages_in_UEYE_Buffer; i++) 
    {
      buf = oi_LIVE_VIDEO->im.mem[i];
      is_SetAllocatedImageMem( m_hCam,  width, height, 8, buf, &m_lMemoryId);
      is_AddToSequence(m_hCam,buf,m_lMemoryId);
    }

  /*
  if((oi_LIVE_VIDEO = (O_i *)calloc(1,sizeof(O_i))) == NULL)	
    return win_printf_OK("Can't create dest image");
  if (init_one_image(oi_LIVE_VIDEO, IS_CHAR_IMAGE))    
    return win_printf_OK("Can't create dest image");
  oi_LIVE_VIDEO->im.data_type = IS_CHAR_IMAGE;	
  oi_LIVE_VIDEO->im.n_f = iImages_in_UEYE_Buffer;
  add_image(imr, IS_CHAR_IMAGE, (void*)oi_LIVE_VIDEO);

  for (i = 0; i < iImages_in_UEYE_Buffer; i++) 
    {
      is_AllocImageMem( m_hCam, width, height, 8, &buf, &m_lMemoryId);
      attach_uEye_pointer_to_movie(oi_LIVE_VIDEO, buf,  width, height, i);
      is_AddToSequence(m_hCam,buf,m_lMemoryId);
    }
  */

  //win_printf("after create");
  map_pixel_ratio_of_image_and_screen(oi_LIVE_VIDEO, 1, 1);
  set_zmin_zmax_values(oi_LIVE_VIDEO, 0, 255);
  set_z_black_z_white_values(oi_LIVE_VIDEO, 0, 255);

  write_Pico_config_file();
  Close_Pico_config_file();
  ask_to_update_menu();
  return 0;
}


# endif


int add_oi_UEYE_2_imr(imreg *imr, O_i* oi)
{

  double fps = 0;
  int i;
  int x0, y0, width, height;
  int m_lMemoryId = 0, ret;
  unsigned char *buf;
  
  if (imr == NULL) return win_printf_OK("IMR NULL");

  ret = is_SetFrameRate(m_hCam, IS_GET_FRAMERATE, &fps);
  if( ret != IS_SUCCESS )       win_printf("Could not get camera rate");
  else Pico_param.camera_param.camera_frequency_in_Hz = fps;

  x0 = 0; y0 = 0; width = sInfo.nMaxWidth;  height = sInfo.nMaxHeight;


  ret = is_SetAOI(m_hCam,IS_GET_IMAGE_AOI,&x0,&y0,&width,&height);
  if( ret != IS_SUCCESS )       win_printf("Could not get AOI");

  i = win_scanf("Defines AOI\n"
		"X0 %8d width %8d\n"
		"Y0 %8d height %8d\n"
		"Nb. of frames in buffer %8d\n"
		,&x0,&width,&y0,&height,&iImages_in_UEYE_Buffer);
  
  if (i == CANCEL) return OFF;

  i = 4*(width/4);
  //if (i != width)
  //{   // we adjust size to a multiple of 4
  width = i;
  ret = is_SetAOI(m_hCam,IS_SET_IMAGE_AOI,&x0,&y0,&width,&height);
  if( ret != IS_SUCCESS )       win_printf("Could not set AOI");
      // }
  //create image

  //win_printf("w %d h %d",width,height);
  Pico_param.camera_param.nb_pxl_x = width;
  Pico_param.camera_param.nb_pxl_y = height;

  oi_LIVE_VIDEO = create_and_attach_movie_to_imr(imr, width, height, IS_CHAR_IMAGE, iImages_in_UEYE_Buffer);
  if (oi_LIVE_VIDEO == NULL)	  return win_printf_OK("Can't create dest image");


  LOCK_DATA(oi_LIVE_VIDEO->im.mem[0], width * height * iImages_in_UEYE_Buffer);
  for (i = 0; i < iImages_in_UEYE_Buffer; i++) 
    {
      buf = oi_LIVE_VIDEO->im.mem[i];
      is_SetAllocatedImageMem( m_hCam,  width, height, 8, buf, &m_lMemoryId);
      is_AddToSequence(m_hCam,buf,m_lMemoryId);
    }

  /*
  if((oi_TRACK = (O_i *)calloc(1,sizeof(O_i))) == NULL)	
    return win_printf_OK("Can't create dest image");
  if (init_one_image(oi_TRACK, IS_CHAR_IMAGE))    
    return win_printf_OK("Can't create dest image");
  oi_TRACK->im.data_type = IS_CHAR_IMAGE;	
  oi_TRACK->im.n_f = iImages_in_UEYE_Buffer;
  add_image(imr, IS_CHAR_IMAGE, (void*)oi_TRACK);

  for (i = 0; i < iImages_in_UEYE_Buffer; i++) 
    {
      is_AllocImageMem( m_hCam, width, height, 8, &buf, &m_lMemoryId);
      attach_uEye_pointer_to_movie(oi_TRACK, buf,  width, height, i);
      is_AddToSequence(m_hCam,buf,m_lMemoryId);
    }
  */

  //win_printf("after create");
  map_pixel_ratio_of_image_and_screen(oi_LIVE_VIDEO, 1, 1);
  set_zmin_zmax_values(oi_LIVE_VIDEO, 0, 255);
  set_z_black_z_white_values(oi_LIVE_VIDEO, 0, 255);

  write_Pico_config_file();
  Close_Pico_config_file();
  ask_to_update_menu();
  return 0;
}





int start_data_movie(imreg *imr)
{
  return 0;
}


int initialize_uEye(imreg *imr)
{   
  int ret;

  //win_printf("bef hcam");
  my_set_window_title("Openning uEye camera");
  m_hCam = hCam_Open_camera();
  //win_printf("after hcam");
  my_set_window_title("Camera %s found seting image ",Pico_param.camera_param.camera_model);
  if ((ret = add_oi_UEYE_2_imr(imr,NULL))) return ret + 0x40;
  //win_printf("after add");
  my_set_window_title("Starting UEYE camera thread");
  //win_printf("after iopen");
  return 0;   

} 



int init_image_source(imreg *imr, int mode)
{
  int ret;
  O_i *oi = NULL;

  //if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
  if (imr == NULL)    imr = create_and_register_new_image_project( 0,   32,  900,  668);
  if (imr == NULL)    return win_printf_OK("Patate t'es mal barre!");
  //imr_LIVE_VIDEO = imr;
  oi = imr->one_i;
  if ((ret = initialize_uEye(imr))) 
    {
      win_printf_OK("Your uEye video camera did not start!");
      return ret; // Initializes UEYE 
    }
  if (mode == 1)      remove_from_image (imr, oi->im.data_type, (void *)oi);
  add_image_treat_menu_item ( "UEYE Genicam", NULL, UEYE_source_image_menu(), 0, NULL);
  return 0;
}



int do_set_camera_freq(void)
{
  double fr, nfr;
  float ffr;
  int pmin, pmax, pm;
  double min, max, intervall;
  char question[1024];
  int i;

  if(updating_menu_state != 0)	return D_O_K;

  is_GetPixelClockRange(m_hCam,&pmin,&pmax);
  pm = is_SetPixelClock(m_hCam,IS_GET_PIXEL_CLOCK);
  is_GetFrameTimeRange(m_hCam, &min, &max, &intervall);
  is_SetFrameRate(m_hCam,IS_GET_FRAMERATE,&fr);

  ffr = fr;
  //  fpsmin= 1/max ; fpsmax=1/min ; fpsn= 1/(min + n * intervall)
  snprintf(question,sizeof(question),"Camera frequency settings\n"
	   "Pixel clock (%d min) < (%d present) < (%d max)\n"
	   "New pixel clock %%8d\n"
	   "Frame rate (%g min) < (%g present) < (%g max)\n"
	   "New Frame rate %%8f\n"
	   ,pmin,pm,pmax,(float)1/max,(float)1/fr,(float)1/min);

  i = win_scanf(question,&pm,&ffr);

  if (i == CANCEL)    return 0;


  is_SetPixelClock(m_hCam,pm);
  fr = ffr;
  is_SetFrameRate(m_hCam,fr,&nfr);
  return 0;
}




int do_set_camera_exposure(void)
{
  int i;
  double min, max, intervall, exp, nexp;
  float fexp;
  char question[1024];

  if(updating_menu_state != 0)	return D_O_K;

  is_GetExposureRange (m_hCam, &min, &max, &intervall);
  is_SetExposureTime (m_hCam, IS_GET_EXPOSURE_TIME, &nexp);
  fexp = nexp;

  snprintf(question,sizeof(question),"Camera exposure setting\n"
	   "Range min %g max %g dt %g\n"
	   "New value: %%5f",min,max,intervall);
  i = win_scanf(question,&fexp);
  if (i == CANCEL)    return 0;
  exp = fexp;
  is_SetExposureTime (m_hCam, exp, &nexp);
  return 0;
}



int do_stop_camera(void)
{
  if(updating_menu_state != 0)	return D_O_K;
  SetEvent(hEventFreeze);
  bEnableThread = FALSE;
  return 0;
}



char	*do_save_avi(void)
{
  register int i = 0;
  int win_file = 0;
  char path[512], file[256];
  static char fullfile[512];
  char *pa = NULL;
  char *fu = NULL;  

  Open_Pico_config_file();
  pa = (char*)strdup((char *)get_config_string("AVI-FILE","last_saved",NULL));
  Close_Pico_config_file();
  if (pa != NULL)		
    {
      strncpy(fullfile,pa,sizeof(fullfile));
      free(pa);
      pa = NULL;
    }
  else			
    {
      my_getcwd(fullfile, sizeof(fullfile));
      strncat(fullfile,"\\test.avi",(sizeof(fullfile) > strlen(fullfile)) ? sizeof(fullfile)-strlen(fullfile) : 0);
    }

 #ifdef XV_WIN32
  if (full_screen)
    {
#endif
      switch_allegro_font(1);
      i = file_select_ex("Save avi movie (*.avi)", fullfile, "avi", 512, 0, 0);
      switch_allegro_font(0);
#ifdef XV_WIN32
    }
  else
    {
      extract_file_path(path, sizeof(path), fullfile);
      if (extract_file_name(file, sizeof(file), fullfile) != NULL)	strncpy(fullfile,file,sizeof(fullfile));
      else fullfile[0] = file[0] = 0;
      fu = DoFileSave("Save AVI movie (*.avi)", path, fullfile, 512, 
		      "AVI movie (*.avi)\0*.trk\0All Files (*.*)\0*.*\0\0", "avi");
      i = (fu == NULL) ? 0 : 1;
      win_file = 1;
    }
#endif 
  if (i != 0) 	
    {
      Open_Pico_config_file();
      set_config_string("AVI-FILE","last_saved",fullfile);	
      write_Pico_config_file();
      Close_Pico_config_file();
      extract_file_name(file, sizeof(file), fullfile);
      extract_file_path(path, sizeof(path), fullfile);
      strncat(path,"\\",(sizeof(path) > strlen(path)) ? sizeof(path)-strlen(path) : 0);
      if (win_file == 1 || do_you_want_to_overwrite(fullfile, NULL, "Saving AVI File") == OK)
	return fullfile;
    }
  return NULL;
}



int start_ueye_avi(void)
{
  int  ret, i;
  static int quality = 80;
  double fr;
  char *avi_file = NULL;
  
  if(updating_menu_state != 0)	return D_O_K;

  if (key[KEY_LSHIFT])
      return win_printf_OK("This routine record an AVI movie");

  if (m_hCam == NULL) 	return D_O_K;

  ret = isavi_InitAVI(&nAviID, m_hCam);
  if (ret != IS_AVI_NO_ERR)    return win_printf_OK("Error initiating AVI %d",ret);

  ret = isavi_SetImageSize (nAviID, IS_AVI_CM_Y8, sInfo.nMaxWidth, sInfo.nMaxHeight, 0, 0, 0); // sInfo.nMaxWidth
  if (ret != IS_AVI_NO_ERR)    return win_printf_OK("Error initiating AVI %d",ret);

  i = win_scanf("Jpeg quality(1->poor 100->excellent) %d",&quality);
  if (i == CANCEL) return D_O_K;

  avi_file = do_save_avi();
  if (avi_file == NULL)  return D_O_K;

  ret = isavi_SetImageQuality (nAviID, quality);
  if (ret != IS_AVI_NO_ERR)    return win_printf_OK("Error initiating AVI %d",ret);

  ret = isavi_OpenAVI(nAviID,avi_file);
  if (ret != IS_AVI_NO_ERR)    return win_printf_OK("Error initiating AVI %d",ret);


  is_SetFrameRate(m_hCam,IS_GET_FRAMERATE,&fr);


  ret = isavi_SetFrameRate (nAviID, fr);
  if (ret != IS_AVI_NO_ERR)    return win_printf_OK("Error initiating AVI %d",ret);

  ret = isavi_StartAVI (nAviID);
  if (ret != IS_AVI_NO_ERR)    return win_printf_OK("Error initiating AVI %d",ret);

  rec_AVI = 1;

  return D_O_K;
}


int stop_ueye_avi(void)
{
  int ret;

  if(updating_menu_state != 0)	return D_O_K;

  if (key[KEY_LSHIFT])
      return win_printf_OK("This routine stop the recording of an AVI movie");
  
  rec_AVI = 0;

  ret = isavi_StopAVI (nAviID);
  if (ret != IS_AVI_NO_ERR)    return win_printf_OK("Error initiating AVI %d",ret);

  ret = isavi_CloseAVI (nAviID);
  if (ret != IS_AVI_NO_ERR)    return win_printf_OK("Error initiating AVI %d",ret);

  ret = isavi_ExitAVI (nAviID);
  if (ret != IS_AVI_NO_ERR)    return win_printf_OK("Error initiating AVI %d",ret);
  nAviID = -1;
  return D_O_K;
}


int adj_AOI_position(void)
{
  int i, ret;
  int x0, y0, width, height;

  
  if(updating_menu_state != 0)	return D_O_K;

  x0 = 0; y0 = 0; width = sInfo.nMaxWidth;  height = sInfo.nMaxHeight;


  ret = is_SetAOI(m_hCam,IS_GET_IMAGE_AOI,&x0,&y0,&width,&height);
  if( ret != IS_SUCCESS )       win_printf("Could not get AOI");

  i = win_scanf("Defines AOI origin position\n"
		"X0 %8d y0 %8d\n",&x0,&y0);
  if (i == CANCEL) return OFF;

  ret = is_SetAOI(m_hCam,IS_SET_IMAGE_AOI,&x0,&y0,&width,&height);
  if( ret != IS_SUCCESS )       win_printf("Could not set AOI");
  return 0;
}


int do_set_camera_gain(void)
{
  int i, ret;
  int gMaxRed, gMaxGreen, gMaxBlue, gMaxGain, ninpos;
  int gRed, gGreen, gBlue, gGain, gboost, gboostavail;
  char question[1024];


  if(updating_menu_state != 0)	return D_O_K;

//Query the maximum gain factor for the red channel:
  gMaxRed = is_SetHWGainFactor (m_hCam, IS_INQUIRE_RED_GAIN_FACTOR, 100);
  gMaxGreen = is_SetHWGainFactor (m_hCam, IS_INQUIRE_GREEN_GAIN_FACTOR, 100);
  gMaxBlue = is_SetHWGainFactor (m_hCam, IS_INQUIRE_BLUE_GAIN_FACTOR, 100);
  gMaxGain = is_SetHWGainFactor (m_hCam, IS_INQUIRE_MASTER_GAIN_FACTOR, 100);


  gRed = is_SetHWGainFactor (m_hCam, IS_GET_RED_GAIN_FACTOR, 100);
  gGreen = is_SetHWGainFactor (m_hCam, IS_GET_GREEN_GAIN_FACTOR, 100);
  gBlue = is_SetHWGainFactor (m_hCam, IS_GET_BLUE_GAIN_FACTOR, 100);
  gGain = is_SetHWGainFactor (m_hCam, IS_GET_MASTER_GAIN_FACTOR, 100);

  gboostavail = is_SetGainBoost (m_hCam, IS_GET_SUPPORTED_GAINBOOST);  
  gboost = is_SetGainBoost (m_hCam, IS_GET_GAINBOOST);  
  /*
  win_printf("Camera has master gain %s current %d max %d\n"
	     "Camera has Red gain %s current %d max %d\n"
	     "Camera has Green gain %s current %d max %d\n"
	     "Camera has Blue gain %s current %d max %d\n"
	     "Camare support gain boost %s current val %d\n"
	     ,((sInfo.bMasterGain)?"Yes":"No"),gGain,gMaxGain,
	     ((sInfo.bRGain) ?"Yes":"No"),gRed,gMaxRed,
	     ((sInfo.bGGain) ?"Yes":"No"),gGreen,gMaxGreen,
	     ((sInfo.bBGain) ?"Yes":"No"),gBlue,gMaxBlue,
	     ((gboostavail) ?"Yes":"No"),gboost
	     );


  */
  snprintf(question,1024,"Camera %s %s \nmaster gain current value %%8d max %d\n"
	   "Camera %s gain boost (No boost->%%R, boost %%r)\n"
	   ,sInfo.strSensorName,((sInfo.bMasterGain)?"has":"does not have"),gMaxGain,
	     ((gboostavail) ?"supports":"does not support"));

  i = win_scanf(question,&gGain,&gboost);
  if (i == CANCEL) return OFF;

  ret = is_SetHWGainFactor (m_hCam, IS_SET_MASTER_GAIN_FACTOR, gGain);

  ret = is_SetGainBoost (m_hCam, (gboost) ? IS_SET_GAINBOOST_ON : IS_SET_GAINBOOST_OFF);  
  return 0;
}




MENU *UEYE_source_image_menu(void)
{
  static MENU mn[32];
    
  if (mn[0].text != NULL)	return mn;

  add_item_to_menu(mn,"chg Freq", do_set_camera_freq,NULL,0,NULL);
  add_item_to_menu(mn,"chg exposure", do_set_camera_exposure,NULL,0,NULL);
  add_item_to_menu(mn,"change gains", do_set_camera_gain,NULL,0,NULL);
  add_item_to_menu(mn,"Adjust AOI origin", adj_AOI_position,NULL,0,NULL);
  add_item_to_menu(mn,"stop uEye movie", do_stop_camera,NULL,0,NULL);
  add_item_to_menu(mn,"freeze uEye movie", freeze_source,NULL,0,NULL);
  add_item_to_menu(mn,"start AVI movie", start_ueye_avi,NULL,0,NULL);
  add_item_to_menu(mn,"stop AVI movie", stop_ueye_avi,NULL,0,NULL);
  

    
  return mn;
    
}



#endif

/*******************************************************************************/
