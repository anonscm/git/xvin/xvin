
#ifndef _FORCE_REC_H_
#define _FORCE_REC_H_


PXV_FUNC(int, redraw_force_from_trk, (void));
PXV_FUNC(int, draw_extension_vs_kz_over_kx, (void));
PXV_FUNC(int, draw_etar_versus_extension, (void));
PXV_FUNC(MENU*, force_rec_plot_menu, (void));
PXV_FUNC(force_param *,load_Force_param_from_trk,(g_record *g_r));
# endif
