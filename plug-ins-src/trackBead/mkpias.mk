# name of the plug'in
NAME	    		= PlayItAgainSam
# specific plugin flags
LCFLAGS 		= -msse2 -g -DPLAY_IT_AGAIN_SAM
# extra file to zip
ZIPALSO			= 
# name of additional source files (.c and .h are already assumed)
OTHER_FILES 		= record hat_rec scan_zmag_rec force_rec fftbtl32n 
# name of libraries this plug'in depends on
LIB_DEPS    		= XVcfg cardinal 
# name of the dll to convert in lib.a      
OTHER_LIB_FROM_DLL 	=  	
XVIN_DIR    		=../../
##################################################################################################
include $(XVIN_DIR)platform.mk		# platform specific definitions
include $(XVIN_DIR)plugin.mk 		# plugin construction rules
