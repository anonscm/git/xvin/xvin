
#ifndef _MOD_ROT_C_
#define _MOD_ROT_C_

# include "allegro.h"
# include "winalleg.h"

# include "xvin.h"
# include "fftl32n.h"
# include "fillib.h"





/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "../../cfg_file/Pico_cfg.h"
# include "../../cardinal/cardinal.h"


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "../focus.h"
# include "../magnetscontrol.h"
# include "../trackBead.h"
# include "../brown_util.h"
# include "../track_util.h"
# include "mod_rot.h"
# include "../scan_zmag.h"
# include "../action.h"

int mod_rot_op_idle_action(O_p *op, DIALOG *d)
{
  if (d == NULL || d->dp == NULL || d->proc == NULL)    return 1;
  if (op->need_to_refresh)      d->proc(MSG_DRAW,d,0);	
  return 0;
}

int mod_rot_job_xyz(int im, struct future_job *job)
{
  register int i, j, k;
  int ci, nf1, irepeat, nf;
  b_track *bt = NULL;
  d_s *dsx, *dsy, *dsz, *ds, *dsm;
  O_p *op;
  mod_rot_param *sp = NULL; 
  char name[256];
  float ax, dx, ay, dy, y_over_x, y_over_z;
  un_s *un;
  char fullfile[1024];


  if (job == NULL || job->oi == NULL || job->in_progress == 0 || job->op->n_dat < 3) return 0;
  if (im < job->imi)  return 0;

  ci = track_info->c_i;
  sp = (mod_rot_param *)job->more_data;
  dsx = job->op->dat[0];
  dsy = job->op->dat[1];
  dsz = job->op->dat[2];
  dsm = job->op->dat[3];
  bt = track_info->bd[job->bead_nb];
  irepeat = sp->irepeat;

  un = job->oi->xu[job->oi->c_xu];
  get_afine_param_from_unit(un, &ax, &dx);
  un = job->oi->yu[job->oi->c_yu];
  get_afine_param_from_unit(un, &ay, &dy);
  y_over_x = (dy != 0) ? dx/dy : 1;
  y_over_z = 1e-6;
  y_over_z = (dy != 0) ? y_over_z/dy : 1;



  while (track_info->imi[ci] > job->imi)
    {
      ci--;
      if (ci < 0) ci += TRACKING_BUFFER_SIZE;
    }

  while (track_info->imi[ci] >= (job->imi-32))
    {
      j = track_info->imi[ci] - job->in_progress;
      if (j < dsx->mx)
	{
	  dsx->yd[j] = y_over_x * bt->x[ci];
	  dsy->yd[j] = bt->y[ci];
	  dsz->yd[j] = y_over_z * bt->z[ci];
	  dsm->yd[j] = track_info->rot_mag[ci];
	  dsx->xd[j] = dsy->xd[j] = dsz->xd[j] = dsm->xd[j] = j;
	}
      ci--;
      if (ci < 0) ci += TRACKING_BUFFER_SIZE;
    }
  j = job->imi - job->in_progress;
  if (j <= dsx->mx)
    dsx->nx = dsx->ny = dsy->nx = dsy->ny = dsz->nx = dsz->ny = dsm->nx = dsm->ny = j;
  if (j >= dsx->mx)
        dsx->nx = dsx->ny = dsy->nx = dsy->ny = dsz->nx = dsz->ny = dsm->nx = dsm->ny = dsx->mx;
  job->op->need_to_refresh = 1;
  if (dsx->nx < dsx->mx) job->imi += 32;
  else 
    {
      irepeat++;
      bt->opt = job->op;
      if (irepeat > sp->nrepeat) 
	{
	  sprintf(fullfile,"%s%s",job->op->dir,job->op->filename);
	  save_one_plot_bin(job->op, fullfile);
	  job->in_progress = 0; 
	  if (job->bead_nb >= sp->last_bead_n) 
	    {
	      im = track_info->imi[track_info->c_i];                                 
	      im += 32;
	      k = fill_next_available_action(im, MV_ZMAG_ABS, sp->zmag_finish);
	      im += 32;
	      k = fill_next_available_action(im, MV_ROT_ABS, sp->ro[0]);
	      n_mod_rot++;
	    }
	}
      else 
	{
	  sprintf(fullfile,"%s%s",job->op->dir,job->op->filename);
	  save_one_plot_bin(job->op, fullfile);
	  if (job->bead_nb == sp->first_bead_n) 
	    {
	      im = track_info->imi[track_info->c_i];                                 
	      im += 64;
	      
	      for (i = 0, nf = 0; i <= sp->nstep ; i++)
		{
		  sp->ro[i] = sp->rot_start + (i * sp->rot_step);
		  sp->zm[i] = sp->zmag;
		  sp->im[i] = im + nf;
		  nf += sp->nf;
		  nf += sp->dead;
		  if (sp->save_bead_position) nf += 20;
		}
	      sp->ro[i] = sp->rot_start;
	      sp->zm[i] = sp->zmag;
	      sp->im[i] = im + nf;
	    }

	  sprintf(name,"Mod_Rot curve %d bead %d step %d",n_mod_rot,job->bead_nb, irepeat);
	  nf1 = sp->nstep * (sp->nf + sp->dead);
	  op = create_and_attach_one_plot(job->pr, nf1, nf1, 0);
	  op->data_changing = 1;
	  ds = op->dat[0];
	  ds->nx = ds->ny = 0;
	  set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
			"X coordinate l = %d, w = %d, nim = %d\n"
			"objective %f, zoom factor %f, sample %s Rotation %g\n"
			"Z magnets %g mm\n"
			"Calibration from %s"
			,Pico_param.camera_param.camera_frequency_in_Hz,n_mod_rot,job->bead_nb
			,track_info->cl,track_info->cw,nf1
			,Pico_param.obj_param.objective_magnification
			,Pico_param.micro_param.zoom_factor
			,pico_sample
			,sp->ro[0]
			,sp->zm[0]
			,bt->calib_im->filename);
	  if ((ds = create_and_attach_one_ds(op, nf1, nf1, 0)) == NULL)
	    return win_printf_OK("I can't create plot !");
	  ds->nx = ds->ny = 0;
	  set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
			"Y coordinate l = %d, w = %d, nim = %d\n"
			"objective %f, zoom factor %f, sample %s Rotation %g\n"
			"Z magnets %g mm\n"
			"Calibration from %s"
			,Pico_param.camera_param.camera_frequency_in_Hz,n_mod_rot,job->bead_nb
			,track_info->cl,track_info->cw,nf1
			,Pico_param.obj_param.objective_magnification
			,Pico_param.micro_param.zoom_factor
			,pico_sample
			,sp->ro[0]
			,sp->zm[0]
			,bt->calib_im->filename);
	  if ((ds = create_and_attach_one_ds(op, nf1, nf1, 0)) == NULL)
	    return win_printf_OK("I can't create plot !");
	  ds->nx = ds->ny = 0;
	  set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
			"Z coordinate l = %d, w = %d, nim = %d\n"
			"objective %f, zoom factor %f, sample %s Rotation %g\n"
			"Z magnets %g mm\n"
			"Calibration from %s"
			,Pico_param.camera_param.camera_frequency_in_Hz,n_mod_rot,job->bead_nb
			,track_info->cl,track_info->cw,nf1
			,Pico_param.obj_param.objective_magnification
			,Pico_param.micro_param.zoom_factor
			,pico_sample
			,sp->ro[0]
			,sp->zm[0]
			,bt->calib_im->filename);
	  
	  uns_op_2_op(op, job->op);
	  op->op_idle_action = mod_rot_op_idle_action; //xyz_scan_idle_action;
	  set_op_filename(op, "X(t)Y(t)Z(t)bd%dmod_rot%d-%d.gr",job->bead_nb,n_mod_rot,irepeat);
	  set_op_path(op, sp->dir);
	  set_plot_title(op, "Bead %d trajectory %d-%d", job->bead_nb,n_mod_rot,irepeat);
	  set_plot_x_title(op, "Time");
	  set_plot_y_title(op, "Position");			
	  
	  if ((ds = create_and_attach_one_ds(op, nf1, nf1, 0)) == NULL)
	    return win_printf_OK("I can't create plot !");
	  ds->nx = ds->ny = 0;
	  set_ds_source(ds,"Magnets rotation");
	  op->x_lo = (-nf1/64);
	  op->x_hi = (nf1 + nf1/64);
	  set_plot_x_fixed_range(op);
	  op->data_changing = 0;

	  job->op = op;
	  job->imi = sp->im[0] + 32;
	  job->in_progress = sp->im[0];
	  if (job->bead_nb >= sp->last_bead_n) 
	    {
	      sp->irepeat = irepeat; 
	      for (i = 0; i < sp->nstep ; i++)
		{
		  if (sp->save_bead_position == 0)
		    {
		      k = fill_next_available_action(sp->im[i], MV_ROT_ABS, sp->ro[i]);
		      if (k < 0)	win_printf_OK("Could not add pending action!");
		    }
		  else
		    {
		      k = fill_next_available_action(sp->im[i], BEAD_CROSS_SAVED, 0);
		      if (k < 0)	win_printf_OK("Could not add pending action!");
		      k = fill_next_available_action(sp->im[i]+5, MV_ROT_ABS, sp->ro[i]);
		      if (k < 0)	win_printf_OK("Could not add pending action!");
		      k = fill_next_available_action(sp->im[i]+15+sp->dead, BEAD_CROSS_RESTORE, 0);
		      if (k < 0)	win_printf_OK("Could not add pending action!");
		      
		    }
		}
	    }
	}
    }
  return 0;
}



int record_mod_rot_curve(void)
{
   int i, j, k, im, nf1, ii, nft;
   imreg *imr;
   O_i *oi;
   b_track *bt = NULL;
   pltreg *pr;
   O_p *op;
   d_s *ds;
   static int nf = 2048, nstep = 8,  dead = 10, nrepeat = 10, save_restore = 1;
   static float   rot_start, zmag, rot_step = 5, zmag_finish = 8, z_cor = 0.878;
   char question[1024], name[256];
   mod_rot_param *sp; 

   if(updating_menu_state != 0)	
     {
       if (track_info->n_b == 0) 
	 active_menu->flags |=  D_DISABLED;
       else
	 {
	   for (i = 0, k = 0; i < track_info->n_b; i++)
	     {
	       bt = track_info->bd[i];
	       k += (bt->calib_im != NULL) ? 1 : 0;
	     }
	   if (k) active_menu->flags &= ~D_DISABLED;	
	   else active_menu->flags |=  D_DISABLED;
	 }
       return D_O_K;
     }
   if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)	return OFF;

   //win_printf("cam freq %f",Pico_param.camera_param.camera_frequency_in_Hz);
   rot_start = what_is_present_rot_value();
   zmag = read_magnet_z_value();
   sprintf(question,"{\\pt12 Starting a new rotation modulation recording}\n"
	   "This function will create a new projets per active bead containing\n"
	   "the x,y,z trajectories for each modulation period.\n"
	   "Z_{mag} present value is %g; rotation position is %g\n"
	   "Starting rot %%6f with step of %%6f nb of step %%6d\n"
	   "A rotation modulation period ends by turning roatation\n"
	   "back to its starting value\n"
	   "Number of frames skipped after each rotation change %%8d\n"
	   "do you want to save and restore bead position before \n"
	   "and after rotation no %%R yes %%r\n"
	   "number of frames at each step %%8d\n"
	   "number of repition of this pattern %%6d\n"
	   "zmag to stand when acquisition(s) are finished %%8f\n"
	   "z correction factor%%8f\n"
	   ,zmag,rot_start);

   i = win_scanf(question,&rot_start,&rot_step,&nstep,&dead,&save_restore,&nf,&nrepeat,&zmag_finish,&z_cor);	

   if (i == CANCEL)	return OFF; 

   //  win_printf("1");
  my_set_window_title("mod_rot 1");
   sp = (mod_rot_param*)calloc(1,sizeof(mod_rot_param));
   if (sp == NULL)  win_printf_OK("Could no allocte mod_rot parameters!");
   sp->im = (int*)calloc(nstep+2,sizeof(int));
   sp->ro = (float*)calloc(nstep+2,sizeof(float));
   sp->zm = (float*)calloc(nstep+2,sizeof(float));
   if (sp->im == NULL || sp->ro == NULL || sp->zm == NULL )  
     win_printf_OK("Could no allocte mod_rot parameters!");

   sp->nstep = nstep;
   sp->nf = nf;
   sp->istep = 0;
   sp->dead = dead;
   sp->zmag = zmag;
   sp->zmag_finish = zmag_finish;
   sp->rot_step = rot_step;
   sp->rot_start = rot_start;
   sp->nrepeat = nrepeat;
   sp->irepeat = 0;
   sp->save_bead_position = save_restore;
   
   im = track_info->imi[track_info->c_i];                                 
   im += 64;
  my_set_window_title("mod_rot 2");

   nf1 = sp->nstep * (sp->nf + sp->dead);
   //win_printf("nf1 %d",nf1);
   
   for (i = 0, nft = 0; i <= sp->nstep ; i++)
     {
       sp->ro[i] = rot_start + (i * rot_step);
       sp->zm[i] = zmag;
       sp->im[i] = im + nft;
       nft += sp->nf + sp->dead;
       if (sp->save_bead_position) nft += 20;
     }
   sp->ro[i] = rot_start;
   sp->zm[i] = zmag;
   sp->im[i] = im + nft;
   
  my_set_window_title("mod_rot 3");
   for (i = 0, ii = 0; i < track_info->n_b; i++)
     {
       bt = track_info->bd[i];
       if ((bt->calib_im == NULL) || (bt->not_lost <= 0) || (bt->in_image == 0))
	 continue;

       if (ii == 0) sp->last_bead_n = sp->first_bead_n = i;
       else	   sp->last_bead_n = i;

       sprintf(name,"Mod_Rot recordings %d bead %d",n_mod_rot,i);
       pr = create_hidden_pltreg_with_op(&op, nf1, nf1, 0,name);
       if (pr == NULL)  win_printf_OK("Could not find or allocte plot region!");
       op = pr->one_p;
       ds = op->dat[0];
       for (ds->nx = 0; ds->nx < ds->mx; ds->nx++) 
	 ds->xd[ds->nx] =  ds->nx;


       //ds->nx = ds->ny = 0;

       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
		     "X coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_mod_rot,i
		     ,track_info->cl,track_info->cw,nf1
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,pico_sample
		     ,what_is_present_rot_value()
		     ,what_is_present_z_mag_value()
		     ,bt->calib_im->filename);


       if ((ds = create_and_attach_one_ds(op, nf1, nf1, 0)) == NULL)
	 return win_printf_OK("I can't create plot !");
       //ds->nx = ds->ny = 0;


       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
		     "Y coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_mod_rot,i
		     ,track_info->cl,track_info->cw,nf1
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,pico_sample
		     ,what_is_present_rot_value()
		     ,what_is_present_z_mag_value()
		     ,bt->calib_im->filename);

       if ((ds = create_and_attach_one_ds(op, nf1, nf1, 0)) == NULL)
	 return win_printf_OK("I can't create plot !");
       //ds->nx = ds->ny = 0;

       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
		     "Z coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_mod_rot,i
		     ,track_info->cl,track_info->cw,nf1
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,pico_sample
		     ,what_is_present_rot_value()
		     ,what_is_present_z_mag_value()
		     ,bt->calib_im->filename);

       if ((ds = create_and_attach_one_ds(op, nf1, nf1, 0)) == NULL)
	 return win_printf_OK("I can't create plot !");
       ds->nx = ds->ny = 0;
       set_ds_source(ds,"Magnets rotation");


       uns_oi_2_op(oi, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
       create_attach_select_x_un_to_op(op, IS_SECOND, 0
				       ,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");
       
       op->op_idle_action = mod_rot_op_idle_action; //xyz_scan_idle_action;
       set_op_filename(op, "X(t)Y(t)Z(t)bd%dmod_rot%d-0.gr",i,n_mod_rot);
       set_plot_title(op, "Bead %d trajectory %d-0  Zmag %g", i,n_mod_rot,what_is_present_z_mag_value());
       set_plot_x_title(op, "Time");
       set_plot_y_title(op, "Position");			

       op->x_lo = (-nf1/64);
       op->x_hi = (nf1 + nf1/64);
       set_plot_x_fixed_range(op);


       if (ii == 0)
	 {
	   win_printf("The next window will allow you to choose the \ndirectory to save rotation modulation data ");

	   save_one_plot_data_bin(op);
	   sp->dir = strdup(op->dir);

	   
	   im = track_info->imi[track_info->c_i];                                 
	   im += 64;
	   
	   for (j = 0, nft = 0; j <= sp->nstep ; j++)
	     {
	       sp->ro[j] = rot_start + (j * rot_step);
	       sp->zm[j] = zmag;
	       sp->im[j] = im + nft;
	       nft += sp->nf + sp->dead;
	       if (sp->save_bead_position) nft += 20;
	     }
	   sp->ro[j] = rot_start;
	   sp->zm[j] = zmag;
	   sp->im[j] = im + nft;
	 }
       else
	 {
	   set_op_path(op, sp->dir);
	 }

       fill_next_available_job_spot(im , im+32, COLLECT_DATA, i, imr, oi, pr, op, (void*)sp, mod_rot_job_xyz, 0);
       ii++;
     }

   //win_printf("3");
 my_set_window_title("mod_rot 4");

   for (i = 0; i < nstep ; i++)
     {
       if (sp->save_bead_position == 0)
	 {
	   k = fill_next_available_action(sp->im[i], MV_ROT_ABS, sp->ro[i]);
	   if (k < 0)	win_printf_OK("Could not add pending action!");
	 }
       else
	 {
	   k = fill_next_available_action(sp->im[i], BEAD_CROSS_SAVED, 0);
	   if (k < 0)	win_printf_OK("Could not add pending action!");
	   k = fill_next_available_action(sp->im[i]+5, MV_ROT_ABS, sp->ro[i]);
	   if (k < 0)	win_printf_OK("Could not add pending action!");
	   k = fill_next_available_action(sp->im[i]+15+sp->dead, BEAD_CROSS_RESTORE, 0);
	   if (k < 0)	win_printf_OK("Could not add pending action!");

	 }
     }
   //win_title_used = 1;
   // win_printf("4");
 my_set_window_title("mod_rot 4");
   return D_O_K;

}

int	mod_rot_main(int argc, char **argv)
{
  add_item_to_menu(trackBead_image_menu(),"Rotation modulation", record_mod_rot_curve,NULL,0 ,NULL);
  return 0;
}

# endif
