
#ifndef _MOD_ROT_H_
#define _MOD_ROT_H_


typedef struct _mod_rot_param
{
  int nrepeat;             // the number of complete modulations
  int irepeat;             // the prsent number of complete modulations
  int first_bead_n;        // the index of the first bead recorded
  int last_bead_n;         // the index of the last bead recorded
  int *im;                 // the array of images number 
  float zmag;              // the zmag acquisition position
  float zmag_finish;       // the final zmag value
  float rot_start;         // the starting rotation position
  float rot_step;          // the step size of rotation step
  int nf;                  // the number of frames of a rotation step 
  int nstep;               // the number of steps in one period of the rotation modulation
  int istep;               // the present acquisition point
  int dead;                // the number of frames disregard just after rotation has changed
  float *zm;               // the array of zmag position
  float *ro;               // the array of rotation position
  char *dir;               // the directory to save data
  int save_bead_position;  // to prevent bead lost during rotation
} mod_rot_param;


# ifndef _MOD_ROT_C_

//PXV_VAR(char, sample[]);
PXV_VAR(int, n_mod_rot);

# else

//char sample[] = "test"; // _PXV_DLL
int n_mod_rot = 0;// _PXV_DLL

# endif

# endif
