
#ifndef _ZDET_C_
#define _ZDET_C_

# include "allegro.h"
#ifdef XV_WIN32
    # include "winalleg.h"
#endif
# include "xvin.h"





/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "../cfg_file/Pico_cfg.h"
#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "../fft2d/fftbtl32n.h"
# include "fillibbt.h"
# include "focus.h"
# include "magnetscontrol.h"
# include "trackBead.h"
# include "brown_util.h"
# include "track_util.h"
# include "scan_zmag.h"
# include "action.h"
# include "record.h"
# include "zdet.h"




//static float z_cor = 0.878;





static int ending = -1;


int zdet_op_post_display(struct one_plot *op, DIALOG *d)
{
  register int i, j;
  BITMAP *imb = NULL;
  d_s *ds = NULL;
  //int na = 0, nb = 0, xc, yc;
  int  nb, xc, yc;
  //float f;

  if (d == NULL || d->dp == NULL || d->proc == NULL)    return 1;
  if (imr_and_pr) return 0;
  ds = op->dat[0];
  if (ds == NULL || ds->source == NULL)       return 0;

  //if (sscanf(ds->source,"Bead tracking at %f Hz Acquistion %d for bead %d"
  //	     ,&f,&na,&nb) != 3) return 0;

  nb = op->user_ispare[ISPARE_BEAD_NB];

  if (nb >= 0 && nb  < track_info->n_b)
    {
      imb = (BITMAP*)oi_TRACK->bmp.stuff;
      if (oi_TRACK->need_to_refresh & BITMAP_NEED_REFRESH)
	{
	  display_image_stuff_16M(imr_TRACK,d_TRACK);
	  oi_TRACK->need_to_refresh |= INTERNAL_BITMAP_NEED_REFRESH;
	  show_bead_cross(imb, imr_TRACK, d_TRACK);
	}
      for(i = nb - 1, j = 0; (i < nb + 2) && (j < track_info->n_b) && (j < 3); i++, j++)
	{
	  xc = track_info->bd[(i+track_info->n_b)%track_info->n_b]->x0 - 100;
	  yc = oi_TRACK->im.ny - track_info->bd[(i+track_info->n_b)%track_info->n_b]->y0 - 100;

	  //SET_BITMAP_IN_USE(oi_TRACK);
	  acquire_bitmap(plt_buffer);
	  blit(imb,plt_buffer,xc,yc,SCREEN_W - 300, 60 + 256*j, 200, 200);
	  release_bitmap(plt_buffer);
	  //SET_BITMAP_NO_MORE_IN_USE(oi_TRACK);
	}
    }
  return 0;
}

int zdet_op_idle_action_old(O_p *op, DIALOG *d)
{
  if (d == NULL || d->dp == NULL || d->proc == NULL)    return 1;
  if (op->need_to_refresh)      d->proc(MSG_DRAW,d,0);
  return 0;
}

int zdet_op_idle_action(O_p *op, DIALOG *d)
{
  register int i, j;
  BITMAP *imb = NULL;
  d_s *ds = NULL;
  //int na = 0, nb = 0, xc, yc;
  int nb, xc, yc;
  //float f;
  //char buf[512];

  if (d == NULL || d->dp == NULL || d->proc == NULL)    return 1;
  if (op->need_to_refresh)      d->proc(MSG_DRAW,d,0);
  if (imr_and_pr) return 0;
  ds = op->dat[0];
  if (ds == NULL || ds->source == NULL)       return 0;

  //if (sscanf(ds->source,"Bead tracking at %f Hz Acquistion %d for bead %d"
  //     ,&f,&na,&nb) != 3) return 0;

  nb = op->user_ispare[ISPARE_BEAD_NB];
  //# ifdef ENCOURS
  if (nb >= 0 && nb  < track_info->n_b)
    {
      imb = (BITMAP*)oi_TRACK->bmp.stuff;
      if (oi_TRACK->need_to_refresh & BITMAP_NEED_REFRESH)
	{
	  display_image_stuff_16M(imr_TRACK,d_TRACK);
	  oi_TRACK->need_to_refresh |= INTERNAL_BITMAP_NEED_REFRESH;
	  show_bead_cross(imb, imr_TRACK, d_TRACK);
	}

      for(i = nb - 1, j = 0; (i < nb + 2) && (j < track_info->n_b) && (j < 3); i++, j++)
	{
	  xc = track_info->bd[(i+track_info->n_b)%track_info->n_b]->x0 - 100;
	  yc = oi_TRACK->im.ny - track_info->bd[(i+track_info->n_b)%track_info->n_b]->y0 - 100;

	  /*
	  snprintf(buf,512,"                 bd %d j %d x0 %d yo %d xc %d yc %d",i,j,track_info->bd[nb]->x0,track_info->bd[nb]->y0,xc,yc);
	  set_window_title(buf);
	  */


	  //SET_BITMAP_IN_USE(oi_TRACK);
	  for(;screen_acquired;); // we wait for screen_acquired back to 0;     
	  screen_acquired = 1;
	  
	  acquire_bitmap(screen);
	  blit(imb,screen,xc,yc,SCREEN_W - 300, 60 + 256*j, 200, 200);
	  release_bitmap(screen);
	  screen_acquired = 0;	  
	  oi_TRACK->need_to_refresh &= ~INTERNAL_BITMAP_NEED_REFRESH;
	  //SET_BITMAP_NO_MORE_IN_USE(oi_TRACK);
	}
    }
  //# endif
  return 0;
}

int zdet_job_z_fil(int im, struct future_job *job)
{
  int i, j, k;
  int ims, nf, page_n, i_page, max_pos, min_pos, lastzm = -1;
  b_record *br = NULL, *brf = NULL;
  d_s *dsz = NULL, *dszf = NULL, *dszm = NULL;
  float min = 0, max = 0, minz = 0, maxz = 0, tmp, tmp2;
  pltreg *pr = NULL;
  g_record *g_r = NULL;

  ac_grep(cur_ac_reg,"%pr",&pr);
  g_r = find_g_record_in_pltreg(pr);
  if (g_r == NULL) return 0;

  if (job == NULL || job->oi == NULL || job->op->n_dat < 3) return 0;

  //if (working_gen_record != zdet_r)  return 0;
  if (im < job->imi)  return 0;
  if (g_r == NULL || g_r->abs_pos < 0)  return 0;

  //ci = track_info->c_i;
  dsz = job->op->dat[0];
  dszf = job->op->dat[1];
  dszm = job->op->dat[2];
  br = g_r->b_r[job->bead_nb%g_r->n_bead];
  if (job->fixed_bead_nb >= 0)   brf = g_r->b_r[job->fixed_bead_nb];


  min_pos = g_r->starting_in_page_index + (g_r->starting_page * g_r->page_size);
  j = max_pos = g_r->abs_pos;
  if (job->op->user_id == 4096 && job->op->user_ispare[ISPARE_TIME_OUT] > (int)(clock()/CLOCKS_PER_SEC))
    {
      j = job->op->user_ispare[ISPARE_BUF_POSITION];
      if (j > max_pos) j = max_pos;
      if (j < min_pos) j = min_pos;
    }
  else  job->op->user_ispare[ISPARE_BUF_POSITION] = j;

  max_pos = j;

  //imi = g_r->imi[j/g_r->page_size][j%g_r->page_size]; //track_info->imi[ci];
  ims = g_r->last_starting_pos;

  j = max_pos - scope_buf_size; //dsz->mx;
  j = (j < min_pos) ? min_pos : j; //512*(1+(j/512));

  nf = max_pos - j;
  nf = (nf < dsz->mx) ? nf : dsz->mx;

  for (i = 0; i < nf; i++, j++)
    {
      page_n = j/g_r->page_size;
      i_page = j%g_r->page_size;
      //if (page_n <= g_r->n_page && i_page <= g_r->in_page_index)
      dsz->xd[i] = dszm->xd[i] = g_r->imi[page_n][i_page] - ims;
      if (brf == NULL)	dsz->yd[i] = g_r->z_cor * br->z[page_n][i_page];
      else dsz->yd[i] = g_r->z_cor * (br->z[page_n][i_page] - brf->z[page_n][i_page]);
      dszm->yd[i] = g_r->zmag[page_n][i_page];
      if (zmag_or_f == 1)
	{
	  if (i == 0 || lastzm != dszm->yd[i])
	    {
	      lastzm = dszm->yd[i];
	      dszm->yd[i] = zmag_to_force(0, dszm->yd[i]);
	    }
	  else dszm->yd[i] = dszm->yd[i-1];
	}
      if (zmag_or_f == 2)
	{
	  if (br->kx_angle > 0)
	    dsz->yd[i] = br->theta[page_n][i_page];
	  dszm->yd[i] = g_r->rot_mag[page_n][i_page];
	}

      if (i == 0)
	{
	  min = max = dsz->yd[i];
	  minz = maxz = dszm->yd[i];
	}
      min = (dsz->yd[i] < min) ? dsz->yd[i] : min;
      minz = (dszm->yd[i] < minz) ? dszm->yd[i] : minz;
      max = (dsz->yd[i] > max) ? dsz->yd[i] : max;
      maxz = (dszm->yd[i] > maxz) ? dszm->yd[i] : maxz;
    }
  dsz->nx = dsz->ny =  dszm->nx = dszm->ny = nf;

  j = ((int)(8*dsz->xd[0]))/scope_buf_size;
  job->op->x_lo = j*scope_buf_size/8;
  job->op->x_hi = (j+9)*scope_buf_size/8;
  set_plot_x_fixed_range(job->op);


  tmp = (max - min)*0.05;
  tmp = (tmp < min_scale_z) ? min_scale_z : tmp;
  for (i = 0, tmp2 = min_scale_z;tmp > tmp2; i++)
    {
      if (i%3 == 0) tmp2 += tmp2;
      else if (i%3 == 1) tmp2 += tmp2;
      else tmp2 *= 2.5;
    }
  tmp = tmp2/2;
  min -=  tmp;
  max +=  tmp;

  if (job->op->iopt2 & Y_LIM)  // fixed range
    {
      min = job->op->y_lo;
      max = job->op->y_hi;
    }


  tmp = (maxz - minz)*0.05;
  minz -=  6*tmp;
  maxz +=  tmp;


  if (minz > min && maxz < max)
    {
      job->op->yu[job->op->c_yu_p]->ax = 0;
      job->op->yu[job->op->c_yu_p]->dx = 1;
    }
  else
    {
      if ((maxz - minz) < min_scale_zmag)
	{
	  minz = (maxz + minz)/2;
	  if (zmag_or_f == 0)
	    {
	      maxz = minz + min_scale_zmag/2;
	      minz = minz - min_scale_zmag/2;
	    }
	  else
	    {
	      maxz = minz + min_scale_force/2;
	      minz = minz - min_scale_force/2;
	    }
	}
      tmp = (max-min)/(maxz - minz);
      for (i = 0; i < nf; i++)
	{
	  dszm->yd[i] = min + (dszm->yd[i] - minz)*tmp;
	}
      if (tmp > 0)
	{
	  job->op->yu[job->op->c_yu_p]->ax = minz - min/tmp;
	  job->op->yu[job->op->c_yu_p]->dx = (float)1/tmp;
	}
    }
  job->op->y_lo = min;
  job->op->y_hi = max;
  //set_plot_y_fixed_range(job->op);

  for (i = 0, j = 0, k = 0, dszf->yd[0] = dszf->xd[0] = 0; i < nf; i++)
    {
      dszf->yd[j] += dsz->yd[i];
      dszf->xd[j] += dsz->xd[i];
      k++;
      if (k >= fir)
	{
	  dszf->yd[j] /= fir;
	  dszf->xd[j] /= fir;
	  j++;
	  dszf->yd[j] = dszf->xd[j] = 0;
	  k = 0;
	}
    }
  dszf->nx = dszf->ny = j;
  //set_plot_title(job->op,"nf  %d fir %d j %d",nf,fir,j);
  job->op->need_to_refresh = 1;
  job->imi += 32;
  //else job->in_progress = 0;
  return 0;
}


int check_zdet(void)
{

  if(updating_menu_state != 0)
    {
      if (working_gen_record == NULL)
	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }
  if (working_gen_record != NULL)
    {
      win_printf("Gen record has %d pages \n"
		 "index %d",working_gen_record->n_page,
		 working_gen_record->in_page_index);
    }
  return D_O_K;
}

int do_move_zdet_in_x_right(void)
{
  pltreg *pr = NULL;
  g_record *g_r = NULL;
  O_p *op = NULL;
  char buf[256] = {0};

  ac_grep(cur_ac_reg,"%pr%op",&pr,&op);
  g_r = find_g_record_in_pltreg(pr);

  if(updating_menu_state != 0)  return 0;

  if (op != NULL && op->user_id == 4096 && g_r != NULL)
    {
      op->user_ispare[ISPARE_BUF_POSITION] += scope_buf_size>>1;
      snprintf(buf,256,"right to %d",op->user_ispare[ISPARE_BUF_POSITION]);
      op->user_ispare[ISPARE_TIME_OUT] = 10 + (int)(clock()/CLOCKS_PER_SEC);
      op->user_ispare[ISPARE_POSITION_UPDATE] = 1;
      set_window_title(buf);
    }
  return 0;
}

int do_move_zdet_in_x_left(void)
{
  pltreg *pr = NULL;
  g_record *g_r = NULL;
  O_p *op = NULL;
  char buf[256] = {0};

  ac_grep(cur_ac_reg,"%pr%op",&pr,&op);
  g_r = find_g_record_in_pltreg(pr);

  if(updating_menu_state != 0)  return 0;

  if (op != NULL && op->user_id == 4096 && g_r != NULL)
    {
      op->user_ispare[ISPARE_BUF_POSITION] -= scope_buf_size>>1;
      snprintf(buf,256,"left to %d",op->user_ispare[ISPARE_BUF_POSITION]);
      op->user_ispare[ISPARE_TIME_OUT] = 10 + (int)(clock()/CLOCKS_PER_SEC);
      op->user_ispare[ISPARE_POSITION_UPDATE] = 1;
      set_window_title(buf);
      //win_printf("left to %d",op->user_ispare[1]);
    }
  return 0;
}


int stop_zdet(void)
{
  pltreg *pr = NULL;
  g_record *g_r = NULL;
  O_p *op = NULL;

  //ac_grep(cur_ac_reg,"%pr%op",&pr,&op);

  pr = find_pr_in_current_dialog(NULL);
  if (pr != NULL)
    {
      g_r = find_g_record_in_pltreg(pr);
      op = pr->one_p;
    }
  if(updating_menu_state != 0)
    {

      if (op != NULL && op->user_id == 4096)
	{
	  remove_short_cut_from_dialog(0, KEY_RIGHT);
	  remove_short_cut_from_dialog(0, KEY_LEFT);
	  add_keyboard_short_cut(0, KEY_RIGHT, 0, do_move_zdet_in_x_right);
	  add_keyboard_short_cut(0, KEY_LEFT, 0, do_move_zdet_in_x_left);
	  //if (plot_wheel_action == NULL)	plot_wheel_action = select_zdet_plot_wheel_action;
	}
      if (working_gen_record == NULL || g_r != working_gen_record)
	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }
  if (working_gen_record != NULL)
    {
      working_gen_record = NULL;
      if (zdet_r != NULL)
	{
	  zdet_r->n_record++;
	  win_title_used = 0;
	  n_zdet++;
	  Open_Pico_config_file();
	  set_config_float("Zdet_Curve", "n_zdet", n_zdet);
	  write_Pico_config_file();
	  Close_Pico_config_file();

	}
    }
  ask_to_update_menu();
  return D_O_K;
}


int stop_zdet_after(void)
{
  pltreg *pr = NULL;
  g_record *g_r = NULL;
  int i, end = 10000;

  pr = find_pr_in_current_dialog(NULL);
  if (pr != NULL)
    {
      g_r = find_g_record_in_pltreg(pr);
    }
  if(updating_menu_state != 0)
    {
      if (working_gen_record == NULL || g_r != working_gen_record)
	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }
  if (working_gen_record != NULL)
    {
      i = win_scanf("In how many frames do you want to stop acquisition %d",&end);
      if (i == WIN_CANCEL) return 0;
      ending = g_r->abs_pos + end;
    }
  return D_O_K;
}




int draw_XYZ_trajectories(void)
{
  int i, j, page_n, i_page;
  b_track *bt = NULL;
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *ds = NULL;
  int nf;
  float rot_start = 10,  zmag;
  b_record *br = NULL;
  g_record *g_r = NULL;

  ac_grep(cur_ac_reg,"%pr",&pr);
  g_r = find_g_record_in_pltreg(pr);

  if(updating_menu_state != 0)
    {  // we wait for recording finish
      if (g_r == NULL || working_gen_record == g_r)
	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }

  if (pr == NULL)	return OFF;
  if (g_r == NULL || g_r->n_record == 0) return OFF;
  nf = g_r->abs_pos;
  if (nf <= 0) return OFF;

  rot_start = g_r->rot_mag[0][0];
  zmag = g_r->zmag[0][0];

  //we create one plot for each bead
  for (i = 0; i < g_r->n_bead; i++)
    {
      br = g_r->b_r[i];
      bt = br->b_t;
      if (bt->calib_im == NULL)	continue;
      op = create_and_attach_one_plot(pr, nf, nf, 0);
      ds = op->dat[0];
      set_dot_line(ds);
      set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
		    "X coordinate l = %d, w = %d, nim = %d\n"
		    "objective %f, zoom factor %f, sample %s Rotation %g\n"
		    "Z magnets %g mm\n"
		    "Calibration from %s"
		    ,Pico_param.camera_param.camera_frequency_in_Hz,n_zdet,i
		    ,track_info->cl,track_info->cw,nf
		    ,Pico_param.obj_param.objective_magnification
		    ,Pico_param.micro_param.zoom_factor
		    ,pico_sample
		    ,rot_start
		    ,zmag
		    ,bt->calib_im->filename);
      for (j = 0; j < nf; j++)
	{
	  page_n = j/g_r->page_size;
	  i_page = j%g_r->page_size;
	  ds->xd[j] = g_r->imi[page_n][i_page];
	  ds->yd[j] = g_r->ax + g_r->dx * br->x[page_n][i_page];
	}

       if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
	 return win_printf_OK("I can't create plot !");
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
		     "Y coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_zdet,i
		     ,track_info->cl,track_info->cw,nf
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,pico_sample
		     ,rot_start
		     ,zmag
		     ,bt->calib_im->filename);
      for (j = 0; j < nf; j++)
	{
	  page_n = j/g_r->page_size;
	  i_page = j%g_r->page_size;
	  ds->xd[j] = g_r->imi[page_n][i_page];
	  ds->yd[j] = g_r->ay + g_r->dy * br->y[page_n][i_page];
	}
       if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
	 return win_printf_OK("I can't create plot !");
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d \n "
		     "Z coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_zdet,i
		     ,track_info->cl,track_info->cw,nf
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,pico_sample
		     ,rot_start
		     ,zmag
		     ,bt->calib_im->filename);
      for (j = 0; j < nf; j++)
	{
	  page_n = j/g_r->page_size;
	  i_page = j%g_r->page_size;
	  ds->xd[j] = g_r->imi[page_n][i_page];
	  ds->yd[j] = g_r->z_cor * br->z[page_n][i_page];
	}
       create_attach_select_y_un_to_op(op, IS_METER, 0 ,(float)1, -6, 0, "\\mu m");
       create_attach_select_x_un_to_op(op, IS_SECOND, 0
				       ,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");


       set_op_filename(op, "X(t)Y(t)Z(t)bd%dzdet%d.gr",i,g_r->n_rec);
       set_plot_title(op, "Bead %d Z(t) %d zmag %g", i,g_r->n_rec,zmag);
       set_plot_x_title(op, "Time");
       set_plot_y_title(op, "Position");


       op->x_lo = (-nf/64); ///Pico_param.camera_param.camera_frequency_in_Hz;
       op->x_hi = (nf + nf/64); ///Pico_param.camera_param.camera_frequency_in_Hz;
       set_plot_x_fixed_range(op);
     }
  return refresh_plot(pr, pr->n_op - 1);
}

int draw_trajectories_params(void)
{
  int  j,  page_n, i_page;
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *ds = NULL;
  int nf;
  g_record *g_r = NULL;

  ac_grep(cur_ac_reg,"%pr",&pr);
  g_r = find_g_record_in_pltreg(pr);

  if(updating_menu_state != 0)
    {  // we wait for recording finish
      if (g_r == NULL || working_gen_record == g_r)
	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }

  if (pr == NULL)	return OFF;
  if (g_r == NULL || g_r->n_record == 0) return OFF;
  nf = g_r->abs_pos;
  if (nf <= 0) return OFF;


  //we create one plot for each bead
  op = create_and_attach_one_plot(pr, nf, nf, 0);
  ds = op->dat[0];
  set_dot_line(ds);
  set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d  \n "
		"Zmag l = %d, w = %d, nim = %d\n"
		"objective %f, zoom factor %f, sample %s\n"
		,Pico_param.camera_param.camera_frequency_in_Hz,n_zdet
		,track_info->cl,track_info->cw,nf
		,Pico_param.obj_param.objective_magnification
		,Pico_param.micro_param.zoom_factor
		,pico_sample);


  for (j = 0; j < nf; j++)
    {
      page_n = j/g_r->page_size;
      i_page = j%g_r->page_size;
      ds->xd[j] = g_r->imi[page_n][i_page];
      ds->yd[j] = g_r->zmag[page_n][i_page];
    }

  if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("I can't create plot !");
  set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d  \n "
		"magnets rotation l = %d, w = %d, nim = %d\n"
		"objective %f, zoom factor %f, sample %s\n"
		,Pico_param.camera_param.camera_frequency_in_Hz,n_zdet
		,track_info->cl,track_info->cw,nf
		,Pico_param.obj_param.objective_magnification
		,Pico_param.micro_param.zoom_factor
		,pico_sample);

  for (j = 0; j < nf; j++)
    {
      page_n = j/g_r->page_size;
      i_page = j%g_r->page_size;
      ds->xd[j] = g_r->imi[page_n][i_page];
      ds->yd[j] = g_r->rot_mag[page_n][i_page];
    }
  if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("I can't create plot !");
  set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d \n "
		"Objective position l = %d, w = %d, nim = %d\n"
		"objective %f, zoom factor %f, sample %s\n"
		,Pico_param.camera_param.camera_frequency_in_Hz,n_zdet
		,track_info->cl,track_info->cw,nf
		,Pico_param.obj_param.objective_magnification
		,Pico_param.micro_param.zoom_factor
		,pico_sample);


  for (j = 0; j < nf; j++)
    {
      page_n = j/g_r->page_size;
      i_page = j%g_r->page_size;
      ds->xd[j] = g_r->imi[page_n][i_page];
      ds->yd[j] = g_r->obj_pos[page_n][i_page];
    }
  create_attach_select_x_un_to_op(op, IS_SECOND, 0
				  ,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");


  set_op_filename(op, "ZmagRotFocuszdet%d.gr",g_r->n_rec);
  set_plot_title(op, "Z(t) %d zmag, rotation & focus ", g_r->n_rec);
  set_plot_x_title(op, "Time");
  set_plot_y_title(op, "Zmag, rot, focus");


  op->x_lo = (-nf/64); ///Pico_param.camera_param.camera_frequency_in_Hz;
  op->x_hi = (nf + nf/64); ///Pico_param.camera_param.camera_frequency_in_Hz;
  set_plot_x_fixed_range(op);

  return refresh_plot(pr, pr->n_op - 1);
}


char *generate_zdet_title(void)
{
  pltreg *pr = NULL;
  g_record *g_r = NULL;
  static char mes[256] = {0}, state[64] = {0};


  pr = find_pr_in_current_dialog(NULL);
  if (pr != NULL)      g_r = find_g_record_in_pltreg(pr);
  if (working_gen_record == NULL || g_r != working_gen_record)
    snprintf(state,64,"stopped");
  else snprintf(state,64,"running");
  snprintf(mes,256,"{Zdet  %s}", state);
  return mes;
}



int zdet_action(int im, int c_i, int aci, int init_to_zero)
{
  int  abs_pos;

  (void)im;
  (void)c_i;
  (void)aci;

  if (init_to_zero) return 0;

  if (zdet_r == NULL)  return 0;
  abs_pos = (zdet_r->abs_pos < 0) ? 0 : zdet_r->abs_pos;


  if (ending <= 0)             return 0;
  else if (abs_pos < ending)   return 0;
  else
    {
      if (working_gen_record != NULL)
	{
	  working_gen_record = NULL;
	  if (zdet_r != NULL)
	    {
	      zdet_r->n_record++;
	      win_title_used = 0;
	      n_zdet++;
	      Open_Pico_config_file();
	      set_config_float("Zdet_Curve", "n_zdet", n_zdet);
	      write_Pico_config_file();
	      Close_Pico_config_file();

	    }
	}
    }

  return 0;
}

int record_zdet(void)
{
  static int first = 1;
   int i, j, k, im, li;
   imreg *imr = NULL;
   O_i *oi = NULL;
   b_track *bt = NULL;
   pltreg *pr = NULL;
   O_p *op = NULL;
   d_s *ds = NULL;
   static int nf = 8192, with_radial_profiles = 0, streaming = 1, ortho_profiles = 0, file_selection = 0, fixed_bead_nbl = 0;
   static float rot_start = 10, zmag_start, zmag, movie_rw = 1.0, movie_rh = 1.0;
   static int special_saving = 0, rec_xy_prof = 0, rec_xy_diff_prof = 0, rec_small_image = 0, xyz_error = 1, xy_tracking_type, movie_track = 0;
   char question[4096], name[64], fullfilename[512];

   if(updating_menu_state != 0)
     {
       if (working_gen_record != NULL || track_info->n_b == 0)
	 active_menu->flags |=  D_DISABLED;
       else
	 {
           if (track_info->SDI_mode == 0)
              {
                  for (i = 0, k = 0; i < track_info->n_b; i++)
                     {
                         bt = track_info->bd[i];
                         k += (bt->calib_im != NULL) ? 1 : 0;
                     }
              }
           else k = 1;
	   if (k) active_menu->flags &= ~D_DISABLED;
	   else active_menu->flags |=  D_DISABLED;
	 }
       return D_O_K;
     }

   if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)	return OFF;


  Open_Pico_config_file();
  if (first)
    {
      n_zdet = get_config_float("Zdet_Curve", "n_zdet", n_zdet);
      first = 0;
    }
  Close_Pico_config_file();
  n_zdet = find_trk_index_not_used("Zdet_", n_zdet);

   rot_start = prev_mag_rot;
   zmag_start = prev_zmag;

   if (current_write_open_path_picofile != NULL)
     {
       snprintf(fullfilename,512,"%s\\Zdet_%d.trk",current_write_open_path_picofile,n_zdet);
       backslash_to_slash(fullfilename);
       file_selection = 0;
     }
   else
     {
       snprintf(fullfilename,512,"Zdet_%d.trk",n_zdet);
       backslash_to_slash(fullfilename);
       file_selection = 1;
     }

  for (i = 0, fixed_bead_nbl = -1; i < track_info->n_b; i++)
  {
       if (track_info->bd[i]->fixed_bead)
	     {
	        fixed_bead_nbl = i;
	        break;
	     }
  }
   snprintf(question,sizeof(question),"Zdet curve %%5d at Z_{mag} present val %g angular position %g\n"
	    "Number of points in the displayed plot %%8d\nNumber of points for averaging %%8d\n"
	    "You can specify a fixed bead to suppress drifts \n"
	    "Indicate the fixed bead number %%6d (set to negative value if not needed)\n"
	    "display zmag %%R or estimated force %%r rotation and angle %%r\n"
	    "\\fbox{Track will be saved in %s\n"
	    "Do you want to select a new file location %%R No %%r Yes\n"
	    "Click here to select special data saving options %%b\n}"
	    ,rot_start,zmag,fullfilename);
   i = win_scanf(question,&n_zdet,&scope_buf_size,&fir,&fixed_bead_nbl,&zmag_or_f,&file_selection ,&special_saving);
   if (i == WIN_CANCEL)	return OFF;

     if (special_saving)
     {
       snprintf(question,sizeof(question),"You can select here special saving options (increasing trk file size)\n"
       "Do you want to record bead profiles %%b\n"
       "Do you want to record bead orthoradial profiles %%b\n"
       "Do you want real time saving %%b\n"
       "Do you want to record bead xyz tracking error %%b\n"
       "Do you want to record bead xy tracking profiles %%b\n"
       "Do you want to record bead xy differential tracking profiles %%b\n"
       "Do you want to record bead image %%b (generates big trk file!)\n"
       "In this case define :\n"
       " \\oc  the ratio of bead image width with the cross arm (roi along x if sdi) %%6f\n"
       " \\oc  the ratio of bead image heigh with the cross arm (roi along y if sdi) %%6f\n"
       " \\oc  does the image center track the bead %%b\n");
       i = win_scanf(question,&with_radial_profiles,&ortho_profiles, &streaming, &xyz_error, &rec_xy_prof,
         &rec_xy_diff_prof, &rec_small_image, &movie_rw, &movie_rh, &movie_track);
         if (i == WIN_CANCEL)	 return OFF;
       }
   

   nf = (scope_buf_size > 8192) ? 2*scope_buf_size : 8192;

   snprintf(name,64,"Zdet curve %d",n_zdet);

   // We create a plot region to display bead position, zdet  etc.
   pr = create_hidden_pltreg_with_op(&op, nf, nf, 0,name);
   if (pr == NULL)  win_printf_OK("Could not find or allocte plot region!");
   //op = pr->one_p;
   ds = op->dat[0];

   //win_printf("nf %d",nf);
   xy_tracking_type = (rec_xy_prof) ? XY_BEAD_PROFILE_RECORDED : 0;
   xy_tracking_type |= (rec_xy_diff_prof) ? XY_BEAD_DIFF_PROFILE_RECORDED : 0;
   xy_tracking_type |= (rec_small_image) ? RECORD_BEAD_IMAGE : 0;
   xy_tracking_type |= (xyz_error) ? XYZ_ERROR_RECORDED : 0;


   zdet_r = create_gen_record(track_info, 1, with_radial_profiles,ortho_profiles,1,xy_tracking_type,movie_rw, movie_rh, movie_track);
   duplicate_Pico_parameter(&Pico_param, &(zdet_r->Pico_param_record));
   if (attach_g_record_to_pltreg(pr, zdet_r))
     win_printf("Could not attach grecord to plot region!");
   if (extract_file_name(zdet_r->filename, 512, fullfilename) == NULL)
     snprintf(zdet_r->filename,512,"Zdet_%d.trk",n_zdet);
   snprintf(zdet_r->name,512,"Zdet curve %d",n_zdet);
   zdet_r->n_rec = n_zdet;
   zdet_r->record_action = zdet_action;
   zdet_action(0,0,0,1);

   zdet_r->data_type |= ZDET_RECORD;

   if (streaming)
     {
       if (current_write_open_path_picofile != NULL && file_selection == 0)
	 {
	   strncpy(zdet_r->path,current_write_open_path_picofile,512);
	   write_record_file_header(zdet_r, oi_TRACK);
	   zdet_r->real_time_saving = 1;
	 }
       else if (do_save_track(zdet_r))
	 win_printf("Pb writing tracking header");
     }



   if (current_write_open_path_picofile != NULL)
     {
       dump_to_html_log_file_with_date_and_time(current_write_open_picofile,
						"<h2>Z(t) recording</h2>"
						"<br>in file %s in directory %s"
						"<br>zmag start %g, rot start %g <br>\n"
						,backslash_to_slash(zdet_r->filename),backslash_to_slash(zdet_r->path)
						,zmag_start,rot_start);
    }

   im = track_info->imi[track_info->c_i];
   im += 64;

   if (track_info->SDI_mode == 1)
   {
     //we create one plot for each bead
     for (i = li = 0; i < track_info->n_b; i++)
     {
       bt = track_info->bd[i];
       if ((bt->not_lost <= 0) || (bt->in_image == 0)) continue;
       if (li)
       {
         op = create_and_attach_one_plot(pr, nf, nf, 0);
         ds = op->dat[0];
       }
       li++;
       bt->opt = op;
       ds->nx = ds->ny = 0;
       set_dot_line(ds);
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d substracting fixed bead %d\n"
       "Z coordinate l = %d, w = %d, nim = %d\n"
       "objective %f, zoom factor %f, sample %s Rotation %g\n"
       "Z magnets %g mm\n" ,Pico_param.camera_param.camera_frequency_in_Hz,n_zdet,i,fixed_bead_nbl
       ,track_info->cl,track_info->cw,nf,Pico_param.obj_param.objective_magnification,Pico_param.micro_param.zoom_factor,pico_sample,rot_start,zmag);
       if ((ds = create_and_attach_one_ds(op, nf/fir, nf/fir, 0)) == NULL)  return win_printf_OK("I can't create plot !");
       ds->nx = ds->ny = 0;
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d substracting fixed bead %d\n"
       "filtred Z over %d coordinate l = %d, w = %d, nim = %d\n"
       "objective %f, zoom factor %f, sample %s Rotation %g\n"
       "Z magnets %g mm\n"
       ,Pico_param.camera_param.camera_frequency_in_Hz,n_zdet,i,fixed_bead_nbl
       ,fir,track_info->cl,track_info->cw,nf
       ,Pico_param.obj_param.objective_magnification ,Pico_param.micro_param.zoom_factor,pico_sample,rot_start,zmag);
       if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL) return win_printf_OK("I can't create plot !");
       ds->nx = ds->ny = 0;
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d substracting fixed bead %d\n"
       "Zmag l = %d, w = %d, nim = %d\n"
       "objective %f, zoom factor %f, sample %s Rotation %g\n"
       "Z magnets %g mm\n"
       ,Pico_param.camera_param.camera_frequency_in_Hz,n_zdet,i,fixed_bead_nbl,track_info->cl,track_info->cw,nf
       ,Pico_param.obj_param.objective_magnification,Pico_param.micro_param.zoom_factor,pico_sample,rot_start,zmag);
       if (zmag_or_f == 0)
       {
         create_attach_select_y_un_to_op(op, IS_METER, 0 ,(float)1, -3, 0, "mm");
         set_plot_y_prime_title(op, "Zmag");
       }
       else if (zmag_or_f == 1)
       {
         create_attach_select_y_un_to_op(op, IS_NEWTON, 0 ,(float)1, -12, 0, "pN");
         set_plot_y_prime_title(op, "Estimated force");
       }
       else
       {
         create_attach_select_y_un_to_op(op, IS_RAW_U, 0 ,(float)1, 0, 0, "rad");
         set_plot_y_prime_title(op, "Magnets Angle");
       }
       op->c_yu_p = op->c_yu;
       if ((zmag_or_f == 2) && (bt->kx_angle > 0))
       {
         create_attach_select_y_un_to_op(op, IS_RAW_U, 0 ,(float)1, 0, 0, "rad");
         set_plot_y_prime_title(op, "Bead Angle");
       }
       create_attach_select_y_un_to_op(op, IS_METER, 0 ,(float)1, -6, 0, "\\mu m");
       create_attach_select_x_un_to_op(op, IS_SECOND, 0,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");

         op->op_idle_action = zdet_op_idle_action;
         op->op_post_display = zdet_op_post_display;
         set_op_filename(op, "Z(t)fil-bd%dzdet%d.gr",i,n_zdet);
         if (fixed_bead_nbl < 0)	 set_plot_title(op, "Bead %d Z(t) %d zmag %g", i,n_zdet,zmag);
         else set_plot_title(op, "Bead %d Z(t) %d zmag %g - fixed bead %d", i,n_zdet,zmag, fixed_bead_nbl);
         set_plot_x_title(op, "Time");
         set_plot_y_title(op, "Molecule extension");


         op->x_lo = (-nf/64); ///Pico_param.camera_param.camera_frequency_in_Hz;
         op->x_hi = (nf + nf/64); ///Pico_param.camera_param.camera_frequency_in_Hz;
         set_plot_x_fixed_range(op);
         j = fill_next_available_job_spot(im, im+32+i, COLLECT_DATA, i, imr, oi, pr, op, NULL, zdet_job_z_fil, 0);
         if (j >= 0) job_pending[j].fixed_bead_nb = fixed_bead_nbl;
         op->user_id = 4096;
         op->user_ispare[ISPARE_BEAD_NB] = i;
       }
   }
   else
   {
     //we create one plot for each bead
     for (i = li = 0; i < track_info->n_b; i++)
     {
       bt = track_info->bd[i];
       if ((bt->calib_im == NULL) || (bt->not_lost <= 0) || (bt->in_image == 0)) continue;
       if (li)
       {
         op = create_and_attach_one_plot(pr, nf, nf, 0);
         ds = op->dat[0];
       }
       li++;
       bt->opt = op;
       ds->nx = ds->ny = 0;
       set_dot_line(ds);
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d substracting fixed bead %d\n"
       "Z coordinate l = %d, w = %d, nim = %d\n"
       "objective %f, zoom factor %f, sample %s Rotation %g\n"
       "Z magnets %g mm\n"
       "Calibration from %s"
       ,Pico_param.camera_param.camera_frequency_in_Hz,n_zdet,i,fixed_bead_nbl
       ,track_info->cl,track_info->cw,nf
       ,Pico_param.obj_param.objective_magnification
       ,Pico_param.micro_param.zoom_factor
       ,pico_sample
       ,rot_start
       ,zmag
       ,bt->calib_im->filename);
       if ((ds = create_and_attach_one_ds(op, nf/fir, nf/fir, 0)) == NULL)
       return win_printf_OK("I can't create plot !");
       ds->nx = ds->ny = 0;
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d substracting fixed bead %d\n"
       "filtred Z over %d coordinate l = %d, w = %d, nim = %d\n"
       "objective %f, zoom factor %f, sample %s Rotation %g\n"
       "Z magnets %g mm\n"
       "Calibration from %s"
       ,Pico_param.camera_param.camera_frequency_in_Hz,n_zdet,i,fixed_bead_nbl
       ,fir,track_info->cl,track_info->cw,nf
       ,Pico_param.obj_param.objective_magnification
       ,Pico_param.micro_param.zoom_factor
       ,pico_sample
       ,rot_start
       ,zmag
       ,bt->calib_im->filename);
       if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
       return win_printf_OK("I can't create plot !");
       ds->nx = ds->ny = 0;
       set_ds_source(ds,"Bead tracking at %g Hz Acquistion %d for bead %d substracting fixed bead %d\n"
       "Zmag l = %d, w = %d, nim = %d\n"
       "objective %f, zoom factor %f, sample %s Rotation %g\n"
       "Z magnets %g mm\n"
       "Calibration from %s"
       ,Pico_param.camera_param.camera_frequency_in_Hz,n_zdet,i,fixed_bead_nbl
       ,track_info->cl,track_info->cw,nf
       ,Pico_param.obj_param.objective_magnification
       ,Pico_param.micro_param.zoom_factor
       ,pico_sample
       ,rot_start
       ,zmag
       ,bt->calib_im->filename);
       if (zmag_or_f == 0)
       {
         create_attach_select_y_un_to_op(op, IS_METER, 0 ,(float)1, -3, 0, "mm");
         set_plot_y_prime_title(op, "Zmag");
       }
       else if (zmag_or_f == 1)
       {
         create_attach_select_y_un_to_op(op, IS_NEWTON, 0 ,(float)1, -12, 0, "pN");
         set_plot_y_prime_title(op, "Estimated force");
       }
       else
       {
         create_attach_select_y_un_to_op(op, IS_RAW_U, 0 ,(float)1, 0, 0, "rad");
         set_plot_y_prime_title(op, "Magnets Angle");
       }
       op->c_yu_p = op->c_yu;
       if ((zmag_or_f == 2) && (bt->kx_angle > 0))
       {
         create_attach_select_y_un_to_op(op, IS_RAW_U, 0 ,(float)1, 0, 0, "rad");
         set_plot_y_prime_title(op, "Bead Angle");
       }
       create_attach_select_y_un_to_op(op, IS_METER, 0 ,(float)1, -6, 0, "\\mu m");
       create_attach_select_x_un_to_op(op, IS_SECOND, 0 ,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");
         op->op_idle_action = zdet_op_idle_action;
         op->op_post_display = zdet_op_post_display;
         set_op_filename(op, "Z(t)fil-bd%dzdet%d.gr",i,n_zdet);
         if (fixed_bead_nbl < 0)	 set_plot_title(op, "Bead %d Z(t) %d zmag %g", i,n_zdet,zmag);
         else set_plot_title(op, "Bead %d Z(t) %d zmag %g - fixed bead %d", i,n_zdet,zmag, fixed_bead_nbl);
         set_plot_x_title(op, "Time");
         set_plot_y_title(op, "Molecule extension");
         op->x_lo = (-nf/64);
         op->x_hi = (nf + nf/64);
         set_plot_x_fixed_range(op);
         j = fill_next_available_job_spot(im, im+32+i, COLLECT_DATA, i, imr, oi, pr, op, NULL, zdet_job_z_fil, 0);
         if (j >= 0) job_pending[j].fixed_bead_nb = fixed_bead_nbl;
         op->user_id = 4096;
         op->user_ispare[ISPARE_BEAD_NB] = i;
       }
   }


   im = track_info->imi[track_info->c_i];
   working_gen_record = zdet_r;
   zdet_r->last_starting_pos = im;
   win_title_used = 1;
   if (imr_and_pr) switch_project_to_this_pltreg(pr);
   ask_to_update_menu();
   generate_imr_TRACK_title = generate_zdet_title;
   return D_O_K;
}



int zdet_scope_zize(void)
{
  pltreg *pr = NULL;
  g_record *g_r = NULL;
  O_p *op = NULL;

  ac_grep(cur_ac_reg,"%pr%op",&pr,&op);
  g_r = find_g_record_in_pltreg(pr);

  if(updating_menu_state != 0)  return 0;

  if (op != NULL && op->user_id == 4096 && g_r != NULL)
    {
      scope_buf_size = RETRIEVE_MENU_INDEX;
      if (op->user_ispare[ISPARE_BUF_POSITION] < scope_buf_size)
	op->user_ispare[ISPARE_BUF_POSITION] = scope_buf_size;
      op->user_ispare[ISPARE_TIME_OUT] = 10 + (int)(clock()/CLOCKS_PER_SEC);
      op->user_ispare[ISPARE_POSITION_UPDATE] = 1;
    }
  return 0;
}


MENU *zdet_menu(void)
{
	static MENU mn[32] = {0};

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn, "record Z(t)", record_zdet, NULL, 0, NULL);
	add_item_to_menu(mn, "check pages", check_zdet, NULL, 0, NULL);
	add_item_to_menu(mn, "stop recording", stop_zdet, NULL, 0, NULL);
	add_item_to_menu(mn,"\0",	NULL,	NULL,	0, NULL);

	return mn;
}

MENU *zdet_plot_menu(void)
{
	static MENU mn[32] = {0};

	if (mn[0].text != NULL)	return mn;
	//	add_item_to_menu(mn, "record Z(t)", record_zdet, NULL, 0, NULL);
	add_item_to_menu(mn, "check pages", check_zdet, NULL, 0, NULL);
	add_item_to_menu(mn, "stop recording now", stop_zdet, NULL, 0, NULL);
	add_item_to_menu(mn, "scheduled stop recording", stop_zdet_after, NULL, 0, NULL);
	add_item_to_menu(mn, "Scope size 256", zdet_scope_zize, NULL, MENU_INDEX(256), NULL);
	add_item_to_menu(mn, "Scope size 512", zdet_scope_zize, NULL, MENU_INDEX(512), NULL);
	add_item_to_menu(mn, "Scope size 1024", zdet_scope_zize, NULL, MENU_INDEX(1024), NULL);
	add_item_to_menu(mn, "Scope size 2048", zdet_scope_zize, NULL, MENU_INDEX(2048), NULL);
	add_item_to_menu(mn, "Scope size 4096", zdet_scope_zize, NULL, MENU_INDEX(4096), NULL);
	add_item_to_menu(mn, "Scope size 8192", zdet_scope_zize, NULL, MENU_INDEX(8192), NULL);



	add_item_to_menu(mn,"\0",	NULL,	NULL,	0, NULL);
	add_item_to_menu(mn, "Dump XYZ", draw_XYZ_trajectories, NULL, 0, NULL);
	add_item_to_menu(mn, "Dump Zmag Rot Focus", draw_trajectories_params, NULL, 0, NULL);
	add_item_to_menu(mn,"\0",	NULL,	NULL,	0, NULL);
	add_item_to_menu(mn, "move >>", do_move_zdet_in_x_right, NULL, 0, NULL);
	add_item_to_menu(mn, "move <<", do_move_zdet_in_x_left, NULL, 0, NULL);

	return mn;
}


# endif
