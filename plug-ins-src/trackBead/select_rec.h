
#ifndef _SELECT_REC_H_
#define _SELECT_REC_H_

# include "record.h"
// a structure containing the information relative to a segment of an event


#ifdef _SELECT_REC_C_

#else

#endif

PXV_FUNC(int, free_event_for_bead, (b_record *b_r, int i_ev));

PXV_FUNC(MENU*, select_plot_menu, (void));
PXV_FUNC(int, redraw_scan_zmag_from_trk, (void));
PXV_FUNC(int, find_segment_from_point,(pltreg *pr, O_p *op, float x, float y, int *i_ev, int *i_seg, int *end));
PXV_FUNC(int,find_front_position,(g_record *gr, b_record *br, b_record *brf, int start, int pos_estimate, int end, int mov_allow));

# endif
