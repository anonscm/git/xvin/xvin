#pragma once
#include "thorlabs/apt_translation_unix.h"

int move_step_translation(int channel, float step);
int do_y_translation_apt_up(void);
int do_x_translation_apt_right(void);
int do_y_translation_apt_down(void);
int do_x_translation_apt_left(void);
int do_change_step_translation_value(void);
MENU *translation_control_plot_menu(void);
