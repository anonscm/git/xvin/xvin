#ifndef _TRACK_UTIL_H_
#define _TRACK_UTIL_H_


# include "brown_util.h"
# include <gsl/gsl_blas.h>
#include <gsl/gsl_permutation.h>
#include <gsl/gsl_linalg.h>
#ifdef SDI_VERSION
# include "SDITracking_v2/sdi_v2_general.h"
#endif



#undef TRACKING_BUFFER_SIZE
//#ifndef TRACKING_BUFFER_SIZE
# ifndef   SDI_MODE
# define TRACKING_BUFFER_SIZE 2048
# else
# define TRACKING_BUFFER_SIZE 256
# endif

// This bit sets on the status_flag
// first group is toggle state
# define USER_MOVING_OBJ     0x00000010     // 5
# define OBJ_MOVING          0x00000020     // 6
# define LOCK_MOVING_OBJ     0x00000008     // 4
# define ZMAG_MOVING         0x00000040     // 7
# define ROT_MOVING          0x00000080     // 8

# define PARTS_MOVING        0x000000F0// 0x03F0
// second group is instantaneous state
# define BEAD_CROSS_SAVED    0x00000100     // 9
# define BEAD_CROSS_RESTORE  0x00000200     // 10
# define REF_ROT_CHANGED     0x00000400     // 11
# define REF_ZMAG_CHANGED    0x00000800     // 12

# define ROT_SPEED_CHANGE    0x00001000     // 13
# define ZMAG_SPEED_CHANGE   0x00002000     // 14
# define LED_CURRENT_CHANGED 0x00004000     // 15
# define TEMPERATURE_CHANGED 0x00008000     // 16
# define ZVCAP_MIN_CHANGED   0x00010000     // 17

# define ZMAG_FACTOR_CHANGED 0x00020000     //18 tv 090317 : instantaneous signal of translation factor changed

# define CAMERA_EXPOSURE_CHG 0x08000000     // 28
# define CAMERA_GAIN_CHG     0x10000000     // 29
# define LED_CURRENT_CHG     0x20000000     // 30
# define TEMP_CURRENT_CHG    0x40000000     // 31


// point of measurement

# define OBJ_MOVING_SAMPLED  0x00100000     // 21
# define ZMAG_MOVING_SAMPLED 0x00200000     // 22
# define ROT_MOVING_SAMPLED  0x00400000     // 23

# define PARAMETERS_CHANGING 0x0000FCF0

// error state

# define VCAP_SERVO_ON       0x00800000     // 24
# define POSITIVE_LIMIT_ON   0x01000000     // 25
# define NEGATIVE_LIMIT_ON   0x02000000     // 26
# define RINCING_FLOW_ON     0x04000000     // 27
# define ENZYME_FLOW_ON      0x80000000     // 32

# define ENZYME_2_FLOW_ON    0x00020000

// 0x00000001 0x00000002 0x00000004  are used to define the enzyme injection device

// not used yet 0x00040000  0x00080000





#define heap_check()                  check_heap_raw(__FILE__, __LINE__)

#ifdef SDI_VERSION_2
//#define MAX_K_BUFFER_SIZE 2048

typedef struct fourier_decomposition
{
  double *amplitude;
  double *phase;

} fdcmp;



typedef struct franges_track
{
  float fx,fy; //fringes position in pixel
  int ifx, ify;
  double phi0;
  double r0; //voir comment initialiser la phase du haut
  float *xi;
  float *yi;
            // tmp int buffers to avg profile
  float *xibp;           // tmp int buffers to bandpass side profile
  float *xf;           // tmp float buffers to do fft
  float max_pos; // position of the max of amplitude
  float max;      // tmp float buffers to do fft
  float dx;                         // the x shift in profile
  float dy;
  float push;
  int c_buff;

  //pure real method
  int best_align;
  int discretization;
  int disp_range;
  int profile_size;
  float *mean_profile;
  float *temp_dsignal;
  int *mean_profile_nb;
  float photons_per_level;
  int temp_start;
  int temp_end;
  float *scores;
  int cl;
  float yslope;

  int buffer_full;
  float quality;
  float quality_max;
  float x[TRACKING_BUFFER_SIZE];     // top fringes position
  float y[TRACKING_BUFFER_SIZE];
  float *avg_profile;
  float *avg_profile_er;
  int init_cycles;
  gsl_matrix *matxt;
  double *fit_phase_coeffs;
  double *fit_max_coeffs;
  int mouse_draged;

  d_s *fit_phase_corr;

  //int c_k;
  //int n_k;
  int yg;
  int yd;
  fdcmp** fpb;

  //double sum_k;
  //double *k_buffer;
  fft_plan *fp;
  d_s *dsfit; //can be useful
  d_s *fitphase;
} sdi_fr;
#endif

typedef struct bead_tracking
{
  int xc, yc; // current cross position in pixel
  int x0, y0; // last correct bead position in pixel
  int not_lost; // count down when bead is lost in a frame, when it reach 0, bead is lost
  int im_prof_ref;
  int xc_1, yc_1;                  // Bead position of previous frame
  int cl, cw; // cross lenght and width
  int type; // specify is ring-> 0 or sdi -> 1
#ifdef SDI_VERSION_2
  int filter_low;
  int filter_high;
  int fixed_mode;
  int moving_forbidden;
  int filter_length;
  int im_buffer_size;
  int mode_avg_fix;

  float sigma_g;
  float om_cos;
  int min_max_iter;
  float min_epsabs;
  float min_epsrel;
  int do_minimize;

  int dy0_fringes; // distance between fringes
  int h_fringes; // fringes height
  int dx_fringes; // fringes max shift
  int fringes_dy_tolerence;
  int problem_fringes;     // for sdi
  int fringes_ny;                  // to track fringes in y
  float fringes_per;    // valeur de 10
  sdi_fr *frange_top ;
  sdi_fr *frange_bottom ;

  float coeffzbot;
  float coeffztop;

  gsl_matrix *w;
  gsl_matrix *matx;
  gsl_matrix *temp;
  gsl_matrix *temp2;
  gsl_matrix *temp3;
  gsl_matrix *temp4;
  gsl_vector *maty;
  gsl_vector *beta;
  gsl_permutation *perm;
  float is_free[TRACKING_BUFFER_SIZE];
#endif
  int calib_im_size;
  int dis_filter, bd_mul, ncl, ncw, cross_45;   // filter def, and bd_mul define the loss of bead condition
  int max_limit;                   // limit the extend in autoconvolution to look for maximum
  int in_image, mouse_draged, mx, my;
  float last_x_not_lost;           // the bead x position saved before a rotation for instance
  float last_y_not_lost;           // the bead y position saved before a rotation for instance
  float saved_x, saved_y;          // position saved
  float calibration_im_x, calibration_im_y;          // calibration
  int calibration_im_xi, calibration_im_yi;          // calibration in screen pixel
  float fence_x, fence_y;          // the fence center in micron: it is calibration_im_x, y except if fence has been moved
  int fence_xi, fence_yi;          // the fence center: it is calibration_im_xi, yi except if fence has been moved
  int start_im;                    // im number for tracking start
  int black_circle;                // flag modifying the bead tracking extend
  fft_plan *xy_trp;                // the fft plan for x, y tracking
  filter_bt *xy_fil;                  // the filter paln for x, y tracking
  int bead_number;                 // a unique bead identifier
  int bp_center;                   // the bp center frequency
  int bp_width;                    // the bp width frequency
  int rc;                          // the critical radius
  int npc;                         // the number of points in radial profile
  float apx;                       // the ratio between image pixel size in x and in radial profile
  float apy;                       // the ratio between image pixel size in y and in radial profile
  fft_plan *z_trp;                 // the fft plan for x, y tracking
  filter_bt *z_fil;                   // the filter plan for x, y tracking
  float x[TRACKING_BUFFER_SIZE];     // bead position
  float y[TRACKING_BUFFER_SIZE];
  float z[TRACKING_BUFFER_SIZE];

#ifdef GAME
  float xt[TRACKING_BUFFER_SIZE]; //simulated bead position
  float yt[TRACKING_BUFFER_SIZE];
  float zt[TRACKING_BUFFER_SIZE];
#endif
  char n_l[TRACKING_BUFFER_SIZE];                       // not lost idicator
  float rad_prof_ref[PROFILE_BUFFER_SIZE];             // reference radial profile
  float rad_prof_ref_fft[PROFILE_BUFFER_SIZE];         // reference radial profile fourrier transform
  float rad_prof_fft[PROFILE_BUFFER_SIZE];             // radial profile fourrier transform
  float rad_prof[TRACKING_BUFFER_SIZE][PROFILE_BUFFER_SIZE];                        // radial profile
#ifdef OPENCL
  float rad_prof_gpu[TRACKING_BUFFER_SIZE][PROFILE_BUFFER_SIZE];                        // radial profile
#endif
  float orthoradial_prof[TRACKING_BUFFER_SIZE][PROFILE_BUFFER_SIZE];                 // ortho radial profile
  float orthoradial_prof_fft[PROFILE_BUFFER_SIZE];               // ortho radial profile array for fft use
  float theta[TRACKING_BUFFER_SIZE]; // the angle measured with othoradial profile
  float spot_light[TRACKING_BUFFER_SIZE]; // the integrated light intensity of the bead for evanescent wave
  int orthoradial_prof_size;       // ortho radial profile size
  int orthoradial_mean_radius;     // ortho radial profile mean radius
  int orthoradial_half_avg_size;   // ortho radial profile radius width
  int kx_angle;                    // fourier mode used to measure angle
  float angle_1;                   // raw phase angle position of last image
  float angle;                     // raw phase angle position of present image
  int phi_jump;                    // number of phase jumps in raw phase
  fft_plan *o_prof_p;              // the fft plan for orthoradial profiles
  O_i *calib_im;		           // calibration image
  O_i *calib_im_fil;	           // filtered calibration image
  int profile_index;               // indicate the nearest profile in calibration image
  float min_xi2;                   // minimum found for profile
  float min_phi;                   // phase at minimum
  int bead_generic_index;          // specify the generic calibration type. -1 mean no generic calibration
  O_i *generic_calib_im;	       // calibration image
  O_i *generic_calib_im_fil;	   // filtered generic calibration image
  int generic_profile_index;       // indicate the nearest profile in generic calibration image
  float generic_min_xi2;           // minimum found for profile
  float generic_min_phi;           // phase at minimum
  O_p *opt;                        // trajectory of the last force point just acquired
  int movie_x0;                    // a rectangle used to record a movie
  int movie_y0;                    // centered on x0, Y0 of width w and height h
  int movie_w;                     // if movie_h <= 0 or movie_w <= 0
  int movie_h;                     // no movie to record
  float mxm;                       // mean value of x coordinate
  float dmxm;                      // error in mean value of x coordinate
  float sx2;                       // mean variance of x coordinate
  float fcx;                       // cutoff frequency in x
  float dfcx;                      // error in cutoff frequency in x
  float ax;                        // integral of fluctuation in x
  float dax;                       // error in integral of fluctuation in x
  float chisqx;                    // chi square in x
  float mym;                       // mean value of y coordinate
  float dmym;                      // error in mean value of y coordinate
  float sy2;                       // mean variance of y coordinate
  float fcy;                       // cutoff frequency in y
  float dfcy;                      // error in cutoff frequency in y
  float ay;                        // integral of fluctuation in y
  float day;                       // error in integral of fluctuation in y
  float chisqy;                    // chi square in y
  float mzm;                        // mean value of z coordinate
  float dmzm;                       // error in mean value of z coordinate
  float sz2;                       // mean variance of z coordinate
  float fcz;                       // cutoff frequency in z
  float dfcz;                      // error in cutoff frequency in z
  float az;                        // integral of fluctuation in z
  float daz;                       // error in integral of fluctuation in z
  float chisqz;                    // chi square in z
  int fixed_bead;                  // specify if bead is a reference fixed beadd
  int coilable_bead;               // specify if bead is torsion sensitive
  int z_track;                     // = 1 if zposition found
  float z_avgd;                    // the bead averaged z position displayed
  float z_avg;                     // the bead averaged z position
  int iz_avg;                      // the nb of bead averaged z position
  float dz_max;                    // the maximum dz measured
  int dz_max_im;                   // the image index of this max
  float z_old_n;                    // the z of bead n frame before now to compute dz
#ifndef SDI_VERSION_2
  int xi[256], yi[256];             // tmp int buffers to avg profile
  int xis[256], yis[256];           // tmp int buffers to avg side profile
  float xf[256], yf[256];           // tmp float buffers to do fft
  float xcor[256], ycor[256];
  float fx1[512], fy1[512];         // tmp float buffers to do fft
  float dx;                         // the x shift in profile
  float dy;                         // the y shift in profile
#endif

  int *x_var, *y_var;               //
  int *xs_var, *ys_var;
  int *x_var_1, *y_var_1;
  int *xs_var_1, *ys_var_1;
  int x_v[2][256], y_v[2][256];     // tmp float buffers to variance in avg profile
  int xs_v[2][256], ys_v[2][256];   // tmp float buffers to variance in avg side profile
  float x_v_var, xs_v_var, x_v_var_m, xs_v_var_m;
  float y_v_var, ys_v_var, y_v_var_m, ys_v_var_m;
  float dxcor_dx, dxcor_dx_m, dycor_dy, dycor_dy_m;
  float dcor_dx[TRACKING_BUFFER_SIZE];
  float dcor_dy[TRACKING_BUFFER_SIZE];     // derivative of the convolution
  float xmax, ymax;
  float xnoise[TRACKING_BUFFER_SIZE];
  float ynoise[TRACKING_BUFFER_SIZE];             // square of noise of bead profiles
  int free_bead;
  //when sdi tracking
#ifdef SDI_VERSION
  sdi_v2_bead *sdi_bd;
#endif
} b_track;



typedef struct _rs232_querry
{
  //data written by the real time timer thread
  int imi;                           // the image nb where to act, if < 0 the next possible frame
  int im_done;                       // the image when read
  int proceeded_RT;                  //  1 if treated, 0 if pending, 2 data copied
  int acknowledge;                   // set to im by the real time timer thread when proceed is turn to 0
  int type;
  float value;
  int spare;                       // a spare int data for user
  //im_nb written by other threads requesting the real time timer thread so set proceed to 0 if proceeded_ask_bo_be_clear >= acknowledge
  int proceeded_ask_bo_be_clear;
} rs232_querry;



typedef struct optical_tweezers
{
  float xc, yc, zc;                // trap position
  float dx, dy, dz;                // trap size
  float strength_x, strength_y, strength_z;
  int cr_l;                        // cross length
  int cr_w;                        // cross width

} op_tweezers;


# define BUF_FREEZED 2
# define BUF_CHANGING 1
# define BUF_UNLOCK 0

typedef struct gen_tracking
{
  int (*user_tracking_action)(struct gen_tracking *gt, DIALOG *d, void *data);  // a user callback function
  int c_i, n_i, m_i, lc_i;          // c_i, index of the current image, n_i size of buffer, m_i size allocated
  int ac_i, lac_i;                  // ac_i actual number of frame since acquisition started + local
  int ac_ri, a_ri;                  // the absolute and relative index of the last pending request
  int ac_pri, a_pri;                // the absolute and relative index of the previous pending request
  int cl, cw, dis_filter, bd_mul;   // cross size, filter def, and bd_mul define the loss of bead condition
  int max_limit;                    // to keep maximum search in a limited range
  float bead_fence;                 // keep bead inside of circle bead_fence arround calibration position (when lost) in microns
  int bead_fence_in_pixel;
  int c_b, n_b, m_b;                // the current bead nb and the max bead nb
  int stop_xy_tracking;             // stop cross repositionning
  int obj_servo_rate;               // nb of frames between two servo moves
  float max_obj_mov;                // Maximum relative objective displacement in servo mode
  float z_obj_servo_start;          // objective position at the start of servo mode
  int bead_servo_nb;                // the bead used to servo the objective
  float integral;                    // a local variable containinin the integral of PID
  float sat_int;                   // the maxium integral value in PID
  float gain;                      // the PID parameters
  float integral_gain;
  float derivative_gain;
  float dz_error_avg;               // averaging of the error signal
  int obj_servo_action;             // indicate if a feedback move is required
  int obj_servo_profile_index;      // indicate arround which profile feedback is done
  float new_obj_servo_pos;          // the new objective position defined par servo
  int n_bead_display;               // the bead number corresponding to the plot display
  int local_lock;                   // 0 unlock, 1 changing, 2 freezed
  int xi[256], yi[256];             // tmp int buffers to avg profile
  float xf[256], yf[256];           // tmp float buffers to do fft
  b_track **bd;                    // the bead pointer
  int c_oi, n_oi, m_oi;            // the current bead nb and the max bead nb
  O_i **generic_calib_im;	   // generic calibration image
  O_i **generic_calib_im_fil;	   // filtered generic calibration image
  float generic_reference;         // offset in z position
  int imi[TRACKING_BUFFER_SIZE];                         // the image nb
  int limi[TRACKING_BUFFER_SIZE];                         // local buffer the image nb
  int imit[TRACKING_BUFFER_SIZE];                        // the image nb by timer
  int limit[TRACKING_BUFFER_SIZE];                        // the image nb by timer
  long long imt[TRACKING_BUFFER_SIZE];                   // the image absolute time
  long long limt[TRACKING_BUFFER_SIZE];                   // local the image absolute time
  long long imtt[TRACKING_BUFFER_SIZE];                  // the image absolute time obtained by timer
  unsigned long imdt[TRACKING_BUFFER_SIZE];              // the time spent in the previous function call
  unsigned long limdt[TRACKING_BUFFER_SIZE];              // local the time spent in the previous function call
  unsigned long imtdt[TRACKING_BUFFER_SIZE];             // the time spent before image flip in timer mode
  float zmag[TRACKING_BUFFER_SIZE];                      // the magnet z position  measured
  float rot_mag[TRACKING_BUFFER_SIZE];                   // the magnet rotation position  measured
  float obj_pos[TRACKING_BUFFER_SIZE];                   // the objective position measured
  float Vcap_zmag[TRACKING_BUFFER_SIZE];                  // the inductive sensor signal for magnet position measured
  float obj_err[TRACKING_BUFFER_SIZE];                   // the objective error signal measured
  //float *user_val1, user_val2;    // user parameters
  int status_flag[TRACKING_BUFFER_SIZE];
  float zmag_cmd[TRACKING_BUFFER_SIZE];                  // the magnet z position command
  float rot_mag_cmd[TRACKING_BUFFER_SIZE];               // the magnet rotation position command
  float obj_pos_cmd[TRACKING_BUFFER_SIZE];               // the objective position command
  char temp_message[TRACKING_BUFFER_SIZE];               // a string containing temperature message
  int nb_bd_in_view[TRACKING_BUFFER_SIZE];               // the number of beads visible in view field
  int last_nb_of_bd_found;
  int n_op_t;                                            // number of optical tweezers
  struct optical_tweezers **op_t;
  float focus_cor;                                       // the immersion correction factor in Z
  int im_focus_check;                                    // the number of images between two ocus read per
  float T0;                                              // the sensor temperature
  float T1;                                              // the sample temperature
  float T2;                                              // the thermal sink temperature
  float T3;                                              // the outside temperature
  float I0;                                              // the peltier current
  int im_temperature_check;                              // the number of images between two temperature read
  int temp_cycle;                                        // the temperature cycle position
  char temp_mes[32];                                     // the string used to save temperature
  int itemp_mes;                                         // the position of byte in string used to save temperature
  int ntemp_mes;                                         // the max position of byte in string used to save temperature
  int com_skip_fr;                                       // communication rate may be different from camera
                                                         // this is the ratio between the two
  int zmag_limit_on_start;                               // the limit switch state when motion started
  int zmag_positive_limit_when_on;                       // set if the motors are bumping in one of the limit switch
  int zmag_negative_limit_when_on;                       // set if the motors are bumping in one of the limit switch
  int vcap_servo_on;                                     // z servo is done par inductive sensor
  float vcap_min;                                        // the voltage to servo inductive sensor (0 no servo)
  int do_diff_track;                                     // use of differential tracking
  int camera_exposure_time_us;                           // 0 means shutter off
  int camera_min_exposure;                               // shutter min value
  int camera_max_exposure;                               // shutter max value
  float camera_gain;                                     // to compensate short exposure
  float camera_min_gain;                                 // to compensate short exposure
  float camera_max_gain;                                 // to compensate short exposure
  float led_level;                                       // increased to compensate short exposur
  float eva_decay;                                       // evanescent decay length
  float eva_decay_cor;                                   // evanescent decay length corrected by the
                                                         // micoscope correction to keep compatibility
  float eva_offset;                                      // evanescent offset
  int evanescent_mode;
  float zmag_from_Vcap;
  bool is_using_gpu;




  float track_free_x_bar;
  float track_free_y_bar;
  int do_move_free_bead;
  int SDI_2_track_free;
  int track_free_lum_threshold;
  int track_free_y_low;
  int track_free_y_high;
  int track_free_window_width;
  int track_free_shift_window;
  d_s *dsfreebeads;
  int track_free_xw;
  int track_free_yw;
  int track_free_window_x;
  int track_free_window_y;

  int SDI_mode; //1 if sdi on
#ifdef SDI_VERSION
  sdi_v2_parameter *sdi_param; //when sdi on, it contains the parameters
#endif
#ifdef RAWHID_V1
  float led_level_2;
#endif
} g_track;


# ifdef _TRACK_UTIL_C_
g_track *track_info = NULL;
int px0,px1,py0,py1;
float intensity[TRACKING_BUFFER_SIZE];
float prev_zmag = 0;
float prev_mag_rot = 0;
float prev_focus = 0;
float prev_load = 0;
float prev_freq = 0;


int eva_spot_radius = 10;           // r below which intensity is added with a weight 1
                                    // for r > re && r < 2*re cosine appodisation
int eva_black_radius = 20;          // radius above which intensity is black level

float evanescent_spot_weighting[256];
//int evanescent_black_level_histo[4096];  // ready for 12 bits
float evanescent_avg_intensity;
float evanescent_avg_n_appo;
float evanescent_black_avg;
int evanescent_black_n_avg;

int fft_used_in_tracking = 0;
int fft_used_in_dialog = 0;
rs232_querry last_zmag_pid;
rs232_querry last_rot_pid;
rs232_querry last_T0_pid;
rs232_querry last_zmag_read;
rs232_querry last_rot_read;
rs232_querry last_led_read;
rs232_querry last_T_read;
rs232_querry last_focus_read;

char *RT_debug_file = "c:\\RT_debug.txt";
int RT_debug_flag = 0;
FILE *fp_RT_debug = NULL;

unsigned long manual_rincing_start = 0;
float rincing_time_ins = 0.0;
int manual_rincing = 0;
int manual_rincing_on = 0;


unsigned long manual_enzyme_injection_start = 0;
float enzyme_injection_time_ins = 0.0;
int manual_enzyme_injection = 0;
int manual_enzyme_injection_on = 0;
int enzyme_nb = 1;

int calib_im_nx_equal_cross_arm_length = 1;
int calib_im_nx_set = 128;
int max_shear_dx = 32;
int dy_fringes = 30;
int fringes_ny = 20;
int dy_fringes_tolerence = 8;
int filter_sdi2_low = 10;
int filter_sdi2_high = 17;
int sdi2_fixed_mode = 14;
int sdi2_buffer_size = -32;
int sdi2_discretization = 1024;
int sdi2_disp_range = 1;

float sdi2_sigma_g = 13.04;
float sdi2_om_cos = 0.6917;
int sdi2_min_max_iter = 2048;
float sdi2_min_epsabs = 0.001;
float sdi2_min_epsrel = 0 ;
int sdi2_do_minimize = 1;

# ifdef USE_MLFI
int beadnb_w_z_switched_to_mfi = -1;
# endif
//int evanescent_intensity = -1;
# else

PXV_VAR(int, calib_im_nx_equal_cross_arm_length);
PXV_VAR(int, calib_im_nx_set);

PXV_VAR(int, max_shear_dx);
PXV_VAR(int, dy_fringes);
PXV_VAR(int, dy_fringes_tolerence);
PXV_VAR(int, fringes_ny);

PXV_VAR(g_track*, track_info);
PXV_VAR(int, px0);
PXV_VAR(int, px1);
PXV_VAR(int, py0);
PXV_VAR(int, py1);
PXV_VAR(int,x_center);
PXV_VAR(int,y_center);
PXV_VAR(float, prev_zmag);
PXV_VAR(float, prev_mag_rot);
PXV_VAR(float, prev_focus);
PXV_VAR(float, prev_load);
PXV_VAR(float, prev_freq);


PXV_VAR(int, eva_spot_radius);
PXV_VAR(int, eva_black_radius);

PXV_VAR(float*, evanescent_spot_weighting);
//PXV_VAR(int*, evanescent_black_level_histo);  // ready for 12 bits

PXV_VAR(float, evanescent_avg_intensity);
PXV_VAR(float, evanescent_avg_n_appo);

PXV_VAR(float, evanescent_black_avg);
PXV_VAR(int, evanescent_black_n_avg);

PXV_VAR(rs232_querry, last_zmag_pid);
PXV_VAR(rs232_querry, last_rot_pid);
PXV_VAR(rs232_querry, last_T0_pid);
PXV_VAR(rs232_querry, last_zmag_read);
PXV_VAR(rs232_querry, last_rot_read);
PXV_VAR(rs232_querry, last_led_read);
PXV_VAR(rs232_querry, last_T_read);
PXV_VAR(rs232_querry, last_focus_read);


//PXV_VAR(int, evanescent_intensity);
PXV_VAR(int, fft_used_in_tracking);
PXV_VAR(int, fft_used_in_dialog);

PXV_ARRAY(MENU, bead_active_menu);
# ifdef USE_MLFI
PXV_VAR(int, beadnb_w_z_switched_to_mfi);
# endif

# endif


//PXV_FUNC(int, sdi_v2_import_all_sdi_bead_into_b_track)
#ifdef SDI_VERSION
PXV_FUNC(int, sdi_v2_proceed_bead, (b_track *bt, int bt_index, g_track *gt, BITMAP *imb, O_i *oi, int ci, int n));
PXV_FUNC(int, sdi_v2_attach_sdi_bead_to_b_track, (sdi_v2_bead *bead, b_track *bd));//links an existing bead
#endif

PXV_FUNC(int, show_bead_cross, (BITMAP *imb, imreg *imr, DIALOG *d));
PXV_FUNC(int, record_calibration, (void));

PXV_FUNC(int, get_present_image_nb, (void));
PXV_FUNC(int, move_bead_cross, (BITMAP *imb, imreg *imr, DIALOG *d, int x0, int y0));
PXV_FUNC(int, x_imdata_2_imr_nl, (imreg *imr, int x, DIALOG *d));
PXV_FUNC(int, y_imdata_2_imr_nl, (imreg *imr, int x, DIALOG *d));
PXV_FUNC(int, ask_to_update_menu,(void));
PXV_FUNC(int, check_heap_raw,(char *file, unsigned long line));
PXV_FUNC(int, grab_basename_and_index, (char *filename, int n_file, char *basename, int n_base));
PXV_FUNC(int, do_follow_bead_in_x_y,(int mode));
#ifdef SDI_VERSION
PXV_FUNC(int, do_add_bead_in_x_y,(int cl, int cw, float x0, float y0));
#else
PXV_FUNC(int, do_add_bead_in_x_y,(int cl, int cw, int calib_im_size, int fringes_dist,
		       int fringes_dy_tolerence, int fringes_ny,
                       float x0, float y0,int filter_low, int filter_high, int im_buffer_size, int fixed_mode, int discretization, int disp_range,
                       int do_minimize,float sigma_g,float om_cos,int min_max_iter,float min_epsrel,float min_epsabs));
#endif
PXV_FUNC(int, do_configure_create_cross_by_shortcut,(void));
PXV_FUNC(int, create_cross_by_shortcut,(void));
PXV_FUNC(int, create_fixed_cross_by_shortcut,(void));


PXV_FUNC(int, RT_debug, (char *format, ...) __attribute__((format(printf, 1, 2))));
PXV_FUNC(int,    dump_to_error_file_only,(const char *fmt, ...) __attribute__((format(printf, 1, 2))));
PXV_FUNC(int,    dump_to_error_file_with_date_and_time, (const char *fmt, ...) __attribute__((format(printf, 1, 2))));
PXV_FUNC(int,    dump_to_error_file_with_time, (const char *fmt, ...) __attribute__((format(printf, 1, 2))));


PXV_FUNC(int, switch_RT_debug_on,(void));
PXV_FUNC(int, switch_RT_debug_off,(void));

PXV_FUNC(int, spectrum_profile, (void));
PXV_FUNC(int, stop_spectrum_idle_action,(void));
PXV_FUNC(int, intensity_vs_time, (void));
PXV_FUNC(int, stop_intensity_vs_time_idle_action, (void));
PXV_FUNC(int, save_finite_movie, (void));
PXV_FUNC(int, mod_params, (void));
PXV_FUNC(int, do_z_profile_mean_and_sigma_of_movie, (void));
PXV_FUNC(int, do_z_profile_mean_and_sigma_of_area_in_movie, (void));
PXV_FUNC(int, do_z_profile_mean_and_sigma_of_movie_auto, (void));
PXV_FUNC(int, do_z_profile_mean_and_sigma_on_area_of_movie_auto, (void));

PXV_FUNC(int, switch_bead_repositionning, (void));
PXV_FUNC(int, count_beads_like_this_one, (void));
PXV_FUNC(int, switch_bead_servo, (void));

PXV_FUNC(int, stop_record_x_y_angle, (void));
PXV_FUNC(int, switch_bead_repositionning, (void));
PXV_FUNC(int, switch_bead_servo, (void));



PXV_FUNC(int, init_track_info, (void));

PXV_VAR(char*, RT_debug_file);
PXV_VAR(int, RT_debug_flag);
PXV_VAR(FILE *, fp_RT_debug);

PXV_VAR(unsigned long, manual_rincing_start);
PXV_VAR(float, rincing_time_ins);
PXV_VAR(int, manual_rincing);



PXV_VAR(unsigned long, manual_enzyme_injection_start);
PXV_VAR(float, enzyme_injection_time_ins);
PXV_VAR(int, manual_enzyme_injection);
PXV_VAR(int, enzyme_nb);


# endif
