#ifndef _SDI_2_FUNCTIONS_H_
#define _SDI_2_FUNCTIONS_H_


#define SDI_NO_WINDOW 0
#define SDI_FLAT_TOP_WINDOW 1
#define SDI_HAMMING_WINDOW  2
#define SDI_HANN_WINDOW 3
#define SDI_BLACKMAN_WINDOW 4
#define SDI_BLACKMAN_HARRIS 5
#define SDI_BLACKMAN_NUTTALL  6
#define SDI_NUTTALL 7
#define SDI_HYPERGAUSSIAN 8

#include "allegro.h"
#include "xvin.h"
#include "xv_main.h"
#include "fftl32n.h"
#include "trackBead.h"
#include "track_util.h"
#include "../fft2d/fftbtl32n.h"
#include "../cfg_file/Pico_cfg.h"
#include "SDI_2_utils.h"


XV_FUNC(int,SDI_2_update_frange_position,(b_track *bt,O_i *oic,int length, int width, sdi_fr *f1));
XV_FUNC(int,SDI_2_project_profile_on_profile,(O_i *oic, int xco, int xend, int yco, int yend, float *xi, float *yi));
XV_FUNC(int, SDI_2_window_profile,(float *x, int lx));
XV_FUNC(int,SDI_2_fill_y_av_x_profile_from_im,(O_i *oi, int xco, int xend, int yco, int yend, float *x, double* fit_param,double *xm));
XV_FUNC(int,SDI_2_fill_x_av_y_profile_from_im,(O_i *oi, int xco, int xend, int yco, int yend, float *y, int *ym,int *yg, int *yd));
XV_FUNC(int, SDI_2_vincent_displacement_function,(float *x ,int nx, int bp_low, int bp_high, double k_av, fft_plan *fp,double *phif, double *k, double *Max_pos, d_s *dsfit, float *xibp, float *xfourier));
XV_FUNC(int ,SDI_2_get_tracking_parameters,( int *ext_window_type, float *ext_mr, float *ext_yratio));
XV_FUNC(int, SDI_2_do_set_tracking_parameters,(void));
XV_FUNC(int, SDI_2_do_choose_window_on_x,(void));
XV_FUNC(int, SDI_2_initialize_phase_fringe, (b_track *bt,sdi_fr *f1));
XV_FUNC(int, SDI_2_initialize_phase,(b_track *bt));
XV_FUNC(int, SDI_2_initialize_k_buffer,(b_track *bt));
XV_FUNC(int, SDI_2_initialize_k_buffer_fringe, (sdi_fr *f1));
XV_FUNC(int, SDI_2_activate_tracking, (void));
XV_FUNC(int, SDI_2_find_new_beads_in_pixel_area,
  (O_i *oi, g_track *gt, int xco, int xend, int y_pixel_low, int  y_pixel_high, float *x_bar_max, float *y_bar_max, float *max_integrated_pix));
XV_FUNC(int, SDI_2_sum_image_on_area_and_send_barycenter,(O_i *oi, int xco, int xend, int yco, int yend, float *x_bar, float *ybar, float *integrated_pix, int only_sum, float *max));
XV_FUNC(d_s*,SDI_2_find_fringes_in_partial_OI,(O_i *oi, int xco, int xend, int yco, int yend, d_s *dsfringes, int threshold, int xw, int yw));
XV_FUNC(d_s*,SDI_2_find_beads_in_partial_OI,(O_i *oi, int xco, int xend, int yco, int yend, int threshold, int xw, int yw, int max_distance_x, int max_distance_y));
XV_FUNC(int, do_find_fringes_in_image,(void));
XV_FUNC(int, SDI_2_do_find_beads_in_image,(void));



MENU *SDI_2_tracking_parameters_menu(void);








#endif
