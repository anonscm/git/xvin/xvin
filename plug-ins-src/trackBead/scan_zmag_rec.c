
#ifndef _SCAN_ZMAG_REC_C_
#define _SCAN_ZMAG_REC_C_

# include "allegro.h"
#ifdef XV_WIN32
# include "winalleg.h"
#endif
# include "xvin.h"

/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "../cfg_file/Pico_cfg.h"
# include "../cardinal/cardinal.h"

#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "../fft2d/fftbtl32n.h"
# include "fillibbt.h"
# include "focus.h"
# include "focus.h"
# include "magnetscontrol.h"
# include "trackBead.h"
# include "brown_util.h"
# include "track_util.h"
# include "scan_zmag_rec.h"
# include "record.h"
# include "force.h"
# include "scan_zmag.h"
# include "action.h"






scan_param *load_Scan_Zmag_param_from_trk(g_record *g_r)
{
  int cfg_size;
  scan_param *S_p = NULL;
  char *cfg = NULL, *st = NULL, *st1 = NULL;
  FILE *fp = NULL;


  if (g_r == NULL || g_r->fullname == NULL) return NULL;
  cfg_size = g_r->header_size - g_r->config_file_position;
  if (cfg_size <= 0)                    return NULL;
  cfg = (char *)calloc(cfg_size,sizeof(char));
  if (cfg == NULL)                    return NULL;
  fp = fopen(g_r->fullname,"rb");
  if (fp == NULL)   
    {
      free(cfg);
      return NULL;
    }
  fseek(fp, 0, SEEK_SET);     
  fseek(fp, g_r->config_file_position, SEEK_SET);     
  if (fread(cfg, sizeof(char),cfg_size,fp) != (size_t)cfg_size)	
    {
      fclose(fp);
      free(cfg);
      return NULL;
    }
  st = strstr(cfg,"[ZScan]");
  if (st == NULL)
    {
      fclose(fp);
      free(cfg);
      return NULL;
    }
  S_p = (scan_param*)calloc(1,sizeof(scan_param));
  if (S_p == NULL)
    {
      fclose(fp);
      free(cfg);
      return NULL;
    }

  st1 = strstr(st,"zstart");
  if (st1 != NULL)
    sscanf(st1,"zstart = %f",&(S_p->zstart));
  st1 = strstr(st,"zstep");
  if (st1 != NULL)
    sscanf(st1,"zstep = %f",&(S_p->zstep));
  st1 = strstr(st,"zmag0");
  if (st1 != NULL)
    sscanf(st1,"zmag0 = %f",&(S_p->zmag0));
  st1 = strstr(st,"lambdazmag");
  if (st1 != NULL)
    sscanf(st1,"lambdazmag = %f",&(S_p->lambdazmag));
  st1 = strstr(st,"zmag_after");
  if (st1 != NULL)
    sscanf(st1,"zmag_after = %f",&(S_p->zlow));
  st1 = strstr(st,"rotation");
  if (st1 != NULL)
    sscanf(st1,"rotation = %f",&(S_p->rotation));
  else	  S_p->rotation = g_r->rot_mag[0][0];


  st1 = strstr(st,"n_step");
  if (st1 != NULL)
    sscanf(st1,"n_step = %d",&(S_p->nstep));
  st1 = strstr(st,"nper");
  if (st1 != NULL)
    sscanf(st1,"nper = %d",&(S_p->nper));
  st1 = strstr(st,"dead");
  if (st1 != NULL)
    sscanf(st1,"dead = %d",&(S_p->dead));
  st1 = strstr(st,"one_way");
  if (st1 != NULL)
    sscanf(st1,"one_way = %d",&(S_p->one_way));
  st1 = strstr(st,"setling");
  if (st1 != NULL)
    sscanf(st1,"setling = %d",&(S_p->setling));
  st1 = strstr(st,"maxnper");
  if (st1 != NULL)
    sscanf(st1,"maxnper = %d",&(S_p->maxnper));

  free(cfg);
  fclose(fp);
  return S_p;
} 

# ifdef ENCOURS

int reload_trk_scan_data_and_force(void)
{
  char *st = NULL, basename[256] = {0}, warning[1024] = {0}, file[1024] = {0}, buf[2048] = {0}, zmags[128] = {0}, path[512] = {0};
  int findex;
  int flow;
  pltreg *pr = NULL;
  O_p *op = NULL, *opn = NULL, *op2 = NULL, *opd = NULL, *opf = NULL, *opzmag = NULL;
  d_s *ds = NULL, *dsx = NULL, *dsy = NULL, *dsz = NULL, *dsn = NULL, *dsm = NULL, *dsk = NULL;
  d_s *dsm2 = NULL, *dsz2 = NULL, *dsf = NULL, *dsfc = NULL, *dsf2 = NULL, *dszmag = NULL, *dszmag2 = NULL;
  int nf, nstep, i, j, k, ki;
  float rots, step, zmag, *n_z, tmp, tmpn, tmp2, tmp2n;
  float fc, a, su, kx, etar, etar2, detar2, dy, dt, da, dfc, chi2, dy0, mean, zmag_m;
  static int	nx = 1024, w_flag = 0, x_or_y = 0, zmag_n;
  static int fm = 1024, fb = 10, cam_fil = 1, frame = 0;
  static float factor = 1, z_offset;
  un_s *un = NULL;
  int  nxo, nx_2, *n_nf;
  int n_warn = 0, found, bd_index = 0, bdfix_index = 0;
  char buf1[256] = {0};
  g_record *g_r = NULL;  
  scan_param *sph = NULL; 

 if(updating_menu_state != 0)	return D_O_K;		
	
  ac_grep(cur_ac_reg,"%pr",&pr);
  g_r = find_g_record_in_pltreg(pr);   
  if(updating_menu_state != 0)	
    {  // we wait for recording finish
      if (g_r == NULL || working_gen_record == g_r) 
	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;	
      return D_O_K;
    }
  if (pr == NULL)return 0;


  if (g_r == NULL) return OFF;
  nf = g_r->abs_pos;
  if (nf <= 0) return OFF;

  sph = load_Scan_Zmag_param_from_trk(g_r);
  if (sph == NULL)
    return win_printf_OK("cannot retrieve scan_zmag info !");

  ops = create_and_attach_one_plot(pr, nw * sph->nstep, nw * sph->nstep, 0);
  dss = ops->dat[0];
  set_ds_source(dss,"Scan_Zmag curve");      


  for (j = 0, im0 = 0; j < dss->nx; j++)
    {
      k = j;
      k %= 2*sph->nstep;
      k = (k < sph->nstep) ? k : 2 * sph->nstep -1 - k;
      zmag_expected = sph->zstart + (k * sph->zstep);
      k = (int)(sph->lambdazmag * (sph->zmag0 - sph->zstart - k * sph->zstep));	
      k = (k <= sph->maxnper) ? k : sph->maxnper;
      nper_expected = (k > 0) ?  sph->nper<<k : sph->nper;
      im0 = retrieve_image_index_of_next_acquisition_point(g_r, im0, nper_expected, &ims, &zmag, &rot);
      if (im0 < 0)
	{
	  win_printf("Pb retrieving scan_zmag point j = %d im0 %d",j,im0);
	  break;
	}
      if (fabs(zmag - zmag_expected) > 0.005)
	win_printf("Pb retrieving data point %d\nZmag found %g zmag expected %g,j,zmag,zmag_expected");
      dss->yd[j] = zmag;
      if (ims - im0 < sph->setling + nper_expected)
	win_printf("Duration Pb  scan_zmag point j = %d im0 %d duration",j,im0,ims - im0);
      if (average_bead_z_during_part_trk(g_r, i, im0+sph->setling, nper_expected, &xavg, &yavg, &zavg) <= 0)
	{
	  win_printf("Pb retrieving scan_zmag avg point");
	  break;
	}
      
      dss->xd[j] = g_r->z_cor * zavg;
      
      if (c_plot)
	{
	  opn = generate_bead_op_during_part_trk(g_r, i, im0+sph->setling, nper_expected);
	  add_data_to_pltreg(pr, IS_ONE_PLOT, (void*)opn);
	}
      
      im0 += sph->setling +nper_expected;
    }


  put_backslash(path);
  snprintf(file,"X(t)Y(t)Z(t)bd%dscan%d.gr",bd_index,findex);
  opn = create_plot_from_gr_file(file, path);
  if (opn == NULL) win_printf_OK("Could not loaded %s\n from %s",
				 backslash_to_slash(file), backslash_to_slash(op->dir));
  add_data_to_pltreg (pr, IS_ONE_PLOT, (void *)opn);
  for (i = 0; i < opn->n_dat; i++)
    {
      dsx = opn->dat[i];
      if (dsx->source == NULL) continue;
      st = strstr(dsx->source,"X coordinate");
      if (st != NULL) break;
    }
  if (st == NULL || dsx == NULL) 
    return win_printf_OK("problem recovering X coordinate");

  for (i = 0; i < opn->n_dat; i++)
    {
      dsy = opn->dat[i];
      if (dsy->source == NULL) continue;
      st = strstr(dsy->source,"Y coordinate");
      if (st != NULL) break;
    }
  if (st == NULL || dsy == NULL) 
    return win_printf_OK("problem recovering Y coordinate");

  for (i = 0; i < opn->n_dat; i++)
    {
      dsz = opn->dat[i];
      if (dsz->source == NULL) continue;
      st = strstr(dsz->source,"Z coordinate");
      if (st != NULL) break;
    }
  if (st == NULL || dsz == NULL) 
    return win_printf_OK("problem recovering Z coordinate");



  i = win_scanf("Indicate the index of the fixed bead %d",&bdfix_index);
  if (i == WIN_CANCEL) return D_O_K;

  snprintf(file,"X(t)Y(t)Z(t)bd%dscan%d.gr",bdfix_index,findex);

  op2 = create_plot_from_gr_file(file, path);
  if (op2 == NULL) win_printf_OK("Could not loaded %s\n from %s",
				 backslash_to_slash(file), backslash_to_slash(op->dir));
  add_data_to_pltreg (pr, IS_ONE_PLOT, (void *)op2);
  for (i = 0; i < op2->n_dat; i++)
    {
      dsz2 = op2->dat[i];
      if (dsz2->source == NULL) continue;
      st = strstr(dsz2->source,"Z coordinate");
      if (st != NULL) break;
    }
  if (st == NULL || dsz2 == NULL) 
    return win_printf_OK("problem recovering Z coordinate");


  snprintf(file,"bddzr%03d.gr",findex);
  opzmag = create_plot_from_gr_file(file, path);
  if (opzmag == NULL) win_printf_OK("Could not loaded %s\n from %s",
				 backslash_to_slash(file), backslash_to_slash(op->dir));
  add_data_to_pltreg (pr, IS_ONE_PLOT, (void *)opzmag);
  dszmag = opzmag->dat[0];




  un = opn->yu[opn->c_yu];
  if (un->type != IS_METER || un->decade != IS_MICRO)
    {
      snprintf(warning+strlen(warning),1024-strlen(warning),"{\\color{Yellow}warning the y unit is not \\mu m !\n"
	      "numerical value will be wrong}\n");
      n_warn++;
    }
  dy = un->dx;
  un = opn->xu[opn->c_xu];
  if (un->type != IS_SECOND || un->decade != 0)
    {
      snprintf(warning+strlen(warning),1024-strlen(warning),"{\\color{Yellow}warning the x unit is not seconds !\n"
	      "numerical value will be wrong}\n");
	    n_warn++;
    }
  dt = un->dx;

  dszmag2 = create_and_attach_one_ds(opzmag, 2*ds->nx, 2*(ds->nx+1), 0);
  if (dszmag2 == NULL) 	 return win_printf("cannot create ds !");
  set_ds_source(dszmag2,"Zmag recover");
  set_plot_symb(dszmag2,"|");
  set_ds_dash_line(dszmag2);

  nf = dszmag->nx;
  n_nf = (int*)calloc(2*(ds->nx),sizeof(int));
  for (i = 0, j = 1; i < ds->nx &&  j < dszmag->nx ; i++)
    {
      for ( ; j < dszmag->nx && fabs(dszmag->yd[j] - dszmag->yd[j-1]) > 0.005 ; j++);
      n_nf[2*i] = j;
      dszmag2->xd[2*i] = dszmag->xd[j];
      dszmag2->yd[2*i] = dszmag->yd[j];
      j+=128;
      for ( ; j < dszmag->nx && fabs(dszmag->yd[j] - dszmag->yd[j-1]) < 0.005 ; j++);
      if (j >= dszmag->nx) j = dszmag->nx-1;
      else j--;
      n_nf[2*i+1] = j;
      nf = (n_nf[2*i+1] - n_nf[2*i] < nf) ? (n_nf[2*i+1] - n_nf[2*i]) : nf;
      dszmag2->xd[2*i+1] = dszmag->xd[j];
      dszmag2->yd[2*i+1] = dszmag->yd[j];
      if (j < dszmag->nx) j++;
    }

  //return win_printf_OK("end of data recovering");




  nxo = nx = nf;
  warning[0] = 0;
  for (nx_2 = 2; nx_2 <= nx; nx_2 *= 2);
  nx_2 /= 2;
  if (nx_2 != nx)
    {
      snprintf(warning+strlen(warning),1024-strlen(warning),
	      "{\\color{Yellow}warning:Your data set size %d is not a power of 2!}\n"
	      "{\\color{Yellow}I am going to analyse only the %d first points}\n",nx,nx_2);
      n_warn++;
      nx = nx_2;
    }
  fm = nx/2;




  i = win_scanf("which horizontal direction\ndo you want to use to\n"
		"measure forece %R X %r Y\n",&x_or_y);

  if (i == WIN_CANCEL) return D_O_K;
  if (x_or_y == 0) dsy = dsx;

  frame = get_config_int("camera", "mode", 0);	
  cam_fil = get_config_int("camera", "correction", 1);	
  factor = get_config_float("camera", "openning", 1);
  w_flag = get_config_int("spectrum", "windowing", 1);
  fb = get_config_int("spectrum", "lower_frequency", 10);

  
  sprintf(buf,"%s\n\n\\frac{f_c [log(1+x^2)]_{fb/fc}^{fm/fc}}"
	  "{2 [arctang(x)]_{fb/fc}^{fm/fc}}\n\nData sets %d fb %%6d fm %%6d nx %%6d\n"
	  "windowing  no%%R yes%%r\ncamera correction no%%R yes%%r\n"
	  "Camera integration mode: field ->%%R frame ->%%r\nopening factor %%6f\n"
	  "Offset to substract to molecule extension %%8f\n"
	  ,warning,ds->nx);
  i = win_scanf(buf,&fb,&fm,&nx,&w_flag,&cam_fil, &frame,&factor,&z_offset);
  if (i == WIN_CANCEL)	
    {
      return 0;
    }


  set_config_int("camera", "mode", frame);	
  set_config_int("camera", "correction", cam_fil);	
  set_config_float("camera", "openning", factor);	
  set_config_int("spectrum", "windowing", w_flag);	
  set_config_int("spectrum", "lower_frequency", fb);



  if (do_create_xvplot_file(NULL) == NULL)	
    {
      return 0;
    }
  log_cardinal++;
  dump_to_current_xvplot_file( "%%index\t{\\delta}x^2\td{\\delta}x^2\tk_x\tdk_x\tf_c(Hz)\tdf_c/f_c"
			      "\t{\\eta}r\t{\\eta}r2\td{\\eta}r2\td<x>\t<x>\tZmag\tRot\tBead_Z\tFix_Z\tZ_offset");



  dsm = create_and_attach_one_ds(op, ds->nx, ds->nx, 0);
  if (dsm == NULL) 	 return win_printf("cannot create ds !");
  set_ds_source(dsm,"Mean Z value for bead");
  //set_plot_symb(dsm,"\\pt5\\oc ");
  //set_ds_dot_line(dsm);

  dsm2 = create_and_attach_one_ds(op, ds->nx, ds->nx, 0);
  if (dsm2 == NULL) 	 return win_printf("cannot create ds !");
  set_ds_source(dsm2,"Mean Z value for fix bead");
  //set_plot_symb(dsm2,"\\pt5\\oc ");
  //set_ds_dot_line(dsm2);



  dsf = create_and_attach_one_ds(op, ds->nx, ds->nx, 0);
  if (dsf == NULL) 	 return win_printf("cannot create ds !");
  set_plot_symb(dsf,"\\pt5\\oc ");
  alloc_data_set_x_error(dsf);
  set_ds_dot_line(dsf);
  set_ds_source(dsf,"Force_{%s} vs Zmag",(x_or_y)?"y":"x");

  dsk = create_and_attach_one_ds(op, ds->nx, ds->nx, 0);
  if (dsk == NULL) 	 return win_printf("cannot create ds !");
  set_plot_symb(dsk,"\\pt5\\di ");
  alloc_data_set_x_error(dsk);
  set_ds_dot_line(dsk);
  set_ds_source(dsk,"10^6 x k_{%s} vs Zmag",(x_or_y)?"y":"x");


  opf = create_and_attach_one_plot(pr,ds->nx,ds->nx, 0);
  if (opf == NULL)	 return win_printf_OK("Cannot allocate plot");
  dsf2 = opf->dat[0];
  if (dsf2 == NULL)	 return D_O_K;
  set_plot_symb(dsf2,"\\pt5\\oc ");
  alloc_data_set_y_error(dsf2);
  set_ds_dot_line(dsf2);
  set_ds_source(dsf2,"Force extension");





  dsfc = create_and_attach_one_ds(opf, ds->nx, ds->nx, 0);
  if (dsfc == NULL) 	 return win_printf("cannot create ds !");
  set_ds_source(dsfc,"Cutoff frequency");
  set_plot_symb(dsfc,"\\pt5\\di ");
  set_ds_dot_line(dsfc);

  set_plot_y_title(opf,"Force & frequency (Hz)");
  set_plot_x_title(opf,"Extension ");
  set_plot_title(opf, "Force curve");

  for (i = 0, opn->cur_dat = opn->n_dat-1, j = 0; i < ds->nx; i++)
    {
      nx = n_nf[2*i+1] - n_nf[2*i];
      for (nx_2 = 2; nx_2 <= nx; nx_2 *= 2);
      nx_2 /= 2;
      if (nx_2 != nx)
	nx = nx_2;
      //if (nx > 32768) nx = 32768;
      fm = nx/2;
      sprintf(buf1,"Treating step %d",i);
      set_window_title(buf1);
      dsn = create_and_attach_one_ds(opn, nx, nx, 0);
      if (dsn == NULL) 	 return win_printf("cannot create ds !");
      grey = 224-(i*192)/(ds->nx);
      set_ds_line_color(dsn, makecol(grey,grey,grey));
      set_ds_dot_line(dsn);
      ki = (n_nf[2*i+1] + n_nf[2*i])/2;
      ki -= nx/2;
      zmag_n = 0;
      zmag_m = 0;

      //win_printf("i %d nx %d zmag %g ki %d ke %d max %d",i,nx,ds->yd[i],ki,ki+nx,dszmag->nx);
      for(k = 0; k < nx && ki < dsy->nx && ki < dsz->nx && ki < dsz2->nx; k++, ki++)
	{
	  dsn->xd[k] = dsy->xd[ki];
	  dsn->yd[k] = dsy->yd[ki];
	  dsm->xd[i] += dsz->yd[ki];
	  dsm2->xd[i] += dsz2->yd[ki];
	  zmag_m += dszmag->yd[ki];
	  zmag_n++;
	}
      if (ki >= dsy->nx || ki >= dsz->nx || ki >= dsz2->nx)
	win_printf("You have reach plot limits");
      //win_printf("i %d after garbing data",i);
      if (zmag_n) zmag_m /= zmag_n;
      dsm->xd[i] /= nx;
      dsm->xd[i] *= dy  * 0.878;
      dsm->yd[i] = zmag_m; //ds->yd[i];
      dsm2->xd[i] /= nx;
      dsm2->xd[i] *= dy * 0.878;
      dsm2->yd[i] = zmag_m; //ds->yd[i];
      dsk->yd[i] = dsf->yd[i] = zmag_m; //ds->yd[i];
      //dsf2->xd[i] = ds->xd[i] - z_offset;
      //dsfc->xd[i] = ds->xd[i] - z_offset;




      set_ds_source(dsn,"Partial trajectory at Zmag = %g Rot %g fileindex %d basename %s",
		    zmag_m,rots,findex,basename);


      for (k = 0, mean = 0; k < dsn->nx; k++)
	mean += dsn->yd[k];
      mean /= dsn->nx;
      mean *= dy;
      dsn->nx = dsn->ny = nx; 
      //win_printf("i %d nx %d zmag %g ki %d ke %d max %d\nzm %g zm2 %g",
      //i,nx,ds->yd[i],ki,ki+nx,dszmag->nx,dsm->xd[i],dsm2->xd[i]);
      //win_printf("before alternate");
      alternate_lorentian_fit_acq_spe_with_error(dsn->yd, nx, dy, fb, fm, 
		w_flag, cam_fil, factor, frame, &fc, &a, &dfc, &da, &chi2, &dy0);
      //win_printf("after alternate");
      su = a * M_PI_2;
      kx = (1.38e-11 * 298)/su;
      etar = kx*nx*dt/(12*M_PI*M_PI*fc);
      flow = (int)(2*fc);
      flow = (flow > nx/4) ? nx/4 : flow;
      flow = (flow < 2*fb) ? 2*fb : flow;
      opn->cur_dat = opn->n_dat-1;
      //win_printf("before derivate");

      opd = do_fft_and_derivate_on_op_2(opn, flow, fm, w_flag+(cam_fil<<1), fc, 
		frame, factor, &etar2, &detar2);
      //win_printf("after derivate");

      if (opd != NULL) free_one_plot(opd);
      //win_printf("after free one plot");
      detar2 = (etar2 != 0) ? detar2/etar2 : detar2;

      sprintf(zmags,"Zmag = %g Rot = %g\n",zmag_m,rots);

      dump_to_current_xvplot_file( "%d\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g",
				   findex,su,(a!=0)?su*da/a:su,kx,(a!=0)?kx*da/a:kx,fc/(nx*dt),
				   (fc!=0)?100*dfc/fc:dfc,etar,etar2,etar2*detar2,dy0,mean,zmag_m,rots,
				   dsm->xd[i],dsm2->xd[i],z_offset);
      //win_printf("after dump");
      dsfc->yd[i] = fc/(nx*dt);

      //win_printf("after dump 2");
      dsf2->yd[i] = dsf->xd[i] = kx * 1000000 * ((dsm->xd[i] - dsm2->xd[i]) - z_offset);
      dsf2->ye[i] = dsf->xe[i] = (a!=0)?dsf->xd[i]*da/a:0;
      dsk->xd[i] = kx * 1000000;
      //win_printf("after dump 3");
      dsk->xe[i] = (a!=0)?dsk->xd[i]*da/a:0;
      dsf2->xd[i] = (dsm->xd[i] - dsm2->xd[i]) - z_offset;
      dsfc->xd[i] = (dsm->xd[i] - dsm2->xd[i]) - z_offset;
      //win_printf("i %d nx %d zmag %g kx %g",i,nx,ds->yd[i],kx);
    }
  //free_one_plot(opn);
  //free_one_plot(op2);
  opn->need_to_refresh = 1;
  refresh_plot(pr, UNCHANGED);		
  return 0;
}


# endif

int redraw_scan_zmag_from_trk(void)
{
  int  i, prev, j, page_n, i_page, k, i_er;
  //b_track *bt = NULL;
  pltreg *pr = NULL;
  O_p *op = NULL, *ops = NULL, *opn = NULL, *opf = NULL, *opxy = NULL;
  d_s *ds = NULL, *dss = NULL, *dsf = NULL, *dsx = NULL, *dsy = NULL;
  static int c_plot = 0, one_bead = 0, bead_nb = 0, no_kx = 1, xy_vs_z = 0;
  int nf, ims = 0, im0 = 0, ima = 0,  nw, nper_expected, profile_invalid = 0, n_avg,  first, no_more = D_O_K, n_maxnper = 0, n_acq, li, nstep = 0, tmpi; // , sc_in_found = 0
  float  zmag = 0, rot = 0, xavg, yavg, zavg, kxl, dkx, fc, dfc, zmag_tmp = 0;// , zmag_expected
  //b_record *br = NULL;
  g_record *g_r = NULL;  
  b_record *b_r = NULL;  
  scan_param *sph = NULL; 
  stiffness_resu *sr = NULL;
  float b_radius = 0;
  char mes[256] = {0};
  scan_info *sc_in = NULL;
  
  ac_grep(cur_ac_reg,"%pr",&pr);
  g_r = find_g_record_in_pltreg(pr);   
  if(updating_menu_state != 0)	
    {  // we wait for recording finish
      if (g_r == NULL || working_gen_record == g_r) 
	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;	
      return D_O_K;
    }
  if (pr == NULL)return 0;
  if (g_r == NULL) return OFF;
  nf = g_r->abs_pos;
  if (nf <= 0) return OFF;

  sph = load_Scan_Zmag_param_from_trk(g_r);
  if (sph == NULL)    return win_printf_OK("cannot retrieve scan_zmag info !");


  for (j = 0, prev = 1; j < nf; j++)
    {
      page_n = j/g_r->page_size;
      i_page = j%g_r->page_size;
      ims = g_r->imi[page_n][i_page];
      if (g_r->status_flag[page_n][i_page] == 0 && prev != 0)
	{
	  im0 = j;
	  zmag = g_r->zmag[page_n][i_page];
	  rot = g_r->rot_mag[page_n][i_page];
	}
      if (g_r->status_flag[page_n][i_page] != 0 && prev == 0)
	{
	  ims = j;
	  if (ims - im0 > sph->nper) break;
	}
      prev = g_r->status_flag[page_n][i_page];
    }

  /* win_printf("Scan_Zmag %d points way %d setling %d nper %d\n" */
  /* 	     "step %g zmag start %g zmag_finish %g\n " */
  /* 	     "zmag0 %g lamdbdazmag %g maxnper %d\n" */
  /* 	     "im0 = %d ims = %d zmag %g rot %g", */
  /* 	     sph->nstep,sph->one_way,sph->setling,sph->nper, */
  /* 	     sph->zstep,sph->zstart,sph->zlow, */
  /* 	     sph->zmag0, sph->lambdazmag, sph->maxnper, */
  /* 	     im0, ims, zmag, rot); */

  im0 = retrieve_image_index_of_next_acquisition_point(g_r, 0, sph->nper+sph->setling, &ims, &zmag, &rot);

  //win_printf("im0 = %d ims = %d zmag %g rot %g",
  //     im0, ims, zmag, rot);

  i = win_scanf("Do you want to create plot \n"
		"from acquisition point No %R yes %r\n"
		"if yes, do you want to compute stiffness oui %R no %r \n"
		"For all beads %R or only one %r\n"
		"In that last case specify which bead %d\n"
		"Do you want <X>/<Y> versus <Z> %b\n"
		,&c_plot,&no_kx,&one_bead,&bead_nb,&xy_vs_z);
  if (i == WIN_CANCEL) return 0;

  nw = (sph->one_way) ? 1 : 2;

  n_acq = nw * sph->nstep;
  // we grab info from the trk averaging signals
  sc_in = retrieve_scan_info(g_r, &nstep, WIN_CANCEL);

  if (sc_in == NULL || nstep < 1)
    {
       //sc_in_found = 0;  // we fill scan info from expected data 
       sc_in = (scan_info*)calloc(n_acq,sizeof(scan_info));
       if (sc_in == NULL) return win_printf_OK("Cannot allocate scan info!");

       for (i = 0, li = 0; i < n_acq ; i++)
	 {
	   j = i;
	   j %= 2*sph->nstep;
	   j = (j < sph->nstep) ? j : 2 * sph->nstep -1 - j;
	   zmag_tmp = sph->zstart + (j * sph->zstep);
	   for (k = 0; k < 1; k++)
	     {
	       j = (int)(sph->lambdazmag * (sph->zmag0 - sph->zstart - j * sph->zstep));	
	       j = (j <= n_maxnper) ? j : n_maxnper;
	       tmpi = (j > 0) ?  sph->nper<<j : sph->nper;
	       tmpi = (tmpi < sph->maxnper) ? tmpi : sph->maxnper;
	       nf += tmpi;
	       nf += sph->dead;
	       sc_in[li].zmag = zmag_tmp;
	       sc_in[li].im_n = tmpi;
	       sc_in[li].i_zm = i;
	       sc_in[li].is_zm = 0;
	       sc_in[li].nis_zm = 1;
	       li++;
	     }
	 }
     }
   //else sc_in_found = 1;
   if (n_acq < 1) win_printf("nstep %d nw %d",nw, sph->nstep);
   //win_printf("nstep %d nw %d nexpo %d, n_acq %d\n zmag expo %f", sph->nstep, nw, sph->n_expo_div,n_acq,sph->zmag_hi_fc);

   if (sc_in != NULL && nstep > 0) n_acq = nstep; 
   // to be done check that both info agree



  pr = create_pltreg_with_op(&ops, n_acq, n_acq, 0);
  //ops = create_and_attach_one_plot(pr, nw * sph->nstep, nw * sph->nstep, 0);
  dss = ops->dat[0];
  dss->nx = dss->ny = 0;       
  set_ds_source(dss,"Scan_Zmag curve");      
  set_dot_line(dss);
  set_plot_symb(dss, "\\pt5\\oc");
  set_plot_title(ops, "Zmag /extension (Zsacn curve %d)", g_r->n_rec);
  set_plot_x_title(ops,"Extension (\\mu m)");
  set_plot_y_title(ops,"Zmag (mm)");
  set_plot_file(ops,"Zmag-exbd%01d%03d%s",0,g_r->n_rec,".gr");



  opf = create_and_attach_one_plot(pr, nw * n_acq, n_acq, 0);
  dsf = opf->dat[0];
  dsf->nx = dsf->ny = 0;       
  set_dot_line(dsf);
  set_plot_symb(dsf, "\\pt5\\oc");
  set_plot_title(opf, "Estimated Force from Z scan %d ", g_r->n_rec);
  set_plot_y_title(opf,"Estimated force (pN)");
  set_plot_x_title(opf,"Extension (\\mu m)");
  set_plot_file(opf,"ForcevsEstibd%01d%03d%s",i,g_r->n_rec,".gr");


  if (xy_vs_z)
    {
      opxy = create_and_attach_one_plot(pr, n_acq, n_acq, 0);
      dsx = opxy->dat[0];
      dsx->nx = dsx->ny = 0;       
      set_dot_line(dsx);
      set_plot_symb(dsx, "\\pt5\\oc");
      dsy = create_and_attach_one_ds(opxy, n_acq, n_acq, 0);
      dsy->nx = dsy->ny = 0;       
      set_dot_line(dsy);
      set_plot_symb(dsy, "\\pt5\\oc");
      set_plot_title(opxy, "<x>/<y> vs <z> Z scan %d ", g_r->n_rec);
      set_plot_x_title(opxy,"<x>, <y>");
      set_plot_y_title(opxy,"Extension (\\mu m)");
      set_plot_file(opxy,"ForcevsEstibd%01d%03d%s",i,g_r->n_rec,".gr");
    }
  

  //  for (j = 1; j < g_r->n_bead; j++)
  //  dss = create_and_attach_one_ds(ops, nw * sph->nstep, nw * sph->nstep, 0);
  b_radius = g_r->Pico_param_record.bead_param.radius;
  for (i = 0, first = 1; i < g_r->n_bead; i++)
    {
      if (one_bead != 0 && bead_nb != i) continue;
      if (g_r->SDI_mode == 0 && g_r->b_r[i]->calib_im == NULL) continue;
      if (first == 0)
	{
	  dss = create_and_attach_one_ds(ops, n_acq, n_acq, 0);
	  set_plot_symb(dss, "\\pt5\\oc");
	  dss->nx = dss->ny = 0;       
	  set_dot_line(dss);
	  dsf = create_and_attach_one_ds(opf, n_acq, n_acq, 0);
	  set_plot_symb(dsf, "\\pt5\\oc");
	  dsf->nx = dsf->ny = 0;       
	  set_dot_line(dsf);
	  if (xy_vs_z && opxy != NULL)
	    {
	      dsx = create_and_attach_one_ds(opxy, n_acq, n_acq, 0);
	      dsx->nx = dsx->ny = 0;       
	      set_dot_line(dsx);
	      set_plot_symb(dsx, "\\pt5\\oc");
	      dsy = create_and_attach_one_ds(opxy, n_acq, n_acq, 0);
	      dsy->nx = dsy->ny = 0;       
	      set_dot_line(dsy);
	      set_plot_symb(dsy, "\\pt5\\oc");
	    }
	}
      else
	{
	  snprintf(mes,sizeof(mes),"Scan Z curve bead %d",i);
	  change_menu_name_of_current_project(mes);
	}
      set_ds_source(dss,"Zmag versus extension \nZ mag start = %g for %d frames force curve by steps of %g"
		    " for %d steps %d way(s)\nrot = %g dead period %d\n"
		    "Bead nb %d force curve %s mol\n"
		    ,sph->zstart,sph->nper,sph->zstep,sph->nstep,nw,g_r->rot_mag[0][0]
		    ,sph->dead,i,g_r->filename);
      set_ds_source(dsf,"estimated force versus extension \nZ mag start = %g for %d frames force curve by steps of %g"
		    " for %d steps %d way(s)\nrot = %g dead period %d\n"
		    "Bead nb %d force curve %s mol\n"
		    ,sph->zstart,sph->nper,sph->zstep,sph->nstep,nw,g_r->rot_mag[0][0]
		    ,sph->dead,i,g_r->filename);
      if (xy_vs_z && dsx != NULL && dsy != NULL)
	{
	  set_ds_source(dsx,"<x> versus extension \nZ mag start = %g for %d frames force curve by steps of %g"
			" for %d steps %d way(s)\nrot = %g dead period %d\n"
			"Bead nb %d force curve %s mol\n"
			,sph->zstart,sph->nper,sph->zstep,sph->nstep,nw,g_r->rot_mag[0][0]
			,sph->dead,i,g_r->filename);
	  set_ds_source(dsy,"<y> versus extension \nZ mag start = %g for %d frames force curve by steps of %g"
			" for %d steps %d way(s)\nrot = %g dead period %d\n"
			"Bead nb %d force curve %s mol\n"
			,sph->zstart,sph->nper,sph->zstep,sph->nstep,nw,g_r->rot_mag[0][0]
			,sph->dead,i,g_r->filename);
	}

      b_r = g_r->b_r[i];
      for (n_maxnper = 0; (sph->nper*(1<<n_maxnper)) < sph->maxnper; n_maxnper++);
      //#ifdef WASBEFORE
      for (j = 0, im0 = 0; j < dss->mx && im0 < nf; j++)
	{
	  k = j;
	  k %= 2*sph->nstep;
	  k = (k < sph->nstep) ? k : 2 * sph->nstep -1 - k;
	  //zmag_expected = sph->zstart + (k * sph->zstep);
	  k = (int)(sph->lambdazmag * (sph->zmag0 - sph->zstart - k * sph->zstep));	
	  k = (k <= n_maxnper) ? k : n_maxnper;
	  nper_expected = (k > 0) ?  sph->nper<<k : sph->nper;
	  nper_expected = (nper_expected < sph->maxnper) ? nper_expected : sph->maxnper;
	  //ret = retrieve_image_index_of_next_acquisition_point(g_r, im0, nper_expected, &ims, &zmag, &rot);
	  /*
	  ret = retrieve_image_index_of_acquisition_point(g_r, j, im0, &ims, &ima, &ime, &zmag, &rot, NULL);
	  if ((ret < 0  || ima < 0) && no_more != WIN_CANCEL)
	    {
	      if (first) 
		no_more = win_printf("Pb retrieving scan_zmag point j = %d im0 %d, ret %d ims %d\n"
				    "To skip further warning press WIN_CANCEL",j,im0,ret, ims);
	      break;
	    }
	  if (fabs(zmag - zmag_expected) > 0.005 && first && no_more != WIN_CANCEL)
	    no_more = win_printf("Pb retrieving data point %d\n"
		       "Zmag found %g zmag expected %g,j,zmag,zmag_expected\n"
				 "To skip further warning press WIN_CANCEL",j,zmag,zmag_expected);


	  n_avg = nper_expected;

	  //if (ims - im0 < sph->setling + nper_expected)
	  //  {
	  //    win_printf("Duration Pb  scan_zmag point j = %d im0 %d duration\n"
	  //		 "while setling = %d averaging size %d",j,im0,ims - im0,sph->setling, nper_expected);
	  //    n_avg = ims - im0 - sph->setling;
	  //  }
	  if ((ime - ima < nper_expected && first) && (no_more != WIN_CANCEL))
	    {
	      no_more = win_printf("Duration Pb  scan_zmag point j = %d ima %d duration %d\n"
			 "while averaging size %d\nTo skip further warning press WIN_CANCEL"
				   ,j,ima,ime - ima, nper_expected);
	      n_avg = ime - ima;
	    }
	  //win_printf("For bead %d data retrieved for point %d start %d end %d zmag %g",i,j,im0,ims,zmag);
	  if (average_bead_z_during_part_trk(g_r, i, ima, n_avg, &xavg, &yavg, &zavg, NULL, 
					     &profile_invalid) <= 0 && first && no_more != WIN_CANCEL)

	  */

	  // new
	  zmag = sc_in[j].zmag;
	  rot = sc_in[j].rot;
	  im0 = ima = sc_in[j].ims;
	  n_avg = sc_in[j].im_n;
	  //win_printf("For bead %d data retrieved for point %d start %d end %d zmag %g",i,j,im0,ims,zmag);
	  i_er = average_bead_z_during_part_trk_tolerant(g_r, i, ima, n_avg, 1, &xavg, &yavg, &zavg, NULL, &profile_invalid);
	  //win_printf("found %d points zavg = %g",i_er,zavg);

	  if (b_r->s_r == NULL)  
	    {
	      b_r->s_r = (stiffness_resu*)calloc(nw*sph->nstep,sizeof(stiffness_resu));
	      if (b_r->s_r == NULL)  
		return win_printf_OK("Could allocate space for stiffness result!");
	      b_r->n_s_r = b_r->m_s_r = nw*sph->nstep;
	      b_r->c_s_r = 0;
	    }
	  
	  

	  if (i_er  == -5)		  b_r->s_r[j].error_message = strdup("Z Tracking excessive noise for all acquisition!");
	  else if (i_er  <= 0 && first && no_more != WIN_CANCEL) no_more = win_printf("Pb retrieving force avg point %d err code %d",j,i_er);
	  if (profile_invalid && first && no_more != WIN_CANCEL)    no_more = win_printf("Pb invalid profiles in force avg point %d",j);
	  b_r->s_r[j].zavg = g_r->z_cor * zavg;
	  // end new

	  if (profile_invalid && first && no_more != WIN_CANCEL)
	    no_more = win_printf("Pb invalid profiles in scan_zmag avg point %d\n"
				 "To skip further warning press CANCEL",j);
	  
	  add_new_point_to_ds(dss, g_r->z_cor * zavg, zmag);
	  if (xy_vs_z)
	    {
	      add_new_point_to_ds(dsx, xavg , g_r->z_cor * zavg);
	      add_new_point_to_ds(dsy, yavg , g_r->z_cor * zavg);
	    }
	  add_new_point_to_ds(dsf, g_r->z_cor * zavg, trk_zmag_to_force(0,zmag, g_r));
	  if (n_avg > 0)
	    {
	      opn = generate_bead_op_during_part_trk(g_r, i, j, ima, n_avg);
	      if (c_plot) add_data_to_pltreg(pr, IS_ONE_PLOT, (void*)opn);
	      if (no_kx == 0)
		{
		  if (b_r->s_r == NULL)  
		    {
		      b_r->s_r = (stiffness_resu*)calloc(nw*sph->nstep,sizeof(stiffness_resu));
		      if (b_r->s_r == NULL)  
			return win_printf_OK("Could allocate space for stiffness result!");
		      b_r->n_s_r = b_r->m_s_r = nw*sph->nstep;
		      b_r->c_s_r = 0;
		    }

		  if (j < b_r->n_s_r)
		    {
		      sr = b_r->s_r + j;
		      mean_y2_in_microns(opn, opn->dat[0], 1, &sr->mxm, &sr->sx2, &sr->sx4);
		      if (sr->sx2 > MIN_DX2 && opn->n_dat > 2)
			{
			  delta_de_fc_4_auto(opn, opn->dat[0], 10, 0.5, 1, 1, 0, 0.97, b_radius, &sr->nxeff, &sr->ax, &sr->dax, &sr->kx,         
					     &sr->fcx, &sr->dfcx, &sr->etarx, &sr->etar2x, &sr->detar2x, &sr->dmxm);
			}
		      mean_y2_in_microns(opn, opn->dat[1], 1, &sr->mym, &sr->sy2, &sr->sy4);
		      if (sr->sy2 > MIN_DX2 && opn->n_dat > 2)
			{
			  delta_de_fc_4_auto(opn, opn->dat[1], 10, 0.5, 1, 1, 0, 0.97, b_radius, &sr->nyeff, &sr->ay, &sr->day, &sr->ky,
					     &sr->fcy, &sr->dfcy, &sr->etary, &sr->etar2y, &sr->detar2y, &sr->dmym);
			}
		      mean_y2_in_microns(opn, opn->dat[2], 1, &sr->mzm, &sr->sz2, &sr->sz4);
		      if (sr->sz2 > MIN_DX2 && opn->n_dat > 2 && profile_invalid == 0)
			{
			  delta_de_fc_4_auto(opn, opn->dat[2], 10, 0.5, 1, 1, 0, 0.97, b_radius, &sr->nzeff, &sr->az, &sr->daz, &sr->kz,
					      &sr->fcz, &sr->dfcz, &sr->etarz, &sr->etar2z, &sr->detar2z, &sr->dmzm);
			}
		      sr->zmag = zmag;
		      sr->rot = rot;
		      b_r->c_s_r = j;
		      my_set_window_title("Treating Bead %d acquisition %d with %d points",i,j,opn->dat[0]->nx);
		    }		  
		}
	      if (c_plot == 0) 
		{
		  free_one_plot(opn);
		  opn = NULL;
		}
	    }

	  im0 += sph->setling +n_avg;
	}

      if ((no_kx == 0) && (b_r->s_r != NULL))
	{
	  op = create_and_attach_one_plot(pr, nw * sph->nstep, nw * sph->nstep, 0);
	  ds = op->dat[0];
	  ds->nx = ds->ny = 0;       
	  set_dot_line(ds);
	  set_plot_symb(ds, "\\pt5\\oc");
	  set_plot_title(op, "Beads %d Stiffness scan %d", i, g_r->n_rec);
	  set_plot_x_title(op,"Acquisition Nb.");
	  set_plot_y_title(op,"K_x and K_y (10^{-6}N/m)");
	  set_plot_file(op,"KxKySzbd%01d%03d%s",i,g_r->n_rec,".gr");
	  
	  set_ds_source(ds,"k_x Stiffnes\nZ mag start = %g for %d frames scan by steps of %g"
			" for %d steps %d way(s)\nrot = %g dead period %d\n"
			"Bead nb %d scan %s \n"
			,sph->zstart,sph->nper,sph->zstep,sph->nstep,nw,g_r->rot_mag[0][0]
			,sph->dead,i,g_r->filename);
	  for (j = 0; (j < nw * sph->nstep)  && (j < b_r->n_s_r); j++)
	    {
	      //win_printf("k pt %d",j);
	      kxl = b_r->s_r[j].kx*1e6;
	      dkx = b_r->s_r[j].dax;
	      dkx = (b_r->s_r[j].ax > 0) ? dkx/b_r->s_r[j].ax : 0;
	      dkx *= kxl;
	      add_new_point_with_y_error_to_ds(ds, ds->nx, kxl, dkx );
	    }
	  //win_printf("end k pt %d",j);
	  
	  if ((ds = create_and_attach_one_ds(op, nw * sph->nstep,  nw * sph->nstep, 0)) == NULL)
	    return win_printf_OK("I can't create plot !");
	  ds->nx = ds->ny = 0;
	  set_dot_line(ds);
	  set_plot_symb(ds, "\\pt5\\oc");
	  set_ds_source(ds,"k_y Stiffnes\nZ mag start = %g for %d frames scan by steps of %g"
			" for %d steps %d way(s)\nrot = %g dead period %d\n"
			"Bead nb %d scan %s \n"
			,sph->zstart,sph->nper,sph->zstep,sph->nstep,nw,g_r->rot_mag[0][0]
			,sph->dead,i,g_r->filename);
	  for (j = 0; (j < nw * sph->nstep) && (j < b_r->n_s_r); j++)
	    {
	      kxl = b_r->s_r[j].ky*1e6;
	      dkx = b_r->s_r[j].day;
	      dkx = (b_r->s_r[j].ay > 0) ? dkx/b_r->s_r[j].ay : 0;
	      dkx *= kxl;
	      add_new_point_with_y_error_to_ds(ds, ds->nx, kxl, dkx );

	    }

	  op = create_and_attach_one_plot(pr, nw * sph->nstep, nw * sph->nstep, 0);
	  ds = op->dat[0];
	  ds->nx = ds->ny = 0;       
	  set_dot_line(ds);
	  set_plot_symb(ds, "\\pt5\\oc");
	  set_plot_title(op, "Beads %d frequency cutoff scan %d", i, g_r->n_rec);
	  set_plot_x_title(op,"Acquisition Nb.");
	  set_plot_y_title(op,"f_c (Hz)");
	  set_plot_file(op,"fcSzbd%01d%03d%s",i,g_r->n_rec,".gr");
	  
	  set_ds_source(ds,"f_{cx} cutoof requency\nZ mag start = %g for %d frames scan by steps of %g"
			" for %d steps %d way(s)\nrot = %g dead period %d\n"
			"Bead nb %d scan %s \n"
			,sph->zstart,sph->nper,sph->zstep,sph->nstep,nw,g_r->rot_mag[0][0]
			,sph->dead,i,g_r->filename);
	  for (j = 0; (j < nw * sph->nstep) && j < b_r->n_s_r; j++)
	    {
	      fc = b_r->s_r[j].fcx;
	      if (b_r->s_r[j].nxeff) fc /= b_r->s_r[j].nxeff;
	      fc *= g_r->Pico_param_record.camera_param.camera_frequency_in_Hz;
	      if (fc > g_r->Pico_param_record.camera_param.camera_frequency_in_Hz)
		fc = g_r->Pico_param_record.camera_param.camera_frequency_in_Hz;
	      dfc = b_r->s_r[j].dfcx;
	      if (b_r->s_r[j].nxeff) dfc /= b_r->s_r[j].nxeff;
	      dfc *= g_r->Pico_param_record.camera_param.camera_frequency_in_Hz;
	      if (dfc > g_r->Pico_param_record.camera_param.camera_frequency_in_Hz)
		dfc = g_r->Pico_param_record.camera_param.camera_frequency_in_Hz;
	      add_new_point_with_y_error_to_ds(ds, ds->nx, fc, dfc );
	    }

	  
	  if ((ds = create_and_attach_one_ds(op, nw * sph->nstep,  nw * sph->nstep, 0)) == NULL)
	    return win_printf_OK("I can't create plot !");
	  ds->nx = ds->ny = 0;
	  set_dot_line(ds);
	  set_plot_symb(ds, "\\pt5\\oc");
	  set_ds_source(ds,"f_{cy} cutoff frequency\nZ mag start = %g for %d frames scan by steps of %g"
			" for %d steps %d way(s)\nrot = %g dead period %d\n"
			"Bead nb %d scan %s \n"
			,sph->zstart,sph->nper,sph->zstep,sph->nstep,nw,g_r->rot_mag[0][0]
			,sph->dead,i,g_r->filename);
	  for (j = 0;(j < nw * sph->nstep) && (j < b_r->n_s_r); j++)
	    {
	      fc = b_r->s_r[j].fcy;
	      if (b_r->s_r[j].nxeff) fc /= b_r->s_r[j].nxeff;
	      fc *= g_r->Pico_param_record.camera_param.camera_frequency_in_Hz;
	      if (fc > g_r->Pico_param_record.camera_param.camera_frequency_in_Hz)
		fc = g_r->Pico_param_record.camera_param.camera_frequency_in_Hz;
	      dfc = b_r->s_r[j].dfcy;
	      if (b_r->s_r[j].nxeff) dfc /= b_r->s_r[j].nxeff;
	      dfc *= g_r->Pico_param_record.camera_param.camera_frequency_in_Hz;
	      if (dfc > g_r->Pico_param_record.camera_param.camera_frequency_in_Hz)
		dfc = g_r->Pico_param_record.camera_param.camera_frequency_in_Hz;
	      add_new_point_with_y_error_to_ds(ds, ds->nx, fc, dfc );
	    }

	  op = create_and_attach_one_plot(pr, nw * sph->nstep, nw * sph->nstep, 0);
	  ds = op->dat[0];
	  ds->nx = ds->ny = 0;       
	  set_dot_line(ds);
	  set_plot_symb(ds, "\\pt5\\oc");
	  set_plot_title(op, "Beads %d extension scan %d", i, g_r->n_rec);
	  set_plot_x_title(op,"Acquisition Nb.");
	  set_plot_y_title(op,"Extension (\\mu m)");
	  set_plot_file(op,"LengthSzbd%01d%03d%s",i,g_r->n_rec,".gr");
	  
	  set_ds_source(ds,"Extension\nZ mag start = %g for %d frames scan by steps of %g"
			" for %d steps %d way(s)\nrot = %g dead period %d\n"
			"Bead nb %d scan %s \n"
			,sph->zstart,sph->nper,sph->zstep,sph->nstep,nw,g_r->rot_mag[0][0]
			,sph->dead,i,g_r->filename);
	  for (j = 0; (j < nw * sph->nstep) && (j < b_r->n_s_r); j++)
	      add_new_point_with_y_error_to_ds(ds, ds->nx, b_r->s_r[j].mzm, b_r->s_r[j].dmzm);



	  op = create_and_attach_one_plot(pr, nw * sph->nstep, nw * sph->nstep, 0);
	  ds = op->dat[0];
	  ds->nx = ds->ny = 0;       
	  set_dot_line(ds);
	  set_plot_symb(ds, "\\pt5\\oc");
	  set_plot_title(op, "Force curve Beads %d scan %d", i, g_r->n_rec);
	  set_plot_x_title(op,"Extension (\\mu m)");
	  set_plot_y_title(op,"Force (pN)");
	  set_plot_file(op,"Forcebd%01d%03d%s",i,g_r->n_rec,".gr");
	  
	  set_ds_source(ds,"F_x Force\nZ mag start = %g for %d frames scan by steps of %g"
			" for %d steps %d way(s)\nrot = %g dead period %d\n"
			"Bead nb %d scan %s \n"
			,sph->zstart,sph->nper,sph->zstep,sph->nstep,nw,g_r->rot_mag[0][0]
			,sph->dead,i,g_r->filename);
	  for (j = 0; (j < nw * sph->nstep) && (j < b_r->n_s_r); j++)
	    {
	      kxl = b_r->s_r[j].kx*1e6;
	      dkx = b_r->s_r[j].dax;
	      dkx = (b_r->s_r[j].ax > 0) ? dkx/b_r->s_r[j].ax : 0;
	      dkx *= kxl;
	      kxl *= b_r->s_r[j].mzm;
	      dkx *= b_r->s_r[j].mzm;
	      add_new_point_with_xy_error_to_ds(ds, b_r->s_r[j].mzm, b_r->s_r[j].dmzm, kxl, dkx);
	    }

	  
	  if ((ds = create_and_attach_one_ds(op, nw * sph->nstep,  nw * sph->nstep, 0)) == NULL)
	    return win_printf_OK("I can't create plot !");
	  ds->nx = ds->ny = 0;
	  set_dot_line(ds);
	  set_plot_symb(ds, "\\pt5\\oc");
	  set_ds_source(ds,"F_y force\nZ mag start = %g for %d frames scan by steps of %g"
			" for %d steps %d way(s)\nrot = %g dead period %d\n"
			"Bead nb %d scan %s \n"
			,sph->zstart,sph->nper,sph->zstep,sph->nstep,nw,g_r->rot_mag[0][0]
			,sph->dead,i,g_r->filename);
	  for (j = 0; (j < nw * sph->nstep) && (j < b_r->n_s_r); j++)
	    {
	      kxl = b_r->s_r[j].ky*1e6;
	      dkx = b_r->s_r[j].day;
	      dkx = (b_r->s_r[j].ay > 0) ? dkx/b_r->s_r[j].ay : 0;
	      dkx *= kxl;
	      kxl *= b_r->s_r[j].mzm;
	      dkx *= b_r->s_r[j].mzm;
	      add_new_point_with_xy_error_to_ds(ds, b_r->s_r[j].mzm, b_r->s_r[j].dmzm, kxl, dkx);
	    }
	}
      first = 0;
    }

  /*
  ops = create_and_attach_one_plot(pr, nf, nf, 0);
  dss = ops->dat[0];
  set_ds_source(dss,"Status flag");      

  for (j = 0; j < nf; j++)
    {
      page_n = j/g_r->page_size;
      i_page = j%g_r->page_size;
      dss->xd[j] = (g_r->timing_mode == 1) ? (g_r->imi[page_n][i_page] - g_r->imi_start) 
	: g_r->imi[page_n][i_page];
      dss->yd[j] = g_r->status_flag[page_n][i_page];
    }

  create_attach_select_x_un_to_op(ops, IS_SECOND, 0
				  ,(float)1/g_r->Pico_param_record.camera_param.camera_frequency_in_Hz, 0, 0, "s");
      
      
  set_op_filename(ops, "Status-%d.gr",g_r->n_rec);
  set_plot_title(ops, "Scan_Zmag %d status ", g_r->n_rec);
  set_plot_x_title(ops, "Time");
  set_plot_y_title(ops, "Status");			
      
  ops->need_to_refresh = 1;


  op = create_and_attach_one_plot(pr, nf, nf, 0);
  ds = op->dat[0];
  set_ds_source(ds,"Zmag recording");      
  for (j = 0; j < nf; j++)
    {
      page_n = j/g_r->page_size;
      i_page = j%g_r->page_size;
      ds->xd[j] = (g_r->timing_mode == 1) ? (g_r->imi[page_n][i_page] - g_r->imi_start) 
	: g_r->imi[page_n][i_page];
      ds->yd[j] = g_r->zmag[page_n][i_page];
    }
      
  if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("I can't create plot !");
  set_ds_source(ds,"Rotation recording");      
      
  for (j = 0; j < nf; j++)
    {
      page_n = j/g_r->page_size;
      i_page = j%g_r->page_size;
      ds->xd[j] = (g_r->timing_mode == 1) ? (g_r->imi[page_n][i_page] - g_r->imi_start) 
	: g_r->imi[page_n][i_page];
      ds->yd[j] = g_r->rot_mag[page_n][i_page];
    }
  if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("I can't create plot !");
  set_ds_source(ds,"Focus recording");      

  for (j = 0; j < nf; j++)
    {
      page_n = j/g_r->page_size;
      i_page = j%g_r->page_size;
      ds->xd[j] = (g_r->timing_mode == 1) ? (g_r->imi[page_n][i_page] - g_r->imi_start) 
	: g_r->imi[page_n][i_page];
      ds->yd[j] = g_r->obj_pos[page_n][i_page];
    }
  create_attach_select_x_un_to_op(op, IS_SECOND, 0
				  ,(float)1/g_r->Pico_param_record.camera_param.camera_frequency_in_Hz, 0, 0, "s");
      
      
  set_op_filename(op, "ZmagRotFocusscan_zmag%d.gr",g_r->n_rec);
  set_plot_title(op, "Scan_Zmag %d zmag, rotation & focus ", g_r->n_rec);
  set_plot_x_title(op, "Time");
  set_plot_y_title(op, "Zmag, rot, focus");			
      
  op->need_to_refresh = 1;
  find_x_limits(op);
  find_y_limits(op);



  */


  switch_plot(pr, pr->n_op - 1);

  free(sph);
  return refresh_plot(pr, pr->n_op - 1);    
}






# endif
