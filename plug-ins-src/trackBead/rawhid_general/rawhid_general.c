
#include "rawhid_general.h"

#define PACKET_SIZE 64


pthread_t rawhid_thread_id = 0;
bool rawhid_continous_listening = false;
int reboot_teensy_in_progress = -1;
rawhid_g_data *g_data=NULL;
bool trigger_async_cmd = false;
report_data *async_cmd = NULL;
report_data *async_cmd_prev = NULL;
float tbo2x_coeff_1 = .0897785; //for tbox2v10 calibrated
static int teensy_id=0;
bool trigger_motor_obj=false;
ad_motorobj_hid *motor_obj = NULL;


temp_PID_state PID_state = {0};
static float Tbox = 0;
static float Tsample = 0;
static float Tmag = 0;
static float Ttresse = 0;
static float Tsink = 0;
static float Tmagsink = 0;
static float RH = 0;
static float Tair = 0;

float prev_x = 0;
float prev_y = 0;

//RAWHID_V1: TBOX2 HW protocol
//RAWHID_V2: TBOX3 HW protocol
#ifdef RAWHID_V2

int reboot_teensy(void)
{
  *async_cmd = *g_data->r_data[g_data->current_available_data];
  async_cmd->msg_type = 'W';
  async_cmd->xy_status = 0;
  async_cmd->index_Temperature = 0;
  async_cmd->ZmagObj_status = 0;
  async_cmd->error_code = 666;
  trigger_async_cmd = true;
  rawhid_continous_listening = false;
  reboot_teensy_in_progress = 0;
  return 0;
}
int do_reboot_teensy(void)
{
  if (updating_menu_state!=0) return D_O_K;
  reboot_teensy();
  return 0;
}

int do_close_teensy(void)
{
  if (updating_menu_state!=0) return D_O_K;
  rawhid_continous_listening = false;
  rawhid_close(teensy_id);
  return 0;
}
int do_reinit_teensy(void)
{
  if (updating_menu_state!=0) return D_O_K;
  rawhid_continous_listening = false;
  _rawhid_init(0);
  rawhid_continous_listening = true;
  return 0;
}

int convert_char_to_report(report_data *msg_out, char *msg_in, float timestamp_offset)
{
  //memcpy( msg_out, msg_in, sizeof(msg_in);//bordel je comprends pas pourquoi ca marche pas!!!
  int ci=0;
  unsigned long *tmp_lu;
  unsigned int *tmp_int;
  float *tmp_fl;
  msg_out->msg_type =(unsigned char) msg_in[ci];
  ci += sizeof(unsigned char);

#ifdef XV_UNIX
  tmp_int = (unsigned int *)(msg_in + ci);
  msg_out->error_code  = (unsigned long) tmp_int[0];
  ci+=sizeof(unsigned int);
#else
  tmp_lu = (unsigned long *)(msg_in + ci);
  msg_out->error_code  = tmp_lu[0];
  ci+=sizeof(unsigned long);
#endif

  msg_out->index_Temperature =(unsigned char) msg_in[ci];
  ci+=sizeof(char);

#ifdef XV_UNIX
  tmp_lu = (unsigned int *)(msg_in + ci);
  msg_out->timestamp  = (unsigned long) tmp_lu[0];
  ci+=sizeof(unsigned int);
#else
  tmp_lu = (unsigned long *)(msg_in + ci);
  msg_out->timestamp  = tmp_lu[0];
  ci+=sizeof(unsigned long);
#endif

  msg_out->ZmagObj_status = (char) msg_in[ci];
  ci+=sizeof(char);
  msg_out->xy_status = (char) msg_in[ci];
  ci+=sizeof(char);
  tmp_fl = (float *)(msg_in + ci);
  msg_out->zmag = (float) tmp_fl[0];
  msg_out->vmag = (float) tmp_fl[1];
  msg_out->zobj = (float) tmp_fl[2];
  msg_out->x = (float) tmp_fl[3];
  msg_out->y = (float) tmp_fl[4];
  //my_set_window_title("%c  %c  %f  %f %f  %f",msg_out->msg_type,msg_out->index_Temperature,msg_out->zmag,msg_out->vmag,msg_out->zobj,msg_out->x);
  msg_out->TemperatureTbox = (float) tmp_fl[5];
  msg_out->TemperatureMag = (float) tmp_fl[6];
  msg_out->TemperatureSink = (float) tmp_fl[7];
  msg_out->intensity_led1 = (float) tmp_fl[8];
  msg_out->intensity_led2 = (float) tmp_fl[9];

  if (msg_out->index_Temperature & 0x01) Tbox = msg_out->TemperatureTbox;
  if (msg_out->index_Temperature & 0x02) Tsample = msg_out->TemperatureTbox;

  if (msg_out->index_Temperature & 0x04) Tmag = msg_out->TemperatureMag;
  if (msg_out->index_Temperature & 0x08) Ttresse = msg_out->TemperatureMag;

  if (msg_out->index_Temperature & 0x10) Tsink = msg_out->TemperatureSink;
  if (msg_out->index_Temperature & 0x20) Tmagsink = msg_out->TemperatureSink;
  if (msg_out->index_Temperature & 0x40) Tair = msg_out->TemperatureSink;
  if (msg_out->index_Temperature & 0x80) RH = msg_out->TemperatureSink;

  prev_x = msg_out->x / 1000;//microns to mm
  prev_y = msg_out->y / 1000;
  return 0;

}
int convert_report_to_char(report_data *msg_in, char *msg_out)
{
  //memcpy(msg_out, msg_in, 64*sizeof(char));
  int ci=0;
  unsigned long *tmp_lu;
  unsigned int *tmp_int;
  float *tmp_fl;
  char *tmp_c;

  msg_out[0] = msg_in->msg_type;
#ifdef XV_UNIX
  tmp_int = (unsigned int *) &msg_out[1];
  tmp_int[0] = (unsigned int) msg_in->error_code;
  tmp_c = (char *) &tmp_int[1];
#else
  tmp_lu = (unsigned long *) &msg_out[1];
  tmp_lu[0] = msg_in->error_code;
  tmp_c = (char *) &tmp_lu[1];
#endif
  tmp_c[0] = msg_in->index_Temperature;

#ifdef XV_UNIX
  tmp_int = (unsigned int *) &tmp_c[1];
  tmp_int[0] = (unsigned int) msg_in->timestamp;
  tmp_c = (char *) &tmp_int[1];
#else
  tmp_lu = (unsigned long *) &tmp_c[1];
  tmp_lu[0] = (unsigned int) msg_in->timestamp;
  tmp_c = (char *) &tmp_lu[1];
#endif
  //tmp_lu = (unsigned long *) &tmp_c[1];
  //tmp_lu[0] = msg_in->timestamp;
  tmp_c[0] = msg_in->ZmagObj_status;
  tmp_c[1] = msg_in->xy_status;
  tmp_fl = (float *) &tmp_c[2];
  tmp_fl[0] = msg_in->zmag;
  tmp_fl[1] = msg_in->vmag;
  tmp_fl[2] = msg_in->zobj;
  tmp_fl[3] = msg_in->x;
  tmp_fl[4] = msg_in->y;
  tmp_fl[5] = msg_in->TemperatureTbox;
  tmp_fl[6] = msg_in->TemperatureMag;
  tmp_fl[7] = msg_in->TemperatureSink;
  tmp_fl[8] = msg_in->intensity_led1;
  tmp_fl[9] = msg_in->intensity_led2;
  return 0;
}
#else
int convert_char_to_report(report_data *msg_out, char *msg_in, float timestamp_offset)
{
  float *tmp = NULL;
  int ci=0;
  if ((msg_in[0] != 'R') && (msg_in[0] != 'W')) return -1;//one must not convert another type of msg onto report one
  msg_out->msg_type = msg_in[0];
  msg_out->error_code = msg_in[1];
  if (msg_out->error_code < 0) return -2;
  msg_out->zmag_status = msg_in[2];
  msg_out->zobj_status = msg_in[3];
  ci=4;
  tmp = (float *) (msg_in+ci);
  ci=0;
  msg_out->timestamp = (tmp[ci] - timestamp_offset);
  msg_out->T0 = tmp[++ci];
  msg_out->T1 = tmp[++ci];
  msg_out->T2 = tmp[++ci];
  msg_out->T3 = tmp[++ci];
  msg_out->T4 = tmp[++ci];
  msg_out->zmag = tmp[++ci];
  msg_out->vmag = tmp[++ci];
  msg_out->rot = tmp[++ci];
  msg_out->zobj = tmp[++ci];
  msg_out->zobj_sensor = tmp[++ci];
  msg_out->vobj = tmp[++ci];
  msg_out->i_pump = tmp[++ci];
  msg_out->i_led_1 = tmp[++ci];
  msg_out->i_led_2 = tmp[++ci];
  return 0;
}
int convert_report_to_char(report_data *msg_in, char *msg_out)
{
  //if ((msg_in->msg_type != 'R') && (msg_in->msg_type != 'W')) return -2;
  int ci=0;
  float *tmp;
  msg_out[0] = msg_in->msg_type;
  msg_out[1] = msg_in->error_code;
  msg_out[2] = msg_in->zmag_status;
  msg_out[3] = msg_in->zobj_status;
  ci = 4;
  tmp = (float *) &msg_out[ci];
  ci = 0;
  tmp[ci] = msg_in->timestamp;
  tmp[++ci] = msg_in->T0;
  tmp[++ci] = msg_in->T1;
  tmp[++ci] = msg_in->T2;
  tmp[++ci] = msg_in->T3;
  tmp[++ci] = msg_in->T4;
  tmp[++ci] = msg_in->zmag;
  tmp[++ci] = msg_in->vmag;
  tmp[++ci] = msg_in->rot;
  tmp[++ci] = msg_in->zobj;
  tmp[++ci] = msg_in->zobj_sensor;
  tmp[++ci] = msg_in->vobj;
  tmp[++ci] = msg_in->i_pump;
  tmp[++ci] = msg_in->i_led_1;
  tmp[++ci] = msg_in->i_led_2;
  return 0;
}
#endif

int copy_report_data(report_data *out, report_data *in)
{
  out = in;
  return 0;
}

int match_timestamp(float t, int *i_match, float *jitter)
{
  int i;
  float delta_timestamp, best_delta_timestamp;
  int match_index = 0;
  best_delta_timestamp = t - g_data->r_data[0]->timestamp;
  best_delta_timestamp = (best_delta_timestamp<0) ? (-best_delta_timestamp) : best_delta_timestamp ;
  for (i=1;i<g_data->n_report_data;i++)
  {
    delta_timestamp =  t - (g_data->r_data[i]->timestamp);
    delta_timestamp = (delta_timestamp<0) ? (-delta_timestamp) : delta_timestamp ;
    if (delta_timestamp < best_delta_timestamp)
    {
      best_delta_timestamp = delta_timestamp;
      match_index = i;
    }
  }
  *i_match = match_index;
  *jitter = best_delta_timestamp;
  return 0;
}


int do_rawhid_manual_async_cmd(void)
{
  int status;
  if (updating_menu_state!=0) return D_O_K;
  //copy_report_data(async_cmd, g_data->r_data[g_data->current_available_data]);
  *async_cmd = *g_data->r_data[g_data->current_available_data];
#ifdef RAWHID_V2
//by default no command
  async_cmd->xy_status = 0;
  async_cmd->index_Temperature = 0;
  async_cmd->ZmagObj_status = 0;

  float ip=0;
  float zmag_prev, zmag_cmd, zobj_prev, zobj_cmd, vmag_prev, vmag_cmd, x_prev, x_cmd, y_prev, y_cmd;
  float Tbox_cmd, Tbox_prev, Tmag_cmd, Tmag_prev, il1_cmd, il1_prev, il2_cmd, il2_prev;
  zmag_prev = zmag_cmd = + Pico_param.translation_motor_param.zmag_offset - async_cmd->zmag / 1000;
  zobj_prev = zobj_cmd = async_cmd->zobj * tbo2x_coeff_1;
  vmag_prev = vmag_cmd = async_cmd->vmag;
  x_prev =x_cmd = async_cmd->x;
  y_prev = y_cmd = async_cmd->y;
  il2_cmd = il2_prev = async_cmd->intensity_led2;
  il1_cmd = il1_prev = async_cmd->intensity_led1;
  Tbox_cmd = Tbox_prev = Tbox;
  Tmag_cmd = Tmag_prev = Tmag;
  int status_obj = 0;
  if ( (win_scanf("Send a set of commands.\n"
      "zmag = %5f  vmag = %5f \n"
      "zobjstatus %2d zobj=%5f \n"
      "Tbox%6f Tmag=%6f \n"
      "ipump=%6f il1=%6f il2=%6f\n"
      "X()=%6f, Y()=%6f",
      &zmag_cmd, &vmag_cmd, &status, &zobj_cmd
      ,&Tbox_cmd, &Tmag_cmd, &ip, &il1_cmd, &il2_cmd, &x_cmd, &y_cmd))==WIN_CANCEL ) return D_O_K;

  if(zmag_cmd != zmag_prev)
  {
    async_cmd->zmag = (- zmag_cmd + Pico_param.translation_motor_param.zmag_offset) * 1000 ;//conversion into microns
    async_cmd->ZmagObj_status |= 0x10;
  }
  if(zobj_cmd != zobj_prev)
  {
    async_cmd->zobj = zobj_cmd / tbo2x_coeff_1;
    async_cmd->ZmagObj_status |= 0x20;
  }
  if(vmag_cmd != vmag_prev)
  {
    async_cmd->vmag = vmag_cmd;
    async_cmd->ZmagObj_status |= 0x40;
  }
  if (Tbox_cmd != Tbox_prev)
  {
    async_cmd->TemperatureTbox = Tbox_cmd;
    async_cmd->index_Temperature |= 0x10;
  }
  if (Tmag_cmd != Tmag_prev)
  {
    async_cmd->TemperatureMag = Tmag_cmd;
    async_cmd->index_Temperature |= 0x20;
  }
  if (x_cmd != x_prev)
  {
    async_cmd->x = x_cmd;
    async_cmd->xy_status |= 0x10;
  }
  if (y_cmd != y_prev)
  {
    async_cmd->y = y_cmd;
    async_cmd->xy_status |= 0x20;
  }
  if (il1_cmd != il1_prev)
  {
    async_cmd->intensity_led1 = il1_cmd;
    async_cmd->xy_status |= 0x01;
  }
  if (il2_cmd != il2_prev)
  {
    async_cmd->intensity_led2 = il2_cmd;
    async_cmd->xy_status |= 0x02;
  }

#else

if ( (win_scanf("this allows to send asynchronous cmd to teensy\n"
    "zmag = %5f vmag = %5f \n"
    "zobjstatus %4d zobj=%5f vobj=%5f\n"
    "TPID%6f \n"
    "ipump=%6f il1=%6f il2=%6f\n",
    &async_cmd->zmag, &async_cmd->vmag, &status, &async_cmd->zobj
    ,&async_cmd->vobj,&async_cmd->T3,&async_cmd->i_pump,&async_cmd->i_led_1
    ,&async_cmd->i_led_2))==WIN_CANCEL ) return D_O_K;
    status = (int)async_cmd->zobj_status;
    async_cmd->zobj_status = (char)status;
#endif
  async_cmd->msg_type = 'W';
  trigger_async_cmd = true;
  return 0;
}


int do_rawhid_temperature_async_cmd(void)
{
  static int doset = 0;
  int dt_deriv = PID_state.dt_deriv;
  int kp = PID_state.kp;             // proportional coeff
  int ki = PID_state.ki;             // integral coeff
  int kd = PID_state.kd;             // derivative coeff
  ad_therma_hid *tmpPID = (ad_therma_hid *)async_cmd;
  int rec_1 = PID_state.rec;
  unsigned long t0;

  if (updating_menu_state!=0) return D_O_K;

  //win_printf("sent %d rec %d",PID_state.sent,PID_state.rec);
  copy_report_data(async_cmd, async_cmd_prev);
  tmpPID->msg_type ='T';               // = T (like thermalisation)
  tmpPID->direction = '?';
  tmpPID->error_code = 0;
  trigger_async_cmd = true;
  for (t0 = my_uclock() + get_my_uclocks_per_sec()/2; rec_1 == PID_state.rec
	 && my_uclock() < t0; ); // to improve
  if(rec_1 == PID_state.rec)
    win_printf("rawhid did not update pid parameters");

  kp = PID_state.kp;             // proportional coef111f
  ki = PID_state.ki;             // integral coeff
  kd = PID_state.kd;             // derivative coeff
  if (win_scanf("this allows to send asynchronous cmd to teensy\n"
		"to modify the PID settings\n"
		"%R->querry parameters %r->set them\n"
		"kp = %5d \n"
		"ki = %5d  kd = %5d\n"
		,&doset,&kp,&ki,&kd) == WIN_CANCEL ) return D_O_K;
  tmpPID->msg_type ='T';               // = T (like thermalisation)
  tmpPID->direction = (doset) ? '=' : '?';
  tmpPID->error_code = 0;
  tmpPID->dt_deriv = dt_deriv;
  tmpPID->kp = kp;             // proportional coeff
  tmpPID->ki = ki;             // integral coeff
  tmpPID->kd = kd;             // derivative coeff

  trigger_async_cmd = true;
  return 0;
}
#ifdef RAWHID_V2
float x_step = 250;
float y_step = 150;
int x_step_up(void)
{
  *async_cmd = *g_data->r_data[g_data->current_available_data];
  async_cmd->msg_type = 'W';
  async_cmd->x += x_step;
  async_cmd->xy_status = 0;
  async_cmd->index_Temperature = 0;
  async_cmd->ZmagObj_status = 0;
  async_cmd->xy_status |= 0x10;
  trigger_async_cmd = true;
  return 0;
}
int x_step_down(void)
{
  *async_cmd = *g_data->r_data[g_data->current_available_data];
  async_cmd->msg_type = 'W';
  async_cmd->x -= x_step;
  async_cmd->xy_status = 0;
  async_cmd->index_Temperature = 0;
  async_cmd->ZmagObj_status = 0;
  async_cmd->xy_status |= 0x10;
  trigger_async_cmd = true;
  return 0;
}
int y_step_up(void)
{
  *async_cmd = *g_data->r_data[g_data->current_available_data];
  async_cmd->msg_type = 'W';
  async_cmd->y += y_step;
  async_cmd->xy_status = 0;
  async_cmd->index_Temperature = 0;
  async_cmd->ZmagObj_status = 0;
  async_cmd->xy_status |= 0x20;
  trigger_async_cmd = true;
  return 0;
}
int y_step_down(void)
{
  *async_cmd = *g_data->r_data[g_data->current_available_data];
  async_cmd->msg_type = 'W';
  async_cmd->y -= y_step;
  async_cmd->xy_status = 0;
  async_cmd->index_Temperature = 0;
  async_cmd->ZmagObj_status = 0;
  async_cmd->xy_status |= 0x20;
  trigger_async_cmd = true;
  return 0;
}
int do_xy_steps(void)
{
  if(updating_menu_state != 0)
  {
    add_keyboard_short_cut(0, KEY_F9, 0, x_step_up);
    add_keyboard_short_cut(0, KEY_F10, 0, x_step_down);
    add_keyboard_short_cut(0, KEY_F11, 0, y_step_up);
    add_keyboard_short_cut(0, KEY_F12, 0, y_step_down);
    return D_O_K;
  }
  return D_O_K;
}
#endif
MENU *rawhidv1_menu(void)
{
  static MENU mn[32];
  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn, "send one async cmd", do_rawhid_manual_async_cmd, NULL, 0, NULL);
#ifndef RAWHID_V2
  add_item_to_menu(mn, "send T pid async cmd", do_rawhid_temperature_async_cmd, NULL, 0, NULL);
#endif
#ifdef RAWHID_V2
  add_item_to_menu(mn, "REBOOT teensy", do_reboot_teensy, NULL, 0, NULL);
  //add_item_to_menu(mn, "reinit RAWHID COM", do_reinit_teensy, NULL, 0, NULL);
  //add_item_to_menu(mn, "close teensy", do_close_teensy, NULL, 0, NULL);
  add_item_to_menu(mn, "XY shortcuts", do_xy_steps, NULL, 0, NULL);
#endif
  return mn;
}

static void *rawhid_thread_proc(void *pdata)
{
  char msg_cmd[PACKET_SIZE] = {0};
  int count = 0;
  int n_byte, ret;
  int rawhid_timeout = 100;
  char msg_buffer[PACKET_SIZE];
  //int i_match, jitter;
  long long t, t_pc, t_daq;
  long long t0_thread = 0;
  unsigned int dt;
  float t_rel, t_pc_rel, t_daq_rel, t_pc_rel_prev;
  ad_therma_hid *PID = NULL;
  float dt_bef_reboot = 500;
  float dt_bef_reopen = 3000;
  (void)pdata;
  add_image_treat_menu_item( "Rawhidtest", NULL, rawhidv1_menu(), 0, NULL);
  static int msg_count = 0;
  int cont_count=0;
  static int reboot_count=0;
  while(true)
  {
    if (reboot_teensy_in_progress == 0)
    {
      reboot_count++;
      if( _rawhid_init(0) == 0 )
      {
        reboot_teensy_in_progress = -1;
        rawhid_continous_listening = true;
      }
      usleep(400000);
    }
    t0_thread = my_uclock();
    cont_count++;
    while (rawhid_continous_listening==true)
    {
      n_byte = rawhid_recv(0, msg_buffer, PACKET_SIZE, rawhid_timeout);
      if (n_byte == PACKET_SIZE)
      {
        msg_count ++;
        if (msg_buffer[0] == 'R')
        {
          t_daq = my_uclock();
          ret = convert_char_to_report(g_data->r_data[g_data->current_report_data], msg_buffer, g_data->t0);
          if (count==0) g_data->t0 = g_data->r_data[g_data->current_report_data]->timestamp;
          g_data->current_available_data = g_data->current_report_data;
          g_data->current_report_data++;
          g_data->current_report_data = (g_data->current_report_data)%g_data->n_report_data;
        }
        if (msg_buffer[0] == 'T') // && msg_buffer[1] == '0')
        {

	         PID = (ad_therma_hid *)msg_buffer;
           printf("received parameters %d %d %d %d\n",PID->dt_deriv,PID->kp,PID->ki,PID->kd);
	         PID_state.dt_deriv = PID->dt_deriv;
	         PID_state.kp = PID->kp;             // proportional coeff
	         PID_state.ki = PID->ki;             // integral coeff
	         PID_state.kd = PID->kd;             // derivative coeff
	         PID_state.last_read = my_uclock();
	         PID_state.rec++;
	      }
        n_byte = 0;
        count ++;
      }
      t_pc = my_uclock();
      dt = get_my_uclocks_per_sec();
      t_pc_rel = (float) (t_pc - t0_thread);
      t_pc_rel = (float)(t_pc_rel)/(float)(dt)*1000;
      t_daq_rel = (float) (t_daq - t0_thread);
      t_daq_rel = (float)(t_daq_rel)/(float)(dt)*1000;
      //my_set_window_title("tpc %f tdaq %f",t_pc_rel,t_daq_rel);
      if ( fabs(t_pc_rel - t_daq_rel) > dt_bef_reboot )
      {
        #ifdef RAWHID_V2
        reboot_teensy();
        #endif
        //rawhid_continous_listening = false;
        //reboot_teensy_in_progress = 0;
      }
      if (count==1)
      {
#ifdef RAWHID_V2
        *async_cmd = *g_data->r_data[g_data->current_available_data];
        *async_cmd_prev = *g_data->r_data[g_data->current_available_data];
#else
        copy_report_data(async_cmd_prev, g_data->r_data[g_data->current_available_data]);
        copy_report_data(async_cmd, g_data->r_data[g_data->current_available_data]);
#endif
      }
      if (trigger_async_cmd==true)
      {
        trigger_async_cmd = false;
        convert_report_to_char(async_cmd, msg_cmd);
        rawhid_send(0, (char *)msg_cmd, PACKET_SIZE, 5);//not 0 otherwise just not sent
#ifdef RAWHID_V2
        *async_cmd_prev = *async_cmd;
#else
        copy_report_data(async_cmd_prev, async_cmd);//update last cmd with last sent
#endif
	      if (msg_cmd[0] == 'T') 	  PID_state.sent++;
      }
    }
  }
  return NULL;
}

int _launch_rawhid_thread(void)
{
  int success, i;
  if (g_data==NULL)
  {
    g_data = (rawhid_g_data *)calloc(1, sizeof(rawhid_g_data));
    g_data->current_report_data = 0;
    g_data->n_report_data = 10;
    g_data->current_available_data = 0;
    g_data->t0 = 0;
    g_data->r_data = (report_data **)calloc(g_data->n_report_data, sizeof(report_data *));
    for (i=0;i<g_data->n_report_data;i++)
    {
      g_data->r_data[i] = (report_data *)calloc(1, sizeof(report_data));
      g_data->r_data[i]->timestamp = 0;
    }
  }
  if (async_cmd==NULL) async_cmd = (report_data *)calloc(1,sizeof(report_data));
  if (async_cmd_prev==NULL) async_cmd_prev = (report_data *)calloc(1,sizeof(report_data));
  if (motor_obj==NULL) motor_obj = (ad_motorobj_hid *)calloc(1, sizeof(ad_motorobj_hid));
  trigger_async_cmd = false;
  success = pthread_create (&rawhid_thread_id, NULL, rawhid_thread_proc, NULL);
  if (success) win_printf_OK("rawhid thread not launched !");
  rawhid_continous_listening = true;
  return success;
}

#ifdef RAWHID_V2
int dialog_set_X(char *xtarc)
{
  register int i;
  float x_targ = 0;
  if (sscanf(xtarc,"%f",&x_targ) != 1) return 0;
  if (i < 0)	win_printf_OK("Could not add pending action!");
  *async_cmd = *g_data->r_data[g_data->current_available_data];
  async_cmd->x = x_targ * 1000;
  async_cmd->index_Temperature = async_cmd->ZmagObj_status = 0;
  async_cmd->xy_status = 0x10;
  async_cmd->msg_type = 'W';
  trigger_async_cmd = true;
  return D_O_K;
}
#endif

int _request_read_temperature_value(int image_n, int t, unsigned long *t0)
{
  (void)image_n;
  (void)t;
  (void)t0;
  return 0;
}

float _read_temperature_from_request_answer(char *answer, int im_n, float val, int *error, int *channel)
{
  (void)answer;
  (void)im_n;
  (void)val;
  (void)error;
  (void)channel;
    *error = 0;
    return 0;
}
int _set_temperature(float z)
{
  static int init=0;
  if (init==0) {;}//need to avoid the first measurement
  else if (init==1) {;}
  else
  {
    //copy_report_data(async_cmd, async_cmd_prev);
    *async_cmd = *g_data->r_data[g_data->current_available_data];
#ifdef RAWHID_V2
    async_cmd->TemperatureTbox = z;
    async_cmd->xy_status = async_cmd->ZmagObj_status = 0;
    async_cmd->index_Temperature = 0x10;  //command mask to apply the cmd
#else
    async_cmd->T3 = z;
#endif
    async_cmd->msg_type = 'W';
    trigger_async_cmd = true;
  }
  init++;
  return 0;
}
float _get_temperature(int t)
{
#ifdef RAWHID_V2
  switch (t)
  {
    case 0:
    return Tbox;
    break;
    case 1:
    return Tsample;
    break;
    case 2:
    return Tmag;
    break;
    case 3:
    return Tsink;
    break;
  }
  return -1;
#else
  switch (t)
  {
    case 0:
    return g_data->r_data[g_data->current_available_data]->T0;
    break;
    case 1:
    return g_data->r_data[g_data->current_available_data]->T1;
    break;
    case 2:
    return g_data->r_data[g_data->current_available_data]->T2;
    break;
    case 3:
    return g_data->r_data[g_data->current_available_data]->T3;
    break;
    case 4:
    return g_data->r_data[g_data->current_available_data]->T4;
    break;
  }
#endif
  return -1;
}
int _set_T0_pid(int num, int val)
{
  (void)num;
  (void)val;
  return 3;
}
int  _get_T0_pid(int num)
{
  (void)num;
  return 1;
}
//pas envie de me faire chier a mettre 2 set_light : si z=1010 on envoie 10 a la deuxieme led
int _set_light(float z)
{
  //copy_report_data(async_cmd, async_cmd_prev);//copy last order in the current one to keep eventual other command running (like temp)
  *async_cmd = *g_data->r_data[g_data->current_available_data];

  if (z>=200)
  {
#ifdef RAWHID_V2
  async_cmd->intensity_led1 = z - 200;
#else
  async_cmd->i_led_1 = z-200;
#endif
  }
  else
  {
#ifdef RAWHID_V2
  async_cmd->intensity_led2 = z;
#else
  async_cmd->i_led_2 = z;
#endif
  }
  async_cmd->msg_type = 'W';
  trigger_async_cmd = true;
  return 2;
}
// return from 0 to 100
float _get_light(void)
{
  static int which_one=0;
  if (which_one==0)
  {
    which_one=1;
#ifdef RAWHID_V2
    return g_data->r_data[g_data->current_available_data]->intensity_led1;
#else
    return g_data->r_data[g_data->current_available_data]->i_led_1;
#endif
  }
  else
  {
    which_one=0;
#ifdef RAWHID_V2
    return g_data->r_data[g_data->current_available_data]->intensity_led2;
#else
    return g_data->r_data[g_data->current_available_data]->i_led_2;
#endif
  }
}
//sdi beta stuff
//compare to rawhid_v1 (sdi alpha) the magnets are also included in rawhid. the report fomat also changes (see .h)
#ifdef  RAWHID_V2
int _read_magnet_z_value_and_status(float *z, float *vcap, int *limit, int *vcap_servo)
{
  if (z != NULL) *z = - (- Pico_param.translation_motor_param.zmag_offset + g_data->r_data[g_data->current_available_data]->zmag / 1000) ; //µC in microns
  if(vcap) *vcap = 0;
  if(limit) *limit=0;
  if(vcap_servo) *vcap_servo=0;
  return 0;
}
int   _set_magnet_z_value(float pos) //track_util with CGH_ZMAG_ABS and REL
{
  static int init=0;
  if (init==0) {;}
  else if (init==1) {;}
  else
  {
    *async_cmd = *g_data->r_data[g_data->current_available_data];
    async_cmd->zmag = (- pos + Pico_param.translation_motor_param.zmag_offset) * 1000 ;
    //win_printf_OK("set z to %f %f",pos,async_cmd->zmag);
    async_cmd->xy_status = 0;
    async_cmd->index_Temperature = 0;
    async_cmd->ZmagObj_status = 0x10; //only zmag moves
    async_cmd->msg_type = 'W';
    trigger_async_cmd = true;
  }
  init++;
  return 0;
}
int _set_zmag_motor_speed (float v) // CHG_ZMAG_PID
{
  *async_cmd = *g_data->r_data[g_data->current_available_data];
  async_cmd->vmag = v ;
  async_cmd->xy_status = 0;
  async_cmd->index_Temperature = 0;
  async_cmd->ZmagObj_status = 0x40;//vmag change
  async_cmd->msg_type = 'W';
  trigger_async_cmd = true;
  return 0;
}
int _set_magnet_z_ref_value(float z) //CHG_ZMAG_REF ds track_util
{
  Pico_param.translation_motor_param.zmag_offset = z + g_data->r_data[g_data->current_available_data]->zmag / 1000;
  return 0;
} //ref SW?
int _get_zmag_motor_pid(int num)
{return 0;}
float  _get_zmag_motor_speed(void)
{return g_data->r_data[g_data->current_available_data]->vmag;}


float _read_magnet_z_value(void) //linked to read_magnet_z_value_raw, which is called by read_magnet_z_value, is called by read_magnet_z_value_and_status
{return 0;}

//useless
int   _set_z_origin(float pos, int dir)
{return 0;}
int _request_read_rot_value(int image_n, unsigned long *t0)
{return 0;}
float _read_rot_raw_from_request_answer(char *answer, int im_n, float val, int *error)
{return 0;}
int _request_set_rot_value_raw(int image_n, float rot, unsigned long *t0)
{return 0;}
int _check_set_rot_request_answer(char *answer, int im_n, float rot)
{return 0;}
int _request_set_magnet_z_value_raw(int image_n, float pos,  unsigned long *t0)
{return 0;}
int _check_set_zmag_request_answer(char *answer, int im_n, float pos)
{return 0;}
int _request_read_magnet_z_value(int image_n, unsigned long *t0)
{return 0;}
float _read_zmag_raw_from_request_answer(char *answer, int im_n, float val, int *error, float *vacp)
{return 0;}
int _request_set_zmag_ref_value(int image_n, float pos, unsigned long *t0)
{return 0;}
int _check_set_zmag_ref_request_answer(char *answer, int im_n, float pos)
{return 0;}
int _request_set_rot_ref_value(int image_n, float rot,  unsigned long *t0)
{return 0;}
int _check_set_rot_ref_request_answer(char *answer, int im_n, float rot)
{return 0;}
int _set_zmag_motor_pid(int num, int val)
{return 0;}
int _set_rotation_motor_pid(int num, int val)
{return 0;}
int  _get_rotation_motor_pid(int num)
{return 0;}
float  _get_Vcap_min(void)
{return 0;}
int _set_Vcap_min(float volts)
{return 0;}
int _set_magnet_z_value_and_wait(float pos)
{return 0;}
int _set_rot_value_and_wait(float rot)
{return 0;}
int _has_magnets_memory(void)
{return 0;}
int  _init_magnets_OK(void)
{return 0;}
int   _set_rot_value(float rot)
{return 0;}
int _set_rot_ref_value(float rot)
{return 0;}
float _read_rot_value(void)
{return 0;}
int _set_motors_speed(float v)
{return 0;}
float  _get_motors_speed(void)
{return 0;}
#endif

int  _init_focus_OK(void)
{
  return 0;
}
int _has_focus_memory(void)
{
 return 0;
}
//result from the encorder in mm, to be converted to µm with the calibration coeff of the deformable platin
float _read_Z_value_accurate_in_micron(void)
{
  float z_ret;
  static int init=0;
  if (g_data==NULL) return -1;
  if (init==0) z_ret=0;
  else if (init==1) z_ret=1;
  else z_ret = g_data->r_data[g_data->current_available_data]->zobj * tbo2x_coeff_1; //ratio
  init++;
  return z_ret;
}
int _read_Z_value(void)
{
  return (int)(_read_Z_value_accurate_in_micron());//ask meaning i10*
}
float _read_Z_value_OK(int *error) //n microns
{
  (void)error;
  return _read_Z_value_accurate_in_micron();
}
float _read_last_Z_value(void) //in micron
{
  return _read_Z_value_accurate_in_micron();// (g_data->r_data[g_data->current_available_data]->zobj);//why not asking the driver?
}
int _set_Z_value(float z)  //in microns
{
  static int init=0;
  if (init==0) {;}
  else if (init==1) {;}
  else
  {
    *async_cmd = *g_data->r_data[g_data->current_available_data];
    //copy_report_data(async_cmd, async_cmd_prev);//copy last order in the current one to keep eventual other command running (like temp)
    async_cmd->zobj = z / tbo2x_coeff_1;
#ifdef RAWHID_V2
    async_cmd->xy_status = 0;
    async_cmd->index_Temperature = 0;
    async_cmd->ZmagObj_status = 0x20;
#endif
    async_cmd->msg_type = 'W';
    trigger_async_cmd = true;
  }
  init++;
  return D_O_K;
}
int _set_Z_step(float z)  // in micron
{
  (void)z;
	return 0;
}
int _set_Z_obj_accurate(float zstart)
{
  return _set_Z_value(zstart);
}
int _set_Z_value_OK(float z)
{
  return _set_Z_value(z);
}
int _inc_Z_value(int step)
{
  (void)step;
  _set_Z_value( g_data->r_data[g_data->current_available_data]->zobj );
  return 0;
}
int _small_inc_Z_value(int up)
{
  //int nstep;
  (void)up;
  _set_Z_step(0.1);
  //nstep = (up>=0)? 1:-1;
  _set_Z_value( g_data->r_data[g_data->current_available_data]->zobj );
  return 0;
}
int _big_inc_Z_value(int up)
{
  //int nstep;
    (void)up;
  _set_Z_step(0.1);
  //nstep = (up>=0)? 10:-10;
  _set_Z_value( g_data->r_data[g_data->current_available_data]->zobj );
  return 0;
}
//#endif
int _rawhid_init(int verbose)
{
  int r;
  //cf id of teensy in rawhid on website
  // Arduino-based example is 16C0:0486:FFAB:0200
  //if ( (rawhid_id = rawhid_open(1, 0x16C0, 0x0480, 0xFFAB, 0x0200)) == -1)  win_printf_OK("unable to open the device");
  //bon d'après les codes c de prjc faut initialiser 2 fois...chais pas pourquoi mais bon
  r = rawhid_open(1, 0x16C0, 0x0480, 0xFFAB, 0x0200);
  if (r <= 0)
  {
    r = rawhid_open(1, 0x16C0, 0x0486, 0xFFAB, 0x0200);
    if (r <= 0)
    {
      if (verbose == 1) win_printf_OK("no rawhid device found\n");
      return -1;
    }
  }
  teensy_id = r;
  return 0;
}
