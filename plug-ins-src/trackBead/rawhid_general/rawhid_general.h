#pragma once

# include <pthread.h>
# include <stdbool.h>
# include <unistd.h>
# include "allegro.h"
#ifdef XV_WIN32
# include "winalleg.h"
#endif
# include "xvin.h"
#ifdef XV_UNIX
#include "hid_LINUX.h"
#else
#include "rawhid-o.h"
#endif
#ifdef XV_UNIX

#endif

# define BUILDING_PLUGINS_DLL
# include "rawhid_general.h"
# include "../trackBead.h"
# include "../track_util.h"
# include "../../cfg_file/Pico_cfg.h"

#define ASYNC_T0 0
#define ASYNC_ZMAG 1
#define ASYNC_ZOBJ 2
#define ASYNC_VMAG 3
#define ASYNC_VOBJ 4

//rawhid_v2: TBox3 hw protocol
#ifdef RAWHID_V2
typedef struct rawhid_report_data
{
  unsigned char msg_type;
  unsigned long error_code; // 16 bits : be careful of endianess
  char index_Temperature;   //| command Mask (From DAQ)  2^0
  unsigned long timestamp;  //This is readonly from the DAQ point of view.
  char ZmagObj_status;      //Hi[0 Zmag, 1 Zobj] LOW[0 immobile, 1 moving, 2 arrived, 3 off]  . | command Mask (From DAQ)  2^1
  char xy_status ;          //HI[0 X, 1 Y ] [0 immobile, 1 moving, 2 arrived, 3 off]  | command Mask (From DAQ)  2^2
  float zmag;               //| command Mask (From DAQ)  2^3
  float vmag;               //| command Mask (From DAQ)  2^4
  float zobj;               //| command Mask (From DAQ)  2^5
  float x;                  //| command Mask (From DAQ)  2^6
  float y;                  //| command Mask (From DAQ)  2^7
  float TemperatureTbox;    //Tbox (index_temp 2^0 ), Tsample(index_temp 2^1 ),   | command Mask (From DAQ)  2^8
  float TemperatureMag;     //Tmagnet (index_temp 2^2 ), Tresse (index_temp 2^3 )  | command Mask (From DAQ)  2^9
  float TemperatureSink;    //TSink, Tmagsink, TAir, RH (relative humidity) //| command Mask (From DAQ)  2^10
  float intensity_led1;     //| command Mask (From DAQ)  2^11
  float intensity_led2;     //| command Mask (From DAQ)  2^12
  char remain[10] ;
}__attribute__((__packed__)) report_data;
#else
//RAWHID_V1 dataframe structure (TBox2)
typedef struct rawhid_report_data
{
  char msg_type ;
  char error_code ;
  char zmag_status ;
  char zobj_status ;
  float timestamp ;
  float T0 ;//thermistance pid tbox
  float T1 ; //thermistante sample
  float T2 ;// temp sink (lm35?)
  float T3 ; //pid magnets
  float T4 ; //magnets
  float zmag ; //zmag
  float vmag ;
  float rot;
  float zobj ;
  float zobj_sensor;
  float vobj ;
  float i_pump;
  float i_led_1;
  float i_led_2;
  //char remain[0] ;
}__attribute__((__packed__)) report_data;
#endif

/*
union raw_rep_data                   // data pointer to pixel
{
  struct rawhid_report_data Tdat;
  char ch_stream[sizeof(report_data)];
};
*/

// thermalisation HID register
/*
typedef struct
{
  char error_code ;
  char msg_type ;
  char r_w ;
  float tcmd ;
  unsigned int dt_deriv : 5;
  unsigned int samp ;
  unsigned int kp ;
  unsigned int ki ;
  unsigned int kd ;
  unsigned int FS : 100 ;
  float ibox ;
  char remain[28] ;
} __attribute__((__packed__)) ad_therma_hid;
*/
// motor objective HID register
typedef struct
{
  char error_code ;
  char msg_type ;
  char r_w ;
  unsigned int gain : 8 ;
  unsigned int mag : 16 ;
  unsigned int angle : 16 ;
  float v ;
  float pos ;           // in um !
  float pos_min ;
  float pos_max ;
  unsigned int kp ;
  unsigned int ki ;
  unsigned int kd ;
  unsigned int precision : 8 ;    // in nm !
  unsigned int nstable : 8 ;
  unsigned int max_acc : 8;
  unsigned int max_v : 8 ;
  unsigned int target_v : 8;
  unsigned int v_p : 8 ;
  char remain[22] ;
} __attribute__((__packed__)) ad_motorobj_hid;


// thermalisation HID register
typedef struct
{
  char msg_type ;               // = T (like thermalisation)
  char direction;
  char error_code ;
  char stat ;
  int dt_deriv;        // number of previous temeprature measurements used to average the derivative of the error (Tmeas-Tcmd)
  int kp ;             // proportional coeff
  int ki ;             // integral coeff
  int kd ;             // derivative coeff
  char remain[44] ;             // remaining bytes (out of 64)
} __attribute__((__packed__)) ad_therma_hid;


typedef struct
{
  int dt_deriv;        // number of previous temeprature measurements used to average the derivative of the error (Tmeas-Tcmd)
  int kp ;             // proportional coeff
  int ki ;             // integral coeff
  int kd ;             // derivative coeff
  long long last_read;                //
  long long last_asked;
  int sent;
  int rec;
} temp_PID_state;


typedef struct rawhid_general_data
{
  report_data **r_data;
  int n_report_data;
  int current_report_data;//the next to be taken
  int current_available_data;
  long long t0;
}rawhid_g_data;


#ifdef RAWHID_V2
PXV_VAR(float, prev_x);
PXV_VAR(float, prev_y);
PXV_FUNC(int, dialog_set_X, (char *xtarc));
#endif

//only in sdi beta verision: ie when magnets handled by rawhid
#ifdef RAWHID_V2
PXV_FUNC(int, _set_magnet_z_value, (float pos));
PXV_FUNC(float, _read_magnet_z_value, (void));
PXV_FUNC(int, _set_magnet_z_ref_value,(float z));  /* in mm */
PXV_FUNC(int,   _set_z_origin,(float pos, int dir));
PXV_FUNC(int, _set_zmag_motor_speed, (float v));
PXV_FUNC(float,  _get_zmag_motor_speed, (void));

PXV_FUNC(int, _init_magnet_OK, (void));
PXV_FUNC(int, _set_rot_value, (float rot));
PXV_FUNC(int, _set_rot_ref_value, (float rot));  /* in tour */
PXV_FUNC(float, _read_rot_value, (void));
PXV_FUNC(int, _set_motors_speed, (float v));
PXV_FUNC(float, _get_motors_speed, ());
PXV_FUNC(int, _has_magnets_memory,(void));
PXV_FUNC(int,    _set_magnet_z_value_and_wait,(float pos));
PXV_FUNC(int,    _set_rot_value_and_wait,(float rot));
PXV_FUNC(int, _go_and_dump_z_magnet,(float z));
PXV_FUNC(int,    _go_wait_and_dump_z_magnet,(float zmag));
PXV_FUNC(int,    _go_and_dump_rot,(float r));
PXV_FUNC(int, _go_wait_and_dump_rot,(float r));
PXV_FUNC(int, _go_wait_and_dump_log_specific_rot,(float r, char *log));
PXV_FUNC(int, _go_wait_and_dump_log_specific_z_magnet,(float zmag, char *log));
PXV_FUNC(int, _request_read_rot_value, (int image_n, unsigned long *t0));
PXV_FUNC(float, _read_rot_raw_from_request_answer, (char *answer, int im_n, float val, int *error));
PXV_FUNC(int, _request_set_rot_value_raw, (int image_n, float rot, unsigned long *t0));
PXV_FUNC(int, _check_set_rot_request_answer, (char *answer, int im_n, float rot));
PXV_FUNC(int, _request_set_magnet_z_value_raw, (int image_n, float pos,  unsigned long *t0));
PXV_FUNC(int, _check_set_zmag_request_answer, (char *answer, int im_n, float pos));
PXV_FUNC(int, _request_read_magnet_z_value, (int image_n, unsigned long *t0));
PXV_FUNC(float, _read_zmag_raw_from_request_answer, (char *answer, int im_n, float val, int *error, float *vacp));
PXV_FUNC(int, _request_set_zmag_ref_value, (int image_n, float pos, unsigned long *t0));
PXV_FUNC(int, _check_set_zmag_ref_request_answer, (char *answer, int im_n, float pos));
PXV_FUNC(int, _request_set_rot_ref_value, (int image_n, float rot,  unsigned long *t0));
PXV_FUNC(int, _check_set_rot_ref_request_answer, (char *answer, int im_n, float rot));
PXV_FUNC(int, _read_magnet_z_value_and_status, (float *z, float *vcap, int *limit, int *vcap_servo));
PXV_FUNC(float,  _get_Vcap_min, (void));
PXV_FUNC(int,   _set_Vcap_min, (float volts));
PXV_FUNC(int, _set_zmag_motor_pid, (int num, int val));
PXV_FUNC(int,  _get_zmag_motor_pid, (int num));
PXV_FUNC(int, _set_rotation_motor_pid, (int num, int val));
PXV_FUNC(int,  _get_rotation_motor_pid, (int num));
#endif

PXV_FUNC(int, _init_focus_OK,  (void));
PXV_FUNC(int, _has_focus_memory, (void));
PXV_FUNC(float, _read_Z_value_accurate_in_micron, (void));
PXV_FUNC(int, _read_Z_value, (void));
PXV_FUNC(float, _read_Z_value_OK, (int *error)); //mic
PXV_FUNC(float, _read_last_Z_value, (void));
PXV_FUNC(int, _set_Z_step, (float z));
PXV_FUNC(int, _set_Z_value, (float z));
PXV_FUNC(int, _set_Z_obj_accurate, (float zstart));
PXV_FUNC(int, _set_Z_value_OK, (float z));
PXV_FUNC(int, _inc_Z_value, (int step));
PXV_FUNC(int, _small_inc_Z_value, (int up));
PXV_FUNC(int, _big_inc_Z_value, (int up));

PXV_FUNC(int, do_rawhid_manual_async_cmd, (void));
PXV_FUNC(int, _rawhid_init, (int verbose));
PXV_FUNC(int, _launch_rawhid_thread,(void));
PXV_FUNC(int, _request_read_temperature_value, (int image_n, int t, unsigned long *t0));
PXV_FUNC(float, _read_temperature_from_request_answer, (char *answer, int im_n, float val, int *error, int *channel));
PXV_FUNC(int, _set_T0_pid, (int num, int val));
PXV_FUNC(int,  _get_T0_pid, (int num));
PXV_FUNC(int, _set_temperature,(float z));
PXV_FUNC(float, _get_temperature,(int t));
PXV_FUNC(int, _set_light,(float z));
PXV_FUNC(float, _get_light,());
