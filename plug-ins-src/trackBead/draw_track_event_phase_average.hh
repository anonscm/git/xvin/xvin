#ifndef XV_DRAW_TE_PA_H
#define XV_DRAW_TE_PA_H

#ifndef XV_WIN32
# include "config.h"
#endif

#include "record.h"
#include "../stat/stat.h"
#include "../filters/filter_core.h"

#include "xvin.h"
#include "allegro.h"
#ifdef XV_WIN32
#   include "winalleg.h"
#endif
#include "float.h"
#include <locale.h>
#include <string>
#include <list>
#include <vector>
#include <type_traits>
#include "memorized_win_scan.hh"

namespace trackrenorm
{

    struct NoScript {};

    struct SaveScript
    {
        int             mincount  = 15;
        int             mincycles = 15;
        float           meddev    = .1f;

        std::string     key1      = "draw_track_event_phase_average";
        std::string     key2      = "selected_path";
        std::string     path;
        std::list<O_p*> selected;
    };

    /* default behaviour, used by NoScript */
    template <typename SCRIPT, typename ...Args>
    int do_win_scanf(SCRIPT &, char const * question, Args ... args)
    { return win_scanf(question, args ...); }

    /* default behaviour, used by NoScript */
    template <typename SCRIPT, typename ...Args>
    int on_done_one_bead(SCRIPT &, Args ...)
    { return D_O_K; }

    /* default behaviour, used by NoScript */
    template <typename SCRIPT>
    int on_do_one_bead(SCRIPT &, int)
    { return D_O_K; }

    /* specific behaviour for SaveScript */
    template <typename ... Args>
    int do_win_scanf    (SaveScript &, char const * question, Args ... args);
    int on_done_one_bead(SaveScript &, pltreg * pr, O_p *& op);

    template <typename T>
    int run(char const * key, T & script)
    {
#       include "record.hh"
    }

    template <typename ...Args>
    int do_win_scanf(SaveScript &script, char const * question, Args ... args)
    {
        char tmp[4096];
        snprintf(tmp, sizeof(tmp),
                "Refuse cycles with median deviation <= %%f, beads with less than %%d cycles.\n"
                "\n%s", question);

        decltype(win_scanf("")) res = D_O_K;
        auto dyn = config::save(script.key1, "median_dev", script.meddev, res);
        auto mc  = config::save(script.key1, "min_n_cycles", script.mincycles, res);

        res = win_scanf(tmp, *dyn, *mc, args ...);
        return res; // make sure to keep res = win_scanf, otherwise menu::save doesn't work
    }


    namespace run_traits
    {
        typedef std::vector<O_p*> vect_t;

        struct _has_allplots
        {
            template<class T>
            static auto test(T* p) -> decltype(p->allplots, std::true_type());

            template<class>
            static auto test(...) -> std::false_type;
        };

        template <typename T>
        struct _trait;

        template <> struct _trait<std::false_type>
        {
            static vect_t select(pltreg * pr)
            {
                g_record * g_r = find_g_record_in_pltreg(pr);
                if (g_r == nullptr)
                {
                    win_printf_OK("no opened recordn");
                    return vect_t{};
                }

                SaveScript script;
                run(script.key1.c_str(), script);
                return vect_t(script.selected.begin(), script.selected.end());
            }

            template <typename ... A>
            static void filter(A ...) {}
        };

        template <> struct _trait<std::true_type>
        {
            template <typename ... A>
            static vect_t   select(A ...) { return vect_t{nullptr}; }

            template <typename A>
            static void     filter(A cf, pltreg * pr, vect_t & sel)
            {
                if(cf.allplots)
                    sel.assign(pr->o_p, pr->o_p+pr->n_op);
                else
                    sel.assign({pr->o_p[pr->cur_op]});
            }
        };

        template <typename T>
        struct trait : public _trait<decltype(_has_allplots::test<T>(0))> {};
    }

    template <typename Cf, typename T>
    inline int run(T run, char const * formkey, char const * help)
    {
        if(updating_menu_state != 0)    return D_O_K;

        /* display routine action if SHIFT is pressed */
        if (key[KEY_LSHIFT]) return win_printf_OK(help);

        //we assume data in xd on integer frame numbers
        /* we first find the data that we need to transform */
        pltreg *pr  = nullptr;
        char tmp[512] = "%pr";
        if (ac_grep(cur_ac_reg,tmp,&pr) != 1)
            return win_printf_OK("cannot find data");

        auto sel = run_traits::trait<Cf>::select(pr);
        if(sel.size() == 0)
            return D_O_K;

        static Cf cf;
        if(query(cf, formkey)) return OFF;
        run_traits::trait<Cf>::filter(cf, pr, sel);

        for(auto op: sel)
            if(run(cf, pr, op))
                return OFF;
        return D_O_K;
    }
}
#endif
