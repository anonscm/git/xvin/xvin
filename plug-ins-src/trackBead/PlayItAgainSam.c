/*
 *    Plug-in program for image treatement in Xvin.
*
*    V. Croquette
*/

#ifndef _TRACKBEAD_C_
#define _TRACKBEAD_C_

# include "allegro.h"
#ifdef XV_WIN32
# include "winalleg.h"
#endif
# include "xvin.h"
# include "xv_main.h"

/* If you include other plug-ins header do it here*/



# define BUILDING_PLUGINS_DLL
# include "../cfg_file/Pico_cfg.h"
# include "../cfg_file/microscope.h"
# include "select_rec.h"

PXV_FUNC(int, autoload_plugins, (void));
PXV_FUNC(int, cardinal_main, (int , char **));
PXV_FUNC(int, wlc_main, (int , char **));
PXV_FUNC(int, stat_main, (int , char **));
PXV_FUNC(int, change_focus_string, (float focus));
PXV_FUNC(int, change_T0_string, (float T0));
PXV_FUNC(int, cordrift_main, (int , char **));
PXV_FUNC(int, ImOliSeq_main, (int , char **));
PXV_FUNC(int, FitInterference_main, (int , char **));
PXV_FUNC(int, polymerWLC_main, (int argc, char **argv));

int       cardinal_main(int, char **);
int       xvtiff_main(int, char **);



XV_FUNC(int, do_load_im, (void));
XV_FUNC(DIALOG *, get_time_slider, (void));

DIALOG *l_slider_di = NULL;

#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
//# include "PlayItAgainSam.h"
# include "../fft2d/fftbtl32n.h"
# include "fillibbt.h"
# include "focus.h"
# include "trackBead.h"
# include "track_util.h"


# include "record.h"
#ifdef OPENCL
#include "gpu/track_beads.h"
#endif


float n_magnet_z = 0, n_rota = 0;

float prev_zmag = 0;
float prev_mag_rot = 0;
float prev_focus = 0;
float prev_T0 = 0;


# ifdef NOTNEEDED

float zmag_to_force(int nb, float zmag)
{
    float z, tmp;
    z = zmag - Pico_param.magnet_param.zmag_contact_sample;
    tmp = Pico_param.magnet_param.a1[nb] * z;

    if (Pico_param.magnet_param.nd[nb] > 2)
    {
        tmp -= Pico_param.magnet_param.a2[nb] * z * z;
    }

    if (Pico_param.magnet_param.nd[nb] > 3)
    {
        tmp += Pico_param.magnet_param.a3[nb] * z * z * z;
    }

    if (Pico_param.magnet_param.nd[nb] > 4)
    {
        tmp -= Pico_param.magnet_param.a4[nb] * z * z * z * z;
    }

    tmp = exp(tmp);
    return (Pico_param.magnet_param.f0[nb] * tmp);
}

# endif
int co_set = 0;
float co_dl_ld[2048];
float my_sqrt[10000];
float my_exp_1[512];



// function executed in the tracking thread
int add_bead_calib_image(O_i *oi,                         // the image to put the bead in
                         float xcf, float ycf, float zcf, // bead position in microns
                         float dx,                        // the extend of one pixel in microns
                         O_i *oic)               // the bead calibration image
{
    register int i, j, k;
    float x, y, x2, y2, r, z, dy, impf, fr;
    int onx, ony, xci, yci, xis, xie, yis, yie, ocnx, ocny, zc0, zc1;
    union pix  *pd, *pdc;
    float yb, yb2, xb2, ys = 3, zb = 63; // bust to measure angle

    if (oi == NULL)
    {
        return 1;
    }

    onx = oi->im.nx;
    ony = oi->im.ny;
    ocnx = oic->im.nx;
    ocny = oic->im.ny;
    dx = oic->dx;
    dy = oic->dy;

    if (co_set == 0)
    {
        for (i = 0; i < 512; i++)
        {
            my_exp_1[i] = exp(-((float)i) / 64);
        }

        for (i = 0; i < 2048; i++)
        {
            co_dl_ld[i] = 2 * cos((M_PI * (float)i) / 100);
        }

        for (i = 0; i < 10000; i++)
        {
            my_sqrt[i] = sqrt((float)i / 16);
        }
    }

    xci = (int)((xcf / dx) + .5);
    xis = xci - ocnx / 2;
    xis = (xis < 0) ? 0 : xis;
    xis = (xis < onx) ? xis : onx - 1;
    xie = xci + ocnx / 2;
    xie = (xie < 0) ? 0 : xie;
    xie = (xie < onx) ? xie : onx;
    yci = (int)((ycf / dx) + .5);
    yis = yci - ocnx / 2;
    yis = (yis < 0) ? 0 : yis;
    yis = (yis < ony) ? yis : ony - 1;
    yie = yci + ocnx / 2;
    yie = (yie < 0) ? 0 : yie;
    yie = (yie < ony) ? yie : ony;
    z = (zcf < 0) ? 0 : zcf / dy; // the position in y in the calibration image
    zc0 = (int)z;
    fr = z - zc0;    // the fractional part
    zc0 = (zc0 < ocny) ? zc0 : ocny - 1;
    zc1 = zc0 + 1;
    zc1 = (zc1 < ocny) ? zc1 : ocny - 1;

    //z2 = 4*z*z; // 3*z*z is interesting
    for (i = yis, pd = oi->im.pixel, pdc = oic->im.pixel; i < yie ; i++)
    {
        y = fabs(ycf - (i * dx));
        yb = fabs(y - ys);     // bust angle
        yb2 = 140 * yb * yb;        // bust angle
        y2 = 16 * y * y; // +0.5
        y2 += 0.5;

        for (j = xis; j < xie; j++)
        {
            x = fabs(xcf - (j * dx));               // x and y are from pixel to bead center in microns
            //r = sqrt(x*x + y*y);                   // r is distance from pixel to bead center in microns
            xb2 = 70 * x * x;        // bust angle
            x2 = 16 * x * x;
            r = my_sqrt[(int)(x2 + y2)]; // r is distance from pixel to bead center in microns
            r /= dx; // this is the distance in pixels
            k = (int)r;

            if (k < ocnx / 2)
            {
                k += ocnx / 2;
                impf = (1 - fr) * pdc[zc0].fl[k] + fr * pdc[zc1].fl[k];

                if ((yb2 + xb2) < zb)
                {
                    impf += zb - xb2 - yb2; // bust angle
                    //my_set_window_title("bump %g",zb - xb2 - yb2);
                }

                impf = (impf < 255) ? impf : 255;
                pd[i].ch[j] = (unsigned char)impf;
            }
            else
            {
                continue;    //pd[i].ch[j] = im_grey_level;
            }
        }
    }

    return 0;
}

int	draw_moving_shear_rect_vga_screen_unit(int xt, int yt, // top fringes center
					       int xb, int yb, // bottom fringes center
					       int nfx, int nfy, // fringes size
					       int maxdx,    // maxium dx allowed
					       int dfy0, int dym,// fringes distance, typical and max dy
					       int color, int scale, int bead_nb,  BITMAP* bm, float *quality_factor)
{
    int x1, x2, x3, x4, y1, y2, y3, y4;
    //int tmp;
    int xcs, ycs;
    int l = (28 * (nfx >> 1)) / scale;
    int w = (28 * (nfy >> 1)) / scale;
    (void)maxdx;
    (void)dfy0;
    (void)dym;
  //
  //   if ((xt - xb) > maxdx)
  //     {
	// tmp = (xt + xb + maxdx)/2;
	// xb = (xt + xb - maxdx)/2;
	// xt = tmp;
  //     }
  //   else if ((xb - xt) > maxdx)
  //     {
	// tmp = (xt + xb + maxdx)/2;
	// xt = (xt + xb - maxdx)/2;
	// xb = tmp;
  //     }
  //   if ((yt - yb) > dfy0 + dym)
  //     {
	// tmp = (yt + yb + dfy0 + dym)/2;
	// yb = (yt + yb - dfy0 - dym)/2;
	// yt = tmp;
  //     }
  //   if ((yt - yb) < dfy0 - dym)
  //     {
	// tmp = (yt + yb + dfy0 - dym)/2;
	// yb = (yt + yb - dfy0 + dym)/2;
	// yt = tmp;
  //     }

    xcs = (28 * xt) / scale;
    ycs = bm->h - (28 * yt) / scale;
    x1 =  xcs - l;
    x2 =  xcs + l;
    y1 =  ycs - w;
    y2 =  ycs + w;
    acquire_bitmap(bm);
    line(bm, x1, y1, x2, y1, color);
    line(bm, x2, y1, x2, y2, color);
    line(bm, x2, y2, x1, y2, color);
    line(bm, x1, y2, x1, y1, color);
    line(bm, xcs, y1, xcs, y2, color);

    xcs = (28 * xb) / scale;
    ycs = bm->h - (28 * yb) / scale;
    x3 =  xcs - l;
    x4 =  xcs + l;
    y3 =  ycs - w;
    y4 =  ycs + w;

    line(bm, x3, y3, x4, y3, color);
    line(bm, x4, y3, x4, y4, color);
    line(bm, x2, y4, x3, y4, color);
    line(bm, x3, y4, x3, y3, color);
    line(bm, xcs, y3, xcs, y4, color);

    line(bm, (x1+x4)/2, y3, (x1+x4)/2, y2, color);
    release_bitmap(bm);

    textprintf_centre_ex(bm, large_font, xcs, ycs+30, color, -1, "%d", bead_nb);
  //  textprintf_centre_ex(bm, large_font, xcs, ycs+55, color, -1, "%f", quality_factor[0]);
  //  textprintf_centre_ex(bm, large_font, xcs, ycs+70, color, -1, "%f", quality_factor[1]);



    return 0;
}



int	draw_cross_in_screen_unit(int xc, int yc, int length, int width, int color, BITMAP *bmp,
                              int scale, int bead_nb, int fixed, char *zpos, int zcolor)
{
    // bead pixel position
    int l = (28 * (length >> 1)) / scale;
    int w = (28 * (width >> 1)) / scale;
    int y = bmp->h - (28 * yc) / scale;
    int xcs = (28 * xc) / scale;
    int x1 =  xcs - l;
    int x4 =  xcs + l;
    int x2 =  xcs - w;
    int x3 =  xcs + w;
    int y1 =  y + l;
    int y4 =  y - l;
    int y2 =  y + w;
    int y3 =  y - w;
    //char bd[8];
    //sprintf(bd,"%d",bead_nb);
    acquire_bitmap(bmp);
    hline(bmp, x1, y2, x2, color);
    vline(bmp, x2, y2, y1, color);
    hline(bmp, x2, y1, x3, color);
    vline(bmp, x3, y1, y2, color);
    hline(bmp, x3, y2, x4, color);
    vline(bmp, x4, y2, y3, color);
    hline(bmp, x3, y3, x4, color);
    vline(bmp, x3, y3, y4, color);
    hline(bmp, x2, y4, x3, color);
    vline(bmp, x2, y3, y4, color);
    hline(bmp, x1, y3, x2, color);
    vline(bmp, x1, y2, y3, color);
    release_bitmap(bmp);
    //switch_allegro_font (big_font);
    textprintf_centre_ex(bmp, large_font, xcs, y, color, -1, "%d", bead_nb);

    if (fixed)
    {
        textprintf_centre_ex(bmp, large_font, xcs - 3 * w, y - 5 * w, color, -1, "F");
    }

    if (zpos != NULL)
    {
      textprintf_ex(bmp, large_font, xcs + w + w, y + w + w, zcolor, -1, "%s",zpos);
    }

    //alfont_textout(bmp,user_font,bd,xcs,y, color);
    return 0;
}



int show_trk_bead_cross(BITMAP *buffer, imreg *imr, DIALOG *d)
{
    int i = 0, yoff, color;
    g_record *g_r = NULL;
    char z_string[128];
    g_r = find_g_record_in_imreg(imr);

    if (g_r == NULL)
    {
        return 0;
    }

    if (imr == NULL || buffer == NULL || d == NULL)
    {
        return 1;
    }

    yoff =  plt_buffer->h - imr->y_off - d->y;

    for (i = 0; i < g_r->n_bead ; i++)
    {
        snprintf(z_string, 128, "Z = %5.3f", g_r->z_cor * g_r->b_r[i]->z[g_r->c_page][g_r->in_page_index]);
        color = (is_bead_profile_good(g_r,g_r->b_r[i]->profile_index[g_r->c_page][g_r->in_page_index]))
                ? makecol(0, 255, 0) : makecol(255, 0, 0);
        if (g_r->SDI_mode)
        {
          draw_moving_shear_rect_vga_screen_unit(imr->x_off + g_r->b_r[i]->x[g_r->c_page][g_r->in_page_index], yoff + g_r->b_r[i]->y[g_r->c_page][g_r->in_page_index],
             imr->x_off + g_r->b_r[i]->x[g_r->c_page][g_r->in_page_index], yoff + g_r->b_r[i]->y[g_r->c_page][g_r->in_page_index], 32, 32,
              0, 0, 0, makecol(0, 255, 0), imr->screen_scale,i, buffer,0);
        }
        else
        {
        draw_cross_in_screen_unit(imr->x_off + g_r->b_r[i]->x[g_r->c_page][g_r->in_page_index]
                                  , yoff + g_r->b_r[i]->y[g_r->c_page][g_r->in_page_index]
                                  , g_r->b_r[i]->cl , g_r->b_r[i]->cw
                                  , makecol(0, 255, 0), buffer,
                                  imr->screen_scale, i, 0, z_string, color);
                                }
    }

    return 0;
}

int pias_oi_post_display(struct one_image *oi, DIALOG *d)
{
    imreg *imr;
    (void)oi;

    if (d == NULL || d->dp == NULL || d->proc == NULL)
    {
        return 1;
    }

    imr = (imreg *)d->dp;
    show_trk_bead_cross(plt_buffer, imr, d);
    return 0;
}





int source_idle_action(DIALOG *d)
{
    int i = 0, yoff, color;
    //  static float last_rot_asked = 0;
    //static float last_focus_asked = 0;
    //static float last_zmag_asked = 0;
    static float prev_zmag_1 = 0;
    static float prev_mag_rot_1 = 0;
    static float prev_focus_1 = 0;
    static float prev_T0_1 = 0;
    pltreg *pr = NULL;
    imreg *imr = NULL;;
    g_record *g_r = NULL;
    char z_string[128];
    DIALOG *di;
    int pos = 0, w = 0;
    static int last_pos = -1, last_w = -1;
    BITMAP *imb;
    (void)d;
    ac_grep(cur_ac_reg, "%im", &imr);
    ac_grep(cur_ac_reg, "%pr", &pr);
    g_r = find_g_record_in_pltreg(pr);

    if (g_r == NULL)
    {
        g_r = find_g_record_in_imreg(imr);
    }

    if (g_r == NULL)
    {
        return 0;
    }

    if (imr != NULL)
    {
        di = find_dialog_associated_to_imr(imr, NULL);

        if (l_slider_di)
        {
            w = (l_slider_di->d1 >> 16) & 0xFFFF;
            l_slider_di->d1 &= 0xFFFF;  // slider width to minimum
            pos = (g_r->abs_pos * l_slider_di->d2) / l_slider_di->d1;
            g_r->c_page = pos / g_r->page_size;
            g_r->in_page_index = pos % g_r->page_size;

            if (pos != last_pos || w != last_w)
            {
                l_slider_di->proc(MSG_DRAW, l_slider_di, 0);
            }

            //imr->o_i[0]->need_to_refresh &= BITMAP_NEED_REFRESH;
        }

        imr->o_i[0]->oi_post_display = pias_oi_post_display;
        //if (pos != last_pos && imr->cur_oi == 0) refresh_image(imr, 0);
        yoff =  plt_buffer->h - imr->y_off - di->y;

        for (i = 0; i < g_r->n_bead && imr->cur_oi == 0 && last_pos != pos ; i++)
        {
            if (i == 0)
            {
                imb = (BITMAP *)imr->one_i->bmp.stuff;
		for(;screen_acquired;); // we wait for screen_acquired back to 0;
		screen_acquired = 1;
                acquire_bitmap(screen);
                blit(imb, screen, 0, 0, imr->x_off + di->x, imr->y_off -
                     imb->h + di->y, imb->w, imb->h);
                release_bitmap(screen);
		screen_acquired = 0;
            }

            snprintf(z_string, 128, "Z = %5.3f", g_r->z_cor * g_r->b_r[i]->z[g_r->c_page][g_r->in_page_index]);
            color = (is_bead_profile_good(g_r,g_r->b_r[i]->profile_index[g_r->c_page][g_r->in_page_index]))
                    ? makecol(0, 255, 0) : makecol(255, 0, 0);
            draw_cross_in_screen_unit(imr->x_off + g_r->b_r[i]->x[g_r->c_page][g_r->in_page_index]
                                      , yoff + g_r->b_r[i]->y[g_r->c_page][g_r->in_page_index]
                                      , g_r->b_r[i]->cl , g_r->b_r[i]->cw
                                      , makecol(0, 255, 0), screen,
                                      imr->screen_scale, i, 0, z_string, color);
            //g_r->b_r[i]->calib_im = oi;
        }

        last_pos = pos;
        last_w = w;
    }
    else if (pr != NULL)
    {
        if (l_slider_di)
        {
	    int itmp;
            w = l_slider_di->d1 & 0xFFFF;
            w = (w * scope_buf_size) / g_r->abs_pos;
	    itmp = w << 16;
	    itmp != (l_slider_di->d1 & 0xFFFF);
	    l_slider_di->d1 = itmp;//(w << 16) | (l_slider_di->d1 &= 0xFFFF);

            if ((l_slider_di->d1 & 0xFFFF))
            {
                pos = (g_r->abs_pos * l_slider_di->d2) / (l_slider_di->d1 & 0xFFFF);
            }

            g_r->c_page = pos / g_r->page_size;
            g_r->in_page_index = pos % g_r->page_size;

            if (pos != last_pos || w != last_w)
            {
                my_set_window_title("w %d", w);

                if (pos != last_pos)
                {
                    l_slider_di->proc(MSG_DRAW, l_slider_di, 0);
                }

                last_pos = pos;
                last_w = w;
            }
        }
    }

    prev_zmag = g_r->zmag[g_r->c_page][g_r->in_page_index];
    prev_mag_rot = g_r->rot_mag[g_r->c_page][g_r->in_page_index];
    prev_focus = g_r->obj_pos[g_r->c_page][g_r->in_page_index];

    if (prev_mag_rot_1 != prev_mag_rot)
    {
        change_rotation_string(prev_mag_rot_1 = prev_mag_rot);
    }

    if (prev_zmag_1 != prev_zmag)
    {
        change_zmag_string(prev_zmag);
        //if (g_r) change_force_string(trk_zmag_to_force(0, prev_zmag, g_r));
        prev_zmag_1 = prev_zmag;
    }

    if (prev_focus_1 != prev_focus)
    {
        change_focus_string(prev_focus_1 = prev_focus);
    }

    if (prev_T0_1 != prev_T0)
    {
        change_T0_string(prev_T0_1 = prev_T0);
    }

    change_zmag_label_string(NULL, 0, 0, 1);
    change_focus_label_string(NULL, 0, 0, 1);
    change_param_label_string(NULL, 0, 0, 1);
    /*
    if (last_focus_asked != last_focus_target)
        last_focus_asked = last_focus_target;
    if (last_zmag_asked != n_magnet_z)
        last_zmag_asked = n_magnet_z;
    if (last_rot_asked != n_rota)
        last_rot_asked = n_rota;

    */
    return 0;
}




extern int (*pias_call)(char const *);
int	PlayItAgainSam_main(int argc, char **argv)
{
    static int first = 1;
    imreg *imr = NULL;;

    if (first)
    {
        //      XVcfg_main();
# ifdef XVIN_STATICLINK
        cardinal_main(0, NULL);
        wlc_main(0, NULL);
        polymerWLC_main(0,NULL);
        stat_main(0, NULL);
        cordrift_main(0, NULL);
        xvtiff_main(0,NULL);
        //ImOliSeq_main(0, NULL); removing this because of missing commits
        FitInterference_main(0, NULL);
#ifdef OPENCL
        trackBead_gpu_main(0, NULL);
#endif
#endif
        insert_item_to_menu_at_line(plot_file_menu, 0, "Reload tracking (trk)", do_load_trk_in_new_project, NULL, 0, NULL);
        insert_item_to_menu_at_line(plot_file_menu, 1, "Reload tracking (verbose)", do_load_trk_in_new_project, NULL,
                                    MENU_INDEX(1), NULL);
        insert_item_to_menu_at_line(plot_file_menu, 2, "\0",  NULL,   NULL,   0, NULL);
        insert_item_to_menu_at_line(plot_menu, 7, "Record menu", NULL, trk_plot_menu(), 0, NULL);
        insert_item_to_menu_at_line(plot_menu, 8, "Select events", NULL, select_plot_menu(), 0, NULL);
        insert_item_to_menu_at_line(image_file_menu, 0, "Reload tracking (trk)", do_load_trk_in_new_project, NULL, 0, NULL);
        insert_item_to_menu_at_line(image_file_menu, 1, "Reload tracking (verbose)", do_load_trk_in_new_project, NULL,
                                    MENU_INDEX(1), NULL);
        insert_item_to_menu_at_line(image_file_menu, 2, "\0",  NULL,   NULL,   0, NULL);
        insert_item_to_menu_at_line(image_menu, 6, "Record menu", NULL, trk_image_menu(), 0, NULL);

        add_item_to_menu(plot_menu, "Quicklink", NULL, quicklink_menu, 0, NULL);
        add_item_to_menu(image_menu, "Quicklink", NULL, quicklink_menu, 0, NULL);
        /*

        insert_item_to_menu_at_line(image_menu,6,"Record menu",NULL,trk_image_menu(),0,NULL);

        add_image_treat_menu_item ("Reload tracking", do_load_trk_in_new_project, NULL, 0, NULL);
        add_image_treat_menu_item ("Reload tracking (verbose)", do_load_trk_in_new_project, NULL, MENU_INDEX(1), NULL);
        add_image_treat_menu_item("Record menu",NULL,trk_image_menu(),0,NULL);
        add_plot_treat_menu_item ( "Reload tracking", do_load_trk_in_new_project, NULL, 0, NULL);
        add_plot_treat_menu_item ("Reload tracking (verbose)", do_load_trk_in_new_project, NULL, MENU_INDEX(1), NULL);
        add_plot_treat_menu_item ("Record menu",NULL,trk_plot_menu(),0,NULL);
        */
        first = 0;
        l_slider_di = get_time_slider();
        general_idle_action = source_idle_action;

        char const * file = argc > 1 ? argv[argc-1] : (char const * ) 0;
        if (file != NULL)
        {
            if(strlen(file) > 3 && strcmp(file+strlen(file)-3, ".py") == 0)
            {
                autoload_plugins();
                if(pias_call != NULL)
                    (*pias_call)(file);
            } else
                load_trk_in_new_project(argv[argc - 1], 0);
        }
        else
        {
            do_load_trk_in_new_project();
        }

        ac_grep(cur_ac_reg, "%im", &imr);

        if (imr != NULL)
        {
            imr->o_i[0]->oi_post_display = pias_oi_post_display;
        }
    }

    return 0;
}



int	PlayItAgainSam_unload(int argc, char **argv)
{
    (void)argc;
    (void)argv;
    remove_item_to_menu(image_treat_menu, "trackBead", NULL, NULL);
    return D_O_K;
}
#endif
