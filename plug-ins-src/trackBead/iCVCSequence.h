
/*********************************************************

						icvcsequence.h

**********************************************************/

#include "windows.h"
#include "iCVCImg.h"

#ifdef __cplusplus
  #define IMPORT(t) extern "C" t __stdcall
#else
  #define IMPORT(t) t __stdcall
#endif

#ifndef CVCSEQUENCE_INCLUDE
  #define CVCSEQUENCE_INCLUDE

   // Record Callbacks
  typedef long ( __stdcall *CBRecStart)   (void * pPrivate);
  typedef long ( __stdcall *CBRecFrame)   (long SeqNumberOfFrames, long SeqNumberOfRecFrames, void * pPrivate);
  typedef long ( __stdcall *CBRecEnd)     (void * pPrivate);
  typedef long ( __stdcall *CBRecPlay)    (long SeqNumberOfRecFrames, long SeqNumberOfPlyFrames, void * pPrivate, IMG PlayImage);
  
  // PlayBack Callback
  typedef long ( __stdcall *CBPlayPlay)   (long SeqNumberOfRecFrames, long SeqNumberOfPlyFrames, void * pPrivate, IMG PlayImage);
  
#endif	// CVCSEQUENCE_INCLUDE



// Record Functions

IMPORT(long)    SEQRecInit(long NumberOfFrames, IMG Image, TDRect RecRect, long Plane,  long& ErrMsg);
IMPORT(long)    SEQRecFree(long& RecHandle);
IMPORT(long)	SEQRecIsSequence(long RecHandle);

IMPORT(double)  SEQRecStart(long RecHandle, long LoopMode, long PostTrigger, void * CBRecStart, void * CBRecFrame, void * CBRecEnd, void * pPrivate, long& RecFrames);
IMPORT(double)  SEQRecPlay (long RecHandle, long Mode, void * CBRecPlay, void * pPrivate);
IMPORT(long)	SEQRecSave (long RecHandle, PCHAR Filename);
IMPORT(long)    SEQRecGetFrame(long RecHandle, long FrameNumber, IMG& Image);
IMPORT(long)    SEQRecAddFrame(long RecHandle, long FrameNumber, IMG Image);


// PlayBack Functions

IMPORT(long)	SEQPlayInit(PCHAR Filename, long& Frames, long& ErrMsg);
IMPORT(long)	SEQPlayFree(long& PlayHandle);
IMPORT(long)	SEQPlayIsSequence(long PlayHandle);

IMPORT(double)	SEQPlayPlay(long PlayHandle, long Mode, void * CBPlayPlay, void * pPrivate); 
IMPORT(long)    SEQPlayGetFrame(long PlayHandle, long ImageNumber, IMG& Image);











                                             






