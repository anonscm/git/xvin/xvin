/** \file brown.c
  \brief Plug-in program for bead tracking

  This is a subprogram that creates, displays and interfaces the menus with the plug-in functions.

  \author V. Croquette, J-F. Allemand
  */

#ifndef _TRACK_UTIL_C_
#define _TRACK_UTIL_C_


# include "allegro.h"
#ifdef XV_WIN32
# include "winalleg.h"
#endif
# include "ctype.h"
# include "xvin.h"
# include "fftl32n.h"
# include "fillib.h"

# ifdef USE_OPENMP
# include <omp.h>
# endif




/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "../cfg_file/Pico_cfg.h"
#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "../fft2d/fftbtl32n.h"
# include "fillibbt.h"
# include "focus.h"
# include "magnetscontrol.h"
# include "trackBead.h"
# include "brown_util.h"
# include "track_util.h"
# include "record.h"
# include "action.h"
# include "calibration.h"
# ifdef FLOW_CONTROL
# include "thorlabs/apt_flow_control.h"
# endif
# ifdef FLOW_CONTROL_DUMMY
# include "flow_control.h"
# endif
#ifdef OPENCL
#include "gpu/track_beads.h"
#endif
# ifdef SDI_VERSION_2
# include "SDI_2_functions.h"
#endif

#ifdef USE_MLFI
#include "MLFI/mlfi.h"
#endif

int zmag_label_modified = 0;
int focus_label_modified = 0;
int param_label_modified = 0;

bool g_is_cross_shortcut_create_enabled = false;
bool g_cross_shortcut_create_enable_fence = true;
int g_cross_shortcut_create_length = 0;
int g_cross_shortcut_create_width = 0;


//sdi stuff
#ifdef SDI_VERSION
extern bool go_rt_track_xz;
extern bool go_rt_track_y;
extern bool go_rt_track_bead;
extern bool go_snapshot;
extern sdi_v2_parameter *sdi_g_param;
extern sdi_v2_diagnose *sdi_g_diagnose;
extern sdi_v2_fov *sdi_g_fov;
extern sdi_v2_grab *sdi_grab;
extern float dt_computation_ms_xz;
extern float dt_computation_ms_y;
#endif
float rolling_plot_width = 0.85;

int dis_obj_servo = 0;
MENU bead_active_menu[32] = {0};

float dz_bead_inst = 0.03;

/** this function initialize the tracking info structure

  This function is started as soon as the video acquisition is launch so that we can
  check the timing

  \param none
  \return A tracking info structure.
  \version 19/05/06
  */

//link one sdi existing bead to one existing b_track structure :
//essentially it does hard link of key fields of the sdi bead fields onto b_track fields to be used in record functions
//this is particularly useful when importing automatic detection of beads made on saved movie of the current fov
//thus : the raw profile of the 2 fringes linked to rad_prof & orthoradial_prof
//x(t), y(t), z(t) onto x, y, z of the b_track structure
#ifdef SDI_VERSION
int sdi_v2_attach_sdi_bead_to_b_track(sdi_v2_bead *bead, b_track *bd)
{
    int i;
    if (bd == NULL) return D_O_K;
    if (bd->sdi_bd != NULL) return D_O_K;
    if (bead == NULL) return D_O_K;

    //to relink the selected field
    //here I use the existing array rad_prof & orthoradial_prof to put the profile for 2 pattern of fringes
    for (i = 0; i < bead->fringes_bead[0]->n_raw_x_profile_buffer; i++)
    {
        free(bead->fringes_bead[0]->raw_x_profile[i]);
        bead->fringes_bead[0]->raw_x_profile[i] = NULL;
    }
    bead->fringes_bead[0]->raw_x_profile = NULL;
    for (i = 0; i < bead->fringes_bead[1]->n_raw_x_profile_buffer; i++)
    {
        free(bead->fringes_bead[1]->raw_x_profile[i]);
        bead->fringes_bead[1]->raw_x_profile[i] = NULL;
    }
    bead->fringes_bead[1]->raw_x_profile = NULL;
    float **ext_float;
    float **field;
    ext_float = (float **)calloc(TRACKING_BUFFER_SIZE, sizeof(float *));
    field = (float **)calloc(TRACKING_BUFFER_SIZE, sizeof(float *));
    int n_field, c_field;

    //re-use rad_prof of bead to put fringes profile of first fringes
    for (i = 0 ; i < TRACKING_BUFFER_SIZE ; i++) ext_float[i] = &(bd->rad_prof[i][0]);
    sdi_v2_switch_ptr_to_sdi_fringes_2d_float_field(bead->fringes_bead[0], PTR_FRINGES_RAW_X_PROFILE, field, &n_field,
            &c_field, ext_float, TRACKING_BUFFER_SIZE);
    //same for orthoradial profile and second fringes
    for (i = 0 ; i < TRACKING_BUFFER_SIZE ; i++) ext_float[i] = &(bd->orthoradial_prof[i][0]);
    sdi_v2_switch_ptr_to_sdi_fringes_2d_float_field(bead->fringes_bead[1], PTR_FRINGES_RAW_X_PROFILE, field, &n_field,
            &c_field, ext_float, TRACKING_BUFFER_SIZE);


    float *field_1d;
    //bead linkage onto b_t to be used by b_r
    free(bead->z_t);
    bead->z_t = NULL;
    free(bead->y_t);
    bead->y_t = NULL;
    free(bead->x_t);
    bead->x_t = NULL;
    free(bead->quality_factor_t);
    bead->quality_factor_t = NULL;
    float *ext_float_1d;
    //put z,y,x of sdi bead in z,y,x of bead
    ext_float_1d = &(bd->z[0]);
    sdi_v2_switch_ptr_to_sdi_bead_1d_float_field(bead, PTR_BEAD_Z_T, &field_1d, &ext_float_1d, TRACKING_BUFFER_SIZE);
    float *ext_float_1d_2;
    ext_float_1d_2 = &(bd->y[0]);
    sdi_v2_switch_ptr_to_sdi_bead_1d_float_field(bead, PTR_BEAD_Y_T, &field_1d, &ext_float_1d_2, TRACKING_BUFFER_SIZE);
    float *ext_float_1d_3;
    ext_float_1d_3 = &(bd->x[0]);
    sdi_v2_switch_ptr_to_sdi_bead_1d_float_field(bead, PTR_BEAD_X_T, &field_1d, &ext_float_1d_3, TRACKING_BUFFER_SIZE);
    float *ext_float_1d_4;
    ext_float_1d_4 = &(bd->xnoise[0]);
    sdi_v2_switch_ptr_to_sdi_bead_1d_float_field(bead, PTR_BEAD_QUALITY_FACTOR_T, &field_1d, &ext_float_1d_4, TRACKING_BUFFER_SIZE);
    float *ext_float_1d_5;
    //ext_float_1d_5 = &(bd->ynoise[0]);


    //tells to sdi objects where you are in buffer
    bead->n_output_t = TRACKING_BUFFER_SIZE;
    bead->c_output_t = 0;
    bd->sdi_bd = bead;
    bd->xc = bead->xc;
    bd->yc = bead->yc;
    bd->xc_1 = bd->xc;
    bd->yc_1 = bd->yc;
    bd->movie_w = bd->cl + 10;
    bd->movie_h = bd->cw + 10;
    return 0;
}
#endif


//sdi proceed bead function : an empty box to not crash vc's code!
#ifdef SDI_VERSION
int sdi_v2_proceed_bead(b_track *bt, int bt_index, g_track *gt, BITMAP *imb, O_i *oi, int ci, int n)
{
    int k;
    int ci_1, ci_2;
    //un certain nombre de trucs que je débranche temporairement car à refaire version sdi : in image, not lost, servo, var des pixels
    static int count = 0;

    (void)bt_index;
    (void)gt;
    (void)imb;
    (void)oi;
    bt->in_image = 1;
    bt->not_lost = 1;//NB_FRAMES_BEF_LOST

    if (count == 0)
    {
        for (k = 0; k < 256; k++)
        {
            bt->x_v[0][k] = .9;
            bt->x_v[1][k] = .9;
            bt->xs_v[0][k] = .9;
            bt->xs_v[1][k] = .9;
            bt->y_v[0][k] = .9;
            bt->y_v[1][k] = .9;
            bt->ys_v[0][k] = .9;
            bt->ys_v[1][k] = .9;
            bt->xi[k] = .9;
            bt->yi[k] = .9;     // these are temporary int buffers for averaged profile
            bt->xis[k] = .9;
            bt->yis[k] = .9;     // these are temporary int buffers for averaged side profile
            bt->xf[k] = .9;
            bt->yf[k] = .9;     // these are temporary float buffers for averaged profile
            bt->xcor[k] = 9;
            bt->ycor[k] = 9;
        }
        for (k = 0; k < 512; k++)
        {
            bt->fx1[k] = 9;
            bt->fy1[k] = 9;
        }
        bt->x_var = bt->x_v[0];
        bt->y_var = bt->y_v[0];
        bt->xs_var = bt->xs_v[0];
        bt->ys_var = bt->ys_v[0];
        bt->x_var_1 = bt->x_v[0];
        bt->y_var_1 = bt->y_v[0];
        bt->xs_var_1 = bt->xs_v[0];
        bt->ys_var_1 = bt->ys_v[0];
        bt->z_track = 9;//escape every ring if z_track
        for (k = 0; k < TRACKING_BUFFER_SIZE; k++)
        {
            bt->n_l[k] = (char) 1;
            //bt->xnoise[k] = .999;//saved in x_er
            bt->ynoise[k] = .999;
            bt->dcor_dx[k] = -1;
            bt->dcor_dy[k] = -1;
            //bt->theta
        }
        count ++;
    }
    ci_2 = ci - 2;
    if (ci_2 < 0) ci_2 += TRACKING_BUFFER_SIZE;
    ci_1 = ci - 1;
    ci_1 = (ci_1 < 0) ? TRACKING_BUFFER_SIZE - 1 : ci_1;
    if (bt->sdi_bd->mouse_draged) bt->mouse_draged;    //could be called
    bt->im_prof_ref = -1;
    //for now only not lost
    if (fabs(bt->z[ci_2] - bt->z[ci]) > dz_bead_inst)
    {
        // if z present a significative jump record it
        bt->dz_max = bt->z[ci_2] - bt->z[ci];
        bt->dz_max_im = n;
    }
    //bt->z[ci] = gt->obj_pos[ci] - bt->z[ci]; // new
    //moving average on 10 frames
    //with sigma list with different memory duration)
    bt->z_avg += bt->z[ci];
    bt->iz_avg++;
    if (bt->iz_avg >= 10)
    {
        bt->z_avgd = bt->z_avg / bt->iz_avg;
        bt->z_avg = 0;
        bt->iz_avg = 0;
    }
    bt->n_l[ci] = (char) bt->not_lost;
    return 0;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// this running in the Real time timer thread
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
int sdi_1_proceed_fringes(g_track *gt, O_i *oi, BITMAP *imb, int ci, int n)
{
    clock_t t0, t1, t2;
    int i, k, c_b;

    if (gt == NULL) return 1;
    if (sdi_g_fov == NULL) return 0;
    gt->obj_servo_action = 0; //no servo on (for now)
    t0 = clock();
    if (go_rt_track_xz == true) //fringes tracking (x, z) (to increase the throughput I keep the possibility of switching off the y tracking (4fft per bead))
    {
	     //#pragma omp parallel for
	     for (k = 0 ; k < sdi_g_fov->n_fringes ; k++)
	     {
         sdi_v2_update_c_profile_fringes_points(sdi_g_fov->fringes_list[k], ci);
         sdi_v2_extract_sdi_fringes_raw_x_profile_from_image(oi, sdi_g_fov->fringes_list[k], sdi_g_fov->sdi_param);
         sdi_v2_process_sdi_fringes_x_profile(sdi_g_fov->fringes_list[k], sdi_g_fov->sdi_param);
         sdi_v2_compute_phase_coeff_of_sdi_fringes(sdi_g_fov->fringes_list[k], sdi_g_fov->sdi_param);
         sdi_v2_compute_x_z_coupled(sdi_g_fov->fringes_list[k]);
       }
    }
    t1 = clock();
    if (go_rt_track_y == true)
    {
	    //#pragma omp parallel for //le pragma omp y est instable : à résoudre
      for (k = 0 ; k < sdi_g_fov->n_y_profile ; k++)
      {
        sdi_v2_update_c_profile_y_profile_points(sdi_g_fov->y_profile_list[k], ci);
        sdi_v2_extract_sdi_y_profile_raw_y_profile_from_image(oi, sdi_g_fov->y_profile_list[k], sdi_g_fov->sdi_param);
        sdi_v2_process_y_profile_y_profile(sdi_g_fov->y_profile_list[k], sdi_g_fov->sdi_param);
        sdi_v2_compute_center_by_autoconv_of_sdi_v2_y_profile(sdi_g_fov->y_profile_list[k], sdi_g_fov->sdi_param);
      }
    }
    t2 = clock();
    if (go_rt_track_bead == true)
    {
      for (k = 0 ; k < sdi_g_fov->n_beads ; k++)
      {
	       sdi_v2_update_c_profile_bead_points(sdi_g_fov->beads_list[k], ci);
         if (go_rt_track_xz == true)
	       {
           sdi_v2_compute_z_bead(sdi_g_fov->beads_list[k]); //ça écrit dans bt->z si c'est linké a b_t
           sdi_v2_compute_x_bead(sdi_g_fov->beads_list[k]); //ça écrit dans bt->x

           //if (follow_drift_x)==true)
           /*sdi_v2_center_roi_x_fringes(O_i *oi, sdi_v2_parameter *sdi_param, sdi_v2_fringes *fringes);
           sdi_v2_center_x_roi_bead(sdi_v2_parameter *sdi_param, sdi_v2_bead *bead)
           need_backgroudn and ref points
           */
	      }
	      else
	      {
		       sdi_v2_dummy_xz_bead(sdi_g_fov->beads_list[k]);
	      }

	    if (go_rt_track_y == true)
	      {
		sdi_v2_compute_y_bead(sdi_g_fov->beads_list[k]); //ça écrit dans bt->y
		//sdi_v2_recenter_y_of_one_bead(sdi_g_fov->beads_list[k], sdi_g_fov->sdi_param, 3);//in pixel
	      }
	    else
	      {
		sdi_v2_dummy_y_bead(sdi_g_fov->beads_list[k]);
	      }

	    if (sdi_g_fov->diagnose != NULL && go_rt_track_xz)
	      {
		sdi_v2_diagnose_one_bead(sdi_g_fov->beads_list[k], sdi_g_fov->diagnose);
	      }
	  }
      }

    //quasiment plus rien là dedans..coquille de plus en plus vide, utilisée comme wrapper vers les track file
    for (c_b = 0; c_b < gt->n_b; c_b++) sdi_v2_proceed_bead(gt->bd[c_b], c_b, gt, imb, oi, ci, n);
    dt_computation_ms_xz = (float)(t1 - t0) / (float)(CLOCKS_PER_SEC / 1000);
    dt_computation_ms_y = (float)(t2 - t1) / (float)(CLOCKS_PER_SEC / 1000);
    //if at least one signal tracked (either xz or y then updates the data of the display (the display itself is idle))
    if (go_rt_track_xz == true || go_rt_track_y == true)
      {
	for (i = 0; i < sdi_g_fov->n_display; i++) //update the display object that are in fov
	  {
	    //pour le ralentir un petit ci%refresh_rate ou refresh_rate de 10 ou 20 pourrait etre une idee
	    sdi_v2_update_x_display(sdi_g_fov->display_list[i], ci, n);
	    sdi_v2_update_y_display(sdi_g_fov->display_list[i], ci);
	  }
      }
    return 0;
}
#endif


int switch_RT_debug_on(void)
{
    int mode;
    char file[1024] = {0};

    if (updating_menu_state != 0)
    {
        if (RT_debug_flag == 1)
        {
            active_menu->flags |=  D_DISABLED;
        }
        else
        {
            active_menu->flags &= ~D_DISABLED;
        }

        return D_O_K;
    }

    strncpy(file, RT_debug_file, sizeof(file));
# ifdef WIN32

    if (do_select_file(file, sizeof(file), "RT debug-FILE", "txt", "Save txt debug file", 0))
    {
        return win_printf_OK("Cannot select ouput file");
    }

# endif
    backslash_to_slash(file);
    mode = win_printf("Debugging file: %s\n"
                      "Do you want to reset all information in that file?\n"
                      , file);

    if (mode == WIN_CANCEL)
    {
        fp_RT_debug = fopen(RT_debug_file, "a");
    }
    else
    {
        fp_RT_debug = fopen(RT_debug_file, "w");
    }

    if (fp_RT_debug == NULL)
    {
        return 1;
    }

    fclose(fp_RT_debug);
    RT_debug_flag = 1;
    return 0;
}
int switch_RT_debug_off(void)
{
    if (updating_menu_state != 0)
    {
        if (RT_debug_flag == 0)
        {
            active_menu->flags |=  D_DISABLED;
        }
        else
        {
            active_menu->flags &= ~D_DISABLED;
        }

        return D_O_K;
    }

    RT_debug_flag = 0;
    return 0;
}




int RT_debug(char *format, ...)
{
    char c[2048] = {0};
    va_list ap;

    if (RT_debug_flag == 0)
    {
        return 0;
    }

    if (format != NULL && RT_debug_file != NULL)
    {
        va_start(ap, format);
        vsnprintf(c, 2048, format, ap);
        va_end(ap);
        fp_RT_debug = fopen(RT_debug_file, "a");

        if (fp_RT_debug != NULL)
        {
            fprintf(fp_RT_debug, "IM %d >%s", ((track_info != NULL) ? track_info->ac_i : -1), c);
            fclose(fp_RT_debug);
        }
    }

    return 0;
}




int check_heap_raw(char *file, unsigned long line)
{
#ifdef OLD //XV_WIN32
    int out;
    out = _heapchk();

    if (out != -2)
    {
        win_printf("File %s at line %d\nheap satus %d (-2 =>OK)\n", file, (int)line, out);
    }

    return (out == -2) ? 0 : out;
#else
    (void) file;
    (void) line;
    return 0;
#endif
}


int grab_basename_and_index(char *filename, int n_file, char *basename, int n_base)
{
    int k, i, index;
    (void)n_file;
    (void)n_base;

    if (filename == NULL || basename == NULL)
    {
        return -1;
    }

    for (k = strlen(filename); k >= 0  && filename[k] != '.' ; k--);

    k = (k > 0) ? k - 1 : k;

    for (; k >= 0 && isdigit(filename[k]); k--);

    if (sscanf(filename + k + 1, "%d", &index) != 1)
    {
        index = -1;
    }

    k++;

    for (i = 0; i < k; i++)
    {
        basename[i] = filename[i];
    }

    basename[k] = 0;
    return index;
}

int general_op_post_display(struct one_plot *op, DIALOG *d)
{
    int i, j;
    BITMAP *imb = NULL;
    d_s *ds = NULL;
    //int na = 0, nb = 0, xc, yc;
    int  nb, xc, yc;
    //float f;

    if (d == NULL || d->dp == NULL || d->proc == NULL)
    {
        return 1;
    }

    if (imr_and_pr)
    {
        return 0;
    }

    ds = op->dat[0];

    if (ds == NULL || ds->source == NULL)
    {
        return 0;
    }

    //  if (sscanf(ds->source,"Bead tracking at %f Hz Acquistion %d for bead %d"
    //     ,&f,&na,&nb) != 3) return 0;
    nb = op->user_ispare[ISPARE_BEAD_NB];

    if (nb >= 0 && nb  < track_info->n_b)
    {
        imb = (BITMAP *)oi_TRACK->bmp.stuff;

        if (oi_TRACK->need_to_refresh & BITMAP_NEED_REFRESH)
        {
            display_image_stuff_16M(imr_TRACK, d_TRACK);
            oi_TRACK->need_to_refresh |= INTERNAL_BITMAP_NEED_REFRESH;
            show_bead_cross(imb, imr_TRACK, d_TRACK);
        }

        for (i = nb - 1, j = 0; (i < nb + 2) && (j < track_info->n_b) && (j < 3); i++, j++)
        {
            xc = track_info->bd[(i + track_info->n_b) % track_info->n_b]->x0 - 100;
            yc = oi_TRACK->im.ny - track_info->bd[(i + track_info->n_b) % track_info->n_b]->y0 - 100;
            acquire_bitmap(plt_buffer);
            blit(imb, plt_buffer, xc, yc, SCREEN_W - 300, 160 + 256 * j, 200, 200);
            release_bitmap(plt_buffer);
        }
    }

    return 0;
}


int timing_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
    int i, j, k;
    static int  immax = -1; // last_c_i = -1,
    d_s *dsx = NULL, *dsy = NULL, *dsz = NULL;
    int nf,  lf, jstart = 0, j1 = 0;
    unsigned long   dtm, dtmax, dts;
    float   fdtm, fdtmax;
    double tmp, tmp2;//min_delay_camera_thread_in_ms,
    long long llt;
    (void)d;

    if (track_info == NULL)
    {
        return D_O_K;
    }

    dts = get_my_uclocks_per_sec();
    dsx = find_source_specific_ds_in_op(op, "Timing rolling buffer");
    dsy = find_source_specific_ds_in_op(op, "Period rolling buffer");
    dsz = find_source_specific_ds_in_op(op, "Timer rolling buffer");

    if (dsx == NULL || dsy == NULL || dsz == NULL)  return D_O_K;
    i = track_info->c_i;
    if (track_info->imi[i] <= immax)
    {
        my_set_window_title("waiting %d %d", track_info->imi[i], immax);
        return D_O_K;   // we are up todate
    }
    if (op->xu[op->c_xu]->type == IS_SECOND) op->xu[op->c_xu]->dx = pow(10, op->xu[op->c_xu]->decade) / Pico_param.camera_param.camera_frequency_in_Hz;

    i = track_info->c_i;
    immax = track_info->imi[i];
    lf = immax - track_info->ac_i;
    if (track_info->ac_i > (TRACKING_BUFFER_SIZE >> 1))
    {
        nf = TRACKING_BUFFER_SIZE >> 1;
        j = jstart = (track_info->ac_i - (TRACKING_BUFFER_SIZE >> 1)) % TRACKING_BUFFER_SIZE; //last_c_i;
    }
    else
    {
        nf = track_info->ac_i - 1;
        j = jstart = 1;
    }
    dsx->nx = dsx->ny = nf;
    dsy->nx = dsy->ny = nf;
    dsz->nx = dsz->ny = nf;
    j = jstart + nf - 1;
    j = (j < TRACKING_BUFFER_SIZE) ? j : j - TRACKING_BUFFER_SIZE;
    llt = track_info->imt[j];
    j1 = j;
    j = jstart;
    j = (j < TRACKING_BUFFER_SIZE) ? j : j - TRACKING_BUFFER_SIZE;
    llt -= track_info->imt[j];
    llt /= (nf - 1);

    for (i = 0, j = jstart, dtm = dtmax = 0, fdtm = fdtmax = 0, k = 0, tmp2 = 0, lf = 0; i < nf; i++, j++, k++)
    {
        j = (j < TRACKING_BUFFER_SIZE) ? j : j - TRACKING_BUFFER_SIZE;
        dsx->xd[i] = dsy->xd[i] = dsz->xd[i] = track_info->imi[j];
        if (i > 0) lf += track_info->imi[j] - track_info->imi[j1] - 1;
        dsx->yd[i] = ((float)(1000 * track_info->imdt[j])) / dts;
        tmp = track_info->imt[j];
        tmp -= (i == 0) ? track_info->imt[j] : track_info->imt[j1];
        tmp = 1000 * (double)(tmp) / get_my_ulclocks_per_sec();
        //tmp -= bid.image_per_in_ms * i;
        tmp2 += tmp;
        dsy->yd[i] = (float)tmp;
        dsz->yd[i] = track_info->imit[j];
        dtm += track_info->imdt[j];
        fdtm += dsx->yd[i];
        dtmax = (track_info->imdt[j] > dtmax) ? track_info->imdt[j] : dtmax;
        fdtmax = (dsx->yd[i] > fdtmax) ? dsx->yd[i] : fdtmax;
        j1 = j;
    }

    dsy->yd[0] =  dsy->yd[1];
    op->need_to_refresh = 1;

    if (k) set_plot_title(op, "\\pt8\\stack{{fr position lac_i %d }"//{%d missed images}",lf,
                              "{dt mean %6.3f ms (%6.3f ms) max %6.3f (%6.3f)}{load %3.1f%% Freq. %g (asked %g)}}"
                              , bid.first_im + track_info->ac_i
                              ,  fdtm / k,
                              1000 * ((double)(dtm / k)) / dts,
                              fdtmax, 1000 * ((double)(dtmax)) / dts,
                              Pico_param.camera_param.camera_frequency_in_Hz * fdtm / (k * 10),
                              get_my_ulclocks_per_sec() / ((double)llt), Pico_param.camera_param.camera_frequency_in_Hz);
    i = TRACKING_BUFFER_SIZE / 8;
    j = ((int)dsx->xd[1]) / i;
    op->x_lo = j * i;
    op->x_hi = (j + 5) * i;
    set_plot_x_fixed_range(op);
    return refresh_plot(pr_TRACK, UNCHANGED);
}



int temperature_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
    int i, j, k;
    static int  immax = -1; // last_c_i = -1,
    d_s *dsx = NULL, *dsy = NULL, *dsz = NULL;
    int nf, prev = -1;
    float  T0 = 0, T1 = 0, T2 = 0;
    float T3=0;
    char l_mes[32]  = {0};
    int lmi = 0;
    (void)d;

    if (track_info == NULL)
    {
        return D_O_K;
    }

    dsx = find_source_specific_ds_in_op(op, "Sensor temperature");
    dsy = find_source_specific_ds_in_op(op, "Sample temperature");
    dsz = find_source_specific_ds_in_op(op, "Heat sink temperature");
#ifdef RAWHID_V2

    d_s *dsw=NULL;
    dsw = find_source_specific_ds_in_op(op, "Magnets temperature");
    if (dsw == NULL) return D_O_K;
#endif
    if (dsx == NULL || dsy == NULL || dsz == NULL)
    {
        return D_O_K;
    }

    if (op->xu[op->c_xu]->type == IS_SECOND)
    {
        op->xu[op->c_xu]->dx = pow(10, op->xu[op->c_xu]->decade) / Pico_param.camera_param.camera_frequency_in_Hz;
    }

    i = track_info->lc_i;

    if (track_info->limi[i] <= immax)
    {
        return D_O_K;    // we are up todate
    }

    if (track_info->local_lock & BUF_CHANGING)
    {
        return D_O_K;    // we have to wait
    }

    track_info->local_lock |= BUF_FREEZED; // we forbid buffer changes
    i = track_info->lc_i;
    immax = track_info->limi[i];

    if (track_info->lac_i > TRACKING_BUFFER_SIZE)
    {
        nf = TRACKING_BUFFER_SIZE;
        j = track_info->lc_i + 1;  //last_c_i;
    }
    else
    {
        nf = track_info->lac_i - 1;
        j = 1;
    }

    dsx->nx = dsx->ny = 0;
    dsy->nx = dsy->ny = 0;
    dsz->nx = dsz->ny = 0;
#ifdef RAWHID_V2
    dsw->nx = dsw->ny = 0;
#endif

    for (i = 0, k = 0, prev = 0, lmi = 0; i < nf; i++, j++, k++)
    {
        j = (j < TRACKING_BUFFER_SIZE) ? j : j - TRACKING_BUFFER_SIZE;

        if (track_info->temp_message[j] == 0)
        {
            if (prev != 0 && lmi > 3)
            {
                if (l_mes[0] == 'T')
                {
                    if (l_mes[1] == '0' && sscanf(l_mes + 3, "%f", &T0) == 1)
                    {
                        dsx->xd[dsx->nx++] = track_info->limi[j];
                        dsx->yd[dsx->ny++] = T0;
                    }
                    else if (l_mes[1] == '1' && sscanf(l_mes + 3, "%f", &T1) == 1)
                    {
                        dsy->xd[dsy->nx++] = track_info->limi[j];
                        dsy->yd[dsy->ny++] = T1;
                    }
                    else if (l_mes[1] == '2' && sscanf(l_mes + 3, "%f", &T2) == 1)
                    {
                        dsz->xd[dsz->nx++] = track_info->limi[j];
                        dsz->yd[dsz->ny++] = T2;
                    }
#ifdef RAWHID_V2
                    else if (l_mes[1] == '3' && sscanf(l_mes + 3, "%f", &T3) == 1)
                    {
                        dsw->xd[dsw->nx++] = track_info->limi[j];
                        dsw->yd[dsw->ny++] = T3;
                    }
#endif
                }

                lmi = 0;
            }

            prev = 0;
        }
        else
        {
            prev = 1;

            if (lmi < 32)
            {
                l_mes[lmi++] = track_info->temp_message[j];
            }
        }
    }

    op->need_to_refresh = 1;

    if (k)
    {
        set_plot_title(op, "T0 %5.3f T1 %5.3f T2 %5.3f T3 %5.3f", T0, T1, T2, T3);
    }

    track_info->local_lock = BUF_UNLOCK; // we allow new buffer changes
    return refresh_plot(pr_TRACK, UNCHANGED);
}



int z_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
    int i, j, k;
    static int last_c_i = -1, immax = -1;
    d_s *dsx = NULL, *dsy = NULL, *dsz = NULL, *dsxcor = NULL, *dsycor = NULL, *dsxnoi = NULL, *dsynoi = NULL;
#ifdef GAME
    d_s *dsxsim = NULL, *dsysim = NULL, *dszsim = NULL;
#endif
    int nf, last_fr = 0, lost_fr, nl, n_bin = 16;
    unsigned long  dtm, dtmax;
    float ax, dx, ay, dy, y_over_x, y_over_z;
    double tmp, mean, meanb, y2b;
    float erx = 0, ery = 0, z;
    float m_nerx, m_nery, m_derivx, m_derivy;
    un_s *un = NULL;
    (void)d;

    if (track_info == NULL)
    {
        return D_O_K;
    }

    un = oi_TRACK->xu[oi_TRACK->c_xu];
    get_afine_param_from_unit(un, &ax, &dx);
    un = oi_TRACK->yu[oi_TRACK->c_yu];
    get_afine_param_from_unit(un, &ay, &dy);
    y_over_x = (dy != 0) ? dx / dy : 1;
    y_over_z = track_info->focus_cor * 1e-6;
    y_over_z = (dy != 0) ? y_over_z / dy : 1;
    dsx = find_source_specific_ds_in_op(op, "X rolling buffer");
    dsy = find_source_specific_ds_in_op(op, "Y rolling buffer");
    dsz = find_source_specific_ds_in_op(op, "Z rolling buffer");
#ifdef GAME
    dsxsim = find_source_specific_ds_in_op(op, "X rolling buffer of simulation");
    dsysim = find_source_specific_ds_in_op(op, "Y rolling buffer of simulation");
    dszsim = find_source_specific_ds_in_op(op, "Z rolling buffer of simulation");

    if (!dsxsim || !dsysim || !dszsim)
    {
        return D_O_K;
    }

#endif
    dsxcor = find_source_specific_ds_in_op(op, "Xcor rolling buffer");
    dsycor = find_source_specific_ds_in_op(op, "Ycor rolling buffer");
    dsxnoi = find_source_specific_ds_in_op(op, "Xnoise rolling buffer");
    dsynoi = find_source_specific_ds_in_op(op, "Ynoise rolling buffer");

    if (dsx == NULL || dsy == NULL || dsz == NULL || dsxcor == NULL || dsycor == NULL)
    {
        return D_O_K;
    }

    /*
       if (sscanf(dsx->source,"X rolling buffer bead %d",&bead_num) != 1)
       return win_printf_OK("Cannot find bead number");
       */
    if (track_info->n_b < 1)
    {
        return D_O_K;    //win_printf_OK("No bead to display");
    }

    i = track_info->c_i;

    if (track_info->imi[i] <= immax)
    {
        return D_O_K;    // we are uptodate
    }

    if (op->xu[op->c_xu]->type == IS_SECOND)
    {
        op->xu[op->c_xu]->dx = pow(10, op->xu[op->c_xu]->decade) / Pico_param.camera_param.camera_frequency_in_Hz;
    }

    immax = track_info->imi[i];
    nf = (track_info->ac_i > TRACKING_BUFFER_SIZE) ? TRACKING_BUFFER_SIZE : track_info->ac_i;
    nf = (nf > (TRACKING_BUFFER_SIZE >> 1)) ? TRACKING_BUFFER_SIZE >> 1 : nf;
    dsx->nx = dsx->ny = dsy->nx = dsy->ny = dsz->nx = dsz->ny = nf;
    dsxcor->nx = dsxcor->ny = dsycor->nx = dsycor->ny = nf;
    dsxnoi->nx = dsxnoi->ny = dsynoi->nx = dsynoi->ny = nf;
    last_c_i = (track_info->c_i > 0) ? track_info->c_i - 1 : 0;  // c_i is not yet valid

    for (i = nf - 1, j = last_c_i, lost_fr = 0, dtm = dtmax = 0, k = 0; i >= 0; i--, j--, k++)
    {
        j = (j < 0) ? TRACKING_BUFFER_SIZE + j : j;
        j = (j < TRACKING_BUFFER_SIZE) ? j : TRACKING_BUFFER_SIZE;
        dsx->xd[i] = dsy->xd[i] = dsz->xd[i] = dsxcor->xd[i] = dsycor->xd[i] = track_info->imi[j];
#ifdef GAME
        dsxsim->nx = dsxsim->ny = dsysim->nx = dsysim->ny = dszsim->nx = dszsim->ny = nf;
        dsxsim->xd[i] = dsysim->xd[i] = dszsim->xd[i] = track_info->imi[j];
#endif
        dsxnoi->xd[i] = dsynoi->xd[i] = track_info->imi[j];
        dsx->yd[i] = y_over_x * track_info->bd[track_info->n_bead_display]->x[j];
        dsy->yd[i] = track_info->bd[track_info->n_bead_display]->y[j];

        if (track_info->evanescent_mode <= 0)
        {
            dsz->yd[i] = y_over_z * track_info->bd[track_info->n_bead_display]->z[j];
        }
        else
        {
            z = track_info->bd[track_info->n_bead_display]->z[j];
            z = (z < 0) ? -z : z;
            dsz->yd[i] = y_over_z * (track_info->eva_offset - track_info->eva_decay * log(z));
        }

#ifdef GAME
        dsxsim->yd[i] = track_info->bd[track_info->n_bead_display]->xt[j] * 10;
        dsysim->yd[i] = track_info->bd[track_info->n_bead_display]->yt[j] * 10;
        dszsim->yd[i] = track_info->bd[track_info->n_bead_display]->zt[j] * 10;
#endif
        dsxcor->yd[i] = track_info->bd[track_info->n_bead_display]->dcor_dx[j];
        dsycor->yd[i] = track_info->bd[track_info->n_bead_display]->dcor_dy[j];
        dsxnoi->yd[i] = track_info->bd[track_info->n_bead_display]->xnoise[j];
        dsynoi->yd[i] = track_info->bd[track_info->n_bead_display]->ynoise[j];

        if (i < nf - 1)
        {
            lost_fr += (last_fr - track_info->imi[j]) - 1;
        }

        last_fr = track_info->imi[j];
        dtm += track_info->imdt[j];
        dtmax = (track_info->imdt[j] > dtmax) ? track_info->imdt[j] : dtmax;
    }

    op->need_to_refresh = 1;

    for (i = 0, nl = 0, k = 0, mean = y2b = 0; k < nf / n_bin; i += n_bin, k++)
    {
        for (j = 0, meanb = 0; (j < n_bin) && ((i + j) < nf); j++)
        {
            meanb += dsxcor->yd[i + j];
        }

        mean += meanb;
        meanb /= (j > 0) ? j : 1;

        for (j = 0; (j < n_bin) && ((i + j) < nf); j++)
        {
            tmp = dsxcor->yd[i + j] - meanb;
            y2b += tmp * tmp;
        }

        nl += (j > 1) ? j - 1 : 1;
    }

    if (nl > 0)
    {
        y2b /= nl;
    }

    if (nf > 0)
    {
        mean /= nf;
    }

    if (mean != 0)
    {
        erx = sqrt(y2b) / mean;
    }

    erx *= dx * 1e9;

    for (i = 0, nl = 0, k = 0, mean = y2b = 0; k < nf / n_bin; i += n_bin, k++)
    {
        for (j = 0, meanb = 0; (j < n_bin) && ((i + j) < nf); j++)
        {
            meanb += dsycor->yd[i + j];
        }

        mean += meanb;
        meanb /= (j > 0) ? j : 1;

        for (j = 0; (j < n_bin) && ((i + j) < nf); j++)
        {
            tmp = dsycor->yd[i + j] - meanb;
            y2b += tmp * tmp;
        }

        nl += (j > 1) ? j - 1 : 1;
    }

    if (nl > 0)
    {
        y2b /= nl;
    }

    if (nf > 0)
    {
        mean /= nf;
    }

    if (mean != 0)
    {
        ery = sqrt(y2b) / mean;
    }

    ery *= dy * 1e9;

    for (k = nf - 64, m_nerx = m_nery = m_derivx = m_derivy = 0, j = 0; k < nf;  k++)
    {
        if (k < 0)
        {
            continue;
        }

        m_nerx += dsxnoi->yd[k] * dsxnoi->yd[k];
        m_nery += dsynoi->yd[k] * dsynoi->yd[k];
        m_derivx += dsxcor->yd[k];
        m_derivy += dsxcor->yd[k];
        j++;
    }

    if (j)
    {
        m_nerx /= j;
        m_nery /= j;
        m_derivx /= j;
        m_derivy /= j;
    }

    m_nerx = sqrt(m_nerx);
    m_nery = sqrt(m_nery);
    m_derivx = fabs(2 * m_derivx);
    m_derivy = fabs(2 * m_derivy);
    m_nerx = (m_derivx > 0) ? m_nerx / m_derivx : 0;
    m_nery = (m_derivy > 0) ? m_nery / m_derivy : 0;
    m_nerx *= dx * 1e9;
    m_nery *= dy * 1e9;

    if (k) set_plot_title(op, "\\stack{{Bead %d, %d missed images}"
                              "{\\pt8 dt mean %6.3f ms max %6.3f}"
                              //"{\\pt8 tracking error in x %5.2f nm -> %5.2f nm.Hz^{1/2}}"
                              //"{\\pt8 tracking error in y %5.2f nm -> %5.2f nm.Hz^{1/2}}"
                              "{\\pt8 Max conv x %5.4f Max conv y %5.4f }"
                              "{\\pt8 New tracking error in x %5.3f nm -> %5.2f nm.Hz^{1/2}}"
                              "{\\pt8 New tracking error in y %5.3f nm -> %5.2f nm.Hz^{1/2}}}"
                              //"{dx %g dy %g y2b %g mean %g meanb %g}}",
                              , track_info->n_bead_display, lost_fr, 1000 * ((double)(dtm / k)) / get_my_uclocks_per_sec(),
                              1000 * ((double)(dtmax)) / get_my_uclocks_per_sec()
                              //,erx, erx/sqrt(Pico_param.camera_param.camera_frequency_in_Hz/2)
                              //,ery, ery/sqrt(Pico_param.camera_param.camera_frequency_in_Hz/2)
                              , track_info->bd[track_info->n_bead_display]->xmax
                              , track_info->bd[track_info->n_bead_display]->ymax
                              , m_nerx, m_nerx / sqrt(Pico_param.camera_param.camera_frequency_in_Hz / 2)
                              , m_nery, m_nery / sqrt(Pico_param.camera_param.camera_frequency_in_Hz / 2)
                              //,dx//track_info->bd[track_info->n_bead_display]->xmax
                              //,dy//track_info->bd[track_info->n_bead_display]->ymax
                              //,y2b,mean, meanb
                             );

    i = TRACKING_BUFFER_SIZE / 8;
    //i = 512;
    j = ((int)dsx->xd[i]) / i;
    op->x_lo = (j - 1) * i;
    op->x_hi = (j + 4) * i;
    set_plot_x_fixed_range(op);
    op->user_ispare[ISPARE_BEAD_NB] = track_info->n_bead_display;
    return refresh_plot(pr_TRACK, UNCHANGED);
}


int obj_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
    int i, j;
    static int last_c_i = -1, immax = -1;
    d_s *dsx = NULL, *dsz = NULL;
    int nf;
    float z;
    (void)d;

    if (track_info == NULL)
    {
        return D_O_K;
    }

    dsx = find_source_specific_ds_in_op(op, "Obj. rolling buffer");
    dsz = find_source_specific_ds_in_op(op, "Z rolling buffer");

    if (dsx == NULL || dsz == NULL)
    {
        return D_O_K;
    }

    if (track_info->n_b < 1)
    {
        return D_O_K;    //win_printf_OK("No bead to display");
    }

    i = track_info->c_i;

    if (track_info->imi[i] <= immax)
    {
        return D_O_K;    // we are uptodate
    }

    if (op->xu[op->c_xu]->type == IS_SECOND)
    {
        op->xu[op->c_xu]->dx = pow(10, op->xu[op->c_xu]->decade) / Pico_param.camera_param.camera_frequency_in_Hz;
    }

    immax = track_info->imi[i];
    nf = (track_info->ac_i > TRACKING_BUFFER_SIZE) ? TRACKING_BUFFER_SIZE : track_info->ac_i;
    nf = (nf > (TRACKING_BUFFER_SIZE >> 1)) ? TRACKING_BUFFER_SIZE >> 1 : nf;
    dsx->nx = dsx->ny = dsz->nx = dsz->ny = nf;

    if (track_info->bd[track_info->n_bead_display]->in_image == 0)
    {
        dsz->nx = dsz->ny = 0;
    }

    last_c_i = (track_info->c_i > 0) ? track_info->c_i - 1 : 0;  // c_i is not yet valid

    for (i = nf - 1, j = last_c_i; i >= 0; i--, j--)
    {
        j = (j < 0) ? TRACKING_BUFFER_SIZE + j : j;
        j = (j < TRACKING_BUFFER_SIZE) ? j : TRACKING_BUFFER_SIZE;
        dsx->xd[i] = dsz->xd[i] = track_info->imi[j];
        dsx->yd[i] = track_info->obj_pos[j];

        if (track_info->evanescent_mode <= 0)
        {
            dsz->yd[i] = track_info->focus_cor * track_info->bd[track_info->n_bead_display]->z[j];
        }
        else
        {
            z = track_info->bd[track_info->n_bead_display]->z[j];
            z = (z < 0) ? -z : z;
            dsz->yd[i] = track_info->eva_offset - track_info->eva_decay * log(z);
        }
    }

    i = TRACKING_BUFFER_SIZE / 8;
    j = ((int)dsx->xd[i]) / i;
    op->x_lo = (j - 1) * i;
    op->x_hi = (j + 4) * i;
    set_plot_x_fixed_range(op);
    op->need_to_refresh = 1;

    if (track_info->evanescent_mode <= 0)
    {
        z = track_info->focus_cor * track_info->bd[track_info->n_bead_display]->z[j];
    }
    else
    {
        z = track_info->bd[track_info->n_bead_display]->z[j];
        z = (z < 0) ? -z : z;
        z = track_info->eva_offset - track_info->eva_decay * log(z);
    }

    set_plot_title(op, "\\stack{{image %d %s}{Obj start %g obj %g}{profile %d z = %g}}"
                   , track_info->ac_i, (track_info->bead_servo_nb >= 0) ? "bead servo" : "",
                   track_info->z_obj_servo_start, track_info->obj_pos[last_c_i],
                   track_info->bd[track_info->n_bead_display]->profile_index, z);
    op->user_ispare[ISPARE_BEAD_NB] = track_info->n_bead_display;
    return refresh_plot(pr_TRACK, UNCHANGED);
}

int bead_count_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
    int i, j;
    static int last_c_i = -1, immax = -1;
    d_s *dsx = NULL, *dsy = NULL, *dsz = NULL; //, *dssf;
    int nf;
    (void)d;

    if (track_info == NULL)
    {
        return D_O_K;
    }

    dsx = find_source_specific_ds_in_op(op, "Zmag rolling buffer");
    dsy = find_source_specific_ds_in_op(op, "Rotation rolling buffer");
    dsz = find_source_specific_ds_in_op(op, "Beads count");

    if (dsx == NULL || dsy == NULL || dsz == NULL)
    {
        return D_O_K;
    }

    i = track_info->c_i;

    if (track_info->imi[i] <= immax)
    {
        return D_O_K;    // we are uptodate
    }

    if (op->xu[op->c_xu]->type == IS_SECOND)
    {
        op->xu[op->c_xu]->dx = pow(10, op->xu[op->c_xu]->decade) / Pico_param.camera_param.camera_frequency_in_Hz;
    }

    if (track_info->n_b < 1)
    {
        return D_O_K;    //win_printf_OK("No bead to display");
    }

    immax = track_info->imi[i];
    nf = (track_info->ac_i > TRACKING_BUFFER_SIZE) ? TRACKING_BUFFER_SIZE : track_info->ac_i;
    nf = (nf > (TRACKING_BUFFER_SIZE >> 1)) ? TRACKING_BUFFER_SIZE >> 1 : nf;
    //dsx->nx = dsx->ny = dsy->nx = dsy->ny = dsz->nx = dsz->ny = dssf->nx = dssf->ny = nf;
    dsx->nx = dsx->ny = dsy->nx = dsy->ny = dsz->nx = dsz->ny = nf;

    if (track_info->bd[track_info->n_bead_display]->in_image == 0)
    {
        dsz->nx = dsz->ny = 0;
    }

    last_c_i = (track_info->c_i > 0) ? track_info->c_i - 1 : 0;  // c_i is not yet valid

    for (i = nf - 1, j = last_c_i; i >= 0; i--, j--)
    {
        j = (j < 0) ? TRACKING_BUFFER_SIZE + j : j;
        j = (j < TRACKING_BUFFER_SIZE) ? j : TRACKING_BUFFER_SIZE;
        dsx->xd[i] = dsy->xd[i] = dsz->xd[i] = track_info->imi[j];
        //dssf->xd[i] = track_info->imi[j];
        dsx->yd[i] = track_info->zmag[j];
        dsy->yd[i] = track_info->rot_mag[j];
        dsz->yd[i] = track_info->nb_bd_in_view[j];
    }

    i = TRACKING_BUFFER_SIZE / 8;
    j = ((int)dsx->xd[i]) / i;
    op->x_lo = (j - 1) * i;
    op->x_hi = (j + 4) * i;
    set_plot_x_fixed_range(op);
    op->need_to_refresh = 1;
    set_plot_title(op, "\\stack{{image %d bead servo %d}{Obj start %g obj %g}{profile %d Z = %g}}"
                   , track_info->ac_i,  track_info->bead_servo_nb,
                   track_info->z_obj_servo_start,
                   track_info->obj_pos[last_c_i], track_info->bd[track_info->n_bead_display]->profile_index,
                   track_info->focus_cor * track_info->bd[track_info->n_bead_display]->z[last_c_i]);
    return refresh_plot(pr_TRACK, UNCHANGED);
}

int magnets_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
    int i, j;
    static int last_c_i = -1, immax = -1;
    d_s *dsx = NULL, *dsy = NULL, *dsz = NULL; //, *dssf;
    d_s *dsv = NULL, *dsl = NULL;
    int nf;
    (void)d;

    if (track_info == NULL)
    {
        return D_O_K;
    }

    dsx = find_source_specific_ds_in_op(op, "Zmag rolling buffer");
    dsy = find_source_specific_ds_in_op(op, "Rotation rolling buffer");
    dsz = find_source_specific_ds_in_op(op, "Z rolling buffer");
    dsv = find_source_specific_ds_in_op(op, "Inductive sensor rolling buffer");
    dsl = find_source_specific_ds_in_op(op, "Limit switch rolling buffer");

    //dssf = find_source_specific_ds_in_op(op,"Status flag rolling buffer");
    //if (dsx == NULL || dsy == NULL || dsz == NULL || dssf == NULL) return D_O_K;
    if (dsx == NULL || dsy == NULL || dsz == NULL)
    {
        return D_O_K;
    }


    if (track_info->n_b < 1)
    {
        return D_O_K;    //win_printf_OK("No bead to display");
    }

    i = track_info->c_i;

    if (track_info->imi[i] <= immax)
    {
        return D_O_K;    // we are uptodate
    }

    if (op->xu[op->c_xu]->type == IS_SECOND)
    {
        op->xu[op->c_xu]->dx = pow(10, op->xu[op->c_xu]->decade) / Pico_param.camera_param.camera_frequency_in_Hz;
    }

    immax = track_info->imi[i];
    nf = (track_info->ac_i > TRACKING_BUFFER_SIZE) ? TRACKING_BUFFER_SIZE : track_info->ac_i;
    nf = (nf > (TRACKING_BUFFER_SIZE >> 1)) ? TRACKING_BUFFER_SIZE >> 1 : nf;
    //dsx->nx = dsx->ny = dsy->nx = dsy->ny = dsz->nx = dsz->ny = dssf->nx = dssf->ny = nf;
    dsx->nx = dsx->ny = dsy->nx = dsy->ny = dsz->nx = dsz->ny = nf;

    if (dsv)
    {
        dsv->nx = dsv->ny = nf;
    }

    if (dsl)
    {
        dsl->nx = dsl->ny = nf;
    }

    if (track_info->bd[track_info->n_bead_display]->in_image == 0)
    {
        dsz->nx = dsz->ny = 0;
    }

    last_c_i = (track_info->c_i > 0) ? track_info->c_i - 1 : 0;  // c_i is not yet valid

    for (i = nf - 1, j = last_c_i; i >= 0; i--, j--)
    {
        j = (j < 0) ? TRACKING_BUFFER_SIZE + j : j;
        j = (j < TRACKING_BUFFER_SIZE) ? j : TRACKING_BUFFER_SIZE;
        dsx->xd[i] = dsy->xd[i] = dsz->xd[i] = track_info->imi[j];
        //dssf->xd[i] = track_info->imi[j];
        dsx->yd[i] = track_info->zmag[j];
        dsy->yd[i] = track_info->rot_mag[j];
        dsz->yd[i] = track_info->focus_cor * track_info->bd[track_info->n_bead_display]->z[j];

        if (dsv)
        {
            dsv->xd[i] = track_info->imi[j];
            dsv->yd[i] = track_info->Vcap_zmag[j];
        }

        if (dsl)
        {
            dsl->xd[i] = track_info->imi[j];

            if (track_info->status_flag[j] & POSITIVE_LIMIT_ON)
            {
                dsl->yd[i] = 1;
            }
            else if (track_info->status_flag[j] & NEGATIVE_LIMIT_ON)
            {
                dsl->yd[i] = 2;
            }
            else
            {
                dsl->yd[i] = 0;
            }

            if (track_info->status_flag[j] & ZMAG_MOVING_SAMPLED)
            {
                dsl->yd[i] += 8;
            }

            if (track_info->status_flag[j] & ZMAG_MOVING)
            {
                dsl->yd[i] += 4;
            }
        }

        //dssf->yd[i] = track_info->status_flag[j];
    }

    i = TRACKING_BUFFER_SIZE / 8;
    j = ((int)dsx->xd[i]) / i;
    op->x_lo = (j - 1) * i;
    op->x_hi = (j + 4) * i;
    set_plot_x_fixed_range(op);
    op->need_to_refresh = 1;
    set_plot_title(op, "\\stack{{image %d bead servo %d}{Obj start %g obj %g}{profile %d Z = %g}}"
                   , track_info->ac_i,  track_info->bead_servo_nb,
                   track_info->z_obj_servo_start,
                   track_info->obj_pos[last_c_i], track_info->bd[track_info->n_bead_display]->profile_index,
                   track_info->focus_cor * track_info->bd[track_info->n_bead_display]->z[last_c_i]);
    return refresh_plot(pr_TRACK, UNCHANGED);
}

int angle_profile_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
    int i, j;
    static int last_c_i = -1, immax = -1;
    d_s *dsx = NULL, *dsy = NULL;
    int nf;
    b_track *bd = NULL;
    (void)d;

    if (track_info == NULL)
    {
        return D_O_K;
    }

    dsx = find_source_specific_ds_in_op(op, "Angle rolling buffer");
    dsy = find_source_specific_ds_in_op(op, "Rotation rolling buffer");

    if (dsx == NULL || dsy == NULL)
    {
        return D_O_K;
    }

    if (track_info->n_b < 1)
    {
        return D_O_K;    //win_printf_OK("No bead to display");
    }

    i = track_info->c_i;

    if (track_info->imi[i] <= immax)
    {
        return D_O_K;    // we are uptodate
    }

    immax = track_info->imi[i];

    if (op->xu[op->c_xu]->type == IS_SECOND)
    {
        op->xu[op->c_xu]->dx = pow(10, op->xu[op->c_xu]->decade) / Pico_param.camera_param.camera_frequency_in_Hz;
    }

    bd = track_info->bd[track_info->n_bead_display];
    nf = (track_info->ac_i > TRACKING_BUFFER_SIZE) ? TRACKING_BUFFER_SIZE : track_info->ac_i;
    nf = (nf > (TRACKING_BUFFER_SIZE >> 1)) ? TRACKING_BUFFER_SIZE >> 1 : nf;
    dsx->nx = dsx->ny = dsy->nx = dsy->ny = nf;
    last_c_i = (track_info->c_i > 0) ? track_info->c_i - 1 : 0;  // c_i is not yet valid

    for (i = nf - 1, j = last_c_i; i >= 0; i--, j--)
    {
        j = (j < 0) ? TRACKING_BUFFER_SIZE + j : j;
        j = (j < TRACKING_BUFFER_SIZE) ? j : TRACKING_BUFFER_SIZE;
        dsx->xd[i] = dsy->xd[i] = track_info->imi[j];
        dsx->yd[i] = bd->theta[j] / (2 * M_PI);
        dsy->yd[i] = track_info->rot_mag[j];
    }

    i = TRACKING_BUFFER_SIZE / 8;
    j = ((int)dsx->xd[i]) / i;
    op->x_lo = (j - 1) * i;
    op->x_hi = (j + 4) * i;
    set_plot_x_fixed_range(op);
    op->need_to_refresh = 1;
    set_plot_title(op, "\\stack{{image %d bead servo %d}{Angle tracking by orthoradial profile}"
                   "{profile size %d k_{\\theta} %d}}"
                   , track_info->ac_i,  track_info->bead_servo_nb,
                   bd->orthoradial_prof_size, bd->kx_angle);
    return refresh_plot(pr_TRACK, UNCHANGED);
}



int spot_light_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
    int i, j;
    static int last_c_i = -1, immax = -1;
    d_s *dsx = NULL;
    int nf;
    b_track *bd = NULL;
    (void)d;

    if (track_info == NULL)
    {
        return D_O_K;
    }

    dsx = find_source_specific_ds_in_op(op, "spot light rolling buffer");

    if (dsx == NULL)
    {
        return D_O_K;
    }

    if (track_info->n_b < 1)
    {
        return D_O_K;    //win_printf_OK("No bead to display");
    }

    i = track_info->c_i;

    if (track_info->imi[i] <= immax)
    {
        return D_O_K;    // we are uptodate
    }

    immax = track_info->imi[i];

    if (op->xu[op->c_xu]->type == IS_SECOND)
    {
        op->xu[op->c_xu]->dx = pow(10, op->xu[op->c_xu]->decade) / Pico_param.camera_param.camera_frequency_in_Hz;
    }

    bd = track_info->bd[track_info->n_bead_display];
    nf = (track_info->ac_i > TRACKING_BUFFER_SIZE) ? TRACKING_BUFFER_SIZE : track_info->ac_i;
    nf = (nf > (TRACKING_BUFFER_SIZE >> 1)) ? TRACKING_BUFFER_SIZE >> 1 : nf;
    dsx->nx = dsx->ny = nf;
    last_c_i = (track_info->c_i > 0) ? track_info->c_i - 1 : 0;  // c_i is not yet valid

    for (i = nf - 1, j = last_c_i; i >= 0; i--, j--)
    {
        j = (j < 0) ? TRACKING_BUFFER_SIZE + j : j;
        j = (j < TRACKING_BUFFER_SIZE) ? j : TRACKING_BUFFER_SIZE;
        dsx->xd[i] = track_info->imi[j];
        dsx->yd[i] = bd->spot_light[j];
    }

    i = TRACKING_BUFFER_SIZE / 8;
    j = ((int)dsx->xd[i]) / i;
    op->x_lo = (j - 1) * i;
    op->x_hi = (j + 4) * i;
    set_plot_x_fixed_range(op);
    op->need_to_refresh = 1;
    set_plot_title(op,
                   "\\stack{{image %d bead servo %d}{Light intensity of bead over %dx%d}{Nb of pixel eff %5.2f nb of black pixel %d}}"
                   , track_info->ac_i,  track_info->bead_servo_nb,
                   bd->cw, bd->cw, evanescent_avg_n_appo, evanescent_black_n_avg);
    return refresh_plot(pr_TRACK, UNCHANGED);
}



int x_y_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
    int i, j;
    static int last_c_i = -1, immax = -1;
    d_s *dsx = NULL;
    int nf;
    b_track *bd = NULL;
    (void)d;

    if (track_info == NULL)
    {
        return D_O_K;
    }

    dsx = find_source_specific_ds_in_op(op, "X VS Y rolling buffer");

    if (dsx == NULL)
    {
        return D_O_K;
    }

    if (track_info->n_b < 1)
    {
        return D_O_K;    //win_printf_OK("No bead to display");
    }

    i = track_info->c_i;

    if (track_info->imi[i] <= immax)
    {
        return D_O_K;    // we are uptodate
    }

    immax = track_info->imi[i];
    bd = track_info->bd[track_info->n_bead_display];
    nf = ((track_info->ac_i - bd->start_im) > (TRACKING_BUFFER_SIZE >> 1)) ?
         (TRACKING_BUFFER_SIZE >> 1) : track_info->ac_i - bd->start_im;
    dsx->nx = dsx->ny = nf;
    last_c_i = (track_info->c_i > 0) ? track_info->c_i - 1 : 0;  // c_i is not yet valid

    for (i = nf - 1, j = last_c_i; i >= 0; i--, j--)
    {
        j = (j < 0) ? TRACKING_BUFFER_SIZE + j : j;
        j = (j < TRACKING_BUFFER_SIZE) ? j : TRACKING_BUFFER_SIZE;
        dsx->xd[i] = bd->x[j];
        dsx->yd[i] = bd->y[j];
    }

    op->need_to_refresh = 1;
    set_plot_title(op, "\\stack{{image %d bead servo %d}{Obj start %g obj %g start %d}}"
                   , track_info->ac_i,  track_info->bead_servo_nb,
                   track_info->z_obj_servo_start,
                   track_info->obj_pos[last_c_i], bd->start_im);
    op->user_ispare[ISPARE_BEAD_NB] = track_info->n_bead_display;
    return refresh_plot(pr_TRACK, UNCHANGED);
}

int x_z_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
    int i, j;
    static int last_c_i = -1, immax = -1;
    float x_over_z, ax, dx;
    un_s *un = NULL;
    d_s *dsx = NULL;
    int nf;
    b_track *bd = NULL;
    (void)d;

    if (track_info == NULL)
    {
        return D_O_K;
    }

    un = oi_TRACK->xu[oi_TRACK->c_xu];
    get_afine_param_from_unit(un, &ax, &dx);
    x_over_z = track_info->focus_cor * 1e-6;
    x_over_z = (dx != 0) ? x_over_z / dx : 1;
    dsx = find_source_specific_ds_in_op(op, "X VS Z rolling buffer");

    if (dsx == NULL)
    {
        return D_O_K;
    }

    if (track_info->n_b < 1)
    {
        return D_O_K;    //win_printf_OK("No bead to display");
    }

    i = track_info->c_i;

    if (track_info->imi[i] <= immax)
    {
        return D_O_K;    // we are uptodate
    }

    immax = track_info->imi[i];
    bd = track_info->bd[track_info->n_bead_display];
    nf = ((track_info->ac_i - bd->start_im) > (TRACKING_BUFFER_SIZE >> 1)) ?
         (TRACKING_BUFFER_SIZE >> 1) : track_info->ac_i - bd->start_im;
    dsx->nx = dsx->ny = nf;
    last_c_i = (track_info->c_i > 0) ? track_info->c_i - 1 : 0;  // c_i is not yet valid

    for (i = nf - 1, j = last_c_i; i >= 0; i--, j--)
    {
        j = (j < 0) ? TRACKING_BUFFER_SIZE + j : j;
        j = (j < TRACKING_BUFFER_SIZE) ? j : TRACKING_BUFFER_SIZE;
        dsx->xd[i] = bd->x[j];
        dsx->yd[i] = x_over_z * bd->z[j];
    }

    op->need_to_refresh = 1;
    set_plot_title(op, "\\stack{{image %d bead servo %d}{Obj start %g obj %g start %d}}"
                   , track_info->ac_i,  track_info->bead_servo_nb,
                   track_info->z_obj_servo_start,
                   track_info->obj_pos[last_c_i], bd->start_im);
    op->user_ispare[ISPARE_BEAD_NB] = track_info->n_bead_display;
    return refresh_plot(pr_TRACK, UNCHANGED);
}

int y_z_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
    int i, j;
    static int last_c_i = -1, immax = -1;
    float y_over_z, ay, dy;
    un_s *un = NULL;
    d_s *dsx = NULL;
    int nf;
    b_track *bd = NULL;
    (void)d;

    if (track_info == NULL)
    {
        return D_O_K;
    }

    un = oi_TRACK->xu[oi_TRACK->c_yu];
    get_afine_param_from_unit(un, &ay, &dy);
    y_over_z = track_info->focus_cor * 1e-6;
    y_over_z = (dy != 0) ? y_over_z / dy : 1;
    dsx = find_source_specific_ds_in_op(op, "Y VS Z rolling buffer");

    if (dsx == NULL)
    {
        return D_O_K;
    }

    if (track_info->n_b < 1)
    {
        return D_O_K;    //win_printf_OK("No bead to display");
    }

    i = track_info->c_i;

    if (track_info->imi[i] <= immax)
    {
        return D_O_K;    // we are uptodate
    }

    immax = track_info->imi[i];
    bd = track_info->bd[track_info->n_bead_display];
    nf = ((track_info->ac_i - bd->start_im) > (TRACKING_BUFFER_SIZE >> 1)) ?
         (TRACKING_BUFFER_SIZE >> 1) : track_info->ac_i - bd->start_im;
    dsx->nx = dsx->ny = nf;
    last_c_i = (track_info->c_i > 0) ? track_info->c_i - 1 : 0;  // c_i is not yet valid

    for (i = nf - 1, j = last_c_i; i >= 0; i--, j--)
    {
        j = (j < 0) ? TRACKING_BUFFER_SIZE + j : j;
        j = (j < TRACKING_BUFFER_SIZE) ? j : TRACKING_BUFFER_SIZE;
        dsx->xd[i] = bd->y[j];
        dsx->yd[i] = y_over_z * bd->z[j];
    }

    op->need_to_refresh = 1;
    set_plot_title(op, "\\stack{{image %d bead servo %d}{Obj start %g obj %g start %d}}"
                   , track_info->ac_i,  track_info->bead_servo_nb,
                   track_info->z_obj_servo_start,
                   track_info->obj_pos[last_c_i], bd->start_im);
    op->user_ispare[ISPARE_BEAD_NB] = track_info->n_bead_display;
    return refresh_plot(pr_TRACK, UNCHANGED);
}


int profile_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
    int i;
    static int last_c_i = -1, immax = -1;
    d_s *ds = NULL, *dsr = NULL; //, *dst = NULL;
#ifdef OPENCL
    d_s *ds_gpu = NULL
#endif
    float dz, min, max, tmp;
    b_track *bd = NULL;
    //static fft_plan *ft_p = NULL;
    char bead_servo[128], mode_track[256];
    (void)d;

    if (track_info == NULL)
    {
        return D_O_K;
    }

    ds = find_source_specific_ds_in_op(op, "Instantaneous radial profile");
    dsr = find_source_specific_ds_in_op(op, "Reference radial profile");
#ifdef OPENCL
    ds_gpu = find_source_specific_ds_in_op(op, "Instantaneous GPU radial profile");

    if (ds_gpu == NULL)
    {
        return D_O_K;
    }

#endif

    if (ds == NULL || dsr == NULL)
    {
        return D_O_K;
    }

    /*
       dst = find_source_specific_ds_in_op(op,"Test radial profile");
       if (dst == NULL)
       {
       dst = create_and_attach_one_ds(op, ds->nx, ds->nx, 0);
       if (dst != NULL)
       {
       set_ds_source(dst,"Test radial profile");
       ft_p = fftbt_init(NULL, ds->nx);
       }
       }
       */

    if (track_info->n_b < 1)
    {
        return D_O_K;    //win_printf_OK("No bead to display");
    }

    i = track_info->c_i;

    if (track_info->imi[i] <= immax)
    {
        return D_O_K;    // we are uptodate
    }

    immax = track_info->imi[i];
    bd = track_info->bd[track_info->n_bead_display];
    last_c_i = track_info->c_i - 1;
    last_c_i = (last_c_i < 0) ? TRACKING_BUFFER_SIZE - 1 : last_c_i;

    if (last_c_i >= TRACKING_BUFFER_SIZE)
    {
        return D_O_K;
    }

    for (i = 0; i < bd->cl && i < ds->mx && i < PROFILE_BUFFER_SIZE; i++)
    {
        ds->xd[i] = dsr->xd[i] = i; // dst->xd[i] =

        if (isnan(bd->rad_prof[last_c_i][i]) || isinf(bd->rad_prof[last_c_i][i]))
        {
            ds->yd[i] = -1;
        }
        else
        {
            ds->yd[i] = bd->rad_prof[last_c_i][i];
        }

        dsr->yd[i] = bd->rad_prof_ref[i];
#ifdef OPENCL
        ds_gpu->xd[i] = i;
        ds_gpu->yd[i] = bd->rad_prof[last_c_i][i];
#endif
    }

#ifdef OPENCL
    ds_gpu->nx = ds_gpu->ny = i;
#endif
    ds->nx = ds->ny = i;
    /*
       if (dst != NULL && ft_p != NULL)
       {
       realtr1bt(ft_p, dst->yd);
       fftbt (ft_p, dst->yd, 1);
       realtr2bt(ft_p, dst->yd, 1);
       dst->yd[0] *= 0.7;
       realtr2bt(ft_p, dst->yd, -1);
       fftbt (ft_p, dst->yd, -1);
       realtr1bt(ft_p, dst->yd);
       }
       */
#ifndef SDI_VERSION
    if (fft_band_pass_in_real_out_complex(dsr->yd, bd->cl, 0, bd->bp_center, bd->bp_width, bd->z_trp, bd->z_fil))
    {
        return win_printf("pb in filtering!");
    }
    dz = find_phase_shift_between_profile(dsr->yd, ds->yd, bd->cl, 0, bd->bp_center, bd->bp_width, bd->rc, bd->z_trp, bd->z_fil);
#endif

    for (i = 0, min = 1e34, max = -1; i < bd->cl && i < ds->mx && i < PROFILE_BUFFER_SIZE; i++)
    {
        ds->xd[i] = dsr->xd[i] = i;

        if (isnan(bd->rad_prof[last_c_i][i]) || isinf(bd->rad_prof[last_c_i][i]))
        {
            ds->yd[i] = tmp = -1;
        }
        else
        {
            ds->yd[i] = tmp = bd->rad_prof[last_c_i][i];
        }

        min = (tmp < min) ? tmp : min;
        max = (tmp < max) ? max : tmp;
        dsr->yd[i] = bd->rad_prof_ref[i];
    }

    dsr->nx = dsr->ny = i;
    /*
    mini = (int)min;
    maxi = (int)max;
    // probably dangereous code
    for (i = 2; i < maxi; i *= 2);

    tmpi = i / 16;

    for (i = tmpi; i < maxi; i += tmpi);

    maxi = i + tmpi;

    for (i = maxi; i > mini; i -= tmpi);

    mini = i - tmpi;
    */
    //  op->x_lo = mini;
    //op->y_hi = maxi;
    //set_plot_y_fixed_range(op);

    if (track_info->bead_servo_nb < 0)
    {
        snprintf(bead_servo, sizeof(bead_servo), "}{");
    }
    else snprintf(bead_servo, sizeof(bead_servo), "bead servo %d}{Obj start %6.3f"
                      , track_info->bead_servo_nb, track_info->z_obj_servo_start);

    if (track_info->evanescent_mode < 0)
    {
        if (bd->calib_im_fil != NULL)
        {
            snprintf(mode_track, sizeof(mode_track), "{Using calibration image pitch %f}"
                     "{\\pt7 Calibration profile %d Z = %g, lost %d %s t %d}}"
                     , (bd->calib_im_fil != NULL) ? bd->calib_im_fil->dy : -1, bd->profile_index,
                     track_info->focus_cor * bd->z[last_c_i], (int)bd->n_l[last_c_i]
                     , (bd->black_circle) ? "Using black circle" : "", bd->z_track);
        }
        else if (bd->generic_calib_im_fil != NULL)
        {
            snprintf(mode_track, sizeof(mode_track), "{Using generic calibration image pitch %f}"
                     "{\\pt7 Calibration profile %d Z = %g, lost %d %s t %d}}"
                     , (bd->generic_calib_im_fil != NULL) ? bd->generic_calib_im_fil->dy : -1, bd->profile_index,
                     track_info->focus_cor * bd->z[last_c_i], (int)bd->n_l[last_c_i]
                     , ((bd->black_circle) ? "Using black circle" : " "), bd->z_track);
        }
        else
        {
            snprintf(mode_track, sizeof(mode_track), "{Using phase difference image}"
                     "{\\pt7 Calibration profile %d Z = %g, lost %d %s t %d}}"
                     , bd->profile_index,
                     track_info->focus_cor * bd->z[last_c_i],
                     (int)bd->n_l[last_c_i], ((bd->black_circle) ? "Using black circle" : " "), bd->z_track);
        }
    }
    else if (track_info->evanescent_mode == 0)
    {
        snprintf(mode_track, sizeof(mode_track), "{Using light intensity average Z = %6.3f}"
                 "{\\pt7 Decay intensity %f offset %f}"
                 "{\\pt7 on %d x %d pixels, lost %d t %d}}"
                 , bd->z[last_c_i], track_info->eva_decay, track_info->eva_offset,
                 bd->cw, bd->cw, (int)bd->n_l[last_c_i], bd->z_track);
    }
    else if (track_info->evanescent_mode == 1)
    {
        snprintf(mode_track, sizeof(mode_track), "{Using light intensity average Z = %6.3f}"
                 "{\\pt7 on %d x %d pixels, lost %d t %d}}"
                 , bd->z[last_c_i], bd->cw, bd->cw, (int)bd->n_l[last_c_i], bd->z_track);
    }
    else if (track_info->evanescent_mode == 2)
    {
        snprintf(mode_track, sizeof(mode_track), "{Using light radial intensity average Z = %6.3f}"
                 "{\\pt7 r_{min}  %d r_{max} %d, lost %d t %d}}"
                 , bd->z[last_c_i], eva_spot_radius, eva_black_radius, (int)bd->n_l[last_c_i], bd->z_track);
    }
    else if (track_info->evanescent_mode == 3)
    {
        snprintf(mode_track, sizeof(mode_track), "{\\pt8 Using light radial intensity average - blacklevel Z = %6.3f}"
                 "{\\pt7 r_{min}  %d r_{max} %d, lost %d t %d}}"
                 , bd->z[last_c_i], eva_spot_radius, eva_black_radius, (int)bd->n_l[last_c_i], bd->z_track);
    }
    else
    {
        snprintf(mode_track, sizeof(mode_track), "}");
    }

    set_plot_title(op, "\\stack{{Bead %d, image %d %s"
                   " dz %6.3f obj %6.3f}%s"
                   , track_info->n_bead_display, track_info->ac_i,  bead_servo
                   , dz, //track_info->bd[0]->z[last_c_i],
                   track_info->obj_pos[last_c_i], mode_track);
    op->need_to_refresh = 1;
    op->user_ispare[ISPARE_BEAD_NB] = track_info->n_bead_display;
    return refresh_plot(pr_TRACK, UNCHANGED);
}

#ifndef SDI_VERSION_2

int x_profile_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
    int i;
    static int last_c_i = -1, immax = -1, tmpi, disp_x = 1;
    d_s *ds = NULL, *dss = NULL, *dsd = NULL, *dsr = NULL, *dsv = NULL, *dsvs = NULL; //
    b_track *bd = NULL;
    float tmp;
    (void)d;

    if (track_info == NULL)
    {
        return D_O_K;
    }

    ds = find_source_specific_ds_in_op(op, "Instantaneous x profile");
    dss = find_source_specific_ds_in_op(op, "side x profile");
    dsd = find_source_specific_ds_in_op(op, "diff x profile");
    dsr = find_source_specific_ds_in_op(op, "Reference x profile");
    dsv = find_source_specific_ds_in_op(op, "X profile variance");
    dsvs = find_source_specific_ds_in_op(op, "Xs profile variance");

    if (ds == NULL || dsr == NULL)
    {
        return D_O_K;
    }

    if (op->x_title != NULL)
    {
        disp_x = (strstr(op->x_title, "delta y") != NULL) ? 0 : 1;
    }

    if (track_info->n_b < 1)
    {
        return D_O_K;    //win_printf_OK("No bead to display");
    }

    i = track_info->c_i;

    if (track_info->imi[i] <= immax)
    {
        return D_O_K;    // we are uptodate
    }

    immax = track_info->imi[i];
    bd = track_info->bd[track_info->n_bead_display];
    last_c_i = track_info->c_i - 1;
    last_c_i = (last_c_i < 0) ? TRACKING_BUFFER_SIZE - 1 : last_c_i;

    for (i = 0, tmpi = 0, tmp = 0; i < bd->cl && i < ds->mx ; i++)
    {
        ds->xd[i] = dss->xd[i] = dsd->xd[i] = dsr->xd[i] = dsv->xd[i] = dsvs->xd[i] = bd->cl / 2 - i;
        tmpi += bd->xi[i];

        if (disp_x)
        {
            ds->yd[i] = bd->xi[i];
            dss->yd[i] = bd->xis[i];
            dsd->yd[i] = bd->xf[i];
            dsr->yd[i] = bd->xcor[i];

            if (bd->x_var && bd->x_var_1)
            {
                dsv->yd[i] = bd->x_var[i] - bd->x_var_1[i];
            }

            if (bd->xs_var && bd->xs_var_1)
            {
                dsvs->yd[i] = bd->xs_var[i] - bd->xs_var_1[i];
            }
        }
        else
        {
            ds->yd[i] = bd->yi[i];
            dss->yd[i] = bd->yis[i];
            dsd->yd[i] = bd->yf[i];
            dsr->yd[i] = bd->ycor[i];

            if (bd->x_var && bd->x_var_1)
            {
                dsv->yd[i] = bd->y_var[i] - bd->y_var_1[i];
            }

            if (bd->xs_var && bd->xs_var_1)
            {
                dsvs->yd[i] = bd->ys_var[i] - bd->ys_var_1[i];
            }
        }

        tmp = (dsr->yd[i] > tmp) ? dsr->yd[i] : tmp;
    }

    if (bd->cl)
    {
        tmpi /= 2 * bd->cl;
    }

    if (tmp == 0)
    {
        tmp = 1;
    }

    for (i = 0; i < bd->cl && i < ds->mx ; i++)
    {
        if (disp_x)
        {
            dsr->yd[i] = tmpi * bd->xcor[i] / tmp;
        }
        else
        {
            dsr->yd[i] = tmpi * bd->ycor[i] / tmp;
        }
    }

    ds->nx = ds->ny = i;
    dsr->nx = dsr->ny = i;
    dss->nx = dss->ny = i;
    dsd->nx = dsd->ny = i;
    dsv->nx = dsv->ny = i;
    dsvs->nx = dsvs->ny = i;
    op->need_to_refresh = 1;
    set_plot_title(op, "\\stack{{%c profile plot %s}"
                   "{im %d dx = %g}"
                   "{1 pixel variance %g and %g}}"
                   , (disp_x) ? 'X' : 'Y'
                   , ((track_info->do_diff_track == 1) ? "diff tracking" : "normal tracking")
                   , track_info->ac_i
                   , bd->dx, (disp_x) ? bd->x_v_var_m / (2 * bd->cw) : bd->y_v_var_m / (2 * bd->cw)
                   , (disp_x) ? bd->xs_v_var_m / (2 * bd->cw) : bd->ys_v_var_m / (2 * bd->cw));
    return refresh_plot(pr_TRACK, UNCHANGED);
}

# endif // #ifndef SDI_VERSION_2

#ifdef SDI_VERSION_2

// int k_buffer_idle_action(O_p *op, DIALOG *d)
//
// {
//     int i;
//     static int last_c_i = -1, immax = -1, disp_x = 1;
//     d_s *ds = NULL, *dss; //
//     b_track *bd = NULL;
//     (void)d;
//
//     if (track_info == NULL)
//     {
//         return D_O_K;
//     }
//
//     ds = find_source_specific_ds_in_op(op, "k_buffer_top");
//     dss = find_source_specific_ds_in_op(op, "k_buffer_bottom");
//
//     if (ds == NULL )
//     {
//         return D_O_K;
//     }
//
//
//     if (track_info->n_b < 1)
//     {
//         return D_O_K;    //win_printf_OK("No bead to display");
//     }
//
//     i = track_info->c_i;
//
//     if (track_info->imi[i] <= immax)
//     {
//         return D_O_K;    // we are uptodate
//     }
//
//     immax = track_info->imi[i];
//     bd = track_info->bd[track_info->n_bead_display];
//     last_c_i = track_info->c_i - 1;
//     last_c_i = (last_c_i < 0) ? TRACKING_BUFFER_SIZE - 1 : last_c_i;
//
//     for (i = 0; i < 256; i++)
//     {
//       ds->xd[i]=dss->xd[i]=i;
//       ds->yd[i]=bd->frange_top->k_buffer[i];
//       dss->yd[i]=bd->frange_bottom->k_buffer[i];
//     }
//
//
//     ds->nx = ds->ny = i;
//
//     dss->nx = dss->ny = i;
//
//     op->need_to_refresh = 1;
//     set_plot_title(op, "\\stack{{%c profile plot}"
//                    "{im %d dx top = %g dx bottom = %g}"
//                    , (disp_x) ? 'X' : 'Y'
//                    , track_info->ac_i
//                    , bd->frange_top->dx,bd->frange_bottom->dx);
//     return refresh_plot(pr_TRACK, UNCHANGED);
// }

int current_phase_differences_idle_action(O_p *op, DIALOG *d)
{
    int i;
    static int last_c_i = -1, immax = -1, disp_x = 1;
    d_s *ds = NULL, *ds2 = NULL, *ds3 = NULL, *ds4 = NULL; //
    b_track *bd = NULL;
    (void)d;

    if (track_info == NULL)
    {
        return D_O_K;
    }

    ds = find_source_specific_ds_in_op(op, "Current phase difference top");
    ds2 = find_source_specific_ds_in_op(op, "Fit of current phase difference top");
    ds3 = find_source_specific_ds_in_op(op, "Current phase difference bottom");
    ds4 = find_source_specific_ds_in_op(op, "Fit of current phase difference bottom");




    if (ds == NULL || ds2 == NULL || ds3 == NULL || ds4 == NULL)
    {
        return D_O_K;
    }

    if (op->x_title != NULL)
    {
        disp_x = (strstr(op->x_title, "delta y") != NULL) ? 0 : 1;
    }

    if (track_info->n_b < 1)
    {
        return D_O_K;    //win_printf_OK("No bead to display");
    }

    i = track_info->c_i;

    if (track_info->imi[i] <= immax)
    {
        return D_O_K;    // we are uptodate
    }

    immax = track_info->imi[i];
    bd = track_info->bd[track_info->n_bead_display];
    last_c_i = track_info->c_i - 1;
    last_c_i = (last_c_i < 0) ? TRACKING_BUFFER_SIZE - 1 : last_c_i;

    printf("b %f \n",bd->frange_top->fit_phase_coeffs[1]);



    for (i = 0; i < bd->filter_high- bd->filter_low + 1 && i < ds->mx ; i++)
    {
        ds->xd[i] = ds2->xd[i] = ds3->xd[i] = ds4->xd[i] = bd->filter_low + i;

        if (disp_x)
        {
            ds->yd[i] = bd->frange_top->fit_phase_corr->yd[i];
            ds->ye[i] = bd->frange_top->fit_phase_corr->ye[i];
            ds2->yd[i] = ds2->xd[i]*bd->frange_top->fit_phase_coeffs[0] + bd->frange_top->fit_phase_coeffs[1];
            ds3->yd[i] = bd->frange_bottom->fit_phase_corr->yd[i];
            ds3->ye[i] = bd->frange_bottom->fit_phase_corr->ye[i];
            ds4->yd[i] = ds4->xd[i]*bd->frange_bottom->fit_phase_coeffs[0] + bd->frange_bottom->fit_phase_coeffs[1];




        }
        else
        {
          ds->yd[i] = bd->frange_top->fit_phase_corr->yd[i];
          ds->ye[i] = bd->frange_top->fit_phase_corr->ye[i];
          ds2->yd[i] = ds2->xd[i]*bd->frange_top->fit_phase_coeffs[0] + bd->frange_top->fit_phase_coeffs[1];
          ds3->yd[i] = bd->frange_bottom->fit_phase_corr->yd[i];
          ds3->ye[i] = bd->frange_bottom->fit_phase_corr->ye[i];
          ds4->yd[i] = ds4->xd[i]*bd->frange_bottom->fit_phase_coeffs[0] + bd->frange_bottom->fit_phase_coeffs[1];


        }
    }


    ds->nx = ds->ny = i;
    ds2->nx = ds2->ny = i;
    ds3->nx = ds3->ny = i;
    ds4->nx = ds4->ny = i;

    op->need_to_refresh = 1;
    set_plot_title(op, "\\stack{{%c profile plot}"
                   "{im %d dx top = %g dx bottom = %g}"
                   , (disp_x) ? 'X' : 'Y'
                   , track_info->ac_i
                   , bd->frange_top->dx,bd->frange_bottom->dx);
    return refresh_plot(pr_TRACK, UNCHANGED);
}

int phase_differences_idle_action(O_p *op, DIALOG *d)
{
    int i;
    static int last_c_i = -1, immax = -1, disp_x = 1;
    d_s *ds = NULL, *ds2 = NULL, *ds3 = NULL, *ds4 = NULL, *ds5, *ds6; //
    b_track *bd = NULL;
    (void)d;

    if (track_info == NULL)
    {
        return D_O_K;
    }

    ds = find_source_specific_ds_in_op(op, "Phase difference 1");
    ds2 = find_source_specific_ds_in_op(op, "Phase difference 2");
    ds3 = find_source_specific_ds_in_op(op, "Phase difference 3");
    ds4 = find_source_specific_ds_in_op(op, "Phase difference 4");
    ds5 = find_source_specific_ds_in_op(op, "Phase 0 top");
    ds6 = find_source_specific_ds_in_op(op, "Phase 0 bottom");



    if (ds == NULL || ds2 == NULL || ds3 == NULL || ds4 == NULL || ds5 == NULL || ds6 == NULL)
    {
        return D_O_K;
    }

    if (op->x_title != NULL)
    {
        disp_x = (strstr(op->x_title, "delta y") != NULL) ? 0 : 1;
    }

    if (track_info->n_b < 1)
    {
        return D_O_K;    //win_printf_OK("No bead to display");
    }

    i = track_info->c_i;

    if (track_info->imi[i] <= immax)
    {
        return D_O_K;    // we are uptodate
    }

    immax = track_info->imi[i];
    bd = track_info->bd[track_info->n_bead_display];
    last_c_i = track_info->c_i - 1;
    last_c_i = (last_c_i < 0) ? TRACKING_BUFFER_SIZE - 1 : last_c_i;

    for (i = 0; i < bd->filter_high- bd->filter_low + 1 && i < ds->mx ; i++)
    {
        ds->xd[i] = ds2->xd[i] = ds3->xd[i] = ds4->xd[i] = ds6->xd[i] =ds5->xd[i] = bd->filter_low + i;

        if (disp_x)
        {
            ds->yd[i] = bd->frange_top->fpb[8]->phase[i]-bd->frange_top->fpb[7]->phase[i];
            ds2->yd[i] = bd->frange_top->fpb[5]->phase[i]-bd->frange_top->fpb[4]->phase[i];
            ds3->yd[i] = bd->frange_top->fpb[3]->phase[i]-bd->frange_top->fpb[2]->phase[i];
            ds4->yd[i] = bd->frange_top->fpb[1]->phase[i]-bd->frange_top->fpb[0]->phase[i];
            ds5->yd[i] = bd->frange_top->fpb[0]->phase[i];
            ds6->yd[i] = bd->frange_bottom->fpb[0]->phase[i];



        }
        else
        {
          ds->yd[i] = bd->frange_top->fpb[8]->phase[i]-bd->frange_top->fpb[7]->phase[i];
          ds2->yd[i] = bd->frange_top->fpb[5]->phase[i]-bd->frange_top->fpb[4]->phase[i];
          ds3->yd[i] = bd->frange_top->fpb[3]->phase[i]-bd->frange_top->fpb[2]->phase[i];
          ds4->yd[i] = bd->frange_top->fpb[1]->phase[i]-bd->frange_top->fpb[0]->phase[i];
          ds5->yd[i] = bd->frange_top->fpb[0]->phase[i];
          ds6->yd[i] = bd->frange_bottom->fpb[0]->phase[i];


        }
    }


    ds->nx = ds->ny = i;
    ds2->nx = ds2->ny = i;
    ds3->nx = ds3->ny = i;
    ds4->nx = ds4->ny = i;
    ds5->nx = ds5->ny = i;
    ds6->nx = ds6->ny = i;

    op->need_to_refresh = 1;
    set_plot_title(op, "\\stack{{%c profile plot}"
                   "{im %d dx top = %g dx bottom = %g}"
                   , (disp_x) ? 'X' : 'Y'
                   , track_info->ac_i
                   , bd->frange_top->dx,bd->frange_bottom->dx);
    return refresh_plot(pr_TRACK, UNCHANGED);
}


int phase_buffer_idle_action(O_p *op, DIALOG *d)
{
    int i;
    static int last_c_i = -1, immax = -1, disp_x = 1;
    d_s *ds = NULL, *ds2 = NULL, *ds3 = NULL, *ds4 = NULL; //
    b_track *bd = NULL;
    (void)d;

    if (track_info == NULL)
    {
        return D_O_K;
    }

    ds = find_source_specific_ds_in_op(op, "Phase Buffer 10");
    ds2 = find_source_specific_ds_in_op(op, "Phase Buffer 11");
    ds3 = find_source_specific_ds_in_op(op, "Phase Buffer 12");
    ds4 = find_source_specific_ds_in_op(op, "Phase Buffer 13");


    if (ds == NULL || ds2 == NULL || ds3 == NULL || ds4 == NULL)
    {
        return D_O_K;
    }

    if (op->x_title != NULL)
    {
        disp_x = (strstr(op->x_title, "delta y") != NULL) ? 0 : 1;
    }

    if (track_info->n_b < 1)
    {
        return D_O_K;    //win_printf_OK("No bead to display");
    }

    i = track_info->c_i;

    if (track_info->imi[i] <= immax)
    {
        return D_O_K;    // we are uptodate
    }

    immax = track_info->imi[i];
    bd = track_info->bd[track_info->n_bead_display];
    last_c_i = track_info->c_i - 1;
    last_c_i = (last_c_i < 0) ? TRACKING_BUFFER_SIZE - 1 : last_c_i;

    for (i = 0; i < bd->im_buffer_size && i < ds->mx ; i++)
    {
        ds->xd[i] = ds2->xd[i] = ds3->xd[i] = ds4->xd[i] = i;

        if (disp_x)
        {
            ds->yd[i] = (float)  bd->frange_top->fpb[i]->phase[0];
            ds2->yd[i] = (float) bd->frange_top->fpb[i]->phase[1];
            ds3->yd[i] = (float) bd->frange_bottom->fpb[i]->phase[2];
            ds4->yd[i] = (float) bd->frange_bottom->fpb[i]->phase[3];

        }
        else
        {
          ds->yd[i] = (float) bd->frange_top->fpb[i]->phase[0];
          ds2->yd[i] = (float) bd->frange_top->fpb[i]->phase[1];
          ds3->yd[i] = (float) bd->frange_bottom->fpb[i]->phase[2];
          ds4->yd[i] = (float) bd->frange_bottom->fpb[i]->phase[3];

        }
    }


    ds->nx = ds->ny = i;
    ds2->nx = ds2->ny = i;
    ds3->nx = ds3->ny = i;
    ds4->nx = ds4->ny = i;

    op->need_to_refresh = 1;
    set_plot_title(op, "\\stack{{%c profile plot}"
                   "{im %d dx top = %g dx bottom = %g}"
                   , (disp_x) ? 'X' : 'Y'
                   , track_info->ac_i
                   , bd->frange_top->dx,bd->frange_bottom->dx);
    return refresh_plot(pr_TRACK, UNCHANGED);
}


int SDI2_mean_real_profile_idle_action(O_p *op, DIALOG *d)
{
  int i;
  static int last_c_i = -1, immax = -1, disp_x = 1;
  d_s *ds = NULL, *dss = NULL; //
  b_track *bd = NULL;
  (void)d;

  if (track_info == NULL)
  {
      return D_O_K;
  }

  ds = find_source_specific_ds_in_op(op, "Real mean profile top");
  dss = find_source_specific_ds_in_op(op, "Real mean profile bottom");



  if (ds == NULL || dss == NULL)
  {
      return D_O_K;
  }

  if (op->x_title != NULL)
  {
      disp_x = (strstr(op->x_title, "delta y") != NULL) ? 0 : 1;
  }

  if (track_info->n_b < 1)
  {
      return D_O_K;    //win_printf_OK("No bead to display");
  }

  i = track_info->c_i;

  if (track_info->imi[i] <= immax)
  {
      return D_O_K;    // we are uptodate
  }

  bd = track_info->bd[track_info->n_bead_display];


  for (i = 0; i < bd->frange_top->profile_size && i < ds->mx ; i++)
  {
      ds->xd[i] = dss->xd[i] =  i;

      if (disp_x)
      {
          ds->yd[i] = bd->frange_top->mean_profile[i];
          dss->yd[i] = bd->frange_bottom->mean_profile[i];

      }
      else
      {
        ds->yd[i] = bd->frange_top->mean_profile[i];
        dss->yd[i] = bd->frange_bottom->mean_profile[i];

      }
  }


  ds->nx = ds->ny = i;

  dss->nx = dss->ny = i;

  op->need_to_refresh = 1;
  set_plot_title(op, "\\stack{{%c profile plot}"
                 "{im %d dx top = %g dx bottom = %g}"
                 , (disp_x) ? 'X' : 'Y'
                 , track_info->ac_i
                 , bd->frange_top->dx,bd->frange_bottom->dx);
  return refresh_plot(pr_TRACK, UNCHANGED);
}

int SDI2_scores_idle_action(O_p *op, DIALOG *d)
{
  int i;
  static int last_c_i = -1, immax = -1, disp_x = 1;
  d_s *ds = NULL, *dss = NULL; //
  b_track *bd = NULL;
  (void)d;

  if (track_info == NULL)
  {
      return D_O_K;
  }

  ds = find_source_specific_ds_in_op(op, "Scores top");
  dss = find_source_specific_ds_in_op(op, "Scores bottom");



  if (ds == NULL || dss == NULL)
  {
      return D_O_K;
  }

  if (op->x_title != NULL)
  {
      disp_x = (strstr(op->x_title, "delta y") != NULL) ? 0 : 1;
  }

  if (track_info->n_b < 1)
  {
      return D_O_K;    //win_printf_OK("No bead to display");
  }

  i = track_info->c_i;

  if (track_info->imi[i] <= immax)
  {
      return D_O_K;    // we are uptodate
  }

  bd = track_info->bd[track_info->n_bead_display];


  for (i = 0; i < 2*bd->frange_top->disp_range*bd->frange_top->discretization && i < ds->mx ; i++)
  {
      ds->xd[i] = dss->xd[i] =  i-bd->frange_top->disp_range*bd->frange_top->discretization;

      if (disp_x)
      {
          ds->yd[i] = bd->frange_top->scores[i];
          dss->yd[i] = bd->frange_bottom->scores[i];

      }
      else
      {
        ds->yd[i] = bd->frange_top->scores[i];
        dss->yd[i] = bd->frange_bottom->scores[i];

      }
  }


  ds->nx = ds->ny = i;

  dss->nx = dss->ny = i;

  op->need_to_refresh = 1;
  set_plot_title(op, "\\stack{{%c profile plot}"
                 "{im %d dx top = %g dx bottom = %g}"
                 , (disp_x) ? 'X' : 'Y'
                 , track_info->ac_i
                 , bd->frange_top->dx,bd->frange_bottom->dx);
  return refresh_plot(pr_TRACK, UNCHANGED);
}


int x_shear_profiles_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
    int i;
    static int last_c_i = -1, immax = -1, disp_x = 1;
    d_s *ds = NULL, *dss = NULL, *dsd = NULL, *dsr = NULL , *dsmin = NULL;//
    b_track *bd = NULL;
    (void)d;

    if (track_info == NULL)
    {
        return D_O_K;
    }

    ds = find_source_specific_ds_in_op(op, "Instantaneous x top shear profile");
    dss = find_source_specific_ds_in_op(op, "Instantaneous x bottom shear profile");
    dsd = find_source_specific_ds_in_op(op, "Instantaneous x top bandpass profile");
    dsr = find_source_specific_ds_in_op(op, "Instantaneous x bottom bandpass profile");
    dsmin = find_source_specific_ds_in_op(op,"Minimization profile");




    if (ds == NULL || dsr == NULL)
    {
        return D_O_K;
    }

    if (op->x_title != NULL)
    {
        disp_x = (strstr(op->x_title, "delta y") != NULL) ? 0 : 1;
    }

    if (track_info->n_b < 1)
    {
        return D_O_K;    //win_printf_OK("No bead to display");
    }

    i = track_info->c_i;

    if (track_info->imi[i] <= immax)
    {
        return D_O_K;    // we are uptodate
    }

    immax = track_info->imi[i];
    bd = track_info->bd[track_info->n_bead_display];
    last_c_i = track_info->c_i - 1;
    last_c_i = (last_c_i < 0) ? TRACKING_BUFFER_SIZE - 1 : last_c_i;

    for (i = 0; i < bd->cl && i < ds->mx ; i++)
    {
        ds->xd[i] = dss->xd[i] = dsd->xd[i] = dsr->xd[i] = - bd->cl / 2 +1+ i;

        if (disp_x)
        {
            ds->yd[i] = bd->frange_top->xi[i];
            dss->yd[i] = bd->frange_bottom->xi[i];
            dsd->yd[i] = bd->frange_top->xibp[i];
            dsr->yd[i] = bd->frange_bottom->xibp[i];

        }
        else
        {
            ds->yd[i] = bd->frange_top->xi[i];
            dss->yd[i] = bd->frange_bottom->xi[i];
            dsd->yd[i] = bd->frange_top->xibp[i];
            dsr->yd[i] = bd->frange_bottom->xibp[i];

        }

        if (bd->do_minimize)
        {
          dsmin->xd[i] = - bd->cl / 2 +1+ i;
          dsmin->yd[i] = (float) 64*(SDI_2_func_min((double)i,bd->cl/2, bd->sigma_g, bd->om_cos));
        }
    }


    ds->nx = ds->ny = i;
    dsr->nx = dsr->ny = i;
    dss->nx = dss->ny = i;
    dsd->nx = dsd->ny = i;

    op->need_to_refresh = 1;
    set_plot_title(op, "\\stack{{%c profile plot}"
                   "{im %d dx top = %g dx bottom = %g}"
                   , (disp_x) ? 'X' : 'Y'
                   , track_info->ac_i
                   , bd->frange_top->dx,bd->frange_bottom->dx);
    return refresh_plot(pr_TRACK, UNCHANGED);
}

int phase_fitting_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{

  int i;
  static int last_c_i = -1, immax = -1, disp_x = 1;
  d_s *ds = NULL, *dss=NULL; //
  b_track *bd = NULL;
  (void)d;

  if (track_info == NULL)
  {
      return D_O_K;
  }

  ds = find_source_specific_ds_in_op(op, "Fitted phase profile top");
  dss = find_source_specific_ds_in_op(op, "Fitted phase profile bottom");
  alloc_data_set_y_error(ds);
  alloc_data_set_y_error(dss);


  if (ds == NULL || dss == NULL)
  {
      return D_O_K;
  }

  if (op->x_title != NULL)
  {
      disp_x = (strstr(op->x_title, "delta x") != NULL) ? 0 : 1;
  }

  if (track_info->n_b < 1)
  {
      return D_O_K;    //win_printf_OK("No bead to display");
  }

  i = track_info->c_i;

  if (track_info->imi[i] <= immax)
  {
      return D_O_K;    // we are uptodate
  }

  immax = track_info->imi[i];
  bd = track_info->bd[track_info->n_bead_display];
  last_c_i = track_info->c_i - 1;
  last_c_i = (last_c_i < 0) ? TRACKING_BUFFER_SIZE - 1 : last_c_i;

  for (i = 0;  i < bd->frange_top->dsfit->nx && i<ds->mx ; i++)
  {
      ds->xd[i] = bd->frange_top->dsfit->xd[i];

      ds->yd[i] = bd->frange_top->dsfit->yd[i];

      ds->ye[i] = bd->frange_top->dsfit->ye[i];

  }

    ds->nx = ds->ny = i;

  for (i = 0;  i < bd->frange_bottom->dsfit->nx && i<dss->mx ; i++)
  {
      dss->xd[i] = bd->frange_bottom->dsfit->xd[i];

      dss->yd[i] = bd->frange_bottom->dsfit->yd[i];

      dss->ye[i] = bd->frange_bottom->dsfit->ye[i];

  }


  dss->nx = dss->ny = i;

  op->need_to_refresh = 1;
  set_plot_title(op, "\\stack{{%c profile plot}"
                 "{im %d dx top = %g dx bottom = %g}"
                 , (disp_x) ? 'X' : 'Y'
                 , track_info->ac_i
                 , bd->frange_top->dx,bd->frange_bottom->dx);
  return refresh_plot(pr_TRACK, UNCHANGED);
}


int Fourier_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{

  int i;
  static int last_c_i = -1, immax = -1, disp_x = 1;
  d_s *ds = NULL, *dss=NULL; //
  b_track *bd = NULL;
  (void)d;

  if (track_info == NULL)
  {
      return D_O_K;
  }

  ds = find_source_specific_ds_in_op(op, "Fourier top");
  dss = find_source_specific_ds_in_op(op, "Fourier bottom");
  alloc_data_set_y_error(ds);
  alloc_data_set_y_error(dss);


  if (ds == NULL || dss == NULL)
  {
      return D_O_K;
  }

  if (op->x_title != NULL)
  {
      disp_x = (strstr(op->x_title, "mode") != NULL) ? 0 : 1;
  }

  if (track_info->n_b < 1)
  {
      return D_O_K;    //win_printf_OK("No bead to display");
  }

  i = track_info->c_i;

  if (track_info->imi[i] <= immax)
  {
      return D_O_K;    // we are uptodate
  }

  immax = track_info->imi[i];
  bd = track_info->bd[track_info->n_bead_display];
  last_c_i = track_info->c_i - 1;
  last_c_i = (last_c_i < 0) ? TRACKING_BUFFER_SIZE - 1 : last_c_i;

  for (i = 0;  i < bd->frange_top->dsfit->nx && i<ds->mx ; i++)
  {
      ds->xd[i] = i;

      ds->yd[i] = bd->frange_top->xf[i];

      ds->ye[i] = bd->frange_top->xf[i];

  }

    ds->nx = ds->ny = i;

  for (i = 0;  i < bd->frange_bottom->dsfit->nx && i<dss->mx ; i++)
  {
      dss->xd[i] = i;

      dss->yd[i] = bd->frange_bottom->xf[i];

      dss->ye[i] = bd->frange_bottom->xf[i];

  }


  dss->nx = dss->ny = i;

  op->need_to_refresh = 1;
  set_plot_title(op, "\\stack{{%c profile plot}"
                 "{im %d dx top = %g dx bottom = %g}"
                 , (disp_x) ? 'X' : 'Y'
                 , track_info->ac_i
                 , bd->frange_top->dx,bd->frange_bottom->dx);
  return refresh_plot(pr_TRACK, UNCHANGED);
}


int average_profile_idle_action(O_p *op, DIALOG *d)
{

  int i;
  static int last_c_i = -1, immax = -1, disp_x = 1;
  d_s *ds = NULL, *dss=NULL; //
  b_track *bd = NULL;
  (void)d;

  if (track_info == NULL)
  {
      return D_O_K;
  }

  ds = find_source_specific_ds_in_op(op, "Average phase profile top");
  dss = find_source_specific_ds_in_op(op, "Average phase profile bottom");
  alloc_data_set_y_error(ds);
  alloc_data_set_y_error(dss);


  if (ds == NULL || dss == NULL)
  {
      return D_O_K;
  }

  if (op->x_title != NULL)
  {
      disp_x = (strstr(op->x_title, "mode") != NULL) ? 0 : 1;
  }

  if (track_info->n_b < 1)
  {
      return D_O_K;    //win_printf_OK("No bead to display");
  }

  i = track_info->c_i;

  if (track_info->imi[i] <= immax)
  {
      return D_O_K;    // we are uptodate
  }

  immax = track_info->imi[i];
  bd = track_info->bd[track_info->n_bead_display];
  last_c_i = track_info->c_i - 1;
  last_c_i = (last_c_i < 0) ? TRACKING_BUFFER_SIZE - 1 : last_c_i;

  for (i = 0;  i < bd->frange_top->fitphase->nx && i<ds->mx ; i++)
  {
      ds->xd[i] = bd->frange_top->fitphase->xd[i];

      ds->yd[i] = bd->frange_top->avg_profile[i];

      ds->ye[i] = bd->frange_top->avg_profile_er[i];

  }

    ds->nx = ds->ny = i;

  for (i = 0;  i < bd->frange_bottom->fitphase->nx && i<dss->mx ; i++)
  {
      dss->xd[i] = bd->frange_bottom->fitphase->xd[i];;

      dss->yd[i] = bd->frange_bottom->avg_profile[i];

      dss->ye[i] = bd->frange_bottom->avg_profile_er[i];

  }




  dss->nx = dss->ny = i;

  op->need_to_refresh = 1;
  set_plot_title(op, "\\stack{{%c profile plot}"
                 "{im %d dx top = %g dx bottom = %g}"
                 , (disp_x) ? 'X' : 'Y'
                 , track_info->ac_i
                 , bd->frange_top->dx,bd->frange_bottom->dx);
  return refresh_plot(pr_TRACK, UNCHANGED);
}


int phase_fitting_derivative_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{

  int i;
  static int last_c_i = -1, immax = -1, disp_x = 1;
  d_s *ds = NULL, *dss=NULL; //
  b_track *bd = NULL;
  (void)d;

  if (track_info == NULL)
  {
      return D_O_K;
  }

  ds = find_source_specific_ds_in_op(op, "Fitted phase profile top");
  dss = find_source_specific_ds_in_op(op, "Fitted phase profile bottom");


  if (ds == NULL || dss == NULL)
  {
      return D_O_K;
  }

  if (op->x_title != NULL)
  {
      disp_x = (strstr(op->x_title, "delta x") != NULL) ? 0 : 1;
  }

  if (track_info->n_b < 1)
  {
      return D_O_K;    //win_printf_OK("No bead to display");
  }

  i = track_info->c_i;

  if (track_info->imi[i] <= immax)
  {
      return D_O_K;    // we are uptodate
  }

  immax = track_info->imi[i];
  bd = track_info->bd[track_info->n_bead_display];
  last_c_i = track_info->c_i - 1;
  last_c_i = (last_c_i < 0) ? TRACKING_BUFFER_SIZE - 1 : last_c_i;

  for (i = 0;  i < bd->frange_top->dsfit->nx-1 && i<ds->mx ; i++)
  {
      ds->xd[i] = bd->frange_top->dsfit->xd[i];

      ds->yd[i] = bd->frange_top->dsfit->yd[i+1]-bd->frange_top->dsfit->yd[i];

  }

    ds->nx = ds->ny = i;

  for (i = 0;  i < bd->frange_bottom->dsfit->nx-1 && i<dss->mx ; i++)
  {
      dss->xd[i] = bd->frange_bottom->dsfit->xd[i];

      dss->yd[i] = bd->frange_bottom->dsfit->yd[i+1]-bd->frange_bottom->dsfit->yd[i];

  }


  dss->nx = dss->ny = i;

  op->need_to_refresh = 1;
  set_plot_title(op, "\\stack{{%c profile plot}"
                 "{im %d dx top = %g dx bottom = %g}"
                 , (disp_x) ? 'X' : 'Y'
                 , track_info->ac_i
                 , bd->frange_top->dx,bd->frange_bottom->dx);
  return refresh_plot(pr_TRACK, UNCHANGED);
}




int y_shear_profiles_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
    int i;
    static int last_c_i = -1, immax = -1, disp_x = 1;
    d_s *ds = NULL, *dss=NULL; //
    b_track *bd = NULL;
    (void)d;

    if (track_info == NULL)
    {
        return D_O_K;
    }

    ds = find_source_specific_ds_in_op(op, "Instantaneous projected y profile top");
    dss = find_source_specific_ds_in_op(op, "Instantaneous projected y profile bottom");


    if (ds == NULL || dss == NULL)
    {
        return D_O_K;
    }

    if (op->x_title != NULL)
    {
        disp_x = (strstr(op->x_title, "delta x") != NULL) ? 0 : 1;
    }

    if (track_info->n_b < 1)
    {
        return D_O_K;    //win_printf_OK("No bead to display");
    }

    i = track_info->c_i;

    if (track_info->imi[i] <= immax)
    {
        return D_O_K;    // we are uptodate
    }

    immax = track_info->imi[i];
    bd = track_info->bd[track_info->n_bead_display];
    last_c_i = track_info->c_i - 1;
    last_c_i = (last_c_i < 0) ? TRACKING_BUFFER_SIZE - 1 : last_c_i;

    for (i = 0; i < bd->cw && i < ds->mx ; i++)
    {
        ds->xd[i] = dss->xd[i] = bd->cw / 2 - i;

        if (disp_x)
        {
            ds->yd[i] = bd->frange_top->yi[i];
            dss->yd[i] = bd->frange_bottom->yi[i];

        }
        else
        {
            ds->yd[i] = bd->frange_top->yi[i];
            dss->yd[i] = bd->frange_bottom->yi[i];

        }
    }


    ds->nx = ds->ny = i;

    dss->nx = dss->ny = i;

    op->need_to_refresh = 1;
    set_plot_title(op, "\\stack{{%c profile plot}"
                   "{im %d dx top = %g dx bottom = %g}"
                   , (disp_x) ? 'X' : 'Y'
                   , track_info->ac_i
                   , bd->frange_top->dx,bd->frange_bottom->dx);
    return refresh_plot(pr_TRACK, UNCHANGED);
}

# endif // #ifdef SDI_VERSION_2

int orthoradial_profile_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
    int i;
    static int last_c_i = -1, immax = -1;
    d_s *ds = NULL; //, *dsr, *dst = NULL;
    int bdn = 0, nim = 0;
    b_track *bd = NULL;
    int orthoradial_prof_size = 0;
    int orthoradial_mean_radius = 0;
    int orthoradial_half_avg_size = 0;
    int kxl = 0;
    (void)d;

    if (track_info == NULL)
    {
        return D_O_K;
    }

    ds = find_source_specific_ds_in_op(op, "Instantaneous orthoradial profile");

    if (ds == NULL)
    {
        return D_O_K;
    }

    if (track_info->n_b < 1)
    {
        return D_O_K;    //win_printf_OK("No bead to display");
    }

    i = track_info->c_i;

    if (track_info->imi[i] <= immax)
    {
        return D_O_K;    // we are uptodate
    }

    immax = track_info->imi[i];
    bd = track_info->bd[track_info->n_bead_display];

    if (op->title != NULL)
    {
        i = sscanf(op->title, "\\stack{{Bead %d, image %d}"
                   "{\\pt7 Orthoradial profile r_{mean} %d +/- %d, size %d k_\\theta = %d}}",
                   &bdn, &nim, &orthoradial_mean_radius, &orthoradial_half_avg_size, &orthoradial_prof_size, &kxl);

        if (i == 6)
        {
            bd->orthoradial_prof_size = orthoradial_prof_size;
            bd->orthoradial_mean_radius = orthoradial_mean_radius;
            bd->orthoradial_half_avg_size = orthoradial_half_avg_size;
            bd->kx_angle = kxl;
        }
    }
    else
    {
        orthoradial_prof_size = bd->orthoradial_prof_size;
        orthoradial_mean_radius = bd->orthoradial_mean_radius;
        orthoradial_half_avg_size = bd->orthoradial_half_avg_size;
        kxl = bd->kx_angle;
    }
#ifndef SDI_VERSION
    if (bd->orthoradial_prof_size > 0)
    {
        bd->o_prof_p = fftbt_init(bd->o_prof_p, bd->orthoradial_prof_size);

        if (bd->o_prof_p == NULL)
        {
            return win_printf_OK("init FFT !");
        }
    }
#endif
    //if (bd->orthoradial_prof_size <= 0) return D_O_K;
    last_c_i = track_info->c_i - 1;
    last_c_i = (last_c_i < 0) ? TRACKING_BUFFER_SIZE - 1 : last_c_i;

    for (i = 0, ds->nx = ds->ny = 0; i < bd->orthoradial_prof_size ; i++)
    {
        add_new_point_to_ds(ds, i, bd->orthoradial_prof[last_c_i][i]);
    }

    /*
       for (i = 0, min = 1e34, max = -1; i < bd->cl && i < ds->mx ; i++)
       {
       tmp = bd->rad_prof[last_c_i][i];
       min = (tmp < min) ? tmp : min;
       max = (tmp < max) ? max : tmp;
       }
       mini = (int)min;
       maxi = (int)max;
       for (i = 2; i < maxi; i *= 2);
       tmpi = i/16;
       for (i = tmpi; i < maxi; i += tmpi);
       maxi = i+tmpi;
       for (i = maxi; i > mini; i -= tmpi);
       mini = i-tmpi;

       op->x_lo = mini;
       op->y_hi = maxi;
       set_plot_y_fixed_range(op);
       */
    op->x_lo = 0;
    op->y_hi = 255;
    set_plot_y_fixed_range(op);
    op->need_to_refresh = 1;
    set_plot_title(op, "\\stack{{Bead %d, image %d}"
                   "{\\pt7 Orthoradial profile r_{mean} %d +/- %d, size %d k_\\theta = %d}}"
                   , track_info->n_bead_display, track_info->ac_i,
                   orthoradial_mean_radius, orthoradial_half_avg_size, orthoradial_prof_size, kxl);
    op->user_ispare[ISPARE_BEAD_NB] = track_info->n_bead_display;
    return refresh_plot(pr_TRACK, UNCHANGED);
}



int grab_profile_rolling_buffer(void)
{
    int i, j, k;
    static int last_c_i = -1, immax = -1;
    int nf;
    b_track *bd = NULL;
    imreg *imr = NULL;
    O_i *oi = NULL, *oic = NULL;
    union pix  *pd = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (ac_grep(cur_ac_reg, "%im%oi", &imr, &oi) != 2)
    {
        return OFF;
    }

    if (track_info == NULL)
    {
        return D_O_K;
    }

    if (track_info->n_b < 1)
    {
        return D_O_K;    //win_printf_OK("No bead to display");
    }

    i = track_info->c_i;

    if (track_info->imi[i] <= immax)
    {
        return D_O_K;    // we are uptodate
    }

    immax = track_info->imi[i];
    bd = track_info->bd[track_info->n_bead_display];
    last_c_i = track_info->c_i - 1;
    last_c_i = (last_c_i < 0) ? TRACKING_BUFFER_SIZE - 1 : last_c_i;
    nf = ((track_info->ac_i - bd->start_im) > TRACKING_BUFFER_SIZE) ?
         TRACKING_BUFFER_SIZE : track_info->ac_i - bd->start_im;
    oic = create_and_attach_oi_to_imr(imr, bd->cl, nf, IS_FLOAT_IMAGE);

    if (oic == NULL)
    {
        return win_printf_OK("cannot create calibration image !");
    }

    set_zmin_zmax_values(oic, oi->z_min, oi->z_max);
    set_z_black_z_white_values(oic, oi->z_black, oi->z_white);
    pd = oic->im.pixel;

    for (i = nf - 1, j = last_c_i; i >= 0; i--, j--)
    {
        j = (j < 0) ? TRACKING_BUFFER_SIZE + j : j;
        j = (j < TRACKING_BUFFER_SIZE) ? j : TRACKING_BUFFER_SIZE;

        for (k = 0; k < bd->cl; k++)
        {
            pd[i].fl[k] = bd->rad_prof[j][k];
        }
    }

    return refresh_image(imr, imr->n_oi - 1);
}



int grab_orthoradial_profile_rolling_buffer(void)
{
    int i, j, k;
    static int last_c_i = -1, immax = -1;
    int nf;
    b_track *bd = NULL;
    imreg *imr = NULL;
    O_i *oi = NULL, *oic = NULL;
    union pix  *pd = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (ac_grep(cur_ac_reg, "%im%oi", &imr, &oi) != 2)
    {
        return OFF;
    }

    if (track_info == NULL)
    {
        return D_O_K;
    }

    if (track_info->n_b < 1)
    {
        return D_O_K;    //win_printf_OK("No bead to display");
    }

    i = track_info->c_i;

    if (track_info->imi[i] <= immax)
    {
        return D_O_K;    // we are uptodate
    }

    immax = track_info->imi[i];
    bd = track_info->bd[track_info->n_bead_display];

    if (bd->orthoradial_prof_size <= 0)
    {
        return win_printf_OK("No ortho profile defined !");
    }

    last_c_i = track_info->c_i - 1;
    last_c_i = (last_c_i < 0) ? TRACKING_BUFFER_SIZE - 1 : last_c_i;
    nf = ((track_info->ac_i - bd->start_im) > TRACKING_BUFFER_SIZE) ?
         TRACKING_BUFFER_SIZE : track_info->ac_i - bd->start_im;
    oic = create_and_attach_oi_to_imr(imr, bd->orthoradial_prof_size, nf, IS_FLOAT_IMAGE);

    if (oic == NULL)
    {
        return win_printf_OK("cannot create calibration image !");
    }

    set_zmin_zmax_values(oic, oi->z_min, oi->z_max);
    set_z_black_z_white_values(oic, oi->z_black, oi->z_white);
    pd = oic->im.pixel;

    for (i = nf - 1, j = last_c_i; i >= 0; i--, j--)
    {
        j = (j < 0) ? TRACKING_BUFFER_SIZE + j : j;
        j = (j < TRACKING_BUFFER_SIZE) ? j : TRACKING_BUFFER_SIZE;

        for (k = 0; k < bd->orthoradial_prof_size; k++)
        {
            pd[i].fl[k] = bd->orthoradial_prof[j][k];
        }
    }

    return refresh_image(imr, imr->n_oi - 1);
}


/*
   simple bead tracking routines

   filling_avg_profile_from_im(oi, xc, yc, bt->cl, bt->cw, gt->xi, gt->yi);
   erase_around_black_circle(gt->xi, gt->yi, bt->cl);
   check_bead_not_lost(gt->xi, gt->xf, gt->yi, gt->yf, bt->cl, gt->bd_mul);
   dx = find_distance_from_center(xf, cl, 0, gt->dis_filter, !black_circle,&corr);
   radial_non_pixel_square_image_sym_profile_in_array_sse(bt->rad_prof[gt->c_i], oi,
   xcf, ycf, cl>>1, bt->apx, bt->apy);

   bead doublet tracking routines

*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// this running in the Real time timer thread
///////////////////////////////////////////////////////////////////////////////////////////////////////////////


int eval_load_from_timing_rolling_buffer(float *load, float *freq)
{
    int i, j, k;
    int BUF_SIZE_FOR_LOAD = 128;
    static int  immax = -1;
    int nf, jstart = 0, j1 = 0;
    unsigned long   dtm, dtmax, dts;
    float   fdtm, fdtmax, ftmp;
    double tmp, tmp2;
    long long llt;

    if (track_info == NULL)
    {
        return D_O_K;
    }

    dts = get_my_uclocks_per_sec();
    i = track_info->c_i;

    if (track_info->imi[i] <= immax)
    {
        return 1;
    }

    i = track_info->c_i;
    immax = track_info->imi[i];
    //lf = immax - track_info->ac_i;

    if (track_info->ac_i < BUF_SIZE_FOR_LOAD)
    {
        return 2;
    }

    nf = BUF_SIZE_FOR_LOAD;
    j = jstart = (track_info->ac_i - BUF_SIZE_FOR_LOAD) % TRACKING_BUFFER_SIZE; //last_c_i;
    j = jstart + nf - 1;
    j = (j < TRACKING_BUFFER_SIZE) ? j : j - TRACKING_BUFFER_SIZE;
    llt = track_info->imt[j];
    j1 = j;
    j = jstart;
    j = (j < TRACKING_BUFFER_SIZE) ? j : j - TRACKING_BUFFER_SIZE;
    llt -= track_info->imt[j];
    llt /= (nf - 1);

    for (i = 0, j = jstart, dtm = dtmax = 0, fdtm = fdtmax = 0, k = 0, tmp2 = 0; i < nf; i++, j++, k++)
    {
        j = (j < TRACKING_BUFFER_SIZE) ? j : j - TRACKING_BUFFER_SIZE;
        ftmp = ((float)(1000 * track_info->imdt[j])) / dts;
        tmp = track_info->imt[j];
        tmp -= (i == 0) ? track_info->imt[j] : track_info->imt[j1];
        tmp = 1000 * (double)(tmp) / get_my_ulclocks_per_sec();
        tmp2 += tmp;
        dtm += track_info->imdt[j];
        fdtm += ftmp;
        j1 = j;
    }

    if (load)
    {
        *load = Pico_param.camera_param.camera_frequency_in_Hz * fdtm / (k * 10);
    }

    if (freq)
    {
        *freq = (float)(get_my_ulclocks_per_sec() / ((double)llt));
    }

    return 0;
}
#ifdef SDI_VERSION_2
int proceed_bead(b_track *bt, int bt_index, g_track *gt, BITMAP *imb, O_i *oi, int ci, int n)
{
  float fxt, fyt, fxb, fyb;
  int ifxt, ifyt, ifxb, ifyb;
  float relative_x=0,relative_y=0;
  int dd1;
  int dd2;
  int top_in_image;
  int bottom_in_image;
  float dummy;
  float dummy2;
  float max_pix;
  b_track *btemp = NULL;
  float btempx;
  float btempy;
  float barx;
  float bary;

  //find the position of the bottom fringe relatively to the x-center of the bottom cross

  bt->not_lost=1;
  bt->in_image=1;

  if (bt->do_minimize == -1) return 0;



  bt->is_free[ci] = bt->free_bead;


   if (gt->SDI_2_track_free && bt->free_bead && gt->do_move_free_bead && gt->dsfreebeads != NULL && gt->dsfreebeads->nx >0)
{
for (int ii = 0;ii<gt->dsfreebeads->nx;ii++)
{
  for (int c_b = 0; c_b < gt->n_b; c_b++)
  {
   btemp =  gt->bd[c_b];
   if (btemp->free_bead == 1) continue;
   btempx = 0.5 * btemp->frange_top->fx +0.5 * btemp->frange_bottom->fx;
   btempy = 0.5 * btemp->frange_top->fy +0.5 * btemp->frange_bottom->fy;
   barx =  0.5*(gt->dsfreebeads->xe[ii] +  gt->dsfreebeads->xd[ii]);
   bary = 0.5*(gt->dsfreebeads->ye[ii] +  gt->dsfreebeads->yd[ii]);

   if ( (btempx-barx)*(btempx-barx) + (btempy-bary)*(btempy-bary) < bt->cw *bt->cw)
   {
     if (gt->dsfreebeads->nx == 1)
     {
       free_data_set(gt->dsfreebeads);
       gt->dsfreebeads = NULL;
       gt->do_move_free_bead = 0;

     }
     else
     {
       remove_point_from_data_set(gt->dsfreebeads,0);
     }
     break;
   }
  }
  if (gt->dsfreebeads == NULL) break;
}
}

if (gt->SDI_2_track_free && bt->free_bead && gt->do_move_free_bead && gt->dsfreebeads != NULL && gt->dsfreebeads->nx >0)
{
    bt->frange_top->fx = gt->dsfreebeads->xd[0];
    bt->frange_top->fy = gt->dsfreebeads->yd[0];
    bt->frange_top->ify = (int)(0.5 + bt->frange_top->fy);
    bt->frange_top->ifx =  (int)(0.5 + bt->frange_top->fx);

    bt->frange_bottom->fx = gt->dsfreebeads->xe[0];
    bt->frange_bottom->fy = gt->dsfreebeads->ye[0];
    bt->frange_bottom->ify = (int)(0.5 + bt->frange_bottom->fy);
    bt->frange_bottom->ifx = (int)(0.5 + bt->frange_bottom->fx);



    if (gt->dsfreebeads->nx == 1)
    {
      free_data_set(gt->dsfreebeads);
      gt->dsfreebeads = NULL;
      gt->do_move_free_bead = 0;
    }
    else
    {
      remove_point_from_data_set(gt->dsfreebeads,0);
    }
    bt->free_bead = 0;
    if (bt->do_minimize )
    {
      bt->frange_top->buffer_full = 0;
      bt->frange_bottom->buffer_full = 0;
    }
  }



  if (bt->mouse_draged && !key[KEY_LCONTROL])
  {
    bt->frange_top->ifx = bt->mx;
    bt->frange_top->fx = (float) bt->mx;
    bt->frange_top->fy = (float)bt->my+(float)bt->dy0_fringes/2;
    bt->frange_top->ify = (int)(0.5 + bt->frange_top->fy);
    bt->frange_bottom->ifx = bt->mx;
    bt->frange_bottom->fx =(float) bt->mx;
    bt->frange_bottom->fy = (float)bt->my-(float)bt->dy0_fringes/2;
    bt->frange_bottom->ify = (int)(0.5 + bt->frange_bottom->fy);
    bt->free_bead = 0;
  }

  else if (bt->frange_top->mouse_draged && key[KEY_LCONTROL])
  {
    bt->frange_top->ifx = bt->mx;
    bt->frange_top->fx = (float) bt->mx;
    bt->frange_top->fy = (float)bt->my+(float)bt->dy0_fringes/2;
    bt->frange_top->ify = (int)(0.5 + bt->frange_top->fy);
    bt->free_bead = 0;


  }
  else if(bt->frange_bottom->mouse_draged && key[KEY_LCONTROL])
    {
      bt->frange_bottom->ifx = bt->mx;
      bt->frange_bottom->fx =(float) bt->mx;
      bt->frange_bottom->fy = (float)bt->my-(float)bt->dy0_fringes/2;
      bt->frange_bottom->ify = (int)(0.5 + bt->frange_bottom->fy);
      bt->free_bead = 0;

    }

    else {

  fxt=bt->frange_top->fx;
  fyt=bt->frange_top->fy;
  ifxt=bt->frange_top->ifx;
  ifyt=bt->frange_top->ify;
  fxb=bt->frange_bottom->fx;
  fyb=bt->frange_bottom->fy;
  ifxb=bt->frange_bottom->ifx;
  ifyb=bt->frange_bottom->ify;



  float max_pix_tot;

  if (gt->SDI_2_track_free)
  {
  if (bt->free_bead) return 0;

  else {

   SDI_2_sum_image_on_area_and_send_barycenter(oi, bt->frange_top->ifx-bt->cl, bt->frange_top->ifx+bt->cl,
        bt->frange_bottom->ify-bt->cw/2, bt->frange_top->ify+bt->cw/2, &dummy, &dummy2, &dummy,1,&max_pix);

     if (max_pix < gt->track_free_lum_threshold ) // if no light in the area of the bead
     {
       bt->free_bead = 1;

       return 0;
     }
  }
}

  SDI_2_update_frange_position(bt,oi,bt->cl,bt->cw,bt->frange_top);

  SDI_2_update_frange_position(bt,oi,bt->cl,bt->cw,bt->frange_bottom);

  // if (bt->frange_top->buffer_full &&(fabs(fabs(bt->frange_top->fx - fxt) - 10) < 3) && (fabs((bt->frange_top->fx - fxt)/(bt->frange_bottom->fx - fxb)) > 2)) // this is propbably a phase jump
  // {
  //   printf("jump detectet tpn\n");
  //   if (bt->frange_top->fx - fxt > 0)
  //   {
  //     bt->frange_top->push = 1;
  //     SDI_2_update_frange_position(bt,oi,bt->cl,bt->cw,bt->frange_top);
  //     bt->frange_top->push = 0;
  //   }
  //   else
  //   {
  //     bt->frange_top->push = -1;
  //     SDI_2_update_frange_position(bt,oi,bt->cl,bt->cw,bt->frange_top);
  //     bt->frange_top->push = 0;
  //   }
  //
  // }
  //
  // else if (bt->frange_top->buffer_full && (fabs(fabs(bt->frange_bottom->fx - fxb) - 10) < 3) && (fabs((bt->frange_bottom->fx - fxb)/(bt->frange_top->fx - fxt)) > 2)) // this is propbably a phase jump
  // {
  //   printf("jump detectet bot\n");
  //
  //   if (bt->frange_bottom->fx - fxb > 0)
  //   {
  //     bt->frange_bottom->push = 1;
  //     SDI_2_update_frange_position(bt,oi,bt->cl,bt->cw,bt->frange_bottom);
  //     bt->frange_bottom->push = 0;
  //   }
  //   else
  //   {
  //     bt->frange_bottom->push = -1;
  //     SDI_2_update_frange_position(bt,oi,bt->cl,bt->cw,bt->frange_bottom);
  //     bt->frange_bottom->push = 0;
  //   }
  //
  // }

if (gt->SDI_2_track_free== 0)
{

  dd1=pow((bt->frange_top->ifx-bt->fence_xi),2)+pow((bt->frange_top->ify-bt->fence_yi),2);
  dd2=pow((bt->frange_bottom->ifx-bt->fence_xi),2)+pow((bt->frange_bottom->ify-bt->fence_yi),2);
  top_in_image = is_cross_in_image(oi, bt->frange_top->ifx, bt->frange_top->ify);
  bottom_in_image = is_cross_in_image(oi, bt->frange_bottom->ifx, bt->frange_bottom->ify);

  if (abs(bt->frange_top->ify-bt->frange_bottom->ify)<(int)(bt->dy0_fringes)/2) //if fringes are too close from one another
  {
    if (bt->problem_fringes)  //first time we try to go back to restore the position of the arm that jumped the most. If it does not work, we recenter the cross.
    {
    bt->frange_bottom->fy=bt->fence_yi-bt->dy0_fringes/2;
    bt->frange_top->fy=bt->fence_yi+bt->dy0_fringes/2;
    bt->frange_bottom->ify=(int) (0.5+bt->frange_bottom->fy);
    bt->frange_top->ify=(int) (0.5+bt->frange_top->fy);
    bt->problem_fringes=0;
  }
   else
   {
      if (fabs(bt->frange_top->ify-ifyt)>fabs(bt->frange_bottom->ify-ifyb))
      {
        bt->frange_top->ify=ifyt;
        bt->frange_top->fy=fyt;
      }
      else
      {
        bt->frange_bottom->ify=ifyb;
        bt->frange_bottom->fy=fyb;
      }
      bt->problem_fringes=1;
  }
}


  if ((top_in_image==0) || (dd1>pow(gt->bead_fence_in_pixel,2) && (bt->fence_xi > 0)))
  {
    bt->frange_top->fx=fxt;
    bt->frange_top->fy=fyt;
    bt->frange_top->ifx=ifxt;
    bt->frange_top->ify=ifyt;

  }

  if ((bottom_in_image==0) || (dd2>pow(gt->bead_fence_in_pixel,2) && (bt->fence_xi > 0) ))
  {
    bt->frange_bottom->fx=fxb;
    bt->frange_bottom->fy=fyb;
    bt->frange_bottom->ifx=ifxb;
    bt->frange_bottom->ify=ifyb;
  }
}
}
  //to change by parameter interfringe



  bt->x[ci]=(bt->frange_top->fx+bt->frange_bottom->fx)/2;
  bt->y[ci]=bt->frange_top->fy/2+bt->frange_bottom->fy/2;
  bt->z[ci]=Pico_param.SDI_2_param.SDI_2_calibration*(bt->frange_top->fx-bt->frange_bottom->fx);





  bt->xc=(int) (0.5+bt->x[ci]);
  bt->yc=(int) (0.5+bt->y[ci]);
  bt->xc_1=bt->xc;
  bt->yc_1=bt->yc;


  // //correct phase if displacements
  //
  // int ifxt_d = bt->frange_top->ifx - ifxt;
  // int ifxb_d = bt->frange_bottom->ifx - ifxb;
  //
  // if (ifxt_d != 0)
  // {
  // for (int j=0 ; j < bt->filter_high-bt->filter_low + 1 ; j++)   bt->frange_top->phase_trk_correction[j] += (2*M_PI*(double)(ifxt_d)*(bt->filter_low+j)/bt->cl);
  // }
  //
  // if (ifxb_d != 0)
  // {
  // for (int j=0 ; j < bt->filter_high-bt->filter_low + 1 ; j++)   bt->frange_bottom->phase_trk_correction[j] += (2*M_PI*(double)(ifxb_d)*(bt->filter_low+j)/bt->cl);
  // }

  //average y displacement
  return 0 ;

}

# endif

#ifndef SDI_VERSION_2
/**
 * @brief Bead Tracking function
 *
 * @param bt bead Track
 * @param bt_index  bead track index in general track
 * @param gt general track
 * @param oi full image
 * @param ci image index in rolling buffer
 * @param n absolute image number
 *
 * @return
 */
int proceed_bead(b_track *bt, int bt_index, g_track *gt, BITMAP *imb, O_i *oi, int ci, int n)
{
    int i, j;
    int xc, yc; //current bead position
    int cl, cw; // cross length and width
    int  cl2, lost_this_time = 0;
    int ci_1, ci_2, dd; //, di
    int  black_circle, *xi, *yi, *xis, *yis;
    float dz = 0, zobjn;
    float xcf, ycf; // new position of the bead
    float dx = 0, dy = 0;// delta between last position and current one
    float corr, *xf = NULL, *yf = NULL, *tpf = NULL, servo_z_ref = 0, dz_error = 0, deriv, tmp; // , dz_1
    int *xvar = NULL, *yvar = NULL, *xsvar = NULL, *ysvar = NULL;
    float evanescent = 0, *lfx1 = NULL, *lfy1 = NULL;
    //extern float fx1[512];
    //float ax = 1, ay = 1;
    //  int red = makecol(255,0,0);
    // int greeen = makecol(0,255,0);
    (void)imb;
    ci_2 = ci - 2;

    if (ci_2 < 0)
    {
        ci_2 += TRACKING_BUFFER_SIZE;
    }

    cl = gt->cl;                   // cross arm length
    bt->cl = bt->ncl;              // we update cross changes
    bt->cw = bt->ncw;
    cl = (bt->cl < 4) ? gt->cl : bt->cl;
    cl2 = cl / 2;
    cw = bt->cw;                   // cross arm width
    //xi = gt->xi;  yi = gt->yi;     // these are temporary int buffers for averaged profile
    //xf = gt->xf;  yf = gt->yf;     // these are temporary float buffers for averaged profile
    xi = bt->xi;
    yi = bt->yi;     // these are temporary int buffers for averaged profile
    xis = bt->xis;
    yis = bt->yis;     // these are temporary int buffers for averaged side profile

    xf = bt->xf;
    yf = bt->yf;     // these are temporary float buffers for averaged profile
    lfx1 = bt->fx1;
    lfy1 = bt->fy1;     // these are temporary  a second float buffers for averaged profile
    cl2 = cl / 2;

    // -- Set current variances variables
    if (ci & 0x01)
    {
        xvar = bt->x_var = bt->x_v[1];
        yvar = bt->y_var = bt->y_v[1];
        xsvar = bt->xs_var = bt->xs_v[1];
        ysvar = bt->ys_var = bt->ys_v[1];
        bt->x_var_1 = bt->x_v[0];
        bt->y_var_1 = bt->y_v[0];
        bt->xs_var_1 = bt->xs_v[0];
        bt->ys_var_1 = bt->ys_v[0];
    }
    else
    {
        xvar = bt->x_var = bt->x_v[0];
        yvar = bt->y_var = bt->y_v[0];
        xsvar = bt->xs_var = bt->xs_v[0];
        ysvar = bt->ys_var = bt->ys_v[0];
        bt->x_var_1 = bt->x_v[1];
        bt->y_var_1 = bt->y_v[1];
        bt->xs_var_1 = bt->xs_v[1];
        bt->ys_var_1 = bt->ys_v[1];
    }

    //di = find_dialog_focus(active_dialog);                // I think this does not work
    // ## Compute last bead position on rolling buffer.
    ci_1 = ci - 1;
    ci_1 = (ci_1 < 0) ? TRACKING_BUFFER_SIZE - 1 : ci_1;
    bt->n_l[ci] = (char) bt->not_lost;
    black_circle = bt->black_circle;

    // ## Set the origin position for bead tracking
    if (bt->mouse_draged)
    {
        xc = bt->mx;
        yc = bt->my;
    }         // last valid position
    else if (bt->not_lost <= 0)                            // the bead is lost
    {
        xc = bt->x0;
        yc = bt->y0;
    }         // last valid position
    else                                              // the bead is not lost
    {
        xc = bt->xc;
        yc = bt->yc;
    } // previous position

    // ## Check and correct cross position at fence
    bt->in_image = is_cross_in_image(oi, xc, yc);

    if ((bt->mouse_draged == 0) && bt->in_image && (bt->fence_xi > 0) && (bt->fence_yi > 0)
            && (gt->bead_fence_in_pixel > 0))
    {
        // we prevent cross to escape from frame
        dd = (bt->fence_xi - xc) * (bt->fence_xi - xc) + (bt->fence_yi - yc) * (bt->fence_yi - yc);

        if (dd > (gt->bead_fence_in_pixel * gt->bead_fence_in_pixel))
        {
            position_cross_at_fence_limit(bt);
        }
    }

    xcf = xc;
    ycf = yc;
    bt->xc_1 = xc;
    bt->yc_1 = yc;

    // ## Initialization of radial profile.
    for (i = 0; i < cl;  i++)
    {
        bt->xcor[i] = 0;
        bt->rad_prof[ci][i] = 0;
    }

    if (bt->in_image)
    {
        // ## fill current profile
        if (bt->cross_45)
        {
            fill_X_avg_profiles_from_im_diff(oi, xc, yc, cl, cw, xi, yi, xis, yis, xvar, yvar, xsvar, ysvar);
        }
        else
        {
            fill_avg_profiles_from_im_diff(oi, xc, yc, cl, cw, xi, yi, xis, yis, xvar, yvar, xsvar, ysvar);
        }

        for (i = (cl - cw) / 2, evanescent = 0; i < (cl + cw) / 2;  i++)
        {
            evanescent += (float)xi[i];
        }

        bt->spot_light[ci] = evanescent / (cw * cw);

        if (black_circle)
        {
            erase_around_black_circle(xi, yi, cl);
        }

        // ## Check if bead is lost for now
        lost_this_time = check_bead_not_lost_diff(xi, xis, xf, yi, yis, yf, cl, gt->bd_mul, gt->do_diff_track);

        if (bt->not_lost <= 0)                 // the bead is still lost
        {
            if (lost_this_time)
            {
                if (bt->mouse_draged == 0)
                {
                    bt->x[ci] = xcf = bt->mx;               // we save its new x, y position
                    bt->y[ci] = ycf = bt->my;
                }
            }
            else
            {
                bt->not_lost = NB_FRAMES_BEF_LOST;
            }
        }
        else
        {
            bt->not_lost -= lost_this_time;
        }

        for (i = 0, bt->x_v_var = bt->xs_v_var = bt->y_v_var = bt->ys_v_var = 0; i < cl;  i++)
        {
            tmp = bt->x_var[i] - bt->x_var_1[i];
            bt->x_v_var += tmp * tmp;
            tmp = bt->xs_var[i] - bt->xs_var_1[i];
            bt->xs_v_var += tmp * tmp;
            tmp = bt->y_var[i] - bt->y_var_1[i];
            bt->y_v_var += tmp * tmp;
            tmp = bt->ys_var[i] - bt->ys_var_1[i];
            bt->ys_v_var += tmp * tmp;
        }

        bt->x_v_var /= cl;
        bt->y_v_var /= cl;
        bt->x_v_var_m = bt->x_v_var_m * 0.99 + bt->x_v_var * 0.01;
        bt->y_v_var_m = bt->y_v_var_m * 0.99 + bt->y_v_var * 0.01;
        bt->xs_v_var /= cl;
        bt->ys_v_var /= cl;
        bt->xs_v_var_m = bt->xs_v_var_m * 0.99 + bt->xs_v_var * 0.01;
        bt->ys_v_var_m = bt->ys_v_var_m * 0.99 + bt->ys_v_var * 0.01;
    }

    bt->n_l[ci] = (char)bt->not_lost;            // we save its present state
    bt->z_track = -1;

    if (bt->in_image && ((lost_this_time == 0)
                         || (track_info->evanescent_mode >= 0)))          // the bead is present for this frame
    {
        if (bt->mouse_draged)
        {
            bt->x[ci] = xcf = bt->mx;               // we save its new x, y position
            bt->y[ci] = ycf = bt->my;

            switch (gt->stop_xy_tracking)
            {
            case 0:
                bt->xc = (int)(xcf + .5);
                bt->yc = (int)(ycf + .5);
                break;

            case 1:
                bt->xc = (int)(xcf + .5);
                break;

            case 2:
                bt->yc = (int)(ycf + .5);
                break;

            default:
                break;
            }

            bt->im_prof_ref = -1;
            bt->z_track = 0;
        }
        else
        {
            dx = find_distance_from_center_2(xf, lfx1, cl, 0, bt->dis_filter, !black_circle, &corr, &deriv,
                                             bt->xy_trp, bt->xy_fil, bt->xnoise + ci, bt->max_limit);
            bt->xnoise[ci] *= (gt->do_diff_track) ? sqrt((bt->x_v_var_m + bt->xs_v_var_m) / 2) : sqrt(bt->x_v_var_m / 2);

            for (i = 0; i < cl;  i++)
            {
                bt->xcor[i] = lfx1[i];
            }

            bt->dxcor_dx = bt->dcor_dx[ci] = deriv;
            bt->xmax = corr;
            dy = find_distance_from_center_2(yf, lfy1, cl, 0, bt->dis_filter, !black_circle, &corr, &deriv,
                                             bt->xy_trp, bt->xy_fil, bt->ynoise + ci, bt->max_limit);
            bt->ynoise[ci] *= (gt->do_diff_track) ? sqrt((bt->y_v_var_m + bt->ys_v_var_m) / 2) : sqrt(bt->y_v_var_m / 2);

            for (i = 0; i < cl;  i++)
            {
                bt->ycor[i] = lfy1[i];
            }

            bt->dycor_dy = bt->dcor_dy[ci] = deriv;
            bt->ymax = corr;

            if (bt->cross_45)
            {
                if (gt->stop_xy_tracking == 0)
                {
                    bt->x[ci] = xcf = xc + (dx + dy);
                    bt->y[ci] = ycf = yc + (dx - dy) - 1;
                }
                else if (gt->stop_xy_tracking == 1)
                {
                    bt->x[ci] = xcf = xc + (dx + dy);
                    bt->y[ci] = ycf = yc;
                }
                else if (gt->stop_xy_tracking == 2)
                {
                    bt->x[ci] = xcf = xc;
                    bt->y[ci] = ycf = yc + (dx - dy) - 1;
                }

                bt->dx = (dx + dy);
                bt->dy = (dx - dy) - 1;
            }
            else
            {
                if (gt->stop_xy_tracking == 0)
                {
                    bt->x[ci] = xcf = dx + xc;               // we save its new x, y position
                    bt->y[ci] = ycf = yc + dy;
                }
                else if (gt->stop_xy_tracking == 1)
                {
                    bt->x[ci] = xcf = dx + xc;               // we save its new x, y position
                    bt->y[ci] = ycf = yc;
                }
                else if (gt->stop_xy_tracking == 2)
                {
                    bt->x[ci] = xcf = xc;               // we save its new x, y position
                    bt->y[ci] = ycf = yc + dy;
                }

                bt->dx = dx;
                bt->dy = dy;
            }

            if (bt->start_im <= 0)
            {
                bt->start_im = gt->ac_i;
            }

            if (gt->stop_xy_tracking == 0)
            {
                bt->xc = (int)(xcf + .5);
                bt->yc = (int)(ycf + .5);

                if ((bt->fence_xi > 0) && (bt->fence_yi > 0) && (gt->bead_fence_in_pixel > 0))
                {
                    // we prevent cross to escape from frame
                    dd = (bt->fence_xi - bt->xc) * (bt->fence_xi - bt->xc) + (bt->fence_yi - bt->yc) * (bt->fence_yi - bt->yc);

                    if (dd > (gt->bead_fence_in_pixel * gt->bead_fence_in_pixel))
                    {
                        position_cross_at_fence_limit(bt);
                        xcf = bt->xc;
                        ycf = bt->yc;
                    }
                }
            }
            else if (gt->stop_xy_tracking == 1)
            {
                bt->xc = (int)(xcf  + .5);
            }
            else if (gt->stop_xy_tracking == 2)
            {
                bt->yc = (int)(ycf + .5);
            }

            if (gt->stop_xy_tracking == 1)
            {
                mean_x_profile_in_array(bt->rad_prof[ci], oi, xcf, ycf, cl >> 1, cw / 2, bt->apx, bt->apy);
            }
            else if (gt->stop_xy_tracking == 2)
            {
                mean_y_profile_in_array(bt->rad_prof[ci], oi, xcf, ycf, cl >> 1, bt->apx, bt->apy);
            }
            else
                radial_non_pixel_square_image_sym_profile_in_array_ptr(bt->rad_prof[ci], oi,
                        xcf, ycf, cl >> 1, bt->apx, bt->apy); // _sse

            if (bt->orthoradial_prof_size > 0)
                orthoradial_non_pixel_square_image_sym_profile_in_array(bt->orthoradial_prof[ci], oi,
                        xcf, ycf, bt->orthoradial_mean_radius, bt->orthoradial_half_avg_size,
                        bt->orthoradial_prof_size, bt->apx, bt->apy);

            bt->z_track = 0;

            if (!lost_this_time)
            {
                if ((gt->bead_servo_nb == (0x100 + bt_index)) || (bt->im_prof_ref < 0))
                {
                    // we grab the reference profile
                    //display_title_message("init servo loop");

                    //printf("setting ref bead %d, bead_servo_nb %d", bt_index, gt->bead_servo_nb);
                    for (i = 0; i < cl;  i++)
                    {
                        bt->rad_prof_ref_fft[i] = bt->rad_prof_ref[i] = bt->rad_prof[ci][i];
                    }

                    gt->z_obj_servo_start = read_last_Z_value();

                    if (fft_band_pass_in_real_out_complex(bt->rad_prof_ref_fft, cl, 0, bt->bp_center,
                                                          bt->bp_width, bt->z_trp, bt->z_fil))
                    {
                        return win_printf("pb in filtering!");
                    }

                    for (i = 0; i < cl;  i++)
                    {
                        bt->rad_prof_ref_fft[i] = bt->rad_prof_ref[i];
                    }

                    i = ci - 1;
                    i = (i < 0) ?  TRACKING_BUFFER_SIZE + i : i;
                    gt->obj_pos[i] = gt->z_obj_servo_start;

                    if (fft_band_pass_in_real_out_complex(bt->rad_prof_ref_fft, cl, 0, bt->bp_center,
                                                          bt->bp_width, bt->z_trp, bt->z_fil))
                    {
                        return win_printf("pb in filtering!");
                    }

                    bt->z[ci] = 0;

                    if (bt->im_prof_ref < 0)
                    {
                        bt->im_prof_ref = gt->ac_i;
                    }
                    else
                    {
                        gt->bead_servo_nb = bt_index;    //gt->c_b;
                    }
                }

                for (i = 0; i < cl;  i++)
                {
                    bt->rad_prof_fft[i] = bt->rad_prof[ci][i];
                }

                dz = find_phase_shift_between_profile(bt->rad_prof_ref_fft, bt->rad_prof_fft, cl, 0,
                                                      bt->bp_center, bt->bp_width, bt->rc, bt->z_trp, bt->z_fil);
                bt->z[ci] = -dz;
                //dz_1 = bt->z[ci_1];

                //FIXME : DEBUG
                //printf("%f\n", -dz);
                //bt->calib_im_fil = NULL;
                //bt->generic_calib_im_fil = NULL;
                //END�FIXME

                if (bt->calib_im_fil != NULL)        // we track in Z using calibration image
                {
                    bt->z_track = 2;

                    for (i = j = 0; i < cl;  i++)
                    {
                        bt->rad_prof_fft[i] = bt->rad_prof[ci][i];
                        j += !!bt->rad_prof_fft[i];
                    }

                    if (j)
                    {
                        i = find_z_profile_by_mean_square_and_phase(bt->rad_prof_fft, cl, bt->calib_im_fil,
                                0, bt->bp_center, bt->bp_width, bt->rc,
                                &dz, &(bt->profile_index), &(bt->min_xi2),
                                &(bt->min_phi),  bt->z_trp, bt->z_fil, NULL);
                        bt->z[ci] = dz;
                        servo_z_ref = gt->obj_servo_profile_index;//bt->calib_im_fil->im.ny/3;
                        servo_z_ref *= bt->calib_im_fil->yu[bt->calib_im_fil->c_yu]->dx;
                        servo_z_ref += bt->calib_im_fil->yu[bt->calib_im_fil->c_yu]->ax;
                    }

                    if (track_info->evanescent_mode == 0)
                    {
                        bt->z[ci] = evanescent_avg_intensity - evanescent_black_avg;

                        if (bt->z[ci] < 0)
                        {
                            bt->z[ci] = - bt->z[ci];
                        }

                        bt->z[ci] = gt->eva_offset - gt->eva_decay_cor * log(bt->z[ci]);
                    }
                    else if (track_info->evanescent_mode == 1)
                    {
                        bt->z[ci] = evanescent;
                    }
                    else if (track_info->evanescent_mode == 2)
                    {
                        bt->z[ci] = evanescent_avg_intensity;
                    }
                    else if (track_info->evanescent_mode == 3)
                    {
                        bt->z[ci] = evanescent_avg_intensity - evanescent_black_avg;
                    }
                }
                else if ((bt->generic_calib_im_fil != NULL) && (bt->cl == bt->generic_calib_im->im.nx))
                {
                    // we track in Z using generic calibration image
                    bt->z_track = 1;

                    for (i = j = 0; i < cl;  i++)
                    {
                        bt->rad_prof_fft[i] = bt->rad_prof[ci][i];
                        j += !!(bt->rad_prof_fft[i]);
                    }

                    if (j)
                    {
                        i = find_z_profile_by_mean_square_and_phase(bt->rad_prof_fft, cl,
                                bt->generic_calib_im_fil, 0, bt->bp_center,
                                bt->bp_width, bt->rc, &dz,
                                &(bt->profile_index), &(bt->min_xi2),
                                &(bt->min_phi), bt->z_trp, bt->z_fil, NULL);
                        bt->z[ci] = dz - track_info->generic_reference;
                        servo_z_ref = track_info->generic_reference;

                        if (track_info->evanescent_mode == 0)
                        {
                            bt->z[ci] = evanescent_avg_intensity - evanescent_black_avg;

                            if (bt->z[ci] < 0)
                            {
                                bt->z[ci] = - bt->z[ci];
                            }

                            bt->z[ci] = gt->eva_offset - gt->eva_decay_cor * log(bt->z[ci]);
                        }
                        else if (track_info->evanescent_mode == 1)
                        {
                            bt->z[ci] = evanescent;
                        }
                        else if (track_info->evanescent_mode == 2)
                        {
                            bt->z[ci] = evanescent_avg_intensity;
                        }
                        else if (track_info->evanescent_mode == 3)
                        {
                            bt->z[ci] = evanescent_avg_intensity - evanescent_black_avg;
                        }
                    }
                }
                else       // we track in Z using phase difference
                {
                    bt->profile_index = -1;
                    bt->z_track = 0;
                    servo_z_ref = 0;

                    if (track_info->evanescent_mode == 0)
                    {
                        bt->z[ci] = evanescent_avg_intensity - evanescent_black_avg;

                        if (bt->z[ci] < 0)
                        {
                            bt->z[ci] = - bt->z[ci];
                        }

                        bt->z[ci] = gt->eva_offset - gt->eva_decay_cor * log(bt->z[ci]);
                    }
                    else if (track_info->evanescent_mode == 1)
                    {
                        bt->z[ci] = evanescent;
                    }
                    else if (track_info->evanescent_mode == 2)
                    {
                        bt->z[ci] = evanescent_avg_intensity;
                    }
                    else if (track_info->evanescent_mode == 3)
                    {
                        bt->z[ci] = evanescent_avg_intensity - evanescent_black_avg;
                    }
                }

                if (bt->orthoradial_prof_size > 0 && bt->kx_angle > 0)
                {
                    // we track angle with orthoradial profiles
                    bt->o_prof_p = fftbt_init(bt->o_prof_p, bt->orthoradial_prof_size);

                    if (bt->o_prof_p != NULL)
                    {
                        tpf = bt->orthoradial_prof_fft;

                        for (i = 0; i < bt->orthoradial_prof_size; i++)
                        {
                            tpf[i] = bt->orthoradial_prof[ci][i];
                        }

                        realtr1bt(bt->o_prof_p, tpf);
                        fftbt(bt->o_prof_p, tpf, 1);
                        realtr2bt(bt->o_prof_p, tpf, 1);
                        bt->angle = (tpf[2 * bt->kx_angle + 1]  == 0 && tpf[2 * bt->kx_angle] == 0)
                                    ? 0 : atan2(tpf[2 * bt->kx_angle + 1], tpf[2 * bt->kx_angle]);

                        if ((bt->angle - bt->angle_1) > M_PI)
                        {
                            bt->phi_jump -= 2;
                        }
                        else if ((bt->angle - bt->angle_1) < -M_PI)
                        {
                            bt->phi_jump += 2;
                        }

                        bt->angle_1 = bt->angle;
                        bt->theta[ci] = (bt->angle + bt->phi_jump * M_PI) / bt->kx_angle;
                    }
                }

                if ((gt->bead_servo_nb & 0xff) == bt_index) // we servo the objective
                {
                    dz_error = dz - servo_z_ref;
                    gt->dz_error_avg += dz_error;
                    if ((gt->ac_i % gt->obj_servo_rate) == 0)
                    {
                        if (gt->obj_servo_rate)
                        {
                            gt->dz_error_avg /= gt->obj_servo_rate;
                        }
                        gt->integral += gt->integral_gain * gt->dz_error_avg;
                        if (gt->integral >= gt->max_obj_mov)
                        {
                            gt->integral = gt->max_obj_mov;
                        }
                        if (gt->integral <= - gt->max_obj_mov)
                        {
                            gt->integral = -gt->max_obj_mov;
                        }
                        zobjn = gt->gain * (gt->dz_error_avg) + gt->integral; // + gt->derivative_gain * (dz - dz_1);
                        if (zobjn >= gt->max_obj_mov)
                        {
                            zobjn = gt->max_obj_mov;
                            my_set_window_title("Objective feedback reaching + limit error %g new pos %g"
                                                , gt->dz_error_avg, zobjn);
                        }

                        if (zobjn <= -gt->max_obj_mov)
                        {
                            zobjn = -gt->max_obj_mov;
                            my_set_window_title("Objective feedback reaching - limit error %g new pos %g"
                                                , gt->dz_error_avg, zobjn);
                        }

                        zobjn = gt->obj_pos[ci_1] - zobjn;
                        gt->obj_servo_action = 1;
                        gt->new_obj_servo_pos = zobjn;

                        if (dis_obj_servo) my_set_window_title("Objective feedback %d error %g new pos %g"
                                                                   , gt->ac_i, gt->dz_error_avg, zobjn);
                    }
                }
                else
                {
                    i = ci - 1;
                    i = (i < 0) ?  TRACKING_BUFFER_SIZE + i : i;
                    //gt->obj_pos[gt->c_i] = gt->obj_pos_cmd[i];
                    //gt->status_flag[gt->c_i] &= ~OBJ_MOVING;
                }
            } // mouse draged
        } // if (in_image)
    }
    else                                              // we save its prevoius position
    {
        bt->x[ci] = bt->x[(ci) ? ci - 1 : 0];
        bt->y[ci] = bt->y[(ci) ? ci - 1 : 0];
    }

    if (track_info->evanescent_mode < 0)
    {
        bt->z[ci] = gt->obj_pos[ci] - bt->z[ci];    // new
    }

    if (fabs(bt->z[ci_2] - bt->z[ci]) > dz_bead_inst)
    {
        // if z present a significative jump record it
        bt->dz_max = bt->z[ci_2] - bt->z[ci];
        bt->dz_max_im = n;
    }

    bt->z_avg += bt->z[ci];
    bt->iz_avg++;

    if (bt->iz_avg >= 10)
    {
        bt->z_avgd = bt->z_avg / bt->iz_avg;
        bt->z_avg = 0;
        bt->iz_avg = 0;
    }

    if (bt->mouse_draged == 0 && bt->in_image)
    {
        if (bt->cross_45)
        {
            clip_X_cross_in_image(oi, bt->xc, bt->yc, cl2, cw / 2); // these are macros
        }
        else
        {
            clip_cross_in_image(oi, bt->xc, bt->yc, cl2);     // we prevent cross to escape from frame
        }
    }

    //  if (imb) draw_cross_in_screen_unit(bt->xc, bt->yc, cl, cw, (tmp > 0)? red: greeen, imb, imr_TRACK->screen_scale);
    return 0;
}
////////////////////////////////////////////////////////////
// end of proceed_bead this running in the timer thread
///////////////////////////////////////////////////////////
#endif // #ifndef SDI_VERSION_2

int x_imdata_2_imr_nl(imreg *imr, int x, DIALOG *d)
{
    int pw;
    (void)d;

    if (imr == NULL || imr->one_i == NULL)
    {
        return -xvin_error(Wrong_Argument);
    }

    pw = imr->one_i->im.nxe - imr->one_i->im.nxs;

    if (imr->s_nx == 0 || pw == 0)
    {
        return 0;
    }

    return imr->x_off + (((x - imr->one_i->im.nxs) * imr->s_nx) / pw); // + d->x
}
int y_imdata_2_imr_nl(imreg *imr, int y, DIALOG *d)
{
    int ph;

    if (imr == NULL || imr->one_i == NULL)
    {
        return -xvin_error(Wrong_Argument);
    }

    ph = imr->one_i->im.nye - imr->one_i->im.nys;

    if (imr->s_ny == 0 || ph == 0)
    {
        return 0;
    }

    return (d->h - imr->y_off  + (((y - imr->one_i->im.nys) * imr->s_ny) / ph)) ; // + d->h - d->y +
}


int move_bead_cross(BITMAP *imb, imreg *imr, DIALOG *d, int x0, int y0)
{
    int cl, cw, pxl, pyl, xb, yb, done = 0, xoff = 0, yoff = 0;
    b_track *bt = NULL;
    int redl = makecol(255, 0, 0), zcolor;
    int greenl = makecol(0, 255, 0);
    int bluel = makecol(0, 0, 255);
    int yellowl=makecol(233, 189, 21);
    int col2=0;
    int in_image = 0, c_b;
    char zpos[256], *zp = NULL;
    char postmp[256];
    float quality_factor[2]={0};

    if (track_info == NULL || imb == NULL || imr == NULL)
    {
        return 0;
    }

    if (track_info->n_b < 1)
    {
        return 0;
    }

    if (imb != (BITMAP *)imr->one_i->bmp.stuff)
    {
        xoff = imr->x_off;
        yoff = imr->y_off - imb->h + d->y;
    }

    //for (track_info->c_b = done = 0; track_info->c_b < track_info->n_b; track_info->c_b++)
    for (c_b = done = 0; c_b < track_info->n_b; c_b++)
    {
        cl = track_info->cl;                   // cross arm length
        cw = track_info->cw;                   // cross arm width
        bt = track_info->bd[c_b];
        cw = bt->cw;                           // cross arm width
        cl = (bt->cl > 4) ? bt->cl : cl;
        col2 = (bt->fixed_bead==1) ? yellowl : greenl;

        if (bt->z_track == 2)
        {
            //snprintf(zpos,256,"%6.3f %6.3f %6.3f",track_info->focus_cor*bt->z_avgd,bt->min_phi,bt->min_xi2);
            if (track_info->imi[track_info->c_i] - bt->dz_max_im < Pico_param.camera_param.camera_frequency_in_Hz * 3)
            {
                snprintf(zpos, 256, "%6.3f dz %6.3f", track_info->focus_cor * bt->z_avgd, track_info->focus_cor * bt->dz_max);
            }
            else
            {
                snprintf(zpos, 256, "%6.3f", track_info->focus_cor * bt->z_avgd);
            }

            zp = zpos;
            zcolor = ((fabs(bt->min_phi) < 1.2) && (bt->min_xi2 < 0.2)) ? greenl : redl;
            if (zcolor==greenl && bt->fixed_bead==1) zcolor=yellowl;
        }
        else if (bt->z_track == 1)
        {
            if (track_info->generic_reference == 0)
            {
                snprintf(zpos, 256, "G %6.3f", track_info->focus_cor * bt->z_avgd);
            }
            else
            {
                snprintf(zpos, 256, "G0 %6.3f", track_info->focus_cor * bt->z_avgd);
            }

            zp = zpos;
            zcolor = ((fabs(bt->min_phi) < 1.2) && (bt->min_xi2 < 0.2)) ? bluel : redl;
        }
        else
        {
            //snprintf(zpos,"P %6.3f %6.3f %6.3f",track_info->focus_cor*bt->z_avgd,bt->min_phi,bt->min_xi2);
            snprintf(zpos, 256, "P. %6.3f", track_info->focus_cor * bt->z_avgd);
            zp = zpos;
            zcolor = redl;
        }

        /*
           if (bt->not_lost == NB_FRAMES_BEF_LOST)
           draw_cross_in_screen_unit(bt->xc, bt->yc, cl, cw, greeen, imb, imr->screen_scale);
           else
           {
           */
        pxl = (int)(0.5 + x_imr_2_imdata(imr, x0));
        pyl = (int)(0.5 + y_imr_2_imdata(imr, y0));

        #ifdef SDI_VERSION_2

        if (bt->mouse_draged == 0)

        {

          if ((pxl < bt->frange_top->ifx + 0.5*bt->cl) &&
          (pxl > bt->frange_top->ifx - 0.5*bt->cl) &&
          (pyl > bt->frange_top->ify - 0.5*bt->cw) &&
          (pyl < bt->frange_top->ify + 0.5*bt->cw))
          {

            bt->frange_top->mouse_draged = 1;
            bt->frange_bottom->mouse_draged = 0;
            bt->mouse_draged = 1;
            if (mouse_b != 0)
            {
                done = 1;
            }
          }
          if ((pxl < bt->frange_bottom->ifx + 0.5*bt->cl) &&
          (pxl > bt->frange_bottom->ifx - 0.5*bt->cl) &&
          (pyl > bt->frange_bottom->ify - 0.5*bt->cw) &&
          (pyl < bt->frange_bottom->ify + 0.5*bt->cw))
          {

            bt->frange_bottom->mouse_draged = 1;
            bt->frange_top->mouse_draged = 0;

            bt->mouse_draged = 1;
            if (mouse_b != 0)
            {
                done = 1;
            }
          }
        }


        #else

        if (bt->mouse_draged == 0
                && (pxl > bt->xc - cw)
                && (pxl < bt->xc + cw)
                && (pyl > bt->yc - cw)
                && (pyl < bt->yc + cw))
        {

            bt->mouse_draged = 1;
            if (mouse_b != 0)
            {
                done = 1;
            }
        }
        #endif


        if (bt->coilable_bead != 0)
        {
            if (zp != NULL && zp[0] != 'I')
            {
                snprintf(postmp, 256, "I %s", zp);

                zp = postmp;
            }
        }

        if (bt->mouse_draged && mouse_b == 1)
        {
            //draw_bubble(screen, B_LEFT, 512, 100, "x %d(%d) y %d(%d)",pxl,bt->xc,pyl,bt->yc);
            if (done == 0)
            {
                bt->mx = bt->xc = bt->x0 = pxl;
                bt->my = bt->yc = bt->y0 = pyl;

                if (key[KEY_LSHIFT])
                {
                    // we move fence
                    bt->fence_xi = pxl;
                    bt->fence_yi = pyl;

                    if (oi_TRACK)
                    {
                        bt->fence_x = bt->fence_xi * oi_TRACK->dx;
                        bt->fence_y = bt->fence_yi * oi_TRACK->dy;
                    }
                }
            }

            done = 1;
        }

        if (mouse_b == 0)
        {
            bt->mouse_draged = 0;
            #if SDI_VERSION_2
            bt->frange_top->mouse_draged = 0;
            bt->frange_bottom->mouse_draged = 0;
            #endif

        }

        //xb = x_imdata_2_imr_nl(imr, bt->xc, d);
        //yb = y_imdata_2_imr_nl(imr, bt->yc, d);
	int x_off, y_off;
        xb = bt->xc + (xoff * imr->screen_scale) / 28;
        yb = bt->yc - (yoff * imr->screen_scale) / 28;
	x_off = (xoff * imr->screen_scale) / 28;
	y_off = (yoff * imr->screen_scale) / 28;
        //draw_bubble(screen, B_LEFT, 100, 100, "x %d(%d) y %d(%d)",xb,bt->xc,yb,bt->yc);
        in_image = is_cross_in_image(imr->one_i, xb, yb);

        if (in_image && bt->not_lost > 0 && bt->mouse_draged == 0)
        {
            if (bt->type == 0 && bt->cross_45)
            {
                draw_X_cross_in_screen_unit(xb, yb, cl, cw, col2, imb, imr->screen_scale, c_b, bt->fixed_bead, zp, zcolor);
            }
            else if (bt->type == 0)
            {
                draw_cross_in_screen_unit(xb, yb, cl, cw, col2, imb, imr->screen_scale, c_b, bt->fixed_bead, zp, zcolor);
            }
            #ifdef SDI_VERSION_2
            else if (bt->type == 1)
            {
        quality_factor[0] = bt->frange_top->quality;
        quality_factor[1] = bt->frange_bottom->quality;
	      draw_moving_shear_rect_vga_screen_unit(bt->frange_top->ifx + x_off, bt->frange_top->ify - y_off, bt->frange_bottom->ifx + x_off, bt->frange_bottom->ify - y_off, cl, cw,
							 bt->dx_fringes, bt->dy0_fringes, bt->fringes_dy_tolerence, col2, imr->screen_scale, c_b, imb,quality_factor);
	    }
          #endif
        }
        else
        {
            if (bt->type == 0 && bt->cross_45)
            {
                draw_X_cross_in_screen_unit(xb, yb, cl, cw, redl, imb, imr->screen_scale, c_b, bt->fixed_bead, zp, zcolor);
            }
            else if (bt->type == 0)
            {
                draw_cross_in_screen_unit(xb, yb, cl, cw, redl, imb, imr->screen_scale, c_b, bt->fixed_bead, zp, zcolor);
            }
            #ifdef SDI_VERSION_2
            else if (bt->type == 1)
            {
              quality_factor[0] = bt->frange_top->quality;
              quality_factor[1] = bt->frange_bottom->quality;
	      draw_moving_shear_rect_vga_screen_unit(bt->frange_top->ifx+ x_off, bt->frange_top->ify - y_off, bt->frange_bottom->ifx+xoff, bt->frange_bottom->ify - y_off, cl, cw,
							 bt->dx_fringes, bt->dy0_fringes, bt->fringes_dy_tolerence, redl, imr->screen_scale,c_b, imb,quality_factor);
	    }
       #endif

        }

        if (bt->type == 0 && bt->orthoradial_prof_size > 0)
        {
            draw_circle_in_screen_unit(xb, yb,
                                       bt->orthoradial_mean_radius - bt->orthoradial_half_avg_size,
                                       col2, imb, imr->screen_scale);
            draw_circle_in_screen_unit(xb, yb,
                                       bt->orthoradial_mean_radius + bt->orthoradial_half_avg_size,
                                       col2, imb, imr->screen_scale);
        }

        //}
    }

    return 0;
}



float   y_imr_2_imdata_hiden(imreg *imr, int y, DIALOG *d)
{
    int ph;

    if (imr == NULL || imr->one_i == NULL)
    {
        return 0;
    }

    ph = imr->one_i->im.nye - imr->one_i->im.nys;

    if (imr->s_ny == 0 || ph == 0)
    {
        return 0;
    }

    return ((float)imr->one_i->im.nys + ((float)((imr->y_off - y + d->y) * ph) / imr->s_ny));
}

float   x_imr_2_imdata_hiden(imreg *imr, int x, DIALOG *d)
{
    int pw;

    if (imr == NULL || imr->one_i == NULL)
    {
        return 0;
    }

    pw  = imr->one_i->im.nxe - imr->one_i->im.nxs;

    if (imr->s_nx == 0 || pw == 0)
    {
        return 0;
    }

    return ((float)imr->one_i->im.nxs + ((float)((x - d->x - imr->x_off) * pw) / imr->s_nx));
}



int show_bead_cross(BITMAP *imb, imreg *imr, DIALOG *d)
{
    int cl, cw, pxl, pyl, redraw = 0, c_b, xoff = 0, yoff = 0;
    b_track *bt = NULL;
    op_tweezers *op_t = NULL;
    int yellowl=makecol(233, 189, 21);
    int redl = makecol(255, 0, 0);
    int greenl = makecol(0, 255, 0);
    int bluel = makecol(0, 0, 255), zcolor = 0, col2=0;
    char zpos[256], *zp = NULL;
    char postmp[264] = {0};
    float quality_factor[2]={0};



    if (track_info == NULL || imb == NULL || imr == NULL)
    {
        return 0;
    }

    if (track_info->n_b < 1)
    {
        return 0;
    }

    if (imb != (BITMAP *)imr->one_i->bmp.stuff)
    {
        xoff = imr->x_off;
        yoff = imr->y_off - imb->h + d->y;
    }

    //for (track_info->c_b = 0; track_info->c_b < track_info->n_b; track_info->c_b++)
    for (c_b = 0; c_b < track_info->n_b; c_b++)
    {

        cl = track_info->cl;                   // cross arm length
        cw = track_info->cw;                   // cross arm width
        bt = track_info->bd[c_b];
        cw = bt->cw;                   // cross arm width
        cl = (bt->cl > 4) ? bt->cl : cl;
        col2= (bt->fixed_bead==1) ? yellowl : greenl ;
        if (bt->z_track == 2)
        {
            if (track_info->imi[track_info->c_i] - bt->dz_max_im < Pico_param.camera_param.camera_frequency_in_Hz * 3)
            {
                snprintf(zpos, 256, "%6.3f dz %6.3f", track_info->focus_cor * bt->z_avgd, track_info->focus_cor * bt->dz_max);
            }
            else
            {
                snprintf(zpos, 256, "%6.3f", track_info->focus_cor * bt->z_avgd);
            }

            //snprintf(zpos,"%6.3f %6.3f %6.3f",track_info->focus_cor*bt->z_avgd,bt->min_phi,bt->min_xi2);
            zp = zpos;
            zcolor = ((fabs(bt->min_phi) < 1.2) && (bt->min_xi2 < 0.2)) ? greenl : redl;
            if (zcolor==greenl && bt->fixed_bead==1) zcolor=yellowl;
        }
        else if (bt->z_track == 1)
        {
            if (track_info->generic_reference == 0)
                //snprintf(zpos,"G %6.3f %6.3f %6.3f",track_info->focus_cor*bt->z_avgd,bt->min_phi,bt->min_xi2);
            {
                snprintf(zpos, 256, "G. %6.3f", track_info->focus_cor * bt->z_avgd);
            }
            else //snprintf(zpos,"G0 %6.3f %6.3f %6.3f",track_info->focus_cor*bt->z_avgd,bt->min_phi,bt->min_xi2);
            {
                snprintf(zpos, 256, "G0 %6.3f", track_info->focus_cor * bt->z_avgd);
            }

            zp = zpos;
            zcolor = ((fabs(bt->min_phi) < 1.2) && (bt->min_xi2 < 0.2)) ? bluel : redl;
        }
        else
        {
            //snprintf(zpos,256,"P %6.3f %6.3f %6.3f",track_info->focus_cor*bt->z_avgd,bt->min_phi,bt->min_xi2);
            snprintf(zpos, 256, "P. %6.3f", track_info->focus_cor * bt->z_avgd);
            zp = zpos;
            zcolor = redl;
        }

        if (bt->coilable_bead != 0)
        {
            if (zp != NULL && zp[0] != 'I')
            {
                snprintf(postmp, 256, "I %s", zp);
                zp = postmp;
            }
        }

        if (bt->movie_w > 0 && bt->movie_h > 0)

            draw_rectangle_in_screen_unit(bt->movie_x0 + xoff, bt->movie_y0 - yoff, bt->movie_w, bt->movie_h,
                                          col2, imb, imr->screen_scale, c_b);

        if (bt->not_lost > 0) //== NB_FRAMES_BEF_LOST)
        {
            if (bt->fence_xi > 0 && bt->fence_yi > 0 && track_info->bead_fence_in_pixel > 0)
            {
                draw_circle_in_screen_unit(bt->fence_xi + xoff, bt->fence_yi - yoff,
                                           track_info->bead_fence_in_pixel,
                                           col2, imb, imr->screen_scale);
            }

            if (bt->type == 0 && bt->cross_45)
                draw_X_cross_in_screen_unit(bt->xc + xoff, bt->yc - yoff, cl, cw, col2, imb, imr->screen_scale, c_b,
                                            bt->fixed_bead, zp, zcolor);
            else if (bt->type == 0)
	      draw_cross_in_screen_unit(bt->xc + xoff, bt->yc - yoff, cl, cw, col2, imb, imr->screen_scale, c_b,
                                               bt->fixed_bead, zp, zcolor);
             #ifdef SDI_VERSION_2
            else if (bt->type == 1)
            {
              quality_factor[0]=bt->frange_top->quality;
              quality_factor[1]=bt->frange_bottom->quality;
	      draw_moving_shear_rect_vga_screen_unit(bt->frange_top->ifx + xoff, bt->frange_top->ify - yoff, bt->frange_bottom->ifx + xoff, bt->frange_bottom->ify - yoff, cl, cw,
							 bt->dx_fringes, bt->dy0_fringes, bt->fringes_dy_tolerence, col2, imr->screen_scale, c_b,imb,quality_factor);
	    }
          #endif

        }
        else
        {
            pxl = (int)(0.5 + x_imr_2_imdata_hiden(imr, mouse_x, d));
            pyl = (int)(0.5 + y_imr_2_imdata_hiden(imr, mouse_y, d));

            #ifdef SDI_VERSION_2

            if ((mouse_b & 0x1) && (pxl < bt->frange_top->ifx + 0.5*bt->cl) &&
            (pxl > bt->frange_top->ifx - 0.5*bt->cl) &&
            (pyl > bt->frange_top->ify - 0.5*bt->cw) &&
            (pyl < bt->frange_top->ify + 0.5*bt->cw))
            {

              bt->frange_top->mouse_draged = 1;
              bt->frange_bottom->mouse_draged = 0;

              bt->mouse_draged = 1;

            }
            else if ((mouse_b & 0x1) && (pxl < bt->frange_bottom->ifx + 0.5*bt->cl) &&
            (pxl > bt->frange_bottom->ifx - 0.5*bt->cl) &&
            (pyl > bt->frange_bottom->ify - 0.5*bt->cw) &&
            (pyl < bt->frange_bottom->ify + 0.5*bt->cw))
            {

              bt->frange_bottom->mouse_draged = 1;
              bt->frange_top->mouse_draged = 0;

              bt->mouse_draged = 1;


            }

            else
            {
              if (mouse_b == 0)
              {
                  bt->mouse_draged = 0;
                  bt->frange_top->mouse_draged = 0;
                  bt->frange_bottom->mouse_draged = 0;
                          }
            }
            #else

            if ((mouse_b & 0x1)
                    && (pxl > bt->xc - cw)
                    && (pxl < bt->xc + cw)
                    && (pyl > bt->yc - cw)
                    && (pyl < bt->yc + cw))
            {
                //draw_bubble(screen, B_LEFT, 512, 100, "x %d(%d) y %d(%d)",pxl,bt->xc,pyl,bt->yc);
                bt->mouse_draged = 1;
                //        bt->mx = bt->xc = bt->x0 = pxl;
                //bt->my = bt->yc = bt->y0 = pyl;
            }

            else
            {
                if (mouse_b == 0)
                {
                    bt->mouse_draged = 0;

                }
            }
            #endif

            //if (bt->mouse_draged == 0) position_cross_at_fence_according_to_calibration_image(bt);
            if (bt->type == 0 && bt->cross_45)
                draw_X_cross_in_screen_unit(bt->xc + xoff, bt->yc - yoff, cl, cw, redl, imb, imr->screen_scale, c_b, bt->fixed_bead, zp,
                                            zcolor);
            else if (bt->type == 0)
	      draw_cross_in_screen_unit(bt->xc + xoff, bt->yc - yoff, cl, cw, redl, imb, imr->screen_scale, c_b, bt->fixed_bead, zp, zcolor);

        #ifdef SDI_VERSION_2
            else if (bt->type == 1)
            {
              quality_factor[0]=bt->frange_top->quality;
              quality_factor[1]=bt->frange_bottom->quality;
	      draw_moving_shear_rect_vga_screen_unit(bt->frange_top->ifx + xoff, bt->frange_top->ify - yoff, bt->frange_bottom->ifx + xoff, bt->frange_bottom->ify - yoff, cl, cw,
							 bt->dx_fringes, bt->dy0_fringes, bt->fringes_dy_tolerence, redl, imr->screen_scale,c_b, imb,quality_factor);
	    }
      #endif

            if (bt->fence_xi > 0 && bt->fence_yi > 0 && track_info->bead_fence_in_pixel > 0)
            {
                draw_circle_in_screen_unit(bt->fence_xi + xoff, bt->fence_yi - yoff,
                                           track_info->bead_fence_in_pixel,
                                           col2, imb, imr->screen_scale);
            }
        }

        if (bt->mouse_draged)
        {
            redraw = 1;
        }

        if (bt->type == 0 && bt->orthoradial_prof_size > 0)
        {
            draw_circle_in_screen_unit(bt->xc + xoff, bt->yc - yoff,
                                       bt->orthoradial_mean_radius - bt->orthoradial_half_avg_size,
                                       col2, imb, imr->screen_scale);
            draw_circle_in_screen_unit(bt->xc + xoff, bt->yc - yoff,
                                       bt->orthoradial_mean_radius + bt->orthoradial_half_avg_size,
                                       col2, imb, imr->screen_scale);
        }
    }

    for (c_b = 0; c_b < track_info->n_op_t; c_b++)
    {
        op_t = track_info->op_t[c_b];

        if (bt->type == 0 && bt->cross_45)
	  {
            draw_X_cross_in_screen_unit(op_t->xc + xoff, op_t->yc - yoff, op_t->cr_l, op_t->cr_w, bluel,
                                        imb, imr->screen_scale, c_b, 0, NULL, zcolor);
	  }
        else if (bt->type == 0)
	  {
	    draw_cross_in_screen_unit(op_t->xc + xoff, op_t->yc - yoff, op_t->cr_l, op_t->cr_w, bluel,
				      imb, imr->screen_scale, c_b, 0, NULL, zcolor);
	  }
    #ifdef  SDI_VERSION_2
	else if (bt->type == 1)
	  {
      quality_factor[0]=bt->frange_top->quality;
      quality_factor[1]=bt->frange_bottom->quality;
	    draw_moving_shear_rect_vga_screen_unit(bt->frange_top->ifx + xoff, bt->frange_top->ify - yoff, bt->frange_bottom->ifx + xoff, bt->frange_bottom->ify - yoff, cl, cw,
							 bt->dx_fringes, bt->dy0_fringes, bt->fringes_dy_tolerence, bluel, imr->screen_scale,c_b, imb,quality_factor);
	  }
    #endif

    }

    return redraw;
}

int ask_to_update_menu(void)
{
    if (track_info == NULL)
    {
        return 1;
    }

    last_im_who_asked_menu_update = track_info->imi[track_info->c_i];
    return 0;
}

int im_ci(void)
{
    return (track_info != NULL) ? track_info->ac_i : -1;
}

#ifdef RAWHID_V1
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// this running in the Real time timer thread
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//tv intermediate function for image processing : using rawhid one gets the total state of the instrument in a separate thread so no need for rs232used and so on to avoid multiple com
//zmag : still apt api possible, same for zobj :  pifoc with pie709
int track_in_x_y_timer(imreg *imr, O_i *oi, int n,  DIALOG *d, long long t, unsigned long dt, void *p, int n_inarow)
{

    int ci, i, ci_1, ci_2, c_b, ci_10; // , k, cl2, cl, cw

    int pi_e709_will_be_used = 0;
    int pi_e709_com_period = 128; //try different refresh period (in frame) for reading pifoc position
//#ifdef ZMAG_APT //tv 120317 : when zmag controlled by apt (independant usb channel)
    int zmag_apt_used = 0;
    int zmag_apt_will_be_used = 0;
    int zmag_apt_com_period = 128;
    float x_bar_max = 0;
    float y_bar_max = 0;
    float max_integrated_pix = 0;
//#endif

    int f_conv = 0, limit = 0; // , focus_chg_by_prog = 0, focus_check = 0
    int page_n, i_page, abs_pos, status;
    BITMAP *imb = NULL;
    g_track *gt = NULL;
    b_track *bt = NULL;
    g_record *temp_gen_record = NULL;
    static int obj_moving = 0, zmag_motion_on = 0, vcap_servo_on = 0, limits_on = 0;// rot_flag_im_start = 0,
    static char buf[512];

    float tmp;
    f_action *ap;
    /* we grab the general structure */
    (void)imr;
    (void)d;
    gt = (g_track *)p;

    if (gt->ac_i % 64 == 0)
    {
        if (eval_load_from_timing_rolling_buffer(&prev_load, &prev_freq) == 0) Pico_param.camera_param.camera_frequency_in_Hz = prev_freq;
    }

    gt->ac_i++;                     // we increment the image number and wrap it eventually
    ci_1 = ci = gt->c_i;
    ci_2 = ci_1 - 1;
    if (ci_2 < 0) ci_2 += TRACKING_BUFFER_SIZE;
    ci_10 = ci_1 - 9;
    if (ci_10 < 0) ci_10 += TRACKING_BUFFER_SIZE;
    ci++;
    ci = (ci < gt->n_i) ? ci : 0;
    gt->imi[ci] = n;                                 // we save image" number
    gt->imit[ci] = n_inarow;
    gt->imt[ci] = t;                                 // image time
    gt->imdt[ci] = dt;
    gt->c_i = ci;

    if (oi->bmp.stuff != NULL) imb = (BITMAP *)oi->bmp.stuff;
    // we grab the video bitmap of the IFC image
    prepare_image_overlay(oi);

    if (oi) gt->bead_fence_in_pixel = (int)(0.5 + (gt->bead_fence / oi_TRACK->dy));
    gt->obj_pos[ci] = read_last_Z_value();
#ifndef SDI_VERSION_2
#ifndef SDIPICO
    if (track_info->SDI_mode == 1)  sdi_1_proceed_fringes(gt, oi, imb, ci, n);
    else  //ring tracking
    {
#endif
#endif       //#ifndef SDI_VERSION_2
        if (gt->n_b > 0)
        {
	  gt->obj_servo_action = 0;
#ifdef OPENCL
	  if (is_gpu_tracking_enable())
            {
	      gt->is_using_gpu = true;
	      proceed_beads_gpu(oi, gt, ci);
            }
	  if (is_normal_tracking_enable())
#endif
            {
	      gt->is_using_gpu = false;
#ifndef SDI_VERSION_2
              #pragma omp parallel for
#endif
        float dummy, dummy2;
        //SDI_2_sum_image_on_area_and_send_barycenter(oi, 0, oi->im.nx,0, oi->im.ny, &dummy, &dummy2, &gt->track_free_lum_threshold,1);


#ifdef SDI_VERSION_2
        if (gt->SDI_2_track_free)
        {
        if (gt->dsfreebeads)
        {
          free_data_set(gt->dsfreebeads);
          gt->dsfreebeads = NULL;
        }
        gt->dsfreebeads = SDI_2_find_beads_in_partial_OI(oi, 0, oi->im.nx, gt->track_free_y_low, gt->track_free_y_high,track_info->track_free_lum_threshold, track_info->track_free_xw, track_info->track_free_yw,track_info->track_free_window_x,track_info->track_free_window_y);
        if (gt->dsfreebeads != NULL)
        {
          if (gt->dsfreebeads->nx>0) gt->do_move_free_bead = 1;
          else
          {
            free_data_set(gt->dsfreebeads);
            gt->dsfreebeads = NULL;
          }
        }


        // SDI_2_find_new_beads_in_pixel_area(oi,gt, 0, oi->im.nx, gt->track_free_y_low, gt->track_free_y_high, &x_bar_max, &y_bar_max, &max_integrated_pix);
        // if (max_integrated_pix > gt->track_free_lum_threshold)
        // {
        //   gt->track_free_x_bar = x_bar_max;
        //   gt->track_free_y_bar = y_bar_max;
        //   gt->do_move_free_bead = 1;
        //
        //
        //
        //
        // }

        }

        #endif
	      for (c_b = 0; c_b < gt->n_b; c_b++)
                {

		  proceed_bead(gt->bd[c_b], c_b, gt, imb, oi, ci, n);

                }
            }
        }
#ifndef SDI_VERSION_2
#ifndef SDIPICO
    }
#endif
#endif       //#ifndef SDI_VERSION_2

# ifdef USE_MLFI
    if (beadnb_w_z_switched_to_mfi >= 0 && beadnb_w_z_switched_to_mfi < gt->n_b)
        {
            gt->bd[beadnb_w_z_switched_to_mfi]->z[ci] = grab_previous_mfi_data();
        }
#endif

    i = find_remaining_action(n);
    if (last_zmag_pid.proceeded_ask_bo_be_clear > last_zmag_pid.acknowledge)
    {
        last_zmag_pid.proceeded_RT = 0;
        last_zmag_pid.acknowledge = n;
        RT_debug("im %d zmag PID ask to clear proceed set to 0\n", gt->imi[ci]);
    }
    if (last_rot_pid.proceeded_ask_bo_be_clear > last_rot_pid.acknowledge)
    {
        last_rot_pid.proceeded_RT = 0;
        last_rot_pid.acknowledge = n;
        RT_debug("im %d rot PID ask to clear proceed set to 0\n", gt->imi[ci]);
    }
    if (last_T0_pid.proceeded_ask_bo_be_clear > last_T0_pid.acknowledge)
    {
        last_T0_pid.proceeded_RT = 0;
        last_T0_pid.acknowledge = n;
    }
    if (last_zmag_read.proceeded_ask_bo_be_clear > last_zmag_read.acknowledge)
    {
        last_zmag_read.proceeded_RT = 0;
        last_zmag_read.acknowledge = n;
        RT_debug("im %d zmag read ask to clear proceed set to 0\n", gt->imi[ci]);
    }
    if (last_rot_read.proceeded_ask_bo_be_clear > last_rot_read.acknowledge)
    {
        last_rot_read.proceeded_RT = 0;
        last_rot_read.acknowledge = n;
        RT_debug("im %d ROT read ask to clear proceed set to 0\n", gt->imi[ci]);
    }
    if (last_led_read.proceeded_ask_bo_be_clear > last_led_read.acknowledge)
    {
        last_led_read.proceeded_RT = 0;
        last_led_read.acknowledge = n;
        RT_debug("im %d LED read ask to clear proceed set to 0\n", gt->imi[ci]);
    }
    if (last_T_read.proceeded_ask_bo_be_clear > last_T_read.acknowledge)
    {
        last_T_read.proceeded_RT = 0;
        last_T_read.acknowledge = n;
    }

    gt->obj_pos_cmd[ci] = gt->obj_pos_cmd[ci_1];
    if (gt->status_flag[gt->c_i]&USER_MOVING_OBJ) gt->obj_pos_cmd[ci] = gt->obj_pos[ci_1];
    gt->zmag_cmd[ci] = gt->zmag_cmd[ci_1];
    gt->rot_mag_cmd[ci] = gt->rot_mag_cmd[ci_1];
    gt->status_flag[ci] |= (gt->status_flag[ci_1] & (VCAP_SERVO_ON | POSITIVE_LIMIT_ON | NEGATIVE_LIMIT_ON));
    gt->Vcap_zmag[ci] = gt->Vcap_zmag[ci_1];

  //list of actions to be done
  while ((i = find_next_action(n)) >= 0)
  {
    ap = action_pending + i;

    if (ap->type == MV_OBJ_ABS)   ////objective actions: independant from others peripherals in rawhidv1 and v2 HW layout
    {
      if (set_Z_value(ap->value) == 0)
      {
        gt->status_flag[ci] |= OBJ_MOVING;
        gt->obj_pos_cmd[ci] = ap->value;
        obj_moving = focus_transient;
        ap->proceeded = 1;
      }
    }
    else if (ap->type == MV_OBJ_REL)
    {
      if (set_Z_value(gt->obj_pos_cmd[ci_1] + ap->value) == 0)
      {
        gt->obj_pos_cmd[ci] = gt->obj_pos_cmd[ci_1] + ap->value;
        gt->status_flag[ci] |= OBJ_MOVING;
        obj_moving = focus_transient;
        ap->proceeded = 1;
      }
    }

    if (ap->type == MV_ZMAG_REL)
    {
#ifdef ZMAG_THREAD
      zmag_continous_polling = false;
#endif
      if (set_magnet_z_value(gt->zmag_cmd[ci_1] + ap->value) == 0)
      {
        gt->zmag_cmd[ci] = gt->zmag_cmd[ci_1] + ap->value;
        if (gt->zmag_cmd[ci] > absolute_maximum_zmag) gt->zmag_cmd[ci] = absolute_maximum_zmag;
        gt->zmag_positive_limit_when_on = gt->zmag_negative_limit_when_on = 0;
        if (v_mag)
        {
          // we try to predict travelling time
          zmag_motion_on = (int)(Pico_param.camera_param.camera_frequency_in_Hz * fabs(gt->zmag_cmd[ci] - gt->zmag_cmd[ci_1]) / v_mag);
          zmag_motion_on += zmag_transient;
        }
        else zmag_motion_on = zmag_transient;
        gt->zmag_limit_on_start = gt->status_flag[ci_1] & (NEGATIVE_LIMIT_ON | POSITIVE_LIMIT_ON);
        gt->status_flag[ci] |= ZMAG_MOVING;
        last_im_who_asked_menu_update = gt->imi[ci];
        ap->proceeded = 1;
        RT_debug("im %d set zmag REL val %g \n", gt->imi[ci], ap->value);
#ifdef ZMAG_THREAD
        zmag_continous_polling = true;
#endif
      }
      zmag_apt_used = 1;
    }
    else if (ap->type == MV_ZMAG_ABS)
    {
#ifdef ZMAG_THREAD
      zmag_continous_polling = false;
#endif
      if (set_magnet_z_value(ap->value) == 0)
      {
        gt->zmag_cmd[ci] = ap->value;
        if (gt->zmag_cmd[ci] > absolute_maximum_zmag) gt->zmag_cmd[ci] = absolute_maximum_zmag;
        gt->zmag_positive_limit_when_on = gt->zmag_negative_limit_when_on = 0;
        if (v_mag)
        {
          // we try to predict travelling time
          zmag_motion_on = (int)(Pico_param.camera_param.camera_frequency_in_Hz * fabs(gt->zmag_cmd[ci] - gt->zmag_cmd[ci_1]) / v_mag);
          zmag_motion_on += zmag_transient;
        }
        else zmag_motion_on = zmag_transient;
        gt->zmag_limit_on_start = gt->status_flag[ci_1] & (NEGATIVE_LIMIT_ON | POSITIVE_LIMIT_ON);
        gt->status_flag[ci] |= ZMAG_MOVING;
        last_im_who_asked_menu_update = gt->imi[ci];
        ap->proceeded = 1;
        RT_debug("im %d set zmag ABS val %g \n", gt->imi[ci], ap->value);
#ifdef ZMAG_THREAD
        zmag_continous_polling = true;
#endif
      }
      zmag_apt_used = 1;
    }
    else if (ap->type == READ_ZMAG_VAL)
    {
#ifdef ZMAG_THREAD
      zmag_continous_polling = false;
#endif
      zmag_motion_on = zmag_transient;
      last_im_who_asked_menu_update = gt->imi[ci];
      ap->proceeded = 1;
#ifdef ZMAG_THREAD
      zmag_continous_polling = true;
#endif
    }
    else if (ap->type == CHG_ZMAG_REF)
    {
#ifdef ZMAG_THREAD
      zmag_continous_polling = false;
#endif
      if (set_magnet_z_ref_value(ap->value) == 0)
      {
        gt->zmag_cmd[ci] = ap->value;
        gt->status_flag[ci] |= REF_ZMAG_CHANGED;
        n_magnet_z_inst = last_n_magnet_z = ap->value;
        n_magnet_z = prev_zmag = ap->value;
        last_im_who_asked_menu_update = gt->imi[ci];
        ap->proceeded = 1;
        zmag_motion_on = zmag_transient;
#ifdef ZMAG_THREAD
        zmag_continous_polling = true;
#endif
      }
      zmag_apt_used = 1;
    }
    else if (ap->type == CHG_ZMAG_FACTOR) //change the translation factor between motor and magnets tv 09/03/17
    {
#ifdef ZMAG_THREAD
      zmag_continous_polling = false;
#endif
      if (set_magnet_z_factor_value(ap->value) == 0)
      {
        gt->zmag_cmd[ci] = ap->value;
        gt->status_flag[ci] |= ZMAG_FACTOR_CHANGED;
        n_magnet_z_inst = last_n_magnet_z = ap->value;
        n_magnet_z = prev_zmag = ap->value;
        last_im_who_asked_menu_update = gt->imi[ci];
        ap->proceeded = 1;
        zmag_motion_on = zmag_transient;
#ifdef ZMAG_THREAD
        zmag_continous_polling = false;
#endif
      }
      zmag_apt_used = 1;
    }
    else if (ap->type == READ_ZMAG_PID)
    {
      if (get_zmag_motor_pid != NULL) last_zmag_pid.value = (float)get_zmag_motor_pid(ap->spare);
      last_zmag_pid.spare = ap->spare;
      last_zmag_pid.type = READ_ZMAG_PID;
      last_zmag_pid.im_done = gt->imi[ci];
      last_zmag_pid.proceeded_RT = 1;
      ap->proceeded = 1;
      RT_debug("im %d zmag PID read param %d val %g\n", gt->imi[ci], ap->spare, last_zmag_pid.value);
      zmag_apt_used = 1;
    }
    else if (ap->type == CHG_ZMAG_PID)
    {
      //tv 1302 : can change speed of zmag with different config of control
# if defined(PI_C843) || defined(PI_C843_TRS) || defined(ZMAG_APT) || defined(RAWHID_V2)
      set_zmag_motor_speed(ap->value);// == 0)
      if (true)
# else
      if (set_zmag_motor_pid(ap->spare, (int)(ap->value)) == 0)
# endif
      {
        //gt->status_flag[ci] |= REF_ZMAG_CHANGED;
        if (ap->spare == 7) gt->status_flag[ci] |= ZMAG_SPEED_CHANGE;
        last_zmag_pid.value = ap->value;
        last_zmag_pid.spare = ap->spare;
        last_zmag_pid.type = CHG_ZMAG_PID;
        last_zmag_pid.im_done = gt->imi[ci];
        last_zmag_pid.proceeded_RT = 1;
        ap->proceeded = 1;
        RT_debug("im %d set zmag PID param %d val %g\n", gt->imi[ci], ap->spare, last_zmag_pid.value);
        my_set_window_title("Change zmag velocity to %f", (float) ap->value);
      }
      else
      {
        snprintf(buf, 512, "Zmag PID error sending %f to param %d!",  tmp, ap->spare);//(int)(ap->value), ap->spare);
        change_param_label_string(buf, makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
        RT_debug("im %d set zmag PID param %d val %g FAILED !\n", gt->imi[ci], ap->spare, last_zmag_pid.value);
      }
      zmag_apt_used = 1;
    }
    else if (ap->type == READ_ROT_VAL) //rotation action
  {
	    rot_moving = rot_transient;
	    last_im_who_asked_menu_update = gt->imi[ci];
	    ap->proceeded = 1;
	  }
	else if (ap->type == MV_ROT_REL)
	  {
	    if (set_rot_value(gt->rot_mag_cmd[ci_1] + ap->value) == 0)
	      {
		gt->rot_mag_cmd[ci] = gt->rot_mag_cmd[ci_1] + ap->value;
		gt->status_flag[ci] |= ROT_MOVING;
		last_im_who_asked_menu_update = gt->imi[ci];//rot_flag_im_start =
		ap->proceeded = 1;
	      }
	  }
	else if (ap->type == CHG_ROT_REF)
	  {
	    if (set_rot_ref_value(ap->value) == 0)
	      {
		gt->rot_mag_cmd[ci] = ap->value;
		last_n_rota = prev_mag_rot = ap->value;
		n_rota_inst = n_rota = ap->value;
		gt->status_flag[ci] |= REF_ROT_CHANGED;
		last_im_who_asked_menu_update = gt->imi[ci];
		ap->proceeded = 1;
		rot_moving = rot_transient;
	      }
	  }
	else if (ap->type == MV_ROT_ABS)
	  {
	    if (set_rot_value(ap->value) == 0)
	      {
		gt->rot_mag_cmd[ci] = ap->value;
		gt->status_flag[ci] |= ROT_MOVING;
		last_im_who_asked_menu_update = gt->imi[ci];//rot_flag_im_start =
		ap->proceeded = 1;
	      }
	  }
	else if (ap->type == CHG_ROT_PID)
	  {
	    if (set_rotation_motor_pid(ap->spare, (int)(ap->value)) == 0)
	      {
		//gt->status_flag[ci] |= REF_ZMAG_CHANGED;
		if (ap->spare == 7) gt->status_flag[ci] |= ROT_SPEED_CHANGE;
		last_rot_pid.value = ap->value;
		last_rot_pid.spare = ap->spare;
		last_rot_pid.type = CHG_ROT_PID;
		last_rot_pid.im_done = gt->imi[ci];
		last_rot_pid.proceeded_RT = 1;
		ap->proceeded = 1;
		RT_debug("im %d chg rot PID param %d val %g proceed set to 1\n", gt->imi[ci], ap->spare, last_rot_pid.value);
	      }
	    else
	      {
		snprintf(buf, 512, "ROT PID error sending %d to param %d!", (int)(ap->value), ap->spare);
		change_param_label_string(buf, makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
		RT_debug("im %d chg rot PID param %d val %g FAILED\n", gt->imi[ci], ap->spare, last_rot_pid.value);
	      }
	  }
	else if (ap->type == CHG_T0_PID)
	  {
	    if (set_T0_pid != NULL)
	      {
		if (true)//if (set_T0_pid(ap->spare, (int)(ap->value)) == 0)
		  {
		    last_T0_pid.value = ap->value;
		    last_T0_pid.spare = ap->spare;
		    last_T0_pid.type = CHG_T0_PID;
		    last_T0_pid.im_done = gt->imi[ci];
		    last_T0_pid.proceeded_RT = 1;
		    ap->proceeded = 1;
		  }
	      }
	  }
	else if (ap->type == GET_FOCUS_POS)
	  {
	    ap->imi += ap->period;
	    ap->proceeded = 0;
	  }
	else if (ap->type == READ_ROT_PID)
	  {
	    if (get_rotation_motor_pid != NULL) last_rot_pid.value = (float)get_rotation_motor_pid(ap->spare);
	    last_rot_pid.spare = ap->spare;
	    last_rot_pid.type = READ_ROT_PID;
	    last_rot_pid.im_done = gt->imi[ci];
	    last_rot_pid.proceeded_RT = 1;
	    ap->proceeded = 1;
	    RT_debug("im %d rot PID read param %d val %g proceed set to 1\n", gt->imi[ci], ap->spare, last_rot_pid.value);
	  }
	else if (ap->type == READ_T0_PID)
	  {
	    if (true)//if (get_T0_pid != NULL) last_T0_pid.value = get_T0_pid(ap->spare);
	      last_T0_pid.spare = ap->spare;
	    last_T0_pid.type = READ_T0_PID;
	    last_T0_pid.im_done = gt->imi[ci];
	    last_T0_pid.proceeded_RT = 1;
	    ap->proceeded = 1;
	  }
	else if (ap->type == READ_LED_VAL)
	  {
	    if (true){;}//if (get_light != NULL) track_info->led_level = get_light();
	    else track_info->led_level = 100.0;
	    last_led_read.value = track_info->led_level;
	    last_led_read.spare = ap->spare;
	    last_led_read.type = READ_LED_VAL;
	    last_led_read.im_done = gt->imi[ci];
	    last_led_read.proceeded_RT = 1;
	    ap->proceeded = 1;
	    RT_debug("im %d LED read val %g proceed set to 1\n", gt->imi[ci], last_led_read.value);
	    my_set_window_title("get light");
	  }
	else if (ap->type == CHG_LED_VAL)
	  {
	    if (true)//if (set_light != NULL) set_light(ap->value);
	      track_info->led_level = ap->value;
	    ap->proceeded = 1;
	    gt->status_flag[ci] |= LED_CURRENT_CHANGED;
	    RT_debug("im %d LED set val %g proceed set to 1\n", gt->imi[ci], ap->value);
	  }
	if (ap->type == BEAD_CROSS_SAVED)
	  {
	    for (c_b = 0; c_b < gt->n_b; c_b++)
	      {
		bt = gt->bd[c_b];
		if (track_info->SDI_mode==1)
		  {
		    //do_save_beads_to_cfg_file(void)
		  }
		else
		  {
		    if ((bt->calib_im_fil == NULL || bt->in_image == 0)) continue;
		  }
		bt->saved_x = bt->x[gt->c_i]; // last_x_not_lost
		bt->saved_y = bt->y[gt->c_i]; // last_y_not_lost
	      }
	    my_set_window_title("Bead position saved");
	    ap->proceeded = 1;
	  }
	else if (ap->type == BEAD_CROSS_RESTORE)
	  {
	    for (c_b = 0; c_b < gt->n_b; c_b++)
	      {
		bt = gt->bd[c_b];
		/*if (track_info->SDI_mode==1)
		  {

		  }
		  else*/
		if ((bt->calib_im_fil == NULL || bt->in_image == 0)) continue;
		bt->mx = bt->xc = bt->x0 = (int)bt->saved_x; //last_x_not_lost;
		bt->my = bt->yc = bt->y0 = (int)bt->saved_y; //last_y_not_lost;
	      }
	    my_set_window_title("Bead position restored");
	    ap->proceeded = 1;
	  }
	else if (ap->type == READ_GAIN_EXPO)
	  {
	    if (get_camera_gain != NULL) get_camera_gain(&track_info->camera_gain, &track_info->camera_min_gain, &track_info->camera_max_gain, NULL);
	    if (get_camera_shutter_in_us != NULL)  get_camera_shutter_in_us(&track_info->camera_exposure_time_us, &track_info->camera_min_exposure, &track_info->camera_max_exposure);
	    ap->proceeded = 1;
	  }
	else if (ap->type == CHG_CAMERA_GAIN)
	  {
	    if (set_camera_gain != NULL) set_camera_gain(ap->value, &track_info->camera_gain, &track_info->camera_min_gain, &track_info->camera_max_gain, NULL);
	    ap->proceeded = 1;
	    gt->status_flag[ci] |= CAMERA_GAIN_CHG;
	  }
	else if (ap->type == CHG_CAMERA_EXPO)
	  {
	    if (set_camera_shutter_in_us != NULL)  set_camera_shutter_in_us((int)ap->value, &track_info->camera_exposure_time_us, &track_info->camera_min_exposure, &track_info->camera_max_exposure);
	    ap->proceeded = 1;
	    gt->status_flag[ci] |= CAMERA_EXPOSURE_CHG;
	  }
	else
	  {
	    ap->imi = n + 1;
	    gt->status_flag[ci] &= ~REF_ROT_CHANGED;
	    gt->status_flag[ci] &= ~REF_ZMAG_CHANGED;
	    gt->status_flag[ci] &= ~ZMAG_FACTOR_CHANGED;
	  }
    }//end of scan of actions to be done

  if ((gt->com_skip_fr == 0) || (ci % gt->com_skip_fr == 0))
    {
      if (zmag_motion_on > 1) zmag_motion_on--;    // zmag just started we wait for motor to start
      else
	{
	  if ((gt->status_flag[ci_1] | ZMAG_MOVING_SAMPLED) && fabs(gt->zmag[ci_10] - gt->zmag[ci_1]) < 0.005) zmag_motion_on = 0;
	  else zmag_motion_on = 1;
	}
    }

  //read zmag
  //for zmag : if in rawhid, asked at all frame, if in usb channel not in separate thread only periodically and when monving, if usd and separate thread all frame
  zmag_moving = (zmag_motion_on > 0) ? ((vcap_servo_on) ? 4 * zmag_transient : zmag_transient) : ((zmag_moving > 0) ? zmag_moving - 1 : 0);

#ifdef ZMAG_THREAD
  if(zmag_continous_polling==true)
    {
      gt->zmag[ci] = zmag_data->zmag_value[available_zmag_id];
      prev_zmag = gt->zmag[ci_1];
      last_zmag_read.value = prev_zmag;
      last_zmag_read.type = READ_ZMAG_VAL;
      last_zmag_read.im_done = gt->imi[ci];
      last_zmag_read.proceeded_RT = 1;
    }
  else prev_zmag = gt->zmag[ci] = gt->zmag[ci_1];
  if (zmag_moving) gt->status_flag[ci] &= ~ZMAG_MOVING; //detects stops
#else
#ifdef RAWHID_V2
  if (true)
  {
#else
  if (zmag_apt_used==1) prev_zmag = gt->zmag[ci] = gt->zmag[ci_1];
  else if (zmag_moving || ((ci & zmag_apt_com_period) == 0x0f))
  {
      zmag_apt_used = 1;
#endif
      if (read_magnet_z_value_and_status(gt->zmag + ci, gt->Vcap_zmag + ci, &limit, &gt->vcap_servo_on) == 0)
	     {
	        prev_zmag = gt->zmag[ci];// = read_magnet_z_value();
	  gt->zmag_from_Vcap = Pico_param.translation_motor_param.Zmag_vs_Vcap_poly_a0;
	  gt->zmag_from_Vcap += gt->Vcap_zmag[ci] * Pico_param.translation_motor_param.Zmag_vs_Vcap_poly_a1;
	  gt->zmag_from_Vcap += gt->Vcap_zmag[ci] * gt->Vcap_zmag[ci] * Pico_param.translation_motor_param.Zmag_vs_Vcap_poly_a2;
	  last_zmag_read.value = prev_zmag;
	  last_zmag_read.type = READ_ZMAG_VAL;
	  last_zmag_read.im_done = gt->imi[ci];
	  last_zmag_read.proceeded_RT = 1;
	  RT_debug("im %d read zmag val %g \n", gt->imi[ci], prev_zmag);
	  if ((gt->vcap_servo_on & 0x04))
	    {
	      my_set_window_title("Motors Brake active! Zmag %g", gt->zmag[ci]);
	      snprintf(buf, 512, "Motors brake active! Zmag %g", gt->zmag[ci]);
	      change_param_label_string(buf, makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
	      param_label_modified = 1;
	      vcap_servo_on = 1;
	      change_zmag_label_string("Brakes on", makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
	      zmag_label_modified = 1;
	    }
	  else if ((gt->vcap_servo_on & 0x02))
	    {
	      my_set_window_title("PID of Zmag motors inactivated !Zmag %g", gt->zmag[ci]);
	      snprintf(buf, 512, "PID of Zmag motors inactivated !Zmag %g", gt->zmag[ci]);
	      change_param_label_string(buf, makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
	      param_label_modified = 1;
	      vcap_servo_on = 1;
	      change_zmag_label_string("Z PID Off!", makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
	      zmag_label_modified = 1;
	    }
	  else if ((gt->vcap_servo_on & 0x01))
	    {
	      gt->status_flag[ci] |= VCAP_SERVO_ON;
	      my_set_window_title("Zmag is servo by inductive sensor Zmag %g Vcap %g", gt->zmag[ci], gt->Vcap_zmag[ci]);
	      snprintf(buf, 512, "Inductive sensor stops Zmag Vcap %g", gt->Vcap_zmag[ci]);
	      change_param_label_string(buf, makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
	      param_label_modified = 1;
	      vcap_servo_on = 1;
	      change_zmag_label_string("Zmag Vservo", makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
	      zmag_label_modified = 1;
	    }
	  else if (fabs(gt->zmag[ci] - absolute_maximum_zmag) < 0.002)
	    {
	      snprintf(buf, 512, "Zmag %g reaching mximum value %g", gt->zmag[ci], absolute_maximum_zmag);
	      change_param_label_string(buf, makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
	      param_label_modified = 1;
	      zmag_label_modified = 1;
	      vcap_servo_on = 1;
	    }
	  else if ((gt->Vcap_zmag[ci] > 0) && (gt->Vcap_zmag[ci] < 10))
	    {
	      if (Pico_param.translation_motor_param.Zmag_vs_Vcap_poly_a1 != 0)  snprintf(buf, 512, "Zmag %5.3f in range of sensor Vcap %g", gt->zmag_from_Vcap, gt->Vcap_zmag[ci]);
	      else
		{
		  my_set_window_title("Zmag approaching 0 Zmag %g Vcap %g", gt->zmag[ci], gt->Vcap_zmag[ci]);
		  snprintf(buf, 512, "Zmag in range of sensor Vcap %g", gt->Vcap_zmag[ci]);
		}
	      change_param_label_string(buf, makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
	      param_label_modified = 1;
	      zmag_label_modified = 1;
	      vcap_servo_on = 1;
	    }
	  else if (vcap_servo_on == 1)
	    {
	      vcap_servo_on = 0;
	      change_param_label_string(NULL, 0, 0, 0);
	      change_zmag_label_string(NULL, 0, 0, 0);
	    }
	  if (limit > 0)
	    {
	      gt->status_flag[ci] |= POSITIVE_LIMIT_ON;
	      my_set_window_title("Zmag positive switch limit ON !");
	      change_param_label_string("Zmag positive switch limit ON !", makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
	      param_label_modified = 1;
	      limits_on = 1;
	      change_zmag_label_string("Zmag +limit", makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
	      zmag_label_modified = 1;
	      if ((gt->zmag_limit_on_start & POSITIVE_LIMIT_ON) == 0) gt->zmag_positive_limit_when_on = 1;
	    }
	  else if (limit < 0)
	    {
	      gt->status_flag[ci] |= NEGATIVE_LIMIT_ON;
	      my_set_window_title("Zmag negative switch limit ON !");
	      change_param_label_string("Zmag negative switch limit ON !", makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
	      param_label_modified = 1;
	      limits_on = 1;
	      change_zmag_label_string("Zmag -limit", makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
	      zmag_label_modified = 1;
	      if ((gt->zmag_limit_on_start & NEGATIVE_LIMIT_ON) == 0) gt->zmag_negative_limit_when_on = 1;
	    }
	  else if (limits_on)
	    {
	      gt->status_flag[ci] &= ~(NEGATIVE_LIMIT_ON | POSITIVE_LIMIT_ON | VCAP_SERVO_ON);
	      change_param_label_string(NULL, 0, 0, 0);
	      change_zmag_label_string(NULL, 0, 0, 0);
	      limits_on = 0;
	    }
	  else gt->status_flag[ci] &= ~(NEGATIVE_LIMIT_ON | POSITIVE_LIMIT_ON | VCAP_SERVO_ON);
	  f_conv = 1;
        }
      else prev_zmag = gt->zmag[ci] = gt->zmag[ci_1];
    }
  else
    {
      prev_zmag = gt->zmag[ci] = gt->zmag[ci_1];
      gt->status_flag[ci] &= ~ZMAG_MOVING;
    }
#endif


  rot_moving = (fabs(gt->rot_mag_cmd[ci] - gt->rot_mag[ci_1]) > 0.005) ? rot_transient : ((rot_moving > 0) ? rot_moving - 1 : 0);
  if (false)
    {
      prev_mag_rot = gt->rot_mag[ci] = gt->rot_mag[ci_1];
    }
  else if (rot_moving || ((ci & 0x0000002f) == 0x01f))
    {
      tmp = read_rot_value();
      if (tmp != __FLT_MAX__)
	{
	  last_rot_read.value = tmp;
	  //last_rot_read.spare = ap->spare;
	  last_rot_read.type = READ_ROT_VAL;
	  last_rot_read.im_done = gt->imi[ci];
	  last_rot_read.proceeded_RT = 1;
	  RT_debug("im %d read rot val %g \n", gt->imi[ci], tmp);
	  prev_mag_rot = gt->rot_mag[ci] = tmp;
	  if (rot_moving) gt->status_flag[ci] |= ROT_MOVING | ROT_MOVING_SAMPLED;
	  else gt->status_flag[ci] |= ROT_MOVING_SAMPLED;
	  f_conv = 1;
	}
      else prev_mag_rot = gt->rot_mag[ci] = gt->rot_mag[ci_1];
    }
  else
    {
      prev_mag_rot = gt->rot_mag[ci] = gt->rot_mag[ci_1];
      gt->status_flag[gt->c_i] &= ~ROT_MOVING;
    }

  if (f_conv) convert_zmag_to_force(prev_zmag, prev_mag_rot, 0);

  tmp = read_Z_value_accurate_in_micron();
  gt->status_flag[ci] |= OBJ_MOVING_SAMPLED; //with rawhid we sample all the time in a separate thread
  if (obj_moving > 0)
  {
    last_focus_read.value = tmp;
    last_focus_read.type = GET_FOCUS_POS;
    last_focus_read.im_done = gt->imi[ci];
    last_focus_read.proceeded_RT = 1;
    if (tmp >=0)
    {
      prev_focus = gt->obj_pos[ci] = tmp;
      obj_moving = (fabs(gt->obj_pos[ci] - gt->obj_pos[ci_1]) > 0.015) ? 5 : obj_moving - 1;
      obj_moving = (obj_moving < 0) ? 0 : obj_moving;
      gt->status_flag[gt->c_i] |= OBJ_MOVING;
      if (focus_label_modified)
      {
        change_focus_label_string(NULL, 0, 0, 0);
        focus_label_modified = 0;
      }
    }
    //else prev_focus = gt->obj_pos[ci] = gt->obj_pos[ci_1];
  }
  else //not supposed to move, except if the user changed the focus
  {
    if (tmp >=0)
    {
      gt->obj_pos[ci] = tmp;
      if (fabs(prev_focus - gt->obj_pos[ci]) > 0.015)
      {
        gt->obj_pos_cmd[ci] = prev_focus = gt->obj_pos[ci];
        gt->status_flag[gt->c_i] |= USER_MOVING_OBJ;
      }
      else gt->status_flag[gt->c_i] &= ~(OBJ_MOVING | USER_MOVING_OBJ);
      if (focus_label_modified)
      {
        change_focus_label_string(NULL, 0, 0, 0);
        focus_label_modified = 0;
      }
    }
    //else prev_focus = gt->obj_pos[ci] = gt->obj_pos[ci_1];
  }


  //still use the cycle of call because of transfer rolling buffer function
  if (gt->itemp_mes == 0 && get_temperature != NULL)
    {
      if (gt->temp_cycle == 0)
	{
	  gt->T0 = get_temperature(0);
	  snprintf(gt->temp_mes, 32, "T0=%5.5f", gt->T0);
	  gt->ntemp_mes = gt->itemp_mes = strlen(gt->temp_mes) + 1;
	}
      else if (gt->temp_cycle == 1)
	{
	  gt->T1 = get_temperature(1);
	  snprintf(gt->temp_mes, 32, "T1=%5.5f", gt->T1);
	  gt->ntemp_mes = gt->itemp_mes = strlen(gt->temp_mes) + 1;
	}
      else if (gt->temp_cycle == 2)
	{
	  gt->T2 = get_temperature(2);
	  snprintf(gt->temp_mes, 32, "T2=%5.5f", gt->T2);
	  gt->ntemp_mes = gt->itemp_mes = strlen(gt->temp_mes) + 1;
	}
      else if (gt->temp_cycle == 3)
	{
	  gt->T3 = get_temperature(3);
	  snprintf(gt->temp_mes, 32, "T3=%5.5f", gt->T3);
	  gt->ntemp_mes = gt->itemp_mes = strlen(gt->temp_mes) + 1;
	}
      gt->temp_cycle++;
      if (gt->temp_cycle > 3) gt->temp_cycle = 0;
    }

  gt->temp_message[ci] = (gt->itemp_mes > 0) ? gt->temp_mes[gt->ntemp_mes - gt->itemp_mes--] : 0;
  gt->nb_bd_in_view[ci] = gt->last_nb_of_bd_found;
  gt->imdt[ci] = my_uclock() - dt;

  if ((gt->local_lock & BUF_FREEZED) == 0)
    {
      gt->local_lock |= BUF_CHANGING;  // we prevent data changes
      for (; gt->lac_i < gt->ac_i; gt->lac_i++)
	{
	  // we update local buffer
	  i = gt->lac_i % TRACKING_BUFFER_SIZE;
	  gt->limi[i] = gt->imi[i];
	  gt->limit[i] = gt->imit[i];
	  gt->limt[i] = gt->imt[i];
	  gt->limdt[i] = gt->imdt[i];
	  gt->lc_i = i;
	}
      gt->local_lock = 0;
    }
#ifndef SDI_VERSION_2
#ifndef SDIPICO
  //tv : grab frame, not exclusive with the other actions, and not a job because there are not prioritary and sometimes missed their starting date
  static bool mvt=false;
  if(sdi_grab!=NULL)//not found if already proceeded
    {
      if ( (sdi_grab->grab==true) )
	{
	  if (obj_moving>0) mvt=true;
	  if (mvt==true)
	    {
	      if(sdi_v2_grab_function(track_info->imi[track_info->c_i])==0)
		{
		  mvt=false;
		  sdi_grab->grab=false;
		}
	    }
	}
    }
#endif
#endif       //#ifndef SDI_VERSION_2
  if (working_gen_record != NULL)
    {
      temp_gen_record = working_gen_record;
      abs_pos = working_gen_record->abs_pos - 1;
      abs_pos = (abs_pos < 0) ? 0 : abs_pos;
      page_n = abs_pos / working_gen_record->page_size;
      i_page = abs_pos % working_gen_record->page_size;
      status = working_gen_record->action_status[page_n][i_page];
      abs_pos = working_gen_record->abs_pos;
      abs_pos = (abs_pos < 0) ? 0 : abs_pos;   // abs_pos may be negative when we start
      page_n = abs_pos / working_gen_record->page_size;
      i_page = abs_pos % working_gen_record->page_size;
      working_gen_record->action_status[page_n][i_page] = status;
      // be careful record_action will set working_gen_record to NULL at the end
      if (working_gen_record->record_action != NULL) working_gen_record->record_action(gt->imi[ci], ci, gt->ac_i, 0);
      i = transfer_last_info_from_rolling_buffer_to_record(gt, ci, temp_gen_record);
      if (i)
	{
	  snprintf(buf, 512, "Transfert pb %d instead of %d !", i, temp_gen_record->one_im_data_size);
	  change_param_label_string(buf, makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
	  param_label_modified = 1;
	}
    }
  return 0;
}
#else
int track_in_x_y_timer(imreg *imr, O_i *oi, int n,  DIALOG *d, long long t, unsigned long dt, void *p, int n_inarow)
{
    int ci, i, ci_1, ci_2, c_b, ci_10; // , cl2, cl, cw
    int  rs232used = 0, rs232_will_be_used = 0; // , *xi, *yi dis_rilter = 0,
#ifdef PI_E709 //tv 120317 : when pifoc used with PIE709 controller (independant usb channel)
    int pi_e709used = 0;
    int pi_e709_com_period = 64; //try different refresh period (in frame) for reading pifoc position
	//je mets 64 de toute façon pi709 raconte de la merde et je fais pas de servo
#endif
#ifdef ZMAG_APT //tv 120317 : when zmag controlled by apt (independant usb channel)
    int zmag_apt_used = 0;
    int zmag_apt_com_period = 64;
#endif
    int f_conv = 0, limit = 0;//, k; // , focus_chg_by_prog = 0, focus_check = 0
    int page_n, i_page, abs_pos, status;
    BITMAP *imb = NULL;
    g_track *gt = NULL;
    b_track *bt = NULL;
    g_record *temp_gen_record = NULL;
    static int obj_moving = 0, zmag_motion_on = 0, vcap_servo_on = 0, limits_on = 0;// rot_flag_im_start = 0,
    static char buf[512];
# if defined(FLOW_CONTROL) || defined(FLOW_CONTROL_DUMMY)
    int rs232_2_used = 0;
    static char buf2[256] = {0}, buf3[256] = {0};
# endif
    float tmpl;
    f_action *ap;
    /* we grab the general structure */
    (void)imr;
    (void)d;
    gt = (g_track *)p;


    if (gt->ac_i % 64 == 0)
    {
        if (eval_load_from_timing_rolling_buffer(&prev_load, &prev_freq) == 0)
        {
            //change_Freq_string(freq);
            Pico_param.camera_param.camera_frequency_in_Hz = prev_freq;
            //change_Load_string(load);
        }
    }
    gt->ac_i++;                     // we increment the image number and wrap it eventually
    //gt->c_i++;                     // we increment the image number and wrap it eventually
    ci_1 = ci = gt->c_i;
    ci_2 = ci_1 - 1;
    if (ci_2 < 0) ci_2 += TRACKING_BUFFER_SIZE;
    ci_10 = ci_1 - 9;
    if (ci_10 < 0) ci_10 += TRACKING_BUFFER_SIZE;
    ci++;
    ci = (ci < gt->n_i) ? ci : 0;
    gt->imi[ci] = n;                                 // we save image" number
    gt->imit[ci] = n_inarow;
    gt->imt[ci] = t;                                 // image time
    gt->imdt[ci] = dt;
    gt->c_i = ci;

    if (oi->bmp.stuff != NULL) imb = (BITMAP *)oi->bmp.stuff;
    // we grab the video bitmap of the IFC image
    prepare_image_overlay(oi);

    if (oi) gt->bead_fence_in_pixel = (int)(0.5 + (gt->bead_fence / oi_TRACK->dy));
    gt->obj_pos[ci] = read_last_Z_value();

#ifdef SDI_VERSION
    if (track_info->SDI_mode == 1)// && sdi_g_fov != NULL)
    {
      sdi_1_proceed_fringes(gt, oi, imb, ci, n);
    }
    else  //ring tracking
#endif  // fin #ifdef SDI_VERSION
    {
        if (gt->n_b > 0)
        {
            gt->obj_servo_action = 0;
#ifdef OPENCL

            if (is_gpu_tracking_enable())
            {
                gt->is_using_gpu = true;
                proceed_beads_gpu(oi, gt, ci);
            }

            if (is_normal_tracking_enable())
#endif
            {
                gt->is_using_gpu = false;

                #ifdef SDI_VERSION_2
                if (gt->SDI_2_track_free)
                {
                if (gt->dsfreebeads)
                {
                  free_data_set(gt->dsfreebeads);
                  gt->dsfreebeads = NULL;
                }
                gt->dsfreebeads = SDI_2_find_beads_in_partial_OI(oi, 0, oi->im.nx, gt->track_free_y_low, gt->track_free_y_high,track_info->track_free_lum_threshold, track_info->track_free_xw, track_info->track_free_yw,track_info->track_free_window_x,track_info->track_free_window_y);
                if (gt->dsfreebeads != NULL)
                {
                  if (gt->dsfreebeads->nx>0) gt->do_move_free_bead = 1;
                  else
                  {
                    free_data_set(gt->dsfreebeads);
                    gt->dsfreebeads = NULL;
                  }
                }
            }
            #endif
              //  #pragma omp parallel for
                for (c_b = 0; c_b < gt->n_b; c_b++)
                {
                    proceed_bead(gt->bd[c_b], c_b, gt, imb, oi, ci, n);
                }
            }
        }
    }
# ifdef USE_MLFI
    if (beadnb_w_z_switched_to_mfi >= 0 && beadnb_w_z_switched_to_mfi < gt->n_b)
        {
            gt->bd[beadnb_w_z_switched_to_mfi]->z[ci] = grab_previous_mfi_data();
        }
#endif

    i = find_remaining_action(n);
    if (last_zmag_pid.proceeded_ask_bo_be_clear > last_zmag_pid.acknowledge)
    {
        last_zmag_pid.proceeded_RT = 0;
        last_zmag_pid.acknowledge = n;
        RT_debug("im %d zmag PID ask to clear proceed set to 0\n", gt->imi[ci]);
    }
    if (last_rot_pid.proceeded_ask_bo_be_clear > last_rot_pid.acknowledge)
    {
        last_rot_pid.proceeded_RT = 0;
        last_rot_pid.acknowledge = n;
        RT_debug("im %d rot PID ask to clear proceed set to 0\n", gt->imi[ci]);
    }
    if (last_T0_pid.proceeded_ask_bo_be_clear > last_T0_pid.acknowledge)
    {
        last_T0_pid.proceeded_RT = 0;
        last_T0_pid.acknowledge = n;
    }
    if (last_zmag_read.proceeded_ask_bo_be_clear > last_zmag_read.acknowledge)
    {
        last_zmag_read.proceeded_RT = 0;
        last_zmag_read.acknowledge = n;
        RT_debug("im %d zmag read ask to clear proceed set to 0\n", gt->imi[ci]);
    }
    if (last_rot_read.proceeded_ask_bo_be_clear > last_rot_read.acknowledge)
    {
        last_rot_read.proceeded_RT = 0;
        last_rot_read.acknowledge = n;
        RT_debug("im %d ROT read ask to clear proceed set to 0\n", gt->imi[ci]);
    }
    if (last_led_read.proceeded_ask_bo_be_clear > last_led_read.acknowledge)
    {
        last_led_read.proceeded_RT = 0;
        last_led_read.acknowledge = n;
        RT_debug("im %d LED read ask to clear proceed set to 0\n", gt->imi[ci]);
    }
    if (last_T_read.proceeded_ask_bo_be_clear > last_T_read.acknowledge)
    {
        last_T_read.proceeded_RT = 0;
        last_T_read.acknowledge = n;
    }

    //gt->status_flag[ci] = gt->status_flag[ci_1];
    rs232_will_be_used = gt->status_flag[ci] = (PARTS_MOVING & gt->status_flag[ci_1]);
    rs232_will_be_used = (ci % gt->im_temperature_check == 0) ? 0 : rs232_will_be_used;
    gt->obj_pos_cmd[ci] = gt->obj_pos_cmd[ci_1];

    if (gt->status_flag[gt->c_i]&USER_MOVING_OBJ) gt->obj_pos_cmd[ci] = gt->obj_pos[ci_1];
    gt->zmag_cmd[ci] = gt->zmag_cmd[ci_1];
    gt->rot_mag_cmd[ci] = gt->rot_mag_cmd[ci_1];
    gt->status_flag[ci] |= (gt->status_flag[ci_1] & (VCAP_SERVO_ON | POSITIVE_LIMIT_ON | NEGATIVE_LIMIT_ON));
    gt->Vcap_zmag[ci] = gt->Vcap_zmag[ci_1];

    if (gt->obj_servo_action)
    {
        if (set_Z_value(gt->new_obj_servo_pos) == 0)
        {
            gt->status_flag[ci] |= LOCK_MOVING_OBJ;
            gt->obj_pos_cmd[ci] = gt->new_obj_servo_pos;
            obj_moving = 1;
        }
        rs232used = 1;
    }
    if ((gt->com_skip_fr != 0) && (ci % gt->com_skip_fr != 0)) rs232used = 1;    // we limit com rate

    //list of actions to be done
    while ((i = find_next_action(n)) >= 0)
    {
        ap = action_pending + i;

//objective actions when controlled by pie709
#ifdef PI_E709 //tv 120317 : when pie709 used, pi_e709used replaces rs232used, for now i still use rs232_will_be_used since it is common
    if (rs232_will_be_used == 0 && pi_e709used == 0)
    {
      if (ap->type == MV_OBJ_ABS)
      {
        if (set_Z_value(ap->value) == 0)
        {
            gt->status_flag[ci] |= OBJ_MOVING;
            gt->obj_pos_cmd[ci] = ap->value;
            obj_moving = focus_transient;
            ap->proceeded = 1;
        }
        pi_e709used = 1;
      }
      else if (ap->type == MV_OBJ_REL)
      {
        if (set_Z_value(gt->obj_pos_cmd[ci_1] + ap->value) == 0)
        {
            gt->obj_pos_cmd[ci] = gt->obj_pos_cmd[ci_1] + ap->value;
            gt->status_flag[ci] |= OBJ_MOVING;
            obj_moving = focus_transient;
            ap->proceeded = 1;
        }
        pi_e709used = 1;
	  }
	}
#endif

//TV : zmag actions when apt used in separate thread : the effective usb comm is not called in the image processing functions. thus, the action only consists in matching the timestamps
//between the SW timestamps of the 2 data (frame and zmag) obtained with my_uclock.
        if (rs232_will_be_used == 0 && rs232used == 0)
        {
            if (ap->type == CHG_TEMPERATURE)
            {
              if (set_temperature(ap->value) == 0) ap->proceeded = 1;
              rs232used = 1;
            }
#ifndef PI_E709
            else if (ap->type == MV_OBJ_ABS)
            {
                if (set_Z_value(ap->value) == 0)
                {
                    gt->status_flag[ci] |= OBJ_MOVING;
                    gt->obj_pos_cmd[ci] = ap->value;
                    //focus_chg_by_prog = 1;
                    obj_moving = focus_transient;
                    ap->proceeded = 1;
                }
                rs232used = 1;
            }
            else if (ap->type == MV_OBJ_REL)
            {
                if (set_Z_value(gt->obj_pos_cmd[ci_1] + ap->value) == 0)
                {
                    gt->obj_pos_cmd[ci] = gt->obj_pos_cmd[ci_1] + ap->value;
                    gt->status_flag[ci] |= OBJ_MOVING;
                    //focus_chg_by_prog = 1;
                    obj_moving = focus_transient;
                    ap->proceeded = 1;
                }
                rs232used = 1;
            }
#endif
            else if (ap->type == MV_ZMAG_REL)
            {
#ifdef ZMAG_THREAD
zmag_continous_polling = false;
#endif
                if (set_magnet_z_value(gt->zmag_cmd[ci_1] + ap->value) == 0)
                {
                    gt->zmag_cmd[ci] = gt->zmag_cmd[ci_1] + ap->value;
                    if (gt->zmag_cmd[ci] > absolute_maximum_zmag) gt->zmag_cmd[ci] = absolute_maximum_zmag;
                    gt->zmag_positive_limit_when_on = gt->zmag_negative_limit_when_on = 0;
                    if (v_mag)
                    {
                        // we try to predict travelling time
                        zmag_motion_on = (int)(Pico_param.camera_param.camera_frequency_in_Hz * fabs(gt->zmag_cmd[ci] - gt->zmag_cmd[ci_1]) / v_mag);
                        zmag_motion_on += zmag_transient;
                    }
                    else zmag_motion_on = zmag_transient;
                    gt->zmag_limit_on_start = gt->status_flag[ci_1] & (NEGATIVE_LIMIT_ON | POSITIVE_LIMIT_ON);
                    gt->status_flag[ci] |= ZMAG_MOVING;
                    last_im_who_asked_menu_update = gt->imi[ci];
                    ap->proceeded = 1;
                    RT_debug("im %d set zmag REL val %g \n", gt->imi[ci], ap->value);
#ifdef ZMAG_THREAD
zmag_continous_polling = true;
#endif
                }
                rs232used = 1;
            }
            else if (ap->type == MV_ZMAG_ABS)
            {
#ifdef ZMAG_THREAD
zmag_continous_polling = false;
#endif
                if (set_magnet_z_value(ap->value) == 0)
                {
                    gt->zmag_cmd[ci] = ap->value;
                    if (gt->zmag_cmd[ci] > absolute_maximum_zmag) gt->zmag_cmd[ci] = absolute_maximum_zmag;
                    gt->zmag_positive_limit_when_on = gt->zmag_negative_limit_when_on = 0;
                    if (v_mag)
                    {
                        // we try to predict travelling time
                        zmag_motion_on = (int)(Pico_param.camera_param.camera_frequency_in_Hz * fabs(gt->zmag_cmd[ci] - gt->zmag_cmd[ci_1]) / v_mag);
                        zmag_motion_on += zmag_transient;
                    }
                    else zmag_motion_on = zmag_transient;
                    gt->zmag_limit_on_start = gt->status_flag[ci_1] & (NEGATIVE_LIMIT_ON | POSITIVE_LIMIT_ON);
                    gt->status_flag[ci] |= ZMAG_MOVING;
                    last_im_who_asked_menu_update = gt->imi[ci];
                    ap->proceeded = 1;
                    RT_debug("im %d set zmag ABS val %g \n", gt->imi[ci], ap->value);
#ifdef ZMAG_THREAD
zmag_continous_polling = true;
#endif
                }
                rs232used = 1;
            }
            else if (ap->type == READ_ZMAG_VAL)
            {
#ifdef ZMAG_THREAD
zmag_continous_polling = false;
#endif
                zmag_motion_on = zmag_transient;
                last_im_who_asked_menu_update = gt->imi[ci];
                ap->proceeded = 1;
#ifdef ZMAG_THREAD
zmag_continous_polling = true;
#endif
            }
            else if (ap->type == CHG_ZMAG_REF)
            {
#ifdef ZMAG_THREAD
zmag_continous_polling = false;
#endif
              if (set_magnet_z_ref_value(ap->value) == 0)
              {
                gt->zmag_cmd[ci] = ap->value;
                gt->status_flag[ci] |= REF_ZMAG_CHANGED;
                n_magnet_z_inst = last_n_magnet_z = ap->value;
                n_magnet_z = prev_zmag = ap->value;
                last_im_who_asked_menu_update = gt->imi[ci];
                ap->proceeded = 1;
                zmag_motion_on = zmag_transient;
#ifdef ZMAG_THREAD
zmag_continous_polling = true;
#endif
              }
              rs232used = 1;
            }
            else if (ap->type == CHG_ZMAG_FACTOR) //change the translation factor between motor and magnets tv 09/03/17
            {
#ifdef ZMAG_THREAD
zmag_continous_polling = false;
#endif
              if (set_magnet_z_factor_value(ap->value) == 0)
              {
                gt->zmag_cmd[ci] = ap->value;
                gt->status_flag[ci] |= ZMAG_FACTOR_CHANGED;
                n_magnet_z_inst = last_n_magnet_z = ap->value;
                n_magnet_z = prev_zmag = ap->value;
                last_im_who_asked_menu_update = gt->imi[ci];
                ap->proceeded = 1;
                zmag_motion_on = zmag_transient;
#ifdef ZMAG_THREAD
zmag_continous_polling = false;
#endif
              }
              rs232used = 1;
            }
            else if (ap->type == READ_ROT_VAL)
            {
                rot_moving = rot_transient;
                last_im_who_asked_menu_update = gt->imi[ci];
                ap->proceeded = 1;
            }
            else if (ap->type == MV_ROT_REL)
            {
                if (set_rot_value(gt->rot_mag_cmd[ci_1] + ap->value) == 0)
                {
                    gt->rot_mag_cmd[ci] = gt->rot_mag_cmd[ci_1] + ap->value;
                    gt->status_flag[ci] |= ROT_MOVING;
                    last_im_who_asked_menu_update = gt->imi[ci];//rot_flag_im_start =
                    ap->proceeded = 1;
                }
                rs232used = 1;
            }
            else if (ap->type == CHG_ROT_REF)
            {
                if (set_rot_ref_value(ap->value) == 0)
                {
                    gt->rot_mag_cmd[ci] = ap->value;
                    last_n_rota = prev_mag_rot = ap->value;
                    n_rota_inst = n_rota = ap->value;
                    gt->status_flag[ci] |= REF_ROT_CHANGED;
                    last_im_who_asked_menu_update = gt->imi[ci];
                    ap->proceeded = 1;
                    rot_moving = rot_transient;
                }
                rs232used = 1;
            }
            else if (ap->type == MV_ROT_ABS)
            {
                if (set_rot_value(ap->value) == 0)
                {
                    gt->rot_mag_cmd[ci] = ap->value;
                    gt->status_flag[ci] |= ROT_MOVING;
                    last_im_who_asked_menu_update = gt->imi[ci];//rot_flag_im_start =
                    ap->proceeded = 1;
                }
                rs232used = 1;
            }
            else if (ap->type == CHG_ZMAG_PID)
            {
                //tv 1302 : can change speed of zmag with different config of control
                int tmp = 0;
# if defined(PI_C843) || defined(PI_C843_TRS) || defined(ZMAG_APT) || defined(RAWHID_V2)
                tmp = set_zmag_motor_speed(ap->value);
                if (true)
# else
                if (set_zmag_motor_pid(ap->spare, (int)(ap->value)) == 0)
# endif
                {
                    //gt->status_flag[ci] |= REF_ZMAG_CHANGED;
                    if (ap->spare == 7) gt->status_flag[ci] |= ZMAG_SPEED_CHANGE;
                    last_zmag_pid.value = ap->value;
                    last_zmag_pid.spare = ap->spare;
                    last_zmag_pid.type = CHG_ZMAG_PID;
                    last_zmag_pid.im_done = gt->imi[ci];
                    last_zmag_pid.proceeded_RT = 1;
                    ap->proceeded = 1;
                    RT_debug("im %d set zmag PID param %d val %g\n", gt->imi[ci], ap->spare, last_zmag_pid.value);
                    //win_printf_OK("set speed to %3.3f", last_zmag_pid.value);
                }
                else
                {
                    snprintf(buf, 512, "Zmag PID error sending %d to param %d!",  tmp, ap->spare);//(int)(ap->value), ap->spare);
                    change_param_label_string(buf, makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
                    RT_debug("im %d set zmag PID param %d val %g FAILED !\n", gt->imi[ci], ap->spare, last_zmag_pid.value);
                }
                rs232used = 1;
            }
            else if (ap->type == CHG_ROT_PID)
            {
                if (set_rotation_motor_pid(ap->spare, (int)(ap->value)) == 0)
                {
                    //gt->status_flag[ci] |= REF_ZMAG_CHANGED;
                    if (ap->spare == 7)
                    {
                        gt->status_flag[ci] |= ROT_SPEED_CHANGE;
                    }

                    last_rot_pid.value = ap->value;
                    last_rot_pid.spare = ap->spare;
                    last_rot_pid.type = CHG_ROT_PID;
                    last_rot_pid.im_done = gt->imi[ci];
                    last_rot_pid.proceeded_RT = 1;
                    ap->proceeded = 1;
                    RT_debug("im %d chg rot PID param %d val %g proceed set to 1\n", gt->imi[ci], ap->spare, last_rot_pid.value);
                }
                else
                {
                    snprintf(buf, 512, "ROT PID error sending %d to param %d!", (int)(ap->value), ap->spare);
                    change_param_label_string(buf, makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
                    RT_debug("im %d chg rot PID param %d val %g FAILED\n", gt->imi[ci], ap->spare, last_rot_pid.value);
                }

                rs232used = 1;
            }
            else if (ap->type == CHG_T0_PID)
            {
                if (set_T0_pid != NULL)
                {
                    if (set_T0_pid(ap->spare, (int)(ap->value)) == 0)
                    {
                        last_T0_pid.value = ap->value;
                        last_T0_pid.spare = ap->spare;
                        last_T0_pid.type = CHG_T0_PID;
                        last_T0_pid.im_done = gt->imi[ci];
                        last_T0_pid.proceeded_RT = 1;
                        ap->proceeded = 1;
                    }
                }

                rs232used = 1;
            }
            else if (ap->type == GET_FOCUS_POS)
            {
                //focus_check = 1;
                ap->imi += ap->period;
                ap->proceeded = 0;
            }
            else if (ap->type == READ_ZMAG_PID)
            {
                if (get_zmag_motor_pid != NULL)
                {
                    last_zmag_pid.value = (float)get_zmag_motor_pid(ap->spare);
                }

                last_zmag_pid.spare = ap->spare;
                last_zmag_pid.type = READ_ZMAG_PID;
                last_zmag_pid.im_done = gt->imi[ci];
                last_zmag_pid.proceeded_RT = 1;
                ap->proceeded = 1;
                rs232used = 1;
                RT_debug("im %d zmag PID read param %d val %g\n", gt->imi[ci], ap->spare, last_zmag_pid.value);
            }
            else if (ap->type == READ_ROT_PID)
            {
                if (get_rotation_motor_pid != NULL)
                {
                    last_rot_pid.value = (float)get_rotation_motor_pid(ap->spare);
                }

                last_rot_pid.spare = ap->spare;
                last_rot_pid.type = READ_ROT_PID;
                last_rot_pid.im_done = gt->imi[ci];
                last_rot_pid.proceeded_RT = 1;
                ap->proceeded = 1;
                rs232used = 1;
                RT_debug("im %d rot PID read param %d val %g proceed set to 1\n", gt->imi[ci], ap->spare, last_rot_pid.value);
            }
            else if (ap->type == READ_T0_PID)
            {
                if (get_T0_pid != NULL)
                {
                    last_T0_pid.value = get_T0_pid(ap->spare);
                }

                last_T0_pid.spare = ap->spare;
                last_T0_pid.type = READ_T0_PID;
                last_T0_pid.im_done = gt->imi[ci];
                last_T0_pid.proceeded_RT = 1;
                ap->proceeded = 1;
                rs232used = 1;
            }
            else if (ap->type == READ_LED_VAL)
            {
                if (get_light != NULL)
                {
                    track_info->led_level = get_light();
                }
                else
                {
                    track_info->led_level = 100.0;
                }

                last_led_read.value = track_info->led_level;
                last_led_read.spare = ap->spare;
                last_led_read.type = READ_LED_VAL;
                last_led_read.im_done = gt->imi[ci];
                last_led_read.proceeded_RT = 1;
                ap->proceeded = 1;
                rs232used = 1
                            ;
                RT_debug("im %d LED read val %g proceed set to 1\n", gt->imi[ci], last_led_read.value);
            }
            else if (ap->type == CHG_LED_VAL)
            {
                if (set_light != NULL)
                {
                    set_light(ap->value);
                }

                track_info->led_level = ap->value;
                ap->proceeded = 1;
                gt->status_flag[ci] |= LED_CURRENT_CHANGED;
                rs232used = 1;
                RT_debug("im %d LED set val %g proceed set to 1\n", gt->imi[ci], ap->value);
            }
        }   // end of action that may need rs232 access

        /*
           else
           {
        //my_set_window_title("Action %d posponed rs232 %d %d",ap->type,rs232_will_be_used, rs232used);
        ap->imi = n+1;
        }
        */
# if defined(FLOW_CONTROL) || defined(FLOW_CONTROL_DUMMY)

        if ((ap->type == RINCE_SAMPLE) || (ap->type == INJECT_ENZYME) || (ap->type == INJECT_TWO_ENZYMES))
        {
            RT_debug("action %d flow %d phase %d\n", i, ((ap->type == RINCE_SAMPLE) ? 1 : 0), ap->phase);

            if (ap->type == RINCE_SAMPLE)
            {
                gt->status_flag[ci] &= ~(RINCING_FLOW_ON);
            }
            else if (ap->type == INJECT_ENZYME)
            {
                gt->status_flag[ci] &= ~(ENZYME_FLOW_ON | 0x07);
            }
            else if (ap->type == INJECT_TWO_ENZYMES)
            {
                gt->status_flag[ci] &= ~(ENZYME_2_FLOW_ON);
            }

            //gt->status_flag[ci] &= ~(RINCING_FLOW_ON | ENZYME_FLOW_ON | 0x07);
            if (ap->phase == PENDING)
            {
                if (rs232_2_used == 0)
                {
                    flow_control_read_MOT_vol(ap->spare, &(ap->initial_pos_mm), &(ap->initial_micro_liter));
                    rs232_2_used = 1;
                    RT_debug("%d flow initial read  %g mm %g microliters\n", i, ap->initial_pos_mm, ap->initial_micro_liter);
                    ap->phase = LAUNCHING_ACTION;
                    ap->im_action_duration = 0;
                }

                ap->imi = n + 1;
            }
            else if (ap->phase == LAUNCHING_ACTION)
            {
                if (rs232_2_used == 0)
                {
                    flow_control_sent_MOT_vol(ap->spare, ap->value, ap->action_duration);
                    RT_debug("%d flow sent %g microliters for %g s\n", i, ap->value, ap->action_duration);
                    rs232_2_used = 1;
                    ap->imi = n + ap->check_latency;
                    ap->started_indeed = gt->imi[ci];
                    ap->nb_check_read = 1;
                    ap->phase = RUNNING_ACTION;
                    ap->im_action_duration = 1;
                    ap->message_sent = 0;

                    if (ap->type == RINCE_SAMPLE)
                    {
                        gt->status_flag[ci] |= RINCING_FLOW_ON;
                        my_set_window_title("Rin. %g mu-l in %f S %s %s", ap->value
                                            , ap->action_duration, buf2, buf3);
                        snprintf(buf, 512, "Rin. %5.1f in %5.1f S %s %s", ap->value
                                 , ap->action_duration, buf2, buf3);
                        change_param_label_string(buf, makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
                        param_label_modified = 1;
                    }
                    else if (ap->type == INJECT_ENZYME)
                    {
                        gt->status_flag[ci] |= ENZYME_FLOW_ON | (0x07 & ap->spare);
                        //my_set_window_title("Inj. %5.1f mu-l in %5.1f S",ap->value
                        //        ,ap->action_duration);
                        snprintf(buf2, 256, "Inj. %5.1f in %5.1f S", ap->value
                                 , ap->action_duration);
                        change_param_label_string(buf, makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
                        param_label_modified = 1;
                    }
                    else if (ap->type == INJECT_TWO_ENZYMES)
                    {
                        gt->status_flag[ci] |= ENZYME_2_FLOW_ON;
                        //my_set_window_title("Inj. %5.1f mu-l in %5.1f S",ap->value
                        //        ,ap->action_duration);
                        snprintf(buf3, 256, "Inj. %5.1f in %5.1f S", ap->value
                                 , ap->action_duration);
                        change_param_label_string(buf, makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
                        param_label_modified = 1;
                    }
                }
                else
                {
                    ap->imi = n + 1;
                }
            }
            else if (ap->phase == RUNNING_ACTION)
            {
                if (ap->type == RINCE_SAMPLE)
                {
                    gt->status_flag[ci] |= RINCING_FLOW_ON;
                    RT_debug("rincing running %d/%d\n", gt->imi[ci] - ap->started_indeed, ap->maximum_duration);
                }
                else if (ap->type == INJECT_ENZYME)
                {
                    gt->status_flag[ci] |= ENZYME_FLOW_ON | (0x07 & ap->spare);
                    RT_debug("injection running %d/%d\n", gt->imi[ci] - ap->started_indeed, ap->maximum_duration);
                }
                else if (ap->type == INJECT_TWO_ENZYMES)
                {
                    gt->status_flag[ci] |= ENZYME_2_FLOW_ON;
                    RT_debug("injection 2 running %d/%d\n", gt->imi[ci] - ap->started_indeed, ap->maximum_duration);
                }

                if (gt->imi[ci] - ap->started_indeed > ap->maximum_duration)
                {
                    ap->phase = ABNORMAL_END;
                }

                if (gt->itemp_mes == 0 && ap->message_sent == 0)
                {
                    if (ap->type == RINCE_SAMPLE)
                    {
                        snprintf(gt->temp_mes, 32, "RIN=%5.1f-%5.1f", ap->value, ap->action_duration);
                    }
                    else if (ap->type == INJECT_TWO_ENZYMES)
                    {
                        snprintf(gt->temp_mes, 32, "LI%d=%5.1f-%5.1f", ap->spare, ap->value, ap->action_duration);
                    }
                    else    snprintf(gt->temp_mes, 32, "EN%d=%5.1f-%5.1f", ap->spare, ap->value
                                         , ap->action_duration);

                    gt->ntemp_mes = gt->itemp_mes = strlen(gt->temp_mes) + 1;
                    ap->message_sent = 1;
                }

                if ((rs232_2_used == 0) && ((gt->imi[ci] - ap->started_indeed) >
                                            (ap->nb_check_read * ap->check_latency)))
                {
                    ap->nb_check_read++;
                    flow_control_read_MOT_vol(ap->spare, &(ap->pos_mm), &(ap->micro_liter));
                    RT_debug("%d flow read %g mm %g microliters\n", i, ap->pos_mm, ap->micro_liter);
                    rs232_2_used = 1;
                    ap->imi = n + ap->check_latency;

                    if (ap->type == RINCE_SAMPLE)
                    {
                        my_set_window_title("Rincing %5.1f/%5.1f %s %s",
                                            ap->micro_liter, ap->value, buf2, buf3);
                        //ap->micro_liter - ap->initial_micro_liter, ap->value);
                        snprintf(buf, 512, "Rin. %5.1f/%5.1f %s %s",
                                 (ap->micro_liter - ap->initial_micro_liter), ap->value, buf2, buf3);
                        //ap->micro_liter - ap->initial_micro_liter, ap->value);
                        change_param_label_string(buf, makecol32(40, 40, 255), makecol32(240, 240, 240), 0);
                        param_label_modified = 1;
                    }
                    else if (ap->type == INJECT_ENZYME)
                    {
                        //my_set_window_title("Injecting %5.1f/%5.1f micro-liters",
                        //        ap->pos_mm, ap->value);
                        //ap->micro_liter - ap->initial_micro_liter, ap->value);
                        snprintf(buf2, 256, "Inj. %5.1f/%5.1f ",
                                 (ap->micro_liter - ap->initial_micro_liter), ap->value);
                        //ap->micro_liter - ap->initial_micro_liter, ap->value);
                        change_param_label_string(buf, makecol32(40, 40, 255), makecol32(240, 240, 240), 0);
                        param_label_modified = 1;
                    }
                    else if (ap->type == INJECT_TWO_ENZYMES)
                    {
                        //my_set_window_title("Injecting 2 %5.1f/%5.1f micro-liters",
                        //        ap->pos_mm, ap->value);
                        //ap->micro_liter - ap->initial_micro_liter, ap->value);
                        snprintf(buf3, 256, "Inj2. %5.1f/%5.1f ",
                                 (ap->micro_liter - ap->initial_micro_liter), ap->value);
                        //ap->micro_liter - ap->initial_micro_liter, ap->value);
                        change_param_label_string(buf, makecol32(40, 40, 255), makecol32(240, 240, 240), 0);
                        param_label_modified = 1;
                    }

                    if (fabs(ap->micro_liter - ap->initial_micro_liter
                             - ap->value) < 0.1)
                    {
                        ap->phase = CHECKING_ACTION;
                    }
                }
                else
                {
                    ap->imi = n + 1;
                }

                ap->im_action_duration++;
            }
            else if (ap->phase == CHECKING_ACTION)
            {
                if ((rs232_2_used == 0) && ((gt->imi[ci] - ap->started_indeed) >
                                            (ap->nb_check_read * ap->check_latency)))
                {
                    ap->nb_check_read++;
                    flow_control_read_MOT_vol(ap->spare, &(ap->pos_mm), &(ap->micro_liter));
                    rs232_2_used = 1;
                    ap->imi = n + ap->check_latency;

                    if (ap->type == RINCE_SAMPLE)
                    {
                        my_set_window_title("Rincing %5.1f/%5.1f %s %s",
                                            (ap->micro_liter - ap->initial_micro_liter), ap->value, buf2, buf3);
                        snprintf(buf, 512, "Rincing %5.1f/%5.1f %s %s",
                                 (ap->micro_liter - ap->initial_micro_liter), ap->value, buf2, buf3);
                        change_param_label_string(buf, makecol32(40, 40, 255), makecol32(240, 240, 240), 0);
                        param_label_modified = 1;
                    }
                    else if (ap->type == INJECT_ENZYME)
                    {
                        //my_set_window_title("Injecting %5.1f/%5.1f micro-liters",
                        //        ap->micro_liter - ap->initial_micro_liter, ap->value);
                        snprintf(buf2, 256, "Inj. %5.1f/%5.1f ",
                                 (ap->micro_liter - ap->initial_micro_liter), ap->value);
                        change_param_label_string(buf, makecol32(40, 40, 255), makecol32(240, 240, 240), 0);
                        param_label_modified = 1;
                    }
                    else if (ap->type == INJECT_TWO_ENZYMES)
                    {
                        //my_set_window_title("Injecting bis %5.1f/%5.1f micro-liters",
                        //        ap->micro_liter - ap->initial_micro_liter, ap->value);
                        snprintf(buf3, 256, "Inj2. %5.1f/%5.1f ",
                                 (ap->micro_liter - ap->initial_micro_liter), ap->value);
                        change_param_label_string(buf, makecol32(40, 40, 255), makecol32(240, 240, 240), 0);
                        param_label_modified = 1;
                    }

                    if ((gt->imi[ci] - ap->started_indeed - ap->im_action_duration) > ap->check_duration)
                    {
                        if (ap->type == INJECT_TWO_ENZYMES)
                        {
                            buf3[0] = 0;
                        }

                        if (ap->type == INJECT_ENZYME)
                        {
                            buf2[0] = 0;
                        }

                        if (ap->type == RINCE_SAMPLE)
                        {
                            buf[0] = 0;
                        }

                        ap->phase = ENDED;
                        ap->proceeded = 1;
                        RT_debug("%d flow check %d/%d finished\n", i, (gt->imi[ci] - ap->started_indeed - ap->im_action_duration),
                                 ap->check_duration);
                    }
                    else
                    {
                        RT_debug("%d flow check %g mm %g microliters\n", i, ap->pos_mm, ap->micro_liter);
                    }
                }
                else
                {
                    ap->imi = n + 1;
                }
            }
            else if (ap->phase == ABNORMAL_END)
            {
                if (rs232_2_used == 0)
                {
                    flow_control_read_MOT_vol(ap->spare, &(ap->pos_mm), &(ap->micro_liter));
                    rs232_2_used = 1;

                    if (ap->type == RINCE_SAMPLE)
                    {
                        my_set_window_title("Pb in rincing %5.1f/%5.1f micro-liters %s %s",
                                            ap->micro_liter - ap->initial_micro_liter, ap->value, buf2, buf3);
                        snprintf(buf, 512, "Pb in rincing %5.1f/%5.1f %s %s",
                                 ap->micro_liter - ap->initial_micro_liter, ap->value, buf2, buf3);
                        change_param_label_string(buf, makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
                        param_label_modified = 1;
                    }
                    else if (ap->type == INJECT_ENZYME)
                    {
                        //my_set_window_title("Pb in injecting %5.1f/%5.1f micro-liters",
                        //        ap->micro_liter - ap->initial_micro_liter, ap->value);
                        snprintf(buf2, 256, "Pb in inj. %5.1f/%5.1f",
                                 ap->micro_liter - ap->initial_micro_liter, ap->value);
                        change_param_label_string(buf, makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
                        param_label_modified = 1;
                    }
                    else if (ap->type == INJECT_TWO_ENZYMES)
                    {
                        //my_set_window_title("Pb in injecting bis %5.1f/%5.1f micro-liters",
                        //        ap->micro_liter - ap->initial_micro_liter, ap->value);
                        snprintf(buf3, 256, "Pb in inj. bis %5.1f/%5.1f",
                                 ap->micro_liter - ap->initial_micro_liter, ap->value);
                        change_param_label_string(buf, makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
                        param_label_modified = 1;
                    }

                    ap->imi = n + 1;
                    ap->phase = ENDED;
                    ap->proceeded = 1;
                }
            }
        }

# endif

        if (ap->type == BEAD_CROSS_SAVED)
        {
            for (c_b = 0; c_b < gt->n_b; c_b++)
            {
                bt = gt->bd[c_b];

                if (bt->calib_im_fil == NULL || bt->in_image == 0)
                {
                    continue;
                }

                bt->saved_x = bt->x[gt->c_i]; // last_x_not_lost
                bt->saved_y = bt->y[gt->c_i]; // last_y_not_lost
            }

            my_set_window_title("Bead position saved");
            ap->proceeded = 1;
        }
        else if (ap->type == BEAD_CROSS_RESTORE)
        {
            for (c_b = 0; c_b < gt->n_b; c_b++)
            {
                bt = gt->bd[c_b];

                if (bt->calib_im_fil == NULL || bt->in_image == 0)
                {
                    continue;
                }

                bt->mx = bt->xc = bt->x0 = (int)bt->saved_x; //last_x_not_lost;
                bt->my = bt->yc = bt->y0 = (int)bt->saved_y; //last_y_not_lost;
            }

            my_set_window_title("Bead position restored");
            ap->proceeded = 1;
        }
        else if (ap->type == READ_GAIN_EXPO)
        {
            if (get_camera_gain != NULL)
            {
                get_camera_gain(&track_info->camera_gain, &track_info->camera_min_gain, &track_info->camera_max_gain, NULL);
            }

            if (get_camera_shutter_in_us != NULL)
                get_camera_shutter_in_us(&track_info->camera_exposure_time_us, &track_info->camera_min_exposure,
                                         &track_info->camera_max_exposure);

            ap->proceeded = 1;
            //gt->status_flag[ci] |= LED_CURRENT_CHANGED;
        }
        else if (ap->type == CHG_CAMERA_GAIN)
        {
            if (set_camera_gain != NULL)
                set_camera_gain(ap->value, &track_info->camera_gain,
                                &track_info->camera_min_gain, &track_info->camera_max_gain, NULL);

            ap->proceeded = 1;
            gt->status_flag[ci] |= CAMERA_GAIN_CHG;
        }
        else if (ap->type == CHG_CAMERA_EXPO)
        {
            if (set_camera_shutter_in_us != NULL)
                set_camera_shutter_in_us((int)ap->value, &track_info->camera_exposure_time_us,
                                         &track_info->camera_min_exposure, &track_info->camera_max_exposure);

            ap->proceeded = 1;
            gt->status_flag[ci] |= CAMERA_EXPOSURE_CHG;
        }
        else
        {
            ap->imi = n + 1;
            gt->status_flag[ci] &= ~REF_ROT_CHANGED;
            gt->status_flag[ci] &= ~REF_ZMAG_CHANGED;
            gt->status_flag[ci] &= ~ZMAG_FACTOR_CHANGED; //tv 090317
        }
    }

    if (manual_rincing)
    {
        if (manual_rincing_on == 0)
        {
            // we swith to rincing
            manual_rincing_on = 1;
            manual_rincing_start = my_uclock();
            rincing_time_ins = 0;
        }

        if (gt->itemp_mes == 0 && manual_rincing_on == 1)
        {
            manual_rincing_on = 2;
            snprintf(gt->temp_mes, 32, "RIN=0-1");
            gt->ntemp_mes = gt->itemp_mes = strlen(gt->temp_mes) + 1;
        }

        //rincing_time_ins = (float)(my_uclock()-manual_rincing_start)/get_my_uclocks_per_sec();
        if (((float)(my_uclock() - manual_rincing_start) / get_my_uclocks_per_sec()) >= rincing_time_ins)
        {
            my_set_window_title("Rincing ON %6.2fs", rincing_time_ins);
            snprintf(buf, 512, "Rincing ON (%6.2fs)", rincing_time_ins);
            change_param_label_string(buf, makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
            rincing_time_ins += 0.25;
            param_label_modified = 1;
        }

        gt->status_flag[ci] |= RINCING_FLOW_ON;
    }
    else
    {
        if (manual_rincing_on)
        {
            // we swith to rincing
            manual_rincing_on = 0;
            my_set_window_title("Rincing stopped %6.2fs", rincing_time_ins);
            snprintf(buf, 512, "Rincing stopped (%6.2fs)", rincing_time_ins);
            change_param_label_string(buf, makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
            param_label_modified = 1;
        }
    }

    if (manual_enzyme_injection)
    {
        if (manual_enzyme_injection_on == 0)
        {
            // we swith to rincing
            manual_enzyme_injection_on = 1;
            manual_enzyme_injection_start = my_uclock();
            enzyme_injection_time_ins = 0;
        }

        if (gt->itemp_mes == 0 && manual_enzyme_injection_on == 1)
        {
            manual_enzyme_injection_on = 2;
            snprintf(gt->temp_mes, 32, "EN%d=0-1", enzyme_nb);
            gt->ntemp_mes = gt->itemp_mes = strlen(gt->temp_mes) + 1;
        }

        if (((float)(my_uclock() - manual_enzyme_injection_start) / get_my_uclocks_per_sec()) >= enzyme_injection_time_ins)
        {
            my_set_window_title("Enzyme-%d injection ON %6.2fs", enzyme_nb, enzyme_injection_time_ins);
            snprintf(buf, 512, "Enzyme-%d injection ON (%6.2fs)", enzyme_nb, enzyme_injection_time_ins);
            change_param_label_string(buf, makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
            param_label_modified = 1;
            enzyme_injection_time_ins += 0.25;
        }

        gt->status_flag[ci] |= ENZYME_FLOW_ON | (0x07 & enzyme_nb);
    }
    else
    {
        if (manual_enzyme_injection_on)
        {
            // we just swith off rincing
            manual_enzyme_injection_on = 0;
            my_set_window_title("Enzyme-%d injection stopped %6.2fs", enzyme_nb, enzyme_injection_time_ins);
            snprintf(buf, 512, "Enzyme-%d injection stopped (%6.2fs)", enzyme_nb, enzyme_injection_time_ins);
            change_param_label_string(buf, makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
            param_label_modified = 1;
        }
    }

    /*
       gt->zmag[ci] = (gt->status_flag[ci] & ZMAG_MOVING)
       ? read_magnet_z_value() : what_is_present_z_mag_value();
       gt->rot_mag[ci] = (gt->status_flag[ci] & ROT_MOVING)
       ? read_rot_value() : what_is_present_rot_value();
       gt->obj_pos[ci] = (gt->status_flag[ci] & OBJ_MOVING)
       ? read_Z_value_accurate_in_micron() : gt->obj_pos[ci_1];
       */
    if ((gt->com_skip_fr == 0) || (ci % gt->com_skip_fr == 0))
    {
        if (zmag_motion_on > 1)
        {
            zmag_motion_on--;    // zmag just started we wait for motor to start
        }
        else
        {
            // limits impose to stop
            //if (gt->zmag_positive_limit_when_on || gt->zmag_negative_limit_when_on)   zmag_motion_on = 0;
            // if zmag does not moves anymore we stop grabing position after transient
            //else
            if ((gt->status_flag[ci_1] | ZMAG_MOVING_SAMPLED) && fabs(gt->zmag[ci_10] - gt->zmag[ci_1]) < 0.005)
            {
                zmag_motion_on = 0;
            }
            else
            {
                zmag_motion_on = 1;
            }
        }
    }

    zmag_moving = (zmag_motion_on > 0) ? ((vcap_servo_on) ? 4 * zmag_transient : zmag_transient)
                  : ((zmag_moving > 0) ? zmag_moving - 1 : 0);

#ifdef ZMAG_THREAD
  if(zmag_data->zmag_value[available_zmag_id]==true)
  {
    gt->zmag[ci] = zmag_data->zmag_value[available_zmag_id];
    prev_zmag = gt->zmag[ci_1];
    last_zmag_read.value = prev_zmag;
    last_zmag_read.type = READ_ZMAG_VAL;
    last_zmag_read.im_done = gt->imi[ci];
    last_zmag_read.proceeded_RT = 1;
  }
  else prev_zmag = gt->zmag[ci] = gt->zmag[ci_1];
  if (zmag_moving) gt->status_flag[ci] &= ~ZMAG_MOVING; //detects stops
#else
    if (rs232used)  // we wait
    {
        prev_zmag = gt->zmag[ci] = gt->zmag[ci_1];
    }
    else if (zmag_moving || ((ci & 0x0000002f) == 0x0f))
    {
        rs232used = 1;
        if (read_magnet_z_value_and_status(gt->zmag + ci, gt->Vcap_zmag + ci, &limit, &gt->vcap_servo_on) == 0)
        {
            prev_zmag = gt->zmag[ci];// = read_magnet_z_value();
            gt->zmag_from_Vcap = Pico_param.translation_motor_param.Zmag_vs_Vcap_poly_a0;
            gt->zmag_from_Vcap += gt->Vcap_zmag[ci] * Pico_param.translation_motor_param.Zmag_vs_Vcap_poly_a1;
            gt->zmag_from_Vcap += gt->Vcap_zmag[ci] * gt->Vcap_zmag[ci] * Pico_param.translation_motor_param.Zmag_vs_Vcap_poly_a2;
	    gt->status_flag[ci] |= ZMAG_MOVING | ZMAG_MOVING_SAMPLED; // reintroduced VC 2017-12-02
            last_zmag_read.value = prev_zmag;
            //last_zmag_read.spare = ap->spare;
            last_zmag_read.type = READ_ZMAG_VAL;
            last_zmag_read.im_done = gt->imi[ci];
            last_zmag_read.proceeded_RT = 1;
            RT_debug("im %d read zmag val %g \n", gt->imi[ci], prev_zmag);
            if ((gt->vcap_servo_on & 0x04))
            {
                //gt->status_flag[ci] |= VCAP_SERVO_ON;
                my_set_window_title("Motors Brake active! Zmag %g", gt->zmag[ci]);
                snprintf(buf, 512, "Motors brake active! Zmag %g", gt->zmag[ci]);
                change_param_label_string(buf, makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
                param_label_modified = 1;
                vcap_servo_on = 1;
                change_zmag_label_string("Brakes on", makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
                zmag_label_modified = 1;
            }
            else if ((gt->vcap_servo_on & 0x02))
            {
                //gt->status_flag[ci] |= VCAP_SERVO_ON;
                my_set_window_title("PID of Zmag motors inactivated !Zmag %g", gt->zmag[ci]);
                snprintf(buf, 512, "PID of Zmag motors inactivated !Zmag %g", gt->zmag[ci]);
                change_param_label_string(buf, makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
                param_label_modified = 1;
                vcap_servo_on = 1;
                change_zmag_label_string("Z PID Off!", makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
                zmag_label_modified = 1;
            }
            else if ((gt->vcap_servo_on & 0x01))
            {
                gt->status_flag[ci] |= VCAP_SERVO_ON;
                my_set_window_title("Zmag is servo by inductive sensor Zmag %g Vcap %g", gt->zmag[ci], gt->Vcap_zmag[ci]);
                snprintf(buf, 512, "Inductive sensor stops Zmag Vcap %g", gt->Vcap_zmag[ci]);
                change_param_label_string(buf, makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
                param_label_modified = 1;
                vcap_servo_on = 1;
                change_zmag_label_string("Zmag Vservo", makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
                zmag_label_modified = 1;
            }
            else if (fabs(gt->zmag[ci] - absolute_maximum_zmag) < 0.002)
            {
                //my_set_window_title("Zmag %g reaching mximum value %g", gt->zmag[ci], absolute_maximum_zmag);
                snprintf(buf, 512, "Zmag %g reaching mximum value %g", gt->zmag[ci], absolute_maximum_zmag);
                change_param_label_string(buf, makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
                param_label_modified = 1;
                zmag_label_modified = 1;
                vcap_servo_on = 1;
            }
            else if ((gt->Vcap_zmag[ci] > 0) && (gt->Vcap_zmag[ci] < 10))
            {
                if (Pico_param.translation_motor_param.Zmag_vs_Vcap_poly_a1 != 0)
                {
                    //my_set_window_title("Zmag approaching 0 Zmag %g Vcap %g", gt->zmag[ci], gt->Vcap_zmag[ci]);
                    snprintf(buf, 512, "Zmag %5.3f in range of sensor Vcap %g", gt->zmag_from_Vcap, gt->Vcap_zmag[ci]);
                }
                else
                {
                    my_set_window_title("Zmag approaching 0 Zmag %g Vcap %g", gt->zmag[ci], gt->Vcap_zmag[ci]);
                    snprintf(buf, 512, "Zmag in range of sensor Vcap %g", gt->Vcap_zmag[ci]);
                }
                change_param_label_string(buf, makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
                param_label_modified = 1;
                zmag_label_modified = 1;
                vcap_servo_on = 1;
            }
            else if (vcap_servo_on == 1)
            {
                vcap_servo_on = 0;
                change_param_label_string(NULL, 0, 0, 0);
                change_zmag_label_string(NULL, 0, 0, 0);
            }
            if (limit > 0)
            {
                gt->status_flag[ci] |= POSITIVE_LIMIT_ON;
                my_set_window_title("Zmag positive switch limit ON !");
                change_param_label_string("Zmag positive switch limit ON !", makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
                param_label_modified = 1;
                limits_on = 1;
                change_zmag_label_string("Zmag +limit", makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
                zmag_label_modified = 1;
                if ((gt->zmag_limit_on_start & POSITIVE_LIMIT_ON) == 0)
                {
                    gt->zmag_positive_limit_when_on = 1;
                }
            }
            else if (limit < 0)
            {
                gt->status_flag[ci] |= NEGATIVE_LIMIT_ON;
                my_set_window_title("Zmag negative switch limit ON !");
                change_param_label_string("Zmag negative switch limit ON !", makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
                param_label_modified = 1;
                limits_on = 1;
                change_zmag_label_string("Zmag -limit", makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
                zmag_label_modified = 1;
                if ((gt->zmag_limit_on_start & NEGATIVE_LIMIT_ON) == 0)
                {
                    gt->zmag_negative_limit_when_on = 1;
                }
            }
            else if (limits_on)
            {
                gt->status_flag[ci] &= ~(NEGATIVE_LIMIT_ON | POSITIVE_LIMIT_ON | VCAP_SERVO_ON);
                change_param_label_string(NULL, 0, 0, 0);
                change_zmag_label_string(NULL, 0, 0, 0);
                limits_on = 0;
            }
            else
            {
                gt->status_flag[ci] &= ~(NEGATIVE_LIMIT_ON | POSITIVE_LIMIT_ON | VCAP_SERVO_ON);
            }
            f_conv = 1;
        }
        else
        {
            prev_zmag = gt->zmag[ci] = gt->zmag[ci_1];
        }
    }
    else
    {
        prev_zmag = gt->zmag[ci] = gt->zmag[ci_1];
        gt->status_flag[ci] &= ~ZMAG_MOVING;
    }
#endif

    rot_moving = (fabs(gt->rot_mag_cmd[ci] - gt->rot_mag[ci_1]) > 0.005) ? rot_transient :
                 ((rot_moving > 0) ? rot_moving - 1 : 0);

    if (rs232used)  // we wait
    {
        prev_mag_rot = gt->rot_mag[ci] = gt->rot_mag[ci_1];
    }
    else if (rot_moving || ((ci & 0x0000002f) == 0x01f))
    {
        rs232used = 1;
        tmpl = read_rot_value();

        if (tmpl != __FLT_MAX__)
        {
            last_rot_read.value = tmpl;
            //last_rot_read.spare = ap->spare;
            last_rot_read.type = READ_ROT_VAL;
            last_rot_read.im_done = gt->imi[ci];
            last_rot_read.proceeded_RT = 1;
            RT_debug("im %d read rot val %g \n", gt->imi[ci], tmpl);
            prev_mag_rot = gt->rot_mag[ci] = tmpl;

            if (rot_moving)
            {
                gt->status_flag[ci] |= ROT_MOVING | ROT_MOVING_SAMPLED;
            }
            else
            {
                gt->status_flag[ci] |= ROT_MOVING_SAMPLED;
            }

            f_conv = 1;
        }
        else
        {
            prev_mag_rot = gt->rot_mag[ci] = gt->rot_mag[ci_1];
        }

        //my_set_window_title("rot read cmd %f val %f flag %x !",gt->rot_mag_cmd[ci],gt->rot_mag[ci],gt->status_flag[gt->c_i]);
    }
    else
    {
        prev_mag_rot = gt->rot_mag[ci] = gt->rot_mag[ci_1];
        gt->status_flag[gt->c_i] &= ~ROT_MOVING;
    }

    if (f_conv)
    {
        convert_zmag_to_force(prev_zmag, prev_mag_rot, 0);
    }

//to read the position of the objective
//tv 120317 : in PIE709 mode i try to record more frequently the obj position to examine servo noise
#ifdef PI_E709
  if (pi_e709used) prev_focus = gt->obj_pos[ci] = gt->obj_pos[ci_1];
#else
  if (rs232used) prev_focus = gt->obj_pos[ci] = gt->obj_pos[ci_1];
#endif
  else if (obj_moving > 0) //(fabs(gt->obj_pos_cmd[ci] - gt->obj_pos[ci_1]) > 0.005)
  {
#ifdef PI_E709
    pi_e709used = 1;
#else
    rs232used = 1;
#endif
    tmpl = read_Z_value_accurate_in_micron();
    gt->status_flag[ci] |= OBJ_MOVING_SAMPLED;
    last_focus_read.value = tmpl;
    last_focus_read.type = GET_FOCUS_POS;
    last_focus_read.im_done = gt->imi[ci];
    last_focus_read.proceeded_RT = 1;
    if (tmpl >= 1000000.0)
    {
      change_focus_label_string("PI overflow ", makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
      focus_label_modified = 1;
      prev_focus = gt->obj_pos[ci] = tmpl - 1000000;
      obj_moving = (fabs(gt->obj_pos[ci] - gt->obj_pos[ci_1]) > 0.015) ? 5 : obj_moving - 1;
      obj_moving = (obj_moving < 0) ? 0 : obj_moving;
      gt->status_flag[gt->c_i] |= OBJ_MOVING;
    }
    else if (tmpl >= 0 && tmpl < 1000000.0)
    {
      prev_focus = gt->obj_pos[ci] = tmpl;
      obj_moving = (fabs(gt->obj_pos[ci] - gt->obj_pos[ci_1]) > 0.015) ? 5 : obj_moving - 1;
      obj_moving = (obj_moving < 0) ? 0 : obj_moving;
      gt->status_flag[gt->c_i] |= OBJ_MOVING;
      if (focus_label_modified)
      {
        change_focus_label_string(NULL, 0, 0, 0);
        focus_label_modified = 0;
      }
    }
    else prev_focus = gt->obj_pos[ci] = gt->obj_pos[ci_1];
  }
#ifdef PI_E709 //last case : obj not supposed to move, we record its position periodically
  else if ( ((ci%pi_e709_com_period) == 0) && pi_e709used==0)
#else
  else if (((ci & 0x0000002f) == 0) && rs232used == 0) // || ((gt->status_flag[gt->c_i])&USER_MOVING_OBJ))
  // we ask the objective position every 48 images (to track manual focus changes)
#endif
  {
#ifdef PI_E709
    pi_e709used = 1;
#else
    rs232used = 1;
#endif
    tmpl = read_Z_value_accurate_in_micron();
    gt->status_flag[ci] |= OBJ_MOVING_SAMPLED;
    if (tmpl >= 1000000)
    {
      gt->obj_pos[ci] = tmpl - 1000000;
      change_focus_label_string("PI overflow", makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
      focus_label_modified = 1;
      if (fabs(prev_focus - gt->obj_pos[ci]) > 0.015)
      {
        gt->obj_pos_cmd[ci] = prev_focus = gt->obj_pos[ci];
        gt->status_flag[gt->c_i] |= USER_MOVING_OBJ;
      }
      else gt->status_flag[gt->c_i] &= ~(OBJ_MOVING | USER_MOVING_OBJ);
    }
    else if (tmpl >= 0 && tmpl < 1000000.0)
    {
      gt->obj_pos[ci] = tmpl;
      if (fabs(prev_focus - gt->obj_pos[ci]) > 0.015)
      {
        gt->obj_pos_cmd[ci] = prev_focus = gt->obj_pos[ci];
        gt->status_flag[gt->c_i] |= USER_MOVING_OBJ;
      }
      else gt->status_flag[gt->c_i] &= ~(OBJ_MOVING | USER_MOVING_OBJ);
      if (focus_label_modified)
      {
        change_focus_label_string(NULL, 0, 0, 0);
        focus_label_modified = 0;
      }
    }
    else prev_focus = gt->obj_pos[ci] = gt->obj_pos[ci_1];
  }
  else
  {
    prev_focus = gt->obj_pos[ci] = gt->obj_pos[ci_1];
    gt->status_flag[gt->c_i] &= ~(OBJ_MOVING | USER_MOVING_OBJ);
  }

    if (((ci % gt->im_temperature_check) == 0) && rs232used == 0 && gt->itemp_mes == 0 && get_temperature != NULL)
    {
        if (gt->temp_cycle == 0)
        {
            rs232used = 1;
            gt->T0 = get_temperature(0);
            //gt->T0 = 10;
            snprintf(gt->temp_mes, 32, "T0=%5.3f", gt->T0);
            gt->ntemp_mes = gt->itemp_mes = strlen(gt->temp_mes) + 1;
            //my_set_window_title("temp read %s",gt->temp_mes);
            //    change_zmag_label_string("petit essai d'�criture de param",makecol32(255, 40, 40),makecol32(240, 240, 240));
            //change_zmag_label_string(gt->temp_mes,makecol32(255, 40, 40),makecol32(240, 240, 240));
        }
        else if (gt->temp_cycle == 1)
        {
            rs232used = 1;
            gt->T1 = get_temperature(1);
            //gt->T1 = 10;
            snprintf(gt->temp_mes, 32, "T1=%5.3f", gt->T1);
            gt->ntemp_mes = gt->itemp_mes = strlen(gt->temp_mes) + 1;
            //my_set_window_title("temp read %s",gt->temp_mes);
        }
        else if (gt->temp_cycle == 2)
        {
            rs232used = 1;
            gt->T2 = get_temperature(2);
            //gt->T2 = 10;
            snprintf(gt->temp_mes, 32, "T2=%5.3f", gt->T2);
            gt->ntemp_mes = gt->itemp_mes = strlen(gt->temp_mes) + 1;
            //my_set_window_title("temp read %s",gt->temp_mes);
        }
        else if (gt->temp_cycle == 3)
        {
            rs232used = 1;
            gt->T3 = get_temperature(3);
            //gt->T3 = 10;
            snprintf(gt->temp_mes, 32, "T3=%5.3f", gt->T3);
            gt->ntemp_mes = gt->itemp_mes = strlen(gt->temp_mes) + 1;
            //my_set_window_title("temp read %s",gt->temp_mes);
        }
        else if (gt->temp_cycle == 4)
        {
            rs232used = 1;
            //gt->T4 = get_temperature(4);
            snprintf(gt->temp_mes, 32, "I0=%5.3f", gt->I0);
            gt->ntemp_mes = gt->itemp_mes = strlen(gt->temp_mes) + 1;
            //my_set_window_title("temp read %s",gt->temp_mes);
            // we update camera info
            if (get_camera_gain != NULL)
            {
                get_camera_gain(&track_info->camera_gain, &track_info->camera_min_gain, &track_info->camera_max_gain, NULL);
            }

            if (get_camera_shutter_in_us != NULL)
                get_camera_shutter_in_us(&track_info->camera_exposure_time_us, &track_info->camera_min_exposure,
                                         &track_info->camera_max_exposure);
        }
        else if (gt->temp_cycle == 5)
        {
            snprintf(gt->temp_mes, 32, "EXPO=%d", gt->camera_exposure_time_us);
            gt->ntemp_mes = gt->itemp_mes = strlen(gt->temp_mes) + 1;
        }
        else if (gt->temp_cycle == 6)
        {
            snprintf(gt->temp_mes, 32, "GAIN=%5.3f", gt->camera_gain);
            gt->ntemp_mes = gt->itemp_mes = strlen(gt->temp_mes) + 1;
        }
        else if (gt->temp_cycle == 7)
        {
            snprintf(gt->temp_mes, 32, "LED=%6.3f", gt->led_level);
            gt->ntemp_mes = gt->itemp_mes = strlen(gt->temp_mes) + 1;
        }
        else if ((track_info->evanescent_mode >= 0) && gt->temp_cycle == 8)
        {
            snprintf(gt->temp_mes, 32, "EVA-ON=%d", track_info->evanescent_mode);
            gt->ntemp_mes = gt->itemp_mes = strlen(gt->temp_mes) + 1;
        }

        gt->temp_cycle++;
        if (track_info->evanescent_mode >= 0)
        {
            if (gt->temp_cycle > 8)
            {
                gt->temp_cycle = 0;
            }
        }
        else if (gt->temp_cycle > 7)
        {
            gt->temp_cycle = 0;
        }
    }

    gt->temp_message[ci] = (gt->itemp_mes > 0) ? gt->temp_mes[gt->ntemp_mes - gt->itemp_mes--] : 0;
    gt->nb_bd_in_view[ci] = gt->last_nb_of_bd_found;
    gt->imdt[ci] = my_uclock() - dt;

    if ((gt->local_lock & BUF_FREEZED) == 0)
    {
        gt->local_lock |= BUF_CHANGING;  // we prevent data changes

        for (; gt->lac_i < gt->ac_i; gt->lac_i++)
        {
            // we update local buffer
            i = gt->lac_i % TRACKING_BUFFER_SIZE;
            gt->limi[i] = gt->imi[i];
            gt->limit[i] = gt->imit[i];
            gt->limt[i] = gt->imt[i];
            gt->limdt[i] = gt->imdt[i];
            gt->lc_i = i;
        }
        gt->local_lock = 0;
    }

    if (working_gen_record != NULL)
    {
        temp_gen_record = working_gen_record;
        abs_pos = working_gen_record->abs_pos - 1;
        abs_pos = (abs_pos < 0) ? 0 : abs_pos;
        page_n = abs_pos / working_gen_record->page_size;
        i_page = abs_pos % working_gen_record->page_size;
        status = working_gen_record->action_status[page_n][i_page];
        abs_pos = working_gen_record->abs_pos;
        abs_pos = (abs_pos < 0) ? 0 : abs_pos;   // abs_pos may be negative when we start
        page_n = abs_pos / working_gen_record->page_size;
        i_page = abs_pos % working_gen_record->page_size;
        working_gen_record->action_status[page_n][i_page] = status;

        // be careful record_action will set working_gen_record to NULL at the end
        if (working_gen_record->record_action != NULL)
        {
            working_gen_record->record_action(gt->imi[ci], ci, gt->ac_i, 0);
        }
        i = transfer_last_info_from_rolling_buffer_to_record(gt, ci, temp_gen_record);
        if (i)
        {
            snprintf(buf, 512, "Transfert pb %d instead of %d !", i, temp_gen_record->one_im_data_size);
            change_param_label_string(buf, makecol32(255, 40, 40), makecol32(240, 240, 240), 0);
            param_label_modified = 1;
        }
    }
    return 0;
}
#endif
END_OF_FUNCTION(track_in_x_y_timer)


int get_present_image_nb(void)
{
    return (track_info != NULL) ?  track_info->imi[track_info->c_i] : -1;
}


g_track *creating_track_info(void)
{
    O_p *op = NULL;
    d_s *ds = NULL;

    if (track_info == NULL)
    {
        track_info = (g_track *)calloc(1, sizeof(g_track));

        if (track_info == NULL)
        {
            return (g_track *) win_printf_ptr("cannot alloc track info!");
        }

        track_info->cl = -1;        // so that we know it was not initialized
        track_info->m_b = 16;
        track_info->n_b = track_info->c_b = 0;
        track_info->bead_servo_nb = -1;
        track_info->n_bead_display = -1;
        track_info->obj_servo_rate = 3;
        track_info->obj_servo_profile_index = 10;
        track_info->max_obj_mov = 5;
        track_info->integral_gain = 0.01;
        track_info->integral = 0.0;
        track_info->gain = 0.05;
        track_info->bead_fence = 5.0;

        if (oi_TRACK)
        {
            track_info->bead_fence_in_pixel = (int)(0.5 + (track_info->bead_fence / oi_TRACK->dy));
        }

        track_info->evanescent_mode = -1;
        //if
        track_info->SDI_mode = 0;
        #ifdef SDI_VERSION
	track_info->SDI_mode = 1; // to check for side effect
        track_info->sdi_param = NULL;
        #endif
        #ifdef SDI_VERSION_2
	track_info->SDI_mode = 1;
        #endif
        track_info->bd = (b_track **)calloc(track_info->m_b, sizeof(b_track *));

        if (track_info->bd == NULL)
        {
            return (g_track *) win_printf_ptr("cannot alloc track info!");
        }

        track_info->m_oi = 16;
        track_info->n_oi = track_info->c_oi = 0;
        track_info->generic_reference = 0;

        if (Pico_param.obj_param.immersion_type == 0)
        {
            track_info->focus_cor = 1.5;
        }
        else if (Pico_param.obj_param.immersion_type == 1)
        {
            track_info->focus_cor = 1;
        }
        else
        {
            if (Pico_param.obj_param.immersion_index > 0 && Pico_param.obj_param.buffer_index > 0)
            {
                track_info->focus_cor = Pico_param.obj_param.buffer_index / Pico_param.obj_param.immersion_index;
            }
            else
            {
                track_info->focus_cor = 0.878;
            }
        }

        track_info->com_skip_fr = (int)(Pico_param.camera_param.camera_frequency_in_Hz / 40);
        track_info->im_temperature_check = (int)Pico_param.camera_param.camera_frequency_in_Hz;
        track_info->im_temperature_check += (track_info->im_temperature_check < 1) ? 30 : 0;
        track_info->im_focus_check = 16;
        track_info->temp_cycle = track_info->itemp_mes = 0;
        track_info->T0 = track_info->T1 = track_info->T2 = track_info->T3 = track_info->I0 = 0;
        track_info->temp_mes[0] = 0;
        track_info->generic_calib_im = (O_i **)calloc(track_info->m_oi, sizeof(O_i *));
        track_info->generic_calib_im_fil = (O_i **)calloc(track_info->m_oi, sizeof(O_i *));

        track_info->track_free_x_bar = 0;
        track_info->track_free_y_bar = 0;
        track_info->track_free_xw = 256;
        track_info->track_free_window_x = 256;
        track_info->track_free_window_y = 128;


        track_info->track_free_yw = 64;

        track_info->do_move_free_bead = 0;
        track_info->dsfreebeads = NULL;
        track_info->SDI_2_track_free = 1;
        track_info->track_free_lum_threshold = 10;
        track_info->track_free_y_low = 0;
        track_info->track_free_y_high = 999;
        track_info->track_free_window_width = 256;
        track_info->track_free_shift_window = 128;

        if (track_info->generic_calib_im == NULL || track_info->generic_calib_im_fil == NULL)
        {
            return (g_track *) win_printf_ptr("cannot alloc o_i!");
        }

        //starting_one = active_dialog; // to check if windows change
        track_info->user_tracking_action = NULL;
        // plots to check tracking
        op = pr_TRACK->o_p[0];
        set_op_filename(op, "Timing.gr");
        op->width = rolling_plot_width * (float)SCREEN_W / SCREEN_H;
        ds = op->dat[0]; // the first data set was already created
        set_ds_source(ds, "Timing rolling buffer");

        if ((ds = create_and_attach_one_ds(op, TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0)) == NULL)
        {
            return (g_track *) win_printf_ptr("I can't create plot !");
        }

        set_ds_source(ds, "Period rolling buffer");

        if ((ds = create_and_attach_one_ds(op, TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0)) == NULL)
        {
            return (g_track *) win_printf_ptr("I can't create plot !");
        }

        set_ds_source(ds, "Timer rolling buffer");
        op->op_idle_action = timing_rolling_buffer_idle_action;
        op->op_post_display = general_op_post_display;
        op->user_ispare[ISPARE_BEAD_NB] = 0;
        set_plot_x_title(op, "Time");
        set_plot_y_title(op, "{\\color{yellow} Tracking duration} {\\color{lightgreen} Frame duration} (ms)");
        create_attach_select_x_un_to_op(op, IS_SECOND, 0
                                        , (float)1 / Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");

        // plots to check temperature
        if ((op = create_and_attach_one_plot(pr_TRACK, TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0)) == NULL)
        {
            return (g_track *) win_printf_ptr("I can't create plot !");
        }

        set_op_filename(op, "Temperatures.gr");
        ds = op->dat[0]; // the first data set was already created
        set_ds_source(ds, "Sensor temperature");
        op->width = rolling_plot_width * (float)SCREEN_W / SCREEN_H;

        if ((ds = create_and_attach_one_ds(op, TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0)) == NULL)
        {
            return (g_track *) win_printf_ptr("I can't create plot !");
        }
        set_ds_source(ds, "Sample temperature");

        if ((ds = create_and_attach_one_ds(op, TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0)) == NULL)
        {
            return (g_track *) win_printf_ptr("I can't create plot !");
        }
        set_ds_source(ds, "Heat sink temperature");
#ifdef RAWHID_V2
        if ((ds = create_and_attach_one_ds(op, TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0)) == NULL)
        {
            return (g_track *) win_printf_ptr("I can't create plot !");
        }
        set_ds_source(ds, "Magnets temperature");
#endif
        op->op_idle_action = temperature_rolling_buffer_idle_action;
        op->op_post_display = general_op_post_display;
        op->user_ispare[ISPARE_BEAD_NB] = 0;
        set_plot_x_title(op, "Time");
        set_plot_y_title(op, "{\\color{yellow} Sensor} {\\color{lightgreen} Sample} Heat sink {}^\\oc C");
        create_attach_select_x_un_to_op(op, IS_SECOND, 0
                                        , (float)1 / Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");
        track_info->m_i = track_info->n_i = TRACKING_BUFFER_SIZE;
        track_info->c_i = track_info->ac_i = track_info->lac_i = track_info->lc_i = 0;
        track_info->local_lock = 0;
        //      LOCK_FUNCTION(track_in_x_y_IFC);
        LOCK_FUNCTION(track_in_x_y_timer);
        LOCK_VARIABLE(track_info);
        bid.param = (void *)track_info;
        bid.to_do = NULL;
        bid.timer_do = track_in_x_y_timer;
        track_info->led_level = 100.0;
        track_info->eva_decay = 0.2;
        track_info->eva_offset = 10;
    }

    return track_info;
}


int init_track_info(void)
{
    if (track_info == NULL)
    {
        track_info = creating_track_info();
        m_action = 256;
        n_action = 0;
        action_pending = (f_action *)calloc(m_action, sizeof(f_action));

        if (action_pending == NULL)
        {
            win_printf_OK("Could not allocate action_pendind!");
        }
    }

    return 0;
}



int trajectories_op_idle_action(O_p *op, DIALOG *d)
{
    // we put in this routine all the screen display stuff
    pltreg *pr = NULL;

    if (d->dp == NULL)
    {
        return 1;
    }

    pr = (pltreg *)d->dp;       /* the image region is here */

    if (pr->one_p == NULL || pr->n_op == 0)
    {
        return 1;
    }

    if (op->need_to_refresh)
    {
        refresh_plot(pr_TRACK, UNCHANGED);
    }

    return 0;
}


int x_y_trajectories_job(int im, struct future_job *job)
{
    int imil, ci;
    b_track *bt1 = NULL, *bt2 = NULL;;
    d_s *ds1 = NULL, *ds2 = NULL;

    if (job == NULL || job->op == NULL || job->in_progress == 0)
    {
        return 0;
    }

    if (im < job->imi)
    {
        return 0;
    }

    ci = track_info->c_i;
    imil = track_info->imi[ci];

    //todo = imil - job->in_progress;
    if (track_info->n_b > 0)
    {
        bt1 = track_info->bd[0];
    }

    if (track_info->n_b > 1)
    {
        bt2 = track_info->bd[1];
    }

    if (job->op->n_dat > 0)
    {
        ds1 = job->op->dat[0];
    }

    if (job->op->n_dat > 1)
    {
        ds2 = job->op->dat[1];
    }

    while (track_info->imi[ci] > job->in_progress)
    {
        ci--;

        if (ci < 0)
        {
            ci += TRACKING_BUFFER_SIZE;
        }
    }

    while (track_info->imi[ci] < imil)
    {
        if (bt1 != NULL && ds1 != NULL)
        {
            add_new_point_to_ds(ds1, bt1->x[ci], bt1->y[ci]);
        }

        if (bt2 != NULL && ds2 != NULL)
        {
            add_new_point_to_ds(ds2, bt2->x[ci], bt2->y[ci]);
        }

        ci++;

        if (ci >= TRACKING_BUFFER_SIZE)
        {
            ci -= TRACKING_BUFFER_SIZE;
        }
    }

    job->in_progress = imil;
    job->imi = imil + 25;
    job->op->need_to_refresh = 1;
    return 0;
}

int angle_op_idle_action(O_p *op, DIALOG *d)
{
    // we put in this routine all the screen display stuff
    pltreg *pr = NULL;

    if (d->dp == NULL)
    {
        return 1;
    }

    pr = (pltreg *)d->dp;       /* the image region is here */

    if (pr->one_p == NULL || pr->n_op == 0)
    {
        return 1;
    }

    if (op->need_to_refresh)
    {
        refresh_plot(pr_TRACK, UNCHANGED);
    }

    return 0;
}




int do_z_profile_mean_and_sigma_of_movie(void)
{
    int i, j, k;
    static int xc = 0, yc = 0, x0 = 0, y0 = 0, x1 = 256, y1 = 256, n_bin = 16;
    O_i *ois = NULL;
    int nf, nl = 0, nt;
    imreg *imr = NULL;
    float *tmp;
    double mean = 0, y2b = 0, sig2 = 0, meanm = 0, sig2m, meanb, min = 0, max = 0;

    if (ac_grep(cur_ac_reg, "%im%oi", &imr, &ois) != 2)
    {
        active_menu->flags |=  D_DISABLED;
        return D_O_K;
    }

    if (updating_menu_state != 0)
    {
        if (ois->im.n_f == 1 || ois->im.n_f == 0)
        {
            active_menu->flags |=  D_DISABLED;
        }
        else
        {
            active_menu->flags &= ~D_DISABLED;
        }

        return D_O_K;
    }

    nf = abs(ois->im.n_f);

    if (nf <= 0)
    {
        win_printf("This is not a movie");
        return D_O_K;
    }

    i = win_scanf("define the area to analyze\n x0 %12d y0 %12d\n x1 %12d y1 %12d\nbin size %10d"
                  , &x0, &y0, &x1, &y1, &n_bin);

    if (i == WIN_CANCEL)
    {
        return D_O_K;
    }

    tmp = (float *)calloc(nf, sizeof(float));

    if (tmp == NULL)
    {
        return win_printf_OK("cant create array!");
    }

    for (yc = y0, nl = 0, meanm = sig2m = 0, y2b = 0; yc < y1; yc++)
    {
        for (xc = x0; xc < x1; xc++)
        {
            for (i = 0 ; i < nf ; i++)
            {
                tmp[i] = 0;
            }

            extract_z_profile_from_movie(ois, xc, yc, tmp);

            for (i = 0, mean = 0; i < nf ; i++)
            {
                mean += tmp[i];
            }

            for (i = 0, sig2 = 0, mean /= nf; i < nf ; i++)
            {
                sig2 += (mean - tmp[i]) * (mean - tmp[i]);
            }

            if ((nf - 1) > 0)
            {
                sig2 /= (nf - 1);
            }

            if (xc == x0 && yc == y0)
            {
                min = max = mean;
            }

            if (mean > max)
            {
                max = mean;
            }

            if (mean < min)
            {
                min = mean;
            }

            meanm += mean;
            sig2m += sig2;

            for (i = 0, k = 0; k < nf / n_bin; i += n_bin, k++)
            {
                for (j = 0, meanb = 0; (j < n_bin) && ((i + j) < nf); j++)
                {
                    meanb += tmp[i + j];
                }

                meanb /= (j > 0) ? j : 1;

                for (j = 0; (j < n_bin) && ((i + j) < nf); j++)
                {
                    y2b += (tmp[i + j] - meanb) * (tmp[i + j] - meanb);
                }

                nl += (j > 1) ? j - 1 : 1;
            }
        }
    }

    nt = (y1 - y0) * (x1 - x0);

    if (nt > 0)
    {
        meanm /= nt;
    }

    if (nt > 0)
    {
        sig2m /= nt;
    }

    if (nl > 0)
    {
        y2b /= nl;
    }

    free(tmp);
    win_printf_OK("For pixel having %d < xc < %d\n"
                  "and %d < yc < %d\n"
                  "min %g and max %g\n"
                  "mean = %g \\sigma^2 = %g \\sigma_{%d}^2 = %g"
                  , x0, x1, y0, y1, min, max, mean, sig2, n_bin, y2b);
    return 0;
}





int do_z_profile_mean_and_sigma_of_movie_auto(void)
{
    int i, j, k;
    static int xc = 0, yc = 0, x0 = 0, y0 = 0, x1 = 256, y1 = 256, n_bin = 16, n_led = 16;
    O_i *ois = NULL;
    O_p *op = NULL;
    d_s *ds = NULL, *dssig = NULL, *dssig2 = NULL;
    int nf, nl = 0, nt, i_led, im0, im;
    imreg *imr = NULL;
    float *tmp = NULL, led;
    static float led_max = 100.0;
    double mean = 0, y2b = 0, sig2 = 0, meanm = 0, sig2m, meanb, min = 0, max = 0;

    if (ac_grep(cur_ac_reg, "%im%oi", &imr, &ois) != 2)
    {
        active_menu->flags |=  D_DISABLED;
        return D_O_K;
    }

    if (updating_menu_state != 0)
    {
        if (ois->im.n_f == 1 || ois->im.n_f == 0)
        {
            active_menu->flags |=  D_DISABLED;
        }
        else
        {
            active_menu->flags &= ~D_DISABLED;
        }

        return D_O_K;
    }

    nf = abs(ois->im.n_f);

    if (nf <= 0)
    {
        win_printf("This is not a movie");
        return D_O_K;
    }

    led_max = track_info->led_level;
    i = win_scanf("define the area to analyze\n"
                  " x0 %12d y0 %12d\n x1 %12d y1 %12d\n"
                  "bin size %10d\n"
                  "Led max %12f nb. of step decreasing led %10d\n"
                  , &x0, &y0, &x1, &y1, &n_bin, &led_max, &n_led);

    if (i == WIN_CANCEL)
    {
        return D_O_K;
    }

    tmp = (float *)calloc(nf, sizeof(float));

    if (tmp == NULL)
    {
        return win_printf_OK("cant create array!");
    }

    if ((op = create_and_attach_one_plot(pr_TRACK, n_led, n_led, 0)) == NULL)
    {
        return win_printf_OK("I can't create plot !");
    }

    set_op_filename(op, "camera-noise.gr");
    ds = op->dat[0]; // the first data set was already created
    set_ds_source(ds, "mean vs led");
    op->width = rolling_plot_width * (float)SCREEN_W / SCREEN_H;

    if ((dssig = create_and_attach_one_ds(op, n_led, n_led, 0)) == NULL)
    {
        return win_printf_OK("I can't create plot !");
    }

    set_ds_source(dssig, "Sig2 vs led");

    if ((dssig2 = create_and_attach_one_ds(op, n_led, n_led, 0)) == NULL)
    {
        return win_printf_OK("I can't create plot !");
    }

    set_ds_source(dssig2, "Sig2 bin vs led");
    //op->op_idle_action = temperature_rolling_buffer_idle_action;
    //op->op_post_display = general_op_post_display;
    set_plot_x_title(op, "Led");

    for (i_led = 0; i_led < n_led; i_led++)
    {
        im0 = get_present_image_nb();
        led = (led_max * (n_led - 1 - i_led)) / (n_led - 1);
        fill_next_available_action(im0 + 1, CHG_LED_VAL, led);

        for (; (im = get_present_image_nb()) < (im0 + nf + 32);)
            my_set_window_title("im %d led %g mean = %g \\sigma^2 = %g \\sigma_{%d}^2 = %g"
                                , im - im0, led, meanm, sig2, n_bin, y2b);;

        for (yc = y0, nl = 0, meanm = sig2m = 0, y2b = 0; yc < y1; yc++)
        {
            for (xc = x0; xc < x1; xc++)
            {
                for (i = 0 ; i < nf ; i++)
                {
                    tmp[i] = 0;
                }

                extract_z_profile_from_movie(ois, xc, yc, tmp);

                for (i = 0, mean = 0; i < nf ; i++)
                {
                    mean += tmp[i];
                }

                for (i = 0, sig2 = 0, mean /= nf; i < nf ; i++)
                {
                    sig2 += (mean - tmp[i]) * (mean - tmp[i]);
                }

                if ((nf - 1) > 0)
                {
                    sig2 /= (nf - 1);
                }

                if (xc == x0 && yc == y0)
                {
                    min = max = mean;
                }

                if (mean > max)
                {
                    max = mean;
                }

                if (mean < min)
                {
                    min = mean;
                }

                meanm += mean;
                sig2m += sig2;

                for (i = 0, k = 0; k < nf / n_bin; i += n_bin, k++)
                {
                    for (j = 0, meanb = 0; (j < n_bin) && ((i + j) < nf); j++)
                    {
                        meanb += tmp[i + j];
                    }

                    meanb /= (j > 0) ? j : 1;

                    for (j = 0; (j < n_bin) && ((i + j) < nf); j++)
                    {
                        y2b += (tmp[i + j] - meanb) * (tmp[i + j] - meanb);
                    }

                    nl += (j > 1) ? j - 1 : 1;
                }
            }
        }

        nt = (y1 - y0) * (x1 - x0);

        if (nt > 0)
        {
            meanm /= nt;
        }

        if (nt > 0)
        {
            sig2m /= nt;
        }

        if (nl > 0)
        {
            y2b /= nl;
        }

        ds->yd[i_led] = meanm;
        ds->xd[i_led] = dssig->xd[i_led] = dssig2->xd[i_led] = led;
        dssig->yd[i_led] = sig2m;
        dssig2->yd[i_led] = y2b;
        my_set_window_title("mean = %g \\sigma^2 = %g \\sigma_{%d}^2 = %g"
                            , meanm, sig2, n_bin, y2b);
    }

    im0 = get_present_image_nb();
    fill_next_available_action(im0 + 1, CHG_LED_VAL, led_max);
    free(tmp);
    win_printf_OK("For pixel having %d < xc < %d\n"
                  "and %d < yc < %d\n"
                  "min %g and max %g\n"
                  "mean = %g \\sigma^2 = %g \\sigma_{%d}^2 = %g"
                  , x0, x1, y0, y1, min, max, meanm, sig2, n_bin, y2b);
    return 0;
}




int do_z_profile_mean_and_sigma_of_area_in_movie(void)
{
    int i, j, k;
    static int x0 = 0, y0 = 0, x1 = 256, y1 = 256, n_bin = 16, cl = 16;
    O_i *ois = NULL;
    int nf, nl = 0, nt, xc = 0, yc = 0;
    imreg *imr = NULL;
    double *tmp = NULL;
    double mean = 0, y2b = 0, sig2 = 0, meanm = 0, sig2m, meanb, min = 0, max = 0;

    if (ac_grep(cur_ac_reg, "%im%oi", &imr, &ois) != 2)
    {
        active_menu->flags |=  D_DISABLED;
        return D_O_K;
    }

    if (updating_menu_state != 0)
    {
        if (ois->im.n_f == 1 || ois->im.n_f == 0)
        {
            active_menu->flags |=  D_DISABLED;
        }
        else
        {
            active_menu->flags &= ~D_DISABLED;
        }

        return D_O_K;
    }

    nf = abs(ois->im.n_f);

    if (nf <= 0)
    {
        win_printf("This is not a movie");
        return D_O_K;
    }

    i = win_scanf("define the area to analyze\n x0 %12d y0 %12d\n"
                  " x1 %12d y1 %12d\n"
                  "Size ov spatial averaging %10d\n"
                  "bin size %10d in time"
                  , &x0, &y0, &x1, &y1, &cl, &n_bin);

    if (i == WIN_CANCEL)
    {
        return D_O_K;
    }

    tmp = (double *)calloc(nf, sizeof(double));

    if (tmp == NULL)
    {
        return win_printf_OK("cant create array!");
    }

    for (yc = y0, nl = 0, meanm = sig2m = 0, y2b = 0; yc < y1; yc += cl)
    {
        for (xc = x0; xc < x1; xc += cl)
        {
            for (i = 0 ; i < nf ; i++)
            {
                tmp[i] = avg_im_on_square_area(ois, i, xc + cl / 2, yc + cl / 2, cl);
            }

            for (i = 0, mean = 0; i < nf ; i++)
            {
                mean += tmp[i];
            }

            for (i = 0, sig2 = 0, mean /= nf; i < nf ; i++)
            {
                sig2 += (mean - tmp[i]) * (mean - tmp[i]);
            }

            if ((nf - 1) > 0)
            {
                sig2 /= (nf - 1);
            }

            if (xc == x0 && yc == y0)
            {
                min = max = mean;
            }

            if (mean > max)
            {
                max = mean;
            }

            if (mean < min)
            {
                min = mean;
            }

            meanm += mean;
            sig2m += sig2;

            for (i = 0, k = 0; k < nf / n_bin; i += n_bin, k++)
            {
                for (j = 0, meanb = 0; (j < n_bin) && ((i + j) < nf); j++)
                {
                    meanb += tmp[i + j];
                }

                meanb /= (j > 0) ? j : 1;

                for (j = 0; (j < n_bin) && ((i + j) < nf); j++)
                {
                    y2b += (tmp[i + j] - meanb) * (tmp[i + j] - meanb);
                }

                nl += (j > 1) ? j - 1 : 1;
            }
        }
    }

    nt = ((y1 - y0) / cl) * ((x1 - x0) / cl);

    if (nt > 0)
    {
        meanm /= nt;
    }

    if (nt > 0)
    {
        sig2m /= nt;
    }

    if (nl > 0)
    {
        y2b /= nl;
    }

    free(tmp);
    win_printf_OK("For square area %d in size\n having %d < xc < %d\n"
                  "and %d < yc < %d\n"
                  "min %g and max %g\n"
                  "mean = %g \\sigma^2 = %g \\sigma_{%d}^2 = %g"
                  "\\sigma^2_p = %g \\sigma_{p-%d}^2 = %g"
                  , cl, x0, x1, y0, y1, min, max, meanm, sig2, n_bin, y2b, sig2 * cl * cl, n_bin, y2b * cl * cl);
    return 0;
}



int do_z_profile_mean_and_sigma_on_area_of_movie_auto(void)
{
    int i, j, k;
    static int xc = 0, yc = 0, x0 = 0, y0 = 0, x1 = 256, y1 = 256, n_bin = 16, n_led = 16, cl = 8;
    O_i *ois = NULL;
    O_p *op = NULL;
    d_s *ds = NULL, *dssig = NULL, *dssig2 = NULL;
    int nf, nl = 0, nt, i_led, im0, im;
    imreg *imr = NULL;
    float *tmp = NULL, led;
    static float led_max = 100.0;
    double mean = 0, y2b = 0, sig2 = 0, meanm = 0, sig2m, meanb, min = 0, max = 0;

    if (ac_grep(cur_ac_reg, "%im%oi", &imr, &ois) != 2)
    {
        active_menu->flags |=  D_DISABLED;
        return D_O_K;
    }

    if (updating_menu_state != 0)
    {
        if (ois->im.n_f == 1 || ois->im.n_f == 0)
        {
            active_menu->flags |=  D_DISABLED;
        }
        else
        {
            active_menu->flags &= ~D_DISABLED;
        }

        return D_O_K;
    }

    nf = abs(ois->im.n_f);

    if (nf <= 0)
    {
        win_printf("This is not a movie");
        return D_O_K;
    }

    led_max = track_info->led_level;
    i = win_scanf("define the area to analyze\n"
                  " x0 %12d y0 %12d\n x1 %12d y1 %12d\n"
                  "Size ov spatial averaging %10d\n"
                  "bin size %10d\n"
                  "Led max %12f nb. of step decreasing led %10d\n"
                  , &x0, &y0, &x1, &y1, &cl, &n_bin, &led_max, &n_led);

    if (i == WIN_CANCEL)
    {
        return D_O_K;
    }

    tmp = (float *)calloc(nf, sizeof(float));

    if (tmp == NULL)
    {
        return win_printf_OK("cant create array!");
    }

    if ((op = create_and_attach_one_plot(pr_TRACK, n_led, n_led, 0)) == NULL)
    {
        return win_printf_OK("I can't create plot !");
    }

    set_op_filename(op, "camera-noise.gr");
    ds = op->dat[0]; // the first data set was already created
    set_ds_source(ds, "mean vs led");
    op->width = rolling_plot_width * (float)SCREEN_W / SCREEN_H;

    if ((dssig = create_and_attach_one_ds(op, n_led, n_led, 0)) == NULL)
    {
        return win_printf_OK("I can't create plot !");
    }

    set_ds_source(dssig, "Sig2 vs led");

    if ((dssig2 = create_and_attach_one_ds(op, n_led, n_led, 0)) == NULL)
    {
        return win_printf_OK("I can't create plot !");
    }

    set_ds_source(dssig2, "Sig2 bin vs led");
    //op->op_idle_action = temperature_rolling_buffer_idle_action;
    //op->op_post_display = general_op_post_display;
    set_plot_title(op, "Noise averaged over %dx%d", cl, cl);
    set_plot_x_title(op, "Led");
    set_plot_y_title(op, "mean, variance, variance(%d)", n_bin);

    for (i_led = 0; i_led < n_led; i_led++)
    {
        im0 = get_present_image_nb();
        led = (led_max * (n_led - 1 - i_led)) / (n_led - 1);
        fill_next_available_action(im0 + 1, CHG_LED_VAL, led);

        for (; (im = get_present_image_nb()) < (im0 + nf + 32);)
            my_set_window_title("im %d led %g mean = %g \\sigma^2 = %g \\sigma_{%d}^2 = %g"
                                , im - im0, led, meanm, sig2, n_bin, y2b);;

        for (yc = y0, nl = 0, meanm = sig2m = 0, y2b = 0; yc < y1; yc += cl)
        {
            for (xc = x0; xc < x1; xc += cl)
            {
                for (i = 0 ; i < nf ; i++)
                {
                    tmp[i] = avg_im_on_square_area(ois, i, xc + cl / 2, yc + cl / 2, cl);
                }

                for (i = 0, mean = 0; i < nf ; i++)
                {
                    mean += tmp[i];
                }

                for (i = 0, sig2 = 0, mean /= nf; i < nf ; i++)
                {
                    sig2 += (mean - tmp[i]) * (mean - tmp[i]);
                }

                if ((nf - 1) > 0)
                {
                    sig2 /= (nf - 1);
                }

                if (xc == x0 && yc == y0)
                {
                    min = max = mean;
                }

                if (mean > max)
                {
                    max = mean;
                }

                if (mean < min)
                {
                    min = mean;
                }

                meanm += mean;
                sig2m += sig2;

                for (i = 0, k = 0; k < nf / n_bin; i += n_bin, k++)
                {
                    for (j = 0, meanb = 0; (j < n_bin) && ((i + j) < nf); j++)
                    {
                        meanb += tmp[i + j];
                    }

                    meanb /= (j > 0) ? j : 1;

                    for (j = 0; (j < n_bin) && ((i + j) < nf); j++)
                    {
                        y2b += (tmp[i + j] - meanb) * (tmp[i + j] - meanb);
                    }

                    nl += (j > 1) ? j - 1 : 1;
                }
            }
        }

        nt = ((y1 - y0) / cl) * ((x1 - x0) / cl);

        if (nt > 0)
        {
            meanm /= nt;
        }

        if (nt > 0)
        {
            sig2m /= nt;
        }

        if (nl > 0)
        {
            y2b /= nl;
        }

        ds->yd[i_led] = meanm;
        ds->xd[i_led] = dssig->xd[i_led] = dssig2->xd[i_led] = led;
        dssig->yd[i_led] = sig2m * cl * cl;
        dssig2->yd[i_led] = y2b * cl * cl;
        my_set_window_title("mean = %g \\sigma^2 = %g \\sigma_{%d}^2 = %g"
                            , meanm, sig2, n_bin, y2b);
    }

    im0 = get_present_image_nb();
    fill_next_available_action(im0 + 1, CHG_LED_VAL, led_max);
    free(tmp);
    win_printf_OK("For pixel having %d < xc < %d\n"
                  "and %d < yc < %d\n"
                  "min %g and max %g\n"
                  "mean = %g \\sigma^2 = %g \\sigma_{%d}^2 = %g"
                  , x0, x1, y0, y1, min, max, meanm, sig2, n_bin, y2b);
    return 0;
}




//not working yet
int x_y_angle_job(int im, struct future_job *job)
{
    int imil, ci;
    float t, theta;
    static int angle_loops = 0;
    static float oldtheta;
    b_track *bt1 = NULL, *bt2 = NULL;
    d_s *ds = NULL;

    if (job == NULL || job->op == NULL || job->in_progress == 0)
    {
        return 0;
    }

    if (im < job->imi)
    {
        return 0;
    }

    ci = track_info->c_i;
    imil = track_info->imi[ci];

    //todo = imil - job->in_progress;
    if (track_info->n_b > 0)
    {
        bt1 = track_info->bd[0];
    }

    if (track_info->n_b > 1)
    {
        bt2 = track_info->bd[1];
    }

    if (job->op->n_dat > 0)
    {
        ds = job->op->dat[0];
    }

    while (track_info->imi[ci] > job->in_progress)
    {
        ci--;

        if (ci < 0)
        {
            ci += TRACKING_BUFFER_SIZE;
        }
    }

    while (track_info->imi[ci] < imil)
    {
        if (bt1 != NULL && bt2 != NULL && ds != NULL)
        {
            theta = (float) atan2(bt1->y[ci] - bt2->y[ci], bt1->x[ci] - bt2->x[ci]);

            if ((theta - oldtheta) >M_PI)
            {
                angle_loops ++;
            }
            else if ((theta - oldtheta) < -M_PI)
            {
                angle_loops--;
            }

            t = theta + angle_loops * 2 * M_PI;
            add_new_point_to_ds(ds, track_info->imi[ci], t);
            oldtheta = theta;
            ci++;

            if (ci >= TRACKING_BUFFER_SIZE)
            {
                ci -= TRACKING_BUFFER_SIZE;
            }
        }
    }

    job->in_progress = imil;
    job->imi = imil + 25;
    job->op->need_to_refresh = 1;
    return 0;
}


int stop_record_x_y_trajectories(void)
{
    pltreg *pr = NULL;
    O_p *op = NULL;
    f_job *job = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2)
    {
        return D_O_K;
    }

    job = find_job_associated_to_plot(op);

    if (job)
    {
        job->in_progress = 0;
    }

    return D_O_K;
}

int record_x_y_trajectories(void)
{
    int im;
    imreg *imr = NULL;
    O_i *oi = NULL, *oic = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (ac_grep(cur_ac_reg, "%im%oi", &imr, &oi) != 2)
    {
        return OFF;
    }

    op = create_and_attach_one_plot(pr_TRACK,  16, 16, 0);
    ds = op->dat[0];
    ds->nx = ds->ny = 0;
    set_ds_source(ds, "XY recording of bead 1");

    if ((ds = create_and_attach_one_ds(op, 16, 16, 0)) == NULL)
    {
        return win_printf_OK("I can't create plot !");
    }

    set_ds_source(ds, "XY recording of bead 2");
    ds->nx = ds->ny = 0;
    im = track_info->imi[track_info->c_i];
    fill_next_available_job_spot(im, im + 25, COLLECT_DATA, 0, imr, oic, pr_TRACK, op, NULL, x_y_trajectories_job, 0);
    op->op_idle_action = trajectories_op_idle_action;
    op->op_post_display = general_op_post_display;
    op->user_ispare[ISPARE_BEAD_NB] = 0;
    return D_O_K;
}

int stop_record_x_y_angle(void)
{
    pltreg *pr = NULL;
    O_p *op = NULL;
    f_job *job = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2)
    {
        return D_O_K;
    }

    job = find_job_associated_to_plot(op);

    if (job)
    {
        job->in_progress = 0;
    }

    return D_O_K;
}

int record_x_y_angle(void)
{
    int im;
    imreg *imr = NULL;
    O_i *oi = NULL, *oic = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (ac_grep(cur_ac_reg, "%im%oi", &imr, &oi) != 2)
    {
        return OFF;
    }

    op = create_and_attach_one_plot(pr_TRACK,  16, 16, 0);
    ds = op->dat[0];
    ds->nx = ds->ny = 0;
    set_ds_source(ds, "XY recording of angle");
    im = track_info->imi[track_info->c_i];
    fill_next_available_job_spot(im, im + 25, COLLECT_DATA, 0, imr, oic, pr_TRACK, op, NULL, x_y_angle_job, 0);
    op->op_idle_action = angle_op_idle_action;
    op->op_post_display = general_op_post_display;
    op->user_ispare[ISPARE_BEAD_NB] = 0;
    return D_O_K;
}



int switch_bead_of_interest(void)
{
    int tmp = 0;

    if (active_menu->text == NULL)
    {
        return D_O_K;
    }

    sscanf((char *)active_menu->text, "Bead %d", &tmp); // != 1) return D_O_K;

    if (updating_menu_state != 0)
    {
        if (tmp == track_info->n_bead_display)
        {
            active_menu->flags |=  D_SELECTED;
        }
        else
        {
            active_menu->flags &= ~D_SELECTED;
        }

        return D_O_K;
    }

    if (tmp >= track_info->n_b || tmp < 0)
    {
        return D_O_K;
    }

    track_info->n_bead_display = tmp;
    return D_O_K;
}

int switch_bead_repositionning(void)
{
    int i, tmp = 0, eva = 0, re, rm, skip;
    float tmpf, xc, yc, dec, off;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    tmp = track_info->stop_xy_tracking;
    skip = track_info->com_skip_fr;
    eva = track_info->evanescent_mode + 1;
    yc = track_info->bd[track_info->n_bead_display]->y0;
    xc = track_info->bd[track_info->n_bead_display]->x0;
    i = win_scanf("Scanning in XY %R in X %r in Y %r freezed %r\n"
                  "Nb of frames between RS232 access %8d\n"
                  "Normal Z tracking %R or \n"
                  "Record evanescent intensity insteed of Z\n"
                  "Uses a radial approximation and substract black level\nrecord Z = Z_0 - L_e. ln(I) %r \n"
                  "On a square (cw x cw) %r \n"
                  "with radial appodisation %r\n"
                  "with radial appodisation - black level %r\n"
                  "If freezd position specify \nCurrent bead position\n xc %12f yc %12f\n"
                  , &tmp, &skip, &eva, &xc, &yc);

    if (i == WIN_CANCEL)
    {
        return D_O_K;
    }

    track_info->stop_xy_tracking = tmp;
    track_info->com_skip_fr = skip;

    if (tmp == 3)
    {
        track_info->bd[track_info->n_bead_display]->y0 = yc;
        track_info->bd[track_info->n_bead_display]->x0 = xc;
    }
    else if (tmp == 1)
    {
        track_info->bd[track_info->n_bead_display]->y0 = yc;
    }
    else if (tmp == 2)
    {
        track_info->bd[track_info->n_bead_display]->x0 = xc;
    }

    if (eva > 0)
    {
        re = eva_spot_radius;
        rm = eva_black_radius;
        dec = track_info->eva_decay;
        off = track_info->eva_offset;
        i = win_scanf("Define the evanescent spot size\n"
                      "minimal radius re below wich weight = 1 %8d\n"
                      "maximum radius rm above wich weight = 0 %8d\n"
                      "and signal is used as black level\n"
                      "Evanescent decay length %12f\n"
                      "Evanescent offset length %12f\n"
                      , &re, &rm, &dec, &off);

        if (i == WIN_CANCEL)
        {
            return D_O_K;
        }

        eva_spot_radius = re;
        eva_black_radius = rm;
        track_info->eva_decay = dec;
        track_info->eva_decay_cor = dec / track_info->focus_cor;
        track_info->eva_offset = off;

        for (i = 0; i < 256; i++)
        {
            if (i < re)
            {
                evanescent_spot_weighting[i] = 1;
            }
            else if (i < rm)
            {
                tmpf = (float)(i - re);

                if (rm > re)
                {
                    tmpf /= (float)(rm - re);
                }

                evanescent_spot_weighting[i] = (1 + cos(M_PI * tmpf)) / 2;
            }
            else
            {
                evanescent_spot_weighting[i] = 0;
            }

            //if (i < 64) win_printf("i %d weight %g",i,evanescent_spot_weighting[i]);
        }
    }

    track_info->evanescent_mode = eva - 1;
    return D_O_K;
}


int switch_bead_servo(void)
{
    int i, tmp = 0, prof_ref = 0, ind_prof = 0;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (track_info == NULL)
    {
        return D_O_K;
    }

    tmp = track_info->bead_servo_nb;
    ind_prof = track_info->obj_servo_profile_index;//bt->calib_im_fil->im.ny/3;
    i = win_scanf("Use bead servo on bead %8d (-1 disable)\n"
                  "Proportionnal gain %8f\n"
                  "Integral gain %8f\n"
                  //"Derivative gain %8f\n"
                  "Number of image between changes %8d (>=2)\n"
                  "Maximum objective displacement %8f\n"
                  "Display objective error %b\n"
                  "Refresh reference profile %b\n"
                  "Index of profile used to servo %8d\n"
                  , &tmp, &track_info->gain, &track_info->integral_gain
                  //,&track_info->derivative_gain
                  , &track_info->obj_servo_rate
                  , &track_info->max_obj_mov, &dis_obj_servo, &prof_ref, &ind_prof);

    if (i == WIN_CANCEL)
    {
        return D_O_K;
    }

    for (i = 0; prof_ref != 0 && i < track_info->n_b; i++)
    {
        track_info->bd[i]->im_prof_ref = -1;
    }

    track_info->bead_servo_nb = (tmp < 0) ? tmp : tmp | 0x100;
    track_info->obj_servo_profile_index = ind_prof;
    return D_O_K;
}

int prepare_bead_menu(void)
{
  char st[128] = {0};
    int i;

    for (i = 0; i < track_info->n_b && i < 31; i++)  // no more than 31 bead in menu
    {
        if (bead_active_menu[i].text)
        {
            free(bead_active_menu[i].text);
            bead_active_menu[i].text = NULL;
        }

        snprintf(st, 128, "Bead %d", i);
        bead_active_menu[i].text = strdup(st);
        bead_active_menu[i].proc = switch_bead_of_interest;
        bead_active_menu[i].child = NULL;
    }

    if (bead_active_menu[i].text)
    {
        free(bead_active_menu[i].text);
    }

    bead_active_menu[i].text = NULL;
    return 0;
}

#ifdef SDI_VERSION
int do_add_bead_in_x_y(int cl, int cw, float x0, float y0) // position of the bead in image pixels
{
    int i;
    int max;
    int xc = 256, yc = 256, dis_state;
    b_track *bd = NULL;
    dis_state = do_refresh_overlay;
    do_refresh_overlay = 0;
    do_refresh_overlay = dis_state;
    xc = (int)(0.5 + x0);
    yc = (int)(0.5 + y0);
    bd = (b_track *)calloc(1, sizeof(b_track));
    if (bd == NULL) return win_printf_OK("cannot alloc bead info of %f Kio", sizeof(b_track) / (float) 1024);
    if (track_info->n_b >= track_info->m_b)
    {
        track_info->m_b += 16;
        track_info->bd = (b_track **)realloc(track_info->bd, track_info->m_b * sizeof(b_track *));
        if (track_info->bd == NULL) return win_printf_OK("cannot alloc track info! of %u Kio", track_info->m_b * sizeof(b_track *) / 1024);
    }
    bd->ncl = bd->cl = bd->orthoradial_prof_size = cl;
    bd->ncw = bd->cw = cw;
    bd->black_circle = 0;
    bd->xy_trp = bd->xy_fil = NULL;
    bd->calib_im_size = 0;
    bd->type = 0;

    for (i = 0; i < bd->cl; i++) bd->rad_prof_ref[i] = -1;    // cannot be a profile

    bd->apx = 1;
    bd->apy = 1;
    bd->calib_im_fil = bd->calib_im = bd->generic_calib_im_fil = bd->generic_calib_im = NULL;
    bd->max_limit = bd->dis_filter = bd->bp_center = bd->bp_width = 0; //16;
    bd->calibration_im_yi = bd->calibration_im_xi = bd->calibration_im_y = bd->calibration_im_x = -1;
    bd->fence_x = -1;
    bd->fence_y = -1;
    bd->rc = 12;
    bd->npc = bd->calib_im_size;
    bd->z_trp = NULL;
    bd->z_fil = NULL;

    bd->orthoradial_mean_radius = 0;
    bd->orthoradial_half_avg_size = 0;

    bd->not_lost = -1;
    bd->calib_im = NULL;
    bd->calib_im_fil = NULL;
    track_info->n_bead_display = track_info->n_b;
    track_info->bd[track_info->n_b++] = bd;
    prepare_bead_menu();
    // win_printf("bead added %d",track_info->n_b);
    LOCK_VARIABLE(bd);
    //bd->cw = track_info->cw;
    bd->saved_x = bd->last_x_not_lost = (float)xc;
    bd->saved_y = bd->last_y_not_lost = (float)yc;
    bd->xc = bd->x0 = xc;
    bd->yc = bd->y0 = yc;
    bd->im_prof_ref = -1;
    bd->mouse_draged = 0;
    bd->not_lost = NB_FRAMES_BEF_LOST;
    bd->start_im = 0;
    bid.param = (void *)track_info;
    bid.timer_do = track_in_x_y_timer;
    ask_to_update_menu();
    return 0;
}
#else
int do_add_bead_in_x_y(int cl, int cw,  // cross size
		       int calib_im_size, int fringes_dist,
		       int fringes_dy_tolerencel, int fringes_nyl,
                       float x0, float y0, int filter_low, int filter_high, int im_buffer_size, int fixed_mode, int discretization, int disp_range,
              int do_minimize,float sigma_g,float om_cos,int min_max_iter,float min_epsrel,float min_epsabs) // position of the bead in image pixels
{
    int i;
    int max_2;
    imreg *imrs = NULL;
    O_i *ois = NULL;
    int xc = 256, yc = 256, dis_state;
    b_track *bd = NULL;

    //DIALOG *d = NULL;

    (void)fringes_dist;
    (void)fringes_dy_tolerencel;
    (void) fringes_nyl;
    //tv: why do we need to find ois and imr here?
    #ifdef SDI_VERSION_2
    if (Pico_param.SDI_2_param.SDI_2_calibration < 0)
    {
      win_printf_OK("No calibration parameter for SDI 2\n");
      return 1;
    }
    remove_short_cut_from_dialog(0, KEY_M);
    add_keyboard_short_cut(0, KEY_M, 0, SDI_2_activate_tracking);
    #endif

    if (ac_grep(NULL, "%im%oi", &imrs, &ois) != 2) return win_printf_OK("cannot find image in acreg!");

    dis_state = do_refresh_overlay;
    do_refresh_overlay = 0;
    /* check for menu in win_scanf */
#ifndef SDI_VERSION_2
#ifndef SDIPICO
    if (cl > 128) return win_printf_OK("cannot use arm bigger\nthan 128");
    if (cw > 32)  return win_printf_OK("cannot use width bigger\nthan 32");
#endif
#endif
    if (fft_init(cl) || filter_init(cl)) return win_printf_OK("cannot init fft!");

    do_refresh_overlay = dis_state;
    xc = (int)(0.5 + x0);
    yc = (int)(0.5 + y0);
    bd = (b_track *)calloc(1, sizeof(b_track));
    if (bd == NULL) return win_printf_OK("cannot alloc bead info of %f Kio", sizeof(b_track) / (float) 1024);

    if (track_info->n_b >= track_info->m_b)
    {
        track_info->m_b += 16;
        track_info->bd = (b_track **)realloc(track_info->bd, track_info->m_b * sizeof(b_track *));
        if (track_info->bd == NULL) return win_printf_OK("cannot alloc track info! of %u Kio", track_info->m_b * sizeof(b_track *) / 1024);
    }

    bd->ncl = bd->cl = cl;
    bd->ncw = bd->cw = cw;
    bd->black_circle = 0;
    bd->xy_trp = fftbt_init(NULL, cl);
    bd->xy_fil = filter_init_bt(NULL, cl);

    bd->calib_im_size = calib_im_size;
    bd->type = 0;

#ifdef SDI_VERSION_2
    int nb_of_modes = filter_high - filter_low + 1 ;
    if (im_buffer_size >= 0)
    {
      bd->im_buffer_size = im_buffer_size;
      bd->mode_avg_fix=0;
    }
    else {
      bd->im_buffer_size = -im_buffer_size;
      bd->mode_avg_fix=1;
    }
    im_buffer_size=abs(im_buffer_size);

    bd->do_minimize = do_minimize;
    bd->min_epsrel = min_epsrel;
    bd->min_epsabs = min_epsabs;
    bd->min_max_iter = min_max_iter;
    bd->om_cos = om_cos;
    bd->sigma_g = sigma_g;

    bd->filter_high = filter_high;
    bd->filter_low = filter_low;
    bd->fixed_mode = fixed_mode;
    bd->filter_length = nb_of_modes;
    bd->frange_top=  (sdi_fr *)calloc(1, sizeof(sdi_fr));
    bd->frange_bottom=  (sdi_fr *)calloc(1, sizeof(sdi_fr));
    bd->dy0_fringes = fringes_dist;
    bd->fringes_dy_tolerence = fringes_dy_tolerencel;
    bd->moving_forbidden = 0;
    bd->fringes_ny = fringes_nyl;
    bd->frange_top->fx = x0;
    bd->frange_bottom->fx= x0;
    bd->frange_top->push=0;
    bd->frange_bottom->push=0;

    bd->frange_top->phi0=0;
    bd->frange_bottom->phi0=0;
    bd->frange_top->r0=0;
    bd->frange_bottom->r0=0;
    bd->frange_top->mouse_draged = 0;
    bd->frange_bottom->mouse_draged = 0;
  //  bd->frange_top->n_k=0; // number of stored k in the buffer
  //  bd->frange_bottom->n_k=0; //number of stored k in the buffer
    // bd->frange_top->k_buffer=calloc(MAX_K_BUFFER_SIZE,sizeof(float));
    // bd->frange_bottom->k_buffer=calloc(MAX_K_BUFFER_SIZE,sizeof(float));
  //  bd->frange_top->sum_k=0;
  //  bd->frange_bottom->sum_k=0;
  //  bd->frange_top->c_k=0;
  //  bd->frange_bottom->c_k=0;
    bd->frange_top->fpb = (fdcmp **) calloc(im_buffer_size,sizeof(fdcmp*));
    bd->frange_bottom->fpb = (fdcmp **) calloc(im_buffer_size,sizeof(fdcmp*));
    for (int iii = 0;iii<im_buffer_size;iii++)
    {
      bd->frange_top->fpb[iii] = (fdcmp *) calloc(1,sizeof(fdcmp));
      bd->frange_bottom->fpb[iii] = (fdcmp *) calloc(1,sizeof(fdcmp));
      bd->frange_top->fpb[iii]->amplitude = (double *) calloc(nb_of_modes,sizeof(double));
      bd->frange_top->fpb[iii]->phase = (double *) calloc(nb_of_modes,sizeof(double));
      bd->frange_bottom->fpb[iii]->amplitude = (double *) calloc(nb_of_modes,sizeof(double));
      bd->frange_bottom->fpb[iii]->phase = (double *) calloc(nb_of_modes,sizeof(double));
    }

    bd->w = gsl_matrix_calloc(nb_of_modes * im_buffer_size,nb_of_modes * im_buffer_size);
    bd->matx = gsl_matrix_calloc(nb_of_modes * im_buffer_size, nb_of_modes + im_buffer_size - 1 );
    bd->frange_top->matxt = gsl_matrix_calloc(nb_of_modes + im_buffer_size - 1, nb_of_modes * im_buffer_size);
    bd->frange_bottom->matxt = gsl_matrix_calloc(nb_of_modes + im_buffer_size - 1, nb_of_modes * im_buffer_size);
    bd->temp = gsl_matrix_calloc(nb_of_modes * im_buffer_size, nb_of_modes + im_buffer_size - 1);
    bd->temp2 = gsl_matrix_calloc(nb_of_modes + im_buffer_size - 1,nb_of_modes + im_buffer_size - 1);
    bd->temp3 = gsl_matrix_calloc(nb_of_modes + im_buffer_size - 1,nb_of_modes + im_buffer_size - 1);
    bd->temp4 = gsl_matrix_calloc(nb_of_modes + im_buffer_size - 1, nb_of_modes * im_buffer_size);
    bd->maty = gsl_vector_calloc(nb_of_modes * im_buffer_size);
    bd->beta = gsl_vector_calloc(nb_of_modes + im_buffer_size - 1);
    bd->perm = gsl_permutation_calloc(nb_of_modes + im_buffer_size - 1);


    bd->frange_top->avg_profile = calloc(filter_high-filter_low+1,sizeof(float));
    bd->frange_bottom->avg_profile = calloc(filter_high-filter_low+1,sizeof(float));
    bd->frange_top->avg_profile_er = calloc(filter_high-filter_low+1,sizeof(float));
    bd->frange_bottom->avg_profile_er = calloc(filter_high-filter_low+1,sizeof(float));
    bd->frange_bottom->fitphase = build_data_set(filter_high-filter_low+1,filter_high-filter_low+1);
    bd->frange_top->fitphase = build_data_set(filter_high-filter_low+1,filter_high-filter_low+1);


    for (int ijij=0;ijij<filter_high-filter_low+1;ijij++)
    {
      bd->frange_bottom->fitphase->xd[ijij] = filter_low+ijij;
      bd->frange_top->fitphase->xd[ijij] = filter_low+ijij;
    }
    bd->frange_top->init_cycles = 0;
    bd->frange_bottom->init_cycles = 0;

    bd->frange_top->discretization = discretization;
    bd->frange_bottom->discretization = discretization;

    bd->frange_top->profile_size = discretization*cl;
    bd->frange_bottom->profile_size = discretization*cl;

    bd->frange_top->mean_profile = calloc(discretization*cl,sizeof(float));
    bd->frange_bottom->mean_profile = calloc(discretization*cl,sizeof(float));

    bd->frange_top->scores = calloc(2*disp_range*discretization,sizeof(float));
    bd->frange_bottom->scores = calloc(2*disp_range*discretization,sizeof(float));

    bd->frange_top->temp_dsignal = calloc(discretization*cl,sizeof(float));
    bd->frange_bottom->temp_dsignal = calloc(discretization*cl,sizeof(float));

    bd->frange_top->mean_profile_nb = calloc(discretization*cl,sizeof(int));
    bd->frange_bottom->mean_profile_nb = calloc(discretization*cl,sizeof(int));

    bd->frange_top->photons_per_level = Pico_param.SDI_2_param.photons_per_level;
    bd->frange_bottom->photons_per_level = Pico_param.SDI_2_param.photons_per_level;

    bd->frange_top->cl =cl;
    bd->frange_bottom->cl =cl;

    bd->frange_top->disp_range = disp_range;
    bd->frange_bottom->disp_range = disp_range;

    bd->problem_fringes=0;
    bd->frange_top->ifx = xc;
    bd->frange_bottom->ifx = xc;
    bd->frange_top->fy = y0+fringes_dist/2;
    bd->frange_bottom->fy = y0-fringes_dist/2;
    bd->frange_top->ify = (int)(0.5 + bd->frange_top->fy);
    bd->frange_bottom->ify = (int)(0.5 + bd->frange_bottom->fy);
    bd->frange_top->buffer_full = 0;
    bd->frange_top->c_buff = 0;
    bd->frange_bottom->buffer_full = 0;
    bd->frange_bottom->c_buff = 0;
    bd->h_fringes = cw; // fringes height
    max_2 = (cl>cw) ? cl : cw;
    bd->frange_top->dsfit=build_adjust_data_set(NULL,max_2,max_2);
    alloc_data_set_y_error(bd->frange_top->dsfit);
    bd->frange_bottom->dsfit=build_adjust_data_set(NULL,max_2,max_2);
    alloc_data_set_y_error(bd->frange_bottom->dsfit);
    bd->type = 1;

    bd->frange_top->fp=fftbt_init(NULL,cl);
    bd->frange_bottom->fp=fftbt_init(NULL,cl);

    bd->frange_top->xi=calloc(cl,sizeof(float));
    bd->frange_top->yi=calloc(cw,sizeof(float));
    bd->frange_bottom->xi=calloc(cl,sizeof(float));
    bd->frange_bottom->yi=calloc(cw,sizeof(float));
    bd->frange_top->yslope = 0;
    bd->frange_bottom->yslope = 0;

    bd->free_bead = 0;


    bd->frange_top->xibp=calloc(cl,sizeof(float));
    bd->frange_bottom->xibp=calloc(cl,sizeof(float));

    bd->frange_top->xf=calloc(cl,sizeof(float));
    bd->frange_bottom->xf=calloc(cl,sizeof(float));



    bd->frange_top->fit_phase_coeffs = calloc(2,sizeof(double));
    bd->frange_bottom->fit_phase_coeffs = calloc(2,sizeof(double));
    bd->frange_top->fit_max_coeffs = calloc(3,sizeof(double));
    bd->frange_bottom->fit_max_coeffs = calloc(3,sizeof(double));
    bd->frange_top->fit_phase_corr = build_data_set(filter_high-filter_low+1,filter_high-filter_low+1);
    bd->frange_bottom->fit_phase_corr = build_data_set(filter_high-filter_low+1,filter_high-filter_low+1);
    alloc_data_set_y_error(bd->frange_top->fit_phase_corr);
    alloc_data_set_y_error(bd->frange_bottom->fit_phase_corr);


#endif


    for (i = 0; i < bd->cl; i++)
    {
        bd->rad_prof_ref[i] = -1;    // cannot be a profile
    }

    bd->apx = 1;
    bd->apy = 1;
    bd->generic_calib_im = NULL;
    bd->generic_calib_im_fil = NULL;
    bd->calib_im = NULL;
    bd->calib_im_fil = NULL;
    #if !defined(SDI_VERSION)
    bd->bp_center = bd->calib_im_size / 8; //16;
    bd->bp_width = bd->calib_im_size / 10; //12;
    #else
    bd->bp_center = cl / 8;
    bd->bp_width = cl / 10;
    #endif
    bd->dis_filter = cl >> 2;
    bd->max_limit = cl >> 1;
    bd->calibration_im_x = -1;
    bd->calibration_im_y = -1;
    bd->calibration_im_xi = -1;
    bd->calibration_im_yi = -1;
    bd->fence_x = -1;
    bd->fence_y = -1;
    bd->rc = 12;
    //bd->npc = track_info->cl;
    #if !defined(SDI_VERSION)
    bd->npc = bd->calib_im_size;
    bd->z_trp = fftbt_init(NULL, bd->calib_im_size);
    bd->z_fil = filter_init_bt(NULL, bd->calib_im_size);
    #else
    bd->npc = cl;
   bd->z_trp = fftbt_init(NULL, cl);
   bd->z_fil = filter_init_bt(NULL, cl);
    #endif
    bd->orthoradial_prof_size = 0;
    bd->orthoradial_mean_radius = 0;
    bd->orthoradial_half_avg_size = 0;

//pb: not sdi_version: pas alloue!
    if (bd->xy_trp == NULL || bd->z_trp == NULL || bd->xy_fil == NULL || bd->z_fil == NULL)
    {
        return win_printf_OK("cannot alloc FFT or Filter plans!");
    }

    bd->not_lost = -1;
    bd->calib_im = NULL;
    bd->calib_im_fil = NULL;
    track_info->n_bead_display = track_info->n_b;
    track_info->bd[track_info->n_b++] = bd;
    prepare_bead_menu();
    // win_printf("bead added %d",track_info->n_b);
    LOCK_VARIABLE(bd);
    //bd->cw = track_info->cw;
    bd->saved_x = bd->last_x_not_lost = (float)xc;
    bd->saved_y = bd->last_y_not_lost = (float)yc;
    bd->xc = bd->x0 = xc;
    bd->yc = bd->y0 = yc;
    bd->im_prof_ref = -1;
    bd->mouse_draged = 0;
    bd->not_lost = NB_FRAMES_BEF_LOST;
    bd->start_im = 0;
    bid.param = (void *)track_info;
    //  bid.to_do = track_in_x_y_IFC;
    bid.timer_do = track_in_x_y_timer;
#ifndef SDI_VERSION
    if (bd->type == 0)     link_beads_to_generic_calibration();
#endif
    //d = find_dialog_associated_to_imr(imrs, NULL);
    //write_and_blit_im_box( plt_buffer, d, imrs);
    //move_bead_cross(screen, imrs, d, xc, yc);
    ask_to_update_menu();
    return 0;
}
#endif

int do_configure_create_cross_by_shortcut(void)
{
    int is_create_cross_by_shortcut_enabled = g_is_cross_shortcut_create_enabled;
    int cl = g_cross_shortcut_create_length;
    int cw = g_cross_shortcut_create_width;
    int enable_fence = g_cross_shortcut_create_enable_fence;
    int icl = 0;
    int ret = 0;
    static int automatic_modes = 1,automatic_fixed=1;
    const int low_ff_128 = 10;
    const int high_ff_128 = 17;


    if (updating_menu_state != 0)
    {
        remove_short_cut_from_dialog(0, KEY_B);
        remove_short_cut_from_dialog(0, KEY_F);

        if (!is_create_cross_by_shortcut_enabled)
        {
            add_keyboard_short_cut(0, KEY_B, 0, do_configure_create_cross_by_shortcut);
            add_keyboard_short_cut(0, KEY_F, 0, do_configure_create_cross_by_shortcut);
        }
        else
        {
            add_keyboard_short_cut(0, KEY_B, 0, create_cross_by_shortcut);
            add_keyboard_short_cut(0, KEY_F, 0, create_fixed_cross_by_shortcut);
        }

        return D_O_K;
    }

    if (cl == 0 && cw == 0)
    {
        cl =  get_config_float("CROSS", "cross_length", 128);
        cw =  get_config_float("CROSS", "cross_width", 16);
        enable_fence =  get_config_int("CROSS", "cross_shortcut_create_enable_fence", 1);
    }

    if (cl == 16)
    {
        icl = 0;
    }
    else if (cl == 32)
    {
        icl = 1;
    }
    else if (cl == 64)
    {
        icl = 2;
    }
    else
    {
        icl = 3;
    }
#ifndef SDI_VERSION_2

    ret = win_scanf("Create cross by pressing B %b\n"
                    "Crosses will be created with the following parameters:\n"
                    "  length 16->%R 32->%r 64->%r 128->%r\n"
                    "  width %3d\n"
                    "%b enable fence",
                    &is_create_cross_by_shortcut_enabled, &icl, &cw, &enable_fence); // %m ,select_power_of_2()
#endif
#ifdef SDI_VERSION_2

    sdi2_om_cos = 2*M_PI/sdi2_om_cos;

    ret = win_scanf("Create an SDI cross by pressing B %b\n"
                    "Crosses will be created with the following parameters:\n"
                    "  length along fringes 16->%R 32->%r 64->%r 128->%r  256 ->%r\n"
                    "  fringes width %3d\n"
		    "  typical distance between fringes %3d\n"
		    "  tolerence on distance between fringes %3d\n"
		    "  Extend over which fringes are tracked in y %3d\n"
		    "  Maximum shift of fringes in x %3d\n"
                    "%b enable fence\n"
        " Indicate the buffer size to compute phase signal %6d\n"
        " %b Automatically compute the Fourier filter ?\n "
        "If not, indicate low filter %6d and high filter %6d\n"
        " %b Automatically compute the fixed mode ?\n "
        "If not, indicate fixed mode %6d\n"
        "Fourier computation -> %R Project on profile ->%r Barycenter computation ->%r \n"
        "If project on profile enter "
        "sigma gaussian %6f and fringe period %6f\n"
        "and max iter %6d rel eps %6f abs eps %6f\n"
                 ,
                    &is_create_cross_by_shortcut_enabled, &icl, &cw,&dy_fringes,
		    &dy_fringes_tolerence,&fringes_ny,&max_shear_dx,&enable_fence,&sdi2_buffer_size,&automatic_modes,&filter_sdi2_low ,&filter_sdi2_high,&automatic_fixed,&sdi2_fixed_mode,
      &sdi2_do_minimize,&sdi2_sigma_g,&sdi2_om_cos,&sdi2_min_max_iter,&sdi2_min_epsrel,&sdi2_min_epsabs);

sdi2_om_cos = 2*M_PI/sdi2_om_cos;


#endif



    if (ret == WIN_CANCEL)
    {
        return OFF;
    }

    if (icl == 0)
    {
        cl = 16;
    }
    else if (icl == 1)
    {
        cl = 32;
    }
    else if (icl == 2)
    {
        cl = 64;
    }
    else if (icl==3)
    {
        cl = 128;
    }

    else
    {
      cl=256;
    }

    if (automatic_modes)
    {
        filter_sdi2_low = (int) ((float) low_ff_128 * (float)cl / 128.0);
        filter_sdi2_high = (int) ((float) high_ff_128 * (float)cl / 128.0);
    }

    if(automatic_fixed)
    {
      sdi2_fixed_mode = (filter_sdi2_low + filter_sdi2_high) / 2 ;
    }

    set_config_float("CROSS", "cross_length", cl);
    set_config_float("CROSS", "cross_width", cw);
    set_config_int("CROSS", "cross_shortcut_create_enable_fence", enable_fence);
    remove_short_cut_from_dialog(0, KEY_B);
    remove_short_cut_from_dialog(0, KEY_F);

    if (!is_create_cross_by_shortcut_enabled)
    {
        add_keyboard_short_cut(0, KEY_B, 0, do_configure_create_cross_by_shortcut);
        add_keyboard_short_cut(0, KEY_F, 0, do_configure_create_cross_by_shortcut);
    }
    else
    {
        add_keyboard_short_cut(0, KEY_B, 0, create_cross_by_shortcut);
        add_keyboard_short_cut(0, KEY_F, 0, create_fixed_cross_by_shortcut);
    }

    g_cross_shortcut_create_width = cw;
    g_cross_shortcut_create_length = cl;
    g_cross_shortcut_create_enable_fence = !!enable_fence;
    g_is_cross_shortcut_create_enabled = !!is_create_cross_by_shortcut_enabled;
    return D_O_K;
}

int create_cross_by_shortcut(void)
{
    int x = 0;
    int y = 0;
    imreg *imrs = NULL;
    O_i *ois = NULL;
    int i = 0;

    if (mouse_needs_poll())
    {
        poll_mouse();
    }

    if (ac_grep(NULL, "%im%oi", &imrs, &ois) != 2)
    {
        return win_printf_OK("cannot find image in acreg!");
    }

    x = round(x_imr_2_imdata(imrs, mouse_x));
    y = round(y_imr_2_imdata(imrs, mouse_y));

    if (g_is_cross_shortcut_create_enabled)
    {
#ifdef SDI_VERSION
        do_add_bead_in_x_y(g_cross_shortcut_create_length, g_cross_shortcut_create_width, x, y);
#else
      i = do_add_bead_in_x_y(g_cross_shortcut_create_length, g_cross_shortcut_create_width,
			   (calib_im_nx_equal_cross_arm_length) ? g_cross_shortcut_create_length : calib_im_nx_set,
			   dy_fringes, dy_fringes_tolerence, fringes_ny,
			   x, y,filter_sdi2_low,  filter_sdi2_high,  sdi2_buffer_size,sdi2_fixed_mode,sdi2_discretization,sdi2_disp_range,
       sdi2_do_minimize,sdi2_sigma_g,sdi2_om_cos,sdi2_min_max_iter,sdi2_min_epsrel,sdi2_min_epsabs);
         if (i == 1) return 0;
#endif
        if (g_cross_shortcut_create_enable_fence)
        {
            track_info->bd[track_info->n_b - 1]->saved_x = x * oi_TRACK->dx;
            track_info->bd[track_info->n_b - 1]->saved_y = y * oi_TRACK->dy;
            track_info->bd[track_info->n_b - 1]->fence_x = x * oi_TRACK->dx;
            track_info->bd[track_info->n_b - 1]->fence_y = y * oi_TRACK->dy;
            track_info->bd[track_info->n_b - 1]->fence_xi = x;
            track_info->bd[track_info->n_b - 1]->fence_yi = y;
            track_info->bd[track_info->n_b - 1]->xc = x;
            track_info->bd[track_info->n_b - 1]->yc = y;
        }
    }

    ask_to_update_menu();
    return 0;
}

int create_fixed_cross_by_shortcut(void)
{
    int x = 0;
    int y = 0;
    imreg *imrs = NULL;
    O_i *ois = NULL;
    int i = 0;

    if (mouse_needs_poll())
    {
        poll_mouse();
    }

    if (ac_grep(NULL, "%im%oi", &imrs, &ois) != 2)
    {
        return win_printf_OK("cannot find image in acreg!");
    }

    x = round(x_imr_2_imdata(imrs, mouse_x));
    y = round(y_imr_2_imdata(imrs, mouse_y));

    if (g_is_cross_shortcut_create_enabled)
    {
#ifdef SDI_VERSION
        do_add_bead_in_x_y(g_cross_shortcut_create_length, g_cross_shortcut_create_width, x, y);
#else
        i = do_add_bead_in_x_y(g_cross_shortcut_create_length, g_cross_shortcut_create_width,
			   (calib_im_nx_equal_cross_arm_length) ? g_cross_shortcut_create_length : calib_im_nx_set,
			   dy_fringes, dy_fringes_tolerence, fringes_ny,
			   x, y,filter_sdi2_low,  filter_sdi2_high,  sdi2_buffer_size,sdi2_fixed_mode,sdi2_discretization,sdi2_disp_range,
         sdi2_do_minimize,sdi2_sigma_g,sdi2_om_cos,sdi2_min_max_iter,sdi2_min_epsrel,sdi2_min_epsabs
       );
        if (i == 1) return 0;
#endif
        track_info->bd[track_info->n_b -1]->fixed_bead = 1;
        if (g_cross_shortcut_create_enable_fence)
        {

            track_info->bd[track_info->n_b - 1]->saved_x = x * oi_TRACK->dx;
            track_info->bd[track_info->n_b - 1]->saved_y = y * oi_TRACK->dy;
            track_info->bd[track_info->n_b - 1]->fence_x = x * oi_TRACK->dx;
            track_info->bd[track_info->n_b - 1]->fence_y = y * oi_TRACK->dy;
            track_info->bd[track_info->n_b - 1]->fence_xi = x;
            track_info->bd[track_info->n_b - 1]->fence_yi = y;
            track_info->bd[track_info->n_b - 1]->xc = x;
            track_info->bd[track_info->n_b - 1]->yc = y;
        }
    }

    ask_to_update_menu();
    return 0;
}

int do_follow_bead_in_x_y(int mode)
{
    int i;
    imreg *imrs = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
#ifndef SDI_VERSION
    O_i *oi = NULL;
    int j;
    int rc, fw, fc, xc = 256, yc = 256,  bnx = 0, bcw = 0;
    float xb = 0, yb = 0, zb = 0;
#endif
    O_i *ois = NULL;
    int icl = 4, dis_state, size_chosen = 0;
    static int cl = 128, cw = 16;
    //, nf = 2048, imstart = 10, prof = 0
    //b_track *bd = NULL;
    int n_bead = 1;
    float zobj;
    //DIALOG *d = NULL;
    if (ac_grep(NULL, "%im%oi", &imrs, &ois) != 2) return win_printf_OK("cannot find image in acreg!");
    dis_state = do_refresh_overlay;
    do_refresh_overlay = 0;
    if (mode & MULTIPLE_BEADS)
    {
        if (mode & CHOOSE_SIZE)
        {
            if (cl == 16)  icl = 0;
            else if (cl == 32) icl = 1;
            else if (cl == 64) icl = 2;
            else icl = 3;
            i = win_scanf("How many beads do you want \nto create %10d\n"
                          "using a cross shaped pattern \n"
                          "arm length 16->%R 32->%r 64->%r 128->%r\narm width %3d\n"
                          , &n_bead, &icl, &cw); // %m ,select_power_of_2()
            if (i == WIN_CANCEL)  return OFF;
            if (icl == 0)  cl = 16;
            else if (icl == 1)
            {
                cl = 32;
            }
            else if (icl == 2)
            {
                cl = 64;
            }
            else
            {
                cl = 128;
            }
            size_chosen = 1;
        }
        else
        {
            i = win_scanf("How many beads do you want \nto create %10d\n", &n_bead);
            if (i == WIN_CANCEL) return 0;
        }
    }
    for (; n_bead > 0; n_bead--)
    {

//those are not required any more in sdi
#ifndef SDI_VERSION
#ifndef SDI_VERSION_2

        if (mode == IMAGE_CAL)
        {
            oi = do_load_calibration(0);
            if (grab_bead_info_from_calibration(oi, &xb, &yb, &zb, &bnx, &bcw))
            {
                return win_printf_OK("error in retrieving bead data");
            }
            cl = bnx;
            cw = bcw;
        }
        else if (mode & GENERIC_IMAGE_CAL)
        {
            j = 0xFF & mode;
            if (track_info->generic_calib_im[j] == NULL || track_info->generic_calib_im[j]->filename == NULL)
            {
                return win_printf_OK("Bad generic image");
            }
            if (grab_bead_info_from_calibration(track_info->generic_calib_im[j], &xb, &yb, &zb, &bnx, &bcw))
            {
                return win_printf_OK("error in retrieving bead data");
            }
            if (grab_bead_filter_info_from_calibration(track_info->generic_calib_im[j], &rc, &fc, &fw))
            {
                return win_printf_OK("error in retrieving bead data");
            }
            cl = bnx;
            cw = bcw;
            //cl_size_selected = 1;
        }
        else if (mode == 0)
        {
            cl = 128;
            cw = 16;
            //cl_size_selected = 1;
        }
        else if ((size_chosen == 0) && (mode & CHOOSE_SIZE))
        {
            if (cl == 16) icl = 0;
            else if (cl == 32) icl = 1;
            else if (cl == 64) icl = 2;
            else icl = 3;
            i = win_scanf("this routine track a bead in X,Y\n"
                          " using a cross shaped pattern \n"
                          "arm length 16->%R 32->%r 64->%r 128->%r\narm width %3d\n"
                          , &icl, &cw); // %m ,select_power_of_2()
            if (i == WIN_CANCEL) return OFF;
            if (icl == 0) cl = 16;
            else if (icl == 1) cl = 32;
            else if (icl == 2) cl = 64;
            else cl = 128;
            size_chosen = 1;
        }
        if (cl > 128) return win_printf_OK("cannot use arm bigger\nthan 128");
        if (cw > 32) return win_printf_OK("cannot use width bigger\nthan 32");
        if (fft_init(cl) || filter_init(cl)) return win_printf_OK("cannot init fft!");
        do_refresh_overlay = dis_state;
        xc = yc = 0;
        if (mode == IMAGE_CAL)
        {
            xc = (int)(xb - oi->ax) / oi->dx;
            yc = (int)(yb - oi->ay) / oi->dy;
        }
        else if (mode & POSITION_SET_BY_MOUSE)
        {
            xc = (int)(0.5 + x_imr_2_imdata(imrs, mouse_x));
            yc = (int)(0.5 + y_imr_2_imdata(imrs, mouse_y));
            if (mode & MULTIPLE_BEADS) yc += n_bead * 10;
        }
        else
        {
            xc = ois->im.nx + 2 * cl / 3;
            yc = cl / 2 + track_info->n_b * cl;
            ois->need_to_refresh |= BITMAP_NEED_REFRESH;
        }
#endif
#endif
        if (track_info == NULL) track_info = creating_track_info();
        if (track_info == NULL) return win_printf_OK("cannot alloc track info!");
        if (track_info->cl == -1)
        {
            // we define tracking parameters
            track_info->cl = cl;
            track_info->cw = cw;
            track_info->bd_mul = 16;
            track_info->dis_filter = cl >> 2;
            track_info->max_limit = cl >> 1;
            track_info->m_b = 16;
            track_info->n_b = track_info->c_b = 0;
            track_info->bead_servo_nb = -1;
            track_info->do_diff_track = 1;
            //starting_one = active_dialog; // to check if windows change
            //index_menu_dialog = retrieve_item_from_dialog(d_menu_proc, NULL);
            //index_menu_dialog = 1;
            track_info->user_tracking_action = NULL;
            // second plots to check tracking
            op = create_and_attach_one_plot(pr_TRACK,  TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0);
            ds = op->dat[0];
            ds->xd[1] = ds->yd[1] = 1;
            set_ds_source(ds, "X rolling buffer");
            set_ds_line_color(ds, Yellow);
            if ((ds = create_and_attach_one_ds(op, TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0)) == NULL)
            {
                return win_printf_OK("I can't create plot !");
            }
            ds->xd[1] = ds->yd[1] = 1;
            set_ds_source(ds, "Y rolling buffer");
            set_ds_line_color(ds, Lightgreen);
            if ((ds = create_and_attach_one_ds(op, TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0)) == NULL)
            {
                return win_printf_OK("I can't create plot !");
            }
            ds->xd[1] = ds->yd[1] = 1;
            set_ds_source(ds, "Z rolling buffer");
            set_ds_line_color(ds, Lightred);
#ifdef GAME
            if ((ds = create_and_attach_one_ds(op, TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0)) == NULL)
            {
                ds->xd[1] = ds->yd[1] = 1;
            }
            set_ds_source(ds, "X rolling buffer of simulation");
            set_ds_line_color(ds, orange);
            if ((ds = create_and_attach_one_ds(op, TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0)) == NULL)
            {
                return win_printf_OK("I can't create plot !");
            }
            ds->xd[1] = ds->yd[1] = 1;
            set_ds_source(ds, "Y rolling buffer of simulation");
            set_ds_line_color(ds, darkgreen);
            if ((ds = create_and_attach_one_ds(op, TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0)) == NULL)
            {
                return win_printf_OK("I can't create plot !");
            }
            ds->xd[1] = ds->yd[1] = 1;
            set_ds_source(ds, "Z rolling buffer of simulation");
            set_ds_line_color(ds, darkred);
#endif
            if ((ds = create_and_attach_one_ds(op, TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0)) == NULL)
            {
                return win_printf_OK("I can't create plot !");
            }
            ds->xd[1] = ds->yd[1] = 1;
            set_ds_source(ds, "Xcor rolling buffer");
            set_ds_line_color(ds, azure);
            ds->invisible = 1;
            if ((ds = create_and_attach_one_ds(op, TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0)) == NULL)
            {
                return win_printf_OK("I can't create plot !");
            }
            ds->xd[1] = ds->yd[1] = 1;
            set_ds_source(ds, "Ycor rolling buffer");
            set_ds_line_color(ds, blue);
            ds->invisible = 1;
            if ((ds = create_and_attach_one_ds(op, TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0)) == NULL)
            {
                return win_printf_OK("I can't create plot !");
            }
            ds->xd[1] = ds->yd[1] = 1;
            set_ds_source(ds, "Xnoise rolling buffer");
            set_ds_line_color(ds, fuchsia);
            ds->invisible = 1;
            if ((ds = create_and_attach_one_ds(op, TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0)) == NULL)
            {
                return win_printf_OK("I can't create plot !");
            }
            ds->xd[1] = ds->yd[1] = 1;
            set_ds_source(ds, "Ynoise rolling buffer");
            set_ds_line_color(ds, indigo);
            ds->invisible = 1;
            uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
            op->op_idle_action = z_rolling_buffer_idle_action;
            op->op_post_display = general_op_post_display;
            set_op_filename(op, "X(t)Y(t)Z(t).gr");
            op->width = rolling_plot_width * (float)SCREEN_W / SCREEN_H;
            op->user_ispare[ISPARE_BEAD_NB] = 0;
            set_plot_x_title(op, "Time");
            set_plot_y_title(op, "{\\color{yellow} X} {\\color{lightgreen} Y} Z");
            create_attach_select_x_un_to_op(op, IS_SECOND, 0
                                            , (float)1 / Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");
            op = create_and_attach_one_plot(pr_TRACK, MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0);
            ds = op->dat[0];
            ds->xd[1] = ds->yd[1] = 1;
            set_ds_source(ds, "Instantaneous radial profile");
            if ((ds = create_and_attach_one_ds(op,  MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0)) == NULL)
            {
                return win_printf_OK("I can't create plot !");
            }
            set_ds_source(ds, "Reference radial profile");
            op->op_idle_action = profile_rolling_buffer_idle_action;
            op->op_post_display = general_op_post_display;
            op->user_ispare[ISPARE_BEAD_NB] = 0;
            set_op_filename(op, "Profiles.gr");
            op->width = rolling_plot_width * (float)SCREEN_W / SCREEN_H;
            uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_X_UNIT_SET);
            set_plot_x_title(op, "Radius");
            set_plot_y_title(op, "Light intensity");

#if defined(OPENCL)

            if ((ds = create_and_attach_one_ds(op,  MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0)) == NULL)
            {
                return win_printf_OK("I can't create plot !");
            }
            set_ds_source(ds, "Instantaneous GPU radial profile");
#endif

#ifndef SDI_VERSION_2
#ifndef SDIPICO


op = create_and_attach_one_plot(pr_TRACK, MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0);
ds = op->dat[0];
ds->xd[1] = ds->yd[1] = 1;
set_ds_source(ds, "Instantaneous x profile");

if ((ds = create_and_attach_one_ds(op,  MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0)) == NULL)
{
    return win_printf_OK("I can't create plot !");
}
set_ds_source(ds, "side x profile");
if ((ds = create_and_attach_one_ds(op,  MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0)) == NULL)
{
    return win_printf_OK("I can't create plot !");
}
set_ds_source(ds, "diff x profile");
if ((ds = create_and_attach_one_ds(op,  MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0)) == NULL)
{
    return win_printf_OK("I can't create plot !");
}
set_ds_source(ds, "Reference x profile");
if ((ds = create_and_attach_one_ds(op,  MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0)) == NULL)
{
    return win_printf_OK("I can't create plot !");
}
set_ds_source(ds, "X profile variance");
if ((ds = create_and_attach_one_ds(op,  MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0)) == NULL)
{
    return win_printf_OK("I can't create plot !");
}
set_ds_source(ds, "Xs profile variance");
op->op_idle_action = x_profile_rolling_buffer_idle_action;
op->op_post_display = general_op_post_display;
op->user_ispare[ISPARE_BEAD_NB] = 0;
set_op_filename(op, "ProfilesX.gr");
op->width = rolling_plot_width * (float)SCREEN_W / SCREEN_H;
uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_X_UNIT_SET);
set_plot_x_title(op, "delta x");
set_plot_y_title(op, "Light intensity");


#endif
#endif // #ifndef SDI_VERSION_2


#ifdef SDI_VERSION_2

            op = create_and_attach_one_plot(pr_TRACK, MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0);
            ds = op->dat[0];
            ds->xd[1] = ds->yd[1] = 1;
            set_ds_source(ds, "Instantaneous x top shear profile");
            if ((ds = create_and_attach_one_ds(op,  MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0)) == NULL)
            {
                return win_printf_OK("I can't create plot !");
            }
            set_ds_source(ds, "Instantaneous x bottom shear profile");
            if ((ds = create_and_attach_one_ds(op,  MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0)) == NULL)
            {
                return win_printf_OK("I can't create plot !");
            }
            set_ds_source(ds, "Instantaneous x top bandpass profile");
            if ((ds = create_and_attach_one_ds(op,  MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0)) == NULL)
            {
                return win_printf_OK("I can't create plot !");
            }
            set_ds_source(ds, "Instantaneous x bottom bandpass profile");
            if ((ds = create_and_attach_one_ds(op,  MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0)) == NULL)
            {
                return win_printf_OK("I can't create plot !");
            }
            set_ds_source(ds, "Minimization profile");

            op->op_idle_action = x_shear_profiles_rolling_buffer_idle_action;
            op->op_post_display = general_op_post_display;
            op->user_ispare[ISPARE_BEAD_NB] = 0;
            set_op_filename(op, "ProfilesX.gr");
            op->width = rolling_plot_width * (float)SCREEN_W / SCREEN_H;
            uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_X_UNIT_SET);
            set_plot_x_title(op, "delta x");
            set_plot_y_title(op, "Light intensity");


            op = create_and_attach_one_plot(pr_TRACK, MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0);
            ds = op->dat[0];
            ds->xd[1] = ds->yd[1] = 1;
            set_ds_source(ds, "Instantaneous projected y profile bottom");
            if ((ds = create_and_attach_one_ds(op,  MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0)) == NULL)
            {
                return win_printf_OK("I can't create plot !");
            }
            set_ds_source(ds, "Instantaneous projected y profile top");


            op->op_idle_action = y_shear_profiles_rolling_buffer_idle_action;
            op->op_post_display = general_op_post_display;
            op->user_ispare[ISPARE_BEAD_NB] = 0;
            set_op_filename(op, "ProfilesY.gr");
            op->width = rolling_plot_width * (float)SCREEN_W / SCREEN_H;
            uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_X_UNIT_SET);
            set_plot_x_title(op, "delta y");
            set_plot_y_title(op, "Light intensity");


            op = create_and_attach_one_plot(pr_TRACK, 130000, 130000, 0);
            ds = op->dat[0];
            ds->xd[1] = ds->yd[1] = 1;
            set_ds_source(ds, "Real mean profile top");
            if ((ds = create_and_attach_one_ds(op,  130000, 130000, 0)) == NULL)
            {
                return win_printf_OK("I can't create plot !");
            }
            set_ds_source(ds, "Real mean profile bottom");



            op->op_idle_action = SDI2_mean_real_profile_idle_action;
            op->op_post_display = general_op_post_display;
            op->user_ispare[ISPARE_BEAD_NB] = 0;
            set_op_filename(op, "realmeanprofile.gr");
            op->width = rolling_plot_width * (float)SCREEN_W / SCREEN_H;
            uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_X_UNIT_SET);
            set_plot_x_title(op, "delta y");
            set_plot_y_title(op, "mean profile");


            op = create_and_attach_one_plot(pr_TRACK, 130000, 130000, 0);
            ds = op->dat[0];
            ds->xd[1] = ds->yd[1] = 1;
            set_ds_source(ds, "Scores top");
            if ((ds = create_and_attach_one_ds(op,  130000, 130000, 0)) == NULL)
            {
                return win_printf_OK("I can't create plot !");
            }
            set_ds_source(ds, "Scores bottom");



            op->op_idle_action = SDI2_scores_idle_action;
            op->op_post_display = general_op_post_display;
            op->user_ispare[ISPARE_BEAD_NB] = 0;
            set_op_filename(op, "scores.gr");
            op->width = rolling_plot_width * (float)SCREEN_W / SCREEN_H;
            uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_X_UNIT_SET);
            set_plot_x_title(op, "delta y");
            set_plot_y_title(op, "scores");



            op = create_and_attach_one_plot(pr_TRACK, MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0);
            ds = op->dat[0];
            ds->xd[1] = ds->yd[1] = 1;
            set_ds_source(ds, "Average phase profile top");
            if ((ds = create_and_attach_one_ds(op,  MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0)) == NULL)
            {
                return win_printf_OK("I can't create plot !");
            }
            set_ds_source(ds, "Average phase profile bottom");


            op->op_idle_action = average_profile_idle_action;
            op->op_post_display = general_op_post_display;
            op->user_ispare[ISPARE_BEAD_NB] = 0;
            set_op_filename(op, "averageprofile.gr");
            op->width = rolling_plot_width * (float)SCREEN_W / SCREEN_H;
            set_plot_x_title(op, "Mode");
            set_plot_y_title(op, "Phase");







            op = create_and_attach_one_plot(pr_TRACK, MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0);
            ds = op->dat[0];
            ds->xd[1] = ds->yd[1] = 1;

            set_ds_source(ds, "Fitted phase profile top");

            if ((ds = create_and_attach_one_ds(op,  MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0)) == NULL)
                {
                  return win_printf_OK("I can't create plot !");
              }
            set_ds_source(ds, "Fitted phase profile bottom");



            op->op_idle_action = phase_fitting_rolling_buffer_idle_action;
            op->op_post_display = general_op_post_display;
            op->user_ispare[ISPARE_BEAD_NB] = 0;
            set_op_filename(op, "Fit_Phases.gr");
            op->width = rolling_plot_width * (float)SCREEN_W / SCREEN_H;
            uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_X_UNIT_SET);
            set_plot_x_title(op, "Pixels");
            set_plot_y_title(op, "Phase");


            op = create_and_attach_one_plot(pr_TRACK, MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0);
            ds = op->dat[0];
            ds->xd[1] = ds->yd[1] = 1;

            set_ds_source(ds, "Fourier top");

            if ((ds = create_and_attach_one_ds(op,  MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0)) == NULL)
                {
                  return win_printf_OK("I can't create plot !");
              }
            set_ds_source(ds, "Fourier bottom");



            op->op_idle_action = Fourier_rolling_buffer_idle_action;
            op->op_post_display = general_op_post_display;
            op->user_ispare[ISPARE_BEAD_NB] = 0;
            set_op_filename(op, "Fourier.gr");
            op->width = rolling_plot_width * (float)SCREEN_W / SCREEN_H;
            //uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_X_UNIT_SET);
            set_plot_x_title(op, "Mode");
            set_plot_y_title(op, "Amplitude");




            op = create_and_attach_one_plot(pr_TRACK, MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0);
            ds = op->dat[0];
            ds->xd[1] = ds->yd[1] = 1;

            set_ds_source(ds, "Fitted phase profile top");

            if ((ds = create_and_attach_one_ds(op,  MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0)) == NULL)
                {
                  return win_printf_OK("I can't create plot !");
              }
            set_ds_source(ds, "Fitted phase profile bottom");



            op->op_idle_action = phase_fitting_derivative_rolling_buffer_idle_action;
            op->op_post_display = general_op_post_display;
            op->user_ispare[ISPARE_BEAD_NB] = 0;
            set_op_filename(op, "Fit_Phases_derivative.gr");
            op->width = rolling_plot_width * (float)SCREEN_W / SCREEN_H;
            uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_X_UNIT_SET);
            set_plot_x_title(op, "Pixels");
            set_plot_y_title(op, "Phase Derivative");




            op = create_and_attach_one_plot(pr_TRACK, MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0);
            ds = op->dat[0];
            ds->xd[1] = ds->yd[1] = 1;

            set_ds_source(ds, "Phase difference 1");

            if ((ds = create_and_attach_one_ds(op,  MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0)) == NULL)
                            {
                              return win_printf_OK("I can't create plot !");
                          }
            set_ds_source(ds, "Phase difference 2");

            if ((ds = create_and_attach_one_ds(op,  MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0)) == NULL)
                            {
                              return win_printf_OK("I can't create plot !");
                          }
            set_ds_source(ds, "Phase difference 3");


            if ((ds = create_and_attach_one_ds(op,  MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0)) == NULL)
                            {
                              return win_printf_OK("I can't create plot !");
                          }
            set_ds_source(ds, "Phase difference 4");

            if ((ds = create_and_attach_one_ds(op,  MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0)) == NULL)
                            {
                              return win_printf_OK("I can't create plot !");
                          }
            set_ds_source(ds, "Phase 0 top");

            if ((ds = create_and_attach_one_ds(op,  MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0)) == NULL)
                            {
                              return win_printf_OK("I can't create plot !");
                          }
            set_ds_source(ds, "Phase 0 bottom");



            op->op_idle_action = phase_differences_idle_action;
            op->op_post_display = general_op_post_display;
            op->user_ispare[ISPARE_BEAD_NB] = 0;
            set_op_filename(op, "Phase_differences.gr");
            op->width = rolling_plot_width * (float)SCREEN_W / SCREEN_H;
            uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_X_UNIT_SET);
            set_plot_x_title(op, "Mode");
            set_plot_y_title(op, "Phase Difference");


            op = create_and_attach_one_plot(pr_TRACK, MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0);
            ds = op->dat[0];

            set_ds_source(ds, "Current phase difference top");
            alloc_data_set_y_error(ds);

            if ((ds = create_and_attach_one_ds(op,  MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0)) == NULL)
                            {
                              return win_printf_OK("I can't create plot !");
                          }
            set_ds_source(ds, "Fit of current phase difference top");

            if ((ds = create_and_attach_one_ds(op,  MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0)) == NULL)
                            {
                              return win_printf_OK("I can't create plot !");
                          }
            set_ds_source(ds, "Fit of current phase difference bottom");

            if ((ds = create_and_attach_one_ds(op,  MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0)) == NULL)
                            {
                              return win_printf_OK("I can't create plot !");
                          }
            set_ds_source(ds, "Current phase difference bottom");
            alloc_data_set_y_error(ds);




            op->op_idle_action = current_phase_differences_idle_action;
            op->op_post_display = general_op_post_display;
            op->user_ispare[ISPARE_BEAD_NB] = 0;
            set_op_filename(op, "Cur_phase_differences.gr");
            op->width = rolling_plot_width * (float)SCREEN_W / SCREEN_H;
            uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_X_UNIT_SET);
            set_plot_x_title(op, "Mode");
            set_plot_y_title(op, "Cur Phase Difference");




                        op = create_and_attach_one_plot(pr_TRACK, MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0);
                        ds = op->dat[0];
                        ds->xd[1] = ds->yd[1] = 1;

                        set_ds_source(ds, "Phase Buffer 10");

                        if ((ds = create_and_attach_one_ds(op,  MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0)) == NULL)
                                        {
                                          return win_printf_OK("I can't create plot !");
                                      }
                        set_ds_source(ds, "Phase Buffer 11");

                        if ((ds = create_and_attach_one_ds(op,  MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0)) == NULL)
                                        {
                                          return win_printf_OK("I can't create plot !");
                                      }
                        set_ds_source(ds, "Phase Buffer 12");


                        if ((ds = create_and_attach_one_ds(op,  MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0)) == NULL)
                                        {
                                          return win_printf_OK("I can't create plot !");
                                      }
                        set_ds_source(ds, "Phase Buffer 13");



                        op->op_idle_action = phase_buffer_idle_action;
                        op->op_post_display = general_op_post_display;
                        op->user_ispare[ISPARE_BEAD_NB] = 0;
                        set_op_filename(op, "Phase_buffer.gr");
                        op->width = rolling_plot_width * (float)SCREEN_W / SCREEN_H;
                        uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_X_UNIT_SET);
                        set_plot_x_title(op, "Mode");
                        set_plot_y_title(op, "Phase Buffer");


            //
            // op = create_and_attach_one_plot(pr_TRACK, MAX_K_BUFFER_SIZE, MAX_K_BUFFER_SIZE, 0);
            // ds = op->dat[0];
            // ds->xd[1] = ds->yd[1] = 1;
            //
            // set_ds_source(ds, "k_buffer_top");
            //
            // if ((ds = create_and_attach_one_ds(op,  MAX_K_BUFFER_SIZE, MAX_K_BUFFER_SIZE, 0)) == NULL)
            //     {
            //       return win_printf_OK("I can't create plot !");
            //   }
            // set_ds_source(ds, "k_buffer_bottom");




            // op->op_idle_action = k_buffer_idle_action;
            // op->op_post_display = general_op_post_display;
            // op->user_ispare[ISPARE_BEAD_NB] = 0;
            // set_op_filename(op, "k_buffer.gr");
            // op->width = rolling_plot_width * (float)SCREEN_W / SCREEN_H;
            // uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_X_UNIT_SET);
            // set_plot_x_title(op, "k");
            // set_plot_y_title(op, "buffer index");


#endif // #ifdef SDI_VERSION_2

            // fouth plot
            op = create_and_attach_one_plot(pr_TRACK,  TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0);
            ds = op->dat[0];
            ds->xd[1] = ds->yd[1] = 1;
            set_ds_source(ds, "Obj. rolling buffer");
            if ((ds = create_and_attach_one_ds(op, TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0)) == NULL)
            {
                return win_printf_OK("I can't create plot !");
            }
            set_ds_source(ds, "Z rolling buffer");
            op->op_idle_action = obj_rolling_buffer_idle_action;
            op->op_post_display = general_op_post_display;
            op->user_ispare[ISPARE_BEAD_NB] = 0;
            set_op_filename(op, "Objective.gr");
            op->width = rolling_plot_width * (float)SCREEN_W / SCREEN_H;
            set_plot_x_title(op, "Time");
            set_plot_y_title(op, "{\\color{yellow} Focus} {\\color{lightgreen} Z}");
            create_attach_select_x_un_to_op(op, IS_SECOND, 0
                                            , (float)1 / Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");




            op = create_and_attach_one_plot(pr_TRACK,  TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0);
            ds = op->dat[0];
            ds->xd[1] = ds->yd[1] = 1;
            set_ds_source(ds, "Zmag rolling buffer");

            if ((ds = create_and_attach_one_ds(op, TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0)) == NULL)
            {
                return win_printf_OK("I can't create plot !");
            }
            set_ds_source(ds, "Rotation rolling buffer");
            if ((ds = create_and_attach_one_ds(op, TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0)) == NULL)
            {
                return win_printf_OK("I can't create plot !");
            }

            set_ds_source(ds, "Z rolling buffer");

            if ((ds = create_and_attach_one_ds(op, TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0)) == NULL)
            {
                return win_printf_OK("I can't create plot !");
            }

            set_ds_source(ds, "Inductive sensor rolling buffer");

            if ((ds = create_and_attach_one_ds(op, TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0)) == NULL)
            {
                return win_printf_OK("I can't create plot !");
            }

            set_ds_source(ds, "Limit switch rolling buffer");
            op->op_idle_action = magnets_rolling_buffer_idle_action;
            op->op_post_display = general_op_post_display;
            op->user_ispare[ISPARE_BEAD_NB] = 0;
            set_op_filename(op, "Magnets.gr");
            op->width = rolling_plot_width * (float)SCREEN_W / SCREEN_H;
            set_plot_x_title(op, "Time");
            set_plot_y_title(op, "{\\color{yellow} Zmag} {\\color{lightgreen} Rotation} Z bead");
            create_attach_select_x_un_to_op(op, IS_SECOND, 0
                                            , (float)1 / Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");
            op = create_and_attach_one_plot(pr_TRACK,  TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0);
            ds = op->dat[0];
            ds->xd[1] = ds->yd[1] = 1;
            set_ds_source(ds, "Zmag rolling buffer");

            if ((ds = create_and_attach_one_ds(op, TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0)) == NULL)
            {
                return win_printf_OK("I can't create plot !");
            }
            set_ds_source(ds, "Rotation rolling buffer");
            if ((ds = create_and_attach_one_ds(op, TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0)) == NULL)
            {
                return win_printf_OK("I can't create plot !");
            }
            set_ds_source(ds, "Beads count");
            op->op_idle_action = bead_count_rolling_buffer_idle_action;
            op->op_post_display = general_op_post_display;
            op->user_ispare[ISPARE_BEAD_NB] = 0;
            set_op_filename(op, "BeadCount.gr");
            op->width = rolling_plot_width * (float)SCREEN_W / SCREEN_H;
            set_plot_x_title(op, "Time");
            set_plot_y_title(op, "{\\color{yellow} Zmag} {\\color{lightgreen} Rotation} # of beads");
            create_attach_select_x_un_to_op(op, IS_SECOND, 0
                                            , (float)1 / Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");
            //fifth plot
            op = create_and_attach_one_plot(pr_TRACK,  TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0);
            ds = op->dat[0];
            ds->xd[1] = ds->yd[1] = 1;
            set_ds_source(ds, "X VS Y rolling buffer");
            op->op_idle_action = x_y_rolling_buffer_idle_action;
            op->op_post_display = general_op_post_display;
            op->user_ispare[ISPARE_BEAD_NB] = 0;
            uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
            uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
            set_op_filename(op, "X VS Y.gr");
            op->width = rolling_plot_width * (float)SCREEN_W / SCREEN_H;
            set_plot_x_title(op, "X");
            set_plot_y_title(op, "Y");
            op = create_and_attach_one_plot(pr_TRACK,  TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0);
            ds = op->dat[0];
            ds->xd[1] = ds->yd[1] = 1;
            set_ds_source(ds, "X VS Z rolling buffer");
            op->op_idle_action = x_z_rolling_buffer_idle_action;
            op->op_post_display = general_op_post_display;
            op->user_ispare[ISPARE_BEAD_NB] = 0;
            uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
            uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_Y_UNIT_SET);
            set_op_filename(op, "X VS Z.gr");
            op->width = rolling_plot_width * (float)SCREEN_W / SCREEN_H;
            set_plot_x_title(op, "X");
            set_plot_y_title(op, "Z");
            op = create_and_attach_one_plot(pr_TRACK,  TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0);
            ds = op->dat[0];
            ds->xd[1] = ds->yd[1] = 1;
            set_ds_source(ds, "Y VS Z rolling buffer");
            op->op_idle_action = y_z_rolling_buffer_idle_action;
            op->op_post_display = general_op_post_display;
            op->user_ispare[ISPARE_BEAD_NB] = 0;
            uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_X_UNIT_SET);
            uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
            set_op_filename(op, "Y VS Z.gr");
            op->width = rolling_plot_width * (float)SCREEN_W / SCREEN_H;
            set_plot_x_title(op, "Y");
            set_plot_y_title(op, "Z");
            op = create_and_attach_one_plot(pr_TRACK, MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0);
            ds = op->dat[0];
            ds->xd[1] = ds->yd[1] = 1;
            set_ds_source(ds, "Instantaneous orthoradial profile");
            op->op_idle_action = orthoradial_profile_rolling_buffer_idle_action;
            op->op_post_display = general_op_post_display;
            op->user_ispare[ISPARE_BEAD_NB] = 0;
            set_op_filename(op, "OrthoProfile.gr");
            op->width = rolling_plot_width * (float)SCREEN_W / SCREEN_H;
            uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_X_UNIT_SET);
            set_plot_x_title(op, "Angular position");
            set_plot_y_title(op, "Light intensity");
            op = create_and_attach_one_plot(pr_TRACK, TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0);
            ds = op->dat[0];
            ds->xd[1] = ds->yd[1] = 1;
            set_ds_source(ds, "Angle rolling buffer");

            if ((ds = create_and_attach_one_ds(op, TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0)) == NULL)
            {
                return win_printf_OK("I can't create plot !");
            }

            set_ds_source(ds, "Rotation rolling buffer");
            op->op_idle_action = angle_profile_rolling_buffer_idle_action;
            op->op_post_display = general_op_post_display;
            op->user_ispare[ISPARE_BEAD_NB] = 0;
            set_op_filename(op, "Angle.gr");
            op->width = rolling_plot_width * (float)SCREEN_W / SCREEN_H;
            set_plot_x_title(op, "Time");
            set_plot_y_title(op, "{\\color{yellow} Angle} {\\color{lightgreen} Rotation}");
            op = create_and_attach_one_plot(pr_TRACK, TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0);
            ds = op->dat[0];
            ds->xd[1] = ds->yd[1] = 1;
            set_ds_source(ds, "spot light rolling buffer");
            op->op_idle_action = spot_light_rolling_buffer_idle_action;
            op->op_post_display = general_op_post_display;
            op->user_ispare[ISPARE_BEAD_NB] = 0;
            set_op_filename(op, "Spot_light.gr");
            op->width = rolling_plot_width * (float)SCREEN_W / SCREEN_H;
            set_plot_x_title(op, "Time");
            set_plot_y_title(op, "Light intensity");
            pr_TRACK->cur_op = 0;
            pr_TRACK->one_p = pr_TRACK->o_p[0];
            zobj = prev_focus; //read_Z_value_accurate_in_micron();
            //win_printf("initialisation 1 zobj = %g",zobj);
            //zobj = read_Z_value_accurate_in_micron();
            //win_printf("initialisation 2 zobj = %g",zobj);
            //zobj = read_Z_value_accurate_in_micron();
            //win_printf("initialisation 3 zobj = %g",zobj);

            for (i = 0, zobj = prev_focus; i < TRACKING_BUFFER_SIZE; i++) // read_Z_value_accurate_in_micron()
            {
                //if (i == 0) win_printf("initialisation zobj = %g",zobj);
                track_info->obj_pos_cmd[i] = track_info->obj_pos[i] = zobj;
                track_info->zmag_cmd[i] = track_info->zmag[i] = n_magnet_z; //what_is_present_z_mag_value()
                track_info->rot_mag_cmd[i] = track_info->rot_mag[i] = n_rota; //what_is_present_rot_value();
            }

            //win_printf("init cmd zmag %g rot %g",n_magnet_z,n_rota);
            track_info->m_i = track_info->n_i = TRACKING_BUFFER_SIZE;
            //track_info->c_i = 0;
            //LOCK_FUNCTION(track_in_x_y_IFC);
            //LOCK_FUNCTION(track_in_x_y_timer);
            //LOCK_VARIABLE(track_info);
        }

        /*
           else
           {
           if (track_info->cl != cl || track_info->cw != cw)
           {
           win_printf("Your arms do not have the right size!\n");
           if (oi) free_one_image(oi);
           return D_O_K;
           }
           }
           */
#if !defined SDI_VERSION && !defined SDI_VERSION_2
        do_add_bead_in_x_y(cl, cw,
			   (calib_im_nx_equal_cross_arm_length) ? cl : calib_im_nx_set,
			   dy_fringes, dy_fringes_tolerence, fringes_ny,
			   xc, yc,0,0,0,0,0,0,0,0,0,0,0,0);
#endif
    }

    ask_to_update_menu();
    return 0;
}


int follow_bead_in_x_y(void)
{
    int mode = 0;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    mode = RETRIEVE_MENU_INDEX;
    return do_follow_bead_in_x_y(mode);
}



int stop_spectrum_profile(void)
{
    return D_O_K;
}

int spectrum_idle_action(O_p *op, DIALOG *d)
{
    int i;
    //static int last_c_i = -1, immax = -1;
    d_s *ds = NULL;
    float  *zt = NULL;
    // b_track *bd;
    (void)d;

    if (track_info == NULL)
    {
        return D_O_K;
    }

    ds = find_source_specific_ds_in_op(op, "Spectrum on CCD camera");

    if (ds == NULL)
    {
        return D_O_K;
    }

    if (IS_DATA_IN_USE(oi_TRACK))
    {
        return 1;
    }

    zt = extract_raw_tilted_profile(oi_TRACK, ds->yd, ds->mx, px0, py0,
                                    px1, py1, &(ds->nx));//passer les parametres

    if (zt != NULL)
    {
        for (i = 0; i < ds->nx && i < ds->mx; i++)
        {
            ds->xd[i] = i;
        }

        ds->yd = zt;
        ds->ny = ds->nx;
    }

    op->need_to_refresh = 1;
    set_plot_title(op, "Spectrum image %d ", track_info->ac_i);
    return refresh_plot(pr_TRACK, UNCHANGED);
}
int spectrum_profile(void)
{
  //int spectrum_idle_action(O_p * op, DIALOG * d);
    int x, y, xw, yw, np, ndi;
    int old_x = 0, old_y, color, x_0, y_0;
    O_i *oi = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    imreg *imr = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    imr = imr_TRACK;
    oi = oi_TRACK;
    np = 1 + (int)sqrt(oi->im.nx * oi->im.nx + oi->im.ny * oi->im.ny);
    op = create_and_attach_one_plot(pr_TRACK,  np, np, 0);
    ds = op->dat[0];
    set_ds_source(ds, "Spectrum on CCD camera");
    color = makecol(255, 64, 64);
    ndi = find_imr_index_in_current_dialog(NULL);

    if (ndi < 0)
    {
        return win_printf_OK("Could not find image dialog");
    }

    while (!(mouse_b & 0x3));

    x_0 = old_x = x = mouse_x;
    y_0 = old_y = y = mouse_y;
    px1 = px0 = x_imr_2_imdata(imr, mouse_x);
    py1 = py0 = y_imr_2_imdata(imr, mouse_y);

    while (mouse_b & 0x3)
    {
        xw = x = mouse_x  - imr->x_off;
        yw = y = imr->y_off - mouse_y;

        if (x  < 0)
        {
            xw = 0;
        }

        if (x > imr->s_nx)
        {
            xw = imr->s_nx;
        }

        if (y  < 0)
        {
            yw = 0;
        }

        if (y > imr->s_ny)
        {
            yw = imr->s_ny;
        }

        if (x != xw || y != yw)
        {
            x = xw;
            y = yw;
            xw += imr->x_off;
            yw = imr->y_off - yw;
            mouse_x = xw;
            mouse_y = yw;
        }

        if (x != old_x || x != old_y)
        {
            px1 = x_imr_2_imdata(imr, mouse_x);
            py1 = y_imr_2_imdata(imr, mouse_y);
            oi->need_to_refresh |= PLOTS_NEED_REFRESH;
            do_one_image(imr);
            oi->need_to_refresh &= BITMAP_NEED_REFRESH;
            write_and_blit_im_box(plt_buffer, active_dialog + ndi, imr);
            draw_bubble(screen, B_CENTER, mouse_x, mouse_y - 20,
                        "px0 %d py0 %d px1 %d py1 %d", px0, py0, px1, py1);
	    for(;screen_acquired;); // we wait for screen_acquired back to 0;
	    screen_acquired = 1;
            acquire_bitmap(screen);
            line(screen, x_0, y_0, mouse_x, mouse_y, color);
            release_bitmap(screen);
	    screen_acquired = 0;
            old_x = x;
            old_y = y;
        }
    }

    op->op_idle_action = spectrum_idle_action;
    set_op_filename(op, "CCDspectrum.gr");
    return D_O_K;
}

int stop_spectrum_idle_action(void)
{
    int n_op;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    n_op = find_op_nb_of_source_specific_ds_in_pr(pr_TRACK, "Spectrum on CCD camera");
    (pr_TRACK->o_p[n_op])->op_idle_action = NULL;
    return D_O_K;
}

int reset_mouse_rect(void)
{
    scare_mouse();
    set_mouse_sprite(NULL);
    set_mouse_sprite_focus(0, 0);
    unscare_mouse();
    return 0;
}

int change_mouse_to_rect(int cl, int cw)
{
    BITMAP *ds_bitmap = NULL;
    int color, col;
    ds_bitmap = create_bitmap(cl + 1, cw + 1);
    col = Lightmagenta;
    color = makecol(EXTRACT_R(col), EXTRACT_G(col), EXTRACT_B(col));
    clear_to_color(ds_bitmap, color);
    color = makecol(255, 0, 0);
    line(ds_bitmap, 0, 0, cl, 0, color);
    line(ds_bitmap, 0, 0, 0, cw, color);
    line(ds_bitmap, 0, cw, cl, cw, color);
    line(ds_bitmap, cl, 0, cl, cw, color);
    scare_mouse();
    set_mouse_sprite(ds_bitmap);
    set_mouse_sprite_focus(cl / 2, cw / 2);
    unscare_mouse();
    return 0;
}

int collect_intensity(imreg *imr, O_i *oi, int n, DIALOG *d, long long t, unsigned long dt, void *p, int n_inarow)
{
    int x_pos_center;
    int y_pos_center;
    int pix_size = 4;
    float average = 0;
    int l, k;
    int *j = NULL;
    (void)imr;
    (void)oi;
    (void)n;
    (void)d;
    (void)t;
    (void)dt;
    (void)p;
    (void)n_inarow;
    j = (int *)bid.next->param;
    pix_size = j[0];
    x_pos_center = j[1];
    y_pos_center = j[2];

    for (k = 0; k < pix_size ; k++)
    {
        for (l = 0; l < pix_size ; l++)
        {
            average += (float)oi_TRACK->im.pixel[y_pos_center + k - (int)(pix_size / 2)].ch[x_pos_center + l - (int)(pix_size / 2)];
        }
    }

    intensity[track_info->c_i % TRACKING_BUFFER_SIZE] = average / pix_size / pix_size;
    return D_O_K;
}
int intensity_vs_time_idle_action(O_p *op, DIALOG *d)
{
    int i;
    d_s *ds = NULL;
    (void)d;

    if (track_info == NULL)
    {
        return D_O_K;
    }

    ds = find_source_specific_ds_in_op(op, "Intensity vs time");

    if (ds == NULL)
    {
        return D_O_K;
    }

    for (i = 0 ; i < TRACKING_BUFFER_SIZE ; i++)
    {
        ds->xd[i] = i;
        ds->yd[i] = intensity[i];
    }

    set_plot_title(op, "\\stack{{Intensity vs time image %d}{x %d y %d}}", track_info->c_i, *((int *)bid.next->param),
                   *((int *)bid.next->param + 1));
    op->need_to_refresh = 1;
    return refresh_plot(pr_TRACK, UNCHANGED);
}


int intensity_vs_time(void)
{
  //int collect_intensity(imreg * imr, O_i * oi, int n, DIALOG * d, long long t, unsigned long dt, void *p, int n_inarow);
  //int intensity_vs_time_idle_action(O_p * op, DIALOG * d);
    O_p *op = NULL;
    d_s *ds = NULL;
    imreg *imr = NULL;
    int pix_size = 4;
    int *position = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    imr = imr_TRACK;
    op = create_and_attach_one_plot(pr_TRACK,  TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0);
    win_scanf("You want to average on how many pixels?%d", &pix_size);
    change_mouse_to_rect(pix_size, pix_size);
    op = create_and_attach_one_plot(pr_TRACK,  TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0);
    ds = op->dat[0];
    set_ds_source(ds, "Intensity vs time");
    set_op_filename(op, "Intensity.gr");

    while (!(mouse_b & 0x3));

    position = (int *)calloc(3, sizeof(int));
    position[0] = pix_size;
    position[1] = x_imr_2_imdata(imr, mouse_x);
    position[2] = y_imr_2_imdata(imr, mouse_y);
    reset_mouse_rect();

    if (bid.timer_do == NULL)
    {
        return win_printf_OK("Bid NULL");
    }

    if (bid.next == NULL)
    {
        bid.next = (Bid *)calloc(1, sizeof(Bid));
        bid.next->param = (void *)position;
        bid.next->timer_do = collect_intensity;
    }

    op->op_idle_action = intensity_vs_time_idle_action;
    return D_O_K;
}


int stop_intensity_vs_time_idle_action(void)
{
    int n_op;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    n_op = find_op_nb_of_source_specific_ds_in_pr(pr_TRACK, "Intensity vs time");
    //bid.next = NULL;
    //bid.next->param = NULL;
    (pr_TRACK->o_p[n_op])->op_idle_action = NULL;
    bid.next = NULL;
    return D_O_K;
}


int    dump_to_error_file_with_date_and_time(const char *fmt, ...)
{
    va_list ap;
    FILE *fp = NULL;
    char s[256];
    struct tm *t = NULL;
    time_t ti;
    char c[2048];
    ti = time(0);
    t = localtime(&ti);
    strftime(s, sizeof(s), "%d/%m/%Y %H:%M:%S", t);
    fp = fopen("pico_err.log", "a");

    if (fp == NULL)
    {
        return 1;
    }

    fprintf(fp, "%s\n", s);
    va_start(ap, fmt);
    vsnprintf(c, sizeof(c), fmt, ap);
    va_end(ap);
    fprintf(fp, "%s\n", c);
    return fclose(fp);
}
int    dump_to_error_file_with_time(const char *fmt, ...)
{
    va_list ap;
    FILE *fp = NULL;
    char s[256];
    struct tm *t = NULL;
    time_t ti;
    char c[2048];
    ti = time(0);
    t = localtime(&ti);
    strftime(s, sizeof(s), "%X", t);
    fp = fopen("pico_err.log", "a");

    if (fp == NULL)
    {
        return 1;
    }

    fprintf(fp, "%s\n", s);
    va_start(ap, fmt);
    vsnprintf(c, sizeof(c), fmt, ap);
    va_end(ap);
    fprintf(fp, "%s\n", c);
    return fclose(fp);
}
int    dump_to_error_file_only(const char *fmt, ...)
{
    va_list ap;
    FILE *fp = NULL;
    char c[2048];
    fp = fopen("pico_err.log", "a");

    if (fp == NULL)
    {
        return 1;
    }

    va_start(ap, fmt);
    vsnprintf(c, sizeof(c), fmt, ap);
    va_end(ap);
    fprintf(fp, "%s\n", c);
    return fclose(fp);
}


#ifdef ENCOURS

HANDLE hReadyEvent = NULL;
HANDLE hFinishEvent = NULL;
BOOL bEnableThread;
// Initialize all handles
HANDLE hPicoComThread = NULL;
HANDLE hPicoComEvent = NULL;
HANDLE hEventFreeze = NULL;

PulseEvent(hPicoComEvent);

//==============================================================////
// Stream Processing Function
//==============================================================////

DWORD WINAPI PicoComProcess(LPVOID lpParam)
{
    Bid *p = NULL;
    uint64_t local_NumberFramesDelivered;
    long long t, dt, lTimeStamp;
    TreatementStruct *pTreatement_Struct = NULL;
    int i, im_n;
    int iWaitResult;
    pTreatement_Struct = (TreatementStruct *) lpParam;

    while (bEnableThread)
    {
        iWaitResult = WaitForSingleObject(hReadyEvent, 60000);

        if (WAIT_OBJECT_0 == iWaitResult)
        {
            local_NumberFramesDelivered = lNumberFramesDelivered;
            p = &bid;

            if (p == NULL)
            {
                return win_printf_OK("MERDE");
            }

            //lNumberFramesDelivered = pTreatement_Struct->lNumberImageDelivered ;
            if (p->first_im == 0)
            {
                p->first_im = (int) lNumberFramesDelivered;
            }

            lTimeStamp = pTreatement_Struct->lTimeStamp;
            // we do stuff here
        }
    }

    return D_O_K;
}


//--------------------------------------------------------------------------------------------------
// Create Stream Thread
//--------------------------------------------------------------------------------------------------
BOOL CreatePicoComThread(void)
{
    DWORD dwThreadId;

    // Is it already created?
    if (hPicoComThread)
    {
        return FALSE;
    }

    // Stream thread event created?
    if (hPicoComEvent == NULL)
    {
        hPicoComEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    }
    else
    {
        ResetEvent(hPicoComEvent);
    }

    // Set the thread execution flag
    bEnableThread = TRUE;
    hPicoComThread = CreateThread(NULL,              // default security attributes
                                  0,                 // use default stack size
                                  (LPTHREAD_START_ROUTINE)PicoComProcess,// thread function
                                  NULL,              // argument to thread function
                                  0,                 // use default creation flags
                                  &dwThreadId);      // returns the thread identifier

    if (hPicoComThread == NULL)
    {
        win_printf_OK("No thread created");
        return FALSE;
    }

    SetThreadPriority(hPicoComThread,  THREAD_PRIORITY_TIME_CRITICAL);
    return TRUE;
}


#endif



#endif
