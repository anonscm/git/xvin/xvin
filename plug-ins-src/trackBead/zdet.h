
#ifndef _ZDET_H_
#define _ZDET_H_



typedef struct _zdet_avg
{
  float xbead, ybead, zbead;               // the bead mean position
  float time_avg;            // in seconds
  int nb_frames_in_avg;      // in frames
  int zbead_valid;           // the flag indicating if the z bead position is valid
  int rec_start;             // record image index where averaging starts
  int rec_end;              // record image index where averaging ends
  int ending_im;
  int n_avg;
  int n_rec;
  float zobj;
  int profile_invalid;         // check if profile was ok
  int i_bead;
}zdet_avg;





typedef struct _zdet_stay_in_range
{
  float z_threshold;        // averaging stops when zbead becomes crosses z_threshold
  int direction;            // 1 if zbeag becomes > threshold, -1 otherwise
  float xbead, ybead, zbead;               // the bead mean position
  float time_avg;            // in seconds
  int nb_frames_in_avg;      // in frames
  float max_duration;        // in seconds
  int nb_frames_max;      // in frames
  int zbead_valid;           // the flag indicating if the z bead position is valid
  int rec_start;             // record image index where averaging starts
  int rec_end;              // record image index where averaging ends
  int ending_im;
  int n_avg;
  int n_rec;
  float zobj;
  int profile_invalid;         // check if profile was ok
  int status;     // progression flag
  int n_bead;
}zdet_stay_in_range;



typedef struct _zdet_param
{
   int nf, nstep,  dead, nper, *im;
   float rot_start, rot_step, zmag, force, z_cor, zmag_finish, *rot;
  int fir;
} zdet_param;


# ifndef _ZDET_C_

PXV_VAR(g_record *, zdet_r);

//PXV_VAR(char, sample[]);
PXV_VAR(int, n_zdet);
//PXV_VAR(int, scope_buf_size);
PXV_VAR(int, fir);
PXV_VAR(int, zmag_or_f);
PXV_VAR(float, min_scale_zmag);
PXV_VAR(float, min_scale_rot);
PXV_VAR(float, min_scale_force);
PXV_VAR(float, min_scale_z);

PXV_FUNC(MENU *, zdet_menu, (void));
PXV_FUNC(MENU *, zdet_plot_menu, (void));
PXV_FUNC(int, draw_XYZ_trajectories, (void));
PXV_FUNC(int, zdet_op_post_display, (struct one_plot *op, DIALOG *d));
PXV_FUNC(int, zdet_op_idle_action, (O_p *op, DIALOG *d));
PXV_FUNC(int, zdet_job_z_fil, (int im, struct future_job *job));
PXV_FUNC(int, stop_zdet, (void));
PXV_FUNC(int, record_zdet, (void));

# else
g_record *zdet_r = NULL;
//int scope_buf_size = 4096;
int fir = 16;
int zmag_or_f = 0;
float min_scale_zmag = 1;
float min_scale_rot = 1;
float min_scale_force = 0.1;
float min_scale_z = 0.5;


//char sample[] = "test"; // _PXV_DLL
int n_zdet = 0;// _PXV_DLL

# endif

# endif
