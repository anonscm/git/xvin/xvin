/*
 *    Plug-in program for image treatement in Xvin.
 *
 *    V. Croquette
 */
#ifndef _TRACKBEAD_C_
#define _TRACKBEAD_C_




#ifndef XV_WIN32
# include "config.h"
#endif

# include "xvin.h"
//# include "System.h"
#include "allegro.h"

#ifdef XV_WIN32
# include "winalleg.h"
#include <windows.h>
#include <tlhelp32.h>
#endif
# include "xv_main.h"
/* If you include other plug-ins header do it here*/

//# include "../CVB/XVCVB.h"
/* But not below this define */

# define BUILDING_PLUGINS_DLL
# include "../cfg_file/Pico_cfg.h"
# include "../cfg_file/microscope.h"


#ifdef XV_UNIX     //DB
#include <dirent.h>
#endif



XV_FUNC(int, do_load_im, (void));

#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "../fft2d/fftbtl32n.h"
# include "fillibbt.h"
# include "trackBead.h"
# include "track_util.h"
# include "focus.h"
# include "action.h"
# include "magnetscontrol.h"
# include "brown_util.h"
# include "save_movie_util.h"
# include "calibration.h"
# include "record.h"
# include "zdet.h"
# include "hat.h"
# include "scan_zmag.h"
# include "force.h"
# include "help_mn.h"
# include "file_picker_gui.h"
# ifdef XVIN_STATICLINK
# include "thermal_z/thermal_z.h"
# endif
# ifdef FLOW_CONTROL
# include "thorlabs/apt_flow_control.h"
# endif
# ifdef FLOW_CONTROL_DUMMY
# include "flow_control.h"
# endif
#include "timer_thread.h"
#ifdef APT_MOT_TRANSLATION
#include "translation_control.h"
#endif
#ifdef OPENCL
#define LTPV_NUMTHREADS 3
#include "ltpv.h"
#include "gpu/track_beads.h"
#endif

#ifdef XV_OPENCV
#include "count_beads_cv.hh"
#endif

# ifdef SDI_VERSION_2
# include "SDI_2_functions.h"
#include "SDI_2_bead_menu.h"
#endif
#ifdef RAWHID_V2
#include "rawhid_general/rawhid_general.h"
#endif

#ifdef USE_MLFI
#include "MLFI/mlfi.h"
#endif


#include "gui.h"

#include <locale.h>
#include <pthread.h>

PXV_FUNC(int, show_bead_cross, (BITMAP *imb, imreg *imr, DIALOG *d));
PXV_FUNC(int, record_calibration, (void));
PXV_FUNC(int, ramp_objective, (void));
PXV_FUNC(int, get_present_image_nb, (void));
PXV_FUNC(int, record_x_y_trajectories,(void));
PXV_FUNC(int, record_x_y_angle,(void));
PXV_FUNC(int, move_bead_cross, (BITMAP *imb, imreg *imr, DIALOG *d, int x0, int y0));
PXV_FUNC(int, grab_profile_rolling_buffer, (void));
PXV_FUNC(int, grab_orthoradial_profile_rolling_buffer, (void));

PXV_FUNC(int, record_scan,(void));
PXV_FUNC(int, record_hat,(void));
PXV_FUNC(int, record_force_curve, (void));
PXV_FUNC(int, record_auto_force_curve, (void));

PXV_FUNC(int, record_mod_rot_curve, (void));
PXV_FUNC(MENU* , prepare_calib_menu, (b_track *bt));
PXV_FUNC(int, init_pico_serial, (void));
PXV_FUNC(O_i*, do_reload_generic_calibration, (int n));
PXV_FUNC(int, cardinal_main, (int argc, char **argv));
PXV_FUNC(int, wlc_main, (int argc, char **argv));
//PXV_FUNC(int, polymerWLC_main, (int argc, char **argv));
PXV_FUNC(int, stat_main, (int argc, char **argv));

# include "pico_serial/pico_serial.h"

FILE *fp_log = NULL;

# ifdef TRACK_WIN_THREAD
HANDLE track_hThread = NULL;
DWORD track_dwThreadId;
# else
pthread_t track_thread;
# endif
int   trackBead_init = 0;

int mouse_selected_bead = -1;

#ifdef SDI_VERSION
//sdi stuff
extern int tracking_type;
extern sdi_v2_parameter *sdi_g_param;
extern sdi_v2_fov *sdi_g_fov;
#endif





#ifdef XV_WIN32
/*!
\brief Check if a process is running
\param [in] processName Name of process to check if is running
\returns \c True if the process is running, or \c False if the process is not running
*/
int IsProcessRunning(const char *processName)
{
    int ret = 0;
    PROCESSENTRY32 entry;
    entry.dwSize = sizeof(PROCESSENTRY32);

    HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

    if (Process32First(snapshot, &entry))
        while (Process32Next(snapshot, &entry))
	  {
	    //win_printf("found %s",entry.szExeFile);
            if (strcmp(entry.szExeFile, processName) == 0)
                ret++;
	  }
    CloseHandle(snapshot);
    return ret;
}


# endif

#ifdef XV_UNIX

int proc_find(const char* name)
{
    DIR* dir;
    struct dirent* ent;
    char* endptr;
    char buf[512];
    int found = 0;

    if (!(dir = opendir("/proc"))) {
        perror("can't open /proc");
        return -1;
    }

    while((ent = readdir(dir)) != NULL) {
        /* if endptr is not a null character, the directory is not
         * entirely numeric, so ignore it */
        long lpid = strtol(ent->d_name, &endptr, 10);
        if (*endptr != '\0') {
            continue;
        }

        /* try to open the cmdline file */
        snprintf(buf, sizeof(buf), "/proc/%ld/cmdline", lpid);
        FILE* fp = fopen(buf, "r");

        if (fp) {
            if (fgets(buf, sizeof(buf), fp) != NULL) {
                /* check the first token in the file, the program name */
                char* first = strtok(buf, " ");
		//win_printf("stuff %s",buf);
                if (strstr(first, name) != NULL) {
		  //fclose(fp);
                  //  closedir(dir);
		    found++;
                }
            }
            fclose(fp);
        }

    }

    closedir(dir);
    return found;
}

# endif


int d_draw_TRACK_Im_proc(int msg, DIALOG *d, int c)
{
    return d_draw_Im_proc(msg, d, c);
}

int track_oi_mouse_action(O_i *oi, int x0, int y0, int mode, DIALOG *d)
{
    // we put in this routine all the screen display stuff
    imreg *imr = NULL;
    unsigned long t0 = 0;
    int cw, pxl, pyl, c_b;
    b_track *bt = NULL;
    int (*post_display)(struct one_image *oi, DIALOG *d);
    static float zmag_asked = 0;
    //  display_title_message("cou cou");
    //return D_O_K;
    (void)x0;
    (void)y0;
    (void)mode;
    if (d->dp == NULL)    return 1;
    imr = (imreg*)d->dp;        /* the image region is here */
    if (imr->one_i == NULL || imr->n_oi == 0)        return 1;


    if(mouse_b == 2)
    {
        //for (track_info->c_b = 0; track_info->c_b < track_info->n_b; track_info->c_b++)
        for (c_b = 0; c_b < track_info->n_b; c_b++)
        {
            //cl = track_info->cl;                   // cross arm length
            cw = track_info->cw;                   // cross arm width
            bt = track_info->bd[c_b];
            //cl = bt->cl;
            cw = bt->cw;

            pxl = (int)(0.5+x_imr_2_imdata(imr, mouse_x));
            pyl = (int)(0.5+y_imr_2_imdata(imr, mouse_y));

            if ((pxl > bt->xc - cw)
                && (pxl < bt->xc + cw)
                && (pyl > bt->yc - cw)
                && (pyl < bt->yc + cw))
            {
                mouse_selected_bead = c_b;
                break;
            }
        }
        if (c_b < track_info->n_b)
        {
          #ifdef SDI_VERSION_2
          do_menu(prepare_SDI_2_bead_menu(bt),mouse_x,mouse_y);
          #else
          do_menu(prepare_calib_menu(bt),mouse_x,mouse_y);
          #endif
        }
        else if (bead_search == 1)
        {
            if (zmag_asked != bead_search_high)
            {
                immediate_next_available_action(MV_ZMAG_ABS, bead_search_high);
                zmag_asked = bead_search_high;
            }
        }
        else
        {
          #ifdef SDI_VERSION_2
          do_menu(prepare_SDI_2_bead_menu(bt),mouse_x,mouse_y);
          #else
          do_menu(prepare_calib_menu(bt),mouse_x,mouse_y);
          #endif
        }

    }

    while(mouse_b == 1)
    {
        if ((bead_search == 1) && (zmag_asked != bead_search_low))
        {
            immediate_next_available_action(MV_ZMAG_ABS, bead_search_low);
            zmag_asked = bead_search_low;
        }
        else if (oi->need_to_refresh & BITMAP_NEED_REFRESH)
        {
            t0 = my_uclock();
            post_display = oi->oi_post_display;
            oi->oi_post_display = NULL;
            display_image_stuff_16M(imr,d);
            oi->need_to_refresh |= INTERNAL_BITMAP_NEED_REFRESH;
            t0 = my_uclock() - t0;
            write_and_blit_im_box( plt_buffer, d, imr);
            oi->oi_post_display = post_display;
            move_bead_cross(screen, imr, d, mouse_x, mouse_y);

        }
        if (general_idle_action) general_idle_action(d);
    }
    for (c_b = 0; c_b < track_info->n_b; c_b++)
        track_info->bd[c_b]->mouse_draged = 0;
    return 0;
}




int track_before_menu(int msg, DIALOG *d, int c)
{
    /* this routine switches the display flag so that no magic color is drawn
       when menu are activated */
    (void)d;
    (void)c;
    if (msg == MSG_GOTMOUSE)
    {
        do_refresh_overlay = 0;
        //display_title_message("menu got mouse %d",n_t++);
    }
    return 0;
}
int track_after_menu(int msg, DIALOG *d, int c)
{
    (void)d;
    (void)c;
    (void)msg;
    //display_title_message("menu exit mouse");
    return 0;
}

int track_oi_got_mouse(struct  one_image *oi, int xm_s, int ym_s, int mode)
{
    /* this routine switches the display flag so that the magic color is drawn again
       after menu are activated */
  (void)oi;
  (void)xm_s;
  (void)ym_s;
  (void)mode;
  do_refresh_overlay = 1;
    //display_title_message("Image got mouse");
  return 0;
}


int track_oi_post_display(struct one_image *oi, DIALOG *d)
{
    imreg *imr = NULL;

  (void)oi;
    if (d == NULL || d->dp == NULL || d->proc == NULL)    return 1;

    imr = (imreg*)d->dp;
    show_bead_cross(plt_buffer, imr, d);
    return 0;
}

int track_oi_idle_action(struct  one_image *oi, DIALOG *d)
{
    // we put in this routine all the screen display stuff
    imreg *imr = NULL;
    BITMAP *imb = NULL;
    unsigned long t0 = 0;
    //float zo, zm;
    //struct box *b = NULL;

    if (d->dp == NULL)    return 1;
    imr = (imreg*)d->dp;        /* the image region is here */
    if (imr->one_i == NULL || imr->n_oi == 0)        return 1;

    t0 = my_uclock();
    if ((IS_DATA_IN_USE(oi) == 0) && (oi->need_to_refresh & BITMAP_NEED_REFRESH))
    {
        display_image_stuff_16M(imr,d);
        oi->need_to_refresh |= INTERNAL_BITMAP_NEED_REFRESH;
    }
    imb = (BITMAP*)oi->bmp.stuff;
    imr_TRACK_redraw_background = show_bead_cross(imb, imr, d);


    if (generate_imr_TRACK_title && imr_TRACK_title_change_asked > imr_TRACK_title_displayed)
    {
        //set_im_title(oi_TRACK, generate_imr_TRACK_title());
        oi_TRACK->title = generate_imr_TRACK_title();
        imr_TRACK_title_displayed = imr_TRACK_title_change_asked;
        imr_TRACK_redraw_background = 1;
        ttf_enable = 0;
        //b = &imr_TRACK->stack;
        do_one_image (imr_TRACK);
        oi_TRACK->need_to_refresh &= BITMAP_NEED_REFRESH;
        adj_image_pos(imr_TRACK, d_TRACK);
        restore_image_parameter(imr_TRACK,  d_TRACK);
    }


    if (imr_TRACK_redraw_background)  // slow redraw
    {
        write_and_blit_im_box( plt_buffer, d, imr);
        //show_bead_cross(plt_buffer, imr, d);
        t0 = my_uclock() - t0;
    }
    else  if (oi->need_to_refresh & INTERNAL_BITMAP_NEED_REFRESH)
    {
        //screen_used = 1;
        SET_BITMAP_IN_USE(oi);
	for(;screen_acquired;); // we wait for screen_acquired back to 0;
	screen_acquired = 1;
        acquire_bitmap(screen);
        blit(imb,screen,0,0,imr->x_off //+ d->x
             , imr->y_off - imb->h + d->y, imb->w, imb->h);
        release_bitmap(screen);
	screen_acquired = 0;
        //screen_used = 0;
        oi->need_to_refresh &= ~INTERNAL_BITMAP_NEED_REFRESH;
        t0 = my_uclock() - t0;
        SET_BITMAP_NO_MORE_IN_USE(oi);
    }
    return 0;
}

void stop_source_thread(void)
{
    time_t ti;

    switch_project_to_this_imreg(imr_TRACK);
    psleep(100);
    freeze_source();
    psleep(500);
    go_track = TRACK_STOP;
    //win_printf("Stopping thread");
    broadcast_dialog_message(MSG_DRAW,0); // needed to prevent freezing dialog!
    if (interfaces_working == 2)
    {
        time(&ti);
        Open_Pico_config_file();
        set_config_int("EXPERIMENT","ended",(int)ti);
        Close_Pico_config_file();
        write_Pico_config_file();
        //win_printf("Stopping thread end %d ",(int)ti);
    }
    shut_down_magnets();
    shut_down_focus();
}


int source_end_action(DIALOG *d)
{
  (void)d;
    stop_source_thread();
    return 0;
}

int find_next_available_job_spot(void)
{
    int i;

    if (job_pending == NULL) return -1;
    // we look for used job to recycle
    for (i = 0; i < n_job; i++)
        if (job_pending[i].in_progress == 0) break;
    if (i == n_job)
        n_job = (n_job < m_job) ? n_job + 1 : n_job;
    if (i >= m_job) return -2;                 // the buffer is full
    return i;
}


f_job* find_job_associated_to_plot(O_p *op)
{
    int i;

    if (job_pending == NULL) return NULL;
    // we look for used job to recycle
    for (i = 0; i < n_job; i++)
    {
        if (job_pending[i].in_progress == 0) continue;
        if (job_pending[i].op == op) return job_pending + i;
    }
    return NULL;
}

f_job* find_job_associated_to_image(O_i *oi)
{
    int i;

    if (job_pending == NULL) return NULL;
    // we look for used job to recycle
    for (i = 0; i < n_job; i++)
    {
        if (job_pending[i].in_progress == 0) continue;
        if (job_pending[i].oi == oi) return job_pending + i;
    }
    return NULL;
}


int fill_next_available_job_spot(int in_progress, int im, int type, int bead_nb,
                                 imreg *imr, O_i *oi, pltreg *pr,
                                 O_p *op, void* more_data,
                                 int (*job_to_do)(int im, struct future_job *job)
                                 ,int local)
{
    int i;
    i = find_next_available_job_spot();
    if (i < 0) return -1;
    job_pending[i].in_progress = in_progress;
    job_pending[i].imi = im;
    job_pending[i].last_imi = im - 1;
    job_pending[i].type = type;
    job_pending[i].bead_nb = bead_nb;
    job_pending[i].imr = imr;
    job_pending[i].oi = oi;
    job_pending[i].pr = pr;
    job_pending[i].op = op;
    job_pending[i].more_data = more_data;
    job_pending[i].job_to_do = job_to_do;
    job_pending[i].local = local;
    return i;
}

//tv : for one type of job, checks whether there is one active instance in job_pending : can be used to avoid launching mutliple time the same jobs
int check_active_job_per_type(int type)
{
  if (job_pending == NULL) return 0;
  int i=0;
  bool found_one_active=false;
  while (i < n_job && found_one_active==false)
  {
      if (job_pending[i].type == type && job_pending[i].in_progress != 0) found_one_active=true;
      i++;
  }
  return (int)found_one_active;
}


int fill_local_job_value(int jobid, int local)
{
    if (jobid < 0 || jobid >= n_job) return  1;
    job_pending[jobid].local = local;
    return 0;
}


int find_next_job(int imi)
{
    int i, j;

    if (job_pending == NULL) return -1;
    for (i = 0, j = -1; i < n_job; i++)
    {
        if (job_pending[i].in_progress == 0) continue;
        else j = i;
        if (job_pending[i].last_imi < imi)
        {
            if (job_pending[i].imi < 0) break;
            if (job_pending[i].imi < imi) break;
        }
    }
    if (i < n_job) return i;
    else  n_job = j+1;
    return -2;
}

int find_remaining_job(int imi)
{
    int i, j;
    (void)imi;
    if (job_pending == NULL) return -1;
    for (i = 0, j = 0; i < n_job; i++)
        j += (job_pending[i].in_progress) ? 1 : 0;
    return j;
}





int source_idle_action(DIALOG *d)
{
    int i = 0, im;
    static float last_rot_asked = 0;
    static float last_focus_asked = 0;
    static float last_zmag_asked = 0;

# ifdef XVIN_STATICLINK

    static float prev_zmag_1 = 0;
    static float prev_mag_rot_1 = 0;
    static float prev_focus_1 = 0;
    static float prev_T0_1 = 0;
    static float prev_load_1 = 0;
    static float prev_freq_1 = 0;
#ifdef RAWHID_V2
    static float prev_x_1 = 0;
    static float prev_y_1 = 0;
#endif
    (void)d;
    if (track_info != NULL && track_info->ac_i > 10)
    {
        if (prev_mag_rot_1 != prev_mag_rot)
            change_rotation_string(prev_mag_rot_1 = prev_mag_rot);
        if (prev_zmag_1 != prev_zmag)
        {
            change_zmag_string(prev_zmag);
            change_force_string(zmag_to_force(0, prev_zmag));
            //my_set_window_title("from source idle %f %f",prev_zmag_1,prev_zmag); //TV debug
            prev_zmag_1 = prev_zmag;
#ifdef RAWHID_V2 //TV : in case of rawhidv2 we are listening to magnet position very fast then it causes config file crashes at some time when too often called
            if ( fabs(prev_zmag_1 - prev_zmag)> 0.01 ) write_Pico_config_file(); //10µm arbitrary (>precision of sensor)
#else
            //write_Pico_config_file();
#endif
        }
        if (prev_focus_1 != prev_focus) change_focus_string(prev_focus_1 = prev_focus);
        if (prev_T0_1 != track_info->T0) change_T0_string(prev_T0_1 = track_info->T0);
        if (prev_load_1 != prev_load) change_Load_string(prev_load_1 = prev_load);
        if (prev_freq_1 != prev_freq) change_Freq_string(prev_freq_1 = prev_freq);
        change_zmag_label_string(NULL,0,0,1);
        change_focus_label_string(NULL,0,0,1);
        change_param_label_string(NULL,0,0,1);
#ifdef RAWHID_V2
        if (prev_x_1 != prev_x) change_X_string(prev_x_1 = prev_x);
        if (prev_y_1 != prev_y) change_Y_string(prev_y_1 = prev_y);
        //tmag to do
#endif
    }


# endif

    if (last_focus_asked != last_focus_target)
    {
        last_focus_asked = last_focus_target;
        /*
           Open_Pico_config_file();
           set_config_float("STATE", "ObjZ", last_focus_asked);
           Close_Pico_config_file();
           */
    }
    if (last_zmag_asked != n_magnet_z)
    {
        last_zmag_asked = n_magnet_z;
        /*
           Open_Pico_config_file();
           set_config_float("STATE", "MagZ", last_zmag_asked);
           Close_Pico_config_file();
           */
    }
    if (last_rot_asked != n_rota)
    {
        last_rot_asked = n_rota;
        /*
           Open_Pico_config_file();
           set_config_float("STATE", "MagRot", n_rota);
           Close_Pico_config_file();
           */
    }


    im = get_present_image_nb();
    if (last_im_who_asked_menu_update > last_im_where_menu_was_updated)
    {
        do_update_menu();
        last_im_where_menu_was_updated = im;
    }
    while ((i = find_next_job(im)) >= 0)
    {
        if (job_pending[i].job_to_do != NULL)
            job_pending[i].job_to_do(im, job_pending + i);
        job_pending[i].last_imi = im;
    }
    return 0;
}



// function executed in the tracking thread
int create_tracking_thread(tid *dtidl)
{
# ifdef TRACK_WIN_THREAD
    track_hThread = CreateThread(
                                 NULL,              // default security attributes
                                 0,                 // use default stack size
                                 TrackingThreadProc,// thread function
                                 (void*)dtidl,       // argument to thread function
                                 0,                 // use default creation flags
                                 &track_dwThreadId);      // returns the thread identifier

    if (track_hThread == NULL) 	win_printf_OK("No thread created");
    SetThreadPriority(track_hThread,  THREAD_PRIORITY_NORMAL);
# else
    if (pthread_create(
                       &track_thread,              // threadId
                       NULL,                 // default security attributes
                       TrackingThreadProc,// thread function
                       (void *) dtidl)!= 0)    // argument to thread function

    {
        win_printf_OK("No thread created");
    }
# endif
    return 0;
}


int display_imr_and_pr(void)
{
    int i;
    DIALOG *di = NULL, *dip = NULL, *dim = NULL, *dmn = NULL;
    //int h;

    if(updating_menu_state != 0)	return D_O_K;


    if (pr_TRACK == NULL || imr_TRACK == NULL) return D_O_K;

    i = retrieve_index_of_menu_from_dialog();
    if (i < 0)     win_printf_OK("Cannot find menu dialog");
    di = the_dialog + i;

    i = find_imr_index_in_current_dialog(the_dialog);
    if (i >= 0)   dim = the_dialog + i;
    else 	        win_printf_OK("Cannot find Im dialog");
    set_dialog_size_and_color(dim, SCREEN_W/2, 38, SCREEN_W/2-1, SCREEN_H-38, 255, 0);
    imr_and_pr = 1;

    //broadcast_dialog_message(MSG_DRAW,0);

    remove_all_keyboard_proc();

    dmn = attach_new_item_to_dialog(d_menu_proc, 0, 0, 0, 0);
    if (dmn == NULL)    { allegro_message("dialog pb") ;return 1;}
    set_dialog_size_and_color(dmn, 1200, 0, 0, 19, 0, 32);
    set_dialog_properties(dmn, plot_menu, NULL, NULL);

    dip = attach_new_plot_region_to_dialog( 0, 0, 0, 0);
    if (dip == NULL)    { allegro_message("dialog pb") ;return 1;}
    set_dialog_size_and_color(dip, 0, 38, SCREEN_W/2-1, SCREEN_H-38, 255, 0);
    //dip->dp = pr_TRACK;
    dip->dp = (void*)imr_TRACK;
    dip->proc = d_draw_Im_proc;
    dip->fg = makecol(0, 0, 0);
    dip->bg = makecol(128, 128, 128);

    set_dialog_size_and_color(di, 0, 0, 500, 19, 0, 0);



    /*
       if (dim != NULL)
       {
       re_attach_plot_region_to_dialog(dim, pr_TRACK);
       set_dialog_size_and_color(dim, 0, 19, SCREEN_W/2, di->h, 255, 0);
       }
       */


    scan_and_update((MENU*)di->dp);
    //scan_and_update((MENU*)dmn->dp);
    switch_project_to_this_pltreg(pr_TRACK);

    //broadcast_dialog_message(MSG_DRAW,0);
    broadcast_dialog_message(MSG_DRAW,0);
    //win_printf("dONE");
    //cardinal_main(0,NULL);

    return D_REDRAWME;
}




int check_remaining_job(void)
{

    if(updating_menu_state != 0)	return D_O_K;

    return win_printf_OK("number of remaining jobs %d",find_remaining_job(get_present_image_nb()));
}

int do_bug(void)
{

    if(updating_menu_state != 0)	return D_O_K;
    xvin_error(Out_Of_Memory);
    return D_O_K;
}


int add_stuff_in_html_log(void)
{
    int i;
    char stuff[2048] = {0};

    if(updating_menu_state != 0)	return D_O_K;
    stuff[0] = 0;
    i = win_scanf("Define the comment that you want to add in your html log file:\n%ls",stuff);
    if (i == WIN_CANCEL)    return D_O_K;
    dump_to_html_log_file_with_date_and_time(current_write_open_picofile,"%s",stuff);
    return D_O_K;
}


int add_rincing_in_html_log(void)
{
    if(updating_menu_state != 0)
    {
        if (manual_rincing == 1)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;
        return D_O_K;
    }
    manual_rincing = 1;
    dump_to_html_log_file_with_date_and_time(current_write_open_picofile,"Sample rincing");
    //win_printf("Added Rincing sample");
    ask_to_update_menu();
    return D_O_K;
}

int add_stop_rincing_in_html_log(void)
{
    if(updating_menu_state != 0)
    {
        if (manual_rincing == 0)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;
        return D_O_K;
    }
    manual_rincing = 0;
    dump_to_html_log_file_with_date_and_time(current_write_open_picofile,"Rincing sample stopped");
    //win_printf("Added Rincing sample stopped");
    ask_to_update_menu();
    return D_O_K;
}

int add_enzyme_injection_in_html_log(void)
{
  int i, e;
    if(updating_menu_state != 0)
    {
        if (manual_enzyme_injection == 1)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;
        return D_O_K;
    }
  e = enzyme_nb;
  i = win_scanf("Enzyme nb %4d",&e);
  if (i == WIN_CANCEL) return D_O_K;
  manual_enzyme_injection = 1;
  dump_to_html_log_file_with_date_and_time(current_write_open_picofile,"Sample rincing");
  //win_printf("Added Rincing sample");
  ask_to_update_menu();
  return D_O_K;
}

int add_stop_enzyme_injection_in_html_log(void)
{
    if(updating_menu_state != 0)
    {
        if (manual_enzyme_injection == 0)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;
        return D_O_K;
    }
    manual_enzyme_injection = 0;
    dump_to_html_log_file_with_date_and_time(current_write_open_picofile,"Rincing sample stopped");
    ask_to_update_menu();
    //win_printf("Added Rincing sample stopped");
    return D_O_K;
}

MENU *html_log_menu(void)
{
    static MENU mn[32] = {0};


    mn[0].text = NULL;
    add_item_to_menu(mn,"Add comment to log file", add_stuff_in_html_log,NULL,0,NULL);
    add_item_to_menu(mn,"Add rincing to trk and log file", add_rincing_in_html_log,NULL,0,NULL);
    add_item_to_menu(mn,"Add stop rincing to trk and log file", add_stop_rincing_in_html_log,NULL,0,NULL);
    add_item_to_menu(mn,"Add enzyme injection to trk and log file", add_enzyme_injection_in_html_log,NULL,0,NULL);
    add_item_to_menu(mn,"Add stop enzyme injection to trk and log file", add_stop_enzyme_injection_in_html_log,NULL,0,NULL);
    return mn;
}


char    *do_create_html_logfile(char *logfile)
{
  // int i;
    char path[512] = { 0 };
    //char file[256] = { 0 };
    char *fullfile = NULL;
    int file_exist = 0;


    if(updating_menu_state != 0)    return D_O_K;

    //switch_allegro_font(1);

    if (logfile != NULL)
    {
        //win_printf("logfile %s",backslash_to_slash(logfile));
        fix_filename_slashes(logfile);
    }
    fullfile = save_one_file_config("Create Pico log file (*.htm)", logfile, "index.htm", "Html Files (*.htm)\0*.htm;*.html;*.xml\0All Files (*.*)\0*.*\0\0", "PICO-LOGFILE","last_created");
    if (fullfile)
    {
        fp_log = fopen(fullfile,"r");
        if (fp_log != NULL)
        {
            file_exist = 1;
            fclose (fp_log);
        }
        fp_log = fopen(fullfile,"w");
        if (fp_log != NULL)
        {
            if (file_exist == 0)
                fprintf(fp_log,"<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n"
                        "<html>\n<head>\n"
                        "<title>Magnetic tweezers experiment log file</title>\n"
                        "<meta http-equiv=\"content-type\"  content=\"text/html; charset=ISO-8859-1\">\n"
                        "<body>\n"
                        "<h3 align=\"center\"><font color=\"#ff0000\">"
                        "Magnetic tweezers experiment log file</font></h3><br>\n");
            fclose (fp_log);
            if (current_write_open_picofile != NULL)
            {
                free(current_write_open_picofile);
                current_write_open_picofile = NULL;
            }
            current_write_open_picofile = fullfile;
            if (extract_file_path(path, 512, fullfile) != NULL)
            {
                if (current_write_open_path_picofile != NULL)
                {
                    free(current_write_open_path_picofile);
                    current_write_open_picofile = NULL;
                }
                current_write_open_path_picofile = strdup(path);
            }
            else
            {
            }
        }
        else
        {
            free(fullfile);
            return NULL;
        }
        return current_write_open_picofile;
    }
    return NULL;
}

int    dump_to_html_log_file_with_date_and_time(char *fullfilename, const char *fmt, ...)
{
    va_list ap;
    FILE *fp = NULL;
    //char s[256];
    //struct tm *t;
    //time_t ti;
    time_t timer;
    char c[2048] = {0};


    if (fullfilename == NULL)    return 1;
    if (fmt == NULL)    return 1;
    //    ti = time(0);
    //t = localtime(&ti);
    //strftime(s,256,"%C",t);

    time(&timer);

    fp = fopen(fullfilename,"a");
    if (fp == NULL)                return 1;
    //fprintf(fp,"%s\n",s);
    fprintf(fp,"%s\n",ctime(&timer));
    //win_printf("time in log : %s\n",ctime(&timer));
    va_start(ap, fmt);
    vsnprintf(c,2048,fmt,ap);
    va_end(ap);
    fprintf(fp,"%s\n",c);
    return fclose(fp);
}
int    dump_to_html_log_file_with_time(char *fullfilename, const char *fmt, ...)
{
    va_list ap;
    FILE *fp = NULL;
    char s[256];
    struct tm *t = NULL;
    time_t ti;
    char c[2048] = {0};


    if (fullfilename == NULL)    return 1;
    if (fmt == NULL)    return 1;
    ti = time(0);
    t = localtime(&ti);
    strftime(s,256,"%X",t);
    fp = fopen(fullfilename,"a");
    if (fp == NULL)                    return 1;
    fprintf(fp,"%s\n",s);
    va_start(ap, fmt);
    vsnprintf(c,2048,fmt,ap);
    va_end(ap);
    fprintf(fp,"%s\n",c);
    return fclose(fp);
}
int    dump_to_html_log_file_only(char *fullfilename, const char *fmt, ...)
{
    va_list ap;
    FILE *fp = NULL;
    char c[2048] = {0};

    if (fullfilename == NULL)    return 1;
    if (fmt == NULL)    return 1;
    fp = fopen(fullfilename,"a");
    if (fp == NULL)                    return 1;
    va_start(ap, fmt);
    vsnprintf(c,2048,fmt,ap);
    va_end(ap);
    fprintf(fp,"%s\n",c);
    return fclose(fp);
}



MENU *trackBead_image_menu(void)
{
    static MENU mn[32]  = {0};

    if (mn[0].text != NULL)	return mn;
    add_item_to_menu(mn,"freeze video", freeze_source,NULL,0,NULL);
    add_item_to_menu(mn,"live video", live_source,NULL,0,NULL);
    add_item_to_menu(mn,"kill video", kill_source,NULL,0,NULL);
    add_item_to_menu(mn,"Load Image\t(Ctrl-I)",do_load_im, NULL, 0, NULL);
#ifndef PICOTWIST
    add_item_to_menu(mn,"New bead track x y", follow_bead_in_x_y,NULL,MENU_INDEX(CHOOSE_SIZE),NULL);
    add_item_to_menu(mn,"Track x y z", follow_bead_in_x_y,NULL,MENU_INDEX(IMAGE_CAL),NULL);
#endif
    add_item_to_menu(mn,"Switch bead",NULL,bead_active_menu,0,NULL);
    add_item_to_menu(mn,"record calibration", record_calibration,NULL,0 ,NULL);
#ifndef PICOTWIST
    add_item_to_menu(mn,"ramp objective", ramp_objective,NULL,0 ,NULL);
# endif
    //add_item_to_menu(mn,"trajectories", record_x_y_trajectories,NULL,0 ,NULL);
    add_item_to_menu(mn,"angle", record_x_y_angle,NULL,0 ,NULL);

    //add_item_to_menu(mn,"grab profiles", grab_profile_rolling_buffer,NULL,0 ,NULL);
    add_item_to_menu(mn,"grab orthoradial profiles", grab_orthoradial_profile_rolling_buffer,NULL,0 ,NULL);

    //add_item_to_menu(mn,"Scan Zmag", record_scan,NULL,0 ,NULL);
    //add_item_to_menu(mn,"Hat curve", record_hat,NULL,0 ,NULL);
    //	add_item_to_menu(mn,"Rotation modulation", record_mod_rot_curve,NULL,0 ,NULL);
    //add_item_to_menu(mn,"Force curve", record_force_curve,NULL,0 ,NULL);
    add_item_to_menu(mn,"Move mag", move_mag,NULL,0 ,NULL);
#ifndef PICOTWIST
    add_item_to_menu(mn,"plot and image", display_imr_and_pr,NULL,0 ,NULL);
#endif
    add_item_to_menu(mn, "mean and sigma of movie", do_z_profile_mean_and_sigma_of_movie, NULL, 0, NULL);
    add_item_to_menu(mn, "mean and sigma of area movie", do_z_profile_mean_and_sigma_of_area_in_movie, NULL, 0, NULL);
    add_item_to_menu(mn, "mean and sigma of movie auto",do_z_profile_mean_and_sigma_of_movie_auto , NULL, 0, NULL);
    add_item_to_menu(mn, "mean and sigma of area movie auto",do_z_profile_mean_and_sigma_on_area_of_movie_auto, NULL, 0, NULL);
    //add_item_to_menu(mn,"Spectrum", spectrum_profile,NULL,0 ,NULL);
    //add_item_to_menu(mn,"Stop Spectrum", stop_spectrum_idle_action,NULL,0 ,NULL);
    //add_item_to_menu(mn,"Intensity vs time", intensity_vs_time,NULL,0 ,NULL);
    //add_item_to_menu(mn,"Stop Intensity", stop_intensity_vs_time_idle_action,NULL,0 ,NULL);
    //add_item_to_menu(mn,"check jobs", check_remaining_job,NULL,0 ,NULL);
    //add_item_to_menu(mn,"Do xvin err", do_bug,NULL,0 ,NULL);
  #ifdef SDI_VERSION_2
  add_item_to_menu(mn, "find beads",SDI_2_do_find_beads_in_image, NULL, 0, NULL);

  #endif

# ifdef GAME
    add_item_to_menu(mn,"Led modulation", mod_params,NULL,0 ,NULL);
# endif

    return mn;
}

int set_camera_to_best_SN(void)
{
    float gain, fmin, fmax, finc, g;
    int set = 0, smin = 0, smax = 0, exp;


    if(updating_menu_state != 0)
    {
        if (get_camera_gain == NULL  || get_camera_shutter_in_us == NULL)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;
        return D_O_K;
    }
    if (get_camera_gain == NULL)	return D_O_K;
    get_camera_gain(&gain, &fmin, &fmax, &finc);
    if (get_camera_shutter_in_us == NULL)	return D_O_K;
    get_camera_shutter_in_us(&set, &smin, &smax);
    g = fmin;
    set_camera_gain(g, &gain, &fmin, &fmax, &finc);
    exp = smax;
    set_camera_shutter_in_us(exp, &set, &smin, &smax);
    return D_O_K;
}
int define_camera_gain(void)
{
    float gain, fmin, fmax, finc, g;
    char question[1024] = {0};
    int j;

    if(updating_menu_state != 0)
    {
        if (get_camera_gain == NULL)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;
        return D_O_K;
    }
    if (get_camera_gain == NULL)
        return win_printf_OK("No function fund to handle camera gain!");
    get_camera_gain(&gain, &fmin, &fmax, &finc);
    snprintf(question,sizeof(question),"Define camera gain\n"
             "%g  <= Gain <= %g (Inc %g)\n"
             "Gain %%8f \n",fmin,fmax,finc);
    g = gain;
    j = win_scanf(question,&g);
    if (j == WIN_CANCEL) return D_O_K;
    set_camera_gain(g, &gain, &fmin, &fmax, &finc);
    return D_O_K;
}


int define_camera_shutter(void)
{
    int j, set = 0, smin = 0, smax = 0, exp;
    char question[1024] = {0};

    if(updating_menu_state != 0)
    {
        if (get_camera_shutter_in_us == NULL)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;
        return D_O_K;
    }
    if (get_camera_shutter_in_us == NULL)
        return win_printf_OK("No function fund to handle camera shutter!");
    get_camera_shutter_in_us(&set, &smin, &smax);
    snprintf(question,sizeof(question),"Define camera exposure Exp\n"
             "%d \\mu s <= Exp <= %d \\mu s\n"
             "Exp %%8d \\mu s\n",smin,smax);
    exp = set;
    j = win_scanf(question,&exp);
    if (j == WIN_CANCEL) return D_O_K;
    set_camera_shutter_in_us(exp, &set, &smin, &smax);
    return D_O_K;
}

MENU *exp_camera_menu(void)
{
    static MENU mn[32]  = {0}, *emn = NULL;
    int i, k;

    if (mn[0].text != NULL) return mn;
    mn[0].text = 0;
    add_item_to_menu(mn,"Define camera shutter", define_camera_shutter,NULL,0,NULL);
    add_item_to_menu(mn,"Define camera gain", define_camera_gain,NULL,0,NULL);
    add_item_to_menu(mn,"Set camera best SN", set_camera_to_best_SN,NULL,0,NULL);
    emn = specific_camera_image_menu();
    if (emn != NULL)
    {
    for (i = 0; i < 32 && mn[i].text != NULL; i++);
    for (k = 0; i < 32 && emn[k].text != NULL; k++)
      mn[i++] = emn[k];
    }

    return mn;
}

MENU *debug_menu(void)
{
    static MENU mn[32] = {0};
    mn[0].text = 0;
    add_item_to_menu(mn,"Switch On RT debug", switch_RT_debug_on,NULL,0,NULL);
    add_item_to_menu(mn,"Switch Off RT debug", switch_RT_debug_off,NULL,0,NULL);
# ifdef SERIAL_PICO
    add_item_to_menu(mn,"Switch On RS232 debug", switch_rs232_debug_on,NULL,0,NULL);
    add_item_to_menu(mn,"Switch Off RS232 debug", switch_rs232_debug_off,NULL,0,NULL);
# endif
    return mn;
}


MENU *trackBead_ex_image_menu(void)
{
    static MENU mn[32] = {0};
    static char stuff[1024] = {0};
# ifdef GAME
    int mod_params(void);
# endif

    mn[0].text = 0;
    //if (mn[0].text != NULL)	return mn;
    if (current_write_open_picofile)
    {
        if (strlen(current_write_open_picofile) < 32)
            snprintf(stuff,1024,"Log file %s",current_write_open_picofile);
        else snprintf(stuff,1024,"Log file %s",current_write_open_picofile
                      +strlen(current_write_open_picofile) -32);
    }
    else snprintf(stuff,1024,"Log file");

    add_item_to_menu(mn, "Config", NULL, config_file_menu(), 0, NULL);
    add_item_to_menu(mn,stuff, NULL,html_log_menu(),0,NULL);
    add_item_to_menu(mn, "focus", NULL, focus_plot_menu(), 0, NULL);
    add_item_to_menu(mn, "magnets", NULL, magnetscontrol_plot_menu(), 0, NULL);
#ifdef APT_MOT_TRANSLATION
    add_item_to_menu(mn, "APT translation", NULL, translation_control_plot_menu(),0,NULL);
#endif
    add_item_to_menu(mn, "Light and temperature", NULL, micro_menu(), 0, NULL);

#ifndef GAME
    add_item_to_menu(mn, "Camera", NULL, exp_camera_menu(), 0, NULL);
    //if (specific_camera_image_menu != NULL) add_item_to_menu(mn, "Specific Camera", NULL, specific_camera_image_menu(), 0, NULL);
    //add_item_to_menu(mn, "Specific Camera", NULL, specific_camera_image_menu(), 0, NULL);
#endif
    add_item_to_menu(mn, "Debugging", NULL, debug_menu(), 0, NULL);
    add_item_to_menu(mn,"\0",	NULL,			NULL,	0, NULL);
#ifdef SDI_VERSION_2
  add_item_to_menu(mn,"SDI 2 tracking controls",NULL,SDI_2_tracking_parameters_menu(),0,NULL);
#endif
#ifndef SDI_VERSION
#ifndef PICOTWIST
    add_item_to_menu(mn, "Profile with sse", switch_radial_sse, NULL, 0, NULL);
    add_item_to_menu(mn, "Profile STD",  switch_radial_std, NULL, 0, NULL);
    add_item_to_menu(mn, "Profile Apo",  switch_radial_apo, NULL, 0, NULL);
    add_item_to_menu(mn,"\0",	NULL,			NULL,	0, NULL);
	add_item_to_menu(mn,"New bead track x y", follow_bead_in_x_y,NULL,MENU_INDEX(CHOOSE_SIZE),NULL);
    add_item_to_menu(mn,"Track x y z", follow_bead_in_x_y,NULL,MENU_INDEX(IMAGE_CAL),NULL);
#endif
    add_item_to_menu(mn,"Switch bead",NULL,bead_active_menu,0,NULL);
    add_item_to_menu(mn, "Servo pifoc on one bead", switch_bead_servo, NULL, 0, NULL);
    add_item_to_menu(mn, "Freeze bead tracking", switch_bead_repositionning, NULL, 0, NULL);
    add_item_to_menu(mn,"\0",	NULL,			NULL,	0, NULL);
    add_item_to_menu(mn,"record calibration", record_calibration,NULL,0 ,NULL);
#endif
    //add_item_to_menu(mn,"trajectories", record_x_y_trajectories,NULL,0 ,NULL);
    //add_item_to_menu(mn,"grab profiles", grab_profile_rolling_buffer,NULL,0 ,NULL);
    //add_item_to_menu(mn,"Scan Zmag", record_scan,NULL,0 ,NULL);
    //add_item_to_menu(mn,"Hat curve", record_hat,NULL,0 ,NULL);
    //	add_item_to_menu(mn,"Force curve", record_force_curve,NULL,0 ,NULL);
    //add_item_to_menu(mn,"Force curve auto", record_auto_force_curve,NULL,0 ,NULL);
#ifdef SDI_VERSION
    add_item_to_menu(mn, "SDI",	NULL, sdi_menu(), 0, NULL);
#endif
    add_item_to_menu(mn, "Z(t)", NULL, zdet_menu(), 0, NULL);
    add_item_to_menu(mn, "Rotation Scan", NULL, hat_menu(), 0, NULL);
    add_item_to_menu(mn, "Vertical Scan", NULL, scan_menu(), 0, NULL);
    add_item_to_menu(mn, "Force curve", NULL, force_menu(), 0, NULL);
    add_item_to_menu(mn, "Stop recording Z(t) 3 phases", stop_zdet, NULL, 0, NULL);
#ifndef SDI_VERSION
    add_item_to_menu(mn, "Reload tracking", do_load_trk_in_new_project, NULL, 0, NULL);
    add_item_to_menu(mn, "Load generic calibration", load_generic_calibration_image, NULL, 0, NULL);
#endif
#if defined(XV_OPENCV) && defined(OPENCL)
    add_item_to_menu(mn, "Count beads using hough transform", do_count_beads_hough, NULL, 0, NULL);
#endif
#ifndef SDI_VERSION
	add_item_to_menu(mn, "Recount beads", count_beads_like_this_one, NULL, MENU_INDEX(RECOUNT), NULL);
    add_item_to_menu(mn, "Configure create cross by shortcut",do_configure_create_cross_by_shortcut, NULL, 0, NULL);
#endif
# ifdef GAME
    add_item_to_menu(mn,"Led modulation", mod_params,NULL,0 ,NULL);
    //add_item_to_menu(mn, "Led modulation", mod_params, NULL, 0, NULL);
# endif
    return mn;
}


int dump_all_config_entries(void)
{
  int i, n, j, m;
  char const **sections = NULL;
  char const **entries = NULL;
  FILE *fp = NULL;
  char file[2048] = {0};

  if(updating_menu_state != 0)	return D_O_K;

   if (do_select_file(file, sizeof(file), "TXT-FILE", "*.txt", "File to save config entries\0", 0))
        return win_printf_OK("Cannot select input file");

  fp = fopen(file,"w");
  if (fp == NULL)  return -2;
  m = list_config_entries("", &entries);
  /* loop through all entries in the section */
  fprintf(fp,"Global section\n");
  for (j = 0; j < m; j++)
    {
      fprintf(fp," %s=\"%s\"\n", entries[j], get_config_string("", entries[j], "-"));
    }


  n = list_config_sections(&sections);
  /* loop through all section names */
  for (i = 0; i < n; i++)
    {
      fprintf(fp,"%s\n", sections[i]);
      m = list_config_entries(sections[i], &entries);
      /* loop through all entries in the section */
      for (j = 0; j < m; j++)
	{
          fprintf(fp," %s=\"%s\"\n", entries[j],
		  get_config_string(sections[i], entries[j], "-"));
	}
    }
   free_config_entries(&sections);
   free_config_entries(&entries);
   fclose(fp);
   return 0;
}



MENU *trackBead_plot_menu(void)
{
  static MENU mn[32] = {0};
  static char stuff[1024] = {0};


    mn[0].text = 0;
    //if (mn[0].text != NULL)	return mn;
    if (current_write_open_picofile)  snprintf(stuff,1024,"Log file %s",current_write_open_picofile);
    else snprintf(stuff,1024,"Log file");

    //add_item_to_menu(mn,"Stop trajectories", stop_record_x_y_trajectories,NULL,0,NULL);
    add_item_to_menu(mn,"Stop angle", stop_record_x_y_angle,NULL,0,NULL);
    add_item_to_menu(mn, "Config", NULL, config_file_menu(), 0, NULL);
    add_item_to_menu(mn,stuff,NULL, html_log_menu(),0,NULL);
    add_item_to_menu(mn, "focus", NULL, focus_plot_menu(), 0, NULL);
    add_item_to_menu(mn, "magnets", NULL, magnetscontrol_plot_menu(), 0, NULL);
    add_item_to_menu(mn, "Light and temperature", NULL, micro_menu(), 0, NULL);
    add_item_to_menu(mn, "Debugging", NULL, debug_menu(), 0, NULL);
    add_item_to_menu(mn,"\0",	NULL,			NULL,	0, NULL);
    add_item_to_menu(mn,"Switch bead",NULL,bead_active_menu,0,NULL);
    add_item_to_menu(mn, "Freeze bead tracking", switch_bead_repositionning, NULL, 0, NULL);
    add_item_to_menu(mn, "Servo pifoc on one bead", switch_bead_servo, NULL, 0, NULL);
    #ifdef SDI_VERSION_2
      add_item_to_menu(mn,"SDI 2 tracking controls",NULL,SDI_2_tracking_parameters_menu(),0,NULL);
    #endif

    #ifdef USE_MLFI
      add_item_to_menu(mn,"MLFI (lock-in) menu",NULL,mlfi_plot_menu(),0,NULL);
    #endif

    add_item_to_menu(mn, "Z(t)", NULL, zdet_plot_menu(), 0, NULL);
    add_item_to_menu(mn, "Hat", NULL, hat_plot_menu(), 0, NULL);
    add_item_to_menu(mn, "Z scan", NULL, scan_plot_menu(), 0, NULL);
    add_item_to_menu(mn, "Force curve", NULL, force_plot_menu(), 0, NULL);
    add_item_to_menu(mn, "Reload tracking", do_load_trk_in_new_project, NULL, 0, NULL);
    add_item_to_menu(mn, "Reload tracking (verbose)", do_load_trk_in_new_project, NULL, MENU_INDEX(1), NULL);
    add_item_to_menu(mn,"Record menu",NULL,trk_plot_menu(),0,NULL);
    add_item_to_menu(mn, "Dump XYZ of tracking", draw_track_XYZ_trajectories, NULL, 0, NULL);

    add_item_to_menu(mn, "Dump config entries", dump_all_config_entries, NULL, 0, NULL);
    return mn;
}


// trackBead 3D bead tracking software wich can be compile to use different image sources
// simulate a bead : "game"
// use a movie :  "movie"
// use IFC data acquisition "ifc"
// use CVB data acquisition "cvb"

// source have specific functions sharing the same names
// int init_image_source();
// int start_data_movie(imreg *imr);
// DWORD WINAPI TrackingThreadProc( LPVOID lpParam );

// trackBead creates an image region withe the special image to track
// it defines the image idle action which typically consists in screen refresh
// it also creates a plot region where timing plots are gathered
// it also fills up a structure with all these variables
// it adds a general menu to images: trackBead_image_menu()
// it lauches a thread for tracking with a specific function TrackingThreadProc
// this function trigger a timer function used to track

int	trackBead_main(int argc, char **argv)
{
    imreg *imr = NULL;
    pltreg *pr = NULL;
    int i;
    O_p *op = NULL;
    //d_s *ds;
    O_i *oi = NULL;
    DIALOG *di_mn = NULL; //, *di;
    int ret, started, ended, load_generic = 1,load_log = 1, load_cal = 0;//, rot_ref_checked = 0, zmag_ref_checked = 0;
    char sdate[256] = {0}, question[2048] = {0}, path[512] = {0};
    const char *generic = NULL, *log_html = NULL, *last_trk = NULL;
    char generic_l[512] = {0}, log_html_l[512] = {0}, last_trk_l[512] = {0};
# ifdef SERIAL_PICO  //# ifndef GAME
    //int is_pico = 0;
# endif
    struct tm *t = NULL;
    time_t ti;
    //float r, z;
    //int i;
    //  extern O_i *oi_CVB;
    //extern int     (*has_magnets_memory)(void);
    //extern int     (*has_focus_memory)(void);

    (void)argc;
    (void)argv;
    //XV_ARRAY(MENU,  help_menu);

    setlocale(LC_ALL, "C");
    add_item_to_menu(plot_menu, "Experiment", NULL, plot_experiment_menu = trackBead_plot_menu(), 0, NULL);
    add_item_to_menu(plot_menu, "Help", NULL, help_menu, 0, NULL);
    add_item_to_menu(plot_menu, "Quicklink", NULL, quicklink_menu, 0, NULL);
    //add_image_menu_item( "Experiment", NULL, trackBead_plot_menu(), 0, NULL);

    dump_to_error_file_with_date_and_time("trackBead_main: File %s line %ul",__FILE__, __LINE__);




#ifdef XV_WIN32

    const char progname[256] = "picoueye.exe";
    int retp = 0;
#ifndef PIAS
    //win_printf("testing if %s already launched!",progname);
    retp = IsProcessRunning(progname);
    if (retp > 1)
      {
	win_printf("%s is already running %d times !\n"
		   "Since the program uses the serial port and the camera that cannot be shared\n"
		   "You should kill it first!\n"
		   "To do that open the Windows Task Manager, go in the process tab\n"
		   "And kill thye %s process\n"
		   "If this does not work reboot the PC.\n",progname, retp-1,progname);
	exit(0);
      }
    //else win_printf("found %d instance of %s!",retp,progname);

#endif
#endif

#ifdef XV_UNIX

    const char progname[256] = "picoueye";
    int retp = 0;
#ifndef PIAS
    //win_printf("testing if %s already launched!",progname);
    retp = proc_find(progname);
    retp += proc_find("pico-ueye");
    if (retp > 1)
      {
	win_printf("%s is already running %d times !\n"
		   "Since the program uses the serial port and the camera that cannot be shared\n"
		   "You should kill it first!\n"
		   "To do that open the Windows Task Manager, go in the process tab\n"
		   "And kill thye %s process\n"
		   "If this does not work reboot the PC.\n",progname, retp-1,progname);
	exit(0);
      }
    //else win_printf("found %d instance of %s!",retp,progname);

#endif
#endif



    XVcfg_main();
    interfaces_working = 2;
# ifndef GAME
    interfaces_working = 2;
    //# if !defined(MAD_CITY_LAB) && !defined(PIFOC_DAS1602) && !defined(PI_E761)
# ifdef SERIAL_PICO   // change from ndef unsure !
    if (init_pico_serial())
    {
      //switch_from_pico_serial_to_dummy(is_pico = 0);
        interfaces_working--;
        win_printf("Your communication interface has been switched to a simultaion\n"
                   "interface for debugging purpose, To recover a working configuration\n"
                   "you need to switch back your configuration to serial interface and\n"
                   "then to restart the program\n");
	dump_to_error_file_with_date_and_time("pico serial failed: File %s line %ul"
					      ,__FILE__, __LINE__);
    }
    else dump_to_error_file_with_date_and_time("pico serial went fine: File %s line %ul"
					      ,__FILE__, __LINE__);
# endif
    dump_to_error_file_only("after pico serial: File %s line %ul",__FILE__, __LINE__);
# endif

    focus_main(0,NULL);
    dump_to_error_file_only("focus_main: File %s line %ul",__FILE__, __LINE__);
    magnetscontrol_main(0,NULL);
    dump_to_error_file_only("magnetcontrol_main: File %s line %ul",__FILE__, __LINE__);
 #if defined(FLOW_CONTROL) || defined(FLOW_CONTROL_DUMMY)
    flow_control_main(0,NULL);
 #endif

    imr = find_imr_in_current_dialog(NULL);
    // if a movie is load already we use this movie as a data source
    ret = 0;
    imr = find_imr_in_current_dialog(NULL);
    if (imr == NULL)    ret = init_image_source(NULL,1);
    else
    {
        if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2) ret = init_image_source(imr,1);
        else if (oi->im.movie_on_disk == 0 || oi->im.n_f < 2) ret = init_image_source(imr,1);
        else if (oi->im.nx == 0) ret = init_image_source(imr,1);
    }
    if (ret) win_printf_OK("Your video source did not initiate properly!");
    if (ret)   interfaces_working--;
    dump_to_error_file_only("init video: File %s line %ul",__FILE__, __LINE__);
    //win_printf("after init");
    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)
        return win_printf_OK("You must load a movie in the active window");
    if (oi->filename == NULL) set_oi_filename(oi, "LIVE VIDEO");
    def_oi_scaling(oi); // oi_TRACK

    // We create a plot region to display bead position, timing  etc.
    pr = create_hidden_pltreg_with_op(&op, TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0,"Tracking status");
    //pr = create_pltreg_with_op(&op, 4096, 4096, 0);
    if (pr == NULL)  win_printf_OK("Could not find or allocte plot region!");
    pr_TRACK = pr;
    if (op == NULL)  win_printf_OK("Could not find or allocate plot !");
    op->dat[0]->xd[1] = op->dat[0]->yd[1] = 1;
    pr->one_p->need_to_refresh = 1;
    switch_project_to_this_imreg(imr);
    broadcast_dialog_message(MSG_DRAW,0);

    m_job = 4096; // [FLY] : increased number of job
    n_job = 0;
    job_pending = (f_job*)calloc(m_job,sizeof(f_job));
    if (job_pending == NULL) win_printf_OK("Could not allocate job_pending!");

    //we attach image menu
    if (imr_TRACK == NULL)
    {
        add_image_treat_menu_item ( "trackBead", NULL, trackBead_image_menu(), 0, NULL);
        di_mn = the_dialog + 1;
	for(;screen_acquired;); // we wait for screen_acquired back to 0;
	screen_acquired = 1;
        acquire_bitmap(screen);
        rectfill(screen,di_mn->x,di_mn->y,di_mn->x+di_mn->w-1,
                 di_mn->y+di_mn->h-1, makecol(255,255,255));
        release_bitmap(screen);
	screen_acquired = 0;
        di_mn->w = 0; 	   di_mn->h = 0;
        xvin_d_menu_proc(MSG_END,the_dialog + 1,0);
        add_item_to_menu(image_menu, "Experiment", NULL, trackBead_ex_image_menu(), 0, NULL);
        add_item_to_menu(image_menu, "Quicklink", NULL, quicklink_menu, 0, NULL);
        //add_item_to_menu(image_menu, "Help", NULL, help_menu, 0, NULL);
        xvin_d_menu_proc(MSG_START,the_dialog + 1,0);
        xvin_d_menu_proc(MSG_DRAW,the_dialog + 1,0);
        start_data_movie(imr);
    }
    dump_to_error_file_only("start: File %s line %ul",__FILE__, __LINE__);
    imr_TRACK = imr;
    oi_TRACK = imr->one_i;

    add_image_treat_menu_item ( "Save movie", NULL, save_movie_in_track_image_menu(), 0, NULL);
    d_TRACK = find_dialog_associated_to_imr(imr_TRACK, NULL);
    dtid.imr = imr;
    dtid.pr = pr;
    dtid.oi = oi_TRACK;
    dtid.op = op;
    dtid.dimr = d_TRACK;
    dtid.dpr = find_dialog_associated_to_pr(pr_TRACK, NULL);
    dtid.dbid = &bid;
    before_menu_proc = track_before_menu;
    after_menu_proc = track_after_menu;
	oi_TRACK->oi_got_mouse = track_oi_got_mouse;
    oi_TRACK->oi_idle_action = track_oi_idle_action;
    oi_TRACK->oi_mouse_action = track_oi_mouse_action;
    oi_TRACK->oi_post_display = track_oi_post_display;
    init_track_info();
    dump_to_error_file_only("init track info: File %s line %ul",__FILE__, __LINE__);
    go_track = TRACK_ON;
    create_tracking_thread(&dtid);
    general_end_action = source_end_action;
    general_idle_action = source_idle_action;

    started = get_config_int("EXPERIMENT","started",0);
    ended = get_config_int("EXPERIMENT","ended",0);
#if !defined(SDI_VERSION) && !defined(SDI_VERSION_2)
    generic = get_config_string("IMAGE-GR-FILE","last_generic_calibration_0",NULL);
    snprintf(generic_l,sizeof(generic_l),"%s",generic);
#endif
    log_html = get_config_string("PICO-LOGFILE","last_created",NULL);
    last_trk = get_config_string("TRACKING-FILE","last_saved",NULL);
    if (last_trk == NULL)
      last_trk = get_config_string("TRACKING-FILE","last_loaded",NULL);
    snprintf(log_html_l,sizeof(log_html_l),"%s",log_html);
    snprintf(last_trk_l,sizeof(last_trk_l),"%s",last_trk);
#if defined USE_MLFI
    opmlfi = prepare_mlfi_thread(pr_TRACK);

#endif    //defined USE_MLFI
#if defined(SDI_VERSION) || defined(SDI_VERSION_2)
    if (log_html)
    {
        if (ended > started)
        {
            ti = (time_t)ended;;
            t = localtime(&ti);
            strftime(sdate,256,"%C",t);
            snprintf(path,sizeof(path),"Your last experiment ended %s",ctime(&ti));
            for (i = 0; i < (int)sizeof(path) && path[i] != '\n'; i++);
            if (i < (int)sizeof(path) && path[i] == '\n') path[i] = 0;
            if (current_write_open_picofile != NULL) dump_to_html_log_file_with_date_and_time(current_write_open_picofile, "New experiment started");
        }
        else
        {
            ti = (time_t)started;;
            t = localtime(&ti);
            strftime(sdate,256,"%C",t);
            snprintf(path,sizeof(path),"Your last experiment started %s apparently crashed ! We apologize ...",ctime(&ti));
            for (i = 0; i < (int)sizeof(path) && path[i] != 0; i++) path[i] = (path[i] == '\n') ? ' ' : path[i];
            if (current_write_open_picofile != NULL) dump_to_html_log_file_with_date_and_time(current_write_open_picofile, "Program restarted started");
        }
        dump_to_error_file_only("reloading: File %s line %ul",__FILE__, __LINE__);
        snprintf(question,2048,"%s\n"
                 "All file recording will use by default the path of your log file.\n"
                 "It is a good idea to save your log file in an appropriate folder.\n"
                 "Do you want to continue using the following log file: \n"
                 "{\\color{yellow}%s}  No %%R yes %%r\n"
                 ,path, backslash_to_slash(log_html));
        win_scanf(question,&load_log);
        current_write_open_picofile = strdup(get_config_string("PICO-LOGFILE","last_created",""));
        snprintf(question,2048,"%s\n"
                 "Do you want to reload beads from your last TRK recording:\n"
                 "{\\color{yellow}%s}  No %%R yes %%r from another file %%r\n"
                 ,path,backslash_to_slash(last_trk_l));
        win_scanf(question,&load_cal);
        if ((load_log > 0) && (current_write_open_picofile != NULL))
        {
            fp_log = fopen(current_write_open_picofile,"r");
            if (fp_log != NULL)
            {
                fclose (fp_log);
                if (extract_file_path(path, sizeof(path), current_write_open_picofile) != NULL)
                {
                    if (current_write_open_path_picofile != NULL) free(current_write_open_path_picofile);
                    current_write_open_path_picofile = strdup(path);
                }
            }
            else do_create_html_logfile(NULL);
        }
        else do_create_html_logfile(NULL);
    }
    else do_create_html_logfile(NULL);
#else
    if (generic && log_html)
    {
        if (ended > started)
        {
            ti = (time_t)ended;;
            t = localtime(&ti);
            strftime(sdate,256,"%d/%m/%Y %H:%M:%S",t);
            snprintf(path,sizeof(path),"Your last experiment ended %s",ctime(&ti));
            for (i = 0; i < (int)sizeof(path) && path[i] != '\n'; i++);
            if (i < (int)sizeof(path) && path[i] == '\n') path[i] = 0;
            //rot_ref_checked = 0; zmag_ref_checked = 0;
            if (current_write_open_picofile != NULL)
                dump_to_html_log_file_with_date_and_time(current_write_open_picofile, "New experiment started");
        }
        else
        {
            ti = (time_t)started;;
            t = localtime(&ti);
            strftime(sdate,256,"%d/%m/%Y %H:%M:%S",t);
            snprintf(path,sizeof(path),"Your last experiment started %s apparently crashed ! We apologize ...",ctime(&ti));
            for (i = 0; i < (int)sizeof(path) && path[i] != 0; i++)
                path[i] = (path[i] == '\n') ? ' ' : path[i];
            if (current_write_open_picofile != NULL)
                dump_to_html_log_file_with_date_and_time(current_write_open_picofile, "Program restarted started");
        }
        dump_to_error_file_only("reloading: File %s line %ul",__FILE__, __LINE__);
        snprintf(question,2048,"%s\n"
                 "Do you want to reload the calibration images used in your last TRK recording:\n"
                 "{\\color{yellow}%s}  No %%R yes %%r from another file %%r\n"
                 "Or do you want to reload the last calibration images recorded: No %%r yes %%r\n"
                 "Do you want to use your last generic calibration image:\n"
                 "{\\color{yellow}%s}  No %%R yes %%r\n"
                 "All file recording will use by default the path of your log file.\n"
                 "It is a good idea to save your log file in an appropriate folder.\n"
                 "Do you want to continue using the following log file: \n"
                 "{\\color{yellow}%s}  No %%R yes %%r\n"
                 //"Do you want to check your magnets rotation reference position No %%R Yes %%r\n"
                 //"Do you want to check your magnets vertical reference position No %%R Yes %%r\n"
                 ,path,backslash_to_slash(last_trk_l),backslash_to_slash(generic_l),
                 backslash_to_slash(log_html_l));
        win_scanf(question,&load_cal,&load_generic,&load_log);
        if (load_generic) 	do_reload_generic_calibration(0);
        else load_generic_calibration_image();
        current_write_open_picofile = strdup(get_config_string("PICO-LOGFILE","last_created",""));
        if ((load_log > 0) && (current_write_open_picofile != NULL))
        {
            fp_log = fopen(current_write_open_picofile,"r");
            if (fp_log != NULL)
            {
                fclose (fp_log);
                if (extract_file_path(path, sizeof(path), current_write_open_picofile) != NULL)
                {
                    if (current_write_open_path_picofile != NULL)
                        free(current_write_open_path_picofile);
                    current_write_open_path_picofile = strdup(path);
                }
            }
            else do_create_html_logfile(NULL);
        }
        else do_create_html_logfile(NULL);
    }
    else
    {
        load_generic_calibration_image();
        do_create_html_logfile(NULL);
    }
#endif
    time(&ti);
    set_config_int("EXPERIMENT","started",(int)ti);

#ifdef SDI_VERSION
	sdi_instrument_init();
	do_follow_bead_in_x_y(GENERIC_IMAGE_CAL);//tv : version edulcorée, juste pour les plots et lancer track_info
#else
    if (load_cal == 1) 	       reload_calibrations_from_last_trk_old();
    else   if (load_cal == 2)    reload_calibrations_from_last_trk();
    else   if (load_cal == 4)    relink_all_calibrations();
    else   do_follow_bead_in_x_y(GENERIC_IMAGE_CAL);
#endif
    //do_follow_bead_in_x_y(GENERIC_IMAGE_CAL);
    dump_to_error_file_only("cardinal_main: File %s line %ul",__FILE__, __LINE__);
# ifdef XVIN_STATICLINK
    cardinal_main(0,NULL);
    stat_main(0,NULL);
    wlc_main(0,NULL);
    //polymerWLC_main(0,NULL);

#if defined(OPENCL) && !defined(SDI_VERSION)
    trackBead_gpu_main(0, NULL);
#endif
#endif

# ifdef XVIN_STATICLINK
    thermal_z_main(0, NULL);
# endif

    // BUG MAGNET MOVE AT START ...
    //immediate_next_available_action(MV_ZMAG_ABS, prev_zmag);
    //immediate_next_available_action(MV_ROT_ABS, prev_mag_rot);
    fill_next_available_action(track_info->imi[track_info->c_i]+30, READ_GAIN_EXPO, 0);
    fill_next_available_action(track_info->imi[track_info->c_i]+40, READ_LED_VAL, 0);

    fill_next_available_action(track_info->imi[track_info->c_i]+45, CHG_CAMERA_GAIN, 0.01);
    fill_next_available_action(track_info->imi[track_info->c_i]+50, CHG_CAMERA_EXPO, 10000000);
    //  if (SCREEN_W > 1800)
    //  display_imr_and_pr();
    return D_O_K;
}


int shut_down_trk(void)
{
    //int shut_down_magnets(void);
# ifdef TRACK_WIN_THREAD
    DWORD lpExitCode;

    GetExitCodeThread(track_hThread, &lpExitCode);
    TerminateThread(track_hThread, lpExitCode);
    CloseHandle(track_hThread);
    // to look at
    while (source_running);
# else
    void * lpExitCode;
    pthread_join(track_thread, &lpExitCode);
# endif
    return 0;
}





int	trackBead_unload(int argc, char **argv)
{
    (void)argc;
    (void)argv;
    remove_item_to_menu(image_treat_menu, "trackBead", NULL, NULL);
    return D_O_K;
}
#endif
