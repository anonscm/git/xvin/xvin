
#ifndef _HAT_REC_C_
#define _HAT_REC_C_

# include "allegro.h"
#ifdef XV_WIN32
# include "winalleg.h"
#endif
# include "xvin.h"





/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "../cfg_file/Pico_cfg.h"
#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "../fft2d/fftbtl32n.h"
# include "fillibbt.h"
# include "focus.h"
# include "magnetscontrol.h"
# include "trackBead.h"
# include "brown_util.h"
# include "track_util.h"
# include "hat.h"
# include "hat_rec.h"
# include "record.h"
# include "scan_zmag.h"
# include "action.h"




hat_param *load_Hat_param_from_trk(g_record *g_r)
{
  int cfg_size;
  hat_param *H_p = NULL;
  char *cfg = NULL, *st = NULL, *st1 = NULL;
  FILE *fp = NULL;


  if (g_r == NULL || g_r->fullname == NULL) return NULL;
  fp = fopen(g_r->fullname,"rb");
  if (fp == NULL)   return NULL;
  fseek(fp, 0, SEEK_SET);     
  fseek(fp, g_r->config_file_position, SEEK_SET);     
  cfg_size = g_r->header_size - g_r->config_file_position;
  if (cfg_size <= 0)                    return NULL;
  cfg = (char *)calloc(cfg_size,sizeof(char));
  if (cfg == NULL)                    return NULL;
  if (fread(cfg, sizeof(char),cfg_size,fp) != (size_t)cfg_size)	return NULL;
  st = strstr(cfg,"[Hat_Curve]");
  if (st == NULL)
    {
      fclose(fp);
      free(cfg);
      return NULL;
    }
  H_p = (hat_param*)calloc(1,sizeof(hat_param));
  if (H_p == NULL)
    {
      fclose(fp);
      free(cfg);
      return NULL;
    }
  st1 = strstr(st,"rot_step");
  if (st1 != NULL)
    sscanf(st1,"rot_step = %f",&(H_p->rot_step));
  st1 = strstr(st,"zmag");
  if (st1 != NULL)
    sscanf(st1,"zmag = %f",&(H_p->zmag));
  st1 = strstr(st,"zmag_after");
  if (st1 != NULL)
    sscanf(st1,"zmag_after = %f",&(H_p->zmag_finish));

  st1 = strstr(st,"n_step");
  if (st1 != NULL)
    sscanf(st1,"n_step = %d",&(H_p->nstep));
  st1 = strstr(st,"nper");
  if (st1 != NULL)
    sscanf(st1,"nper = %d",&(H_p->nper));
  st1 = strstr(st,"dead");
  if (st1 != NULL)
    sscanf(st1,"dead = %d",&(H_p->dead));
  st1 = strstr(st,"one_way");
  if (st1 != NULL)
    sscanf(st1,"one_way = %d",&(H_p->one_way));
  st1 = strstr(st,"setling");
  if (st1 != NULL)
    sscanf(st1,"setling = %d",&(H_p->setling));
  free(cfg);
  fclose(fp);
  return H_p;
} 

int redraw_hat_from_trk(void)
{
  int  i, prev, j, page_n, i_page;
  //b_track *bt = NULL;
  pltreg *pr = NULL;
  O_p *op = NULL, *ops = NULL;
  d_s *ds = NULL, *dss = NULL;
  int nf, ims = 0, im0 = 0, nw, profile_invalid = 0, n_avg_pt, n_test, debug = 0, ima, ime, ret;
  float   zmag = 0, rot = 0, xavg, yavg, zavg, obj; // rot_start = 10,
  //b_record *br = NULL;
  g_record *g_r = NULL;  
  hat_param *sph = NULL; 
  
  ac_grep(cur_ac_reg,"%pr",&pr);
  g_r = find_g_record_in_pltreg(pr);   
  if(updating_menu_state != 0)	
    {  // we wait for recording finish
      if (g_r == NULL || working_gen_record == g_r) 
	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;	
      return D_O_K;
    }
  if (pr == NULL)return 0;

  //if (g_r == NULL || g_r->n_record == 0) return OFF;
  if (g_r == NULL) return OFF;
  nf = g_r->abs_pos;
  if (nf <= 0) return OFF;



  sph = load_Hat_param_from_trk(g_r);
  if (sph == NULL)
    return win_printf_OK("cannot retrieve hat info !");





  //free(sph);
  //return 0;;




  for (j = 0, prev = 1; j < nf; j++)
    {
      page_n = j/g_r->page_size;
      i_page = j%g_r->page_size;
      ims = g_r->imi[page_n][i_page];
      if (g_r->status_flag[page_n][i_page] == 0 && prev != 0)
	{
	  im0 = j;
	  zmag = g_r->zmag[page_n][i_page];
	  rot = g_r->rot_mag[page_n][i_page];
	}
      if (g_r->status_flag[page_n][i_page] != 0 && prev == 0)
	{
	  ims = j;
	  if (ims - im0 > sph->nper) break;
	}
      prev = g_r->status_flag[page_n][i_page];
    }


  n_avg_pt = track_has_averaging_data(g_r);
  n_test = track_has_n_test_data(g_r);


  win_printf("Hat %d points way %d setling %d nper %d\n"
	     "rot_step %g zmag %g zmag_finish %g\n "
	     "im0 = %d ims = %d zmag %g rot %g\n"
	     "n average pt %d n test pt %d\n",
	     sph->nstep,sph->one_way,sph->setling,sph->nper,
	     sph->rot_step,sph->zmag,sph->zmag_finish, im0, ims, zmag, rot, n_avg_pt, n_test);


   nw = (sph->one_way) ? 1 : 2;
   if (n_avg_pt >=  nw * sph->nstep)
      {
       retrieve_image_index_of_acquisition_point(g_r,0,0,&im0, &ima, &ime, &zmag, &rot, &obj);
     }
   else
     {
       im0 = retrieve_image_index_of_next_acquisition_point(g_r, 0, sph->nper+sph->setling, &ims, &zmag, &rot);
     }


   //win_printf("im0 = %d ims = %d zmag %g rot %g",    im0, ims, zmag, rot);



   //rot_start = g_r->rot_mag[0][0];
  zmag = g_r->zmag[0][0];





  ops = create_and_attach_one_plot(pr, nw * sph->nstep, nw * sph->nstep, 0);
  dss = ops->dat[0];
  set_ds_source(dss,"Hat curve");      
  set_op_filename(ops, "Hat-%d.gr",g_r->n_rec);
  set_plot_title(ops, "Hat %d status ", g_r->n_rec);
  set_plot_x_title(ops, "Rotation");
  set_plot_y_title(ops, "Extension");			
      
  ops->need_to_refresh = 1;

  for (j = 1; j < g_r->n_bead; j++)
    dss = create_and_attach_one_ds(ops, nw * sph->nstep, nw * sph->nstep, 0);
  for (i = 0; i < g_r->n_bead; i++)
    {
      //win_printf("bead %d",i);
      if (g_r->b_r[i]->calib_im == NULL) continue;
      dss = ops->dat[i];
      for (j = 0, im0 = 0; j < dss->nx; j++)
	{
	  if (n_avg_pt >=  nw * sph->nstep)
	    {
	      ret = retrieve_image_index_of_acquisition_point(g_r,j,0,&im0, &ima, &ime, &zmag, &rot, &obj);
	      if (ret && debug != WIN_CANCEL)
		debug = win_printf("Pb retrievin data pt %d\nTo skip further warning press CANCEL",j);
	      if (ime - ima < sph->nper && debug != WIN_CANCEL)
		debug = win_printf("Duration Pb  hat point j = %d im0 %d duration\n"
				   "To skip further warning press CANCEL",j,ime - ima);
	      ret = average_bead_z_during_part_trk(g_r, i, ima, ime-ima, &xavg, &yavg, &zavg, 
						   NULL, &profile_invalid);
	      if (ret <= 0 && ret > -5 && debug != WIN_CANCEL)
		  debug = win_printf("Pb retrieving hat avg point %d, ima %d duration %d\n"
			     "To skip further warning press CANCEL",i,ima,ime-ima);
	      else if (ret == -5)
		my_set_window_title("Pb retrieving hat avg point %d, ima %d duration %d",i,ima,ime-ima);
	    }
	  else
	    {
	      im0 = retrieve_image_index_of_next_acquisition_point(g_r, im0, sph->nper, &ims, &zmag, &rot);
	      if (im0 < 0 && debug != WIN_CANCEL)
		{
		  debug = win_printf("Pb retrieving hat point j = %d im0 %d\n"
			     "To skip further warning press CANCEL",j,im0);
		}
	      if (ims - im0 < sph->nper && debug != WIN_CANCEL)
		debug = win_printf("Duration Pb  hat point j = %d im0 %d duration %d expected %d\n"
				   "To skip further warning press CANCEL",
				   j,im0,ims - im0,sph->setling + sph->nper);

	      ret = average_bead_z_during_part_trk(g_r, i,ims-sph->nper, sph->nper, &xavg, &yavg, &zavg, 
						   &obj, &profile_invalid);
	      if (ret <= 0 && ret > -5 && debug != WIN_CANCEL) 
		{
		  debug = win_printf("Pb retrieving hat avg point  %d, ima %d duration %d\n"
				     "To skip further warning press CANCEL",i,im0,ims-im0);
		}
	      else if (ret == -5)
		my_set_window_title("Pb retrieving hat avg point  %d, ima %d duration %d",i,im0,ims-im0);

	    }
	  dss->xd[j] = rot;
	  
	  dss->yd[j] = g_r->z_cor * zavg;
	  im0 += sph->setling +sph->nper;
	}
    }



  ops = create_and_attach_one_plot(pr, nf, nf, 0);
  dss = ops->dat[0];
  set_ds_source(dss,"Status flag");      

  for (j = 0; j < nf; j++)
    {
      page_n = j/g_r->page_size;
      i_page = j%g_r->page_size;
      dss->xd[j] = (g_r->timing_mode == 1) ? (g_r->imi[page_n][i_page] - g_r->imi_start) 
					   : g_r->imi[page_n][i_page];
      dss->yd[j] = g_r->status_flag[page_n][i_page];
    }

  create_attach_select_x_un_to_op(ops, IS_SECOND, 0
				  ,(float)1/g_r->Pico_param_record.camera_param.camera_frequency_in_Hz, 0, 0, "s");
      
      
  set_op_filename(ops, "Status-%d.gr",g_r->n_rec);
  set_plot_title(ops, "Hat %d status ", g_r->n_rec);
  set_plot_x_title(ops, "Time");
  set_plot_y_title(ops, "Status");			
      
  ops->need_to_refresh = 1;



  op = create_and_attach_one_plot(pr, nf, nf, 0);
  ds = op->dat[0];
  set_ds_source(ds,"Zmag recording");      
  for (j = 0; j < nf; j++)
    {
      page_n = j/g_r->page_size;
      i_page = j%g_r->page_size;
      ds->xd[j] = (g_r->timing_mode == 1) ? (g_r->imi[page_n][i_page] - g_r->imi_start) 
					   : g_r->imi[page_n][i_page];
      ds->yd[j] = g_r->zmag[page_n][i_page];
    }
      
  if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("I can't create plot !");
  set_ds_source(ds,"Rotation recording");      
      
  for (j = 0; j < nf; j++)
    {
      page_n = j/g_r->page_size;
      i_page = j%g_r->page_size;
      ds->xd[j] = (g_r->timing_mode == 1) ? (g_r->imi[page_n][i_page] - g_r->imi_start) 
					   : g_r->imi[page_n][i_page];
      ds->yd[j] = g_r->rot_mag[page_n][i_page];
    }
  if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("I can't create plot !");
  set_ds_source(ds,"Focus recording");      

  for (j = 0; j < nf; j++)
    {
      page_n = j/g_r->page_size;
      i_page = j%g_r->page_size;
      ds->xd[j] = (g_r->timing_mode == 1) ? (g_r->imi[page_n][i_page] - g_r->imi_start) 
					   : g_r->imi[page_n][i_page];
      ds->yd[j] = g_r->obj_pos[page_n][i_page];
    }
  create_attach_select_x_un_to_op(op, IS_SECOND, 0
				  ,(float)1/g_r->Pico_param_record.camera_param.camera_frequency_in_Hz, 0, 0, "s");
      
      
  set_op_filename(op, "ZmagRotFocushat%d.gr",g_r->n_rec);
  set_plot_title(op, "Hat %d zmag, rotation & focus ", g_r->n_rec);
  set_plot_x_title(op, "Time");
  set_plot_y_title(op, "Zmag, rot, focus");			
      
  op->need_to_refresh = 1;
  switch_plot(pr, pr->n_op - 1);



  return refresh_plot(pr, pr->n_op - 1);    
}






# endif
