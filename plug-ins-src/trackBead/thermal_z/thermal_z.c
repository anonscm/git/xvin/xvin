
#ifndef _THERMAL_Z_C_
#define _THERMAL_Z_C_

# include "allegro.h"
#ifdef XV_WIN32
# include "winalleg.h"
#endif
# include "xvin.h"
# include "fftl32n.h"
# include "fillib.h"





/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "../../cfg_file/Pico_cfg.h"
# include "../../cardinal/cardinal.h"


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "../../fft2d/fftbtl32n.h"
# include "../fillibbt.h"

# include "../focus.h"
# include "../magnetscontrol.h"
# include "../trackBead.h"
# include "../brown_util.h"
# include "../track_util.h"
# include "thermal_z.h"
# include "../scan_zmag.h"
# include "../action.h"
# include "../focus.h"


float T0 = 28, Tset = 28, step_T = 2.5;
int n_Tstep = 2, time_step = 1800, modulate = 0;

int thermal_z_op_idle_action(O_p *op, DIALOG *d)
{
  // we put in this routine all the screen display stuff
  pltreg *pr = NULL;

  if (d->dp == NULL)    return 1;
  pr = (pltreg*)d->dp;        /* the image region is here */
  if (pr->one_p == NULL || pr->n_op == 0)        return 1;

  if (op->need_to_refresh)
      refresh_plot(pr_TRACK, UNCHANGED);
  return 0;
}


int thermal_z_job(int im, struct future_job *job)
{
  int imil, ci, k, mean_n[32], nf = 0, i;
  float mean_x = 0;
  //b_track *bt1 = NULL, *bt2 = NULL;;
  //d_s *ds1 = NULL, *ds2 = NULL;


  if (job == NULL || job->op == NULL || job->in_progress == 0) return 0;
  if (im < job->imi)  return 0;
  ci = track_info->c_i;
  imil = track_info->imi[ci];
  //todo = imil - job->in_progress;
  //if (track_info->n_b > 0)    bt1 = track_info->bd[0];
  //if (track_info->n_b > 1)    bt2 = track_info->bd[1];

  //if (job->op->n_dat > 0) ds1 = job->op->dat[0];
  //if (job->op->n_dat > 1) ds2 = job->op->dat[1];

  for (k = 0; k < job->bead_nb; k++)
    mean_n[k] = 0;

  while (track_info->imi[ci] > job->in_progress)
    {
      ci--;
      if (ci < 0) ci += TRACKING_BUFFER_SIZE;
    }
  while (track_info->imi[ci] < imil)
    {
      for (k = 0; k < job->bead_nb; k++)
	{
	  if (job->op->dat[k] != NULL && track_info->bd[k] != NULL)
	    {
	      if (mean_n[k] == 0)
		{
		  add_new_point_to_ds(job->op->dat[k],track_info->imi[ci],track_info->bd[k]->z[ci]);
		  mean_n[k]++;
		  nf = job->op->dat[k]->nx;
		}
	      else 
		{
		  job->op->dat[k]->xd[job->op->dat[k]->nx-1] += 
		    track_info->imi[ci];
		  job->op->dat[k]->yd[job->op->dat[k]->nx-1] += 
		    track_info->bd[k]->z[ci];
		  mean_n[k]++;
		}
	    }
	}
      ci++;
      if (ci >= TRACKING_BUFFER_SIZE) ci -= TRACKING_BUFFER_SIZE;
    }

  
  for (k = 0; k < job->bead_nb; k++)
    {
      if (job->op->dat[k] != NULL && track_info->bd[k] != NULL)
	{
	  nf = job->op->dat[k]->nx;
	  if (mean_n[k] != 0)
	    {
	      job->op->dat[k]->xd[job->op->dat[k]->nx-1] /= mean_n[k];
	      mean_x = job->op->dat[k]->xd[job->op->dat[k]->nx-1];
	      job->op->dat[k]->yd[job->op->dat[k]->nx-1] /= mean_n[k];
	    }
	}
    }


  add_new_point_to_ds(job->op->dat[job->bead_nb],mean_x,get_temperature(0));
  add_new_point_to_ds(job->op->dat[job->bead_nb+1],mean_x,get_temperature(1));

  if (modulate)
    {
      nf -= 120; 
      if ((nf % time_step) == 0)
	{
	  i = nf / time_step;
	  i %= (2*n_Tstep);
	  if (i < n_Tstep)	Tset = T0 + i * step_T;
	  else Tset = T0 + (2*n_Tstep - i) * step_T;
	  set_temperature(Tset);
	}
    }

  
  job->in_progress = imil;
  job->imi = imil + 60;
  job->op->need_to_refresh = 1;
  return 0;
}



int stop_record_thermal_z(void)
{
   pltreg *pr = NULL;
   O_p *op = NULL;
   f_job* job = NULL;

   if(updating_menu_state != 0)	return D_O_K;

   if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return D_O_K;

   job = find_job_associated_to_plot(op);
   if (job)       job->in_progress = 0;
   return D_O_K;
}

int record_thermal_z(void)
{
   int im, i;
   imreg *imr = NULL;
   O_i *oi, *oic = NULL;
   O_p *op = NULL;
   d_s *ds = NULL;

   if(updating_menu_state != 0)	return D_O_K;

   if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)	return OFF;

   i = win_scanf("Do you want a temperature modulation up and down %b\n"
		 "Starting at %8f with steps of %6f\n"
		 "number of steps %6d up before reversing\n"
		 "time in second per step %6d\n",
		 &modulate,&T0,&step_T,&n_Tstep, &time_step);
   if (i == WIN_CANCEL) return D_O_K;


   op = create_and_attach_one_plot(pr_TRACK,  16, 16, 0);
   ds = op->dat[0];
   ds->nx = ds->ny = 0;
   set_ds_source(ds, "thermal recording of bead 0");

   for (i = 1; i < track_info->n_b; i++)
     {
       if ((ds = create_and_attach_one_ds(op, 16, 16, 0)) == NULL)
	 return win_printf_OK("I can't create plot !");
       set_ds_source(ds, "thermal recording of bead %d",i);
       ds->nx = ds->ny = 0;
     }
   
   if ((ds = create_and_attach_one_ds(op, 16, 16, 0)) == NULL)
     return win_printf_OK("I can't create plot !");
   set_ds_source(ds, "Sensor temperature");
   ds->nx = ds->ny = 0;

   if ((ds = create_and_attach_one_ds(op, 16, 16, 0)) == NULL)
     return win_printf_OK("I can't create plot !");
   set_ds_source(ds, "Sample temperature");
   ds->nx = ds->ny = 0;
   
   im = track_info->imi[track_info->c_i];                                 
   
   fill_next_available_job_spot(im, im + 60, COLLECT_DATA, track_info->n_b, imr, 
				oic, pr_TRACK, op, NULL, thermal_z_job, 0);
   op->op_idle_action = thermal_z_op_idle_action;
   return D_O_K;
}


int	thermal_z_main(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  add_item_to_menu(trackBead_ex_image_menu(),"Start thermal Z", record_thermal_z,NULL,0 ,NULL);
  add_item_to_menu(trackBead_plot_menu(),"Stop thermal Z", stop_record_thermal_z,NULL,0,NULL);

  return 0;
}

# endif
