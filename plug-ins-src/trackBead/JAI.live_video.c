/*******************************************************************************
                         LPS ENS
--------------------------------------------------------------------------------

  Program      : JAI Genicam GigE_Source for bead live_videoing 
  Author       : JFA
  Date         : 27.10.2007

  Purpose      : JAI Manager Functions for XVin
  CVB          : 
  Hints        : 
      
  Updates      : 
********************************************************************************/
#ifndef _JAI_SOURCE_C_
#define _JAI_SOURCE_C_

#include "allegro.h"
#include "winalleg.h"
#include "xvin.h"

/* If you include other plug-ins header do it here*/ 
//#include "stdafx.h"
#include "Jai_Error.h"
#include "Jai_Types.h"
#include "Jai_Factory.h"
#include "JAI_2_XVIN.h"
/* But not below this define */

# define BUILDING_PLUGINS_DLL
#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)

# include "fftbtl32n.h"
# include "fillibbt.h"
//# include "PlayItSam.h"
//# include "track_util.h"
# include "JAI_live_video.h"

# include "../cfg_file/Pico_cfg.h"

//char title[512];
int k_test = 0;
// Global variable
CRITICAL_SECTION CriticalSection; 
int Delay_counter = 0;



int JAI_oi_idle_action(struct  one_image *oi, DIALOG *d)
{
  return D_O_K;
}

///////////////////////////////////////////////////////////
//                    OPEN JAI SDK HANDLES FUNCTIONS     //
///////////////////////////////////////////////////////////

# define NO_FACTORY  8

int iOpen_Factory(void)
{

  // Open the Factory
  retval = J_Factory_Open("" , &hFactory);
  if (retval != J_ST_SUCCESS)    return NO_FACTORY; //win_printf_OK("Can not open Factory");
  return 0;
}

CAM_HANDLE hCam_Open_camera(void)
{
  register int i = 0, j;
  int  index = 0;
  //char **psCameraId = NULL;
  //char question[4096] , string[2048];
  //  CAM_HANDLE hCamera = NULL;
  int nCameras = -1;
  //unsigned char bHasChanged = 0;
  int size;
  char test[512], ctest[512], ptest[512], *dev[6], *stk, *cname = NULL, *s;


  //win_printf_OK("entering hcam");
  if (hFactory == NULL) 
    {
      //win_printf_OK("Factory should be created First");
      return NULL;
    }

  // Search for cameras on all the networks
  //win_printf_OK("bef cam list");
/*   retval = J_Factory_UpdateCameraList(hFactory, &bHasChanged); */
/*   win_printf_OK("after cam list"); */
/*   if (retval == J_ST_SUCCESS && bHasChanged) */
/*     { */
      // Get the number of cameras. This number can be larger than the actual camera count because 
      // the cameras can be detected through different driver types!
      // This mean that we might need to filter the cameras in order to avoid dublicate references.
      retval = J_Factory_GetNumOfCameras(hFactory, &nCameras);
      if (nCameras <= 0)    return NULL;
      //win_printf("nCam = %d",nCameras);
      index = 0;
      size = 512;
      retval = J_Factory_GetCameraIDByIndex(hFactory, index,test, &size);
      strncpy(ctest,test,512);
      for (i = 0, stk = strtok(ctest,"::"), ptest[0] = 0; i < 6 && stk != NULL; i++, stk = strtok(NULL,"::"))
	{
	  cname = dev[i] = stk;
	  strncpy(ptest+strlen(ptest),stk,512-strlen(ptest));
	  strncpy(ptest+strlen(ptest),"\n",512-strlen(ptest));
	}
      
      //win_printf("Cam 0 has %d field \n%s",i,ptest);
      if (i > 4)
	{
	  s = dev+4;
	  for (j = 0; s[j] != 0; j++)
	    s[j] = (s[j] == '\n' || s[j] == '\r') ? 0 : s[j]; 
	  if (Pico_param.camera_param.camera_manufacturer)      free(Pico_param.camera_param.camera_manufacturer);
	  Pico_param.camera_param.camera_manufacturer = strdup(dev[4]);
	}
      if (cname != NULL) 
	{
	  s = cname;
	  for (j = 0; s[j] != 0; j++)
	    s[j] = (s[j] == '\n' || s[j] == '\r') ? 0 : s[j]; 
	  if (Pico_param.camera_param.camera_model)      free(Pico_param.camera_param.camera_model);
	  Pico_param.camera_param.camera_model = strdup(cname);
	  //win_printf("Camera 0 found %s",Pico_param.camera_param.camera_model);

	}
      //win_printf("index = %d \n test = %s",index,test);
      /* if (retval == J_ST_SUCCESS && nCameras > 0) */
/* 	{ */
/* 	  psCameraId = (char **)calloc(nCameras, sizeof(char*)); */
/* 	  for (index = 0 ; index < nCameras ; index++) */
/* 	    { */
/* 	      psCameraId[index] = (char *)calloc(512,sizeof(char));//allow only 64 char in the string */
/* 	      if (psCameraId[index] == NULL) */
/* 		{ */
/* 		  win_printf_OK("Calloc Pb for CameraStrings"); */
/* 		  return NULL; */
/* 		} */
/* 	    } */
/* 	  snprintf(question,4096,"Choose you camera \n"); */
/* 	  // Run through the list of found cameras */
/* 	  for ( index = 0; index < nCameras; index++) */
/* 	    { */
/* 	      size = sizeof(psCameraId[index]); */
/* 	      // Get CameraID */
/* 	      retval = J_Factory_GetCameraIDByIndex(hFactory, index,psCameraId[index], &size); */
/* 	      win_printf("camera i %d retval %d\nname:\n%s",index,retval,psCameraId[index]); */
/* 	      if (index == 0)retval = J_Factory_GetCameraIDByIndex(hFactory, index,test, &size); */
/* 	      win_printf("index = %d \n cameraid = %s \n test = %s",index,*(psCameraId +index),test); */
/* 		 /\*  if (retval == J_ST_SUCCESS) *\/ */
/* /\* 		    { *\/ */
/* /\* 		      if (index == 0 ) *\/ */
/* /\* 			{ *\/ */
/* /\* 			  snprintf(string , 2048,"Camera %d = %s" , index, psCameraId[index]); *\/ */
/* /\* 			  strcat(string," %R \n"); *\/ */
/* /\* 			} *\/ */
/* /\* 		      else  *\/ */
/* /\* 			{ *\/ */
/* /\* 			  snprintf(string , 2048,"Camera %d = %s " , index, psCameraId[index]); *\/ */
/* /\* 			  strcat(string," %r \n"); *\/ */
/* /\* 			} *\/ */
/* /\* 		      strcat(question,string); *\/ */
/* /\* 		    } *\/ */
/* 	    }    */
/* 	}  */
/* 	  i = win_scanf(question,&index); */
/* 	  if (i == CANCEL)  */
/* 	    { */
/* 	      win_printf_OK("END"); */
/* 	      return NULL; */
/* 	    } */
	  // Open the camera
  retval = J_Camera_Open( hFactory, test/**(psCameraId+2)*/, &hCamera);

  if (retval != J_ST_SUCCESS)
    {
      win_printf_OK("Can't open camera %d",retval);
      return NULL;
    }
  return hCamera;
}


int iOpen_JAI_DataStream_and_set_buffers_for_XVIN(O_i *oi,CAM_HANDLE hCamera)
{
  
  int i;
  int iNumber_of_DataStream = -1, in = -1, size;

  if (oi_JAI == NULL) { win_printf("No image in for set pointers"); return 1;}
  if (hCamera == NULL) { win_printf_OK("Camera should be opened first");  return 2;} 
  //First need for datastream creation
  J_Camera_GetNumOfDataStreams  ( hCamera, &iNumber_of_DataStream ); 

  //win_printf("Num data Streams %d",iNumber_of_DataStream );
  in = (iNumber_of_DataStream > 0) ? iNumber_of_DataStream - 1 : 0;
  if (J_ST_SUCCESS != J_Camera_CreateDataStream(hCamera, in , &hDS))
    win_printf("Pb creating hDS");

  JAI_Im_buffer_ID  = (void  **) calloc(oi_JAI->im.n_f, sizeof(void *));
  if (JAI_Im_buffer_ID == NULL) 
    { win_printf("Could not allocate memory for Buffer ID"); return 3;}

  for(i = 0 ; i < oi_JAI->im.n_f ; i++)
    {
      // Announce the buffer pointer to the Acquisition engine.
      size = oi_JAI->im.nx*oi_JAI->im.ny*((oi_JAI->im.data_type == IS_CHAR_IMAGE) ? 1 : 2);
      if(J_ST_SUCCESS != J_DataStream_AnnounceBuffer( hDS, oi_JAI->im.mem[i] , size, NULL, JAI_Im_buffer_ID + i))
	{   win_printf("Announce Buffer failed"); return 4;}
      // Queueing it.
      if(J_ST_SUCCESS != J_DataStream_QueueBuffer( hDS, JAI_Im_buffer_ID[i] ))
	{  win_printf("Failed in Queue buffer");  return 5; }
    }
  return 0;
}


//////////////////////////////////////////////////////////////////
//                 CLOSE JAI HANDLES FUNCTIONS                  //
/////////////////////////////////////////////////////////////////
int iClose_Camera(void)
{
  retval = J_Camera_Close  (hCamera);
  if (retval == J_ST_SUCCESS)
    return win_printf_OK("Can not close Camera");
  return D_O_K;
}
int iClose_DataStream(void)
{
  retval = J_DataStream_Close (hDS);
  if (retval == J_ST_SUCCESS)
    return win_printf_OK("Can not close DataStream");
  return D_O_K;
}
int iClose_Factory(void)
{
  // Close the factory
  J_Factory_Close(hFactory);
  if (retval == J_ST_SUCCESS)
    return win_printf_OK("Can not close Factory");
  return D_O_K;
}
////////////////////////////////////////////////////////////////////////
// END OF CLOSE JAI HANDLES FUNCTIONS                                 //
///////////////////////////////////////////////////////////////////////


//--------------------------------------------------------------------------------------------------
// Create Stream Thread
//--------------------------------------------------------------------------------------------------
BOOL CreateStreamThread(void)
{

  DWORD dwThreadId;

    // Is it already created?
    if(hStreamThread)
        return FALSE;
    // Stream thread event created?
    if(hStreamEvent == NULL)
      hStreamEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    else
      ResetEvent(hStreamEvent);
    // Set the thread execution flag
    bEnableThread = TRUE;
    hStreamThread = CreateThread( 
            NULL,              // default security attributes
            0,                 // use default stack size  
            (LPTHREAD_START_ROUTINE)StreamProcess,// thread function 
            NULL,       // argument to thread function 
            0,                 // use default creation flags 
            &dwThreadId);      // returns the thread identifier 
    if(hStreamThread == NULL) 
      {
	//      J_DataStream_Close(m_hDS);
	win_printf_OK("No thread created");
	return FALSE;
      }    
    SetThreadPriority(hStreamThread,  THREAD_PRIORITY_TIME_CRITICAL);
    return TRUE;
}


//==============================================================////
// Stream Processing Function
//==============================================================////

DWORD WINAPI ProcessTreatement(LPVOID lpParam) 
{
  Bid *p = NULL;
  uint64_t local_NumberFramesDelivered;
  long long t,dt, lTimeStamp;
  TreatementStruct * pTreatement_Struct = NULL;
  int i,im_n;
  int iWaitResult;

  pTreatement_Struct = (TreatementStruct *) lpParam;
  
  while(bEnableThread)
    {
      iWaitResult = WaitForSingleObject(hReadyEvent, 60000);
      if(WAIT_OBJECT_0 == iWaitResult/* || WAIT_OBJECT_0+1 == iWaitResult*/)
	{
	  // Request ownership of the critical section.
	  //EnterCriticalSection(&CriticalSection); 
	  // Access the shared resource.
	  //Image_to_treat = Delay_counter;
	  // Release ownership of the critical section.
	  //LeaveCriticalSection(&CriticalSection);
	  local_NumberFramesDelivered = lNumberFramesDelivered;
	  p = &bid;
	  if (p == NULL) return win_printf_OK("MERDE");
	  //lNumberFramesDelivered = pTreatement_Struct->lNumberImageDelivered ;
	  if (p->first_im == 0) p->first_im = (int) lNumberFramesDelivered;
	  lTimeStamp = pTreatement_Struct->lTimeStamp;
	  t = lTimeStamp;//my_ulclock();
	  dt = my_uclock();
	 
	  
	  p->previous_in_time_fr = t = my_ulclock();//modifier
	  p->timer_dt = 0;//idem
	  p->previous_fr_nb = (int)lNumberFramesDelivered;

	  for (i = 0 ;lNumberFramestreated < local_NumberFramesDelivered ; im_n++, i++, lNumberFramestreated++)
	    {
	      switch_frame(oi_LIVE_VIDEO,(int)lNumberFramestreated%oi_LIVE_VIDEO->im.n_f);/*A modifier en cas de retard*/
	      t = lTimeStamp;//my_ulclock();
	      dt = my_uclock();
	      p->previous_fr_nb = im_n = (int)lNumberFramestreated;

	      for (p->timer_do(imr_LIVE_VIDEO, oi_LIVE_VIDEO, im_n, d_LIVE_VIDEO, t ,dt ,p->param, i) ;
		   p->timer_do != NULL && p->next != NULL; p = p->next)
		{
		  p->timer_do(imr_LIVE_VIDEO, oi_LIVE_VIDEO,im_n, d_LIVE_VIDEO, t, dt, p->param, i);
		}
	    }

	  // Request ownership of the critical section.
	  //EnterCriticalSection(&CriticalSection); 
	  
	  // Access the shared resource.
	  //Delay_counter -= Image_to_treat;
	  // Release ownership of the critical section.
	  //LeaveCriticalSection(&CriticalSection);
	  
      	}

    }
 
  return D_O_K;
}

void StreamProcess(void)
{
  J_STATUS_TYPE	iResult;
  size_t iSize;
  BUF_HANDLE  iBufferID, iOrigin;
  // Create structure to be used for image display
  //J_tIMAGE_INFO	tAqImageInfo = {0, 0, 0, 0, NULL};
  DWORD	iWaitResult;
  HANDLE hEventNewImage = NULL;
  HANDLE	EventList[2];
  EVT_HANDLE	hEvent; // Buffer event handle
  NODE_HANDLE     hNode;
  DWORD dwThreadId = 0;
  int first = 1;
  HANDLE hTreatementThread = NULL;
  TreatementStruct TreatementStruct;
  Bid *p = NULL;
  uint64_t local_NumberFramesDelivered, b_i_n;
  long long t, dt, lTimeStamp, llt, jai_origin, ulclock_ori;
  //TreatementStruct * pTreatement_Struct = NULL;
  int i,im_n = 0, my_index, *pI, j, fr_delivered, prev_fr_delivered, i_todo, k;
  void *pBuffer;
  uint32_t pSize; 
  double min_delay_camera_thread_in_ms;


  if (imr_LIVE_VIDEO == NULL || oi_LIVE_VIDEO == NULL)	return ;

  //  if (!InitializeCriticalSectionAndSpinCount(&CriticalSection, 4000/*0x80000400*/) )     return;

  hReadyEvent = CreateEvent(NULL, TRUE, TRUE/*FALSE*/, NULL);

 /*  hTreatementThread =  CreateThread(NULL,             // default security attributes */
/* 				    0,                // use default stack size */
/* 				    ProcessTreatement,// thread function */
/* 				    (void *)&TreatementStruct,       	   // argument to thread function */
/* 				    0,                // use default creation flags */
/* 				    &dwThreadId);     // returns the thread identifier */
  
/*   if (hTreatementThread == NULL) 	win_printf_OK("No thread created"); */
/*   SetThreadPriority(hTreatementThread,  THREAD_PRIORITY_TIME_CRITICAL); */
  
  // Is the kill event already defined?
  if (hEventFreeze != NULL)    CloseHandle(hEventFreeze);
  // Create a new one
  hEventFreeze = CreateEvent(NULL, TRUE, FALSE, NULL);
  hEventNewImage = CreateEvent(NULL, TRUE, FALSE, NULL);
  // Make a list of events to be used in WaitForMultipleObjects
  
  EventList[0] = hEventNewImage;
  EventList[1] = hEventFreeze;
  
  // Register the event with the acquisition engine
  if (J_DataStream_RegisterEvent(hDS, EVENT_NEW_BUFFER, hEventNewImage, &hEvent) !=  J_ST_SUCCESS)
    { my_set_window_title("Register Failed");      return ;    } 
 
 // Start Node Acquisition
  retval = J_Camera_GetNodeByName(hCamera,"AcquisitionStart", &hNode);
  retval = J_Node_ExecuteCommand(hNode);
  
  // Start image acquisition
  iResult = J_DataStream_StartAcquisition( hDS, ACQ_START_FLAGS_NONE, 0xFFFFFFFFFFFFFFFF );
  if (iResult != J_ST_SUCCESS)
    {  my_set_window_title("StartAcquisition Failed");        return ;   }
  Sleep(1500);  // we wait to allow frequency stabilization

  J_DataStream_GetBufferID (hDS, 0, &iOrigin);

  while(bEnableThread)
    {
      // Wait for New Buffer event (or Freeze event)
      iWaitResult = WaitForMultipleObjects(2, EventList,FALSE/*TRUE*/, 10000/*00*/);
      // Did we get a new buffer event?
      if(WAIT_OBJECT_0 == iWaitResult)
	{
	  // Get the Buffer Handle from the event
	  // Request ownership of the critical section.
	  //EnterCriticalSection(&CriticalSection); 
	  // Access the shared resource.
	  //Delay_counter++;
	  // Release ownership of the critical section.
	  //LeaveCriticalSection(&CriticalSection);
	  iSize = sizeof(void *);
	  J_Event_GetData(hEvent, &iBufferID,  &iSize);
	  my_index = (int)iBufferID;
	  for (fr_delivered = 0; fr_delivered < iImages_in_JAI_Buffer; fr_delivered++)
	    if (my_index ==(int)JAI_Im_buffer_ID[fr_delivered]) break;
	  if (fr_delivered == iImages_in_JAI_Buffer)   my_set_window_title("out of range");
	  for (i_todo = 0, j = prev_fr_delivered; j != fr_delivered; i_todo++)
	    {
	      j++;
	      j = (j >= iImages_in_JAI_Buffer) ? 0 : j;
	    }
	  //if (fr_delivered == 64) continue;
	  /*
	  j = prev_fr_delivered + 1;
	  j = (j >= iImages_in_JAI_Buffer) ? 0 : j;
	  if (fr_delivered != j)
	    my_set_window_title("Missed fr %d %d diff %d", fr_delivered , j, fr_delivered - j);*/

	  //my_set_window_title("im index = %d j %d size %u",my_index-(int)JAI_Im_buffer_ID[0], j, iSize);
	  iSize = sizeof(uint64_t);
	  J_DataStream_GetStreamInfo  (hDS, STREAM_INFO_CMD_NUMBER_OF_FRAMES_DELIVERED , &lNumberFramesDelivered,  &iSize    );
	  if (first)
	    {
	      //lNumberFramestreated = lNumberFramesDelivered;
	      lNumberFramestreated = 0;
	      prev_fr_delivered = (fr_delivered + iImages_in_JAI_Buffer - 1)%iImages_in_JAI_Buffer;
	    }

	  if (i_todo > 1)
	    my_set_window_title("Missed fr %d at %d", i, (int)lNumberFramesDelivered);

	  k = (int)lNumberFramesDelivered%iImages_in_JAI_Buffer;
	  for (i = 0, j = fr_delivered; j != k; i++)
	    {
	      j++;
	      j = (j >= iImages_in_JAI_Buffer) ? 0 : j;
	    }

	  //my_set_window_title("delivered fr %d", i);
	  //attention ceci etant dans le thread l'image peut potentiellement avoir change
	  TreatementStruct.lTimeStamp = my_ulclock();//tAqImageInfo.iTimeStamp ;
	  //TreatementStruct.lNumberImageDelivered = lNumberFramesDelivered ;

	  local_NumberFramesDelivered = lNumberFramesDelivered;
	  p = &bid;
	  if (p == NULL) { win_printf("MERDE"); return;}
	  //lNumberFramesDelivered = pTreatement_Struct->lNumberImageDelivered ;
	  if (p->first_im == 0) p->first_im = (int) lNumberFramesDelivered;
	  //lTimeStamp = TreatementStruct.lTimeStamp;
	  t = my_ulclock();// lTimeStamp
	  dt = my_uclock();
	  
	  p->previous_in_time_fr = t = my_ulclock();//modifier
	  p->timer_dt = 0;//idem
	  p->previous_fr_nb = (int)lNumberFramesDelivered;
	  j = prev_fr_delivered + 1;
	  j = (j >= iImages_in_JAI_Buffer) ? 0 : j;
	  for (i = 0; i < i_todo ; im_n++, i++, lNumberFramestreated++)
	    {
	      switch_frame(oi_LIVE_VIDEO,j);
	      t = my_ulclock();// lTimeStamp
	      dt = my_uclock();
	      p->previous_fr_nb = im_n = (int)lNumberFramestreated;
              J_DataStream_GetBufferInfo(hDS, JAI_Im_buffer_ID[j], BUFFER_INFO_NUMBER, &b_i_n, &pSize); 
              J_DataStream_GetBufferInfo(hDS, JAI_Im_buffer_ID[j], BUFFER_INFO_TIMESTAMP, &llt, &pSize); 
	      // frequency at 50 MHz

	      //BUFFER_INFO_BASE  Base address of delivered buffer (void *).  
	      //BUFFER_INFO_SIZE  Size in Bytes ( size_t ).  
	      //BUFFER_INFO_USER_PTR  Private Pointer ( void * ).  
	      //BUFFER_INFO_TIMESTAMP  Timestamp (uint64_t).  
	      //BUFFER_INFO_NUMBER  Buffer Number as announced (uint64_t).  
	      //BUFFER_INFO_NEW_DATA  Flag if Buffer contains new data since it was queued.  

	      //my_set_window_title("im diff %d %d", (int)lNumberFramestreated , (int)b_i_n);
	      p->image_per_in_ms = (double)(llt -lTimeStamp)/50000;
	      Pico_param.camera_param.camera_frequency_in_Hz = (float)1000/p->image_per_in_ms;
	      //my_set_window_title("im diff %g", p->image_per_in_ms);//lNumberFramestreated , (int)b_i_n);
	      if (first)
		{
		  first = 0;
		  jai_origin = llt;
		  ulclock_ori = t;
		  p->min_delay_camera_thread_in_ms = (1000*(double)t/get_my_ulclocks_per_sec()) 
		    - (double)(llt-jai_origin)/50000;   
		}
	      else
		{
		  min_delay_camera_thread_in_ms = (1000*(double)t/get_my_ulclocks_per_sec()) 
		    - (double)(llt-jai_origin)/50000;   
		  if (min_delay_camera_thread_in_ms < p->min_delay_camera_thread_in_ms)
		    p->min_delay_camera_thread_in_ms = min_delay_camera_thread_in_ms;
		}
	      //my_set_window_title("min delay %6.3f %6.3f", p->min_delay_camera_thread_in_ms, min_delay_camera_thread_in_ms);
	      for (p->timer_do(imr_LIVE_VIDEO, oi_LIVE_VIDEO, im_n, d_LIVE_VIDEO, t ,dt ,p->param, i_todo - i - 1) ;
		   p->timer_do != NULL && p->next != NULL; p = p->next)
		{
		  p->timer_do(imr_LIVE_VIDEO, oi_LIVE_VIDEO,im_n, d_LIVE_VIDEO, t, dt, p->param,  i_todo - i - 1);
		}
	      J_DataStream_QueueBuffer(hDS, JAI_Im_buffer_ID[j]);
	      j++;
	      j = (j >= iImages_in_JAI_Buffer) ? 0 : j;
	      lTimeStamp = llt;//my_ulclock();
	    }

	  //	  PulseEvent(hReadyEvent);
	  
	  prev_fr_delivered = fr_delivered;

	  //if (j == 64) Sleep(50); test of overrun
        }
      else
        {
	  switch(iWaitResult)
            {
	      // Freeze event
	    case	WAIT_OBJECT_0 + 1:
	      iResult = 1;
	      bEnableThread = FALSE;
	      break;
	      // Timeout
	    case	WAIT_TIMEOUT:
	      iResult = 2;
	      win_printf("10s Timeout");
	      bEnableThread = FALSE;
	      break;
	      // Unknown?
	    default:
	      iResult = 3;
	      break;
            }
        }
    }
  my_set_window_title("Out loop");
 
 // Release resources used by the critical section object.
  //DeleteCriticalSection(&CriticalSection);

  // Unregister new buffer event with acquisition engine
  J_DataStream_UnRegisterEvent(hDS, EVENT_NEW_BUFFER); 
  
  // Free the event object
  if (hEvent != NULL)
    {
      J_Event_Close(hEvent);
      hEvent = NULL;
    }
   // Start Acquision
  retval = J_Camera_GetNodeByName(hCamera,"AcquisitionStop", &hNode);
  retval = J_Node_ExecuteCommand(hNode);
    // Terminate the thread.
  Terminate_JAI_Thread();
  
  // Free the kill event
    if (hEventFreeze != NULL)
      {
        CloseHandle(hEventFreeze);
        hEventFreeze = NULL;
      }
    
    // Free the new image event object
    if (hEventNewImage != NULL)
      {
        CloseHandle(hEventNewImage);
        hEventNewImage = NULL;
      }
    WaitForThreadToTerminate();
    my_set_window_title("Out thread");
}

BOOL TerminateStreamThread(void)
{
    // Is the data stream opened?
    if(hDS == NULL)        return FALSE;
    // Reset the thread execution flag.
    bEnableThread = FALSE;
    // Signal the image thread to stop faster
    SetEvent(hEventFreeze);
    // Stop the image acquisition engine
    J_DataStream_StopAcquisition(hDS, ACQ_STOP_FLAG_KILL);
    // Wait for the thread to end
    WaitForThreadToTerminate();
    return TRUE;
}

//==============================================================////
// Terminate image acquisition thread
//==============================================================////
void Terminate_JAI_Thread(void)
{
    SetEvent(hStreamEvent);
    bEnableThread = FALSE;
}

//==============================================================////
// Wait for thread to terminate
//==============================================================////

void WaitForThreadToTerminate(void)
{
    WaitForSingleObject(hStreamEvent, INFINITE);
    // Close the thread handle and stream event handle
    CloseThreadHandle();
}

//==============================================================////
// Close handles and stream
//==============================================================////
void CloseThreadHandle(void)
{
    if(hStreamThread)
    {
        CloseHandle(hStreamThread);
        hStreamThread = NULL;
    }

    if(hStreamEvent)
    {
        CloseHandle(hStreamEvent);
        hStreamEvent = NULL;
    }
}
////////////////////////////////////////////////////////////////////

int add_oi_JAI_2_imr (imreg *imr,O_i* oi)
{
  NODE_HANDLE     hNode;
  int64_t         int64Val;
  int iNx=-1, iNy=-1, iType;
  double dVal = 0;
  char ValueStr[512];
  uint32_t pSize = 512;
  float fac = 0;
  
  //win_printf ("IN");
  if ( imr == NULL ){ win_printf("IMR NULL");  return 1;}

/*   if (oi == NULL) */
      // Get Width from the camera
  if (J_Camera_GetNodeByName(hCamera, "Width", &hNode) != J_ST_SUCCESS)   return 2;
  J_Node_GetValueInt64(hNode, FALSE, &int64Val);
  iNx = (int)int64Val;     // Set window size x
  Pico_param.camera_param.nb_pxl_x = iNx;
  // win_printf("nx %d \n ny %d",iNx,iNy);
   

  // Get Height from the camera
  if (J_Camera_GetNodeByName(hCamera, "Height", &hNode) != J_ST_SUCCESS)  return 3;
  J_Node_GetValueInt64(hNode, FALSE, &int64Val);
  iNy = (int)int64Val;     // Set window size y
  Pico_param.camera_param.nb_pxl_y = iNy;
  //win_printf("nx %d \n ny %d",Pico_param.camera_param.nb_pxl_x,Pico_param.camera_param.nb_pxl_y);
  
  // Get Format from the camera
  if (J_Camera_GetNodeByName(hCamera, "PixelFormat", &hNode) != J_ST_SUCCESS)  return 4;
  J_Node_GetValueInt64  (hNode,   FALSE,  &int64Val);
  //iValue = int64Val;
  
  //      J_Node_GetEnumEntryValue  ( hNode, &iValue);
  if (int64Val == BIT_PER_PIX_8) 
    {
      iType = IS_CHAR_IMAGE; 
      //win_printf("8 bits image type");
      Pico_param.camera_param.nb_bits = 8;
    } 
  else if (int64Val == BIT_PER_PIX_10) 
    {
      iType = IS_UINT_IMAGE; 
      //win_printf("10 bits image type");
      Pico_param.camera_param.nb_bits = 10;
    } 
  //Voir si cela ne marche pas! ...et voir autres cameras values only for A10!
  else iType = IS_CHAR_IMAGE;
  //win_printf("type  %d",iType);
 

  if (J_Camera_GetNodeByName(hCamera, "AcquisitionFrameRate", &hNode) != J_ST_SUCCESS)
    { win_printf("cannot get AcquisitionFrameRate");}
  else 
    {
      J_Node_GetValueString(hNode, FALSE, ValueStr, &pSize);
      if (sscanf(ValueStr,"fps%f",&fac) == 1)
	{
	  //win_printf("AcquisitionFrameRate  %g Hz",fac);
	  Pico_param.camera_param.camera_frequency_in_Hz = fac;
	}
    }

  //create JAI image
  //  win_printf("bef create  iNx %d, iNy %d,\n iType %d, iImages_in_JAI_Buffer%d", 
  //     iNx, iNy, iType, iImages_in_JAI_Buffer);
  oi_JAI = create_and_attach_movie_to_imr(imr, iNx, iNy, iType, iImages_in_JAI_Buffer);
  if (oi_JAI == NULL)   { win_printf("Error in allocate oi_JAI"); return 5;}
  //win_printf("after create");
  oi_JAI->width  = (float)iNx/512;
  oi_JAI->height = (float)iNy/512;
  //iOpen_JAI_DataStream_for_XVIN(oi_JAI);
  set_zmin_zmax_values(oi_JAI, 0, 255);
  set_z_black_z_white_values(oi_JAI, 0, 255);
  write_Pico_config_file();
  Close_Pico_config_file();
  return 0;
}
	

int initialize_JAI(void)
{   
  int ret;

  // Open the Factory
  //win_printf("bef factory");
  my_set_window_title("Openning JAI stream factory");
  if ((ret = iOpen_Factory())) return ret + 0x10;
  //win_printf("bef hcam");
  my_set_window_title("Openning JAI camera");
  if (hCam_Open_camera() == NULL) return 0x20;
  //win_printf("after hcam");
  my_set_window_title("Camera %s found seting image ",Pico_param.camera_param.camera_model);
  if ((ret = add_oi_JAI_2_imr (imr_JAI,NULL))) return ret + 0x40;
  //win_printf("after add");
  my_set_window_title("Starting JAI camera thread");
  if ((ret = iOpen_JAI_DataStream_and_set_buffers_for_XVIN(oi_JAI,hCamera))) return ret + 0x80;
  //win_printf("after iopen");
  return 0;   

} 


int freeze_video(void)
{
  imreg *imr;
  if(updating_menu_state != 0)	return D_O_K;
  allegro_display_on = FALSE;

  TerminateStreamThread();


  imr = find_imr_in_current_dialog(NULL);  
  imr->one_i->need_to_refresh &= BITMAP_NEED_REFRESH;  
  find_zmin_zmax(imr->one_i);
  refresh_image(imr, UNCHANGED); 
  return 0;
}


int freeze_source(void)
{
  if(updating_menu_state != 0)	return D_O_K;
  freeze_video();
  return 0;
}

int JAI_live(void)
{
  int is_live = 0;
  if(updating_menu_state != 0)	return D_O_K;

  CreateStreamThread();

  is_live  = 1;
  allegro_display_on = TRUE;
  //create_display_thread();
  return D_O_K;//live_video(-1);	
}


///////////////////BOF la suite////////////////////
int kill_source(void)
{
  imreg *imr;
  if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
    {
      active_menu->flags |= D_DISABLED;
      return D_O_K;	
    }
  //else if (go_live_video == LIVE_VIDEO_ON) active_menu->flags |=  D_DISABLED;
  else active_menu->flags &= ~D_DISABLED;
  if(updating_menu_state != 0)	return D_O_K;
  go_live_video = LIVE_VIDEO_STOP;
  return D_O_K;
}

int live_source(void)
{
  if(updating_menu_state != 0)	return D_O_K;

  //is_live  = 1;
  return JAI_live();	
}


int start_data_movie(imreg *imr)
{
  return 0;
}


DWORD WINAPI LiveVideoThreadProc(LPVOID lpParam) 
{
  imreg *imr;
  tid *ltid;
  DIALOG *d;
  O_i *oi;
  Bid *p;
    
  if (imr_LIVE_VIDEO == NULL || oi_LIVE_VIDEO == NULL)	return 1;

  ltid = (tid*)lpParam;

  imr = ltid->imr;
  d = ltid->dimr;
  oi = ltid->oi;
  p = ltid->dbid;
 
  live_source();		

  return 0;
}

  
MENU *JAI_source_image_menu(void)
{
  static MENU mn[32];
    
  if (mn[0].text != NULL)	return mn;

  add_item_to_menu(mn,"Live JAI", JAI_live ,NULL,0,NULL);
  add_item_to_menu(mn,"Freeze JAI", freeze_video ,NULL,0,NULL);
    
  return mn;
    
}


int init_image_source(imreg *imr, int mode)
{
  int ret;
  O_i *oi = NULL;

  //if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
  if (imr == NULL)    imr = create_and_register_new_image_project( 0,   32,  900,  668);
  if (imr == NULL)    return win_printf_OK("Patate t'es mal barre!");
  imr_JAI = imr;
  oi = imr->one_i;
  if ((ret = initialize_JAI())) 
    {
      win_printf_OK("Your Giga Ethernet video camera did not start!");
      return ret; // Initializes JAI 
    }
  if (mode == 1)      remove_from_image (imr, oi->im.data_type, (void *)oi);
  add_image_treat_menu_item ( "JAI Genicam", NULL, JAI_source_image_menu(), 0, NULL);
  return 0;
}

int JAI_source_main(void)
{
  imreg *imr = NULL;
  O_i *oi = NULL;
  
    

  imr = find_imr_in_current_dialog(NULL);
  if (imr == NULL)    init_image_source(NULL,0);
  else
    {
        if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)
	  init_image_source(imr,0);
	if (oi->im.movie_on_disk == 0 || oi->im.n_f < 2)
	  init_image_source(imr,0);

    }
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)
    return win_printf_OK("You must load a movie in the active window");

  d_JAI = find_dialog_associated_to_imr(imr_JAI, NULL);
  before_menu_proc = JAI_before_menu;
  after_menu_proc = JAI_after_menu;
  oi_JAI->oi_got_mouse = JAI_oi_got_mouse;
  //  oi_JAI->oi_idle_action = JAI_oi_idle_action;
  oi_JAI->oi_mouse_action = JAI_oi_mouse_action;

  general_end_action = source_end_action;
  //  general_idle_action = source_idle_action;

   add_image_treat_menu_item ( "JAI Genicom", NULL, JAI_source_image_menu(), 0, NULL); // JAI user menus
       
  return D_O_K;
}

/** Unload function for the  JAI.c menu.*/
int	JAI_unload(void)
{
  remove_item_to_menu(image_treat_menu, "JAI Genicam", NULL, NULL);
  return D_O_K;
}



#endif

/*******************************************************************************/
