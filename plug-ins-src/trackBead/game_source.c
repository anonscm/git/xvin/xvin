/*
 *    Plug-in module allowing to track bead from a movie
 *
 *    V. Croquette
 *    JF Allemand
 */
#ifndef _GAME_SOURCE_C_
#define _GAME_SOURCE_C_



# include "allegro.h"
#ifdef XV_WIN32
# include "winalleg.h"
# include <windowsx.h>
#endif
# include "ctype.h"
# include "xvin.h"
# include "../nrutil/nrutil.h"

XV_FUNC(int, do_load_im, (void));


# define BUILDING_PLUGINS_DLL
# include "../cfg_file/Pico_cfg.h"
# include "../wlc/wlc.h"
# ifndef TRACK_WIN_THREAD
# include <pthread.h>
# endif
#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)

//# include "movie_source.h"
# include "focus.h"
# include "../fft2d/fftbtl32n.h"
# include "fillibbt.h"
# include "trackBead.h"
# include "track_util.h"
# include "magnetscontrol.h"
# include "game_source.h"
# ifndef TRACK_WIN_THREAD
# include "timer_thread.h"
# endif
//# define BURST_ANGLE

//# define TEST_LED_MOD


/*

   Pico_param.dna_molecule.dsDNA_molecule_extension;
   Pico_param.dna_molecule.dsxi;

   Pico_param.bead_param.radius;
   */

int add_bead_SDI_calib_image(O_i *oi, float xcf, float ycf, float zcf, float dx, O_i *oic);


O_i *calibration_image = NULL;
O_i *SDI_calibration_image = NULL;

float x_bead = 15, y_bead = 20, z_bead = 5, L0 = 5, z_obj = 0;
float  x_bn = 0, y_bn = 0, z_bn = 0, x_b0 = 15, y_b0 = 20; // bead noise
float  x_bnm = 0, y_bnm = 0, z_bnm = 0; // noise averaged over one frame
float  x_bn2 = 0, y_bn2 = 0, z_bn2 = 0, x_b02 = 40, y_b02 = 25; // bead noise
float  x_bn2m = 0, y_bn2m = 0, z_bn2m = 0; // noise averaged over one frame
static float x_b[1024], y_b[1024];
int bead_type[1024];
int ns_bead = 0;
bool random_placement = false;

int co_set = 0;
float co_dl_ld[2048];
float my_sqrt[10000];
float my_exp_1[512];
# define POISSON_MAK_K 20
float  LED_fp[POISSON_MAK_K], LED_sfp[POISSON_MAK_K];

// illumination modulation


unsigned char im_grey_level = 128;
/*
   float read_Z_value(void)
   {
   return z_obj;
   }
   int set_Z_value(float z)
   {
   z_obj = z;
   return 0;
   }
   */



float DNA_extension(void)
{
    /*
       static float prev_zmag_1 = 0;
       static float prev_mag_rot_1 = 0;


       if (prev_zmag_1 != prev_zmag  || prev_mag_rot_1 != prev_mag_rot)
       {
       convert_zmag_to_force(prev_zmag, prev_mag_rot, 0);
       prev_zmag_1 = prev_zmag;
       prev_mag_rot_1 = prev_mag_rot;
       }
       */
    return mean_extension;
}


int freeze_source(void)
{
    imreg *imr = NULL;

    if (ac_grep(cur_ac_reg, "%im", &imr) != 1)
    {
        if (active_menu) active_menu->flags |=  D_DISABLED;

        return D_O_K;
    }
    //else if (go_track == TRACK_FREEZE) active_menu->flags |=  D_DISABLED;
    else
    {
        if (active_menu) active_menu->flags &=  ~D_DISABLED;
    }

    if (updating_menu_state != 0)    return D_O_K;

    go_track = TRACK_FREEZE;
    return D_O_K;
}


int live_source(void)
{
    imreg *imr = NULL;

    if (ac_grep(cur_ac_reg, "%im", &imr) != 1)
    {
        if (active_menu) active_menu->flags |= D_DISABLED;

        return D_O_K;
    }
    //else if (go_track == TRACK_ON) active_menu->flags |=  D_DISABLED;
    else
    {
        if (active_menu) active_menu->flags &= ~D_DISABLED;
    }

    if (updating_menu_state != 0)    return D_O_K;

    go_track = TRACK_ON;
    return D_O_K;
}
int kill_source(void)
{
    imreg *imr = NULL;

    if (ac_grep(cur_ac_reg, "%im", &imr) != 1)
    {
        if (active_menu) active_menu->flags |= D_DISABLED;

        return D_O_K;
    }
    //else if (go_track == TRACK_ON) active_menu->flags |=  D_DISABLED;
    else
    {
        if (active_menu) active_menu->flags &= ~D_DISABLED;
    }

    if (updating_menu_state != 0)    return D_O_K;

    go_track = TRACK_STOP;
    return D_O_K;
}

int prepare_image_overlay(O_i *oi)
{
    (void)oi;
    return 0;
}



// function executed in the tracking thread
int add_bead_talbot_image(O_i *oi,                         // the image to put the bead in
                          float xcf, float ycf, float zcf, // bead position in microns
                          float dx,                        // the extend of one pixel in microns
                          float lambda,         // the light wavelentgth in microns
                          float l_d,            // the coherence decay length of light in microns
                          float Id,             // the ratio of diffuse light
                          float theta_max,      // the maximum angle that the light ray can have
                          int rc)               // the radius of the bead
{
    register int i, j;
    float x, y, x2, y2, z2, r, z, l, dl, It, Imin, Ic, pxl, rcp, lambda_100, l_d_64;
    int onx, ony, xci, yci, xis, xie, yis, yie, in;
    union pix  *pd = NULL;

    Imin = cos(M_PI * theta_max / 180);

    if (oi == NULL) return 1;

    onx = oi->im.nx;
    ony = oi->im.ny;

    if (co_set == 0)
    {
        for (i = 0; i < 512; i++)
            my_exp_1[i] = exp(-((float)i) / 64);

        for (i = 0; i < 2048; i++)
            co_dl_ld[i] = 2 * cos((M_PI * (float)i) / 100);

        for (i = 0; i < 10000; i++)
            my_sqrt[i] = sqrt((float)i / 16);
    }

    xci = (int)((xcf / dx) + .5);
    xis = xci - 99;
    xis = (xis < 0) ? 0 : xis;
    xis = (xis < onx) ? xis : onx - 1;
    xie = xci + 100;
    xie = (xie < 0) ? 0 : xie;
    xie = (xie < onx) ? xie : onx;
    yci = (int)((ycf / dx) + .5);
    yis = yci - 99;
    yis = (yis < 0) ? 0 : yis;
    yis = (yis < ony) ? yis : ony - 1;
    yie = yci + 100;
    yie = (yie < 0) ? 0 : yie;
    yie = (yie < ony) ? yie : ony;

    rcp = rc * dx;
    lambda_100 = (float)100 / lambda;
    l_d_64 = (float)64 / l_d;
    z = zcf;
    z2 = 16 * z * z; // 3*z*z is interesting

    for (i = yis, pd = oi->im.pixel; i < yie ; i++)
    {
        y = fabs(ycf - (i * dx));
        y2 = 16 * y * y; // + 0.5
        y2 += 0.5; // for integer rounding

        for (j = xis; j < xie; j++)
        {
            x = fabs(xcf - (j * dx));               // x and y are from pixel to bead center in microns
            //r = sqrt(x*x + y*y);                   // r is distance from pixel to bead center in microns
            x2 = 16 * x * x;
            r = my_sqrt[(int)(x2 + y2)]; // r is distance from pixel to bead center in microns
            //k = (int)(r/dx);                             // its integer part
            Ic = (r < rcp) ? 0 : 1;                  //
            //r = dx * r;                             // this is the distance in microns
            //l = sqrt(z*z + r*r);
            l = my_sqrt[(int)(z2 + x2 + y2)]; // this is the optical ray path
            It = (l == 0) ? 1 : fabs(z / l);        // Id*cos \theta, simulate the decay of diffusion
            It = (It < Imin) ? 0 : It;
            dl = l - fabs(z);                       // the difference of ray travel
            //It *= Id * exp(-dl/l_d);                // It now contains deifuse light level
            It *= Id * my_exp_1[(int)((dl * l_d_64) + .5)];
            pxl = Ic - It;
            in = (int)(0.5 + dl * lambda_100);
            in = (in < 2048) ? in : 2047;
            in = (in < 0) ? 0 : in;
            It *= co_dl_ld[in];
            //It *= 2 * cos(M_PI*dl/lambda);//co_dl_ld[in]; //cos(M_PI*dl/lambda);
            pxl += It;
            pd[i].ch[j] = (unsigned char)(pxl * 130);
        }
    }

    return 0;
}






// function executed in the tracking thread
int add_bead_calib_image(O_i *oi,                         // the image to put the bead in
                         float xcf, float ycf, float zcf, // bead position in microns
                         float dx,                        // the extend of one pixel in microns
                         O_i *oic)               // the bead calibration image
{
    register int i, j, k;
    float x, y, x2, y2, r, z, dyc, impf, fr;
    int onx, ony, xci, yci, xis, xie, yis, yie, ocnx, ocny, zc0, zc1;
    union pix  *pd = NULL, *pdc = NULL;
# ifdef BURST_ANGLE
    float ys = 3; // bust to measure angle , xb
    float yb, yb2, xb2; // z2;
# endif

    if (oi == NULL) return 1;

    onx = oi->im.nx;
    ony = oi->im.ny;
    ocnx = oic->im.nx;
    ocny = oic->im.ny;
    dyc = oic->dy;

    if (co_set == 0)
    {
        for (i = 0; i < 512; i++)
            my_exp_1[i] = exp(-((float)i) / 64);

        for (i = 0; i < 2048; i++)
            co_dl_ld[i] = 2 * cos((M_PI * (float)i) / 100);

        for (i = 0; i < 10000; i++)
            my_sqrt[i] = sqrt((float)i / 16);
    }

    xci = (int)((xcf / dx) + .5);
    xis = xci - ocnx / 2;
    xis = (xis < 0) ? 0 : xis;
    xis = (xis < onx) ? xis : onx - 1;
    xie = xci + ocnx / 2;
    xie = (xie < 0) ? 0 : xie;
    xie = (xie < onx) ? xie : onx;
    yci = (int)((ycf / dx) + .5);
    yis = yci - ocny / 2;
    yis = (yis < 0) ? 0 : yis;
    yis = (yis < ony) ? yis : ony - 1;
    yie = yci + ocny / 2;
    yie = (yie < 0) ? 0 : yie;
    yie = (yie < ony) ? yie : ony;

    z = (zcf < 0) ? 0 : zcf / dyc; // the position in y in the calibration image
    zc0 = floor(z);
    fr = z - zc0;    // the fractional part
    zc0 = (zc0 < ocny - 1) ? zc0 : ocny - 2;
    zc0 = (zc0 < 0) ? 0 : zc0;
    //printf("sim calib :%d/%d\n", zc0, oic->im.ny);
    zc1 = zc0 + 1;
    zc1 = (zc1 < ocny) ? zc1 : ocny - 1;

    // z2 = 4*z*z; // 3*z*z is interesting
    for (i = yis, pd = oi->im.pixel, pdc = oic->im.pixel; i < yie ; i++)
    {
        y = fabs(ycf - (i * dx));
# ifdef BURST_ANGLE
        yb = fabs(y - ys);     // bust angle
        yb2 = 140 * yb * yb;        // bust angle
# endif
        y2 = 16 * y * y; // +0.5
        y2 += 0.5;

        for (j = xis; j < xie; j++)
        {
            x = fabs(xcf - (j * dx));               // x and y are from pixel to bead center in microns
            //r = sqrt(x*x + y*y);                   // r is distance from pixel to bead center in microns
# ifdef BURST_ANGLE
            xb2 = 70 * x * x;        // bust angle
# endif
            x2 = 16 * x * x;
            r = my_sqrt[(int)(x2 + y2)]; // r is distance from pixel to bead center in microns
            r /= dx; // this is the distance in pixels
            k = (int)r;

            if (k < ocnx / 2)
            {
                k += ocnx / 2;
                impf = (1 - fr) * pdc[zc0].fl[k] + fr * pdc[zc1].fl[k];
# ifdef BURST_ANGLE

                if ((yb2 + xb2) < zb)
                {
                    impf += zb - xb2 - yb2; // bust angle
                    //my_set_window_title("bump %g",zb - xb2 - yb2);
                }

# endif
                impf = (impf < 255) ? impf : 255;

                if (random_placement)
                {
                    pd[i].ch[j] = (unsigned char)(impf < pd[i].ch[j] ? impf : pd[i].ch[j]);
                }
                else
                {
                    pd[i].ch[j] = (unsigned char)impf;

                }
            }
            else continue; //pd[i].ch[j] = im_grey_level;
        }
    }

    return 0;
}




// function executed in the tracking thread
int add_bead_SDI_calib_image(O_i *oi,                         // the image to put the bead in
                         float xcf, float ycf, float zcf, // bead position in microns
			     float dx, O_i *oic)               // the bead calibration image
{
    register int i, j, k;
    float x, y, z, ay, dyc, impf, fr, frx, amp;
    int onx, ony, xci, yci, xis, xie, yis, yie, ocnx, ocny, zc0, zc1;
    union pix  *pd = NULL, *pdc = NULL;

    if (oi == NULL) return 1;

    onx = oi->im.nx;
    ony = oi->im.ny;
    ocnx = oic->im.nx/2;
    ocny = oic->im.ny;
    dyc = oic->dy;
    ay = oic->ay;

    if (co_set == 0)
    {
        for (i = 0; i < 512; i++)
            my_exp_1[i] = exp(-((float)i) / 64);

        for (i = 0; i < 2048; i++)
            co_dl_ld[i] = 2 * cos((M_PI * (float)i) / 100);

        for (i = 0; i < 10000; i++)
            my_sqrt[i] = sqrt((float)i / 16);
    }

    xci = (int)((xcf / dx) + .5);
    xis = xci - ocnx / 2;
    xis = (xis < 0) ? 0 : xis;
    xis = (xis < onx) ? xis : onx - 1;
    xie = xci + ocnx / 2;
    xie = (xie < 0) ? 0 : xie;
    xie = (xie < onx) ? xie : onx;
    yci = (int)((ycf / dx) + .5);
    yis = yci - ocnx / 4;
    yis = (yis < 0) ? 0 : yis;
    yis = (yis < ony) ? yis : ony - 1;
    yie = yci + ocnx / 4;
    yie = (yie < 0) ? 0 : yie;
    yie = (yie < ony) ? yie : ony;

    z = (zcf < ay) ? 0 : (zcf - ay) / dyc; // the position in y in the calibration image
    z = (z < ocny) ? z : ocny-1;
    zc0 = floor(z);
    fr = z - zc0;    // the fractional part
    zc0 = (zc0 < ocny - 1) ? zc0 : ocny - 2;
    zc0 = (zc0 < 0) ? 0 : zc0;
    //printf("sim calib :%d/%d\n", zc0, oic->im.ny);
    zc1 = zc0 + 1;
    zc1 = (zc1 < ocny) ? zc1 : ocny - 1;

    // z2 = 4*z*z; // 3*z*z is interesting
    for (i = yis, pd = oi->im.pixel, pdc = oic->im.pixel; i < yie ; i++)
    {
      y = fabs((ycf/dx) - i);
      y -= ocnx/8;
      if ((y < -ocnx/16) || (y > ocnx/16))
	{
	  for (j = xis; j < xie; j++)
	    pd[i].ch[j] = 0;
	  continue;
	}
      amp = 1+cos(M_PI*y/(ocnx/16));
      amp /= 10.4; // to be changed;
      for (j = xis; j < xie-1; j++)
	{
	  x = (xcf/dx) - j;               // x and y are from pixel to bead center in microns
	  k = floor(x);
	  frx = x - k;
	  k = j - xis;
	  if (i < yci)
	    {
	      impf = (1 - frx) * ((1 - fr) * pdc[zc0].fl[k] + fr * pdc[zc1].fl[k]);
	      impf += frx * ((1 - fr) * pdc[zc0].fl[k+1] + fr * pdc[zc1].fl[k+1]);
	    }
	  else
	    {
	      impf = (1 - frx) * ((1 - fr) * pdc[zc0].fl[ocnx+k] + fr * pdc[zc1].fl[ocnx+k]);
	      impf += frx * ((1 - fr) * pdc[zc0].fl[ocnx+k+1] + fr * pdc[zc1].fl[ocnx+k+1]);
	    }
	  impf *= amp;
	  impf = (impf < 255) ? impf : 255;
	  if (random_placement)
	    {
	      pd[i].ch[j] = (unsigned char)(impf < pd[i].ch[j] ? impf : pd[i].ch[j]);
	    }
	  else
	    {
	      pd[i].ch[j] = (unsigned char)impf;
	    }
	}
    }
  return 0;
}





// function executed in the tracking thread
#define IA 16807
#define RIM 2147483647
//#define AM (1.0/RIM)
#define IQ 127773
#define IR 2836
#define MASK 123459876
// was long
int ran0(int idum) // Minimal random number generator of Park and Miller.
// Returns a uniform random deviate between 0 and 2^31 - 1
{
    int k;
    idum ^= MASK;                 // XORing with MASK allows use of zero and other simple bit patterns for
    k = (idum) / IQ;              //idum.
    idum = IA * (idum - k * IQ) - IR * k; // Compute idum=(IA*idum) % IM without over

    if (idum < 0) idum += RIM;     //flows by Schrage�s method.

    //ans=AM*(*idum); Convert idum to a floating result.
    idum ^= MASK;                //Unmask before return.
    return idum;
}

// function executed in the tracking thread
int simulate_video_noise(O_i *oi, int idum)
{
    register int i, j;
    int onx, ony;
    int l = 0;

    if (oi == NULL)  return 1;

    onx = oi->im.nx;
    ony = oi->im.ny;

    for (i = 0; i < ony; i++)
    {
        for (j = 0; j < onx; j++)
        {
	    if (SDI_calibration_image != NULL)
	      oi->im.pixel[i].ch[j] = 4;
	    else
	      {
		if (j % 15 == 0) l = idum = ran0(idum);
		oi->im.pixel[i].ch[j] = im_grey_level + ((l & 0x3) << 2);
		l >>= 2;
	      }
        }
    }

    return idum;
}


// function executed in the tracking thread
# ifdef TRACK_WIN_THREAD
// function executed in the tracking thread
VOID CALLBACK dummyAPCProc(LPVOID lpArgToCompletionRoutine,
                           DWORD dwTimerLowValue,  DWORD dwTimerHighValue)
{
    (void)lpArgToCompletionRoutine;
    (void)dwTimerLowValue;
    (void)dwTimerHighValue;
    return;
}


DWORD WINAPI TrackingThreadProc(LPVOID lpParam)
# else
void *TrackingThreadProc(void *lpParam)
# endif
{
# ifdef TRACK_WIN_THREAD
    HANDLE hTimer = NULL;
    LARGE_INTEGER liDueTime;
    tid *ltid = NULL;
# else
    timer_thread_t hTimer;
    long int liDueTime = 0;
    tid *ltid = (tid *) lpParam;
# endif
    //imreg *imr;
    DIALOG *d = NULL;
    O_i *oi = NULL;
    long int j = 0;
    int nf, im_n = 0, i;//, l;//, norm = 0;//, nb;
    unsigned int dt = 0, t0 = 0, t1 = 0, dtmax = 0;
    long long t;
    //double xnoisep, ynoisep, znoisep;
    //DIALOG *starting_one = NULL;
    Bid *p = NULL;
    //float tmp;

# ifdef TRACK_WIN_THREAD

    if (imr_TRACK == NULL || oi_TRACK == NULL)  return 1;

    liDueTime.QuadPart = -300000; // 30 ms
    ltid = (tid *)lpParam;
# else

    if (imr_TRACK == NULL || oi_TRACK == NULL)  return (void *) 1;

    liDueTime = 60; // 30 ms
# endif

    //imr = ltid->imr;
    d = ltid->dimr;
    oi = ltid->oi;
    p = ltid->dbid;
    nf = abs(oi->im.n_f);
    // Create a waitable timer.
# ifdef TRACK_WIN_THREAD
    hTimer = CreateWaitableTimer(NULL, TRUE, "WaitableTimer2");

    if (!hTimer)   win_printf_OK("CreateWaitableTimer failed (%d)\n", GetLastError());

# else

    if (create_timer(&hTimer, liDueTime) != 0)
        win_printf_OK("CreateTimer failed\n");

# endif
    t0 = my_uclock();
    //starting_one = active_dialog; // to check if windows change
    source_running = 1;

# ifdef TRACK_WIN_THREAD

    // Set a timer to wait for 10 seconds.
    if (!SetWaitableTimer(hTimer, &liDueTime, 0, dummyAPCProc, NULL, 0))
        win_printf_OK("SetWaitableTimer failed (%d)\n", GetLastError());

# endif

    for (i = 0, j = 1; i < POISSON_MAK_K ; i++)
    {
        if (i == 0)
        {
            LED_fp[i] = exp(-LED_mod_freq);
            LED_sfp[i] = LED_fp[i];
        }
        else
        {
            j *= i;
            LED_fp[i] = pow(LED_mod_freq, i) * exp(-LED_mod_freq) / j;
            LED_sfp[i] = LED_sfp[i - 1] + LED_fp[i];
        }
    }


    while (go_track)
    {

        // Wait for the timer.
# ifdef TRACK_WIN_THREAD
        SleepEx(INFINITE, TRUE);
        t1 = my_uclock();

        // Set a timer to wait for 10 seconds.
        if (!SetWaitableTimer(hTimer, &liDueTime, 0, dummyAPCProc, NULL, 0))
            win_printf_OK("SetWaitableTimer failed (%d)\n", GetLastError());

# else
        wait_timer(&hTimer);
        t1 = my_uclock();

        if (create_timer(&hTimer, liDueTime) != 0)
            win_printf_OK("CreateTimer failed\n");

#endif
        dt = t1 - t0;

        if (dt > dtmax) dtmax = dt;

        t0 = t1;

        if (go_track == TRACK_FREEZE) continue;

        oi->im.c_f++;

        if (oi->im.c_f >= nf) oi->im.c_f = 0;


        t0 = t1;

        if (IS_DATA_IN_USE(oi))   continue;

        SET_DATA_IN_USE(oi);
        dt_simul = my_uclock();
        //xnoisep = xnoise /oi->dx;
        //ynoisep = ynoise /oi->dy;

        generate_simulation_image(oi);

        SET_DATA_NO_MORE_IN_USE(oi);
        dt_simul = my_uclock() - dt_simul;
        oi->need_to_refresh |= BITMAP_NEED_REFRESH;
        //display_image_stuff_16M(imr,d); done idle refresh
        //snprintf(running_message,128,"simul %6.3f ms",1000*(double)(dt_simul)/get_my_uclocks_per_sec());

        snprintf(running_message, 128, "x_bnm %g xbead %g", x_bnm, x_bead);
        t0 = my_uclock();
        d->d1 = (int)dt;

        t = my_ulclock();
        dt = my_uclock();
        bid.previous_fr_nb = im_n;

        do
        {
            p->timer_do(imr_TRACK, oi_TRACK, im_n, d_TRACK, t , dt , p->param, 0);
            p = p->next;

        }
        while (p && p->timer_do != NULL);

        p = ltid->dbid;

        im_n++;
        oi->need_to_refresh |= INTERNAL_BITMAP_NEED_REFRESH;

    }

    source_running = 0;
    return 0;
}

void generate_simulation_image(O_i *oi)
{
    static int idum = 87;
    int tmp;
    int l;
    int norm = 0;

    idum = simulate_video_noise(oi, idum);
    for (int nb = 0; nb < ns_bead; nb++)
    {
        for (int i = 0, x_bnm = y_bnm = z_bnm = 0, norm = 0; i < OVER_SAMPLING; i++)
        {
            x_bn = x_bn * betax + (float)(xnoise * gasdev(&idum));
            y_bn = y_bn * betay + (float)(ynoise * gasdev(&idum));
            z_bn = z_bn * betaz + (float)(znoise * gasdev(&idum));

            if (LED_mod_type == SINE_LED_MOD)
            {
                x_bnm += x_bn;
                x_bnm += cos(M_PI * 2 * i * LED_mod_freq / OVER_SAMPLING) * (x_bn * LED_mod) / 100 ;
                y_bnm += y_bn;
                y_bnm += cos(M_PI * 2 * i * LED_mod_freq / OVER_SAMPLING) * (y_bn * LED_mod) / 100 ;
                z_bnm += z_bn;
                z_bnm += cos(M_PI * 2 * i * LED_mod_freq / OVER_SAMPLING) * (z_bn * LED_mod) / 100 ;
            }
            else if (LED_mod_type == PULSE_LED_MOD)
            {
                if (i <= LED_pulse_w)
                {
                    x_bnm += x_bn;
                    y_bnm += y_bn;
                    z_bnm += z_bn;
                }
            }
            else if (LED_mod_type == NO_LED_MOD)
            {
                x_bnm += x_bn;
                y_bnm += y_bn;
                z_bnm += z_bn;
            }
            else if (LED_mod_type == POISSON_LED_MOD)
            {
                for (l = 0, tmp = ran1(&idum); l < POISSON_MAK_K && tmp > LED_sfp[l]; l++);

                tmp = l * LED_mod;
                tmp /= 100;
                norm += l;
                x_bnm += tmp * x_bn;
                y_bnm += tmp * y_bn;
                z_bnm += tmp * z_bn;
            }
        }

        if (LED_mod_type == PULSE_LED_MOD)
        {
            x_bnm /= LED_pulse_w;
            y_bnm /= LED_pulse_w;
            z_bnm /= LED_pulse_w;
        }
        else if (LED_mod_type == POISSON_LED_MOD)
        {
            x_bnm /= norm;
            y_bnm /= norm;
            z_bnm /= norm;
        }
        else
        {
            x_bnm /= OVER_SAMPLING;
            y_bnm /= OVER_SAMPLING;
            z_bnm /= OVER_SAMPLING;
        }

        //draw_bubble(screen, B_LEFT, 512, 100, "x %g y %g z %g",x_bnm,y_bnm,z_bnm);

        x_bead = x_b[nb] + x_bnm;
        y_bead = y_b[nb] + y_bnm;
        //z_bead = L0 + read_last_Z_value() + z_bn;  // z_obj
        z_bead = DNA_extension();

        if (track_info != NULL && track_info->focus_cor > 0)  z_bead /= track_info->focus_cor;

        z_bead = read_last_Z_value() + z_bnm - z_bead;  // z_obj


        if (bead_type[nb] == 0)
        {
            x_bead = x_b[nb];// + x_bn2m;
            y_bead = y_b[nb];// + x_bn2m;
            z_bead = read_last_Z_value();  // z_obj
        }



        if (track_info != NULL && nb < track_info->n_b)
        {
            track_info->bd[nb]->xt[track_info->c_i] = x_bead;
            track_info->bd[nb]->yt[track_info->c_i] = y_bead;
            track_info->bd[nb]->zt[track_info->c_i] = z_bead;
        }

        //z_bead = L0 + z_obj + z_bn;
        //add_bead_talbot_image(oi, x_bead, y_bead, z_bead, 0.1, 0.45, 1.5, 0.3, 70, 0);
        if (SDI_calibration_image != NULL)
	  add_bead_SDI_calib_image(oi, x_bead, y_bead, z_bead, 0.1, SDI_calibration_image);
        else if (calibration_image == NULL)
            add_bead_talbot_image(oi, x_bead, y_bead, z_bead, 0.1, 0.45, 1.5, 0.3, 70, 0);
        else
            add_bead_calib_image(oi, x_bead, y_bead, z_bead, 0.1, calibration_image);

        //set_im_title(oi,"bead %d xbead %g ybead %g",nb, x_bead, y_bead);
        //if (nb == 0)  my_set_window_title("bead %d x_b[0] %g y_b[0] %g xbead %g ybead %g",nb,x_b[0],y_b[0], x_bead, y_bead);

    }
}

int dna_ext(void)
{
    if (updating_menu_state != 0)          return D_O_K;

    win_printf("Force %f \nextension %f", applied_force, DNA_extension());
    return D_O_K;
}




int _set_camera_gain(float gain, float *set, float *fmin, float *fmax, float *finc)
{
    int  max = 200;

    if (gain > 4)       gain = 4;

    if (gain < 0.707)   gain = 0.707;

    if (fmin) *fmin = 0.707;

    if (fmax) *fmax = 4;

    if (finc) *finc = (float)4 / max;

    camera_gain = gain;

    if (set) *set = gain;

    return 0;
}



int _get_camera_gain(float *gain, float *fmin, float *fmax, float *finc)
{
    if (gain) *gain = camera_gain;

    if (fmin) *fmin = 0.707;

    if (fmax) *fmax = 4;

    if (finc) *finc = (float)16 / 200;

    return 0;
}




int _set_camera_shutter_in_us(int exp, int *set, int *smin, int *smax)
{
    int min, max;

    min = 130;
    max = 33333;

    if (exp == 0)
    {
        LED_mod_type = 0;  // no shutter

        if (set) *set = max;
    }
    else
    {
        LED_mod_type = PULSE_LED_MOD;  // simple pulse shutter

        if (exp > max) exp = max;

        if (exp < min) exp = min;

        LED_pulse_w = (exp * OVER_SAMPLING) / 33333;

        if (set) *set = (LED_pulse_w * 33333) / OVER_SAMPLING;
    }

    if (smin) *smin = min;

    if (smax) *smax = max;

    return 0;
}

int _get_camera_shutter_in_us(int *set, int *smin, int *smax)
{
    if (smin) *smin = 130;

    if (smax) *smax = 33333;

    if (set) *set = (LED_pulse_w * 33333) / OVER_SAMPLING;

    return 0;
}







int mod_params(void)
{
# ifdef TEST_LED_MOD
    int i, j;
# endif

    if (updating_menu_state != 0)          return D_O_K;

# ifdef TEST_LED_MOD

    win_scanf("Led modulation parameter\n"
              "What kind of modulation do you want:\n"
              "None %R Single pulse %r Sinus %r Poisson noise %r\n"
              "For pulse specify width %6d/100 \n"
              "For sinus or Poisson noise define\n"
              "freqency of bursts %8f \nand amplitude %8d/100\n"
              , &LED_mod_type, &LED_pulse_w, &LED_mod_freq, &LED_mod);

    for (i = 0, j = 1; i < POISSON_MAK_K ; i++)
    {
        if (i == 0)
        {
            LED_fp[i] = exp(-LED_mod_freq);
            LED_sfp[i] = LED_fp[i];
        }
        else
        {
            j *= i;
            LED_fp[i] = pow(LED_mod_freq, i) * exp(-LED_mod_freq) / j;
            LED_sfp[i] = LED_sfp[i - 1] + LED_fp[i];
        }
    }

    if (LED_sfp[POISSON_MAK_K - 1] < 0.999)
        win_printf("sfp[%d] = %g", POISSON_MAK_K - 1, LED_sfp[POISSON_MAK_K - 1]);

# endif
    return D_O_K;
}


/*
   MENU *simulation_image_menu(void)
   {
   static MENU mn[32];

   if (mn[0].text != NULL)  return mn;
   add_item_to_menu(mn,"Move up", move_obj_up,NULL,0,NULL);
   add_item_to_menu(mn,"Move down", move_obj_dwn,NULL,0,NULL);
   add_item_to_menu(mn,"DNA extension", dna_ext,NULL,0,NULL);
   return mn;
   }
   */


int    do_row_tweezers_move(O_i *oi, int n_row, DIALOG *d)
{
    static int old_y_pos = -1;

    (void)d;

    if (old_y_pos == n_row) return 0;

    if (track_info == NULL || track_info->op_t == NULL)  return D_O_K;

    if (track_info->n_op_t > 0)
        track_info->op_t[track_info->n_op_t - 1]->xc = (float)n_row;

    oi->need_to_refresh &= ~PLOTS_NEED_REFRESH;
    old_y_pos = n_row;
    return D_O_K;

}


int    do_line_tweezers_move(O_i *oi, int n_line, DIALOG *d)
{
    static int old_x_pos = -1;

    (void)d;

    if (old_x_pos == n_line) return 0;

    if (track_info == NULL || track_info->op_t == NULL) return D_O_K;

    if (track_info->n_op_t > 0)
        track_info->op_t[track_info->n_op_t - 1]->yc = (float)n_line;

    oi->need_to_refresh &= ~PLOTS_NEED_REFRESH;
    old_x_pos = n_line;
    return D_O_K;

}

int    do_tweezers_move_right(void)
{

    if (updating_menu_state != 0) return 0;

    if (track_info == NULL || track_info->op_t == NULL) return D_O_K;

    if (track_info->n_op_t > 0)
        track_info->op_t[track_info->n_op_t - 1]->xc += 1;

    return 0;

}

int    do_tweezers_move_left(void)
{

    if (updating_menu_state != 0) return 0;

    if (track_info == NULL || track_info->op_t == NULL) return D_O_K;

    if (track_info->n_op_t > 0)
        track_info->op_t[track_info->n_op_t - 1]->xc -= 1;

    return 0;

}

int    do_tweezers_move_up(void)
{

    if (updating_menu_state != 0) return 0;

    if (track_info == NULL || track_info->op_t == NULL) return D_O_K;

    if (track_info->n_op_t > 0)
        track_info->op_t[track_info->n_op_t - 1]->yc += 1;

    return 0;

}

int    do_tweezers_move_down(void)
{

    if (updating_menu_state != 0) return 0;

    if (track_info == NULL || track_info->op_t == NULL) return D_O_K;

    if (track_info->n_op_t > 0)
        track_info->op_t[track_info->n_op_t - 1]->yc -= 1;

    return 0;

}


int create_tweezers(void)
{
    int i;
    static float xc = 10, yc = 10;
    static int l = 32, w = 8;
    static float stx = 10, sty = 10, stz = 10;
    static float dx = 0.5, dy = 0.5, dz = 1;


    if (updating_menu_state != 0)
    {
        add_keyboard_short_cut(0, KEY_RIGHT, 0, do_tweezers_move_right);
        add_keyboard_short_cut(0, KEY_LEFT, 0, do_tweezers_move_left);
        add_keyboard_short_cut(0, KEY_UP, 0, do_tweezers_move_up);
        add_keyboard_short_cut(0, KEY_DOWN, 0, do_tweezers_move_down);
    }

    if (updating_menu_state != 0 || track_info == NULL)      return D_O_K;




    i = win_scanf("Create an optical tweezers\n"
                  "cros length %6d width %6d\n"
                  "position in X %8f Y %8f\n"
                  "extension in X %8f Y %8f Z %8f\n"
                  "strength in X %8f Y %8f Z %8f\n",
                  &l, &w, &xc, &yc, &dx, &dy, &dz, &stx, &sty, &stz);

    if (i == WIN_CANCEL) return 0;

    if (track_info->op_t == NULL)
    {
        track_info->op_t = (op_tweezers **)calloc(sizeof(op_tweezers *), 5);

        if (track_info->op_t == NULL)
            return win_printf_OK("Cannot allocate optical tweezers array");
    }

    if (track_info->n_op_t >= 4)
        return win_printf_OK("Two many optical tweezers");

    track_info->op_t[track_info->n_op_t] = (op_tweezers *)calloc(sizeof(op_tweezers), 1);

    if (track_info->op_t[track_info->n_op_t] == NULL)
        return win_printf_OK("Cannot allocate optical tweezers");

    track_info->op_t[track_info->n_op_t]->xc = xc;
    track_info->op_t[track_info->n_op_t]->yc = yc;
    track_info->op_t[track_info->n_op_t]->dx = dx;
    track_info->op_t[track_info->n_op_t]->dy = dy;
    track_info->op_t[track_info->n_op_t]->dz = dz;
    track_info->op_t[track_info->n_op_t]->cr_l = l;
    track_info->op_t[track_info->n_op_t]->cr_w = w;
    track_info->op_t[track_info->n_op_t]->strength_x = stx;
    track_info->op_t[track_info->n_op_t]->strength_y = sty;
    track_info->op_t[track_info->n_op_t]->strength_z = stz;
    track_info->n_op_t++;
    //vert_marker_action = do_line_tweezers_move;
    //horz_marker_action = do_row_tweezers_move;

    return D_O_K;
}


MENU *op_tweezers_image_menu(void)
{
    static MENU mn[32] = {0};

    if (mn[0].text != NULL) return mn;

    add_item_to_menu(mn, "create tweezers", create_tweezers, NULL, 0, NULL);
    return mn;
}


int Define_SDI(void)
{
    static int idum = 87;
  int i;
    imreg *imr = NULL;
    O_i *oi = NULL;

    if (updating_menu_state != 0)    return D_O_K;
    if (ac_grep(cur_ac_reg, "%im%oi", &imr,&oi) != 2)
    {
        return D_O_K;
    }
    i = win_printf("if you press Ok I will use the current image as an SDI calibration");
    if (i == WIN_CANCEL) return D_O_K;
    SDI_calibration_image = oi;
    idum = simulate_video_noise(oi_TRACK, idum);
    return D_O_K;

}


int init_image_source(imreg *imr1, int mode)
{
    register int i, j = 0, k;
    imreg *imr = NULL;
    O_i *oi = NULL;
    static int ony = 500;
    static int onx = 600;
    static int classic = 1;
    static int pitch = 120;
    static int is_random = 0;
    static int random_seed = 42;
    static int nbead = 100;
    int onx2, nc = -1, ii;
    int  l = 0;// idum = 45,
    float dx = 0.1, tmp;
    char *cs = NULL;

    (void)imr1;
    (void)mode;
    i = win_scanf("This will simulate the bead movement\n"
                  "Click here %b to produce the classical 2 beads systemas in in the help menu\n"
                  "Otherwise you can select a larger camera and more beads\n"
                  "In this last case specify :\n"
                  "The camera size in pixel in x %8d in y %8d\n"
                  "The distance between beads in pixels  %8d\n"
                  "%b random placement.\n"
                  "  Number of bead %8d\n"
                  "  Seed %8d\n (negative value use a time related value)"
                  , &classic, &onx, &ony, &pitch, &is_random, &nbead, &random_seed);


    //add_image_treat_menu_item ( "Optical tweezers", NULL, op_tweezers_image_menu(), 0, NULL);
    add_image_treat_menu_item ( "Define SDI", Define_SDI, NULL, 0, NULL);
    if ((imr = find_imr_in_current_dialog(NULL)) == NULL)
    {
        imr = create_and_register_new_image_project(0,   32,  900,  668);
        j = 1;
    }
    else
    {
        for (i = 0; i < imr->n_oi; i++)
        {
            if (imr->o_i[i] == NULL) continue;

            cs = (imr->o_i[i]->im.source != NULL) ? imr->o_i[i]->im.source
                 : imr->o_i[i]->im.treatement;

            if (cs == NULL) continue;

            if (strncmp(cs, "equally spaced reference profile", 32) == 0)
            {
                calibration_image = imr->o_i[i];
                onx2 = imr->o_i[i]->im.nx - 1;

                for (k = 0, tmp = 0; k < imr->o_i[i]->im.ny; k++)
                    tmp += imr->o_i[i]->im.pixel[k].fl[onx2];

                tmp /= imr->o_i[i]->im.ny;
                im_grey_level = (unsigned char)(tmp + 0.5);
                //win_printf_OK("tmp %f grey %d",tmp,(int)im_grey_level);
                nc = i;
            }
        }
    }

    if (imr == NULL)    win_printf_OK("could not create imreg");

    oi = create_and_attach_movie_to_imr(imr, onx, ony, IS_CHAR_IMAGE, 1);

    if (oi == NULL) return win_printf_OK("cannot create profile !");

    if (j == 1)  remove_from_image(imr, imr->o_i[0]->im.data_type, (void *)imr->o_i[0]);


    if (nc >= 0 && imr->n_oi > 1)
    {
        imr->n_oi--;

        for (i = nc ; i < imr->n_oi ; i++)
            imr->o_i[i] = imr->o_i[i + 1];

        if (imr->cur_oi >= j && imr->cur_oi > 0)imr->cur_oi--;

        imr->one_i = imr->o_i[imr->cur_oi];
        imr->one_i->need_to_refresh |= PLOTS_NEED_REFRESH;
    }

    select_image_of_imreg(imr, imr->n_oi - 1);
    oi->im.source = strdup("Dummy beads");

    for (i = 0, l = 0; i < ony; i++)
    {
        for (j = 0; j < onx; j++)
        {
            //if (j%15 == 0) l = idum = ran0(idum);
            oi->im.pixel[i].ch[j] = im_grey_level + (l & 0x3);
            l >>= 2;
        }
    }

    oi->width = (float)onx / 512;
    oi->height = (float)ony / 512;
    oi->iopt2 &= ~X_NUM;
    oi->iopt2 &= ~Y_NUM;
    /*
       x_bead = 20; y_bead = 15; z_bead = 2;x1
       add_bead_talbot_image(oi, x_bead, y_bead, z_bead, 0.1, 0.6, 3, 0.1, 70, 0);
       */


    if (classic)
    {
        x_b[0] = x_b0;
        y_b[0] = y_b0;
        x_bead = x_b0;
        y_bead = y_b0;
        z_bead = read_last_Z_value() - DNA_extension();

        if (SDI_calibration_image != NULL)
	  add_bead_SDI_calib_image(oi, x_bead, y_bead, z_bead, 0.1, SDI_calibration_image);
        else if (calibration_image == NULL)
            add_bead_talbot_image(oi, x_bead, y_bead, z_bead, 0.1, 0.45, 1.5, 0.3, 70, 0);
        else add_bead_calib_image(oi, x_bead, y_bead, z_bead, 0.1, calibration_image);

        bead_type[0] = 1;

        x_b[1] = x_b02;
        y_b[1] = y_b02;
        x_bead = x_b02;
        y_bead = y_b02;
        z_bead = read_last_Z_value();

        if (SDI_calibration_image != NULL)
	  add_bead_SDI_calib_image(oi, x_bead, y_bead, z_bead, 0.1, SDI_calibration_image);
        else if (calibration_image == NULL)
            add_bead_talbot_image(oi, x_bead, y_bead, z_bead, 0.1, 0.45, 1.5, 0.3, 70, 0);
        else add_bead_calib_image(oi, x_bead, y_bead, z_bead, 0.1, calibration_image);

        bead_type[1] = 0;
        ns_bead = 2;
    }
    else if (is_random)
    {
        random_placement = is_random;

        if (random_seed < 0)
        {
            random_seed = time(NULL);
        }

        srand(random_seed);

        for (int kl = 0; kl < nbead; ++kl)
        {
            x_b[kl] = x_bead = (rand() % onx) * 0.1;
            y_b[kl] = y_bead = (rand() % ony) * 0.1;
            z_bead = read_last_Z_value() - DNA_extension();

	    if (SDI_calibration_image != NULL)
	      add_bead_SDI_calib_image(oi, x_bead, y_bead, z_bead, 0.1, SDI_calibration_image);
	    else if (calibration_image == NULL)
                add_bead_talbot_image(oi, x_bead, y_bead, z_bead, 0.1, 0.45, 1.5, 0.3, 70, 0);
            else
                add_bead_calib_image(oi, x_bead, y_bead, z_bead, 0.1, calibration_image);

            bead_type[kl] = 1;
        }

        bead_type[0] = 0;
        ns_bead = nbead;

    }
    else
    {
        for (ii = 0, i = pitch / 2, k = 0; i < (ony - pitch / 2); i += pitch, ii++)
        {
            for (j = (ii % 2 + 1) * pitch / 2; j < (onx - pitch / 2); j += pitch)
            {
                x_b[k] = x_bead = ((float)j) * 0.1;
                y_b[k] = y_bead = ((float)i) * 0.1;
                z_bead = read_last_Z_value() - DNA_extension();

		if (SDI_calibration_image != NULL)
		  add_bead_SDI_calib_image(oi, x_bead, y_bead, z_bead, 0.1, SDI_calibration_image);
		else if (calibration_image == NULL)
                    add_bead_talbot_image(oi, x_bead, y_bead, z_bead, 0.1, 0.45, 1.5, 0.3, 70, 0);
                else
                    add_bead_calib_image(oi, x_bead, y_bead, z_bead, 0.1, calibration_image);

                bead_type[k] = 1;
                k++;
            }
        }

        ns_bead = k;
        bead_type[0] = 0;
    }



    //  find_zmin_zmax(oi);
    if (SDI_calibration_image != NULL)
    {
        set_zmin_zmax_values(oi, SDI_calibration_image->z_min, SDI_calibration_image->z_max);
        set_z_black_z_white_values(oi, SDI_calibration_image->z_black, SDI_calibration_image->z_white);
    }
    else if (calibration_image)
    {
        set_zmin_zmax_values(oi, calibration_image->z_min, calibration_image->z_max);
        set_z_black_z_white_values(oi, calibration_image->z_black, calibration_image->z_white);
    }
    else
    {
        set_zmin_zmax_values(oi, 50, 150);
        set_z_black_z_white_values(oi, 50, 150);
    }

    oi->need_to_refresh = ALL_NEED_REFRESH;
    create_attach_select_x_un_to_oi(oi, IS_METER, 0, dx, -6, 0, "\\mu m");
    create_attach_select_y_un_to_oi(oi, IS_METER, 0, dx, -6, 0, "\\mu m");

    get_camera_shutter_in_us = _get_camera_shutter_in_us;
    set_camera_shutter_in_us = _set_camera_shutter_in_us;
    get_camera_gain = _get_camera_gain;
    set_camera_gain = _set_camera_gain;

    // select_image_of_imreg(imr, imr->n_oi -1);
    //broadcast_dialog_message(MSG_DRAW,0);
    //switch_project_to_this_imreg(imr);
    broadcast_dialog_message(MSG_DRAW, 0);
    //  refresh_image(imr, imr->n_oi - 1);
    //  starting_one = active_dialog; // to check if windows change
    //index_menu_dialog = retrieve_item_from_dialog(d_menu_proc, NULL);
    //index_menu_dialog = 1;


    /*
    # ifdef TEST_LED_MOD
    add_item_to_menu(trackBead_ex_image_menu(),"Led modulation", mod_params,NULL,0 ,NULL);
    add_item_to_menu(trackBead_plot_menu(),"Led modulation", mod_params,NULL,0,NULL);
    win_printf("adding menu");
    # endif
    */


    return 0;
}
int start_data_movie(imreg *imr)
{
    (void)imr;
    return 0;
}



# endif
