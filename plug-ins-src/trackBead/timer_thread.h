#pragma once
#include <pthread.h>
#include "platform.h"
typedef pthread_t timer_thread_t;

typedef struct {
    unsigned long int time_interval;
    int (*callback)(void);
} call_timer_t;

PXV_FUNC(int, create_timer, (timer_thread_t *hTimer, unsigned long int dueTime));
PXV_FUNC(int, create_call_timer, (timer_thread_t *hTimer, unsigned long int time_interval, int (*callback)(void)));

PXV_FUNC(int, wait_timer, (timer_thread_t *hTimer));
PXV_FUNC(int, kill_timer, (timer_thread_t *hTimer));

PXV_FUNC(void, psleep, (unsigned long int milliseconds));
