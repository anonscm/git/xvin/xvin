#ifndef _BROWN_UTIL_H_
#define _BROWN_UTIL_H_

#include "xvin.h"
#include "../fft2d/fftbtl32n.h"
#include "fillibbt.h"

# define 	WRONG_ARGUMENT		-1
# define 	OUT_MEMORY			-2
# define 	FFT_NOT_SUPPORTED	-15
# define	PB_WITH_EXTREMUM	2
# define	TOO_MUCH_NOISE		4
# define	GETTING_REF_PROFILE	1

# define        NEW

# define PROFILE_BUFFER_SIZE 128


# define        clip_X_cross_in_image(oi, x, y, l2, w2) \
		(x) = ((x) < (oi)->im.nx - (l2) - (w2)) ? (x) : (oi)->im.nx - (l2) - (w2);\
		(y) = ((y) < (oi)->im.ny - (l2) - (w2)) ? (y) : (oi)->im.ny - (l2) - (w2);\
		(x) = ((x) < (l2)+(w2)) ? (l2)+(w2) : (x);\
		(y) = ((y) < (l2)+(w2)) ? (l2)+(w2) : (y)

# define        clip_cross_in_image(oi, x, y, l2) \
		(x) = ((x) < (oi)->im.nx - (l2)) ? (x) : (oi)->im.nx - (l2);\
		(y) = ((y) < (oi)->im.ny - (l2)) ? (y) : (oi)->im.ny - (l2);\
		(x) = ((x) < (l2)) ? (l2) : (x);\
		(y) = ((y) < (l2)) ? (l2) : (y)


# define        is_cross_in_image(oi, x, y) \
		(((x) < (oi)->im.nx) && ((y) < (oi)->im.ny)\
                 && ((x) > 0) && ((y) > 0)) ? 1 : 0


PXV_ARRAY(float, fx1);

# define NB_FRAMES_BEF_LOST 8

#ifdef NEW
PXV_FUNC(int,	fill_avg_profiles_from_im, (O_i *oi, int xc, int yc, int cl, int cw, int *x, int *y, float *x_var, float *y_var));
PXV_FUNC(int,	fill_X_avg_profiles_from_im, (O_i *oi, int xc, int yc, int cl, int cw, int *x, int *y, float *x_var, float *y_var));
PXV_FUNC(int,	fill_avg_profiles_from_im_diff, (O_i *oi, int xc, int yc, int cl, int cw, int *x, int *y, int *xd, int *yd
						 ,int *x_var, int *y_var, int *xd_var, int *yd_var));
PXV_FUNC(int,	fill_X_avg_profiles_from_im_diff, (O_i *oi, int xc, int yc, int cl, int cw, int *x, int *y, int *xd, int *yd,
						   int *x_var, int *y_var, int *xd_var, int *yd_var));
#else
PXV_FUNC(int,	fill_avg_profiles_from_im, (O_i *oi, int xc, int yc, int cl, int cw, int *x, int *y));
PXV_FUNC(int,	fill_X_avg_profiles_from_im, (O_i *oi, int xc, int yc, int cl, int cw, int *x, int *y));
PXV_FUNC(int,	fill_avg_profiles_from_im_diff, (O_i *oi, int xc, int yc, int cl, int cw, int *x, int *y, int *xd, int *yd));
PXV_FUNC(int,	fill_X_avg_profiles_from_im_diff, (O_i *oi, int xc, int yc, int cl, int cw, int *x, int *y, int *xd, int *yd));
#endif



PXV_FUNC(double, avg_im_on_square_area, (O_i *oi, int frame, int xc, int yc, int cl));

PXV_FUNC(int,  defftwindow_flat_top1,(fft_plan *fp, int npts, float *x, float smp));

PXV_FUNC(int,	check_bead_not_lost, (int *x, float *x1, int *y, float *y1, int cl, float bd_mul));
PXV_FUNC(int,	check_bead_not_lost_diff, (int *x, int *xd, float *x1, int *y, int *yd, float *y1, int cl, float bd_mul, int do_diff));
PXV_FUNC(int, erase_around_black_circle, (int *x, int *y, int cl));
PXV_FUNC(int,     deplace, (float *x, int nx));
PXV_FUNC(int,     fftwindow_flat_top1, (fft_plan *fp, int npts, float *x, float smp));
PXV_FUNC(int,     reset_mouse, (void));
PXV_FUNC(int,	find_max1, (float *x, int nx, float *Max_pos, float *Max_val, float *Max_deriv, int max_limit));

PXV_FUNC(int,	correlate_1d_sig_and_invert, (float *x1, int nx, int window, float *fx1, int lfilter,
					      int remove_dc, fft_plan *fp, filter_bt *fil, float *noise));


# ifdef PIAS


PXV_FUNC(int,	correlate_1d_sig_and_invert_lp_and_bp,(float *x1, int nx, int window, float *fx1, int lp_filter, int bp_filter, int bp_width, int remove_dc, fft_plan *fp, filter_bt *fillp, filter_bt *filbp, float *noise));

PXV_FUNC(int,	correlate_1d_sig_and_invert_bp_and_complexify,(float *x1, int nx, int window, float *fx1, float *fcx1, int bp_filter, int bp_width, int remove_dc, fft_plan *fp, filter_bt *filbp, float *noise));

PXV_FUNC(int,	correlate_1d_sig_and_invert_lp_and_complexify,(float *x1, int nx, int window, float *fx1, float *fcx1, int lp_filter, int remove_dc, fft_plan *fp, filter_bt *fillp, float *noise));




# endif

PXV_FUNC(int, prepare_filtered_profile_lphp, (float *zp, int nx, int flag, int lpfilter, int lw, int hpfilter, int hw, fft_plan *fp, filter_bt *fil));


PXV_FUNC(float,	find_distance_from_center, (float *x1, int cl, int flag, int lfilter, int black_circle,
					    float *corr, float *Max_deriv, fft_plan *fp, filter_bt *fil, float *noise, int max_limit));
PXV_FUNC(float,	find_distance_from_center_2, (float *x1, float *fx1, int cl, int flag, int lfilter, int black_circle,
					    float *corr, float *Max_deriv, fft_plan *fp, filter_bt *fil, float *noise, int max_limit));
PXV_FUNC(int,	draw_cross_in_screen_unit, (int xc, int yc, int length, int width, int color, BITMAP* bmp,
					    int sc, int bead_nb, int fixed, char *zpos, int zcolor));

PXV_FUNC(int,	draw_X_cross_in_screen_unit, (int xc, int yc, int length, int width, int color, BITMAP* bmp,
					    int sc, int bead_nb, int fixed, char *zpos, int zcolor));

PXV_FUNC(int,	draw_rectangle_in_screen_unit, (int xc, int yc, int width, int height, int color, BITMAP* bmp,
						int scale, int bead_nb));

PXV_FUNC(int,	draw_circle_in_screen_unit, (int xc, int yc, int radius, int color, BITMAP* bmp, int scale));


PXV_FUNC(int,	draw_moving_shear_rect_vga_screen_unit, (int xt, int yt, int xb, int yb, int nfx, int nfy,
							 int maxdx, int dfy0, int dym, int color, int scale, int bead_nb, BITMAP* bmp, float* quality_factor));



PXV_FUNC(int, fft_band_pass_in_real_out_complex, (float *x, int nx, int w_flag, int fc, int width, fft_plan *fp, filter_bt *fil));
PXV_FUNC(float, find_phase_shift_between_profile, (float *zr, float *zp, int npc, int flag, int lfilter, int width,
						   int rc, fft_plan *fp, filter_bt *fil));

PXV_FUNC(O_i*, image_band_pass_in_real_out_complex, (O_i *dst, O_i *oi,int w_flag, int lfilter, int width,
						     int rc, fft_plan *fp, filter_bt *fil));

PXV_FUNC(O_i*, image_lowpass_and_highpass_in_real_out_complex, (O_i *dst, O_i *oi, int w_flag, int lpfilter, int lw, int hpfilter, int hw, int rc, fft_plan *fp, filter_bt *fil));



PXV_FUNC(int, find_z_profile_by_mean_square_and_phase, (float *zp, int nx, O_i *oir, int w_flag, int lfilter,
							int width, int rc, float *z, int *profil_index, float *min_c,
							float *min_phi, fft_plan *fp, filter_bt *fil,  float *z_tmp_prof));


PXV_FUNC(int, find_z_profile_by_mean_square_and_phase_lphp, (float *zp, int nx, O_i *oir, int w_flag, int lfilter, int lw, int hpfilter, int hw, int rc, float *z, int *profil_index, float *min_c, float *min_phi, fft_plan *fp, filter_bt *fil, float *z_tmp_prof));



PXV_FUNC(float*, radial_non_pixel_square_image_sym_profile_in_array, (float *rz, O_i *ois, float xc, float yc,
								      int r_max, float ax, float ay));

PXV_FUNC(float*, radial_non_pixel_square_image_sym_profile_in_array_sse, (float *rz, O_i *ois, float xc, float yc,
									  int r_max, float ax, float ay));


PXV_FUNC(float*, mean_y_profile_in_array, (float *rz, O_i *ois, float xc, float yc, int r_max, float ax, float ay));

PXV_FUNC(float*, mean_x_profile_in_array, (float *rz, O_i *ois, float xc, float yc, int r_max, int r_max_y, float ax, float ay));

PXV_FUNCPTR(float*, radial_non_pixel_square_image_sym_profile_in_array_ptr, (float *rz, O_i *ois, float xc, float yc,
									     int r_max, float ax, float ay));

PXV_FUNC(float *, orthoradial_non_pixel_square_image_sym_profile_in_array, (float *orz, O_i *ois, float xc, float yc,
									    int r_mean, int r_width, int orz_size, float ax, float ay));


PXV_FUNC(int, change_mouse_to_cross,(int cl, int cw));
PXV_FUNC(int, reset_mouse, (void));


PXV_FUNC(int, switch_radial_sse,(void));
PXV_FUNC(int, switch_radial_std,(void));
PXV_FUNC(int, switch_radial_apo, (void));
# ifdef _BROWN_UTIL_C_

int rm_low_modes_in_auto_convolution = 0;
int noise_from_auto_conv = 2;
int auto_conv_norm = 0;
int filter_after_conv = 1;

# else
PXV_VAR(int, rm_low_modes_in_auto_convolution);
PXV_VAR(int, noise_from_auto_conv);
PXV_VAR(int, auto_conv_norm);
PXV_VAR(int, filter_after_conv);

# endif

# endif
