#ifndef _REAL_TIME_AVG_H_
#define _REAL_TIME_AVG_H_

# define live_video_info real_time_info

# define REAL_TIME_BUFFER_SIZE 4096


# define BUF_FREEZED 2
# define BUF_CHANGING 1
# define BUF_UNLOCK 0


# define USER_MOVING_OBJ     0x00000010 
# define OBJ_MOVING          0x00000020 
# define H_LEVELS            2560
# define H_FACTOR            10
# define BP_LEVELS           640
# define BP_FACTOR           10
# define BP_DSL_LEVELS       5000
# define BP_DSL_FACTOR       100

typedef struct gen_real_time
{
  int (*user_real_time_action)(struct gen_real_time *gt, DIALOG *d, void *data);  // a user callback function       
  int c_i, n_i, m_i, lc_i;          // c_i, index of the current image, n_i size of buffer, m_i size allocated
  int mc_i;                         // mc_i, index of the current averaged image (oidm)
  int ac_i, lac_i;                  // ac_i actual number of frame since acquisition started + local
  int rc_i;                         // rc_i number of frames since last bp histo reset
  int ac_ri, a_ri;                  // the absolute and relative index of the last pending request
  int ac_pri, a_pri;                // the absolute and relative index of the previous pending request
  int imi[REAL_TIME_BUFFER_SIZE];                // the image nb
  int imit[REAL_TIME_BUFFER_SIZE];               // n_inarow
  long long imt[REAL_TIME_BUFFER_SIZE];          // the image absolute time
  long long imt0;
  long long imtt[REAL_TIME_BUFFER_SIZE];         // the image absolute time obtained by timer 
  unsigned long imdt[REAL_TIME_BUFFER_SIZE];     // the time spent in the previous function call
  unsigned long imtdt[REAL_TIME_BUFFER_SIZE];    // the time spent before image flip in timer mode
  float obj_pos[REAL_TIME_BUFFER_SIZE];          // the objective position measured

  int status_flag[REAL_TIME_BUFFER_SIZE];
  float obj_pos_cmd[REAL_TIME_BUFFER_SIZE];      // the objective position command
  char temp_message[REAL_TIME_BUFFER_SIZE];      // a string containing temperature message 
  float avg_pxl[REAL_TIME_BUFFER_SIZE];          // one pixel value versus time
  int x_pxl;
  int y_pxl;
  int **raw_single_histo;                        // raw histo of all img in video buffer
  int raw_histo[256];                            // raw histo of current video buffer
  int raw_histotmp[256];
  int raw_histo_update;
  int raw_min;
  int raw_wi;

  int nf_autocorr;                               // nb of frmae for mean filtered movie oidm

  float bp_bl;                                   // black level bp
  float bp_wi;                                   // black width bp
  int bp_nx, bp_ny;                              // nb of small img for bp treatment
  int histo_bpd[BP_LEVELS];                      // histo of band-passed image
  int histo_bpd_tmp[BP_LEVELS];                  // use this one in non rt thread
  int histo_bpd_update;
  int histo_bpf[BP_LEVELS];                      // histo of band-passed image
  int histo_bpf_tmp[BP_LEVELS];                  // use this one in non rt thread
  int histo_bpf_update;
  int reset_histo_bp;
  long long *dur_since_last;                     // duration since last time rms was 
                                                 // above bl_bp+w_bp (has length nf 
                                                 // ie nb of frame of oifb)
  int histo_dsl[BP_DSL_LEVELS];
  int histo_dsl_tmp[BP_DSL_LEVELS];
  int histo_dsl_update;
  int reset_histo_dsl;
/*   unsigned long t_last_im[BP_LEVELS/BP_FACTOR]; */

  float focus_cor;                               // the immersion correction factor in Z
  int im_focus_check;                            // the number of images between two ocus read per
  int com_skip_fr;                               // communication rate may be different from camera
                                                 // this is the ratio between the two
  O_p *op_bp_histo, *op_tl;
  O_i *oim, *oibg, *oif, *oifb, *oic, *oid, *oidm;
  int rot;                                       // orientation of the beam
  int xdc, ydc, xfc, yfc;                        // beam center coordinates
  int xd0, yd0, xf0, yf0;
  int wx, wy;
  
  int fsize, fwidth;
  int f_nf;
  //int x0, y0, wx, wy;                          // interest region
  float lp, lw, hp, hw;

  int do_rm_bg;

  int is_dv;
  int is_tl_on;                                  // point measure or timelapse
  int tl_add_point;                              // flag to trigger tl plot update
  int duration;                                  // total duration in min
  long long t_ini;                               // time at time lapse start
  long long tacq_start;                          // time at start of current measurement
  long long tacq_end;                            // time at end of current measurement
  int max_tacq;                                  // max period during 2 measurements
  float max_prec;                                // max precision threshold for update
  int redundancy;                                // how many time a particle appears
                                                 // in histo (used in error estimation)
  int stir;                                      // remote stirring control flag
  int stirActive, delayActive;
  int stirStop, delayStop;
  int max_tstir, max_tdelay;
  long long tstir_start, tdelay_start;
} g_real_time;



# ifdef _REAL_TIME_AVG_C_
g_real_time *real_time_info = NULL;
int px0,px1,py0,py1;
float intensity[REAL_TIME_BUFFER_SIZE];
float prev_focus = 0;

int fft_used_in_real_timeing = 0;
int fft_used_in_dialog = 0;
# else
PXV_VAR(g_real_time*, real_time_info);
PXV_VAR(int, px0);
PXV_VAR(int, px1);
PXV_VAR(int, py0);
PXV_VAR(int, py1);
PXV_VAR(int,x_center);
PXV_VAR(int,y_center);
PXV_VAR(float, prev_focus);

PXV_VAR(int, fft_used_in_real_time);
PXV_VAR(int, fft_used_in_dialog);


# endif

PXV_FUNC(g_real_time *, creating_live_video_info, (void));

PXV_FUNC(MENU *, real_time_avg_menu, (void));
# endif



