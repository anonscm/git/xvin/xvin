
/**
\file ni_6503.c

This file defines control functions for NI 6503 card to be used with C Hermann controller.

\author Adrien Meglio
\author JF Allemand

\date 02/07/07
*/

#ifndef NI_6503_C 
#define NI_6503_C

#include <allegro.h>
#include <winalleg.h>
#include <xvin.h>
#include <windows.h>
#include <stdio.h>

#include "ni_6503.h"

//#include "nidaqcns.h"
//#include <NIDAQmx.h>
#include "nidaq.h"

/************************CARD INITIALIZATION (LOW-LEVEL)*************************/

int detect_NI_boards(void)
{	
  short int infovalue = 0;
  short int i;
  int status;
    
  if(updating_menu_state != 0)	return D_O_K;	
  
  NI_6503 = 0;
  NI_6602 = 0;
 
 for (i=1;i<3;i++)
    {
      status = Init_DA_Brds (i,&infovalue);
      //win_printf("Init board number %d",(int)infovalue);
      if (infovalue==PCI_6503)
	{
	  NI_6503=i;
	  //win_printf(" Board %d is DAQ6503",i);
	}
      else if (infovalue==PCI_6602)
	{
	  NI_6602=i;
	  //win_printf(" Board %d is Counter 6602",i);
	}
      
      else 
	{ 
	  //win_printf("No board number %d",i);
	  //NI_6602 = 1;
	}
    }
  if (NI_6503 == 0) 
    {
      win_printf("NI 6503 could not be initialized");
      //return 0;
    }
  /*status = Get_DAQ_Device_Info (NI_6602, ND_DEVICE_TYPE_CODE		, &infoValue);
    win_printf("Counter on board=%d (ND_AM9513 ?)",infovalue);*/ 
  
  //win_report("Value of NI_6503 : %d",NI_6503);
  
  return D_O_K;
}

int IO__is_6503_present(int* flag)
{
  if(updating_menu_state != 0)	return D_O_K;
  
  //detect_NI_boards();
  
  /* *flag = NI_6503; */
  if (NI_6503 !=0) 
    {
      *flag = 1;
    }
  else 
    {
      *flag = 0;
    }

  //win_event("NI 6503 status requested.\nValue given was : %d with NI 6503 value %d",*flag,NI_6503);

  return D_O_K;
}

/* int IO__is_6602_present(int* flag) */
/* { */
/*   if(updating_menu_state != 0)	return D_O_K; */
  
/*   //detect_NI_boards(); */
  
/*   /\* *flag = NI_6602; *\/ */
/*   if (NI_6602 !=0)  */
/*     { */
/*       *flag = 1; */
/*     } */
/*   else  */
/*     { */
/*       *flag = 0; */
/*     } */
/*   //win_event("NI 6602 status requested.\nValue given was : %d with NI 6602 value %d",*flag,NI_6602); */

/*   return D_O_K; */
/* } */

int read_card_type(void)
{        
  unsigned char card_type[4];
  unsigned char i;
  
  if(updating_menu_state != 0)	return D_O_K;	
  
  /* Protection if NI 6503 is not present */
  if (NI_6503 == 0)
    {
      win_printf("Error (read-card-type) : Attempting to read/write on NI 6503 that is not present !\nNI 6503 is : %d",NI_6503);
      return D_O_K;
    }

  for (i=0;i<4;i++)
    {
      card_type[i]=read_data_from_controller(CARD_TYPE,CARD_ADRESS+i);
      win_printf ("card type %0x i=%0x ",card_type[i],i);
      if (card_type[0]!=0x02) win_printf("T'as un souci ! Je ne suis pas qui tu crois");
      
    }
  return (int)card_type[0];
}

/************************MOTOR INTERFACE(LOW-LEVEL)*************************/
int send_command_to_controller (unsigned char commande,unsigned char adr,unsigned char data)
{
  char n=0;
  short int hands= 0;
  /*char *value=NULL;*/
  int status;
  
  /* Protection if NI 6503 is not present */
  if (NI_6503 == 0)
    {
      win_printf("Error (send-commend-to-controller) : Attempting to read/write on NI 6503 that is not present !\nNI 6503 is : %d",NI_6503);
      return D_O_K;
    }

  status= DIG_Prt_Config(NI_6503,PORT_A,NO_HANDSHAKING,OUT_SIGNAL);

  if (adr < 4)
    {
   
      status = DIG_Out_Prt (NI_6503,PORT_A,data);
   		
      status = DIG_Out_Prt (NI_6503,PORT_B,commande+adr);
      while ((hands!=1)& (n < 100)) 
      	{
	  n = n + 1;
	  status = DIG_Prt_Status (NI_6503,PORT_B,&hands);
        }
      if (hands == 1) 
        {
	  status = DIG_Out_Prt (NI_6503,PORT_B,0xFF);/**1 donc on �crit sur B pour avoir un accuse de reception je crois**/
	  hands = 0;
	  n=0;
	  while ((hands != 1) & (n < 100))
	    {
	      n = n + 1;
	      status = DIG_Prt_Status (NI_6503,PORT_B,&hands);
	    }
        }
    } 
        
  status = DIG_Prt_Config(NI_6503,PORT_A,NO_HANDSHAKING,IN_SIGNAL);/***on remet la ligne en etat***/
   
  return 0;
}

unsigned char read_data_from_controller (unsigned char commande,unsigned char  adr)
{
  int n=0;

  long int value;
  int status;
  short int hands=0;
        
  /* Protection if NI 6503 is not present */
  if (NI_6503 == 0)
    {
      win_printf("Error (read-data-from-controller) : Attempting to read/write on NI 6503 that is not present !\nNI 6503 is : %d",NI_6503);
      return D_O_K;
    }

  if (adr < 4) 
    {
      status = DIG_Out_Prt (NI_6503,PORT_B,commande+adr);
      while ((hands != 1) & (n < 1000))
	{
	  n = n + 1;
	  status = DIG_Prt_Status (NI_6503,PORT_B,&hands);
	}
      if (hands == 1)
	{
	  status = DIG_In_Prt (NI_6503,PORT_A, &value);
	  status = DIG_Out_Prt (NI_6503,PORT_B,0xFF);
	}
      status = DIG_Prt_Config(NI_6503,PORT_B,HANDSHAKING,OUT_SIGNAL);
    }

  return  value;
}


/*********************************PIFOC WRITE INTERFACE(LOW-LEVEL)**************************************/
int IO_write(unsigned char canal, short int data)
{
  unsigned char voie;
  
  /* Protection if NI 6503 is not present */
  if (NI_6503 == 0)
    {
      win_printf("Error (IO-write) : Attempting to write on NI 6503 that is not present !\nNI 6503 is : %d",NI_6503);
      return D_O_K;
    }

  voie=canal*64;
  DIG_Prt_Config (NI_6503,PORT_A_AD,NO_HANDSHAKING,OUT_SIGNAL);
  DIG_Prt_Config (NI_6503,PORT_B_AD,NO_HANDSHAKING,OUT_SIGNAL);
  DIG_Prt_Config (NI_6503,PORT_C_AD,NO_HANDSHAKING,OUT_SIGNAL);
  DIG_Out_Prt (NI_6503,PORT_A_AD,(0xFF&data));
  DIG_Out_Prt (NI_6503,PORT_B_AD,63+voie);
  DIG_Out_Line (NI_6503,PORT_C_AD,6,1);
  DIG_Out_Prt (NI_6503,PORT_B_AD,9+voie);
  DIG_Out_Prt (NI_6503,PORT_B_AD,12+voie);
  DIG_Out_Prt (NI_6503,PORT_A_AD,((0xFF00&data)>>8));
  DIG_Out_Prt (NI_6503,PORT_B_AD,5+voie);
  DIG_Out_Line (NI_6503,PORT_C_AD,6,0);
  DIG_Prt_Config (NI_6503,PORT_A_AD,NO_HANDSHAKING,IN_SIGNAL);
  
  return 0;
}

/********************************PIFOC READ INTERFACE(LOW-LEVEL)***************************/
int IO_read(unsigned char canal)
{
  long int voie;
  int i;
  unsigned long mesure=0;
  unsigned long read_1=0;
  unsigned long read_2=0;
    
  voie = (canal-1) * 2;
  
  /* Protection if NI 6503 is not present */
  if (NI_6503 == 0)
    {
      win_printf("Error (IO-read) : Attempting to read NI 6503 that is not present !\nNI 6503 is : %d",NI_6503);
      return D_O_K;
    }

  for (i=0;i<2;i++)/* we need to read twice!!!!!!*/
    {
      DIG_Prt_Config (NI_6503,PORT_A_AD,NO_HANDSHAKING,IN_SIGNAL);
      DIG_Prt_Config (NI_6503,PORT_B_AD,NO_HANDSHAKING,OUT_SIGNAL);
      DIG_Prt_Config (NI_6503,PORT_C_AD,NO_HANDSHAKING,OUT_SIGNAL);
      
      DIG_Out_Prt (NI_6503,PORT_B_AD,voie+57);
      DIG_Out_Line (NI_6503,PORT_C_AD,6,1);
      DIG_Out_Prt (NI_6503,PORT_B_AD,voie+0);
      DIG_Out_Prt (NI_6503,PORT_B_AD,voie+8);
      DIG_In_Prt (NI_6503,PORT_A_AD,&read_1);
      
      DIG_Out_Prt (NI_6503,PORT_B_AD,voie+9);
      DIG_In_Prt (NI_6503,PORT_A_AD,&read_2);
      
      DIG_Out_Prt (NI_6503,PORT_B_AD,voie+57);
      DIG_Out_Line (NI_6503,PORT_C_AD,6,0);
      DIG_Prt_Config (NI_6503,PORT_B_AD,HANDSHAKING,OUT_SIGNAL);
    }
  
  mesure = ((((0xff&read_1)<<8)+(0xff&read_2)+32768) & 0xffff);
  //draw_bubble(screen,0,750,50,"canal %d read 1 %d  read 2 %d  mesure %d ",(int) canal, read_1, read_2,mesure);
  
  return (unsigned int)mesure;
}

/****************************CONVERTERS INITIALIZATION (LOW-LEVEL)********************************/
int init_CNA(void)
{
  if(updating_menu_state != 0)	return D_O_K;

  /* Protection if NI 6503 is not present */
  if (NI_6503 == 0)
    {
      win_printf("Error (init-cna) : Attempting to read/write on NI 6503 that is not present !\nNI 6503 is : %d",NI_6503);
      return D_O_K;
    }
	
  DIG_Prt_Config (NI_6503,PORT_A_AD,NO_HANDSHAKING,IN_SIGNAL);
  DIG_Prt_Config (NI_6503,PORT_B_AD,NO_HANDSHAKING,OUT_SIGNAL);
  DIG_Prt_Config (NI_6503,PORT_C_AD,NO_HANDSHAKING,OUT_SIGNAL);
  DIG_Out_Prt (NI_6503,PORT_C_AD,0xFF);
  IO_write(CNA_1,0);
  IO_write(CNA_2,0);
  IO_write(CNA_3,0);
  IO_read(CAN_1);
  IO_read(CAN_1);
  /*win_printf("CNA_1 %d CNA_2 %d CNA_3 %d CAN_1%d",CNA_1,CNA_2,CNA_3,CAN_1);*/  
  return 0;
}

int init_CAN(void)
{	
  if(updating_menu_state != 0)	return D_O_K;	

  /* Protection if NI 6503 is not present */
  if (NI_6503 == 0)
    {
      win_printf("Error (init-can) : Attempting to read/write on NI 6503 that is not present !\nNI 6503 is : %d",NI_6503);
      return D_O_K;
    }

  DIG_Prt_Config (NI_6503,PORT_A_AD,NO_HANDSHAKING,IN_SIGNAL);
  DIG_Prt_Config (NI_6503,PORT_B_AD,NO_HANDSHAKING,OUT_SIGNAL);
  DIG_Prt_Config (NI_6503,PORT_C_AD,NO_HANDSHAKING,OUT_SIGNAL);
  DIG_Out_Prt (NI_6503,PORT_C_AD,0xFF);
  IO_write (CNA_1, 0);
  IO_write (CNA_2, 0);
  IO_write (CNA_3, 0);
  IO_read (CAN_1);
  IO_read (CAN_1);
  
  return 0;
}

#endif

