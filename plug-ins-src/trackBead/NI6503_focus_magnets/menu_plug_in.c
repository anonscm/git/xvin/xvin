/** \file menu_plug_in.c
    \brief Plug-in program for menu settings and display.
    
    This is a subprogram that creates, displays and interfaces the menus with the plug-in functions.
    
    \author Adrien Meglio
*/
#ifndef _MENU_PLUG_IN_C
#define _MENU_PLUG_IN_C

#include <allegro.h>
#include <winalleg.h>
#include <xvin.h>

/* If you include other plug-ins header do it here*/ 
#include <NIDAQmx.h>
#include "ni_software.h"
#include "./aotf/aotf.h"

#define BUILDING_PLUGINS_DLL

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Menu functions

/** A menu containing a clickable item and a static submenu */
MENU *aotf_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
    
  return mn;
}

/** Main menu for plug_in */     
int menu_plug_in_main(void)
{   
    if(updating_menu_state != 0) return D_O_K;
    
    //win_report("Menu capabilities loaded");
    
    //add_plot_treat_menu_item("AOTF control",NULL,aotf_menu(),0,NULL);
    //add_image_treat_menu_item("AOTF control",NULL,aotf_menu(),0,NULL);
	
    return D_O_K;    
}


#endif

