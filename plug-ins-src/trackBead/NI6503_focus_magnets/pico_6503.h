#ifndef _PICO_6503_H_
#define _PICO_6503_H_

# ifndef _PICO_6503_C_

PXV_VAR(float ,  ni6503_obj_position);
PXV_VAR(float , ni6503_Z_step);
PXV_VAR(int ,  CANAL);
# endif

# ifdef _PICO_6503_C_
float  ni6503_obj_position = 0;
float  ni6503_Z_step = 0.1;
int CANAL = 1;
# endif

PXV_FUNC(float, _read_rot_value, (void));

PXV_FUNC(int, nidaqmx_magnets_main, (int argc, char **argv));


PXV_FUNC(int, IO__whatis__motor_r_position_in_turns, (float* position_in_turns));
PXV_FUNC(int, IO__whatis__motor_t_position_in_mm, (float* position_in_mm));

PXV_FUNC(int, MOTOR_R__set_interface, (float consigne_in_turns));
PXV_FUNC(int, MOTOR_T__set_interface, (float consigne_in_mm));

PXV_FUNC(int, set_speed_feedback_1, (unsigned char feedback_speed));
PXV_FUNC(int, set_speed_feedback_2, (unsigned char feedback_speed));

#endif

