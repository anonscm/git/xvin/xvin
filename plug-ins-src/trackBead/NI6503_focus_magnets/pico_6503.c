/** \file adrien_magnets.c

Interface plugin for ni_software magnet functions use by trackBead. 

\author Adrien Meglio
\version 17/09/2007
*/
# ifndef _PICO_6503_C_
# define _PICO_6503_C_

# include "allegro.h"
# include "xvin.h"


/* If you include other plug-ins header do it here*/ 
#include "../Pico_cfg.h"
#include "ni_6503.h"



/* But not below this define */
# define BUILDING_PLUGINS_DLL
#include "motor.h"
# include "../../plug-ins-src/trackBead/magnetscontrol.h"


# undef _PXV_DLL
# define _PXV_DLL   __declspec(dllexport)
# include "pico_6503.h"


int motor_initiated =0;
float Z_value;
/*done*/
char	*describe_magnets_device_dummy(void)
{
  return "The translation and rotation of the magnets with 6503\n";
}

int _set_light(float z)  
{ return D_O_K;}

float _get_light()  
{ return 0.5;}

int _set_temperature(float z)  
{ return D_O_K;}

float _get_temperature()  
{return 0.5;}


int _has_magnets_memory(void)
{
 return 0;
}

int _has_focus_memory(void)
{
 return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
// Magnets rotation

/*done*/
int _set_rot_value(float rot)
{
  rot -= n_rot_offset;
  MOTOR_R__set_interface(rot); /* Sends rotation command to controller */
  return 0;
}

/*done*/
float _read_rot_value(void)
{
  float position_in_turns = 42.4242;
  return IO__whatis__motor_r_position_in_turns(&position_in_turns);
}

/*done*/
int _set_rot_value_working(float rot)
{
  win_printf("In -set-rot-value with rot=%.2f",rot);

  //rot += n_rota-_read_rot_value();

  //n_rota = rot;
  //rot -= n_rot_offset;
 
  win_printf("Rot value is currently %.2f",_read_rot_value());

  //_read_rot_value();

  MOTOR_R__set_interface(rot);

  win_printf("Rot value is currently %.2f",_read_rot_value());

  return 0;
}

/*done*/
float _read_rot_value_working(void)
{
  float position_in_turns = 42.4242;

  //win_printf("In -read-rot-value");

  IO__whatis__motor_r_position_in_turns(&position_in_turns);

  n_rota = position_in_turns;

  return position_in_turns;
}

/*done*/
int _set_rot_ref_value(float rot)  /* in tour */
{
  //  win_printf("In -set-rot-ref-value with rot=%.2f",rot);

  n_rot_offset = rot;
  n_rota = rot;
  //??????
  return 0;
}
int _set_rot_value_and_wait(float rot)
{
  //win_printf("In -set-rot-value-and-wait with rot=%.2f",rot);

  _set_rot_value(rot);

  return 0;
}

/*done*/
int _go_and_dump_rot(float r)
{
  // win_printf("In -go-and-dump-rot with r=%.2f",r);
 
  n_rota = r;
  _set_rot_value(n_rota);	

  //return dump_to_specific_log_file_with_time(CURRENT_LOG_FILE,"Rotation number auto changed %g\n",n_rota);

  return 0;
}

/*done*/
int _go_wait_and_dump_rot(float r)
{
  //win_printf("In -go-wait-and-dump-rot with r=%.2f",r);
  
  if (n_rota == r) return 0;
  _set_rot_value_and_wait(r);

  //return dump_to_specific_log_file_with_time(CURRENT_LOG_FILE,"Rotation number auto changed %g\n",n_rota);	

  return 0;
}

/*done*/
int _go_wait_and_dump_log_specific_rot(float r, char *log)
{	
  //win_printf("In -go-wait-and-dump-log-specific with r=%.2f",r);
 
  if (n_rota == r) return 0;
  _set_rot_value_and_wait(r);

  //dump_to_specific_log_file_only(log,"Rotation number auto changed %g\n",n_rota);

  return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
// Magnets translation

int   _set_z_origin(float pos, int dir)
{
  //A verifier

		if (Pico_param.motor_param.translation_motor_inverted) 	set_Vmax_moteur_1(255);
		else	set_Vmax_moteur_1(0);

  return D_O_K;
}


/*done*/
int _set_magnet_z_value(float pos)
{
  MOTOR_T__set_interface(pos);  
  
  return 0;
}

/*done*/
int _set_magnet_z_value_old(float pos)
{
  pos = (pos > absolute_maximum_zmag) ? absolute_maximum_zmag : pos; 
  pos -=  n_magnet_offset;
  n_magnet_z = pos + n_magnet_offset;

  MOTOR_T__set_interface(pos);

  return 0;
}

/*done*/
float _read_magnet_z_value(void)
{
  float position_in_mm;
  
  IO__whatis__motor_t_position_in_mm(&position_in_mm);
  
  return position_in_mm;	
}

/*done*/
int _set_magnet_z_ref_value(float z)  /* in mm */
{
  n_magnet_offset = z;
  n_magnet_z = z;
  //NOT SURE
  return 0;
}

/*done*/
int _set_magnet_z_value_and_wait(float pos)
{
  _set_magnet_z_value(pos);

  return 0;
}

/*done*/
int _go_and_dump_z_magnet(float z)
{
  n_magnet_z = z;	
  _set_magnet_z_value(n_magnet_z);

  //return dump_to_specific_log_file_with_time(CURRENT_LOG_FILE,"Magnet Z changed auto to %g\n",n_magnet_z);

  return 0;
}

/*done*/
int _go_wait_and_dump_z_magnet(float zmag)
{
  if (n_magnet_z == zmag) return 0;
  _set_magnet_z_value_and_wait(zmag);

  //return dump_to_specific_log_file_with_time(CURRENT_LOG_FILE,"Magnet Z changed auto to %g\n",n_magnet_z);	

  return 0;
}

/*done*/
int _go_wait_and_dump_log_specific_z_magnet(float zmag, char *log)
{
  if (n_magnet_z == zmag) return 0;
  _set_magnet_z_value_and_wait(zmag);

  //dump_to_specific_log_file_only(log,"Magnet Z changed auto to %g\n",n_magnet_z);
	
  return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

/* Not sure it is the correct functions */
int _set_motors_speed(float v)
{


  //set_Vmax_moteur_1(128);
  //set_speed_feedback_1(unsigned char feedback_speed);

  return 0;	
}
float _get_motors_speed(void)
{
  return v_rota;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
// Main functions


int _init_magnets_OK(void)
{
  

  //  win_printf("initmagnets");
  if (NI_6503 == 0)      detect_NI_boards();
  if (motor_initiated ==0)    init_motors();

  return D_O_K;
}

/*done*/
int pico_6503_main(int argc, char **argv)
{

  //	add_plot_treat_menu_item ( "dummy_magnets", NULL, dummy_magnets_plot_menu(), 0, NULL);
	return D_O_K;
}

/*done*/
int pico_6503_unload(int argc, char **argv)
{
  //remove_item_to_menu(plot_treat_menu, "dummy_magnets", NULL, NULL);
	return D_O_K;
}
#endif


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////FOCUS functions///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
int  _init_focus_OK(void)
{
  if(updating_menu_state != 0)	return D_O_K;
  //win_printf("Init focus %d", NI_6503);
  if (NI_6503 == 0)
    detect_NI_boards();
  //win_printf("Init focus %d", NI_6503);
  init_CNA();
  init_CAN();

  return 0;   
}
float _read_Z_value_accurate_in_micron(void)
{

/*   Pico_param.piezzo_param.Piezzo_max_expansion = 250; */
/* //      win_printf("max position: %d \n IO_read: %d \n canal: %d",Pico_param.piezzo_param.Piezzo_max_expansion,(int)IO_read((unsigned char)CANAL), CANAL); */
/*   ni6503_obj_position = (float)Pico_param.piezzo_param.Piezzo_max_expansion*IO_read((unsigned char)CAN_1)/65535; */
   return ni6503_obj_position; 
}
int _read_Z_value(void)
{
  return (int)(_read_Z_value_accurate_in_micron());//ask meaninig 10*	
}
float _read_Z_value_OK(int *error) /* in microns */
{
  return _read_Z_value_accurate_in_micron();
}
float _read_last_Z_value(void) /* in microns */
{
    return _read_Z_value_accurate_in_micron();
}
int _set_Z_step(float z)  /* in micron */
{
	ni6503_Z_step = z;
	return 0;
}
int _set_Z_value(float z)  /* in microns */
{
  int iDig_position;
  Pico_param.piezzo_param.Piezzo_max_expansion = 250;
  if (z > Pico_param.piezzo_param.Piezzo_max_expansion) z = Pico_param.piezzo_param.Piezzo_max_expansion ;
  if (z < 0)z = 0;
  iDig_position = 65535*(int)(z/Pico_param.piezzo_param.Piezzo_max_expansion);
  IO_write((unsigned char)CNA_2,(short int)iDig_position);
  ni6503_obj_position = z; 
  return D_O_K;
}
int _set_Z_value_6503(void)
{
  float goal = 0;

  if(updating_menu_state != 0)	return D_O_K;
  win_scanf("Where do you want to go ?%f",goal);
  _set_Z_value(goal);
  return D_O_K;
}
int _set_Z_obj_accurate(float zstart)
{
  return _set_Z_value(zstart);
}
int _set_Z_value_OK(float z) 
{
  return _set_Z_value(z);
}

int _inc_Z_value(int step)  /* in micron */
{
  _set_Z_value(ni6503_obj_position + ni6503_Z_step*step);
  return 0;
}
int _small_inc_Z_value(int up)  
{
  int nstep;
  
  _set_Z_step(0.1);
  nstep = (up>=0)? 1:-1;
  _set_Z_value(ni6503_obj_position + ni6503_Z_step*nstep);
  
  return 0;
}
int _big_inc_Z_value(int up)  
{
  int nstep;
  
  _set_Z_step(0.1);
  nstep = (up>=0)? 10:-10;
  _set_Z_value(ni6503_obj_position + ni6503_Z_step*nstep);
  return 0;
}

