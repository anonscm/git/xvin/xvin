/** \file adrien_magnets.c

Interface plugin for ni_software magnet functions use by trackBead. 

\author Adrien Meglio
\version 17/09/2007
*/
# ifndef _PICO_6503_C_
# define _PICO_6503_C_

# include "allegro.h"
# include "xvin.h"


/* If you include other plug-ins header do it here*/ 



/* But not below this define */
# define BUILDING_PLUGINS_DLL
//#include <NIDAQmx.h>
# include "../../plug-ins-src/trackBead/magnetscontrol.h"

# undef _PXV_DLL
# define _PXV_DLL   __declspec(dllexport)
# include "pico_6503.h"

/*done*/
char	*describe_magnets_device_dummy(void)
{
  return "The translation and rotation of the magnets with 6503\n";
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
// Magnets rotation

/*done*/
int _set_rot_value(float rot)
{
  float rot_position;
  int rot_finished = 0;
  time_t time_initial;
  time_t time_current;

  n_rota = rot; /* This value will be interpreted by trackBead as the rotation target */
 
  MOTOR_R__set_interface(rot); /* Sends rotation command to controller */

  /* Checks that rotation has reached target value. This is VERY important.
     The << MOTOR_R__set_interface(rot);>> command immediately returns control to the <<_set_rot_value>>
     function. If this control is not done, the function <<_set_rot_value>> quietly ends way before rotation has ended.
     But, fore some unexpected reason, when it ends it causes abortion of rotation. Then it is very important to check 
     that rotation has ended before returning control to the higher processes. 
     Anyway, this is a problem since no action can be performed while rotation is running !! */
  rot_finished = NO;
  time_initial = time(NULL); /* Sets a timer to avoid infinite loop */
  while (rot_finished != YES)
    {
      sleep (100); /* Check current position not too often */

      IO__whatis__motor_r_position_in_turns(&rot_position); 
      /* Cheks position. 
	 ATTENTION : be sure to use "_in_turns" if  the instruction <<rot>> is in turns, because MOTOR_R_POINTS_PER_TURN
	 might have different values in this plugin and in ni_software, thus leading to weird conversions. At least, all values will be 
	 consistent if input (<<rot>>) and output (<<rot_position>>) are in the same unit. */

      /* Rotation is deemed complete at 0.01 turn precision */
      if (abs(rot_position-rot) < 0.01)
	{
	  rot_finished = YES;
	}

      /* Timeout */
      time_current = time(NULL);
      if ((time_current-time_initial) > 20)
	{
	  win_printf("Rotation timeout !");
	  rot_finished = YES;
	}
    }

  return 0;
}

/*done*/
float _read_rot_value(void)
{
  float position_in_turns = 42.4242;

  //win_printf("In -read-rot-value");

  IO__whatis__motor_r_position_in_turns(&position_in_turns);

  n_rota = position_in_turns;

  //sleep(40);

  return n_rota;
}







/*done*/
int _set_rot_value_working(float rot)
{
  win_printf("In -set-rot-value with rot=%.2f",rot);

  //rot += n_rota-_read_rot_value();

  //n_rota = rot;
  //rot -= n_rot_offset;
 
  win_printf("Rot value is currently %.2f",_read_rot_value());

  //_read_rot_value();

  MOTOR_R__set_interface(rot);

  win_printf("Rot value is currently %.2f",_read_rot_value());

  return 0;
}

/*done*/
float _read_rot_value_working(void)
{
  float position_in_turns = 42.4242;

  //win_printf("In -read-rot-value");

  IO__whatis__motor_r_position_in_turns(&position_in_turns);

  n_rota = position_in_turns;

  return position_in_turns;
}

/*done*/
int _set_rot_ref_value(float rot)  /* in tour */
{
  win_printf("In -set-rot-ref-value with rot=%.2f",rot);

  n_rot_offset = rot;
  n_rota = rot;

  return 0;
}

/* What about the 'wait' ? */
int _set_rot_value_and_wait(float rot)
{
  win_printf("In -set-rot-value-and-wait with rot=%.2f",rot);

  _set_rot_value(rot);

  return 0;
}

/*done*/
int _go_and_dump_rot(float r)
{
  win_printf("In -go-and-dump-rot with r=%.2f",r);
 
  n_rota = r;
  _set_rot_value(n_rota);	

  //return dump_to_specific_log_file_with_time(CURRENT_LOG_FILE,"Rotation number auto changed %g\n",n_rota);

  return 0;
}

/*done*/
int _go_wait_and_dump_rot(float r)
{
  win_printf("In -go-wait-and-dump-rot with r=%.2f",r);
  
  if (n_rota == r) return 0;
  _set_rot_value_and_wait(r);

  //return dump_to_specific_log_file_with_time(CURRENT_LOG_FILE,"Rotation number auto changed %g\n",n_rota);	

  return 0;
}

/*done*/
int _go_wait_and_dump_log_specific_rot(float r, char *log)
{	
  win_printf("In -go-wait-and-dump-log-specific with r=%.2f",r);
 
  if (n_rota == r) return 0;
  _set_rot_value_and_wait(r);

  //dump_to_specific_log_file_only(log,"Rotation number auto changed %g\n",n_rota);

  return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
// Magnets translation

/*done*/
int _set_magnet_z_value(float pos)
{
  int i;
  float z_position;
  int z_finished = 0;
  time_t time_initial;
  time_t time_current;

  //win_report("You asked for z_{mag} = %.2f mm",pos);

  /*pos = (pos > absolute_maximum_zmag) ? absolute_maximum_zmag : pos; */
  
  //pos -=  n_magnet_offset;
  //n_magnet_z = pos + n_magnet_offset;
  n_magnet_z = pos; 

  MOTOR_T__set_interface(pos);  
  
  /* Checks that translation has reached target value. This is VERY important.
     The << MOTOR_T__set_interface(pos);>> command immediately returns control to the <<_set_magnet_z_value>>
     function. If this control is not done, the function <<_set_magnet_z_value>> quietly ends way before translation has ended.
     But, fore some unexpected reason, when it ends it causes abortion of translation. Then it is very important to check 
     that translation has ended before returning control to the higher processes. 
     Anyway, this is a problem since no action can be performed while translation is running !! */
  z_finished = NO;
  time_initial = time(NULL); /* Sets a timer to avoid infinite loop */
  while (z_finished != YES)
    {
      sleep (500); /* Wait enough time for the magnets to move */

      IO__whatis__motor_t_position_in_mm(&z_position); 
      /* Cheks position. 
	 ATTENTION : be sure to use "_in_mm" if  the instruction <<pos>> is in mm, because MOTOR_T_POINTS_PER_MM
	 might have different values in this plugin and in ni_software, thus leading to weird conversions. At least, all values will be 
	 consistent if input (<<pos>>) and output (<<z_position>>) are in the same unit. */

      /* Translation is deemed complete at 0.05 mm precision */
      if (abs(z_position-(pos)) < 0.05)
      	{
	  //win_report("Error is %.2f",abs(z_position-(pos)));
	  z_finished = YES;
	  //win_printf("Translation complete !\nNow at %.2f mum",-z_position);
	}

      /* Timeout */
      time_current = time(NULL);
      if ((time_current-time_initial) > 5)
	{
	  i = win_printf("Translation timeout !\nRetry ?");
	  //win_printf("i = %d",i);
	  if (i == 129) /* Means one clicked CANCEL */
	    {
	      z_finished = YES;
	    }
	  else if ( i== 128) /* Means one clicked OK */
	    {
	      MOTOR_T__set_interface(pos);
	      time_initial = time(NULL);
	      //_set_magnet_z_value(pos);
	    }
	}
      else 
	{
	  MOTOR_T__set_interface(pos);  /* Retries to move to position */
	}
    }

  //win_report("Current z_{mag} = %.2f mm\nRequested z_{mag} = %.2f mm",z_position,pos);

  return 0;
}

/*done*/
int _set_magnet_z_value_old(float pos)
{
  pos = (pos > absolute_maximum_zmag) ? absolute_maximum_zmag : pos; 
  pos -=  n_magnet_offset;
  n_magnet_z = pos + n_magnet_offset;

  MOTOR_T__set_interface(pos);

  return 0;
}

/*done*/
float _read_magnet_z_value(void)
{
  float position_in_mm;
  
  IO__whatis__motor_t_position_in_mm(&position_in_mm);
  
  return position_in_mm;	
}

/*done*/
int _set_magnet_z_ref_value(float z)  /* in mm */
{
  n_magnet_offset = z;
  n_magnet_z = z;

  return 0;
}

/* What about the 'wait' ? */
int _set_magnet_z_value_and_wait(float pos)
{
  _set_magnet_z_value(pos);

  return 0;
}

/*done*/
int _go_and_dump_z_magnet(float z)
{
  n_magnet_z = z;	
  _set_magnet_z_value(n_magnet_z);

  //return dump_to_specific_log_file_with_time(CURRENT_LOG_FILE,"Magnet Z changed auto to %g\n",n_magnet_z);

  return 0;
}

/*done*/
int _go_wait_and_dump_z_magnet(float zmag)
{
  if (n_magnet_z == zmag) return 0;
  _set_magnet_z_value_and_wait(zmag);

  //return dump_to_specific_log_file_with_time(CURRENT_LOG_FILE,"Magnet Z changed auto to %g\n",n_magnet_z);	

  return 0;
}

/*done*/
int _go_wait_and_dump_log_specific_z_magnet(float zmag, char *log)
{
  if (n_magnet_z == zmag) return 0;
  _set_magnet_z_value_and_wait(zmag);

  //dump_to_specific_log_file_only(log,"Magnet Z changed auto to %g\n",n_magnet_z);
	
  return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

/* Not sure it is the correct functions */
int _set_motors_speed(float v)
{
  v_rota = v;
  v_mag = v;

  //set_speed_feedback_1(v_mag); 
  //set_speed_feedback_2(v_rota); 

  return 0;	
}
float _get_motors_speed(void)
{
  return v_rota;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
// Main functions

/* ?? */
int _init_magnets_OK(void)
{
  //win_printf("In -init-magnets-OK\nWhat am I supposed to do here ??");

  //_read_rot_value();

  //win_printf("Rot value is currently %.2f",_read_rot_value());

  return D_O_K;
}

/*done*/
int pico_6503_main(int argc, char **argv)
{

  //	add_plot_treat_menu_item ( "dummy_magnets", NULL, dummy_magnets_plot_menu(), 0, NULL);
	return D_O_K;
}

/*done*/
int pico_6503_unload(int argc, char **argv)
{
  //remove_item_to_menu(plot_treat_menu, "dummy_magnets", NULL, NULL);
	return D_O_K;
}
#endif

