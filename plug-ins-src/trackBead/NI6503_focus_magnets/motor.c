/**
\file motor.c

\brief Program for driving DC motors with C Herrmann controller
\version 11/03
\author JF Allemand	
*/				 				
 
#ifndef motor_C
#define motor_C

#include <allegro.h>
#include <winalleg.h>
#include <xvin.h>
#include <windows.h>
#include <stdio.h>

//#include <NIDAQmx.h>
#include "nidaq.h"
#include "ni_6503.h"


/* But not below this define */
#define BUILDING_PLUGINS_DLL
#include "motor.h"
//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
// RESET THE MOTORS (LOW-LEVEL)

MENU *DAQMOTOR_menu(void);

int new_res_motor_1(void)
{	
  int i;
  clock_t start;
    	
  if(updating_menu_state != 0)	return D_O_K;	
  send_command_to_controller(COMMAND_PID,CARD_ADRESS,RESET_MOT_1);
  send_command_to_controller(PARAMETER_PID,CARD_ADRESS,RESET_MOT_1);
        
  /*for (i=0,start=clock();(clock()-start<(2*CLOCKS_PER_SEC))&&(*/read_data_from_controller(TEST_COMMAND_PID,CARD_ADRESS)/*!=255);i++)*/;

  //win_report("Motor 1 reset");

  return 0;
}

int new_res_motor_2(void)
{		
  int i;
  clock_t start;
    		
  if(updating_menu_state != 0)	return D_O_K;	
  send_command_to_controller(COMMAND_PID,CARD_ADRESS,RESET_MOT_2);
  send_command_to_controller(PARAMETER_PID,CARD_ADRESS,RESET_MOT_2);
        
  /*for (i=0,start=clock();(clock()-start<(2*CLOCKS_PER_SEC))&&(*/read_data_from_controller(TEST_COMMAND_PID,CARD_ADRESS)/*!=255);i++)*/;

  //win_report("Motor 2 reset");

  return 0;
}	
  
//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
// PID PARAMETERS (LOW-LEVEL)

int set_Vmax_moteur_1(unsigned char v_max_1)
{  
  send_command_to_controller(READ_V_MAX_1,CARD_ADRESS,v_max_1);
   
  return 0;
}

int set_Vmax_moteur_2(unsigned char v_max_2)
{   
  send_command_to_controller(READ_V_MAX_2,CARD_ADRESS,v_max_2);
  
  return 0;
}

int set_speed_feedback_1(unsigned char feedback_speed)
{
  send_command_to_controller(SPEED_FEEDBACK_VMAX_MOT_1,CARD_ADRESS,feedback_speed);
    
  return 0;
}

int set_speed_feedback_2(unsigned char feedback_speed)
{	
  send_command_to_controller(SPEED_FEEDBACK_VMAX_MOT_2,CARD_ADRESS,feedback_speed);
  
  return 0;
}

int set_acc_pos_feedback_1(unsigned char acc)
{   
  int i;
  clock_t start;
  send_command_to_controller(COMMAND_PID,CARD_ADRESS,ACCELERATION_1);
  send_command_to_controller(PARAMETER_PID,CARD_ADRESS,acc);
    
  /*for (i=0,start=clock();(clock()-start<(2*CLOCKS_PER_SEC))&&(*/read_data_from_controller(TEST_COMMAND_PID,CARD_ADRESS)/*!=255);i++)*/;
        
  return 0;
}

int set_acc_pos_feedback_2(unsigned char acc)
{  
  int i;
  clock_t start;

  send_command_to_controller(COMMAND_PID,CARD_ADRESS,ACCELERATION_2);
  send_command_to_controller(PARAMETER_PID,CARD_ADRESS,acc);
    
  /*for (i=0,start=clock();(clock()-start<(2*CLOCKS_PER_SEC))&&(*/read_data_from_controller(TEST_COMMAND_PID,CARD_ADRESS)/*!=255);i++)*/;
  return 0;
}

int set_lim_dec_feedback_pos_1(short int lim_pos_feedback)
{  
  unsigned char pos0=0;
  unsigned char pos1=0;
	
  pos0=(unsigned char)(lim_pos_feedback&0xFF);
  pos1=(unsigned char)((lim_pos_feedback&0xFF00)>>8);
   
  send_command_to_controller(POS_FEEDBACK_LIM_1_MOT_1,CARD_ADRESS,pos1);
  send_command_to_controller(POS_FEEDBACK_LIM_2_MOT_1,CARD_ADRESS,pos0);
    
  return 0;
}

int set_lim_dec_feedback_pos_2(short int lim_pos_feedback)
{  	
  unsigned char pos0=0;
  unsigned char pos1=1;

  pos0=(unsigned char)(lim_pos_feedback&0xFF);
  pos1=(unsigned char)((lim_pos_feedback&0xFF00)>>8);
  send_command_to_controller(POS_FEEDBACK_LIM_1_MOT_2,CARD_ADRESS,pos1);
  send_command_to_controller(POS_FEEDBACK_LIM_2_MOT_2,CARD_ADRESS,pos0);
   
  return 0;
}

long int read_pos_motor_1(void)
{    
  unsigned char pos1=0;
  unsigned char pos2=0;
  unsigned char pos3=0;
  unsigned char pos4=0;
  long int position_motor_1=0;

  pos1=   read_data_from_controller(LECT_POS_1_1,CARD_ADRESS);
  pos2=   read_data_from_controller(LECT_POS_1_2,CARD_ADRESS);
  pos3=   read_data_from_controller(LECT_POS_1_3,CARD_ADRESS);
  pos4=   read_data_from_controller(LECT_POS_1_4,CARD_ADRESS);
  //win_printf("1 %d pos2 %d  3 %d  4 %d ",(int)pos1, (int)pos2, (int)pos3,(int)pos4);
    
  position_motor_1=pos4+(pos3<<8)+(pos2<<16)+(pos1<<24);

  return position_motor_1;
}

long int read_pos_motor_2(void)
{    
  unsigned char pos1=0;
  unsigned char pos2=0;
  unsigned char pos3=0;
  unsigned char pos4=0;
  long int position_motor_2=0;

  pos1=   read_data_from_controller(LECT_POS_2_1,CARD_ADRESS);
  pos2=   read_data_from_controller(LECT_POS_2_2,CARD_ADRESS);
  pos3=   read_data_from_controller(LECT_POS_2_3,CARD_ADRESS);
  pos4=   read_data_from_controller(LECT_POS_2_4,CARD_ADRESS);
  position_motor_2=pos4+(pos3<<8)+(pos2<<16)+(pos1<<24);
    
  return position_motor_2;
}

int new_P_motor_1(unsigned char new_P_1)
{		
  int i;
  clock_t start;
  send_command_to_controller(COMMAND_PID,CARD_ADRESS,P_1);
  send_command_to_controller(PARAMETER_PID,CARD_ADRESS,new_P_1); 
    
    
  /*for (i=0,start=clock();(clock()-start<(2*CLOCKS_PER_SEC))&&(*/read_data_from_controller(TEST_COMMAND_PID,CARD_ADRESS)/*!=255);i++)*/;
    
  return 0;
}

int new_P_motor_2(unsigned char new_P_2)
{		   
  int i;
  clock_t start;
  send_command_to_controller(COMMAND_PID,CARD_ADRESS,P_2);
  send_command_to_controller(PARAMETER_PID,CARD_ADRESS,new_P_2); 

         
  /*for (i=0,start=clock();(clock()-start<(2*CLOCKS_PER_SEC))&&(*/read_data_from_controller(TEST_COMMAND_PID,CARD_ADRESS)/*!=255);i++)*/;
   
  return 0;
}

int new_I_motor_1(unsigned char new_I_1)
{        int i;
  clock_t start;
  send_command_to_controller(COMMAND_PID,CARD_ADRESS,I_1);
  send_command_to_controller(PARAMETER_PID,CARD_ADRESS,new_I_1); 
       
  /* for (i=0,start=clock();(clock()-start<(2*CLOCKS_PER_SEC))&&( */read_data_from_controller(TEST_COMMAND_PID,CARD_ADRESS)/*!=255);i++) */;
  return 0;
}

int new_I_motor_2(unsigned char new_I_2)
{
  int i;
  clock_t start;
  send_command_to_controller(COMMAND_PID,CARD_ADRESS,I_2);
  send_command_to_controller(PARAMETER_PID,CARD_ADRESS,new_I_2);
            
  /* for (i=0,start=clock();(clock()-start<(2*CLOCKS_PER_SEC))&& (*/read_data_from_controller(TEST_COMMAND_PID,CARD_ADRESS)/* !=255);i++) */;
  return 0;
}

int new_D_motor_1(unsigned char new_D_1)
{
  int i;
  clock_t start;
  send_command_to_controller(COMMAND_PID,CARD_ADRESS,D_1);
  send_command_to_controller(PARAMETER_PID,CARD_ADRESS,new_D_1);
        
  /*for (i=0,start=clock();(clock()-start<(2*CLOCKS_PER_SEC))&&(*/read_data_from_controller(TEST_COMMAND_PID,CARD_ADRESS)/*!=255);i++)*/;
  return 0;
}

int new_D_motor_2(unsigned char new_D_2)
{		int i;
  clock_t start;
  send_command_to_controller(COMMAND_PID,CARD_ADRESS,D_2);
  send_command_to_controller(PARAMETER_PID,CARD_ADRESS,new_D_2);
      
  /*for (i=0,start=clock();(clock()-start<(2*CLOCKS_PER_SEC))&&(*/read_data_from_controller(TEST_COMMAND_PID,CARD_ADRESS)/*!=255);i++)*/;
  return 0;
}

int new_R_motor_1(unsigned char new_R_1)
{	int i;
  clock_t start;

  send_command_to_controller(COMMAND_PID,CARD_ADRESS,R_1);
  send_command_to_controller(PARAMETER_PID,CARD_ADRESS,new_R_1);
        
  /*for (i=0,start=clock();(clock()-start<(2*CLOCKS_PER_SEC))&&(*/read_data_from_controller(TEST_COMMAND_PID,CARD_ADRESS)/*!=255);i++)*/;
  return 0;
}

int new_R_motor_2(unsigned char new_R_2)
{		int i;
  clock_t start;
  send_command_to_controller(COMMAND_PID,CARD_ADRESS,R_2);
  send_command_to_controller(PARAMETER_PID,CARD_ADRESS,new_R_2);
       
  /*for (i=0,start=clock();(clock()-start<(2*CLOCKS_PER_SEC))&&(*/read_data_from_controller(TEST_COMMAND_PID,CARD_ADRESS)/*!=255);i++)*/;
  return 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
// PID PARAMETERS (LOW-LEVEL)

int init_motors(void)
{
  if(updating_menu_state != 0)	return D_O_K;
  //win_printf(" In Menu"); 
  /* Reset and init motor 1 */
  new_res_motor_2();                         /* translation */
  
  set_lim_dec_feedback_pos_2(200);
  new_P_motor_2(40);//20
  new_I_motor_2(5);//0
  new_D_motor_2(0);
  new_R_motor_2(6); //6
  set_acc_pos_feedback_2(1); //1
  set_speed_feedback_2(85/*25*/); 
  set_Vmax_moteur_2(100);//128

  /* Reset and init motor 2 */
  new_res_motor_1();                         /* rotation */

  set_lim_dec_feedback_pos_1(200); 
  new_P_motor_1(4);//20
  new_I_motor_1(0);//0
  new_D_motor_1(0);
  new_R_motor_1(6); //6
  set_acc_pos_feedback_1(10);  //1
  set_speed_feedback_1(15/*25*/); 
  set_Vmax_moteur_1(100);//128

  add_image_treat_menu_item ( "Motors 6503", NULL, DAQMOTOR_menu(), 0, NULL); //
  //win_printf("Menu"); 
  return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
// Motors Control

int set_consigne_pos_moteur_1(long cons_pos_1)
{    
  unsigned char pos1=0;
  unsigned char pos2=0;
  unsigned char pos3=0;
  unsigned char pos4=0;

  pos4=(unsigned char)(cons_pos_1&0xFF);
  pos3=(unsigned char)((cons_pos_1&0xFF00)>>8);
  pos2=(unsigned char)((cons_pos_1&0xFF0000)>>16);
  pos1=(unsigned char)((cons_pos_1&0xFF000000)>>24);
  send_command_to_controller(CONSIGNE_POS_1_1,CARD_ADRESS,pos1);
  send_command_to_controller(CONSIGNE_POS_1_2,CARD_ADRESS,pos2);
  send_command_to_controller(CONSIGNE_POS_1_3,CARD_ADRESS,pos3);
  send_command_to_controller(CONSIGNE_POS_1_4,CARD_ADRESS,pos4);

  //GLOBAL__consigne__t_in_mm = ((float) cons_pos_1/MOTOR_T_POINTS_PER_MM);

  return 0;
}

int set_consigne_pos_moteur_2(long cons_pos_2)
{     
  unsigned char pos1=0;
  unsigned char pos2=0;
  unsigned char pos3=0;
  unsigned char pos4=0;
	
  pos4=(unsigned char)(cons_pos_2&0xFF);
  pos3=(unsigned char)((cons_pos_2&0xFF00)>>8);
  pos2=(unsigned char)((cons_pos_2&0xFF0000)>>16);
  pos1=(unsigned char)((cons_pos_2&0xFF000000)>>24);
  send_command_to_controller(CONSIGNE_POS_2_1,CARD_ADRESS,pos1);
  send_command_to_controller(CONSIGNE_POS_2_2,CARD_ADRESS,pos2);
  send_command_to_controller(CONSIGNE_POS_2_3,CARD_ADRESS,pos3);
  send_command_to_controller(CONSIGNE_POS_2_4,CARD_ADRESS,pos4);

  //  GLOBAL__consigne__r_in_turns = ((float) cons_pos_2/MOTOR_R_POINTS_PER_TURN);

  return 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
// Shortcut functions

int MOTOR_T__move(float amount_in_mm)
{
  long position_T;

  if(updating_menu_state != 0) return D_O_K;

  read_pos_motor_1(); /* Need to read twice, if another canal (eg pifoc) has been read/written just before */
  position_T = read_pos_motor_1();

  position_T += (long) (-amount_in_mm*MOTOR_T_POINTS_PER_MM); /* The motor is mounted upside-down */

  set_consigne_pos_moteur_1(position_T);

  return D_O_K; 
}

int MOTOR_R__move(float amount_in_turns)
{
  long position_R;

  if(updating_menu_state != 0) return D_O_K;

  read_pos_motor_2(); /* Need to read twice, if another canal (eg pifoc) has been read/written just before */
  position_R = read_pos_motor_2();

  position_R += (long) (amount_in_turns*MOTOR_R_POINTS_PER_TURN);

  set_consigne_pos_moteur_2(position_R);

  return D_O_K;
}

int MOTOR_R__set(void)
{	
  float consigne=0;

  if(updating_menu_state != 0)	return D_O_K;	
	
  if (win_scanf("Position : %f (tours) ?",&consigne) == CANCEL) return D_O_K;

  set_consigne_pos_moteur_2((long) consigne*MOTOR_R_POINTS_PER_TURN);

  //MOTOR_R__set_interface(consigne);

  //win_report("Asked the motor to move to %.2f turns",consigne);

  return D_O_K;
}

int MOTOR_R__set_interface(float consigne_in_turns)
{	
  long consigne_in_points;

  if(updating_menu_state != 0)	return D_O_K;	
	
  consigne_in_points = (long) (consigne_in_turns*MOTOR_R_POINTS_PER_TURN);

  //win_report("Points per turn : %d",MOTOR_R_POINTS_PER_TURN);

  //win_report("About to change rotation!\nTarget is %.2f turns\nor %d points",consigne_in_turns,consigne_in_points);

  set_consigne_pos_moteur_2(consigne_in_points);

  return D_O_K;
}

int MOTOR_T__set(void)
{	
  float consigne=0;
	 	
  if(updating_menu_state != 0)	return D_O_K;	
	
  if (win_scanf("Position : %f (mm) ?",&consigne) == CANCEL) return D_O_K;

  set_consigne_pos_moteur_1((long) -consigne*MOTOR_T_POINTS_PER_MM); /* The motor is mounted upside-down */

  //MOTOR_T__set_interface(consigne);

  //win_report("Asked the motor to move to %.2f mm",consigne);

  return D_O_K;
}

int MOTOR_T__set_interface(float consigne_in_mm)
{	
  long consigne_in_points;

  if(updating_menu_state != 0)	return D_O_K;	
	
  consigne_in_points = (long) (-consigne_in_mm*MOTOR_T_POINTS_PER_MM); /* The motor is mounted upside-down */

  //win_report("About to change z position!\nTarget is %.2f mm\nor %d points",consigne_in_mm,consigne_in_points);

  set_consigne_pos_moteur_1(consigne_in_points); 

  return D_O_K;
}



/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
// I/O

/* Returns points to mm conversion in translation

\author Adrien Meglio
\version 14/05/07
*/
int IO__whatis__motor_t_points_per_mm(int* motor_t_points_per_mm )
{
  if(updating_menu_state != 0)	return D_O_K;

  *motor_t_points_per_mm = MOTOR_T_POINTS_PER_MM;

  return D_O_K;
}

/* Returns points to turns conversion in rotation

\author Adrien Meglio
\version 14/05/07
*/
int IO__whatis__motor_r_points_per_turn(int* motor_r_points_per_turn)
{
  if(updating_menu_state != 0)	return D_O_K;

  //*motor_r_points_per_turn = MOTOR_R_POINTS_PER_TURN;

  return D_O_K;
}

/* Returns rotation consigne in turns

\author Adrien Meglio
\version 16/05/07
*/
int IO__whatis__motor_r_consigne_in_turns(float* position_in_turns)
{
  if(updating_menu_state != 0)	return D_O_K;

  //*position_in_turns= GLOBAL__consigne__r_in_turns; 
  
  return D_O_K;
}

/* Returns translation consigne in mm

\author Adrien Meglio
\version 16/05/07
*/
int IO__whatis__motor_t_consigne_in_mm(float* position_in_mm)
{
  if(updating_menu_state != 0)	return D_O_K;

  //*position_in_mm = GLOBAL__consigne__t_in_mm; 
  
  return D_O_K;
}

/* Returns rotation position in points

\author Adrien Meglio
\version 28/05/07
*/
int IO__whatis__motor_r_position_in_points(long* position_in_points)
{
  long pos;

  if(updating_menu_state != 0)	return D_O_K;

  /* For some unknown reason, the position has to be read twice (if read/written on another canal previsously, eg pifoc, 
     communications parameters may have changed) */
  read_pos_motor_2();
  pos = read_pos_motor_2();

  *position_in_points= pos; 
  
  return D_O_K;
}

/* Returns rotation position in turns

\author Adrien Meglio
\version 17/09/2007
*/
int IO__whatis__motor_r_position_in_turns(float* position_in_turns)
{
  long pos;

  if(updating_menu_state != 0)	return D_O_K;

  /* For some unknown reason, the position has to be read twice (if read/written on another canal previsously, eg pifoc, 
     communications parameters may have changed) */
  read_pos_motor_2();
  pos = read_pos_motor_2();

  *position_in_turns = (float) pos/MOTOR_R_POINTS_PER_TURN; 
  
  return D_O_K;
}

/* Returns translation position in mm

\author Adrien Meglio
\version 28/05/07
*/
int IO__whatis__motor_t_position_in_points(long* position_in_points)
{
  long pos;

  if(updating_menu_state != 0)	return D_O_K;

  /* For some unknown reason, the position has to be read twice (if read/written on another canal previsously, eg pifoc, 
     communications parameters may have changed) */
  read_pos_motor_1();
  pos = read_pos_motor_1();

  *position_in_points = pos; 
  
  return D_O_K;
}

/* Returns translation position in mm

\author Adrien Meglio
\version 28/05/07
*/
int IO__whatis__motor_t_position_in_mm(float* position_in_mm)
{
  long pos;

  if(updating_menu_state != 0)	return D_O_K;

  /* For some unknown reason, the position has to be read twice (if read/written on another canal previsously, eg pifoc, 
     communications parameters may have changed) */
  read_pos_motor_1();
  pos = read_pos_motor_1();

  *position_in_mm = (float) -pos/MOTOR_T_POINTS_PER_MM; /* The motor is mounted upside-down */
  
  return D_O_K;
}

/**********************TRADUCTION EN BOUTONS **************************/

int new_set_P_1(void)
{       
  static int new_P_1;
          
  if(updating_menu_state != 0)	return D_O_K;	
  win_scanf("Proportional factor motor 1 %d ?",&new_P_1);
  new_P_motor_1((unsigned char)new_P_1);

  return 0;
}

int new_set_P_2(void)
{       
  static int new_P_2;
        
  if(updating_menu_state != 0)	return D_O_K;	
  win_scanf("Proportional factor motor 2 %d ?",&new_P_2);
  new_P_motor_2((unsigned char)new_P_2);

  return 0;
}


int new_set_I_1(void)
{
  static int new_I_1;
  	    
  if(updating_menu_state != 0)	return D_O_K;	
  win_scanf("Integral factor motor 1 %d ?",&new_I_1);
  new_I_motor_1((unsigned char)new_I_1);
  
  return 0;
}

int new_set_I_2(void)
{ 
  static int new_I_2;
           
  if(updating_menu_state != 0)	return D_O_K;	
  win_scanf("Integral factor motor 2 %d ?",&new_I_2);
  new_I_motor_2((unsigned char)new_I_2);
  
  return 0;
}

int new_set_D_1(void)
{ 
  static int new_D_1;
        
  if(updating_menu_state != 0)	return D_O_K;	
  win_scanf("Derivative factor motor 1 %d ?",&new_D_1);
  new_D_motor_1((unsigned char)new_D_1);
  
  return 0;
}

int new_set_D_2(void)
{ 
  static int new_D_2;
         
  if(updating_menu_state != 0)	return D_O_K;	
  win_scanf("Derivative factor motor 2 %d ?",&new_D_2);
  new_D_motor_2((unsigned char)new_D_2);
  
  return 0;
}

int new_set_ratio_motor_1(void)
{ 
  static int new_R_1;
        
  if(updating_menu_state != 0)	return D_O_K;	
  win_scanf("Ratio factor motor 1 %d ?",&new_R_1);
  new_R_motor_1((unsigned char)new_R_1);
  
  return 0;
}

int new_set_ratio_motor_2(void)
{ 
  static int new_R_2;
  	    
  if(updating_menu_state != 0)	return D_O_K;	
  win_scanf("Ratio factor motor 2 %d ?",&new_R_2);
  new_R_motor_2((unsigned char)new_R_2);
  
  return 0;
}

int new_reset_motor_1(void)
{
  if(updating_menu_state != 0)	return D_O_K;	
  new_res_motor_1();
  
  return 0;
}

int new_reset_motor_2(void)
{
  if(updating_menu_state != 0)	return D_O_K;	
  new_res_motor_2();

  return 0;
}	

int set_cons_max_speed_pos_1(void)
{
  static int v_max=0;

  if(updating_menu_state != 0)	return D_O_K;	
      
  win_scanf("Max Speed %d ?",&v_max);
  set_speed_feedback_1((unsigned char)v_max);

  return 0;
}

int set_cons_max_speed_pos_2(void)
{ 
  static int v_max=0;

  if(updating_menu_state != 0)	return D_O_K;	
      
  win_scanf("Max Speed %d ?",&v_max);
  set_speed_feedback_2((unsigned char)v_max);

  return 0;
}
	
int Read_pos_mot_1(void)
{	
  float result;
  if(updating_menu_state != 0)	return D_O_K;	

  IO__whatis__motor_t_position_in_mm(&result);	
  win_printf("Pos Mot 1 %f",result);//read_pos_motor_1());
	
  return 0;
}

int Read_pos_mot_2(void)
{	
  if(updating_menu_state != 0)	return D_O_K;		
  win_printf("Pos Mot 2 %ld",read_pos_motor_2());
	
  return 0;
}

int set_cons_pos_mot_1(void)
{
  static  long cons1=0;
		
  if(updating_menu_state != 0)	return D_O_K;	
  win_scanf("Position %d ?",&cons1);
  set_consigne_pos_moteur_1((long)cons1*MOTOR_T_POINTS_PER_MM);
	
  return 0;
}
int set_cons_pos_mot_2(void)
{
  static  long  cons2=0;
		
  if(updating_menu_state != 0)	return D_O_K;	
  win_scanf("Position %d ?",&cons2);
  set_consigne_pos_moteur_2((long)cons2*MOTOR_T_POINTS_PER_MM);
	
  return 0;
}

int set_vitesse_mot_1(void)
{	 
  static int vmax=0;
  
  if(updating_menu_state != 0)	return D_O_K;	
  win_scanf("Speed %d ?(0-255)",&vmax);
  set_Vmax_moteur_1((unsigned char)vmax);
  
  return 0;
}

int set_vitesse_mot_2(void)
{
  static int vmax=0;
     	 
  if(updating_menu_state != 0)	return D_O_K;	
  win_scanf("Speed %d ?",&vmax);
  set_Vmax_moteur_2((unsigned char)vmax);
  
  return 0;
}

int set_acc_pos_1(void)
{ 
  static int acc=0;
  
  if(updating_menu_state != 0)	return D_O_K;	
  win_scanf("Acceleration position feedback %d ?",&acc);
  set_acc_pos_feedback_1((unsigned char)acc);
  
  return 0;
}

int set_acc_pos_2(void)
{  
  static int acc=0;
	 
  if(updating_menu_state != 0)	return D_O_K;	
  win_scanf("Acceleration position feedback %d ?",&acc);
  set_acc_pos_feedback_2((unsigned char)acc);
  
  return 0;
}

int set_cons_pos_lim_1(void)
{  
  static int pos_lim=0;
     
  if(updating_menu_state != 0)	return D_O_K;	
  win_scanf("Pos lim %d ?",&pos_lim);
  set_lim_dec_feedback_pos_1((short int)pos_lim);
	
  return 0;
}

int set_cons_pos_lim_2(void)
{   
  static int pos_lim=0;
     
  if(updating_menu_state != 0)	return D_O_K;	
  win_scanf("Pos lim %d ?",&pos_lim);
  set_lim_dec_feedback_pos_2((short int)pos_lim);
	
  return 0;
}



MENU *DAQMOTOR_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
	
  add_item_to_menu(mn,"Read_pos_mot_1",Read_pos_mot_1,NULL,0,NULL);
  add_item_to_menu(mn,"set_cons_pos_mot_1",set_cons_pos_mot_1,NULL,0,NULL);
  add_item_to_menu(mn,"Reset motor 1",new_reset_motor_1,NULL,0,NULL);
  add_item_to_menu(mn,"set_cons_max_speed_pos_1",set_cons_max_speed_pos_1,NULL,0,NULL);
  add_item_to_menu(mn,"set_acc_pos_1",set_acc_pos_1,NULL,0,NULL);
  add_item_to_menu(mn,"set_cons_pos_lim_1",set_cons_pos_lim_1,NULL,0,NULL);
  add_item_to_menu(mn,"P motor 1",new_set_P_1,NULL,0,NULL);
  add_item_to_menu(mn,"I motor 1",new_set_I_1,NULL,0,NULL);
  add_item_to_menu(mn,"D motor 1",new_set_D_1,NULL,0,NULL);
  add_item_to_menu(mn,"Ratio motor 1",new_set_ratio_motor_1,NULL,0,NULL);
  add_item_to_menu(mn,"set_vitesse_mot_1",set_vitesse_mot_1,NULL,0,NULL);
  add_item_to_menu(mn,"Read_pos_mot_2",Read_pos_mot_2,NULL,0,NULL);
  add_item_to_menu(mn,"set_cons_pos_mot_2",set_cons_pos_mot_2,NULL,0,NULL);
  add_item_to_menu(mn,"Reset motor 2",new_reset_motor_2,NULL,0,NULL);
  add_item_to_menu(mn,"set_cons_max_speed_pos_2",set_cons_max_speed_pos_2,NULL,0,NULL);
  add_item_to_menu(mn,"set_acc_pos_2",set_acc_pos_2,NULL,0,NULL);
  add_item_to_menu(mn,"set_cons_pos_lim_2",set_cons_pos_lim_2,NULL,0,NULL);
  add_item_to_menu(mn,"P motor 2",new_set_P_2,NULL,0,NULL);
  add_item_to_menu(mn,"I motor 2",new_set_I_2,NULL,0,NULL);
  add_item_to_menu(mn,"D motor 2",new_set_D_2,NULL,0,NULL);
  add_item_to_menu(mn,"Ratio motor 2",new_set_ratio_motor_2,NULL,0,NULL);
  add_item_to_menu(mn,"set_vitesse_mot_2",set_vitesse_mot_2,NULL,0,NULL);
  add_item_to_menu(mn,"do_initialisation",init_motors ,NULL,0,NULL);

  add_item_to_menu(mn,"Set rotation",MOTOR_R__set,NULL,0,NULL);
  add_item_to_menu(mn,"Set translation",MOTOR_T__set,NULL,0,NULL);	


  return mn;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Main functions

int motorfile_main(void)
{
  if(updating_menu_state != 0)	return D_O_K;	

  //win_report("Motors initialization");
  
  // if (IS_NI_6503_PRESENT != 0)
  //{
   init_motors();
   //win_report("Motors initialized");
   //   GLOBAL__consigne__t_in_mm = (float) read_pos_motor_1()*MOTOR_T_POINTS_PER_MM;
   //GLOBAL__consigne__r_in_turns = (float) read_pos_motor_2()*MOTOR_R_POINTS_PER_TURN;
   //win_report("After motors initialization \nT : %.2f mm \nR : %.2f turns",GLOBAL__consigne__t_in_mm,GLOBAL__consigne__r_in_turns);
   //}

  /* Loads menu functions */
  //menu_main();
  //win_report("Menus initialized");

  return D_O_K;    
}

#endif








