#pragma once
# include "allegro.h"
#ifdef XV_WIN32
# include "winalleg.h"
#endif
#include "xvin.h"
#include "track_util.h"
#include "trackBead.h"
# include "action.h"

typedef struct _calib_param
{
  int nf;                  // the total number of frames requires to perform calibration
  int nstep;               // the number of steps i.e. of lines in the calib image
  int dead;                // the number of frames diregard just after focus has changed
  int nper;                // the number of frames between two focus changes
  int n_zerof;             // the number of frames while the z position is measured 
                           // with bead at F = 0
  int zero_dead;           // the number of frames disregard after F is set to zero
  int *im;                 // the array of images number where each objecive step occurs
  int im_start_zerof;      // the image number defining the beginning of the zero adjustment phase 
  float zobj_start;        // the starting objective position
  float zobj_step;         // the step size of focus step
  float zmag_cal;          // the z magnet position during calibration
  float zmag_ground;       // the z magnet leading to bead on ground
  float zmag_after;        // the z magnet position after calibration
  float *zo;               // the array of objective position
  float rot_pos;           // Rotation where image is made
} calib_param;


PXV_FUNC(int, grab_bead_info_from_calibration, (O_i *oi, float *xb, float *yb, float *zb, int *nx, int *cw));
PXV_FUNC(int, grab_bead_filter_info_from_calibration, (O_i *oi, int *rc, int *fc, int *fw));
PXV_FUNC(O_i*, do_load_calibration, (int n));
PXV_FUNC(int, load_generic_calibration_image,(void));
PXV_FUNC(int, record_calibration, (void));
PXV_FUNC(int, calibration_oi_idle_action, (struct  one_image *oi, DIALOG *d));
PXV_FUNC(MENU* , prepare_calib_menu, (b_track *bt));
PXV_FUNC(int, link_beads_to_generic_calibration, (void));
PXV_FUNC(int, link_calibration_to_cross, (int cross, O_i* oi));
PXV_FUNC(int, reload_calibrations_from_last_trk, (void));
PXV_FUNC(int, reload_calibrations_from_last_trk_old, (void));
PXV_FUNC(int, position_cross_at_fence_limit, (b_track *bt));
PXV_FUNC(int, relink_all_calibrations, (void));
PXV_FUNC(int, adj_calibration_image_job, (int im, struct future_job *job));
