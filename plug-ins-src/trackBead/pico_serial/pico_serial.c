/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
 * http://www.yov408.com/html/tutorials.php?s=41
http://msdn.microsoft.com/en-us/library/ms810467.aspx
  */
# ifndef _PICO_SERIAL_C_
# define _PICO_SERIAL_C_

# include "allegro.h"
#ifdef XV_WIN32
# include "winalleg.h"
#endif
# include "xvin.h"




#ifdef XV_WIN32
/* If you include other plug-ins header do it here*/
# include "../trackBead.h"


/* But not below this define */
# define BUILDING_PLUGINS_DLL

# undef _PXV_DLL
# define _PXV_DLL   __declspec(dllexport)
# include "pico_serial.h"

# include "../../cfg_file/Pico_cfg.h"
# include "../action.h"
# include "../../fft2d/fftbtl32n.h"
# include "../fillibbt.h"

# include "../trackBead.h"
# include "../track_util.h"
unsigned long t_rs = 0, dt_rs, t_sen = 0;

UINT timeout = 200;  // ms

char last_answer_2[128] = {0};

int im_ci(void);

// mode = 0 erase logfile
int switch_rs232_debug_on(void)
{
    int mode;
    char ffile[2048] = {0};

    if (updating_menu_state != 0)
    {
        if (debug == 1)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }

    snprintf(ffile, sizeof(ffile), "%s\\%s", current_write_open_path_picofile, debug_file);
    backslash_to_slash(ffile);
    mode = win_printf("Debugging file: %s\n"
                      "Do you want to reset all information in that file?"
                      , ffile);
    if (debug_file_w_path != NULL)
      {
	free(debug_file_w_path);
	debug_file_w_path = NULL;
      }
    debug_file_w_path = strdup(ffile);

    if (mode == WIN_CANCEL) fp_debug = fopen(debug_file_w_path, "a");
    else   fp_debug = fopen(debug_file_w_path, "w");

    if (fp_debug == NULL)
      {
	win_printf("Could not open rs232 debug file:\n%s",ffile);
	return 1;
      }

    fclose(fp_debug);
    debug = 1;
    return 0;
}

int switch_rs232_debug_off(void)
{
    if (updating_menu_state != 0)
    {
        if (debug == 0)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }

    debug = 0;
    return 0;
}


HANDLE init_serial_port(char *port_str, int baudrate, int n_bits, int parity, int stops, int RTSCTS)
{
    int k, pb = 0;
    HANDLE hCom_port;
    DWORD dwErrors;
    COMSTAT comStat;

    t_rs = my_uclock();
    dt_rs = get_my_uclocks_per_sec() / 1000;

    strcpy(str_com, port_str);

    //SecureZeroMemory(&lpCC.dcb, sizeof(DCB));//test if allocation issue in dcb (only declaration of lpCC in .h file)
	hCom_port = CreateFile(str_com, GENERIC_READ | GENERIC_WRITE, 0, NULL,
               OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, NULL);
    PurgeComm(hCom_port, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);

    if (GetCommState(hCom_port, &lpCC.dcb) == 0)
    {
        win_printf("GetCommState FAILED\n port %s baudrate %d \nbits %d parity %d stops %d RTSCTS %d\n"
                   , port_str, baudrate, n_bits, parity, stops, RTSCTS);
        return NULL;
    }

    /* Initialisation des parametres par defaut */
    /*http://msdn.microsoft.com/library/default.asp?url=/library/en-us/devio/base/dcb_str.asp*/
    //win_printf("lpCC.dcb.BaudRate = %d",lpCC.dcb.BaudRate);
    lpCC.dcb.BaudRate = CBR_57600;//baudrate//CBR_115200;
    //lpCC.dcb.BaudRate = CBR_115200;
    //win_printf("lpCC.dcb.BaudRate = %d",lpCC.dcb.BaudRate);
    lpCC.dcb.ByteSize = n_bits; //8;
    lpCC.dcb.StopBits = (stops == 1) ? ONESTOPBIT : TWOSTOPBITS;//stops;//ONESTOPBIT;
    lpCC.dcb.Parity = parity;//NOPARITY;
    lpCC.dcb.fOutX = FALSE;
    lpCC.dcb.fInX = FALSE;


    if (RTSCTS == 0)
    {
        lpCC.dcb.fDtrControl = DTR_CONTROL_DISABLE;//DTR_CONTROL_HANDSHAKE or DTR_CONTROL_DISABLE;
        lpCC.dcb.fRtsControl = RTS_CONTROL_DISABLE;//RTS_CONTROL_HANDSHAKE
    }
    else
    {
        lpCC.dcb.fDtrControl = DTR_CONTROL_HANDSHAKE;// or DTR_CONTROL_DISABLE;
        lpCC.dcb.fRtsControl = RTS_CONTROL_HANDSHAKE;
        lpCC.dcb.fOutxCtsFlow = TRUE;
    }


    if (SetCommState(hCom_port, &lpCC.dcb) == 0)
    {
        win_printf("SetCommState FAILED\n port %s baudrate %d \nbits %d parity %d stops %d RTSCTS %d\n"
                   , port_str, baudrate, n_bits, parity, stops, RTSCTS);
        return NULL;
    }

    //Delay(60);

    if (GetCommTimeouts(hCom_port, &lpTo) == 0)
    {
        win_printf("GetCommTimeouts FAILED\n port %s baudrate %d \nbits %d parity %d stops %d RTSCTS %d\n"
                   , port_str, baudrate, n_bits, parity, stops, RTSCTS);
        return NULL;
    }

    lpTo.ReadIntervalTimeout = timeout;   // 100 ms max entre characters
    lpTo.ReadTotalTimeoutMultiplier = 1;
    lpTo.ReadTotalTimeoutConstant = 1;
    lpTo.WriteTotalTimeoutMultiplier = 1;
    lpTo.WriteTotalTimeoutConstant = 1;

    if (SetCommTimeouts(hCom_port, &lpTo) == 0)
    {
        win_printf("SetCommTimeouts FAILED\n port %s baudrate %d \nbits %d parity %d stops %d RTSCTS %d\n"
                   , port_str, baudrate, n_bits, parity, stops, RTSCTS);
        return NULL;
    }

    //    we check that the rs232 port is ready for writing

    if (SetupComm(hCom_port, 2048, 2048) == 0)
    {
        win_printf("Init Serial port %s FAILED", port_str);

        if (debug)
        {
            fp_debug = fopen(debug_file_w_path, "a");

            if (fp_debug != NULL)
            {
                fprintf(fp_debug, "RS232 init port failed IM %d <-- \n", im_ci());
                fclose(fp_debug);
            }
        }

        CloseSerialPort(hCom_port);
        hCom_port = NULL;
        return NULL;
    }

    k = ClearCommError(hCom, &dwErrors, &comStat);

    if (k && comStat.fCtsHold)  pb = 1;//win_printf(" Tx waiting for CTS signal");

    if (k && comStat.fDsrHold)  pb = 2;

    if (k && comStat.fDsrHold)  pb = 3;

    if (k && comStat.fRlsdHold) pb = 4;

    if (k && comStat.fXoffHold) pb = 5;

    if (k && comStat.fXoffSent) pb = 6;

    if (k && comStat.fEof)      pb = 7;

    if (k && comStat.fTxim)     pb = 8;

    if (k && comStat.cbInQue)   pb = 9;

    if (k && comStat.cbOutQue)  pb = 10;

    if (pb != 0)
    {
        win_printf("Init Serial port %s went Ok but the port in not ready for Writting\n"
                   "Check if the rs232 cable is correctly connected pb %d", port_str, pb);

        if (debug)
        {
            fp_debug = fopen(debug_file_w_path, "a");

            if (fp_debug != NULL)
            {
                fprintf(fp_debug, "RS232 init port Ok but not ready for writting IM %d <-- \npb %d\n", im_ci(), pb);
                fclose(fp_debug);
            }
        }

        /*
        CloseSerialPort(hCom_port);
        hCom_port = NULL;
            return NULL;
        */
    }

    if (debug)
    {
        fp_debug = fopen(debug_file_w_path, "a");

        if (fp_debug != NULL)
        {
            fprintf(fp_debug, "RS232 init successfull IM %d <-- \n", im_ci());
            fclose(fp_debug);
        }
    }

    my_set_window_title("Init Serial port %s OK", port_str);
    return hCom_port;
}


int Write_serial_port(HANDLE handle, char *data, DWORD length)
{
    int success = 0, k, pb = 0;
    DWORD dwWritten = 0, dwWritten2 = 0;
    OVERLAPPED o = {0};
    DWORD dwErrors;
    COMSTAT comStat;


    k = ClearCommError(hCom, &dwErrors, &comStat);

    if (k && comStat.fCtsHold)  pb = 1;//win_printf(" Tx waiting for CTS signal");

    if (k && comStat.fDsrHold)  pb = 2;

    if (k && comStat.fDsrHold)  pb = 3;

    if (k && comStat.fRlsdHold) pb = 4;

    if (k && comStat.fXoffHold) pb = 5;

    if (k && comStat.fXoffSent) pb = 6;

    if (k && comStat.fEof)      pb = 7;

    if (k && comStat.fTxim)     pb = 8;

    if (k && comStat.cbInQue)   pb = 9;

    if (k && comStat.cbOutQue)  pb = 10;

# ifdef JF_DEBUG

    if (pb) win_printf("Error writting %d", pb);

# endif


    /*
    if (k && comStat.fCtsHold)  pb = 1;//win_printf(" Tx waiting for CTS signal");
    if (k && comStat.fDsrHold)  win_printf(" Tx waiting for DSR signal");
    if (k && comStat.fRlsdHold) win_printf(" Tx waiting for RLSD signal");
    if (k && comStat.fXoffHold) win_printf(" Tx waiting, XOFF char rec'd");
    if (k && comStat.fXoffSent) win_printf(" Tx waiting, XOFF char sent");
    if (k && comStat.fEof)      win_printf(" EOF character received");
    if (k && comStat.fTxim)     win_printf(" Character waiting for Tx; char queued with TransmitCommChar");
    if (k && comStat.cbInQue)   win_printf(" comStat.cbInQue bytes have been received, but not read");
    if (k && comStat.cbOutQue)  win_printf(" comStat.cbOutQue bytes are awaiting transfer");
    */

    if (pb != 0)
    {
        if (debug)
        {
            fp_debug = fopen(debug_file_w_path, "a");

            if (fp_debug != NULL)
            {
                fprintf(fp_debug, ">pb before writting pb = %d\n", pb);
                fclose(fp_debug);
            }
        }

        //      return -1;
    }

    // (pb == 0)

    o.hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

    if (!WriteFile(handle, (LPCVOID)data, length, &dwWritten, &o))
    {
        if (GetLastError() == ERROR_IO_PENDING)
            if (WaitForSingleObject(o.hEvent, timeout) == WAIT_OBJECT_0)
                if (GetOverlappedResult(handle, &o, &dwWritten2, FALSE))
                    success = 1;
    }
    else    success = 1;

    if (dwWritten != length)     success = 0;

    if (dwWritten2 == length)     success = 1;

    CloseHandle(o.hEvent);


    if (debug)
    {
        fp_debug = fopen(debug_file_w_path, "a");

        if (fp_debug != NULL)
        {
            if (success == 0)
                fprintf(fp_debug, ">pb writting %s : %d/%d char instead of %d\n", data, (int)dwWritten, (int)dwWritten2, (int)length);
            else fprintf(fp_debug, "IM %d >\t sent succesfully: %s at (t = %g)\n", im_ci(), data,(double)(my_uclock() - t_rs) / dt_rs);
	    t_sen = my_uclock();
            fclose(fp_debug);
        }
    }

    return (success == 0) ? -1 : ((dwWritten < dwWritten2) ? dwWritten2 : dwWritten);
}


int ReadData(HANDLE handle, BYTE *data, DWORD length, DWORD *dwRead)
{
    int success = 0;
    OVERLAPPED o = {0};

    o.hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

    if (!ReadFile(handle, data, length, dwRead, &o))
    {
        if (GetLastError() == ERROR_IO_PENDING)
            if (WaitForSingleObject(o.hEvent, timeout) == WAIT_OBJECT_0)
                success = 1;

        GetOverlappedResult(handle, &o, dwRead, FALSE);
    }
    else    success = 1;

    CloseHandle(o.hEvent);
    return success;
}

int Read_serial_port(HANDLE handle, char *stuff, DWORD max_size, DWORD *dwRead)
{
    register int i, j, k;
    unsigned long t0;
    //  int len;
    BYTE lch[2] = {0};//, chtmp[128];
    int timeoutl = 0;
    DWORD dwErrors;
    COMSTAT comStat;

    if (handle == NULL) return -2;


    t0 = get_my_uclocks_per_sec() / 20; // we cannot wait more than 50 ms
    t0 += my_uclock();

    for (i = j = 0; j == 0 && i < (int)max_size - 1 && timeoutl == 0;)
    {
        *dwRead = 0;

        if (ReadData(handle, lch, 1, dwRead) == 0)
        {
            stuff[i] = 0;
            *dwRead = i;
            k = ClearCommError(hCom, &dwErrors, &comStat);

            //if (k && comStat.cbInQue)   Read_serial_port(hCom,chtmp,127,&len);
            //if (j && comStat.fCtsHold)  my_set_window_title(" Tx waiting for CTS signal");
            //if (j && comStat.fDsrHold)  my_set_window_title(" Tx waiting for DSR signal");
            //if (j && comStat.fRlsdHold) my_set_window_title(" Tx waiting for RLSD signal");
            //if (j && comStat.fXoffHold) my_set_window_title(" Tx waiting, XOFF char rec'd");
            //if (j && comStat.fXoffSent) my_set_window_title(" Tx waiting, XOFF char sent");
            //if (j && comStat.fEof)      my_set_window_title(" EOF character received");
            if (k && comStat.fTxim)     my_set_window_title(" Character waiting for Tx; char queued with TransmitCommChar");

            //if (j && comStat.cbInQue)   my_set_window_title(" comStat.cbInQue bytes have been received, but not read");
            if (k && comStat.cbOutQue)  my_set_window_title(" comStat.cbOutQue bytes are awaiting transfer");

            if (debug)
            {
                fp_debug = fopen(debug_file_w_path, "a");

                if (fp_debug != NULL)
                {
                    fprintf(fp_debug, "ReadData error IM %d <-- \n", im_ci());
                    fprintf(fp_debug, "IM %d <\t %s (t = %g) dt = %g\n"
			    , im_ci(), stuff, (double)(my_uclock() - t_rs) / dt_rs
			    , (double)(my_uclock() - t_sen) / dt_rs);
                    fclose(fp_debug);
                }
            }



            //return -1;
        }

        /*
        if (*dwRead < 1)
        {
          stuff[i] = 0;
          *dwRead = i;
          if (debug)
            {
              fp_debug = fopen(debug_file,"a");
              if (fp_debug != NULL)
           {
             fprintf (fp_debug,"<-- ReadData 0 char read\n");
             fprintf (fp_debug,"<\t %s\n",stuff);
             fclose(fp_debug);
           }
            }
          return -2;
        }
             */
        if (*dwRead == 1)
        {
            stuff[i] = lch[0];
            j = (lch[0] == '\n') ? 1 : 0;
            i++;
        }

        timeoutl = (t0 > my_uclock()) ? 0 : 1;
    }

    stuff[i] = 0;
    *dwRead = i;

    if (debug)
    {
        fp_debug = fopen(debug_file_w_path, "a");

        if (fp_debug != NULL)
        {
            if (timeoutl) fprintf(fp_debug, "IM %d <\t timeout %s (t = %g)\n", im_ci(), stuff, (double)(my_uclock() - t_rs) / dt_rs);
            else fprintf(fp_debug, "IM %d <\t %s (t = %g) dt = %g \n"
			 , im_ci(), stuff, (double)(my_uclock() - t_rs) / dt_rs
			 , (double)(my_uclock() - t_sen) / dt_rs);

            fclose(fp_debug);
        }
    }

    return (timeoutl) ? -2 : i;
}




int sent_cmd_and_read_answer(HANDLE hComl, char *Command, char *answer, DWORD n_answer, DWORD *n_written,
                             DWORD *dwErrors, unsigned long *t0)
{
    int rets = 0, ret = 0, i = 0, j = 0;
    COMSTAT comStat = {0};
    char l_command[128] = {0};
 char chtmp[128] = {0}; // , ch[2]
    DWORD len = 0;

    if (hComl == NULL || Command == NULL || answer == NULL || strlen(Command) < 1) return 1;

    for (i = 0, j = 1; i < 5 && j != 0; i++)
    {
        j = ClearCommError(hComl, dwErrors, &comStat);

        if (j && comStat.cbInQue)   Read_serial_port(hComl, chtmp, 127, &len);

        //if (j && comStat.fCtsHold)  my_set_window_title(" Tx waiting for CTS signal");
        //if (j && comStat.fDsrHold)  my_set_window_title(" Tx waiting for DSR signal");
        //if (j && comStat.fRlsdHold) my_set_window_title(" Tx waiting for RLSD signal");
        //if (j && comStat.fXoffHold) my_set_window_title(" Tx waiting, XOFF char rec'd");
        //if (j && comStat.fXoffSent) my_set_window_title(" Tx waiting, XOFF char sent");
        //if (j && comStat.fEof)      my_set_window_title(" EOF character received");
        if (j && comStat.fTxim)     my_set_window_title(" Character waiting for Tx; char queued with TransmitCommChar");

        //if (j && comStat.cbInQue)   my_set_window_title(" comStat.cbInQue bytes have been received, but not read");
        if (j && comStat.cbOutQue)  my_set_window_title(" comStat.cbOutQue bytes are awaiting transfer");
    }


    for (i = 0; i < 126 && Command[i] != 0 && Command[i] != 13; i++)
        l_command[i] = Command[i]; // we copy until CR

    l_command[i++] = 13;         // we add it
    l_command[i] = 0;            // we end string

    if (t0) *t0 = my_uclock();

    /*
    for (i = 0; i < 126 && l_command[i] != 0; i++)
      {
        ch[0] = l_command[i];
        ch[1] = 0;
        rets = Write_serial_port(hCom,ch,1);
        if (rets <= 0)
    {
      if (t0) *t0 = my_uclock() - *t0;
      // Get and clear current errors on the port.
      if (ClearCommError(hCom, dwErrors, &comStat))
        {
          if (comStat.cbInQue)  Read_serial_port(hCom,chtmp,127,&len);
          return -2;
        }
      return -1;
    }
      }
    */

    rets = Write_serial_port(hComl, l_command, strlen(l_command));

    if (rets <= 0)
    {
        if (t0) *t0 = my_uclock() - *t0;

        // Get and clear current errors on the port.
        if (ClearCommError(hComl, dwErrors, &comStat))
        {
            if (comStat.cbInQue)  Read_serial_port(hComl, chtmp, 127, &len);

            //if (comStat.fCtsHold)  my_set_window_title(" Tx waiting for CTS signal");
            //if (comStat.fDsrHold)  my_set_window_title(" Tx waiting for DSR signal");
            //if (comStat.fRlsdHold) my_set_window_title(" Tx waiting for RLSD signal");
            //if (comStat.fXoffHold) my_set_window_title(" Tx waiting, XOFF char rec'd");
            //if (comStat.fXoffSent) my_set_window_title(" Tx waiting, XOFF char sent");
            //if (comStat.fEof)      my_set_window_title(" EOF character received");
            if (comStat.fTxim)     my_set_window_title(" Character waiting for Tx; char queued with TransmitCommChar");

            //if (comStat.cbInQue)   my_set_window_title(" comStat.cbInQue bytes have been received, but not read");
            if (comStat.cbOutQue)  my_set_window_title(" comStat.cbOutQue bytes are awaiting transfer");

            return -2;
        }

        return -1;
    }

    *n_written = 0;
    ret = Read_serial_port(hComl, answer, n_answer, n_written);

    for (; ret == 0;)
        ret = Read_serial_port(hComl, answer, n_answer, n_written);

    if (t0) *t0 = my_uclock() - *t0;

    answer[*n_written] = 0;
    strncpy(last_answer_2, answer, 127);

    if (ret < 0)
    {
        // Get and clear current errors on the port.
        if (ClearCommError(hComl, dwErrors, &comStat))
        {
            if (comStat.cbInQue)  Read_serial_port(hComl, chtmp, 127, &len);

            //if (comStat.fCtsHold)  my_set_window_title(" Tx waiting for CTS signal");
            //if (comStat.fDsrHold)  my_set_window_title(" Tx waiting for DSR signal");
            //if (comStat.fRlsdHold) my_set_window_title(" Tx waiting for RLSD signal");
            //if (comStat.fXoffHold) my_set_window_title(" Tx waiting, XOFF char rec'd");
            //if (comStat.fXoffSent) my_set_window_title(" Tx waiting, XOFF char sent");
            //if (comStat.fEof)      my_set_window_title(" EOF character received");
            if (comStat.fTxim)     my_set_window_title(" Character waiting for Tx; char queued with TransmitCommChar");

            //if (comStat.cbInQue)   my_set_window_title(" comStat.cbInQue bytes have been received, but not read");
            if (comStat.cbOutQue)  my_set_window_title(" comStat.cbOutQue bytes are awaiting transfer");

            return 2;
        }

        return 1;
    }

    return 0;
}

# ifdef PREVIOUS
int sent_cmd_and_read_answer(HANDLE hComl, char *Command, char *answer, DWORD n_answer, DWORD *n_written,
                             DWORD *dwErrors, unsigned long *t0)
{
    int rets, ret, i, j;
    COMSTAT comStat;
    char l_command[128] = {0}, ch[2] = {0}, chtmp[128] = {0};
    DWORD len = 0;

    if (hComl == NULL || Command == NULL || answer == NULL || strlen(Command) < 1) return 1;

    for (i = 0, j = 1; i < 5 && j != 0; i++)
    {
        j = ClearCommError(hComl, dwErrors, &comStat);

        if (j && comStat.cbInQue)  Read_serial_port(hComl, chtmp, 127, &len);
    }


    for (i = 0; i < 126 && Command[i] != 0 && Command[i] != 13; i++)
        l_command[i] = Command[i]; // we copy until CR

    l_command[i++] = 13;         // we add it
    l_command[i] = 0;            // we end string

    if (t0) *t0 = my_uclock();

    rets = Write_serial_port(hComl, l_command, strlen(l_command));

    if (rets <= 0)
    {
        if (t0) *t0 = my_uclock() - *t0;

        // Get and clear current errors on the port.
        if (ClearCommError(hComl, dwErrors, &comStat))
        {
            if (comStat.cbInQue)  Read_serial_port(hComl, chtmp, 127, &len);

            return -2;
        }

        return -1;
    }

    *n_written = 0;
    ret = Read_serial_port(hComl, answer, n_answer, n_written);

    for (; ret == 0;)
        ret = Read_serial_port(hComl, answer, n_answer, n_written);

    if (t0) *t0 = my_uclock() - *t0;

    answer[*n_written] = 0;

    if (ret < 0)
    {
        // Get and clear current errors on the port.
        if (ClearCommError(hComl, dwErrors, &comStat))
        {
            if (comStat.cbInQue)  Read_serial_port(hComl, chtmp, 127, &len);

            return 2;
        }

        return 1;
    }

    return 0;
}

# endif

int talk_serial_port(HANDLE hCom_port, char *command, int wait_answer, char *answer, DWORD NumberOfBytesToWrite,
                     DWORD nNumberOfBytesToRead)
{
    //int NumberOfBytesWrittenOnPort = -1;
    unsigned long NumberOfBytesWritten = -1;


    if (hCom == NULL) return win_printf_OK("No serial port init");

    if (wait_answer != READ_ONLY)
    {
        //NumberOfBytesWrittenOnPort =
        Write_serial_port(hCom_port, command, NumberOfBytesToWrite);
    }

    if (wait_answer != WRITE_ONLY)
    {
        Read_serial_port(hCom_port, answer, nNumberOfBytesToRead, &NumberOfBytesWritten);

    }

    return 0;
}

/*
Attemp to read the pico response from serial port
if a full answer is available then transfer it
otherwise grab characters and respond that full message is not yet there

*/

char *catch_pending_request_answer(int *size, int im_n, int *error, unsigned long *t0)
{
    static char answer[128] = {0}, request_answer[128] = {0};
    unsigned long dwRead = 0;
    static int i = 0;
    int j, max_size = 128, lf_found = 0;
    BYTE lch[2] = {0};

    if (t0 != NULL) *t0 = my_uclock();

    if (hCom == NULL)
    {
        *error = -3;

        if (t0 != NULL) *t0 = my_uclock() - *t0;

        return NULL;   // no serial port !
    }

    for (j = 0; j == 0 && i < max_size - 1;)
    {
        // we grab rs232 char already arrived
        dwRead = 0;

        if (ReadData(hCom, lch, 1, &dwRead) == 0)
        {
            // something wrong
            answer[i] = 0;
            dwRead = i;

            if (debug)
            {
                fp_debug = fopen(debug_file_w_path, "a");

                if (fp_debug != NULL)
                {
                    fprintf(fp_debug, "IM %d <-- ReadData error at im %d\n", im_ci(), im_n);
                    fprintf(fp_debug, "IM %d <\t %s\n", im_ci(), answer);
                    fclose(fp_debug);
                }
            }

            *error = -2;

            if (t0 != NULL) *t0 = my_uclock() - *t0;

            return NULL;    // serial port read error !
        }

        if (dwRead == 1)
        {
            answer[i] = lch[0];
            j = lf_found = (lch[0] == '\n') ? 1 : 0;
            i++;
        }
        else if (dwRead == 0) j = 1;
    }

    answer[i] = 0;
    *size = i;

    if (lf_found)
    {
        *error = 0;
        strncpy(request_answer, answer, i);

        if (debug)
        {
            fp_debug = fopen(debug_file_w_path, "a");

            if (fp_debug != NULL)
            {
                fprintf(fp_debug, "Im %d : <\t %s\n", im_n, request_answer);
                fclose(fp_debug);
            }
        }

        if (t0 != NULL) *t0 = my_uclock() - *t0;

        i = 0;
        return request_answer;
    }

    *error = 1;

    if (debug)
    {
        fp_debug = fopen(debug_file_w_path, "a");

        if (fp_debug != NULL)
        {
            fprintf(fp_debug, "Im %d : <\t %s...\n", im_n, answer);
            fclose(fp_debug);
        }
    }

    if (t0 != NULL) *t0 = my_uclock() - *t0;

    return NULL;
}




int n_write_read_serial_port(void)
{
    static char Command[128] = "dac16?";
    static int ntimes = 1, i;
    unsigned long t0;
    double dt, dtm, dtmax;
    char resu[128], *ch;
    int ret = 0;
    unsigned long lpNumberOfBytesWritten = 0;
    DWORD nNumberOfBytesToRead = 127;

    if (updating_menu_state != 0)    return D_O_K;

    if (hCom == NULL) return win_printf_OK("No serial port init");

    for (ch = Command; *ch != 0; ch++)
        if (*ch == '\r') *ch = 0;

    win_scanf("Command to send? %snumber of times %d", &Command, &ntimes);

    strncat(Command, "\r", (sizeof(Command) > strlen(Command)) ? sizeof(Command) - strlen(Command) : 0);

    for (i = 0, dtm = dtmax = 0; i < ntimes; i++)
    {
        t0 = my_uclock();
        Write_serial_port(hCom, Command, strlen(Command));

        ret = Read_serial_port(hCom, resu, nNumberOfBytesToRead, &lpNumberOfBytesWritten);
        t0 = my_uclock() - t0;
        dt = 1000 * (double)(t0) / get_my_uclocks_per_sec();
        resu[lpNumberOfBytesWritten] = 0;
        dtm += dt;

        if (dt > dtmax) dtmax = dt;
    }

    dtm /= ntimes;
    win_printf("command read = %s \n ret %d lpNumberOfBytesWritten = %ld\nin %g mS in avg %g max", resu, ret,
               lpNumberOfBytesWritten, dtm, dtmax);
    return D_O_K;
}

int purge_com(void)
{
    if (hCom == NULL) return 1;

    return PurgeComm(hCom, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);
}
int init_pico_serial(void)
{
    int i, j;
    static int port_number = 1, sbaud = 0, hand = 0, parity = 0, stops = 1, n_bits = 8;
    char command[128], answer[128], *gr, *s;
    int  ret, iret;
    unsigned long len = 0;
    static int first_init = 1;
    char port_str[256] = {0};

# ifdef JF_DEBUG
    win_printf("Entering init_pico_serial");
# endif

    if (hCom != NULL) return 0;

    if (first_init == 0) return 1;

    first_init = 0;
    // we only get here once
# ifdef RT_DEBUG
    debug = 1;
#endif
    answer[0] = 0;
    port_number = Pico_param.serial_param.serial_port;
    parity = Pico_param.serial_param.serial_Parity;
    stops = Pico_param.serial_param.serial_StopBits;
    n_bits = Pico_param.serial_param.serial_ByteSize;
    sbaud = Pico_param.serial_param.serial_BaudRate;

    sbaud /= 57600;
    sbaud--;
    hand = Pico_param.serial_param.serial_fDtrControl;

    if (state_is_pico() == 0)
    {
        i = win_scanf("Your interface device has been switched to a dummy mode\n"
                      "in the previous experiment. This is a debugging mode\n"
                      "which does not allow normal operation. Do you want to\n"
                      "switch back your configuration to the normal state ?");

        if (i == WIN_OK)  switch_from_pico_serial_to_dummy(1);
    }



    if (Pico_param.serial_param.rs232_started_fine == 0)
    {
# ifdef JF_DEBUG
        win_printf("You enter here because the last time your program started\n"
                   "the serial interface did not run properly\n"
                   "You will be ask to enter serial parameters\n");
# endif

        for (; hCom == NULL;)
        {
            i = win_scanf("{\\color{yellow}Your Picotwist serial interface did not open properly on last trial!} Check:\n"
                          " - that another program is not using the same serial port\n"
                          " - Another instance of Picojai.exe may still be running\n"
                          "    If this is the case open the Windows task manager and kill the process\n"
                          " - Last but not least your configuration may not be correct\n"
                          "If this is the case select a new one. {\\color{yellow}Port number:} ?%5d\n"
                          "Dump in and out in a debug file No %R yes %r\n"
                          "{\\color{yellow}Baudrate:} 57.6k %R 115.2k %r 230.4k %r 460.8k %r 921.6k %r (default 57.6k)\n"
                          "{\\color{yellow}Number of bits:} %4d  (default 8)\n"
                          "{\\color{yellow}Parity:} No %R even %r odd %r (default No)\n"
                          "{\\color{yellow}Number of stop bits:} 0->%R 1->%r 2->%r (default 1)\n"
                          "{\\color{yellow}HandShaking:} None %R Cts/Rts %r (default Cts/Rts)\n"
                          "{\\color{lightred}If you hit cancel a dummy interface will be used\n"
                          "This only allows you to check your camera}\n"
                          , &port_number, &debug, &sbaud, &n_bits, &parity, &stops, &hand);

            if (i == WIN_CANCEL)
            {
                switch_from_pico_serial_to_dummy(0);
                Pico_param.serial_param.rs232_started_fine = 0;
                write_Pico_config_file();
                return 1;
            }

            sprintf(port_str, "\\\\.\\COM%d\0", port_number);
            hCom = init_serial_port(port_str, (sbaud + 1) * 57600, n_bits, parity , stops, hand);
        }
    }

    if (hCom == NULL)
    {
# ifdef JF_DEBUG
        win_printf("init_serial_port %d, baud %d bits %d parity %d\n stops %d hand %d", port_number, (sbaud + 1) * 57600,
                   n_bits, parity , stops, hand);
# endif

        sprintf(port_str, "\\\\.\\COM%d\0", port_number);
        hCom = init_serial_port(port_str, (sbaud + 1) * 57600, n_bits, parity , stops, hand);

# ifdef JF_DEBUG

        if (hCom == NULL)
            win_printf("Bad port init");
        else  win_printf("port init Ok");

# endif
    }

    for (; hCom == NULL;)
    {
# ifdef JF_DEBUG
        win_printf("The port initialization was not successful\n");
# endif
        i = win_scanf("{\\color{yellow}Your Picotwist serial interface is not opening properly!} Check:\n"
                      " - that another program is not using the same serial port\n"
                      " - Another instance of Picojai.exe may still be running\n"
                      "    If this is the case open the Windows task manager and kill the process\n"
                      " - Last but not least your configuration may not be correct\n"
                      "If this is the case select a new one. {\\color{yellow}Port number:} ?%5d\n"
                      "Dump in and out in a debug file No %R yes %r\n"
                      "{\\color{yellow}Baudrate:} 57.6k %R 115.2k %r 230.4k %r 460.8k %r 921.6k %r (default 57.6k)\n"
                      "{\\color{yellow}Number of bits:} %4d  (default 8)\n"
                      "{\\color{yellow}Parity:} No %R even %r odd %r (default No)\n"
                      "{\\color{yellow}Number of stop bits:} 0->%R 1->%r 2->%r (default 1)\n"
                      "{\\color{yellow}HandShaking:} None %R Cts/Rts %r (default Cts/Rts)\n"
                      "{\\color{lightred}If you hit cancel a dummy interface will be used\n"
                      "This only allows you to check your camera}\n"
                      , &port_number, &debug, &sbaud, &n_bits, &parity, &stops, &hand);

        if (i == WIN_CANCEL)
        {
            switch_from_pico_serial_to_dummy(0);
            Pico_param.serial_param.rs232_started_fine = 0;
            write_Pico_config_file();
            return 1;
        }

        sprintf(port_str, "\\\\.\\COM%d\0", port_number);
        hCom = init_serial_port(port_str, (sbaud + 1) * 57600, n_bits, parity , stops, hand);
    }

    if (hCom == NULL)
    {
# ifdef JF_DEBUG
        win_printf("the serial interface did not stated properly\n"
                   "We switch to Dummy\n");
# endif
        switch_from_pico_serial_to_dummy(0);
        Pico_param.serial_param.rs232_started_fine = 0;
        write_Pico_config_file();
        return 1;
    }

    if (debug)
    {
        fp_debug = fopen(debug_file_w_path, "a");

        if (fp_debug != NULL) fclose(fp_debug);
    }

# ifdef JF_DEBUG
    //win_printf("Before port purge");
# endif
    purge_com();
# ifdef JF_DEBUG
    // win_printf("after port purge");
# endif

    sprintf(command, "\r");

    for (i = 3, j = Write_serial_port(hCom, command, strlen(command)); j < 0 && i > 0; i--)
        j = Write_serial_port(hCom, command, strlen(command));

# ifdef JF_DEBUG
    win_printf("after writing end of line to port");
# endif
    Sleep(50);

    for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
        ret = Read_serial_port(hCom, answer, 127, &len);

    answer[len] = 0;

# ifdef JF_DEBUG
    win_printf("after reading end of line to port");
# endif

    sprintf(command, "echo=0\r");

    for (j = Write_serial_port(hCom, command, strlen(command)); j < 0;)
    {
# ifdef JF_DEBUG
        win_printf("You have initialized your serial port OK\n"
                   "but sending the first command 'echo=0' failed!\n"
                   "You will be ask to enter serial parameters\n");
        Pico_param.serial_param.rs232_started_fine = 0;
        write_Pico_config_file();
        CloseSerialPort(hCom);
        win_printf("after closing serial port");
        return 1;
        //exit(0);
# endif


        i = win_scanf("{\\color{yellow}Your Picotwist device is not recieving data from your PC properly!}\n"
                      "Check:\n - that you have switched on your device ;-)\n"
                      " - that you have hook up properly your PC to the device.\n"
                      " - that the Picotwist micro-controller is responding: switch the device off\n"
                      "and then on again and restart this program.\n"
                      " - Last but not least your configuration may not be correct\n"
                      "If this is the case select a new one. {\\color{yellow}Port number:} ?%5d\n"
                      "Dump in and out in a debug file No %R yes %r\n"
                      "{\\color{yellow}Baudrate:} 57.6k %R 115.2k %r 230.4k %r 460.8k %r 921.6k %r (default 57.6k)\n"
                      "{\\color{yellow}Number of bits:} %4d  (default 8)\n"
                      "{\\color{yellow}Parity:} No %R even %r odd %r (default No)\n"
                      "{\\color{yellow}Number of stop bits:} 0->%R 1->%r 2->%r (default 1)\n"
                      "{\\color{yellow}HandShaking:} None %R Cts/Rts %r (default Cts/Rts)\n"
                      "{\\color{lightred}If you hit cancel a dummy interface will be used\n"
                      "This only allows you to check your camera}\n"
                      , &port_number, &debug, &sbaud, &n_bits, &parity, &stops, &hand);

        if (i == WIN_CANCEL)
        {
            switch_from_pico_serial_to_dummy(0);
            Pico_param.serial_param.rs232_started_fine = 0;
            write_Pico_config_file();
            return 1;
        }

        CloseSerialPort(hCom);
        sprintf(port_str, "\\\\.\\COM%d\0", port_number);
        hCom = init_serial_port(port_str, (sbaud + 1) * 57600, n_bits, parity , stops, hand);

        if (hCom == NULL)
        {
# ifdef JF_DEBUG
            win_printf("You new initialization of your serial port OK failed again\n"
                       "switching to Dummy\n");
# endif
            switch_from_pico_serial_to_dummy(0);
            Pico_param.serial_param.rs232_started_fine = 0;
            write_Pico_config_file();
            return 1;
        }

        j = Write_serial_port(hCom, command, strlen(command));
    }

    Sleep(50);

    for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
        ret = Read_serial_port(hCom, answer, 127, &len);

    answer[len] = 0;
    gr = strstr(answer, "Echo off");

    if (gr == NULL)
    {
        sprintf(command, "\r");
        j = Write_serial_port(hCom, command, strlen(command));
        Sleep(50);

        for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
            ret = Read_serial_port(hCom, answer, 127, &len);

        answer[len] = 0;

        sprintf(command, "echo=0\r");
        j = Write_serial_port(hCom, command, strlen(command));
        Sleep(50);

        for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
            ret = Read_serial_port(hCom, answer, 127, &len);

        answer[len] = 0;
    }

    gr = strstr(answer, "Echo off");

    if (gr == NULL)
    {
# ifdef JF_DEBUG
        win_printf("You have a problem while reading the reply to the first\n"
                   "command 'echo=0' your device replied %d characters:\n>%s<\n"
                   "We shall try again initializing PicoTwist device\n", len, answer);

        sprintf(command, "\r");
        j = Write_serial_port(hCom, command, strlen(command));
        Sleep(50);

        for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
            ret = Read_serial_port(hCom, answer, 127, &len);

        answer[len] = 0;

        sprintf(command, "echo=0\r");
        j = Write_serial_port(hCom, command, strlen(command));
        Sleep(50);

        for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
            ret = Read_serial_port(hCom, answer, 127, &len);

        answer[len] = 0;

        gr = strstr(answer, "Echo off");

        if (gr == NULL)
        {
            win_printf("You still have a problem ! Serial port initialized properly\n"
                       "Sending data went smootly but the reply was not the one expected\n"
                       "command 'echo=0' your device replied %d characters:\n>%s<\n"
                       "This error can occur if the PicoTwist device is not connected\n"
                       "or not running or it needs to be restarted\n"
                       , len, answer);
        }

        Pico_param.serial_param.rs232_started_fine = 0;
        write_Pico_config_file();
        win_printf("exiting");
        return 1;
        //exit(0);
# endif
        i = win_scanf("{\\color{yellow}Your Picotwist device is not responding properly to the initialization!}\n"
                      "Check:\n - that you have switched on your device ;-)\n"
                      " - that you have hook up properly your PC to the device.\n"
                      " - that the Picotwist micro-controller is responding: switch the device off\n"
                      "   and then on again and restart this program.\n"
                      " - Last but not least your configuration may not be correct\n"
                      "If this is the case select a new one. {\\color{yellow}Port number} ?%5d\n"
                      "Dump in and out in a debug file No %R yes %r\n"
                      "{\\color{yellow}Baudrate:} 57.6k %R 115.2k %r 230.4k %r 460.8k %r 921.6k %r (default 57.6k)\n"
                      "{\\color{yellow}Number of bits:} %4d  (default 8)\n"
                      "{\\color{yellow}Parity:} No %R even %r odd %r (default No)\n"
                      "{\\color{yellow}Number of stop bits:} 0->%R 1->%r 2->%r (default 1)\n"
                      "{\\color{yellow}HandShaking:} None %R Cts/Rts %r (default Cts/Rts)\n"
                      "{\\color{lightred}If you hit cancel a dummy interface will be used\n"
                      "This only allows you to check your camera}\n"
                      , &port_number, &debug, &sbaud, &n_bits, &parity, &stops, &hand);

        if (i == WIN_CANCEL)
        {
            switch_from_pico_serial_to_dummy(0);
            Pico_param.serial_param.rs232_started_fine = 0;
            write_Pico_config_file();
            return 1;
        }

        sprintf(port_str, "\\\\.\\COM%d\0", port_number);
        hCom = init_serial_port(port_str, (sbaud + 1) * 57600, n_bits, parity , stops, hand);

        if (hCom == NULL)
        {
            switch_from_pico_serial_to_dummy(0);
            Pico_param.serial_param.rs232_started_fine = 0;
            write_Pico_config_file();
            return 1;
        }

        j = Write_serial_port(hCom, command, strlen(command));

        if (j < 0)
        {
            switch_from_pico_serial_to_dummy(0);
            Pico_param.serial_param.rs232_started_fine = 0;
            write_Pico_config_file();
            return 1;
        }

        Sleep(50);

        for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
            ret = Read_serial_port(hCom, answer, 127, &len);

        answer[len] = 0;
        gr = strstr(answer, "Echo off ver.");
    }

    if (gr != NULL && strlen(gr + 14) > 3)
    {
        s = gr + 14;

        for (j = 0; s[j] != 0; j++)
            s[j] = (s[j] == '\n' || s[j] == '\r') ? 0 : s[j];

        my_set_window_title("picotwist interface started firmware %s", gr + 14);

        if (Pico_param.serial_param.firmware_version)
            free(Pico_param.serial_param.firmware_version);

        Pico_param.serial_param.firmware_version = strdup(gr + 14);
        Pico_param.serial_param.rs232_started_fine = 1;
        Pico_param.serial_param.serial_port = port_number;
        Pico_param.serial_param.serial_Parity = parity;
        Pico_param.serial_param.serial_StopBits = stops;
        Pico_param.serial_param.serial_ByteSize = n_bits;
        Pico_param.serial_param.serial_BaudRate = (sbaud + 1) * 57600;
        Pico_param.serial_param.serial_fDtrControl = hand;
        Pico_param.serial_param.serial_fRtsControl = hand;
        write_Pico_config_file();
    }
    else
    {
        Pico_param.serial_param.rs232_started_fine = 0;
        write_Pico_config_file();
    }

    add_plot_treat_menu_item("pico_serial", NULL, pico_serial_plot_menu(), 0, NULL);
    return (gr != NULL) ? 0 : 1;
}

int init_port(void)
{
    if (updating_menu_state != 0)    return D_O_K;

    if (hCom == NULL)
    {

        //add_item_to_menu(trackBead_ex_image_menu, "pico_serial", NULL, pico_serial_plot_menu(), 0, NULL);

        add_plot_treat_menu_item("pico_serial", NULL, pico_serial_plot_menu(), 0, NULL);
        add_image_treat_menu_item("pico_serial", NULL, pico_serial_plot_menu(), 0, NULL);
    }

    init_pico_serial();
    return D_O_K;
}



int write_command_on_serial_port(void)
{
    static char Command[128] = "test";
    //char *test="\r";

    if (updating_menu_state != 0)    return D_O_K;

    win_scanf("Command to send? %s", &Command);
    strncat(Command, "\r", (sizeof(Command) > strlen(Command)) ? sizeof(Command) - strlen(Command) : 0);

    //win_printf("deja cela %d\n Command %s",strlen(Command), Command);
    Write_serial_port(hCom, Command, strlen(Command));
    //snprintf(Command,128,"?nm\r");
    //strcat(Command,'\r');
    //win_printf("deja cela %d\n Command %s",strlen(Command), Command);

    //win_printf("%s \n lpNumberOfBytesWritten = %d",Command,lpNumberOfBytesWritten);
    return D_O_K;
}


int read_on_serial_port(void)
{
    char Command[128] = {0};
    unsigned long lpNumberOfBytesWritten = 0;
    int ret = 0;
    DWORD nNumberOfBytesToRead = 127;
    unsigned long t0;

    if (updating_menu_state != 0)    return D_O_K;

    t0 = my_uclock();
    //win_scanf("Command to send? %s",Command);
    ret = Read_serial_port(hCom, Command, nNumberOfBytesToRead, &lpNumberOfBytesWritten);
    t0 = my_uclock() - t0;
    Command[lpNumberOfBytesWritten] = 0;

    win_printf("command read = %s \n ret %d lpNumberOfBytesWritten = %ld\n in %g mS", Command, ret, lpNumberOfBytesWritten,
               1000 * (double)(t0) / get_my_uclocks_per_sec());

    //win_printf("command read = %s \n lpNumberOfBytesWritten = %d",Command,lpNumberOfBytesWritten);
    return D_O_K;
}



int write_read_serial_port(void)
{
    if (updating_menu_state != 0)    return D_O_K;

    write_command_on_serial_port();
    read_on_serial_port();
    return D_O_K;
}




int CloseSerialPort(HANDLE hComl)
{
    if (CloseHandle(hComl) == 0) return win_printf("Close Handle FAILED!");

    return D_O_K;
}

int close_serial_port(void)
{
    if (updating_menu_state != 0)    return D_O_K;

    CloseSerialPort(hCom);
    hCom = NULL;
    return D_O_K;
}




MENU *pico_serial_plot_menu(void)
{
    static MENU mn[32] = {0};

    if (mn[0].text != NULL) return mn;

    add_item_to_menu(mn, "Init serial port", init_port, NULL, 0, NULL);
    add_item_to_menu(mn, "Write serial port", write_command_on_serial_port, NULL, 0, NULL);
    add_item_to_menu(mn, "Read serial port", read_on_serial_port, NULL, 0, NULL);
    add_item_to_menu(mn, "Write read serial port", write_read_serial_port, NULL, 0, NULL);
    add_item_to_menu(mn, "Write read n times", n_write_read_serial_port, NULL, 0, NULL);
    add_item_to_menu(mn, "Close serial port", close_serial_port, NULL, 0, NULL);
    //  add_item_to_menu(mn,"Read Objective position", pico_read_Z,NULL,0,NULL);
    //add_item_to_menu(mn,"Set Objective position", pico_set_Z,NULL,0,NULL);

    return mn;
}

int pico_serial_main(int argc, char **argv)
{
    (void)argc;
    (void)argv;
    init_port();

    if (hCom != NULL) return D_O_K;

    add_plot_treat_menu_item("pico_serial", NULL, pico_serial_plot_menu(), 0, NULL);
    add_image_treat_menu_item("pico_serial", NULL, pico_serial_plot_menu(), 0, NULL);
    return D_O_K;
}

int pico_serial_unload(int argc, char **argv)
{
    (void)argc;
    (void)argv;
    remove_item_to_menu(plot_treat_menu, "pico_serial", NULL, NULL);
    return D_O_K;
}
#endif

#endif
