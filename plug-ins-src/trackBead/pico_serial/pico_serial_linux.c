/*
 *    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
 */

# ifndef _PICO_SERIAL_C_
# define _PICO_SERIAL_C_

# include "allegro.h"
#ifdef XV_WIN32
# include "winalleg.h"
#endif
# include "xvin.h"




/* If you include other plug-ins header do it here*/
# include "../trackBead.h"


/* But not below this define */
# define BUILDING_PLUGINS_DLL

#ifdef XV_WIN32
# undef _PXV_DLL
# define _PXV_DLL   __declspec(dllexport)
#endif

# include "pico_serial.h"

# include "../../cfg_file/Pico_cfg.h"
# include "../action.h"
# include "../../fft2d/fftbtl32n.h"
# include "../fillibbt.h"

# include "../trackBead.h"
# include "../track_util.h"
#include "../timer_thread.h"
#include <termios.h>
unsigned long t_rs = 0, dt_rs;

uint timeout = 200;  // ms

char last_answer_2[128];

int im_ci(void);

int switch_rs232_debug_on(void)
{
    int mode;
    char ffile[2048];

    if (updating_menu_state != 0)
    {
        if (debug == 1)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }

    snprintf(ffile, sizeof(ffile), "%s\\%s", current_write_open_path_picofile, debug_file);
    backslash_to_slash(ffile);
    mode = win_printf("Debugging file: %s\n"
                      "Do you want to reset all information in that file?"
                      , ffile);

    if (mode == WIN_CANCEL) fp_debug = fopen(ffile, "a");
    else   fp_debug = fopen(ffile, "w");

    if (fp_debug == NULL) return 1;

    fclose(fp_debug);
    debug = 1;
    return 0;
}

int switch_rs232_debug_off(void)
{
    if (updating_menu_state != 0)
    {
        if (debug == 0)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }

    debug = 0;
    return 0;
}


com_port_t init_serial_port(char *port, int baudrate, int n_bits, int parity, int stops, int RTSCTS)
{
    int fd = -1;
    struct termios options = {0};

    (void)baudrate;
    (void)n_bits;
    (void)parity;
    (void)stops;
    (void)RTSCTS;
    fd = open(port, O_RDWR | O_NOCTTY | O_NDELAY);

    if (fd == -1)
    {
        error_message_log(debug_file, "can't open RS232 : %s", strerror(errno));
    }

    //else
    //{
    //    fcntl(fd, F_SETFL, 0);
    //}

    if (!isatty(fd))
    {
        close(fd);
        error_message_log(debug_file, "%s is not a tty", port);
        return -1;
    }

    if (tcgetattr(fd, &options) != 0)
    {
        error_message_log(debug_file, "can't get %s attributs, %s ", port, strerror(errno));
        close(fd);
        return -1;
    }


    if (cfsetispeed(&options, B57600) != 0)
    {
        error_message_log(debug_file, "can't set %s input speed, %s", port, strerror(errno));
        close(fd);
        return -1;

    }

    if (cfsetospeed(&options, B57600) != 0)
    {
        error_message_log(debug_file, "can't set %s output speed, %s", port, strerror(errno));
        close(fd);
        return -1;

    }

    options.c_cflag &= ~(CSIZE | CSTOPB | HUPCL | PARENB | PARODD);
    options.c_cflag |= (CS8 | CLOCAL | CREAD | CRTSCTS);
    options.c_iflag &= ~(IXON | IXOFF | IXANY | INLCR | IGNBRK | BRKINT | PARMRK | INLCR | IGNCR | ICRNL);
    options.c_iflag |= (IGNBRK | IGNPAR | INPCK | ISTRIP);
    options.c_oflag &= ~(OPOST | ONLCR);
    options.c_lflag &= ~(ICANON | ISIG | IEXTEN | ECHO | ECHOE | ECHOK | ECHOCTL | ECHOKE);
    //options.c_cc[VMIN] = XXX;
    //options.c_cc[VTIME] = XXX;

    if (tcsetattr(fd, TCSANOW, &options) != 0)
    {
        error_message_log(debug_file, "can't set %s attributs, %s ", port, strerror(errno));
        close(fd);
        return -1;
    }

    psleep(50);

    return fd;
}


int Write_serial_port(com_port_t hComl, char *data, unsigned long length)
{
    int written = write(hComl, data, length);
    char buf[1024] = {0};
    int l = length < sizeof(buf) - 1 ? length : sizeof(buf) - 1;

    if (debug && written < (int) length)
    {
        strncpy(buf, data, l);
        buf[l] = '\0';
        debug_message_log(debug_file, "IM %d: error writing \"%s\" to serial port, %d byte written, %s ", 0/*im_ci()*/,buf, written,
                          strerror(errno));
    }

    return written;
}


int ReadData(com_port_t hComl, char *out_data, unsigned long length, unsigned long *dwRead)
{
    int redl = read(hComl, out_data, length);

    if (redl > -1 && dwRead)
        *dwRead = redl;

    return redl;
}



int Read_serial_port(com_port_t hComl, char *stuff, unsigned long max_size, unsigned long *dwRead)
{
    unsigned long i;
    int j;
    unsigned long t0;
    char buf[2] = {0};
    bool timeisout = false;

    if (hComl < 0)
        return -2;


    t0 = get_my_uclocks_per_sec() / 20; // we cannot wait more than 50 ms
    t0 += my_uclock();

    for (i = j = 0; j == 0 && i < max_size - 1 && !timeisout;)
    {
        *dwRead = 0;

        if (ReadData(hComl, buf, 1, dwRead) == 0)
        {
            stuff[i] = 0;
            *dwRead = i;

        }

        if (*dwRead == 1)
        {
            stuff[i] = buf[0];
            j = (buf[0] == '\n') ? 1 : 0;
            i++;
        }

        timeisout = !(t0 > my_uclock());
    }

    stuff[i] = 0;
    *dwRead = i;

    if (timeisout && debug)
    {
        debug_message_log(debug_file, "IM %d: error reading from serial port, timeout after %d bytes red : %s",
                          0/*im_ci()*/,i, stuff);
    }

    return (timeisout) ? -2 : (int)i;
}

int clear_serial_port(com_port_t hComl)
{
    int i = 0;
    char buf[2] = {0};

    if (hComl < 0)
        return -2;


    while (ReadData(hComl, buf, 1, NULL) > 0 && i < 128)
    {
        i++;
    };


    if (debug && i)
    {
        debug_message_log(debug_file, "clearing serial port : %d junk bytes cleaned", i);
    }

    return 0;
}



int sent_cmd_and_read_answer(com_port_t hComl, char *Command, char *answer, unsigned long n_answer,
                             unsigned long *n_written, unsigned long *dwErrors, unsigned long *t0)
{
    int rets = 0, ret =0, i=0;
    char l_command[128] = {0}; // , ch[2]

    (void)dwErrors;
    if (hComl < 0 || Command == NULL || answer == NULL || strlen(Command) < 1) return 1;

    clear_serial_port(hComl);

    for (i = 0; i < 125 && Command[i] != '\0' && Command[i] != '\r'; i++)
    {
        l_command[i] = Command[i]; // we copy until CR
    }

    l_command[i++] = '\r';         // we add it
    l_command[i] = '\0';            // we end string

    if (t0)
    {
        *t0 = my_uclock();
    }

    rets = Write_serial_port(hComl, l_command, strlen(l_command));

    if (debug)
    {
        debug_message_log(debug_file, "sending command: \"%s\"", l_command);
    }

    if (rets <= 0)
    {
        if (t0)
        {
            *t0 = my_uclock() - *t0;
        }

    }

    *n_written = 0;
    ret = Read_serial_port(hComl, answer, n_answer, n_written);

    for (; ret == 0;)
        ret = Read_serial_port(hComl, answer, n_answer, n_written);

    if (t0) *t0 = my_uclock() - *t0;

    answer[*n_written] = 0;
    strncpy(last_answer_2, answer, 127);

    if (ret < 0)
    {
        if (debug)
        {
	  debug_message_log(debug_file, "IM %d: unable to get answer from command: \"%s\"",0/*im_ci()*/, l_command);
        }

        return 1;
    }
    else
    {
        if (debug)
        {
	  debug_message_log(debug_file, "Im %d:command: \"%s\", answer: \"%s\"", 0/*im_ci()*/, l_command, answer);
        }
    }

    return 0;
}


int talk_serial_port(com_port_t hCom_port, char *command, int wait_answer, char *answer,
                     unsigned long NumberOfBytesToWrite, unsigned long nNumberOfBytesToRead)
{
    //int NumberOfBytesWrittenOnPort = -1;
    unsigned long NumberOfBytesWritten = -1;


    if (hCom < 0)
    {
        return win_printf_OK("No serial port init");
    }

    if (wait_answer != READ_ONLY)
    {
        //NumberOfBytesWrittenOnPort =
        Write_serial_port(hCom_port, command, NumberOfBytesToWrite);
    }

    if (wait_answer != WRITE_ONLY)
    {
        Read_serial_port(hCom_port, answer, nNumberOfBytesToRead, &NumberOfBytesWritten);

    }

    return 0;
}

/*
   Attemp to read the pico response from serial port
   if a full answer is available then transfer it
   otherwise grab characters and respond that full message is not yet there

*/

char *catch_pending_request_answer(int *size, int im_n, int *error, unsigned long *t0)
{
    static char answer[128], request_answer[128];
    unsigned long dwRead = 0;
    static int i = 0;
    int j, max_size = 128, lf_found = 0;
    char buf[2] = {0};
    
    (void)im_n;
    if (t0 != NULL) *t0 = my_uclock();

    if (hCom <  0)
    {
        *error = -3;

        if (t0 != NULL) *t0 = my_uclock() - *t0;

        return NULL;   // no serial port !
    }

    for (j = 0; j == 0 && i < max_size - 1;)
    {
        // we grab rs232 char already arrived
        dwRead = 0;

        if (ReadData(hCom, buf, 1, &dwRead) == 0)
        {
            // something wrong
            answer[i] = 0;
            dwRead = i;


            if (debug)
            {
                debug_message_log(debug_file, "error reading from serial port, %d bytes red : %s", i, answer);
            }

            *error = -2;

            if (t0 != NULL) *t0 = my_uclock() - *t0;

            return NULL;    // serial port read error !
        }

        if (dwRead == 1)
        {
            answer[i] = buf[0];
            j = lf_found = (buf[0] == '\n') ? 1 : 0;
            i++;
        }
        else if (dwRead == 0) j = 1;
    }

    answer[i] = 0;
    *size = i;

    if (lf_found)
    {
        *error = 0;
        strncpy(request_answer, answer, i);

        if (debug)
        {
            debug_message_log(debug_file, "reading request_answer for pico, %d bytes red : %s", i, answer);
        }

        if (t0 != NULL) *t0 = my_uclock() - *t0;

        i = 0;
        return request_answer;
    }

    *error = 1;

    if (t0 != NULL) *t0 = my_uclock() - *t0;

    return NULL;
}




int n_write_read_serial_port(void)
{
    static char Command[128] = "dac16?";
    static int ntimes = 1, i;
    unsigned long t0;
    double dt, dtm, dtmax;
    char resu[128], *ch;
    int ret = 0;



    unsigned long lpNumberOfBytesWritten = 0;
    unsigned long nNumberOfBytesToRead = 127;

    if (updating_menu_state != 0)    return D_O_K;

    if (hCom < 0) return win_printf_OK("No serial port init");

    for (ch = Command; *ch != 0; ch++)
        if (*ch == '\r') *ch = 0;

    win_scanf("Command to send? %snumber of times %d", &Command, &ntimes);

    strncat(Command, "\r", (sizeof(Command) > strlen(Command)) ? sizeof(Command) - strlen(Command) : 0);

    for (i = 0, dtm = dtmax = 0; i < ntimes; i++)
    {
        t0 = my_uclock();
        Write_serial_port(hCom, Command, strlen(Command));

        ret = Read_serial_port(hCom, resu, nNumberOfBytesToRead, &lpNumberOfBytesWritten);
        t0 = my_uclock() - t0;
        dt = 1000 * (double)(t0) / get_my_uclocks_per_sec();
        resu[lpNumberOfBytesWritten] = 0;
        dtm += dt;

        if (dt > dtmax) dtmax = dt;
    }

    dtm /= ntimes;
    win_printf("command read = %s \n ret %d lpNumberOfBytesWritten = %lu\nin %g mS in avg %g max", resu, ret,
               lpNumberOfBytesWritten, dtm, dtmax);
    return D_O_K;
}

int purge_com(void)
{
    if (hCom < 0) return 1;

    return 0;
    //return PurgeComm(hCom, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);
}
int input_pico_setup(char *error_message, char *port, int port_size, int *debugl, int *sbaud, int *n_bits, int *parity,
                     int *stops, int *hand)
{
    char buf[16000] = {0};
    char port_buf[256] = {0};


    (void)port;
    (void)port_size;
    (void)debugl;
    (void)sbaud;
    (void)n_bits;
    (void)parity;
    (void)stops;
    (void)hand;
    
    snprintf(port_buf, sizeof(port_buf), "%s", port);

    my_strncat(buf, error_message, sizeof(buf));
    strcat(buf, "{\\color{yellow}Port number:} ?%s\n"
           "Dump in and out in a debug file No %R yes %r\n"
           //"{\\color{yellow}Baudrate:} 57.6k %R 115.2k %r 230.4k %r 460.8k %r 921.6k %r (default 57.6k)\n"
           //"{\\color{yellow}Number of bits:} %4d  (default 8)\n"
           //"{\\color{yellow}Parity:} No %R even %r odd %r (default No)\n"
           //"{\\color{yellow}Number of stop bits:} 0->%R 1->%r 2->%r (default 1)\n"
           //"{\\color{yellow}HandShaking:} None %R Cts/Rts %r (default Cts/Rts)\n"
           "{\\color{lightred}If you hit cancel a dummy interface will be used\n"
           "This only allows you to check your camera}\n");
    int win_answer = win_scanf(buf, port_buf, debugl
                               //,sbaud, n_bits, parity, stops, hand
                              );

    if (win_answer == D_O_K)
    {
        snprintf(port, port_size, "%s", port_buf);
    }

    return win_answer;
}

int init_pico_serial(void)
{
    int i, j;
    static int  sbaud = 0, hand = 0, parity = 0, stops = 1, n_bits = 8;
    char command[128] = {0}, answer[128], *gr, *s;
    int  ret, iret;
    unsigned long len = 0;
    static bool first_init = true;
    char port_str[256] = {0};


    if (hCom > 0)
        return 0;

    if (first_init == false)
        return 1;

    first_init = false;
    answer[0] = 0;
    strncpy(port_str, Pico_param.serial_param.serial_port_str, sizeof(port_str));
    parity = Pico_param.serial_param.serial_Parity;
    stops = Pico_param.serial_param.serial_StopBits;
    n_bits = Pico_param.serial_param.serial_ByteSize;
    sbaud = Pico_param.serial_param.serial_BaudRate;

    sbaud /= 57600;
    sbaud--;

    hand = Pico_param.serial_param.serial_fDtrControl;



    if (state_is_pico() == 0)
    {
        i = win_scanf("Your interface device has been switched to a dummy mode\n"
                      "in the previous experiment. This is a debugging mode\n"
                      "which does not allow normal operation. Do you want to\n"
                      "switch back your configuration to the normal state ?");

        if (i == WIN_OK)
            switch_from_pico_serial_to_dummy(1);
    }



    if (Pico_param.serial_param.rs232_started_fine == 0)
    {
        for (; hCom < 0;)
        {
            i = input_pico_setup("{\\color{yellow}Your Picotwist serial interface did not open properly on last trial!} Check:\n"
                             " - that another program is not using the same serial port\n"
                             " - Another instance of Picojai.exe may still be running\n"
                             "    If this is the case open the Windows task manager and kill the process\n"
                             " - Last but not least your configuration may not be correct\n"
                             "If this is the case select a new one.", port_str, sizeof(port_str),  &debug, &sbaud, &n_bits, &parity, &stops, &hand);

            if (i == WIN_CANCEL)
            {
                switch_from_pico_serial_to_dummy(0);
                Pico_param.serial_param.rs232_started_fine = 0;
                write_Pico_config_file();
                return 1;
            }

            hCom = init_serial_port(port_str, (sbaud + 1) * 57600, n_bits, parity , stops, hand);
        }
    }

    if (hCom == -1)
    {
        hCom = init_serial_port(port_str, (sbaud + 1) * 57600, n_bits, parity , stops, hand);
    }

    for (; hCom < 0;)
    {
        i = input_pico_setup("{\\color{yellow}Your Picotwist serial interface is not opening properly!} Check:\n"
                             " - that another program is not using the same serial port\n"
                             " - Another instance of Picojai.exe may still be running\n"
                             "    If this is the case open the Windows task manager and kill the process\n"
                             " - Last but not least your configuration may not be correct\n"
                             "If this is the case select a new one."
                             , port_str, sizeof(port_str), &debug, &sbaud, &n_bits, &parity, &stops, &hand);

        if (i == WIN_CANCEL)
        {
            switch_from_pico_serial_to_dummy(0);
            Pico_param.serial_param.rs232_started_fine = 0;
            write_Pico_config_file();
            return 1;
        }

        hCom = init_serial_port(port_str, (sbaud + 1) * 57600, n_bits, parity , stops, hand);
    }

    if (hCom < 0)
    {
        switch_from_pico_serial_to_dummy(0);
        Pico_param.serial_param.rs232_started_fine = 0;
        write_Pico_config_file();
        return 1;
    }

    sprintf(command, "\r");

    for (i = 3, j = Write_serial_port(hCom, command, strlen(command)); j < 0 && i > 0; i--)
        j = Write_serial_port(hCom, command, strlen(command));

    psleep(50);

    for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
        ret = Read_serial_port(hCom, answer, 127, &len);

    answer[len] = 0;


    sprintf(command, "echo=0\r");

    for (j = Write_serial_port(hCom, command, strlen(command)); j < 0;)
    {
        i = input_pico_setup("{\\color{yellow}Your Picotwist device is not recieving data from your PC properly!}\n"
                             "Check:\n - that you have switched on your device ;-)\n"
                             " - that you have hook up properly your PC to the device.\n"
                             " - that the Picotwist micro-controller is responding: switch the device off\n"
                             "and then on again and restart this program.\n"
                             " - Last but not least your configuration may not be correct\n"
                             "If this is the case select a new one."
                             , port_str, sizeof(port_str), &debug, &sbaud, &n_bits, &parity, &stops, &hand);

        if (i == WIN_CANCEL)
        {
            switch_from_pico_serial_to_dummy(0);
            Pico_param.serial_param.rs232_started_fine = 0;
            write_Pico_config_file();
            return 1;
        }

        CloseSerialPort(hCom);
        hCom = init_serial_port(port_str, (sbaud + 1) * 57600, n_bits, parity , stops, hand);

        if (hCom < 0)
        {
            switch_from_pico_serial_to_dummy(0);
            Pico_param.serial_param.rs232_started_fine = 0;
            write_Pico_config_file();
            return 1;
        }

        j = Write_serial_port(hCom, command, strlen(command));
    }

    psleep(50);

    for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
        ret = Read_serial_port(hCom, answer, 127, &len);

    answer[len] = 0;
    gr = strstr(answer, "Echo off");

    if (gr == NULL)
    {
        sprintf(command, "\r");
        j = Write_serial_port(hCom, command, strlen(command));
        psleep(50);

        for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
            ret = Read_serial_port(hCom, answer, 127, &len);

        answer[len] = 0;

        sprintf(command, "echo=0\r");
        j = Write_serial_port(hCom, command, strlen(command));
        psleep(50);

        for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
            ret = Read_serial_port(hCom, answer, 127, &len);

        answer[len] = 0;
    }

    gr = strstr(answer, "Echo off");

    if (gr == NULL)
    {
        i = input_pico_setup("{\\color{yellow}Your Picotwist device is not responding properly to the initialization!}\n"
                             "Check:\n - that you have switched on your device ;-)\n"
                             " - that you have hook up properly your PC to the device.\n"
                             " - that the Picotwist micro-controller is responding: switch the device off\n"
                             "   and then on again and restart this program.\n"
                             " - Last but not least your configuration may not be correct\n"
                             "If this is the case select a new one."
                             , port_str, sizeof(port_str), &debug, &sbaud, &n_bits, &parity, &stops, &hand);

        if (i == WIN_CANCEL)
        {
            switch_from_pico_serial_to_dummy(0);
            Pico_param.serial_param.rs232_started_fine = 0;
            write_Pico_config_file();
            return 1;
        }

        hCom = init_serial_port(port_str, (sbaud + 1) * 57600, n_bits, parity , stops, hand);

        if (hCom  <  0)
        {
            switch_from_pico_serial_to_dummy(0);
            Pico_param.serial_param.rs232_started_fine = 0;
            write_Pico_config_file();
            return 1;
        }

        j = Write_serial_port(hCom, command, strlen(command));

        if (j < 0)
        {
            switch_from_pico_serial_to_dummy(0);
            Pico_param.serial_param.rs232_started_fine = 0;
            write_Pico_config_file();
            return 1;
        }

        psleep(50);

        for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
            ret = Read_serial_port(hCom, answer, 127, &len);

        answer[len] = 0;
        gr = strstr(answer, "Echo off ver.");
    }

    if (gr != NULL && strlen(gr + 14) > 3)
    {
        s = gr + 14;

        for (j = 0; s[j] != 0; j++)
            s[j] = (s[j] == '\n' || s[j] == '\r') ? 0 : s[j];

        my_set_window_title("picotwist interface started firmware %s", gr + 14);

        if (Pico_param.serial_param.firmware_version)
            free(Pico_param.serial_param.firmware_version);

        Pico_param.serial_param.firmware_version = strdup(gr + 14);
        Pico_param.serial_param.rs232_started_fine = 1;

        if (Pico_param.serial_param.serial_port_str)
            free(Pico_param.serial_param.serial_port_str);

        Pico_param.serial_param.serial_port_str = strndup(port_str, 256);
        Pico_param.serial_param.serial_Parity = parity;
        Pico_param.serial_param.serial_StopBits = stops;
        Pico_param.serial_param.serial_ByteSize = n_bits;
        Pico_param.serial_param.serial_BaudRate = (sbaud + 1) * 57600;
        Pico_param.serial_param.serial_fDtrControl = hand;
        Pico_param.serial_param.serial_fRtsControl = hand;
        write_Pico_config_file();
    }
    else
    {
        Pico_param.serial_param.rs232_started_fine = 0;
        write_Pico_config_file();
    }

    add_plot_treat_menu_item("pico_serial", NULL, pico_serial_plot_menu(), 0, NULL);
    return (gr != NULL) ? 0 : 1;
}

int init_port(void)
{
    if (updating_menu_state != 0)    return D_O_K;

    if (hCom < 0)
    {

        //add_item_to_menu(trackBead_ex_image_menu, "pico_serial", NULL, pico_serial_plot_menu(), 0, NULL);

        add_plot_treat_menu_item("pico_serial", NULL, pico_serial_plot_menu(), 0, NULL);
        add_image_treat_menu_item("pico_serial", NULL, pico_serial_plot_menu(), 0, NULL);
    }

    init_pico_serial();
    return D_O_K;
}



int write_command_on_serial_port(void)
{
    static char Command[128] = "test";
    //char *test="\r";

    if (updating_menu_state != 0)    return D_O_K;

    win_scanf("Command to send? %s", &Command);
    strncat(Command, "\r", (sizeof(Command) > strlen(Command)) ? sizeof(Command) - strlen(Command) : 0);

    //win_printf("deja cela %d\n Command %s",strlen(Command), Command);
    Write_serial_port(hCom, Command, strlen(Command));
    //snprintf(Command,128,"?nm\r");
    //strcat(Command,'\r');
    //win_printf("deja cela %d\n Command %s",strlen(Command), Command);

    //win_printf("%s \n lpNumberOfBytesWritten = %d",Command,lpNumberOfBytesWritten);
    return D_O_K;
}


int read_on_serial_port(void)
{
    char Command[128];
    unsigned long lpNumberOfBytesWritten = 0;
    int ret = 0;
    unsigned long nNumberOfBytesToRead = 127;
    unsigned long t0;

    if (updating_menu_state != 0)    return D_O_K;

    t0 = my_uclock();
    //win_scanf("Command to send? %s",Command);
    ret = Read_serial_port(hCom, Command, nNumberOfBytesToRead, &lpNumberOfBytesWritten);
    t0 = my_uclock() - t0;
    Command[lpNumberOfBytesWritten] = 0;

    win_printf("command read = %s \n ret %d lpNumberOfBytesWritten = %lu\n in %g mS", Command, ret, lpNumberOfBytesWritten,
               1000 * (double)(t0) / get_my_uclocks_per_sec());

    //win_printf("command read = %s \n lpNumberOfBytesWritten = %d",Command,lpNumberOfBytesWritten);
    return D_O_K;
}



int write_read_serial_port(void)
{
    if (updating_menu_state != 0)    return D_O_K;

    write_command_on_serial_port();
    read_on_serial_port();
    return D_O_K;
}




int CloseSerialPort(com_port_t hComl)
{
    close(hComl);
    return D_O_K;
}

int close_serial_port(void)
{
    if (updating_menu_state != 0)    return D_O_K;

    CloseSerialPort(hCom);
    hCom = 0;
    return D_O_K;
}




MENU *pico_serial_plot_menu(void)
{
    static MENU mn[32];

    if (mn[0].text != NULL) return mn;

    add_item_to_menu(mn, "Init serial port", init_port, NULL, 0, NULL);
    add_item_to_menu(mn, "Write serial port", write_command_on_serial_port, NULL, 0, NULL);
    add_item_to_menu(mn, "Read serial port", read_on_serial_port, NULL, 0, NULL);
    add_item_to_menu(mn, "Write read serial port", write_read_serial_port, NULL, 0, NULL);
    add_item_to_menu(mn, "Write read n times", n_write_read_serial_port, NULL, 0, NULL);
    add_item_to_menu(mn, "Close serial port", close_serial_port, NULL, 0, NULL);

    return mn;
}

int pico_serial_main(int argc, char **argv)
{
    (void)argc;
    (void)argv;
    init_port();

    if (hCom >= 0)
        return D_O_K;

    //add_plot_treat_menu_item("pico_serial", NULL, pico_serial_plot_menu(), 0, NULL);
    //add_image_treat_menu_item("pico_serial", NULL, pico_serial_plot_menu(), 0, NULL);
    return D_O_K;
}

int pico_serial_unload(int argc, char **argv)
{
    (void)argc;
    (void)argv;  
    remove_item_to_menu(plot_treat_menu, "pico_serial", NULL, NULL);
    return D_O_K;
}

#endif







