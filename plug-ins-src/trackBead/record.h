/*
 * @addtogroup trackBead
 * @{
 */

#ifndef _RECORD_H_
#define _RECORD_H_

# include "xvin.h"
# include "track_util.h"
# include "../cfg_file/Pico_cfg.h"
# define EVENT_POLY_LINE 1
# define EVENT_DASH_LINE 2

// for xy_tracking_type
# define XY_TRACKING_TYPE_NORMAL         0x00
# define XY_TRACKING_TYPE_DIFFERENTIAL   0x01
# define XY_TRACKING_X_CROSS_USED        0x02
# define XY_BEAD_PROFILE_RECORDED        0x04
# define XY_BEAD_DIFF_PROFILE_RECORDED   0x08
# define XYZ_ERROR_RECORDED              0x10
# define RECORD_BEAD_IMAGE               0x20
# define FIXED_EXTENDED_BEAD_IMAGE       0x40
# define XY_SHEAR_FRINGES_TRACKING       0x80


#define IS_G_RECORD		45378
#define IS_B_RECORD		45379

// plot ispare definition

# define ISPARE_BEAD_NB 0
# define ISPARE_TIME_OUT 1
# define ISPARE_BUF_POSITION 2          // last point displayed
# define ISPARE_POSITION_UPDATE 3
# define ISPARE_STARTING_POSITION 4
# define ISPARE_N_TEST 5                // the actual test displayed
# define ISPARE_FOLLOW_ALL_SCOPE 6
# define ISPARE_BUF_POSITION_SET 7      // last point actually set for displayed
# define ISPARE_STARTING_POSITION_SET 8 // first point actually set for displayed


// bead ispare definition

# define FPARAM_Z_OFFSET   0
# define FPARAM_F_ADJUST   1
# define FPARAM_Z_BASE_CONV   2    // number of basepairs per micron

# define IPARAM_BEAD_QUALITY   0
# define IPARAM_NB_GOOD_CYCLES   1
# define IPARAM_BEAD_FIXED      2

// IPARAM_BEAD_QUALITY   values

# define NOT_DEFINED   0
# define NO_CYCLES   1
# define BAD_HYSTERESIS_CYCLE   2
# define GOOD_QUALITY   3
# define VERY_GOOD_QUALITY   4


// general_track ispare definition

# define I_PC_ULCLOCKS_PER_SEC 63
# define I_EVANESCENT_MODE 62

# define I_SDI_MODE 61
//if g_r->iparam[I_SDI_MODE] then at the following position in iparam and fparam are sdi related
# define I_SDI_BEAD_FUNCTION 60 //encodes the function of the beads

# define F_EVA_DECAY 62
# define F_EVA_OFFSET 61
//specific to sdi float parameters to be saved in fparam of g_r


// action_status

//# define DATA_AVERAGING_START 0x20000000
# define DATA_AVERAGING 0x40000000

// g_r->data_type
# define SCAN_ZMAG  8192
# define HAT_CURVE  16384
# define FORCE_CURVE  65536
# define ZDET_RECORD  32768
// 2011-02-27
# define STATUS_EXTENDE_TO_CMD  0x00010000  // specify that zmag_cmd, rot_cmd, obj_cmd are used to expand status info

# define H_SEGMENT 1024
# define R_SEGMENT 2048
# define Z_SEGMENT 256
# define U_SEGMENT 128
# define N_SEGMENT 512


// scope scope operation
# define DISPLAY_Z 0
# define DISPLAY_X 1
# define DISPLAY_Y 2
# define DISPLAY_XZ 3
# define DISPLAY_YZ 4



/*    Structure used to record tracking data in a streaming mode.
 *    the data are memorized using pages of small size which are allocated quickly
 *    For instance the image number is store in imi[c_page][in_page_index]
 *    one page is typically 4096 records
 */

typedef struct gen_record
{
  // all the following elements are written only in the tracking thread !
  // the other thread can only read these values
  int n_page, m_page, c_page;      // the page idexes
  int in_page_index;               // the position of the last record in the current page
  int page_size;                   // the page size
  int abs_pos;                     // the absolute position of valid data
  int abs_pos0;                    // the absolute position of valid data on loading
  int iparam[64];                  // an array to save int parameters
  float fparam[64];                  // an array to save float parameters
  int n_bead, m_bead, c_bead;        // the bead idexes
  struct gen_tracking *g_t;          // the pointer to the general tracking data
  struct bead_record **b_r;          // the bead pointer
  struct ghost_bead_record **b_rg;   // the ghost bead pointer to keep events of beads not loaded
  int start_bead, in_bead;           // b_r has n_bead elements b_rg has in_bead is elements = the total number of beads in trk
  struct bead_record **b_ref;        // a reference bead pointer, contruct in analysis
  int nb_ref;                        // the number of ref beads
  int starting_fr_from_trk;          // for partial track loading
  int nb_fr_from_trk;                // for partial track loading
  int **imi;                         // the image nb
  int **imit;                        // the image nb by timer
  long long **imt;                   // the image absolute time
  unsigned int **imdt;              // the time spent in the previous function call // was long
  float **zmag;                      // the magnet position
  float **rot_mag;                   // the magnet rotation position
  float **obj_pos;                   // the objective position measured
  int **status_flag;
  float **zmag_cmd;                  // the magnet command asked
  float **rot_mag_cmd;               // the magnet rotation command asked
  float **obj_pos_cmd;               // the objective position command asked
  // all the following elements are written only in the allegro thread (in idle actions) !
  // the other thread can only read these values
  int **action_status;               // contains information like data acquisition type or point nb
  char **message;                    // runing message use to save temperature, buffer changes etc
  int last_starting_pos;              // the first position in general tracking of next recording phase
  int starting_in_page_index;        // the position of the last record in the current page
  int starting_page;                 // the absolute position
  int n_record;                      // the number of recors in this stucture
  int last_saved_pos;                // the first position of last recording phase
  int last_saved_in_page_index;      // the position of the last record in the current page
  int last_saved_page;               // the absolute position
  float ax, dx;                      // the x affine transform to microns
  float ay, dy;                      // the y affine transform to microns
  float z_cor;                       // the immersion correction 0.878
  int (*record_action)(int im, int c_i, int aci, int init_to_zero);      // the routine changing magnets or objectif
  char filename[512];
  char path[512];
  char *fullname;                    // the complete filename
  char name[512];
  int n_rec;
  int data_type;
  unsigned int time;			     // date of creation   // was time_t
  double record_duration;         // duration of data acquisition in micro seconds
  long long record_start;         // start of duration of data
  long long record_end;         // end of duration of data
  int header_state;                //
  int header_size;                // the offset to skip before real data
  int file_error;                  // report if error during saving occured
  int one_im_data_size;            // number of bytes written ay each image
  int real_time_saving;            // flag on when saving is active
  int config_file_position;
  Pico_parameter Pico_param_record;
  long long pc_ulclocks_per_sec;
  int n_events;                   // the number of events, if = n, event n was recorded
  int imi_start;                  // the image nb of the first image in trk
  int timing_mode;                // define the way to display time
  int evanescent_mode;            // are we using evanescent z measure
  float eva_decay;
  float eva_offset;
  int im_data_type;               // the type of video image
  int im_nx;                      // the x size of video image
  int im_ny;                      // the y size of video image
  int SDI_mode;
  int bug_no_zmag_moving_sampled; // some trk files were recorded with a bug;
} g_record;


typedef struct _stiffness_resu
{
  int nxeff;
  float mxm;                        // mean value of x coordinate
  float dmxm;                       // error in mean value of x coordinate
  float sx2;                       // mean variance of x coordinate
  float sx4;                       // mean variance of x coordinate
  float sx2_16;                    // mean variance of x coordinate with average on 16 points
  float sx2_m2;                    // mean variance of x coordinate with bin = 2
  float sx2_m4;                    // mean variance of x coordinate with bin = 4
  float sx2_m8;                    // mean variance of x coordinate with bin = 8
  float fcx;                       // cutoff frequency in x
  float dfcx;                      // error in cutoff frequency in x
  float kx;
  float ax;                        // integral of fluctuation in x
  float dax;                       // error in integral of fluctuation in x
  float chisqx;                    // chi square in x
  float etarx;
  float etar2x;
  float detar2x;
  int x_cardinal_er;
  int nyeff;
  float mym;                        // mean value of y coordinate
  float dmym;                       // error in mean value of y coordinate
  float sy2;                       // mean variance of y coordinate
  float sy4;                       // mean variance of y coordinate
  float sy2_16;                    // mean variance of y coordinate with average on 16 points
  float sy2_m2;                    // mean variance of y coordinate with bin = 2
  float sy2_m4;                    // mean variance of y coordinate with bin = 4
  float sy2_m8;                    // mean variance of y coordinate with bin = 8
  float fcy;                       // cutoff frequency in y
  float dfcy;                      // error in cutoff frequency in y
  float ky;
  float ay;                        // integral of fluctuation in y
  float day;                       // error in integral of fluctuation in y
  float chisqy;                    // chi square in y
  float etary;
  float etar2y;
  float detar2y;
  int y_cardinal_er;
  int nzeff;
  float mzm;                        // mean value of z coordinate
  float dmzm;                       // error in mean value of z coordinate
  float sz2;                       // mean variance of z coordinate
  float sz4;                       // mean variance of z coordinate
  float sz2_16;                    // mean variance of z coordinate with average on 16 points
  float sz2_m2;                    // mean variance of z coordinate with bin = 2
  float sz2_m4;                    // mean variance of z coordinate with bin = 4
  float sz2_m8;                    // mean variance of Z coordinate with bin = 8
  float fcz;                       // cutoff frequency in z
  float dfcz;                      // error in cutoff frequency in z
  float kz;
  float az;                        // integral of fluctuation in z
  float daz;                       // error in integral of fluctuation in z
  float chisqz;                    // chi square in z
  float etarz;
  float etar2z;
  float detar2z;
  int z_cardinal_er;
  float zmag;
  float rot;
  int exp;
  float gain;
  float led;
  float zavg;                      // mean of Z avg disgarding points with bad profiles
  char* error_message;
  int i_zmag;
  int i_exp;
  int n_exp;
  float wong_x2;
  float wong_taux;
  float wong_chi2x;
  float wong_y2;
  float wong_tauy;
  float wong_chi2y;
  float wong_z2;
  float wong_tauz;
  float wong_chi2z;
  int n_lost;
} stiffness_resu;


# if defined(PLAYITSAM) || defined(PIAS)


typedef struct _event_seg
{
  int i_ev;
  int i_seg;
  int n_bead;
  int fixed_bead;
  int seg_start;              // the starting point index of segment
  int seg_end;                // the ending point index of segment
  int type;                   // specify if it is a Noise, Rising, Falling ... segment
  int n_badxy;                // numbers of bad tracking point in xy
  int n_badz;                 // numbers of bad tracking point in z
  int par_chg;                // indicates if zmag or rot or zobj has changed during segment
  int ispare;
  float zstart_vertex;        // Z start value of the segment assuming continuity with the previous segment
  float zend_vertex;          // Z end value of the segment assuming continuity with the next segment
  float xavg;                 // X aveage over the segment
  float xavg_er;              // error in X aveage over the segment
  float yavg;                 // Y aveage over the segment
  float yavg_er;              // error in Y aveage over the segment
  float zavg;                 // Z aveage over the segment
  float zavg_er;              // error in Z aveage over the segment
  float zstart;               // Z start value of the segment obtained by fit
  float zstart_er;            // error in Z start value of the segment
  float zend;                 // Z end value of the segment obtained by fit
  float zend_er;              // error in Z end value of the segment
  float sig_z;                // quadratic error in z on fit
  float z_ref;                // the base signal level
  float sig_z_ref;            // the quadratic error base signal level
  float zmag;
  float rot;
  float force;
  float zobj;
  float fspare;
  float dt;                   // the segment time duration
  float taux;                 // the characteristic time of fluctuation in x
  float tauy;                 // the characteristic time of fluctuation in y
  float tauz;                 // the characteristic time of fluctuation in z
}event_seg;

typedef struct _analyzed_event
{
  int i_ev;
  int ev_start;               // the starting point index of the event
  int ev_end;                 // the ending point index of the event
  int type;
  char stype[64];
  int n_badxy;                // numbers of bad tracking point in xy
  int n_badz;                 // numbers of bad tracking point in z
  float zmag;
  float rot;
  float force;
  float zobj;
  int n_seg;                  // the number of segments
  int i_seg;
  struct _event_seg *e_s;     // the segment array
}analyzed_event;

# endif

# define COMPLETELY_LOST 1
# define XV_FIXED 0x100


typedef struct bead_record
{
  // all the following elements are written only in the tracking thread !
  // the other thread can only read these values
  int n_page, m_page, c_page;      // the page idexes
  int abs_pos;                     // the absolute position
  int in_page_index;               // the position of the last record in the current page
  int iparam[64];                  // an array to save int parameters
  float fparam[64];                  // an array to save float parameters
  int page_size;                   // the page size (typically 4096)
  int profile_radius;              // the profile size (in fact just the radius size)
  float **x, **y, **z;             // bead position organized in pages x[page][im_in_page]
  float **ftmp;                    // extra data for analysis
  int **xstat;                     // extra data status for analysis
  //  float **xt, **yt, **zt;          // bead true position
  char **n_l;                      // not lost idicator
  char **n_l2;                     // not lost post idicator
  int **profile_index;             // indicate the nearest profile in calibration image
  float *rad_prof_ref;             // reference radial profile
  int rad_prof_ref_size;           // reference radial profile size
  float ***rad_prof;               // radial profile  rad_prof[page][im_in_page][radius}
  float ***orthorad_prof;          // orthoradial profile  orthorad_prof[page][im_in_page][angle}
  int ortho_prof_size;             // the number of angular points
  float **theta;                   // bead angular position organized in pages theta[page][im_in_page]
  int kx_angle;                    // if 0 no theta register, != 0 the mode use to find angle
  int ***x_bead_prof;              // bead profile in x  x_bead_prof[page][im_in_page][radius}
                                   // in sdi top fringes profile
  int ***x_bead_prof_diff;         // bead profile in x differential  x_bead_prof_diff[page][im_in_page][radius}
                                   // in sdi bottom fringes profile
  int ***y_bead_prof;              // bead profile in y  y_bead_prof[page][im_in_page][radius}
  int ***y_bead_prof_diff;         // bead profile in y differential  y_bead_prof_diff[page][im_in_page][radius}
  //int ***xt_bead_prof;              // bead top profile in x  xt_bead_prof[page][im_in_page][radius}
  //int ***xb_bead_prof;              // bead bottom profile in x xb_bead_prof[page][im_in_page][radius}
  //int ***yt_bead_prof;              // bead top profile in y  yt_bead_prof[page][im_in_page][radius}
  //int ***yb_bead_prof;              // bead bottom profile in y   yb_bead_prof[page][im_in_page][radius}
  int xy_tracking_type;            // type of tracking differential or normal
  float **x_er, **y_er, **z_er;    // bead position error organized in pages x_er[page][im_in_page] in SDI x_er is the top fringe amp max position z_er the bottom one
  struct bead_tracking *b_t;       // the bead tracking stucture associate
  int cal_im_start;                // the position in byte in the track file of calibration image
  int cal_im_data;                 // the position in byte in the track file of calibration im data
  int cl;                          // the cross size and fringes extend
  int cw;                          // the cross width or fringes width
  int nx_prof;                     // the profile size
  int fringes_dy;                  // for sdi
  int fringes_dy_tolerence;        // for sdi
  int fringes_ny;                  // to track fringes in y
  float fringes_per;
  float t_fringes_phi0;
  float b_fringes_phi0;
  int calib_im_size;               // for rings
  int forget_circle;
  int bp_filter;
  int bp_width;
  int movie_w;                     // width of a movie recorded centered on the bead
  int movie_h;                     // height of a movie recorded centered on the bead
  int movie_track;                 // if 1 indicates that the center of the movie image track the bead position
  int movie_xc;                    // x center of a movie recorded if movie_track == 0
  int movie_yc;                    // y center of a movie recorded if movie_track == 0
  stiffness_resu *s_r;
  int n_s_r, m_s_r, c_s_r;         // the number of result allocated and current
  O_i *calib_im;		   // calibration image (on reading only)
  O_i *calib_im_fil;	           // filtered calibration image
  float minz_bead;                 // the minimum position in the calibration image
  float maxz_bead;                 // the maximum position in the calibration image
  int **event_nb;                  // data analyzing
  int **event_index;               // data analyzing
  int completely_losted;           // if set means that no valid data was recorded
  int xc_1, yc_1;  // Bead position of previous frame
  float **rot_mat;
  float angle_x;
  float angle_y;
# if defined(PLAYITSAM) || defined(PIAS)
  analyzed_event *a_e;
  int na_e, ma_e, ia_e;

# endif
} b_record;

typedef struct ghost_bead_record
{
  int bead_id; // bead number
# if defined(PLAYITSAM) || defined(PIAS)
  analyzed_event *a_e;
  int na_e, ma_e, ia_e;
# endif
} ghostb_record;

typedef struct gbead_record
{
  g_record *g_r;
  int n_br;
} gb_record;



typedef struct _scan_info
{
  int ims, im_n;             // starting image and number of images averaged
  int im_shutter_chg;        // the previous position of shutter chg
  int im_led_chg;            // the previous position of led chg
  int im_gain_chg;           // the previous position of gain chg
  int i_zm;                  // the index of zmag modulation for points
  int is_zm;                 // the index of shutter modulation for points having the same zmag step
  int nis_zm;                // the number of steps of shutter modulation done at the same zmag
  float zmag;                // the zmag value etc
  float rot;
  float zobj;
  float vcap;
  int shutter;
  float led;
  float gain;
  int param_cst;
} scan_info;




typedef struct _oligo_resu
{
  int n_ev;
  int ev_start;
  int ev_size;
  int ph1_s;
  int ph1_n;
  int ph2_s;
  int ph2_n;
  int ph3_s;
  int ph3_n;
  int ph4_s;
  int ph4_n;
  int ph5_s;
  int ph5_n;
  int ph6_s;
  int ph6_n;
  int invalid;
  int *binvalid;
  float *zavg1;
  float *zavg3;
  float *zavg6;
}oligo_resu;


PXV_FUNC(int, do_test_list_zmag_cmd_values_frame_list,(void));

PXV_FUNC(int, add_pages_to_g_record, (g_record *g_r, int n_pages));
PXV_FUNC(int, add_pages_to_bead_record, (b_record *b_r, int n_pages));
PXV_FUNC(b_record*, create_bead_record, (b_track *b_t, int n_pages, int with_radial_profiles,
					 int rad_prof_ref_size, int ortho_prof_size, int save_angle_kx
					 , int xy_tracking_type, int bead_xy_prof_size, float movie_rw,
					 float movie_rh, int movie_track));

PXV_FUNC(g_record*, create_gen_record, (g_track *g_t, int n_pages, int with_radial_profiles,
					int with_orthoradial_profiles, int save_angle_kx, int xy_tracking_type,
					float movie_rw, float movie_rh, int movie_track));

PXV_FUNC(int, free_gen_record, (g_record *g_r));
PXV_FUNC(int, transfer_last_info_from_rolling_buffer_to_record, (g_track *g_t, int current_im, g_record *g_r));
PXV_FUNC(g_record*, find_g_record_in_pltreg, (pltreg *pr));
PXV_FUNC(int, attach_g_record_to_pltreg, (pltreg *pr, g_record  *g_r));
PXV_FUNC(gb_record*, find_gb_record_in_pltreg, (pltreg *pr));
PXV_FUNC(int, attach_gb_record_to_pltreg, (pltreg *pr, g_record  *g_r, int n_br));
PXV_FUNC(g_record*, find_g_record_in_imreg, (imreg *pr));
PXV_FUNC(int, attach_g_record_to_imreg, (imreg *pr, g_record  *g_r));
PXV_FUNC(int, write_record_file_header, (g_record *g_r, O_i *oi));
PXV_FUNC(g_record *, read_record_file_header, (char *fullname, int verbose));
PXV_FUNC(int, read_record_file_data, (g_record *g_r, int start, int n_images));
PXV_FUNC(int, append_record_data, (g_record *g_r));
PXV_FUNC(int, do_save_track,(g_record *g_r));
PXV_FUNC(int, do_load_trk_in_new_project, (void));
PXV_FUNC(int, do_erase_corresponding_points_in_g4_data, (void));


PXV_FUNC(int, reload_trk_in_new_project, (char * fullfile, int verbose));
PXV_FUNC(int, draw_track_XYZ_trajectories, (void));
PXV_FUNC(MENU *, trk_plot_menu, (void));
PXV_FUNC(MENU *, trk_image_menu, (void));
PXV_FUNC(int, duplicate_pico_params, (Pico_parameter *in, Pico_parameter *out));

PXV_FUNC(int, average_bead_z_during_part_trk, (g_record *g_r, int bead_nb, int start_index,
					       int duration, float *xavg, float *yavg, float *zavg,
					       float *zobj, int *profile_invalid));

PXV_FUNC(int, average_bead_z_during_part_trk_tolerant, (g_record *g_r, int bead_nb, int start_index, int duration,
							int tolerant, float *xavg, float *yavg, float *zavg,
							float *zobj, int *profile_invalid));

PXV_FUNC(int, average_bead_z_minus_fixed_during_part_trk, (g_record *g_r, int bead_nb, int fixed_nb, int start_index,
							   int duration, float *xavg, float *yavg,
							   float *zavg, float *zobj, int *profile_invalid));

PXV_FUNC(int, fit_bead_z_to_line_during_part_trk, (g_record *g_r, int bead_nb, int fixed_nb, int start_index,
						   int duration, float *zavg_s, float *zavg_e,
						   int *profile_invalid, float *chi2));


PXV_FUNC(int, fit_bead_z_to_line_during_part_trk_tolerant, (g_record *g_r, int bead_nb, int fixed_nb, int start_index,
						int duration, int tolerant, float *zavg_s, float *zavg_e,
							    int *profile_invalid, float *chi2));



PXV_FUNC(int, fit_bead_z_to_NU_during_part_trk, (g_record *g_r, int bead_nb, int fixed_nb, int start_index,
						 int duration_N, int duration_U, float *zavg_n, float *zavg_e,
						 int *profile_invalid, float *chi2));


PXV_FUNC(int, retrieve_image_index_of_next_acquisition_point, (g_record *g_r, int start_index,
							       int duration, int *ending, float *zmag, float *rot));


PXV_FUNC(int, retrieve_image_index_of_acquisition_point, (g_record *g_r, int point_index, int sarting_im,
					      int *begining, int *avg_start, int *ending, float *zmag, float *rot,
					      float *obj));

PXV_FUNC(int, grab_record_temp, (g_record *g_r, int rank, float *T0,  float *T1,  float *T2));
PXV_FUNC(d_s*, transfer_bead_z_to_ds, (g_record *g_r, int i_bead, int fixed_bd, int start, int size));
#ifdef PIAS
PXV_FUNC(int, update_correction_param_in_events,(g_record *g_r, b_record *br));
PXV_FUNC(int, switch_current_bead_fixed_status, (void));
#endif

/**
 * @brief retrieve the starting and ending index of the phase of one point (aka cycle)
 *
 * @param g_r the track to search in.
 * @param n_point index of the cycle wanted
 * @param n_phase index of the phase wanted
 * @param start_index if not NULL input: index of the search starting point | output: index of the first point of the
 * current zone after the input starting point
 * @param ending if not null, the last point of the zone.
 * @param param_cst FIXME
 *
 * @return an negative number on error or the index of the first point of the zone starting from the input start_index
 */
PXV_FUNC(int, retrieve_image_index_of_next_point_phase, (g_record *g_r,
                                             int n_point, int n_phase,
                                             int *start_index,
                                             int *ending,
                                             int *param_cst));

PXV_FUNC(int, retrieve_image_index_of_next_point, (g_record *g_r,
                                       int n_point,    // specify the point
                                       int *start_index,           // return the start index, you can specify
                                       //a possible starting point from where the
                                       //search will be initiated
                                       int *ending));
PXV_FUNC(int, track_has_averaging_data, (g_record *g_r));
PXV_FUNC(int, track_has_n_test_data, (g_record *g_r));

PXV_FUNC(O_p* ,generate_bead_op_during_part_trk, (g_record *g_r, int bead_nb, int point_nb, int start_index, int duration));
PXV_FUNC(O_p* ,generate_bead_ds_during_part_trk, (O_p *op, g_record *g_r, int bead_nb, int point_nb, int start_index, int duration));

PXV_FUNC(int, retrieve_calibration_image_properties, (g_record *g_r, int bead_nb, int *cl, int *cw, int *nprof));
PXV_FUNC(int, is_bead_profile_good, (g_record *g_r,int profile_index));
PXV_FUNC(int, retrieve_image_index_of_next_averaging_point, (g_record *g_r, int start_index, int *ending, int *param_cst));
PXV_FUNC(int, average_param_during_part_trk, (g_record *g_r, int start_index, int duration,
					      float *zmag, float *rot, float *zobj, int *param_cst));

PXV_FUNC(int, average_Vcap_during_part_trk, (g_record *g_r, int start_index, int duration,
					     float *zmag, float *vcap, int *param_cst));

PXV_FUNC(int, average_bead_z_minus_fixed_during_part_trk_tolerant, (g_record *g_r, int bead_nb, int fixed_nb,
        int start_index, int duration, int tolerant,
        float *xavg, float *yavg, float *zavg,
        float *zobj, int *profile_invalid));

PXV_FUNC(int, average_bead_z_during_part_trk_tolerant, (g_record *g_r, int bead_nb, int start_index, int duration, int tolerant,
        float *xavg, float *yavg, float *zavg, float *zobj, int *profile_invalid));

PXV_FUNC(int, retrieve_index_of_segment_in_event, (g_record *g_r, b_record *b_r, int start_index, int *ending, int *param_cst, int *type));

PXV_FUNC(int, find_shutter_chg_from_partial_trk, (g_record *g_r, int ims, int ime, int *im_shutter, int *im_gain, int *im_led));

PXV_FUNC(int, find_bead_z_max_histo_during_part_trk, (g_record *g_r, int bead_nb, int fixed_bead, int start_index,
						    int duration, float fbin, float *zmax, float *zsig,
						      float *zobj, int *profile_invalid));


PXV_FUNC(int, checking_bead_not_lost_during_part_trk, (g_record *g_r, int bead_nb, int start_index, int duration));

PXV_FUNC(int, find_trk_index_not_used, (char *basename, int starting_inex));

PXV_FUNC(int, load_trk_in_new_project, (char *fullfile, int verbose));
PXV_FUNC(char*, load_trk_config_file, (g_record *g_r));
PXV_FUNC(O_i *, load_calib_im_file_from_record, (g_record *g_r, int im));
PXV_FUNC(float, trk_zmag_to_force,(int nb, float zmag, g_record *g_r));

PXV_FUNC(int, extract_shutter_expo_from_partial_trk, (g_record *g_r, int ims, int ime, int *shutter, float *gain, float *led));

PXV_FUNC(d_s*, jumpfit_vc,(d_s *dsi, d_s *ds, int repeat, int max, int min, int delta, float sigm, float p, double *chi2t));

# if defined(PLAYITSAM) || defined(PIAS)
PXV_FUNC(int, readjust_z_of_segments_in_event, (analyzed_event *a_e, g_record *g_r, int fixed_bead_nb));
# endif

PXV_FUNC(int, scope_z_fil, (pltreg *pr, O_p *op, g_record *g_r, int bead_nb));


PXV_FUNC(scan_info *, retrieve_scan_info,(g_record *g_r, int *nstep, int verbose));
PXV_FUNC(int, retrieve_min_max_event_and_phases, (g_record *g_r, int *min, int *max, int *ph_max));
PXV_FUNC(int, find_bead_z_by_max_gaussian_histo_during_part_trk_tolerant,
                (g_record *g_r, int bead_nb, int start_index,
                 int duration, int tolerant,
                 float biny, float *xavg, float *yavg, float *zavg, float *zobj,
                 int *profile_invalid));
PXV_FUNC(float, zmag_valid,(g_record *g_r, int index));
PXV_FUNC(int,  is_bead_profile_in_im_range_good,(g_record *g_r, int bead_nb, int ims, int range));
PXV_FUNC(int,  find_max_of_distribution_by_histo,(d_s *dst, O_p *ops, float biny, float *Max));
PXV_FUNC(O_p, *x_openning_extend_of_pts_crossing_y_all_ds_same_op,(pltreg  *pr, O_p  *op, float theshold));
PXV_FUNC(int, mean_y2_on_array, (float *yd, int nx, int win_flag, float *meany, float *my2, float *my4));


PXV_FUNC(int, camera_known_shutter_range,(char *model, float freq, int *shut_min, int *shut_max));
PXV_FUNC(int, camera_known_gain_range,(char *model, float freq, float *gain_min, float *gain_max));
PXV_FUNC(int, find_analyzed_event_for_slipage, (void));
PXV_FUNC(int, find_analyzed_event_for_translocation, (void));
PXV_FUNC(int, find_analyzed_event_single_Z_or_U, (void));
PXV_FUNC(int, find_analyzed_event_ZN_or_UN, (void));
PXV_FUNC(int, find_analyzed_event_for_NRN_or_NHN,(void));
PXV_FUNC(int, average_between_closest_HR_event,(d_s *dszv,d_s *dszf));
PXV_FUNC(int,FIR_between_closest_HR_event,(d_s *dszv, d_s *dszf, int rec_fir));




PXV_FUNC(int, histo_analyzed_hybrid_events, (void));
PXV_FUNC(int, histo_analyzed_event_single_segment, (void));
PXV_FUNC(int, histo_analyzed_event_URNHU, (void));
PXV_FUNC(int, histo_analyzed_event_NNN, (void));
PXV_FUNC(int, histo_analyzed_event_NRNHN_two_levels, (void));
PXV_FUNC(int, histo_analyzed_event_NRNHN_discrete_levels, (void));


PXV_FUNC(int, sort_events_by_starting_time,(void));

PXV_FUNC(int, do_write_analyzed_events, (void));
PXV_FUNC(int, do_read_analyzed_events, (void));
PXV_FUNC(int, erase_bead_recorded_events, (void));
PXV_FUNC(int, erase_bead_visible_events,(void));

PXV_FUNC(int, display_analyzed_event, (void));
PXV_FUNC(int, set_g_record_starting_time, (g_record  *g_r));
PXV_FUNC(int, set_g_record_ending_time,   (g_record  *g_r));
PXV_FUNC(int, draw_track_event_phase_average, (void));
PXV_FUNC(int, find_analyzed_event_single_H_or_R, (void));


PXV_FUNC(int, free_bead_record, (b_record *b_r));
PXV_FUNC(int, eval_record_data_size, (g_record *g_r));
PXV_FUNC(int, what_data_is_in_im_record, (void));
PXV_FUNC(int, write_analyzed_events, (g_record *g_r));
PXV_FUNC(int, read_analyzed_events, (g_record *g_r));
PXV_FUNC(O_i *, read_image_from_record_file_header, (g_record *g_r));
PXV_FUNC(float, camera_known_freq, (char *model, int sizex, int sizey));
PXV_FUNC(float, camera_known_pixelw, (char *model));
PXV_FUNC(float, camera_known_pixelh, (char *model));
PXV_FUNC(int, remove_trk_NAN_data, (g_record *g_r));
PXV_FUNC(imreg*, find_bead_image_in_record_file_data, (void));
PXV_FUNC(int, bead_phase_profile_quality, (int profile_index));
PXV_FUNC(int, bead_xi2_profile_quality, (int profile_index));
PXV_FUNC(int, bead_profile_index, (int profile_index));
PXV_FUNC(int, is_bead_profile_good_2, (g_record *g_r,int profile_index));
PXV_FUNC(int, retrieve_image_page_and_index_of_image_nb,
              (g_record *g_r, int im_nb, int *page, int *index, float *zmag,
			   float *rot, float *obj));
PXV_FUNC(int, do_retrieve_point, (void));
PXV_FUNC(int, do_retrieve_point_no_avg, (void));
PXV_FUNC(int, retrieve_min_max_event, (g_record *g_r, int *min, int *max));
PXV_FUNC(int, find_bead_XYZ_max_by_gaussian_histo_during_part_trk_tolerant,
              (g_record *g_r, int bead_nb, int start_index,
               int duration, int tolerant,
               float binxy, float binz, float *xavg, float *yavg, float *zavg, float *zobj,
               int *profile_invalid));
PXV_FUNC(int, grab_all_bead_at_index, (g_record *g_r,           // the record
			   int index,
			   int tolerant,
			   float *zobj,
			   float *zavg,
			   int *profiles_invalid));
PXV_FUNC(O_p *, grab_Zmag_vs_Vacp, (g_record *g_r, int start_index, int duration));
PXV_FUNC(float, get_sigma_of_derivative, (d_s *dsi, float uperlimit));
PXV_FUNC(d_s*,  jumpfit_vc_2, (d_s *dsi, d_s *ds, int repeat, int min,int max, float sigm, float p, double *chi2t));
PXV_FUNC(d_s*, jumpfit_height,(d_s *dsi, d_s *ds, d_s *level_left,d_s *maw_weights,int repeat, int min, int max, float sigm, float p, double *chi2t));
PXV_FUNC(int, draw_track_event_phase, (void));
PXV_FUNC(int, draw_track_event_phase_colapse, (void));
PXV_FUNC(int, draw_oligo_event_phase_diff, (void));
PXV_FUNC(int, draw_oligo_event_significant_hybridization, (void));
PXV_FUNC(int, draw_oligo_average_significant_event, (void));
PXV_FUNC(int, draw_oligo_average_significant_event_block2, (void));
PXV_FUNC(int, compute_chi2_of_oligo_phase,(oligo_resu *o_e,
                                int i_ev,
                                int ph,
                                g_record *g_r,
                                int i_bead,
                                d_s *ds,
                                float sig,
                                float *chi2));
PXV_FUNC(int, copy_and_correct_event_of_oligo, (oligo_resu *o_e,
                                    int i_ev,
                                    g_record *g_r,
                                    int i_bead,
                                    d_s *dscp,
                                    d_s *dsavg,
                                    float sig5,
                                    int phi5_avg));
PXV_FUNC(int, find_histo_max_and_sigma_of_oligo_val, (oligo_resu *o_e,
        int n_ev,
        int pos,
        int ph6, g_record *g_r,
        int i_bead,
        float *mean, float *sig,
        int *use_pre_defined));
PXV_FUNC(int, find_histo_max_and_sigma_of_oligo_extension,(oligo_resu *o_e, int n_ev, int i_bead, int mode, float *mean, float *sig));
PXV_FUNC(int, draw_oligo_average_significant_event_2, (void));
PXV_FUNC(int, draw_oligo_average_significant_event_image, (void));
PXV_FUNC(int, draw_track_Z_drift, (void));
PXV_FUNC(int, draw_track_Z_drift_2, (void));
PXV_FUNC(int, do_unwind_circle, (void));
PXV_FUNC(int, draw_track_XY_rotation, (void));
PXV_FUNC(int, get_all_bead_z_at_cycle_and_phase, (void));
PXV_FUNC(int, get_all_bead_z_at_cycle_and_phase_track_drift, (void));
PXV_FUNC(int, convert_ds_pauses_in_mean_z_and_duration, (void));
PXV_FUNC(int, compute_blocking_position_distance_from_peaks, (void));
PXV_FUNC(int, mod_bead_Z_off, (void));
PXV_FUNC(int, push_ds_as_bead_Z, (void));
PXV_FUNC(int, draw_track_XYZ_avg_trajectories, (void));
PXV_FUNC(int, draw_track_Zmag_avg_vs_Vcap_zmag, (void));
PXV_FUNC(int, draw_track_Zmag_avg_vs_Vcap_zmag_2, (void));
PXV_FUNC(char*, do_select_saving_file_csv,(g_record *g_r, char *filename));
PXV_FUNC(int, dump_track_trajectories_to_csv, (void));
PXV_FUNC(int, draw_track_angle_rot_trajectories, (void));
PXV_FUNC(int, draw_track_profiles, (void));
PXV_FUNC(int, extract_diff_profiles_from_movie, (void));
PXV_FUNC(int, track_bead_from_movie, (void));
PXV_FUNC(int, translate_periodic_ds, (d_s *ds, float dx, int verbose));
PXV_FUNC(int, simul_x_tracking, (void));
PXV_FUNC(int, draw_tracking_xy_profiles, (void));
PXV_FUNC(int, draw_track_orthoradial_profiles, (void));
PXV_FUNC(int, draw_track_profiles_phase_diff, (void));
PXV_FUNC(int, recompute_Z_from_profiles_and_calib_im, (void));
PXV_FUNC(int, recompute_Z_from_oi_profiles_and_calib_im, (void));
PXV_FUNC(int, draw_track_orthoradial_profiles_phase, (void));
PXV_FUNC(int, draw_track_params, (void));
PXV_FUNC(int, draw_track_timing, (void));
PXV_FUNC(int, draw_track_peltier_current, (void));
PXV_FUNC(int, draw_track_temperatures, (void));
PXV_FUNC(int, draw_track_shutter, (void));
PXV_FUNC(int, draw_track_flows, (void));
PXV_FUNC(int, draw_track_visible_Z_vs_rot, (void));
PXV_FUNC(int, draw_track_average_Z_vs_rot, (void));
PXV_FUNC(int, scope_horz_marker_plt_action, (pltreg *pr, O_p *op, float x, DIALOG *d));
PXV_FUNC(int, scope_adjust_select, (void));
PXV_FUNC(int, scope_param_select, (void));
PXV_FUNC(int, scope_bead_to_substract_select, (void));
PXV_FUNC(int, scope_op_idle_action, (O_p *op, DIALOG *d));
PXV_FUNC(int, trk_scope_zize, (void));
PXV_FUNC(int, trk_lost_display_off, (void));
PXV_FUNC(int, trk_lost_display_on, (void));
PXV_FUNC(int, do_move_scope_in_x_right, (void));
PXV_FUNC(int, do_move_scope_in_x_left, (void));
PXV_FUNC(int, do_increment_scope_size, (void));
PXV_FUNC(int, do_decrement_scope_size, (void));
PXV_FUNC(int, disp_trk_apropos, (void));
PXV_FUNC(int, toggle_bead_completely_lost, (void));
PXV_FUNC(int, set_bead_as_fix, (void));
PXV_FUNC(int, inc_bead, (void));
PXV_FUNC(int, dec_bead, (void));
PXV_FUNC(int, extract_config_from_trk, (void));
PXV_FUNC(int, write_Obj_param_from_trk, (g_record *g_r, FILE *fp));
PXV_FUNC(int, diplay_trk_molecule_cfg, (void));
PXV_FUNC(int, diplay_trk_obj_cfg, (void));
PXV_FUNC(int, diplay_trk_magnets_cfg, (void));
PXV_FUNC(int, diplay_trk_microscope_cfg, (void));
PXV_FUNC(int, diplay_trk_bead_cfg, (void));
PXV_FUNC(int,fill_phase_k_position_array,(g_record *g_r,int ***im_array_k_ph,int min,int max,int ph_max));

PXV_FUNC(int, diplay_trk_camera_cfg, (void));
PXV_FUNC(int, attach_filtered_calibration, (void));
PXV_FUNC(int, load_correction_from_file, (void));
PXV_FUNC(d_s*, nl_fit_step_and_slope_vc_2_r, (d_s *dsi,   // data set of data
				     d_s *ds, // data set to save filtrerd data compatible with dsi
				     d_s *dsdz,  // pointer to derivative data produced
				     int max, int min,  // size to perform averaging
				     int size_to_evaluate_noise,  // extend of data on which you will evaluate noise
				     float p,     // weigthing power
				     double *chi2t,  // chi2 returned by the function
				     float mul, //Multiplicative factor favoring long average (1.2 typically)
				     float noise_mul));


# ifdef _RECORD_C_
g_record *working_gen_record = NULL;

int zmag_f_or_rot = 0;
int fixed_bead_nb = 0;
int do_substract_fixed_bead = 0;
int do_substract_a_set_of_bead = 0;
int set_of_fixed_beads[1024] = {0};
int rec_fir = 16;
int scope_buf_size = 512;
int scope_ds0_vis = 1;
int scope_ds1_vis = 1;
int scope_ds2_vis = 1;
int scope_ds3_vis = 1;
int scope_ds4_vis = 1;
int scope_ds5_vis = 1;
int scope_ds6_vis = 1;
int display_event_label = 1;
bool check_z_average=0; //for Martin's plugin - to avoid checking of z position of events

float display_event_label_min = -2000;
float display_event_label_max = 2000;

# else
PXV_VAR(g_record*, working_gen_record);

PXV_VAR(int, zmag_f_or_rot);
PXV_VAR(int, fixed_bead_nb);
PXV_VAR(int, do_substract_fixed_bead);
PXV_VAR(int, rec_fir);



PXV_VAR(int, scope_buf_size);
# endif

# endif

/* @} */
