#ifndef _GAME_SOURCE_H_
#define _GAME_SOURCE_H_


# define OVER_SAMPLING 512


# define NO_LED_MOD  0
# define PULSE_LED_MOD  1
# define SINE_LED_MOD  2
# define POISSON_LED_MOD  3


# ifdef _GAME_SOURCE_C_

int LED_mod = 0; // in percent
int LED_mod_type = 0; // in percent
float  LED_mod_freq = 2.0;
int LED_pulse_w = 0; // pluse width modulation

float camera_gain = 1;
# endif

# ifndef _GAME_SOURCE_C_

PXV_VAR(int, LED_mod);
PXV_VAR(int, LED_mod_type);
PXV_VAR(int, LED_pulse_w);
PXV_VAR(float, LED_mod_freq);

PXV_VAR(float, camera_gain);

# endif

void generate_simulation_image(O_i *oi);

# endif
