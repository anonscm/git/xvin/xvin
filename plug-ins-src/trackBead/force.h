
#ifndef _FORCE_H_
#define _FORCE_H_



typedef struct _force_param
{
  int nf;                  // the total number of frames requires to perform calibration
  int nstep;               // the number of force acquisition points
  int istep;               // the present acquisition point
  int setling;             // define the time skipped after rotation to allow bead to settle
  int dead;                // the number of frames diregard just after focus has changed
  int nper;                // the number of frames between two focus changes
  int maxnper;             // the maximum number of frames between two focus changes
  int c_step;
  int *im;                 // the array of images number 
  int *grim;               // the general record image index
  int *rec_n;              // the general record image index
  int one_way;             // is the scan one way or back and forh
  int adj_focus;           // 0 -> do not change focus, > 0 adjust focus to this profile
  float zmag_start;        // the starting zmag position
  float zmag_step;         // the step size of zmag step
  float zlow;
  float rot_start;         // the starting zmag position
  float rot_step;          // the step size of zmag step
  float *zm;               // the array of zmag position
  int *i_mod_shut;         // the index of point with shutter modulation
  int *i_zm;               // the index of zmag
  int *is_zm;              // the shutter index at zmag
  int *nis_zm;             // the total nb of shutter index at zmag
  float *ro;               // the array of rotation position
  float *fm;               // the array of estimated forces
  float *zo;               // the array of zobj
  float fbr;               // the low frequency mode suppression 
  float fdr;               // the high frequency mode suppression 
  float accuracy;          // accuracy for the force measurements 
  float lambdazmag;
  float zmag0;
  float rotation;
  int fir;                 // the nb of points for averaging
  int *exp;                // the array of camera exposition
  float *gain;             // the array of camera gain
  float *led;              // the array of led level
  int exp_0;               // the full exposure for camera shutter
  int exp_min;             // the min exposure for camera shutter
  float gain_0;            // the initial camera gain
  float gain_max;          // the maximum camera gain
  float led_0;             // the initial led current
  float led_max;           // the maximum led current
  float zmag_hi_fc;        // zmag value below which scan with short exposure is done 
  int nb_acq_pt;           // actual number of acquisition point
  int n_expo_div;          // number of acquisition varying shutter at one zmag
  int mod_expo_enable;     // a general flag telling if exposure modulation is enable
} force_param;

# define MIN_DX2 1e-5            
# define ISPARE_N_RECOD_TREATED 15            // the actual recording in treatment

PXV_FUNC(int, record_auto_force_curve, (void));

# ifndef _FORCE_C_

//PXV_VAR(char, sample[]);
PXV_VAR(int, n_force);
PXV_FUNC(MENU *, force_menu,(void));
PXV_FUNC(MENU *, force_plot_menu,(void));

# else

//char sample[] = "test"; // _PXV_DLL
int n_force = 0;// _PXV_DLL

# endif

# endif
