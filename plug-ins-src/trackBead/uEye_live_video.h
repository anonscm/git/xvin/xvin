#ifndef _UEYE_LIVE_SOURCE_H_
#define _UEYE_LIVE_SOURCE_H_


#define BIT_PER_PIX_8  0x01080001
#define BIT_PER_PIX_10  0x01100003
#define ACQUIRE_INFINITY 0xFFFFFFFFFFFFFFFF

#define D_BAD 1

typedef struct Treatement_struct
{
  void *pInfoBase;
  //int k;
  uint64_t lTimeStamp;
  //uint64_t lNumberImageDelivered;
} TreatementStruct;


# ifndef _UEYE_LIVE_VIDEO_C_


PXV_VAR(int, nAviID);
PXV_VAR(int, rec_AVI);

PXV_VAR(int, iImages_in_UEYE_Buffer);
PXV_VAR(imreg  *,imr_UEYE);
PXV_VAR(pltreg  *,pr_UEYE);
PXV_VAR(O_i  *, oi_UEYE);
PXV_VAR(DIALOG *, d_UEYE);
PXV_VAR(BOOL , allegro_display_on);
PXV_VAR(BOOL, overlay);
PXV_VAR(int, do_refresh_overlay);
PXV_VAR(VIEW_HANDLE, hView);
PXV_VAR(HIDS, m_hCam);			// handle to camera
PXV_VAR(SENSORINFO, sInfo);

PXV_VAR(void ** , UEYE_Im_buffer_ID);
PXV_VAR(STREAM_HANDLE, hDS);
PXV_VAR(BOOL , bEnableThread);
PXV_VAR(HANDLE , hReadyEvent);
PXV_VAR(uint64_t, lNumberFramesDelivered);
PXV_VAR(uint64_t, lNumberFramestreated);

# else 

int nAviID = -1;
int rec_AVI = 0;


HIDS	m_hCam;			// handle to camera
SENSORINFO sInfo;


uint64_t lNumberFramesDelivered = 0;   // written only by camera thread
uint64_t lNumberFramestreated = 0;     // written only by treatment thread

HANDLE hReadyEvent = NULL;
HANDLE hFinishEvent = NULL;





imreg   	*imr_UEYE = NULL;
pltreg  	*pr_UEYE = NULL;
O_i     	*oi_UEYE = NULL;
DIALOG  	*d_UEYE = NULL;
int iImages_in_UEYE_Buffer = 128;
int 		num_image_UEYE = -1;
BOOL 		allegro_display_on = TRUE;
BOOL 		overlay = TRUE;
//int 		do_refresh_overlay = 1;
int absolute_image_number = 0;
int lNumBuff;
void **UEYE_Im_buffer_ID = NULL;
BOOL bEnableThread;
char win_title[512];

HIDS	m_hCam;			// handle to camera
SENSORINFO sInfo;





// Initialize all handles
//HANDLE hStreamEvent = NULL;
HANDLE hEventFreeze = NULL;
HANDLE hEventNewImage = NULL;
HANDLE hEventRemoved = NULL;
HANDLE hEventReplug = NULL;
HANDLE EventList[4];
HANDLE hEventKill = NULL;

// Initialize all handles

int iStreamChannel = 0;


void StreamProcess(void);
void Terminate_UEYE_Thread(void);
void WaitForThreadToTerminate(void);
void CloseThreadHandle(void);
int add_oi_UEYE_2_imr (imreg *imr,O_i* oi);



////////////////








#endif
#endif
