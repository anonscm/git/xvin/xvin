# ifndef _APT_TRANSLATION_UNIX_C_
# define _APT_TRANSLATION_UNIX_C_

# include "allegro.h"

# include "xvin.h"


/* If you include other plug-ins header do it here*/
# include "../magnetscontrol.h"
# include "../action.h"
# include "../../cfg_file/Pico_cfg.h"

//# include "../fftbtl32n.h"
//# include "../fillibbt.h"
# include "../trackBead.h"
# include "../track_util.h"



# include "apt_translation_unix.h"
# include "APTAPI.h"



static long  plNumUnits = -1;
static float pfPosition = 0;
static float pfZeroOffset = 1;
static char szModel[256], szSWVer[256], szHWNotes[256];



int do_thorlabs_APT_set_MOT_pos_t(void);
int do_thorlabs_APT_get_MOT_AxisInfo_t(void);
int do_thorlabs_APT_stop_t(void);
int do_thorlabs_APT_get_MOT_BlashDistance_t(void);
int do_thorlabs_APT_get_MOT_home_t(void);
int grab_plot_from_serial_port_t(void);
int do_thorlabs_APT_chg_MOT_PID_t(void);
int do_thorlabs_APT_set_MOT_VelParams_t(void);




char	*describe_apt_motors_device_apt(void)
{
  return "The translation apt_motor uses the Thorlab DC motor and rotation of the magnets\n"
    "is faked by apt functions just for test...\n";
}

void stop_apt_mot(tl_apt_context *mot_context)
{
    if ((mot_context) != NULL) tl_apt_cleanup(mot_context);
}



int _init_apt_motors_OK(char *serial, tl_apt_context **mot_context )
{
  int i, j,  er;
  static MENU ref[32] = {0};
  static int init = 0;

  *mot_context = tl_apt_init_ws(0x0403, 0xfaf0, NULL,NULL, &er, serial);
  if (*mot_context == NULL)
  {
    dump_to_error_file_with_date_and_time("APT initialisation failed: File %s line %ul"
					  ,__FILE__, __LINE__);

      win_printf("APTinit failed !");
      return 0;
  }
  else     dump_to_error_file_with_date_and_time("APT initialisation Success: File %s line %ul"
					  ,__FILE__, __LINE__);



  tl_apt_cleanup(*mot_context);

  j = tl_apt_mot_set_backlash_distance(*mot_context, 0);
  //if (j) return win_printf_OK("Error setting blash distance\non Cube with serial # %s\n" ,motSerialNum);
  //win_printf("backlashset");
  j = t1_apt_mot_get_pos(*mot_context, &pfPosition);
  if (j) return win_printf_OK("Error reading position from Cube with serial # %s\n" ,serial);
  printf("%f\n",pfPosition);

  // if (init==0)
  // {
  //   add_item_to_menu(ref,"Clean UP APT", do_thorlabs_APT_stop,NULL,0,NULL);
  //   add_item_to_menu(ref,"APT backslash", do_thorlabs_APT_get_MOT_BlashDistance,NULL,0,NULL);
  //   add_item_to_menu(ref,"Motor PID params",do_thorlabs_APT_chg_MOT_PID,NULL,0,NULL);
  //   add_item_to_menu(ref,"Motor Velocity params",do_thorlabs_APT_set_MOT_VelParams,NULL,0,NULL);
  //   init=1;
  // }
  // add_image_treat_menu_item("Magnets APT", NULL, thorlabs_APT_plot_menu(), 0, NULL);
  return 0;
}



int   _set_apt_motor_value(tl_apt_context *mot_context,float  pos)
{
  int i;
  float zcmd = 0;
  if (mot_context == NULL)return 1;

  i = tl_apt_mot_set_move_abspos(mot_context, (int32_t) (POS_ENC_CONV_FACTOR*pos));
  i = tl_apt_mot_move_absolute(mot_context);

  if (i) return 1;
  return 0;
}

int   _set_apt_motor_rel_value(tl_apt_context *mot_context,float  pos)
{
  int i;

  if (mot_context == NULL)return 1;

  i = tl_apt_mot_set_mov_rel(mot_context, (int32_t) (POS_ENC_CONV_FACTOR*pos));
  i = tl_apt_mot_move_rel(mot_context);

  if (i) return 1;
  return 0;
}

float _read_apt_motor_value(tl_apt_context *mot_context)
{
  int i = 0;
  float pos = 0;
  if (mot_context == NULL)return __FLT_MAX__;

  if (i) return __FLT_MAX__;
  t1_apt_mot_get_pos(mot_context,&pfPosition);



	pos = pfPosition;

  return pos;
}

int _read_apt_motor_value_and_status(tl_apt_context *mot_context,float  *z, float *vcap, int *limit, int *status)
{
  int i, k;
  float zout;
  uint32_t plStatusBits;

  if (mot_context == NULL)return 1;
  i=t1_apt_mot_get_pos(mot_context, &pfPosition);

  if (i) return 2;
  pfPosition = zout;

  if (z != NULL) *z = pfPosition;

  i = t1_apt_mot_get_statusbits(mot_context,&plStatusBits);
  if (i) return 3;
  if (status) *status = 0;
  if (vcap) *vcap = 0;
  k = (int)plStatusBits&0x07;
  if (limit)
    {
      if (k == 1) *limit = -1;
      else if (k == 2) *limit = 1;
      else if (k == 3) *limit = -1;
      else if (k == 4) *limit = 1;
      else *limit = 0;
    }
  return 0;
}



int _set_apt_motors_speed(tl_apt_context *mot_context,float  v)
{
  int i;
  int ipfMinVel, ipfAccn, ipfMaxVel;
  if (mot_context == NULL)return 1;
  i = tl_apt_mot_get_velocity(mot_context, &ipfMinVel, &ipfAccn, &ipfMaxVel);

  if (i) return 2;

  i = tl_apt_mot_set_velocity(mot_context, ipfMinVel, ipfAccn, ipfMaxVel);
  if (i) return 3;
  return 0;
}
float  _get_apt_motors_speed(tl_apt_context *mot_context)
{
  int i;
  int ipfMinVel,ipfAccn,ipfMaxVel;
  if (mot_context == NULL)return -1;
  i = tl_apt_mot_get_velocity(mot_context, &ipfMinVel, &ipfAccn, &ipfMaxVel);
  if (i) return -2;
  return ((float)ipfMaxVel)/SPEED_ENC_CONV_FACTOR;
}

int _set_apt_motor_speed(tl_apt_context *mot_context,float  v)
{
  int i;
  int ipfMinVel, ipfAccn, ipfMaxVel;
  if (mot_context == NULL)return 1;
  i = tl_apt_mot_get_velocity(mot_context, &ipfMinVel, &ipfAccn, &ipfMaxVel);
  if (i) return 2;
  ipfMaxVel = ( v < 3.0) ? v *SPEED_ENC_CONV_FACTOR : 2.5*SPEED_ENC_CONV_FACTOR;
  ipfMaxVel = ( v > 0) ? v *SPEED_ENC_CONV_FACTOR: 2.5*SPEED_ENC_CONV_FACTOR;
  i = tl_apt_mot_set_velocity(mot_context, ipfMinVel, ipfAccn, ipfMaxVel);
  if (i) return 3;
  return 0;
}
float  _get_apt_motor_speed(tl_apt_context *mot_context)
{
  int i;
  int ipfMinVel, ipfAccn, ipfMaxVel;

  if (mot_context == NULL)return -1;
  i =tl_apt_mot_get_velocity(mot_context, &ipfMinVel, &ipfAccn, &ipfMaxVel);
  if (i) return -2;
  return ((float)ipfMaxVel)/SPEED_ENC_CONV_FACTOR;
}






int _set_apt_motor_pid(tl_apt_context *mot_context,int num, int val)
{
  int ilProp, ilInt, ilDeriv, ilIntLimit;
  uint16_t ifctrl;

  if (num < 0 || num > 4) return 1;
  if (num == 3) return 1;
  if (val > 32768) val = 32768;
  tl_apt_mot_get_PID(mot_context, &ilProp, &ilInt, &ilDeriv, &ilIntLimit,&ifctrl);
  if (num == 0)       ilProp = val;
  else if (num == 1)  ilInt = val;
  else if (num == 2)  ilDeriv = val;
  //else if (num == 3)  iret = sscanf(answer,"zmax PWM %d",&set);
  else if (num == 4)  ilIntLimit = val;
  tl_apt_mot_set_PID(mot_context, (int32_t)ilProp, (int32_t)ilInt, (int32_t)ilDeriv, (int32_t)ilIntLimit,(uint16_t) 0x0F00);
  return 0;
}
int  _get_apt_motor_pid(tl_apt_context *mot_context,int num)
{
  int set = -1;
  int lProp, lInt, lDeriv, lIntLimit;
  unsigned short int fctrl;


  if (num < 0 || num > 4) return -1;
  if (num == 3) return -1;
  if (tl_apt_mot_get_PID(mot_context, &lProp, &lInt, &lDeriv, &lIntLimit,&fctrl)) return -1;
  if (num == 0)       set = lProp;
  else if (num == 1)  set = lInt;
  else if (num == 2)  set = lDeriv;
  else if (num == 4)  set = lIntLimit;
  return set;
}

/*    Set new motor reference by changing the offset
 *

/*    Set new motor Origin, Dir = 0 => top, Dir = 1 => bottom
 *    Thorlabs motors will go to limit switch ans set thorlabs position to zero
 *    The ofsset parameter will be set to the pos parameter
 */

int   _set_apt_motors_origin(tl_apt_context *mot_context,float  pos, int dir)
{
  uint16_t plDirection, plLimSwitch;


//  tl_apt_mot_pmdstageaxis_t *pmdparam;

  if (mot_context == NULL)return 1;

  //if(tl_apt_mot_get_pmdstageaxis_l(mot_context,pmdparam)) return 2;
  if (dir == 0)
    {
      //Pico_param.translation_motor_param.ref_limit_switch = 1;
      //pmdparam->min_pos = -Pico_param.translation_motor_param.motor_range;
      //pmdparam->min_pos -= 2*pfZeroOffset;
      //pmdparam->max_pos = 2*pfZeroOffset;
      plDirection = HOME_FWD;
      plLimSwitch = HOMELIMSW_FWD;
    }
  else
    {

      plDirection = HOME_REV;
      plLimSwitch = HOMELIMSW_REV;
    }
  if (tl_apt_mot_set_homeparam(mot_context, plDirection, plLimSwitch, (int32_t)(1.0*SPEED_ENC_CONV_FACTOR), (int32_t)(1.0*POS_ENC_CONV_FACTOR))) return 4;
  if (tl_apt_mot_home(mot_context)) return 5;

  return 0;
}


int   _set_Vcap_min_t(float volts)
{
  (void)volts;
  return 0;
}

float  _get_Vcap_min_t(void)
{
  return 0;
}

int    _set_apt_motor_value_and_wait(tl_apt_context *mot_context,float pos)
{
  int i;

  if (mot_context == NULL)return 1;
  i = tl_apt_mot_set_move_abspos(mot_context,(int32_t)(POS_ENC_CONV_FACTOR*(pos)));
  i = tl_apt_mot_move_absolute(mot_context);
  if (i) return 1;
  return 0;
}



int do_thorlabs_APT_get_MOT_BlashDistance_t(void)
{

  if(updating_menu_state != 0)	return D_O_K;

  register int i, j;
  char message[2048];
  float pfBLashDist;
  extern tl_apt_context **mot_contexts_translation; extern char **motSerials_translation;
  tl_apt_context *mot_context=NULL;
  static int channel=0;
  i = win_scanf("Choose the channel \n %R x \n %r y \n",channel);
  if (i== WIN_CANCEL) return 0;
  mot_context=mot_contexts_translation[channel];
  if (mot_context == NULL)win_printf("APT not initiated");
  message[0] = 0;
  j = t1_apt_mot_get_bldist_l(mot_context, &pfBLashDist);
  if (j) return win_printf_OK("Error reading blash distance\non Cube with serial # %s\n"
			      ,motSerials_translation[channel]);
  snprintf(message,sizeof(message),"Device with serial # %s\n"
	     "Blash distance (%g), %%12f \n"
	   ,motSerials_translation[channel],pfBLashDist);

  i = win_scanf(message,&pfBLashDist);
  if (i == WIN_CANCEL) return D_O_K;
  j =tl_apt_mot_set_backlash_distance_l(mot_context, pfBLashDist);
  if (j) return win_printf_OK("Error setting blash distance\non Cube with serial # %s\n"
			      ,motSerials_translation[channel]);
  return 0;
}


int do_thorlabs_APT_chg_MOT_PID_t(void)
{
  register int i, j;
  char message[2048];
  int32_t ilProp, ilInt, ilDeriv, ilIntLimit;
  ushort fctrl;


  if(updating_menu_state != 0)	return D_O_K;
  extern tl_apt_context **mot_contexts_translation; extern char **motSerials_translation;
  tl_apt_context *mot_context=NULL;
  static int channel=0;
  i = win_scanf("Choose the channel \n %R x \n %r y \n",channel);
  if (i== WIN_CANCEL) return 0;
  mot_context=mot_contexts_translation[channel];


  if (mot_context == NULL)return win_printf_OK("APT DC motor %s not found",motSerials_translation[channel]);
  message[0] = 0;
  j = tl_apt_mot_get_PID(mot_context, &ilProp, &ilInt, &ilDeriv, &ilIntLimit,&fctrl);
  if (j) return win_printf_OK("Error reading PID params\non Cube with serial # %s\n"
			      ,motSerials_translation[channel]);
  snprintf(message,sizeof(message),"Device with serial # %s\n"
	     "Proportionnal (%d),  \n"
	     "Integral (%d),  \n"
	     "Derivative (%d),  \n"
	     "Limit (%d),  \n"
	   ,motSerials_translation[channel],ilProp, ilInt, ilDeriv, ilIntLimit);

  i = win_scanf(message,&ilProp, &ilInt, &ilDeriv, &ilIntLimit);
  if (i == WIN_CANCEL) return D_O_K;
  j = tl_apt_mot_set_PID(mot_context, ilProp, ilInt, ilDeriv, ilIntLimit,(uint16_t)0x0F00);
  if (j) return win_printf_OK("Error reading PID params\non Cube with serial # %s\n"
			      ,motSerials_translation[channel]);

  return 0;
}



int do_thorlabs_APT_set_MOT_pos_t(void)
{
  register int i, j, k;
  char message[2048], status[64];
  float Position;
  uint32_t StatusBits;
  float z = -1, vcap = -1;
  int limit = 0, statu = 0;

  if(updating_menu_state != 0)	return D_O_K;
  extern tl_apt_context **mot_contexts_translation; extern char **motSerials_translation;
  tl_apt_context *mot_context=NULL;
  static int channel=0;
  i = win_scanf("Choose the channel \n %R x \n %r y \n",channel);
  if (i== WIN_CANCEL) return 0;
  mot_context=mot_contexts_translation[channel];


  i = _read_apt_motor_value_and_status(mot_context,&z, &vcap, &limit, &statu);
  if (i) win_printf_OK("Error reading position \non Cube with serial # %s\n"
		       "z = %g vcap = %g\nlimit = %d status = %d\n"
		       ,motSerials_translation[channel],z, vcap, limit, statu);
  else win_printf_OK("Reading position \non Cube with serial # %s\n"
		       "z = %g vcap = %g\nlimit = %d status = %d\n"
		       ,motSerials_translation[channel],z, vcap, limit, statu);

 if (mot_context == NULL)return win_printf_OK("APT DC motor %s not found",motSerials_translation[channel]);
  status[0] = message[0] = 0;
  j = t1_apt_mot_get_pos(mot_context,&Position);;
  t1_apt_mot_get_statusbits(mot_context, &StatusBits);
  if (j == 0)
    {
      k = (int)StatusBits&0x07;
      if (k == 1) sprintf(status,"Rev. limit On!");
      else if (k == 2) sprintf(status,"Fwd. limit On!");
      else if (k == 3) sprintf(status," Rev. soft limit On!");
      else if (k == 4) sprintf(status," Fwd. soft limit On!");
      else if (k == 5) sprintf(status," Mot. moving Rev.!");
      else if (k == 6) sprintf(status," Mot. moving Fwd.!");
      else if (k == 5) sprintf(status," Shaft jogging Rev.!");
      else if (k == 6) sprintf(status," Shaft jogging Fwd.!");
      snprintf(message+strlen(message),sizeof(message)-strlen(message),
	       "Cube with serial # %s move to %%12f %s\n"
	       ,motSerials_translation[channel],status);
    }
  else
    {
      k = (int)StatusBits&0x07;
      if (k == 1) sprintf(status,"Rev. limit On!");
      else if (k == 2) sprintf(status,"Fwd. limit On!");
      else if (k == 3) sprintf(status," Rev. soft limit On!");
      else if (k == 4) sprintf(status," Fwd. soft limit On!");
      else if (k == 5) sprintf(status," Mot. moving Rev.!");
      else if (k == 6) sprintf(status," Mot. moving Fwd.!");
      else if (k == 5) sprintf(status," Shaft jogging Rev.!");
      else if (k == 6) sprintf(status," Shaft jogging Fwd.!");
      snprintf(message+strlen(message),sizeof(message)-strlen(message),
	       "Error on Cube with serial # %s scanf move to %%12f %s\n"
	       ,motSerials_translation[channel],status);
    }

  i = win_scanf(message, &Position);
  if (i == WIN_CANCEL) return D_O_K;
  j =  tl_apt_mot_set_move_abspos(mot_context, (int32_t) ( Position * POS_ENC_CONV_FACTOR));
  if (j) win_printf_OK("Error setting position \non Cube with serial # %s\n",motSerials_translation[channel]);
  return 0;
}

int do_thorlabs_APT_stop_t(void)
{
  if(updating_menu_state != 0)	return D_O_K;
  extern tl_apt_context **mot_contexts_translation; extern char **motSerials_translation;
  tl_apt_context *mot_context=NULL;
  static int channel=0;
  register int i;
  i = win_scanf("Choose the channel \n %R x \n %r y \n",channel);
  if (i== WIN_CANCEL) return 0;
  mot_context=mot_contexts_translation[channel];
    if ((mot_context) != NULL) tl_apt_cleanup(mot_context);
  return 0;
}


int do_thorlabs_APT_set_MOT_VelParams_t(void)
{
  register int i, j;
  char message[2048];
  float pfMinVel, pfAccn, pfMaxVel;
  int ipfMinVel, ipfAccn, ipfMaxVel;

  if(updating_menu_state != 0)	return D_O_K;
  extern tl_apt_context **mot_contexts_translation; extern char **motSerials_translation;
  tl_apt_context *mot_context=NULL;
  static int channel=0;
  i = win_scanf("Choose the channel \n %R x \n %r y \n",channel);
  if (i== WIN_CANCEL) return 0;
  mot_context=mot_contexts_translation[channel];
  if (mot_context == NULL)return win_printf_OK("APT DC motor %s not found",motSerials_translation[channel]);
  message[0] = 0;
  j = tl_apt_mot_get_velocity(mot_context, &ipfMinVel, &ipfAccn, &ipfMaxVel);
  pfMinVel=(float)ipfMinVel/SPEED_ENC_CONV_FACTOR;
  pfAccn=(float)ipfAccn/ACC_ENS_CONV_FACTOR;
  pfMaxVel=(float)ipfMaxVel/SPEED_ENC_CONV_FACTOR;
  if (j) return win_printf_OK("Error reading velocity params\non Cube with serial # %s\n"
			      ,motSerials_translation[channel]);
  snprintf(message,sizeof(message),"Device with serial # %s\n"
	     "Min velocity (%g), %%12f \n"
	     "Max velocity (%g), %%12f \n"
	     "Acceleration (%g), %%12f \n"
	   ,motSerials_translation[channel],pfMinVel, pfMaxVel, pfAccn);

  i = win_scanf(message,&pfMinVel, &pfMaxVel, &pfAccn);
  if (i == WIN_CANCEL) return D_O_K;
  j = tl_apt_mot_set_velocity(mot_context, (int32_t)(pfMinVel*SPEED_ENC_CONV_FACTOR),(int32_t)( pfAccn*ACC_ENS_CONV_FACTOR),(int32_t)( pfMaxVel*SPEED_ENC_CONV_FACTOR));
  if (j) return win_printf_OK("Error reading velocity params\non Cube with serial # %s\n"
			      ,motSerials_translation[channel]);
  return 0;
}



#endif
