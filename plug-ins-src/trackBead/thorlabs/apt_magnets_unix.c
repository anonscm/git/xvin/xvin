
# ifndef _APT_MAGNETS_UNIX_C_
# define _APT_MAGNETS_UNIX_C_

# include "allegro.h"

# include "xvin.h"


/* If you include other plug-ins header do it here*/
# include "../magnetscontrol.h"
# include "../action.h"
# include "../../cfg_file/Pico_cfg.h"

//# include "../fftbtl32n.h"
//# include "../fillibbt.h"
# include "../trackBead.h"
# include "../track_util.h"



# include "apt_magnets_unix.h"
# include "APTAPI.h"


#ifdef ZMAG_FACTOR  //tv : scale factor between acuator and magnet movement
int zmag_scale_on = 1;
#endif

static tl_apt_context *mot_context=NULL;


long  plNumUnits = -1;
char zmagSerialNum[25]="-1";
long dummy_zmagSerialNum = -1; //to avoid changing it
float pfPosition = 0;
float pfHomeVel = 1, pfZeroOffset = 1;
char szModel[256], szSWVer[256], szHWNotes[256];


unsigned long last_rot_set_l, rot_moving_time_l;


int do_thorlabs_APT_set_MOT_pos(void);
int do_thorlabs_APT_get_MOT_AxisInfo(void);
int do_thorlabs_APT_stop(void);
int do_thorlabs_APT_get_MOT_BlashDistance(void);
int do_thorlabs_APT_get_MOT_home(void);
int grab_plot_from_serial_port(void);
int do_thorlabs_APT_chg_MOT_PID(void);
int do_thorlabs_APT_set_MOT_VelParams(void);




char	*describe_magnets_device_apt(void)
{
  return "The translation motor uses the Thorlab DC motor and rotation of the magnets\n"
    "is faked by apt functions just for test...\n";
}

void stop_apt(void)
{
    if ((mot_context) != NULL) tl_apt_cleanup(mot_context);
}

int do_active_zmag_scale_factor(void)
{
	if (updating_menu_state!=0) return D_O_K;

	if (win_scanf("switch on/off zmag scale to %d",&zmag_scale_on)==WIN_CANCEL) return D_O_K;

	return 0;
}

MENU *thorlabs_APT_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"Set APT pos",
			 do_thorlabs_APT_set_MOT_pos,NULL,0,NULL);
	add_item_to_menu(mn,"Clean UP APT",
			 do_thorlabs_APT_stop,NULL,0,NULL);
	add_item_to_menu(mn,"APT backslash",
			 do_thorlabs_APT_get_MOT_BlashDistance,NULL,0,NULL);
	add_item_to_menu(mn,"Set Home",
			 do_thorlabs_APT_get_MOT_home,NULL,0,NULL);
	add_item_to_menu(mn,"Motor PID params",
			 do_thorlabs_APT_chg_MOT_PID,NULL,0,NULL);
	add_item_to_menu(mn,"Motor Velocity params",
			 do_thorlabs_APT_set_MOT_VelParams,NULL,0,NULL);

	//add_item_to_menu(mn,"plot rescale in Y", do_thorlabs_APT_rescale_plot,NULL,0,NULL);
	return mn;
}



int _init_magnets_OK(void)
{
  int i, j,  er;
  static MENU ref[32] = {0};
  static int init = 0;

  strcpy(zmagSerialNum,get_config_string("SDI","magnets_motor_serial","1"));
  mot_context = tl_apt_init_ws(0x0403, 0xfaf0, NULL,NULL, &er, zmagSerialNum);
  tl_apt_cleanup(mot_context);
  if (mot_context == NULL)
  {
    dump_to_error_file_with_date_and_time("APT initialisation failed: File %s line %ul"
					  ,__FILE__, __LINE__);

      win_printf("APTinit failed !");
      return 1;
  }
  else     dump_to_error_file_with_date_and_time("APT initialisation Success: File %s line %ul"
					  ,__FILE__, __LINE__);




  j = tl_apt_mot_set_backlash_distance(mot_context, 0);
  //if (j) return win_printf_OK("Error setting blash distance\non Cube with serial # %s\n" ,zmagSerialNum);
  //win_printf("backlashset");
  j = t1_apt_mot_get_pos(mot_context, &pfPosition);
  if (j) return win_printf_OK("Error reading position from Cube with serial # %s\n" ,zmagSerialNum);
  i=win_printf("If the software has been rebooted but the APT magnet not, please enter OK\n"
               "If the magnets have been reset, please press CANCEL\n");
  if (i==WIN_CANCEL)
  {
  #ifdef ZMAG_FACTOR
  Pico_param.translation_motor_param.zmag_offset=pfPosition + Pico_param.translation_motor_param.last_saved_z_position/Pico_param.translation_motor_param.zmag_factor;
  #else
  Pico_param.translation_motor_param.zmag_offset=pfPosition + Pico_param.translation_motor_param.last_saved_z_position;
  #endif
}
  n_magnet_z = Pico_param.translation_motor_param.zmag_offset - pfPosition;
  if (init==0)
  {
    add_item_to_menu(ref,"Clean UP APT", do_thorlabs_APT_stop,NULL,0,NULL);
    add_item_to_menu(ref,"APT backslash", do_thorlabs_APT_get_MOT_BlashDistance,NULL,0,NULL);
    add_item_to_menu(ref,"Motor PID params",do_thorlabs_APT_chg_MOT_PID,NULL,0,NULL);
    add_item_to_menu(ref,"Motor Velocity params",do_thorlabs_APT_set_MOT_VelParams,NULL,0,NULL);
    init=1;
  }
  add_image_treat_menu_item("Magnets APT", NULL, thorlabs_APT_plot_menu(), 0, NULL);
  return 0;
}

int _has_magnets_memory(void)
{
 return 1;
}



int   _set_magnet_z_value(float pos)
{
  int i;
  float zcmd = 0;
  pos = (pos > Pico_param.translation_motor_param.translation_max_pos) ? Pico_param.translation_motor_param.translation_max_pos : pos;
  if (mot_context == NULL)return 1;
#ifdef ZMAG_FACTOR
	if (zmag_scale_on==1)
	{  //scale the distance between magents and surface
    printf("rcmd=%f\n",zcmd);
		zcmd = Pico_param.translation_motor_param.zmag_offset- pos/ Pico_param.translation_motor_param.zmag_factor;
	}
	else zcmd = Pico_param.translation_motor_param.zmag_offset - pos;
	i =tl_apt_mot_set_move_abspos(mot_context,(int32_t) (zcmd*POS_ENC_CONV_FACTOR));
  i = tl_apt_mot_move_absolute(mot_context);
#else
  i = tl_apt_mot_set_move_abspos(mot_context, (int32_t) (POS_ENC_CONV_FACTOR*(Pico_param.translation_motor_param.zmag_offset - pos, 0)));
  i = tl_apt_mot_move_absolute(mot_context);
#endif
  if (i) return 1;
  return 0;
}

float _read_magnet_z_value(void)
{
  int i = 0;
  float pos = 0;
  if (mot_context == NULL)return __FLT_MAX__;

  if (i) return __FLT_MAX__;
  t1_apt_mot_get_pos(mot_context,&pfPosition);

#ifdef ZMAG_FACTOR
	if (zmag_scale_on==1)
	{
		pos = (Pico_param.translation_motor_param.zmag_offset- pfPosition) * Pico_param.translation_motor_param.zmag_factor;
	}
	else pos = Pico_param.translation_motor_param.zmag_offset - pfPosition;
#else
	pos = Pico_param.translation_motor_param.zmag_offset - pfPosition;
#endif
  Pico_param.translation_motor_param.last_saved_z_position = pos;
  return pos;
}

int _read_magnet_z_value_and_status(float *z, float *vcap, int *limit, int *status)
{
  int i, k;
  float zout;
  uint32_t plStatusBits;

  if (mot_context == NULL)return 1;
  i=t1_apt_mot_get_pos(mot_context, &pfPosition);

  if (i) return 2;
  pfPosition = zout;
#ifdef ZMAG_FACTOR
	if (zmag_scale_on==1)
	{
		if(z!=NULL) *z  =  (Pico_param.translation_motor_param.zmag_offset - pfPosition) * Pico_param.translation_motor_param.zmag_factor;
	}
	else if (z!=NULL) *z = Pico_param.translation_motor_param.zmag_offset - pfPosition;
#else
  if (z != NULL) *z = Pico_param.translation_motor_param.zmag_offset - pfPosition;
#endif
  i = t1_apt_mot_get_statusbits(mot_context,&plStatusBits);
  if (i) return 3;
  if (status) *status = 0;
  if (vcap) *vcap = 0;
  k = (int)plStatusBits&0x07;
  if (limit)
    {
      if (k == 1) *limit = -1;
      else if (k == 2) *limit = 1;
      else if (k == 3) *limit = -1;
      else if (k == 4) *limit = 1;
      else *limit = 0;
    }
    Pico_param.translation_motor_param.last_saved_z_position = *z;
  return 0;
}



int _set_motors_speed(float v)
{
  int i;
  int ipfMinVel, ipfAccn, ipfMaxVel;
  if (mot_context == NULL)return 1;
  i = tl_apt_mot_get_velocity(mot_context, &ipfMinVel, &ipfAccn, &ipfMaxVel);

  if (i) return 2;

  ipfMaxVel = ( v > 0) ?  (int) (v * SPEED_ENC_CONV_FACTOR): (int) (2.5*SPEED_ENC_CONV_FACTOR);

  i = tl_apt_mot_set_velocity(mot_context, ipfMaxVel, 100*ACC_ENS_CONV_FACTOR,ipfMaxVel);
  if (i) return 3;
  return 0;
}
float  _get_motors_speed(void)
{
  int i;
  int ipfMinVel,ipfAccn,ipfMaxVel;
  if (mot_context == NULL)return -1;
  i = tl_apt_mot_get_velocity(mot_context, &ipfMinVel, &ipfAccn, &ipfMaxVel);
  if (i) return -2;

  return ((float)ipfMaxVel)/SPEED_ENC_CONV_FACTOR;
}

int _set_zmag_motor_speed(float v)
{
  int i;
  int ipfMinVel, ipfAccn, ipfMaxVel;
  if (mot_context == NULL)return 1;
  i = tl_apt_mot_get_velocity(mot_context, &ipfMinVel, &ipfAccn, &ipfMaxVel);
  if (i) return 2;

  ipfMaxVel = ( v > 0) ?  (int) (v * SPEED_ENC_CONV_FACTOR): (int) (2.5*SPEED_ENC_CONV_FACTOR);
  i = tl_apt_mot_set_velocity(mot_context, ipfMaxVel, 100*ACC_ENS_CONV_FACTOR, ipfMaxVel);
  if (i) return 3;
  return 0;
}
float  _get_zmag_motor_speed(void)
{
  int i;
  int ipfMinVel, ipfAccn, ipfMaxVel;

  if (mot_context == NULL)return -1;

  i =tl_apt_mot_get_velocity(mot_context, &ipfMinVel, &ipfAccn, &ipfMaxVel);

  if (i) return -2;
  return ((float)ipfMaxVel)/SPEED_ENC_CONV_FACTOR;
}






int _set_zmag_motor_pid(int num, int val)
{
  int ilProp, ilInt, ilDeriv, ilIntLimit;
  uint16_t ifctrl;

  if (num < 0 || num > 4) return 1;
  if (num == 3) return 1;
  if (val > 32768) val = 32768;
  tl_apt_mot_get_PID(mot_context, &ilProp, &ilInt, &ilDeriv, &ilIntLimit,&ifctrl);
  if (num == 0)       ilProp = val;
  else if (num == 1)  ilInt = val;
  else if (num == 2)  ilDeriv = val;
  //else if (num == 3)  iret = sscanf(answer,"zmax PWM %d",&set);
  else if (num == 4)  ilIntLimit = val;
  tl_apt_mot_set_PID(mot_context, (int32_t)ilProp, (int32_t)ilInt, (int32_t)ilDeriv, (int32_t)ilIntLimit,(uint16_t) 0x0F00);
  return 0;
}
int  _get_zmag_motor_pid(int num)
{
  int set = -1;
  int lProp, lInt, lDeriv, lIntLimit;
  unsigned short int fctrl;


  if (num < 0 || num > 4) return -1;
  if (num == 3) return -1;
  if (tl_apt_mot_get_PID(mot_context, &lProp, &lInt, &lDeriv, &lIntLimit,&fctrl)) return -1;
  if (num == 0)       set = lProp;
  else if (num == 1)  set = lInt;
  else if (num == 2)  set = lDeriv;
  else if (num == 4)  set = lIntLimit;
  return set;
}

/*    Set new motor reference by changing the offset
 *
 */

int _set_magnet_z_ref_value(float pos)  /* in mm */
{
  int i;

  pos = (pos > Pico_param.translation_motor_param.translation_max_pos) ? Pico_param.translation_motor_param.translation_max_pos : pos;
  if (mot_context == NULL)return 1;
  i = t1_apt_mot_get_pos(mot_context,&pfPosition);
  if (i) return 2;
  #ifdef ZMAG_FACTOR
  Pico_param.translation_motor_param.zmag_offset = pfPosition + pos/Pico_param.translation_motor_param.zmag_factor;
  #else
	Pico_param.translation_motor_param.zmag_offset = pfPosition + pos;
  #endif
  n_magnet_z = pos;
  return 0;
}

/*    Set new motor Origin, Dir = 0 => top, Dir = 1 => bottom
 *    Thorlabs motors will go to limit switch ans set thorlabs position to zero
 *    The ofsset parameter will be set to the pos parameter
 */

int   _set_z_origin(float pos, int dir)
{
  uint16_t plDirection, plLimSwitch;


//  tl_apt_mot_pmdstageaxis_t *pmdparam;

  if (mot_context == NULL)return 1;

  //if(tl_apt_mot_get_pmdstageaxis_l(mot_context,pmdparam)) return 2;
  if (dir == 0)
    {
      //Pico_param.translation_motor_param.ref_limit_switch = 1;
      //pmdparam->min_pos = -Pico_param.translation_motor_param.motor_range;
      //pmdparam->min_pos -= 2*pfZeroOffset;
      //pmdparam->max_pos = 2*pfZeroOffset;
      plDirection = HOME_FWD;
      plLimSwitch = HOMELIMSW_FWD;
    }
  else
    {
      //Pico_param.translation_motor_param.ref_limit_switch = -1;
      //pmdparam->max_pos = Pico_param.translation_motor_param.motor_range;
      //pmdparam->max_pos = 2*pfZeroOffset;
      //pmdparam->min_pos -= 2*pfZeroOffset;
      plDirection = HOME_REV;
      plLimSwitch = HOMELIMSW_REV;
    }
  //if (tl_apt_mot_set_pmdstageaxis_l(mot_context,pmdparam)) return 3;
  if (tl_apt_mot_set_homeparam(mot_context, plDirection, plLimSwitch, (int32_t)(1.0*SPEED_ENC_CONV_FACTOR), (int32_t)(1.0*POS_ENC_CONV_FACTOR))) return 4;
  if (tl_apt_mot_home(mot_context)) return 5;
//#ifdef ZMAG_FACTOR
//#else
  Pico_param.translation_motor_param.zmag_offset = pos;
//#endif
  return 0;
}


int   _set_Vcap_min(float volts)
{
  (void)volts;
  return 0;
}

float  _get_Vcap_min(void)
{
  return 0;
}

int    _set_magnet_z_value_and_wait(float pos)
{
  int i;

  pos = (pos > Pico_param.translation_motor_param.translation_max_pos) ? Pico_param.translation_motor_param.translation_max_pos : pos;
  if (mot_context == NULL)return 1;
  #ifdef ZMAG_FACTOR
  i = tl_apt_mot_set_move_abspos(mot_context,(int32_t)(POS_ENC_CONV_FACTOR*(Pico_param.translation_motor_param.zmag_offset - pos/Pico_param.translation_motor_param.zmag_factor)));
  #else
  i = tl_apt_mot_set_move_abspos(mot_context,(int32_t)(POS_ENC_CONV_FACTOR*(Pico_param.translation_motor_param.zmag_offset - pos)));
  #endif
  i = tl_apt_mot_move_absolute(mot_context);
  if (i) return 1;
  return 0;
}


int	_set_rot_value(float rot)
{
  //rot -= n_rot_offset;
  unsigned long dt;
  float tmp;

  if (rot_moving_time_l == 0)   // rotation has stopped
    {
      rot_moving_time_l = get_my_uclocks_per_sec()*fabs((rot + n_rot_offset - prev_mag_rot)/v_rota);
      last_rot_set_l = my_uclock();
      last_n_rota = prev_mag_rot;
      n_rota_inst = prev_mag_rot;
      n_rota = rot + n_rot_offset;
    }
  else
    {
      dt = my_uclock();
      if (last_rot_set_l + rot_moving_time_l < dt) // in fact rotating is finished
	{
	  rot_moving_time_l = get_my_uclocks_per_sec()*fabs((rot + n_rot_offset - prev_mag_rot)/v_rota);
	  last_rot_set_l = my_uclock();
	  n_rota_inst = last_n_rota = prev_mag_rot;
	  n_rota = rot + n_rot_offset;
	}
      else  // we are still rotating !
	{
	  tmp = (n_rota - last_n_rota) * ((float)(dt - last_rot_set_l))/rot_moving_time_l;
	  tmp += last_n_rota; // this is where we are
	  rot_moving_time_l = get_my_uclocks_per_sec()*fabs((rot + n_rot_offset - tmp)/v_rota);
	  last_rot_set_l = my_uclock();
	  n_rota_inst = last_n_rota = tmp;
	  n_rota = rot + n_rot_offset;
	}
    }
# ifdef RT_MV_DEBUG
  RT_debug("roti=%d&%d\n",(int)(0.5 + 8000*rot),(int)(0.5 + 8000*rot));
#endif
  return 0;
}

int _set_rot_ref_value(float rot)  /* in tour */
{
  n_rot_offset = rot;
  //  n_rota = rot;
# ifdef RT_MV_DEBUG
  RT_debug("rhome=%f\r",rot);
#endif
  return 0;
}

float _read_rot_value(void)
{
  unsigned long dt;
  float tmp;
  //char buf[512];

  if (rot_moving_time_l == 0)   return n_rota_inst;	// magnets have stopped
  dt = my_uclock();
  if (last_rot_set_l + rot_moving_time_l < dt) // in fact translating is finished
    {
      rot_moving_time_l = 0;
      tmp = n_rota;	// magnets have stopped
      n_rota_inst = n_rota;
    }
  else
    {
      tmp = (n_rota - last_n_rota) * ((float)(dt - last_rot_set_l))/rot_moving_time_l;
      tmp += last_n_rota; // this is where we are
      n_rota_inst = tmp;
    }
  //snprintf(buf,512,"rot  read %g ",(tmp - n_rot_offset));
  //my_set_window_title(buf);
# ifdef RT_MV_DEBUG
  RT_debug("roti?\n\t < roti%d\n",(int)(n_rota_inst*8000));
#endif
  return tmp - n_rot_offset;
}



int    _set_rot_value_and_wait(float rot)
{
  //rot -= n_rot_offset;
  n_rota = rot;// + n_rot_offset;
  return 0;
}

int	_go_and_dump_z_magnet(float z)
{
  n_magnet_z = z;
  _set_magnet_z_value(n_magnet_z);
  return 0;
  //return dump_to_specific_log_file_with_time(CURRENT_LOG_FILE,"Magnet Z changed auto to %g\n",n_magnet_z);
}
int    _go_wait_and_dump_z_magnet(float zmag)
{
  if (n_magnet_z == zmag)		return 0;
  _set_magnet_z_value_and_wait(zmag);
  return 0;
  //return dump_to_specific_log_file_with_time(CURRENT_LOG_FILE,"Magnet Z changed auto to %g\n",n_magnet_z);
}

int    _go_and_dump_rot(float r)
{
  //n_rota = r;
  _set_rot_value(r);
  return 0;
  //return dump_to_specific_log_file_with_time(CURRENT_LOG_FILE,"Rotation number auto changed %g\n",n_rota);
}
int	_go_wait_and_dump_rot(float r)
{
  if ((n_rota) == r)		return 0;
  _set_rot_value_and_wait(r);
  return 0;
  //return dump_to_specific_log_file_with_time(CURRENT_LOG_FILE,"Rotation number auto changed %g\n",n_rota);
}


int	_go_wait_and_dump_log_specific_rot(float r, char *log)
{
  (void)log;
  if ((n_rota) == r)		return 0;
  _set_rot_value_and_wait(r);
  //dump_to_specific_log_file_only(log,"Rotation number auto changed %g\n",n_rota);
  return 0;
}

int	_go_wait_and_dump_log_specific_z_magnet(float zmag, char *log)
{
  (void)log;
  if (n_magnet_z == zmag)		return 0;
  _set_magnet_z_value_and_wait(zmag);
  //dump_to_specific_log_file_only(log,"Magnet Z changed auto to %g\n",n_magnet_z);
  return 0;
}


int do_thorlabs_APT_get_MOT_home(void)
{
  register int i;
  char message[2048];
  uint16_t plDirection, plLimSwitch;
  int32_t homevel,offset;
  float lpfHomeVel, lpfZeroOffset;
  int dir = 0, do_home = 1;

  if(updating_menu_state != 0)	return D_O_K;
  if (mot_context == NULL)win_printf("APT not initiated");
  message[0] = 0;
  i = t1_apt_mot_get_homeparam(mot_context, &plDirection, &plLimSwitch, &homevel, &offset);
  lpfHomeVel = (float)(homevel / SPEED_ENC_CONV_FACTOR);
  lpfZeroOffset = (float)( offset / POS_ENC_CONV_FACTOR);

  if (i) return win_printf_OK("Error reading home params\non Cube with serial # %s\n"
			      ,zmagSerialNum);
  if (plDirection == HOME_REV) dir = 1;
  snprintf(message,sizeof(message),"Device with serial # %s\n"
	   "Homing in %%R forward or %%r backward direction\n"
	   "pfHomeVel (%g) %%12f \npfZeroOffset (%g) %%12f\nDo home setting %%b\n"
	   ,zmagSerialNum, lpfHomeVel, lpfZeroOffset);


  i = win_scanf(message, &dir, &lpfHomeVel, &lpfZeroOffset, &do_home);
  if (i == WIN_CANCEL) return D_O_K;
  if (dir == 0)
    {
      plDirection = HOME_FWD;
      plLimSwitch = HOMELIMSW_FWD;
    }
  else
    {
      plDirection = HOME_REV;
      plLimSwitch = HOMELIMSW_REV;
    }
  i = tl_apt_mot_set_homeparam(mot_context, plDirection, plLimSwitch, (int32_t) lpfHomeVel*SPEED_ENC_CONV_FACTOR, (int32_t)lpfZeroOffset*POS_ENC_CONV_FACTOR);
  if (i) return win_printf_OK("Error reading home params\non Cube with serial # %s\n"
			      ,zmagSerialNum);
  if (do_home)
    {
      i = tl_apt_mot_home(mot_context);
      if (i) return win_printf_OK("Error stting home\non Cube with serial # %s\n"
				  ,zmagSerialNum);
      i = immediate_next_available_action(READ_ZMAG_VAL, 0);
      if (i < 0)	my_set_window_title("Could not add pending action!");
    }
  return 0;
}


int do_thorlabs_APT_get_MOT_BlashDistance(void)
{
  register int i, j;
  char message[2048];
  float pfBLashDist;

  if(updating_menu_state != 0)	return D_O_K;
  if (mot_context == NULL)win_printf("APT not initiated");
  message[0] = 0;
  j = t1_apt_mot_get_bldist_l(mot_context, &pfBLashDist);
  if (j) return win_printf_OK("Error reading blash distance\non Cube with serial # %s\n"
			      ,zmagSerialNum);
  snprintf(message,sizeof(message),"Device with serial # %s\n"
	     "Blash distance (%g), %%12f \n"
	   ,zmagSerialNum,pfBLashDist);

  i = win_scanf(message,&pfBLashDist);
  if (i == WIN_CANCEL) return D_O_K;
  j =tl_apt_mot_set_backlash_distance_l(mot_context, pfBLashDist);
  if (j) return win_printf_OK("Error setting blash distance\non Cube with serial # %s\n"
			      ,zmagSerialNum);
  return 0;
}

int do_thorlabs_APT_get_MOT_AxisInfo(void)
{
  register int i, j;
  char message[2048];
  float pfMinPos, pfMaxPos;
  tl_apt_mot_pmdstageaxis_t *pmdparam;
  if(updating_menu_state != 0)	return D_O_K;
  message[0] = 0;
  j = tl_apt_mot_get_pmdstageaxis_l(mot_context,pmdparam);
  if (j) return win_printf_OK("Error reading axis params\non Cube with serial # %s\n"
			      ,zmagSerialNum);
  pfMinPos=pmdparam->min_pos / POS_ENC_CONV_FACTOR;
  pfMaxPos=pmdparam->max_pos / POS_ENC_CONV_FACTOR;
  snprintf(message,sizeof(message),"APT Device with serial # %s\n"
	     "Min position (%g), %%12f \n"
	     "Max position (%g), %%12f \n"

	   ,zmagSerialNum,pfMinPos, pfMaxPos);

  i = win_scanf(message,&pfMinPos, &pfMaxPos);
  if (i == WIN_CANCEL) return D_O_K;
  pmdparam->min_pos=(  int32_t)(pfMinPos * POS_ENC_CONV_FACTOR);
  pmdparam->max_pos=(  int32_t)(pfMaxPos * POS_ENC_CONV_FACTOR);


  j = tl_apt_mot_set_pmdstageaxis_l(mot_context, pmdparam);
  if (j) return win_printf_OK("Error reading axis params\non Cube with serial # %s\n"
			      ,zmagSerialNum);
  return 0;
}


int do_thorlabs_APT_chg_MOT_PID(void)
{
  register int i, j;
  char message[2048];
  int32_t ilProp, ilInt, ilDeriv, ilIntLimit;
  ushort fctrl;


  if(updating_menu_state != 0)	return D_O_K;


  if (mot_context == NULL)return win_printf_OK("APT DC motor %s not found",zmagSerialNum);
  message[0] = 0;
  j = tl_apt_mot_get_PID(mot_context, &ilProp, &ilInt, &ilDeriv, &ilIntLimit,&fctrl);
  if (j) return win_printf_OK("Error reading PID params\non Cube with serial # %s\n"
			      ,zmagSerialNum);
  snprintf(message,sizeof(message),"Device with serial # %s\n"
	     "Proportionnal (%d),  \n"
	     "Integral (%d),  \n"
	     "Derivative (%d),  \n"
	     "Limit (%d),  \n"
	   ,zmagSerialNum,ilProp, ilInt, ilDeriv, ilIntLimit);

  i = win_scanf(message,&ilProp, &ilInt, &ilDeriv, &ilIntLimit);
  if (i == WIN_CANCEL) return D_O_K;
  j = tl_apt_mot_set_PID(mot_context, ilProp, ilInt, ilDeriv, ilIntLimit,(uint16_t)0x0F00);
  if (j) return win_printf_OK("Error reading PID params\non Cube with serial # %s\n"
			      ,zmagSerialNum);

  return 0;
}



int do_thorlabs_APT_set_MOT_pos(void)
{
  register int i, j, k;
  char message[2048], status[64];
  float Position;
  uint32_t StatusBits;
  float z = -1, vcap = -1;
  int limit = 0, statu = 0;

  if(updating_menu_state != 0)	return D_O_K;



  i = _read_magnet_z_value_and_status(&z, &vcap, &limit, &statu);
  if (i) win_printf_OK("Error reading position \non Cube with serial # %s\n"
		       "z = %g vcap = %g\nlimit = %d status = %d\n"
		       ,zmagSerialNum,z, vcap, limit, statu);
  else win_printf_OK("Reading position \non Cube with serial # %s\n"
		       "z = %g vcap = %g\nlimit = %d status = %d\n"
		       ,zmagSerialNum,z, vcap, limit, statu);

 if (mot_context == NULL)return win_printf_OK("APT DC motor %s not found",zmagSerialNum);
  status[0] = message[0] = 0;
  j = t1_apt_mot_get_pos(mot_context,&Position);;
  t1_apt_mot_get_statusbits(mot_context, &StatusBits);
  if (j == 0)
    {
      k = (int)StatusBits&0x07;
      if (k == 1) sprintf(status,"Rev. limit On!");
      else if (k == 2) sprintf(status,"Fwd. limit On!");
      else if (k == 3) sprintf(status," Rev. soft limit On!");
      else if (k == 4) sprintf(status," Fwd. soft limit On!");
      else if (k == 5) sprintf(status," Mot. moving Rev.!");
      else if (k == 6) sprintf(status," Mot. moving Fwd.!");
      else if (k == 5) sprintf(status," Shaft jogging Rev.!");
      else if (k == 6) sprintf(status," Shaft jogging Fwd.!");
      snprintf(message+strlen(message),sizeof(message)-strlen(message),
	       "Cube with serial # %s move to %%12f %s\n"
	       ,zmagSerialNum,status);
    }
  else
    {
      k = (int)StatusBits&0x07;
      if (k == 1) sprintf(status,"Rev. limit On!");
      else if (k == 2) sprintf(status,"Fwd. limit On!");
      else if (k == 3) sprintf(status," Rev. soft limit On!");
      else if (k == 4) sprintf(status," Fwd. soft limit On!");
      else if (k == 5) sprintf(status," Mot. moving Rev.!");
      else if (k == 6) sprintf(status," Mot. moving Fwd.!");
      else if (k == 5) sprintf(status," Shaft jogging Rev.!");
      else if (k == 6) sprintf(status," Shaft jogging Fwd.!");
      snprintf(message+strlen(message),sizeof(message)-strlen(message),
	       "Error on Cube with serial # %s scanf move to %%12f %s\n"
	       ,zmagSerialNum,status);
    }

  i = win_scanf(message, &Position);
  if (i == WIN_CANCEL) return D_O_K;
  j =  tl_apt_mot_set_move_abspos(mot_context, (int32_t) ( Position * POS_ENC_CONV_FACTOR));
  if (j) win_printf_OK("Error setting position \non Cube with serial # %s\n",zmagSerialNum);
  return 0;
}

int do_thorlabs_APT_stop(void)
{
  if(updating_menu_state != 0)	return D_O_K;
    if ((mot_context) != NULL) tl_apt_cleanup(mot_context);
  return 0;
}


int do_thorlabs_APT_set_MOT_VelParams(void)
{
  register int i, j;
  char message[2048];
  float pfMinVel, pfAccn, pfMaxVel;
  int ipfMinVel, ipfAccn, ipfMaxVel;

  if(updating_menu_state != 0)	return D_O_K;
  if (mot_context == NULL)return win_printf_OK("APT DC motor %s not found",zmagSerialNum);
  message[0] = 0;
  j = tl_apt_mot_get_velocity(mot_context, &ipfMinVel, &ipfAccn, &ipfMaxVel);
  pfMinVel=(float)ipfMinVel/SPEED_ENC_CONV_FACTOR;
  pfAccn=(float)ipfAccn/ACC_ENS_CONV_FACTOR;
  pfMaxVel=(float)ipfMaxVel/SPEED_ENC_CONV_FACTOR;
  if (j) return win_printf_OK("Error reading velocity params\non Cube with serial # %s\n"
			      ,zmagSerialNum);
  snprintf(message,sizeof(message),"Device with serial # %s\n"
	     "Min velocity (%g), %%12f \n"
	     "Max velocity (%g), %%12f \n"
	     "Acceleration (%g), %%12f \n"
	   ,zmagSerialNum,pfMinVel, pfMaxVel, pfAccn);

  i = win_scanf(message,&pfMinVel, &pfMaxVel, &pfAccn);
  if (i == WIN_CANCEL) return D_O_K;
  j = tl_apt_mot_set_velocity(mot_context, (int32_t)(pfMinVel*SPEED_ENC_CONV_FACTOR),(int32_t)( pfAccn*ACC_ENS_CONV_FACTOR),(int32_t)( pfMaxVel*SPEED_ENC_CONV_FACTOR));
  if (j) return win_printf_OK("Error reading velocity params\non Cube with serial # %s\n"
			      ,zmagSerialNum);
  return 0;
}



# ifdef KEEP


int  do_motor1_in_turn_apt(void)
{
  register int  i;
  float rot;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf("This routine moves motor 1 \n"
			"to specified position in turn");
    }
  rot = n_rota;
  i = win_scanf("enter motor 2 new position %f",&rot);
  if (i == WIN_CANCEL)	return OFF;
  set_rot_value_apt(rot);
  return 0;
}
int  do_motor1_in_turn_and_wait_apt(char ch)
{
  register int  i;
  float rot;
  char pos[64];

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf("This routine moves motor 1 \n"
			"to specified position in turn");
    }

  rot = read_rot_value_apt();
  snprintf(pos,64,"position read %g \n enter new position %%f",rot);
  rot = n_rota;
  i = win_scanf(pos,&rot);
  if (i == WIN_CANCEL)	return OFF;
  set_rot_value_and_wait_apt(rot);
  return 0;
}



int	do_motor2_in_mm_apt(void)
{
  register  int i;
  float pos;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf("This routine moves motor 1 \n"
			"to specified position in mm");
    }
  /* we first find the data that we need to transform */
  pos =  n_magnet_z;
  i = win_scanf("enter motor 1 new position %f in mm",&pos);
  if (i == WIN_CANCEL)	return OFF;
  set_magnet_z_value_apt(pos);
  return 0;
}
int	do_motor2_in_mm_and_wait_apt(char ch)
{
  register  int i;
  float pos;
  char question[64];

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf("This routine moves motor 2 \n"
			"to specified position in mm");
    }

  pos = read_magnet_z_value_apt();
  snprintf(question,64,"read pos %g \n new pos in mm %%f",pos);
  pos =  n_magnet_z;
  i = win_scanf(question,&pos);
  if (i == WIN_CANCEL)	return OFF;
  set_magnet_z_value_and_wait_apt(pos);
  return 0;
}
int	do_motor2_in_mm_n_time_apt(void)
{
  register  int i, j;
  static float pos0 = 17,  pos_step = .1;
  static int nstep = 5, ntimes = 100;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf("This routine moves motor 1 \n"
			"to specified position in mm");
    }

  pos0 =  n_magnet_z;
  i = win_scanf("Zmag multi test \nenter motor max position in mm "
		"%fpos step %f nstep %d ntimes %d",
		&pos0,&pos_step,&nstep,&ntimes);
  if (i == WIN_CANCEL)	return OFF;
  for ( i = 0; i < ntimes; i++)
    {
      for ( j = 0; j < nstep; j++)
	{
	  set_magnet_z_value_apt(pos0+j*pos_step);
	}
    }
  return 0;
}
int    do_rotate_n_time_apt(void)
{
  register  int i, j;
  static float pos0 = 17,  pos_step = .1;
  static int nstep = 5, ntimes = 100;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf("This routine moves motor 1 \n"
			"to specified position in turns");
    }


  pos0 =  n_rota;
  i = win_scanf("Rotation multi test \nenter motor max position in mm "
		"%fpos step %f nstep %d ntimes %d",
		&pos0,&pos_step,&nstep,&ntimes);
  if (i == WIN_CANCEL)	return OFF;
  for ( i = 0; i < ntimes; i++)
    {
      for ( j = 0; j < nstep; j++)
	{
	  set_rot_value_apt(pos0+j*pos_step);
	}
    }
  return 0;
}

# endif


MENU *apt_magnets_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	//add_item_to_menu(mn,"data set rescale in Y", do_apt_magnets_rescale_data_set,NULL,0,NULL);
	//add_item_to_menu(mn,"plot rescale in Y", do_apt_magnets_rescale_plot,NULL,0,NULL);
	return mn;
}


int	apt_magnets_main(int argc, char **argv)
{
  static int init = 0;
  //	add_plot_treat_menu_item ( "apt_magnets", NULL, apt_magnets_plot_menu(), 0, NULL);

  (void)argc;
  (void)argv;

  if (init == 0)
    {
      add_image_treat_menu_item("Magnets", NULL, thorlabs_APT_plot_menu(), 0, NULL);
      init=1;

    }
    return D_O_K;
}

int	apt_magnets_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  //remove_item_to_menu(plot_treat_menu, "apt_magnets", NULL, NULL);
	return D_O_K;
}
#endif
