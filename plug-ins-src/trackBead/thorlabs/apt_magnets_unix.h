#ifndef _SDI_MAGNETS_APT_H_
#define _SDI_MAGNETS_APT_H_

#include "../../thorlabs_apt_api/thorlabs_apt_api.h"

PXV_FUNC(MENU*, apt_magnets_plot_menu, (void));
PXV_FUNC(int, apt_magnets_main, (int argc, char **argv));
PXV_FUNC(int,do_thorlabs_APT_stop,(void));
PXV_FUNC(int,do_thorlabs_APT_get_MOT_BlashDistance,(void));
PXV_FUNC(int,   do_thorlabs_APT_chg_MOT_PID,(void));
PXV_FUNC(int,do_thorlabs_APT_set_MOT_VelParams,(void));

PXV_FUNC(int, _request_read_rot_value, (int image_n, unsigned long *t0));
PXV_FUNC(float, _read_rot_raw_from_request_answer, (char *answer, int im_n, float val, int *error));
PXV_FUNC(int, _request_set_rot_value_raw, (int image_n, float rot, unsigned long *t0));
PXV_FUNC(int, _check_set_rot_request_answer, (char *answer, int im_n, float rot));
PXV_FUNC(int, _request_set_magnet_z_value_raw, (int image_n, float pos,  unsigned long *t0));
PXV_FUNC(int, _check_set_zmag_request_answer, (char *answer, int im_n, float pos));
PXV_FUNC(int, _request_read_magnet_z_value, (int image_n, unsigned long *t0));
PXV_FUNC(float, _read_zmag_raw_from_request_answer, (char *answer, int im_n, float val, int *error, float *vacp));
PXV_FUNC(int, _request_set_zmag_ref_value, (int image_n, float pos, unsigned long *t0));
PXV_FUNC(int, _check_set_zmag_ref_request_answer, (char *answer, int im_n, float pos));
PXV_FUNC(int, _request_set_rot_ref_value, (int image_n, float rot,  unsigned long *t0));
PXV_FUNC(int, _check_set_rot_ref_request_answer, (char *answer, int im_n, float rot));
PXV_FUNC(int, _read_magnet_z_value_and_status, (float *z, float *vcap, int *limit, int *vcap_servo));
#endif
