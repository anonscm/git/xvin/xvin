/*
 *    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
 */
# ifndef _APT_FLOW_CONTROL_C_
# define _APT_FLOW_CONTROL_C_

# include "allegro.h"
# include "winalleg.h"
# include "xvin.h"


/* If you include other plug-ins header do it here*/
# include "../action.h"
# include "../../cfg_file/Pico_cfg.h"
# include "../../fft2d/fftbtl32n.h"
# include "../fillibbt.h"
# include "../trackBead.h"
# include "../track_util.h"

# include "APTAPI.h"
/* But not below this define */
# define BUILDING_PLUGINS_DLL

# undef _PXV_DLL
# define _PXV_DLL   __declspec(dllexport)

# include "apt_flow_control.h"

# define MAX_APT_UNITS 16

long plNumUnits = -1, plSerialNum[MAX_APT_UNITS];
float pfPosition[MAX_APT_UNITS];
float pf_offset[MAX_APT_UNITS];
float pvel[MAX_APT_UNITS] = {0.1,0.1,0.1,0.1,0.1,0.1,0.1};

// define the seringhe full volume in ml
float ser_vol[MAX_APT_UNITS] = {20,20,1,1,1,1,1,1};
// define the seringhe full range mm per full vol
float ser_range[MAX_APT_UNITS] = {65,65,60,60,60,60,60,60};
// define the seringhe motor range
float ser_mot_range[MAX_APT_UNITS] = {50,50,50,50,50,50,50,50,50};
char mot_name[MAX_APT_UNITS][64];

int	flow_control_main(int argc, char **argv);


void stop_apt(void)
{
  APTCleanUp();
}


//int flow_control_read_MOT_vol(int icube, float *pos_mm, float *micro_liter)

int do_inject_ligase_enzyme_mot(void)
{
  int index;
  register int i, j;
  char message[2048];
  static int init = 0, ligase_index = 1;
  static float micro_en[MAX_APT_UNITS], en_duration[MAX_APT_UNITS], offset[MAX_APT_UNITS];
  static float micro_li[MAX_APT_UNITS], li_duration[MAX_APT_UNITS], li_offset[MAX_APT_UNITS];
  static float micro_rin[MAX_APT_UNITS], rin_duration[MAX_APT_UNITS];
  int im, ien_duration, irin_duration, ioff, ili_duration, ili_off, check, latency;

  if(updating_menu_state != 0)	return D_O_K;
  index = RETRIEVE_MENU_INDEX;
  if (plNumUnits < 0)      return win_printf_OK("APT is not initiated!\n Click on start");
  if (index >= plNumUnits) return win_printf_OK("Mot index too high!");

  if (init == 0)
    {
      for (i = 0; i < MAX_APT_UNITS; i++)
	{
	  micro_en[i] = 1;
	  micro_li[i] = 1;
	  micro_rin[i] = 20;
	  en_duration[i] = 10;
	  li_duration[i] = 10;
	  rin_duration[i] = 20;
	  offset[i] = 5;
	  li_offset[i] = 4;
	}
      i = win_scanf("Define the motor index\n of the ligase %8d\n",&ligase_index);
      if (i == CANCEL) return D_O_K;
      init = 1;
    }

  snprintf(message,sizeof(message),"Injection procces on %s\n"
	   "With simultaneous rincing action\n"
	   "Rincing of %%8f \\mu l lasting %%8f S\n"
	   "%s injection will occur %%8f S after rincing started\n"
	   "will inject %%8f \\mu l during %%8f S\n"
	   "With simultaneous %s injection\n"
	   "of %%8f \\mu l during %%8f S\n"
	   "%s injection will occur %%8f S after rincing started\n"
	   "Rincing should stop after %s and %s injections have finished\n"
	   ,mot_name[index],mot_name[index],mot_name[ligase_index]
	   ,mot_name[ligase_index],mot_name[index],mot_name[ligase_index]);
  i = win_scanf(message,micro_rin+index,rin_duration+index,offset+index,micro_en+index,en_duration+index
		,micro_li+ligase_index,li_duration+ligase_index,li_offset+ligase_index);
  if (i == CANCEL) return D_O_K;
  if (rin_duration[index] < offset[index] + en_duration[index])
    i = win_printf("The rincing phase will stop\nbefore the enzyme injection finished!\nDo you want to precedd ?");
  if (i == CANCEL) return D_O_K;
  if (rin_duration[index] < li_offset[ligase_index] + li_duration[ligase_index])
    i = win_printf("The rincing phase will stop\nbefore the %s injection finished!\nDo you want to precedd ?"
		   ,mot_name[ligase_index]);
  if (i == CANCEL) return D_O_K;

  ien_duration = (int) (Pico_param.camera_param.camera_frequency_in_Hz * en_duration[index]);
  irin_duration  = (int) (Pico_param.camera_param.camera_frequency_in_Hz * rin_duration[index]);
  ili_duration  = (int) (Pico_param.camera_param.camera_frequency_in_Hz * li_duration[ligase_index]);
  ili_off = (int) (Pico_param.camera_param.camera_frequency_in_Hz * li_offset[ligase_index]);
  ioff = (int) (Pico_param.camera_param.camera_frequency_in_Hz * offset[index]);
  check = (int) (Pico_param.camera_param.camera_frequency_in_Hz * 3);
  latency = (int) (Pico_param.camera_param.camera_frequency_in_Hz / 10);
  if (latency < 1) latency = 1;

  //win_printf("ien_duration %d\nirin_duration %d\nioff %d\ncheck %d\nlatency %d\nindex %d"
  //	     ,ien_duration,irin_duration,ioff,check,latency, index);

  im = track_info->imi[track_info->c_i] + 5;
  i = fill_next_available_action(im, RINCE_SAMPLE, micro_rin[index]);
  if (i >= 0)
    {
      action_pending[i].phase = PENDING;
      action_pending[i].spare = 0;
      action_pending[i].action_duration = rin_duration[index];
      action_pending[i].im_action_duration = irin_duration;
      action_pending[i].check_duration = check;
      action_pending[i].maximum_duration = (3*irin_duration)/2;
      action_pending[i].check_latency = latency;
    }
  else win_printf("Rincing Action was not registered!\nim %d type %d vol %f"
		  ,im, RINCE_SAMPLE, micro_rin[index]);
  im += ioff;
  j = i;
  i = fill_next_available_action(im, INJECT_ENZYME, micro_en[index]);
  if (i >= 0)
    {
      action_pending[i].phase = PENDING;
      action_pending[i].spare = index;
      action_pending[i].action_duration = en_duration[index];
      action_pending[i].im_action_duration = ien_duration;
      action_pending[i].check_duration = check;
      action_pending[i].maximum_duration = (3*ien_duration)/2;
      action_pending[i].check_latency = latency;
    }
  else win_printf("Injecting Action was not registered!\nim %d type %d vol %f"
		  ,im, INJECT_ENZYME, micro_en[index]);

  im += ili_off - ioff;
  j = i;
  i = fill_next_available_action(im, INJECT_TWO_ENZYMES, micro_li[ligase_index]);
  if (i >= 0)
    {
      action_pending[i].phase = PENDING;
      action_pending[i].spare = ligase_index;
      action_pending[i].action_duration = li_duration[ligase_index];
      action_pending[i].im_action_duration = ili_duration;
      action_pending[i].check_duration = check;
      action_pending[i].maximum_duration = (3*ili_duration)/2;
      action_pending[i].check_latency = latency;
    }
  else win_printf("Injecting Action was not registered!\nim %d type %d vol %f"
		  ,im, INJECT_TWO_ENZYMES, micro_li[ligase_index]);


  //  win_printf("Action rincing %d spare %d im0 %d\ninjecting %d spare %d im0 %d"
  //     ,j,action_pending[j].spare,action_pending[j].imi0,i,action_pending[i].spare,action_pending[i].imi0);
  return 0;
}




int do_inject_mot(void)
{
  int index;
  register int i, j;
  char message[2048];
  static int init = 0;
  static float micro_en[MAX_APT_UNITS], en_duration[MAX_APT_UNITS], offset[MAX_APT_UNITS];
  static float micro_rin[MAX_APT_UNITS], rin_duration[MAX_APT_UNITS];
  int im, ien_duration, irin_duration, ioff, check, latency;

  if(updating_menu_state != 0)	return D_O_K;
  index = RETRIEVE_MENU_INDEX;
  if (plNumUnits < 0)      return win_printf_OK("APT is not initiated!\n Click on start");
  if (index >= plNumUnits) return win_printf_OK("Mot index too high!");

  if (init == 0)
    {
      for (i = 0; i < MAX_APT_UNITS; i++)
	{
	  micro_en[i] = 1;
	  micro_rin[i] = 20;
	  en_duration[i] = 10;
	  rin_duration[i] = 20;
	  offset[i] = 5;
	}
      init = 1;
    }

  snprintf(message,sizeof(message),"Injection procces on %s\n"
	   "With simultaneous rincing action\n"
	   "Rincing of %%8f \\mu l lasting %%8f S\n"
	   "%s injection will occur %%8f S after rincing started\n"
	   "will inject %%8f \\mu l during %%8f S\n"
	   "Rincing should stop after %s injection is finished\n"
	   ,mot_name[index],mot_name[index],mot_name[index]);
  i = win_scanf(message,micro_rin+index,rin_duration+index,offset+index,micro_en+index,en_duration+index);
  if (i == CANCEL) return D_O_K;
  if (rin_duration[index] < offset[index] + en_duration[index])
    i = win_printf("The rincing phase will stop\nbefore the enzyme injection finished!\nDo you want to precedd ?");
  if (i == CANCEL) return D_O_K;

  ien_duration = (int) (Pico_param.camera_param.camera_frequency_in_Hz * en_duration[index]);
  irin_duration  = (int) (Pico_param.camera_param.camera_frequency_in_Hz * rin_duration[index]);
  ioff = (int) (Pico_param.camera_param.camera_frequency_in_Hz * offset[index]);
  check = (int) (Pico_param.camera_param.camera_frequency_in_Hz * 3);
  latency = (int) (Pico_param.camera_param.camera_frequency_in_Hz / 10);
  if (latency < 1) latency = 1;

  //win_printf("ien_duration %d\nirin_duration %d\nioff %d\ncheck %d\nlatency %d\nindex %d"
  //	     ,ien_duration,irin_duration,ioff,check,latency, index);

  im = track_info->imi[track_info->c_i] + 5;
  i = fill_next_available_action(im, RINCE_SAMPLE, micro_rin[index]);
  if (i >= 0)
    {
      action_pending[i].phase = PENDING;
      action_pending[i].spare = 0;
      action_pending[i].action_duration = rin_duration[index];
      action_pending[i].im_action_duration = irin_duration;
      action_pending[i].check_duration = check;
      action_pending[i].maximum_duration = (3*irin_duration)/2;
      action_pending[i].check_latency = latency;
    }
  else win_printf("Rincing Action was not registered!\nim %d type %d vol %f"
		  ,im, RINCE_SAMPLE, micro_rin[index]);
  im += ioff;
  j = i;
  i = fill_next_available_action(im, INJECT_ENZYME, micro_en[index]);
  if (i >= 0)
    {
      action_pending[i].phase = PENDING;
      action_pending[i].spare = index;
      action_pending[i].action_duration = en_duration[index];
      action_pending[i].im_action_duration = ien_duration;
      action_pending[i].check_duration = check;
      action_pending[i].maximum_duration = (3*ien_duration)/2;
      action_pending[i].check_latency = latency;
    }
  else win_printf("Injecting Action was not registered!\nim %d type %d vol %f"
		  ,im, INJECT_ENZYME, micro_en[index]);

  //  win_printf("Action rincing %d spare %d im0 %d\ninjecting %d spare %d im0 %d"
  //     ,j,action_pending[j].spare,action_pending[j].imi0,i,action_pending[i].spare,action_pending[i].imi0);
  return 0;
}



int do_rince(void)
{
  int index;
  register int i;
  static int init = 0;
  static float micro_rin, rin_duration;
  int im, irin_duration, check, latency;

  if(updating_menu_state != 0)	return D_O_K;
  index = 0;
  if (plNumUnits < 0)      return win_printf_OK("APT is not initiated!\n Click on start");
  if (index >= plNumUnits) return win_printf_OK("Mot index too high!");

  if (init == 0)
    {
      micro_rin = 20;
      rin_duration = 20;
      init = 1;
    }

  i = win_scanf("Rincing  procces  alone\n"
		"Rincing of %8f \\mu l \nlasting %8f S\n",&micro_rin,&rin_duration);
  if (i == CANCEL) return D_O_K;

  irin_duration  = (int) (Pico_param.camera_param.camera_frequency_in_Hz * rin_duration);
  check = (int) (Pico_param.camera_param.camera_frequency_in_Hz * 3);
  latency = (int) (Pico_param.camera_param.camera_frequency_in_Hz / 10);
  if (latency < 1) latency = 2;

  im = track_info->imi[track_info->c_i] + 5;
  i = fill_next_available_action(im, RINCE_SAMPLE, micro_rin);
  if (i >= 0)
    {
      action_pending[i].phase = PENDING;
      action_pending[i].spare = index;
      action_pending[i].action_duration = rin_duration;
      action_pending[i].im_action_duration = irin_duration;
      action_pending[i].check_duration = check;
      action_pending[i].maximum_duration = (3*irin_duration)/2;
      action_pending[i].check_latency = latency;
    }
  return 0;
}


int do_flow_control_start(void)
{
  register int i, j;
  int ret, er = 0, prog_restart = 0;
  char message[2048];
  const char *nm;
  char szModel[256], szSWVer[256], szHWNotes[256], cfg_nb[128];
  float pfMinVel, pfAccn, pfMaxVel;
  float pfMinPos, pfMaxPos, pfPitch, vel, pos, vol;;
  long plUnits;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine test Thorlabs APT lib");
    }
  //  i = win_scanf("Do you want RS232 error window on %b",&dlg);
  //if (i == CANCEL) return D_O_K;
  i = APTInit();
  APTCleanUp();
  i = APTInit();
  if (i != 0)
    {
      APTCleanUp();
      i = APTInit();
    }
  if (i != 0)
    {
      win_printf("APTinit failed !");
      APTCleanUp();
      return 0;
    }
  //if (dlg == 0)
  EnableEventDlg(FALSE);

  ret = GetNumHWUnitsEx(HWTYPE_TDC001, &plNumUnits);
  //win_printf("GetNumHWUnitsEx Ret %d num -> %d",ret, (int)plNumUnits);
  if (plNumUnits < 1)
    {
      win_printf("No device connected");
      APTCleanUp();
      return 0;
    }
  //win_printf("%d devices connected",plNumUnits);
  atexit(stop_apt);
  snprintf(message,sizeof(message),"You have %d TDC001 cube connected\n",(int)plNumUnits);
  for (i = 0; i < plNumUnits && i < MAX_APT_UNITS; i++)
    {
      er = 0;
      ret = GetHWSerialNumEx(HWTYPE_TDC001, i, plSerialNum + i);
      //win_printf("GetHWSerialNumEx Ret %d num %d -> %d",ret, i,(int)plSerialNum[i]);
      er += InitHWDevice(plSerialNum[i]);
      er += MOT_GetPosition(plSerialNum[i], pfPosition + i);
      if (er == 0) prog_restart += (fabs(pfPosition[i]) < 0.005) ? 0 : 1;
      er += GetHWInfo(plSerialNum[i], szModel, 256, szSWVer, 256, szHWNotes, 256);
      for (j = 0; j < sizeof(szHWNotes) && szHWNotes[j] != 0; j++)
	if (szHWNotes[j] < 32) szHWNotes[j] = 0;
      //win_printf("Mot num %d -> %g",plSerialNum[i],pfPosition[i]);
      if (er == 0)
	snprintf(message+strlen(message),sizeof(message)-strlen(message),
		 "Cube %d with serial # %d at %g model %s ver.%s\n"
		 ,i,(int)plSerialNum[i],pfPosition[i],szModel, szSWVer);
      else
	snprintf(message+strlen(message),sizeof(message)-strlen(message),
		 "Com error on Cube %d with serial # %d at %g model %s ver. %s\n"
		 ,i,(int)plSerialNum[i],pfPosition[i],szModel, szSWVer);
      //GetHWInfo(long lSerialNum, TCHAR *szModel, long lModelLen, TCHAR *szSWVer, long lSWVerLen, TCHAR *szHWNotes, long lHWNotesLen);
      if (MOT_GetStageAxisInfo(plSerialNum[i], &pfMinPos, &pfMaxPos, &plUnits, &pfPitch))
	win_printf_OK("Error reading axis info\non Cube with serial # %d\n",(int)plSerialNum[i]);
      pfMaxPos = 100;
      pfMinPos = -100;
      if (MOT_SetStageAxisInfo(plSerialNum[i], pfMinPos, pfMaxPos, plUnits, pfPitch))
	win_printf_OK("Error setting axis info\non Cube with serial # %d\n",(int)plSerialNum[i]);
      if (MOT_SetBLashDist(plSerialNum[i], 0.0))
	win_printf_OK("Error setting  blash distance\non Cube with serial # %d\n",(int)plSerialNum[i]);

      snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_max_vel",(int)plSerialNum[i]);
      vel = get_config_float("FLOW_CONTROL",cfg_nb,0.1);
      snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_position",(int)plSerialNum[i]);
      pos = get_config_float("FLOW_CONTROL",cfg_nb,0);
      pf_offset[i] = pos - pfPosition[i];
      snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_volume",(int)plSerialNum[i]);
      vol = get_config_float("FLOW_CONTROL",cfg_nb,ser_vol[i]);
      ser_vol[i] = vol;
      snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_volume_range",(int)plSerialNum[i]);
      vol = get_config_float("FLOW_CONTROL",cfg_nb,ser_range[i]);
      ser_range[i] = vol;
      if (MOT_GetVelParams(plSerialNum[i], &pfMinVel, &pfAccn, &pfMaxVel))
	win_printf_OK("Error reading axis velocity\non Cube with serial # %d\n",(int)plSerialNum[i]);
      pvel[i] = vel;
      if (MOT_SetVelParams(plSerialNum[i], pfMinVel, pfAccn, vel))
	win_printf_OK("Error setting axis velocity\non Cube with serial # %d\n",(int)plSerialNum[i]);
      snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_name",(int)plSerialNum[i]);
      nm = get_config_string("FLOW_CONTROL",cfg_nb,NULL);
      if (nm == NULL) snprintf(mot_name[i],sizeof(mot_name[i]),"Mot_%d",(int)plSerialNum[i]);
      else strncpy(mot_name[i],nm,64);
      if (i == 0)
	{
	  add_item_to_menu(flow_control_plot_menu(),"\0",NULL,NULL,0, NULL);
	  add_item_to_menu(flow_control_plot_menu(),"Rincing",do_rince,NULL,MENU_INDEX(i),NULL);
	}
      if (i > 0)
	{
	  snprintf(cfg_nb,sizeof(cfg_nb),"Inject in %s",mot_name[i]);
	  add_item_to_menu(flow_control_plot_menu(),strdup(cfg_nb),
			   do_inject_mot,NULL,MENU_INDEX(i),NULL);
	  snprintf(cfg_nb,sizeof(cfg_nb),"Inject %s with ligase",mot_name[i]);
	  add_item_to_menu(flow_control_plot_menu(),strdup(cfg_nb),
			   do_inject_ligase_enzyme_mot,NULL,MENU_INDEX(i),NULL);
	}
    }
  write_Pico_config_file();
  win_printf(message);
  return 0;
}




int do_flow_control_stop(void)
{
  if(updating_menu_state != 0)	return D_O_K;
  APTCleanUp();
  return 0;
}

int do_flow_control_read_MOT_pos(void)
{
  register int i, j;
  char message[2048], cfg_nb[128];
  const char *nm = NULL;
  float vol, vol_range, tmp = 0;

  if(updating_menu_state != 0)	return D_O_K;
  if (plNumUnits < 0) win_printf("APT not initiated");
  message[0] = 0;
  for (i = 0; i < plNumUnits && i < MAX_APT_UNITS; i++)
    {
      snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_name",(int)plSerialNum[i]);
      nm = get_config_string("FLOW_CONTROL",cfg_nb,"\0");
      snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_volume",(int)plSerialNum[i]);
      vol = get_config_float("FLOW_CONTROL",cfg_nb,ser_vol[i]);
      snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_volume_range",(int)plSerialNum[i]);
      vol_range = get_config_float("FLOW_CONTROL",cfg_nb,ser_range[i]);

      j = MOT_GetPosition(plSerialNum[i], pfPosition + i);
      tmp = (vol_range > 0) ? pfPosition[i]*vol/vol_range : 0;
      if (j == 0)
	{
	  if (nm == NULL)
	    snprintf(message+strlen(message),sizeof(message)-strlen(message),
		     "Cube %d with serial # %d at %g\n",i,(int)plSerialNum[i],pfPosition[i]);
	  else
	    snprintf(message+strlen(message),sizeof(message)-strlen(message),
		     "Cube %d at %7.4f mm vol %7.4f ml named %s\n",i,pfPosition[i],tmp,nm);
	}
      else
	{
	  if (nm == NULL)
	    snprintf(message+strlen(message),sizeof(message)-strlen(message),
		     "error on Cube %d with serial # %d at %g\n",
		     i,(int)plSerialNum[i],pfPosition[i]);
	  else
	    snprintf(message+strlen(message),sizeof(message)-strlen(message),
		     "error on Cube %d named %s at %g\n",
		     i,nm,pfPosition[i]);

	}
    }
  win_printf(message);
  return 0;
}

int do_flow_control_set_MOT_pos(void)
{
  register int i, j, k;
  char message[2048], status[16];
  float newPosition[MAX_APT_UNITS];
  long plStatusBits[MAX_APT_UNITS];

  if(updating_menu_state != 0)	return D_O_K;
  if (plNumUnits < 0) return win_printf_OK("APT not initiated");
  status[0] = message[0] = 0;
  for (i = 0; i < plNumUnits && i < MAX_APT_UNITS; i++)
    {
      j = MOT_GetPosition(plSerialNum[i], newPosition + i);
      MOT_GetStatusBits(plSerialNum[i], plStatusBits + i);
      k = (int)plStatusBits[i]&0x07;
      if (k == 1) sprintf(status,"Rev. limit On!");
      else if (k == 2) sprintf(status,"Fwd. limit On!");
      else if (k == 3) sprintf(status," Rev. soft limit On!");
      else if (k == 4) sprintf(status," Fwd. soft limit On!");
      else if (k == 5) sprintf(status," Mot. moving Rev.!");
      else if (k == 6) sprintf(status," Mot. moving Fwd.!");
      else if (k == 7) sprintf(status," Shaft jogging Rev.!");
      else if (k == 8) sprintf(status," Shaft jogging Fwd.!");
      if (j == 0)
	{
	  pfPosition[i] = newPosition[i];
	  snprintf(message+strlen(message),sizeof(message)-strlen(message),
		   "Cube %d with serial # %d move to %%12f %s\n"
		   ,i,(int)plSerialNum[i],status);
	}
      else
	{
	  snprintf(message+strlen(message),sizeof(message)-strlen(message),
		   "Error on Cube %d with serial # %d move to %%12f %s\n"
		   ,i,(int)plSerialNum[i],status);
	}
    }
  if (plNumUnits == 0) return 0;
  if (plNumUnits == 1) i = win_scanf(message,newPosition);
  else if (plNumUnits == 2) i = win_scanf(message,newPosition,newPosition+1);
  else if (plNumUnits == 3) i = win_scanf(message,newPosition,newPosition+1,newPosition+2);
  else if (plNumUnits == 4) i = win_scanf(message,newPosition,newPosition+1,newPosition+2,newPosition+3);
  else i = win_scanf(message,newPosition,newPosition+1,newPosition+2,newPosition+3,newPosition+4);
  if (i == CANCEL) return D_O_K;
  for (i = 0; i < plNumUnits && i < MAX_APT_UNITS; i++)
    {
      if (pfPosition[i] != newPosition[i])
	{
	  j = MOT_MoveAbsoluteEx(plSerialNum[i], newPosition[i], 0);
	  if (j) win_printf_OK("Error setting position \non Cube %d with serial # %d\n",i,(int)plSerialNum[i]);
	}
    }
  return 0;
}



int do_flow_control_sent_MOT_vol(void)
{
  register int i, j, k;
  char message[2048], status[16], cfg_nb[128];
  const char *nm = NULL;
  static int first = 1;
  static float newPosition[MAX_APT_UNITS], duration[MAX_APT_UNITS];
  long plStatusBits[MAX_APT_UNITS];
  float pfMinVel, pfAccn, pfMaxVel;
  float tmp, vel, vol, vol_range;

  if(updating_menu_state != 0)	return D_O_K;
  if (plNumUnits < 0) return win_printf_OK("APT not initiated");
  status[0] = message[0] = 0;
  for (i = 0; i < plNumUnits && i < MAX_APT_UNITS; i++)
    {
      j = MOT_GetPosition(plSerialNum[i], pfPosition + i);
      if (first) newPosition[i] = 0;
      if (first) duration[i] = 1;
      MOT_GetStatusBits(plSerialNum[i], plStatusBits + i);
      k = (int)plStatusBits[i]&0x07;
      if (k == 1) sprintf(status,"Rev. limit On!");
      else if (k == 2) sprintf(status,"Fwd. limit On!");
      else if (k == 3) sprintf(status," Rev. soft limit On!");
      else if (k == 4) sprintf(status," Fwd. soft limit On!");
      else if (k == 5) sprintf(status," Mot. moving Rev.!");
      else if (k == 6) sprintf(status," Mot. moving Fwd.!");
      else if (k == 7) sprintf(status," Shaft jogging Rev.!");
      else if (k == 8) sprintf(status," Shaft jogging Fwd.!");
      snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_name",(int)plSerialNum[i]);
      nm = get_config_string("FLOW_CONTROL",cfg_nb,"\0");
      if (j == 0)
	{
	  //pfPosition[i] = newPosition[i];
	  if (nm == NULL)
	    snprintf(message+strlen(message),sizeof(message)-strlen(message),
		     "Cube %d with serial # %d send %%6f \\mu l in %%6f S %s\n"
		     ,i,(int)plSerialNum[i],status);
	  else
	    snprintf(message+strlen(message),sizeof(message)-strlen(message),
		     "Cube %d send %%6f \\mu l in %%6f S to %s %s\n"
		     ,i,nm,status);
	}
      else
	{
	  if (nm == NULL)
	    snprintf(message+strlen(message),sizeof(message)-strlen(message),
		     "Error on Cube %d with serial # %d send %%6f \\mu l in %%6f S %s\n"
		     ,i,(int)plSerialNum[i],status);
	  else
	    snprintf(message+strlen(message),sizeof(message)-strlen(message),
		     "Error on Cube %d send %%6f \\mu l in %%6f S to %s %s\n"
		     ,i,nm,status);
	}
    }
  first = 0;
  if (plNumUnits == 0) return 0;
  if (plNumUnits == 1) i = win_scanf(message,newPosition,duration);
  else if (plNumUnits == 2)
    i = win_scanf(message,newPosition,duration,newPosition+1,duration+1);
  else if (plNumUnits == 3)
    i = win_scanf(message,newPosition,duration,newPosition+1,duration+1
		  ,newPosition+2,duration+2);
  else if (plNumUnits == 4)
    i = win_scanf(message,newPosition,duration,newPosition+1,duration+1
		  ,newPosition+2,duration+2,newPosition+3,duration+3);
  else if (plNumUnits == 5)
    i = win_scanf(message,newPosition,duration,newPosition+1,duration+1
		  ,newPosition+2,duration+2,newPosition+3,duration+3
		  ,newPosition+4,duration+4);
  else if (plNumUnits == 6)
    i = win_scanf(message,newPosition,duration,newPosition+1,duration+1
		  ,newPosition+2,duration+2,newPosition+3,duration+3
		  ,newPosition+4,duration+4,newPosition+5,duration+5);
  else if (plNumUnits == 7)
    i = win_scanf(message,newPosition,duration,newPosition+1,duration+1
		  ,newPosition+2,duration+2,newPosition+3,duration+3
		  ,newPosition+4,duration+4,newPosition+5,duration+5
		  ,newPosition+6,duration+6);
  else if (plNumUnits == 8)
    i = win_scanf(message,newPosition,duration,newPosition+1,duration+1
		  ,newPosition+2,duration+2,newPosition+3,duration+3
		  ,newPosition+4,duration+4,newPosition+5,duration+5
		  ,newPosition+6,duration+6,newPosition+7,duration+7);
  else return win_printf_OK("Too many cubes !");
  if (i == CANCEL) return D_O_K;
  for (i = 0; i < plNumUnits && i < MAX_APT_UNITS; i++)
    {
      if (fabs(newPosition[i]) > 0.01)
	{
	  // do speed !
	  snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_volume",(int)plSerialNum[i]);
	  vol = get_config_float("FLOW_CONTROL",cfg_nb,ser_vol[i]);
	  snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_volume_range",(int)plSerialNum[i]);
	  vol_range = get_config_float("FLOW_CONTROL",cfg_nb,ser_range[i]);
	  tmp = newPosition[i]*vol_range/(1000*vol);
	  if (MOT_GetVelParams(plSerialNum[i], &pfMinVel, &pfAccn, &pfMaxVel))
	    win_printf_OK("Error reading axis velocity\non Cube with serial # %d\n",(int)plSerialNum[i]);
	  duration[i] = fabs(duration[i]);
	  if (duration[i] < 1.0) duration[i]  = 1;
	  vel = fabs(tmp/duration[i]);
	  if (vel > 2.5) vel = 2.5;
	  if (vel < 0.0001) vel = 0.0001;
	  if (MOT_SetVelParams(plSerialNum[i], pfMinVel, pfAccn, vel))
	    win_printf_OK("Error setting axis velocity\non Cube with serial # %d\n",(int)plSerialNum[i]);
	  tmp += pfPosition[i];
	  j = MOT_MoveAbsoluteEx(plSerialNum[i], tmp, 0);
	  if (j) win_printf_OK("Error setting position \non Cube %d with serial # %d\n",i,(int)plSerialNum[i]);
	  snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_position",(int)plSerialNum[i]);
	  set_config_float("FLOW_CONTROL",cfg_nb,tmp);
	}
    }
  write_Pico_config_file();
  return 0;
}




int flow_control_sent_MOT_vol(int icube, float micro_liter, float duration)
{
  int err = 0;
  char cfg_nb[128];
  float pfMinVel, pfAccn, pfMaxVel;
  float tmp, vel, vol, vol_range;

  if (icube < 0 || icube >= plNumUnits) return 0x80;
  if (fabs(micro_liter) < 0.01) return 0x10;
  if (plNumUnits < 0) return 0x20;

  vol = ser_vol[icube];
  vol_range = ser_range[icube];
  tmp = micro_liter*vol_range/(1000*vol);
  if (MOT_GetVelParams(plSerialNum[icube], &pfMinVel, &pfAccn, &pfMaxVel))
    err |= 0x01;
  duration = fabs(duration);
  if (duration < 1.0) duration  = 1;
  vel = fabs(tmp/duration);
  if (vel > 2.5) vel = 2.5;
  if (vel < 0.0001) vel = 0.0001;
  if (MOT_SetVelParams(plSerialNum[icube], pfMinVel, pfAccn, vel))
    err |= 0x02;
  tmp += pfPosition[icube];
  if (MOT_MoveAbsoluteEx(plSerialNum[icube], tmp, 0))
    err |= 0x04;
  snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_position",(int)plSerialNum[icube]);
  set_config_float("FLOW_CONTROL",cfg_nb,tmp);
  return err;
}

int flow_control_read_MOT_vol(int icube, float *pos_mm, float *micro_liter)
{
  int err = 0;
  char cfg_nb[128];
  float tmp, vol, vol_range;

  if (icube < 0 || icube >= plNumUnits) return 0x80;
  if (plNumUnits < 0) return 0x20;

  vol = ser_vol[icube];
  vol_range = ser_range[icube];

  err = MOT_GetPosition(plSerialNum[icube], pfPosition + icube);
  if (pos_mm != NULL) *pos_mm = pfPosition[icube];
  if (micro_liter != NULL)
    {
      tmp = (vol_range > 0) ? 1000*pfPosition[icube]*vol/vol_range : 0;
      *micro_liter = tmp;
    }
  snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_position",(int)plSerialNum[icube]);
  set_config_float("FLOW_CONTROL",cfg_nb,pfPosition[icube]);
  return err;
}



int do_flow_control_set_Cube_properties(void)
{
  register int i;
  static int devid = 0;
  char message[2048], name[128], cfg_nb[128];
  const char *nm;
  float vol = 0, vol_range = 0, mot_range = 0;

  if(updating_menu_state != 0)	return D_O_K;
  if (plNumUnits < 0) win_printf("APT not initiated");
  message[0] = 0;
  if (plNumUnits > 1)
    {
      snprintf(message,sizeof(message),"Choose your device: \n");
      for (i = 0; i < plNumUnits && i < 16; i++)
	{
	  snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_name",(int)plSerialNum[i]);
	  nm = get_config_string("FLOW_CONTROL",cfg_nb,"\0");
	  if (nm == NULL)
	    snprintf(message+strlen(message),sizeof(message)-strlen(message)
		     ,"Cube %d with serial # %d \n",i,(int)plSerialNum[i]);
	  else snprintf(message+strlen(message),sizeof(message)-strlen(message)
			,"Cube %d with name %s (%d)\n",i,nm,(int)plSerialNum[i]);
	}
      snprintf(message+strlen(message),sizeof(message),"Device index %%d \n");
      i = win_scanf(message,&devid);
      if (i == CANCEL) return D_O_K;
    }
  else devid = 0;
  snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_name",(int)plSerialNum[devid]);
  nm = get_config_string("FLOW_CONTROL",cfg_nb,NULL);
  if (nm != NULL)
      strncpy(name,nm,sizeof(name));
  else name[0] = 0;
  snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_volume",(int)plSerialNum[devid]);
  vol = get_config_float("FLOW_CONTROL",cfg_nb,0);
  snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_volume_range",(int)plSerialNum[devid]);
  vol_range = get_config_float("FLOW_CONTROL",cfg_nb,0);
  snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_stage_range",(int)plSerialNum[devid]);
  mot_range = get_config_float("FLOW_CONTROL",cfg_nb,0);

  snprintf(message,sizeof(message),"Device %d with serial # %d\n"
	   "Define name %%20ls\n"
	   "Seringhe volume (%g), %%8f {\\sl ml}\n"
	   "Piston travel (%g), %%8f {\\sl mm}\n"
	   "stage range (%g), %%8f {\\sl mm}\n"
	   ,devid,(int)plSerialNum[devid],vol,vol_range,mot_range);

  i = win_scanf(message,name,&vol,&vol_range,&mot_range);
  if (i == CANCEL) return D_O_K;
  snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_name",(int)plSerialNum[devid]);
  set_config_string("FLOW_CONTROL",cfg_nb,name);
  snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_volume",(int)plSerialNum[devid]);
  set_config_float("FLOW_CONTROL",cfg_nb,vol);
  snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_volume_range",(int)plSerialNum[devid]);
  set_config_float("FLOW_CONTROL",cfg_nb,vol_range);
  snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_stage_range",(int)plSerialNum[devid]);
  set_config_float("FLOW_CONTROL",cfg_nb,mot_range);
  write_Pico_config_file();
  return 0;
}




MENU *flow_control_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	//add_item_to_menu(mn,"data set rescale in Y", do_apt_flow_control_rescale_data_set,NULL,0,NULL);
	//add_item_to_menu(mn,"plot rescale in Y", do_apt_flow_control_rescale_plot,NULL,0,NULL);
	return mn;
}

int	flow_control_main(int argc, char **argv)
{
  static int init = 0;
  //	add_plot_treat_menu_item ( "apt_flow_control", NULL, apt_flow_control_plot_menu(), 0, NULL);



  if (init == 0)
  {
      add_item_to_menu(flow_control_plot_menu(),"start",
		       do_flow_control_start,NULL,0,NULL);
      add_item_to_menu(flow_control_plot_menu(),"Read moteur position",
		       do_flow_control_read_MOT_pos,NULL,0,NULL);
      add_item_to_menu(flow_control_plot_menu(),"Set cube properties",
		       do_flow_control_set_Cube_properties,NULL,0,NULL);
      add_item_to_menu(flow_control_plot_menu(),"Inject volume",
		       do_flow_control_sent_MOT_vol,NULL,0,NULL);

      init = 1;
      add_plot_treat_menu_item ( "apt_flow_control", NULL, flow_control_plot_menu(), 0, NULL);
      add_image_treat_menu_item ("apt_flow_control", NULL, flow_control_plot_menu(), 0, NULL);
    }
    return D_O_K;
}

int	flow_control_unload(int argc, char **argv)
{
  //remove_item_to_menu(plot_treat_menu, "apt_flow_control", NULL, NULL);
	return D_O_K;
}
#endif
