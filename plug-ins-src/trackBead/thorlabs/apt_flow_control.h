#ifndef _PICO_FLOW_CONTROL_H_
#define _PICO_FLOW_CONTROL_H_

PXV_FUNC(int, flow_control_sent_MOT_vol, (int icube, float micro_liter, float duration));
PXV_FUNC(int, flow_control_read_MOT_vol, (int icube, float *pos_mm, float *micro_liter));
PXV_FUNC(MENU*, flow_control_plot_menu, (void));
PXV_FUNC(int, flow_control_main, (int argc, char **argv));



#endif

