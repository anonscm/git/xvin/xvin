#pragma once

#include "../../thorlabs_apt_api/thorlabs_apt_api.h"


int do_thorlabs_APT_set_MOT_pos(void);
int _set_apt_motor_value_and_wait(tl_apt_context *mot_context,float pos);
float _get_Vcap_min_t(void);
int _set_Vcap_min_t(float volts);
int _set_apt_motors_origin(tl_apt_context *mot_context,float pos,int dir);
int _get_apt_motor_pid(tl_apt_context *mot_context,int num);
int _set_apt_motor_pid(tl_apt_context *mot_context,int num,int val);
float _get_apt_motor_speed(tl_apt_context *mot_context);
int _set_apt_motor_speed(tl_apt_context *mot_context,float v);
float _get_apt_motors_speed(tl_apt_context *mot_context);
int _set_apt_motors_speed(tl_apt_context *mot_context,float v);
int _read_apt_motor_value_and_status(tl_apt_context *mot_context,float *z,float *vcap,int *limit,int *status);
float _read_apt_motor_value(tl_apt_context *mot_context);
int _set_apt_motor_rel_value(tl_apt_context *mot_context,float pos);
int _set_apt_motor_value(tl_apt_context *mot_context,float pos);

int _init_apt_motors_OK(char *serial, tl_apt_context **mot_context);
void stop_apt_mot(tl_apt_context *mot_context);
char *describe_apt_motors_device_apt(void);
int do_thorlabs_APT_set_MOT_VelParams_t(void);
int do_thorlabs_APT_chg_MOT_PID_t(void);
int grab_plot_from_serial_port_t(void);
int do_thorlabs_APT_get_MOT_home_t(void);
int do_thorlabs_APT_get_MOT_BlashDistance_t(void);
int do_thorlabs_APT_stop_t(void);
int do_thorlabs_APT_get_MOT_AxisInfo_t(void);
int do_thorlabs_APT_set_MOT_pos_t(void);
