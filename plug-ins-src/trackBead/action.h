
#ifndef _ACTION_H_
#define _ACTION_H_


# define MV_OBJ_ABS         0x00000001
# define MV_OBJ_REL         0x00000002
# define MV_ZMAG_ABS        0x00000004
# define MV_ZMAG_REL        0x00000008
# define MV_ROT_ABS         0x00000010
# define MV_ROT_REL         0x00000020
# define CHG_ROT_REF        0x00000040
# define CHG_ZMAG_REF       0x00000080

# define SAVE_BEAD_CROSS    0x00000100
# define RESTORE_BEAD_CROSS 0x00000200

# define GET_TEMPERATURE    0x00000400
# define GET_FOCUS_POS      0x00000800
# define READ_ZMAG_VAL      0x00001000
# define READ_ROT_VAL       0x00002000
# define CHG_ZMAG_PID       0x00004000
# define CHG_ROT_PID        0x00008000
# define READ_TRANSIENT     0x00010000
# define READ_ZMAG_PID      0x00020000
# define READ_ROT_PID       0x00040000
# define READ_T0_PID        0x00080000
# define CHG_T0_PID         0x00100000

# define READ_LED_VAL       0x00400000
# define CHG_LED_VAL        0x00800000

# define READ_GAIN_EXPO     0x01000000
# define CHG_CAMERA_GAIN    0x02000000
# define CHG_CAMERA_EXPO    0x04000000
# define CHG_TEMPERATURE    0x08000000

# define CHG_ZMAG_FACTOR    0x10000000 //tv 09/03 : change translation factor between motor and magnets
# define GRAB_FRAME         0x20000000 //tv grab frames during motion : not a job because it is prioritary



PXV_FUNC(int, immediate_next_available_action, (int type, float value));

PXV_FUNC(int, immediate_next_available_action_spare,(int type, float value, int spare));
PXV_FUNC(int, fill_next_available_action, (int im, int type, float value));
PXV_FUNC(int, fill_next_available_action_with_spare, (int im, int type, float value, int spare));
PXV_FUNC(int, fill_next_available_periodic_action, (int im, int type, float value, int period));

PXV_FUNC(int, find_next_action, (int imi));
PXV_FUNC(int, find_next_action_by_type, (int imi, int type));
PXV_FUNC(int, find_remaining_action, (int imi));
PXV_FUNC(int, find_next_available_action, (void));
PXV_FUNC(int, is_there_pending_action_of_type, (int type));
PXV_FUNC(int, find_action_to_be_done_by_type, (int type));

// request definition


# define SET_ROT_REQUEST             1
# define SET_ZMAG_REQUEST            2
# define READ_ROT_REQUEST            3
# define READ_ZMAG_REQUEST           4
# define SET_ZMAG_REF_REQUEST        5
# define SET_ROT_REF_REQUEST         6
# define READ_TEMPERATURE_REQUEST    7
# define READ_FOCUS_REQUEST          8
# define SET_FOCUS_REQUEST           9
# define RINCE_SAMPLE               10
# define INJECT_ENZYME              11
# define INJECT_TWO_ENZYMES         12


// third group is errors state
# define PICO_COM_ERROR      0x01000000
# define ZMAG_TOP_LIMIT      0x02000000
# define ZMAG_BOTTOM_LIMIT   0x02000000
# define ZMAG_VCAP_LIMIT     0x04000000
# define PIFOC_OVERFLOW      0x08000000
# define PIFOC_OUT_OF_RANGE  0x10000000

// Phase state
# define PENDING             0
# define LAUNCHING_ACTION    1    // corresponds to the start of the action initiated by a command
# define RUNNING_ACTION      2    // action will last for a while: motors running towards target
# define ENDING_ACTION       4    // some extra time is provide to take care of overshoot
# define CHECKING_ACTION     8    // final measure to check that motors reached their target position
# define ABNORMAL_END       16    // the action last too much and was stopped since duration exceeded security time
# define ENDED              32



PXV_FUNC(int, rs232_register, (int type, int image_n, float val));
PXV_FUNC(int, what_is_pending_request, (int *type, int *image_n, float *val));


// this describe real time action to perform like moving objective, magnets etc
typedef struct future_action
{
  int imi;                           // the image nb where to act, if < 0 the next possible frame
  int imi0;                           // the original image to start
  int proceeded;                     //  1 if treated, 0 if pending
  int type;
  float value;
  int period;                       // if > 0 means a periodic event
  int spare;                       // a spare int data for user
  int phase;                       // state, 0 waiting to start, 1 action started,
                                   // 2 waiting action finish, 3 post-waiting to take care of overshoot
  int started_indeed;              // the frame where action actually started
  float action_duration;           // in seconds
  int im_action_duration;          // in frames
  int check_duration;
  int maximum_duration;            // security timeout
  int check_latency;               // the number of images between two position read
  int nb_check_read;               // the number of time position has been read to check motion end since strated
  int message_sent;                // flag to know if a message was sent
  float initial_pos_mm;
  float initial_micro_liter;
  float pos_mm;
  float micro_liter;
} f_action;


# ifndef _ACTION_C_

# else
f_action *action_pending = NULL;
int m_action = 0;
int n_action = 0;

# endif
PXV_VAR(f_action*, action_pending);
PXV_VAR(int, m_action);
PXV_VAR(int, n_action);

# endif
