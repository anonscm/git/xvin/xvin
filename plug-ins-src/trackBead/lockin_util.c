#ifndef _LOCKIN_UTIL_C_
#define _LOCKIN_UTIL_C_



# include "allegro.h"
#ifdef XV_WIN32
# include "winalleg.h"
#endif
# include "ctype.h"
# include "xvin.h"
# include "fftl32n.h"
# include "fillib.h"
# include "fftbtl32n.h"
# include "fillibbt.h"



/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "../cfg_file/Pico_cfg.h"
# include "../phaseindex/phaseindex.h"
#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)

# include "PlayItSam.h"
# include "brown_util.h"
# include "lockin_util.h"
# include "action_live_video.h"
//# include "focus.h"

MENU bead_active_menu[32];

/** this function initialize the tracking info structure

  This function is started as soon as the video acquisition is launch so that we can
  check the timing

  \param none
  \return A tracking info structure.
  \version 19/05/06
  */
MENU bead_menu[32];


int vco_phase_pid(long long t,      // the time stamp to determine phase
                  float error,      // error signal
                  int mode,         // mode = 0, return P and I, else change their value
                  float *P,         // proportionnal 
                  float *I,         // integralal
                  int *per,         // if > 0 define the new period of the vco else
                  // return perl the present vco period
                  int *n_cycle,     // the number of cycles since started
                  int *cycl_ph      // the position inside cycle [0 to perl]
                 );

void phase_timer(void)
{
    int ci, phi1, phi2;

    ci = ph_lock.c_i;
    ci++;
    if (ci >= ph_lock.n_i) ci = 0;
    phi1 = ph_lock.phase2[ci] = do_read_db25();  
    ph_lock.imt[ci] = my_ulclock(); 
    phi2 = do_read_db25();  
    if (ph_lock.ac_i == 0) 
    {
        ph_lock.phase2[0] = phi1;
        ph_lock.imt[0] = ph_lock.imt[1];
        ph_lock.imt0 = ph_lock.imt[ci];
    }
    if (phi1 == phi2)
    {
        ph_lock.c_i = ci;
        ph_lock.ac_i++;
    }

}
END_OF_FUNCTION(phase_timer)

int live_video_oi_got_mouse(struct  one_image *oi, int xm_s, int ym_s, int mode)
{
    /* this routine switches the display flag so that the magic color is drawn again
       after menu are activated */
    do_refresh_overlay = 1;
    return 0;
}
int live_video_oi_post_display(struct one_image *oi, DIALOG *d)
{
    imreg *imr;

    if (d == NULL || d->dp == NULL || d->proc == NULL)    return 1;

    imr = (imreg*)d->dp;
    show_bead_cross(plt_buffer, imr, d);
    return 0;
}


int change_cross_avg_width(void)
{
    int i, bcw;
    b_track *bt = NULL;

    if (mouse_selected_bead < 0 || mouse_selected_bead >= lockin_info->n_b)    return D_O_K;        
    bt = lockin_info->bd[mouse_selected_bead];
    bcw = bt->cw;
    i = win_scanf("Define cross averaging depth %6d\n",&bcw);
    if (i == CANCEL) return D_O_K;
    bt->ncw = bcw;
    return D_O_K;
}


int change_cross_size(void)
{
    int i, bcl;
    b_track *bt = NULL;

    if (mouse_selected_bead < 0 || mouse_selected_bead >= lockin_info->n_b)    return D_O_K;        
    bt = lockin_info->bd[mouse_selected_bead];
    bcl = bt->cl;
    if (bt->calib_im != NULL)
        return win_printf_OK("You must first remove the \nassociated calibration image!");
    i = win_scanf("Define cross size %6d\n",&bcl);
    if (i == CANCEL) return D_O_K;
    bt->ncl = bcl;
    return D_O_K;
}

int change_cross_angle(void)
{
    b_track *bt = NULL;

    if (mouse_selected_bead < 0 || mouse_selected_bead >= lockin_info->n_b)    return D_O_K;        
    bt = lockin_info->bd[mouse_selected_bead];
    bt->cross_45 = (bt->cross_45) ? 0 : 1;
    //win_printf("cross angle %d",bt->cross_45);
    return D_O_K;
}

int fix_bead(void)
{
    b_track *bt = NULL;
    if (mouse_selected_bead < 0 || mouse_selected_bead >= lockin_info->n_b)    return D_O_K; 
    bt = lockin_info->bd[mouse_selected_bead];
    bt->fixed_bead = (bt->fixed_bead) ? 0 : 1;
    return D_O_K;  
}



MENU *prepare_calib_menu(b_track *bt)
{
    register int i, j;
    i = 0;
    char ch[256];

    if (bt != NULL)
    {
        bead_menu[i].text = "Change averaging width";
        bead_menu[i].proc = change_cross_avg_width;
        bead_menu[i].child = NULL;
        bead_menu[i].flags = 0;
        bead_menu[i++].dp = NULL;

        bead_menu[i].text = "Change cross size";
        bead_menu[i].proc = change_cross_size;
        bead_menu[i].child = NULL;
        bead_menu[i].flags = 0;
        bead_menu[i++].dp = NULL;

        bead_menu[i].text = "Turn cross by 45 degrees";
        bead_menu[i].proc = change_cross_angle;
        bead_menu[i].child = NULL;
        bead_menu[i].flags = 0;
        bead_menu[i++].dp = NULL;

        bead_menu[i].text = "Fix bead";
        bead_menu[i].proc = fix_bead;
        bead_menu[i].child = NULL;
        bead_menu[i].flags = 0;
        bead_menu[i++].dp = NULL;




        bead_menu[i].text = "\0";
        bead_menu[i].proc = NULL;
        bead_menu[i].child = NULL;
        bead_menu[i].flags = 0;
        bead_menu[i++].dp = NULL;  

    }
    else
    {
        bead_menu[i].text = "New bead";
        bead_menu[i].proc = follow_bead_in_x_y;
        bead_menu[i].child = NULL;
        bead_menu[i].flags = MENU_INDEX(0);
        bead_menu[i++].dp = NULL;
    }
    bead_menu[i].text = NULL;  
    bead_menu[i].proc = NULL;

    //do_bead_menu(0);
    return bead_menu;
}



int live_video_oi_mouse_action(O_i *oi, int x0, int y0, int mode, DIALOG *d)
{
    // we put in this routine all the screen display stuff
    imreg *imr;
    BITMAP *imb;
    unsigned long t0 = 0;
    int cl, cw, pxl, pyl, c_b;
    b_track *bt = NULL;
    int (*post_display)(struct one_image *oi, DIALOG *d);
    //  display_title_message("cou cou");
    //return D_O_K;
    if (d->dp == NULL)    return 1;
    imr = (imreg*)d->dp;        /* the image region is here */
    if (imr->one_i == NULL || imr->n_oi == 0)        return 1;

    if(mouse_b == 2)
    {
        //for (lockin_info->c_b = 0; lockin_info->c_b < lockin_info->n_b; lockin_info->c_b++)
        for (c_b = 0; c_b < lockin_info->n_b; c_b++)
        {
            cl = lockin_info->cl;                   // cross arm length	
            cw = lockin_info->cw;                   // cross arm width	
            bt = lockin_info->bd[c_b];
            cl = bt->cl;
            cw = bt->cw;

            pxl = (int)(0.5+x_imr_2_imdata(imr, mouse_x));    
            pyl = (int)(0.5+y_imr_2_imdata(imr, mouse_y));    

            if ((pxl > bt->xc - cw)
                && (pxl < bt->xc + cw)
                && (pyl > bt->yc - cw)
                && (pyl < bt->yc + cw))
            {
                mouse_selected_bead = c_b;
                break;
            }
        }
        if (c_b < lockin_info->n_b)
            do_menu(prepare_calib_menu(bt),mouse_x,mouse_y);
        else do_menu(prepare_calib_menu(NULL),mouse_x,mouse_y);
    }

    while(mouse_b == 1)
    {
        if (oi->need_to_refresh & BITMAP_NEED_REFRESH)
        {
            t0 = my_uclock();
            post_display = oi->oi_post_display;
            oi->oi_post_display = NULL;
            display_image_stuff_16M(imr,d);
            oi->need_to_refresh |= INTERNAL_BITMAP_NEED_REFRESH;
            t0 = my_uclock() - t0;
            write_and_blit_im_box( plt_buffer, d, imr);
            oi->oi_post_display = post_display; 

            imb = (BITMAP*)oi->bmp.stuff;
            //show_bead_cross(screen, imr, d);
            move_bead_cross(screen, imr, d, mouse_x, mouse_y);
        }
        if (general_idle_action) general_idle_action(d);
    }
    for (c_b = 0; c_b < lockin_info->n_b; c_b++)
        lockin_info->bd[c_b]->mouse_draged = 0;

    return 0;
}

int	x_imdata_2_imr_nl(imreg *imr, int x, DIALOG *d)
{
    int pw;
    if (imr == NULL || imr->one_i == NULL)	return -xvin_error(Wrong_Argument);
    pw = imr->one_i->im.nxe - imr->one_i->im.nxs;
    if (imr->s_nx == 0 || pw == 0) return 0;
    return (imr->x_off + d->x + ((x - imr->one_i->im.nxs) * imr->s_nx)/pw);
}
int	y_imdata_2_imr_nl(imreg *imr, int y, DIALOG *d)
{
    int ph;
    if (imr == NULL || imr->one_i == NULL)	return -xvin_error(Wrong_Argument);
    ph = imr->one_i->im.nye - imr->one_i->im.nys;		
    if (imr->s_ny == 0 || ph == 0) return 0;	
    return (d->h - imr->y_off  + ((y - imr->one_i->im.nys) * imr->s_ny)/ph); // + 
}
float	y_imr_2_imdata_hiden(imreg *imr, int y, DIALOG *d)
{
    int ph;

    if (imr == NULL || imr->one_i == NULL)	  return 0;
    ph = imr->one_i->im.nye - imr->one_i->im.nys;
    if (imr->s_ny == 0 || ph == 0) return 0;
    return((float)imr->one_i->im.nys + ((float)((imr->y_off - y+d->y)*ph)/imr->s_ny));
}

float	x_imr_2_imdata_hiden(imreg *imr, int x, DIALOG *d)
{
    int pw;

    if (imr == NULL || imr->one_i == NULL)	  return 0;
    pw  = imr->one_i->im.nxe - imr->one_i->im.nxs;
    if (imr->s_nx == 0 || pw == 0) return 0;
    return((float)imr->one_i->im.nxs + ((float)((x - d->x - imr->x_off)*pw)/imr->s_nx));

}


int move_bead_cross(BITMAP *imb, imreg *imr, DIALOG *d, int x0, int y0)
{
    int cl, cw, pxl, pyl, xb, yb, done = 0;
    b_track *bt = NULL;
    int red = makecol(255,0,0), zcolor;
    int green = makecol(0,255,0);
    int blue = makecol(0,0,255);
    int in_image = 0, c_b;
    char zpos[256], *zp = NULL;

    if (lockin_info == NULL || imb == NULL || imr == NULL) return 0;
    if (lockin_info->n_b < 1) return 0;

    for (c_b = done = 0; c_b < lockin_info->n_b; c_b++)
    {
        cl = lockin_info->cl;                   // cross arm length	
        cw = lockin_info->cw;                   // cross arm width	
        bt = lockin_info->bd[c_b];
        cw = bt->cw;                           // cross arm width	
        cl = (bt->cl > 4) ? bt->cl : cl;
        if (bt->z_track == 2) 
        {
            snprintf(zpos,256,"%6.3f",lockin_info->focus_cor*bt->z_avgd);
            zp = zpos;
            zcolor = ((fabs(bt->min_phi) < 1.2) && (bt->min_xi2 < 0.2)) ? green : red;
        }
        else if (bt->z_track == 1) 
        {
            snprintf(zpos,256,"G0 %6.3f",lockin_info->focus_cor*bt->z_avgd);
            zp = zpos;
            zcolor = ((fabs(bt->min_phi) < 1.2) && (bt->min_xi2 < 0.2)) ? blue : red;
        }
        else
        {
            snprintf(zpos,256,"P. %6.3f",lockin_info->focus_cor*bt->z_avgd);
            zp = zpos;
            zcolor = red;
        }
        pxl = (int)(0.5+x_imr_2_imdata(imr, x0));    
        pyl = (int)(0.5+y_imr_2_imdata(imr, y0));    

        if (bt->mouse_draged == 0
            && (pxl > bt->xc - cw)
            && (pxl < bt->xc + cw)
            && (pyl > bt->yc - cw)
            && (pyl < bt->yc + cw))
        {
            bt->mouse_draged = 1;
            if (mouse_b != 0) done = 1;
        }
        if (bt->mouse_draged && mouse_b == 1)   
        {
            if (done == 0)
            {
                bt->mx = bt->xc = bt->x0 = pxl;
                bt->my = bt->yc = bt->y0 = pyl;
            }
            done = 1;
        }
        if (mouse_b == 0)   bt->mouse_draged = 0;
        xb = x_imdata_2_imr_nl(imr, bt->xc, d);
        yb = y_imdata_2_imr_nl(imr, bt->yc, d);
        in_image = is_cross_in_image(imr->one_i, xb, yb);
        if (in_image && bt->not_lost > 0 && bt->mouse_draged == 0) 
        {
            if (bt->cross_45)
                draw_X_cross_in_screen_unit(xb, yb-3, cl, cw, green, imb, imr->screen_scale,c_b,bt->fixed_bead,zp,zcolor);

            else draw_cross_in_screen_unit(xb, yb-3, cl, cw, green, imb, imr->screen_scale,c_b,bt->fixed_bead,zp,zcolor);
        }
        else 
        {
            if (bt->cross_45)
                draw_X_cross_in_screen_unit(xb, yb-3, cl, cw, red, imb, imr->screen_scale,c_b,bt->fixed_bead,zp,zcolor); 
            else draw_cross_in_screen_unit(xb, yb-3, cl, cw, red, imb, imr->screen_scale,c_b,bt->fixed_bead,zp,zcolor); 
        }
        /*
           if (bt->orthoradial_prof_size > 0)
           {
           draw_circle_in_screen_unit(xb, yb - 3,
           bt->orthoradial_mean_radius - bt->orthoradial_half_avg_size, 
           green, imb, imr->screen_scale);
           draw_circle_in_screen_unit(xb, yb - 3,
           bt->orthoradial_mean_radius + bt->orthoradial_half_avg_size, 
           green, imb, imr->screen_scale);
           }
           */
    }
    return 0;
}


int show_bead_cross(BITMAP *imb, imreg *imr, DIALOG *d)
{
    int cl, cw, pxl, pyl, redraw = 0, c_b, xoff = 0, yoff = 0;
    b_track *bt = NULL;
    int red = makecol(255,0,0);
    int green = makecol(0,255,0);
    int blue = makecol(0,0,255), zcolor = 0;
    char zpos[256], *zp = NULL;

    if (lockin_info == NULL || imb == NULL || imr == NULL) return 0;
    if (lockin_info->n_b < 1) return 0;


    if (imb != (BITMAP*)imr->one_i->bmp.stuff)
    {
        xoff = imr->x_off;
        yoff = imr->y_off - imb->h + d->y;
    }
    for (c_b = 0; c_b < lockin_info->n_b; c_b++)
    {
        cl = lockin_info->cl;                   // cross arm length	
        cw = lockin_info->cw;                   // cross arm width	
        bt = lockin_info->bd[c_b];
        cw = bt->cw;                   // cross arm width	
        cl = (bt->cl > 4) ? bt->cl : cl;
        if (bt->z_track == 2)
        {
            snprintf(zpos,256,"%6.3f",lockin_info->focus_cor*bt->z_avgd);
            zp = zpos;
            zcolor = ((fabs(bt->min_phi) < 1.2) && (bt->min_xi2 < 0.2)) ? green : red;
        }
        else if (bt->z_track == 1)
        {
            snprintf(zpos,256,"G0 %6.3f",lockin_info->focus_cor*bt->z_avgd);
            zp = zpos;
            zcolor = ((fabs(bt->min_phi) < 1.2) && (bt->min_xi2 < 0.2)) ? blue : red;
        }
        else 
        {
            snprintf(zpos,256,"P. %6.3f",lockin_info->focus_cor*bt->z_avgd);
            zp = zpos;
            zcolor = red;
        }
        if (bt->not_lost > 0) //== NB_FRAMES_BEF_LOST)
        {
            if (bt->cross_45)
                draw_X_cross_in_screen_unit(bt->xc+xoff, bt->yc-yoff, cl, cw, green, imb, imr->screen_scale,c_b,
                                            bt->fixed_bead,zp,zcolor);
            else draw_cross_in_screen_unit(bt->xc+xoff, bt->yc-yoff, cl, cw, green, imb, imr->screen_scale,c_b,
                                           bt->fixed_bead,zp,zcolor);

            //my_set_window_title("nb of lost beads %d at %d %d",lockin_info->n_b,bt->xc+xoff, bt->yc-yoff);
        }
        else
        {
            pxl = (int)(0.5+x_imr_2_imdata_hiden(imr, mouse_x, d));    
            pyl = (int)(0.5+y_imr_2_imdata_hiden(imr, mouse_y, d));    
            if ((mouse_b & 0x1) 
                && (pxl > bt->xc - cw)
                && (pxl < bt->xc + cw)
                && (pyl > bt->yc - cw)
                && (pyl < bt->yc + cw))

            {
                bt->mouse_draged = 1;
            }
            else 
            {
                if (mouse_b == 0)		  bt->mouse_draged = 0;
            }
            if (bt->cross_45)
                draw_X_cross_in_screen_unit(bt->xc+xoff, bt->yc-yoff, cl, cw, red, imb, imr->screen_scale,c_b,bt->fixed_bead, zp,zcolor);
            else draw_cross_in_screen_unit(bt->xc+xoff, bt->yc-yoff, cl, cw, red, imb, imr->screen_scale,c_b,bt->fixed_bead, zp,zcolor);
            //my_set_window_title("nb of beads %d",lockin_info->n_b);
        }
        if  (bt->mouse_draged) redraw = 1;
        /*
           if (bt->orthoradial_prof_size > 0)
           {
           draw_circle_in_screen_unit(bt->xc+xoff, bt->yc-yoff,
           bt->orthoradial_mean_radius - bt->orthoradial_half_avg_size, 
           green, imb, imr->screen_scale);
           draw_circle_in_screen_unit(bt->xc+xoff, bt->yc-yoff,
           bt->orthoradial_mean_radius + bt->orthoradial_half_avg_size, 
           green, imb, imr->screen_scale);

           }
           */
    }
    return redraw;
}

int z_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
    register int i, j, k;
    static int last_c_i = -1, immax = -1;
    d_s *dsx, *dsy;// *dsz;
    int nf, last_fr = 0, lost_fr;
    unsigned long  dtm, dtmax;
    float ax, dx, ay, dy, y_over_x, y_over_z;
    un_s *un;

    if (lockin_info == NULL) return D_O_K;
    un = oi_LIVE_VIDEO->xu[oi_LIVE_VIDEO->c_xu];
    get_afine_param_from_unit(un, &ax, &dx);
    un = oi_LIVE_VIDEO->yu[oi_LIVE_VIDEO->c_yu];
    get_afine_param_from_unit(un, &ay, &dy);
    y_over_x = (dy != 0) ? dx/dy : 1;
    y_over_z = lockin_info->focus_cor * 1e-6;
    y_over_z = (dy != 0) ? y_over_z/dy : 1;

    dsx = find_source_specific_ds_in_op(op,"X rolling buffer");
    dsy = find_source_specific_ds_in_op(op,"Y rolling buffer");
    //dsz = find_source_specific_ds_in_op(op,"Z rolling buffer");
    //if (dsx == NULL || dsy == NULL || dsz == NULL) return D_O_K;
    if (dsx == NULL || dsy == NULL) return D_O_K;		
    /*
       if (sscanf(dsx->source,"X rolling buffer bead %d",&bead_num) != 1)
       return win_printf_OK("Cannot find bead number");
       */
    if (lockin_info->n_b < 1)    return D_O_K;	//win_printf_OK("No bead to display");
    i = lockin_info->c_i;
    if (lockin_info->imi[i] <= immax) return D_O_K;   // we are uptodate
    immax = lockin_info->imi[i];               
    nf = (lockin_info->ac_i > LOCKIN_BUFFER_SIZE) ? LOCKIN_BUFFER_SIZE : lockin_info->ac_i;
    nf = (nf > (LOCKIN_BUFFER_SIZE>>1)) ? LOCKIN_BUFFER_SIZE>>1 : nf;
    //dsx->nx = dsx->ny = dsy->nx = dsy->ny = dsz->nx = dsz->ny = nf;
    dsx->nx = dsx->ny = dsy->nx = dsy->ny = nf;
    last_c_i = (lockin_info->c_i > 0) ? lockin_info->c_i - 1 : 0;  // c_i is not yet valid
    for (i = nf-1, j = last_c_i, lost_fr = 0, dtm = dtmax = 0, k = 0; i >= 0; i--, j--, k++)
    {
        j = (j < 0) ? LOCKIN_BUFFER_SIZE + j: j;
        j = (j < LOCKIN_BUFFER_SIZE) ? j : LOCKIN_BUFFER_SIZE;
        //dsx->xd[i] = dsy->xd[i] = dsz->xd[i] = lockin_info->imi[j];
        dsx->xd[i] = dsy->xd[i] = lockin_info->imi[j];
        dsx->yd[i] = y_over_x * lockin_info->bd[lockin_info->n_bead_display]->x[j];
        dsy->yd[i] = lockin_info->bd[lockin_info->n_bead_display]->y[j];
        //dsz->yd[i] = y_over_z * lockin_info->bd[lockin_info->n_bead_display]->z[j];
        if (i < nf-1) lost_fr += (last_fr - lockin_info->imi[j]) - 1;
        last_fr = lockin_info->imi[j];
        dtm += lockin_info->imdt[j];
        dtmax = (lockin_info->imdt[j] > dtmax) ? lockin_info->imdt[j] : dtmax;
    }
    op->need_to_refresh = 1; 
    if (k) set_plot_title(op, "\\stack{{Bead %d, %d missed images}{\\pt8 dt mean %6.3f ms max %6.3f}}",
                          lockin_info->n_bead_display, lost_fr,1000*((double)(dtm/k))/get_my_uclocks_per_sec(),
                          1000*((double)(dtmax))/get_my_uclocks_per_sec());
    i = LOCKIN_BUFFER_SIZE/8;
    //i = 512;
    j = ((int)dsx->xd[i])/i;
    op->x_lo = (j-1)*i;
    op->x_hi = (j+4)*i;
    set_plot_x_fixed_range(op);
    op->user_ispare[ISPARE_BEAD_NB] = lockin_info->n_bead_display;
    return refresh_plot(pr_LIVE_VIDEO, UNCHANGED);
}

int x_profile_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
    register int i;
    static int last_c_i = -1, immax = -1, tmpi;
    d_s *ds, *dsr; //
    b_track *bd;


    if (lockin_info == NULL) return D_O_K;

    ds = find_source_specific_ds_in_op(op,"Instantaneous x profile");
    dsr = find_source_specific_ds_in_op(op,"Reference x profile");
    if (ds == NULL || dsr == NULL) return D_O_K;	


    if (lockin_info->n_b < 1)    return D_O_K;	//win_printf_OK("No bead to display");
    i = lockin_info->c_i;
    if (lockin_info->imi[i] <= immax) return D_O_K;   // we are uptodate
    immax = lockin_info->imi[i];               
    bd = lockin_info->bd[lockin_info->n_bead_display];
    last_c_i = lockin_info->c_i - 1;
    last_c_i = (last_c_i < 0) ? 4095 : last_c_i;

    for (i = 0, tmpi = 0; i < bd->cl && i < ds->mx ; i++)
    {
        ds->xd[i] = dsr->xd[i] = bd->cl/2 - i;
        tmpi += bd->xi[i]; 
        ds->yd[i] = bd->xi[i]; 
        dsr->yd[i] = bd->xcor[i];
    }
    if (bd->cl) tmpi /= 2*bd->cl;
    for (i = 0; i < bd->cl && i < ds->mx ; i++)
    {
        dsr->yd[i] = tmpi*bd->xcor[i];
    }
    ds->nx = ds->ny = i;
    dsr->nx = dsr->ny = i;


    op->need_to_refresh = 1; 
    set_plot_title(op, "X profile plot");
    return refresh_plot(pr_LIVE_VIDEO, UNCHANGED);
}


int integrate_intensity_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
    register int i, j, k;
    static int last_c_i = -1, immax = -1;
    d_s *dsx;
    int nf;
    b_track *bd;

    if (lockin_info == NULL) return D_O_K;
    dsx = find_source_specific_ds_in_op(op,"integrate intensity");
    if (dsx == NULL) return D_O_K;
    // dsy = find_source_specific_ds_in_op(op,"ac begin");
    //if (dsy == NULL) return D_O_K;	

    i = lockin_info->c_i;
    if (lockin_info->imi[i] <= immax) return D_O_K;   // we are uptodate
    immax = lockin_info->imi[i];   
    for (k = 0; k < lockin_info->n_b; k++)
    {
        if (k >= op->n_dat)
            create_and_attach_one_ds(op, LOCKIN_BUFFER_SIZE, LOCKIN_BUFFER_SIZE, 0);
        op->dat[k]->nx = op->dat[k]->ny = 0;
    }

    nf = (lockin_info->ac_i > LOCKIN_BUFFER_SIZE) ? LOCKIN_BUFFER_SIZE : lockin_info->ac_i;      
    nf = (nf > (LOCKIN_BUFFER_SIZE>>1)) ? LOCKIN_BUFFER_SIZE>>1 : nf;
    //dsy->nx = dsy->ny = nf;
    bd = lockin_info->bd[lockin_info->n_bead_display];

    last_c_i = (lockin_info->c_i > 0) ? lockin_info->c_i - 1 : 0;  // c_i is not yet valid
    for (i = nf-1, j = last_c_i; i >= 0; i--, j--)
    {
        j = (j < 0) ? LOCKIN_BUFFER_SIZE + j: j;
        for (k = 0; k < lockin_info->n_b; k++)
        {
            op->dat[k]->yd[i] = lockin_info->bd[k]->amp[j];
            op->dat[k]->xd[i] = lockin_info->imi[j];
            op->dat[k]->nx = op->dat[k]->ny = nf;
        }
    }

    i = LOCKIN_BUFFER_SIZE/8;
    j = ((int)dsx->xd[i])/i;
    op->x_lo = (j-1)*i;
    op->x_hi = (j+4)*i;
    set_plot_x_fixed_range(op);

    op->need_to_refresh = 1; 

    set_plot_title(op, "\\stack{{bead %d}{image%d}}"
                   ,lockin_info->n_bead_display,lockin_info->ac_i);
    op->user_ispare[ISPARE_BEAD_NB] =lockin_info->n_bead_display;
    return refresh_plot(pr_LIVE_VIDEO, UNCHANGED);
}


/* int integrate_intensity_2phases_rolling_buffer_idle_action(O_p *op, DIALOG *d) */
/* { */
/*   register int i, j, k; */
/*   static int last_c_i = -1, immax = -1; */
/*   d_s *dsx; */
/*   int nf; */
/*   b_track *bd; */

/*   if (lockin_info == NULL) return D_O_K; */
/*   dsx = find_source_specific_ds_in_op(op,"Integrate intensity on 2 phases"); */
/*   if (dsx == NULL) return D_O_K; */
/*   // dsy = find_source_specific_ds_in_op(op,"ac begin"); */
/*   //if (dsy == NULL) return D_O_K;	 */

/*   i = lockin_info->c_i; */
/*   if (lockin_info->imi[i] <= immax) return D_O_K;   // we are uptodate */
/*   immax = lockin_info->imi[i];    */
/*   for (k = 0; k < lockin_info->n_b; k++) */
/*     { */
/*       if (k >= op->n_dat) */
/* 	create_and_attach_one_ds(op, LOCKIN_BUFFER_SIZE, LOCKIN_BUFFER_SIZE, 0); */
/*       op->dat[k]->nx = op->dat[k]->ny = 0; */
/*     } */

/*   nf = (lockin_info->ac_i > LOCKIN_BUFFER_SIZE) ? LOCKIN_BUFFER_SIZE : lockin_info->ac_i;       */
/*   nf = (nf > (LOCKIN_BUFFER_SIZE>>1)) ? LOCKIN_BUFFER_SIZE>>1 : nf; */
/*   //dsy->nx = dsy->ny = nf; */
/*   bd = lockin_info->bd[lockin_info->n_bead_display]; */

/*   last_c_i = (lockin_info->c_i > 0) ? lockin_info->c_i - 1 : 0;  // c_i is not yet valid */
/*   for (i = nf-1, j = last_c_i; i >= 0; i--, j--) */
/*     { */
/*       j = (j < 0) ? LOCKIN_BUFFER_SIZE + j: j; */
/*       for (k = 0; k < lockin_info->n_b; k++) */
/* 	{ */
/* 	  op->dat[k]->yd[i] = lockin_info->bd[k]->amp_2phase[j]; */
/* 	  op->dat[k]->xd[i] = lockin_info->imi[j]; */
/* 	  op->dat[k]->nx = op->dat[k]->ny = nf; */
/* 	} */
/*     } */

/*   i = LOCKIN_BUFFER_SIZE/8; */
/*   j = ((int)dsx->xd[i])/i; */
/*   op->x_lo = (j-1)*i; */
/*   op->x_hi = (j+4)*i; */
/*   set_plot_x_fixed_range(op); */

/*   op->need_to_refresh = 1;  */

/*   set_plot_title(op, "\\stack{{bead %d}{image%d}}" */
/*   		 ,lockin_info->n_bead_display,lockin_info->ac_i); */
/*   op->user_ispare[ISPARE_BEAD_NB] =lockin_info->n_bead_display; */
/*   return refresh_plot(pr_LIVE_VIDEO, UNCHANGED); */
/* } */


int integrate_intensity_async1_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
    register int i, j, k;
    static int last_c_i = -1, immax = -1;
    d_s *dsx;
    int nf;
    b_track *bd;

    if (lockin_info == NULL) return D_O_K;
    dsx = find_source_specific_ds_in_op(op,"Integrate intensity async1");
    if (dsx == NULL) return D_O_K;
    // dsy = find_source_specific_ds_in_op(op,"ac begin");
    //if (dsy == NULL) return D_O_K;	

    i = lockin_info->c_i;
    if (lockin_info->imi[i] <= immax) return D_O_K;   // we are uptodate
    immax = lockin_info->imi[i];   
    for (k = 0; k < lockin_info->n_b; k++)
    {
        if (k >= op->n_dat)
            create_and_attach_one_ds(op, LOCKIN_BUFFER_SIZE, LOCKIN_BUFFER_SIZE, 0);
        op->dat[k]->nx = op->dat[k]->ny = 0;
    }

    nf = (lockin_info->ac_i > LOCKIN_BUFFER_SIZE) ? LOCKIN_BUFFER_SIZE : lockin_info->ac_i;      
    nf = (nf > (LOCKIN_BUFFER_SIZE>>1)) ? LOCKIN_BUFFER_SIZE>>1 : nf;
    //dsy->nx = dsy->ny = nf;
    bd = lockin_info->bd[lockin_info->n_bead_display];

    last_c_i = (lockin_info->c_i > 0) ? lockin_info->c_i - 1 : 0;  // c_i is not yet valid
    for (i = nf-1, j = last_c_i; i >= 0; i--, j--)
    {
        j = (j < 0) ? LOCKIN_BUFFER_SIZE + j: j;
        for (k = 0; k < lockin_info->n_b; k++)
        {
            op->dat[k]->yd[i] = lockin_info->bd[k]->amp_async1[j];
            op->dat[k]->xd[i] = lockin_info->imi[j];
            op->dat[k]->nx = op->dat[k]->ny = nf;
        }
    }

    i = LOCKIN_BUFFER_SIZE/8;
    j = ((int)dsx->xd[i])/i;
    op->x_lo = (j-1)*i;
    op->x_hi = (j+4)*i;
    set_plot_x_fixed_range(op);

    op->need_to_refresh = 1; 

    set_plot_title(op, "\\stack{{bead %d}{image%d}}"
                   ,lockin_info->n_bead_display,lockin_info->ac_i);
    op->user_ispare[ISPARE_BEAD_NB] =lockin_info->n_bead_display;
    return refresh_plot(pr_LIVE_VIDEO, UNCHANGED);
}

int integrate_intensity_async2_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
    register int i, j, k;
    static int last_c_i = -1, immax = -1;
    d_s *dsx;
    int nf;
    b_track *bd;

    if (lockin_info == NULL) return D_O_K;
    dsx = find_source_specific_ds_in_op(op,"Integrate intensity async2");
    if (dsx == NULL) return D_O_K;
    // dsy = find_source_specific_ds_in_op(op,"ac begin");
    //if (dsy == NULL) return D_O_K;	

    i = lockin_info->c_i;
    if (lockin_info->imi[i] <= immax) return D_O_K;   // we are uptodate
    immax = lockin_info->imi[i];   
    for (k = 0; k < lockin_info->n_b; k++)
    {
        if (k >= op->n_dat)
            create_and_attach_one_ds(op, LOCKIN_BUFFER_SIZE, LOCKIN_BUFFER_SIZE, 0);
        op->dat[k]->nx = op->dat[k]->ny = 0;
    }

    nf = (lockin_info->ac_i > LOCKIN_BUFFER_SIZE) ? LOCKIN_BUFFER_SIZE : lockin_info->ac_i;      
    nf = (nf > (LOCKIN_BUFFER_SIZE>>1)) ? LOCKIN_BUFFER_SIZE>>1 : nf;
    //dsy->nx = dsy->ny = nf;
    bd = lockin_info->bd[lockin_info->n_bead_display];

    last_c_i = (lockin_info->c_i > 0) ? lockin_info->c_i - 1 : 0;  // c_i is not yet valid
    for (i = nf-1, j = last_c_i; i >= 0; i--, j--)
    {
        j = (j < 0) ? LOCKIN_BUFFER_SIZE + j: j;
        for (k = 0; k < lockin_info->n_b; k++)
        {
            op->dat[k]->yd[i] = lockin_info->bd[k]->amp_async2[j];
            op->dat[k]->xd[i] = lockin_info->imi[j];
            op->dat[k]->nx = op->dat[k]->ny = nf;
        }
    }

    i = LOCKIN_BUFFER_SIZE/8;
    j = ((int)dsx->xd[i])/i;
    op->x_lo = (j-1)*i;
    op->x_hi = (j+4)*i;
    set_plot_x_fixed_range(op);

    op->need_to_refresh = 1; 

    set_plot_title(op, "\\stack{{bead %d}{image%d}}"
                   ,lockin_info->n_bead_display,lockin_info->ac_i);
    op->user_ispare[ISPARE_BEAD_NB] =lockin_info->n_bead_display;
    return refresh_plot(pr_LIVE_VIDEO, UNCHANGED);
}
/* int integrate_intensity_2phases_mod_rolling_buffer_idle_action(O_p *op, DIALOG *d) */
/* { */
/*   register int i, j, k; */
/*   static int last_c_i = -1, immax = -1; */
/*   d_s *dsx; */
/*   int nf; */
/*   b_track *bd; */

/*   if (lockin_info == NULL) return D_O_K; */
/*   dsx = find_source_specific_ds_in_op(op,"Integrate intensity_mod on 2 phases"); */
/*   if (dsx == NULL) return D_O_K; */
/*   // dsy = find_source_specific_ds_in_op(op,"ac begin"); */
/*   //if (dsy == NULL) return D_O_K;	 */

/*   i = lockin_info->c_i; */
/*   if (lockin_info->imi[i] <= immax) return D_O_K;   // we are uptodate */
/*   immax = lockin_info->imi[i];    */
/*   for (k = 0; k < lockin_info->n_b; k++) */
/*     { */
/*       if (k >= op->n_dat) */
/* 	create_and_attach_one_ds(op, LOCKIN_BUFFER_SIZE, LOCKIN_BUFFER_SIZE, 0); */
/*       op->dat[k]->nx = op->dat[k]->ny = 0; */
/*     } */

/*   nf = (lockin_info->ac_i > LOCKIN_BUFFER_SIZE) ? LOCKIN_BUFFER_SIZE : lockin_info->ac_i;       */
/*   nf = (nf > (LOCKIN_BUFFER_SIZE>>1)) ? LOCKIN_BUFFER_SIZE>>1 : nf; */
/*   //dsy->nx = dsy->ny = nf; */
/*   bd = lockin_info->bd[lockin_info->n_bead_display]; */

/*   last_c_i = (lockin_info->c_i > 0) ? lockin_info->c_i - 1 : 0;  // c_i is not yet valid */
/*   for (i = nf-1, j = last_c_i; i >= 0; i--, j--) */
/*     { */
/*       j = (j < 0) ? LOCKIN_BUFFER_SIZE + j: j; */
/*       for (k = 0; k < lockin_info->n_b; k++) */
/* 	{ */
/* 	  op->dat[k]->yd[i] = lockin_info->bd[k]->amp_2phase_mod[j]; */
/* 	  op->dat[k]->xd[i] = lockin_info->imi[j]; */
/* 	  op->dat[k]->nx = op->dat[k]->ny = nf; */
/* 	} */
/*     } */

/*   i = LOCKIN_BUFFER_SIZE/8; */
/*   j = ((int)dsx->xd[i])/i; */
/*   op->x_lo = (j-1)*i; */
/*   op->x_hi = (j+4)*i; */
/*   set_plot_x_fixed_range(op); */

/*   op->need_to_refresh = 1;  */

/*   set_plot_title(op, "\\stack{{bead %d}{image%d}}" */
/*   		 ,lockin_info->n_bead_display,lockin_info->ac_i); */
/*   op->user_ispare[ISPARE_BEAD_NB] =lockin_info->n_bead_display; */
/*   return refresh_plot(pr_LIVE_VIDEO, UNCHANGED); */
/* } */

int integrate_intensity_on_real_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
    register int i, j, k;
    static int last_c_i = -1, immax = -1;
    d_s *dsx;
    int nf;
    b_track *bd;

    if (lockin_info == NULL) return D_O_K;
    dsx = find_source_specific_ds_in_op(op,"Integrate intensity real");
    if (dsx == NULL) return D_O_K;	

    i = lockin_info->c_i;
    if (lockin_info->imi[i] <= immax) return D_O_K;   // we are uptodate
    immax = lockin_info->imi[i];   
    for (k = 0; k < lockin_info->n_b; k++)
    {
        if (k >= op->n_dat)
            create_and_attach_one_ds(op, LOCKIN_BUFFER_SIZE, LOCKIN_BUFFER_SIZE, 0);
        op->dat[k]->nx = op->dat[k]->ny = 0;
    }

    nf = (lockin_info->ac_i > LOCKIN_BUFFER_SIZE) ? LOCKIN_BUFFER_SIZE : lockin_info->ac_i;      
    nf = (nf > (LOCKIN_BUFFER_SIZE>>1)) ? LOCKIN_BUFFER_SIZE>>1 : nf;
    //dsx->nx = dsx->ny = nf;
    bd = lockin_info->bd[lockin_info->n_bead_display];

    last_c_i = (lockin_info->c_i > 0) ? lockin_info->c_i - 1 : 0;  // c_i is not yet valid
    for (i = nf-1, j = last_c_i; i >= 0; i--, j--)
    {
        j = (j < 0) ? LOCKIN_BUFFER_SIZE + j: j;
        for (k = 0; k < lockin_info->n_b; k++)
        {
            op->dat[k]->yd[i] = lockin_info->bd[k]->ampds_rel[j];
            op->dat[k]->xd[i] = lockin_info->imi[j];
            op->dat[k]->nx = op->dat[k]->ny = nf;
        }
    }

    i = LOCKIN_BUFFER_SIZE/8;
    j = ((int)dsx->xd[i])/i;
    op->x_lo = (j-1)*i;
    op->x_hi = (j+4)*i;
    set_plot_x_fixed_range(op);

    op->need_to_refresh = 1; 

    set_plot_title(op, "\\stack{{bead %d}{image%d}}"
                   ,lockin_info->n_bead_display,lockin_info->ac_i);
    op->user_ispare[ISPARE_BEAD_NB] =lockin_info->n_bead_display;
    return refresh_plot(pr_LIVE_VIDEO, UNCHANGED);
}

int integrate_intensity_on_im_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
    register int i, j, k;
    static int last_c_i = -1, immax = -1;
    d_s *dsx;
    int nf;
    b_track *bd;

    if (lockin_info == NULL) return D_O_K;
    dsx = find_source_specific_ds_in_op(op,"Integrate intensity im");
    if (dsx == NULL) return D_O_K;	

    i = lockin_info->c_i;
    if (lockin_info->imi[i] <= immax) return D_O_K;   // we are uptodate
    immax = lockin_info->imi[i];   
    for (k = 0; k < lockin_info->n_b; k++)
    {
        if (k >= op->n_dat)
            create_and_attach_one_ds(op, LOCKIN_BUFFER_SIZE, LOCKIN_BUFFER_SIZE, 0);
        op->dat[k]->nx = op->dat[k]->ny = 0;
    }

    nf = (lockin_info->ac_i > LOCKIN_BUFFER_SIZE) ? LOCKIN_BUFFER_SIZE : lockin_info->ac_i;      
    nf = (nf > (LOCKIN_BUFFER_SIZE>>1)) ? LOCKIN_BUFFER_SIZE>>1 : nf;
    //dsx->nx = dsx->ny = nf;
    bd = lockin_info->bd[lockin_info->n_bead_display];

    last_c_i = (lockin_info->c_i > 0) ? lockin_info->c_i - 1 : 0;  // c_i is not yet valid
    for (i = nf-1, j = last_c_i; i >= 0; i--, j--)
    {
        j = (j < 0) ? LOCKIN_BUFFER_SIZE + j: j;
        for (k = 0; k < lockin_info->n_b; k++)
        {
            op->dat[k]->yd[i] = lockin_info->bd[k]->ampds_im[j];
            op->dat[k]->xd[i] = lockin_info->imi[j];
            op->dat[k]->nx = op->dat[k]->ny = nf;
        }
    }

    i = LOCKIN_BUFFER_SIZE/8;
    j = ((int)dsx->xd[i])/i;
    op->x_lo = (j-1)*i;
    op->x_hi = (j+4)*i;
    set_plot_x_fixed_range(op);

    op->need_to_refresh = 1; 

    //set_plot_title(op, "\\stack{{bead %d}{image%d}}"
    // 		 ,lockin_info->n_bead_display,lockin_info->ac_i);
    op->user_ispare[ISPARE_BEAD_NB] =lockin_info->n_bead_display;
    return refresh_plot(pr_LIVE_VIDEO, UNCHANGED);
}

/* int integrate_intensity_on_mod_rolling_buffer_idle_action(O_p *op, DIALOG *d) */
/* { */
/*   register int i, j, k; */
/*   static int last_c_i = -1, immax = -1; */
/*   d_s *dsx; */
/*   int nf; */
/*   b_track *bd; */

/*   if (lockin_info == NULL) return D_O_K; */
/*   dsx = find_source_specific_ds_in_op(op,"Integrate intensity mod"); */
/*   if (dsx == NULL) return D_O_K;	 */

/*   i = lockin_info->c_i; */
/*   if (lockin_info->imi[i] <= immax) return D_O_K;   // we are uptodate */
/*   immax = lockin_info->imi[i];    */
/*   for (k = 0; k < lockin_info->n_b; k++) */
/*     { */
/*       if (k >= op->n_dat) */
/* 	create_and_attach_one_ds(op, LOCKIN_BUFFER_SIZE, LOCKIN_BUFFER_SIZE, 0); */
/*       op->dat[k]->nx = op->dat[k]->ny = 0; */
/*     } */

/*   nf = (lockin_info->ac_i > LOCKIN_BUFFER_SIZE) ? LOCKIN_BUFFER_SIZE : lockin_info->ac_i;       */
/*   nf = (nf > (LOCKIN_BUFFER_SIZE>>1)) ? LOCKIN_BUFFER_SIZE>>1 : nf; */
/*   //dsx->nx = dsx->ny = nf; */
/*   bd = lockin_info->bd[lockin_info->n_bead_display]; */

/*   last_c_i = (lockin_info->c_i > 0) ? lockin_info->c_i - 1 : 0;  // c_i is not yet valid */
/*   for (i = nf-1, j = last_c_i; i >= 0; i--, j--) */
/*     { */
/*       j = (j < 0) ? LOCKIN_BUFFER_SIZE + j: j; */
/*       for (k = 0; k < lockin_info->n_b; k++) */
/* 	{ */
/* 	  op->dat[k]->yd[i] = lockin_info->bd[k]->ampds_mod[j]; */
/* 	  op->dat[k]->xd[i] = lockin_info->imi[j]; */
/* 	  op->dat[k]->nx = op->dat[k]->ny = nf; */
/* 	} */
/*     } */

/*   i = LOCKIN_BUFFER_SIZE/8; */
/*   j = ((int)dsx->xd[i])/i; */
/*   op->x_lo = (j-1)*i; */
/*   op->x_hi = (j+4)*i; */
/*   set_plot_x_fixed_range(op); */

/*   op->need_to_refresh = 1;  */

/*   //set_plot_title(op, "\\stack{{bead %d}{image%d}}" */
/*   // 		 ,lockin_info->n_bead_display,lockin_info->ac_i); */
/*   op->user_ispare[ISPARE_BEAD_NB] =lockin_info->n_bead_display; */
/*   return refresh_plot(pr_LIVE_VIDEO, UNCHANGED); */
/* } */


int general_op_post_display(struct one_plot *op, DIALOG *d)
{
    register int i, j;
    BITMAP *imb;
    d_s *ds;
    int  nb, xc, yc;

    if (d == NULL || d->dp == NULL || d->proc == NULL)    return 1;
    if (imr_and_pr) return 0;
    ds = op->dat[0];
    if (ds == NULL || ds->source == NULL)       return 0;
    //  if (sscanf(ds->source,"Bead tracking at %f Hz Acquistion %d for bead %d"
    //     ,&f,&na,&nb) != 3) return 0;
    nb = op->user_ispare[ISPARE_BEAD_NB];

    if (nb >= 0 && nb  < lockin_info->n_b)
    {
        imb = (BITMAP*)oi_LIVE_VIDEO->bmp.stuff;
        if (oi_LIVE_VIDEO->need_to_refresh & BITMAP_NEED_REFRESH)
        {
            display_image_stuff_16M(imr_LIVE_VIDEO,d_LIVE_VIDEO);
            oi_LIVE_VIDEO->need_to_refresh |= INTERNAL_BITMAP_NEED_REFRESH;
            show_bead_cross(imb, imr_LIVE_VIDEO, d_LIVE_VIDEO);
        }
        for(i = nb - 1, j = 0; (i < nb + 2) && (j < lockin_info->n_b) && (j < 3); i++, j++) 
        {
            xc = lockin_info->bd[(i+lockin_info->n_b)%lockin_info->n_b]->x0 - 100;
            yc = oi_LIVE_VIDEO->im.ny - lockin_info->bd[(i+lockin_info->n_b)%lockin_info->n_b]->y0 - 100;
            acquire_bitmap(plt_buffer);
            blit(imb,plt_buffer,xc,yc,SCREEN_W - 300, 60 + 256*j, 200, 200); 
            release_bitmap(plt_buffer);
        }
    }
    return 0;
}

int switch_bead_of_interest(void)
{
    int tmp = 0;

    if (active_menu->text == NULL) return D_O_K;	
    sscanf((char*)active_menu->text,"Bead %d",&tmp); // != 1) return D_O_K;	
    if(updating_menu_state != 0)	
    {
        if (tmp == lockin_info->n_bead_display)
            active_menu->flags |=  D_SELECTED;
        else active_menu->flags &= ~D_SELECTED;			
        return D_O_K;	
    }

    if (tmp >= lockin_info->n_b || tmp < 0) return D_O_K;	
    lockin_info->n_bead_display = tmp;    
    return D_O_K;
}

int prepare_bead_menu(void)
{
    char st[128];
    register int i;
    for (i = 0; i < lockin_info->n_b; i++)
    {
        if (bead_active_menu[i].text)
        {
            free(bead_active_menu[i].text);
            bead_active_menu[i].text = NULL;
        }
        snprintf(st,128,"Bead %d",i);
        bead_active_menu[i].text = strdup(st);
        bead_active_menu[i].proc = switch_bead_of_interest;

    }
    if (bead_active_menu[i].text)
        free(bead_active_menu[i].text);
    bead_active_menu[i].text = NULL;
    return 0;
}

int do_follow_bead_in_x_y(int mode)
{
    register int i, di;
    imreg *imrs;	
    O_p *op;
    d_s *ds;
    O_i *ois;
    int xc = 256, yc = 256, color,  dis_state, icl = 4;
    static int cl = 16, cw = 8;
    //, nf = 2048, imstart = 10, prof = 0
    b_track *bd = NULL;
    DIALOG *d;

    di = find_dialog_focus(active_dialog);	
    if (ac_grep(NULL,"%im%oi",&imrs,&ois) != 2)	
        return win_printf_OK("cannot find image in acreg!");
    //win_printf("cross mode %d",mode);

    dis_state = do_refresh_overlay;   do_refresh_overlay = 0;
    color = makecol(255,64,64);
    /* check for menu in win_scanf */
    if (cl == 16)      icl = 0;
    else if (cl == 32) icl = 1;
    else if (cl == 64) icl = 2;
    else icl = 3;
    i = win_scanf("this routine track a bead in X,Y\n"
                  " using a cross shaped pattern \n"
                  "arm length 16->%R 32->%r 64->%r 128->%r\narm width %3d\n"
                  ,&icl,&cw); // %m ,select_power_of_2() 
    if (i == CANCEL)	return OFF; 
    if (icl == 0)      cl = 16;
    else if (icl == 1) cl = 32;
    else if (icl == 2) cl = 64;
    else cl = 128;
    //cl_size_selected = 1;

    if (cl > 128)	return win_printf_OK("cannot use arm bigger\nthan 128");
    if (cw > 32)	return win_printf_OK("cannot use width bigger\nthan 32");

    if (fft_init(cl) || filter_init(cl))	return win_printf_OK("cannot init fft!");	
    do_refresh_overlay = dis_state; 

    //live_video(0);
    xc = yc = 0;
    xc = (int)(0.5+x_imr_2_imdata(imrs, mouse_x));    
    yc = (int)(0.5+y_imr_2_imdata(imrs, mouse_y));    

    if (lockin_info == NULL) return win_printf_OK("cannot alloc track info!");	
    if (lockin_info->cl == -1)	    
    {
        // we define tracking parameters
        lockin_info->cl = cl;	    
        lockin_info->cw = cw;	    
        lockin_info->bd_mul = 16;	    
        lockin_info->dis_filter = cl>>2;
        lockin_info->m_b = 16;	    
        lockin_info->n_b = lockin_info->c_b = 0;

        // plots to check tracking
        op = create_and_attach_one_plot(pr_LIVE_VIDEO,  LOCKIN_BUFFER_SIZE, LOCKIN_BUFFER_SIZE, 0);
        ds = op->dat[0];
        ds->xd[1] = ds->yd[1] = 1; 
        set_ds_source(ds, "X rolling buffer");
        if ((ds = create_and_attach_one_ds(op, LOCKIN_BUFFER_SIZE, LOCKIN_BUFFER_SIZE, 0)) == NULL)
            return win_printf_OK("I can't create plot !");
        ds->xd[1] = ds->yd[1] = 1; 
        set_ds_source(ds, "Y rolling buffer");
        /*  if ((ds = create_and_attach_one_ds(op, TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0)) == NULL) */
        /* 		return win_printf_OK("I can't create plot !"); */
        /*       ds->xd[1] = ds->yd[1] = 1;  */
        /*       set_ds_source(ds, "Z rolling buffer"); */
        uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
        op->op_idle_action = z_rolling_buffer_idle_action;
        op->op_post_display = general_op_post_display;
        set_op_filename(op, "X(t)Y(t).gr");
        op->user_ispare[ISPARE_BEAD_NB] = 0;

        set_plot_x_title(op, "Time");
        set_plot_y_title(op, "{\\color{yellow} X} {\\color{lightgreen} Y} ");			

        create_attach_select_x_un_to_op(op, IS_SECOND, 0
                                        ,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");


        // plot to check profile 
        op = create_and_attach_one_plot(pr_LIVE_VIDEO, MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0);
        ds = op->dat[0];
        ds->xd[1] = ds->yd[1] = 1; 
        set_ds_source(ds, "Instantaneous x profile");
        if ((ds = create_and_attach_one_ds(op,  MAX_RADIAL_PROFILE_SIZE, MAX_RADIAL_PROFILE_SIZE, 0)) == NULL)
            return win_printf_OK("I can't create plot !");
        set_ds_source(ds, "Reference x profile");
        op->op_idle_action = x_profile_rolling_buffer_idle_action;
        op->op_post_display = general_op_post_display;
        op->user_ispare[ISPARE_BEAD_NB] = 0;
        set_op_filename(op, "ProfilesX.gr");
        uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_X_UNIT_SET);
        set_plot_x_title(op, "\\delta x");
        set_plot_y_title(op, "Light intensity");			

        // plots to check detection
        op = create_and_attach_one_plot(pr_LIVE_VIDEO, LOCKIN_BUFFER_SIZE, LOCKIN_BUFFER_SIZE, 0);
        ds = op->dat[0];
        ds->xd[1] = ds->yd[1] = 1; 
        set_ds_source(ds, "integrate intensity");
        //if ((ds = create_and_attach_one_ds(op, LOCKIN_BUFFER_SIZE, LOCKIN_BUFFER_SIZE, 0)) == NULL)
        //	return win_printf_OK("I can't create plot !");
        //ds->xd[1] = ds->yd[1] = 1; 
        //set_ds_source(ds, "ac begin");
        uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
        op->op_idle_action = integrate_intensity_rolling_buffer_idle_action;
        op->op_post_display = general_op_post_display;
        op->user_ispare[ISPARE_BEAD_NB] = 0;
        set_op_filename(op, "Integrate intensity.gr");
        set_plot_x_title(op, "Image number");
        set_plot_y_title(op, "Beads intensity dataset");

        /*  op = create_and_attach_one_plot(pr_LIVE_VIDEO, LOCKIN_BUFFER_SIZE, LOCKIN_BUFFER_SIZE, 0); */
        /*       ds = op->dat[0]; */
        /*       ds->xd[1] = ds->yd[1] = 1;  */
        /*       set_ds_source(ds, "Integrate intensity on 2 phases"); */
        /*       uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET); */
        /*       op->op_idle_action = integrate_intensity_2phases_rolling_buffer_idle_action; */
        /*       op->user_ispare[ISPARE_BEAD_NB] = 0; */
        /*       set_op_filename(op, "Integrate intensity on 2 phases.gr"); */
        /*       set_plot_x_title(op, "Image number"); */
        /*       set_plot_y_title(op, "Beads intensity dataset"); */


        op = create_and_attach_one_plot(pr_LIVE_VIDEO, LOCKIN_BUFFER_SIZE, LOCKIN_BUFFER_SIZE, 0);
        ds = op->dat[0];
        ds->xd[1] = ds->yd[1] = 1; 
        set_ds_source(ds, "Integrate intensity async1");
        uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
        op->op_idle_action = integrate_intensity_async1_rolling_buffer_idle_action;
        op->user_ispare[ISPARE_BEAD_NB] = 0;
        set_op_filename(op, "Integrate intensity async1.gr");
        set_plot_x_title(op, "Image number");
        set_plot_y_title(op, "Beads intensity dataset");

        op = create_and_attach_one_plot(pr_LIVE_VIDEO, LOCKIN_BUFFER_SIZE, LOCKIN_BUFFER_SIZE, 0);
        ds = op->dat[0];
        ds->xd[1] = ds->yd[1] = 1; 
        set_ds_source(ds, "Integrate intensity async2");
        uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
        op->op_idle_action = integrate_intensity_async2_rolling_buffer_idle_action;
        op->user_ispare[ISPARE_BEAD_NB] = 0;
        set_op_filename(op, "Integrate intensity async2.gr");
        set_plot_x_title(op, "Image number");
        set_plot_y_title(op, "Beads intensity dataset");

        /*  op = create_and_attach_one_plot(pr_LIVE_VIDEO,  LOCKIN_BUFFER_SIZE, LOCKIN_BUFFER_SIZE, 0); */
        /*       ds = op->dat[0]; */
        /*       ds->xd[1] = ds->yd[1] = 1;  */
        /*       set_ds_source(ds, "Integrate intensity_mod on 2 phases"); */
        /*       uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET); */
        /*       op->op_idle_action = integrate_intensity_2phases_mod_rolling_buffer_idle_action; */
        /*       op->user_ispare[ISPARE_BEAD_NB] = 0; */
        /*       set_op_filename(op, "Integrate intensity_mod on 2 phases.gr"); */
        /*       set_plot_x_title(op, "Image number"); */
        /*       set_plot_y_title(op, "Beads intensity dataset"); */

        op = create_and_attach_one_plot(pr_LIVE_VIDEO,  LOCKIN_BUFFER_SIZE, LOCKIN_BUFFER_SIZE, 0);
        ds = op->dat[0];
        ds->xd[1] = ds->yd[1] = 1; 
        set_ds_source(ds, "Integrate intensity real");
        uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
        op->op_idle_action = integrate_intensity_on_real_rolling_buffer_idle_action;
        op->user_ispare[ISPARE_BEAD_NB] = 0;
        set_op_filename(op, "Integrate intensity real.gr");
        set_plot_x_title(op, "Image number");
        //set_plot_y_title(op, "{\\color{yellow} X} {\\color{lightgreen} Y} Z");
        set_plot_y_title(op, "Beads intensity dataset");	

        op = create_and_attach_one_plot(pr_LIVE_VIDEO,  LOCKIN_BUFFER_SIZE, LOCKIN_BUFFER_SIZE, 0);
        ds = op->dat[0];
        ds->xd[1] = ds->yd[1] = 1; 
        set_ds_source(ds, "Integrate intensity im");
        uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
        op->op_idle_action = integrate_intensity_on_im_rolling_buffer_idle_action;
        op->user_ispare[ISPARE_BEAD_NB] = 0;
        set_op_filename(op, "Integrate intensity im.gr");
        set_plot_x_title(op, "Image number");
        set_plot_y_title(op, "Beads intensity dataset");	

        /*  op = create_and_attach_one_plot(pr_LIVE_VIDEO,  LOCKIN_BUFFER_SIZE, LOCKIN_BUFFER_SIZE, 0); */
        /*       ds = op->dat[0]; */
        /*       ds->xd[1] = ds->yd[1] = 1;  */
        /*       set_ds_source(ds, "Integrate intensity mod"); */
        /*       uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET); */
        /*       op->op_idle_action = integrate_intensity_on_mod_rolling_buffer_idle_action; */
        /*       op->user_ispare[ISPARE_BEAD_NB] = 0; */
        /*       set_op_filename(op, "Integrate intensity mod.gr"); */
        /*       set_plot_x_title(op, "Image number"); */
        /*       //set_plot_y_title(op, "{\\color{yellow} X} {\\color{lightgreen} Y} Z"); */
        /*       set_plot_y_title(op, "Beads intensity dataset");	 */	
    }

    bd = (b_track*)calloc(1,sizeof(b_track));
    if (bd == NULL) return win_printf_OK("cannot alloc bead info!");	
    if (lockin_info->n_b >= lockin_info->m_b)
    {
        lockin_info->m_b += 16;	    
        lockin_info->bd = (b_track**)realloc(lockin_info->bd,lockin_info->m_b*sizeof(b_track*));
        if (lockin_info->bd == NULL) return win_printf_OK("cannot alloc track info!");	
    }
    bd->ncl = bd->cl = cl;
    bd->ncw = bd->cw = cw;
    bd->black_circle = 0;
    bd->xy_trp = fftbt_init(NULL, cl);
    bd->xy_fil = filter_init_bt(NULL, cl);
    for (i = 0; i < bd->cl; i++) bd->rad_prof_ref[i] = -1; // cannot be a profile

    bd->apx = 1;
    bd->apy = 1;
    bd->generic_calib_im = NULL;
    bd->generic_calib_im_fil = NULL;
    bd->calib_im = NULL;
    bd->calib_im_fil = NULL;
    bd->bp_center = 16;
    bd->bp_width = 12;
    bd->rc = 12;

    //bd->npc = lockin_info->cl;
    bd->npc = cl;
    bd->z_trp = fftbt_init(NULL, cl);
    bd->z_fil = filter_init_bt(NULL, cl);

    if (bd->xy_trp == NULL || bd->z_trp == NULL || bd->xy_fil == NULL || bd->z_fil == NULL)
        return win_printf_OK("cannot alloc FFT or Filter plans!");	
    bd->not_lost = -1;
    {
        bd->calib_im = NULL;
        bd->calib_im_fil = NULL;
        bd->not_lost = -1;
    }
    lockin_info->n_bead_display = lockin_info->n_b;    
    lockin_info->bd[lockin_info->n_b++] = bd;
    prepare_bead_menu();
    // win_printf("bead added %d",lockin_info->n_b);
    LOCK_VARIABLE(bd);
    //bd->cw = lockin_info->cw;	    
    bd->last_x_not_lost = (float)xc;
    bd->last_y_not_lost = (float)yc;
    bd->xc = bd->x0 = xc;
    bd->yc = bd->y0 = yc;
    bd->im_prof_ref = -1;
    bd->mouse_draged = 0;
    bd->not_lost = NB_FRAMES_BEF_LOST;
    bd->start_im = 0;
    bid.param = (void*)lockin_info;
    //  bid.to_do = lockin_in_x_y_IFC;
    d = find_dialog_associated_to_imr(imrs, NULL);
    write_and_blit_im_box( plt_buffer, d, imrs);
    move_bead_cross(screen, imrs, d, xc, yc);

    return 0;
}


int follow_bead_in_x_y(void)
{
    int mode = 0;

    if(updating_menu_state != 0)	return D_O_K;
    mode = RETRIEVE_MENU_INDEX;
    return do_follow_bead_in_x_y(mode);
}


int check_heap_raw(char *file, unsigned long line)
{
#ifdef XV_WIN32
    int out;

    out = _heapchk();
    if (out != -2)  win_printf("File %s at line %d\nheap satus %d (-2 =>OK)\n",file,(int)line,out);
    return (out == -2) ? 0 : out;
#else
    return 0;
#endif
}

int timing_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
    register int i, j, k;
    static int  immax = -1; // last_c_i = -1,
    d_s *dsx, *dsy, *dsz;
    int nf, lost_fr, lf, jstart = 0, j1 = 0;
    unsigned long   dtm, dtmax, dts;
    float   fdtm, fdtmax;
    double  tmp, tmp2;
    long long llt;

    if (lockin_info == NULL) return D_O_K;
    dts = get_my_uclocks_per_sec();
    dsx = find_source_specific_ds_in_op(op,"Timing rolling buffer");
    dsy = find_source_specific_ds_in_op(op,"Period rolling buffer");
    dsz = find_source_specific_ds_in_op(op,"Timer rolling buffer");
    if (dsx == NULL || dsy == NULL || dsz == NULL) return D_O_K;	

    i = lockin_info->c_i;
    win_title_used = 1;
    if (lockin_info->imi[i] <= immax) 
    {
        my_set_window_title("waiting %d %d",lockin_info->imi[i], immax);
        return D_O_K;   // we are up todate
    }


    i = lockin_info->c_i;
    immax = lockin_info->imi[i];               
    lf = immax - lockin_info->ac_i;
    if (lockin_info->ac_i > (LOCKIN_BUFFER_SIZE>>1)) 
    {
        nf = LOCKIN_BUFFER_SIZE>>1;
        j = jstart = (lockin_info->ac_i - (LOCKIN_BUFFER_SIZE>>1))%LOCKIN_BUFFER_SIZE;  //last_c_i;
    }
    else
    {
        nf = lockin_info->ac_i-1;
        j = jstart = 1;
    }
    dsx->nx = dsx->ny = nf;
    dsy->nx = dsy->ny = nf;
    dsz->nx = dsz->ny = nf;
    j = jstart + nf - 1;
    j = (j < LOCKIN_BUFFER_SIZE) ? j : j - LOCKIN_BUFFER_SIZE;
    llt = lockin_info->imt[j];
    j1 = j;
    j = jstart;
    j = (j < LOCKIN_BUFFER_SIZE) ? j : j - LOCKIN_BUFFER_SIZE;
    llt -= lockin_info->imt[j];
    llt /= (nf - 1);

    for (i = 0, j = jstart, lost_fr = 0, dtm = dtmax = 0, fdtm = fdtmax = 0, k = 0, tmp2 = 0, lf = 0; i < nf; i++, j++, k++)
    {
        j = (j < LOCKIN_BUFFER_SIZE) ? j : j - LOCKIN_BUFFER_SIZE;
        dsx->xd[i] = dsy->xd[i] = dsz->xd[i] = lockin_info->imi[j];
        if (i > 0)
            lf += lockin_info->imi[j] - lockin_info->imi[j1] - 1;
        dsx->yd[i] = ((float)(1000*lockin_info->imdt[j]))/dts;
        tmp = lockin_info->imt[j];
        tmp -= (i == 0) ? lockin_info->imt[j] : lockin_info->imt[j1];
        tmp = 1000*(double)(tmp)/get_my_ulclocks_per_sec();
        //tmp -= bid.image_per_in_ms * i;
        tmp2 += tmp;
        dsy->yd[i] = (float)tmp;
        dsz->yd[i] = lockin_info->imit[j];

        dtm += lockin_info->imdt[j];
        fdtm += dsx->yd[i];
        dtmax = (lockin_info->imdt[j] > dtmax) ? lockin_info->imdt[j] : dtmax;
        fdtmax = (dsx->yd[i] > fdtmax) ? dsx->yd[i] : fdtmax;
        j1 = j;
    }
    dsy->yd[0] =  dsy->yd[1];
    op->need_to_refresh = 1; 
    if (k) set_plot_title(op, "\\stack{{fr position lac_i %d }{%d missed images}"
                          "{\\pt8 dt mean %6.3f ms (%6.3f ms) max %6.3f (%6.3f)}}"
                          ,bid.first_im +lockin_info->ac_i 
                          ,lf,fdtm/k,
                          1000*((double)(dtm/k))/dts,
                          fdtmax, 1000*((double)(dtmax))/dts);

    i = LOCKIN_BUFFER_SIZE/8;
    j = ((int)dsx->xd[1])/i;
    op->x_lo = j*i;
    op->x_hi = (j+5)*i;
    set_plot_x_fixed_range(op);
    return refresh_plot(pr_LIVE_VIDEO, UNCHANGED);
}




int phase_in_imagethread_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
    register int i, j;
    static int last_c_i = -1, immax = -1;
    d_s *dsx;
    int nf;

    if (lockin_info == NULL) return D_O_K;
    dsx = find_source_specific_ds_in_op(op,"Phase expected");
    if (dsx == NULL) return D_O_K;	

    i = lockin_info->c_i;
    if (lockin_info->imi[i] <= immax) return D_O_K;   // we are uptodate
    immax = lockin_info->imi[i];   

    nf = (lockin_info->ac_i > LOCKIN_BUFFER_SIZE) ? LOCKIN_BUFFER_SIZE : lockin_info->ac_i;      
    nf = (nf > (LOCKIN_BUFFER_SIZE>>1)) ? LOCKIN_BUFFER_SIZE>>1 : nf;
    dsx->nx = dsx->ny = nf;

    last_c_i = (lockin_info->c_i > 0) ? lockin_info->c_i - 1 : 0;  // c_i is not yet valid
    for (i = nf-1, j = last_c_i; i >= 0; i--, j--)
    {
        j = (j < 0) ? LOCKIN_BUFFER_SIZE + j: j;
        //dsx->yd[i]= (float)(1000*(double)(lockin_info->imt[j] - lockin_info->imt0)/get_my_ulclocks_per_sec() - lockin_info->delta[j]); 
        dsx->yd[i]= lockin_info->phase[j]; 
        dsx->xd[i] = lockin_info->imi[j];
    }

    i = LOCKIN_BUFFER_SIZE/8;
    j = ((int)dsx->xd[i])/i;
    op->x_lo = (j-1)*i;
    op->x_hi = (j+4)*i;
    set_plot_x_fixed_range(op);

    op->need_to_refresh = 1; 
    set_plot_title(op, "\\stack{{image %d}{period %8f}{phi start %d}}"
                   ,lockin_info->ac_i, (float)(1000*(double)lockin_info->per_i/get_my_ulclocks_per_sec()), lockin_info->ph_start);

    return refresh_plot(pr_LIVE_VIDEO, UNCHANGED);
}

int phase_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
    register int i, j   ;
    static int last_c_i = -1, immax = -1;
    d_s *dsx, *dsy, *dsz;
    int nf, b_size;
    double tmpd;

    if (lockin_info == NULL) return D_O_K;

    dsx = find_source_specific_ds_in_op(op,"Modulation phase measured");
    if (dsx == NULL) return D_O_K;
    dsy = find_source_specific_ds_in_op(op,"Modulation phase simulated"); 
    if (dsy == NULL) return D_O_K; 
    dsz = find_source_specific_ds_in_op(op,"Error");
    if (dsz == NULL) return D_O_K;

    i = ph_lock.c_i;
    if (ph_lock.ac_i <= immax) return D_O_K;   // we are uptodate
    immax = ph_lock.ac_i;   

    b_size = 50*LOCKIN_BUFFER_SIZE/3;            
    nf = (ph_lock.ac_i > b_size) ? b_size : ph_lock.ac_i;      
    nf = (nf > (b_size>>4)) ? (b_size>>4) : nf;
    dsx->nx = dsx->ny = nf;
    dsy->nx = dsy->ny = nf;
    dsz->nx = dsz->ny = nf;

    last_c_i = (ph_lock.c_i > 0) ? ph_lock.c_i - 1 : 0;  // c_i is not yet valid
    for (i = nf-1, j = last_c_i; i >= 0; i--, j--) 
    {
        j = (j < 0) ? b_size + j: j;
        tmpd = 1000*((double)(ph_lock.imt[j] - ph_lock.imt0))/get_my_ulclocks_per_sec();
        dsx->xd[i] = (float)tmpd;
        dsx->yd[i] = (float)ph_lock.phase2[j];
        dsy->xd[i] = (float)tmpd;
        dsy->yd[i] = (float)ph_lock.phase1[j];
        dsz->xd[i] = (float)tmpd;
        dsz->yd[i] = (float)ph_lock.dephi[j];
    }
    op->x_lo = dsx->xd[0] - (dsx->xd[dsx->nx-1] - dsx->xd[0])/20;  
    op->x_hi = dsx->xd[dsx->nx-1] + (dsx->xd[dsx->nx-1] - dsx->xd[0])/20;
    set_plot_x_fixed_range(op);

    op->need_to_refresh = 1; 
    set_plot_title(op, "\\stack{{image %d}{Period %d}{Period in ms %8f}{d\\phi %g}}"
                   ,ph_lock.ac_i, lockin_info->per_i,(float)(1000*(double)lockin_info->per_i/get_my_ulclocks_per_sec()),lockin_info->dphi_t);
    return refresh_plot(pr_LIVE_VIDEO, UNCHANGED);
}


int vco_phase_pid(long long t,      // the time stamp to determine phase
                  float error,      // error signal
                  int mode,         // mode = 0, return P and I, else change their value
                  float *P,         // proportionnal 
                  float *I,         // integralal
                  int *per,         // if > 0 define the new period of the vco else
                  // return perl the present vco period
                  int *n_cycle,     // the number of cycles since started
                  int *cycl_ph      // the position inside cycle [0 to perl]
                 )
{
    static long long org = 0, perl = 16666, t0 = 0, tmpl;
    static float Pl = 500, Il = 0.0001;
    static double Ival = 0;
    static int ph0, first = 1;
    int ph;

    if (mode == 0 && P != NULL) *P = Pl;
    else if (mode != 0 && P != NULL) Pl = *P;
    if (mode == 0 && I != NULL) *I = Il;
    else if (mode != 0 && I != NULL) Il = *I;
    if (per != NULL)
    {
        if (*per > 0) 
        {
            tmpl = (t - t0 + org) / perl;
            ph0 = (int)tmpl + ph0;
            org = (t - t0 + org) % perl;
            if (first)
            {
                first = 0;
                ph0 = 0;
            }
            perl = (long long)*per; 
            t0 = t;
        }
        else *per = (int)perl;
    }
    if (error != 0.0)
    {
        tmpl = (t - t0 + org) / perl;
        ph0 = (int)tmpl + ph0;
        org = (t - t0 + org) % perl;
        t0 = t;
        Ival += error;
        perl = perl + Pl * error + Il * Ival;
    }  
    if (t > 0)
    {
        //org = (t - t0) + org;
        tmpl = (t - t0 + org) / perl;
        ph = (int)tmpl + ph0;
        //org -= perl * ph;
        if (n_cycle) *n_cycle = ph;
        if(cycl_ph) *cycl_ph = (int) (t - t0 + org - ph* perl);
    }
    return 0;
}

int adj_vco(void)
{
    int i;
    float P, I;
    int per = 0;

    if (lockin_info == NULL)  return 0;

    if(updating_menu_state != 0)	return 0;  
    vco_phase_pid(0,0,0,&P,&I,&per,NULL,NULL);
    i = win_scanf("VCO P %12f I %12f \nper %20d\n",&P,&I,&per);
    if (i == CANCEL)  	return 0;  
    vco_phase_pid(0,0,1,&P,&I,&per,NULL,NULL);
    return 0;  
}


double do_dphi(int n)
{
    int i, j, ndphi, b_size, last_ci, phase = 0;
    double dphi;
    if (lockin_info == NULL) return D_O_K;

    b_size = 50*LOCKIN_BUFFER_SIZE/3;
    last_ci = ph_lock.c_i;
    for (i = 0, j = last_ci, dphi = 0, ndphi = 0; i < n; i++, j--)
    {
        j = (j < 0) ? b_size + j: j;
        vco_phase_pid(ph_lock.imt[j],0,0,NULL,NULL,NULL,&phase,NULL);
        ph_lock.phase1[j] = 90*(phase%4);
        //if (phase%4 == 0)	
        //{
        // dphi += (ph_lock.phase2[j] <= 180) ? -ph_lock.phase2[j] : 90; 
        //ndphi++;
        //}
        if (phase%4 == 1)	
        {
            dphi += 90 - ph_lock.phase2[j]; 
            ndphi++;
        }
        else if (phase%4 == 2)	
        {
            dphi += 180 - ph_lock.phase2[j]; 
            ndphi++;
        }
        //else 
        // {
        //dphi += (ph_lock.phase2[j] <= 90) ? -90 - ph_lock.phase2[j] : 270 - ph_lock.phase2[j]; 
        //ndphi++;
        // }
    } 
    dphi = (ndphi > 0) ? dphi/ndphi : -1000;
    for (i = 0, j = last_ci; i < n; i++, j--)
    {
        j = (j < 0) ? b_size + j: j;
        ph_lock.dephi[j] = dphi;
    }
    return dphi;
}

//commands to operate on the averaged movies/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int adj_navg(void)
{
    int na, i;

    if (lockin_info == NULL)  return 0;
    na = lockin_info->navg;
    if(updating_menu_state != 0)	return 0;  
    i = win_scanf("Navg %d",&na);
    if (i == CANCEL)  	return 0;  
    lockin_info->navg = 2*na/2;
    return 0;  
}

int adj_toogle_frame(void)
{
    int fr;

    if (lockin_info == NULL)  return 0;
    fr = lockin_info->toogle_frame;
    if(updating_menu_state != 0)	
    {
        if (fr == RETRIEVE_MENU_INDEX)
            active_menu->flags |= D_SELECTED;	
        else active_menu->flags &= ~D_SELECTED;	
        return 0;  
    }
    lockin_info->toogle_frame = (fr) ? 0 : 1;
    return 0;  
}

//int ask_to_update_menu(void)
//{
//  if (lockin_info == NULL)  return 1;
//  last_im_who_asked_menu_update = lockin_info->imi[lockin_info->c_i];
// return 0;
//}

int oi_avg_idle_action(O_i *oi, DIALOG *d)
{
    if (d == NULL || d->dp == NULL || d->proc == NULL)    return 1;
    if (oi->need_to_refresh)      d->proc(MSG_DRAW,d,0);	
    return 0;
}



int	fill_avg_spot_from_im(O_i *oi, int xc, int yc,  int cw)
{
    register int j, i;
    unsigned char *choi;	
    short int *inoi;
    int ytt, xll, onx, ony, xco, yco, cl2, cw2, val = 0;
    union pix *pd;

    if (oi == NULL)	return 1;
    onx = oi->im.nx;	ony = oi->im.ny;
    pd = oi->im.pixel;
    cl2 = cw >> 1;	cw2 = cw >> 1;
    yco = (yc - cl2 < 0) ? cl2 : yc;
    yco = (yco + cl2 <= ony) ? yco : ony - cl2;
    xco = (xc - cl2 < 0) ? cl2 : xc;
    xco = (xco + cl2 <= onx) ? xco : onx - cl2;
    val = 0;
    if (oi->im.data_type == IS_CHAR_IMAGE)
    {
        for(j = 0, ytt = yco - cw2, xll = xco - cl2; j< cw; j++)
        {	
            choi = pd[ytt+j].ch + xll;
            for (i = 0; i < cw; i++)		val += (int)choi[i];	
        }
    }
    else if (oi->im.data_type == IS_INT_IMAGE)
    {
        for(j = 0, ytt = yco - cw2, xll = xco - cl2; j< cw; j++)
        {	
            inoi = pd[ytt+j].in + xll;
            for (i = 0; i < cw; i++)		val += (int)inoi[i];	
        }
    }
    else return 1;	
    return val;		
}

///////


# ifdef TOCOPY
// this running in the timer thread
int proceed_bead(b_track *bt, int bt_index, g_track *gt, BITMAP *imb, O_i *oi)
{
    register int i, j;
    int xc, yc, cl, cw, cl2, di, lost_this_time = 0, ci_1;
    int  black_circle, *xi, *yi;
    float dz = 0, dz_1, zobjn, xcf, ycf, dx = 0, dy = 0, corr, *xf, *yf, *tpf;
    //float ax = 1, ay = 1;
    //  int red = makecol(255,0,0);
    // int greeen = makecol(0,255,0);

    cl = gt->cl;                   // cross arm length	
    bt->cl = bt->ncl;              // we update cross changes 
    bt->cw = bt->ncw;
    cl = (bt->cl < 4) ? gt->cl : bt->cl; 

    cl2 = cl/2;
    //cw = gt->cw;                   // cross arm width	
    cw = bt->cw;                   // cross arm width	
    xi = gt->xi;	yi = gt->yi;     // these are temporary int buffers for averaged profile
    xf = gt->xf;	yf = gt->yf;     // these are temporary float buffers for averaged profile
    cl2 = cl/2;
    di = find_dialog_focus(active_dialog);                // I think this does not work
    ci_1 = gt->c_i -1;
    ci_1 = (ci_1 < 0) ? LOCKIN_BUFFER_SIZE-1 : ci_1; 
    bt->n_l[gt->c_i] = (char)bt->not_lost;
    black_circle = bt->black_circle;
    if (bt->mouse_draged)
    { xc = bt->mx;		yc = bt->my;  }         // last valid position
    else if (bt->not_lost <= 0)                            // the bead is lost
    { xc = bt->x0;		yc = bt->y0;  }         // last valid position
    else                                              // the bead is not lost
    {	xc = bt->xc;		yc = bt->yc;  } // previous position
    bt->in_image = is_cross_in_image(oi, xc, yc);
    xcf = xc; 	        ycf = yc;
    if (bt->in_image)
    {
        if (bt->cross_45) fill_X_avg_profiles_from_im(oi, xc, yc, cl, cw, xi, yi);
        else fill_avg_profiles_from_im(oi, xc, yc, cl, cw, xi, yi);
        if (black_circle)	    erase_around_black_circle(xi, yi, cl);
        lost_this_time = check_bead_not_lost(xi, xf, yi, yf, cl, gt->bd_mul);
        if (bt->not_lost <= 0 && lost_this_time > 0)                 // the bead is still lost
        {
            if (bt->mouse_draged == 0) clip_cross_in_image(oi, bt->xc, bt->yc, cl2);
            return 1;
        }
        else if (bt->not_lost <= 0 && lost_this_time == 0) 	        // the bead is recovered we reset its state
            bt->not_lost = NB_FRAMES_BEF_LOST;
        else bt->not_lost -= lost_this_time;                         // the bead was not lost
    }
    bt->n_l[gt->c_i] = (char)bt->not_lost;            // we save its present state
    if (lost_this_time == 0 && bt->in_image)                          // the bead is present for this frame
    {
        if (bt->mouse_draged == 0 && bt->in_image)
        {
            dx = find_distance_from_center(xf, cl, 0, gt->dis_filter, !black_circle,&corr, bt->xy_trp, bt->xy_fil);
            dy = find_distance_from_center(yf, cl, 0, gt->dis_filter, !black_circle,&corr, bt->xy_trp, bt->xy_fil);
            if (bt->cross_45) 
            {
                bt->x[gt->c_i] = xcf = xc + (dx + dy);
                bt->y[gt->c_i] = ycf = yc + (dx - dy) - 1;
            }
            else 
            {
                bt->x[gt->c_i] = xcf = dx + xc;               // we save its new x, y position
                bt->y[gt->c_i] = ycf = yc + dy;
            }
            if (bt->start_im <= 0) bt->start_im = gt->ac_i;
        }
        else 
        {
            bt->x[gt->c_i] = xcf = bt->mx;               // we save its new x, y position
            bt->y[gt->c_i] = ycf = bt->my;
        }
        bt->xc = (int)(xcf + .5);	bt->yc = (int)(ycf + .5);
        if (bt->mouse_draged) bt->im_prof_ref = -1; 

        bt->z_track = 0;
        bt->profile_index = -1;
        bt->z_track = 0;
    }
    else                                              // we save its prevoius position
    {
        bt->x[gt->c_i] = bt->x[(gt->c_i) ? gt->c_i-1 : 0];
        bt->y[gt->c_i] = bt->y[(gt->c_i) ? gt->c_i-1 : 0];
    }
    if (bt->mouse_draged == 0 && bt->in_image) 
    {
        if (bt->cross_45)
        {  
            clip_X_cross_in_image(oi, bt->xc, bt->yc, cl2, cw/2);
        }
        else
        {               
            clip_cross_in_image(oi, bt->xc, bt->yc, cl2);     // we prevent cross to escape from frame
        }
    }
    return 0;
}


# endif

////////



/*		correlate "x1" with its invert over "nx" points, returns the correlation
 *		in "fx1". "fx1" will be used to compute fft and result 
 *		setting them to NULL will allocate space. "window" will enable
 *		a windowing process if != 0 a double windowing if = to 2.
 */
int	correlate_1d_sig_and_invert2(float *x1, int nx, int window, float *fx1, int lfilter, int remove_dc, fft_plan *fp, filter *fil)
{
    register int i, j;
    float moy, smp = 0.05;
    float m1, re1, re2, im1, im2;

    if (x1 == NULL)				return WRONG_ARGUMENT;
    if (fp == NULL || fil == NULL)          return FFT_NOT_SUPPORTED;
    if (fftbt_init(fp, nx) == NULL)         return FFT_NOT_SUPPORTED;
    //if (fft_init(nx) || filter_init(nx))	return FFT_NOT_SUPPORTED;
    if (fx1 == NULL)	fx1 = (float*)calloc(nx,sizeof(float));
    if (fx1 == NULL)			return OUT_MEMORY;

    /*	compute fft of x1 */
    for(i = 0; i < nx; i++)		fx1[i] = x1[i];
    for(i = 0, j = (3*nx)/4, moy = 0; i < nx/4; i++, j++)		
        moy += fx1[i] + fx1[j];	
    for(i = 0, moy = (2*moy)/nx; remove_dc && i < nx; i++)	
        fx1[i] = x1[i] - moy;
    if (window == 1)	fftbtwindow1(fp, fx1, smp);
    else if (window == 2)	fftwindow_flat_top1(fp, nx, fx1, smp);

    realtr1bt(fp, fx1);
    //for(i = 0; i < nx; i++)		x1[i] = fx1[i];
    //return 0;
    //win_printf("bef fft fx1[3] = %f moy %f nx %d filter %d",fx1[3],moy,nx,filter);
    fftbt(fp, fx1, 1);
    realtr2bt(fp, fx1, 1);

    //crude high pass filter
    fx1[0] = fx1[1] = 0;
    fx1[2] = fx1[3] = 0;
    fx1[4] *= 0.1;	fx1[5] *= 0.1;
    fx1[6] *= 0.3;	fx1[7] *= 0.3;
    fx1[8] *= 0.6;	fx1[9] *= 0.6;
    fx1[10] *= 0.9;	fx1[11] *= 0.9;

    /*	compute normalization */
    for (i=0, m1 = 0; i< nx; i+=2)
        m1 += fx1[i] * fx1[i] + fx1[i+1] * fx1[i+1];
    m1 /= 2;	

    /*	compute correlation in Fourier space */
    for (i=2; i< nx; i+=2)
    {
        re1 = fx1[i];		re2 = fx1[i];
        im1 = fx1[i+1];		im2 = -fx1[i+1];
        fx1[i] 		= (re1 * re2 + im1 * im2)/m1;
        fx1[i+1] 	= (im1 * re2 - re1 * im2)/m1;
    }
    fx1[0]	= 2*(fx1[0] * fx1[0])/m1;	/* these too mode are special */
    fx1[1] 	= 2*(fx1[1] * fx1[1])/m1;
    if (lfilter> 0)		lowpass_smooth_half_bt(fil, nx, fx1, lfilter);

    /*	get back to real world */
    realtr2bt(fp, fx1, -1);
    //win_printf("bef 2 fft fx1[2] = %f",fx1[2]);
    fftbt(fp, fx1, -1);
    realtr1bt(fp, fx1);
    for (i=nx/8; i< 7*nx/8; i++) fx1[i] = 0;
    deplace(fx1, nx);
    return 0;
}	

float	find_distance_from_center2(float *x1, int cl, int flag, int lfilter, int black_circle, float *corr, float *deriv, fft_plan *fp, filter *fil)
{
    float dx = 0;
    extern float fx1[512];

    correlate_1d_sig_and_invert2(x1, cl, flag, fx1, lfilter, 1, fp, fil);//(black_circle)?0:1);
    find_max1(fx1, cl, &dx,corr, deriv);
    dx -= cl/2;
    dx /=2;
    return dx;
}



// this running in the timer thread
int proceed_bead(b_track *bt, int bt_index, g_lockin *gt, BITMAP *imb, O_i *oi, O_i *oi_async1, O_i *oi_async2, O_i *oi_rel, O_i *oi_im)
{
    register int i, j;
    int xc, yc, cl, cw, cl2, di, lost_this_time = 0, ci_1;
    int  black_circle, *xi, *yi, tmpi;
    float dz = 0, dz_1, zobjn, xcf, ycf, dx = 0, dy = 0, corr, deriv, *xf, *yf, *tpf;
    extern float fx1[512];

    cl = gt->cl;                   // cross arm length	
    bt->cl = bt->ncl;              // we update cross changes 
    bt->cw = bt->ncw;
    cl = (bt->cl < 4) ? gt->cl : bt->cl; 

    cl2 = cl/2;
    cw = bt->cw;                   // cross arm width	
    // xi = gt->xi;	yi = gt->yi;     // these are temporary int buffers for averaged profile
    //xf = gt->xf;	yf = gt->yf;     // these are temporary float buffers for averaged profile
    xi = bt->xi;	yi = bt->yi;     // these are temporary int buffers for averaged profile
    xf = bt->xf;	yf = bt->yf;     // these are temporary float buffers for averaged profile
    cl2 = cl/2;
    di = find_dialog_focus(active_dialog);                // I think this does not work
    ci_1 = gt->c_i -1;
    ci_1 = (ci_1 < 0) ? LOCKIN_BUFFER_SIZE-1 : ci_1; 
    bt->n_l[gt->c_i] = (char)bt->not_lost;
    black_circle = bt->black_circle;
    if (bt->mouse_draged)
    { xc = bt->mx;		yc = bt->my;  }         // last valid position
    //else if (bt->not_lost <= 0)                            // the bead is lost
    //{ xc = bt->x0;		yc = bt->y0;  }         // last valid position
    else                                              // the bead is not lost
    {	xc = bt->xc;		yc = bt->yc;  } // previous position
    bt->in_image = is_cross_in_image(oi, xc, yc);
    xcf = xc; 	        ycf = yc;

    if (bt->in_image)
    {
        if (bt->cross_45) fill_X_avg_profiles_from_im(oi, xc, yc, cl, cw, xi, yi,0,0);
        fill_avg_profiles_from_im(oi, xc, yc, cl, cw, xi, yi,0,0);
        if (black_circle)	    erase_around_black_circle(xi, yi, cl);
        lost_this_time = check_bead_not_lost(xi, xf, yi, yf, cl, gt->bd_mul);

        if (bt->not_lost <= 0 && lost_this_time > 0)                 // the bead is still lost
        {
            if (bt->mouse_draged == 0) clip_cross_in_image(oi, bt->xc, bt->yc, cl2);
            return 1;
        }
        else if (bt->not_lost <= 0 && lost_this_time == 0) 	        // the bead is recovered we reset its state
            bt->not_lost = NB_FRAMES_BEF_LOST;
        else bt->not_lost -= lost_this_time;                         // the bead was not lost
        for (i = -cw/2, tmpi =0; i < cw/2; i++) tmpi += xi[cl/2 + i];
        bt->amp[gt->c_i] = (float)tmpi/(cw*cw);

        // tmpi = fill_avg_spot_from_im(oi_2phase, xc, yc, cw);
        //bt->amp_2phase[gt->c_i] = (float)tmpi/(cw*cw*lockin_info->navg);

        tmpi = fill_avg_spot_from_im(oi_async1, xc, yc, cw);
        bt->amp_async1[gt->c_i] = (float)tmpi/(cw*cw*lockin_info->navg);

        tmpi = fill_avg_spot_from_im(oi_async2, xc, yc, cw);
        bt->amp_async2[gt->c_i] = (float)tmpi/(cw*cw*lockin_info->navg);

        //tmpi = fill_avg_spot_from_im(oi_2phase_mod, xc, yc, cw);
        //bt->amp_2phase_mod[gt->c_i] = (float)tmpi/(cw*cw);

        tmpi = fill_avg_spot_from_im(oi_rel, xc, yc, cw);
        bt->ampds_rel[gt->c_i] = (float)tmpi/(cw*cw*lockin_info->navg);

        tmpi = fill_avg_spot_from_im(oi_im, xc, yc, cw);
        bt->ampds_im[gt->c_i] = (float)tmpi/(cw*cw*lockin_info->navg);

        //tmpi = fill_avg_spot_from_im(oi_mod, xc, yc, cw);
        //bt->ampds_mod[gt->c_i] = (float)tmpi/(cw*cw);

    }
    //win_title_used = 1;
    // my_set_window_title("in image %d xc %d yc %d",bt->in_image,xc,yc);
    bt->n_l[gt->c_i] = (char)bt->not_lost;            // we save its present state
    if (bt->in_image)                          // the bead is present for this frame
    {
        if (bt->mouse_draged == 0 && bt->in_image)
        {
            dx = find_distance_from_center2(xf, cl, 0, gt->dis_filter, !black_circle,&corr, &deriv, bt->xy_trp, bt->xy_fil);
            for (i = 0; i < cl;  i++) bt->xcor[i] = fx1[i];
            dy = find_distance_from_center2(yf, cl, 0, gt->dis_filter, !black_circle,&corr, &deriv, bt->xy_trp, bt->xy_fil);
            for (i = 0; i < cl;  i++) bt->ycor[i] = fx1[i];
            if (bt->fixed_bead) 
            {
                bt->x[gt->c_i] = xcf;             
                bt->y[gt->c_i] = ycf;
            }
            else
            {
                if (bt->cross_45) 
                {
                    bt->x[gt->c_i] = xcf = xc + (dx + dy);
                    bt->y[gt->c_i] = ycf = yc + (dx - dy) - 1;
                }
                else 
                {
                    bt->x[gt->c_i] = xcf = dx + xc;               // we save its new x, y position
                    bt->y[gt->c_i] = ycf = yc + dy;
                }
            }
            if (bt->start_im <= 0) bt->start_im = gt->ac_i;
        }
        else 
        {
            bt->x[gt->c_i] = xcf = bt->mx;               
            bt->y[gt->c_i] = ycf = bt->my;
        }
        bt->xc = (int)(xcf + .5);	bt->yc = (int)(ycf + .5);
        if (bt->mouse_draged) bt->im_prof_ref = -1; 

        bt->z_track = 0;
        bt->profile_index = -1;
        bt->z_track = 0;

    }
    else                                              // we save its prevoius position
    {
        bt->x[gt->c_i] = bt->x[(gt->c_i) ? gt->c_i-1 : 0];
        bt->y[gt->c_i] = bt->y[(gt->c_i) ? gt->c_i-1 : 0];
    }
    if (bt->mouse_draged == 0 && bt->in_image) 
    {
        if (bt->cross_45)
        {  
            clip_X_cross_in_image(oi, bt->xc, bt->yc, cl2, cw/2);
        }
        else
        {               
            clip_cross_in_image(oi, bt->xc, bt->yc, cl2);     // we prevent cross to escape from frame
        }
    }
    return 0;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// this running in the timer thread
int do_at_all_image(imreg *imr, O_i *oi, int n,  DIALOG *d, long long t, unsigned long dt, void *p, int n_inarow) 
{
    register int k, j;
    int ci, ci_1, ac_i;
    BITMAP *imb = NULL;
    g_lockin *gt = NULL;

    O_i *oia1, *oia2, *oia_async1, *oia_async2;//*oia
    short int  *pi_avg1, *pi_avg2, *pi_avg_async1, *pi_avg_async2;//*pi_avg
    int i_avg1=0, i_avg2=0, iavg_async1=0, iavg_async2=0, peri, tmpi, phase;//iavg = 0
    unsigned char *pc;

    int cl, cw, cl2, c_b;
    int dis_filter = 0, *xi, *yi;
    float  *xf, *yf;
    b_track *bt = NULL;


    gt = (g_lockin*)p;

    ac_i = lockin_info->ac_i;
    ac_i++;
    ci_1 = ci = lockin_info->c_i;
    ci++;
    ci = (ci < lockin_info->n_i) ? ci : 0;
    lockin_info->imi[ci] = n;                                 // we save image" number
    lockin_info->imit[ci] = n_inarow;                                 
    lockin_info->imt[ci] = t;                                 // image time
    lockin_info->c_i = ci; 


    if (lockin_info->ac_i == 0)  
    {
        lockin_info->imt0 = lockin_info->imt[ci]; 
        //my camera  peri = (int)(((double)get_my_ulclocks_per_sec())*0.016674);
        // hz test 
        peri = (int)(((double)get_my_ulclocks_per_sec())*0.016674);    
        vco_phase_pid(lockin_info->imt0,0,0,NULL,NULL,&peri,NULL,NULL); 
    }

    if (ph_lock.ac_i < ph_lock.n_phi)       lockin_info->per_i = 0;
    else if ((ac_i%4) == 0)
    {
        lockin_info->dphi_t =  do_dphi(ph_lock.n_phi);
        peri = 0;
        tmpi = ph_lock.c_i - ph_lock.n_phi;
        tmpi = (tmpi < 0) ? tmpi + 50*LOCKIN_BUFFER_SIZE/3 : tmpi;
        vco_phase_pid(ph_lock.imt[tmpi],lockin_info->dphi_t,0,NULL,NULL,&peri,NULL,NULL);
        lockin_info->per_i = peri;
        vco_phase_pid(lockin_info->imt[ci],0,0,NULL,NULL,NULL,&phase,NULL);
    }
    //vco_phase_pid(lockin_info->imt[ci]-33365228,0,0,NULL,NULL,NULL,&phase,NULL);
    vco_phase_pid(lockin_info->imt[ci]-lockin_info->per_i,0,0,NULL,NULL,NULL,&phase,NULL);
    lockin_info->phase[ci] = 90*(phase%4);


    O_i *oi_synchro;
    short int *pi_sync;
    O_p *op;
    int ac_istart;
    oi_synchro = lockin_info->oid;
    op = oi_synchro->o_p[0];

    ac_istart =  ph_lock.n_phi;
    if (ac_i >= ac_istart && ac_i < ac_istart + 256)
    {
        op->dat[0]->xd[ac_i-ac_istart] = ac_i-ac_istart;
        op->dat[0]->yd[ac_i-ac_istart] = lockin_info->phase[ci];

        for (k = lockin_info->y0; k < lockin_info->y1; k++)
        {
            pi_sync = oi_synchro->im.pxl[ac_i-ac_istart][k].in;
            pc = oi->im.pixel[k].ch;
            for (j = lockin_info->x0; j < lockin_info->x1; j++)   pi_sync[j] = pc[j];
        }
    }

    ////image treatment///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    if (oi->bmp.stuff != NULL) imb = (BITMAP*)oi->bmp.stuff;
    // we grab the video bitmap of the IFC image
    prepare_image_overlay(oi);
    //oia = lockin_info->oi_avg; 
    oia_async1 = lockin_info->oi_avg_async_1;
    oia_async2 = lockin_info->oi_avg_async_2;
    oia1 = lockin_info->oi_avg1;
    oia2 = lockin_info->oi_avg2;
    //oia3 = lockin_info->oi_avg3;




    /* //we create an avg image over navg frames by changing phase every frame with 2 phases */

    /*  i_avg = (oia->im.c_f == 0) ? 1 : 0; */
    /*   //i_avg0 = (oia0->im.c_f == 0) ? 1 : 0; */


    /*   if (lockin_info->toogle_frame == 0) */
    /*     { */
    /*       if ((ac_i%lockin_info->navg) == 0) */
    /* 	{ */
    /* 	  for (k = lockin_info->y0; k < lockin_info->y1; k++) */
    /* 	    { */
    /* 	      pi_avg = oia->im.pxl[i_avg][k].in; */
    /* 	      pc = oi->im.pixel[k].ch; */
    /* 	      for (j = lockin_info->x0; j < lockin_info->x1; j++)        pi_avg[j] = pc[j]; */
    /* 	    } */
    /* 	} */
    /*       else if ((ac_i%2) == 0) */
    /* 	{ */
    /* 	  for (k = lockin_info->y0; k < lockin_info->y1; k++) */
    /* 	    { */
    /* 	      pi_avg = oia->im.pxl[i_avg][k].in; */
    /* 	      pc = oi->im.pixel[k].ch; */
    /* 	      for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg[j] += pc[j]; */
    /* 	    } */
    /* 	} */
    /*       else */
    /* 	{ */
    /* 	  for (k = lockin_info->y0; k < lockin_info->y1; k++) */
    /* 	    { */
    /* 	  pi_avg = oia->im.pxl[i_avg][k].in; */
    /* 	  pc = oi->im.pixel[k].ch; */
    /* 	  for (j = lockin_info->x0; j < lockin_info->x1; j++)           pi_avg[j] -= pc[j];   */
    /* 	    } */
    /* 	} */
    /*     } */
    /*   else  */
    /*     { */
    /*      if ((ac_i%lockin_info->navg) == 0) */
    /* 	{ */
    /* 	  for (k = lockin_info->y0; k < lockin_info->y1; k++) */
    /* 	    { */
    /* 	      pi_avg = oia->im.pxl[i_avg][k].in; */
    /* 	      pc = oi->im.pixel[k].ch; */
    /* 	      for (j = lockin_info->x0; j < lockin_info->x1; j++)        pi_avg[j] = -pc[j]; */
    /* 	    } */
    /* 	} */
    /*       else if ((ac_i%2) == 0) */
    /* 	{ */
    /* 	  for (k = lockin_info->y0; k < lockin_info->y1; k++) */
    /* 	    { */
    /* 	      pi_avg = oia->im.pxl[i_avg][k].in; */
    /* 	      pc = oi->im.pixel[k].ch; */
    /* 	      for (j = lockin_info->x0; j < lockin_info->x1; j++)        pi_avg[j] -= pc[j]; */
    /* 	    } */
    /* 	} */
    /*       else */
    /* 	{ */
    /* 	  for (k = lockin_info->y0; k < lockin_info->y1; k++) */
    /* 	    { */
    /* 	  pi_avg = oia->im.pxl[i_avg][k].in; */
    /* 	  pc = oi->im.pixel[k].ch; */
    /* 	  for (j = lockin_info->x0; j < lockin_info->x1; j++)            pi_avg[j] += pc[j];   */
    /* 	    } */
    /* 	} */
    /*     }  */
    /*  /\*  if ((ac_i%lockin_info->navg) == (lockin_info->navg -1)) *\/ */
    /* /\*     { *\/ */
    /* /\*       for (k = lockin_info->y0; k < lockin_info->y1; k++) *\/ */
    /* /\* 	{ *\/ */
    /* /\* 	  pi_avg = oia->im.pxl[i_avg][k].in; *\/ */
    /* /\* 	  pi_avg0 = oia0->im.pxl[i_avg0][k].in; *\/ */
    /* /\* 	  for (j = lockin_info->x0; j < lockin_info->x1; j++)     pi_avg0[j] = pi_avg[j]*pi_avg[j];  *\/ */
    /* /\*         } *\/ */
    /* /\*     } *\/ */

    /*   if ((ac_i%lockin_info->navg) == (lockin_info->navg -1)) */
    /*      { */
    /*        switch_frame(oia,i_avg); */
    /*        //switch_frame(oia0,i_avg0); */
    /*        // find_zmin_zmax(oia); */
    /*        oia->need_to_refresh |= BITMAP_NEED_REFRESH; */
    /*        //oia0->need_to_refresh |= BITMAP_NEED_REFRESH; */
    /*        set_im_title(oia, "Image avg %d", ac_i/lockin_info->navg); */
    /*        // set_im_title(oia0, "Image avg %d", ac_i/lockin_info->navg); */
    /*      } */


    //we create 2 avg image over navg frames with real and imaginary part without knowledge about phase */
    /* //phase rolling = 0, 90, 180, 270 */ 
    iavg_async1 = (oia_async1->im.c_f == 0) ? 1 : 0;
    iavg_async2 = (oia_async2->im.c_f == 0) ? 1 : 0;

    if ((ac_i%lockin_info->navg) == 0)
    {
        for (k = lockin_info->y0; k < lockin_info->y1; k++)
        {
            pi_avg_async1 = oia_async1->im.pxl[iavg_async1][k].in;
            pc = oi->im.pixel[k].ch;
            for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg_async1[j] = pc[j];
        }
    }
    else if ((ac_i%lockin_info->navg)%4 == 0)
    {
        for (k = lockin_info->y0; k < lockin_info->y1; k++)
        {
            pi_avg_async1 = oia_async1->im.pxl[iavg_async1][k].in;
            pc = oi->im.pixel[k].ch;
            for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg_async1[j] += pc[j];
        }
    }
    else if ((ac_i%lockin_info->navg)%4 == 2)
    {
        for (k = lockin_info->y0; k < lockin_info->y1; k++)
        {
            pi_avg_async1 = oia_async1->im.pxl[iavg_async1][k].in;
            pc = oi->im.pixel[k].ch;
            for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg_async1[j] -= pc[j];
        }
    }

    if ((ac_i%lockin_info->navg) == 1)
    {
        for (k = lockin_info->y0; k < lockin_info->y1; k++)
        {
            pi_avg_async2 = oia_async2->im.pxl[iavg_async2][k].in;
            pc = oi->im.pixel[k].ch;
            for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg_async2[j] = pc[j];
        }
    }
    else if ((ac_i%lockin_info->navg)%4 == 1)
    {
        for (k = lockin_info->y0; k < lockin_info->y1; k++)
        {
            pi_avg_async2 = oia_async2->im.pxl[iavg_async2][k].in;
            pc = oi->im.pixel[k].ch;
            for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg_async2[j] += pc[j];
        }
    }
    else if ((ac_i%lockin_info->navg)%4 == 3)
    {	
        for (k = lockin_info->y0; k < lockin_info->y1; k++)
        {
            pi_avg_async2 = oia_async2->im.pxl[iavg_async2][k].in;
            pc = oi->im.pixel[k].ch;
            for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg_async2[j] -= pc[j];
        }
    }

    /* //we create 2 avg image over navg frames with real and imaginary part */
    /* //phase rolling = 0, 90, 180, 270 */

    i_avg1 = (oia1->im.c_f == 0) ? 1 : 0;
    i_avg2 = (oia2->im.c_f == 0) ? 1 : 0;
    //i_avg3 = (oia3->im.c_f == 0) ? 1 : 0;

    if ((ac_i%lockin_info->navg) == 0) lockin_info->ph_start = lockin_info->phase[ci]; 
    if (lockin_info->ph_start == 0)
    {
        if ((ac_i%lockin_info->navg) == 0)
        {
            for (k = lockin_info->y0; k < lockin_info->y1; k++)
            {
                pi_avg1 = oia1->im.pxl[i_avg1][k].in;
                pc = oi->im.pixel[k].ch;
                for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg1[j] = -pc[j];
            }
        }
        else if (((ac_i%lockin_info->navg)%4) == 2)
        {
            for (k = lockin_info->y0; k < lockin_info->y1; k++)
            {
                pi_avg1 = oia1->im.pxl[i_avg1][k].in;
                pc = oi->im.pixel[k].ch;
                for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg1[j] += pc[j];
            }
        }
        else if (((ac_i%lockin_info->navg)%4) == 0)
        {
            for (k = lockin_info->y0; k < lockin_info->y1; k++)
            {
                pi_avg1 = oia1->im.pxl[i_avg1][k].in;
                pc = oi->im.pixel[k].ch;
                for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg1[j] -= pc[j];
            }
        }

        if ((ac_i%lockin_info->navg) == 1)
        {
            for (k = lockin_info->y0; k < lockin_info->y1; k++)
            {
                pi_avg2 = oia2->im.pxl[i_avg2][k].in;
                pc = oi->im.pixel[k].ch;
                for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg2[j] = -pc[j];
            }
        }
        else if (((ac_i%lockin_info->navg)%4) == 3)
        {
            for (k = lockin_info->y0; k < lockin_info->y1; k++)
            {
                pi_avg2 = oia2->im.pxl[i_avg2][k].in;
                pc = oi->im.pixel[k].ch;
                for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg2[j] += pc[j];
            }
        }
        else if (((ac_i%lockin_info->navg)%4) == 1)
        {
            for (k = lockin_info->y0; k < lockin_info->y1; k++)
            {
                pi_avg2 = oia2->im.pxl[i_avg2][k].in;
                pc = oi->im.pixel[k].ch;
                for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg2[j] -= pc[j];
            }
        }
    }

    if (lockin_info->ph_start == 90)
    {

        if ((ac_i%lockin_info->navg) == 0)
        {
            for (k = lockin_info->y0; k < lockin_info->y1; k++)
            {
                pi_avg2 = oia2->im.pxl[i_avg2][k].in;
                pc = oi->im.pixel[k].ch;
                for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg2[j] = -pc[j];
            }
        }
        else if (((ac_i%lockin_info->navg)%4) == 2)
        {
            for (k = lockin_info->y0; k < lockin_info->y1; k++)
            {
                pi_avg2 = oia2->im.pxl[i_avg2][k].in;
                pc = oi->im.pixel[k].ch;
                for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg2[j] += pc[j];
            }
        }
        else if (((ac_i%lockin_info->navg)%4) == 0)
        {
            for (k = lockin_info->y0; k < lockin_info->y1; k++)
            {
                pi_avg2 = oia2->im.pxl[i_avg2][k].in;
                pc = oi->im.pixel[k].ch;
                for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg2[j] -= pc[j];
            }
        }

        if ((ac_i%lockin_info->navg) == 1)
        {
            for (k = lockin_info->y0; k < lockin_info->y1; k++)
            {
                pi_avg1 = oia1->im.pxl[i_avg1][k].in;
                pc = oi->im.pixel[k].ch;
                for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg1[j] = pc[j];
            }
        }
        else if (((ac_i%lockin_info->navg)%4) == 3)
        {
            for (k = lockin_info->y0; k < lockin_info->y1; k++)
            {
                pi_avg1 = oia1->im.pxl[i_avg1][k].in;
                pc = oi->im.pixel[k].ch;
                for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg1[j] -= pc[j];
            }
        }
        else if (((ac_i%lockin_info->navg)%4) == 1)
        {
            for (k = lockin_info->y0; k < lockin_info->y1; k++)
            {
                pi_avg1 = oia1->im.pxl[i_avg1][k].in;
                pc = oi->im.pixel[k].ch;
                for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg1[j] += pc[j];
            }
        }
    }

    if (lockin_info->ph_start == 180)
    {

        if ((ac_i%lockin_info->navg) == 0)
        {
            for (k = lockin_info->y0; k < lockin_info->y1; k++)
            {
                pi_avg1 = oia1->im.pxl[i_avg1][k].in;
                pc = oi->im.pixel[k].ch;
                for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg1[j] = pc[j];
            }
        }
        else if (((ac_i%lockin_info->navg)%4) == 2)
        {
            for (k = lockin_info->y0; k < lockin_info->y1; k++)
            {
                pi_avg1 = oia1->im.pxl[i_avg1][k].in;
                pc = oi->im.pixel[k].ch;
                for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg1[j] -= pc[j];
            }
        }
        else if (((ac_i%lockin_info->navg)%4) == 0)
        {
            for (k = lockin_info->y0; k < lockin_info->y1; k++)
            {
                pi_avg1 = oia1->im.pxl[i_avg1][k].in;
                pc = oi->im.pixel[k].ch;
                for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg1[j] += pc[j];
            }
        }

        if ((ac_i%lockin_info->navg) == 1)
        {
            for (k = lockin_info->y0; k < lockin_info->y1; k++)
            {
                pi_avg2 = oia2->im.pxl[i_avg2][k].in;
                pc = oi->im.pixel[k].ch;
                for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg2[j] = pc[j];
            }
        }
        else if (((ac_i%lockin_info->navg)%4) == 3)
        {
            for (k = lockin_info->y0; k < lockin_info->y1; k++)
            {
                pi_avg2 = oia2->im.pxl[i_avg2][k].in;
                pc = oi->im.pixel[k].ch;
                for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg2[j] -= pc[j];
            }
        }
        else if (((ac_i%lockin_info->navg)%4) == 1)
        {
            for (k = lockin_info->y0; k < lockin_info->y1; k++)
            {
                pi_avg2 = oia2->im.pxl[i_avg2][k].in;
                pc = oi->im.pixel[k].ch;
                for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg2[j] += pc[j];
            }
        }
    }

    if (lockin_info->ph_start == 270)
    {

        if ((ac_i%lockin_info->navg) == 0)
        {
            for (k = lockin_info->y0; k < lockin_info->y1; k++)
            {
                pi_avg2 = oia2->im.pxl[i_avg2][k].in;
                pc = oi->im.pixel[k].ch;
                for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg2[j] = pc[j];
            }
        }
        else if (((ac_i%lockin_info->navg)%4) == 2)
        {
            for (k = lockin_info->y0; k < lockin_info->y1; k++)
            {
                pi_avg2 = oia2->im.pxl[i_avg2][k].in;
                pc = oi->im.pixel[k].ch;
                for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg2[j] -= pc[j];
            }
        }
        else if (((ac_i%lockin_info->navg)%4) == 0)
        {
            for (k = lockin_info->y0; k < lockin_info->y1; k++)
            {
                pi_avg2 = oia2->im.pxl[i_avg2][k].in;
                pc = oi->im.pixel[k].ch;
                for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg2[j] += pc[j];
            }
        }

        if ((ac_i%lockin_info->navg) == 1)
        {
            for (k = lockin_info->y0; k < lockin_info->y1; k++)
            {
                pi_avg1 = oia1->im.pxl[i_avg1][k].in;
                pc = oi->im.pixel[k].ch;
                for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg1[j] = -pc[j];
            }
        }
        else if (((ac_i%lockin_info->navg)%4) == 3)
        {
            for (k = lockin_info->y0; k < lockin_info->y1; k++)
            {
                pi_avg1 = oia1->im.pxl[i_avg1][k].in;
                pc = oi->im.pixel[k].ch;
                for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg1[j] += pc[j];
            }
        }
        else if (((ac_i%lockin_info->navg)%4) == 1)
        {
            for (k = lockin_info->y0; k < lockin_info->y1; k++)
            {
                pi_avg1 = oia1->im.pxl[i_avg1][k].in;
                pc = oi->im.pixel[k].ch;
                for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg1[j] -= pc[j];
            }
        }
    }
    /* if ((ac_i%lockin_info->navg) == (lockin_info->navg - 1)) */
    /*     { */

    /*       for (k = lockin_info->y0; k < lockin_info->y1; k++) */
    /* 	 { */
    /* 	  pi_avg1 = oia1->im.pxl[i_avg1][k].in; */
    /*           pi_avg2 = oia2->im.pxl[i_avg2][k].in; */
    /*           pi_avg3 = oia3->im.pxl[i_avg3][k].in; */
    /* 	  for (j = lockin_info->x0; j < lockin_info->x1; j++)             pi_avg3[j] = pi_avg2[j]*pi_avg2[j] + pi_avg1[j]*pi_avg1[j]; */
    /*          } */
    /*     } */
    if ((ac_i%lockin_info->navg) == (lockin_info->navg - 1))
    {
        switch_frame(oia_async1,iavg_async1);
        switch_frame(oia_async2,iavg_async2);
        switch_frame(oia1,i_avg1);
        switch_frame(oia2,i_avg2);
        // switch_frame(oia3,i_avg3);
        oia_async1->need_to_refresh |= BITMAP_NEED_REFRESH;
        oia_async2->need_to_refresh |= BITMAP_NEED_REFRESH;
        oia1->need_to_refresh |= BITMAP_NEED_REFRESH;
        oia2->need_to_refresh |= BITMAP_NEED_REFRESH;
        // oia3->need_to_refresh |= BITMAP_NEED_REFRESH;
    }

    if (gt->n_b > 0)
    {

        cl = gt->cl;                   // cross arm length	
        cl2 = cl/2;
        cw = gt->cw;                   // cross arm width	
        xi = gt->xi;	yi = gt->yi;     // these are temporary int buffers for averaged profile
        xf = gt->xf;	yf = gt->yf;     // these are temporary float buffers for averaged profile
        dis_filter = gt->dis_filter;   // this is the filter description

        // loop scanning beads tracked
        for (c_b = 0; c_b < gt->n_b; c_b++)
        {
            bt = gt->bd[c_b];
            proceed_bead(bt, c_b, gt, imb, oi, oia_async1, oia_async2, oia1, oia2);
            //bt->z[ci] = gt->obj_pos[ci] - bt->z[ci]; // new
            //bt->z_avg += bt->z[ci];
            //bt->iz_avg++;
            //if(bt->iz_avg >= 10)
            // {
            //    bt->z_avgd = bt->z_avg/bt->iz_avg;
            //    bt->z_avg = 0;
            //    bt->iz_avg = 0;
            //  }
        }
    }

    //image treatment end////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    /*  if (gt->n_b > 0) */
    /*     { */
    /*       cl = gt->cl;                   // cross arm length	 */
    /*       cl2 = cl/2; */
    /*       cw = gt->cw;                   // cross arm width	 */
    /*       xi = gt->xi;	yi = gt->yi;     // these are temporary int buffers for averaged profile */
    /*       xf = gt->xf;	yf = gt->yf;     // these are temporary float buffers for averaged profile */
    /*       dis_filter = gt->dis_filter;   // this is the filter description */

    /*       // loop scanning beads tracked */
    /*       for (c_b = 0; c_b < gt->n_b; c_b++) */
    /* 	{ */
    /* 	  bt = gt->bd[c_b]; */
    /* 	  proceed_bead(bt, c_b, gt, imb, oi); */
    /* 	  bt->z[ci] = gt->obj_pos[ci] - bt->z[ci]; // new */
    /* 	  bt->z_avg += bt->z[ci]; */
    /* 	  bt->iz_avg++; */
    /* 	  if(bt->iz_avg >= 10) */
    /* 	    { */
    /* 	      bt->z_avgd = bt->z_avg/bt->iz_avg; */
    /* 	      bt->z_avg = 0; */
    /* 	      bt->iz_avg = 0; */
    /* 	    } */
    /* 	} */
    /*     } */
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////::
    lockin_info->imdt[ci] = my_uclock() - dt;
    lockin_info->ac_i = ac_i;

    return 0; 
}
END_OF_FUNCTION(do_at_all_image)



MENU *phase_in_live_video_image_menu(void)
{
    static MENU mn[32];


    if (mn[0].text != NULL)	return mn;
    add_item_to_menu(mn,"frame 0->1", adj_toogle_frame ,NULL,MENU_INDEX(0) ,NULL);
    add_item_to_menu(mn,"frame 1->0", adj_toogle_frame ,NULL,MENU_INDEX(1) ,NULL);
    add_item_to_menu(mn,"navg",adj_navg ,NULL,MENU_INDEX(2) ,NULL);
    //add_item_to_menu(mn,"refold phase",refold_phase ,NULL,0 ,NULL);
    return mn;
}


int get_present_image_nb(void)
{
    return (lockin_info != NULL) ?  lockin_info->imi[lockin_info->c_i] : -1;
}                                 


g_lockin *creating_lockin_info(void)
{
    O_p *op;
    d_s *ds;
    O_i *oi_rel, *oi_im, *oi1, *oi2;//*oi
    O_i *oi_sync;


    if (lockin_info == NULL)
    {
        lockin_info = (g_lockin*)calloc(1,sizeof(g_lockin));
        if (lockin_info == NULL) return win_printf_ptr("cannot alloc lockin info!");	

        lockin_info->im_focus_check = 16;
        lockin_info->temp_cycle = lockin_info->itemp_mes = 0;
        lockin_info->T0 = lockin_info->T1 = lockin_info->T2 = lockin_info->T3 = lockin_info->I0 = 0;   
        lockin_info->temp_mes[0] = 0;
        lockin_info->user_lockin_action = NULL;
        lockin_info->cl = -1;



        lockin_info->m_b = 16;	    
        lockin_info->n_b = lockin_info->c_b = 0;
        lockin_info->n_bead_display = -1;
        lockin_info->bd = (b_track**)calloc(lockin_info->m_b,sizeof(b_track*));
        if (lockin_info->bd == NULL) return win_printf_ptr("cannot alloc track info!");	



        // plots to check lockin
        op = pr_LIVE_VIDEO->o_p[0];
        set_op_filename(op, "Timing.gr");
        ds = op->dat[0]; // the first data set was already created
        set_ds_source(ds, "Timing rolling buffer");
        if ((ds = create_and_attach_one_ds(op, LOCKIN_BUFFER_SIZE, LOCKIN_BUFFER_SIZE, 0)) == NULL)
            return win_printf_ptr("I can't create plot !");
        set_ds_source(ds, "Period rolling buffer");
        if ((ds = create_and_attach_one_ds(op, LOCKIN_BUFFER_SIZE, LOCKIN_BUFFER_SIZE, 0)) == NULL)
            return win_printf_ptr("I can't create plot !");
        set_ds_source(ds, "Timer rolling buffer");
        op->op_idle_action = timing_rolling_buffer_idle_action;
        //      op->op_post_display = general_op_post_display;
        set_plot_x_title(op, "Time");
        set_plot_y_title(op, "{\\color{yellow} Lockining duration} {\\color{lightgreen} Frame duration} (ms)");

        create_attach_select_x_un_to_op(op, IS_SECOND, 0
                                        ,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");


        //plot to check phase in image thread
        if ((op = create_and_attach_one_plot(pr_LIVE_VIDEO, 50*LOCKIN_BUFFER_SIZE/3, 50*LOCKIN_BUFFER_SIZE/3, 0)) == NULL)
            return win_printf_ptr("I can't create plot !");
        set_op_filename(op, "Lock_phase in cvb thread.gr");
        ds = op->dat[0]; // the first data set was already created
        set_ds_source(ds, "Phase expected");
        op->op_idle_action = phase_in_imagethread_rolling_buffer_idle_action;
        set_plot_x_title(op, "Time");
        //set_plot_y_title(op, "{\\color{yellow} Lockining duration} {\\color{lightgreen} Frame duration} (ms)");			
        // create_attach_select_x_un_to_op(op, IS_SECOND, 0
        //		       ,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");



        // plots to check phase index in phase-timer
        if ((op = create_and_attach_one_plot(pr_LIVE_VIDEO, 50*LOCKIN_BUFFER_SIZE/3, 50*LOCKIN_BUFFER_SIZE/3, 0)) == NULL)
            return win_printf_ptr("I can't create plot !");
        set_op_filename(op, "Lock_phase.gr");
        ds = op->dat[0]; // the first data set was already created
        set_ds_source(ds, "Modulation phase measured");
        if ((ds = create_and_attach_one_ds(op, 50*LOCKIN_BUFFER_SIZE/3, 50*LOCKIN_BUFFER_SIZE/3, 0)) == NULL)
            return win_printf_ptr("I can't create plot !");
        set_ds_source(ds, "Modulation phase simulated");
        if ((ds = create_and_attach_one_ds(op, 50*LOCKIN_BUFFER_SIZE/3, 50*LOCKIN_BUFFER_SIZE/3, 0)) == NULL)
            return win_printf_ptr("I can't create plot !");
        set_ds_source(ds, "Error");

        op->op_idle_action = phase_rolling_buffer_idle_action;
        set_plot_x_title(op, "Time");
        set_plot_y_title(op, "{\\color{yellow} Measured}{\\color{lightgreen} Simulated} phase");
        //create_attach_select_x_un_to_op(op, IS_SECOND, 0 ,1, 0, 0, "s");



        /*  if ((oi = create_and_attach_movie_to_imr(imr_LIVE_VIDEO, oi_LIVE_VIDEO->im.nx, oi_LIVE_VIDEO->im.ny, IS_INT_IMAGE, 2)) == NULL) */
        /*      	return win_printf_ptr("I can't create image !"); */
        /*       set_oi_filename(oi, "Im_avg 2 phases.gr"); */
        /*       uns_oi_2_oi(oi,oi_LIVE_VIDEO); */
        /*       oi->oi_mouse_action = live_video_oi_mouse_action; */
        /*       oi->oi_got_mouse = live_video_oi_got_mouse; */
        /*       oi->oi_post_display = live_video_oi_post_display; */

        if ((oi1 = create_and_attach_movie_to_imr(imr_LIVE_VIDEO, oi_LIVE_VIDEO->im.nx, oi_LIVE_VIDEO->im.ny, IS_INT_IMAGE, 2)) == NULL)
            return win_printf_ptr("I can't create image !");
        set_oi_filename(oi1, "Im_avg1 async.gr");
        uns_oi_2_oi(oi1,oi_LIVE_VIDEO);
        oi1->oi_mouse_action = live_video_oi_mouse_action;
        oi1->oi_got_mouse = live_video_oi_got_mouse;
        oi1->oi_post_display = live_video_oi_post_display;
        if ((oi2 = create_and_attach_movie_to_imr(imr_LIVE_VIDEO, oi_LIVE_VIDEO->im.nx, oi_LIVE_VIDEO->im.ny, IS_INT_IMAGE, 2)) == NULL)
            return win_printf_ptr("I can't create image !");
        set_oi_filename(oi2, "Im_avg2 async.gr");
        uns_oi_2_oi(oi2,oi_LIVE_VIDEO);
        oi2->oi_mouse_action = live_video_oi_mouse_action;
        oi2->oi_got_mouse = live_video_oi_got_mouse;
        oi2->oi_post_display = live_video_oi_post_display;


        /*  if ((oi0 = create_and_attach_movie_to_imr(imr_LIVE_VIDEO, oi_LIVE_VIDEO->im.nx, oi_LIVE_VIDEO->im.ny, IS_INT_IMAGE, 2)) == NULL) */
        /*      	return win_printf_ptr("I can't create image !"); */
        /*       set_oi_filename(oi0, "Mod Im_avg on 2 phases.gr"); */
        /*       uns_oi_2_oi(oi0,oi_LIVE_VIDEO); */
        /*       oi0->oi_mouse_action = live_video_oi_mouse_action; */
        /*       oi0->oi_got_mouse = live_video_oi_got_mouse; */
        /*       oi0->oi_post_display = live_video_oi_post_display; */


        if ((oi_rel = create_and_attach_movie_to_imr(imr_LIVE_VIDEO, oi_LIVE_VIDEO->im.nx, oi_LIVE_VIDEO->im.ny, IS_INT_IMAGE, 2)) == NULL)
            return win_printf_ptr("I can't create image !");
        set_oi_filename(oi_rel, "Im_avg_real.gr");
        uns_oi_2_oi(oi_rel,oi_LIVE_VIDEO);
        oi_rel->oi_mouse_action = live_video_oi_mouse_action;
        oi_rel->oi_got_mouse = live_video_oi_got_mouse;
        oi_rel->oi_post_display = live_video_oi_post_display;

        if ((oi_im = create_and_attach_movie_to_imr(imr_LIVE_VIDEO, oi_LIVE_VIDEO->im.nx, oi_LIVE_VIDEO->im.ny, IS_INT_IMAGE, 2)) == NULL)
            return win_printf_ptr("I can't create image !");
        set_oi_filename(oi_im, "Im_avg_im.gr");
        uns_oi_2_oi(oi_im,oi_LIVE_VIDEO);
        oi_im->oi_mouse_action = live_video_oi_mouse_action;
        oi_im->oi_got_mouse = live_video_oi_got_mouse;
        oi_im->oi_post_display = live_video_oi_post_display;

        /* if ((oi_mod = create_and_attach_movie_to_imr(imr_LIVE_VIDEO, oi_LIVE_VIDEO->im.nx, oi_LIVE_VIDEO->im.ny, IS_INT_IMAGE, 2)) == NULL) */
        /*       	return win_printf_ptr("I can't create image !"); */
        /*       set_oi_filename(oi_mod, "Im_avg_mod.gr"); */
        /*       uns_oi_2_oi(oi_mod,oi_LIVE_VIDEO); */
        /*       oi_mod->oi_got_mouse = live_video_oi_got_mouse; */
        /*       oi_mod->oi_post_display = live_video_oi_post_display; */
        /*       oi_mod->oi_mouse_action = live_video_oi_mouse_action; */

        // plot and movie for phase identification with black frame
        if ((oi_sync = create_and_attach_movie_to_imr(imr_LIVE_VIDEO,oi_LIVE_VIDEO->im.nx , oi_LIVE_VIDEO->im.ny, IS_INT_IMAGE,256)) == NULL)
            return win_printf_ptr("I can't create image !");
        oi_sync->oi_mouse_action = live_video_oi_mouse_action;
        if ((op = create_and_attach_op_to_oi(oi_sync, 256, 256, 0, 0)) == NULL)
            return win_printf_ptr("I can't create op !");
        lockin_info->oid = oi_sync;
        oi_sync->oi_idle_action = oi_avg_idle_action;
        map_pixel_ratio_of_image_and_screen(oi_sync, 1, 1);

        //lockin_info->oi_avg = oi;
        lockin_info->oi_avg_async_1 = oi1;
        lockin_info->oi_avg_async_2 = oi2;
        lockin_info->oi_avg1 = oi_rel;
        lockin_info->oi_avg2 = oi_im;
        //lockin_info->oi_avg3 = oi_mod;

        lockin_info->x0 =0; // oi_LIVE_VIDEO->im.nx/4;
        lockin_info->y0 = 0;//oi_LIVE_VIDEO->im.ny/4;
        lockin_info->x1 = oi_LIVE_VIDEO->im.nx; //3*oi_LIVE_VIDEO->im.nx/4;
        lockin_info->y1 = oi_LIVE_VIDEO->im.ny; //3*oi_LIVE_VIDEO->im.ny/4;

        //oi->oi_idle_action = oi_avg_idle_action;
        oi1->oi_idle_action = oi_avg_idle_action;
        oi2->oi_idle_action = oi_avg_idle_action;
        oi_rel->oi_idle_action = oi_avg_idle_action;
        oi_im->oi_idle_action = oi_avg_idle_action;
        oi_sync->oi_idle_action = oi_avg_idle_action;
        //oi_mod->oi_idle_action = oi_avg_idle_action;

        map_pixel_ratio_of_image_and_screen(oi1, 1, 1);
        map_pixel_ratio_of_image_and_screen(oi2, 1, 1);
        // map_pixel_ratio_of_image_and_screen(oi0, 1, 1);
        map_pixel_ratio_of_image_and_screen(oi_rel, 1, 1);
        map_pixel_ratio_of_image_and_screen(oi_im, 1, 1);
        //map_pixel_ratio_of_image_and_screen(oi_mod, 1, 1);

        lockin_info->m_i = lockin_info->n_i = LOCKIN_BUFFER_SIZE;
        lockin_info->c_i = lockin_info->ac_i = 0;
        lockin_info->navg = 8;
        lockin_info->per_t = 66.7965;

        LOCK_FUNCTION(do_at_all_image);
        LOCK_VARIABLE(lockin_info);

        ph_lock.n_i = 50*LOCKIN_BUFFER_SIZE/3;
        ph_lock.ac_i = ph_lock.c_i = 0;
        ph_lock.peri = (int)66.7965;
        //ph_lock.n_phi = (int)(128 * 66.7965);
        // test 6hz
        ph_lock.n_phi = (int)(128 * 66.7965);
        LOCK_FUNCTION(phase_timer);
        LOCK_VARIABLE(ph_lock);
        install_int(phase_timer,1);

        bid.param = (void*)lockin_info;
        bid.to_do = NULL;
        bid.timer_do = do_at_all_image;

        add_image_treat_menu_item ( "Avg", NULL, phase_in_live_video_image_menu(), 0, NULL);
        //add_plot_treat_menu_item ( "refold phase",refold_phase ,NULL,0 ,NULL);
        //add_plot_treat_menu_item ("simul phase",simul_phase ,NULL,0 ,NULL);
        //add_plot_treat_menu_item ("slide phase",slidding_phase ,NULL,0 ,NULL);
        //add_plot_treat_menu_item ("adjust pid",set_alpha ,NULL,0 ,NULL);
        add_plot_treat_menu_item ("adjust vco",adj_vco ,NULL,0 ,NULL);
        add_plot_treat_menu_item("Switch bead",NULL,bead_active_menu,0,NULL);
        //add_plot_treat_menu_item ("change bead",prepare_bead_menu ,NULL,0 ,NULL);

    }
    return lockin_info;
}


int init_live_video_info(void)
{
    if (lockin_info == NULL) 
    {
        lockin_info = creating_lockin_info();
        m_action = 256;
        n_action = 0;
        action_pending = (f_action*)calloc(m_action,sizeof(f_action));
        if (action_pending == NULL)
            win_printf_OK("Could not allocate action_pendind!");
        phaseindex_main(0, NULL);
        do_follow_bead_in_x_y(0);
    }
    return 0;
}




#endif
