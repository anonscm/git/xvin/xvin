/** \file MCL_FOCUS.c
    \brief Plug-in program for pifoc control in Xvin.
    \author JF
*/
#ifndef _PIFOC_USB_3101_C_
#define _PIFOC_USB_3101_C_

#include "allegro.h"
#include "winalleg.h"
#include <windowsx.h>
#include "ctype.h"
#include "xvin.h"

# include "../../cfg_file/Pico_cfg.h"

#undef NOCLEAR
#include "cbw.h"
#include "Pifoc_usb_3101.h"
#define MAXNUMDEVS 100

#define BUILDING_PLUGINS_DLL

int _has_focus_memory(void)
{return 0;}

int _set_light(float z)
{(void)z;return D_O_K;}

float _get_light(void)
{ return 0.5;}

int _set_temperature(float z)
{ (void)z;return D_O_K;}

float _get_temperature(void)
{return 0.5;}

int  _init_focus_OK(void)
{
    /* Variable Declarations */
    int i;
    int BoardNum = 0;
    int UDStat = 0;
	float    RevLevel = (float)CURRENTREVNUM;
	int numberOfDevices = MAXNUMDEVS;
	DaqDeviceDescriptor inventory[MAXNUMDEVS];
	DaqDeviceDescriptor DeviceDescriptor;
    char message[4096] = {0};
	if(updating_menu_state != 0)	return D_O_K;


	/* Declare UL Revision Level */
	UDStat = cbDeclareRevision(&RevLevel);


    /* Initiate error handling
       Parameters:
       PRINTALL :all warnings and errors encountered will be printed
       DONTSTOP :program will continue even if error occurs.
       Note that STOPALL and STOPFATAL are only effective in 
       Windows applications, not Console applications. 
    */
    cbErrHandling (PRINTALL, DONTSTOP);

	/* Ignore InstaCal device discovery */
	cbIgnoreInstaCal();

	/* Discover DAQ devices with cbGetDaqDeviceInventory()
       Parameters:
       InterfaceType   :interface type of DAQ devices to be discovered
       inventory[]		:array for the discovered DAQ devices
       numberOfDevices	:number of DAQ devices discovered */

	UDStat = cbGetDaqDeviceInventory(ANY_IFC, inventory, &numberOfDevices);

	if(numberOfDevices <= 0)
        {
            win_printf("no MCC device found!"); 
        }
    else
        {
            snprintf (message,sizeof(message),"Select a DAQ device(s) amon %d.\n", numberOfDevices);

            for(BoardNum = 0; BoardNum < numberOfDevices; BoardNum++)
                {
                    DeviceDescriptor = inventory[BoardNum];
                    snprintf (message+strlen(message),sizeof(message)+strlen(message),
                              "%%%c     Device Name: %s\n", (BoardNum == 0)?'R':'r',DeviceDescriptor.ProductName);
                    snprintf (message+strlen(message),sizeof(message)+strlen(message),
                              "         Device Identifier: %s\n", DeviceDescriptor.UniqueID);
                    snprintf (message+strlen(message),sizeof(message)+strlen(message),
                              "         Assigned Board Number: %d\n", BoardNum);

                    /* Creates a device object within the Universal Library and 
                       assign a board number to the specified DAQ device with cbCreateDaqDevice()

                       Parameters:
                       BoardNum			: board number to be assigned to the specified DAQ device
                       DeviceDescriptor	: device descriptor of the DAQ device */

                    UDStat = cbCreateDaqDevice(BoardNum, DeviceDescriptor);
                }
            i = win_scanf(message,&BoardNum);
            if (i == WIN_CANCEL) return 0;
            /* Flash the LED of the selected device */

            if(BoardNum >=0 && BoardNum < numberOfDevices)
                {
                    /* Flash the LED of the specified DAQ device with cbFlashLED()
                       Parameters:
                       BoardNum			: board number assigned to the DAQ  */
                    UDStat = cbFlashLED(BoardNum);
                }
            else
                {
                    return win_printf_OK ("Invalid device number %d\n",BoardNum);
                }
        }

    //for(BoardNum = 0; BoardNum < numberOfDevices; BoardNum++)
    //{
    /* Release resources associated with the specified board number within the Universal Library with cbReleaseDaqDevice()
       Parameters:
       BoardNum			: board number assigned to the DAQ  */

	//		UDStat = cbReleaseDaqDevice(BoardNum);
	//	}
	//init_parameters();
	return 0;
}

float _read_Z_value_accurate_in_micron(void)
{
  return PIFOC_position;
}

int _read_Z_value(void)
{

  return (int)(_read_Z_value_accurate_in_micron());//ask meaninig 10*
}

float _read_Z_value_OK(int *error) /* in microns */
{
    (void)error;
  return _read_Z_value_accurate_in_micron();
}

float _read_last_Z_value(void) /* in microns */
{
  return PIFOC_position;
}

int _set_Z_step(float z)  /* in micron */
{
	PIFOC_Z_step = z;
	return 0;
}

int _set_Z_value(float z)  /* in microns */
{
    unsigned short *datavalue;

    datavalue=(unsigned short *)malloc(sizeof(unsigned short));
    *datavalue=0;

    //if( (z > Pico_param.focus_param.Focus_max_expansion) || (z<0) ) return D_O_K;
    if( (z > 250) || (z<0) ) return D_O_K;

	//cbFromEngUnits(parameters.BoardNum, parameters.range_out,(z*10/Pico_param.focus_param.Focus_max_expansion), datavalue);
    cbFromEngUnits(parameters.BoardNum, parameters.range_out,(z*10/250), datavalue);
	        my_set_window_title("z is %f, datavalue %d",z,*datavalue);
	if(cbAOut(parameters.BoardNum,parameters.LowChan_out,parameters.range_out,*datavalue)==0)
        PIFOC_position = z;
    else win_printf("Error in setting z");

  return D_O_K;
}


int _set_Z_value_PIFOC(void)
{
  float goal = 0;

  if(updating_menu_state != 0)	return D_O_K;

  win_scanf("Where do you want to go ?%f",goal);

  _set_Z_value(goal);
  return D_O_K;
}


int _set_Z_obj_accurate(float zstart)
{
  return _set_Z_value(zstart);
}

int _set_Z_value_OK(float z)
{
  return _set_Z_value(z);
}

int _inc_Z_value(int step)  /* in micron */
{
  _set_Z_value(PIFOC_position + PIFOC_Z_step*step);
  return 0;
}

int _small_inc_Z_value(int up)
{
  int nstep;

  _set_Z_step(0.1);
  nstep = (up>=0)? 1:-1;
  _set_Z_value(PIFOC_position + PIFOC_Z_step*nstep);

  return 0;
}

int _big_inc_Z_value(int up)
{
  int nstep;

  _set_Z_step(0.1);
  nstep = (up>=0)? 10:-10;
  _set_Z_value(PIFOC_position + PIFOC_Z_step*nstep);

  return 0;
}


int do_set_out(void)
{
    static float data=0;

    if(updating_menu_state != 0)	return D_O_K;

    win_scanf("Value to write?%6f (0,+10V)",&data);
    _set_Z_value(data);

    return 0;
}

int do_get_in(void)
{

    if(updating_menu_state != 0)	return D_O_K;

    win_printf("On est de retour %6f",PIFOC_position);

    return 0;
}


MENU *Pifoc_usb_3101_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn, "&Set point on line 0",  do_set_out,      NULL,0,NULL);
    add_item_to_menu(mn, "Read &Single data",    do_get_in,    	   NULL,0,NULL);
	add_item_to_menu(mn,"Read Objective position", _read_Z_value,  NULL,0,NULL);
	add_item_to_menu(mn,"Set Objective position",_set_Z_value_PIFOC, NULL,0,NULL);
    add_item_to_menu(mn, "Initialize", _init_focus_OK,    	       NULL,0,NULL);
	return mn;
}

int Pifoc_usb_3101_main (int argc, char **argv)
{
    (void)argc;
    (void)argv;
  _init_focus_OK();
  win_printf("Hello, main");
  add_plot_treat_menu_item ( "PIFOC usb 3101", NULL, Pifoc_usb_3101_plot_menu(), 0, NULL);
  add_image_treat_menu_item ( "PIFOC usb 3101", NULL, Pifoc_usb_3101_plot_menu(), 0, NULL);
  return 0;
}

int	Pifoc_usb_3101_unload(int argc, char **argv)
{
    (void)argc;
    (void)argv;
	remove_item_to_menu(plot_treat_menu, "PIFOC usb 3101", NULL, NULL);
	return D_O_K;
}


int init_parameters(void)
{
                             parameters.HighChan_in=1;
                             parameters.BoardNum=0;
                             parameters.HighChan_out=1;
                             parameters.LowChan_in=1;
                             parameters.LowChan_out=1;
                             parameters.rate_in=10000;
                             parameters.rate_out=1000;
                             parameters.range_in=UNI10VOLTS;
                             parameters.range_out=UNI10VOLTS;
                             parameters.trigger=TRIGABOVE;
                             parameters.count_in=8192;
                             parameters.options_in=BACKGROUND+CONTINUOUS;
                             parameters.options_out=BACKGROUND+CONTINUOUS;
                             parameters.count_out=8192;
                             parameters.HighThreshold=5.0;
                             parameters.LowThreshold=0;
                             //win_printf("Acquisition parameters are set to default value");
                             return 0;
}

#endif
