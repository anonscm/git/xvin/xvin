#include "gpu_track_utils.h"
#include "cpu_track_utils.h"
#include "../../opencl_utils/cl_utils.h"
#include "track_beads.h"
#include "ltpv.h"
#include "debug_tools.h"
#include "../timer_thread.h"
#include "struct_convert.h"
#include "xvin.h"

bool platform_change_requested = false;
bool waiting_for_platform_change = true;

int g_tracking_device = TRACKING_DISABLE;
int g_preferred_calibration_type = XV_CALIB_TYPE_CUSTOM_IMG;
int g_update_reference_prof = false;


bool opencl_is_init = false;

bool is_gpu_tracking_enable(void)
{
    return (g_tracking_device & (TRACKING_DEVICE_CPU | TRACKING_DEVICE_GPU));
}
bool is_normal_tracking_enable(void)
{
    return g_tracking_device & TRACKING_DISABLE;
}
int get_tracking_device(void)
{
    return g_tracking_device;
}

int set_tracking_device(int tracking_device)
{
    g_tracking_device = tracking_device;
    return 0;
}

void proceed_beads_gpu(O_i *oi, g_track *gt, int ci)
{
    static cpu_crosses *crosses = NULL;
    cpu_image *img = NULL;
    cl_int err;


    if (!opencl_is_init)
    {
        init_opencl_last_config();
        //debug_init_plot();
        opencl_is_init = true;
    }

    if (platform_change_requested)
    {
        gpu_tracking_reset();
        platform_change_requested = false;
        waiting_for_platform_change = true;

        while (waiting_for_platform_change)
        {
            psleep(10);
        }
    }

    //LTPV(psleep(300), "sleeping time", 2);
    LTPV(trackbead_from_pico(oi, gt, &img, &crosses, g_preferred_calibration_type), "convert struct", 2);


    if (crosses->parameter_has_changed)
    {
        cpu_image_free(img);
        cpu_crosses_free(crosses);
        crosses = NULL;
        gpu_tracking_reset();

        LTPV(trackbead_from_pico(oi, gt, &img, &crosses, g_preferred_calibration_type), "convert struct", 2);
        crosses->parameter_has_changed = false;
    }

    // update if ask by the converter or the user
    crosses->update_reference_profile |= g_update_reference_prof;
    g_update_reference_prof = false;

    LTPV(track_beads(img, crosses), "trackbeads", 2);
    LTPV(cpu_to_pico(crosses, gt, ci), "convert back struct", 2);
    cpu_image_free(img);
}

void track_beads(cpu_image *image, cpu_crosses *crosses)
{
    cl_float min_c;
    cl_float min_phi;

    //printf("%d\n", crosses->calib_data->height);
    if (crosses->size > 0)
    {
        //init beads
        LTPV(cpu_create_beads(image, crosses), "create bead", 1); // creating the bead for the current frame.

        //#define OPENCL_TRACK_CPU
        if (g_tracking_device & TRACKING_DEVICE_CPU)
        {
            //printf("bandpass_width%d, bandpass_center%d\n", crosses->bandpass_width, crosses->bandpass_center);

            LTPV(cpu_fill_profiles_xy(image, crosses), "FILL XY", 1);
            LTPV(cpu_track_xy(crosses), "TRACK XY", 1);
            LTPV(cpu_fill_profile_z(image, crosses), "FILL Z", 1);

            LTPV(cpu_track_z(crosses, &min_c, &min_phi), "TRACK Z", 1);
        }
        else
        {


            if (gpu_needs_to_init())
            {
                LTPV(gpu_init_kernels(crosses), "INIT KERNEL", 1);
            }

            gpu_event_driven_memory *gpu_profs = NULL;
            gpu_event_driven_memory *gpu_beads_pos = NULL;
            gpu_event_driven_memory *gpu_beads_z_pos = NULL;

           my_set_window_title(crosses->calib_type == XV_CALIB_TYPE_CUSTOM_IMG ? (char *) "Using calibrated images" :
                             crosses->calib_type == XV_CALIB_TYPE_GENERIC_IMG ? (char *) "Using generic calib image" :
                             (char *) "Using Reference profile");

            LTPV(gpu_init_mem(&gpu_profs, &gpu_beads_pos, &gpu_beads_z_pos, crosses->beads_pos, crosses->length, crosses->size),
                 "INIT MEM", 1);

            //for (int i = 0; i < crosses->size ; i++)
            //{
            //    printf("bead%d : (%f, %f)\n", i,crosses->beads_pos[i].x, crosses->beads_pos[i].y);
            //}
            //LTPV(cpu_fill_profiles_xy(image, crosses, crosses_count), "FILL XY", 1);
            LTPV(gpu_fill_profiles_xy(image, crosses->size, gpu_profs, gpu_beads_pos), "FILL XY", 1);

            //debug_profiles_xy_to_gpu(gpu_profs, crosses, crosses_count, crosses[0]->length);
            //debug_profiles_xy_to_cpu(gpu_profs, crosses, crosses->length);
            //output_debug_profiles(crosses, crosses->cross_data[0]->bead->avg_x_profile, 1);

            LTPV(gpu_track_xy(crosses->size, gpu_profs, gpu_beads_pos, NULL), "track xy", 1);
            //LTPV(cpu_track_xy(crosses), "track xy", 1);

            //debug_xy_pos_to_cpu(gpu_beads_pos, crosses);

            //debug_xy_pos_to_gpu(gpu_beads_pos, crosses);

            //LTPV(cpu_fill_profile_z(image, crosses), "FILL Z", 1);

            //if (crosses->cross_data[0]->bead) debug_plot_profile(0, crosses->cross_data[0]->bead->radial_z_profile,crosses->cross_data[0]->bead->radial_profile_size );

            LTPV(gpu_fill_profile_z(image, crosses->size, gpu_profs, gpu_beads_pos), "fill profile z", 1);

            //if (crosses[0]->bead) debug_plot_gpu_profile(1, gpu_profs,crosses[0]->bead->radial_profile_size);

            //debug_xy_pos_to_cpu(gpu_beads_pos, crosses);
            debug_profiles_z_to_cpu(crosses, gpu_profs, crosses->length);

            //if (crosses[0]->bead)
            //{
            //    debug_plot_profile(1, crosses[0]->bead->radial_z_profile, crosses[0]->length);
            //}

            //debug_xy_pos_to_gpu(gpu_beads_pos, crosses, crosses_count);
            //debug_profiles_z_to_gpu(crosses, crosses_count, gpu_profs, crosses[0]->length);

            // LTPV(cpu_track_z(crosses, &min_c, &min_phi), "TRACK Z", 1);

            LTPV(gpu_track_z(crosses, gpu_profs, gpu_beads_z_pos), "track z", 1);


            debug_xy_pos_to_cpu(gpu_beads_pos, crosses);

            gpu_event_driven_memory_free(gpu_profs);
            gpu_profs = NULL;

            gpu_event_driven_memory_free(gpu_beads_pos);
            gpu_beads_pos = NULL;

            gpu_event_driven_memory_free(gpu_beads_z_pos);
            gpu_beads_pos = NULL;
        }
    }
}

int trackBead_gpu_main(int argc, char **argv)
{
    (void) argc;
    (void) argv;

    add_plot_treat_menu_item("trackBead GPU", NULL, trackBead_gpu_plot_menu(), 0, NULL);
    add_image_treat_menu_item("trackBead GPU", NULL, trackBead_gpu_plot_menu(), 0, NULL);
    return D_O_K;
}

int trackBead_gpu_unload(int argc, char **argv)
{
    (void) argc;
    (void) argv;
    remove_item_to_menu(plot_treat_menu, "trackBead GPU", NULL, NULL);
    remove_item_to_menu(image_treat_menu, "trackBead GPU", NULL, NULL);
    return D_O_K;
}

MENU *trackBead_gpu_plot_menu(void)
{

    static MENU mn[32];

    if (mn[0].text != NULL) return mn;

    //do_load_fasta_file();
    //do_load_oligo_file();
    //do_import_from_sequence();
    //do_fingerprint_load();
    //do_fingerprint_load();
    //do_fingerprint_multiple_match();
    //do_fingerprint_search();
    //do_fingerprint_efficency_by_size();
    //do_matchdata_load();
    //do_fingerprint_view();

    add_item_to_menu(mn, "Change Tracking Compute Method", do_change_tracking_device, NULL, 0, NULL);
    add_item_to_menu(mn, "Change OpenCL Platform", do_change_cl_plateform, NULL, 0, NULL);
    add_item_to_menu(mn, "Set current image as ref profile", do_set_current_as_reference_calibration, NULL, 0, NULL);

    //do_fingerprint_load();
    //do_fingerprint_efficency_by_size();

    return mn;

}

int do_change_tracking_device(void)
{

    int new_tracking_device = log(g_tracking_device) / log(2) - 1;

    if ((g_tracking_device & TRACKING_BOTH) == TRACKING_BOTH)
    {
        new_tracking_device = 3;
    }

    if (updating_menu_state != 0)
    {
        active_menu->flags &= ~D_DISABLED;
        return D_O_K;
    }

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("Change The method executing tracking calculus");
    }

    int allegro_win_return = win_scanf("%R Compute using GPU\n"
                                       "%r Compute using simplify CPU\n"
                                       "%r switch to normal tracking\n"
                                       "%r both", &new_tracking_device);

    if (allegro_win_return == CANCEL)
        return 0;

    if (new_tracking_device == 3)
    {
        new_tracking_device = TRACKING_BOTH;
    }
    else
    {
        new_tracking_device = pow(2, new_tracking_device + 1);
    }

    if (new_tracking_device & TRACKING_DEVICE_GPU)
        do_choose_calibration_method();

    g_tracking_device = new_tracking_device;

    return 0;
}

int do_change_cl_plateform(void)
{
    bool is_tracking = g_tracking_device & TRACKING_DEVICE_GPU;

    if (updating_menu_state != 0)
    {
        active_menu->flags &= ~D_DISABLED;
        return D_O_K;
    }

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("Change OpenCL Plateform");
    }

    // false mutex : not robust, but only solution in xvin : several thread library are use together
    if (is_tracking)
    {
        platform_change_requested = true;

        while (!waiting_for_platform_change)
        {
            psleep(10);
        }
    }

    if (do_choose_plateform() == D_O_K)
        gpu_tracking_reset();


    if (is_tracking)
    {
        waiting_for_platform_change = false;
        platform_change_requested = false;
    }

    return D_O_K;
}


# ifdef GPU_CUSTOM_PARAMETER
void do_choose_gpu_params(void)
{
    bool is_tracking = g_tracking_device & TRACKING_DEVICE_GPU;
    cl_int use_custom_params = 0;
    cl_int length, width;  // cross size;
    cl_int max_meaningful_radius;
    cl_int bandpass_center;
    cl_int bandpass_width;
    int allegro_win_return =
        win_scanf("GPU handle only one configuration for all beads,\n individual configuration won't be used\n"
                  "Choose global parameters :\n"
                  "%R use first bead parameters\n"
                  "%r use custom parameters :\n"
                  "    crosses length %d\n"
                  "    crosses width %d\n"
                  "    max meaningful radius %d\n"
                  "    radial filter bandpass:\n"
                  "        width %d\n"
                  "        center %d\n",
                  &use_custom_params, &length, &width, &max_meaningful_radius, &bandpass_width, &bandpass_center);

    // false mutex : not robust, but only solution in xvin : several thread library are use together
    if (is_tracking)
    {

        platform_change_requested = true;

        while (!waiting_for_platform_change)
        {
            psleep(10);
        }
    }

    //TODO : add logic.

    if (is_tracking)
    {
        waiting_for_platform_change = false;
        platform_change_requested = false;
    }

}
#endif

int do_choose_calibration_method(void)
{
    win_scanf("Prefered calibration method : %R No calibration (use reference profile)\n"
              "%r Generic calibration image\n"
              "%r Customize calibration image \n"
              "Note : if all calibration are not uniform,\n"
              "the system will fallback on generic calibration then reference profile.", &g_preferred_calibration_type);
    return D_O_K;
}

int do_set_current_as_reference_calibration(void)
{
    g_update_reference_prof = true;
    return D_O_K;
}

