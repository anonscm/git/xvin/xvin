#pragma once

#include "xvin.h"
#include "../track_util.h"
#include "track_struct.h"

void cpu_track_beads(const cpu_image* image, cpu_crosses* crosses);

cpu_bead* cpu_create_bead(cpu_bead_cross* cross, cl_int prof_size);

void cpu_create_beads(const cpu_image *image, cpu_crosses*crosses);

bool cpu_bound_cross_position(cpu_bead_cross* cross, const cpu_image* image, cl_int crosse_len);

bool cpu_cross_in_image(const cpu_bead_cross* cross, const cpu_image* image);

void cpu_clip_cross_in_image(cpu_bead_cross* cross, const cpu_image* image, cl_int cross_len); 

void cpu_fill_profiles_xy(const cpu_image* image, cpu_crosses *crosses);

cl_float *cpu_profile_autoconv(cl_float* prof, cl_int size);

cl_float *cpu_profile_autoconv_complex(cl_float* prof, cl_int size);

cl_float cpu_find_center(cl_float* x, cl_int size, cl_float *max_pos, cl_float *max_val, cl_float *max_deriv, cl_int max_limit);

void cpu_track_xy(cpu_crosses *crosses);

cl_int cpu_remove_profile_average(cl_float* prof, cl_int size);

void cpu_deplace(cl_float *x, int nx);

cl_int cpu_track_z(cpu_crosses *crosses, float *min_c, float *min_phi);

cl_float *cpu_fill_profile_z(const cpu_image* image, cpu_crosses *crosses);

int cpu_prepare_filtered_profile(cl_float *zp, cl_int nx, cpu_crosses *crosses, cpu_bead_param *bead_param);

cl_int cpu_coarse_find_z_by_least_square_min(cl_float *yt, cl_int nxp, const cpu_calib_images *calib_imgs, cl_int beadid, float *min, int rc);

cl_float cpu_find_simple_phase_shift_between_filtered_profile(cl_float *zr, cl_float *zp, cl_int nx, cl_int rc);

cl_float cpu_find_zero_of_3_points_polynome(cl_float y_1, cl_float y0, cl_float y1);

bool cpu_check_calibration_image_uniform(cpu_crosses *crosses);
