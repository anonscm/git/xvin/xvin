#pragma once
#include "../../opencl_utils/cl_utils.h"
#include "../../fft2d/fftbtl32n.h"
#include "../fillibbt.h"
#include <stdbool.h>

#define XV_CALIB_TYPE_NONE 0
#define XV_CALIB_TYPE_GENERIC_IMG 1
#define XV_CALIB_TYPE_CUSTOM_IMG 2

typedef struct
{
    cl_mem mem;
    cl_event event;
} gpu_event_driven_memory;

typedef struct
{
    cl_float x;
    cl_float y;
} cpu_coord;

typedef struct
{
    cl_int width, height; // image size
    cl_uchar *data;
    gpu_event_driven_memory gpu_data;
} cpu_image;

typedef struct
{
    cl_int width, height; // image size
    cl_float ay, dy; // transform between
    cl_float *data;
    gpu_event_driven_memory gpu_data;

} cpu_image_complex;

typedef struct
{
    cl_int max_size;
    cl_int size;
    cl_int width; // number of points as float (2 * nb of complex points)
    cl_int height; // one bead image size
    cl_float ay, dy; // transform between
    union
    {
        cl_float *fl;
        cpu_coord *co;
    } filtered_calib_data;
    cl_float *z_ajust;
    gpu_event_driven_memory gpu_filtered_calib_data;
    gpu_event_driven_memory  *gpu_z_ajust;
} cpu_calib_images;

typedef struct
{
    cl_float x, y, z; // x and y bead center
    cl_uint radial_profile_size;
    cl_float *avg_x_profile,
             *avg_y_profile,
             *radial_z_profile; // radial profiles
    cl_int calib_img_index;
    cl_int calib_min_phi;
    cl_int calib_min_xi2;
} cpu_bead;

typedef struct
{
    fft_plan *fplan;
    filter_bt *fil;
} cpu_bead_param;

typedef struct
{
    cl_int x, y;            // x and y cross center
    cl_int last_tracked_x, last_tracked_y; // last correct position, when bead was not lost
    bool mouse_draged;
    bool in_image;
    cl_int lost_counter;
    cpu_bead *bead;
} cpu_bead_cross;

typedef struct
{
    cl_int size;
    cl_int max_size;
    cl_int length, width;
    cl_int max_meaningful_radius;
    cl_int bandpass_center;
    cl_int bandpass_width;
    cl_int calib_type;
    cl_bool update_reference_profile;
    cl_bool parameter_has_changed;

    cpu_bead_cross **cross_data;
    cpu_coord *beads_pos; //only for internal use

    cpu_bead_param **bead_param;
    cpu_calib_images *calib_data;

} cpu_crosses;

typedef struct
{
    cl_int size;        // absolute size off rolling buffer
    cl_int first, last; // first and last index of the buffer
    cpu_image *images;
    cpu_crosses *crosses;
} cpu_bead_track;



static inline cl_uchar cpu_image_pixel(const cpu_image *image, int x, int y)
{
    return image->data[y * image->width + x];
}

static inline cl_float cpu_image_complex_real_pixel(const cpu_image_complex *image, int x, int y)
{
    return image->data[y * image->width + x];
}

static inline cl_uchar *cpu_image_line(const cpu_image *image, int y)
{
    return image->data + (y * image->width);
}

static inline cl_float *cpu_image_complex_line(const cpu_image_complex *image, int y)
{
    return image->data + (y * image->width * 2);
}

static inline cl_float *cpu_calib_line(const cpu_calib_images *calib_images, int beadid ,int y)
{
    return calib_images->filtered_calib_data.fl
    + calib_images->height * calib_images->width * beadid
    + calib_images->width * y;
}
