// macros replaced during compilation :
//#define NB_ADD_LINE 16 //- number of line to add
//#define PROF_SIZE 128 //- profile size
//#define PROF_WIDTH 32
#ifdef cl_khr_fp64
#pragma OPENCL EXTENSION cl_khr_fp64 : enable
#elif defined(cl_amd_fp64)
#pragma OPENCL EXTENSION cl_amd_fp64 : enable
#else
#error "Double precision floating point not supported by OpenCL implementation."
#endif

// get_global_id(0) --> position in profile
// get_global_id(1) --> cross id

__kernel void fill_horiz_profiles (
        read_only image2d_t image,
        __global float2 *beads_pos,  // absolute begin prof position in image
        __global float *profs // horizontal profile data for all crosses
        )
{

    const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_NONE;
    int pos = get_global_id(0);
    int cross_id = get_global_id(1);
    int profspos =  PROF_SIZE * cross_id + pos;// absolute position of the point to average in image
    int2 orig = convert_int2(beads_pos[cross_id]) - (int2)(PROF_SIZE / 2, NB_ADD_LINE / 2);
    int2 coord = orig + (int2)(pos, 0);
    int4 sum = 0;

#pragma unroll
    for (int i =  0; i < NB_ADD_LINE; i++)
    {
        sum += read_imagei(image, sampler, coord);
        coord += (int2)(0, 1);
    }
    profs[profspos] = sum.x;
}

// get_global_id(0) --> position in profile
// get_global_id(1) --> cross id

__kernel void fill_vert_profiles (
        read_only image2d_t image,
        __global float2 *beads_pos,  // absolute begin prof position in image
        __global float *profs // vertical profile data for all crosses
        )
{
    const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_NONE;
    int pos = get_global_id(0);
    int cross_id = get_global_id(1);
    int profspos =  PROF_SIZE * cross_id + pos;// absolute position of the point to average in image
    int2 orig = convert_int2(beads_pos[cross_id]) - (int2)(NB_ADD_LINE / 2, PROF_SIZE / 2);
    int2 coord = orig + (int2)(0, pos);
    int4 sum = 0;

#pragma unroll
    for (int i =  0; i < NB_ADD_LINE; i++)
    {
        sum += read_imagei(image, sampler, coord);
        coord += (int2)(1, 0);
    }

    profs[profspos] = sum.x;
}

// FIXME ADD LIMIT
// kernel launched on (PROF_SIZE)
__kernel void prof_max (
        __global const float *profs,
        __local float *partial_max, // hiorizontal profile data for all crosses
        __local float *partial_maxval, // horizontal profile data for all crosses
        __global int *maxs
        )
{
    int pos = get_global_id(0);
    int prof_id = get_global_id(1);
    __global const float *prof =  profs + PROF_SIZE * prof_id;// absolute position of the point to average in image

    partial_maxval[pos] = prof[pos];
    partial_max[pos] = pos;

    barrier(CLK_LOCAL_MEM_FENCE);

    for(int i = PROF_SIZE / 2; i > 0; i >>= 1)
    {
        if(pos < i)
        {
            if (partial_maxval[pos + i] > partial_maxval[pos])
            {
                partial_maxval[pos] = partial_maxval[pos + i];
                partial_max[pos] = partial_max[pos + i];
            }
        }
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (pos == 0)
    {
        maxs[prof_id] = partial_max[0];
    }
}

// executed on crosses_count * 2 (each avg profiles)
__kernel void deplacement_delta(
        __global const float *profs,
        __global const int *maxs,
        __global float *deltas
)
{
    int prof_id = get_global_id(0);
    double a, b, c, d, f_2, f_1, f0, f1, f2, xm = 0;
    int nmax;
    int delta = 0;
    __global const float *prof =  profs + PROF_SIZE * prof_id;// absolute position of the point to average in image

    nmax = maxs[prof_id];

    f_2 = prof[(PROF_SIZE + nmax - 2) % PROF_SIZE];
    f_1 = prof[(PROF_SIZE + nmax - 1) % PROF_SIZE];
    f0 = prof[nmax];
    f1 = prof[(nmax + 1) % PROF_SIZE];
    f2 = prof[(nmax + 2) % PROF_SIZE];

    // polinomiale interpolation
    if (f_1 < f1)
    {
        a = -f_1/6 + f0/2 - f1/2 + f2/6;
        b = f_1/2 - f0 + f1/2;
        c = -f_1/3 - f0/2 + f1 - f2/6;
        d = f0;
        delta = 0;
    }
    else
    {
        a = b = c = 0;
        a = -f_2/6 + f_1/2 - f0/2 + f1/6;
        b = f_2/2 - f_1 + f0/2;
        c = -f_2/3 - f_1/2 + f0 - f1/6;
        d = f_1;
        delta = -1;
    }

    if (fabs(a) < 1e-8)
    {
        if (b != 0)
        {
            xm =  - c/(2*b) - (3 * a * c * c)/(4 * b * b * b);
        }
        else
        {
            xm = 0;
        }
    }
    else if ((b*b - 3*a*c) < 0)
    {
        xm = 0;
    }
    else
        xm = (-b - sqrt(b*b - 3*a*c))/(3*a);

    deltas[prof_id] = (float)(xm + nmax + delta);
    //printf("%f", deltas[prof_id]);

}


__kernel void update_bead_center (
        __global const float *deltas,
        __global float2 *beads_pos
        )
{
    int cross_id = get_global_id(0);
    float dx = deltas[cross_id];
    float dy = deltas[cross_id + get_global_size(0)];

    dx -= PROF_SIZE / 2;
    dx /= 2;
    dy -= PROF_SIZE / 2;
    dy /= 2;

    beads_pos[cross_id] += (float2) (dx, dy);
}

