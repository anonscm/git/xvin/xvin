#pragma OPENCL EXTENSION cl_khr_global_int32_base_atomics : enable
#ifdef cl_khr_fp64
#pragma OPENCL EXTENSION cl_khr_fp64 : enable
#elif defined(cl_amd_fp64)
#pragma OPENCL EXTENSION cl_amd_fp64 : enable
#else
#error "Double precision floating point not supported by OpenCL implementation."
#endif

#define ATOMIC
// MAX_R is define at compilation time by the host program
__kernel void fill_profile (read_only image2d_t image,      // the full image
        __global float2 *beads_center,  // cross bead center
        volatile __global int *profs             // all profiles in one array
        )
{
    int i = get_global_id(0);            //  coordinate of the pixel treated
    int j = get_global_id(1);
    int cross_id = get_global_id(2);     // bead number

    float r; // polar radius of (x,y)
    int4 pix; // pixel value of (i,j)
    int ri, risec; // r in int
    float p, q; // fractionnal part q = 1 - p
    const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_CLAMP;
    float2 center = beads_center[cross_id];
    float2 center_int;
    float2 center_fraq = fract(center, &center_int); // q here for linker disambiguiation, not used

    //printf("id=%d (%f,%f)\n", cross_id, center.x, center.y);

    float2 cartcoord = (float2)(i, j) - (float2)(MAX_R, MAX_R);
    int2 imgcoord = convert_int2 (round(cartcoord + center));
    volatile __global int *prof = profs + cross_id * (MAX_R * 2 + 2);


    pix = read_imagei(image, sampler, imgcoord);

    //TEST
    //pix.x = 128;

    r = length(convert_float2(imgcoord) - center);
    p = fract(r, &q) * 64.;
    ri = q; // Resolving link ambiguity...
    risec = ri + MAX_R;
    q = 64. - p;

    if (ri < MAX_R)
    {
#ifdef ATOMIC
        atomic_add(prof + risec,  round(q * pix.x));
        atomic_add(prof + ri,  round(q * 128));  // we make h that image is 8 bits
#else
        prof[risec] += round(q * pix.x);
        prof[ri] +=  round(q * 128);  // we make h that image is 8 bits
#endif
        ri++;
        if (ri < MAX_R)
        {
            risec++;
#ifdef ATOMIC
            atomic_add(prof + risec, round(p * pix.x));
            atomic_add(prof + ri, round(p * 128));
#else
            prof[risec] +=  round(p * pix.x);
            prof[ri] += round(p * 128);
#endif
        }
    }
    else if (ri == MAX_R)
    {
#ifdef ATOMIC
        atomic_add(prof + MAX_R * 2, round(q * pix.x));
        atomic_add(prof + MAX_R * 2 + 1, round(q * 128));
#else
        prof[MAX_R * 2] += (int) round(q * pix.x);
        prof[MAX_R * 2 + 1] += (int) round(q * 128);
#endif
    }
}

// MAX_R is define at compilation time by the host program
__kernel void fill_profile_ocl_interpol (read_only image2d_t image,      // the full image
        __global float2 *beads_center,  // cross bead center
        volatile __global int *profs             // all profiles in one array
        )
{
    int i = get_global_id(0);            //  coordinate of the pixel treated
    int j = get_global_id(1);
    int cross_id = get_global_id(2);     // bead number

    float r; // polar radius of (x,y)
    int4 pix; // pixel value of (i,j)
    int ri, risec; // r in int
    float p, q; // fractionnal part q = 1 - p
    const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_CLAMP;
    float2 center = beads_center[cross_id];
    float2 center_int;
    float2 center_fraq = fract(center, &center_int); // q here for linker disambiguiation, not used

    //printf("id=%d (%f,%f)\n", cross_id, center.x, center.y);

    float2 cartcoord = (float2)(i, j) - (float2)(MAX_R, MAX_R);
    int2 imgcoord = convert_int2 (round(cartcoord + center));
    volatile __global int *prof = profs + cross_id * (MAX_R * 2 + 2);


    pix = read_imagei(image, sampler, imgcoord);

    //TEST
    //pix.x = 128;

    r = length(convert_float2(imgcoord) - center);
    p = fract(r, &q) * 64.;
    ri = q; // Resolving link ambiguity...
    risec = ri + MAX_R;
    q = 64. - p;

    if (ri < MAX_R)
    {
#ifdef ATOMIC
        atomic_add(prof + risec,  round(q * pix.x));
        atomic_add(prof + ri,  round(q * 128));  // we make h that image is 8 bits
#else
        prof[risec] += round(q * pix.x);
        prof[ri] +=  round(q * 128);  // we make h that image is 8 bits
#endif
        ri++;
        if (ri < MAX_R)
        {
            risec++;
#ifdef ATOMIC
            atomic_add(prof + risec, round(p * pix.x));
            atomic_add(prof + ri, round(p * 128));
#else
            prof[risec] +=  round(p * pix.x);
            prof[ri] += round(p * 128);
#endif
        }
    }
    else if (ri == MAX_R)
    {
#ifdef ATOMIC
        atomic_add(prof + MAX_R * 2, round(q * pix.x));
        atomic_add(prof + MAX_R * 2 + 1, round(q * 128));
#else
        prof[MAX_R * 2] += (int) round(q * pix.x);
        prof[MAX_R * 2 + 1] += (int) round(q * 128);
#endif
    }
}

__kernel void average_profile(
        __global float *fprofs,
        __global const int *iprofs
        )
{
    int pos = get_global_id(0);
    int cross_id = get_global_id(1);

    __global float *fprof = fprofs + cross_id * (MAX_R * 2);
    __global const int *iprof = iprofs + cross_id * (MAX_R * 2 + 2);

    //fprof[pos + MAX_R] = 1;//pos; //(float) iprof[pos + MAX_R];
    //fprof[0] = 0;//iprof[MAX_R * 2];

    fprof[pos + MAX_R] = (iprof[pos] == 0) ? 0 : iprof[pos + MAX_R] / (((float)iprof[pos]));
    fprof[pos + MAX_R] *= 128;
    if (pos == 0)
    {
        fprof[0] = (iprof[MAX_R * 2 + 1] == 0) ? 0 : iprof[MAX_R * 2] / (((float)iprof[MAX_R * 2 + 1]));
        fprof[0] *= 128;
    }
}

__kernel void deplace_profile(
        __global float *profs
        )
{
    int pos = get_global_id(0);
    int cross_id = get_global_id(1);
    __global float *prof = profs + cross_id * (MAX_R * 2);

    prof[MAX_R - pos] = prof[MAX_R + pos];
}


// Calculate fits of all profils on the whole calibration image
__kernel void calculate_fits_on_calib_image(
        __global const float *calib_image,
        __global const float *profs,
        __local float *partial_fit,
        __global float *fits)
{
    int pos = get_global_id(0);
    int cross_id = get_global_id(1);
    int calib_id = get_global_id(2);

    __global const float *prof = profs + cross_id * (MAX_R * 2) + MAX_R;
    __global const float *calib = calib_image + cross_id * (CALIB_SIZE) * (MAX_R * 2) + calib_id * (MAX_R * 2) + MAX_R;
    __global float *fit = fits + cross_id * (CALIB_SIZE);
    //__local float *partial_fit = partial_fits + (MAX_R) * get_local_id(2);

    if (pos >= MAX_MEANINGFUL_RADIUS)
    {
        float tmp = calib[pos] - prof[pos];
        partial_fit[pos] = tmp * tmp;
    }
    else
    {
        partial_fit[pos] = 0;
    }

    barrier(CLK_LOCAL_MEM_FENCE);

#pragma unroll
    for (int i = MAX_R / 2; i > 0; i >>= 1)
    {
        if (pos < i)
        {
            partial_fit[pos] += partial_fit[pos + i];
        }
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (pos == 0)
    {
        fit[calib_id] = partial_fit[0];
    }
}

// more basic but faster :( ...
// Calculate fits of all profils on the whole calibration image
__kernel void calculate_fits_on_calib_image2(
        __global const float *calib_image,
        __global const float *profs,
        __global float *fits)
{
    int cross_id = get_global_id(0);
    int calib_id = get_global_id(1);

    __global const float *prof = profs + cross_id * (MAX_R * 2) + MAX_R;
    __global const float *calib = calib_image + cross_id * (CALIB_SIZE) * (MAX_R * 2) + calib_id * (MAX_R * 2) + MAX_R;
    __global float *cross_fits = fits + cross_id * (CALIB_SIZE);
    //__local float *partial_fit = partial_fits + (MAX_R) * get_local_id(2);

    float fit = 0;
    for (int i = MAX_MEANINGFUL_RADIUS; i < MAX_R; ++i)
    {
        //printf("%f",calib);
        float tmp = calib[i] - prof[i];
        fit += tmp * tmp;
    }
    //printf("\n",calib);
    cross_fits[calib_id] = fit;
}


// find the minimum (best) fitness value for each profil
__kernel void find_best_calib_profile(
        __global float  *fits,
        __local int   *partial_best_fit,
        __local float   *partial_best_fitval,
        __global int  *best_fits_index,
        __global float *best_fits_xi2)
{
    int calib_id = get_global_id(0);
    int cross_id = get_global_id(1);

    __global float *fit = fits + cross_id * (CALIB_SIZE);

    partial_best_fitval[calib_id] = fit[calib_id];
    partial_best_fit[calib_id] = calib_id;

    barrier(CLK_LOCAL_MEM_FENCE);

    for (int i = CALIB_SIZE / 2; i > 0; i >>= 1)
    {
        if (calib_id < i)
        {
            if (partial_best_fitval[calib_id + i] < partial_best_fitval[calib_id])
            {
                partial_best_fitval[calib_id] = partial_best_fitval[calib_id + i];
                partial_best_fit[calib_id] = partial_best_fit[calib_id + i];
            }
        }
        barrier(CLK_LOCAL_MEM_FENCE);
    }

    best_fits_index[cross_id] = partial_best_fit[0];
    best_fits_xi2[cross_id] = partial_best_fitval[0];
}

// NOT TESTED
//caculate normalize fitness for the best
__kernel void compute_normalized_fitness_calib(
        __global float *calib_image,
        __global int *best_fits_index,
        __local float *partial_norm,
        __global float *normalize_fits)
{
    int pos = get_global_id(0);
    int cross_id = get_global_id(1);

    __global float *calib = calib_image + cross_id * (CALIB_SIZE) * (MAX_R * 2) + best_fits_index[cross_id] * (MAX_R * 2);

    // TODO : add only relevant values based on max_meaningful_radius
    float tmp = calib[pos];
    partial_norm[pos] = tmp * tmp;

    barrier(CLK_LOCAL_MEM_FENCE);

    for (int i = MAX_R; i > 0; i >>= 1)
    {
        if (pos < i)
        {
            partial_norm[pos] += partial_norm[pos + i];
        }
        barrier(CLK_LOCAL_MEM_FENCE);
    }

    normalize_fits[cross_id] = partial_norm[0];
}


__kernel void prepare_first_shift(
        __global int *calib_ids
        )
{
    int pos = get_global_id(0);
    int calib_id = calib_ids[pos];

    calib_id = calib_id == 0 ? 1 : (calib_id == CALIB_SIZE - 1 ? CALIB_SIZE - 2 : calib_id);
    calib_ids[pos] = calib_id;
}

//set the index to be the first calib image to process over 3;
__kernel void prepare_shifts(
        __global int *calib_ids,
        __global float *phis
        )
{
    int cross_id = get_global_id(0);
    int calib_id = calib_ids[cross_id];
    float phi = phis[cross_id];

    calib_id = calib_id <= 0 ? 0 : calib_id >= CALIB_SIZE - 2 ? CALIB_SIZE - 3 : calib_id - 1;

    calib_ids[cross_id] = calib_id;
}

// FIXME : NEED PERFOMANCE OPTIMISATION
// Working with small variations
__kernel void simple_shift_between_profile(
        __global float2 *calib_image, //0
        __global float2 *profs, //1
        __local float *partial_phi,//2 contains phase in x and amplitude of the phase in y
        __global float *phis//3
#ifndef CALIB_TYPE_NONE
        ,__global int *calib_indexes//4
#endif
        )
{
    int amp_pos = get_global_id(0);
    int cplx_pos = MAX_R - amp_pos;
    int cross_id = get_global_id(1);
#ifdef CALIB_TYPE_NONE
    int calib_id = 0; // WHY 1 ?;
#else
    int calib_id = calib_indexes[cross_id] + get_global_id(2);
#endif
    float ph, amp;
    float re, im;

    __global float2 *calib_prof = calib_image + cross_id * (CALIB_SIZE) * (MAX_R) + calib_id *
(MAX_R);
    __global float2 *prof = profs + cross_id * (MAX_R);
    __local float *partial_w = partial_phi + MAX_R;

    re = calib_prof[cplx_pos].x * prof[cplx_pos].x + calib_prof[cplx_pos].y * prof[cplx_pos].y;
    im = prof[cplx_pos].y * calib_prof[cplx_pos].x - prof[cplx_pos].x * calib_prof[cplx_pos].y;
    ph = (im == 0.0 && re == 0.0) ? 0.0 : atan2(im, re);

    amp = prof[amp_pos].x * calib_prof[amp_pos].x; // the y is always 0.

    if (amp_pos == 0 || amp_pos >= (MAX_R - MAX_MEANINGFUL_RADIUS) / 2 )
    {
        partial_phi[amp_pos] = 0;
        partial_w[amp_pos] = 0;
    }
    else
    {
        partial_phi[amp_pos] = amp * ph;
        partial_w[amp_pos] = amp;
    }

    barrier(CLK_LOCAL_MEM_FENCE);

    for(int i = MAX_R / 4 ; i > 0; i >>= 1)
    {
        if(amp_pos < i)
        {
            partial_phi[amp_pos] += partial_phi[amp_pos + i];
            partial_w[amp_pos] += partial_w[amp_pos + i];
        }
        barrier(CLK_LOCAL_MEM_FENCE);
    }

    if (amp_pos == 0)
    {
        phis[cross_id * get_global_size(2) + get_global_id(2)] = partial_w[0] == 0.0 ? partial_phi[0] :
            partial_phi[0] / partial_w[0];
    }

    //printf("gpu phi: %f \n", phis[cross_id + get_global_id(2)]);
}

// NOT USED
__kernel void phase_shift_between_profile(
        __global float2 *ref_profs,
        __global float2 *profs,
        __local float *phs,
        __global float *phis
        )
{
    int cross_id = get_global_id(0);

    float re = 0;
    float im = 0;
    float ph = 0;
    float amp = 0;
    float phi = 0;
    float w = 0;

    __global float2 *ref_prof = ref_profs + cross_id * MAX_R;
    __global float2 *prof = profs + cross_id * (MAX_R);
    __local float *phs1 = phs + MAX_R;

    for (int cplx_pos = 0; cplx_pos < MAX_R; cplx_pos++)
    {
        re = ref_prof[cplx_pos].x * prof[cplx_pos].x + ref_prof[cplx_pos].y * prof[cplx_pos].y;
        im = prof[cplx_pos].y * ref_prof[cplx_pos].x - prof[cplx_pos].x * ref_prof[cplx_pos].y;
        phs[cplx_pos] = (im == 0.0 && re == 0.0) ? 0.0 : atan2(im, re);
    }

    for (int i = MAX_R / 2 + MAX_MEANINGFUL_RADIUS / 2; i < MAX_R; i++)
    {
        int dphi = phs[i] - phs[i - 1];
        if (dphi > M_PI)
            phi -= 2*M_PI;
        if (dphi < -M_PI)
            phi += 2*M_PI;

        phs1[i] = phs[i] + phi;
    }

    phi = 0;
    for (int i = MAX_R / 2 - MAX_MEANINGFUL_RADIUS / 2; i >= 0; --i)
    {
        int dphi = phs[i] - phs[i + 1];
        if (dphi > M_PI)
            phi -= 2*M_PI;
        if (dphi < -M_PI)
            phi += 2*M_PI;

        phs1[i] = phs[i] + phi;
    }

    phi = 0;
    w = 0;
    for (int i = MAX_R / 2 + MAX_MEANINGFUL_RADIUS; i < MAX_R; i++)
    {
        amp =   prof[i].x * prof[i].x;
        amp += prof[i].y * prof[i].y;
        w += amp;
        phi += amp * phs1[i];
    }

    phis[cross_id] = (w != 0.0) ? phi / w : phi;
}


__kernel void compute_3_points_polynome_zero(
        __global float *phis,
        __global float *zeros
        )
{
    int cross_id = get_global_id(0);
    float y_1 = phis[cross_id * 3];
    float y0 = phis[cross_id * 3 + 1];
    float y1 = phis[cross_id * 3 + 2];

    double a = ((y_1 + y1) / 2) - y0;
    double b = (y1 - y_1) / 2;
    double c = y0;
    double delta = b * b - 4 * a * c;
    double x;


    if (a == 0)
    {
        x = (b != 0) ? -c / b : 0;
    }
    else if (delta >= 0)
    {
        delta = sqrt(delta);
        x = (fabs(-b + delta) < fabs(-b - delta)) ? -b + delta : -b - delta ;
        x /= 2 * a;
    }
    zeros[cross_id] = (float) x;
}
