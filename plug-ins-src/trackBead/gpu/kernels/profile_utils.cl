//#define prof_size 128 //- profile size

__kernel void  fill_empty(
        __global int* prof
        )
{
    prof[get_global_id(0)] = 0;
}

// kernel lauched on (PROF_SIZE / 2) points
__kernel void rdft_deplace(__global float *profs)
{
    int pos = get_global_id(0);
    float tmp;
    int prof_id = get_global_id(1);
    __global float *prof = profs + PROF_SIZE * prof_id;

    tmp = prof[pos + PROF_SIZE / 2];
    prof[pos + PROF_SIZE / 2] = prof[pos];
    prof[pos] = tmp;
}

// kernel launched on (PROF_SIZE)
__kernel void remove_avg (
        __global float *profs,
        __local float *partial_sums // horizontal profile data for all crosses
        )
{
    int pos = get_global_id(0);
    int prof_id = get_global_id(1);
    int profspos =  PROF_SIZE * prof_id + pos;// absolute position of the point to average in image

    // FIXME: computing useless points.
    partial_sums[pos] = profs[profspos] * !!((pos < PROF_SIZE / 4) || (pos >= (3 * PROF_SIZE) / 4));

    barrier(CLK_LOCAL_MEM_FENCE);

    for(int i = PROF_SIZE / 2; i > 0; i >>= 1)
    {
        if(pos < i)
        {
            partial_sums[pos] += partial_sums[pos + i];
        }
        barrier(CLK_LOCAL_MEM_FENCE);
    }

    profs[profspos] -= partial_sums[0] / (PROF_SIZE / 2);
}



__kernel void generate_sincos_lookup_table(__global float2 *lookup)
{
    int pos = get_global_id(0);
    float co;
    float si = sincos(2 * M_PI_F * pos / PROF_SIZE, &co);
    lookup[pos] = (float2)(si, co);
}


// kernel lauched on (PROF_SIZE / 2 + 1) points
__kernel void rdft_glob_lookup(
        __global float *timeprofs,
        __local float *tmp,
        __global float2 *lookup,
        __global float *freqprofs)
{

    int pos = get_global_id(0);
    int prof_id = get_global_id(1);
    __global float *timeprof = timeprofs + PROF_SIZE * prof_id;
    __global float *freqprof = freqprofs + PROF_SIZE * prof_id;

    float X_real = 0.0f;
    float X_imag = 0.0f;
    float arg;

    if (pos != PROF_SIZE / 2)
    {
        tmp[pos] = timeprof[pos];
        tmp[pos + PROF_SIZE / 2] = timeprof[pos + PROF_SIZE / 2];
    }

    barrier(CLK_LOCAL_MEM_FENCE);

    for(int i = 0; i < PROF_SIZE; i++)
    {
        float2 sico = lookup[(pos * i) % PROF_SIZE];

        X_real += tmp[i] * sico.y;
        X_imag -= tmp[i] * sico.x;
    }

    barrier(CLK_LOCAL_MEM_FENCE);

    if(pos == 0)
    {
        freqprof[0] = X_real / (PROF_SIZE / 2) / 2;
    }
    else if(pos == PROF_SIZE / 2)
    {
        freqprof[1] = X_real / (PROF_SIZE / 2) / 2;
    }
    else
    {
        freqprof[pos * 2] = X_real / (PROF_SIZE / 2);
        freqprof[pos * 2 + 1] = X_imag / (PROF_SIZE / 2);
    }
}

// kernel lauched on (PROF_SIZE / 2 + 1) points
__kernel void rdft(
        __global float *timeprofs,
        __local float *tmps,
        __local float2 *lookup,
        __global float *freqprofs)
{

    int pos = get_global_id(0);
    int prof_id = get_global_id(1);
    __global float *timeprof = timeprofs + PROF_SIZE * prof_id;
    __global float *freqprof = freqprofs + PROF_SIZE * prof_id;
    __local float *tmp = tmps + PROF_SIZE * get_local_id(1);

    float X_real = 0.0f;
    float X_imag = 0.0f;
    float arg;

    if (pos != PROF_SIZE / 2)
    {
        tmp[pos] = timeprof[pos];
        tmp[pos + PROF_SIZE / 2] = timeprof[pos + PROF_SIZE / 2];

        float co;
        float si = sincos(2 * M_PI_F * pos / PROF_SIZE, &co);

        lookup[pos] = (float2)(si, co);

        si = sincos(2 * M_PI_F * (pos + PROF_SIZE / 2) / PROF_SIZE, &co);

        lookup[pos + PROF_SIZE / 2] = (float2)(si, co);
    }

    barrier(CLK_LOCAL_MEM_FENCE);

    for(int i = 0; i < PROF_SIZE; i++)
    {
        float2 sico = lookup[(pos * i) % PROF_SIZE];

        X_real += tmp[i] * sico.y;
        X_imag -= tmp[i] * sico.x;
    }

    barrier(CLK_LOCAL_MEM_FENCE);

    if(pos == 0)
    {
        freqprof[0] = X_real / (PROF_SIZE / 2) / 2;
    }
    else if(pos == PROF_SIZE / 2)
    {
        freqprof[1] = X_real / (PROF_SIZE / 2) / 2;
    }
    else
    {
        freqprof[pos * 2] = X_real / (PROF_SIZE / 2);
        freqprof[pos * 2 + 1] = X_imag / (PROF_SIZE / 2);
    }
}

// kernel lauched on (PROF_SIZE) points
// in hermitian interleaved vector - real data in fourier space
__kernel void ridft(__global float *freqprofs, __local float *tmps, __local float2 *lookup, __global float *timeprofs)
{
    int pos = get_global_id(0);
    int prof_id = get_global_id(1);
    __global float *timeprof = timeprofs + PROF_SIZE * prof_id;
    __global float *freqprof = freqprofs + PROF_SIZE * prof_id;
    __local float *tmp = tmps + PROF_SIZE * get_local_id(1);
    float res = 0;

    tmp[pos] = freqprof[pos];
    float co;
    float si = sincos(2 * M_PI_F * pos / PROF_SIZE, &co);
    lookup[pos] = (float2)(si, co);

    // waiting for tmp and lookup
    barrier(CLK_LOCAL_MEM_FENCE);

    res = tmp[0] + tmp[1] * cos(M_PI_F * pos);
    for(int i = 1; i < PROF_SIZE / 2; i++)
    {

        float2 sico = lookup[(pos * i) % PROF_SIZE];
        res += tmp[i * 2] * sico.y - tmp[i * 2 + 1] * sico.x;
    }

    timeprof[pos] = res;
}

// kernel lauched on (PROF_SIZE / 2) points
// in complex interleaved vector - complex data in fourier space
__kernel void idft(__global float2 *freqprofs,
        __local float2 *tmp,
        __local float2 *lookup,
        __global float2 *timeprofs)
{
    int pos = get_global_id(0);
    int prof_id = get_global_id(1);
    __global float2 *timeprof = timeprofs + PROF_SIZE / 2 * prof_id;
    __global float2 *freqprof = freqprofs + PROF_SIZE / 2 * prof_id;
    float2 res = 0;

    tmp[pos] = freqprof[pos];
    float co;
    float si = sincos(2 * M_PI_F * pos / (PROF_SIZE / 2), &co);
    lookup[pos] = (float2)(si, co);

    barrier(CLK_LOCAL_MEM_FENCE);

    for(int i = 0; i < PROF_SIZE / 2; i++)
    {
        float2 pt = tmp[i];
        float2 sico = lookup[(pos * i) % (PROF_SIZE / 2)];
        res += (float2)(
                pt.x * sico.y - pt.y * sico.x,  // re = re * cos(arg) - im * sin(arg)
                pt.x * sico.x + pt.y * sico.y); // im = re * sin(arg) + im * cos(arg)
    }
    timeprof[pos] = res;
}

// kernel lauched on (PROF_SIZE / 2) points
// in hermitian interleaved vector - real data in fourier space
__kernel void auto_conv(__global float2 *freqprofs)
{
    int pos = get_global_id(0);
    int prof_id = get_global_id(1);
    __global float2 *freqprof = freqprofs + PROF_SIZE / 2 * prof_id;

    float2 c = freqprof[pos];

    if (pos == 0)
    {
        freqprof[0] = (float2)(2 * (c.x * c.x), 0);
    }
    else
    {
        freqprof[pos] = (float2)(c.x * c.x - c.y * c.y, 2 * c.x * c.y);
    }
}


// kernel lauched on (PROF_SIZE / 2) points
// in hermitian interleaved vector - real data in fourier space
__kernel void prepare_bandpass_smooth_filter(__global float* filter, int center, int width)
{
    int pos = get_global_id(0);
    int c1 = center - width;
    int c2 = center + width;

    if (pos < c1  || pos >= c2 || pos == PROF_SIZE / 2 - 1 )
    {
        filter[pos] = 0;
    }
    else
    {
        filter[pos] = (1 - cos ( (pos - c1) * M_PI_F / width)) / 2;
    }
}

//untested
// kernel lauched on (PROF_SIZE / 2) points
// in hermitian interleaved vector - real data in fourier space
__kernel void prepare_hipass_smooth_filter(__global float* filter, int cutoff)
{
    int pos = get_global_id(0);

    if (pos >= 2 * cutoff)
    {
        filter[pos] = 1;
    }
    else if (pos > 0)
    {
        filter[pos] = (1 - cos ( pos * M_PI_F / cutoff)) / 2;
    }
    else
    {
        filter[pos] = 0;
    }
}

// untested
// kernel lauched on (PROF_SIZE / 2) points
// in hermitian interleaved vector - real data in fourier space
__kernel void prepare_lowpass_smooth_filter(__global float* filter, int cutoff)
{
    int pos = get_global_id(0);

    if (pos >= 2 * cutoff)
    {
        filter[pos] = 0;
    }
    else if (pos > 0)
    {
        filter[pos] = (1 + cos ( pos * M_PI_2_F / cutoff)) / 2;
    }
    else
    {
        filter[pos] = 1;
    }
}

//kernel get first mode for nomalization
__kernel void get_first_mode(__global float *profs, __global float *divs)
{
    int prof_id = get_global_id(0);
    __global float *prof = profs + prof_id * (PROF_SIZE);

    divs[prof_id] = prof[2];
}

// kernel lauched on (CROSS_COUNT) points
__kernel void normalize_profiles(__global float *divs, __global float *profs)
{
    int pos = get_global_id(0);
    int prof_id = get_global_id(1);
    __global float *prof = profs + prof_id * (PROF_SIZE);

    float div = divs[prof_id];

    if (div != 0)
    {
        prof[pos] /= div;
    }
}

// kernel lauched on (PROF_SIZE / 2) points
// in hermitian interleaved vector - real data in fourier space
__kernel void filter(__global float *filter, __global float2 *profs)
{
    int pos = get_global_id(0);
    int prof_id = get_global_id(1);
    __global float2 *prof = profs + prof_id * (PROF_SIZE / 2);

    if (pos != 0)
    {
        prof[pos] *= filter[pos];
    }
    else
    {
        prof[0].x *= filter[0];
        prof[0].y *= filter[PROF_SIZE / 2 - 1];
    }
}

__kernel void complex_module(__global float2 *profs)
{
    int pos = get_global_id(0);
    int prof_id = get_global_id(1);
    __global float2 *prof = profs + prof_id * (PROF_SIZE / 2);

    prof[pos] = (float2) (length(prof[pos]), 0);
}

