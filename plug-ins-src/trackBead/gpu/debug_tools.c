#include "debug_tools.h"
#include "xvin.h"
#include <assert.h>

pltreg *g_pr = 0;
O_p *g_plot = 0;
d_s *g_dataset1 = 0;
d_s *g_dataset2 = 0;

void output_debug_image(const cpu_crosses *crosses, const cpu_image *image, const cpu_bead_cross *cross ,
                        const cl_float *xprof, const cl_float *yprof)
{
    static int filecounter = 0;
    cpu_image img;
    memcpy(&img, image, sizeof(cpu_image));

    img.data = (cl_uchar *) calloc(sizeof(cl_uchar), image->width * image->height);
    memcpy(img.data, image->data, sizeof(cl_uchar) * image->width *  image->height);

    if (cross)
    {
        cl_int cross_len = crosses->length;
        cl_int cross_wth = crosses->width;
        cl_int arm_len = cross_len / 2;
        cl_int arm_wth = cross_wth / 2;

        cl_int xbegin = cross->x - arm_len;
        cl_int ybegin = cross->y - arm_wth;

        if (xprof)
        {
            for (int j = 0; j < cross_wth; j++)
            {
                cl_uchar *line = cpu_image_line(&img, ybegin + j);

                for (int i = 0; i < cross_len; i++)
                {
                    line[xbegin + i] = xprof[i] / cross_wth;
                }
            }
        }

        if (yprof)
        {
            xbegin = cross->x - arm_wth;
            ybegin = cross->y - arm_len;

            for (int i = 0; i < cross_len; i++)
            {
                cl_uchar *line = cpu_image_line(&img, ybegin + i);

                for (int j = 0; j < cross_wth; j++)
                {
                    line[xbegin + cross_wth - j - 1] = yprof[i] / cross_wth;
                }
            }
        }
    }

    char file[200] = {0};
    sprintf(file, "img%d.dat", filecounter++);
    FILE *f = fopen(file, "w");

    if (f == NULL)
    {
        printf("fail to open file %s\n", file);
        return;
    }

    printf("%s\n", file);

    for (int i = img.height - 1; i >= 0; i--)
    {
        cl_uchar *line = cpu_image_line(&img, i);

        for (int j = 0; j < img.width; j++)
        {
            fputc(line[j], f);
        }
    }

    fclose(f);
    free(img.data);
}
int debug_init_plot()
{
    if (g_pr != 0)
    {
        return 0;
    }

    imreg *last_imreg = NULL;
    ac_grep(cur_ac_reg, "%im", &last_imreg);

    g_pr = create_and_register_new_plot_project(0, 0, 1024, 1024);
    g_plot = create_and_attach_one_plot(g_pr, 128, 128 , 0);
    g_dataset1 = g_plot->dat[0];
    g_dataset2 = create_and_attach_one_ds(g_plot, 128, 128 , 0);
    //plot->op_idle_action = cpu_plot_idle_action;
    refresh_plot(g_pr, UNCHANGED);

    switch_project_to_this_imreg(last_imreg);
    return D_O_K;
}

void output_debug_profiles(const cpu_crosses *crosses, const cl_float *profs, cl_int nb_profs)
{
    static int filecounter = 0;

    char file[200] = {0};
    sprintf(file, "prof%d-w%d-h%d.dat", filecounter++, crosses->length, crosses->width * nb_profs);
    FILE *f = fopen(file, "w");

    if (f == NULL)
    {
        printf("fail to open file %s\n", file);
        return;
    }

    printf("%s\n", file);

    for (int k = 0; k < nb_profs; ++k)
    {
        for (int i = 0; i < crosses->width; ++i)
        {
            for (int j = 0; j < crosses->length; ++j)
            {
                cl_uchar c = profs[k * crosses->length + j] / crosses->width;
                fputc(c, f);
            }
        }
    }

    fclose(f);
}

void output_debug_crosses_beads(const cpu_image *image, cpu_crosses *crosses)
{
    static int filecounter = 0;
    cl_int cross_len = crosses->length;
    cl_int cross_wth = crosses->width;
    cl_int arm_len = cross_len / 2;
    cl_int arm_wth = cross_wth / 2;
    cpu_image img;
    memcpy(&img, image, sizeof(cpu_image));

    img.data = (cl_uchar *) calloc(sizeof(cl_uchar), image->width * image->height);
    memcpy(img.data, image->data, sizeof(cl_uchar) * image->width *  image->height);

    for (int i = 0; i < crosses->size; ++i)
    {
        const cpu_bead_cross *cross = crosses->cross_data[i];

        if (cross && cross->bead)
        {
            const cpu_bead *bead = cross->bead;

            cl_int xbegin = cross->x - arm_len;
            cl_int ybegin = cross->y - arm_wth;

            if (bead->avg_x_profile)
            {
                for (int j = 0; j < cross_wth; j++)
                {
                    cl_uchar *line = cpu_image_line(&img, ybegin + j);

                    for (int i = 0; i < cross_len; i++)
                    {
                        line[xbegin + i] = bead->avg_x_profile[i] / cross_wth;
                    }
                }
            }

            if (bead->avg_y_profile)
            {
                xbegin = cross->x - arm_wth;
                ybegin = cross->y - arm_len;

                for (int i = 0; i < cross_len; i++)
                {
                    cl_uchar *line = cpu_image_line(&img, ybegin + i);

                    for (int j = 0; j < cross_wth; j++)
                    {
                        line[xbegin + cross_wth - j - 1] = bead->avg_y_profile[i] / cross_wth;
                    }
                }
            }
        }
    }

    char file[200] = {0};
    sprintf(file, "img%d.dat", filecounter++);
    FILE *f = fopen(file, "w");

    if (f == NULL)
        printf("fail to open file %s\n", file);

    return;
    printf("%s\n", file);

    for (int i = img.height - 1; i >= 0; i--)
    {
        cl_uchar *line = cpu_image_line(&img, i);

        for (int j = 0; j < img.width; j++)
        {
            fputc(line[j], f);
        }
    }

    fclose(f);
    free(img.data);
}

d_s *debug_get_debug_plot(int ds_index)
{
    while (g_plot->n_dat <= ds_index)
    {
        create_and_attach_one_ds(g_plot, 1, 1 , 0);
    }

    return g_plot->dat[ds_index];
}

void debug_plot_profile(d_s *data, float *prof, int size)
{
    assert(size > 0);

    data->xd = (float *) realloc(data->xd, size * sizeof(float));
    data->yd = (float *) realloc(data->yd, size * sizeof(float));

    data->mx = data->mx < size ? size : data->mx;
    data->my = data->my < size ? size : data->my;
    data->nx = size;
    data->ny = size;

    for (int i = 0; i < size; ++i)
    {
        data->xd[i] = i;
        data->yd[i] = prof[i];
    }



    g_plot->need_to_refresh = ALL_NEED_REFRESH;

    //pltreg *cur_pr = NULL;
    //ac_grep(cur_ac_reg,"%im", &cur_pr);
    //if (cur_pr == g_pr)
    //    refresh_plot(g_pr, UNCHANGED);

}

void debug_plot_int_profile(int ds_index, int *prof, int size)
{
    while (g_plot->n_dat <= ds_index)
    {
        create_and_attach_one_ds(g_plot, size, size , 0);
    }

    d_s *data = g_plot->dat[ds_index];

    data->xd = (float *) realloc(data->xd, size * sizeof(float));
    data->yd = (float *) realloc(data->yd, size * sizeof(float));

    data->mx = data->mx < size ? size : data->mx;
    data->my = data->my < size ? size : data->my;
    data->nx = size;
    data->ny = size;

    for (int i = 0; i < size; ++i)
    {
        data->xd[i] = i;
        data->yd[i] = prof[i];
    }



    g_plot->need_to_refresh = ALL_NEED_REFRESH;

    //pltreg *cur_pr = NULL;
    //ac_grep(cur_ac_reg,"%im", &cur_pr);
    //if (cur_pr == g_pr)
    //    refresh_plot(g_pr, UNCHANGED);

}

void debug_plot_complex_profile(int ds_index, float *prof, int size)
{

    while (g_plot->n_dat <= ds_index)
    {
        create_and_attach_one_ds(g_plot, size, size , 0);
    }

    d_s *data = g_plot->dat[ds_index];

    data->xd = (float *) realloc(data->xd, size * sizeof(float));
    data->yd = (float *) realloc(data->yd, size * sizeof(float));

    data->mx = data->mx < size ? size : data->mx;
    data->my = data->my < size ? size : data->my;
    data->nx = size;
    data->ny = size;

    for (int i = 0; i < size; ++i)
    {
        data->xd[i] = prof[i * 2];
        data->yd[i] = prof[i * 2 + 1];
        ++i;
    }

    g_plot->need_to_refresh = ALL_NEED_REFRESH;
    //pltreg *cur_pr = NULL;
    //ac_grep(cur_ac_reg,"%im", &cur_pr);
    //if (cur_pr == g_pr)
    //    refresh_plot(g_pr, UNCHANGED);

}
void debug_plot_complex_int_profile(int ds_index, int *prof, int size)
{

    while (g_plot->n_dat <= ds_index)
    {
        create_and_attach_one_ds(g_plot, size, size , 0);
    }

    d_s *data = g_plot->dat[ds_index];

    data->xd = (float *) realloc(data->xd, size * sizeof(float));
    data->yd = (float *) realloc(data->yd, size * sizeof(float));

    data->mx = data->mx < size ? size : data->mx;
    data->my = data->my < size ? size : data->my;
    data->nx = size;
    data->ny = size;

    for (int i = 0; i < size; ++i)
    {
        data->xd[i] = prof[i * 2];
        data->yd[i] = prof[i * 2 + 1];
        ++i;
    }

    g_plot->need_to_refresh = ALL_NEED_REFRESH;
    //pltreg *cur_pr = NULL;
    //ac_grep(cur_ac_reg,"%im", &cur_pr);
    //if (cur_pr == g_pr)
    //    refresh_plot(g_pr, UNCHANGED);

}
void debug_plot_complex_int_real_part_profile(int ds_index, int *prof, int size)
{

    while (g_plot->n_dat <= ds_index)
    {
        create_and_attach_one_ds(g_plot, size, size , 0);
    }

    d_s *data = g_plot->dat[ds_index];

    data->xd = (float *) realloc(data->xd, size * sizeof(float));
    data->yd = (float *) realloc(data->yd, size * sizeof(float));

    data->mx = data->mx < size ? size : data->mx;
    data->my = data->my < size ? size : data->my;
    data->nx = size;
    data->ny = size;

    for (int i = 0; i < size; ++i)
    {
        data->xd[i] = i;
        data->yd[i] = prof[i * 2];
        ++i;

    }

    g_plot->need_to_refresh = ALL_NEED_REFRESH;
    //pltreg *cur_pr = NULL;
    //ac_grep(cur_ac_reg,"%im", &cur_pr);
    //if (cur_pr == g_pr)
    //    refresh_plot(g_pr, UNCHANGED);

}

void debug_profiles_xy_to_cpu(gpu_event_driven_memory *gpu_profs, cpu_crosses *crosses, int gpu_cross_length)
{

    cl_int profs_size = gpu_cross_length * crosses->size * 2;
    cl_float *profs = (cl_float *) calloc(profs_size, sizeof(cl_float));
    cl_float *profs_vert = profs + gpu_cross_length * crosses->size;

    CLCHECK(clEnqueueReadBuffer(get_dth_queue(),
                                gpu_profs->mem,
                                CL_TRUE,
                                0,
                                sizeof(cl_float) * profs_size,
                                profs,
                                1,
                                &gpu_profs->event,
                                NULL));
    clFinish(get_dth_queue());

    for (int i = 0; i < crosses->size; ++i)
    {
        cpu_bead *bead = crosses->cross_data[i]->bead;

        if (bead == NULL)
        {
            bead = crosses->cross_data[i]->bead = (cpu_bead *) calloc(1, sizeof(cpu_bead));
            bead->avg_x_profile = (float *) calloc(gpu_cross_length, sizeof(float));
            bead->avg_y_profile = (float *) calloc(gpu_cross_length, sizeof(float));
            bead->radial_z_profile = (float *) calloc(gpu_cross_length, sizeof(float));
            bead->radial_profile_size = gpu_cross_length;
        }

        {
            memcpy(bead->avg_x_profile, profs + i * gpu_cross_length, sizeof(cl_float) * gpu_cross_length);
            memcpy(bead->avg_y_profile, profs_vert + i * gpu_cross_length, sizeof(cl_float) * gpu_cross_length);
        }
    }

    free(profs);
}
void debug_profiles_xy_to_gpu(gpu_event_driven_memory *gpu_profs, cpu_crosses *crosses, int gpu_cross_length)
{
    cl_event write_gpu_profs_event;

    cl_int profs_size = gpu_cross_length * crosses->size * 2;
    cl_float *profs = (cl_float *) calloc(profs_size, sizeof(cl_float));
    cl_float *profs_vert = profs + gpu_cross_length * crosses->size;

    for (int i = 0; i < crosses->size; ++i)
    {
        cpu_bead *bead = crosses->cross_data[i]->bead;

        if (bead != NULL)
        {
            memcpy(profs + i * gpu_cross_length,      bead->avg_x_profile, sizeof(cl_float) * gpu_cross_length);
            memcpy(profs_vert + i * gpu_cross_length, bead->avg_y_profile, sizeof(cl_float) * gpu_cross_length);
        }
    }

    CLCHECK(clEnqueueWriteBuffer(get_dth_queue(),
                                 gpu_profs->mem,
                                 CL_TRUE,
                                 0,
                                 sizeof(cl_float) * profs_size,
                                 profs,
                                 1,
                                 &gpu_profs->event,
                                 &write_gpu_profs_event));
    clReleaseEvent(gpu_profs->event);
    gpu_profs->event = write_gpu_profs_event;

    free(profs);
}

void debug_xy_pos_to_cpu(gpu_event_driven_memory *gpu_beads_pos, cpu_crosses *crosses)
{
    cpu_coord *beads_pos = crosses->beads_pos;


    CLCHECK(clEnqueueReadBuffer(get_dth_queue(),
                                gpu_beads_pos->mem,
                                CL_TRUE,
                                0,
                                sizeof(cpu_coord) * crosses->size,
                                beads_pos,
                                1,
                                &gpu_beads_pos->event,
                                NULL));

    for (int i = 0; i < crosses->size; ++i)
    {
        cpu_bead *bead = crosses->cross_data[i]->bead;

        if (bead)
        {
            bead->x = beads_pos[i].x;
            bead->y = beads_pos[i].y;
        }
    }
}

void debug_xy_pos_to_gpu(gpu_event_driven_memory *gpu_beads_pos, cpu_crosses *crosses)
{

    cpu_coord *beads_pos = crosses->beads_pos;

    for (int i = 0; i < crosses->size; i++)
    {
        cpu_bead_cross *cross = crosses->cross_data[i];
        assert(cross != NULL);

        if (cross->bead != NULL)
        {
            cpu_bead *bead = cross->bead;
            beads_pos[i].x = bead->x;
            beads_pos[i].y = bead->y;
            //printf("%f,%f;\n", beads_center[i], beads_center[i+1]);
        }
        else
        {
            // the bead is not found
            beads_pos[i].x = cross->last_tracked_x;
            beads_pos[i].y = cross->last_tracked_y;
            //printf("notfound : %f,%f;\n", beads_center[i], beads_center[i+1]);
        }
    }

    clReleaseEvent(gpu_beads_pos->event);
    CLCHECK(clEnqueueWriteBuffer(get_htd_queue(), gpu_beads_pos->mem, CL_TRUE, 0 ,
                                 sizeof(cpu_coord) * crosses->size, beads_pos, 0, NULL, &gpu_beads_pos->event));
}

void debug_profiles_z_to_cpu(cpu_crosses *crosses, gpu_event_driven_memory *gpu_profs, int prof_size)
{

    cl_int profs_size = prof_size * crosses->size;
    cl_float *profs = (cl_float *) calloc(profs_size, sizeof(cl_float));

    CLCHECK(clEnqueueReadBuffer(get_dth_queue(),
                                gpu_profs->mem,
                                CL_TRUE,
                                0,
                                sizeof(cl_float) * profs_size,
                                profs,
                                1,
                                &(gpu_profs->event),
                                NULL));

    for (int i = 0; i < crosses->size; ++i)
    {
        cpu_bead *bead = crosses->cross_data[i]->bead;

        if (bead != NULL)
        {
            memcpy(bead->radial_z_profile, profs + i * prof_size, sizeof(cl_float) * prof_size);
        }
    }

    free(profs);
}

void debug_profiles_z_to_gpu(cpu_crosses *crosses, gpu_event_driven_memory *gpu_profs, int prof_size)
{

    cl_int profs_size = prof_size * crosses->size;
    cl_float *profs = (cl_float *) calloc(profs_size, sizeof(cl_float));

    for (int i = 0; i < crosses->size; ++i)
    {
        cpu_bead *bead = crosses->cross_data[i]->bead;

        if (bead != NULL)
        {
            memcpy(profs + i * prof_size, bead->radial_z_profile, sizeof(cl_float) * prof_size);
        }
    }

    cl_event copy_event;
    CLCHECK(clEnqueueWriteBuffer(get_dth_queue(),
                                 gpu_profs->mem,
                                 CL_TRUE,
                                 0,
                                 sizeof(cl_float) * profs_size,
                                 profs,
                                 1,
                                 &(gpu_profs->event),
                                 &copy_event));

    clReleaseEvent(gpu_profs->event);
    gpu_profs->event = copy_event;
}

void debug_plot_gpu_profile(int ds_index, gpu_event_driven_memory *prof, int size)
{
    cl_float *buf = (cl_float *) calloc(size, sizeof(cl_float));

    CLCHECK(clEnqueueReadBuffer(get_dth_queue(),
                                prof->mem,
                                CL_TRUE,
                                0,
                                size * sizeof(cl_float),
                                buf,
                                1,
                                &(prof->event),
                                NULL));

    debug_plot_profile(g_plot->dat[ds_index], buf, size);

}

void debug_plot_complex_gpu_profile(int ds_index, gpu_event_driven_memory *prof, int size)
{
    cl_float *buf = (cl_float *) calloc(size * 2, sizeof(cl_float));

    CLCHECK(clEnqueueReadBuffer(get_dth_queue(),
                                prof->mem,
                                CL_TRUE,
                                0,
                                size * sizeof(cl_float) * 2,
                                buf,
                                1,
                                &(prof->event),
                                NULL));

    debug_plot_complex_profile(ds_index, buf, size);
}
void debug_plot_complex_int_gpu_profile(int ds_index, gpu_event_driven_memory *prof, int size)
{
    cl_int *buf = (cl_int *) calloc(size * 2, sizeof(cl_float));

    CLCHECK(clEnqueueReadBuffer(get_dth_queue(),
                                prof->mem,
                                CL_TRUE,
                                0,
                                size * sizeof(cl_float) * 2,
                                buf,
                                1,
                                &(prof->event),
                                NULL));

    debug_plot_complex_int_real_part_profile(ds_index, buf, size);
}

void debug_plot_int_gpu_profile(int ds_index, gpu_event_driven_memory *prof, int size)
{
    cl_int *buf = (cl_int *) calloc(size, sizeof(cl_int));

    CLCHECK(clEnqueueReadBuffer(get_dth_queue(),
                                prof->mem,
                                CL_TRUE,
                                0,
                                size * sizeof(cl_float),
                                buf,
                                1,
                                &(prof->event),
                                NULL));

    debug_plot_int_profile(ds_index, buf, size);

}
