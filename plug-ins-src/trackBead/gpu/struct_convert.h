#pragma once

#include "xvin.h"
#include "../track_util.h"
#include "track_struct.h"

void trackbead_from_pico(O_i* char_io, g_track* gt, cpu_image** outimage,
                         cpu_crosses **crosses, int preferred_calibration_type);

cpu_image *cpu_chario_to_image(O_i * char_io);

cpu_image_complex *cpu_complexio_to_complex_image(O_i *complexio);
void cpu_image_complex_free(cpu_image_complex *image);
void gpu_event_driven_memory_free(gpu_event_driven_memory *mem);
void cpu_to_pico(cpu_crosses* crosses, g_track* gt, cl_int cur_buf_pos);
void cpu_image_free(cpu_image *image);
cpu_crosses *create_crosses(cl_int max_size);
void cpu_bead_param_free(cpu_bead_param *bead_param);
void cpu_crosses_free (cpu_crosses *bead_params);
void cpu_crosses_trunc(cpu_crosses *bead_params, cl_int trunc_idx);
bool pico_check_calibration_image_uniform(g_track *gt);
bool pico_check_generic_calibration_image_uniform(g_track *gt);
int best_calibration_type_available(g_track *gt);
void cpu_image_to_gpu(cpu_image *image);
void cpu_complex_image_to_gpu(cpu_image_complex *image);
void cpu_bead_cross_free(cpu_bead_cross *cross);
