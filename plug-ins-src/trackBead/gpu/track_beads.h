#pragma once
#include "track_struct.h"
#include "../track_util.h"

#define TRACKING_DEVICE_GPU 2
#define TRACKING_DEVICE_CPU 4
#define TRACKING_DISABLE 8
#define TRACKING_BOTH 10

void proceed_beads_gpu(O_i *oi, g_track *gt, int ci);
bool is_gpu_tracking_enable(void);
bool is_normal_tracking_enable(void);
int get_tracking_device(void);
PXV_FUNC(int, set_tracking_device,(int tracking_device));

void track_beads(cpu_image* image, cpu_crosses* crosses);

int	trackBead_gpu_main(int argc, char **argv);
int	trackBead_gpu_unload(int /*argc*/, char ** /*argv*/);
MENU *trackBead_gpu_plot_menu(void);
int do_change_tracking_device(void);
int do_change_cl_plateform(void);
int do_choose_calibration_method(void);
int do_set_current_as_reference_calibration(void);
