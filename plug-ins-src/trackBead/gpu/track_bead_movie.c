#include "track_bead_movie.h"
#include "xvin.h"
#include "../record.h"
#include "../fillibbt.h"
#include "../../fft2d/fftbtl32n.h"
#include "track_struct.h"
#include "gpu_track_utils.h"
#include "struct_convert.h"
#include "debug_tools.h"
#include "assert.h"
int do_track_bead_from_movie_gpu(void)
{
    int i;
    O_i *ois = NULL, *oid = NULL, *oic = NULL, *oicf = NULL;
    union pix  *pd = NULL;
    O_p *op = NULL;
    d_s *dsx = NULL, *dsy = NULL, *dsz = NULL;
    static int start = 0, end = 2048, prof_type = 0, cl = 0, cw = 0, xc0 = 0, yc0 = 0;
    static int fc = 0, fw = 0, rc = 0, np = 0;
    imreg *imr = NULL;
    char question[2048] = {0};
    int xc, yc, nf;
    float xcf, ycf;
    fft_plan *xy_trp = NULL;
    filter_bt *xy_fil = NULL;
    fft_plan *z_trp = NULL;
    filter_bt *z_fil = NULL;
    char *seed = "Tracking movie from bead";//, chtmp[128] = {0};
    int i_bead = -1;
    b_record *br = NULL;
    g_record *g_r = NULL;
    static int dis_filter, max_limit, bd_mul;
    int lost_this_time, not_lost, imi, imil;
    static int black_circle = 0, stop_xy_tracking = 0;
    cl_int err;


    ac_grep(cur_ac_reg, "%imr%oi", &imr, &ois);

    if (updating_menu_state != 0)
    {
        // we wait for recording finish
        if ((ois == NULL) || (ois->im.n_f <= 1) || (ois->im.source == NULL)
                || (strncmp(seed, ois->im.source, strlen(seed))))
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }

    if (imr == NULL || ois == NULL || ois->im.n_f <= 1)
        return win_printf_OK("No movie found !");

    i = sscanf(ois->im.source, "Tracking movie from bead %d", &i_bead);

    if (i != 1) return win_printf_OK("Cannot find bead number from tracking movie !");

    g_r = find_g_record_in_imreg(imr);

    if (g_r == NULL) return win_printf_OK("Cannot find tracking record!");

    if (i_bead >= 0 && i_bead < g_r->n_bead) br = g_r->b_r[i_bead];
    else return win_printf_OK("Bead number out of tracking record range !");

    oic = br->calib_im;

    /*
       snprintf(chtmp,sizeof(chtmp),"equally spaced reference profile\nBead%d",i_bead);
       oic = find_source_specific_oi_in_imr(imr, chtmp, &n_oic);
       if (oic == NULL) return win_printf_OK("Cannot find calibration image of bead %d!",i_bead);
       */

    if (cl == 0) cl = br->cl;

    if (cw == 0) cw = br->cw;

    if (fc == 0) fc = br->bp_filter;

    if (fw == 0) fw = br->bp_width;

    if (rc == 0) rc = br->forget_circle;


    if ((dis_filter == 0) || (dis_filter > (cl >> 2))) dis_filter = cl >> 2;

    if ((max_limit == 0) || (max_limit > (cl > 1)))    max_limit = cl >> 1;

    bd_mul = 16;

    lost_this_time = 0;
    not_lost = 1;
    black_circle = 0;

    nf = (ois->im.n_f < 1) ? 1 : ois->im.n_f;

    if (xc0 == 0) xc0 = ois->im.nx / 2;

    if (yc0 == 0) yc0 = ois->im.ny / 2;

    end = (end > nf) ? nf : end;

    snprintf(question, sizeof(question),
             "Tracking of bead movie. Define starting and ending points index\n"
             "Starting %%6d ending %%6d in [0,%d[\n"
             "%%RProfile in X, "
             "%%rin Y, "
             "%%rX-convo, "
             "%%rY-convo, "
             "%%rZ profile, "
             "%%r No profile.\n"
             "Bead center xc %%6d yc %%6d Cross size length %%6d width %%6d\n"
             "Tracking in xy, (low pass filter %%6d,extend of covolution max search %%6d)\n"
             "Z tracking bp-fil %%6d w-fil %%6d forget circle %%6d\n"
             "%%RTracks XY %%rX only %%rY only %%rDo not track\n"
             , nf);

    i = win_scanf(question, &start, &end, &prof_type, &xc0, &yc0, &cl, &cw, &dis_filter,
                  &max_limit , &fc, &fw, &rc, &stop_xy_tracking);

    if (i == CANCEL)
        return D_O_K;

    xy_trp = fftbt_init(xy_trp, cl);
    xy_fil = filter_init_bt(xy_fil, cl);

    if (xy_trp == NULL || xy_fil == NULL) return win_printf_OK("cannot init fft!");

    np = oic->im.nx;

    if (np != br->nx_prof) win_printf_OK("nx_prof (%d) != np (%d)!", br->nx_prof, np);

    z_trp = fftbt_init(NULL, np);
    z_fil = filter_init_bt(NULL, np);

    if (fc != br->bp_filter || fw != br->bp_width ||
            rc != br->forget_circle || br->calib_im_fil == NULL)
    {
        oicf = image_band_pass_in_real_out_complex(NULL, oic, 0, fc, fw, rc, z_trp, z_fil);
    }
    else oicf = br->calib_im_fil;

    if (prof_type < 4)
    {
        oid = create_and_attach_oi_to_imr(imr, cl, end - start, IS_LINT_IMAGE);

        if (oid == NULL)  return win_printf_OK("cannot create profile image !");

        pd = oid->im.pixel;

        if (prof_type == 0)
        {
            set_oi_source(oid, "Bead x profiles from im %d to %d at xc %d yc %d cl %d cw %d"
                          , start, end, xc0, yc0, cl, cw);
            set_im_title(oid, "Bead x profiles from im %d to %d", start, end);
        }
        else if (prof_type == 1)
        {
            set_oi_source(oid, "Bead y profiles from im %d to %d at xc %d yc %d cl %d cw %d"
                          , start, end, xc0, yc0, cl, cw);
            set_im_title(oid, "Bead y profiles from im %d to %d", start, end);
        }
        else if (prof_type == 2)
        {
            set_oi_source(oid, "Bead x convolution profile from im %d to %d at xc %d yc %d cl %d cw %d"
                          , start, end, xc0, yc0, cl, cw);
            set_im_title(oid, "Bead x convolution profile from im %d to %d", start, end);
        }
        else if (prof_type == 3)
        {
            set_oi_source(oid, "Bead y convolution profile from im %d to %d at xc %d yc %d cl %d cw %d"
                          , start, end, xc0, yc0, cl, cw);
            set_im_title(oid, "Bead y convolution profile from im %d to %d", start, end);
        }
    }

    if (prof_type == 4)
    {
        oid = create_and_attach_oi_to_imr(imr, cl / 2, end - start, IS_COMPLEX_IMAGE);

        if (oid == NULL)
            return win_printf_OK("cannot create profile image !");

        pd = oid->im.pixel;
        set_oi_source(oid, "Bead Z profiles from im %d to %d at xc %d yc %d cl %d cw %d"
                      , start, end, xc0, yc0, cl, cw);
        set_im_title(oid, "Bead Z profiles from im %d to %d", start, end);
    }

    op = create_and_attach_op_to_oi(ois, end - start, end - start, 0, 0);

    if (op == NULL)  return win_printf_OK("cannot create plot !");

    dsx = op->dat[0];
    set_ds_source(dsx, "Axis x");

    if ((dsy = create_and_attach_one_ds(op, end - start, end - start, 0)) == NULL)
        return win_printf_OK("I can't create dsy !");

    set_ds_source(dsy, "Axis y");

    if ((dsz = create_and_attach_one_ds(op, end - start, end - start, 0)) == NULL)
        return win_printf_OK("I can't create dsy !");

    set_ds_source(dsz, "Axis z");

    cpu_crosses *crosses = create_crosses(1);
    crosses->calib_type = XV_CALIB_TYPE_CUSTOM_IMG;
    crosses->update_reference_profile = false;
    crosses->parameter_has_changed = false;
    crosses->bandpass_width = fw;
    crosses->bandpass_center = fc;
    crosses->length = cl;
    crosses->width = cw;
    crosses->max_meaningful_radius = rc;
    crosses->beads_pos[0].x = xc0;
    crosses->beads_pos[0].y = yc0;
    crosses->cross_data[0] = (cpu_bead_cross *) calloc(1, sizeof(cpu_bead_cross));
    crosses->cross_data[0]->last_tracked_x = xc0;
    crosses->cross_data[0]->last_tracked_y = yc0;
    crosses->cross_data[0]->x = xc0;
    crosses->cross_data[0]->y = yc0;

    crosses->calib_data = (cpu_calib_images *) calloc(1, sizeof(cpu_calib_images));

    assert(cl == oicf->im.nx * 2);

    crosses->calib_data->height = oicf->im.ny;
    crosses->calib_data->width = oicf->im.nx * 2;
    crosses->calib_data->ay = oicf->ay;
    crosses->calib_data->dy = oicf->dy;
    crosses->calib_data->filtered_calib_data.fl = (cl_float *) oicf->im.mem[0];

    printf("%d, %d\n", crosses->calib_data->height,crosses->calib_data->width  );

    crosses->calib_data->max_size = 1;
    crosses->calib_data->size = 1;
    crosses->size = 1;

    init_opencl_last_config();
    gpu_tracking_reset();
    gpu_init_kernels(crosses);

    gpu_event_driven_memory *gpu_profs = NULL;
    gpu_event_driven_memory *gpu_conv_profs = NULL;
    gpu_event_driven_memory *gpu_beads_pos = NULL;
    gpu_event_driven_memory *gpu_beads_z_pos = NULL;

    gpu_init_mem(&gpu_profs, &gpu_beads_pos, &gpu_beads_z_pos, crosses->beads_pos, crosses->length, crosses->size);

    gpu_conv_profs = (gpu_event_driven_memory *) calloc(1, sizeof(gpu_event_driven_memory));
    gpu_conv_profs->event = clCreateUserEvent(get_context(), &err);
    clSetUserEventStatus((gpu_conv_profs)->event, CL_COMPLETE);

    gpu_conv_profs->mem = clCreateBuffer(get_context(), CL_MEM_READ_WRITE,
                                         sizeof(cl_float) * crosses->length * 2 * crosses->size,
                                         NULL, &err);



    crosses->calib_data->gpu_filtered_calib_data.mem = clCreateBuffer(get_context(),
            CL_MEM_READ_ONLY,
            crosses->calib_data->height *
            crosses->calib_data->width *
            crosses->calib_data->size *
            sizeof(cl_float),
            NULL,
            &err);
    CLCHECK(clEnqueueWriteBuffer(get_htd_queue(),
                                 crosses->calib_data->gpu_filtered_calib_data.mem,
                                 CL_TRUE, 0,
                                 crosses->calib_data->height *
                                 crosses->calib_data->width *
                                 crosses->calib_data->size *
                                 sizeof(cl_float),
                                 crosses->calib_data->filtered_calib_data.fl,
                                 0, NULL , &(crosses->calib_data->gpu_filtered_calib_data.event)));




    for (imil = start, imi = 0, xcf = xc0, xc = xc0, ycf = yc0, yc = yc0; imil < end && imi < oid->im.ny; imi++, imil++)
    {
        switch_frame(ois, imil);
        cpu_image *gpu_img = cpu_chario_to_image(ois);
        cpu_bead_cross *cross = crosses->cross_data[0];


        //xc = (int)(0.5+xcf);
        //yc = (int)(0.5+ycf);
        dsx->xd[imi] = dsy->xd[imi] = dsz->xd[imi] = imil;


        //gpu_init_mem(&gpu_profs, &gpu_beads_pos, &gpu_beads_z_pos, crosses->beads_pos, crosses->length, crosses->size);
        debug_xy_pos_to_gpu(gpu_beads_pos, crosses);

        gpu_fill_profiles_xy(gpu_img, crosses->size, gpu_profs, gpu_beads_pos);
        debug_profiles_xy_to_cpu(gpu_profs, crosses, cl);


        if (prof_type == 0)
        {
            for (int k = 0; k < cl; k++)
                pd[imi].li[k] = cross->bead->avg_x_profile[k];
        }

        if (prof_type == 1)
        {
            for (int k = 0; k < cl; k++)
                pd[imi].li[k] = cross->bead->avg_y_profile[k];
        }

        gpu_track_xy(crosses->size, gpu_profs, gpu_beads_pos, gpu_conv_profs);
        debug_profiles_xy_to_cpu(gpu_conv_profs, crosses, cl);
        debug_xy_pos_to_cpu(gpu_beads_pos, crosses);

        if (prof_type == 2)
        {
            for (int k = 0; k < cl; k++)
                pd[imi].li[k] = cross->bead->avg_x_profile[k];
        }
        else if (prof_type == 3)
        {
            for (int k = 0; k < cl; k++)
                pd[imi].li[k] = cross->bead->avg_y_profile[k];
        }

        // FIXME Check lost
        if (stop_xy_tracking == 0)
        {

            //if (debug != CANCEL) debug = win_printf("imi %d\nxc = %d dx = %g xcf = %g \n"
            //                                            "yc = %d dy = %g ycf = %g\nCancel to stop\n"
            //                                            , imi, xc, dx, xcf, yc, dy, ycf);
        }
        else if (stop_xy_tracking == 1)
        {
            cross->bead->y = yc0;
        }
        else if (stop_xy_tracking == 2)
        {
            cross->bead->x = xc0;
        }
        else if (stop_xy_tracking == 3)
        {
            cross->bead->x = xc0;
            cross->bead->y = yc0;
        }


        debug_xy_pos_to_gpu(gpu_beads_pos, crosses);

        gpu_fill_profile_z(gpu_img, crosses->size, gpu_profs, gpu_beads_pos);


        gpu_track_z(crosses, gpu_profs, gpu_beads_z_pos);
        // FIXME Check lost

        debug_profiles_z_to_cpu(crosses, gpu_profs, crosses->length);

        if (prof_type == 4)
        {
            for (int k = 0; k < np; k++)
            {
                pd[imi].fl[k] = crosses->cross_data[0]->bead->radial_z_profile[k];
            }
        }


        dsx->yd[imi] = cross->bead->x;               // we save its new x, y position
        dsy->yd[imi] = cross->bead->y;
        dsz->yd[imi] = cross->bead->z;

        crosses->beads_pos[0].x = cross->last_tracked_x = cross->bead->x = round(cross->bead->x);
        crosses->beads_pos[0].y = cross->last_tracked_y = cross->bead->y = round(cross->bead->y);
    }

    O_i *oicalib = create_and_attach_oi_to_imr(imr, crosses->calib_data->width / 2, crosses->calib_data->height,
                   IS_COMPLEX_IMAGE);
    oicalib->im.mode = oicf->im.mode;

    for (int i = 0; i < crosses->calib_data->height; ++i)
    {
        for (int j = 0; j < crosses->calib_data->width; ++j)
            ((float *) oicalib->im.mem[0])[i * crosses->calib_data->width + j] =
                crosses->calib_data->filtered_calib_data.fl[crosses->calib_data->width * i + j];
    }


    if (oid)
        find_zmin_zmax(oid);

    if (oicf != br->calib_im_fil)
        free_one_image(oicf);

    return refresh_image(imr, imr->n_oi - 1);
}
