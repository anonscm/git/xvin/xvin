#pragma once
#include "track_struct.h"

int debug_init_plot();
PXV_FUNC(void, output_debug_image,(const cpu_crosses *crosses, const cpu_image *image, const cpu_bead_cross *cross, const cl_float *xprof, const cl_float *yprof));
void output_debug_profiles(const cpu_crosses *crosses, const cl_float *profs, cl_int nb_profs);
void output_debug_crosses_beads(const cpu_image *image, cpu_crosses *crosses);
void debug_plot_profile(d_s *data, float *prof, int size);
void debug_plot_complex_profile(int ds_index, float *prof, int size);

void debug_profiles_xy_to_cpu(gpu_event_driven_memory* gpu_profs, cpu_crosses * crosses,  int gpu_cross_length);
void debug_profiles_xy_to_gpu(gpu_event_driven_memory* gpu_profs, cpu_crosses * crosses,  int gpu_cross_length);
void debug_profiles_z_to_gpu(cpu_crosses *crosses,  gpu_event_driven_memory *gpu_profs, int prof_size);
void debug_profiles_z_to_cpu(cpu_crosses *crosses,  gpu_event_driven_memory *gpu_profs, int prof_size);
void debug_xy_pos_to_cpu(gpu_event_driven_memory* gpu_beads_pos, cpu_crosses * crosses);
void debug_xy_pos_to_gpu(gpu_event_driven_memory* gpu_beads_pos, cpu_crosses * crosses);
void debug_plot_gpu_profile(int ds_index, gpu_event_driven_memory *prof, int size);
void debug_plot_complex_gpu_profile(int ds_index, gpu_event_driven_memory *prof, int size);
void debug_plot_int_gpu_profile(int ds_index, gpu_event_driven_memory *prof, int size);
void debug_plot_complex_int_gpu_profile(int ds_index, gpu_event_driven_memory *prof, int size);
void debug_plot_complex_int_profile(int ds_index, int *prof, int size);
