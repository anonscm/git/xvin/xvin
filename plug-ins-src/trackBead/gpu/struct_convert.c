#include "struct_convert.h"
#include <assert.h>
#include <ltpv.h>
#include "debug_tools.h"
#include "../calibration.h"

bool first = true;

// Those functions are needed to simplify data structure for bead tracking, in the futur, this codes aims
// to disappear.

void trackbead_from_pico(O_i *char_io, g_track *gt, cpu_image **outimage,
                         cpu_crosses **outcrosses, int preferred_calibration_type)
{
    b_track *first_bead = gt->bd[0];
    cpu_crosses *crosses = NULL;
    assert(char_io);
    assert(gt);
    assert(outimage);
    //    assert(calib_imgs);

    if (*outcrosses == NULL)
    {
        *outcrosses = (cpu_crosses *) calloc(1, sizeof(cpu_crosses));
        (*outcrosses)->calib_data = (cpu_calib_images *) calloc(1, sizeof(cpu_calib_images));
        (*outcrosses)->calib_type = XV_CALIB_TYPE_GENERIC_IMG;
    }

    crosses = *outcrosses;

    int bead_count = gt->n_b;
    cpu_image *image = cpu_chario_to_image(char_io);

    int best_calib_type = my_min(preferred_calibration_type, best_calibration_type_available(gt));

    if (crosses->size != bead_count || (best_calib_type != crosses->calib_type))
    {
        crosses->parameter_has_changed = true;
        cpu_calib_images *calib_images = crosses->calib_data;
        calib_images->size = bead_count;
        calib_images->max_size = bead_count;


        if (bead_count > crosses->max_size)
        {
            crosses->bead_param = (cpu_bead_param **) realloc(crosses->bead_param, bead_count * sizeof(cpu_bead_param *));
            crosses->cross_data = (cpu_bead_cross **) realloc(crosses->cross_data, bead_count * sizeof(cpu_bead_cross *));
            crosses->beads_pos = (cpu_coord *) realloc(crosses->beads_pos, bead_count * sizeof(cpu_coord));
            crosses->max_size = bead_count;

        }

        //#pragma omp parallel for
        for (int i = crosses->size; i < bead_count; ++i)
        {
            crosses->bead_param[i] = (cpu_bead_param *) calloc(1, sizeof(cpu_bead_param));
        }


        if (best_calib_type == XV_CALIB_TYPE_GENERIC_IMG)
        {
            calib_images->height = first_bead->generic_calib_im_fil->im.ny;
            calib_images->width = first_bead->generic_calib_im_fil->im.nx * 2;
            calib_images->ay = first_bead->generic_calib_im_fil->ay;
            calib_images->dy = first_bead->generic_calib_im_fil->dy;
        }
        else if (best_calib_type == XV_CALIB_TYPE_CUSTOM_IMG)
        {

            calib_images->height = first_bead->calib_im_fil->im.ny;
            calib_images->width = first_bead->calib_im_fil->im.nx * 2;
            calib_images->ay = first_bead->calib_im_fil->ay;
            calib_images->dy = first_bead->calib_im_fil->dy;

            if (first_bead->calib_im_fil->o_p[0])
            {
                calib_images->z_ajust; //FIXME
            }

        }
        else //best_calib_type == XV_CALIB_TYPE_NONE
        {
            crosses->update_reference_profile = true;
            calib_images->height = 1;
            calib_images->width = first_bead->cl;
            calib_images->ay = 0 ;//first_bead->calib_im_fil->ay;
            calib_images->dy = 1 ;//first_bead->calib_im_fil->dy;
        }

        if (calib_images->filtered_calib_data.fl)
        {
            free(calib_images->filtered_calib_data.fl);
            calib_images->filtered_calib_data.fl = NULL;
        }

        calib_images->filtered_calib_data.fl = (cl_float *) calloc(calib_images->height *
                                               calib_images->width *
                                               calib_images->size,
                                               sizeof(cl_float));


        for (int i = 0; i < bead_count; ++i)
        {
            b_track *bt = gt->bd[i];
            cl_float *current_calib_img = crosses->calib_data->filtered_calib_data.fl +
                                          calib_images->height *
                                          calib_images->width * i;

            cpu_bead_param *bead_param = crosses->bead_param[i];
            bead_param->fplan = gt->bd[i]->z_trp;
            bead_param->fil = gt->bd[i]->z_fil;

            if (best_calib_type == XV_CALIB_TYPE_GENERIC_IMG)
            {
                if (bt->generic_calib_im_fil == NULL)
                {
                    memcpy(current_calib_img, gt->generic_calib_im_fil[0] , calib_images->width * calib_images->height * sizeof(cl_float));
                }
                else
                {
                    memcpy(current_calib_img, bt->generic_calib_im_fil->im.mem[0] ,
                           calib_images->width * calib_images->height * sizeof(cl_float));
                }
            }
            else if (best_calib_type == XV_CALIB_TYPE_CUSTOM_IMG)
            {
                memcpy(current_calib_img, bt->calib_im_fil->im.mem[0], calib_images->width * calib_images->height * sizeof(cl_float));
            }
        }

        // GPU
        cl_int err;

        if (calib_images->gpu_filtered_calib_data.mem)
        {
            clReleaseMemObject(calib_images->gpu_filtered_calib_data.mem);
            clReleaseEvent(calib_images->gpu_filtered_calib_data.event);
            calib_images->gpu_filtered_calib_data.mem = NULL;
            calib_images->gpu_filtered_calib_data.event = NULL;
        }

        if (calib_images->gpu_z_ajust)
        {
            if (crosses->calib_data->gpu_z_ajust->mem)
            {
                clReleaseMemObject(calib_images->gpu_z_ajust->mem);
                clReleaseEvent(calib_images->gpu_z_ajust->event);
                calib_images->gpu_filtered_calib_data.mem = NULL;
                calib_images->gpu_filtered_calib_data.event = NULL;
            }

            free(calib_images->gpu_z_ajust);
            calib_images->gpu_z_ajust = NULL;
        }

        calib_images->gpu_filtered_calib_data.mem = clCreateBuffer(get_context(),
                CL_MEM_READ_ONLY,
                calib_images->height *
                calib_images->width *
                calib_images->size *
                sizeof(cl_float),
                NULL,
                &err);
        CLCHECK(err);

        if (best_calib_type > XV_CALIB_TYPE_NONE)
        {
            CLCHECK(clEnqueueWriteBuffer(get_htd_queue(),
                                         calib_images->gpu_filtered_calib_data.mem,
                                         CL_TRUE, 0,
                                         calib_images->height *
                                         calib_images->width *
                                         calib_images->size *
                                         sizeof(cl_float),
                                         calib_images->filtered_calib_data.fl,
                                         0, NULL , &calib_images->gpu_filtered_calib_data.event));
        }

        crosses->size = bead_count;
        crosses->calib_type = best_calib_type;

        first = true;


        //for (int i = 0; i < calib_images->height; ++i)
        //{
        //debug_plot_profile(i, cpu_calib_line(calib_images, 0, i) , calib_images->width);
        //}

    }

    //    update_calib_images(*outcrosses, *outcross_count, *calib_imgs);


    //every time
    #pragma omp parallel for

    for (int i = 0; i < bead_count; ++i)
    {
        cpu_bead_cross *cross = (cpu_bead_cross *) malloc(sizeof(cpu_bead_cross));

        b_track *bt = gt->bd[i];


# if !defined(PLAYITSAM) && !defined(PIAS)
        position_cross_at_fence_limit(bt);
#endif

        cross->last_tracked_x = bt->x0;
        cross->last_tracked_y = bt->y0;

        if (bt->mouse_draged)
        {
            cross->x = bt->mx;
            cross->y = bt->my;
        }
        else
        {
            cross->x = bt->xc;
            cross->y = bt->yc;
        }

        cross->in_image = cross->x < image->width && cross->x > 0 && cross->y < image->height && cross->y > 0;
        cross->lost_counter = bt->not_lost;
        cross->mouse_draged = bt->mouse_draged;
        cross->bead = NULL;
        crosses->cross_data[i] = cross;

#ifdef OLD

        if (first)
        {
            cross->last_tracked_x = cross->x = 500;
            cross->last_tracked_y = cross->y = 430;
        }

#endif
        crosses->beads_pos[i].x = cross->x;
        crosses->beads_pos[i].y = cross->y;

    }

    first = false;
    *outimage = image;

    if (crosses->length == 0)
    {
        crosses->length = first_bead->cl;
        crosses->width = first_bead->cw;
        crosses->max_meaningful_radius = first_bead->rc;
        crosses->bandpass_width = first_bead->bp_width;
        crosses->bandpass_center = first_bead->bp_center;
    }
}


cpu_image *cpu_chario_to_image(O_i *char_io)
{
    cl_int err = CL_SUCCESS;
    cpu_image *image = (cpu_image *) calloc(sizeof(cpu_image), 1);
    cl_mem current_gpu_image = NULL;

    image->width = char_io->im.nx;
    image->height = char_io->im.ny;
    image->data = (cl_uchar *) char_io->im.mem[char_io->im.c_f];
    assert(char_io->im.mem[char_io->im.c_f] != NULL);

    cpu_image_to_gpu(image);
    return image;
}

void cpu_image_to_gpu(cpu_image *image)
{
    cl_image_format imgformat = {CL_R, CL_UNSIGNED_INT8};
    size_t origin[3] = {0, 0, 0};
    size_t region[3] = {image->width, image->height, 1};
    cl_mem current_gpu_image = NULL;
    cl_int err;

    current_gpu_image = clCreateImage2D(get_context(), CL_MEM_READ_ONLY,  &imgformat , image->width, image->height, 0, NULL,
                                        &err);
    //#define XVIN_OPENCL_MAP_IMAGE
#ifdef XVIN_OPENCL_MAP_IMAGE
    cl_uchar *mappedmem = NULL;
    size_t row_pitch =  0;
    size_t slice_pitch = 0;
    cl_event map_event;

    mappedmem = (cl_uchar *) clEnqueueMapImage(get_htd_queue(), current_gpu_image, CL_TRUE, CL_MAP_WRITE , origin, region,
                &row_pitch, &slice_pitch, 0, NULL, &map_event, &err);
    CLCHECK(err);

    memcpy(mappedmem, image->data, sizeof(cl_uchar) * image->width * image->height);

    cl_event unmap_event;
    CLCHECK(clEnqueueUnmapMemObject(get_htd_queue(), current_gpu_image, mappedmem, 0, NULL, &unmap_event));
#else
    CLCHECK(clEnqueueWriteImage(get_htd_queue(), current_gpu_image, CL_TRUE, origin, region, 0, 0, image->data, 0, NULL ,
                                &image->gpu_data.event));
#endif
    image->gpu_data.mem = current_gpu_image;

}

cpu_crosses *create_crosses(cl_int max_size)
{
    cpu_crosses *res = (cpu_crosses *) calloc(1, sizeof(cpu_crosses));

    res->bead_param = (cpu_bead_param **) calloc(max_size, sizeof(cpu_bead_param));
    res->cross_data = (cpu_bead_cross **) calloc(max_size, sizeof(cpu_bead_cross));
    res->beads_pos = (cpu_coord *) calloc(max_size, sizeof(cpu_coord));
    res->calib_data = NULL;
    res->max_size = max_size;
    return res;
}

void cpu_crosses_free(cpu_crosses *crosses)
{

    if (crosses->calib_data->gpu_filtered_calib_data.mem)
    {
        clReleaseMemObject(crosses->calib_data->gpu_filtered_calib_data.mem);
        clReleaseEvent(crosses->calib_data->gpu_filtered_calib_data.event);
        crosses->calib_data->gpu_filtered_calib_data.mem = NULL;
        crosses->calib_data->gpu_filtered_calib_data.event = NULL;
    }

    if (crosses->calib_data->gpu_z_ajust)
    {
        if (crosses->calib_data->gpu_z_ajust->mem)
        {
            clReleaseMemObject(crosses->calib_data->gpu_z_ajust->mem);
            clReleaseEvent(crosses->calib_data->gpu_z_ajust->event);
            crosses->calib_data->gpu_z_ajust->mem = NULL;
            crosses->calib_data->gpu_z_ajust->event = NULL;
        }

        free(crosses->calib_data->gpu_z_ajust);
        crosses->calib_data->gpu_z_ajust = NULL;

    }


    cpu_crosses_trunc(crosses, 0);
    free(crosses->beads_pos);
    free(crosses);
}

void cpu_crosses_trunc(cpu_crosses *crosses, cl_int trunc_idx)
{
    for (int i = trunc_idx; i < crosses->size; ++i)
    {
        cpu_bead_param_free(crosses->bead_param[i]);
        cpu_bead_cross_free(crosses->cross_data[i]);
    }

    //cpu_calib_images_trunc(crosses->calib_data, trunc_idx);
    crosses->size = trunc_idx;
}
void cpu_bead_param_free(cpu_bead_param *bead_param)
{
    if (bead_param)
    {
        // Not allocated by us
        //free(bead_param->fil);
        //free(bead_param->fplan);
    }
}
void cpu_bead_cross_free(cpu_bead_cross *cross)
{
    if (cross->bead)
    {
        free(cross->bead->avg_x_profile);
        free(cross->bead->avg_y_profile);
        free(cross->bead->radial_z_profile);
        free(cross->bead);
        cross->bead = NULL;
    }

    free(cross);

}
void cpu_image_free(cpu_image *image)
{
    if (image != NULL)
    {
        //free(image->data);
        clReleaseMemObject(image->gpu_data.mem);
        clReleaseEvent(image->gpu_data.event);
        free(image);
    }
}
void cpu_image_complex_free(cpu_image_complex *image)
{
    if (image != NULL)
    {
        //free(image->data); // Not allocated by us
        clReleaseMemObject(image->gpu_data.mem);
        free(image);
    }
}

//cl_mem gpum = NULL;
//cl_event gpue = NULL;
cpu_image_complex *cpu_complexio_to_complex_image(O_i *complexio)
{
    cpu_image_complex *image = (cpu_image_complex *) calloc(1, sizeof(cpu_image_complex));
    image->height = complexio->im.ny;
    image->width = complexio->im.nx;
    image->ay = complexio->ay;
    image->dy = complexio->dy;
    image->data = (cl_float *) complexio->im.mem[0];
    assert(complexio->im.mem[0] != NULL);


    //gpum = clCreateBuffer(get_context(), CL_MEM_READ_ONLY, 2 * image->width * image->height * sizeof(cl_float), NULL, &err);
    //CLCHECK(err);
    //assert(image->data != NULL);
    //LCHECK(clEnqueueWriteBuffer(get_htd_queue(), gpum, CL_TRUE, 0, 2 * image->width * image->height * sizeof(cl_float), image->data, 0,NULL, &gpue));

    //image->gpu_data.mem = gpum;
    //image->gpu_data.event = gpue;

    cpu_complex_image_to_gpu(image);
    return image;
}

void cpu_complex_image_to_gpu(cpu_image_complex *image)
{
    cl_int err = CL_SUCCESS;
    image->gpu_data.mem = clCreateBuffer(get_context(), CL_MEM_READ_ONLY,
                                         2 * image->width * image->height * sizeof(cl_float), NULL, &err);
    CLCHECK(err);
    assert(image->data != NULL);
    CLCHECK(clEnqueueWriteBuffer(get_htd_queue(), image->gpu_data.mem, CL_TRUE, 0,
                                 2 * image->width * image->height * sizeof(cl_float), image->data, 0, NULL, &image->gpu_data.event));

}

void cpu_to_pico(cpu_crosses *crosses, g_track *gt, cl_int cur_buf_pos)
{

    #pragma omp parallel for

    for (int i = 0; i < crosses->size; ++i)
    {
        cpu_bead_cross *cross = crosses->cross_data[i];
        b_track *bt = gt->bd[i];

        if (cross->bead)
        {
            // TODO not very consistant with pico tracking system
            bt->x0 = round(cross->bead->x);
            bt->y0 = round(cross->bead->y);
            bt->xc = round(cross->bead->x);
            bt->yc = round(cross->bead->y);

            bt->x[cur_buf_pos] = cross->bead->x;
            bt->y[cur_buf_pos] = cross->bead->y;
            bt->z[cur_buf_pos] = gt->obj_pos[cur_buf_pos] - cross->bead->z;
            bt->min_xi2 = cross->bead->calib_min_xi2;
            bt->min_phi = cross->bead->calib_min_phi;
            bt->profile_index = cross->bead->calib_img_index;
            bt->in_image = cross->in_image; // to always make calibration

            bt->z_avg += bt->z[cur_buf_pos];
            bt->iz_avg++;

            if (bt->iz_avg >= 10)
            {
                bt->z_avgd = bt->z_avg / bt->iz_avg;
                bt->z_avg = 0;
                bt->iz_avg = 0;
            }

            //printf("%d\n", bt->z_track);
            bt->z_track =
                crosses->calib_type; // z_track = 0 no calibration image, z_track = 1 generic calib image, z_track = 2 calibrated image

            for (cl_uint j = 0; j < cross->bead->radial_profile_size; ++j)
            {
                bt->rad_prof_gpu[cur_buf_pos][j] = bt->rad_prof[cur_buf_pos][j] = cross->bead->radial_z_profile[j];
                //bt->rad_prof_ref[i] = cross->bead->radial_x_profile[i];
            }

            free(cross->bead->avg_x_profile);
            free(cross->bead->avg_y_profile);
            free(cross->bead->radial_z_profile);
            free(cross->bead);
            cross->bead = NULL;
        }

        //FIXME handle lost XY
        bt->not_lost = NB_FRAMES_BEF_LOST;//cross->lost_counter;
        bt->n_l[cur_buf_pos] = (char) bt->not_lost;

        free(cross);
        crosses->cross_data[i] = NULL;

    }
}

void gpu_event_driven_memory_free(gpu_event_driven_memory *mem)
{
    if (mem != NULL)
    {
        CLCHECK(clReleaseMemObject(mem->mem));
        mem->mem = NULL;
        CLCHECK(clReleaseEvent(mem->event));
        mem->event = NULL;
        free(mem);
    }
}


int best_calibration_type_available(g_track *gt)
{
    if (pico_check_calibration_image_uniform(gt))
    {
        return XV_CALIB_TYPE_CUSTOM_IMG;
    }

    //else if (pico_check_generic_calibration_image_uniform(gt))
    //{
    return XV_CALIB_TYPE_GENERIC_IMG;
    //}

    //return XV_CALIB_TYPE_NONE;
}

bool pico_check_calibration_image_uniform(g_track *gt)
{
    if (gt->n_b < 1)
        return true;

    if (gt->bd[0]->calib_im_fil == NULL)
        return false;

    int calib_height = gt->bd[0]->calib_im_fil->im.ny;
    int calib_width = gt->bd[0]->calib_im_fil->im.nx;

    for (int i = 1; i < gt->n_b; ++i)
    {
        if (gt->bd[i]->calib_im_fil == NULL ||
                gt->bd[i]->calib_im_fil->im.ny != calib_height ||
                gt->bd[i]->calib_im_fil->im.nx != calib_width)
        {
            return false;
        }
    }

    return true;
}

bool pico_check_generic_calibration_image_uniform(g_track *gt)
{
    if (gt->n_b < 1)
        return true;

    if (gt->bd[0]->generic_calib_im_fil == NULL)
        return false;

    int calib_height = gt->bd[0]->generic_calib_im_fil->im.ny;
    int calib_width = gt->bd[0]->generic_calib_im_fil->im.nx;

    for (int i = 1; i < gt->n_b; ++i)
    {
        if (gt->bd[i]->generic_calib_im_fil == NULL ||
                gt->bd[i]->generic_calib_im_fil->im.ny != calib_height ||
                gt->bd[i]->generic_calib_im_fil->im.nx != calib_width)
        {
            return false;
        }
    }

    return true;
}
