#include "cpu_track_utils.h"
#include "xvin.h"
#include "debug_tools.h"
#include <assert.h>
#include <float.h>
#include <ltpv.h>

cpu_bead *cpu_create_bead(cpu_bead_cross *cross, cl_int prof_size)
{
    cpu_bead *bead = (cpu_bead *) malloc(sizeof(cpu_bead));
    bead->x = cross->last_tracked_x;
    bead->y = cross->last_tracked_y;
    bead->z = 0;

    bead->radial_profile_size = prof_size;
    bead->avg_x_profile = (cl_float *) calloc(bead->radial_profile_size, sizeof(cl_float));
    bead->avg_y_profile = (cl_float *) calloc(bead->radial_profile_size, sizeof(cl_float));
    bead->radial_z_profile = (cl_float *) calloc(bead->radial_profile_size + 2, sizeof(cl_float));
    bead->calib_img_index = 0;
    cross->bead = bead;
    return bead;
}

void cpu_create_beads(const cpu_image *image, cpu_crosses *crosses)
{
#pragma omp parallel for
    for (cl_int i = 0; i < crosses->size; ++i)
    {

        cpu_bead_cross *cross = crosses->cross_data[i];
        if (cpu_bound_cross_position(cross, image, crosses->length)) // the bead position is correct, we track the bead
        {
            cross->bead = cpu_create_bead(cross, crosses->length);
        }
    }
}

bool cpu_bound_cross_position(cpu_bead_cross *cross, const cpu_image *image, cl_int cross_len)
{
    cl_bool in_image = cpu_cross_in_image(cross, image); //TODO handle fence circle
    if (in_image /*&& !cross->mouse_draged*/)
    {
        cpu_clip_cross_in_image(cross, image, cross_len);
        return true;
    }
    return false;
}

inline bool cpu_cross_in_image(const cpu_bead_cross *cross, const cpu_image *image)
{
    return cross->x < image->width && cross->x > 0 && cross->y < image->height && cross->y > 0;
}

inline void cpu_clip_cross_in_image(cpu_bead_cross *cross, const cpu_image *image, cl_int cross_len)
{
    cl_int arm_size = cross_len / 2;

    cl_int maxx = (image->width - arm_size);
    cl_int maxy = (image->height - arm_size);

    if (cross->x > maxx)
    {
        cross->x = maxx;
    }
    if (cross->x < arm_size)
    {
        cross->x = arm_size;
    }
    if (cross->y > maxy)
    {
        cross->y = maxy;
    }
    if (cross->y < arm_size)
    {
        cross->y = arm_size;
    }
}

void cpu_fill_profiles_xy(const cpu_image *image, cpu_crosses *crosses)
{
    cl_int cross_len = crosses->length;
    cl_int cross_wth = crosses->width;
    cl_int arm_len = cross_len / 2;
    cl_int arm_wth = cross_wth / 2;

#pragma omp parallel for
    for (cl_int crossid = 0; crossid < crosses->size; ++crossid)
    {
        cpu_bead_cross *cross = crosses->cross_data[crossid];
        cpu_bead *bead =  cross->bead;
        if (bead != NULL)
        {
            cl_float *xprof = bead->avg_x_profile;
            cl_float *yprof = bead->avg_y_profile;

            // placing at the left-up corner of the horizontal rectangle
            //printf("crosspos x:%d, y:%d\n", cross->x, cross->y);

            cl_int xbegin = cross->x - arm_len;
            cl_int ybegin = cross->y - arm_wth;

            // we sum the central band
            for (int j = 0; j < cross_wth; j++)
            {
                cl_uchar *line = cpu_image_line(image, ybegin + j);
                for (int i = 0; i < cross_len; i++)
                {
                    xprof[i] += (float) line[xbegin + i];
                }
            }

            // placing at the left-up corner of the vertical rectangle
            xbegin = cross->x - arm_wth;
            ybegin = cross->y - arm_len;
            // we sum the central band
            for (int i = 0; i < cross_len; i++)
            {
                cl_uchar *line = cpu_image_line(image, ybegin + i);

                for (int j = 0; j < cross_wth; j++)
                {
                    yprof[i] += (float) line[xbegin + j];
                }
            }
        }
    }
}

void cpu_track_xy(cpu_crosses *crosses)
{
    cl_float dx = 0;
    cl_float dy = 0;
    cl_float max_val = 0;
    cl_float max_deriv = 0;
    cl_int max_limit = 0;

#pragma omp parallel for
    for (cl_int crossid = 0; crossid < crosses->size; ++crossid)
    {
        cpu_bead_cross *cross = crosses->cross_data[crossid];
        cpu_bead *bead =  cross->bead;
        if (bead != NULL)
        {
            cl_float *xauto_conv = cpu_profile_autoconv(bead->avg_x_profile, bead->radial_profile_size);
            cl_float *yauto_conv = cpu_profile_autoconv(bead->avg_y_profile, bead->radial_profile_size);

            cpu_find_center(xauto_conv,
                            bead->radial_profile_size,
                            &dx,
                            &max_val,
                            &max_deriv,
                            max_limit);

            cpu_find_center(yauto_conv,
                            bead->radial_profile_size,
                            &dy,
                            &max_val,
                            &max_deriv,
                            max_limit);
            dx -= bead->radial_profile_size / 2;
            dx /= 2;
            dy -= bead->radial_profile_size / 2;
            dy /= 2;

            bead->x += dx;
            bead->y += dy;
        }
    }
}


cl_float cpu_find_center(cl_float *x, cl_int size, cl_float *max_pos, cl_float *max_val, cl_float *max_deriv,
                         cl_int max_limit)
{
    int i;
    int  nmax;
    int ret = 0;
    int delta = 0;
    int imin, imax;
    double a, b, c, d, f_2, f_1, f0, f1, f2, xm = 0;

    if (max_limit < 2)
    {
        max_limit = size / 2;
    }
    imin = size / 2 - max_limit;
    imin = (imin < 0) ? 0 : imin;
    imax = size / 2 + max_limit;
    imax = (imax >= size) ? size : imax;

    // find maximum
    for (i = imin, nmax = 0; i < imax; i++)
    {
        nmax = (x[i] > x[nmax]) ? i : nmax;
    }

    // get neighbourhood
    f_2 = x[(size + nmax - 2) % size];
    f_1 = x[(size + nmax - 1) % size];
    f0 = x[nmax];
    f1 = x[(nmax + 1) % size];
    f2 = x[(nmax + 2) % size];

    // polinomiale interpolation
    if (f_1 < f1)
    {
        a = -f_1 / 6 + f0 / 2 - f1 / 2 + f2 / 6;
        b = f_1 / 2 - f0 + f1 / 2;
        c = -f_1 / 3 - f0 / 2 + f1 - f2 / 6;
        d = f0;
        delta = 0;
    }
    else
    {
        a = b = c = 0;
        a = -f_2 / 6 + f_1 / 2 - f0 / 2 + f1 / 6;
        b = f_2 / 2 - f_1 + f0 / 2;
        c = -f_2 / 3 - f_1 / 2 + f0 - f1 / 6;
        d = f_1;
        delta = -1;
    }

    if (fabs(a) < 1e-8)
    {
        if (b != 0)
        {
            xm =  - c / (2 * b) - (3 * a * c * c) / (4 * b * b * b);
        }
        else
        {
            xm = 0;
            ret |= PB_WITH_EXTREMUM;
        }
    }
    else if ((b * b - 3 * a * c) < 0)
    {
        ret |= PB_WITH_EXTREMUM;
        xm = 0;
    }
    else
    {
        xm = (-b - sqrt(b * b - 3 * a * c)) / (3 * a);
    }

    *max_pos = (float)(xm + nmax + delta);
    *max_val = (float)((a * xm * xm * xm) + (b * xm * xm) + (c * xm) + d);
    if (max_deriv)
    {
        *max_deriv = (float)((6 * a * xm) + (2 * b));
    }

    return ret;

}

cl_float *cpu_profile_autoconv(cl_float *prof, cl_int size)
{
    fft_plan *fplan = NULL;
    fplan = fftbt_init(fplan, size);

    cl_float *auto_conv = (cl_float *) malloc(size * sizeof(cl_float));
    memcpy(auto_conv, prof, size * sizeof(cl_float));

    cpu_remove_profile_average(auto_conv, size);


    realtr1bt(fplan, auto_conv);
    fftbt(fplan, auto_conv, 1);
    realtr2bt(fplan, auto_conv, 1);


    // TODO remove noise

    // making auto - convolution (x * x)

    auto_conv[0] = 2 * (auto_conv[0] * auto_conv[0]);
    auto_conv[1] = 0;
    for (int i = 2; i < size; i += 2)
    {
        cl_float re = auto_conv[i];
        cl_float im = auto_conv[i + 1];

        auto_conv[i] = (re * re - im * im);
        auto_conv[i + 1] = 2 * re * im;
    }

    static bool first2 = true;
    if (first2)
    {
        //debug_plot_profile(3, auto_conv, size);
        first2 = false;
    }

    // TODO remove convolution noise

    // get back to real world
    realtr2bt(fplan, auto_conv, -1);
    fftbt(fplan, auto_conv, -1);
    realtr1bt(fplan, auto_conv);

    cpu_deplace(auto_conv, size);

    return auto_conv;
}

/**
 * @brief Remove average of a profile
 *
 * @param prof
 * @param size
 *
 * @return the average
 */
cl_int cpu_remove_profile_average(cl_float *prof, cl_int size)
{
    cl_float moy = 0;

    //compute average from the to border : the variation to average is smaller.
    int i = 0;
    int j = (3 * size) / 4;
    for (; j < size; i++, j++)
    {
        moy += prof[i] + prof[j];
    }

    moy = (2 * moy) / size;

    for (i = 0 ; i < size; i++)
    {
        prof[i] -= moy;
    }
    return moy;
}

void cpu_deplace(cl_float *x, int nx)
{
    int i, j;
    float tmp;

    for (i = 0, j = nx / 2; j < nx ; i++, j++)
    {
        tmp = x[j];
        x[j] = x[i];
        x[i] = tmp;
    }
}

cl_float *cpu_fill_profile_z(const cpu_image *image, cpu_crosses *crosses)
{

    cl_int onx, ony; //image size
    cl_int i, j;//pixel index
    cl_float xc, yc; // cartesian coord of the bead center
    cl_float x, y; // cartesien coordonate to the current point
    cl_float r; // polar radius of (x,y)
    cl_float pix; // pixel value of (i,j)
    cl_int ri; // r in int
    cl_float p, q; // position on profil
    cl_float p0 = 0; // origin value
    cl_float pr0 = 0; // origin radius value
    cl_int imin, imax, jmin, jmax; //  xci, yci
    cl_float *prof = NULL;
    cl_int max_r = crosses->length / 2;

    //    #pragma omp parallel for
    for (cl_int crossid = 0; crossid < crosses->size; ++crossid)
    {
        cpu_bead_cross *cross = crosses->cross_data[crossid];
        cpu_bead *bead =  cross->bead;
        if (bead != NULL)
        {
            assert(image != NULL);
            assert(bead->radial_z_profile != NULL);

            ony = image->height;
            onx = image->width;

            prof = bead->radial_z_profile;
            xc = bead->x;
            yc = bead->y;

            //set prof to 0
            memset(prof, 0, 2 * max_r * sizeof(cl_float));

            //xci = (int)xc;
            //yci = (int)yc;

            //index calculus
            imin = (int) (yc - max_r);
            imax = (int) (0.5 + yc + max_r);
            jmin = (int) (xc -  max_r);
            jmax = (int) (0.5 + xc + max_r);
            imin = (imin < 0) ? 0 : imin;
            imax = (imax > ony) ? ony : imax;
            jmin = (jmin < 0) ? 0 : jmin;
            jmax = (jmax > onx) ? onx : jmax;

            // we calculate profil using an average on bead disk point
            // at this step, the profil has to be perfectly symetric (after x & y tracking), during the
            // calculus the first half of the array contains the sum of all pixel that is close the a
            // certain int distance from the center ponderated by the real distance. the second half
            // contains the total ponderation of an int distance.
            for (i = imin; i < imax; i++)
            {
                cl_uchar *line = cpu_image_line(image, i);
                y = (cl_float) i - yc;
                y *= y;
                for (j = jmin; j < jmax; j++)
                {
                    x = (cl_float) j - xc;
                    x *= x;
                    r = (cl_float) sqrt(x + y);

                    ri = (int)r;
                    pix = line[j];
                    //printf("(%d,%d) = %f\n", j,i, pix);
                    p = r - ri;
                    q = 1 - p;

                    if (ri < max_r)
                    {
                        cl_int risec = ri + max_r;
                        prof[risec] += q * pix;
                        prof[ri] += q ;
                        ri++;
                        if (ri < max_r)
                        {
                            risec++;
                            prof[risec] += p * pix;
                            prof[ri] += p;
                        }
                    }
                    else if (ri == max_r)
                    {
                        p0 += q * pix;
                        pr0 += q;
                    }

                }
            }

            // we divide the second half by the ponderation
            for (i = 0; i < max_r; i++)
            {
                prof[i + max_r] = (prof[i] == 0) ? 0 : prof[i + max_r] / prof[i];
            }
            p0 = (pr0 == 0) ? 0 : p0 / pr0;


            for (i = 1; i < max_r; i++)
            {
                prof[max_r - i] = prof[max_r + i];
            }
            prof[0] = p0;

            //for (int i = 0; i < bead->radial_profile_size; ++i)
            //{
            //    printf("%f|",prof[i]);
            //}
            //printf("\n");
        }
    }
    return prof;

}

int cpu_track_z(cpu_crosses *crosses, float *min_c, float *min_phi)
{
    cl_int rc = crosses->max_meaningful_radius;
    //    cl_int calib_height = crosses->calib_data->height;
    //    cl_int calib_width = crosses->calib_data->width;
    cpu_calib_images *calib_images = crosses->calib_data;

#pragma omp parallel for
    for (cl_int crossid = 0; crossid < crosses->size; ++crossid)
    {
        cpu_bead_cross *cross = crosses->cross_data[crossid];
        cpu_bead_param *bead_param =  crosses->bead_param[crossid];
        cpu_bead *bead =  cross->bead;
        if (bead != NULL)
        {

            int k, j;
            int er = 0;
            cl_int prof_size = crosses->length;
            float yt[256];
            float min, tmp, ph_1, phi, ph1;

            memcpy(yt, bead->radial_z_profile, prof_size * sizeof(float));

            cpu_prepare_filtered_profile(yt, prof_size, crosses, bead_param);
            //debug_plot_profile(0, yt, prof_size);

            j = cpu_coarse_find_z_by_least_square_min(yt, prof_size, calib_images, crossid, &min, rc);
            bead->calib_img_index = j;

            //debug_plot_profile(1, cpu_calib_line(calib_images,crossid,j), prof_size);

            j = (j == 0) ? j + 1 : j;
            j = (j == prof_size - 1) ? prof_size - 2 : j;
            *min_c = min;
            //printf("cpu calib :%d/%d\n", j, calib_images->height);
            /*
             *z = oir->ay + oir->dy *  j;
             return er;
             */
            tmp = (float) j;
            phi = cpu_find_simple_phase_shift_between_filtered_profile(cpu_calib_line(calib_images,crossid, j), yt, prof_size, rc);
            //tmp = (dsrz != NULL) ? dsrz->xd[j] : 0;
            tmp = (tmp != 0) ? (float) j + phi / tmp : (float) j;
            k = (int)(tmp + .5);
            er = (abs(k - j) >= 2) ? 1 : 0;
            k = (k <= 0) ? 1 : k;
            k = (k >= prof_size - 1) ? prof_size - 2 : k;

            ph_1 = (k - 1 == j) ? phi :
                cpu_find_simple_phase_shift_between_filtered_profile(cpu_calib_line(calib_images,crossid, k - 1), yt, prof_size, rc);
            ph1 = (k + 1 == j) ? phi :
                cpu_find_simple_phase_shift_between_filtered_profile(cpu_calib_line(calib_images, crossid, k + 1), yt, prof_size, rc);
            phi = (k == j) ? phi :
                cpu_find_simple_phase_shift_between_filtered_profile(cpu_calib_line(calib_images, crossid, k), yt, prof_size, rc);
            bead->z =  crosses->calib_data->ay + crosses->calib_data->dy * (cpu_find_zero_of_3_points_polynome(ph_1, phi, ph1) + k);
            //printf("%f\n", bead->z);

            *min_phi = ((ph_1 - ph1) != 0) ? phi / (ph_1 - ph1) : phi; // erreur in phase normalized by profile distance
        }
    }
    return 0;
}

cl_int cpu_prepare_filtered_profile(cl_float *zp, cl_int nx, cpu_crosses *crosses, cpu_bead_param *bead_param)
{
    cl_float moy, re, im;
    fft_plan *fp = bead_param->fplan;
    filter_bt *fil = bead_param->fil;


    /* remove dc based upon the first and the last quarter of data */
    /*  we assume a peak in the middle !*/
    if (fp == NULL || fil == NULL || zp == NULL)
    {
        return 1;
    }
    if (fftbt_init(fp, nx) == NULL)
    {
        return 2;
    }

    cpu_remove_profile_average(zp, nx);

    /* no windowing, profile must be symmetrical */
    realtr1bt(fp, zp);
    fftbt(fp, zp, 1);
    realtr2bt(fp, zp, 1);
    /* I take the amplitude of the cosine of the first mode to normalize */
    moy = zp[2];
    if (moy == 0)
    {
        return 2;
    } //win_printf("pb of normalisation zp[2] %f zp[3] %f",  zp[2],zp[3]);
    else
    {
        moy = 1. / moy;
        for (cl_int j = 0; j < nx ; j++)
        {
            zp[j] *= moy;
        }
    }

    zp[0] = zp[1] = 0;
    bandpass_smooth_half_bt (fil, nx, zp, crosses->bandpass_center, crosses->bandpass_width);
    fftbt(fp,  zp, -1);

    /* first half of profile amplitude, second half re im */
    for (cl_int j = 0; j < nx / 2 ; j += 2)
    {
        re = zp[j];
        im = zp[j + 1];
        zp[j] = sqrt(re * re + im * im);
        zp[j + 1] = 0;
    }

    return 0;
}
cl_int cpu_coarse_find_z_by_least_square_min(cl_float *yt, cl_int nxp, const cpu_calib_images *calib_imgs, cl_int beadid,
                                             float *min, int rc)
{
    int nx, ny;
    int min_pos = INT_MAX;
    float min_val = FLT_MAX;
    float norm = 0;
    float *line;
    ny = calib_imgs->height;
    nx = calib_imgs->width / 2;
    assert (2 * nx == nxp);

    //TODO Vectorisation
    //gpu done
    //cl_float fits[ny];
    for (int i = 0; i < ny ; i++)
    {
        float val = 0;
        float tmp;
        line = cpu_calib_line(calib_imgs, beadid, i);
        //gpu done
        for (int j = nx + rc; j < nxp; j++)
        {
            tmp = line[j] - yt[j];
            val += tmp * tmp;

        }
        //fits[i] = val;
        //gpu done
        if (val < min_val)
        {
            min_pos = i;
            min_val = val;
        }
    }
    //debug_plot_profile(0, fits, ny);
    line = cpu_calib_line(calib_imgs, beadid, min_pos);
    //gpu done
    for (int j = nx + rc; j < nxp; j++)
    {
        norm += line[j] * line[j];
    }
    //*min /= ((nx-rc)/2);  was working
    if (norm > 0)
    {
        min_val /= norm;
    }
    *min = min_val;
    return min_pos;
}

/**
 * @brief 
 *
 * @param zr z reference profile from calibration image, in hilbert space
 * @param zp z the current radial profile of the bead, in hilbert space. 
 * @param nx profiles size.
 * @param rc max meaningful radius
 *
 * @return 
 */
cl_float    cpu_find_simple_phase_shift_between_filtered_profile(cl_float *zr, cl_float *zp, cl_int nx, cl_int rc)
{
    float   im = 0.;
    float   re = 0.;
    float   amp = 0.;
    float   phi = 0.;
    float   w = 0.;
    float   ph = 0.;

    if (zr == NULL || zp == NULL)
    {
        return WRONG_ARGUMENT;
    }
    int j = 2; // 
    int i = nx - j;
    for (; j < nx / 2 - rc; i -= 2, j += 2)
    {
        re = zr[i] * zp[i] + zp[i + 1] * zr[i + 1];
        im = zp[i + 1] * zr[i] - zp[i] * zr[i + 1];
        ph = (im == 0.0 && re == 0.0) ? 0.0 : atan2(im, re);
        amp = zr[j] * zp[j];
        w += amp;
        phi += amp * ph;
    }
    return  (w != 0.0) ? phi / w : phi;
}

cl_float cpu_find_zero_of_3_points_polynome(cl_float y_1, cl_float y0, cl_float y1)
{
    double a, b, c, x = 0, delta;

    a = ((y_1 + y1) / 2) - y0;
    b = (y1 - y_1) / 2;
    c = y0;
    delta = b * b - 4 * a * c;
    if (a == 0)
    {
        x = (b != 0) ? -c / b : 0;
    }
    else if (delta >= 0)
    {
        delta = sqrt(delta);
        x = (fabs(-b + delta) < fabs(-b - delta)) ? -b + delta : -b - delta ;
        x /= 2 * a;
    }
    return (float)x;
}

