#include "gpu_track_utils.h"
#include "cpu_track_utils.h"
#include "../../opencl_utils/cl_utils.h"
#include "debug_tools.h"
#include "struct_convert.h"
#include <assert.h>
#include <stdio.h>
#include "ltpv.h"
#include "util.h"

#define LOG_SIZE 100000000

cl_kernel profile_empty_kernel = NULL;
cl_kernel profile_rdft_deplace_kernel = NULL;
cl_kernel profile_rdft_kernel = NULL;
cl_kernel profile_ridft_kernel = NULL;
cl_kernel profile_idft_kernel = NULL;
cl_kernel profile_remove_avg_kernel = NULL;
cl_kernel profile_auto_conv_kernel = NULL;
cl_kernel profile_get_first_mode_kernel = NULL;
cl_kernel profile_normalize_kernel = NULL;
cl_kernel profile_prepare_bandpass_smooth_filter_kernel = NULL;
cl_kernel profile_filter_kernel = NULL;
cl_kernel profile_complex_module_kernel = NULL;
cl_kernel profile_generate_sincos_lookup_table_kernel = NULL;
cl_kernel xy_fill_horiz_profile_kernel = NULL;
cl_kernel xy_fill_vert_profile_kernel = NULL;
cl_kernel xy_profile_max_kernel = NULL;
cl_kernel xy_deplacement_delta_kernel = NULL;
cl_kernel xy_update_bead_center_kernel = NULL;
cl_kernel z_average_kernel = NULL;
cl_kernel z_fill_kernel = NULL;
cl_kernel z_deplace_kernel = NULL;
cl_kernel z_calculate_fits_on_calib_image_kernel = NULL;
cl_kernel z_find_best_calib_profile_kernel = NULL;
cl_kernel z_compute_normalize_fitness_calib_kernel = NULL;
cl_kernel z_prepare_shifts_kernel = NULL;
cl_kernel z_prepare_first_shift_kernel = NULL;
cl_kernel z_simple_shift_between_profile_kernel = NULL;
cl_kernel z_phase_shift_between_profile_kernel = NULL;
cl_kernel z_compute_3_points_polynome_zero_kernel = NULL;

gpu_event_driven_memory *gpu_bandpass_filter_global = NULL;

gpu_event_driven_memory *gpu_sincos_lookup_table_global = NULL;

// Setted during initialization
cl_int gpu_cross_length = 0;
cl_int gpu_cross_width = 0;
cl_int gpu_calib_height = 0;
cl_int gpu_max_meaningful_radius = 0;
cl_int gpu_bandpass_width = 0;
cl_int gpu_bandpass_center = 0;
cl_program profile_utils_program = NULL; 
cl_program xy_tracking_program = NULL;  
cl_program z_tracking_program = NULL;

cl_bool gpu_requested_default_params = false;
cl_bool gpu_is_init = false;

void gpu_tracking_reset()
{
    clReleaseKernel(profile_empty_kernel);
    profile_empty_kernel = NULL;
    clReleaseKernel(profile_rdft_deplace_kernel);
    profile_rdft_deplace_kernel = NULL;
    clReleaseKernel(profile_rdft_kernel);
    profile_rdft_kernel = NULL;
    clReleaseKernel(profile_ridft_kernel);
    profile_ridft_kernel = NULL;
    clReleaseKernel(profile_idft_kernel);
    profile_idft_kernel = NULL;
    clReleaseKernel(profile_remove_avg_kernel);
    profile_remove_avg_kernel = NULL;
    clReleaseKernel(profile_auto_conv_kernel);
    profile_auto_conv_kernel = NULL;
    clReleaseKernel(profile_get_first_mode_kernel);
    profile_get_first_mode_kernel = NULL;
    clReleaseKernel(profile_normalize_kernel);
    profile_normalize_kernel = NULL;
    clReleaseKernel(profile_prepare_bandpass_smooth_filter_kernel);
    profile_prepare_bandpass_smooth_filter_kernel = NULL;
    clReleaseKernel(profile_filter_kernel);
    profile_filter_kernel = NULL;
    clReleaseKernel(profile_complex_module_kernel);
    profile_complex_module_kernel = NULL;
    clReleaseKernel(profile_generate_sincos_lookup_table_kernel);
    profile_generate_sincos_lookup_table_kernel = NULL;
    clReleaseKernel(xy_fill_horiz_profile_kernel);
    xy_fill_horiz_profile_kernel = NULL;
    clReleaseKernel(xy_fill_vert_profile_kernel);
    xy_fill_vert_profile_kernel = NULL;
    clReleaseKernel(xy_profile_max_kernel);
    xy_profile_max_kernel = NULL;
    clReleaseKernel(xy_deplacement_delta_kernel);
    xy_deplacement_delta_kernel = NULL;
    clReleaseKernel(xy_update_bead_center_kernel);
    xy_update_bead_center_kernel = NULL;
    clReleaseKernel(z_average_kernel);
    z_average_kernel = NULL;
    clReleaseKernel(z_fill_kernel);
    z_fill_kernel = NULL;
    clReleaseKernel(z_deplace_kernel);
    z_deplace_kernel = NULL;
    clReleaseKernel(z_calculate_fits_on_calib_image_kernel);
    z_calculate_fits_on_calib_image_kernel = NULL;
    clReleaseKernel(z_find_best_calib_profile_kernel);
    z_find_best_calib_profile_kernel = NULL;
    clReleaseKernel(z_compute_normalize_fitness_calib_kernel);
    z_compute_normalize_fitness_calib_kernel = NULL;
    clReleaseKernel(z_prepare_shifts_kernel);
    z_prepare_shifts_kernel = NULL;
    clReleaseKernel(z_prepare_first_shift_kernel);
    z_prepare_first_shift_kernel = NULL;
    clReleaseKernel(z_simple_shift_between_profile_kernel);
    z_simple_shift_between_profile_kernel = NULL;
    clReleaseKernel(z_phase_shift_between_profile_kernel);
    z_phase_shift_between_profile_kernel = NULL;
    clReleaseKernel(z_compute_3_points_polynome_zero_kernel);
    z_compute_3_points_polynome_zero_kernel = NULL;

    gpu_event_driven_memory_free(gpu_bandpass_filter_global);
    gpu_bandpass_filter_global = NULL;
    gpu_event_driven_memory_free(gpu_sincos_lookup_table_global);
    gpu_sincos_lookup_table_global = NULL;

    init_opencl_last_config();
    gpu_is_init = false;

}

bool gpu_needs_to_init()
{
    return !gpu_is_init;
}

void gpu_init_kernels(cpu_crosses *crosses)
{

    char *xy_kernel_source = NULL;
    char *profile_utils_kernel_source = NULL;
    char *z_kernel_source = NULL;
    char flags[255] = { 0 };
    cl_int err = CL_SUCCESS;
    cl_context context = get_context();
    cl_device_id device = get_device();
    size_t len;
    char *buffer = (char *) malloc(LOG_SIZE); // log ask for a very big buffer in windows, can't be allocated on stack
    char filename[1024] = {0};

    //debug_init_plot();

    gpu_cross_length = crosses->length;
    gpu_cross_width = crosses->width;
    gpu_max_meaningful_radius = crosses->max_meaningful_radius;
    gpu_bandpass_width = crosses->bandpass_width;
    gpu_bandpass_center = crosses->bandpass_center;
    gpu_calib_height = crosses->calib_data->height;

    my_getcwd(filename, sizeof(filename));
    my_strncat(filename, "/gpu/kernels/xy_tracking.cl", sizeof(filename));
    get_os_specific_path(filename);
    xy_kernel_source = load_opencl_file(filename);

    my_getcwd(filename, sizeof(filename));
    my_strncat(filename, "/gpu/kernels/profile_utils.cl", sizeof(filename));
    get_os_specific_path(filename);
    profile_utils_kernel_source = load_opencl_file(filename);

    my_getcwd(filename, sizeof(filename));
    my_strncat(filename, "/gpu/kernels/z_tracking.cl", sizeof(filename));
    get_os_specific_path(filename);
    z_kernel_source = load_opencl_file(filename);

    if (xy_kernel_source == NULL || profile_utils_kernel_source == NULL || z_kernel_source == NULL)
    {
        printf("Unable to load kernel\n");

    }

    sprintf(flags,
            "-DNB_ADD_LINE=%d -DPROF_SIZE=%d -DCALIB_SIZE=%d -DMAX_R=%d -DMAX_MEANINGFUL_RADIUS=%d" /*-cl-nv-verbose"*/,
            gpu_cross_width, gpu_cross_length, gpu_calib_height, gpu_cross_length / 2, gpu_max_meaningful_radius);

    if (crosses->calib_type == XV_CALIB_TYPE_NONE)
    {
        sprintf(flags, "%s -DCALIB_TYPE_NONE", flags);
    }

    if (z_tracking_program != NULL)
    {
        clReleaseProgram(z_tracking_program);
        z_tracking_program = NULL;
    }
    z_tracking_program = clCreateProgramWithSource(context, 1, (const char **) &z_kernel_source, NULL, &err);
    CLCHECK(err);
    // Build the program executable
    CLCHECK(clBuildProgram(z_tracking_program, 1, &device, flags, NULL, NULL));
    CLCHECK(clGetProgramBuildInfo(z_tracking_program, device, CL_PROGRAM_BUILD_LOG, LOG_SIZE, buffer, &len));

    if (len > LOG_SIZE)
    {
        printf("Warning : log buffer is too small for build log\n");
        printf("len=%lu, sizeof(buffer)=%d \n", len, LOG_SIZE);
    }

    printf("Build log: %s\n", buffer);

    if (profile_utils_program != NULL)
    {
        clReleaseProgram(profile_utils_program);
        profile_utils_program = NULL;
    }
    profile_utils_program = clCreateProgramWithSource(context, 1, (const char **) &profile_utils_kernel_source, NULL, &err);
    CLCHECK(err);
    CLCHECK(clBuildProgram(profile_utils_program, 1, &device, flags, NULL, NULL));
    CLCHECK(clGetProgramBuildInfo(profile_utils_program, device, CL_PROGRAM_BUILD_LOG, LOG_SIZE, buffer, &len));

    if (len > LOG_SIZE)
    {
        printf("Warning : log buffer is too small for build log\n");
        printf("len=%lu, sizeof(buffer)=%d \n", len, LOG_SIZE);
    }

    printf("Build log: %s\n", buffer);

    if (xy_tracking_program != NULL)
    {
        clReleaseProgram(xy_tracking_program);
        xy_tracking_program = NULL;
    }
    xy_tracking_program = clCreateProgramWithSource(context, 1, (const char **) &xy_kernel_source, NULL, &err);
    CLCHECK(err);
    CLCHECK(clBuildProgram(xy_tracking_program, 1, &device, flags, NULL, NULL));
    CLCHECK(clGetProgramBuildInfo(xy_tracking_program, device, CL_PROGRAM_BUILD_LOG, LOG_SIZE, buffer, &len));

    if (len > LOG_SIZE)
    {
        printf("Warning : log buffer is too small for build log\n");
        printf("len=%lu, sizeof(buffer)=%d \n", len, LOG_SIZE);
    }

    printf("Build log: %s\n", buffer);


    free(z_kernel_source);
    free(xy_kernel_source);
    free(profile_utils_kernel_source);

    xy_fill_vert_profile_kernel =   clCreateKernel(xy_tracking_program, "fill_vert_profiles",   &err);
    CLCHECK(err);
    xy_fill_horiz_profile_kernel =  clCreateKernel(xy_tracking_program, "fill_horiz_profiles",  &err);
    CLCHECK(err);
    profile_empty_kernel =          clCreateKernel(profile_utils_program, "fill_empty"  , &err);
    CLCHECK(err);
    z_average_kernel =            clCreateKernel(z_tracking_program, "average_profile", &err);
    CLCHECK(err);
    z_deplace_kernel =              clCreateKernel(z_tracking_program, "deplace_profile", &err);
    CLCHECK(err);
    profile_rdft_deplace_kernel =   clCreateKernel(profile_utils_program, "rdft_deplace", &err);
    CLCHECK(err);
    profile_rdft_kernel =           clCreateKernel(profile_utils_program, "rdft",                 &err);
    CLCHECK(err);
    profile_ridft_kernel =          clCreateKernel(profile_utils_program, "ridft",                &err);
    CLCHECK(err);
    profile_idft_kernel =          clCreateKernel(profile_utils_program, "idft",                &err);
    CLCHECK(err);
    profile_auto_conv_kernel =      clCreateKernel(profile_utils_program, "auto_conv",            &err);
    CLCHECK(err);
    profile_remove_avg_kernel =     clCreateKernel(profile_utils_program, "remove_avg",           &err);
    CLCHECK(err);
    profile_get_first_mode_kernel =     clCreateKernel(profile_utils_program, "get_first_mode",           &err);
    CLCHECK(err);
    profile_normalize_kernel =     clCreateKernel(profile_utils_program, "normalize_profiles",           &err);
    CLCHECK(err);
    profile_complex_module_kernel =     clCreateKernel(profile_utils_program, "complex_module",           &err);
    CLCHECK(err);
    profile_prepare_bandpass_smooth_filter_kernel =     clCreateKernel(profile_utils_program,
            "prepare_bandpass_smooth_filter",           &err);
    CLCHECK(err);
    profile_filter_kernel =     clCreateKernel(profile_utils_program, "filter", &err);
    CLCHECK(err);
    profile_generate_sincos_lookup_table_kernel =     clCreateKernel(profile_utils_program, "generate_sincos_lookup_table",
            &err);
    CLCHECK(err);
    xy_profile_max_kernel =         clCreateKernel(xy_tracking_program, "prof_max",             &err);
    CLCHECK(err);
    xy_deplacement_delta_kernel =         clCreateKernel(xy_tracking_program, "deplacement_delta",             &err);
    CLCHECK(err);
    xy_update_bead_center_kernel =         clCreateKernel(xy_tracking_program, "update_bead_center",             &err);
    CLCHECK(err);
    z_fill_kernel =                 clCreateKernel(z_tracking_program, "fill_profile"   , &err);
    CLCHECK(err);
    z_calculate_fits_on_calib_image_kernel = clCreateKernel(z_tracking_program, "calculate_fits_on_calib_image2"   , &err);
    CLCHECK(err);
    z_find_best_calib_profile_kernel = clCreateKernel(z_tracking_program, "find_best_calib_profile"   , &err);
    CLCHECK(err);
    z_compute_normalize_fitness_calib_kernel = clCreateKernel(z_tracking_program, "compute_normalized_fitness_calib"   ,
            &err);
    CLCHECK(err);
    z_prepare_first_shift_kernel = clCreateKernel(z_tracking_program, "prepare_first_shift", &err);
    CLCHECK(err);
    z_prepare_shifts_kernel = clCreateKernel(z_tracking_program, "prepare_shifts"   , &err);
    CLCHECK(err);
    z_phase_shift_between_profile_kernel = clCreateKernel(z_tracking_program, "phase_shift_between_profile", &err);
    CLCHECK(err);
    z_simple_shift_between_profile_kernel = clCreateKernel(z_tracking_program, "simple_shift_between_profile", &err);
    CLCHECK(err);
    z_compute_3_points_polynome_zero_kernel = clCreateKernel(z_tracking_program, "compute_3_points_polynome_zero", &err);
    CLCHECK(err);


    //cl_event dep[3] = {0};
    size_t globoff[3] = {0}; //offset
    size_t globsize[3] = {0}; // size
    size_t workgroup_size[3] = {1};


    // prepare bandpass
    globoff[0] = 0;
    globsize[0] = gpu_cross_length / 2;
    workgroup_size[0] = gpu_cross_length / 2;

    gpu_bandpass_filter_global = (gpu_event_driven_memory *) calloc(1, sizeof(gpu_event_driven_memory));
    gpu_bandpass_filter_global->mem = clCreateBuffer(get_context(), CL_MEM_READ_WRITE,
                                      sizeof(cl_float) * (gpu_cross_length / 2 + 1) , NULL, &err);
    CLCHECK(err);

    gpu_sincos_lookup_table_global = (gpu_event_driven_memory *) calloc(1, sizeof(gpu_event_driven_memory));
    gpu_sincos_lookup_table_global->mem = clCreateBuffer(get_context(), CL_MEM_READ_WRITE,
                                          sizeof(cl_float) * (gpu_cross_length) , NULL, &err);
    CLCHECK(err);

    // prepare bandpass
    CLCHECK(clSetKernelArg(profile_prepare_bandpass_smooth_filter_kernel, 0, sizeof(cl_mem),
                           (void *) & (gpu_bandpass_filter_global->mem)));
    CLCHECK(clSetKernelArg(profile_prepare_bandpass_smooth_filter_kernel, 1, sizeof(int), (void *) &gpu_bandpass_center));
    CLCHECK(clSetKernelArg(profile_prepare_bandpass_smooth_filter_kernel, 2, sizeof(int), (void *) &gpu_bandpass_width));

    CLCHECK(clSetKernelArg(profile_generate_sincos_lookup_table_kernel, 0, sizeof(cl_mem),
                           (void *) & (gpu_sincos_lookup_table_global->mem)));
    CLCHECK(clEnqueueNDRangeKernel(get_dtd_queue(),
                                   profile_prepare_bandpass_smooth_filter_kernel,
                                   1, globoff, globsize, workgroup_size,
                                   0, NULL , &(gpu_bandpass_filter_global->event)));

    CLCHECK(clEnqueueNDRangeKernel(get_dtd_queue(),
                                   profile_generate_sincos_lookup_table_kernel,
                                   1, globoff, globsize, workgroup_size,
                                   0, NULL , &(gpu_sincos_lookup_table_global->event)));
    gpu_is_init = true;
    free(buffer);
}

void gpu_init_mem(gpu_event_driven_memory **gpu_profs, gpu_event_driven_memory **gpu_beads_pos,
                  gpu_event_driven_memory **gpu_beads_z_pos, cpu_coord *beads_pos, cl_int prof_size, cl_int crosses_count)
{
    if (!gpu_is_init)
    {
        return;
    }

    cl_int err;

    if (*gpu_profs == NULL)
    {
        *gpu_profs = (gpu_event_driven_memory *) calloc(1, sizeof(gpu_event_driven_memory));
        (*gpu_profs)->event = clCreateUserEvent(get_context(), &err);
        (*gpu_profs)->mem = clCreateBuffer(get_context(), CL_MEM_READ_WRITE, sizeof(cl_float) * prof_size * 2 * crosses_count,
                                           NULL, &err);
        clSetUserEventStatus((*gpu_profs)->event, CL_COMPLETE);
    }

    if (*gpu_beads_pos == NULL)
    {
        *gpu_beads_pos = (gpu_event_driven_memory *) calloc(1, sizeof(gpu_event_driven_memory));
        (*gpu_beads_pos)->mem = clCreateBuffer(get_context(), CL_MEM_READ_WRITE, sizeof(cpu_coord) * crosses_count, NULL, &err);
    }

    if (*gpu_beads_z_pos == NULL)
    {
        *gpu_beads_z_pos = (gpu_event_driven_memory *) calloc(1, sizeof(gpu_event_driven_memory));
        (*gpu_beads_z_pos)->event = clCreateUserEvent(get_context(), &err);
        (*gpu_beads_z_pos)->mem = clCreateBuffer(get_context(), CL_MEM_READ_WRITE, sizeof(cl_float) * crosses_count, NULL,
                                  &err);
        clSetUserEventStatus((*gpu_beads_z_pos)->event, CL_COMPLETE);
    }

    CLCHECK(clEnqueueWriteBuffer(get_htd_queue(), (*gpu_beads_pos)->mem, CL_FALSE, 0 ,
                                 sizeof(cpu_coord) * crosses_count, beads_pos, 0, NULL, &(*gpu_beads_pos)->event));

}

void gpu_fill_profiles_xy(const cpu_image *image, int crosses_count, gpu_event_driven_memory *gpu_profs,
                          gpu_event_driven_memory *gpu_beads_pos)
{
    if (!gpu_is_init)
    {
        return;
    }

    cl_int profs_size = gpu_cross_length * crosses_count * 2;

    cl_mem gpu_profs_vert = NULL;
    //    cl_mem gpu_image_orig_positions = NULL;
    cl_buffer_region buf_region = {0, 0};
    cl_int err          = CL_SUCCESS;


    buf_region.size = (profs_size / 2) * sizeof(cl_float);
    buf_region.origin = (profs_size / 2) * sizeof(cl_float);

    CLCHECK(err);
    gpu_profs_vert = clCreateSubBuffer(gpu_profs->mem, CL_MEM_READ_WRITE,
                                       CL_BUFFER_CREATE_TYPE_REGION, &buf_region, &err);
    CLCHECK(err);

    CLCHECK(clSetKernelArg(xy_fill_horiz_profile_kernel, 0, sizeof(cl_mem), (void *) & (image->gpu_data.mem)));
    CLCHECK(clSetKernelArg(xy_fill_horiz_profile_kernel, 1, sizeof(cl_mem), (void *) &gpu_beads_pos->mem));
    CLCHECK(clSetKernelArg(xy_fill_horiz_profile_kernel, 2, sizeof(cl_mem), (void *) &gpu_profs->mem));

    CLCHECK(clSetKernelArg(xy_fill_vert_profile_kernel, 0, sizeof(cl_mem), (void *) & (image->gpu_data.mem)));
    CLCHECK(clSetKernelArg(xy_fill_vert_profile_kernel, 1, sizeof(cl_mem), (void *) &gpu_beads_pos->mem));
    CLCHECK(clSetKernelArg(xy_fill_vert_profile_kernel, 2, sizeof(cl_mem), (void *) &gpu_profs_vert));

    size_t globoff[2] = {0, 0}; //offset
    size_t globsize[2] = {gpu_cross_length, crosses_count}; // size
    size_t work_group_size[2] = {gpu_cross_length, 1}; //one cross by workgroup
    cl_event deps[3] = {image->gpu_data.event, gpu_beads_pos->event, NULL};

    cl_event xy_fill_horiz_profile_event;
    CLCHECK(clEnqueueNDRangeKernel(get_dtd_queue(),
                                   xy_fill_horiz_profile_kernel,
                                   2, globoff, globsize, work_group_size,
                                   2, deps, &xy_fill_horiz_profile_event));

    deps[2] = xy_fill_horiz_profile_event;
    cl_event xy_fill_vert_profile_event;
    CLCHECK(clEnqueueNDRangeKernel(get_dtd_queue(), xy_fill_vert_profile_kernel,
                                   2, globoff, globsize, work_group_size,
                                   3, deps, &xy_fill_vert_profile_event));

    clReleaseEvent(gpu_profs->event);
    gpu_profs->event = xy_fill_vert_profile_event;


    CLCHECK(clReleaseMemObject(gpu_profs_vert));
    gpu_profs_vert = NULL;
    clReleaseEvent(xy_fill_horiz_profile_event);
}

void gpu_track_xy(cl_int crosses_count, gpu_event_driven_memory *gpu_profs, gpu_event_driven_memory *gpu_beads_pos,
                  gpu_event_driven_memory *gpu_conv_profs)
{
    if (!gpu_is_init)
    {
        return;
    }

    cl_int prof_size = gpu_cross_length;
    cl_int profs_size = prof_size * crosses_count * 2;
    cl_int err = 0;
    cl_mem gpu_fourier_profs = NULL;
    cl_mem gpu_time_profs = NULL;
    cl_mem gpu_autoconv_maxs = NULL;
    cl_mem gpu_deltas = NULL;

    gpu_fourier_profs = clCreateBuffer(get_context(), CL_MEM_READ_WRITE, sizeof(cl_float) * profs_size, NULL, &err);
    CLCHECK(err);

    if (gpu_conv_profs)
    {
        gpu_time_profs = gpu_conv_profs->mem;
    }
    else
    {
        gpu_time_profs = clCreateBuffer(get_context(), CL_MEM_READ_WRITE, sizeof(cl_float) * profs_size, NULL, &err);
        CLCHECK(err);
    }

    gpu_autoconv_maxs = clCreateBuffer(get_context(), CL_MEM_READ_WRITE, sizeof(cl_int) * crosses_count * 2, NULL, &err);
    CLCHECK(err);

    gpu_deltas = clCreateBuffer(get_context(), CL_MEM_READ_WRITE, sizeof(cl_float) * crosses_count * 2, NULL, &err);
    CLCHECK(err);

    CLCHECK(clSetKernelArg(profile_remove_avg_kernel, 0, sizeof(cl_mem), (void *) &gpu_profs->mem));
    CLCHECK(clSetKernelArg(profile_remove_avg_kernel, 1, prof_size * sizeof(cl_float), NULL));


    CLCHECK(clSetKernelArg(profile_rdft_kernel, 0, sizeof(cl_mem), (void *) &gpu_profs->mem));
    CLCHECK(clSetKernelArg(profile_rdft_kernel, 1, prof_size * sizeof(cl_float), NULL));
    CLCHECK(clSetKernelArg(profile_rdft_kernel, 2, 2 * prof_size * sizeof(cl_float), NULL));
    CLCHECK(clSetKernelArg(profile_rdft_kernel, 3, sizeof(cl_mem), (void *) &gpu_fourier_profs));

    //FIXME: ADD HERE LOW PASS FILTER

    CLCHECK(clSetKernelArg(profile_auto_conv_kernel, 0, sizeof(cl_mem), (void *) &gpu_fourier_profs));

    CLCHECK(clSetKernelArg(profile_ridft_kernel, 0, sizeof(cl_mem), (void *) &gpu_fourier_profs));
    CLCHECK(clSetKernelArg(profile_ridft_kernel, 1, prof_size * sizeof(cl_float) * 4, NULL));
    CLCHECK(clSetKernelArg(profile_ridft_kernel, 2, 2 * prof_size * sizeof(cl_float), NULL));
    CLCHECK(clSetKernelArg(profile_ridft_kernel, 3, sizeof(cl_mem), (void *) &gpu_time_profs));


    CLCHECK(clSetKernelArg(profile_rdft_deplace_kernel, 0, sizeof(cl_mem), (void *) &gpu_time_profs));

    CLCHECK(clSetKernelArg(xy_profile_max_kernel, 0, sizeof(cl_mem), (void *) &gpu_time_profs));
    CLCHECK(clSetKernelArg(xy_profile_max_kernel, 1, prof_size * sizeof(cl_float), NULL));
    CLCHECK(clSetKernelArg(xy_profile_max_kernel, 2, prof_size * sizeof(cl_float), NULL));
    CLCHECK(clSetKernelArg(xy_profile_max_kernel, 3, sizeof(cl_mem), &gpu_autoconv_maxs));

    CLCHECK(clSetKernelArg(xy_deplacement_delta_kernel, 0, sizeof(cl_mem), &gpu_time_profs));
    CLCHECK(clSetKernelArg(xy_deplacement_delta_kernel, 1, sizeof(cl_mem), &gpu_autoconv_maxs));
    CLCHECK(clSetKernelArg(xy_deplacement_delta_kernel, 2, sizeof(cl_mem), &gpu_deltas));

    CLCHECK(clSetKernelArg(xy_update_bead_center_kernel, 0, sizeof(cl_mem), &gpu_deltas));
    CLCHECK(clSetKernelArg(xy_update_bead_center_kernel, 1, sizeof(cl_mem), &gpu_beads_pos->mem));

    size_t globoff[2] = {0, 0}; //offset
    size_t globsize[2] = {prof_size, crosses_count * 2}; // size
    size_t work_group_size[2] = {prof_size, 1}; //one cross by workgroup

    cl_event remove_avg_event;
    CLCHECK(clEnqueueNDRangeKernel(get_dtd_queue(),
                                   profile_remove_avg_kernel,
                                   2, globoff, globsize, work_group_size,
                                   1, &(gpu_profs->event), &remove_avg_event));



    globsize[0] = prof_size / 2 + 1;
    globsize[1] = crosses_count * 2;
    work_group_size[0] = prof_size / 2 + 1;
    work_group_size[1] = 1;

    cl_event rdft_event;
    CLCHECK(clEnqueueNDRangeKernel(get_dtd_queue(),
                                   profile_rdft_kernel,
                                   2, globoff, globsize, work_group_size,
                                   1, &remove_avg_event, &rdft_event));


    globsize[0] = prof_size / 2;
    globsize[1] = crosses_count * 2;
    work_group_size[0] = prof_size / 2;
    work_group_size[1] = 1;

    cl_event auto_conv_event;
    CLCHECK(clEnqueueNDRangeKernel(get_dtd_queue(),
                                   profile_auto_conv_kernel,
                                   2, globoff, globsize, work_group_size,
                                   1, &rdft_event, &auto_conv_event));

    globsize[0] = prof_size;
    globsize[1] = crosses_count * 2;
    work_group_size[0] = prof_size;
    work_group_size[1] = crosses_count % 2 == 0 ? 4 : 2; // /!\ does not work with more than max_workgroup_size beads

    cl_event ridft_event;
    CLCHECK(clEnqueueNDRangeKernel(get_dtd_queue(),
                                   profile_ridft_kernel,
                                   2, globoff, globsize, NULL, // work_group_size,
                                   1, &auto_conv_event, &ridft_event));

    globsize[0] = prof_size / 2;
    globsize[1] = crosses_count * 2;
    work_group_size[0] = prof_size / 2;
    work_group_size[1] = 1;
    cl_event deplace_event;
    CLCHECK(clEnqueueNDRangeKernel(get_dtd_queue(),
                                   profile_rdft_deplace_kernel,
                                   2, globoff, globsize, work_group_size,
                                   1, &ridft_event, &deplace_event));

    if (gpu_conv_profs)
    {
        gpu_conv_profs->event = deplace_event;
    }

    globsize[0] = prof_size;
    globsize[1] = crosses_count * 2;
    work_group_size[0] = prof_size;
    work_group_size[1] = 1;
    cl_event max_event;
    CLCHECK(clEnqueueNDRangeKernel(get_dtd_queue(),
                                   xy_profile_max_kernel,
                                   2, globoff, globsize, work_group_size,
                                   1, &deplace_event, &max_event));

    globsize[0] = crosses_count * 2;
    work_group_size[0] = crosses_count % 2 == 0 ? 4 : 2; // /!\ does not work with more than max_workgroup_size / 2 beads
    cl_event deplacement_delta_event;
    CLCHECK(clEnqueueNDRangeKernel(get_dtd_queue(),
                                   xy_deplacement_delta_kernel,
                                   1, globoff, globsize, NULL,// work_group_size,
                                   1, &max_event, &deplacement_delta_event));

    globsize[0] = crosses_count;
    work_group_size[0] = crosses_count % 4 == 0 ? 4 : crosses_count % 2 == 0 ? 2 :
                         1; // /!\ does not work with more than max_workgroup_size / 2 beads
    cl_event update_bead_center_event;
    CLCHECK(clEnqueueNDRangeKernel(get_dtd_queue(),
                                   xy_update_bead_center_kernel,
                                   1, globoff, globsize, NULL, //work_group_size,
                                   1, &deplacement_delta_event, &update_bead_center_event));

    clReleaseEvent(gpu_beads_pos->event);
    gpu_beads_pos->event = update_bead_center_event;


    clReleaseEvent(remove_avg_event);
    clReleaseEvent(rdft_event);
    clReleaseEvent(auto_conv_event);
    clReleaseEvent(ridft_event);
    clReleaseEvent(max_event);
    clReleaseEvent(deplacement_delta_event);

    if (!gpu_conv_profs)
    {
        clReleaseEvent(deplace_event);

    }

    CLCHECK(clReleaseMemObject(gpu_fourier_profs));
    gpu_fourier_profs = NULL;

    if (!gpu_conv_profs)
    {
        CLCHECK(clReleaseMemObject(gpu_time_profs));
        gpu_time_profs = NULL;
    }

    CLCHECK(clReleaseMemObject(gpu_autoconv_maxs));
    gpu_autoconv_maxs = NULL;

    CLCHECK(clReleaseMemObject(gpu_deltas));
    gpu_deltas = NULL;
}

void gpu_fill_profile_z(const cpu_image *image, int crosses_count, gpu_event_driven_memory *gpu_profs,
                        gpu_event_driven_memory *gpu_beads_pos)
{

    if (!gpu_is_init)
    {
        return;
    }

    cl_mem gpu_profs_int = NULL;
    cl_int err;
    cl_int prof_size = gpu_cross_length;
    cl_int profs_size = (prof_size + 2) * crosses_count;


    //assert(image != NULL);

    gpu_profs_int = clCreateBuffer(get_context(), CL_MEM_READ_WRITE,  sizeof(cl_int) * profs_size, NULL, &err);
    CLCHECK(err);


    size_t empty_globsize = profs_size;
    cl_event wb_event;
    CLCHECK(clSetKernelArg(profile_empty_kernel, 0, sizeof(cl_mem), (void *) &gpu_profs_int));
    CLCHECK(clEnqueueNDRangeKernel(get_dtd_queue(), profile_empty_kernel, 1, NULL, &empty_globsize, NULL, 0, NULL,
                                   &wb_event));

#ifndef NDEBUG
    //    size_t wgsize = 0;
    //    clGetKernelWorkGroupInfo(z_fill_kernel, NULL, CL_KERNEL_WORK_GROUP_SIZE, sizeof(size_t), &wgsize, NULL);
    //    printf("workgroup size %d\n", wgsize);
#endif

    CLCHECK(clSetKernelArg(z_fill_kernel, 0, sizeof(cl_mem), (void *) & (image->gpu_data.mem)));
    CLCHECK(clSetKernelArg(z_fill_kernel, 1, sizeof(cl_mem), (void *) & (gpu_beads_pos->mem)));
    CLCHECK(clSetKernelArg(z_fill_kernel, 2, sizeof(cl_mem), (void *) &gpu_profs_int));

    CLCHECK(clSetKernelArg(z_average_kernel, 0, sizeof(cl_mem), (void *) & (gpu_profs->mem)));
    CLCHECK(clSetKernelArg(z_average_kernel, 1, sizeof(cl_mem), (void *) &gpu_profs_int));

    CLCHECK(clSetKernelArg(z_deplace_kernel, 0, sizeof(cl_mem), (void *) & (gpu_profs->mem)));

    //index calculus

    //size_t globoff[3] = {0, 0, 0}; //offset
    size_t globsize[3] = {gpu_cross_length, gpu_cross_length, crosses_count}; // size
    size_t work_group_size[3] = {gpu_cross_length, 4, 1}; //TESTING
    //printf("%d, %d\n", globsize[0], globsize[1]);

    cl_event fill_event;
    cl_event dep[3] = {image->gpu_data.event, wb_event, gpu_beads_pos->event};
    CLCHECK(clEnqueueNDRangeKernel(get_dtd_queue(), z_fill_kernel, 3, NULL, globsize, work_group_size, 3, dep,
                                   &fill_event));


    globsize[0] = gpu_cross_length / 2;
    globsize[1] = crosses_count;

    //assert(z_average_kernel);
    cl_event avg_event;
    CLCHECK(clEnqueueNDRangeKernel(get_dtd_queue(), z_average_kernel, 2, NULL, globsize, NULL, 1, &fill_event,
                                   &avg_event));

    //globoff[0] = 1; //offset
    globsize[0] = gpu_cross_length / 2;
    globsize[1] = crosses_count;

    //assert(profile_rdft_deplace_kernel);
    cl_event deplace_event;
    CLCHECK(clEnqueueNDRangeKernel(get_dtd_queue(), z_deplace_kernel, 2, NULL, globsize, NULL, 1, &avg_event,
                                   &deplace_event));
    CLCHECK(clReleaseMemObject(gpu_profs_int));
    gpu_profs_int = NULL;
    clReleaseEvent(gpu_profs->event);
    gpu_profs->event = deplace_event;

    clReleaseEvent(wb_event);
    clReleaseEvent(fill_event);
    clReleaseEvent(avg_event);
}


void gpu_track_z(cpu_crosses *crosses, gpu_event_driven_memory *gpu_profs, gpu_event_driven_memory *gpu_beads_z_pos)
{
    if (!gpu_is_init)
    {
        return;
    }

    cl_int calib_height = gpu_calib_height;
    cl_int prof_size =  gpu_cross_length;
    cl_int profs_size = prof_size * crosses->size;
    cl_int err;
    cpu_calib_images *calib_images = crosses->calib_data;
    //assert(calib_images);

    cl_event dep[3] = {0};
    size_t globoff[3] = {0}; //offset
    size_t globsize[3] = {0}; // size
    size_t workgroup_size[3] = {1};

    int fits_size = calib_images->height * crosses->size;

    cl_mem gpu_fourier_profs = clCreateBuffer(get_context(), CL_MEM_READ_WRITE, sizeof(cl_float) * profs_size, NULL, &err);
    CLCHECK(err);

    cl_mem gpu_hilbert_profs = clCreateBuffer(get_context(), CL_MEM_READ_WRITE, sizeof(cl_float) * profs_size, NULL, &err);
    CLCHECK(err);

    cl_mem gpu_fits = clCreateBuffer(get_context(), CL_MEM_READ_WRITE, sizeof(cl_float) * fits_size, NULL, &err);
    CLCHECK(err);

    cl_mem gpu_best_fits_ids = clCreateBuffer(get_context(), CL_MEM_READ_WRITE, sizeof(cl_int) * crosses->size, NULL, &err);
    CLCHECK(err);

    cl_mem gpu_best_fits_xi2 = clCreateBuffer(get_context(), CL_MEM_READ_WRITE, sizeof(cl_float) * crosses->size, NULL,
                               &err);

    cl_mem gpu_phis = clCreateBuffer(get_context(), CL_MEM_READ_WRITE, sizeof(cl_float) * crosses->size * 3, NULL,
                                     &err);
    CLCHECK(err);

    CLCHECK(err);

    /* Filling kernel arguments */

    // rm average profs
    CLCHECK(clSetKernelArg(profile_remove_avg_kernel, 0, sizeof(cl_mem), (void *) &gpu_profs->mem));
    CLCHECK(clSetKernelArg(profile_remove_avg_kernel, 1, prof_size * sizeof(cl_float), NULL));

    // rdft profs
    CLCHECK(clSetKernelArg(profile_rdft_kernel, 0, sizeof(cl_mem), (void *) &gpu_profs->mem));
    CLCHECK(clSetKernelArg(profile_rdft_kernel, 1, prof_size * sizeof(cl_float), NULL));
    CLCHECK(clSetKernelArg(profile_rdft_kernel, 2, 2 * prof_size * sizeof(cl_float), NULL));
    CLCHECK(clSetKernelArg(profile_rdft_kernel, 3, sizeof(cl_mem), (void *) &gpu_fourier_profs));

    // get first mode
    CLCHECK(clSetKernelArg(profile_get_first_mode_kernel, 0, sizeof(cl_mem), (void *) &gpu_fourier_profs));
    CLCHECK(clSetKernelArg(profile_get_first_mode_kernel, 1, sizeof(cl_mem),
                           (void *) &gpu_phis)); // using phi memory to save space, will contains divider at this point

    // normalize
    CLCHECK(clSetKernelArg(profile_normalize_kernel, 0, sizeof(cl_mem),
                           (void *) &gpu_phis)); // using phi memory to save space, will contains divider at this point
    CLCHECK(clSetKernelArg(profile_normalize_kernel, 1, sizeof(cl_mem), (void *) &gpu_fourier_profs));

    // filter
    CLCHECK(clSetKernelArg(profile_filter_kernel, 0, sizeof(cl_mem), (void *) &gpu_bandpass_filter_global->mem));
    CLCHECK(clSetKernelArg(profile_filter_kernel, 1, sizeof(cl_mem), (void *) &gpu_fourier_profs));

    // idft (hilbert space)
    CLCHECK(clSetKernelArg(profile_idft_kernel, 0, sizeof(cl_mem), (void *) &gpu_fourier_profs));
    CLCHECK(clSetKernelArg(profile_idft_kernel, 1, prof_size * sizeof(cl_float), NULL));
    CLCHECK(clSetKernelArg(profile_idft_kernel, 2, 2 * prof_size * sizeof(cl_float), NULL));
    CLCHECK(clSetKernelArg(profile_idft_kernel, 3, sizeof(cl_mem), (void *) &gpu_hilbert_profs));

    // complex module
    CLCHECK(clSetKernelArg(profile_complex_module_kernel, 0, sizeof(cl_mem), (void *) &gpu_hilbert_profs));

    // cal fits
    // CLCHECK(clSetKernelArg(z_calculate_fits_on_calib_image_kernel, 0, sizeof(cl_mem), (void *) &(calib_image->gpu_data)));
    // CLCHECK(clSetKernelArg(z_calculate_fits_on_calib_image_kernel, 1, sizeof(cl_mem), (void *) &gpu_hilbert_profs));
    // CLCHECK(clSetKernelArg(z_calculate_fits_on_calib_image_kernel, 2, sizeof(cl_float) * prof_size, NULL));
    // CLCHECK(clSetKernelArg(z_calculate_fits_on_calib_image_kernel, 3, sizeof(cl_mem), (void *) &gpu_fits));
    if (crosses->calib_type == XV_CALIB_TYPE_NONE)
    {
        CLCHECK(clSetKernelArg(z_phase_shift_between_profile_kernel, 0, sizeof(cl_mem),
                               (void *) & (calib_images->gpu_filtered_calib_data.mem)));
        CLCHECK(clSetKernelArg(z_phase_shift_between_profile_kernel, 1, sizeof(cl_mem), (void *) &gpu_hilbert_profs));
        CLCHECK(clSetKernelArg(z_phase_shift_between_profile_kernel, 2, sizeof(cl_float) * prof_size, NULL));
        CLCHECK(clSetKernelArg(z_phase_shift_between_profile_kernel, 3, sizeof(cl_mem), (void *) & (gpu_beads_z_pos->mem)));
    }
    else
    {
        CLCHECK(clSetKernelArg(z_calculate_fits_on_calib_image_kernel, 0, sizeof(cl_mem),
                               (void *) & (calib_images->gpu_filtered_calib_data.mem)));
        CLCHECK(clSetKernelArg(z_calculate_fits_on_calib_image_kernel, 1, sizeof(cl_mem), (void *) &gpu_hilbert_profs));
        CLCHECK(clSetKernelArg(z_calculate_fits_on_calib_image_kernel, 2, sizeof(cl_mem), (void *) &gpu_fits));

        // find best fits
        CLCHECK(clSetKernelArg(z_find_best_calib_profile_kernel, 0, sizeof(cl_mem), (void *) &gpu_fits));
        CLCHECK(clSetKernelArg(z_find_best_calib_profile_kernel, 1, sizeof(cl_int) * calib_height, NULL));
        CLCHECK(clSetKernelArg(z_find_best_calib_profile_kernel, 2, sizeof(cl_float) * calib_height, NULL));
        CLCHECK(clSetKernelArg(z_find_best_calib_profile_kernel, 3, sizeof(cl_mem), (void *) &gpu_best_fits_ids));
        CLCHECK(clSetKernelArg(z_find_best_calib_profile_kernel, 4, sizeof(cl_mem), (void *) &gpu_best_fits_xi2));

        // compute nomalized fitness ?
        // prepare_first_shift
        CLCHECK(clSetKernelArg(z_prepare_first_shift_kernel, 0, sizeof(cl_mem), (void *) &gpu_best_fits_ids));


        // simple shift_between_profile (one line)
        CLCHECK(clSetKernelArg(z_simple_shift_between_profile_kernel, 0, sizeof(cl_mem),
                               (void *) & (calib_images->gpu_filtered_calib_data.mem)));
        CLCHECK(clSetKernelArg(z_simple_shift_between_profile_kernel, 1, sizeof(cl_mem), (void *) &gpu_hilbert_profs));
        CLCHECK(clSetKernelArg(z_simple_shift_between_profile_kernel, 2, sizeof(cl_float) * prof_size, NULL));
        CLCHECK(clSetKernelArg(z_simple_shift_between_profile_kernel, 3, sizeof(cl_mem), (void *) &gpu_phis));
        CLCHECK(clSetKernelArg(z_simple_shift_between_profile_kernel, 4, sizeof(cl_mem), (void *) &gpu_best_fits_ids));

        // prepare_shifts
        CLCHECK(clSetKernelArg(z_prepare_shifts_kernel, 0, sizeof(cl_mem), (void *) &gpu_best_fits_ids));
        CLCHECK(clSetKernelArg(z_prepare_shifts_kernel, 1, sizeof(cl_mem), (void *) &gpu_phis));

        // compute_3_points_polynome_zero
        CLCHECK(clSetKernelArg(z_compute_3_points_polynome_zero_kernel, 0, sizeof(cl_mem), (void *) &gpu_phis));
        CLCHECK(clSetKernelArg(z_compute_3_points_polynome_zero_kernel, 1, sizeof(cl_mem), (void *) &gpu_beads_z_pos->mem));
    }

    /* Launch kernels */


    // rm average profs
    globoff[0] = 0;
    globoff[1] = 0;
    globsize[0] = prof_size;
    globsize[1] = crosses->size;
    workgroup_size[0] = prof_size;
    workgroup_size[1] = 1;

    cl_event remove_avg_event;
    CLCHECK(clEnqueueNDRangeKernel(get_dtd_queue(),
                                   profile_remove_avg_kernel,
                                   2, globoff, globsize, workgroup_size,
                                   1, &gpu_profs->event, &remove_avg_event));
    // rdft profs
    globoff[0] = 0;
    globoff[1] = 0;
    globsize[0] = prof_size / 2 + 1;
    globsize[1] = crosses->size;
    workgroup_size[0] = prof_size / 2 + 1;
    workgroup_size[1] = 1;

    cl_event rdft_event;
    CLCHECK(clEnqueueNDRangeKernel(get_dtd_queue(),
                                   profile_rdft_kernel,
                                   2, globoff, globsize, workgroup_size,
                                   1, &remove_avg_event, &rdft_event));
    // first normalize
    globoff[0] = 0;
    globsize[0] = crosses->size;

    cl_event get_first_mode_event;
    CLCHECK(clEnqueueNDRangeKernel(get_dtd_queue(),
                                   profile_get_first_mode_kernel,
                                   1, globoff, globsize, NULL,
                                   1, &rdft_event, &get_first_mode_event));
    // first normalize
    globoff[0] = 0;
    globoff[1] = 0;
    globsize[0] = prof_size;
    globsize[1] = crosses->size;
    workgroup_size[0] = prof_size;
    workgroup_size[1] = 1;

    cl_event first_mode_norm_event;
    CLCHECK(clEnqueueNDRangeKernel(get_dtd_queue(),
                                   profile_normalize_kernel,
                                   2, globoff, globsize, workgroup_size,
                                   1, &get_first_mode_event, &first_mode_norm_event));




    // filter profs
    globoff[0] = 0;
    globoff[1] = 0;
    globsize[0] = prof_size / 2;
    globsize[1] = crosses->size;
    workgroup_size[0] = prof_size / 2;
    workgroup_size[1] = 1;

    cl_event filter_event;
    CLCHECK(clEnqueueNDRangeKernel(get_dtd_queue(),
                                   profile_filter_kernel,
                                   2, globoff, globsize, workgroup_size,
                                   1, &first_mode_norm_event, &filter_event));

    // idft (hilbert space of n/2 points)
    globoff[0] = 0;
    globoff[1] = 0;
    globsize[0] = prof_size / 2;
    globsize[1] = crosses->size;
    workgroup_size[0] = prof_size / 2;
    workgroup_size[1] = 1;

    cl_event idft_event;
    CLCHECK(clEnqueueNDRangeKernel(get_dtd_queue(),
                                   profile_idft_kernel,
                                   2, globoff, globsize, workgroup_size,
                                   1, &filter_event, &idft_event));


    // complex module
    globoff[0] = 0;
    globoff[1] = 0;
    globsize[0] = prof_size / 4; // using float2, calculating for half the profil
    globsize[1] = crosses->size;
    workgroup_size[0] = prof_size / 4;
    workgroup_size[1] = 1;

    cl_event complex_module_event;
    CLCHECK(clEnqueueNDRangeKernel(get_dtd_queue(),
                                   profile_complex_module_kernel,
                                   2, globoff, globsize, workgroup_size,
                                   1, &idft_event, &complex_module_event));

    CLCHECK(clEnqueueCopyBuffer(get_dtd_queue(),
                                gpu_hilbert_profs,
                                (gpu_profs->mem),
                                0,
                                0,
                                sizeof(cl_float) * profs_size,
                                1,
                                &complex_module_event,
                                NULL));

    clFinish(get_dtd_queue());

    cl_event ref_profile_event = NULL;

    if (crosses->calib_type == XV_CALIB_TYPE_NONE && crosses->update_reference_profile)
    {
        clEnqueueCopyBuffer(get_dtd_queue(), gpu_hilbert_profs, crosses->calib_data->gpu_filtered_calib_data.mem, 0, 0,
                            crosses->length * crosses->size * sizeof(cl_float), 1, &complex_module_event,
                            &ref_profile_event);
        crosses->update_reference_profile = false;
        //printf("set ref profs");
    }

    if (crosses->calib_type == XV_CALIB_TYPE_NONE)
    {
        globoff[0] = 0;
        globoff[1] = 0;
        globoff[2] = 0;

        globsize[0] = crosses->size;
        globsize[1] = 0;
        globsize[2] = 0;

        workgroup_size[0] = crosses->size;
        workgroup_size[1] = 1;
        workgroup_size[2] = 1;

        cl_event phase_shift_deps[2] = { complex_module_event, ref_profile_event};
        cl_event phase_shift_event;

        CLCHECK(clEnqueueNDRangeKernel(get_dtd_queue(), z_phase_shift_between_profile_kernel , 1, globoff, globsize,
                                       workgroup_size, !!(ref_profile_event != NULL) + 1,  phase_shift_deps, &phase_shift_event));

        clReleaseEvent(gpu_beads_z_pos->event);
        gpu_beads_z_pos->event = phase_shift_event;
    }
    else
    {
        //cal fits
        globoff[0] = 0;
        globoff[1] = 0;
        // globoff[2] = 0;

        //globsize[0] = prof_size / 2;
        globsize[0] = crosses->size;
        globsize[1] = calib_height; // size

        //workgroup_size[0] = prof_size / 2;
        workgroup_size[0] = crosses->size % 4 == 0 ? 4 : crosses->size % 2 == 0 ? 2 : 1;
        workgroup_size[1] = calib_height;

        dep[0] = complex_module_event;
        dep[1] = calib_images->gpu_filtered_calib_data.event;
        cl_event cal_fits_event;
        CLCHECK(clEnqueueNDRangeKernel(get_dtd_queue(), z_calculate_fits_on_calib_image_kernel , 2, globoff, globsize,
                                       workgroup_size, 2,  dep, &cal_fits_event));

        // find best fits

        globoff[0] = 0;
        globoff[1] = 0;

        globsize[0] = calib_images->height;
        globsize[1] = crosses->size;

        workgroup_size[0] = calib_images->height; // ! we assume that a calibration image as less thate 1024 lines
        workgroup_size[1] = 1;

        cl_event find_best_fits_event;
        CLCHECK(clEnqueueNDRangeKernel(get_dtd_queue(), z_find_best_calib_profile_kernel , 2, globoff, globsize, workgroup_size,
                                       1,  &cal_fits_event, &find_best_fits_event));

//
//        printf("prof_idx: %d\n", best[0]);
        // compute nomalized fitness ?

        // prepare_first_shift
        globoff[0] = 0;
        globsize[0] = crosses->size;
        workgroup_size[0] = crosses->size;

        //printf("%d\n", (int) workgroup_size[0]);
        cl_event prepare_first_shift_event;
        CLCHECK(clEnqueueNDRangeKernel(get_dtd_queue(), z_prepare_first_shift_kernel, 1, globoff, globsize, workgroup_size, 1,
                                       &find_best_fits_event, &prepare_first_shift_event));

        // simple shift_between_profile (one line)
        globoff[0] = 0;
        globoff[1] = 0;
        globoff[2] = 0;

        globsize[0] = (prof_size / 4);
        globsize[1] = crosses->size;
        globsize[2] = 1;

        workgroup_size[0] = (prof_size / 4);
        workgroup_size[1] = 1;
        workgroup_size[2] = 1;
        cl_event simple_shift_event;
        CLCHECK(clEnqueueNDRangeKernel(get_dtd_queue(), z_simple_shift_between_profile_kernel , 3, globoff, globsize,
                                       workgroup_size, 1,  &prepare_first_shift_event, &simple_shift_event));




        // prepare_shifts
        globoff[0] = 0;
        globsize[0] = crosses->size;
        workgroup_size[0] = crosses->size; // ! we assume that a calibration image as less thate 1024 lines

        cl_event prepare_shifts_event;
        CLCHECK(clEnqueueNDRangeKernel(get_dtd_queue(), z_prepare_shifts_kernel , 1, globoff, globsize, workgroup_size, 1,
                                       &simple_shift_event, &prepare_shifts_event));

        // simple shift_between_profile (3 lines)
        globoff[0] = 0;
        globoff[1] = 0;
        globoff[2] = 0;

        globsize[0] = prof_size / 4;
        globsize[1] = crosses->size;
        globsize[2] = 3;

        workgroup_size[0] = prof_size / 4;
        workgroup_size[1] = 1;
        workgroup_size[2] = 1;

        cl_event simple_shifts_event;
        CLCHECK(clEnqueueNDRangeKernel(get_dtd_queue(), z_simple_shift_between_profile_kernel , 3, globoff, globsize,
                                       workgroup_size, 1,  &prepare_shifts_event, &simple_shifts_event));

//        clFinish(get_dtd_queue());
//        CLCHECK(clEnqueueReadBuffer(get_dth_queue(),
//                                    gpu_phis,
//                                    CL_TRUE,
//                                    0,
//                                    sizeof(cl_float) * crosses->size * 3,
//                                    phis,
//                                    1,
//                                    &simple_shifts_event,
//                                    NULL));
//
//    printf("phi-1 %f phi %f phi+1 %f\n", phis[0], phis[1], phis[2]);

        //        //    /* --------------------- TEST -------------------------------------------------*/
        //        if (crosses->size  > 1)
        //        {
        //            cpu_bead *bead = crosses[0]->bead;
        //            cl_float min;
        //            cl_int rc = crosses[0]->bead_param->max_meaningful_radius;
        //        //
        //            cl_float zprof[1024];
        //            cl_float zproffromgpu[1024];
        //            cl_int best_fit_ids[1024];
        //
        //            CLCHECK(clEnqueueReadBuffer(get_dth_queue(), gpu_phis, CL_TRUE, 0,
        //                                        sizeof(cl_int) * crosses->size * 3, zproffromgpu, 1, &simple_shifts_event, NULL));
        //        //
        //            printf("gpu phi: %f,%f,%f and %f,%f,%f\n",
        //                   zproffromgpu[0],
        //                   zproffromgpu[1],
        //                   zproffromgpu[2],
        //                   zproffromgpu[3],
        //                   zproffromgpu[4],
        //                   zproffromgpu[5]);
        //        //
        //        //    //debug_plot_profile(0, fil->f, prof_size / 2 + 1);
        //        //    //debug_plot_profile(1, zproffromgpu, prof_size / 2 + 1);
        //            debug_plot_profile(0, zproffromgpu, fits_size);
        //            debug_plot_profile(1, zproffromgpu + fits_size, fits_size);
        //        }
        //        //    /* --------------------- END TEST -------------------------------------------------*/

        // compute_3_points_polynome_zero
        globoff[0] = 0;
        globsize[0] = crosses->size;
        workgroup_size[0] = crosses->size; // ! we assume that a calibration image as less thate 1024 lines

        cl_event compute_3_points_polynome_event;
        CLCHECK(clEnqueueNDRangeKernel(get_dtd_queue(), z_compute_3_points_polynome_zero_kernel , 1, globoff, globsize,
                                       workgroup_size, 1,  &simple_shifts_event, &compute_3_points_polynome_event));

        clReleaseEvent(cal_fits_event);
        clReleaseEvent(find_best_fits_event);
        clReleaseEvent(prepare_first_shift_event);
        clReleaseEvent(simple_shift_event);
        clReleaseEvent(prepare_shifts_event);
        clReleaseEvent(simple_shifts_event);
        clReleaseEvent(gpu_beads_z_pos->event);
        gpu_beads_z_pos->event = compute_3_points_polynome_event;
    }

    cl_int *z_pos_int = (cl_int *) calloc(crosses->size, sizeof(cl_int));
    cl_float *z_pos_fraq = (cl_float *) calloc(crosses->size, sizeof(cl_float));

    CLCHECK(clEnqueueReadBuffer(get_dth_queue(), gpu_beads_z_pos->mem, CL_TRUE, 0 ,
                                sizeof(cl_float) * crosses->size, z_pos_fraq, 1, &gpu_beads_z_pos->event, NULL));

    cl_float *best_fits_xi2 = (cl_float *) calloc(crosses->size, sizeof(cpu_coord));
    cl_float *phis = (cl_float *) calloc(crosses->size * 3, sizeof(cl_float));

    if (crosses->calib_type != XV_CALIB_TYPE_NONE)
    {
        CLCHECK(clEnqueueReadBuffer(get_dth_queue(),
                                    gpu_phis,
                                    CL_TRUE,
                                    0,
                                    sizeof(cl_float) * crosses->size * 3,
                                    phis,
                                    1,
                                    &gpu_beads_z_pos->event,
                                    NULL));
        CLCHECK(clEnqueueReadBuffer(get_dth_queue(), gpu_best_fits_ids, CL_TRUE, 0 ,
                                    sizeof(cl_int) * crosses->size, z_pos_int, 1, &gpu_beads_z_pos->event, NULL));

        CLCHECK(clEnqueueReadBuffer(get_dth_queue(),
                                    gpu_best_fits_xi2,
                                    CL_TRUE,
                                    0,
                                    sizeof(cl_float) * crosses->size,
                                    best_fits_xi2,
                                    1,
                                    &gpu_beads_z_pos->event,
                                    NULL));
    }


    clFinish(get_dth_queue());

    for (int i = 0; i < crosses->size; ++i)
    {
        cpu_bead *bead  = crosses->cross_data[i]->bead;

        if (bead)
        {
            if (crosses->calib_type == XV_CALIB_TYPE_NONE)
            {
                bead->z = -z_pos_fraq[i];
                //printf("gpu: %f\n", bead->z);
            }
            else
            {
                bead->z = calib_images->ay + calib_images->dy * (z_pos_int[i] + 1 + z_pos_fraq[i]);
                bead->calib_img_index = z_pos_int[i];
                bead->calib_min_phi = (phis[3 * i] - phis[3 * i + 2]) != 0 ? phis[3 * i + 1] / (phis[3 * i] - phis[3 * i + 2]) : phis[3
                                      * i + 1];
                bead->calib_min_xi2 = best_fits_xi2[i];
            }

            //printf("%f\n", bead->z);
        }
    }

    CLCHECK(clReleaseMemObject(gpu_fourier_profs));
    gpu_fourier_profs = NULL;
    CLCHECK(clReleaseMemObject(gpu_hilbert_profs));
    gpu_hilbert_profs = NULL;
    CLCHECK(clReleaseMemObject(gpu_fits));
    gpu_fits = NULL;
    CLCHECK(clReleaseMemObject(gpu_best_fits_ids));
    gpu_best_fits_ids = NULL;
    CLCHECK(clReleaseMemObject(gpu_best_fits_xi2));
    gpu_best_fits_xi2 = NULL;
    CLCHECK(clReleaseMemObject(gpu_phis));
    gpu_phis = NULL;

    free(z_pos_int);
    z_pos_int = NULL;
    free(z_pos_fraq);
    z_pos_fraq = NULL;
    free(best_fits_xi2);
    best_fits_xi2 = NULL;
    free(phis);
    phis = NULL;

    clReleaseEvent(remove_avg_event);
    clReleaseEvent(rdft_event);
    clReleaseEvent(get_first_mode_event);
    clReleaseEvent(first_mode_norm_event);
    clReleaseEvent(filter_event);
    clReleaseEvent(idft_event);
    clReleaseEvent(complex_module_event);
    clReleaseEvent(ref_profile_event);
}




