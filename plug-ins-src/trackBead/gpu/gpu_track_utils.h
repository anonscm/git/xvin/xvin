#pragma once
#include "track_struct.h"
bool gpu_needs_to_init();
void gpu_tracking_reset();
void gpu_init_kernels(cpu_crosses *crosses);
void gpu_init_mem(gpu_event_driven_memory **gpu_profs, gpu_event_driven_memory **gpu_beads_pos, gpu_event_driven_memory **gpu_beads_z_pos, cpu_coord *beads_pos, cl_int prof_size, cl_int crosses_count);
void gpu_fill_profiles_xy(const cpu_image* images, int crosses_count, gpu_event_driven_memory* gpu_profs, gpu_event_driven_memory *gpu_beads_pos);
void gpu_fill_profile_z(const cpu_image *image, int crosses_count,gpu_event_driven_memory* gpu_profs, gpu_event_driven_memory *gpu_beads_pos);
void gpu_track_xy(cl_int crosses_count,gpu_event_driven_memory* gpu_profs, gpu_event_driven_memory *gpu_beads_pos, gpu_event_driven_memory *gpu_conv_profs);
void gpu_track_z(cpu_crosses *crosses, gpu_event_driven_memory *gpu_profs, gpu_event_driven_memory *gpu_beads_z_pos);
