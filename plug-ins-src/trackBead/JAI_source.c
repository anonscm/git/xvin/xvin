/*******************************************************************************
                         LPS ENS
--------------------------------------------------------------------------------

  Program      : JAI Genicam GigE_Source for bead tracking
  Author       : JFA
  Date         : 27.10.2007

  Purpose      : JAI Manager Functions for XVin
  CVB          :
  Hints        :

  Updates      :
********************************************************************************/
#ifndef _JAI_SOURCE_C_
#define _JAI_SOURCE_C_

#include "allegro.h"
#ifdef XV_WIN32
#include "winalleg.h"
#endif
#include "xvin.h"

/* If you include other plug-ins header do it here*/
//#include "stdafx.h"
#ifdef XV_WIN32
# ifdef SDK_1_2_1
#include "JAI/SDK_1_2_1/Jai_Error.h"
#include "JAI/SDK_1_2_1/Jai_Types.h"
#include "JAI/SDK_1_2_1/Jai_Factory.h"
# endif
# ifdef SDK_1_2_5
#include "JAI/SDK_1_2_5/Jai_Error.h"
#include "JAI/SDK_1_2_5/Jai_Types.h"
#include "JAI/SDK_1_2_5/Jai_Factory.h"
# endif
# ifdef SDK_1_3_0
#include "JAI/SDK_1_3_0/Jai_Error.h"
#include "JAI/SDK_1_3_0/Jai_Types.h"
#include "JAI/SDK_1_3_0/Jai_Factory.h"
# endif
#else
#include "JAI/Jai_Factory.h"
#include "JAI/Jai_Error.h"
#include "JAI/Jai_Types.h"
#endif



#include "JAI_2_XVIN.h"
/* But not below this define */

# define BUILDING_PLUGINS_DLL
#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)

# include "../fft2d/fftbtl32n.h"
# include "fillibbt.h"
# include "trackBead.h"
# include "track_util.h"
# include "JAI_source.h"

# include "../cfg_file/Pico_cfg.h"

//char title[512];
int k_test = 0;
// Global variable
CRITICAL_SECTION CriticalSection;
int Delay_counter = 0;


int timestampsfreq = 50000000;


int JAI_oi_idle_action(struct  one_image *oi, DIALOG *d)
{
    return D_O_K;
}

///////////////////////////////////////////////////////////
//                    OPEN JAI SDK HANDLES FUNCTIONS     //
///////////////////////////////////////////////////////////

# define NO_FACTORY  8

int iOpen_Factory(void)
{

    // Open the Factory
  retval = J_Factory_Open(J_REG_DATABASE , &hFactory); // (int8_t *)""

    if (retval != J_ST_SUCCESS)
      {
	win_printf_OK("Can not open Factory");
	return NO_FACTORY; //
      }
    else     dump_to_error_file_only("fact %u",hFactory);
    return 0;
}

CAM_HANDLE hCam_Open_camera(void)
{
    register int i = 0, j;
    int32_t  index = 0;
    //char **psCameraId = NULL;
    //char question[4096] , string[2048];
    //  CAM_HANDLE hCamera = NULL;
    char test[512] = {0};
    uint32_t nCameras = 0;
    unsigned char bHasChanged = 0;
    int8_t sCameraId[J_CAMERA_ID_SIZE];

    uint32_t size;
    char ctest[512] = {0}, ptest[512] = {0}, *dev[6] = {0}, *stk = NULL, *cname = NULL, *s = NULL;

    dump_to_error_file_only("entering hcam fact %u: File %s line %ul",hFactory,__FILE__, __LINE__);
    if (hFactory == NULL)
    {
        win_printf_OK("Factory should be created First");
        return NULL;
    }

    retval = J_Factory_EnableForceIp  (hFactory, 1);
    if (retval != J_ST_SUCCESS)
      win_printf_OK("Could not enable force IP");

    // Search for cameras on all the networks
    dump_to_error_file_only("bef cam list");
    retval = J_Factory_UpdateCameraList(hFactory, &bHasChanged);
    dump_to_error_file_only("after cam list");
    if (retval == J_ST_SUCCESS && bHasChanged)
      {
    // Get the number of cameras. This number can be larger than the actual camera count because
    // the cameras can be detected through different driver types!
    // This mean that we might need to filter the cameras in order to avoid dublicate references.
	dump_to_error_file_only("before GetNumOfCameras");
	retval = J_Factory_GetNumOfCameras(hFactory, &nCameras);

	if (nCameras <= 0)    return NULL;

	// Run through the list of found cameras
	for (uint32_t index = 0; index < nCameras; ++index)
	  {
	    size = sizeof(sCameraId);
	    // Get CameraID
	    retval = J_Factory_GetCameraIDByIndex(hFactory, index, sCameraId, &size);
	    if (retval == J_ST_SUCCESS)
	      {
		dump_to_error_file_only("Camera %d = %s\n", index, sCameraId);

		// Open the camera
		retval = J_Camera_Open(hFactory, sCameraId, &hCamera);
		if (retval == J_ST_SUCCESS) break;
		// ... do something

		// Close the camera again
		//retval = J_Camera_Close(hCamera);
	      }
	  }
      }


	dump_to_error_file_only("old JF code nCam = %d",nCameras);
	/*
    index = 0;
    size = 512;
    retval = J_Factory_GetCameraIDByIndex(hFactory, index, (int8_t *)test, &size);
    strncpy(ctest, test, 512);
	*/

    strncpy(ctest, sCameraId, size);
    for (i = 0, stk = strtok(ctest, "::"), ptest[0] = 0; i < 6 && stk != NULL; i++, stk = strtok(NULL, "::"))
    {
        cname = dev[i] = stk;
        strncpy(ptest + strlen(ptest), stk, 512 - strlen(ptest));
        strncpy(ptest + strlen(ptest), "\n", 512 - strlen(ptest));
    }

    dump_to_error_file_only("Cam 0 has %d field \n%s",i,ptest);
    if (i > 4)
    {
        s = dev[4];

        for (j = 0; s[j] != 0; j++)
            s[j] = (s[j] == '\n' || s[j] == '\r') ? 0 : s[j];

        if (Pico_param.camera_param.camera_manufacturer)      free(Pico_param.camera_param.camera_manufacturer);

        Pico_param.camera_param.camera_manufacturer = strdup(dev[4]);
    }

    if (cname != NULL)
    {
        s = cname;

        for (j = 0; s[j] != 0; j++)
            s[j] = (s[j] == '\n' || s[j] == '\r') ? 0 : s[j];

        if (Pico_param.camera_param.camera_model)      free(Pico_param.camera_param.camera_model);

        Pico_param.camera_param.camera_model = strdup(cname);

        // changed may 25th 2009 previous trk will have a wrong recording in freq
        if (strstr(cname, "CM-140") != NULL) timestampsfreq = 62500000;


        dump_to_error_file_only("Camera 0 found %s",Pico_param.camera_param.camera_model);

    }

    dump_to_error_file_only("index = %d \n test = %s",index,test);
    /* if (retval == J_ST_SUCCESS && nCameras > 0) */
    /*  { */
    /*    psCameraId = (char **)calloc(nCameras, sizeof(char*)); */
    /*    for (index = 0 ; index < nCameras ; index++) */
    /*      { */
    /*        psCameraId[index] = (char *)calloc(512,sizeof(char));//allow only 64 char in the string */
    /*        if (psCameraId[index] == NULL) */
    /*      { */
    /*        win_printf_OK("Calloc Pb for CameraStrings"); */
    /*        return NULL; */
    /*      } */
    /*      } */
    /*    snprintf(question,4096,"Choose you camera \n"); */
    /*    // Run through the list of found cameras */
    /*    for ( index = 0; index < nCameras; index++) */
    /*      { */
    /*        size = sizeof(psCameraId[index]); */
    /*        // Get CameraID */
    /*        retval = J_Factory_GetCameraIDByIndex(hFactory, index,psCameraId[index], &size); */
    /*        win_printf("camera i %d retval %d\nname:\n%s",index,retval,psCameraId[index]); */
    /*        if (index == 0)retval = J_Factory_GetCameraIDByIndex(hFactory, index,test, &size); */
    /*        win_printf("index = %d \n cameraid = %s \n test = %s",index,*(psCameraId +index),test); */
    /*       /\*  if (retval == J_ST_SUCCESS) *\/ */
    /* /\*          { *\/ */
    /* /\*            if (index == 0 ) *\/ */
    /* /\*          { *\/ */
    /* /\*            snprintf(string , 2048,"Camera %d = %s" , index, psCameraId[index]); *\/ */
    /* /\*            strcat(string," %R \n"); *\/ */
    /* /\*          } *\/ */
    /* /\*            else  *\/ */
    /* /\*          { *\/ */
    /* /\*            snprintf(string , 2048,"Camera %d = %s " , index, psCameraId[index]); *\/ */
    /* /\*            strcat(string," %r \n"); *\/ */
    /* /\*          } *\/ */
    /* /\*            strcat(question,string); *\/ */
    /* /\*          } *\/ */
    /*      }    */
    /*  }  */
    /*    i = win_scanf(question,&index); */
    /*    if (i == CANCEL)  */
    /*      { */
    /*        win_printf_OK("END"); */
    /*        return NULL; */
    /*      } */
    // Open the camera
    /*
    retval = J_Camera_Open(hFactory, test, &hCamera); //(psCameraId+2)

    if (retval != J_ST_SUCCESS)
    {
        win_printf_OK("Can't open camera %d", retval);
        return NULL;
    }
    */
    return hCamera;
}

int iOpen_JAI_DataStream_and_set_buffers_for_XVIN(O_i *oi, CAM_HANDLE hCamera)
{

    int i;
    int  in = -1, size;
    uint32_t iNumber_of_DataStream = -1;

    if (oi_JAI == NULL)
    {
        win_printf("No image in for set pointers");
        return 1;
    }

    if (hCamera == NULL)
    {
        win_printf_OK("Camera should be opened first");
        return 2;
    }

    //First need for datastream creation
    J_Camera_GetNumOfDataStreams(hCamera, &iNumber_of_DataStream);

    //win_printf("Num data Streams %d",iNumber_of_DataStream );
    in = (iNumber_of_DataStream > 0) ? iNumber_of_DataStream - 1 : 0;

    if (J_ST_SUCCESS != J_Camera_CreateDataStream(hCamera, in , &hDS))
        win_printf("Pb creating hDS");

    JAI_Im_buffer_ID  = (void **) calloc(oi_JAI->im.n_f, sizeof(void *));

    if (JAI_Im_buffer_ID == NULL)
    {
        win_printf("Could not allocate memory for Buffer ID");
        return 3;
    }

    for (i = 0 ; i < oi_JAI->im.n_f ; i++)
    {
        // Announce the buffer pointer to the Acquisition engine.
        size = oi_JAI->im.nx * oi_JAI->im.ny * ((oi_JAI->im.data_type == IS_CHAR_IMAGE) ? 1 : 2);

        if (J_ST_SUCCESS != J_DataStream_AnnounceBuffer(hDS, oi_JAI->im.mem[i] , size, NULL, JAI_Im_buffer_ID + i))
        {
            win_printf("Announce Buffer failed");
            return 4;
        }

        // Queueing it.
        if (J_ST_SUCCESS != J_DataStream_QueueBuffer(hDS, JAI_Im_buffer_ID[i]))
        {
            win_printf("Failed in Queue buffer");
            return 5;
        }
    }

    return 0;
}


//////////////////////////////////////////////////////////////////
//                 CLOSE JAI HANDLES FUNCTIONS                  //
/////////////////////////////////////////////////////////////////
int iClose_Camera(void)
{
    retval = J_Camera_Close(hCamera);

    if (retval == J_ST_SUCCESS)
        return win_printf_OK("Can not close Camera");

    return D_O_K;
}
int iClose_DataStream(void)
{
    retval = J_DataStream_Close(hDS);

    if (retval == J_ST_SUCCESS)
        return win_printf_OK("Can not close DataStream");

    return D_O_K;
}
int iClose_Factory(void)
{
    // Close the factory
    retval = J_Factory_Close(hFactory);

    if (retval == J_ST_SUCCESS)
        return win_printf_OK("Can not close Factory");

    return D_O_K;
}

int set_JAI_gain(int gain)
{
    NODE_HANDLE     hNode;

    if (hCamera == NULL) return 1;

    retval = J_Camera_GetNodeByName(hCamera, (int8_t *)"GainRaw", &hNode);  // Get the node handle

    if (retval == J_ST_SUCCESS)
        retval = J_Node_SetValueInt64(hNode, FALSE, gain);         // Set the value

    return (retval == J_ST_SUCCESS) ? 0 : 1;
}

int get_JAI_gain(int *gain, int *min, int *max, int *inc)
{
    NODE_HANDLE     hNode;
    int ret = 0;
    long long gain64 = 0;

    if (hCamera == NULL) return 1;

    retval = J_Camera_GetNodeByName(hCamera, (int8_t *)"GainRaw", &hNode);  // Get the node handle

    if (retval != J_ST_SUCCESS) return 1;

    retval = J_Node_GetValueInt64(hNode, FALSE, &gain64);         // Set the value
    *gain = (int)gain64;
    ret = (retval == J_ST_SUCCESS) ? ret : 1;
    retval = J_Node_GetMinInt64(hNode, &gain64);         // Set the value
    *min = (int)gain64;
    ret = (retval == J_ST_SUCCESS) ? ret : 1;
    retval = J_Node_GetMaxInt64(hNode, &gain64);         // Set the value
    *max = (int)gain64;
    ret = (retval == J_ST_SUCCESS) ? ret : 1;
    retval = J_Node_GetInc(hNode, &gain64);         // Set the value
    *inc = (int)gain64;
    ret = (retval == J_ST_SUCCESS) ? ret : 1;
    return ret;
}
/*
  if (strstr(model,"CM-140GE") != NULL) f = 4.65;
  else if (strstr(model,"CV-A10GE") != NULL) f = 8.3;
  else if (strstr(model,"BM-141GE") != NULL) f = 6.45;
  else if (strstr(model,"TM-6740GE") != NULL) f = 7.4;
  else if (strstr(model,"CM-030GE") != NULL)  f = 7.4;
  else if (strstr(model,"CV-M40") != NULL) f = 9.9;
  else if (strstr(model,"CV-M30") != NULL) f = 8.4;
*/
// set gain in normalized unit
int _set_camera_gain(float gain, float *set, float *fmin, float *fmax, float *finc)
{
    int pgain, igain, min, max, inc;

    if (get_JAI_gain(&pgain, &min, &max, &inc))
        return 1;

    gain = fabs(gain);

    if ((strstr(Pico_param.camera_param.camera_model, "CM-140") != NULL)
            || (strstr(Pico_param.camera_param.camera_model, "CV-A10GE") != NULL)
            || (strstr(Pico_param.camera_param.camera_model, "CM-030GE") != NULL)
            || (strstr(Pico_param.camera_param.camera_model, "CM-040GE") != NULL))
    {
        if (gain > 4)
        {
            gain = 4;
            igain = max;
        }

        if (gain < 0.707)
        {
            gain = 0.707;
            igain = min;
        }

        igain = (int)(0.5 + (log10(gain) * (float)(max * 5)) / 3);

        if (igain > max)  igain = max;

        if (igain < min)  igain = min;

        if (fmin) *fmin = 0.707;

        if (fmax) *fmax = 4;

        if (finc) *finc = (float)4 / max;
    }
    else if (strstr(Pico_param.camera_param.camera_model, "BM-141GE") != NULL)
    {
        if (gain > 16)
        {
            gain = 16;
            igain = max;
        }

        if (gain < 0.707)
        {
            gain = 0.707;
            igain = min;
        }

        igain = (int)(0.5 + (log10(gain) * (float)(max * 5)) / 6);

        if (igain > max)  igain = max;

        if (igain < min)  igain = min;

        if (fmin) *fmin = 0.707;

        if (fmax) *fmax = 16;

        if (finc) *finc = ((float)16) / max;
    }
    else return 2;

    set_JAI_gain(igain);

    if (set) *set = gain;

    return 0;
}



int _get_camera_gain(float *gain, float *fmin, float *fmax, float *finc)
{
    int igain, min, max, inc;

    if (get_JAI_gain(&igain, &min, &max, &inc))
        return 1;

    if ((strstr(Pico_param.camera_param.camera_model, "CM-140") != NULL)
            || (strstr(Pico_param.camera_param.camera_model, "CV-A10GE") != NULL)
            || (strstr(Pico_param.camera_param.camera_model, "CM-030GE") != NULL)
            || (strstr(Pico_param.camera_param.camera_model, "CM-040GE") != NULL))
    {
        if (gain) *gain = pow(10, (float)(3 * igain) / (5 * max));

        if (fmin) *fmin = 0.707;

        if (fmax) *fmax = 4;

        if (finc) *finc = 4 / max;
    }
    else if (strstr(Pico_param.camera_param.camera_model, "BM-141GE") != NULL)
    {
        if (gain) *gain = pow(10, (float)(6 * igain) / (5 * max));

        if (fmin) *fmin = 0.707;

        if (fmax) *fmax = 16;

        if (finc) *finc = ((float)16) / max;
    }
    else return 2;

    return 0;
}





int change_gain(void)
{
    int i, gain = 0, inc = 0, min = 0, max = 0;
    char message[1024] = {0};

    if (updating_menu_state != 0)  return D_O_K;

    if (get_JAI_gain(&gain, &min, &max, &inc))
        return win_printf_OK("Problem to access camera gain!");

    snprintf(message, sizeof(message), "Camera gain min %d max %d increment %d\n"
             "New value %%10d \n", min , max, inc);

    i = win_scanf(message, &gain);

    if (i == CANCEL)   return D_O_K;

    if (set_JAI_gain(gain))
        return win_printf_OK("Problem setting camera gain!");

    return D_O_K;
}

int get_JAI_shutter_mode(void)
{
    NODE_HANDLE     hNode, hNode1, hNode_us = NULL, hNode_li = NULL, hNode_ps = NULL;
    NODE_HANDLE hEnumEntryNode, hEnumEntryNode1;
    J_NODE_TYPE nType;
    int64_t iEnumEntryValue, EntryValue = 0, iEnumEntryValue1, EntryValue1 = 0;
    int mode = 0, i, j, k, mode1 = 0, mode10 = 0, n_us = 0, n_li = 0, n_us0 = 0, n_li0 = 0;
    unsigned int n_enum = 0, n_enum1 = 0;
    char message[4096] = {0}, name[128] = {0},  name1[128] = {0}, *pat = NULL;
    uint32_t iSizeOfName;

    if (updating_menu_state != 0)  return D_O_K;

    if (hCamera == NULL) return 0;

    retval = J_Camera_GetNodeByName(hCamera, (int8_t *)"ShutterMode", &hNode);  // Get the node handle

    if (retval == J_ST_SUCCESS)
        retval = J_Node_GetNumOfEnumEntries(hNode, &n_enum);         // Set the value

    J_Node_GetValueInt64(hNode, 0, &iEnumEntryValue);
    EntryValue = iEnumEntryValue;

    if (n_enum == 0)    return win_printf_OK("Problem access shutter mode!");

    snprintf(message, sizeof(message), "Shutter mode setting:\n");

    for (i = 0; i < n_enum; i++)
    {
        retval = J_Node_GetEnumEntryByIndex(hNode, i, &hEnumEntryNode);

        if (retval == J_ST_SUCCESS)
        {
            iSizeOfName = sizeof(name);
            retval = J_Node_GetDisplayName(hEnumEntryNode, (int8_t *)name, &iSizeOfName);

            if (retval == J_ST_SUCCESS)
            {
                if (i == 0)  snprintf(message + strlen(message), sizeof(message) - strlen(message), "%%R %s\n", name);
                else         snprintf(message + strlen(message), sizeof(message) - strlen(message), "%%r %s\n", name);
            }

            J_Node_GetEnumEntryValue(hEnumEntryNode, &iEnumEntryValue);

            if (EntryValue == iEnumEntryValue)          mode = i;

        }
    }


    for (i = 0; i < n_enum; i++)
    {
        retval = J_Node_GetEnumEntryByIndex(hNode, i, &hEnumEntryNode);

        if (retval == J_ST_SUCCESS)
        {
            iSizeOfName = sizeof(name);
            retval = J_Node_GetName(hEnumEntryNode, (int8_t *)name, &iSizeOfName, 0);

            if ((pat = strstr(name, "ShutterMode")) != NULL)
            {
                for (j = 0; pat[j] != 0 && pat[j] != '_'; j++);

                if (pat[j] != 0)
                {
                    retval = J_Camera_GetNodeByName(hCamera, (int8_t *)pat + j + 1, &hNode1);  // Get the node handle

                    if (retval == J_ST_SUCCESS)
                    {
                        if (i == 0) hNode_ps = hNode1;
                        else  if (i == 3) hNode_us = hNode1;

                        retval = J_Node_GetNumOfEnumEntries(hNode1, &n_enum1);         // Set the value
                        iSizeOfName = sizeof(name);
                        J_Node_GetDisplayName(hNode1, (int8_t *)name, &iSizeOfName);
                        snprintf(message + strlen(message), sizeof(message) - strlen(message),
                                 "For option %s you have the folowing choice\n", name);

                        J_Node_GetType(hNode1, &nType);

                        if (nType == J_IEnumeration)
                        {
                            retval = J_Node_GetNumOfEnumEntries(hNode1, &n_enum1);         // Set the value
                            J_Node_GetValueInt64(hNode1, 0, &iEnumEntryValue1);
                            EntryValue1 = iEnumEntryValue1;

                            for (k = 0; k < n_enum1; k++)
                            {
                                retval = J_Node_GetEnumEntryByIndex(hNode1, k, &hEnumEntryNode1);

                                if (retval == J_ST_SUCCESS)
                                {
                                    iSizeOfName = sizeof(name);
                                    retval = J_Node_GetDisplayName(hEnumEntryNode1, (int8_t *)name, &iSizeOfName);

                                    if (retval == J_ST_SUCCESS)
                                    {
                                        if (k == 0)  snprintf(message + strlen(message),
                                                                  sizeof(message) - strlen(message), "%%R %s  ", name);
                                        else         snprintf(message + strlen(message),
                                                                  sizeof(message) - strlen(message), "%%r %s  %s", name,
                                                                  ((k % 3 == 2) ? "\n" : ""));
                                    }

                                    J_Node_GetEnumEntryValue(hEnumEntryNode1, &iEnumEntryValue1);

                                    if (EntryValue1 == iEnumEntryValue1)        mode10 = mode1 = k;
                                }
                            }
                        }
                        else if (nType == J_IInteger)
                        {
                            J_Node_GetValueInt64(hNode1, 0, &iEnumEntryValue1);
                            n_us0 = n_us = iEnumEntryValue1;
                            iSizeOfName = sizeof(name);
                            retval = J_Node_GetDisplayName(hNode1, (int8_t *)name, &iSizeOfName);
                            snprintf(message + strlen(message),
                                     sizeof(message) - strlen(message), "Set %s to %%8d \\mu s\n", name);

                        }

                    }
                    else if (i == 1)
                    {
                        retval = J_Camera_GetNodeByName(hCamera, (int8_t *)"ExposureTimeRaw", &hNode1);  // Get the node handle

                        if (retval == J_ST_SUCCESS)
                        {
                            if (i == 1) hNode_li = hNode1;

                            iSizeOfName = sizeof(name);
                            J_Node_GetDisplayName(hNode1, (int8_t *)name, &iSizeOfName);
                            snprintf(message + strlen(message), sizeof(message) - strlen(message),
                                     "For option %s you have the folowing choice\n", name);
                            J_Node_GetType(hNode1, &nType);

                            if (nType == J_IInteger)
                            {
                                J_Node_GetValueInt64(hNode1, 0, &iEnumEntryValue1);
                                n_li0 = n_li = iEnumEntryValue1;
                                iSizeOfName = sizeof(name);
                                retval = J_Node_GetDisplayName(hNode1, (int8_t *)name, &iSizeOfName);
                                snprintf(message + strlen(message),
                                         sizeof(message) - strlen(message), "Set %s to %%8d lines \n", name);
                            }
                        }

                    }

                    //else win_printf("node name %s not found\n",pat+j+1);
                }

            }
        }
    }


    i = win_scanf(message, &mode, &mode1, &n_li, &n_us);

    if (i == CANCEL)   return D_O_K;

    retval = J_Node_GetEnumEntryByIndex(hNode, mode, &hEnumEntryNode);

    if (retval == J_ST_SUCCESS)
    {
        iSizeOfName = sizeof(name);
        //retval = J_Node_GetName(hEnumEntryNode, name, &iSizeOfName, 0);
        retval = J_Node_GetDisplayName(hEnumEntryNode, (int8_t *)name, &iSizeOfName);
        J_Node_GetEnumEntryValue(hEnumEntryNode, &iEnumEntryValue);
        J_Node_SetValueInt64(hNode, 0, iEnumEntryValue);

        J_Node_GetEnumEntryByIndex(hNode_ps, mode1, &hEnumEntryNode1);

        iSizeOfName = sizeof(name1);
        retval = J_Node_GetDisplayName(hEnumEntryNode1, (int8_t *)name1, &iSizeOfName);

        if (hNode_li != NULL && n_li != n_li0)
            J_Node_SetValueInt64(hNode_li, 0, iEnumEntryValue = n_li);

        if (hNode_us != NULL && n_us != n_us0)
            J_Node_SetValueInt64(hNode_us, 0, iEnumEntryValue = n_us);

        if (hNode_ps != NULL && mode10 != mode1)
            J_Node_SetValueInt64(hNode_ps, 0, iEnumEntryValue = mode1);

        if (mode == 0)
            win_printf_OK("Shutter mode set to %s\nwith %s exposure", name, name1);
        else if (mode == 1)
            win_printf_OK("Shutter mode set to %s\nwith an exposure of %d lines", name, n_li);
        else if (mode == 2)
            win_printf_OK("Shutter mode set to %s", name);
        else if (mode == 3)
        {
            win_printf_OK("Shutter mode set to %s\nwith an exposure of %d \\mu s", name, n_us);
            Pico_param.camera_param.shutter_time_us = n_us;
        }
    }


    return  D_O_K;
}

int get_JAI_shutter_reg_mode(void)
{
    int64_t regval;
    uint32_t iSizeOfName;
    int i, val = -1, val1, shutter_mode = 0, val2;//, min, max
    unsigned char *uc = (unsigned char *)&val, *uc1 = (unsigned char *)&val1;



    if (updating_menu_state != 0)  return D_O_K;

    if (hCamera == NULL) return 0;

    iSizeOfName = 4;
    retval = J_Camera_ReadData(hCamera, regval = 0xA000, (void *)&val, &iSizeOfName);

    if (retval == J_ST_SUCCESS)
    {
        for (i = 0; i < 4; i++) uc1[i] = uc[3 - i];

        shutter_mode = val1;
        //win_printf("Shutter by reg = %d",val1);
    }

    if (strstr(Pico_param.camera_param.camera_model, "CM-140") != NULL)
    {
        regval = 0xA018;
        //min = 61;
        //max = 32174;
    }
    else if (strstr(Pico_param.camera_param.camera_model, "CV-A10GE") != NULL)
    {
        regval = 0xA054;
        //min = 3;
        //max = 16666;
    }
    else if (strstr(Pico_param.camera_param.camera_model, "CM-030GE") != NULL)
    {
        regval = 0xA018;
        //min = 43;
        //max = 11038;
    }
    else if (strstr(Pico_param.camera_param.camera_model, "BM-141GE") != NULL)
    {
        regval = 0xA018;
        //min = 63;
        //max = 33192;
    }
    else return win_printf_OK("Unknown camera ");

    retval = J_Camera_ReadData(hCamera, regval, (void *)&val, &iSizeOfName);

    if (retval == J_ST_SUCCESS)
    {
        for (i = 0; i < 4; i++) uc1[i] = uc[3 - i];

        val2 = val1;
        i = win_scanf("Shutter by reg = %d \\mu s", &val2);

        if (i == CANCEL) return 0;

        if (shutter_mode != 4)
        {
            val1 = 4;

            for (i = 0; i < 4; i++) uc[i] = uc1[3 - i];

            retval = J_Camera_WriteData(hCamera, 0xA000, (void *)&val, &iSizeOfName);
        }

        val1 = val2;

        for (i = 0; i < 4; i++) uc[i] = uc1[3 - i];

        retval = J_Camera_WriteData(hCamera, regval, (void *)&val, &iSizeOfName);
    }

    return 0;
}

// if exp = 0, set to shutter off, else set exposure in micro second

int _set_camera_shutter_in_us(int exp, int *set, int *smin, int *smax)
{
    int64_t regval;
    uint32_t iSizeOfName = 4;
    int i, val = -1, val1, min, max, itmp;
    unsigned char *uc = (unsigned char *)&val, *uc1 = (unsigned char *)&val1;

    if (hCamera == NULL) return 1;

    //itmp = val1 = (exp == 0) ? 0 : 4;

    if (strstr(Pico_param.camera_param.camera_model, "CM-140") != NULL)
    {
        regval = 0xA018;
        min = 61;
        max = 32174;
    }
    else if (strstr(Pico_param.camera_param.camera_model, "CV-A10GE") != NULL)
    {
        regval = 0xA054;
        min = 3;
        max = 16666;
    }
    else if (strstr(Pico_param.camera_param.camera_model, "CM-030GE") != NULL)
    {
        regval = 0xA018;
        min = 43;
        max = 11038;
    }
    else if (strstr(Pico_param.camera_param.camera_model, "CM-040GE") != NULL)
    {
        regval = 0xA018;
        min = 51;
        max = 16353;
    }
    else if (strstr(Pico_param.camera_param.camera_model, "BM-141GE") != NULL)
    {
        regval = 0xA018;
        min = 63;
        max = 33192;
    }
    else return 5;

    itmp = val1 = 4;

    for (i = 0; i < 4; i++) uc[i] = uc1[3 - i];

    retval = J_Camera_WriteData(hCamera, 0xA000, (void *)&val, &iSizeOfName);

    if (retval != J_ST_SUCCESS)  return 2;

    retval = J_Camera_ReadData(hCamera,  0xA000, (void *)&val, &iSizeOfName);

    if (retval != J_ST_SUCCESS)  return 3;

    for (i = 0; i < 4; i++) uc1[i] = uc[3 - i];

    if (val1 != itmp)     return 4;


    if (exp == 0) exp = max;  // no shutter means maximum exposure

    if (exp < min) exp = min;

    if (exp > max) exp = max;

    val1 = exp;

    for (i = 0; i < 4; i++) uc[i] = uc1[3 - i];

    retval = J_Camera_WriteData(hCamera, regval, (void *)&val, &iSizeOfName);

    if (retval != J_ST_SUCCESS)  return 6;

    retval = J_Camera_ReadData(hCamera, regval, (void *)&val, &iSizeOfName);

    if (retval != J_ST_SUCCESS)  return 7;

    for (i = 0; i < 4; i++) uc1[i] = uc[3 - i];

    if (set) *set = val1;

    if (smin) *smin = min;

    if (smax) *smax = max;

    return 0;
}

int _get_camera_shutter_in_us(int *set, int *smin, int *smax)
{
    int64_t regval;
    uint32_t iSizeOfName = 4;
    int i, val = -1, val1, min, max, itmp;
    unsigned char *uc = (unsigned char *)&val, *uc1 = (unsigned char *)&val1;

    if (hCamera == NULL) return 1;

    if (strstr(Pico_param.camera_param.camera_model, "CM-140") != NULL)
    {
        regval = 0xA018;
        min = 61;
        max = 32174;
    }
    else if (strstr(Pico_param.camera_param.camera_model, "CV-A10GE") != NULL)
    {
        regval = 0xA054;
        min = 3;
        max = 16666;
    }
    else if (strstr(Pico_param.camera_param.camera_model, "CM-030GE") != NULL)
    {
        regval = 0xA018;
        min = 43;
        max = 11038;
    }
    else if (strstr(Pico_param.camera_param.camera_model, "CM-040GE") != NULL)
    {
        regval = 0xA018;
        min = 51;
        max = 16353;
    }
    else if (strstr(Pico_param.camera_param.camera_model, "BM-141GE") != NULL)
    {
        regval = 0xA018;
        min = 63;
        max = 33192;
    }
    else return 4;

    itmp = val1 = 4;
    retval = J_Camera_ReadData(hCamera,  0xA000 , (void *)&val, &iSizeOfName);

    if (retval != J_ST_SUCCESS)  return 2;

    for (i = 0; i < 4; i++) uc1[i] = uc[3 - i];

    if (val1 != itmp)     return 3;

    retval = J_Camera_ReadData(hCamera, regval, (void *)&val, &iSizeOfName);

    if (retval != J_ST_SUCCESS)  return 5;

    for (i = 0; i < 4; i++) uc1[i] = uc[3 - i];

    if (set) *set = val1;

    if (smin) *smin = min;

    if (smax) *smax = max;

    return 0;
}

# ifdef ENCOURS
int _get_camera_timestamp_freq(int *freq)
{
    int64_t regval;
    uint32_t iSizeOfName = 4;
    int i, val = -1, val1, min, max, itmp;
    unsigned char *uc = (unsigned char *)&val, *uc1 = (unsigned char *)&val1;


    if (hCamera == NULL) return 1;

    iSizeOfName = 4;
    retval = J_Camera_ReadData(hCamera, regval = 0xA000, (void *)&val, &iSizeOfName);

    if (retval == J_ST_SUCCESS)
    {
        for (i = 0; i < 4; i++) uc1[i] = uc[3 - i];

        shutter_mode = val1;
        win_printf("Shutter by reg = %d", val1);
    }

    if (strstr(Pico_param.camera_param.camera_model, "CM-140") != NULL)
        regval = 0x093C;
    else if (strstr(Pico_param.camera_param.camera_model, "CV-A10GE") != NULL)
        regval = 0xA054;
    else if (strstr(Pico_param.camera_param.camera_model, "CM-030GE") != NULL)
        regval = 0xA018;
    else if (strstr(Pico_param.camera_param.camera_model, "BM-141GE") != NULL)
        regval = 0xA018;
    else return 2;

    retval = J_Camera_ReadData(hCamera, regval, (void *)&val, &iSizeOfName);

    if (retval == J_ST_SUCCESS)
    {
        for (i = 0; i < 4; i++) uc1[i] = uc[3 - i];

        //i = win_scanf("Shutter by reg = %d \\mu s",&val2);
        //if (i == CANCEL) return 0;
        if (shutter_mode != 4)
        {
            val1 = 4;

            for (i = 0; i < 4; i++) uc[i] = uc1[3 - i];

            retval = J_Camera_WriteData(hCamera, 0xA000, (void *)&val, &iSizeOfName);
        }

        val1 = val2;

        for (i = 0; i < 4; i++) uc[i] = uc1[3 - i];

        retval = J_Camera_WriteData(hCamera, regval, (void *)&val, &iSizeOfName);
    }





    int igain, min, max, inc;

    if (get_JAI_gain(&igain, &min, &max, &inc))
        return 1;

    if ((strstr(Pico_param.camera_param.camera_model, "CM-140") != NULL)
            || (strstr(Pico_param.camera_param.camera_model, "CV-A10GE") != NULL)
            || (strstr(Pico_param.camera_param.camera_model, "CM-030GE") != NULL)
            || (strstr(Pico_param.camera_param.camera_model, "CM-040GE") != NULL))
    {
        if (gain) *gain = pow(10, (float)(3 * igain) / (5 * max));

        if (fmin) *fmin = 0.707;

        if (fmax) *fmax = 4;

        if (finc) *finc = 4 / max;
    }
    else if (strstr(Pico_param.camera_param.camera_model, "BM-141GE") != NULL)
    {
        if (gain) *gain = pow(10, (float)(6 * igain) / (5 * max));

        if (fmin) *fmin = 0.707;

        if (fmax) *fmax = 16;

        if (finc) *finc = ((float)16) / max;
    }
    else return 2;

    return 0;
}

# endif

////////////////////////////////////////////////////////////////////////
// END OF CLOSE JAI HANDLES FUNCTIONS                                 //
///////////////////////////////////////////////////////////////////////


//--------------------------------------------------------------------------------------------------
// Create Stream Thread
//--------------------------------------------------------------------------------------------------
BOOL CreateStreamThread(void)
{

    DWORD dwThreadId;

    // Is it already created?
    if (hStreamThread)
        return FALSE;

    // Stream thread event created?
    if (hStreamEvent == NULL)
        hStreamEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    else
        ResetEvent(hStreamEvent);

    // Set the thread execution flag
    bEnableThread = TRUE;
    hStreamThread = CreateThread(
                        NULL,              // default security attributes
                        0,                 // use default stack size
                        (LPTHREAD_START_ROUTINE)StreamProcess,// thread function
                        NULL,       // argument to thread function
                        0,                 // use default creation flags
                        &dwThreadId);      // returns the thread identifier

    if (hStreamThread == NULL)
    {
        //      J_DataStream_Close(m_hDS);
        win_printf_OK("No thread created");
        return FALSE;
    }

    SetThreadPriority(hStreamThread,  THREAD_PRIORITY_TIME_CRITICAL);
    return TRUE;
}


//==============================================================////
// Stream Processing Function
//==============================================================////

DWORD WINAPI ProcessTreatement(LPVOID lpParam)
{
    Bid *p = NULL;
    uint64_t local_NumberFramesDelivered;
    long long t, dt, lTimeStamp;
    TreatementStruct *pTreatement_Struct = NULL;
    int i, im_n;
    int iWaitResult;

    pTreatement_Struct = (TreatementStruct *) lpParam;

    while (bEnableThread)
    {
        iWaitResult = WaitForSingleObject(hReadyEvent, 60000);

        if (WAIT_OBJECT_0 == iWaitResult/* || WAIT_OBJECT_0+1 == iWaitResult*/)
        {
            // Request ownership of the critical section.
            //EnterCriticalSection(&CriticalSection);
            // Access the shared resource.
            //Image_to_treat = Delay_counter;
            // Release ownership of the critical section.
            //LeaveCriticalSection(&CriticalSection);
            local_NumberFramesDelivered = lNumberFramesDelivered;
            p = &bid;

            if (p == NULL) return win_printf_OK("MERDE");

            //lNumberFramesDelivered = pTreatement_Struct->lNumberImageDelivered ;
            if (p->first_im == 0) p->first_im = (int) lNumberFramesDelivered;

            lTimeStamp = pTreatement_Struct->lTimeStamp;
            t = lTimeStamp;//my_ulclock();
            dt = my_uclock();


            p->previous_in_time_fr = t = my_ulclock();//modifier
            p->timer_dt = 0;//idem
            p->previous_fr_nb = (int)lNumberFramesDelivered;

            for (i = 0 ; lNumberFramestreated < local_NumberFramesDelivered ; im_n++, i++, lNumberFramestreated++)
            {
                switch_frame(oi_TRACK, (int)lNumberFramestreated % oi_TRACK->im.n_f); /*A modifier en cas de retard*/
                t = lTimeStamp;//my_ulclock();
                dt = my_uclock();
                p->previous_fr_nb = im_n = (int)lNumberFramestreated;

                for (p->timer_do(imr_TRACK, oi_TRACK, im_n, d_TRACK, t , dt , p->param, i) ;
                        p->timer_do != NULL && p->next != NULL; p = p->next)
                {
                    p->timer_do(imr_TRACK, oi_TRACK, im_n, d_TRACK, t, dt, p->param, i);
                }
            }

            // Request ownership of the critical section.
            //EnterCriticalSection(&CriticalSection);

            // Access the shared resource.
            //Delay_counter -= Image_to_treat;
            // Release ownership of the critical section.
            //LeaveCriticalSection(&CriticalSection);

        }

    }

    return D_O_K;
}

void StreamProcess(void)
{
    J_STATUS_TYPE iResult;
    size_t iSize;
    BUF_HANDLE  iBufferID, iOrigin;
    // Create structure to be used for image display
    //J_tIMAGE_INFO   tAqImageInfo = {0, 0, 0, 0, NULL};
# ifdef SDK_1_2_1
    DWORD iWaitResult;
# endif
    HANDLE hEventNewImage = NULL;
    //# ifdef SDK_1_2_5
# if defined(SDK_1_2_5) || defined(SDK_1_3_0)
    J_COND_WAIT_RESULT  WaitResult;
    HANDLE  m_hCondition;
# endif
    HANDLE    EventList[2];
    EVT_HANDLE    hEvent; // Buffer event handle
    NODE_HANDLE     hNode;
    //DWORD dwThreadId = 0;
    int first = 1;
    //HANDLE hTreatementThread = NULL;
    //TreatementStruct TreatementStruct;
    Bid *p = NULL;
    uint64_t b_i_n;// local_NumberFramesDelivered,
    //# ifdef SDK_1_2_5
# if defined(SDK_1_2_5) || defined(SDK_1_3_0)
    uint64_t      iQueued = 0;
# endif

    long long t, dt, lTimeStamp = 0, llt, jai_origin = 0;//, ulclock_ori;
    //TreatementStruct * pTreatement_Struct = NULL;
    int i = 0, im_n = 0, my_index, j, fr_delivered, prev_fr_delivered = 0, i_todo, k; // , *pI
    //void *pBuffer;
    uint32_t pSize;
    double min_delay_camera_thread_in_ms;


    if (imr_TRACK == NULL || oi_TRACK == NULL)    return ;

    //  if (!InitializeCriticalSectionAndSpinCount(&CriticalSection, 4000/*0x80000400*/) )     return;

    hReadyEvent = CreateEvent(NULL, TRUE, TRUE/*FALSE*/, NULL);

    /*  hTreatementThread =  CreateThread(NULL,             // default security attributes */
    /*                  0,                // use default stack size */
    /*                  ProcessTreatement,// thread function */
    /*                  (void *)&TreatementStruct,             // argument to thread function */
    /*                  0,                // use default creation flags */
    /*                  &dwThreadId);     // returns the thread identifier */

    /*   if (hTreatementThread == NULL)     win_printf_OK("No thread created"); */
    /*   SetThreadPriority(hTreatementThread,  THREAD_PRIORITY_TIME_CRITICAL); */

    // Is the kill event already defined?
    if (hEventFreeze != NULL)    CloseHandle(hEventFreeze);

    // Create a new one
    hEventFreeze = CreateEvent(NULL, TRUE, FALSE, NULL);
    hEventNewImage = CreateEvent(NULL, TRUE, FALSE, NULL);
    // Make a list of events to be used in WaitForMultipleObjects

    EventList[0] = hEventNewImage;
    EventList[1] = hEventFreeze;

    //# ifdef SDK_1_2_5
# if defined(SDK_1_2_5) || defined(SDK_1_3_0)
    // Create the condition used for signalling the new image event
    iResult = J_Event_CreateCondition(&m_hCondition);

// Register the event with the acquisition engine
    iResult = J_DataStream_RegisterEvent(hDS, EVENT_NEW_BUFFER, m_hCondition, &hEvent);

    if (iResult != J_ST_SUCCESS)
    {
        my_set_window_title("Register Failed");
        return ;
    }

# endif

# ifdef SDK_1_2_1

    // Register the event with the acquisition engine
    if (J_DataStream_RegisterEvent(hDS, EVENT_NEW_BUFFER, hEventNewImage, &hEvent) !=  J_ST_SUCCESS)
    {
        my_set_window_title("Register Failed");
        return ;
    }

# endif

// Start Node Acquisition
    retval = J_Camera_GetNodeByName(hCamera, (int8_t *)"AcquisitionStart", &hNode);
    retval = J_Node_ExecuteCommand(hNode);

    // Start image acquisition
# ifdef SDK_1_2_1
    iResult = J_DataStream_StartAcquisition(hDS, ACQ_START_FLAGS_NONE, 0xFFFFFFFFFFFFFFFFLL);
# endif
    //# ifdef SDK_1_2_5
# if defined(SDK_1_2_5) || defined(SDK_1_3_0)
    iResult = J_DataStream_StartAcquisition(hDS, ACQ_START_NEXT_IMAGE, 0);
# endif

    if (iResult != J_ST_SUCCESS)
    {
        my_set_window_title("StartAcquisition Failed");
        return ;
    }

# ifdef SDK_1_2_1
    Sleep(5000);  // we wait to allow frequency stabilization
# endif
    J_DataStream_GetBufferID(hDS, 0, &iOrigin);

    while (bEnableThread)
    {
# ifdef SDK_1_2_1
        // Wait for New Buffer event (or Freeze event)
        iWaitResult = WaitForMultipleObjects(2, EventList, FALSE/*TRUE*/, 10000/*00*/);

        // Did we get a new buffer event?
        if (WAIT_OBJECT_0 == iWaitResult)
        {
            // Get the Buffer Handle from the event
            // Request ownership of the critical section.
            //EnterCriticalSection(&CriticalSection);
            // Access the shared resource.
            //Delay_counter++;
            // Release ownership of the critical section.
            //LeaveCriticalSection(&CriticalSection);
            iSize = sizeof(void *);
# endif
            //# ifdef SDK_1_2_5
# if defined(SDK_1_2_5) || defined(SDK_1_3_0)
            // Wait for Buffer event (or kill event)
            iResult = J_Event_WaitForCondition(m_hCondition, 10000, &WaitResult);

            // Did we get a new buffer event?
            if (WaitResult == J_COND_WAIT_SIGNAL)
            {
                // Get the Buffer Handle from the event
                iSize = (uint32_t)sizeof(void *);
# endif
                J_Event_GetData(hEvent, &iBufferID,  &iSize);
                my_index = (int)iBufferID;

                for (fr_delivered = 0; fr_delivered < iImages_in_JAI_Buffer; fr_delivered++)
                    if (my_index == (int)JAI_Im_buffer_ID[fr_delivered]) break;

                if (fr_delivered == iImages_in_JAI_Buffer)   my_set_window_title("out of range");

                for (i_todo = 0, j = prev_fr_delivered; j != fr_delivered; i_todo++)
                {
                    j++;
                    j = (j >= iImages_in_JAI_Buffer) ? 0 : j;
                }

                iSize = sizeof(uint64_t);
                J_DataStream_GetStreamInfo(hDS, STREAM_INFO_CMD_NUMBER_OF_FRAMES_DELIVERED , &lNumberFramesDelivered,  &iSize);

                if (first)
                {
                    //lNumberFramestreated = lNumberFramesDelivered;
                    lNumberFramestreated = 0;
                    prev_fr_delivered = (fr_delivered + iImages_in_JAI_Buffer - 1) % iImages_in_JAI_Buffer;
                }

                if (i_todo > 1)
                    my_set_window_title("Missed fr %d at %d", i, (int)lNumberFramesDelivered);

                k = (int)lNumberFramesDelivered % iImages_in_JAI_Buffer;

                for (i = 0, j = fr_delivered; j != k; i++)
                {
                    j++;
                    j = (j >= iImages_in_JAI_Buffer) ? 0 : j;
                }

                //my_set_window_title("delivered fr %d", i);
                //attention ceci etant dans le thread l'image peut potentiellement avoir change
                //TreatementStruct.lTimeStamp = my_ulclock();//tAqImageInfo.iTimeStamp ;
                //TreatementStruct.lNumberImageDelivered = lNumberFramesDelivered ;

                //local_NumberFramesDelivered = lNumberFramesDelivered;
                p = &bid;

                if (p == NULL)
                {
                    win_printf("MERDE");
                    return;
                }

                //lNumberFramesDelivered = pTreatement_Struct->lNumberImageDelivered ;
                if (p->first_im == 0) p->first_im = (int) lNumberFramesDelivered;

                //lTimeStamp = TreatementStruct.lTimeStamp;
                t = my_ulclock();// lTimeStamp
                dt = my_uclock();

                p->previous_in_time_fr = t = my_ulclock();//modifier
                p->timer_dt = 0;//idem
                p->previous_fr_nb = (int)lNumberFramesDelivered;
                j = prev_fr_delivered + 1;
                j = (j >= iImages_in_JAI_Buffer) ? 0 : j;

                for (i = 0; i < i_todo ; im_n++, i++, lNumberFramestreated++)
                {
                    switch_frame(oi_TRACK, j);
                    t = my_ulclock();// lTimeStamp
                    dt = my_uclock();
                    p->previous_fr_nb = im_n = (int)lNumberFramestreated;
                    J_DataStream_GetBufferInfo(hDS, JAI_Im_buffer_ID[j], BUFFER_INFO_NUMBER, &b_i_n, &pSize);
                    J_DataStream_GetBufferInfo(hDS, JAI_Im_buffer_ID[j], BUFFER_INFO_TIMESTAMP, &llt, &pSize);
                    // frequency at 50 MHz

                    //BUFFER_INFO_BASE  Base address of delivered buffer (void *).
                    //BUFFER_INFO_SIZE  Size in Bytes ( size_t ).
                    //BUFFER_INFO_USER_PTR  Private Pointer ( void * ).
                    //BUFFER_INFO_TIMESTAMP  Timestamp (uint64_t).
                    //BUFFER_INFO_NUMBER  Buffer Number as announced (uint64_t).
                    //BUFFER_INFO_NEW_DATA  Flag if Buffer contains new data since it was queued.

                    //my_set_window_title("im diff %d %d", (int)lNumberFramestreated , (int)b_i_n);
                    p->image_per_in_ms = 1000 * (double)(llt - lTimeStamp) / timestampsfreq;
                    Pico_param.camera_param.camera_frequency_in_Hz = (float)1000 / p->image_per_in_ms;

                    //my_set_window_title("im diff %g camera frequency  %g Hz dt %d", p->image_per_in_ms,Pico_param.camera_param.camera_frequency_in_Hz,(int)(llt -lTimeStamp));//lNumberFramestreated , (int)b_i_n);
                    if (first)
                    {
                        first = 0;
                        jai_origin = llt;
                        //ulclock_ori = t;
                        p->min_delay_camera_thread_in_ms = (1000 * (double)t / get_my_ulclocks_per_sec())
                                                           - 1000 * (double)(llt - jai_origin) / timestampsfreq;
                    }
                    else
                    {
                        min_delay_camera_thread_in_ms = (1000 * (double)t / get_my_ulclocks_per_sec())
                                                        - 1000 * (double)(llt - jai_origin) / timestampsfreq;

                        if (min_delay_camera_thread_in_ms < p->min_delay_camera_thread_in_ms)
                            p->min_delay_camera_thread_in_ms = min_delay_camera_thread_in_ms;
                    }

                    //my_set_window_title("min delay %6.3f %6.3f", p->min_delay_camera_thread_in_ms, min_delay_camera_thread_in_ms);
                    for (p->timer_do(imr_TRACK, oi_TRACK, im_n, d_TRACK, t , dt , p->param, i_todo - i - 1) ;
                            p->timer_do != NULL && p->next != NULL; p = p->next)
                    {
                        p->timer_do(imr_TRACK, oi_TRACK, im_n, d_TRACK, t, dt, p->param,  i_todo - i - 1);
                    }

                    J_DataStream_QueueBuffer(hDS, JAI_Im_buffer_ID[j]);
                    j++;
                    j = (j >= iImages_in_JAI_Buffer) ? 0 : j;
                    lTimeStamp = llt;//my_ulclock();
                }

                //      PulseEvent(hReadyEvent);

                prev_fr_delivered = fr_delivered;
                //# ifdef SDK_1_2_5
# if defined(SDK_1_2_5) || defined(SDK_1_3_0)
                // Get # of frames awaiting delivery
                iSize = sizeof(uint64_t);
                iResult = J_DataStream_GetStreamInfo(hDS, STREAM_INFO_CMD_NUMBER_OF_FRAMES_AWAIT_DELIVERY, &iQueued, &iSize);

                if ((iResult == J_ST_SUCCESS) && (iQueued > 0))
                    J_Event_SignalCondition(m_hCondition);

# endif


                //if (j == 64) Sleep(50); test of overrun
            }
            else
            {
# ifdef SDK_1_2_1

                switch (iWaitResult)
# endif
                    //# ifdef SDK_1_2_5
# if defined(SDK_1_2_5) || defined(SDK_1_3_0)
                    switch (WaitResult)
# endif
                    {
                    // Freeze event
                    case    WAIT_OBJECT_0 + 1:
                        iResult = 1;
                        bEnableThread = FALSE;
                        break;

                    // Timeout
                    case    WAIT_TIMEOUT:
                        iResult = 2;
                        win_printf("10s Timeout");
                        bEnableThread = FALSE;
                        break;

                    // Unknown?
                    default:
                        iResult = 3;
                        break;
                    }
            }
        }

        my_set_window_title("Out loop");

// Release resources used by the critical section object.
        //DeleteCriticalSection(&CriticalSection);

        // Unregister new buffer event with acquisition engine
        J_DataStream_UnRegisterEvent(hDS, EVENT_NEW_BUFFER);

        // Free the event object
        if (hEvent != NULL)
        {
            J_Event_Close(hEvent);
            hEvent = NULL;
        }

        // Start Acquision
        retval = J_Camera_GetNodeByName(hCamera, (int8_t *)"AcquisitionStop", &hNode);
        retval = J_Node_ExecuteCommand(hNode);
        // Terminate the thread.
        Terminate_JAI_Thread();

        // Free the kill event
        if (hEventFreeze != NULL)
        {
            CloseHandle(hEventFreeze);
            hEventFreeze = NULL;
        }

        // Free the new image event object
        if (hEventNewImage != NULL)
        {
            CloseHandle(hEventNewImage);
            hEventNewImage = NULL;
        }

        WaitForThreadToTerminate();
        my_set_window_title("Out thread");
    }

    BOOL TerminateStreamThread(void)
    {
        // Is the data stream opened?
        if (hDS == NULL)        return FALSE;

        // Reset the thread execution flag.
        bEnableThread = FALSE;
        // Signal the image thread to stop faster
        SetEvent(hEventFreeze);
        // Stop the image acquisition engine
        J_DataStream_StopAcquisition(hDS, ACQ_STOP_FLAG_KILL);
        // Wait for the thread to end
        WaitForThreadToTerminate();
        return TRUE;
    }

//==============================================================////
// Terminate image acquisition thread
//==============================================================////
    void Terminate_JAI_Thread(void)
    {
        SetEvent(hStreamEvent);
        bEnableThread = FALSE;
    }

//==============================================================////
// Wait for thread to terminate
//==============================================================////

    void WaitForThreadToTerminate(void)
    {
        WaitForSingleObject(hStreamEvent, INFINITE);
        // Close the thread handle and stream event handle
        CloseThreadHandle();
    }

//==============================================================////
// Close handles and stream
//==============================================================////
    void CloseThreadHandle(void)
    {
        if (hStreamThread)
        {
            CloseHandle(hStreamThread);
            hStreamThread = NULL;
        }

        if (hStreamEvent)
        {
            CloseHandle(hStreamEvent);
            hStreamEvent = NULL;
        }
    }
////////////////////////////////////////////////////////////////////

    int add_oi_JAI_2_imr(imreg * imr, O_i * oi)
    {
        NODE_HANDLE     hNode;
        int64_t         int64Val;
        int iNx = -1, iNy = -1, iType, i, len;
        //double dVal = 0;
        char ValueStr[512];
        uint32_t pSize = 512;
        float fac = 0;

        //win_printf ("IN");
        if (imr == NULL)
        {
            win_printf("IMR NULL");
            return 1;
        }

        /*   if (oi == NULL) */
        // Get Width from the camera
        if (J_Camera_GetNodeByName(hCamera, (int8_t *)"Width", &hNode) != J_ST_SUCCESS)   return 2;

        J_Node_GetValueInt64(hNode, FALSE, &int64Val);
        iNx = (int)int64Val;     // Set window size x
        Pico_param.camera_param.nb_pxl_x = iNx;
        // win_printf("nx %d \n ny %d",iNx,iNy);


        // Get Height from the camera
        if (J_Camera_GetNodeByName(hCamera, (int8_t *)"Height", &hNode) != J_ST_SUCCESS)  return 3;

        J_Node_GetValueInt64(hNode, FALSE, &int64Val);
        iNy = (int)int64Val;     // Set window size y
        Pico_param.camera_param.nb_pxl_y = iNy;
        //win_printf("nx %d \n ny %d",Pico_param.camera_param.nb_pxl_x,Pico_param.camera_param.nb_pxl_y);

        // Get Format from the camera
        if (J_Camera_GetNodeByName(hCamera, (int8_t *)"PixelFormat", &hNode) != J_ST_SUCCESS)  return 4;

        J_Node_GetValueInt64(hNode,   FALSE,  &int64Val);
        //iValue = int64Val;




        //      J_Node_GetEnumEntryValue  ( hNode, &iValue);
        if (int64Val == BIT_PER_PIX_8)
        {
            iType = IS_CHAR_IMAGE;
            //win_printf("8 bits image type");
            Pico_param.camera_param.nb_bits = 8;
        }
        else if (int64Val == BIT_PER_PIX_10)
        {
            iType = IS_UINT_IMAGE;
            //win_printf("10 bits image type");
            Pico_param.camera_param.nb_bits = 10;
        }
        //  else if (int64Val == BIT_PER_PIX_12)
        //{
        //  iType = IS_UINT_IMAGE;
        //  Pico_param.camera_param.nb_bits = 12;
        //}
        //Voir si cela ne marche pas! ...et voir autres cameras values only for A10!
        else iType = IS_CHAR_IMAGE;

        //win_printf("type  %d",iType);


        if (J_Camera_GetNodeByName(hCamera, (int8_t *)"GevTimestampTickFrequency", &hNode) == J_ST_SUCCESS)
        {
            J_Node_GetValueInt64(hNode,   FALSE,  &int64Val);
            timestampsfreq = int64Val;
            //win_printf("Tick camera freq %d",timestampsfreq);
        }

        if (strstr(Pico_param.camera_param.camera_model, "CV-A10GE") != NULL)
            timestampsfreq = 50000000;



        if (J_Camera_GetNodeByName(hCamera, (int8_t *)"AcquisitionFrameRateRaw", &hNode) != J_ST_SUCCESS)
        {
            my_set_window_title("cannot get AcquisitionFrameRate");
        }
        else
        {
            J_Node_GetValueString(hNode, FALSE, (int8_t *)ValueStr, &pSize);

            if (sscanf(ValueStr, "fps%f", &fac) == 1)
            {
                //win_printf("AcquisitionFrameRate  %g Hz",fac);
                Pico_param.camera_param.camera_frequency_in_Hz = fac;
            }
        }

        //create JAI image
        //  win_printf("bef create  iNx %d, iNy %d,\n iType %d, iImages_in_JAI_Buffer%d",
        //     iNx, iNy, iType, iImages_in_JAI_Buffer);
        oi_JAI = create_and_attach_movie_to_imr(imr, iNx, iNy, iType, iImages_in_JAI_Buffer);

        if (oi_JAI == NULL)
        {
            win_printf("Error in allocate oi_JAI");
            return 5;
        }

        i = (Pico_param.camera_param.nb_bits + 7) / 8;
        len = i * iNx * iNy * iImages_in_JAI_Buffer;
        LOCK_DATA(oi_JAI->im.mem[0], len);
        //win_printf("after create");
        oi_JAI->width  = (float)iNx / 512;
        oi_JAI->height = (float)iNy / 512;
        //iOpen_JAI_DataStream_for_XVIN(oi_JAI);
        oi_JAI->iopt2 &= ~X_NUM;
        oi_JAI->iopt2 &= ~Y_NUM;

        if (Pico_param.camera_param.nb_bits == 12)
        {
            set_zmin_zmax_values(oi_JAI, 0, 4096);
            set_z_black_z_white_values(oi_JAI, 0, 4096);
        }
        else if (Pico_param.camera_param.nb_bits == 10)
        {
            set_zmin_zmax_values(oi_JAI, 0, 1024);
            set_z_black_z_white_values(oi_JAI, 0, 1024);
        }
        else
        {
            set_zmin_zmax_values(oi_JAI, 0, 255);
            set_z_black_z_white_values(oi_JAI, 0, 255);
        }

        write_Pico_config_file();
        Close_Pico_config_file();
        ask_to_update_menu();
        return 0;
    }
    /*
    int JAI_set_gain()
    {
    retval = J_Camera_Open(hFactory, sCameraId, &hCamera);
    if (retval == J_ST_SUCCESS)
    {
    // Get the node handle
    retval = J_Camera_GetNodeByName(hCamera, "GainRaw", &hNode);
    if (retval == J_ST_SUCCESS)
    {
    // Set the value
    retval = J_Node_SetValueInt64(hNode, false, 100);
    }
    // Close the camera again
    retval = J_Camera_Close(hCamera);
    }
    }
    */

    int initialize_JAI(void)
    {
        int ret;

        // Open the Factory
        dump_to_error_file_only("bef factory");
        my_set_window_title("Openning JAI stream factory");

        if ((ret = iOpen_Factory())) return ret + 0x10;

        dump_to_error_file_only("bef hcam");
        my_set_window_title("Openning JAI camera");

        if (hCam_Open_camera() == NULL) return 0x20;

        dump_to_error_file_only("after hcam");
        my_set_window_title("Camera %s found seting image ", Pico_param.camera_param.camera_model);

        if ((ret = add_oi_JAI_2_imr(imr_JAI, NULL))) return ret + 0x40;

        dump_to_error_file_only("after add");
        my_set_window_title("Starting JAI camera thread");

        if ((ret = iOpen_JAI_DataStream_and_set_buffers_for_XVIN(oi_JAI, hCamera))) return ret + 0x80;

        dump_to_error_file_only("after iopen");
        return 0;

    }


    int freeze_video(void)
    {
        imreg *imr;

        if (updating_menu_state != 0)  return D_O_K;

        allegro_display_on = FALSE;

        TerminateStreamThread();


        imr = find_imr_in_current_dialog(NULL);
        imr->one_i->need_to_refresh &= BITMAP_NEED_REFRESH;
        find_zmin_zmax(imr->one_i);
        refresh_image(imr, UNCHANGED);
        return 0;
    }


    int freeze_source(void)
    {
        if (updating_menu_state != 0)  return D_O_K;

        freeze_video();
        return 0;
    }

    int JAI_live(void)
    {
        //int is_live = 0;
        if (updating_menu_state != 0)  return D_O_K;

        CreateStreamThread();

        //is_live  = 1;
        allegro_display_on = TRUE;
        //create_display_thread();
        return D_O_K;//live_video(-1);
    }


///////////////////BOF la suite////////////////////
    int kill_source(void)
    {
        imreg *imr;

        if (ac_grep(cur_ac_reg, "%im", &imr) != 1)
        {
            if (active_menu) active_menu->flags |= D_DISABLED;

            return D_O_K;
        }
        //else if (go_track == TRACK_ON) active_menu->flags |=  D_DISABLED;
        else
        {
            if (active_menu) active_menu->flags &= ~D_DISABLED;
        }

        if (updating_menu_state != 0)  return D_O_K;

        go_track = TRACK_STOP;
        return D_O_K;
    }

    int live_source(void)
    {
        if (updating_menu_state != 0)  return D_O_K;

        //is_live  = 1;
        return JAI_live();
    }


    int start_data_movie(imreg * imr)
    {
        return 0;
    }


    DWORD WINAPI TrackingThreadProc(LPVOID lpParam)
    {
        //tid *ltid;

        if (imr_TRACK == NULL || oi_TRACK == NULL)    return 1;

        //ltid = (tid*)lpParam;
        live_source();
        return 0;
    }


    MENU *JAI_source_image_menu(void)
    {
        static MENU mn[32];

        if (mn[0].text != NULL)   return mn;

        add_item_to_menu(mn, "Live JAI", JAI_live , NULL, 0, NULL);
        add_item_to_menu(mn, "Freeze JAI", freeze_video , NULL, 0, NULL);
        add_item_to_menu(mn, "Change Camera gain", change_gain , NULL, 0, NULL);
        add_item_to_menu(mn, "Change shutter", get_JAI_shutter_mode , NULL, 0, NULL);
        add_item_to_menu(mn, "Change shutter reg", get_JAI_shutter_reg_mode , NULL, 0, NULL);

        return mn;

    }


    int init_image_source(imreg * imr, int mode)
    {
        int ret;
        O_i *oi = NULL;

        //if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
        if (imr == NULL)    imr = create_and_register_new_image_project(0,   32,  900,  668);

        if (imr == NULL)    return win_printf_OK("Patate t'es mal barre!");

        imr_JAI = imr;
        oi = imr->one_i;

        if ((ret = initialize_JAI()))
        {
            win_printf_OK("Your Giga Ethernet video camera did not start!");
            return ret; // Initializes JAI
        }

        if (mode == 1)      remove_from_image(imr, oi->im.data_type, (void *)oi);

        add_image_treat_menu_item("JAI Genicam", NULL, JAI_source_image_menu(), 0, NULL);
        get_camera_shutter_in_us = _get_camera_shutter_in_us;
        set_camera_shutter_in_us = _set_camera_shutter_in_us;
        get_camera_gain = _get_camera_gain;
        set_camera_gain = _set_camera_gain;

        return 0;
    }

    int JAI_source_main(void)
    {
        imreg *imr = NULL;
        O_i *oi = NULL;



        imr = find_imr_in_current_dialog(NULL);

        if (imr == NULL)    init_image_source(NULL, 0);
        else
        {
            if (ac_grep(cur_ac_reg, "%im%oi", &imr, &oi) != 2)
                init_image_source(imr, 0);

            if (oi->im.movie_on_disk == 0 || oi->im.n_f < 2)
                init_image_source(imr, 0);

        }

        if (ac_grep(cur_ac_reg, "%im%oi", &imr, &oi) != 2)
            return win_printf_OK("You must load a movie in the active window");

        d_JAI = find_dialog_associated_to_imr(imr_JAI, NULL);
        before_menu_proc = JAI_before_menu;
        after_menu_proc = JAI_after_menu;
        oi_JAI->oi_got_mouse = JAI_oi_got_mouse;
        //  oi_JAI->oi_idle_action = JAI_oi_idle_action;
        oi_JAI->oi_mouse_action = JAI_oi_mouse_action;

        general_end_action = source_end_action;
        //  general_idle_action = source_idle_action;

        add_image_treat_menu_item("JAI Genicom", NULL, JAI_source_image_menu(), 0, NULL);   // JAI user menus

        return D_O_K;
    }

    /** Unload function for the  JAI.c menu.*/
    int JAI_unload(void)
    {
        remove_item_to_menu(image_treat_menu, "JAI Genicam", NULL, NULL);
        return D_O_K;
    }



#endif

/*******************************************************************************/
