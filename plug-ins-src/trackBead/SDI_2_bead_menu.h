#pragma once
# include "allegro.h"
#ifdef XV_WIN32
# include "winalleg.h"
#endif
#include "xvin.h"
#include "track_util.h"
#include "trackBead.h"
# include "action.h"

XV_FUNC(MENU* , prepare_SDI_2_bead_menu, (b_track *bt));
XV_FUNC(int, do_SDI_2_initialize_phase,(void));
// XV_FUNC(int, do_SDI_2_initialize_k_buffer,(void));
XV_FUNC(int, do_SDI_2_initialize_phase_all,(void));
XV_FUNC(int, do_SDI_2_forbid_move,(void));
XV_FUNC(int, do_SDI_2_global_maximize,(void));
XV_FUNC(int, do_SDI_2_track_mode,(void));
XV_FUNC(int,do_change_free,(void));
