
#ifndef _SDI_2__FUNCTIONS_C_
#define _SDI_2__FUNCTIONS_C_

# include "SDI_2_functions.h"
# include <stdlib.h>
# include <string.h>
# include <math.h>

static float yratio=3; //pour la sélectino en y
static float mr=2; //pour la phase (attention, au carré ici : 4->2)
static int window_type=8;
static float sdi2_hypg_sig = 40;
static float sdi2_hypg_pow = 12;
static bool move_franges=1;
static int nb_init_cycles=128;
static int useapo=1;

//static int k_averaging_frame_nb=1024;
//static int k_buffer_active=0;


int SDI_2_get_tracking_parameters( int *ext_window_type, float *ext_mr, float *ext_yratio)
{

  *ext_window_type=window_type;
  *ext_mr=mr;
  *ext_yratio=yratio;
  return 0;
}

int SDI_2_initialize_phase(b_track *bt)
{
  if (SDI_2_initialize_phase_fringe(bt,bt->frange_top)) return 1;;
  if (SDI_2_initialize_phase_fringe(bt,bt->frange_bottom)) return 1;

  return 0;
}

int SDI_2_initialize_phase_fringe(b_track *bt,sdi_fr *f1)
{
  if (f1==NULL) return 1;

  f1->r0=0;
  f1->phi0=0;
  f1->init_cycles=0;
  f1->yg=0;
  f1->yd=0;
  f1->buffer_full = 0;
  f1->c_buff = 0;
  f1->yslope = 0;



  return 0;
}


// int SDI_2_initialize_k_buffer_fringe(sdi_fr *f1)
// {
//   int k;
//   if (f1==NULL) return 1;
//
//   f1->c_k=0;
//   f1->n_k=0;
//   f1->sum_k=0;
//
//   for (k=0;k<MAX_K_BUFFER_SIZE;k++)
//     f1->k_buffer[k]=0;
//
//
//   return 0;
// }




int SDI_2_do_set_tracking_parameters(void)
{
  int i;
  if (updating_menu_state!=0) return 0;
  i=win_scanf("Please indicate the value of the tracking parameters : \n"
              "Indicate the  ratio signal/ maximum amplitude to take in account y -lines : %f\n"
              "Indicate the  ratio signal/ maximum amplitude to take in the x-phase  : %f\n"
              "Choose the number of frames on which initialize the phase : %d \n"
              "Click to use apodisation : %b \n"
              "Click to activate frange tracking (if not, they will stay fixed) : %b\n",
              //"Click to activate k buffer : %b \n",
          &yratio,&mr,&nb_init_cycles,&useapo,&move_franges);//,&k_buffer_active);

  return 0;

}


int SDI_2_do_set_free_tracking_parameters(void)
{
  int i;
  if (updating_menu_state!=0) return 0;
  i=win_scanf("Do you want to activate free tracking ? : %b\n"
              "Find new beads between yco %d and yend %d\n"
              "Look for a window of size xw %d and yw %d\n"
              "And use a recognition threshold of %d\n"
              "Max x distance between fringes %d \n"
              "Max y distance between fringes %d \n",
              //"Click to activate k buffer : %b \n",
          &track_info->SDI_2_track_free,&track_info->track_free_y_low,&track_info->track_free_y_high,&track_info->track_free_xw,&track_info->track_free_yw,
          &track_info->track_free_lum_threshold,&track_info->track_free_window_x,&track_info->track_free_window_y);//,&k_buffer_active);

  return 0;

}


int SDI_2_do_choose_window_on_x(void)
{

int i;
if (updating_menu_state!=0) return 0;


i=win_scanf("Please choose the window that you want to appwidth to your real x-signal before smoothing\n"
"No window -> %R \n"
"Flat top window (small scalloping loss but very bad resolution)-> %r \n"
"The following have different trade-off between resolution and scalloping \n"
"Hamming window -> %r \n"
"Hann window -> %r \n"
"Blackman window -> %r \n"
"Blackman-Harris window -> %r \n"
"Blackman-Nuttal window -> %r \n"
"Nuttal window (continuous derivative of the window)->%r \n"
"Hypergaussian -> %r, sigma ->%6f, pow ->%6f \n"
,&window_type,&sdi2_hypg_sig,&sdi2_hypg_pow);

return 0;

}


int SDI_2_update_frange_position(b_track *bt,O_i *oic,int length, int width, sdi_fr *f1)

{

  int yg,yd,ymax=0;
  float ymax_real;
  float dummy;
  float dummy2;
  double pos_x;
  double Max_pos=0;
  double dummyd = 0;
  int i=0;
  double low,high;
  double k=0,phif=0;
  double k_av=0;
  int yco,yend,xco,xend;
  int onx = oic->im.nx;
  int ony = oic->im.ny;
  int cw2 = width >> 1; //divide by 2
  int cl2 = length >> 1; // divide by 2

  float total_amplitude = 0;
  float slope = 0;
  double center = 0;
  float T =0;



  int ifxmax,ifxmin,ifymax,ifymin;

  ifxmin = cl2;
  ifxmax = onx - cl2;

  ifymin = cw2;
  ifymax = ony - cw2;

  //adjust beginning and ending of the franges so that they don't go outside the image
  if (f1->ifx < ifxmin) f1->ifx = ifxmin;
  if (f1->ifx > ifxmax) f1->ifx = ifxmax;

  if (f1->ify < ifymin) f1->ify = ifymin;
  if (f1->ify > ifymax) f1->ify = ifymax;

  yco = f1->ify-cw2;
  yend =  f1->ify+cw2-1 ;
  xco = f1->ifx-cl2;
  xend = f1->ifx+cl2-1;

  if (xco>=xend || xco>=onx || xend <=0) return 1;
  if (yco>=yend || yco>=ony || yend <=0) return 1;




  if (SDI_2_fill_x_av_y_profile_from_im(oic, xco,  xend,  yco,  yend,  f1->yi,&ymax,&yg,&yd)==1) return 1;

  // for (int iii = 0; iii < width; iii++)
  //   f1->iyi[iii] = (int)(f1->yi[iii] * wx);


  // if (f1->init_cycles<nb_init_cycles)
  // {
  //   f1->yg +=  yg;
  //   f1->yd +=  yd;
  //   }
  //
  // yg = (int) (0.5+((float) (f1->yg))/(f1->init_cycles+1));
  // yd = (int) (0.5+((float) (f1->yd))/(f1->init_cycles+1));
  //
  // yg=3;
  // yd=25;


  if (SDI_2_fill_y_av_x_profile_from_im(oic, xco,  xend,yco,  yend,  f1->xi,f1->fit_max_coeffs,&Max_pos)==1) return 1;
  // wx = xend-xco+1;
  // //wy = yd-yg+1;
  // for (int iii = 0; iii < length; iii++)
  //   f1->ixi[iii] = (int)(f1->xi[iii] * wy);


  if (bt->do_minimize == 1)
  {
    if (f1->buffer_full == 0)
    {
      SDI_2_minimize_distance_global(bt,f1,&pos_x);
      f1->buffer_full = 1;
    }

    else
    {
      center = f1->fx - f1->ifx;
      T = 2*M_PI/(bt->om_cos);
      SDI_2_minimize_distance_local(bt,f1, &pos_x, &dummyd ,center-T/2,center+T/2);
      //SDI_2_minimize_distance_global(bt,f1,&pos_x);

    }


  }

  else if (bt->do_minimize == 2)
  {
    SDI_2_find_x_from_centro(oic,xco,xend,yco,yend,f1->yslope,&pos_x);

  }



  //filter_x_profile(f1->xi);
  //find_position_from_filtered_x_profile(&pos_x);
  //if (k_buffer_active) k_av=(f1->n_k == 0 ) ? 0 : f1->sum_k/f1->n_k;

  else if (bt->im_buffer_size == 0) //old
  {
    SDI_2_window_profile(f1->xi,length);

    SDI_2_vincent_displacement_function(f1->xi,length,bt->filter_low, bt->filter_high,k_av,f1->fp,&phif,&k,&Max_pos,f1->dsfit,f1->xibp,f1->xf);

    f1->quality = 0;

    for (i= bt->filter_low; i <= bt->filter_high  && i < length/2-1;i++) f1->quality += f1 -> xf[i];

    total_amplitude = 0 ;

    for (i= 0; i <= length/2 - 1; i++) total_amplitude += f1 -> xf[i];

    f1 -> quality = f1 -> quality / total_amplitude;


    if (f1->quality > f1->quality_max) f1->quality_max = f1->quality;


    f1->max_pos = Max_pos + f1->ifx - length/2;
//   if (k_buffer_active)
//   {
//   k=fabs(k);
//   f1->k_buffer[f1->c_k] = k;
//   f1->sum_k += k;  // we add the new k
//   f1->sum_k-=f1->k_buffer[(f1->c_k+1)%k_averaging_frame_nb]; //we remove the oldest one
//   f1->c_k=(f1->c_k+1)%k_averaging_frame_nb; // we roll the index
//   f1->n_k=((f1->n_k+1)>k_averaging_frame_nb) ? k_averaging_frame_nb : f1->n_k+1; // we update n_k (for the beginning);
// }
    k_av=fabs(k);

  if (f1->init_cycles<nb_init_cycles) //initiate phi_0
    {
      f1->r0+=(Max_pos-phif/k_av)/nb_init_cycles;
      f1->init_cycles+=1;
      return 0;
    }

  //here we correct phase jumps
  pos_x= f1->r0 + phif/k_av;
  low=Max_pos-M_PI/k_av;
  high=Max_pos+M_PI/k_av;
  while (pos_x>high)
  pos_x-=2*M_PI/k_av;
  while (pos_x<low)
  pos_x+=2*M_PI/k_av;
}
else if (bt->mode_avg_fix==0) //averaging profile in live (add temporal drifts in case of jump)
 {
   SDI_2_window_profile(f1->xi,length);



   if (f1->init_cycles < nb_init_cycles)
   {
     SDI_2_real_max_pos(f1->xi,length, bt->filter_low, bt->filter_high, f1->fp,&Max_pos);
     f1->r0 += Max_pos / nb_init_cycles;
     f1->init_cycles += 1;
     return 0;
   }

   else if (f1->init_cycles == nb_init_cycles)
   {
       // int ifx_d = f1 ->ifx;
     SDI_2_fill_amplitude_phase(f1->xi,bt->filter_low,f1->fp,bt->filter_high,f1->fpb[f1->c_buff],length,f1->xf,f1->xibp);
     SDI_2_project_profile_on_profile(oic,xco,xend,yco,yend,f1->xibp,f1->yi);
     find_max_around(f1->yi,width,ymax,&ymax_real,&dummy, &dummy2);

     f1->ify=(int)( 0.5+ f1->ify - width/2 + ymax_real);
     f1->ifx=(int)(0.5+ f1->ifx + f1->r0 - length/2);

     if (f1->ifx < ifxmin) f1->ifx = ifxmin;
     if (f1->ifx > ifxmax) f1->ifx = ifxmax;

     if (f1->ify < ifymin) f1->ify = ifymin;
     if (f1->ify > ifymax) f1->ify = ifymax;

     f1->init_cycles += 1;
     f1->r0=0;

     return 0;

   }


   if (f1->buffer_full == 0)
   {
     SDI_2_fill_amplitude_phase(f1->xi,bt->filter_low,f1->fp,bt->filter_high,f1->fpb[f1->c_buff],length,f1->xf,f1->xibp);
     if (f1->c_buff != 0) SDI_2_correct_phase(f1->fpb[f1->c_buff - 1],f1->fpb[f1->c_buff],bt->filter_high-bt->filter_low+1,bt->filter_low,bt->fixed_mode);

     f1->c_buff += 1;

     if (f1 -> c_buff == bt -> im_buffer_size )

     {

       f1 -> c_buff = 0 ;
       f1 -> buffer_full = 1 ;
       SDI_2_initiate_matrix(bt,f1) ;

    }

     return 0;
   }

   else
    {

      SDI_2_fill_amplitude_phase(f1->xi,bt->filter_low,f1->fp,bt->filter_high,f1->fpb[f1->c_buff],length,f1->xf,f1->xibp);
      if (f1->c_buff != 0) SDI_2_correct_phase(f1->fpb[f1->c_buff-1],f1->fpb[f1->c_buff],bt->filter_high-bt->filter_low+1,bt->filter_low,bt->fixed_mode);
      else SDI_2_correct_phase(f1->fpb[bt->im_buffer_size - 1],f1->fpb[0],bt->filter_high-bt->filter_low+1,bt->filter_low,bt->fixed_mode);
      SDI_2_fit_position_from_buffer(bt,f1,&pos_x,&f1->quality);




      f1->c_buff += 1;
      if (f1 -> c_buff == bt -> im_buffer_size ) f1->c_buff = 0;

      if (f1->init_cycles <= 2*nb_init_cycles)
      {
        //SDI_2_real_max_pos(f1->xi,lx, bt->filter_low, bt->filter_high, f1->fp,&Max_pos);
        f1->r0 += (Max_pos - pos_x) / nb_init_cycles;
        f1->init_cycles += 1;
        return 0;
      }


      pos_x +=  f1->r0 ;


    }
 }


 else // now used
  {
      SDI_2_window_profile(f1->xi,length);



       if (f1->init_cycles < nb_init_cycles || ((nb_init_cycles < f1->init_cycles) && (f1->init_cycles < 2*nb_init_cycles+1))) //on calcule le maximum on se déplace et on le recalcule
       {
         //SDI_2_real_max_pos(f1->xi,lx, bt->filter_low, bt->filter_high, f1->fp,&Max_pos);
         f1->r0 += Max_pos / nb_init_cycles; //utilise le max pos fourni par par le fit des peak
         f1->init_cycles += 1;

         return 0;
       }

       else if (f1->init_cycles == nb_init_cycles || (f1->init_cycles == 1+2*nb_init_cycles)) // on se centre sur le maximum calculé plus haut
       {
           // int ifx_d = f1 ->ifx;
         SDI_2_fill_amplitude_phase(f1->xi,bt->filter_low,f1->fp,bt->filter_high,f1->fpb[f1->c_buff],length,f1->xf,f1->xibp); // n'est pas utilisé
         SDI_2_project_profile_on_profile(oic,xco,xend,yco,yend,f1->xibp,f1->yi);
         SDI_2_find_centroid(f1->yi,yend-yco+1,&ymax_real,&dummy);

         //find_max_around(f1->yi,width,ymax,&ymax_real,&dummy, &dummy2);

         f1->ify=(int)( 0.5+ f1->ify - width/2 + ymax_real); //uses fy for here
         f1->ifx=(int)(0.5+ f1->ifx + f1->r0 - length/2);  //uses fx computed above

         if (f1->ifx < ifxmin) f1->ifx = ifxmin;
         if (f1->ifx > ifxmax) f1->ifx = ifxmax;

         if (f1->ify < ifymin) f1->ify = ifymin;
         if (f1->ify > ifymax) f1->ify = ifymax;

         f1->init_cycles += 1;
         f1->r0=0;

         return 0;

       }


    if (f1->buffer_full == 0)
    {

      SDI_2_fill_amplitude_phase(f1->xi,bt->filter_low,f1->fp,bt->filter_high,f1->fpb[f1->c_buff],length,f1->xf,f1->xibp);
      if (f1->c_buff != 0) SDI_2_correct_phase(f1->fpb[f1->c_buff - 1],f1->fpb[f1->c_buff],bt->filter_high-bt->filter_low+1,bt->filter_low,bt->fixed_mode);
      else SDI_2_correct_phase_N(f1->fpb[0],bt->filter_high-bt->filter_low+1,bt->filter_low,bt->fixed_mode);

      SDI_2_find_yslope(oic,xco,xend,yco,yend,&slope,&dummy);
      f1->yslope += slope;


      f1->c_buff += 1;

      if (f1 -> c_buff == bt -> im_buffer_size )

      {

        f1 -> c_buff = 0 ;
        f1 -> buffer_full = 1 ;
        f1->yslope /= bt->im_buffer_size;

        SDI_2_find_average_profile(bt,f1) ;


     }

      return 0;
    }

    else
     {



       SDI_2_fill_phase_only(f1->xi,bt->filter_low,f1->fp,bt->filter_high,f1->fitphase->yd,length,f1->xf,f1->xibp);

       SDI_2_correct_and_find_phase(bt,f1->push,f1->avg_profile,f1->avg_profile_er,f1->fitphase->yd,bt->filter_high-bt->filter_low+1,bt->filter_low,f1->fit_phase_coeffs,f1->fit_phase_corr,&pos_x,&f1->quality);


             if (f1->init_cycles < 3*nb_init_cycles+2) // initiaise correspondance position réelle et phase
             {

               //SDI_2_real_max_pos(f1->xi,lx, bt->filter_low, bt->filter_high, f1->fp,&Max_pos);
               f1->r0 += (Max_pos - pos_x) / nb_init_cycles;
               f1->init_cycles += 1;
               return 0;

             }


             pos_x +=  f1->r0 ;




     }
  }





//  SDI_2_project_profile_on_profile(oic,xco,xend,yco,yend,f1->xibp,f1->yi);
//  SDI_2_find_centroid(f1->yi,yend-yco+1,&ymax_real);
  SDI_2_find_y(oic,xco,xend,yco,yend,f1->yslope,&ymax_real);


  //find_max_around(f1->yi,width,ymax,&ymax_real,&dummy, &dummy2);


  //update y position. The first pixel in the profil is the piwel bt->ify-width/2
  f1->fy = f1->ify - width/2 + ymax_real;
  //update x position of the cross
  if (bt->do_minimize != 1 )  f1->fx = f1->ifx + pos_x - length/2;
   else f1->fx = f1->ifx + pos_x ;

if (move_franges && bt->moving_forbidden == 0)
{

  // int ifx_d = f1 ->ifx;


  int new_ifx=(int)(0.5+ f1->ifx + Max_pos - length/2);
  if (bt->do_minimize == 1) new_ifx=(int)(0.5+ f1->ifx + pos_x);
  if (bt->do_minimize == 2) new_ifx=(int)(0.5+ f1->ifx + pos_x -length/2);

  int new_ify=(int)( 0.5+ f1->fy);

  int ecart_x = abs(new_ifx - f1->ifx);
  int ecart_y = abs(new_ify - f1->ify);


  // if ((ecart_x>=2 && ecart_x <= 7) || ecart_x>10)  f1->ifx=new_ifx;
  // if ((ecart_y>=2 && ecart_y <= 7) || ecart_y>10)   f1->ify=new_ify;

  if (track_info->SDI_2_track_free)
  {
    f1->ifx=new_ifx;
    f1->ify=new_ify;
  }

  else
  {

  if (ecart_x>2)  f1->ifx=new_ifx;
  if (ecart_y>2)   f1->ify=new_ify;
}

  if (f1->ifx < ifxmin) f1->ifx = ifxmin;
  if (f1->ifx > ifxmax) f1->ifx = ifxmax;

  if (f1->ify < ifymin) f1->ify = ifymin;
  if (f1->ify > ifymax) f1->ify = ifymax;





  // if (f1->ifx != ifx_d)
  // {
  //   // si on se deplace corrige la phase des anciens modes
  //   ifx_d = f1->ifx - ifx_d;
  //   for (int j=0 ; j < bt->filter_high-bt->filter_low + 1 ; j++)   f1->phase_trk_correction[j] -= (2*M_PI*(double)(ifx_d)*(bt->filter_low+j)/wx);
  //   // ifx_d = f1->ifx - ifx_d;
  //   // for (i = 0; i < bt->im_buffer_size  ; i++)
  //   // {
  //   //   for (int j=0 ; j < bt->filter_high-bt->filter_low + 1 ; j++)   f1->fpb[i]->phase[j] += 2*M_PI*ifx_d*(bt->filter_low+j)/wx;
  //   //
  //   // }
  // }

}


  return 0;

}


int SDI_2_project_profile_on_profile(O_i *oic, int xco, int xend, int yco, int yend, float *xi, float *yi)
{
    int i,j;
    int lx=xend-xco+1;
    int ly=yend-yco+1;
    float temp_mr[lx];

    for (i=0;i<ly;i++)
    {
      yi[i]=0;
      extract_raw_partial_line(oic, yco+i, temp_mr,xco,lx); //take a line with constant y
      for (j=0;j<lx;j++)
      {
        yi[i]+=temp_mr[j]*xi[j];   // project it on i
      }
      yi[i] /= lx;

    }

  return 0;


}

int SDI_2_window_profile(float *x, int lx)
{
int i;
switch (window_type)

{
case SDI_FLAT_TOP_WINDOW:
  for (i=0;i<lx;i++)
  {
    x[i]=x[i]*(1-1.93*cos(2*M_PI*i/(lx-1))+1.29*cos(4*M_PI*i/(lx-1))-0.388*cos(6*M_PI*i/(lx-1))+0.028*cos(8*M_PI*i/(lx-1)));
  }
  break;

case SDI_NO_WINDOW:
  break;

case SDI_HAMMING_WINDOW:
for (i=0;i<lx;i++)
{
  x[i]=x[i]*(0.54-0.46*cos(2*M_PI*i/(lx-1)));
}
break;

case SDI_HANN_WINDOW:
for (i=0;i<lx;i++)
{
  x[i]=x[i]*(0.5-0.5*cos(2*M_PI*i/(lx-1)));
}
break;


case SDI_BLACKMAN_WINDOW:
for (i=0;i<lx;i++)
{
  x[i]=x[i]*(0.42659-0.49656*cos(2*M_PI*i/(lx-1))+0.076849*cos(4*M_PI*i/(lx-1)));
}
break;

case SDI_BLACKMAN_HARRIS:
for (i=0;i<lx;i++)
{
  x[i]=x[i]*(0.35875-0.48829*cos(2*M_PI*i/(lx-1))+0.14128*cos(4*M_PI*i/(lx-1))-0.01168*cos(6*M_PI*i/(lx-1)));
}
break;

case SDI_BLACKMAN_NUTTALL:
for (i=0;i<lx;i++)
{
  x[i]=x[i]*(0.3635819-0.4891775*cos(2*M_PI*i/(lx-1))+0.1365995*cos(4*M_PI*i/(lx-1))-0.0106411*cos(6*M_PI*i/(lx-1)));
}
break;

case SDI_NUTTALL:
for (i=0;i<lx;i++)
{
  x[i]=x[i]*(0.355768-0.487396*cos(2*M_PI*i/(lx-1))+0.144232*cos(4*M_PI*i/(lx-1))-0.012604*cos(6*M_PI*i/(lx-1)));
}
break;

case SDI_HYPERGAUSSIAN:
for (i=0;i<lx;i++)
{
  x[i]=x[i]*exp(-pow((i-lx/2)/sdi2_hypg_sig,sdi2_hypg_pow));
}
break;


}


return 0;

}


int SDI_2_sum_image_on_area_and_send_barycenter(O_i *oi, int xco, int xend, int yco, int yend, float *x_bar, float *y_bar, float *integrated_pix, int only_sum, float *max)
{


    if (xco < 0 ) xco = 0;
    if (yco < 0 ) yco = 0;
    if (xend >= oi->im.nx) xend = oi->im.nx -1;
    if (yend >= oi->im.ny) yend = oi->im.ny -1;
    if (xend <= xco ) return 1;
    if (yend <= yco ) return 1;

  int j, i;
  int ly=yend-yco+1;
  int lx=xend-xco+1;

  float ymax=0;
  union pix *pd=NULL;
  char a;



  pd  = oi->im.pixel;


  if (oi == NULL  )   return 1;



  *integrated_pix = 0;
  *x_bar += 0;
  *y_bar += 0;
  *max = 0;

  if (oi->im.data_type == IS_CHAR_IMAGE)
  {

      for (i = 0; i < ly; i++)
      {
          for (j = 0; j < lx; j++)
          {
              a = (float) pd[yco+i].ch[xco+j];
              if (a > *max) *max = a;
              *integrated_pix += a;
              if (only_sum == 0)
              {
              *x_bar += (xco+j) * a;
              *y_bar += (yco+i) * a;

            }
      }
    }
      if (only_sum == 0)
      {
      *x_bar /= *integrated_pix;
      *y_bar /= *integrated_pix;
    }

  }

  else return(win_printf_OK("bad type image, free tracking is for 8 bits so far \n"));

  *integrated_pix /= ly * lx;

  return 0;

}




int SDI_2_fill_x_av_y_profile_from_im(O_i *oi, int xco, int xend, int yco, int yend, float *y, int *ym,int *yg, int *yd)

{
    int j, i;
    int ly=yend-yco+1;
    int lx=xend-xco+1;

    float ymax=0;
    union pix *pd=NULL;


    pd  = oi->im.pixel;


    if (oi == NULL  || y == NULL)   return 1;


    for (i = 0; i < ly; i++)      y[i] = 0;

    if (oi->im.data_type == IS_CHAR_IMAGE)
    {

        for (i = 0; i < ly; i++)
        {
            for (j = 0; j < lx; j++)
                y[i] += (float) pd[yco+i].ch[xco+j];
            y[i]=y[i]/lx;
        }



    }
    else if (oi->im.data_type == IS_INT_IMAGE)
    {
      for (i = 0; i < ly; i++)
      {
          for (j = 0; j < lx; j++)
              y[i] += (float) pd[yco+i].in[xco+j];
          y[i]=y[i]/lx;
      }


    }
    else if (oi->im.data_type == IS_UINT_IMAGE)
    {
      for (i = 0; i < ly; i++)
      {
          for (j = 0; j < lx; j++)
              y[i] += (float) pd[yco+i].ui[xco+j];
          y[i]=y[i]/lx;
      }

    }

    else return 1;

    for (i=0;i<ly;i++)
    {
      if (y[i]>ymax)
      {
        *ym=i;
        ymax=y[i];
      }
    }

    (*yg)=(*ym);
    (*yd)=(*ym);
    //find yg limit given yratio
    while ( (y[*yg]>ymax/yratio) && (*yg>0))
    {
      *yg-=1;
    }

    //find yd limit given y ratio
    while ( (y[*yd]>ymax/yratio) && (*yd<ly-1))
    {
      *yd+=1;
    }

    return 0;
}

int SDI_2_fill_y_av_x_profile_from_im(O_i *oi, int xco, int xend, int yco, int yend, float *x, double *fit_param,double *xm)

{
    int j, i;
    int ly=yend-yco+1;
    int lx=xend-xco+1;
    double *a = NULL;


    union pix *pd=NULL;


    pd  = oi->im.pixel;


    if (oi == NULL  || x == NULL)   return 1;


    for (i = 0; i < lx; i++)      x[i] = 0;

    if (oi->im.data_type == IS_CHAR_IMAGE)
    {

        for (i = 0; i < lx; i++)
        {
            for (j = 0; j < ly; j++)
                x[i] += (float) pd[yco+j].ch[xco+i];
            x[i]=x[i]/ly;
        }



    }
    else if (oi->im.data_type == IS_INT_IMAGE)
    {

              for (i = 0; i < lx; i++)
              {
                  for (j = 0; j < ly; j++)
                      x[i] += (float) pd[yco+j].in[xco+i];
                  x[i]=x[i]/ly;
              }



    }
    else if (oi->im.data_type == IS_UINT_IMAGE)
    {

              for (i = 0; i < lx; i++)
              {
                  for (j = 0; j < ly; j++)
                      x[i] += (float) pd[yco+j].ui[xco+i];
                  x[i]=x[i]/ly;
              }


    }
    else return 1;




    d_s *ds = build_data_set(5,5);
    ds->nx=ds->ny=5;

    for (i=lx/2-10;i<lx/2+10;i++)
    {
      if (x[i]>ds->yd[2])
      {
        ds->xd[2]=(double)i;
        ds->yd[2]=x[i];
      }
    }

    int deb =(int) (ds->xd[2]+5);
    int fin =(int) (ds->xd[2]+15);
    ds->yd[3] = 0;
    for (i=deb;i<fin;i++)
    {
      if (x[i]>ds->yd[3])
      {
        ds->xd[3]=(double)i;
        ds->yd[3]=x[i];
      }
    }


    deb =(int) (ds->xd[3]+5);
    fin =(int) (ds->xd[3]+15);
    ds->yd[4] = 0;
    for (i=deb;i<fin;i++)
    {
      if (x[i]>ds->yd[4])
      {
        ds->xd[4]=(double)i;
        ds->yd[4]=x[i];
      }
    }

    deb =(int) (ds->xd[2]-5);
    fin =(int) (ds->xd[2]-15);
    ds->yd[1] = 0;
    for (i=deb;i>=fin;i--)
    {
      if (x[i]>ds->yd[1])
      {
        ds->xd[1]=(double)i;
        ds->yd[1]=x[i];
      }
    }

    deb =(int) (ds->xd[1]-5);
    fin =(int) (ds->xd[1]-15);
    ds->yd[0] = 0;
    for (i=deb;i>=fin;i--)
    {
      if (x[i]>ds->yd[0])
      {
        ds->xd[0]=(double)i;
        ds->yd[0]=x[i];
      }
    }


    if (fit_ds_to_xn_polynome(ds, 3, ds->xd[0]-1, ds->xd[4]+1, 0, 256, &a) < 0) *xm = ds->xd[2];
    else *xm = -a[1]/(2*a[2]);

    if (!((*xm>ds->xd[0]) && (*xm<ds->xd[4])))  *xm = ds->xd[2];
    free_data_set(ds);


    if (a!=NULL && fit_param != NULL)
    {
      fit_param[0] = a[0];
      fit_param[1] = a[1];
      fit_param[2] = a[2];
      free(a);
    }





    return 0;
}


int SDI_2_activate_tracking(void)
{
  move_franges = !move_franges;
  return 0;

}

int SDI_2_find_new_beads_in_pixel_area(O_i *oi, g_track *gt, int xco, int xend, int y_pixel_low, int  y_pixel_high, float *x_bar_max, float *y_bar_max, float *max_integrated_pix)
{


  float x_bar = 0;
  float y_bar = 0;

  if (xco < 0 ) xco = 0;
  if (y_pixel_low < 0 ) y_pixel_low = 0;
  if (xend >= oi->im.nx) xend = oi->im.nx -1;
  if (y_pixel_high >= oi->im.ny) y_pixel_high = oi->im.ny -1;
  if (xend <= xco ) return 1;
  if (y_pixel_high <= y_pixel_low ) return 1;
  float integrated_pix = 0;
  float max;

  *max_integrated_pix = 0;



  int n_x_win = (int) ((xend-xco - gt->track_free_window_width) / gt->track_free_shift_window);

  for (int i = 0;i < n_x_win;i++)
  {
    SDI_2_sum_image_on_area_and_send_barycenter(oi, xco + i *gt->track_free_shift_window, xco + i *gt->track_free_shift_window +  gt->track_free_window_width,
       y_pixel_low, y_pixel_high, &x_bar, &y_bar, &integrated_pix,0,&max);

    if (max > *max_integrated_pix)
    {
      *x_bar_max = x_bar;
      *y_bar_max = y_bar;
      *max_integrated_pix = max;

    }


  }

return 0;


}

d_s *SDI_2_find_fringes_in_partial_OI(O_i *oi, int xco, int xend, int yco, int yend, d_s *dsfringes, int threshold, int xw, int yw)
{

  if (oi == NULL) return NULL;
  if (oi->im.data_type != IS_CHAR_IMAGE) return(NULL);
  if (dsfringes == NULL) dsfringes = build_data_set(16,16);
  dsfringes ->nx = dsfringes->ny = 0;

  if (xco < 0 ) xco = 0;
  if (xend >=oi->im .nx) xend =oi->im .nx - 1;
  if (yco <0) yco = 0;
  if (yend >=oi->im .ny) yend =oi->im .ny - 1;
  if (xco >= xend) return NULL;
  if (yco >= yend) return NULL;

  int ly = yend - yco + 1;
  int lx = xend - xco + 1;
  union pix *pd=NULL;

  float *xp = calloc(lx,sizeof(float));
  float *yp = calloc(ly,sizeof(float));


  pd  = oi->im.pixel;

 int stop = 0;

 int ymax = 0;
 double xm = 0;
 int dummy = 0;
  for (int i= yw/2; i < ly-yw/2; i++)
  {
      for (int j = xw/2; j < lx-xw/2; j++)
      {
         if (pd[yco+i].ch[xco+j] <  threshold) continue; // si en dessous du trehold ne nous intéresse pas
         stop = 0;
         for (int k = 0 ; k< dsfringes->nx;k++)
         {
           if ((fabs(dsfringes->xd[k] - xco -j  ) < xw) && (fabs(dsfringes->yd[k] - yco - i ) < yw)) // si près d'une autre frange ne nous intéresse pas
           {
             stop = 1;

             continue;
           }
         }
           if (stop) continue;

           if (SDI_2_fill_x_av_y_profile_from_im(oi, xco+j-xw/2,  xco+j+xw/2,  yco+i-yw/2, yco+ i+yw/2,yp,&ymax,&dummy,&dummy)==1) return NULL;
           if (SDI_2_fill_y_av_x_profile_from_im(oi,  xco+j-xw/2,  xco+j+xw/2, yco+ i-yw/2,  yco+i+yw/2, xp , NULL,&xm)) return NULL;
           if (xm < xw/6 || xm > xw-xw/6) continue;//not interested by side maxima
           if (ymax < yw/6 || ymax > yw-yw/6) continue; //not interested by side maxima


           add_new_point_to_ds(dsfringes,xco+j-xw/2+(float) xm, yco+i-yw/2+(float) ymax);
       }
     }
     free(xp);
     free(yp);
     return dsfringes;
}


d_s *SDI_2_find_beads_in_partial_OI(O_i *oi, int xco, int xend, int yco, int yend, int threshold, int xw, int yw, int distance_x_max, int distance_y_max)
{
  d_s *dsbeads = NULL;
  d_s *dsfringes = NULL;
  dsfringes = SDI_2_find_fringes_in_partial_OI(oi,xco,xend, yco, yend, NULL, threshold, xw, yw);

  if (dsfringes == NULL) return 1;
  dsbeads = build_data_set(16,16);
  dsbeads->nx = dsbeads->ny = 0;
  alloc_data_set_y_error(dsbeads);
  alloc_data_set_x_error(dsbeads);
  int invert = 0;
  for (int i = 0;i<dsfringes->nx;i++)
  {
    if (dsfringes->xd[i] == 0) continue;
    for (int j = i+1;j<dsfringes->nx;j++)
    {
      if (dsfringes->xd[j] == 0) continue;
      if ( (fabs(dsfringes->xd[i] - dsfringes->xd[j]) < distance_x_max) && (fabs(dsfringes->yd[i] - dsfringes->yd[j]) < distance_y_max))
      {
        invert = (dsfringes->yd[i] - dsfringes->yd[j] > 0) ? 1:0;
        add_new_point_to_ds(dsbeads,invert ? dsfringes->xd[i] : dsfringes->xd[j],invert ? dsfringes->yd[i] : dsfringes->yd[j]) ;
        dsbeads->xe[dsbeads->nx-1] = invert ? dsfringes->xd[j] : dsfringes->xd[i];
        dsbeads->ye[dsbeads->nx-1] = invert ? dsfringes->yd[j] : dsfringes->yd[i];
        dsfringes->xd[i] = dsfringes->xd[j] = 0;
        break;

      }
    }
  }

  free_data_set(dsfringes);

     return dsbeads;
   }




int SDI_2_do_find_fringes_in_image(void)
{
    if (updating_menu_state != 0) return 0;
  O_i *ois = NULL;
  d_s *dsfringes = NULL;
  imreg *imr = NULL;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2) return D_O_K;


  static int xco=0,xend=2,yco=0,yend=200,threshold=50,xw=64,yw=32;
  xend = ois->im.nx;

  int i = win_scanf("Find fringes between xco %d and xend %d \n"
                "Between yco %d and yend %d \n"
                "Using intensity threshold %d \n"
                  "Width x %d\n"
                "Width y %d\n",&xco,&xend,&yco,&yend,&threshold,&xw,&yw);
  clock_t begin = clock();
  dsfringes = SDI_2_find_fringes_in_partial_OI(ois,  xco,  xend,  yco,  yend, dsfringes,  threshold,  xw,  yw);
  clock_t end = clock();
  double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
  for (i = 0;i<dsfringes->nx;i++)
  {
    draw_moving_shear_rect_vga_screen_unit(dsfringes->xd[i], dsfringes->yd[i],dsfringes->xd[i], dsfringes->yd[i], xw, yw,
           0, 0,0, lightgreen, imr->screen_scale, i, imr->one_i->bmp.stuff,0);
    }
  free_data_set(dsfringes);
  win_printf_OK("Function took %lf seconds\n",time_spent);

  return 0;
}


int SDI_2_do_find_beads_in_image(void)
{
    if (updating_menu_state != 0) return 0;
  O_i *ois = NULL;
  d_s *dsbeads = NULL;
  imreg *imr = NULL;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    return D_O_K;


  static int xco=0,xend=2,yco=0,yend=200,threshold=50,xw=256,yw=64,max_distance_x = 256,max_distance_y = 128;
  xend = ois->im.nx;

  int i = win_scanf("Find fringes between xco %d and xend %d \n"
                "Between yco %d and yend %d \n"
                "Using intensity threshold %d \n"
                "Width x %d \n"
                "Width y %d \n"
                "Max distance x %d \n"
                "Max distance y %d \n",&xco,&xend,&yco,&yend,&threshold,&xw,&yw,&max_distance_x,&max_distance_y);
  clock_t begin = clock();
  dsbeads = SDI_2_find_beads_in_partial_OI(ois,  xco,  xend,  yco,  yend,  threshold,  xw,  yw,max_distance_x,max_distance_y);
  clock_t end = clock();
  double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
  for (i = 0;i<dsbeads->nx;i++)
  {
    draw_moving_shear_rect_vga_screen_unit(dsbeads->xd[i], dsbeads->yd[i],dsbeads->xe[i], dsbeads->ye[i], xw, yw,
           0, 0,0, lightgreen, imr->screen_scale, i, imr->one_i->bmp.stuff,0);
    }
  free_data_set(dsbeads);
  win_printf_OK("Function took %lf seconds\n",time_spent);

  return 0;
}


int SDI_2_vincent_displacement_function(float *x ,int nx, int bp_low, int bp_high, double k_av,fft_plan *fp,double *phif, double *k, double *Max_pos, d_s *dsfit, float *xibp, float *xfourier)
{

register int i, j;
float dsr[nx], dsi[nx], dsfr[nx], dsfi[nx];
int imax;
float tmp, f0, amp, Max_val;
double re, im, phi;
float Max_pos_float;



//copy dst to dsr




for (j = 0;j < nx; j++)
{
  dsr[j] = x[j];

}

realtr1bt(fp, dsr);  fftbt(fp, dsr, 1);  realtr2bt(fp, dsr, 1);

for (i=1;i<nx/2-1;i++) xfourier[i]=dsr[2*i]*dsr[2*i]+dsr[2*i+1]*dsr[2*i+1];
xfourier[0]=dsr[0]*dsr[0];

for (j = 0;j < nx/2; j++)
{
  if ((j >= bp_low) && (j <= bp_high)) continue;
  dsr[2*j] = 0;
  dsr[2*j+1] = 0;

}
//
// duplicate_data_set(dsr,ds3);
// set_ds_source(ds3,"fourier transform");

j = bp_low;
for ( f0 = amp = 0;j <= bp_high; j++)
{  // we compute the frequency barycenter
  if (j < 0 || j >= nx) continue;
  tmp = dsr[2*j] * dsr[2*j] + dsr[2*j+1] * dsr[2*j+1];
  f0 += tmp * j;
  amp += tmp;
}
if (amp > 0) f0 /= amp;
for (j = 2;j < nx; j+=2)
{  // we prepare imaginary part
  dsi[j+1] = dsr[j];
  dsi[j] = -dsr[j+1];
}

dsi[1] = dsi[0] = 0;
/*	get back to complex world */
realtr2bt(fp, dsr, -1);  fftbt(fp, dsr, -1); realtr1bt(fp, dsr);
realtr2bt(fp, dsi, -1);  fftbt(fp, dsi, -1); realtr1bt(fp, dsi);


for (i=0;i<nx;i++) xibp[i]=dsr[i]; //copy in the supplied array the filtered x

for (j = 0;j < nx; j++)
{  // we find the amplitude maximum and place the amplitude in dsfr
  tmp = sqrt(dsr[j] * dsr[j] + dsi[j] * dsi[j]);
  dsfr[j] = tmp;
} // the amplitude is stored in dsfr

for (j = 4, imax = 4;j < nx-4; j++)
{  // we skip the border that are poluted by dewindowing
  if (dsfr[j] > dsfr[imax]) imax = j;
} // the amplitude is stored in dsfr
j = find_max_around(dsfr, nx, imax, &Max_pos_float, &Max_val, NULL);

*Max_pos= (double) Max_pos_float;


j = imax;
re = dsr[j];
im = dsi[j];
phi = (im == 0.0 && re == 0.0) ? 0.0 : atan2(im, re); // phase in j
dsfi[j] = phi;
//win_printf("1");
for (j = imax+1;j < nx; j++)
{
  re = dsr[j] * dsr[j-1] + dsi[j] * dsi[j-1];
  im = dsi[j] * dsr[j-1] - dsr[j] * dsi[j-1];
  phi += (im == 0.0 && re == 0.0) ? 0.0 : atan2(im, re);
  dsfi[j] = phi;
}
//win_printf("2");
phi = dsfi[imax];
for (j = imax-1;j >= 0; j--)
{
  re = dsr[j+1] * dsr[j] + dsi[j+1] * dsi[j];
  im = dsi[j+1] * dsr[j] - dsr[j+1] * dsi[j];
  phi -= (im == 0.0 && re == 0.0) ? 0.0 : atan2(im, re);
  dsfi[j] = phi;
}
//win_printf("3");
for (j = 0, dsfit->nx = 0;j < nx; j++)
{

  if (mr*mr*dsfr[j] > Max_val) // we keep phase if amp^2 > max/2
  {
    dsfit->yd[dsfit->nx] = dsfi[j];
    dsfit->ye[dsfit->nx] = (tmp > 0) ? ((float)1)/dsfr[j] : 10;
    dsfit->xd[dsfit->nx++] = j;
  }
}

for (j = 0; useapo == 1 && j < dsfit->nx; j++) // appodisation
dsfit->ye[j] *= (float)2/(1.05 - cos(2*j*M_PI/dsfit->nx));

if (dsfit->nx < 3)       display_title_message("Fitting over %d max val %g at %g",dsfit->nx,Max_val,*Max_pos);
else
{
  //win_printf("fitting over %d pts",dsfit->nx);
  if (compute_least_square_fit_on_ds(dsfit, 1, k, phif))
  {
    display_title_message("fitting problem");
  }
}

// if (k_buffer_active==0) return 0;
//
// if (k_av != 0)
// {
//
// *phif=0;
//
//
//
// for (i=0;i<dsfit->nx;i++)
// {
//   dsfit->yd[i]-=k_av*dsfit->xd[i];
//   *phif+=dsfit->yd[i]/dsfit->nx;
// }
//
// }

return 0;


}

MENU *SDI_2_tracking_parameters_menu(void)
{
    static MENU mn[32] = {0};

    if (mn[0].text != NULL) return mn;
    add_item_to_menu(mn, "Change tracking parameters", SDI_2_do_set_tracking_parameters, NULL, 0, NULL);
    add_item_to_menu(mn, "Change window on x", SDI_2_do_choose_window_on_x,NULL,0,NULL);
    add_item_to_menu(mn,"Change free tracking parameters",SDI_2_do_set_free_tracking_parameters,NULL,0,NULL);

    return mn;
}

 #endif
