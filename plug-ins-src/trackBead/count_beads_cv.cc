#include "count_beads_cv.hh"
#include "trackBead.h"
#include "track_util.h"
#include "gpu/debug_tools.h"
#include "gpu/track_beads.h"

#undef USER
#undef DEFAULT
#undef DONE
//#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include <cstdio>

//using namespace cv;
//using namespace std;


int do_count_beads_hough(void)
{
    int nearness_radius = 60;
    int distinction_radius = 40;
    int cannythreashold = 30;
    int accumulator_val = 30;
    int min_radius = 20;
    int max_radius = 60;
    int create_cross = 1;
    int cl = 128;
    int cw = 16;
    int switch_to_gpu = 1;
    //imreg *imr = NULL;
    //O_i * ois = NULL;

    //if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)  return OFF;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }


    nearness_radius = get_config_int("CALIBRATION-HOUGH", "nearness_radius_between_beads_center", nearness_radius);
    distinction_radius = get_config_int("CALIBRATION-HOUGH", "distinction_radius_between_beads_center", distinction_radius);
    cannythreashold = get_config_int("CALIBRATION-HOUGH", "canny_threashold", cannythreashold);
    accumulator_val = get_config_int("CALIBRATION-HOUGH", "accumulator_value", accumulator_val);
    min_radius = get_config_int("CALIBRATION-HOUGH", "min_circle_radius", min_radius);
    max_radius = get_config_int("CALIBRATION-HOUGH", "max_circle_radius", max_radius);
    switch_to_gpu = get_config_int("CALIBRATION-HOUGH", "switch_to_gpu", switch_to_gpu);
    create_cross = get_config_int("CALIBRATION", "count_beads_do_create_crosses", create_cross);
    cl = get_config_int("CALIBRATION", "count_beads_crosses_length", cl);
    cw = get_config_int("CALIBRATION", "count_beads_crosses_width", cw);

    int ret_val = win_scanf("Count beads with hough transform: \n"
                            "min distance to distinguish two beads%5d\n"
                            "min pixel distance between beads %5d\n"
                            "canny threashold %5d\n"
                            "accumulator minmum value %5d\n"
                            "min circle radius %5d\n"
                            "max circle radius %5d\n"
                            "%b Create crosses foreach circle\n length %6d width %6d\n"
                            "switch to GPU %b\n", &distinction_radius, &nearness_radius, &cannythreashold,
                            &accumulator_val, &min_radius,
                            &max_radius, &create_cross, &cl, &cw, &switch_to_gpu);

    if (ret_val == CANCEL)
        return 0;

    set_config_int("CALIBRATION-HOUGH", "nearness_radius_between_beads_center", nearness_radius);
    set_config_int("CALIBRATION-HOUGH", "distinction_radius_between_beads_center", distinction_radius);
    set_config_int("CALIBRATION-HOUGH", "canny_threashold", cannythreashold);
    set_config_int("CALIBRATION-HOUGH", "accumulator_value", accumulator_val);
    set_config_int("CALIBRATION-HOUGH", "min_circle_radius", min_radius);
    set_config_int("CALIBRATION-HOUGH", "max_circle_radius", max_radius);
    set_config_int("CALIBRATION-HOUGH", "switch_to_gpu", switch_to_gpu);
    set_config_int("CALIBRATION", "count_beads_do_create_crosses", create_cross);
    set_config_int("CALIBRATION", "count_beads_crosses_length", cl);
    set_config_int("CALIBRATION", "count_beads_crosses_width", cw);

    if (switch_to_gpu)
    {
        set_tracking_device(TRACKING_DEVICE_GPU);
    }


    return count_beads_hough(oi_TRACK, distinction_radius, nearness_radius, cannythreashold, accumulator_val, min_radius,
                             max_radius, create_cross, cl, cw);


}

bool cross_exist(float x, float y, float nearness_radius)
{

    for (int i = 0; i < track_info->n_b; ++i)
    {
        b_track *bt = track_info->bd[i];

        float x1 = x - bt->xc;
        float y1 = y - bt->yc;
        float r = sqrt(x1 * x1 + y1 * y1);

        if (r < nearness_radius)
        {
            return true;
        }
    }

    return false;
}

bool are_near_but_distinct_beads(float x1, float y1, float x2, float y2, float distinction_radius,
                                 float nearness_radius)
{
    float x = x2 - x1;
    float y = y2 - y1;
    float r = sqrt(x * x + y * y);
    return (r > distinction_radius && r < nearness_radius);
}

std::vector<bool> *get_not_clustered_beads(std::vector<cv::Vec3f>& circles, float distinction_radius,
                                           float nearness_radius)
{
    std::vector<bool> *are_beads_not_clustered = new std::vector<bool>(circles.size(), true);

    for (size_t i = 0; i < circles.size(); ++i)
    {
        float x1 = circles[i][0];
        float y1 = circles[i][1];

        for (size_t j = 0; j < circles.size(); ++j)
        {
            float x2 = circles[j][0];
            float y2 = circles[j][1];

            if (are_near_but_distinct_beads(x1, y1, x2, y2, distinction_radius, nearness_radius))
            {
                (*are_beads_not_clustered)[i] = false;
                (*are_beads_not_clustered)[j] = false;
            }
        }
    }

    return are_beads_not_clustered;
}

int count_beads_hough(O_i *im,
                      int distinction_radius,
                      int nearness_radius,
                      int cannythreashold,
                      int accumulator_val,
                      int min_radius,
                      int max_radius,
                      int create_cross,
                      int cl,
                      int cw)
{
    imreg *imr = NULL;
    O_i *cio = NULL;
    O_p *op = NULL;
    d_s *clustered_ds = NULL;
    d_s *not_clustered_ds = NULL;

    if (ac_grep(NULL, "%im", &imr) < 1 || imr == NULL)
        return -1;

    cio = create_and_attach_oi_to_imr(imr, im->im.nx, im->im.ny, IS_CHAR_IMAGE);

    duplicate_image_or_movie(im, cio, 0);

    cv::Mat img(cio->im.ny, cio->im.nx, CV_8UC1, cio->im.mem[cio->im.c_f]);

    //imwrite( "Gray_Image.jpg", img );

    //std::cout << im->im.ny  << std::endl;
    //cpu_image cim = {0};
    //cim.width = im->im.nx;
    //cim.height = im->im.ny;
    //cim.data = img.data;

    //crosses.length = 128;
    // output_debug_image(NULL, &cim, NULL, NULL, NULL);

    std::vector<cv::Vec3f> circles;
    HoughCircles(img, circles, CV_HOUGH_GRADIENT, 1,
                 distinction_radius, cannythreashold, accumulator_val, min_radius, max_radius);
    if (circles.size() == 0)
        return 1;

    std::vector<bool> *are_beads_not_clustered = get_not_clustered_beads(circles, distinction_radius, nearness_radius);

    op = create_and_attach_op_to_oi(cio, circles.size(), circles.size(), 0, IM_SAME_X_AXIS + IM_SAME_Y_AXIS);
    clustered_ds = op->dat[0];
    not_clustered_ds = create_and_attach_ds_to_oi(cio, circles.size(), circles.size(), 0);
    clustered_ds->nx = 0;
    clustered_ds->ny = 0;
    not_clustered_ds->nx = 0;
    not_clustered_ds->ny = 0;

    set_ds_source(clustered_ds, "clustered beads");
    set_ds_source(not_clustered_ds, "not clustered beads");
    set_ds_dot_line(clustered_ds);
    set_ds_dot_line(not_clustered_ds);
    set_ds_point_symbol(clustered_ds, "\\fd");
    set_ds_point_symbol(not_clustered_ds, "\\fd");



    //std::cout << circles.size()  << std::endl;

    for (size_t i = 0; i < circles.size(); i++)
    {
        cv::Vec3f c = circles[i];

        if ((*are_beads_not_clustered)[i])
        {
            add_new_point_to_ds(not_clustered_ds, c[0], c[1]);

            if (create_cross && !cross_exist(c[0], c[1], nearness_radius))
            {
                do_add_bead_in_x_y(cl, cw, c[0], c[1]);
                track_info->bd[track_info->n_b - 1]->saved_x = c[0] * im->dx;
                track_info->bd[track_info->n_b - 1]->saved_y = c[1] * im->dy;
                track_info->bd[track_info->n_b - 1]->fence_x = c[0] * im->dx;
                track_info->bd[track_info->n_b - 1]->fence_y = c[1] * im->dy;
                track_info->bd[track_info->n_b - 1]->fence_xi = c[0];
                track_info->bd[track_info->n_b - 1]->fence_yi = c[1];
                track_info->bd[track_info->n_b - 1]->xc = c[0];
                track_info->bd[track_info->n_b - 1]->yc = c[1];
            }
        }
        else
        {
            add_new_point_to_ds(clustered_ds, c[0], c[1]);
        }
    }
    cio->need_to_refresh = ALL_NEED_REFRESH;
    op->need_to_refresh = ALL_NEED_REFRESH;

    return 0;
}
