/** \file magnetscontrol.c

    \brief
    \sa movie_statistics.c (c'est un "see also")

    \author V. Croquette

    \version 4/10/07
*/

////////////////////////////////////


#ifndef _MAGNETSCONTROL_C_
#define _MAGNETSCONTROL_C_


# include "ctype.h"
# include "allegro.h"
#ifdef XV_WIN32
# include "winalleg.h"
#endif
# include "xvin.h"
# include "xv_plugins.h"    // for inclusions of libraries that control dynamic loading of libraries.
#include "timer_thread.h"
#include <pthread.h>


int rot_pid[10], zmag_pid[10];

# ifdef XVIN_STATICLINK

int _init_magnets_OK(void);
int   _set_rot_value(float rot);
int _set_rot_ref_value(float rot);  /* in tour */
float _read_rot_value(void);
int   _set_magnet_z_value(float pos);

float _read_magnet_z_value(void);

int _set_motors_speed(float v);
float  _get_motors_speed();

int _set_magnet_z_ref_value(float z);  /* in mm */
int   _set_z_origin(float pos, int dir);
int _has_magnets_memory(void);
int    _set_magnet_z_value_and_wait(float pos);
int    _set_rot_value_and_wait(float rot);
int _go_and_dump_z_magnet(float z);
int    _go_wait_and_dump_z_magnet(float zmag);
int    _go_and_dump_rot(float r);
int _go_wait_and_dump_rot(float r);
int _go_wait_and_dump_log_specific_rot(float r, char *log);
int _go_wait_and_dump_log_specific_z_magnet(float zmag, char *log);


PXV_FUNC(int, _request_read_rot_value, (int image_n, unsigned long *t0));
PXV_FUNC(float, _read_rot_raw_from_request_answer, (char *answer, int im_n, float val, int *error));
PXV_FUNC(int, _request_set_rot_value_raw, (int image_n, float rot, unsigned long *t0));
PXV_FUNC(int, _check_set_rot_request_answer, (char *answer, int im_n, float rot));
PXV_FUNC(int, _request_set_magnet_z_value_raw, (int image_n, float pos,  unsigned long *t0));
PXV_FUNC(int, _check_set_zmag_request_answer, (char *answer, int im_n, float pos));
PXV_FUNC(int, _request_read_magnet_z_value, (int image_n, unsigned long *t0));
PXV_FUNC(float, _read_zmag_raw_from_request_answer, (char *answer, int im_n, float val, int *error, float *vacp));
PXV_FUNC(int, _request_set_zmag_ref_value, (int image_n, float pos, unsigned long *t0));
PXV_FUNC(int, _check_set_zmag_ref_request_answer, (char *answer, int im_n, float pos));
PXV_FUNC(int, _request_set_rot_ref_value, (int image_n, float rot,  unsigned long *t0));
PXV_FUNC(int, _check_set_rot_ref_request_answer, (char *answer, int im_n, float rot));
PXV_FUNC(int, _read_magnet_z_value_and_status, (float *z, float *vcap, int *limit, int *vcap_servo));

PXV_FUNC(float,  _get_Vcap_min, (void));
PXV_FUNC(int,   _set_Vcap_min, (float volts));


PXV_FUNC(int, _set_zmag_motor_speed, (float v));
PXV_FUNC(float,  _get_zmag_motor_speed, (void));
PXV_FUNC(int, _set_zmag_motor_pid, (int num, int val));
PXV_FUNC(int,  _get_zmag_motor_pid, (int num));
PXV_FUNC(int, _set_rotation_motor_pid, (int num, int val));
PXV_FUNC(int,  _get_rotation_motor_pid, (int num));

# endif





# define BUILDING_PLUGINS_DLL
# include "../cfg_file/Pico_cfg.h"
# include "../wlc/wlc.h"


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "magnetscontrol.h"
# include "../fft2d/fftbtl32n.h"
# include "fillibbt.h"
# include "trackBead.h"
# include "track_util.h"
# include "action.h"
# include "focus.h"
# include "game_source.h"
# include <pthread.h>

#ifdef XV_WIN32
#   include <windows.h>
#endif
#include <unistd.h>
// local variables

unsigned long last_zmag_set, zmag_moving_time = 0;
unsigned long last_rot_set, rot_moving_time;

int Pos_limit = 0, Neg_limit = 0;
char m_speed[512], max_pos[512], ref_pos[512];

float preset_zmag_key_val[8];
int preset_zmag_key_index = -1;
int preset_zmag_key_active = 0;

float preset_force_zmag_key_val[20];
float preset_force_key_val[20];
int preset_force_key_index = -1;
int preset_force_key_active = 0;


float Mag_vcap_min = 0;

/*
FILE *fp_ldebug = NULL;
char *ldebug_file = "c:\\dummy_debug.txt"; //NULL;
int im_ci(void);
*/

int     dummy_init_magnets_OK(void);
int dummy_set_rot_value(float rot);
int     dummy_set_rot_ref_value(float rot);  /* in tour */
float   dummy_read_rot_value(void);
int     dummy_set_magnet_z_value(float pos);
float   dummy_read_magnet_z_value(void);
int     dummy_set_motors_speed(float v);
float   dummy_get_motors_speed(void);

int     dummy_set_zmag_motor_pid(int param_nb, int val);
int     dummy_get_zmag_motor_pid(int param_nb);

int     dummy_set_rotation_motor_pid(int param_nb, int val);
int     dummy_get_rotation_motor_pid(int param_nb);

int     dummy_set_magnet_z_ref_value(float z);  /* in mm */
int     dummy_set_magnet_z_value_and_wait(float pos);
int     dummy_set_rot_value_and_wait(float rot);
int dummy_go_and_dump_z_magnet(float z);
int     dummy_go_wait_and_dump_z_magnet(float zmag);
int     dummy_go_and_dump_rot(float r);
int dummy_go_wait_and_dump_rot(float r);
int dummy_go_wait_and_dump_log_specific_rot(float r, char *log);
int dummy_go_wait_and_dump_log_specific_z_magnet(float zmag, char *log);
int     dummy_set_z_origin(float z, int dir);
int     dummy_has_magnets_memory(void);
int     dummy_read_magnet_z_value_and_status(float *z, float *vcap, int *limit, int *vcap_servo);
int     dummy_set_Vcap_min(float v);
float   dummy_get_Vcap_min(void);
int     dummy_set_zmag_motor_speed(float v);
float   dummy_get_zmag_motor_speed(void);

int zmag_bd_search_key(void);
int preset_zmag_key(void);

int select_dummy_magnets(void)
{
    // failsafe dummy device !
    init_magnets_OK = dummy_init_magnets_OK;
    set_rot_value_raw = dummy_set_rot_value;
    set_rot_ref_value = dummy_set_rot_ref_value;
    read_rot_value_raw = dummy_read_rot_value;
    set_magnet_z_value_raw = dummy_set_magnet_z_value;
    read_magnet_z_value_raw = dummy_read_magnet_z_value;
    read_magnet_z_value_and_status_raw = dummy_read_magnet_z_value_and_status;
    set_motors_speed = dummy_set_motors_speed;
    get_motors_speed = dummy_get_motors_speed;
    set_magnet_z_ref_value = dummy_set_magnet_z_ref_value;
    set_magnet_z_value_and_wait_raw = dummy_set_magnet_z_value_and_wait;
    set_rot_value_and_wait_raw = dummy_set_rot_value_and_wait;
    set_z_origin = dummy_set_z_origin;
    has_magnets_memory = dummy_has_magnets_memory;
    get_Vcap_min = dummy_get_Vcap_min;
    set_Vcap_min = dummy_set_Vcap_min;
    set_zmag_motor_speed = dummy_set_zmag_motor_speed;
    get_zmag_motor_speed = dummy_get_zmag_motor_speed;
    set_zmag_motor_pid = dummy_set_zmag_motor_pid;
    get_zmag_motor_pid = dummy_get_zmag_motor_pid;
    set_rotation_motor_pid = dummy_set_rotation_motor_pid;
    get_rotation_motor_pid = dummy_get_rotation_motor_pid;


    init_magnets_OK();

    return 0;
}

#ifdef ZMAG_THREAD

pthread_t zmag_thread = 0;
zmag_t_d *zmag_data = NULL;
bool zmag_continous_polling = false;
int available_zmag_id = 0;

float thread_zmag_float = -1;
int thread_zmag_int = -1;
int zmag_thread_action = 0;

int do_launch_zmag_thread(void)
{
  if (updating_menu_state!=0) return D_O_K;
  launch_zmag_thread();
  return 0;
}
int launch_zmag_thread(void)
{
  int success = 0;

  if (zmag_data == NULL)
    {
      zmag_data = (zmag_t_d *)calloc(1, sizeof(zmag_t_d));
      zmag_data->zmag_thread_buffer = 128;
      zmag_data->period_zmag_thread = 0.1; // in seconds (in linux at least)
      zmag_data->timestamp = (long long *)calloc(zmag_data->zmag_thread_buffer, sizeof(long long));
      zmag_data->zmag_value = (float *)calloc(zmag_data->zmag_thread_buffer, sizeof(float));
      zmag_data->limit = (int *)calloc(zmag_data->zmag_thread_buffer, sizeof(int));
      zmag_data->current_i = 0;
    }
  if (zmag_thread == 0)
    success = pthread_create (&zmag_thread, NULL, zmag_continous_thread, NULL);
  zmag_continous_polling = true;
  if (success) win_printf_OK("zmag thread not launched !");
  return success;
}
int zmag_continous_thread(void)
{
  static float vcap_dummy = 0;
  static int status_dummy = 0;
  static int cc = 0;
  time_t t1=0,t2=0;
  int ft_locked = 0;

  while(true)

  {


    sleep(zmag_data->period_zmag_thread);


    while ((zmag_thread_action) && ~(zmag_thread_action & ZMAG_LOCKED))  // only go in this if there is un action and the thread is not locked.
    {
    if (zmag_thread_action & ZMAG_THREAD_GET_SPEED)
    {

      zmag_thread_action &= (~ZMAG_THREAD_GET_SPEED);
      zmag_thread_action |= ZMAG_LOCKED;
      thread_zmag_float = _get_motors_speed();
    }
    else if (zmag_thread_action & ZMAG_THREAD_SET_SPEED)
    {

      zmag_thread_action &= (~ZMAG_THREAD_SET_SPEED);
      zmag_thread_action |= ZMAG_LOCKED;
      thread_zmag_int = _set_motors_speed(thread_zmag_float);
    }

    else if (zmag_thread_action & ZMAG_THREAD_SET_ZMAG_SPEED)
    {

      zmag_thread_action &= (~ZMAG_THREAD_SET_ZMAG_SPEED);
      zmag_thread_action |= ZMAG_LOCKED;
      thread_zmag_int = _set_zmag_motor_speed(thread_zmag_float);
    }
    else if (zmag_thread_action & ZMAG_THREAD_GET_ZMAG_SPEED)
    {

      zmag_thread_action &= (~ZMAG_THREAD_GET_ZMAG_SPEED);
      zmag_thread_action |= ZMAG_LOCKED;
      thread_zmag_float = _get_zmag_motor_speed();
    }
    else break;
  }

    if (zmag_thread_action & ZMAG_LOCKED) // Add a 5 seconds timeout to the lock
    {
      if (ft_locked == 0)
      {
        ft_locked = 1;
        t1 = my_uclock();
      }
      t2 = my_uclock();
      if (t2 - t1 > get_my_uclocks_per_sec() * 5)
      {
        zmag_thread_action &= ~ZMAG_LOCKED;
        ft_locked = 0;
      }
    }

    else ft_locked = 0;



     // if the lock does not go away after 2 seconds, we take it away manually

    if (zmag_continous_polling == false) continue;

      zmag_data->timestamp[zmag_data->current_i] = my_ulclock();
      read_magnet_z_value_and_status(zmag_data->zmag_value + zmag_data->current_i, &vcap_dummy, zmag_data->limit + zmag_data->current_i, &status_dummy);
      available_zmag_id = zmag_data->current_i;
      zmag_data->current_i ++;
      zmag_data->current_i = (zmag_data->current_i)%zmag_data->zmag_thread_buffer;
      //my_set_window_title("polling zmag %3.3f count =%d", zmag_data->zmag_value[available_zmag_id], cc);
      cc++;

  }
  return 0;
}

float zmag_find_timestamp_match(long long t) //adaptive freq to poll 4 time quicker
{
  int i=0;
  static int last_match_index = 0;
  long long delta_timestamp = 0;
  long long best_delta_timestamp = ( (zmag_data->timestamp[0] - t)<0 ) ? (-(zmag_data->timestamp[0] - t)) : (zmag_data->timestamp[0] - t);
  static int match_index=0;
  long long jitter;
  for (i=0;i<zmag_data->zmag_thread_buffer;i++)//attention a loop
  {
    delta_timestamp =  ( (zmag_data->timestamp[i] - t)<0 ) ? (-(zmag_data->timestamp[i] - t)) : (zmag_data->timestamp[i] - t);
    if (delta_timestamp <= best_delta_timestamp)
    {
      best_delta_timestamp = delta_timestamp;
      match_index = i;
    }
    jitter = abs(t-zmag_data->timestamp[match_index]);
    //if ( jitter >= 10000 )  zmag_data->period_zmag_thread = ( (zmag_data->period_zmag_thread-2) >=4) ? (zmag_data->period_zmag_thread-2) : zmag_data->period_zmag_thread;
    //if ( jitter <= 5000 ) zmag_data->period_zmag_thread += 2;
  }
  //my_set_window_title("period %d t%d t0%d tf%d", zmag_data->period_zmag_thread, (int)t, (int)mag_data->timestamp[0], (int) mag_data->timestamp[zmag_data->zmag_thread_buffer-1]);
  float z;
  //z = ( fabs(zmag_data->zmag_value[zmag_data->current_i])>20 ) ? 0 : zmag_data->zmag_value[zmag_data->current_i];
  return zmag_data->zmag_value[match_index] ;
}

int zmag_thread_set_motor_speed (float v)
{
  time_t t1,t2;
  t1 = my_uclock();
  pthread_t pthread_self(void);
  int r;

  while (zmag_thread_action)
  {
    t2 = my_uclock();
    if ((t2-t1) > get_my_uclocks_per_sec()) return(printf("Zmag thread busy\n"));
  }

  thread_zmag_float = v;
  thread_zmag_int = -1;

  zmag_thread_action |= ZMAG_THREAD_SET_SPEED;


  t1 = my_uclock();

  while (thread_zmag_int == -1)
  {
    t2 = my_uclock();
    if ((t2-t1) > get_my_uclocks_per_sec()) return(printf("No update from zmag thread\n"));
  }
  r = thread_zmag_int;
  zmag_thread_action &= (~ZMAG_THREAD_SET_SPEED);
  zmag_thread_action &= (~ZMAG_LOCKED);
  return r;
}

int zmag_thread_set_zmag_motor_speed (float v)
{
  time_t t1,t2;
  t1 = my_uclock();
  int r;

  while (zmag_thread_action)
  {
    t2 = my_uclock();
    if ((t2-t1) > get_my_uclocks_per_sec()) return(printf("Zmag thread busy\n"));
  }

  thread_zmag_float = v;
  thread_zmag_int = -1;

  zmag_thread_action |= ZMAG_THREAD_SET_ZMAG_SPEED;


  t1 = my_uclock();

  while (thread_zmag_int == -1)
  {
    t2 = my_uclock();
    if ((t2-t1) > get_my_uclocks_per_sec()) return(printf("No update from zmag thread\n"));
  }
  r = thread_zmag_int;
  zmag_thread_action &= (~ZMAG_THREAD_SET_ZMAG_SPEED);
  zmag_thread_action &= (~ZMAG_LOCKED);
  return r;
}

float zmag_thread_get_zmag_motor_speed(void)
{
  time_t t1,t2;
  t1 = my_uclock();
  float v;



  while (zmag_thread_action)
  {
    t2 = my_uclock();
    if ((t2-t1) > get_my_uclocks_per_sec()) return(printf("Zmag thread busy\n"));
  }

  thread_zmag_float = -1;


  zmag_thread_action |= ZMAG_THREAD_GET_ZMAG_SPEED;


  t1 = my_uclock();

  while (thread_zmag_float == -1)
  {
    t2 = my_uclock();
    if ((t2-t1) > get_my_uclocks_per_sec()) return(printf("No update from zmag thread\n"));
  }
  v = thread_zmag_float;
  zmag_thread_action &= (~ZMAG_THREAD_GET_ZMAG_SPEED);
  zmag_thread_action &= (~ZMAG_LOCKED);

  return v;

}

float zmag_thread_get_motor_speed(void)
{
  time_t t1,t2;
  t1 = my_uclock();
  float v;


  while (zmag_thread_action)
  {
    t2 = my_uclock();
    if ((t2-t1) > get_my_uclocks_per_sec()) return(printf("Zmag thread busy\n"));
  }

  thread_zmag_float = -1;


  zmag_thread_action |= ZMAG_THREAD_GET_SPEED;


  t1 = my_uclock();

  while (thread_zmag_float == -1)
  {
    t2 = my_uclock();
    if ((t2-t1) > get_my_uclocks_per_sec()) return(printf("No update from zmag thread\n"));
  }
  v = thread_zmag_float;
  zmag_thread_action &= (~ZMAG_THREAD_GET_SPEED);
  zmag_thread_action &= (~ZMAG_LOCKED);


  return v;

}
#endif

# if !defined(DUMMY) && defined(XVIN_STATICLINK)
int fixed_select_magnets(void)
{
    init_magnets_OK = _init_magnets_OK;
    set_rot_value_raw = _set_rot_value;
    set_rot_ref_value = _set_rot_ref_value;
    read_rot_value_raw = _read_rot_value;
    set_magnet_z_value_raw = _set_magnet_z_value;
    read_magnet_z_value_raw = _read_magnet_z_value;
    set_motors_speed = _set_motors_speed;
    get_motors_speed = _get_motors_speed;
    set_magnet_z_ref_value = _set_magnet_z_ref_value;
    set_magnet_z_value_and_wait_raw = _set_magnet_z_value_and_wait;
    set_rot_value_and_wait_raw = _set_rot_value_and_wait;
    set_z_origin = _set_z_origin;
    has_magnets_memory = _has_magnets_memory;
    set_zmag_motor_speed = _set_zmag_motor_speed;
    get_zmag_motor_speed = _get_zmag_motor_speed;

#ifdef PI_E761
    read_magnet_z_value_and_status_raw = _read_magnet_z_value_and_status;
#endif

#if !defined(PI_C843) && !defined(PI_E761) && !defined(PI_E709) && !defined(ZMAG_APT)

    request_set_rot_value_raw = _request_set_rot_value_raw;
    check_set_rot_request_answer = _check_set_rot_request_answer;
    request_set_magnet_z_value_raw = _request_set_magnet_z_value_raw;
    check_set_zmag_request_answer = _check_set_zmag_request_answer;
    request_read_rot_value = _request_read_rot_value;
    read_rot_raw_from_request_answer = _read_rot_raw_from_request_answer;
    request_read_magnet_z_value = _request_read_magnet_z_value;
    read_zmag_raw_from_request_answer = _read_zmag_raw_from_request_answer;
    request_set_rot_ref_value = _request_set_rot_ref_value;
    check_set_rot_ref_request_answer = _check_set_rot_ref_request_answer;
    request_set_zmag_ref_value = _request_set_zmag_ref_value;
    check_set_zmag_ref_request_answer = _check_set_zmag_ref_request_answer;
    read_magnet_z_value_and_status_raw = _read_magnet_z_value_and_status;
    get_Vcap_min = _get_Vcap_min;
    set_Vcap_min = _set_Vcap_min;
    set_zmag_motor_pid = _set_zmag_motor_pid;
    get_zmag_motor_pid = _get_zmag_motor_pid;
    set_rotation_motor_pid = _set_rotation_motor_pid;
    get_rotation_motor_pid = _get_rotation_motor_pid;

# endif

#ifdef ZMAG_THREAD
   set_motors_speed = zmag_thread_set_motor_speed;
   get_motors_speed = zmag_thread_get_motor_speed;
   set_zmag_motor_speed = zmag_thread_set_zmag_motor_speed;
   get_zmag_motor_speed = zmag_thread_get_zmag_motor_speed;

#endif
//# ifdef ZMAG_APT
//    win_printf("Hello from fixed_select_magnets before init magnets");
//# endif
    init_magnets_OK();
    return 0;
}
# endif

int     set_magnetscontrol_device(Translation_Motor_param *t_m, Rotation_Motor_param *r_m)
{
    int  i;
# ifndef XVIN_STATICLINK
    char error[2048] = {0};
    char *argvp[2] = {0};
    void *hinstLib = {0};
    int  abort_loading = 0;
# endif
    char pl_name[256];
    Zmag_Ticks_Per_Mm = t_m->nb_pulses_per_mm;
    Zmag_Ticks_Per_Mm = (Zmag_Ticks_Per_Mm <= 0) ? 8000 : Zmag_Ticks_Per_Mm;
    Rot_Ticks_Per_Turn = r_m->nb_pulses_per_turn;
    Rot_Ticks_Per_Turn = (Rot_Ticks_Per_Turn <= 0) ? 8000 : Rot_Ticks_Per_Turn;
    snprintf(pl_name, 256, "%s_magnets", t_m->translation_motor_controller);
    dump_to_error_file_with_date_and_time("Selecting magnets control device from %s: File %s line %ul"
					  ,t_m->translation_motor_controller,__FILE__, __LINE__);
    for (i = 0; pl_name[i]; i++)
        pl_name[i] = tolower(pl_name[i]);


# ifndef DUMMY
# if defined(ZMAG_APT ) || defined(NO_DUMMY)
    // tv :if zmag_apt i do not care about successful serial comm
    if (1==1)
#else
    if (strcmp(pl_name, "dummy_magnets") != 0)
#endif
    {
# if defined(XVIN_STATICLINK) || !defined(XV_WIN32)

        get_Vcap_min = NULL;
        set_Vcap_min = NULL;
        set_zmag_motor_speed = NULL;
        get_zmag_motor_speed = NULL;
        set_zmag_motor_pid = NULL;
        get_zmag_motor_pid = NULL;
        set_rotation_motor_pid = NULL;
        get_rotation_motor_pid = NULL;

# if defined(PIAS)
	//ifdef ZMAG_APT
    win_printf("Dummy magnets selected");
    //endif
    dump_to_error_file_with_date_and_time("Selecting dummy device: File %s line %ul"
					  ,__FILE__, __LINE__);
        return select_dummy_magnets();
# else
# ifdef ZMAG_APT
    dump_to_error_file_with_date_and_time("Selecting normal device: File %s line %ul"
					  ,__FILE__, __LINE__);
    //win_printf("Device magnets selected");
# endif
        return fixed_select_magnets();
# endif
        return 0;
    }
# else
        if (strcmp(r_m->rotation_motor_controller, t_m->translation_motor_controller) == 0)
        {
            argvp[0] = pl_name;
            argvp[1] = NULL;
            load_plugin(1, argvp);
            hinstLib = grep_plug_ins_hinstLib(argvp[0]);
            abort_loading = 0;
            error[0] = 0;

            if (hinstLib != NULL)
            {
                init_magnets_OK = (int (*)(void))GetProcAddress(hinstLib, "_init_magnets_OK");
                if (init_magnets_OK != NULL)
                {
                    if (init_magnets_OK())
                    {
                        abort_loading++;
                        snprintf(error + strlen(error), 2048 - strlen(error), "Bad magnets device initialization\nin%s", argvp[0]);
                    }
                    else
                    {
                        set_rot_value_raw = (int (*)(float rot))GetProcAddress(hinstLib, "_set_rot_value");
                        if (set_rot_value_raw == NULL)
                        {
                            abort_loading++;
                            snprintf(error + strlen(error), 2048 - strlen(error), "Could not find: set_rot_value()\nin%s", argvp[0]);
                        }
                        set_rot_ref_value = (int (*)(float rot))GetProcAddress(hinstLib, "_set_rot_ref_value");
                        if (set_rot_ref_value == NULL)
                        {
                            abort_loading++;
                            snprintf(error + strlen(error), 2048 - strlen(error), "Could not find: set_rot_ref_value()\nin%s", argvp[0]);
                        }
                        read_rot_value_raw = (float (*)(void))GetProcAddress(hinstLib, "_read_rot_value");
                        if (read_rot_value_raw == NULL)
                        {
                            abort_loading++;
                            snprintf(error + strlen(error), 2048 - strlen(error), "Could not find: read_rot_value()\nin%s", argvp[0]);
                        }
                        set_magnet_z_value_raw = (int (*)(float z))GetProcAddress(hinstLib, "_set_magnet_z_value");
                        if (set_magnet_z_value_raw == NULL)
                        {
                            abort_loading++;
                            snprintf(error + strlen(error), 2048 - strlen(error), "Could not find: set_magnet_z_value()\nin%s", argvp[0]);
                        }
                        set_magnet_z_ref_value = (int (*)(float z))GetProcAddress(hinstLib, "_set_magnet_z_ref_value");
                        if (set_magnet_z_ref_value == NULL)
                        {
                            abort_loading++;
                            snprintf(error + strlen(error), 2048 - strlen(error), "Could not find: set_magnet_z_ref_value()\nin%s", argvp[0]);
                        }
                        read_magnet_z_value_raw = (float (*)(void))GetProcAddress(hinstLib, "_read_magnet_z_value");
                        if (read_magnet_z_value_raw == NULL)
                        {
                            abort_loading++;
                            snprintf(error + strlen(error), 2048 - strlen(error), "Could not find: read_magnet_z_value()\nin%s", argvp[0]);
                        }
                        read_magnet_z_value_and_status_raw  = (int (*)(float * z, float * vcap, int *limit, int *vcap_servo))
                                                              GetProcAddress(hinstLib, "_read_magnet_z_value_and_status");
                        if (read_magnet_z_value_and_status_raw == NULL)
                        {
                            abort_loading++;
                            snprintf(error + strlen(error), 2048 - strlen(error), "Could not find: _read_magnet_z_value_and_status_()\nin%s",
                                     argvp[0]);
                        }
                        set_magnet_z_value_and_wait_raw = (int (*)(float pos))GetProcAddress(hinstLib, "_set_magnet_z_value_and_wait");
                        if (set_magnet_z_value_and_wait_raw == NULL)
                        {
                            abort_loading++;
                            snprintf(error + strlen(error), 2048 - strlen(error), "Could not find: set_magnet_z_value_and_wait()\nin%s", argvp[0]);
                        }
                        set_rot_value_and_wait_raw = (int (*)(float rot))GetProcAddress(hinstLib, "_set_rot_value_and_wait");
                        if (set_rot_value_and_wait_raw == NULL)
                        {
                            abort_loading++;
                            snprintf(error + strlen(error), 2048 - strlen(error), "Could not find: set_rot_value_and_wait()\nin%s", argvp[0]);
                        }
                        set_motors_speed = (int (*)(float v))GetProcAddress(hinstLib, "_set_motors_speed");
                        if (set_motors_speed == NULL)
                        {
                            abort_loading++;
                            snprintf(error + strlen(error), 2048 - strlen(error), "Could not find: set_motors_speed()\nin%s", argvp[0]);
                        }
                        get_motors_speed = (float (*)(void))GetProcAddress(hinstLib, "_get_motors_speed");
                        if (get_motors_speed == NULL)
                        {
                            abort_loading++;
                            snprintf(error + strlen(error), 2048 - strlen(error), "Could not find: get_motors_speed()\nin%s", argvp[0]);
                        }
                    }
                }
                else
                {
                    abort_loading++;
                    snprintf(error + strlen(error), 2048 - strlen(error), "Could not find: _init_magnets_OK\nin%s", argvp[0]);
                }
            }
            else
            {
                abort_loading++;
                snprintf(error + strlen(error), 2048 - strlen(error), "Could not find plugins %s", argvp[0]);
            }
        }
        else
        {
            abort_loading++;
            snprintf(error + strlen(error), 2048 - strlen(error), "Your motor translation model :\n%s\n"
                     "Differs from your motor rotation model:\n%s"
                     "We cannot handle this situation, please change your config",
                     t_m->translation_motor_controller, r_m->rotation_motor_controller);
        }
        if (abort_loading == 0)
        {
            has_magnets_memory = (int (*)(void))GetProcAddress(hinstLib, "_has_magnets_memory");

            if (has_magnets_memory == NULL)  has_magnets_memory = dummy_has_magnets_memory;

            set_z_origin = (int (*)(float up, int dir))GetProcAddress(hinstLib, "_set_z_origin");

            if (set_z_origin == NULL)  set_z_origin = dummy_set_z_origin;

            request_set_rot_value_raw = (int (*)(int image_n, float rot,  unsigned long * t0))
                                        GetProcAddress(hinstLib, "_request_set_rot_value_raw");
            check_set_rot_request_answer  = (int (*)(char *answer, int im_n, float rot))
                                            GetProcAddress(hinstLib, "_check_set_rot_request_answer");
            request_set_magnet_z_value_raw = (int (*)(int image_n, float z, unsigned long * t0))
                                             GetProcAddress(hinstLib, "_request_set_magnet_z_value_raw");
            check_set_zmag_request_answer = (int (*)(char *answer, int im_n, float pos))
                                            GetProcAddress(hinstLib, "_check_set_zmag_request_answer");
            request_read_rot_value = (int (*)(int image_n, unsigned long * t0))
                                     GetProcAddress(hinstLib, "_request_read_rot_value");
            read_rot_raw_from_request_answer = (float (*)(char *answer, int im_n, float val, int *error))
                                               GetProcAddress(hinstLib, "_read_rot_raw_from_request_answer");
            request_read_magnet_z_value = (int (*)(int image_n, unsigned long * t0))
                                          GetProcAddress(hinstLib, "_request_read_magnet_z_value");
            read_zmag_raw_from_request_answer = (float (*)(char *answer, int im_n, float val, int *error, float * vacp))
                                                GetProcAddress(hinstLib, "_read_zmag_raw_from_request_answer");
            request_set_rot_ref_value = (int (*)(int image_n, float rot,  unsigned long * t0))
                                        GetProcAddress(hinstLib, "_request_set_rot_ref_value");
            check_set_rot_ref_request_answer = (int (*)(char *answer, int im_n, float rot))
                                               GetProcAddress(hinstLib, "_check_set_rot_ref_request_answer");
            request_set_zmag_ref_value = (int (*)(int image_n, float pos,  unsigned long * t0))
                                         GetProcAddress(hinstLib, "_request_set_zmag_ref_value");
            check_set_zmag_ref_request_answer = (int (*)(char *answer, int im_n, float pos))
                                                GetProcAddress(hinstLib, "_check_set_zmag_ref_request_answer");

            return 0;
        }

        win_printf("Aborting specific magnets device,\nloading encountered %d errors:\n%s\n"
                   "Using the dummy magnets device instead\n", abort_loading, error);
    }
# endif
# endif // DUMMY
    return select_dummy_magnets();
    return 0;
}

int dummy_has_magnets_memory(void)
{
    return 0;
}


int dummy_set_z_origin(float z, int dir)
{
  (void)z;
    (void)dir;
    return 0;
}

int dummy_init_magnets_OK(void)
{
    return 0;
}

int dummy_set_rot_value(float rot)
{
    //rot -= n_rot_offset;
    unsigned long dt;
    float tmp;

    if (rot_moving_time == 0)   // rotation has stopped
    {
        rot_moving_time = get_my_uclocks_per_sec() * fabs((rot + n_rot_offset - prev_mag_rot) / v_rota);
        last_rot_set = my_uclock();
        last_n_rota = prev_mag_rot;
        n_rota_inst = prev_mag_rot;
        n_rota = rot + n_rot_offset;
    }
    else
    {
        dt = my_uclock();

        if (last_rot_set + rot_moving_time < dt) // in fact rotating is finished
        {
            rot_moving_time = get_my_uclocks_per_sec() * fabs((rot + n_rot_offset - prev_mag_rot) / v_rota);
            last_rot_set = my_uclock();
            n_rota_inst = last_n_rota = prev_mag_rot;
            n_rota = rot + n_rot_offset;
        }
        else  // we are still rotating !
        {
            tmp = (n_rota - last_n_rota) * ((float)(dt - last_rot_set)) / rot_moving_time;
            tmp += last_n_rota; // this is where we are
            rot_moving_time = get_my_uclocks_per_sec() * fabs((rot + n_rot_offset - tmp) / v_rota);
            last_rot_set = my_uclock();
            n_rota_inst = last_n_rota = tmp;
            n_rota = rot + n_rot_offset;
        }
    }

# ifdef RT_MV_DEBUG
    RT_debug("roti=%d&%d\n", (int)(0.5 + 8000 * rot), (int)(0.5 + 8000 * rot));
#endif
    return 0;
}

int dummy_set_rot_ref_value(float rot)  /* in tour */
{
    n_rot_offset = rot;
    //  n_rota = rot;
# ifdef RT_MV_DEBUG
    RT_debug("rhome=%f\r", rot);
#endif
    return 0;
}

float dummy_read_rot_value(void)
{
    unsigned long dt;
    float tmp;
    //char buf[512];

    if (rot_moving_time == 0)   return n_rota_inst;   // magnets have stopped

    dt = my_uclock();

    if (last_rot_set + rot_moving_time < dt) // in fact translating is finished
    {
        rot_moving_time = 0;
        tmp = n_rota; // magnets have stopped
        n_rota_inst = n_rota;
    }
    else
    {
        tmp = (n_rota - last_n_rota) * ((float)(dt - last_rot_set)) / rot_moving_time;
        tmp += last_n_rota; // this is where we are
        n_rota_inst = tmp;
    }

    //snprintf(buf,512,"rot  read %g ",(tmp - n_rot_offset));
    //my_set_window_title(buf);
# ifdef RT_MV_DEBUG
    RT_debug("roti?\n\t < roti%d\n", (int)(n_rota_inst * 8000));
#endif
    return tmp - n_rot_offset;
}

int   dummy_set_magnet_z_value_old(float pos)
{
    unsigned long dt;
    float tmp;

    pos = (pos > absolute_maximum_zmag) ? absolute_maximum_zmag : pos;

    if (zmag_moving_time == 0)   // magnets have stopped
    {
        zmag_moving_time = get_my_uclocks_per_sec() * fabs((pos + n_magnet_offset - prev_zmag) / v_mag);
        last_zmag_set = my_uclock();
        n_magnet_z_inst = last_n_magnet_z = prev_zmag;
        n_magnet_z = pos + n_magnet_offset;
    }
    else
    {
        dt = my_uclock();

        if (last_zmag_set + zmag_moving_time < dt) // in fact translating is finished
        {
            zmag_moving_time = get_my_uclocks_per_sec() * fabs((pos + n_magnet_offset - prev_zmag) / v_mag);
            last_zmag_set = my_uclock();
            n_magnet_z_inst = last_n_magnet_z = n_magnet_z;
            n_magnet_z = pos + n_magnet_offset;
        }
        else  // we are still magnetating !
        {
            tmp = (n_magnet_z - last_n_magnet_z) * ((float)(dt - last_zmag_set)) / zmag_moving_time;
            tmp += last_n_magnet_z; // this is where we are
            zmag_moving_time = get_my_uclocks_per_sec() * fabs((pos + n_magnet_offset - tmp) / v_mag);
            last_zmag_set = my_uclock();
            n_magnet_z_inst = last_n_magnet_z = tmp;
            n_magnet_z = pos + n_magnet_offset;
        }
    }

# ifdef RT_MV_DEBUG
    RT_debug("zmagi=%d&%d\n", (int)(0.5 + 8000 * pos), (int)(0.5 + 8000 * pos));
#endif
    return 0;
}


int   dummy_set_magnet_z_value(float pos)
{
    unsigned long dt;
    float tmp, vcap;

    pos = (pos > absolute_maximum_zmag) ? absolute_maximum_zmag : pos;

    if (zmag_moving_time == 0)   // magnets have stopped
    {
        zmag_moving_time = get_my_uclocks_per_sec() * fabs((pos + n_magnet_offset - prev_zmag) / v_mag);
        last_zmag_set = my_uclock();
        n_magnet_z_inst = last_n_magnet_z = prev_zmag;
        n_magnet_z = pos + n_magnet_offset;
    }
    else
    {
        dt = my_uclock();

        if (last_zmag_set + zmag_moving_time < dt) // in fact translating is finished
        {
            zmag_moving_time = get_my_uclocks_per_sec() * fabs((pos + n_magnet_offset - prev_zmag) / v_mag);
            last_zmag_set = my_uclock();
            vcap = -12.5 * n_magnet_z;

            if (vcap < 0) vcap = 0;

            if (Vcap_min > 0 && vcap < Vcap_min)   n_magnet_z_inst = -0.08 * Vcap_min;
            else                                   n_magnet_z_inst = n_magnet_z;

            last_n_magnet_z = n_magnet_z_inst;
            n_magnet_z = pos + n_magnet_offset;
        }
        else  // we are still magnetating !
        {
            tmp = (n_magnet_z - last_n_magnet_z) * ((float)(dt - last_zmag_set)) / zmag_moving_time;
            tmp += last_n_magnet_z; // this is where we are
            zmag_moving_time = get_my_uclocks_per_sec() * fabs((pos + n_magnet_offset - tmp) / v_mag);
            last_zmag_set = my_uclock();
            vcap = -12.5 * tmp;

            if (vcap < 0) vcap = 0;

            if (Vcap_min > 0 && vcap < Vcap_min)   n_magnet_z_inst = -0.08 * Vcap_min;
            else                                   n_magnet_z_inst = tmp;

            last_n_magnet_z = n_magnet_z_inst;
            n_magnet_z = pos + n_magnet_offset;
        }
    }

# ifdef RT_MV_DEBUG
    RT_debug("zmagi=%d&%d\n", (int)(0.5 + 8000 * pos), (int)(0.5 + 8000 * pos));
#endif
    return 0;
}
float dummy_read_magnet_z_value_old(void)
{
    unsigned long dt;
    float tmp;
    //char buf[512];

    if (zmag_moving_time == 0)   return n_magnet_z_inst;  // magnets have stopped

    dt = my_uclock();

    if (last_zmag_set + zmag_moving_time < dt) // in fact translating is finished
    {
        zmag_moving_time = 0;
        tmp = n_magnet_z; // magnets have stopped
        last_n_magnet_z = n_magnet_z;
    }
    else
    {
        tmp = (n_magnet_z - last_n_magnet_z) * ((float)(dt - last_zmag_set)) / zmag_moving_time;
        tmp += last_n_magnet_z; // this is where we are
        n_magnet_z_inst = tmp;
    }

    //  snprintf(buf,512,"Zmag read %g ",tmp);
    //my_set_window_title(buf);
    n_magnet_z_inst = tmp;
# ifdef RT_MV_DEBUG
    RT_debug("zmagi?\n\tzmagi=%d\n", (int)(0.5 + 8000 * n_magnet_z_inst));
#endif
    return n_magnet_z_inst;
}


float dummy_read_magnet_z_value(void)
{
    unsigned long dt;
    float tmp, vcap;
    //char buf[512];



    if (zmag_moving_time == 0)
    {
# ifdef RT_MV_DEBUG
        RT_debug("zmagi?\n\t > zmagi=%d\n", (int)(n_magnet_z_inst * 8000));
#endif
        return n_magnet_z_inst;   // magnets have stopped
    }

    dt = my_uclock();

    if (last_zmag_set + zmag_moving_time < dt) // in fact translating is finished
    {
        zmag_moving_time = 0;
        vcap = -12.5 * n_magnet_z;

        if (vcap < 0) vcap = 0;

        if (Vcap_min > 0 && vcap < Vcap_min)   n_magnet_z_inst = -0.08 * Vcap_min;
        else                                   n_magnet_z_inst = n_magnet_z;

        last_n_magnet_z = n_magnet_z_inst; // magnets have stopped
    }
    else
    {
        tmp = (n_magnet_z - last_n_magnet_z) * ((float)(dt - last_zmag_set)) / zmag_moving_time;
        tmp += last_n_magnet_z; // this is where we are
        vcap = -12.5 * n_magnet_z;

        if (vcap < 0) vcap = 0;

        if (Vcap_min > 0 && vcap < Vcap_min)   n_magnet_z_inst = -0.08 * Vcap_min;
        else                                   n_magnet_z_inst = tmp;
    }

    //  snprintf(buf,512,"Zmag read %g ",tmp);
    //my_set_window_title(buf);
# ifdef RT_MV_DEBUG
    RT_debug("zmagi?\n\t > zmagi=%d\n", (int)(n_magnet_z_inst * 8000));
#endif
    return n_magnet_z_inst;
}

int dummy_read_magnet_z_value_and_status_old(float *z, float *vcap, int *limit, int *vcap_servo)
{

    unsigned long dt;
    float tmp;
    //char buf[512];


    if (zmag_moving_time == 0)
    {
        // magnets have stopped
        if (z) *z = n_magnet_z_inst;

        if (vcap) *vcap = 0;

        if (limit) *limit = 0;

        if (vcap_servo) *vcap_servo = 0;

        psleep(10);
        return 0;
    }
    dt = my_uclock();

    if (last_zmag_set + zmag_moving_time < dt) // in fact translating is finished
    {
        zmag_moving_time = 0;
        tmp = n_magnet_z; // magnets have stopped
        last_n_magnet_z = n_magnet_z;
    }
    else
    {
        tmp = (n_magnet_z - last_n_magnet_z) * ((float)(dt - last_zmag_set)) / zmag_moving_time;
        tmp += last_n_magnet_z; // this is where we are
        n_magnet_z_inst = tmp;
    }
    //  snprintf(buf,512,"Zmag read %g ",tmp);
    //my_set_window_title(buf);
    n_magnet_z_inst = tmp;

    if (z) *z = n_magnet_z_inst;

    if (vcap) *vcap = 0;

    if (limit) *limit = 0;

    if (vcap_servo) *vcap_servo = 0;

# ifdef RT_MV_DEBUG
    RT_debug("zmagi?\n\t > zmagi=%d\n", (int)(n_magnet_z_inst * 8000));
#endif
    psleep(10);
    return 0;
}


int dummy_read_magnet_z_value_and_status(float *z, float *vcap, int *limit, int *vcap_servo)
{
    unsigned long dt;
    float tmp, v_cap;
    //char buf[512];


    dt = my_uclock();
    if ((zmag_moving_time == 0) || (last_zmag_set + zmag_moving_time < dt)) // in fact translating is finished
    {
        zmag_moving_time = 0;
        v_cap = -(12.5 * n_magnet_z);

        if (v_cap < 0) v_cap = 0;

        if (Vcap_min > 0 && v_cap < Vcap_min)  n_magnet_z_inst = -(0.08 * Vcap_min);
        else                              n_magnet_z_inst = n_magnet_z;

        last_n_magnet_z = n_magnet_z_inst;
    }
    else
    {
        tmp = (n_magnet_z - last_n_magnet_z) * ((float)(dt - last_zmag_set)) / zmag_moving_time;
        tmp += last_n_magnet_z; // this is where we are

        if (tmp > 0) tmp = 0.001;

        if (tmp < -28) tmp = -28.001;

        v_cap = -12.5 * n_magnet_z;

        if (v_cap < 0) v_cap = 0;

        if (Vcap_min > 0 && v_cap < Vcap_min)  n_magnet_z_inst = -(0.08 * Vcap_min);
        else                              n_magnet_z_inst = tmp;
    }

    //  snprintf(buf,512,"Zmag read %g ",tmp);
    //my_set_window_title(buf);
    //n_magnet_z_inst = tmp;
    if (z) *z = n_magnet_z_inst;

    if (vcap)
    {
        if (n_magnet_z_inst <= 0 && n_magnet_z_inst > -0.8)    *vcap = 12.5 * (-n_magnet_z_inst);
        else *vcap = 0;

        if (*vcap < 0) *vcap = 0;
    }

    if (limit)
    {
        if (n_magnet_z_inst < -28.0)                     Neg_limit = 1;
        else if (Neg_limit && n_magnet_z_inst > -27.6)   Neg_limit = 0;

        if (n_magnet_z_inst > 0)                         Pos_limit = 1;
        else if (Pos_limit && n_magnet_z_inst < -0.4)    Pos_limit = 0;

        if (Neg_limit)       *limit = -1;
        else if (Pos_limit)  *limit = 1;
        else                 *limit = 0;
    }

    if (vcap_servo)
    {
        v_cap = -12.5 * n_magnet_z_inst;

        if (v_cap < 0) v_cap = 0;

        *vcap_servo = ((Vcap_min > 0) && (fabs(v_cap - Vcap_min) < 0.005)) ? 1 : 0;
    }
# ifdef RT_MV_DEBUG
    RT_debug("zmagi?\n\t > zmagi=%d\n", (int)(n_magnet_z_inst * 8000));
#endif
    return 0;
}

float dummy_get_Vcap_min(void)
{
    return Vcap_min;
}
int dummy_set_Vcap_min(float v)
{
    Vcap_min = v;
    return 0;
}

int dummy_set_motors_speed(float v)
{
    v_rota = v;

# ifdef RT_MV_DEBUG
    RT_debug("rpid7=%d\n", (int)(0.5 + 8 * fabs(v)));
#endif
    return 0;
}
float  dummy_get_motors_speed(void)
{
# ifdef RT_MV_DEBUG
    RT_debug("rpid7?\n\trpid7=%f\n", v_rota);
#endif
    return v_rota;
}




int dummy_set_zmag_motor_pid(int n, int val)
{
    if (n < 0 || n > 9) return 1;

# ifdef RT_MV_DEBUG
    RT_debug("zpid%d=%d\n", n, val);
#endif
    zmag_pid[n] = val;
    return 0;
}
int  dummy_get_zmag_motor_pid(int n)
{
# ifdef RT_MV_DEBUG
    RT_debug("zpid%d?\n", n);
#endif

    if (n < 0 || n > 9) return -1;

    return zmag_pid[n];
}


int dummy_set_rotation_motor_pid(int n, int val)
{
    if (n < 0 || n > 9) return 1;

    //my_set_window_title("                                                                          pid rot %d ->%d!",n,val);
# ifdef RT_MV_DEBUG
    RT_debug("rpid%d=%d\n", n, val);
#endif
    rot_pid[n] = val;
    return 0;
}
int  dummy_get_rotation_motor_pid(int n)
{
# ifdef RT_MV_DEBUG
    RT_debug("rpid%d?\n", n);
#endif

    if (n < 0 || n > 9) return -1;

    return rot_pid[n];
}


int dummy_set_zmag_motor_speed(float v)
{
    v_mag = v;
# ifdef RT_MV_DEBUG
    RT_debug("zpid7=%d\n", (int)(0.5 + 8 * fabs(v)));
#endif
    return 0;
}
float  dummy_get_zmag_motor_speed(void)
{
# ifdef RT_MV_DEBUG
    RT_debug("Zpid7?\n\tZpid7=%f\n", v_mag);
#endif
    return v_mag;
}

int dummy_set_magnet_z_ref_value(float z)  /* in mm */
{
    n_magnet_offset = z;
    n_magnet_z = z;
# ifdef RT_MV_DEBUG
    RT_debug("zhome=%f\r", z);
#endif
    return 0;
}
//this function is not related to a physical measurement (no call to dll or serial)
//
int set_magnet_z_factor_value(float fac)
{
  static int i = 0;
  (void)fac;
#ifdef ZMAG_FACTOR
  //if (i!=0) win_printf_OK("test ztranslation");//, Pico_param.translation_motor_param.zmag_factor);
  Pico_param.translation_motor_param.zmag_factor = fac;
  win_printf_OK("zmag offset %f \n fac %f \n zmagcontact %f",
  Pico_param.translation_motor_param.zmag_offset,Pico_param.translation_motor_param.zmag_factor,
  Pico_param.magnet_param.zmag_contact_sample);
#endif
  i++;
  return 0;
}

int    dummy_set_magnet_z_value_and_wait(float pos)
{
    pos = (pos > absolute_maximum_zmag) ? absolute_maximum_zmag : pos;
    dummy_set_magnet_z_value(pos);

    if (zmag_moving_time == 0)   return 0;    // magnets have stopped

    while (last_zmag_set + zmag_moving_time > my_uclock());

    zmag_moving_time = 0;
    last_n_magnet_z = n_magnet_z;
    return 0;
}
int    dummy_set_rot_value_and_wait(float rot)
{
    dummy_set_rot_value(rot);

    if (rot_moving_time == 0)   return 0; // magnets have stopped

    while (last_rot_set + rot_moving_time > my_uclock());

    rot_moving_time = 0;
    last_n_rota = n_rota;
    return 0;
}




float zmag_to_force(int nb, float zmag)
{
    float z, tmp;
    z = zmag - Pico_param.magnet_param.zmag_contact_sample; // z is negaative

    if (fabs(z) > 5) return 0;

    tmp = Pico_param.magnet_param.a1[nb] * z;

    if (Pico_param.magnet_param.nd[nb] > 2)
        tmp -= Pico_param.magnet_param.a2[nb] * z * z;

    if (Pico_param.magnet_param.nd[nb] > 3)
        tmp += Pico_param.magnet_param.a3[nb] * z * z * z;

    if (Pico_param.magnet_param.nd[nb] > 4)
        tmp -= Pico_param.magnet_param.a4[nb] * z * z * z * z;

    tmp = exp(tmp);
    return (Pico_param.magnet_param.f0[nb] * tmp);
}


float force_to_tau_x_in_fr(float force, int bead_type)
{
    float l_alpha, l_x, m_x, l_kx, l_taux;
    double T = 298;

    (void)bead_type;
    l_alpha = force;
    l_alpha *= (Pico_param.dna_molecule.dsxi) / (1.38e-2 * T);
    l_x = extension_versus_alpha(l_alpha);
    m_x = Pico_param.dna_molecule.dsDNA_molecule_extension * l_x;

    if (l_x > 0.1)       l_kx = force / m_x; // pN/microns
    else
    {
        l_kx = marko_siggia_improved_derivate(l_x);
        l_kx *= (1.38e-2 * T) / (Pico_param.dna_molecule.dsxi);
        l_kx /= Pico_param.dna_molecule.dsDNA_molecule_extension;
    }

    l_taux = 6 * M_PI * 1e-3 * Pico_param.bead_param.radius;

    if (l_kx > 0) l_taux /= l_kx;
    else l_taux = 0;

    l_taux *= Pico_param.camera_param.camera_frequency_in_Hz;
    return (l_taux > 0) ? l_taux : 1;
}

float convert_zmag_to_force(float zmag, float rot, int nb)
{
    float z, tmp;
    float x;
    double T = 298, dt, tmpd;//, rot, zmag;
    char buf[1024] = {0};

    //zmag = read_magnet_z_value();
    z = zmag - Pico_param.magnet_param.zmag_contact_sample;
    nb = (nb < Pico_param.magnet_param.nb) ? nb : 0;
    nb = (nb < 0) ? 0 : nb;
    tmp = Pico_param.magnet_param.a1[nb] * z;

    if (fabs(z) > 5) return 0;

    if (Pico_param.magnet_param.nd[nb] > 2)
        tmp -= Pico_param.magnet_param.a2[nb] * z * z;

    if (Pico_param.magnet_param.nd[nb] > 3)
        tmp += Pico_param.magnet_param.a3[nb] * z * z * z;

    if (Pico_param.magnet_param.nd[nb] > 4)
        tmp -= Pico_param.magnet_param.a4[nb] * z * z * z * z;

    tmp = exp(tmp);
    applied_force = Pico_param.magnet_param.f0[nb] * tmp;

    alpha = applied_force;
    alpha *= (Pico_param.dna_molecule.dsxi) / (1.38e-2 * T);
    x = extension_versus_alpha(alpha);
    mean_extension = Pico_param.dna_molecule.dsDNA_molecule_extension * x;

    if (x > 0.1)
    {
        kx = applied_force / mean_extension; // pN/microns
        ky = applied_force / (mean_extension + Pico_param.bead_param.radius); // pN/microns
    }
    else
    {
        kx = marko_siggia_improved_derivate(x);
        kx *= (1.38e-2 * T) / (Pico_param.dna_molecule.dsxi);
        ky = kx;
        kx /= Pico_param.dna_molecule.dsDNA_molecule_extension;
        //      ky /= (mean_extension + Pico_param.bead_param.radius);  // pN/microns
        ky /= (Pico_param.dna_molecule.dsDNA_molecule_extension
               + Pico_param.bead_param.radius);  // pN/microns
    }

    //rot = read_rot_value();
    mean_extension -= (rot * rot) / 100;

    if (mean_extension < 0)
    {
        snprintf(buf, 1024, "mean_extension set to 0! rot %g zmag %g dsDNA %g",
                 rot, zmag, Pico_param.dna_molecule.dsDNA_molecule_extension);
        //my_set_window_title(buf);
    }

    mean_extension = (mean_extension < 0) ? 0 : mean_extension;


    kz = marko_siggia_improved_derivate(x);
    kz *= (1.38e-2 * T) / (Pico_param.dna_molecule.dsxi);
    kz /= Pico_param.dna_molecule.dsDNA_molecule_extension;

    tauz = tauy = taux = 6 * M_PI * 1e-3 * Pico_param.bead_param.radius;

    if (kx > 0) taux /= kx;
    else taux = 0;

    if (ky > 0) tauy /= ky;
    else tauy = 0;

    if (kz > 0) tauz /= kz;
    else tauz = 0;

    // oversampling at OVER_SAMPLING x the camera rate
    dt = ((double)1) / (OVER_SAMPLING * Pico_param.camera_param.camera_frequency_in_Hz);

    betax = exp(-dt / taux);
    betay = exp(-dt / tauy);
    betaz = exp(-dt / tauz);

    tmpd = 1;
    tmpd -= betax * betax;
    xnoise = sqrt((tmpd * 1.38e-5 * T) / kx); // en microns

    tmpd = 1;
    tmpd -= betay * betay;
    ynoise = sqrt((tmpd * 1.38e-5 * T) / ky); // en microns

    tmpd = 1;
    tmpd -= betaz * betaz;
    znoise = sqrt((tmpd * 1.38e-5 * T) / kz); // en microns


    return applied_force;
}


float convert_only_zmag_to_force(float zmag, int nb)
{
    float z, tmp;

    z = zmag - Pico_param.magnet_param.zmag_contact_sample;
    nb = (nb < Pico_param.magnet_param.nb) ? nb : 0;
    nb = (nb < 0) ? 0 : nb;
    tmp = Pico_param.magnet_param.a1[nb] * z;

    if (fabs(z) > 5) return 0;

    if (Pico_param.magnet_param.nd[nb] > 2)
        tmp -= Pico_param.magnet_param.a2[nb] * z * z;

    if (Pico_param.magnet_param.nd[nb] > 3)
        tmp += Pico_param.magnet_param.a3[nb] * z * z * z;

    if (Pico_param.magnet_param.nd[nb] > 4)
        tmp -= Pico_param.magnet_param.a4[nb] * z * z * z * z;

    tmp = exp(tmp);
    tmp = Pico_param.magnet_param.f0[nb] * tmp;

    return tmp;
}



float convert_force_to_zmag(float force,  int nb)
{
    float tmp;
    double a, b, c, D, z1, z2;


    nb = (nb < Pico_param.magnet_param.nb) ? nb : 0;
    nb = (nb < 0) ? 0 : nb;
    tmp = force / Pico_param.magnet_param.f0[nb];
    c = log(tmp);
    a = Pico_param.magnet_param.a2[nb];
    b = Pico_param.magnet_param.a1[nb];

    if (a == 0 && b == 0) return -25;

    if (a == 0)  return (Pico_param.magnet_param.zmag_contact_sample + (c / b));

    D = b * b - 4 * a * c;

    if (D < 0) return -25;

    D = sqrt(D);
    z1 = (b - D) / (2 * a);
    z2 = (b + D) / (2 * a);

    if (z2 >= 0 && z1 < 0) return (Pico_param.magnet_param.zmag_contact_sample + z1);

    if (z1 >= 0 && z1 < 0) return (Pico_param.magnet_param.zmag_contact_sample + z2);

    if (z1 <= z2 && z2 <= 0) return (Pico_param.magnet_param.zmag_contact_sample + z2);

    if (z2 <= z1 && z2 <= 0) return (Pico_param.magnet_param.zmag_contact_sample + z1);

    return -25;
}

int set_rot_value(float r)
{
    n_rota = Pico_param.rotation_motor_param.gear_ratio * r;
    set_rot_value_raw(n_rota);
    return 0;
}

int set_rot_value_and_wait(float r)
{
    n_rota = Pico_param.rotation_motor_param.gear_ratio * r;
    set_rot_value_and_wait_raw(n_rota);
    return 0;
}

float   read_rot_value(void)
{
    float r;

    if (Pico_param.rotation_motor_param.gear_ratio != 0)
        n_rota_inst = r = read_rot_value_raw() / Pico_param.rotation_motor_param.gear_ratio;
    else n_rota_inst = r = read_rot_value_raw();

    //convert_zmag_to_force(n_magnet_z_inst,r,0);
    return r;
}

float   read_magnet_z_value(void)
{
    float z;

    if (Pico_param.translation_motor_param.translation_motor_inverted <= 0)
        z =  Pico_param.translation_motor_param.translation_max_pos - read_magnet_z_value_raw();
    else z =  read_magnet_z_value_raw();

    //convert_zmag_to_force(z,n_rota_inst,0);
    return z;
}


int read_magnet_z_value_and_status(float *z, float *vcap, int *limit, int *vcap_servo)
{
    float zl;
    int ret = 0;
    if (read_magnet_z_value_and_status_raw)
    {
      ret = read_magnet_z_value_and_status_raw(&zl, vcap, limit, vcap_servo);
    }
    else
    {
        zl = read_magnet_z_value_raw();

        if (vcap) vcap = 0;

        if (limit) limit = 0;

        if (vcap_servo) vcap_servo = 0;
    }

    if (Pico_param.translation_motor_param.translation_motor_inverted <= 0)
        *z = Pico_param.translation_motor_param.translation_max_pos - zl;
    else *z =  zl;

    //convert_zmag_to_force(z,n_rota_inst,0);
    return ret;
}





int set_magnet_z_value(float z)
{
    if (Pico_param.translation_motor_param.translation_motor_inverted <= 0)
    {
        set_magnet_z_value_raw(Pico_param.translation_motor_param.translation_max_pos - z);
        n_magnet_z = z;
    }
    else set_magnet_z_value_raw(n_magnet_z = z);

    return 0;
}

int set_magnet_z_value_and_wait(float z)
{
    if (Pico_param.translation_motor_param.translation_motor_inverted <= 0)
    {
        set_magnet_z_value_and_wait_raw(Pico_param.translation_motor_param.translation_max_pos - z);
        n_magnet_z = z;
    }
    else set_magnet_z_value_and_wait_raw(n_magnet_z = z);

    return 0;
}

int go_and_dump_z_magnet(float z)
{
    //if (n_magnet_z == z) return 0;
    set_magnet_z_value(z);
    return 0;
    //return dump_to_specific_log_file_with_time(CURRENT_LOG_FILE,"Magnet Z changed auto to %g\n",n_magnet_z);
}
int    go_wait_and_dump_z_magnet(float zmag)
{
    //if (n_magnet_z == zmag)     return 0;
    set_magnet_z_value_and_wait(zmag);
    return 0;
    //return dump_to_specific_log_file_with_time(CURRENT_LOG_FILE,"Magnet Z changed auto to %g\n",n_magnet_z);
}

int    go_and_dump_rot(float r)
{
    set_rot_value(r);
    return 0;
    //return dump_to_specific_log_file_with_time(CURRENT_LOG_FILE,"Rotation number auto changed %g\n",n_rota);
}
int go_wait_and_dump_rot(float r)
{
    set_rot_value_and_wait(r);
    return 0;
    //return dump_to_specific_log_file_with_time(CURRENT_LOG_FILE,"Rotation number auto changed %g\n",n_rota);
}


int go_wait_and_dump_log_specific_rot(float r, char *log)
{
  (void)log;
    set_rot_value_and_wait(r);
    //dump_to_specific_log_file_only(log,"Rotation number auto changed %g\n",n_rota);
    return 0;
}

int    go_wait_and_dump_log_specific_z_magnet(float zmag, char *log)
{
  (void)log;
    //if (n_magnet_z == zmag)     return 0;
    set_magnet_z_value_and_wait(zmag);
    //dump_to_specific_log_file_only(log,"Magnet Z changed auto to %g\n",n_magnet_z);
    return 0;
}




int  request_set_magnet_z_value(int im, float z, unsigned long *t0)
{
    int ret;

    if (Pico_param.translation_motor_param.translation_motor_inverted <= 0)
    {
        n_magnet_z = z;
        ret = request_set_magnet_z_value_raw(im, Pico_param.translation_motor_param.translation_max_pos - z, t0);
    }
    else ret = request_set_magnet_z_value_raw(im, n_magnet_z = z, t0);

    return ret;
}



int  request_set_rot_value(int im, float r, unsigned long *t0)
{
    return request_set_rot_value_raw(im, n_rota = r, t0);
}


float read_zmag_from_request_answer(char *answer, int im_n, float val, int *error, float *vacp)
{
    float z;

    if (Pico_param.translation_motor_param.translation_motor_inverted <= 0)
        z = Pico_param.translation_motor_param.translation_max_pos
            - read_zmag_raw_from_request_answer(answer, im_n, val, error, vacp);
    else z = read_zmag_raw_from_request_answer(answer, im_n, val, error, vacp);

    return z;
}

float read_rot_from_request_answer(char *answer, int im_n, float val, int *error)
{
    float r;
    n_rota_inst = r = read_rot_raw_from_request_answer(answer, im_n, val, error);
    return r;
}



int mag_z(void)
{
    int index, i;
    index = RETRIEVE_MENU_INDEX;

    if (updating_menu_state != 0)
    {
        if ((fabs(prev_zmag - (((float)index) / 1000))) < 0.002) // n_magnet_z
            active_menu->flags |=  D_SELECTED;
        else active_menu->flags &= ~D_SELECTED;

        return D_O_K;
    }

    i = immediate_next_available_action(MV_ZMAG_ABS, ((float)index) / 1000);

    if (i < 0)    win_printf_OK("Could not add pending action!");

    //go_and_dump_z_magnet( ((float)index) /100);
    //write_mag_value_to_disk();
    //do_update_menu();
    return 0;
}



int mag_z_eforce(void)
{
    int index, i;
    index = RETRIEVE_MENU_INDEX;
    float pf, z;

    pf = convert_only_zmag_to_force(prev_zmag, 0);
    if (updating_menu_state != 0)
    {
        if ((fabs(pf - (((float)index) / 100))) < 0.02) // n_magnet_z
            active_menu->flags |=  D_SELECTED;
        else active_menu->flags &= ~D_SELECTED;

        return D_O_K;
    }
    z = convert_force_to_zmag(((float)index)/100,  0);
    z = (z < -20) ? -20 : z;

    i = immediate_next_available_action(MV_ZMAG_ABS, z);

    if (i < 0)    win_printf_OK("Could not add pending action!");
    return 0;
}


MENU *preset_force_menu(void)
{
    static MENU mn[32] = {0};

    add_item_to_menu(mn, "0.01 pN", mag_z_eforce, NULL, MENU_INDEX(1), NULL);
    add_item_to_menu(mn, "0.1 pN", mag_z_eforce, NULL, MENU_INDEX(10), NULL);
    add_item_to_menu(mn, "0.3 pN", mag_z_eforce, NULL, MENU_INDEX(30), NULL);
    add_item_to_menu(mn, "0.7 pN", mag_z_eforce, NULL, MENU_INDEX(50), NULL);
    add_item_to_menu(mn, "1 pN", mag_z_eforce, NULL, MENU_INDEX(100), NULL);
    add_item_to_menu(mn, "2 pN", mag_z_eforce, NULL, MENU_INDEX(200), NULL);
    add_item_to_menu(mn, "3 pN", mag_z_eforce, NULL, MENU_INDEX(300), NULL);
    add_item_to_menu(mn, "4 pN", mag_z_eforce, NULL, MENU_INDEX(400), NULL);
    add_item_to_menu(mn, "5 pN", mag_z_eforce, NULL, MENU_INDEX(500), NULL);
    add_item_to_menu(mn, "6 pN", mag_z_eforce, NULL, MENU_INDEX(600), NULL);
    add_item_to_menu(mn, "7 pN", mag_z_eforce, NULL, MENU_INDEX(700), NULL);
    add_item_to_menu(mn, "8 pN", mag_z_eforce, NULL, MENU_INDEX(800), NULL);
    add_item_to_menu(mn, "9 pN", mag_z_eforce, NULL, MENU_INDEX(900), NULL);
    add_item_to_menu(mn, "10 pN", mag_z_eforce, NULL, MENU_INDEX(1000), NULL);
    add_item_to_menu(mn, "11 pN", mag_z_eforce, NULL, MENU_INDEX(1100), NULL);
    add_item_to_menu(mn, "12 pN", mag_z_eforce, NULL, MENU_INDEX(1200), NULL);
    add_item_to_menu(mn, "13 pN", mag_z_eforce, NULL, MENU_INDEX(1300), NULL);
    add_item_to_menu(mn, "14 pN", mag_z_eforce, NULL, MENU_INDEX(1400), NULL);
    add_item_to_menu(mn, "15 pN", mag_z_eforce, NULL, MENU_INDEX(1500), NULL);
    add_item_to_menu(mn, "16 pN", mag_z_eforce, NULL, MENU_INDEX(1600), NULL);
    add_item_to_menu(mn, "17 pN", mag_z_eforce, NULL, MENU_INDEX(1700), NULL);
    add_item_to_menu(mn, "18 pN", mag_z_eforce, NULL, MENU_INDEX(1800), NULL);
    add_item_to_menu(mn, "19 pN", mag_z_eforce, NULL, MENU_INDEX(1900), NULL);
    add_item_to_menu(mn, "20 pN", mag_z_eforce, NULL, MENU_INDEX(2000), NULL);
    add_item_to_menu(mn, "22 pN", mag_z_eforce, NULL, MENU_INDEX(2200), NULL);
    add_item_to_menu(mn, "25 pN", mag_z_eforce, NULL, MENU_INDEX(2500), NULL);
    return mn;
}


int switch_motors_pid(int state)
{
    static int l_state = -1;

    if (track_info == NULL) return 0;

    //return 0;  // ldebug
    if (l_state != state)
    {
        if (set_zmag_motor_pid != NULL)
        {
            immediate_next_available_action_spare(CHG_ZMAG_PID, (float)state, 8);
            psleep(50);
        }

        if (set_rotation_motor_pid != NULL)
            immediate_next_available_action_spare(CHG_ROT_PID, (float)state, 8);

        l_state = state;
    }

    return 0;
}


int grab_rot_pid_param(int param)
{
  int i, im, done, rep;

    im = track_info->imi[track_info->c_i];
    i = (im > last_rot_pid.acknowledge) ? im : last_rot_pid.acknowledge + 1;
    last_rot_pid.proceeded_ask_bo_be_clear = i;
    for (done = 1, i = im; done == 1 && i < im + 100;)
      {  // we wait proceeded_RT to go down
        i = track_info->imi[(volatile int)track_info->c_i];
        done = (volatile int)last_rot_pid.proceeded_RT;
        my_set_window_title("waiting proceeded_RT to go down im %d state %d!", i - im, done);
    }
    im += 2;
    i = fill_next_available_action_with_spare(im, READ_ROT_PID, 0, param);
    rep = find_action_to_be_done_by_type(READ_ROT_PID);
    if (i < 0 || rep < 0)
    {
      i = fill_next_available_action_with_spare(im, READ_ROT_PID, 0, param);
      if (i < 0)
	{
	  win_printf("cannot fill next action");
	  return -1;
	}
    }
    rep = find_action_to_be_done_by_type(READ_ROT_PID);
    //action_pending[i].spare = param;
    for (done = 0, i = im; done == 0 && i < im + 100 && rep >= 0;)
    {
        i = track_info->imi[(volatile int)track_info->c_i];
        done = (volatile int)last_rot_pid.proceeded_RT;
        my_set_window_title("read rot pid im %d state %d!", i - im, done);
    }

    if (rep < 0 || i >= im + 100)
    {
      return win_printf("could not read param %d %s", param,(rep < 0)?"No action in queue":"too long");
        return -2;
    }

    //last_rot_pid.proceeded = 2;
    return (int)last_rot_pid.value;
}


int chg_rot_pid_param(int param, int value)
{
  int i, im, done, rep;

    im = track_info->imi[track_info->c_i];
    i = (im > last_rot_pid.acknowledge) ? im : last_rot_pid.acknowledge + 1;
    last_rot_pid.proceeded_ask_bo_be_clear = i;
    for (done = 1, i = im; done == 1 && i < im + 100;)
      {  // we wait proceeded_RT to go down
        i = track_info->imi[(volatile int)track_info->c_i];
        done = (volatile int)last_rot_pid.proceeded_RT;
        my_set_window_title("waiting proceeded_RT to go down im %d state %d!", i - im, done);
    }
    im += 2;

    i = fill_next_available_action_with_spare(im, CHG_ROT_PID, value, param);
    rep = find_action_to_be_done_by_type(CHG_ROT_PID);
    if (i < 0 || rep < 0)
    {
      i = fill_next_available_action_with_spare(im, CHG_ROT_PID, value, param);
      if (i < 0)
	{
	  win_printf("cannot fill next action");
	  return -1;
	}
    }
    rep = find_action_to_be_done_by_type(CHG_ROT_PID);

    //action_pending[i].spare = param;
    for (done = 0, i = im; done == 0 && i < im + 100 && rep >= 0;)
    {
        i = track_info->imi[(volatile int)track_info->c_i];
        done = (volatile int)last_rot_pid.proceeded_RT;
        my_set_window_title("set rot pid im %d state %d!", i - im, done);
    }

    if (rep < 0 || i >= im + 100)
    {
        return win_printf("could not read param %d", param);
        return -2;
    }

    //last_rot_pid.proceeded = 2;
    return (int)last_rot_pid.value;
}



int set_rot_pid(void)
{
    int i;
    int R_P, R_I, R_D, Rv, Rpwmmax, Racc, Ron, Bon;
    int R_Pc, R_Ic, R_Dc, Rvc, Rpwmmaxc, Raccc, Ronc, Bonc;

    if (updating_menu_state != 0)   return D_O_K;

    if (set_rotation_motor_pid == NULL || get_rotation_motor_pid == NULL)
        return win_printf_OK("No PID setting available on Rot");

    R_Pc = R_P = grab_rot_pid_param(0);

    if (R_P < 0) return D_O_K;

    R_Ic = R_I = grab_rot_pid_param(1);

    if (R_I < 0) return D_O_K;

    R_Dc = R_D = grab_rot_pid_param(2);

    if (R_D < 0) return D_O_K;

    Rpwmmaxc = Rpwmmax = grab_rot_pid_param(3);

    if (Rpwmmax < 0) return D_O_K;

    Raccc = Racc = grab_rot_pid_param(5);

    if (Racc < 0) return D_O_K;

    Rvc = Rv = grab_rot_pid_param(7);

    if (Rv < 0) return D_O_K;

    Ronc = Ron = grab_rot_pid_param(8);

    if (Ron < 0) return D_O_K;

    Bonc = Bon = grab_zmag_pid_param(9);

    if (Bon < 0) return D_O_K;


    i = win_scanf("Rot PID parameters setting\n"
                  "Entering wrong parameters will produce\n"
                  "motor vibration and heating! \n"
                  "Proportional (default 6)% 6d\n"
                  "Integral (default 3) %6d\n"
                  "Derivative (default 12)%6d\n"
                  "Max PWM (default 120 max 255) %6d\n"
                  "Rot acceletation (default 4) %6d\n"
                  "Max speed (default 40) %6d\n"
                  "Rot PID (%R->Off)   (%r->On)\n"
                  "General brake (%R->Off)   (%r->On)\n"
                  , &R_P, &R_I, &R_D, &Rpwmmax, &Racc, &Rv, &Ron, &Bon);

    if (i == WIN_CANCEL) return OFF;

    if (R_P != R_Pc || R_I != R_Ic || R_D != R_Dc || Rv != Rvc || Rpwmmax != Rpwmmaxc
            || Racc != Raccc || Ron != Ronc || Bon != Bonc)
    {
        i = chg_rot_pid_param(8, 0); // we switch off PID

        if (i < 0) return win_printf_OK("Rot PID problem!");

        if (R_P != R_Pc)
        {
            i = chg_rot_pid_param(0, R_P);

            if (i < 0) return win_printf_OK("Rot PID problem!");
        }

        if (R_I != R_Ic)
        {
            i = chg_rot_pid_param(1, R_I);

            if (i < 0) return win_printf_OK("Rot PID problem!");
        }

        if (R_D != R_Dc)
        {
            i = chg_rot_pid_param(2, R_D);

            if (i < 0) return win_printf_OK("Rot PID problem!");
        }

        if (Rv != Rvc)
        {
            i = chg_rot_pid_param(7, Rv);

            if (i < 0) return win_printf_OK("Rot PID problem!");
        }

        if (Rpwmmax != Rpwmmaxc)
        {
            i = chg_rot_pid_param(3, Rpwmmax);

            if (i < 0) return win_printf_OK("Rot PID problem!");
        }

        if (Racc != Raccc)
        {
            i = chg_rot_pid_param(5, Racc);

            if (i < 0) return win_printf_OK("Rot PID problem!");
        }

        i = chg_rot_pid_param(8, Ron); // we switch back PID

        if (i < 0) return win_printf_OK("Rot PID problem!");

        if (Bon != Bonc)
        {
            i = chg_zmag_pid_param(9, Bon);

            if (i < 0) return win_printf_OK("Zmag PID problem!");
        }
    }

    if (Bon != 0)
    {
        win_printf("Brake is ON, both motors will not work\n"
                   "neither the temperature control!");
    }

    if (Ron == 0)
    {
        win_printf("Rot PID is OFF Rot motor will not move!");
    }

    return D_O_K;
}


int grab_zmag_pid_param(int param)
{
  int i, im, done, rep;

    im = track_info->imi[track_info->c_i];
    i = (im > last_zmag_pid.acknowledge) ? im : last_zmag_pid.acknowledge + 1;
    last_zmag_pid.proceeded_ask_bo_be_clear = i;
    for (done = 1, i = im; done == 1 && i < im + 100;)
      {  // we wait proceeded_RT to go down
        i = track_info->imi[(volatile int)track_info->c_i];
        done = (volatile int)last_zmag_pid.proceeded_RT;
        my_set_window_title("waiting proceeded_RT to go down im %d state %d!", i - im, done);
    }

    im += 2;

    i = fill_next_available_action_with_spare(im, READ_ZMAG_PID, 0, param);
    rep = find_action_to_be_done_by_type(READ_ZMAG_PID);
    if (i < 0 || rep < 0)
    {
      i = fill_next_available_action_with_spare(im, READ_ZMAG_PID, 0, param);
      if (i < 0)
	{
	  win_printf("cannot fill next action");
	  return -1;
	}
    }
    rep = find_action_to_be_done_by_type(READ_ZMAG_PID);

    //action_pending[i].spare = param;
    for (done = 0, i = im; done == 0 && i < im + 100 && rep >= 0;)
    {
        i = track_info->imi[(volatile int)track_info->c_i];
        done = (volatile int)last_zmag_pid.proceeded_RT;
        my_set_window_title("Read Zmag pid im %d state %d!", i - im, done);
    }

    if (rep < 0 || i >= im + 100)
    {
        return win_printf("could not read param %d", param);
        return -2;
    }

    //last_zmag_pid.proceeded = 2;
    return (int)last_zmag_pid.value;
}


int chg_zmag_pid_param(int param, int value)
{
  int i, im, done, rep;

    im = track_info->imi[track_info->c_i];
    i = (im > last_zmag_pid.acknowledge) ? im : last_zmag_pid.acknowledge + 1;
    last_zmag_pid.proceeded_ask_bo_be_clear = i;
    for (done = 1, i = im; done == 1 && i < im + 100;)
      {  // we wait proceeded_RT to go down
        i = track_info->imi[(volatile int)track_info->c_i];
        done = (volatile int)last_zmag_pid.proceeded_RT;
        my_set_window_title("waiting proceeded_RT to go down im %d state %d!", i - im, done);
    }

    im += 2;

    i = fill_next_available_action_with_spare(im, CHG_ZMAG_PID, value, param);
    rep = find_action_to_be_done_by_type(CHG_ZMAG_PID);
    if (i < 0 || rep < 0)
    {
      i = fill_next_available_action_with_spare(im, CHG_ZMAG_PID, value, param);
      if (i < 0)
	{
	  win_printf("cannot fill next action");
	  return -1;
	}
    }
    rep = find_action_to_be_done_by_type(CHG_ZMAG_PID);

    //action_pending[i].spare = param;
    for (done = 0, i = im; done == 0 && i < im + 100 && rep >= 0;)
    {
        i = track_info->imi[(volatile int)track_info->c_i];
        done = (volatile int)last_zmag_pid.proceeded_RT;
        my_set_window_title("Set Zmag pid im %d state %d!", i - im, done);
    }

    if (rep < 0 || i >= im + 100)
    {
        return win_printf("could not read param %d", param);
        return -2;
    }

    //last_zmag_pid.proceeded = 2;
    return (int)last_zmag_pid.value;
}


int set_zmag_pid(void)
{
    int i;
    int Z_P, Z_I, Z_D, Zv, Zpwmmax, Zacc, Zon, Bon;
    int Z_Pc, Z_Ic, Z_Dc, Zvc, Zpwmmaxc, Zaccc, Zonc, Bonc;

    if (updating_menu_state != 0)   return D_O_K;

    if (set_zmag_motor_pid == NULL || get_zmag_motor_pid == NULL)
        return win_printf_OK("No PID setting available on Zmag");

    Z_Pc = Z_P = grab_zmag_pid_param(0);

    if (Z_P < 0) return D_O_K;

    Z_Ic = Z_I = grab_zmag_pid_param(1);

    if (Z_I < 0) return D_O_K;

    Z_Dc = Z_D = grab_zmag_pid_param(2);

    if (Z_D < 0) return D_O_K;

    Zpwmmaxc = Zpwmmax = grab_zmag_pid_param(3);

    if (Zpwmmax < 0) return D_O_K;

    Zaccc = Zacc = grab_zmag_pid_param(5);

    if (Zacc < 0) return D_O_K;

    Zvc = Zv = grab_zmag_pid_param(7);

    if (Zv < 0) return D_O_K;

    Zonc = Zon = grab_zmag_pid_param(8);

    if (Zon < 0) return D_O_K;

    Bonc = Bon = grab_zmag_pid_param(9);

    if (Bon < 0) return D_O_K;


    i = win_scanf("Zmag PID parameters setting\n"
                  "Entering wrong parameters will produce\n"
                  "motor vibration and heating! \n"
                  "Proportional (default 5)% 6d\n"
                  "Integral (default 2) %6d\n"
                  "Derivative (default 7)%6d\n"
                  "Max PWM (default 180 max 255) %6d\n"
                  "Zmag acceletation (default 2) %6d\n"
                  "Max speed (default 80) %6d\n"
                  "Zmag PID (%R->Off)   (%r->On)\n"
                  "General brake (%R->Off)   (%r->On)\n"
                  , &Z_P, &Z_I, &Z_D, &Zpwmmax, &Zacc, &Zv, &Zon, &Bon);

    if (i == WIN_CANCEL) return OFF;

    if (Z_P != Z_Pc || Z_I != Z_Ic || Z_D != Z_Dc || Zv != Zvc || Zpwmmax != Zpwmmaxc
            || Zacc != Zaccc || Zon != Zonc || Bon != Bonc)
    {
        i = chg_zmag_pid_param(8, 0); // we switch off PID

        if (i < 0) return win_printf_OK("Zmag PID problem!");

        if (Z_P != Z_Pc)
        {
            i = chg_zmag_pid_param(0, Z_P);

            if (i < 0) return win_printf_OK("Zmag PID problem!");
        }

        if (Z_I != Z_Ic)
        {
            i = chg_zmag_pid_param(1, Z_I);

            if (i < 0) return win_printf_OK("Zmag PID problem!");
        }

        if (Z_D != Z_Dc)
        {
            i = chg_zmag_pid_param(2, Z_D);

            if (i < 0) return win_printf_OK("Zmag PID problem!");
        }

        if (Zv != Zvc)
        {
            i = chg_zmag_pid_param(7, Zv);

            if (i < 0) return win_printf_OK("Zmag PID problem!");
        }

        if (Zpwmmax != Zpwmmaxc)
        {
            i = chg_zmag_pid_param(3, Zpwmmax);

            if (i < 0) return win_printf_OK("Zmag PID problem!");
        }

        if (Zacc != Zaccc)
        {
            i = chg_zmag_pid_param(5, Zacc);

            if (i < 0) return win_printf_OK("Zmag PID problem!");
        }

        if (Bon != Bonc)
        {
            i = chg_zmag_pid_param(9, Bon);

            if (i < 0) return win_printf_OK("Zmag PID problem!");
        }

        i = chg_zmag_pid_param(8, Zon); // we switch back PID

        if (i < 0) return win_printf_OK("Zmag PID problem!");
    }

    if (Bon != 0)
    {
        win_printf("Brake is ON, both motors will not work\n"
                   "neither the temperature control!");
    }

    if (Zon == 0)
    {
        win_printf("Zmag PID is OFF Zmag motor will not move!");
    }

    return D_O_K;
}
MENU *preset_mag_z(float z0, float z1, float dz)
{
    int i, j;
    float tmp;
    char c[32] = {0};
    static MENU pmp[32] = {0};


    if (z0 == z1)
    {
        for (i = WIN_CANCEL; i == WIN_CANCEL;)
            i = win_scanf("You can define preset values to move Z position"
                          "of the magnets by a menu. Please define the maximum value (mm) %f"
                          "the minimum value (mm) %f"
                          "the step size (mm) (no more than 20 steps are possible %f", &z1, &z0, &dz);
    }

    if (z0 > z1)
    {
        tmp = z1;
        z1 = z0;
        z0 = tmp;
    }

    if (dz < 0) dz = -dz;

    if (dz > fabs(z1 - z0) / 32) dz = (z1 - z0) / 32;

    for (i = (int)(1000 * z0), j = 0; i <= (int)(1000 * z1) && j < 30 ; i += (int)(1000 * dz), j++)
    {
        snprintf(c, 32, "%5.3f mm", ((float)i) / 1000);
        // Sanitizer says : runtime error: left shift of negative value -3000 --> negative value passed to MENU_INDEX
        add_item_to_menu(pmp, strdup(c), mag_z, NULL, MENU_INDEX(i), NULL);
    }

    pmp[j].text = NULL;
    return pmp;
}
int set_max_abs_zmag(void)
{
    int i;
    float m = 0, vcap = 0;
    absolute_maximum_zmag=Pico_param.translation_motor_param.translation_max_pos;
    if (updating_menu_state != 0)
    {
        if (get_Vcap_min != NULL)
        {
            snprintf(active_menu->text, 512, "Zmag absolute maximum %g mm (Vcap %2.3fV)", absolute_maximum_zmag, Mag_vcap_min);
        }
        else snprintf(active_menu->text, 512, "Zmag absolute maximum %g mm", absolute_maximum_zmag);

        return D_O_K;
    }

    m = absolute_maximum_zmag;
    vcap = get_config_float("TRANSLATION_MOTOR", "Inductive_sensor_servo_voltage", 0);

    if (get_Vcap_min != NULL)
    {
        vcap = track_info->vcap_min = get_Vcap_min();
        //win_printf("zvcap %f",vcap);
        i = win_scanf("Define the absolute maximum Z magnet\n"
                      "either in millimeter %8f\n"
                      "or by the voltage of the inductive sensor %8f\n", &m, &vcap);

        if (i == WIN_CANCEL)  return OFF;

        if (set_Vcap_min) set_Vcap_min(vcap);

        Mag_vcap_min = track_info->vcap_min = vcap;
    }
    else
    {
        i = win_scanf("Define the absolute maximum Z mag %f", &m);

        if (i == WIN_CANCEL)  return OFF;
    }

    absolute_maximum_zmag = m;
    Pico_param.translation_motor_param.translation_max_pos = m;

    if (get_Vcap_min != NULL)
        Pico_param.translation_motor_param.Inductive_sensor_servo_voltage = vcap;

    Open_Pico_config_file();
    set_config_float("STATE", "AbsoluteMaximumZmag", m);

    if (get_Vcap_min != NULL)
        set_config_float("TRANSLATION_MOTOR", "Inductive_sensor_servo_voltage", vcap);

    Close_Pico_config_file();
    write_Pico_config_file();
    return OFF;
}

float what_is_present_rot_value(void)
{
    return prev_mag_rot; //n_rota_inst;
}
float what_is_present_z_mag_value(void)
{
    return prev_zmag;//n_magnet_z_inst;
}
int read_magnets(void)
{
    int i, im;

    if (updating_menu_state != 0)  return D_O_K;

    im = track_info->imi[track_info->c_i] + 6;
    i = immediate_next_available_action(READ_ZMAG_VAL, 0);

    if (i < 0)    my_set_window_title("Could not add pending action!");

    for (; track_info->imi[track_info->c_i] < im;);

    win_printf("The magnets position \nin z is %f mm\n"
               "Estimated force %6.2f pN \\alpha  %g\nrotation %f \n"
               "mean extension %f \\mu m\n"
               "Stiffness Kx = %g Ky = %g Kz = %g \n"
               "\\tau_x = %g \\tau_y = %g \\tau_z = %g \n"
               "\\beta_x = %g \\beta_y = %g \\beta_z = %g \n"
               "x_n = %g y_n = %g z_n = %g \n"
               , prev_zmag, applied_force, alpha, prev_mag_rot,
               mean_extension, kx, ky, kz, taux, tauy, tauz,
               betax, betay, betaz, xnoise, ynoise, znoise);

    return D_O_K;
}
int set_magnets_rot(void)
{
    int i;
    static float z = 0;

    if (updating_menu_state != 0)  return D_O_K;

    z = prev_mag_rot; //n_rota;
    i = win_scanf("New magnets angular position in turns %f", &z);

    if (i == WIN_CANCEL)  return D_O_K;

    i = immediate_next_available_action(MV_ROT_ABS, z);

    if (i < 0)    win_printf_OK("Could not add pending action!");

    //go_and_dump_rot(z);
    return D_O_K;;
}

int dialog_set_magnets_rot(char *rots)
{
    int i;
    float rot = 0;

    if (sscanf(rots, "%f", &rot) != 1) return 0;

    i = immediate_next_available_action(MV_ROT_ABS, rot);

    if (i < 0)    win_printf_OK("Could not add pending action!");

    return D_O_K;;
}

int dialog_set_magnets_z(char *zmags)
{
    int i;
    float zmag = 0;

    if (sscanf(zmags, "%f", &zmag) != 1) return 0;

    i = immediate_next_available_action(MV_ZMAG_ABS, zmag);

    if (i < 0)    win_printf_OK("Could not add pending action!");

    return D_O_K;;
}



int dialog_set_magnets_z_to_force(char *forces)
{
    int i;
    float z = 0, f;

    if (sscanf(forces, "%f", &f) != 1) return 0;

    z = convert_force_to_zmag(f,  0);

    if (z == -25)    return win_printf_OK("Pb of conversion");

    i = immediate_next_available_action(MV_ZMAG_ABS, z);

    if (i < 0)    win_printf_OK("Could not add pending action!");

    return D_O_K;;
}




int set_magnets_z(void)
{
    int i;
    static float z = 0;

    if (updating_menu_state != 0)  return D_O_K;
    z = prev_zmag; //n_magnet_z;
    i = win_scanf("New magnets position in mm %f", &z);
    if (i == WIN_CANCEL)  return D_O_K;
    i = immediate_next_available_action(MV_ZMAG_ABS, z);
    if (i < 0)    win_printf_OK("Could not add pending action!");
    //go_and_dump_z_magnet(z);
    //do_update_menu();
    return D_O_K;;
}



int park_magnets(void)
{
    int i;
    static float zpark = -20, zbefore = -100;
    static char stuff[128] = {0};

    if (updating_menu_state != 0)
    {

        return D_O_K;
    }

    if (zbefore < -99)
    {
        zbefore = prev_zmag;//n_magnet_z;
        snprintf(stuff, 128, "Unpark and go to %g", zbefore);
        active_menu->text = stuff;
        i = immediate_next_available_action(MV_ZMAG_ABS, zpark);

        if (i < 0)    win_printf_OK("Could not add pending action!");
    }
    else
    {
        snprintf(stuff, 128, "Park magnets to %g", zpark);
        active_menu->text = stuff;
        i = immediate_next_available_action(MV_ZMAG_ABS, zbefore);

        if (i < 0)    win_printf_OK("Could not add pending action!");

        zbefore = -100;
    }

    //go_and_dump_z_magnet(z);
    //do_update_menu();

    return D_O_K;;
}


int set_magnets_z_to_force(void)
{
    int i;
    static float z = 0, f;
    char question[256] = {0};

    if (updating_menu_state != 0)  return D_O_K;

    z = prev_zmag; //n_magnet_z;
    f = applied_force;
    snprintf(question, 256, "Present estimated force %6.3f pN\n"
             "corresponding to zmag = %6.3f mm\n"
             "What estimated force to you want %%6f\n", applied_force, z);
    i = win_scanf(question, &f);

    if (i == WIN_CANCEL)  return D_O_K;

    z = convert_force_to_zmag(f,  0);

    if (z == -25)     return win_printf_OK("Pb of conversion");

    i = immediate_next_available_action(MV_ZMAG_ABS, z);

    if (i < 0)    win_printf_OK("Could not add pending action!");

    //go_and_dump_z_magnet(z);
    //do_update_menu();

    return D_O_K;;
}


int move_mag(void)
{
    int i;
    static float z = 0;

    if (updating_menu_state != 0)  return D_O_K;

    z = prev_zmag; //n_magnet_z;
    i = win_scanf("New magnets position in mm %f", &z);

    if (i == WIN_CANCEL)  return D_O_K;


    i = immediate_next_available_action(MV_ZMAG_ABS, z);

    if (i < 0)    win_printf_OK("Could not add pending action!");

    return D_O_K;;
}



int   ref_magnets_zmag(void)
{
    int i;
    float z = prev_zmag, vz0, vz = 0; //n_magnet_z;
    if (updating_menu_state != 0)  return D_O_K;
    #ifdef ZMAG_THREAD
    int store_zmag_continous_polling=zmag_continous_polling;
    zmag_continous_polling=0;
    #endif



    if (set_zmag_motor_speed == NULL || get_zmag_motor_speed == NULL)
    {
        i = win_scanf("Enter the new Z reference magnet position %f", &z);

        if (i == WIN_CANCEL)   return OFF;

        i = win_printf("are you sure that you want to \n"
                       "change reference of Z magnet to %g", z);

        if (i == WIN_CANCEL)   return OFF;

        i = immediate_next_available_action(CHG_ZMAG_REF, z);

        if (i < 0) my_set_window_title("Could not add pending action!");
    }
    else
    {

        vz0 = vz = get_zmag_motor_speed();

        if (vz <= 2)
        {
            i = win_scanf("Enter the new Z reference magnet position %fDo you want to change motor speed %f", &z, &vz);

            if (i == WIN_CANCEL)   return OFF;

            i = win_printf("are you sure that you want to \n"
                           "change reference of Z magnet to %g", z);

            if (i == WIN_CANCEL)   return OFF;

            i = immediate_next_available_action(CHG_ZMAG_REF, z);

            if (i < 0) my_set_window_title("Could not add pending action!");

            if (vz != vz0) set_zmag_motor_speed(vz);
        }
        else
        {
            i = win_scanf("Enter the new Z reference magnet position %f", &z);

            if (i == WIN_CANCEL)   return OFF;

            i = win_printf("are you sure that you want to \n"
                           "change reference of Z magnet to %g", z);

            if (i == WIN_CANCEL)   return OFF;

            i = immediate_next_available_action(CHG_ZMAG_REF, z);

            if (i < 0) my_set_window_title("Could not add pending action!");
        }
    }
    #ifdef ZMAG_THREAD
    zmag_continous_polling=store_zmag_continous_polling;
    #endif
    return D_O_K;;
    //return dump_to_specific_log_file_with_time(CURRENT_LOG_FILE,"Reference Magnet Z changed to %g\n",n_magnet_z);
}

//change the translation factor tv 09/03/17
int translation_factor_magnets_zmag(void)
{
    int i;
    //oat z = prev_zmag;//n_magnet_z;
    float translation_factor = 1;
    if (updating_menu_state != 0)  return D_O_K;
    if (win_scanf("Do you want to change the translation factor %f?", &translation_factor) == WIN_CANCEL) return D_O_K;
    else
    {
      i = immediate_next_available_action(CHG_ZMAG_FACTOR, translation_factor);
      if (i < 0) my_set_window_title("Could not add pending action!");
    }
    return D_O_K;
    //return dump_to_specific_log_file_with_time(CURRENT_LOG_FILE,"Translation factor of magnet changed to %g\n",translation_factor);
}

float get_rot_motor_speed_from_pid(void)
{
    int vz;

    if (get_rotation_motor_pid != NULL)
    {
        vz = grab_rot_pid_param(7);
        v_rota = (float)vz / 8;

        if (vz >= 0)  Pico_param.rotation_motor_param.rotation_velocity = v_rota;

        return v_rota;
    }

    win_printf_OK("No ROT PID !");
    return 0;

}

float get_zmag_motor_speed_from_pid(void)
{
    int vz;

    if (get_zmag_motor_pid != NULL)
    {
        vz = grab_zmag_pid_param(7);
        v_mag = (float)vz / 8;

        if (vz >= 0)  Pico_param.translation_motor_param.translation_velocity = v_mag;

        return v_mag;
    }

    win_printf_OK("No zmag PID");
    return 0;
}

int set_rot_motor_speed_from_pid(float speed)
{
    int vzi;

    if (set_rotation_motor_pid != NULL)
    {
        speed = fabs(speed);
        vzi = (int)(0.5 + 8 * fabs(speed));
        vzi = (vzi < 1) ? 1 : vzi; // motors do not like 0 velocity
        return chg_rot_pid_param(7, vzi);
    }

    return win_printf_OK("No ROT PID !");
}

int set_zmag_motor_speed_from_pid(float speed)
{
    int vzi;

    if (set_zmag_motor_pid != NULL)
    {
        speed = fabs(speed);
        vzi = (int)(0.5 + 8 * fabs(speed));
        vzi = (vzi < 1) ? 1 : vzi; // motors do not like 0 velocity
        return chg_zmag_pid_param(7, vzi);
    }

    return win_printf_OK("No Zmag PID !");
}

int set_zmag_motor_speed_generic(float motor_speed)
{
# if defined(PI_C843) || defined(PI_C843_TRS) || defined(ZMAG_APT) || defined(RAWHID_V2)
            set_zmag_motor_speed(motor_speed);
# else
            set_zmag_motor_speed_from_pid(motor_speed);
# endif
            Pico_param.translation_motor_param.translation_velocity = motor_speed;
            Open_Pico_config_file();
            set_config_float("TRANSLATION_MOTOR", "translation_velocity", motor_speed);
            write_Pico_config_file();
            Close_Pico_config_file();
            return 0;
}

int set_magnets_rot_velocity(void)
{
    int i;
    static float vr = 0, vz = 0;

    if (updating_menu_state != 0)
    {
        if (set_zmag_motor_speed == NULL || get_zmag_motor_speed == NULL)
            snprintf(active_menu->text, 512, "Rotation motors speed %g tr/s", v_rota);
        else snprintf(active_menu->text, 512, "Motors speed %g tr/s %g mm/s", v_rota, v_mag);

        return D_O_K;
    }

#if defined(RAWHID_V2)
  vr = v_rota = 0;
#else
# if defined(PI_C843) || defined(PI_C843_TRS) || defined(ZMAG_APT)

    vr = v_rota = get_motors_speed();
# else
    vr = v_rota = get_rot_motor_speed_from_pid();
# endif
#endif


    if (set_zmag_motor_speed == NULL || get_zmag_motor_speed == NULL)
    {
        i = win_scanf("New magnets angular velocity in turns/s %f", &vr);
        if (i == WIN_CANCEL)  return D_O_K;
# if defined(PI_C843) || defined(PI_C843_TRS)


        set_motors_speed(vr);
# else


        set_rot_motor_speed_from_pid(vr);
# endif
    }
    else
    {

# if defined(PI_C843) || defined(PI_C843_TRS) || defined(ZMAG_APT) || defined(RAWHID_V2)


        v_mag = vz = get_zmag_motor_speed();
# else
        v_mag = vz = get_zmag_motor_speed_from_pid();
# endif

#if defined(RAWHID_V2)  || defined(ZMAG_APT)
      i = win_scanf("New magnets translation velocity in mm/s %8f\n",&vz);
#else
        i = win_scanf("Define the motors speed\n"
                      "New magnets angular velocity in turns/s %8f\n"
                      "New magnets translation velocity in mm/s %8f\n"
                      , &vr, &vz);
#endif
        if (i == WIN_CANCEL)  return D_O_K;
        if (v_rota != vr)
        {
# if defined(PI_C843) || defined(PI_C843_TRS) || defined(ZMAG_APT)
            set_motors_speed(v_rota = vr);
# else
            set_rot_motor_speed_from_pid(v_rota = vr);
# endif
            Pico_param.rotation_motor_param.rotation_velocity = vr ;
            Open_Pico_config_file();
            set_config_float("ROTATION_MOTOR", "rotation_velocity", vr);
            write_Pico_config_file();
            Close_Pico_config_file();
            win_printf("Set vrot = %g", vr);
        }
        if (v_mag != vz)
        {
			       set_zmag_motor_speed_generic(vz);
            Pico_param.translation_motor_param.translation_velocity = vz;
            Open_Pico_config_file();
            set_config_float("TRANSLATION_MOTOR", "translation_velocity", vz);
            write_Pico_config_file();
            Close_Pico_config_file();
            win_printf("Set vzmag = %g", vz);
            v_mag = vz;
        }
    }
    return D_O_K;
}



int origin_job(int im, struct future_job *job)
{
    int i, j;
    int ci;
    static int im0 = -1, found = 0;
    static float z_pos_at_org = 0;
    float vtmp;

    if (job == NULL || job->in_progress == 0) return 0;

    if (im < job->imi)  return 0;

    ci = track_info->c_i;
    j = track_info->status_flag[ci] & ZMAG_MOVING;//& POSITIVE_LIMIT_ON|& NEGATIVE_LIMIT_ON;
    // new test of limit switch 2009/10/09
    j = (track_info->status_flag[ci] & (POSITIVE_LIMIT_ON | NEGATIVE_LIMIT_ON)) ? 0 : 1;

    if (j == 0) found = 1;

    if (job->in_progress == 1)
    {
        // first entry we save actual image number and reset variables
        im0 = im;
        found = 0;
        z_pos_at_org = 0;
        job->in_progress++;
    }

    // we check if the process did not last too long
    if (im - im0 > 3000)
    {
        if (set_zmag_motor_speed != NULL)
        {
            set_zmag_motor_speed(job->f_local[3]);
            //tv 08/02/17
            //set_zmag_motor_speed_generic(job->f_local[3]);
            psleep(50);
        }

        absolute_maximum_zmag = job->f_local[2];

        if (found == 0)     win_printf("Motor took too long to move reference was not changed!\n");

        job->in_progress = 0;
        return 0;
    }


    if (job->in_progress == 2)
    {
        if (found == 0)
        {
            job->imi++;   // we display status message
            my_set_window_title("Moving towards origin Z = %g", z_pos_at_org = prev_zmag);
            return 0;
        }
        else
        {
            job->in_progress = 3;
            i = immediate_next_available_action(CHG_ZMAG_REF, job->f_local[0]);

            if (i < 0)    my_set_window_title("Could not add pending action!");

            //display_title_message("changing Z reference");
            my_set_window_title("Limit found for origin at %g moving towards origin Z = %g", z_pos_at_org, prev_zmag);
            job->imi += 4;
            return 0;
        }
    }


    if (job->in_progress == 3)
    {
        job->in_progress = 4;

        if (set_zmag_motor_speed != NULL)
        {
            set_zmag_motor_speed(job->f_local[3]);
            psleep(50);

            if (get_zmag_motor_speed != NULL)
            {
                vtmp = get_zmag_motor_speed();
                psleep(50);

                if (fabs(vtmp - job->f_local[3]) > 0.05)
                {
                    set_zmag_motor_speed(job->f_local[3]);
                    psleep(50);
                }
            }

            //win_printf("restorin Zmag speed to %g",job->f_local[3]);
        }

        //display_title_message("Restoring Vz");
        my_set_window_title("Restoring Vz");
        job->imi += 20;
        return 0;
    }


    if (job->in_progress == 4)
    {
        {
            job->in_progress = 5;
            immediate_next_available_action(MV_ZMAG_ABS, job->f_local[1] / 2);
            job->imi += 4;
            return 0;
        }
    }

    if (job->in_progress == 5)
    {
        if (track_info->status_flag[ci] & ZMAG_MOVING)
        {
            job->imi++;   // we display status message
            my_set_window_title("Moving towards position %g, Z = %g", job->f_local[1] / 2, z_pos_at_org = prev_zmag);
            return 0;
        }
        else
        {
            job->in_progress = 6;
            immediate_next_available_action(MV_ZMAG_ABS, job->f_local[1]);
            job->imi += 4;
            return 0;
        }
    }


    if (job->in_progress == 6)
    {
        if (track_info->status_flag[ci] & ZMAG_MOVING)
        {
            job->imi++;   // we display status message
            my_set_window_title("Moving towards position %g, Z = %g", job->f_local[1], z_pos_at_org = prev_zmag);
            return 0;
        }
        else
        {
            absolute_maximum_zmag = job->f_local[2];
            my_set_window_title("The motor origin has been set to %g", job->f_local[0]);

            if (current_write_open_path_picofile != NULL)
            {
                dump_to_html_log_file_with_date_and_time(current_write_open_picofile
                        , "New zmag reference done at %g mm done<br>", job->f_local[0]);
            }

            job->in_progress = 0;
            return 0;
        }
    }


    if (job->in_progress > 6)
    {
        job->in_progress = 0;
        return 0;
    }

    return 0;
}



int do_set_z_origin(void)
{
    int i;
    static int dir = 1, im;
    static float z0 = 0, vz = 1, z_finish = -3;
    float z, vzorg = 0, zabbs;

    if (updating_menu_state != 0)  return D_O_K;

    if (get_zmag_motor_speed != NULL)
    {
        vzorg = get_zmag_motor_speed();
        psleep(50);

        if (vzorg < 1)
        {
            vzorg = get_zmag_motor_speed();
            psleep(50);
        }
    }
    else vzorg = 15;

    vzorg = (vzorg < 1) ? 1 : vzorg;
    i = win_scanf("This routine adjust the absolute position of the magnets Z position by\n"
                  "checking the translation limits at low speed \n"
                  "Do you want to bump in the top %R or bottom %r limit\n"
                  "(Bottom limits requires that you swing the magnet holder aside)\n"
                  "define the low magnets velocity (1mm/s is fine) %8f\n"
                  "define the value to reset back magnets velocity after %8f\n"
                  "Specify the reference value to set on limit reached %8f\n"
                  "Indicate the magnets position after reference has been set %8f\n"
                  , &dir, &vz, &vzorg, &z0, &z_finish);

    if (i == WIN_CANCEL)  return D_O_K;

    if (dir == 1)
        win_printf("Be sure to move the magnets on the side\n"
                   "to avoid crashing your sample!");

    //set_z_origin(z,dir);
    z = prev_zmag; //n_magnet_z;
    zabbs = absolute_maximum_zmag;

    if (dir)
    {
        z += 50;
        absolute_maximum_zmag += 50;
    }
    else
    {
        z -= 50;
        absolute_maximum_zmag -= 50;
    }

    /*
    if (get_zmag_motor_speed != NULL)
      {
        vzorg = get_zmag_motor_speed();
        psleep(50);
      }
    */
    if (set_zmag_motor_speed != NULL)
    {
        set_zmag_motor_speed(vz);
        psleep(50);
    }

    immediate_next_available_action(MV_ZMAG_ABS, z);
    psleep(50);
    im = track_info->imi[track_info->c_i];
    i = fill_next_available_job_spot(1, im + 1, 0, 0, NULL, NULL, NULL, NULL, NULL, origin_job, 0);

    if (i < 0)  return win_printf_OK("Could not add pending action!");

    job_pending[i].f_local[0] = z0;
    job_pending[i].f_local[1] = z_finish;
    job_pending[i].f_local[2] = zabbs;
    job_pending[i].f_local[3] = vzorg;
    return D_O_K;;
}


int do_go_to_vcap_z_ref(void)
{

    if (updating_menu_state != 0)
    {
        if (get_Vcap_min != NULL)
        {
            snprintf(active_menu->text, 512, "Move zmag to %2.3f mm (Vcap ref %2.3fV))"
                     , Pico_param.translation_motor_param.zmag_ref_for_inductive_test
                     , Pico_param.translation_motor_param.Vcap_at_zmag_ref_test);
        }
        else snprintf(active_menu->text, 512, "Go to Zmag ref");

        return D_O_K;
    }

    immediate_next_available_action(MV_ZMAG_ABS, Pico_param.translation_motor_param.zmag_ref_for_inductive_test);
    return D_O_K;;
}

int origin_check_job(int im, struct future_job *job)
{
    int i, j;
    int ci;
    static int im0 = -1;
    float zmag = -1, Vcap_zmag = -1;
    int limit = 0, vcap_servo_on = 0;
    char buf[4][256] = {"\0", "\0", "\0", "\0"};

    if (job == NULL || job->in_progress == 0) return 0;

    if (im < job->imi)  return 0;

    ci = track_info->c_i;
    j = track_info->status_flag[ci] & ZMAG_MOVING;//& POSITIVE_LIMIT_ON|& NEGATIVE_LIMIT_ON;

    if (job->in_progress == 1)
    {
        im0 = im;
        job->in_progress++;
        job->imi += 20;
        return 0;
    }

    if (im < im0 + 3000 && j != 0)
    {
        job->imi += 20;
        snprintf(buf[0], 256, "Moving towards origin Z = %g", prev_zmag);
        //my_set_window_title("Moving towards origin Z = %g",prev_zmag);
        my_set_window_title("%s %s %s %s", buf[0], buf[1], buf[2], buf[3]);
        //display_title_message("Moving towards origin Z = %g",prev_zmag);
        return 0;
    }

    if (j != 0)
    {
        if (set_Vcap_min != NULL)
        {
            set_Vcap_min(job->f_local[3]);
            psleep(50);
        }

        absolute_maximum_zmag = job->f_local[2];
        immediate_next_available_action(MV_ZMAG_ABS, job->f_local[1]);
        win_printf("Motor took too long to move ogigin coul not be checked!\n");
        job->in_progress = 0;
        return 0;
    }
    else
    {
        if (job->in_progress == 2)
        {
            i = immediate_next_available_action(READ_ZMAG_VAL, 0);

            if (i < 0)    my_set_window_title("Could not add pending action!");

            snprintf(buf[1], 256, "reading reference");
            //my_set_window_title("reading reference");
            my_set_window_title("%s %s %s %s", buf[0], buf[1], buf[2], buf[3]);
            job->imi += 20;
            job->in_progress++;
            return 0;
        }
        else if (read_magnet_z_value_and_status(&zmag, &Vcap_zmag, &limit, &vcap_servo_on) == 0)
        {
            job->imi += 4;
            job->in_progress = 0;

            if (vcap_servo_on)
            {
                i = win_printf("Motors ervo is done with inductive sensor at %g Volts (%g asked).\n"
                               "This corresponds to incremental encoder position %g mm\n"
                               "Previous reference of incremental encoder position %g mm\n"
                               "Press Ok to save the present zmag as a refence.\n"
                               , Vcap_zmag, job->f_local[0], zmag
                               , Pico_param.translation_motor_param.zmag_ref_for_inductive_test);

                if (i == WIN_OK)
                {
                    Pico_param.translation_motor_param.zmag_ref_for_inductive_test = zmag;
                    write_Pico_config_file();

                    if (current_write_open_path_picofile != NULL)
                    {
                        dump_to_html_log_file_with_date_and_time(current_write_open_picofile
                                , "New zmag reference at %g mm done using inductive sensor at voltage %g<br>"
                                , Pico_param.translation_motor_param.zmag_ref_for_inductive_test
                                , Pico_param.translation_motor_param.Vcap_at_zmag_ref_test);
                    }

                }
            }
            else
            {
                i = win_printf("Motors ervo is not done with inductive sensor at %g Volts (%g asked)\n"
                               "Corresponds to incremental encoder position %g mm\n"
                               "This reference test failed!"
                               , Vcap_zmag, job->f_local[0], zmag);
            }

            //if (i == WIN_CANCEL)      return 0;
            if (set_Vcap_min != NULL)
            {
                set_Vcap_min(job->f_local[3]);
                psleep(50);
            }

            absolute_maximum_zmag = job->f_local[2];

            if (set_zmag_motor_speed != NULL)
            {
                set_zmag_motor_speed(job->f_local[4]);
                psleep(50);
            }

            //i = immediate_next_available_action(MV_ZMAG_ABS, job->f_local[1]);
            //if (i < 0)  my_set_window_title("Could not add pending action!");
            //my_set_window_title("checking Z reference");
            return 0;
        }
    }

    return 0;
}




int do_check_z_origin(void)
{
    int i;
    static int im;
    static float Vz0 = 8, z_finish = -3;
    float z, vcap_org = 0, zabbs, vzorg = 0;// , z0

    if (updating_menu_state != 0)  return D_O_K;

    Vz0 = Pico_param.translation_motor_param.Vcap_at_zmag_ref_test;

    if (Vz0 <= 0) Vz0 = 8;

    i = win_scanf("This routine check the absolute position of the magnets Z position by\n"
                  "using the inductive sensor indication. \n"
                  "It requires that your magnet holder is on top of your sample.\n"
                  "This routine will send the magnets towards the sample at slow speed\n"
                  "until the inductive sensor voltage reaches the desired value\n"
                  "Specify the reference voltage value to set on  %8f\n"
                  //"Indicate the magnets position after reference has been set %8f\n"
                  , &Vz0); //,&z_finish

    if (i == WIN_CANCEL)  return D_O_K;

    Pico_param.translation_motor_param.Vcap_at_zmag_ref_test = Vz0;
    write_Pico_config_file();
    z = prev_zmag; //n_magnet_z;// z0 =
    zabbs = absolute_maximum_zmag;
    z += 50;
    absolute_maximum_zmag += 50;

    if (get_Vcap_min) vcap_org = get_Vcap_min();

    psleep(50);

    if (set_Vcap_min) set_Vcap_min(Vz0);

    psleep(150);

    if (get_zmag_motor_speed != NULL)
    {
        vzorg = get_zmag_motor_speed();
        psleep(50);
    }

    if (set_zmag_motor_speed != NULL)
    {
        set_zmag_motor_speed(1.0);
        psleep(50);
    }

    immediate_next_available_action(MV_ZMAG_ABS, z);
    //psleep(50);
    im = track_info->imi[track_info->c_i];
    i = fill_next_available_job_spot(1, im + 1, 0, 0, NULL, NULL, NULL, NULL, NULL, origin_check_job, 0);

    if (i < 0)  return win_printf_OK("Could not add pending action!");

    job_pending[i].f_local[0] = Vz0;
    job_pending[i].f_local[1] = z_finish;
    job_pending[i].f_local[2] = zabbs;
    job_pending[i].f_local[3] = vcap_org;
    job_pending[i].f_local[4] = vzorg;
    return D_O_K;;
}

int zmag_small_step_up(void)
{
    float z = 0, f;
    if (updating_menu_state != 0)        return D_O_K;
    //z = n_magnet_z + 0.05;
    if (key[KEY_LSHIFT])
    {
        z = 200 * prev_zmag; //n_magnet_z;
        z = 0.005 * ((int)floor(z + 1.5));
    }
    else
    {
        z = 20 * prev_zmag; //n_magnet_z;
        z = 0.05 * ((int)floor(z + 1.5));
    }
    if (z > absolute_maximum_zmag)  z = absolute_maximum_zmag;
    if (is_there_pending_action_of_type(MV_ZMAG_ABS) >= 0)
    {
        my_set_window_title("MV mag already pending");
        return D_O_K;
    }
    if (track_info->status_flag[track_info->c_i] & ZMAG_MOVING)
    {
        my_set_window_title("Zmag already moving");
        return D_O_K;
    }
    f = convert_only_zmag_to_force(z, 0);
    immediate_next_available_action(MV_ZMAG_ABS, z);
    my_set_window_title("Obj : %g Zmag %g rotation %g Fe = %6.3f pN", read_last_Z_value(), z, prev_mag_rot, f); // n_rota
    return D_O_K;
}
int zmag_small_step_dwn(void)
{
    //char buf[256];
    float z = 0, f, dz;

    if (updating_menu_state != 0)        return D_O_K;

    //z = n_magnet_z - 0.05;
    dz = absolute_maximum_zmag - prev_zmag;

    if (key[KEY_LSHIFT])
    {
        z = 200 * prev_zmag; //n_magnet_z;
        z = 0.005 * ((int)floor(z - 0.5));
    }
    else
    {
        if (fabs(dz) < 0.1)
        {
            z = 200 * prev_zmag; //n_magnet_z;
            z = 0.005 * ((int)floor(z - 0.5));
        }
        else
        {
            z = 20 * prev_zmag; //n_magnet_z;
            z = 0.05 * ((int)floor(z - 0.5));
        }
    }

    if (z > absolute_maximum_zmag)  z = absolute_maximum_zmag;

    if (is_there_pending_action_of_type(MV_ZMAG_ABS) >= 0)
    {
        my_set_window_title("MV mag already pending");
        return D_O_K;
    }

    if (track_info->status_flag[track_info->c_i] & ZMAG_MOVING)
    {
        my_set_window_title("Zmag already moving");
        return D_O_K;
    }

    f = convert_only_zmag_to_force(z, 0);
    //f = convert_zmag_to_force(z, n_rota, 0);
    immediate_next_available_action(MV_ZMAG_ABS, z);
    my_set_window_title("Obj : %g Zmag %g rotation %g Fe = %6.3f pN", read_last_Z_value(), z, prev_mag_rot, f); // n_rota
    return D_O_K;
}
int zmag_step_up(void)
{
    //char buf[256];
    float z = 0, f, dz;
    if (updating_menu_state != 0)        return D_O_K;
    dz = absolute_maximum_zmag - prev_zmag;
    if (key[KEY_LSHIFT])
    {
        z = 40 * prev_zmag;//n_magnet_z;
        z = 0.025 * ((int)floor(z + 1.5));
    }
    else
    {
        if (fabs(dz) < 0.25)
        {
            z = 40 * prev_zmag;//n_magnet_z;
            z = 0.025 * ((int)floor(z + 1.5));
        }
        else
        {
//			win_printf_OK("test dz%f prevzmag%f", dz, prev_zmag);
            z = 4 * prev_zmag; //n_magnet_z;
            z = 0.25 * ((int)floor(z + 1.5));
        }
    }
    if (z > absolute_maximum_zmag)  z = absolute_maximum_zmag;
    //z = n_magnet_z + 0.25;
    if (is_there_pending_action_of_type(MV_ZMAG_ABS) >= 0)
    {
        my_set_window_title("MV mag already pending");
        return D_O_K;
    }
    if (track_info->status_flag[track_info->c_i] & ZMAG_MOVING)
    {
        my_set_window_title("Zmag already moving");
        return D_O_K;
    }
    f = convert_only_zmag_to_force(z, 0);
    immediate_next_available_action(MV_ZMAG_ABS, z);
    my_set_window_title("Obj : %g Zmag %g rotation %g Fe = %6.3f pN", read_last_Z_value(), z, prev_mag_rot, f); //n_rota
    return D_O_K;
}

int zmag_step_dwn(void)
{
    //char buf[256];
    float z = 0, f;

    if (updating_menu_state != 0)        return D_O_K;

    //z = n_magnet_z - 0.25;
    if (key[KEY_LSHIFT])
    {
        z = 40 * prev_zmag; //n_magnet_z;
        z = 0.025 * ((int)floor(z - 0.5));
    }
    else
    {
        z = 4 * prev_zmag; //n_magnet_z;
        z = 0.25 * ((int)floor(z - 0.5));
    }

    if (z > absolute_maximum_zmag)  z = absolute_maximum_zmag;

    //f = convert_zmag_to_force(z, n_rota, 0);
    if (is_there_pending_action_of_type(MV_ZMAG_ABS) >= 0)
    {
        my_set_window_title("MV mag already pending");
        return D_O_K;
    }

    if (track_info->status_flag[track_info->c_i] & ZMAG_MOVING)
    {
        my_set_window_title("Zmag already moving");
        return D_O_K;
    }

    f = convert_only_zmag_to_force(z, 0);
    immediate_next_available_action(MV_ZMAG_ABS, z);
    my_set_window_title("Obj : %g Zmag %g rotation %g Fe = %6.3f pN", read_last_Z_value(), z, prev_mag_rot, f); // n_rota
    return D_O_K;
}


int rot_small_step_up(void)
{
    //  char buf[256];
    float r = 0, f;

    if (updating_menu_state != 0)        return D_O_K;

    //r = n_rota + 0.25;
    if (key[KEY_LSHIFT])
    {
        r = 40 * prev_mag_rot;//n_rota;
        r = 0.025 * ((int)floor(r + 1.5));
    }
    else
    {
        r = 4 * prev_mag_rot;//n_rota;
        r = 0.25 * ((int)floor(r + 1.5));
    }

    if (is_there_pending_action_of_type(MV_ROT_ABS) >= 0)
    {
        my_set_window_title("ROT mag already pending");
        return D_O_K;
    }

    if (track_info->status_flag[track_info->c_i] & ROT_MOVING)
    {
        my_set_window_title("Motors already rotating");
        return D_O_K;
    }

    immediate_next_available_action(MV_ROT_ABS, r);
    f = convert_only_zmag_to_force(prev_zmag, 0);//n_magnet_z
    //f = convert_zmag_to_force(n_magnet_z, r, 0);
    my_set_window_title("Obj : %g Zmag %g rotation %g Fe = %6.3f pN", read_last_Z_value(), n_magnet_z, r, f);
    return D_O_K;
}
int rot_small_step_dwn(void)
{
    //char buf[256];
    float r = 0, f;

    if (updating_menu_state != 0)        return D_O_K;

    //r = n_rota - 0.25;
    if (key[KEY_LSHIFT])
    {
        r = 40 * prev_mag_rot; //n_rota;
        r = 0.025 * ((int)floor(r - 0.5));
    }
    else
    {
        r = 4 * prev_mag_rot;//n_rota;
        r = 0.25 * ((int)floor(r - 0.5));
    }

    //f = convert_zmag_to_force(n_magnet_z, r, 0);
    if (is_there_pending_action_of_type(MV_ROT_ABS) >= 0)
    {
        my_set_window_title("MV mag already pending");
        return D_O_K;
    }

    if (track_info->status_flag[track_info->c_i] & ROT_MOVING)
    {
        my_set_window_title("Motors already rotating");
        return D_O_K;
    }

    f = convert_only_zmag_to_force(prev_zmag, 0); // n_magnet_z
    immediate_next_available_action(MV_ROT_ABS, r);
    my_set_window_title("Obj : %g Zmag %g rotation %g Fe = %6.3f pN", read_last_Z_value(), prev_zmag, r, f); // n_magnet_z
    return D_O_K;
}
int rot_step_up(void)
{
    //char buf[256];
    float r = 0, f;

    if (updating_menu_state != 0)        return D_O_K;

    //r = n_rota + 1;
    if (key[KEY_LSHIFT])
    {
        r = 10 * prev_mag_rot; //n_rota;
        r = 0.1 * (float)((int)floor((r + 0.5) + 1));
    }
    else
    {
        r = prev_mag_rot;//n_rota;
        r = (float)((int)floor((r + 0.5) + 1));
    }

    //f = convert_zmag_to_force(n_magnet_z, r, 0);
    if (is_there_pending_action_of_type(MV_ROT_ABS) >= 0)
    {
        my_set_window_title("MV mag already pending");
        return D_O_K;
    }

    if (track_info->status_flag[track_info->c_i] & ROT_MOVING)
    {
        my_set_window_title("Motors already rotating");
        return D_O_K;
    }

    f = convert_only_zmag_to_force(prev_zmag, 0); //n_magnet_z, 0);
    immediate_next_available_action(MV_ROT_ABS, r);
    my_set_window_title("Obj : %g Zmag %g rotation %g Fe = %6.3f pN", read_last_Z_value(), prev_zmag, r, f); // n_magnet_z
    return D_O_K;
}
int rot_step_dwn(void)
{
    //char buf[256];
    float r = 0, f;

    if (updating_menu_state != 0)        return D_O_K;

    //r = n_rota - 1;
    if (key[KEY_LSHIFT])
    {
        r = 10 * prev_mag_rot; //n_rota;
        r = 0.1 * (float)((int)floor((r + 0.5) - 1));
    }
    else
    {
        r = prev_mag_rot;//n_rota;
        r = (float)((int)floor((r + 0.5) - 1));
    }

    //f = convert_zmag_to_force(n_magnet_z, r, 0);
    if (is_there_pending_action_of_type(MV_ROT_ABS) >= 0)
    {
        my_set_window_title("MV mag already pending");
        return D_O_K;
    }

    if (track_info->status_flag[track_info->c_i] & ROT_MOVING)
    {
        my_set_window_title("Motors already rotating");
        return D_O_K;
    }

    f = convert_only_zmag_to_force(prev_zmag, 0); //n_magnet_z, 0);
    immediate_next_available_action(MV_ROT_ABS, r);
    my_set_window_title("Obj : %g Zmag %g rotation %g Fe = %6.3f pN", read_last_Z_value(), prev_zmag, r, f); //n_magnet_z
    return D_O_K;
}


int preset_zmag_key_set(void)
{
   int i, active = 1, num = 0;
   float z[8] = {0};

    if (updating_menu_state != 0)             return D_O_K;

    z[0] = get_config_float("zmag_short_cuts", "zmag_z", -0.15);
    z[1] = get_config_float("zmag_short_cuts", "zmag_Z", -0.3);
    z[2] = get_config_float("zmag_short_cuts", "zmag_CTRL_z", -0.45);
    z[3] = get_config_float("zmag_short_cuts", "zmag_CTRL_Z", -0.6);
    z[4] = get_config_float("zmag_short_cuts", "zmag_ALT_z", -0.75);
    z[5] = get_config_float("zmag_short_cuts", "zmag_ALT_Z", -0.9);
    active = preset_zmag_key_active;

    for (i = 0; i < 20; i++)
      {
	preset_force_key_val[i] = (i > 0)? (float)i : 0.01;
	preset_force_zmag_key_val[i] =  convert_force_to_zmag(preset_force_key_val[i],  0);
	//	if (i == 5)
	//  win_printf("Force %g zmag %g",preset_force_key_val[i],preset_force_zmag_key_val[i]);
      }

    num = preset_force_key_active;

    i = win_scanf("You can define zmag preset values accessible through keyboard short cuts\n"
                  "Define preset value reached on pressing {\\color{yellow}z} %10f\n"
                  "Define preset value reached on pressing {\\color{yellow}Z} %10f\n"
                  "Define preset value reached on pressing {\\color{yellow}CRTL-z} %10f\n"
                  "Define preset value reached on pressing {\\color{yellow}CRTL-Z} %10f\n"
                  "Define preset value reached on pressing {\\color{yellow}ALT-z} %10f\n"
                  "Define preset value reached on pressing {\\color{yellow}ALT-Z} %10f\n"
                  "Short cuts active %R->No %r->yes\n"
		  "You can also define that numerical keys as short cuts for force\n"
		  "Pressing (Num)N key setting force to N pN\n"
		  "Pressing Shift(Num)N key setting force to 1N pN\n"
		  "Key 0 setting force to 0.01pN\n"
		  "Click here to select this option %b\n"
		  , z, z + 1, z + 2, z + 3, z + 4, z + 5, &active,&num);

    if (i == WIN_CANCEL) return 0;

    for (i = 0; i < 8; i++) preset_zmag_key_val[i] = z[i];

    set_config_float("zmag_short_cuts", "zmag_z", preset_zmag_key_val[0]);
    set_config_float("zmag_short_cuts", "zmag_Z", preset_zmag_key_val[1]);
    set_config_float("zmag_short_cuts", "zmag_CTRL_z", preset_zmag_key_val[2]);
    set_config_float("zmag_short_cuts", "zmag_CTRL_Z", preset_zmag_key_val[3]);
    set_config_float("zmag_short_cuts", "zmag_ALT_z", preset_zmag_key_val[4]);
    set_config_float("zmag_short_cuts", "zmag_ALT_Z", preset_zmag_key_val[5]);

    preset_zmag_key_active = active;
    preset_force_key_active = num;
    preset_zmag_key_index = 0;
    preset_force_key_index = 0;
    return 0;
}


int preset_force_key(void)
{
    float val;

    if (updating_menu_state != 0)              return D_O_K;

    if (preset_force_key_active == 0) return D_O_K;

    if (key[KEY_0_PAD])     preset_force_key_index = 0;
    else if (key[KEY_1_PAD])     preset_force_key_index = 1;
    else if (key[KEY_2_PAD])     preset_force_key_index = 2;
    else if (key[KEY_3_PAD])     preset_force_key_index = 3;
    else if (key[KEY_4_PAD])     preset_force_key_index = 4;
    else if (key[KEY_5_PAD])     preset_force_key_index = 5;
    else if (key[KEY_6_PAD])     preset_force_key_index = 6;
    else if (key[KEY_7_PAD])     preset_force_key_index = 7;
    else if (key[KEY_8_PAD])     preset_force_key_index = 8;
    else if (key[KEY_9_PAD])     preset_force_key_index = 9;
    else return D_O_K;
    if ((key_shifts & __allegro_KB_SHIFT_FLAG) || (key_shifts & KB_SHIFT_FLAG)  || (key_shifts & KB_CTRL_FLAG))
      {
	preset_force_key_index += 10;
	win_printf("index %d val %g",preset_force_key_index,preset_force_zmag_key_val[preset_force_key_index]);
      }

    if (preset_force_key_index < 0) return D_O_K;

    if (preset_force_key_index > 19) return D_O_K;

    val = preset_force_zmag_key_val[preset_force_key_index];

    //win_printf("index %d val %g",preset_force_key_index,val);
    if (fabs(prev_zmag - val) < 0.005) return D_O_K;

    immediate_next_available_action(MV_ZMAG_ABS, val);
    return D_O_K;
}


int preset_zmag_key(void)
{
    float val;

    if (updating_menu_state != 0)              return D_O_K;

    if (preset_zmag_key_active == 0) return D_O_K;

    if (key_shifts & __allegro_KB_CTRL_FLAG) preset_zmag_key_index = 2;
    else if (key_shifts & __allegro_KB_ALT_FLAG) preset_zmag_key_index = 4;
    else preset_zmag_key_index = 0;

    if (key_shifts & __allegro_KB_SHIFT_FLAG) preset_zmag_key_index += 1;

    if (preset_zmag_key_index < 0) return D_O_K;

    if (preset_zmag_key_index > 5) return D_O_K;

    val = preset_zmag_key_val[preset_zmag_key_index];

    //win_printf("index %d val %g",preset_zmag_key_index,val);
    if (fabs(prev_zmag - val) < 0.005) return D_O_K;

    immediate_next_available_action(MV_ZMAG_ABS, val);
    return D_O_K;
}


int preset_zmag_key_ctrl(void)
{
    float val;

    if (updating_menu_state != 0)              return D_O_K;

    if (preset_zmag_key_active == 0) return D_O_K;

    if (key_shifts & __allegro_KB_CTRL_FLAG) preset_zmag_key_index = 2;
    else if (key_shifts & __allegro_KB_ALT_FLAG) preset_zmag_key_index = 4;
    else preset_zmag_key_index = 0;

    if (key_shifts & __allegro_KB_SHIFT_FLAG) preset_zmag_key_index += 1;

    if (preset_zmag_key_index < 0) return D_O_K;

    if (preset_zmag_key_index > 5) return D_O_K;

    val = preset_zmag_key_val[preset_zmag_key_index];

    //win_printf("alt index %d val %g",preset_zmag_key_index,val);
    if (fabs(prev_zmag - val) < 0.005) return D_O_K;

    immediate_next_available_action(MV_ZMAG_ABS, val);
    return D_O_K;
}


int preset_zmag_key_alt(void)
{
    float val;

    if (updating_menu_state != 0)              return D_O_K;

    if (preset_zmag_key_active == 0) return D_O_K;

    if (key_shifts & __allegro_KB_CTRL_FLAG) preset_zmag_key_index = 2;
    else if (key_shifts & __allegro_KB_ALT_FLAG) preset_zmag_key_index = 4;
    else preset_zmag_key_index = 0;

    if (key_shifts & __allegro_KB_SHIFT_FLAG) preset_zmag_key_index += 1;

    if (preset_zmag_key_index < 0) return D_O_K;

    if (preset_zmag_key_index > 5) return D_O_K;

    val = preset_zmag_key_val[preset_zmag_key_index];
    win_printf("alt index %d val %g", preset_zmag_key_index, val);

    if (fabs(prev_zmag - val) < 0.005) return D_O_K;

    immediate_next_available_action(MV_ZMAG_ABS, val);
    return D_O_K;
}

int magnets_keyboard_shortcuts(void)
{
  if (updating_menu_state!=0)
  {
    add_keyboard_short_cut(0, KEY_F5, 0, zmag_step_up);
    add_keyboard_short_cut(0, KEY_F6, 0, zmag_small_step_up);
    add_keyboard_short_cut(0, KEY_F7, 0, zmag_small_step_dwn);
    add_keyboard_short_cut(0, KEY_F8, 0, zmag_step_dwn);
    add_keyboard_short_cut('z', KEY_Z, 0, preset_zmag_key);
    add_keyboard_short_cut(KEY_Z, 0, 0, preset_zmag_key_ctrl);
    add_keyboard_short_cut((KEY_Z << 8), 0, 0, preset_zmag_key_alt);
    add_keyboard_short_cut(0, KEY_S, 0, zmag_bd_search_key);
    add_keyboard_short_cut (0, KEY_0_PAD, 0, preset_force_key);
    add_keyboard_short_cut (0, KEY_1_PAD, 0, preset_force_key);
    add_keyboard_short_cut (0, KEY_2_PAD, 0, preset_force_key);
    add_keyboard_short_cut (0, KEY_3_PAD, 0, preset_force_key);
    add_keyboard_short_cut (0, KEY_4_PAD, 0, preset_force_key);
    add_keyboard_short_cut (0, KEY_5_PAD, 0, preset_force_key);
    add_keyboard_short_cut (0, KEY_6_PAD, 0, preset_force_key);
    add_keyboard_short_cut (0, KEY_7_PAD, 0, preset_force_key);
    add_keyboard_short_cut (0, KEY_8_PAD, 0, preset_force_key);
    add_keyboard_short_cut (0, KEY_9_PAD, 0, preset_force_key);
    return D_O_K;
  }
  return 0;
}


int ref_magnets_rot(void)
{
  int i;
  float r = prev_mag_rot;//n_rota;
  if (updating_menu_state != 0)
  {
    add_keyboard_short_cut(0, KEY_F5, 0, zmag_step_up);
    add_keyboard_short_cut(0, KEY_F6, 0, zmag_small_step_up);
    add_keyboard_short_cut(0, KEY_F7, 0, zmag_small_step_dwn);
    add_keyboard_short_cut(0, KEY_F8, 0, zmag_step_dwn);
    add_keyboard_short_cut(0, KEY_F9, 0, rot_step_up);
    add_keyboard_short_cut(0, KEY_F10, 0, rot_small_step_up);
    add_keyboard_short_cut(0, KEY_F11, 0, rot_small_step_dwn);
    add_keyboard_short_cut(0, KEY_F12, 0, rot_step_dwn);
    add_keyboard_short_cut('z', KEY_Z, 0, preset_zmag_key);
    add_keyboard_short_cut(KEY_Z, 0, 0, preset_zmag_key_ctrl);
    add_keyboard_short_cut((KEY_Z << 8), 0, 0, preset_zmag_key_alt);
    add_keyboard_short_cut(0, KEY_S, 0, zmag_bd_search_key);
    add_keyboard_short_cut (0, KEY_0_PAD, 0, preset_force_key);
    add_keyboard_short_cut (0, KEY_1_PAD, 0, preset_force_key);
    add_keyboard_short_cut (0, KEY_2_PAD, 0, preset_force_key);
    add_keyboard_short_cut (0, KEY_3_PAD, 0, preset_force_key);
    add_keyboard_short_cut (0, KEY_4_PAD, 0, preset_force_key);
    add_keyboard_short_cut (0, KEY_5_PAD, 0, preset_force_key);
    add_keyboard_short_cut (0, KEY_6_PAD, 0, preset_force_key);
    add_keyboard_short_cut (0, KEY_7_PAD, 0, preset_force_key);
    add_keyboard_short_cut (0, KEY_8_PAD, 0, preset_force_key);
    add_keyboard_short_cut (0, KEY_9_PAD, 0, preset_force_key);
return D_O_K;
}

    i = win_scanf("Enter the new reference \n of rotation number %f", &r);

    if (i == WIN_CANCEL)  return OFF;

    i = win_printf("are you sure that you want to \n"
                   "change reference of rotation number to %g", r);

    if (i == WIN_CANCEL)  return OFF;

    i = immediate_next_available_action(CHG_ROT_REF, r);

    if (i < 0)    my_set_window_title("Could not add pending action!");

    //n_rota = r;
    //set_rot_ref_value(n_rota);
    return D_O_K;
    //return dump_to_specific_log_file_with_time(CURRENT_LOG_FILE,"{\\bf Reference Rotation number changed %g}\n",n_rota);
}

int inc_magnets_rot(void)
{
    int i;
    float r = prev_mag_rot;//n_rota;
    static float dr = 10;
    char c[256] = {0};

    if (updating_menu_state != 0)  return D_O_K;

    snprintf(c, 256, "the rotation number is presently %g {\\sl turns} \n"
             "increment this value by %%f", r);

    i = win_scanf(c, &dr);

    if (i == WIN_CANCEL)  return OFF;

    if (r > 1000 || r < -1000)
        i = win_printf("the new rot position %f\n"
                       "may take some time to reach !\n"
                       " do you really want to go there ?", r);

    if (i == WIN_CANCEL)  return OFF;


    r = prev_mag_rot + dr; // n_rota
    immediate_next_available_action(MV_ROT_ABS, r);
    return D_O_K;
    //return dump_to_specific_log_file_with_time(CURRENT_LOG_FILE,"Rotation number incremented to %g\n",n_rota);
}


int do_search_coilable_bead(void)
{
    int i;
    float zlow = 14, zhigh = 16;
    int do_again = 1, act = 0, cl, color; // , tobe_turned = 0
    imreg *imr = NULL;;
    O_i *oi = NULL;
    pltreg *pr = NULL;
    O_p *op = NULL;
    char buf[1024] = {0};
    //extern int      (*motion_action)(int mode);
    //static long noaction = 0;
    float key_rot = 0;
    DIALOG *di = NULL;

    if (updating_menu_state != 0)  return D_O_K;

    ac_grep(cur_ac_reg, "%im%oi", &imr, &oi);
    ac_grep(cur_ac_reg, "%pr%op", &pr, &op);

    if (imr == NULL && pr == NULL)    return OFF;

    bead_search_low = get_config_float("bead.search", "zmaglow", bead_search_low);
    bead_search_high = get_config_float("bead.search", "zmaghigh", bead_search_high);

    zlow = bead_search_low;
    zhigh = bead_search_high;
    act = bead_search;
    i = win_scanf("Search bead inactive %R active by mouse %r by S key %r\n"
                  "enter low magnet position (right button) %6f\n "
                  "enter high magnet position (left button) %6f\n"
                  "(keyboad short cut S)\n",
                  &act, &zlow, &zhigh);

    if (i == WIN_CANCEL)  return OFF;

    bead_search_low = zlow;
    bead_search_high = zhigh;
    bead_search = act;

    set_config_float("bead.search", "zmaglow", zlow);
    set_config_float("bead.search", "zmaghigh", zhigh);

    if (bead_search == 1)
    {
        if (search_bitmap == NULL)
            search_bitmap = create_bitmap(80, 60);

        cl = Lightmagenta;
        color = makecol(EXTRACT_R(cl), EXTRACT_G(cl), EXTRACT_B(cl));
        clear_to_color(search_bitmap, color);
        textprintf_centre_ex(search_bitmap, large_font, 40, 14, Lightgreen, cl,
                             "Bead");
        textprintf_centre_ex(search_bitmap, large_font, 40, 30, Lightgreen, cl,
                             "X");
        textprintf_centre_ex(search_bitmap, large_font, 40, 46, Lightgreen, cl,
                             "search");
        //switch_to_memory_buffer(screen);
        scare_mouse();
        set_mouse_sprite(search_bitmap);
        set_mouse_sprite_focus(40, 30);
        unscare_mouse();
    }
    else if (search_bitmap != NULL)
    {
        scare_mouse();
        set_mouse_sprite_focus(0, 0);
        set_mouse_sprite(NULL);
        unscare_mouse();
        destroy_bitmap(search_bitmap);
        search_bitmap = NULL;
    }

    return 0;

    //write_cfg(hard_cfg);
    do
    {
        ac_grep(cur_ac_reg, "%im%oi", &imr, &oi);
        ac_grep(cur_ac_reg, "%pr%op", &pr, &op);

        if (mouse_b & 0x01)
            immediate_next_available_action(MV_ZMAG_ABS, zhigh);//  go_and_dump_z_magnet(zhigh);

        if (mouse_b & 0x02)
            immediate_next_available_action(MV_ZMAG_ABS, zlow);//   go_and_dump_z_magnet(zlow);

        if (keypressed())
        {
            do_again++;

            if (key[KEY_RIGHT])
            {
                key_rot = what_is_present_rot_value();
                key_rot += ((key_shifts & KB_SHIFT_FLAG) == 0) ? 1 : 0.25;
                //tobe_turned = (fabs(what_is_present_rot_value() - key_rot)) > 0.005 ? 2 : 0;


                immediate_next_available_action(MV_ROT_ABS, key_rot);
                snprintf(buf, 1024, "Obj : %g Zmag %g rotation %g", read_last_Z_value(), prev_zmag, key_rot); // n_magnet_z
		my_set_window_title("%s",buf);



                //          set_rot_value(key_rot);
                //display_title_message("going to  = %g To be rot %d",key_rot,tobe_turned);
            }

            if (key[KEY_LEFT])
            {
                key_rot = what_is_present_rot_value();
                key_rot -= ((key_shifts & KB_SHIFT_FLAG) == 0) ? 1 : 0.25;
                //tobe_turned = (fabs(what_is_present_rot_value() - key_rot)) > 0.005 ? 2 : 0;
                immediate_next_available_action(MV_ROT_ABS, key_rot);
                snprintf(buf, 1024, "Obj : %g Zmag %g rotation %g", read_last_Z_value(), prev_zmag, key_rot); // n_magnet_z
		my_set_window_title("%s",buf);

                //          set_rot_value(key_rot);
                //display_title_message("going to  = %g To be rot %d",key_rot,tobe_turned);
            }

            if ((key[KEY_PLUS_PAD]) && ((key_shifts & KB_SHIFT_FLAG) == 0))
                big_inc_Z_value(+1);

            if ((key[KEY_MINUS_PAD]) && ((key_shifts & KB_SHIFT_FLAG) == 0))
                big_inc_Z_value(-1);

            if ((key[KEY_PLUS_PAD]) && (key_shifts & KB_SHIFT_FLAG))
                small_inc_Z_value(1);

            if ((key[KEY_MINUS_PAD]) && (key_shifts & KB_SHIFT_FLAG))
                small_inc_Z_value(-1);

            if (key[KEY_ESC]) do_again = 0;

            clear_keybuf();
        }

        /*
        if (event.flags & (M_MOTION | M_BUTTON_DOWN))
        if (motion_action != NULL) motion_action(1);
             */
        //refresh_pc_screen(CR);
        if (imr != NULL && oi != NULL)
            track_oi_idle_action(oi, dtid.dimr);

        if (pr != NULL && op != NULL && op->op_idle_action != NULL)
        {
            di = find_dialog_associated_to_pr(pr, NULL);

            if (di != NULL) op->op_idle_action(op, di);
        }


        //display_title_message("Bead searching Rot %g Z mag %g mm",n_rota,n_magnet_z);
    }
    while (do_again);

    return 0;
}

int zmag_bd_search_key(void)
{
    if (updating_menu_state != 0)        return D_O_K;

    if (bead_search != 2) return 0;

    if (fabs(prev_zmag - bead_search_low) > fabs(prev_zmag - bead_search_high))
        immediate_next_available_action(MV_ZMAG_ABS, bead_search_low);
    else immediate_next_available_action(MV_ZMAG_ABS, bead_search_high);

    return D_O_K;
}




int draw_estimated_force_versus_zmag_manip(void)
{
    int  j;
    //b_track *bt = NULL;
    pltreg *pr = NULL;
    O_p *op;
    d_s *ds;
    float kxl, dkxl, zm;

    ac_grep(cur_ac_reg, "%pr", &pr);

    if (updating_menu_state != 0)        return D_O_K;

    if (pr == NULL)return 0;


    op = create_and_attach_one_plot(pr, 125, 125, 0);
    ds = op->dat[0];
    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_plot_symb(ds, "\\pt5\\oc");
    set_plot_title(op, "Estimated Force curve");
    set_plot_x_title(op, "Zmag (mm)");
    set_plot_y_title(op, "Estimated force (pN)");
    set_plot_file(op, "ZmagForce%s", ".gr");
    set_ds_source(ds, "Estimated Force\nZ mag ");

    for (j = 0; j < 125; j++)
    {
        zm = -0.1 - ((float)j) / 25;
        kxl= convert_only_zmag_to_force(zm, 0);
        dkxl = kxl * 0.15;
        add_new_point_with_y_error_to_ds(ds, zm,  kxl, dkxl);
    }

    switch_plot(pr, pr->n_op - 1);
    return refresh_plot(pr, pr->n_op - 1);
}



int set_motor_speed_to_max(void)
{
  if (updating_menu_state != 0)        return D_O_K;
  set_zmag_motor_speed_generic(10.);
  return 0;
}

int set_motor_speed_to_min(void)
{
    if (updating_menu_state != 0)        return D_O_K;
#ifdef RAWHID_V2
  set_zmag_motor_speed_generic(0.05);
#else
  set_zmag_motor_speed_generic(0.125);
#endif
    return 0;
}


MENU *magnetscontrol_plot_menu(void)
{
    static MENU mn[32] = {0}, ref[32] = {0}, *pm = NULL;

    if (mn[0].text != NULL) return mn;

#ifdef ZMAG_THREAD
    add_item_to_menu(ref, "test thread", do_launch_zmag_thread, NULL, 0, NULL);
#endif
#ifndef RAWHID_V2
    add_item_to_menu(ref, "Set Z origin", do_set_z_origin, NULL, 0, NULL);
    add_item_to_menu(ref, "Check Z origin", do_check_z_origin, NULL, 0, NULL);
#endif
#ifndef PICOTWIST
    add_item_to_menu(ref, "Set translation factor", translation_factor_magnets_zmag, NULL, 0, NULL);
#endif
#ifndef RAWHID_V2
    add_item_to_menu(ref, "Set rotation reference", ref_magnets_rot, NULL, 0, NULL);
    add_item_to_menu(ref, "Set Z magnet reference", ref_magnets_zmag, NULL, 0, NULL);
    snprintf(ref_pos, 512, "Goto to ref");
    add_item_to_menu(ref, ref_pos, do_go_to_vcap_z_ref, NULL, 0, NULL);
#endif
#ifdef RAWHID_V2
    add_item_to_menu(mn, "Magnets shortcut", magnets_keyboard_shortcuts, NULL, 0, NULL);
    add_item_to_menu(mn, "Set Z magnet reference", ref_magnets_zmag, NULL, 0, NULL);
#endif
    pm = preset_mag_z(-3, -0.125, 0.25);
    add_item_to_menu(mn, "Read position", read_magnets, NULL, 0, NULL);
    add_item_to_menu(mn, "Set magnets z position", set_magnets_z, NULL, 0, NULL);
    add_item_to_menu(mn, "Park magnets", park_magnets, NULL, 0, NULL);
    add_item_to_menu(mn, "Preset Zmag", NULL, pm, 0, NULL);
    add_item_to_menu(mn, "\0",   NULL,           NULL,   0, NULL);
    add_item_to_menu(mn, "Set estimated force", set_magnets_z_to_force, NULL, 0, NULL);
    add_item_to_menu(mn, "Preset estimated force", NULL, preset_force_menu(), 0, NULL);
    add_item_to_menu(mn, "Draw estimated force vs zmag", draw_estimated_force_versus_zmag_manip, NULL, 0, NULL);
    add_item_to_menu(mn, "\0",   NULL,           NULL,   0, NULL);
#ifndef RAWHID_V2
    add_item_to_menu(mn, "Set magnets rotation", set_magnets_rot, NULL, 0, NULL);
    add_item_to_menu(mn, "Increment rotation", inc_magnets_rot, NULL, 0, NULL);
    add_item_to_menu(mn, "\0",   NULL,           NULL,   0, NULL);
    add_item_to_menu(mn, "References", NULL, ref, 0, NULL);
    add_item_to_menu(mn, "\0",   NULL,           NULL,   0, NULL);
    snprintf(max_pos, 512, "set maximum Z position");
    add_item_to_menu(mn, max_pos, set_max_abs_zmag, NULL, 0, NULL);
    add_item_to_menu(mn, "\0",   NULL,           NULL,   0, NULL);
#endif
    snprintf(m_speed, 512, "Motors speed");
    add_item_to_menu(mn, m_speed, set_magnets_rot_velocity, NULL, 0, NULL);
#ifndef RAWHID_V2
    add_item_to_menu(mn, "Zmag PID settings", set_zmag_pid, NULL, 0, NULL);
    add_item_to_menu(mn, "Rotation PID settings", set_rot_pid, NULL, 0, NULL);
#endif
    add_item_to_menu(mn, "Search bead", do_search_coilable_bead, NULL, 0, NULL);
    add_item_to_menu(mn, "Magnets short cuts", preset_zmag_key_set, NULL, 0, NULL);

    add_item_to_menu(quicklink_menu, "Set max magnet speed", set_motor_speed_to_max, NULL, 0, NULL);
    add_item_to_menu(quicklink_menu, "Set min magnet speed", set_motor_speed_to_min, NULL, 0, NULL);
    add_item_to_menu(quicklink_menu, "Search Bead", do_search_coilable_bead, NULL , 0, NULL);
    add_item_to_menu(quicklink_menu, "Magnets shortcuts", preset_zmag_key_set, NULL, 0, NULL);



    return mn;
}



int magnetscontrol_main(int argc, char **argv)
{
    (void)argc;
    (void)argv;
    set_magnetscontrol_device(&(Pico_param.translation_motor_param), &(Pico_param.rotation_motor_param));
    Open_Pico_config_file();
    absolute_maximum_zmag = get_config_float("STATE", "AbsoluteMaximumZmag", 20);
    Close_Pico_config_file();
    //Pico_param.translation_motor_param.translation_max_pos = m;
    read_magnet_z_value();
    read_rot_value();
# ifdef DYNAMIC
    add_image_treat_menu_item("Magnets", NULL, magnetscontrol_plot_menu(), 0, NULL);
# endif
    if (has_magnets_memory() == 0)
    {
        Open_Pico_config_file();
        prev_mag_rot = get_config_float("STATE", "MagRot", 0);
        prev_zmag = get_config_float("STATE", "MagZ", 0);
        absolute_maximum_zmag = get_config_float("STATE", "AbsoluteMaximumZmag", -0.1);
        Close_Pico_config_file();
        last_n_rota = prev_mag_rot;
        n_rota_inst = prev_mag_rot;
        n_rota = prev_mag_rot + n_rot_offset;
        n_magnet_z_inst = last_n_magnet_z = prev_zmag;
        n_magnet_z = prev_zmag + n_magnet_offset;


        v_rota = get_config_float("ROTATION_MOTOR", "rotation_velocity", 1);
        //win_printf ("read v_rota = %g from cfg",v_rota);
        set_motors_speed(v_rota);
        v_mag = get_config_float("TRANSLATION_MOTOR", "translation_velocity", 1);

        //win_printf ("read v_zmag = %g from cfg",v_mag);
        //tv : vmag set by generic function (apt driven) 09/02/17
        if (set_zmag_motor_speed != NULL)   set_zmag_motor_speed(v_mag);
#ifndef RAWHID_V2
        set_magnet_z_value(prev_zmag);
#endif
        set_rot_value(prev_mag_rot);
        //win_printf("magnet from config  %g rot %g",prev_zmag,prev_mag_rot);
    }
    else
    {
        prev_zmag = read_magnet_z_value();
        prev_mag_rot = read_rot_value();
        v_rota = get_motors_speed();


        if (get_zmag_motor_speed != NULL)
            v_mag = get_zmag_motor_speed();

        if (get_Vcap_min != NULL)
            Mag_vcap_min = get_Vcap_min();

        //win_printf("magnet rotation %g",prev_mag_rot);
    }

#ifdef ZMAG_THREAD
launch_zmag_thread();
#endif
    return D_O_K;
}
int shut_down_magnets(void)
{
    if (has_magnets_memory() == 0)
    {
        Open_Pico_config_file();
        set_config_float("STATE", "MagRot", prev_mag_rot);
        set_config_float("STATE", "MagZ", prev_zmag);
        set_config_float("STATE", "AbsoluteMaximumZmag", absolute_maximum_zmag);
        write_Pico_config_file();
        Close_Pico_config_file();
    }

    /*
    else
      {
        allegro_message("Not Saving position");
        my_set_window_title("Not Saving position");
      }
    */
    return 0;
}


/*
to add

int do_search_coilable_bead(void)
{
    int i;
    static float zlow = 14, zhigh = 16;
    int do_again = 1, tobe_turned = 0;
    extern int      (*motion_action)(int mode);
    static long noaction = 0;
    float key_rot = 0;
    int refresh_pc_screen(char ch);

    if(ch != CR) return OFF;

    zlow = get_cfg_float(hard_cfg,"bead.search","zmaglow", zlow);
    zhigh = get_cfg_float(hard_cfg,"bead.search","zmaghigh", zhigh);


    i = win_scanf("enter low magnet pos (right button) %f "
              "enter high magnet pos (left button) %f",&zlow,&zhigh);

    if (i == WIN_CANCEL)    return OFF;
    set_cfg_float(hard_cfg,"bead.search","zmaglow", zlow);
    set_cfg_float(hard_cfg,"bead.search","zmaghigh", zhigh);
    write_cfg(hard_cfg);
    do
    {
        MyGetEvent((M_MOTION | M_BUTTON_DOWN | M_KEYPRESS | M_POLL), &event);
        if (event.flags & (M_MOTION | M_BUTTON_DOWN | M_KEYPRESS))
            if (motion_action != NULL)  motion_action(0);
        if (event.flags & (M_LEFT_DOWN))
        {
            last_m_event = event;
            event.flags = 0;
            go_and_dump_z_magnet(zhigh);
        }
        if (event.flags & (M_RIGHT_DOWN))
        {
            last_m_event = event;
            event.flags = 0;
            go_and_dump_z_magnet(zlow);
        }

        if (event.flags & M_KEYPRESS)
        {
            do_again++;

            if (event.key == RIGHT_AR)
              {
                key_rot = what_is_present_rot_value();
                key_rot += ((event.kbstat & KB_SHIFT) == 0) ? 1 : 0.25;
                tobe_turned = (fabs(what_is_present_rot_value() - key_rot)) > 0.005 ? 2 : 0;
                set_rot_value(key_rot);
                display_title_message("going to  = %g To be rot %d",key_rot,tobe_turned);
              }
            if (event.key == LEFT_AR)
              {
                key_rot = what_is_present_rot_value();
                key_rot -= ((event.kbstat & KB_SHIFT) == 0) ? 1 : 0.25;
                tobe_turned = (fabs(what_is_present_rot_value() - key_rot)) > 0.005 ? 2 : 0;
                set_rot_value(key_rot);
                display_title_message("going to  = %g To be rot %d",key_rot,tobe_turned);
              }

            if (event.key == '+' && (event.kbstat & KB_SHIFT) == 0)
                big_inc_Z_value(+1);
            if (event.key == '-' && (event.kbstat & KB_SHIFT) == 0)
                big_inc_Z_value(-1);
            if (event.key == '+' && event.kbstat & KB_SHIFT)
                small_inc_Z_value(1);
            if (event.key == '-' && event.kbstat & KB_SHIFT)
                small_inc_Z_value(-1);
            delta_t = noaction + event.dtime;
            if (event.key == ESC ) do_again = 0;
            last_m_event = event;
            event.flags = 0;
        }
        if (event.flags & (M_MOTION | M_BUTTON_DOWN))
            if (motion_action != NULL)  motion_action(1);
        refresh_pc_screen(CR);
        display_title_message("Bead searching Rot %g Z mag %g mm",n_rota,n_magnet_z);
    }while(do_again);
    return 0;
}

int do_search_spike_bead(char ch)
{
    int i;
    static float zlow = 0, zhigh = 1;
    int do_again = 1;
    extern int      (*motion_action)(int mode);
    static long noaction = 0;
    int refresh_pc_screen(char ch);

    if(ch != CR) return OFF;

    zlow = get_config_float("bead.search","rotleft", zlow);
    zhigh = get_config_float("bead.search","rotright", zhigh);


    i = win_scanf("enter first  rot magnet pos (left button) %f"
        "enter second rot  magnet pos (right button) %f",&zlow,&zhigh);

    if (i == WIN_CANCEL) return OFF;
    set_cfg_float(hard_cfg,"bead.search","rotleft", zlow);
    set_cfg_float(hard_cfg,"bead.search","rotright", zhigh);
    write_cfg(hard_cfg);
    do
    {
        MyGetEvent((M_MOTION | M_BUTTON_DOWN | M_KEYPRESS | M_POLL), &event);
        if (event.flags & (M_MOTION | M_BUTTON_DOWN | M_KEYPRESS))
            if (motion_action != NULL)  motion_action(0);
        if (event.flags & M_KEYPRESS)
        {
            if (event.key == ESC ) do_again = 0;
            last_m_event = event;
            event.flags = 0;
        }
        if (event.flags & (M_LEFT_DOWN))
        {
            last_m_event = event;
            event.flags = 0;
            go_and_dump_rot(zlow);
        }
        if (event.flags & (M_RIGHT_DOWN))
        {
            last_m_event = event;
            event.flags = 0;
            go_and_dump_rot(zhigh);
        }
        if (event.flags & M_KEYPRESS)
        {
            do_again++;
            if (event.key == '+' && (event.kbstat & KB_SHIFT) == 0)
                big_inc_Z_value(+1);
            if (event.key == '-' && (event.kbstat & KB_SHIFT) == 0)
                big_inc_Z_value(-1);
            if (event.key == '+' && event.kbstat & KB_SHIFT)
                small_inc_Z_value(1);
            if (event.key == '-' && event.kbstat & KB_SHIFT)
                small_inc_Z_value(-1);
            if (event.key == 's')
                return 0;

            delta_t = noaction + event.dtime;
            last_m_event = event;
            event.flags = 0;
        }
        if (event.flags & (M_MOTION | M_BUTTON_DOWN))
            if (motion_action != NULL)  motion_action(1);
        refresh_pc_screen(CR);
        display_title_message("Bead spiking Rot %g Z mag %g mm",n_rota,n_magnet_z);
    }while(do_again);
    return 0;
}


 */

# endif
