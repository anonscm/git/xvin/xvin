#ifndef _REAL_TIME_AVG_C_
#define _REAL_TIME_AVG_C_


# include "allegro.h"
# include "winalleg.h"
# include "ctype.h"
# include "xvin.h"
# include "fftl32n.h"
# include "fillib.h"
# include <fftw3.h>		// include the fftw v3.0.1 or v3.1.1 library for computing spectra:
# include "../fft2d/fft2d.h"
# include "../cellcount/cellcount.h"
# include "../cellcount/cellcount_img.h"
# include "pico_serial/pico_serial.h"


/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "../cfg_file/Pico_cfg.h"
//# include "../phaseindex/phaseindex.h"
#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)

# include "PlayItSam.h"
# include "real_time_avg.h"
# include "action_live_video.h"
//# include "focus.h"

int im_ci(void) // required by pico_serial
{ return 0;}

int check_heap_raw(char *file, unsigned long line)
{
  int out;

  out = _heapchk();
  if (out != -2)  win_printf("File %s at line %d\nheap satus %d (-2 =>OK)\n",file,(int)line,out);
  return (out == -2) ? 0 : out;
}

// Draw rois
int draw_rtd_roi(S_l *s_l, O_i *oi, void *imr)
{
  int xp, yp, wx, wy;
  wx = real_time_info->wx;
  wy = real_time_info->wy;
  xp = x_imdata_2_imr((imreg*)imr, real_time_info->xd0);
  yp = y_imdata_2_imr((imreg*)imr, real_time_info->yd0)+16 + wy/2;

/*   my_set_window_title("x0 %d    xp %d    wx %d\n" */
/* 	     "y0 %d    yp %d    wy %d", x0, xp, wx, y0, yp, wy); */
  for(;screen_acquired;); // we wait for screen_acquired back to 0;     
  screen_acquired = 1;
  acquire_bitmap(screen);
  rect(screen, xp, yp, xp+wx, yp-wy, makecol(0,0,255));
  release_bitmap(screen);
  screen_acquired = 0;  
  return 0;
}
int draw_rtf_roi(S_l *s_l, O_i *oi, void *imr)
{
  int xp, yp, wx, wy;
  wx = real_time_info->wx;
  wy = real_time_info->wy;
  xp = x_imdata_2_imr((imreg*)imr, real_time_info->xf0);
  yp = y_imdata_2_imr((imreg*)imr, real_time_info->yf0)+16 + wy/2;

/*   my_set_window_title("x0 %d    xp %d    wx %d\n" */
/* 	     "y0 %d    yp %d    wy %d", x0, xp, wx, y0, yp, wy); */
  for(;screen_acquired;); // we wait for screen_acquired back to 0;     
  screen_acquired = 1;  
  acquire_bitmap(screen);
  rect(screen, xp, yp, xp+wx, yp-wy, makecol(0,0,255));
  release_bitmap(screen);
  screen_acquired = 0;  
  return 0;
}

// Plot idle actions
int timing_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  register int i, j, k;
  static int  immax = -1; // last_c_i = -1,
  d_s *dsx, *dsy, *dsz;
  int nf, lost_fr, lf, jstart = 0, j1 = 0;
  unsigned long   dtm, dtmax, dts;
  float   fdtm, fdtmax;
  double min_delay_camera_thread_in_ms, tmp, tmp2;
  long long llt;

  if (real_time_info == NULL) return D_O_K;
  dts = get_my_uclocks_per_sec();
  dsx = find_source_specific_ds_in_op(op,"Timing rolling buffer");
  dsy = find_source_specific_ds_in_op(op,"Period rolling buffer");
  dsz = find_source_specific_ds_in_op(op,"Timer rolling buffer");
  if (dsx == NULL || dsy == NULL || dsz == NULL) return D_O_K;	

  i = real_time_info->c_i;
  if (real_time_info->imi[i] <= immax) 
    {
        my_set_window_title("waiting %d %d",real_time_info->imi[i], immax);
	return D_O_K;   // we are up todate
    }

  min_delay_camera_thread_in_ms = bid.min_delay_camera_thread_in_ms;

  i = real_time_info->c_i;
  immax = real_time_info->imi[i];               
  lf = immax - real_time_info->ac_i;
  if (real_time_info->ac_i > (REAL_TIME_BUFFER_SIZE>>1)) 
    {
      nf = REAL_TIME_BUFFER_SIZE>>1;
      j = jstart = (real_time_info->ac_i - (REAL_TIME_BUFFER_SIZE>>1))%REAL_TIME_BUFFER_SIZE;  //last_c_i;
    }
  else
    {
      nf = real_time_info->ac_i-1;
      j = jstart = 1;
    }
  dsx->nx = dsx->ny = nf;
  dsy->nx = dsy->ny = nf;
  dsz->nx = dsz->ny = nf;
  
  j = jstart + nf - 1;
  j = (j < REAL_TIME_BUFFER_SIZE) ? j : j - REAL_TIME_BUFFER_SIZE;
  llt = real_time_info->imt[j];
  j1 = j;
  j = jstart;
  j = (j < REAL_TIME_BUFFER_SIZE) ? j : j - REAL_TIME_BUFFER_SIZE;
  llt -= real_time_info->imt[j];
  llt /= (nf - 1);
  
  for (i = 0, j = jstart, lost_fr = 0, dtm = dtmax = 0, fdtm = fdtmax = 0, k = 0, tmp2 = 0, lf = 0; i < nf; i++, j++, k++)
    {
      j = (j < REAL_TIME_BUFFER_SIZE) ? j : j - REAL_TIME_BUFFER_SIZE;
      dsx->xd[i] = dsy->xd[i] = dsz->xd[i] = real_time_info->imi[j];
      if (i > 0)
	lf += real_time_info->imi[j] - real_time_info->imi[j1] - 1;
      dsx->yd[i] = ((float)(1000*real_time_info->imdt[j]))/dts;
      tmp = real_time_info->imt[j];
      tmp -= (i == 0) ? real_time_info->imt[j] : real_time_info->imt[j1];
      tmp = 1000*(double)(tmp)/get_my_ulclocks_per_sec();
      //tmp -= bid.image_per_in_ms * i;
      tmp2 += tmp;
      dsy->yd[i] = (float)tmp;
      dsz->yd[i] = real_time_info->imit[j];

      dtm += real_time_info->imdt[j];
      fdtm += dsx->yd[i];
      dtmax = (real_time_info->imdt[j] > dtmax) ? real_time_info->imdt[j] : dtmax;
      fdtmax = (dsx->yd[i] > fdtmax) ? dsx->yd[i] : fdtmax;
      j1 = j;
    }
  dsy->yd[0] =  dsy->yd[1];
  op->need_to_refresh = 1; 
  if (k) set_plot_title(op, "\\stack{{fr position lac_i %d }{%d missed images}"
			"{\\pt8 dt mean %6.3f ms (%6.3f ms) max %6.3f (%6.3f)}}"
			,bid.first_im +real_time_info->ac_i 
			,lf,fdtm/k,
			1000*((double)(dtm/k))/dts,
			fdtmax, 1000*((double)(dtmax))/dts);
  i = REAL_TIME_BUFFER_SIZE/8;
  j = ((int)dsx->xd[1])/i;
  op->x_lo = j*i;
  op->x_hi = (j+5)*i;
  set_plot_x_fixed_range(op);
  return refresh_plot(pr_LIVE_VIDEO, UNCHANGED);
}


	
int obj_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  register int i, j;
  static int last_c_i = -1, immax = -1;
  d_s *dsx;
  int nf;
  
  if (real_time_info == NULL) return D_O_K;

  dsx = find_source_specific_ds_in_op(op,"Obj. rolling buffer");
  if (dsx == NULL) return D_O_K;	

  i = real_time_info->c_i;
  if (real_time_info->imi[i] <= immax) return D_O_K;   // we are uptodate
  immax = real_time_info->imi[i];               
  nf = (real_time_info->ac_i > REAL_TIME_BUFFER_SIZE) ? REAL_TIME_BUFFER_SIZE : real_time_info->ac_i;
  nf = (nf > (REAL_TIME_BUFFER_SIZE>>1)) ? REAL_TIME_BUFFER_SIZE>>1 : nf;
  dsx->nx = dsx->ny = nf;
  last_c_i = (real_time_info->c_i > 0) ? real_time_info->c_i - 1 : 0;  // c_i is not yet valid
  for (i = nf-1, j = last_c_i; i >= 0; i--, j--)
    {
      j = (j < 0) ? REAL_TIME_BUFFER_SIZE + j: j;
      j = (j < REAL_TIME_BUFFER_SIZE) ? j : REAL_TIME_BUFFER_SIZE;
      dsx->xd[i] = real_time_info->imi[j];
      dsx->yd[i] = real_time_info->obj_pos[j];
    }
  i = REAL_TIME_BUFFER_SIZE/8;
  j = ((int)dsx->xd[i])/i;
  op->x_lo = (j-1)*i;
  op->x_hi = (j+4)*i;
  set_plot_x_fixed_range(op);

  op->need_to_refresh = 1; 
  set_plot_title(op, "\\stack{{image %d}{Obj %g obj}}"
		 ,real_time_info->ac_i, real_time_info->obj_pos[last_c_i]);
  return refresh_plot(pr_LIVE_VIDEO, UNCHANGED);
}

	
int raw_histo_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  register int i;
  d_s *dsx, *dsy;
/*   int nf, im, itmp; */
  static int immax = -1, wi, min;
  
  if (real_time_info == NULL) return D_O_K;

  i = real_time_info->c_i;
  if (real_time_info->imi[i] <= immax) return D_O_K;   // we are uptodate
  immax = real_time_info->imi[i];               

  if (real_time_info->raw_histo_update == 1) return D_O_K;   // we are uptodate

  dsx = find_source_specific_ds_in_op(op,"Raw bar histogram");
  if (dsx == NULL) return D_O_K;	
  dsy = find_source_specific_ds_in_op(op,"Raw histogram");
  if (dsx == NULL) return D_O_K;	

  if (op->title) 
    if (sscanf(op->title, "\\stack{{Raw histo}{min %d width %d}}",&min,&wi) == 2)
      {
	real_time_info->raw_min = min;
	real_time_info->raw_wi = wi;
      }
  display_histo_in_op(real_time_info->raw_histotmp, dsx, dsy, 256, 1.0);

  op->need_to_refresh = 1; 
  set_plot_title(op, "\\stack{{Raw histo}{min %d width %d}}", real_time_info->raw_min, real_time_info->raw_wi);
  real_time_info->raw_histo_update = 1;
  return refresh_plot(pr_LIVE_VIDEO, UNCHANGED);
}

	
int bp_pxl_correl_rolling_buffer_idle_action(O_p *op, DIALOG *d)
// autocorrelation is computed on diffused light beam by suming the autocorrel of each averaged pixel
{
  register int i, j, jim;
  static int last_c_i = -1, immax = -1;// itmp, x, y;
  d_s *dsx;
  int nf, ipx, ipy;
  double re, im, mean;
  static double  	*in1=NULL;
  static fftw_complex	*outc1=NULL;
  static fftw_plan     plan1;     // fft plan for fftw3
  static int n_fftw = 0;
  
  if (real_time_info == NULL) return D_O_K;

  dsx = find_source_specific_ds_in_op(op,"Band passed small image correlation");
  if (dsx == NULL) return D_O_K;

  i = real_time_info->c_i;
  if (real_time_info->imi[i] <= immax) return D_O_K;   // we are uptodate
  immax = real_time_info->imi[i];

  if (real_time_info->ac_i < real_time_info->nf_autocorr) return 0;
  nf = real_time_info->nf_autocorr;
  //nf = (real_time_info->ac_i > real_time_info->nf_autocorr) ? real_time_info->nf_autocorr : real_time_info->ac_i;

  for (i = 0; i < nf; i++)       dsx->xd[i] = 0;
  for (ipy = 0; ipy < real_time_info->bp_ny; ipy++)
    {
      for (ipx = 0; ipx < real_time_info->bp_nx; ipx++)
	{
	  dsx->nx = dsx->ny = nf;
	  last_c_i = (real_time_info->c_i > 0) ? real_time_info->c_i - 1 : 0;  // c_i is not yet valid
	  // this loop allows to find the jim corresponding to i
	  // avg rms is stored along time ds->yd
	  for (i = nf-1, j = last_c_i, jim = real_time_info->mc_i; i >= 0; i--, j--, jim--)
	    {
	      j = (j < 0) ? REAL_TIME_BUFFER_SIZE + j: j;
	      j = (j < REAL_TIME_BUFFER_SIZE) ? j : REAL_TIME_BUFFER_SIZE;
	      //dsx->xd[i] = real_time_info->imi[j];
	      jim = (jim < 0) ? real_time_info->nf_autocorr + jim: jim;
	      jim = (jim < real_time_info->nf_autocorr) ? jim : real_time_info->nf_autocorr-1;
	      dsx->yd[i] = real_time_info->oidm->im.pxl[jim][ipy].li[ipx];
	      dsx->yd[i] /= real_time_info->fsize*real_time_info->fsize;
	    }

	  // fft init and alloc
	  if (n_fftw < nf || in1 == NULL || outc1 == NULL)
	    {
	      in1   = fftw_malloc( nf * sizeof(double));
	      outc1 = fftw_malloc((nf/2+1)*sizeof(fftw_complex));
	      if (in1 == NULL || outc1 == NULL)  
		return win_printf_OK("cannot create fftw arrays !");
	      n_fftw = nf;
	    }
	  plan1 = fftw_plan_dft_r2c_1d(nf, in1, outc1, FFTW_ESTIMATE);
	  
	  // store data to be filtered (- mean) and run fft
	  for (i = 0, mean = 0; i < nf; i++) 
	    {
	      in1[i] = dsx->yd[i]; 
	      mean += in1[i];
	    }
	  for (i = 0, mean /= nf; i < nf; i++) 
	    in1[i] -= mean; 
	  fftw_execute(plan1);
	  
	  // compute correlation in Fourier space
	  outc1[0][0] = outc1[0][0]*outc1[0][0];
	  outc1[0][1] = 0;
	  for (i = 1; i < nf/2; i++) 
	    {
	      re = outc1[i][0]*outc1[i][0] + outc1[i][1]*outc1[i][1]; 
	      im = 0; //outc1[i][0]*outc1[i][1] - outc1[i][1]*outc1[i][0]; 
	      outc1[i][0] = re;
	      outc1[i][1] = im;
	    }
	  outc1[nf/2][0] = outc1[nf/2][0]*outc1[nf/2][0];
	  outc1[nf/2][1] = 0;
	  
	  // store sum of correlation over x and y (in real space) in ds->xd
	  fftw_destroy_plan(plan1);
	  plan1 = fftw_plan_dft_c2r_1d(nf, outc1, in1, FFTW_ESTIMATE);   
	  fftw_execute(plan1);
	  for (i = 0; i < nf; i++) 
	    dsx->xd[i] += (float)(in1[i]/nf); 
	  fftw_destroy_plan(plan1);
	}
    }

  // finally put the the correl in y and index in x
  for (i = 0; i < nf; i++)       
    {
      dsx->yd[i] =  dsx->xd[i]; 
      dsx->xd[i] = (float)i; 
    }

  //fftw_free(in1); fftw_free(outc1);

  dsx->nx = dsx->ny = nf/2;
  op->need_to_refresh = 1; 
  set_plot_title(op, "Image %d  correlation of all pixels" ,real_time_info->ac_i);

  return refresh_plot(pr_LIVE_VIDEO, UNCHANGED);
}


int dsl_histo_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  register int i;
  int itmp;
  d_s *dsx, *dsy;
  float bl = 0, wi;
  static int immax = -1;
  
  if (real_time_info == NULL) return D_O_K;

  i = real_time_info->c_i;
  if (real_time_info->imi[i] <= immax) return D_O_K;   // we are uptodate
  immax = real_time_info->imi[i];               

  if (real_time_info->histo_dsl_update == 1) return D_O_K;   // we are uptodate


  dsx = find_source_specific_ds_in_op(op,"Bar Histogram of duration since last event");
  if (dsx == NULL) return D_O_K;	
  dsy = find_source_specific_ds_in_op(op,"Histogram of duration since last event");
  if (dsy == NULL) return D_O_K;

  if (op->title)
    if (sscanf(op->title,"image %d blacklevel %f width %f" ,&itmp,&bl,&wi) == 3)
      {
	real_time_info->bp_bl = bl;
	real_time_info->bp_wi = wi;
      }
  display_histo_in_op(real_time_info->histo_dsl_tmp, dsx, dsy, BP_DSL_LEVELS, (float) BP_DSL_FACTOR);

  op->need_to_refresh = 1; 

  set_plot_title(op, "image %d  blacklevel %1.2f width %1.2f"
		 ,real_time_info->ac_i,real_time_info->bp_bl,real_time_info->bp_wi);
  real_time_info->histo_dsl_update = 1;
  return refresh_plot(pr_LIVE_VIDEO, UNCHANGED);
}


int bp_histo_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  register int i;
  d_s *dsx, *dsy;
  int itmp, do_refresh = 0;
  float bl = 0, wi;
  static int immax = -1;
  double mean = 0, sig2 = 0, ncd = 0, nc0d = 0, ncf = 0, nc0f = 0; 
  double bumpd = 0, lbdmd = 0, lbd0d = 0, bumpf = 0, lbdmf = 0, lbd0f = 0;

  if (real_time_info == NULL) return D_O_K;

  i = real_time_info->c_i;
  if (real_time_info->imi[i] <= immax) return D_O_K;   // we are uptodate
  immax = real_time_info->imi[i];               

  if (op->title)
    if (sscanf(op->title,"\\stack{{Image %d  blacklevel %f width %f" ,&itmp,&bl,&wi) == 3)
      {
	real_time_info->bp_bl = bl;
	real_time_info->bp_wi = wi;
      }
  
  // Treat diffused light histo
  if (real_time_info->histo_bpd_update == 0)
    {
      do_refresh = 1;
      dsx = find_source_specific_ds_in_op(op,"Bar Histogram all band-passed pixels (diffused light)");
      if (dsx == NULL) return D_O_K;	
      dsy = find_source_specific_ds_in_op(op,"Histogram all band-passed pixels (diffused light)");
      if (dsy == NULL) return D_O_K;	

      display_histo_in_op(real_time_info->histo_bpd_tmp, dsx, dsy, BP_LEVELS, (float) BP_FACTOR);

      histo_mean_var(dsy, real_time_info->bp_bl, real_time_info->bp_wi, &mean, &sig2, &ncd);
      bumpd = sig2/mean;
      lbdmd = mean/bumpd;
      histo_lambda_zero(dsy, real_time_info->bp_bl, real_time_info->bp_wi, &lbd0d, &ncd, &nc0d);
      real_time_info->histo_bpd_update = 1;
    }

  // Treat fluorescence histo
  if (real_time_info->histo_bpf_update == 0)
    {
      do_refresh = 1;
      dsx = find_source_specific_ds_in_op(op,"Bar Histogram all band-passed pixels (fluorescence)");
      if (dsx == NULL) return D_O_K;
      dsy = find_source_specific_ds_in_op(op,"Histogram all band-passed pixels (fluorescence)");
      if (dsy == NULL) return D_O_K;

      display_histo_in_op(real_time_info->histo_bpf_tmp, dsx, dsy, BP_LEVELS, (float) BP_FACTOR);

      histo_mean_var(dsy, real_time_info->bp_bl, real_time_info->bp_wi, &mean, &sig2, &ncf);
      bumpf = sig2/mean;
      lbdmf = mean/bumpf;
      histo_lambda_zero(dsy, real_time_info->bp_bl, real_time_info->bp_wi, &lbd0f, &ncf, &nc0f);
      real_time_info->histo_bpf_update = 1;
    }

/*   raw_histo_lambda_mean(real_time_info->histo_bpd_tmp, BP_LEVELS, (float) BP_FACTOR, real_time_info->bp_bl, real_time_info->bp_wi, &tmplmd, &tmpn); */
/*   raw_histo_lambda_zero(real_time_info->histo_bpd_tmp, BP_LEVELS, (float) BP_FACTOR, real_time_info->bp_bl, real_time_info->bp_wi, &tmpl0d, &tmpn, &tmpn0); */

/*   raw_histo_lambda_mean(real_time_info->histo_bpf_tmp, BP_LEVELS, (float) BP_FACTOR, real_time_info->bp_bl, real_time_info->bp_wi, &tmplmd, &tmpn); */
/*   raw_histo_lambda_zero(real_time_info->histo_bpf_tmp, BP_LEVELS, (float) BP_FACTOR, real_time_info->bp_bl, real_time_info->bp_wi, &tmpl0f, &tmpn, &tmpn0); */

  if (do_refresh)
    {
      op->need_to_refresh = 1;
      set_plot_title(op, "\\stack{{Image %d  blacklevel %1.2f width %1.2f}"
		     "{Diff light: b=%.3g \\lambda =%.2e \\lambda_0=%.2e}"
		     "{Fluo:      b=%.3g \\lambda =%.2e \\lambda_0=%.2e}"
		     "{nc %g nc0 %g  error %.3g}}",
		     real_time_info->ac_i,real_time_info->bp_bl,real_time_info->bp_wi,
		     bumpd,lbdmd,lbd0d,
		     bumpf,lbdmf,lbd0f,
		     ncd, nc0d, (double)compute_lbd_generic_error(lbdmd, ncd, real_time_info->redundancy)/lbdmd);
    }
  
  return refresh_plot(pr_LIVE_VIDEO, UNCHANGED);
}


int gl_tl_idle_action(DIALOG *d)
// this general idle action add a new point to the time lapse plot 
// after a flag (tl_add_point) has been turned on in do_at_all_img.
// it is running all time but active only when is_tl_on == 1
{
  O_p *optl;
  d_s *dsl0d, *dslmd, *dsl0f, *dslmf;
  float t, dt, bl, wi, tmp;
  double lambda, nc, nc0;
  int redundancy;
  char fullname[1024];
  char command[128];


  // Handle stirring end
  if (real_time_info->stir & real_time_info->stirStop)
    {
      sprintf(command,"M=OFF\r"); // switch motor state
      Write_serial_port(hCom, command, strlen(command));

      real_time_info->tacq_start = my_ulclock();
      real_time_info->tdelay_start = my_ulclock();
      real_time_info->delayActive = 1;
      real_time_info->reset_histo_bp = 1;
      real_time_info->stirStop = 0;
    }

  // Handle delay (after stirring) end
  if (real_time_info->stir & real_time_info->delayStop)
    {
      real_time_info->tacq_start = my_ulclock();
      real_time_info->reset_histo_bp = 1;
      real_time_info->delayStop = 0;
    }
  
  // If time lapse running, exit
  if (real_time_info->is_tl_on == 0)
    return 0;
  if (real_time_info->tl_add_point == 0)
    return 0;

  // Else add a new point to time lapse
  optl = real_time_info->op_tl;
  if (optl == NULL) return D_O_K;


  dsl0d = find_source_specific_ds_in_op(optl,"Time lapse of lbd0 measurements (diffused light)");
  if (dsl0d == NULL) return D_O_K;
  dslmd = find_source_specific_ds_in_op(optl,"Time lapse of lbdm measurements (diffused light)");
  if (dslmd == NULL) return D_O_K;
  dsl0f = find_source_specific_ds_in_op(optl,"Time lapse of lbd0 measurements (fluorescence)");
  if (dsl0f == NULL) return D_O_K;
  dslmf = find_source_specific_ds_in_op(optl,"Time lapse of lbdm measurements (fluorescence)");
  if (dslmf == NULL) return D_O_K;
 
  if (optl->title)
    if (sscanf(optl->title, "\\stack{{Time Lapse}{blacklevel %f  width %f  redundancy %d}", &bl, &wi, &redundancy) == 3)
      {
	  real_time_info->bp_bl = bl;
	  real_time_info->bp_wi = wi;
	  real_time_info->redundancy = redundancy;
      }

  // Treat time
  dt = (float) ((real_time_info->tacq_end - real_time_info->tacq_start) / get_my_ulclocks_per_sec());
  dt /= 2;
  t = (float) ((real_time_info->tacq_end - real_time_info->t_ini) / get_my_ulclocks_per_sec()) - dt;

  // Save in ds
  // add_new_point_with_xy_error_to_ds(d_s *ds, float x, float dx, float y, float dy)
  // does plot x�dx, y�dy
  raw_histo_lambda_zero(real_time_info->histo_bpd_tmp, BP_LEVELS, (float) BP_FACTOR, real_time_info->bp_bl, real_time_info->bp_wi, &lambda, &nc, &nc0);
  add_new_point_with_xy_error_to_ds(dsl0d, t, dt, (float) lambda, compute_lbd_generic_error(lambda, nc, redundancy));
  raw_histo_lambda_mean(real_time_info->histo_bpd_tmp, BP_LEVELS, (float) BP_FACTOR, real_time_info->bp_bl, real_time_info->bp_wi, &lambda, &nc);
  add_new_point_with_xy_error_to_ds(dslmd, t, dt, (float) lambda, compute_lbd_generic_error(lambda, nc, redundancy));

  raw_histo_lambda_zero(real_time_info->histo_bpf_tmp, BP_LEVELS, (float) BP_FACTOR, real_time_info->bp_bl, real_time_info->bp_wi, &lambda, &nc, &nc0);
  add_new_point_with_xy_error_to_ds(dsl0f, t, dt, (float) lambda, compute_lbd_generic_error(lambda, nc, redundancy));
  raw_histo_lambda_mean(real_time_info->histo_bpf_tmp, BP_LEVELS, (float) BP_FACTOR, real_time_info->bp_bl, real_time_info->bp_wi, &lambda, &nc);
  add_new_point_with_xy_error_to_ds(dslmf, t, dt, (float) lambda, compute_lbd_generic_error(lambda, nc, redundancy));

  // Refresh and save time lapse plot
  set_plot_title(optl, "\\stack{{Time Lapse}{blacklevel %.2f  width %.2f  redundancy %d}{Duration %d'  max period %ds  max precision %g}}",
		 real_time_info->bp_bl,real_time_info->bp_wi, real_time_info->redundancy,
		 real_time_info->duration, real_time_info->max_tacq, real_time_info->max_prec);
  optl->need_to_refresh = 1;
  append_filename(fullname, optl->dir, optl->filename, 1024);
  save_one_plot_bin(optl, fullname);

  // House keeping
  real_time_info->tacq_start = my_ulclock();
  real_time_info->tl_add_point = 0;
  real_time_info->reset_histo_bp = 1;
  if (real_time_info->stir) // if remote stirring control, start motor 
    {
      sprintf(command,"M=ON\r"); // switch motor state
      Write_serial_port(hCom, command, strlen(command));
      real_time_info->tstir_start = my_ulclock();
      real_time_info->stirActive = 1;
    }

  // End time lapse acquisition
  tmp = (float)(real_time_info->tacq_end - real_time_info->t_ini);
  tmp /= (float) get_my_ulclocks_per_sec();
  if (tmp > (float) real_time_info->duration * 60)
    {
      stop_time_lapse_record();
      if (real_time_info->stir)
	{
	  sprintf(command,"M=OFF\r"); // switch motor state
	  Write_serial_port(hCom, command, strlen(command));
	  CloseSerialPort(hCom);
	  hCom = NULL;
	}
    }
  return D_O_K;
}


int op_tl_idle_action(O_p *op, DIALOG *d)
{
  if (real_time_info == NULL) return D_O_K;
  //op->need_to_refresh = 1;	      

  return refresh_plot(pr_LIVE_VIDEO, UNCHANGED);
}


//commands to operate on the averaged movies//
int oi_std_idle_action(O_i *oi, DIALOG *d)
{
  if (d == NULL || d->dp == NULL || d->proc == NULL)    return 1;
  if (oi->need_to_refresh)      d->proc(MSG_DRAW,d,0);	
  return 0;
}



// Menu functions

int display_info(void)
{
  char tlstr[1024];
  float tmp;

  if(updating_menu_state != 0)
    {
      add_keyboard_short_cut(0, KEY_I, 0, display_info);
      return D_O_K;
    }
  if (real_time_info == NULL)
    return win_printf_OK("data structure not ready!");

  if (real_time_info->is_tl_on == 0)
    sprintf(tlstr, "No time lapse running");
  else
    {
      tmp = (float)(my_ulclock() - real_time_info->t_ini);
      tmp /= (float) get_my_ulclocks_per_sec() * 60;
      sprintf(tlstr, "Time lapse running: %.1f' over %d'\nMax period (%d) and max precision (%f) at each measurement", tmp, real_time_info->duration, real_time_info->max_tacq, real_time_info->max_prec);
    }

  win_printf("%s beam properties\nWidth: %dpx  bin: %dpx\n"
	     "Diffused light center position: (%d, %d)\n"
	     "Fluorescence center position: (%d, %d)\n\n\n%s\n\n\n"
	     "Band pass filter\nCutoff lowpass %.2f  Lowpass width %.2f\n"
	     "Cutoff highpass %.2f  Highpass width %.2f",
	     real_time_info->rot ? "Vertical" : "Horizontal",
	     real_time_info->fwidth, real_time_info->fsize,
	     real_time_info->xdc, real_time_info->ydc,
	     real_time_info->xfc, real_time_info->yfc, tlstr,
	     real_time_info->lp, real_time_info->lw,
	     real_time_info->hp, real_time_info->hw);
  return D_O_K;
}

int zero_hist(void)
{
  if(updating_menu_state != 0)
    {
      add_keyboard_short_cut(0, KEY_R, 0, zero_hist);
      return D_O_K;
    }
  if (real_time_info == NULL)
    return win_printf_OK("data structure not ready!");

  real_time_info->reset_histo_bp = 1;
  real_time_info->reset_histo_dsl = 1;

  return D_O_K;
}

int set_beam_height(void)
{
  int i, rot, fw2, zd0, zf0;

  if(updating_menu_state != 0)	return D_O_K;
  if (real_time_info == NULL)
    return win_printf("data structure not ready!");

  rot = real_time_info->rot;
  fw2 = real_time_info->fwidth / 2;
  zd0 = (rot==0) ? real_time_info->ydc : real_time_info->xdc;
  zf0 = (rot==0) ? real_time_info->yfc : real_time_info->xfc;
  i = win_scanf("Enter diffused light beam height %6d\n"
		"Enter fluorescence beam height %6d"
		,&zd0, &zf0);
  if (i == CANCEL) return D_O_K;

  // Check boundary conditions
  if (rot == 0)
    {
      if ((zd0<fw2) || (zd0 > oi_LIVE_VIDEO->im.ny-fw2) || (zf0<fw2) || (zf0 > oi_LIVE_VIDEO->im.ny-fw2))
	return win_printf("Beam height must be within %d and %d", fw2, oi_LIVE_VIDEO->im.ny-fw2);
    }
  else
    {
      if ((zd0<fw2) || (zd0 > oi_LIVE_VIDEO->im.nx-fw2) || (zf0<fw2) || (zf0 > oi_LIVE_VIDEO->im.nx-fw2))
	return win_printf("Beam height must be within %d and %d", fw2, oi_LIVE_VIDEO->im.nx-fw2);
    }

  // Store updated values
  real_time_info->xdc = (rot==0) ? (oi_LIVE_VIDEO->im.nx/2) : zd0;
  real_time_info->ydc = (rot==0) ? zd0 : (oi_LIVE_VIDEO->im.ny/2);
  real_time_info->xfc = (rot==0) ? (oi_LIVE_VIDEO->im.nx/2) : zf0;
  real_time_info->yfc = (rot==0) ? zf0 : (oi_LIVE_VIDEO->im.ny/2);
  
  real_time_info->xd0 = (rot==0) ? 0 : (real_time_info->xdc - real_time_info->fwidth/2);
  real_time_info->yd0 = (rot==0) ? (real_time_info->ydc - real_time_info->fwidth/2) : 0;
  real_time_info->xf0 = (rot==0) ? 0 : (real_time_info->xfc - real_time_info->fwidth/2);
  real_time_info->yf0 = (rot==0) ? (real_time_info->yfc - real_time_info->fwidth/2) : 0;
 
#ifdef UEYE
  set_config_int("CELLCOUNT_UEYE_CFG", "diff_h", zd0);
  set_config_int("CELLCOUNT_UEYE_CFG", "fluo_h", zf0);
#endif
#ifdef JAI_SDK
  set_config_int("CELLCOUNT_JAI_CFG", "diff_h", zd0);
  set_config_int("CELLCOUNT_JAI_CFG", "fluo_h", zf0);
#endif
  flush_config_file();

  // Reset histos
  real_time_info->reset_histo_bp = 1;
  real_time_info->reset_histo_dsl = 1;

  return D_O_K;
}

int do_start_time_lapse_record(void)
{
  O_p *op;
  d_s *ds;

  static int duration = 1, max_tacq = -1, stir = 0;
  static float max_prec = -1, conc_coef = 1;
  static int port_number = 4, sbaud = 0, hand = 0, parity = 0, stops = 1, n_bits = 8;
  static int mtstir = 0, mtdelay = 0;
  int itmp, red;
  float bl, wi;
  char command[128];
  
  if(updating_menu_state != 0)	return D_O_K;
  if (real_time_info == NULL)
    return win_printf("data structure not ready!");

  if (real_time_info->is_tl_on == 1)
    return win_printf("A time lapse record is already running");
  else
    {
      bl = real_time_info->bp_bl;
      wi = real_time_info->bp_wi;
      red = real_time_info->redundancy;

      itmp = win_scanf("Acquisition duration (including stirring and delay, in min) %5d\n\n"
		       "Maximum duration per point (in sec) %5d\n"
		       "Maximum precision per point (between 0 and 1) %5f\n"
		       "(enter negative value to disable)\n\n"
		       "Histo processing\n"
		       "Black level %5f  Black level width %5f\n"
		       "Concentration coefficient %5f\nParticles redundancy %5d\n\n"
		       "%b Remote stirring control",
		       &duration, &max_tacq, &max_prec, 
		       &bl, &wi, &conc_coef, &red, &stir);
      if (itmp == CANCEL) return 0;
      
      real_time_info->duration = duration;
      real_time_info->max_tacq = max_tacq;
      real_time_info->max_prec = max_prec;
      real_time_info->bp_bl = bl;
      real_time_info->bp_wi = wi;
      real_time_info->redundancy = red;
      real_time_info->stir = stir;

      // Start serial communication with stirrer motor
      if (stir)
	{
	  itmp = win_scanf("Stirring between each frame of the timelapse\n"
			   "Stirring duration (in sec) %5d\n"
			   "Delay after stirring (in sec) %5d\n\n"
			   "Serial port parameters\n"
			   "{\\color{yellow}Port number:} %5d\n"
			   "{\\color{yellow}Baudrate (default 115.2k):}"
			   "19.2k %R  38.4k %r  57.6k %r  115.2k %r  230.4k %r\n"
			   "{\\color{yellow}Number of bits:} %4d  (default 8)\n"
			   "{\\color{yellow}Parity:} No %R even %r odd %r (default No)\n"
			   "{\\color{yellow}Number of stop bits:} 0->%R 1->%r 2->%r (default 1)\n"
			   "{\\color{yellow}HandShaking:} None %R Cts/Rts %r (default None)\n"
			   "Dump in and out in a debug file  No %R yes %r\n",
			   &mtstir, &mtdelay, &port_number, &sbaud, &n_bits, &parity, &stops, &hand, &debug);
	  if (itmp == CANCEL) return 0;

	  real_time_info->max_tstir = mtstir;
	  real_time_info->max_tdelay = mtdelay;
	  real_time_info->stirActive = 0;
	  real_time_info->stirStop = 0;
	  real_time_info->delayActive = 0;
	  real_time_info->delayStop = 0;

	  if (hCom != NULL)
	    CloseSerialPort(hCom);
	  hCom = init_serial_port(port_number, (sbaud+1)*19200, n_bits, parity , stops, hand);
	  if (hCom == NULL) return win_printf("Error when opening serial communication with motor");

	  sprintf(command,"M=POTA\r"); // manual control of motor speed
	  Write_serial_port(hCom, command, strlen(command));
	  sprintf(command,"M=OFF\r"); // stop motor at first
	  Write_serial_port(hCom, command, strlen(command));
	}

      // Info about precision
      if (max_prec > 1)
	return win_printf("Precision (%2.2f) must be between 0 and 1: time lapse aborted", max_prec);
      if ((max_tacq < 0) && (max_prec < 0))
	return win_printf_OK("No time or precision limit: time lapse aborted");
      else if (max_prec < 0)
	win_printf_OK("Time limit: acquisition will not last more than %d s", max_tacq);
      else if (max_tacq < 0)
	win_printf_OK("Precision limit: acquisition will not exceed %4f%% precision", max_prec);
      else
	win_printf_OK("Time and precision limits: acquisition will not exceed %d s or %4f%% precision", max_tacq, max_prec);
      
      // Allocate plots
      if (max_tacq > 0)
	itmp = (duration*60/max_tacq) + 1;
      else itmp = 48;
      if ((op = create_and_attach_one_plot(pr_LIVE_VIDEO, itmp, itmp, 0)) == NULL)
	return win_printf("I can't create plot !");
      set_op_filename(op, "bp-timelapse.gr");
      ds = op->dat[0]; // the first data set was already created
      set_ds_source(ds, "Time lapse of lbd0 measurements (diffused light)");
      ds->nx = ds->ny = 0;
      alloc_data_set_x_error(ds);
      alloc_data_set_y_error(ds);

      if ((ds = create_and_attach_one_ds(op, itmp, itmp, 0)) == NULL)
	return win_printf("I can't create plot !");
      set_ds_source(ds, "Time lapse of lbdm measurements (diffused light)");
      ds->nx = ds->ny = 0;
      alloc_data_set_x_error(ds);
      alloc_data_set_y_error(ds);

      if ((ds = create_and_attach_one_ds(op, itmp, itmp, 0)) == NULL)
	return win_printf("I can't create plot !");
      set_ds_source(ds, "Time lapse of lbd0 measurements (fluorescence)");
      ds->nx = ds->ny = 0;
      alloc_data_set_x_error(ds);
      alloc_data_set_y_error(ds);

      if ((ds = create_and_attach_one_ds(op, itmp, itmp, 0)) == NULL)
	return win_printf("I can't create plot !");
      set_ds_source(ds, "Time lapse of lbdm measurements (fluorescence)");
      ds->nx = ds->ny = 0;
      alloc_data_set_x_error(ds);
      alloc_data_set_y_error(ds);

      set_plot_y_log(op);
      set_plot_x_auto_range(op);
      set_plot_y_auto_range(op);
      create_attach_select_x_un_to_op(op, IS_RAW_U, 0, (float) 1/60, 0, 0, "'");
      create_attach_select_y_un_to_op(op, IS_RAW_U, 0, conc_coef, 0, 0, "no_name");
      set_plot_x_title(op, "Time");
      set_plot_y_title(op, "Concentration");			

      op->op_idle_action = op_tl_idle_action;
      switch_project_to_this_pltreg(pr_LIVE_VIDEO);
      do_save_bin();
      refresh_image(imr_LIVE_VIDEO, 0);

      // initialize flags
      real_time_info->op_tl = op;
      real_time_info->t_ini = 0;
      real_time_info->tl_add_point = 0;
      real_time_info->is_tl_on = 1;
    }
  return D_O_K;
}

int stop_time_lapse_record(void)
{
  real_time_info->is_tl_on = 0;
  real_time_info->op_tl = NULL;
  real_time_info->tl_add_point = 0;

  return D_O_K;
}
int do_stop_time_lapse_record(void)
{
  if(updating_menu_state != 0)	return D_O_K;
  if (real_time_info == NULL)
    return win_printf("data structure not ready!");

  if (real_time_info->is_tl_on == 0)
    return win_printf("No time lapse running.");

  stop_time_lapse_record();
  return win_printf("Time lapse has been stopped.");
}


///////////////////////////////////
// this running in the timer thread

int histo_duration_since_last_event(O_i *oifb, long long *dur_since_last, int *histo_dsl, float bl, float wi, float bp_factor, long long dsl_factor, long long dsl_levels)
{
  int i, j, im, itmp;
  int xmin = 0, xmax = 0, ymin = 0, ymax = 0;
  float tmp;
  long long lltmp;

  xmin = oifb->im.nx / 8;
  xmax = 7 * oifb->im.nx / 8; 
  ymin = oifb->im.ny / 8;
  ymax = 7 * oifb->im.ny / 8;

  for (im = 0, tmp = 0, itmp = 0; im < oifb->im.n_f; im++)
    {
      // compute RMS of center of frame
      for (j = ymin; j < ymax; j++)
	{
	  for (i = xmin; i < xmax; i++)
	    {
	      tmp += oifb->im.pxl[im][j].fl[i] * oifb->im.pxl[im][j].fl[i];
	      itmp++;
	    }
	}
      tmp /= (float) itmp;
      tmp = sqrt(tmp) * bp_factor;

      if (tmp/bp_factor > bl+wi)
	{
	  if (dur_since_last[im] == 0)
	    {
	      dur_since_last[im] = my_ulclock();
	    }
	  else
	    {
	      lltmp = my_ulclock() - dur_since_last[im];
	      lltmp *= dsl_factor;
	      lltmp /= get_my_ulclocks_per_sec();
	      dur_since_last[im] = my_ulclock();
	      lltmp = (lltmp < dsl_levels) ? lltmp : dsl_levels-1;
	      histo_dsl[(int)lltmp]++;
	    }  
	}
    }
  return 0;
}


int do_at_all_image(imreg *imr, O_i *oi, int n,  DIALOG *d, long long t, unsigned long dt, void *p, int n_inarow) 
{
  register int j;
  int ci, i, im, ci_1, ac_i, im_i, mc_i;
  BITMAP *imb = NULL;
  g_real_time *gt = NULL;

  O_i *oim, *oif, *oifb, *oid;
  static O_i *apo=NULL, *desapo=NULL;

  int itmp = 0, xtmp, ytmp, xb, yb, wb, hb, size;
  int xmin = 0, xmax = 0, ymin = 0, ymax = 0;
  static unsigned long t_c, to = 0;
  float tmp = 0;
  double lambda = 0, nc = 0, nc0 = 0;
   
  if (apo == NULL || desapo == NULL)
    {
      apo = create_appodisation_float_image(real_time_info->fsize,
					    real_time_info->fsize, 0.05, 0);
      if (apo == NULL)	return win_printf_OK("Could not create apodisation image!");
      desapo = create_appodisation_float_image(real_time_info->fsize,
					       real_time_info->fsize, 0.05, 1);
      if (desapo == NULL)	return win_printf_OK("Could not create apodisation image!");
    }

  gt = (g_real_time*)p;
  ac_i = real_time_info->ac_i;
  ac_i++;
  real_time_info->rc_i++;
  ci_1 = ci = real_time_info->c_i;
  ci++;
  ci = (ci < real_time_info->n_i) ? ci : 0;
  real_time_info->imi[ci] = n;                                 // we save image" number
  real_time_info->imit[ci] = n_inarow;
  real_time_info->imt[ci] = t;                                 // image time
  im_i = oi->im.c_f;
  if (real_time_info->ac_i == 0)  real_time_info->imt0 = real_time_info->imt[ci];
  //real_time_info->delta[ci] = ac_i * real_time_info->per_t/4;

  // Filtered mean img counter
  mc_i = real_time_info->mc_i;
  mc_i++;
  if (mc_i >= real_time_info->nf_autocorr) mc_i = 0;

  if (oi->bmp.stuff != NULL) imb = (BITMAP*)oi->bmp.stuff;
  // we grab the video bitmap of the IFC image
  prepare_image_overlay(oi);

  // Add roi display (and remove other one)
  remove_all_screen_label_from_movie(oi);
  add_screen_label(oi, 0, 0, 0, NULL, draw_rtd_roi);
  add_screen_label(oi, 0, 0, 0, NULL, draw_rtf_roi);


  //// Raw histo ////  xx to be transformed depending on rot...
  // substract first img histo in raw_histo (curr index)
  for (i = 0; i < 256; i++)
    {
      real_time_info->raw_histo[i] -= real_time_info->raw_single_histo[oi->im.c_f][i];
      real_time_info->raw_single_histo[oi->im.c_f][i] = 0;
    }
  // calculate curr histo
  for (i = 0; i < oi->im.nx; i++)
    {
      for (j = real_time_info->raw_min; j < real_time_info->raw_min+real_time_info->raw_wi; j++)
	{
	  itmp = (int) oi->im.pixel[j].ch[i];
	  real_time_info->raw_single_histo[oi->im.c_f][0xff&itmp] += 1;
	}
    }
  // add curr img in raw_histo
  for (i = 0; i < 256; i++)
    real_time_info->raw_histo[i] += real_time_info->raw_single_histo[oi->im.c_f][i];

  if (real_time_info->raw_histo_update)
    {
      for (i = 0; i < 256; i++) 
	real_time_info->raw_histotmp[i] = real_time_info->raw_histo[i];
      real_time_info->raw_histo_update = 0;
    }


  //// Fourier treatment ////
  //if (real_time_info->ac_i > real_time_info->delay)
  {
    oim = real_time_info->oim;
    oif = real_time_info->oif;
    oifb = real_time_info->oifb;
    oid = real_time_info->oid;
    size = real_time_info->fsize;

    // Diffused light band-pass
    t_c = my_uclock();
    split_before_bp(oi, oim, real_time_info->oibg, real_time_info->f_nf, size, real_time_info->xd0, real_time_info->yd0, real_time_info->wx, real_time_info->wy);
      
    forward_fftbt_2d_im_appo(oif, oim, 0, screen, apo, 0);
    //my_set_window_title("Treating image %d after fft",ims);
    band_pass_filter_fft_2d_im(oif, oif,  0, real_time_info->lp,  real_time_info->lw,  real_time_info->hp,  real_time_info->hw, 0, 0);
    //my_set_window_title("Treating image %d after band pass ",ims);
    backward_fftbt_2d_im_desappo(oifb, oif, 0, screen, NULL, 0);
    //my_set_window_title("Treating image %d after back",ims);

    reconstruct_after_bp(oi, oifb, oid, real_time_info->f_nf, size, real_time_info->wx, real_time_info->wy, 0, 0);

    t_c = my_uclock() - t_c;
    if (to == 0) to = MY_UCLOCKS_PER_SEC;
    //my_set_window_title("FFT of diffused light beam took %g ms",(double)(t_c*1000)/to);

    // Diffused light band-passed image histo
    produce_histo_from_bp(oifb, real_time_info->f_nf, real_time_info->histo_bpd, BP_LEVELS, (float) BP_FACTOR);

    // Store averaged filtered image for autocorrelation treatment
    for (im = 0; im < real_time_info->f_nf; im++)
      {
	switch_frame(oifb,im);
	find_min_subset_good_pixel_pos_ROI(oi, real_time_info->fsize, real_time_info->fsize, im, &xb, &yb, &wb, &hb, real_time_info->wx, real_time_info->wy);
	  
	// compute RMS of center of frame
	xmin = oifb->im.nx / 8;
	xmax = 7 * oifb->im.nx / 8;
	ymin = oifb->im.ny / 8;
	ymax = 7 * oifb->im.ny / 8;

	for (j = ymin, tmp = 0, itmp = 0; j < ymax; j++)
	  {
	    for (i = xmin; i < xmax; i++)
	      {
		tmp += oifb->im.pxl[im][j].fl[i] * oifb->im.pxl[im][j].fl[i];
		itmp++;
	      }
	  }
	tmp /= (float) itmp;
	tmp = sqrt(tmp);

	xtmp = (int) (1.34 * xb / real_time_info->fsize);  // xxx is it fsize
	ytmp = (int) (1.34 * yb / real_time_info->fsize);  // xxx
	real_time_info->oidm->im.pxl[mc_i][ytmp].fl[xtmp] = tmp;
      }

    // Fluorescence band-pass
    t_c = my_uclock();
    split_before_bp(oi, oim, real_time_info->oibg, real_time_info->f_nf, size, real_time_info->xf0, real_time_info->yf0, real_time_info->wx, real_time_info->wy);
      
    forward_fftbt_2d_im_appo(oif, oim, 0, screen, apo, 0);
    //my_set_window_title("Treating image %d after fft",ims);
    band_pass_filter_fft_2d_im(oif, oif,  0, real_time_info->lp,  real_time_info->lw,  real_time_info->hp,  real_time_info->hw, 0, 0);
    //my_set_window_title("Treating image %d after band pass ",ims);
    backward_fftbt_2d_im_desappo(oifb, oif, 0, screen, NULL, 0);
    //my_set_window_title("Treating image %d after back",ims);

    reconstruct_after_bp(oi, oifb, oid, real_time_info->f_nf, size, real_time_info->wx, real_time_info->wy, 0, real_time_info->wy); // put the fluo image above

    t_c = my_uclock() - t_c;
    if (to == 0) to = MY_UCLOCKS_PER_SEC;
    //my_set_window_title("FFT of fluorescence beam took %g ms",(double)(t_c*1000)/to);

    // Fluorescence band-passed image histo
    produce_histo_from_bp(oifb, real_time_info->f_nf, real_time_info->histo_bpf, BP_LEVELS, (float) BP_FACTOR);
 

    // Reset bp histos
    if (real_time_info->reset_histo_bp)
      {
	real_time_info->reset_histo_bp = 0;
	real_time_info->rc_i = 0;
	for (i = 0; i < BP_LEVELS; i++)
	  {
	    real_time_info->histo_bpd[i] = 0;
	    real_time_info->histo_bpf[i] = 0;
	  }
      }


    /*       // 2D histo of events vs y */
    /*       for (i = 0; i < oid->im.nx; i++) */
    /* 	{ */
    /* 	  for (j = 0; j < oid->im.ny; j++) */
    /* 	    { */
    /* 	      tmp = fabs(oid->im.pxl[im_i][j].fl[i]); // RMS is absolute value for one pixel */
    /* 	      tmp *= (float) BP_FACTOR; */
    /* 	      itmp = (tmp < (float)BP_LEVELS) ? (int) tmp : (int) BP_LEVELS-1; */
    /* 	      oihbp->im.pixel[j].li[itmp] += 1; */
    /* 	    } */
    /* 	} */
    /*       oihbp->need_to_refresh = BITMAP_NEED_REFRESH; */


    // Duration since last event histo
    if (real_time_info->reset_histo_dsl)
      {
	real_time_info->reset_histo_dsl = 0;
	for (i = 0; i < BP_DSL_LEVELS; i++)
	  real_time_info->histo_dsl[i] = 0;
      }
    histo_duration_since_last_event(oifb, real_time_info->dur_since_last, real_time_info->histo_dsl, real_time_info->bp_bl, real_time_info->bp_wi, (float) BP_FACTOR, (long long) BP_DSL_FACTOR, (long long) BP_DSL_LEVELS);
  }


  //// Manage time lapse ////
  if (real_time_info->is_tl_on == 1)
    {
      // time lapse init
      if (real_time_info->t_ini == 0)
	{
	  real_time_info->t_ini = real_time_info->tacq_start = my_ulclock();
	  real_time_info->reset_histo_bp = 1;
	}

      // Update stirring state
      if (real_time_info->stir & real_time_info->stirActive)
	{
	  tmp = (float)(my_ulclock() - real_time_info->tstir_start);
	  tmp /= (float) get_my_ulclocks_per_sec();
	  if (tmp > (float)real_time_info->max_tstir)
	    {
	      real_time_info->stirStop = 1;
	      real_time_info->stirActive = 0;
	    }
	}
      // Update stirring state
      else if (real_time_info->stir & real_time_info->delayActive)
	{
	  tmp = (float)(my_ulclock() - real_time_info->tdelay_start);
	  tmp /= (float) get_my_ulclocks_per_sec();
	  if (tmp > (float)real_time_info->max_tdelay)
	    {
	      real_time_info->delayStop = 1;
	      real_time_info->delayActive = 0;
	    }
	}
      // Update time lapse
      else
	{
	  itmp = 0;
	  tmp = (float)(my_ulclock() - real_time_info->tacq_start);
	  tmp /= (float) get_my_ulclocks_per_sec();
	  if (real_time_info->max_tacq > 0 && tmp > (float)real_time_info->max_tacq)
	    itmp++;

	  raw_histo_lambda_zero(real_time_info->histo_bpd_tmp, BP_LEVELS, (float) BP_FACTOR, real_time_info->bp_bl, real_time_info->bp_wi, &lambda, &nc, &nc0);
	  //tmp = compute_lbd_generic_error(lambda, nc, real_time_info->redundancy) / (float) lambda;
	  tmp = compute_lbd_generic_error(lambda, nc, real_time_info->redundancy) / (float) lambda;
	  if (real_time_info->max_prec >= 0 && tmp < real_time_info->max_prec)
	    itmp ++;

	  if (itmp > 0)
	    {
	      real_time_info->tacq_end = my_ulclock();
	      real_time_info->tl_add_point = 1;
	    }
	}    
    }

  /// Update tmp histo used in non-RT thread  ///
  // this must be after time lapse management
  if (real_time_info->histo_bpd_update || real_time_info->tl_add_point)
    {
      for (i = 0; i < BP_LEVELS; i++)
	real_time_info->histo_bpd_tmp[i] = real_time_info->histo_bpd[i];
      real_time_info->histo_bpd_update = 0;
    }      
  if (real_time_info->histo_bpf_update || real_time_info->tl_add_point)
    {
      for (i = 0; i < BP_LEVELS; i++)
	real_time_info->histo_bpf_tmp[i] = real_time_info->histo_bpf[i];
      real_time_info->histo_bpf_update = 0;
    }      
  
  if (real_time_info->histo_dsl_update)
    {
      for (i = 0; i < BP_DSL_LEVELS; i++)
	real_time_info->histo_dsl_tmp[i] = real_time_info->histo_dsl[i];
      real_time_info->histo_dsl_update = 0;
    }

  /////////////////////////////////////
  switch_frame(oid, im_i);
  oim->need_to_refresh = ALL_NEED_REFRESH;
  oif->need_to_refresh = ALL_NEED_REFRESH;
  oid->need_to_refresh = ALL_NEED_REFRESH;
  switch_frame(real_time_info->oidm,mc_i);
  real_time_info->oidm->need_to_refresh = ALL_NEED_REFRESH;

  real_time_info->imdt[ci] = my_uclock() - dt;
  real_time_info->c_i = ci;
  real_time_info->ac_i = ac_i;
  real_time_info->mc_i = mc_i;

  i = find_remaining_action(n);
  real_time_info->status_flag[ci] = real_time_info->status_flag[ci_1];  
  real_time_info->obj_pos_cmd[ci] = real_time_info->obj_pos_cmd[ci_1];
  while ((i = find_next_action(n)) >= 0)
    {
      action_pending[i].proceeded = 1;
    }

   
  return 0; 
}
END_OF_FUNCTION(do_at_all_image)



int get_present_image_nb(void)
{
 return (real_time_info != NULL) ?  real_time_info->imi[real_time_info->c_i] : -1;
}

O_i *load_background_oi_from_file(void)
{
  char path[512], file[256];
  static char fullfile[512], *fu = NULL, name[128];
  O_i *oi = NULL;

  snprintf (name,128,"last_background_img");
  fu = (char*)get_config_string("IMAGE-GR-FILE",name,NULL);
  if (fu != NULL)	
    {
      strcpy(fullfile,fu);
      fu = fullfile;
    }
  else
    {
      my_getcwd(fullfile, 510);
      strncat(fullfile,"\\",2);
    }

  extract_file_path(path, 512, fullfile);
  put_backslash(path);
  if (extract_file_name(file, 256, fullfile) == NULL)
    fullfile[0] = file[0] = 0;
  else strcpy(fullfile,file);
  fu = DoFileOpen("Load background image (*.gr)", path, fullfile, 512, "Image Files (*.gr)\0*.gr\0All Files (*.*)\0*.*\0\0", "gr");

  if (fu == NULL) 	
    return NULL;
  else
    {
      fu = fullfile;
      snprintf (name,128,"last_background_img");
      set_config_string("IMAGE-GR-FILE",name,fu);
      write_Pico_config_file();
      extract_file_name(file, 256, fullfile);
      extract_file_path(path, 512, fullfile);
      strcat(path,"\\");
      if ((oi = load_im_file_in_oi(file, path)) == NULL)
	{   
	  win_printf("could not load background image\n%s\n"
		     "from path %s",file, backslash_to_slash(path));
	  return NULL;
	}
      else
	if ((oi->im.nx == oi_LIVE_VIDEO->im.nx)
	    && (oi->im.ny == oi_LIVE_VIDEO->im.ny)
	    && (oi->im.n_f <= 1))
	  {
	    add_image(imr_LIVE_VIDEO, oi->im.data_type, (void *)oi);
	  }
	else
	  {
	    free_one_image(oi);
	    win_printf("Image has not the same size as the camera or is\n"
		       "a movie. Background processing is cancelled.");
	    return NULL;
	  }
    }
  return oi;
}

g_real_time *creating_real_time_info(int rot, int do_rm_bg, int zd0, int zf0, int fsize, int fwidth, float lp, float lw, float hp, float hw, int nf_autocorr, int is_dv, float bp_bl, float bp_wi)
{
  O_p *op;
  d_s *ds;
  O_i *oi;
  int i, nf, nfi;
  //int nx, ny;
  int xd0, yd0, xf0, yf0, wx, wy;
  int cx, cy, nx0, ny0;

  int do_save(void);
  
  if (real_time_info == NULL)
    {
      nfi = oi_LIVE_VIDEO->im.n_f;

      real_time_info = (g_real_time*)calloc(1,sizeof(g_real_time));
      if (real_time_info == NULL) return win_printf_ptr("cannot alloc real_time info!");	

      real_time_info->raw_single_histo = (int**)calloc(nfi, sizeof(int*));
      if (real_time_info->raw_single_histo == NULL) return win_printf_ptr("cannot alloc raw_single_histo table info!");
      for (i = 0; i < oi_LIVE_VIDEO->im.n_f; i++)
	{
	  real_time_info->raw_single_histo[i] = (int*) calloc(256, sizeof(int));
	  if (real_time_info->raw_single_histo[i] == NULL) return win_printf_ptr("cannot alloc raw_single_histo info!");
	}

      real_time_info->im_focus_check = 16;
      real_time_info->user_real_time_action = NULL;
      real_time_info->rc_i = 0;

      real_time_info->rot = rot;
      real_time_info->xdc = (rot==0) ? (oi_LIVE_VIDEO->im.nx/2) : zd0;
      real_time_info->ydc = (rot==0) ? zd0 : (oi_LIVE_VIDEO->im.ny/2);
      real_time_info->xfc = (rot==0) ? (oi_LIVE_VIDEO->im.nx/2) : zf0;
      real_time_info->yfc = (rot==0) ? zf0 : (oi_LIVE_VIDEO->im.ny/2);
      
      xd0 = real_time_info->xd0 = (rot==0) ? 0 : (real_time_info->xdc - fwidth/2);
      yd0 = real_time_info->yd0 = (rot==0) ? (real_time_info->ydc - fwidth/2) : 0;
      xf0 = real_time_info->xf0 = (rot==0) ? 0 : (real_time_info->xfc - fwidth/2);
      yf0 = real_time_info->yf0 = (rot==0) ? (real_time_info->yfc - fwidth/2) : 0;
      wx = real_time_info->wx = (rot==0) ? oi_LIVE_VIDEO->im.nx : fwidth;
      wy = real_time_info->wy = (rot==0) ? fwidth : oi_LIVE_VIDEO->im.ny;
      real_time_info->fsize = fsize;
      real_time_info->fwidth = fwidth;
      real_time_info->lp = lp;
      real_time_info->lw = lw;
      real_time_info->hp = hp;
      real_time_info->hw = hw;

      real_time_info->raw_min = 300;
      real_time_info->raw_wi = 100;
      real_time_info->nf_autocorr = nf_autocorr;

      real_time_info->do_rm_bg = do_rm_bg;
      real_time_info->bp_bl = bp_bl;
      real_time_info->bp_wi = bp_wi;
      real_time_info->is_dv = is_dv;
      real_time_info->is_tl_on = 0;
      real_time_info->tl_add_point = 0;
      real_time_info->duration = 0;
      real_time_info->max_tacq = 0;
      real_time_info->max_prec = 0;
      real_time_info->redundancy = 1;
      real_time_info->t_ini = 0;


     // Allocate images
      smooth_interpol = 0;  // disable image interpolation

      real_time_info->f_nf = nf = compute_nb_of_min_subset_ROI(oi_LIVE_VIDEO, fsize, fsize, xd0, yd0, wx, wy);
      // display nb of small images
      cx = 4*(wx - fsize);
      nx0 = cx/(3*fsize);
      nx0 += (cx%(3*fsize)) ? 2 : 1;
      cy = 4*(wy - fsize);
      ny0 = cy/(3*fsize);
      ny0 += (cy%(3*fsize)) ? 2 : 1;
      real_time_info->bp_nx = nx0;
      real_time_info->bp_ny = ny0;
      win_printf_OK("Nb of small images %d (%d x %d)", nf, nx0, ny0);

      // Alloc table late because several params are needed
      real_time_info->dur_since_last = (long long*) calloc(nf, sizeof(long long));
      if (real_time_info->dur_since_last == NULL)
	return win_printf_ptr("cannot alloc dur_since_last info!");
      for (i = 0; i < nf; i++)
	real_time_info->dur_since_last[i] = 0;


      switch (do_rm_bg)
	{
	case 2:
	  if ((oi = load_background_oi_from_file()) == NULL)
	    real_time_info->oibg = NULL;
	  else
	    real_time_info->oibg = oi;
	  break;

	case 1:
	  win_printf("The mean background processing is not implemented\n"
		     "yet. Background processing is cancelled");
	  if ((oi = create_and_attach_oi_to_imr(imr_LIVE_VIDEO, oi_LIVE_VIDEO->im.nx, oi_LIVE_VIDEO->im.ny, IS_FLOAT_IMAGE)) == NULL)
	    return win_printf_ptr("I can't create image !");
	  set_oi_filename(oi, "bg-img.gr");
	  real_time_info->oibg = oi;
	  break;

	default:
	  real_time_info->oibg = NULL;
	}
      if (real_time_info->oibg != NULL)
	{
	  set_im_title(real_time_info->oibg, "Background image");
	  set_z_black_z_white_values(real_time_info->oibg, 0, 25);
	  set_zmin_zmax_values(real_time_info->oibg, 0, 25);
	  //map_pixel_ratio_of_image_and_screen(real_time_info->oibg, 1, 1);
	  real_time_info->oibg->oi_idle_action = oi_std_idle_action;
	}
      
      
      if ((oi = create_and_attach_movie_to_imr(imr_LIVE_VIDEO, fsize, fsize, IS_FLOAT_IMAGE, nf)) == NULL)
      	return win_printf_ptr("I can't create image !");
      set_z_black_z_white_values(oi, -1, 255);
      set_zmin_zmax_values(oi, 0, 255);
      oi->oi_idle_action = oi_std_idle_action;
      set_oi_filename(oi, "subset-img.gr");
      set_im_title(oi, "Subset images");
      real_time_info->oim = oi;

      if ((oi = create_and_attach_movie_to_imr(imr_LIVE_VIDEO, fsize, fsize/2, IS_COMPLEX_IMAGE, nf)) == NULL)
      	return win_printf_ptr("I can't create image !");
      oi->height *= 0.5;
      set_zmin_zmax_values(oi, -10, 0);
      oi->oi_idle_action = oi_std_idle_action;
      set_oi_filename(oi, "fft-img.gr");
      real_time_info->oif = oi;

      if ((oi = create_and_attach_movie_to_imr(imr_LIVE_VIDEO, fsize, fsize, IS_FLOAT_IMAGE, nf)) == NULL)
      	return win_printf_ptr("I can't create image !");
      oi->oi_idle_action = oi_std_idle_action;
      set_z_black_z_white_values(oi, -10, 40);
      set_zmin_zmax_values(oi, -10, 40);
      set_oi_filename(oi, "fftb-img.gr");
      real_time_info->oifb = oi;

/*       if ((oi = create_and_attach_movie_to_imr(imr_LIVE_VIDEO, fsize, fsize, IS_FLOAT_IMAGE, (int) (BP_LEVELS/BP_FACTOR))) == NULL) */
/*       	return win_printf_ptr("I can't create image !"); */
/*       oi->oi_idle_action = oi_std_idle_action; */
/*       set_z_black_z_white_values(oi, -10, 40); */
/*       set_zmin_zmax_values(oi, -10, 40); */
/*       real_time_info->oic = oi; */

      // the fluo filtered image is stored on the top of the diffused light image (hence 2*wy)
      if ((oi = create_and_attach_movie_to_imr(imr_LIVE_VIDEO, wx, 2*wy, IS_FLOAT_IMAGE, nfi)) == NULL)
      	return win_printf_ptr("I can't create image !");
      oi->oi_idle_action = oi_std_idle_action;
      map_pixel_ratio_of_image_and_screen(oi, 1, 1);
      set_z_black_z_white_values(oi, -10, 40);
      set_zmin_zmax_values(oi, -10, 40);
      set_oi_filename(oi, "bp-img.gr");
      real_time_info->oid = oi;
      
      if ((oi = create_and_attach_movie_to_imr(imr_LIVE_VIDEO, nx0, ny0, IS_FLOAT_IMAGE, nf_autocorr)) == NULL)
      	return win_printf_ptr("I can't create image !");
      oi->oi_idle_action = oi_std_idle_action;
      map_pixel_ratio_of_image_and_screen(oi, (float)1/fsize, (float)1/fsize);
      set_z_black_z_white_values(oi, -10, 40);
      set_zmin_zmax_values(oi, -10, 40);
      set_oi_filename(oi, "bpm-img.gr");
      real_time_info->oidm = oi;
      
/*       if ((oi = create_and_attach_oi_to_imr(imr_LIVE_VIDEO, (int)BP_LEVELS, wy, IS_LINT_IMAGE)) == NULL) */
/*       	return win_printf_ptr("I can't create image !"); */
/*       oi->oi_idle_action = oi_hbp_idle_action; */
/*       map_pixel_ratio_of_image_and_screen(oi, 1, 1); */
/*       //set_z_black_z_white_values(oi, -10, 40); */
/*       set_zmin_zmax_values(oi, 0, 1000); */
/*       real_time_info->oihbp = oi; */




      // Allocate plots to check real_time
      op = pr_LIVE_VIDEO->o_p[0];
      set_op_filename(op, "Timing.gr");
      ds = op->dat[0]; // the first data set was already created
      set_ds_source(ds, "Timing rolling buffer");
      if ((ds = create_and_attach_one_ds(op, REAL_TIME_BUFFER_SIZE, REAL_TIME_BUFFER_SIZE, 0)) == NULL)
		return win_printf_ptr("I can't create plot !");
      set_ds_source(ds, "Period rolling buffer");
      if ((ds = create_and_attach_one_ds(op, REAL_TIME_BUFFER_SIZE, REAL_TIME_BUFFER_SIZE, 0)) == NULL)
		return win_printf_ptr("I can't create plot !");
      set_ds_source(ds, "Timer rolling buffer");
      op->op_idle_action = timing_rolling_buffer_idle_action;
      //      op->op_post_display = general_op_post_display;
      set_plot_x_title(op, "Time");
      set_plot_y_title(op, "{\\color{yellow} Real_Timing duration} {\\color{lightgreen} Frame duration} (ms)");



      if ((op = create_and_attach_one_plot(pr_LIVE_VIDEO, 1024, 1024, 0)) == NULL)
	return win_printf_ptr("I can't create plot !");
      set_op_filename(op, "raw-histo.gr");
      ds = op->dat[0]; // the first data set was already created
      set_ds_source(ds, "Raw bar histogram");

      if ((ds = create_and_attach_one_ds(op, 256, 256, 0)) == NULL)
		return win_printf_ptr("I can't create plot !");
      set_ds_source(ds, "Raw histogram");

      op->op_idle_action = raw_histo_rolling_buffer_idle_action;

      set_plot_y_log(op);
      set_plot_x_title(op, "Signal level");
      set_plot_y_title(op, "Counts");			



     if ((op = create_and_attach_one_plot(pr_LIVE_VIDEO, nf_autocorr, nf_autocorr, 0)) == NULL)
	return win_printf_ptr("I can't create plot !");
      set_op_filename(op, "autocorr.gr");
      ds = op->dat[0]; // the first data set was already created
      set_ds_source(ds, "Band passed small image correlation");
      op->op_idle_action = bp_pxl_correl_rolling_buffer_idle_action;

      set_plot_x_title(op, "Time");
      set_plot_y_title(op, "Autocorrelation");			

      create_attach_select_x_un_to_op(op, IS_SECOND, 0
				       ,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");



      if ((op = create_and_attach_one_plot(pr_LIVE_VIDEO, BP_DSL_LEVELS*4, BP_DSL_LEVELS*4, 0)) == NULL)
	return win_printf_ptr("I can't create plot !");
      set_op_filename(op, "bp-duration-histo.gr");
      ds = op->dat[0]; // the first data set was already created
      set_ds_source(ds, "Bar Histogram of duration since last event");

      if ((ds = create_and_attach_one_ds(op, BP_DSL_LEVELS, BP_DSL_LEVELS, 0)) == NULL)
		return win_printf_ptr("I can't create plot !");
      set_ds_source(ds, "Histogram of duration since last event");

      op->op_idle_action = dsl_histo_rolling_buffer_idle_action;

      set_plot_y_log(op);
      set_plot_x_title(op, "Duration");
      set_plot_y_title(op, "Counts");			



     if ((op = create_and_attach_one_plot(pr_LIVE_VIDEO, BP_LEVELS*4, BP_LEVELS*4, 0)) == NULL)
	return win_printf_ptr("I can't create plot !");
      set_op_filename(op, "bp-histo.gr");
      ds = op->dat[0]; // the first data set was already created
      set_ds_source(ds, "Bar Histogram all band-passed pixels (diffused light)");

      if ((ds = create_and_attach_one_ds(op, BP_LEVELS, BP_LEVELS, 0)) == NULL)
	return win_printf_ptr("I can't create plot !");
      set_ds_source(ds, "Histogram all band-passed pixels (diffused light)");

      if ((ds = create_and_attach_one_ds(op, BP_LEVELS*4, BP_LEVELS*4, 0)) == NULL)
	return win_printf_ptr("I can't create plot !");
      set_ds_source(ds, "Bar Histogram all band-passed pixels (fluorescence)");

      if ((ds = create_and_attach_one_ds(op, BP_LEVELS, BP_LEVELS, 0)) == NULL)
	return win_printf_ptr("I can't create plot !");
      set_ds_source(ds, "Histogram all band-passed pixels (fluorescence)");

      op->op_idle_action = bp_histo_rolling_buffer_idle_action;

      set_plot_y_log(op);
      set_plot_x_title(op, "Signal level");
      set_plot_y_title(op, "Counts");			

      real_time_info->op_bp_histo = op;     

      // start general idle action used in time lapse recording
      general_idle_action = gl_tl_idle_action;

      // Save config params in file
      flush_config_file();

      real_time_info->m_i = real_time_info->n_i = REAL_TIME_BUFFER_SIZE;
      real_time_info->c_i = real_time_info->ac_i = 0;
      real_time_info->mc_i = 0;
      LOCK_FUNCTION(do_at_all_image);
      LOCK_VARIABLE(real_time_info);

      bid.param = (void*)real_time_info;
      bid.to_do = NULL;
      bid.timer_do = do_at_all_image;
    }
  return real_time_info;
}


int init_live_video_info(void)
{
  int i, rot=0, zd0=300, zf0=160, fsize=16, fwidth = 40;
  int nf_autocorr = 1024, is_dv = 1, do_rm_bg = 2;
  float lp = 3, lw = 1, hp = 6, hw = 2, bl = 0, wi = 0.95;

  if (real_time_info == NULL) 
    {
#ifdef UEYE
      do_set_camera_exposure();
#endif
      set_zmin_zmax_values(oi_LIVE_VIDEO, 0, 25);
      oi_LIVE_VIDEO->need_to_refresh = ALL_NEED_REFRESH;

#ifdef UEYE
      zd0 = get_config_int("CELLCOUNT_UEYE_CFG", "diff_h", zd0);
      zf0 = get_config_int("CELLCOUNT_UEYE_CFG", "fluo_h", zf0);
      fsize  = get_config_int("CELLCOUNT_UEYE_CFG", "fourier_bin", fsize);
      fwidth = get_config_int("CELLCOUNT_UEYE_CFG", "fourier_width", fwidth);
      wi = get_config_float("CELLCOUNT_UEYE_CFG", "bl_width", wi);
#endif
#ifdef JAI_SDK
      zd0 = get_config_int("CELLCOUNT_JAI_CFG", "diff_h", zd0);
      zf0 = get_config_int("CELLCOUNT_JAI_CFG", "fluo_h", zf0);
      fsize  = get_config_int("CELLCOUNT_JAI_CFG", "fourier_bin", fsize);
      fwidth = get_config_int("CELLCOUNT_JAI_CFG", "fourier_width", fwidth);
      wi = get_config_float("CELLCOUNT_JAI_CFG", "bl_width", wi);
#endif

      i = win_scanf("Setup:\n%R Single beam   %r Dual view\nOrientation  %R h   %r v\n\n"
		    "For real time treatment\n"
		    "Background filtering:  %R no  %r mean  %r image\n"
		    "Fourier movie: bin size %8d   total width %8d\n"
		    "Averaged Fourier movie: %8dframes\n\n"
		    
		    "Diffused light heigth %8d\nFluorescence heigth %8d\n\n"
		    "Histogram threshold\nBlack level %8f  Width %8f\n\n"
		    "Band-pass filter\n"
		    "Cutoff lowpass %5f       Lowpass width %5f\n"
		    "Cutoff highpass %5f       Highpass width %5f"
		    ,&is_dv,&rot,&do_rm_bg,&fsize,&fwidth, &nf_autocorr,&zd0,&zf0,&bl,&wi, &lp,&lw,&hp,&hw);
      //&min_pt,&inc_pt,&asize,&nf,
      if (i == CANCEL) return 0;

      if (fsize > fwidth) return win_printf_OK("Fourier treatment width (bin * width) must be larger than beam width");

#ifdef UEYE
      set_config_int("CELLCOUNT_UEYE_CFG", "diff_h", zd0);
      set_config_int("CELLCOUNT_UEYE_CFG", "fluo_h", zf0);
      set_config_int("CELLCOUNT_UEYE_CFG", "fourier_bin", fsize);
      set_config_int("CELLCOUNT_UEYE_CFG", "fourier_width", fwidth);
      set_config_float("CELLCOUNT_UEYE_CFG", "bl_width", wi);
#endif
#ifdef JAI
      set_config_int("CELLCOUNT_JAI_CFG", "diff_h", zd0);
      set_config_int("CELLCOUNT_JAI_CFG", "fluo_h", zf0);
      set_config_int("CELLCOUNT_JAI_CFG", "fourier_bin", fsize);
      set_config_int("CELLCOUNT_JAI_CFG", "fourier_width", fwidth);
      set_config_float("CELLCOUNT_JAI_CFG", "bl_width", wi);
#endif

      real_time_info = creating_real_time_info(rot,do_rm_bg,zd0,zf0,fsize,fwidth,lp,lw,hp,hw,nf_autocorr,is_dv, bl, wi);
      
      m_action = 256;
      n_action = 0;
      action_pending = (f_action*)calloc(m_action,sizeof(f_action));
      if (action_pending == NULL)
	     win_printf_OK("Could not allocate action_pending!");

      win_printf_OK("Dbg 1");

    }
  return 0;
}


MENU *real_time_avg_img_menu(void)
{
  static MENU mn[32];
  extern int save_finite_movie(void);
  
  
  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"Display information", display_info,NULL,0,NULL);
  add_item_to_menu(mn,"Set beams height", set_beam_height,NULL,0,NULL);
  add_item_to_menu(mn,"Reset histo", zero_hist,NULL,0,NULL);
  add_item_to_menu(mn,"Start time-lapse recording", do_start_time_lapse_record,NULL,0,NULL);
  add_item_to_menu(mn,"\0",NULL,NULL,0,NULL);
  add_item_to_menu(mn,"Movie avg over time", do_movie_avg,NULL,0,NULL);


  return mn;
}

MENU *real_time_avg_plot_menu(void)
{
  static MENU mn[32];
  extern int save_finite_movie(void);
  
  
  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"Display information", display_info,NULL,0,NULL);
  add_item_to_menu(mn,"Set beams height", set_beam_height,NULL,0,NULL);
  add_item_to_menu(mn,"Reset histo", zero_hist,NULL,0,NULL);
  add_item_to_menu(mn,"Start time-lapse recording", do_start_time_lapse_record,NULL,0,NULL);
  add_item_to_menu(mn,"Stop time-lapse recording", do_stop_time_lapse_record,NULL,0,NULL);


  return mn;
}

#endif
