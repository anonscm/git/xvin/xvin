#ifndef _UEYE_SOURCE_H_
#define _UEYE_SOURCE_H_


#define BIT_PER_PIX_8  0x01080001
#define BIT_PER_PIX_10  0x01100003
#define ACQUIRE_INFINITY 0xFFFFFFFFFFFFFFFF

#define D_BAD 1
#include "xvin.h"
#include "allegro.h"
#ifdef XV_WIN32
#include "winalleg.h"
#endif


# include "../fft2d/fftbtl32n.h"
# include "fillibbt.h"
# include "trackBead.h"
# include "track_util.h"

/* If you include other plug-ins header do it here*/ 
/* But not below this define */

# define BUILDING_PLUGINS_DLL
#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# if defined(XV_WIN32) && !defined(WAFBUILD)
# ifdef SDK_3_5_0
# undef IS_CHAR
# include "uEye/SDK_3_5_0/uEye.h"
# include "uEye/SDK_3_5_0/uEye_tools.h"
# endif
# ifdef SDK_3_8_0
# undef IS_CHAR
# include "uEye/SDK_3_8_0/uEye.h"
# include "uEye/SDK_3_8_0/uEye_tools.h"
# endif
# ifdef SDK_4_0_0
# undef IS_CHAR
# include "uEye/SDK_4_0_0/uEye.h"
# include "uEye/SDK_4_0_0/uEye_tools.h"
# endif
# ifdef SDK_4_2_1
# undef IS_CHAR
# include "uEye/SDK_4_2_1/uEye.h"
# include "uEye/SDK_4_2_1/uEye_tools.h"
# endif
# ifdef SDK_4_2_2
# undef IS_CHAR
# include "uEye/SDK_4_2_2/uEye.h"
# include "uEye/SDK_4_2_2/uEye_tools.h"
# endif
# ifdef SDK_4_3_0
# undef IS_CHAR
# include "uEye/SDK_4_3_0/uEye.h"
# include "uEye/SDK_4_3_0/uEye_tools.h"
# endif
# ifdef SDK_4_6_0
# undef IS_CHAR
# include "uEye/SDK_4_6_0/uEye.h"
# include "uEye/SDK_4_6_0/uEye_tools.h"
# endif
# ifdef SDK_4_6_1
# undef IS_CHAR
# include "uEye/SDK_4_6_1/uEye.h"
# include "uEye/SDK_4_6_1/uEye_tools.h"
# endif
# ifdef SDK_4_7_0
# undef IS_CHAR
# include "uEye/SDK_4_7_0/uEye.h"
# include "uEye/SDK_4_7_0/uEye_tools.h"
# endif
# ifdef SDK_4_7_2
# undef IS_CHAR
# include "uEye/SDK_4_7_2/uEye.h"
# include "uEye/SDK_4_7_2/uEye_tools.h"
# endif
# ifdef SDK_4_8_0
# undef IS_CHAR
# include "uEye/SDK_4_8_0/uEye.h"
# include "uEye/SDK_4_8_0/uEye_tools.h"
# endif
# ifdef SDK_4_9_0
# undef IS_CHAR
# include "uEye/SDK_4_9_0/uEye.h"
# include "uEye/SDK_4_9_0/uEye_tools.h"
# endif
# ifdef SDK_4_9_1
# undef IS_CHAR
# include "uEye/SDK_4_9_0/uEye.h"
# include "uEye/SDK_4_9_0/uEye_tools.h"
# endif
#else
# undef IS_CHAR
#include <ueye.h>
#ifdef XV_WIN32
#include <ueye_tools.h>
#endif
#endif



typedef struct Treatement_struct
{
  void *pInfoBase;
  //int k;
  uint64_t lTimeStamp;
  //uint64_t lNumberImageDelivered;
} TreatementStruct;

# ifndef _UEYE_SOURCE_C_

PXV_VAR(int, nAviID);
PXV_VAR(int, rec_AVI);

PXV_VAR(int, iImages_in_UEYE_Buffer);
PXV_VAR(imreg  *,imr_UEYE);
PXV_VAR(pltreg  *,pr_UEYE);
PXV_VAR(O_i  *, oi_UEYE);
PXV_VAR(DIALOG *, d_UEYE);
PXV_VAR(BOOL , allegro_display_on);
PXV_VAR(BOOL, overlay);
PXV_VAR(int, do_refresh_overlay);
PXV_VAR(HIDS, m_hCam);			// handle to camera
PXV_VAR(SENSORINFO, sInfo);

PXV_VAR(void ** , UEYE_Im_buffer_ID);
PXV_VAR(BOOL , bEnableThread);
PXV_VAR(uint64_t, lNumberFramesDelivered);
PXV_VAR(uint64_t, lNumberFramestreated);

#ifdef XV_WIN32
PXV_VAR(VIEW_HANDLE, hView);
PXV_VAR(STREAM_HANDLE, hDS);
PXV_VAR(HANDLE , hReadyEvent);
#endif
# endif
# ifdef _UEYE_SOURCE_C_


int nAviID = -1;
int rec_AVI = 0;

HIDS	m_hCam;			// handle to camera
SENSORINFO sInfo;

uint64_t lNumberFramesDelivered = 0;   // written only by camera thread
uint64_t lNumberFramestreated = 0;     // written only by treatment thread

imreg   	*imr_UEYE = NULL;
pltreg  	*pr_UEYE = NULL;
O_i     	*oi_UEYE = NULL;
DIALOG  	*d_UEYE = NULL;
int iImages_in_UEYE_Buffer = 128;
int 		num_image_UEYE = -1;
BOOL 		allegro_display_on = TRUE;
BOOL 		overlay = TRUE;
//int 		do_refresh_overlay = 1;
int absolute_image_number = 0;
int lNumBuff;
void **UEYE_Im_buffer_ID = NULL;
BOOL bEnableThread;
//char win_title[512];

// Initialize all handles
//HANDLE hStreamEvent = NULL;
#ifdef XV_WIN32
HANDLE hReadyEvent = NULL;
HANDLE hFinishEvent = NULL;
HANDLE hEventFreeze = NULL;
HANDLE hEventNewImage = NULL;
HANDLE hEventRemoved = NULL;
HANDLE hEventReplug = NULL;
HANDLE EventList[4];
HANDLE hEventKill = NULL;
#endif


// Initialize all handles

int iStreamChannel = 0;


void StreamProcess(void);
void Terminate_UEYE_Thread(void);
void WaitForThreadToTerminate(void);
void CloseThreadHandle(void);
int add_oi_UEYE_2_imr (imreg *imr,O_i* oi);
#endif
#endif
