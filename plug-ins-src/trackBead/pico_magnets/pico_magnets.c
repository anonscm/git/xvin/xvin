/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
# ifndef _PICO_MAGNETS_C_
# define _PICO_MAGNETS_C_

# include "allegro.h"
#ifdef XV_WIN32
# include "winalleg.h"
#endif
# include "xvin.h"


/* If you include other plug-ins header do it here*/
# include "../magnetscontrol.h"
# include "../pico_serial/pico_serial.h"
# include "../action.h"
//# include "../fftbtl32n.h"
//# include "../fillibbt.h"

//# include "../trackBead.h"
//# include "../track_util.h"

/* But not below this define */
# define BUILDING_PLUGINS_DLL

# undef _PXV_DLL
# define _PXV_DLL   __declspec(dllexport)
# include "pico_magnets.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// these rotines all work in the Real time timer thread
///////////////////////////////////////////////////////////////////////////////////////////////////////////////


int grab_plot_from_serial_port(void);

char	*describe_magnets_device_pico(void)
{
  return "The translation and rotation of the magnets\n"
    "is faked by pico functions just for test...\n";
}

int _init_magnets_OK(void)
{
  static int init = 0;
  if (init_pico_serial())
    {
      win_printf_OK("Serial port initialisation failed !");
      return D_O_K;
    }

  if (init == 0)
    {
      add_item_to_menu(magnetscontrol_plot_menu(),"Grab transient",
		       grab_plot_from_serial_port,NULL,0,NULL);
      init = 1;
    }


  return 0;
}

int _has_magnets_memory(void)
{
 return 1;
}

int	_set_rot_value(float rot)
{
  char command[128] = {0}, answer[128] = {0};
  unsigned long len = 0;
  int roti, set = -1, ret;
  unsigned long dwErrors;

  roti = (int)(0.5 + Rot_Ticks_Per_Turn*rot);
  snprintf(command,128,"roti=%d&%d\r",roti,roti);
  /*Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
    answer[len] = 0;*/
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return (ret -5);
  if (sscanf(answer,"Roti aiming %d",&set) != 1)  return -1;
  if (roti == set)    n_rota = rot;
  return (roti == set) ? 0 : 1;
}


int	_set_rot_ref_value(float rot)
{
  char command[128] = {0}, answer[128] = {0};
  unsigned long len = 0;
  int ret;
  float set = -1;
  //unsigned long t0;
  unsigned long dwErrors;


  snprintf(command,128,"rhome=%f\r",rot);
  /*Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
    answer[len] = 0; */
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return (ret -5);
  if (sscanf(answer,"Rot home: %f",&set) != 1)  return -1;
  if (rot == set)    n_rota = rot;
  return (fabs(rot - set) > 0.01) ? 1 : 0;
}



float _read_rot_value(void)
{
  char command[128] = {0}, answer[128] = {0};
  unsigned long len = 0;
  int set = 0x7FFFFFFF, ret, rot_frac;
  float rot = 0;
  //unsigned long t0;
  unsigned long dwErrors;

  snprintf(command,128,"roti?\r");
  /*Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
    answer[len] = 0;*/
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return __FLT_MAX__;
  if (sscanf(answer,"Roti = %d",&set) != 1)  return __FLT_MAX__;
  if (set == 0x7FFFFFFF)  return __FLT_MAX__;
  rot_frac = Rot_Ticks_Per_Turn/2000;
  rot = ((float)(set+rot_frac))/Rot_Ticks_Per_Turn;
  rot = (float)((int)((1000*rot) + 0.5))/1000;
  return n_rota = rot;//n_rota - n_rot_offset;
}


int _request_read_rot_value(int image_n, unsigned long *t0)
{
  char command[128] = {0};

  if (rs232_register(READ_ROT_REQUEST,image_n,0)) return 1; // rs232 buisy
  if (t0 != NULL) *t0 = my_uclock();
  snprintf(command,128,"roti?\r");
  Write_serial_port(hCom,command,strlen(command));
  if (t0 != NULL) *t0 = my_uclock()- *t0;
  return 0;
}
float _read_rot_raw_from_request_answer(char *answer, int im_n, float val, int *error)
{
  int set = -1, rot_frac;
  float r = 0;

  (void)im_n;
  (void)val;
  *error = 0;
  if (answer != NULL)
    {
      rot_frac = Rot_Ticks_Per_Turn/2000;
      if (sscanf(answer,"Roti = %d",&set) == 1)
	r =  n_rota = ((float)(set+rot_frac))/Rot_Ticks_Per_Turn;
      else *error = PICO_COM_ERROR;
    }
  else *error =  PICO_COM_ERROR;
  return r;
}


int _request_set_rot_value_raw(int image_n, float rot, unsigned long *t0)
{
  char command[128] = {0};
  int roti;

  if (rs232_register(SET_ROT_REQUEST,image_n,rot)) return 1;
  if (t0 != NULL) *t0 = my_uclock();
  roti = (int)(0.5 + Rot_Ticks_Per_Turn*rot);
  snprintf(command,128,"roti=%d&%d\r",roti,roti);
  Write_serial_port(hCom,command,strlen(command));
  if (t0 != NULL) *t0 = my_uclock()- *t0;
  return 0;
}
int _check_set_rot_request_answer(char *answer, int im_n, float rot)
{
  int roti, set;

  (void)im_n;
  if (answer == NULL)     return PICO_COM_ERROR;
  roti = (int)(0.5 + Rot_Ticks_Per_Turn*rot);
  if (sscanf(answer,"Roti aiming %d",&set) == 1)
    {
      if (set == roti) return 0;
    }
  return PICO_COM_ERROR;
}

int _request_set_magnet_z_value_raw(int image_n, float pos,  unsigned long *t0)
{
  char command[128] = {0};
  int zmagi;

  if (rs232_register(SET_ZMAG_REQUEST,image_n,pos)) return 1;
  if (t0 != NULL) *t0 = my_uclock();
  pos = (pos > absolute_maximum_zmag) ? absolute_maximum_zmag : pos;
  zmagi = (int)(0.5 + Zmag_Ticks_Per_Mm*pos);
  snprintf(command,128,"zmagi=%d&%d\r",zmagi,zmagi);
  Write_serial_port(hCom,command,strlen(command));
  if (t0 != NULL) *t0 = my_uclock()- *t0;
  return 0;
}
int _check_set_zmag_request_answer(char *answer, int im_n, float pos)
{
  int zmagi, set ;
  (void)im_n;
  if (answer == NULL)     return PICO_COM_ERROR;
  pos = (pos > absolute_maximum_zmag) ? absolute_maximum_zmag : pos;
  zmagi = (int)(0.5 + Zmag_Ticks_Per_Mm*pos);
  if (sscanf(answer,"Zmag aiming %d",&set) == 1)
    {
      if (set == zmagi) return 0;
    }
  return PICO_COM_ERROR;
}


int _request_read_magnet_z_value(int image_n, unsigned long *t0)
{
  char command[128] = {0};

  if (rs232_register(READ_ZMAG_REQUEST,image_n,0)) return 1;
  if (t0 != NULL) *t0 = my_uclock();
  snprintf(command,128,"zmagi?\r");
  Write_serial_port(hCom,command,strlen(command));
  if (t0 != NULL) *t0 = my_uclock()- *t0;
  return 0;
}
float _read_zmag_raw_from_request_answer(char *answer, int im_n, float val, int *error, float *vcap)
{
  int set = -1;
  float z = 0;

  (void)val;
  (void)im_n;
  *error = 0;
  if (answer != NULL)
    {
      if (sscanf(answer,"Zmag = %d",&set) == 1)
	z = n_magnet_z = ((float)(set+4))/Zmag_Ticks_Per_Mm;
      else *error = PICO_COM_ERROR;
      if (strstr(answer,"PL") != NULL)	             *error |= ZMAG_TOP_LIMIT;
      if (strstr(answer,"NL") != NULL)	             *error |= ZMAG_BOTTOM_LIMIT;
      if (sscanf(answer,"Zmag = %d Vcap = %f",&set,vcap) == 2) 	*vcap /= 1000;
    }
  else *error =  PICO_COM_ERROR;
  return z;
}



int _request_set_zmag_ref_value(int image_n, float pos, unsigned long *t0)
{
  char command[128] = {0};

  if(rs232_register(SET_ZMAG_REF_REQUEST,image_n,pos)) return 1;
  if (t0 != NULL) *t0 = my_uclock();
  pos = (pos > absolute_maximum_zmag) ? absolute_maximum_zmag : pos;
  snprintf(command,128,"zhome=%f\r",pos);
  Write_serial_port(hCom,command,strlen(command));
  if (t0 != NULL) *t0 = my_uclock()- *t0;
  return 0;
}

int _check_set_zmag_ref_request_answer(char *answer, int im_n, float pos)
{
  float set;
  (void)im_n;
  if (answer == NULL)     return PICO_COM_ERROR;
  if (sscanf(answer,"Zmag home: %f",&set) == 1)
    {
      if (fabs(set - pos) < 0.005) return 0;
    }
  return PICO_COM_ERROR;
}


int _request_set_rot_ref_value(int image_n, float rot,  unsigned long *t0)
{
  char command[128] = {0};

  if(rs232_register(SET_ROT_REF_REQUEST,image_n,rot)) return 1;
  if (t0 != NULL) *t0 = my_uclock();
  snprintf(command,128,"rhome=%f\r",rot);
  Write_serial_port(hCom,command,strlen(command));
  if (t0 != NULL) *t0 = my_uclock()- *t0;
  return 0;
}

int _check_set_rot_ref_request_answer(char *answer, int im_n, float rot)
{
  float set;

  (void) im_n;
  if (answer == NULL)     return PICO_COM_ERROR;
  if (sscanf(answer,"Rot home: %f",&set) == 1)
    {
      if (fabs(set - rot) < 0.005) return 0;
    }
  return PICO_COM_ERROR;
}









int   _set_magnet_z_value(float pos)
{
  char command[128] = {0}, answer[128] = {0};
  unsigned long len = 0;
  int zmagi, set = -1, ret;
  unsigned long dwErrors;

  pos = (pos > absolute_maximum_zmag) ? absolute_maximum_zmag : pos;
  zmagi = (int)(0.5 + Zmag_Ticks_Per_Mm*pos);
  snprintf(command,128,"zmagi=%d&%d\r",zmagi,zmagi);
  /*Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
    answer[len] = 0; */
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return ret;
  if (sscanf(answer,"Zmag aiming %d",&set) != 1)  return -3;
  if (zmagi == set) n_magnet_z = pos;
  return (zmagi == set) ? 0 : 3;
}

float _read_magnet_z_value(void)
{
  char command[128] = {0}, answer[128] = {0};
  unsigned long len = 0;
  int  set = 0x7FFFFFFF, ret;
  unsigned long dwErrors;

  snprintf(command,128,"zmagi?\r");
  /*Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
    answer[len] = 0; */
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return __FLT_MAX__;
  if (sscanf(answer,"Zmag = %d",&set) != 1)  return __FLT_MAX__;
  if (set == 0x7FFFFFFF)  return __FLT_MAX__;
  n_magnet_z = ((float)(set+4))/Zmag_Ticks_Per_Mm;
  n_magnet_z = (float)((int)((1000*n_magnet_z) + 0.5))/1000;
  return n_magnet_z;
}

int _read_magnet_z_value_and_status(float *z, float *vcap, int *limit, int *status)
{
  char command[128] = {0}, answer[128] = {0};
  unsigned long len = 0;
  int  set = -1, cap = -1, ret = 0, nr = 0;
  unsigned long dwErrors;

  snprintf(command,128,"zmagi?\r");
  /*Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
    answer[len] = 0; */
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return ret;
  if (status) *status = 0;
  if (limit) *limit = 0;
  if (vcap) *vcap = 0;
  if (z) *z = 0;
  nr = sscanf(answer,"Zmag = %d",&set);
  if (nr == 1)
    {
      if (strstr(answer+7,"BR") != NULL && status != NULL) *status = 4;
      if (strstr(answer+7,"FO") != NULL && status != NULL) *status = 2;
      nr = sscanf(answer,"Zmag = %d Vcap = %d",&set,&cap);
      if (nr == 2)
	{
	  if (status) *status = 0;
	}
      else
	{
	  nr = sscanf(answer,"Zmag = %d Vservo = %d",&set,&cap);
	  if (nr == 2 && status != NULL) *status = 1;
	}
    }
  else ret = 3;
  if (strstr(answer,"NL") != NULL && limit != NULL) *limit = -1;
  if (strstr(answer,"PL") != NULL && limit != NULL) *limit = 1;
  if (nr > 0)
    {
      n_magnet_z = ((float)(set+4))/Zmag_Ticks_Per_Mm;
      n_magnet_z = (float)((int)((1000*n_magnet_z) + 0.5))/1000;
      if (z) *z = n_magnet_z;
    }
  if ((nr > 1) && (vcap != NULL))
      *vcap = ((float)(cap))/1000;
  return ret;
}



int _set_motors_speed(float v)
{
  char command[128] = {0}, answer[128] = {0};
  unsigned long len = 0;
  int vzi, set = -1, ret;
  unsigned long dwErrors;

  // 8 pulses per ms => 1 mm per sec
  v_rota = fabs(v);
  vzi = (int)(0.5 + 8*fabs(v));
  vzi = (vzi < 1) ? 1 : vzi; // motors do not like 0 velocity !
  snprintf(command,128,"rpid7=%d\r",vzi);
  /*  Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
    answer[len] = 0;*/
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return ret;
  if (sscanf(answer,"Rot vmax %d",&set) != 1) return 1;
  return (vzi == set) ? 0 : 1;
}
float  _get_motors_speed(void)
{
  char command[128] = {0}, answer[128] = {0};
  unsigned long len = 0;
  int  set = -1, ret;
  unsigned long dwErrors;

  // 8 pulses per ms => 1 mm per sec
  snprintf(command,128,"rpid7?\r");
  /*Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
    answer[len] = 0; */
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return __FLT_MAX__;
  if (sscanf(answer,"Rot vmax %d",&set) != 1) return __FLT_MAX__;
  v_rota = (float)set/8;
    return v_rota;
}



int _set_zmag_motor_speed(float v)
{
  char command[128] = {0}, answer[128] = {0};
  unsigned long len = 0;
  int vzi, set = -1, ret;
  unsigned long dwErrors;

  // 8 pulses per ms => 1 mm per sec
  v_mag = fabs(v);
  vzi = (int)(0.5 + 8*fabs(v));
  vzi = (vzi > 120) ? 120 : vzi;
  vzi = (vzi < 1) ? 1 : vzi; // motors do not like 0 velocity !
  snprintf(command,128,"zpid7=%d\r",vzi);
  /*Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
    answer[len] = 0; */
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return ret;
  if (sscanf(answer,"Zmag vmax %d",&set) != 1) return 1;
  return (vzi == set) ? 0 : 1;
}
float  _get_zmag_motor_speed(void)
{
  char command[128] = {0}, answer[128] = {0};
  unsigned long len = 0;
  int  set = -1, ret;
  unsigned long dwErrors;

  // 8 pulses per ms => 1 mm per sec
  snprintf(command,128,"zpid7?\r");
  /*Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
    answer[len] = 0; */
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return __FLT_MAX__;
  if (sscanf(answer,"Zmag vmax %d",&set) != 1)
    {
      my_set_window_title("Pb retrieving data in %s",answer);
      return __FLT_MAX__;
    }
  v_mag = (float)set/8;
  return v_mag;
}





int _set_zmag_motor_pid(int num, int val)
{
  char command[128] = {0}, answer[128] = {0}, ison[32] = {0};
  unsigned long len = 0;
  int  set = -1, ret, iret = 0;
  unsigned long dwErrors;

  if (num < 0 || num > 9) return 1;
  if (num < 3 && (val < 0 || val > 15))                   return 2;
  else if (num == 2 && (val < 0 || val > 255))            return 3;
  else if (num > 4 && num < 7 && (val < 0 || val > 128)) return 4;
  if (num == 7 && val < 1) val = 1;  // motors do not like 0 velocity !
  if (num == 7 && val > 120) val = 120;  // motors do not like 0 velocity !
  snprintf(command,128,"zpid%d=%d\r",num,val);
  /*Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
    answer[len] = 0;*/
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return ret;
  iret = 0;
  if (num == 0)       iret = sscanf(answer,"Zmag P %d",&set);
  else if (num == 1)  iret = sscanf(answer,"Zmag I %d",&set);
  else if (num == 2)  iret = sscanf(answer,"Zmag D %d",&set);
  else if (num == 3)  iret = sscanf(answer,"zmax PWM %d",&set);
  else if (num == 4)  iret = sscanf(answer,"zmax Range %d",&set);
  else if (num == 5)  iret = sscanf(answer,"Zmag vP %d",&set);
  else if (num == 6)  iret = sscanf(answer,"Zmag vI %d",&set);
  else if (num == 7)  iret = sscanf(answer,"Zmag vmax %d",&set);
  else if (num == 8)
    {
      iret = sscanf(answer,"Zmag PID %s",ison);
      set = (strstr(ison,"On")) ? 1 : 0;
    }
  else if (num == 9)
    {
      iret = sscanf(answer,"brake %s",ison);
      set = (strstr(ison,"On")) ? 1 : 0;
    }
  if (iret == 0) return 5;
  return (val == set) ? 0 : 1;
}
int  _get_zmag_motor_pid(int num)
{
  char command[128] = {0}, answer[128] = {0}, ison[32] = {0};
  unsigned long len = 0;
  int  set = -1, ret, iret = 0;
  unsigned long dwErrors;

  if (num < 0 || num > 9) return -1;
  snprintf(command,128,"zpid%d?\r",num);
  /*Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
    answer[len] = 0;*/
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return (ret-5);

  if (num == 0)       iret = sscanf(answer,"Zmag P %d",&set);
  else if (num == 1)  iret = sscanf(answer,"Zmag I %d",&set);
  else if (num == 2)  iret = sscanf(answer,"Zmag D %d",&set);
  else if (num == 3)  iret = sscanf(answer,"zmax PWM %d",&set);
  else if (num == 4)  iret = sscanf(answer,"zmag ticks/mm %d",&set);
  else if (num == 5)  iret = sscanf(answer,"Zmag vP %d",&set);
  else if (num == 6)  iret = sscanf(answer,"Zmag vI %d",&set);
  else if (num == 7)  iret = sscanf(answer,"Zmag vmax %d",&set);
  else if (num == 8)
    {
      iret = sscanf(answer,"Zmag PID %s",ison);
      set = (strstr(ison,"On")) ? 1 : 0;
    }
  else if (num == 9)
    {
      iret = sscanf(answer,"brake %s",ison);
      set = (strstr(ison,"On")) ? 1 : 0;
    }
  if (iret == 0) return -5;
  return set;
}



int _set_rotation_motor_pid(int num, int val)
{
  char command[128] = {0}, answer[128] = {0}, ison[32] = {0};
  unsigned long len = 0;
  int  set = -1, ret, iret = 0;
  unsigned long dwErrors;

  if (num < 0 || num > 9) return 1;
  if (num < 3 && (val < 0 || val > 15))                   return 2;
  else if (num == 2 && (val < 0 || val > 255))            return 3;
  else if (num > 4 && num < 7 && (val < 0 || val > 128)) return 4;
  if (num == 7 && val < 1) val = 1;  // motors do not like 0 velocity !
  snprintf(command,128,"rpid%d=%d\r",num,val);
  /*Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
    answer[len] = 0;*/
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return ret;

  iret = 0;
  if (num == 0)       iret = sscanf(answer,"Rot P %d",&set);
  else if (num == 1)  iret = sscanf(answer,"Rot I %d",&set);
  else if (num == 2)  iret = sscanf(answer,"Rot D %d",&set);
  else if (num == 3)  iret = sscanf(answer,"max PWM %d",&set);
  else if (num == 4)  iret = sscanf(answer,"Nb. of ticks per turn %d",&set);
  else if (num == 5)  iret = sscanf(answer,"Rot vP %d",&set);
  else if (num == 6)  iret = sscanf(answer,"Rot vI %d",&set);
  else if (num == 7)  iret = sscanf(answer,"Rot vmax %d",&set);
  else if (num == 8)
    {
      iret = sscanf(answer,"Rot PID %s",ison);
      set = (strstr(ison,"On")) ? 1 : 0;
    }
  else if (num == 9)
    {
      iret = sscanf(answer,"brake %s",ison);
      set = (strstr(ison,"On")) ? 1 : 0;
    }
  if (iret == 0) return 5;
  return (val == set) ? 0 : 1;
}
int  _get_rotation_motor_pid(int num)
{
  char command[128] = {0}, answer[128] = {0}, ison[32] = {0};
  unsigned long len = 0;
  int  set = -1, ret, iret = 0;
  unsigned long dwErrors;

  if (num < 0 || num > 9) return -1;
  snprintf(command,128,"rpid%d?\r",num);
  /*Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
    answer[len] = 0; */
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return (ret-5);
  if (num == 0)       iret = sscanf(answer,"Rot P %d",&set);
  else if (num == 1)  iret = sscanf(answer,"Rot I %d",&set);
  else if (num == 2)  iret = sscanf(answer,"Rot D %d",&set);
  else if (num == 3)  iret = sscanf(answer,"max PWM %d",&set);
  else if (num == 4)  iret = sscanf(answer,"Nb. of ticks per turn %d",&set);
  else if (num == 5)  iret = sscanf(answer,"Rot vP %d",&set);
  else if (num == 6)  iret = sscanf(answer,"Rot vI %d",&set);
  else if (num == 7)  iret = sscanf(answer,"Rot vmax %d",&set);
  else if (num == 8)
    {
      iret = sscanf(answer,"Rot PID %s",ison);
      set = (strstr(ison,"On")) ? 1 : 0;
    }
  else if (num == 9)
    {
      iret = sscanf(answer,"brake %s",ison);
      set = (strstr(ison,"On")) ? 1 : 0;
    }
  if (iret == 0) return -5;
  return set;
}



int _set_magnet_z_ref_value(float pos)  /* in mm */
{
  char command[128] = {0}, answer[128] = {0};
  unsigned long len = 0;
  int  ret;
  float set = -1;
  unsigned long dwErrors;

  pos = (pos > absolute_maximum_zmag) ? absolute_maximum_zmag : pos;
  snprintf(command,128,"zhome=%f\r",pos);
  /*Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
    answer[len] = 0;*/
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return ret;
  if (sscanf(answer,"Zmag home: %f",&set) != 1) return 1;
  n_magnet_z = pos;
  return (fabs(pos - set) > 0.01) ? 1 : 0;
}

int   _set_z_origin(float pos, int dir)
{
  char command[128] = {0}, answer[128] = {0};
  unsigned long len = 0;
  int ret;
  float  set = -1;
  unsigned long dwErrors;

  snprintf(command,128,"zori%d=%g\r",(dir==0)?0:1,pos);
  /*Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
    answer[len] = 0;*/
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return ret;
  if (sscanf(answer,"Zmag origin: %f",&set) != 1) return 1;
  return (pos == set) ? 0 : 1;
}


int   _set_Vcap_min(float volts)
{
  char command[128] = {0}, answer[128] = {0};
  unsigned long len = 0;
  int ret = 0;
  float  set = -1;
  unsigned long dwErrors;

  snprintf(command,128,"zvcap=%f\r",volts);
  /*Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
    answer[len] = 0;*/
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return ret;
  if (sscanf(answer,"Vcap_min %f",&set) != 1) ret = 1;
  else if (fabs(set - volts) > 0.010) ret = 1;
  else ret = 0;
  return ret;
}

float  _get_Vcap_min(void)
{
  char command[128] = {0}, answer[128] = {0};
  unsigned long len = 0;
  int ret;
  float set = -1;
  unsigned long dwErrors;

  snprintf(command,128,"zvcap?\r");
  /*Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
    answer[len] = 0; */
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return ret;
  if (sscanf(answer,"Vcap_min %f",&set) != 1) return -1;
  else return set;
}

/* pb de timing ?

int   _get_z_origin(float pos, int dir)
{
  char command[128], answer[128], *gr;
  unsigned long len = 0;
  int ret, iret;
  float  set = -1;
  unsigned long t0;

  snprintf(command,128,"zori%d?\r",(dir==0)?0:1);
  Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
  answer[len] = 0;
  gr = strstr(answer," origin: ");
  if (gr != NULL) sscanf(gr," origin: %f",&set);
  //win_printf("answer %s\ndac %d",answer,set);
  //if (len)my_set_window_title(answer);
  return (pos == set) ? 0 : 1;
}

*/


int    _set_magnet_z_value_and_wait(float pos)
{
  pos = (pos > absolute_maximum_zmag) ? absolute_maximum_zmag : pos;
  //pos -=  n_magnet_offset;
  n_magnet_z = pos;// + n_magnet_offset;
  return 0;
}
int    _set_rot_value_and_wait(float rot)
{
  //rot -= n_rot_offset;
  n_rota = rot;// + n_rot_offset;
  return 0;
}

int	_go_and_dump_z_magnet(float z)
{
  n_magnet_z = z;
  _set_magnet_z_value(n_magnet_z);
  return 0;
  //return dump_to_specific_log_file_with_time(CURRENT_LOG_FILE,"Magnet Z changed auto to %g\n",n_magnet_z);
}
int    _go_wait_and_dump_z_magnet(float zmag)
{
  if (n_magnet_z == zmag)		return 0;
  _set_magnet_z_value_and_wait(zmag);
  return 0;
  //return dump_to_specific_log_file_with_time(CURRENT_LOG_FILE,"Magnet Z changed auto to %g\n",n_magnet_z);
}

int    _go_and_dump_rot(float r)
{
  //n_rota = r;
  _set_rot_value(r);
  return 0;
  //return dump_to_specific_log_file_with_time(CURRENT_LOG_FILE,"Rotation number auto changed %g\n",n_rota);
}
int	_go_wait_and_dump_rot(float r)
{
  if ((n_rota) == r)		return 0;
  _set_rot_value_and_wait(r);
  return 0;
  //return dump_to_specific_log_file_with_time(CURRENT_LOG_FILE,"Rotation number auto changed %g\n",n_rota);
}


int	_go_wait_and_dump_log_specific_rot(float r, char *log)
{
  (void)log;
  if ((n_rota) == r)		return 0;
  _set_rot_value_and_wait(r);
  //dump_to_specific_log_file_only(log,"Rotation number auto changed %g\n",n_rota);
  return 0;
}

int	_go_wait_and_dump_log_specific_z_magnet(float zmag, char *log)
{
  (void)log;
  if (n_magnet_z == zmag)		return 0;
  _set_magnet_z_value_and_wait(zmag);
  //dump_to_specific_log_file_only(log,"Magnet Z changed auto to %g\n",n_magnet_z);
  return 0;
}


int grab_plot_from_serial_port(void)
{
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *ds = NULL, *ds2 = NULL;
  static int axe = 0;
  int  i, nx = 512, a = 0, b = 0, c = 0, ret = 0;
  char Command[128] = {0}, bug[256] = {0}, answer[128] = {0};
  unsigned long lpNumberOfBytesWritten = 0;
  unsigned long nNumberOfBytesToRead = 127;

  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return(win_printf("can't find plot"));

  i = win_scanf("choose the axis to record transitory\n"
		"Rot %R Zmag %r focus %r\n",&axe);
  if (i == WIN_CANCEL) return D_O_K;


  bug[0] = 0; answer[0] = 0;
  op = create_and_attach_one_plot(pr, nx,nx, 0);
  if (op == NULL)    return win_printf("Cannot allocate plot");
  ds = op->dat[0];
  if (ds == NULL)    return win_printf("Cannot allocate plot");
  if (axe < 2)
    {
      ds2 = create_and_attach_one_ds(op, nx, nx, 0);
      if (ds2 == NULL)    return win_printf("Cannot allocate plot");
    }


  if (axe == 0) snprintf(Command,128,"rtran=0\r\n");
  else if (axe == 1) snprintf(Command,128,"ztran=0\r\n");
  else if (axe == 2) snprintf(Command,128,"ftran=0\r\n");
  i = 0;
  Write_serial_port(hCom,Command,9);
  ret = Read_serial_port(hCom,answer,nNumberOfBytesToRead,&lpNumberOfBytesWritten);
  for  (; ret <= 0; )
    ret = Read_serial_port(hCom,answer,nNumberOfBytesToRead,&lpNumberOfBytesWritten);

  if (axe == 0) snprintf(Command,128,"rtran?\r\n");
  else if (axe == 1) snprintf(Command,128,"ztran?\r\n");
  else if (axe == 2) snprintf(Command,128,"ftran?\r\n");



  //win_printf("0 %s",answer);
  for (i = 0; i < nx; )
    {
      Write_serial_port(hCom,Command,8);
      ret = Read_serial_port(hCom,answer,nNumberOfBytesToRead,&lpNumberOfBytesWritten);
      for  (; ret <= 0; )
	ret = Read_serial_port(hCom,answer,nNumberOfBytesToRead,&lpNumberOfBytesWritten);
      if (axe < 2)
	{
	  if (sscanf(answer,"%d\t%d\t%d",&a,&b,&c) == 3)
	    {
	      ds->xd[i] = ds2->xd[i] = a;
	      ds->yd[i] = b;
	      ds2->yd[i] = c;
	      i++;
	    }
	  else snprintf(bug,256,"line %d >%s<len \n",i,answer);
	  if (i%16 == 0) display_title_message("reading %d >%d %d %d",i,a,b,c);
	}
      else
	{
	  if (sscanf(answer,"%d\t%d",&a,&b) == 2)
	    {
	      ds->xd[i] = a;
	      ds->yd[i] = b;
	      i++;
	    }
	  else snprintf(bug,256,"line %d >%s<len \n",i,answer);
	  if (i%16 == 0) display_title_message("reading %d >%d %d",i,a,b);
	}

    }

  refresh_plot(pr, pr->n_op-1);
  return D_O_K;
}





# ifdef KEEP


int  do_motor1_in_turn_pico(void)
{
  register int  i;
  float rot;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf("This routine moves motor 1 \n"
			"to specified position in turn");
    }
  rot = n_rota;
  i = win_scanf("enter motor 2 new position %f",&rot);
  if (i == CANCEL)	return OFF;
  set_rot_value_pico(rot);
  return 0;
}
int  do_motor1_in_turn_and_wait_pico(char ch)
{
  register int  i;
  float rot;
  char pos[64] = {0};

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf("This routine moves motor 1 \n"
			"to specified position in turn");
    }

  rot = read_rot_value_pico();
  snprintf(pos,64,"position read %g \n enter new position %%f",rot);
  rot = n_rota;
  i = win_scanf(pos,&rot);
  if (i == CANCEL)	return OFF;
  set_rot_value_and_wait_pico(rot);
  return 0;
}



int	do_motor2_in_mm_pico(void)
{
  register  int i;
  float pos;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf("This routine moves motor 1 \n"
			"to specified position in mm");
    }
  /* we first find the data that we need to transform */
  pos =  n_magnet_z;
  i = win_scanf("enter motor 1 new position %f in mm",&pos);
  if (i == CANCEL)	return OFF;
  set_magnet_z_value_pico(pos);
  return 0;
}
int	do_motor2_in_mm_and_wait_pico(char ch)
{
  register  int i;
  float pos;
  char question[64] = {0};

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf("This routine moves motor 2 \n"
			"to specified position in mm");
    }

  pos = read_magnet_z_value_pico();
  snprintf(question,64,"read pos %g \n new pos in mm %%f",pos);
  pos =  n_magnet_z;
  i = win_scanf(question,&pos);
  if (i == CANCEL)	return OFF;
  set_magnet_z_value_and_wait_pico(pos);
  return 0;
}
int	do_motor2_in_mm_n_time_pico(void)
{
  register  int i, j;
  static float pos0 = 17,  pos_step = .1;
  static int nstep = 5, ntimes = 100;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf("This routine moves motor 1 \n"
			"to specified position in mm");
    }

  pos0 =  n_magnet_z;
  i = win_scanf("Zmag multi test \nenter motor max position in mm "
		"%fpos step %f nstep %d ntimes %d",
		&pos0,&pos_step,&nstep,&ntimes);
  if (i == CANCEL)	return OFF;
  for ( i = 0; i < ntimes; i++)
    {
      for ( j = 0; j < nstep; j++)
	{
	  set_magnet_z_value_pico(pos0+j*pos_step);
	}
    }
  return 0;
}
int    do_rotate_n_time_pico(void)
{
  register  int i, j;
  static float pos0 = 17,  pos_step = .1;
  static int nstep = 5, ntimes = 100;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf("This routine moves motor 1 \n"
			"to specified position in turns");
    }


  pos0 =  n_rota;
  i = win_scanf("Rotation multi test \nenter motor max position in mm "
		"%fpos step %f nstep %d ntimes %d",
		&pos0,&pos_step,&nstep,&ntimes);
  if (i == CANCEL)	return OFF;
  for ( i = 0; i < ntimes; i++)
    {
      for ( j = 0; j < nstep; j++)
	{
	  set_rot_value_pico(pos0+j*pos_step);
	}
    }
  return 0;
}

# endif


MENU *pico_magnets_plot_menu(void)
{
	static MENU mn[32] = {0};

	if (mn[0].text != NULL)	return mn;
	//add_item_to_menu(mn,"data set rescale in Y", do_pico_magnets_rescale_data_set,NULL,0,NULL);
	//add_item_to_menu(mn,"plot rescale in Y", do_pico_magnets_rescale_plot,NULL,0,NULL);
	return mn;
}

int	pico_magnets_main(int argc, char **argv)
{
  static int init = 0;
  //	add_plot_treat_menu_item ( "pico_magnets", NULL, pico_magnets_plot_menu(), 0, NULL);
  (void)argc;
  (void)argv;
  if (init == 0)
    {
      add_item_to_menu(magnetscontrol_plot_menu(),"Grab transient",
		       grab_plot_from_serial_port,NULL,0,NULL);
      init = 1;
    }

	return D_O_K;
}

int	pico_magnets_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  //remove_item_to_menu(plot_treat_menu, "pico_magnets", NULL, NULL);
	return D_O_K;
}
#endif
