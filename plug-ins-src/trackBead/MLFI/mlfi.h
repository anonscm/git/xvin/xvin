#ifndef _MLFI_H_
#define _MLFI_H_
//#pragma once
PXV_FUNC(int, do_mlfi_rescale_plot, (void));
PXV_FUNC(MENU*, mlfi_plot_menu, (void));
PXV_FUNC(int, do_mlfi_rescale_data_set, (void));
PXV_FUNC(int, mlfi_main, (int argc, char **argv));
PXV_FUNC(O_p*, prepare_mlfi_thread,(pltreg *pr));
PXV_FUNC(double, grab_previous_mfi_data, (void));
#endif
