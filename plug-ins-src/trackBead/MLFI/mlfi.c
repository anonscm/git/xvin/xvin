/*
 *    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
 */
#ifndef _MLFI_C_
#define _MLFI_C_

#include <allegro.h>
#include "winalleg.h"
# include "xvin.h"
#include <pthread.h>

/* If you include other regular header do it here*/
#include "ziAPI.h"
#ifdef _WIN32
#include <windows.h>
#define PRId64 "d"
#define PRIu64 "u"
#define PRsize_t "I"
#define PRptrdiff_t "I"
#else
#include <inttypes.h>
#define PRsize_t "z"
#define PRptrdiff_t "t"
#include <string.h>
#include <unistd.h>
#endif
# define USE_MLFI
#include "../track_util.h"

#define DEMOD_COUNT 4
#define PID_COUNT 4

/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "mlfi.h"

//# include "ziUtils.h"

# define MLFI_ID 245

struct MyZIgenData {
    ZIConnection conn;
    char serverAddress[128];
    char node_name[16][128];

    uint64_t lastTimestamp[16];
    uint64_t totalSampleCount[16];
    double dataTransferRate[16];
    uint64_t expectedTimestampDiff[16];
    uint64_t eventsWithDataloss[16];
    int ds_nb[16];
    ZIIntegerData clockbase;
    const char *deviceId;
};
typedef struct MyZIgenData MyZIgenData;

MyZIgenData GenD;


/// The structure used to hold data for a single demodulator sample
struct MyZIDemodSample {
    /// inode => eventPath
    int inode;
    /// The timestamp at which the sample has been measured.
    ZITimeStamp timeStamp;  // MFFI one
    /// X part of the sample.
    float x;
    /// Y part of the sample.
    float y;

    /// oscillator frequency at that sample.
    float frequency;
    /// oscillator phase at that sample.
    float phase;

    /// the current bits of the DIO.
    uint32_t dioBits;

    /// trigger bits
    uint32_t trigger;

    /// value of Aux input 0.
    float auxIn0;
    /// value of Aux input 1.
    float auxIn1;
    unsigned char n_read;
    unsigned char new;
    unsigned char not_treated;
    int n_pack;
    int n_rec;
    long stimestamp;        // PC one
};
typedef struct MyZIDemodSample MyZIDemodSample;


# define N_PACK 131072
MyZIDemodSample packet[N_PACK];
int i_pack_r = 0;  // packet recieved in rolling buffer [0, N_PACK[, i_packet_r is the next packet to fill-in
int ai_pack_r = 0; // packet recieved in absolute number
int i_pack_t = 0;  // packet treated in rolling buffer [0, N_PACK[
int ai_pack_t = 0; // packet treated in absolute number
int pack_overrun = 0;  // packet written before treated
int grabbing_mlfi = 1;
char warning_in_thread[256] = {0};

ZITimeStamp starting_timeStamp = 0, abs_last_timeStamp = 0;

pthread_t thread_mlfi = 0;
O_p *opm = NULL;

float scope_t_size = 10.0;

char mlfidatafilename[2048] = {0};

# define ROLLING_BUFFER_SIZE 65536



double grab_previous_mfi_data(void)
{
    static int prev_ai_pack_r = -1;
    int now_ai_pack_r = ai_pack_r;
    double avg = 0, tx, ty;
    int i, n_avg = 0;

    prev_ai_pack_r = (prev_ai_pack_r < 0) ? ai_pack_r - 20 : prev_ai_pack_r;
    prev_ai_pack_r = (prev_ai_pack_r < 0) ? 0 : prev_ai_pack_r;

    for (i = prev_ai_pack_r, avg = 0, n_avg = 0; i < now_ai_pack_r; i++)
        {
            if (packet[i%N_PACK].inode == 0)
                {
                    //ds->xd[k] = packet[i%N_PACK].timeStamp - starting_timeStamp;
                    tx = packet[i%N_PACK].x;
                    ty = packet[i%N_PACK].y;
                    avg += 1000* sqrt(tx*tx+ty*ty);
                    n_avg++;
                }
        }
    avg = (n_avg > 0) ? avg/n_avg : avg;
    return avg;
}


static void *grab_mlfi_data(void* data)
{
    ZIEvent* Event;
    FILE *fp = NULL;
    /* Allocate ZIEvent in heap memory instead of getting it from stack will secure against stack overflows
       especially in windows.    */

    (void)data;
    if ((Event = ziAPIAllocateEventEx()) == NULL){
        win_printf("[ERROR] Can't allocate memory for Event.\n");
    }



    int n = 0, inode = 0, i;
    while (grabbing_mlfi) {
        n++;
        // Poll for data with timeout.
        if (ziAPIPollDataEx(GenD.conn, Event, 1000) == ZI_INFO_SUCCESS)
            {
                if (strlen(mlfidatafilename) > 0)
                    fp = fopen (mlfidatafilename,"a");

                switch (Event->valueType) {
                case ZI_VALUE_TYPE_DEMOD_SAMPLE:
                    {
                        for (inode=0; inode < DEMOD_COUNT + PID_COUNT; inode++)
                            if (stricmp(GenD.node_name[inode],(const char*)Event->path) == 0) break;
                        if (inode < DEMOD_COUNT + PID_COUNT)
                            {
                                // Check for sampleloss between polled events.
                                if (GenD.lastTimestamp[inode] != 0) {
                                    uint64_t timestampDiff = Event->value.demodSample[0].timeStamp - GenD.lastTimestamp[inode];
                                    GenD.lastTimestamp[inode] = Event->value.demodSample[Event->count - 1].timeStamp;
                                    if (timestampDiff != GenD.expectedTimestampDiff[inode]) {
                                        snprintf(warning_in_thread,sizeof(warning_in_thread),"[WARN] event %d sampleloss detected between events from %s.\n",n,Event->path);
                                        GenD.eventsWithDataloss[inode]++;
                                        GenD.lastTimestamp[inode] = Event->value.demodSample[Event->count - 1].timeStamp;
                                        break;
                                    }
                                }
                                // Check for sampleloss between all samples within the received event
                                for (i = 1; i < (int)Event->count; ++i) {
                                    uint64_t timestampDiff = Event->value.demodSample[i].timeStamp - Event->value.demodSample[i-1].timeStamp;
                                    if (timestampDiff != GenD.expectedTimestampDiff[inode]) {
                                        snprintf(warning_in_thread,sizeof(warning_in_thread),"[WARN] sampleloss detected within %s.\n",Event->path);
                                        GenD.eventsWithDataloss[inode]++;
                                        break;
                                    }
                                }
                                for (i = 0; i < (int)Event->count; i++)
                                    {
                                        GenD.lastTimestamp[inode] = Event->value.demodSample[i].timeStamp;
                                        packet[i_pack_r].stimestamp = my_uclock();
                                        pack_overrun += (packet[i_pack_r].not_treated) ? 1 : 0;
                                        packet[i_pack_r].inode = inode;
                                        packet[i_pack_r].x = Event->value.demodSample[i].x;
                                        packet[i_pack_r].y = Event->value.demodSample[i].y;
                                        packet[i_pack_r].timeStamp = Event->value.demodSample[i].timeStamp;
                                        if (ai_pack_r == 0) starting_timeStamp = packet[i_pack_r].timeStamp;
                                        abs_last_timeStamp = (packet[i_pack_r].timeStamp > abs_last_timeStamp)
                                            ? packet[i_pack_r].timeStamp : abs_last_timeStamp;
                                        packet[i_pack_r].frequency = Event->value.demodSample[i].frequency;
                                        packet[i_pack_r].phase = Event->value.demodSample[i].phase;
                                        packet[i_pack_r].dioBits = Event->value.demodSample[i].dioBits;
                                        packet[i_pack_r].trigger = Event->value.demodSample[i].trigger;
                                        packet[i_pack_r].auxIn0 = Event->value.demodSample[i].auxIn0;
                                        packet[i_pack_r].auxIn1 = Event->value.demodSample[i].auxIn1;
                                        packet[i_pack_r].new = 1;
                                        packet[i_pack_r].not_treated = 1;
                                        if (fp != NULL)
                                            {
                                                fprintf(fp,"%d,\t%I64d,\t"
                                                        "%g,\t%g,\t%g,\t%g,\t"
                                                        "%u,\t%u,\t%g,\t%g,\t%lu,\t%g\n"
                                                        ,packet[i_pack_r].inode,packet[i_pack_r].timeStamp
                                                        ,packet[i_pack_r].x,packet[i_pack_r].y
                                                        ,packet[i_pack_r].frequency,packet[i_pack_r].phase
                                                        ,packet[i_pack_r].dioBits,packet[i_pack_r].trigger
                                                        ,packet[i_pack_r].auxIn0,packet[i_pack_r].auxIn1
                                                        ,packet[i_pack_r].stimestamp,prev_zmag);
                                            }
                                        ai_pack_r++;
                                        i_pack_r++;
                                        i_pack_r = (i_pack_r < N_PACK) ? i_pack_r : 0;
                                    }
                                GenD.totalSampleCount[inode] += Event->count;
                            }
                        else snprintf(warning_in_thread,sizeof(warning_in_thread),"Node %s not recognized!",Event->path);
                        // The fields of the demodulator sample can be accessed as following.
                        // for (size_t i = 0; i < 1; ++i) {
                        //   printf("[INFO]    - sample %" PRsize_t "d TS = %" PRsize_t "d, X = %e, Y = %e, auxin0 = %e, auxin1 = %e, bits = %d\n",
                        //          i, Event->value.demodSample[i].timeStamp, Event->value.demodSample[i].x, Event->value.demodSample[i].y,
                        //          Event->value.demodSample[i].auxIn0, Event->value.demodSample[i].auxIn1, Event->value.demodSample[i].dioBits);
                        // }
                    }
                    break;
                case ZI_VALUE_TYPE_DOUBLE_DATA:
                    snprintf(warning_in_thread,sizeof(warning_in_thread),"[INFO] Poll returned double data from %s.\n",Event->path);
                    break;
                case ZI_VALUE_TYPE_INTEGER_DATA:
                    snprintf(warning_in_thread,sizeof(warning_in_thread),"[INFO] Poll returned integer data from %s.\n",Event->path);
                    break;
                case ZI_VALUE_TYPE_DOUBLE_DATA_TS:
                    {
                        // API Level >= 4 only: Double data with a timestamp, e.g., /devX/pids/0/stream/value
                        // std::cout << "[INFO] Poll returned double data with timestamps from " << Event->path << ".\n";
                        for (inode=0; inode < DEMOD_COUNT + PID_COUNT; inode++)
                            if (stricmp(GenD.node_name[inode],(const char*)Event->path) == 0) break;
                        if (inode < DEMOD_COUNT + PID_COUNT)
                            {
                                GenD.totalSampleCount[inode] += Event->count;

                                if ((GenD.expectedTimestampDiff[inode] == 0) && (Event->count > 1)) {
                                    // Due to #9093 (see above), we use the first measured timestamp delta as the reference for all subsequent
                                    // samples.
                                    GenD.expectedTimestampDiff[inode] = Event->value.doubleDataTS[1].timeStamp - Event->value.doubleDataTS[0].timeStamp;
                                    GenD.dataTransferRate[inode] = (double)GenD.clockbase/GenD.expectedTimestampDiff[inode];
                                    snprintf(warning_in_thread,sizeof(warning_in_thread)
                                             ,"[INFO] %s  measured rate %lg ,measured timestamp delta: %I64d",Event->path
                                             ,GenD.dataTransferRate[inode],GenD.expectedTimestampDiff[inode]);
                                }
                                // Check for sampleloss between polled events
                                if (GenD.lastTimestamp[inode] != 0) {
                                    uint64_t timestampDiff = Event->value.doubleDataTS[0].timeStamp - GenD.lastTimestamp[inode];
                                    GenD.lastTimestamp[inode] = Event->value.doubleDataTS[Event->count - 1].timeStamp;
                                    if (timestampDiff != GenD.expectedTimestampDiff[inode]) {
                                        snprintf(warning_in_thread,sizeof(warning_in_thread),"[WARN] event %d sampleloss detected between events from %s.\n", n, Event->path);
                                        GenD.eventsWithDataloss[inode]++;
                                        GenD.lastTimestamp[inode] = Event->value.doubleDataTS[Event->count - 1].timeStamp;
                                        break;
                                    }
                                }
                                // Check for sampleloss between polled events
                                for (i = 1; i < (int)Event->count; ++i) {
                                    uint64_t timestampDiff = Event->value.doubleDataTS[i].timeStamp - Event->value.doubleDataTS[i-1].timeStamp;
                                    if (timestampDiff != GenD.expectedTimestampDiff[inode]) {
                                        snprintf(warning_in_thread,sizeof(warning_in_thread),"[WARN] sampleloss detected within %s.\n",Event->path);
                                        GenD.eventsWithDataloss[inode]++;
                                        break;
                                    }
                                }
                            }
                        else snprintf(warning_in_thread,sizeof(warning_in_thread),"Node %s not recognized!",Event->path);
                        break;
                    }
                case ZI_VALUE_TYPE_INTEGER_DATA_TS:
                    // API Level >= 4 only: Integer data with a timestamp, e.g., a device setting such as /devX/
                    snprintf(warning_in_thread,sizeof(warning_in_thread),"[INFO] Poll returned integer data with timestamps from %s.\n",Event->path);
                    break;
                case ZI_VALUE_TYPE_NONE:
                    snprintf(warning_in_thread,sizeof(warning_in_thread),"[INFO] No event was polled (sample count is %d.\n",Event->count);
                    break;
                default:
                    snprintf(warning_in_thread,sizeof(warning_in_thread),"[WARN] The returned event has an unexpected ValueType..\n");
                    break;
                }
                if (fp) fclose(fp);
            }
    }
    return 0;
}


int mlfi_grab_signal_idle_action(O_p *op, DIALOG *d)
{
    int i,  k,  inode, transfert, it = 0, it1 = 0, bad_inode = 0, it10 = 0;
    pltreg *pr;
    d_s *ds;
    float tx, ty, mint, maxt;

    ZITimeStamp Zts, Zd, Zdc, Zts0 = 0, Ztf0 = 0;

    (void)d;
    (void)op;
    if(updating_menu_state != 0)	return D_O_K;
    if ((pr = find_pr_in_current_dialog(NULL)) == NULL)
        return win_printf_OK("cannot find pr");

    Zdc = Zd = GenD.clockbase;
    Zd = (Zd > 60000000) ? 60000000 : Zd;
    Zd *= scope_t_size;
    maxt = starting_timeStamp;
    mint = abs_last_timeStamp;
    for (inode = 0, transfert = 0; inode < DEMOD_COUNT + PID_COUNT; inode++)
        {
            if (GenD.dataTransferRate[inode] == 0) continue;
            Zts = GenD.lastTimestamp[inode];
            Zts = Zts - Zd;
            Zts = (Zts < starting_timeStamp) ? starting_timeStamp : Zts;
            if (GenD.ds_nb[inode] == 0) Zts0 = Zts;
            for (i = ai_pack_r - 1; (i > 0) && ((ai_pack_r - i + 64) < N_PACK) && (packet[i%N_PACK].timeStamp >= Zts); i--);

            it1 = i;
            if (GenD.ds_nb[inode] == 0)
                {
                    it10 = it1;
                    Ztf0 = packet[i%N_PACK].timeStamp;
                }
            i =  (i >= ai_pack_r - 256) ? ai_pack_r - 256 : i;
            i = (i < 0) ? 0 : i;
            it = i;

            ds = opm->dat[GenD.ds_nb[inode]];
            ds->nx = ds->ny = 0;
            for(k = 0 ; i < ai_pack_r && k < ds->mx; i++)
                {
                    if (packet[i%N_PACK].inode == inode)
                        {
                            ds->xd[k] = packet[i%N_PACK].timeStamp - starting_timeStamp;
                            mint = (ds->xd[k] < mint) ? ds->xd[k] : mint;
                            maxt = (ds->xd[k] > maxt) ? ds->xd[k] : maxt;
                            tx = packet[i%N_PACK].x;
                            ty = packet[i%N_PACK].y;
                            ds->yd[k] =  1000* sqrt(tx*tx+ty*ty);
                            packet[i%N_PACK].not_treated = 0;
                            k++;
                            transfert++;
                        }
                    else bad_inode++;
                }
            ds->nx = ds->ny = k;
        }
    if (opm != NULL)
        set_plot_title(opm, "\\stack{{[%d(%d), %d] signal points received)}"
                       "{\\pt7   %d points in window bad %d}"
                       "{\\pt7 start %I64d; last %I64d }"
                       "{\\pt7 zts %I64d Zd %I64d Cl %I64d}"
                       "{\\pt7 Ztf0 %I64d Zmag %g mm}"
                       "{\\pt7 ds 0 %d alloc %d %s}"
                       "{\\pt7 scope size %g; pts in ds 0 %d }}"
                       ,it,it10,ai_pack_r,transfert,bad_inode
                       ,starting_timeStamp,GenD.lastTimestamp[0],Zts0,Zd,Zdc
                       ,Ztf0,prev_zmag
                       ,GenD.ds_nb[0],opm->dat[GenD.ds_nb[0]]->mx
                       ,opm->dat[GenD.ds_nb[0]]->source
                       ,scope_t_size,opm->dat[0]->nx);

    tx = scope_t_size/10;
    ty = mint/GenD.clockbase;
    ty /= tx;
    ty = tx*(int)ty;
    opm->x_lo = ty * GenD.clockbase;
    ty = maxt/GenD.clockbase;
    ty /= tx;
    ty = tx*(1+(int)ty);
    opm->x_hi = ty * GenD.clockbase;
    opm->need_to_refresh = 1;
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}



int do_mlfi_hello(void)
{
    int i;
    ZIResult_enum retVal;
    ZIConnection conn;
    char* errBuffer;
    char serverAddress[128] = {0};

    if(updating_menu_state != 0)	return D_O_K;


    // Initialize ZIConnection.
    retVal = ziAPIInit(&conn);
    if (retVal != ZI_INFO_SUCCESS) {
        ziAPIGetError(retVal, &errBuffer, NULL);
        win_printf("Can't init Connection: %s\n", errBuffer);
        return 0;
    }


    // Connect to the Data Server: Use port 8005 for the HF2 Data Server, use
    // 8004 for the UHF and MF Data Servers. HF2 only support ZI_API_VERSION_1,
    // see the LabOne Programming Manual for an explanation of API Levels.
    //const char serverAddress[] = "localhost";
    i = win_scanf("Serveur name (may be localhost mf-dev3200 or 192.168.50.2):\n"
                  "%32s",serverAddress);
    if (i == WIN_CANCEL) return 0;
    retVal = ziAPIConnectEx(conn, serverAddress, 8004, ZI_API_VERSION_5, NULL);
    if (retVal != ZI_INFO_SUCCESS) {
        ziAPIGetError(retVal, &errBuffer, NULL);
        win_printf("Error, can't connect to the Data Server: `%s`.\n", errBuffer);
    } else {
        /*
          Do something using ZIConnection here.
        */
        win_printf("Do something using ZIConnection here.");
        // Since ZIAPIDisconnect always returns ZI_INFO_SUCCESS
        // no error handling is required.
        ziAPIDisconnect(conn);
    }

    // Since ZIAPIDestroy always returns ZI_INFO_SUCCESS
    // no error handling is required.
    ziAPIDestroy(conn);
    return 0;
}


bool isError(ZIResult_enum resultCode) {
    if (resultCode != ZI_INFO_SUCCESS) {
        char* message;
        ziAPIGetError(resultCode, &message, NULL);
        win_printf("Error: %s\n", message);
        return true;
    }
    return false;
}

void checkError(ZIResult_enum resultCode) {
    if (resultCode != ZI_INFO_SUCCESS) {
        char* message;
        ziAPIGetError(resultCode, &message, NULL);
        win_printf("%s",message);
    }
}

char *checkError_str(ZIResult_enum resultCode)
{
    if (resultCode != ZI_INFO_SUCCESS)
        {
            char* message;
            ziAPIGetError(resultCode, &message, NULL);
            return strdup(message);
        }
    else return NULL;
}



/// Create a Data Server session for the device and connect it on a physical interface (if not previously connected).
/** This function is a helper function to create an API session for the specified device on an appropriate Data
    Server. It uses Zurich Instruments Device Discovery to find the specified device on the local area network and
    determine which Data Server may be used to connect to it. The API Level used for the connection is the minimum Level
    supported by the device and by the input argument maxSupportedApilevel.

    @param[in]  conn                  The initialised ::ZIConnection which will be associated with the created API
    session.
    @param[in]  deviceAddress         The device address for which to create the API session, e.g., dev2006 or
    UHF-DEV2006 (as displayed on the back panel of the instrument).
    @param[in]  maxSupportedApilevel  A valid API Level (ZIAPIVersion_enum) that specifies the maximum API Level
    supported by the client code that will work with the API session.
    @param[out] deviceId              The device's ID as reported by ::ziAPIDiscoveryFind.
*/
int ziCreateAPISession(ZIConnection conn, char* deviceAddress, ZIAPIVersion_enum maxSupportedApilevel,
                       const char** deviceId) {
    if (!isError(ziAPIDiscoveryFind(conn, deviceAddress, deviceId))) {
        char message[1024];
        {
            const char *serveraddress;
            ZIIntegerData discoverable = 0;
            ZIIntegerData serverport = 0;
            ZIIntegerData apilevel;
            const char* connected;
            // First check that the device is discoverable on the network or another interface.
            checkError(ziAPIDiscoveryGetValueI(conn, *deviceId, "discoverable", &discoverable));
            if (discoverable != 1) {
                snprintf(message, sizeof(message), "`%s` is not discoverable.", *deviceId);
                win_printf("%s",message);
                return 1;
            } else {
                snprintf(message, sizeof(message), "Discovered device `%s`.", *deviceId);
                win_printf("%s",message);
            }
            // The device is discoverable - get the discovery properties required to
            // create a connnection via a Data Server.
            checkError(ziAPIDiscoveryGetValueS(conn, *deviceId, "serveraddress", &serveraddress));
            checkError(ziAPIDiscoveryGetValueI(conn, *deviceId, "serverport", &serverport));
            checkError(ziAPIDiscoveryGetValueI(conn, *deviceId, "apilevel", &apilevel));
            checkError(ziAPIDiscoveryGetValueS(conn, *deviceId, "connected", &connected));

            //ZIIntegerData apilevelConnection = apilevel;
            if (maxSupportedApilevel < apilevel) {
                //apilevelConnection = maxSupportedApilevel;
            }
            // Create an API Session to the Data Server reported by discovery.
            snprintf(message, sizeof(message), "Creating an API Session with the Data Server running on %s on port %I64d with API Level %I64d", serveraddress, serverport, apilevel);
            win_printf("%s",message);
            //checkError(ziAPIConnectEx(conn, serveraddress, serverport,
            //                        static_cast<ZIAPIVersion_enum>(apilevelConnection), NULL));

            // Try to connect the device to the Data Server if not already.
            if (strcmp(connected, "\0")) {
                snprintf(message, sizeof(message), "Device is already connected on interface `%s`.", connected);
                ziAPIWriteDebugLog(0, message);
            } else {
                const char* interfaces;
                checkError(ziAPIDiscoveryGetValueS(conn, *deviceId, "interfaces", &interfaces));
                snprintf(message, sizeof(message), "Device is not connected, available interfaces: `%s`.", interfaces);
                win_printf("%s",message);
                // Get the first available interface.
                char* saveptr;
                char* interfaceConnection = strtok_r((const char*)interfaces, "\n", &saveptr);
                if (strcmp(interfaceConnection, "\0") == 0) {
                    // This should not happen if the device is discoverable.
                    snprintf(message, sizeof(message),
                             "Error: The device `%s` is not connected but could not read an available interface.\n", *deviceId);
                    win_printf("%s",message);
                    return 1;
                }
                snprintf(message, sizeof(message), "Will try to connect on: `%s`.\n", interfaceConnection);
                win_printf("%s",message);
                checkError(ziAPIConnectDevice(conn, *deviceId, interfaceConnection, NULL));
            }
            return 0;
        }
    } else {
        return 1;
    }

}




int do_mlfi_read_AuxIn(void)
{
    int i;
    ZIResult_enum retVal;
    ZIConnection conn;
    char* errBuffer;
    char serverAddress[128] = {0};
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds;


    if(updating_menu_state != 0)	return D_O_K;


    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
        return win_printf_OK("cannot find data");

    if ((op = create_and_attach_one_plot(pr, ROLLING_BUFFER_SIZE, ROLLING_BUFFER_SIZE, 0)) == NULL)
        return win_printf_OK("cannot create plot !");
    op->dat[0]->nx =   op->dat[0]->ny = 0;
    for (i = 1; i < DEMOD_COUNT + PID_COUNT; i++)
        {
            if ((ds = create_and_attach_one_ds(op, 16, 16, 0)) == NULL)
                return win_printf_OK("cannot create plot !");
            ds->nx = ds->ny = 0;
        }
    refresh_plot(pr, pr->n_op-1);
    // Initialize ZIConnection.
    retVal = ziAPIInit(&conn);
    if (retVal != ZI_INFO_SUCCESS) {
        ziAPIGetError(retVal, &errBuffer, NULL);
        win_printf("Can't init Connection: %s\n", errBuffer);
        return 0;
    }




    // Connect to the Data Server: Use port 8005 for the HF2 Data Server, use
    // 8004 for the UHF and MF Data Servers. HF2 only support ZI_API_VERSION_1,
    // see the LabOne Programming Manual for an explanation of API Levels.
    //const char serverAddress[] = "localhost";
    snprintf(serverAddress,sizeof(serverAddress),"192.168.53.70");
    i = win_scanf("Serveur name (may be localhost mf-dev3409 or 192.168.53.70):\n"
                  "%32s",serverAddress);
    if (i == WIN_CANCEL) return 0;
    retVal = ziAPIConnectEx(conn, serverAddress, 8004, ZI_API_VERSION_5, NULL);
    if (retVal != ZI_INFO_SUCCESS) {
        ziAPIGetError(retVal, &errBuffer, NULL);
        win_printf("Error, can't connect to the Data Server: `%s`.\n", errBuffer);
    } else {
        char deviceAddress[] = "dev3409";
        const char *deviceId;
        if (ziCreateAPISession(conn, deviceAddress, ZI_API_VERSION_5, &deviceId) != 0)
            return  win_printf("Error, can't create APISession\n");
        win_printf("DeviceId = %s\n",deviceId);

        ZIAuxInSample AuxInSamplen[3];
        char nodePath[1024];
        ZIResult_enum RetVal;
        char* ErrBuffer;

        // Get the instrument's clockbase to convert timestamps (in ticks) to seconds
        snprintf(nodePath, sizeof(nodePath), "/%s/auxins/0/sample", deviceId);
        for (i = 0; i < 3; i++)
            RetVal = ziAPIGetAuxInSample(conn, nodePath, AuxInSamplen + i);

        if (RetVal!= ZI_INFO_SUCCESS) {
            ziAPIGetError(RetVal, &ErrBuffer, NULL);
            win_printf("Error, can't get Parameter: %s\n", ErrBuffer);
        } else {
            win_printf("0-> TimeStamp = %I64d, ch0=%f, ch1=%f\n"
                       "1-> TimeStamp = %I64d, ch0=%f, ch1=%f\n"
                       "2-> TimeStamp = %I64d, ch0=%f, ch1=%f\n",
                       AuxInSamplen[0].timeStamp, AuxInSamplen[0].ch0, AuxInSamplen[0].ch1,
                       AuxInSamplen[1].timeStamp, AuxInSamplen[1].ch0, AuxInSamplen[1].ch1,
                       AuxInSamplen[2].timeStamp, AuxInSamplen[2].ch0, AuxInSamplen[2].ch1);
        }
    }
    return 0;
}



int do_mlfi_test(void)
{
    int i;
    ZIResult_enum retVal;
    ZIConnection conn;
    char* errBuffer;
    char serverAddress[128] = {0};
    char node_name[16][128] = {0};

    uint64_t lastTimestamp[16];
    uint64_t totalSampleCount[16];
    double dataTransferRate[16];
    uint64_t expectedTimestampDiff[16];
    uint64_t eventsWithDataloss[16];

    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds;


    if(updating_menu_state != 0)	return D_O_K;


    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
        return win_printf_OK("cannot find data");

    if ((op = create_and_attach_one_plot(pr, ROLLING_BUFFER_SIZE, ROLLING_BUFFER_SIZE, 0)) == NULL)
        return win_printf_OK("cannot create plot !");
    op->dat[0]->nx =   op->dat[0]->ny = 0;
    for (i = 1; i < DEMOD_COUNT + PID_COUNT; i++)
        {
            if ((ds = create_and_attach_one_ds(op, 16, 16, 0)) == NULL)
                return win_printf_OK("cannot create plot !");
            ds->nx = ds->ny = 0;
        }
    refresh_plot(pr, pr->n_op-1);
    // Initialize ZIConnection.
    retVal = ziAPIInit(&conn);
    if (retVal != ZI_INFO_SUCCESS) {
        ziAPIGetError(retVal, &errBuffer, NULL);
        win_printf("Can't init Connection: %s\n", errBuffer);
        return 0;
    }


    // Connect to the Data Server: Use port 8005 for the HF2 Data Server, use
    // 8004 for the UHF and MF Data Servers. HF2 only support ZI_API_VERSION_1,
    // see the LabOne Programming Manual for an explanation of API Levels.
    //const char serverAddress[] = "localhost";
    snprintf(serverAddress,sizeof(serverAddress),"192.168.53.70");
    i = win_scanf("Serveur name (may be localhost mf-dev3409 or 192.168.53.70):\n"
                  "%32s",serverAddress);
    if (i == WIN_CANCEL) return 0;
    retVal = ziAPIConnectEx(conn, serverAddress, 8004, ZI_API_VERSION_5, NULL);
    if (retVal != ZI_INFO_SUCCESS) {
        ziAPIGetError(retVal, &errBuffer, NULL);
        win_printf("Error, can't connect to the Data Server: `%s`.\n", errBuffer);
    } else {
        char deviceAddress[] = "dev3409";
        const char *deviceId;
        if (ziCreateAPISession(conn, deviceAddress, ZI_API_VERSION_5, &deviceId) != 0)
            return  win_printf("Error, can't create APISession\n");
        win_printf("DeviceId = %s\n",deviceId);


        char nodePath[1024];

        // Get the instrument's clockbase to convert timestamps (in ticks) to seconds
        snprintf(nodePath, sizeof(nodePath), "/%s/clockbase", deviceId);
        ZIIntegerData clockbase;
        checkError(ziAPIGetValueI(conn, nodePath, &clockbase));

        win_printf("Clock is set to %I64d \n",clockbase);

        for (i=0; i < DEMOD_COUNT; i++)
            {
                snprintf(node_name[i], sizeof(node_name[i]), "/%s/demods/%u/sample", deviceId,i);
                checkError(ziAPISubscribe(conn, node_name[i]));
                win_printf("nodePath=\n%s",node_name[i]);
                lastTimestamp[i] = 0;
                totalSampleCount[i] = 0;
                eventsWithDataloss[i] = 0;

                ZIDoubleData rate;
                snprintf(nodePath, sizeof(nodePath), "/%s/demods/%d/rate", deviceId, i);
                checkError(ziAPIGetValueD(conn, nodePath, &rate));
                dataTransferRate[i] = rate;

                expectedTimestampDiff[i] = (rate > 0) ? ((double)clockbase/rate + 0.5) : 0;
                win_printf("Rate = %g, expectedTimestampDiff %I64d",rate,expectedTimestampDiff[i]);
            }
        /*
          win_printf("Device 0: %s\nRate = %g, expectedTimestampDiff %ld\n"
          "Device 1: %s\nRate = %g, expectedTimestampDiff %ld\n"
          "Device 2: %s\nRate = %g, expectedTimestampDiff %ld\n"
          "Device 3: %s\nRate = %g, expectedTimestampDiff %ld\n"
          ,node_name[0],dataTransferRate[0],expectedTimestampDiff[0]
          ,node_name[1],dataTransferRate[1],expectedTimestampDiff[1]
          ,node_name[2],dataTransferRate[2],expectedTimestampDiff[2]
          ,node_name[3],dataTransferRate[3],expectedTimestampDiff[3]
          );
        */
        for (i=0; i < PID_COUNT; i++)
            {
                snprintf(node_name[DEMOD_COUNT+i], sizeof(node_name[DEMOD_COUNT+i]), "/%s/pids/%u/stream/value", deviceId, i);
                checkError(ziAPISubscribe(conn, node_name[DEMOD_COUNT+i]));
                lastTimestamp[DEMOD_COUNT+i] = 0;
                totalSampleCount[DEMOD_COUNT+i] = 0;
                eventsWithDataloss[DEMOD_COUNT+i] = 0;

                ZIDoubleData pidStreamRate;
                snprintf(nodePath, sizeof(nodePath), "/%s/pids/%d/stream/effectiverate", deviceId, i);
                checkError(ziAPIGetValueD(conn, nodePath, &pidStreamRate));
                dataTransferRate[DEMOD_COUNT+i] = pidStreamRate;
                //win_printf("pidStreamRate = %g",pidStreamRate);
                expectedTimestampDiff[DEMOD_COUNT+i] = (pidStreamRate > 0) ? ((double)clockbase/pidStreamRate + 0.5) : 0;
                win_printf("[INFO] %s is using rate %g expected timestamp delta: %I64d",node_name[DEMOD_COUNT+i]
                           ,dataTransferRate[DEMOD_COUNT+i],expectedTimestampDiff[DEMOD_COUNT+i]);

            }



        ZIEvent* Event;
        /* Allocate ZIEvent in heap memory instead of getting it from stack will secure against stack overflows
           especially in windows.
        */
        if ((Event = ziAPIAllocateEventEx()) == NULL){
            win_printf("[ERROR] Can't allocate memory for Event.\n");
        }



        int n = 0, inode = 0;
        float tx, ty;
        time_t t_start = time(NULL);
        double poll_duration = 200.0, tS, tS0 = 0;  // seconds
        while (difftime(time(NULL), t_start) < poll_duration) {
            n++;
            // Poll for data with timeout.
            checkError(ziAPIPollDataEx(conn, Event, 1000));
            // if (Event->count > 0) {
            //   std::cout << "[INFO] Event contains " <<  Event->count << " samples from " << Event->path << ".\n";
            // }
            switch (Event->valueType) {
            case ZI_VALUE_TYPE_DEMOD_SAMPLE:
                {
                    for (inode=0; inode < DEMOD_COUNT + PID_COUNT; inode++)
                        if (stricmp(node_name[inode],(const char*)Event->path) == 0) break;
                    if (inode < DEMOD_COUNT + PID_COUNT)
                        {
                            // Check for sampleloss between polled events.
                            if (lastTimestamp[inode] != 0) {
                                uint64_t timestampDiff = Event->value.demodSample[0].timeStamp - lastTimestamp[inode];
                                lastTimestamp[inode] = Event->value.demodSample[Event->count - 1].timeStamp;
                                if (timestampDiff != expectedTimestampDiff[inode]) {
                                    win_printf("[WARN] event %d sampleloss detected between events from %s.\n",n,Event->path);
                                    eventsWithDataloss[inode]++;
                                    lastTimestamp[inode] = Event->value.demodSample[Event->count - 1].timeStamp;
                                    break;
                                }
                            }
                            // Check for sampleloss between all samples within the received event
                            for (i = 1; i < (int)Event->count; ++i) {
                                uint64_t timestampDiff = Event->value.demodSample[i].timeStamp - Event->value.demodSample[i-1].timeStamp;
                                if (timestampDiff != expectedTimestampDiff[inode]) {
                                    win_printf("[WARN] sampleloss detected within %s.\n",Event->path);
                                    eventsWithDataloss[inode]++;
                                    break;
                                }
                            }
                            ds = op->dat[inode];
                            if (inode == 0)
                                {
                                    if (ds->nx + Event->count >= ROLLING_BUFFER_SIZE)
                                        {
                                            for (i = 0; i < (int)(ROLLING_BUFFER_SIZE - Event->count); i++)
                                                {
                                                    ds->xd[i] = ds->xd[i + Event->count];
                                                    ds->yd[i] = ds->yd[i + Event->count];
                                                }
                                            for (i = 0; i < (int)Event->count; i++)
                                                {
                                                    tS = (double)Event->value.demodSample[i].timeStamp/clockbase;
                                                    ds->xd[ROLLING_BUFFER_SIZE - Event->count + i] = tS - tS0;
                                                    tx = Event->value.demodSample[i].x;
                                                    ty = Event->value.demodSample[i].y;
                                                    ds->yd[ROLLING_BUFFER_SIZE - Event->count + i] =  1000* sqrt(tx*tx+ty*ty);
                                                }
                                            ds->nx = ds->ny = ROLLING_BUFFER_SIZE;
                                        }
                                    else
                                        {
                                            for (i = 0; i < (int)Event->count; i++)
                                                {
                                                    tS = (double)Event->value.demodSample[i].timeStamp/clockbase;
                                                    if (tS0 == 0) tS0 = tS;
                                                    ds->xd[ds->nx] = tS - tS0;
                                                    tx = Event->value.demodSample[i].x;
                                                    ty = Event->value.demodSample[i].y;
                                                    ds->yd[ds->nx] =  1000* sqrt(tx*tx+ty*ty);
                                                    ds->nx = (ds->nx < ROLLING_BUFFER_SIZE) ? ds->nx+1 : ROLLING_BUFFER_SIZE;
                                                }
                                        }
                                    op->need_to_refresh = 1;
                                    refresh_plot(pr, pr->n_op-1);
                                }
                            else
                                {
                                    for (i = 0; i < (int)Event->count; i++)
                                        {
                                            add_new_point_to_ds(ds, Event->value.demodSample[i].x, Event->value.demodSample[i].y);
                                        }
                                }
                            totalSampleCount[inode] += Event->count;

                        }
                    else win_printf("Node %s not recognized!",Event->path);
                    // The fields of the demodulator sample can be accessed as following.
                    // for (size_t i = 0; i < 1; ++i) {
                    //   printf("[INFO]    - sample %" PRsize_t "d TS = %" PRsize_t "d, X = %e, Y = %e, auxin0 = %e, auxin1 = %e, bits = %d\n",
                    //          i, Event->value.demodSample[i].timeStamp, Event->value.demodSample[i].x, Event->value.demodSample[i].y,
                    //          Event->value.demodSample[i].auxIn0, Event->value.demodSample[i].auxIn1, Event->value.demodSample[i].dioBits);
                    // }
                }
                break;
            case ZI_VALUE_TYPE_DOUBLE_DATA:
                win_printf("[INFO] Poll returned double data from %s.\n",Event->path);
                break;
            case ZI_VALUE_TYPE_INTEGER_DATA:
                win_printf("[INFO] Poll returned integer data from %s.\n",Event->path);
                break;
            case ZI_VALUE_TYPE_DOUBLE_DATA_TS:
                {
                    // API Level >= 4 only: Double data with a timestamp, e.g., /devX/pids/0/stream/value
                    // std::cout << "[INFO] Poll returned double data with timestamps from " << Event->path << ".\n";
                    for (inode=0; inode < DEMOD_COUNT + PID_COUNT; inode++)
                        if (stricmp(node_name[inode],(const char*)Event->path) == 0) break;
                    if (inode < DEMOD_COUNT + PID_COUNT)
                        {
                            totalSampleCount[inode] += Event->count;

                            if ((expectedTimestampDiff[inode] == 0) && (Event->count > 1)) {
                                // Due to #9093 (see above), we use the first measured timestamp delta as the reference for all subsequent
                                // samples.
                                expectedTimestampDiff[inode] = Event->value.doubleDataTS[1].timeStamp - Event->value.doubleDataTS[0].timeStamp;
                                dataTransferRate[inode] = (double)clockbase/expectedTimestampDiff[inode];
                                win_printf("[INFO] %s  measured rate %g ,measured timestamp delta: %I64d",Event->path
                                           ,dataTransferRate[inode],expectedTimestampDiff[inode]);
                            }
                            // Check for sampleloss between polled events
                            if (lastTimestamp[inode] != 0) {
                                uint64_t timestampDiff = Event->value.doubleDataTS[0].timeStamp - lastTimestamp[inode];
                                lastTimestamp[inode] = Event->value.doubleDataTS[Event->count - 1].timeStamp;
                                if (timestampDiff != expectedTimestampDiff[inode]) {
                                    win_printf("[WARN] event %d sampleloss detected between events from %s.\n", n, Event->path);
                                    eventsWithDataloss[inode]++;
                                    lastTimestamp[inode] = Event->value.doubleDataTS[Event->count - 1].timeStamp;
                                    break;
                                }
                            }
                            // Check for sampleloss between polled events
                            for (i = 1; i < (int)Event->count; ++i) {
                                uint64_t timestampDiff = Event->value.doubleDataTS[i].timeStamp - Event->value.doubleDataTS[i-1].timeStamp;
                                if (timestampDiff != expectedTimestampDiff[inode]) {
                                    win_printf("[WARN] sampleloss detected within %s.\n",Event->path);
                                    eventsWithDataloss[inode]++;
                                    break;
                                }
                            }
                        }
                    else win_printf("Node %s not recognized!",Event->path);
                    break;
                }
            case ZI_VALUE_TYPE_INTEGER_DATA_TS:
                // API Level >= 4 only: Integer data with a timestamp, e.g., a device setting such as /devX/
                win_printf("[INFO] Poll returned integer data with timestamps from %s.\n",Event->path);
                break;
            case ZI_VALUE_TYPE_NONE:
                win_printf("[INFO] No event was polled (sample count is %d.\n",Event->count);
                break;
            default:
                win_printf("[WARN] The returned event has an unexpected ValueType..\n");
                break;
            }
        }


        for (inode=0; inode < DEMOD_COUNT + PID_COUNT; inode++)
            {
                //if (totalSampleCount[inode] > 0)
                win_printf("%d events\nfor Node %s, total sample count %I64d",n,node_name[inode],totalSampleCount[inode]);
            }


        /*
          Do something using ZIConnection here.
        */
        win_printf("Do something using ZIConnection here.");
        // Since ZIAPIDisconnect always returns ZI_INFO_SUCCESS
        // no error handling is required.
        ziAPIDisconnect(conn);

    }

    // Since ZIAPIDestroy always returns ZI_INFO_SUCCESS
    // no error handling is required.
    ziAPIDestroy(conn);
    refresh_plot(pr, pr->n_op-1);
    return 0;
}


int do_inc_timelapse(void)
{
    pltreg *pr;
    O_p *op;
    d_s *ds;

    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
        return D_O_K;

    //if (strncmp(op->dat[0]->source,"M = ",4)) return D_O_K;
    if(updating_menu_state != 0)
        {
            if (op != NULL && (op->user_id == MLFI_ID))
                {
                    remove_short_cut_from_dialog(0, KEY_PGUP);
                    add_keyboard_short_cut(0, KEY_PGUP, 0, do_inc_timelapse);
                }
            return D_O_K;
        }
    if (scope_t_size >= 5000)
        {
            scope_t_size = 5000;
            return 0;
        }
    else if (fabs(scope_t_size) < 0.02)
        {
            scope_t_size = 0.01;
            return 0;
        }
    else if (fabs(scope_t_size - 2000) < 0.1)  scope_t_size = 5000;
    else if (fabs(scope_t_size - 1000) < 0.1)  scope_t_size = 2000;
    else if (fabs(scope_t_size - 500) < 0.1)  scope_t_size = 1000;
    else if (fabs(scope_t_size - 200) < 0.1)  scope_t_size = 500;
    else if (fabs(scope_t_size - 100) < 0.1)  scope_t_size = 200;
    else if (fabs(scope_t_size - 50) < 0.1)  scope_t_size = 100;
    else if (fabs(scope_t_size - 20) < 0.1)  scope_t_size = 50;
    else if (fabs(scope_t_size - 10) < 0.1)  scope_t_size = 20;
    else if (fabs(scope_t_size - 5) < 0.1)  scope_t_size = 10;
    else if (fabs(scope_t_size - 2) < 0.1)  scope_t_size = 5;
    else if (fabs(scope_t_size - 1) < 0.01)  scope_t_size = 2;
    else if (fabs(scope_t_size - .5) < 0.01)  scope_t_size = 1;
    else if (fabs(scope_t_size - .2) < 0.01)  scope_t_size = .5;
    else if (fabs(scope_t_size - .1) < 0.01)  scope_t_size = .2;
    else if (fabs(scope_t_size - .05) < 0.001)  scope_t_size = .1;
    else if (fabs(scope_t_size - .02) < 0.01)  scope_t_size = .05;
    else if (fabs(scope_t_size - .01) < 0.001)  scope_t_size = .02;
    op->need_to_refresh = 1;
    /* refisplay the entire plot */
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}


int do_dec_timelapse(void)
{
    pltreg *pr;
    O_p *op;
    d_s *ds;

    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
        return D_O_K;

    //if (strncmp(op->dat[0]->source,"M = ",4)) return D_O_K;
    if(updating_menu_state != 0)
        {
            if (op != NULL && (op->user_id == MLFI_ID))
                {
                    remove_short_cut_from_dialog(0, KEY_PGDN);
                    add_keyboard_short_cut(0, KEY_PGDN, 0, do_dec_timelapse);
                }
            return D_O_K;
        }
    if (fabs(scope_t_size - 0.01) < 0.001)
        {
            scope_t_size = 0.01;
            return 0;
        }
    else if (scope_t_size >= 5000)
        {
            scope_t_size = 5000;
            return 0;
        }
    else if (fabs(scope_t_size - 5000) < 1)  scope_t_size = 2000;
    else if (fabs(scope_t_size - 2000) < 1) scope_t_size = 1000;
    else if (fabs(scope_t_size - 1000) < 1)  scope_t_size = 500;
    else if (fabs(scope_t_size - 500)  < 1) scope_t_size = 200;
    else if (fabs(scope_t_size - 200) < 1) scope_t_size = 100;
    else if (fabs(scope_t_size - 100) < 1)  scope_t_size = 50;
    else if (fabs(scope_t_size - 50) < 1) scope_t_size = 20;
    else if (fabs(scope_t_size - 20) < 1) scope_t_size = 10;
    else if (fabs(scope_t_size - 10) < 1)  scope_t_size = 5;
    else if (fabs(scope_t_size - 5)  < 1) scope_t_size = 2;
    else if (fabs(scope_t_size - 2)  < 1) scope_t_size = 1;
    else if (fabs(scope_t_size - 1)  < 0.1) scope_t_size = .5;
    else if (fabs(scope_t_size - .5)  < 0.1) scope_t_size = .2;
    else if (fabs(scope_t_size - .2)  < 0.01) scope_t_size = .1;
    else if (fabs(scope_t_size - .1) < 0.01)  scope_t_size = .05;
    else if (fabs(scope_t_size - .05) < 0.01)  scope_t_size = .02;
    else if (scope_t_size <= .02)  scope_t_size = .01;
    op->need_to_refresh = 1;
    /* refisplay the entire plot */
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}


int select_dump_data_packet(void)
{
    FILE *fp = NULL;

    if(updating_menu_state != 0)	return D_O_K;

    if (do_select_file(mlfidatafilename, sizeof(mlfidatafilename), "TXT-FILE", "*.csv", "File to save data", 0))
        return win_printf_OK("Cannot select output file");
    fp = fopen (mlfidatafilename,"w");
    if (fp == NULL)
        return win_printf_OK("Cannot open file:\n%s",backslash_to_slash(mlfidatafilename));
    fprintf(fp, "Inode,\t");
    fprintf(fp, "timeStamp,\t");
    fprintf(fp, "X,\t");
    fprintf(fp, "Y,\t");
    fprintf(fp, "Frequency,\t");
    fprintf(fp, "Phase,\t");
    fprintf(fp, "dioBits,\t");
    fprintf(fp, "Trigger,\t");
    fprintf(fp, "AuxIn0,\t");
    fprintf(fp, "AuxIn1,\t");
    fprintf(fp, "PCtimestamp,\t");
    fprintf(fp, "Zmag \n");
    fclose(fp);
    return 0;
}


O_p *prepare_mlfi_thread(pltreg *pr)
{
    int i;
    ZIResult_enum retVal;
    char* errBuffer;
    d_s *ds;

    // Initialize ZIConnection.
    retVal = ziAPIInit(&GenD.conn);
    if (retVal != ZI_INFO_SUCCESS) {
        ziAPIGetError(retVal, &errBuffer, NULL);
        win_printf("Can't init Connection: %s\n", errBuffer);
        return 0;
    }


    // Connect to the Data Server: Use port 8005 for the HF2 Data Server, use
    // 8004 for the UHF and MF Data Servers. HF2 only support ZI_API_VERSION_1,
    // see the LabOne Programming Manual for an explanation of API Levels.
    //const char serverAddress[] = "localhost";
    snprintf(GenD.serverAddress,sizeof(GenD.serverAddress),"192.168.53.70");
    i = win_scanf("Serveur name (may be localhost mf-dev3409 or 192.168.53.70):\n"
                  "%32s",GenD.serverAddress);
    if (i == WIN_CANCEL) return 0;
    retVal = ziAPIConnectEx(GenD.conn, GenD.serverAddress, 8004, ZI_API_VERSION_5, NULL);
    if (retVal != ZI_INFO_SUCCESS) {
        ziAPIGetError(retVal, &errBuffer, NULL);
        win_printf("Error, can't connect to the Data Server: `%s`.\n", errBuffer);
    } else {
        char deviceAddress[] = "dev3409";
        if (ziCreateAPISession(GenD.conn, deviceAddress, ZI_API_VERSION_5, &GenD.deviceId) != 0)
            return  win_printf_ptr("Error, can't create APISession\n");
        //win_printf("DeviceId = %s\n",deviceId);


        char nodePath[1024];

        // Get the instrument's clockbase to convert timestamps (in ticks) to seconds
        snprintf(nodePath, sizeof(nodePath), "/%s/clockbase", GenD.deviceId);

        checkError(ziAPIGetValueI(GenD.conn, nodePath, &(GenD.clockbase)));

        //win_printf("Clock is set to %ld \n",GenD.clockbase);



        if ((opm = create_and_attach_one_plot(pr, ROLLING_BUFFER_SIZE, ROLLING_BUFFER_SIZE, 0)) == NULL)
            return win_printf_ptr("cannot create plot !");
        opm->dat[0]->nx =   opm->dat[0]->ny = 0;
        opm->user_id = MLFI_ID;
        ds = opm->dat[0];
        create_attach_select_x_un_to_op(opm, IS_SECOND, 0, (float)1/GenD.clockbase, 0, 0, "s");
        refresh_plot(pr, pr->n_op-1);


        for (i=0; i < DEMOD_COUNT; i++)
            {
                ZIDoubleData rate;
                snprintf(GenD.node_name[i], sizeof(GenD.node_name[i]), "/%s/demods/%u/sample", GenD.deviceId,i);
                if (ziAPISubscribe(GenD.conn, GenD.node_name[i]) == ZI_INFO_SUCCESS)
                    {
                        GenD.lastTimestamp[i] = 0;
                        GenD.totalSampleCount[i] = 0;
                        GenD.eventsWithDataloss[i] = 0;
                        snprintf(nodePath, sizeof(nodePath), "/%s/demods/%d/rate", GenD.deviceId, i);
                        if (ziAPIGetValueD(GenD.conn, nodePath, &rate)  == ZI_INFO_SUCCESS)
                            GenD.dataTransferRate[i] = rate;
                        else rate = 0;
                        GenD.expectedTimestampDiff[i] = (rate > 0) ? ((double)GenD.clockbase/rate + 0.5) : 0;
                        if (ds == NULL)
                            {
                                if ((ds = create_and_attach_one_ds(opm, ROLLING_BUFFER_SIZE, ROLLING_BUFFER_SIZE, 0)) == NULL)
                                    return win_printf_ptr("cannot create plot !");
                            }
                        ds->nx = ds->ny = 0;
                        GenD.ds_nb[i] = (opm->n_dat > 0) ? opm->n_dat-1 : 0;
                        set_ds_source(ds,"%s",GenD.node_name[i]);
                        ds = NULL;
                    }
            }
        for (i=0; i < PID_COUNT; i++)
            {
                ZIDoubleData pidStreamRate;
                snprintf(GenD.node_name[DEMOD_COUNT+i], sizeof(GenD.node_name[DEMOD_COUNT+i]), "/%s/pids/%u/stream/value", GenD.deviceId, i);
                if (ziAPISubscribe(GenD.conn, GenD.node_name[DEMOD_COUNT+i]) == ZI_INFO_SUCCESS)
                    {
                        GenD.lastTimestamp[DEMOD_COUNT+i] = 0;
                        GenD.totalSampleCount[DEMOD_COUNT+i] = 0;
                        GenD.eventsWithDataloss[DEMOD_COUNT+i] = 0;
                        snprintf(nodePath, sizeof(nodePath), "/%s/pids/%d/stream/effectiverate", GenD.deviceId, i);
                        if (ziAPIGetValueD(GenD.conn, nodePath, &pidStreamRate) == ZI_INFO_SUCCESS)
                            GenD.dataTransferRate[DEMOD_COUNT+i] = pidStreamRate;
                        else pidStreamRate = 0;
                        GenD.expectedTimestampDiff[DEMOD_COUNT+i] = (pidStreamRate > 0) ? ((double)GenD.clockbase/pidStreamRate + 0.5) : 0;
                        if (ds == NULL)
                            {
                                if ((ds = create_and_attach_one_ds(opm, ROLLING_BUFFER_SIZE, ROLLING_BUFFER_SIZE, 0)) == NULL)
                                    return win_printf_ptr("cannot create plot !");
                            }
                        ds->nx = ds->ny = 0;
                        GenD.ds_nb[DEMOD_COUNT+i] = (opm->n_dat > 0) ? opm->n_dat-1 : 0;
                        set_ds_source(ds,"%s",GenD.node_name[DEMOD_COUNT+i]);
                        ds = NULL;

                    }
            }
        select_dump_data_packet();
        //win_printf_OK("starting MLFI grabbing thread!");
        if (pthread_create (&thread_mlfi, NULL, grab_mlfi_data, NULL))
            win_printf_OK("Could not start MLFI grabbing thread!");
        opm->op_idle_action = mlfi_grab_signal_idle_action;
    }

    // Since ZIAPIDestroy always returns ZI_INFO_SUCCESS
    // no error handling is required.
    //  ziAPIDestroy(conn);
    return opm;
}
int do_prepare_mlfi_thread(void)
{
    pltreg *pr = NULL;

    if(updating_menu_state != 0)	return D_O_K;

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
        return win_printf_OK("cannot find data");
    prepare_mlfi_thread(pr);
    refresh_plot(pr, pr->n_op-1);
    return 0;
}

int switch_beadz_to_mfi(void)
{
    int i, mfibd = beadnb_w_z_switched_to_mfi;

    if(updating_menu_state != 0)	return D_O_K;
    i = win_scanf("Indicate the bead number for witch you will switch Z to the MLFI signal\n"
                  "(set to -1 to stop) Bead number = %4d\n",&mfibd);
    if (i == WIN_CANCEL) return 0;
    /* we first find the data that we need to transform */
    beadnb_w_z_switched_to_mfi = mfibd;
    return 0;
}


MENU *mlfi_plot_menu(void)
{
    static MENU mn[32];

    if (mn[0].text != NULL)	return mn;
    add_item_to_menu(mn,"Hello example", do_mlfi_hello,NULL,0,NULL);
    add_item_to_menu(mn,"test example", do_mlfi_test,NULL,0,NULL);
    add_item_to_menu(mn,"Read Aux Ins", do_mlfi_read_AuxIn,NULL,0,NULL);
    add_item_to_menu(mn,"Start data grabbing", do_prepare_mlfi_thread,NULL,0,NULL);
    add_item_to_menu(mn,"Increment time window (PgUp)",do_inc_timelapse ,NULL,0,NULL);
    add_item_to_menu(mn,"Decrement time window (PgDwn)",do_dec_timelapse ,NULL,0,NULL);
    add_item_to_menu(mn,"Replace a bead Z by MLFI signal",switch_beadz_to_mfi,NULL,0,NULL);

    return mn;
}

int	mlfi_main(int argc, char **argv)
{
    (void)argc;
    (void)argv;
    add_plot_treat_menu_item ( "mlfi", NULL, mlfi_plot_menu(), 0, NULL);
    return D_O_K;
}

int	mlfi_unload(int argc, char **argv)
{
    (void)argc;
    (void)argv;
    remove_item_to_menu(plot_treat_menu, "mlfi", NULL, NULL);
    return D_O_K;
}
#endif
