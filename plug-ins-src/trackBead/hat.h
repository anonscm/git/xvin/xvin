
#ifndef _HAT_H_
#define _HAT_H_


typedef struct _hat_param
{
  int nf;
  int nstep;
  int dead;             // define the time spent during rotation
  int setling;          // define the time skipped after rotation to allow bead to settle
  int nper;
  int *im;
  int *grim;            // the general record image index
  int one_way;
  float rot_start, rot_step, zmag, force, z_cor, zmag_finish, zmag_rotate;
  float *rot;
  int c_step;
  int fir;              // the nb of points for averaging
} hat_param;



# ifndef _HAT_C_

//PXV_VAR(char, sample[]);
PXV_VAR(int, n_hat);
PXV_FUNC(MENU *, hat_menu,(void));
PXV_FUNC(MENU *, hat_plot_menu,(void));

# else

//char sample[] = "test"; // _PXV_DLL
int n_hat = 0;// _PXV_DLL

# endif

# endif
