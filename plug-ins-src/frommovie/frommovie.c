/*
*    Plug-in program for image treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _FROMMOVIE_C_
#define _FROMMOVIE_C_

# include "allegro.h"
# include "xvin.h"
# include "fftl32n.h"
# include "fillib.h"


# define 	WRONG_ARGUMENT		-1
# define 	OUT_MEMORY			-2
# define 	FFT_NOT_SUPPORTED	-15
# define	PB_WITH_EXTREMUM	2
# define	TOO_MUCH_NOISE		4
# define	GETTING_REF_PROFILE	1

/* If you include other plug-ins header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "frommovie.h"

int flat_end = 80;
int bp_center = 40, bp_width = 36, rc = 40;


int coarse_find_z_by_least_square_min_2(float *yt, int nxp, O_i *ois, float *min, int rc)
{
	register int i, j, k;
	int nx, ny, nt = 1;
	union pix *ps;
	float  tmp, *yr, val;
	
	if (ois->im.data_type != IS_COMPLEX_IMAGE)
		return win_printf_OK("image must be complex!");
	ny = ois->im.ny;	nx = ois->im.nx;
	if (2*nx != nxp)			
	  return win_printf_OK("image and profile have not the right number of points!");;
	ps = ois->im.pixel;
	for (i = k = 0; i< ny ; i++)
	{
		yr = ps[i].fl;
		for (j = nt = 0, val = 0; j < nx - rc; j++) 
		{
			tmp = yr[j] -  yt[j];
			val += tmp * tmp;
			nt++;
		}
		for (j = nx + rc; j < nxp; j++) 
		{
			tmp = yr[j] -  yt[j];
			val += tmp * tmp;
			nt++;
		}
		if (i == 0 || val < *min)
		{
			k = i;
			*min = val;
		}
	}
	*min /= nt;
	return k;
}

float find_zero_of_3_points_polynome(float y_1, float y0, float y1)
{
	double a, b, c, x = 0, delta;
	
	a = ((y_1+y1)/2) - y0;
	b = (y1-y_1)/2;
	c = y0;
	delta = b*b - 4*a*c;
	if (a == 0)
	{
		x = (b != 0) ? -c/b : 0;
	}
	else if (delta >=0)
	{
		delta = sqrt(delta);
		x = (fabs(-b + delta) < fabs(-b - delta)) ? -b + delta : -b - delta ;
		x /= 2*a; 
	}
	return (float)x;
}

int prepare_filtered_profile_2(float *zp, int nx, int flag, int filter, int width, int flat_end)
{
	register int j, i;
	float moy,  moyc;

	
	/* remove dc based upon the first and the last part of data */
	/*	we assume a peak in the middle !*/	

	for(i = 0, j = nx - flat_end, moy = 0; i < flat_end; i++, j++)		
		moy += zp[i] + zp[j];	

	for(j = (nx/2) - flat_end, moyc = 0; j < (nx/2) + flat_end; j++)		
		moyc += zp[j];	

	for(i = 0, moy = moy/(2*flat_end); i < nx; i++)	
		zp[i] -= moy;
	
	moyc = moyc/(2*flat_end);

	/* I take the amplitude of the modulation to normalize */
	moy = moy - moyc; 
	if (moy == 0) 
	{
	  /*		activate_acreg(create_plot_from_1d_array("profile", nx, zpk));*/
		win_printf("pb of normalisation moy %f moyc %f",  moy,moyc);
	}
	else {for (j=0, moy = ((float)1)/moy; j< nx ; j++)   zp[j] *= moy;}

	/* no windowing, profile must be symmetrical */
	realtr1(nx, zp);		fft(nx, zp, 1);	realtr2(nx, zp, 1);


	zp[0] = zp[1] = 0; 
	bandpass_smooth_half (nx, zp, filter, width);
	fft(nx, zp, -1);

	return 0;
}

float 	find_simple_phase_shift_between_filtered_profile_2(float *zr, float *zp, int nx, int rc)
{
	register int i, j;
	float im, re, amp, phi, w, ph;
	
	if (zr == NULL || zp == NULL)	return WRONG_ARGUMENT;
	for (j = 2, i = nx - j, phi = 0, w = 0; j <= nx/2 - rc; i -= 2, j += 2)
	{
		re = zr[i] * zp[i] + zp[i+1] * zr[i+1];
		im = zp[i+1] * zr[i] - zp[i] * zr[i+1];
		ph = (im == 0.0 && re == 0.0) ? 0.0 : atan2(im,re);
		amp = (zr[i] * zr[i] + zr[i+1] * zr[i+1]) * (zp[i] * zp[i] + zp[i+1] * zp[i+1]);
		w += amp;
		phi += amp*ph;
	}
	for (j = 0, i = j; j < nx/2 - rc; i += 2, j += 2)
	{
		re = zr[i] * zp[i] + zp[i+1] * zr[i+1];
		im = zp[i+1] * zr[i] - zp[i] * zr[i+1];
		ph = (im == 0.0 && re == 0.0) ? 0.0 : atan2(im,re);
		amp = (zr[i] * zr[i] + zr[i+1] * zr[i+1]) * (zp[i] * zp[i] + zp[i+1] * zp[i+1]);
		w += amp;
		phi -= amp*ph;
	}
	return  (w != 0.0) ? phi/w : phi;
}


int find_z_profile_by_mean_square_and_phase_2(float *zp, int nx, O_i *oir, int w_flag, int filter, int width, int rc, float *z, int flat_end)
{
	register int k, j;
	int er = 0, ny;
	static int oldnx = 0;
	static float *yt = NULL;
	float min, tmp, ph_1, phi, ph1;
	union pix *psd;
	O_p *opz;
	d_s *dsrz;
	
	if (yt == NULL || oldnx != nx)
	{
		yt = (float*)realloc(yt,nx*sizeof(float));
		if (yt == NULL)		return -1;
		oldnx = nx;
	}
	psd = oir->im.pixel;
	ny = oir->im.ny;
	opz = (oir->n_op != 0) ? oir->o_p[0] : NULL;
	dsrz = (opz != NULL) ? opz->dat[0] : NULL;
	for (j=0; j< nx ; j++) yt[j] = zp[j];
	prepare_filtered_profile_2(yt, nx, w_flag, filter, width, flat_end);
	j = coarse_find_z_by_least_square_min_2(yt, nx, oir, &min,  rc);
	j = (j == 0) ? j + 1: j;
	j = (j == ny-1) ? ny - 2: j;
	/*
	*z = oir->ay + oir->dy *  j;
	return er;	
	*/
	tmp = (float)j;
	phi = find_simple_phase_shift_between_filtered_profile_2(psd[j].fl, yt, nx, rc);
	tmp = (dsrz != NULL) ? dsrz->xd[j] : 0;
	tmp = (tmp != 0) ? (float)j + phi/tmp : (float)j;
	k = (int)(tmp+.5);  
	er = (abs(k - j) >= 2) ? 1 : 0;
	k = (k <= 0) ? 1: k;
	k = (k >= ny-1) ? ny - 2: k;
	
	ph_1 = (k-1 == j) ? phi : 
		find_simple_phase_shift_between_filtered_profile_2(psd[k-1].fl, yt, nx, rc);
	ph1 = (k+1 == j) ? phi :
		find_simple_phase_shift_between_filtered_profile_2(psd[k+1].fl, yt, nx, rc);
	phi = (k == j) ? phi :
		find_simple_phase_shift_between_filtered_profile_2(psd[k].fl, yt, nx, rc);
	*z = oir->ay + oir->dy * (find_zero_of_3_points_polynome(ph_1, phi, ph1) + k);
	return er;
}



O_i	*image_band_pass_in_real_out_complex_2(O_i *dst, O_i *oi,int w_flag, int filter, int width, int rc, int flat_end)
{
	register int i;
	int nx, ny;
	union pix *ps;
	O_p *op = NULL;
	d_s *ds = NULL;

	
	if (oi == NULL || oi->im.data_type == IS_COMPLEX_IMAGE)		
	  {
	    win_printf("Wrong image passed");
	    return NULL;
	  }
	ny = oi->im.ny;	nx = oi->im.nx;
	
	if (dst == NULL)	dst = create_one_image( nx/2,  ny, IS_COMPLEX_IMAGE);
	else if (dst->im.data_type != IS_COMPLEX_IMAGE || dst->im.nx != oi->im.nx/2
		|| dst->im.ny != oi->im.ny || dst->im.n_f != 1)
	{
		free_one_image(dst);
		dst = create_one_image( nx/2,  ny, IS_COMPLEX_IMAGE);
	}
	if (dst == NULL)	return NULL;
	ps = dst->im.pixel;
	for (i=0 ; i< ny ; i++)
	{
	        display_title_message("doing line %d",i);
		extract_raw_line (oi, i, ps[i].fl);
		prepare_filtered_profile_2(ps[i].fl, nx, w_flag, filter, width, flat_end);
	}
	inherit_from_im_to_im(dst,oi);
	uns_oi_2_oi(dst,oi);	
	dst->im.win_flag = oi->im.win_flag;
	if (oi->x_title != NULL)	set_im_x_title(dst,oi->x_title);
	if (oi->y_title != NULL)	set_im_y_title(dst,oi->y_title);
	set_im_title(dst, "\\stack{{Bandpass filter image}{%s}"
	"{\\pt8 filter %d width %d flag %s}}",(oi->title != NULL) ? oi->title 
	: "untitled",filter,width,(w_flag)?"periodic":"not periodic");
	set_formated_string(&dst->im.treatement,"Bandpass filter image %s"
	"filter %d width %d flag %s",(oi->title != NULL) ? oi->title 
	: "untitled",filter,width,(w_flag)?"periodic":"not periodic");
	op = create_and_attach_op_to_oi(dst, ny, ny, 0,0);
	if (op == NULL)		return win_printf_ptr("cannot create plot!");
	ds = op->dat[0];
	op->type = IM_SAME_Y_AXIS;
	uns_oi_2_op(dst, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
	set_formated_string(&ds->treatement,"Simple phase difference from n and n-1 profile");
	for (i=0 ; i< ny-1 ; i++)
	{	
		ds->xd[i] = find_simple_phase_shift_between_filtered_profile_2(ps[i].fl, ps[i+1].fl, nx, rc);
		ds->yd[i] = (float)i;
	}
	return dst;
}



int translate_fft(float *x, int nx, float dx, float *xt)
{
	register int i,j;
	double phase, co, si, re, im;
	
	phase = -2*M_PI*dx/nx;
	for ( i=1 ; i<nx/2 ; i++)
	{
		j = 2*i;
		co = cos(phase*i);
		si = sin(phase*i);
		re = (double) x[j];
		im = (double) x[j+1];
		xt[j]   = (float)( co*re - si*im);
		xt[j+1] = (float)( co*im + si*re);
	}
	xt[0] = x[0];
	xt[1] = x[1];
	return 0;
}

int fftwindow_flat_top1(int npts, float *x, float smp)
{
	register int i, j;
	int n_2, n_4;
	float tmp, *offtsin;
	
	offtsin = get_fftsin();

	if ((j = fft_init(npts)) != 0)
		return j;

	n_2 = npts/2;
	n_4 = n_2/2;
	x[0] *= smp;
	smp += 1.0;
	for (i=1, j=n_4-2; j >=0; i++, j-=2)
	{
		tmp = (smp - offtsin[j]);
		x[i] *= tmp;
		x[npts-i] *= tmp;
	}
	for (i=n_4/2, j=0; i < n_4 ; i++, j+=2)
	{
		tmp = (smp + offtsin[j]);
		x[i] *= tmp;
		x[npts-i] *= tmp;
	}
	return 0;
}
int deplace(float *x, int nx)
{
	register int i, j;
	float tmp;
	
	for (i = 0, j = nx/2; j < nx ; i++, j++)
	{
		tmp = x[j];
		x[j] = x[i];
		x[i] = tmp;
	}
	return 0;
}




/*		correlate "x1" with its invert over "nx" points, returns the correlation
 *		in "fx1". "fx1" will be used to compute fft and result 
 *		setting them to NULL will allocate space. "window" will enable
 *		a windowing process if != 0 a double windowing if = to 2.
 */
int	correlate_1d_sig_and_invert_2(float *x1, int nx, int window, float *fx1, int filter, int remove_dc, int flat_end)
{
	register int i, j;
	float moy, smp = 0.05;
	float m1, re1, re2, im1, im2;
	
	if (x1 == NULL)				return WRONG_ARGUMENT;
	if (fft_init(nx) || filter_init(nx))	return FFT_NOT_SUPPORTED;
	if (fx1 == NULL)	fx1 = (float*)calloc(nx,sizeof(float));
	if (fx1 == NULL)			return OUT_MEMORY;

	/*	compute fft of x1 */
	for(i = 0; i < nx; i++)		fx1[i] = x1[i];
	for(i = 0, j = nx - flat_end, moy = 0; i < flat_end; i++, j++)		
		moy += fx1[i] + fx1[j];	
	for(i = 0, moy = moy/(2*flat_end); remove_dc && i < nx; i++)	
		fx1[i] = x1[i] - moy;
	if (window == 1)	fftwindow1(nx, fx1, smp);
	else if (window == 2)	fftwindow_flat_top1(nx, fx1, smp);
	realtr1(nx, fx1);
	fft(nx, fx1, 1);
	realtr2(nx, fx1, 1);
	
	/*	compute normalization */
	for (i=0, m1 = 0; i< nx; i+=2)
		m1 += fx1[i] * fx1[i] + fx1[i+1] * fx1[i+1];
	m1 /= 2;	
		
	/*	compute correlation in Fourier space */
	for (i=2; i< nx; i+=2)
	{
		re1 = fx1[i];		re2 = fx1[i];
		im1 = fx1[i+1];		im2 = -fx1[i+1];
		fx1[i] 		= (re1 * re2 + im1 * im2)/m1;
		fx1[i+1] 	= (im1 * re2 - re1 * im2)/m1;
	}
	fx1[0]	= 2*(fx1[0] * fx1[0])/m1;	/* these too mode are special */
	fx1[1] 	= 2*(fx1[1] * fx1[1])/m1;
	if (filter> 0)		lowpass_smooth_half (nx, fx1, filter);
	
	/*	get back to real world */
	realtr2(nx, fx1, -1);
	fft(nx, fx1, -1);
	realtr1(nx, fx1);
	deplace(fx1, nx);
	return 0;
}	


/*	Find maximum position in a 1d array, the array is supposed periodic
 *	return 0 -> everything fine, TOO_MUCH_NOISE or PB_WITH_EXTREMUM
 *
 */
int	find_max1(float *x, int nx, float *Max_pos, float *Max_val)
{
	register int i;
	int  nmax, ret = 0, delta;
	double a, b, c, d, f_2, f_1, f0, f1, f2, xm = 0;

	for (i = 0, nmax = 0; i < nx; i++) nmax = (x[i] > x[nmax]) ? i : nmax;
	f_2 = x[(nx + nmax - 2) % nx];
	f_1 = x[(nx + nmax - 1) % nx];
	f0 = x[nmax];
	f1 = x[(nmax + 1) % nx];
	f2 = x[(nmax + 2) % nx];
	if (f_1 < f1)
	{
		a = -f_1/6 + f0/2 - f1/2 + f2/6;
		b = f_1/2 - f0 + f1/2;
		c = -f_1/3 - f0/2 + f1 - f2/6;
		d = f0;
		delta = 0;
	}	
	else
	{
		a = b = c = 0;	
		a = -f_2/6 + f_1/2 - f0/2 + f1/6;
		b = f_2/2 - f_1 + f0/2;
		c = -f_2/3 - f_1/2 + f0 - f1/6;
		d = f_1;
		delta = -1;
	}
	if (fabs(a) < 1e-8)
	{
		if (b != 0)	xm =  - c/(2*b) - (3 * a * c * c)/(4 * b * b * b);
		else
		{
			xm = 0;
			ret |= PB_WITH_EXTREMUM;
		}
	}
	else if ((b*b - 3*a*c) < 0)
	{
		ret |= PB_WITH_EXTREMUM;
		xm = 0;
	}
	else
		xm = (-b - sqrt(b*b - 3*a*c))/(3*a);
	/*
	for (p = -2; p < 1; p++)
	{
		if (6*a*p+b > 0) ret |= TOO_MUCH_NOISE;
	}	
	*/
	*Max_pos = (float)(xm + nmax + delta); 
	*Max_val = (a * xm * xm * xm) + (b * xm * xm) + (c * xm) + d;
	return ret;
}


float	find_distance_from_center_2(float *x1, int cl, int flag, int filter, int black_circle, float *corr, int flat_end)
{
	static float fx1[512];
	float dx;
	
	correlate_1d_sig_and_invert_2(x1, cl, flag, fx1, filter, !black_circle, flat_end);
	find_max1(fx1, cl, &dx,corr);
	dx -= cl/2;
	dx /=2;
	return dx;
}



O_i	*frommovie_profile_averaged_along_y(O_i *ois, int l_s, int l_e, int dead, int avg_frames)
{
	register int i, j, k;
	O_i *oid;
	float tmp, *tmpf = NULL;
	int onx, ony, data_type, nf, no;
	union pix  *pd;

	onx = ois->im.nx;	ony = ois->im.ny;	
	data_type = ois->im.data_type;
	nf = abs(ois->im.n_f);		
	no = nf/avg_frames;
	oid =  create_one_image(onx, no, IS_FLOAT_IMAGE);
	tmpf = (float*)calloc(onx,sizeof(float));
	if (oid == NULL || tmpf == NULL)
	{
	  win_printf("Can't create dest image");
	  return NULL;
	}
	uns_oi_2_oi_by_type(ois, IS_Z_UNIT_SET, oid, IS_Z_UNIT_SET);
	uns_oi_2_oi_by_type(ois, IS_X_UNIT_SET, oid, IS_X_UNIT_SET);
	uns_oi_2_oi_by_type(ois, IS_T_UNIT_SET, oid, IS_Y_UNIT_SET);
	for (i = 1 ; i< oid->n_yu ; i++)
	    oid->yu[i]->dx *= avg_frames; 

	pd = oid->im.pixel;
	for (i=0 ; i< nf ; i++)
	  { 
	    switch_frame(ois,i);
	    for (j = l_s ; j < l_e ; j++)
	      {
		extract_raw_line(ois, j, tmpf);
		if (i%avg_frames >= dead)
		  {
		    for (k=0 ; k < onx ; k++)	pd[i/avg_frames].fl[k] += tmpf[k];
		  }	
	      }
	  }
	tmp = ((l_e - l_s) != 0) ? (float)1/((avg_frames-dead)*(l_e - l_s)) : 1; 
	for (i=0 ; i< no ; i++)
	    for (k=0; k< onx; k++) pd[i].fl[k] *= tmp;

	inherit_from_im_to_im(oid,ois);
	oid->width = 1.0;
	oid->height = 1.0;

	oid->im.win_flag = ois->im.win_flag;
	if (ois->title != NULL)	
	  set_im_title(oid, "%s x averaged profile from y= %d to %d",
		 ois->title,l_s,l_e);
	set_formated_string(&oid->im.treatement,
			    "%s x averaged profile from y= %d to %d",
			    ois->filename,l_s,l_e);
	free(tmpf);
	return oid;
}

int do_frommovie_profile_averaged_along_y(void)
{
	O_i *ois, *oid;
	imreg *imr;
	int i, nf;
	static int l_s = 0, l_e = 0, avg = 1, dead = 2;

	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	{
		win_printf("Cannot find image");
		return D_O_K;
	}
	if(updating_menu_state != 0)	
	{
		if (ois->im.n_f == 1 || ois->im.n_f == 0)	
			active_menu->flags |=  D_DISABLED;
		else active_menu->flags &= ~D_DISABLED;
		return D_O_K;	
	}	
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine take a profile along the X "
				     "direction averaged along y and place it "
				     "in an image");
	}
	nf = abs(ois->im.n_f);		
	if (nf <= 0)	
	{
		win_printf("This is not a movie");
		return D_O_K;
	}
	i = ois->im.c_f;
	l_s = (l_s >=0 && l_s < ois->im.ny && l_s < l_e) ? l_s : 0;
	l_e = (l_e >=0 && l_e <= ois->im.ny && l_s < l_e) ? l_e : ois->im.ny;
	i = win_scanf("Average in a frame between lines%d and %daverage frames dead period %d "
		      "over a period %d",&l_s,&l_e,&dead,&avg);
	if (i == CANCEL)	return D_O_K;
	oid = frommovie_profile_averaged_along_y(ois,l_s,l_e, dead, avg);
	if (oid == NULL)	return win_printf_OK("unable to create image!");
	add_image(imr, oid->type, (void*)oid);
	find_zmin_zmax(oid);
	return (refresh_image(imr, imr->n_oi - 1));
}

int do_find_prof_center(void)
{
	register int i;
	O_i *ois;
	O_p *op;
	d_s *ds;	
	imreg *imr;
	float *tmpf, corr;
	
	if(updating_menu_state != 0)	return D_O_K;	
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	{
		win_printf("Cannot find image");
		return D_O_K;
	}
	i = win_scanf("extend of light region at both end %d",&flat_end);
	if (i == CANCEL)  return OFF;
	tmpf = (float*)calloc(ois->im.nx,sizeof(float));
	fft_init(ois->im.nx);
	filter_init(ois->im.nx);
	op = create_and_attach_op_to_oi(ois, ois->im.ny,ois->im.ny, 0, IM_SAME_Y_AXIS|IM_SAME_X_AXIS);
	if (op == NULL || tmpf == NULL) return (win_printf_OK("cant create plot!"));
	ds = op->dat[0];

	set_plot_title(op,"Offset of pattern");
	set_formated_string(&ds->treatement,"Offset of pattern");
	uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
	uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);

	//for (i=0 ; i< ds->ny ; i++)	ds->yd[i] = i;
	for (i=0 ; i< ois->im.ny ; i++)
	{
	  ds->yd[i] = i;
	  extract_raw_line(ois, i, tmpf);
	  ds->xd[i] = find_distance_from_center_2(tmpf, ois->im.nx, 0, ois->im.nx>>2, 1, &corr,flat_end);
	  ds->xd[i] += ois->im.nx/2;	
	}
	free(tmpf);
	ois->cur_op = ois->n_op - 1;
	broadcast_dialog_message(MSG_DRAW,0);	
	return D_REDRAWME;		
}


int do_recenter(void)
{
	register int i, k;
	O_i *ois, *oid;
	imreg *imr;
	float *tmpf, corr, dx;
	union pix  *pd;
	
	if(updating_menu_state != 0)	return D_O_K;	
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	{
		win_printf("Cannot find image");
		return D_O_K;
	}
	i = win_scanf("extend of light region at both end %d",&flat_end);
	if (i == CANCEL)  return OFF;
	oid = create_and_attach_oi_to_imr(imr, ois->im.nx, ois->im.ny, IS_FLOAT_IMAGE);
	tmpf = (float*)calloc(ois->im.nx,sizeof(float));
	fft_init(ois->im.nx);
	filter_init(ois->im.nx);
	if (tmpf == NULL || oid == NULL) return (win_printf_OK("cant create array!"));
	uns_oi_2_oi(oid, ois);
	pd = oid->im.pixel;
	for (i=0 ; i< ois->im.ny ; i++)
	{
		extract_raw_line(ois, i, tmpf);
		dx = find_distance_from_center_2(tmpf, ois->im.nx, 0, ois->im.nx>>2, 1, &corr, flat_end);
		extract_raw_line(ois, i, tmpf);
		realtr1(ois->im.nx, tmpf);
		fft(ois->im.nx, tmpf, 1);
		realtr2(ois->im.nx, tmpf, 1);
		translate_fft(tmpf,ois->im.nx,-dx,tmpf);
		realtr2(ois->im.nx, tmpf, -1);
		fft(ois->im.nx, tmpf, -1);
		realtr1(ois->im.nx, tmpf);
		for (k=0 ; k < ois->im.nx ; k++)	pd[i].fl[k] += tmpf[k];	
	}
	free(tmpf);
	find_zmin_zmax(oid);
	return (refresh_image(imr, imr->n_oi - 1));
}


int do_band_pass(void)
{
	register int i;
	O_i *ois, *oid;
	imreg *imr;
	
	if(updating_menu_state != 0)	return D_O_K;	
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	{
		win_printf("Cannot find image");
		return D_O_K;
	}
	i = win_scanf("band pass center %4d band pass width %4d critical r %4d\n"
		      "extend of light region at both end %4d\n"
		      ,&bp_center,&bp_width,&rc,&flat_end);	
	if (i == CANCEL)  return OFF;
	oid = image_band_pass_in_real_out_complex_2(NULL, ois, 0, bp_center, bp_width, rc, flat_end);
	if (oid == NULL)	return win_printf_OK("unable to create image!");
	add_image(imr, oid->type, (void*)oid);
	find_zmin_zmax(oid);
	return (refresh_image(imr, imr->n_oi - 1));
}

int do_find_prof_z(void)
{
	register int i;
	O_i *ois, *oir = NULL;
	O_p *op;
	d_s *ds;	
	imreg *imr;
	static int oir_n = 0;
	float *tmpf;// min
	
	if(updating_menu_state != 0)	return D_O_K;	
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	{
		win_printf("Cannot find image");
		return D_O_K;
	}
	i = win_scanf("Index of band-passed ref image %4d\n"
		      "band pass center %4d band pass width %4d critical r %4d\n"
		      "extend of light region at both end %4d\n"
		      ,&oir_n,&bp_center,&bp_width,&rc,&flat_end);	
	if (i == CANCEL)  return OFF;
	if (oir_n < 0 || oir_n > imr->n_oi) return win_printf_OK("wrong image index");
	oir = imr->o_i[oir_n];

	tmpf = (float*)calloc(ois->im.nx,sizeof(float));
	fft_init(ois->im.nx);
	filter_init(ois->im.nx);
	op = create_and_attach_op_to_oi(ois, ois->im.ny,ois->im.ny, 0, IM_SAME_Y_AXIS|IM_SAME_X_AXIS);
	if (op == NULL || tmpf == NULL) return (win_printf_OK("cant create plot!"));
	ds = op->dat[0];

	set_plot_title(op,"Z of pattern");
	set_formated_string(&ds->treatement,"Z of pattern");
	uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_X_UNIT_SET);
	uns_oi_2_op(oir, IS_Z_UNIT_SET, op, IS_Y_UNIT_SET);

	//for (i=0 ; i< ds->ny ; i++)	ds->yd[i] = i;
	for (i=0 ; i< ois->im.ny ; i++)
	{
	  ds->xd[i] = i;
	  extract_raw_line(ois, i, tmpf);
	  find_z_profile_by_mean_square_and_phase_2(tmpf, ois->im.nx, oir, 0, bp_center, bp_width, rc, ds->yd+i, flat_end);
	  /*
	  prepare_filtered_profile_2(tmpf, ois->im.nx,  0, bp_center, bp_width, flat_end);
	  ds->yd[i] = coarse_find_z_by_least_square_min_2(tmpf, ois->im.nx, oir, &min, rc);
	  */
	}
	free(tmpf);
	ois->cur_op = ois->n_op - 1;
	broadcast_dialog_message(MSG_DRAW,0);	
	return D_REDRAWME;		
}


MENU *frommovie_image_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"image average X profile", do_frommovie_profile_averaged_along_y,NULL,0,NULL);
	add_item_to_menu(mn,"track center", do_find_prof_center,NULL,0,NULL);
	add_item_to_menu(mn,"recenter", do_recenter,NULL,0,NULL);
	add_item_to_menu(mn,"band pass", do_band_pass,NULL,0,NULL);
	add_item_to_menu(mn,"Track Z", do_find_prof_z,NULL,0,NULL);

	return mn;
}

int	frommovie_main(int argc, char **argv)
{
	add_image_treat_menu_item ( "frommovie", NULL, frommovie_image_menu(), 0, NULL);
	return D_O_K;
}

int	frommovie_unload(int argc, char **argv)
{
	remove_item_to_menu(image_treat_menu, "frommovie", NULL, NULL);
	return D_O_K;
}
#endif








