#ifndef _FROMMOVIE_H_
#define _FROMMOVIE_H_

PXV_FUNC(int, do_frommovie_average_along_y, (void));
PXV_FUNC(MENU*, frommovie_image_menu, (void));
PXV_FUNC(int, frommovie_main, (int argc, char **argv));
#endif

