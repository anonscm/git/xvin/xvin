#ifndef _BROWN_H_
#define _BROWN_H_


# define TRACKING_BUFFER_SIZE 4096
# define PROFILE_BUFFER_SIZE 128

# define OBJ_MOVING 32
typedef struct bead_tracking
{
  int xc, yc, x0, y0, not_lost, im_prof_ref;    // bead pixel position
  int starting_traching_im_nb;     // the absolute number of image when tracking was started 
  //  int c_i, n_i, m_i;
  int black_circle;                // flag modifying the bead tracking extend
  int bead_number;                 // a unique bead identifier
  int bp_center;                   // the bp center frequency
  int bp_width;                    // the bp widt frequency
  int rc;                          // the critical radius 
  int integral;                    // a local variable containinin the integral of PID
  float sat_int;                   // the maxium integral value in PID
  float gain;                      // the PID parameters
  float integral_gain;
  float derivative_gain;
  float x[TRACKING_BUFFER_SIZE], y[TRACKING_BUFFER_SIZE], z[TRACKING_BUFFER_SIZE];                // bead position
  char n_l[TRACKING_BUFFER_SIZE];                       // not lost idicator
  float rad_prof_ref[PROFILE_BUFFER_SIZE];             // reference radial profile
  float rad_prof_ref_fft[PROFILE_BUFFER_SIZE];         // reference radial profile fourrier transform
  float rad_prof_fft[PROFILE_BUFFER_SIZE];             // radial profile fourrier transform
  float rad_prof[TRACKING_BUFFER_SIZE][PROFILE_BUFFER_SIZE];                // radial profile
  O_i *calib_im;		   // calibration image
  O_i *calib_im_fil;	           // filtered calibration image
} b_track;


typedef struct gen_tracking
{
  int (*user_tracking_action)(struct gen_tracking *gt, DIALOG *d);  // a user callback function       
  int c_i, n_i, m_i;                // c_i, index of the current image, n_i size of buffer, m_i size allocated
  int ac_i;                         // ac_i actual number of frame since acquisition started
  int cl, cw, dis_filter, bd_mul;   // cross size, filter def, and bd_mul define the loss of bead condition 
  int c_b, n_b, m_b;                // the current bead nb and the max bead nb
  float max_obj_mov;                // Maximum relative objective displacement in servo mode
  float z_obj_servo_start;          // objective position at the start of servo mode
  int bead_servo_nb;                // the bead used to servo the objective
  int xi[256], yi[256];             // tmp int buffers to avg profile
  float xf[256], yf[256];           // tmp float buffers to do fft
  b_track **bd;                     // the bead pointer
  int imi[TRACKING_BUFFER_SIZE];                         // the image nb
  long long imt[TRACKING_BUFFER_SIZE];                   // the image absolute time
  unsigned long imdt[TRACKING_BUFFER_SIZE];              // the time spent in the previous function call
  float zmag[TRACKING_BUFFER_SIZE], rot_mag[TRACKING_BUFFER_SIZE], obj_pos[TRACKING_BUFFER_SIZE];  // the magnet position, the objective position measured
  //float *user_val1, user_val2;    // user parameters
  int status_flag[TRACKING_BUFFER_SIZE];
  float zmag_cmd[TRACKING_BUFFER_SIZE], rot_mag_cmd[TRACKING_BUFFER_SIZE], obj_pos_cmd[TRACKING_BUFFER_SIZE];  // the magnet position, the objective position asked for

} g_track;






# endif








