/*
 *
 *    V. Croquette
 *      JF Allemand
 */
#ifndef _BROWN_C_
#define _BROWN_C_

# include "allegro.h"
# include "winalleg.h"
# include "config.h"
# include "float.h"
# include "ifcapi.h"
# include "xvin.h"
# include "fftl32n.h"
# include "fillib.h"
# include "largeint.h"
# include "xvplot.h"

# define NB_FRAMES_BEF_LOST 8

/* If you include other plug-ins header do it here*/ 

# include "trmov.h"
# include "ifc.h"


//extern __declspec(dllexport) IFC_disp disp_IFC;
//extern __declspec(dllexport) int aquisition_period;

extern IFC_disp disp_IFC;
extern int aquisition_period;
extern O_i  *oi_IFC;
extern pltreg  *pr_IFC;


unsigned long min_plot_refresh = 0;

float keep = 0, keep_1 = 0, keep_2 = 0;

/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "brown.h"



float *radial_non_pixel_square_image_sym_profile_in_array(float *rz, O_i *ois, float xc, float yc, int r_max, float ax, float ay);
float *radial_non_pixel_square_image_sym_profile_in_array_sse(float *rz, O_i *ois, float xc, float yc, int r_max, float ax, float ay);
int grab_bead_info_from_calibration(O_i *oi, float *xb, float *yb, float *zb, int *nx, int *cw);

int save_data_to_disk_from_rolling_buffer(int starting_index, int end_index, char **filenames, g_track *track_information);
int aquire_and_save_data_in_real_time(void);
int test_background_saving(void);
void CALLBACK automatic_saving(HWND hwnd, UINT uMsg, UINT_PTR idEvent,  DWORD dwTime);
int kill_timer(void);
int save_data_to_disk_from_rolling_buffer_xv_format(int starting_index,int end_index,char *filename,g_track *track_information);
int fft_band_pass_in_real_out_complex(float *x, int nx, int w_flag, int fc, int width);
float 	find_phase_shift_between_profile(float *zr, float *zp, int npc, 
					 int flag, int filter, int width, int rc);


float read_Z_value(void);
int set_Z_value(float z);

typedef int v4sf __attribute__ ((mode(V4SF))); // vector of four single floats

union f4vector 
{
  v4sf v;
  float f[4];
};
      
union f4vector f4, fax, fay, f0, fx, fy, fxc, fyc, fy2, fr[64], fx2[64];


unsigned char ch_max = 255;
int	filter_ratio = 2;
int	bead_lost_mul = 16;
float radial_dx[256];

DIALOG *starting_one = NULL;
int index_menu_dialog = -1;

g_track *track_info = NULL;



// from trmov

    





int	fill_avg_profiles_from_im(O_i *oi, int xc, int yc, int cl, int cw, int *x, int *y)
{
	register int j, i;
	unsigned char *choi;	
	short int *inoi;
	int ytt, xll, onx, ony, xco, yco, cl2, cw2;
	union pix *pd;
	
	if (oi == NULL || x == NULL || y == NULL)	return 1;
	onx = oi->im.nx;	ony = oi->im.ny;
	pd = oi->im.pixel;
	cl2 = cl >> 1;	cw2 = cw >> 1;
	yco = (yc - cl2 < 0) ? cl2 : yc;
	yco = (yco + cl2 <= ony) ? yco : ony - cl2;
	xco = (xc - cl2 < 0) ? cl2 : xc;
	xco = (xco + cl2 <= onx) ? xco : onx - cl2;
	for(i = 0; i < cl; i++)		x[i] = y[i] = 0;	
	if (oi->im.data_type == IS_CHAR_IMAGE)
	{
		for(j = 0, ytt = yco - cw2, xll = xco - cl2; j< cw; j++)
		{	
			choi = pd[ytt+j].ch + xll;
			for (i = 0; i < cl; i++)		x[i] += (int)choi[i];	
		}
		for(j = 0, ytt = yco - cl2, xll = xco - cw2; j <cl; j++)
		{	
			choi = pd[ytt+j].ch + xll;
			for (i = 0; i < cw; i++)		y[j] += (int)choi[i];	
		}
	}
	else if (oi->im.data_type == IS_INT_IMAGE)
	{
		for(j = 0, ytt = yco - cw2, xll = xco - cl2; j< cw; j++)
		{	
			inoi = pd[ytt+j].in + xll;
			for (i = 0; i < cl; i++)		x[i] += (int)inoi[i];	
		}
		for(j = 0, ytt = yco - cl2, xll = xco - cw2; j <cl; j++)
		{	
			inoi = pd[ytt+j].in + xll;
			for (i = 0; i < cw; i++)		y[j] += (int)inoi[i];	
		}
	}
	else return 1;	
	return 0;		
}


int	check_bead_not_lost(int *x, float *x1, int *y, float *y1, int cl, float bd_mul)
{
	register  int i, j;
	int minx, maxx, miny, maxy;
	
	for (i = 0, minx = maxx = x[0], miny = maxy = y[0]; i < cl; i++)
	{
		j = x[i];
		minx = (j < minx) ? j : minx;
		maxx = (j > maxx) ? j : maxx;			
		x1[i] = (float)j;
		j = y[i];
		miny = (j < miny) ? j : miny;
		maxy = (j > maxy) ? j : maxy;						
		y1[i] = (float)j;			
	}
	return (((maxx-minx)*bd_mul) < (maxx+minx) && 
		((maxy-miny)*bd_mul) < (maxy+miny)) ? 1 : 0;
}

int     erase_around_black_circle(int *x, int *y, int cl)
{
	register int j;
	int xb0, xb1, zb0, zb1;
	
	/* 	for good bead there is a definite black circle around the image
		for rotation with a small bead you can suppress the image around
		this black circle to avoid perturbation by the small bead
	*/
		
	for (xb0 = j = 0, zb0 = x[0]; j < cl>>1; j++)
		zb0 = (zb0 < x[j]) ? zb0 : x[(xb0 = j)];
	for (xb1 = j = cl-1, zb1 = x[xb1]; j >= cl>>1; j--)
		zb1 = (zb1 < x[j]) ? zb1 : x[(xb1 = j)];
	zb0 = (zb0 > zb1) ? zb0 : zb1;
	for (j = 0; j < cl; j++)
		x[j] = (j < xb0 || j > xb1) ? 0 : x[j] - zb0;
	for (xb0 = j = 0, zb0 = y[0]; j < cl>>1; j++)
		zb0 = (zb0 < y[j]) ? zb0 : y[(xb0 = j)];
	for (xb1 = j = cl-1, zb1 = y[xb1]; j >= cl>>1; j--)
		zb1 = (zb1 < y[j]) ? zb1 : y[(xb1 = j)];
	zb0 = (zb0 > zb1) ? zb0 : zb1;
	for (j = 0; j < cl; j++)
		y[j] = (j < xb0 || j > xb1) ? 0 : y[j] - zb0;
	return 0;	
}	

int deplace(float *x, int nx)
{
	register int i, j;
	float tmp;
	
	for (i = 0, j = nx/2; j < nx ; i++, j++)
	{
		tmp = x[j];
		x[j] = x[i];
		x[i] = tmp;
	}
	return 0;
}
int fftwindow_flat_top1(int npts, float *x, float smp)
{
	register int i, j;
	int n_2, n_4;
	float tmp, *offtsin;
	
	offtsin = get_fftsin();

	if ((j = fft_init(npts)) != 0)
		return j;

	n_2 = npts/2;
	n_4 = n_2/2;
	x[0] *= smp;
	smp += 1.0;
	for (i=1, j=n_4-2; j >=0; i++, j-=2)
	{
		tmp = (smp - offtsin[j]);
		x[i] *= tmp;
		x[npts-i] *= tmp;
	}
	for (i=n_4/2, j=0; i < n_4 ; i++, j+=2)
	{
		tmp = (smp + offtsin[j]);
		x[i] *= tmp;
		x[npts-i] *= tmp;
	}
	return 0;
}
int reset_mouse()
{
    scare_mouse();    
    set_mouse_sprite(NULL);
    unscare_mouse();
    return 0;
}


/*	Find maximum position in a 1d array, the array is supposed periodic
 *	return 0 -> everything fine, TOO_MUCH_NOISE or PB_WITH_EXTREMUM
 *
 */
int	find_max1(float *x, int nx, float *Max_pos, float *Max_val)
{
	register int i;
	int  nmax, ret = 0, delta;
	double a, b, c, d, f_2, f_1, f0, f1, f2, xm = 0;

	for (i = 0, nmax = 0; i < nx; i++) nmax = (x[i] > x[nmax]) ? i : nmax;
	f_2 = x[(nx + nmax - 2) % nx];
	f_1 = x[(nx + nmax - 1) % nx];
	f0 = x[nmax];
	f1 = x[(nmax + 1) % nx];
	f2 = x[(nmax + 2) % nx];
	if (f_1 < f1)
	{
		a = -f_1/6 + f0/2 - f1/2 + f2/6;
		b = f_1/2 - f0 + f1/2;
		c = -f_1/3 - f0/2 + f1 - f2/6;
		d = f0;
		delta = 0;
	}	
	else
	{
		a = b = c = 0;	
		a = -f_2/6 + f_1/2 - f0/2 + f1/6;
		b = f_2/2 - f_1 + f0/2;
		c = -f_2/3 - f_1/2 + f0 - f1/6;
		d = f_1;
		delta = -1;
	}
	if (fabs(a) < 1e-8)
	{
		if (b != 0)	xm =  - c/(2*b) - (3 * a * c * c)/(4 * b * b * b);
		else
		{
			xm = 0;
			ret |= PB_WITH_EXTREMUM;
		}
	}
	else if ((b*b - 3*a*c) < 0)
	{
		ret |= PB_WITH_EXTREMUM;
		xm = 0;
	}
	else
		xm = (-b - sqrt(b*b - 3*a*c))/(3*a);
	/*
	for (p = -2; p < 1; p++)
	{
		if (6*a*p+b > 0) ret |= TOO_MUCH_NOISE;
	}	
	*/
	*Max_pos = (float)(xm + nmax + delta); 
	*Max_val = (a * xm * xm * xm) + (b * xm * xm) + (c * xm) + d;
	return ret;
}



/*		correlate "x1" with its invert over "nx" points, returns the correlation
 *		in "fx1". "fx1" will be used to compute fft and result 
 *		setting them to NULL will allocate space. "window" will enable
 *		a windowing process if != 0 a double windowing if = to 2.
 */
int	correlate_1d_sig_and_invert(float *x1, int nx, int window, float *fx1, int filter, int remove_dc)
{
	register int i, j;
	float moy, smp = 0.05;
	float m1, re1, re2, im1, im2;
	
	if (x1 == NULL)				return WRONG_ARGUMENT;
	if (fft_init(nx) || filter_init(nx))	return FFT_NOT_SUPPORTED;
	if (fx1 == NULL)	fx1 = (float*)calloc(nx,sizeof(float));
	if (fx1 == NULL)			return OUT_MEMORY;

	/*	compute fft of x1 */
	for(i = 0; i < nx; i++)		fx1[i] = x1[i];
	for(i = 0, j = (3*nx)/4, moy = 0; i < nx/4; i++, j++)		
		moy += fx1[i] + fx1[j];	
	for(i = 0, moy = (2*moy)/nx; remove_dc && i < nx; i++)	
		fx1[i] = x1[i] - moy;
	if (window == 1)	fftwindow1(nx, fx1, smp);
	else if (window == 2)	fftwindow_flat_top1(nx, fx1, smp);
	realtr1(nx, fx1);
	fft(nx, fx1, 1);
	realtr2(nx, fx1, 1);
	
	/*	compute normalization */
	for (i=0, m1 = 0; i< nx; i+=2)
		m1 += fx1[i] * fx1[i] + fx1[i+1] * fx1[i+1];
	m1 /= 2;	
		
	/*	compute correlation in Fourier space */
	for (i=2; i< nx; i+=2)
	{
		re1 = fx1[i];		re2 = fx1[i];
		im1 = fx1[i+1];		im2 = -fx1[i+1];
		fx1[i] 		= (re1 * re2 + im1 * im2)/m1;
		fx1[i+1] 	= (im1 * re2 - re1 * im2)/m1;
	}
	fx1[0]	= 2*(fx1[0] * fx1[0])/m1;	/* these too mode are special */
	fx1[1] 	= 2*(fx1[1] * fx1[1])/m1;
	if (filter> 0)		lowpass_smooth_half (nx, fx1, filter);
	
	/*	get back to real world */
	realtr2(nx, fx1, -1);
	fft(nx, fx1, -1);
	realtr1(nx, fx1);
	deplace(fx1, nx);
	return 0;
}	

float	find_distance_from_center(float *x1, int cl, int flag, int filter, int black_circle, float *corr)
{
	static float fx1[512];
	float dx;
	
	correlate_1d_sig_and_invert(x1, cl, flag, fx1, filter, !black_circle);
	find_max1(fx1, cl, &dx,corr);
	dx -= cl/2;
	dx /=2;
	return dx;
}


// end trmov



O_i	*do_load_calibration(int n);



int	draw_cross_in_screen_unit(int xc, int yc, int length, int width, int color, BITMAP* bmp)
{// bead pixel position
        int l = length >> 1;
	int w = width >> 1;
	int y = bmp->h - yc;

	int x1 =  xc - l;
	int x4 =  xc + l;
	int x2 =  xc - w;
	int x3 =  xc + w;
	int y1 =  y + l;
	int y4 =  y - l;
	int y2 =  y + w;
	int y3 =  y - w;

	hline(bmp, x1, y2, x2, color);
	vline(bmp, x2, y2, y1, color);
	hline(bmp, x2, y1, x3, color);
	vline(bmp, x3, y1, y2, color);
	hline(bmp, x3, y2, x4, color);
	vline(bmp, x4, y2, y3, color);
	hline(bmp, x3, y3, x4, color);
	vline(bmp, x3, y3, y4, color);
	hline(bmp, x2, y4, x3, color);
	vline(bmp, x2, y3, y4, color);
	hline(bmp, x1, y3, x2, color);
	vline(bmp, x1, y2, y3, color);
	/*

	int px[24], *py = px + 1;

	px[0<<1] = px[1<<1] = xc - l;
	px[10<<1] = px[11<<1] = px[2<<1] = px[3<<1] = xc - w;
	px[8<<1] = px[9<<1] = px[4<<1] = px[5<<1] = xc + w;
	px[6<<1] = px[7<<1] = xc + l;
	py[11<<1] = py[7<<1] = py[8<<1] = py[0<<1] = y - w;
	py[1<<1] = py[2<<1] = py[5<<1] = py[6<<1]  = y + w;
	py[3<<1] = py[4<<1] = y + l;
	py[9<<1] = py[10<<1] = y - l;
	polygon(bmp, 12, px, color);
	*/
	return 0;						
}														


# ifdef PB
/* Return values for fpclass. */
#define	_FPCLASS_SNAN	0x0001	/* Signaling "Not a Number" */
#define	_FPCLASS_QNAN	0x0002	/* Quiet "Not a Number" */
#define	_FPCLASS_NINF	0x0004	/* Negative Infinity */
#define	_FPCLASS_NN	0x0008	/* Negative Normal */
#define	_FPCLASS_ND	0x0010	/* Negative Denormal */
#define	_FPCLASS_NZ	0x0020	/* Negative Zero */
#define	_FPCLASS_PZ	0x0040	/* Positive Zero */
#define	_FPCLASS_PD	0x0080	/* Positive Denormal */
#define	_FPCLASS_PN	0x0100	/* Positive Normal */
#define	_FPCLASS_PINF	0x0200	/* Positive Infinity */
# endif


int start_servo(void)
{
  register int i;
  static float gain = .175, integral_gain = .01, derivative_gain = .2, max_obj_mov = 10;
  static float sat_int = 0.05;
  static int beadNumber = 0, bp_center = 12, bp_width = 10, rc = 10;
  b_track *bt = NULL;

  if(updating_menu_state != 0)	return D_O_K;
  
  if (track_info == NULL) 	return D_O_K;
  

  if (track_info->bead_servo_nb >= 0)
    {
      i = win_printf("Do you wabt to stop servo on bead %d!",track_info->bead_servo_nb);
      if (i == CANCEL)   	return D_O_K;
      track_info->bead_servo_nb = -1;
      set_Z_value(track_info->z_obj_servo_start);
      return D_O_K;
    }


  i = win_scanf("This routine servo the objective to maintain the the \n"
		"difraction pattern constant. It uses the starting position\n"
		"as the reference one. Choose bead to servo \n"
		"Bead 0 %R 1 %r 2 %r 3 %r\n ? Band pass filter setting"
		"center freq. %8d width of filter %8d critical circle %6d\n"
		"indicage what gain of PID for objective displacement %12f\n"
		"integral gain in the PID %12f derivative gain in the PID %12f\n"
		"saturation of error sig on integral %12f\n"
		"what is the maximum displacement of the objective %12f\n"
		,&beadNumber,&bp_center, &bp_width, &rc, &gain, &integral_gain, 
		&derivative_gain,&sat_int,&max_obj_mov);
  if (i == CANCEL)	return OFF;		
  if (beadNumber >= track_info->n_b)    return win_printf_OK("Bad bead number!");
  bt = track_info->bd[beadNumber];
  track_info->max_obj_mov = max_obj_mov; 
  bt->bp_center = bp_center;
  bt->bp_width = bp_width;
  bt->rc = rc;
  bt->gain = gain;
  bt->integral_gain = integral_gain;
  bt->derivative_gain = derivative_gain;
  bt->sat_int = sat_int;
  track_info->bead_servo_nb = beadNumber | 0x100;
  if (fft_init(track_info->cl) || filter_init(track_info->cl))	return win_printf("cannot init fft!");	
  return D_O_K;
}


int scan_zobj(void)
{
  register int i, k, l;
  static int nf = 16, nstep = 32;
  static float zsize = 0.3, dz;

  if(updating_menu_state != 0)	return D_O_K;
  
  if (track_info == NULL) 	return D_O_K;
  

  if (track_info->bead_servo_nb >= 0)
    {
      win_printf_OK("Stop servo first for bead %d!",track_info->bead_servo_nb);
      return D_O_K;
    }


  i = win_scanf("Step size %6f Nb of steps %5d Nb.\n of frames in one step %5d\n"
		,&zsize,&nstep, &nf);
  if (i == CANCEL)	return OFF;		
  if (nstep*nf > TRACKING_BUFFER_SIZE)    return win_printf_OK("too many frames!");
  i = (track_info->c_i + 25)%TRACKING_BUFFER_SIZE;
  for (k = 0, l = 0, dz = 0; l < nstep; k++, i++)
    {
      if (k >= nf)
	{
	  k = 0;
	  l++;
	  dz += zsize;
	}
      track_info->obj_pos_cmd[i%TRACKING_BUFFER_SIZE] += dz;
    } 
  return D_O_K;
}


int proceed_bead(b_track *bt, g_track *gt, BITMAP *imb, O_i *oi)
{
  register int i;
  int xc, yc, cl, cw, cl2, di, tmp;
  int dis_filter = 0, black_circle, *xi, *yi;
  float dz, dz_1, zobjn, xcf, ycf, dx, dy, corr, *xf, *yf;
  float ax = 1, ay = 1;
  int red = makecol(255,0,0);
  int green = makecol(0,255,0);
  extern int do_refresh_overlay;

  cl = gt->cl;                   // cross arm length	
  cl2 = cl/2;
  cw = gt->cw;                   // cross arm width	
  xi = gt->xi;	yi = gt->yi;     // these are temporary int buffers for averaged profile
  xf = gt->xf;	yf = gt->yf;     // these are temporary float buffers for averaged profile
  cl2 = cl/2;
  di = find_dialog_focus(active_dialog);                // I think this does not work
  bt->n_l[gt->c_i] = (char)bt->not_lost;
  black_circle = bt->black_circle;
  if (bt->not_lost <= 0)                            // the bead is lost
    { xc = bt->x0;		yc = bt->y0;  }         // last valid position
  else                                              // the bead is not lost
    {	xc = bt->xc;		yc = bt->yc;  } // previous position
  xcf = xc; 	        ycf = yc;
  fill_avg_profiles_from_im(oi, xc, yc, cl, cw, xi, yi);
  if (black_circle)	    erase_around_black_circle(xi, yi, cl);
  tmp = check_bead_not_lost(xi, xf, yi, yf, cl, gt->bd_mul);
  if (bt->not_lost <= 0 && tmp > 0)                 // the bead is still lost
    {
      clip_cross_in_image(oi, bt->xc, bt->yc, cl2);
      if ((imb != NULL) && (win_get_window() == GetForegroundWindow()) && (screen_restoring == 0) ) 
	{
	  acquire_bitmap(imb);
	  draw_cross_in_screen_unit(bt->xc, bt->yc, cl, cw, red, imb);
	  release_bitmap(imb);
	}
      return 1;
    }
  else if (bt->not_lost <= 0 && tmp == 0) 	        // the bead is recovered we reset its state
    bt->not_lost = NB_FRAMES_BEF_LOST;
  else bt->not_lost -= tmp;                         // the bead was not lost
  bt->n_l[gt->c_i] = (char)bt->not_lost;            // we save its present state
  if (tmp == 0)                                     // the bead is present for this frame
    {
      dx = find_distance_from_center(xf, cl, 0, dis_filter, !black_circle,&corr);
      dy = find_distance_from_center(yf, cl, 0, dis_filter, !black_circle,&corr);
      bt->x[gt->c_i] = xcf = dx + xc;               // we save its new x, y position
      bt->y[gt->c_i] = ycf = yc + dy;
      bt->xc = (int)(xcf + .5);	bt->yc = (int)(ycf + .5);
      
      radial_non_pixel_square_image_sym_profile_in_array(bt->rad_prof[gt->c_i], oi, 
							     xcf, ycf, cl>>1, ax, ay);

      if (starting_one == active_dialog && di != index_menu_dialog  && (screen_restoring == 0) 
	  && do_refresh_overlay == 1  && (win_get_window() == GetForegroundWindow()))    
	display_title_message("1 status %u dz %06.4f",_statusfp(),dx); 


      if ((gt->bead_servo_nb == (0x100 + gt->c_b)) || (bt->im_prof_ref < 0))
	{ 
	  //display_title_message("init servo loop");
	  
	  for (i = 0; i < cl;  i++)
	    bt->rad_prof_ref_fft[i] = bt->rad_prof_ref[i] = bt->rad_prof[gt->c_i][i];
	  gt->z_obj_servo_start =read_Z_value();
	  if (fft_band_pass_in_real_out_complex(bt->rad_prof_ref_fft, cl, 0, bt->bp_center, bt->bp_width))
	    return win_printf("pb in filtering!"); 
	  for (i = 0; i < cl;  i++)
	    bt->rad_prof_ref_fft[i] = bt->rad_prof_ref[i];
	  i = gt->c_i - 1;
	  i = (i < 0) ?  TRACKING_BUFFER_SIZE + i : i;
	  gt->obj_pos[i] = gt->z_obj_servo_start;
	  if (fft_band_pass_in_real_out_complex(bt->rad_prof_ref_fft, cl, 0, bt->bp_center, bt->bp_width))
	    return win_printf("pb in filtering!"); 
	  keep = bt->rad_prof_ref[15];
	  keep_1 = bt->rad_prof_ref_fft[15];
	  bt->z[gt->c_i] = 0;
	  if (bt->im_prof_ref < 0)  bt->im_prof_ref = gt->ac_i;
	  else gt->bead_servo_nb = gt->c_b;
	}
      for (i = 0; i < cl;  i++)
	bt->rad_prof_fft[i] = bt->rad_prof[gt->c_i][i];
      dz = find_phase_shift_between_profile(bt->rad_prof_ref_fft, bt->rad_prof_fft, cl, 0, 
						bt->bp_center, bt->bp_width, bt->rc);
      bt->z[gt->c_i] = dz;

      if ((gt->bead_servo_nb&0xff) == gt->c_b )
	{

	  i = gt->c_i - 1;
	  i = (i < 0) ?  TRACKING_BUFFER_SIZE + i : i;
	  zobjn = gt->obj_pos[i];
	  dz_1 = bt->z[i];

	  if (starting_one == active_dialog && di != index_menu_dialog  && (screen_restoring == 0) 
	      && do_refresh_overlay == 1 && (win_get_window() == GetForegroundWindow()))    
	    display_title_message("cl %d bp %d w %d rc %d zobjn = %06.4f dz %06.4f",cl,bt->bp_center, 
	    		  bt->bp_width, bt->rc,zobjn,dz);

	  if (-bt->sat_int < dz && dz < bt->sat_int) bt->integral += bt->integral_gain * dz;
	  if (3*bt->integral >= gt->max_obj_mov) bt->integral = gt->max_obj_mov/3;
	  if (3*bt->integral <= - gt->max_obj_mov) bt->integral = - gt->max_obj_mov/3;

	  zobjn += bt->gain*(dz) + bt->integral + bt->derivative_gain * (dz - dz_1);
	  
	  if (zobjn >= gt->z_obj_servo_start + gt->max_obj_mov) 
	    zobjn = gt->z_obj_servo_start + gt->max_obj_mov;
	  if (zobjn <= gt->z_obj_servo_start - gt->max_obj_mov) 
	    zobjn = gt->z_obj_servo_start - gt->max_obj_mov;
	  set_Z_value(zobjn);
	  gt->obj_pos_cmd[gt->c_i] =zobjn;
	  gt->obj_pos[gt->c_i] = zobjn;
	}
      else 
	{
	  i = gt->c_i - 1;
	  i = (i < 0) ?  TRACKING_BUFFER_SIZE + i : i;
	  gt->obj_pos[gt->c_i] = gt->obj_pos_cmd[i];
	  gt->status_flag[gt->c_i] &= ~OBJ_MOVING;
	}
    }
  else                                              // we save its prevoius position
    {
      bt->x[gt->c_i] = bt->x[(gt->c_i) ? gt->c_i-1 : 0];
      bt->y[gt->c_i] = bt->y[(gt->c_i) ? gt->c_i-1 : 0];
    }
  clip_cross_in_image(oi, bt->xc, bt->yc, cl2);     // we prevent cross to escape from frame
  if ((imb != NULL) && (win_get_window() == GetForegroundWindow()) && (screen_restoring == 0) ) 
    {
      acquire_bitmap(imb);
      draw_cross_in_screen_unit(bt->xc, bt->yc, cl, cw, green, imb);
      release_bitmap(imb);
    }
  return 0;
}

/* this is the function called at each video frame */
int track_in_x_y(imreg *imr, O_i *oi, int n,  int n_in_movie, DIALOG *d, long long t, unsigned long dt, void *p) 
{
  int  cl, cw, cl2, di;
  int dis_filter = 0, *xi, *yi;
  float  *xf, *yf;
  BITMAP *imb = NULL;
  g_track *gt = NULL;
  b_track *bt = NULL;
  extern int do_refresh_overlay;
	/* we grab the general structure */
  
  gt = (g_track*)p;
  cl = gt->cl;                   // cross arm length	
  cl2 = cl/2;
  cw = gt->cw;                   // cross arm width	
  xi = gt->xi;	yi = gt->yi;     // these are temporary int buffers for averaged profile
  xf = gt->xf;	yf = gt->yf;     // these are temporary float buffers for averaged profile
  dis_filter = gt->dis_filter;   // this is the filter description
  switch_frame(oi, n_in_movie);
                                 // we grab the video bitmat of the IFC image
  if ((imr->one_i->bmp.to == IS_BITMAP) &&  (imr->one_i->bmp.stuff != NULL))
    {
      imb = (BITMAP*)imr->one_i->bmp.stuff;
      if ((imb != NULL) && (win_get_window() == GetForegroundWindow())) 
	{   // we only draw on video bitmap is xvin window is on top 
	  acquire_bitmap(imb);   // we must do that since imb is a video buffer   
	  clear_to_color(imb, makecol(255,0,255)); // set the magic overlay color
	  release_bitmap(imb);
	}
    }
  gt->ac_i++;                     // we increment the image number and wrap it eventually
  gt->c_i++;                     // we increment the image number and wrap it eventually
  gt->c_i = (gt->c_i < gt->n_i) ? gt->c_i : 0;

                                 // loop scanning beads tracked
  for (gt->c_b = 0; gt->c_b < gt->n_b; gt->c_b++)
    {
      bt = gt->bd[gt->c_b];
      proceed_bead(bt, gt, imb, oi);
      // we draw bead in green if not lost in red otherwise
      //if (gt->c_b == gt->n_b -1) draw_bubble(screen,0,550,50,"%d xc %f yc %f",gt->c_b,xcf,ycf);	    
    }
  gt->imi[gt->c_i] = n;                                 // we save image" number
  gt->imt[gt->c_i] = t;                                 // image time
  gt->imdt[gt->c_i] = dt;
  /*                               
  gt->zmag[gt->c_i] = (tobe_moved == 0) ? what_is_present_z_mag_value() : read_magnet_z_value(); 
  gt->rot_mag[gt->c_i] = (tobe_turned == 0) ? what_is_present_rot_value() : read_rot_value();
  gt->obj_pos[gt->c_i] = (float)read_Z_value()/10;

  if (gt->zmag_cmd[gt->c_i] != gt->zmag[gt->c_i])    
    {
      set_magnet_z_value(gt->zmag_cmd[gt->c_i]);
      gt->status_flag[gt->c_i] |= ZMAG_MOVING;
    }
  if (gt->rot_mag_cmd[gt->c_i] != gt->rot_mag[gt->c_i])    
    {
      set_rot_value(gt->rot_mag_cmd[gt->c_i]);
      gt->status_flag[gt->c_i] |= ROT_MOVING;
    }

  */
  if (gt->obj_pos_cmd[gt->c_i] != gt->obj_pos[gt->c_i])    
    {
      set_Z_value(gt->obj_pos_cmd[gt->c_i]);
      gt->status_flag[gt->c_i] |= OBJ_MOVING;
    }

  di = find_dialog_focus(active_dialog);                // I think this does not work
  // we only draw on video bitmap is xvin window is on top 
  if (imb && (starting_one == active_dialog) && (di != index_menu_dialog) && (screen_restoring == 0) 
      && (do_refresh_overlay == 1) && (win_get_window() == GetForegroundWindow()))    
    {     // we draw a line indicating the routine time compared with frame time
      //hline(imb,0,0,(imb->w*1000000*dt)/(MY_UCLOCKS_PER_SEC*aquisition_period) ,makecol(255,255,255));
      acquire_bitmap(imb);   // we must do that since imb is a video buffer   
      blit(imb,screen,0,0,imr->x_off + d->x, imr->y_off - imb->h + d->y, imb->w, imb->h); 
      release_bitmap(imb);
    }
  //draw_bubble(screen,0,750,50,"dt %g",1000000*(double)dt/MY_UCLOCKS_PER_SEC);	 
  return 0; 
}
END_OF_FUNCTION(track_in_x_y)

/* start grabing a bead, if mode = IMAGE_CAL, load a calibration image and track x, y, z 
 */

# ifdef ORIGINAL

int z_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  register int i, j;
  static int last_c_i = -1, immax = -1, immax_index = -1;
  d_s *dsx, *dsy, *dsz;
  int bead_num, nf;
  
  
  if (track_info == NULL) return D_O_K;

  dsx = find_source_specific_ds_in_op(op,"X rolling buffer");
  dsy = find_source_specific_ds_in_op(op,"Y rolling buffer");
  dsz = find_source_specific_ds_in_op(op,"Z rolling buffer");
  if (dsx == NULL || dsy == NULL || dsz == NULL) return D_O_K;	
  /*
  if (sscanf(dsx->source,"X rolling buffer bead %d",&bead_num) != 1)
    return win_printf_OK("Cannot find bead number");
  */
  if (track_info->n_b < 1)    return win_printf_OK("No bead to display");
  i = track_info->c_i;
  if (track_info->imi[i] <= immax) return D_O_K;   // we are uptodate
  nf = (track_info->ac_i > TRACKING_BUFFER_SIZE) ? TRACKING_BUFFER_SIZE : track_info->ac_i;
  last_c_i = track_info->c_i;
  for (i = nf-1, j = last_c_i; i >= 0; i--, j--)
    {
      j = (j < 0) ? TRACKING_BUFFER_SIZE - j: j;
      dsx->xd[i] = dsy->xd[i] = dsz->xd[i] = track_info->imi[j];
      dsx->yd[i] = track_info->bd[0]->x[j];
      dsy->yd[i] = track_info->bd[0]->y[j];
      dsz->yd[i] = track_info->bd[0]->z[j];
    }
  op->need_to_refresh = 1;                
  return refresh_plot(pr_IFC, UNCHANGED);
}

# endif


int z_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  register int i, j, k;
  static int last_c_i = -1, immax = -1;
  d_s *dsx, *dsy, *dsz;
  int nf, last_fr = 0, lost_fr;
  unsigned long  dtm, dtmax;

  if (track_info == NULL) return D_O_K;

  dsx = find_source_specific_ds_in_op(op,"X rolling buffer");
  dsy = find_source_specific_ds_in_op(op,"Y rolling buffer");
  dsz = find_source_specific_ds_in_op(op,"Z rolling buffer");
  if (dsx == NULL || dsy == NULL || dsz == NULL) return D_O_K;	
  /*
  if (sscanf(dsx->source,"X rolling buffer bead %d",&bead_num) != 1)
    return win_printf_OK("Cannot find bead number");
  */
  if (track_info->n_b < 1)    return win_printf_OK("No bead to display");
  i = track_info->c_i;
  if (track_info->imi[i] <= immax) return D_O_K;   // we are uptodate
  immax = track_info->imi[i];               
  nf = (track_info->ac_i > TRACKING_BUFFER_SIZE) ? TRACKING_BUFFER_SIZE : track_info->ac_i;
  dsx->nx = dsx->ny = dsy->nx = dsy->ny = dsz->nx = dsz->ny = nf;
  last_c_i = track_info->c_i;
  for (i = nf-1, j = last_c_i, lost_fr = 0, dtm = dtmax = 0, k = 0; i >= 0; i--, j--, k++)
    {
      j = (j < 0) ? TRACKING_BUFFER_SIZE + j: j;
      j = (j < TRACKING_BUFFER_SIZE) ? j : TRACKING_BUFFER_SIZE;
      dsx->xd[i] = dsy->xd[i] = dsz->xd[i] = track_info->imi[j];
      dsx->yd[i] = track_info->bd[0]->x[j];
      dsy->yd[i] = track_info->bd[0]->y[j];
      dsz->yd[i] = track_info->bd[0]->z[j];
      if (i < nf-1) lost_fr += (last_fr - track_info->imi[j]) - 1;
      last_fr = track_info->imi[j];
      dtm += track_info->imdt[j];
      dtmax = (track_info->imdt[j] > dtmax) ? track_info->imdt[j] : dtmax;
    }
  op->need_to_refresh = 1; 
  if (k) set_plot_title(op, "\\stack{{%d missed images}{\\pt8 dt mean %6.3f ms max %6.3f}}",lost_fr,
			1000*((double)(dtm/k))/get_my_uclocks_per_sec(),1000*((double)(dtmax))/get_my_uclocks_per_sec());
  i = TRACKING_BUFFER_SIZE/4;
  j = ((int)dsx->xd[0])/i;
  op->x_lo = j*i;
  op->x_hi = (j+5)*i;
  set_plot_x_fixed_range(op);
  return refresh_plot(pr_IFC, UNCHANGED);
}

int timing_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  register int i, j, k;
  static int last_c_i = -1, immax = -1, ac_imax = -1;
  static unsigned long last_refresh = 0;
  d_s *dsx, *dsy, *dsz;
  int nf, last_fr = 0, lost_fr;
  unsigned long   dtm, dtmax;
  long long last_ti = 0;

  i = (disp_IFC.ac_i-1)%TRACKING_BUFFER_SIZE;
  if (min_plot_refresh == 0)  min_plot_refresh = get_my_uclocks_per_sec()/15;
  if (disp_IFC.ac_i <= ac_imax)   return D_O_K;   // we are uptodate
  //if (my_uclock() < last_refresh) return D_O_K;   // no need to uptodate too quicly
  dsx = find_source_specific_ds_in_op(op,"Timing rolling buffer");
  dsy = find_source_specific_ds_in_op(op,"Period rolling buffer");
  dsz = find_source_specific_ds_in_op(op,"Timer rolling buffer");
  if (dsx == NULL || dsy == NULL || dsz == NULL) return D_O_K;	
  immax = disp_IFC.imi[i];               
  nf = (disp_IFC.ac_i > TRACKING_BUFFER_SIZE) ? TRACKING_BUFFER_SIZE : disp_IFC.ac_i;

  for (i = 0, j = disp_IFC.ac_i, k = j%TRACKING_BUFFER_SIZE, dtm = dtmax = 0, lost_fr = 0; i < nf; )
    {
      if (i == 0)
	{
	  dtm = dtmax = 0;
	  ac_imax = disp_IFC.ac_i - 1;
	  k = ac_imax%TRACKING_BUFFER_SIZE;
	  immax = disp_IFC.imi[k];
	  j = (disp_IFC.ac_i > TRACKING_BUFFER_SIZE) ? disp_IFC.ac_i : 0;
	  k = j%TRACKING_BUFFER_SIZE;
	  last_ti = disp_IFC.imt[k];
	  last_fr = disp_IFC.imi[k];
	  nf = (disp_IFC.ac_i > TRACKING_BUFFER_SIZE) ? TRACKING_BUFFER_SIZE : disp_IFC.ac_i;
	  nf -= 10;
	}
      dsx->xd[i] = dsy->xd[i] = disp_IFC.imi[k];
      dsx->yd[i] = 1000*(float)disp_IFC.imdt[k]/get_my_uclocks_per_sec(); 
      dsy->yd[i] = 1000*(float)(disp_IFC.imt[k] - last_ti)/get_my_ulclocks_per_sec(); 
      dsy->yd[i] = (dsy->yd[i] < 0) ? 0 : dsy->yd[i];
      if ((disp_IFC.imi[k] - last_fr) - 1 > 0)	lost_fr += (disp_IFC.imi[k] - last_fr) - 1;
      last_ti = disp_IFC.imt[k];
      last_fr = disp_IFC.imi[k];
      dtm += disp_IFC.imdt[k];
      dtmax = (disp_IFC.imdt[k] > dtmax) ? disp_IFC.imdt[k] : dtmax;
      i++;
      j++;
      if (disp_IFC.ac_i >= j)  	  i = 0;   // we have been overrun, we restart
      else k = j%TRACKING_BUFFER_SIZE;
    }
  dsx->nx = dsx->ny = dsy->nx = dsy->ny  = nf;
  //if (lost_fr) 
  //display_title_message("fr lost %d at i = %d j = %d",(last_fr - disp_IFC.imi[k]) - 1,i,k);


# ifdef OLD

  nf_safe = nf - 64;
  if (nf_safe < 0) return D_O_K;
  dsx->nx = dsx->ny = dsy->nx = dsy->ny  = nf_safe + 1;
  last_c_i = (disp_IFC.c_i + TRACKING_BUFFER_SIZE - 1)%TRACKING_BUFFER_SIZE;
  for (i = nf_safe, j = last_c_i, lost_fr = 0, dtm = dtmax = 0, k = 0; i >= 0; i--, j--, k++)
    {
      j = (j+TRACKING_BUFFER_SIZE )%TRACKING_BUFFER_SIZE;
      //j = (j < 0) ? TRACKING_BUFFER_SIZE + j: j;
      //j = (j < TRACKING_BUFFER_SIZE) ? j : TRACKING_BUFFER_SIZE-1;
      dsx->xd[i] = dsy->xd[i] = disp_IFC.imi[j];
      dsx->yd[i] = 1000*(float)disp_IFC.imdt[j]/get_my_uclocks_per_sec(); 
      if (i < nf_safe) 
	{
	  dsy->yd[i] = 1000*(float)(last_ti - disp_IFC.imt[j])/get_my_ulclocks_per_sec(); 
	  dsy->yd[i] = (dsy->yd[i] < 0) ? 0 : dsy->yd[i];
	  if ((last_fr - disp_IFC.imi[j]) - 1 > 0)
	      lost_fr += (last_fr - disp_IFC.imi[j]) - 1;
	  if (lost_fr) display_title_message("fr lost %d at i = %d j = %d",(last_fr - disp_IFC.imi[j]) - 1,i,j);
	}
      last_ti = disp_IFC.imt[j];
      last_fr = disp_IFC.imi[j];
      dtm += disp_IFC.imdt[j];
      dtmax = (disp_IFC.imdt[j] > dtmax) ? disp_IFC.imdt[j] : dtmax;
    }
   /*
  for (i = 0; i < 4096; i++)
      dsz->yd[i] = 0;
  for (i = 0; i < 4096 && i < disp_IFC.c_i; i++)
    {
      dsz->xd[i] = tim_im[i];
      dsz->yd[i]++;
    }
  dsz->nx = dsz->ny  = (n_tim < 4096) ? n_tim : 4096;
  */

# endif

  op->need_to_refresh = 1; 
  if (nf-1) set_plot_title(op, "\\stack{{%d missed images, total %d}{\\pt8 dt mean %6.3f ms max %6.3f}}",
			lost_fr,disp_IFC.lost_fr,1000*((double)(dtm/k))/get_my_uclocks_per_sec(),
			1000*((double)(dtmax))/get_my_uclocks_per_sec());
  i = TRACKING_BUFFER_SIZE/4;
  j = ((int)dsx->xd[0])/i;
  op->x_lo = j*i;
  op->x_hi = (j+5)*i;
  set_plot_x_fixed_range(op);
  refresh_plot(pr_IFC, UNCHANGED);
  last_refresh = my_uclock() + min_plot_refresh;
  return 0;
}


int obj_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  register int i, j;
  static int last_c_i = -1, immax = -1;
  d_s *dsx, *dsz;
  int nf;
  
  if (track_info == NULL) return D_O_K;

  dsx = find_source_specific_ds_in_op(op,"Obj. rolling buffer");
  dsz = find_source_specific_ds_in_op(op,"Z rolling buffer");
  if (dsx == NULL || dsz == NULL) return D_O_K;	

  if (track_info->n_b < 1)    return win_printf_OK("No bead to display");
  i = track_info->c_i;
  if (track_info->imi[i] <= immax) return D_O_K;   // we are uptodate
  immax = track_info->imi[i];               
  nf = (track_info->ac_i > TRACKING_BUFFER_SIZE) ? TRACKING_BUFFER_SIZE : track_info->ac_i;
  dsx->nx = dsx->ny = dsz->nx = dsz->ny = nf;
  last_c_i = track_info->c_i;
  for (i = nf-1, j = last_c_i; i >= 0; i--, j--)
    {
      j = (j < 0) ? TRACKING_BUFFER_SIZE + j: j;
      j = (j < TRACKING_BUFFER_SIZE) ? j : TRACKING_BUFFER_SIZE;
      dsx->xd[i] = dsz->xd[i] = track_info->imi[j];
      dsx->yd[i] = track_info->obj_pos[j];
      dsz->yd[i] = track_info->bd[0]->z[j];
    }
  op->need_to_refresh = 1; 
  // set_plot_title(op, "\\stack{{image %d bead servo %d}{Obj start %g obj %g}}"
  //	 ,track_info->ac_i,  track_info->bead_servo_nb,
  //		 track_info->z_obj_servo_start,
  //		 track_info->obj_pos[last_c_i]);
  return refresh_plot(pr_IFC, UNCHANGED);
}


int profile_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  register int i;
  static int last_c_i = -1, immax = -1;
  d_s *ds, *dsr;
  float dz;
  
  if (track_info == NULL) return D_O_K;

  ds = find_source_specific_ds_in_op(op,"Instantaneous radial profile");
  dsr = find_source_specific_ds_in_op(op,"Reference radial profile");
  if (ds == NULL || dsr == NULL) return D_O_K;	

  if (track_info->n_b < 1)    return win_printf_OK("No bead to display");
  i = track_info->c_i;
  if (track_info->imi[i] <= immax) return D_O_K;   // we are uptodate
  immax = track_info->imi[i];               
  last_c_i = track_info->c_i;
  for (i = 0; i < track_info->cl && i < ds->nx ; i++)
    {
      ds->xd[i] = dsr->xd[i] = i;
      ds->yd[i] = track_info->bd[0]->rad_prof[last_c_i][i];
      dsr->yd[i] = track_info->bd[0]->rad_prof_ref[i];
    }



  if (fft_band_pass_in_real_out_complex(dsr->yd, track_info->cl, 0, 
	 track_info->bd[0]->bp_center, track_info->bd[0]->bp_width))
    return win_printf("pb in filtering!"); 

  dz = find_phase_shift_between_profile(dsr->yd, ds->yd, track_info->cl, 0, 
	 track_info->bd[0]->bp_center, track_info->bd[0]->bp_width, track_info->bd[0]->rc);

  for (i = 0; i < track_info->cl && i < ds->nx ; i++)
    {
      ds->xd[i] = dsr->xd[i] = i;
      ds->yd[i] = track_info->bd[0]->rad_prof[last_c_i][i];
      dsr->yd[i] = track_info->bd[0]->rad_prof_ref[i];
    }


  op->need_to_refresh = 1; 
  set_plot_title(op, "\\stack{{image %d bead servo %d}{Obj start %6.3f dz %6.3f obj %6.3f}}"
		 ,track_info->ac_i,  track_info->bead_servo_nb,
		 track_info->z_obj_servo_start,dz, //track_info->bd[0]->z[last_c_i],
		 track_info->obj_pos[last_c_i]);
  return refresh_plot(pr_IFC, UNCHANGED);
}


int follow_bead_in_x_y(void)
{
  register int i, di;
  imreg *imrs;	
  O_p *op;
  d_s *ds;
  O_i *ois, *oi = NULL;
  int xc = 256, yc = 256, color, junk, junk2, bnx = 0, bcw = 0, dis_state;
  static int cl = 64, cw = 16, nf = 2048, imstart = 10, prof = 0;
  b_track *bd = NULL;
  int mode = 0;
  float xb = 0, yb = 0, zb = 0, zobj;
	
	
  if(updating_menu_state != 0)	return D_O_K;
  di = find_dialog_focus(active_dialog);	
  mode = RETRIEVE_MENU_INDEX;
  if (ac_grep(NULL,"%im%oi",&imrs,&ois) != 2)	
    return win_printf_OK("cannot find image in acreg!");

  //if (ois != oi_IFC) return win_printf_OK("this is not the IFC image!");
  
  dis_state = do_refresh_overlay;   do_refresh_overlay = 0;
  //freeze_video();
  color = makecol(255,64,64);
  junk = (int)MY_UCLOCKS_PER_SEC;
  junk2 = aquisition_period;
  /* check for menu in win_scanf */
  if (mode == IMAGE_CAL)
    {
      //freeze_video();
      oi = do_load_calibration(0);
      if (grab_bead_info_from_calibration(oi, &xb, &yb, &zb, &bnx, &bcw))
	win_printf("error in retrieving bead data");
      cl = bnx;
      cw = bcw;
      //live_video(0);
    }
  else
    {
      i = win_scanf("this routine track a bead in X,Y\n"
		    " using a cross shaped pattern \n"
		    "arm length %3d arm width %3d\n"
		    "how many frames %6d imstart %2d\n"
		    "profile %b\nkey {\\it b} black circle {\\it w} to stop\njunk %djunk %d"
		    ,&cl,&cw,&nf,&imstart,&prof,&junk,&junk2); /* %m ,select_power_of_2() */
      if (i == CANCEL)	return OFF; 
    }
  if (cl > 128)	return win_printf_OK("cannot use arm bigger\nthan 128");
  if (cw > 32)	return win_printf_OK("cannot use width bigger\nthan 32");
  
  if (fft_init(cl) || filter_init(cl))	return win_printf_OK("cannot init fft!");	
  do_refresh_overlay = dis_state; 
  
  //live_video(0);
  xc = yc = 0;
  if ( mode == IMAGE_CAL)
    {
      xc = (int)(xb - oi->ax)/oi->dx;
      yc = (int)(yb - oi->ay)/oi->dy;
    }
  else
    {
      for(change_mouse_to_rect(cl, cl); !key[KEY_ENTER]; )
	{	
	  xc = mouse_x;
	  yc = mouse_y;
	}
      reset_mouse();
      xc = (int)x_imr_2_imdata(imrs, xc);	
      yc = (int)y_imr_2_imdata(imrs, yc);	
      clear_keybuf();		
    }					

  
  if (track_info == NULL)
    {
      track_info = (g_track*)calloc(1,sizeof(g_track));
      if (track_info == NULL) return win_printf_OK("cannot alloc track info!");	
      track_info->cl = cl;	    
      track_info->cw = cw;	    
      track_info->bd_mul = 16;	    
      track_info->dis_filter = cl>>2;
      track_info->m_b = 16;	    
      track_info->n_b = track_info->c_b = 0;
      track_info->bead_servo_nb = -1;
      track_info->bd = (b_track**)calloc(track_info->m_b,sizeof(b_track*));
      if (track_info->bd == NULL) return win_printf_OK("cannot alloc track info!");	
      starting_one = active_dialog; // to check if windows change
      index_menu_dialog = retrieve_item_from_dialog(d_menu_proc, NULL);
      index_menu_dialog = 1;
      track_info->user_tracking_action = NULL;

      op = pr_IFC->o_p[0];
      ds = op->dat[0];
      set_ds_source(ds, "X rolling buffer");
      if ((ds = create_and_attach_one_ds(op, TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0)) == NULL)
		return win_printf_OK("I can't create plot !");
      set_ds_source(ds, "Y rolling buffer");
      if ((ds = create_and_attach_one_ds(op, TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0)) == NULL)
		return win_printf_OK("I can't create plot !");
      set_ds_source(ds, "Z rolling buffer");
      op->op_idle_action = z_rolling_buffer_idle_action;
      //win_printf("index %d %d",index_menu_dialog,di);
      op = create_and_attach_one_plot(pr_IFC, track_info->cl, track_info->cl, 0);
      ds = op->dat[0];
      set_ds_source(ds, "Instantaneous radial profile");
      if ((ds = create_and_attach_one_ds(op,  track_info->cl, track_info->cl, 0)) == NULL)
		return win_printf_OK("I can't create plot !");
      set_ds_source(ds, "Reference radial profile");
      op->op_idle_action = profile_rolling_buffer_idle_action;

      op = create_and_attach_one_plot(pr_IFC,  TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0);
      ds = op->dat[0];
      set_ds_source(ds, "Obj. rolling buffer");
      if ((ds = create_and_attach_one_ds(op, TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0)) == NULL)
		return win_printf_OK("I can't create plot !");
      set_ds_source(ds, "Z rolling buffer");
      op->op_idle_action = obj_rolling_buffer_idle_action;
      for (i = 0, zobj = read_Z_value() ; i < TRACKING_BUFFER_SIZE; i++) // 
	track_info->obj_pos_cmd[i] = zobj;



      op = create_and_attach_one_plot(pr_IFC,  TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0);
      ds = op->dat[0];
      set_ds_source(ds, "Timing rolling buffer");
      if ((ds = create_and_attach_one_ds(op, TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0)) == NULL)
		return win_printf_OK("I can't create plot !");
      set_ds_source(ds, "Period rolling buffer");
      if ((ds = create_and_attach_one_ds(op, TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0)) == NULL)
		return win_printf_OK("I can't create plot !");
      set_ds_source(ds, "Timer rolling buffer");
      op->op_idle_action = timing_rolling_buffer_idle_action;

      track_info->m_i = track_info->n_i = TRACKING_BUFFER_SIZE;
      track_info->c_i = 0;
      LOCK_FUNCTION(track_in_x_y_IFC);
      LOCK_FUNCTION(track_in_x_y_timer);
      LOCK_VARIABLE(track_info);
    }
  else
    {
      if (track_info->cl != cl || track_info->cw != cw)
	{
	  win_printf("Your arms do not have the right size!\n");
	  if (oi) free_one_image(oi);
	  return D_O_K;
	}	    
    }
  bd = (b_track*)calloc(1,sizeof(b_track));
  if (bd == NULL) return win_printf_OK("cannot alloc bead info!");	
  if (track_info->n_b >= track_info->m_b)
    {
      track_info->m_b += 16;	    
      track_info->bd = (b_track**)realloc(track_info->bd,track_info->m_b*sizeof(b_track*));
      if (track_info->bd == NULL) return win_printf_OK("cannot alloc track info!");	
    }
  for (i = 0; i < track_info->cl; i++) bd->rad_prof_ref[i] = -1; // cannot be a profile
  
  if (mode == IMAGE_CAL)
    {
      bd->calib_im = oi;
      bd->calib_im_fil = NULL;
    }
  else 
    {
      bd->calib_im = NULL;
      bd->calib_im_fil = NULL;
    }
  
  track_info->bd[track_info->n_b++] = bd;
  LOCK_VARIABLE(bd);
  bd->xc = bd->x0 = xc;
  bd->yc = bd->y0 = yc;
  bd->im_prof_ref = -1;
  bd->not_lost = NB_FRAMES_BEF_LOST;
  disp_IFC.param = (void*)track_info;
  disp_IFC.to_do = track_in_x_y;
  //disp_IFC.timer_do = track_in_x_y_timer;
  return 0;
}


int draw_bead_in_x_y(void)
{
	register int i;
	imreg *imrs;	
	O_i *ois;
	int imstart = 0, lc_i = 0;
 	int im;
	static int nf = 2048;
	b_track *bt = NULL;
	unsigned long dtr = MY_UCLOCKS_PER_SEC;	    
	
	if(updating_menu_state != 0)	return D_O_K;
	
	if (ac_grep(NULL,"%im%oi",&imrs,&ois) != 2)	
		return win_printf_OK("cannot find image in acreg!");

	//if (ois != oi_IFC) return win_printf_OK("this is not the IFC image!");

	if (track_info == NULL) return win_printf_OK("No bead selected!");

	freeze_video();
	i = win_scanf("For how many frames %d",&nf);
	if (i == CANCEL) return  live_video(0);
	live_video(0);
	imstart = track_info->imi[lc_i = track_info->c_i];

	for (im = imstart; im < imstart + nf;  )
	  {
	    if (lc_i != track_info->c_i)
	      {
		lc_i++;
		lc_i = (lc_i < track_info->n_i) ? lc_i : 0;
		draw_bubble(screen,0,550,30,"dt %g ",1000000*(float)(track_info->imdt[lc_i])/dtr);   
		//1000000*(((float)track_info->imdt[lc_i])/MY_UCLOCKS_PER_SEC));	    
		for (track_info->c_b = 0; track_info->c_b < track_info->n_b; track_info->c_b++)
		  {
		    bt = track_info->bd[track_info->c_b];
		    draw_bubble(screen,0,550,50+track_info->c_b*20,"%d xc %f yc %f rad_prof",track_info->c_b,bt->x[lc_i],bt->y[lc_i]);	    
		  }
		im++;
	      }
	  }
	return 0;

}




int get_tracking_points(void)
{     
       O_i *oi = NULL;
       O_p *op = NULL;
       d_s *ds_x = NULL, *ds_y = NULL, *ds_z = NULL;
       int i = 0;
       imreg *imr = NULL;
       
       if(updating_menu_state != 0)	return D_O_K;


	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)
		return win_printf("cannot find image!");

	if (track_info == NULL) return win_printf_OK("No bead selected!");


       op = create_and_attach_op_to_oi(oi, TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0,0);
       if (op == NULL)	return win_printf("cannot create plot!");
       
       ds_x = op->dat[0];
       ds_y = create_and_attach_one_ds(op,TRACKING_BUFFER_SIZE,TRACKING_BUFFER_SIZE,0);
       if (ds_y == NULL) return win_printf("Cannot create data set");
       ds_z = create_and_attach_one_ds(op,track_info->cl,track_info->cl,0);
       if (ds_z == NULL) return win_printf("Cannot create data set");
       
       for (i = 0 ; i < TRACKING_BUFFER_SIZE; i++)
       {
	 ds_x->xd[i] = ds_y->xd[i] = /*1000*/track_info->imi[i];//t[i]/MY_UCLOCKS_PER_SEC;
	 ds_x->yd[i] = track_info->bd[0]->x[i];
	 ds_y->yd[i] = track_info->bd[0]->y[i];
       }

       for (i = 0 ; i < track_info->cl; i++)
       {
	 ds_z->xd[i] = i;
	 ds_z->yd[i] = track_info->bd[0]->rad_prof[track_info->c_i][i];
       }
       return 0;
}



int fft_band_pass_in_real_out_complex(float *x, int nx, int w_flag, int fc, 
	int width)
{
	register int i, j;
	float  moy, smp = 0.05;

	/* remove dc based upon the first and the last quarter of data */
	/*	we assume a peak in the middle !*/	
	for(i = 0, j = (3*nx)/4, moy = 0; i < nx/4; i++, j++)		
		moy += x[i] + x[j];	
	for(i = 0, moy = (2*moy)/nx; i < nx; i++)		x[i] -= moy;	
	if (w_flag)		fftwindow1(nx, x, smp);
	realtr1(nx, x);		fft(nx, x, 1);	realtr2(nx, x, 1);
	bandpass_smooth_half (nx, x, fc, width);
	fft(nx, x, -1);
	if (w_flag)		defftwc1(nx, x, smp);
	return 0;	
}

float 	find_phase_shift_between_profile(float *zr, float *zp, int npc, 
	int flag, int filter, int width, int rc)
{
	register int i;
	float im, re, amp, phi, dphi, w;
	static float *tmp = NULL, *tmp1 = NULL;
	static int ntmp = 0;
	int nx, onx;
		
	
	if (zr == NULL || zp == NULL)	return WRONG_ARGUMENT;
	nx = npc;
	if (fft_init(nx) || filter_init(nx))			return FFT_NOT_SUPPORTED;
	if (tmp == NULL || ntmp != nx)
	{
		tmp = (float *)calloc(nx,sizeof(float));
		if (tmp == NULL)	return -1;
		ntmp = nx;
		tmp1 = tmp + nx/2;
	}
	fft_band_pass_in_real_out_complex(zp, nx, flag, filter, width);
	for (i=0; i< nx/2; i++)
	{
		re = zr[2*i] * zp[2*i] + zp[2*i+1] * zr[2*i+1];
		im = zp[2*i+1] * zr[2*i] - zp[2*i] * zr[2*i+1];
		tmp[i] = (im == 0.0 && re == 0.0) ? 0.0 : atan2(im,re);
	}
	onx = nx/2;
	tmp1[onx/2] = tmp[onx/2];
	for (i=onx/2+rc/2, phi = 0; i< onx; i++)
	{
		dphi = tmp[i] - tmp[i-1];
		if (dphi > M_PI)	phi -= 2*M_PI;
		if (dphi < -M_PI)	phi += 2*M_PI;
		tmp1[i] = tmp[i] + phi;
	}		
	for (i=onx/2-rc/2, phi = 0; i>= 0; i--)
	{
		dphi = tmp[i] - tmp[i+1];
		if (dphi > M_PI)	phi -= 2*M_PI;
		if (dphi < -M_PI)	phi += 2*M_PI;
		tmp1[i] = tmp[i] + phi;
	}	
	for (i=onx/2+rc, phi = 0, w = 0; i< onx; i++)			
	{
		amp = zp[2*i] * zp[2*i];
		amp += zp[2*i+1] * zp[2*i+1];
		w += amp;
		phi += amp*tmp1[i];
	}
	return  (w != 0.0) ? phi/w : phi;
}




/*	take a radial image profile of an image with a different scaling in 
 *	x and y, real x = i_image * ax,  y = i_image * ay
 *
 *
 *
 */

float *radial_non_pixel_square_image_sym_profile_in_array(float *rz, O_i *ois, float xc, float yc, int r_max, float ax, float ay)
{
  int i, j;
  union pix *ps;
  int onx, ony, ri, type;
  float z = 0, z1, x, y, r, p, p0 = 0, pr0 = 0;
  int xci, yci, imin, imax, jmin,jmax;
  
  for (i = 0; i < 2*r_max && i < 256; i++)	radial_dx[i] = 0;
  if (ois == NULL)	return NULL;
  if (rz == NULL)		rz = (float*)calloc(2*r_max,sizeof(float));
  if (rz == NULL)		return NULL;
  ps = ois->im.pixel;
  ony = ois->im.ny;	onx = ois->im.nx;
  type = ois->im.data_type;
  for (i = 0; i < 2*r_max; i++)	rz[i] = 0;
  xci = (int)xc;
  yci = (int)yc;
  
  imin = (int)(yc - (r_max/ay));
  imax = (int)(0.5 + yc + (r_max/ay));	
  jmin = (int)(xc -  r_max/ax);
  jmax = (int)(0.5 + xc + r_max/ax);
  imin = (imin < 0) ? 0 : imin;
  imax = (imax > ony) ? ony : imax;
  jmin = (jmin < 0) ? 0 : jmin;
  jmax = (jmax > onx) ? onx : jmax;	
  if (type == IS_COMPLEX_IMAGE)
    {  
      for (i = imin; i < imax; i++)
	{
	  y = ay * ((float)i - yc);
	  y *= y;
	  for (j = jmin; j < jmax; j++)
	    {
	      x = ax * ((float)j - xc);
	      x *= x;
	      r = (float)sqrt(x + y);
	      ri = (int)r;
	      if (ri > r_max)	continue;	/* bug break;*/
	      if ( ois->im.mode == RE)
		z = ps[i].fl[2*j];
	      else if ( ois->im.mode == IM)
		z = ps[i].fl[2*j+1];
	      else if ( ois->im.mode == AMP)
		{
		  z = ps[i].fl[2*j];
		  z1 = ps[i].fl[2*j+1];
		  z = sqrt(z*z + z1*z1);
		}
	      else if ( ois->im.mode == AMP_2)
		{
		  z = ps[i].fl[2*j];
		  z1 = ps[i].fl[2*j+1];
		  z = z*z + z1*z1;
		}				
	      else if ( ois->im.mode == LOG_AMP)
		{
		  z = ps[i].fl[2*j];
		  z1 = ps[i].fl[2*j+1];
		  z = z*z + z1*z1;
		  z = (z > 0) ? log10(z) : -40.0;
		}				
	      else
		{
		  win_printf("Unknown mode for complex image");
		  return NULL;
		}
	      p = r - ri;
	      if (ri == r_max)
		{
		  p0 += (1-p)*z;
		  pr0 += (1-p);
		  radial_dx[r_max] -= 1-p;
		}
	      else 
		{
		  rz[ri] += (1-p)*z;
		  rz[ri+r_max] += (1-p);
		  radial_dx[ri] += p;
		}
	      ri++;
	      if (ri < r_max)
		{
		  rz[ri] += p*z;
		  rz[ri+r_max] += p;
		  radial_dx[ri] -= 1-p;
		}
	    }
	}
    }
  else if (type == IS_FLOAT_IMAGE) 		
    {
      for (i = imin; i < imax; i++)
	{
	  y = ay * ((float)i - yc);
	  y *= y;
	  for (j = jmin; j < jmax; j++)
	    {
	      x = ax * ((float)j - xc);
	      x *= x;
	      r = (float)sqrt(x + y);
	      ri = (int)r;
	      if (ri > r_max)	continue;	/* bug break;*/
	      z = ps[i].fl[j];
	      p = r - ri;
	      if (ri == r_max)
		{
		  p0 += (1-p)*z;
		  pr0 += (1-p);
		  radial_dx[r_max] -= 1-p;
		}
	      else 
		{
		  rz[ri] += (1-p)*z;
		  rz[ri+r_max] += (1-p);
		  radial_dx[ri] += p;
		}
	      ri++;
	      if (ri < r_max)
		{
		  rz[ri] += p*z;
		  rz[ri+r_max] += p;
		  radial_dx[ri] -= 1-p;
		}
	    }
	}
    }
  else if (type == IS_INT_IMAGE)			
    {
      for (i = imin; i < imax; i++)
	{
	  y = ay * ((float)i - yc);
	  y *= y;
	  for (j = jmin; j < jmax; j++)
	    {
	      x = ax * ((float)j - xc);
	      x *= x;
	      r = (float)sqrt(x + y);
	      ri = (int)r;
	      if (ri > r_max)	continue;	/* bug break;*/
	      z = (float)ps[i].in[j];
	      p = r - ri;
	      if (ri == r_max)
		{
		  p0 += (1-p)*z;
		  pr0 += (1-p);
		  radial_dx[r_max] -= 1-p;
		}
	      else 
		{
		  rz[ri] += (1-p)*z;
		  rz[ri+r_max] += (1-p);
		  radial_dx[ri] += p;
		}
	      ri++;
	      if (ri < r_max)
		{
		  rz[ri] += p*z;
		  rz[ri+r_max] += p;
		  radial_dx[ri] -= 1-p;
		}
	    }
	}
    }
  else if (type == IS_CHAR_IMAGE)			
    {
      for (i = imin; i < imax; i++)
	{
	  y = ay * ((float)i - yc);
	  y *= y;
	  for (j = jmin; j < jmax; j++)
	    {
	      x = ax * ((float)j - xc);
	      x *= x;
	      r = (float)sqrt(x + y);
	      ri = (int)r;
	      if (ri > r_max)	continue;	/* bug break;*/
	      z = (float)ps[i].ch[j];
	      p = r - ri;
	      if (ri == r_max)
		{
		  p0 += (1-p)*z;
		  pr0 += (1-p);
		  radial_dx[r_max] -= 1-p;
		}
	      else 
		{
		  rz[ri] += (1-p)*z;
		  rz[ri+r_max] += (1-p);
		  radial_dx[ri] += p;
		}
	      ri++;
	      if (ri < r_max)
		{
		  rz[ri] += p*z;
		  rz[ri+r_max] += p;
		  radial_dx[ri] -= 1-p;
		}
	    }
	}
    }
  for (i = 0; i < r_max; i++)
    {
      rz[i] = (rz[i+r_max] == 0) ? 0 : rz[i]/rz[i+r_max];
      radial_dx[i] = (rz[i+r_max] == 0) ? 0 : radial_dx[i]/rz[i+r_max];
    }
  p0 = (pr0 == 0) ? 0 : p0/pr0;  
  for (i = 0; i < r_max; i++)		rz[i+r_max] = rz[i]; 
  for (i = 1; i < r_max; i++)		rz[r_max-i] = rz[r_max+i]; 
  rz[0] = p0;
  return rz;
}

/*	take a radial image profile of an image with a different scaling in 
 *	x and y, real x = i_image * ax,  y = i_image * ay
 *
 *      use smid P4 instruction
 *
 */

float *radial_non_pixel_square_image_sym_profile_in_array_sse(float *rz, O_i *ois, float xc, float yc, int r_max, float ax, float ay)
{
  int i, j;
  union pix *ps;
  int onx, ony, ri, type, onx_4;
  float z = 0, *r, p, p0 = 0, pr0 = 0, rmax2, tmp; // , z1, x, y
  int xci, yci, imin, imax, jmin, jmax, jmin_4, jmax_4;
  
  
  
  for (i = 0; i < 2*r_max && i < 256; i++)	radial_dx[i] = 0;
  if (ois == NULL)	return NULL;
  if (rz == NULL)		rz = (float*)calloc(2*r_max,sizeof(float));
  if (rz == NULL)		return NULL;
  ps = ois->im.pixel;
  ony = ois->im.ny;	onx = ois->im.nx;
  onx_4 = onx >> 2;
  type = ois->im.data_type;
  for (i = 0; i < 2*r_max; i++)	rz[i] = 0;
  rmax2 = r_max * r_max;
  xci = (int)xc;
  yci = (int)yc;
  imin = (int)(yc - (r_max/ay));
  imax = (int)(0.5 + yc + (r_max/ay));	
  jmin = (int)(xc -  r_max/ax);
  jmax = (int)(0.5 + xc + r_max/ax);
  imin = (imin < 0) ? 0 : imin;
  imax = (imax > ony) ? ony : imax;
  jmin = (jmin < 0) ? 0 : jmin;
  jmax = (jmax > onx) ? onx : jmax;
  //	jmin_4 = jmin = jmin >> 2;	jmin <<= 2;
  //jmax_4 = jmax = jmax >> 2;	jmax++; jmax <<= 2;
  jmin_4 = jmin >> 2;
  jmax_4 = 1+(jmax >> 2);
  jmax_4 = (jmax_4 > onx_4) ? onx_4 : jmax_4;
  
  
  /*win_printf("Imin %d imax %d, jmin %d jmax %d\njmin_4 %d jmax_4 %d"
    ,imin,imax,jmin,jmax,jmin_4,jmax_4); */
  
  fxc.f[0] = fxc.f[1] = fxc.f[2] = fxc.f[3] = xc;
  fyc.f[0] = fyc.f[1] = fyc.f[2] = fyc.f[3] = yc;
  fax.f[0] = fax.f[1] = fax.f[2] = fax.f[3] = ax;
  fay.f[0] = fay.f[1] = fay.f[2] = fay.f[3] = ay;
  f4.f[0] = f4.f[1] = f4.f[2] = f4.f[3] = 4;
  r = fr[0].f;
  if (type == IS_INT_IMAGE)			
    {
      f0.f[0] = 0; f0.f[1] = 1; f0.f[2] = 2; f0.f[3] = 3;  // pixel index 
      fx.v = __builtin_ia32_subps(f0.v, fxc.v);            // distance in pixel from center
      fx.v = __builtin_ia32_mulps (fax.v, fx.v);           // rescaling in x
      f4.v = __builtin_ia32_mulps (fax.v, f4.v);           // we prepare rescaled substraction
      for (j = 0; j < onx_4; j++)
	{
	  fx2[j].v = __builtin_ia32_mulps (fx.v, fx.v);    // we compute X2
	  fx.v = __builtin_ia32_addps(fx.v, f4.v);         // we move index
	}
      f0.f[0] = f0.f[1] = f0.f[2] = f0.f[3] = imin;        // starting y position
      fy.v = __builtin_ia32_subps(f0.v, fyc.v);            // distance in pixel to yc
      fy.v = __builtin_ia32_mulps (fay.v, fy.v);           // rescaled in y
      
      
      for (i = imin; i < imax; i++)
	{
	  fy2.v = __builtin_ia32_mulps (fy.v, fy.v);       // we compute y2 
	  fy.v = __builtin_ia32_addps(fy.v, fay.v);        // move by one rescaled pixel in y
	  tmp = rmax2 - fy2.f[0];
	  if (tmp < 0) continue;
	  tmp = sqrt(tmp);
	  tmp = (tmp+1)/ax;
	  jmin = (int)(xc - tmp);
	  jmax = (int)(xc + tmp + 1);
	  jmin = (jmin < 0) ? 0 : jmin;
	  jmax = (jmax > onx) ? onx : jmax;			
	  jmin_4 = jmin >> 2;
	  jmax_4 = 1+(jmax >> 2);
	  jmax_4 = (jmax_4 > onx_4) ? onx_4 : jmax_4;
	  for (j = jmin_4; j < jmax_4; j++)
	    {
	      fr[j].v = __builtin_ia32_addps(fy2.v, fx2[j].v);  // we compute x2 + y2
	      fr[j].v = __builtin_ia32_sqrtps(fr[j].v);         // we get the distance
	    }
	  for (j = jmin; j < jmax; j++)
	    {
	      ri = (int)r[j];
	      if (ri > r_max)	continue;	/* bug break;*/
	      z = (float)ps[i].in[j];
	      p = r[j] - ri;
	      if (ri == r_max)
		{
		  p0 += (1-p)*z;
		  pr0 += (1-p);
		  radial_dx[r_max] -= 1-p;
		}
	      else 
		{
		  rz[ri] += (1-p)*z;
		  rz[ri+r_max] += (1-p);
		  radial_dx[ri] += p;
		}
	      ri++;
	      if (ri < r_max)
		{
		  rz[ri] += p*z;
		  rz[ri+r_max] += p;
		  radial_dx[ri] -= 1-p;
		}
	    }
	}
    }
  else if (type == IS_CHAR_IMAGE)			
    {
      f0.f[0] = 0; f0.f[1] = 1; f0.f[2] = 2; f0.f[3] = 3;  // pixel index 
      fx.v = __builtin_ia32_subps(f0.v, fxc.v);            // distance in pixel from center
      fx.v = __builtin_ia32_mulps (fax.v, fx.v);           // rescaling in x
      f4.v = __builtin_ia32_mulps (fax.v, f4.v);           // we prepare rescaled substraction
      for (j = 0; j < onx_4; j++)
	{
	  fx2[j].v = __builtin_ia32_mulps (fx.v, fx.v);    // we compute X2
	  fx.v = __builtin_ia32_addps(fx.v, f4.v);         // we move index
	}
      f0.f[0] = f0.f[1] = f0.f[2] = f0.f[3] = imin;        // starting y position
      fy.v = __builtin_ia32_subps(f0.v, fyc.v);            // distance in pixel to yc
      fy.v = __builtin_ia32_mulps (fay.v, fy.v);           // rescaled in y
      
      for (i = imin; i < imax; i++)
	{
	  fy2.v = __builtin_ia32_mulps (fy.v, fy.v);       // we compute y2 
	  fy.v = __builtin_ia32_addps(fy.v, fay.v);        // move by one rescaled pixel in y
	  tmp = rmax2 - fy2.f[0];
	  if (tmp < 0) continue;
	  tmp = sqrt(tmp);
	  tmp = (tmp+1)/ax;
	  jmin = (int)(xc - tmp);
	  jmax = (int)(xc + tmp + 1);
	  jmin = (jmin < 0) ? 0 : jmin;
	  jmax = (jmax > onx) ? onx : jmax;			
	  jmin_4 = jmin >> 2;
	  jmax_4 = 1+(jmax >> 2);
	  jmax_4 = (jmax_4 > onx_4) ? onx_4 : jmax_4;
	  for (j = jmin_4; j < jmax_4; j++)
	    {
	      fr[j].v = __builtin_ia32_addps(fy2.v, fx2[j].v);  // we compute x2 + y2
	      fr[j].v = __builtin_ia32_sqrtps(fr[j].v);         // we get the distance
	      /*
		if ((debug != CANCEL) && (j == (onx >> 3)) && (i%16 == 0))
		debug = win_printf("y2 = %g i = %d j %d\n%g %g %g %g\n%g %g %g %g",
		fy2.f[0],i,j,fr[j].f[0],fr[j].f[1],fr[j].f[2],fr[j].f[3],
		r[4*j],r[(4*j)+1],r[(4*j)+2],r[(4*j)+3]);
	      */
	    }
	  
	  for (j = jmin; j < jmax; j++)
	    {
	      ri = (int)r[j];
	      if (ri > r_max)	continue;	/* bug break;*/
	      z = (float)ps[i].ch[j];
	      p = r[j] - ri;
	      if (ri == r_max)
		{
		  p0 += (1-p)*z;
		  pr0 += (1-p);
		  radial_dx[r_max] -= 1-p;
		}
	      else 
		{
		  rz[ri] += (1-p)*z;
		  rz[ri+r_max] += (1-p);
		  radial_dx[ri] += p;
		}
	      ri++;
	      if (ri < r_max)
		{
		  rz[ri] += p*z;
		  rz[ri+r_max] += p;
		  radial_dx[ri] -= 1-p;
		}
	    }
	}
    }
  for (i = 0; i < r_max; i++)
    {
      rz[i] = (rz[i+r_max] == 0) ? 0 : rz[i]/rz[i+r_max];
      radial_dx[i] = (rz[i+r_max] == 0) ? 0 : radial_dx[i]/rz[i+r_max];
    }
  p0 = (pr0 == 0) ? 0 : p0/pr0;  
  for (i = 0; i < r_max; i++)		rz[i+r_max] = rz[i]; 
  for (i = 1; i < r_max; i++)		rz[r_max-i] = rz[r_max+i]; 
  rz[0] = p0;
  return rz;
}

O_i	*do_load_calibration(int n)
{
  register int i;
  char path[512], file[256], name[128];
  static char fullfile[512], *fu = NULL;
  O_i *oi = NULL;
  
  
  if (fu == NULL)
    {
      sprintf (name,"last_calibration_%d",n);
      fu = (char*)get_config_string("IMAGE-GR-FILE",name,NULL);
      if (fu != NULL)	
	{
	  strcpy(fullfile,fu);
	  fu = fullfile;
	}
      else
	{
	  my_getcwd(fullfile, 512);
	  strcat(fullfile,"\\");
	}
    }
#ifdef XV_WIN32
  if (full_screen)
    {
#endif
      switch_allegro_font(1);
      i = file_select_ex("Load calibration image (*.gr)", fullfile, "gr", 512, 0, 0);
      switch_allegro_font(0);
#ifdef XV_WIN32
    }
  else
    {
      extract_file_path(path, 512, fullfile);
      put_backslash(path);
      if (extract_file_name(file, 256, fullfile) == NULL)
	fullfile[0] = file[0] = 0;
      else strcpy(fullfile,file);
      fu = DoFileOpen("Load calibration image (*.gr)", path, fullfile, 512, "Image Files (*.gr)\0*.gr\0All Files (*.*)\0*.*\0\0", "gr");
      i = (fu == NULL) ? 0 : 1;
      
    }
#endif
  
  if (i != 0) 	
    {
      fu = fullfile;
      set_config_string("IMAGE-GR-FILE",name,fu);
      extract_file_name(file, 256, fullfile);
      extract_file_path(path, 512, fullfile);
      strcat(path,"\\");
      oi = load_im_file_in_oi(file, path);
      if (oi == NULL)
	{   
	  win_printf("could not load image\n%s\n"
		     "from path %s",file, backslash_to_slash(path));
	  return NULL;
	}
      
    }
  return oi;
}

int grab_bead_info_from_calibration(O_i *oi, float *xb, float *yb, float *zb, int *nx, int *cw)
{
  char *co, *cs;
  
  if (oi == NULL) return 1;
  co = cs = (oi->im.source != NULL) ? oi->im.source : oi->im.treatement; 
  if (cs == NULL)
    {
      win_printf("wrong souce!");	
      return 1;
    }
  if (strncmp(cs,"equally spaced reference profile",32) != 0)
    win_printf("image must be :\\sl  equally spaced reference profile!");
  cs = strstr(co,"Bead xcb");
  if (cs != NULL && xb != NULL)
    {
      if (sscanf(cs+9,"%f",xb) != 1)
	win_printf("cannot read bead xc!");
    }
  cs = strstr(cs,"ycb");
  if (cs != NULL && yb != NULL)
    {
      if (sscanf(cs+3,"%f",yb) != 1)
	win_printf("cannot read bead yc!");
    }
  cs = strstr(cs,"zcb");
  if (cs != NULL && zb != NULL)
    {
      if (sscanf(cs+3,"%f",zb) != 1)
	win_printf("cannot read bead zc!");
    }	
  cs = strstr(cs,"nxb");
  if (cs != NULL && nx != NULL)
    {
      if (sscanf(cs+3,"%d",nx) != 1)
	win_printf("cannot read bead ncd!");
    }	
  cs = strstr(cs,"cwb");
  if (cs != NULL && cw != NULL)
    {
      if (sscanf(cs+3,"%d",cw) != 1)
	win_printf("cannot read bead cwd!");
    }
  return 0;
}




#endif
    
    
    
    
