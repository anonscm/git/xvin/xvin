/*
 *    Plug-in program for image treatement in Xvin.
 *
 *    V. Croquette
 *      JF Allemand
 */
#ifndef _IFC_C_
#define _IFC_C_


# include "allegro.h"
# include "winalleg.h"
# include "ifcapi.h"
# include <windowsx.h>
# include "ctype.h"
# include "xvin.h"

# include "pthread/pthread.h"

# define BUILDING_PLUGINS_DLL
# include "ifc.h"

int change_mouse_to_rect(int cl, int cw);
int reset_mouse_rect();
int determine_time_between_frames(imreg *imr, O_i *oi, int n,  DIALOG *d, long long t, unsigned long dt, void *p);



imreg  *imr_IFC = NULL;
pltreg  *pr_IFC = NULL;
O_i  *oi_IFC = NULL;
DIALOG *d_IFC = NULL;

float IFC_delay = 0.0001;
//O_p *idle_op = NULL;
//long long my_ulcl_t0;
//long long my_ulcl_dt_0;

int open_close_P2V_trigger(void)
{	int i;

 if(updating_menu_state != 0)	return D_O_K;
 /*check camlink is on*/
 if (get_pci_slot_for_IFCboard[P2V_BOARD] == -1)
  	return win_printf("P2V board is not active!");
 i = RETRIEVE_MENU_INDEX ;
 //win_printf("i=%d \n(WORD)i %d",i,(WORD)i);
 CICapMod_OutportVal(capmod,(WORD)i,0); 
 return 0;
}


char	*do_load_cam_file(void)
{
  register int i = 0;
  char path[512], file[256];
  static char fullfile[512], *fu = NULL;
  int full=1;

  if (fu == NULL)
    {
      fu = (char*)get_config_string("CAM-FILE","last_loaded",NULL);
      if (fu != NULL)	
	{
	  strcpy(fullfile,fu);
	  fu = fullfile;
	}
      else
	{
	  my_getcwd(fullfile, 512);
	  strcat(fullfile,"\\");
	}
    }
#ifdef XV_WIN32
  if (full) // 
    {
#endif
      switch_allegro_font(1);
      i = file_select_ex("Load camer (*.txt)", fullfile, "txt", 512, 0, 0);
      switch_allegro_font(0);
#ifdef XV_WIN32
    }
  else
    {
      extract_file_path(path, 512, fullfile);
      put_backslash(path);
      if (extract_file_name(file, 256, fullfile) == NULL)
	fullfile[0] = file[0] = 0;
      else strcpy(fullfile,file);
      fu = DoFileOpen("Load cam file (*.txt)", path, fullfile, 512, "Cam Files (*.txt)\0*.txt\0All Files (*.*)\0*.*\0\0", "txt");
      i = (fu == NULL) ? 0 : 1;
      win_printf("loading %d\n%s",backslash_to_slash(fullfile));
      slash_to_backslash(fullfile);
    }
#endif
  if (i != 0) 	
    {
      fu = fullfile;
      set_config_string("CAM-FILE","last_loaded",fu);
      return fullfile;;
    }
  return NULL;
}

int delete_IFC_stuff(void)
{
  if(updating_menu_state != 0)	return D_O_K;
  IFC_IfxDeleteImgConn(pImg_Conn);
  IFC_IfxDeleteCaptureModule(capmod);
  return 0;
}

int retrieve_IFC_image_index(imreg *imr)
{
  register int i;
  
  if (imr == NULL)   imr = imr_IFC;
  if (imr == NULL || oi_IFC == NULL)  return -1;
  for (i = 0; i < imr->n_oi; i++)
    if (imr->o_i[i] == oi_IFC) return i;
  return -1;
}

/*     This function resize the oi_IFC image to the the camera size, this is needed when
 *     whe change camera or select anothe video mode
 */

int resize_image(int i, imreg *imr, unsigned char ***buf, int nframes_in_buffer)
{	
  int j, k=0;
  O_i *oi;	
  void *pbuf;
  if (nframes_in_buffer > 1) imr->one_i->im.n_f = nframes_in_buffer;
	
  num_image_IFC = retrieve_IFC_image_index(imr);
  if (num_image_IFC != imr->cur_oi)	select_image_of_imreg_and_display(imr,num_image_IFC);
  
  oi = imr->one_i;
  num_image_IFC = imr->cur_oi;
  if (attr.dwBytesPerPixel == 1)
    {
      alloc_one_image (oi, attr.dwWidth, attr.dwHeight, IS_CHAR_IMAGE);
      oi->z_black = oi->z_min = 0;
      oi->z_white = oi->z_max = 255;
    }
  else if (attr.dwBytesPerPixel == 2)
    {
      alloc_one_image (oi, attr.dwWidth, attr.dwHeight, IS_UINT_IMAGE);
      oi->z_black = oi->z_min = 0;
      oi->z_white = oi->z_max = 65535;
    }
  else if (attr.dwBytesPerPixel == 3)
    alloc_one_image (oi, attr.dwWidth, attr.dwHeight, IS_RGB_PICTURE);
    
  set_oi_source(imr->one_i, "IFC");    
  for (k = 0; k < nframes_in_buffer; k++)
    {    
      if (nframes_in_buffer > 1) switch_frame(oi,k);     
      for (j = 0, pbuf = oi->im.mem[k]; j < attr.dwHeight; j++)
	oi->im.pixel[attr.dwHeight -1 -j].ch = (unsigned char*)pbuf + (j * attr.dwWidth*attr.dwBytesPerPixel);
    }
  if ((attr.dwWidth <1024) && (attr.dwHeight<1024))
    {
    	
      oi->width  = ((float)attr.dwWidth)/512;
      oi->height = ((float)attr.dwHeight)/512;
    }
  else
    {
      oi->width  = ((float)attr.dwWidth)/1024;
      oi->height = ((float)attr.dwHeight)/1024;
    }
  broadcast_dialog_message(MSG_DRAW,0);
  imr_IFC = imr;
  oi_IFC = imr->one_i;
  d_IFC = find_dialog_associated_to_imr(imr_IFC, NULL);
  *buf =  (unsigned char**)oi->im.mem;
  return 0;
}


int wait_next_frame(O_i *oi, int *fr_nb, int *fr_time)
{
    DWORD acquiredDy;
    GRAB_EXT_ATTR ExtendedAttr;
    int seq_num = 0;
    static double t = 0;
         		
    seq_num = CICamera_GrabWaitFrameEx(cam,grabID,oi->im.mem[0], IFC_WAIT_NEWER_FRAME,50,FALSE, &acquiredDy ,&ExtendedAttr );
    *fr_nb = ExtendedAttr.frameSeqNum;
    *fr_time = ExtendedAttr.ArrivalTime;
    
    if (t!= 0) draw_bubble(screen,0,850,100,"SeqNum %d frame %d Time%g",seq_num,*fr_nb ,ExtendedAttr.ArrivalTime-t);
    t = ExtendedAttr.ArrivalTime;
    
    
    return seq_num;
}



BOOL dummy_what_to_do_before_display(void *parameter)
{
  static int n = 0;
  unsigned long dt = (unsigned long)(IFC_delay*get_my_uclocks_per_sec());
  for (dt += my_uclock(); my_uclock() < dt; );
  display_title_message("call %d",n++);
  return TRUE;
}

BOOL what_to_do_before_display(void *parameter)
{	
  int nf;
  long long t;
  static unsigned long dt, dt_1 = 0;
  struct before_IFC_display *p;
  /*static int i = 0;
    static unsigned long acqtime = 0 , start = 0;*/

  if (imr_IFC == NULL || oi_IFC == NULL)	return FALSE;
  CICamera_GetGrabStats(cam,grabID,&stats);
  nf = stats.CurrentFrameSeqNum;
  if (parameter == NULL)	return TRUE;
  t /*= start*/ = my_ulclock();
  dt = my_uclock();
  p = (struct before_IFC_display*)parameter;
  if (p->to_do == NULL) return TRUE;
  for (p->to_do(imr_IFC, oi_IFC, nf, 0, d_IFC, t ,dt_1 ,p->param) ; p->to_do != NULL && p->next != NULL; p = p->next)
    p->to_do(imr_IFC, oi_IFC,nf, 0, d_IFC, t, dt_1, p->param);
  dt_1 = my_uclock() - dt;
    
  if ( nf%2 == 0 ) return TRUE; // why that ?
  return TRUE;
}

int set_all_IFC_for_aquisition(int boardtype, imreg *imr, HWND hWnd)
{
  pCITIMods itimod;
  ITI_PARENT_MOD mod;
  //char cam_file[256];	
  int i, j;
  float scx, scy;
  unsigned char **buf;
  BOOL what_to_do_before_display(void *received_frame_numb);
  //BITMAP *imb,*ds_bitmap;
  DIALOG *d;
  //HDC dc;
  //pCOverlay pC_Overlay = NULL;
  DWORD ColorKeyRep;

  j = get_pci_slot_for_IFCboard[boardtype];
  if (j > number_of_board_detected) 
    return win_printf_OK("You ask too much young Jedi!");

  d = find_dialog_associated_to_imr(imr, NULL);
  itimod = IFC_IfxCreateITIMods();
  for (i = 0; i <= j; i++)
    {
      if (i==0) CITIMods_GetFirst(itimod,&mod);
      else CITIMods_GetNext(itimod,&mod);
    }
 	
  if (!(capmod = IFC_IfxCreateCaptureModule(mod.name,0,do_load_cam_file()))) //prevoir 2 cartes identiques
    {
      win_printf("No Image Capture Module detected");
      exit(0);
    }
  cam = CICapMod_GetCam(capmod,0);
  if 	(cam == NULL) win_printf("Bad camera!");
  CICamera_GetAttr(cam,&attr,TRUE);
  resize_image(i,imr,&buf,nframes_in_buffer);
  imr->one_i->filename = "IFC";
  imr->one_i->need_to_refresh |= ALL_NEED_REFRESH;


  pImg_Conn = IFC_IfxCreateImgConn_HostBuf(imr->one_i->im.mem[0], attr.dwWidth, attr.dwHeight, 
					   (attr.dwBytesPerPixel == 2)?10:8, hWnd, 
					   IFC_MONO, IFC_LIVE_IMAGE|IFC_YCRCB_SINK/*|IFC_HW_OVERLAY*/ 
					   ,cam, ICAP_INTR_EOF,1,NULL,NULL);
					   //what_to_do_before_display,(void *) &disp_IFC);
  if 	(pImg_Conn == NULL) win_printf("Bad connection!");
  pImg_Sink = CImgConn_GetSink(pImg_Conn);
  if 	(pImg_Sink == NULL) win_printf("Bad Sink!");
  pImg_Src = CImgConn_GetSrc(pImg_Conn);	
  if 	(pImg_Src == NULL) win_printf("Bad src!");
  ColorKeyRep = CImgSink_GetColorKeyRep(pImg_Sink, 0x00FF00FF) ;
	
  if ( CImgSink_SetDestColorKey(pImg_Sink,ColorKeyRep) == FALSE) win_printf("Set color failed");
  scx = (float)(get_oi_horizontal_extend(imr->one_i)*512)/attr.dwWidth;
  scy = (float)(get_oi_vertical_extend(imr->one_i)*512)/attr.dwHeight;
  CImgSink_SetAoiPos(pImg_Sink,imr->x_off + d->x , imr->y_off - scy*attr.dwHeight+d->y);

  CImgSink_SetZoom(pImg_Sink,scx,scy);
  previous_board = boardtype;    
  set_config_int("IFC","last_board",boardtype);
  //win_printf("prev set to %d",boardtype);
  live_video(boardtype);
  return 0;
}   




int freeze_video(void)
{
  CICamera_Freeze(cam);  
  return 0;
}
int freeze_video_menu(void)
{
  imreg *imr;
	
  if(updating_menu_state != 0)	return D_O_K;
  is_live = 0;
  CICamera_Freeze(cam);  
  imr = find_imr_in_current_dialog(NULL);  
  imr->one_i->need_to_refresh &= BITMAP_NEED_REFRESH;  
  find_zmin_zmax(imr->one_i);
  refresh_image(imr, UNCHANGED);
  return 0;

}
int live_video(int mode)
{
  HWND hWnd;
  imreg *imr;
  O_i *oi;
  unsigned char **buf;
  int i;
  DIALOG *d;
  BITMAP *imb;
  unsigned long dt = 0;

#ifdef MODIF        
  int seq_num = 0;
  DWORD acquiredDy;
  double t = 0;	
# endif
	
  if(updating_menu_state != 0)	return D_O_K;
	
  hWnd = win_get_window();
  imr = find_imr_in_current_dialog(NULL);
  if (imr == NULL)	return 1;
  d = find_dialog_associated_to_imr(imr, NULL);

  if (mode != 0)
    {	
      if (mode < 0) i = RETRIEVE_MENU_INDEX ;// board is defined by menu
      else i = mode; 	
      if ((previous_board != i)||(pImg_Conn == NULL))
	{
	  win_printf("prev %d asked %d",previous_board,i);
	  delete_IFC_stuff();
	  set_all_IFC_for_aquisition(i,imr,hWnd);
	  /*aquisition_period = measure_aquisition_period_in_ms(imr->one_i);
	    if (create_and_attach_unit_set_to_oi (imr->one_i,IS_SECOND, 0, aquisition_period, -3, 0, "ms", 
	    IS_T_UNIT_SET) == NULL) avoir il faudrait modifier et pas rajouter
	    win_printf("Failed");
	    set_oi_t_unit_set(imr->one_i, imr->one_i->n_tu-1);*/
	}
    }
  if (num_image_IFC != imr->cur_oi)	select_image_of_imreg_and_display(imr,num_image_IFC);
  oi = imr->one_i ;
  oi->filename = "IFC";
  buf =  oi->im.mem[0]; 

  if ((imr->one_i->bmp.to == IS_BITMAP) &&  (imr->one_i->bmp.stuff != NULL))
    {
      imb = (BITMAP*)imr->one_i->bmp.stuff;
      if (is_video_bitmap(imb) != TRUE)
	{
	  imb = create_video_bitmap(imb->w,imb->h);
	  if (imb != NULL)
	    {
	      destroy_bitmap((BITMAP*)imr->one_i->bmp.stuff);
	      imr->one_i->bmp.stuff = (void*)imb;
	    }
	}
      imr->one_i->need_to_refresh &= ~BITMAP_NEED_REFRESH;
      dt = my_uclock();
      acquire_bitmap(imb);
      acquire_screen();
      clear_to_color(imb, makecol(255,0,255));
      blit(imb,screen,0,0,imr->x_off + d->x, imr->y_off - 
	   imb->h + d->y, imb->w, imb->h); 
      release_screen();
      release_bitmap(imb);
      dt = my_uclock() -dt;
      //destroy_bitmap(imb);

    }  else   win_printf("bitmap not allocated yet");

  broadcast_dialog_message(MSG_DRAW,0);// |IFC_SW_OVERLAY |IFC_DIB_SINK
  draw_bubble(screen,0,550,75,"t  %g ",((double)1000000*dt)/get_my_uclocks_per_sec());
 	
  LOCK_DATA( buf , oi->im.nx * oi->im.ny * attr.dwBytesPerPixel * nframes_in_buffer );
  grabID = CICamera_Grab_HostBufEx(cam,0,(unsigned char *)oi->im.mem[0],nframes_in_buffer,IFC_INFINITE_FRAMES, 0, 0, attr.dwWidth, attr.dwHeight);
  if (grabID == NULL) win_printf("GrabID failed");
  //  while((keypressed()!=TRUE)) 
  //wait_next_frame(oi, &arrived_frame, &arrived_frame_time);
        
  /*
  for (i = 0; i < 75; i++)
    {
      CICamera_GrabWaitFrameEx(cam,grabID,oi->im.mem[0], IFC_WAIT_NEWER_FRAME,50,FALSE, &acquiredDy ,&ExtendedAttr );
      if (i == 64) 
	{
	  disp_IFC.previous_in_time_fr = t = my_ulclock();
	  disp_IFC.previous_in_time_fr_nb = ExtendedAttr.frameSeqNum;
	  //ExtendedAttr.ArrivalTime;
	}
    }
  t = my_ulclock() - t;
  t /= 10;
  disp_IFC.timer_dt = (unsigned long)t;
  */
  return 0;
}

int IFC_live(void)
{
  if(updating_menu_state != 0)	return D_O_K;

  is_live  = 1;
  return live_video(-1);	
}

int image_ifc_got_mouse(imreg *imr, int xm_s, int ym_s, int mode)
{
  return live_video(0);
}
int image_ifc_lost_mouse(imreg *imr, int xm_s, int ym_s, int mode)
{
  return freeze_video();
}  


int build_list_board(int i,char *module_name)
{
  
  if (strncmp(module_name,"P2V",3) == 0)   		
    {
      board_list[i]  = P2V_BOARD;
      get_pci_slot_for_IFCboard[P2V_BOARD] = i;
      //      win_printf("Board P2V name number %d = %s\n ",i,module_name);
    }
  else if (strncmp(module_name,"PCD",3) == 0)	
    {
      board_list[i]  = PCD_BOARD;
      get_pci_slot_for_IFCboard[PCD_BOARD] = i;
      //win_printf("Board PCD name number %d = %s\n ",i,module_name);
    }
  else if (strncmp(module_name,"ICP",3) == 0)	
    {
      board_list[i]  = ICP_BOARD;
      get_pci_slot_for_IFCboard[ICP_BOARD] = i;
      //win_printf("Board ICP name number %d = %s\n ",i,module_name);
    }
  else if (strncmp(module_name,"LNK",3) == 0)	
    {
      board_list[i]  = PCLink_BOARD;
      get_pci_slot_for_IFCboard[PCLink_BOARD] = i;
      //win_printf("Board LNK name number %d = %s\n ",i,module_name);
    }
  else win_printf("Unknown board!");
  return 0;
}

int list_number_of_board_detected(void)
{
  pCITIMods itimod;
  ITI_PARENT_MOD mod[6];
  int num_board_detected = 0 ;
  int i;
  
  if(updating_menu_state != 0)	return D_O_K;
  
  itimod = IFC_IfxCreateITIMods();
  for (i = 0; i < 6 ; i++)
    {
      if (i == 0)
	{
	  if (CITIMods_GetFirst(itimod,&mod[0])) 
	    {
	      //win_printf("Board name number %d = %s\n ",i,mod[i].name);
	      build_list_board(i,mod[i].name);
	      num_board_detected = i+1 ;
	    }
	}
      else
	{
	  if (CITIMods_GetNext(itimod,&mod[i] ))
	    {
	      //win_printf("Board name number %d = %s\n ",i,mod[i].name);
	      build_list_board(i,mod[i].name);
	      num_board_detected = i+1 ;
	    }
	}
    }	
  if (num_board_detected == 0 ) 
    {
      win_printf("No board detected, we leave the program");
      exit(0);
    }
  //win_printf("Board LNK name number %d \n ",get_pci_slot_for_IFCboard[PCLink_BOARD]);
  IFC_IfxDeleteITIMods(itimod);
  return num_board_detected;
}


int change_mouse_to_rect(int cl, int cw)
{
  BITMAP *ds_bitmap = NULL;
  int color, col;


  ds_bitmap = create_bitmap(cl+1,cw+1);
  col = Lightmagenta;
  color = makecol(EXTRACT_R(col), EXTRACT_G(col), EXTRACT_B(col));
  clear_to_color(ds_bitmap, color);

  color = makecol(255, 0, 0);                        

  line(ds_bitmap,0,0,cl,0,color);
  line(ds_bitmap,0,0,0,cw,color);
  line(ds_bitmap,0,cw,cl,cw,color);
  line(ds_bitmap,cl,0,cl,cw,color);
  scare_mouse();    
  set_mouse_sprite(ds_bitmap);
  set_mouse_sprite_focus(cl/2, cw/2);
  unscare_mouse();
  return 0;    
}
int reset_mouse_rect()
{
  scare_mouse();    
  set_mouse_sprite(NULL);
  set_mouse_sprite_focus(0, 0);
  unscare_mouse();
  return 0;
}
int	select_rect(void)
{
  static int cl = 128, cw = 16, i;
	
  if(updating_menu_state != 0)	return D_O_K;	
  i = win_scanf("Rectangle size cl %d cw %d",&cl,&cw);
	
  if (i == CANCEL)	 return reset_mouse_rect();
  return change_mouse_to_rect(cl, cw);
}



int IFC_lost_fr(void)
{
  if(updating_menu_state != 0)	return D_O_K;

  display_title_message("disp_IFC ac_i %d c_i %d lost fr %d last in time %d",disp_IFC.ac_i,disp_IFC.c_i,disp_IFC.lost_fr,disp_IFC.previous_in_time_fr_nb);
  //disp_IFC.lost_fr = 0;
  return D_O_K;
}


MENU *ifc_image_menu(void)
{
  static MENU mn[32];
  int follow_bead_in_x_y(void);
  int draw_bead_in_x_y(void);
  int get_tracking_points(void);
  int aquire_and_save_data_in_real_time(void);
  int test_background_saving(void);
  int start_save_acq_ds(void);
  int plot_acqplt(void);
  int do_load_track_data(void);
  int get_acq_time(void);
  int change_nb_images_in_buffer(void);
  int save_tape_mode(void);
  int play_movie_real_time(void);
  int set_background_transfer_image_data(void);
  int get_movie(void);
  int get_small_movie(void);
  int average_real_time(void);
  int start_servo(void);
  int grab_tracking_timing(void);
    
  if (mn[0].text != NULL)	return mn;
  // lauch bead tracking in 2D or 3D
  add_item_to_menu(mn,"New bead track x y", follow_bead_in_x_y,NULL,0,NULL);
  add_item_to_menu(mn,"Track x y z", follow_bead_in_x_y,NULL,MENU_INDEX(IMAGE_CAL),NULL);
  add_item_to_menu(mn,"Servo Obj.", start_servo,NULL,0,NULL);
  // debug : display bead position
  add_item_to_menu(mn,"display x y", draw_bead_in_x_y,NULL,0,NULL);
  //old stuff to retrieve data from rolling buffer
  add_item_to_menu(mn,"get previous points of tracking",get_tracking_points,NULL,0,NULL);
  //    add_item_to_menu(mn,"Save data",start_save_acq_ds,NULL,0,NULL);
  add_item_to_menu(mn,"Grab timing",grab_tracking_timing,NULL,0,NULL);

  //  add_item_to_menu(mn,"Set delay",change_IFC_delay,NULL,0,NULL);
  add_item_to_menu(mn,"lost frames",IFC_lost_fr,NULL,0,NULL);

  //add_item_to_menu(mn,"Read acq data",read_acq_file,NULL,0,NULL);
  //add_item_to_menu(mn,"plot_acqplt",plot_acqplt,NULL,0,NULL);
  //  add_item_to_menu(mn,"Load Track",do_load_track_data,NULL,0,NULL);
  // add_item_to_menu(mn,"Adjust buffer", change_nb_images_in_buffer, NULL, 0, NULL);
  // add_item_to_menu(mn,"Save video tape", save_tape_mode, NULL, 0, NULL);
  //add_item_to_menu(mn,"Play video tape", play_movie_real_time, NULL, 0, NULL);
  //add_item_to_menu(mn,"Background task",set_background_transfer_image_data, NULL, 0, NULL);
  //add_item_to_menu(mn,"Get movie",get_movie, NULL, 0, NULL);
  //add_item_to_menu(mn,"Get SMALLmovie",get_small_movie, NULL, 0, NULL);
  //add_item_to_menu(mn,"Average_real_time",average_real_time, NULL, 0, NULL);
  
  add_item_to_menu(mn,"Detect Board",  list_number_of_board_detected, NULL ,  0, NULL);
  add_item_to_menu(mn,"Freeze",	freeze_video_menu, NULL ,  0, NULL );
  //	add_item_to_menu(mn,"idle stuff",	active_idle_plot, NULL ,  0, NULL );
  if (get_pci_slot_for_IFCboard[P2V_BOARD] != -1) 	
    add_item_to_menu(mn,"Live P2V",	IFC_live, NULL ,  MENU_INDEX(P2V_BOARD), NULL );
  if (get_pci_slot_for_IFCboard[PCD_BOARD] != -1)	
    add_item_to_menu(mn,"Live PCD_BOARD",IFC_live, NULL ,  MENU_INDEX(PCD_BOARD), NULL );
  if (get_pci_slot_for_IFCboard[PCLink_BOARD] != -1)	
    add_item_to_menu(mn,"Live PCLink",IFC_live, NULL ,  MENU_INDEX(PCLink_BOARD), NULL );
  if (get_pci_slot_for_IFCboard[ICP_BOARD] != -1)	
    add_item_to_menu(mn,"Live ICP",IFC_live, NULL ,  MENU_INDEX(ICP_BOARD), NULL );
    
  if (get_pci_slot_for_IFCboard[P2V_BOARD] != -1)
    {	
      add_item_to_menu(mn,"OPEN shutter",open_close_P2V_trigger, NULL ,  MENU_INDEX(OPEN), NULL );
      add_item_to_menu(mn,"CLOSE shutter",open_close_P2V_trigger, NULL ,  MENU_INDEX(CLOSE), NULL );
    }
  
  return mn;
}


int ifc_before_menu(int msg, DIALOG *d, int c)
{
  /* this routine switches the display flag so that no magic color is drawn 
     when menu are activated */
   
  if (msg == MSG_GOTMOUSE)
    {
      do_refresh_overlay = 0;
      //display_title_message("menu got mouse");
    }
  return 0;
}
int ifc_after_menu(int msg, DIALOG *d, int c)
{
  return 0;
}

int ifc_oi_got_mouse(struct  one_image *oi, int xm_s, int ym_s, int mode)
{
  /* this routine switches the display flag so that the magic color is drawn again
     wafter menu are activated */
  do_refresh_overlay = 1;
  //display_title_message("Image got mouse");
  return 0;
}






/* detect if a new image has been acquired, previous refer to the image number grabbed
   at the previous call 
*/
int new_image_has_arrived(int *previous)
{
  register int imp, new;
  imp = *previous;
  CICamera_GetGrabStats(cam,grabID,&stats);
  *previous = new = stats.CurrentFrameSeqNum;
  return (new > imp && imp > 0) ? 1 : 0;
}

int this_image_has_not_arrived_yet(int imp)
{
  register int new;
  CICamera_GetGrabStats(cam,grabID,&stats);
  new = stats.CurrentFrameSeqNum;
  return (new < imp) ? 1 : 0;
}


# define AcquisitionPeriodInHundredsOfNanoSeconds   \
(int)((((double)AcquisitionPeriod())*10000000)/get_my_ulclocks_per_sec())


int AcquisitionPeriod(void)
{
  register int i, k;
  long long t0 = 0, t = 0;

  CICamera_GetGrabStats(cam,grabID,&stats);
  k = stats.CurrentFrameSeqNum;
  for (i = 0; i < 11; )
    {
      CICamera_GetGrabStats(cam,grabID,&stats);
      if (stats.CurrentFrameSeqNum != k)
	{
	  if (i == 0)  t0 = my_ulclock();
	  else t = my_ulclock();
	  i++;
	}
    }
  t -= t0;
  return (int)(t/10);
}



int grab_tracking_timing(void)
{     
  O_i *oi = NULL;
  O_p *op = NULL;
  d_s *ds_x = NULL, *ds_y = NULL;
  int i = 0, nf;
  imreg *imr = NULL;
  long long t;
  unsigned long dt;
  struct before_IFC_display *p;
  
  if(updating_menu_state != 0)	return D_O_K;
  
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)
    return win_printf("cannot find image!");

  p = &disp_IFC;
  nf = (p->ac_i < TRACKING_BUFFER_SIZE) ? p->ac_i : TRACKING_BUFFER_SIZE;
  op = create_and_attach_op_to_oi(oi, nf ,nf , 0,0);
  if (op == NULL)	return win_printf("cannot create plot!");
  
  ds_x = op->dat[0];
  ds_y = create_and_attach_one_ds(op,nf,nf,0);
  if (ds_y == NULL) return win_printf("Cannot create data set");


  for (i = nf-1; i > 0; i--)
    {
      ds_x->xd[i-1] = ds_y->xd[i-1] = p->imi[(p->ac_i-i)%TRACKING_BUFFER_SIZE];
      t = p->imt[(p->ac_i-i)%TRACKING_BUFFER_SIZE] - p->imt[(p->ac_i-i-1)%TRACKING_BUFFER_SIZE];
      dt = p->imdt[(p->ac_i-i)%TRACKING_BUFFER_SIZE];
      ds_x->yd[i-1] = 1000*((float)t)/get_my_ulclocks_per_sec();
      ds_y->yd[i-1] = 1000*((float)dt)/get_my_uclocks_per_sec();
    }
  oi->need_to_refresh |= PLOT_NEED_REFRESH	| EXTRA_PLOT_NEED_REFRESH;	
  return refresh_im_plot(imr, oi->cur_op);

}



DWORD WINAPI TrackingThreadProc(LPVOID lpParam) 
{
  imreg *imr;
  HWND hWnd;
  int n = 0, nf, i, k;
  O_i *oi;
  long long t;
  static unsigned long dt, dt_1 = 0;
  struct before_IFC_display *p;
  DWORD acquiredDy;
  GRAB_EXT_ATTR ExtendedAttr;

  if (imr_IFC == NULL || oi_IFC == NULL)	return FALSE;
  imr = (imreg*)lpParam;
  CImgConn_GetSrc(&pImg_Conn);
  hWnd = win_get_window(); // we grab WINDOWS context
  oi = imr->one_i;
  p = &disp_IFC;
  while (1)
    {
      n = CICamera_GrabWaitFrameEx(cam,grabID,(unsigned char**)oi->im.mem, p->ac_i,//IFC_WAIT_NEWER_FRAME,
				   50,FALSE, &acquiredDy ,&ExtendedAttr );
      CImgSrc_SetBufferAddr(pImg_Src,oi->im.mem[n]);
      p->c_i %= TRACKING_BUFFER_SIZE;
      p->previous_fr_nb = p->imi[p->c_i] = nf = (int)ExtendedAttr.frameSeqNum;
      p->imt[p->c_i] = t = my_ulclock();
      p->imdt[p->c_i] = dt = my_uclock();
      if (p->ac_i < 3) 
	{
	  p->previous_in_time_fr_nb = nf;
	  p->offset_im = nf - p->ac_i;
	  p->previous_in_time_fr = t;
	  p->lost_fr = 0;
	}
      else
	{
	  i = p->ac_i - 1;
	  i %= TRACKING_BUFFER_SIZE;
	  if (nf - p->imi[i] <= 1)
	    {
	      p->previous_in_time_fr_nb = nf;
	      p->previous_in_time_fr = t;
	      p->lost_fr =  p->ac_i - nf - p->offset_im;
	    }
	  else if (nf > 0 && p->ac_i > 10)
	    {
	      p->lost_fr =  p->ac_i - nf - p->offset_im;
	      display_title_message("lost frames -> %d",p->lost_fr);
	    }
	}
      if (disp_IFC.to_do != NULL)
	{ 
	  for (p->to_do(imr_IFC, oi_IFC, nf, n, d_IFC, t ,dt_1 ,p->param) 
		 ; p->to_do != NULL && p->next != NULL; p = p->next)
	    p->to_do(imr_IFC, oi_IFC,nf, n, d_IFC, t, dt_1, p->param);
	  dt_1 = my_uclock() - dt;
	}
      p->timer_dt = p->imdt[p->c_i] = my_uclock() - dt;
      p->c_i++;
      p->ac_i++;
    }
  return 0;
}



int create_tracking_thread(imreg *imr)
{
    HANDLE hThread = NULL;
    DWORD dwThreadId;

    disp_IFC.lost_fr = 0;  disp_IFC.ac_i = 0; disp_IFC.c_i = 0;
    hThread = CreateThread( 
            NULL,              // default security attributes
            0,                 // use default stack size  
            TrackingThreadProc,// thread function 
            (void*)imr,        // argument to thread function 
            0,                 // use default creation flags 
            &dwThreadId);      // returns the thread identifier 

    if (hThread == NULL) 	win_printf_OK("No thread created");
    SetThreadPriority(hThread,  THREAD_PRIORITY_TIME_CRITICAL);


    return 0;
}



int	ifc_main(int argc, char **argv)
{
  //pthread_t threads;
  //int rc;
  imreg *imr = NULL;
  pltreg *pr = NULL;
  O_p *op;
  HWND hWnd;
  int n;


 
  hWnd = win_get_window(); // we gran WINDOWS context
  // we create a plot region associated with IFC stuff 
  pr = create_pltreg_with_op(&op, 4096, 4096, 0);
  if (pr == NULL)  win_printf_OK("Could not find or allocte plot region!");
  pr_IFC = pr;
  if (op == NULL)  win_printf_OK("Could not find or allocte plot !");
  refresh_plot(pr,0);
  // we create the image region associated with IFC stuff 
  imr = create_and_register_new_image_project( 0,   32,  900,  668);
  if (imr == NULL) win_printf("Patate t'es mal barre!");
  //idle_imr = do_activity_check;
  //the_dialog[2].proc = d_draw_Im_proc_ifc;
  number_of_board_detected = list_number_of_board_detected();
  n = get_config_int("IFC","last_board",ICP_BOARD);
  add_image_treat_menu_item ( "ifc", NULL, ifc_image_menu(), 0, NULL);
  set_all_IFC_for_aquisition(n,imr,hWnd);
  // we switch specific functions  
  before_menu_proc = ifc_before_menu;
  after_menu_proc = ifc_after_menu;
  oi_IFC->oi_got_mouse = ifc_oi_got_mouse;
  //win_printf_OK("bef entering thread!");
  create_tracking_thread(imr);
  return D_O_K;
}

int	ifc_unload(int argc, char **argv)
{
  remove_item_to_menu(image_treat_menu, "ifc", NULL, NULL);
  return D_O_K;
}
#endif

