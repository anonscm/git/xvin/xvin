/* LS7266 APPLICATION NOTE */
/* note: pin 18, 19, 28 and 1 are normally high, pin 13 to address 0, pin 17 to address 1*/
/* Borland C source code */

#include "stdio.h"             
#include "dos.h"
#include "conio.h"

/* addresses */
#define BASE			0X300			// base address of interface board
#define DATA1			BASE+0			// data register of LS7266 axis X
#define CONTROL1		BASE+1			// control register of LS7266 axis X
#define DATA2			BASE+2			// data register of LS7266 axis Y
#define CONTROL2		BASE+3			// control register of LS7266 axis Y

/* LS7266 commands */
#define CLOCK_DATA 		14			// FCK frequency divider
#define CLOCK_SETUP 		0X98			// transfer PR0 to PSC (x and y)
#define INPUT_SETUP   		0XC1			// enable inputs A and B (x and y)
#define QUAD_X1       		0XA8			// quadrature multiplier to 1 (x and y)
#define QUAD_X2       		0XB0			// quadrature multiplier to 2 (x and y)
#define QUAD_X4       		0XB8			// quadrature multiplier to 4 (x and y)
#define BP_RESET    		0X01			// reset byte pointer
#define BP_RESETB  		0X81			// reset byte pointer (x and y)
#define CNTR_RESET    		0X02			// reset counter
#define CNTR_RESETB    		0X82			// reset counter (x and y)
#define TRSFRPR_CNTR    		0X08			// transfer preset register to counter
#define TRSFRCNTR_OL		0X90			// transfer CNTR to OL (x and y)
#define EFLAG_RESET		0X86			// reset E bit of flag register

/*global variables*/
int axis;

int init(void) {
	int fail = 0;
	/*initialize the 7266*/
	outportb(CONTROL1, EFLAG_RESET);		// reset E bit of flag register
	outportb(CONTROL1, BP_RESETB);		// reset byte pointer (x and y)
	outportb(DATA1, CLOCK_DATA);			// FCK frequency divider
	outportb(CONTROL1, CLOCK_SETUP);		// transfer PR0 to PSC (x and y)
	outportb(CONTROL1, INPUT_SETUP);		// enable inputs A and B (x and y)
	outportb(CONTROL1, QUAD_X4);			// quadrature multiplier to 4 (x and y)
	outportb(CONTROL1, CNTR_RESETB);		// reset counter (x and y)

	/* preset values to the X-axis preset register*/
	outportb(CONTROL1, BP_RESET);			// reset byte pointer
	outportb(DATA1, 0x12);				// output preset value
	outportb(DATA1, 0x34);
	outportb(DATA1, 0x56);
	/*read counter back to test it*/
	outportb(CONTROL1, TRSFRPR_CNTR);		// transfer preset to counter
	outportb(CONTROL1, TRSFRCNTR_OL);		// transfer CNTR to OL
	outportb(CONTROL1, CNTR_RESET);		// reset counter

	outportb(CONTROL1, BP_RESET);				// reset byte pointer
	if (inportb(DATA1) != 0x12) fail++;				// read output latch
	if (inportb(DATA1) != 0x34) fail++;				// if data is different, 1 is added to fail
	if (inportb(DATA1) != 0x56) fail++;
	if (fail) printf("Axis X counter failed");				// print message if failed

	/* preset values to the Y-axis preset register*/
	outportb(CONTROL2, BP_RESET);				// reset byte pointer
	outportb(DATA2, 0x78);					// output preset value
	outportb(DATA2, 0x9A);
	outportb(DATA2, 0xBC);
	/*read counter back to test it*/
	outportb(CONTROL2, TRSFRPR_CNTR);			// transfer preset to counter
	outportb(CONTROL2, TRSFRCNTR_OL);			// transfer CNTR to OL
	outportb(CONTROL2, CNTR_RESETB);			// reset counter (x and y)
	outportb(CONTROL2, BP_RESET);				// reset byte pointer
	if (inportb(DATA2) != 0x78) fail++;				// read output latch
	if (inportb(DATA2) != 0x9A) fail++;				// if data is different, 1 is added to fail
	if (inportb(DATA2) != 0xBC) fail++;
	if (fail) printf("\nAxis Y counter failed");				// print message if failed
	return fail;						// return fail variable
}

long read_position(axis) {						// read position of encoder
	long position;
	outportb(CONTROL1, BP_RESETB);			// reset byte pointer
	position  = (long)inportb(DATA1 + 2 * axis);			// least significant byte
	position += (long)inportb(DATA1 + 2 * axis) << 8;
	position += (long)inportb(DATA1 + 2 * axis) <<16;		// most significant byte
	return position;
}


int main(void) {
	if (init()) return 0;					// exit if LS7266 failed
	while (!kbhit()) {
		for(axis = 0; axis < 2; axis++) {			// switch between values 0 and 1 for axis variable
			outportb(CONTROL1, TRSFRCNTR_OL);        // transfer CNTR to OL
			printf("position =%8lX\t", read_position(axis));        // print position
		}
		printf("flag X=%X  Y=%X\t", inportb(CONTROL1), inportb(CONTROL2));        // display flag registers
		printf("\n");
	}
return 0;
}
