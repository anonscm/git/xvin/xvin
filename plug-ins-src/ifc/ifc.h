#ifndef _IFC_H_
#define _IFC_H_

#define P2V_BOARD			1
#define PCD_BOARD			2
#define PCLink_BOARD    		3
#define ICP_BOARD			4 
#define FRAMES_IN_HOSTBUFF             16
#define NUM_MAX_BOARDS 	               10
#define IMAGE_CAL			1
#define	OPEN				1
#define CLOSE				0


# define TRACKING_BUFFER_SIZE 4096

typedef struct before_IFC_display
{
  int (*to_do)(imreg *imr, O_i *oi, int n, int n_in_movie, DIALOG *d, long long t, unsigned long dt, void *p);
  void *param;
  int offset_im;                                 // offset between the actual image number and index
  int previous_fr_nb;
  unsigned long timer_dt;
  int previous_in_time_fr_nb;
  long long previous_in_time_fr;
  int lost_fr;
  struct before_IFC_display *next;
  int (*timer_do)(imreg *imr, O_i *oi, int n, int n_in_movie, DIALOG *d, long long t, unsigned long dt, void *p);
  int c_i;                                       // c_i, index of the current image
  int ac_i;                                      // ac_i actual number of frame since acquisition started
  int imi[TRACKING_BUFFER_SIZE];                 // the image nb
  long long imt[TRACKING_BUFFER_SIZE];           // the image absolute time
  unsigned long imdt[TRACKING_BUFFER_SIZE];      // the time spent in the previous function call
} IFC_disp;



# ifndef _IFC_C_
PXV_VAR(int, board_type);
PXV_VAR(pCICapMod, capmod);
PXV_VAR(pCICamera, cam);
PXV_VAR(CAM_ATTR, attr);
PXV_VAR(HIFCGRAB, grabID);
PXV_VAR(pCImgConn, pImg_Conn);
PXV_VAR(pCImgSink, pImg_Sink);
PXV_VAR(pCImgSrc, pImg_Src);
PXV_VAR(int, number_of_board_detected); 
PXV_VAR(int, num_board_detected);
PXV_VAR(int, board_list[NUM_MAX_BOARDS]);
PXV_VAR(int,get_pci_slot_for_IFCboard[NUM_MAX_BOARDS]);
PXV_VAR(int,nframes_in_buffer);
PXV_VAR(GRAB_EXT_ATTR,ExtendedAttr);
PXV_VAR(int , previous_board);
PXV_VAR(int , previous_image_for_saving);
PXV_VAR(IFC_GRAB_STATS, stats);
PXV_VAR(int, aquisition_period);
PXV_VAR(int, is_live);
PXV_VAR(int, do_refresh_overlay);
PXV_VAR(IFC_disp, disp_IFC);
# endif

# ifdef _IFC_C_
int num_image_IFC = 0;
int previous_image_for_saving = 0;
GRAB_EXT_ATTR ExtendedAttr;
int nframes_in_buffer = 64;
int board_type =0 ;
pCICapMod capmod;
pCICamera cam;
CAM_ATTR attr;
HIFCGRAB grabID;
pCImgConn pImg_Conn;
pCImgSink pImg_Sink;
pCImgSrc pImg_Src;
int number_of_board_detected = 1;
int num_board_detected = 0;
int board_list[NUM_MAX_BOARDS];
int get_pci_slot_for_IFCboard[NUM_MAX_BOARDS] = {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};
int previous_board = P2V_BOARD;
IFC_GRAB_STATS stats;
IFC_disp disp_IFC={NULL,NULL,0,0,0,0,0,0,NULL,NULL,0,0};
int aquisition_period = 0;
int is_live = 0;
int do_refresh_overlay = 1; // tell if we can redisplay the magic color
# endif

PXV_FUNC(int, retrieve_IFC_image_index, (imreg *imr));
PXV_FUNC(int,live_video,(int mode));
PXV_FUNC(float, measure_aquisition_period_in_ms,(O_i *oi));
PXV_FUNC(int, delete_IFC_stuff,(void));
PXV_FUNC(int, resize_image,(int i, imreg *imr,unsigned char ***buf, int nf));
PXV_FUNC(int, set_all_IFC_for_aquisition,(int j,imreg *imr,HWND hWnd));
PXV_FUNC(int, IFC_live,(void));
PXV_FUNC(int, build_list_board,(int i,char *module_name));
PXV_FUNC(int, list_number_of_board_detected,(void));
PXV_FUNC(int,  freeze_video,(void));


PXV_FUNC(int, do_ifc_average_along_y, (void));
PXV_FUNC(MENU*, ifc_image_menu, (void));
PXV_FUNC(int, ifc_main, (int argc, char **argv));
PXV_FUNC(int, change_mouse_to_rect, (int cl, int cw));
#endif

