#ifndef JAI_2_XViN_C
#define JAI_2_XViN_C
#include "allegro.h"
#include "winalleg.h"
#include "xvin.h"
#include "Jai_Error.h"
#include "Jai_Types.h"
#include "Jai_Factory.h"
#include "JAI_source.h"
int JAI_oi_mouse_action(O_i *oi, int x0, int y0, int mode, DIALOG *d)
{
  // we put in this routine all the screen display stuff
  imreg *imr;
  BITMAP *imb;
  unsigned long t0 = 0;

  //  display_title_message("cou cou");
  //return D_O_K;
  if (d->dp == NULL)    return 1;
  imr = (imreg*)d->dp;        /* the image region is here */
  if (imr->one_i == NULL || imr->n_oi == 0)        return 1;


  while(mouse_b == 1)
    {
      if (oi->need_to_refresh & BITMAP_NEED_REFRESH)
	{
	  t0 = my_uclock();
	  
	  display_image_stuff_16M(imr,d);
	  oi->need_to_refresh |= INTERNAL_BITMAP_NEED_REFRESH;
	  t0 = my_uclock() - t0;
	  write_and_blit_im_box( plt_buffer, d, imr);
      

	  imb = (BITMAP*)oi->bmp.stuff;
	  //show_bead_cross(screen, imr, d);
	  //move_bead_cross(screen, imr, d, mouse_x, mouse_y);


/* 	  if (dt_simul > 0) */
/* 	    display_title_message("Mouse im %d dt %6.3f ms sim %6.3f aff %6.3f",oi->im.c_f,  1000*(double)(d->d1)/get_my_uclocks_per_sec(),1000*(double)(dt_simul)/get_my_uclocks_per_sec(),1000*(double)(t0)/get_my_uclocks_per_sec()); */
/* 	  else */
	    display_title_message("Mouse im %d dt %6.3f ms",oi->im.c_f,  1000*(double)(d->d1)/get_my_uclocks_per_sec());
	}
      if (general_idle_action) general_idle_action(d);
    }

  // this routine switches supress the image idle action, 
  // in particular info display by xvin on image by default 

  return 0;
}




int JAI_before_menu(int msg, DIALOG *d, int c)
{
  /* this routine switches the display flag so that no magic color is drawn 
     when menu are activated */
  if (msg == MSG_GOTMOUSE)
    {
      //do_refresh_overlay = 0;
      //display_title_message("menu got mouse %d",n_t++);
    }
  return 0;
}

int JAI_after_menu(int msg, DIALOG *d, int c)
{
  //display_title_message("menu exit mouse");
  return 0;
}

int JAI_oi_got_mouse(struct  one_image *oi, int xm_s, int ym_s, int mode)
{
  /* this routine switches the display flag so that the magic color is drawn again
     after menu are activated */
  //do_refresh_overlay = 1;
  //display_title_message("Image got mouse");
  return 0;
}


void stop_source_thread(void)
{
  //  go_track = TRACK_STOP;
  //win_printf("Stopping thread");
  broadcast_dialog_message(MSG_DRAW,0); // needed to prevent freezing dialog!
  //while (source_running);
}


int JAI_oi_idle_action(struct  one_image *oi, DIALOG *d)
{
  // we put in this routine all the screen display stuff
  imreg *imr;
  int redraw_background = 0;
  BITMAP *imb;
  unsigned long t0 = 0;


  if (d->dp == NULL)    return 1;
  imr = (imreg*)d->dp;        /* the image region is here */
  if (imr->one_i == NULL || imr->n_oi == 0)        return 1;

  t0 = my_uclock();
  if (oi->need_to_refresh & BITMAP_NEED_REFRESH)
    {
      display_image_stuff_16M(imr,d);
      oi->need_to_refresh |= INTERNAL_BITMAP_NEED_REFRESH;
    }



  imb = (BITMAP*)oi->bmp.stuff;
  //  redraw_background = show_bead_cross(imb, imr, d);

  if (redraw_background)  // slow redraw
    {
      write_and_blit_im_box( plt_buffer, d, imr);
      //      show_bead_cross(plt_buffer, imr, d);
 
    }
 
 
      else  if (oi->need_to_refresh & INTERNAL_BITMAP_NEED_REFRESH)
    {
      //screen_used = 1;
      SET_BITMAP_IN_USE(oi);
      acquire_bitmap(screen);
      blit(imb,screen,0,0,imr->x_off //+ d->x
	   , imr->y_off - imb->h + d->y, imb->w, imb->h); 
      release_bitmap(screen);
      //screen_used = 0;
      oi->need_to_refresh &= ~INTERNAL_BITMAP_NEED_REFRESH;
      t0 = my_uclock() - t0;

      SET_BITMAP_NO_MORE_IN_USE(oi);
    
	display_title_message("im %d dt %6.3f ms",oi->im.c_f,  1000*(double)(d->d1)/get_my_uclocks_per_sec());
      
    }
  // this routine switches supress the image idle action, 
  // in particular info display by xvin on image by default 

  return 0;
}



int source_end_action(DIALOG *d)
{
  stop_source_thread();
  return 0;
}

int prepare_image_overlay(O_i* oi)
{
  return 0;
}


#endif
