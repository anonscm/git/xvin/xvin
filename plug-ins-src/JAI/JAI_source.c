/*******************************************************************************
                         LPS ENS
--------------------------------------------------------------------------------

  Program      : JAI Genicam GigE_Source for bead tracking 
  Author       : JFA
  Date         : 27.10.2007

  Purpose      : JAI Manager Functions for XVin
  CVB          : 
  Hints        : 
      
  Updates      : 
********************************************************************************/
#ifndef _JAI_SOURCE_C_
#define _JAI_SOURCE_C_

#include "allegro.h"
#include "winalleg.h"
#include "xvin.h"

/* If you include other plug-ins header do it here*/ 
//#include "stdafx.h"
#include "Jai_Error.h"
#include "Jai_Types.h"
#include "Jai_Factory.h"
#include "JAI_2_XVIN.h"
/* But not below this define */

# define BUILDING_PLUGINS_DLL
#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)


# include "JAI_source.h"

 VIEW_HANDLE     g_hView;    // Global view handle

///////////////////////////////////////////////////////////
//                    OPEN JAI SDK HANDLES FUNCTIONS     //
///////////////////////////////////////////////////////////
int iOpen_Factory(void)
{

  // Open the Factory
  retval = J_Factory_Open("" , &hFactory);
  if (retval != J_ST_SUCCESS)
    return win_printf_OK("Can not open Factory");

  // Close the factory
/*   retval = J_Factory_Close(hFactory); */
  return D_O_K;
}

CAM_HANDLE hCam_Open_camera(void)
{
  register int i = 0;
  int  index = 0;
  char **psCameraId = NULL;
  char question[4096] , string[2048];
  BOOL bHasChanged;
  //  CAM_HANDLE hCamera = NULL;
  int nCameras;
  int size;
  char test[512];


  if (hFactory == NULL) 
    {
      win_printf_OK("Factory should be created First");
      return NULL;
    }

 /*  // Search for cameras on all the networks */
/*   retval = J_Factory_UpdateCameraList(hFactory, &bHasChanged); */
/*   if (retval == J_ST_SUCCESS && bHasChanged) */
/*     { */
/*       // Get the number of cameras. This number can be larger than the actual camera count because  */
/*       // the cameras can be detected through different driver types! */
/*       // This mean that we might need to filter the cameras in order to avoid dublicate references. */
      retval = J_Factory_GetNumOfCameras(hFactory, &nCameras);
      win_printf("nCam = %d",nCameras);
/*       if (retval == J_ST_SUCCESS && nCameras > 0) */
/* 	{ */
/* 	  psCameraId = (char **)calloc(nCameras, sizeof(char*)); */
/* 	  for (index = 0 ; index < nCameras ; index++) */
/* 	    { */
/* 	      *(psCameraId+index) = (char *)calloc(512,sizeof(char));//allow only 64 char in the string */
/* 	      if (*(psCameraId+index)== NULL) */
/* 		{ */
/* 		  win_printf_OK("Calloc Pb for CameraStrings"); */
/* 		  return NULL; */
/* 		} */
/* 	    } */
/* 	  sprintf(question,"Choose you camera \n"); */
/* 	  // Run through the list of found cameras */
/* 	  for ( index = 0; index < nCameras; index++) */
/* 	    { */
/* 	      size = sizeof(*(psCameraId + index)); */
/* 	      // Get CameraID */
/* 	      retval = J_Factory_GetCameraIDByIndex(hFactory, index,*(psCameraId + index), &size); */
/* 	      if (index == 0)retval = J_Factory_GetCameraIDByIndex(hFactory, index,test, &size); */
/* 	      //win_printf("index = %d \n cameraid = %s \n test = %s",index,*(psCameraId +index),test); */
/* 		  if (retval == J_ST_SUCCESS) */
/* 		    { */
/* 		      if (index == 0 ) */
/* 			{ */
/* 			  sprintf(string , "Camera %d = %s" , index, *(psCameraId+index)); */
/* 			  strcat(string," %R \n"); */
/* 			} */
/* 		      else  */
/* 			{ */
/* 			  sprintf(string , "Camera %d = %s " , index, *(psCameraId+index)); */
/* 			  strcat(string," %r \n"); */
/* 			} */
/* 		      strcat(question,string); */
/* 		    } */
/* 	    }    */
/* 	  i = win_scanf(question,&index); */
/* 	  if (i == CANCEL)  */
/* 	    { */
/* 	      win_printf_OK("END"); */
/* 	      return NULL; */
/* 	    } */
  index = 0;
  size = 512;
  if (index == 0)retval = J_Factory_GetCameraIDByIndex(hFactory, index,test, &size);
	  // Open the camera
	  retval = J_Camera_Open( hFactory, test/**(psCameraId+2)/*index*/, &hCamera);
	  if (retval != J_ST_SUCCESS)
	    {
	      win_printf_OK("Can't open camera");
	      return NULL;
	    }
/* 	} */
 
/*     } */
     return hCamera;
}


int iOpen_JAI_DataStream_and_set_buffers_for_XVIN(O_i *oi,CAM_HANDLE hCamera)
{
  
  int                        i;

  int iNumber_of_DataStream = -1;

  if (oi_JAI == NULL) return win_printf_OK("No image in for set pointers");
  if (hCamera == NULL) return win_printf_OK("Camera should be opened first");  
  //First need for datastream creation
  J_Camera_GetNumOfDataStreams  ( hCamera, &iNumber_of_DataStream ); 

  if (J_ST_SUCCESS !=J_Camera_CreateDataStream ( hCamera, (iNumber_of_DataStream > 0) ? iNumber_of_DataStream - 1 : 0 , &hDS))win_printf("Pb creating hDS");

  JAI_Im_buffer_ID  = (void  **) calloc(oi_JAI->im.n_f, sizeof(void *));
  if (JAI_Im_buffer_ID == NULL) return win_printf_OK("Could not allocate memory for Buffer ID");

  for(i = 0 ; i < oi_JAI->im.n_f ; i++)
    {
      // Announce the buffer pointer to the Acquisition engine.
      if(J_ST_SUCCESS != J_DataStream_AnnounceBuffer( hDS, oi_JAI->im.mem[i] , oi_JAI->im.nx*oi_JAI->im.ny/* *((oi_JAI->im.data_type == IS_CHAR_IMAGE) ? 1 : 2) */ , NULL, &JAI_Im_buffer_ID[i]))
	{
	  return win_printf_OK("Announce Buffer failed");
	}
      // Queueing it.
      if(J_ST_SUCCESS != J_DataStream_QueueBuffer( hDS, JAI_Im_buffer_ID[i] ))
	{
	  return win_printf_OK("Failed in Queue buffer");
	}
    }
  return D_O_K;
}


//////////////////////////////////////////////////////////////////
//                 CLOSE JAI HANDLES FUNCTIONS                  //
/////////////////////////////////////////////////////////////////
int iClose_Camera(void)
{
  retval = J_Camera_Close  (hCamera);
  if (retval == J_ST_SUCCESS)
    return win_printf_OK("Can not close Camera");
  return D_O_K;
}
int iClose_DataStream(void)
{
  retval = J_DataStream_Close (hDS);
  if (retval == J_ST_SUCCESS)
    return win_printf_OK("Can not close DataStream");
  return D_O_K;
}
int iClose_Factory(void)
{
  // Close the factory
  J_Factory_Close(hFactory);
  if (retval == J_ST_SUCCESS)
    return win_printf_OK("Can not close Factory");
  return D_O_K;
}
////////////////////////////////////////////////////////////////////////
// END OF CLOSE JAI HANDLES FUNCTIONS                                 //
///////////////////////////////////////////////////////////////////////


//--------------------------------------------------------------------------------------------------
// Create Stream Thread
//--------------------------------------------------------------------------------------------------
BOOL CreateStreamThread(void)
{

  DWORD dwThreadId;

    // Is it already created?
    if(hStreamThread)
        return FALSE;
    // Stream thread event created?
    if(hStreamEvent == NULL)
      hStreamEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    else
      ResetEvent(hStreamEvent);
    // Set the thread execution flag
    bEnableThread = TRUE;
    hStreamThread = CreateThread( 
            NULL,              // default security attributes
            0,                 // use default stack size  
            (LPTHREAD_START_ROUTINE)StreamProcess,// thread function 
            NULL,       // argument to thread function 
            0,                 // use default creation flags 
            &dwThreadId);      // returns the thread identifier 
    if(hStreamThread == NULL) 
      {
	//      J_DataStream_Close(m_hDS);
	win_printf_OK("No thread created");
	return FALSE;
      }    
    SetThreadPriority(hStreamThread,  THREAD_PRIORITY_TIME_CRITICAL);
    return TRUE;
}


//==============================================================////
// Stream Processing Function
//==============================================================////
void StreamProcess(void)
{
  J_STATUS_TYPE	iResult;
  size_t iSize;
  BUF_HANDLE  iBufferID;
  // Create structure to be used for image display
  J_tIMAGE_INFO	tAqImageInfo = {0, 0, 0, 0, NULL};
  DWORD	iWaitResult;
  HANDLE hEventNewImage = NULL;
  HANDLE	EventList[2];
  EVT_HANDLE	hEvent; // Buffer event handle
  BITMAP *imb = NULL;
  uint64_t pInfo = -1;
  BOOL bool;
  unsigned int timeout;
  NODE_HANDLE     hNode;
 /*  sprintf(win_title,"IN"); */
/*   set_window_title(win_title);   */
  // Is the kill event already defined?
  if (hEventFreeze != NULL)
    CloseHandle(hEventFreeze);
  
  // Create a new one
  hEventFreeze = CreateEvent(NULL, TRUE, FALSE, NULL);
  hEventNewImage = CreateEvent(NULL, TRUE, FALSE, NULL);
  // Make a list of events to be used in WaitForMultipleObjects
  
  EventList[0] = hEventNewImage;
  EventList[1] = hEventFreeze;
  
  // Register the event with the acquisition engine
  if (J_DataStream_RegisterEvent(hDS, EVENT_NEW_BUFFER, hEventNewImage, &hEvent) !=  J_ST_SUCCESS)
    { sprintf(win_title,"Register Failed");
      set_window_title(win_title);  
      return ;
    } 
  // win_printf("Before loop %d",(int)bool);


 // Start Acquision
  retval = J_Camera_GetNodeByName(hCamera,"AcquisitionStart", &hNode);
  retval = J_Node_ExecuteCommand(hNode);
  // Start image acquisition
  iResult = J_DataStream_StartAcquisition( hDS, ACQ_START_FLAGS_NONE, 0xFFFFFFFFFFFFFFFF );
  if (iResult != J_ST_SUCCESS)
    { 
      sprintf(win_title,"StartAcquisition Failed");
      set_window_title(win_title);  
      return ;
    }
  Sleep(15);
  iSize = sizeof (BOOL);
  J_DataStream_GetStreamInfo  ( hDS, STREAM_INFO_CMD_ISGRABBING  , &bool,  &iSize    ); 
  iSize = sizeof (unsigned int);
  J_DataStream_GetParam  ( hDS, STREAM_PARAM_CMD_RECIEVE_TIMEOUT , &timeout , &iSize ) ;
  sprintf(win_title,"Before loop %d \n timeout %d",(int)bool, timeout);
  set_window_title(win_title);
  while(bEnableThread)
    {
      // Wait for New Buffer event (or Freeze event)
      iWaitResult = WaitForMultipleObjects(2, EventList,FALSE/*TRUE*/, 10000/*00*/);
      /*       iSize = sizeof(uint64_t); */
      /*             J_DataStream_GetStreamInfo  ( hDS, STREAM_INFO_CMD_NUMBER_OF_FRAMES_DELIVERED , &pInfo,  iSize    ); */
      /*    sprintf(win_title,"BEnabl %d waitresult %d Ref message %d and tileout %d",(int)(bEnableThread),(int)iWaitResult, (int)WAIT_OBJECT_0,(int)WAIT_TIMEOUT);  */
      /*       set_window_title(win_title);  */
      // Did we get a new buffer event?
      if(WAIT_OBJECT_0 == iWaitResult)
	{
/* 	  sprintf(win_title,"Inside loop"); */
/* 	  set_window_title(win_title);   */
	  // Get the Buffer Handle from the event
	  iSize = sizeof(void *);
	  J_Event_GetData(hEvent, &iBufferID,  &iSize);
	  
	  // Fill in structure for image display
	  // Get the pointer to the frame buffer.
	  iSize = sizeof (void *);
	  iResult = J_DataStream_GetBufferInfo(hDS, iBufferID, BUFFER_INFO_BASE	, &(tAqImageInfo.pImageBuffer), &iSize);
	  // Get the effective data size.
	  iSize = sizeof (uint32_t);
	  iResult = J_DataStream_GetBufferInfo(hDS, iBufferID, BUFFER_INFO_SIZE	, &(tAqImageInfo.iImageSize), &iSize);
	  // Get Pixel Format Type.
	  iSize = sizeof (uint32_t);
	  iResult = J_DataStream_GetBufferInfo(hDS, iBufferID, BUFFER_INFO_PIXELTYPE, &(tAqImageInfo.iPixelType), &iSize);
	  // Get Frame Width.
	  iSize = sizeof (uint32_t);
	  iResult = J_DataStream_GetBufferInfo(hDS, iBufferID, BUFFER_INFO_WIDTH	, &(tAqImageInfo.iSizeX), &iSize);
	  // Get Frame Height.
	  iSize = sizeof (uint32_t);
	  iResult = J_DataStream_GetBufferInfo(hDS, iBufferID, BUFFER_INFO_HEIGHT	, &(tAqImageInfo.iSizeY), &iSize);
	  // Get Timestamp.
	  iSize = sizeof (uint64_t);
	  iResult = J_DataStream_GetBufferInfo(hDS, iBufferID, BUFFER_INFO_TIMESTAMP, &(tAqImageInfo.iTimeStamp), &iSize);
	    
	  iSize = sizeof(uint64_t);
	  
	  J_DataStream_GetStreamInfo  ( hDS, STREAM_INFO_CMD_NUMBER_OF_FRAMES_DELIVERED , &pInfo,  &iSize    );
	  if (tAqImageInfo.pImageBuffer == oi_JAI->im.mem[0])
	    {
	      sprintf(win_title,"Frames %d Buffer %d Size %d Anticipated %d",(int)(pInfo), (int)iBufferID, (int) tAqImageInfo.iImageSize,oi_JAI->im.nx*oi_JAI->im.ny);
	      set_window_title(win_title);
	    }

	  /* if(bEnableThread) */
/*             { */

	      set_zmin_zmax_values(oi_JAI, 0, 255);
	      set_z_black_z_white_values(oi_JAI, 0, 255);
	      oi_JAI->need_to_refresh |= BITMAP_NEED_REFRESH;
	      display_image_stuff_16M(imr_JAI,  d_JAI);
	      imb = (BITMAP*)oi_JAI->bmp.stuff;
	      acquire_bitmap(screen);
	      blit(imb,screen,0,0,imr_JAI->x_off + d_JAI->x, imr_JAI->y_off - imb->h + d_JAI->y, imb->w, imb->h);
	      release_bitmap(screen);
	      switch_frame(oi_JAI,(int)pInfo%oi_JAI->im.n_f);
	      /* // Display image */
	      /* 	      if(hView) */
	      /*                 { */
	      /* 		  // Shows image */
	      /* 		  J_Image_ShowImage(hView, &tAqImageInfo); */
	      /*                 } */
	      
	      // Queue This Buffer Again for reuse in acquisition engine
	      iResult = J_DataStream_QueueBuffer(hDS, iBufferID);
          /*   } */
        }
      else
        {
	  switch(iWaitResult)
            {
	      // Freeze event
	    case	WAIT_OBJECT_0 + 1:
	      iResult = 1;
	      bEnableThread = FALSE;
	      break;
	      // Timeout
	    case	WAIT_TIMEOUT:
	      iResult = 2;
	      win_printf("Timeout");
	      bEnableThread = FALSE;
	      break;
	      // Unknown?
	    default:
	      iResult = 3;
	      break;
            }
        }
    }
  sprintf(win_title,"Out loop");
  set_window_title(win_title);  
 
  // Unregister new buffer event with acquisition engine
  J_DataStream_UnRegisterEvent(hDS, EVENT_NEW_BUFFER); 
  
  // Free the event object
  if (hEvent != NULL)
    {
      J_Event_Close(hEvent);
      hEvent = NULL;
    }
   // Start Acquision
  retval = J_Camera_GetNodeByName(hCamera,"AcquisitionStop", &hNode);
  retval = J_Node_ExecuteCommand(hNode);
    // Terminate the thread.
  Terminate_JAI_Thread();
  
  // Free the kill event
    if (hEventFreeze != NULL)
      {
        CloseHandle(hEventFreeze);
        hEventFreeze = NULL;
      }
    
    // Free the new image event object
    if (hEventNewImage != NULL)
      {
        CloseHandle(hEventNewImage);
        hEventNewImage = NULL;
      }
    WaitForThreadToTerminate();
    sprintf(win_title,"Out thread");
    set_window_title(win_title);  
}




BOOL TerminateStreamThread(void)
{
    // Is the data stream opened?
    if(hDS == NULL)
        return FALSE;

    // Reset the thread execution flag.
    bEnableThread = FALSE;

    // Signal the image thread to stop faster
    SetEvent(hEventFreeze);

    // Stop the image acquisition engine
    J_DataStream_StopAcquisition(hDS, ACQ_STOP_FLAG_KILL);

    // Wait for the thread to end
    WaitForThreadToTerminate();


    return TRUE;
}






//==============================================================////
// Terminate image acquisition thread
//==============================================================////
void Terminate_JAI_Thread(void)
{
    SetEvent(hStreamEvent);
    bEnableThread = FALSE;
}

//==============================================================////
// Wait for thread to terminate
//==============================================================////

void WaitForThreadToTerminate(void)
{
    WaitForSingleObject(hStreamEvent, INFINITE);
    // Close the thread handle and stream event handle
    CloseThreadHandle();
}

//==============================================================////
// Close handles and stream
//==============================================================////
void CloseThreadHandle(void)
{
    if(hStreamThread)
    {
        CloseHandle(hStreamThread);
        hStreamThread = NULL;
    }

    if(hStreamEvent)
    {
        CloseHandle(hStreamEvent);
        hStreamEvent = NULL;
    }
}


////////////////////////////////////////////////////////////////////





int add_oi_JAI_2_imr (imreg *imr,O_i* oi)
{
  NODE_HANDLE     hNode;
  int64_t         int64Val;
  int32_t   iValue;   
  int iNx=-1, iNy=-1, iType;
  

  
  //win_printf ("IN");
  if ( imr == NULL ) return win_printf_OK("IMR NULL");//xvin_error(Wrong_Argument);

/*   if (oi == NULL) */
/*     { */
      // Get Width from the camera
      if (J_Camera_GetNodeByName(hCamera, "Width", &hNode) != J_ST_SUCCESS)   return FALSE;
      J_Node_GetValueInt64(hNode, FALSE, &int64Val);
      iNx = (int)int64Val;     // Set window size x
      // win_printf("nx %d \n ny %d",iNx,iNy);
      // Get Height from the camera
      if (J_Camera_GetNodeByName(hCamera, "Height", &hNode) != J_ST_SUCCESS)  return FALSE;
      J_Node_GetValueInt64(hNode, FALSE, &int64Val);
      iNy = (int)int64Val;     // Set window size y

      //win_printf("nx %d \n ny %d",iNx,iNy);
      
      // Get Height from the camera
      if (J_Camera_GetNodeByName(hCamera, "PixelFormat", &hNode) != J_ST_SUCCESS)  return FALSE;
      J_Node_GetEnumEntryValue  ( hNode, &iValue);
      if (iValue == 17301505/*BIT_PER_PIX_8*/) iType = IS_CHAR_IMAGE; 
      else if (iValue == 17825795/*BIT_PER_PIX_10*/) iType = IS_UINT_IMAGE;//Voir si cela ne marche pas! ...et voir autres cameras values only for A10!
      else iType = IS_UINT_IMAGE;
      iType = IS_CHAR_IMAGE; 
      win_printf("type  %d ivalue %d",iType,iValue);
      //create JAI image
      oi_JAI = create_and_attach_movie_to_imr(imr, iNx, iNy, iType, iImages_in_JAI_Buffer);

      //iOpen_JAI_DataStream_for_XVIN(oi_JAI);

      if (oi_JAI == NULL) 
	win_printf_OK("Error in allocate oi_JAI");
/*     }	 */
  return 0;
}
	

int initialize_JAI(void)
{   
  if(updating_menu_state != 0)	return D_O_K;
 

  // Open the Factory
  iOpen_Factory();
  hCam_Open_camera();
  add_oi_JAI_2_imr (imr_JAI,NULL);
  iOpen_JAI_DataStream_and_set_buffers_for_XVIN(oi_JAI,hCamera);

  return D_O_K;   

} 

int test_event(void)
{
  if(updating_menu_state != 0)	return D_O_K;
  SetEvent(hEventFreeze);
  return D_O_K;
}

int freeze_video(void)
{
  imreg *imr;
  if(updating_menu_state != 0)	return D_O_K;
  allegro_display_on = FALSE;

  TerminateStreamThread();


  imr = find_imr_in_current_dialog(NULL);  
  imr->one_i->need_to_refresh &= BITMAP_NEED_REFRESH;  
  find_zmin_zmax(imr->one_i);
  refresh_image(imr, UNCHANGED); 
  return 0;
}


int freeze_source(void)
{
  if(updating_menu_state != 0)	return D_O_K;
  freeze_video();
  return 0;
}

DWORD WINAPI DisplayThreadProc(LPVOID lpParam) 
{
	

  BITMAP * imb = NULL;
  //    	long lSequenceIndex = 0;

#ifdef MODIF        
  int seq_num = 0;
  DWORD acquiredDy;
  double t = 0;	
# endif


  while(allegro_display_on == TRUE )
    {

      if (overlay == TRUE && do_refresh_overlay == 1)
	{
			
	  set_zmin_zmax_values(oi_JAI, 0, 255);
	  set_z_black_z_white_values(oi_JAI, 0, 255);
	  oi_JAI->need_to_refresh |= BITMAP_NEED_REFRESH;            
	  display_image_stuff_16M(imr_JAI,  d_JAI);
	  imb = (BITMAP*)oi_JAI->bmp.stuff;
	  acquire_bitmap(screen);
	  blit(imb,screen,0,0,imr_JAI->x_off + d_JAI->x, imr_JAI->y_off - imb->h + d_JAI->y, imb->w, imb->h); 
	  release_bitmap(screen);
	  switch_frame(oi_JAI,(int) absolute_image_number%lNumBuff); 

	}
    }
	  
  return D_O_K;

}


int JAI_live(void)
{
  int is_live = 0;
  if(updating_menu_state != 0)	return D_O_K;

  CreateStreamThread();

  is_live  = 1;
  allegro_display_on = TRUE;
  //create_display_thread();
  return D_O_K;//live_video(-1);	
}


int live_source(void)
{
  if(updating_menu_state != 0)	return D_O_K;

  //is_live  = 1;
  return JAI_live();	
}


int start_data_movie(imreg *imr)
{
  return 0;
}

//--------------------------------------------------------------------------------------------------
// Live View sample function
//--------------------------------------------------------------------------------------------------
int LiveView(void)
{
  THRD_HANDLE     hThread;
  NODE_HANDLE     hNode;
  int64_t         int64Val;
  SIZE            ViewSize = {100, 100};
  POINT           TopLeft = {0, 0};
  RECT Rect;
  

  if(updating_menu_state != 0)	return D_O_K;


  iOpen_Factory();
  hCam_Open_camera();
  // Get Width from the camera
  if (J_Camera_GetNodeByName(hCamera, "Width", &hNode) != J_ST_SUCCESS)   return D_BAD;
  J_Node_GetValueInt64(hNode, FALSE, &int64Val);
  ViewSize.cx = (LONG)int64Val;     // Set window size cx
  
  // Get Height from the camera
  if (J_Camera_GetNodeByName(hCamera, "Height", &hNode) != J_ST_SUCCESS)  return D_BAD;
  J_Node_GetValueInt64(hNode, FALSE, &int64Val);
  ViewSize.cy = (LONG)int64Val;     // Set window size cy
  win_printf("Width %d Height %d ",(int)ViewSize.cx,(int)ViewSize.cy); 

  Rect.left =150;
  Rect.top =150;
  Rect.right = ViewSize.cx;
  Rect.bottom = ViewSize.cy;
  // Open view window
  //if (J_Image_OpenViewWindow((LPCTSTR)"JAI Live!", &TopLeft, &ViewSize, &hView) != J_ST_SUCCESS)   return D_BAD;
  J_Image_OpenViewWindowEx  (J_IVW_CHILD_STRETCH,  (LPCTSTR)"JAI Live!",  &Rect, &ViewSize,  win_get_window(),   &hView   	     ) ;

  // Open stream
  
  if (J_Image_OpenStreamLight ( hCamera, 0, &hThread) != J_ST_SUCCESS)   return D_BAD;
  //win_printf("Stream opened");
  // Acquisition Start
  if (J_Camera_GetNodeByName(hCamera, "AcquisitionStart", &hNode) != J_ST_SUCCESS)    return D_BAD;
  J_Node_ExecuteCommand(hNode);
  //win_printf("Start");
  // Message loop
  // win_printf("Any key to exit!\n");
  while (!keypressed())
    {
      //J_Image_SetPixel  ( J_tIMAGE_INFO *  pBufferInfo,    POINT *  pPoint,    void *  pPixel    ) ;

    }
  
  // Acquisition Stop
  if (J_Camera_GetNodeByName(hCamera, "AcquisitionStop", &hNode) != J_ST_SUCCESS)     return D_BAD;
  J_Node_ExecuteCommand(hNode);

  // Close stream
  J_Image_CloseStream(hThread);
  
  // Close view window
  J_Image_CloseViewWindow(hView);
  
  return D_O_K;
}




 
 // Call back sample function
static void __stdcall StreamCBFunc(J_tIMAGE_INFO * pAqImageInfo)
{
   POINT                  TopLeft = {50, 50};
   
   J_tIMAGE_INFO tCnvImageInfo;    // Image info structure
   static int i = 0;
   int pixelvalue = 0xFF;
   
  /*  if (i == 0) */
/*      { */
/*        // Allocate the buffer to hold converted the image */
/*        if (J_Image_Malloc(pAqImageInfo, &tCnvImageInfo) != J_ST_SUCCESS)             return FALSE; */
/*      } */
/*    i++; */
/*        // Convert the raw image to image format */
/*    if (J_Image_FromRawToImage(pAqImageInfo, &tCnvImageInfo,0x01000,0x01000,0x01000) != J_ST_SUCCESS)     return FALSE; */
   
/*    J_Image_SetPixel  ( &tCnvImageInfo, &TopLeft, &pixelvalue ) ; */
     
/*    J_Image_ShowImage(g_hView, &tCnvImageInfo,0x01000,0x01000,0x01000); */
   // Free up the image buffer
   //if (J_Image_Free(&tCnvImageInfo) != J_ST_SUCCESS)                               return FALSE;
}

// Live View sample function
int LivePixelView(void)
{
  THRD_HANDLE     hThread;
  NODE_HANDLE     hNode;
  int64_t         int64Val;
  SIZE            ViewSize = {100, 100};
  POINT           TopLeft = {0, 0};
  RECT Rect;
  void *vfptr = NULL;
  J_IMG_CALLBACK_FUNCTION *cbfptr = NULL;
 
  //if(updating_menu_state != 0)	return D_O_K;
  
  
  // Get Width from the camera
  if (J_Camera_GetNodeByName(hCamera, "Width", &hNode) != J_ST_SUCCESS)   return FALSE;
  J_Node_GetValueInt64(hNode, FALSE, &int64Val);
  ViewSize.cx = (LONG)int64Val;     // Set window size cx
  
  // Get Height from the camera
  if (J_Camera_GetNodeByName(hCamera, "Height", &hNode) != J_ST_SUCCESS)  return FALSE;
  J_Node_GetValueInt64(hNode, FALSE, &int64Val);
  ViewSize.cy = (LONG)int64Val;     // Set window size cy

  Rect.left =150;
  Rect.top =150;
  Rect.right = ViewSize.cx;
  Rect.bottom = ViewSize.cy;
  
  // Open view window
  J_Image_OpenViewWindowEx  (J_IVW_CHILD_STRETCH,  (LPCTSTR)"JAI Live!",  &Rect, &ViewSize,  win_get_window(),   &g_hView   	     ) ;
 
  //if (J_Image_OpenViewWindow((LPCTSTR)"Live view!", &TopLeft, &ViewSize, &g_hView) != J_ST_SUCCESS)   return FALSE;
  //win_printf("Before open Stream");
  // Open stream
  vfptr = (void*)(StreamCBFunc);
  cbfptr =  (J_IMG_CALLBACK_FUNCTION*)(&vfptr);
  if (J_Image_OpenStream(hCamera, 0, NULL, NULL/*cbfptr*/, &hThread, ViewSize.cx*ViewSize.cy*16,0) != J_ST_SUCCESS)   return FALSE;
  
  // Acquisition Start
  if (J_Camera_GetNodeByName(hCamera, "AcquisitionStart", &hNode) != J_ST_SUCCESS)    return FALSE;
  J_Node_ExecuteCommand(hNode);
  
  // Message loop
  //printf("Any key to exit!");
  while (!keypressed())
    {
      
    }
  

  // Acquisition Stop
  if (J_Camera_GetNodeByName(hCamera, "AcquisitionStop", &hNode) != J_ST_SUCCESS)     return FALSE;
  J_Node_ExecuteCommand(hNode);
  
  // Close stream
  J_Image_CloseStream(hThread);
  
  // Close view window
  J_Image_CloseViewWindow(g_hView);
  return D_O_K;
}

MENU *JAI_source_image_menu(void)
{
  static MENU mn[32];
    
  if (mn[0].text != NULL)	return mn;

  //  add_item_to_menu(mn,"Init JAI", initialize_JAI ,NULL,0,NULL);
  add_item_to_menu(mn,"Live JAI", JAI_live ,NULL,0,NULL);
  add_item_to_menu(mn,"Freeze JAI", freeze_video ,NULL,0,NULL);
  add_item_to_menu(mn,"Test", test_event ,NULL,0,NULL);
  add_item_to_menu(mn,"View JAI",LiveView ,NULL,0,NULL);
  add_item_to_menu(mn,"View JAI bis",LivePixelView ,NULL,0,NULL);
    
  return mn;
    
}


int init_image_source(void)
{
  imreg *imr = NULL; 

  imr = create_and_register_new_image_project( 0,   32,  900,  668);
  if (imr == NULL) 
    return win_printf_OK("Patate t'es mal barre!");
  imr_JAI = imr;
  add_image_treat_menu_item ( "JAI Genicam", NULL, JAI_source_image_menu(), 0, NULL);
  initialize_JAI(); // Initializes JAI 
  remove_from_image (imr, imr->o_i[0]->im.data_type, (void *)imr->o_i[0]);
  //imr_TRACK = NULL;
  return 0;
}

int JAI_source_main(void)
{
  imreg *imr = NULL;
  O_i *oi = NULL;

  win_printf("IN");
  LiveView();  

/*   imr = find_imr_in_current_dialog(NULL); */
/*   if (imr == NULL)    init_image_source(); */
/*   else */
/*     { */
/*         if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2) */
/* 	  init_image_source(); */
/* 	if (oi->im.movie_on_disk == 0 || oi->im.n_f < 2) */
/* 	  init_image_source(); */

/*     } */
/*   if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2) */
/*     return win_printf_OK("You must load a movie in the active window"); */

/*   d_JAI = find_dialog_associated_to_imr(imr_JAI, NULL); */
/*   before_menu_proc = JAI_before_menu; */
/*   after_menu_proc = JAI_after_menu; */
/*   oi_JAI->oi_got_mouse = JAI_oi_got_mouse; */
/*   //  oi_JAI->oi_idle_action = JAI_oi_idle_action; */
/*   oi_JAI->oi_mouse_action = JAI_oi_mouse_action; */

/*   general_end_action = source_end_action; */
/*   //  general_idle_action = source_idle_action; */

/*   //  add_image_treat_menu_item ( "JAI Genicam", NULL, JAI_source_image_menu(), 0, NULL); // JAI user menus */
       
  return D_O_K;
}

/** Unload function for the  JAI.c menu.*/
int	JAI_unload(void)
{
  remove_item_to_menu(image_treat_menu, "JAI Genicam", NULL, NULL);
  return D_O_K;
}



#endif

