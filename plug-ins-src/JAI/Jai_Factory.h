////////////////////////////////////////////////////////////////////////////////////////////
/// \file		Jai_Factory.h
/// \brief		JAI SDK
/// \version	Revison: 0.1
/// \author		mat, kic (JAI)
////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _JAI_FACTORY_H
#define _JAI_FACTORY_H

#include <windows.h>
#include "Jai_Types.h"
#include "Jai_Error.h"

#ifdef FACTORY_EXPORTS
#define GCFC_IMPORT_EXPORT __declspec(dllexport)
#else
#define GCFC_IMPORT_EXPORT __declspec(dllimport)
#endif
  
#ifdef __cplusplus
#ifndef EXTERN_C
#define EXTERN_C extern "C"
#endif
#else
#define EXTERN_C
#endif

#define J_REG_DATABASE	"$(JAISDK_ROOT)\\bin\\Win32_i86\\Registry.xml"	///< Default location of the JAI SDK Registry database file
#define J_ROOT_NODE		"Root"											///< Name of the topmost node in the GenICam Node tree

#define J_FACTORY_INFO_SIZE (512)    ///< Maximum size needed for factory information strings
#define J_CAMERA_ID_SIZE    (512)    ///< Maximum size needed for camera ID strings
#define J_CAMERA_INFO_SIZE  (512)    ///< Maximum size needed for camera information strings
#define J_STREAM_INFO_SIZE  (512)    ///< Maximum size needed for data stream information strings
#define J_CONFIG_INFO_SIZE  (512)    ///< Maximum size needed for config information strings


typedef void* FACTORY_HANDLE;		///< Factory object handle type, obtained by J_Factory_Open()
typedef void* CAM_HANDLE;           ///< Camera object handle type, obtained by J_Camera_Open()
typedef void* STREAM_HANDLE;        ///< Data Stream object handle type, obtained by J_Camera_CreateDataStream()
typedef void* EVT_HANDLE;           ///< Event handle type, obtained by J_Event_Register()
typedef void* BUF_HANDLE;           ///< Buffer object handle type, obtained by J_DataStream_AnnounceBuffer()
typedef	void* NODE_HANDLE;		    ///< Node object handle type, obtained by J_Camera_GetNode****
typedef	void* VIEW_HANDLE;          ///< View object handle type, obtained by J_Image_OpenViewWindow() or J_Image_OpenViewWindowEx()
typedef void* THRD_HANDLE;          ///< Thread object handle type, obtained by J_Image_OpenStream() or J_Image_OpenStreamLight()

/////////////////////////////////////////////////////////////////////////////////////
/// \brief Factory information type enumeration.
/// \note This enum is used in the \c J_Factory_GetInfo() function to retrieve detailed information about the \c Jai_Factory.dll
/// \sa \c J_Factory_GetInfo()
/////////////////////////////////////////////////////////////////////////////////////
typedef enum _J_FACTORY_INFO
  {
    FAC_INFO_VERSION,         ///< \c Jai_Factory.dll Version
    FAC_INFO_BUILDDATE,       ///< \c Jai_Factory.dll Build date
    FAC_INFO_BUILDTIME,       ///< \c Jai_Factory.dll Build time
    FAC_INFO_MANUFACTURER     ///< \c Jai_Factory.dll Manufacturer info
  } J_FACTORY_INFO;

/////////////////////////////////////////////////////////////////////////////////////
/// \brief Camera information type enumeration. This enumeration is used in \c J_Factory_GetCameraInfo() to acquire detailed information about each detected camera.
/// \note The camera connection does not need to be established in order to get this information. The information is retrieved during \b Device \b Discovery.
/// \sa \c J_Factory_GetCameraInfo()
/////////////////////////////////////////////////////////////////////////////////////
typedef enum _J_CAMERA_INFO
  {
    CAM_INFO_MANUFACTURER = 0,    //!< Manufacturer name
    CAM_INFO_MODELNAME,           //!< Model name
    CAM_INFO_IP,                  //!< IP address
    CAM_INFO_MAC,                 //!< MAC address
    CAM_INFO_SERIALNUMBER,        //!< Serial number
    CAM_INFO_USERNAME,            //!< User name
    CAM_INFO_INTERFACE_ID = 100,  //!< Interface name
  } J_CAMERA_INFO;

/// \brief The J_CONFIG_INFO is used to obtain information about a camera configuration file (XML-file)
typedef enum _J_CONFIG_INFO
  {
    CONF_INFO_MODEL_NAME = 0,       //!< Model name (CV-A70GE...)
    CONF_INFO_VENDOR_NAME,          //!< Vendor name (JAI)
    CONF_INFO_TOOL_TIP,             //!< Tool tip. Short description about the configuration file
    CONF_INFO_STANDARD_NAME_SPACE,  //!< Standard name space (GigE Vision, IEEE...)
    CONF_INFO_GENAPI_VERSION,       //!< Version of the DLL's GenApi implementation
    CONF_INFO_SCHEMA_VERSION,       //!< GenICam Schema version number
    CONF_INFO_DEVICE_VERSION,       //!< Version of the configuration file
    CONF_INFO_PRODUCT_GUID,         //!< Product GUID. Unique identifier for the camera. This will be unique to the camera type
    CONF_INFO_VERSION_GUID          //!< Version GUID. Unique identifier for the XML-file version. This will help to identify the current XML-file version
  } J_CONFIG_INFO;


/// \brief The J_ACQ_QUEUE_TYPE is used to select queue type to be flushed using \c J_DataStream_FlushQueue()
typedef enum  _J_ACQ_QUEUE_TYPE
  {
    ACQ_QUEUE_INPUT_TO_OUTPUT   = 0,    ///< Flush queue of frames, waiting for new data through the acquisition engine to the output queue
    ACQ_QUEUE_OUTPUT_DISCARD    = 1     ///< Flush queue of frames, containing new data, waiting to be delivered
    //ACQ_QUEUE_OUTPUT_TO_INPUT                           = 2   ///< Flush queue of frames, containing new data, waiting to be delivered
  } J_ACQ_QUEUE_TYPE;

/// \brief The J_ACQ_START_FLAGS are used to control the start of image acquisition using \c J_DataStream_StartAcquisition()
typedef enum  _J_ACQ_START_FLAGS
  {
    ACQ_START_FLAGS_NONE  = 0,              ///< No flags.       
    ACQ_START_NEXT_IMAGE  = 0x1,            ///< Deliver the image acquired after the image delivered last <br>
                                            ///< Missed images only happen if the acquisiton queue is empty and therefore no buffers available
    ACQ_START_LAST_IMAGE  = 0x2,            ///< Deliver the last complete image acquired <br>
                                            ///< This means that between the image deliverd last and the image delivered next can be a number of missed images <br>
                                            ///< Missed images happen if one processing cycle takes to long <br>
                                            ///< The positive effect is that the acquired image is not out of the loop but max one image behind
    ACQ_START_NEW_IMAGE   = 0x3             ///< Deliver the new image acquired while the wait for buffer function is called <br>
                                            ///< The positive effect is that the acquired image is not out of the loop
  } J_ACQ_START_FLAGS;

/// \brief The J_ACQ_STOP_FLAGS are used to control the start of image acquisition using \c J_DataStream_StartAcquisition()
typedef enum  _J_ACQ_STOP_FLAGS
  {
    /// So Stop Flag set
    ACQ_STOP_FLAGS_NONE = 0,                ///< No stop flags  
    ACQ_STOP_FLAG_KILL = 1                  ///< Kill ongoing acquisition instead of waiting for the next image
  } J_ACQ_STOP_FLAGS;


/// \brief The J_BUFFER_INFO_CMD is used to get static buffer information \c J_DataStream_GetBufferInfo()
typedef enum _J_BUFFER_INFO_CMD
  {
    BUFFER_INFO_BASE,             ///< Base address of delivered buffer (void *)
    BUFFER_INFO_SIZE,             ///< Size in Bytes ( size_t )
    BUFFER_INFO_USER_PTR,         ///< Private Pointer ( void * )
    BUFFER_INFO_TIMESTAMP,        ///< Timestamp (uint64_t)
    BUFFER_INFO_NUMBER,           ///< Buffer Number as announced (uint64_t)
    BUFFER_INFO_NEW_DATA,         ///< Flag if Buffer contains new data since it was queued
    BUFFER_INFO_ISQUEUED,
    BUFFER_INFO_PAYLOADTYPE,
    BUFFER_INFO_PIXELTYPE,
    BUFFER_INFO_WIDTH,
    BUFFER_INFO_HEIGHT,
    BUFFER_INFO_XOFFSET,
    BUFFER_INFO_YOFFSET,
    BUFFER_INFO_XPADDING,
    BUFFER_INFO_YPADDING,
    BUFFER_INFO_NUM_PACKETS_MISSING
  } J_BUFFER_INFO_CMD;

/// \brief The STREAM_INFO_CMD is used to get acquisition information \c J_DataStream_GetStreamInfo()
typedef enum _J_STREAM_INFO_CMD
  {
    STREAM_INFO_CMD_NONE                                 = 0,

    STREAM_INFO_CMD_ISGRABBING                           = 1, ///< bool8_t inquire if the acquisition engine is running. 
    STREAM_INFO_CMD_NUMBER_OF_FRAMES_DELIVERED           = 2, ///< uint64_t inquire the number of acquired frames since the last star.  
    STREAM_INFO_CMD_NUMBER_OF_FRAMES_LOST_QUEUE_UNDERRUN = 3, ///< uint64_t number of lost frames due to a queue under run
    STREAM_INFO_CMD_NUMBER_OF_FRAMES_ANNOUNCED           = 4, ///< size_t number of announced frames
    STREAM_INFO_CMD_NUMBER_OF_FRAMES_QUEUED              = 5, ///< size_t number of frames queued for acquisition
    STREAM_INFO_CMD_NUMBER_OF_FRAMES_AWAIT_DELIVERY      = 6, ///< size_t number of queued await delivery
    STREAM_INFO_CMD_NUMBER_OF_FRAMES_CORRUPT_ON_DELIEVRY = 7, ///< uint64_t number of frames which has been corrupted

    STREAM_INFO_CMD_CUSTOM_INFO                          = 1000 ///< every command with an id above this is for custom TLC use
  } J_STREAM_INFO_CMD;

/// \brief Recommended visibility of an GenICam node
typedef enum _J_NODE_VISIBILITY
  {
    Beginner = 0,				//!< Always visible
    Expert = 1,					//!< Visible for experts or Gurus
    Guru = 2,					//!< Visible for Gurus
    Invisible = 3,				//!< Not Visible
    _UndefinedVisibility  = 99	//!< Object is not yetinitialized
  } J_NODE_VISIBILITY;

/// \brief The GenICam interface node type of a specific node
typedef enum _J_NODE_TYPE
  {
    J_UnknowNodeType = 1,	//!< Unknown node type
    J_INode        = 100,	//!< INode node
    J_ICategory,			//!< ICategory node
    J_IInteger,			//!< IInteger node
    J_IEnumeration,		//!< IEnumeration node
    J_IEnumEntry,			//!< node IEnumEntry
    J_IMaskedIntReg,		//!< node IMaskedIntReg
    J_IRegister,			//!< IRegister node
    J_IIntReg,			//!< IIntReg node
    J_IFloat,				//!< IFloat node
    J_IFloatReg,			//!< IFloatReg node
    J_ISwissKnife,		//!< ISwissKnife node
    J_IIntSwissKnife,		//!< IIntSwissKnife node
    J_IIntKey,			//!< IIntKey node
    J_ITextDesc,			//!< ITextDesc node
    J_IPort,				//!< IPort node
    J_IConfRom,			//!< IConfRom node
    J_IAdvFeatureLock,	//!< IAdvFeatureLock node
    J_ISmartFeature,		//!< ISmartFeature node
    J_IStringReg,			//!< IStringReg node
    J_IBoolean,			//!< IBoolean node
    J_ICommand,			//!< ICommand node
    J_IConverter,			//!< IConverter node
    J_IIntConverter,		//!< IIntConverter node

    J_IChunkPort,			//!< IChunkPort node
    J_INodeMap,			//!< INodeMap node
    J_INodeMapDyn,		//!< INodeMapDyn node
    J_IDeviceInfo,		//!< IDeviceInfo node
    J_ISelector,			//!< ISelector node
    J_IPortConstruct		//!< IPortConstruct node 
  } J_NODE_TYPE;

/// \brief Access mode of a GenICam node
typedef enum _J_NODE_ACCESSMODE
  {
    NI,					//!< Not implemented
    NA,					//!< Not available
    WO,					//!< Write Only
    RO,					//!< Read Only
    RW,					//!< Read and Write
    _UndefinedAccesMode //!< Object is not yetinitialized
  } J_NODE_ACCESSMODE; 

/// \brief Defines if a node name is standard or custom namespace
typedef enum _J_NODE_NAMESPACE
  {
    Custom,             //!< name resides in custom namespace
    Standard,           //!< name resides in one of the standard namespaces
    _UndefinedNameSpace //!< Object is not yetinitialized
  } J_NODE_NAMESPACE;

/// \brief Caching mode of a GenICam register node
typedef enum _J_NODE_CACHINGMODE
  {
    NoCache,								//!< Do not use cache
    WriteThrough,						//!< Write to cache and register
    WriteAround,						//!< Write to register, write to cache on read
    _UndefinedCachingMode		//!< Not yet initialized
  } J_NODE_CACHINGMODE;

/// \brief GenICam Error Info
typedef struct {
  char sDescription[1024];
  char sNodeName[128];
} tGenICamErrorInfo;

/// \brief Device access Flags, used by J_Camera_OpenMc
typedef enum _J_DEVICE_ACCESS_FLAGS_TYPE_T_
  {
    DEVICE_ACCESS_NONE = 0,			       //!< Access denied
    DEVICE_ACCESS_READONLY = 1,		     //!< Host has / can have Read Only access to the device
    DEVICE_ACCESS_CONTROL = 2,         //!< Host has / can have Control Mode access to the device
    DEVICE_ACCESS_EXCLUSIVE = 4,       //!< Host has / can have Exclusive Mode access to the device

    DEVICE_ACCESS_MAX_ID
  } J_DEVICE_ACCESS_FLAGS;

/// \brief The J_STREAM_PARAM_CMD is used to get/set parameters of streaming using \c J_DataStream_GetStreamParam() and \c J_DataStream_SetStreamParam()
typedef enum _J_STREAM_PARAM_CMD
  {
    STREAM_PARAM_CMD_CAN_RESEND = 0x1,                    //!< uint32_t flag indicating if the source can handle resends. !=0: Enable resends(default), =0: Disable resends
    STREAM_PARAM_CMD_TOTALNUMBEROFBYTESPERFRAME ,         //!< xx
    STREAM_PARAM_CMD_PASS_CORRUPT_FRAMES,                 //!< uint32_t flag indicating if corrupt frames are passed to the layer above
    STREAM_PARAM_CMD_MAX_FRAMES_IN_NOT_COMPLETE_LIST,     //!< uint32_t max number of frames in not complete list 
    STREAM_PARAM_CMD_RECIEVE_TIMEOUTS_BEFORE_LISTFLUSH,   //!< uint32_t number of recieve timeouts before incomplete list is flushed / delivered
    STREAM_PARAM_CMD_OOO_PACKETS_BEFORE_RESEND,           //!< uint32_t Out of order Packets before resend is issued
    STREAM_PARAM_CMD_RECIEVE_TIMEOUT,                     //!< uint32_t Timeout in ms for recieve
    STREAM_PARAM_CMD_MAX_ID
  }  J_STREAM_PARAM_CMD;


//---------------------------------------------------------------------------------------------------------------------------------------------------

#define	J_XGA_WIDTH		(1024)
#define	J_XGA_HEIGHT	(768)
#define	J_SXGA_WIDTH	(1280)
#define	J_SXGA_HEIGHT	(1024)
#define	J_NUM_OF_BUFFER	(10)
#define	J_MAX_BPP		(4)
#define	J_BUFFER_SIZE	(J_XGA_WIDTH * J_XGA_HEIGHT * J_MAX_BPP)

/// \brief Window type used in \c J_Image_OpenViewWindowEx()
typedef enum _J_IVW_WINDOW_TYPE
  {
    J_IVW_OVERLAPPED = 0,		//!< Overlapped window
    J_IVW_CHILD = 1,			//!< Child window
    J_IVW_CHILD_STRETCH = 2,	//!< Stretched child window
  } J_IVW_WINDOW_TYPE;

//===================================================
// PIXEL TYPES
//===================================================
// Indicate if pixel is monochrome or RGB
#define J_GVSP_PIX_MONO	0x01000000
#define J_GVSP_PIX_RGB	0x02000000
#define J_GVSP_PIX_CUSTOM	0x80000000
#define J_GVSP_PIX_COLOR_MASK	0xFF000000

// Indicate effective number of bits occupied by the pixel (including padding).
// This can be used to compute amount of memory required to store an image.
#define J_GVSP_PIX_OCCUPY8BIT 0x00080000
#define J_GVSP_PIX_OCCUPY12BIT 0x000C0000
#define J_GVSP_PIX_OCCUPY16BIT 0x00100000
#define J_GVSP_PIX_OCCUPY24BIT 0x00180000
#define J_GVSP_PIX_OCCUPY32BIT 0x00200000
#define J_GVSP_PIX_OCCUPY36BIT 0x00240000
#define J_GVSP_PIX_OCCUPY48BIT 0x00300000
#define J_GVSP_PIX_EFFECTIVE_PIXEL_SIZE_MASK 0x00FF0000
#define J_GVSP_PIX_EFFECTIVE_PIXEL_SIZE_SHIFT 16

// Pixel ID: lower 16-bit of the pixel type
#define J_GVSP_PIX_ID_MASK 0x0000FFFF

// 26.1 Mono buffer format defines
#define J_GVSP_PIX_MONO8 (J_GVSP_PIX_MONO | J_GVSP_PIX_OCCUPY8BIT | 0x0001)
#define J_GVSP_PIX_MONO8_SIGNED (J_GVSP_PIX_MONO | J_GVSP_PIX_OCCUPY8BIT | 0x0002)
#define J_GVSP_PIX_MONO10 (J_GVSP_PIX_MONO | J_GVSP_PIX_OCCUPY16BIT | 0x0003)
#define J_GVSP_PIX_MONO10_PACKED (J_GVSP_PIX_MONO | J_GVSP_PIX_OCCUPY12BIT | 0x0004)
#define J_GVSP_PIX_MONO12 (J_GVSP_PIX_MONO | J_GVSP_PIX_OCCUPY16BIT | 0x0005)
#define J_GVSP_PIX_MONO12_PACKED (J_GVSP_PIX_MONO | J_GVSP_PIX_OCCUPY12BIT | 0x0006)
#define J_GVSP_PIX_MONO16 (J_GVSP_PIX_MONO | J_GVSP_PIX_OCCUPY16BIT | 0x0007)

// 26.2 Bayer buffer format defines
#define J_GVSP_PIX_BAYGR8 (J_GVSP_PIX_MONO | J_GVSP_PIX_OCCUPY8BIT | 0x0008)
#define J_GVSP_PIX_BAYRG8 (J_GVSP_PIX_MONO | J_GVSP_PIX_OCCUPY8BIT | 0x0009)
#define J_GVSP_PIX_BAYGB8 (J_GVSP_PIX_MONO | J_GVSP_PIX_OCCUPY8BIT | 0x000A)
#define J_GVSP_PIX_BAYBG8 (J_GVSP_PIX_MONO | J_GVSP_PIX_OCCUPY8BIT | 0x000B)
#define J_GVSP_PIX_BAYGR10 (J_GVSP_PIX_MONO | J_GVSP_PIX_OCCUPY16BIT | 0x000C)
#define J_GVSP_PIX_BAYRG10 (J_GVSP_PIX_MONO | J_GVSP_PIX_OCCUPY16BIT | 0x000D)
#define J_GVSP_PIX_BAYGB10 (J_GVSP_PIX_MONO | J_GVSP_PIX_OCCUPY16BIT | 0x000E)
#define J_GVSP_PIX_BAYBG10 (J_GVSP_PIX_MONO | J_GVSP_PIX_OCCUPY16BIT | 0x000F)
#define J_GVSP_PIX_BAYGR12 (J_GVSP_PIX_MONO | J_GVSP_PIX_OCCUPY16BIT | 0x0010)
#define J_GVSP_PIX_BAYRG12 (J_GVSP_PIX_MONO | J_GVSP_PIX_OCCUPY16BIT | 0x0011)
#define J_GVSP_PIX_BAYGB12 (J_GVSP_PIX_MONO | J_GVSP_PIX_OCCUPY16BIT | 0x0012)
#define J_GVSP_PIX_BAYBG12 (J_GVSP_PIX_MONO | J_GVSP_PIX_OCCUPY16BIT | 0x0013)

// 26.3 RGB Packed buffer format defines
#define J_GVSP_PIX_RGB8_PACKED (J_GVSP_PIX_RGB | J_GVSP_PIX_OCCUPY24BIT | 0x0014)
#define J_GVSP_PIX_BGR8_PACKED (J_GVSP_PIX_RGB | J_GVSP_PIX_OCCUPY24BIT | 0x0015)
#define J_GVSP_PIX_RGBA8_PACKED (J_GVSP_PIX_RGB | J_GVSP_PIX_OCCUPY32BIT | 0x0016)
#define J_GVSP_PIX_BGRA8_PACKED (J_GVSP_PIX_RGB | J_GVSP_PIX_OCCUPY32BIT | 0x0017)
#define J_GVSP_PIX_RGB10_PACKED (J_GVSP_PIX_RGB | J_GVSP_PIX_OCCUPY48BIT | 0x0018)
#define J_GVSP_PIX_BGR10_PACKED (J_GVSP_PIX_RGB | J_GVSP_PIX_OCCUPY48BIT | 0x0019)
#define J_GVSP_PIX_RGB12_PACKED (J_GVSP_PIX_RGB | J_GVSP_PIX_OCCUPY48BIT | 0x001A)
#define J_GVSP_PIX_BGR12_PACKED (J_GVSP_PIX_RGB | J_GVSP_PIX_OCCUPY48BIT | 0x001B)
#define J_GVSP_PIX_RGB10V1_PACKED (J_GVSP_PIX_RGB | J_GVSP_PIX_OCCUPY32BIT | 0x001C)
#define J_GVSP_PIX_RGB10V2_PACKED (J_GVSP_PIX_RGB | J_GVSP_PIX_OCCUPY32BIT | 0x001D)

// 26.4 YUV Packed buffer format defines
#define J_GVSP_PIX_YUV411_PACKED (J_GVSP_PIX_RGB | J_GVSP_PIX_OCCUPY12BIT | 0x001E)
#define J_GVSP_PIX_YUV422_PACKED (J_GVSP_PIX_RGB | J_GVSP_PIX_OCCUPY16BIT | 0x001F)
#define J_GVSP_PIX_YUV444_PACKED (J_GVSP_PIX_RGB | J_GVSP_PIX_OCCUPY24BIT | 0x0020)

// 26.5 RGB Planar buffer format defines
#define J_GVSP_PIX_RGB8_PLANAR (J_GVSP_PIX_RGB | J_GVSP_PIX_OCCUPY24BIT | 0x0021)
#define J_GVSP_PIX_RGB10_PLANAR (J_GVSP_PIX_RGB | J_GVSP_PIX_OCCUPY48BIT | 0x0022)
#define J_GVSP_PIX_RGB12_PLANAR (J_GVSP_PIX_RGB | J_GVSP_PIX_OCCUPY48BIT | 0x0023)
#define J_GVSP_PIX_RGB16_PLANAR (J_GVSP_PIX_RGB | J_GVSP_PIX_OCCUPY48BIT | 0x0024)


// Internal use only
#define	J_GVSP_PIX_BGR16_PACKED_INTERNAL (J_GVSP_PIX_RGB | J_GVSP_PIX_OCCUPY48BIT | 0x0000)

#define J_BPP(x) (((uint32_t)x & J_GVSP_PIX_EFFECTIVE_PIXEL_SIZE_MASK) >>(uint32_t)J_GVSP_PIX_EFFECTIVE_PIXEL_SIZE_SHIFT)/8

/// \brief	Image information structure used in callback and image conversion functions
typedef struct _J_tIMAGE_INFO 
{
  uint32_t	iPixelType;		    ///< Pixel Format Type
  uint32_t	iSizeX;			      ///< Image width
  uint32_t	iSizeY;			      ///< Image height
  uint32_t	iImageSize;		    ///< Number of bytes for image
  uint8_t		*pImageBuffer;	  ///< Buffer pointer
  uint64_t	iTimeStamp;		    ///< Timestamp
  uint32_t	iMissingPackets;	///< Number of missing packets
} J_tIMAGE_INFO;

// R/G/B structure
typedef struct _J_tDIB24 {
  BYTE	B;
  BYTE	G;
  BYTE	R;
} J_tDIB24;

#define	J_tBGR24	J_tDIB24

// Alpha/R/G/B structure
typedef struct _J_tDIB32 {
  J_tDIB24	BGR;
  BYTE	Alpha;
} J_tDIB32;

// Structure of word three data
typedef struct _J_tBGR48 {
  uint16_t	B16;
  uint16_t	G16;
  uint16_t	R16;
} J_tBGR48;

/*JF MODIFICATION*/
/* class CJDummyClass */
/* { */
/* }; */
/* typedef CJDummyClass * J_IMG_CALLBACK_OBJECT; */
/* typedef void (CJDummyClass::*J_IMG_CALLBACK_FUNCTION)(J_tIMAGE_INFO * pAqImageInfo); */



typedef void* J_IMG_CALLBACK_OBJECT;
typedef void *J_IMG_CALLBACK_FUNCTION(J_tIMAGE_INFO * pAqImageInfo);

//END MOFICATION


typedef void (__stdcall *J_STATIC_GVSP_CALLBACK_FUNCTION)(J_tIMAGE_INFO * pAqImageInfo);

typedef void (__stdcall *J_NODE_CALLBACK_FUNCTION)(void * pNode);

/// @}
/// \defgroup WRAP_FACTORY	Factory specific functions
/// @{

//******************************************************************************************************************
///  \brief                        Open the factory and return a valid handle
///  \param [in] psPrivateData     The name of the registry database. Usually J_REG_DATABASE should be specified.
///  \param [out] pHandle          Pointer to a variable in which a handle of the factory object is stored.
///  \retval                       Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note                         This function creats and open a new factory object.
///                                J_Factory_Close function must be called when the factory object is no longer needed.
/// \sa \c J_Factory_Close(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Factory_Open(const int8_t* psPrivateData, FACTORY_HANDLE *pHandle);

//******************************************************************************************************************
///  \brief                        Close a previously opened factory handle
///  \param [in] hFac              Handle to a valid factory object, obtained by \c J_Factory_Open()
///  \retval                       Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  When the factory is closed the underlaying transport layer and interfaces will be automatically closed and released as well
///  \sa  \c J_Factory_Open(),  \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Factory_Close(FACTORY_HANDLE hFac);

//******************************************************************************************************************
///  \brief                        Get information about the \c Jai_Factory.dll
///  \param [in] info              The type of information from the factory that is requested
///  \param [out] pBuffer          Pointer to a buffer in which the factory information is stored.
///  \param [in,out] pSize         Specify the size of the buffer. It must be equal or greater then J_FACTORY_INFO_SIZE.
///                                The function sets the actual size of data stored into the buffer.
///  \retval                       Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
/// \par
/// \code
/// // C Sample demonstrating how to read the version information from the factory
/// 
/// int8_t sVersion[J_FACTORY_INFO_SIZE];
/// size_t size = sizeof(sVersion);
/// J_Factory_GetInfo(FAC_INFO_VERSION, sVersion, &size);
/// printf("Version of Factory = %s\n", sVersion);
/// \endcode
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Factory_GetInfo(J_FACTORY_INFO info, int8_t* pBuffer, size_t* pSize);

//******************************************************************************************************************
///  \brief                        Update a list of all cameras connected to interfaces in the factory object.
///  \param [in] hFac              Handle to a valid factory object, obtained by \c J_Factory_Open()
///  \param [out] bHasChanged      A bool that tells if there is found any new cameras
///  \retval                       Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///
/// \par
/// \code
/// // C Sample that perform Device Discovery and opens all cameras found
/// 
/// J_STATUS_TYPE   retval;
/// bool8_t         bHasChange;
/// uint32_t        nCameras;
/// int8_t          sCameraId[J_CAMERA_ID_SIZE];
/// size_t          size;
/// 
/// // Perform Device Discovery
/// retval = J_Factory_UpdateCameraList(hFactory, &bHasChange);
/// if (retval == J_ST_SUCCESS)
/// {
///   // Get the number of cameras found
///   retval = J_Factory_GetNumOfCameras(hFactory, &nCameras);
///   if (retval == J_ST_SUCCESS && nCameras > 0)
///   {
///     // Run through the list of cameras found and get the unique camera ID's
///     for (uint32_t index = 0; index < nCameras; ++index)
///     {
///       size = sizeof(sCameraId);
///       retval = J_Factory_GetCameraIDByIndex(hFactory, index, sCameraId, &size);
///       if (retval == J_ST_SUCCESS)
///       {
///        CAM_HANDLE  hCamera;
///         // And open it
///         J_Camera_Open(hFactory, sCameraId, &hCamera);
///       }
///     }
///   }
/// }
/// \endcode
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Factory_UpdateCameraList(FACTORY_HANDLE hFac, BOOL *bHasChanged);
                                                                       
//******************************************************************************************************************
/// \brief                        Get the number of cameras found during \b Device \b Discovery with \c J_Factory_UpdateCameraList().
/// \param [in] hFac              Handle to a valid factory object, obtained by \c J_Factory_Open() function.
/// \param [out] pNum             Pointer to a variable in which the number of cameras is stored.
/// \retval                       Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///
/// \sa \c J_Factory_UpdateCameraList(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Factory_GetNumOfCameras(FACTORY_HANDLE hFac, uint32_t *pNum);

//******************************************************************************************************************
/// \brief                       Get a string that uniquely identifies the camera, interface and the transport layer. \n
///	                             This string will be used to open the camera later on with \c J_Camera_Open() \n
///                              ex: "TL->GevTL::INT->FD::MAC::...::DEV->CV-A10GE".
/// \param [in] hFac             Handle to a valid factory object, obtained by \c J_Factory_Open()
/// \param [in] iNum             Zero based index in the camera list. It must be smaller than the number obtained by \c J_Factory_GetNumOfCameras()
/// \param [out] pBuffer         Pointer to a buffer in which the camera ID is stored. 
/// \param [in,out] pSize        The size of the buffer. It must be equal or larger than J_CAMERA_ID_SIZE.
///                               The function sets the actual size of data stored into the buffer.
/// \retval                      Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
/// \sa \c J_Camera_Open(), \c J_Factory_UpdateCameraList(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Factory_GetCameraIDByIndex(FACTORY_HANDLE hFac, int32_t iNum, int8_t* pBuffer, size_t* pSize);

//******************************************************************************************************************
///  \brief                        Get detailed information about a camera
///  \param [in] hFac              Handle to a valid factory object, obtained by \c J_Factory_Open() function.
///  \param [in] pCameraId         Unique Camera ID obtained by \c J_Factory_GetCameraIDByIndex() function. 
///  \param [in] InfoId            The information type that is requested
///  \param [out] pBuffer          Pointer to a buffer in which the information is stored.
///  \param [in,out] pSize         The size of the buffer. It must be equal or larger than J_CAMERA_INFO_SIZE.
///                                The function sets the actual size of data stored into the buffer.
///  \retval                       Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
/// \sa \c J_CAMERA_INFO, \c J_Factory_GetCameraIDByIndex()
///  \note The camera connection does not need to be established in order to get this information. The information is retrieved during \b Device \b Discovery.
/// \code
/// // C Sample demonstrating how to read the manufacturer information from the camera
/// 
///  int8_t  sManufacturer[J_CAMERA_INFO_SIZE];
///  size_t  size;
///
///  J_Factory_GetCameraInfo(hFactory, sCameraId, CAM_INFO_MANUFACTURER, sManufacturer, &size);
///
///  printf("Manufacturer = %s\n", sManufacturer);
/// \endcode
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Factory_GetCameraInfo(FACTORY_HANDLE hFac, int8_t* pCameraId, J_CAMERA_INFO InfoId, int8_t* pBuffer, size_t* pSize);

//******************************************************************************************************************
///  \brief                        Set Force IP configuration.
///  \param [in] hFac              Handle to a valid factory object, obtained by \c J_Factory_Open() function.
///  \param [in] ForceEnabled      =0: Force IP disabled, !=0: Force IP enabled
///  \note                         This function must be called before calling J_Factory_UpdateCameraList();
///  \retval                       Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Factory_EnableForceIp(FACTORY_HANDLE hFac, uint32_t ForceEnabled);


/// @}
/// \defgroup WRAP_CAM	Camera specific functions
/// @{
///

//******************************************************************************************************************
///  \brief                        Open a camera from a camera ID and return valid camera handle
///                                The ID can be camera ID returned from the \c J_Factory_GetCameraID() or a user defined name
///  \param [in] hFac              Handle to a valid factory object, obtained by \c J_Factory_Open() function.
///  \param [in] pCameraID         Camera ID obtained by \c J_Factory_GetCameraIDByIndex() function.
///  \param [out] hCam             Pointer to the variable in which the handle is stored.
///  \retval                       Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  When a cameras has been opened using \c J_Camera_Open() it needs to be closed using \c J_Camera_Close()
///  \sa \c J_Camera_Close(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Camera_Open(FACTORY_HANDLE hFac, int8_t* pCameraID, CAM_HANDLE* hCam);

//******************************************************************************************************************
///  \brief                        Open a camera  with multicasting from a camera ID and return valid camera handle
///                                The camera ID can be returned from the \c J_Factory_GetCameraID() or a user defined name
///  \param [in] hFac              Handle to a valid factory object, obtained by \c J_Factory_Open() function.
///  \param [in] pCameraID         Camera ID obtained by \c J_Factory_GetCameraIDByIndex() function.
///  \param [out] hCam             Pointer to the variable in which the handle is stored.
///  \param [in] iOpenFlags        Device access Flags
///  \param [in] iMcIP             IP address for the multicasting
///  \retval                       Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  When a cameras has been opened using \c J_Camera_Open() it needs to be closed using \c J_Camera_Close()
///  \sa \c J_Camera_Close(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Camera_OpenMc(FACTORY_HANDLE hFac, int8_t* pCameraID, CAM_HANDLE* hCam, J_DEVICE_ACCESS_FLAGS iOpenFlags, uint32_t iMcIP);

//******************************************************************************************************************
///  \brief                        Close the camera object
///  \param [in] hCam              Handle to a valid camera object, obtained by \c J_Camera_Open() function.
///  \retval                       Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///
///  \sa \c J_Camera_Open(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Camera_Close(CAM_HANDLE hCam);

//******************************************************************************************************************
///  \brief                       Read data from a register or memory in the camera
///  \param [in] hCam             Handle to a valid camera object, obtained by \c J_Camera_Open() function.
///  \param [in] iAddress         Address to a register or memory in the camera
///  \param [out] pData           Pointer to a buffer in which the read data is stored.
///  \param [in,out] iSize        Specify the size of the data, and then the function sets an actual size of read data.
///  \retval                      Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before read and write operations can be performed
///  \sa \c J_Camera_Open(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Camera_ReadData(CAM_HANDLE hCam, int64_t iAddress, void* pData, size_t* iSize);

//******************************************************************************************************************
///  \brief                       Write data to a register or memory in the camera
///  \param [in] hCam             Handle to a valid camera object, obtained by \c J_Camera_Open() function.
///  \param [in] iAddress         Address to a register or memory in the camera
///  \param [in] pData            Data to be written
///  \param [in,out] iSize        Specify the size of the data, and then the function sets an actual size of written data.
///  \retval                      Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before read and write operations can be performed
///  \sa \c J_Camera_Open(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Camera_WriteData(CAM_HANDLE hCam, int64_t iAddress, const void* pData, size_t* iSize);

//******************************************************************************************************************
///  \brief                     Retrieve information about the GenICam XML-file configuration loaded for the device
///  \param [in] hCam           Handle to a valid camera object, obtained by \c J_Camera_Open() function.
///  \param [in] cinfo			The type of information requested
///  \param [out] pBuffer       Pointer to a buffer in which the information is stored.
///  \param [in,out] pSize      Specify the size of the buffer. It must be equal or greater then J_CONFIG_INFO_SIZE.
///                             The function sets the actual size of data stored into the buffer.
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the information about the GenICam XML-file can be retrieved
///  \sa \c J_Camera_Open(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Camera_GetConfigInfo(CAM_HANDLE hCam, J_CONFIG_INFO cinfo, int8_t* pBuffer, uint32_t* pSize); 

//******************************************************************************************************************
///  \brief                     Get the number of nodes in the entire GenICam node map for the camera
///  \param [in] hCam           Handle to a valid camera object, obtained by \c J_Camera_Open() function.
///  \param [out] pNum          Pointer to a variable in which the number of nodes is stored.
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the number of nodes can be read
/// \par
/// \code
/// // C Sample enumerates all GenICam nodes in a camera and prints out node names
///
/// uint32_t        nNodes;
/// J_STATUS_TYPE   retval;
/// NODE_HANDLE     hNode;
/// int8_t          sNodeName[256];
/// uint32_t        size;
///  
/// // Get the number of nodes
/// retval = J_Camera_GetNumOfNodes(hCamera, &nNodes);
/// if (retval == J_ST_SUCCESS)
/// {
///   printf("%u nodes were found\n", nNodes);
///   // Run through the list of nodes and print out the names
///   for (uint32_t index = 0; index < nNodes; ++index)
///   {
///     // Get node handle
///     retval = J_Camera_GetNodeByIndex(hCamera, index, &hNode);
///     if (retval == J_ST_SUCCESS)
///     {
///       // Get node name
///       size = sizeof(sNodeName);
///       retval = J_Node_GetName(hNode, sNodeName, &size, 0);
///       if (retval == J_ST_SUCCESS)
///       {
///         // Print out the name
///         printf("%u NodeName = %s\n", index, sNodeName);
///       }
///     }
///   }
/// }
/// \endcode
///  \sa \c J_Camera_Open(), \c J_Node_GetName(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Camera_GetNumOfNodes(CAM_HANDLE hCam, uint32_t* pNum);

//******************************************************************************************************************
///  \brief                     Get the node by the node map index. Valid indices will be between 0 and the value returned using \c J_Camera_GetNumOfNodes()
///  \param [in] hCam           Handle to a valid camera object, obtained by \c J_Camera_Open() function.
///  \param [in] index          Zero based index of the node in the entire node map.
///                             It must be smaller than the number obtained by \c J_Camera_GetNumOfNodes() function.
///  \param [out] phNode        Pointer to a variable in which the handle of the node object is stored.
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the nodes can be opened
/// \par
/// \code
/// // C Sample enumerates all GenICam nodes in a camera and prints out node names
///
/// uint32_t        nNodes;
/// J_STATUS_TYPE   retval;
/// NODE_HANDLE     hNode;
/// int8_t          sNodeName[256];
/// uint32_t        size;
///  
/// // Get the number of nodes
/// retval = J_Camera_GetNumOfNodes(hCamera, &nNodes);
/// if (retval == J_ST_SUCCESS)
/// {
///   printf("%u nodes were found\n", nNodes);
///   // Run through the list of nodes and print out the names
///   for (uint32_t index = 0; index < nNodes; ++index)
///   {
///     // Get node handle
///     retval = J_Camera_GetNodeByIndex(hCamera, index, &hNode);
///     if (retval == J_ST_SUCCESS)
///     {
///       // Get node name
///       size = sizeof(sNodeName);
///       retval = J_Node_GetName(hNode, sNodeName, &size, 0);
///       if (retval == J_ST_SUCCESS)
///       {
///         // Print out the name
///         printf("%u NodeName = %s\n", index, sNodeName);
///       }
///     }
///   }
/// }
/// \endcode
///  \sa \c J_Camera_GetNumOfNodes(), \c J_Camera_Open(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Camera_GetNodeByIndex(CAM_HANDLE hCam, uint32_t index, NODE_HANDLE* phNode);

//******************************************************************************************************************
///  \brief                     Get node by the GenICam node name
///  \param [in] hCam           Handle to a valid camera object, obtained by \c J_Camera_Open() function.
///  \param [in] sNodeName      Name of the node requested
///  \param [out] phNode        Pointer to a variable in which the handle of the node object is stored.
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the nodes can be opened
///  \sa \c J_Camera_Open(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Camera_GetNodeByName(CAM_HANDLE hCam, int8_t* sNodeName, NODE_HANDLE * phNode);

//******************************************************************************************************************
///  \brief                       Get the number of sub feature nodes in the given node map tree specified by a parent node name
///  \param [in] hCam             Handle to a valid camera object, obtained by \c J_Camera_Open() function.
///  \param [in] sParentNodeName  Name of the parent node name in the node map tree.
///  \param [out] pNum            Pointer to a variable in which the number of nodes is stored.
///  \retval                      Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the number of sub feature nodes can be read
/// \par
/// \code
/// // C Sample enumerates all GenICam sub feature nodes in the root tree of a camera and prints out the node names
///
///   J_STATUS_TYPE   retval;
///   uint32_t        nFeatureNodes;
///   NODE_HANDLE     hNode;
///   int8_t          sNodeName[256];
///   uint32_t        size;
/// 
///   retval = J_Camera_GetNumOfSubFeatures(hCamera, J_ROOT_NODE, &nFeatureNodes);
///   if (retval == J_ST_SUCCESS)
///   {
///     printf("%u subfeature nodes were found in the Root node\n", nFeatureNodes);
///     // Run through the list of feature nodes and print out the names
///     for (uint32_t index = 0; index < nFeatureNodes; ++index)
///     {
///       // Get subfeature node handle
///       retval = J_Camera_GetSubFeatureByIndex(hCamera, J_ROOT_NODE, index, &hNode);
///       if (retval == J_ST_SUCCESS)
///       {
///         // Get subfeature node name
///         size = sizeof(sNodeName);
///         retval = J_Node_GetName(hNode, sNodeName, &size, 0);
///         if (retval == J_ST_SUCCESS)
///         {
///           // Print out the name
///           printf("%u Feature Name = %s\n", index, sNodeName);
///         }
///       }
///     }
///   }
/// \endcode
///  \sa \c J_Camera_Open(), \c J_Node_GetName(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Camera_GetNumOfSubFeatures(CAM_HANDLE hCam, int8_t* sParentNodeName, uint32_t *pNum);

//******************************************************************************************************************
///  \brief                       Get a handle to the feature node object by the index in the given node map
///  \param [in] hCam             Handle to a valid camera object, obtained by \c J_Camera_Open() function.
///  \param [in] sParentNodeName  The name of the parent node
///  \param [in] index            Zero based index of the node requested
///                               It must be smaller than the number obtained by \c J_Camera_GetNumOfSubFeatures() function.
///  \param [out] phNode          Pointer to a variable in which the handle of the feature node object is stored.
///  \retval                      Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the sub feature nodes can be opened
/// \par
/// \code
/// // C Sample enumerates all GenICam sub feature nodes in the root tree of a camera and prints out the node names
///
///   J_STATUS_TYPE   retval;
///   uint32_t        nFeatureNodes;
///   NODE_HANDLE     hNode;
///   int8_t          sNodeName[256];
///   uint32_t        size;
/// 
///   retval = J_Camera_GetNumOfSubFeatures(hCamera, J_ROOT_NODE, &nFeatureNodes);
///   if (retval == J_ST_SUCCESS)
///   {
///     printf("%u subfeature nodes were found in the Root node\n", nFeatureNodes);
///     // Run through the list of feature nodes and print out the names
///     for (uint32_t index = 0; index < nFeatureNodes; ++index)
///     {
///       // Get subfeature node handle
///       retval = J_Camera_GetSubFeatureByIndex(hCamera, J_ROOT_NODE, index, &hNode);
///       if (retval == J_ST_SUCCESS)
///       {
///         // Get subfeature node name
///         size = sizeof(sNodeName);
///         retval = J_Node_GetName(hNode, sNodeName, &size, 0);
///         if (retval == J_ST_SUCCESS)
///         {
///           // Print out the name
///           printf("%u Feature Name = %s\n", index, sNodeName);
///         }
///       }
///     }
///   }
/// \endcode
///  \sa \c J_Camera_Open(), \c J_Node_GetName(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Camera_GetSubFeatureByIndex(CAM_HANDLE hCam, int8_t* sParentNodeName, uint32_t index, NODE_HANDLE * phNode);

//******************************************************************************************************************
///  \brief                       Get feature node by the name
///  \param [in] hCam             Handle to a valid camera object, obtained by \c J_Camera_Open() function.
///  \param [in] sFeatureNodeName Name of the feature node.
///  \param [out] phNode          Pointer to a variable in which the handle of the feature node object is stored.
///  \retval                      Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the feature nodes can be opened
///  \sa \c J_Camera_Open(), \c J_Node_GetName(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Camera_GetFeatureByName(CAM_HANDLE hCam, int8_t* sFeatureNodeName, NODE_HANDLE* phNode);

//******************************************************************************************************************
///  \brief                     Invalidates all GenICam nodes
///  \param [in] hCam           Handle to a valid camera object, obtained by \c J_Camera_Open() function.
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the feature nodes can be opened
///  \sa \c J_Camera_Open(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Camera_InvalidateNodes(CAM_HANDLE hCam);

//******************************************************************************************************************
///  \brief                     Get the number of available Data Streams from the Camera
///  \param [in] hCam           Handle to a valid camera object, obtained by \c J_Camera_Open() function.
///  \param [out] pNum          Pointer to a variable in which the number of data streams is stored.
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the number of data streams can be read. The number of data 
///  streams are used to check the available data streams so the stream channel index used in \c J_Camera_CreateDataStream()
///  is within leagal range.
///  \sa \c J_Camera_Open(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Camera_GetNumOfDataStreams(CAM_HANDLE hCam, uint32_t *pNum);

//******************************************************************************************************************
///  \brief                     Create and open data stream channel to the camera
///  \param [in] hCam           Handle to a valid camera object, obtained by \c J_Camera_Open() function.
///  \param [in] iChannel       Data stream channel number. Must be within 0 and the value returned by \c J_Camera_GetNumOfDataStreams()
///  \param [out] pDS           Pointer to a variable in which the data streams handle is stored.
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the data stream can be created.
///  \sa \c J_Camera_Open(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Camera_CreateDataStream(CAM_HANDLE hCam, uint32_t iChannel, STREAM_HANDLE *pDS);

//******************************************************************************************************************
///  \brief                     Create and open data stream channel to the camera
///  \param [in] hCam           Handle to a valid camera object, obtained by \c J_Camera_Open() function.
///  \param [in] iChannel       Data stream channel number. Must be within 0 and the value returned by \c J_Camera_GetNumOfDataStreams()
///  \param [out] pDS           Pointer to a variable in which the data streams handle is stored.
///	 \param [in] iMcIP          Multicast IP address. It will be ignored if it is 0
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the data stream can be created.
///  \sa \c J_Camera_Open(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Camera_CreateDataStreamMc(CAM_HANDLE hCam, uint32_t iChannel, STREAM_HANDLE *pDS, DWORD iMcIP);

/// @}
/// \defgroup WRAP_DATASTREAM	Data Stream specific functions
/// @{
///
//******************************************************************************************************************
///  \brief                     Announce buffer pointer for the acquisition engine
///  \param [in] hDS            Handle to a valid data stream, obtained by \c J_Camera_CreateDataStream() function.
///  \param [in] pBuffer        Pointer to a allocated memory buffer to hold image data 
///  \param [in] iSize          Size of the allocated memory buffer 
///  \param [in] pPrivate       Pointer to private user data to be attached to the image buffer 
///  \param [out] hBuf          Pointer to a variable in which the buffer handle is stored.
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The data stream needs to be created using \c J_Camera_CreateDataStream() before buffers can be announced.
/// \code
/// //==============================================================////
/// // Prepare frame buffers for the data stream
/// //==============================================================////
/// uint32_t CStreamThread::PrepareBuffers(int bufferCount, int bufferSize, void *pPrivateData)
/// {
///     J_STATUS_TYPE	iResult = J_ST_SUCCESS;
///     int			i;
///
///     m_iValidBuffers = 0;
///
///     for(i = 0 ; i < bufferCount ; i++)
///     {
///		    // Make the buffer for one frame. 
/// 		m_pAquBuffer[i] = new uint8_t[bufferSize];
///
/// 		// Announce the buffer pointer to the Acquisition engine.
/// 		if(J_ST_SUCCESS != J_DataStream_AnnounceBuffer(m_hDS, m_pAquBuffer[i] , bufferSize , pPrivateData, &(m_pAquBufferID[i])))
/// 		{
/// 			delete m_pAquBuffer[i];
/// 			break;
/// 		}
///
/// 		// Queueing it.
/// 		if(J_ST_SUCCESS != J_DataStream_QueueBuffer(m_hDS, m_pAquBufferID[i]))
/// 		{
/// 			delete m_pAquBuffer[i];
/// 			break;
/// 		}
///
/// 		m_iValidBuffers++;
/// 	}
///
/// 	return m_iValidBuffers;
/// }
/// \endcode
///  \sa \c J_Camera_CreateDataStream(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE  J_DataStream_AnnounceBuffer(STREAM_HANDLE hDS, void *pBuffer, size_t iSize, void *pPrivate, BUF_HANDLE *hBuf);


//******************************************************************************************************************
///  \brief                     Flush Queues
///  \param [in] hDS            Handle to a valid data stream, obtained by \c J_Camera_CreateDataStream() function.
///  \param [in] QueueType      Queue type to Flush 
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The data stream needs to be created using \c J_Camera_CreateDataStream() before queues can be flushed.
/// \code
/// //==============================================================////
/// // Unprepare buffers
/// //==============================================================////
/// BOOL CStreamThread::UnPrepareBuffers(void)
/// {
/// 	void		*pPrivate;
/// 	void		*pBuffer;
/// 	uint32_t	i;
///
/// 	// Flush Queues
/// 	J_DataStream_FlushQueue(m_hDS, ACQ_QUEUE_INPUT_TO_OUTPUT);
/// 	J_DataStream_FlushQueue(m_hDS, ACQ_QUEUE_OUTPUT_DISCARD);
///	  
/// 	for(i = 0 ; i < m_iValidBuffers ; i++)
/// 	{
/// 		// Remove the frame buffer from the Acquisition engine.
/// 		J_DataStream_RevokeBuffer(m_hDS, m_pAquBufferID[i], &pBuffer , &pPrivate);
///
/// 		delete m_pAquBuffer[i];
/// 		m_pAquBuffer[i] = NULL;
/// 		m_pAquBufferID[i] = 0;
/// 	}
///
/// 	m_iValidBuffers = 0;
///
/// 	return TRUE;
/// }
/// \endcode
///  \sa \c J_Camera_CreateDataStream(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE  J_DataStream_FlushQueue(STREAM_HANDLE hDS, J_ACQ_QUEUE_TYPE QueueType);


//******************************************************************************************************************
///  \brief                 Start the image acquisition on the stream channel
///  \param [in] hDS        Handle to a valid data stream, obtained by \c J_Camera_CreateDataStream() function.
///  \param [in] iFlags     Aquisition start control flag.
///  \param [in] iNumImages Number of images to acquire. The value 0xFFFFFFFFFFFFFFFF indicates unlimited.
///  \retval                Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The data stream needs to be created using \c J_Camera_CreateDataStream() and buffers needs to be announced using 
///  \c J_DataStream_AnnounceBuffer() before acquisition can be started.
///  \sa \c J_Camera_CreateDataStream(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE  J_DataStream_StartAcquisition(STREAM_HANDLE hDS, J_ACQ_START_FLAGS iFlags, uint64_t iNumImages );

//******************************************************************************************************************
///  \brief                 Stop the image acquisition on the stream channel
///  \param [in] hDS        Handle to a valid data stream, obtained by \c J_Camera_CreateDataStream() function.
///  \param [in] iFlags     Aquisition stop control flag.
///  \retval                Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \sa \c J_Camera_CreateDataStream(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE  J_DataStream_StopAcquisition(STREAM_HANDLE hDS, J_ACQ_STOP_FLAGS iFlags);

//******************************************************************************************************************
///  \brief                      Get detailed information about a data stream
///  \param [in] hDS             Handle to a valid data stream object, obtained by \c J_Camera_CreateDataStream() function.
///  \param [in] iCmd            The information type that is requested
///  \param [out] pBuffer        Pointer to a buffer in which the information is stored.
///  \param [in,out] pSize       The size of the buffer. It must be equal or larger than J_STREAM_INFO_SIZE.
///                              The function sets the actual size of data stored into the buffer.
///  \retval                     Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \sa \c J_STREAM_INFO_CMD
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE  J_DataStream_GetStreamInfo(STREAM_HANDLE hDS, J_STREAM_INFO_CMD iCmd, void *pBuffer, size_t *pSize);

//******************************************************************************************************************
///  \brief                 Get the buffer handle by index
///  \param [in] hDS        Handle to a valid data stream, obtained by \c J_Camera_CreateDataStream() function.
///  \param [in] iIndex     Zero based index for the buffer.
///  \param [out] hBuf      Pointer to a variable in which the buffer handle is stored.
///  \retval                Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \sa \c J_Camera_CreateDataStream(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE  J_DataStream_GetBufferID(STREAM_HANDLE hDS, uint32_t iIndex, BUF_HANDLE *hBuf);

//******************************************************************************************************************
///  \brief                 Close the data stream
///  \param [in] hDS        Handle to a valid data stream, obtained by \c J_Camera_CreateDataStream() function.
///  \retval                Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \sa \c J_Camera_CreateDataStream(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE  J_DataStream_Close(STREAM_HANDLE hDS);

//******************************************************************************************************************
///  \brief                 Remove the frame buffer from the Acquisition engine
///  \param [in] hDS        Handle to a valid data stream, obtained by \c J_Camera_CreateDataStream() function.
///  \param [in] hBuf       Handle to the buffer to be revoked 
///  \param [in] pBuffer    Pointer to the buffer to remove from the acquisition engine  
///  \param [in] pPrivate   Pointer to the private data to remove from the acquisition engine 
///  \retval                Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
/// \code
/// //==============================================================////
/// // Unprepare buffers
/// //==============================================================////
/// BOOL CStreamThread::UnPrepareBuffers(void)
/// {
/// 	void		*pPrivate;
/// 	void		*pBuffer;
/// 	uint32_t	i;
///
/// 	// Flush Queues
/// 	J_DataStream_FlushQueue(m_hDS, ACQ_QUEUE_INPUT_TO_OUTPUT);
/// 	J_DataStream_FlushQueue(m_hDS, ACQ_QUEUE_OUTPUT_DISCARD);
///	  
/// 	for(i = 0 ; i < m_iValidBuffers ; i++)
/// 	{
/// 		// Remove the frame buffer from the Acquisition engine.
/// 		J_DataStream_RevokeBuffer(m_hDS, m_pAquBufferID[i], &pBuffer , &pPrivate);
///
/// 		delete m_pAquBuffer[i];
/// 		m_pAquBuffer[i] = NULL;
/// 		m_pAquBufferID[i] = 0;
/// 	}
///
/// 	m_iValidBuffers = 0;
///
/// 	return TRUE;
/// }
/// \endcode
///  \sa \c J_Camera_CreateDataStream(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE  J_DataStream_RevokeBuffer(STREAM_HANDLE hDS, BUF_HANDLE hBuf, void **pBuffer, void **pPrivate);

//******************************************************************************************************************
///  \brief                    Queue the buffer in the acquisition engine
///  \param [in] hDS           Handle to a valid data stream, obtained by \c J_Camera_CreateDataStream() function.
///  \param [in] hBuf          Handle of the buffer to be queued, obtained by \c J_DataStream_AnnounceBuffer().
///  \retval                   Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The data stream needs to be created using \c J_Camera_CreateDataStream() before buffers can be announced.
/// \code
/// //==============================================================////
/// // Prepare frame buffers for the data stream
/// //==============================================================////
/// uint32_t CStreamThread::PrepareBuffers(int bufferCount, int bufferSize, void *pPrivateData)
/// {
///     J_STATUS_TYPE	iResult = J_ST_SUCCESS;
///     int			i;
///
///     m_iValidBuffers = 0;
///
///     for(i = 0 ; i < bufferCount ; i++)
///     {
///		    // Make the buffer for one frame. 
/// 		m_pAquBuffer[i] = new uint8_t[bufferSize];
///
/// 		// Announce the buffer pointer to the Acquisition engine.
/// 		if(J_ST_SUCCESS != J_DataStream_AnnounceBuffer(m_hDS, m_pAquBuffer[i] , bufferSize , pPrivateData, &(m_pAquBufferID[i])), 0)
/// 		{
/// 			delete m_pAquBuffer[i];
/// 			break;
/// 		}
///
/// 		// Queueing it.
/// 		if(J_ST_SUCCESS != J_DataStream_QueueBuffer(m_hDS, m_pAquBufferID[i]))
/// 		{
/// 			delete m_pAquBuffer[i];
/// 			break;
/// 		}
///
/// 		m_iValidBuffers++;
/// 	}
///
/// 	return m_iValidBuffers;
/// }
/// \endcode
///  \sa \c J_Camera_CreateDataStream(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE  J_DataStream_QueueBuffer(STREAM_HANDLE hDS, BUF_HANDLE hBuf);

//******************************************************************************************************************
///  \brief                      Get detailed information about a buffer
///  \param [in] hDS             Handle to a valid data stream object, obtained by \c J_Camera_CreateDataStream() function.
///  \param [in] hBuf            Handle of the buffer to be queued, obtained by \c J_DataStream_AnnounceBuffer().
///  \param [in] iCmd            The information type that is requested
///  \param [out] pBuffer        Pointer to a buffer in which the information is stored.
///  \param [in,out] pSize       The size of the buffer. It must be equal or larger than J_STREAM_INFO_SIZE.
///                              The function sets the actual size of data stored into the buffer.
///  \retval                     Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \sa \c J_STREAM_INFO_CMD
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE  J_DataStream_GetBufferInfo(STREAM_HANDLE hDS, BUF_HANDLE hBuf, J_BUFFER_INFO_CMD iCmd, void *pBuffer, uint32_t *pSize);


///  @}
///  \addtogroup WRAP_EventInterface Event Interface functions
///  @{
/// \brief The J_EVENT_INFO_ID is used to obtain information about the number of entries in the queue
typedef enum _J_EVENT_INFO_ID
  {
    EVENT_INFO_NUM_ENTRYS_IN_QUEUE
  } J_EVENT_INFO_ID;

/// \brief The J_EVENT_TYPE is used to specify which type of event to register with the Transport Layer
typedef enum _J_DEVICE_EVENT_TYPE
  {
    EVENT_NEW_BUFFER,           ///< New buffer ready
    EVENT_GEV_EVENT_CMD,        ///< Event command received
    EVENT_GEV_EVENTDATA_CMD,    ///< Event data command received
    EVENT_ERROR,                ///< Error                            
    EVENT_MAX_ID
  } J_EVENT_TYPE;

/// \brief Struct containing the information provided by a GigE Event to pass it on to the GenAPI
typedef struct _J_EVENT_DATA_GEV_EVENT_CMD
{
  uint16_t              m_EventID;          	   ///< \c EVENT identifying reason.
  uint16_t              m_StreamChannelIndex;    ///< Index of stream channel (0xFFFF for no channel).
  uint16_t              m_BlockID;          	   ///< Data block ID (0 for no block).
  uint64_t              m_Timestamp;        	   ///< Event timestamp (0 if not supported).
} J_EVENT_DATA_GEV_EVENT_CMD;


/// \brief Struct containing the information for a New Buffer Event
typedef struct _J_EVENT_DATA_NEW_IMAGE
{
  BUF_HANDLE         m_iBufferID;          	///< Buffer handle of the delivered buffer
} J_EVENT_DATA_NEW_IMAGE;

//******************************************************************************************************************
///  \brief                     Register an event with the Transport Layer interface
///  \param [in] hDS            Handle to a valid data stream object, obtained by \c J_Camera_CreateDataStream() function.
///  \param [in] iEventType     Event type to register
///  \param [in] hEvent         Handle to the event object to be used for signalling the event. This has to be created using kernel32
///                             function \c CreateEvent()
///  \param [out] pEventHandle  Pointer to a variable in which the internal event handle is stored. This handle is used in
///                             \c J_Event_GetData() and \c J_Event_GetInfo()
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
/// \code
/// // This small piece of code demonstrate the basics used in the image aquisition thread for a camera. It is not
/// // intended to be used directly since it has no error handling built in but merely to show the API functions needed.
///
/// // Create event object to be used by the acquisition engine
/// HANDLE	hEventNewImage = CreateEvent(NULL, true, false, NULL);
///
/// // Define internal event handle to be used when the event is fired
/// EVENT_HANDLE	hEvent;
/// 
/// // Register the hEventNewImage event with the acquisition engine to be signaled whenever a new image buffer is ready.
/// J_Event_Register(m_hDS, EVENT_NEW_BUFFER, hEventNewImage, &hEvent);
///
/// // Start the image acquisition
/// iResult = DSStartAcquisition(m_hDS, ACQ_START_NEXT_IMAGE, 0 );
///
/// // Main loop for image acquisition
/// while(m_bEnableThread)
///	{
/// 	// Wait for new Buffer event to be signaled
///		iWaitResult = WaitForSingleObject(hEventNewImage, 1000);
///
///     // Did we recieve a new buffer?
///		if(WAIT_OBJECT_0 == iWaitResult)
///		{
///         // Get the buffer associated with this event
///			iSize = sizeof(void *);
///			J_Event_GetData(hEvent, &iBufferhandle,  &iSize);
///
///			// Get the pointer to the frame buffer. 
///			iSize = sizeof (void *);
///			iResult = J_DataStream_GetBufferInfo(m_hDS, iBufferhandle, BUFFER_INFO_BASE	, &(tAqImageInfo.pImageBuffer), &iSize);
///			// Get the effective data size.
///			iSize = sizeof (uint32_t);
///			iResult = J_DataStream_GetBufferInfo(m_hDS, iBufferhandle, BUFFER_INFO_SIZE	, &(tAqImageInfo.iImageSize), &iSize);
///			// Get Pixel Format Type.
///			iSize = sizeof (uint32_t);
///			iResult = J_DataStream_GetBufferInfo(m_hDS, iBufferhandle, BUFFER_INFO_PIXELTYPE, &(tAqImageInfo.iPixelType), &iSize);
///			// Get Frame Width.
///			iSize = sizeof (uint32_t);
///			iResult = J_DataStream_GetBufferInfo(m_hDS, iBufferhandle, BUFFER_INFO_WIDTH	, &(tAqImageInfo.iSizeX), &iSize);
///			// Get Frame Height.
///			iSize = sizeof (uint32_t);
///			iResult = J_DataStream_GetBufferInfo(m_hDS, iBufferhandle, BUFFER_INFO_HEIGHT	, &(tAqImageInfo.iSizeY), &iSize);
///			// Get Timestamp 
///			iSize = sizeof (uint64_t);
///			iResult = J_DataStream_GetBufferInfo(m_hDS, iBufferhandle, BUFFER_INFO_TIMESTAMP, &(tAqImageInfo.iTimeStamp), &iSize);
///
///			if(m_bEnableThread)
///			{
///				// ... do something with the image data located in tAqImageInfo.pImageBuffer!!!
///             // This could for instance be to display the image.
///				J_Image_ShowImage(reinterpret_cast<VW_HANDLE>(g_hWin), &tAqImageInfo, m_iRGain, m_iGGain, m_iBGain);
///			}
///
///			// Queue This Buffer Again
///			iResult = J_DataStream_QueueBuffer(m_hDS, iBufferID);
///		}
///	}
/// \endcode
///  \sa \c J_Camera_CreateDataStream(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE  J_DataStream_RegisterEvent(STREAM_HANDLE hDS, J_EVENT_TYPE iEventType, HANDLE hEvent, EVT_HANDLE *pEventHandle);

//******************************************************************************************************************
///  \brief                     Un-register an event with the Transport Layer interface
///  \param [in] hDS            Handle to a valid data stream object, obtained by \c J_Camera_CreateDataStream() function.
///  \param [in] iEventType     Event type to un-register
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \sa \c J_Camera_CreateDataStream(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE  J_DataStream_UnRegisterEvent(STREAM_HANDLE hDS, J_EVENT_TYPE iEventType);

//******************************************************************************************************************
///  \brief                     Get parameters about data stream.
///  \param [in] hDS            Handle to a valid data stream object, obtained by \c J_Camera_CreateDataStream() function.
///  \param [in] iCmd           The parameter type which is requested
///  \param [out] pBuffer       Pointer to a buffer in which the information is stored.
///  \param [in,out] pSize      The size of the buffer.
///                             The function sets the actual size of data stored into the buffer.
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \sa \c J_Camera_CreateDataStream(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_DataStream_GetParam(STREAM_HANDLE hDS, J_STREAM_PARAM_CMD iCmd, void *pBuffer, size_t *pSize);
    
//******************************************************************************************************************
///  \brief                     Set parameters about data stream.
///  \param [in] hDS            Handle to a valid data stream object, obtained by \c J_Camera_CreateDataStream() function.
///  \param [in] iCmd           The parameter type which is requested
///  \param [out] pBuffer       Pointer to a buffer in which the information is stored.
///  \param [in,out] pSize      The size of the buffer.
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \sa \c J_Camera_CreateDataStream(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_DataStream_SetParam(STREAM_HANDLE hDS, J_STREAM_PARAM_CMD iCmd, void *pBuffer, size_t *pSize);

//******************************************************************************************************************
///  \brief                     Retrieve the event data associated with the event.
///  \param [in] pEventHandle   Internal event handle obtained by the \c J_Event_Register() function.
///  \param [out] pBuffer       Pointer to a buffer in which the information is stored.
///  \param [in,out] pSize      The size of the buffer.
///                             The function sets the actual size of data stored into the buffer.
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The data type returned by this function will depend on the event type. If the event is of type \c EVENT_NEW_BUFFER then this
///  function will return the image buffer handle as described by the \c J_EVENT_DATA_NEW_IMAGE structure.
///  If the event is of type \c EVENT_GEV_EVENT_CMD then this function will return information about the GEV command as 
///  described by the \c J_EVENT_DATA_GEV_EVENT_CMD structure.    
///  \sa \c J_Event_Register(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE  J_Event_GetData(EVT_HANDLE pEventHandle, void *pBuffer, size_t *pSize);

//******************************************************************************************************************
///  \brief                     Retrieve event information.
///  \param [in] pEventHandle   Internal event handle obtained by the \c J_Event_Register() function.
///  \param [in] iID            Event information type to retrieve
///  \param [out] pBuffer       Pointer to a buffer in which the information is stored.
///  \param [in,out] pSize      The size of the buffer.
///                             The function sets the actual size of data stored into the buffer.
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \sa \c J_Event_Register(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE  J_Event_GetInfo(EVT_HANDLE pEventHandle, J_EVENT_INFO_ID iID, void *pBuffer, size_t *pSize);

//******************************************************************************************************************
///  \brief                     Flush all Event currently in the queue
///  \param [in] pEventHandle   Internal event handle obtained by the \c J_Event_Register() function.
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \sa \c J_Event_Register(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE  J_Event_Flush(EVT_HANDLE pEventHandle);

//******************************************************************************************************************
///  \brief                     Close Event
///  \param [in] pEventHandle   Internal event handle obtained by the \c J_Event_Register() function.
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \sa \c J_Event_Register(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE  J_Event_Close(EVT_HANDLE pEventHandle);

/// @}
/// \defgroup WRAP_NODE	GenICam node specific functions
/// @{
///

//******************************************************************************************************************
///  \brief                 Get the access mode for a GenICam node
///  \param [in] hNode      Handle to a valid node object, obtained by \c J_Camera_GetNode****, \c J_Camera_GetFeature**** or so on.
///  \param [out] pValue    Pointer to the variable in which the data is stored.
///  \retval                Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the access mode can be read
///  \sa \c J_Camera_Open(), \c EConfAccessMode, \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Node_GetAccessMode(NODE_HANDLE hNode, J_NODE_ACCESSMODE *pValue);

//******************************************************************************************************************
///  \brief                     Get the GenICam name for a node
///  \param [in] hNode          Handle to a valid node object, obtained by \c J_Camera_GetNode****, \c J_Camera_GetFeature**** or so on.
///  \param [out] pBuffer       Pointer to the buffer in which the data is stored.
///  \param [in,out] pSize      Specify the size of the buffer, and then the function sets an actual size of stored data.
///  \param [in] FullQualified
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the GenICam name can be read
///  \sa \c J_Camera_Open(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Node_GetName(NODE_HANDLE hNode, int8_t* pBuffer, uint32_t* pSize, uint32_t FullQualified );

//******************************************************************************************************************
///  \brief                 Get the namespace for a GenICam node
///  \param [in] hNode      Handle to a valid node object, obtained by \c J_Camera_GetNode****, \c J_Camera_GetFeature**** or so on.
///  \param [out] pValue    Pointer to the variable in which the data is stored.
///  \retval                Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the namespace can be read
///  \sa \c J_Camera_Open(), \c J_NODE_NAMESPACE, \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Node_GetNameSpace(NODE_HANDLE hNode, J_NODE_NAMESPACE *pValue);

//******************************************************************************************************************
///  \brief                     Get the recommended visibility for a GenICam node
///  \param [in] hNode          Handle to a valid node object, obtained by \c J_Camera_GetNode****, \c J_Camera_GetFeature**** or so on.
///  \param [out] pVisibility   Pointer to the variable in which the data is stored.
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the recommended visibility can be read
///  \sa \c J_Camera_Open(), \c J_NODE_VISIBILITY, \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Node_GetVisibility(NODE_HANDLE hNode, J_NODE_VISIBILITY* pVisibility);

//******************************************************************************************************************
///  \brief                      Invalidate the node
///  \param [in] hNode           Handle to a valid node object, obtained by \c J_Camera_GetNode****, \c J_Camera_GetFeature**** or so on.
///  \retval                     Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the node can be invalidated
///  \sa \c J_Camera_Open(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Node_Invalidate(NODE_HANDLE hNode);

//******************************************************************************************************************
///  \brief                     Get if the GenICam node is cachable. If the node is not cachable it will need to be read periodically.
///  \param [in] hNode          Handle to a valid node object, obtained by \c J_Camera_GetNode****, \c J_Camera_GetFeature**** or so on.
///  \param [out] pVal          Pointer to the variable in which the data is stored.
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the cachability can be read
///  \sa \c J_Camera_Open(), \c J_Node_GetPollingTime(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Node_GetIsCachable(NODE_HANDLE hNode, uint32_t* pVal);

//******************************************************************************************************************
///  \brief                     Get the caching mode for a GenICam node
///  \param [in] hNode          Handle to a valid node object, obtained by \c J_Camera_GetNode****, \c J_Camera_GetFeature**** or so on.
///  \param [out] pValue        Pointer to the variable in which the data is stored.
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the caching mode can be read
///  \sa \c J_Camera_Open(), \c J_NODE_CACHINGMODE, \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Node_GetCachingMode(NODE_HANDLE hNode, J_NODE_CACHINGMODE *pValue);

//******************************************************************************************************************
///  \brief                     Get the recommended polling time for the GenICam node. This is only necessary if the node is not cachable.
///  \param [in] hNode          Handle to a valid node object, obtained by \c J_Camera_GetNode****, \c J_Camera_GetFeature**** or so on.
///  \param [out] pValue        Pointer to the variable in which the data is stored.
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the cachability can be read
///  \sa \c J_Camera_Open(), \c J_Node_GetIsCachable(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Node_GetPollingTime(NODE_HANDLE hNode, int64_t *pValue);

//******************************************************************************************************************
///  \brief                     Get the tooltip for a node
///  \param [in] hNode          Handle to a valid node object, obtained by \c J_Camera_GetNode****, \c J_Camera_GetFeature**** or so on.
///  \param [out] pBuffer       Pointer to the buffer in which the data is stored.
///  \param [in,out] pSize      Specify the size of the buffer, and then the function sets an actual size of stored data.
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the tooltip can be read
///  \sa \c J_Camera_Open(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Node_GetToolTip(NODE_HANDLE hNode, int8_t* pBuffer, uint32_t* pSize);

//******************************************************************************************************************
///  \brief                     Get the description for a node
///  \param [in] hNode          Handle to a valid node object, obtained by \c J_Camera_GetNode****, \c J_Camera_GetFeature**** or so on.
///  \param [out] pBuffer       Pointer to the buffer in which the data is stored.
///  \param [in,out] pSize      Specify the size of the buffer, and then the function sets an actual size of stored data.
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the description can be read
///  \sa \c J_Camera_Open(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Node_GetDescription(NODE_HANDLE hNode, int8_t* pBuffer, uint32_t* pSize);

//******************************************************************************************************************
///  \brief                     Get the display name for a node
///  \param [in] hNode          Handle to a valid node object, obtained by \c J_Camera_GetNode****, \c J_Camera_GetFeature**** or so on.
///  \param [out] pBuffer       Pointer to the buffer in which the data is stored.
///  \param [in,out] pSize      Specify the size of the buffer, and then the function sets an actual size of stored data.
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the display name can be read
///  \sa \c J_Camera_Open(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Node_GetDisplayName(NODE_HANDLE hNode, int8_t* pBuffer, uint32_t* pSize);

//******************************************************************************************************************
///  \brief                     Get the Event ID string for a node
///  \param [in] hNode          Handle to a valid node object, obtained by \c J_Camera_GetNode****, \c J_Camera_GetFeature**** or so on.
///  \param [out] pBuffer       Pointer to the buffer in which the data is stored.
///  \param [in,out] pSize      Specify the size of the buffer, and then the function sets an actual size of stored data.
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the Event ID can be read
///  \sa \c J_Camera_Open(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Node_GetEventID(NODE_HANDLE hNode, int8_t* pBuffer, uint32_t* pSize);

//******************************************************************************************************************
///  \brief                     Get if the GenICam node is streamable
///  \param [in] hNode          Handle to a valid node object, obtained by \c J_Camera_GetNode****, \c J_Camera_GetFeature**** or so on.
///  \param [out] pVal          Pointer to the variable in which the data is stored. Non zero if the node is streamable.
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the streamability can be read
///  \sa \c J_Camera_Open(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Node_GetIsStreamable(NODE_HANDLE hNode, uint32_t* pVal);

//******************************************************************************************************************
///  \brief                     Get the number of properties on the node
///  \param [in] hNode          Handle to a valid node object, obtained by \c J_Camera_GetNode****, \c J_Camera_GetFeature**** or so on.
///  \param [out] pNum          Pointer to the variable in which the number of properties is stored.
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the number of properties can be read
///  \sa \c J_Camera_Open(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Node_GetNumOfProperties(NODE_HANDLE hNode, uint32_t* pNum);

//******************************************************************************************************************
///  \brief                     Get the name of the property by index
///  \param [in] hNode          Handle to a valid node object, obtained by \c J_Camera_GetNode****, \c J_Camera_GetFeature**** or so on.
///  \param [in] index          Zero based index of the property. It must be smaller than the number obtained by \c J_Node_GetNumOfProperties()
///  \param [out] pBuffer       Pointer to a buffer in which the data is stored.
///  \param [in,out] pSize      Specify the size of the buffer, and then the function sets an actual size of stored data.
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the property names can be read
///  \sa \c J_Camera_Open(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Node_GetPropertyNameByIndex(NODE_HANDLE hNode, uint32_t index, int8_t* pBuffer, uint32_t* pSize);

//******************************************************************************************************************
///  \brief                             Get the property value and attribute string by property name
///  \param [in] hNode                  Handle to a valid node object, obtained by \c J_Camera_GetNode****, \c J_Camera_GetFeature**** or so on.
///  \param [in] PropertyName           Name of the property retrieved using \c J_Node_GetPropertyNameByIndex()
///  \param [out] ValueStr              Pointer to a buffer in which value is stored.
///  \param [in,out] pSizeValueStr      Specify the size of the buffer for the value, and then the function sets an actual size of stored data.
///  \param [out] AttributeStr          Pointer to a buffer in which attribute is stored.
///  \param [in,out] pSizeAttributeStr  Specify the size of the buffer for the attribute, and then the function sets an actual size of stored data.
///  \retval                            Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note                              If a property has multiple values/attribute they come with Tabs as delimiters
///  The camera needs to be opened using \c J_Camera_Open() before the property values and attributes can be read
///  \sa \c J_Camera_Open(), \c J_Node_GetPropertyNameByIndex(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Node_GetPropertyByName(NODE_HANDLE hNode, const int8_t* PropertyName, 
                                                                   int8_t* ValueStr, uint32_t* pSizeValueStr,
                                                                   int8_t* AttributeStr, uint32_t* pSizeAttributeStr);

//******************************************************************************************************************
///  \brief                         Change the access mode for a mode
///  \param [in] hNode              Handle to a valid node object, obtained by \c J_Camera_GetNode****, \c J_Camera_GetFeature**** or so on.
///  \param [in] AccessMode         The imposed access mode
///  \retval                        Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the access mode can be changed
///  \sa \c J_Camera_Open(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Node_GetImposeAccessMode(NODE_HANDLE hNode, J_NODE_ACCESSMODE AccessMode);

//******************************************************************************************************************
///  \brief                         Change the recommended visibility for a mode
///  \param [in] hNode              Handle to a valid node object, obtained by \c J_Camera_GetNode****, \c J_Camera_GetFeature**** or so on.
///  \param [in] Visibility         The recommended visibility mode 
///  \retval                        Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the recommended visibility can be changed
///  \sa \c J_Camera_Open(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Node_GetImposeVisibility(NODE_HANDLE hNode, J_NODE_VISIBILITY Visibility);

//******************************************************************************************************************
///  \brief                     Get a handle to a node object which describes the same feature in a different way
///  \param [in] hNode          Handle to a valid node object, obtained by \c J_Camera_GetNode****, \c J_Camera_GetFeature**** or so on.
///  \param [out] phAliasNode   Pointer to the variable in which the handle to alias node object is stored.
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the alias node can be read
///  \sa \c J_Camera_Open(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Node_GetAlias(NODE_HANDLE hNode, NODE_HANDLE* phAliasNode);

//******************************************************************************************************************
///  \brief                        Register a callback function to a specific node. The callback function will be called when the node changes state or value
///  \param [in] hNode             Handle to a valid node object, obtained by \c J_Camera_GetNode****, \c J_Camera_GetFeature**** or so on.
///  \param [in] CallBackFunction  Pointer to user call back function.
///  \retval                       Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the callback fucntion can be registered
///  \sa \c J_Camera_Open(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Node_RegisterCallback(NODE_HANDLE hNode, J_NODE_CALLBACK_FUNCTION CallBackFunction);

//******************************************************************************************************************
///  \brief                     Get the GenICam node type of the node
///  \param [in] hNode          Handle to a valid node object, obtained by \c J_Camera_GetNode****, \c J_Camera_GetFeature**** or so on.
///  \param [out] pNodeType     Pointer to the variable in which the data is stored.
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the node type can be read
///  \sa \c J_Camera_Open(), \c J_NODE_TYPE, \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Node_GetType(NODE_HANDLE hNode, J_NODE_TYPE* pNodeType);

/// @}
/// \defgroup WRAP_NODE_INT	IInteger GenICam node specific functions
/// \ingroup WRAP_NODE
/// @{
///

//******************************************************************************************************************
///  \brief                     Get the minimum value as int64_t type of the node
///  \param [in] hNode          Handle to a valid node object, obtained by \c J_Camera_GetNode****, \c J_Camera_GetFeature**** or so on.
///  \param [out] pValue        Pointer to the variable in which the value is stored.
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the minimum value can be read
/// The node type has to be \c IInteger
///  \sa \c J_Camera_Open(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Node_GetMinInt64(NODE_HANDLE hNode, int64_t* pValue);

//******************************************************************************************************************
///  \brief                     Get the maximum value as int64_t type of the node
///  \param [in] hNode          Handle to a valid node object, obtained by \c J_Camera_GetNode****, \c J_Camera_GetFeature**** or so on.
///  \param [out] pValue        Pointer to the variable in which the value is stored.
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the maximum value can be read
/// The node type has to be \c IInteger
///  \sa \c J_Camera_Open(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Node_GetMaxInt64(NODE_HANDLE hNode, int64_t* pValue);

//******************************************************************************************************************
///  \brief                     Get the increment value of the node
///  \param [in] hNode          Handle to a valid node object, obtained by \c J_Camera_GetNode****, \c J_Camera_GetFeature**** or so on.
///  \param [out] pValue        Pointer to the variable in which the value is stored.
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the increment value can be read
/// The node type has to be \c IInteger
///  \sa \c J_Camera_Open(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Node_GetInc(NODE_HANDLE hNode,int64_t *pValue);

//******************************************************************************************************************
///  \brief                     Set the value as int64_t type to the node object
///  \param [in] hNode          Handle to a valid node object, obtained by \c J_Camera_GetNode****, \c J_Camera_GetFeature**** or so on.
///  \param [in] Verify         Verify the value set
///  \param [in] Value          Value to set
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the value can be set
/// The node type has to be \c IInteger
///  \sa \c J_Camera_Open(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Node_SetValueInt64(NODE_HANDLE hNode, BOOL Verify, int64_t Value);

//******************************************************************************************************************
///  \brief                     Get the value as int64_t type from the node object
///  \param [in] hNode          Handle to a valid node object, obtained by \c J_Camera_GetNode****, \c J_Camera_GetFeature**** or so on.
///  \param [in] Verify         Verify the value set.
///  \param [in] pValue         Pointer to the variable in which the value is stored.
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the value can be read
/// The node type has to be \c IInteger
///  \sa \c J_Camera_Open(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Node_GetValueInt64(NODE_HANDLE hNode, BOOL Verify, int64_t* pValue);

/// @}
/// \defgroup WRAP_NODE_FLOAT	IFloat GenICam node specific functions
/// \ingroup WRAP_NODE
/// @{
///

//******************************************************************************************************************
///  \brief                     Get the minimum value as double type of the node
///  \param [in] hNode          Handle to a valid node object, obtained by \c J_Camera_GetNode****, \c J_Camera_GetFeature**** or so on.
///  \param [out] pValue        Pointer to the variable in which the value is stored.
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the minimum value can be read
/// The node type has to be \c IFloat
///  \sa \c J_Camera_Open(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Node_GetMinDouble(NODE_HANDLE hNode, double* pValue);


//******************************************************************************************************************
///  \brief                     Get the maximum value as double type of the node
///  \param [in] hNode          Handle to a valid node object, obtained by \c J_Camera_GetNode****, \c J_Camera_GetFeature**** or so on.
///  \param [out] pValue        Pointer to the variable in which the value is stored.
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the maximum value can be read
/// The node type has to be \c IFloat
///  \sa \c J_Camera_Open(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Node_GetMaxDouble(NODE_HANDLE hNode, double* pValue);

//******************************************************************************************************************
///  \brief                     Set the value as double type to the node object
///  \param [in] hNode          Handle to a valid node object, obtained by \c J_Camera_GetNode****, \c J_Camera_GetFeature**** or so on.
///  \param [in] Verify         Verify the value set
///  \param [in] Value          Value to set
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the value can be set
/// The node type has to be \c IFloat
///  \sa \c J_Camera_Open(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Node_SetValueDouble(NODE_HANDLE hNode, BOOL Verify, double Value);

//******************************************************************************************************************
///  \brief                     Get the value as double type from the node object
///  \param [in] hNode          Handle to a valid node object, obtained by \c J_Camera_GetNode****, \c J_Camera_GetFeature**** or so on.
///  \param [in] Verify         Verify the value set.
///  \param [in] pValue         Pointer to the variable in which the value is stored.
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the value can be read
/// The node type has to be \c IFloat
///  \sa \c J_Camera_Open(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Node_GetValueDouble(NODE_HANDLE hNode, BOOL Verify, double* pValue);

/// @}
/// \defgroup WRAP_NODE_ENUM	IEnumeration GenICam node specific functions
/// \ingroup WRAP_NODE
/// @{
///

//******************************************************************************************************************
///  \brief                     Get the number of enumeration entries on a J_NODE_TYPE.IEnumeration node
///  \param [in] hEnumNode      Handle to a valid enumeration node object, obtained by \c J_Camera_GetNode****, \c J_Camera_GetFeature**** or so on.
///  \param [out] pNum          Pointer to the variable in which the value is stored.
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the number of enumeration entries can be read.
/// The node type has to be \c J_NODE_TYPE.IEnumeration
///  \sa \c J_Camera_Open(), \c J_STATUS_CODES, \c J_NODE_TYPE
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Node_GetNumOfEnumEntries(NODE_HANDLE hEnumNode, uint32_t *pNum);

//******************************************************************************************************************
///  \brief                       et the enumeration entry on a IEnumeration node. The node type returned will be \c J_NODE_TYPE.IEnumEntry
///  \param [in] hEnumNode        Handle to a valid enumeration node object, obtained by \c J_Camera_GetNode****, \c J_Camera_GetFeature**** or so on.
///  \param [in] index            Zero based index of the entry nodes. It must be smaller than the number obtained by \c J_Node_GetNumOfEnumEntries().
///  \param [out] hEnumEntryNode  Pointer to the variable in which the value is stored.
///  \retval                      Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the enumeration entry handle can be read.
/// The node type has to be \c J_NODE_TYPE.IEnumeration
///  \sa \c J_Camera_Open(), \c J_STATUS_CODES, \c J_NODE_TYPE
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Node_GetEnumEntryByIndex(NODE_HANDLE hEnumNode, uint32_t index, NODE_HANDLE* hEnumEntryNode);

//******************************************************************************************************************
///  \brief                       Get the integer value of a J_NODE_TYPE.IEnumEntry node
///  \param [in] hEnumNode        Handle to a valid enumeration entry node object, obtained by \c J_Node_GetEnumEntryByIndex()
///  \param [out] pValue          Pointer to the variable in which the value is stored.
///  \retval                      Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the enumeration entry value can be read.
/// The node type has to be \c J_NODE_TYPE.IEnumeration
///  \sa \c J_Camera_Open(), \c J_STATUS_CODES, \c J_NODE_TYPE
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Node_GetEnumEntryValue(NODE_HANDLE hEnumNode, int32_t* pValue);

/// @}
/// \defgroup WRAP_NODE_CMD	ICommand GenICam node specific functions
/// \ingroup WRAP_NODE
/// @{
///

//******************************************************************************************************************
///  \brief                     Execute the GenICam command
///  \param [in] hNode          Handle to a valid command node object, obtained by \c J_Camera_GetNode****, \c J_Camera_GetFeature**** or so on.
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the command can be executed.
/// The node type has to be \c J_NODE_TYPE.ICommand
///  \sa \c J_Camera_Open(), \c J_STATUS_CODES, \c J_NODE_TYPE
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Node_ExecuteCommand(NODE_HANDLE hNode);

//******************************************************************************************************************
///  \brief                     Read if the GenICam command has been executed
///  \param [in] hNode          Handle to a valid command node object, obtained by \c J_Camera_GetNode****, \c J_Camera_GetFeature**** or so on.
///  \param [out] pValue        Pointer to the variable in which the value is stored. Non zero if the command execution is done.
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the commandstatus can be read.
/// The node type has to be \c J_NODE_TYPE.ICommand
///  \sa \c J_Camera_Open(), \c J_STATUS_CODES, \c J_NODE_TYPE
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Node_GetCommandIsDone(NODE_HANDLE hNode, uint32_t* pValue);

//******************************************************************************************************************
///  \brief                     Get GenICam error info
///  \param [in] gc             Handle to a valid command node object, obtained by \c J_Camera_GetNode****, \c J_Camera_GetFeature**** or so on.
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Factory_GetGenICamErrorInfo(tGenICamErrorInfo* gc);

/// @}
/// \defgroup WRAP_NODE_STR	IString GenICam node specific functions
/// \ingroup WRAP_NODE
/// @{
///

//******************************************************************************************************************
///  \brief                     Set the value as string type to the node object
///  \param [in] hNode          Handle to a valid node object, obtained by \c J_Camera_GetNode****, \c J_Camera_GetFeature**** or so on.
///  \param [in] Verify         Verify the value set
///  \param [in] ValueStr       Value to set
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the value can be set
/// The node type can be any type. The value will automatically be converted internally in the factory
///  \sa \c J_Camera_Open(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Node_SetValueString(NODE_HANDLE hNode, BOOL Verify, int8_t* ValueStr);


//******************************************************************************************************************
///  \brief                     Get the value as string type from the node object
///  \param [in] hNode          Handle to a valid node object, obtained by \c J_Camera_GetNode****, \c J_Camera_GetFeature**** or so on.
///  \param [in] Verify         Verify the value set.
///  \param [in] ValueStr       Pointer to the variable in which the value is stored.
///  \param [in,out] pSize      Specify the size of the value, and then the function sets an actual size of stored value.
///  \retval                    Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
///  \note
///  The camera needs to be opened using \c J_Camera_Open() before the value can be read
/// The node type can be any type. The value will automatically be converted internally in the factory
///  \sa \c J_Camera_Open(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Node_GetValueString(NODE_HANDLE hNode, BOOL Verify, int8_t* ValueStr, uint32_t* pSize);

/// @}

/// @}
/// \defgroup WRAP_IMAGE	Image acquisition and manupulation functions
/// @{
///

/// @}
/// \defgroup WRAP_IMAGE_VIEW	View window specific functions
/// \ingroup WRAP_IMAGE
/// @{

//******************************************************************************************************************
/// \brief	Open View Window (Create)
///
/// A window of size MaxSize is created for the client area. When the images are displayed using \c J_ImageShowImage() the view window will automatically be resized. 
///
///	\param	pWindowName		[in]	Name to be displayed in the view window caption
///	\param	*pPoint			[in]	Point of the upper left corner of the view window (absolute coordinate).
///	\param	*pMaxSize		[in]	Maximum size of the view window
///	\param	*pWin			[out]	The handle of the created window (window object) is returned.
/// \retval               Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
/// \par
/// \code
/// // C Sample that opens a live view window and starts acquisition
///
///  VIEW_HANDLE     hView;
///  THRD_HANDLE     hThread;
///  J_STATUS_TYPE   retval;
///  NODE_HANDLE     hNode;
///  int64_t         int64Val;
///  SIZE            ViewSize = {100, 100};
///  POINT           TopLeft = {0, 0};
///
///  // Get Width from the camera
///  if (J_Camera_GetNodeByName(hCamera, "Width", &hNode) == J_ST_SUCCESS &&
///      J_Node_GetValueInt64(hNode, FALSE, &int64Val) == J_ST_SUCCESS)
///  {
///    ViewSize.cx = (LONG)int64Val;     // Set window size cx
///  }
///  // Get Height from the camera
///  if (J_Camera_GetNodeByName(hCamera, "Height", &hNode) == J_ST_SUCCESS &&
///      J_Node_GetValueInt64(hNode, FALSE, &int64Val) == J_ST_SUCCESS)
///  {
///    ViewSize.cy = (LONG)int64Val;     // Set window size cy
///  }
///
///  // Open view window
///  retval = J_Image_OpenViewWindow((LPCTSTR)"Live view!", &TopLeft, &ViewSize, &hView);
///  if (retval == J_ST_SUCCESS)
///  {
///    // Open stream
///    retval = J_Image_OpenStreamLight(hCamera, 0, &hThread); 
///    if (retval == J_ST_SUCCESS)
///    {
///      // Acquisition Start
///      if (J_Camera_GetNodeByName(hCamera, "AcquisitionStart", &hNode) == J_ST_SUCCESS)
///      {
///        J_Node_ExecuteCommand(hNode);
///      }
///    }
///  }
///
///  // Message loop
///  printf("Any key to exit!");
///  while (!_kbhit())
///  {
///	  MSG msg;
///	  if (::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
///		  ::TranslateMessage(&msg);
///		  ::DispatchMessage(&msg);
///	  }
///  }
///
///  // Acquisition Stop
///  if (J_Camera_GetNodeByName(hCamera, "AcquisitionStop", &hNode) == J_ST_SUCCESS)
///  {
///    J_Node_ExecuteCommand(hNode);
///  }
///
///  // Close stream
///  retval = J_Image_CloseStream(hThread);
///
///  // Close view window
///  retval = J_Image_CloseViewWindow(hView);
/// \endcode
/// \sa \c J_Image_OpenViewWindow(), \c J_Camera_GetNodeByName(), \c J_Node_ExecuteCommand(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Image_OpenViewWindow(LPCTSTR pWindowName, POINT *pPoint, SIZE *pMaxSize, VIEW_HANDLE *pWin);

//******************************************************************************************************************
/// \brief	Open View Window Ex (Create)
/// 
/// A window of size MaxSize is created for the client area. When the images are displayed using \c J_ImageShowImage() the view window will automatically be resized.
///	The window of size pFrameRect with window name pWindowName is created, and a view window of size pMaxSize is made for the client area.
/// At this time, the size of the window displayed in the first stage is specified for pFrameRect, and the assumed maximum image size is specified for pViewRect. 
/// Therefore, it usually becomes
/// pFrameRect<=*pMaxSize. 
///
///	\param	[in] iWindowType	Window type
///	\param	[in] pWindowName	Window name
///	\param	[in] *pFrameRect	Externals size of frame window(absolute coordinate).
///	\param	[in] *pMaxSize		Maximum size of image data.
/// \param  [in] hParent        Handle to parent window.
///	\param	[out] *pWin			The handle of the created window (window object) is returned.
/// \retval						Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Image_OpenViewWindowEx(J_IVW_WINDOW_TYPE iWindowType, LPCTSTR pWindowName, RECT *pFrameRect, SIZE *pMaxSize, HWND hParent, VIEW_HANDLE *pWin);

//******************************************************************************************************************
/// \brief	Close view window previously opened with \c J_Image_OpenViewWindow() or \c J_Image_OpenViewWindowEx() (Destroy it)
/// 
/// The window of handle hWin that \c J_Image_OpenViewWindow() or \c J_Image_OpenViewWindowEx() returned is destroyed.
///
///	\param	[in] hWin	Handle that \c J_Image_OpenViewWindow() or c J_Image_OpenViewWindowEx() returned.
/// \retval             Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Image_CloseViewWindow(VIEW_HANDLE hWin);

//******************************************************************************************************************
/// \brief	Display an image in a view window. 
/// 
/// This function automatically converts the RAW image specified with pAqImageInfo into DIB, and display it in the view window in the window of handle
/// hWin made with \c J_Image_OpenViewWindow() or c J_Image_OpenViewWindowEx() . 
///
///	\param [in] hWin			Handle to a view window previously opened using \c J_Image_OpenViewWindow() or \c J_Image_OpenViewWindowEx().
///	\param [out] pAqImageInfo	ImageInfo structure with information on RAW image.
/// \param [in] iRGain          Gain for the red color channel for Bayer conversion (0x01000(4096) = 1.00)
/// \param [in] iGGain          Gain for the green color channel for Bayer conversion (0x01000(4096) = 1.00)
/// \param [in] iBGain          Gain for the blue color channel for Bayer conversion (0x01000(4096) = 1.00)
/// \retval						Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Image_ShowImage(VIEW_HANDLE hWin, J_tIMAGE_INFO * pAqImageInfo, uint32_t iRGain, uint32_t iGGain, uint32_t iBGain);

//******************************************************************************************************************
/// \brief	Set image offset and zoom ratio.
///
/// Sets the offset and the zoom ratio of the view window image. 
///
///	\param	[in] hWin		Handle to a view window previously opened using \c J_Image_OpenViewWindow() or \c J_Image_OpenViewWindowEx()
///	\param	[in] *pOffset	POINT structure pointer with the horizontal, vertical offset (number of pixels).
/// \param	[in] ZoomRatio	Zoom ratio.
/// 						100:The original image is reduced to the window size.
///							1  :1/100 of the original images is expanded to the window size. 
/// \retval					Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Image_SetImageOffset(VIEW_HANDLE hWin, POINT *pOffset, int32_t ZoomRatio);

//******************************************************************************************************************
/// \brief	Move view window.
/// 
/// Moved the view window to the absolute position specified by \c Point
///
///	\param	[in] hWin		Handle to a view window previously opened using \c J_Image_OpenViewWindow() or \c J_Image_OpenViewWindowEx()
///	\param	[in] *pPoint	Upper left position of view window.
/// \retval					Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Image_MoveViewWindow(VIEW_HANDLE hWin, POINT *pPoint);

//******************************************************************************************************************
/// \brief	Get rectangular coordinates of the client area of View Window. 
/// 
/// The current position and size of the client area of the view window is read into a \c RECT structure.
///
///	\param	[in] hWin		Handle to a view window previously opened using \c J_Image_OpenViewWindow() or \c J_Image_OpenViewWindowEx()
///	\param	[out] *pRect	Present window coordinates.
/// \retval					Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Image_GetViewWindowRect(VIEW_HANDLE hWin, RECT *pRect);

//******************************************************************************************************************
/// \brief	Get position and size of the view window including the windows frame
/// 
/// The current position and size of the frame of the view window is read into a \c RECT structure.
///
///	\param	[in] hWin		Handle to a view window previously opened using \c J_Image_OpenViewWindow() or \c J_Image_OpenViewWindowEx()
///	\param	[out] *pRect	Present window coordinates.
/// \retval					Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Image_GetFrameWindowRect(VIEW_HANDLE hWin, RECT *pRect);

//******************************************************************************************************************
/// \brief	Set the title of view window
/// 
/// Modify the caption text of the view window 
///
///	\param	[in] hWin			Handle to a view window previously opened using \c J_Image_OpenViewWindow() or \c J_Image_OpenViewWindowEx()
///	\param	[in] *pWindowTitle	Character string that wants to be displayed in title bar.
/// \retval						Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Image_SetViewWindowTitle(VIEW_HANDLE hWin, LPCTSTR pWindowTitle);

/// @}
/// \defgroup WRAP_IMAGE_STREAM	Stream channel specific functions
/// \ingroup WRAP_IMAGE
/// @{

//******************************************************************************************************************
/// \brief	Open image stream channel
///
/// Start stream processing thread, and call pCallBack whenever it receives a new frame.
///
///	\param [in] hCam			Camera Handle
///	\param [in] iChannel		Stream Channel
/// \param [in] CBObject		Class object that belongs CallBack function.
/// \param [in] CBFunction		CallBack function.
///	\param [out] phThread   	Pointer of HANDLE that receives thread handle.
///	\param [in] iBufferSize	    Buffersize to be allocated per image
///	\param [in] iMcIP           Multicast IP address
/// \retval						Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
/// \par
/// \code
/// // C Sample that opens a live view window and starts acquisition
/// 
/// VIEW_HANDLE     g_hView;    // Global view handle
/// 
/// // Call back sample function
/// static void __stdcall StreamCBFunc(J_tIMAGE_INFO * pAqImageInfo)
/// {
///   // Shows image
///   J_Image_ShowImage(g_hView, pAqImageInfo);
/// }
/// 
/// // Live View sample function
/// BOOL LiveView(CAM_HANDLE hCamera)
/// {
///   THRD_HANDLE     hThread;
///   NODE_HANDLE     hNode;
///   int64_t         int64Val;
///   SIZE	          ViewSize = {100, 100};
///   POINT	          TopLeft = {0, 0};
/// 
///   // Get Width from the camera
///   if (J_Camera_GetNodeByName(hCamera, "Width", &hNode) != J_ST_SUCCESS)   return FALSE;
///   J_Node_GetValueInt64(hNode, FALSE, &int64Val);
///   ViewSize.cx = (LONG)int64Val;     // Set window size cx
/// 
///   // Get Height from the camera
///   if (J_Camera_GetNodeByName(hCamera, "Height", &hNode) != J_ST_SUCCESS)  return FALSE;
///   J_Node_GetValueInt64(hNode, FALSE, &int64Val);
///   ViewSize.cy = (LONG)int64Val;     // Set window size cy
/// 
///   // Open view window
///   if (J_Image_OpenViewWindow((LPCTSTR)"Live view!", &TopLeft, &ViewSize, &g_hView) != J_ST_SUCCESS)   return FALSE;
/// 
///   // Open stream
///   void *vfptr = reinterpret_cast<void*>(StreamCBFunc);
/// 	J_IMG_CALLBACK_FUNCTION *cbfptr =  reinterpret_cast<J_IMG_CALLBACK_FUNCTION*>(&vfptr);
///   if (J_Image_OpenStream(hCamera, 0, NULL, *cbfptr, &hThread, ViewSize.cx*ViewSize.cy*6) != J_ST_SUCCESS)   return FALSE;
/// 
///   // Acquisition Start
///   if (J_Camera_GetNodeByName(hCamera, "AcquisitionStart", &hNode) != J_ST_SUCCESS)    return FALSE;
///   J_Node_ExecuteCommand(hNode);
/// 
///   // Message loop
///   printf("Any key to exit!");
///   while (!_kbhit())
///   {
///     MSG msg;
///     if (::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
///       ::TranslateMessage(&msg);
///       ::DispatchMessage(&msg);
///     }
///   }
/// 
///   // Acquisition Stop
///   if (J_Camera_GetNodeByName(hCamera, "AcquisitionStop", &hNode) != J_ST_SUCCESS)     return FALSE;
///   J_Node_ExecuteCommand(hNode);
/// 
///   // Close stream
///   J_Image_CloseStream(hThread);
/// 
///   // Close view window
///   J_Image_CloseViewWindow(g_hView);
///   
///   return TRUE;
/// }
/// \endcode
///  \sa \c J_Image_OpenViewWindow(), \c J_Camera_GetNodeByName(), \c J_Node_ExecuteCommand(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Image_OpenStream(CAM_HANDLE hCam, uint32_t iChannel, J_IMG_CALLBACK_OBJECT CBObject, J_IMG_CALLBACK_FUNCTION CBFunction, THRD_HANDLE * phThread, uint32_t iBufferSize, DWORD iMcIP);

//******************************************************************************************************************
/// \brief	Open image stream channel and let the factory automatically create all callback delegates and automatically display live images
///
///	\param [in] hCam			Handle to the camera
///	\param [in] iChannel		Stream channel index. This index is zero-based
///	\param [out] phThread		The handle of the created stream channel is returned.
/// \retval						Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
/// \note
/// This is the easiest way to create a view window that will display live video from the cameras. After opening the stream channel the acquisition has to be started
/// using the standard GenICam command \c AcquisitionStart.
/// 
/// \par
/// \code
/// // C Sample that opens a live view window and starts acquisition
///
///  VIEW_HANDLE     hView;
///  THRD_HANDLE     hThread;
///  J_STATUS_TYPE   retval;
///  NODE_HANDLE     hNode;
///  int64_t         int64Val;
///  SIZE            ViewSize = {100, 100};
///  POINT           TopLeft = {0, 0};
///
///  // Get Width from the camera
///  if (J_Camera_GetNodeByName(hCamera, "Width", &hNode) == J_ST_SUCCESS &&
///      J_Node_GetValueInt64(hNode, FALSE, &int64Val) == J_ST_SUCCESS)
///  {
///    ViewSize.cx = (LONG)int64Val;     // Set window size cx
///  }
///  // Get Height from the camera
///  if (J_Camera_GetNodeByName(hCamera, "Height", &hNode) == J_ST_SUCCESS &&
///      J_Node_GetValueInt64(hNode, FALSE, &int64Val) == J_ST_SUCCESS)
///  {
///    ViewSize.cy = (LONG)int64Val;     // Set window size cy
///  }
///
///  // Open view window
///  retval = J_Image_OpenViewWindow((LPCTSTR)"Live view!", &TopLeft, &ViewSize, &hView);
///  if (retval == J_ST_SUCCESS)
///  {
///    // Open stream
///    retval = J_Image_OpenStreamLight(hCamera, 0, &hThread); 
///    if (retval == J_ST_SUCCESS)
///    {
///      // Acquisition Start
///      if (J_Camera_GetNodeByName(hCamera, "AcquisitionStart", &hNode) == J_ST_SUCCESS)
///      {
///        J_Node_ExecuteCommand(hNode);
///      }
///    }
///  }
///
///  // Message loop
///  printf("Any key to exit!");
///  while (!_kbhit())
///  {
///	   MSG msg;
///    if (::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
///      ::TranslateMessage(&msg);
///      ::DispatchMessage(&msg);
///    }
///  }
///
///  // Acquisition Stop
///  if (J_Camera_GetNodeByName(hCamera, "AcquisitionStop", &hNode) == J_ST_SUCCESS)
///  {
///    J_Node_ExecuteCommand(hNode);
///  }
///
///  // Close stream
///  retval = J_Image_CloseStream(hThread);
///
///  // Close view window
///  retval = J_Image_CloseViewWindow(hView);
/// \endcode
///  \sa \c J_Image_OpenViewWindow(), \c J_Camera_GetNodeByName(), \c J_Node_ExecuteCommand(), \c J_STATUS_CODES
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Image_OpenStreamLight(CAM_HANDLE hCam, uint32_t iChannel,  THRD_HANDLE* phThread);

//******************************************************************************************************************
/// \brief	Close a stream channel that has previously been opened using \c J_Image_OpenStream() or \c J_Image_OpenStreamLight().
/// 
/// Terminate the stream processing thread
///
///	\param	[in] hThread	Handle of the thread.
/// \retval					Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Image_CloseStream(THRD_HANDLE hThread);

/// @}
/// \defgroup WRAP_IMAGE_MANI	Image manipulation functions
/// \ingroup WRAP_IMAGE
/// @{


//******************************************************************************************************************
/// \brief	Convert image from RAW to DIB
/// 
/// Convert the RAW image specified with \c pAqImageInfo into 32 bit aRGB, and store it in the area specified with pBufferInfo.
/// The Alpha-channel will be set to 255 for all pixels.
///
///	\param [in] *pAqImageInfo   Pointer of J_tIMAGE_INFO structure with information on RAW data.
///	\param [in] *pBufferInfo    Pointer of J_tIMAGE_INFO structure in which information on converted image is written.
/// \param [in] iRGain          R Gain for Bayer (0x01000(4096) = 1.00)
/// \param [in] iGGain          G Gain for Bayer (0x01000(4096) = 1.00)
/// \param [in] iBGain          B Gain for Bayer (0x01000(4096) = 1.00)
/// \retval                     Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Image_FromRawToDIB(J_tIMAGE_INFO * pAqImageInfo, J_tIMAGE_INFO * pBufferInfo, uint32_t iRGain, uint32_t iGGain, uint32_t iBGain);

//******************************************************************************************************************
/// \brief	Convert from RAW to an image that can be saved or modified
/// 
/// Convert the RAW image specified with AqImageInfo by the following rule, and store it in the area specified with BufferInfo.\n\n
/// Information on the converted image is returned to BufferInfo:\n
///  8bpp Grayscale ->  8bpp Grayscale\n
/// 10bpp Grayscale -> 16bpp Grayscale\n
/// 24bpp RGB       -> 24bpp RGB\n
/// 30bpp RGB       -> 48bpp RGB\n
///  8bpp Bayer     -> 24bpp RGB\n
/// 10bpp Bayer     -> 48bpp RGB\n
///
///	\param [in] pAqImageInfo	Pointer of ImageInfo structure with information on RAW data.
///	\param [out] pBufferInfo	Pointer of ImageInfo structure in which the converted image is written.
/// \param [in] iRGain          Gain for the red color channel for Bayer conversion (0x01000(4096) = 1.00)
/// \param [in] iGGain          Gain for the green color channel for Bayer conversion (0x01000(4096) = 1.00)
/// \param [in] iBGain          Gain for the blue color channel for Bayer conversion (0x01000(4096) = 1.00)
/// \retval						Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
/// \par
/// \code
/// // C Sample that converts a raw image and saved the result to a file
/// BOOL SaveImageToFile(J_tIMAGE_INFO* ptRawImageInfo, LPCTSTR sFileName)
/// {
///   J_tIMAGE_INFO tCnvImageInfo;    // Image info structure
/// 
///   // Allocate the buffer to hold converted the image
///   if (J_Image_Malloc(ptRawImageInfo, &tCnvImageInfo) != J_ST_SUCCESS)             return FALSE;
///   // Convert the raw image to image format
///   if (J_Image_FromRawToImage(ptRawImageInfo, &tCnvImageInfo) != J_ST_SUCCESS)     return FALSE;
///   // Save the image to disk in TIFF format
///   if (J_Image_SaveFile(&tCnvImageInfo, sFileName) != J_ST_SUCCESS)                return FALSE;
///   // Free up the image buffer
///   if (J_Image_Free(&tCnvImageInfo) != J_ST_SUCCESS)                               return FALSE;
/// 
///   return TRUE;
/// }
/// \endcode
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Image_FromRawToImage(J_tIMAGE_INFO * pAqImageInfo, J_tIMAGE_INFO * pBufferInfo, uint32_t iRGain, uint32_t iGGain, uint32_t iBGain);

//******************************************************************************************************************
/// \brief	Allocate the buffer memory for the image
///
/// Allocate the buffer to store the converted image.
/// Information on the allocated buffer is returned in J_tIMAGE_INFO structure.
///
///	\param [in] pAqImageInfo	Pointer of J_tIMAGE_INFO structure with information on RAW data.
///	\param [out] pBufferInfo	Pointer of J_tIMAGE_INFO structure in which the information about the allocated buffer will be stored.
/// \retval						Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
/// \par
/// \code
/// // C Sample that converts a raw image and saved the result to a file
/// BOOL SaveImageToFile(J_tIMAGE_INFO* ptRawImageInfo, LPCTSTR sFileName)
/// {
///   J_tIMAGE_INFO tCnvImageInfo;    // Image info structure
/// 
///   // Allocate the buffer to hold converted the image
///   if (J_Image_Malloc(ptRawImageInfo, &tCnvImageInfo) != J_ST_SUCCESS)             return FALSE;
///   // Convert the raw image to image format
///   if (J_Image_FromRawToImage(ptRawImageInfo, &tCnvImageInfo) != J_ST_SUCCESS)     return FALSE;
///   // Save the image to disk in TIFF format
///   if (J_Image_SaveFile(&tCnvImageInfo, sFileName) != J_ST_SUCCESS)                return FALSE;
///   // Free up the image buffer
///   if (J_Image_Free(&tCnvImageInfo) != J_ST_SUCCESS)                               return FALSE;
/// 
///   return TRUE;
/// }
/// \endcode
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Image_Malloc(J_tIMAGE_INFO * pAqImageInfo, J_tIMAGE_INFO * pBufferInfo);

//******************************************************************************************************************
/// \brief	Allocate the buffer memory for the DIB (32 bit ARGB)
///
/// Allocate the buffer to store the converted image from J_Image_FromRawToDIB.
/// Information on the allocated buffer is returned in J_tIMAGE_INFO structure.
///
///	\param [in] pAqImageInfo	Pointer of J_tIMAGE_INFO structure with information on RAW data.
///	\param [out] pBufferInfo	Pointer of J_tIMAGE_INFO structure in which the information about the allocated buffer will be stored.
/// \retval						Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
/// \par
/// \code
/// // C Sample that converts a raw image and saved the result to a file
/// BOOL ConvertImageToDIB(J_tIMAGE_INFO* ptRawImageInfo, LPCTSTR sFileName)
/// {
///   J_tIMAGE_INFO tCnvImageInfo;    // Image info structure
/// 
///   // Allocate the buffer to hold converted the image
///   if (J_Image_MallocDIB(ptRawImageInfo, &tCnvImageInfo) != J_ST_SUCCESS)          return FALSE;
///   // Convert the raw image to image format
///   if (J_Image_FromRawToDIB(ptRawImageInfo, &tCnvImageInfo) != J_ST_SUCCESS)       return FALSE;
///   // Process the image
///   ... do something with the ARGB buffer
///   // Free up the image buffer
///   if (J_Image_Free(&tCnvImageInfo) != J_ST_SUCCESS)                               return FALSE;
/// 
///   return TRUE;
/// }
/// \endcode
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Image_MallocDIB(J_tIMAGE_INFO * pAqImageInfo, J_tIMAGE_INFO * pBufferInfo);

//******************************************************************************************************************
/// \brief	Free a previously allocated image buffer
/// 
/// Frees the memory allocated to the image buffer 
///
///	\param [in] pBufferInfo		Pointer of J_tIMAGE_INFO structure with a buffer previously allocated with \c J_Image_Malloc()
/// \retval						Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
/// \par
/// \code
/// // C Sample that converts a raw image and saved the result to a file
/// BOOL SaveImageToFile(J_tIMAGE_INFO* ptRawImageInfo, LPCTSTR sFileName)
/// {
///   J_tIMAGE_INFO tCnvImageInfo;    // Image info structure
/// 
///   // Allocate the buffer to hold converted the image
///   if (J_Image_Malloc(ptRawImageInfo, &tCnvImageInfo) != J_ST_SUCCESS)             return FALSE;
///   // Convert the raw image to image format
///   if (J_Image_FromRawToImage(ptRawImageInfo, &tCnvImageInfo) != J_ST_SUCCESS)     return FALSE;
///   // Save the image to disk in TIFF format
///   if (J_Image_SaveFile(&tCnvImageInfo, sFileName) != J_ST_SUCCESS)                return FALSE;
///   // Free up the image buffer
///   if (J_Image_Free(&tCnvImageInfo) != J_ST_SUCCESS)                               return FALSE;
/// 
///   return TRUE;
/// }
/// \endcode
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Image_Free(J_tIMAGE_INFO * pBufferInfo);

//******************************************************************************************************************
/// \brief	Save an image to disk as TIFF file
/// 
/// Saves an image to disk as a TIFF file. A raw image needs to be converted using \c J_Image_FromRawToImage() before it can be saved
///
///	\param [in] pBufferInfo		Pointer of J_tIMAGE_INFO structure in which the information about the allocated buffer will be stored.
///	\param [in] pPath			Full file name and path of the tiff file to be stored to disk.
/// \retval						Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
/// \par
/// \code
/// // C Sample that converts a raw image and saved the result to a file
/// BOOL SaveImageToFile(J_tIMAGE_INFO* ptRawImageInfo, LPCTSTR sFileName)
/// {
///   J_tIMAGE_INFO tCnvImageInfo;    // Image info structure
/// 
///   // Allocate the buffer to hold converted the image
///   if (J_Image_Malloc(ptRawImageInfo, &tCnvImageInfo) != J_ST_SUCCESS)             return FALSE;
///   // Convert the raw image to image format
///   if (J_Image_FromRawToImage(ptRawImageInfo, &tCnvImageInfo) != J_ST_SUCCESS)     return FALSE;
///   // Save the image to disk in TIFF format
///   if (J_Image_SaveFile(&tCnvImageInfo, sFileName) != J_ST_SUCCESS)                return FALSE;
///   // Free up the image buffer
///   if (J_Image_Free(&tCnvImageInfo) != J_ST_SUCCESS)                               return FALSE;
/// 
///   return TRUE;
/// }
/// \endcode
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Image_SaveFile(J_tIMAGE_INFO * pBufferInfo, LPCTSTR pPath);

//******************************************************************************************************************
/// \brief	Get the pixel at a specified position
/// 
/// Read the current pixel value at the position \c pPoint. A raw image needs to be converted using \c J_Image_FromRawToImage() before pixels can be read.
///
///	\param [in] pBufferInfo		J_tIMAGE_INFO structure in which the pixel value will be read.
///	\param [in] pPoint			Absolute position in which pixel value is read.
///	\param [out] pPixel			Pixel value data read from the image.
///	            				Where to get the actual values from the \c Pixel Value structure depends on the \c PixelFormat that can be read from \c pBufferInfo.
/// \retval						Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
/// \sa \c J_tIMAGE_INFO
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Image_GetPixel(J_tIMAGE_INFO * pBufferInfo, POINT *pPoint, void * pPixel);

//******************************************************************************************************************
/// \brief	Set the pixel at a specified position
/// 
/// Writes the current pixel value at the position \c pPoint. A raw image needs to be converted using \c J_Image_FromRawToImage() before pixels can be written.
///
///	\param [in] pBufferInfo		J_tIMAGE_INFO structure in which the pixel value will be written.
///	\param [in] pPoint			Absolute position in which pixel value is written.
///	\param [in] pPixel			Pixel value data to write.
///	    						Where to set the actual values inside the \c PixelValue structure depends on the \c PixelFormat that can be read from \c BufferInfo.
/// \retval						Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
/// \sa \c J_tIMAGE_INFO
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Image_SetPixel(J_tIMAGE_INFO * pBufferInfo, POINT *pPoint, void * pPixel);

//******************************************************************************************************************
/// \brief	Measures pixel mean value.
///
///	\param [in] pBufferInfo		J_tIMAGE_INFO structure which contains a converted image.
///	\param [in] pMeasureRect	Measurement rectangular coordinates.
///	\param [out] pRGBAverage	Measured average pixel value for all the pixels inside the measurement rectangle. 
///                             The pixel format for the pRGBAverage structure will always be in BGR48 format (16 bit per color)
/// \retval						Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE J_Image_GetAverage(J_tIMAGE_INFO * pBufferInfo, RECT *pMeasureRect, J_tBGR48 *pRGBAverage);

/// @}
/// \defgroup WRAP_IMAGE_WB	Image white balance control functions
/// \ingroup WRAP_IMAGE
/// @{

//******************************************************************************************************************
/// \brief	Set RGB software gain.
/// 
/// The gain is only used inside the factory when the \c J_Image_OpenStreamLight() has been used to display live video. 
///	\param [in] hThread		Handle to the stream channel opened using \c J_Image_OpenStream() or \c J_Image_OpenStreamLight().
/// \param [in] iRGain      Gain for the red color channel for Bayer conversion (0x01000(4096) = 1.00)
/// \param [in] iGGain      Gain for the green color channel for Bayer conversion (0x01000(4096) = 1.00)
/// \param [in] iBGain      Gain for the blue color channel for Bayer conversion (0x01000(4096) = 1.00)
/// \note                 
/// This function doesn't return the white balance gain from inside the camera. It is only used inside the factory when 
/// the \c J_Image_OpenStreamLight() has been used to display live video
/// \retval               Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
/// \note                 This function doesn't affect the gain in the camera.
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE	 J_Image_SetGain(HANDLE hThread, uint32_t iRGain, uint32_t iGGain, uint32_t iBGain);

//******************************************************************************************************************
/// \brief	Get RGB software gain.
/// 
/// The gain is only used inside the factory when the \c J_Image_OpenStreamLight() has been used to display live video. \n
/// If \c J_Image_ExecuteWhiteBalance() has been called, the resulting gain values can be read with this function after the next 
/// image has been received on the stream channel.
///	\param [in] hThread		Handle to the stream channel opened using \c J_Image_OpenStream() or \c J_Image_OpenStreamLight().
/// \param [out] piRGain    Gain for the red color channel for Bayer conversion (0x01000(4096) = 1.00)
/// \param [out] piGGain    Gain for the green color channel for Bayer conversion (0x01000(4096) = 1.00)
/// \param [out] piBGain    Gain for the blue color channel for Bayer conversion (0x01000(4096) = 1.00)
/// \note                 
/// This function doesn't affect the white balance settings inside the camera.
/// \retval					Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE	 J_Image_GetGain(HANDLE hThread, uint32_t * piRGain, uint32_t * piGGain, uint32_t * piBGain);

//******************************************************************************************************************
/// \brief	Calculate software auto white balance
/// 
/// When this function has been called, the resulting gain values can be read with \c J_Image_GetGain() function after the next 
/// image has been received on the stream channel.
/// The resulting white balance is only used inside the factory when the \c J_Image_OpenStreamLight() has been used to display live video. 
/// 
///	\param [in] hThread		Handle to the stream channel opened using \c J_Image_OpenStream() or \c J_Image_OpenStreamLight().
/// \retval					Status code defined in the J_STATUS_CODES. If the function succeeds, returns J_ST_SUCCESS.
/// \note                 
/// After executing this function, you can use the \c J_Image_GetGain() function to get the resulting gain values.
EXTERN_C GCFC_IMPORT_EXPORT J_STATUS_TYPE	 J_Image_ExecuteWhiteBalance(THRD_HANDLE hThread);
/// @}

/// @}
#endif
