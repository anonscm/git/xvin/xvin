# ifndef MENU_DAS1602_C
# define MENU_DAS1602_C

# include <allegro.h>
# include <winalleg.h>

# include <stdio.h>
# include <conio.h>
# include <float.h>


//# include "example.h"
# include "cbw.h"
/* If you include other plug-ins header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "xvin.h"

# ifndef menu_das1602_H
# include "menu_das1602.h"
# endif

# ifndef DAS1602_H 
# include "das1602.h"
# endif

int init_parameters(void)
{
                             parameters.HighChan_in=1;
                             parameters.BoardNum=1;
                             parameters.HighChan_out=1;
                             parameters.LowChan_in=1;
                             parameters.LowChan_out=1;
                             parameters.rate_in=10000;
                             parameters.rate_out=10000;
                             parameters.range_in=BIP10VOLTS;
                             parameters.range_out=UNI10VOLTS;
                             parameters.trigger=TRIGABOVE;
                             parameters.count_in=8192;
                             parameters.options_in=BACKGROUND+CONTINUOUS;
                             parameters.count_out=8192;
                             parameters.HighThreshold=5.0;
                             parameters.LowThreshold=0;
                             //win_printf("Acquisition parameters are set to default value");
                             return 0;
}


MENU *das1602_menu(void)
{
	static MENU mn[32];
	
	if (mn[0].text != NULL)	return mn;

	
   add_item_to_menu(mn, "Generate sinusoide",                	do_sinusoide_out,   NULL,       0, NULL  );
   add_item_to_menu(mn, "&Set point on line 1",                do_set_out,    		NULL,       0, NULL  );
   add_item_to_menu(mn, "S&witch ON the card",                 init_das1602,    	NULL,       0, NULL  );
   add_item_to_menu(mn, "&Read data",                			do_read_data,    	NULL,       0, NULL  );
   add_item_to_menu(mn, "Test",                	            test,  NULL,       0, NULL  );
   add_item_to_menu(mn, "Read &Single data",                			do_get_in,    	NULL,       0, NULL  );
   
   add_item_to_menu(mn, "Test stop",                			stop_background_out,  NULL,       0, NULL  );
   add_item_to_menu(mn, "&Parameters",                	        NULL, parameters_menu,       0, NULL  ); 
   add_item_to_menu(mn, "Increment pifoc",                			increment_pifoc,    	NULL,       0, NULL  );
   add_item_to_menu(mn, "Decrement pifoc",                			decrement_pifoc,    	NULL,       0, NULL  );
   
   
   return mn;
}




//MENU parameters_menu[32] =
//{
//   static MENU mn[32];
//	
//	if (mn[0].text != NULL)	return mn;
//	
//   add_item_to_menu(mn, "Number of points in",                	do_set_Count_in,   NULL,       0, NULL  );
//   add_item_to_menu(mn, "Rate in",                				do_set_Rate_in,    		NULL,       0, NULL  );
//   add_item_to_menu(mn, "Number of points out",               	do_set_Count_out,   NULL,       0, NULL  );
//   add_item_to_menu(mn, "Rate out",                			do_set_Rate_out,    		NULL,       0, NULL  );
//   
//   add_item_to_menu(mn, "HighTreshold",                		do_set_H_Treshold,    		NULL,       0, NULL  );
//   add_item_to_menu(mn, "Low Threshold",                		do_set_L_Treshold,    		NULL,       0, NULL  );
//   add_item_to_menu(mn, "Range in",                			NULL,    		Range_in_menu,       0, NULL  );
//   add_item_to_menu(mn, "Range out",                			NULL,    		Range_out_menu,       0, NULL  );
//   
//   add_item_to_menu(mn, "Trigger",                				NULL,    		Trigger_menu,       0, NULL  );
//   
//   add_item_to_menu(mn, "Channels in",                			NULL,    		Channel_in_menu,       0, NULL  );
//   add_item_to_menu(mn, "Channels out",                		NULL,    		Channel_out_menu,       0, NULL  );
//   
//   add_item_to_menu(mn, "Options in",                		do_set_options_in_bis,    		NULL,       0, NULL  );
//   add_item_to_menu(mn, "Options out",                		do_set_options_out_bis,    		NULL,       0, NULL  );
//   
//   return mn;
//}
MENU parameters_menu[32] =
{
   
   { "Number of points in",                	do_set_Count_in,   NULL,       0, NULL  },
   { "Rate in",                				do_set_Rate_in,    		NULL,       0, NULL  },
   { "Number of points out",               	do_set_Count_out,   NULL,       0, NULL  },
   { "Rate out",                			do_set_Rate_out,    		NULL,       0, NULL  },
   
   { "HighTreshold",                		do_set_H_Treshold,    		NULL,       0, NULL  },
   { "Low Threshold",                		do_set_L_Treshold,    		NULL,       0, NULL  },
   { "Range in",                			NULL,    		Range_in_menu,       0, NULL  },
   { "Range out",                			NULL,    		Range_out_menu,       0, NULL  },
   
   { "Trigger",                				NULL,    		Trigger_menu,       0, NULL  },
   /*{ "Options in",                		NULL,    		Options_in_menu,       0, NULL  },
   { "Options out",                			NULL,    		Options_out_menu,       0, NULL  },
   */
   { "Channels in",                			NULL,    		Channel_in_menu,       0, NULL  },
   { "Channels out",                		NULL,    		Channel_out_menu,       0, NULL  },
   
   { "Options in",                		do_set_options_in_bis,    		NULL,       0, NULL  },
   { "Options out",                		do_set_options_out_bis,    		NULL,       0, NULL  },
   
   { NULL,                                  NULL,             NULL,       0, NULL  }
};

MENU Trigger_menu[32] =
{
   
   { " TRIGABOVE",                	do_set_trig,   NULL,       0, NULL  },
   { "TRIGBELOW",                	do_set_trig,    		NULL,       0, NULL  },
   { "GATE_NEG_HYS",                do_set_trig,    		NULL,       0, NULL  },
   { "GATE_POS_HYS",                do_set_trig,    		NULL,       0, NULL  },
   { "GATE_ABOVE",                	do_set_trig,    		NULL,       0, NULL  },
   { "GATE_BELOW",                	do_set_trig,NULL,       0, NULL  },
   { "GATE_IN_WINDOW",              do_set_trig,    		NULL,       0, NULL  },
   { "GATE_OUT_WINDOW",             do_set_trig,    		NULL,       0, NULL  },
   { "GATE_HIGH",                	do_set_trig,    		NULL,       0, NULL  },
   { "GATE_LOW",                	do_set_trig,    		NULL,       0, NULL  },
   { "TRIG_HIGH  ",                	do_set_trig,    		NULL,       0, NULL  },
   { "TRIG_LOW",                	do_set_trig,    		NULL,       0, NULL  },
   { "TRIG_POS_EDGE",              	do_set_trig,    		NULL,       0, NULL  },
   { "TRIG_NEG_EDGE",               do_set_trig,    		NULL,       0, NULL  },
   { NULL,                                  NULL,             NULL,       0, NULL  }
};
//MENU *Trigger_menu(void)
//{
//   static MENU mn[32];
//	
//	if (mn[0].text != NULL)	return mn;
//
//   add_item_to_menu(mn, " TRIGABOVE",                	do_set_trig,            NULL,       TRIGABOVE, NULL  );
//   add_item_to_menu(mn,"TRIGBELOW",                	do_set_trig,    		NULL,       TRIGBELOW, NULL  );
//   add_item_to_menu(mn, "GATE_NEG_HYS",                do_set_trig,    		NULL,       GATE_NEG_HYS, NULL  );
//   add_item_to_menu(mn, "GATE_POS_HYS",                do_set_trig,    		NULL,       GATE_POS_HYS, NULL  );
//   add_item_to_menu(mn, "GATE_ABOVE",                	do_set_trig,    		NULL,       GATE_ABOVE, NULL  );
//   add_item_to_menu(mn, "GATE_BELOW",                	do_set_trig,            NULL,       GATE_BELOW, NULL  );
//   add_item_to_menu(mn, "GATE_IN_WINDOW",              do_set_trig,    		NULL,       GATE_IN_WINDOW, NULL  );
//   add_item_to_menu(mn, "GATE_OUT_WINDOW",             do_set_trig,    		NULL,       GATE_OUT_WINDOW, NULL  );
//   add_item_to_menu(mn, "GATE_HIGH",                	do_set_trig,    		NULL,       GATE_HIGH, NULL  );
//   add_item_to_menu(mn, "GATE_LOW",                	do_set_trig,    		NULL,       GATE_LOW, NULL  );
//   add_item_to_menu(mn, "TRIG_HIGH  ",                	do_set_trig,    		NULL,       TRIG_HIGH, NULL  );
//   add_item_to_menu(mn, "TRIG_LOW",                	do_set_trig,    		NULL,       TRIG_LOW, NULL  );
//   add_item_to_menu(mn, "TRIG_POS_EDGE",              	do_set_trig,    		NULL,       TRIG_POS_EDGE, NULL  );
//   add_item_to_menu(mn, "TRIG_NEG_EDGE",               do_set_trig,    		NULL,       TRIG_NEG_EDGE, NULL  );
//   return mn;
//}

MENU Options_in_menu[32] =
{
   
   { "BACKGROUND",                	do_set_background_in,   NULL,       0, NULL  },
   { "CONTINUOUS",                	do_set_continuous_in,    		NULL,       0, NULL  },
   { "CONVERTDATA",                	do_set_convertdata_in,    		NULL,       0, NULL  },
   { "SINGLEIO ",                	do_set_singleio_in,    		NULL,       0, NULL  },
   { "BURSTMODE",                	do_set_burstmode_in,    		NULL,       0, NULL  },
   { "EXTTRIGGER",                	do_set_exttrigger_in,NULL,       0, NULL  },
   { NULL,                                  NULL,             NULL,       0, NULL  }
};


MENU Options_out_menu[32] =
{
   
   { "BACKGROUND",                	do_set_background_out,   NULL,       0, NULL  },
   { "CONTINUOUS",                	do_set_continuous_out,    		NULL,       0, NULL  },
   { "CONVERTDATA",                	do_set_convertdata_out,    		NULL,       0, NULL  },
   { "SINGLEIO ",                	do_set_singleio_out,    		NULL,       0, NULL  },
   { "BURSTMODE",                	do_set_burstmode_out,    		NULL,       0, NULL  },
   { "EXTTRIGGER",                	do_set_exttrigger_out,NULL,       0, NULL  },
   { NULL,                                  NULL,             NULL,       0, NULL  }
};


MENU Range_in_menu[32] =
{   
   { "BIP10VOLTS",                	do_set_Range_in,   NULL,       BIP10VOLTS, NULL  },
   { "BIP5VOLTS",                	do_set_Range_in,    		NULL,       BIP5VOLTS, NULL  },
   { "BIP2PT5VOLTS",                do_set_Range_in,    		NULL,       BIP2PT5VOLTS, NULL  },
   { "BIP1PT25VOLTS",               do_set_Range_in,    		NULL,       BIP1PT25VOLTS, NULL  },
   { "UNI10VOLTS",                	do_set_Range_in,   NULL,       UNI10VOLTS, NULL  },
   { "UNI5VOLTS",                	do_set_Range_in,    		NULL,       UNI5VOLTS, NULL  },
   { "UNI2PT5VOLTS",                do_set_Range_in,    		NULL,       UNI2PT5VOLTS, NULL  },
   { "UNI1PT25VOLTS",               do_set_Range_in,    		NULL,       UNI1PT25VOLTS, NULL  },
   { NULL,                                  NULL,             NULL,       0, NULL  }
};

MENU Range_out_menu[32] =
{   
   { "BIP10VOLTS",                	do_set_Range_out,   NULL,       BIP10VOLTS, NULL  },
   { "BIP5VOLTS",                	do_set_Range_out,    		NULL,       BIP5VOLTS, NULL  },
   { "BIP2PT5VOLTS",                do_set_Range_out,    		NULL,       BIP2PT5VOLTS, NULL  },
   { "BIP1PT25VOLTS",               do_set_Range_out,    		NULL,       BIP1PT25VOLTS, NULL  },
   { "UNI10VOLTS",                	do_set_Range_out,   NULL,       UNI10VOLTS, NULL  },
   { "UNI5VOLTS",                	do_set_Range_out,    		NULL,       UNI5VOLTS, NULL  },
   { "UNI2PT5VOLTS",                do_set_Range_out,    		NULL,       UNI2PT5VOLTS, NULL  },
   { "UNI1PT25VOLTS",               do_set_Range_out,    		NULL,       UNI1PT25VOLTS, NULL  },
   { NULL,                                  NULL,             NULL,       0, NULL  }
};

MENU Channel_in_menu[32] =
{   
   { "Low Channel in",              	do_set_LowChan_in,   NULL,       0, NULL  },
   { "High Channel in",                	do_set_HighChan_in,    		NULL,       0, NULL  },
   { NULL,                                  NULL,             NULL,       0, NULL  }
};

MENU Channel_out_menu[32] =
{   
   { "Low Channel out",              	do_set_LowChan_out,   NULL,       0, NULL  },
   { "High Channel out",                do_set_HighChan_out,    		NULL,       0, NULL  },
   { NULL,                                  NULL,             NULL,       0, NULL  }
};


//MENU *Options_in_menu(void)
//{
//   static MENU mn[32];
//	
//	if (mn[0].text != NULL)	return mn;
//
//   add_item_to_menu(mn, "BACKGROUND",                	do_set_background_in,   NULL,       0, NULL  );
//   add_item_to_menu(mn, "CONTINUOUS",                	do_set_continuous_in,    		NULL,       0, NULL  );
//   add_item_to_menu(mn, "CONVERTDATA",                	do_set_convertdata_in,    		NULL,       0, NULL  );
//   add_item_to_menu(mn, "SINGLEIO ",                	do_set_singleio_in,    		NULL,       0, NULL  );
//   add_item_to_menu(mn, "BURSTMODE",                	do_set_burstmode_in,    		NULL,       0, NULL  );
//   add_item_to_menu(mn, "EXTTRIGGER",                	do_set_exttrigger_in,NULL,       0, NULL  );
//   return mn;
//}
//
//
//MENU *Options_out_menu(void)
//{
//    static MENU mn[32];
//	
//	if (mn[0].text != NULL)	return mn;   
//   add_item_to_menu(mn, "BACKGROUND",                	do_set_background_out,   NULL,       0, NULL  );
//   add_item_to_menu(mn, "CONTINUOUS",                	do_set_continuous_out,    		NULL,       0, NULL  );
//   add_item_to_menu(mn, "CONVERTDATA",                	do_set_convertdata_out,    		NULL,       0, NULL  );
//   add_item_to_menu(mn, "SINGLEIO ",                	do_set_singleio_out,    		NULL,       0, NULL  );
//   add_item_to_menu(mn, "BURSTMODE",                	do_set_burstmode_out,    		NULL,       0, NULL  );
//   add_item_to_menu(mn, "EXTTRIGGER",                	do_set_exttrigger_out,NULL,       0, NULL  );
//   return mn;
//}
//
//
//MENU * Range_in_menu (void)
//{   static MENU mn[32];
//	
//	if (mn[0].text != NULL)	return mn;
//   add_item_to_menu(mn, "BIP10VOLTS",                	do_set_Range_in,            NULL,       BIP10VOLTS, NULL  );
//   add_item_to_menu(mn, "BIP5VOLTS",                	do_set_Range_in,    		NULL,       BIP5VOLTS, NULL  );
//   add_item_to_menu(mn, "BIP2PT5VOLTS",                do_set_Range_in,    		NULL,       BIP2PT5VOLTS, NULL  );
//   add_item_to_menu(mn, "BIP1PT25VOLTS",               do_set_Range_in,    		NULL,       BIP1PT25VOLTS, NULL  );
//   add_item_to_menu(mn, "UNI10VOLTS",                	do_set_Range_in,            NULL,       UNI10VOLTS, NULL  );
//   add_item_to_menu(mn, "UNI5VOLTS",                	do_set_Range_in,    		NULL,       UNI5VOLTS, NULL  );
//   add_item_to_menu(mn, "UNI2PT5VOLTS",                do_set_Range_in,    		NULL,       UNI2PT5VOLTS, NULL  );
//   add_item_to_menu(mn, "UNI1PT25VOLTS",               do_set_Range_in,    		NULL,       UNI1PT25VOLTS, NULL  );
//   return mn;
//}
//
//MENU * Range_out_menu (void)
//{   static MENU mn[32];
//	
//	if (mn[0].text != NULL)	return mn;
//   add_item_to_menu(mn, "BIP10VOLTS",                	do_set_Range_out,           NULL,       BIP10VOLTS, NULL  );
//   add_item_to_menu(mn, "BIP5VOLTS",                	do_set_Range_out,    		NULL,       BIP5VOLTS, NULL  );
//   add_item_to_menu(mn, "BIP2PT5VOLTS",                do_set_Range_out,    		NULL,       BIP2PT5VOLTS, NULL  );
//   add_item_to_menu(mn, "BIP1PT25VOLTS",               do_set_Range_out,    		NULL,       BIP1PT25VOLTS, NULL  );
//   add_item_to_menu(mn, "UNI10VOLTS",                	do_set_Range_out,           NULL,       UNI10VOLTS, NULL  );
//   add_item_to_menu(mn, "UNI5VOLTS",                	do_set_Range_out,    		NULL,       UNI5VOLTS, NULL  );
//   add_item_to_menu(mn, "UNI2PT5VOLTS",                do_set_Range_out,    		NULL,       UNI2PT5VOLTS, NULL  );
//   add_item_to_menu(mn, "UNI1PT25VOLTS",               do_set_Range_out,    		NULL,       UNI1PT25VOLTS, NULL  );
//   return mn;
//}
//
//MENU * Channel_in_menu (void)
//{   static MENU mn[32];
//	
//	if (mn[0].text != NULL)	return mn;
//   add_item_to_menu(mn, "Low Channel in",              	do_set_LowChan_in,   NULL,       0, NULL  );
//   add_item_to_menu(mn, "High Channel in",                	do_set_HighChan_in,    		NULL,       0, NULL  );
//   return mn;
//}
//
//MENU * Channel_out_menu (void)
//{  static MENU mn[32];
//	
//	if (mn[0].text != NULL)	return mn; 
//   add_item_to_menu(mn, "Low Channel out",              	do_set_LowChan_out,   NULL,       0, NULL  );
//   add_item_to_menu(mn, "High Channel out",                do_set_HighChan_out,    		NULL,       0, NULL  );
//   return mn;
//}

int do_set_LowChan_in(void)
{
    if(updating_menu_state != 0)	return D_O_K;

	win_scanf("Minimum number channel for input(0 to 8) %d",&parameters.LowChan_in);
	if ((parameters.LowChan_in<0)||(parameters.LowChan_in>8)||((parameters.LowChan_in>parameters.HighChan_in)))
	{win_printf("Bad Value");parameters.LowChan_in=0;}
 
 	return 0;
}


int do_set_LowChan_out(void)
{
    if(updating_menu_state != 0)	return D_O_K;

	win_scanf("Minimum number channel for output(0 to 1) %d",&parameters.LowChan_out);
	if ((parameters.LowChan_out<0)||(parameters.LowChan_out>1)||((parameters.LowChan_out>parameters.HighChan_out)))
	{win_printf("Bad Value");parameters.LowChan_out=0;}
 
 	return 0;
}

int do_set_HighChan_in(void)
{
    if(updating_menu_state != 0)	return D_O_K;

	win_scanf("Maximum number channel for input(0 to 8) %d",&parameters.HighChan_in);
	if ((parameters.HighChan_in<0)||(parameters.HighChan_in>8)||((parameters.LowChan_in>parameters.HighChan_in)))
	{win_printf("Bad Value");parameters.HighChan_in=8;}
 
 
 	return 0;
}


int do_set_HighChan_out(void)
{
    if(updating_menu_state != 0)	return D_O_K;

	win_scanf("Minimum number channel for output(0 to 1) %d",&parameters.HighChan_out);
	if ((parameters.HighChan_out<0)||(parameters.HighChan_out>1)||((parameters.LowChan_out>parameters.HighChan_out)))
	{win_printf("Bad Value");parameters.HighChan_out=1;}
 
 
 	return 0;
}

int do_set_Range_out(void)
{
    if(updating_menu_state != 0)	return D_O_K;

	parameters.range_out=RETRIEVE_MENU_INDEX;
	return 0;
}


int do_set_Range_in(void)
{
    if(updating_menu_state != 0)	return D_O_K;

	parameters.range_in=RETRIEVE_MENU_INDEX;
	return 0;
}

int do_set_options_in_bis(void)
{	static int background=0;
	static int extrigger=1;
	static int continuous=0;
	static int burstmode=1;
	static int singleio=0;
	static int convertdata=1;

	if(updating_menu_state != 0)	return D_O_K;

	win_scanf("BACKGROUND?(0,1) %d\n BURSTMODE? %d \n CONTINUOUS? %d\n SINGLEIO %d\n EXTERNAL TRIGGER %d\n COVERTDATA %d\n",&background,&burstmode,&continuous,&singleio,&extrigger,&convertdata);
 
 
 	parameters.options_in=background*BACKGROUND+extrigger*EXTTRIGGER+continuous*CONTINUOUS+burstmode*BURSTMODE+singleio*SINGLEIO+convertdata*CONVERTDATA;
	return 0;
	
}


int do_set_options_out_bis(void)
{	static int background=0;
	static int extrigger=1;
	static int continuous=0;
	static int burstmode=1;
	static int singleio=0;
	static int convertdata=1;

	if(updating_menu_state != 0)	return D_O_K;
	
	win_scanf("BACKGROUND?(0,1) %d\n BURSTMODE? %d \n CONTINUOUS? %d\n SINGLEIO %d\n EXTERNAL TRIGGER %d\n COVERTDATA %d\n",&background,&burstmode,&continuous,&singleio,&extrigger,&convertdata);
 
 
 	parameters.options_out=background+extrigger+continuous+burstmode+singleio+convertdata;
	return 0;
	
}


int do_set_exttrigger_in(void)
{	
    if(updating_menu_state != 0)	return D_O_K;
    
    if (((parameters.options_in)&EXTTRIGGER) == EXTTRIGGER)
		
		{
  		(parameters.options_in)-=EXTTRIGGER;
		win_printf("EXTTRIGGER WAS ON IT IS NOW OFF %x",parameters.options_in);
    	}
    
    
    else 
    	{
  		(parameters.options_in)+=EXTTRIGGER;
		win_printf("EXTTRIGGER WAS OFF IT IS NOW ON %x",parameters.options_in);
    	}
     
		return 0;

}

int do_set_burstmode_in(void)
{	
    if(updating_menu_state != 0)	return D_O_K;

    if (((parameters.options_in)&BURSTMODE) == BURSTMODE)
		
		{
  		(parameters.options_in)-=BURSTMODE;
		win_printf("BURSTMODE WAS ON IT IS NOW OFF %x",parameters.options_in);
    	}
    
    
    else 
    	{
  		(parameters.options_in)+=BURSTMODE;
		win_printf("BURSTMODE WAS OFF IT IS NOW ON %x",parameters.options_in);
    	}
     
		return 0;

}

int do_set_singleio_in(void)
{	
    if(updating_menu_state != 0)	return D_O_K;

    if (((parameters.options_in)&SINGLEIO)==SINGLEIO)
		
		{
  		(parameters.options_in)-=SINGLEIO;
		win_printf("SINGLEIO WAS ON IT IS NOW OFF %x",parameters.options_in);
    	}
    
    
    else 
    	{
  		(parameters.options_in)+=SINGLEIO;
		win_printf("SINGLEIO WAS OFF IT IS NOW ON %x",parameters.options_in);
    	}
     
		return 0;

}



int do_set_convertdata_in(void)
{
    if(updating_menu_state != 0)	return D_O_K;

     	if (((parameters.options_in)&CONVERTDATA)==CONVERTDATA)
		
		{
  		(parameters.options_in)-=CONVERTDATA;
		win_printf("CONVERTDATA WAS ON IT IS NOW OFF %x",parameters.options_in);
    	}
    
    
    else 
    	{
  		(parameters.options_in)+=CONVERTDATA;
		win_printf("CONVERTDATA WAS OFF IT IS NOW ON %x",parameters.options_in);
    	}
     
		return 0;

}









int do_set_background_in(void)
{	
    if(updating_menu_state != 0)	return D_O_K;

    if (((parameters.options_in)&BACKGROUND)==BACKGROUND)
		
		{
  		(parameters.options_in)-=BACKGROUND;
		win_printf("BACKGROUNG WAS ON IT IS NOW OFF %x",parameters.options_in);
    	}
    
    
    else 
    	{
  		(parameters.options_in)+=BACKGROUND;
		win_printf("BACKGROUNG WAS OFF IT IS NOW ON %x",parameters.options_in);
    	}
     
		return 0;

}


int do_set_continuous_in(void)
{	
    if(updating_menu_state != 0)	return D_O_K;

    if (((parameters.options_in)&CONTINUOUS)==CONTINUOUS)
		
		{
  		(parameters.options_in)-=CONTINUOUS;
		win_printf("CONTINUOUS WAS ON IT IS NOW OFF %x",parameters.options_in);
    	}
    
    
    else 
    	{
  		(parameters.options_in)+=CONTINUOUS;
		win_printf("CONTINUOUS WAS OFF IT IS NOW ON %x",parameters.options_in);
    	}
     
		return 0;

}

int do_set_trig(void)
{	
     if(updating_menu_state != 0)	return D_O_K;
     parameters.trigger = RETRIEVE_MENU_INDEX ;
 	return 0;
}


int do_set_Count_out(void)
{
    if(updating_menu_state != 0)	return D_O_K;


	win_scanf("Number of samples %d",&(parameters.count_out));
	return 0;
}

int do_set_Rate_out(void)
{
    if(updating_menu_state != 0)	return D_O_K;

	win_scanf("Sampling Rate %d",&(parameters.rate_out) );
	return 0;
}



int do_set_Count_in(void)
{
    if(updating_menu_state != 0)	return D_O_K;

	win_scanf("Number of samples %d",&(parameters.count_in));
	return 0;
}

int do_set_Rate_in(void)
{
    if(updating_menu_state != 0)	return D_O_K;

	win_scanf("Sampling Rate %d",&(parameters.rate_in) );
	return 0;
}


int do_set_H_Treshold(void)
{
    if(updating_menu_state != 0)	return D_O_K;

	win_scanf("High Threshold %f",&(parameters.HighThreshold) );
	return 0;
}


int do_set_L_Treshold(void)
{
    if(updating_menu_state != 0)	return D_O_K;

	win_scanf("Low Threshold %f",&(parameters.LowThreshold) );
	return 0;
}

int test(void)
{   int toto;

    if(updating_menu_state != 0)	return D_O_K;
	
	win_scanf("Toto %d",&toto);
	win_printf("toto&background %x",(toto&BACKGROUND)); 
	win_printf("toto&burstmode %x",(toto&BURSTMODE));
	win_printf("toto&continuous %x",(toto&CONTINUOUS));
/*	int status=1;
	
	float *datavolts;
	int i=0;
	long Count=parameters.count;
	long Rate=parameters.rate;
	
	
	
	while (keypressed()!=TRUE)
	{for (i=0;i<1000000;i++);
	}
	      
	
	
	
	
	datavolts=(float *)malloc(Count*sizeof(float));
	for(i=0;i<Count;i++)
	{*(datavolts+i)=i;}
	
	do_plot_oscillo(status, Count,Rate,datavolts);*/
	return 0;
}

int test_stop(void)
{	int j=1;
	
 if(updating_menu_state != 0)	return D_O_K;
//i=j;
	return 0;
}



int do_set_out(void)
{   static float data=0;

    if(updating_menu_state != 0)	return D_O_K;

    win_scanf("Value to write?%f (-10,+10V)",&data);
    do_set_out_data(data);
    //win_printf("On est de retour %f",data);

    return 0;
}

int do_get_in(void)
{   static float data=0;

    if(updating_menu_state != 0)	return D_O_K;

    //win_scanf("Value to write?%f (-10,+10V)",&data);
    do_get_in_data(&data);
    win_printf("On est de retour %f",data);

    return 0;
}

int do_plot_oscillo(int status, long Count,long Rate, int Options, float *datavolts)
{
    static O_p *op = NULL;
	static d_s *ds_bin = NULL;
    static pltreg *pr = NULL;
    int i;
    
    
    if(updating_menu_state != 0)	return D_O_K;

    pr = find_pr_in_current_dialog(NULL);
    if (pr == NULL)	return 1;
	if (op==NULL)
 	{/*op=pr->o_p[pr->cur_op];
	ds_bin = get_op_cur_ds(op);*/
    /*win_printf("On arrive ds nx %d  \n op %d",ds_bin->nx,pr->cur_op);*/	
	op = create_one_plot(Count, Count, 0);
	ds_bin = get_op_cur_ds(op);
	ds_bin=build_adjust_data_set(ds_bin,Count,Count);
	 
	add_data( pr, IS_ONE_PLOT, (void*)op);
	}
	else
	{
		op=pr->o_p[pr->cur_op];
		ds_bin = get_op_cur_ds(op);
		ds_bin=build_adjust_data_set(ds_bin,Count,Count);//faudrait berifier point pour eviter alloc systematique
	}
	
	
	for (i = 0; i < Count; i++)
		   {
		    	ds_bin->xd[i] = (Rate != 0) ?((float)i)/(Rate) : i;
		    	
			    ds_bin->yd[i] = datavolts[i];
		   }
	
 		   if (status==1)
		   set_plot_title(op, "Acquisition ON %x",parameters.options_in);
		   else set_plot_title(op, "Acquisition OFF %x",parameters.options_in);
		   set_plot_x_title(op, "Time");	
		   set_plot_y_title(op, "Volt ");
		   set_ds_plain_line(ds_bin);
		   op->need_to_refresh = 1;
		   /*return D_REDRAWME;*/
	    broadcast_dialog_message(MSG_DRAW,0);
  
  return 0;
  }
  
  
  
int do_set_exttrigger_out(void)
{
    if(updating_menu_state != 0)	return D_O_K;

	if (((parameters.options_out)&EXTTRIGGER)==EXTTRIGGER)
		
		{
  		(parameters.options_out)-=EXTTRIGGER;
		win_printf("EXTTRIGGER WAS ON IT IS NOW OFF %x",parameters.options_in);
    	}
    
    
    else 
    	{
  		(parameters.options_out)+=EXTTRIGGER;
		win_printf("EXTTRIGGER WAS OFF IT IS NOW ON %x",parameters.options_in);
    	}
     
		return 0;

}

int do_set_burstmode_out(void)
{
    if(updating_menu_state != 0)	return D_O_K;

	if (((parameters.options_out)&BURSTMODE)==BURSTMODE)
		
		{
  		(parameters.options_out)-=BURSTMODE;
		win_printf("BURSTMODE WAS ON IT IS NOW OFF %x",parameters.options_in);
    	}
    
    
    else 
    	{
  		(parameters.options_out)+=BURSTMODE;
		win_printf("BURSTMODE WAS OFF IT IS NOW ON %x",parameters.options_in);
    	}
     
		return 0;

}

int do_set_singleio_out(void)
{
    if(updating_menu_state != 0)	return D_O_K;
	if (((parameters.options_out)&SINGLEIO)==SINGLEIO)
		
		{
  		(parameters.options_out)-=SINGLEIO;
		win_printf("SINGLEIO WAS ON IT IS NOW OFF %x",parameters.options_in);
    	}
    
    
    else 
    	{
  		(parameters.options_out)+=SINGLEIO;
		win_printf("SINGLEIO WAS OFF IT IS NOW ON %x",parameters.options_in);
    	}
     
		return 0;

}



int do_set_convertdata_out(void)
{
    if(updating_menu_state != 0)	return D_O_K;
	if (((parameters.options_out)&CONVERTDATA)==CONVERTDATA)
		
		{
  		(parameters.options_out)-=CONVERTDATA;
		win_printf("CONVERTDATA WAS ON IT IS NOW OFF %x",parameters.options_in);
    	}
    
    
    else 
    	{
  		(parameters.options_out)+=CONVERTDATA;
		win_printf("CONVERTDATA WAS OFF IT IS NOW ON %x",parameters.options_in);
    	}
     
		return 0;

}









int do_set_background_out(void)
{
    if(updating_menu_state != 0)	return D_O_K;
	if (((parameters.options_out)&BACKGROUND)==BACKGROUND)
		
		{
  		(parameters.options_out)-=BACKGROUND;
		win_printf("BACKGROUNG WAS ON IT IS NOW OFF %x",parameters.options_in);
    	}
    
    
    else 
    	{
  		(parameters.options_out)+=BACKGROUND;
		win_printf("BACKGROUNG WAS OFF IT IS NOW ON %x",parameters.options_in);
    	}
     
		return 0;

}


int do_set_continuous_out(void)
{
    if(updating_menu_state != 0)	return D_O_K;

	if (((parameters.options_out)&CONTINUOUS)==CONTINUOUS)
		
		{
  		(parameters.options_out)-=CONTINUOUS;
		win_printf("CONTINUOUS WAS ON IT IS NOW OFF %x",parameters.options_in);
    	}
    
    
    else 
    	{
  		(parameters.options_out)+=CONTINUOUS;
		win_printf("CONTINUOUS WAS OFF IT IS NOW ON %x",parameters.options_in);
    	}
     
		return 0;

}


int das1602_main(int argc, char **argv)
{
	
	init_das1602();
	
    add_plot_treat_menu_item("DAS 1602", NULL, das1602_menu() , 0, NULL);
    add_image_treat_menu_item("DAS 1602", NULL, das1602_menu() , 0, NULL);
    
    
    broadcast_dialog_message(MSG_DRAW,0);
	


	return 0;    
}


#endif


