#ifndef menu_das1602_H
#define menu_das1602_H

# define  	IS_DAS_PARAMETERS    32770


typedef struct {
		
long 	count_in;
long 	count_out;
long 	rate_in;
long 	rate_out;
float 	HighThreshold;
float 	LowThreshold;
int		trigger;
int 	range_in;
int 	range_out;
int 	options_in;
int 	options_out;
int 	BoardNum;
int 	LowChan_in;
int 	HighChan_in;
int 	LowChan_out;
int 	HighChan_out;

}das_param;



# ifndef MENU_DAS1602_C
PXV_VAR(das_param , parameters);
#else
das_param parameters;
#endif

PXV_FUNC(MENU, *das1602_menu,(void));
PXV_FUNC(MENU, parameters_menu,[32]);
PXV_FUNC(MENU, Trigger_menu,[32]);
PXV_FUNC(MENU, Options_in_menu,[32]);
PXV_FUNC(MENU, Options_out_menu,[32]);
PXV_FUNC(MENU, Range_in_menu,[32]);
PXV_FUNC(MENU, Range_out_menu,[32]);
PXV_FUNC(MENU, Channel_out_menu,[32]);
PXV_FUNC(MENU, Channel_in_menu,[32]);


PXV_FUNC(int, do_plot_oscillo,(int status,long Count,long Rate,int Options,float *datavolts));
PXV_FUNC(int, do_set_out,(void));
PXV_FUNC(int, do_get_in,(void));
PXV_FUNC(int, do_set_LowChan_in,(void));
PXV_FUNC(int, do_set_LowChan_out,(void));
PXV_FUNC(int, do_set_HighChan_in,(void));
PXV_FUNC(int, do_set_HighChan_out,(void));
PXV_FUNC(int, do_set_options_in_bis,(void));
PXV_FUNC(int, do_set_options_out_bis,(void));
PXV_FUNC(int, do_set_UNI1PT25VOLTS_in,(void));
PXV_FUNC(int, do_set_UNI2PT5VOLTS_in,(void));
PXV_FUNC(int, do_set_UNI5VOLTS_in,(void));
PXV_FUNC(int, do_set_UNI10VOLTS_in,(void));
PXV_FUNC(int, do_set_BIP1PT25VOLTS_in,(void));
PXV_FUNC(int, do_set_BIP2PT5VOLTS_in,(void));
PXV_FUNC(int, do_set_BIP5VOLTS_in,(void));
PXV_FUNC(int, do_set_BIP10VOLTS_in,(void));

PXV_FUNC(int, do_set_UNI1PT25VOLTS_out,(void));
PXV_FUNC(int, do_set_UNI2PT5VOLTS_out,(void));
PXV_FUNC(int, do_set_UNI5VOLTS_out,(void));
PXV_FUNC(int, do_set_UNI10VOLTS_out,(void));
PXV_FUNC(int, do_set_BIP1PT25VOLTS_out,(void));
PXV_FUNC(int, do_set_BIP2PT5VOLTS_out,(void));
PXV_FUNC(int, do_set_BIP5VOLTS_out,(void));
PXV_FUNC(int, do_set_BIP10VOLTS_out,(void));


PXV_FUNC(MENU*,Range_out_MENU, (void));
PXV_FUNC(MENU*,Range_in_MENU, (void));
PXV_FUNC(MENU*,Range_out_MENU, (void));
PXV_FUNC(MENU*,Channel_out_MENU, (void));
PXV_FUNC(MENU*,Channel_in_MENU, (void));



PXV_FUNC(int, do_set_trig,(void));
PXV_FUNC(int, do_set_options,(void));
PXV_FUNC(int, do_set_Range_in,(void));
PXV_FUNC(int, do_set_Range_out,(void));

PXV_FUNC(int, do_set_Count_in,(void));
PXV_FUNC(int, do_set_Rate_in,(void));
PXV_FUNC(int, do_set_Count_out,(void));
PXV_FUNC(int, do_set_Rate_out,(void));
PXV_FUNC(int, do_set_H_Treshold,(void));
PXV_FUNC(int, do_set_L_Treshold,(void));
PXV_FUNC(int, init_parameters,(void));

PXV_FUNC(int, do_set_exttrigger_in,(void));
PXV_FUNC(int, do_set_burstmode_in,(void));
PXV_FUNC(int, do_set_singleio_in,(void));
PXV_FUNC(int, do_set_convertdata_in,(void));
PXV_FUNC(int, do_set_background_in,(void));
PXV_FUNC(int, do_set_continuous_in,(void));

PXV_FUNC(int, do_set_exttrigger_out,(void));
PXV_FUNC(int, do_set_burstmode_out,(void));
PXV_FUNC(int, do_set_singleio_out,(void));
PXV_FUNC(int, do_set_convertdata_out,(void));
PXV_FUNC(int, do_set_background_out,(void));
PXV_FUNC(int, do_set_continuous_out,(void));
#endif
