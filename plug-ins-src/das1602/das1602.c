# ifndef DAS1602_C
# define DAS1602_C

#include <allegro.h>
# include <winalleg.h>

#include <stdio.h>
#include <conio.h>
#include <time.h>

#include <float.h>


#include "cbw.h"




/* If you include other plug-ins header do it here*/ 

/* But not below this define */
# define BUILDING_PLUGINS_DLL
//#include "example.h"
# include "xvin.h"
# ifndef menu_das1602_H
# include "menu_das1602.h"
# endif



# ifndef DAS1602_H 
# include "das1602.h"
# endif





/*extern das_param parameters;*/
/*extern int BoardNum;
*/
int init_das1602(void)
{
	
 	int err=0;
	int BoardNum=0;
	char *BoardnName=NULL,*ErrMsg=NULL;
	float    RevLevel = (float)CURRENTREVNUM;
	int  BoardType;
	int  NumBoards;
	
	if(updating_menu_state != 0)	return D_O_K;
	
	BoardnName=(char *)malloc(BOARDNAMELEN*sizeof(char));
	ErrMsg=(char *)malloc(ERRSTRLEN*sizeof(char));

  /* Declare UL Revision Level Must be the first function used*/
   	err = cbDeclareRevision(&RevLevel);
   	if (err!=0)
   	{
   		cbGetErrMsg( err, ErrMsg );
   		win_printf("Error %s", ErrMsg);
   	 	return 0;
	}
	
	
	cbErrHandling( DONTPRINT, DONTSTOP );
	
 /* Get the number of boards installed in system */
    cbGetConfig (GLOBALINFO, 0, 0, GINUMBOARDS, &NumBoards);

    /* Print out board type name of each installed board */
    for (BoardNum=0; BoardNum<NumBoards; BoardNum++)
        {
        /* Get board type of each board */
        cbGetConfig (BOARDINFO, BoardNum, 0, BIBOARDTYPE, &BoardType);
        
        /* If a board is installed */
        if (BoardType > 0)
           {
            cbGetBoardName (BoardNum, BoardnName);
            //win_printf ("    Board #%d = %s\n", BoardNum, BoardnName);
           }
        }
 
         
         
    BoardNum=1;//Change and put a real selection
 	err=cbGetBoardName(BoardNum, BoardnName);
	
	if (err!=0) win_printf("No card on 0");
	//else win_printf("The card installed is %s",BoardnName);
	init_parameters();
	return 0;
}

int do_read_data(void)
{
	int BoardNum=parameters.BoardNum;
 	int LowChan=parameters.LowChan_in;
  	int HighChan=parameters.HighChan_in;
   	long Count=parameters.count_in*(parameters.HighChan_in-parameters.LowChan_in+1);
    long Rate=parameters.rate_in;
    int Range=parameters.range_in;
    int Options=parameters.options_in/*BURSTMODE+BACKGROUND+EXTTRIGGER+CONVERTDATA;*/;
	unsigned short *data=NULL,LowThreshold_bin,HighThreshold_bin;
	float LowThreshold=parameters.LowThreshold,HighThreshold=parameters.HighThreshold;
	float /*EngUnits=NULL,*/*datavolts=NULL;
	int i;
	clock_t start,start1;
	int on=1,off=0;


	if(updating_menu_state != 0)	return D_O_K;	
	
	data=(unsigned short *)malloc((Count+1)*sizeof(unsigned short));
	datavolts=(float *)malloc((Count+1)*sizeof(float));
	
//	install_keyboard();
	
    
	cbFromEngUnits(BoardNum,Range, HighThreshold, &HighThreshold_bin);
	cbFromEngUnits(BoardNum,Range, LowThreshold, &LowThreshold_bin);
	cbSetTrigger (BoardNum,TRIGABOVE,LowThreshold_bin,HighThreshold_bin);
		
		start1=0;
		
	while(keypressed()!=TRUE)
	{	Rate=parameters.rate_in;
		start=clock();
		cbAInScan (BoardNum,LowChan,HighChan,Count,&Rate,Range,data, Options);
		/*start=clock();	
   		Status=RUNNING;
    	
			while(Status==RUNNING)
	    	{   while (clock()-start<(1000*(float)Count/(Rate)));
               cbGetStatus(BoardNum,&Status,&CurCount,&CurIndex,AIFUNCTION);
	    	}*/
	    	
	    	if ((clock()-start1)>100)
	    	{
			    		for (i = 0; i < Count; i++)
		   			    		{
		    	
		    			   			    		cbToEngUnits (BoardNum, Range, *(data+i), /*EngUnits*/datavolts+i);
		    			    			   			    		/*(datavolts+i)=*EngUnits;*/
	    		                }       
            
    			            do_plot_oscillo(on,Count,Rate,Options,datavolts);
    			            start1=clock();
   			}
   			
	}
	if ((parameters.options_in&BACKGROUND)==BACKGROUND)
 	{cbStopBackground(BoardNum,AIFUNCTION);}
 	
	do_plot_oscillo(off,Count,Rate,Options,datavolts);
	//remove_keyboard();
	free(data);
	free(datavolts);
	
	return 0;
}


int increment_pifoc(void)
{
    if(updating_menu_state != 0)	
    {
                      add_keyboard_short_cut(0, KEY_PLUS_PAD, 0, increment_pifoc);
                      return D_O_K;
    }
    
    piezzo_pos = (piezzo_pos > 249.0 ) ? 250.0 : piezzo_pos +1 ;
    do_set_out_data(piezzo_pos * 10.0 / 250.0);
    draw_bubble(screen,0,550,100,"piezzo pos %f",piezzo_pos);
    return 0;
} 


int decrement_pifoc(void)
{
    if(updating_menu_state != 0)	
    {
                           add_keyboard_short_cut(0, KEY_MINUS_PAD, 0, decrement_pifoc);
                           return D_O_K;
    }
    
    piezzo_pos = (piezzo_pos < 1.0 ) ? 0.0 : piezzo_pos - 1 ;
    do_set_out_data(piezzo_pos * 10.0 / 250.0);
    draw_bubble(screen,0,550,100,"piezzo pos %f",piezzo_pos);
    return 0;
}

int set_Z_value(float pos)
{
    pos = (pos < 0.0 ) ? 0.0 : pos ;
    pos = (pos > 250.0 ) ? 250.0 : pos ;
    do_set_out_data( pos * 10.0 / 250.0);
    piezzo_pos = pos;
    display_title_message("piezzo pos %f",piezzo_pos);
    //draw_bubble(screen,0,550,100,"piezzo pos %f",piezzo_pos);
    return 0;
    
    
}

float read_Z_value(void)
{
    return piezzo_pos;
    
    
}
    
int do_set_out_data(float data)
{
	
	
	int err=0;
	int BoardNum=parameters.BoardNum;
	int Channel=parameters.LowChan_out;
	int Range=parameters.range_out;
	unsigned short *datavalue;
	char ErrMsg[256];
	int InfoType=BOARDINFO;
	int DevNum=0;
	int ConfigItem=BIBOARDTYPE;
	char *BoardnName=NULL;
	int  BoardType;
	
	BoardnName=(char *)malloc(BOARDNAMELEN*sizeof(char));
    datavalue=(unsigned short *)malloc(sizeof(unsigned short));
	*datavalue=0;
	cbFromEngUnits(BoardNum,Range, data, datavalue);
	

	//win_printf("datavalue = %d",*datavalue);
	
	cbGetConfig (InfoType,BoardNum, DevNum,ConfigItem,&BoardType);
	
	if (BoardType > 0)
           {
            cbGetBoardName (BoardNum, BoardnName);
            //win_printf ("    Board #%d = %s\n", BoardNum, BoardnName);
           }
	  
	err=cbAOut(BoardNum,Channel,Range,*datavalue );
	
	
	//return 0;
	if (err!=0)
   	{
   		cbGetErrMsg( err, ErrMsg );
   		win_printf("Error %s", ErrMsg);
   	 	return 0;
	}
	
	return 0;	
}


int do_get_in_data(float *data)
{
	
	
	int err=0;
	int BoardNum=parameters.BoardNum;
	int Channel=parameters.LowChan_in;
	int Range=parameters.range_in;
	WORD datavalue = 0;
	char ErrMsg[256];
	int InfoType=BOARDINFO;
	int DevNum=0;
	int ConfigItem=BIBOARDTYPE;
	char *BoardnName=NULL;
	int  BoardType;
	
	BoardnName=(char *)malloc(BOARDNAMELEN*sizeof(char));
    
//	cbFromEngUnits(BoardNum,Range, data, datavalue);
	

	//win_printf("datavalue = %d",*datavalue);
	
	cbGetConfig (InfoType,BoardNum, DevNum,ConfigItem,&BoardType);
	
	if (BoardType > 0)
           {
            cbGetBoardName (BoardNum, BoardnName);
            //win_printf ("    Board #%d = %s\n", BoardNum, BoardnName);
           }
	  
	err=cbAIn( BoardNum, Channel, Range, &datavalue );
	win_printf("Error %d \nChannel %d\n datavalue %d\n Range %d\n BoardNum%d",err,Channel,datavalue,Range,BoardNum);
	
	//return 0;
	if (err!=0)
   	{
   		cbGetErrMsg( err, ErrMsg );
   		win_printf("Error %s", ErrMsg);
   	 	return 0;
	}
	err = cbToEngUnits ( BoardNum,  Range,  datavalue , data);
	if (err!=0)
   	{
   		cbGetErrMsg( err, ErrMsg );
   		win_printf("Error %s", ErrMsg);
   	 	return 0;
	}
	
	return 0;	
}

int do_sinusoide_out(void)
{
	static float amplitude=10;
	long n_points=parameters.count_out;
	int i;
	int err=0;
	int BoardNum=parameters.BoardNum;
	int Range=parameters.range_out;
	unsigned short *datavalue=NULL;
	unsigned short *transfer_data=0;
	int LowChan=parameters.LowChan_out;
	int HighChan=parameters.HighChan_out;
	long Rate=parameters.rate_out;
	char *ErrMsg=NULL;
	int Options=parameters.options_out;
	
	if(updating_menu_state != 0)	return D_O_K;
		
	datavalue=(unsigned short *)malloc(n_points*sizeof(unsigned short));
	transfer_data=(unsigned short *)malloc(sizeof(unsigned short));
	ErrMsg=(char *)malloc(ERRSTRLEN*sizeof(char));

	win_scanf("Amplitude %f",&amplitude);
	for (i=0;i<n_points;i++)
	{
		/**(data+i)=amplitude*sin(2*M_PI_2*i/n_points);*/
		cbFromEngUnits(BoardNum,Range, amplitude*sin(2*M_PI_2*i/n_points), transfer_data);
		*(datavalue+i)=*transfer_data;
	/*	win_printf("data %d",*(datavalue+i));*/
	draw_bubble(screen,0,550,100,"datavalue %d",*(datavalue+i));
	}
	while(keypressed()!=TRUE)
	{
	     err=cbAOutScan (BoardNum,LowChan,HighChan,(long)n_points,&Rate,Range,datavalue,Options);
	}
	/*	cbGetStatus(BoardNum,Status,CurCount,CurIndex,AIFUNCTION);
	
	if((*Status!=IDLE)&&(clock()-start<(2*1000*(float)Count/(*Rate))));*/
	if (err!=0)
   	{
   		cbGetErrMsg( err, ErrMsg );
   		win_printf("Error %s", ErrMsg);
   		cbStopBackground(BoardNum, AOFUNCTION);
   	 	return 0;
	}
	
	
	/*cbStopBackground(BoardNum, AOFUNCTION);*/
	
	
	
	
	return 0;	
}

int stop_background_out(void)
{	int BoardNum=parameters.BoardNum;

	if(updating_menu_state != 0)	return D_O_K;

	cbStopBackground(BoardNum, AOFUNCTION);
	return 0;
}

int stop_background_in(void)
{	int BoardNum=parameters.BoardNum;

	if(updating_menu_state != 0)	return D_O_K;
 	cbStopBackground(BoardNum, AIFUNCTION);
	return 0;
}

#endif
