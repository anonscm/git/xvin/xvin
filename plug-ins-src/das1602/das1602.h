# ifndef DAS1602_H
# define DAS1602_H


# ifndef DAS1602_C
PXV_VAR(float , piezzo_pos);
#else
float piezzo_pos;
#endif

PXV_FUNC(int, out_flag,(void));
PXV_FUNC(int, init_das1602,(void));
PXV_FUNC(int, do_read_data,(void));
PXV_FUNC(int, do_set_out_data,(float data));
PXV_FUNC(int, do_get_in_data,(float *data));
PXV_FUNC(int, increment_pifoc,(void));
PXV_FUNC(int, decrement_pifoc,(void));


PXV_FUNC(int, do_sinusoide_out,(void));
/*PXV_FUNC(int, do_stop_read_data,(void));*/
PXV_FUNC(int, test,(void));
PXV_FUNC(int, test_stop,(void));
PXV_FUNC(int, stop_background_out,(void));
PXV_FUNC(int, stop_background_in,(void));

PXV_FUNC(int, set_Z_value, (float pos));
PXV_FUNC(float, read_Z_value, (void));





#endif
