/*************************************************************
                   STEMMER IMAGING GmbH                       
--------------------------------------------------------------

	Filename    : icvcPlugIn.h
	Date        : 31.05.200 15:32:13
	Description : 
	Created by  : Martin Kersting
	Revision    : 1.0

*************************************************************/
#ifndef PLUGIN_INCLUDE
  #define PLUGIN_INCLUDE

  // used by the bitmap and the fixbitmap plugins
  typedef struct
  {
    long  lDummy;
    LPSTR lpstrFilename;
    DWORD dwTransparent;
    long  hBitmap;
  } TBitmapPlugInData;

  // used by the rotatedrect plugin
  typedef struct
  {
    DWORD   dwWidth;
    DWORD   dwHeight;
    double  dRotation;				
  } TRotatedRectPlugInData;

  // used by the MultipleRotatedRect PlugIn 
#pragma pack (1)
  typedef struct
  {
    DWORD   dwNumObjects;
    long    pPosX;
    long    pPosY;
    DWORD   dwWidth;
    DWORD   dwHeight;
    double  dRotation;				
  } TMultipleRotatedRectPlugInData;
#pragma pack()

  // used by the statictextout plugin
  // valid flags
  #define F_NOMARKER  0
  #define F_MARKER    1
  // valid StringTypes
  #define ST_BSTR     0
  #define ST_LPSTR    1
  typedef struct
  {
    DWORD	dwFlags;
    LPSTR	lpstrText;
	  long	nStringType;
  } TStaticTextOutPlugInData;

  // used by the PolyLine plugin
  // pen styles
  #define PS_SOLID      0
  #define PS_DASH       1
  #define PS_DOT        2
  #define PS_DASHDOT    3
  #define PS_DASHDOTDOT 4

  // used by the PolyLine plugin
  typedef struct
  {
    long fnPenStyle;
	  long nPenWidth;
    long nNumVertices;
  } TPolyLinePlugInData;

  // used by the PixelList plugin
  typedef struct
  {
    PIXELLIST pixelList;
    long      fnPenStyle;
    long      nPenWidth;
  } TPixelListPlugInData;

  // used by the NamedCompass plugin
#pragma pack (1)
  typedef struct
  {
    long      nLength;
    double    dAlpha;
    long      nBaseRadius;
    long      nBaseCircleRadius;
    long      nPenWidth;
    long      nFontSize;
    BOOL      bTransparentText;      
    LPSTR     lpstrText;
    long      nStringType;
    BOOL      bFixToInitialLength;
    BOOL      bSnapBack;
    BOOL      bShowAngle;
  } TNamedCompassData;
#pragma pack ()

  // used by the Arc plugin
#pragma pack (1)
  typedef struct
  {
    double    nRadius;
    double    dStartAngle;
    double    dStopAngle;
    long      nLineWidth;
    BOOL      bShowHandles;
    int       nPenStyle;
  } TArcPlugInData;
#pragma pack ()

#endif	// PLUGIN_INCLUDE

