#ifndef _WLC_H_
#define _WLC_H_

PXV_FUNC(double, high_extension, (double alpha));
PXV_FUNC(double, small_extension, (double alpha));
PXV_FUNC(double,  extension_versus_alpha, (double alpha));
PXV_FUNC(double,  alpha_versus_extension, (double x));
PXV_FUNC(double,  marko_siggia_improved, ( double x));
PXV_FUNC(double,  marko_siggia_improved_derivate, ( double x));
PXV_FUNC(double,  extension_versus_kz_over_kx, (double r));
PXV_FUNC(float, find_best_L, (d_s *dsi, float T, float xi, float *E));
PXV_FUNC(float, find_best_xi, (d_s *dsi, float T, float xi, float L, float *E));
PXV_FUNC(int, do_fit_log_xi_auto, (void));
PXV_FUNC(int, do_fit_L, (void));
PXV_FUNC(int, do_fit_L_auto, (void));
PXV_FUNC(int, do_fit_log_xi, (void));
PXV_FUNC(int, do_fit_xi, (void));
PXV_FUNC(int, do_give_force, (void));
PXV_FUNC(int, do_give_extension, (void));
PXV_FUNC(int, test_div, (void));
PXV_FUNC(int, fjc_curve, (void));
PXV_FUNC(int, moroz_curve, (void));
PXV_FUNC(int, convert_tanh, (void));
PXV_FUNC(int, xi_effectif_vs_z, (void));
PXV_FUNC(int, xi_effectif_vs_f, (void));
PXV_FUNC(int, fit_kz_over_kx, (void));
PXV_FUNC(int, do_wlc_rescale_data_set, (void));
PXV_FUNC(int, do_wlc_rescale_plot, (void));



PXV_FUNC(int, xi_effectif_vs_f, (void));
PXV_FUNC(int, fit_kz_over_kx, (void));

PXV_FUNC(MENU*, wlc_plot_menu, (void));
PXV_FUNC(int, do_wlc_rescale_data_set, (void));
PXV_FUNC(int, wlc_main, (int argc, char **argv));
#endif
