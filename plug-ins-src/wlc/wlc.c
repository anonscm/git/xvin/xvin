/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _WLC_C_
#define _WLC_C_

# include "allegro.h"
# include "xvin.h"
# include "math.h"
# include "../../src/menus/plot/treat/p_treat_basic.h"
//# include "interpol.h"

//XV_FUNC(float, interpolate_point_by_poly_2_in_array, (float *x, float *y, int nx, float xp));

/* If you include other plug-ins header do it here*/ 
# include "../nrutil/nrutil.h"
# include "../xvplot/xvplot.h"

/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "wlc.h"


// marko siggia F = kBT/xi * ((z/L) - (1/4) + 1/(4*(1-z/L)*(1-z/L)))
// mFJC z = L0 (coth(F*2*xi/kBT) - (kBT/F*2xi))(1+F/S) 

typedef struct _point
{
	double	x;
	double	alpha;
}point;
typedef struct _hatFel
{
	double	kappa;
	double	ext;
}hatFel;
typedef struct _hatFwr
{
	double	kappa;
	double	wr;
}hatFwr;
/*
typedef struct _FullhatFel
{
  double alpha;
  double kappared;
  double ext;
};
typedef struct _FullhatFwr
{
  double alpha;
  double kappared;
  double wr;
}FullhatFwr;
*/

# include "wlchext1.h"
# include "wlchwr1.h"

point claude[] = {
{0, 0}, {0.0663429693860778, 0.1}, {0.1308070300784587, 0.2},
{0.1917945637040238, 0.3}, {0.2481769292167819, 0.4}, {0.2993475411318005, 0.5},
{0.3451604988688679, 0.6}, {0.3858083062788908, 0.7}, {0.421692164682175, 0.8},
{0.4533118215589949, 0.9}, {0.4811913700931646, 1}, {0.5058313207223902, 1.1},
{0.5276859752735726, 1.2}, {0.5471542060162626, 1.3}, {0.5645789445376268, 1.4},
{0.5802515771059838, 1.5}, {0.5944179782585552, 1.6}, {0.6072846651776055, 1.7},
{0.6190251673042787, 1.8}, {0.6297862848819241, 1.9}, {0.6396890639597461, 2},
{0.6488383527547905, 2.1}, {0.6573222730626851, 2.2}, {0.6652153404298557, 2.3},
{0.6725831594001269, 2.4}, {0.6794802866116449, 2.5}, {0.6859543859428811, 2.6},
{0.6920468373112659, 2.7}, {0.6977936318987086, 2.8}, {0.7032263878535905, 2.9},
{0.7083726906348174, 3}, {0.7132570540754512, 3.1}, {0.7179010892514152, 3.2},
{0.7223239350715171, 3.3}, {0.7265428229309871, 3.4}, {0.7305730491300351, 3.5},
{0.7344285230222948, 3.6}, {0.7381213185596907, 3.7}, {0.7416628163288346, 3.8},
{0.7450631318310252, 3.9}, {0.7483315055578017, 4}, {0.7514762932876148, 4.1},
{0.754505189861633, 4.2}, {0.7574251694986723, 4.3}, {0.7602426653142996, 4.4},
{0.7629635856574124, 4.5}, {0.7655933631584667, 4.6}, {0.7681369948274729, 4.7},
{0.7705991116412026, 4.8}, {0.7729840307861458, 4.9}, {0.7752957320447465, 5},
{0.7775378617227103, 5.1}, {0.779713880388767, 5.2}, {0.7818269501579583, 5.3},
{0.7838801408024195, 5.4}, {0.7858762237405698, 5.5}, {0.7878177834176864, 5.6},
{0.7897072819285773, 5.7}, {0.7915470209635629, 5.8}, {0.7933391719437615, 5.9},
{0.7950857105437994, 6}, {0.796788581339187, 6.1}, {0.7984495974920608, 6.2},
{0.800070364778259, 6.3}, {0.801652587766069, 6.4}, {0.803197721028237, 6.5},
{0.804707202722944, 6.6}, {0.806182369769301, 6.7}, {0.807624563314944, 6.8},
{0.809034964086006, 6.9}, {0.81041472090821, 7}, {0.811764931282341, 7.1},
{0.813086675061978, 7.2}, {0.814380876661942, 7.3}, {0.815648517725648, 7.4},
{0.816890507721307, 7.5}, {0.818107657746162, 7.6}, {0.819300850559891, 7.7},
{0.820470735123784, 7.8}, {0.821618260487843, 7.9}, {0.822744052377953, 8},
{0.823848688467555, 8.1}, {0.824932892469576, 8.2}, {0.825997293099563, 8.3},
{0.827042489520056, 8.4}, {0.828069000072157, 8.5}, {0.829077545265911, 8.6},
{0.830068462493787, 8.7}, {0.831042278550667, 8.8}, {0.831999552938841, 8.9},
{0.832940723837092, 9}, {0.833866299840179, 9.1}, {0.834776543908945, 9.2},
{0.835672002000899, 9.3}, {0.836553048528464, 9.4}, {0.83742013922938, 9.5},
{0.838273482153407, 9.6}, {0.839113512986235, 9.7}, {0.839940578168181, 9.8},
{0.840755026410412, 9.9}, {0.841557117203486, 10}};



point claude_h[] = {
{0, 0}, {0.4811913700931646, 1}, {0.6396890639597461, 2},
{0.7083726906348174, 3}, {0.7483315055578017, 4}, {0.7752957320447465, 5},
{0.7950857105437994, 6}, {0.81041472090821, 7}, {0.822744052377953, 8},
{0.832940723837092, 9}, {0.841558065721687,	10}, {0.848964543289949,	11},
{0.85542022138516,	12}, {0.861112664843849,	13}, {0.866181381332273,	14},
{0.870732584479857,	15}, {0.874848768442312,	16}, {0.878595114303152,	17},
{0.882023892852423,	18}, {0.885177588671249,	19}, {0.888091128058148,	20},
{0.890793525215664,	21}, {0.893309113232524,	22}, {0.895658474915025,	23},
{0.897859145703911,	24}, {0.899926179179626,	25}, {0.901872576594166,	26},
{0.90370963374047,	27}, {0.905447223404618,	28}, {0.907094006038223,	29},
{0.908657628779858,	30}, {0.910144862569365,	31}, {0.911561732374397,	32},
{0.912913616919618,	33}, {0.914205336930698,	34}, {0.915441228306273,	35},
{0.916625199126635,	36}, {0.917760787028513,	37}, {0.918851198173183,	38},
{0.919899352504625,	39}, {0.920907910750623,	40}, {0.92187930452085,	41},
{0.9228157624494,	42}, {0.923719328908648,	43}, {0.924591885508625,	44},
{0.925435166467693,	45}, {0.926250774309051,	46}, {0.927040187630446,	47},
{0.927804781668885,	48}, {0.928545829310563,	49}, {0.929264514993216,	50},
{0.929961941702463,	51}, {0.930639136667056,	52}, {0.93129706079012,	53},
{0.931936608479175,	54}, {0.932558622022625,	55}, {0.933163887262106,	56},
{0.933753142929711,	57}, {0.934327083386497,	58}, {0.934886358424006,	59},
{0.935431584086461, 60}, {0.935963340004499, 61}, {0.93648216876797, 62},
{0.936988586961888, 63}, {0.937483083014935, 64}, {0.937966114827123, 65},
{0.938438121634431, 66}, {0.938899514642887, 67}, {0.939350686666172, 68},
{0.93979200900208, 69}, {0.940223834864231, 70}, {0.940646500611919, 71},
{0.941060325700487, 72}, {0.941465613098723, 73}, {0.941862652519759, 74},
{0.942251721841535, 75}, {0.942633082322975, 76}, {0.943006985025276, 77},
{0.943373670653585, 78}, {0.943733368704908, 79}, {0.944086297716478, 80},
{0.944432667101678, 81}, {0.944772679248265, 82}, {0.945106524401106, 83},
{0.945434387361479, 84}, {0.945756444851968, 85}, {0.946072866615783, 86},
{0.946383815051094, 87}, {0.94668944480472, 88}, {0.946989907474756, 89},
{0.947285345970282, 90}, {0.947575898558776, 91}, {0.947861699691504, 92},
{0.94814287681438, 93}, {0.948419552863888, 94}, {0.948691847854887, 95},
{0.948959874964978, 96}, {0.949223744869408, 97}, {0.949483563676061, 98},
{0.949739435058469, 99}, {0.949991456516568, 100}, {0.95023972526339, 101},
{0.950484331779316, 102}, {0.950725367017832, 103}, {0.9509629151231, 104},
{0.951197061412979, 105}, {0.951427883959155, 106}, {0.951655463164298, 107},
{0.951879872689811, 108}, {0.952101185799208, 109}, {0.95231947322551, 110},
{0.952534803095428, 111}, {0.952746,         112}, {0.952956852535672, 113},
{0.953163699045002, 114}, {0.953367840306219, 115}, {0.953569336362088, 116},
{0.953768241457309, 117}, {0.953964612123577, 118}, {0.954158502591343, 119},
{0.954349962278896, 120}, {0.954539043399677, 121}, {0.954725794397079, 122},
{0.954910262323132, 123}, {0.955092493972543, 124}, {0.955272533220727, 125},
{0.955450424957586, 126}, {0.955626210308306, 127}, {0.955799931239753, 128},
{0.955971627844592, 129}, {0.95614133868816, 130}, {0.956309101889304, 131},
{0.956474954893582, 132}, {0.95663893318922, 133}, {0.956801071964549, 134},
{0.956961405643498, 135}, {0.957119966240835, 136}, {0.957276787983927, 137},
{0.957431901358775, 138}, {0.957585337332522, 139}, {0.957737125827262, 140},
{0.957887296486686, 141}, {0.958035877528312, 142}, {0.958182896991717, 143},
{0.958328381903158, 144}, {0.958472358639509, 145}, {0.958614853693623, 146},
{0.958755891054719, 147}, {0.958895497316707, 148}, {0.959033694390038, 149},
{0.95917050713782, 150}, {0.959305958360557, 151}, {0.959440070455099, 152},
{0.959572865431031, 153}, {0.959704364079313, 154}, {0.959834588218889, 155},
{0.959963557572258, 156}, {0.960091292483067, 157}, {0.9602178126241, 158},
{0.960343137310172, 159}, {0.960467284395357, 160}, {0.960590272978181, 161},
{0.960712120864436, 162}, {0.960833819838433, 163}, {0.960952463999449, 164},
{0.961070992944627, 165}, {0.961188449266791, 166}, {0.961304848850994, 167},
{0.961420207103151, 168}, {0.961534539717395, 169}, {0.961647861657715, 170},
{0.961760188130274, 171}, {0.961871533622498, 172}, {0.961981911711437, 173},
{0.962091336977932, 174}, {0.96219982239981, 175}, {0.962307381900861, 176},
{0.962414028393313, 177}, {0.962519775085165, 178}, {0.962624623362707, 179},
{0.962728606315481, 180}, {0.96283172702715, 181}, {0.962933996160703, 182},
{0.963035425657193, 183}, {0.963136026178725, 184}, {0.963235810625269, 185},
{0.963334789843792, 186}, {0.963432984075986, 187}, {0.963530382946636, 188},
{0.963627007740634, 189}, {0.963722868465507, 190}, {0.963817974578478, 191},
{0.963912337462802, 192}, {0.964005965422328, 193}, {0.964098868313401, 194},
{0.96419105555731, 195}, {0.964282536805928, 196}, {0.964373319834497, 197},
{0.964463414400516, 198}, {0.964552828783621, 199}, {0.964641571870021, 200}};

# include "matrice.h"

int fit_double_arrays_to_xn_polynome(double *xd, double*yd, int array_size, int n, double **a)
{
	register int i,j,k;
	int npt, id, nx = 0, np;
	int status, ret;
	double *m = NULL, *l = NULL, *b = NULL, *x = NULL, ax, ay, x1;
	
	if (xd == NULL || xd == NULL)		return -1;
	npt = n*(n+1)/2;
	
	m = (double *) calloc(npt, sizeof(double));
	l = (double *) calloc(npt, sizeof(double));
	b = (double *) calloc(n, sizeof(double));
	x = (double *) calloc(n, sizeof(double));
	
	for (k=0, i=0, j=0 ; k < npt ; k++)
	{
		for (id = nx = 0, m[k] = 0; id < array_size; id++)
		{
			ax = xd[id];
			ay = yd[id];
			x1 = 1;
			for (np = 0; np < (j + i); np++)
			  x1 *= ax;		
			m[k] += x1;
		}
		j++;
		if (j==n) j=++i;
	}
	for (i=0 ; i<n ; i++)
	{
		for (id = nx = 0, b[i] = 0; id < array_size; id++)
		{
			ax = xd[id];
			x1 = ay = yd[id];
			for (np = 0; np < i; np++)
			  x1 *= ax;
			nx++;		
			b[i] += x1;
		}
	}	
	ret = (nx < n) ? -2 : nx;	
	status = sym_matrix_to_triang(n, m, l);
	ret = (status ==  ERR_NEGATIVE_SQRT) ? -3 : nx;
	sym_matrix_solve(n, l, b, x);
	ret = (status ==  NULL_DENOMINATOR) ? -3 : nx;
	sym_matrix_improv(n, l, b, x);
	ret = (status ==  NULL_DENOMINATOR) ? -3 : nx;
	*a = x;
	if (ret <0)	free(x);	
	free(l);
	free(m);
	free(b);	
	return ret;
}

double compute_writhe_renormalize_torsion(double A, double C, double alpha, int *error)
{
  double Cs;

  if (error) *error = 0;
  if (alpha <= 0 || A <= 0)
    {
      if (error) *error = 1;
      return 0;
    }
  alpha = (double)1/alpha;
  Cs = sqrt(alpha);
  Cs *= C/(4*A);
  Cs = (double)1 - Cs;
  Cs *= C;
  return Cs;
}

double compute_energy_order_1(double A, double f, double alpha, int *error)
{
  double g;

  if (error) *error = 0;
  if (alpha < 0 || A <= 0)
    {
      if (error) *error = 1;
      return 0;
    }
  g = sqrt((double)1/alpha);
  g = (double)1 - g;
  g *= f;
  return g;
}

double compute_energy_wlc(double A, double f, double alpha, int *error)
{
  double g, alphal, prev, fl, tmp, fl_1;
  int na, i;

  if (error) *error = 0;
  if (alpha < 0 || A <= 0)
    {
      if (error) *error = 1;
      return 0;
    }
  if (alpha < 0.1)
    return (f * extension_versus_alpha(alpha))/4;
  na = (int)(alpha*10);

  for(g = prev = fl_1 = 0, i = 1; i <= na; i++)
    {  
      fl = (f*i)/na;
      alphal = (alpha*i)/na;
      tmp = extension_versus_alpha(alphal);
      g += (fl - fl_1) * (tmp + prev)/2;
      prev = tmp;
      fl_1 = fl;
    }
  return g;
}





double give_extension_order_1(double f, double A, double kBT, int *error)
{
  double x;

  (void)error;
  if (A*f == 0) return 0;
  x = kBT/(A*f);
  x = sqrt(x);
  x = 1 - x/2;
  return x;
}

double give_extension_wlc(double f, double A, double kBT, int *error)
{
  double x;

  (void)error;
  x = (A*f)/kBT;
  return extension_versus_alpha(x);
}


double give_extension_order_2(double f, double A, double kBT, int *error)
{
  double x;

  (void)error;
  if (A*f == 0) return 0;
  x = (A*f)/kBT;
  x += 0.25;
  if (x <= 0) return 0;
  x = sqrt((float)1/x);
  x = 1 - x/2;
  return x;
}

double give_extension_order_1_vs_sigma(double f, double A, double C, double kBT, double sigma, int *error)
{
  double alpha_1, x;
  double omega_0;

  (void)error;
  omega_0 = 2*M_PI/3.6;

  if (A*f == 0) return 0;
  alpha_1 = kBT/(A*f);
  alpha_1 = sqrt(alpha_1);
  x = 1 - alpha_1/2;
  x -= C * C * sigma * sigma * kBT * omega_0*omega_0 * alpha_1 / (A * 16 * f);
  return x;
}

double give_extension_wlc_vs_sigma(double f, double A, double C, double kBT, double sigma, int *error)
{
  double alpha_1, x;
  double omega_0;

  (void)error;
  omega_0 = 2*M_PI/3.6;

  x = (A*f)/kBT;
  x = extension_versus_alpha(x);
  if (A*f == 0) return 0;
  alpha_1 = kBT/(A*f);
  alpha_1 = sqrt(alpha_1);
  x -= C * C * sigma * sigma * kBT * omega_0*omega_0 * alpha_1 / (A * 16 * f);
  return x;
}


double compute_sigma_s(double P, double g, double Cs, double kBT, int *error)
{
  double sigma;
  double omega_0;

  omega_0 = 2*M_PI/3.6;
  Cs *= kBT*omega_0*omega_0;
  P  *= kBT*omega_0*omega_0;
  if (error) *error = 0;
  if (Cs <= 0)
    {
      if (error) *error = 1;
      return 0;
    }  
  Cs = (double)1/Cs;
  sigma = P*Cs;
  sigma = (double)1 - sigma;
  if (sigma <= 0)
    {
      if (error) *error = 1;
      return 0;
    }  
  sigma = (double)1/sigma;  
  sigma *= 2*g*P;
  sigma = sqrt(sigma);
  sigma *= Cs;
  return sigma;
}
double compute_sigma_p(double P, double g, double Cs,  double kBT, int *error)
{
  double sigma;
  double omega_0;

  omega_0 = 2*M_PI/3.6;

  if (error) *error = 0;
  if (Cs <= 0)
    {
      if (error) *error = 1;
      return 0;
    }
  Cs *= kBT*omega_0*omega_0;
  P  *= kBT*omega_0*omega_0;
  Cs = (double)1/Cs;
  sigma = P*Cs;
  sigma = (double)1 - sigma;
  if (sigma <= 0)
    {
      if (error) *error = 1;
      return 0;
    }  
  sigma = (double)1/sigma;  
  sigma *= 2*g*P;
  sigma = sqrt(sigma);
  if (P <= 0)
    {
      if (error) *error = 1;
      return 0;
    }  
  sigma /= P;
  return sigma;
}

double torque_no_plecto(double C, double A, double kBT, double sigma, double f)
{
  double Cs, alpha, omega_0, tau;
  int error;

  alpha = f*A;
  alpha /= kBT;
  omega_0 = 2*M_PI/3.6;
  Cs = compute_writhe_renormalize_torsion(A, C, alpha, &error);
  tau = kBT*Cs*omega_0*sigma;
  return tau;
}
double torque_plecto(double P, double kBT, double sigma)
{
  double omega_0, tau;

  omega_0 = 2*M_PI/3.6;
  tau = kBT*P*omega_0*sigma;
  return tau;
}

int	draw_moroz_hat_correction(void)
{
  register int i;
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *ds = NULL;
  static int nf = 1024;
  static float T = 298;
  static float A = 50, C = 85, fm = 5;	/* nm*/
  //  static float L = 16.2;	/* microns */
  float  alpha, kBT;
  double  tmp, f = 0;

	
  if(updating_menu_state != 0)	return D_O_K;	
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)	
    return win_printf_OK("cannot find data");

  i = win_scanf("Draw Moroz torque correction\n"
		"Bending pesistence length (nm) %6f\n"
		"Torsionnal persistence length (nm) %6f\n"
		"Force max (pN) %6f\n"
		"Temperature value (Kelvin) %6f\n"
		"number of points %6d\n"
		,&A,&C,&fm,&T,&nf);
  if (i == WIN_CANCEL)	return OFF;
  op = create_and_attach_one_plot(pr, nf, nf, 0);
  if (op == NULL)    return win_printf("cannot create plot !");
  ds = op->dat[0];

  kBT = 1.38062e-2 * T; // pN . nm

  for (i = 0; i < nf; i++)
    {
      f = (fm * (i+1))/nf;
      alpha = A*f/kBT;
      ds->xd[i] = f;
      if (i == 0) win_printf("f = %f",f);
      tmp = (double)1/sqrt(alpha);
      if (i == 0) win_printf("tmp = %f",tmp);
      tmp *= C/(4*A);
      tmp = (double)1 - tmp;
      ds->yd[i] = tmp;
    }

  //set_plot_title(op, "\\stack{{Exact WLC model}{- 0.25*(1-x)^{-2}+(1/4) -x}}");
  set_plot_y_title(op, "Moroz correction");
  set_plot_x_title(op, "Force ");

  set_ds_source(ds, "Moroz correction hat A = %g C = %g fm = %g T = %g ",A,C,f,T);

  return refresh_plot(pr,pr->cur_op);			
}

int	fit_marko_type_hat(void)
{
  register int i, j, k;
  char *ctmp = NULL;
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *ds = NULL, *ds2 = NULL, *ds3 = NULL, *ds4 = NULL, *ds5 = NULL;
  static int n_min = 6;
  static float T = 298;
  static float A = 50, C = 85, f= 0.5, g = 0;	/* nm*/
  static float sigmad = -.05;	/* microns */
  static float ex = 0.85;
  static int cont =1;
  int initdat = 0;
  int run = 0;
  //float sigma, sigmas, sigmap, Cs, xm, P = 24, xf;
  float alpha, kBT, xp, xs, l0, max, xmin1, xmin2;
  float sigmainter = 0, delta, leff, center, P = 0, Csb = 0, Csbinfty = 0, delta2;
  int  nx, error;
  double omega_0;
  double *a, hat_slope = 0, line_b = 0;
  FILE *filehats;
  char 	fullfilename[512] = {0};
  omega_0 = 2*M_PI/3.6;
	
	
  if(updating_menu_state != 0)
    {
      add_keyboard_short_cut(0, KEY_K, 0, fit_marko_type_hat);
      return D_O_K;	
    }
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)	
    return win_printf_OK("cannot find data");
  run = op->cur_dat;
  win_printf("found %d datasets",initdat = op->n_dat);
  while(cont){
    ds=op->dat[run];
    ctmp = strstr(ds->source,"force= ");
    if (ctmp != NULL)
      sscanf(ctmp,"force = %g",&f);
    ctmp = strstr(ds->source,"A=");
    if (ctmp != NULL)
      sscanf(ctmp,"A=%g",&A);
  i = win_scanf("Fit Marko like hat curve\n Bending pesistence length (nm) %6f\n"
		"Force (pN) %6f\n"
		"fit extend %6f\n"
		"minimum number of points to fit hat top %6d\n"
		"Temperature value (Kelvin) %6f\n"
		"minimum \\sigma  value before denaturation %6f\n,continue %R No%r Yes",&A,&f,&ex,&n_min,&T,&sigmad,&cont);
  if (i == WIN_CANCEL) 	return OFF;

  kBT = 1.38062e-2 * T; // pN . nm
  alpha = A*f/kBT;
  l0 = extension_versus_alpha(alpha);
  xs = (l0/2)*(1 + ex);
  xp = (l0/2)*(1 - ex);

  // we find the nb of point in the linear part
  for (i = 0, j = 0; i < ds->nx; i++)
    {
      if ((ds->xd[i] > 0) && (ds->yd[i] >= xp) && (ds->yd[i] <= xs)) j++;
    }
  if (j > 1)
    {
      ds2 = create_and_attach_one_ds(op, j, j, 0);
      if (ds2 == NULL)    return win_printf("cannot create plot !");
      set_plot_symb(ds2,"\\pt5\\oc ");
      set_ds_dot_line(ds2);
      
      for (i = 0, j = 0; i < ds->nx && j < ds2->nx; i++)
	{
	  if ((ds->xd[i] > 0) && (ds->yd[i] >= xp) && (ds->yd[i] <= xs))
	    {
	      ds2->xd[j] = ds->xd[i];
	      ds2->yd[j] = ds->yd[i];
	      j++;
	    }
	}
      ds2->nx = ds2->ny = j;
      nx = fit_ds_to_xn_polynome(ds2, 2, (float)-1, (float)1, (float)0, (float)1, &a);
      if (nx < 0)		return win_printf_OK("something went wrong!");
  //win_printf("nx %d a0 = %g a1 = %g",nx,a[0],a[1]);
      line_b = a[0];
      hat_slope = a[1];
      if (hat_slope == 0) return win_printf_OK("Hat slope is null!");
      xs = (l0 - line_b)/hat_slope;
      xp = -line_b/hat_slope;
      for (i = 0, max = 0; i < ds->nx; i++)
	{
	  if ((fabs(ds->xd[i]) <= fabs(sigmad)) && (ds->yd[i] > max))
	    max = ds->yd[i];
	}
      
      xs = (max - line_b)/hat_slope;
      xp = -line_b/hat_slope;
      // we create a line 
      ds3 = create_and_attach_one_ds(op, 2, 2, 0);
      if (ds3 == NULL)    return win_printf("cannot create plot !");
      ds3->xd[0] = xs;
      ds3->yd[0] = max;
      ds3->xd[1] = xp;
      ds3->yd[1] = 0;
      
      //win_printf("xs = %g xp = %g",xs,xp);
      free(a);

    }
  else
    {
      xs = -sigmad;
    }
  xmin2 = xs;
  for (i = 0, j = 0; i < ds->nx; i++)
    {
      if ((fabs(ds->xd[i]) < xs)) j++;
    }
  if (j < n_min)
    {
      win_printf("Xe did not found enough points %d with |x| < %g",j,xs); 
      for (i = j = 0, xmin1 = 10; i < ds->nx; i++)
	  if ((fabs(ds->xd[i]) < xmin1) && (ds->xd[i] > sigmad)) 
	    xmin1 = fabs(ds->xd[j = i]);
      //win_printf("1 point %d x %g y %g",j,ds->xd[j],ds->yd[j]);

      for (j = 0 ;j < n_min; )
	{
	  for (i = k = 0, xmin1 = xmin2, xmin2 = 10; i < ds->nx; i++)
	    if ((fabs(ds->xd[i]) <= xmin2) && (fabs(ds->xd[i]) > xmin1) && (ds->xd[i] > sigmad)) 
	      xmin2 = fabs(ds->xd[k = i]);

	  for (i = 0, j = 0; i < ds->nx; i++)
	    {
	      if ((fabs(ds->xd[i]) <= xmin2) && (ds->xd[i] > sigmad))
		{
		  j++;
		}
	    }
	  win_printf("%d points at %d x %g y %g",j,k,ds->xd[k],ds->yd[k]);
	}
    }


  ds4 = create_and_attach_one_ds(op, j, j, 0);
  if (ds4 == NULL)    return win_printf("cannot create plot !");
  set_plot_symb(ds4,"\\pt5\\sq ");
  set_ds_dot_line(ds4);

  for (i = 0, j = 0; i < ds->nx && j < ds4->nx; i++)
    {
      if ((fabs(ds->xd[i]) <= xmin2) && (ds->xd[i] > sigmad))
	{
	  ds4->xd[j] = ds->xd[i];
	  ds4->yd[j] = ds->yd[i];
	  //win_printf("selected point %d x %g y %g",j,ds->xd[i],ds->yd[i]);
	  j++;
	}
    }
  ds4->nx = ds4->ny = j;
  win_printf("%d points in hat top",j);
  nx = fit_ds_to_xn_polynome(ds4, 3, (float)-1, (float)1, (float)0, (float)1, &a);
  if (nx < 0)		return win_printf_OK("something went wrong!");

  ds5 = create_and_attach_one_ds(op, 512, 512, 0);
  if (ds5 == NULL)    return win_printf("cannot create plot !");

  for (i = 0; i < ds5->nx; i++)
    {
      ds5->xd[i] =  (2*i - ds5->nx) * (1.5 * xs)/ds5->nx; 
      ds5->yd[i] = a[0] + a[1] * ds5->xd[i] + a[2] * ds5->xd[i] * ds5->xd[i];
    }

  if (hat_slope != 0)
    {

      delta = (a[1] - hat_slope) * (a[1] - hat_slope) + 4 * (line_b - a[0]) * a[2];
      if (delta < 0)   return win_printf_OK("negative delta!");
      delta = sqrt(delta);
      if (a[2] == 0)   return win_printf_OK("zero curvature!");
      sigmainter = (hat_slope + delta - a[1])/(2*a[2]);
      ds3->xd[0] = sigmainter;
      ds3->yd[0] = line_b + hat_slope * sigmainter;
      
      set_plot_symb(ds3,"\\pt6\\oc ");

    }
  center = -a[1] / (2 * a[2]);


  C = ((-a[2]) * 16 * alpha * sqrt(alpha)) / (omega_0 * omega_0);
  C = sqrt(C);

  g = compute_energy_wlc( A, f,  alpha, &error);

  Csb = sigmainter*(xp - sigmainter);
  if (Csb != 0)    Csb = 2*g/Csb;
  Csb /= (kBT*omega_0*omega_0);
  if (xp != 0)     P = Csb * sigmainter / xp;

  delta2 = 1 - Csb/(A*sqrt(alpha));
  if (delta2 > 0)
    Csbinfty = (A*sqrt(alpha)*2)*(1-sqrt(delta2));

  leff = a[0] + a[1] * center + a[2] * center * center;
  set_ds_source(ds5, "Fit Marko type hat\ncenter %g l0 %g leff %g force %g\n"
		"curvature %g nb. of pts at top %d sigmas %g sigmap %g slope %g C %g A %g alpha %g\nCsb = %g P = %g Cb = %g",
		center,l0,leff,f,a[2],j,sigmainter,xp,hat_slope,C,A,alpha,Csb,P,Csbinfty);

  win_printf("Fit Marko type hat\ncenter %g l0 %g leff %g force %g\n"
		"curvature %g nb. of pts at top %d sigmas %g sigmap %g slope %g C %g A %g alpha %g\nCsb = %g P = %g Cb = %g",
		center,l0,leff,f,a[2],j,sigmainter,xp,hat_slope,C,A,alpha,Csb,P,Csbinfty);

  sprintf(fullfilename,"%s%s_treat.xv",op->dir,op->filename);

  filehats = fopen(fullfilename,"a+");
  fprintf(filehats,"%g \t %g \t %g \t %g \t %g \t %d \t %g \t %g \t %g \t %g \t %g \t %g \t %g \t %g \t %g\n",center,l0,leff,f,a[2],j,sigmainter,xp,hat_slope,C,A,alpha,Csb,P,Csbinfty);
  fclose(filehats);
  //win_printf("sigmainter = %g",sigmainter);


  //win_printf("nx %d a0 = %g a1 = %g",nx,a[0],a[1]);

  refresh_plot(pr,pr->cur_op);
  run++;
  if(run==initdat)
    run=0;
  }

  return refresh_plot(pr,pr->cur_op);			
}


int	draw_marko_hat(void)
{
  register int i;
  //  char st[256];
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *ds = NULL;
  static int nf = 1024;
  static float T = 298;
  static float A = 50, C = 85, P = 24, sigmam = 0.015, f= 0.5;	/* nm*/
  static float L = 16.2;	/* microns */
  float sigma, sigmas, sigmap, alpha, g, xf, kBT, Cs, xp;
  int error;
  //double omega_0 = 2*M_PI/3.6;
	
	
  if(updating_menu_state != 0)	return D_O_K;	
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	
    return win_printf_OK("cannot find data");
  i = win_scanf("Draw Marko hat curve\n Bending pesistence length (nm) %6f\n"
		"Torsionnal persistence length (nm) %6f\n"
		"Plectoneme persistence length (nm) %6f\n"
		"Force (pN) %6f\n"
		"Length of molecule (\\mu m)%6f\n"
		"Temperature value (Kelvin) %6f\n"
		"number of points %6d\n"
		"\\sigma_{max} %6f\n",&A,&C,&P,&f,&L,&T,&nf,&sigmam);
  if (i == WIN_CANCEL)	return OFF;
  ds = create_and_attach_one_ds(op, 2*nf+1, 2*nf+1, 0);
  if (ds == NULL)    return win_printf("cannot create plot !");
  kBT = 1.38062e-2 * T; // pN . nm
  alpha = A*f/kBT;
  Cs = compute_writhe_renormalize_torsion(A, C, alpha, &error);
  g = compute_energy_order_1( A, f,  alpha, &error);
  sigmas = compute_sigma_s(P, g, Cs, kBT, &error);
  sigmap = compute_sigma_p(P, g, Cs, kBT, &error);
  ds->yd[nf] = xf = give_extension_order_1( f, A,  kBT, &error);
  ds->xd[nf] = 0;
  xf = give_extension_order_1_vs_sigma( f, A, C, kBT, sigmas, &error);
  for (i = 0; i < nf; i++)
    {
      sigma = (i+1)*sigmam/nf;
      if (sigma < sigmas)
	{
	  ds->yd[nf+i+1] = give_extension_order_1_vs_sigma( f, A, C, kBT, sigma, &error);
	  ds->yd[nf-i-1] = ds->yd[nf+i+1];
	  ds->xd[nf-i-1] = -sigma;
	  ds->xd[nf+i+1] = sigma;
	}
      else if (sigma > sigmap)
	{
	  ds->yd[nf-i-1] = ds->yd[nf+i+1] = 0;
	  ds->xd[nf-i-1] = -sigma;
	  ds->xd[nf+i+1] = sigma;
	}
      else
	{
	  xp = (sigma - sigmas)/(sigmap - sigmas);
	  ds->yd[nf-i-1] = ds->yd[nf+i+1] = xf * (1-xp);
	  ds->xd[nf-i-1] = -sigma;
	  ds->xd[nf+i+1] = sigma;
	}
    }
  set_ds_source(ds, "Marko hat A = %g C = %g P = %g f = %g T = %g sigma_s %g sigma_p %g Cs %g",A,C,P,f,T,sigmas,sigmap,Cs);

  return refresh_plot(pr,pr->cur_op);			
  //return win_printf_OK("Cs = %g g = %g \n\\sigma_s = %g \\sigma_p = %g xf = %g",Cs,g,sigmas,sigmap,xf);
}

int	draw_wlc_model(void)
{
  register int i;
  //  char st[256];
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *ds = NULL;
  static int nf = 1024;
  static float T = 298;
  static float A = 50, fm= 0.05, fM = 50;	/* nm*/
  static float L = 16.2;	/* microns */
  float f, kBT;
  int error;
  //double omega_0 = 2*M_PI/3.6;
	
	
  if(updating_menu_state != 0)	return D_O_K;	
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)	
    return win_printf_OK("cannot find data");
  i = win_scanf("Draw wlc curve\n"
		"Persistence length %6f (nm)\n"
		"Length of molecule %6f (\\mu m)\n"
		"Temperature value (Kelvin) %6f\n"
		"number of points %6d\n"
		"f_{min} = %5f f_{max} = %5f (pN)\n"
		,&A,&L,&T,&nf,&fm,&fM);
  if (i == WIN_CANCEL)	return OFF;
  op = create_and_attach_one_plot(pr, nf, nf, 0);
  if (op == NULL)    return win_printf("cannot create plot !");
  ds = op->dat[0];
  kBT = 1.38062e-2 * T; // pN . nm
  for (i = 0; i < nf; i++)
    {
      f = fm + ((fM - fm)*i)/nf;
      ds->xd[i] = L* give_extension_wlc( f, A,  kBT, &error);
      ds->yd[i] = f;
    }
  set_ds_plot_label(ds, L/2,fM/2, USR_COORD,"\\center{\\pt8\\stack{{WLC model}"
		    "{A = %g nm}{L = %g \\mu m}}}", A, L);  
  set_ds_source(ds, "WLC model A = %g L = %g T = %g ",A,L,T);
  return refresh_plot(pr,pr->cur_op);			
}


int	draw_marko_hat_improved(void)
{
  register int i;
  //  char st[256];
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *ds = NULL;
  static int nf = 1024;
  static float T = 298;
  static float A = 50, C = 85, P = 24, sigmam = 0.1, f= 0.5;	/* nm*/
  static float L = 16.2;	/* microns */
  float sigma, sigmas, sigmap, alpha, g, xf, kBT, Cs, xp;
  int error;
  //double omega_0 = 2*M_PI/3.6;
	
	
  if(updating_menu_state != 0)	return D_O_K;	
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	
    return win_printf_OK("cannot find data");
  i = win_scanf("Draw Marko hat improved curve\n"
		"Bending pesistence length (nm) %6f\n"
		"Torsionnal persistence length (nm) %6f\n"
		"Plectoneme persistence length (nm) %6f\n"
		"Force (pN) %6f\n"
		"Length of molecule (\\mu m)%6f\n"
		"Temperature value (Kelvin) %6f\n"
		"number of points %6d\n"
		"\\sigma_{max} %6f\n",&A,&C,&P,&f,&L,&T,&nf,&sigmam);
  if (i == WIN_CANCEL)	return OFF;
  ds = create_and_attach_one_ds(op, 2*nf+1, 2*nf+1, 0);
  if (ds == NULL)    return win_printf("cannot create plot !");
  kBT = 1.38062e-2 * T; // pN . nm
  alpha = A*f/kBT;
  Cs = compute_writhe_renormalize_torsion(A, C, alpha, &error);
  g = compute_energy_wlc( A, f,  alpha, &error);
  sigmas = compute_sigma_s(P, g, Cs, kBT, &error);
  sigmap = compute_sigma_p(P, g, Cs, kBT, &error);
  ds->yd[nf] = xf = give_extension_wlc( f, A,  kBT, &error);
  ds->xd[nf] = 0;
  xf = give_extension_wlc_vs_sigma( f, A, C, kBT, sigmas, &error);
  for (i = 0; i < nf; i++)
    {
      sigma = (i+1)*sigmam/nf;
      if (sigma < sigmas)
	{
	  ds->yd[nf+i+1] = give_extension_wlc_vs_sigma( f, A, C, kBT, sigma, &error);
	  ds->yd[nf-i-1] = ds->yd[nf+i+1];
	  ds->xd[nf-i-1] = -sigma;
	  ds->xd[nf+i+1] = sigma;
	}
      else if (sigma > sigmap)
	{
	  ds->yd[nf-i-1] = ds->yd[nf+i+1] = 0;
	  ds->xd[nf-i-1] = -sigma;
	  ds->xd[nf+i+1] = sigma;
	}
      else
	{
	  xp = (sigma - sigmas)/(sigmap - sigmas);
	  ds->yd[nf-i-1] = ds->yd[nf+i+1] = xf * (1-xp);
	  ds->xd[nf-i-1] = -sigma;
	  ds->xd[nf+i+1] = sigma;
	}
    }
  set_ds_source(ds, "Marko hat  improved A = %g C = %g P = %g f = %g T = %g sigma_s %g sigma_p %g Cs %g",A,C,P,f,T,sigmas,sigmap,Cs);
  return refresh_plot(pr,pr->cur_op);			
  //return win_printf_OK("Cs = %g g = %g \n\\sigma_s = %g \\sigma_p = %g xf = %g",Cs,g,sigmas,sigmap,xf);
}

int	draw_marko_hat_improved_no_Cs(void)
{
  register int i;
  //  char st[256];
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *ds = NULL;
  static int nf = 1024;
  static float T = 298;
  static float A = 50, C = 85, P = 24, sigmam = 0.1, f= 0.5;	/* nm*/
  static float L = 16.2;	/* microns */
  float sigma, sigmas, sigmap, alpha, g, xf, kBT, Cs, xp;
  int error;
  //double omega_0 = 2*M_PI/3.6;
	
	
  if(updating_menu_state != 0)	return D_O_K;	
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	
    return win_printf_OK("cannot find data");
  i = win_scanf("Draw Marko hat improved without Nelson correction curve\n"
		"Bending pesistence length (nm) %6f\n"
		"Torsionnal persistence length (nm) %6f\n"
		"Plectoneme persistence length (nm) %6f\n"
		"Force (pN) %6f\n"
		"Length of molecule (\\mu m)%6f\n"
		"Temperature value (Kelvin) %6f\n"
		"number of points %6d\n"
		"\\sigma_{max} %6f\n",&A,&C,&P,&f,&L,&T,&nf,&sigmam);
  if (i == WIN_CANCEL)	return OFF;
  ds = create_and_attach_one_ds(op, 2*nf+1, 2*nf+1, 0);
  if (ds == NULL)    return win_printf("cannot create plot !");
  kBT = 1.38062e-2 * T; // pN . nm
  alpha = A*f/kBT;
  Cs = C;//compute_writhe_renormalize_torsion(A, C, alpha, &error);
  g = compute_energy_wlc( A, f,  alpha, &error);
  sigmas = compute_sigma_s(P, g, Cs, kBT, &error);
  sigmap = compute_sigma_p(P, g, Cs, kBT, &error);
  ds->yd[nf] = xf = give_extension_wlc( f, A,  kBT, &error);
  ds->xd[nf] = 0;
  //xf = give_extension_wlc_vs_sigma( f, A, C, kBT, sigmas, &error);
  for (i = 0; i < nf; i++)
    {
      sigma = (i+1)*sigmam/nf;
      if (sigma < sigmas)
	{
	  ds->yd[nf+i+1] = xf;//give_extension_wlc_vs_sigma( f, A, C, kBT, sigma, &error);
	  ds->yd[nf-i-1] = ds->yd[nf+i+1];
	  ds->xd[nf-i-1] = -sigma;
	  ds->xd[nf+i+1] = sigma;
	}
      else if (sigma > sigmap)
	{
	  ds->yd[nf-i-1] = ds->yd[nf+i+1] = 0;
	  ds->xd[nf-i-1] = -sigma;
	  ds->xd[nf+i+1] = sigma;
	}
      else
	{
	  xp = (sigma - sigmas)/(sigmap - sigmas);
	  ds->yd[nf-i-1] = ds->yd[nf+i+1] = xf * (1-xp);
	  ds->xd[nf-i-1] = -sigma;
	  ds->xd[nf+i+1] = sigma;
	}
    }
  set_ds_source(ds, "Marko hat improved no Nelson correction A = %g C = %g P = %g f = %g T = %g sigma_s %g sigma_p %g Cs %g",A,C,P,f,T,sigmas,sigmap,Cs);
  return refresh_plot(pr,pr->cur_op);			
  //return win_printf_OK("Cs = %g g = %g \n\\sigma_s = %g \\sigma_p = %g xf = %g",Cs,g,sigmas,sigmap,xf);
}


int	draw_marko_hat_slope_vs_F(void)
{
  register int i;
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *ds = NULL, *dscs = NULL, *dsss = NULL, *dssp = NULL, *dse = NULL;
  static int nf = 1024;
  static float T = 298;
  static float A = 50, C = 85, P = 24, fm = 5;	/* nm*/
  //  static float L = 16.2;	/* microns */
  float  sigmas, sigmap, alpha, g, xf, kBT, Cs, f;
  int error;
  double omega_0;

  omega_0 = 2*M_PI/3.6;
	
	
  if(updating_menu_state != 0)	return D_O_K;	
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	
    return win_printf_OK("cannot find data");
  i = win_scanf("Draw Marko hat slope vs F curve\n"
		"Bending pesistence length (nm) %6f\n"
		"Torsionnal persistence length (nm) %6f\n"
		"Plectoneme persistence length (nm) %6f\n"
		"max Force (pN) %6f\n"
		"Temperature value (Kelvin) %6f\n"
		"number of points %6d\n"
		,&A,&C,&P,&fm,&T,&nf);
  if (i == WIN_CANCEL)	return OFF;
  op = create_and_attach_one_plot(pr, nf, nf, 0);
  if (op == NULL)    return win_printf("cannot create plot !");
  ds = op->dat[0];

  dscs = create_and_attach_one_ds(op, nf, nf, 0);
  dsss = create_and_attach_one_ds(op, nf, nf, 0);
  dssp = create_and_attach_one_ds(op, nf, nf, 0);
  dse = create_and_attach_one_ds(op, nf, nf, 0);


  kBT = 1.38062e-2 * T; // pN . nm

  for (i = 0, ds->nx = 0; i < nf; i++)
    {
      f = fm * (1 - (float)i/nf);
      alpha = A*f/kBT;
      if (give_extension_order_1( f, A,  kBT, &error) <= 0.01) break;
      dscs->yd[ds->nx] = Cs = compute_writhe_renormalize_torsion(A, C, alpha, &error);
      dse->yd[ds->nx] = g = compute_energy_order_1( A, f,  alpha, &error);
      dsss->yd[ds->nx] = sigmas = compute_sigma_s(P, g, Cs, kBT, &error);
      dssp->yd[ds->nx] = sigmap = compute_sigma_p(P, g, Cs, kBT, &error);
      xf = give_extension_order_1_vs_sigma( f, A, C, kBT, sigmas, &error);

      if (sigmap-sigmas > 0)
	  ds->yd[ds->nx] = 2*M_PI*xf/((sigmap-sigmas)*omega_0);
      else ds->yd[ds->nx] = 0;
      dssp->xd[ds->nx] = dsss->xd[ds->nx] = dscs->xd[ds->nx] = dse->xd[ds->nx] = ds->xd[ds->nx] = f;
      dssp->nx = dsss->nx = dse->nx = dscs->nx = ds->nx = i+1;

    }
  set_ds_source(ds, "Marko hat A = %g C = %g P = %g f = %g T = %g ",A,C,P,fm,T);

  return refresh_plot(pr,pr->cur_op);			
  //return win_printf_OK("Cs = %g g = %g \n\\sigma_s = %g \\sigma_p = %g xf = %g",Cs,g,sigmas,sigmap,xf);
}

int	draw_marko_hat_improved_slope_vs_F(void)
{
  register int i;
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *ds = NULL, *dscs = NULL, *dsss = NULL, *dssp = NULL, *dse = NULL;
  static int nf = 1024;
  static float T = 298;
  static float A = 50, C = 85, P = 24, fm = 5;	/* nm*/
  //  static float L = 16.2;	/* microns */
  float  sigmas, sigmap, alpha, g, xf, kBT, Cs, f;
  int error;
  double omega_0;

  omega_0 = 2*M_PI/3.6;
	
	
  if(updating_menu_state != 0)	return D_O_K;	
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	
    return win_printf_OK("cannot find data");
  i = win_scanf("Draw Marko hat improved slope vs F curve\n"
		"Bending pesistence length (nm) %6f\n"
		"Torsionnal persistence length (nm) %6f\n"
		"Plectoneme persistence length (nm) %6f\n"
		"max Force (pN) %6f\n"
		"Temperature value (Kelvin) %6f\n"
		"number of points %6d\n"
		,&A,&C,&P,&fm,&T,&nf);
  if (i == WIN_CANCEL)	return OFF;
  op = create_and_attach_one_plot(pr, nf, nf, 0);
  if (op == NULL)    return win_printf("cannot create plot !");
  ds = op->dat[0];

  dscs = create_and_attach_one_ds(op, nf, nf, 0);
  dsss = create_and_attach_one_ds(op, nf, nf, 0);
  dssp = create_and_attach_one_ds(op, nf, nf, 0);
  dse = create_and_attach_one_ds(op, nf, nf, 0);

  kBT = 1.38062e-2 * T; // pN . nm

  for (i = 0, ds->nx = 0; i < nf; i++)
    {
      f = fm * (1 - (float)i/nf);
      alpha = A*f/kBT;
      if (give_extension_wlc( f, A,  kBT, &error) <= 0.01) break;
      Cs = compute_writhe_renormalize_torsion(A, C, alpha, &error);
      //Cs = (Cs > 2*P) ? Cs : 2*P;
      //Cs = C;
      dscs->yd[ds->nx] = Cs;
      dse->yd[ds->nx] = g = compute_energy_wlc( A, f,  alpha, &error);
      dsss->yd[ds->nx] = sigmas = compute_sigma_s(P, g, Cs, kBT, &error);
      dssp->yd[ds->nx] = sigmap = compute_sigma_p(P, g, Cs, kBT, &error);
      xf = give_extension_wlc_vs_sigma( f, A, C, kBT, sigmas, &error);
      //xf = give_extension_wlc( f, A,  kBT, &error);
      if (sigmap-sigmas > 0)
	  ds->yd[ds->nx] = 2*M_PI*xf/((sigmap-sigmas)*omega_0);
      else ds->yd[ds->nx] = 0;
      dssp->xd[ds->nx] = dsss->xd[ds->nx] = dscs->xd[ds->nx] = dse->xd[ds->nx] = ds->xd[ds->nx] = f;
      //if (i%128 == 0) win_printf("i %d f %g slope %g",i,f,ds->yd[ds->nx]);
      dssp->nx = dsss->nx = dse->nx = dscs->nx = ds->nx = i+1;
    }
  set_ds_source(ds, "Marko hat improved A = %g C = %g P = %g f = %g T = %g ",A,C,P,fm,T);

  return refresh_plot(pr,pr->cur_op);			
  //return win_printf_OK("Cs = %g g = %g \n\\sigma_s = %g \\sigma_p = %g xf = %g",Cs,g,sigmas,sigmap,xf);
}

int	draw_marko_hat_improved_no_Cs_slope_vs_F(void)
{
  register int i;
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *ds = NULL, *dscs = NULL, *dsss = NULL, *dssp = NULL, *dse = NULL;
  static int nf = 1024;
  static float T = 298;
  static float A = 50, C = 85, P = 24, fm = 5;	/* nm*/
  float  sigmas, sigmap, alpha, g, xf, kBT, Cs, f;
  int error;
  double omega_0;

  omega_0 = 2*M_PI/3.6;
	
	
  if(updating_menu_state != 0)	return D_O_K;	
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	
    return win_printf_OK("cannot find data");
  i = win_scanf("Draw Marko hat improved without Nelson correction slope vs F curve\n"
		"Bending pesistence length (nm) %6f\n"
		"Torsionnal persistence length (nm) %6f\n"
		"Plectoneme persistence length (nm) %6f\n"
		"max Force (pN) %6f\n"
		"Temperature value (Kelvin) %6f\n"
		"number of points %6d\n"
		,&A,&C,&P,&fm,&T,&nf);
  if (i == WIN_CANCEL)	return OFF;
  op = create_and_attach_one_plot(pr, nf, nf, 0);
  if (op == NULL)    return win_printf("cannot create plot !");
  ds = op->dat[0];

  dscs = create_and_attach_one_ds(op, nf, nf, 0);
  dsss = create_and_attach_one_ds(op, nf, nf, 0);
  dssp = create_and_attach_one_ds(op, nf, nf, 0);
  dse = create_and_attach_one_ds(op, nf, nf, 0);

  kBT = 1.38062e-2 * T; // pN . nm

  for (i = 0, ds->nx = 0; i < nf; i++)
    {
      f = fm * (1 - (float)i/nf);
      alpha = A*f/kBT;
      if (give_extension_wlc( f, A,  kBT, &error) <= 0.01) break;
      //Cs = compute_writhe_renormalize_torsion(A, C, alpha, &error);
      //Cs = (Cs > 2*P) ? Cs : 2*P;
      Cs = C;
      dscs->yd[ds->nx] = Cs;
      dse->yd[ds->nx] = g = compute_energy_wlc( A, f,  alpha, &error);
      dsss->yd[ds->nx] = sigmas = compute_sigma_s(P, g, Cs, kBT, &error);
      dssp->yd[ds->nx] = sigmap = compute_sigma_p(P, g, Cs, kBT, &error);
      //xf = give_extension_wlc_vs_sigma( f, A, C, kBT, sigmas, &error);
      xf = give_extension_wlc( f, A,  kBT, &error);
      if (sigmap-sigmas > 0)
	  ds->yd[ds->nx] = 2*M_PI*xf/((sigmap-sigmas)*omega_0);
      else ds->yd[ds->nx] = 0;
      dssp->xd[ds->nx] = dsss->xd[ds->nx] = dscs->xd[ds->nx] = dse->xd[ds->nx] = ds->xd[ds->nx] = f;
      //if (i%128 == 0) win_printf("i %d f %g slope %g",i,f,ds->yd[ds->nx]);
      dssp->nx = dsss->nx = dse->nx = dscs->nx = ds->nx = i+1;
    }
  set_ds_source(ds, "Marko hat improved Cs = C, A = %g C = %g P = %g f = %g T = %g ",A,C,P,fm,T);

  return refresh_plot(pr,pr->cur_op);			
  //return win_printf_OK("Cs = %g g = %g \n\\sigma_s = %g \\sigma_p = %g xf = %g",Cs,g,sigmas,sigmap,xf);
}

int	draw_extension_order_1(void)
{
  register int i;
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *ds = NULL;
  d_s *ds2 = NULL;
  static int nf = 1024;
  static float T = 298;
  static float A = 50, fm = 50;	/* nm*/
  //  static float L = 16.2;	/* microns */
  float  alpha, xf, kBT, f;
  int error;
  //double omega_0 = 2*M_PI/3.6;
	
	
  if(updating_menu_state != 0)	return D_O_K;	
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	
    return win_printf_OK("cannot find data");
  i = win_scanf("Draw extension order 1 F curve\n"
		"Bending pesistence length (nm) %6f\n"
		"max Force (pN) %6f\n"
		"Temperature value (Kelvin) %6f\n"
		"number of points %6d\n"
		,&A,&fm,&T,&nf);
  if (i == WIN_CANCEL)	return OFF;
  op = create_and_attach_one_plot(pr, nf, nf, 0);
  if (op == NULL)    return win_printf("cannot create plot !");
  ds = op->dat[0];
  ds2 = create_and_attach_one_ds(op, nf, nf, 0);
  if (ds2 == NULL)    return win_printf("cannot create plot !");
  kBT = 1.38062e-2 * T; // pN . nm

  for (i = 0, ds->nx = 0, f = fm; i < nf; f *= 0.99, i++)
    {
      alpha = A*f/kBT;
      //if (alpha <= 0.251) break;
      xf = give_extension_order_2( f, A,  kBT, &error);
      if (xf <= 0.001) break;
      ds->xd[ds->nx] = xf;
      ds->yd[ds->nx] = alpha;
      ds2->xd[ds->nx] = xf;
      ds2->yd[ds->nx] = marko_siggia_improved( xf);
      ds->nx = ds2->nx = i+1;
    }
  set_ds_source(ds, "Marko hat A = %g f = %g T = %g ",A,f,T);

  return refresh_plot(pr,pr->cur_op);			
  //return win_printf_OK("Cs = %g g = %g \n\\sigma_s = %g \\sigma_p = %g xf = %g",Cs,g,sigmas,sigmap,xf);
}


int dump_wlc_marko_diff(void)
{
  register int i, j;
  double F[300] = {0}, l[300] = {0}, x, FMS, *a;


    for (i = 0; i < 100; i++)
      {
	x = l[i] = claude[i].x;
	F[i] = claude[i].alpha;
	FMS = (x == 0) ? 0 : (x -.25 + .25*(1/((1-x)*(1-x))));
	F[i] -= FMS;
      }

    for (j = 10; j <= 200; i++, j++)
      {
	x = l[i] = claude_h[j].x;
	F[i] = claude_h[j].alpha;
	FMS = (x == 0) ? 0 : (x -.25 + .25*(1/((1-x)*(1-x))));
	F[i] -= FMS;
      }
    l[i] = 1;
    F[i++] = 0;
    
    fit_double_arrays_to_xn_polynome(l, F, i, 8, &a);
      
    win_printf("Fit of exact WLC - Marko-Siggia over %d points with a poly 7th\n"
	       "gives",i);
    return i;

}



// hat curves 
/* dna relative extension  < z >/L and mormalized torsion angle  eta = 2  pi n A/L are given as 
function of the normalized torque  kappa = Gamma /(k_B T) and  alphaa = F A/(k_B T)  for agiven value 
of A/C :  < z >/L =   Fel(alpha,kappa) and  eta = kappa ( A/C   +  Fwr(alpha,kappa)).
For a given value of < z >/L, eta varies linearly with A/C with a slope kappa.
Fel_2 correspond to alpha = 2
*/
int nb_a_2 = 88, nb_hat[] = {88,133,181};
hatFel Fel_2[] = {
{0,0.627572464314631}, {0.1,0.6267968887437515}, {0.1414213562373095,0.6260087089174926},
{0.17320508075688773,0.6252074776227738}, {0.2,0.6243927252457498},    {0.22360679774997896,0.6235639583753481},
{0.2449489742783178,0.6227206583037296}, {0.2645751311064591,0.6218622794149147}, {0.282842712474619,0.6209882474519645},
{0.3,0.6200979576522039}, {0.31622776601683794,0.6191907727389089}, {0.33166247903553997,0.6182660207567742},
{0.34641016151377546,0.6173229927371781}, {0.36055512754639896,0.6163609401778825}, {0.37416573867739417,0.6153790723202153},
{0.3872983346207417,0.6143765532050679}, {0.4,0.6133524984870742}, {0.41231056256176607,0.6123059719842058},
{0.4242640687119285,0.6112359819375698}, {0.43588989435406733,0.610141476953542}, {0.4472135954999579,0.609021341597312},
{0.458257569495584,0.6078743916035838}, {0.469041575982343,0.606699368666362}, {0.47958315233127197,0.6054949347655423},
{0.4898979485566356,0.6042596659832523}, {0.5,0.6029920457575477}, {0.5099019513592785,0.6016904575150593},
{0.5196152422706632,0.6003531766174146}, {0.5291502622129182,0.598978361548627}, {0.5385164807134504,0.5975640442620488},
{0.5477225575051661,0.5961081195957654}, {0.5567764362830022,0.5946083336543331}, {0.565685424949238,0.5930622710423548},
{0.5744562646538028,0.5914673408213285}, {0.5830951894845301,0.5898207610452993}, {0.5916079783099616,0.5881195417128028},
{0.6,0.5863604659521251}, {0.6082762530298219,0.5845400692336891}, {0.6164414002968976,0.5826546163770144},
{0.6244997998398398,0.5807000760897459}, {0.6324555320336759,0.5786720927422482}, {0.6403124237432849,0.5765659550426001},
{0.648074069840786,0.5743765612329459}, {0.6557438524302001,0.57209838037832}, {0.6633249580710799,0.5697254092625017},
{0.6708203932499369,0.5672511243413904}, {0.6782329983125268,0.564668428131806}, {0.6855654600401044,0.5619695893317312},
{0.6928203230275509,0.5591461758756409}, {0.7,0.5561889800249593}, {0.7071067811865476,0.553087934477819},
{0.714142842854285,0.5498320183536025}, {0.7211102550927979,0.546409151765792}, {0.7280109889280518,0.5428060775417687},
{0.7348469228349535,0.5390082284813733}, {0.7416198487095663,0.5349995783697814}, {0.7483314773547883,0.5307624747788707},
{0.754983443527075,0.526277451512007}, {0.7615773105863908,0.5215230183812802}, {0.7681145747868608,0.5164754258706723},
{0.7745966692414834,0.5111084021583704}, {0.7810249675906654,0.5053928599825417}, {0.7874007874011811,0.4992965709884718},
{0.7937253933193772,0.49278380556292894}, {0.8,0.48581493684324795}, {0.806225774829855,0.4783460087199006},
{0.812403840463596,0.47032826941554745}, {0.8185352771872451,0.4617076748646938}, {0.8246211251235321,0.45242436995451346},
{0.8306623862918076,0.44241216112648263}, {0.8366600265340756,0.43159800138582094}, {0.8426149773176358,0.419901519024984},
{0.848528137423857,0.40723463501758905}, {0.8544003745317531,0.3935013317711531}, {0.8602325267042626,0.3785976583126839},
{0.8660254037844386,0.36241208423385984}, {0.8717797887081347,0.34482634630025183}, {0.8774964387392122,0.32571696562977087},
{0.8831760866327847,0.3049576456596628}, {0.8888194417315589,0.2824227843816886}, {0.8944271909999159,0.2579923368321227},
{0.9,0.23155822889932717}, {0.9055385138137417,0.20303243011378005}, {0.9110433579144299,0.17235661893190996},
{0.916515138991168,0.1395131026629159}, {0.9219544457292888,0.10453628682622895}, {0.9273618495495703,0.06752355957845377},
{0.9327379053088815,0.028644046784610497}};




hatFwr Fwr_2[] = {
{0,0.2959937389279431}, {0.1,0.2975113192807429},
{0.1414213562373095,0.29906633109129116}, {0.17320508075688773,0.3006603229183665},
{0.2,0.30229492705813965}, {0.22360679774997896,0.3039718650335291},
{0.2449489742783178,0.3056929535045621}, {0.2645751311064591,0.3074601106366556},
{0.282842712474619,0.30927536296738933}, {0.3,0.3111408528163645},
{0.31622776601683794,0.3130588462872401}, {0.33166247903553997,0.315031741916013},
{0.34641016151377546,0.3170620800251562}, {0.36055512754639896,0.31915255284940025},
{0.37416573867739417,0.32130601550582927}, {0.3872983346207417,0.3235254978886301},
{0.4,0.32581421757742096}, {0.41231056256176607,0.3281755938576528},
{0.4242640687119285,0.33061326296232535}, {0.43588989435406733,0.33313109465625024},
{0.4472135954999579,0.33573321029759146}, {0.458257569495584,0.3384240025265188},
{0.469041575982343,0.34120815674781596}, {0.47958315233127197,0.3440906745933988},
{0.4898979485566356,0.3470768995722245}, {0.5,0.35017254513933344},
{0.5099019513592785,0.35338372544315266}, {0.5196152422706632,0.356716989041105},
{0.5291502622129182,0.36017935590854033}, {0.5385164807134504,0.36377835810558357},
{0.5477225575051661,0.3675220845113327}, {0.5567764362830022,0.3714192300856969},
{0.565685424949238,0.3754791501768997}, {0.5744562646538028,0.37971192045826},
{0.5830951894845301,0.38412840315249164}, {0.5916079783099616,0.38874032028665584},
{0.6,0.393560334817668}, {0.6082762530298219,0.3986021405785434},
{0.6164414002968976,0.40388056212137347}, {0.6244997998398398,0.40941166567666765},
{0.6324555320336759,0.41521288261268624}, {0.6403124237432849,0.4213031469657562},
{0.648074069840786,0.42770304882665}, {0.6557438524302001,0.4344350056126937},
{0.6633249580710799,0.4415234535346967}, {0.6708203932499369,0.44899506188680494},
{0.6782329983125268,0.45687897315133064}, {0.6855654600401044,0.46520707232528535},
{0.6928203230275509,0.47401428934701606}, {0.7,0.4833389390366328},
{0.7071067811865476,0.4932231035694385}, {0.714142842854285,0.5037130631838813},
{0.7211102550927979,0.5148597815902156}, {0.7280109889280518,0.5267194533970858},
{0.7348469228349535,0.5393541218117437}, {0.7416198487095663,0.5528323758918418},
{0.7483314773547883,0.5672301377216282}, {0.754983443527075,0.582631551030558},
{0.7615773105863908,0.5991299839289385}, {0.7681145747868608,0.6168291595402584},
{0.7745966692414834,0.6358444292658947}, {0.7810249675906654,0.6563042040787703},
{0.7874007874011811,0.6783515593939613}, {0.7937253933193772,0.7021460283977685},
{0.8,0.7278655967951884}, {0.806225774829855,0.7557089081485223},
{0.812403840463596,0.7858976824863272}, {0.8185352771872451,0.8186793405216517},
{0.8246211251235321,0.854329810111055}, {0.8306623862918076,0.8931564685240722},
{0.8366600265340756,0.9355011411454449}, {0.8426149773176358,0.9817430312828546},
{0.848528137423857,1.0323013931437404}, {0.8544003745317531,1.0876376767956184},
{0.8602325267042626,1.1482567662522316}, {0.8660254037844386,1.2147067971695689},
{0.8717797887081347,1.2875768794298272}, {0.8774964387392122,1.3674918684144215},
{0.8831760866327847,1.4551031432746504}, {0.8888194417315589,1.5510741926477907},
{0.8944271909999159,1.656059731103766}, {0.9,1.7706771523495217},
{0.9055385138137417,1.8954694727998274}, {0.9110433579144299,2.030859650511371},
{0.916515138991168,2.177097382059836}, {0.9219544457292888,2.33420121422357},
{0.9273618495495703,2.501900940062069}, {0.9327379053088815,2.6795874362896313}};

// 133 pts
int nb_a_3 = 133;
hatFel Fel_3[] = {
{0,0.6999812005334466}, {0.1,0.6996809097542886},
{0.1414213562373095,0.6993784599631978}, {0.17320508075688773,0.699073807473513},
{0.2,0.6987669071692768}, {0.22360679774997896,0.6984577124442884},
{0.2449489742783178,0.6981461751380147}, {0.2645751311064591,0.6978322454681904},
{0.282842712474619,0.6975158719598928}, {0.3,0.6971970013708825},
{0.31622776601683794,0.6968755786129832}, {0.33166247903553997,0.6965515466692531},
{0.34641016151377546,0.6962248465066893}, {0.36055512754639896,0.6958954169841784},
{0.37416573867739417,0.6955631947554048}, {0.3872983346207417,0.6952281141663789},
{0.4,0.6948901071472594}, {0.41231056256176607,0.6945491030980798},
{0.4242640687119285,0.6942050287679934}, {0.43588989435406733,0.6938578081276091},
{0.4472135954999579,0.6935073622339504}, {0.458257569495584,0.6931536090875536},
{0.469041575982343,0.6927964634811674}, {0.47958315233127197,0.692435836839478},
{0.4898979485566356,0.6920716370492536}, {0.5,0.6917037682792296},
{0.5099019513592785,0.6913321307890244}, {0.5196152422706632,0.6909566207263095},
{0.5291502622129182,0.6905771299113918}, {0.5385164807134504,0.6901935456083071},
{0.5477225575051661,0.6898057502814418}, {0.5567764362830022,0.68941362133662},
{0.565685424949238,0.6890170308455144}, {0.5744562646538028,0.688615845252123},
{0.5830951894845301,0.6882099250599666}, {0.5916079783099616,0.6877991244985385},
{0.6,0.6873832911674063}, {0.6082762530298219,0.686962265656235},
{0.6164414002968976,0.6865358811388379}, {0.6244997998398398,0.6861039629392037},
{0.6324555320336759,0.6856663280672504}, {0.6403124237432849,0.6852227847218658},
{0.648074069840786,0.6847731317585565}, {0.6557438524302001,0.6843171581187927},
{0.6633249580710799,0.6838546422178446}, {0.6708203932499369,0.6833853512876268},
{0.6782329983125268,0.6829090406707037}, {0.6855654600401044,0.6824254530612683},
{0.6928203230275509,0.6819343176884678}, {0.7,0.681435349437008},
{0.7071067811865476,0.6809282478994698}, {0.714142842854285,0.6804126963541753},
{0.7211102550927979,0.6798883606618511}, {0.7280109889280518,0.6793548880736117},
{0.7348469228349535,0.6788119059420158}, {0.7416198487095663,0.6782590203260637},
{0.7483314773547883,0.6776958144800522}, {0.754983443527075,0.6771218472150784},
{0.7615773105863908,0.6765366511207878}, {0.7681145747868608,0.6759397306335662},
{0.7745966692414834,0.6753305599358399}, {0.7810249675906654,0.6747085806694044},
{0.7874007874011811,0.674073199443736}, {0.7937253933193772,0.6734237851180299},
{0.8,0.6727596658332183}, {0.806225774829855,0.6720801257673557},
{0.812403840463596,0.6713844015845888}, {0.8185352771872451,0.6706716785442557},
{0.8246211251235321,0.6699410862325307}, {0.8306623862918076,0.6691916938743047},
{0.8366600265340756,0.6684225051776193}, {0.8426149773176358,0.6676324526568311},
{0.848528137423857,0.6668203913736431}, {0.8544003745317531,0.6659850920271019},
{0.8602325267042626,0.6651252333143991}, {0.8660254037844386,0.6642393934737043},
{0.8717797887081347,0.6633260409079925}, {0.8774964387392122,0.6623835237747308},
{0.8831760866327847,0.661410058409922}, {0.8888194417315589,0.6604037164361362},
{0.8944271909999159,0.6593624103821775}, {0.9,0.6582838776165666},
{0.9055385138137417,0.6571656623672806}, {0.9110433579144299,0.6560050955655637},
{0.916515138991168,0.6547992722110598}, {0.9219544457292888,0.6535450259080674},
{0.9273618495495703,0.6522389001669608}, {0.9327379053088815,0.6508771159992764},
{0.938083151964686,0.649455535257691}, {0.9433981132056604,0.6479696190808711},
{0.9486832980505138,0.6464143806952053}, {0.9539392014169457,0.6447843316974137},
{0.9591663046625439,0.6430734207899483}, {0.9643650760992956,0.641274963760129},
{0.9695359714832659,0.6393815632781674}, {0.9746794344808964,0.6373850168315124},
{0.9797958971132712,0.635276210804728}, {0.9848857801796105,0.6330449983448314},
{0.9899494936611666,0.6306800582092958}, {0.99498743710662,0.6281687312624389},
{1.,0.6254968306477715}, {1.004987562112089,0.6226484208975318},
{1.0099504938362078,0.6196055603208919}, {1.014889156509222,0.616347999910091},
{1.019803902718557,0.6128528306859913}, {1.02469507659596,0.6090940698353227},
{1.0295630140987,0.6050421741339335}, {1.03440804327886,0.6006634669693924},
{1.0392304845413265,0.5959194627476578}, {1.044030650891055,0.5907660695901799},
{1.0488088481701516,0.5851526480412113}, {1.0535653752852738,0.5790209001303845},
{1.0583005244258363,0.5723035598283835}, {1.063014581273465,0.5649228531753154},
{1.0677078252031311,0.5567886950014069}, {1.0723805294763609,0.54779659063314},
{1.0770329614269007,0.537825217633846}, {1.0816653826391966,0.526733678207293},
{1.0862780491200215,0.514358443197125}, {1.0908712114635715,0.5005100623742053},
{1.0954451150103321,0.48496980557164326}, {1.1,0.46748654260823364},
{1.104536101718726,0.4477743888964793}, {1.1090536506409416,0.42551196222883164},
{1.1135528725660044,0.4003445322489471}, {1.118033988749895,0.3718908900326836},
{1.1224972160321824,0.33975735208081603}, {1.1269427669584644,0.3035617530932799},
{1.131370849898476,0.26297019932383975}, {1.1357816691600546,0.21774814883628785},
{1.140175425099138,0.16782433409643485}, {1.1445523142259597,0.11336070009509974},
{1.1489125293076057,0.05481450733288579}}; 

hatFwr Fwr_3[] = {
{0,0.1999382822134173}, {0.1,0.20033788820335818}, 
{0.1414213562373095,0.20074229211812514}, {0.17320508075688773,0.20115161745478669},
{0.2,0.2015659922130774}, {0.22360679774997896,0.2019855490994224},
{0.2449489742783178,0.2024104257418895}, {0.2645751311064591,0.20284076491674125},
{0.282842712474619,0.2032767147873112}, {0.3,0.20371842915597274},
{0.31622776601683794,0.20416606773002774}, {0.33166247903553997,0.20461979640240285},
{0.34641016151377546,0.20507978754809894}, {0.36055512754639896,0.20554622033741446},
{0.37416573867739417,0.20601928106703343}, {0.3872983346207417,0.2064991635101518},
{0.4,0.20698606928690388}, {0.41231056256176607,0.20748020825643995},
{0.4242640687119285,0.2079817989321155}, {0.43588989435406733,0.20849106892135746},
{0.4472135954999579,0.20900825539189832}, {0.458257569495584,0.20953360556619394},
{0.469041575982343,0.21006737724599067}, {0.47958315233127197,0.21060983936915062},
{0.4898979485566356,0.21116127260102782}, {0.5,0.21172196996285292},
{0.5099019513592785,0.21229223749979878}, {0.5196152422706632,0.2128723949916076},
{0.5291502622129182,0.21346277670890046}, {0.5385164807134504,0.21406373221855063},
{0.5477225575051661,0.21467562724177913}, {0.5567764362830022,0.215298844568951},
{0.565685424949238,0.21593378503537788}, {0.5744562646538028,0.21658086856281175},
{0.5830951894845301,0.2172405352717186}, {0.5916079783099616,0.21791324666986533},
{0.6,0.21859948692324901}, {0.6082762530298219,0.21929976421592867},
{0.6164414002968976,0.22001461220591723}, {0.6244997998398398,0.22074459158494283},
{0.6324555320336759,0.22149029175060525}, {0.6403124237432849,0.2222523326002491},
{0.648074069840786,0.22303136645674423}, {0.6557438524302001,0.22382808013734012},
{0.6633249580710799,0.22464319717781767}, {0.6708203932499369,0.22547748022536032},
{0.6782329983125268,0.2263317336148613}, {0.6855654600401044,0.22720680614485633},
{0.6928203230275509,0.22810359407087}, {0.7,0.2290230443357764},
{0.7071067811865476,0.22996615805875883}, {0.714142842854285,0.23093399430669048},
{0.7211102550927979,0.23192767417423787}, {0.7280109889280518,0.2329483852017588},
{0.7348469228349535,0.23399738616317262}, {0.7416198487095663,0.23507601225944239},
{0.7483314773547883,0.2361856807572029}, {0.754983443527075,0.23732789711642005},
{0.7615773105863908,0.2385042616558773}, {0.7681145747868608,0.2397164768107788},
{0.7745966692414834,0.24096635504297892}, {0.7810249675906654,0.24225582747133023},
{0.7874007874011811,0.2435869532975498}, {0.7937253933193772,0.24496193011193607},
{0.8,0.24638310517337303}, {0.806225774829855,0.2478529877695462},
{0.812403840463596,0.24937426277630034}, {0.8185352771872451,0.25094980554989194},
{0.8246211251235321,0.25258269830276364}, {0.8306623862918076,0.2542762481327085},
{0.8366600265340756,0.25603400689728695}, {0.8426149773176358,0.2578597931505369},
{0.848528137423857,0.25975771638785444}, {0.8544003745317531,0.2617322038780705},
{0.8602325267042626,0.26378803039984244}, {0.8660254037844386,0.26593035124339126},
{0.8717797887081347,0.2681647388893034}, {0.8774964387392122,0.2704972238347073},
{0.8831760866327847,0.27293434010504386}, {0.8888194417315589,0.2754831760684367},
{0.8944271909999159,0.2781514312613103}, {0.9,0.2809474800406623},
{0.9055385138137417,0.28388044300302473}, {0.9110433579144299,0.2869602672559445},
{0.916515138991168,0.29019781679866224}, {0.9219544457292888,0.2936049744693608},
{0.9273618495495703,0.2971947571525089}, {0.9327379053088815,0.300981446218337},
{0.938083151964686,0.3049807354956096}, {0.9433981132056604,0.3092098994686084},
{0.9486832980505138,0.31368798485173743}, {0.9539392014169457,0.3184360292451287},
{0.9591663046625439,0.3234773112299015}, {0.9643650760992956,0.3288376370440734},
{0.9695359714832659,0.33454566991592016}, {0.9746794344808964,0.3406333092530553},
{0.9797958971132712,0.34713612823176165}, {0.9848857801796105,0.35409387994979935},
{0.9899494936611666,0.3615510842548323}, {0.99498743710662,0.3695577097098417},
{1.,0.3781699679909148}, {1.004987562112089,0.38745124143303655},
{1.0099504938362078,0.3974731685667091}, {1.014889156509222,0.40831691746431126},
{1.019803902718557,0.4200746827043119}, {1.02469507659596,0.43285144894872457},
{1.0295630140987,0.4467670727147555}, {1.03440804327886,0.4619587441076493},
{1.0392304845413265,0.47858390224603986}, {1.044030650891055,0.49682369195890713},
{1.0488088481701516,0.5168870650127977}, {1.0535653752852738,0.5390156462878646},
{1.0583005244258363,0.5634895030746915}, {1.063014581273465,0.590633972206178},
{1.0677078252031311,0.6208277117396648}, {1.0723805294763609,0.6545121455587583},
{1.0770329614269007,0.6922024508424499}, {1.0816653826391966,0.734500184048918},
{1.0862780491200215,0.7821075259447966}, {1.0908712114635715,0.8358429121062181},
{1.0954451150103321,0.8966574458984397}, {1.1,0.9656508870843787},
{1.104536101718726,1.0440850677242526}, {1.1090536506409416,1.1333911896683055},
{1.1135528725660044,1.235165504343319}, {1.118033988749895,1.3511453629088088},
{1.1224972160321824,1.483154805549773}, {1.1269427669584644,1.633006499503021},
{1.131370849898476,1.8023465484472865}, {1.1357816691600546,1.9924331573238567},
{1.140175425099138,2.2038527319581913}, {1.1445523142259597,2.436200155665057},
{1.1489125293076057,2.6877817466244522}};

int nb_a_4 = 181;
hatFel Fel_4[] = {
{0,0.742130748518306}, {0.1,0.7419691762450474},
{0.1414213562373095,0.7418070061206309}, {0.17320508075688773,0.7416442312702614},
{0.2,0.7414808446698179}, {0.22360679774997896,0.7413168391413175},
{0.2449489742783178,0.7411522073482071}, {0.2645751311064591,0.7409869417904742},
{0.282842712474619,0.7408210347995751}, {0.3,0.740654478533169},
{0.31622776601683794,0.7404872649696492}, {0.33166247903553997,0.7403193859024593},
{0.34641016151377546,0.7401508329341925}, {0.36055512754639896,0.739981597470457},
{0.37416573867739417,0.7398116707134994}, {0.3872983346207417,0.7396410436555743},
{0.4,0.7394697070720488}, {0.41231056256176607,0.7392976515142303},
{0.4242640687119285,0.739124867301901}, {0.43588989435406733,0.7389513445155536},
{0.4472135954999579,0.7387770729883001}, {0.458257569495584,0.7386020422974515},
{0.469041575982343,0.738426241755749}, {0.47958315233127197,0.7382496604022197},
{0.4898979485566356,0.7380722869926536}, {0.5,0.7378941099896772},
{0.5099019513592785,0.7377151175523945}, {0.5196152422706632,0.737535297525591},
{0.5291502622129182,0.7373546374284647}, {0.5385164807134504,0.7371731244428668},
{0.5477225575051661,0.7369907454010248}, {0.5567764362830022,0.7368074867727249},
{0.565685424949238,0.7366233346519185}, {0.5744562646538028,0.7364382747427319},
{0.5830951894845301,0.7362522923448411}, {0.5916079783099616,0.736065372338181},
{0.6,0.7358774991669567}, {0.6082762530298219,0.7356886568229092},
{0.6164414002968976,0.7354988288278108}, {0.6244997998398398,0.7353079982151334},
{0.6324555320336759,0.7351161475108579}, {0.6403124237432849,0.7349232587133614},
{0.648074069840786,0.7347293132723536}, {0.6557438524302001,0.7345342920667826},
{0.6633249580710799,0.7343381753816778}, {0.6708203932499369,0.7341409428838499},
{0.6782329983125268,0.7339425735963939}, {0.6855654600401044,0.733743045871925},
{0.6928203230275509,0.7335423373644677}, {0.7,0.7333404249999289},
{0.7071067811865476,0.7331372849450636}, {0.714142842854285,0.7329328925748492},
{0.7211102550927979,0.7327272224381705}, {0.7280109889280518,0.7325202482217141},
{0.7348469228349535,0.7323119427119675}, {0.7416198487095663,0.732102277755198},
{0.7483314773547883,0.7318912242152978}, {0.754983443527075,0.7316787519293575},
{0.7615773105863908,0.731464829660819}, {0.7681145747868608,0.7312494250500743},
{0.7745966692414834,0.7310325045623186}, {0.7810249675906654,0.7308140334325158},
{0.7874007874011811,0.7305939756072578}, {0.7937253933193772,0.7303722936833343},
{0.8,0.7301489488427929}, {0.806225774829855,0.7299239007842491},
{0.812403840463596,0.7296971076502066}, {0.8185352771872451,0.7294685259501048},
{0.8246211251235321,0.7292381104788181}, {0.8306623862918076,0.7290058142302772},
{0.8366600265340756,0.7287715883058918}, {0.8426149773176358,0.7285353818173942},
{0.848528137423857,0.7282971417837173}, {0.8544003745317531,0.7280568130214906},
{0.8602325267042626,0.7278143380286727}, {0.8660254037844386,0.7275696568608463},
{0.8717797887081347,0.7273227069996203}, {0.8774964387392122,0.7270734232125654},
{0.8831760866327847,0.7268217374040413}, {0.8888194417315589,0.7265675784562415},
{0.8944271909999159,0.7263108720596964}, {0.9,0.7260515405324318},
{0.9055385138137417,0.7257895026269027}, {0.9110433579144299,0.7255246733237375},
{0.916515138991168,0.7252569636112519}, {0.9219544457292888,0.7249862802495922},
{0.9273618495495703,0.7247125255182617}, {0.9327379053088815,0.7244355969456743},
{0.938083151964686,0.7241553870192547}, {0.9433981132056604,0.7238717828744498},
{0.9486832980505138,0.723584665960888}, {0.9539392014169457,0.7232939116837219},
{0.9591663046625439,0.7229993890180231}, {0.9643650760992956,0.7227009600938794},
{0.9695359714832659,0.7223984797496111}, {0.9746794344808964,0.7220917950502593},
{0.9797958971132712,0.7217807447682236}, {0.9848857801796105,0.7214651588225909},
{0.9899494936611666,0.7211448576733412}, {0.99498743710662,0.7208196516662271},
{1.,0.7204893403236574}, {1.004987562112089,0.720153711576427},
{1.0099504938362078,0.7198125409305653}, {1.014889156509222,0.7194655905629479},
{1.019803902718557,0.7191126083386079}, {1.02469507659596,0.718753326741872},
{1.0295630140987,0.7183874617125698}, {1.03440804327886,0.718014711377518},
{1.0392304845413265,0.7176347546663607}, {1.044030650891055,0.7172472497995241},
{1.0488088481701516,0.7168518326345837}, {1.0535653752852738,0.7164481148556571},
{1.0583005244258363,0.7160356819885415}, {1.063014581273465,0.7156140912221252},
{1.0677078252031311,0.7151828690141491}, {1.0723805294763609,0.7147415084565434},
{1.0770329614269007,0.7142894663723358}, {1.0816653826391966,0.7138261601123856},
{1.0862780491200215,0.7133509640159524}, {1.0908712114635715,0.7128632054941513},
{1.0954451150103321,0.7123621606896862}, {1.1,0.7118470496596697},
{1.104536101718726,0.7113170310207304}, {1.1090536506409416,0.7107711959867721},
{1.1135528725660044,0.7102085617194615}, {1.118033988749895,0.7096280638995348},
{1.1224972160321824,0.7090285484130017}, {1.1269427669584644,0.7084087620298963},
{1.131370849898476,0.7077673419339685}, {1.1357816691600546,0.7071028039389835},
{1.140175425099138,0.7064135292005436}, {1.1445523142259597,0.7056977492006363},
{1.1489125293076057,0.7049535287445102}, {1.1532562594670797,0.7041787466647558},
{1.1575836902790226,0.7033710738740908}, {1.161895003862225,0.7025279483444887},
{1.1661903789690602,0.701646546513655}, {1.1704699910719625,0.7007237505276037},
{1.174734012447073,0.699756110616681}, {1.1789826122551597,0.6987398017674358},
{1.1832159566199232,0.6976705736887042}, {1.1874342087037917,0.6965436928702494},
{1.1916375287812984,0.6953538752874852}, {1.1958260743101399,0.6940952080051971},
{1.2,0.6927610575626185}, {1.2041594578792296,0.6913439625638736},
{1.2083045973594573,0.6898355073286498}, {1.2124355652982142,0.688226172748609},
{1.2165525060596438,0.6865051596076397}, {1.2206555615733703,0.6846601785095128},
{1.224744871391589,0.6826771991515771}, {1.2288205727444508,0.680540149905411},
{1.2328828005937953,0.6782305564078001}, {1.2369316876852983,0.6757271049886551},
{1.2409673645990857,0.6730051130848536}, {1.2449899597988732,0.6700358840744105},
{1.2489995996796797,0.6667859179075406}, {1.2529964086141667,0.6632159411155369},
{1.2569805089976536,0.6592797097419538}, {1.2609520212918492,0.6549225258329094},
{1.2649110640673518,0.6500793915772031}, {1.2688577540449522,0.644672704113897},
{1.2727922061357855,0.6386093674909307}, {1.2767145334803705,0.6317771654847609},
{1.2806248474865698,0.6240401998113588}, {1.284523257866513,0.6152331540706933},
{1.2884098726725126,0.6051540994252296}, {1.2922847983320085,0.593555525459848},
{1.296148139681572,0.5801332846606269}, {1.3,0.5645132330434522},
{1.3038404810405297,0.5462356302977398}, {1.307669683062202,0.52473800550127},
{1.3114877048604001,0.4993384924956525}, {1.3152946437965904,0.46922403542246904},
{1.3190905958272918,0.4334519050895209}, {1.3228756555322954,0.3909789947537075},
{1.3266499161421599,0.340740613373461}, {1.3304134695650072,0.2818051773361223},
{1.3341664064126333,0.21362398340923408}, {1.3379088160259651,0.13636147787322606},
{1.3416407864998738,0.051222326896425266}}; 

hatFwr Fwr_4[] = {
{0,0.15615318831160477}, {0.1,0.1563195499602647},
{0.1414213562373095,0.1564869140480926}, {0.17320508075688773,0.15665529653182775},
{0.2,0.1568247137743013}, {0.22360679774997896,0.15699518255781986},
{0.2449489742783178,0.15716672009807586}, {0.2645751311064591,0.15733934405861014},
{0.282842712474619,0.15751307256585195}, {0.3,0.1576879242247619},
{0.31622776601683794,0.15786391813510897}, {0.33166247903553997,0.15804107390840605},
{0.34641016151377546,0.15821941168554007}, {0.36055512754639896,0.15839895215512584},
{0.37416573867739417,0.1585797165726206}, {0.3872983346207417,0.1587617267802336},
{0.4,0.15894500522766983}, {0.41231056256176607,0.15912957499374852},
{0.4242640687119285,0.15931545980893777}, {0.43588989435406733,0.15950268407885276},
{0.4472135954999579,0.15969127290876067}, {0.458257569495584,0.1598812521291478},
{0.469041575982343,0.16007264832239612}, {0.47958315233127197,0.1602654888506292},
{0.4898979485566356,0.16045980188478512}, {0.5,0.16065561643497742},
{0.5099019513592785,0.16085296238221297}, {0.5196152422706632,0.1610518705115337},
{0.5291502622129182,0.16125237254665717}, {0.5385164807134504,0.16145450118619487},
{0.5477225575051661,0.16165829014153016}, {0.5567764362830022,0.16186377417644268},
{0.565685424949238,0.16207098914857498}, {0.5744562646538028,0.16227997205283753},
{0.5830951894845301,0.16249076106685648}, {0.5916079783099616,0.16270339559857824},
{0.6,0.162917916336143}, {0.6082762530298219,0.16313436530015982},
{0.6164414002968976,0.16335278589850874}, {0.6244997998398398,0.16357322298381535},
{0.6324555320336759,0.16379572291374703}, {0.6403124237432849,0.1640203336142913},
{0.648074069840786,0.1642471046461827}, {0.6557438524302001,0.16447608727466417},
{0.6633249580710799,0.1647073345427719}, {0.6708203932499369,0.16494090134834966},
{0.6782329983125268,0.1651768445250126}, {0.6855654600401044,0.16541522292729158},
{0.6928203230275509,0.1656560975202095}, {0.7,0.16589953147355346},
{0.7071067811865476,0.16614559026112652}, {0.714142842854285,0.1663943417652841},
{0.7211102550927979,0.1666458563870768}, {0.7280109889280518,0.1669002071623469},
{0.7348469228349535,0.16715746988415076}, {0.7416198487095663,0.16741772323190207},
{0.7483314773547883,0.16768104890766283}, {0.754983443527075,0.16794753178003657},
{0.7615773105863908,0.1682172600361534}, {0.7681145747868608,0.1684903253422708},
{0.7745966692414834,0.16876682301355342}, {0.7810249675906654,0.16904685219363397},
{0.7874007874011811,0.1693305160446089}, {0.7937253933193772,0.16961792194816316},
{0.8,0.1699091817185776}, {0.806225774829855,0.17020441182842733},
{0.812403840463596,0.17050373364784166}, {0.8185352771872451,0.17080727369826665},
{0.8246211251235321,0.1711151639217391}, {0.8306623862918076,0.1714275419667699},
{0.8366600265340756,0.17174455149201204}, {0.8426149773176358,0.17206634248899197},
{0.848528137423857,0.17239307162528156}, {0.8544003745317531,0.17272490260960174},
{0.8602325267042626,0.17306200658047657}, {0.8660254037844386,0.17340456252018308},
{0.8717797887081347,0.1737527576958994}, {0.8774964387392122,0.17410678813010688},
{0.8831760866327847,0.1744668591024836}, {0.8888194417315589,0.17483318568571998},
{0.8944271909999159,0.17520599331789646}, {0.9,0.17558551841430112},
{0.9055385138137417,0.17597200902181873}, {0.9110433579144299,0.17636572551930582},
{0.916515138991168,0.17676694136767732}, {0.9219544457292888,0.17717594391377095},
{0.9273618495495703,0.1775930352524366}, {0.9327379053088815,0.178018533151711},
{0.938083151964686,0.1784527720463989}, {0.9433981132056604,0.17889610410589651},
{0.9486832980505138,0.1793489003826491}, {0.9539392014169457,0.17981155204826643},
{0.9591663046625439,0.18028447172501094}, {0.9643650760992956,0.18076809492114407},
{0.9695359714832659,0.18126288157947226}, {0.9746794344808964,0.18176931774939328},
{0.9797958971132712,0.18228791739379707}, {0.9848857801796105,0.182819224343371},
{0.9899494936611666,0.18336381441217925}, {0.99498743710662,0.18392229768986798},
{1.,0.18449532102751062}, {1.004987562112089,0.1850835707359622},
{1.0099504938362078,0.18568777551768992}, {1.014889156509222,0.18630870965538116},
{1.019803902718557,0.18694719648328822}, {1.02469507659596,0.1876041121702375},
{1.0295630140987,0.18828038984659473}, {1.03440804327886,0.18897702411128414},
{1.0392304845413265,0.18969507595925708}, {1.044030650891055,0.19043567817468324},
{1.0488088481701516,0.1912000412406815}, {1.0535653752852738,0.19198945982270108},
{1.0583005244258363,0.1928053198898444}, {1.063014581273465,0.19364910654660517},
{1.0677078252031311,0.1945224126568556}, {1.0723805294763609,0.19542694835263044},
{1.0770329614269007,0.19636455153253723}, {1.0816653826391966,0.19733719946874834},
{1.0862780491200215,0.1983470216577708}, {1.0908712114635715,0.19939631406894728},
{1.0954451150103321,0.20048755496629023}, {1.1,0.20162342250433962},
{1.104536101718726,0.20280681432783934}, {1.1090536506409416,0.2040408694388634},
{1.1135528725660044,0.20532899263446558}, {1.118033988749895,0.20667488186397903},
{1.1224972160321824,0.20808255890901275}, {1.1269427669584644,0.20955640385245605},
{1.131370849898476,0.21110119387720647}, {1.1357816691600546,0.21272214702308376},
{1.140175425099138,0.21442497163410476}, {1.1445523142259597,0.21621592235123283},
{1.1489125293076057,0.2181018636517987}, {1.1532562594670797,0.2200903421109113},
{1.1575836902790226,0.2221896687682331}, {1.161895003862225,0.224409013232907},
{1.1661903789690602,0.22675851145923956}, {1.1704699910719625,0.22924938948731954},
{1.174734012447073,0.23189410588013734}, {1.1789826122551597,0.23470651611963222},
{1.1832159566199232,0.23770206287050696}, {1.1874342087037917,0.24089799681048998},
{1.1916375287812984,0.24431363369417694}, {1.1958260743101399,0.24797065450927983},
{1.2,0.25189345705589067}, {1.2041594578792296,0.2561095691037949},
{1.2083045973594573,0.26065013555302147}, {1.2124355652982142,0.2655504948584451},
{1.2165525060596438,0.27085086353486953}, {1.2206555615733703,0.2765971520348873},
{1.224744871391589,0.28284194094786946}, {1.2288205727444508,0.28964565364343964},
{1.2328828005937953,0.29707797061872326}, {1.2369316876852983,0.3052195424833815},
{1.2409673645990857,0.31416407348580655}, {1.2449899597988732,0.3240208667357118},
{1.2489995996796797,0.3349179471008816}, {1.2529964086141667,0.3470059098200088},
{1.2569805089976536,0.36046268432784506}, {1.2609520212918492,0.37549945635474047},
{1.2649110640673518,0.39236806040344446}, {1.2688577540449522,0.41137024320683174},
{1.2727922061357855,0.43286931109825905}, {1.2767145334803705,0.4573048144108491},
{1.2806248474865698,0.4852110920430201}, {1.284523257866513,0.5172406954409007},
{1.2884098726725126,0.5541939165402738}, {1.2922847983320085,0.5970558141120691},
{1.296148139681572,0.647042168045171}, {1.3,0.7056554879961809},
{1.3038404810405297,0.7747511700180605}, {1.307669683062202,0.8566114197649416},
{1.3114877048604001,0.9540194314902309}, {1.3152946437965904,1.0703166633691905},
{1.3190905958272918,1.209409507927023}, {1.3228756555322954,1.3756665056875739},
{1.3266499161421599,1.5736160835267947}, {1.3304134695650072,1.8073324315934873},
{1.3341664064126333,2.0794215359826413}, {1.3379088160259651,2.389652249307244},
{1.3416407864998738,2.733560143949959}}; 
 

double	high_extension(double alpha)
{ 	
  double sa = 0,  tmp = 0;
	 	
  if (alpha <= 0) return 0;
  alpha += .03474; 	
  if (alpha >0)	sa = sqrt(alpha);
  if (sa != 0)	tmp = 1/(sa*2); 
  /*	return (1-tmp - 17/(2304*alpha));*/
  return (1-tmp);  }
 
double	small_extension(double alpha)
{ 	
  double de = 0, num;
  
  de = 1 + alpha*alpha/3;
  num = 4*alpha*alpha*alpha/27;
  num += 2*alpha/(3*de);
  return (num/de);
}

double extension_versus_alpha(double alpha)
{
	register int i;
	double x, y;
	
	if (alpha < 0)	return 0;
	else if (alpha >= 198) return high_extension(alpha);
	else if (alpha < .2) return small_extension(alpha);
	else if (alpha < 9.9 && alpha >= .2)
	{
		i = (int)(alpha*10);	
		x = (alpha*10) - i;
/*		i--;*/
		if (alpha >= claude[i+1].alpha || alpha < claude[i].alpha)
			win_printf("extrapolation pb !");
		y = -  x * (x - 1) * (x - 2) * claude[i-1].x/6;
		y +=  (x + 1) * (x - 1) * (x - 2) * claude[i].x/2;
		y -=  (x + 1) * x * (x - 2) * claude[i+1].x/2;
		y +=  (x + 1) * x * (x - 1) * claude[i+2].x/6;
		return y;
	}
	else if (alpha < 198 && alpha >= 9.9)
	{
		i = (int)(alpha);	
		x = alpha - i;
		if (alpha >= claude_h[i+1].alpha || alpha < claude_h[i].alpha)
			win_printf("extrapolation pb !");		
		y = -  x * (x - 1) * (x - 2) * claude_h[i-1].x/6;
		y +=  (x + 1) * (x - 1) * (x - 2) * claude_h[i].x/2;
		y -=  (x + 1) * x * (x - 2) * claude_h[i+1].x/2;
		y +=  (x + 1) * x * (x - 1) * claude_h[i+2].x/6;
		return y;
	}	
	else return 0;
}


double alpha_versus_extension(double x)
{
	register int i;
	double	x0 , x1, y0 = 0.05, y1, yt = 0, accuracy = 1e-8, xt;
	
	if (x < 0 || x > 1)	return 0;
	y0 = 3 * x/2;
	x0 = extension_versus_alpha(y0);
	for (y1 = 2*y0; (x1 = extension_versus_alpha(y1)) < x; y1 *= 2)
	{
		x0 = x1;
		y0 = y1;
	}
/*	win_printf("x0 = %g x = %g x1 = %g");*/
	for(i = 0, xt = x0; fabs(xt - x) > accuracy && i < 200; i++)
	{
		yt = y0 + (y1 - y0) * (x - x0)/(x1 - x0);
		xt = extension_versus_alpha(yt);
		if (xt > x)
		{
			x1 = xt;
			y1 = yt;
		}
		else
		{
			x0 = xt;
			y0 = yt;		
		}
	}
	if (i == 200)	win_printf("pb of convergence");
	return yt;
}	
double marko_siggia_improved( double x)
{
	double tmp;
	double a_2 = -5.164228e-01, a_3 = -2.737418e+00, a_4 = 1.607497e+01;
	double a_5 = -3.887607e+01, a_6 = 3.949944e+01, a_7 = -1.417718e+01;

	
	tmp = (x -.25 + .25*(1/((1-x)*(1-x))));
	tmp += a_2 * x * x;
	tmp += a_3 * x * x * x;
	tmp += a_4 * x * x * x * x;
	tmp += a_5 * x * x * x * x * x;
	tmp += a_6 * x * x * x * x * x * x;
	tmp += a_7 * x * x * x * x * x * x * x;
	return tmp;
}
double marko_siggia_improved_derivate( double x)
{
	double tmp;
	double a_2 = -5.164228e-01, a_3 = -2.737418e+00, a_4 = 1.607497e+01;
	double a_5 = -3.887607e+01, a_6 = 3.949944e+01, a_7 = -1.417718e+01;

	tmp = (1  + .5*(1/((1-x)*(1-x)*(1-x))));
	tmp += a_2 * 2 * x;
	tmp += a_3 * 3 * x * x;
	tmp += a_4 * 4 * x * x * x;
	tmp += a_5 * 5 * x * x * x * x;
	tmp += a_6 * 6 * x * x * x * x * x;
	tmp += a_7 * 7 * x * x * x * x * x * x;
	return tmp;
}
double extension_versus_kz_over_kx(double r)
{
	register int i;
	double	x0 , x1, y0 = 0.05, y1, yt = 0, accuracy = 1e-8, xt = 0;
	
	if (r < 1)	return 0;
	y0 = 1;
	x0 = 0;
	y1 = r+1;
	x1 = 1-((float)1/y1);
	y1 = x1 * marko_siggia_improved_derivate(x1);	
	y1 /= marko_siggia_improved(x1);
/*	win_printf("y0 = %g y = %g y1 = %g",y0,r,y1);*/
	for(i = 0, yt = y0; fabs(yt - r) > accuracy && i < 200; i++)
	{
/*		win_printf("i = %d y0 = %g y = %g y1 = %g",i,y0,r,y1);*/
	
		xt = x0 + (x1 - x0) * (r - y0)/(y1 - y0);
		yt = xt * marko_siggia_improved_derivate(xt);	
		yt /= marko_siggia_improved(xt);		
		if (yt > r)
		{
			x1 = xt;
			y1 = yt;
		}
		else
		{
			x0 = xt;
			y0 = yt;		
		}
		
	}
	if (i == 200)	win_printf_OK("pb of convergence");
	return xt;
}	

float	find_best_L(d_s *dsi, float T, float xi, float *E)
{	
	register int i;
	float ly, l, x, y, lx;

	for (i = 0; i < dsi->nx; i++)
	{
		x = dsi->xd[i];
		if (x < 0 )	
			return win_printf_OK("cannot fit \n"
			"L = %g at %d is too small or negative!",x,i);
		y = dsi->yd[i];
		if (y <= 0)	
			return win_printf_OK("cannot fit \n"
			"force  = %g at %d is negative!",y,i);

	}	
		
	for (i = 0, l = lx = 0; i < dsi->nx; i++)
	{
		x = dsi->xd[i];
		y = dsi->yd[i] * 1e-12;
		y /= 1.38e-23 * T/(xi*1e-9);
		ly = extension_versus_alpha(y);
		lx += ly*ly;
		l += x*ly;
	}
	l = (lx != 0) ? l/lx : l;

	for (i = 0, *E = 0; i < dsi->nx; i++)
	{
		x = dsi->xd[i];
		y = dsi->yd[i] * 1e-12;
		y /= 1.38e-23 * T/(xi*1e-9);
		ly = extension_versus_alpha(y);
		lx = l*ly - x;
		*E += lx*lx;
	}	
	*E /= dsi->nx;
	return l;	
}

float	find_best_xi(d_s *dsi, float T, float xi, float L, float *E)
{	
	register int i;
	float fy, f, x, y, fx;

	for (i = 0; i < dsi->nx; i++)
	{
		x = dsi->xd[i];
		if (x < 0 || x > L)	
			return win_printf("cannot fit \n"
			"L = %g at %d is too small or negative!",x,i);
		y = dsi->yd[i];
		if (y <= 0)	
			return win_printf("cannot fit \nforce  = %g at %d is negative!",y,i);

	}	
			
	for (i = 0, f = fy = 0; i < dsi->nx; i++)
	{
		x = dsi->xd[i]/L;
		y = dsi->yd[i] * 1e-12;
		y /=  1.38e-23 * T/(xi*1e-9);
		fx = alpha_versus_extension(x);
		if (y < 0)	return win_printf("cannot treat negative force");
		fy += log(y);
		f += log(fx);
	}
	fy -= f;
	fy /= dsi->ny;
	xi = xi / exp(fy);
	for (i = 0, *E = 0; i < dsi->nx; i++)
	{
		x = dsi->xd[i]/L;
		y = dsi->yd[i] * 1e-12;
		y /= 1.38e-23 * T/(xi*1e-9);
		fx = alpha_versus_extension(x);
		f = log(y) - log(fx);
		*E += f*f;
	}	
	*E /= dsi->nx;
	return xi;	
}
int	do_fit_log_xi_auto(void)
{
	register int i;
	char st[256] = {0};
	pltreg *pr = NULL;
	O_p *op = NULL;
	d_s *dsi = NULL, *dsd = NULL;
	float x, y, tmp, E = 0, E1 = 0, dl = .1, lmin;
	int nf = 1024;
	static float T = 298;
	static float xi = 50;	/* nm*/
	static float L = 16.2;	/* microns */
	
	
	
	if(updating_menu_state != 0)	return D_O_K;	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)	
			return win_printf_OK("cannot find data");
	i = win_scanf("Fit the pesistence length\nwith a log scheme\n"
	"Length of	molecule %fpossible \\xi  value %f",&L,&xi);
	if (i == WIN_CANCEL)	return OFF;
	for (i = 0, lmin = 0; i < dsi->nx; i++)
	{
		x = fabs(dsi->xd[i]);
		lmin =	(x > lmin) ? x : lmin;
	}	
	L = (L < lmin) ? lmin +.01 : L;
	
	xi = find_best_xi(dsi,	T,  xi, L, &E);
	for (i = 0; fabs(dl) > .0125 && i < 1024; i++)
	{
		L += dl;
		L = (L < lmin) ? lmin +.01 : L;
		xi = find_best_xi(dsi,	T,  xi, L, &E1);
		if (E1 > E) 
		{
			dl = -dl/2;
		}
		E = E1;
	}
	
	i = win_printf("%d iter \\xi = %g, the energy is %g",i,xi,E);
	if (i == WIN_CANCEL)	return OFF;
	if ((dsd = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
		return win_printf_OK("cannot create data set");
	for (i = 0, tmp = 0.01 ; i < dsd->nx; i++, tmp *= 1.01)	
	{
		dsd->xd[i] = (float)(extension_versus_alpha((double)tmp)*L);
		y = 1.38e-23 * T/(xi*1e-9);
		y *= tmp;
		dsd->yd[i] = y*1e12;
	}		

	
	sprintf(st,"\\fbox{\\pt8\\stack{{\\sl Excact WLM model}"
	"{L =  %g \\mu m}{\\xi = %g nm}}}",L,xi);
	push_plot_label(op, op->x_lo + (op->x_hi - op->x_lo)/4, 
		op->y_hi - (op->y_hi - op->y_lo)/4, st, USR_COORD);	 
	return refresh_plot(pr,pr->cur_op);			
}
float	find_best_xi_at_fixed_L(d_s *dsi, float T, float xi, float L, float *E)
{	
  register int i, j;
  static int debug = 0;
  double fy, f, x, y, fx, er;
  
  for (i = 0; i < dsi->nx; i++)
    {
      x = dsi->xd[i];
      if (x < 0 || x > L)	
	return (debug != WIN_CANCEL) ? debug = win_printf("cannot fit \n"
					      "L = %g at %d is too small or negative!",x,i) : 1;
      y = dsi->yd[i];
      if (y <= 0)	
	return (debug != WIN_CANCEL) ? debug = win_printf("cannot fit \nforce  = %g at %d is negative!",y,i) : 1;

    }	
			
  for (i = 0, f = fy = 0; i < dsi->nx; i++)
    {
      er = (dsi->ye != NULL) ? dsi->ye[i] : dsi->yd[i]/10; 
      x = dsi->xd[i]/L;
      y = dsi->yd[i] * 1e-12;
      y /=  1.38e-23 * T/(xi*1e-9);
      er *= 1e-12;
      er /= 1.38e-23 * T/(xi*1e-9);
      fx = alpha_versus_extension(x);
      if (y < 0)	return (debug != WIN_CANCEL) ? debug = win_printf("cannot treat negative force") : 0;
      if (er > 0)
	{
	  fy += y*fx/(er*er);
	  f += (fx*fx)/(er*er);
	}
    }
  xi = xi * f/fy;
  for (i = 0, j = 0, *E = 0; i < dsi->nx; i++)
    {
      x = dsi->xd[i]/L;
      y = dsi->yd[i];// * 1e-12;
      //y /= 1.38e-23 * T/(xi*1e-9);
      fx = alpha_versus_extension(x);
      fx *= 1.38e-11 * T/(xi*1e-9);
      er = (dsi->ye != NULL) ? dsi->ye[i] : dsi->yd[i]/10; 
      f = y - fx;
      if (er > 0)
	{
	  *E += (f*f)/(er*er);
	  j++;
	}
    }	
  if (j) *E /= j;
  return (float)xi;	
}

int	do_fit_xi_auto_with_er(void)
{
	register int i;
	char st[256] = {0};
	pltreg *pr = NULL;
	O_p *op = NULL;
	d_s *dsi = NULL, *dsd = NULL;
	float x, y, tmp, E = 0, E1 = 0, dl = .1, lmin;
	int nf = 1024;
	static float T = 298;
	static float xi = 50;	/* nm*/
	static float L = 16.2;	/* microns */
	
	
	
	if(updating_menu_state != 0)	return D_O_K;	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)	
			return win_printf_OK("cannot find data");
	i = win_scanf("Fit the pesistence length\nwith a log scheme\n"
	"Length of	molecule %fpossible \\xi  value %f",&L,&xi);
	if (i == WIN_CANCEL)	return OFF;
	for (i = 0, lmin = 0; i < dsi->nx; i++)
	{
		x = fabs(dsi->xd[i]);
		lmin =	(x > lmin) ? x : lmin;
	}	
	L = (L < lmin) ? lmin +.01 : L;
	
	xi = find_best_xi_at_fixed_L(dsi,T,  xi, L, &E);
	for (i = 0; fabs(dl) > .000125 && i < 1024; i++)
	{
		L += dl;
		L = (L < lmin) ? lmin +.01 : L;
		xi = find_best_xi_at_fixed_L(dsi, T,  xi, L, &E1);
		if (E1 > E) 
		{
			dl = -dl/2;
		}
		E = E1;
	}
	
	i = win_printf("%d iter \\xi = %g, the energy is %g",i,xi,E);
	if (i == WIN_CANCEL)	return OFF;
	if ((dsd = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
		return win_printf_OK("cannot create data set");
	for (i = 0, tmp = 0.01 ; i < dsd->nx; i++, tmp *= 1.01)	
	{
		dsd->xd[i] = (float)(extension_versus_alpha((double)tmp)*L);
		y = 1.38e-23 * T/(xi*1e-9);
		y *= tmp;
		dsd->yd[i] = y*1e12;
	}		

	
	sprintf(st,"\\fbox{\\pt8\\stack{{\\sl Excact WLM model}"
	"{L =  %g \\mu m}{\\xi = %g nm}}}",L,xi);
	push_plot_label(op, op->x_lo + (op->x_hi - op->x_lo)/4, 
		op->y_hi - (op->y_hi - op->y_lo)/4, st, USR_COORD);	 
	return refresh_plot(pr,pr->cur_op);			
}

int	do_fit_L(void)
{
	register int i;
	char st[256] = {0};
	pltreg *pr = NULL;
	O_p *op = NULL;
	d_s *dsi = NULL, *dsd = NULL;
	float x, y, tmp, E = 0, lmin;
	int nf = 1024;
	static float T = 298;
	static float xi = 50;	/* nm*/
	static float L = 16.2;	/* microns */
	
	
	
	if(updating_menu_state != 0)	return D_O_K;	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)	return 1;
	i = win_scanf("Fit the molecule length\nPersistence length \\xi %f",&xi);
	if (i == WIN_CANCEL)	return OFF;
	for (i = 0, lmin = 0; i < dsi->nx; i++)
	{
		x = fabs(dsi->xd[i]);
		lmin =	(x > lmin) ? x : lmin;
	}	
	L = (L < lmin) ? lmin +.01 : L;
	
	L = find_best_L(dsi,	T,  xi, &E);
	i = win_printf(" L = %g, the energy is %g (\\xi = %g nm)",L,E,xi);
	if (i == WIN_CANCEL)	return OFF;
	if ((dsd = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
		return win_printf_OK("cannot create data set");
	for (i = 0, tmp = 0.01 ; i < dsd->nx; i++, tmp *= 1.01)	
	{
		dsd->xd[i] = (float)(extension_versus_alpha((double)tmp)*L);
		y = 1.38e-23 * T/(xi*1e-9);
		y *= tmp;
		dsd->yd[i] = y*1e12;
	}		

	
	sprintf(st,"\\fbox{\\pt8\\stack{{\\sl Excact WLM model}"
	"{L =  %g \\mu m}{\\xi = %g nm}}}",L,xi);
	push_plot_label(op, op->x_lo + (op->x_hi - op->x_lo)/4, 
	op->y_hi - (op->y_hi - op->y_lo)/4, st, USR_COORD);	 
	return refresh_plot(pr,pr->cur_op);			
}
	
int	do_fit_L_auto(void)
{
	register int i;
	char st[256] = {0};
	pltreg *pr = NULL;
	O_p *op = NULL;
	d_s *dsi = NULL, *dsd = NULL;
	float x, y, tmp, E = 0, E1 = 0, dxi = 5, lmin;
	int nf = 1024;
	static float T = 298;
	static float xi = 50;	/* nm*/
	static float L = 16.2;	/* microns */
	
	
	
	if(updating_menu_state != 0)	return D_O_K;	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)	return 1;
	i = win_scanf("Fit length of molecule \nand the pesistence length %f"
	"possible \\xi  value",&xi);
	if (i == WIN_CANCEL)	return OFF;
	for (i = 0, lmin = 0; i < dsi->nx; i++)
	{
		x = fabs(dsi->xd[i]);
		lmin =	(x > lmin) ? x : lmin;
	}	
	L = (L < lmin) ? lmin +.01 : L;
	
	L = find_best_L(dsi,	T,  xi, &E);
	for (i = 0; fabs(dxi) > .125 && i < 1024; i++)
	{
		xi += dxi;
		xi = (xi < 1) ? 1 : xi;
		L = find_best_L(dsi, T, xi, &E1);
		if (E1 > E) 
		{
			dxi = -dxi/2;
		}
		E = E1;
	}
	
	i = win_printf("%d iter \\xi = %g,\n L = %g the energy is %g",i,xi,L,E);
	if (i == WIN_CANCEL)	return OFF;
	if ((dsd = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
		return win_printf_OK("cannot create data set");
	for (i = 0, tmp = 0.01 ; i < dsd->nx; i++, tmp *= 1.01)	
	{
		dsd->xd[i] = (float)(extension_versus_alpha((double)tmp)*L);
		y = 1.38e-23 * T/(xi*1e-9);
		y *= tmp;
		dsd->yd[i] = y*1e12;
	}		

	
	sprintf(st,"\\fbox{\\pt8\\stack{{\\sl Excact WLM model}{L =  %g \\mu m}"
	"{\\xi = %g nm}}}",L,xi);
	push_plot_label(op, op->x_lo + (op->x_hi - op->x_lo)/4, 
	op->y_hi - (op->y_hi - op->y_lo)/4, st, USR_COORD);	 
	return refresh_plot(pr,pr->cur_op);			
}

int	do_fit_log_xi(void)
{
	register int i;
	char st[256] = {0};
	pltreg *pr = NULL;
	O_p *op = NULL;
	d_s *dsi = NULL, *dsd = NULL;
	float x, y, tmp, E = 0, lmin;
	int nf = 1024;
	static float T = 298;
	static float xi = 50;	/* nm*/
	static float L = 16.2;	/* microns */
	
	
	
	if(updating_menu_state != 0)	return D_O_K;	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)	return 1;
	i = win_scanf("Fit the pesistence length\nwith a log scheme\n"
	"Length of	molecule %fpossible \\xi  value %f",&L,&xi);
	if (i == WIN_CANCEL)	return OFF;
	for (i = 0, lmin = 0; i < dsi->nx; i++)
	{
		x = fabs(dsi->xd[i]);
		lmin =	(x > lmin) ? x : lmin;
	}	
	L = (L < lmin) ? lmin +.01 : L;
	
	xi = find_best_xi(dsi,	T,  xi, L, &E);
	i = win_printf(" \\xi = %g, the energy is %g",xi,E);
	if (i == WIN_CANCEL)	return OFF;
	if ((dsd = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
		return win_printf_OK("cannot create data set");
	for (i = 0, tmp = 0.01 ; i < dsd->nx; i++, tmp *= 1.01)	
	{
		dsd->xd[i] = (float)(extension_versus_alpha((double)tmp)*L);
		y = 1.38e-23 * T/(xi*1e-9);
		y *= tmp;
		dsd->yd[i] = y*1e12;
	}		

	
	sprintf(st,"\\fbox{\\pt8\\stack{{\\sl Excact WLM model}"
	"{L =  %g \\mu m}{\\xi = %g nm}}}",L,xi);
	push_plot_label(op, op->x_lo + (op->x_hi - op->x_lo)/4, 
	op->y_hi - (op->y_hi - op->y_lo)/4, st, USR_COORD);	 
	return refresh_plot(pr,pr->cur_op);			
}
	

int	do_fit_xi(void)
{
	register int i;
	char st[256] = {0};
	pltreg *pr = NULL;
	O_p *op = NULL;
	d_s *dsi = NULL, *dsd = NULL;
	float fy, f, x, y, fx, tmp;
	int nf = 1024;
	static float T = 298;
	static float xi = 50;	/* nm*/
	static float L = 16.2;	/* microns */
	
	
	
	if(updating_menu_state != 0)	return D_O_K;	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)	return 1;
	i = win_scanf("Fit the pesistence length\n"
	"Length of molecule %fpossible \\xi  value %f",&L,&xi);
	if (i == WIN_CANCEL)	return OFF;
	for (i = 0, f = fy = 0; i < dsi->nx; i++)
	{
		x = dsi->xd[i]/L;
		y = dsi->yd[i] * 1e-12;
		y /= 1.38e-23 * T/(xi*1e-9);
		fx = alpha_versus_extension(x);
		fy += fx*y;
		f += fx*fx;
	}
	fy = (f !=0) ? fy/f: fy;
	xi = (fy !=0) ? xi/fy : xi;
	i = win_printf(" \\xi = %g",xi);
	if (i == WIN_CANCEL)	return OFF;	
	if ((dsd = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
		return win_printf_OK("cannot create data set");
		
	for (i = 0, tmp = 0.01 ; i < dsd->nx; i++, tmp *= 1.01)	
	{
		dsd->xd[i] = (float)(extension_versus_alpha((double)tmp)*L);
		y = 1.38e-23 * T/(xi*1e-9);
		y *= tmp;
		dsd->yd[i] = y*1e12;
	}		

	
	sprintf(st,"\\fbox{\\pt8\\stack{{\\sl Excact WLM model}"
	"{L =  %g \\mu m}{\\xi = %g nm}}}",L,xi);
	push_plot_label(op, op->x_lo + (op->x_hi - op->x_lo)/4, 
	op->y_hi - (op->y_hi - op->y_lo)/4, st, USR_COORD);	 
	return refresh_plot(pr,pr->cur_op);			
}
int	do_give_force(void)
{
	register int i;
	float y, ypn, ys, ypns, ysi, ypnsi, dx, x;
	static float xl = 5;
	static float T = 298;
	static float xi = 50;	/* nm*/
	static float L = 16.2;	/* microns */
	
	if(updating_menu_state != 0)	return D_O_K;	
	i = win_scanf("Extension (in \\mu m)%fLength of molecule (in \\mu m)%f"
	"\\xi  value %ftemperature %f",&xl,&L,&xi,&T);	
	if (i == WIN_CANCEL)	return OFF;
	x = (L != 0) ? xl/L : xl;
	y = alpha_versus_extension(x);
	ys = (x - .25 + .25/((1-x)*(1-x)));
	ysi = marko_siggia_improved(x);
	ypnsi = ypns = ypn = 1.38e-23 * T/(xi*1e-9);
	ypn *= y*1e12;
	ypns *= ys*1e12;
	ypnsi *= ysi*1e12;
	dx = xl * 1e-6 * 1.38e-23 * T/(ypnsi*1e-12);
	dx = sqrt(dx);
	return win_printf_OK("for an extension of %g \\mu m -> relative extension %g \n"
	"the mormalize WLC \\alpha  = %g Siggia's approx = %g\n"
	"Siggia's improved = %g\nthe WLC force is %g pN Siggia's approx = %g\n"
	"Siggia's improved = %g\n brownian mean fluctuation \\delta x = %g (\\mu m)\n"
	,x*L,x,y,ys,ysi,ypn,ypns,ypnsi,dx*1e6);
}
int	do_give_extension(void)
{
	register int i;
	float x, y;
	static float f = 1;
	static float T = 298;
	static float xi = 50;	/* nm*/
	static float L = 16.2;	/* microns */
	
	if(updating_menu_state != 0)	return D_O_K;	
	i = win_scanf("Force (in pN)%fLength of molecule (in \\mu m)%f"
	"\\xi  (in nm) %ftemperature (in K) %f",&f,&L,&xi,&T);	
	if (i == WIN_CANCEL)	return OFF;

	y = f * 1e-12;
	y /= 1.38e-23 * T/(xi*1e-9);
	x = extension_versus_alpha(y);
	
	return win_printf_OK("According to the WLC model\n"
	" a force %g pN applied to a \n"
	"molecule %g \\mu m long\n with a \\xi  = %g (nm) at a temperature of %g K\n"
	" corresponding to a normalized force of %g \n"
	" leads to a relative extension of %g \ncorresponding to %g \\mu m",
	f,L,xi,T,y,x,x*L);
}





int	test_div(void)
{
	register int i, j;
	static int nx = 1024;
	double x, y, tmp;
	pltreg *pr;
	O_p *op;
	d_s *ds, *dsd;
	double F[300] = {0}, l[300] = {0}, FMS; //, *a;
	
	if(updating_menu_state != 0)	return D_O_K;	
	
	if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)	
		return win_printf("cannot find stuff in acreg!");	

	i = win_scanf("nb points %d",&nx);
	if (i == WIN_CANCEL)	return OFF;

	op = create_and_attach_one_plot(pr, nx, nx, 0);
	if (op == NULL)		return win_printf("cannot create plot !");
	ds = op->dat[0];

/*		
	for (i = 0, y = 0.01 ; i < nx; i++, y *= 1.01)	
	{
		x = extension_versus_alpha(y);
		ds->xd[i] = (float)x;
		tmp = 4*(1-x)*(1-x);
		tmp = 1/tmp;
		tmp -= 0.25;
		tmp += x;
		ds->yd[i] = (float)(y-tmp);
	}
*/
		
	for (i = 0; i < nx; i++)	
	{
		x = ((double)i)/nx;
		y = (i == 0) ? 0 : alpha_versus_extension(x);
		ds->xd[i] = (float)x;
		tmp = 4*(1-x)*(1-x);
		tmp = 1/tmp;
		tmp -= 0.25;
		tmp += x;
		ds->yd[i] = (float)(y-tmp);
	}

	
	set_plot_title(op, "\\stack{{Exact WLC model}{- 0.25*(1-x)^{-2}+(1/4) -x}}");
	set_plot_x_title(op, "Allongement");
	set_plot_y_title(op, "Force ");


	nx = 300;
	op = create_and_attach_one_plot(pr, nx, nx, 0);
	if (op == NULL)		return win_printf("cannot create plot !");
	ds = op->dat[0];


	if ((dsd = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
		return win_printf_OK("cannot create data set");



    for (i = 0; i < 100; i++)
      {
	x = l[i] = claude[i].x;
	F[i] = claude[i].alpha;
	FMS = (x == 0) ? 0 : (x -.25 + .25*(1/((1-x)*(1-x))));
	F[i] -= FMS;
	ds->xd[i] = dsd->xd[i] = x;
	ds->yd[i] = F[i];
	dsd->yd[i] = F[i] + 0.75 * x * x;

      }

    for (j = 10; j <= 200; i++, j++)
      {
	x = l[i] = claude_h[j].x;
	F[i] = claude_h[j].alpha;
	FMS = (x == 0) ? 0 : (x -.25 + .25*(1/((1-x)*(1-x))));
	F[i] -= FMS;
	ds->xd[i] = dsd->xd[i] = x;
	ds->yd[i] = F[i];
	dsd->yd[i] = F[i] + 0.75 * x * x;
      }
    /*
    ds->xd[i] = l[i] = 1;
    ds->yd[i] = F[i] = 0;
    i++;
    */
    ds->nx = ds->ny = dsd->nx = dsd->ny = i;


	return refresh_plot(pr,pr->n_op-1);			
}



int	fjc_curve(void)
{
	register int i;
	static int nx = 1024;
	double  tmp, y, y1;
	static float T = 298;
	static float xi = 50;	/* nm*/
	static float L = 16.2;	/* microns */
	pltreg *pr = NULL;
	O_p *op = NULL;
	d_s *ds = NULL;
	
	if(updating_menu_state != 0)	return D_O_K;	
	
	if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)	
		return win_printf("cannot find stuff in acreg!");	
	
	i = win_scanf("Calcul de la courbe de force FJC \n\n"
	" avec	\\xi (en nm) = %f T (en ^{\\pt5 \\oc}) = %f L (rn \\mu m) = %f"
	" avec le nombre de pts %d",&xi,&T,&L,&nx);
	
	if (i == WIN_CANCEL)	return OFF;
	
	op = create_and_attach_one_plot(pr, nx, nx, 0);
	if (op == NULL)		return win_printf("cannot create plot !");
	ds = op->dat[0];

	for (i = 0, tmp = 0.001 ; i < nx; i++, tmp *= 1.01)	
	{
		y = 1.38e-23 * T/(2*xi*1e-9);
		y /= tmp;
		y *= 1e12;
		ds->yd[i] = tmp;
		y1 = (double)1/y;
		if ((i*20)%nx == 0)	win_printf("y = %f y1 = %f",y,y1); 
		ds->xd[i] = L * (cosh(y1)/sinh(y1) - y);
	}
	

	
	set_plot_title(op, "\\stack{{FJC model}"
	"{\\pt8 \\xi = %g nm, T = %g, L = %g \\mu m}}",xi,T,L);
	set_plot_x_title(op, "Allongement (\\mu m)");
	set_plot_y_title(op, "Force (pN)");
	return refresh_plot(pr,pr->n_op-1);			

}


int	moroz_curve(void)
{
	register int i;
	static int nx = 1024;
	double  tmp, y, y1;
	static float T = 298;
	static float xi = 50;	/* nm*/
	static float L = 16.2;	/* microns */
	pltreg *pr = NULL;
	O_p *op = NULL;
	d_s *ds = NULL;
	
	if(updating_menu_state != 0)	return D_O_K;	
	
	if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)	
		return win_printf("cannot find stuff in acreg!");	
	
	i = win_scanf("Calcul de la courbe de force moroz \n\n"
	" avec	\\xi (en nm) = %f T (en ^{\\pt5 \\oc}) = %f"
	" L (rn \\mu m) = %f avec le nombre de pts %d",&xi,&T,&L,&nx);
	
	if (i == WIN_CANCEL)	return OFF;
	
	op = create_and_attach_one_plot(pr, nx, nx, 0);
	if (op == NULL)		return win_printf("cannot create plot !");
	ds = op->dat[0];
	
			
	for (i = 0, tmp = 0.28125 * 1.38e-23 * T/(xi*1e-9); i < nx; i++, tmp *= 1.01)		{
		y = 1.38e-23 * T/(xi*1e-9);
		y = tmp/y;
		ds->yd[i] = tmp  * 1e12;
		y1 = y - (float)1/32;
		y1 = (y1 > 0) ? sqrt(y1) : y1;
		ds->xd[i] = L * (1 - 0.5/y1);
	}
	

	
	set_plot_title(op, "\\stack{{moroz model}"
	"{\\pt8 \\xi = %g nm, T = %g, L = %g \\mu m}}",xi,T,L);
	set_plot_x_title(op, "Allongement (\\mu m)");
	set_plot_y_title(op, "Force (pN)");

	return refresh_plot(pr,pr->n_op-1);			
}
# ifdef ASINH
int	convert_tanh(void)
{
	register int i;
	pltreg *pr = NULL;
	O_p *op = NULL, *opd = NULL;
	d_s *dsi = NULL, *dsd = NULL;
	float tmp;
	int nf;
	static float a = 1;
	static float b = 1;	
	
	
	if(updating_menu_state != 0)	return D_O_K;	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)	return 1;
	i = win_scanf("z = b * asinh (y/a) \n a = %f b = %f",&a,&b);
	if (i == WIN_CANCEL)	return OFF;
	nf = dsi->nx;
	if ((opd = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
		return win_printf_OK("cannot create plot");
	dsd = opd->dat[0];
	for (i = 0; i < nf; i++)	
	{
		dsd->xd[i] = dsi->xd[i];
		tmp = dsi->yd[i]/a;
/*		if (i == 0 || i == nf-1 || (8*i)%nf == 0)	
			win_printf("bef atanh (%g) ",tmp);*/
		dsd->yd[i] = b*(asinh(tmp));
/*		if (i == 0 || i == nf-1 || (8*i)%nf == 0)
			win_printf("after atanh (%g) = %g",tmp,dsd->yd[i]);*/
	}
/*	set_plot_title(op, "\\stack{{FJC model}{\\pt8 \\xi = %g nm, T = %g, L = %g \\mu m}}",xi,T,L); */
	set_plot_x_title(opd, op->x_title);
	set_plot_y_title(opd, "cotanh");
	return refresh_plot(pr,pr->n_op-1);			
}
# endif 
int	xi_effectif_vs_z(void)
{
	register int i;
	pltreg *pr = NULL;
	O_p *op = NULL, *opd = NULL;
	d_s *dsi = NULL, *dsd = NULL;
	float tmp;
	int nf;
	static float L = 16.2;
	static float K = 1000;	
	static float T = 298;	
	
	
	
	if(updating_menu_state != 0)	return D_O_K;	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)	return 1;
	i = win_scanf("compute effective \\xi vs (z/L - F/K0) \n"
	"L_0 (\\mu m) = %f K_0 (pN) = %fT (K) %f",&L,&K,&T);
	if (i == WIN_CANCEL)	return OFF;
	nf = dsi->nx;
	if ((opd = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
		return win_printf_OK("cannot create plot");
	dsd = opd->dat[0];
	for (i = 0; i < nf; i++)	
	{
		dsd->xd[i] = dsi->xd[i];
		tmp = dsi->xd[i]/L;
		tmp -= dsi->yd[i]/K;
		dsd->xd[i] = tmp;
		dsd->yd[i] = (dsi->yd[i] > 0) ? (1.38e-2*T/dsi->yd[i])*
		marko_siggia_improved(tmp) : 0;

	}
	set_plot_title(opd, "\\stack{{Effective \\xi}"
	"{\\pt8 K = %g, T = %g, L = %g \\mu m}}",K,T,L); 
	set_plot_x_title(opd, "Relative extension");
	set_plot_y_title(opd, "\\xi_{eff} (nm)");
	return refresh_plot(pr,pr->n_op-1);			
}
int	xi_effectif_vs_f(void)
{
	register int i;
	pltreg *pr = NULL;
	O_p *op = NULL, *opd = NULL;
	d_s *dsi = NULL, *dsd = NULL;
	float tmp;
	int nf;
	static float L = 16.2;
	static float K = 1000;	
	static float T = 298;	
	
	
	
	if(updating_menu_state != 0)	return D_O_K;	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)	return 1;
	i = win_scanf("compute effective \\xi vs (z/L - F/K0) \n"
	" L_0 (\\mu m) = %f K_0 (pN) = %fT (K) %f",&L,&K,&T);
	if (i == WIN_CANCEL)	return OFF;
	nf = dsi->nx;
	if ((opd = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
		return win_printf_OK("cannot create plot");
	dsd = opd->dat[0];
	for (i = 0; i < nf; i++)	
	{
		dsd->xd[i] = dsi->yd[i];
		tmp = dsi->xd[i]/L;
		tmp -= dsi->yd[i]/K;
		dsd->xd[i] = dsi->yd[i]; /* tmp; */
		dsd->yd[i] = (dsi->yd[i] > 0) ? (1.38e-2*T/dsi->yd[i])*
		marko_siggia_improved(tmp) : 0;

	}
	set_plot_title(opd, "\\stack{{Effective \\xi}"
	"{\\pt8 K = %g, T = %g, L = %g \\mu m}}",K,T,L); 
	set_plot_x_title(opd, "Force");
	set_plot_y_title(opd, "\\xi_{eff} (nm)");
	return refresh_plot(pr,pr->n_op-1);			
}
int	fit_kz_over_kx(void)
{
	register int i;
	static float kz = 2e-8, erz = 5, kx = 1e-8, erx = 10, l = 5, T = 298;
	float xi, r, ex, exp, exm, xip, xim;
	
	if(updating_menu_state != 0)	return D_O_K;	
	i = win_scanf("Find  L_o from {\\sl kz/kx} and then \\xi \n"
	"{\\sl kz} in N/m %frelative error on {\\sl kz} in %% %f"
	"{\\sl kx} in N/m %frelative error on {\\sl kx} in %% %f"
	"extension of DNA molecule (\\mu m)%ftemperatue (Kelvin)%f",
	&kz,&erz,&kx,&erx,&l,&T);
	if (i == WIN_CANCEL)	return OFF;
	if (kx <= 0)	return win_printf_OK("kx = %g cannot be zero or negative!",kx);
	if (kz <= kx)	return win_printf_OK("kz = %g must be greater than kx = %g!",kz,kx);
	r = kz/kx;
	erx /= 100;
	erz /= 100;
	ex = extension_versus_kz_over_kx((double)r);
	exp = extension_versus_kz_over_kx((double)r*(1+erx+erz));
	exm = extension_versus_kz_over_kx((double)r*(1-erx-erz));
	xi = 1.38e-23 * T * marko_siggia_improved(ex)/(kx * l * 1e-6);
	xi *= 1e9;
	xip = 1.38e-23 * T * marko_siggia_improved(exm)/(kx *(1+erx)* l * 1e-6);
	xip *= 1e9;
	xim = 1.38e-23 * T * marko_siggia_improved(exp)/(kx *(1-erx)* l * 1e-6);
	xim *= 1e9;	
	erx *= 100;
	erz *= 100;	
	return win_printf_OK("xi_- = %g  xi = %g xi_+ %g(nm)\n"
	"l_{0-} = %g l_0 = %g l_{0+} = %g (\\mu m)",xim,xi,xip,l/exm,l/ex,l/exp); 
}



double marko_siggia_improved_plus_enthalpy(double l, double xi, double Lo, double T, double Ko)
{
	register int i;
	double x, F, y, fo = 0, y1, f1;
	double kbt;
	
	kbt = 1.38062e-2 * T; /* pN  et nm*/
	if (xi != 0)	kbt /= xi;
	x = l/Lo;
	if (x < 0.98)
	{
		y1 = x;
		f1 = F = kbt * marko_siggia_improved(y1);
	}
	else
	{
		f1 = F = Ko * (x - .98);
		F =  kbt * marko_siggia_improved(.98);
		if (f1 < F)	f1 = F;
	}
	for (i = 0; i < 32; i++)
	{
		y = x - (fo + f1)/(2*Ko);
		if (y >= 1)
		{
			fo = (fo + f1)/2;
		}
		else if (y < 0)
		{
			f1 = (fo + f1)/2;
		}
		else
		{
			F = kbt * marko_siggia_improved(y);
/*			win_printf("iter %d x = %g F = %g",i,x,F);*/
			if (2*F > fo+f1 ) 
			{
				fo = (fo + f1)/2;
			}
			else
			{
				f1 = (fo + f1)/2;		
			}
		}
	}
/*	win_printf("iter %d x = %g F = %g",i,x,F);*/
	return F;
}


double marko_siggia( double x)
{
	double tmp;

	
	tmp = (x -.25 + .25*(1/((1-x)*(1-x))));
	return tmp;
}
double marko_siggia_derivate( double x)
{
	double tmp;

	tmp = (1  + .5*(1/((1-x)*(1-x)*(1-x))));
	return tmp;
}
double marko_siggia_plus_enthalpy(double l, double xi, double Lo, double T, double Ko)
{
	register int i;
	double x, F, y, fo = 0, y1, f1;
	double kbt;
	
	kbt = 1.38062e-2 * T; /* pN  et nm*/
	if (xi != 0)	kbt /= xi;
	x = l/Lo;
	if (x < 0.98)
	{
		y1 = x;
		f1 = F = kbt * marko_siggia(y1);
	}
	else
	{
		f1 = F = Ko * (x - .98);
		F =  kbt * marko_siggia(.98);
		if (f1 < F)	f1 = F;
	}
	for (i = 0; i < 32; i++)
	{
		y = x - (fo + f1)/(2*Ko);
		if (y >= 1)
		{
			fo = (fo + f1)/2;
		}
		else if (y < 0)
		{
			f1 = (fo + f1)/2;
		}
		else
		{
			F = kbt * marko_siggia(y);
/*			win_printf("iter %d x = %g F = %g",i,x,F);*/
			if (2*F > fo+f1 ) 
			{
				fo = (fo + f1)/2;
			}
			else
			{
				f1 = (fo + f1)/2;		
			}
		}
	}
/*	win_printf("iter %d x = %g F = %g",i,x,F);*/
	return F;
}

int	test_enthalpy(void)
{
	register int i;
	static float l = .5, xi = 50, Lo = 1, T = 298, Ko = 1200;
	static int nx = 1024;
	pltreg *pr = NULL;
	O_p *op = NULL;
	d_s *ds = NULL;
	double x;
	
	if(updating_menu_state != 0)	return D_O_K;	
	
	if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)	
		return win_printf("cannot find stuff in acreg!");	
	
	i = win_scanf("\\xi %f Lo %f, Ko %f, l_{max} %fnx %d",&xi,&Lo,&Ko,&l,&nx);
	if (i == WIN_CANCEL)	return OFF;

	op = create_and_attach_one_plot(pr, nx, nx, 0);
	if (op == NULL)		return win_printf("cannot create plot !");
	ds = op->dat[0];

	
	for (i = 0; i < nx; i++)	
	{
		x = ((double)i)/nx;
		x *= l;
		ds->xd[i] = (float)x;
		ds->yd[i] = (float)marko_siggia_improved_plus_enthalpy( x, xi, Lo, T, Ko);
	}

	
	set_plot_title(op, "enthalpic model");
	set_plot_x_title(op, "Allongement");
	set_plot_y_title(op, "Force ");

	return refresh_plot(pr,pr->n_op-1);			
}
void siggia_func_improve_and_enthalpy(float l, float *a, float *y, float *dyda, int ma)
{
	double  xi, Lo, Ko, dydxi, dydL, dydK;
	double kbt, T = 298, tmp;

	(void)ma;
	kbt = 1.38062e-2 * T;
	xi = a[1];
	Lo = a[2];
	Ko = a[3];
	*y = marko_siggia_improved_plus_enthalpy( l, xi, Lo, T, Ko);
	
	dydxi = -*y/xi;
	tmp = (kbt/xi ) * marko_siggia_improved_derivate((l/Lo) - (*y/Ko));
	tmp /= Ko;
	tmp += 1;
	dydxi /= tmp;
		

	dydL = -(kbt/xi) * marko_siggia_improved_derivate((l/Lo) - (*y/Ko));
	dydL *= l;
	dydL /= Lo*Lo;
	dydL /= tmp;
	
	dydK = (kbt/xi) * marko_siggia_improved_derivate((l/Lo) - (*y/Ko));
	dydK *= *y;
	dydK /= Ko * Ko;
	dydK /= tmp;
	
	
	dyda[1] = dydxi;
	dyda[2] = dydL;
	dyda[3] = dydK;
}

void siggia_func_and_enthalpy(float l, float *a, float *y, float *dyda, int ma)
{
	double  xi, Lo, Ko, dydxi, dydL, dydK;
	double kbt, T = 298, tmp;

	(void)ma;	
	kbt = 1.38062e-2 * T;
	xi = a[1];
	Lo = a[2];
	Ko = a[3];
	*y = marko_siggia_plus_enthalpy( l, xi, Lo, T, Ko);
	
	dydxi = -*y/xi;
	tmp = (kbt/xi ) * marko_siggia_derivate((l/Lo));
	tmp /= Ko;
	tmp += 1;
	dydxi /= tmp;
		

	dydL = -(kbt/xi) * marko_siggia_derivate((l/Lo));
	dydL *= l;
	dydL /= Lo*Lo;
	dydL /= tmp;
	
	dydK = (kbt/xi) * marko_siggia_derivate((l/Lo));
	dydK *= *y;
	dydK /= Ko * Ko;
	dydK /= tmp;
	
	
	dyda[1] = dydxi;
	dyda[2] = dydL;
	dyda[3] = dydK;
}
void siggia_func_improve(float l, float *a, float *y, float *dyda, int ma)
{
	double x, xi, L, dydxi, dydL;
	double kbt, T = 298, tmp;
	double a_2 = -5.164228e-01, a_3 = -2.737418e+00, a_4 = 1.607497e+01;
	double a_5 = -3.887607e+01, a_6 = 3.949944e+01, a_7 = -1.417718e+01;

	(void)ma;	
	kbt = 1.38062e-2 * T; /* pN  et nm*/
	xi = a[1];
	L = a[2];
	x = l/L;
	tmp = (x -.25 + .25*(1/((1-x)*(1-x))));
	tmp += a_2 * x * x;
	tmp += a_3 * x * x * x;
	tmp += a_4 * x * x * x * x;
	tmp += a_5 * x * x * x * x * x;
	tmp += a_6 * x * x * x * x * x * x;
	tmp += a_7 * x * x * x * x * x * x * x;
	*y = (kbt/xi) * tmp;
	
	dydxi = -(kbt/(xi*xi)) * tmp;
	tmp = (1  + .5*(1/((1-x)*(1-x)*(1-x))));
	tmp += a_2 * 2 * x;
	tmp += a_3 * 3 * x * x;
	tmp += a_4 * 4 * x * x * x;
	tmp += a_5 * 5 * x * x * x * x;
	tmp += a_6 * 6 * x * x * x * x * x;
	tmp += a_7 * 7 * x * x * x * x * x * x;
	dydL = -(kbt/xi) * tmp * (l/(L*L));
	dyda[1] = dydxi;
	dyda[2] = dydL;
}
double marko_siggia_w3( double x)
{
	double tmp;
	double a_2 = -0.75;
	
	tmp = (x -.25 + .25*(1/((1-x)*(1-x))));
	tmp += a_2 * x * x;
	return tmp;
}
double marko_siggia_w3_derivate( double x)
{
	double tmp;
	double a_2 = -0.75;

	tmp = (1  + .5*(1/((1-x)*(1-x)*(1-x))));
	tmp += a_2 * 2 * x;
	return tmp;
}

void siggia_func_w3(float l, float *a, float *y, float *dyda, int ma)
{
	double x, xi, L, dydxi, dydL;
	double kbt, T = 298, tmp;
	double a_2 = -.75;

	(void)ma;	
	kbt = 1.38062e-2 * T; /* pN  et nm*/
	xi = a[1];
	L = a[2];
	x = l/L;
	tmp = (x -.25 + .25*(1/((1-x)*(1-x))));
	tmp += a_2 * x * x;
	*y = (kbt/xi) * tmp;
	
	dydxi = -(kbt/(xi*xi)) * tmp;
	tmp = (1  + .5*(1/((1-x)*(1-x)*(1-x))));
	tmp += a_2 * 2 * x;
	dydL = -(kbt/xi) * tmp * (l/(L*L));
	dyda[1] = dydxi;
	dyda[2] = dydL;
}
void siggia_func(float l, float *a, float *y, float *dyda, int ma)
{
	float x, xi, L, dydxi, dydL;
	float kbt, T = 298;
	
	(void)ma;	
	kbt = 1.38062e-2 * T; /* pN  et nm*/
	xi = a[1];
	L = a[2];
	x = l/L;
	*y = (kbt/xi) * (x -.25 + .25*(1/((1-x)*(1-x))));
	dydxi = -(kbt/(xi*xi)) * (x -.25 + .25*(1/((1-x)*(1-x))));
	dydL = -(kbt/xi) * (1  + .5*(1/((1-x)*(1-x)*(1-x)))) *(l/(L*L));
	dyda[1] = dydxi;
	dyda[2] = dydL;
}

int	do_fit_Levenberg_Marquardt(void)
{
    register int i;
    char st[256] = {0};
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *dsi = NULL, *dsd = NULL;
    float x, y, lmin;
    int nf = 1024;
    static float T = 298, error = .1;
    static float xi = 50;	/* nm*/
    static float L = 20;	/* microns */
    static int flag = 1;
    float a[2] = {0}, chisq, chisq0, alamda, *sig  = NULL, **covar = NULL, **alpha = NULL; 
    int ia[2] = {0}, nder = 1;	
    //float gammq(float a, float x);
	
	
    if(updating_menu_state != 0)	return D_O_K;	
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)	return 1;
    i = win_scanf("Fit length of molecule \n"
		  "and the pesistence length possible L value %f"
		  "\\xi  value %ferror type, const -> 0 relative 1 "
		  "error in data set ->2 %derror %f",&L,&xi,&flag,&error);
    if (i == WIN_CANCEL)	return OFF;
    for (i = 0, lmin = 0; i < dsi->nx; i++)
      {
	x = fabs(dsi->xd[i]);
	lmin =	(x > lmin) ? x : lmin;
      }	
    L = (L < lmin) ? lmin +.01 : L;
	


    ia[0] = ia[1] = 1;
    a[0] = xi;
    a[1] = L;
    covar = matrix(1, 2, 1, 2);
    alpha = matrix(1, 2, 1, 2);
    sig = vector(1, dsi->nx);
    for (i = 1; i <= dsi->nx; i++)
      sig[i] = (flag) ? dsi->yd[i-1] * error : error;
    if (flag == 2)
      {
	i = win_scanf("enter index of error data set %d",&nder);
	if (i == WIN_CANCEL)	return OFF;
	if (op->dat[nder]->nx != dsi->nx)	
	  return win_printf("your data sets differs in size!");
	for (i = 1; i <= dsi->nx; i++)
	  sig[i] =  op->dat[nder]->yd[i-1];
      }				
    alamda = -1;

    //mrqmin(dsi->xd-1, dsi->yd-1, sig, dsi->nx, a-1, ia-1,2, covar, alpha, &chisq, siggia_func, &alamda);
    mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, a, ia,2, covar, alpha, &chisq, siggia_func, &alamda);
    /*
      win_printf("%d iter \\xi = %g,\n L = %g the \\chi^2  is %g\n\\lambda = %g",-2,a[0],a[1],chisq,alamda);
    */
    chisq0 = chisq;
    //mrqmin(dsi->xd-1, dsi->yd-1, sig, dsi->nx, a-1, ia-1,2, covar, alpha, &chisq, siggia_func, &alamda);
    mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, a, ia,2, covar, alpha, &chisq, siggia_func, &alamda);
    /*
      win_printf("%d iter \\xi = %g,\n L = %g the \\chi^2  is %g\n\\lambda = %g",-1,a[0],a[1],chisq,alamda);
    */
    for (i = 0 ; (chisq0 - chisq <= 0) || (chisq0 - chisq > 1e-2); i++)
      {
	chisq0 = chisq;
	//mrqmin(dsi->xd-1, dsi->yd-1, sig, dsi->nx, a-1, ia-1,2, covar, alpha, &chisq, siggia_func, &alamda);
	mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, a, ia,2, covar, alpha, &chisq, siggia_func, &alamda);
	/*
	  j = win_printf("%d iter \\xi = %g,\n L = %g the \\chi^2  is %g\n\\lambda = %g",i,a[0],a[1],chisq,alamda);
	  if (j == WIN_CANCEL)	break;
	*/
	if (i >= 10000) break;
	if (alamda > 1e12) break;
      }
	
    alamda = 0;
    //mrqmin(dsi->xd-1, dsi->yd-1, sig, dsi->nx, a-1, ia-1,2, covar, alpha, &chisq, siggia_func, &alamda);
    mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, a, ia,2, covar, alpha, &chisq, siggia_func, &alamda);	
    i = win_printf("%d iter \\xi = %g,\n L = %g the \\chi^2  is %g\n"
		   " covar \\xi\\xi %g \\xi L %g\nL \\xi %g LL %g\n"
		   "curvature \\xi \\xi %g \\xi L %g\n L \\xi %g LL %g",i,a[0],a[1],chisq,
		   covar[1][1],covar[1][2],covar[2][1],covar[2][2],alpha[1][1],alpha[1][2],
		   alpha[2][1],alpha[2][2]);
    xi = a[0];
    L = a[1];
    if (i == WIN_CANCEL)	return OFF;
    if (dsi->nx > 256)	nf = dsi->nx;
    if ((dsd = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
      return win_printf("cannot create data set");
    if (dsi->nx > 256)	
      {
	for (i = 0; i < dsd->nx && i < dsi->nx; i++)	
	  {
	    x = dsi->xd[i]/L;
	    dsd->xd[i] = dsi->xd[i];
	    y = 1.38e-23 * T/(xi*1e-9);
	    y *= (x -.25 + .25*(1/((1-x)*(1-x))));
	    dsd->yd[i] = y*1e12;
	  }
      }
    else
      {		
	for (i = 0; i < dsd->nx; i++)	
	  {
	    x = (float)i/nf;
	    dsd->xd[i] = x*L;
	    y = 1.38e-23 * T/(xi*1e-9);
	    y *= (x -.25 + .25*(1/((1-x)*(1-x))));
	    dsd->yd[i] = y*1e12;
	  }		
      }

	
    sprintf(st,"\\fbox{\\pt8\\stack{{\\sl Marko-Siggia model}"
	    "{L =  %g \\mu m +/- %g}{\\xi = %g nm +/- %g}{\\chi^2 = %g n = %d}{p = %g}}}"
	    ,L,sqrt(covar[2][2]),xi,sqrt(covar[1][1]),chisq,dsi->nx-2,
	    gammq((float)(dsi->nx-2)/2, chisq/2));
    push_plot_label(op, op->x_lo + (op->x_hi - op->x_lo)/4, 
		    op->y_hi - (op->y_hi - op->y_lo)/4, st, USR_COORD);
    free_vector(sig, 1, dsi->nx);	
    free_matrix(alpha, 1, 2, 1, 2);
    free_matrix(covar, 1, 2, 1, 2);
    return refresh_plot(pr,pr->cur_op);			
}
int	do_fit_Levenberg_Marquardt_improve(void)
{
    register int i;
    char st[256] = {0};
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *dsi = NULL, *dsd = NULL;
    float x, y, lmin;
    int nf = 1024;
    static float T = 298, error = .1;
    static float xi = 50;	/* nm*/
    static float L = 20, maxF = 50;	/* microns */
    static int flag = 1;
    float a[2] = {0}, chisq, chisq0, alamda, *sig = NULL, **covar = NULL, **alpha = NULL; 
    int ia[2] = {0}, nder = 1;	
    //float gammq(float a, float x);
    //double marko_siggia_improved( double x);
    
    if(updating_menu_state != 0)	return D_O_K;	
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)	return 1;
    ia[0] = ia[1] = 1;

    if (dsi->ye == NULL)
      {
	i = win_scanf("Fit length of molecule \n"
		      "and the persistence length \npossible L value %8f (fit this parameter %b)\n"
		      "\\xi  value  %8f (fit this parameter %b)\n"
		      "error type, const -> 0 relative 1 error in data set ->2 %derror %f"
		      ,&L,ia+1,&xi,ia,&flag,&error);
	if (i == WIN_CANCEL)	return OFF;
      }
    else
      {
		i = win_scanf("Fit length of molecule \n"
			      "and the persistence length \npossible L value %8f (fit this parameter %b)\n"
			      "\\xi  value  %8f (fit this parameter %b)\n"
			      "error is taken from the erro bars",
			      &L,ia+1,&xi,ia);
		if (i == WIN_CANCEL)	return OFF;
      }	
	
    for (i = 0, lmin = 0, maxF = dsi->yd[0]; i < dsi->nx; i++)
      {
	x = fabs(dsi->xd[i]);
	lmin =	(x > lmin) ? x : lmin;
	maxF = (dsi->yd[i] > maxF) ? dsi->yd[i] : maxF;
	
      }	
    L = (L < lmin) ? lmin +.01 : L;
    
    a[0] = xi;
    a[1] = L;
    covar = matrix(1, 2, 1, 2);
    alpha = matrix(1, 2, 1, 2);
    sig = vector(1, dsi->nx);
    
    if (dsi->ye == NULL)
      {
	for (i = 1; i <= dsi->nx; i++)
	  sig[i] = (flag) ? dsi->yd[i-1] * error : error;
	
	if (flag == 2)
	  {
	    i = win_scanf("enter index of error data set %d",&nder);
	    if (i == WIN_CANCEL)	return OFF;
	    if (op->dat[nder]->nx != dsi->nx)	
	      return win_printf("your data sets differs in size!");
	    for (i = 1; i <= dsi->nx; i++)
	      sig[i] =  op->dat[nder]->yd[i-1];
	  }		
	
      }
    else
      {
	for (i = 1; i <= dsi->nx; i++)
	  sig[i] = dsi->ye[i-1];	
      }
    
    
    alamda = -1;
    mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, a, ia,2, covar, alpha, &chisq, 
	     siggia_func_improve, &alamda);
/*
	win_printf("%d iter \\xi = %g,\n L = %g the \\chi^2  is %g\n\\lambda = %g",-2,a[0],a[1],chisq,alamda);
*/
    chisq0 = chisq;
    mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, a, ia,2, covar, alpha, &chisq, 
	     siggia_func_improve, &alamda);
/*
	win_printf("%d iter \\xi = %g,\n L = %g the \\chi^2  is %g\n\\lambda = %g",-1,a[0],a[1],chisq,alamda);
*/
    for (i = 0 ; (chisq0 - chisq <= 0) || (chisq0 - chisq > 1e-2); i++)
      {
	chisq0 = chisq;
	mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, a, ia,2, covar, alpha, 
		 &chisq, siggia_func_improve, &alamda);
	/*
	  j = win_printf("%d iter \\xi = %g,\n L = %g the \\chi^2  is %g\n\\lambda = %g",i,a[0],a[1],chisq,alamda);
	  if (j == WIN_CANCEL)	break;
	*/
	if (i >= 10000) break;
	if (alamda > 1e12) break;
      }
    
    alamda = 0;
    mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, a, ia,2, covar, alpha, 
	     &chisq, siggia_func_improve, &alamda);	
    i = win_printf("%d iter \\xi = %g,\n L = %g the \\chi^2  is %g\n"
		   " covar \\xi\\xi %g \\xi L %g\nL \\xi %g LL %g\ncurvature \\xi \\xi %g"
		   " \\xi L %g\n L \\xi %g LL %g",i,a[0],a[1],chisq,covar[1][1],covar[1][2],
		   covar[2][1],covar[2][2],alpha[1][1],alpha[1][2],alpha[2][1],alpha[2][2]);
    xi = a[0];
    L = a[1];
    if (i == WIN_CANCEL)	return OFF;
    if (dsi->nx > 256)	nf = dsi->nx;
    if ((dsd = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
      return win_printf("cannot create data set");
    if (dsi->nx > 256)	
      {
	for (i = 0; i < dsd->nx && i < dsi->nx; i++)	
	  {
	    x = dsi->xd[i]/L;
	    dsd->xd[i] = dsi->xd[i];
	    y = 1.38e-23 * T/(xi*1e-9);
	    y *= marko_siggia_improved( x);
	    dsd->yd[i] = y*1e12;
	  }
      }
    else
      {		
	for (i = 0; i < dsd->nx; i++)	
	  {
	    x = (float)i/nf;
	    dsd->xd[i] = x*L;
	    y = 1.38e-23 * T/(xi*1e-9);
	    y *= marko_siggia_improved( x);
	    dsd->yd[i] = y*1e12;
	    if (dsd->yd[i] > (1.5*maxF))
	      { // we limit force to 50 pN
		dsd->nx = dsd->ny = i;
	      }
	  }		
      }
    
    
    sprintf(st,"\\fbox{\\pt8\\stack{{\\sl Marko-Siggia improved}"
	    "{L =  %g \\mu m +/- %g}{\\xi = %g nm +/- %g}{\\chi^2 = %g n = %d}{p = %g}}}"
	    ,L,sqrt(covar[2][2]),xi,sqrt(covar[1][1]),chisq,dsi->nx-2,
	    gammq((float)(dsi->nx-2)/2, chisq/2));
    push_plot_label(op, op->x_lo + (op->x_hi - op->x_lo)/4, 
		    op->y_hi - (op->y_hi - op->y_lo)/4, st, USR_COORD);
    free_vector(sig, 1, dsi->nx);	
    free_matrix(alpha, 1, 2, 1, 2);
    free_matrix(covar, 1, 2, 1, 2);
    return refresh_plot(pr,pr->cur_op);			
}
int	do_fit_Levenberg_Marquardt_w3(void)
{
    register int i;
    char st[256] = {0};
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *dsi = NULL, *dsd = NULL;
    float x, y, lmin;
    int nf = 1024;
    static float T = 298, error = .1;
    static float xi = 50;	/* nm*/
    static float L = 20;	/* microns */
    static int flag = 1;
    float a[2] = {0}, chisq, chisq0, alamda, *sig = NULL, **covar = NULL, **alpha = NULL; 
    int ia[2] = {0}, nder = 1;	
    //float gammq(float a, float x);
    //double marko_siggia_w3( double x);
	
    if(updating_menu_state != 0)	return D_O_K;	
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)	return 1;
    ia[0] = ia[1] = 1;


    if (dsi->ye == NULL)
      {
	i = win_scanf("W3 approximation. Fit length of molecule \n"
		      "and the persistence length \npossible L value %8f (fit this parameter %b)\n"
		      "\\xi  value  %8f (fit this parameter %b)\n"
		      "error type, const -> 0 relative 1 error in data set ->2 %derror %f",
		      &L,ia+1,&xi,ia,&flag,&error);
	if (i == WIN_CANCEL)	return OFF;
      }
    else
      {
	i = win_scanf("W3 approximation. Fit length of molecule \n"
		      "and the persistence length \npossible L value %8f (fit this parameter %b)\n"
		      "\\xi  value  %8f (fit this parameter %b)\n"
		      "error is taken from the erro bars",
		      &L,ia+1,&xi,ia);
	if (i == WIN_CANCEL)	return OFF;
      }	
    for (i = 0, lmin = 0; i < dsi->nx; i++)
      {
	x = fabs(dsi->xd[i]);
	lmin =	(x > lmin) ? x : lmin;
      }	
    L = (L < lmin) ? lmin +.01 : L;
    a[0] = xi;
    a[1] = L;

    covar = matrix(1, 2, 1, 2);
    alpha = matrix(1, 2, 1, 2);
    sig = vector(1, dsi->nx);
	
    if (dsi->ye == NULL)
      {
	for (i = 1; i <= dsi->nx; i++)
	  sig[i] = (flag) ? dsi->yd[i-1] * error : error;
		
	if (flag == 2)
	  {
	    i = win_scanf("enter index of error data set %d",&nder);
	    if (i == WIN_CANCEL)	return OFF;
	    if (op->dat[nder]->nx != dsi->nx)	
	      return win_printf("your data sets differs in size!");
	    for (i = 1; i <= dsi->nx; i++)
	      sig[i] =  op->dat[nder]->yd[i-1];
	  }		
	
      }
    else
      {
	for (i = 1; i <= dsi->nx; i++)
	  sig[i] = dsi->ye[i-1];	
      }
		

    alamda = -1;
    mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, a, ia, 2, covar, alpha, &chisq, 
	   siggia_func_w3, &alamda);

    chisq0 = chisq;
    mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, a, ia,2, covar, alpha, &chisq, 
	   siggia_func_w3, &alamda);

    for (i = 0 ; (chisq0 - chisq <= 0) || (chisq0 - chisq > 1e-2); i++)
      {
	chisq0 = chisq;
	mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, a, ia,2, covar, alpha, 
	       &chisq, siggia_func_w3, &alamda);
	if (i >= 10000) break;
	if (alamda > 1e12) break;
      }
	
    alamda = 0;
    mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, a, ia,2, covar, alpha, 
	   &chisq, siggia_func_w3, &alamda);	
    i = win_printf("%d iter \\xi = %g,\n L = %g the \\chi^2  is %g\n"
		   " covar \\xi\\xi %g \\xi L %g\nL \\xi %g LL %g\ncurvature \\xi \\xi %g"
		   " \\xi L %g\n L \\xi %g LL %g",i,a[0],a[1],chisq,covar[1][1],covar[1][2],
		   covar[2][1],covar[2][2],alpha[1][1],alpha[1][2],alpha[2][1],alpha[2][2]);
    xi = a[0];
    L = a[1];
    if (i == WIN_CANCEL)	return OFF;
    if (dsi->nx > 256)	nf = dsi->nx;
    if ((dsd = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
      return win_printf("cannot create data set");
    if (dsi->nx > 256)	
      {
	for (i = 0; i < dsd->nx && i < dsi->nx; i++)	
	  {
	    x = dsi->xd[i]/L;
	    dsd->xd[i] = dsi->xd[i];
	    y = 1.38e-23 * T/(xi*1e-9);
	    y *= marko_siggia_w3( x);
	    dsd->yd[i] = y*1e12;
	  }
      }
    else
      {		
	for (i = 0; i < dsd->nx; i++)	
	  {
	    x = (float)i/nf;
	    dsd->xd[i] = x*L;
	    y = 1.38e-23 * T/(xi*1e-9);
	    y *= marko_siggia_w3( x);
	    dsd->yd[i] = y*1e12;
	  }		
      }

	
    sprintf(st,"\\fbox{\\pt8\\stack{{\\sl Marko-Siggia W3 approx}"
	    "{L =  %g \\mu m +/- %g}{\\xi = %g nm +/- %g}{\\chi^2 = %g n = %d}{p = %g}}}"
	    ,L,sqrt(covar[2][2]),xi,sqrt(covar[1][1]),chisq,dsi->nx-2,
	    gammq((float)(dsi->nx-2)/2, chisq/2));
    push_plot_label(op, op->x_lo + (op->x_hi - op->x_lo)/4, 
		    op->y_hi - (op->y_hi - op->y_lo)/4, st, USR_COORD);
    free_vector(sig, 1, dsi->nx);	
    free_matrix(alpha, 1, 2, 1, 2);
    free_matrix(covar, 1, 2, 1, 2);
    return refresh_plot(pr,pr->cur_op);			
}
int	do_fit_Levenberg_Marquardt_enthalpy(void)
{
    register int i;
    char st[256] = {0};
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *dsi = NULL, *dsd = NULL;
    float x, lmin;
    int nf = 1024;
    static float T = 298, error = .1;
    static float xi = 50;	/* nm*/
    static float L = 20;	/* microns */
    static float Ko = 1200; /* pN*/
    static int flag = 1;
    float a[3] = {0}, chisq, chisq0, alamda, *sig = NULL, **covar = NULL, **alpha = NULL; 
    int ia[3] = {0}, nder;	
    //float gammq(float a, float x);
	
    if(updating_menu_state != 0)	return D_O_K;	
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)	return 1;
    i = win_scanf("Fit length of molecule \n"
		  "and the pesistence length and stiffness \npossible L value %f"
		  "\\xi  value %fKo value %f error type, const -> 0 relative 1 "
		  "other data set 2%derror %f",&L,&xi,&Ko,&flag,&error);
    if (i == WIN_CANCEL)	return OFF;
    for (i = 0, lmin = 0; i < dsi->nx; i++)
      {
	x = fabs(dsi->xd[i]);
	lmin =	(x > lmin) ? x : lmin;
      }	
    L = (L < lmin) ? lmin +.01 : L;
	
    ia[0] = ia[1] = ia[2] = 1;
    a[0] = xi;
    a[1] = L;
    a[2] = Ko;
    covar = matrix(1, 3, 1, 3);
    alpha = matrix(1, 3, 1, 3);
    sig = vector(1, dsi->nx);
    for (i = 1; i <= dsi->nx; i++)
      sig[i] = (flag) ? dsi->yd[i-1] * error : error;
    if (flag == 2)
      {
	i = win_scanf("enter index of error data set %d",&nder);
	if (i == WIN_CANCEL)	return OFF;
	if (op->dat[nder]->nx != dsi->nx)	
	  return win_printf("your data sets differs in size!");
	for (i = 1; i <= dsi->nx; i++)
	  sig[i] =  op->dat[nder]->yd[i-1];
      }
    alamda = -1;
    mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, a, ia,3, covar, alpha, &chisq, 
	   siggia_func_and_enthalpy, &alamda);
    /*
      win_printf("%d iter \\xi = %g,\n L = %g Ko = %g the \\chi^2  is %g\n\\lambda = %g",-2,a[0],a[1],a[2],chisq,alamda);
    */
    chisq0 = chisq;
    mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, a, ia,3, covar, alpha, &chisq, 
	   siggia_func_and_enthalpy, &alamda);
    /*
      win_printf("%d iter \\xi = %g,\n L = %g the \\chi^2  is %g\n\\lambda = %g",-1,a[0],a[1],chisq,alamda);
    */
    for (i = 0 ; (chisq0 - chisq <= 0) || (chisq0 - chisq > 1e-2); i++)
      {
	chisq0 = chisq;
	mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, a, ia,3, covar, alpha, 
	       &chisq, siggia_func_and_enthalpy, &alamda);
	/*
	  j = win_printf("%d iter \\xi = %g,\n L = %g the \\chi^2  is %g\n\\lambda = %g",i,a[0],a[1],chisq,alamda);
	  if (j == WIN_CANCEL)	break;
	*/
	if (i >= 10000) break;
	if (alamda > 1e12) break;
      }
	
    alamda = 0;
    mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, a, ia,3, covar, alpha, 
	   &chisq, siggia_func_and_enthalpy, &alamda);	
    i = win_printf("%d iter \\xi = %g,\n L = %g Ko = %g the \\chi^2  is %g\n"
		   " covar \\xi \\xi %g \\xi L %g\nL \\xi %g LL %g\n"
		   "curvature \\xi \\xi %g \\xi L %g\n L \\xi %g LL %g",i,a[0],a[1],a[2],chisq,
		   covar[1][1],covar[1][2],covar[2][1],covar[2][2],alpha[1][1],alpha[1][2],
		   alpha[2][1],alpha[2][2]);
    xi = a[0];
    L = a[1];
    Ko = a[2];
    if (i == WIN_CANCEL)	return OFF;
    if (dsi->nx > 256)	nf = dsi->nx;
    if ((dsd = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
      return win_printf("cannot create data set");
    if (dsi->nx > 256)	
      {
	for (i = 0; i < dsd->nx && i < dsi->nx; i++)	
	  {
	    x = dsi->xd[i];
	    dsd->xd[i] = dsi->xd[i];
	    dsd->yd[i] = marko_siggia_plus_enthalpy( x, xi, L, T, Ko);
	  }
      }
    else
      {		
	for (i = 0; i < dsd->nx; i++)	
	  {
	    x = (float)i/nf;
	    dsd->xd[i] = x*L;
	    dsd->yd[i] = marko_siggia_plus_enthalpy( x*L, xi, L, T, Ko);
	  }		
      }

	
    sprintf(st,"\\fbox{\\pt8\\stack{{\\sl Marko-Siggia enthalpy}"
	    "{L =  %g \\mu m +/- %g}{\\xi = %g nm +/- %g}{Ko = %g +/- %g pn}"
	    "{\\chi^2 = %g n = %d}{p = %g}}}",L,sqrt(covar[2][2]),xi,sqrt(covar[1][1]),Ko,
	    sqrt(covar[3][3]),chisq,dsi->nx-3,gammq((float)(dsi->nx-3)/2, chisq/2));
    push_plot_label(op, op->x_lo + (op->x_hi - op->x_lo)/4, 
		    op->y_hi - (op->y_hi - op->y_lo)/4, st, USR_COORD);
    free_vector(sig, 1, dsi->nx);	
    free_matrix(alpha, 1, 3, 1, 3);
    free_matrix(covar, 1, 3, 1, 3);
    return refresh_plot(pr,pr->cur_op);			
}
int	do_fit_Levenberg_Marquardt_improve_enthalpy(void)
{
    register int i;
    char st[256] = {0};
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *dsi = NULL, *dsd = NULL;
    float x, lmin;
    int nf = 1024;
    static float T = 298, error = .1;
    static float xi = 50;	/* nm*/
    static float L = 20;	/* microns */
    static float Ko = 1200; /* pN*/
    static int flag = 1;
    float a[3] = {0}, chisq, chisq0, alamda, *sig = NULL, **covar = NULL, **alpha = NULL; 
    int ia[3] = {0}, nder;	
	
    if(updating_menu_state != 0)	return D_O_K;	
    ia[0] = ia[1] = ia[2] = 1;
    
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)	return 1;
    i = win_scanf("Fit length of molecule \n"
		  "and the pesistence length and stiffness \npossible L value %8f (fit this parameter %b)\n"
		  "\\xi  value %8f (fit this parameter %b)\nKo value %8f  (fit this parameter %b)\n"
		  "error type, const -> 0 relative 1 "
		  "other data set 2%derror %f",&L,ia+1,&xi,ia,&Ko,ia+2,&flag,&error);
    if (i == WIN_CANCEL)	return OFF;
    for (i = 0, lmin = 0; i < dsi->nx; i++)
      {
	x = fabs(dsi->xd[i]);
	lmin =	(x > lmin) ? x : lmin;
      }	
    L = (L < lmin) ? lmin +.01 : L;
    
    //win_printf("ia 0 %d 1 %d 2 %d",ia[0],ia[1],ia[2]);
    
    a[0] = xi;
    a[1] = L;
    a[2] = Ko;
    covar = matrix(1, 3, 1, 3);
    alpha = matrix(1, 3, 1, 3);
    sig = vector(1, dsi->nx);
    for (i = 1; i <= dsi->nx; i++)
      sig[i] = (flag) ? dsi->yd[i-1] * error : error;
    if (flag == 2)
      {
	i = win_scanf("enter index of error data set %d",&nder);
	if (i == WIN_CANCEL)	return OFF;
	if (op->dat[nder]->nx != dsi->nx)	
	  return win_printf("your data sets differs in size!");
	for (i = 1; i <= dsi->nx; i++)
	  sig[i] =  op->dat[nder]->yd[i-1];
      }
    alamda = -1;
    mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, a, ia,3, covar, alpha, &chisq, 
	   siggia_func_improve_and_enthalpy, &alamda);
    /*
      win_printf("%d iter \\xi = %g,\n L = %g Ko = %g the \\chi^2  is %g\n\\lambda = %g",-2,a[0],a[1],a[2],chisq,alamda);
    */
    chisq0 = chisq;
    mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, a, ia,3, covar, alpha, &chisq, 
	   siggia_func_improve_and_enthalpy, &alamda);
    /*
      win_printf("%d iter \\xi = %g,\n L = %g the \\chi^2  is %g\n\\lambda = %g",-1,a[0],a[1],chisq,alamda);
    */
    for (i = 0 ; (chisq0 - chisq <= 0) || (chisq0 - chisq > 1e-2); i++)
      {
	chisq0 = chisq;
	mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, a, ia,3, covar, alpha, 
	       &chisq, siggia_func_improve_and_enthalpy, &alamda);
	/*
	  j = win_printf("%d iter \\xi = %g,\n L = %g the \\chi^2  is %g\n\\lambda = %g",i,a[0],a[1],chisq,alamda);
	  if (j == WIN_CANCEL)	break;
	*/
	if (i >= 10000) break;
	if (alamda > 1e12) break;
      }
    
    alamda = 0;
    mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, a, ia,3, covar, alpha, &chisq, siggia_func_improve_and_enthalpy, &alamda);	
    i = win_printf("%d iter \\xi = %g,\n L = %g Ko = %g the \\chi^2  is %g\n"
		   " covar \\xi \\xi %g \\xi L %g\nL \\xi %g LL %g\n"
		   "curvature \\xi \\xi %g \\xi L %g\n L \\xi %g LL %g",i,a[0],a[1],a[2],chisq,
		   covar[1][1],covar[1][2],covar[2][1],covar[2][2],alpha[1][1],alpha[1][2],
		   alpha[2][1],alpha[2][2]);
    xi = a[0];
    L = a[1];
    Ko = a[2];
    if (i == WIN_CANCEL)	return OFF;
    if (dsi->nx > 256)	nf = dsi->nx;
    if ((dsd = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
      return win_printf("cannot create data set");
    if (dsi->nx > 256)	
      {
	for (i = 0; i < dsd->nx && i < dsi->nx; i++)	
	  {
	    x = dsi->xd[i];
	    dsd->xd[i] = dsi->xd[i];
	    dsd->yd[i] = marko_siggia_improved_plus_enthalpy( x, xi, L, T, Ko);
	  }
      }
    else
      {		
	for (i = 0; i < dsd->nx; i++)	
	  {
	    x = (Ko != 0) ? (1+(float)100/Ko)*(float)i/nf : (float)i/nf;
	    dsd->xd[i] = x*L;
	    dsd->yd[i] = marko_siggia_improved_plus_enthalpy( x*L, xi, L, T, Ko);
	  }		
      }
    
    
    sprintf(st,"\\fbox{\\pt8\\stack{{\\sl Marko-Siggia improved enthalpy}"
	    "{L =  %g \\mu m +/- %g}{\\xi = %g nm +/- %g}{Ko = %g +/- %g pn}"
	    "{\\chi^2 = %g n = %d}{p = %g}}}",L,sqrt(covar[2][2]),xi,sqrt(covar[1][1]),
	    Ko,sqrt(covar[3][3]),chisq,dsi->nx-3,gammq((float)(dsi->nx-3)/2, chisq/2));
    push_plot_label(op, op->x_lo + (op->x_hi - op->x_lo)/4, 
		    op->y_hi - (op->y_hi - op->y_lo)/4, st, USR_COORD);
    free_vector(sig, 1, dsi->nx);	
    free_matrix(alpha, 1, 3, 1, 3);
    free_matrix(covar, 1, 3, 1, 3);
    return refresh_plot(pr,pr->cur_op);			
}


int	do_check_fit_Levenberg_Marquardt(void)
{
    register int i;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *dsi = NULL, *dsd = NULL;
    float x, y;
    int nf = 1024;
    static float T = 298, error = .1;
    static float xi = 50, xit = 50;	/* nm*/
    static float L = 16.2, Lt = 16.2;	/* microns */
    float a[2] = {0}, chisq, chisq0, alamda, *sig = NULL, **covar = NULL, **alpha = NULL; 
    int ia[2] = {0}, ifit;	
    static int idum = 0;	
    
    if(updating_menu_state != 0)	return D_O_K;	
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)	return 1;
    i = win_scanf("monte carlo Fit length of molecule \n"
		  "and the pesistence length possible L value %f"
		  "\\xi  value %f error %ftrue \\xi %ftrue L %fnb of fits %dseed %d",
		  &L,&xi,&error,&xit,&Lt,&nf,&idum);
    if (i == WIN_CANCEL)	return OFF;
    
    covar = matrix(1, 2, 1, 2);
    alpha = matrix(1, 2, 1, 2);
    sig = vector(1, dsi->nx);
    
    op = create_and_attach_one_plot(pr, nf, nf, 0);
    dsd = op->dat[0];
    if (dsd == NULL)		return win_printf("can't create ds");;
    
    for (ifit = 0; ifit < nf; ifit++)
      {
	ia[0] = ia[1] = 1;
	a[0] = xi;
	a[1] = L;
	covar[1][1] = covar[1][2] = covar[2][1] = covar[2][2] = 0;
	alpha[1][1] = alpha[1][2] = alpha[2][1] = alpha[2][2] = 0;
	for (i = 0; i < dsi->nx; i++)	
	  {
	    x = dsi->xd[i]/Lt;
	    y = 1.38e-23 * T/(xit*1e-9);
	    y *= (x -.25 + .25*(1/((1-x)*(1-x))));
	    dsi->yd[i] = y*1e12 + error*gasdev(&idum);
	  }	
	
	refresh_plot(pr,pr->n_op-2);		
	
	
	for (i = 1; i <= dsi->nx; i++)
	  sig[i] =  error;
	alamda = -1;
	mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, a, ia,2, covar, alpha, 
	       &chisq, siggia_func, &alamda);
/*
	win_printf("%d iter \\xi = %g,\n L = %g the \\chi^2  is %g\n\\lambda = %g",-2,a[0],a[1],chisq,alamda);
*/
	chisq0 = chisq;
	mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, a, ia,2, covar, alpha, 
	       &chisq, siggia_func, &alamda);
	/*
	  win_printf("%d iter \\xi = %g,\n L = %g the \\chi^2  is %g\n\\lambda = %g",-1,a[0],a[1],chisq,alamda);
	*/
	for (i = 0 ; (chisq0 - chisq <= 0) || (chisq0 - chisq > 1e-2); i++)
	  {
	    chisq0 = chisq;
	    mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, a, ia,2, covar, alpha, 
		   &chisq, siggia_func, &alamda);
	    /*
	      j = win_printf("%d iter \\xi = %g,\n L = %g the \\chi^2  is %g\n\\lambda = %g",i,a[0],a[1],chisq,alamda);
	      
	      if (j == WIN_CANCEL)	break;
	    */
	    if (i >= 10000) break;
	    if (alamda > 1e12) break;
	  }
	
	alamda = 0;
	mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, a, ia,2, covar, alpha, 
	       &chisq, siggia_func, &alamda);	
	/*
	  i = win_printf("%d iter \\xi = %g,\n L = %g the \\chi^2  is %g\n covar \\xi
	  \\xi %g \\xi L %g\nL \\xi %g LL %g\ncurvature \\xi \\xi %g \\xi L %g\n L \\xi %g LL %g",i,a[0],a[1],chisq,covar[1][1],covar[1][2],covar[2][1],covar[2][2],alpha[1][1],alpha[1][2],alpha[2][1],alpha[2][2]);
	  if (i == WIN_CANCEL)	return OFF;
	*/
	dsd->xd[ifit] = a[0];
	dsd->yd[ifit] = a[1];
	
	display_title_message("iter %d \\xi %g L %g",ifit,a[0],a[1]);


      }
    
    free_vector(sig, 1, dsi->nx);	
    free_matrix(alpha, 1, 2, 1, 2);
    free_matrix(covar, 1, 2, 1, 2);
    set_plot_x_title(op, "\\xi (nm)");
    set_plot_y_title(op, "L (\\mu m)");
	
    return refresh_plot(pr,pr->cur_op);			
}

// hat curves 
/* dna relative extension  < z >/L and mormalized torsion angle  eta = 2  pi n A/L are given as 
function of the normalized torque  kappa = Gamma /(k_B T) and  alphaa = F A/(k_B T)  for agiven value 
of A/C :  < z >/L =   Fel(alpha,kappa) and  eta = kappa ( A/C   +  Fwr(alpha,kappa)).
For a given value of < z >/L, eta varies linearly with A/C with a slope kappa.
Fel_2 correspond to alpha = 2
*/

int	generate_hat_curve(void)
{
	register int i, j;
	pltreg *pr = NULL;
	O_p *op = NULL, *opk = NULL;
	d_s  *dsd = NULL, *dsk = NULL;
	float x, y, kappa = 0, wr = 0, ext = 0, F = 0;
	int nf = 1024, nh;
	static int nalpha = 1;
	static float T = 298;
	static float xi = 50, ci = 100;	/* nm*/
	static float L = 16.2;	/* microns */

	
	if(updating_menu_state != 0)	return D_O_K;	
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return D_O_K;	
	i = win_scanf("Draw at curve with variable persistence lengths \n"
		      "give bending pesistence length (nm) %12f\n"
		      "rotationnal one (nm) %12f\nnormalized force (F\\xi/k_BT) = 2 %R 3 %r 4 %r\n"
		      "molecule extension (\\mu m)%12f\n T (K) %12f\n",&xi,&ci,&nalpha,&L,&T);

	if (i == WIN_CANCEL)	return OFF;
	nh = nb_hat[nalpha];
	nf = (2 * nh) - 1;
	op = create_and_attach_one_plot(pr, nf, nf, 0);
	dsd = op->dat[0];
	if (dsd == NULL)	return win_printf_OK("can't create ds");
	opk = create_and_attach_one_plot(pr, nf, nf, 0);
	dsk = opk->dat[0];
	if (dsd == NULL)	return win_printf_OK("can't create ds");
	F = (nalpha+2)*1.38e-11 * T/(xi*1e-9);
	for (i = 0; i < nh; i++)
	  {
	    if (nalpha == 0) 	    
	      {
		ext = Fel_2[i].ext;
		kappa = Fel_2[i].kappa;
		wr = Fwr_2[i].wr;
	      }
	    else if (nalpha == 1)   
	      {
		ext = Fel_3[i].ext;
		kappa = Fel_3[i].kappa;
		wr = Fwr_3[i].wr;
	      }
	    else if (nalpha == 2)   
	      {
		ext = Fel_4[i].ext;
		kappa = Fel_4[i].kappa;
		wr = Fwr_4[i].wr;
	      }
	    y = L * ext;
	    x = L * 1000 * kappa *( ((float)1/ci) + (wr/xi))/(2*M_PI);
	    j = nh - 1 - i;
	    dsd->xd[j] = dsk->xd[j] = -x;
	    dsd->xd[nh-1+i] = dsk->xd[nh-1+i] = x;
	    dsk->yd[j] = -kappa*1.38e-2 * T;
	    dsk->yd[nh-1+i] = kappa*1.38e-2 * T;
	    dsd->yd[j] = dsd->yd[nh-1+i] = y;
	  }
	set_plot_title(op, "\\stack{{Hat curve at F = %g pN}{\\pt8 \\xi = %g, \\xi_t = %g}}",
		       F,xi,ci);

	set_plot_x_title(op, "Nb. of turns");
	set_plot_y_title(op, "Extension (\\mu m)");
	set_plot_title(opk, "\\stack{{Torque curve at F = %g pN}{\\pt8 \\xi = %g, \\xi_t = %g}}",
		       F,xi,ci);

	set_plot_x_title(opk, "Nb. of turns");
	set_plot_y_title(opk, "\\Gamma (pN.nm)");
	return refresh_plot(pr,pr->cur_op);			
}

int	generate_many_hat_curve(void)
{
	register int i, j, k;
	pltreg *pr = NULL;
	O_p *op = NULL, *opk = NULL;
	d_s  *dsd = NULL, *dsk = NULL;
	float x, y, kappa = 0, wr = 0, ext = 0, F = 0;
	int nf = 1024, nh, np, nc;
	static int nalpha = 10;
	static float T = 298;
	static float xi = 50, ci = 100;	/* nm*/
	static float L = 16.2;	/* microns */

	
	if(updating_menu_state != 0)	return D_O_K;	
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return D_O_K;	
	i = win_scanf("Draw at curve with variable persistence lengths \n"
		      "give bending pesistence length (nm) %12f\n"
		      "rotationnal one (nm) %12f\nnormalized force 0.2*(F\\xi/k_BT) = %4d (4 < integer < 100)\n"
		      "molecule extension (\\mu m)%12f\n T (K) %12f\n",&xi,&ci,&nalpha,&L,&T);

	if (i == WIN_CANCEL)	return OFF;
	nh = 99;
	nf = 2*nh-1;
	op = create_and_attach_one_plot(pr, nf, nf, 0);
	dsd = op->dat[0];
	if (dsd == NULL)	return win_printf_OK("can't create ds");
	opk = create_and_attach_one_plot(pr, nf, nf, 0);
	dsk = opk->dat[0];
	if (dsd == NULL)	return win_printf_OK("can't create ds");
	nc = (nalpha > 100)? 20 : 1;
	for (k = 0; k < nc; k++)
	  {
	    if (nc > 1)
	      {
		nalpha = 5*k;
		if (k > 0)
		  {
		    if ((dsd = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
		      return win_printf("cannot create data set");
		    if ((dsk = create_and_attach_one_ds(opk, nf, nf, 0)) == NULL)
		      return win_printf("cannot create data set");
		  }
	      }
	    np = nalpha - 2; 
	    F = nalpha*1.38e-11 * T/(xi*5*1e-9);
	    for (i = 0; i < nh; i++)
	      {
		kappa = ((double)i)/100;
		kappa *= kappamax[np];
		ext = FdFel[(np*101)+i];
		wr = FdFwr[(np*101)+i];
		y = L * ext;
		x = L * 1000 * kappa *( ((float)1/ci) + (wr/xi))/(2*M_PI);
		j = nh - 1 - i;
		dsd->xd[j] = dsk->xd[j] = -x;
		dsd->xd[nh-1+i] = dsk->xd[nh-1+i] = x;
		dsk->yd[j] = -kappa*1.38e-2 * T;
		dsk->yd[nh-1+i] = kappa*1.38e-2 * T;
		dsd->yd[j] = dsd->yd[nh-1+i] = y;
	      }
	  }
	set_plot_title(op, "\\stack{{Hat curve at F = %g pN}{\\pt8 \\xi = %g, \\xi_t = %g}}",
		       F,xi,ci);

	set_plot_x_title(op, "Nb. of turns");
	set_plot_y_title(op, "Extension (\\mu m)");
	set_plot_title(opk, "\\stack{{Torque curve at F = %g pN}"
		       "{\\pt8 \\xi = %g, \\xi_t = %g}}",F,xi,ci);
	set_plot_x_title(opk, "Nb. of turns");
	set_plot_y_title(opk, "\\Gamma (pN.nm)");
	return refresh_plot(pr,pr->cur_op);			
}

int	generate_force_curve_with_rot(void)
{
	register int i, k;
	pltreg *pr = NULL;
	O_p *op = NULL;
	d_s  *dsd = NULL;
	float x, y, kappa = 0, wr = 0, ext = 0, F = 0, xh[99], yh[99];
	static int f0 = 10, f1 = 50;
	int  nh;
	static float T = 298, nturn = 10;
	static float xi = 50, ci = 100;	/* nm*/
	static float L = 16.2;	/* microns */
	double w = 0;
	//float interpolate_point_by_poly_4_in_array(float *x, float *y, int nx, float xp);
	
	if(updating_menu_state != 0)	return D_O_K;	
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return D_O_K;	
	i = win_scanf("Draw at force curve with variable persistence lengths \n"
		      "give bending pesistence length (nm) %12f\n"
		      "rotationnal one (nm) %12f\nnnumber of turns %8f\n"
		      "molecule extension (\\mu m)%12f\n T (K) %12f\n"
		      "Integrate from index %d to %d",&xi,&ci,&nturn,&L,&T,&f0,&f1);

	if (i == WIN_CANCEL)	return OFF;
	nh = 99;
	op = create_and_attach_one_plot(pr, nh, nh, 0);
	dsd = op->dat[0];
	if (dsd == NULL)	return win_printf_OK("can't create ds");

	for (k = 0, w = 0; k < nh; k++)
	  {
	    F = (k+2)*1.38e-11 * T/(xi*5*1e-9);
	    for (i = 0; i < nh; i++)
	      {
		kappa = ((double)i)/100;
		kappa *= kappamax[k];
		ext = FdFel[(k*101)+i];
		wr = FdFwr[(k*101)+i];
		y = L * ext;
		x = L * 1000 * kappa *( ((float)1/ci) + (wr/xi))/(2*M_PI);
		xh[i] = x;
		yh[i] = y;
	      }
	    dsd->xd[k] = interpolate_point_by_poly_4_in_array(xh, yh, nh, nturn);
	    dsd->yd[k] = F; 
	    if (((k+2) > f0) && ((k+2) <= f1))
	      {
		w += (dsd->xd[k] - dsd->xd[(k>0)?k-1:0])*(dsd->yd[k] + dsd->yd[(k>0)?k-1:0])/2;
	      }
	  }
	set_plot_title(op, "\\stack{{Force curve at F = %g pN}{\\pt8 \\xi = %g, \\xi_t = %g}"
		       "{\\int_{f_0=%g}^{f_1 = %g} F.dl  w = %g pN.nm}}",
		       F,xi,ci,f0*1.38e-11 * T/(xi*5*1e-9),f1*1.38e-11 * T/(xi*5*1e-9),1000*w);
	set_plot_y_title(op, "Force curve at n = %f",nturn);
	set_plot_x_title(op, "Extension (\\mu m)");
	return refresh_plot(pr,pr->cur_op);			
}


int	generate_nelson_hat_top_curve(void)
{
	register int i, j;
	pltreg *pr = NULL;
	O_p *op = NULL, *opk = NULL;
	d_s  *dsd = NULL, *dsk = NULL;
	float  y, kappa = 0, F = 0, ceff, n;
	int nf = 1024;
	static int nturns = 50;
	static float alpha = 1;
	static float T = 298;
	static float xi = 50, ci = 100;	/* nm*/
	static float L = 16.2;	/* microns */

	
	if(updating_menu_state != 0)	return D_O_K;	
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return D_O_K;	
	i = win_scanf("Draw top of hat curve (according to P. Nelson) with variable \n"
		      "persistence lengths \n"
		      "give bending pesistence length (nm) %12f\n"
		      "rotationnal one (nm) %12f\nnormalized force (F\\xi/k_BT) %12f\n"
		      "molecule extension (\\mu m)%12f\n T (K) %12f\nn turns %8d\n ",&xi,&ci,&alpha,&L,&T,&nturns);

	if (i == WIN_CANCEL)	return OFF;
	op = create_and_attach_one_plot(pr, (2*nf)-1, (2*nf)-1, 0);
	dsd = op->dat[0];
	if (dsd == NULL)	return win_printf_OK("can't create ds");
	opk = create_and_attach_one_plot(pr, (2*nf)-1, (2*nf)-1, 0);
	dsk = opk->dat[0];
	if (dsd == NULL)	return win_printf_OK("can't create ds");


	F = (alpha)*1.38e-11 * T/(xi*1e-9);
	ceff = ((double)1/ci) + ((double)1/(4*xi*sqrt(alpha)));
	ceff = (double)1/ceff;

	for (i = 0; i < nf; i++)
	  {
	    n = ((double)(i*nturns))/nf;
	    //sigma = (theta*3.6)/(1000*L);
	    kappa = ceff * n * 2 * M_PI/(1000*L);
	    if (kappa*kappa < (4*alpha))
	      y = 1 - 0.5 / sqrt(alpha - (kappa*kappa)/4 - (double)1/32);
	    else y = 0;
	    //y += xi/(1000*L
	    y *= L;
	    j = nf - 1 - i;
	    dsd->xd[j] = dsk->xd[j] = -n;
	    dsd->xd[nf-1+i] = dsk->xd[nf-1+i] = n;

	    dsk->yd[j] = -kappa*1.38e-2 * T;
	    dsk->yd[nf-1+i] = kappa*1.38e-2 * T;

	    dsd->yd[j] = dsd->yd[nf-1+i] = y;
	  }
	set_plot_title(op, "\\stack{{Hat curve at F = %g pN}{\\pt8 \\xi = %g, \\xi_t = %g}}",
		       F,xi,ci);

	set_plot_x_title(op, "Nb. of turns");
	set_plot_y_title(op, "Extension (\\mu m)");
	set_plot_title(opk, "\\stack{{Torque curve at F = %g pN}{\\pt8 \\xi = %g, \\xi_t = %g}}",
		       F,xi,ci);

	set_plot_x_title(opk, "Nb. of turns");
	set_plot_y_title(opk, "\\Gamma (pN.nm)");
	return refresh_plot(pr,pr->cur_op);			
}
/* f in pN, C, A, L0 all in nm ! nc in turns
 */
double pal_cross_over_versus_F(double f,double C,double A, double L0)
{
  double n_c, alpha, ceff;
  static float T = 298;

  if(C==0) return 0;
  alpha = f*A/(1.38e-2 * T);
  ceff = ((double)1/C) + ((double)1/(4*A*sqrt(alpha)));
  ceff = (double)1/ceff;

  n_c = 10.5* L0 * extension_versus_alpha(alpha) * 0.34 * f / (4 * M_PI * M_PI * ceff * 1.38e-2 * T);


  return n_c;
}



int	generate_nc_vs_f(void)
{
	register int i;
	pltreg *pr;
	O_p *op;
	d_s  *dsd;
	int nf = 1024;
	static float xi = 50, ci = 100, Fm = 10;	/* nm*/
	static float L = 16.2;	/* microns */

	
	if(updating_menu_state != 0)	return D_O_K;	
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return D_O_K;	
	i = win_scanf("Draw palindrome cross over in turns n_c versus F\n"
		      "give bending pesistence length (nm) %12f\n "
		      "rotationnal one (nm) %12f\n"
		      "molecule extension (\\mu m)%12f\nMax force %12f\n ",&xi,&ci,&L,&Fm);

	if (i == WIN_CANCEL)	return OFF;
	op = create_and_attach_one_plot(pr, nf, nf, 0);
	dsd = op->dat[0];
	if (dsd == NULL)	return win_printf_OK("can't create ds");


	for (i = 0; i < nf; i++)
	  {
	    dsd->xd[i] = ((double)((i+1)*Fm))/nf;
	    dsd->yd[i] = pal_cross_over_versus_F(dsd->xd[i],ci,xi, 1000* L);
	  }
	set_plot_title(op, "\\color{white}\\stack{{Palindrome cross over }{\\pt8 \\xi = %g, C = %g L = %g}}",
		       xi,ci,L);

	set_plot_y_title(op, "\\color{white}Nb. of turns");
	set_plot_x_title(op, "\\color{white}Force (pN)");
	return refresh_plot(pr,pr->cur_op);			
}



int draw_hat_claude(void)
{
  register int i;
  pltreg *pr = NULL;
  O_p *op = NULL, *opk = NULL;
  d_s  *dsd = NULL, *dsk = NULL;
  float  kBT;
  static float alpha = 1, F = 0.3;
  static float T = 298;
  static float xi = 50, ci = 100, cwr = 0.93;	/* nm*/
  multi_d_s *mds = NULL;
  double omega_0;

  omega_0 = 2*M_PI/3.6;
  
  
  if(updating_menu_state != 0)	return D_O_K;	
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return D_O_K;	

  mds = find_multi_d_s_in_pltreg(pr);
  if (mds == NULL)
    return win_printf_OK("Could not find an xvplot containing Claude data!");
  if (mds->n_d_s != 3)
    return win_printf_OK("your xvplot does not contain Claude data!");
  if (strncmp("Couple/(kBT)",mds->name[0],12) != 0)
    return win_printf_OK("Col 1 of your xvplot is not the torque!\n%s",
			 mds->name[0]);
  if (strncmp("<z>/L",mds->name[1],5) != 0)
    return win_printf_OK("Col 2 of your xvplot is not <z>/L!\n%s",
			 mds->name[1]);
  if (strncmp("(A/L)*Writhe",mds->name[2],12) != 0)
    return win_printf_OK("Col 3 of your xvplot is not (A/L)*Writhe!\n%s",
			 mds->name[2]);



  i = win_scanf("Draw hat curve and torque versus sigma using Claude data\n"
		"give bending pesistence length (nm) %12f\n"
		"rotationnal one (nm) %12f\n"
		"normalized force (F\\xi/k_BT) %12f Force (pN) %8f\n"
		"T (K) %12f Cwr %6f\n ",&xi,&ci,&alpha,&F,&T,&cwr);
  
  if (i == WIN_CANCEL)	return OFF;

  kBT = 1.38062e-2 * T; // pN . nm

  op = create_and_attach_one_plot(pr, (2*mds->n), (2*mds->n), 0);
  dsd = op->dat[0];
  if (dsd == NULL)	return win_printf_OK("can't create ds");
  set_plot_x_title(op, "\\sigma");
  set_plot_y_title(op, "<z>/L");
  set_plot_title(op, "A = %g, C = %g, F = %g, \\alpha = %g cwr %g",xi,ci,F,alpha,cwr);
  set_ds_source(dsd, "Claude hat A = %g, C = %g, f = %g, alpha = %g T = %g Cwr = %g",xi,ci,F,alpha,T,cwr);

  opk = create_and_attach_one_plot(pr, (2*mds->n), (2*mds->n), 0);
  dsk = opk->dat[0];
  if (dsk == NULL)	return win_printf_OK("can't create ds");
  set_plot_x_title(opk, "\\sigma");
  set_plot_y_title(opk, "Torque (pN.mn)");
  set_plot_title(opk, "A = %g, C = %g, F = %g, \\alpha = %g Cwr %g",xi,ci,F,alpha,cwr);
  set_ds_source(dsk, "Claude torque A = %g, C = %g, f = %g, alpha = %g T = %g cwr %g",xi,ci,F,alpha,T,cwr);

  for ( i = 0 ; i < mds->n ; i++)
    {
      dsd->xd[mds->n + i - 1] = ((mds->x[0][i]/ci) + (cwr*mds->x[2][i]/xi))/omega_0;
      dsk->xd[mds->n + i - 1] = dsd->xd[mds->n + i - 1];
      dsd->yd[mds->n + i - 1] = mds->x[1][i];
      dsk->yd[mds->n + i - 1] = kBT * mds->x[0][i];
      if (i)
	{
	  dsd->xd[mds->n - i - 1] = -dsd->xd[mds->n + i - 1];
	  dsk->xd[mds->n - i - 1] = -dsd->xd[mds->n + i - 1];
	  dsd->yd[mds->n - i - 1] = mds->x[1][i];
	  dsk->yd[mds->n - i - 1] = -dsk->yd[mds->n + i - 1];
	}
    }
  dsd->nx = dsd->ny = dsk->nx = dsk->ny = (2*mds->n) - 1;



  return refresh_plot(pr,pr->cur_op);			
}
int aff_force_to_hats(void)
{
  return 0;
}
int derive_hat(void)
{
  register int i, j;
  pltreg *pr = NULL;
  O_p *op = NULL, *opd = NULL;
  d_s  *ds = NULL, *dsi = NULL, *dsp = NULL, *dsd = NULL;
  static int nvoisin = 2, npoly = 2;
  int nf;
  double *a = NULL;

  if(updating_menu_state != 0)	return D_O_K;	
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)	return D_O_K;

  i = win_scanf("I am going to derivate your curve by fitting a polynome\n"
		"to 2n + 1 points of the original data centered on each data\n"
		"and derivating this polynome\n"
		"Indicate the number n of points on each side %6d\n"
		"and the order of the polynome %6d",&nvoisin,&npoly);
  if (i == WIN_CANCEL)  return D_O_K;

  dsi = duplicate_data_set(ds,NULL);
  sort_ds_along_x(dsi);

  nf = 1+(2* nvoisin);
  dsp = build_data_set(nf,nf);
  if (dsp == NULL)     return win_printf_OK("cannot data set !");
  dsp->nx = dsp->ny = nf;

  opd = create_and_attach_one_plot(pr, dsi->nx - 2*nvoisin ,dsi->nx - 2*nvoisin, 0);
  if (opd == NULL)	return win_printf_OK("Could not create plot data");
  dsd = opd->dat[0];

  for (i = nvoisin; i < dsi->nx - nvoisin; i++)
    {
      for(j = -nvoisin; j <= nvoisin; j++)
	{
	  dsp->xd[nvoisin+j] = dsi->xd[i+j] - dsi->xd[i];
	  dsp->yd[nvoisin+j] = dsi->yd[i+j] - dsi->yd[i];
	}
      fit_all_ds_to_xn_polynome(dsp, npoly+1, &a);
      dsd->xd[i - nvoisin] = dsi->xd[i];
      dsd->yd[i - nvoisin] = a[1];
      free(a);
      a = NULL;
    }


  free_data_set(dsi);
  free_data_set(dsp);
  return refresh_plot(pr,pr->cur_op);			
}

int concert_hat_to_torque(void)
{
  register int i, j, k;
  pltreg *pr = NULL;
  O_p *op = NULL, *opf = NULL, *opf2 = NULL, *opf3 = NULL;
  d_s  *ds = NULL, *dsf = NULL, *dsd = NULL, *dsk = NULL, *dstmp = NULL, *dstmpi = NULL, *dstmpi2 = NULL, *dst = NULL;
  //float  kBT, alpha = 1;
  float x, y;
  static float F = 0.3, sigmam = 0.03,  h0 = 3.6, kBT, Cs = 80;
  float zmag, zmagmax, Fmax, imax;
  static float T = 298;
  //static float xi = 50, ci = 100;	/* nm*/
  double omega_0, tmpd;
  int i_sigma0, i_sigmam, nf;
  char question[1024] = {0};




  if(updating_menu_state != 0)	return D_O_K;	
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return D_O_K;	

  for (i = imax = 0, zmagmax = 0, Fmax = 0; i < op->n_dat; i++)
    {
      ds = op->dat[i];
      if (ds->source == NULL) 
	return win_printf_OK("data set %d has no source !",i);
      if ((j = sscanf(ds->source,"Hat at Zmag = %f Force = %f pN",&zmag,&F)) != 2)
	return win_printf_OK("%d cannot recover zmag and force in data set %d\n"
			     "source : \n%s !\n",j,i,ds->source);
      for(j = 0; j < ds->nx && j < op->dat[0]->nx; j++)
	if (ds->xd[j] != op->dat[0]->xd[j])
	  return win_printf_OK("data set %d as not the same x value ->%g than data set 0->%g\n"
			       "at point index %d !",i,ds->xd[j],op->dat[0]->xd[j],j);
      if (zmag > zmagmax || F > Fmax)
	{
	  imax = i;
	  zmagmax = zmag;
	  Fmax = F;
	}
    }
  dsf = build_data_set(op->n_dat,op->n_dat);
  if (dsf == NULL)     return win_printf_OK("cannot data set !");
  dsf->nx = dsf->ny = op->n_dat;
  for (i = 0; i < op->n_dat; i++)
    {
      ds = op->dat[i];
      sscanf(ds->source,"Hat at Zmag = %f Force = %f pN",&zmag,&F);
      dsf->xd[i] = F;
      dsf->yd[i] = i;
    }
  
  sort_ds_along_x(dsf);
  for(i = 0; i < dsf->nx/2; i++)
    {
      x = dsf->xd[dsf->nx-i-1];
      y = dsf->yd[dsf->nx-i-1];
      dsf->xd[dsf->nx-i-1] = dsf->xd[i];
      dsf->yd[dsf->nx-i-1] = dsf->yd[i];
      dsf->xd[i] = x;
      dsf->yd[i] = y;
    }
  //dsf is now ordered by decreasing force
  sprintf(question,"I have found %d hat curves Fmax %g\n"
	  "define the maximum \\sigma  values %%6f\n"
	  "give Cs at %g pN %%6f nm\nDNA helical pitch %%6f nm\n"
	  "Temperature in %%6f K\n",op->n_dat,Fmax,Fmax);
  i = win_scanf(question,&sigmam,&Cs,&h0,&T);
  if (i == WIN_CANCEL) 
    {
      free_data_set(dsf);
      return D_O_K;  
    }
  kBT = 1.38062e-2 * T;
  omega_0 = 2*M_PI/h0;
  for(j = 0, i_sigma0 = i_sigmam = 0, ds = op->dat[0]; j < ds->nx; j++)
    {
      if (ds->xd[j] >= 0 && ds->xd[j] < fabs(ds->xd[i_sigma0])) 
	i_sigma0 = j;
      if (ds->xd[j] >= 0 && ds->xd[j] > ds->xd[i_sigmam] && ds->xd[j] < sigmam) 
	i_sigmam = j;
      }
  //win_printf("\\sigma min %g at %d\n\\sigma max %g at %d",
  //	     ds->xd[i_sigma0],i_sigma0,ds->xd[i_sigmam],i_sigmam);


  opf = create_and_attach_one_plot(pr, op->n_dat,op->n_dat, 0);
  if (opf == NULL)	return win_printf_OK("Could not create plot data");
  for (i = i_sigma0 ; i < i_sigmam; i++)
    {
      if ((dsd = create_and_attach_one_ds(opf, op->n_dat, op->n_dat, 0)) == NULL)
	return win_printf("I can't create ds !");
    }
  opf->filename = Transfer_filename(op->filename);
  uns_op_2_op_by_type(op, IS_Y_UNIT_SET, opf, IS_X_UNIT_SET);
  uns_op_2_op_by_type(op, IS_X_UNIT_SET, opf, IS_Y_UNIT_SET);
  //set_plot_title(opf,"row profiles from %d to %d",start,end);
  for (i = i_sigma0, k = 0; i <= i_sigmam; i++, k++)
    {
      dsd = opf->dat[k];
      set_plot_symb(dsd,"\\pt6\\di ");
      for (j=0 ; j< dsd->nx ; j++)    
	{
	  ds = op->dat[(int)(dsf->yd[j]+0.5)];
	  dsd->xd[j] = ds->xd[i];
	  dsd->yd[j] = ds->yd[i];
	}
      set_ds_source(dsd, "Hat derivative at \\sigma = %f",ds->xd[i]);
    }

  opf2 = create_and_attach_one_plot(pr, op->n_dat,op->n_dat, 0);
  if (opf2 == NULL)	return win_printf_OK("Could not create plot data");
  for (i = i_sigma0 ; i < i_sigmam; i ++)
    {
      if ((dsk = create_and_attach_one_ds(opf2, op->n_dat, op->n_dat, 0)) == NULL)
	return win_printf("I can't create ds !");
    }
  opf2->filename = Transfer_filename(op->filename);
  uns_op_2_op_by_type(op, IS_Y_UNIT_SET, opf2, IS_X_UNIT_SET);
  uns_op_2_op_by_type(op, IS_X_UNIT_SET, opf2, IS_Y_UNIT_SET);
  //set_plot_title(opf,"row profiles from %d to %d",start,end);

  dstmp = build_data_set(op->n_dat,op->n_dat);
  //dstmp = create_and_attach_one_ds(opf, op->n_dat, op->n_dat, 0);
  if (dstmp == NULL)     return win_printf_OK("cannot data set !");
  dstmp->nx = dstmp->ny = op->n_dat;

  nf = (20*op->n_dat)+1; 
  //dstmpi = create_and_attach_one_ds(opf2, nf, nf, 0);
  dstmpi = build_data_set(nf,nf);
  if (dstmpi == NULL)     return win_printf_OK("cannot data set !");
  dstmpi->nx = dstmpi->ny = nf;

  //dstmpi2 = create_and_attach_one_ds(opf2, nf, nf, 0);
  dstmpi2 = build_data_set(nf,nf);
  if (dstmpi2 == NULL)     return win_printf_OK("cannot data set !");
  dstmpi2->nx = dstmpi2->ny = nf;


  for (i = i_sigma0, k = 0; i <= i_sigmam; i++, k++)
    {
      if (k < opf->n_dat) dsd = opf->dat[k];
      else win_printf("k = %d out of range %d",k,opf->n_dat);
      for (j=0 ; j< dsd->nx ; j++)    
	{
	  dstmp->xd[j] = -dsf->xd[j];
	  dstmp->yd[j] = dsd->yd[j];
	}
      for (j = 0; j < nf; j++)	
	{
	  dstmpi->xd[j] = dstmp->xd[0] + j * (dstmp->xd[dsd->nx-1] - dstmp->xd[0])/(nf-1); 
	  dstmpi->yd[j] = interpolate_point_by_poly_2_in_array(dstmp->xd, dstmp->yd, dstmp->nx, 
							       dstmpi->xd[j]);
	}
      dstmpi2->xd[0] = dstmpi->xd[0];
      dstmpi2->yd[0] = 0;
      for (j=1, tmpd = 0; j< nf ; j++)    
	{
	  dstmpi2->xd[j] = dstmpi->xd[j];
	  tmpd += (dstmpi->xd[j] - dstmpi->xd[j-1])*(dstmpi->yd[j] + dstmpi->yd[j-1])/2;
	  dstmpi2->yd[j] = tmpd; 
	}

      if (k < opf2->n_dat) dsk = opf2->dat[k];
      else win_printf("k = %d out of range 2 %d",k,opf2->n_dat);
      set_plot_symb(dsk,"\\pt6\\di ");


      for (j = 0; j < dsd->nx; j++)	
	{
	  dsk->xd[j] = op->dat[0]->xd[i];
	  dsk->yd[j] = interpolate_point_by_poly_2_in_array(dstmpi2->xd, dstmpi2->yd, dstmpi2->nx, 
							       dstmp->xd[j]);
	}
      /*
      for (j=1 ; j< dsd->nx ; j++)    
	{
	  dsk->xd[j] = dsk->xd[0];
	  dsk->yd[j] = dsk->yd[j-1] - (dsf->xd[j] - dsf->xd[j-1])*(dsd->yd[j] + dsd->xd[j-1])/2;
	}
      */
      set_ds_source(dsk, "Hat derivative at \\sigma = %f",dsk->xd[0]);
    }

  opf3 = create_and_attach_one_plot(pr, i_sigmam - i_sigma0 + 1,i_sigmam - i_sigma0 + 1, 0);
  if (opf3 == NULL)	return win_printf_OK("Could not create plot data");
  for (i = 1 ; i < op->n_dat; i++)
    {
      if ((dsk = create_and_attach_one_ds(opf3, i_sigmam - i_sigma0 + 1, i_sigmam - i_sigma0 + 1, 0)) == NULL)
	return win_printf("I can't create ds !");
    }
  opf3->filename = Transfer_filename(op->filename);
  set_plot_x_title(opf3,"\\sigma");
  set_plot_y_title(opf3,"\\Gamma  (pN.nm)");
  uns_op_2_op_by_type(op, IS_Y_UNIT_SET, opf3, IS_X_UNIT_SET);
  uns_op_2_op_by_type(op, IS_X_UNIT_SET, opf3, IS_Y_UNIT_SET);


  for (i = i_sigma0, k = 0; i <= i_sigmam; i++, k++)
    {
      if (k < opf2->n_dat) dsk = opf2->dat[k];
      else win_printf("k = %d out of range %d",k,opf3->n_dat);
      for (j=0 ; j< dsk->nx ; j++)    
	{
	  opf3->dat[j]->xd[k] = dsk->xd[j];
	  opf3->dat[j]->yd[k] = dsk->yd[j]/omega_0;
	  opf3->dat[j]->yd[k] += kBT * Cs * omega_0 * opf3->dat[j]->xd[k]; 
	}
    }
  for (i = 0 ; i < opf3->n_dat; i++)
    {
      dst = opf3->dat[i];
      set_ds_plot_label(dst,(dst->xd[dst->nx-1] + 0.0005), dst->yd[dst->nx-1], 
		USR_COORD, "\\pt6 F = %6.3f pN",dsf->xd[i]);

    }

  free_data_set(dsf);
  free_data_set(dstmp);
  free_data_set(dstmpi);
  free_data_set(dstmpi2);
  return refresh_plot(pr,pr->cur_op);			
}
MENU *wlc_plot_menu(void)
{
  static MENU mn[32] = {0}, mn1[32] = {0}, mn2[32] = {0};

	if (mn[0].text != NULL)	return mn;
	

	add_item_to_menu(mn2,"Hat CLaude pour Francesco ",draw_hat_claude, NULL,0, NULL);
	add_item_to_menu(mn2,"Hat derivative to torque ",concert_hat_to_torque, NULL,0, NULL);
	add_item_to_menu(mn2,"Derivate Hat by poly",derive_hat, NULL,0, NULL);



	add_item_to_menu(mn1,"fit \\xi ",do_fit_xi,NULL, 0, NULL);
	add_item_to_menu(mn1,"fit log \\xi ",do_fit_log_xi,NULL, 0, NULL);
	add_item_to_menu(mn1,"auto fit \\xi ",do_fit_log_xi_auto,NULL, 0, NULL);
	add_item_to_menu(mn1,"auto fit \\xi 2",do_fit_xi_auto_with_er,NULL, 0, NULL);

	add_item_to_menu(mn1,"fit L",do_fit_L,NULL, 0, NULL);	
	add_item_to_menu(mn1,"monte carlo ",do_check_fit_Levenberg_Marquardt,NULL, 0, NULL);		
/*
	add_item_to_menu(mn1,"tanh",convert_tanh,NULL, 0, NULL);
*/
	add_item_to_menu(mn1,"test",test_div,NULL, 0, NULL);
	add_item_to_menu(mn1,"test Ko",test_enthalpy,NULL, 0, NULL);
	add_item_to_menu(mn1,"\\xi eff vs z",xi_effectif_vs_z,NULL, 0, NULL);
	add_item_to_menu(mn1,"\\xi eff vs f",xi_effectif_vs_f,NULL, 0, NULL);
	add_item_to_menu(mn1,"Marko hat",draw_marko_hat	,NULL, 0, NULL);
	add_item_to_menu(mn1,"Marko hat improved",draw_marko_hat_improved ,NULL, 0, NULL);
	add_item_to_menu(mn1,"Marko hat Cs = C",draw_marko_hat_improved_no_Cs ,NULL, 0, NULL);


	add_item_to_menu(mn1,"Fit Marko type hat",fit_marko_type_hat ,NULL, 0, NULL);




	add_item_to_menu(mn1,"Marko hat slope",draw_marko_hat_slope_vs_F  ,NULL, 0, NULL);
	add_item_to_menu(mn1,"Marko hat improved slope",draw_marko_hat_improved_slope_vs_F ,NULL, 0, NULL);
	add_item_to_menu(mn1,"Marko hat improved Cs = C slope",draw_marko_hat_improved_no_Cs_slope_vs_F ,NULL, 0, NULL);
	add_item_to_menu(mn1,"extension order 1",draw_extension_order_1 ,NULL, 0, NULL);




	add_item_to_menu(mn1,"Moroz correction",draw_moroz_hat_correction ,NULL, 0, NULL);
		



	
	add_item_to_menu(mn,"more", NULL, mn1, 0, NULL);
	add_item_to_menu(mn,"more 2", NULL, mn2, 0, NULL);
	



  	add_item_to_menu(mn,"Draw WLC model",draw_wlc_model,NULL, 0, NULL);
	add_item_to_menu(mn,"fit L and \\xi",do_fit_L_auto,NULL, 0, NULL);
	add_item_to_menu(mn,"give \\vec{F} ",do_give_force,NULL, 0, NULL);
	add_item_to_menu(mn,"give extension",do_give_extension,NULL, 0, NULL);
	add_item_to_menu(mn,"FJC",fjc_curve,NULL, 0, NULL);	
	add_item_to_menu(mn,"moroz",moroz_curve,NULL, 0, NULL);
/*		
	add_item_to_menu(mn,"WLC+noise",WLC_gaussian_noise,NULL, 0, NULL);		
	add_item_to_menu(mn,"WLC+log noise",WLC_gaussian_noise_log,NULL, 0, NULL);
*/
	add_item_to_menu(mn,"Levenberg ",do_fit_Levenberg_Marquardt,NULL, 0, NULL);
	add_item_to_menu(mn,"Fit Worm Like Chain ",do_fit_Levenberg_Marquardt_improve,NULL, 0, NULL);
	add_item_to_menu(mn,"MS imp + enthal ",do_fit_Levenberg_Marquardt_improve_enthalpy,NULL, 0, NULL);
	add_item_to_menu(mn,"MS + enthal ",do_fit_Levenberg_Marquardt_enthalpy,NULL, 0, NULL);
	add_item_to_menu(mn,"Fit W3 ",do_fit_Levenberg_Marquardt_w3,NULL, 0, NULL);

	add_item_to_menu(mn,"k_z/k_x fit",fit_kz_over_kx,NULL, 0, NULL);		
	add_item_to_menu(mn,"hat curves",generate_hat_curve,NULL, 0, NULL);		
	add_item_to_menu(mn,"many hat curves",generate_many_hat_curve,NULL, 0, NULL);			
	add_item_to_menu(mn,"Force extension(n)",generate_force_curve_with_rot,NULL, 0, NULL);			
	add_item_to_menu(mn,"Nelson hat",generate_nelson_hat_top_curve,NULL, 0, NULL);		
	add_item_to_menu(mn,"Palindrome n_c(F)",generate_nc_vs_f,NULL, 0, NULL);		


	return mn;
}

int	wlc_main(int argc, char **argv)
{
  (void)argc;
  (void)argv;
    xvplot_main(0, NULL);
    add_plot_treat_menu_item ( "Polymer WLC model", NULL, wlc_plot_menu(), 0, NULL);
    return D_O_K;
}
int	wlc_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;  
        remove_item_to_menu(image_treat_menu, "Polymer WLC model", NULL, NULL);
	return D_O_K;
}

#endif
