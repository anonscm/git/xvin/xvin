/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _PALIER_C_
#define _PALIER_C_

#include <allegro.h>
# include "xvin.h"
# include "../nrutil/nrutil.h"

/* If you include other regular header do it here*/ 

#include <math.h>

/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "palier.h"


int do_palier_hello(void)
{
  int i;

  if(updating_menu_state != 0)	return D_O_K;

  i = win_printf("Hello from palier");
  if (i == WIN_CANCEL) win_printf_OK("you have press WIN_CANCEL");
  else win_printf_OK("you have press OK");
  return 0;
}

int do_palier_rescale_data_set(void)
{
  int i, j;
  O_p *op = NULL;
  int nf;
  static float factor = 1;
  d_s *ds, *dsi;
  pltreg *pr = NULL;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  nf = dsi->nx;	/* this is the number of points in the data set */
  i = win_scanf("By what factor do you want to multiply y %f",&factor);
  if (i == WIN_CANCEL)	return OFF;

  if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");

  for (j = 0; j < nf; j++)
  {
    ds->yd[j] = factor * dsi->yd[j];
    ds->xd[j] = dsi->xd[j];
  }

  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"y multiplied by %f",factor);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}

# define IN_PALIER 1
# define NOMORE_BLOCK 0
# define MAYBE_BLOCK 2

int do_palier_rescale_plot(void)
{
  int i, j;
  O_p *op = NULL, *opn = NULL, *opn2 = NULL;
  int nf, palier;
  static float factor = 1;
  d_s *ds = NULL, *dsi, *dsc = NULL;
  pltreg *pr = NULL;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the y coordinate"
			   "of a data set by a number and place it in a new plot");
    }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  nf = dsi->nx;	/* this is the number of points in the data set */
  i = win_printf("Did you select the X and Y boundaries of the palier ?");
  //i = win_scanf("By what factor do you want to multiply y %f",&factor);
  if (i == WIN_CANCEL)	return OFF;

  if ((dsc = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");



  for (j = 0, palier = NOMORE_BLOCK; j < nf-1; j++)
    {
		if (dsi->xd[j] >= op->x_lo && dsi->xd[j] <= op->x_hi)
		{
		
      if (dsi->yd[j] >= op->y_lo && dsi->yd[j] <= op->y_hi)
	{
          if (palier == NOMORE_BLOCK && dsi->yd[j+1] >= op->y_lo && dsi->yd[j+1] <= op->y_hi)
	    {
	      if (ds == NULL)
		{
		  if ((ds = build_data_set(16, 16)) == NULL)
		    return win_printf_OK("cannot create plot !");
		  ds->nx = ds->ny = 0;
		}
	    }
	  if (dsi->yd[j+1] >= op->y_lo && dsi->yd[j+1] <= op->y_hi)
	    palier = IN_PALIER;
	  if (ds != NULL)  add_new_point_to_ds(ds, dsi->xd[j], dsi->yd[j]);
	}
      if (dsi->yd[j] > op->y_hi)
	{
	  if (ds != NULL) 
	    {
	      inherit_from_ds_to_ds(ds, dsi);
	      set_ds_treatement(ds,"y multiplied by %f",factor);

	      if (palier == IN_PALIER)
		{
		  if (opn2 == NULL)
		    {
		      opn2 = create_one_empty_plot();
		      if (opn2 == NULL)     return win_printf_OK("cannot create plot !");
		      if (add_data_to_pltreg(pr, IS_ONE_PLOT, (void*)opn2))
			return win_printf_OK("cannot attach plot !");
		    }
		  if (add_one_plot_data(opn2, IS_DATA_SET, (void*)ds))
		    return win_printf_OK("cannot attach ds !");
		  ds = NULL;
		}
	      if (palier == MAYBE_BLOCK)
		{
		  if (opn == NULL)
		    {
		      opn = create_one_empty_plot();
		      if (opn == NULL)     return win_printf_OK("cannot create plot !");
		      if (add_data_to_pltreg(pr, IS_ONE_PLOT, (void*)opn))
			return win_printf_OK("cannot attach plot !");
		    }
		  add_new_point_to_ds(ds, dsi->xd[j], dsi->yd[j]);
		  if (add_one_plot_data(opn, IS_DATA_SET, (void*)ds))
		    return win_printf_OK("cannot attach ds !");
		  ds = NULL;
		}
	    }
	  palier = NOMORE_BLOCK;
	}
      if (dsi->yd[j] < op->y_lo) 
	palier = (palier == IN_PALIER || palier == MAYBE_BLOCK) ? MAYBE_BLOCK : NOMORE_BLOCK;
      dsc->xd[j] = dsi->xd[j];
      dsc->yd[j] = palier;
	  
		}
    }

  /* now we must do some house keeping */
  if (opn != NULL)
    {
      set_plot_title(opn, "Block and exit at low force");
      if (op->x_title != NULL) set_plot_x_title(opn, op->x_title);
      if (op->y_title != NULL) set_plot_y_title(opn, op->y_title);
      opn->filename = Transfer_filename(op->filename);
      uns_op_2_op(opn, op);
    }
  if (opn2 != NULL)
    {
      set_plot_title(opn2, "Block and exit at high force");
      if (op->x_title != NULL) set_plot_x_title(opn2, op->x_title);
      if (op->y_title != NULL) set_plot_y_title(opn2, op->y_title);
      opn2->filename = Transfer_filename(op->filename);
      uns_op_2_op(opn2, op);
    }

  /* refisplay the entire plot */
  refresh_plot(pr, pr->n_op-1);
  return D_O_K;
}


int do_palier_simple_plot(void)
{
  int i, j;
  O_p *op = NULL, *opn = NULL, *opn2 = NULL;
  int nf, palier;
  static float factor = 1;
  d_s *dsl = NULL, *dsi, *dsc = NULL, *dsh = NULL;
  pltreg *pr = NULL;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the y coordinate"
			   "of a data set by a number and place it in a new plot");
    }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  nf = dsi->nx;	/* this is the number of points in the data set */
  i = win_printf("Did you select the X and Y boundaries of the palier ?");
  //i = win_scanf("By what factor do you want to multiply y %f",&factor);
  if (i == WIN_CANCEL)	return OFF;

  if ((dsc = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");

  // Nombre de ds incomplets (init. � -1 car le premier cycle est mal d�fini)
  int nb_ds_incomp = -1;

  int palier_1 = -1;
  /* Palier = 1 : on est dans un palier
  Palier = -1 : initialisation
  Palier = 0 : pas dans le palier

  Palier_1 = 1 : le dernier cycle � haute force �tait bloqu�
  palier_1 = 0 : soit le dernier palier n'�tait pas bloqu�, soit on est sur un palier (remise � z�ro)
  */

if   (dsi->yd[0] >= op->y_lo && dsi->yd[0] <= op->y_hi)
	palier = 1;

else if (dsi->yd[0] <= op->y_lo)
	palier=0;

else if (dsi->yd[0] >= op->y_hi)
	palier=2;


for (j = 0; j < nf; j++) // It�ration sur tous les points
    {
		if (dsi->xd[j] >= op->x_lo && dsi->xd[j] <= op->x_hi)
		{
		
      if (palier != 2 && dsi->yd[j] >= op->y_hi)
	 palier = 2;
	 
      if (palier == 2 && dsi->yd[j] >= op->y_lo && dsi->yd[j] <= op->y_hi)
	palier = -1;
	
      if (palier ==-1  && dsi->yd[j] <= op->y_lo)
	palier=0;
      
      if (palier == 2 && dsi->yd[j] <= op->y_lo)
	palier = 0;
	

	
      if (palier ==0 && dsi->yd[j] >= op->y_lo && dsi->yd[j] <= op->y_hi) // Quand on arrive sur le palier
	{
	  

	// D�but du palier
	  palier = 1; 
	  palier_1 = 0;

	  // Dataset des points � haute force bloqu�s
	  if (dsl != NULL && dsl->nx > 0) dsl = NULL;
	  if (dsh == NULL)
	    {
	      if (opn2 == NULL)
		{
		  opn2 = create_and_attach_one_plot(pr,16,16,0);
		  if (opn2 == NULL)     return win_printf_OK("cannot create plot !");
		  dsh = opn2->dat[0];
		}
	      else
		{
		  if ((dsh = create_and_attach_one_ds(opn2, 16, 16, 0)) == NULL)
		    return win_printf_OK("cannot create plot !");
		}
	      dsh->nx = dsh->ny = 0;
	    }
	}
      else if (palier == 1 && (dsi->yd[j] < op->y_lo || dsi->yd[j] > op->y_hi)) // Si on �tait dans le palier et qu'on sort
	{
		  palier_1 = (dsi->yd[j] < op->y_lo) ? 1 : 0;

	  if (palier_1 == 1 && dsl == NULL)
	    {
	      if (opn == NULL)
		{
		  opn = create_and_attach_one_plot(pr,16,16,0);
		  if (opn == NULL)     return win_printf_OK("cannot create plot !");
		  dsl = opn->dat[0];
		}
	      else
		{
		  if ((dsl = create_and_attach_one_ds(opn, 16, 16, 0)) == NULL)
		    return win_printf_OK("cannot create plot !");
		}
	      dsl->nx = dsl->ny = 0;
	    }
	  palier = 0;
	  dsh = NULL;
	}
	  else if (dsi->yd[j] > op->y_hi && palier_1 == 1) // Si on sort par le haut et qu'on faisait un cycle
	  {
		  palier_1 = 0;
		  nb_ds_incomp++;
	  }
      if (palier == 1 && dsh != NULL)  
	add_new_point_to_ds(dsh, dsi->xd[j], dsi->yd[j]);
      if (palier !=1)
	dsh = NULL;
      if (palier_1 == 1 && dsl != NULL)  
	add_new_point_to_ds(dsl, dsi->xd[j], dsi->yd[j]);

   

dsc->xd[j] = dsi->xd[j];
dsc->yd[j] = palier;

		}
}
  // Calcul du temps caract�ristique tau

  int nb_tot = opn->n_dat;
  //float dl = dsi->user_fspare[0]; // R�cup�ration du temps � basse force
  float dl;
  i = win_scanf("Value of time at low force ? %4f\n", &dl);
  if (i == WIN_CANCEL)	return OFF;
  double tau = dl / log((float)nb_tot / (nb_tot - nb_ds_incomp)); // Tps carac dans l'exponentielle

  /* now we must do some house keeping */
  if (opn != NULL)
    {
      set_plot_title(opn, "\\stack{{Block and exit at low force}{%d incomplete ds}{tau = %f}}", nb_ds_incomp, tau);
      if (op->x_title != NULL) set_plot_x_title(opn, op->x_title);
      if (op->y_title != NULL) set_plot_y_title(opn, op->y_title);
      opn->filename = Transfer_filename(op->filename);
      uns_op_2_op(opn, op);
    }
  if (opn2 != NULL)
    {
      set_plot_title(opn2, "Block and exit at high force");
      if (op->x_title != NULL) set_plot_x_title(opn2, op->x_title);
      if (op->y_title != NULL) set_plot_y_title(opn2, op->y_title);
      opn2->filename = Transfer_filename(op->filename);
      uns_op_2_op(opn2, op);
    }

  /* refisplay the entire plot */
  refresh_plot(pr, pr->n_op-1);
  return D_O_K;
}

int do_simul_palier_plot(void)
{
  int i, j;
  O_p *op = NULL;
  int nf, palier;
  static float dh = 1, dl = 5, th = 10, tl = 50, to = 30, fs = 10;
  float rh, rl, ro;
  static int nc = 3000;	
  static int idum = 4587;	
  d_s *ds = NULL;
  pltreg *pr = NULL;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the y coordinate"
			   "of a data set by a number and place it in a new plot");
    }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return win_printf_OK("cannot find data");

  i = win_scanf("Simulate EcoR1 binding signal\n"
		"Specify duration of high force phase %4f\n"
		"Specify duration of low force phase %4f\n"
		"Specify Toff at high force phase %4f\n"
		"Specify Toff  at low force phase %4f\n"
		"Specify Ton  at low force phase %4f\n"		
		"Total number of cycles %4d\n"
		"sampling freq %5f\n"
		,&dh,&dl,&th,&tl,&to,&nc,&fs);		
   if (i == WIN_CANCEL)	return OFF;

  rh = (float)1/(fs*th);
  rl = (float)1/(fs*tl);
  ro = (float)1/(fs*to);

  nf = nc*(dh+dl)*fs;
  if ((op = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  ds = op->dat[0];
  ds->user_fspare[0] = dl; // Sauvegarde du temps � basse force

  for(i = 0, palier = NOMORE_BLOCK; i < nf; i++)
    {
      j = i % (int)((dh+dl)*fs);
      ds->xd[i] = (float)i/fs;      
      if (j < dh*fs)
	{
	  if (palier != NOMORE_BLOCK && ran1(&idum) < rh)
	    palier = NOMORE_BLOCK;
	  ds->yd[i] = (palier == NOMORE_BLOCK) ? 1 : 0.5;
	}
      else
	{
	  if (palier == NOMORE_BLOCK && ran1(&idum) < ro)
	    palier = MAYBE_BLOCK;
	  else if (palier == MAYBE_BLOCK && ran1(&idum) < rl)
	    palier = NOMORE_BLOCK;
	  ds->yd[i] = (palier == MAYBE_BLOCK) ? 0.05 : 0;
	}

    }
  /* refisplay the entire plot */
  set_plot_title(op, "\\stack{{Simulation EcoR1}{Toff_{hf} = %gs, Toff_{lf} = %gs}{Ton %gs f_s = %gHz}}"
		 ,th,tl,to,fs);
  refresh_plot(pr, pr->n_op-1);
  return D_O_K;
}



MENU *palier_plot_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
  //add_item_to_menu(mn,"Hello example", do_palier_hello,NULL,0,NULL);
  //add_item_to_menu(mn,"data set rescale in Y", do_palier_rescale_data_set,NULL,0,NULL);
  add_item_to_menu(mn,"look for palier", do_palier_rescale_plot,NULL,0,NULL);
  add_item_to_menu(mn,"look for palier simple", do_palier_simple_plot,NULL,0,NULL);
  add_item_to_menu(mn,"Simul palier",do_simul_palier_plot ,NULL,0,NULL);

  return mn;
}

int	palier_main(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  add_plot_treat_menu_item ( "palier", NULL, palier_plot_menu(), 0, NULL);
  return D_O_K;
}

int	palier_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  remove_item_to_menu(plot_treat_menu, "palier", NULL, NULL);
  return D_O_K;
}
#endif

