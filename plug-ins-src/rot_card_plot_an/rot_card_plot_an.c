#ifndef ROT_CARD_PLOT_AN_C_
#define ROT_CARD_PLOT_AN_C_


# include <allegro.h>
# include <winalleg.h>
# include <xvin.h>
# include <NIDAQmx.h>

/* If you include other plug-ins header do it here*/ 
#include "../cardinal/cardinal.h"
#include "../trackBead/track_util.h"
//#include "../NIDAQ_commander_rotation_orig/include/global_functions.h"
#include "../nidaq_commander_rotation/nidaq_commander_rotation.h"
#include "../logfile/logfile.h"
#include <gsl/gsl_statistics.h>

/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "rot_card_plot_an.h"

extern g_track *track_info ;

O_p *g_op = NULL;
pltreg *g_pr = NULL;

int do_acquisition_interface(void)
{
 
  if(updating_menu_state != 0)	return D_O_K;		
  
  return D_O_K;
}

saap* init_saap(saap *sa)
{
  if(sa == NULL) 
    sa = (saap*)calloc(1,sizeof(saap));
  if(sa == NULL) xvin_ptr_error(Out_Of_Memory);
  sa->type = FREQUPDATE;
  sa->number_of_data_points_per_point = 2048;
  sa->number_of_data_points = 64;
  sa->freq_min = 0.1;
  sa->freq_factor = 1.15;
  sa->ampl_max = 0.1;
  sa->ampl_factor = 0.;
  return sa;
}

slip* init_slip(slip *sl)
{
  
  if(sl == NULL) 
    sl = (slip*)calloc(1,sizeof(saap));
  if(sl == NULL) return xvin_ptr_error(Out_Of_Memory);
    sl->rf = init_rfp(sl->rf);
    sl->sa = init_saap(sl->sa);
  return sl;
}

int set_slip(slip *sl)
{
  int i=0;
  if(sl == NULL) sl = init_slip(sl);
  i = win_scanf("Acquisition Parameters Setting:\n"
		"Rotation Parameters Setting:\n"
		"sl->rf->num_turns: %R infinite %r one\n"
		"sl->rf->direction: %R CCW %r CW\n"
		"sl->rf->mean_value(only useful for alternating): %R 0 %r 1/2\n"
		"sl->rf->frequency (periods per s): %6f\n"
		"sl->rf->num_points: %6d\n"
		"sl->rf->level [-0.3:0.3]: %6f\n"
		"sl->rf:Phase = %4d/%4d*PI\n"
		"sl->rf:stepped? %R NO %r YES\n"
		"sl->rf->switch_on[0] %R OFF %r ON\n"
		"sl->rf->switch_on[1] %R OFF %r ON\n"
		"sl->rf->switch_on[2] %R OFF %r ON\n",
		"sl->sa->type: %R FREQUPDATE %r AMPLUPDATE\n"
		"sl->sa->number_of_data_points_per_point: %6d\n"
		"sl->sa->number_of_data_points: %6d\n"
		"sl->sa->freq_min: %6f\n"
		"sl->sa->freq_factor: %6f\n"
		"sl->sa->ampl_max: %6f\n"
		"sl->sa->ampl_factor: %6f\n",
		&(sl->rf->num_turns),
		&(sl->rf->direction),
		&(sl->rf->meanvalue),
		&(sl->rf->frequency),
		&(sl->rf->num_points),
		&(sl->rf->level),
		&(sl->rf->phasenum),
		&(sl->rf->phaseden),
		&(sl->rf->stepflag),
		&(sl->rf->switch_on[0]),
		&(sl->rf->switch_on[1]),
		&(sl->rf->switch_on[2]),
		&(sl->sa->type),
		&(sl->sa->number_of_data_points_per_point),
		&(sl->sa->number_of_data_points),
		&(sl->sa->freq_min),
		&(sl->sa->freq_factor),
		&(sl->sa->ampl_max),
		&(sl->sa->ampl_factor));
    if((i = check_slip(sl))!=0) {
      win_printf("something is wrong!\ncheck slip returned %d",i);
      i =CANCEL;
    }
    return i;
}


int check_saap(saap *sa)
{
  int ret = 0;
  if(sa->type != 0 && sa->type != 1) return ret = -9;
  if(sa->number_of_data_points_per_point < 0) return ret = -10;
  if(sa->number_of_data_points <0) return ret -11;
  if(sa->freq_min < 0)return -12;
  if(sa->freq_factor < 0) return -13;
  if(sa->ampl_max < 0) return -14;
  if(sa->ampl_factor < 0) return -15;
  if(sa->number_of_data_points_per_point > TRACKING_BUFFER_SIZE)
    return -16;
  return ret;
}


int check_slip(slip *sl)
{
  int ret = 0;
  float max_frequency = 0;
  if((ret = check_rfp(sl->rf))!=0) return ret;
  if((ret = check_saap(sl->sa))!=0) return ret;
  IO__NIDAQ_max_frequency(&max_frequency);
  if((sl->rf->num_points*sl->sa->freq_min*
      ((float)pow(sl->sa->freq_factor,sl->sa->number_of_data_points)))
     >210000) return -17;
  return ret;
}

int do_rfp_update(slip *sl,int stepnumber)
{		  
  float ampl = 0;
  float freq = 0;
  if(sl == NULL) sl = init_slip(sl);
  if(sl->sa->type == FREQUPDATE){
    freq = sl->sa->freq_min * (float)pow(sl->sa->freq_factor,stepnumber);
    sl->rf->frequency = freq;
    return 0;
  }
  else if(sl->sa->type == AMPLUPDATE){
    ampl = sl->sa->ampl_max - sl->sa->ampl_factor*stepnumber;
    sl->rf->level = ampl;
  }
  return 0;
}

DWORD WINAPI slippage_function_thread(LPVOID lpParam)
{
  return D_O_K;
}

int launcher__slippage_function_thread(void)
{
    HANDLE hThread = NULL;
    DWORD dwThreadId;
    if(updating_menu_state != 0)	return D_O_K;
    hThread = CreateThread(
            NULL,              // default security at_per_pointtributes
            0,                 // use default stack size
            slippage_function_thread,// thread function
            NULL,        // argument to thread function
            0,                 // use default creation flags
            &dwThreadId);      // returns the thread identifier

    if (hThread == NULL) return win_printf("no thread created");
    SetThreadPriority(hThread,  THREAD_PRIORITY_LOWEST);

    return D_O_K;
}

int slippage_function(void)
{
  slip *sl =NULL;
  int i ;
  pltreg *pr = NULL;
  d_s *ds = NULL;
  O_p *op = NULL;
  char source[256];
  char plot_title[512];
  char x[2] = "x";
  char y[2] = "y";
  //to be done better!
  int number_of_data_points = 0;
  int number_of_data_points_per_point = 0;
  float frequency_min = 0;
  float freq_factor = 0;

  if(updating_menu_state != 0)	return D_O_K;	

  sl = init_slip(sl);
  number_of_data_points = sl->sa->number_of_data_points;
  number_of_data_points_per_point = sl->sa->number_of_data_points_per_point;
  frequency_min = sl->sa->freq_min;
  freq_factor = sl->sa->freq_factor;

  if(sl == NULL) return win_printf("sl == NULL!");
  if (track_info == NULL) return win_printf_OK("NO track info!");
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)//to be changed so that I can start routine from image region as well
    return(win_printf("can't find plot region"));
  g_pr = pr;
  
  //setting all parameters
  i = set_slip(sl);
  if(i==CANCEL) return D_O_K;
  
  sprintf(plot_title,"F min %4.3fHz freqfactor %4.3f L %4.2fV",frequency_min,freq_factor,sl->rf->level*10); 
  
  if((op = create_and_attach_one_plot(pr,  number_of_data_points_per_point,
				  number_of_data_points_per_point, 0)) == NULL)
    return win_printf("cannot create plot");

  g_op = op; //setting the global plot pointer

  set_op_filename(op, "Slippagedata.gr");
  set_plot_title(op,plot_title);
  ds = op->dat[0];
  sprintf(source," x frequency %f",frequency_min);
  set_ds_source(ds,source );

  for ( i = 0 ; i < 2 *number_of_data_points-1 ; i++)
    {
      if ((ds = create_and_attach_one_ds(op, number_of_data_points_per_point, number_of_data_points_per_point, 0)) == NULL)
	return win_printf_OK("I can't create ds !");
      sprintf(source,"%s frequency %f",(i%2 == 0)? x : y, frequency_min*(float)pow(freq_factor,(i/2)));
      set_ds_source(ds,source );
    }
  win_printf("I created all the stuff. now I begin!");
  slippage_loop(sl);
  
  treat_slippage_data_with_parameters(op,sl->rf->level,frequency_min,freq_factor,number_of_data_points_per_point,number_of_data_points);

  sl->rf->frequency = frequency_min;
  AOTF__rotating_B_all_gparam_rf(sl->rf);
  return D_O_K;		
}

int slippage_loop(slip *sl)
{
  register int i = 0, j =0;
  O_p *op = g_op;
  pltreg *pr = g_pr;
  int count = 0;
  int number_of_data_points = sl->sa->number_of_data_points;
  int number_of_data_points_per_point = sl->sa->number_of_data_points_per_point;
  int starting_image = -1;
  int starting_image_buffer = -1;
  
  for ( i = 0 ; i < 2*number_of_data_points ; i=i+2)
    {
      count = 0;
      do_rfp_update(sl,i/2);
      AOTF__rotating_B_all_gparam_rf(sl->rf);
      Sleep (500);
      starting_image = track_info->lac_i ;
      starting_image_buffer = track_info->lc_i ;
      while (track_info->lac_i < starting_image + number_of_data_points_per_point )
	{
	  if ( keypressed() == TRUE ) return D_O_K;
	  Sleep(100);
	}
      for (j = 0 ; j < number_of_data_points_per_point ;j ++)
	{
	  if (track_info->bd[0]->n_l[j] == 1) count ++;
	  op->dat[i]->xd[j]=track_info->limi[(starting_image_buffer + j)%TRACKING_BUFFER_SIZE];
	  op->dat[i+1]->xd[j]=track_info->limi[(starting_image_buffer + j)%TRACKING_BUFFER_SIZE];
	  op->dat[i]->yd[j]= track_info->bd[0]->x[(starting_image_buffer + j)%TRACKING_BUFFER_SIZE];
	  op->dat[i+1]->yd[j]= track_info->bd[0]->y[(starting_image_buffer + j)%TRACKING_BUFFER_SIZE];
	  op->need_to_refresh |=1;
	  
	}
      refresh_plot(pr, UNCHANGED);
      if (count > 10) return win_printf_OK("Lost bead");
      if (count > 0) i-=2;
    }
  return 0;
}

int treat_slippage_data_with_parameters(O_p *op, float amplitude, float frequency_min, float freq_factor, int number_of_data_points_per_point, int number_of_data_points)
{	
  O_p *op_slippage = NULL;
  d_s *x_slippage = NULL , *y_slippage = NULL, *avg_slippage = NULL; 
  int i , j;
  double data[number_of_data_points_per_point];
  char source[256];
  char plot_title[512];
  pltreg *pr = NULL;
  
  if (op == NULL) return D_O_K;

  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return(win_printf("can't find plot region"));
  
  op_slippage = create_and_attach_one_plot(pr,  number_of_data_points, number_of_data_points, 0);
  if (op_slippage == NULL) return D_O_K;
  
  set_op_filename(op, "Slippageresults.gr");
  set_plot_title(op,plot_title);
  x_slippage = op_slippage->dat[0];
  sprintf(source," x slippage");
  set_ds_source(x_slippage,source );
  
  if ((y_slippage = create_and_attach_one_ds(op_slippage, number_of_data_points, number_of_data_points, 0)) == NULL)
    return win_printf_OK("I can't create ds !");
  sprintf(source," y slippage");
  set_ds_source(y_slippage,source );
  
  if ((avg_slippage = create_and_attach_one_ds(op_slippage, number_of_data_points, number_of_data_points, 0)) == NULL)
    return win_printf_OK("I can't create ds !");
  sprintf(source," avg slippage");
  set_ds_source(avg_slippage,source );
  
  for (i = 0 ; i < number_of_data_points ; i++)
    {
      x_slippage->xd[i] = y_slippage->xd[i] = avg_slippage->xd[i] =frequency_min*(float)pow(freq_factor,i);//Attention si log
      
      for (j = 0; j < number_of_data_points_per_point; j++) data[j] = op->dat[2*i]->yd[j];
      x_slippage->yd[i] = (float) gsl_stats_sd (data, 1, number_of_data_points_per_point) ;
      
      for (j = 0; j < number_of_data_points_per_point; j++) data[j] = op->dat[2*i+1]->yd[j];
      y_slippage->yd[i] =  (float) gsl_stats_sd (data, 1, number_of_data_points_per_point) ;
      
      avg_slippage->yd[i] = (x_slippage->yd[i] + y_slippage->yd[i])/2;
    }    
  // return refresh_plot(pr, UNCHANGED);
  return D_O_K;
}        


int do_analysis_interface(void)
{
  
  register int i,j;
  pltreg *pr;
  O_p *op_meany, *op_my2, *op_my4;
  d_s *ds, *ds_freq, *ds_meany, *ds_my2, *ds_my4;
  int win_flag;
  int np;
  
  if(updating_menu_state != 0)	return D_O_K;		
  
  win_flag = RETRIEVE_MENU_INDEX;
  
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return(win_printf("can't find plot region"));
  
  np = pr->n_op; //number of plots in the plot region
  
  if((i = win_printf("I detected %d plots in the plot region.\n"
		     "If they are not all to be analysed please kill\n"
		     "the unnecessary ones and then come back!\n"
		     "Remember that the last plot should contain the frequencies!\n",np
		     )) == CANCEL) return D_O_K;
  
  op_meany = create_one_plot(np-1,np-1,0);
  create_and_attach_one_ds(op_meany,np-1,np-1,0);
  op_my2 = create_one_plot(np-1,np-1,0);
  create_and_attach_one_ds(op_my2,np-1,np-1,0);
  op_my4 = create_one_plot(np-1,np-1,0);
  create_and_attach_one_ds(op_my4,np-1,np-1,0);

  ds_freq = pr->o_p[np-1]->dat[0];
  
  for(j = 0 ;j < 2; j++){  
      ds_meany = op_meany->dat[j];
      ds_my2 = op_my2->dat[j];
      ds_my4 = op_my4->dat[j];
    for(i=0;i<np-1;i++){
      ds = pr->o_p[i]->dat[j];
      ds_meany->xd[i] = ds_my2->xd[i] = ds_my4 -> xd[i] = ds_freq->yd[i];
      mean_y2_on_array(ds->yd, ds->nx, win_flag, ds_meany->yd+i , ds_my2->yd+i, ds_my4->yd+i);
    }
    set_ds_dot_line(ds_meany);
    set_ds_dot_line(ds_my2);
    set_ds_dot_line(ds_my4);
    set_ds_point_symbol(ds_meany,"\\oc");
    set_ds_point_symbol(ds_my2,"\\oc");
    set_ds_point_symbol(ds_my4,"\\oc");
    
  }

  set_plot_title(op_meany,"Mean value");
  set_plot_title(op_my2,"second moment");
  set_plot_title(op_my4,"fourth moment");
  set_plot_x_log(op_meany);
  set_plot_x_log(op_my2);
  set_plot_x_log(op_my4);
  set_plot_x_title(op_meany,"Frequency (Hz)");
  set_plot_x_title(op_my2,"Frequency (Hz)");
  set_plot_x_title(op_my4,"Frequency (Hz)");

  op_meany-> need_to_refresh |= 1;
  op_my2-> need_to_refresh |= 1;
  op_my4-> need_to_refresh |= 1;
  
  add_data(pr,IS_ONE_PLOT,op_meany);
  add_data(pr,IS_ONE_PLOT,op_my2);
  add_data(pr,IS_ONE_PLOT,op_my4);
  
  return D_O_K;

}



MENU *rot_card_plot_an_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	//add_item_to_menu(mn,"do acquisition", do_acquisition_interface ,NULL,0,NULL);
	add_item_to_menu(mn,"slippage function", slippage_function,NULL,0,NULL);
	//add_item_to_menu(mn,"slippage function thread", launcher__slippage_function_thread,NULL,0,NULL);
	add_item_to_menu(mn,"do analysis", do_analysis_interface ,NULL,0,NULL);
	return mn;
}

MENU *rot_card_plot_an_image_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	//add_item_to_menu(mn,"do acquisition", do_acquisition_interface ,NULL,0,NULL);
	add_item_to_menu(mn,"slippage function", slippage_function,NULL,0,NULL);
	//add_item_to_menu(mn,"slippage function thread", launcher__slippage_function_thread,NULL,0,NULL);
	//add_item_to_menu(mn,"do analysis", do_analysis_interface ,NULL,0,NULL);
	return mn;
}

int	rot_card_plot_an_main(int argc, char **argv)
{
  //ask vincent how to check if a plug-in is loaded already
  //cardinal_main();
  //nidaq_commander_rotation_main();
  //trackBead_main();
  add_plot_treat_menu_item ( "rot_card_plot_an", NULL, rot_card_plot_an_plot_menu(), 0, NULL);
  add_image_treat_menu_item ( "rot_card_plot_an", NULL, rot_card_plot_an_image_menu(), 0, NULL);
  return D_O_K;
}

int	rot_card_plot_an_unload(int argc, char **argv)
{
	remove_item_to_menu(plot_treat_menu, "rot_card_plot_an", NULL, NULL);
	return D_O_K;
}

#endif
