#ifndef ROT_CARD_PLOT_AN_H_
#define ROT_CARD_PLOT_AN_H_


#define FREQUPDATE 0
#define AMPLUPDATE 1

typedef struct slippage_automatic_acquisition_parameters
{
  int type;
  int number_of_data_points_per_point;
  int number_of_data_points;
  float freq_min;
  float freq_factor;
  float ampl_max;
  float ampl_factor;
} saap;

typedef struct slippage
{
  rfp *rf;
  saap *sa;
} slip;

PXV_FUNC(rfp*, init_rfp,(rfp *rf));
PXV_FUNC(saap*, init_saap,(saap *sa));
PXV_FUNC(slip*, init_slip,(slip *sl));
PXV_FUNC(int, set_slip,(slip *sl));
PXV_FUNC(int, check_saap,(saap *sa));
PXV_FUNC(int, check_slip,(slip *sl));
PXV_FUNC(int, do_rfp_update,(slip *sl,int stepnumber));
PXV_FUNC(int, slippage_loop,(slip *sl));
PXV_FUNC(MENU*, rot_card_plot_an_plot_menu, (void));
PXV_FUNC(int, treat_slippage_data_with_parameters,(O_p *op, float amplitude, float frequency_min,float freq_factor,int number_of_data_points_per_point,int number_of_data_points));
PXV_FUNC(int,slippage_function,(void));
PXV_FUNC(int, rot_card_plot_an_main, (int argc, char **argv));

#endif

