
/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _FITINTERFERENCE_C_
#define _FITINTERFERENCE_C_

# include "allegro.h"
# include "xvin.h"
# include "math.h"
# include "fftbtl32n.h"
# include "fillibbt.h"

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled
# include "../nrutil/nrutil.h"

#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
//place here other headers of this plugin 

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "FitInterference.h"

# define 	WRONG_ARGUMENT		-1
# define 	OUT_MEMORY			-2
# define 	FFT_NOT_SUPPORTED	-15
# define	PB_WITH_EXTREMUM	2
# define	TOO_MUCH_NOISE		4
# define	GETTING_REF_PROFILE	1


int rm_low_modes_in_auto_convolution = 0;
int noise_from_auto_conv = 2;
int auto_conv_norm = 0;
int filter_after_conv = 1;



//float a = ran1(&idum); a random ds [0, 1]


int do_FitInterference_hello(void)
{
  register int i;

  if(updating_menu_state != 0)	return D_O_K;

  i = win_printf("Hello from FitInterference");
  if (i == CANCEL) win_printf_OK("you have press CANCEL");
  else win_printf_OK("you have press OK");
  return 0;
}


int do_fill_gaussian_photons(d_s *ds, float sig, int *idum)
{
  register int j;
  float tmp, val;
  

  if (ds == NULL) return -1;

  for (j = 0; j < ds->nx; )
    {
      tmp = 5 * sig * (2*ran1(idum) - 1);
      val = exp(-(tmp*tmp)/(2*sig*sig));
      if (ran1(idum) < val)
	{
	  ds->yd[j] = 1;
	  ds->xd[j] = tmp;
	  j++;
	  
	}
    }
  return 0;
}

int ds_barycentre(d_s *ds, float *barycentre)
{
  register int i;
  double tmp;

  if (ds == NULL || ds->nx < 1) return 1;
  for (i = 0, tmp = 0; i < ds->nx; i++) tmp += ds->xd[i];
  if (barycentre != NULL) *barycentre = tmp/ds->nx;
  return 0;
}

//----------Section 1 By Samar ------------------

int do_fill_gaussian_Photons_Distribution(d_s *ds, float sig, int *idum, int nf,float X_Shift)
{
  register int j;
  float tmp, val;
  int X_Lower=-((int) round(ds->nx/2)),X_Upper=((int) round(ds->nx/2));
  //X_Shift=100;
  if (ds == NULL) return -1;

 for (j = 0; j <ds->nx;j++ )
   {
     ds->yd[j]= 0;//initiate the coordenates of the Data-Set 
 ds->xd[j] =j+ X_Lower;

   }

  for (j = 0; j <nf; )
    {
      tmp = X_Lower + ran1(idum)*(X_Upper - X_Lower);// a+Ra(b-a) where Ra is the random variable
 if ((round(tmp-X_Lower+X_Shift)>=0)&&(round(tmp-X_Lower+X_Shift)<ds->nx))
   {      
  val = exp(-(tmp*tmp)/(2*sig*sig));
      if (ran1(idum) < val)
	{
	 
	  ds->yd[(int)round(tmp-X_Lower+X_Shift)] += 1;
	  ds->xd[(int)round(tmp-X_Lower+X_Shift)] = round(tmp+X_Shift);
	  j++;
	}
   }    
}
  return 0;
}

//-----------End Section 1 By Samar-----------------
int do_fill_gaussian_interference_photons(d_s *ds, float sig, float lamb, int *idum)
{
  register int j;
  float tmp, val;

  if (ds == NULL) return -1;

  for (j = 0; j < ds->nx; )
    {
      tmp = 5 * sig * (2*ran1(idum) - 1);
      val = (1+cos (2*M_PI*tmp/lamb)) * 0.5 *exp(-tmp*tmp/(2*sig*sig));
      if (ran1(idum) < val)
	{
	  ds->yd[j] = 1;
	  ds->xd[j] = tmp;
	  j++;
	}
    }
  return 0;
}

//----------Section 2 By Samar------------------

int do_fill_gaussian_Interference_Photons_Distribution(d_s *ds, float sig, float lamb, int *idum, int nf,float X_Shift)
{
  register int j;
  float tmp, val;
  int X_Lower=-((int) round((ds->nx)/2)),X_Upper=((int) round((ds->nx)/2));

  if (ds == NULL) return -1;

 for (j = 0; j <ds->nx; j++)
   {
     ds->yd[j]= 0;//initiate the coordenates of the Data-Set 
 ds->xd[j] =j+ X_Lower;

   }

  for (j = 0; j <nf; )
    {
      tmp = X_Lower + ran1(idum)*(X_Upper - X_Lower);// a+Ra(b-a) where Ra is the random variable
 if ((round(tmp-X_Lower+X_Shift)>=0)&&(round(tmp-X_Lower+X_Shift)<ds->nx))
   {      
val = (1+cos (2*M_PI*tmp/lamb)) * 0.5 *exp(-tmp*tmp/(2*sig*sig));
      if (ran1(idum) < val)
	{
	 
	  ds->yd[(int)round(tmp-X_Lower+X_Shift)] += 1;
	  ds->xd[(int)round(tmp-X_Lower+X_Shift)] = round(tmp+X_Shift);
	  j++;
	}
   }    
 
}

  return 0;
}

// ----End Section 2By Samar-----------------------
int do_FitInterference_generate_plot(void)
{
  register int i, j;
  O_p  *opn = NULL, *opb = NULL;
  //float *BaryCentre = ((float*)calloc(1, sizeof(float)); 
  float BaryCentre;
  static int nf = 10000, eqn = 0, nba = 1;
  static float x0 = 0, lamb = 1, sig = 2;
  d_s *ds, *dsb = NULL;
  //float tmp, val;
  pltreg *pr = NULL;
  static int idum = 56;
  //float l = sig/3;
  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number and place it in a new plot");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return win_printf_OK("cannot find data");

  i = win_scanf("This function create a random photon distribution\n"
		"corresponding to an interference patern with one of these equations:\n\n"
		"%R exp-(x^2/2*\\sigma^2)\n\n"
		"%r ([1 + cos ({2\\pi (x - x_0)}\{\\lambda})]/2)*exp-(x^2/2*\\sigma^2)\n\n"
		"Specify x_0 %8f, \\lambda  %8f, \\sigma %8f\n"
		"With howmany photons %8d\n"
		"Random seed %8d\n"
		"Repeat this operation %8d times and compute barycenter\n"
		,&eqn,&x0,&lamb,&sig,&nf,&idum,&nba);
  if (i == CANCEL)	return OFF;

  if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  ds = opn->dat[0];

  if (nba > 1)
    {
      if ((opb = create_and_attach_one_plot(pr, nba, nba, 0)) == NULL)
	return win_printf_OK("cannot create plot !");
      dsb = opb->dat[0];
      set_plot_y_title(opb, "Barycenter Position");
    }
  /*
  for (j = 0; j < nf; )
    {
      tmp = 5 * sig * (2*ran1(&idum) - 1);
      val = (eqn == 0) ? exp(-(tmp*tmp)/(2*sig*sig)) : (1+cos (2*M_PI*tmp/lamb)) * 0.5 *exp(-tmp*tmp/(2*sig*sig));
      if (ran1(&idum) < val)
	{
	  ds->yd[j] = 1;
	  ds->xd[j] = tmp;
	  j++;
	}
    }
  */


  for (j = 0;j < nba; j++)
    {
      if (eqn == 0)   do_fill_gaussian_photons(ds, sig, &idum);
      else            do_fill_gaussian_interference_photons(ds, sig, lamb, &idum);
      ds_barycentre(ds, &BaryCentre);
      if (dsb != NULL)
	 {  
	   dsb->xd[j] = j;
	   dsb->yd[j] = BaryCentre;
	 }
      else win_printf(" The value of the barycenter is %g",BaryCentre);
    } 


  /* now we must do some house keeping */
    set_ds_source(ds,"interence pattern x0 %g lambda %g sigma %g",x0,lamb,sig);
  set_plot_title(opn,"interence pattern x0 %g lambda %g sigma %g",x0,lamb,sig);
  set_plot_x_title(opn, "Position");
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}

//-----Section_ 3  Writen By Samar to be validated---------------

int do_FitInterference_Photon_Distribution(void)
{
  //register int i, j;
  register int i;
  O_p  *opd = NULL, *opb = NULL;
  //float *BaryCentre = ((float*)calloc(1, sizeof(float)); 
  static int nf = 10000, eqn = 0, nba = 1,Nb_Pixel=256,Shift=0;
  static float x0 = 0, lamb = 1, sig = 2;
  d_s *dsd, *dsb = NULL;
  //float tmp, val;
  pltreg *pr = NULL;
  static int idum = 56;
  //float l = sig/3;
  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number and place it in a new plot");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return win_printf_OK("cannot find data");

  i = win_scanf("This function simulate a random photon distribution\n"
		"corresponding to an interference patern with one of these equations:\n\n"
		"%R exp-(x^2/2*\\sigma^2)\n\n"
		"%r ([1 + cos ({2\\pi (x - x_0)}\{\\lambda})]/2)*exp-(x^2/2*\\sigma^2)\n\n"
		"Specify x_0 %8f, \\lambda  %8f, \\sigma %8f\n"
		"With how many photons %8d\n"
"How many pixel in the x axis %8d"
		"Random seed %8d\n"
"Shift the distribution by  %8d \n" 
		,&eqn,&x0,&lamb,&sig,&nf,&Nb_Pixel,&idum,&Shift);
  if (i == CANCEL)	return OFF;

  if ((opd = create_and_attach_one_plot(pr, Nb_Pixel,Nb_Pixel, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  dsd = opd->dat[0];
  set_plot_y_title(opd, " Photon distribution");


  if (nba > 1)// it is not used here
    {
      if ((opb = create_and_attach_one_plot(pr, nba, nba, 0)) == NULL)
	return win_printf_OK("cannot create plot !");
      dsb = opb->dat[0];
      set_plot_y_title(opb, "Barycenter Position");
    }
  /*
  for (j = 0; j < nf; )
    {
      tmp = 5 * sig * (2*ran1(&idum) - 1);
      val = (eqn == 0) ? exp(-(tmp*tmp)/(2*sig*sig)) : (1+cos (2*M_PI*tmp/lamb)) * 0.5 *exp(-tmp*tmp/(2*sig*sig));
      if (ran1(&idum) < val)
	{
	  ds->yd[j] = 1;
	  ds->xd[j] = tmp;
	  j++;
	}
    }
  */


  
    
  if (eqn == 0)   do_fill_gaussian_Photons_Distribution(dsd, sig, &idum,nf,Shift);
  else           do_fill_gaussian_Interference_Photons_Distribution(dsd, sig,lamb, &idum,nf,Shift);
      



  /* now we must do some house keeping */
    set_ds_source(dsd,"interence pattern x0 %g lambda %g sigma %g",x0,lamb,sig);
  set_plot_title(opd,"interence pattern x0 %g lambda %g sigma %g",x0,lamb,sig);
  set_plot_x_title(opd, "Position");
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}




int deplace(float *x, int nx)
{
    register int i, j;
    float tmp;

    for (i = 0, j = nx/2; j < nx ; i++, j++)
    {
        tmp = x[j];
        x[j] = x[i];
        x[i] = tmp;
    }
    return 0;
}
int fftwindow_flat_top1(fft_plan *fp, int npts, float *x, float smp)
{
    register int i, j;
    //int n_2, n_4;
    float tmp; //, *offtsin;

    if (fp == NULL )  return 1;
    //offtsin = get_fftsin();
    (void) npts;
    //if ((j = fft_init(npts)) != 0)  return j;

    //n_2 = npts/2;
    //n_4 = n_2/2;
    x[0] *= smp;
    smp += 1.0;
    for (i=1, j=fp->n_4-2; j >=0; i++, j-=2)
    {
        tmp = (smp - fp->fftbtsin[j]);
        x[i] *= tmp;
        x[fp->nx-i] *= tmp;
    }
    for (i=fp->n_4/2, j=0; i < fp->n_4 ; i++, j+=2)
    {
        tmp = (smp + fp->fftbtsin[j]);
        x[i] *= tmp;
        x[fp->nx-i] *= tmp;
    }
    return 0;
}



/*		correlate "x1" with its invert over "nx" points, returns the correlation
 *		in "fx1". "fx1" will be used to compute fft and result 
 *		setting them to NULL will allocate space. "window" will enable
 *		a windowing process if != 0 a double windowing if = to 2.
 */
int	correlate_1d_sig_and_invert(float *x1, int nx, int window, float *fx1, int lfilter, int fil_width, int fil_type, int remove_dc, fft_plan *fp, filter *fil, float *noise)
{
    register int i, j;
    int n_2;
    float moy, smp = 0.05, tmpf;
    float m1, m2, ms, md, re1, re2, im1, im2;

    if (x1 == NULL)				return WRONG_ARGUMENT;
    if (fp == NULL || fil == NULL)          return FFT_NOT_SUPPORTED;
    if (fftbt_init(fp, nx) == NULL)         return FFT_NOT_SUPPORTED;
    //if (fft_init(nx) || filter_init(nx))	return FFT_NOT_SUPPORTED;
    if (fx1 == NULL)	fx1 = (float*)calloc(nx,sizeof(float));
    if (fx1 == NULL)			return OUT_MEMORY;

    n_2 = nx/2;

    // we cleam mean values
    for(i = 0; i < nx; i++)		fx1[i] = x1[i];
    for(i = 0, j = (3*nx)/4, moy = 0; i < nx/4; i++, j++)		
        moy += fx1[i] + fx1[j];	
    for(i = 0, moy = (2*moy)/nx; remove_dc && i < nx; i++)	
        fx1[i] = x1[i] - moy;
    if (window == 1)	fftbtwindow1(fp, fx1, smp);
    else if (window == 2)	fftwindow_flat_top1(fp, nx, fx1, smp);

    /*	compute fft of x1 as real data */    
    realtr1bt(fp, fx1);
    fftbt(fp, fx1, 1);
    realtr2bt(fp, fx1, 1);


    if (noise && (noise_from_auto_conv == 0))
    {    // we estimate noise on convolution by using the high frequency modes
        for (j = 0, i=3*nx/4, m1 = 0; i< nx; i++, j++)
            m1 += fx1[i] * fx1[i];
        m1 = (j) ? m1/j : m1;
        m1 *= n_2;   // we extend the noise to all modes
        *noise = sqrt(m1);
    }

    //we filter low modes
    //for (i = 1; i < rm_low_modes_in_auto_convolution; i++)    fx1[i] = 0;  


    if (filter_after_conv == 0)
      {
	if (fil_type == LOW_PASS && lfilter> 0 )		
	  lowpass_smooth_half_bt(fil, nx, fx1, lfilter);
	else if (fil_type == BAND_PASS && lfilter> 0 && fil_width > 0 )		 
	  bandpass_smooth_half_bt (fil, nx, fx1, lfilter, fil_width);
      }
    /*	compute normalization without DC and Nyquist*/
    for (i=0, ms = md = 0; i< nx; i+=2)
    {
        tmpf = fx1[i] * fx1[i] + fx1[i+1] * fx1[i+1];
        ms += tmpf;
        md += tmpf * i * i;  // we compute derivative amplitude
    }
    md *= (M_PI * M_PI)/(nx * nx);
    //m1 /= 2;	
    m1 = (ms == 0) ? 1 : ms;
    if (auto_conv_norm == 0)      m1 = 1;
    else if (auto_conv_norm == 1) m1 = (float)1/m1;
    else if (auto_conv_norm == 2) m1 = (float)1/sqrt(m1);

    /* if m1 = 0, the normalization still needs to multiply by nx/2 to reach \Sigma_i=1^N Si * Si' 
       if we assume shot noise in the profile with variance \sigma_n, the error
       on the convolution error is \sigma_c^2 = 4 * \Sigma_i=1^N Si * Si' * \sigma_n^2

       To determine in the error on the position of the maximum, one needs to compute
       the derivative of the maximum and the noise of this derirative, the error on this 
       derivatives equals \sigma_d^2 = 4 * \Sigma_i=1^N \Delta Si * \Delta Si' * \sigma_n^2
       without filtering.
       */

    /*	compute correlation in Fourier space */
    for (i=2; i< nx; i+=2)
    {
        re1 = fx1[i];		re2 = fx1[i];
        im1 = fx1[i+1];		im2 = -fx1[i+1];
        fx1[i] 		= (re1 * re2 + im1 * im2)*m1;
        fx1[i+1] 	= (im1 * re2 - re1 * im2)*m1;
    }
    fx1[0]	= 2*(fx1[0] * fx1[0])*m1;	/* these too mode are special */
    fx1[1] 	= 0;//2*(fx1[1] * fx1[1])/m1;   may be to correct with remove_dc

    if (noise && noise_from_auto_conv ==  1)
    {    // we estimate noise on convolution by using the high frequency modes
        for (j = 0, i=3*nx/4, m1 = 0; i< nx; i++, j++)
            m1 += fx1[i] * fx1[i];
        m1 = (j) ? m1/j : m1;
        m1 *= n_2;   // we extend the noise to all modes
    }

    if (noise && noise_from_auto_conv == 2)
    {    // we estimate noise using the derivative of the autoconvolution
        if (auto_conv_norm == 0)      m1 = md;
        else if (auto_conv_norm == 1) m1 = md/ms;
        else if (auto_conv_norm == 2) m1 = md/sqrt(ms);
        m1 = 4*m1/n_2;
    }

    if (filter_after_conv)
      {
	if (fil_type == LOW_PASS && lfilter > 0 )		
	  lowpass_smooth_half_bt(fil, nx, fx1, lfilter);
	else if (fil_type == BAND_PASS && lfilter > 0 && fil_width > 0 )		 
	  bandpass_smooth_half_bt (fil, nx, fx1, lfilter, fil_width);
      }

    if (noise && noise_from_auto_conv)
    {
        if (lfilter> 0)
        {
            for (i = 0, m2 = 0; i < n_2; i++)
                m2 += fil->f[i];// * fil->f[i];
            m2 /= n_2;
            m1 *= m2;           // we take in account the filter
        }
        *noise = sqrt(m1);
    }
    /*	get back to real world */
    realtr2bt(fp, fx1, -1);
    fftbt(fp, fx1, -1);
    realtr1bt(fp, fx1);
    deplace(fx1, nx);
    return 0;
}	


/*	Find maximum position in a 1d array, the array is supposed periodic
 *	return 0 -> everything fine, TOO_MUCH_NOISE or PB_WITH_EXTREMUM
 *
 */
int	find_max1(float *x, int nx, float *Max_pos, float *Max_val, float *Max_deriv, int max_limit)
{
    register int i;
    int  nmax, ret = 0, delta, imin, imax;
    double a, b, c, d, f_2, f_1, f0, f1, f2, xm = 0;

    if (max_limit < 2) max_limit = nx/2;
    imin = nx/2 - max_limit;
    imin = (imin < 0) ? 0 : imin;
    imax = nx/2 + max_limit;
    imax = (imax >= nx) ? nx : imax;

    for (i = imin, nmax = 0; i < imax; i++) nmax = (x[i] > x[nmax]) ? i : nmax;
    f_2 = x[(nx + nmax - 2) % nx];
    f_1 = x[(nx + nmax - 1) % nx];
    f0 = x[nmax];
    f1 = x[(nmax + 1) % nx];
    f2 = x[(nmax + 2) % nx];
    if (f_1 < f1)
    {
        a = -f_1/6 + f0/2 - f1/2 + f2/6;
        b = f_1/2 - f0 + f1/2;
        c = -f_1/3 - f0/2 + f1 - f2/6;
        d = f0;
        delta = 0;
    }	
    else
    {
        a = b = c = 0;	
        a = -f_2/6 + f_1/2 - f0/2 + f1/6;
        b = f_2/2 - f_1 + f0/2;
        c = -f_2/3 - f_1/2 + f0 - f1/6;
        d = f_1;
        delta = -1;
    }
    if (fabs(a) < 1e-8)
    {
        if (b != 0)	xm =  - c/(2*b) - (3 * a * c * c)/(4 * b * b * b);
        else
        {
            xm = 0;
            ret |= PB_WITH_EXTREMUM;
        }
    }
    else if ((b*b - 3*a*c) < 0)
    {
        ret |= PB_WITH_EXTREMUM;
        xm = 0;
    }
    else
        xm = (-b - sqrt(b*b - 3*a*c))/(3*a);
    /*
       for (p = -2; p < 1; p++)
       {
       if (6*a*p+b > 0) ret |= TOO_MUCH_NOISE;
       }	
       */
    *Max_pos = (float)(xm + nmax + delta); 
    *Max_val = (float)((a * xm * xm * xm) + (b * xm * xm) + (c * xm) + d);
    if (Max_deriv)    *Max_deriv = (float)((6 * a * xm) + (2 * b));
    return ret;
}




int do_FitInterference_compute_autoconv(void)
{
  //register int i, j;
  register int i;
  O_p  *op = NULL;
  static int bp = 0, filf = 8, filw = 4, win = 0, rdc = 0;
  d_s *dsi, *dsd = NULL;
  pltreg *pr = NULL;
  float noise, Max_pos, Max_val, Max_deriv;
  static fft_plan *fp = NULL;
  static filter *fil = NULL;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
			 "of a data set by a number and place it in a new plot");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");

  i = win_scanf("Compute auto convolution\n"
		"%R->low-pass %r->band-pass\n"
		"Filter freq %6d width %6d (for BP)\n"
		"Windowing %b Remove-dc %b"
		,&bp,&filf,&filw,&win,&rdc);
  if (i == CANCEL) return 0;

  fp = fftbt_init(fp, dsi->nx);
  fil = filter_init_bt(fil, dsi->nx);


  dsd = create_and_attach_one_ds(op, dsi->nx, dsi->nx, 0);

  correlate_1d_sig_and_invert(dsi->yd, dsi->nx, win, dsd->yd, filf, filw, bp, rdc, fp, fil, &noise);
  for (i = 0; i < dsi->nx; i++) dsd->xd[i] = dsi->xd[i];

  find_max1(dsd->yd, dsd->nx, &Max_pos, &Max_val, &Max_deriv, dsd->nx);
  i = (int)Max_pos;
  i = (i < 0) ? 0 : i;
  i = (i < dsd->nx) ? i : dsd->nx - 1;
  set_ds_plot_label(dsd, dsd->xd[i],Max_val, USR_COORD, "\\fbox{\\pt8\\stack{{Max at %g}"
	  "{val =  %g}}}",Max_pos,Max_val);


  /* now we must do some house keeping */
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;


}


//-------------end of the section_3 writen by Samar---------------------------- 
MENU *FitInterference_plot_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"Hello example", do_FitInterference_hello,NULL,0,NULL);
  add_item_to_menu(mn,"Display interference", do_FitInterference_generate_plot,NULL,0,NULL);
  add_item_to_menu(mn,"Display photons distrubution", do_FitInterference_Photon_Distribution,NULL,0,NULL);
  add_item_to_menu(mn,"Compute auro-convolution",do_FitInterference_compute_autoconv,NULL,0,NULL);



 return mn;
}

int	FitInterference_main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  add_plot_treat_menu_item ( "FitInterference", NULL, FitInterference_plot_menu(), 0, NULL);
  return D_O_K;
}

int	FitInterference_unload(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  remove_item_to_menu(plot_treat_menu, "FitInterference", NULL, NULL);
  return D_O_K;
}

# ifdef ENCOURS


int do_generate_image(void)
{
  O_i *oid, *ois;
  imreg *imr;
  union pix *pd;
  static int i, num_spots = 10, image_dimension_x =128,
    image_dimension_y =128;
  static float Amplitude = 2000., Mean_background = 8000.,
    Noise_amplitude= 200., Fringe_Length;
  static float sigma = 1.4, noise, signal; Fringe_Length = sigma/3;

  const int PI=3.1416;
  double *x_c, *y_c, *x0;
  gsl_rng_type * T_1;
  gsl_rng_type * T_2;
  gsl_rng_type * T_3;
  gsl_rng * r_1;
  gsl_rng * r_2;
  gsl_rng * r_3;

  int j, k;

  if(updating_menu_state != 0) return D_O_K;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2) return OFF;

  i = win_scanf("Generate an image with noise\n"
		"Number of bright spots %d Amplitude %5f Mean background %5f \n"
		"Image noise amplitude %5f\n"
		"Image dimension in x %3d \n Image dimension in y %3d \n"
		"Sigma of the gaussian %5f\n"
		"spacing of the fringes"
		,&num_spots,&Amplitude,&Mean_background,
		&Noise_amplitude,&image_dimension_x,&image_dimension_y,&sigma,&Fringe_Length);
  if (i == CANCEL) return D_O_K;

  x_c = (double *)calloc(num_spots, sizeof(double));
  y_c = (double *)calloc(num_spots, sizeof(double));



  oid = create_and_attach_oi_to_imr(imr, image_dimension_x,
				    image_dimension_y, IS_UINT_IMAGE);
  if (oid == NULL)
    return win_printf_OK("Can't create dest movie");

  /* create a generator chosen by the
     environment variable GSL_RNG_TYPE */



  T_1 = gsl_rng_mt19937;//gsl_rng_default;
  r_1 = gsl_rng_alloc (T_1);
  T_2 = gsl_rng_mt19937;//gsl_rng_default;
  r_2 = gsl_rng_alloc (T_2);
  T_3 = gsl_rng_mt19937;//gsl_rng_default;
  r_3 = gsl_rng_alloc (T_3);

  gsl_rng_set (r_1, (unsigned long int)clock());
  gsl_rng_set (r_2, (unsigned long int)clock()+2);
  gsl_rng_set (r_3, (unsigned long int)clock()+4);
  x0= gsl_ran_flat (r_3, (double) 0, (double) oid->im.nx);
  // gsl_rng_env_setup ();
  // if (!getenv("GSL_RNG_SEED")) gsl_rng_default_seed = clock();

  for (k= 0 ; k<num_spots ; k++)
    {
      x_c[k]= gsl_ran_flat (r_2, (double) 0, (double) oid->im.nx);
      y_c[k]= gsl_ran_flat (r_2, (double) 0, (double) oid->im.ny);

    }

  for (i = 0, pd = oid->im.pixel; i < oid->im.ny ; i++)
    {
      for (j=0; j< oid->im.nx; j++)
	{
	  signal = 0;
	  noise = gsl_ran_gaussian (r_1, (double)
				    Noise_amplitude)+ (double)Mean_background;

	  for (k= 0 ; k<num_spots ; k++)
	    {
	      //signal += Amplitude * exp (-(j-x_c[k])*(j-x_c[k])/2/sigma)* exp 
	      //(-(i-y_c[k])*(i-y_c[k])/2/sigma); 
	      signal += Amplitude * (1 + 
	      cos(2*PI*(x-x0)/Fringe_Length))*exp(-(j-x_c[k])*(j-x_c[k])/2/sigma)* exp 
					       (-(i-y_c[k])*(i-y_c[k])/2/sigma);

	    }
	  pd[i].ui[j] = ((noise+signal > 65535) ? 65535 :
			 (unsigned short int)(noise+signal));
	}
    }
  gsl_rng_free (r_1);
  gsl_rng_free (r_2);
  
  gsl_rng_free (r_3);
  
  
  // oid->oi_mouse_action = oi_mouse_RGB_GifAvg_action;
  find_zmin_zmax(oid);
  oid->need_to_refresh |= ALL_NEED_REFRESH;
  return (refresh_image(imr, imr->n_oi - 1));
}





# endif

#endif

