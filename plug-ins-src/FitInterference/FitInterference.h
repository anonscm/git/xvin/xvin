#ifndef _FITINTERFERENCE_H_
#define _FITINTERFERENCE_H_

PXV_FUNC(int, do_FitInterference_rescale_plot, (void));
PXV_FUNC(MENU*, FitInterference_plot_menu, (void));
PXV_FUNC(int, do_FitInterference_rescale_data_set, (void));
PXV_FUNC(int, FitInterference_main, (int argc, char **argv));
#endif

