/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _FITINTERFERENCE_C_
#define _FITINTERFERENCE_C_

# include "allegro.h"
# include "xvin.h"

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled
# include "../nrutil/nrutil.h"

#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
//place here other headers of this plugin 

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "FitInterference.h"




//float a = ran1(&idum); a random ds [0, 1]


int do_FitInterference_hello(void)
{
  register int i;

  if(updating_menu_state != 0)	return D_O_K;

  i = win_printf("Hello from FitInterference");
  if (i == CANCEL) win_printf_OK("you have press CANCEL");
  else win_printf_OK("you have press OK");
  return 0;
}

int do_FitInterference_generate_plot(void)
{
  register int i, j;
  O_p  *opn = NULL;
  static int nf = 10000;
  static float x0 = 0, lamb = 1, sig = 2;
  d_s *ds;
  float tmp;
  pltreg *pr = NULL;
  int idum = 56;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number and place it in a new plot");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return win_printf_OK("cannot find data");

  i = win_scanf("This function create a random photon distribution\n"
  "corresponding to an interference patern with the equation:\n\n"
  "[1 + cos ({2\\pi (x - x_0)}\{\\lambda})]/2*exp-(x^2/2*\\sigma^2)\n\n"
  "Specify x_0 %8f, \\lambda  %8f, \\sigma %8f\n"
  "With howmany photons %8d\n"
  "Random seed %8d",&x0,&lamb,&sig,&nf,&idum);
  if (i == CANCEL)	return OFF;

  if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  ds = opn->dat[0];



  for (j = 0; j < nf; )
  {
    tmp = 5 * sig * (2*ran1(&idum) - 1);
    if (ran1(&idum) < exp(-(tmp*tmp)/(sig*sig))) 
      {
	ds->yd[j] = 1;
	ds->xd[j] = tmp;
	j++;
      }
  }

  /* now we must do some house keeping */
    set_ds_source(ds,"interence pattern x0 %g lambda %g sigma %g",x0,lamb,sig);
  set_plot_title(opn,"interence pattern x0 %g lambda %g sigma %g",x0,lamb,sig);
  set_plot_x_title(opn, "Position");
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}



MENU *FitInterference_plot_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"Hello example", do_FitInterference_hello,NULL,0,NULL);
  add_item_to_menu(mn,"plot rescale in Y", do_FitInterference_generate_plot,NULL,0,NULL);
  return mn;
}

int	FitInterference_main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  add_plot_treat_menu_item ( "FitInterference", NULL, FitInterference_plot_menu(), 0, NULL);
  return D_O_K;
}

int	FitInterference_unload(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  remove_item_to_menu(plot_treat_menu, "FitInterference", NULL, NULL);
  return D_O_K;
}

# ifdef ENCOURS


int do_generate_image(void)
{
  O_i *oid, *ois;
  imreg *imr;
  union pix *pd;
  static int i, num_spots = 10, image_dimension_x =128,
    image_dimension_y =128;
  static float Amplitude = 2000., Mean_background = 8000.,
    Noise_amplitude= 200., Fringe_Length;
  static float sigma = 1.4, noise, signal; Fringe_Length = sigma/3;

  const int PI=3.1416;
  double *x_c, *y_c, *x0;
  gsl_rng_type * T_1;
  gsl_rng_type * T_2;
  gsl_rng_type * T_3;
  gsl_rng * r_1;
  gsl_rng * r_2;
  gsl_rng * r_3;

  int j, k;

  if(updating_menu_state != 0) return D_O_K;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2) return OFF;

  i = win_scanf("Generate an image with noise\n"
		"Number of bright spots %d Amplitude %5f Mean background %5f \n"
		"Image noise amplitude %5f\n"
		"Image dimension in x %3d \n Image dimension in y %3d \n"
		"Sigma of the gaussian %5f\n"
		"spacing of the fringes"
		,&num_spots,&Amplitude,&Mean_background,
		&Noise_amplitude,&image_dimension_x,&image_dimension_y,&sigma,&Fringe_Length);
  if (i == CANCEL) return D_O_K;

  x_c = (double *)calloc(num_spots, sizeof(double));
  y_c = (double *)calloc(num_spots, sizeof(double));



  oid = create_and_attach_oi_to_imr(imr, image_dimension_x,
				    image_dimension_y, IS_UINT_IMAGE);
  if (oid == NULL)
    return win_printf_OK("Can't create dest movie");

  /* create a generator chosen by the
     environment variable GSL_RNG_TYPE */



  T_1 = gsl_rng_mt19937;//gsl_rng_default;
  r_1 = gsl_rng_alloc (T_1);
  T_2 = gsl_rng_mt19937;//gsl_rng_default;
  r_2 = gsl_rng_alloc (T_2);
  T_3 = gsl_rng_mt19937;//gsl_rng_default;
  r_3 = gsl_rng_alloc (T_3);

  gsl_rng_set (r_1, (unsigned long int)clock());
  gsl_rng_set (r_2, (unsigned long int)clock()+2);
  gsl_rng_set (r_3, (unsigned long int)clock()+4);
  x0= gsl_ran_flat (r_3, (double) 0, (double) oid->im.nx);
  // gsl_rng_env_setup ();
  // if (!getenv("GSL_RNG_SEED")) gsl_rng_default_seed = clock();

  for (k= 0 ; k<num_spots ; k++)
    {
      x_c[k]= gsl_ran_flat (r_2, (double) 0, (double) oid->im.nx);
      y_c[k]= gsl_ran_flat (r_2, (double) 0, (double) oid->im.ny);

    }

  for (i = 0, pd = oid->im.pixel; i < oid->im.ny ; i++)
    {
      for (j=0; j< oid->im.nx; j++)
	{
	  signal = 0;
	  noise = gsl_ran_gaussian (r_1, (double)
				    Noise_amplitude)+ (double)Mean_background;

	  for (k= 0 ; k<num_spots ; k++)
	    {
	      //signal += Amplitude * exp (-(j-x_c[k])*(j-x_c[k])/2/sigma)* exp 
	      //(-(i-y_c[k])*(i-y_c[k])/2/sigma); 
	      signal += Amplitude * (1 + 
	      cos(2*PI*(x-x0)/Fringe_Length))*exp(-(j-x_c[k])*(j-x_c[k])/2/sigma)* exp 
					       (-(i-y_c[k])*(i-y_c[k])/2/sigma);

	    }
	  pd[i].ui[j] = ((noise+signal > 65535) ? 65535 :
			 (unsigned short int)(noise+signal));
	}
    }
  gsl_rng_free (r_1);
  gsl_rng_free (r_2);
  
  gsl_rng_free (r_3);
  
  
  // oid->oi_mouse_action = oi_mouse_RGB_GifAvg_action;
  find_zmin_zmax(oid);
  oid->need_to_refresh |= ALL_NEED_REFRESH;
  return (refresh_image(imr, imr->n_oi - 1));
}





# endif

#endif

