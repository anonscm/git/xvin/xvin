
/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _FITINTERFERENCE_C_
#define _FITINTERFERENCE_C_
//#include <sys/types>
 #include "allegro.h"
# include "xvin.h"
# include "math.h"
# include "../fft2d/fftbtl32n.h"
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
//# include "fillibbt.h"

/*
XV_VAR(un_s*, to_modify_un);
XV_FUNC(int, pop_unit_decade_mn, (void));
XV_FUNC(int, pop_unit_type_mn, (void));

 */

/* If you include other regular header do it here*/

#define _TRKMAX_C_
# include "float.h"
# include "unitset.h"


/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled
# include "../nrutil/nrutil.h"

#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
//place here other headers of this plugin

/* If you include other regular header do it here*/
# include "brown_util.h"
# include "fillibbt.h"

/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "FitInterference.h"

# define 	WRONG_ARGUMENT		-1
# define 	OUT_MEMORY			-2
# define 	FFT_NOT_SUPPORTED	-15
# define	PB_WITH_EXTREMUM	2
# define	TOO_MUCH_NOISE		4
# define	GETTING_REF_PROFILE	1


int find_phase_zero_with_poly(d_s *dsphi, float *zero);

//float a = ran1(&idum); a random ds [0, 1]


int do_FitInterference_hello(void)
{
  register int i;

  if(updating_menu_state != 0)	return D_O_K;

  i = win_printf("Hello from FitInterference");
  if (i == WIN_CANCEL) win_printf_OK("you have press WIN_CANCEL");
  else win_printf_OK("you have press OK");
  return 0;
}


int do_fill_gaussian_photons(d_s *ds, float sig, int *idum)
{
  register int j;
  float tmp, val;


  if (ds == NULL) return -1;

  for (j = 0; j < ds->nx; )
    {
      tmp = 5 * sig * (2*ran1(idum) - 1);
      val = exp(-(tmp*tmp)/(2*sig*sig));
      if (ran1(idum) < val)
	{
	  ds->yd[j] = 1;
	  ds->xd[j] = tmp;
	  j++;

	}
    }
  return 0;
}

int ds_barycentre(d_s *ds, float *barycentre)
{
  register int i;
  double tmp;

  if (ds == NULL || ds->nx < 1) return 1;
  for (i = 0, tmp = 0; i < ds->nx; i++) tmp += ds->xd[i];
  if (barycentre != NULL) *barycentre = tmp/ds->nx;
  return 0;
}

//----------Section 1 By Samar ------------------

int do_fill_gaussian_Photons_Distribution(d_s *ds, float sig, int *idum, int nf,float X_Shift)
{
  register int j;
  float tmp, val;
  int X_Lower=-((int) round(ds->nx/2)),X_Upper=((int) round(ds->nx/2));
  //X_Shift=100;
  if (ds == NULL) return -1;

 for (j = 0; j <ds->nx;j++ )
   {
     ds->yd[j]= 0;//initiate the coordenates of the Data-Set
 ds->xd[j] =j+ X_Lower;

   }

  for (j = 0; j <nf; )
    {
      tmp = X_Lower + ran1(idum)*(X_Upper - X_Lower);// a+Ra(b-a) where Ra is the random variable
 if ((round(tmp-X_Lower+X_Shift)>=0)&&(round(tmp-X_Lower+X_Shift)<ds->nx))
   {
  val = exp(-(tmp*tmp)/(2*sig*sig));
      if (ran1(idum) < val)
	{

	  ds->yd[(int)round(tmp-X_Lower+X_Shift)] += 1;
	  ds->xd[(int)round(tmp-X_Lower+X_Shift)] = round(tmp+X_Shift);
	  j++;
	}
   }
}
  return 0;
}

//---------------------------------------------------------------


int do_gaussian_Function(d_s *ds, float sig, int nf,float X_Shift)
{
  register int j;
  //float tmp, val;
  int X_Lower=-((int) round((ds->nx)/2));//,X_Upper=((int) round((ds->nx)/2));

  if (ds == NULL) return -1;
  (void)nf;
  for (j = 0; j <ds->nx; j++)
    {
      //  ds->yd[j]= 0;//initiate the coordenates of the Data-Set
      ds->xd[j] =j+ X_Lower;


      ds->yd[j]= exp(-pow(( ds->xd[j]-X_Shift),2)/(2*sig*sig));
      //exp(-(tmp*tmp)/(2*sig*sig));
    }
  return 0;
}


//------------------------------------------------------------------




//-----------End Section 1 By Samar-----------------
int do_fill_gaussian_interference_photons(d_s *ds, float sig, float lamb, int *idum)
{
  register int j;
  float tmp, val;

  if (ds == NULL) return -1;

  for (j = 0; j < ds->nx; )
    {
      tmp = 5 * sig * (2*ran1(idum) - 1);
      val = (1+cos (2*M_PI*tmp/lamb)) * 0.5 *exp(-tmp*tmp/(2*sig*sig));
      if (ran1(idum) < val)
	{
	  ds->yd[j] = 1;
	  ds->xd[j] = tmp;
	  j++;
	}
    }
  return 0;
}


//----------Section 2 By Samar------------------

int do_fill_gaussian_Interference_Photons_Distribution(d_s *ds, float sig, float lamb, int *idum, int nf,float X_Shift)
{
  register int j;
  float tmp, val;
  int X_Lower=-((int) round((ds->nx)/2)),X_Upper=((int) round((ds->nx)/2));

  if (ds == NULL) return -1;

 for (j = 0; j <ds->nx; j++)
   {
     ds->yd[j]= 0;//initiate the coordenates of the Data-Set
 ds->xd[j] =j+ X_Lower;

   }

  for (j = 0; j <nf; )
    {
      tmp = X_Lower + ran1(idum)*(X_Upper - X_Lower);// a+Ra(b-a) where Ra is the random variable
 if ((round(tmp-X_Lower+X_Shift)>=0)&&(round(tmp-X_Lower+X_Shift)<ds->nx))
   {
val = (1+cos (2*M_PI*tmp/lamb)) * 0.5 *exp(-tmp*tmp/(2*sig*sig));
      if (ran1(idum) < val)
	{

	  ds->yd[(int)round(tmp-X_Lower+X_Shift)] += 1;
	  ds->xd[(int)round(tmp-X_Lower+X_Shift)] = round(tmp+X_Shift);
	  j++;
	}
   }

}

   return 0;
}
//---------------------------------------------------------------------------------


int do_gaussian_Interference_Function(d_s *ds, float sig, float lamb, int nf,float X_Shift, float X1_Shift)
{
  register int j;
  //float tmp, val;
  int X_Lower=-((int) round((ds->nx)/2));//,X_Upper=((int) round((ds->nx)/2));

  if (ds == NULL) return -1;
  (void)nf;
  for (j = 0; j <ds->nx; j++)
   {
     //  ds->yd[j]= 0;//initiate the coordenates of the Data-Set
     ds->xd[j] =j+ X_Lower;


     ds->yd[j]= (1+cos (2*M_PI*( ds->xd[j]-X1_Shift)/lamb)) *exp(-pow(( ds->xd[j]-X_Shift),2)/(2*sig*sig));
   }


  return 0;
}




int vincent_2raw_phase_function( int bp_filter,  int bp_width, d_s *dst,double *phif,double *k, float *Max_pos)
{

register int i, j;
int r_s, r_e, size, comp;
static int  nx = 128;

static float mr = 2;
d_s *ds = NULL,*ds2=NULL,*ds3=NULL;
O_p *op= NULL;
d_s *dsr = NULL, *dsi = NULL, *dsfr = NULL, *dsfi = NULL, *dsp = NULL, *dsfit = NULL;
imreg *imr;
int imax;
float tmp, f0, amp, Max_val;
double re, im, phi;
static float phi0 = 0;
fft_plan *fp = NULL;
O_i *ois=NULL;


//copy dst to dsr
nx=dst->nx;
fp = fftbt_init(fp, nx);
// if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)    return D_O_K;
// comp = (ois->im.data_type == IS_COMPLEX_IMAGE
//   || ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE) ? 1 : 0;
// if(updating_menu_state != 0)
//   {
//     if (comp) 	 active_menu->flags |=  D_DISABLED;
//     else               active_menu->flags &= ~D_DISABLED;
//     return D_O_K;
//   }
//
//
//
// op = create_and_attach_op_to_oi(ois, nx, nx, 0, IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
//
// ds2 = create_and_attach_one_ds(op, nx, nx, 0);
// ds3 = create_and_attach_one_ds(op, nx, nx, 0);


//
//
//
// duplicate_data_set(dst,ds2);
// set_ds_source(ds2,"x profiles");


dsr=build_adjust_data_set(NULL,nx,nx);
dsi=build_adjust_data_set(NULL,nx,nx);
dsfr=build_adjust_data_set(NULL,nx,nx);
dsfi=build_adjust_data_set(NULL,nx,nx);
dsfit=build_adjust_data_set(NULL,nx,nx);
dsp=build_adjust_data_set(NULL,nx,nx);
alloc_data_set_y_error(dsfit);



for (j = 0;j < nx; j++)
{
  dsr->yd[j] = dst->yd[j];
  dsr->xd[j] = dsi->xd[j] = j;
}


realtr1bt(fp, dsr->yd);  fftbt(fp, dsr->yd, 1);  realtr2bt(fp, dsr->yd, 1);

for (j = 0;j < nx/2; j++)
{
  if ((j > bp_filter - bp_width) && (j <= bp_filter + bp_width)) continue;
  dsr->yd[2*j] = 0;
  dsr->yd[2*j+1] = 0;

}
//
// duplicate_data_set(dsr,ds3);
// set_ds_source(ds3,"fourier transform");


for (j = bp_filter-2 * bp_width, f0 = amp = 0;j < bp_filter+2*bp_width; j++)
{  // we compute the frequency barycenter
  if (j < 0 || j >= nx) continue;
  tmp = dsr->yd[2*j] * dsr->yd[2*j] + dsr->yd[2*j+1] * dsr->yd[2*j+1];
  f0 += tmp * j;
  amp += tmp;
}
if (amp > 0) f0 /= amp;
for (j = 2;j < nx; j+=2)
{  // we prepare imaginary part
  dsi->yd[j+1] = dsr->yd[j];
  dsi->yd[j] = -dsr->yd[j+1];
}

dsi->yd[1] = dsi->yd[0] = 0;
/*	get back to complex world */
realtr2bt(fp, dsr->yd, -1);  fftbt(fp, dsr->yd, -1); realtr1bt(fp, dsr->yd);
realtr2bt(fp, dsi->yd, -1);  fftbt(fp, dsi->yd, -1); realtr1bt(fp, dsi->yd);

for (j = 0;j < nx; j++)
{  // we find the amplitude maximum and place the amplitude in dsfr
  tmp = sqrt(dsr->yd[j] * dsr->yd[j] + dsi->yd[j] * dsi->yd[j]);
  dsfr->yd[j] = tmp;
} // the amplitude is stored in dsfr->yd

for (j = 4, imax = 4;j < nx-4; j++)
{  // we skip the border that are poluted by dewindowing
  if (dsfr->yd[j] > dsfr->yd[imax]) imax = j;
} // the amplitude is stored in dsfr->yd
j = find_max_around(dsfr->yd, dsfr->nx, imax, Max_pos, &Max_val, NULL);


j = imax;
re = dsr->yd[j];
im = dsi->yd[j];
phi = (im == 0.0 && re == 0.0) ? 0.0 : atan2(im, re); // phase in j
dsfi->yd[j] = dsfi->xd[j] = phi;
//win_printf("1");
for (j = imax+1;j < nx; j++)
{
  re = dsr->yd[j] * dsr->yd[j-1] + dsi->yd[j] * dsi->yd[j-1];
  im = dsi->yd[j] * dsr->yd[j-1] - dsr->yd[j] * dsi->yd[j-1];
  phi += (im == 0.0 && re == 0.0) ? 0.0 : atan2(im, re);
  dsfi->yd[j] = dsfi->xd[j] = phi;
}
//win_printf("2");
phi = dsfi->yd[imax];
for (j = imax-1;j >= 0; j--)
{
  re = dsr->yd[j+1] * dsr->yd[j] + dsi->yd[j+1] * dsi->yd[j];
  im = dsi->yd[j+1] * dsr->yd[j] - dsr->yd[j+1] * dsi->yd[j];
  phi -= (im == 0.0 && re == 0.0) ? 0.0 : atan2(im, re);
  dsfi->yd[j] = dsfi->xd[j] = phi;
}
//win_printf("3");
for (j = 0, dsfit->nx = 0;j < nx; j++)
{
  dsp->yd[j] = dsfi->yd[j];
  if (mr*dsfr->yd[j] > Max_val) // we keep phase if amp^2 > max/2
  {
    dsfit->yd[dsfit->nx] = dsfi->yd[j];
    dsfit->ye[dsfit->nx] = (tmp > 0) ? ((float)1)/dsfr->yd[j] : 10;
    dsfit->xd[dsfit->nx++] = j;
  }
}

double x = 0;
if (dsfit->nx < 3)       display_title_message("Fitting over %d max val %g at %g",dsfit->nx,Max_val,*Max_pos);
else
{
  //win_printf("fitting over %d pts",dsfit->nx);
  if (compute_least_square_fit_on_ds(dsfit, 1, k, phif))
  {
    display_title_message("fitting problem");
  }
}
free_data_set(dsr);
free_data_set(dsi);
free_data_set(dsfr);
free_data_set(dsfit);
free_data_set(dsfi);
free_data_set(dsp);


return 0;


}


int profiles_to_displacement( int bp_filter,  int bp_width,d_s *ds1, d_s *ds2, float *displacement)

{
  double phi1raw=0,phi2raw=0;
  float Maxpos1=0,Maxpos2=0;
  double k1=0,k2=0,km=0;
  double d_displacement=0;
  double low,high;


  vincent_2raw_phase_function(bp_filter, bp_width, ds1,&phi1raw,&k1, &Maxpos1);
  vincent_2raw_phase_function(bp_filter, bp_width, ds2,&phi2raw,&k2, &Maxpos2);


  km=(fabs(k1)+fabs(k2))/2;
  d_displacement=(double)(Maxpos2-Maxpos1);


  *displacement=(float) ((phi2raw-phi1raw)/km);// si k1 et k2 sont très différents, problème (pas de warning jusque là, à penser)!

  low=d_displacement-M_PI/km;
  high=d_displacement+M_PI/km;

  while (*displacement>high)
    *displacement-=2*M_PI/km;

  while (*displacement<low)
    *displacement+=2*M_PI/km;



  return 0;

}


//------------------------------------------------------------------------------------
int eliminate_strange_values_and_return_mean(float *arrayin, float *meanvalue, int n_array)
{
  float mean=0;
  int i;


  for (i=0;i<n_array;i++)
  {
    mean+=arrayin[i]/n_array;
  }




  *meanvalue=mean;
  printf("mean value=%f\n",meanvalue);

  return 0;


}


int mean_displacement_between_2_images(O_i *ois, int frame1, int frame2, int min_yl, int max_yl,  int bp_filter,  int bp_width, float *displacement)
{
  int nx =  ois->im.nx;
  int i;
  int n_y=max_yl-min_yl+1;
  float displacements[n_y+1];
  O_p *op=NULL;
  d_s *dst1=NULL,*dst2=NULL,*ds=NULL;


  op = create_and_attach_op_to_oi(ois, n_y, n_y, 0, IM_SAME_X_AXIS|IM_SAME_Y_AXIS);

  ds = op->dat[0];



  dst1=build_adjust_data_set(NULL,nx,nx);
  dst2=build_adjust_data_set(NULL,nx,nx);

  for (i=0;i<nx;i++) dst1->xd[i]=dst2->xd[i]=i;
  for (i=0;i<n_y;i++)
  {
  switch_frame(ois,frame1);
  extract_raw_line(ois, i+min_yl, dst1->yd);
  switch_frame(ois,frame2);
  extract_raw_line(ois, i+min_yl, dst2->yd);
  profiles_to_displacement(bp_filter, bp_width,dst1, dst2, displacements+i);
  ds->yd[i]=displacements[i];
  ds->xd[i]=min_yl+i;
  printf("displacements[%d]=%f\n",i,displacements[i]);

}

  eliminate_strange_values_and_return_mean(displacements,displacement,n_y);

  free_data_set(dst1);
  free_data_set(dst2);
  return 0;

}






  int extract_x_averaged_on_y(O_i *ois,int pxg,int pxd,char *yd)
  {
    int i,j;
    int lx=ois->im.nx;
    d_s *dstmp=build_data_set(lx,lx);

    for (i=0;i<lx;i++)
    {
      yd[i]=0;
    }

    for (i=pxg;i<=pxd;i++)
    {
        extract_raw_partial_line(ois,i,dstmp->yd,0,lx);
        for (j=0;j<lx;j++)
          {
              yd[j]+=(float)(dstmp->yd[j])/(pxd-pxg+1);
          }
    }

    return 0;

  }

  int unwrap_simple(float *angles,int n)
  {
    int i;
    for (i=1;i<n;i++)
    {
      while (angles[i]>angles[i-1]+2*M_PI)
              angles[i]-=2*M_PI;
      while (angles[i]<angles[i-1])
            angles[i]+=2*M_PI;
    }
    return 0;
  }


  int spectral_phase( float *tf_complex, d_s *phase, int n)
  {
    int i;
    float re;
    float im;

    if (tf_complex == NULL || phase == NULL) return 1;

    for (i=1; i<n ;  i++)
    {
      re = tf_complex[2 * i];
      im = tf_complex[2 * i + 1];
      phase->yd[i] = (float) atan2(im , re);
      phase->ye[i]= (float) (1/(pow(im,2) + pow(re,2)));
    }
    phase->yd[0]=0;
    phase->ye[0]=1/pow(tf_complex[0],3);
    return 0;
  }







// ----End Section 2By Samar-----------------------
int do_FitInterference_generate_plot(void)
{
  register int i, j;
  O_p  *opn = NULL, *opb = NULL;
  //float *BaryCentre = ((float*)calloc(1, sizeof(float));
  float BaryCentre = 0;
  static int nf = 10000, eqn = 0, nba = 1;
  static float x0 = 0, lamb = 1, sig = 2;
  d_s *ds, *dsb = NULL;
  //float tmp, val;
  pltreg *pr = NULL;
  static int idum = 56;
  //float l = sig/3;
  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number and place it in a new plot");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return win_printf_OK("cannot find data");

  i = win_scanf("This function create a random photon distribution\n"
		"corresponding to an interference patern with one of these equations:\n\n"
		"%R exp-(x^2/2*\\sigma^2)\n\n"
		"%r ([1 + cos ({2\\pi (x - x_0)}\{\\lambda})]/2)*exp-(x^2/2*\\sigma^2)\n\n"
		"Specify x_0 %8f, \\lambda  %8f, \\sigma %8f\n"
		"With howmany photons %8d\n"
		"Random seed %8d\n"
		"Repeat this operation %8d times and compute barycenter\n"
		,&eqn,&x0,&lamb,&sig,&nf,&idum,&nba);
  if (i == WIN_CANCEL)	return OFF;

  if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  ds = opn->dat[0];

  if (nba > 1)
    {
      if ((opb = create_and_attach_one_plot(pr, nba, nba, 0)) == NULL)
	return win_printf_OK("cannot create plot !");
      dsb = opb->dat[0];
      set_plot_y_title(opb, "Barycenter Position");
    }
  /*
  for (j = 0; j < nf; )
    {
      tmp = 5 * sig * (2*ran1(&idum) - 1);
      val = (eqn == 0) ? exp(-(tmp*tmp)/(2*sig*sig)) : (1+cos (2*M_PI*tmp/lamb)) * 0.5 *exp(-tmp*tmp/(2*sig*sig));
      if (ran1(&idum) < val)
	{
	  ds->yd[j] = 1;
	  ds->xd[j] = tmp;
	  j++;
	}
    }
  */


  for (j = 0;j < nba; j++)
    {
      if (eqn == 0)   do_fill_gaussian_photons(ds, sig, &idum);
      else            do_fill_gaussian_interference_photons(ds, sig, lamb, &idum);
      ds_barycentre(ds, &BaryCentre);
      if (dsb != NULL)
	 {
	   dsb->xd[j] = j;
	   dsb->yd[j] = BaryCentre;
	 }
      else win_printf(" The value of the barycenter is %g",BaryCentre);
    }


  /* now we must do some house keeping */
    set_ds_source(ds,"interence pattern x0 %g lambda %g sigma %g",x0,lamb,sig);
  set_plot_title(opn,"interence pattern x0 %g lambda %g sigma %g",x0,lamb,sig);
  set_plot_x_title(opn, "Position");
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}

//-----Section_ 3  Writen By Samar to be validated---------------

int do_FitInterference_Photon_Distribution(void)
{
  //register int i, j;
  register int i;
  O_p  *opd = NULL, *opb = NULL;
  //float *BaryCentre = ((float*)calloc(1, sizeof(float));
  static int nf = 10000, eqn = 0, nba = 1,Nb_Pixel=256,Shift=0;
  static float x0 = 0, lamb = 1, sig = 2;
  d_s *dsd;
  //float tmp, val;
  pltreg *pr = NULL;
  static int idum = 56;
  //float l = sig/3;
  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number and place it in a new plot");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return win_printf_OK("cannot find data");

  i = win_scanf("This function simulate a random photon distribution\n"
		"corresponding to an interference patern with one of these equations:\n\n"
		"%R exp-(x^2/2*\\sigma^2)\n\n"
		"%r ([1 + cos ({2\\pi (x - x_0)}\{\\lambda})]/2)*exp-(x^2/2*\\sigma^2)\n\n"
		"Specify x_0 %8f, \\lambda  %8f, \\sigma %8f\n"
		"With how many photons %8d\n"
"How many pixel in the x axis %8d"
		"Random seed %8d\n"
"Shift the distribution by  %8d \n"
		,&eqn,&x0,&lamb,&sig,&nf,&Nb_Pixel,&idum,&Shift);
  if (i == WIN_CANCEL)	return OFF;

  if ((opd = create_and_attach_one_plot(pr, Nb_Pixel,Nb_Pixel, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  dsd = opd->dat[0];
  set_plot_y_title(opd, " Photon distribution");


  if (nba > 1)// it is not used here
    {
      if ((opb = create_and_attach_one_plot(pr, nba, nba, 0)) == NULL)
	return win_printf_OK("cannot create plot !");
      //dsb = opb->dat[0];
      set_plot_y_title(opb, "Barycenter Position");
    }
  /*
  for (j = 0; j < nf; )
    {
      tmp = 5 * sig * (2*ran1(&idum) - 1);
      val = (eqn == 0) ? exp(-(tmp*tmp)/(2*sig*sig)) : (1+cos (2*M_PI*tmp/lamb)) * 0.5 *exp(-tmp*tmp/(2*sig*sig));
      if (ran1(&idum) < val)
	{
	  ds->yd[j] = 1;
	  ds->xd[j] = tmp;
	  j++;
	}
    }
  */




  if (eqn == 0)   do_fill_gaussian_Photons_Distribution(dsd, sig, &idum,nf,Shift);
  else           do_fill_gaussian_Interference_Photons_Distribution(dsd, sig,lamb, &idum,nf,Shift);




  /* now we must do some house keeping */
    set_ds_source(dsd,"interence pattern x0 %g lambda %g sigma %g",x0,lamb,sig);
  set_plot_title(opd,"interence pattern x0 %g lambda %g sigma %g",x0,lamb,sig);
  set_plot_x_title(opd, "Position");
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);


  return D_O_K;

}
///////////////////////////////





int do_FitInterference_compute_autoconv(void)
{
  //register int i, j;
  register int i;
  O_p  *op = NULL;
  static int bp = 0, filf = 8, filw = 4, win = 0, rdc = 0;
  d_s *dsi, *dsd = NULL;
  pltreg *pr = NULL;
  float noise, Max_pos, Max_val, Max_deriv;
  fft_plan *fp = NULL;
  filter *fil = NULL;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
			 "of a data set by a number and place it in a new plot");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");

  i = win_scanf("Compute auto convolution\n"
		"%R->low-pass %r->band-pass\n"
		"Filter freq %6d width %6d (for BP)\n"
		"Windowing %b Remove-dc %b"
		,&bp,&filf,&filw,&win,&rdc);
  if (i == WIN_CANCEL) return 0;

  fp = fftbt_init(fp, dsi->nx);
  fil = filter_init_bt(fil, dsi->nx);


  dsd = create_and_attach_one_ds(op, dsi->nx, dsi->nx, 0);

  correlate_1d_sig_and_invert(dsi->yd, dsi->nx, win, dsd->yd, filf, filw, ((bp) ? BAND_PASS: LOW_PASS), rdc, fp, fil, &noise);
  for (i = 0; i < dsi->nx; i++) dsd->xd[i] = dsi->xd[i];

  find_max1(dsd->yd, dsd->nx, &Max_pos, &Max_val, &Max_deriv, dsd->nx);
  i = (int)Max_pos;
  i = (i < 0) ? 0 : i;
  i = (i < dsd->nx) ? i : dsd->nx - 1;
  set_ds_plot_label(dsd, dsd->xd[i],Max_val, USR_COORD, "\\fbox{\\pt8\\stack{{Max at %g}"
	  "{val =  %g}}}",Max_pos,Max_val);

  free_fft_plan_bt(fp);
  free_filter_bt(fil);
  /* now we must do some house keeping */
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;


}
//---------------------------------------- Start Do_AutoCorre_Max_Fluct (void)------------------------------------------------------------/////////////
int Do_AutoCorre_Max_Fluct (void)
{
  //register int i, j;
  register int i,j;
  O_p  *opd = NULL, *opm = NULL,*opc=NULL;
  static int nf = 10000, eqn = 1, Nb_Max = 10,Nb_Pixel=256,Shift=10;
  static float x0 = 0, lamb = 8, sig = 20;
  d_s *dsd=NULL, *dsc=NULL, *dsm = NULL; /*dsd is the data set of the Photon distribution, dsc is the data set of the auto Conconv and dsm is the data set of the autoconvolution maximum*/
  //float tmp, val;
  pltreg *pr = NULL;
  static int idum = 56;

  static int bp = 0, filf = 32, filw = 4, win = 0, rdc = 0, n_expan = 4;
  float noise, Max_pos, Max_val, Max_deriv;
  fft_plan *fp = NULL, *fpe = NULL;
  filter*fil = NULL;



  //float l = sig/3;
  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the y coordinate"
			   "of a data set by a number and place it in a new plot");
    }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return win_printf_OK("cannot find data");

  i = win_scanf("This function simulate a random photon distribution\n"
		"corresponding to an interference patern with one of these equations:\n\n"
		"%R exp-(x^2/2*\\sigma^2)\n\n"
		"%r ([1 + cos ({2\\pi (x - x_0)}\{\\lambda})]/2)*exp-(x^2/2*\\sigma^2)\n\n"
		"Specify x_0 %8f, \\lambda  %8f, \\sigma %8f\n"
		"With how many photons %8d\n"
		"How many pixel in the x axis %8d"
		"Random seed %8d\n"
		"Shift the distribution by  %8d \n"
		"---------------------------------------------------------\n"
		"Necessary to compute the autocorrelation and to calculate its maximum\n"
		"%R->low-pass %r->band-pass\n"
		"Filter freq %6d width %6d (for BP)\n"
		"Windowing %b Remove-dc %b expansion %6d\n"
		"How many times do you want to compute the AutoConv%8d \n"
		,&eqn,&x0,&lamb,&sig,&nf,&Nb_Pixel,&idum,&Shift,&bp,&filf,&filw,&win,&rdc,&n_expan,&Nb_Max);


  if (i == WIN_CANCEL)	return OFF;

  if ((opd = create_and_attach_one_plot(pr, Nb_Pixel,Nb_Pixel, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  dsd = opd->dat[0];
  set_plot_y_title(opd, " Photon distribution");
  set_plot_x_title(opd, " position");

  if ((opc = create_and_attach_one_plot(pr, Nb_Pixel*n_expan,Nb_Pixel*n_expan, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  dsc = opc->dat[0];
  set_plot_y_title(opc, " autocovonlution value");
  set_plot_x_title(opc, " position");


  if (Nb_Max > 1)
    {
      if ((opm = create_and_attach_one_plot(pr, Nb_Max, Nb_Max, 0)) == NULL)
	return win_printf_OK("cannot create plot !");
      dsm = opm->dat[0];
      set_plot_y_title(opm, "Autoconvolution maximum position fluctuation");
      set_plot_x_title(opm, "iteration number");
    }


  fp = fftbt_init(fp, dsd->nx);
  fpe = fftbt_init(fpe, dsd->nx*n_expan);
  fil = filter_init_bt(fil, dsd->nx);

  for (j = 0;j < Nb_Max; j++)
    {
      if (eqn == 0)    do_fill_gaussian_Photons_Distribution(dsd, sig, &idum,nf,Shift);
      else           do_fill_gaussian_Interference_Photons_Distribution(dsd, sig,lamb, &idum,nf,Shift);



      correlate_1d_sig_and_invert(dsd->yd, dsd->nx, win, dsc->yd, filf, filw, ((bp) ? BAND_PASS: LOW_PASS), rdc, fp, fil, &noise);

      for (i = dsd->nx; i < dsc->nx; i++) dsc->yd[i] = 0;
      // we fft on dsd->nx points
      realtr1bt(fp, dsc->yd);           fftbt(fp, dsc->yd, 1);      realtr2bt(fp, dsc->yd, 1);
      // we fft-1 on n_expan * dsd->nx points
      realtr2bt(fpe, dsc->yd, -1);      fftbt(fpe, dsc->yd, -1);      realtr1bt(fpe, dsc->yd);


      for (i = 0; i < dsc->nx; i++) dsc->xd[i] = dsd->xd[0] + ((dsd->xd[dsd->nx-1] - dsd->xd[0])*i)/dsc->nx;

      find_max1(dsc->yd, dsc->nx, &Max_pos, &Max_val, &Max_deriv, dsc->nx);
      Max_pos /= 2*n_expan;
      if (j==Nb_Max-1)
	{
	  i = (int)Max_pos;
	  i = (i < 0) ? 0 : i;
	  i = (i < dsc->nx) ? i : dsc->nx - 1;
	  set_ds_plot_label(dsc, dsc->xd[i],Max_val, USR_COORD, "\\fbox{\\pt8\\stack{{Max at %g}"
			    "{val =  %g}}}",Max_pos,Max_val);
	}
      if (dsm != NULL)
	{
	  dsm->xd[j] = j;
	  dsm->yd[j] = Max_pos;
	}
      else win_printf(" The value of the maximum of the  autoconvolution is %g and its position is %g",Max_val, Max_pos);
    }



  free_fft_plan_bt(fp);
  free_filter_bt(fil);
  free_fft_plan_bt(fpe);


  /* now we must do some house keeping */
  set_ds_source(dsm," x0 %g lambda %g sigma %g",x0,lamb,sig);
  set_plot_title(opm," x0 %g lambda %g sigma %g",x0,lamb,sig);
  //  set_plot_x_title(opd, "Position");
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);


  return D_O_K;

}
//---------------------------- end Do_AutoCorre_Max_Fluct (void)----------------------

//-------------------------------Start of Mean_ds-----------------
float Mean_ds(d_s * ds)
{
  float Mean=0;
  register int i;
  for (i=0; i<ds->nx;i++)
    {
      Mean +=ds->yd[i];
    }
  Mean/=ds->nx;
  return Mean;
}
//-------------------------------End of  Mean_dv-----------------
//-------------------------------Start of Stand_Dev-----------------
float Stand_Dev(d_s * ds)
{
  register int i;
  float StandDev=0,Variance=0;
float Mean = Mean_ds (ds);
 for (i=0; i<ds->nx;i++)
    {
     Variance +=pow((ds->yd[i]-Mean),2);
    }
Variance/=ds->nx-1;
 StandDev=sqrt(Variance);
  return StandDev;
}
//-------------------------------End of Stand_Dev-----------------

//-------------------------------start of ErrDevia_Max_Shift-----------------

int ErrDevia_Max_Shiftt (void)
{
  //register int i, j;
  register int i,k;
  O_p  *opd = NULL, *opm = NULL,*opc=NULL, *ops=NULL;
  static int nf = 10000, eqn = 1, Nb_Max = 10,Nb_Pixel=256;
  static float Min_Shift=0,Step_Shift=0.1,Max_Shift=10,Shift;
  static float x0 = 0, lamb = 8, sig = 20;
  d_s *dsd=NULL, *dsc=NULL, *dsm = NULL, *dss=NULL; /*dsd is the data set of the Photon distribution, dsc is the data set of the auto Conconv and dsm is the data set of the autoconvolution maximum*/
  //float tmp, val;
  pltreg *pr = NULL;
  static int idum = 56;

  static int bp = 0, filf = 32, filw = 4, win = 0, rdc = 0, n_expan = 4;
  int  Nb_Step=0;
  float noise, Max_pos, Max_val, Max_deriv,StandDev = 0;
  fft_plan *fp = NULL, *fpe = NULL;
  filter *fil = NULL;



  //float l = sig/3;
  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the y coordinate"
			   "of a data set by a number and place it in a new plot");
    }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return win_printf_OK("cannot find data");

  i = win_scanf("This function simulate a random photon distribution\n"
		"corresponding to an interference patern with one of these equations:\n\n"
		"%R exp-(x^2/2*\\sigma^2)\n\n"
		"%r ([1 + cos ({2\\pi (x - x_0)}\{\\lambda})]/2)*exp-((x-x_0)^2/2*\\sigma^2)\n\n"
		"Specify x_0 %8f, \\lambda  %8f, \\sigma %8f\n"
		"With how many photons %8d\n"
		"How many pixel in the x axis %8d"
		"Random seed %8d\n"
		"---------------------------------------------------------\n"
		"Necessary to compute the autocorrelation and to calculate its maximum\n"
		"%R->low-pass %r->band-pass\n"
		"Filter freq %6d width %6d (for BP)\n"
		"Windowing %b Remove-dc %b expansion %6d\n"
		"expanding factor %8d \n"
	"---------------------------------------------------------\n"
		"-----------------Shift ranging-------------------------\n"
		"x_0 Shift start point %8f"
		"x_0 Shift end point %8f"
		"x_0 Shift step %8f \n"
		,&eqn,&x0,&lamb,&sig,&nf,&Nb_Pixel,&idum,&bp,&filf,&filw,&win,&rdc,&n_expan
		,&Nb_Max,&Min_Shift,&Max_Shift,&Step_Shift);

  if (i == WIN_CANCEL)	return OFF;

  if ((opd = create_and_attach_one_plot(pr, Nb_Pixel,Nb_Pixel, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  dsd = opd->dat[0];
  set_plot_y_title(opd, " Photon distribution");
  set_plot_x_title(opd, " position");

  if ((opc = create_and_attach_one_plot(pr, Nb_Pixel*n_expan,Nb_Pixel*n_expan, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  dsc = opc->dat[0];
  set_plot_y_title(opc, " autocovonlution value");
  set_plot_x_title(opc, " position");

  /* now we must do some house keeping */
  set_ds_source(dsm," x0 %g lambda %g sigma %g",x0,lamb,sig);
  set_plot_title(opm," x0 %g lambda %g sigma %g",x0,lamb,sig);
  if (Max_Shift>(Min_Shift+Step_Shift))
    {

      Nb_Step = 1 + (int)round((Max_Shift-Min_Shift)/Step_Shift);
      if ((ops = create_and_attach_one_plot(pr, Nb_Step, Nb_Step, 0)) == NULL)
	return win_printf_OK("cannot create plot for standard deviation !");
      dss = ops->dat[0];
      set_plot_y_title(ops, "Max_Position");
      set_plot_x_title(ops, "Shift Value");
      for (k=0;k<=Nb_Step;k++)
	{
	  Shift=Min_Shift+k*Step_Shift;
	  fp = fftbt_init(fp, dsd->nx);
	  fpe = fftbt_init(fpe, dsd->nx*n_expan);
	  fil = filter_init_bt(fil, dsd->nx);
	  if (eqn == 0)   do_gaussian_Function(dsd, sig,nf,Shift);
	  else           do_gaussian_Interference_Function(dsd, sig,lamb,nf,Shift,Shift);

	  correlate_1d_sig_and_invert(dsd->yd, dsd->nx, win, dsc->yd, filf, filw, ((bp) ? BAND_PASS: LOW_PASS), rdc, fp, fil, &noise);

	  for (i = dsd->nx; i < dsc->nx; i++) dsc->yd[i] = 0;
	  // we fft on dsd->nx points
	  realtr1bt(fp, dsc->yd);           fftbt(fp, dsc->yd, 1);      realtr2bt(fp, dsc->yd, 1);
	  // we fft-1 on n_expan * dsd->nx points
	  realtr2bt(fpe, dsc->yd, -1);      fftbt(fpe, dsc->yd, -1);      realtr1bt(fpe, dsc->yd);


	  for (i = 0; i < dsc->nx; i++) dsc->xd[i] = dsd->xd[0] + ((dsd->xd[dsd->nx-1] - dsd->xd[0])*i)/dsc->nx;
	  find_max1(dsc->yd, dsc->nx, &Max_pos, &Max_val, &Max_deriv, dsc->nx);
	  Max_pos /= 2*n_expan;
	  if (dss != NULL)
	    {
	      dss->xd[k] = Shift;
	      dss->yd[k] = Max_pos;
	    }
	  else win_printf(" The value of the standard deviation is %g and the shift position is  %g",StandDev, Shift);
	}
    }
  free_fft_plan_bt(fp);
  free_filter_bt(fil);
  free_fft_plan_bt(fpe);

  //  set_plot_x_title(opd, "Position");
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;

}



//-------------------------------start of generate_Shifted_profil_image-----------------

int generate_Shifted_profil_image (void)
{
  register int i,k;
  static int nf = 10000, eqn = 1, Nb_Pixel=256, Ns = 1024, wn = 1, x1shiftmode=0;
  static float Min_Shift = 0, Max_Shift = 10, Shift,Shift_x1;
  static float x0 = 0, x1=0, lamb = 8, sig = 20;
  static float ap=0.0,bp=0.0,cp=1.2,dp=0.0;
  static float x1bound;
  O_p *op =NULL;
  int size=0;
  d_s *ds=NULL;

  d_s *dsd = NULL; /*dsd is the data set of the Photon distribution, */
  imreg *imr = NULL;
  static int idum = 56;
  O_i *oid = NULL;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the y coordinate"
			   "of a data set by a number and place it in a new plot");
    }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%imr",&imr) != 1)
    return win_printf_OK("cannot find image ");

  i = win_scanf("This function simulate a random photon distribution\n"
		"corresponding to an interference patern with one of these equations:\n\n"
		"%R exp-(x^2/2*\\sigma^2)\n\n"
		"%r ([1 + cos ({2\\pi (x - x_1)}\{\\lambda})]/2)*exp-((x-x_0)^2/2*\\sigma^2)\n\n"
		"Specify x_0 %8f, \\lambda  %8f, \\sigma %8f\n"
		"With how many photons %8d\n"
		"How many pixel in the x axis %8d"
		"with noise %b Random seed %8d\n"
		"---------------------------------------------------------\n"
		"-----------------Shift ranging-------------------------\n"
		"x_0 Shift start point %8f"
		"x_0 Shift end point %8f  \n"
    "-----------------------------------Shift of x1 -------------------------\n"
    "%R same shift as x0\n\n"
    "%r polynom of x0 shift to check non linearity \n\n"
    "%r bound shift to check phase jumps\n\n"
    "------------------------if Polynomial shift of x1 -------------------------\n"
    "x_1  shift is a polynome of degree 3 of x_0 shift. Please precise the coeffs :  \n"
    "a_3=%5f b_2=%5f c_1=%5f d_0=%5f \n"
    "-------------------------------if bound shift of x1 -------------------------\n"
    "Enter the bound for x1-x0 : %5f \n"
    "Nb of steps %8d \n"

		,&eqn,&x0,&lamb,&sig,&nf,&Nb_Pixel,&wn,&idum,&Min_Shift,&Max_Shift,&x1shiftmode,&ap,&bp,&cp,&dp,&x1bound,&Ns);
  float x1_table[Ns];

  if (i == WIN_CANCEL)	return OFF;

  oid = create_and_attach_oi_to_imr(imr, Nb_Pixel, Ns, IS_FLOAT_IMAGE);
  if (oid == NULL)	return win_printf_OK("cannot create profile !");
  dsd = build_data_set(Nb_Pixel,Nb_Pixel);
  if (dsd == NULL)	return win_printf_OK("cannot create data sets !");

  for (i = 0; i < Ns; i++)
    {
      Shift = Min_Shift + i * (Max_Shift - Min_Shift)/Ns;

      if (x1shiftmode==0) Shift_x1=Shift;
      else if (x1shiftmode==1) Shift_x1=ap*pow(Shift,3)+bp*pow(Shift,2)+cp*Shift+dp;
      else if (x1shiftmode==2) Shift_x1=Shift+x1bound*sin(2*M_PI*4*(float)i/Ns);

      x1_table[i]=Nb_Pixel/2+Shift_x1;

      if (wn)
	{
	  if (eqn == 0)   do_fill_gaussian_Photons_Distribution(dsd, sig, &idum,nf,Shift);
	  else   do_fill_gaussian_Interference_Photons_Distribution(dsd, sig, lamb, &idum, nf, Shift);
	}
      else
	{
	  if (eqn == 0)   do_gaussian_Function(dsd, sig,nf,Shift);
	  else           do_gaussian_Interference_Function(dsd, sig,lamb,nf,Shift,Shift_x1);
	}
      if (i%64 == 0) my_set_window_title("Preparing profile %d",i);
      for (k = 0; k < Nb_Pixel; k++)
	{
	  oid->im.pixel[i].fl[k] = dsd->yd[k];
	}
    }
  free_data_set(dsd);
  if (oid != NULL) find_zmin_zmax(oid);
  size = oid->im.ny;

  op = create_and_attach_op_to_oi(oid, size, size, 0, IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
  if (op == NULL) return (win_printf_OK("cant create plot!"));
  ds = op->dat[0];
  for (i=0;i<size;i++)
  {
    ds->xd[i]=i;
    ds->yd[i]=x1_table[i];
  }
  set_ds_source(ds,"Position of the center of the fringes");
  broadcast_dialog_message(MSG_DRAW,0);
  return D_REDRAWME;
}




int ErrDeviation_Max_Shift_complexify(void)
{
  register int i, j, k;
  static int nf = 10000, eqn = 1, Nb_Pixel = 256, jj;
  static float Min_Shift = 0,Step_Shift = 0.1, Max_Shift = 10, Shift;
  static float x0 = 0, lamb = 8, sig = 20;
  O_p  *opd = NULL, *opm = NULL,*opc=NULL, *ops=NULL;
  d_s *dsd = NULL; //dsd is the data set of the Photon distribution,
  d_s *dsc = NULL; //dsc is the data set of the auto Conconv
  d_s *dsm = NULL; // dsm is the data set of the autoconvolution maximum
  d_s *dss = NULL;
  d_s *dsphi = NULL;
  //float tmp, val;
  pltreg *pr = NULL;
  static int idum = 56;
  static int bp = 0, filf = 32, filw = 4, win = 0, rdc = 0, n_phi = 4;
  int  Nb_Step = 0, imax;
  float noise, zero, max;
  fft_plan *fp = NULL;
  filter *fil = NULL;


  //float l = sig/3;
  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the y coordinate"
			   "of a data set by a number and place it in a new plot");
    }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return win_printf_OK("cannot find data");

  i = win_scanf("This function simulate a random photon distribution\n"
		"corresponding to an interference patern with one of these equations:\n\n"
		"%R exp-\\(\\frac{x^2}{2*\\sigma^2}\\)\n\n"
		"%r (\\frac{1 + cos ({2\\pi (x - x_0)}\{\\lambda})}{2})*exp-\\(\\frac{(x-x_0)^2}{2*\\sigma^2)}\n\n"
		"Specify x_0 %6f, \\lambda  %6f, \\sigma %6f\n"
		"With how many photons %8d\n"
		"How many pixel in the x axis %8d"
		"Random seed %8d\n"
		"---------------------------------------------------------\n"
		"Autocorrelation maximum caculated from complexified data\n"
		"%R->low-pass %r->band-pass\n"
		"Filter freq %6d width %6d (for BP)\n"
		"Windowing %b Remove-dc %b\n"
		"Zero is find over 2n+1 points specify n:%8d \n"
		"---------------------------------------------------------\n"
		"-----------------Shift ranging-------------------------\n"
		"x_0 Start point %4f"
		"x_0 End point %4f"
		"x_0 Step %4f \n"
		,&eqn,&x0,&lamb,&sig,&nf,&Nb_Pixel,&idum,&bp,&filf,&filw,&win,&rdc,&n_phi
		,&Min_Shift,&Max_Shift,&Step_Shift);


  if (i == WIN_CANCEL)	return OFF;

  if ((opd = create_and_attach_one_plot(pr, Nb_Pixel,Nb_Pixel, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  dsd = opd->dat[0];
  set_plot_y_title(opd, " Photon distribution");
  set_plot_x_title(opd, " position");

  if ((opc = create_and_attach_one_plot(pr, Nb_Pixel,Nb_Pixel, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  dsc = opc->dat[0];
  set_plot_y_title(opc, " autocovonlution value");
  set_plot_x_title(opc, " position");

  /* now we must do some house keeping */
  set_ds_source(dsm," x0 %g lambda %g sigma %g",x0,lamb,sig);
  set_plot_title(opm," x0 %g lambda %g sigma %g",x0,lamb,sig);
  dsphi = build_data_set(2*n_phi+1, 2*n_phi+1);
  fp = fftbt_init(fp, dsd->nx);
  fil = filter_init_bt(fil, dsd->nx);

  if (Max_Shift>(Min_Shift+Step_Shift))
    {

      Nb_Step = 1 + (int)round((Max_Shift-Min_Shift)/Step_Shift);
      if ((ops = create_and_attach_one_plot(pr, Nb_Step, Nb_Step, 0)) == NULL)
	return win_printf_OK("cannot create plot for standard deviation !");
      dss = ops->dat[0];
      set_plot_y_title(ops, "Max_Position");
      set_plot_x_title(ops, "Shift Value");

      for (k=0;k<=Nb_Step;k++)
	{
	  Shift = Min_Shift + k * Step_Shift;
	  if (eqn == 0)   do_gaussian_Function(dsd, sig,nf,Shift);
	  else            do_gaussian_Interference_Function(dsd, sig,lamb,nf,Shift,Shift);
	  if (bp)
	    correlate_1d_sig_and_invert_bp_and_complexify(dsd->yd, dsd->nx, win, dsc->yd, dsc->xd, filf, filw, rdc, fp, fil, &noise);
	  else
	    correlate_1d_sig_and_invert_lp_and_complexify(dsd->yd, dsd->nx, win, dsc->yd, dsc->xd, filf, rdc, fp, fil, &noise);
	  for (j = dsc->nx/4, imax = -1; j < 3*dsc->nx/4; j++)
	    max = ((imax < 0) || (dsc->yd[j] > max)) ? dsc->yd[imax = j] : max;
	  for (j = 0; j <= 2*n_phi; j++)
	    {
	      jj = imax - n_phi + j;
	      jj += (jj < 0) ? dsc->nx : 0;
	      jj = (jj < dsc->nx) ? jj : jj - dsc->nx;
	      dsphi->yd[j] = dsc->xd[jj];
	      dsphi->xd[j] = j - n_phi;
	    }
	  find_phase_zero_with_poly(dsphi, &zero);
	  if (dss != NULL)
	    {
	      dss->xd[k] = Shift;
	      dss->yd[k] = ((zero + imax) - dsc->nx/2)/2;
	    }
	  else win_printf("The zero position is %g\nand the shift position is  %g"
			  ,((zero + imax) - dsc->nx/2)/2, Shift);
	}
    }
  free_fft_plan_bt(fp);
  free_filter_bt(fil);
  free_data_set(dsphi);
  //  set_plot_x_title(opd, "Position");
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}



//------------------End -ErrDevia_Max_Shift--------------------------------
int do_Distribution_and_autoconv(void)
{
  do_FitInterference_Photon_Distribution();
  do_FitInterference_compute_autoconv();
   return 0;
}


//-------------end of the section_3 writen by Samar----------------------------

//---------------------------------start_TrkMax------------------------------------


int do_trk_local_convol_max_along_y(void)
{
  register int i, j;
  int r_s, r_e, size, nfi;
  static int imax = 5, lnx = 128, win=0, rdc=0, n_expan=4, imdb = 0; // , filf=8, filw=4, , bp=0
  static int lp_filter = 3, bp_filter = 8, bp_width = 3;
  O_i *ois, *oid = NULL;
  O_p *op;
  d_s *ds, *dst, *dsc=NULL;
  imreg *imr;
  float noise, Max_pos, Max_val, Max_deriv;
  int k;
  filter *fil= NULL, *filbp = NULL;
  fft_plan *fp = NULL, *fpe= NULL;


  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    return D_O_K;
  nfi = abs(ois->im.n_f);
  nfi = (nfi == 0) ? 1 : nfi;
  if(updating_menu_state != 0)
    {
      if (nfi <= 1) 	 active_menu->flags &= ~D_DISABLED;
      else               active_menu->flags |=  D_DISABLED;
      return D_O_K;
    }



  r_s = 0; r_e = ois->im.ny;
  i = win_scanf("look for maximum by autoconvolution in X near position %8d\n"
		"Over %8d points\n"
		"starting at line %8d and ending at line %8d\n"
		"---------------------------------------------------------\n"
		"Necessary to compute the autocorrelation and to calculate its maximum\n"
		"For low-pass f_c%6d (<0 no lp). For band-pass filter\n"
		"freq %6d width %6d (<0 no bp)\n"
		"Windowing %b Remove-dc %b expansion %6d\n"
		"Debug image: None%R profile%r correl%r expand%r\n"
		,&imax,&lnx,&r_s,&r_e,&lp_filter,&bp_filter,&bp_width,&win,&rdc,&n_expan,&imdb);
  if (i == WIN_CANCEL) return D_O_K;
  size = 1 + r_e - r_s;
  size = (size > ois->im.ny) ? ois->im.ny : size;
  op = create_and_attach_op_to_oi(ois, size, size, 0, IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
  if (op == NULL) return (win_printf_OK("cant create plot!"));
  ds = op->dat[0];
  set_plot_title(op,"Center %d (%d to %d)",imax,r_s, r_e);
  set_ds_treatement(ds,"Local max at %d (%d to %d)",imax,r_s, r_e);
  inherit_from_im_to_ds(ds, ois);
  uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
  uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
  op->filename = strdup(ois->filename);
  dst = build_data_set(ois->im.nx, ois->im.nx);
  if (dst == NULL) return (win_printf_OK("cant create dst!"));
  dsc = build_data_set(lnx*n_expan, lnx*n_expan);
  fp = fftbt_init(fp, lnx);
  fpe = fftbt_init(fpe, lnx*n_expan);
  fil = filter_init_bt(fil, lnx);
  filbp = filter_init_bt(filbp, lnx);
  if (imdb == 1 || imdb == 2)
    {
      oid = create_and_attach_oi_to_imr(imr, lnx, size, IS_FLOAT_IMAGE);
      if (oid == NULL)	return win_printf_OK("cannot create profile !");
    }

  if (imdb == 3)
    {
      oid = create_and_attach_oi_to_imr(imr, lnx*n_expan, size, IS_FLOAT_IMAGE);
      if (oid == NULL)	return win_printf_OK("cannot create profile !");
    }


  for (i=0 ; i< size ; i++)
    {
      extract_raw_line(ois, i+r_s, dst->yd);
      for(j = 0; j < lnx; j++)
	{
	  k = imax - lnx/2 + j;
	  k = (k < 0) ? 0 : k;
	  k = (k < ois->im.nx) ? k : ois->im.nx - 1;
	  dsc->xd[j] = dst->yd[k];
	  dst->xd[j] = k;
	  if (oid != NULL && imdb == 1) oid->im.pixel[i].fl[j] = dst->yd[k];
	}

      //correlate_1d_sig_and_invert(dsc->xd, lnx, win, dsc->yd, filf, filw,
      //			    ((bp) ? BAND_PASS: LOW_PASS), rdc, fp, fil, &noise);


      correlate_1d_sig_and_invert_lp_and_bp(dsc->xd, lnx, win, dsc->yd, lp_filter, bp_filter,
					    bp_width, rdc, fp, fil, filbp, &noise);

	if (oid != NULL && imdb == 2)
	  {
	    for(j = 0; j < lnx; j++)
	      oid->im.pixel[i].fl[j] = dsc->yd[j];
	  }
	for (j = lnx; j < dsc->nx; j++) dsc->yd[j] = 0;
	  // we fft on dsd->nx points
	realtr1bt(fp, dsc->yd);           fftbt(fp, dsc->yd, 1);      realtr2bt(fp, dsc->yd, 1);
	  // we fft-1 on n_expan * dsd->nx points
	realtr2bt(fpe, dsc->yd, -1);      fftbt(fpe, dsc->yd, -1);      realtr1bt(fpe, dsc->yd);
	if (oid != NULL && imdb == 3)
	  {
	    for(j = 0; j < dsc->nx; j++)
	      oid->im.pixel[i].fl[j] = dsc->yd[j];
	  }


	for (j = 0; j < dsc->nx; j++)
	    dsc->xd[j] = dst->xd[0] + ((dst->xd[lnx-1] - dst->xd[0])*j)/dsc->nx;

	find_max1(dsc->yd, dsc->nx, &Max_pos, &Max_val, &Max_deriv, dsc->nx);
	Max_pos /= 2*n_expan;
	ds->xd[i] = imax + Max_pos;
	ds->yd[i] = i + r_s;
	//imax = (int)Max_pos;
    }
  //add_data_to_one_plot(op, IS_DATA_SET, (void*)dst);
  //add_data_to_one_plot(op, IS_DATA_SET, (void*)dsc);
  free_fft_plan_bt(fp);
  free_filter_bt(fil);
  free_fft_plan_bt(fpe);
  free_filter_bt(filbp);

  free_data_set(dst);
  free_data_set(dsc);
  if (oid != NULL) find_zmin_zmax(oid);
  broadcast_dialog_message(MSG_DRAW,0);
  return D_REDRAWME;
}

/*
 */
int	hilbert_1d_sig(float *x1, int nx, int window, float *fx1, int bp_filter, int bp_width, fft_plan *fp, filter *fil)
{
    register int i, j;
    float moy, smp = 0.05;

    if (x1 == NULL)				return WRONG_ARGUMENT;
    if (fp == NULL || fil == NULL)          return FFT_NOT_SUPPORTED;
    if (fftbt_init(fp, nx) == NULL)         return FFT_NOT_SUPPORTED;
    //if (fft_init(nx) || filter_init(nx))	return FFT_NOT_SUPPORTED;
    if (fx1 == NULL)	fx1 = (float*)calloc(nx,sizeof(float));
    if (fx1 == NULL)			return OUT_MEMORY;


    // we cleam mean values
    for(i = 0; i < nx; i++)		fx1[i] = x1[i];
    for(i = 0, j = (3*nx)/4, moy = 0; i < nx/4; i++, j++)
        moy += fx1[i] + fx1[j];
    for(i = 0, moy = (2*moy)/nx; window && i < nx; i++)
        fx1[i] = x1[i] - moy;
    if (window == 1)	fftbtwindow1(fp, fx1, smp);
    else if (window == 2)	fftwindow_flat_top1(fp, nx, fx1, smp);

    /*	compute fft of x1 as real data */
    realtr1bt(fp, fx1);
    fftbt(fp, fx1, 1);
    realtr2bt(fp, fx1, 1);

    if (bp_filter> 0 && bp_width > 0 )
      bandpass_smooth_half_bt (fil, nx, fx1, bp_filter, bp_width);
    /*	compute normalization without DC and Nyquist*/


    /*	get back to complex world */
    fftbt(fp, fx1, -1);
    if (window)		defftbtwc1(fp,  fx1, smp);
    return 0;
}

int	max_of_amplitude(float *fx1, int nx, int *maxi, float *maxa2)
{
  int i, imax = 0;
  float max, *re, *im, amp;

  for(i = 0, max = 0; i < nx; i += 2)
    {
      re = fx1 + i;
      im = re +1;
      amp = (*re) * (*re) + (*im) * (*im);
      if (i == 0 || amp > max)
	{
	  imax = i;
	  max = amp;
	}
    }
  if (maxi) *maxi = imax;
  if (maxa2) *maxa2 = max;
  return 0;
}

// grab 2 * n_phi + 1 phase points
int	grab_phase_around_max_of_amplitude(float *fx1, int nx, int maxi, int n_phi, d_s *dsphi, int *iph0)
{
  int i, imin = 0;
  static int n_phil = 0;
  float min, *re = NULL, *im = NULL;
  static float *phil = NULL;

  if (phil == NULL || nx/2 > n_phil)
    {
      phil = (float*)realloc(phil,sizeof(float)*nx/2);
      if (phil == NULL) return 1;
      n_phil = nx/2;
    }

  for(i = 0; i < nx; i += 2)
    {
      re = fx1 + i;
      im = re +1;
      if ( *re == 0.0 && *im == 0.0)	phil[i/2] = 0;
      else 	phil[i/2] = (float)atan2 ( *im, *re );
    }
  for(i = maxi/2, min = phil[i]; i < nx/2; i++)
    {
      if (fabs(phil[i]) > fabs(min)) break;
      else min = phil[imin = i];
    }
  for(i = imin ; i >= 0; i--)
    {
      if (fabs(phil[i]) > fabs(min)) break;
      else min = phil[imin = i];
    }
  for (i = 0; i <= 2*n_phi; i++)
    {
      dsphi->yd[i] = phil[imin - n_phi + i];
      dsphi->xd[i] = i - n_phi;
    }
  if (iph0) *iph0 = imin;
  return 0;
}

int find_phase_zero_with_poly(d_s *dsphi, float *zero)
{
  double *a = NULL, xmin, ymin;
  int j, ret, imin, debug = WIN_CANCEL;

  ret = fit_ds_to_xn_polynome(dsphi, 4, -FLT_MAX, FLT_MAX, -FLT_MAX, FLT_MAX, &a);
  if (debug != WIN_CANCEL) debug = win_printf("\\fbox{\\stack{{\\sl y = \\Sigma_{i=0}^{i<=%d} a_i x^i }"
					  "{a_0 =  %e}{a_1 = %e}{a_2 = %e}{a_3 = %e}{ret %d}}}"
					  ,3,a[0],a[1],a[2],a[3],ret);
  for (j = 0, imin = 0, ymin = dsphi->yd[0]; j < dsphi->nx; j++)
    {
      if (fabs(dsphi->yd[j]) < fabs(ymin))
	{
	  imin = j;
	  ymin = dsphi->yd[j];
	}
    }
  for (xmin = dsphi->xd[imin], j = 0; j < 10; j++)
    {
      xmin -= ymin/a[1];
      ymin = a[0] + a[1] * xmin + a[2] * xmin * xmin + a[3] * xmin * xmin * xmin;
      if (debug != WIN_CANCEL) debug = win_printf("iter %d xmin %g ymin %g",j,xmin,ymin);
    }
  if (zero) *zero = xmin;
  if (a != NULL) free(a);
  return 0;
}




int do_trk_local_phase_max_along_y(void)
{
  register int i, j;
  int r_s, r_e, size, nfi, comp;
  static int  lnx = 128, win=0, rdc=0, imdb = 0; // , filf=8, filw=4, , bp=0
  static int bp_filter = 7, bp_width = 3, n_phi = 3;
  O_i *ois, *oid = NULL;
  O_p *op;
  d_s *ds, *dst, *dsphi = NULL;
  imreg *imr;
  int iph0 = 0, maxi = 0;
  float zero = 0, maxa2;
  filter *filbp = NULL;
  fft_plan *fp = NULL;


  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)    return D_O_K;
  nfi = abs(ois->im.n_f);
  nfi = (nfi == 0) ? 1 : nfi;
  comp = (ois->im.data_type == IS_COMPLEX_IMAGE
	  || ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE) ? 1 : 0;
  if(updating_menu_state != 0)
    {
      if (nfi <= 1 && comp) 	 active_menu->flags &= ~D_DISABLED;
      else               active_menu->flags |=  D_DISABLED;
      return D_O_K;
    }

  r_s = 0; r_e = ois->im.ny;
  i = win_scanf("look for maximum by wave and zero of phase\n"
		"starting at line %8d and ending at line %8d\n"
		"---------------------------------------------------------\n"
		"Necessary to compute the autocorrelation and to calculate its maximum\n"
		"Band-pass filter freq %6d width %6d \n"
		"Windowing %b Remove-dc %b\n"
		"Phase zero will be search of 2n+1 points n=%3d\n"
		"Debug image: None%R profile%r comp%r\n"
		,&r_s,&r_e,&bp_filter,&bp_width,&win,&rdc,&n_phi,&imdb);
  if (i == WIN_CANCEL) return D_O_K;
  size = 1 + r_e - r_s;
  size = (size > ois->im.ny) ? ois->im.ny : size;
  op = create_and_attach_op_to_oi(ois, size, size, 0, IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
  if (op == NULL) return (win_printf_OK("cant create plot!"));
  ds = op->dat[0];
  set_plot_title(op,"(%d to %d)",r_s, r_e);
  set_ds_treatement(ds,"Local BP max  and phase zero (%d to %d)",r_s, r_e);
  inherit_from_im_to_ds(ds, ois);
  uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
  uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
  op->filename = strdup(ois->filename);
  dst = build_data_set(ois->im.nx, ois->im.nx);
  dsphi = build_data_set(2*n_phi+1, 2*n_phi+1);
  if (dst == NULL || dsphi == NULL) return (win_printf_OK("cant create dst!"));
  lnx = ois->im.nx;
  fp = fftbt_init(fp, lnx);
  filbp = filter_init_bt(filbp, lnx);
  if (imdb == 1 || imdb == 2)
    {
      oid = create_and_attach_oi_to_imr(imr, lnx, size, IS_FLOAT_IMAGE);
      if (oid == NULL)	return win_printf_OK("cannot create profile !");
    }

  for (i=0 ; i< size ; i++)
    {
      extract_raw_line(ois, i+r_s, dst->yd);
      for(j = 0; j < lnx; j++)
	{
	  dst->xd[j] = j;
	  if (oid != NULL && imdb == 1) oid->im.pixel[i].fl[j] = dst->yd[j];
	}
      hilbert_1d_sig(dst->yd, lnx, win, dst->xd, bp_filter, bp_width, fp, filbp);
      if (oid != NULL && imdb == 2)
	{
	  for(j = 0; j < lnx; j++)
	    oid->im.pixel[i].fl[j] = dst->xd[j];
	}
      max_of_amplitude(dst->xd, lnx, &maxi, &maxa2);
      grab_phase_around_max_of_amplitude(dst->xd, lnx, maxi, n_phi, dsphi, &iph0);
      find_phase_zero_with_poly(dsphi, &zero);
      ds->xd[i] = 2*(zero + iph0);
      ds->yd[i] = i + r_s;
    }
  //add_data_to_one_plot(op, IS_DATA_SET, (void*)dst);
  //add_data_to_one_plot(op, IS_DATA_SET, (void*)dsphi);
  free_data_set(dst);
  free_data_set(dsphi);
  free_fft_plan_bt(fp);
  free_filter_bt(filbp);

  if (oid != NULL) find_zmin_zmax(oid);
  broadcast_dialog_message(MSG_DRAW,0);
  return D_REDRAWME;
}



int do_trk_local_zero_of_imaginary_along_y(void)
{
  register int i, j, k;
  int r_s, r_e, size, imax, debug = WIN_CANCEL, nfi;
  static int  lnx = 128, win=0, rdc=0, imdb = 0; // , filf=8, filw=4, , bp=0
  static int lp_filter = 7, n_phi = 3;
  O_i *ois, *oid = NULL;
  O_p *op;
  d_s *ds, *dst, *dsphi = NULL;
  imreg *imr;
  float zero = 0, max;
  filter *fillp = NULL;
  fft_plan *fp = NULL;


  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)    return D_O_K;
  nfi = abs(ois->im.n_f);
  nfi = (nfi == 0) ? 1 : nfi;
  if(updating_menu_state != 0)
    {
      if (nfi <= 1) 	 active_menu->flags &= ~D_DISABLED;
      else               active_menu->flags |=  D_DISABLED;
      return D_O_K;
    }






  r_s = 0; r_e = ois->im.ny;
  i = win_scanf("look for maximum by wave and zero of phase\n"
		"starting at line %8d and ending at line %8d\n"
		"---------------------------------------------------------\n"
		"Necessary to compute the autocorrelation and to calculate its maximum\n"
		"Low-pass filter freq %6d \n"
		"Windowing %b Remove-dc %b\n"
		"Phase zero will be search of 2n+1 points n=%3d\n"
		"Debug image: None%R profile%r comp%r\n"
		,&r_s,&r_e,&lp_filter,&win,&rdc,&n_phi,&imdb);
  if (i == WIN_CANCEL) return D_O_K;
  size = 1 + r_e - r_s;
  size = (size > ois->im.ny) ? ois->im.ny : size;
  op = create_and_attach_op_to_oi(ois, size, size, 0, IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
  if (op == NULL) return (win_printf_OK("cant create plot!"));
  ds = op->dat[0];
  set_plot_title(op,"(%d to %d)",r_s, r_e);
  set_ds_treatement(ds,"Local BP max  and phase zero (%d to %d)",r_s, r_e);
  inherit_from_im_to_ds(ds, ois);
  uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
  uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
  op->filename = strdup(ois->filename);
  dst = build_data_set(ois->im.nx, ois->im.nx);
  dsphi = build_data_set(2*n_phi+1, 2*n_phi+1);
  if (dst == NULL || dsphi == NULL) return (win_printf_OK("cant create dst!"));
  lnx = ois->im.nx;
  fp = fftbt_init(fp, lnx);
  fillp = filter_init_bt(fillp, lnx);
  if (imdb == 1 || imdb == 2)
    {
      oid = create_and_attach_oi_to_imr(imr, lnx, size, IS_FLOAT_IMAGE);
      if (oid == NULL)	return win_printf_OK("cannot create profile !");
    }

  for (i=0 ; i< size ; i++)
    {
      extract_raw_line(ois, i+r_s, dst->yd);
      for(j = 0; j < lnx; j++)
	{
	  dst->xd[j] = j;
	  if (oid != NULL && imdb == 1) oid->im.pixel[i].fl[j] = dst->yd[j];
	}

      realtr1bt(fp, dst->yd);
      fftbt(fp, dst->yd, 1);
      realtr2bt(fp, dst->yd, 1);

      if (lp_filter> 0)
	lowpass_smooth_half_bt(fillp, lnx, dst->yd, lp_filter);

      /*	get back to complex world */
      for (j = 2;j < lnx; j+=2)
	{
	  dst->xd[j+1] = dst->yd[j];
	  dst->xd[j] = -dst->yd[j+1];
	}
      dst->xd[1] = dst->xd[0] = 0;



      realtr2bt(fp, dst->xd, 1);
      fftbt(fp, dst->xd, -1);
      realtr1bt(fp, dst->xd);

      realtr2bt(fp, dst->yd, 1);
      fftbt(fp, dst->yd, -1);
      realtr1bt(fp, dst->yd);

      if (oid != NULL && imdb == 2)
	{
	  for(j = 0; j < lnx; j++)
	    oid->im.pixel[i].fl[j] = dst->xd[j];
	}
      for (j = imax = 0; j < lnx; j++)
	max = ((j == 0) || (dst->yd[j] > max)) ? dst->yd[imax = j] : max;


      for (j = 0; j <= 2*n_phi; j++)
	{
	  k = imax - n_phi + j;
	  k += (k < 0) ? lnx : 0;
	  k = (k < lnx) ? k : k - lnx;
	  dsphi->yd[j] = dst->xd[k];
	  dsphi->xd[j] = j - n_phi;
	}

      j = find_phase_zero_with_poly(dsphi, &zero);
      if (debug != WIN_CANCEL) debug = win_printf("i %d ret %d zero %g",i,j,zero);
      ds->xd[i] = zero + imax;
      ds->yd[i] = i + r_s;
    }
  //add_data_to_one_plot(op, IS_DATA_SET, (void*)dst);
  //add_data_to_one_plot(op, IS_DATA_SET, (void*)dsphi);
  free_data_set(dst);
  free_data_set(dsphi);
  free_fft_plan_bt(fp);
  free_filter_bt(fillp);

  if (oid != NULL) find_zmin_zmax(oid);
  broadcast_dialog_message(MSG_DRAW,0);
  return D_REDRAWME;
}


int complexify_profile(d_s *dst, int lp_filter, fft_plan *fp, filter *fillp)
{
  int j;

  realtr1bt(fp, dst->yd);
  fftbt(fp, dst->yd, 1);
  realtr2bt(fp, dst->yd, 1);

  if (lp_filter> 0)
    lowpass_smooth_half_bt(fillp, dst->nx, dst->yd, lp_filter);

  /*	get back to complex world */
  for (j = 2;j < dst->nx; j+=2)
    {
      dst->xd[j+1] = dst->yd[j];
      dst->xd[j] = -dst->yd[j+1];
    }
  dst->xd[1] = dst->xd[0] = 0;



  realtr2bt(fp, dst->xd, 1);
  fftbt(fp, dst->xd, -1);
  realtr1bt(fp, dst->xd);

  realtr2bt(fp, dst->yd, 1);
  fftbt(fp, dst->yd, -1);
  realtr1bt(fp, dst->yd);
  return 0;
}

int	draw_shear_rect_vga_screen_unit_w_profiles(int xc, int yc, int length, int width, int h_size, int shear_in_y, int dz, int color, BITMAP* bm, float scx, float scy, float *ampt, float maxt, float *ampb, float maxb, float *profy, float maxprof, int lny)
{
  int l = length >> 1;
  int w = width >> 2;
  int y = bm->h - yc;
  int x1, x2, x3, x4, y1, y2, y3, y4, xz, yz, xp, yp, xp_1, yp_1;
  int i;

  y = yc;

  if (shear_in_y == 0)
    {
      l = (int)(0.5+ scx * l);
      w = (int)(0.5+ scy * w);
      h_size = (int)(0.5+ scy * h_size);
      x1 =  xc - l;
      x2 =  xc + l;
      y1 =  y - w - h_size;
      y2 =  y - w + h_size;
      y3 =  y + w - h_size;
      y4 =  y + w + h_size;
      xz = xc-(int)(0.5+dz*scx/2);
      line(bm, x1, y1, x2, y1, color);
      line(bm, x2, y1, x2, y2, color);
      line(bm, x2, y2, x1, y2, color);
      line(bm, x1, y2, x1, y1, color);
      line(bm, xz, y1, xz, y2, color);
      line(bm, xc, y3, xc, y2, color);

      xz = xc+(int)(0.5+dz*scx/2);
      line(bm, x1, y3, x2, y3, color);
      line(bm, x2, y3, x2, y4, color);
      line(bm, x2, y4, x1, y4, color);
      line(bm, x1, y4, x1, y3, color);
      line(bm, xz, y3, xz, y4, color);
      if (ampt != NULL)
	{
	  xp_1 = x1; yp_1 = y2 - (h_size*2*ampt[0])/maxt;
	  for (i = 1; i < length; i++)
	    {
	      xp = x1 + (i*(x2-x1))/length;
	      yp = y2 - (h_size*2*ampt[i])/maxt;
	      line(bm, xp_1, yp_1, xp, yp, color);
	      xp_1 = xp;
	      yp_1 = yp;
	    }
	}
      if (ampb != NULL)
	{
	  xp_1 = x1; yp_1 = y4 - (h_size*2*ampb[0])/maxb;
	  for (i = 1; i < length; i++)
	    {
	      xp = x1 + (i*(x2-x1))/length;
	      yp = y4 - (h_size*2*ampb[i])/maxb;
	      line(bm, xp_1, yp_1, xp, yp, color);
	      xp_1 = xp;
	      yp_1 = yp;
	    }
	}
      if (profy != NULL)
	{
	  w = (int)(0.5+ scy * lny);
	  xp_1 = xc + (l*2*profy[0])/maxprof; yp_1 = yc + w/2;
	  for (i = 1; i < lny; i++)
	    {
	      xp = xc + (l*2*profy[i])/maxprof;
	      yp = yc + (w/2) - (i*w)/lny;
	      line(bm, xp_1, yp_1, xp, yp, color);
	      xp_1 = xp;
	      yp_1 = yp;
	    }
	}
    }
  else
    {
      l = (int)(0.5+ scy * l);
      w = (int)(0.5+ scx * w);
      h_size = (int)(0.5+ scx * h_size);
      y1 =  y - l;
      y2 =  y + l;
      x1 =  xc - w - h_size;
      x2 =  xc - w + h_size;
      x3 =  xc + w - h_size;
      x4 =  xc + w + h_size;
      yz = yc-(int)(0.5+dz*scy/2);
      line(bm, x1, y1, x2, y1, color);
      line(bm, x2, y1, x2, y2, color);
      line(bm, x1, y2, x1, y1, color);
      line(bm, x2, y2, x1, y2, color);
      line(bm, x1, yz, x2, yz, color);
      line(bm, x3, yc, x2, yc, color);

      yz = yc+(int)(0.5+dz*scy/2);
      line(bm, x3, y1, x4, y1, color);
      line(bm, x4, y1, x4, y2, color);
      line(bm, x3, y2, x3, y1, color);
      line(bm, x3, y2, x4, y2, color);
      line(bm, x3, yz, x4, yz, color);
      if (ampt != NULL)
	{
	  yp_1 = y1; xp_1 = y1 + (h_size*2*ampt[0])/maxt;
	  for (i = 1; i < length; i++)
	    {
	      yp = y1 + (i*(y2-y1))/length;
	      xp = x1 + (h_size*2*ampt[i])/maxt;
	      line(bm, xp_1, yp_1, xp, yp, color);
	      xp_1 = xp;
	      yp_1 = yp;
	    }
	}
      if (ampb != NULL)
	{
	  yp_1 = y1; xp_1 = y1 + (h_size*2*ampb[0])/maxb;
	  for (i = 1; i < length; i++)
	    {
	      yp = y1 + (i*(y2-y1))/length;
	      xp = x3 + (h_size*2*ampb[i])/maxb;
	      line(bm, xp_1, yp_1, xp, yp, color);
	      xp_1 = xp;
	      yp_1 = yp;
	    }
	}
      if (profy != NULL)
	{
	  yp_1 = yc + (l*2*profy[0])/maxprof; xp_1 = x1;
	  for (i = 1; i < width; i++)
	    {
	      yp = yc + (l*2*profy[i])/maxprof;
	      xp = x4 - (i*(x4-x1))/width;
	      line(bm, xp_1, yp_1, xp, yp, color);
	      xp_1 = xp;
	      yp_1 = yp;
	    }
	}


    }
  return 0;
}




int	draw_shear_rect_vga_screen_unit(int xc, int yc, int length, int width, int h_size, int shear_in_y, int dz, int color, BITMAP* bm, float scx, float scy)
{
  int l = length >> 1;
  int w = width >> 2;
  int y = bm->h - yc;
  int x1, x2, x3, x4, y1, y2, y3, y4, xz, yz;

  y = yc;

  if (shear_in_y == 0)
    {
      l = (int)(0.5+ scx * l);
      w = (int)(0.5+ scy * w);
      h_size = (int)(0.5+ scy * h_size);
      x1 =  xc - l;
      x2 =  xc + l;
      y1 =  y - w - h_size;
      y2 =  y - w + h_size;
      y3 =  y + w - h_size;
      y4 =  y + w + h_size;
      xz = xc-(int)(0.5+dz*scx/2);
      line(bm, x1, y1, x2, y1, color);
      line(bm, x2, y1, x2, y2, color);
      line(bm, x2, y2, x1, y2, color);
      line(bm, x1, y2, x1, y1, color);
      line(bm, xz, y1, xz, y2, color);
      line(bm, xc, y3, xc, y2, color);

      xz = xc+(int)(0.5+dz*scx/2);
      line(bm, x1, y3, x2, y3, color);
      line(bm, x2, y3, x2, y4, color);
      line(bm, x2, y4, x1, y4, color);
      line(bm, x1, y4, x1, y3, color);
      line(bm, xz, y3, xz, y4, color);
    }
  else
    {
      l = (int)(0.5+ scy * l);
      w = (int)(0.5+ scx * w);
      h_size = (int)(0.5+ scx * h_size);
      y1 =  y - l;
      y2 =  y + l;
      x1 =  xc - w - h_size;
      x2 =  xc - w + h_size;
      x3 =  xc + w - h_size;
      x4 =  xc + w + h_size;
      yz = yc-(int)(0.5+dz*scy/2);
      line(bm, x1, y1, x2, y1, color);
      line(bm, x2, y1, x2, y2, color);
      line(bm, x1, y2, x1, y1, color);
      line(bm, x2, y2, x1, y2, color);
      line(bm, x1, yz, x2, yz, color);
      line(bm, x3, yc, x2, yc, color);

      yz = yc+(int)(0.5+dz*scy/2);
      line(bm, x3, y1, x4, y1, color);
      line(bm, x4, y1, x4, y2, color);
      line(bm, x3, y2, x3, y1, color);
      line(bm, x3, y2, x4, y2, color);
      line(bm, x3, yz, x4, yz, color);
    }
  return 0;
}


int	draw_moving_shear_rect_vga_screen_unit_fitInter(int xc, int yc, int length, int width, int h_size, int dz, int color, BITMAP* bm, float scx, float scy)
{
  int l = length >> 1;
  int w = width >> 1;
  int y = bm->h - yc;
  int x1, x2, x3, x4, y1, y2, y3, y4, xz, yz, xp, yp, xp_1, yp_1;
  int i;
  float maxb, maxt, maxyt, maxyb;

  y = (int)(0.5+scy*yc);//yc;
  xc = (int)(0.5+scx*xc);
  dz = (dz > length/4) ? length/4 : dz;
  dz = (dz < -length/4) ? -length/4 : dz;
  l = (int)(0.5+ scx * l);
  w = (int)(0.5+ scy * w);
  h_size = (int)(0.5+ scy * h_size);
  xz = xc+(int)(0.5+dz*scx/2);
  x1 =  xz - l;
  x2 =  xz + l;
  y1 =  y - w - h_size;
  y2 =  y - w + h_size;
  y3 =  y + w - h_size;
  y4 =  y + w + h_size;

  line(bm, x1, y1, x2, y1, color);
  line(bm, x2, y1, x2, y2, color);
  line(bm, x2, y2, x1, y2, color);
  line(bm, x1, y2, x1, y1, color);
  line(bm, xz, y1, xz, y2, color);
  line(bm, xc, y3, xc, y2, color);

  xz = xc-(int)(0.5+dz*scx/2);
  x1 =  xz - l;
  x2 =  xz + l;

  line(bm, x1, y3, x2, y3, color);
  line(bm, x2, y3, x2, y4, color);
  line(bm, x2, y4, x1, y4, color);
  line(bm, x1, y4, x1, y3, color);
  line(bm, xz, y3, xz, y4, color);
  return 0;
}

int	draw_moving_shear_rect_vga_screen_unit_fitInter_w_profiles(int xc, int yc, int length, int width, int h_size, int dz, int color, BITMAP* bm, float scx, float scy, float *ampt, float *ampb, float *profyt, float *profyb, int lny)
{
  int l = length >> 1;
  int w = width >> 1;
  int y = bm->h - yc;
  int x1, x2, x3, x4, y1, y2, y3, y4, xz, yz, xp, yp, xp_1, yp_1;
  int i, w2;
  float maxb, maxt, maxyt, maxyb;

  y = (int)(0.5+scy*yc);//yc;
  xc = (int)(0.5+scx*xc);
  dz = (dz > length/4) ? length/4 : dz;
  dz = (dz < -length/4) ? -length/4 : dz;
  l = (int)(0.5+ scx * l);
  w = (int)(0.5+ scy * w);
  h_size = (int)(0.5+ scy * h_size);
  xz = xc+(int)(0.5+dz*scx/2);
  x1 =  xz - l;
  x2 =  xz + l;
  y1 =  y - w - h_size;
  y2 =  y - w + h_size;
  y3 =  y + w - h_size;
  y4 =  y + w + h_size;

  line(bm, x1, y1, x2, y1, color);
  line(bm, x2, y1, x2, y2, color);
  line(bm, x2, y2, x1, y2, color);
  line(bm, x1, y2, x1, y1, color);
  line(bm, xz, y1, xz, y2, color);
  line(bm, xc, y3, xc, y2, color);

  if (ampt != NULL)
    {
      for (i = 1, maxt = ampt[0]; i < length; i++)
	maxt = (ampt[i] > maxt) ? ampt[i] : maxt;
    }
  if (ampb != NULL)
    {
      for (i = 1, maxb = ampb[0]; i < length; i++)
	maxb = (ampb[i] > maxb) ? ampb[i] : maxb;
    }
  if ((ampt != NULL) && (ampb != NULL))
    {
      maxb = (maxt > maxb) ?  maxt : maxb;
      maxt = (maxb > maxt) ?  maxb : maxt;
    }
  if (profyt != NULL)
    {
      for (i = 1, maxyt = profyt[0]; i < lny; i++)
	maxyt = (profyt[i] > maxyt) ? profyt[i] : maxyt;
    }
  if (profyb != NULL)
    {
      for (i = 1, maxyb = profyb[0]; i < lny; i++)
	maxyb = (profyb[i] > maxyb) ? profyb[i] : maxyb;
    }

  if (ampt != NULL)
    {
      xp_1 = x1; yp_1 = y2 - (h_size*2*ampt[0])/maxt;
      for (i = 1; i < length; i++)
	{
	  xp = x1 + (i*(x2-x1))/length;
	  yp = y2 - (h_size*2*ampt[i])/maxt;
	  line(bm, xp_1, yp_1, xp, yp, color);
	  xp_1 = xp;
	  yp_1 = yp;
	}
    }
  if (profyt != NULL)
    {
      w2 = (int)(0.5+ scy * lny);
      xp_1 = x2 + (l*2*profyt[0])/maxyt;
      yp_1 = y -w + w2/2;
      for (i = 1; i < lny; i++)
	{
	  xp = x2 - (l*2*profyt[i])/maxyt;
	  yp = y - w + (w2/2) - (i*w2)/lny;
	  line(bm, xp_1, yp_1, xp, yp, color);
	  xp_1 = xp;
	  yp_1 = yp;
	}
    }

  xz = xc-(int)(0.5+dz*scx/2);
  x1 =  xz - l;
  x2 =  xz + l;

  line(bm, x1, y3, x2, y3, color);
  line(bm, x2, y3, x2, y4, color);
  line(bm, x2, y4, x1, y4, color);
  line(bm, x1, y4, x1, y3, color);
  line(bm, xz, y3, xz, y4, color);

  if (ampb != NULL)
    {
      xp_1 = x1; yp_1 = y4 - (h_size*2*ampb[0])/maxb;
      for (i = 1; i < length; i++)
	{
	  xp = x1 + (i*(x2-x1))/length;
	  yp = y4 - (h_size*2*ampb[i])/maxb;
	  line(bm, xp_1, yp_1, xp, yp, color);
	  xp_1 = xp;
	  yp_1 = yp;
	}
    }
  if (profyb != NULL)
    {
      w2 = (int)(0.5+ scy * lny);
      xp_1 = x1 + (l*2*profyb[0])/maxyb;
      yp_1 = y + w + w2/2;
      for (i = 1; i < lny; i++)
	{
	  xp = x1 + (l*2*profyb[i])/maxyb;
	  yp = y + w + (w2/2) - (i*w2)/lny;
	  line(bm, xp_1, yp_1, xp, yp, color);
	  xp_1 = xp;
	  yp_1 = yp;
	}
    }
  return 0;
}

int place_tracking_shear_parren_with_mouse_movie(int *xc, int *yc, int cl, int cw, int h_size, int shear_in_y, int dz, int color)
{
  int  xm, ym;
  int ix, iy;
  float scx, scy;
  imreg *imr;

  imr = find_imr_in_current_dialog(NULL);
  if (imr == NULL || imr->one_i == NULL)	return 1;
  ix = imr->one_i->im.nxe - imr->one_i->im.nxs;
  iy = imr->one_i->im.nye - imr->one_i->im.nys;
  scx = (float)imr->s_nx;
  scx = (ix > 0) ? scx/ix : 1;
  scy = (float)imr->s_ny;
  scy = (iy > 0) ? scy/iy : 1;

  xm = mouse_x = x_imdata_2_imr(imr, *xc);
  ym = mouse_y = y_imdata_2_imr(imr, *yc);
  for(; !key[KEY_ENTER]; )
    {
      if (xm != mouse_x || ym != mouse_y)
	{
	  xm = mouse_x;
	  ym = mouse_y;
	  refresh_image(imr,UNCHANGED);
	  draw_shear_rect_vga_screen_unit(xm, ym, cl, cw, h_size, shear_in_y, dz, color, screen, scx, scy);
	}
    }
  *xc = x_imr_2_imdata(imr, xm);
  *yc = y_imr_2_imdata(imr, ym);
  return 0;
}





int refresh_screen_image_and_moving_shear_rect_w_profiles(imreg *imrs, O_i *movie, DIALOG *d, int xc, int yc, int dz, int cl, int cw, int h_size, int color, float *ampt, float *ampb,  float *profyt, float *profyb, int lny)
{
  BITMAP *imb;
  int ix, iy;
  float scx, scy;
  int xi, yi;
  static int debug = 0;

  if (movie == NULL || imrs == NULL || d == NULL) return 1;
  movie->need_to_refresh |= BITMAP_NEED_REFRESH;
  display_image_stuff_16M(imrs,d);
  ix = movie->im.nxe - movie->im.nxs;
  iy = movie->im.nye - movie->im.nys;
  scx = (float)imrs->s_nx;
  scx = (ix > 0) ? scx/ix : 1;
  scy = (float)imrs->s_ny;
  scy = (iy > 0) ? scy/iy : 1;
  if ((movie->bmp.to == IS_BITMAP) &&  (movie->bmp.stuff != NULL))
    {
      imb = (BITMAP*)movie->bmp.stuff;
      acquire_bitmap(screen);
      xi = x_imdata_2_imr(imrs, xc) - imrs->x_off;
      yi = imrs->y_off - y_imdata_2_imr(imrs, yc);//imb->h - yc;

      /*
	ys = imr->y_off - ((yc - imr->one_i->im.nys) * imr->s_ny)/ix;


	yd  = ((float)imr->one_i->im.nys + ((float)((imr->y_off - y+d->y)*ix)/imr->s_ny));
      */

      draw_moving_shear_rect_vga_screen_unit_fitInter_w_profiles(xc, yc, cl, cw, h_size, dz, color, imb, scx, scy, ampt,
							ampb, profyt,  profyb, lny);

      blit(imb,screen,0,0,imrs->x_off + d->x, imrs->y_off - imb->h + d->y, imb->w, imb->h);
      release_bitmap(screen);
      if (debug != WIN_CANCEL) debug = win_printf("xc = %d xi = %d\nyc = %d yi = %d\n"
						  "dz = %d",xc,xi,yc,yi,dz);
    }
  return 0;
}


int refresh_screen_image_and_shear_rect_w_profiles(imreg *imrs, O_i *movie, DIALOG *d, int xc, int yc, int dz, int cl, int cw, int h_size, int shear_in_y, int color, float *ampt, float maxt, float *ampb, float maxb, float *profy, float maxprof, int lny)
{
  BITMAP *imb;
  int ix, iy;
  float scx, scy;
  int xi, yi;
  static int debug = 0;

  if (movie == NULL || imrs == NULL || d == NULL) return 1;
  movie->need_to_refresh |= BITMAP_NEED_REFRESH;
  display_image_stuff_16M(imrs,d);
  ix = movie->im.nxe - movie->im.nxs;
  iy = movie->im.nye - movie->im.nys;
  scx = (float)imrs->s_nx;
  scx = (ix > 0) ? scx/ix : 1;
  scy = (float)imrs->s_ny;
  scy = (iy > 0) ? scy/iy : 1;
  if ((movie->bmp.to == IS_BITMAP) &&  (movie->bmp.stuff != NULL))
    {
      imb = (BITMAP*)movie->bmp.stuff;
      acquire_bitmap(screen);
      xi = x_imdata_2_imr(imrs, xc) - imrs->x_off;
      yi = imrs->y_off - y_imdata_2_imr(imrs, yc);//imb->h - yc;

      /*
	ys = imr->y_off - ((yc - imr->one_i->im.nys) * imr->s_ny)/ix;


	yd  = ((float)imr->one_i->im.nys + ((float)((imr->y_off - y+d->y)*ix)/imr->s_ny));
      */

      draw_shear_rect_vga_screen_unit_w_profiles(xi, yi, cl, cw, h_size, shear_in_y, dz, color, imb, scx, scy, ampt, maxt, ampb, maxb, profy, maxprof,lny);
      blit(imb,screen,0,0,imrs->x_off + d->x, imrs->y_off -
	   imb->h + d->y, imb->w, imb->h);
      release_bitmap(screen);
      if (debug != WIN_CANCEL) debug = win_printf("xc = %d xi = %d\nyc = %d yi = %d\n",xc,xi,yc,yi);
    }
  return 0;
}


int refresh_screen_image_and_shear_rect(imreg *imrs, O_i *movie, DIALOG *d, int xc, int yc, int dz, int cl, int cw, int h_size, int shear_in_y, int color)
{
  BITMAP *imb;
  int ix, iy;
  float scx, scy;
  int xi, yi;
  static int debug = 0;

  if (movie == NULL || imrs == NULL || d == NULL) return 1;
  movie->need_to_refresh |= BITMAP_NEED_REFRESH;
  display_image_stuff_16M(imrs,d);
  ix = movie->im.nxe - movie->im.nxs;
  iy = movie->im.nye - movie->im.nys;
  scx = (float)imrs->s_nx;
  scx = (ix > 0) ? scx/ix : 1;
  scy = (float)imrs->s_ny;
  scy = (iy > 0) ? scy/iy : 1;
  if ((movie->bmp.to == IS_BITMAP) &&  (movie->bmp.stuff != NULL))
    {
      imb = (BITMAP*)movie->bmp.stuff;
      acquire_bitmap(screen);
      xi = x_imdata_2_imr(imrs, xc) - imrs->x_off;
      yi = imrs->y_off - y_imdata_2_imr(imrs, yc);//imb->h - yc;

      /*
	ys = imr->y_off - ((yc - imr->one_i->im.nys) * imr->s_ny)/ix;


	yd  = ((float)imr->one_i->im.nys + ((float)((imr->y_off - y+d->y)*ix)/imr->s_ny));
      */

      draw_shear_rect_vga_screen_unit(xi, yi, cl, cw, h_size, shear_in_y, dz, color, imb, scx, scy);
      blit(imb,screen,0,0,imrs->x_off + d->x, imrs->y_off -
	   imb->h + d->y, imb->w, imb->h);
      release_bitmap(screen);
      if (debug != WIN_CANCEL) debug = win_printf("xc = %d xi = %d\nyc = %d yi = %d\n",xc,xi,yc,yi);
    }
  return 0;
}



int	grab_mean_position_from_shear_profiles_from_movie_2(void)
{
  int i, j, k, color, cl, cw;
  //int snx = 0, sny = 0;
  int  nfi, ROI_set, lx0, lnx, ly0, lny, size, imax, lx0s, ly0s;
  O_i   *ois;
  O_p *op = NULL;
  d_s *dstmpt = NULL, *dstmpb = NULL, *dstmp2 = NULL, *dsx, *dsy, *dsz, *dsphi, *dsphi2;
  imreg *imr;
  float zero = 0, max = 0, noise, dx = 0, dy = 0, dz = 0;
  unsigned long dt;
  static int x0 = 0, nx = 64, y0 = 0, ny = 128, mouse_xy = 0, track_xy = 0;
  static int start = 0, end = 1, sw = 0;
  static float black_l = 190;
  static int lp_filter = 4, n_phi = 3, lp2_filter = 4, n_phi2 = 3, h_size = 5, timing = 0;
  filter *fillp = NULL, *fillp2 = NULL;
  fft_plan *fp = NULL, *fp2 = NULL;
  DIALOG *dia = NULL;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) < 1)	return OFF;
  nfi = abs(ois->im.n_f);
  nfi = (nfi == 0) ? 1 : nfi;
  if(updating_menu_state != 0)
    {
      if (nfi <= 1) 	 active_menu->flags |=  D_DISABLED;
      else               active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }

  end = nfi;
  ROI_set = (ois->im.nxs > 0 || ois->im.nys > 0 || ois->im.nxe < ois->im.nx || ois->im.nye < ois->im.ny) ? 1 : 0;

  if (ROI_set)
    {
      lx0 = ois->im.nxs; lnx = ois->im.nxe - ois->im.nxs;
      ly0 = ois->im.nys; lny = ois->im.nye - ois->im.nys;
    }
  else
    {
      lx0 = x0; lnx = nx; ly0 = y0; lny = ny;
    }
  color = makecol(255,64,64);
  i = win_scanf("Measure mean position of shear profiles of offset interferometry images\n"
		"Specify the center and size of the sub image in the movie:\n"
		"Xc %6d Yc %6d or specify xy by mouse %b\n"
		"Size in X %6d Size in Y %6d\n"
		"Normal%R or swap XY%r half size of shear avg %3d\n"
		"In the shear direction Low-pass filter freq %6d \n"
		"Phase zero will be search of 2n+1 points n=%3d\n"
		"Perpendicular to shear direction Low-pass filter freq %6d \n"
		"Phase zero will be search of 2n+1 points n=%3d\n"
		"Grab profiles starting at frame %6d ending at %6d\n"
		"Black level %4f track in XY %b timing %b\n"
		,&lx0,&ly0,&mouse_xy,&lnx,&lny,&sw,&h_size,&lp_filter
		,&n_phi,&lp2_filter,&n_phi2,&start,&end,&black_l,&track_xy,&timing);
  if (i == WIN_CANCEL) return  D_O_K;

  cl = (sw)?lny:lnx; cw = (sw)?lnx:lny;
  if (mouse_xy)
    place_tracking_shear_parren_with_mouse_movie(&lx0, &ly0, (sw)?lny:lnx, (sw)?lnx:lny, h_size, sw, 0, color);

  //i = win_printf("Mouse select x %d y %d",lx0,ly0);
  //if (i == WIN_CANCEL) return  D_O_K;

  size = end - start;
  size = (size > ois->im.ny) ? ois->im.ny : size;
  op = create_and_attach_op_to_oi(ois, size, size, 0, 0);
  if (op == NULL) return (win_printf_OK("cant create plot!"));
  dsx = op->dat[0];
  dsy = create_and_attach_one_ds(op, size, size, 0);
  if (dsy == NULL) return (win_printf_OK("cant create plot!"));
  dsz = create_and_attach_one_ds(op, size, size, 0);
  if (dsz == NULL) return (win_printf_OK("cant create plot!"));
  set_plot_title(op,"(%d to %d)",start, end);
  set_ds_treatement(dsx,"Track mean position at X0 = %d size %d Y0 %d size %d, swap %d, avg_size %d,"
		    " lp_shear_filter %d, n_phi %d, lp2_filter %d n_phi2 %d, black %g"
		    ,lx0,lnx,ly0,lny,sw,h_size,lp_filter,n_phi,lp2_filter,n_phi2,black_l);
  set_ds_treatement(dsy,"Track mean position at X0 = %d size %d Y0 %d size %d, swap %d, avg_size %d,"
		    " lp_shear_filter %d, n_phi %d, lp2_filter %d n_phi2 %d, black %g"
		    ,lx0,lnx,ly0,lny,sw,h_size,lp_filter,n_phi,lp2_filter,n_phi2,black_l);
  set_ds_treatement(dsz,"Track mean position at X0 = %d size %d Y0 %d size %d, swap %d, avg_size %d,"
		    " lp_shear_filter %d, n_phi %d, lp2_filter %d n_phi2 %d, black %g"
		    ,lx0,lnx,ly0,lny,sw,h_size,lp_filter,n_phi,lp2_filter,n_phi2,black_l);
  inherit_from_im_to_ds(dsx, ois);
  inherit_from_im_to_ds(dsz, ois);
  uns_oi_2_op(ois, (sw == 0) ? IS_X_UNIT_SET : IS_Y_UNIT_SET, op, IS_X_UNIT_SET);
  uns_oi_2_op(ois, IS_T_UNIT_SET, op, IS_Y_UNIT_SET);
  op->filename = strdup(ois->filename);
  dsphi = build_data_set(2*n_phi+1, 2*n_phi+1);
  dsphi2 = build_data_set(2*n_phi2+1, 2*n_phi2+1);
  dia = find_dialog_associated_to_imr(imr, NULL);

  fp = fftbt_init(fp, (sw == 0) ? lnx : lny);
  fillp = filter_init_bt(fillp, (sw == 0) ? lnx : lny);
  fp2 = fftbt_init(fp2, (sw == 1) ? lnx : lny);
  fillp2 = filter_init_bt(fillp2, (sw == 1) ? lnx : lny);

  if (sw == 0)
    {
      if (dstmp2 == NULL) dstmp2 = build_data_set(lny,lny);
      if (dstmp2 == NULL) return win_printf_OK("cannot create ds !");
      if (dstmpt == NULL) dstmpt = build_data_set(lnx,lnx);
      if (dstmpb == NULL) dstmpb = build_data_set(lnx,lnx);
      if (dstmpt == NULL || dstmpb == NULL) return win_printf_OK("cannot create ds !");
    }
  else
    {
      if (dstmp2 == NULL) dstmp2 = build_data_set(lnx,lnx);
      if (dstmp2 == NULL) return win_printf_OK("cannot create ds !");
      if (dstmpt == NULL) dstmpt = build_data_set(lny,lny);
      if (dstmpb == NULL) dstmpb = build_data_set(lny,lny);
      if (dstmpt == NULL || dstmpb == NULL) return win_printf_OK("cannot create ds !");
    }
  lx0s = lx0; ly0s = ly0;
  dt = my_uclock();
  for (i = start; i < end; i++)
    {
      switch_frame(ois,i);
      dsx->yd[i-start] = dsy->yd[i-start] = dsz->yd[i-start] = i;
      if (sw == 0)   // we track in direction perpendicular to shear
	{
	  for (k=0 ; k < lny ; k++) dstmp2->yd[k] = 0;
	  for (j = 0 ; j< lnx ; j++)
	    {
	      extract_raw_partial_row(ois, lx0+j-lnx/2, dstmp2->xd,ly0-lny/2,lny);
	      for (k=0 ; k < lny ; k++)
		  dstmp2->yd[k] += dstmp2->xd[k] - black_l;
	    }
	  correlate_1d_sig_and_invert_lp_and_complexify(dstmp2->yd, lny, 0, dstmp2->xd,
							dstmp2->yd, lp2_filter, 1, fp2, fillp2, &noise);
	  for (j = lny/4, imax = -1; j < 3*lny/4; j++)
	    max = ((imax < 0) || (dstmp2->xd[j] > max)) ? dstmp2->xd[imax = j] : max;
	  for (j = 0; j <= 2*n_phi2; j++)
	    {
	      k = imax - n_phi2 + j;
	      k += (k < 0) ? lny : 0;
	      k = (k < lny) ? k : k - lny;
	      dsphi2->yd[j] = dstmp2->yd[k];
	      dsphi2->xd[j] = j - n_phi;
	    }
	  find_phase_zero_with_poly(dsphi2, &zero);
	  dy = ((zero + imax) - lny/2)/2;
	  if (track_xy) dsy->xd[i-start] = ly0 + dy;
	  else dsy->xd[i-start] = dy;
	}
      else
	{
	  for (k=0 ; k < lnx ; k++) dstmp2->yd[k] = 0;
	  for (j = 0 ; j< lny ; j++)
	    {
	      extract_raw_partial_line(ois, ly0+j-lny/2, dstmp2->xd, lx0-lnx/2, lnx);
	      for (k=0 ; k < lnx ; k++)
		  dstmp2->yd[k] += dstmp2->xd[k] - black_l;
	    }
	  correlate_1d_sig_and_invert_lp_and_complexify(dstmp2->yd, lnx, 0, dstmp2->xd,
							dstmp2->yd, lp2_filter, 1, fp2, fillp2, &noise);
	  for (j = lnx/4, imax = -1; j < 3*lnx/4; j++)
	    max = ((imax < 0) || (dstmp2->xd[j] > max)) ? dstmp2->xd[imax = j] : max;
	  for (j = 0; j <= 2*n_phi2; j++)
	    {
	      k = imax - n_phi2 + j;
	      k += (k < 0) ? lnx : 0;
	      k = (k < lnx) ? k : k - lnx;
	      dsphi2->yd[j] = dstmp2->yd[k];
	      dsphi2->xd[j] = j - n_phi;
	    }
	  find_phase_zero_with_poly(dsphi2, &zero);
	  dx = ((zero + imax) - lnx/2)/2;
	  if (track_xy) dsx->xd[i-start] = lx0 + dx;
	  else dsx->xd[i-start] = dx;
	}
      if (sw == 0)   // we track in shear direction
	{
	  for (k=0 ; k < lnx ; k++)	dstmpt->yd[k] = dstmpb->yd[k] = 0;
	  for (j = lny/4 - h_size; j <= lny/4 + h_size ; j++)
	    {
	      if (j < 0 || j >= lny/2) continue;
	      extract_raw_partial_line(ois, ly0-j-1, dstmpt->xd,lx0-lnx/2,lnx);
	      extract_raw_partial_line(ois, ly0+j, dstmpb->xd,lx0-lnx/2,lnx);
	      for (k=0 ; k < lnx ; k++)
		{
		  dstmpt->yd[k] += dstmpt->xd[k] - black_l;
		  dstmpb->yd[k] += dstmpb->xd[k] - black_l;
		}
	    }
	  complexify_profile(dstmpt, lp_filter, fp, fillp);
	  for (j = imax = 0; j < lnx; j++)
	    max = ((j == 0) || (dstmpt->yd[j] > max)) ? dstmpt->yd[imax = j] : max;
	  for (j = 0; j <= 2*n_phi; j++)
	    {
	      k = imax - n_phi + j;
	      k += (k < 0) ? lnx : 0;
	      k = (k < lnx) ? k : k - lnx;
	      dsphi->yd[j] = dstmpt->xd[k];
	      dsphi->xd[j] = j - n_phi;
	    }
	  find_phase_zero_with_poly(dsphi, &zero);
	  dx = (zero + imax);
	  dz = (zero + imax);
	  complexify_profile(dstmpb, lp_filter, fp, fillp);
	  for (j = imax = 0; j < lnx; j++)
	    max = ((j == 0) || (dstmpb->yd[j] > max)) ? dstmpb->yd[imax = j] : max;
	  for (j = 0; j <= 2*n_phi; j++)
	    {
	      k = imax - n_phi + j;
	      k += (k < 0) ? lnx : 0;
	      k = (k < lnx) ? k : k - lnx;
	      dsphi->yd[j] = dstmpb->xd[k];
	      dsphi->xd[j] = j - n_phi;
	    }
	  find_phase_zero_with_poly(dsphi, &zero);
	  dx += (zero + imax) - lnx;
	  dx /= 2;
	  if (track_xy) dsx->xd[i-start] = dx + lx0;
	  else dsx->xd[i-start] = dx;
	  dz -= (zero + imax);
	  dsz->xd[i-start] = dz;
	}
      else
	{
	  for (k=0 ; k < lny ; k++)	dstmpt->yd[k] = dstmpb->yd[k] = 0;
	  for (j = lnx/4 - h_size; j <= lnx/4 + h_size ; j++)
	    {
	      if (j < 0 || j >= lnx/2) continue;
	      extract_raw_partial_row(ois, lx0-j-1, dstmpt->xd,ly0-lny/2,lny);
	      extract_raw_partial_row(ois, lx0+j, dstmpb->xd,ly0-lny/2,lny);
	      for (k=0 ; k < lny ; k++)
		{
		  dstmpt->yd[k] += dstmpt->xd[k] - black_l;
		  dstmpb->yd[k] += dstmpb->xd[k] - black_l;
		}
	    }
	  complexify_profile(dstmpt, lp_filter, fp, fillp);
	  for (j = imax = 0; j < lny; j++)
	    max = ((j == 0) || (dstmpt->yd[j] > max)) ? dstmpt->yd[imax = j] : max;
	  for (j = 0; j <= 2*n_phi; j++)
	    {
	      k = imax - n_phi + j;
	      k += (k < 0) ? lny : 0;
	      k = (k < lny) ? k : k - lny;
	      dsphi->yd[j] = dstmpt->xd[k];
	      dsphi->xd[j] = j - n_phi;
	    }
	  find_phase_zero_with_poly(dsphi, &zero);
	  dy = (zero + imax);
	  dz = (zero + imax);
	  complexify_profile(dstmpb, lp_filter, fp, fillp);
	  for (j = imax = 0; j < lny; j++)
	    max = ((j == 0) || (dstmpb->yd[j] > max)) ? dstmpb->yd[imax = j] : max;
	  for (j = 0; j <= 2*n_phi; j++)
	    {
	      k = imax - n_phi + j;
	      k += (k < 0) ? lny : 0;
	      k = (k < lny) ? k : k - lny;
	      dsphi->yd[j] = dstmpb->xd[k];
	      dsphi->xd[j] = j - n_phi;
	    }
	  find_phase_zero_with_poly(dsphi, &zero);
	  dy += (zero + imax) - lny;
	  dy /= 2;
	  if (track_xy) dsy->xd[i-start] = dy + ly0;
	  else dsy->xd[i-start] = dy;
	  dz -= (zero + imax);
	  dsz->xd[i-start] = dz;
	}
      if (track_xy)
	{
	  lx0 += (int)(dx + 0.5);
	  ly0 += (int)(dy + 0.5);
	  refresh_screen_image_and_shear_rect(imr, ois, dia, lx0, ly0, (int)(0.5+dz), cl, cw, h_size, sw, color);
	}
    }
  lx0 = lx0s; ly0 = ly0s;
  dt = my_uclock() - dt;
  if (timing) win_printf("took %g ms",1000*(double)dt/get_my_uclocks_per_sec());
  if (ROI_set == 0)
    {
      x0 = lx0; nx = lnx; y0 = ly0; ny = lny;
    }
  free_data_set(dstmpt);
  free_data_set(dstmpb);
  free_data_set(dstmp2);
  free_data_set(dsphi);
  free_data_set(dsphi2);
  free_fft_plan_bt(fp);
  free_filter_bt(fillp);
  free_fft_plan_bt(fp2);
  free_filter_bt(fillp2);

  return refresh_image(imr,UNCHANGED);
}




int	grab_position_from_shear_profiles_from_movie_2(void)
{
  int i, j, k, cl;//, cw;
  //int snx = 0, sny = 0;
  int  nfi, ROI_set, lx0, lnx, ly0, lny, iph0, maxi = 0, size, imax = 0 ,color, lx0s, ly0s;
  O_i *ois;
  O_p *op = NULL;
  d_s *dstmpt = NULL, *dstmpb = NULL, *dstmp2 = NULL, *dsx, *dsy, *dsz, *dsphi, *dsphi2;
  imreg *imr;
  float zero = 0, maxa2 = 0, noise, max, dx = 0, dy = 0, dz = 0;
  unsigned long dt;
  static int x0 = 0, nx = 64, y0 = 0, ny = 128, dpx = 32, mouse_xy = 0, track_xy = 0;
  static int start = 0, end = 1, sw = 0, timing = 0;
  static float black_l = 190;
  static int bp_filter = 7, bp_width = 3, lp2_filter = 3, n_phi = 3, n_phi2 = 3, h_size = 5;
  filter *filbp = NULL, *fillp2 = NULL;
  fft_plan *fp = NULL, *fp2 = NULL;
  DIALOG *dia = NULL;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) < 1)	return OFF;
  nfi = abs(ois->im.n_f);
  nfi = (nfi == 0) ? 1 : nfi;
  if(updating_menu_state != 0)
    {
      if (nfi <= 1) 	 active_menu->flags |=  D_DISABLED;
      else               active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }

  end = nfi;
  ROI_set = (ois->im.nxs > 0 || ois->im.nys > 0 || ois->im.nxe < ois->im.nx || ois->im.nye < ois->im.ny) ? 1 : 0;

  if (ROI_set)
    {
      lx0 = ois->im.nxs; lnx = ois->im.nxe - ois->im.nxs;
      ly0 = ois->im.nys; lny = ois->im.nye - ois->im.nys;
    }
  else
    {
      lx0 = x0; lnx = nx; ly0 = y0; lny = ny;
    }
  color = makecol(255,64,64);
  i = win_scanf("Construction of shear profiles of offset interferometry images\n"
		"Specify the center and size of the sub image in the movie:\n"
		"Xc %6d  Yc %6d or select by mouse %b\n"
		"Size in X %6d Distance between profiles %6d Size in Y %6d\n"
		"Normal%R or swap XY%r half size of shear avg %3d\n"
		"In the shear direction Band-pass filter freq %6d width %6d \n"
		"Phase zero will be search of 2n+1 points n=%3d\n"
		"Perpendicular to shear direction Low-pass filter freq %6d \n"
		"Phase zero will be search of 2n+1 points n=%3d\n"
		"Grab profiles starting at frame %6d ending at %6d\n"
		"Black level %4f track XY %b timing %b\n"
		,&lx0,&ly0,&mouse_xy,&lnx,&dpx,&lny,&sw,&h_size,&bp_filter,&bp_width
		,&n_phi,&lp2_filter,&n_phi2,&start,&end,&black_l,&track_xy,&timing);
  if (i == WIN_CANCEL) return  D_O_K;

  cl = (sw)?lny:lnx;
  //cw = (sw)?lnx:lny;
  if (mouse_xy)
    place_tracking_shear_parren_with_mouse_movie(&lx0, &ly0, cl, 2*dpx, h_size, sw, 0, color);
  //place_tracking_shear_parren_with_mouse_movie(&lx0, &ly0, cl, cw, h_size, sw, 0, color);


  size = end - start;
  size = (size > nfi) ? nfi : size; // ois->im.ny
  op = create_and_attach_op_to_oi(ois, size, size, 0, 0);
  if (op == NULL) return (win_printf_OK("cant create plot!"));
  dsx = op->dat[0];
  dsy = create_and_attach_one_ds(op, size, size, 0);
  if (dsy == NULL) return (win_printf_OK("cant create plot!"));
  dsz = create_and_attach_one_ds(op, size, size, 0);
  if (dsz == NULL) return (win_printf_OK("cant create plot!"));
  set_plot_title(op,"(%d to %d)",start, end);
  set_ds_treatement(dsx,"Track mean position at X0 = %d size %d Y0 %d size %d, swap %d, avg_size %d,"
		    " Bp_shear_filter %d, Bp_shear_width %d, n_phi %d, lp2_filter %d n_phi2 %d, black %g"
		    ,lx0,lnx,ly0,lny,sw,h_size,bp_filter,bp_width,n_phi,lp2_filter,n_phi2,black_l);
  set_ds_treatement(dsy,"Track mean position at X0 = %d size %d Y0 %d size %d, swap %d, avg_size %d,"
		    " Bp_shear_filter %d, Bp_shear_width %d, n_phi %d, lp2_filter %d n_phi2 %d, black %g"
		    ,lx0,lnx,ly0,lny,sw,h_size,bp_filter,bp_width,n_phi,lp2_filter,n_phi2,black_l);
  set_ds_treatement(dsz,"Track mean position at X0 = %d size %d Y0 %d size %d, swap %d, avg_size %d,"
		    " Bp_shear_filter %d, Bp_shear_width %d, n_phi %d, lp2_filter %d n_phi2 %d, black %g"
		    ,lx0,lnx,ly0,lny,sw,h_size,bp_filter,bp_width,n_phi,lp2_filter,n_phi2,black_l);
  inherit_from_im_to_ds(dsx, ois);
  inherit_from_im_to_ds(dsy, ois);
  inherit_from_im_to_ds(dsz, ois);
  uns_oi_2_op(ois, (sw == 0) ? IS_X_UNIT_SET : IS_Y_UNIT_SET, op, IS_X_UNIT_SET);
  uns_oi_2_op(ois, IS_T_UNIT_SET, op, IS_Y_UNIT_SET);
  op->filename = strdup(ois->filename);
  dsphi = build_data_set(2*n_phi+1, 2*n_phi+1);
  dsphi2 = build_data_set(2*n_phi2+1, 2*n_phi2+1);

  fp = fftbt_init(fp, (sw == 0) ? lnx : lny);
  filbp = filter_init_bt(filbp, (sw == 0) ? lnx : lny);
  fp2 = fftbt_init(fp2, (sw == 1) ? lnx : lny);
  fillp2 = filter_init_bt(fillp2, (sw == 1) ? lnx : lny);
  dia = find_dialog_associated_to_imr(imr, NULL);

  if (sw == 0)
    {
      if (dstmp2 == NULL) dstmp2 = build_data_set(lny,lny);
      if (dstmp2 == NULL) return win_printf_OK("cannot create ds !");
      if (dstmpt == NULL) dstmpt = build_data_set(lnx,lnx);
      if (dstmpb == NULL) dstmpb = build_data_set(lnx,lnx);
      if (dstmpt == NULL || dstmpb == NULL) return win_printf_OK("cannot create ds !");
    }
  else
    {
      if (dstmp2 == NULL) dstmp2 = build_data_set(lnx,lnx);
      if (dstmp2 == NULL) return win_printf_OK("cannot create ds !");
      if (dstmpt == NULL) dstmpt = build_data_set(lny,lny);
      if (dstmpb == NULL) dstmpb = build_data_set(lny,lny);
      if (dstmpt == NULL || dstmpb == NULL) return win_printf_OK("cannot create ds !");
    }
  lx0s = lx0; ly0s = ly0;
  dt = my_uclock();
  for (i = start; i < end; i++)
    {
      switch_frame(ois,i);
      dsx->yd[i-start] =  dsy->yd[i-start] = dsz->yd[i-start] = i;
      if (sw == 0) // we track in direction perpendicular to shear
	{
	  for (k=0 ; k < lny ; k++) dstmp2->yd[k] = 0;
	  for (j = 0 ; j< lnx ; j++)
	    {
	      extract_raw_partial_row(ois, lx0+j-lnx/2, dstmp2->xd,ly0-lny/2,lny);
	      for (k=0 ; k < lny ; k++)
		  dstmp2->yd[k] += dstmp2->xd[k] - black_l;
	    }
	  correlate_1d_sig_and_invert_lp_and_complexify(dstmp2->yd, lny, 0, dstmp2->xd, dstmp2->yd,
							lp2_filter, 1, fp2, fillp2, &noise);
	  for (j = lny/4, imax = -1; j < 3*lny/4; j++)
	    max = ((imax < 0) || (dstmp2->xd[j] > max)) ? dstmp2->xd[imax = j] : max;
	  for (j = 0; j <= 2*n_phi2; j++)
	    {
	      k = imax - n_phi2 + j;
	      k += (k < 0) ? lny : 0;
	      k = (k < lny) ? k : k - lny;
	      dsphi2->yd[j] = dstmp2->yd[k];
	      dsphi2->xd[j] = j - n_phi;
	    }
	  find_phase_zero_with_poly(dsphi2, &zero);
	  dy = ((zero + imax) - lny/2)/2;
	  if (track_xy) dsy->xd[i-start] = ly0 + dy;
	  else dsy->xd[i-start] = dy;
	}
      else
	{
	  for (k=0 ; k < lnx ; k++) dstmp2->yd[k] = 0;
	  for (j = 0 ; j< lny ; j++)
	    {
	      extract_raw_partial_line(ois, ly0+j-lny/2, dstmp2->xd, lx0-lnx/2, lnx);
	      for (k=0 ; k < lnx ; k++)
		  dstmp2->yd[k] += dstmp2->xd[k] - black_l;
	    }
	  correlate_1d_sig_and_invert_lp_and_complexify(dstmp2->yd, lnx, 0, dstmp2->xd, dstmp2->yd,
							lp2_filter, 1, fp2, fillp2, &noise);
	  for (j = lnx/4, imax = -1; j < 3*lnx/4; j++)
	    max = ((imax < 1) || (dstmp2->xd[j] > max)) ? dstmp2->xd[imax = j] : max;
	  for (j = 0; j <= 2*n_phi2; j++)
	    {
	      k = imax - n_phi2 + j;
	      k += (k < 0) ? lnx : 0;
	      k = (k < lnx) ? k : k - lnx;
	      dsphi2->yd[j] = dstmp2->yd[k];
	      dsphi2->xd[j] = j - n_phi;
	    }
	  find_phase_zero_with_poly(dsphi2, &zero);
	  dx = ((zero + imax) - lnx/2)/2;
	  if (track_xy) dsx->xd[i-start] = lx0 + dx;
	  else dsx->xd[i-start] = dx;
	}
      if (sw == 0) // we track in shear direction
	{
	  for (k=0 ; k < lnx ; k++)	dstmpt->xd[k] = dstmpb->xd[k] = 0;
	  //for (j = lny/2 - dpx/2 - h_size; j <= lny/2 - dpx/2 + h_size ; j++)
	  for (j = dpx/2 - h_size; j <= dpx/2 + h_size ; j++)
	    {
	      if (j < 0 || j >= lny/2) continue;
	      extract_raw_partial_line(ois, ly0-j-1, dstmpt->yd,lx0-lnx/2,lnx);
	      extract_raw_partial_line(ois, ly0+j, dstmpb->yd,lx0-lnx/2,lnx);
	      for (k=0 ; k < lnx ; k++)
		{
		  dstmpt->xd[k] += dstmpt->yd[k] - black_l;
		  dstmpb->xd[k] += dstmpb->yd[k] - black_l;
		}
	    }
	  hilbert_1d_sig(dstmpt->xd, lnx, 0, dstmpt->yd, bp_filter, bp_width, fp, filbp);
	  max_of_amplitude(dstmpt->yd, lnx, &maxi, &maxa2);
	  grab_phase_around_max_of_amplitude(dstmpt->yd, lnx, maxi, n_phi, dsphi, &iph0);
	  find_phase_zero_with_poly(dsphi, &zero);
	  dx = (zero + iph0);
	  dz = 2*(zero + iph0);
	  hilbert_1d_sig(dstmpb->xd, lnx, 0, dstmpb->yd, bp_filter, bp_width, fp, filbp);
	  max_of_amplitude(dstmpb->yd, lnx, &maxi, &maxa2);
	  grab_phase_around_max_of_amplitude(dstmpb->yd, lnx, maxi, n_phi, dsphi, &iph0);
	  find_phase_zero_with_poly(dsphi, &zero);
	  dx += (zero + iph0) - lnx/2;
	  if (track_xy) dsx->xd[i-start] = dx + lx0;
	  else dsx->xd[i-start] = dx;
	  dz -= 2*(zero + iph0);
	  dsz->xd[i-start] = dz;
	}
      else
	{
	  for (k=0 ; k < lny ; k++)	dstmpt->xd[k] = dstmpb->xd[k] = 0;
	  //for (j = lnx/2 - dpx/2 - h_size; j <= lnx/2 - dpx/2 + h_size ; j++)
	  for (j = dpx/2 - h_size; j <= dpx/2 + h_size ; j++)
	    {
	      if (j < 0 || j >= lnx/2) continue;
	      extract_raw_partial_row(ois, lx0-j-1, dstmpt->yd,ly0-lny/2,lny);
	      extract_raw_partial_row(ois, lx0+j, dstmpb->yd,ly0-lny/2,lny);
	      for (k=0 ; k < lny ; k++)
		{
		  dstmpt->xd[k] += dstmpt->yd[k] - black_l;
		  dstmpb->xd[k] += dstmpb->yd[k] - black_l;
		}
	    }
	  hilbert_1d_sig(dstmpt->xd, lny, 0, dstmpt->yd, bp_filter, bp_width, fp, filbp);
	  max_of_amplitude(dstmpt->yd, lny, &maxi, &maxa2);
	  grab_phase_around_max_of_amplitude(dstmpt->yd, lny, maxi, n_phi, dsphi, &iph0);
	  find_phase_zero_with_poly(dsphi, &zero);
	  dy = (zero + iph0);
	  dz = 2*(zero + iph0);
	  hilbert_1d_sig(dstmpb->xd, lny, 0, dstmpb->yd, bp_filter, bp_width, fp, filbp);
	  max_of_amplitude(dstmpb->yd, lny, &maxi, &maxa2);
	  grab_phase_around_max_of_amplitude(dstmpb->yd, lny, maxi, n_phi, dsphi, &iph0);
	  find_phase_zero_with_poly(dsphi, &zero);
	  dy += (zero + iph0) - lny/2;
	  if (track_xy)   dsy->xd[i-start] = dy + ly0;
	  else    dsy->xd[i-start] = dy;
	  dz -= 2*(zero + iph0);
	  dsz->xd[i-start] = dz;
	}
      if (track_xy)
	{
	  lx0 += (int)(dx + 0.5);
	  ly0 += (int)(dy + 0.5);
	  refresh_screen_image_and_shear_rect(imr, ois, dia, lx0, ly0, (int)(0.5+dz), cl, 2*dpx, h_size, sw, color);
	}
    }
  lx0 = lx0s; ly0 = ly0s;
  dt = my_uclock() - dt;
  if (timing)  win_printf("took %g ms",1000*(double)dt/get_my_uclocks_per_sec());
  if (ROI_set == 0)
    {
      x0 = lx0; nx = lnx; y0 = ly0; ny = lny;
    }
  free_data_set(dstmpt);
  free_data_set(dstmpb);
  free_data_set(dstmp2);
  free_data_set(dsphi);
  free_fft_plan_bt(fp);
  free_filter_bt(filbp);
  free_fft_plan_bt(fp2);
  free_filter_bt(fillp2);
  return refresh_image(imr,UNCHANGED);
}



int	grab_shear_profiles_from_movie_2(void)
{
  int i, itmp, k, j;
  //int snx = 0, sny = 0;
  int  nfi, ROI_set, lx0, lnx, ly0, lny, ips;
  O_i   *oid, *ois, *oid2;
  d_s *dstmp = NULL, *dstmp2 = NULL;
  imreg *imr;
  static int x0 = 0, nx = 64, y0 = 0, ny = 128, dpx = 32, top = 0, h_size = 5;
  static int start = 0, end = 1, sw = 0, invert = 0, black_l = 100;
  double dtmp = -1024;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) < 1)	return OFF;
  nfi = abs(ois->im.n_f);
  nfi = (nfi == 0) ? 1 : nfi;
  if(updating_menu_state != 0)
    {
      if (nfi <= 1) 	 active_menu->flags |=  D_DISABLED;
      else               active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }

  end = nfi;
  ROI_set = (ois->im.nxs > 0 || ois->im.nys > 0 || ois->im.nxe < ois->im.nx || ois->im.nye < ois->im.ny) ? 1 : 0;

  if (ROI_set)
    {
      lx0 = ois->im.nxs; lnx = ois->im.nxe - ois->im.nxs;
      ly0 = ois->im.nys; lny = ois->im.nye - ois->im.nys;
    }
  else
    {
      lx0 = x0; lnx = nx; ly0 = y0; lny = ny;
    }

  i = win_scanf("Construction of shear profiles of offset interferometry images\n"
		"Specify the center and size of the sub image in the movie:\n"
		"Xc %6d size in X %6d Yc %6d size in Y %6d\n"
		"Distance between profiles %6d\n"
		"Normal%R or swap XY%r half size of shear avg %3d\n"
		"Both profiles %R just top one %r or bottom one %r\n"
		"Invert second profile %b\n"
		"Grab profiles starting at frame %6d ending at %6d\n"
		"Black level %4d\n"
		"Wrinte value in image %8lf (<0 => no writting)"
		,&lx0,&lnx,&ly0,&lny,&dpx,&sw,&h_size,&top,&invert,&start,&end,&black_l,&dtmp);
  if (i == WIN_CANCEL) return  D_O_K;

  ips = (top == 0) ? 2 : 1;
  ips = (sw == 0) ? ips*lnx : ips*lny;

  oid = create_and_attach_oi_to_imr(imr, ips, end-start, IS_FLOAT_IMAGE);
  oid2 = create_and_attach_oi_to_imr(imr, (sw == 0) ? lny : lnx, end-start, IS_FLOAT_IMAGE);
  if (oid == NULL ||oid2 == NULL)	return win_printf_OK("cannot create profile !");

  for (i = start; i < end; i++)
    {
      switch_frame(ois,i);
      if (sw == 0)
	{
	  if (dstmp2 == NULL) dstmp2 = build_data_set(lny,lny);
	  if (dstmp2 == NULL) return win_printf_OK("cannot create ds !");
	  for (j = 0 ; j< lnx ; j++)
	    {
	      extract_raw_partial_row(ois, lx0+j-lnx/2, dstmp2->yd,ly0-lny/2,lny);
	      for (k=0 ; k < lny ; k++)
		oid2->im.pixel[i-start].fl[k] += dstmp2->yd[k] - black_l;
	    }
	}
      else
	{
	  if (dstmp2 == NULL) dstmp2 = build_data_set(lnx,lnx);
	  if (dstmp2 == NULL) return win_printf_OK("cannot create ds !");

	  for (j = 0 ; j< lny ; j++)
	    {
	      extract_raw_partial_line(ois, ly0+j-lny/2, dstmp2->yd, lx0-lnx/2, lnx);
	      for (k=0 ; k < lnx ; k++)
		oid2->im.pixel[i-start].fl[k] += dstmp2->yd[k] - black_l;
	    }
	}
    }




  for (i = start; i < end; i++)
    {
      switch_frame(ois,i);
      if (sw == 0)
	{
	  if (dstmp == NULL) dstmp = build_data_set(lnx,lnx);
	  if (dstmp == NULL) return win_printf_OK("cannot create ds !");
	  //for (j = lny/2 - dpx/2 - h_size; j <= lny/2 - dpx/2 + h_size ; j++)
	  for (j = dpx/2 - h_size; j <= dpx/2 + h_size ; j++)
	    {
	      if (j < 0 || j >= lny/2) continue;
	      extract_raw_partial_line(ois, ly0-j-1, dstmp->yd,lx0-lnx/2,lnx);
	      extract_raw_partial_line(ois, ly0+j, dstmp->xd,lx0-lnx/2,lnx);
	      for (k=0 ; k < lnx ; k++)
		{
		  if (top < 2) oid->im.pixel[i-start].fl[k] += dstmp->yd[k] - black_l;
		  itmp = (invert) ? lnx-1-k  : k;
		  if (top == 0) oid->im.pixel[i-start].fl[lnx+k] += dstmp->xd[itmp] - black_l;
		  else if (top == 2) oid->im.pixel[i-start].fl[k] += dstmp->xd[itmp] - black_l;
		}
	      for (k=0; dtmp > 0 && k < lnx ; k++)
		{
		  set_raw_pixel_value(ois, lx0+k-lnx/2, ly0-j-1, &dtmp);
		  set_raw_pixel_value(ois, lx0+k-lnx/2, ly0+j, &dtmp);
		}
	    }
	}
      else
	{
	  if (dstmp == NULL) dstmp = build_data_set(lny,lny);
	  if (dstmp == NULL) return win_printf_OK("cannot create ds !");
	  //for (j = lnx/2 - dpx/2 - h_size; j <= lnx/2 - dpx/2 + h_size ; j++)
	  for (j = dpx/2 - h_size; j <= dpx/2 + h_size ; j++)
	    {
	      if (j < 0 || j >= lnx/2) continue;
	      extract_raw_partial_row(ois, lx0-j-1, dstmp->yd,ly0-lny/2,lny);
	      extract_raw_partial_row(ois, lx0+j, dstmp->xd,ly0-lny/2,lny);
	      for (k=0 ; k < lny ; k++)
		{
		  if (top < 2)  oid->im.pixel[i-start].fl[k] += dstmp->yd[k] - black_l;
		  itmp = (invert) ? lny-1-k  : k;
		  if (top == 0)  oid->im.pixel[i-start].fl[lny+k] += dstmp->xd[itmp] - black_l;
		  else if (top == 2)  oid->im.pixel[i-start].fl[k] += dstmp->xd[itmp] - black_l;
		}
	      for (k=0; dtmp > 0 && k < lnx ; k++)
		{
		  set_raw_pixel_value(ois, lx0-j-1, ly0+k-lny/2, &dtmp);
		  set_raw_pixel_value(ois, lx0+j, ly0+k-lny/2, &dtmp);
		}
	    }
	}
    }

  if (ROI_set == 0)
    {
      x0 = lx0; nx = lnx; y0 = ly0; ny = lny;
    }
  find_zmin_zmax(oid);
  find_zmin_zmax(oid2);
  if (sw == 0)
    {
      uns_oi_2_oi_by_type(ois, IS_X_UNIT_SET, oid, IS_X_UNIT_SET);
      uns_oi_2_oi_by_type(ois, IS_Y_UNIT_SET, oid, IS_Y_UNIT_SET);
      uns_oi_2_oi_by_type(ois, IS_X_UNIT_SET, oid2, IS_X_UNIT_SET);
      uns_oi_2_oi_by_type(ois, IS_Y_UNIT_SET, oid2, IS_Y_UNIT_SET);
    }
  else
    {
      uns_oi_2_oi_by_type(ois, IS_X_UNIT_SET, oid, IS_Y_UNIT_SET);
      uns_oi_2_oi_by_type(ois, IS_Y_UNIT_SET, oid, IS_X_UNIT_SET);
      uns_oi_2_oi_by_type(ois, IS_X_UNIT_SET, oid2, IS_Y_UNIT_SET);
      uns_oi_2_oi_by_type(ois, IS_Y_UNIT_SET, oid2, IS_X_UNIT_SET);
    }
  inherit_from_im_to_im(oid,ois);
  inherit_from_im_to_im(oid2,ois);
  smart_map_pixel_ratio_of_image_and_screen(oid);
  if (oid->height > 1.25) set_oi_vertical_extend(oid, 1.25);
  smart_map_pixel_ratio_of_image_and_screen(oid2);
  if (oid2->height > 1.25) set_oi_vertical_extend(oid2, 1.25);
  //map_pixel_ratio_of_image_and_screen(oid, 1, 1);
  //map_pixel_ratio_of_image_and_screen(oid2, 1, 1);
  oid->need_to_refresh = ALL_NEED_REFRESH;
  oid2->need_to_refresh = ALL_NEED_REFRESH;
  free_data_set(dstmp);
  free_data_set(dstmp2);
  return refresh_image(imr, imr->n_oi - 1);
}



int	grab_shear_profiles_from_movie(void)
{
  int i, itmp, k, j;
  //int snx = 0, sny = 0;
  int  nfi, ROI_set, lx0, lnx, ly0, lny, ips;
  O_i   *oid, *ois, *oid2;
  d_s *dstmp = NULL, *dstmp2 = NULL;
  imreg *imr;
  static int x0 = 0, nx = 64, y0 = 0, ny = 128, top = 0;
  static int start = 0, end = 1, sw = 0, invert = 0;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) < 1)	return OFF;
  nfi = abs(ois->im.n_f);
  nfi = (nfi == 0) ? 1 : nfi;
  if(updating_menu_state != 0)
    {
      if (nfi <= 1) 	 active_menu->flags |=  D_DISABLED;
      else               active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }

  end = nfi;
  ROI_set = (ois->im.nxs > 0 || ois->im.nys > 0 || ois->im.nxe < ois->im.nx || ois->im.nye < ois->im.ny) ? 1 : 0;

  if (ROI_set)
    {
      lx0 = ois->im.nxs; lnx = ois->im.nxe - ois->im.nxs;
      ly0 = ois->im.nys; lny = ois->im.nye - ois->im.nys;
    }
  else
    {
      lx0 = x0; lnx = nx; ly0 = y0; lny = ny;
    }

  i = win_scanf("Construction of shear profiles of offset interferometry images\n"
		"Specify the center and size of the sub image in the movie:\n"
		"Xc %6d size in X %6d Yc %6d size in Y %6d\n"
		"Normal%R or swap XY%r Both profiles %R just top one %r or bottom one %r\n"
		"Invert second profile %b\n"
		"Grab profiles starting at frame %6d ending at %6d\n"
		,&lx0,&lnx,&ly0,&lny,&sw,&top,&invert,&start,&end);
  if (i == WIN_CANCEL) return  D_O_K;

  ips = (top == 0) ? 2 : 1;
  ips = (sw == 0) ? ips*lnx : ips*lny;

  oid = create_and_attach_oi_to_imr(imr, ips, end-start, IS_FLOAT_IMAGE);
  oid2 = create_and_attach_oi_to_imr(imr, (sw == 0) ? lny : lnx, end-start, IS_FLOAT_IMAGE);
  if (oid == NULL ||oid2 == NULL)	return win_printf_OK("cannot create profile !");

  for (i = start; i < end; i++)
    {
      switch_frame(ois,i);
      if (sw == 0)
	{
	  if (dstmp2 == NULL) dstmp2 = build_data_set(ois->im.ny,ois->im.ny);
	  if (dstmp2 == NULL) return win_printf_OK("cannot create ds !");
	  for (j = 0 ; j< lnx ; j++)
	    {
	      extract_raw_row(ois, lx0+j-lnx/2, dstmp2->yd);
	      for (k=0 ; k < lny ; k++)
		{
		  itmp = k+ly0-lny/2;
		  itmp = (itmp < 0) ? 0 : itmp;
		  itmp = (itmp < ois->im.ny) ? itmp : ois->im.ny-1;
		  oid2->im.pixel[i-start].fl[k] += dstmp2->yd[itmp];
		}
	    }
	}
      else
	{
	  if (dstmp2 == NULL) dstmp2 = build_data_set(ois->im.nx,ois->im.nx);
	  if (dstmp2 == NULL) return win_printf_OK("cannot create ds !");
	  for (j = 0 ; j< lny ; j++)
	    {
	      extract_raw_line(ois, ly0+j-lny/2, dstmp2->yd);
	      for (k=0 ; k < lnx ; k++)
		{
		  itmp = k+lx0-lnx/2;
		  itmp = (itmp < 0) ? 0 : itmp;
		  itmp = (itmp < ois->im.nx) ? itmp : ois->im.nx-1;
		  oid2->im.pixel[i-start].fl[k] += dstmp2->yd[itmp];
		}
	    }
	}
    }

  for (i = start; i < end; i++)
    {
      switch_frame(ois,i);
      if (sw == 0)
	{
	  if (dstmp == NULL) dstmp = build_data_set(ois->im.nx,ois->im.nx);
	  if (dstmp == NULL) return win_printf_OK("cannot create ds !");
	  for (j = 0 ; j< lny/2 ; j++)
	    {
	      extract_raw_line(ois, ly0-j-1, dstmp->yd);
	      extract_raw_line(ois, ly0+j, dstmp->xd);
	      for (k=0 ; k < lnx ; k++)
		{
		  itmp = lx0+k-lnx/2;
		  itmp = (itmp < 0) ? 0 : itmp;
		  itmp = (itmp < ois->im.nx) ? itmp : ois->im.nx-1;
		  if (top < 2) oid->im.pixel[i-start].fl[k] += dstmp->yd[itmp];
		  itmp = (invert) ? lx0-1-k+lnx/2  : k+lx0-lnx/2;
		  itmp = (itmp < 0) ? 0 : itmp;
		  itmp = (itmp < ois->im.nx) ? itmp : ois->im.nx-1;
		  if (top == 0) oid->im.pixel[i-start].fl[lnx+k] += dstmp->xd[itmp];
		  else if (top == 2) oid->im.pixel[i-start].fl[k] += dstmp->xd[itmp];
		}
	    }
	}
      else
	{
	  if (dstmp == NULL) dstmp = build_data_set(ois->im.ny,ois->im.ny);
	  if (dstmp == NULL) return win_printf_OK("cannot create ds !");
	  for (j = 0 ; j< lnx/2 ; j++)
	    {
	      extract_raw_row(ois, lx0-j-1, dstmp->yd);
	      extract_raw_row(ois, lx0+j, dstmp->xd);
	      for (k=0 ; k < lny ; k++)
		{
		  itmp = k+ly0-lny/2;
		  itmp = (itmp < 0) ? 0 : itmp;
		  itmp = (itmp < ois->im.ny) ? itmp : ois->im.ny-1;
		  if (top < 2)  oid->im.pixel[i-start].fl[k] += dstmp->yd[itmp];
		  itmp = (invert) ? ly0-1-k+lny/2  : k+ly0-lny/2;
		  itmp = (itmp < 0) ? 0 : itmp;
		  itmp = (itmp < ois->im.ny) ? itmp : ois->im.ny-1;
		  if (top == 0)  oid->im.pixel[i-start].fl[lny+k] += dstmp->xd[itmp];
		  else if (top == 2)  oid->im.pixel[i-start].fl[k] += dstmp->xd[itmp];
		}
	    }
	}
    }

  if (ROI_set == 0)
    {
      x0 = lx0; nx = lnx; y0 = ly0; ny = lny;
    }
  find_zmin_zmax(oid);
  find_zmin_zmax(oid2);
  if (sw == 0)
    {
      uns_oi_2_oi_by_type(ois, IS_X_UNIT_SET, oid, IS_X_UNIT_SET);
      uns_oi_2_oi_by_type(ois, IS_Y_UNIT_SET, oid, IS_Y_UNIT_SET);
      uns_oi_2_oi_by_type(ois, IS_X_UNIT_SET, oid2, IS_X_UNIT_SET);
      uns_oi_2_oi_by_type(ois, IS_Y_UNIT_SET, oid2, IS_Y_UNIT_SET);
    }
  else
    {
      uns_oi_2_oi_by_type(ois, IS_X_UNIT_SET, oid, IS_Y_UNIT_SET);
      uns_oi_2_oi_by_type(ois, IS_Y_UNIT_SET, oid, IS_X_UNIT_SET);
      uns_oi_2_oi_by_type(ois, IS_X_UNIT_SET, oid2, IS_Y_UNIT_SET);
      uns_oi_2_oi_by_type(ois, IS_Y_UNIT_SET, oid2, IS_X_UNIT_SET);
    }
  inherit_from_im_to_im(oid,ois);
  inherit_from_im_to_im(oid2,ois);
  smart_map_pixel_ratio_of_image_and_screen(oid);
  if (oid->height > 1.25) set_oi_vertical_extend(oid, 1.25);
  smart_map_pixel_ratio_of_image_and_screen(oid2);
  if (oid2->height > 1.25) set_oi_vertical_extend(oid2, 1.25);
  //  map_pixel_ratio_of_image_and_screen(oid, 1, 1);
  //map_pixel_ratio_of_image_and_screen(oid2, 1, 1);
  oid->need_to_refresh = ALL_NEED_REFRESH;
  oid2->need_to_refresh = ALL_NEED_REFRESH;
  free_data_set(dstmp);
  free_data_set(dstmp2);
  return refresh_image(imr, imr->n_oi - 1);
}




int do_complexify_ds(void)
{
  register int j;
  O_p  *op = NULL;
  d_s *ds, *dsc = NULL;
  pltreg *pr = NULL;
  float tmp;
  fft_plan *fp = NULL;

  if(updating_menu_state != 0)	return D_O_K;

  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
    return win_printf_OK("cannot find data");

  fp = fftbt_init(fp, ds->nx);
  if (fp == NULL) 	return win_printf_OK("cannot init FFT !");

  if ((dsc = create_and_attach_one_ds(op, ds->nx, ds->nx, 0)) == NULL)
    return win_printf_OK("cannot create plot !");

  for (j = 0;j < ds->nx; j++)
    {
      dsc->yd[j] = ds->yd[j];
      dsc->xd[j] = ds->xd[j];
    }

  realtr1bt(fp, dsc->yd);
  fftbt(fp, dsc->yd, 1);
  realtr2bt(fp, dsc->yd, 1);

  for (j = 2;j < ds->nx; j+=2)
    {
      tmp = dsc->yd[j+1];
      dsc->yd[j+1] = dsc->yd[j];
      dsc->yd[j] = -tmp;
    }
  dsc->yd[1] = dsc->yd[0] = 0;

    /*	get back to complex world */
  realtr2bt(fp, dsc->yd, -1);
  fftbt(fp, dsc->yd, -1);
  realtr1bt(fp, dsc->yd);

  free_fft_plan_bt(fp);

  /* now we must do some house keeping */
  set_ds_source(dsc,"complexified ds");
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}





int do_bp_complexify_ds(void)
{
  register int j;
  O_p  *op = NULL;
  d_s *ds, *dsr = NULL, *dsi = NULL;
  pltreg *pr = NULL;
  float tmp, f0, amp;
  fft_plan *fp = NULL;
  filter*fil = NULL;
  static int bp_f = 16, bp_w = 6;


  if(updating_menu_state != 0)	return D_O_K;

  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
    return win_printf_OK("cannot find data");

  fp = fftbt_init(fp, ds->nx);
  if (fp == NULL) 	return win_printf_OK("cannot init FFT !");
  fil = filter_init_bt(fil, ds->nx);
  if (fil == NULL) 	return win_printf_OK("cannot init filter !");


  j = win_scanf("Complexify after BP filter\nCenter of BP %6d width %6d\n",&bp_f,&bp_w);
  if (j == WIN_CANCEL) return 0;

  if ((dsr = create_and_attach_one_ds(op, ds->nx, ds->nx, 0)) == NULL)
    return win_printf_OK("cannot create plot !");

  if ((dsi = create_and_attach_one_ds(op, ds->nx, ds->nx, 0)) == NULL)
    return win_printf_OK("cannot create plot !");

  for (j = 0;j < ds->nx; j++)
    {
      dsr->yd[j] = ds->yd[j];
      dsr->xd[j] = dsi->xd[j] = ds->xd[j];
    }

  realtr1bt(fp, dsr->yd);
  fftbt(fp, dsr->yd, 1);
  realtr2bt(fp, dsr->yd, 1);

  bandpass_smooth_half_bt (fil, dsr->nx, dsr->yd, bp_f, bp_w);

  for (j = bp_f-2 * bp_w, f0 = amp = 0;j < bp_f+2*bp_w; j++)
    {
      if (j < 0 || j >= ds->nx) continue;
      tmp = dsr->yd[2*j] * dsr->yd[2*j] + dsr->yd[2*j+1] * dsr->yd[2*j+1];
      f0 += tmp * j;
      amp += tmp;
    }
  if (amp > 0) f0 /= amp;
  for (j = 2;j < ds->nx; j+=2)
    {
      dsi->yd[j+1] = dsr->yd[j];
      dsi->yd[j] = -dsr->yd[j+1];
    }
  dsi->yd[1] = dsi->yd[0] = 0;

    /*	get back to complex world */
  realtr2bt(fp, dsr->yd, -1);
  fftbt(fp, dsr->yd, -1);
  realtr1bt(fp, dsr->yd);

  realtr2bt(fp, dsi->yd, -1);
  fftbt(fp, dsi->yd, -1);
  realtr1bt(fp, dsi->yd);

  free_fft_plan_bt(fp);
  free_filter_bt(fil);

  /* now we must do some house keeping */
  set_ds_source(dsi,"complexified ds");
  set_ds_source(dsr,"BP ds f0 = %d w = %d, f0 = %g",bp_f,bp_w,f0);

  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}

# ifdef ENCOURS

int measure_phase(float *dre, float *dim, int nx)
{
  float re, im, phi, phim, phit, phil, amp;
  register int i, j;

  re = dre[0];
  im = dim[0];
  phi = (im == 0.0 && re == 0.0) ? 0.0 : atan2(im, re);
  dsp->yd[0] = dsfi->yd[0] = dsfi->xd[0] = phi;

  for (j = 1, phit = phim = 0, amp = 0;j < nx; j++)
    {
      re = dre[j] * dre[j-1] + dim[j] * dim[j-1];
      im = dim[j] * dre[j-1] - dre[j] * dim[j-1];
      phi += (im == 0.0 && re == 0.0) ? 0.0 : atan2(im, re);
      re = dre[j];
      im = dim[j];
      phil = (im == 0.0 && re == 0.0) ? 0.0 : atan2(im, re);
      for (k = 0; phi - phil - phit > 1; k++, phit += 2*M_PI);
      if (k > 1) win_printf("k = %d j=%d -> phi 1 %g phi 2 -> %g dphi %g",k,j,phi,phil+phit,phil+phit-phi);
      for (k = 0; phi - phil - phit < -1; k--, phit -= 2*M_PI);
      if (k < -1) win_printf("k = %d j=%d -> phi 1 %g phi 2 -> %g dphi %g",k,j,phi,phil+phit,phil+phit-phi);
      if (fabs(phil+phit-phi) > 1e-6)
	win_printf("j=%d -> phi 1 %g phi 2 -> %g dphi %g",j,phi,phil+phit,phil+phit-phi);

      dsp->yd[j] = (2*M_PI*j*bp_filter)/nx;
      dsp->yd[j] += phil + phit;
      if (2*dsfr->yd[j] > Max_val)
	{
	  phim += dsp->yd[j] * dsfr->yd[j];
	  amp += dsfr->yd[j];
	}
      dsfr->yd[j] = sqrt(dsfr->yd[j]);
    }
  if (amp > 0) phim /= amp;
  ds->xd[i] = phim;
  if (i > 0)
    {
      for (k = 0; ds->xd[i] - ds->xd[i-1] > M_PI; k++, ds->xd[i] -= 2*M_PI);
      for (k = 0; ds->xd[i] - ds->xd[i-1] < -M_PI; k++, ds->xd[i] += 2*M_PI);
    }
  ds->yd[i] = i + r_s;
  for (j = 0;j < nx; j++)
    dsfi->xd[j] = dsfr->xd[j] = dsp->xd[j] = j;

}

# endif

int do_trk_phase_avg_along_y(void)
{
  register int i, j, k;
  int r_s, r_e, size, comp;
  static int  nx = 128; // , filf=8, filw=4, , bp=0
  static int bp_filter = 7, bp_width = 3;
  O_i *ois;
  O_p *op;
  d_s *ds, *dsa = NULL, *dst = NULL;
  d_s *dsr = NULL, *dsi = NULL, *dsfr = NULL, *dsfi = NULL, *dsp = NULL;
  imreg *imr;
  int imax;
  float tmp, f0, amp, Max_pos, Max_val;
  double re, im, phi, phil, phit, phim;
  filter *fil = NULL;
  fft_plan *fp = NULL;


  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)    return D_O_K;
  comp = (ois->im.data_type == IS_COMPLEX_IMAGE
	  || ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE) ? 1 : 0;
  if(updating_menu_state != 0)
    {
      if (comp) 	 active_menu->flags |=  D_DISABLED;
      else               active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }

  r_s = 0; r_e = ois->im.ny;
  nx = ois->im.nx;
  fp = fftbt_init(fp, nx);
  if (fp == NULL) 	return win_printf_OK("cannot init FFT !");
  fil = filter_init_bt(fil, nx);
  if (fil == NULL) 	return win_printf_OK("cannot init filter !");

  i = win_scanf("look for maximum by wave and zero of phase\n"
		"starting at line %8d and ending at line %8d\n"
		"---------------------------------------------------------\n"
		"Band-pass filter freq %6d width %6d \n"
		,&r_s,&r_e,&bp_filter,&bp_width);
  if (i == WIN_CANCEL) return D_O_K;

  /*
  if ((dst = build_data_set(nx, nx)) == NULL)
    return win_printf_OK("cannot create plot !");
  if ((dsr = build_data_set(nx, nx)) == NULL)
    return win_printf_OK("cannot create plot !");
  if ((dsi = build_data_set(nx, nx)) == NULL)
    return win_printf_OK("cannot create plot !");
  if ((dsfr = build_data_set(nx, nx)) == NULL)
    return win_printf_OK("cannot create plot !");
  if ((dsfi = build_data_set(nx, nx)) == NULL)
    return win_printf_OK("cannot create plot !");
  if ((dsp = build_data_set(nx, nx)) == NULL)
    return win_printf_OK("cannot create plot !");
  */

  size = 1 + r_e - r_s;
  size = (size > ois->im.ny) ? ois->im.ny : size;
  op = create_and_attach_op_to_oi(ois, size, size, 0, IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
  if (op == NULL) return (win_printf_OK("cant create plot!"));
  ds = op->dat[0];
  dsa = create_and_attach_one_ds(op, size, size, 0);
  set_plot_title(op,"(%d to %d)",r_s, r_e);
  set_ds_treatement(ds,"Local BP max  and phase zero (%d to %d)",r_s, r_e);
  inherit_from_im_to_ds(ds, ois);
  uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
  uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
  op->filename = strdup(ois->filename);

  if ((dst = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  set_ds_treatement(dst,"Local fringes profile (%d to %d)",r_s, r_e);
  if ((dsr = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  set_ds_treatement(dsr,"Fringes profile real part (%d to %d)",r_s, r_e);
  if ((dsi = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  set_ds_treatement(dsi,"Fringes profile imaginary part (%d to %d)",r_s, r_e);
  if ((dsfr = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  set_ds_treatement(dsfr,"Dummy Fringes profile real part (%d to %d)",r_s, r_e);
  if ((dsfi = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  set_ds_treatement(dsfi,"Dummy Fringes profile imaginary part (%d to %d)",r_s, r_e);
  if ((dsp = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  set_ds_treatement(dsp,"Fringes profile phase (%d to %d)",r_s, r_e);

  for (i=0 ; i< size ; i++)
    { // grab one line profile
      extract_raw_line(ois, i+r_s, dst->yd);
      for(j = 0; j < nx; j++)	  dst->xd[j] = j;
      //copy dst to dsr
      for (j = 0;j < nx; j++)
	{
	  dsr->yd[j] = dst->yd[j];
	  dsr->xd[j] = dsi->xd[j] = j;
	}
      // fft and bp dsr
      realtr1bt(fp, dsr->yd);  fftbt(fp, dsr->yd, 1);  realtr2bt(fp, dsr->yd, 1);
      bandpass_smooth_half_bt (fil, dsr->nx, dsr->yd, bp_filter, bp_width);

      for (j = bp_filter-2 * bp_width, f0 = amp = 0;j < bp_filter+2*bp_width; j++)
	{  // we compute the frequency barycenter
	  if (j < 0 || j >= nx) continue;
	  tmp = dsr->yd[2*j] * dsr->yd[2*j] + dsr->yd[2*j+1] * dsr->yd[2*j+1];
	  f0 += tmp * j;
	  amp += tmp;
	}
      if (amp > 0) f0 /= amp;
      for (j = 2;j < nx; j+=2)
	{  // we prepare imaginary part
	  dsi->yd[j+1] = dsr->yd[j];
	  dsi->yd[j] = -dsr->yd[j+1];
	}
      dsi->yd[1] = dsi->yd[0] = 0;

      /*	get back to complex world */
      realtr2bt(fp, dsr->yd, -1);  fftbt(fp, dsr->yd, -1); realtr1bt(fp, dsr->yd);
      realtr2bt(fp, dsi->yd, -1);  fftbt(fp, dsi->yd, -1); realtr1bt(fp, dsi->yd);

      for (j = 0, imax = 0;j < nx; j++)
	{  // we find the amplitude maximum and place the amplitude in dsfr
	  tmp = dsr->yd[j] * dsr->yd[j] + dsi->yd[j] * dsi->yd[j];
	  dsfr->yd[j] = tmp;
	  if (tmp > dsfr->yd[imax]) imax = j;
	} // the amplitude is stored in dsfr->yd
      j = find_max_around(dsfr->yd, dsfr->nx, imax, &Max_pos, &Max_val, NULL);
      amp = sqrt(Max_val);
      dsa->xd[i] = Max_pos; // this is the amplitude max position
      dsa->yd[i] = i + r_s;

      /*
      for (j = 0;j < nx; j++)
	{ // we build a complex signal in xd of dsf(r,i) with the frequency of bp_filter centered on n_2
	  dsfr->xd[j] = amp * cos((2*M_PI*(j-n_2)*bp_filter)/nx);
	  dsfi->xd[j] = -amp * sin((2*M_PI*(j-n_2)*bp_filter)/nx);
	}
      */
      /*

      for (j = 0, phi = 0, amp = 0;j < nx; j++)
	{
	  re = dsr->yd[j] * dsfr->xd[j] + dsi->yd[j] * dsfi->xd[j];
	  im = dsfi->xd[j] * dsr->yd[j] - dsfr->xd[j] * dsi->yd[j];
	  dsfi->yd[j] = (im == 0.0 && re == 0.0) ? 0.0 : atan2(im, re);
	  //dsp->yd[j] = (2*M_PI*(j-n_2)*bp_filter)/nx;
	  dsp->yd[j] = -dsfi->yd[j];
	  if (2*dsfr->yd[j] > Max_val)
	    {
	      phi += dsp->yd[j] * dsfr->yd[j];
	      amp += dsfr->yd[j];
	    }
	  dsfr->yd[j] = sqrt(dsfr->yd[j]);
	}
      if (amp > 0) phi /= amp;
      ds->xd[i] = phi;

      */

      re = dsr->yd[0];
      im = dsi->yd[0];
      phi = (im == 0.0 && re == 0.0) ? 0.0 : atan2(im, re);
      dsp->yd[0] = dsfi->yd[0] = dsfi->xd[0] = phi;
      for (j = 1, phit = phim = 0, amp = 0;j < nx; j++)
	{
	  re = dsr->yd[j] * dsr->yd[j-1] + dsi->yd[j] * dsi->yd[j-1];
	  im = dsi->yd[j] * dsr->yd[j-1] - dsr->yd[j] * dsi->yd[j-1];
	  // phi integrates phase difference between j and j-1, thus absolute but error prone
	  phi += (im == 0.0 && re == 0.0) ? 0.0 : atan2(im, re);
	  dsfi->xd[j] = phi;
	  re = dsr->yd[j];
	  im = dsi->yd[j];
	  phil = (im == 0.0 && re == 0.0) ? 0.0 : atan2(im, re); // phase in j
	  // we unwrappe phil so that it matches phi (but with less error)
	  for (k = 0; phi - phil - phit > 1; k++, phit += 2*M_PI);
	  if (k > 1) win_printf("k = %d j=%d -> phi 1 %g phi 2 -> %g dphi %g",k,j,phi,phil+phit,phil+phit-phi);
	  for (k = 0; phi - phil - phit < -1; k--, phit -= 2*M_PI);
	  if (k < -1) win_printf("k = %d j=%d -> phi 1 %g phi 2 -> %g dphi %g",k,j,phi,phil+phit,phil+phit-phi);
	  dsfi->yd[j] = phil + phit; // we unwrappe
	  if (fabs(phil+phit-phi) > 1e-6)  // we check that this is ok
	    win_printf("j=%d -> phi 1 %g phi 2 -> %g dphi %g",j,phi,phil+phit,phil+phit-phi);

	  dsp->yd[j] = (2*M_PI*j*bp_filter)/nx;
	  dsp->yd[j] += dsfi->yd[j];
	  if (2*dsfr->yd[j] > Max_val) // we keep phase if amp^2 > max/2
	    {
	      phim += dsp->yd[j] * dsfr->yd[j];
	      amp += dsfr->yd[j];
	    }
	  dsfr->yd[j] = sqrt(dsfr->yd[j]);
	}
      if (amp > 0) phim /= amp;
      ds->xd[i] = phim;      // average phase
      if (i > 0)
	{
	  for (k = 0; ds->xd[i] - ds->xd[i-1] > M_PI; k++, ds->xd[i] -= 2*M_PI);
	  for (k = 0; ds->xd[i] - ds->xd[i-1] < -M_PI; k++, ds->xd[i] += 2*M_PI);
	} // we unwrap in time
      ds->yd[i] = i + r_s;
      for (j = 0;j < nx; j++) // we restore xd
	dsfi->xd[j] = dsfr->xd[j] = dsp->xd[j] = j;

    }
  for (i=0 ; i< size ; i++) // we convert phase in position
    ds->xd[i] *= (double)nx/(2*M_PI*bp_filter);



  /*
  free_data_set(dst);
  free_data_set(dsr);
  free_data_set(dsi);
  free_data_set(dsfr);
  free_data_set(dsfi);
  free_data_set(dsp);
  */
  free_fft_plan_bt(fp);
  free_filter_bt(fil);

  broadcast_dialog_message(MSG_DRAW,0);
  return D_REDRAWME;
}


int do_trk_phase_avg_along_y2(void)
{
  register int i, j, kim;
  int nim;
  int new_plot=0;
  int r_s, r_e, size, comp;
  static int  nx = 128, brick = 0; // , filf=8, filw=4, , bp=0
  static int bp_filter = 7, bp_width = 3, useapo = 1, flatw = 0, dew = 0;
  static float mr = 2;
  O_i *ois = NULL;
  O_p *op = NULL, *op_debug=NULL;
  d_s *ds = NULL, *dsa = NULL, *dst = NULL;
  d_s *dsr = NULL, *dsi = NULL, *dsfr = NULL, *dsfi = NULL, *dsp = NULL, *dsfit = NULL;
  imreg *imr;
  int imax;
  float tmp, f0, amp, Max_pos, Max_val;
  double re, im, phi;
  static float phi0 = 0;
  filter *fil = NULL;
  fft_plan *fp = NULL;
  static int movie=0,movie_min=0,movie_max=0;



  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)    return D_O_K;
  comp = (ois->im.data_type == IS_COMPLEX_IMAGE
	  || ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE) ? 1 : 0;
  if(updating_menu_state != 0)
    {
      if (comp) 	 active_menu->flags |=  D_DISABLED;
      else               active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }

  r_s = 0; r_e = ois->im.ny;
  nx = ois->im.nx;
  fp = fftbt_init(fp, nx);
  movie_max=abs(ois->im.n_f);
  if (fp == NULL) 	return win_printf_OK("cannot init FFT !");
  fil = filter_init_bt(fil, nx);
  if (fil == NULL) 	return win_printf_OK("cannot init filter !");

  i = win_scanf("look for maximum by wave and zero of phase\n"
		"starting at line %8d and ending at line %8d\n"
		"---------------------------------------------------------\n"
		"Band-pass filter freq %6d width %6d; %b->Use a brick wall filter\n"
		"\\phi_0 = %5f\n"
		"%b->Use apo on fitting, max range %4f\n"
		"%R->No windowing %r->apply signal windowing or %r->flat top\n"
		"%R->No dewindowing %r->apply dewindowing %r->deflat\n"
    "%b Movie from frame %d to frame %d \n"
		,&r_s,&r_e,&bp_filter,&bp_width,&brick,&phi0,&useapo,&mr,&flatw,&dew,&movie,&movie_min,&movie_max);



      if (movie)
      {

         nim = abs(ois->im.n_f);
         movie_max=(movie_max>nim) ? nim : movie_max;
         movie_min=(movie_min>movie_max) ? movie_max : movie_min;
         nim=movie_max-movie_min+1;
      }
      else

      {
         nim =1;
         movie_min=0;
      }




  if (i == WIN_CANCEL) return D_O_K;


  size = 1 + r_e - r_s;
  size = (size > ois->im.ny) ? ois->im.ny : size;
  op=find_oi_cur_op(ois);
  if (op==NULL)
  {
     op = create_and_attach_op_to_oi(ois, size, size, 0, IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
     new_plot=1;
   }
  if (op == NULL) return (win_printf_OK("cant create plot!"));
  if (new_plot==1) ds = op->dat[0];
  else ds = create_and_attach_one_ds(op, size, size, 0);
  op_debug = create_and_attach_op_to_oi(ois, size, size, 0, IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
  if (op_debug == NULL) return (win_printf_OK("cant create plot!"));
  dsa = create_and_attach_one_ds(op_debug, size, size, 0);
  set_plot_title(op,"(%d to %d)",r_s, r_e);
  set_ds_treatement(ds,"Local BP max  and phase zero (%d to %d)",r_s, r_e);
  inherit_from_im_to_ds(ds, ois);
  uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
  uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
  op->filename = strdup(ois->filename);

  if ((dst = create_and_attach_one_ds(op_debug, nx, nx, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  set_ds_treatement(dst,"Local fringes profile (%d to %d)",r_s, r_e);
  if ((dsr = create_and_attach_one_ds(op_debug, nx, nx, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  set_ds_treatement(dsr,"Fringes profile real part (%d to %d)",r_s, r_e);
  if ((dsi = create_and_attach_one_ds(op_debug, nx, nx, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  set_ds_treatement(dsi,"Fringes profile imaginary part (%d to %d)",r_s, r_e);
  if ((dsfr = create_and_attach_one_ds(op_debug, nx, nx, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  set_ds_treatement(dsfr,"Dummy Fringes profile real part (%d to %d)",r_s, r_e);
  if ((dsfi = create_and_attach_one_ds(op_debug, nx, nx, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  set_ds_treatement(dsfi,"Dummy Fringes profile imaginary part (%d to %d)",r_s, r_e);
  if ((dsp = create_and_attach_one_ds(op_debug, nx, nx, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  set_ds_treatement(dsp,"Fringes profile phase (%d to %d)",r_s, r_e);
  if ((dsfit = create_and_attach_one_ds(op_debug, nx, nx, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  set_ds_treatement(dsfit,"Fringes profile phase fit");
  alloc_data_set_y_error(dsfit);

  for (kim=movie_min;kim<movie_min+nim;kim++)
  {
    if (movie)
    {
      switch_frame(ois,kim);
      if (kim>movie_min)   ds = create_and_attach_one_ds(op, size, size, 0);
    }

  for (i=0 ; i< size ; i++)
    { // grab one line profile
      extract_raw_line(ois, i+r_s, dst->yd);
      for(j = 0; j < nx; j++)	  dst->xd[j] = j;
      //copy dst to dsr
      for (j = 0;j < nx; j++)
	{
	  dsr->yd[j] = dst->yd[j];
	  dsr->xd[j] = dsi->xd[j] = j;
	}
      // fft and bp dsr
      if (flatw == 1) fftbtwindow(fp, dsr->yd);
      else if (flatw == 2) fftwindow_flat_top1(fp, nx, dsr->yd, 0);
      realtr1bt(fp, dsr->yd);  fftbt(fp, dsr->yd, 1);  realtr2bt(fp, dsr->yd, 1);
      if (brick == 0)
	bandpass_smooth_half_bt (fil, dsr->nx, dsr->yd, bp_filter, bp_width);
      else
	{
	  for (j = 0;j < nx/2; j++)
	    {
	      if ((j > bp_filter - bp_width) && (j <= bp_filter+ + bp_width)) continue;
	      dsr->yd[2*j] = 0;
	      dsr->yd[2*j+1] = 0;
	    }

	}

      for (j = bp_filter-2 * bp_width, f0 = amp = 0;j < bp_filter+2*bp_width; j++)
	{  // we compute the frequency barycenter
	  if (j < 0 || j >= nx) continue;
	  tmp = dsr->yd[2*j] * dsr->yd[2*j] + dsr->yd[2*j+1] * dsr->yd[2*j+1];
	  f0 += tmp * j;
	  amp += tmp;
	}
      if (amp > 0) f0 /= amp;
      for (j = 2;j < nx; j+=2)
	{  // we prepare imaginary part
	  dsi->yd[j+1] = dsr->yd[j];
	  dsi->yd[j] = -dsr->yd[j+1];
	}
      dsi->yd[1] = dsi->yd[0] = 0;
      /*	get back to complex world */
      realtr2bt(fp, dsr->yd, -1);  fftbt(fp, dsr->yd, -1); realtr1bt(fp, dsr->yd);
      realtr2bt(fp, dsi->yd, -1);  fftbt(fp, dsi->yd, -1); realtr1bt(fp, dsi->yd);
      if (dew == 1)
	{
	  defftbtwindow(fp, dsr->yd);
	  defftbtwindow(fp, dsi->yd);
	  dsr->yd[0] = dsi->yd[0] = 0;
	}
      else if (dew == 1)
	{
	  defftwindow_flat_top1(fp, nx, dsr->yd, 0.01);
	  defftwindow_flat_top1(fp, nx, dsi->yd, 0.01);
	}


      for (j = 0;j < nx; j++)
	{  // we find the amplitude maximum and place the amplitude in dsfr
	  tmp = sqrt(dsr->yd[j] * dsr->yd[j] + dsi->yd[j] * dsi->yd[j]);
	  dsfr->yd[j] = tmp;
	} // the amplitude is stored in dsfr->yd
      for (j = 4, imax = 4;j < nx-4; j++)
	{  // we skip the border that are poluted by dewindowing
	  if (dsfr->yd[j] > dsfr->yd[imax]) imax = j;
	} // the amplitude is stored in dsfr->yd
      j = find_max_around(dsfr->yd, dsfr->nx, imax, &Max_pos, &Max_val, NULL);
      dsa->xd[i] = Max_pos; // this is the amplitude max position
      //win_printf("After max at %g",Max_pos);
      dsa->yd[i] = i + r_s;

      j = imax;
      re = dsr->yd[j];
      im = dsi->yd[j];
      phi = (im == 0.0 && re == 0.0) ? 0.0 : atan2(im, re); // phase in j
      dsfi->yd[j] = dsfi->xd[j] = phi;
      //win_printf("1");
      for (j = imax+1;j < nx; j++)
	{
	  re = dsr->yd[j] * dsr->yd[j-1] + dsi->yd[j] * dsi->yd[j-1];
	  im = dsi->yd[j] * dsr->yd[j-1] - dsr->yd[j] * dsi->yd[j-1];
	  phi += (im == 0.0 && re == 0.0) ? 0.0 : atan2(im, re);
	  dsfi->yd[j] = dsfi->xd[j] = phi;
	}
      //win_printf("2");
      phi = dsfi->yd[imax];
      for (j = imax-1;j >= 0; j--)
	{
	  re = dsr->yd[j+1] * dsr->yd[j] + dsi->yd[j+1] * dsi->yd[j];
	  im = dsi->yd[j+1] * dsr->yd[j] - dsr->yd[j+1] * dsi->yd[j];
	  phi -= (im == 0.0 && re == 0.0) ? 0.0 : atan2(im, re);
	  dsfi->yd[j] = dsfi->xd[j] = phi;
	}
      //win_printf("3");
      for (j = 0, dsfit->nx = 0;j < nx; j++)
	{
	  dsp->yd[j] = dsfi->yd[j];
	  if (mr*dsfr->yd[j] > Max_val) // we keep phase if amp^2 > max/2
	    {
	      dsfit->yd[dsfit->nx] = dsfi->yd[j];
	      dsfit->ye[dsfit->nx] = (tmp > 0) ? ((float)1)/dsfr->yd[j] : 10;
	      dsfit->xd[dsfit->nx++] = j;
	    }
	}

      for (j = 0; useapo == 1 && j < dsfit->nx; j++) // appodisation
	dsfit->ye[j] *= (float)2/(1.05 - cos(2*j*M_PI/dsfit->nx));
      double out_a = 0, out_b = 0, x = 0;
      if (dsfit->nx < 3)       display_title_message("Fitting over %d max val %g at %g",dsfit->nx,Max_val,Max_pos);
      else
	{
	  //win_printf("fitting over %d pts",dsfit->nx);
	  if (compute_least_square_fit_on_ds(dsfit, 1, &out_a, &out_b))
	    {
	      display_title_message("fitting problem");
	    }
	}
      if (out_a != 0)
	{
	  for (ds->xd[i] = x = phi0 - (out_b-(4*M_PI))/out_a; x < (double)nx; x -= (2*M_PI)/out_a)
	    {
	      //win_printf("x = %g max_pas %g",x, Max_pos);
	      if (fabs(x - Max_pos) < fabs(ds->yd[i] - Max_pos))
		ds->yd[i] = x;
	    }
	}
      else ds->xd[i] = 0;
      ds->xd[i] = i + r_s;
      for (j = 0;j < nx; j++) // we restore xd
	dsfi->xd[j] = dsfr->xd[j] = dsp->xd[j] = j;

    }

  set_ds_source(ds,"Vincent 2 algorithm for phase position, fr %d\n",kim);
}




  free_fft_plan_bt(fp);
  free_filter_bt(fil);

  broadcast_dialog_message(MSG_DRAW,0);
  return D_REDRAWME;
}



int track_line_by_line(void)
{
  int i;
  static int bp_filter=30;
  static int bp_width=15;
  static int movie_min=0;
  static int movie_max=0;
  imreg *imr = NULL;
  static int r_s=0,r_e=0;
  O_p *op = NULL;
  d_s *ds=NULL;
  int new_plot=0;
  int comp = 0;
  O_i *ois=NULL;
  float displacement;
  int nframe=0;
  int nx=0,n_frame=0;


    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)    return D_O_K;
    comp = (ois->im.data_type == IS_COMPLEX_IMAGE
  	  || ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE) ? 1 : 0;
    if(updating_menu_state != 0)
      {
        if (comp) 	 active_menu->flags |=  D_DISABLED;
        else               active_menu->flags &= ~D_DISABLED;
        return D_O_K;
      }



  r_e = ois->im.ny;
  nx = ois->im.nx;
  movie_max=abs(ois->im.n_f);


    i = win_scanf("Track fringes using Vincent algorithm line by line\n"
  		"starting at line %8d and ending at line %8d (this can be large as the non flat region will not be taken in consideration)\n"
      "Band-pass filter freq %6d width %6d; \n"
      "Analyze from frame %6d to frame  %6d"
  		"---------------------------------------------------------\n"
  		,&r_s,&r_e,&bp_filter,&bp_width,&movie_min,&movie_max);

    if (i==WIN_CANCEL) return 0;


    n_frame=movie_max-movie_min+1;


        op=find_oi_cur_op(ois);
        if (op==NULL)
          {
             op = create_and_attach_op_to_oi(ois, n_frame, n_frame, 0, IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
             new_plot=1;
           }
        if (op == NULL) return (win_printf_OK("cant create plot!"));
        if (new_plot==1) ds = op->dat[0];
        else ds = create_and_attach_one_ds(op, n_frame, n_frame, 0);

    ds->xd[0]=movie_min;
    ds->yd[0]=0; //à corriger pour avoir une vraie position en pixel




    for (i=movie_min+1;i<=movie_max;i++)
    {
      printf("frame = %d\n",i);
      ds->xd[i-movie_min]=i;
      mean_displacement_between_2_images(ois, i-1, i, r_s, r_e, bp_filter, bp_width, &displacement);
      ds->yd[i-movie_min]=ds->yd[i-movie_min-1]+displacement;

    }

    broadcast_dialog_message(MSG_DRAW,0);
    return D_REDRAWME;



}









int do_trk_phase_avg_along_Martin(void)
{
  register int i, j, ii,k,kim;
  static float min_mode,max_mode,step_mode;
  float mode_list[128] = {0};
  static int degree=1;
  static int plot_max_pos=0;
  int n_mode;
  static int movie=0;
  int nim=1;
  static int sweep_frequencies=0;
  double *coeffs=NULL;
  int r_s, r_e, size, comp;
  static int  nx = 128; // , filf=8, filw=4, , bp=0
  static int bp_filter = 7, bp_width = 3;
  double out_am,out_bm;
  static float wave_length=10.0;
  static int fit_width=2;
  static int plt_derivative=0;
  static float main_mode;
  bool continuing;
  int min_frame_c=0,max_frame_c=0;
  int compare_on_flat=0;
  int new_plot=0;
  int count_loop=0;
  float phase;
  O_i *ois;
  O_p *op  = NULL,*opphase = NULL, *opdiff = NULL, *opcomp= NULL;
  d_s *ds = NULL, *dst = NULL, *dstemp=NULL;
  d_s *dsr = NULL, *dsi = NULL, *dsfr = NULL, *dsfi = NULL, *dsp = NULL, *dsfit = NULL, *dsmax=NULL, *dsdiff = NULL, *dscomp = NULL;
  imreg *imr;
  int imax, n_2;
  static int apoyes=0;
  static float aposize=14.0;
  static int movie_min=0,movie_max=0;
  float tmp, f0, amp, Max_pos, Max_val;
  float phi_0,r_0,phi_approximate,low,high,final_position;
  //double re, im, phi, phil, phit, phim;
  static float phi0 = 0;
  filter *fil = NULL;
  fft_plan *fp = NULL;


  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)    return D_O_K;
  comp = (ois->im.data_type == IS_COMPLEX_IMAGE
	  || ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE) ? 1 : 0;
  if(updating_menu_state != 0)
    {
      if (comp) 	 active_menu->flags |=  D_DISABLED;
      else               active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }

  r_s = 0; r_e = ois->im.ny;
  nx = ois->im.nx;
  fp = fftbt_init(fp, nx);
  if (fp == NULL) 	return win_printf_OK("cannot init FFT !");
  fil = filter_init_bt(fil, nx);
  if (fil == NULL) 	return win_printf_OK("cannot init filter !");
  i=win_scanf("Please indicate the wave length you used %f\n",&wave_length);
  main_mode=(float)nx/wave_length;
  movie_max=abs(ois->im.n_f);
  i=win_scanf("look for maximum by wave and zero of phase\n"
		"%b fenêtre ? Si oui, quelle largeur ? %6f \n"
    "starting at line %8d and ending at line %8d\n"
		"---------------------------------------------------------\n"
		"Band-pass filter freq %6d width %6d \n"
    "Phase computed on mode %6f computed on 2x %6d points \n"
    "Polynome fit of degree %6d\n"
		"\\phi_0 = %5f\n"
    "%b compare different central modes choises \n"
    "If yes, precise the min mode %6f, the main mode %6f, and the step %6f. (max 128 modes) \n"
    "Do you want to compare different modes ? \n"
    "%b Plot the Max Position \n? "
    "%b Movie from frame %d to frame %d \n"
		,&apoyes,&aposize,&r_s,&r_e,&bp_filter,&bp_width,&main_mode,&fit_width,&degree,&phi0,&sweep_frequencies,&min_mode,&max_mode,&step_mode,&plot_max_pos,&movie,&movie_min,&movie_max);



  if (i == WIN_CANCEL) return D_O_K;



  if (movie)
  {

     nim = abs(ois->im.n_f);
     movie_max=(movie_max>nim) ? nim : movie_max;
     movie_min=(movie_min>movie_max) ? movie_max : movie_min;
     nim=movie_max-movie_min+1;
  }
  else

  {
     nim =1;
     movie_min=0;
  }




  if (sweep_frequencies)
  {
    n_mode=(int)((max_mode-min_mode)/step_mode);
    if (n_mode>128) return 0;
    for (ii=0;ii<n_mode;ii++)
    {
      mode_list[ii]=min_mode+step_mode*ii;
    }
  }

  else
  {
    n_mode=1;
    mode_list[0]=main_mode;
  }


  size = 1 + r_e - r_s;
  size = (size > ois->im.ny) ? ois->im.ny : size;
  op=find_oi_cur_op(ois);
  if (op==NULL)
  {
     op = create_and_attach_op_to_oi(ois, size, size, 0, IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
     new_plot=1;
   }
  if (op == NULL) return (win_printf_OK("cant create plot!"));
  if (new_plot==1) ds = op->dat[0];
  else ds = create_and_attach_one_ds(op, size, size, 0);
  if (sweep_frequencies == 0 && movie==0)
  {
  opphase=create_and_attach_op_to_oi(ois, size, size, 0, IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
  if (opphase == NULL) return (win_printf_OK("cant create plot!"));
}


if (plt_derivative == 1)
{
opdiff=create_and_attach_op_to_oi(ois, size, size, 0, IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
if (opdiff == NULL) return (win_printf_OK("cant create plot!"));
}

if (compare_on_flat == 1)
{
opcomp=create_and_attach_op_to_oi(ois, size, size, 0, IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
if (opcomp == NULL) return (win_printf_OK("cant create plot!"));
}



  set_plot_title(op,"(%d to %d)",r_s, r_e);
  set_ds_treatement(ds,"Local BP max  and phase zero (%d to %d)",r_s, r_e);
  inherit_from_im_to_ds(ds, ois);
  uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
  uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
  op->filename = strdup(ois->filename);

  if ((dst = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  set_ds_treatement(dst,"Local fringes profile (%d to %d)",r_s, r_e);
  if ((dsr = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  set_ds_treatement(dsr,"Fringes profile real part (%d to %d)",r_s, r_e);
  if ((dsi = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  set_ds_treatement(dsi,"Fringes profile imaginary part (%d to %d)",r_s, r_e);
  if ((dsfr = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  set_ds_treatement(dsfr,"Dummy Fringes profile real part (%d to %d)",r_s, r_e);
  if ((dsfi = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  set_ds_treatement(dsfi,"Dummy Fringes profile imaginary part (%d to %d)",r_s, r_e);
  if ((dsp = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  set_ds_treatement(dsp,"Fringes profile phase (%d to %d)",r_s, r_e);
  if ((dsfit = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  set_ds_treatement(dsfit,"Fringes profile phase fit");
  alloc_data_set_y_error(dsfit);

  if (plot_max_pos==1)  dsmax=create_and_attach_one_ds(op, size, size, 0);
  if (compare_on_flat==1) dscomp=create_and_attach_one_ds(opcomp,size,size,0);

for (kim=movie_min;kim<movie_min+nim;kim++)
{

  if (movie)
  {
    switch_frame(ois,kim);
    if (kim>movie_min)   ds = create_and_attach_one_ds(op, size, size, 0);
  }

for (k=0;k<n_mode;k++)

{
  printf("k=%d\n",k);
  if (k>0)   ds = create_and_attach_one_ds(op, size, size, 0);

  main_mode=mode_list[k];



  n_2 = nx/2;
  for (i=0 ; i< size ; i++)
    { // grab one line profile
      dstemp=build_adjust_data_set(NULL,nx/2,nx/2);
      if (plt_derivative==1)  dsdiff=create_and_attach_one_ds(opdiff, nx/2, nx/2, 0);

      alloc_data_set_y_error(dstemp);
      extract_raw_line(ois, i+r_s, dst->yd);
      for(j = 0; j < nx; j++)	  dst->xd[j] = j;
      //copy dst to dsr
      for (j = 0;j < nx; j++)
	{
	  dsr->yd[j] = dst->yd[j];
	  dsr->xd[j] = dsi->xd[j] =  j;
	}
      // fft and bp dsr
      realtr1bt(fp, dsr->yd);  fftbt(fp, dsr->yd, 1);  realtr2bt(fp, dsr->yd, 1);
      //bandpass_smooth_half_bt (fil, dsr->nx, dsr->yd, bp_filter, bp_width);



      // spectral_phase(dsr->yd,dstemp,n_2);
      //
      // unwrap_simple(dstemp->yd,n_2);
      //
      // if (sweep_frequencies == 0)    add_ds_to_op (opphase,dstemp);
      //
      //
      // for (ii=0;ii<nx/2;ii++)
      // {
      //   dstemp->xd[ii]=ii;
      //
      // }
      // printf("main_mode=%f\n",main_mode);
      //
      //
      // // dataset ye, xe. Barres d'erreurs. L'inverse de l'amplitude de Fourier.
      // //compute_least_square_fit_on_ds_between_indexes(dstemp, 1, (int)(main_mode-fit_width),1+(int)(main_mode+fit_width),&out_am, &out_bm);
      // fit_ds_to_xn_polynome(dstemp, degree+1, (int)(main_mode-fit_width), 1+(int)(main_mode+fit_width), -10000.0, 10000.0, &coeffs);
      // phase=0;
      // for (ii=0;ii<=degree;ii++)
      // {
      //   phase+=(float)(coeffs[ii])*pow(main_mode,ii);
      //
      // }

      //phase=out_bm+out_am*main_mode;

      bandpass_smooth_half_bt(fil, dsr->nx, dsr->yd, bp_filter, bp_width);
      for (j = bp_filter-2 * bp_width, f0 = amp = 0;j < bp_filter+2*bp_width; j++)
	{  // we compute the frequency barycenter
	  if (j < 0 || j >= nx) continue;
	  tmp = dsr->yd[2*j] * dsr->yd[2*j] + dsr->yd[2*j+1] * dsr->yd[2*j+1];
	  f0 += tmp * j;
	  amp += tmp;
	}
      if (amp > 0) f0 /= amp;
      for (j = 2;j < nx; j+=2)
	{  // we prepare imaginary part
	  dsi->yd[j+1] = dsr->yd[j];
	  dsi->yd[j] = -dsr->yd[j+1];
	}
      dsi->yd[1] = dsi->yd[0] = 0;

      /*	get back to complex world */
      realtr2bt(fp, dsr->yd, -1);  fftbt(fp, dsr->yd, -1); realtr1bt(fp, dsr->yd);
      realtr2bt(fp, dsi->yd, -1);  fftbt(fp, dsi->yd, -1); realtr1bt(fp, dsi->yd);

      for (j = 0, imax = 0;j < nx; j++)
	{  // we find the amplitude maximum and place the amplitude in dsfr
	  tmp = dsr->yd[j] * dsr->yd[j] + dsi->yd[j] * dsi->yd[j];
	  dsfr->yd[j] = tmp;
	  if (tmp > dsfr->yd[imax]) imax = j;
	} // the amplitude is stored in dsfr->yd
      j = find_max_around(dsfr->yd, dsfr->nx, imax, &Max_pos, &Max_val, NULL);

if (apoyes)
{
      for (j = 0;j < nx; j++) //fenetre
      {
      dsr->yd[j]=dsr->yd[j]*(float)exp(-pow(((double)j-(double)Max_pos),4)/(2*pow((double)aposize,4)));
    }
  }


    realtr1bt(fp, dsr->yd);  fftbt(fp, dsr->yd, 1);  realtr2bt(fp, dsr->yd, 1);
    //bandpass_smooth_half_bt (fil, dsr->nx, dsr->yd, bp_filter, bp_width);

    spectral_phase(dsr->yd,dstemp,n_2);

    unwrap_simple(dstemp->yd,n_2);

    if (sweep_frequencies == 0 && movie==0)    add_ds_to_op (opphase,dstemp);


    for (ii=0;ii<nx/2;ii++)
    {
      dstemp->xd[ii]=ii;

    }
    printf("main_mode=%f\n",main_mode);


    // dataset ye, xe. Barres d'erreurs. L'inverse de l'amplitude de Fourier.
    //compute_least_square_fit_on_ds_between_indexes(dstemp, 1, (int)(main_mode-fit_width),1+(int)(main_mode+fit_width),&out_am, &out_bm);
    fit_ds_to_xn_polynome(dstemp, degree+1, (int)(main_mode-fit_width), 1+(int)(main_mode+fit_width), -10000.0, 10000.0, &coeffs);
    phase=0;
    for (ii=0;ii<=degree;ii++)
    {
      phase+=(float)(coeffs[ii])*pow(main_mode,ii);

    }




    if (i==0)
      {
      phi_0=phase;
      r_0=Max_pos;
      }

  phi_approximate=phi_0-2*M_PI*(float)main_mode*(Max_pos-r_0)/nx;
  low=phi_approximate-M_PI;
  high=phi_approximate+M_PI;
  count_loop=0;
  while (phase>high)
  {
      count_loop++;
      phase-=2*M_PI;
      if (count_loop>1000)
      {
        win_printf("there was probably a problem with the fit. i=%d, a=%f, b=%f \n "
        "Continuing\n ", i,out_am, out_bm);
        continuing=1;
        break;
      }
    }
    if (continuing==1)
    {
      continuing=0;
      continue;
    }
  while (phase<low)
  {
      count_loop++;
      phase+=2*M_PI;
      if (count_loop>1000)
      {
        win_printf("there was probably a problem with the fit. i=%d a=%f, b=%f \n"
        "Continuing \n",i, out_am, out_bm);
        continuing=1;
        break;

      }
    }
    if (continuing==1)
    {
      continuing=0;
      continue;
    }
  final_position=r_0+(phase-phi_0)*nx/(-main_mode*2*M_PI);
  if (plot_max_pos==1)
  {
  dsmax->xd[i]=i;
  dsmax->yd[i]=Max_pos;
}
  ds->yd[i]=final_position;
  ds->xd[i]=i;

}

  set_ds_source(ds,"Martin algorithm for phase position, dg %d, pt %d, apo %d, wd %f,mm %f ,im%d\n",degree,fit_width,apoyes,aposize,main_mode,kim);

  if (sweep_frequencies != 0 || movie !=0)   free_data_set(dstemp);
}
}

  free_fft_plan_bt(fp);
  free_filter_bt(fil);

  remove_ds_from_op(op,dsfit);
  remove_ds_from_op(op,dsfi);
  remove_ds_from_op(op,dsfr);
  remove_ds_from_op(op,dsp);
  remove_ds_from_op(op,dsi);
  remove_ds_from_op(op,dst);
  remove_ds_from_op(op,dsr);



  broadcast_dialog_message(MSG_DRAW,0);
  return D_REDRAWME;
}


float phase_vincent()
{



}


d_s *track_one_fringes_set(d_s *dst, fft_plan *fp, filter *fil, int bp_filter, int bp_width, float phi0, float *Maxpos, float *phipos, float *dx2pi)
{
  register int  j;
  int nx, imax;
  static d_s *dsr = NULL, *dsi = NULL, *dsfr = NULL, *dsfi = NULL, *dsp = NULL, *dsfit = NULL;
  float amp, tmp, Max_pos, Max_val;
  double re, im, phi;
  double out_a = 0, out_b = 0, x = 0;

  if (dst == NULL || fp == NULL || fil == NULL) return NULL;
  nx = dst->nx;

  if (dsr == NULL || nx > dsr->mx)
    dsr = build_adjust_data_set(dsr,nx,nx);
  if (dsr == NULL) return NULL;
  if (dsi == NULL || nx > dsi->mx)
    dsi = build_adjust_data_set(dsi,nx,nx);
  if (dsi == NULL) return NULL;
  if (dsfr == NULL || nx > dsfr->mx)
    dsfr = build_adjust_data_set(dsfr,nx,nx);
  if (dsfr == NULL) return NULL;
  if (dsfi == NULL || nx > dsfi->mx)
    dsfi = build_adjust_data_set(dsfi,nx,nx);
  if (dsfi == NULL) return NULL;
  if (dsp == NULL || nx > dsp->mx)
    dsp = build_adjust_data_set(dsp,nx,nx);
  if (dsp == NULL) return NULL;
  if (dsfit == NULL || nx > dsfit->mx)
    dsfit = build_adjust_data_set(dsfit,nx,nx);
  if (dsfit == NULL) return NULL;
  alloc_data_set_y_error(dsfit);


  for (j = 0;j < nx; j++)
    {
      dsr->yd[j] = dst->xd[j];
      dsr->xd[j] = dsi->xd[j] = j;
    }
  // fft and bp dsr

  fftwindow_flat_top1(fp, nx, dsr->yd, 0);
  realtr1bt(fp, dsr->yd);  fftbt(fp, dsr->yd, 1);  realtr2bt(fp, dsr->yd, 1);
  bandpass_smooth_half_bt (fil, dsr->nx, dsr->yd, bp_filter, bp_width);

  for (j = bp_filter-2 * bp_width, amp = 0;j < bp_filter+2*bp_width; j++)
    {  // we compute the frequency barycenter
      if (j < 0 || j >= nx) continue;
      tmp = dsr->yd[2*j] * dsr->yd[2*j] + dsr->yd[2*j+1] * dsr->yd[2*j+1];
      //f0 += tmp * j;
      amp += tmp;
    }
  //if (amp > 0) f0 /= amp;
  for (j = 2;j < nx; j+=2)
    {  // we prepare imaginary part
      dsi->yd[j+1] = dsr->yd[j];
      dsi->yd[j] = -dsr->yd[j+1];
    }
  dsi->yd[1] = dsi->yd[0] = 0;

  /*	get back to complex world */
  realtr2bt(fp, dsr->yd, -1);  fftbt(fp, dsr->yd, -1); realtr1bt(fp, dsr->yd);
  realtr2bt(fp, dsi->yd, -1);  fftbt(fp, dsi->yd, -1); realtr1bt(fp, dsi->yd);

  for (j = 0;j < nx; j++)
    {  // we find the amplitude maximum and place the amplitude in dsfr
      tmp = sqrt(dsr->yd[j] * dsr->yd[j] + dsi->yd[j] * dsi->yd[j]);
      dsfr->yd[j] = tmp;
    } // the amplitude is stored in dsfr->yd
  for (j = 4, imax = 4;j < nx-4; j++)
    {  // we skip the border that are poluted by dewindowing
      if (dsfr->yd[j] > dsfr->yd[imax]) imax = j;
    } // the amplitude is stored in dsfr->yd
  j = find_max_around(dsfr->yd, dsfr->nx, imax, &Max_pos, &Max_val, NULL);
  *Maxpos = Max_pos; // this is the amplitude max position



  j = imax;
  re = dsr->yd[j];
  im = dsi->yd[j];
  phi = (im == 0.0 && re == 0.0) ? 0.0 : atan2(im, re); // phase in j
  dsfi->yd[j] = dsfi->xd[j] = phi;
  //win_printf("1");
  for (j = imax+1;j < nx; j++)
    {
      re = dsr->yd[j] * dsr->yd[j-1] + dsi->yd[j] * dsi->yd[j-1];
      im = dsi->yd[j] * dsr->yd[j-1] - dsr->yd[j] * dsi->yd[j-1];
      phi += (im == 0.0 && re == 0.0) ? 0.0 : atan2(im, re);
      dsfi->yd[j] = dsfi->xd[j] = phi;
    }
  //win_printf("2");
  phi = dsfi->yd[imax];
  for (j = imax-1;j >= 0; j--)
    {
      re = dsr->yd[j+1] * dsr->yd[j] + dsi->yd[j+1] * dsi->yd[j];
      im = dsi->yd[j+1] * dsr->yd[j] - dsr->yd[j+1] * dsi->yd[j];
      phi -= (im == 0.0 && re == 0.0) ? 0.0 : atan2(im, re);
      dsfi->yd[j] = dsfi->xd[j] = phi;
    }
  //win_printf("3");
  for (j = 0, dsfit->nx = 0;j < nx; j++)
    {
      dsp->yd[j] = dsfi->yd[j];
      if (3*dsfr->yd[j] > Max_val) // we keep phase if amp^2 > max/2
	{
	  dsfit->yd[dsfit->nx] = dsfi->yd[j];
	  dsfit->ye[dsfit->nx] = (tmp > 0) ? ((float)1)/dsfr->yd[j] : 10;
	  dsfit->xd[dsfit->nx++] = j;
	}
    }

  for (j = 0; j < dsfit->nx; j++) // appodisation
    dsfit->ye[j] *= (float)2/(1.05 - cos(2*j*M_PI/dsfit->nx));
  if (dsfit->nx < 3)       display_title_message("Fitting over %d max val %g at %g",dsfit->nx,Max_val,Max_pos);
  else
    {
      if (compute_least_square_fit_on_ds(dsfit, 1, &out_a, &out_b))
	{
	  display_title_message("fitting problem");
	}
    }
  if (out_a != 0)
    {
      for (*phipos = x = phi0 - (out_b-(4*M_PI))/out_a; x < (double)nx; x -= (2*M_PI)/out_a)
	{
	  //win_printf("x = %g max_pas %g",x, Max_pos);
	  if (fabs(x - Max_pos) < fabs(*phipos - Max_pos))
	    *phipos = x;
	}
      *dx2pi = (2*M_PI)/out_a;
    }
  else *phipos = 0;
  *Maxpos = Max_pos;
  for (j = 0;j < nx; j++)
    {
      dsfr->yd[j] = dsr->yd[j];
      dsfr->xd[j] = dsi->yd[j];
    }
  return dsfr;
}



int	grab_position_from_shear_profiles_from_movie_3(void)
{
  int i, j, k, cl;//, cw;
  //int snx = 0, sny = 0;
  int  nfi, ROI_set, lx0, lnx, ly0, lny, size, imax = 0 ,color, lx0s, ly0s;
  O_i *ois;
  O_p *op = NULL;
  d_s *dstmpt = NULL, *dstmpb = NULL, *dstmp2 = NULL, *dsx = NULL, *dsy = NULL, *dsz = NULL, *dsphi2 = NULL, *dsza = NULL, *dsxa = NULL, *dst = NULL, *dsfr = NULL, *dsy2 = NULL;
  d_s *dsymb = NULL, *dsymt = NULL;
  imreg *imr;
  float zero = 0, noise, max, dx = 0, dy = 0, dz = 0, maxt, maxb, maxp;
  unsigned long dt;
  static int x0 = 0, nx = 64, y0 = 0, ny = 128, dpx = 32, mouse_xy = 0, track_xy = 0;
  static int start = 0, end = 1,  timing = 0;
  static float black_l = 190, phi0t = 0, phi0b = 0;
  static int bp_filter = 7, bp_width = 3, lp2_filter = 3, n_phi2 = 3, h_size = 5;
  filter *filbp = NULL, *fillp2 = NULL;
  fft_plan *fp = NULL, *fp2 = NULL;
  DIALOG *dia = NULL;
  float Mpost = 0, phipost = 0, Mposb = 0, phiposb = 0, dx2pit = 0, dx2pib = 0, tmp;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) < 1)	return OFF;
  nfi = abs(ois->im.n_f);
  nfi = (nfi == 0) ? 1 : nfi;
  if(updating_menu_state != 0)
    {
      if (nfi <= 1) 	 active_menu->flags |=  D_DISABLED;
      else               active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }

  end = nfi;
  ROI_set = (ois->im.nxs > 0 || ois->im.nys > 0 || ois->im.nxe < ois->im.nx || ois->im.nye < ois->im.ny) ? 1 : 0;

  if (ROI_set)
    {
      lx0 = ois->im.nxs; lnx = ois->im.nxe - ois->im.nxs;
      ly0 = ois->im.nys; lny = ois->im.nye - ois->im.nys;
    }
  else
    {
      lx0 = x0; lnx = nx; ly0 = y0; lny = ny;
    }
  color = makecol(255,64,64);
  i = win_scanf("Construction of shear profiles of offset interferometry images\n"
		"Specify the center and size of the sub image in the movie:\n"
		"Xc %6d  Yc %6d or select by mouse %b\n"
		"Size in X %6d Distance between profiles %6d Size in Y %6d\n"
		"half size of shear avg %3d\n"
		"In the shear direction Band-pass filter freq %6d width %6d \n"
		"Phase offset for fringes at the top %5f at the bottom %5f\n"
		"Perpendicular to shear direction Low-pass filter freq %6d \n"
		"Phase zero will be search of 2n+1 points n=%3d\n"
		"Grab profiles starting at frame %6d ending at %6d\n"
		"Black level %4f track XY %b timing %b\n"
		,&lx0,&ly0,&mouse_xy,&lnx,&dpx,&lny,&h_size,&bp_filter,&bp_width,&phi0t
		,&phi0b,&lp2_filter,&n_phi2,&start,&end,&black_l,&track_xy,&timing);
  if (i == WIN_CANCEL) return  D_O_K;



  cl = lnx;
  //cw = (sw)?lnx:lny;
  if (mouse_xy)
    place_tracking_shear_parren_with_mouse_movie(&lx0, &ly0, cl, 2*dpx, h_size, 0, 0, color);
  //place_tracking_shear_parren_with_mouse_movie(&lx0, &ly0, cl, cw, h_size, sw, 0, color);


  size = end - start;
  size = (size > nfi) ? nfi : size; // ois->im.ny
  op = create_and_attach_op_to_oi(ois, size, size, 0, 0);
  if (op == NULL) return (win_printf_OK("cant create plot!"));
  dsx = op->dat[0];
  dsy = create_and_attach_one_ds(op, size, size, 0);
  if (dsy == NULL) return (win_printf_OK("cant create plot!"));
  dsz = create_and_attach_one_ds(op, size, size, 0);
  if (dsz == NULL) return (win_printf_OK("cant create plot!"));
  dsxa = create_and_attach_one_ds(op, size, size, 0);
  if (dsxa == NULL) return (win_printf_OK("cant create plot!"));
  dsza = create_and_attach_one_ds(op, size, size, 0);
  if (dsza == NULL) return (win_printf_OK("cant create plot!"));
  set_plot_title(op,"(%d to %d)",start, end);
  set_ds_treatement(dsx,"Track mean position at X0 = %d size %d Y0 %d size %d, avg_size %d,"
		    " Bp_shear_filter %d, Bp_shear_width %d, lp2_filter %d n_phi2 %d, black %g"
		    ,lx0,lnx,ly0,lny,h_size,bp_filter,bp_width,lp2_filter,n_phi2,black_l);
  set_ds_treatement(dsy,"Track mean position at X0 = %d size %d Y0 %d size %d, avg_size %d,"
		    " Bp_shear_filter %d, Bp_shear_width %d, lp2_filter %d n_phi2 %d, black %g"
		    ,lx0,lnx,ly0,lny,h_size,bp_filter,bp_width,lp2_filter,n_phi2,black_l);
  set_ds_treatement(dsz,"Track mean position at X0 = %d size %d Y0 %d size %d, avg_size %d,"
		    " Bp_shear_filter %d, Bp_shear_width %d, lp2_filter %d n_phi2 %d, black %g"
		    ,lx0,lnx,ly0,lny,h_size,bp_filter,bp_width,lp2_filter,n_phi2,black_l);
  inherit_from_im_to_ds(dsx, ois);
  inherit_from_im_to_ds(dsy, ois);
  inherit_from_im_to_ds(dsz, ois);
  uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
  uns_oi_2_op(ois, IS_T_UNIT_SET, op, IS_Y_UNIT_SET);
  op->filename = strdup(ois->filename);

  dsphi2 = build_data_set(2*n_phi2+1, 2*n_phi2+1);
  dst = build_data_set(lnx, lnx);
  dsy2 = build_data_set(lny, lny);

  fp = fftbt_init(fp, lnx);
  filbp = filter_init_bt(filbp, lnx);
  fp2 = fftbt_init(fp2, lny);
  fillp2 = filter_init_bt(fillp2, lny);
  dia = find_dialog_associated_to_imr(imr, NULL);
  dsymb = build_data_set(4*h_size, 4*h_size);
  dsymt = build_data_set(4*h_size, 4*h_size);

  if (dstmp2 == NULL) dstmp2 = build_data_set(lny,lny);
  if (dstmp2 == NULL) return win_printf_OK("cannot create ds !");
  if (dstmpt == NULL) dstmpt = build_data_set(lnx,lnx);
  if (dstmpb == NULL) dstmpb = build_data_set(lnx,lnx);
  if (dstmpt == NULL || dstmpb == NULL) return win_printf_OK("cannot create ds !");

  lx0s = lx0; ly0s = ly0;
  dt = my_uclock();
  for (i = start; i < end; i++)
    {
      switch_frame(ois,i);
      dsx->yd[i-start] =  dsy->yd[i-start] = dsz->yd[i-start] = dsxa->yd[i-start] = dsza->yd[i-start] = i;
      // we track in direction perpendicular to shear
      for (k=0 ; k < lny ; k++) dstmp2->yd[k] = 0;
      for (j = 0 ; j< lnx ; j++)
	{
	  extract_raw_partial_row(ois, lx0+j-lnx/2, dstmp2->xd,ly0-lny/2,lny);
	  for (k=0 ; k < lny ; k++)
	    dstmp2->yd[k] += dstmp2->xd[k] - black_l;
	}
      for (k=0 ; k < lny ; k++)
	dsy2->yd[k] = dstmp2->yd[k];
      correlate_1d_sig_and_invert_lp_and_complexify(dstmp2->yd, lny, 0, dstmp2->xd, dstmp2->yd,
						    lp2_filter, 1, fp2, fillp2, &noise);
      for (j = lny/4, imax = -1; j < 3*lny/4; j++)
	max = ((imax < 0) || (dstmp2->xd[j] > max)) ? dstmp2->xd[imax = j] : max;
      for (j = 0; j <= 2*n_phi2; j++)
	{
	  k = imax - n_phi2 + j;
	  k += (k < 0) ? lny : 0;
	  k = (k < lny) ? k : k - lny;
	  dsphi2->yd[j] = dstmp2->yd[k];
	  dsphi2->xd[j] = j - n_phi2;
	}
      find_phase_zero_with_poly(dsphi2, &zero);
      dy = ((zero + imax) - lny/2)/2;
      if (track_xy) dsy->xd[i-start] = ly0 + dy;
      else dsy->xd[i-start] = dy;
      // we track in shear direction
      for (k=0 ; k < lnx ; k++)	dstmpt->xd[k] = dstmpb->xd[k] = 0;
      //for (j = lny/2 - dpx/2 - h_size; j <= lny/2 - dpx/2 + h_size ; j++)
      for (j = dpx/2 - h_size; j <= dpx/2 + h_size ; j++)
	{
	  if (j < 0 || j >= lny/2) continue;
	  extract_raw_partial_line(ois, ly0-j-1, dstmpt->yd,lx0-lnx/2,lnx);
	  extract_raw_partial_line(ois, ly0+j, dstmpb->yd,lx0-lnx/2,lnx);
	  for (k=0 ; k < lnx ; k++)
	    {
	      dstmpt->xd[k] += dstmpt->yd[k] - black_l;
	      dstmpb->xd[k] += dstmpb->yd[k] - black_l;
	    }
	}

      dsfr = track_one_fringes_set(dstmpt, fp, filbp, bp_filter, bp_width, phi0t, &Mpost, &phipost, &dx2pit);


      for (j = dpx/2 - 2*h_size, dsymt->nx = 0; j <= dpx/2 + 2*h_size ; j++)
	{
	  if (j < 0 || j >= lny/2) continue;
	  extract_raw_partial_line(ois, ly0-j-1, dstmpt->yd,lx0-lnx/2,lnx);
	  for (k=0, dsymt->yd[dsymt->nx] = 0, tmp = 0; k < lnx ; k++)
	    {
	      tmp += dsfr->yd[k] * dsfr->yd[k];
	      dsymt->yd[dsymt->nx] += dsfr->yd[k] * dstmpt->yd[k];
	    }
	  if (tmp > 0) dsymt->yd[dsymt->nx] /= tmp;
	  dsymt->xd[dsymt->nx] = j - dpx/2;
	  dsymt->nx++;
	}
      for (j = 0; dsfr != NULL && j < lnx; j++)
	{
	  dst->yd[j] = sqrt(dsfr->yd[j]*dsfr->yd[j] + dsfr->xd[j]*dsfr->xd[j]);
	  maxt = (j == 0 || dst->yd[j] > maxt)? dst->yd[j] : maxt;
	}
      dsfr = track_one_fringes_set(dstmpb, fp, filbp, bp_filter, bp_width, phi0b, &Mposb, &phiposb, &dx2pib);
      for (j = dpx/2 - 2*h_size, dsymb->nx = 0; j <= dpx/2 + 2*h_size ; j++)
	{
	  if (j < 0 || j >= lny/2) continue;
	  extract_raw_partial_line(ois, ly0+j, dstmpb->yd,lx0-lnx/2,lnx);
	  for (k=0, dsymb->yd[dsymb->nx] = 0, tmp = 0; k < lnx ; k++)
	    {
	      tmp += dsfr->yd[k] * dsfr->yd[k];
	      dsymb->yd[dsymb->nx] += dsfr->yd[k] * dstmpb->yd[k];
	    }
	  if (tmp > 0) dsymb->yd[dsymb->nx] /= tmp;
	  dsymb->xd[dsymb->nx] = j - dpx/2;
	  dsymb->nx++;
	}
      for (j = 0; dsfr != NULL && j < lnx; j++)
	{
	  dst->xd[j] = sqrt(dsfr->yd[j]*dsfr->yd[j] + dsfr->xd[j]*dsfr->xd[j]);
	  maxb = (j == 0 || dst->xd[j] > maxb)? dst->xd[j] : maxb;
	}
      for (j = 0, maxp = dsy2->yd[j]; j < lny; j++)
	{
	  maxp = (dsy2->yd[j] > maxp)? dsy2->yd[j] : maxp;
	  dsy2->xd[j] = j;
	}

      dx =  (phipost + phiposb - lnx)/2;
      if (track_xy)
	dsx->xd[i-start] = dx + lx0;
      else dsx->xd[i-start] = dx;
      dx =  (Mpost + Mposb - lnx)/2;
      dsxa->xd[i-start] = dx;
      dz = phiposb - phipost;
      dsz->xd[i-start] = dz;
      dz = Mpost - Mposb;
      dsza->xd[i-start] = dz;
      if (track_xy)
	{
	  lx0 += (int)(dx + 0.5);
	  ly0 += (int)(dy + 0.5);
	  refresh_screen_image_and_shear_rect_w_profiles(imr, ois, dia, lx0, ly0, (int)(0.5+dz),
							 cl, 2*dpx, h_size, 0, color,dst->xd,maxb,dst->yd,maxt,dsy2->yd,maxp,lny);
	}
    }
  lx0 = lx0s; ly0 = ly0s;
  dt = my_uclock() - dt;
  if (timing)  win_printf("took %g ms",1000*(double)dt/get_my_uclocks_per_sec());
  if (ROI_set == 0)
    {
      x0 = lx0; nx = lnx; y0 = ly0; ny = lny;
    }
  free_data_set(dstmpt);
  free_data_set(dstmpb);
  //free_data_set(dstmp2);
  add_one_plot_data(op, IS_DATA_SET, (void*)dsy2);
  add_one_plot_data(op, IS_DATA_SET, (void*)dsymt);
  add_one_plot_data(op, IS_DATA_SET, (void*)dsymb);
  free_fft_plan_bt(fp);
  free_filter_bt(filbp);
  free_fft_plan_bt(fp2);
  free_filter_bt(fillp2);
  return refresh_image(imr,UNCHANGED);
}

//int	draw_moving_shear_rect_vga_screen_unit_fitInter_w_profiles(int xc, int yc, int length, int width, int h_size, int shear_in_y, int dz, int color, BITMAP* bm, float scx, float scy, float *ampt, float maxt, float *ampb, float maxb, float *profyt, float maxproft, float *profyb, float maxprofb, int lny)

int	grab_position_from_moving_shear_profiles_from_movie(void)
{
  int i, j, k, cl;//, cw;
  //int snx = 0, sny = 0;
  int  nfi, ROI_set, lx0, lnx, ly0, lny, size, imax = 0 ,color, lx0s, ly0s;
  O_i *ois;
  O_p *op = NULL;
  d_s *dstmpt = NULL, *dstmpb = NULL, *dstmp2 = NULL, *dsx = NULL, *dsy = NULL, *dsz = NULL, *dsphi2 = NULL, *dsza = NULL, *dsxa = NULL, *dst = NULL, *dsfr = NULL, *dsy2 = NULL;
  d_s *dsymb = NULL, *dsymt = NULL;
  imreg *imr;
  float zero = 0, noise, max, dx = 0, dy = 0, dz = 0, maxt, maxb, maxp;
  unsigned long dt;
  int idz_1 = 0; // the previous dz shift in int
  static int x0 = 0, nx = 128, y0 = 0, ny = 64, dpx = 32, mouse_xy = 1, track_xy = 1;
  static int start = 0, end = 1,  timing = 0;
  static float black_l = 20, phi0t = 0, phi0b = 3;
  static int bp_filter = 13, bp_width = 6, lp2_filter = 3, n_phi2 = 3, h_size = 5;
  filter *filbp = NULL, *fillp2 = NULL;
  fft_plan *fp = NULL, *fp2 = NULL;
  DIALOG *dia = NULL;
  float Mpost = 0, phipost = 0, Mposb = 0, phiposb = 0, dx2pit = 0, dx2pib = 0, tmp;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) < 1)	return OFF;
  nfi = abs(ois->im.n_f);
  nfi = (nfi == 0) ? 1 : nfi;
  if(updating_menu_state != 0)
    {
      if (nfi <= 1) 	 active_menu->flags |=  D_DISABLED;
      else               active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }

  end = nfi;
  ROI_set = (ois->im.nxs > 0 || ois->im.nys > 0 || ois->im.nxe < ois->im.nx || ois->im.nye < ois->im.ny) ? 1 : 0;

  if (ROI_set)
    {
      lx0 = ois->im.nxs; lnx = ois->im.nxe - ois->im.nxs;
      ly0 = ois->im.nys; lny = ois->im.nye - ois->im.nys;
    }
  else
    {
      lx0 = x0; lnx = nx; ly0 = y0; lny = ny;
    }
  color = makecol(255,64,64);
  i = win_scanf("Construction of shear profiles of offset interferometry images\n"
		"Specify the center and size of the sub image in the movie:\n"
		"Xc %6d  Yc %6d or select by mouse %b\n"
		"Size in X %6d Distance between profiles %6d Size in Y %6d\n"
		"half size of shear avg %3d\n"
		"In the shear direction Band-pass filter freq %6d width %6d \n"
		"Phase offset for fringes at the top %5f at the bottom %5f\n"
		"Perpendicular to shear direction Low-pass filter freq %6d \n"
		"Phase zero will be search of 2n+1 points n=%3d\n"
		"Grab profiles starting at frame %6d ending at %6d\n"
		"Black level %4f track XY %b timing %b\n"
		,&lx0,&ly0,&mouse_xy,&lnx,&dpx,&lny,&h_size,&bp_filter,&bp_width,&phi0t
		,&phi0b,&lp2_filter,&n_phi2,&start,&end,&black_l,&track_xy,&timing);
  if (i == WIN_CANCEL) return  D_O_K;



  cl = lnx;
  //cw = (sw)?lnx:lny;
  if (mouse_xy)
    place_tracking_shear_parren_with_mouse_movie(&lx0, &ly0, cl, 2*dpx, h_size, 0, 0, color);
  //place_tracking_shear_parren_with_mouse_movie(&lx0, &ly0, cl, cw, h_size, sw, 0, color);

  win_printf("lx0 = %d ly0 = %d",lx0,ly0);
  size = end - start;
  size = (size > nfi) ? nfi : size; // ois->im.ny
  op = create_and_attach_op_to_oi(ois, size, size, 0, 0);
  if (op == NULL) return (win_printf_OK("cant create plot!"));
  dsx = op->dat[0];
  dsy = create_and_attach_one_ds(op, size, size, 0);
  if (dsy == NULL) return (win_printf_OK("cant create plot!"));
  dsz = create_and_attach_one_ds(op, size, size, 0);
  if (dsz == NULL) return (win_printf_OK("cant create plot!"));
  dsxa = create_and_attach_one_ds(op, size, size, 0);
  if (dsxa == NULL) return (win_printf_OK("cant create plot!"));
  dsza = create_and_attach_one_ds(op, size, size, 0);
  if (dsza == NULL) return (win_printf_OK("cant create plot!"));
  set_plot_title(op,"(%d to %d)",start, end);
  set_ds_treatement(dsx,"Track mean position at X0 = %d size %d Y0 %d size %d, avg_size %d,"
		    " Bp_shear_filter %d, Bp_shear_width %d, lp2_filter %d n_phi2 %d, black %g"
		    ,lx0,lnx,ly0,lny,h_size,bp_filter,bp_width,lp2_filter,n_phi2,black_l);
  set_ds_treatement(dsy,"Track mean position at X0 = %d size %d Y0 %d size %d, avg_size %d,"
		    " Bp_shear_filter %d, Bp_shear_width %d, lp2_filter %d n_phi2 %d, black %g"
		    ,lx0,lnx,ly0,lny,h_size,bp_filter,bp_width,lp2_filter,n_phi2,black_l);
  set_ds_treatement(dsz,"Track mean position at X0 = %d size %d Y0 %d size %d, avg_size %d,"
		    " Bp_shear_filter %d, Bp_shear_width %d, lp2_filter %d n_phi2 %d, black %g"
		    ,lx0,lnx,ly0,lny,h_size,bp_filter,bp_width,lp2_filter,n_phi2,black_l);
  inherit_from_im_to_ds(dsx, ois);
  inherit_from_im_to_ds(dsy, ois);
  inherit_from_im_to_ds(dsz, ois);
  uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
  uns_oi_2_op(ois, IS_T_UNIT_SET, op, IS_Y_UNIT_SET);
  op->filename = strdup(ois->filename);

  dsphi2 = build_data_set(2*n_phi2+1, 2*n_phi2+1);
  dst = build_data_set(lnx, lnx);
  dsy2 = build_data_set(lny, lny);

  fp = fftbt_init(fp, lnx);
  filbp = filter_init_bt(filbp, lnx);
  fp2 = fftbt_init(fp2, lny);
  fillp2 = filter_init_bt(fillp2, lny);
  dia = find_dialog_associated_to_imr(imr, NULL);
  dsymb = build_data_set(4*h_size, 4*h_size);
  dsymt = build_data_set(4*h_size, 4*h_size);

  if (dstmp2 == NULL) dstmp2 = build_data_set(lny,lny);
  if (dstmp2 == NULL) return win_printf_OK("cannot create ds !");
  if (dstmpt == NULL) dstmpt = build_data_set(lnx,lnx);
  if (dstmpb == NULL) dstmpb = build_data_set(lnx,lnx);
  if (dstmpt == NULL || dstmpb == NULL) return win_printf_OK("cannot create ds !");

  lx0s = lx0; ly0s = ly0;
  dt = my_uclock();
  for (i = start; i < end; i++)
    {
      switch_frame(ois,i);
      dsx->yd[i-start] =  dsy->yd[i-start] = dsz->yd[i-start] = dsxa->yd[i-start] = dsza->yd[i-start] = i;
      // we track in direction perpendicular to shear
      for (k=0 ; k < lny ; k++) dstmp2->yd[k] = 0;
      for (j = 0 ; j< lnx ; j++)
	{
	  extract_raw_partial_row(ois, lx0+j-lnx/2, dstmp2->xd,ly0-lny/2,lny);
	  for (k=0 ; k < lny ; k++)
	    dstmp2->yd[k] += dstmp2->xd[k] - black_l;
	}
      for (k=0 ; k < lny ; k++)
	dsy2->yd[k] = dstmp2->yd[k];
      correlate_1d_sig_and_invert_lp_and_complexify(dstmp2->yd, lny, 0, dstmp2->xd, dstmp2->yd,
						    lp2_filter, 1, fp2, fillp2, &noise);
      for (j = lny/4, imax = -1; j < 3*lny/4; j++)
	max = ((imax < 0) || (dstmp2->xd[j] > max)) ? dstmp2->xd[imax = j] : max;
      for (j = 0; j <= 2*n_phi2; j++)
	{
	  k = imax - n_phi2 + j;
	  k += (k < 0) ? lny : 0;
	  k = (k < lny) ? k : k - lny;
	  dsphi2->yd[j] = dstmp2->yd[k];
	  dsphi2->xd[j] = j - n_phi2;
	}
      find_phase_zero_with_poly(dsphi2, &zero);
      dy = ((zero + imax) - lny/2)/2;
      if (track_xy) dsy->xd[i-start] = ly0 + dy;
      else dsy->xd[i-start] = dy;

      // we track in shear direction
      for (k=0 ; k < lnx ; k++)	dstmpt->xd[k] = dstmpb->xd[k] = 0;
      //for (j = lny/2 - dpx/2 - h_size; j <= lny/2 - dpx/2 + h_size ; j++)
      for (j = dpx/2 - h_size; j <= dpx/2 + h_size ; j++)
	{
	  if (j < 0 || j >= lny/2) continue;
	  extract_raw_partial_line(ois, ly0+j, dstmpt->yd,lx0+(idz_1/2)-(lnx/2),lnx);
	  extract_raw_partial_line(ois, ly0-j-1, dstmpb->yd,lx0-(idz_1/2)-lnx/2,lnx);
	  for (k=0 ; k < lnx ; k++)
	    {
	      dstmpt->xd[k] += dstmpt->yd[k] - black_l;
	      dstmpb->xd[k] += dstmpb->yd[k] - black_l;
	    }
	}

      dsfr = track_one_fringes_set(dstmpt, fp, filbp, bp_filter, bp_width, phi0t, &Mpost, &phipost, &dx2pit);


      for (j = dpx/2 - 2*h_size, dsymt->nx = 0; j <= dpx/2 + 2*h_size ; j++)
	{
	  if (j < 0 || j >= lny/2) continue;
	  extract_raw_partial_line(ois, ly0+j, dstmpt->yd,lx0+(idz_1/2)-lnx/2,lnx);
	  for (k=0, dsymt->yd[dsymt->nx] = 0, tmp = 0; k < lnx ; k++)
	    {
	      tmp += dsfr->yd[k] * dsfr->yd[k];
	      dsymt->yd[dsymt->nx] += dsfr->yd[k] * dstmpt->yd[k];
	    }
	  if (tmp > 0) dsymt->yd[dsymt->nx] /= tmp;
	  dsymt->xd[dsymt->nx] = j - dpx/2;
	  dsymt->nx++;
	}
      for (j = 0; dsfr != NULL && j < lnx; j++)
	{
	  dst->yd[j] = sqrt(dsfr->yd[j]*dsfr->yd[j] + dsfr->xd[j]*dsfr->xd[j]);
	  maxt = (j == 0 || dst->yd[j] > maxt)? dst->yd[j] : maxt;
	}
      dsfr = track_one_fringes_set(dstmpb, fp, filbp, bp_filter, bp_width, phi0b, &Mposb, &phiposb, &dx2pib);
      for (j = dpx/2 - 2*h_size, dsymb->nx = 0; j <= dpx/2 + 2*h_size ; j++)
	{
	  if (j < 0 || j >= lny/2) continue;
	  extract_raw_partial_line(ois, ly0-j-1, dstmpb->yd,lx0-(idz_1/2)-lnx/2,lnx);
	  for (k=0, dsymb->yd[dsymb->nx] = 0, tmp = 0; k < lnx ; k++)
	    {
	      tmp += dsfr->yd[k] * dsfr->yd[k];
	      dsymb->yd[dsymb->nx] += dsfr->yd[k] * dstmpb->yd[k];
	    }
	  if (tmp > 0) dsymb->yd[dsymb->nx] /= tmp;
	  dsymb->xd[dsymb->nx] = j - dpx/2;
	  dsymb->nx++;
	}
      for (j = 0; dsfr != NULL && j < lnx; j++)
	{
	  dst->xd[j] = sqrt(dsfr->yd[j]*dsfr->yd[j] + dsfr->xd[j]*dsfr->xd[j]);
	  maxb = (j == 0 || dst->xd[j] > maxb)? dst->xd[j] : maxb;
	}
      for (j = 0, maxp = dsy2->yd[j]; j < lny; j++)
	{
	  maxp = (dsy2->yd[j] > maxp)? dsy2->yd[j] : maxp;
	  dsy2->xd[j] = j;
	}
      //win_printf("Bottom pos %g %g\nTop ps %g %g\n",phiposb,Mposb, phipost,Mpost);
      dx =  (phipost + phiposb - lnx)/2;
      if (track_xy)
	dsx->xd[i-start] = dx + lx0;
      else dsx->xd[i-start] = dx;
      dx =  (Mpost + Mposb - lnx)/2;
      dsxa->xd[i-start] = dx;
      dz = phiposb - phipost + idz_1;
      dsz->xd[i-start] = dz;
      dz = Mpost - Mposb + idz_1;
      dsza->xd[i-start] = dz;
      idz_1 = (int)(0.5+dz);
      idz_1 = (idz_1 > cl/4) ? cl/4 : idz_1;
      idz_1 = (idz_1 < -cl/4) ? -cl/4 : idz_1;
      if (track_xy)
	{
	  lx0 += (int)(dx + 0.5);
	  ly0 += (int)(dy + 0.5);

	  refresh_screen_image_and_moving_shear_rect_w_profiles(imr, ois, dia, lx0, ly0, idz_1, cl, dpx, h_size
								, color, dst->xd, dst->yd, dsymt->yd, dsymb->yd, dsymt->nx);


	}
    }
  lx0 = lx0s; ly0 = ly0s;
  dt = my_uclock() - dt;
  if (timing)  win_printf("took %g ms",1000*(double)dt/get_my_uclocks_per_sec());
  if (ROI_set == 0)
    {
      x0 = lx0; nx = lnx; y0 = ly0; ny = lny;
    }
  free_data_set(dstmpt);
  free_data_set(dstmpb);
  //free_data_set(dstmp2);
  add_one_plot_data(op, IS_DATA_SET, (void*)dsy2);
  add_one_plot_data(op, IS_DATA_SET, (void*)dsymt);
  add_one_plot_data(op, IS_DATA_SET, (void*)dsymb);
  free_fft_plan_bt(fp);
  free_filter_bt(filbp);
  free_fft_plan_bt(fp2);
  free_filter_bt(fillp2);
  return refresh_image(imr,UNCHANGED);
}

int find_sigma_peak_y(O_i *ois,int *pxg1,int *pxd1, int *pxg2, int *pxd2, float peak_ratio)
{

    if (ois == NULL || pxg1==NULL || pxd1==NULL || pxg2==NULL || pxd2==NULL) return 1;
    int ly=ois->im.ny;
    int lx=ois->im.nx;
    int i,j;
    d_s *dstmp2 = build_data_set(ly,ly);
    d_s *mean_y_profile = build_data_set(ly,ly);
    d_s *sum_y_profile = build_data_set(ly,ly);
    float coeff;
    int max_1=0,max_2=0;

    for (i=0;i<lx;i++)
    {

      extract_raw_partial_row(ois,i, dstmp2->yd,0,ly);
      for (j=0;j<ly;j++)
      {
          sum_y_profile->yd[j]+=dstmp2->yd[j];
      }
    }

    coeff=(float)(sum_y_profile->yd[ly]-sum_y_profile->yd[0])/(float)(ly-1);
    for (j=0;j<ly;j++)
      {
        sum_y_profile->yd[j]-=sum_y_profile->yd[0]+j*coeff;  //enleve la droite qui passe par 0
      }

    for (j=0;j<ly/2;j++)
    {
      max_1=(sum_y_profile->yd[j]>sum_y_profile->yd[max_1]) ? j : max_1;
    }
    *pxg1=max_1;
    *pxd1=max_1;
    for (j=0;sum_y_profile->yd[*pxg1]>peak_ratio*sum_y_profile->yd[max_1];)
    {
      *pxg1-=1;
    }
    for (j=0;sum_y_profile->yd[*pxd1]>peak_ratio*sum_y_profile->yd[max_1];)
    {
      *pxd1+=1;
    }


    for (j=ly/2;j<ly;j++)
    {
      max_2=(sum_y_profile->yd[j]>sum_y_profile->yd[max_2]) ? j : max_2;
    }
    *pxg2=max_2;
    *pxd2=max_2;
    for (j=0;sum_y_profile->yd[*pxg2]>peak_ratio*sum_y_profile->yd[max_2];)
    {
      *pxg2-=1;
    }
    for (j=0;sum_y_profile->yd[*pxd2]>peak_ratio*sum_y_profile->yd[max_2];)
    {
      *pxd2+=1;
    }
    free_data_set(mean_y_profile);
    free_data_set(dstmp2);

    return 0;
  }





//
// int	grab_position_from_shear_profiles_from_movie_3_Martin(void)
// {
//   int i, j, k, cl;//, cw;
//   //int snx = 0, sny = 0;
//   int  nfi, ROI_set, lx0, lnx, ly0, lny, size, imax = 0 ,color, lx0s, ly0s;
//   O_i *ois;
//   O_p *op = NULL;
//   d_s *dstmpt = NULL, *dstmpb = NULL, *dstmp2 = NULL, *dsx = NULL, *dsy = NULL, *dsz = NULL, *dsphi2 = NULL, *dsza = NULL, *dsxa = NULL, *dst = NULL, *dsfr = NULL, *dsy2 = NULL;
//   imreg *imr;
//   float zero = 0, noise, max, dx = 0, dy = 0, dz = 0, maxt, maxb, maxp;
//
//   unsigned long dt;
//   static int x0 = 0, nx = 64, y0 = 0, ny = 128, dpx = 32, mouse_xy = 0, track_xy = 0;
//   static int start = 0, end = 1,  timing = 0;
//   static float black_l = 190, phi0t = 0, phi0b = 0;
//   static int bp_filter = 7, bp_width = 3, lp2_filter = 3, n_phi2 = 3, h_size = 5;
//   filter *filbp = NULL, *fillp2 = NULL;
//   fft_plan *fp = NULL, *fp2 = NULL;
//   DIALOG *dia = NULL;
//   float Mpost = 0, phipost = 0, Mposb = 0, phiposb = 0, dx2pit = 0, dx2pib = 0,peak_ratio=0.5;
//   int pxg1,pxd1,pxg2,pxd2;
//
//   if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) < 1)	return OFF;
//   nfi = abs(ois->im.n_f);
//   nfi = (nfi == 0) ? 1 : nfi;
//   if(updating_menu_state != 0)
//     {
//       if (nfi <= 1) 	 active_menu->flags |=  D_DISABLED;
//       else               active_menu->flags &= ~D_DISABLED;
//       return D_O_K;
//     }
//
//   end = nfi;
//   ROI_set = (ois->im.nxs > 0 || ois->im.nys > 0 || ois->im.nxe < ois->im.nx || ois->im.nye < ois->im.ny) ? 1 : 0;
//
//   if (ROI_set)
//     {
//       lx0 = ois->im.nxs; lnx = ois->im.nxe - ois->im.nxs;
//       ly0 = ois->im.nys; lny = ois->im.nye - ois->im.nys;
//     }
//   else
//     {
//       lx0 = x0; lnx = nx; ly0 = y0; lny = ny;
//     }
//   color = makecol(255,64,64);
//
//
//
//   i = win_scanf("Construction of shear profiles of offset interferometry images\n"
// 		"Specify the center and size of the sub image in the movie:\n"
// 		"Xc %6d  Yc %6d or select by mouse %b\n"
// 		"Size in X %6d Distance between profiles %6d Size in Y %6d\n"
// 		"half size of shear avg %3d\n"
// 		"In the shear direction Band-pass filter freq %6d width %6d \n"
// 		"Phase offset for fringes at the top %5f at the bottom %5f\n"
// 		"Perpendicular to shear direction Low-pass filter freq %6d \n"
// 		"Phase zero will be search of 2n+1 points n=%3d\n"
// 		"Grab profiles starting at frame %6d ending at %6d\n"
// 		"Black level %4f track XY %b timing %b\n"
//     "Peak ratio %f\n"
// 		,&lx0,&ly0,&mouse_xy,&lnx,&dpx,&lny,&h_size,&bp_filter,&bp_width,&phi0t
// 		,&phi0b,&lp2_filter,&n_phi2,&start,&end,&black_l,&track_xy,&timing,&peak_ratio);
//
//
//
//   if (i == WIN_CANCEL) return  D_O_K;
//
//
//
//   cl = lnx;
//   //cw = (sw)?lnx:lny;
//
//   size = end - start;
//   size = (size > nfi) ? nfi : size; // ois->im.ny
//   op = create_and_attach_op_to_oi(ois, size, size, 0, 0);
//   if (op == NULL) return (win_printf_OK("cant create plot!"));
//   dsx = op->dat[0];
//   dsy = create_and_attach_one_ds(op, size, size, 0);
//   if (dsy == NULL) return (win_printf_OK("cant create plot!"));
//   dsz = create_and_attach_one_ds(op, size, size, 0);
//   if (dsz == NULL) return (win_printf_OK("cant create plot!"));
//   dsxa = create_and_attach_one_ds(op, size, size, 0);
//   if (dsxa == NULL) return (win_printf_OK("cant create plot!"));
//   dsza = create_and_attach_one_ds(op, size, size, 0);
//   if (dsza == NULL) return (win_printf_OK("cant create plot!"));
//   set_plot_title(op,"(%d to %d)",start, end);
//
//   set_ds_treatement(dsx,"Track mean position at X0 = %d size %d Y0 %d size %d, avg_size %d,"
// 		    " Bp_shear_filter %d, Bp_shear_width %d, lp2_filter %d n_phi2 %d, black %g"
// 		    ,lx0,lnx,ly0,lny,h_size,bp_filter,bp_width,lp2_filter,n_phi2,black_l);
//   set_ds_treatement(dsy,"Track mean position at X0 = %d size %d Y0 %d size %d, avg_size %d,"
// 		    " Bp_shear_filter %d, Bp_shear_width %d, lp2_filter %d n_phi2 %d, black %g"
// 		    ,lx0,lnx,ly0,lny,h_size,bp_filter,bp_width,lp2_filter,n_phi2,black_l);
//   set_ds_treatement(dsz,"Track mean position at X0 = %d size %d Y0 %d size %d, avg_size %d,"
// 		    " Bp_shear_filter %d, Bp_shear_width %d, lp2_filter %d n_phi2 %d, black %g"
// 		    ,lx0,lnx,ly0,lny,h_size,bp_filter,bp_width,lp2_filter,n_phi2,black_l);
//   inherit_from_im_to_ds(dsx, ois);
//   inherit_from_im_to_ds(dsy, ois);
//   inherit_from_im_to_ds(dsz, ois);
//   uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
//   uns_oi_2_op(ois, IS_T_UNIT_SET, op, IS_Y_UNIT_SET);
//   op->filename = strdup(ois->filename);
//
//   dsphi2 = build_data_set(2*n_phi2+1, 2*n_phi2+1);
//   dst = build_data_set(lnx, lnx);
//   dsy2 = build_data_set(lny, lny);
//
//   fp = fftbt_init(fp, im->nx);
//   filbp = filter_init_bt(filbp, lnx);
//   fp2 = fftbt_init(fp2, lny);
//   fillp2 = filter_init_bt(fillp2, lny);
//   dia = find_dialog_associated_to_imr(imr, NULL);
//
//
//   if (dstmp2 == NULL) dstmp2 = build_data_set(lny,lny);
//   if (dstmp2 == NULL) return win_printf_OK("cannot create ds !");
//   if (dstmpt == NULL) dstmpt = build_data_set(lnx,lnx);
//   if (dstmpb == NULL) dstmpb = build_data_set(lnx,lnx);
//   if (dstmpp == NULL) dstmppp = build_data_set(lnx,lnx);
//   if (dstmpt == NULL || dstmpb == NULL) return win_printf_OK("cannot create ds !");
//
//   lx0s = lx0; ly0s = ly0;
//   dt = my_uclock();
//   find_sigma_peak_y(ois,&pxg1,&pxd1,&pxg2,&pxd2,peak_ratio);
//
//     float out_a;
//   float out_b;
//
//   for (i = start; i < end; i++)
//
//     {
//       switch_frame(ois,i);
//       dsx->yd[i-start] =  dsy->yd[i-start] = dsz->yd[i-start] = dsxa->yd[i-start] = dsza->yd[i-start] = i;
//
//       // we track in direction perpendicular to shear
//       extract_x_averaged_on_y(ois,pxg1,pxd1,dstmp2->yd);
//
//       realtr1bt(fp, dstmp2->yd);
//       fftbt(fp, dstmp2->yd, 1);
//       realtr2bt(fp, dstmp2->yd, 1);
//
//       spectral_phase(fp,dstmpp->yd,im->nx);
//       unwrap_simple(dstmpp->yd,im->nx);
//       for (i=0;i<im->nx;i++)
//       {
//         dstmpp->xd=i;
//       }
//       compute_least_square_fit_on_ds_between_indexes(dstmpp, 1, 20,40,&out_a, &out_b);
//
//
//
//
//       for (k=0 ; k < lny ; k++) dstmp2->yd[k] = 0;
//       for (j = 0 ; j< lnx ; j++)
// 	     {
// 	        extract_raw_partial_row(ois, lx0+j-lnx/2, dstmp2->xd,ly0-lny/2,lny);
//
// 	        for (k=0 ; k < lny ; k++)
// 	           dstmp2->yd[k] += dstmp2->xd[k] - black_l;
// 	     }
//       for (k=0 ; k < lny ; k++)
// 	       dsy2->yd[k] = dstmp2->yd[k];
//       correlate_1d_sig_and_invert_lp_and_complexify(dstmp2->yd, lny, 0, dstmp2->xd, dstmp2->yd,
// 						    lp2_filter, 1, fp2, fillp2, &noise);
//       for (j = lny/4, imax = -1; j < 3*lny/4; j++)
// 	         max = ((imax < 0) || (dstmp2->xd[j] > max)) ? dstmp2->xd[imax = j] : max;
//       for (j = 0; j <= 2*n_phi2; j++)
// 	       {
// 	           k = imax - n_phi2 + j;
// 	           k += (k < 0) ? lny : 0;
// 	           k = (k < lny) ? k : k - lny;
// 	           dsphi2->yd[j] = dstmp2->yd[k];
// 	           dsphi2->xd[j] = j - n_phi2;
// 	       }
//       find_phase_zero_with_poly(dsphi2, &zero);
//       dy = ((zero + imax) - lny/2)/2;
//       if (track_xy) dsy->xd[i-start] = ly0 + dy;
//       else dsy->xd[i-start] = dy;
//       // we track in shear direction
//       for (k=0 ; k < lnx ; k++)	dstmpt->xd[k] = dstmpb->xd[k] = 0;
//       //for (j = lny/2 - dpx/2 - h_size; j <= lny/2 - dpx/2 + h_size ; j++)
//       for (j = dpx/2 - h_size; j <= dpx/2 + h_size ; j++)
// 	{
// 	  if (j < 0 || j >= lny/2) continue;
// 	  extract_raw_partial_line(ois, ly0-j-1, dstmpt->yd,lx0-lnx/2,lnx);
// 	  extract_raw_partial_line(ois, ly0+j, dstmpb->yd,lx0-lnx/2,lnx);
// 	  for (k=0 ; k < lnx ; k++)
// 	    {
// 	      dstmpt->xd[k] += dstmpt->yd[k] - black_l;
// 	      dstmpb->xd[k] += dstmpb->yd[k] - black_l;
// 	    }
// 	}
//
//       dsfr = track_one_fringes_set(dstmpt, fp, filbp, bp_filter, bp_width, phi0t, &Mpost, &phipost, &dx2pit);
//       for (j = 0, maxt = dsfr->yd[j]; dsfr != NULL && j < lnx; j++)
// 	{
// 	  dst->yd[j] = dsfr->yd[j];
// 	  maxt = (dsfr->yd[j] > maxt)? dsfr->yd[j] : maxt;
// 	}
//       dsfr = track_one_fringes_set(dstmpb, fp, filbp, bp_filter, bp_width, phi0b, &Mposb, &phiposb, &dx2pib);
//       for (j = 0, maxb = dsfr->yd[j]; dsfr != NULL && j < lnx; j++)
// 	{
// 	  dst->xd[j] = dsfr->yd[j];
// 	  maxb = (dsfr->yd[j] > maxb)? dsfr->yd[j] : maxb;
// 	}
//       for (j = 0, maxp = dsy2->yd[j]; j < lny; j++)
// 	{
// 	  maxp = (dsy2->yd[j] > maxp)? dsy2->yd[j] : maxp;
// 	}
//
//       dx =  (phipost + phiposb - lnx)/2;
//       if (track_xy)
// 	dsx->xd[i-start] = dx + lx0;
//       else dsx->xd[i-start] = dx;
//       dx =  (Mpost + Mposb - lnx)/2;
//       dsxa->xd[i-start] = dx;
//       dz = phipost - phiposb;
//       dsz->xd[i-start] = dz;
//       dz = Mpost - Mposb;
//       dsza->xd[i-start] = dz;
//       if (track_xy)
// 	{
// 	  lx0 += (int)(dx + 0.5);
// 	  ly0 += (int)(dy + 0.5);
// 	  refresh_screen_image_and_shear_rect_w_profiles(imr, ois, dia, lx0, ly0, (int)(0.5+dz),
// 							 cl, 2*dpx, h_size, 0, color,dst->xd,maxb,dst->yd,maxt,dsy2->yd,maxp,lny);
// 	}
//     }
//   lx0 = lx0s; ly0 = ly0s;
//   dt = my_uclock() - dt;
//   if (timing)  win_printf("took %g ms",1000*(double)dt/get_my_uclocks_per_sec());
//   if (ROI_set == 0)
//     {
//       x0 = lx0; nx = lnx; y0 = ly0; ny = lny;
//     }
//   free_data_set(dstmpt);
//   free_data_set(dstmpb);
//   //free_data_set(dstmp2);
//   add_one_plot_data(op, IS_DATA_SET, (void*)dsy2);
//   free_fft_plan_bt(fp);
//   free_filter_bt(filbp);
//   free_fft_plan_bt(fp2);
//   free_filter_bt(fillp2);
//   return refresh_image(imr,UNCHANGED);
// }
//
//


int do_bp_complexify_phi_ds(void)
{
  register int j, k;
  O_p  *op = NULL;
  d_s *ds, *dsr = NULL, *dsi = NULL, *dsfr = NULL, *dsfi = NULL, *dsp = NULL;
  pltreg *pr = NULL;
  float tmp, f0, amp, Max_pos, Max_val;
  double  re, im, phi, phit, phil, phim;
  fft_plan *fp = NULL;
  filter*fil = NULL;
  int imax, n_2;
  static int bp_f = 16, bp_w = 6;


  if(updating_menu_state != 0)	return D_O_K;

  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
    return win_printf_OK("cannot find data");

  fp = fftbt_init(fp, ds->nx);
  if (fp == NULL) 	return win_printf_OK("cannot init FFT !");
  fil = filter_init_bt(fil, ds->nx);
  if (fil == NULL) 	return win_printf_OK("cannot init filter !");


  j = win_scanf("Complexify after BP filter\nCenter of BP %6d width %6d\n",&bp_f,&bp_w);
  if (j == WIN_CANCEL) return 0;

  if ((dsr = create_and_attach_one_ds(op, ds->nx, ds->nx, 0)) == NULL)
    return win_printf_OK("cannot create plot !");

  if ((dsi = create_and_attach_one_ds(op, ds->nx, ds->nx, 0)) == NULL)
    return win_printf_OK("cannot create plot !");

  if ((dsfr = create_and_attach_one_ds(op, ds->nx, ds->nx, 0)) == NULL)
    return win_printf_OK("cannot create plot !");

  if ((dsfi = create_and_attach_one_ds(op, ds->nx, ds->nx, 0)) == NULL)
    return win_printf_OK("cannot create plot !");

  if ((dsp = create_and_attach_one_ds(op, ds->nx, ds->nx, 0)) == NULL)
    return win_printf_OK("cannot create plot !");

  for (j = 0;j < ds->nx; j++)
    {
      dsr->yd[j] = ds->yd[j];
      dsr->xd[j] = dsi->xd[j] = ds->xd[j];
    }

  realtr1bt(fp, dsr->yd);
  fftbt(fp, dsr->yd, 1);
  realtr2bt(fp, dsr->yd, 1);

  bandpass_smooth_half_bt (fil, dsr->nx, dsr->yd, bp_f, bp_w);

  for (j = bp_f-2 * bp_w, f0 = amp = 0;j < bp_f+2*bp_w; j++)
    {
      if (j < 0 || j >= ds->nx) continue;
      tmp = dsr->yd[2*j] * dsr->yd[2*j] + dsr->yd[2*j+1] * dsr->yd[2*j+1];
      f0 += tmp * j;
      amp += tmp;
    }
  if (amp > 0) f0 /= amp;
  for (j = 2;j < ds->nx; j+=2)
    {
      dsi->yd[j+1] = dsr->yd[j];
      dsi->yd[j] = -dsr->yd[j+1];
    }
  dsi->yd[1] = dsi->yd[0] = 0;

    /*	get back to complex world */
  realtr2bt(fp, dsr->yd, -1);
  fftbt(fp, dsr->yd, -1);
  realtr1bt(fp, dsr->yd);

  realtr2bt(fp, dsi->yd, -1);
  fftbt(fp, dsi->yd, -1);
  realtr1bt(fp, dsi->yd);

  for (j = 0, imax = 0;j < ds->nx; j++)
    {
      tmp = dsr->yd[j] * dsr->yd[j] + dsi->yd[j] * dsi->yd[j];
      dsfr->yd[j] = tmp;
      if (tmp > dsfr->yd[imax]) imax = j;
    }
  j = find_max_around(dsfr->yd, dsfr->nx, imax, &Max_pos, &Max_val, NULL);
  amp = sqrt(Max_val);
  n_2 = ds->nx/2;
  for (j = 0;j < ds->nx; j++)
    {
      dsfr->xd[j] = amp * cos((2*M_PI*(j-n_2)*bp_f)/ds->nx);
      dsfi->xd[j] = -amp * sin((2*M_PI*(j-n_2)*bp_f)/ds->nx);
    }

  re = dsr->yd[0];
  im = dsi->yd[0];
  phi = (im == 0.0 && re == 0.0) ? 0.0 : atan2(im, re);
  dsp->yd[0] = dsfi->yd[0] = dsfi->xd[0] = phi;
  for (j = 1, phit = phim = 0, amp = 0;j < ds->nx; j++)
    {
      re = dsr->yd[j] * dsr->yd[j-1] + dsi->yd[j] * dsi->yd[j-1];
      im = dsi->yd[j] * dsr->yd[j-1] - dsr->yd[j] * dsi->yd[j-1];
      phi += (im == 0.0 && re == 0.0) ? 0.0 : atan2(im, re);
      dsfi->xd[j] = phi;
      re = dsr->yd[j];
      im = dsi->yd[j];
      phil = (im == 0.0 && re == 0.0) ? 0.0 : atan2(im, re);
      for (k = 0; phi - phil - phit > 1; k++, phit += 2*M_PI);
      if (k > 1) win_printf("k = %d j=%d -> phi 1 %g phi 2 -> %g dphi %g",k,j,phi,phil+phit,phil+phit-phi);
      for (k = 0; phi - phil - phit < -1; k--, phit -= 2*M_PI);
      if (k < -1) win_printf("k = %d j=%d -> phi 1 %g phi 2 -> %g dphi %g",k,j,phi,phil+phit,phil+phit-phi);
      dsfi->yd[j] = phil + phit;
      if (fabs(phil+phit-phi) > 1e-6)
	win_printf("j=%d -> phi 1 %g phi 2 -> %g dphi %g",j,phi,phil+phit,phil+phit-phi);

      dsp->yd[j] = (2*M_PI*j*bp_f)/ds->nx;
      dsp->yd[j] += dsfi->yd[j];
      if (2*dsfr->yd[j] > Max_val)
	{
	  phim += dsp->yd[j] * dsfr->yd[j];
	  amp += dsfr->yd[j];
	}
      dsfr->yd[j] = sqrt(dsfr->yd[j]);

      //re = dsfr->xd[j];
      //im = dsfi->xd[j];
      //dsfr->yd[j] = (im == 0.0 && re == 0.0) ? 0.0 : atan2(im, re);
    }
  if (amp > 0) phim /= amp;



  for (j = 0;j < ds->nx; j++)
    dsfi->xd[j] = dsfr->xd[j] = dsp->xd[j] = ds->xd[j];

  free_fft_plan_bt(fp);
  free_filter_bt(fil);

  /* now we must do some house keeping */
  set_ds_source(dsi,"complexified ds");
  set_ds_source(dsp,"Phase = %g",phim);
  set_ds_source(dsr,"BP ds f0 = %d w = %d, f0 = %g, AmpMax at %g val %g"
		,bp_f,bp_w,f0,Max_pos, Max_val);

  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}





int	draw_cross_vga_screen_unit(int xc, int yc, int length, int width, int color, imreg *imrs, BITMAP* bm)
{
        int l = length >> 1;
	int w = width >> 1;
	int y = bm->h - yc;
	int x1 =  xc - l;
	int x4 =  xc + l;
	int x2 =  xc - w;
	int x3 =  xc + w;
	int y1 =  y + l;
	int y4 =  y - l;
	int y2 =  y + w;
	int y3 =  y - w;

	(void)imrs;
	line(bm, x1, y2, x2, y2, color);
	line(bm, x2, y2, x2, y1, color);
	line(bm, x2, y1, x3, y1, color);
	line(bm, x3, y1, x3, y2, color);
	line(bm, x3, y2, x4, y2, color);
	line(bm, x4, y2, x4, y3, color);
	line(bm, x3, y3, x4, y3, color);
	line(bm, x3, y3, x3, y4, color);
	line(bm, x2, y4, x3, y4, color);
	line(bm, x2, y3, x2, y4, color);
	line(bm, x1, y3, x2, y3, color);
	line(bm, x1, y2, x1, y3, color);
	return 0;
}
int place_tracking_cross_with_mouse_movie(int *xc, int *yc, int cl, int cw, int color)
{
	int  xm, ym;
	imreg *imr;

    imr = find_imr_in_current_dialog(NULL);
	if (imr == NULL)	return 1;

	xm = mouse_x = x_imdata_2_imr(imr, *xc);
	ym = mouse_y = y_imdata_2_imr(imr, *yc);
	for(; !key[KEY_ENTER]; )
	{
		if (xm != mouse_x || ym != mouse_y)
		{
			xm = mouse_x;
			ym = mouse_y;
			refresh_image(imr,UNCHANGED);
			//draw_cross_vga(xm, ym, cl, cw, color, imr, screen, find_dialog_associated_to_imr(imr, NULL));// _screen_unit
			draw_cross_vga_screen_unit(xm, ym, cl, cw, color, imr, screen);
		}
	}
	*xc = x_imr_2_imdata(imr, xm);
	*yc = y_imr_2_imdata(imr, ym);
	return 0;
}


int refresh_screen_image_and_cross(imreg *imrs, O_i *movie, DIALOG *d, int xc, int yc, int cl, int cw, int color)
{
        BITMAP *imb;

	if (movie == NULL || imrs == NULL || d == NULL) return 1;
	movie->need_to_refresh |= BITMAP_NEED_REFRESH;
	display_image_stuff_16M(imrs,d);
	if ((movie->bmp.to == IS_BITMAP) &&  (movie->bmp.stuff != NULL))
	{
	  imb = (BITMAP*)movie->bmp.stuff;
	  acquire_bitmap(screen);
	  draw_cross_vga_screen_unit(xc, yc, cl, cw, color, imrs, imb);
	  blit(imb,screen,0,0,imrs->x_off + d->x, imrs->y_off -
		imb->h + d->y, imb->w, imb->h);
	  release_bitmap(screen);
	}
	return 0;
}



int track_in_x_y_from_movie(imreg *imrs, O_i *movie, int *xci, int *yci, int cl,
			    int cw, int nf, int imstart, int black_circle, float *xb,
			    float *yb, O_i *oip, O_i *oiz, fft_plan *fp, filter *fil)
{
  register int k;
  int  wait = 0, wait_min, im, ia;
  int xc, yc, cl2 = cl/2;
  int  bead_lost = 0, bd_mul = 8;
  static int dis_filter = 0;
  int x[256], y[256];
  float x1[256], y1[256];
  float xcf, ycf, dx, dy, corr, *zp = NULL, x_var, y_var;
  int  done;
  int  color;
  DIALOG *othe_dialog;
  DIALOG *d;
  unsigned long dt, dtm, dts;
  float Max_deriv, noise;

  othe_dialog = get_the_dialog();
  d = othe_dialog + 2;
  xc = *xci;	yc = *yci;
  xcf = xc; 	ycf = yc;

  dts = get_my_uclocks_per_sec();
  color = makecol(255,64,64);

  for (im = 0, wait_min = 1000, done = 0, dtm = 0; im < (nf + imstart) && done == 0 ; im++ )
    {
      ia = (im < imstart) ? 0 : im - imstart;
      switch_frame(movie,ia);		/* no negative index */
      if (oip != NULL)	zp = oip->im.pixel[ia].fl;

      if (fill_avg_profiles_from_im(movie, xc, yc, cl, cw, x, y, &x_var, &y_var))
	win_printf("profil error");
      if ( im == imstart || wait < wait_min )	wait_min = wait;
      for (k = 0;(oip != NULL) && (k < cl); k++)
	{
	  zp[k] = (float)x[k];
	  zp[k+cl] = (float)y[k];
	}

      if (key[KEY_ESC]) done = 1;
      dt = my_uclock();

      if (wait < wait_min )	wait_min = wait;
      if (black_circle)		erase_around_black_circle(x, y, cl);
      bead_lost += check_bead_not_lost(x, x1, y, y1, cl, bd_mul);
      if (bead_lost > 10)		done = 2;

      dx = find_distance_from_center(x1, cl, 0, dis_filter, !black_circle,&corr, &Max_deriv,
				     fp, fil, &noise, cl);
      dy = find_distance_from_center(y1, cl, 0, dis_filter, !black_circle,&corr, &Max_deriv,
				     fp, fil, &noise, cl);

      xb[ia] = xcf = dx + xc;
      yb[ia] = ycf = yc + dy;
      if (oiz)
	{
	  radial_non_pixel_square_image_sym_profile_in_array(oiz->im.pixel[ia].fl, movie, xcf, ycf, oiz->im.nx/2, 1, 1);
	}
      xc = (int)(xcf + .5);	yc = (int)(ycf + .5);
      clip_cross_in_image(movie, xc, yc, cl2);
      dt = my_uclock() - dt;
      dtm += dt;
      refresh_screen_image_and_cross(imrs, movie, d, xc, yc, cl, cw, color);
    }
  dtm /= (nf) ? nf : 1;
  win_printf("grabbing time = %g \\mu s\nmax \\mu s"
	     , (dts) ? (double)dtm/dts : (double)dtm);
  *xci = xc;
  *yci = yc;
  return (done);
}


int follow_bead_in_x_y_from_movie(void)
{
  register int i;
  imreg *imrs;
  O_p *op;
  O_i *ois, *oidp = NULL, *oiz = NULL;
  d_s *ds;
  int xc = 256, yc = 256, color;
  static int cl = 64, cw = 16, nf = 2048, imstart = 10, prof = 0, zprof = 0;
  int  black_circle = 0;
  fft_plan *fp = NULL;
  filter *fil = NULL;

  if(updating_menu_state != 0)	return D_O_K;

  if (ac_grep(NULL,"%im%oi",&imrs,&ois) != 2)
    return win_printf_OK("cannot find image in acreg!");
  nf = ois->im.n_f;
  color = makecol(255,64,64);
  /* check for menu in win_scanf */
  i = win_scanf("this routine track a bead in X,Y using a cross shaped pattern\n"
		"arm length %6d arm width %6d\n howmany frames %6d imstart %6d\n"
		"generate XY profile image %b\n"
		"generate Radial profile image %b\n key {\\it b} black circle {\\it w} to stop"
		,&cl,&cw,&nf,&imstart,&prof,&zprof); /* %m ,select_power_of_2() */
  if (i == WIN_CANCEL)	return OFF;
  if (cl > 256)	return win_printf_OK("cannot use arm bigger\nthan 128");
  if (cw > 32)	return win_printf_OK("cannot use width bigger\nthan 32");

  fp = fftbt_init(fp, cl);
  fil = filter_init_bt(fil, cl);
  if (fp == NULL || fil == NULL)	return win_printf_OK("cannot init fft!");
  xc = ois->im.nx/2;	yc = ois->im.ny/2;
  i = win_scanf("enter starting position in pixels \n xc %d yc %d",&xc,&yc);
  if (i == WIN_CANCEL) 	return OFF;
  if (xc < 0 || yc < 0)
    {
      xc = ois->im.nx/2;	yc = ois->im.ny/2;
      place_tracking_cross_with_mouse_movie(&xc, &yc, cl, cw, color);
    }

  op = create_and_attach_op_to_oi(ois, nf, nf, 0,IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
  if (op == NULL)		return win_printf_OK("cannot create plot !");
  ds = op->dat[0];

  ds->source = my_sprintf(ds->source,"Bead tracking at 25 Hz Acquistion %d"
			  " de 1 billes \n from movie l = %d, w = %d, nim = %d\nobjective %d,"
			  " zoom factor %f\nIC-PCI %g MHz",0,cl,cw,nf,63,1.6,14.18);
  uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
  uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
  set_plot_title(op, "Trajectoire 0 bis");

  op->filename = my_sprintf(op->filename,"tracs000d%s",".gr");
  if (prof)
    {
      oidp = create_and_attach_oi_to_imr(imrs, 2*cl, nf, IS_FLOAT_IMAGE);
      oidp->im.source = strdup("MOVIE bead profile");
    }

  if (zprof)
    {
      oiz = create_and_attach_oi_to_imr(imrs, cl, nf, IS_FLOAT_IMAGE);
      oiz->im.source = strdup("MOVIE bead profile");
    }

  track_in_x_y_from_movie(imrs, ois, &xc, &yc, cl, cw, nf, imstart, black_circle, ds->xd,
			  ds->yd, oidp, oiz, fp, fil);

  op->need_to_refresh = 1;
  ois->need_to_refresh = PLOTS_NEED_REFRESH;
  if (ois->n_op) 	    ois->cur_op = ois->n_op - 1;
  if (prof)
    {
      find_zmin_zmax(oidp);
      oidp->need_to_refresh = ALL_NEED_REFRESH;
    }
  if (zprof)
    {
      find_zmin_zmax(oiz);
      oiz->need_to_refresh = ALL_NEED_REFRESH;
    }
  return broadcast_dialog_message(MSG_DRAW,0);
}



MENU *FitInterference_image_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;

  add_item_to_menu (mn,"Track local maximum along y",do_trk_local_convol_max_along_y,NULL,0,NULL);
  add_item_to_menu (mn,"Track local zero of phase",do_trk_local_phase_max_along_y,NULL,0,NULL);
  add_item_to_menu (mn,"Track zero of imaginary sig",do_trk_local_zero_of_imaginary_along_y,NULL,0,NULL);
  add_item_to_menu (mn,"Track phase avg",do_trk_phase_avg_along_y,NULL,0,NULL);
  add_item_to_menu (mn,"Track phase avg 2",do_trk_phase_avg_along_y2,NULL,0,NULL);
  add_item_to_menu (mn,"Track phase Martin",do_trk_phase_avg_along_Martin,NULL,0,NULL);
  add_item_to_menu (mn,"Track line by line",track_line_by_line,NULL,0,NULL);

  add_item_to_menu(mn,"\0",		NULL,			NULL, 0,	NULL);
  add_item_to_menu (mn,"Track position from shear profiles",grab_position_from_shear_profiles_from_movie_2,NULL,0,NULL);
  add_item_to_menu (mn,"Track mean position from shear profiles",grab_mean_position_from_shear_profiles_from_movie_2,NULL,0,NULL);

  add_item_to_menu (mn,"Track mean position from shear profiles avg",grab_position_from_shear_profiles_from_movie_3,NULL,0,NULL);
  add_item_to_menu (mn,"Track mean position from moving shear profiles",grab_position_from_moving_shear_profiles_from_movie,NULL,0,NULL);




  add_item_to_menu (mn,"Track bead rings in XY",follow_bead_in_x_y_from_movie,NULL,0,NULL);


  add_item_to_menu(mn,"\0",		NULL,			NULL, 0,	NULL);

  add_item_to_menu (mn,"extract shear profiles",grab_shear_profiles_from_movie,NULL,0,NULL);
  add_item_to_menu (mn,"extract shear profiles 2",grab_shear_profiles_from_movie_2,NULL,0,NULL);

  add_item_to_menu(mn,"\0",		NULL,			NULL, 0,	NULL);
  add_item_to_menu (mn,"generate profiles",generate_Shifted_profil_image,NULL,0,NULL);



  return mn;
}



MENU *FitInterference_plot_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"Hello example", do_FitInterference_hello,NULL,0,NULL);
  add_item_to_menu(mn,"Display interference", do_FitInterference_generate_plot,NULL,0,NULL);
  add_item_to_menu(mn,"Display photons distrubution", do_FitInterference_Photon_Distribution,NULL,0,NULL);
  add_item_to_menu(mn,"autoconvolution",do_FitInterference_compute_autoconv,NULL,0,NULL);
  add_item_to_menu(mn,"Generate distributions and auto-convolution",do_Distribution_and_autoconv,NULL,0,NULL);
  add_item_to_menu(mn,"Auto-convolution maximum fluctuation",Do_AutoCorre_Max_Fluct,NULL,0,NULL);
  add_item_to_menu(mn,"Test variable shift",ErrDevia_Max_Shiftt,NULL,0,NULL);
  add_item_to_menu(mn,"Test variable shift complex",ErrDeviation_Max_Shift_complexify,NULL,0,NULL);
  add_item_to_menu(mn,"complexify",do_complexify_ds,NULL,0,NULL);
  add_item_to_menu(mn,"BP and complexify",do_bp_complexify_ds,NULL,0,NULL);
  add_item_to_menu(mn,"BP and complexify phi",do_bp_complexify_phi_ds,NULL,0,NULL);

 return mn;
}

int	FitInterference_main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  add_plot_treat_menu_item ( "FitInterference", NULL, FitInterference_plot_menu(), 0, NULL);
  add_image_treat_menu_item ( "FitInterference", NULL, FitInterference_image_menu(), 0, NULL);
  return D_O_K;
}

int	FitInterference_unload(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  remove_item_to_menu(plot_treat_menu, "FitInterference", NULL, NULL);
  return D_O_K;
}

#endif
