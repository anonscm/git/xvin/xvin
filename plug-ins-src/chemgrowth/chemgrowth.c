/*
 *    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
 *    T. Julou
 */
#ifndef _CHEMGROWTH_C_
#define _CHEMGROWTH_C_

# include "allegro.h"
# include "xvin.h"


/* If you include other plug-ins header do it here*/ 

/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "fft32d.h"
# include "chemgrowth.h"

chemo *find_chemo_in_op(O_p *op) 
{
  register int i;
  d_s *ds;
	
  if (op == NULL) return NULL;
  for ( i = 0; i < op->n_dat; i++)
    {
      ds = op->dat[i];
      if ( ds->use.to == IS_CHEMGROWTH )	return (chemo *)ds->use.stuff;
    }
  return NULL;
}

int simulate_one_time_step(O_p *op, DIALOG *d)
{
  register int i, re, ii;
  double nq, nq2;
  double tmp = 1;
  chemo *r = NULL;
  d_s *dsE, *dsN, *dsdE;
  pltreg *pr = NULL;
  float min = 0, max = 1;

  r = find_chemo_in_op(op);
  if (r == NULL) return win_printf_OK("cannot find data");

  if (r->idle_state == 0)
    return 0;
  if (d->dp == NULL)    return D_O_K;
  pr = (pltreg*)d->dp;        /* the plot region is here */


  dsE = find_source_specific_ds_in_op(op, "Bacterial Concentration E");
  if (dsE == NULL) return win_printf_OK("cannot find data set E");
  dsN = find_source_specific_ds_in_op(op, "Nutrients Concentration N");
  if (dsN == NULL) return win_printf_OK("cannot find data set N");
  dsdE = find_source_specific_ds_in_op(op, "Bacterial growth dE");
  if (dsdE == NULL) return win_printf_OK("cannot find data set dE");

  r->niter++;

  // Init temp var
  for (i= 0; i < r->nx; i++)
    {
      r->Etmp[i] = r->E[i];
      r->Ntmp[i] = r->N[i];
    }
      
  // First RK's term
  realtr1_d(r->nx, r->Etmp);
  realtr1_d(r->nx, r->Ntmp);
  fft_d(r->nx, r->Etmp, 1);
  fft_d(r->nx, r->Ntmp, 1);
  realtr2_d(r->nx, r->Etmp, 1);
  realtr2_d(r->nx, r->Ntmp, 1);
  // Using this fft with only nx/2 terms, the first complex hides actually
  // its real part as real part (!) and the real part of the n/2th mode as imaginary
  // part (put to 0). Hence the condition (i==0) above...

  for (i= 0; i < r->nx/2; i++)
    {
      re = 2*i;                              // this is the index of real part
      ii = re+1;                             // this is the index of imaginary part
      nq = ((double)i*8)/r->nx;         
      nq2 = ((double)i*i*64)/(r->nx*r->nx);
      tmp = (i > r->nx/4) ? (0.5 * (1 - cos((M_PI * i * 2)/ r->nx))) : 1;
			
      if (i == 0)
	{
	  r->k1e[re] = tmp * r->h * (- nq2 * r->De * r->Etmp[re] - nq * r->ce * r->Etmp[ii]);
	  r->k1e[ii] = 0;
	  r->k1n[re] = tmp * r->h * (- nq2 * r->Dn * r->Ntmp[re] - nq * r->cn * r->Ntmp[ii]);
	  r->k1n[ii] = 0;
	}
      else
	{
	  r->k1e[re] = tmp * r->h * (- nq2 * r->De * r->Etmp[re] - nq * r->ce * r->Etmp[ii]);
	  r->k1e[ii] = tmp * r->h * (- nq2 * r->De * r->Etmp[ii] + nq * r->ce * r->Etmp[re]);
	  r->k1n[re] = tmp * r->h * (- nq2 * r->Dn * r->Ntmp[re] - nq * r->cn * r->Ntmp[ii]);
	  r->k1n[ii] = tmp * r->h * (- nq2 * r->Dn * r->Ntmp[ii] + nq * r->cn * r->Ntmp[re]);
	}
    }
  realtr2_d(r->nx, r->Etmp, -1);
  realtr2_d(r->nx, r->Ntmp, -1);
  realtr2_d(r->nx, r->k1e, -1);
  realtr2_d(r->nx, r->k1n, -1);
  fft_d(r->nx, r->Etmp, -1);
  fft_d(r->nx, r->Ntmp, -1);
  fft_d(r->nx, r->k1e, -1);
  fft_d(r->nx, r->k1n, -1);
  realtr1_d(r->nx, r->Etmp);
  realtr1_d(r->nx, r->Ntmp);
  realtr1_d(r->nx, r->k1e);
  realtr1_d(r->nx, r->k1n);

  for (i= 0; i < r->nx; i++)
    {
      r->k1e[i] += r->h * (r->Etmp[i] * (r->gr[i] * r->Ntmp[i] / (r->Ntmp[i]+r->No)));
      r->k1n[i] -= r->h * (r->Etmp[i] * (r->gr[i] * r->Ntmp[i] / (r->Ntmp[i]+r->No)) / r->yld[i]);
      //INTRODUIRE DE LA DENSITE DEPENDANCE
      
      r->Etmp[i] = r->E[i] + r->k1e[i]/2;
      r->Ntmp[i] = r->N[i] + r->k1n[i]/2;
    } 

  // Second RK's term
  realtr1_d(r->nx, r->Etmp);
  realtr1_d(r->nx, r->Ntmp);
  fft_d(r->nx, r->Etmp, 1);
  fft_d(r->nx, r->Ntmp, 1);
  realtr2_d(r->nx, r->Etmp, 1);
  realtr2_d(r->nx, r->Ntmp, 1);

  for (i= 0; i < r->nx/2; i++)
    {
      re = 2*i;                              // this is the index of real part
      ii = re+1;                             // this is the index of imaginary part
      nq = ((double)i*8)/r->nx;         
      nq2 = ((double)i*i*64)/(r->nx*r->nx);
      tmp = (i > r->nx/4) ? (0.5 * (1 - cos((M_PI * i * 2)/ r->nx))) : 1;
			
      if (i == 0)
	{
	  r->k2e[re] = tmp * r->h * (- nq2 * r->De * r->Etmp[re] - nq * r->ce * r->Etmp[ii]);
	  r->k2e[ii] = 0;
	  r->k2n[re] = tmp * r->h * (- nq2 * r->Dn * r->Ntmp[re] - nq * r->cn * r->Ntmp[ii]);
	  r->k2n[ii] = 0;
	}
      else
	{
	  r->k2e[re] = tmp * r->h * (- nq2 * r->De * r->Etmp[re] - nq * r->ce * r->Etmp[ii]);
	  r->k2e[ii] = tmp * r->h * (- nq2 * r->De * r->Etmp[ii] + nq * r->ce * r->Etmp[re]);
	  r->k2n[re] = tmp * r->h * (- nq2 * r->Dn * r->Ntmp[re] - nq * r->cn * r->Ntmp[ii]);
	  r->k2n[ii] = tmp * r->h * (- nq2 * r->Dn * r->Ntmp[ii] + nq * r->cn * r->Ntmp[re]);
	}
    }
  realtr2_d(r->nx, r->Etmp, -1);
  realtr2_d(r->nx, r->Ntmp, -1);
  realtr2_d(r->nx, r->k2e, -1);
  realtr2_d(r->nx, r->k2n, -1);
  fft_d(r->nx, r->Etmp, -1);
  fft_d(r->nx, r->Ntmp, -1);
  fft_d(r->nx, r->k2e, -1);
  fft_d(r->nx, r->k2n, -1);
  realtr1_d(r->nx, r->Etmp);
  realtr1_d(r->nx, r->Ntmp);
  realtr1_d(r->nx, r->k2e);
  realtr1_d(r->nx, r->k2n);

  for (i= 0; i < r->nx; i++)
    {
      r->k2e[i] += r->h * (r->Etmp[i] * (r->gr[i] * r->Ntmp[i] / (r->Ntmp[i]+r->No)));
      r->k2n[i] -= r->h * (r->Etmp[i] * (r->gr[i] * r->Ntmp[i] / (r->Ntmp[i]+r->No)) / r->yld[i]);
       
      r->Etmp[i] = r->E[i] + r->k2e[i]/2;
      r->Ntmp[i] = r->N[i] + r->k2n[i]/2;
    } 

  // Third RK's term
  realtr1_d(r->nx, r->Etmp);
  realtr1_d(r->nx, r->Ntmp);
  fft_d(r->nx, r->Etmp, 1);
  fft_d(r->nx, r->Ntmp, 1);
  realtr2_d(r->nx, r->Etmp, 1);
  realtr2_d(r->nx, r->Ntmp, 1);

  for (i= 0; i < r->nx/2; i++)
    {
      re = 2*i;                              // this is the index of real part
      ii = re+1;                             // this is the index of imaginary part
      nq = ((double)i*8)/r->nx;         
      nq2 = ((double)i*i*64)/(r->nx*r->nx);
      tmp = (i > r->nx/4) ? (0.5 * (1 - cos((M_PI * i * 2)/ r->nx))) : 1;
			
      if (i == 0)
	{
	  r->k3e[re] = tmp * r->h * (- nq2 * r->De * r->Etmp[re] - nq * r->ce * r->Etmp[ii]);
	  r->k3e[ii] = 0;
	  r->k3n[re] = tmp * r->h * (- nq2 * r->Dn * r->Ntmp[re] - nq * r->cn * r->Ntmp[ii]);
	  r->k3n[ii] = 0;
	}
      else
       {
	  r->k3e[re] = tmp * r->h * (- nq2 * r->De * r->Etmp[re] - nq * r->ce * r->Etmp[ii]);
	  r->k3e[ii] = tmp * r->h * (- nq2 * r->De * r->Etmp[ii] + nq * r->ce * r->Etmp[re]);
	  r->k3n[re] = tmp * r->h * (- nq2 * r->Dn * r->Ntmp[re] - nq * r->cn * r->Ntmp[ii]);
	  r->k3n[ii] = tmp * r->h * (- nq2 * r->Dn * r->Ntmp[ii] + nq * r->cn * r->Ntmp[re]);
	}
    }
  realtr2_d(r->nx, r->Etmp, -1);
  realtr2_d(r->nx, r->Ntmp, -1);
  realtr2_d(r->nx, r->k3e, -1);
  realtr2_d(r->nx, r->k3n, -1);
  fft_d(r->nx, r->Etmp, -1);
  fft_d(r->nx, r->Ntmp, -1);
  fft_d(r->nx, r->k3e, -1);
  fft_d(r->nx, r->k3n, -1);
  realtr1_d(r->nx, r->Etmp);
  realtr1_d(r->nx, r->Ntmp);
  realtr1_d(r->nx, r->k3e);
  realtr1_d(r->nx, r->k3n);

  for (i= 0; i < r->nx; i++)
    {
      r->k3e[i] += r->h * (r->Etmp[i] * (r->gr[i] * r->Ntmp[i] / (r->Ntmp[i]+r->No)));
      r->k3n[i] -= r->h * (r->Etmp[i] * (r->gr[i] * r->Ntmp[i] / (r->Ntmp[i]+r->No)) / r->yld[i]);
       
      r->Etmp[i] = r->E[i] + r->k3e[i]/2;
      r->Ntmp[i] = r->N[i] + r->k3n[i]/2;
    } 

  // Fourth RK's term
  realtr1_d(r->nx, r->Etmp);
  realtr1_d(r->nx, r->Ntmp);
  fft_d(r->nx, r->Etmp, 1);
  fft_d(r->nx, r->Ntmp, 1);
  realtr2_d(r->nx, r->Etmp, 1);
  realtr2_d(r->nx, r->Ntmp, 1);

  for (i= 0; i < r->nx/2; i++)
    {
      re = 2*i;                              // this is the index of real part
      ii = re+1;                             // this is the index of imaginary part
      nq = ((double)i*8)/r->nx;         
      nq2 = ((double)i*i*64)/(r->nx*r->nx);
      tmp = (i > r->nx/4) ? (0.5 * (1 - cos((M_PI * i * 2)/ r->nx))) : 1;
			
      if (i == 0)
	{
	  r->k4e[re] = tmp * r->h * (- nq2 * r->De * r->Etmp[re] - nq * r->ce * r->Etmp[ii]);
	  r->k4e[ii] = 0;
	  r->k4n[re] = tmp * r->h * (- nq2 * r->Dn * r->Ntmp[re] - nq * r->cn * r->Ntmp[ii]);
	  r->k4n[ii] = 0;
	}
      else
       {
	  r->k4e[re] = tmp * r->h * (- nq2 * r->De * r->Etmp[re] - nq * r->ce * r->Etmp[ii]);
	  r->k4e[ii] = tmp * r->h * (- nq2 * r->De * r->Etmp[ii] + nq * r->ce * r->Etmp[re]);
	  r->k4n[re] = tmp * r->h * (- nq2 * r->Dn * r->Ntmp[re] - nq * r->cn * r->Ntmp[ii]);
	  r->k4n[ii] = tmp * r->h * (- nq2 * r->Dn * r->Ntmp[ii] + nq * r->cn * r->Ntmp[re]);
	}
    }
  realtr2_d(r->nx, r->Etmp, -1);
  realtr2_d(r->nx, r->Ntmp, -1);
  realtr2_d(r->nx, r->k4e, -1);
  realtr2_d(r->nx, r->k4n, -1);
  fft_d(r->nx, r->Etmp, -1);
  fft_d(r->nx, r->Ntmp, -1);
  fft_d(r->nx, r->k4e, -1);
  fft_d(r->nx, r->k4n, -1);
  realtr1_d(r->nx, r->Etmp);
  realtr1_d(r->nx, r->Ntmp);
  realtr1_d(r->nx, r->k4e);
  realtr1_d(r->nx, r->k4n);

  for (i= 0; i < r->nx; i++)
    {
      r->k4e[i] += r->h * (r->Etmp[i] * (r->gr[i] * r->Ntmp[i] / (r->Ntmp[i]+r->No)));
      r->k4n[i] -= r->h * (r->Etmp[i] * (r->gr[i] * r->Ntmp[i] / (r->Ntmp[i]+r->No)) / r->yld[i]);
       
      r->Etmp[i] = r->E[i] + r->k4e[i]/2;
      r->Ntmp[i] = r->N[i] + r->k4n[i]/2;

      r->E[i] += (r->k1e[i] + 2*r->k2e[i] + 2*r->k3e[i] + r->k4e[i])/6;
      r->N[i] += (r->k1n[i] + 2*r->k2n[i] + 2*r->k3n[i] + r->k4n[i])/6;
    }

  /* Apodization at the border */
  for (i = 0; i < r->nx/2; i++)
    {
      if (i < r->at/2)
	{
	  r->E[i] = 0;
	  r->N[i] = 0;
	  r->N[r->nx-i-1]  = 1;
	}
      else if (i < 1.5*r->at)
	{
	  tmp = 0.5 * (1 - cos((i-(r->at/2)) * M_PI / (r->at)));

	  r->E[i]         *= tmp;
	  r->N[i]         *= tmp;
	}
      // Prevent negative concentration
      if (r->E[i] < TINY)         r->E[i] = TINY;
      if (r->E[r->nx-i-1] < TINY) r->E[r->nx-i-1] = TINY;
      if (r->N[i] < TINY)      r->N[i] = TINY;
      if (r->N[r->nx-i-1] < TINY) r->N[r->nx-i-1] = TINY;
    }

  // Update display
  if (r->niter % r->nboucle== 0)
    {
      for (i = 0; i < r->nx; i++)
	{
	  dsE->yd[i] =  r->E[i];
	  //dsE->yd[i] = 1e2 * r->gr[i];
	  if (i == 0 || min > dsE->yd[i])  min = dsE->yd[i];
	  if (i == 0 || max < dsE->yd[i])  max = dsE->yd[i];

	  dsN->yd[i] = r->N[i];
	  if (i == 0 || min > dsN->yd[i])  min = dsN->yd[i];
	  if (i == 0 || max < dsN->yd[i])  max = dsN->yd[i];

	  dsdE->yd[i] = 10000 * r->E[i] * r->gr[i] * r->N[i] / (r->N[i]+r->No);
	  //dsdE->yd[i] = r->yld[i];
	  if (i == 0 || min > dsdE->yd[i])  min = dsdE->yd[i];
	  if (i == 0 || max < dsdE->yd[i])  max = dsdE->yd[i];

	  /* 	  if (r->E[i] < - TINY) */
	  /* 	    win_printf_OK("negative real part"); */
	}
		
      if ((max - min) < ((op->y_hi - op->y_lo)*.66)
	  || min < op->y_lo || max > op->y_hi )
	{
	  op->y_lo = min - 0.03*(max-min);
	  op->y_hi = max + 0.03*(max-min);
	  op->iopt2 |= Y_LIM;
	}			
      set_plot_title(op, "Chemostat density profile at %uh %02umin %02usec",(int)(r->h*r->niter)/3600, ((int)(r->h*r->niter)%3600)/60, (int)(r->h*r->niter)%60);
      op->need_to_refresh = 1;  
      /* refisplay the entire plot */
      refresh_plot(pr, UNCHANGED);
    }
  return 0;
}


int create_simulation(void)
{
  int i, j;
  O_p  *opn = NULL;
  static int  n_sto = 0, nx = 512, at = 32, nt = 40;
  static float h = 0.1;	
  static float Tmin = 20, Tmax = 43, Rmin = 35, Rmax= 50;
  static float ce = 0.0001, cn = 0.0001, De = 0.000007, Dn = 0.00005;
  static float Go = 1, Eo = 1;
  float tmp, Tj, Tint;
  float b = 0.00075, c = 0.5;
  float h_yield = 0.26, No = 0.3;
  d_s *dsE, *dsN, *dsdE;
  pltreg *pr = NULL;
  chemo *r = NULL;

  if(updating_menu_state != 0)	return D_O_K;	
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine simulate bacterial growth in "
			   "a chemostat and place it in a new plot");
    }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return win_printf_OK("cannot find data");
	
  r = (chemo*)calloc(1,sizeof(chemo));	
  if (r == NULL)	return win_printf_OK("cannot create structure");

  i = win_scanf("Chemostat growth model parameters\n"
		"Units: cm, g, s\n\n"
		"Nb of points %4d  Apodiz width %4d\n"
		"Step in time %4f\n"
		"Nb. of time steps between display %4d\n\n"
		"Flow rate c_E %4f  Diff coeff D_E %4f\n"
		"Flow rate c_N %4f  Diff coeff D_N %4f\n\n"
		"Input glucose concentration     Go %4f\n"
		"Initial bacterial density              Eo %4f\n\n"
		"Growth rate params    b %4f  c %4f\n"
		"Nutrient saturation    No %4f\n"
		"Max biomass yield      h %4f\n\n"
		"Bact T_{min}       %4f  Bact T_{max}      %4f\n"
		"Chem T_{min}    %4f  Chem T_{max}    %4f\n",
		&nx,&at,&h,&nt,&ce,&De,&cn,&Dn,&Go,&Eo,&b,&c,&No,&h_yield,&Tmin,&Tmax,&Rmin,&Rmax);
  if (i == CANCEL)	return OFF;

  r->nx = nx;
  r->at = at;
  r->h = h;
  r->nboucle = nt;
  r->ce = ce * 4 * M_PI;
  r->cn = cn * 4 * M_PI;
  r->De = De * 16 * M_PI * M_PI;
  r->Dn = Dn * 16 * M_PI * M_PI;
  r->No = No;
  r->Tmin = Tmin;
  r->Tmax = Tmax;
  r->Rmin = Rmin;
  r->Rmax = Rmax;

  r->idle_state = 0;

  if ((opn = create_and_attach_one_plot(pr, r->nx, r->nx, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  dsE = opn->dat[0];
  if ((dsN = create_and_attach_one_ds(opn, r->nx, r->nx, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  if ((dsdE = create_and_attach_one_ds(opn, r->nx, r->nx, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  opn->op_idle_action = simulate_one_time_step;

  r->E   = (double*)calloc(r->nx,sizeof(double));
  r->N   = (double*)calloc(r->nx,sizeof(double));
  r->dE  = (double*)calloc(r->nx,sizeof(double));
  r->gr  = (float*) calloc(r->nx,sizeof(float));
  r->yld = (float*) calloc(r->nx,sizeof(float));
  r->Etmp= (double*)calloc(r->nx,sizeof(double));
  r->Ntmp= (double*)calloc(r->nx,sizeof(double));

  r->k1e = (double*)calloc(r->nx,sizeof(double));
  r->k2e = (double*)calloc(r->nx,sizeof(double));
  r->k3e = (double*)calloc(r->nx,sizeof(double));
  r->k4e = (double*)calloc(r->nx,sizeof(double));
	
  r->k1n = (double*)calloc(r->nx,sizeof(double));
  r->k2n = (double*)calloc(r->nx,sizeof(double));
  r->k3n = (double*)calloc(r->nx,sizeof(double));
  r->k4n = (double*)calloc(r->nx,sizeof(double));
			
  if (r->E == NULL || r->N == NULL)
    return win_printf_OK("cannot create double arrays !");

  fft_init_d(r->nx);

  /* we calculate the growth rate along the temperature gradient*/
#ifdef TEMP_GRAD
  Tint = (r->Rmax - r->Rmin) / r->nx;
  Tj = r->Rmin - Tint;
  for (j = 0; j < r->nx; j++)
    {
      Tj += Tint;
      if (Tj < r->Tmin || Tj > r->Tmax)
	r->gr[j] = 0;
      else
	{
	  r->gr[j] = b * (Tj - r->Tmin);                 // Ratkowsky formula
	  r->gr[j] *= (1 - exp(c * (Tj - r->Tmax)));     // Ratkowsky formula (continued)
	  r->gr[j] *= (1 - exp(c * (Tj - r->Tmax)));     // Ratkowsky formula (end)
	  r->gr[j] *= (No + Go) / Go;                    // Normalize the saturation coeff
	}

      //win_printf_OK("gr[%4f] = %4f", Tj, r->gr[j]);
    }
#else
  for (j = 0; j < r->nx; j++)
      r->gr[j] = b;
#endif

  /* we calculate the biomass yield along the temperature gradient*/
#ifdef YIELD
  Tj = r->Rmin - Tint;
  for (j = 0; j < r->nx; j++)
    {
      Tj += Tint;
      tmp = 1 - exp((Tj-(Tmax+2)) * 0.3);
      if (Tj <= r->Tmin || Tj >= r->Tmax + 1)
	r->yld[j] = 1;
      else
	r->yld[j] = h_yield * tmp;
    }
#else
  for (j = 0; j < r->nx; j++)
      r->yld[j] = 1;
#endif

  /* we create the initial condition */	
  switch(IC)
    {
    case 0:                                              // DIRAC
      for (j = 0; j < r->nx; j++)
	{
	  if (j == nx/2-3)	r->E[j] = r->N[j] =  .2;		
	  else if (j == nx/2-2) r->E[j] = r->N[j] =  .5;
	  else if (j == nx/2-1)	r->E[j] = r->N[j] =  .8;
	  else if (j == nx/2)	r->E[j] = r->N[j] =   1;
	  else if (j == nx/2+1)	r->E[j] = r->N[j] =  .8;		
	  else if (j == nx/2+2)	r->E[j] = r->N[j] =  .5;
	  else if (j == nx/2+3)	r->E[j] = r->N[j] =  .2;		
	  else r->E[j] = r->N[j] =  0;		
	}
      break;
    case 1:                                              // RECTANGULAR PULSE
      for (j = 0; j <= r->nx/2; j++)
	{
	  if (j < 2*at)
	    r->E[j] = r->N[j] = r->E[r->nx-j-1] = r->N[r->nx-j-1] = 0;		
	  else
	    r->E[j] = r->N[j] = r->E[r->nx-j-1] = r->N[r->nx-j-1] = 1;
	}
      break;
    case 2:                                              // SMOOTH RECTANGULAR PULSE
      for (j = 0; j <= r->nx/2; j++)
	{
	  if (j < at/2)
	    {
	      r->E[j] = r->N[j] = r->E[r->nx-j-1] = 0;
	      r->N[r->nx-j-1] = 1;
	    }
	  else if (j < 1.5*at)
	    {
	      tmp = 0.5 * (1 - cos((j-(r->at/2)) * M_PI / (r->at)));
	      r->E[j] = r->N[j] = r->E[r->nx-j-1] = tmp;
	      r->N[r->nx-j-1] = 1;
	    }
	  else
	    r->E[j] = r->N[j] = r->E[r->nx-j-1] = r->N[r->nx-j-1] = 1;
	}
      break;
    }
  
  Tj = r->Rmin - Tint;
  for (j = 0; j < r->nx; j++)
    {
      dsE->yd[j] = r->E[j];
      dsN->yd[j] = r->N[j];
      dsdE->yd[j] = 0;
      Tj += Tint;
      dsE->xd[j] = dsN->xd[j] = dsdE->xd[j] = Tj;
    }

  dsE->use.stuff = (void*)r;
  dsE->use.to = IS_CHEMGROWTH;

  /* now we must do some house keeping */
  dsE->source = my_sprintf(dsE->source,"Bacterial Concentration E");
  dsN->source = my_sprintf(dsN->source,"Nutrients Concentration N");
  dsdE->source = my_sprintf(dsdE->source,"Bacterial growth dE");
  set_plot_title(opn, "Chemostat density profile");
  set_plot_x_title(opn, "Position X along chemostat");
  set_plot_y_title(opn, "E and N");
  set_op_filename(opn, "ChemGrowth%03d.gr", n_sto++);
  opn->y_lo = -0.2;
  opn->y_hi = 1.1;
  opn->width = 1.3;
  opn->iopt2 |= Y_LIM;
  build_and_display_y_axis_prime_menu(pr, opn);
  create_attach_select_y_un_to_op(opn, 0, 0, 1e-4, 0, 0, "no_name");
  opn->c_yu_p = 1;
  opn->c_yu = 0;
  set_plt_y_unit_set(pr, opn->c_yu);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}

int start_pause_simulation(void)
{
  pltreg *pr = NULL;
  O_p *op = NULL;
  chemo *r = NULL;

  if(updating_menu_state != 0)
    {
      add_keyboard_short_cut(0, KEY_SPACE, 0, start_pause_simulation);
      return D_O_K;
    }

  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
    return win_printf_OK("cannot find data");

  r = find_chemo_in_op(op);
  if (r == NULL) return win_printf_OK("cannot find data");

  r->idle_state = r->idle_state ? 0 : 1;
  return D_O_K;	
}

MENU *chemgrowth_plot_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn, "Start / Pause simulation", start_pause_simulation,  NULL, 0, NULL);
  add_item_to_menu(mn, "Create new simulation",    create_simulation,       NULL, 0, NULL);

  return mn;
}

int chemgrowth_main(int argc, char **argv)
{
  add_plot_treat_menu_item ("Chemostat growth", NULL, chemgrowth_plot_menu(), 0, NULL);
  create_simulation();
  return D_O_K;
}
int	chemgrowth_unload(int argc, char **argv)
{ 
  remove_item_to_menu(image_treat_menu, "Chemostat growth", NULL, NULL);
  return D_O_K;
}

#endif
