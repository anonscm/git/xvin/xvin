#ifndef _CHEMGROWTH_H_
#define _CHEMGROWTH_H_

#define IS_CHEMGROWTH  5020
#define TEMP_GRAD                /* Temperature gradient */
#define YIELD                    /* Biomass yield        */
#define TINY           1e-6
#define IC             2         /* Initial condtions    */
/* 0 is dirac shape */
/* 1 is a rectangular pulse */
/* 2 is a smooth rectangular pulse */

typedef struct _chemo
{
  int    idle_state;             /* flag for active simulation (idle action)    */
  int    niter;                  /* nb of time iteration in a set 		*/
  int    nboucle;	 	 /* the nb of iter between profile display 	*/
  double h;			 /* the step 					*/
  int    nx;			 /* nb of points along the chemostat		*/
  int    at;			 /* nb of points used to apodize densities      */
  double *E, *N, *dE;	         /* The profile for E and N + the growth of bact*/
  double *Etmp, *Ntmp;
  float	 ce;			 /* The flow rate for E (bacteria)     		*/
  float	 cn;			 /* The flow rate for N (nutrients)     	*/
  float	 De;			 /* The Diffusion coefficient of C1 		*/
  float  Dn;			 /* The Diffusion coefficient of Q1		*/
  float  *gr, *yld;              /* The bacterial growth rate and yield         */
  float  No;                     /* Nutrient saturation                         */
  float  Tmin, Tmax;             /* Min and max growth temperatures of bacteria */
  float  Rmin, Rmax;             /* Min and max temp at chemostat extremities   */
  double *k1e, *k2e, *k3e, *k4e; /* Runge Kutta 4 integration parameters        */
  double *k1n, *k2n, *k3n, *k4n; /* Runge Kutta 4 integration parameters        */
} chemo;

//PXV_VAR(double, *Etmp);
//PXV_VAR(double, *Ntmp);

PXV_FUNC(chemo*, find_chemo_in_op, (O_p *op));
PXV_FUNC(int, simulate_one_time_step, (O_p *op, DIALOG *d));
PXV_FUNC(int, create_simulation, (void));
PXV_FUNC(int, start_pause_simulation, (void));
PXV_FUNC(MENU*, chemgrowth_plot_menu, (void));
PXV_FUNC(int, chemgrowth_main, (int argc, char **argv));

#endif
