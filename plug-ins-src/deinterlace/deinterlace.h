#ifndef _DEINTERLACE_H_
#define _DEINTERLACE_H_

PXV_FUNC(int, do_deinterlace_average_along_y, (void));
PXV_FUNC(MENU*, deinterlace_image_menu, (void));
PXV_FUNC(int, deinterlace_main, (int argc, char **argv));
#endif

