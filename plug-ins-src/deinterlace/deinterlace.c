/*
*    Plug-in program for image treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _DEINTERLACE_C_
#define _DEINTERLACE_C_

# include "allegro.h"
# include "xvin.h"

/* If you include other plug-ins header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "deinterlace.h"



int deinterlace_movie(void)
{
  register int i, j, k;
  imreg *imr;
  O_i *oi, *oid;
  union pix *pd, *ps; 
  static int nf = 16, nx = 32, ny = 32, im, odd = 0;
  

  if(updating_menu_state != 0)	return D_O_K;
  
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;	
    }		
  


  nf = abs(oi->im.n_f);		
  if (nf <= 0)	
    {
      win_printf("This is not a movie");
      return D_O_K;
    }
  i = oi->im.c_f;
  if (oi->im.data_type != IS_CHAR_IMAGE)	
    {
      win_printf("This is not a movie");
      return D_O_K;
    }
  nx = oi->im.nx;
  ny = oi->im.ny;

  i = win_scanf("first image odd %R even %r\n",&odd);
  if (i == CANCEL) return 0;

  oid = create_and_attach_movie_to_imr(imr, nx, ny, IS_CHAR_IMAGE, 2*nf);
  if (oid == NULL)      return win_printf_OK("Can't create dest movie");


  for (im = 0; im < nf; im++)
    {
      switch_frame(oi,im);
      switch_frame(oid,2*im+((odd+1)%2));
      for (i = 1, ps = oi->im.pixel, pd = oid->im.pixel; i < ny ; i+=2)
	{
	  for (j=0; j< nx; j++)
	      pd[i].ch[j] = ps[i].ch[j];
	}
      for (i = 0, ps = oi->im.pixel, pd = oid->im.pixel; i < ny ; i+=2)
	{
	  k = (i == 0) ? 0 : i-1;
	  for (j=0; j< nx; j++)
	      pd[i].ch[j] = (ps[k].ch[j] + ps[i+1].ch[j])/2;
	}
      switch_frame(oid,2*im+(odd%2));
      for (i = 0, ps = oi->im.pixel, pd = oid->im.pixel; i < ny ; i+=2)
	{
	  for (j=0; j< nx; j++)
	      pd[i].ch[j] = ps[i].ch[j];
	}
      for (i = 1, ps = oi->im.pixel, pd = oid->im.pixel; i < ny ; i+=2)
	{
	  k = (i == ny - 1) ? ny - 2 : i+1;
	  for (j=0; j< nx; j++)
	      pd[i].ch[j] = (ps[k].ch[j] + ps[i-1].ch[j])/2;
	}
    }
  inherit_from_im_to_im(oid,oi);
  uns_oi_2_oi(oid,oi);
  oid->im.win_flag = oi->im.win_flag;
  if (oi->title != NULL)	set_im_title(oid, "%s ",
					     oi->title);
  set_formated_string(&oid->im.treatement,
		      "Image %s ", oi->filename);
  find_zmin_zmax(oid);
  return (refresh_image(imr, imr->n_oi - 1));
}


int phase_shift_between_consecutive_lines(void)
{
 register int i, j;
  imreg *imr;
  O_i *oi;
  O_p *op;
  d_s *ds;
  int ne, ns;
  union pix *ps; 
  static int nt = 16, nx = 32, ny = 32, con = 0;
  static float frac = 0.5, im, re;
  
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;	
    }		
  else if (oi->im.data_type != IS_COMPLEX_IMAGE)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;	
    }
  else active_menu->flags &= ~D_DISABLED;				
  if(updating_menu_state != 0)	return D_O_K;

  nx = oi->im.nx;
  ny = oi->im.ny;

  i = win_scanf("I am going to compute phase-shifts between consecurive lines %R\n"
		"or between the first and line n %r\n"
		"enter the size of the centered window over which phase is computed\n"
		"(this avoid bourdary effect) in %% %6f \n",&con,&frac);
  if (i == CANCEL) return 0;

  ns = nx/2 - nx*frac/200;
  ne = nx/2 + nx*frac/200;
  nt = ne - ns + 1;

  op = create_and_attach_op_to_oi(oi, ny-1, ny-1, 0,0);
  if (op == NULL)    return win_printf_OK("cannot create plot!");
  ds = op->dat[0];				

  for (i = 1, ps = oi->im.pixel; i < ny; i++)
    {
      for(j = ns, ds->xd[i-1] = 0; j <= ne; j++)
	{
	  if (con == 0)
	    {
	      re = ps[i-1].fl[2*j] * ps[i].fl[2*j] +  ps[i-1].fl[2*j+1] * ps[i].fl[2*j+1];
	      im = ps[i-1].fl[2*j] * ps[i].fl[2*j+1] -  ps[i-1].fl[2*j+1] * ps[i].fl[2*j];
	    }
	  else
	    {
	      re = ps[0].fl[2*j] * ps[i].fl[2*j] +  ps[0].fl[2*j+1] * ps[i].fl[2*j+1];
	      im = ps[0].fl[2*j] * ps[i].fl[2*j+1] -  ps[0].fl[2*j+1] * ps[i].fl[2*j];
	    }
	  ds->xd[i-1] += (im == 0.0 && re == 0.0) ? 0.0 : atan2(im,re);

	}
      ds->xd[i-1] /= nt;
      ds->xd[i-1] /= 2*M_PI;
      ds->yd[i-1] = ((float)i)-0.5;
    }
  return refresh_im_plot(imr, oi->n_op-1);
}


MENU *deinterlace_image_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"separate odd and even", deinterlace_movie,NULL,0,NULL);
	add_item_to_menu(mn,"phase shift", phase_shift_between_consecutive_lines,NULL,0,NULL);

	return mn;
}

int	deinterlace_main(int argc, char **argv)
{
	add_image_treat_menu_item ( "deinterlace", NULL, deinterlace_image_menu(), 0, NULL);
	return D_O_K;
}

int	deinterlace_unload(int argc, char **argv)
{
	remove_item_to_menu(image_treat_menu, "deinterlace", NULL, NULL);
	return D_O_K;
}
#endif

