/************************************************************************/
/* Lotus-related functions on images					*/
/*									*/
/************************************************************************/
/* Vincent Croquette, Nicolas Garnier	nicolas.garnier@ens-lyon.fr	*/
/************************************************************************/
#ifndef _LOTUS_C_
#define _LOTUS_C_

#include "allegro.h"
#include "xvin.h"

#include <stdlib.h>
#include <math.h>
#include <malloc.h>

#include "../ohy/ohy.h"

#define BUILDING_PLUGINS_DLL
#include "lotus.h"

#ifndef min_in
#define min_in(a,b)  (a<b ? a : b)
#endif

#ifndef MAX_TITLE_STRING_SIZE
#define MAX_TITLE_STRING_SIZE 1024
#endif

/********************************************************************************/
/* depliage d'une image pour passer d'une geometrie cylindrique a cartesienne 	*/
/* et pliage reciproque								*/
/* 	original C version 17.12.1997						*/
/*	XVin version	   17.03.1999						*/
/* 	bug correction 	   23.03.1999						*/
/* 	pliage	           24.03.1999						*/
/*										*/
/* this function can be called AUTOmaticaly					*/
/********************************************************************************/
int do_lotus_deplie(void)
{      
	register int i,k;             	/* intermediaire compteurs de boucle    */
	imreg 	*imr;		       	/* image region 			*/
	O_i 	*ois,*oid;    		/* one image source and destination 	*/
	union 	pix *ps,*pd;   		/* data des images (matrice) 	       	*/
	ohy_s 	*as, *asd;		/* for uses 				*/
	int	nx, ny;		       	/* dimensions en x, y			*/
	char  	c1[128];      		/* chaine pour l'info et l'heritage     */
	int 	operation=0;		/* for the menu management (not used)	*/
	int     centre_X=0, centre_Y=0; /* the center of the cylindrical coord.	*/
        int     R;                      /* radius of a temp circle		*/
        int     x,y;                    /* intermediaires               	*/
	int	bool_average;		/* boolean for averaging ?		*/
        float   moyenne;                /* deroulement de l'image       	*/
        float   theta,R2;		/* intermediaires 			*/
	int	R_travail;		/* intermediaire 			*/
	float	theta_depart=M_PI;
	int 	taille_c=512, 		/* nbre de points par cercle en coordonnees polaires */
		taille_R=512,		/* nbre de cercles concentriques 	*/
		taille_X=512, 		/* nbre de points en X (rolling case)	*/
		taille_Y=512;		/* nbre de points en Y (rolling case)	*/


	if(updating_menu_state != 0)	
	{    	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)  
			active_menu->flags |=  D_DISABLED;
		else active_menu->flags &= ~D_DISABLED;
    		if (imr->n_oi < 1 || ois->im.data_type == IS_COMPLEX_IMAGE) 
    			active_menu->flags |=  D_DISABLED;
		else active_menu->flags &= ~D_DISABLED;
		return D_O_K;	
	} 

//	operation = active_menu->flags & 0xFFFFFF00;

	if (active_menu->dp == NULL)	return D_O_K;
	if (strcmp(active_menu->dp,"ROLL") == 0) 		operation = LOTUS_ROLL;
	else if (strcmp(active_menu->dp,"UNROLL") == 0) 	operation = LOTUS_UNROLL;
	else return D_O_K;		

	
	if (key[KEY_LSHIFT])
	return win_printf_OK("To transform a cylindrical image into a cartesian one\n"
				"The source image is conserved.\n"
				"The parameters are stored in the destination image.");

	
	if ( (operation!=LOTUS_UNROLL) && (operation!=LOTUS_ROLL) )
				return (win_printf_OK("You asked for something impossible !"));
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)  
				return (win_printf_OK("I don't find the image!"));						
	if (imr == NULL)	return (win_printf_OK("I've lost the image !"));						
	if (ois->im.data_type == IS_COMPLEX_IMAGE) 
				return (win_printf_OK("I cannot treat complex image !"));
	ps = ois->im.pixel;
	if (ps == NULL) 	return (win_printf_OK("I cannot find any data in image"));
	
	if ( (operation==LOTUS_ROLL) && (ois->im.data_type != IS_FLOAT_IMAGE) ) 
				return(win_printf_OK("I cannot roll back non-float image !"));
	nx = ois->im.nx;  ny = ois->im.ny; 		/* image dimensions		 */
	bool_average=0;  
	
	if (ois->im.use.stuff == NULL || ois->im.use.to != IS_OHY)
	{
		as = (ohy_s*) calloc(1, sizeof(ohy_s));
		ois->im.use.to = IS_OHY;
		ois->im.use.stuff = (void*)as;
	}
	else	as = (ohy_s*)ois->im.use.stuff;

	if (as->Xcenter != 0)   { centre_X=as->Xcenter; }
	else			{ centre_X=(int)(nx/2); }
	if (as->Ycenter != 0)	{ centre_Y=as->Ycenter; }
	else			{ centre_Y=(int)(ny/2); }


	{	i = win_scanf("Position of the center x= %d and y= %d",&centre_X,&centre_Y);
		if (i == CANCEL)	return OFF;
		if ((centre_X >= nx) || (centre_Y >= ny)) return(win_printf_OK("Center is off the image !"));
	}

	if (operation==LOTUS_ROLL)
	{	i = win_scanf("Dimensions of the destination unrolled image\n in \\theta %d and in R %d"
				" averaging ? (0 or 1) %d", &taille_c,&taille_R,&bool_average);
	}
	else 
        {	i = win_scanf("Dimensions of the destination rolled-back image\n in X %d and in Y %d"
				" averaging ? (0 or 1) %d", &taille_X,&taille_Y,&bool_average);
		
	}
	if (i == CANCEL)	return OFF;

	if (operation==LOTUS_UNROLL) { oid = create_one_image(taille_c ,taille_R, IS_FLOAT_IMAGE); }
	else 			  { oid = create_one_image(taille_X ,taille_Y, IS_FLOAT_IMAGE); }	
	if (oid == NULL)	return win_printf_OK("I cannot create the destination image!");
	pd = oid->im.pixel;

	if (operation==LOTUS_UNROLL)
	{ R_travail=min_in( min_in(centre_X, nx - centre_X), min_in(centre_Y, ny - centre_Y) );
 	  if (bool_average!=0) { if ((centre_X + R_travail + 1 >= nx) || (centre_Y + R_travail + 1 >= ny)) 
	  			 {R_travail=R_travail-1;} }
	  /* this test prevents the averaging process from taking a point outside the image */
	}
	else
	{ R_travail=min_in( min_in(centre_X, taille_X - centre_X), min_in(centre_Y, taille_Y - centre_Y) );
	}

/* step 2 : mathematical part : */
	if (operation==LOTUS_UNROLL)
	{	if (ois->im.data_type == IS_CHAR_IMAGE)
		{for (R=1; R<=taille_R; R++)
	        {       R2=(R * R_travail) / taille_R;
        	        for (k=0; k<taille_c; k++)
                	{       theta=k*(2*PI/taille_c);
                        	x= (int)( centre_X + R2*cos(theta+theta_depart) );
	                        y= (int)( centre_Y + R2*sin(theta+theta_depart) );
        	                if (bool_average!=0)  { moyenne = ( ps[y].ch[x] + ps[y+1].ch[x+1]
				                                  + ps[y].ch[x+1] + ps[y+1].ch[x] ) /4;
							pd[R-1].fl[k] = moyenne;
						      }
				else            	pd[R-1].fl[k] = ps[y].ch[x] ;
        	        }; /*for k*/
	        } /*for R*/
		} /*if*/
		else if (ois->im.data_type == IS_INT_IMAGE)
		{for (R=1; R<=taille_R; R++)
	        {       R2=(R * min_in( min_in(centre_X, nx - centre_X),  
				        min_in(centre_Y, ny - centre_Y)) ) / taille_R;
                	for (k=0; k<taille_c; k++)
	                {       theta=k*(2*PI/taille_c);
        	                x= (int)( centre_X + (int)R2*cos(theta+theta_depart) );
                	        y= (int)( centre_Y + (int)R2*sin(theta+theta_depart) );
                        	if (bool_average!=0)  { moyenne = ( ps[y].in[x] + ps[y+1].in[x+1]
                                	  			  + ps[y].in[x+1] + ps[y+1].in[x] ) /4;
							pd[R-1].fl[k]= moyenne;
						      }
	                        else 			pd[R-1].fl[k]= ps[y].in[x];
        	        }; /*for k*/
	        } /*for R*/
		} /*else if*/
		else if (ois->im.data_type == IS_FLOAT_IMAGE)
		{for (R=1; R<=taille_R; R++)
	        {       R2=(R * min_in( min_in(centre_X, nx - centre_X),  
				        min_in(centre_Y, ny - centre_Y)) ) / taille_R;
                	for (k=0; k<taille_c; k++)
	                {       theta=k*(2*PI/taille_c);
        	                x= (int)( centre_X + (int)R2*cos(theta+theta_depart) );
                	        y= (int)( centre_Y + (int)R2*sin(theta+theta_depart) );
                        	if (bool_average!=0)  { moyenne = ( ps[y].fl[x] + ps[y+1].fl[x+1]
			                                  	  + ps[y].fl[x+1] + ps[y+1].fl[x] ) /4;
							pd[R-1].fl[k]= moyenne;
						      }	
        	                else 			pd[R-1].fl[k]= ps[y].fl[x];
                	}; /*for k*/
	        } /*for R*/
		} /*else if*/
	}
	else /* ie "rolling" */
	{	for (x=0; x<taille_X; x++)
	        {       for (y=0; y<taille_Y; y++)
	                {	if (x!=centre_X)	{ theta = atan2( y-centre_Y, x-centre_X); }
				else 	if (y>centre_Y)	{ theta = PI/2; }
					else 		{ theta	= -PI/2;}
				if (abs(cos(theta)) >= 0.5 ) 	{ R2 = (x-centre_X)/cos(theta); }
				else			 	{ R2 = (y-centre_Y)/sin(theta); }
				theta = (theta + theta_depart) / (2*PI) * nx;
				R2    = R2 * ny / R_travail;
				if (R2<0) win_printf_OK("debug - ca va merder !!! (x=%d, y=%d)",x,y);
				theta = (theta<0)   ? theta+2*PI : theta;
				theta = (theta>=nx) ? theta-2*PI : theta;
				if ((theta<0) || (theta>=nx)) win_printf_OK("debug - ca va merder !!! (theta=%f)",theta);
				if (R2>=ny)	{ pd[y].fl[x] = 0; }
				else		{ R=(int)R2;
						  k=(int)theta;
						  pd[y].fl[x] = ps[R].fl[k]; 
						}
                	}; /*for y*/
	        } /*for x*/
//		if (NG_DEBUG_IM2==1) win_printf_OK("debug - rolling back - part I ended correctly"); 
		if (bool_average!=0) /* a trick for "smoothing" easily (one pass)
					and exactely the same way as in the unrolling */
		{	for (x=0; x<taille_X-1; x++)
	       		for (y=0; y<taille_Y-1; y++)
	       		{	pd[y].fl[x] += pd[y].fl[x+1] + pd[y+1].fl[x] + pd[y+1].fl[x+1]; 
				/* the 3 quantities of the right member are yet unaltered ! 		*/
				pd[y].fl[x] /=4;				
                	};/*for for*/
		}/*if*/
	}
//	if (NG_DEBUG_IM2==1) win_printf_OK("debug - math part ended");

/* step 3 : presentation of the results */
	/* getting the title of the source image, and protection against too long names */
	if ( (ois->title != NULL) && (strlen(ois->title) < MAX_TITLE_STRING_SIZE) )
	{	strcpy(c1, ois->title);	}
	else {	if ( (ois->im.source != NULL) && (strlen(ois->im.source) < MAX_TITLE_STRING_SIZE) )
			strcpy(c1, ois->im.source);
		else	c1[0] = 0;	}

	if (operation==LOTUS_UNROLL) 
	{ set_im_title(oid,"unrolled %s", c1); 
	  set_formated_string(&oid->im.treatement,"unrolled"); 
	  oid->im.win_flag |= X_PER; }			/* because the image is periodic in theta !	*/
	else
	{ set_im_title(oid,"%s rolled back", c1); 
	  set_formated_string(&oid->im.treatement,"rolled"); }	


	if (oid->im.use.stuff == NULL || oid->im.use.to != IS_OHY)
	{
		asd = (ohy_s*) calloc(1, sizeof(ohy_s));
		oid->im.use.to = IS_OHY;
		oid->im.use.stuff = (void*)asd;
	}
	else	asd = (ohy_s*)oid->im.use.stuff;

	asd->Xcenter=centre_X;	// the center used is stored
	asd->Ycenter=centre_Y;
	asd->radius =R_travail;	// the radius used is stored


	inherit_from_im_to_im(oid, ois);
//	uns_oi_2_oi(oid,ois);

	add_image(imr, oid->type, (void*)oid);
	find_zmin_zmax(oid);
	return (refresh_image(imr, (imr->n_oi - 1) ));

} /* end of the function "do_lotus_deplie" */





/********************************************************************************/
/* Reconstruct complex image : 							*/
/* 	construct an image of complexes with 3 real images of			*/
/*			- the amplitude A					*/
/*			- the phase derivate in x : kx				*/
/*			- the phase derivate in y : ky				*/
/********************************************************************************/
int do_reconstruct_complex_image(void)
{
	register int i, j, k;  		/* intermediaire compteurs de boucle    */
	imreg 	*imr;		      	/* image region 			*/
	O_i 	*ois1, *ois2, *ois3, *oid;	/* one image source (x3) et destination */
	union pix *ps1, *ps2, *ps3, *pd;  	/* data des images (matrice) 	       	*/
	int	nx1, nx2, nx3, nx;	/* dimensions en x 		   	*/
	int 	ny1, ny2, ny3, ny;	/* dimensions en y 		    	*/
	char  	c2[128];		/* chaine pour l'info et l'heritage 	*/
	float	phase_depart = 0;	/* phase at starting point x0, y0	*/
	int	x_0, y_0;		/* starting point for phase calculation */
	float	phase_old=0, phase_new=0;	/* intermediary */


	if(updating_menu_state != 0)	
	{    	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois1) != 2)  
			active_menu->flags |=  D_DISABLED;
		else active_menu->flags &= ~D_DISABLED;
    		if (imr->n_oi < 3) 
    			active_menu->flags |=  D_DISABLED;
		else active_menu->flags &= ~D_DISABLED;
		return D_O_K;	
	} 

	if (key[KEY_LSHIFT])
		return win_printf_OK("Construct a complex image using 3 real images of :\n"
				" i   : amplitude \n"
				" i+1 : \\Phi_x \n"
				" i+2 : \\Phi_y \n"
				"The source images should be placed in this hierarchy\n"
				"and the active one be the last.");

	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois1) != 2)  
        	return (win_printf_OK("I cannot find any image !"));
	if (imr->n_oi < 3)
		return (win_printf_OK("Not enough images to perform that !"));

	i = imr->cur_oi; 		/* on recupere le numero de l'image active 	*/
	ois1 = imr->o_i[i];  		/* on recupere la o_i numero i			*/
	ps1 = imr->o_i[i]->im.pixel;	/* on recupere les data de l'image n�i 		*/

	i = ( (i-1 < 0) ? (imr->n_oi-1) : (i-1) ); /* preceding image (with CL)		*/
	ois2 = imr->o_i[i]; 		/* getting image n�i-1				*/
	ps2 = imr->o_i[i]->im.pixel;	/* on recupere les data de l'image n�i-1	*/

	i = ( (i-1 < 0) ? (imr->n_oi-1) : (i-1) ); /* preceding image (with CL)		*/
	ois3 = imr->o_i[i]; 		/* on recupere la deuxieme image n�i-2		*/
	ps3 = imr->o_i[i]->im.pixel;	/* on recupere les data de l'image n�i-2	*/

	if (ps1 == NULL || ps2 == NULL || ps3 == NULL)
		return (win_printf_OK("I cannot find any data in image"));
						
	nx1 = ois1->im.nx;  ny1 = ois1->im.ny;  /* dimensions de l'image 1 (ky)		*/
	nx2 = ois2->im.nx;  ny2 = ois2->im.ny;  /* dimensions de l'image 2 (kx)		*/
	nx3 = ois3->im.nx;  ny3 = ois3->im.ny;  /* dimensions de l'image 3 (A)		*/

	if ( (nx1!=nx2) || (nx2!=nx3) || (nx3!=nx1) ) return(win_printf_OK("images have different number of columns !"));
	if ( (ny1!=ny2) || (ny2!=ny3) || (ny3!=ny1) ) return(win_printf_OK("images have different number of lines !"));
	nx=nx1; ny=ny1;

	if ( (ois1->im.data_type != IS_FLOAT_IMAGE)
	  || (ois1->im.data_type != IS_FLOAT_IMAGE)
	  || (ois1->im.data_type != IS_FLOAT_IMAGE) )  return(win_printf_OK("Images must be REAL !"));

	x_0 = (int)(nx/2);
	y_0 = (int)(ny/2);
	i = win_scanf("Enter the coordinates of the starting\n point (in pixels units) :\n X_0 = %d Y_0 = %d",
							&x_0,&y_0);
	
	if (i == CANCEL)	return OFF;
	if ( (x_0>=nx) || (y_0>=ny) || (x_0<0) || (y_0<0) )
		return(win_printf_OK("Point is of the image !\n X_0 in [0,%d] and Y_0 in [0,%d]",nx-1,ny-1));

	oid = create_one_image(nx ,ny, IS_COMPLEX_IMAGE); 
	if (oid == NULL)	return win_printf_OK("I cannot create the destination image!");
	pd  = oid->im.pixel;

	/* construction of a phase image with the kx data previliged */
	/* under this comment : version with starting point at (0,0)			*/
/*	for (i=0; i<ny; i++) 
		{	if (i==0) { pd[i].fl[2*0] = phase_depart; }
			else	  { pd[i].fl[2*0] = phase_old + ps1[i].fl[0]/ny; }
			phase_old = pd[i].fl[2*0];
			for (j=0; j<(nx-1); j++)
			{	k = (j+1) % nx;
				pd[i].fl[2*k] 	= pd[i].fl[2*j] + ps2[i].fl[j]/nx;
			}
		}
*/
	/* under this comment : version with starting point (x0,y0)			*/
	for (i=y_0; i<ny; i++) /* construction of a phase image with the kx data previliged */
		{	if (i==y_0) { pd[i].fl[2*x_0] = phase_depart; }
/*			else	    { pd[i].fl[2*x_0] = phase_old + ps1[i].fl[x_0]/ny; } AC 22.04.99 */
			else	    { pd[i].fl[2*x_0] = phase_old + ps1[i-1].fl[x_0]/ny; }
			phase_old = pd[i].fl[2*x_0];
			for (j=x_0; j<(nx-1); j++)
			{	k = (j+1) % nx;
				pd[i].fl[2*k] 	= pd[i].fl[2*j] + ps2[i].fl[j]/nx;
			}/* end of first quadrant */
			for (j=x_0; j>0; j--)
			{	k = (j-1) % nx;
/*				pd[i].fl[2*k] 	= pd[i].fl[2*j] - ps2[i].fl[j]/nx; AC 22.04.99 */
				pd[i].fl[2*k] 	= pd[i].fl[2*j] - ps2[i].fl[k]/nx;
			}/* end of second quadrant */
		}
	for (i=y_0; i>=0; i--) /* construction of a phase image with the kx data previliged */
		{	if (i==y_0) { pd[i].fl[2*x_0] = phase_depart; }
			else	    { pd[i].fl[2*x_0] = phase_old - ps1[i].fl[x_0]/ny; }
			phase_old = pd[i].fl[2*x_0];
			for (j=x_0; j<(nx-1); j++)
			{	k = (j+1) % nx;
				pd[i].fl[2*k] 	= pd[i].fl[2*j] + ps2[i].fl[j]/nx;
			}/* end of 3th quadrant */
		for (j=x_0; j>0; j--)
			{	k = (j-1) % nx;
/*				pd[i].fl[2*k] 	= pd[i].fl[2*j] - ps2[i].fl[j]/nx; AC 22.04.99 */
				pd[i].fl[2*k] 	= pd[i].fl[2*j] - ps2[i].fl[k]/nx;
			}/* end of 4th quadrant */
		}

	/* test de precision sur la premiere ligne : */
	if (ois1->im.win_flag & X_PER) 
	{	phase_old = pd[0].fl[0] - pd[0].fl[2*(nx-1)] + ps2[0].fl[nx-1]/nx;
		phase_old = fmod(phase_old,2*PI);
		win_printf_OK("precision : %f",phase_old);
	}
	for (i=0; i<ny; i++) /* construction of the complete complex image */
			for (j=0; j<nx; j++)
			{	phase_new = pd[i].fl[2*j];
				pd[i].fl[2*j]   = ps3[i].fl[j] * cos(phase_new*2*PI);
				pd[i].fl[2*j+1] = ps3[i].fl[j] * sin(phase_new*2*PI);
			}
		
	if (ois2->im.source != NULL)	{ strcpy(c2, ois2->im.source);}
	else				{ strcpy(c2, "complex"); } /* recuperation de la source de l'image 2 */
	set_im_title(oid,"reconstructed image %s", c2);
	
	inherit_from_im_to_im(oid,ois1); 	/* heritage global des proprietes de l'image 1 */
	uns_oi_2_oi(oid,ois1);			/* heritage des unites de l'image 1 */
	oid->im.win_flag = ois1->im.win_flag;	/* heritage des boundary condition (periodic or not) de l'image 1 */

	add_image(imr, oid->type, (void*)oid);	/* on place la nouvelle image dans l'image region */
	find_zmin_zmax(oid);			/* recherche du meilleur contraste pour l'affichage */
	return (refresh_image(imr, (imr->n_oi - 1) ));	/* retour et rafraichissement video */

} /* end of the "do_reconstruct_complex_image" function */






/********************************************************************************/
/* surimpression d'un lieu geometrique simple (ligne, colone, cercle) sur une  	*/
/* 	image									*/
/*		28.04.1999							*/
/* 	aucune image n'est cree : on travaille avec the old one 		*/
/********************************************************************************/
int 	do_lotus_printlocus(void)
{
	register int i;             	/* intermediaire compteur de boucle	*/
	imreg 	*imr;		     	/* image region 			*/
	O_i 	*ois;    		/* one image source and destination 	*/
	union 	pix *ps;   		/* data des images (matrice) 	       	*/
	int	nx, ny;		       	/* dimensions en x, y			*/
	int 	operation;
	int 	centre_X=256,centre_Y=256,/* position of the object		*/
		R,npts,x,y,color;	/* other intermediates			*/
	float 	theta;			/* angle 				*/
	ohy_s 	*as;			/* use 					*/
//	char	s[128];
	

	if(updating_menu_state != 0)	return D_O_K;	
	
	if (key[KEY_LSHIFT])
		return win_printf_OK("Over-print an object on an image\n"
				"\n"
				"It does not work with complexes\n"
				"The source image is worked over.\n"
				"The coordinates are kept as specials.");

	operation = active_menu->flags & 0xFFFFFF00;

	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)  
        	return (win_printf_OK("I cannot find any image !"));

	if (imr == NULL)	return (win_printf_OK("I cannot find any image !"));
	ps = ois->im.pixel;		/* getting the image n�i data			*/
	if (ps == NULL)		return (win_printf_OK("I cannot find any data in image"));
	if (ois->im.data_type==IS_COMPLEX_IMAGE) return(win_printf_OK("I cannot work with complexes"));

	color=ois->z_max;

	nx = ois->im.nx;  ny = ois->im.ny; /* image dimensions		 		*/

	if (ois->im.use.stuff == NULL || ois->im.use.to != IS_OHY)
	{	as = (ohy_s*) calloc(1, sizeof(ohy_s));
		ois->im.use.to = IS_OHY;
		ois->im.use.stuff = (void*)as;
	}
	else	as = (ohy_s*)ois->im.use.stuff;

	if (operation & LOTUS_CIRCLE)
	{	
		centre_X=as->Xcenter;
		centre_Y=as->Ycenter;
		R     = 70;
		npts  = 512;
		i=win_scanf("let's print a circle over the image...\n center x : %d center Y : %d radius   : %d "
				"nb. pts. : %d grey level : %d",
				&centre_X,&centre_Y,&R,&npts,&color);
		if (i==CANCEL) return(OFF);
		if ( (centre_X-R<0) || (centre_X+R>=nx) || (centre_Y-R<0) || (centre_Y+R>=ny) ) 
			return( win_printf_OK("circle is out of the image !") );
		for (i=0; i<npts; i++)
		{ 	theta=2*PI*i/npts;
			x=(int)(centre_X+R*cos(theta));
			y=(int)(centre_Y+R*sin(theta));
			if (ois->im.data_type==IS_CHAR_IMAGE)       ps[y].ch[x]=(int)color;
			else if (ois->im.data_type==IS_INT_IMAGE)   ps[y].in[x]=(int)color;
			else if (ois->im.data_type==IS_FLOAT_IMAGE) ps[y].fl[x]=(float)color;
		}
		as->Xcenter=centre_X;	// the center used is stored
		as->Ycenter=centre_Y;
		as->radius =R;		// the radius used is stored

/*		sprintf(s,"R=%d",R);
		ng_set_image_label(ois, s, 6);	// put a white label
*/
	}
	else if (operation & LOTUS_LINE)
	{	
		centre_Y=as->Ycenter;
		i=win_scanf("let's print a line over the image...\n line number : %d grey level : %d",
				&centre_Y,&color);
		if (i==CANCEL) return(OFF);
		if ( (centre_Y<0) || (centre_Y>=ny) ) 
			return( win_printf_OK("line is out of the image !") );
		if (ois->im.data_type==IS_CHAR_IMAGE)       
			for (i=0; i<nx; i++) ps[centre_Y].ch[i]=(int)color;
		else if (ois->im.data_type==IS_INT_IMAGE)   
			for (i=0; i<nx; i++) ps[centre_Y].in[i]=(int)color;
		else if (ois->im.data_type==IS_FLOAT_IMAGE) 
			for (i=0; i<nx; i++) ps[centre_Y].fl[i]=(float)color;
		as->Ycenter=centre_Y; /* kind a sort of a backup */	
	}
	else if (operation & LOTUS_COLOMN)
	{	
		centre_X=as->Xcenter;
		i=win_scanf("let's print a column over the image...\n column number : %d grey level : %d",
				&centre_X,&color);
		if (i==CANCEL) return(OFF);
		if ( (centre_X<0) || (centre_X>=nx) ) 
			return( win_printf_OK("column is out of the image !") );
		if (ois->im.data_type==IS_CHAR_IMAGE)       
			for (i=0; i<ny; i++) ps[i].ch[centre_X]=(int)color;
		else if (ois->im.data_type==IS_INT_IMAGE)   
			for (i=0; i<ny; i++) ps[i].in[centre_X]=(int)color;
		else if (ois->im.data_type==IS_FLOAT_IMAGE) 
			for (i=0; i<ny; i++) ps[i].fl[centre_X]=(float)color;
		as->Xcenter=centre_X; /* kind a sort of a backup */		
	}
	else 	return(win_printf_OK("bad operation..."));

	find_zmin_zmax(ois);			/* recherche du meilleur contraste pour l'affichage */
	return (refresh_image(imr, imr->cur_oi ));		/* retour et rafraichissement video */

} /* end of the "do_lotus_printlocus" function */






MENU *lotus_image_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;

	add_item_to_menu(mn,"Unroll photograph", 		do_lotus_deplie,		NULL, 0, "UNROLL");
	add_item_to_menu(mn,"Roll back photograph", 		do_lotus_deplie,		NULL, 0, "ROLL");
	add_item_to_menu(mn,"Overprint circle",			do_lotus_printlocus, 		NULL, LOTUS_CIRCLE, NULL);
	add_item_to_menu(mn,"Overprint line",			do_lotus_printlocus, 		NULL, LOTUS_LINE,   NULL);
	add_item_to_menu(mn,"Overprint colomn",			do_lotus_printlocus, 		NULL, LOTUS_COLOMN, NULL);
	add_item_to_menu(mn,"Reconstruct complex image", 	do_reconstruct_complex_image,	NULL, 0, NULL);
	return mn;
}



int	lotus_main(int argc, char **argv)
{
	add_image_treat_menu_item ("Lotus stuff", 		NULL, lotus_image_menu(), 0, NULL);
	return D_O_K;
}



int	lotus_unload(int argc, char **argv)
{ 
	remove_item_to_menu(image_treat_menu,"Lotus stuff",	NULL, NULL);
	return D_O_K;
}
 
#endif 


