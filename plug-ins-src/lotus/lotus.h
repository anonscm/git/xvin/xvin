#ifndef _LOTUS_H_
#define _LOTUS_H_

#include <stdio.h>


#define LOTUS_ROLL 	1
#define LOTUS_UNROLL 	2
#define LOTUS_LINE	0x000100
#define LOTUS_COLOMN	0x001000
#define LOTUS_CIRCLE	0x010000

/************************************************************************/
/* Procedures :								*/
/************************************************************************/
PXV_FUNC(int, do_lotus_deplie,			(void));
PXV_FUNC(int, do_reconstruct_complex_image,  	(void));
PXV_FUNC(int, do_lotus_printlocus,		(void));

PXV_FUNC(MENU*, lotus_image_menu, (void));
PXV_FUNC(int, lotus_main, 	(int argc, char **argv));
PXV_FUNC(int, lotus_unload, 	(int argc, char **argv));

#endif
