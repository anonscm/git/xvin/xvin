#ifndef _DIFF_H_
#define _DIFF_H_


#define	DERIVATION_METHOD_FINITE_DIFFERENCES_ORDRE1	0x0100
#define	DERIVATION_METHOD_FINITE_DIFFERENCES_ORDRE2	0x0200
#define	DERIVATION_METHOD_FOURIER				0x0300
#define	DERIVATION_METHOD_SPLINE				0x0400

PXV_FUNC(MENU*, diff_plot_menu, 	(void));
PXV_FUNC(int, diff_main, 		(int argc, char **argv));
PXV_FUNC(int, diff_unload,	(int argc, char **argv));

PXV_FUNC(int,    diff_1o,				(double *x, int nx, double dt, double *dx));
PXV_FUNC(int,    diff_1o_float,			(float  *x, int nx, double dt, float  *dx));
PXV_FUNC(int,    diff_2o,				(double *x, int nx, double dt, double *dx));
PXV_FUNC(int,    diff_2o_float,			(float  *x, int nx, double dt, float  *dx));
PXV_FUNC(int,    diff_splines,			(double *t, double *x, int nx, double *dx));
PXV_FUNC(int,    diff_splines_float,	(float  *t, float  *x, int nx, float  *dx));
PXV_FUNC(int,    diff_fourier,			(double *x, int nx, double dt, double *dx, int bool_hanning));
PXV_FUNC(int,    diff_fourier_float,	(float *x, int nx, double dt, float *dx, int bool_hanning));
PXV_FUNC(int,	 do_diff,				(void));


#endif

