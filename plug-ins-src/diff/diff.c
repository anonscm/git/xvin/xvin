#ifndef _DIFF_C_
#define _DIFF_C_

#include "xvin.h"
#include "fft_filter_lib.h"



#include <gsl/gsl_spline.h>
#include <fftw3.h>

/* If you include other plug-ins header do it here*/ 

/* But not below this define */
#define BUILDING_PLUGINS_DLL
#include "diff.h"



// derive facon Matlab
int diff_1o(double *x, int nx, double dt, double *dx)
{	register int i;
	double f;
	f = 1./dt;
	for (i=0; i<nx-1;i++)
		{	dx[i] = f*(x[i+1]-x[i]);
		}
	return(nx-1);
} //end of diff_1o


int diff_1o_float(float *x, int nx, double dt, float *dx)
{	register int i;
	double f;
	double *x_tmp;
	
	x_tmp=(double*)calloc(nx, sizeof(double));
	for (i=0; i<nx; i++)
	{	x_tmp[i] = (double) x[i];
	}
	
	f = 1./dt;
	for (i=0; i<nx-1;i++)
		{	dx[i] = (float)(f*(x_tmp[i+1]-x_tmp[i]));
		}
	free(x_tmp);
	return(nx-1);
} //end of diff_1o_float


//derive ordre 2
int diff_2o(double *x, int nx, double dt, double *dx)
{	register int i;
	double f;
	f=1./(2.*dt);
	for (i=1; i<nx-1;i++)
		{dx[i] = f*(x[i+1]-x[i-1]);
		}
	return(nx-2);
} //end of diff_2o


//derive ordre 2
int diff_2o_float(float *x, int nx, double dt, float *dx)
{	register int i;
	double f;
	double *x_tmp;
	x_tmp=(double*)calloc(nx, sizeof(double));
	for (i=0; i<nx; i++)
	{	x_tmp[i] = (double) x[i];
	}

	f=1./(2.*dt);
	for (i=1; i<nx-1;i++)
		{dx[i] = (float)(f*(x_tmp[i+1]-x_tmp[i-1]));
		}
	free(x_tmp);
	return(nx-2);
} //end of diff_2o

// derive with splines
// x has nx points
// dx should also have nx points.
// be carefull, as points 0 and nx-1 are badly evaluated and should not be used
int diff_splines(double *t, double *x, int nx, double *dx) 
{	register int i;
	double *t_tmp,*x_tmp;
	gsl_interp_accel *acc;
	gsl_spline	*spline;
	
	acc = gsl_interp_accel_alloc();
	spline = gsl_spline_alloc(gsl_interp_cspline, nx);
	gsl_spline_init(spline, t, x, nx);
	t_tmp=(double*)calloc(nx-1, sizeof(double));
	x_tmp=(double*)calloc(nx-1, sizeof(double));
	
	for (i=0; i<nx-1; i++)
	{	t_tmp[i] = (t[i]+t[i+1])/2.;
		x_tmp[i] = gsl_spline_eval_deriv(spline, t_tmp[i], acc);
	}
	
	gsl_spline_free(spline);
	gsl_interp_accel_free(acc);

	acc    = gsl_interp_accel_alloc();
	spline = gsl_spline_alloc(gsl_interp_cspline, nx-1);
	gsl_spline_init(spline, t_tmp, x_tmp, nx-1);		
	
	for (i=0; i<nx; i++)
	{	dx[i] = gsl_spline_eval(spline, t[i] , acc);
	}
	
	gsl_spline_free(spline);
	gsl_interp_accel_free(acc);
	free(t_tmp);
	free(x_tmp);
	return(nx);
} //end of diff_splines



// derive with splines using float input and float output
// x has nx points
// dx should also have nx points.
// be carefull, as points 0 and nx-1 are badly evaluated and should not be used
int diff_splines_float(float *t, float *x, int nx, float *dx) 
{	register int i;
	double *t_tmp,*x_tmp, *dx_tmp;
	gsl_interp_accel *acc;
	gsl_spline	*spline;
	
	t_tmp=(double*)calloc(nx, sizeof(double));
	x_tmp=(double*)calloc(nx, sizeof(double));
	for (i=0; i<nx; i++)
	{	t_tmp[i] = (double) t[i];
		x_tmp[i] = (double) x[i];
	}
	
	acc = gsl_interp_accel_alloc();
	spline = gsl_spline_alloc(gsl_interp_cspline, nx);
	gsl_spline_init(spline, t_tmp, x_tmp, nx);
	
	dx_tmp=(double*)calloc(nx-1, sizeof(double));
	for (i=0; i<nx-1; i++)
	{	t_tmp[i] = (t_tmp[i]+t_tmp[i+1])/2.;
       dx_tmp[i] = gsl_spline_eval_deriv(spline, t_tmp[i], acc);
	}
	
	gsl_spline_free(spline);
	gsl_interp_accel_free(acc);

	acc    = gsl_interp_accel_alloc();
	spline = gsl_spline_alloc(gsl_interp_cspline, nx-1);
	gsl_spline_init(spline, t_tmp, dx_tmp, nx-1);
		
	for (i=0; i<nx; i++)
	{	dx[i] =(float)gsl_spline_eval(spline, (double)t[i] , acc);
	}
	
	gsl_spline_free(spline);
	gsl_interp_accel_free(acc);
	free(t_tmp);
	free(x_tmp);
	free(dx_tmp);
	return(nx);
} //end of diff_splines_float



// derive with Fourier
int diff_fourier(double *x, int nx, double dt, double *dx, int bool_hanning)
{	register int i;
	double f;
	double tmp, c;
	fftw_complex 	*ft;
	fftw_plan 	plan;
			
	f=1./dt;
	
	
	ft = fftw_malloc((nx/2+1)   *sizeof(fftw_complex));
		
	if (bool_hanning==1) fft_window_real(nx, x, 0.001);

	plan = fftw_plan_dft_r2c_1d(nx, x, ft, FFTW_ESTIMATE);
	fftw_execute(plan); 
	fftw_destroy_plan(plan);

	for (i=0; i<(nx+1)/2; i++)	// the fft has nx/2+1 complex points
        {	tmp = (double)i*f/nx*2.*M_PI; 
            c   = ft[i][0];
			ft[i][0] = -ft[i][1]*tmp;
			ft[i][1] =  c*tmp;
		}
	if (nx%2==0) 
		{ 	ft[nx/2][0]=0.;
			ft[nx/2][1]=0.;
		}

	plan = fftw_plan_dft_c2r_1d(nx, ft, dx, FFTW_ESTIMATE);   
	fftw_execute(plan); 
	fftw_destroy_plan(plan);

	if (bool_hanning==1) fft_unwindow_real(nx, dx, 0.001);
	
	for (i=0; i<nx; i++)
		{dx[i] /= (double)nx;
		}

	fftw_free(ft);
	return(nx);
}// End of diff_fourier



// derive with Fourier
int diff_fourier_float(float *x, int nx, double dt, float *dx, int bool_hanning)
{	register int i;
	double f;
	double tmp, c;
	double		*tmp_x;
	fftw_complex 	*ft;
	fftw_plan 	plan;
			
	f=1./dt;
	
	tmp_x= fftw_malloc(  nx     *sizeof(double)); 		for (i=0; i<nx; i++) tmp_x[i]=(double)x[i];
	ft   = fftw_malloc( (nx/2+1)*sizeof(fftw_complex));
	
	if (bool_hanning==1) fft_window_real(nx, tmp_x, 0.001);

	plan = fftw_plan_dft_r2c_1d(nx, tmp_x, ft, FFTW_ESTIMATE);
	fftw_execute(plan); 
	fftw_destroy_plan(plan);

	for (i=0; i<(nx+1)/2; i++)	// the fft has nx/2+1 complex points
        {	tmp = (double)i*f/nx*2.*M_PI;
            c   = ft[i][0];
			ft[i][0] = -ft[i][1]*tmp;
			ft[i][1] =         c*tmp;
		}
	if (nx%2==0) 
		{ 	ft[nx/2][0]=0.;
			ft[nx/2][1]=0.;
		}

	plan = fftw_plan_dft_c2r_1d(nx, ft, tmp_x, FFTW_ESTIMATE);   
	fftw_execute(plan); 
	fftw_destroy_plan(plan);

	if (bool_hanning==1) fft_unwindow_real(nx, tmp_x, 0.001);
	
	for (i=0; i<nx; i++) { dx[i] = (float)((tmp_x[i]/(double)nx)); }

	fftw_free(ft);
	fftw_free(tmp_x);
	return(nx);
}// End of diff_fourier

//Function do_diff (2006/11/10)
int do_diff(void)
{	O_p 	*op1 = NULL;
	d_s 	*ds1, *ds2=NULL;
	pltreg *pr = NULL;
	double precision=1.0e-5;
	int		index;
	int ny, j;
	register int i;
	int 	bool_hanning=1;
	double dt;
	float *dy;
	
	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])	
		return win_printf_OK("This routine plots the derivative of the dataset.\n\n"
							"a new dataset is created.");
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)	return win_printf_OK("cannot plot region");
	ny = ds1->ny;	// number of points in time series 


	if (ny<4) 						return(win_printf_OK("not enough points !"));
//Test if dt is constant
	dt = fabs((double)(ds1->xd[ny-1]-ds1->xd[0])/((double)(ny-1)));
	if ( fabs(dt-fabs((double)(ds1->xd[2]-ds1->xd[1]))) > (dt*precision) ) 
       if (win_printf("dt is not constant!\n %g vs %g\n\n click CANCEL to abort", 
                              fabs(dt-(ds1->xd[2]-ds1->xd[1])), dt*precision*ny)==CANCEL)
          return(D_O_K);
	if ( fabs(dt-fabs((double)ds1->xd[1]-ds1->xd[0])) > (dt*precision) ) 
       if (win_printf("dt is not constant!\n %g vs %g\n\n click CANCEL to abort", 
                                fabs(dt-(ds1->xd[1]-ds1->xd[0])), dt*precision*ny)==CANCEL)
          return(D_O_K);
//end of test
    	
	if (dt==0)				return(win_printf_OK("dt is zero !"));
	
	if (index==DERIVATION_METHOD_FINITE_DIFFERENCES_ORDRE1)
	{	ds2 = create_and_attach_one_ds (op1, ny-1, ny-1, 0);
		j=diff_1o_float(ds1->yd, ny, dt, ds2->yd);
		if (j!=ny-1)  return(win_printf_OK("derivation error"));
		for (i=0; i<j; i++)
		{	ds2->xd[i]=(ds1->xd[i+1]+ds1->xd[i])/2.;
		}
	}
	else if (index==DERIVATION_METHOD_FINITE_DIFFERENCES_ORDRE2)
	{	ds2 = create_and_attach_one_ds (op1, ny-2, ny-2, 0);
		dy=(float*)calloc(ny-1, sizeof(float));
		j=diff_2o_float(ds1->yd, ny, dt, dy);
		if (j!=ny-2)  return(win_printf_OK("derivation error"));
		memcpy(ds2->xd, ds1->xd+1, j *sizeof(float));
		for (i=0; i<j; i++)
		{	ds2->yd[i]=dy[i+1];
		} 
	}
	else if (index==DERIVATION_METHOD_SPLINE)
	{	ds2 = create_and_attach_one_ds (op1, ny, ny, 0);
	 	j=diff_splines_float(ds1->xd, ds1->yd, ny, ds2->yd);
		if (j!=ny) return(win_printf_OK("spline error"));
		memcpy(ds2->xd, ds1->xd, j *sizeof(float)); 
	}
	else if (index==DERIVATION_METHOD_FOURIER)
	{	if (win_scanf("%b Hanning window", &bool_hanning)==CANCEL) return(D_O_K);
		ds2 = create_and_attach_one_ds (op1, ny, ny, 0);
		j=diff_fourier_float(ds1->yd, ny, dt, ds2->yd, bool_hanning);
		if (j!=ny) return(win_printf_OK("fourier diff error"));
		memcpy(ds2->xd, ds1->xd, j *sizeof(float)); 
	}

	inherit_from_ds_to_ds(ds2, ds1);
	ds2->treatement = my_sprintf(ds2->treatement,"derivative (%s)", 
			(index==DERIVATION_METHOD_FINITE_DIFFERENCES_ORDRE1) ? "finite differences order 1" :
			(index==DERIVATION_METHOD_FINITE_DIFFERENCES_ORDRE2) ? "finite differences order 2" :
			(index==DERIVATION_METHOD_SPLINE) 	      ? "splines" :
														"Fourier");
	
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the do_diff


MENU *diff_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;

	add_item_to_menu(mn,"Compute derivative of ds (finite differences o(1))",	do_diff,	NULL, DERIVATION_METHOD_FINITE_DIFFERENCES_ORDRE1, NULL);	
	add_item_to_menu(mn,"Compute derivative of ds (finite differences o(2))",	do_diff,	NULL, DERIVATION_METHOD_FINITE_DIFFERENCES_ORDRE2, NULL);	
	add_item_to_menu(mn,"Compute derivative of ds (spline functions)",			do_diff,	NULL, DERIVATION_METHOD_SPLINE, NULL);	
	add_item_to_menu(mn,"Compute derivative of ds (Fourier)",					do_diff,	NULL, DERIVATION_METHOD_FOURIER, NULL);	
	return mn;
}




int	diff_main(int argc, char **argv)
{
	add_plot_treat_menu_item ("diff", NULL, diff_plot_menu(), 0, NULL);
	return D_O_K;
}


int	diff_unload(int argc, char **argv)
{ 
	remove_item_to_menu(plot_treat_menu,"diff",	NULL, NULL);
	return D_O_K;
}
#endif
