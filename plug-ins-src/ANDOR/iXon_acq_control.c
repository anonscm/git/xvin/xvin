/** \file ixon.c
    \brief Plug-in program for iXon control in Xvin.
    This is the master program that controls the iXon camera.
    \sa movie_statistics.c
    \author JF Allemand & A Meglio
*/
#ifndef _IXON_ACQ_CONTROL_C_
#define _IXON_ACQ_CONTROL_C_

#include "allegro.h"
#include "winalleg.h"
#include <windowsx.h>
#include "ctype.h"
#include "xvin.h"
#include "ATMCD32D.h"
#include "global_define.h"
#include "global_variables.h"
#include "global_functions.h"

//#include "iXon.h"
//#include "iXon_tape.h"
//#include "iXon_threads.h"

#define BUILDING_PLUGINS_DLL
#include "iXon_acq_control.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Live display functions
    
/** Allocates a buffer for the current video source.

\param int sizeX Width in pixels.
\param int sizeY Height in pixels.
\param int pixelDepth Type of image.
\return int Error code.
\author JF Allemand
\version 09/05/06 modified 09/2007
*/
/* int allocate_iXon_buffer(int sizeX,int sizeY,int pixelDepth) */
/* { */
/*     imreg *imr = NULL; */

/*     if (imr_iXon == NULL)  */
/*     { */
/*       return win_printf("imriXon NULL"); */
       
/*     }     */
        
/*     if (oi_iXon == NULL)  */
/*     {     */
/*         oi_iXon = imr_iXon->one_i; */
/*         oi_iXon->filename = "iXon"; */
/*         iXon.num_images = imr_iXon->cur_oi;////bizarre */
/*         //oi_iXon = imr_iXon->one_i; */
/*         //oi_iXon->filename = "iXon"; */
/*         set_oi_source(oi_iXon, "iXon"); */
/*         d_iXon = find_dialog_associated_to_imr(imr_iXon, NULL); */
/*     } */
    
/*     imr_iXon->one_i->need_to_refresh |= ALL_NEED_REFRESH; */
            
/*     alloc_one_image(oi_iXon,sizeX,sizeY,iXon.image_type); */

/*     //win_report("Allocation successful"); */
    
/*     return D_O_K; */
/* } */

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/** Sets Xvin in real-time imaging mode with iXon.

\param None.
\return int Error code.
\author JF Allemand
*/ 
int iXon_real_time_imaging(void)
{
    if(updating_menu_state != 0) return D_O_K;
    
    //win_report("Real-time imaging");
    
    if (SetEMCCDGain(iXon.gain) != DRV_SUCCESS) win_report("Could not set gain to min in function 'iXon-real-time-imaging' !");
    
    /* Launches the acquisition and display thread */
    create_fluo_thread();
    
    return D_O_K;   
}   

////////////////////////////////////////////////////////////////////////////////

/** Stops iXon image acquisition.

\param None.
\return int Error code.
\author JF Allemand
*/ 
int Stop_acquisition(void)
{
    if(updating_menu_state != 0) return D_O_K;
    
    iXon.is_live_previous = iXon.is_live;
    iXon.is_live = FREEZE;
    
    Sleep(200); 
    /* Quite necessary. Following functions might be lured by the camera telling them
    it is still acquiring whereas it has just ceased to be. */

    /* Free data memory of the camera to ensure better efficiency */
    if (FreeInternalMemory() != DRV_SUCCESS) win_report("Could not set free internal memory in function 'Stop-acquisition' !");
    
    return D_O_K;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Housekeeping functions

/** Main function of \a iXon_acq_control.c . This function initializes the system, loads the menus. */
int	ixon_acq_main(void)
{   
    //atexit(Exit_iXon);
      
    return D_O_K;
}

/** Unload function for the \a iXon_acq_control.c menu.*/
int	ixon_acq_unload(void)
{
	return D_O_K;
}
#endif


