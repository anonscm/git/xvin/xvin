# ifndef _PICO_CFG_H_
# define _PICO_CFG_H_


int	def_oi_scaling(O_i *oi);
int	def_scaling(char ch);

# define OIL_IMMERSION 2
# define WATER_IMMERSION 1
# define AIR_IMMERSION 0


# define COMMERCIAL 0
# define HOME_MADE 1
//Plutot faire une structure de structures
typedef struct _micro_param
{
    char* config_microscope_time;
    
	char *microscope_user_name;				/* the name of the microscope */
	char *microscope_manufacturer_name;				/* the name of the microscope */
	int	microscope_type;				/* COMMERCIAL or HOME_MADE		*/
	char *PicoTwist_model;	
 	float zoom_factor;
	float zoom_min, zoom_max;
	float field_factor;
	float microscope_factor;
	float imaging_lens_focal_distance_in_mm;
	float x_pixel_2_microns;
	float y_pixel_2_microns;
	int LED_wavelength;
}Micro_param;

typedef struct _camera_param
{
	char* config_camera_time;
	char *camera_manufacturer;
	char *camera_model;
	int camera_frequency_in_Hz;
	char *camera_config_file;
    int camera_software_program;
}Camera_param;

typedef struct _obj_param
{
	char* config_objective_time;
 	char *objective_manufacturer;
	float objective_magnification;
	int immersion_type;
	float objective_numerical_aperture;
}Obj_param;

typedef struct _piezzo_param
{
	char* config_piezzo_time;
	int Is_Piezzo_in;
	int Piezzo_max_expansion;
	int Piezzo_driver;
	int Piezzo_direction;
	char *Piezzo_model;
}Piezzo_param;

    
typedef struct _motor_param
{
   	char* config_motor_time;
	char *rotation_motor_manufacturer;
	char *rotation_motor_model;
	float P_rotation_motor;
	float I_rotation_motor;
	float D_rotation_motor;
	float rotation_max_velocity;
	float rotation_velocity;
	float rotation_motor_position;

	char *translation_motor_manufacturer;
	char *translation_motor_model;
	float P_translation_motor;
	float I_translation_motor;
	float D_translation_motor;
	int translation_motor_inverted;
	float translation_max_velocity;
	float translation_velocity;
	float translation_motor_position;
	float translation_max_pos;
	
	int motor_controller;
}Motor_param;

typedef struct _serial_param
{
   	char* config_serial_time;
	int serial_port;
	int serial_BaudRate;
    int serial_ByteSize;
    int serial_StopBits;
    int serial_Parity;
    int serial_fDtrControl;
    int serial_fRtsControl;
}Serial_param;

typedef struct _pico_parameter
{
    Micro_param micro_param;
    Camera_param camera_param;
    Obj_param obj_param;
    Piezzo_param piezzo_param;
    Motor_param motor_param;
    Serial_param serial_param;
}Pico_parameter;    



//Micro_param Default_Micro_param;
//{
//    NULL,
//	NULL,
//	0,				/* COMMERCIAL or HOME_MADE		*/
//	NULL,
// 	1.,
//	1., 1.,
//	1.,
//	1.,
//	200,
//	10.,
//	10.,
//	525,
//};
//
//Camera_param Default_Camera_param
//{
//	NULL,
//	NULL,
//	NULL,
//	0,
//	NULL,
//    0,
//};
//
//Obj_param Default_Obj_param
//{
//	NULL,
// 	NULL,
//	1.,
//	0,
//	1.,
//};
//
//Piezzo_param Default_Piezzo_param
//{
//	NULL,
//	1,
//	100,
//	0,
//	0,
//	NULL,
//};
//
//    
//Motor_param Default_Motor_param
//{
//   	NULL,
//	NULL,
//	NULL,
//	200,
//	100,
//	50,
//	10,
//	1,
//	0,
//
//	NULL,
//	NULL,
//	200,
//	100,
//	50,
//	1.,
//	15,
//	15,
//	0,
//	22,
//	
//	1,
//};
//
//Serial_param Default_Serial_param
//{
//   	NULL,
//	1,
//	9600,
//    8,
//    1,
//    0,
//    0,
//    0,
//};
//
//Pico_parameter Default_Pico_param
//{
//    Default_Micro_param ,
//    Default_Camera_param ,
//    Default_Obj_param ,
//    Default_Piezzo_param ,
//    Default_Motor_param ,
//    Default_Serial_param ,
//};    
//




	
#define YES 1


//Pico_param Pico_micro_param;
Pico_parameter Pico_param;

# endif 

