/*
*    Plug-in program for image treatement in Xvin.
*
*    V. Croquette JF Allemand
*/

#ifndef _IM_FLUO_C_
#define _IM_FLUO_C_

# include "allegro.h"
# include "winalleg.h"
# include "xvin.h"

/* If you include other plug-ins header do it here*/ 

/* But not below this define */



# define BUILDING_PLUGINS_DLL
# include "../cfg_file/Pico_cfg.h"
# include "../cfg_file/microscope.h"




XV_FUNC(int, do_load_im, (void));

#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "trackBead.h"
# include "track_util.h"
# include "focus.h"
# include "magnetscontrol.h"
# include "im_fluo.h"
# include "fluo_util.h" 


PXV_FUNC(int, show_bead_cross, (BITMAP *imb, imreg *imr, DIALOG *d));
PXV_FUNC(int, get_present_image_nb, (void));
PXV_FUNC(int, move_bead_cross, (BITMAP *imb, imreg *imr, DIALOG *d, int x0, int y0));


int   trackBead_init = 0;

int fluo_oi_mouse_action(O_i *oi, int x0, int y0, int mode, DIALOG *d)
{
  // we put in this routine all the screen display stuff
  imreg *imr;
  BITMAP *imb;
  unsigned long t0 = 0;

  //return D_O_K;
  if (d->dp == NULL)    return 1;
  imr = (imreg*)d->dp;        /* the image region is here */
  if (imr->one_i == NULL || imr->n_oi == 0)        return 1;

  while(mouse_b == 1)
    {
      if (oi->need_to_refresh & BITMAP_NEED_REFRESH)
	{
	  t0 = my_uclock();
	  display_image_stuff_16M(imr,d);
	  oi->need_to_refresh |= INTERNAL_BITMAP_NEED_REFRESH;
	  t0 = my_uclock() - t0;
	  write_and_blit_im_box( plt_buffer, d, imr);
	  imb = (BITMAP*)oi->bmp.stuff;
	    display_title_message("Mouse im %d dt %6.3f ms",oi->im.c_f,  1000*(double)(d->d1)/get_my_uclocks_per_sec());
	}
      if (general_idle_action) general_idle_action(d);
    }
  for (fluo_info->c_b = 0; fluo_info->c_b < fluo_info->n_b; fluo_info->c_b++)
    fluo_info->bd[fluo_info->c_b]->mouse_draged = 0;
  
  
  // this routine switches supress the image idle action, 
  // in particular info display by xvin on image by default 

  return 0;
}

int fluo_before_menu(int msg, DIALOG *d, int c)
{
  /* this routine switches the display flag so that no magic color is drawn 
     when menu are activated */
  if (msg == MSG_GOTMOUSE)
    {
      do_refresh_overlay = 0;
    }
  return 0;
}
int fluo_after_menu(int msg, DIALOG *d, int c)
{
  return 0;
}

int fluo_oi_got_mouse(struct  one_image *oi, int xm_s, int ym_s, int mode)
{
  /* this routine switches the display flag so that the magic color is drawn again
     after menu are activated */
  do_refresh_overlay = 1;
  return 0;
}
int fluo_oi_idle_action(struct  one_image *oi, DIALOG *d)
{
  // we put in this routine all the screen display stuff
  imreg *imr;
  int redraw_background = 0;
  BITMAP *imb;
  unsigned long t0 = 0;
  float zo;//, zm;

  if (d->dp == NULL)    return 1;
  imr = (imreg*)d->dp;        /* the image region is here */
  if (imr->one_i == NULL || imr->n_oi == 0)        return 1;

  t0 = my_uclock();
  if (oi->need_to_refresh & BITMAP_NEED_REFRESH)
    {
      display_image_stuff_16M(imr,d);
      oi->need_to_refresh |= INTERNAL_BITMAP_NEED_REFRESH;
    }
  imb = (BITMAP*)oi->bmp.stuff;
  //  redraw_background = show_bead_cross(imb, imr, d);

  //if (redraw_background)  // slow redraw
  //{
      write_and_blit_im_box( plt_buffer, d, imr);
      show_bead_cross(plt_buffer, imr, d);
      t0 = my_uclock() - t0;

    /*   if (dt_simul > 0) */
/* 	display_title_message("    im %d dt %6.3f ms sim %6.3f aff %6.3f",oi->im.c_f,  1000*(double)(d->d1)/get_my_uclocks_per_sec(),1000*(double)(dt_simul)/get_my_uclocks_per_sec(),1000*(double)(t0)/get_my_uclocks_per_sec()); */
/*       else */
	display_title_message("im %d dt %6.3f ms",oi->im.c_f,  1000*(double)(d->d1)/get_my_uclocks_per_sec());
	// }
/*   else */  if (oi->need_to_refresh & INTERNAL_BITMAP_NEED_REFRESH)
    {
      //screen_used = 1;
      SET_BITMAP_IN_USE(oi);
      acquire_bitmap(screen);
      blit(imb,screen,0,0,imr->x_off //+ d->x
	   , imr->y_off - imb->h + d->y, imb->w, imb->h); 
      release_bitmap(screen);
      //screen_used = 0;
      oi->need_to_refresh &= ~INTERNAL_BITMAP_NEED_REFRESH;
      t0 = my_uclock() - t0;
/*       zo = read_last_Z_value(); */
      SET_BITMAP_NO_MORE_IN_USE(oi);
/*       if (dt_simul > 0) */
/* 	{ */
/* 	  display_title_message(" Zobj %6.3f im %d dt %6.3f ms sim %6.3f b %d ",zo,oi->im.c_f,   */
/* 				1000*(double)(d->d1)/get_my_uclocks_per_sec(), */
/* 				1000*(double)(dt_simul)/get_my_uclocks_per_sec(),(int)mouse_b); */
/* 	} */
/*       else */
	display_title_message("im %d dt %6.3f ms",oi->im.c_f,  1000*(double)(d->d1)/get_my_uclocks_per_sec());
      
    }
  // this routine switches supress the image idle action, 
  // in particular info display by xvin on image by default 

  return 0;
}

void stop_source_thread(void)
{
  go_FLUO = FLUO_STOP;
  broadcast_dialog_message(MSG_DRAW,0); // needed to prevent freezing dialog!
  while (source_running);
}


int source_end_action(DIALOG *d)
{
  stop_source_thread();
  return 0;
}

#ifndef TRACKBEAD_C

int find_next_available_job_spot(void)
{
  int i;

  if (job_pending == NULL) return -1;
  // we look for used job to recycle 
  for (i = 0; i < n_job; i++)  
    if (job_pending[i].in_progress == 0) break; 
  if (i == n_job)
        n_job = (n_job < m_job) ? n_job + 1 : n_job;
  if (i >= m_job) return -2;                 // the buffer is full
  return i;
}


f_job* find_job_associated_to_plot(O_p *op)
{
  int i;

  if (job_pending == NULL) return NULL;
  // we look for used job to recycle 
  for (i = 0; i < n_job; i++)  
    {
      if (job_pending[i].in_progress == 0) continue; 
      if (job_pending[i].op == op) return job_pending + i; 
    }
  return NULL;
}

f_job* find_job_associated_to_image(O_i *oi)
{
  int i;

  if (job_pending == NULL) return NULL;
  // we look for used job to recycle 
  for (i = 0; i < n_job; i++)  
    {
      if (job_pending[i].in_progress == 0) continue; 
      if (job_pending[i].oi == oi) return job_pending + i; 
    }
  return NULL;
}


int fill_next_available_job_spot(int in_progress, int im, int type, int bead_nb, 
				 imreg *imr, O_i *oi, pltreg *pr, 
				  O_p *op, void* more_data, 
				 int (*job_to_do)(int im, struct future_job *job)
				 ,int local)
{
  int i;

  i = find_next_available_job_spot();
  if (i < 0) return -1;
  job_pending[i].in_progress = in_progress;
  job_pending[i].imi = im;
  job_pending[i].last_imi = im - 1;
  job_pending[i].type = type;
  job_pending[i].bead_nb = bead_nb;
  job_pending[i].imr = imr;
  job_pending[i].oi = oi;
  job_pending[i].pr = pr;
  job_pending[i].op = op;
  job_pending[i].more_data = more_data;
  job_pending[i].job_to_do = job_to_do;
  return i;
}


int fill_local_job_value(int jobid, int local)
{
  if (jobid < 0 || jobid >= n_job) return  1;
  job_pending[jobid].local = local;
  return 0;
}


int find_next_job(int imi)
{
  int i, j;

  if (job_pending == NULL) return -1;
  for (i = 0, j = -1; i < n_job; i++)
    {
      if (job_pending[i].in_progress == 0) continue;
      else j = i;
      if (job_pending[i].last_imi < imi) 
	{
	  if (job_pending[i].imi < 0) break;
	  if (job_pending[i].imi < imi) break;
	}
    }
  if (i < n_job) return i;
  else  n_job = j+1;
  return -2;
}

int find_remaining_job(int imi)
{
  int i, j;

  if (job_pending == NULL) return -1;
  for (i = 0, j = 0; i < n_job; i++)
      j += (job_pending[i].in_progress) ? 1 : 0;
  return j;
}





int source_idle_action(DIALOG *d)
{
  int i = 0, im;

  im = get_present_image_nb();
  while ((i = find_next_job(im)) >= 0)
    {
      if (job_pending[i].job_to_do != NULL)
	  job_pending[i].job_to_do(im, job_pending + i);
      job_pending[i].last_imi = im;
    }
  return 0;
}
#endif

int create_fluo_thread(Fluo_tid *fluo_dtid)
{
    HANDLE hThread = NULL;
    DWORD dwThreadId;
    
    hThread = CreateThread( 
            NULL,              // default security attributes
            0,                 // use default stack size  
            FluoThreadProc,// thread function 
            (void*)fluo_dtid,       // argument to thread function 
            0,                 // use default creation flags 
            &dwThreadId);      // returns the thread identifier 

    if (hThread == NULL) 	win_printf_OK("No thread created");
    SetThreadPriority(hThread,  THREAD_PRIORITY_NORMAL);
    return 0;
}

int display_imr_and_pr(void)
{
  int i;
  DIALOG *di, *dip, *dim = NULL;
  //int h;

  if(updating_menu_state != 0)	return D_O_K;


  if (pr_FLUO == NULL || imr_FLUO == NULL) return D_O_K;

  i = retrieve_index_of_menu_from_dialog();
  if (i < 0)     win_printf_OK("Cannot find menu dialog");
  di = the_dialog + i;

  i = find_imr_index_in_current_dialog(the_dialog);
  if (i >= 0)   dim = the_dialog + i;
  else 	        win_printf_OK("Cannot find Im dialog");
  set_dialog_size_and_color(dim, SCREEN_W/2, 19, SCREEN_W-1, dim->h, 255, 0);

  remove_all_keyboard_proc();	
  dip = attach_new_plot_region_to_dialog( 0, 0, 0, 0);
  if (dip == NULL)        allegro_message("dialog pb");
  set_dialog_size_and_color(dip, 0, 19, SCREEN_W/2-1, dim->h, 255, 0);
  dip->dp = pr_FLUO;
  scan_and_update((MENU*)di->dp);

  broadcast_dialog_message(MSG_DRAW,0);

  return D_REDRAWME;
}


MENU *fluo_plot_menu(void)
{
   static MENU mn[32];
   //   extern MENU bead_active_menu[];

     
   if (mn[0].text != NULL)	return mn;
/*    add_item_to_menu(mn,"Stop trajectories", stop_record_x_y_trajectories,NULL,0,NULL); */
/*    add_item_to_menu(mn,"Stop angle", stop_record_x_y_angle,NULL,0,NULL); */
/*    add_item_to_menu(mn,"Switch bead",NULL,bead_active_menu,0,NULL); */
   return mn;
}

MENU * fluo_image_menu(void)
{
	static MENU mn[32];
	extern MENU bead_active_menu[];
	extern int spectrum_profile(void);
	extern int stop_spectrum_idle_action(void);
	extern int intensity_vs_time(void);
	extern int stop_intensity_vs_time_idle_action(void);
	extern int save_finite_movie(void);


	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"freeze video", freeze_fluo_source,NULL,0,NULL);
	add_item_to_menu(mn,"live video", live_fluo_source,NULL,0,NULL);
	add_item_to_menu(mn,"kill video", kill_fluo_source,NULL,0,NULL);
	add_item_to_menu(mn,"Load Image\t(Ctrl-I)",do_load_im, NULL, 0, NULL);
/* 	add_item_to_menu(mn,"New bead track x y", follow_bead_in_x_y,NULL,0,NULL); */
/* 	add_item_to_menu(mn,"Track x y z", follow_bead_in_x_y,NULL,MENU_INDEX(IMAGE_CAL),NULL); */
/* 	add_item_to_menu(mn,"Switch bead",NULL,bead_active_menu,0,NULL); */
/* 	add_item_to_menu(mn,"Record movie", record_movie,NULL,0 ,NULL); */
/* 	add_item_to_menu(mn,"plot and image", display_imr_and_pr,NULL,0 ,NULL); */
/* 	add_item_to_menu(mn,"Spectrum", spectrum_profile,NULL,0 ,NULL); */
/* 	add_item_to_menu(mn,"Stop Spectrum", stop_spectrum_idle_action,NULL,0 ,NULL); */
/* 	add_item_to_menu(mn,"Intensity vs time", intensity_vs_time,NULL,0 ,NULL); */
/* 	add_item_to_menu(mn,"Stop Intensity", stop_intensity_vs_time_idle_action,NULL,0 ,NULL); */
/* 	add_item_to_menu(mn,"Record Movie in thread", save_finite_movie,NULL,0 ,NULL); */



	return mn;
}

// trackBead 3D bead tracking software wich can be compile to use different image sources
// simulate a bead : "game"
// use a movie :  "movie"
// use IFC data acquisition "ifc"
// use CVB data acquisition "cvb"
  
// source have specific functions sharing the same names
// int init_image_source();
// int start_data_movie(imreg *imr);
// DWORD WINAPI TrackingThreadProc( LPVOID lpParam ); 

// trackBead creates an image region withe the special image to track
// it defines the image idle action which typically consists in screen refresh
// it also creates a plot region where timing plots are gathered
// it also fills up a structure with all these variables
// it adds a general menu to images: trackBead_image_menu()
// it lauches a thread for tracking with a specific function TrackingThreadProc
// this function trigger a timer function used to track 


int im_fluo_main(int argc, char **argv)
{
  imreg *imr = NULL;
  pltreg *pr = NULL;
  O_p *op;
  O_i *oi;
  float o;
  int init_fluo_info(void);


  XVcfg_main();
  focus_main(0,NULL);
  read_Z_value_accurate_in_micron();
/*   magnetscontrol_main(0,NULL); */
   Open_Pico_config_file();
/*   r = get_config_float("STATE", "MagRot", 0); */
/*   z = get_config_float("STATE", "MagZ", 5); */
   o = get_config_float("STATE", "ObjZ", 5); 
   Close_Pico_config_file(); 

/*   set_magnet_z_value(z); */
/*   set_rot_value(r); */
   set_Z_value(o); 

  // if a movie is load already we use this movie as a data source 
  imr = find_imr_in_current_dialog(NULL);
  if (imr == NULL)    init_image_source_for_fluo();
  else
    {
        if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)
	  init_image_source_for_fluo();
	if (oi->im.movie_on_disk == 0 || oi->im.n_f < 2)
	  init_image_source_for_fluo();

    }
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)
    return win_printf_OK("You must load a movie in the active window");

  imr_FLUO = imr;
  if (oi_FLUO == NULL)
    oi_FLUO = oi;
  def_oi_scaling(oi_FLUO); // oi_FLUO


  //we attach image menu
  if (imr_FLUO != NULL)
    {
      add_image_treat_menu_item ( "FLUO image", NULL, fluo_image_menu(), 0, NULL);
      //start_data_movie(imr); 
    }

  // We create a plot region to display bead position, timing  etc.
  pr = create_hidden_pltreg_with_op(&op, 4096, 4096, 0,"iXon status");
  if (pr == NULL)  win_printf_OK("Could not find or allocate plot region!");
  pr_FLUO = pr;
  add_plot_treat_menu_item ( "FLUO", NULL, fluo_plot_menu(), 0, NULL);
  if (op == NULL)  win_printf_OK("Could not find or allocate plot !");
  refresh_plot(pr,0);
  switch_project_to_this_imreg(imr);
  broadcast_dialog_message(MSG_DRAW,0);    

#ifdef TRACKBEAD_C
  m_job = 256;
  n_job = 0;
  job_pending = (f_job*)calloc(m_job,sizeof(f_job));
  if (job_pending == NULL)
    win_printf_OK("Could not allocate job_pending!");

#endif
  //  imr_FLUO = imr;
  //  oi_FLUO = imr->one_i;
 
  d_FLUO = find_dialog_associated_to_imr(imr_FLUO, NULL);
  fluo_dtid.imr = imr;
  fluo_dtid.pr = pr;
  fluo_dtid.oi = oi_FLUO;
  fluo_dtid.op = op;
  fluo_dtid.dimr = d_FLUO;
  fluo_dtid.dpr = find_dialog_associated_to_pr(pr_FLUO, NULL);
  fluo_dtid.dbfid = &fluo_bid;
  before_menu_proc = fluo_before_menu;
  after_menu_proc = fluo_after_menu;
  oi_FLUO->oi_got_mouse = fluo_oi_got_mouse;
  oi_FLUO->oi_idle_action = fluo_oi_idle_action;
  oi_FLUO->oi_mouse_action = fluo_oi_mouse_action;

  init_fluo_info();
  go_FLUO = FLUO_ON;
  create_fluo_thread(&fluo_dtid);
  //  atexit(stop_source_thread);
  general_end_action = source_end_action;
  general_idle_action = source_idle_action;

  return D_O_K;
}

int	im_fluo_unload(int argc, char **argv)
{
  remove_item_to_menu(image_treat_menu, "FLUO", NULL, NULL);
  return D_O_K;
}
#endif

