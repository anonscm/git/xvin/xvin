#ifndef _IM_FLUO_H_
#define _IM_FLUO_H_

#define STOP_FLUO_THREAD 0

typedef struct function_in_thread
{
  void *function_param;
  int (*timer_do)(imreg *imr, O_i *oi, int n, DIALOG *d, long long t, unsigned long dt, void *p, int n_inarow);
  char *source;
}fit;

typedef struct before_fluo_image_display
{
  void *param;
  int previous_fr_nb;
  unsigned long timer_dt;
  int previous_in_time_fr_nb;
  int first_im;                     // the nb of the first image
  long long previous_in_time_fr;
  int lost_fr;
  fit *function;
  int n_functions;

} Fluo_Bid;



typedef struct thread_fluo_image_data
{
  imreg *imr;
  pltreg *pr;
  O_p *op;
  O_i *oi;
  DIALOG *dpr, *dimr;
  Fluo_Bid *dbfid;
} Fluo_tid;


# define FLUO_FREEZE   2
# define FLUO_ON       1
# define FLUO_STOP     0

// job type

# define GRAB_CALIBRATION_IMAGE     8

# define COLLECT_DATA   16
// this is lower priority routine done in the general_idle_action
// typically grabbing the calibration image, force measurements etc

/* typedef struct future_job */
/* { */
/*   int imi;             // the image nb where to start action, if < 0 the next possible frame */
/*   int last_imi;        // an internal idex used to avoid calling several time this job for the same image */
/*   int in_progress;    // 0 if finished, anything else if active */
/*   int type; */
/*   int bead_nb; */
/*   int local; */
/*   imreg *imr; */
/*   O_i *oi; */
/*   pltreg *pr; */
/*   O_p *op; */
/*   void *more_data ;                       // data structure for function */
/*   int (*job_to_do)(int im, struct future_job *job); */
/* } f_job; */





#define IMAGE_CAL			1

#define DEF_CROSS_SIZE			2

# ifndef _IM_FLUO_C_
//extern  __declspec(dllexport) int go_track ;             // flag on means tracking
PXV_VAR(int, go_FLUO);
PXV_VAR(int,StopFluoThread);
PXV_VAR(Fluo_tid, fluo_dtid);
PXV_VAR(int, do_refresh_overlay);
PXV_VAR(Fluo_Bid, fluo_bid);
PXV_VAR(imreg  *,imr_FLUO);
PXV_VAR(pltreg  *,pr_FLUO);
PXV_VAR(O_i  *, oi_FLUO);
PXV_VAR(DIALOG *, d_FLUO);
PXV_VAR(int, source_running);
PXV_VAR(char , running_message[]);
PXV_VAR(unsigned long, dt_simul);

PXV_VAR(f_job*, job_pending);
PXV_VAR(int, m_job);
PXV_VAR(int, n_job);

# endif
# ifdef _IM_FLUO_C_
int go_FLUO = 0;             // flag on means tracking
int StopFluoThread = 1;
Fluo_tid fluo_dtid;                     // the data passed to the thread proc
int do_refresh_overlay = 0;
Fluo_Bid fluo_bid={NULL,0,0,0,0,0,0,NULL,0};
void *param;
int previous_fr_nb;
unsigned long timer_dt;
int previous_in_time_fr_nb;
int first_im;                     // the nb of the first image
long long previous_in_time_fr;
int lost_fr;
fit *function;
int n_functions;

imreg  *imr_FLUO = NULL;
pltreg  *pr_FLUO = NULL;
O_i  *oi_FLUO = NULL;
DIALOG *d_FLUO = NULL;
int source_running = 0;
char running_message[128];
unsigned long dt_simul = 0;

f_job *job_pending = NULL;
int m_job = 0;
int n_job = 0;

# endif


PXV_FUNC(MENU*, fluo_image_menu, (void));
PXV_FUNC(int, trackBead_main, (int argc, char **argv));
PXV_FUNC(int, init_image_source_for_fluo,(void));
PXV_FUNC(int, start_data_movie, (imreg *imr));
PXV_FUNC(DWORD WINAPI, FluoThreadProc, (LPVOID lpParam)); 
PXV_FUNC(int, live_fluo_source, (void));
PXV_FUNC(int, freeze_fluo_source, (void));
PXV_FUNC(int, kill_fluo_source, (void));
PXV_FUNC(int, prepare_image_overlay, (O_i* oi));
PXV_FUNC(int, follow_bead_in_x_y,(void));
PXV_FUNC(int, track_oi_idle_action, (struct  one_image *oi, DIALOG *d));
PXV_FUNC(int, create_fluo_thread,(Fluo_tid *fluo_dtid));



PXV_FUNC(int, fill_next_available_job_spot, (int in_progress, int im, int type, int bead_nb, imreg *imr, O_i *oi, pltreg *pr, O_p *op, void* more_data, int (*job_to_do)(int im, struct future_job *job),int local));

PXV_FUNC(f_job*, find_job_associated_to_image,(O_i *oi));
PXV_FUNC(f_job*, find_job_associated_to_plot,(O_p *op));

#endif

