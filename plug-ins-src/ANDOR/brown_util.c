/** \file brown_util.c
    \brief Plug-in program for bead tracking 
    
    This is a subprogram that creates, displays and interfaces the menus with the plug-in functions.
    
    \author V. Croquette, J-F. Allemand
*/

#ifndef _BROWN_UTIL_C_
#define _BROWN_UTIL_C_




#include "allegro.h"
# include "winalleg.h"
# include "xvin.h"
# include "fftl32n.h"
# include "fillib.h"
# include "largeint.h"



/* But not below this define */
# define BUILDING_PLUGINS_DLL
#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "brown_util.h"
// required for SMID 
//typedef int v4sf __attribute__ ((mode(V4SF))); // vector of four single floats
typedef float v4sf __attribute__ ((vector_size (16))); // vector of four single floats
union f4vector 
{
  v4sf v;
  float f[4];
};
union f4vector f4, fax, fay, f0, fx, fy, fxc, fyc, fy2, fr[64], fx2[64];
// debugging array
float radial_dx[256];

float fx1[512];


int	fill_avg_profiles_from_im(O_i *oi, int xc, int yc, int cl, int cw, int *x, int *y)
{
	register int j, i;
	unsigned char *choi;	
	short int *inoi;
	int ytt, xll, onx, ony, xco, yco, cl2, cw2;
	union pix *pd;
	
	if (oi == NULL || x == NULL || y == NULL)	return 1;
	onx = oi->im.nx;	ony = oi->im.ny;
	pd = oi->im.pixel;
	cl2 = cl >> 1;	cw2 = cw >> 1;
	yco = (yc - cl2 < 0) ? cl2 : yc;
	yco = (yco + cl2 <= ony) ? yco : ony - cl2;
	xco = (xc - cl2 < 0) ? cl2 : xc;
	xco = (xco + cl2 <= onx) ? xco : onx - cl2;
	for(i = 0; i < cl; i++)		x[i] = y[i] = 0;	
	if (oi->im.data_type == IS_CHAR_IMAGE)
	{
		for(j = 0, ytt = yco - cw2, xll = xco - cl2; j< cw; j++)
		{	
			choi = pd[ytt+j].ch + xll;
			for (i = 0; i < cl; i++)		x[i] += (int)choi[i];	
		}
		for(j = 0, ytt = yco - cl2, xll = xco - cw2; j <cl; j++)
		{	
			choi = pd[ytt+j].ch + xll;
			for (i = 0; i < cw; i++)		y[j] += (int)choi[i];	
		}
	}
	else if (oi->im.data_type == IS_INT_IMAGE)
	{
		for(j = 0, ytt = yco - cw2, xll = xco - cl2; j< cw; j++)
		{	
			inoi = pd[ytt+j].in + xll;
			for (i = 0; i < cl; i++)		x[i] += (int)inoi[i];	
		}
		for(j = 0, ytt = yco - cl2, xll = xco - cw2; j <cl; j++)
		{	
			inoi = pd[ytt+j].in + xll;
			for (i = 0; i < cw; i++)		y[j] += (int)inoi[i];	
		}
	}
	else return 1;	
	return 0;		
}


int	check_bead_not_lost(int *x, float *x1, int *y, float *y1, int cl, float bd_mul)
{
	register  int i, j;
	int minx, maxx, miny, maxy;
	
	for (i = 0, minx = maxx = x[0], miny = maxy = y[0]; i < cl; i++)
	{
		j = x[i];
		minx = (j < minx) ? j : minx;
		maxx = (j > maxx) ? j : maxx;			
		x1[i] = (float)j;
		j = y[i];
		miny = (j < miny) ? j : miny;
		maxy = (j > maxy) ? j : maxy;						
		y1[i] = (float)j;			
	}
	return (((maxx-minx)*bd_mul) < (maxx+minx) && 
		((maxy-miny)*bd_mul) < (maxy+miny)) ? 1 : 0;
}

int     erase_around_black_circle(int *x, int *y, int cl)
{
	register int j;
	int xb0, xb1, zb0, zb1;
	
	/* 	for good bead there is a definite black circle around the image
		for rotation with a small bead you can suppress the image around
		this black circle to avoid perturbation by the small bead
	*/
		
	for (xb0 = j = 0, zb0 = x[0]; j < cl>>1; j++)
		zb0 = (zb0 < x[j]) ? zb0 : x[(xb0 = j)];
	for (xb1 = j = cl-1, zb1 = x[xb1]; j >= cl>>1; j--)
		zb1 = (zb1 < x[j]) ? zb1 : x[(xb1 = j)];
	zb0 = (zb0 > zb1) ? zb0 : zb1;
	for (j = 0; j < cl; j++)
		x[j] = (j < xb0 || j > xb1) ? 0 : x[j] - zb0;
	for (xb0 = j = 0, zb0 = y[0]; j < cl>>1; j++)
		zb0 = (zb0 < y[j]) ? zb0 : y[(xb0 = j)];
	for (xb1 = j = cl-1, zb1 = y[xb1]; j >= cl>>1; j--)
		zb1 = (zb1 < y[j]) ? zb1 : y[(xb1 = j)];
	zb0 = (zb0 > zb1) ? zb0 : zb1;
	for (j = 0; j < cl; j++)
		y[j] = (j < xb0 || j > xb1) ? 0 : y[j] - zb0;
	return 0;	
}	

int deplace(float *x, int nx)
{
	register int i, j;
	float tmp;
	
	for (i = 0, j = nx/2; j < nx ; i++, j++)
	{
		tmp = x[j];
		x[j] = x[i];
		x[i] = tmp;
	}
	return 0;
}
int fftwindow_flat_top1(int npts, float *x, float smp)
{
	register int i, j;
	int n_2, n_4;
	float tmp, *offtsin;
	
	offtsin = get_fftsin();

	if ((j = fft_init(npts)) != 0)
		return j;

	n_2 = npts/2;
	n_4 = n_2/2;
	x[0] *= smp;
	smp += 1.0;
	for (i=1, j=n_4-2; j >=0; i++, j-=2)
	{
		tmp = (smp - offtsin[j]);
		x[i] *= tmp;
		x[npts-i] *= tmp;
	}
	for (i=n_4/2, j=0; i < n_4 ; i++, j+=2)
	{
		tmp = (smp + offtsin[j]);
		x[i] *= tmp;
		x[npts-i] *= tmp;
	}
	return 0;
}
int change_mouse_to_cross(int cl, int cw)
{
    BITMAP *ds_bitmap = NULL;
    int color, cw2, cl2;

    cl2 = cl/2;
    cw2 = cw/2;
    ds_bitmap = create_bitmap(cl+1,cl+1);
    cl = Lightmagenta;
    color = makecol(EXTRACT_R(cl), EXTRACT_G(cl), EXTRACT_B(cl));
    clear_to_color(ds_bitmap, color);

    color = makecol(255, 0, 0);                        

    line(ds_bitmap,0,cl2-cw2,0,cl2+cw2,color);
    line(ds_bitmap,cl-1,cl2-cw2,cl-1,cl2+cw2,color);

    line(ds_bitmap,cl2-cw2,0,cl2+cw2,0,color);
    line(ds_bitmap,cl2-cw2,cl-1,cl2+cw2,cl-1,color);

    line(ds_bitmap,0,cl2-cw2,cl2-cw2,cl2-cw2,color);
    line(ds_bitmap,cl,cl2-cw2,cl2+cw2,cl2-cw2,color);
    line(ds_bitmap,0,cl2+cw2,cl2-cw2,cl2+cw2,color);
    line(ds_bitmap,cl,cl2+cw2,cl2+cw2,cl2+cw2,color);

    line(ds_bitmap,cl2-cw2,0,cl2-cw2,cl2-cw2,color);
    line(ds_bitmap,cl2+cw2,0,cl2+cw2,cl2-cw2,color);
    line(ds_bitmap,cl2-cw2,cl,cl2-cw2,cl2+cw2,color);
    line(ds_bitmap,cl2+cw2,cl,cl2+cw2,cl2+cw2,color);

    scare_mouse();    
    set_mouse_sprite(ds_bitmap);
    set_mouse_sprite_focus(cl2, cl2);
    unscare_mouse();
    return 0;    
}
int reset_mouse()
{
    scare_mouse();    
    set_mouse_sprite(NULL);
    unscare_mouse();
    return 0;
}


/*	Find maximum position in a 1d array, the array is supposed periodic
 *	return 0 -> everything fine, TOO_MUCH_NOISE or PB_WITH_EXTREMUM
 *
 */
int	find_max1(float *x, int nx, float *Max_pos, float *Max_val)
{
	register int i;
	int  nmax, ret = 0, delta;
	double a, b, c, d, f_2, f_1, f0, f1, f2, xm = 0;

	for (i = 0, nmax = 0; i < nx; i++) nmax = (x[i] > x[nmax]) ? i : nmax;
	f_2 = x[(nx + nmax - 2) % nx];
	f_1 = x[(nx + nmax - 1) % nx];
	f0 = x[nmax];
	f1 = x[(nmax + 1) % nx];
	f2 = x[(nmax + 2) % nx];
	if (f_1 < f1)
	{
		a = -f_1/6 + f0/2 - f1/2 + f2/6;
		b = f_1/2 - f0 + f1/2;
		c = -f_1/3 - f0/2 + f1 - f2/6;
		d = f0;
		delta = 0;
	}	
	else
	{
		a = b = c = 0;	
		a = -f_2/6 + f_1/2 - f0/2 + f1/6;
		b = f_2/2 - f_1 + f0/2;
		c = -f_2/3 - f_1/2 + f0 - f1/6;
		d = f_1;
		delta = -1;
	}
	if (fabs(a) < 1e-8)
	{
		if (b != 0)	xm =  - c/(2*b) - (3 * a * c * c)/(4 * b * b * b);
		else
		{
			xm = 0;
			ret |= PB_WITH_EXTREMUM;
		}
	}
	else if ((b*b - 3*a*c) < 0)
	{
		ret |= PB_WITH_EXTREMUM;
		xm = 0;
	}
	else
		xm = (-b - sqrt(b*b - 3*a*c))/(3*a);
	/*
	for (p = -2; p < 1; p++)
	{
		if (6*a*p+b > 0) ret |= TOO_MUCH_NOISE;
	}	
	*/
	*Max_pos = (float)(xm + nmax + delta); 
	*Max_val = (a * xm * xm * xm) + (b * xm * xm) + (c * xm) + d;
	return ret;
}



/*		correlate "x1" with its invert over "nx" points, returns the correlation
 *		in "fx1". "fx1" will be used to compute fft and result 
 *		setting them to NULL will allocate space. "window" will enable
 *		a windowing process if != 0 a double windowing if = to 2.
 */
int	correlate_1d_sig_and_invert(float *x1, int nx, int window, float *fx1, int filter, int remove_dc)
{
	register int i, j;
	float moy, smp = 0.05;
	float m1, re1, re2, im1, im2;
	
	if (x1 == NULL)				return WRONG_ARGUMENT;
	if (fft_init(nx) || filter_init(nx))	return FFT_NOT_SUPPORTED;
	if (fx1 == NULL)	fx1 = (float*)calloc(nx,sizeof(float));
	if (fx1 == NULL)			return OUT_MEMORY;

	/*	compute fft of x1 */
	for(i = 0; i < nx; i++)		fx1[i] = x1[i];
	for(i = 0, j = (3*nx)/4, moy = 0; i < nx/4; i++, j++)		
		moy += fx1[i] + fx1[j];	
	for(i = 0, moy = (2*moy)/nx; remove_dc && i < nx; i++)	
		fx1[i] = x1[i] - moy;
	if (window == 1)	fftwindow1(nx, fx1, smp);
	else if (window == 2)	fftwindow_flat_top1(nx, fx1, smp);

	realtr1(nx, fx1);
	//for(i = 0; i < nx; i++)		x1[i] = fx1[i];
	//return 0;
	//win_printf("bef fft fx1[3] = %f moy %f nx %d filter %d",fx1[3],moy,nx,filter);
	fft(nx, fx1, 1);
	realtr2(nx, fx1, 1);
	
	/*	compute normalization */
	for (i=0, m1 = 0; i< nx; i+=2)
		m1 += fx1[i] * fx1[i] + fx1[i+1] * fx1[i+1];
	m1 /= 2;	
		
	/*	compute correlation in Fourier space */
	for (i=2; i< nx; i+=2)
	{
		re1 = fx1[i];		re2 = fx1[i];
		im1 = fx1[i+1];		im2 = -fx1[i+1];
		fx1[i] 		= (re1 * re2 + im1 * im2)/m1;
		fx1[i+1] 	= (im1 * re2 - re1 * im2)/m1;
	}
	fx1[0]	= 2*(fx1[0] * fx1[0])/m1;	/* these too mode are special */
	fx1[1] 	= 2*(fx1[1] * fx1[1])/m1;
	if (filter> 0)		lowpass_smooth_half (nx, fx1, filter);
	
	/*	get back to real world */
	realtr2(nx, fx1, -1);
	//win_printf("bef 2 fft fx1[2] = %f",fx1[2]);
	fft(nx, fx1, -1);
	realtr1(nx, fx1);
	deplace(fx1, nx);
	return 0;
}	

float	find_distance_from_center(float *x1, int cl, int flag, int filter, int black_circle, float *corr)
{
	float dx = 0;
	
	correlate_1d_sig_and_invert(x1, cl, flag, fx1, filter, 1);//(black_circle)?0:1);
	find_max1(fx1, cl, &dx,corr);
	dx -= cl/2;
	dx /=2;
	return dx;
}



int	draw_cross_in_screen_unit(int xc, int yc, int length, int width, int color, BITMAP* bmp, int scale)
{// bead pixel position
  
        int l = (28*(length >> 1))/scale;
	int w = (28*(width >> 1))/scale;
	int y = bmp->h - (28*yc)/scale;
	int xcs = (28*xc)/scale;
	int x1 =  xcs - l;
	int x4 =  xcs + l;
	int x2 =  xcs - w;
	int x3 =  xcs + w;
	int y1 =  y + l;
	int y4 =  y - l;
	int y2 =  y + w;
	int y3 =  y - w;

	hline(bmp, x1, y2, x2, color);
	vline(bmp, x2, y2, y1, color);
	hline(bmp, x2, y1, x3, color);
	vline(bmp, x3, y1, y2, color);
	hline(bmp, x3, y2, x4, color);
	vline(bmp, x4, y2, y3, color);
	hline(bmp, x3, y3, x4, color);
	vline(bmp, x3, y3, y4, color);
	hline(bmp, x2, y4, x3, color);
	vline(bmp, x2, y3, y4, color);
	hline(bmp, x1, y3, x2, color);
	vline(bmp, x1, y2, y3, color);
	return 0;						
}														



int fft_band_pass_in_real_out_complex(float *x, int nx, int w_flag, int fc, 
	int width)
{
	register int i, j;
	float  moy, smp = 0.05;

	/* remove dc based upon the first and the last quarter of data */
	/*	we assume a peak in the middle !*/	
	for(i = 0, j = (3*nx)/4, moy = 0; i < nx/4; i++, j++)		
		moy += x[i] + x[j];	
	for(i = 0, moy = (2*moy)/nx; i < nx; i++)		x[i] -= moy;	
	if (w_flag)		fftwindow1(nx, x, smp);
	realtr1(nx, x);		fft(nx, x, 1);	realtr2(nx, x, 1);
	bandpass_smooth_half (nx, x, fc, width);
	fft(nx, x, -1);
	if (w_flag)		defftwc1(nx, x, smp);
	return 0;	
}


int prepare_filtered_profile(float *zp, int nx, int flag, int filter, int width)
{
	register int j, i;
	float moy, re, im;

	
	/* remove dc based upon the first and the last quarter of data */
	/*	we assume a peak in the middle !*/	
	for(i = 0, j = (3*nx)/4, moy = 0; i < nx/4; i++, j++)		
		moy += zp[i] + zp[j];	
	for(i = 0, moy = (2*moy)/nx; i < nx; i++)		zp[i] -= moy;
		
	/* no windowing, profile must be symmetrical */
	realtr1(nx, zp);		fft(nx, zp, 1);	realtr2(nx, zp, 1);
	/* I take the amplitude of the cosine of the fist mode to normalize */
	moy = zp[2]; 
	if (moy == 0) 
	{
	  /*		activate_acreg(create_plot_from_1d_array("profile", nx, zpk));*/
		win_printf("pb of normalisation zp[2] %f zp[3] %f",  zp[2],zp[3]);
	}
	else {for (j=0, moy = ((float)1)/moy; j< nx ; j++)   zp[j] *= moy;}
	zp[0] = zp[1] = 0; 
	bandpass_smooth_half (nx, zp, filter, width);
	fft(nx, zp, -1);

	/* first halh of profile ampliture, second half re im */
	for (j=0 ; j< nx/2 ; j += 2)
	{
		re = zp[j];
		im = zp[j+1];
		zp[j] = sqrt(re*re+im*im);
		zp[j+1] = 0;
	}
	return 0;
}

float 	find_simple_phase_shift_between_filtered_profile(float *zr, float *zp, int nx, int rc)
{
	register int i, j;
	float im, re, amp, phi, w, ph;
	
	if (zr == NULL || zp == NULL)	return WRONG_ARGUMENT;
	for (j = 2, i = nx - j, phi = 0, w = 0; j < nx/2 - rc; i -= 2, j += 2)
	{
		re = zr[i] * zp[i] + zp[i+1] * zr[i+1];
		im = zp[i+1] * zr[i] - zp[i] * zr[i+1];
		ph = (im == 0.0 && re == 0.0) ? 0.0 : atan2(im,re);
		amp = zr[j] * zp[j];
		w += amp;
		phi += amp*ph;
	}
	return  (w != 0.0) ? phi/w : phi;
}


float 	find_phase_shift_between_profile(float *zr, float *zp, int npc, 
	int flag, int filter, int width, int rc)
{
	register int i;
	float im, re, amp, phi, dphi, w;
	static float *tmp = NULL, *tmp1 = NULL;
	static int ntmp = 0;
	int nx, onx;
		
	
	if (zr == NULL || zp == NULL)	return WRONG_ARGUMENT;
	nx = npc;
	if (fft_init(nx) || filter_init(nx))			return FFT_NOT_SUPPORTED;
	if (tmp == NULL || ntmp != nx)
	{
		tmp = (float *)calloc(nx,sizeof(float));
		if (tmp == NULL)	return -1;
		ntmp = nx;
		tmp1 = tmp + nx/2;
	}
	fft_band_pass_in_real_out_complex(zp, nx, flag, filter, width);
	for (i=0; i< nx/2; i++)
	{
		re = zr[2*i] * zp[2*i] + zp[2*i+1] * zr[2*i+1];
		im = zp[2*i+1] * zr[2*i] - zp[2*i] * zr[2*i+1];
		tmp[i] = (im == 0.0 && re == 0.0) ? 0.0 : atan2(im,re);
	}
	onx = nx/2;
	tmp1[onx/2] = tmp[onx/2];
	for (i=onx/2+rc/2, phi = 0; i< onx; i++)
	{
		dphi = tmp[i] - tmp[i-1];
		if (dphi > M_PI)	phi -= 2*M_PI;
		if (dphi < -M_PI)	phi += 2*M_PI;
		tmp1[i] = tmp[i] + phi;
	}		
	for (i=onx/2-rc/2, phi = 0; i>= 0; i--)
	{
		dphi = tmp[i] - tmp[i+1];
		if (dphi > M_PI)	phi -= 2*M_PI;
		if (dphi < -M_PI)	phi += 2*M_PI;
		tmp1[i] = tmp[i] + phi;
	}	
	for (i=onx/2+rc, phi = 0, w = 0; i< onx; i++)			
	{
		amp = zp[2*i] * zp[2*i];
		amp += zp[2*i+1] * zp[2*i+1];
		w += amp;
		phi += amp*tmp1[i];
	}
	return  (w != 0.0) ? phi/w : phi;
}


O_i	*image_band_pass_in_real_out_complex(O_i *dst, O_i *oi,int w_flag, int filter, int width, int rc)
{
	register int i;
	int nx, ny;
	union pix *ps;
	O_p *op = NULL;
	d_s *ds = NULL;

	
	if (oi == NULL || oi->im.data_type == IS_COMPLEX_IMAGE)		return NULL;
	ny = oi->im.ny;	nx = oi->im.nx;
	
	if (dst == NULL)	dst = create_one_image( nx/2,  ny, IS_COMPLEX_IMAGE);
	else if (dst->im.data_type != IS_COMPLEX_IMAGE || dst->im.nx != oi->im.nx/2
		|| dst->im.ny != oi->im.ny || dst->im.n_f != 1)
	{
		free_one_image(dst);
		dst = create_one_image( nx/2,  ny, IS_COMPLEX_IMAGE);
	}
	if (dst == NULL)	return NULL;
	ps = dst->im.pixel;
	for (i=0 ; i< ny ; i++)
	{
	        display_title_message("doing line %d",i);
		extract_raw_line (oi, i, ps[i].fl);
		prepare_filtered_profile(ps[i].fl, nx, w_flag, filter, width);
	}
	inherit_from_im_to_im(dst,oi);
	uns_oi_2_oi(dst,oi);	
	dst->im.win_flag = oi->im.win_flag;
	if (oi->x_title != NULL)	set_im_x_title(dst,oi->x_title);
	if (oi->y_title != NULL)	set_im_y_title(dst,oi->y_title);
	set_im_title(dst, "\\stack{{Bandpass filter image}{%s}"
	"{\\pt8 filter %d width %d flag %s}}",(oi->title != NULL) ? oi->title 
	: "untitled",filter,width,(w_flag)?"periodic":"not periodic");
	set_formated_string(&dst->im.treatement,"Bandpass filter image %s"
	"filter %d width %d flag %s",(oi->title != NULL) ? oi->title 
	: "untitled",filter,width,(w_flag)?"periodic":"not periodic");
	op = create_and_attach_op_to_oi(dst, ny, ny, 0,0);
	if (op == NULL)		return win_printf_ptr("cannot create plot!");
	ds = op->dat[0];
	op->type = IM_SAME_Y_AXIS;
	uns_oi_2_op(dst, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
	set_formated_string(&ds->treatement,"Simple phase difference from n and n-1 profile");
	for (i=0 ; i< ny-1 ; i++)
	{	
		ds->xd[i] = find_simple_phase_shift_between_filtered_profile(ps[i].fl, ps[i+1].fl, nx, rc);
		ds->yd[i] = (float)i;
	}
	return dst;
}


int coarse_find_z_by_least_square_min(float *yt, int nxp, O_i *ois, float *min, int rc)
{
	register int i, j, k;
	int nx, ny;
	union pix *ps;
	float  tmp, *yr, val;
	
	if (ois->im.data_type != IS_COMPLEX_IMAGE)
		return win_printf_OK("image must be complex!");
	ny = ois->im.ny;	nx = ois->im.nx;
	if (2*nx != nxp)			return -1;
	ps = ois->im.pixel;
	for (i = k = 0; i< ny ; i++)
	{
		yr = ps[i].fl;
		/* first attemp pb !
		for (j = 0, val = 0; j < nx - rc/2; j +=2) 
		{
			tmp = yr[j] -  yt[j];
			val += tmp * tmp;
		}
		*/
		for (j = nx + rc, val = 0; j < nxp; j++) 
		{
			tmp = yr[j] -  yt[j];
			val += tmp * tmp;
		}
		if (i == 0 || val < *min)
		{
			k = i;
			*min = val;
		}
	}
	*min /= ((nx-rc)/2);
	return k;
}
float find_zero_of_3_points_polynome(float y_1, float y0, float y1)
{
	double a, b, c, x = 0, delta;
	
	a = ((y_1+y1)/2) - y0;
	b = (y1-y_1)/2;
	c = y0;
	delta = b*b - 4*a*c;
	if (a == 0)
	{
		x = (b != 0) ? -c/b : 0;
	}
	else if (delta >=0)
	{
		delta = sqrt(delta);
		x = (fabs(-b + delta) < fabs(-b - delta)) ? -b + delta : -b - delta ;
		x /= 2*a; 
	}
	return (float)x;
}

int find_z_profile_by_mean_square_and_phase(float *zp, int nx, O_i *oir, int w_flag, int filter, int width, int rc, float *z)
{
	register int k, j;
	int er = 0, ny;
	static int oldnx = 0;
	static float *yt = NULL;
	float min, tmp, ph_1, phi, ph1;
	union pix *psd;
	O_p *opz;
	d_s *dsrz;
	
	if (yt == NULL || oldnx != nx)
	{
		yt = (float*)realloc(yt,nx*sizeof(float));
		if (yt == NULL)		return -1;
		oldnx = nx;
	}
	psd = oir->im.pixel;
	ny = oir->im.ny;
	opz = (oir->n_op != 0) ? oir->o_p[0] : NULL;
	dsrz = (opz != NULL) ? opz->dat[0] : NULL;
	for (j=0; j< nx ; j++) yt[j] = zp[j];
	prepare_filtered_profile(yt, nx, w_flag, filter, width);
	j = coarse_find_z_by_least_square_min(yt, nx, oir, &min,  rc);
	j = (j == 0) ? j + 1: j;
	j = (j == ny-1) ? ny - 2: j;
	/*
	*z = oir->ay + oir->dy *  j;
	return er;	
	*/
	tmp = (float)j;
	phi = find_simple_phase_shift_between_filtered_profile(psd[j].fl, yt, nx, rc);
	tmp = (dsrz != NULL) ? dsrz->xd[j] : 0;
	tmp = (tmp != 0) ? (float)j + phi/tmp : (float)j;
	k = (int)(tmp+.5);  
	er = (abs(k - j) >= 2) ? 1 : 0;
	k = (k <= 0) ? 1: k;
	k = (k >= ny-1) ? ny - 2: k;
	
	ph_1 = (k-1 == j) ? phi : 
		find_simple_phase_shift_between_filtered_profile(psd[k-1].fl, yt, nx, rc);
	ph1 = (k+1 == j) ? phi :
		find_simple_phase_shift_between_filtered_profile(psd[k+1].fl, yt, nx, rc);
	phi = (k == j) ? phi :
		find_simple_phase_shift_between_filtered_profile(psd[k].fl, yt, nx, rc);
	*z = oir->ay + oir->dy * (find_zero_of_3_points_polynome(ph_1, phi, ph1) + k);
	return er;
}




/*	take a radial image profile of an image with a different scaling in 
 *	x and y, real x = i_image * ax,  y = i_image * ay
 *
 *
 *
 */

float *radial_non_pixel_square_image_sym_profile_in_array(float *rz, O_i *ois, float xc, float yc, int r_max, float ax, float ay)
{
  int i, j;
  union pix *ps;
  int onx, ony, ri, type;
  float z = 0, z1, x, y, r, p, p0 = 0, pr0 = 0;
  int xci, yci, imin, imax, jmin,jmax;
  
  for (i = 0; i < 2*r_max && i < 256; i++)	radial_dx[i] = 0;
  if (ois == NULL)	return NULL;
  if (rz == NULL)		rz = (float*)calloc(2*r_max,sizeof(float));
  if (rz == NULL)		return NULL;
  ps = ois->im.pixel;
  ony = ois->im.ny;	onx = ois->im.nx;
  type = ois->im.data_type;
  for (i = 0; i < 2*r_max; i++)	rz[i] = 0;
  xci = (int)xc;
  yci = (int)yc;
  
  imin = (int)(yc - (r_max/ay));
  imax = (int)(0.5 + yc + (r_max/ay));	
  jmin = (int)(xc -  r_max/ax);
  jmax = (int)(0.5 + xc + r_max/ax);
  imin = (imin < 0) ? 0 : imin;
  imax = (imax > ony) ? ony : imax;
  jmin = (jmin < 0) ? 0 : jmin;
  jmax = (jmax > onx) ? onx : jmax;	
  if (type == IS_COMPLEX_IMAGE)
    {  
      for (i = imin; i < imax; i++)
	{
	  y = ay * ((float)i - yc);
	  y *= y;
	  for (j = jmin; j < jmax; j++)
	    {
	      x = ax * ((float)j - xc);
	      x *= x;
	      r = (float)sqrt(x + y);
	      ri = (int)r;
	      if (ri > r_max)	continue;	/* bug break;*/
	      if ( ois->im.mode == RE)
		z = ps[i].fl[2*j];
	      else if ( ois->im.mode == IM)
		z = ps[i].fl[2*j+1];
	      else if ( ois->im.mode == AMP)
		{
		  z = ps[i].fl[2*j];
		  z1 = ps[i].fl[2*j+1];
		  z = sqrt(z*z + z1*z1);
		}
	      else if ( ois->im.mode == AMP_2)
		{
		  z = ps[i].fl[2*j];
		  z1 = ps[i].fl[2*j+1];
		  z = z*z + z1*z1;
		}				
	      else if ( ois->im.mode == LOG_AMP)
		{
		  z = ps[i].fl[2*j];
		  z1 = ps[i].fl[2*j+1];
		  z = z*z + z1*z1;
		  z = (z > 0) ? log10(z) : -40.0;
		}				
	      else
		{
		  win_printf("Unknown mode for complex image");
		  return NULL;
		}
	      p = r - ri;
	      if (ri == r_max)
		{
		  p0 += (1-p)*z;
		  pr0 += (1-p);
		  radial_dx[r_max] -= 1-p;
		}
	      else 
		{
		  rz[ri] += (1-p)*z;
		  rz[ri+r_max] += (1-p);
		  radial_dx[ri] += p;
		}
	      ri++;
	      if (ri < r_max)
		{
		  rz[ri] += p*z;
		  rz[ri+r_max] += p;
		  radial_dx[ri] -= 1-p;
		}
	    }
	}
    }
  else if (type == IS_FLOAT_IMAGE) 		
    {
      for (i = imin; i < imax; i++)
	{
	  y = ay * ((float)i - yc);
	  y *= y;
	  for (j = jmin; j < jmax; j++)
	    {
	      x = ax * ((float)j - xc);
	      x *= x;
	      r = (float)sqrt(x + y);
	      ri = (int)r;
	      if (ri > r_max)	continue;	/* bug break;*/
	      z = ps[i].fl[j];
	      p = r - ri;
	      if (ri == r_max)
		{
		  p0 += (1-p)*z;
		  pr0 += (1-p);
		  radial_dx[r_max] -= 1-p;
		}
	      else 
		{
		  rz[ri] += (1-p)*z;
		  rz[ri+r_max] += (1-p);
		  radial_dx[ri] += p;
		}
	      ri++;
	      if (ri < r_max)
		{
		  rz[ri] += p*z;
		  rz[ri+r_max] += p;
		  radial_dx[ri] -= 1-p;
		}
	    }
	}
    }
  else if (type == IS_INT_IMAGE)			
    {
      for (i = imin; i < imax; i++)
	{
	  y = ay * ((float)i - yc);
	  y *= y;
	  for (j = jmin; j < jmax; j++)
	    {
	      x = ax * ((float)j - xc);
	      x *= x;
	      r = (float)sqrt(x + y);
	      ri = (int)r;
	      if (ri > r_max)	continue;	/* bug break;*/
	      z = (float)ps[i].in[j];
	      p = r - ri;
	      if (ri == r_max)
		{
		  p0 += (1-p)*z;
		  pr0 += (1-p);
		  radial_dx[r_max] -= 1-p;
		}
	      else 
		{
		  rz[ri] += (1-p)*z;
		  rz[ri+r_max] += (1-p);
		  radial_dx[ri] += p;
		}
	      ri++;
	      if (ri < r_max)
		{
		  rz[ri] += p*z;
		  rz[ri+r_max] += p;
		  radial_dx[ri] -= 1-p;
		}
	    }
	}
    }
  else if (type == IS_CHAR_IMAGE)			
    {
      for (i = imin; i < imax; i++)
	{
	  y = ay * ((float)i - yc);
	  y *= y;
	  for (j = jmin; j < jmax; j++)
	    {
	      x = ax * ((float)j - xc);
	      x *= x;
	      r = (float)sqrt(x + y);
	      ri = (int)r;
	      if (ri > r_max)	continue;	/* bug break;*/
	      z = (float)ps[i].ch[j];
	      p = r - ri;
	      if (ri == r_max)
		{
		  p0 += (1-p)*z;
		  pr0 += (1-p);
		  radial_dx[r_max] -= 1-p;
		}
	      else 
		{
		  rz[ri] += (1-p)*z;
		  rz[ri+r_max] += (1-p);
		  radial_dx[ri] += p;
		}
	      ri++;
	      if (ri < r_max)
		{
		  rz[ri] += p*z;
		  rz[ri+r_max] += p;
		  radial_dx[ri] -= 1-p;
		}
	    }
	}
    }
  for (i = 0; i < r_max; i++)
    {
      rz[i] = (rz[i+r_max] == 0) ? 0 : rz[i]/rz[i+r_max];
      radial_dx[i] = (rz[i+r_max] == 0) ? 0 : radial_dx[i]/rz[i+r_max];
    }
  p0 = (pr0 == 0) ? 0 : p0/pr0;  
  for (i = 0; i < r_max; i++)		rz[i+r_max] = rz[i]; 
  for (i = 1; i < r_max; i++)		rz[r_max-i] = rz[r_max+i]; 
  rz[0] = p0;
  return rz;
}


/*	take a radial image profile of an image with a different scaling in 
 *	x and y, real x = i_image * ax,  y = i_image * ay
 *
 *      use smid P4 instruction
 *
 */

float *radial_non_pixel_square_image_sym_profile_in_array_sse(float *rz, O_i *ois, float xc, float yc, int r_max, float ax, float ay)
{
  int i, j;
  union pix *ps;
  int onx, ony, ri, type, onx_4;
  float z = 0, *r, p, p0 = 0, pr0 = 0, rmax2, tmp; // , z1, x, y
  int xci, yci, imin, imax, jmin, jmax, jmin_4, jmax_4;
  
  
  
  for (i = 0; i < 2*r_max && i < 256; i++)	radial_dx[i] = 0;
  if (ois == NULL)	return NULL;
  if (rz == NULL)		rz = (float*)calloc(2*r_max,sizeof(float));
  if (rz == NULL)		return NULL;
  ps = ois->im.pixel;
  ony = ois->im.ny;	onx = ois->im.nx;
  onx_4 = onx >> 2;
  type = ois->im.data_type;
  for (i = 0; i < 2*r_max; i++)	rz[i] = 0;
  rmax2 = r_max * r_max;
  xci = (int)xc;
  yci = (int)yc;
  imin = (int)(yc - (r_max/ay));
  imax = (int)(0.5 + yc + (r_max/ay));	
  jmin = (int)(xc -  r_max/ax);
  jmax = (int)(0.5 + xc + r_max/ax);
  imin = (imin < 0) ? 0 : imin;
  imax = (imax > ony) ? ony : imax;
  jmin = (jmin < 0) ? 0 : jmin;
  jmax = (jmax > onx) ? onx : jmax;
  //	jmin_4 = jmin = jmin >> 2;	jmin <<= 2;
  //jmax_4 = jmax = jmax >> 2;	jmax++; jmax <<= 2;
  jmin_4 = jmin >> 2;
  jmax_4 = 1+(jmax >> 2);
  jmax_4 = (jmax_4 > onx_4) ? onx_4 : jmax_4;
  
  
  /*win_printf("Imin %d imax %d, jmin %d jmax %d\njmin_4 %d jmax_4 %d"
    ,imin,imax,jmin,jmax,jmin_4,jmax_4); */
  
  fxc.f[0] = fxc.f[1] = fxc.f[2] = fxc.f[3] = xc;
  fyc.f[0] = fyc.f[1] = fyc.f[2] = fyc.f[3] = yc;
  fax.f[0] = fax.f[1] = fax.f[2] = fax.f[3] = ax;
  fay.f[0] = fay.f[1] = fay.f[2] = fay.f[3] = ay;
  f4.f[0] = f4.f[1] = f4.f[2] = f4.f[3] = 4;
  r = fr[0].f;
  if (type == IS_INT_IMAGE)			
    {
      f0.f[0] = 0; f0.f[1] = 1; f0.f[2] = 2; f0.f[3] = 3;  // pixel index 
      fx.v = __builtin_ia32_subps(f0.v, fxc.v);            // distance in pixel from center
      fx.v = __builtin_ia32_mulps (fax.v, fx.v);           // rescaling in x
      f4.v = __builtin_ia32_mulps (fax.v, f4.v);           // we prepare rescaled substraction
      for (j = 0; j < onx_4; j++)
	{
	  fx2[j].v = __builtin_ia32_mulps (fx.v, fx.v);    // we compute X2
	  fx.v = __builtin_ia32_addps(fx.v, f4.v);         // we move index
	}
      f0.f[0] = f0.f[1] = f0.f[2] = f0.f[3] = imin;        // starting y position
      fy.v = __builtin_ia32_subps(f0.v, fyc.v);            // distance in pixel to yc
      fy.v = __builtin_ia32_mulps (fay.v, fy.v);           // rescaled in y
      
      
      for (i = imin; i < imax; i++)
	{
	  fy2.v = __builtin_ia32_mulps (fy.v, fy.v);       // we compute y2 
	  fy.v = __builtin_ia32_addps(fy.v, fay.v);        // move by one rescaled pixel in y
	  tmp = rmax2 - fy2.f[0];
	  if (tmp < 0) continue;
	  tmp = sqrt(tmp);
	  tmp = (tmp+1)/ax;
	  jmin = (int)(xc - tmp);
	  jmax = (int)(xc + tmp + 1);
	  jmin = (jmin < 0) ? 0 : jmin;
	  jmax = (jmax > onx) ? onx : jmax;			
	  jmin_4 = jmin >> 2;
	  jmax_4 = 1+(jmax >> 2);
	  jmax_4 = (jmax_4 > onx_4) ? onx_4 : jmax_4;
	  for (j = jmin_4; j < jmax_4; j++)
	    {
	      fr[j].v = __builtin_ia32_addps(fy2.v, fx2[j].v);  // we compute x2 + y2
	      fr[j].v = __builtin_ia32_sqrtps(fr[j].v);         // we get the distance
	    }
	  for (j = jmin; j < jmax; j++)
	    {
	      ri = (int)r[j];
	      if (ri > r_max)	continue;	/* bug break;*/
	      z = (float)ps[i].in[j];
	      p = r[j] - ri;
	      if (ri == r_max)
		{
		  p0 += (1-p)*z;
		  pr0 += (1-p);
		  radial_dx[r_max] -= 1-p;
		}
	      else 
		{
		  rz[ri] += (1-p)*z;
		  rz[ri+r_max] += (1-p);
		  radial_dx[ri] += p;
		}
	      ri++;
	      if (ri < r_max)
		{
		  rz[ri] += p*z;
		  rz[ri+r_max] += p;
		  radial_dx[ri] -= 1-p;
		}
	    }
	}
    }
  else if (type == IS_CHAR_IMAGE)			
    {
      f0.f[0] = 0; f0.f[1] = 1; f0.f[2] = 2; f0.f[3] = 3;  // pixel index 
      fx.v = __builtin_ia32_subps(f0.v, fxc.v);            // distance in pixel from center
      fx.v = __builtin_ia32_mulps (fax.v, fx.v);           // rescaling in x
      f4.v = __builtin_ia32_mulps (fax.v, f4.v);           // we prepare rescaled substraction
      for (j = 0; j < onx_4; j++)
	{
	  fx2[j].v = __builtin_ia32_mulps (fx.v, fx.v);    // we compute X2
	  fx.v = __builtin_ia32_addps(fx.v, f4.v);         // we move index
	}
      f0.f[0] = f0.f[1] = f0.f[2] = f0.f[3] = imin;        // starting y position
      fy.v = __builtin_ia32_subps(f0.v, fyc.v);            // distance in pixel to yc
      fy.v = __builtin_ia32_mulps (fay.v, fy.v);           // rescaled in y
      
      for (i = imin; i < imax; i++)
	{
	  fy2.v = __builtin_ia32_mulps (fy.v, fy.v);       // we compute y2 
	  fy.v = __builtin_ia32_addps(fy.v, fay.v);        // move by one rescaled pixel in y
	  tmp = rmax2 - fy2.f[0];
	  if (tmp < 0) continue;
	  tmp = sqrt(tmp);
	  tmp = (tmp+1)/ax;
	  jmin = (int)(xc - tmp);
	  jmax = (int)(xc + tmp + 1);
	  jmin = (jmin < 0) ? 0 : jmin;
	  jmax = (jmax > onx) ? onx : jmax;			
	  jmin_4 = jmin >> 2;
	  jmax_4 = 1+(jmax >> 2);
	  jmax_4 = (jmax_4 > onx_4) ? onx_4 : jmax_4;
	  for (j = jmin_4; j < jmax_4; j++)
	    {
	      fr[j].v = __builtin_ia32_addps(fy2.v, fx2[j].v);  // we compute x2 + y2
	      fr[j].v = __builtin_ia32_sqrtps(fr[j].v);         // we get the distance
	      /*
		if ((debug != CANCEL) && (j == (onx >> 3)) && (i%16 == 0))
		debug = win_printf("y2 = %g i = %d j %d\n%g %g %g %g\n%g %g %g %g",
		fy2.f[0],i,j,fr[j].f[0],fr[j].f[1],fr[j].f[2],fr[j].f[3],
		r[4*j],r[(4*j)+1],r[(4*j)+2],r[(4*j)+3]);
	      */
	    }
	  
	  for (j = jmin; j < jmax; j++)
	    {
	      ri = (int)r[j];
	      if (ri > r_max)	continue;	/* bug break;*/
	      z = (float)ps[i].ch[j];
	      p = r[j] - ri;
	      if (ri == r_max)
		{
		  p0 += (1-p)*z;
		  pr0 += (1-p);
		  radial_dx[r_max] -= 1-p;
		}
	      else 
		{
		  rz[ri] += (1-p)*z;
		  rz[ri+r_max] += (1-p);
		  radial_dx[ri] += p;
		}
	      ri++;
	      if (ri < r_max)
		{
		  rz[ri] += p*z;
		  rz[ri+r_max] += p;
		  radial_dx[ri] -= 1-p;
		}
	    }
	}
    }
  for (i = 0; i < r_max; i++)
    {
      rz[i] = (rz[i+r_max] == 0) ? 0 : rz[i]/rz[i+r_max];
      radial_dx[i] = (rz[i+r_max] == 0) ? 0 : radial_dx[i]/rz[i+r_max];
    }
  p0 = (pr0 == 0) ? 0 : p0/pr0;  
  for (i = 0; i < r_max; i++)		rz[i+r_max] = rz[i]; 
  for (i = 1; i < r_max; i++)		rz[r_max-i] = rz[r_max+i]; 
  rz[0] = p0;
  return rz;
}



#endif
