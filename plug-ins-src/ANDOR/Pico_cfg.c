/*
*    Plug-in program for picotwist config file
*
*    V. Croquette &JF Allemand
*/

#ifndef _PICO_CFG_C_
#define _PICO_CFG_C_

# include "allegro.h"
# include "winalleg.h"
# include "xvin.h"

/* If you include other plug-ins header do it here*/ 

//# include "../CVB/XVCVB.h"
/* But not below this define */

# define BUILDING_PLUGINS_DLL
#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "Pico_cfg.h"


//oi_TRACK
//oi_TRACK->im.nx;
//Taille pixel, resolution
//units!!!!
//ctime(&frame_time)
//unsigned long frame_time

int set_pico_config_file_parameters(void);

int Open_Pico_config_file(void)
{

  char file[256];
  extern char prog_dir[256];
  
  sprintf(file,"%sPicotwist.cfg",prog_dir);
  override_config_file(file);

  return D_O_K;
}  


int Close_Pico_config_file(void)
{
	override_config_file(NULL);

  return D_O_K;
}


int write_Pico_config_file(void)
{
    set_pico_config_file_parameters();//All written...not optimum
	flush_config_file();
	return D_O_K;
}	

    
    
Micro_param * duplicate_pico_Micro_param( Micro_param *pico_in, Micro_param *pico_out)
{
    if (pico_in == NULL) return win_printf_OK("IN NULL POINTER");
    if (pico_out == NULL) return win_printf_OK("OUT NULL POINTER");//pico_out = (Pico_param *)calloc(1, sizeof(Pico_param);
        
    pico_out->config_microscope_time = pico_in->config_microscope_time;
    
	pico_out->microscope_user_name = pico_in->microscope_user_name;				/* the name of the microscope */
	pico_out->microscope_manufacturer_name = pico_in->microscope_manufacturer_name;				/* the name of the microscope */
	pico_out->microscope_type = pico_in->microscope_type;				/* COMMERCIAL or HOME_MADE		*/
	pico_out->PicoTwist_model = pico_in->PicoTwist_model;	
 	pico_out->zoom_factor = pico_in->zoom_factor;
	pico_out->zoom_min = pico_in->zoom_min,
  	pico_out->zoom_max = pico_in->zoom_max;
	pico_out->field_factor = pico_in->field_factor;
	pico_out->microscope_factor = pico_in->microscope_factor;
	pico_out->imaging_lens_focal_distance_in_mm = pico_in->imaging_lens_focal_distance_in_mm;
	pico_out->x_pixel_2_microns = pico_in->x_pixel_2_microns;
	pico_out->y_pixel_2_microns = pico_in->y_pixel_2_microns;
	pico_out->LED_wavelength = pico_in->LED_wavelength;
	
	return pico_out;
}	
	
Camera_param * duplicate_pico_Camera_param( Camera_param *pico_in, Camera_param *pico_out)
{
    if (pico_in == NULL) return win_printf_OK("IN NULL POINTER");
    if (pico_out == NULL) return win_printf_OK("OUT NULL POINTER");//pico_out = (Pico_param *)calloc(1, sizeof(Pico_param);
        
   	pico_out->config_camera_time = pico_in->config_camera_time;
	pico_out->camera_manufacturer = pico_in->camera_manufacturer;
	pico_out->camera_model = pico_in->camera_model;
	pico_out->camera_frequency_in_Hz = pico_in->camera_frequency_in_Hz;
	pico_out->camera_config_file = pico_in->camera_config_file;
    pico_out->camera_software_program = pico_in->camera_software_program;
    return pico_out;
}	    
	
Obj_param * duplicate_pico_Obj_param( Obj_param *pico_in, Obj_param *pico_out)
{
    if (pico_in == NULL) return win_printf_OK("IN NULL POINTER");
    if (pico_out == NULL) return win_printf_OK("OUT NULL POINTER");//pico_out = (Pico_param *)calloc(1, sizeof(Pico_param);
	pico_out->config_objective_time = pico_in->config_objective_time;
 	pico_out->objective_manufacturer = pico_in->objective_manufacturer;
	pico_out->objective_magnification = pico_in->objective_magnification;
	pico_out->immersion_type = pico_in->immersion_type;
	pico_out->objective_numerical_aperture = pico_in->objective_numerical_aperture;
	
    return pico_out;
}

Piezzo_param * duplicate_pico_Piezzo_param( Piezzo_param *pico_in, Piezzo_param *pico_out)
{
    if (pico_in == NULL) return win_printf_OK("IN NULL POINTER");
    if (pico_out == NULL) return win_printf_OK("OUT NULL POINTER");//pico_out = (Pico_param *)calloc(1, sizeof(Pico_param);
	pico_out->config_piezzo_time = pico_in->config_piezzo_time;
	pico_out->Is_Piezzo_in = pico_in->Is_Piezzo_in;
	pico_out->Piezzo_max_expansion = pico_in->Piezzo_max_expansion;
	pico_out->Piezzo_driver = pico_in->Piezzo_driver;
	pico_out->Piezzo_direction = pico_in->Piezzo_direction;
	pico_out->Piezzo_model = pico_in->Piezzo_model;
    return pico_out;
}	
Motor_param * duplicate_pico_Motor_param( Motor_param *pico_in, Motor_param *pico_out)
{
    if (pico_in == NULL) return win_printf_OK("IN NULL POINTER");
    if (pico_out == NULL) return win_printf_OK("OUT NULL POINTER");
	pico_out->config_motor_time = pico_in->config_motor_time;
	pico_out->rotation_motor_manufacturer = pico_in->rotation_motor_manufacturer;
	pico_out->rotation_motor_model = pico_in->rotation_motor_model;
	pico_out->P_rotation_motor = pico_in->P_rotation_motor;
	pico_out->I_rotation_motor = pico_in->I_rotation_motor;
	pico_out->D_rotation_motor = pico_in->D_rotation_motor;
	pico_out->rotation_max_velocity = pico_in->rotation_max_velocity;
	pico_out-> rotation_velocity = pico_in-> rotation_velocity;
	pico_out->rotation_motor_position = pico_in->rotation_motor_position;
	
	pico_out->translation_motor_manufacturer = pico_in->translation_motor_manufacturer;
	pico_out->translation_motor_model = pico_in->translation_motor_model;
	pico_out->P_translation_motor = pico_in->P_translation_motor;
	pico_out->I_translation_motor = pico_in->I_translation_motor;
	pico_out->D_translation_motor = pico_in->D_translation_motor;
	pico_out-> translation_motor_inverted= pico_in-> translation_motor_inverted;
	pico_out-> translation_max_velocity=	pico_out-> translation_max_velocity;
	pico_out-> translation_velocity=	pico_in-> translation_velocity;
	pico_out-> translation_motor_position = pico_in-> translation_motor_position;
	pico_out-> translation_max_pos = pico_in-> translation_max_pos;
	
	pico_out->motor_controller = pico_in->motor_controller;
    return pico_out;
}	
	
	
Serial_param * duplicate_pico_Serial_param( Serial_param *pico_in, Serial_param *pico_out)
{
    if (pico_in == NULL) return win_printf_OK("IN NULL POINTER");
    if (pico_out == NULL) return win_printf_OK("OUT NULL POINTER");//pico_out = (Pico_param *)calloc(1, sizeof(Pico_param);
	pico_out->config_serial_time = pico_in->config_serial_time;
	pico_out->serial_port = pico_in->serial_port;
	pico_out->serial_BaudRate = pico_in->serial_BaudRate;
    pico_out->serial_ByteSize = pico_in->serial_ByteSize;
    pico_out->serial_StopBits = pico_in->serial_StopBits;
    pico_out->serial_Parity = pico_in->serial_Parity;
    pico_out->serial_fDtrControl = pico_in->serial_fDtrControl;
    pico_out->serial_fRtsControl = pico_in->serial_fRtsControl;

    return pico_out;
} 


Pico_parameter * duplicate_Pico_parameter(Pico_parameter *pico_in, Pico_parameter *pico_out)
{
    if (pico_in == NULL) return win_printf_OK("IN NULL POINTER");
    if (pico_out == NULL) return win_printf_OK("OUT NULL POINTER");//pico_out = (Pico_param *)calloc(1, sizeof(Pico_param);
    
    duplicate_pico_Micro_param(pico_in->micro_param,pico_out->micro_param);
    duplicate_pico_Camera_param(pico_in->camera_param,pico_out->camera_param);
    duplicate_pico_Obj_param(pico_in->obj_param,pico_out->obj_param);
    duplicate_pico_Piezzo_param(pico_in->piezzo_param,pico_out->piezzo_param);
    duplicate_pico_Motor_param(pico_in->motor_param,pico_out->motor_param);
    duplicate_pico_Serial_param(pico_in->serial_param,pico_out->serial_param);
    
    return pico_out;
}



/////////////////////////////////////////SETTING PARAMETERS//////////////////////////////////////////



	


int set_pico_config_file_Micro_parameters(void)
{
    Open_Pico_config_file();
    set_config_string("PICO_CFG", "config_microscope_time", Pico_param.micro_param.config_microscope_time);
    set_config_string("PICO_CFG", "microscope_user_name", Pico_param.micro_param.microscope_user_name);
    set_config_string("PICO_CFG", "microscope_manufacturer_name", Pico_param.micro_param.microscope_manufacturer_name);
    set_config_int("PICO_CFG", "microscope_type", Pico_param.micro_param.microscope_type);
    set_config_string("PICO_CFG", "PicoTwist_model", Pico_param.micro_param.PicoTwist_model);
    set_config_float("PICO_CFG", "zoom_factor", Pico_param.micro_param.zoom_factor);
    set_config_float("PICO_CFG", "zoom_min", Pico_param.micro_param.zoom_min);
    set_config_float("PICO_CFG", "zoom_max", Pico_param.micro_param.zoom_max);
    set_config_float("PICO_CFG", "field_factor", Pico_param.micro_param.field_factor);
    set_config_float("PICO_CFG", "microscope_factor", Pico_param.micro_param.microscope_factor);
    set_config_float("PICO_CFG", "imaging_lens_focal_distance_in_mm", Pico_param.micro_param.imaging_lens_focal_distance_in_mm);
    set_config_float("PICO_CFG", "x_pixel_2_microns", Pico_param.micro_param.x_pixel_2_microns);
    set_config_float("PICO_CFG", "y_pixel_2_microns", Pico_param.micro_param.y_pixel_2_microns);
    set_config_int("PICO_CFG", "LED_wavelength", Pico_param.micro_param.LED_wavelength);
    write_Pico_config_file();
    Close_Pico_config_file();
    return D_O_K;
}    
    
    
int set_pico_config_file_Camera_parameters(void)
{
	Open_Pico_config_file();
    set_config_string("PICO_CFG", "config_camera_time", Pico_param.camera_param.config_camera_time);
    set_config_string("PICO_CFG", "camera_manufacturer", Pico_param.camera_param.camera_manufacturer);
    set_config_string("PICO_CFG", "camera_model", Pico_param.camera_param.camera_model);
    set_config_int("PICO_CFG", "camera_frequency_in_Hz", Pico_param.camera_param.camera_frequency_in_Hz);
    set_config_string("PICO_CFG", "camera_config_file", Pico_param.camera_param.camera_config_file);
    set_config_int("PICO_CFG", "camera_software_program", Pico_param.camera_param.camera_software_program);;
    write_Pico_config_file();
    Close_Pico_config_file();
    return D_O_K;
}
        
int set_pico_config_file_Obj_parameters(void)
{
	Open_Pico_config_file();
    set_config_string("PICO_CFG", "config_objective_time", Pico_param.obj_param.config_objective_time);
    set_config_string("PICO_CFG", "objective_manufacturer", Pico_param.obj_param.objective_manufacturer);
    set_config_float("PICO_CFG", "objective_magnification", Pico_param.obj_param.objective_magnification);
    set_config_int("PICO_CFG", "immersion_type", Pico_param.obj_param.immersion_type);
    set_config_float("PICO_CFG", "objective_numerical_aperture", Pico_param.obj_param.objective_numerical_aperture);
    write_Pico_config_file();
    Close_Pico_config_file();
    return D_O_K;
}
    
int set_pico_config_file_Piezzo_parameters(void)
{
    Open_Pico_config_file();
	set_config_string("PICO_CFG", "config_piezzo_time", Pico_param.piezzo_param.config_piezzo_time);
    set_config_int("PICO_CFG", "Is_Piezzo_in", Pico_param.piezzo_param.Is_Piezzo_in);
    set_config_string("PICO_CFG", "Is_Piezzo_in", Pico_param.piezzo_param.Piezzo_model);
    set_config_int("PICO_CFG", "Piezzo_max_expansion", Pico_param.piezzo_param.Piezzo_max_expansion);
    set_config_int("PICO_CFG", "Piezzo_driver", Pico_param.piezzo_param.Piezzo_driver);
    set_config_int("PICO_CFG", "Piezzo_direction", Pico_param.piezzo_param.Piezzo_direction);
    write_Pico_config_file();
    Close_Pico_config_file();
    return D_O_K;
}
    
int set_pico_config_file_Motor_parameters(void)
{
    Open_Pico_config_file();
    set_config_string("PICO_CFG", "config_motor_time", Pico_param.motor_param.config_motor_time);
    set_config_string("PICO_CFG", "rotation_motor_manufacturer", Pico_param.motor_param.rotation_motor_manufacturer);
	set_config_string("PICO_CFG", "rotation_motor_model", Pico_param.motor_param.rotation_motor_model);
	set_config_float("PICO_CFG", "P_rotation_motor", Pico_param.motor_param.P_rotation_motor);
	set_config_float("PICO_CFG", "I_rotation_motor", Pico_param.motor_param.I_rotation_motor);
	set_config_float("PICO_CFG", "D_rotation_motor", Pico_param.motor_param.D_rotation_motor);
	set_config_float("PICO_CFG", "rotation_max_velocity", Pico_param.motor_param.rotation_max_velocity);
	set_config_float("PICO_CFG", "rotation_velocity", Pico_param.motor_param.rotation_velocity);
	set_config_float("PICO_CFG", "rotation_motor_position", Pico_param.motor_param.rotation_motor_position);
 	set_config_string("PICO_CFG", "translation_motor_manufacturer", Pico_param.motor_param.translation_motor_manufacturer);
	set_config_string("PICO_CFG", "translation_motor_model", Pico_param.motor_param.translation_motor_model);
	set_config_float("PICO_CFG", "P_translation_motor", Pico_param.motor_param.P_translation_motor);
	set_config_float("PICO_CFG", "I_translation_motor", Pico_param.motor_param.I_translation_motor);
	set_config_float("PICO_CFG", "D_translation_motor", Pico_param.motor_param.D_translation_motor);
	set_config_int("PICO_CFG", "motor_controller", Pico_param.motor_param.motor_controller);
    write_Pico_config_file();
    Close_Pico_config_file();
    return D_O_K;
}
    
    
    
int set_pico_config_file_Serial_parameters(void)
{
    Open_Pico_config_file();
    set_config_string("PICO_CFG", "config_serial_time", Pico_param.serial_param.config_serial_time);
    set_config_int("PICO_CFG", "serial_port", Pico_param.serial_param.serial_port);
	set_config_int("PICO_CFG", "serial_BaudRate", Pico_param.serial_param.serial_BaudRate);
    set_config_int("PICO_CFG", "serial_ByteSize", Pico_param.serial_param.serial_ByteSize);
    set_config_int("PICO_CFG", "serial_StopBits", Pico_param.serial_param.serial_StopBits);
    set_config_int("PICO_CFG", "serial_Parity", Pico_param.serial_param.serial_Parity);
    set_config_int("PICO_CFG", "serial_fDtrControl", Pico_param.serial_param.serial_fDtrControl);
    set_config_int("PICO_CFG", "serial_fRtsControl", Pico_param.serial_param.serial_fRtsControl);
    write_Pico_config_file();
    Close_Pico_config_file();
    return D_O_K;
}


int set_pico_config_file_all_parameters(void)
{
    set_pico_config_file_Micro_parameters();
    set_pico_config_file_Camera_parameters();
    set_pico_config_file_Obj_parameters();
    set_pico_config_file_Piezzo_parameters();
    set_pico_config_file_Motor_parameters();
    set_pico_config_file_Serial_parameters();
    return D_O_K;
}    

////////////////////////////////////LOADING PARAMETERS/////////////////////////////////////

   
 
int load_Pico_Micro_config(void)
{   
    Open_Pico_config_file();
    
    Pico_param.micro_param.config_microscope_time = get_config_string("PICO_CFG", "config_microscope_time", NULL);//Default_Pico_param.micro_param.config_microscope_time);
    Pico_param.micro_param.microscope_user_name = get_config_string("PICO_CFG", "microscope_user_name", NULL);//Default_Pico_param.micro_param.microscope_user_name);
    Pico_param.micro_param.microscope_manufacturer_name= get_config_string("PICO_CFG", "microscope_manufacturer_name", NULL);//Default_Pico_param.micro_param.microscope_manufacturer_name);
    Pico_param.micro_param.microscope_type = get_config_string("PICO_CFG", "microscope_type", Default_Pico_param.micro_param.microscope_type);
    Pico_param.micro_param.PicoTwist_model=get_config_string("PICO_CFG", "PicoTwist_model", NULL);//Default_Pico_param.micro_param.PicoTwist_model);
    Pico_param.micro_param.zoom_factor =    get_config_float("PICO_CFG", "zoom_factor", 1.);//Default_Pico_param.micro_param.zoom_factor);
    Pico_param.micro_param.zoom_min =   get_config_float("PICO_CFG", "zoom_min", 1.);//Default_Pico_param.micro_param.zoom_min);
    Pico_param.micro_param.zoom_max =    get_config_float("PICO_CFG", "zoom_max", 1.);//Default_Pico_param.micro_param.zoom_max);
    Pico_param.micro_param.field_factor =    get_config_float("PICO_CFG", "field_factor", 1.);// Default_Pico_param.micro_param.field_factor);
    Pico_param.micro_param.microscope_factor =    get_config_float("PICO_CFG", "microscope_factor", 1.);// Default_Pico_param.micro_param.microscope_factor);
    Pico_param.micro_param.imaging_lens_focal_distance_in_mm =    get_config_float("PICO_CFG", "imaging_lens_focal_distance_in_mm",  250.);//Default_Pico_param.micro_param.imaging_lens_focal_distance_in_mm);
    Pico_param.micro_param.x_pixel_2_microns =	get_config_float("PICO_CFG", "x_pixel_2_microns",  10.);//Default_Pico_param.micro_param.x_pixel_2_microns);
    Pico_param.micro_param.y_pixel_2_microns =	get_config_float("PICO_CFG", "y_pixel_2_microns",  10.);//Default_Pico_param.micro_param.y_pixel_2_microns);
    Pico_param.micro_param.LED_wavelength =	get_config_int("PICO_CFG", "LED_wavelength", 635);//Default_Pico_param.micro_param.LED_wavelength);
	
    Close_Pico_config_file();
	
    return D_O_K
}   
	
 
int load_Pico_Camera_config(void)
{   
    Open_Pico_config_file();
 
 
	Pico_param.camera_param.config_camera_time = get_config_string("PICO_CFG", "config_camera_time",NULL);// Default_Pico_param.camera_param.config_camera_time);
    Pico_param.camera_param.camera_manufacturer =	get_config_string("PICO_CFG", "camera_manufacturer", NULL);//Default_Pico_param.camera_param.camera_manufacturer);
    Pico_param.camera_param.camera_model =	get_config_string("PICO_CFG", "camera_model", NULL);//Default_Pico_param.camera_param.camera_model);
    Pico_param.camera_param.camera_frequency_in_Hz = 	get_config_int("PICO_CFG", "camera_frequency_in_Hz",  60.);//Default_Pico_param.camera_param.camera_frequency_in_Hz);
    Pico_param.camera_param.camera_config_file =	get_config_string("PICO_CFG", "camera_config_file",NULL);// Default_Pico_param.camera_param.camera_config_file);
    Pico_param.camera_param.camera_software_program = 	get_config_int("PICO_CFG", "camera_software_program",  1);//Default_Pico_param.camera_param.camera_software_program);;
     
    Close_Pico_config_file();
	
    return D_O_K;
}


int load_Pico_Obj_config(void)
{   
    Open_Pico_config_file();
    
    Pico_param.obj_param.config_objective_time = get_config_string("PICO_CFG", "config_objective_time", NULL);//Default_Pico_param.obj_param.config_objective_time);
    Pico_param.obj_param.objective_manufacturer =	get_config_string("PICO_CFG", "objective_manufacturer", NULL);//Default_Pico_param.obj_param.objective_manufacturer);
    Pico_param.obj_param.objective_magnification = 	get_config_float("PICO_CFG", "objective_magnification", 1.);//Default_Pico_param.obj_param.objective_magnification);
    Pico_param.obj_param.immersion_type =	get_config_int("PICO_CFG", "immersion_type", 1);//Default_Pico_param.obj_param.immersion_type);
    Pico_param.obj_param.objective_numerical_aperture =	get_config_float("PICO_CFG", "objective_numerical_aperture",1.);// Default_Pico_param.obj_param.objective_numerical_aperture);
    
    Close_Pico_config_file();
	
    return D_O_K;
}    
    

int load_Pico_Piezzo_config(void)
{   
    Open_Pico_config_file();
    
    Pico_param.piezzo_param.config_piezzo_time = get_config_string("PICO_CFG", "config_piezzo_time", NULL);//Default_Pico_param.piezzo_param.config_piezzo_time);
    Pico_param.piezzo_param.Is_Piezzo_in =	get_config_int("PICO_CFG", "Is_Piezzo_in", 1);//Default_Pico_param.piezzo_param.Is_Piezzo_in);
    Pico_param.piezzo_param.Piezzo_model =	get_config_string("PICO_CFG", "Is_Piezzo_in", NULL);//Default_Pico_param.piezzo_param.Piezzo_model);
    Pico_param.piezzo_param.Piezzo_max_expansion =	get_config_int("PICO_CFG", "Piezzo_max_expansion", 250);//Default_Pico_param.piezzo_param.Piezzo_max_expansion);
    Pico_param.piezzo_param..Piezzo_driver =	get_config_int("PICO_CFG", "Piezzo_driver", 1);//Default_Pico_param.piezzo_param.Piezzo_driver);
    Pico_param.piezzo_param..Piezzo_direction =	get_config_int("PICO_CFG", "Piezzo_direction", Default_Pico_param.piezzo_param.Piezzo_direction);
    
    Close_Pico_config_file();
	
    return D_O_K;
}

int load_Pico_Motor_config(void)
{   
    Open_Pico_config_file();
    
    Pico_param.motor_param.config_motor_time = get_config_string("PICO_CFG", "config_config_time",NULL);// Default_Pico_param.motor_param.config_motor_time);
    Pico_param.motor_param.rotation_motor_manufacturer =	get_config_string("PICO_CFG", "rotation_motor_manufacturer",NULL);// Default_Pico_param.motor_param.Piezzo_model);
	Pico_param.motor_param.rotation_motor_model =	get_config_string("PICO_CFG", "rotation_motor_model", NULL);//Default_Pico_param.motor_param.Piezzo_model);
	Pico_param.motor_param.P_rotation_motor=	get_config_float("PICO_CFG", "P_rotation_motor", 200.);//Default_Pico_param.motor_param.P_rotation_motor);
	Pico_param.motor_param.I_rotation_motor=	get_config_float("PICO_CFG", "I_rotation_motor", 100.);//Default_Pico_param.motor_param.I_rotation_motor);
	Pico_param.motor_param.D_rotation_motor=	get_config_float("PICO_CFG", "D_rotation_motor", 50.);//Default_Pico_param.motor_param.D_rotation_motor);
	Pico_param.motor_param.rotation_max_velocity=	get_config_float("PICO_CFG", "rotation_max_velocity",1.);// Default_Pico_param.motor_param.rotation_max_velocity);
	Pico_param.motor_param.rotation_velocity=	get_config_float("PICO_CFG", "rotation_velocity", 1.);//Default_Pico_param.motor_param.rotation_velocity);
	Pico_param.motor_param.rotation_motor_position=	get_config_float("PICO_CFG", "rotation_motor_position",0);// Default_Pico_param.motor_param.rotation_motor_position);
	Pico_param.motor_param.translation_motor_inverted=	get_config_int("PICO_CFG", "translation_motor_inverted",1);// Default_Pico_param.motor_param.translation_motor_inverted);
	Pico_param.motor_param.translation_max_velocity=	get_config_float("PICO_CFG", "translation_max_velocity", 15.);//Default_Pico_param.motor_param.translation_max_velocity);
	Pico_param.motor_param.translation_velocity=	get_config_float("PICO_CFG", "translation_velocity", 1);//Default_Pico_param.motor_param.translation_velocity);
	Pico_param.motor_param.translation_motor_position=	get_config_float("PICO_CFG", "translation_motor_position",0);// Default_Pico_param.motor_param.translation_motor_position);
	Pico_param.motor_param.translation_max_pos=	get_config_float("PICO_CFG", "translation_max_pos", 25);//Default_Pico_param.motor_param.translation_max_pos);
 	Pico_param.motor_param.translation_motor_manufacturer  =	get_config_string("PICO_CFG", "translation_motor_manufacturer", NULL);//Default_Pico_param.motor_param.translation_motor_manufacturer);
	Pico_param.motor_param.translation_motor_model  =	get_config_string("PICO_CFG", "translation_motor_model", NULL);//Default_Pico_param.motor_param.translation_motor_model);
	Pico_param.motor_param.P_translation_motor=	get_config_float("PICO_CFG", "P_translation_motor", 200);//Default_Pico_param.motor_param.P_translation_motor);
	Pico_param.motor_param.I_translation_motor=	get_config_float("PICO_CFG", "I_translation_motor", 100);//Default_Pico_param.motor_param.I_translation_motor);
	Pico_param.motor_param.D_translation_motor=	get_config_float("PICO_CFG", "D_translation_motor", 50);//Default_Pico_param.motor_param.D_translation_motor);
//A FINIR IL EN MANQUE peut etre

    Pico_param.motor_param.motor_controller =	get_config_int("PICO_CFG", "motor_controller", 0);//Default_Pico_param.motor_param.motor_controller);
    
    Close_Pico_config_file();
	
    return D_O_K;
}    


int load_Pico_Serial_config(void)
{   
    Open_Pico_config_file();
    Pico_param.serial_param.config_serial_time = get_config_string("PICO_CFG", "config_serial_time", NULL);//Default_Pico_param.serial_param.config_serial_time);
    Pico_param.serial_param.serial_port=	get_config_int("PICO_CFG", "serial_port", 1);//Default_Pico_param.serial_param.serial_port);
	Pico_param.serial_param.serial_BaudRate=	get_config_int("PICO_CFG", "serial_BaudRate",9600);// Default_Pico_param.serial_param.serial_BaudRate);
    Pico_param.serial_param.serial_ByteSize=	get_config_int("PICO_CFG", "serial_ByteSize", 8);//Default_Pico_param.serial_param.serial_ByteSize);
    Pico_param.serial_param.serial_StopBits=	get_config_int("PICO_CFG", "serial_StopBits", 1);//Default_Pico_param.serial_param.serial_StopBits);
    Pico_param.serial_param.serial_Parity=	get_config_int("PICO_CFG", "serial_Parity", 1);//Default_Pico_param.serial_param.serial_Parity);
    Pico_param.serial_param.serial_fDtrControl= get_config_int("PICO_CFG", "serial_fDtrControl", 1);//Default_Pico_param.serial_param.serial_fDtrControl);
    Pico_param.serial_param.serial_fRtsControl= get_config_int("PICO_CFG", "serial_fRtsControl", 1);//Default_Pico_param.serial_param..serial_fRtsControl);
    //Prevoir time ctime(&frame_time) time(&(oi->im.time));
	
     Close_Pico_config_file();
	
    return D_O_K;
}   


int load_Pico_config(void)
{
    load_Pico_Micro_config();
    load_Pico_Camera_config();
    load_Pico_Obj_config();
    load_Pico_Piezzo_config();
    load_Pico_Motor_config();
    load_Pico_Serial_config();
    return D_O_K;
} 

///////////////////////////////////////////////////////////////////////////////////////////////////////
// WE GO TO THE MODIFICATION PART/////////////////////////////////////////////////////////////////////
 
int change_microscope_config(void)
{
 	int change_button = 1;
    Micro_param tmp_pico_param;
    int i;
   //  load_Pico_config();
    char *question = NULL;
      	
  	duplicate_pico_Micro_param(&Pico_param.micro_param,&tmp_pico_param);
  	
   sprintf(question,"Last modification %s\n",Pico_param.micro_param.config_microscope_time);
   strcat(question,"\n User name %s \n Manufacturer %s \n Type %R Commercial %r PicoTwist %r Homemade \n Model %s \n Zoom Factor %f \n"
   "Zoom Min %4f   Zoom Max %4f \n Field Factor %4f  Microscope Factor %4f \n Lens focal distance %4f"
   "LED Wavelength in nm %5d \n X pix2microns conversion factor %6f \nY pix2microns conversion factor %6f"
   "Do you want to change these settings? %R YES %r NO");
  	
   i = win_scanf(question,
   &(tmp_pico_param.microscope_user_name),//
   &tmp_pico_param.microscope_manufacturer_name,//
   &tmp_pico_param.chartype, //
   &tmp_pico_param.PicoTwist_model,//
   &tmp_pico_param.zoom_factor, //
   &tmp_pico_param.zoom_min, //
   &tmp_pico_param.zoom_max,//
   &tmp_pico_param.field_factor,//
   &tmp_pico_param.microscope_factor,//
   &tmp_pico_param.imaging_lens_focal_distance_in_mm,//
   &tmp_pico_param.LED_wavelength,//
   &tmp_pico_param.x_pixel_2_microns,//
   &tmp_pico_param.y_pixel_2_microns,//
   &change_button);//
   
  
  
  if (change_button == 0 && i != CANCEL)  
  {
      change_button = 1;
      win_scanf("Are you really sure you want to change the settings \n This may have unwanted effects \n %R YES %r NO",&change_button);
        if (change_button == 0 && i != CANCEL)  
          {
            duplicate_pico_Micro_param(&tmp_pico_param,&Pico_param.micro_param);
            Pico_param.micro_param.config_microscope_time = ctime(time());  
          	//Cette partie est a actualiser
          	write_Pico_config_file();
  	      }
  }        
  
  return D_O_K;   
}    


int change_objective_config(void)
{
 		
  	int change_button = 1;
    Obj_param tmp_pico_param;
    int i;
    char *question = NULL;
      	
  	duplicate_pico_Obj_param(&Pico_param.obj_param,&tmp_pico_param);
  	
   sprintf(question,"Last modification %s\n",Pico_param.obj_param.config_objective_time);
   strcat(question,"Manufacturer %s \n  Objective magnification %4f \n"
   "immersion_type %R Air %r Water %r Oil \n  objective numerical aperture %4f "
   "Do you want to change these settings? %R YES %r NO");
  	
   i = win_scanf(question,
    &tmp_pico_param.objective_manufacturer,//
   &tmp_pico_param.objective_magnification,//
   &tmp_pico_param.immersion_type, //
   &tmp_pico_param.objective_numerical_aperture,  
   &change_button);//
   
  
  
  if (change_button == 0 && i != CANCEL)  
  {
      change_button = 1;
      win_scanf("Are you really sure you want to change the settings \n This may have unwanted effects \n %R YES %r NO",&change_button);
        if (change_button == 0 && i != CANCEL)  
          {
            duplicate_pico_Obj_param(&tmp_pico_param,&Pico_param.obj_param);
            Pico_param.obj_param.config_objective_time = ctime(time());  
          	
          	write_Pico_config_file();
  	      }
  }        
  
  return D_O_K;   
  
  
  
} 

int change_camera_config(void)
{
 		
  	int change_button = 1;
    Camera_param tmp_pico_param;
    int i;
    char *question = NULL;
      	
  	duplicate_pico_Camera_param(&Pico_param.camera_param,&tmp_pico_param);
  	
   sprintf(question,"Last modification %s\n",Pico_param.camera_param.config_camera_time);
   strcat(question,"Manufacturer %s \n  Model %s \n"
   "Frequency %4d \n Config file %s \n  objective numerical aperture %4f "
   "Do you want to change these settings? %R YES %r NO");
  	
   i = win_scanf(question,
    &tmp_pico_param.camera_manufacturer,//
   &tmp_pico_param.camera_model,//
   &tmp_pico_param.camera_frequency_in_Hz, //
   &tmp_pico_param.camera_config_file,  
   &change_button);//
   
  
  
  if (change_button == 0 && i != CANCEL)  
  {
      change_button = 1;
      win_scanf("Are you really sure you want to change the settings \n This may have unwanted effects \n %R YES %r NO",&change_button);
        if (change_button == 0 && i != CANCEL)  
          {
            duplicate_pico_Camera_param(&tmp_pico_param,&Pico_param.camera_param);
            Pico_param.camera_param.config_camera_time = ctime(time());  
          	
          	write_Pico_config_file();
  	      }
  }       
  return D_O_K;   
}   

int change_piezzo_config(void)
{
 	int change_button = 1;
    Piezzo_param tmp_pico_param;
    int i;
    char *question = NULL;
      	
  	duplicate_pico_Piezzo_param(&Pico_param.piezzo_param,&tmp_pico_param);
  	
   sprintf(question,"Last modification %s\n",Pico_micro_param.config_piezzo_time);
   strcat(question,"Are you using a piezzo %R Yes %r No \n  Model %s \n"
   "What is its maximum expansion in �m %4d \n Config file %s \n  What is its direction %R UP %r DOWN "
   "Do you want to change these settings? %R YES %r NO");
  	
   i = win_scanf(question,
    &tmp_pico_param.Is_Piezzo_in,//
   &tmp_pico_param.Piezzo_model,//
   &tmp_pico_param.Piezzo_max_expansion, //
   &tmp_pico_param.Piezzo_driver,
   &tmp_pico_param.Piezzo_direction,  
   &change_button);//
   
  
  
  if (change_button == 0 && i != CANCEL)  
  {
      change_button = 1;
      win_scanf("Are you really sure you want to change the settings \n This may have unwanted effects \n %R YES %r NO",&change_button);
        if (change_button == 0 && i != CANCEL)  
          {
            duplicate_pico_Piezzo_param(&tmp_pico_param,&Pico_param.piezzo_param);
            Pico_param.piezzo_param.config_piezzo_time = ctime(time());  
          	
          	write_Pico_config_file();
  	      }
  } 	
  
  return D_O_K;   
} 


//TO FINISH


int change_motor_config(void)
{
 	int change_button = 1;
    Motor_param tmp_pico_param;
    int i;
    char *question = NULL;
      	
  	duplicate_pico_Motor_param(&Pico_param.motor_param,&tmp_pico_param);
  	
   sprintf(question,"Last modification %s\n",Pico_param.motor_param.config_motor_time);
   strcat(question,"Motor manufacturer %s \n  Motor Model %s \n Controller Model \n"
   "%R Microcontroller %r PCI PI %r ISA PI %r LPS made"
   "Do you want to change these settings? %R YES %r NO");
   win_scanf(question,
   &Pico_micro_param.motor_manufacturer,//
   &Pico_micro_param.motor_model,//
   &Pico_micro_param.motor_controller,
   &change_button);//
   
   ///IL FAUT FINIR LA LISTE
   
  
  
  if (change_button == 0 && i != CANCEL)  
  {
      change_button = 1;
      win_scanf("Are you really sure you want to change the settings \n This may have unwanted effects \n %R YES %r NO",&change_button);
        if (change_button == 0 && i != CANCEL)  
          {
            duplicate_pico_Motor_param(&tmp_pico_param,&Pico_param.motor_param);
            Pico_param.motor_param.config_piezzo_time = ctime(time());  
          	
          	write_Pico_config_file();
  	      }
  } 	
  
  return D_O_K;   
} 

# endif /* _MICROSCOPE_C_ */

