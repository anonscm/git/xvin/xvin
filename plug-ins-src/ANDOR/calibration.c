
#ifndef _CALIBRATION_C_
#define _CALIBRATION_C_

# include "allegro.h"
# include "winalleg.h"

# include "xvin.h"
# include "fftl32n.h"
# include "fillib.h"





/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "../cfg_file/Pico_cfg.h"
#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "focus.h"
# include "magnetscontrol.h"
# include "trackBead.h"
# include "brown_util.h"
# include "track_util.h"
# include "calibration.h"
# include "action.h"

int n_calib = 0;


int calibration_oi_idle_action(struct  one_image *oi, DIALOG *d)
{
  // we put in this routine all the screen display stuff
  imreg *imr;
  BITMAP *imb;

  if (d->dp == NULL)    return 1;
  imr = (imreg*)d->dp;        /* the image region is here */
  if (imr->one_i == NULL || imr->n_oi == 0)        return 1;

  if (oi->need_to_refresh & BITMAP_NEED_REFRESH)
    {
      display_image_stuff_16M(imr,d);
      oi->need_to_refresh |= INTERNAL_BITMAP_NEED_REFRESH;
    }



  imb = (BITMAP*)oi->bmp.stuff;

  if (oi->need_to_refresh & INTERNAL_BITMAP_NEED_REFRESH)
    {
      //screen_used = 1;
      SET_BITMAP_IN_USE(oi);
      acquire_bitmap(screen);
      blit(imb,screen,0,0,imr->x_off + d->x, imr->y_off - 
	   imb->h + d->y, imb->w, imb->h); 
      release_bitmap(screen);
      //screen_used = 0;
      oi->need_to_refresh &= ~INTERNAL_BITMAP_NEED_REFRESH;
      SET_BITMAP_NO_MORE_IN_USE(oi);
    }
  return 0;
}

int calibration_image_job(int im, struct future_job *job)
{
  register int i;
  int onx, ony, i_line, ci;
  union pix  *pd;
  b_track *bt = NULL;
  calib_param *sp = NULL; 
  char buf[1024];

  if (job == NULL || job->oi == NULL || job->in_progress == 0) return 0;
  if (im < job->imi)  return 0;

  ci = track_info->c_i;
  bt = track_info->bd[job->bead_nb];
  sp = (calib_param *)job->more_data;
  onx = job->oi->im.nx;   ony = job->oi->im.ny;
  i_line = job->local;
  pd = job->oi->im.pixel;




  while (track_info->imi[ci] >= sp->im[i_line])
    {
      ci--;
      if (ci < 0) ci += TRACKING_BUFFER_SIZE;
    }


  for (i = 0; i < onx; i++)
	pd[i_line].fl[i] = 0; 


  while (track_info->imi[ci] <= job->imi && i_line < ony)
    {
      if ((track_info->imi[ci] > (sp->im[i_line] + sp->dead))
	  && (track_info->imi[ci] < sp->im[i_line+1]))
	{
	  for (i = 0; i < onx; i++)
	    pd[i_line].fl[i] += bt->rad_prof[ci][i]; 
	}
      else if (track_info->imi[ci] >= sp->im[i_line+1])
	{

	  for (i = 0; i < onx; i++)
	    pd[i_line].fl[i] /= (sp->nper - sp->dead - 1);
	  sprintf(buf,"calibration %d %d/%d Zobj %6.3f Zmag %6.3f mm ",n_calib,job->local,ony,sp->zo[i_line],sp->zmag_cal);
	  job->local++;
	  set_window_title(buf);
	}
      ci++;
      if (ci >= TRACKING_BUFFER_SIZE) ci -= TRACKING_BUFFER_SIZE;
    }

  job->oi->need_to_refresh |= BITMAP_NEED_REFRESH;
  if (job->local < ony) job->imi = sp->im[job->local+1];
  else
    {
      job->in_progress = 0; 
      bt->calib_im = job->oi;
      bt->calib_im_fil = image_band_pass_in_real_out_complex(NULL, job->oi, 0, bt->bp_center, bt->bp_width, bt->rc);
      if (bt->calib_im_fil == NULL)	return win_printf_OK("Can't compute calibration image");
      n_calib++;
    }
  return 0;
}

int adj_calibration_image_job(int im, struct future_job *job)
{
  int onx, ony, nmean, ci;
  b_track *bt = NULL;
  calib_param *sp = NULL; 
  char buf[1024];
  float zmean = 0;

  if (job == NULL || job->oi == NULL || job->in_progress == 0) return 0;
  if (im < job->imi)  return 0;

  ci = track_info->c_i;
  bt = track_info->bd[job->bead_nb];
  sp = (calib_param *)job->more_data;
  onx = job->oi->im.nx;   ony = job->oi->im.ny;

  while (track_info->imi[ci] >= job->in_progress)
    {
      ci--;
      if (ci < 0) ci += TRACKING_BUFFER_SIZE;
    }
  nmean = 0;
  while (track_info->imi[ci] <= job->imi)
    {
      zmean += bt->z[ci];
      nmean++;
      ci++;
      if (ci >= TRACKING_BUFFER_SIZE) ci -= TRACKING_BUFFER_SIZE;
    }
  if (nmean) zmean /= nmean;
  zmean = -zmean;
  sprintf(buf,"calibration adjustment %6.3f microns ",zmean);
  set_window_title(buf);

  create_attach_select_y_un_to_oi(job->oi, IS_METER, job->oi->ay - zmean, sp->zobj_step, -6, 0,"\\mu m");		

  create_attach_select_y_un_to_oi(bt->calib_im_fil, IS_METER, bt->calib_im_fil->ay - zmean, sp->zobj_step, -6, 0,"\\mu m");		


  job->in_progress = 0; 
  win_title_used = 0;
  return 0;
}


int record_calibration(void)
{
  int i, j, im, k;
  float obj_step = 0.3, tmp;
  imreg *imr;
  O_i *oi, *oic = NULL;
  b_track *bt = NULL;
  DIALOG *d;
  static int nf, nstep = 32,  dead = 2, nper = 12, n_zerof = 128, zero_dead = 10;
  static float zobj_start, zobj_step = 0.3, zmag_cal = 9, zmag_ground = 4;
  static float zmag_after = 8, rot;
  static float z_cor = 0.878;
  calib_param *sp;
  char question[1024];

   if(updating_menu_state != 0)	
     {
       if (track_info->n_b == 0) 
	 active_menu->flags |=  D_DISABLED;
       else
	 {
	   for (i = 0, k = 0; i < track_info->n_b; i++)
	     {
	       bt = track_info->bd[i];
	       k += (bt->calib_im == NULL) ? 1 : 0;
	     }
	   if (k) active_menu->flags &= ~D_DISABLED;	
	   else active_menu->flags |=  D_DISABLED;
	 }
       return D_O_K;
     }
   if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)	return OFF;
  
  zobj_start = tmp = read_Z_value_accurate_in_micron();

   rot = what_is_present_rot_value();
   zmag_cal = read_magnet_z_value();
   sprintf(question,"{\\pt14 Recording calibration images}\n"
	   "Z_{mag} present val %g angular position %g\n" 
	   "starting objective at %%8f with step of %%8f nb of step %%8d\n"
	   "complete period including dead %%8d dead period(ms) %%8d\n" 
	   "Z_{mag} for calibration %%8f\n" 
	   "Z_{mag} for F = 0 %%8f settling time %%8d recoding during %%8d\n" 
	   "zmag finish %%8f z correction factor%%8f\n"
	   ,rot,zmag_cal);

   i = win_scanf(question,&zobj_start,&zobj_step,&nstep,&nper,&dead,
		 &zmag_cal,&zmag_ground,&zero_dead,&n_zerof,&zmag_after,&z_cor);	

   if (i == CANCEL)	return OFF; 

   sp = (calib_param*)calloc(1,sizeof(calib_param));
   if (sp == NULL)  win_printf_OK("Could no allocte hat parameters!");
   sp->im = (int*)calloc(nstep+1,sizeof(int));
   sp->zo = (float*)calloc(nstep+1,sizeof(float));
   if (sp->im == NULL || sp->zo == NULL)  win_printf_OK("Could no allocte hat parameters!");


   sp->zobj_start = zobj_start;
   sp->zobj_step = zobj_step;
   sp->nstep = nstep;
   sp->nper = nper;
   sp->dead = dead;
   sp->n_zerof = n_zerof;
   sp->zero_dead = zero_dead;
   sp->zmag_cal = zmag_cal;
   sp->zmag_ground = zmag_ground;
   sp->zmag_after = zmag_after;

  
  im = track_info->imi[track_info->c_i] +10;                                 

  k = fill_next_available_action(im+4, MV_ZMAG_ABS, sp->zmag_cal);
  if (k < 0)	win_printf_OK("Could not add pending action!");
  
  im += 64;



   for (i = 0, nf = 0; i < nstep ; i++)
     {
       sp->zo[i] = sp->zobj_start + (i * sp->zobj_step);
       sp->im[i] = im + nf;
       nf += nper;
     }
   sp->zo[i] = sp->zobj_start + (nstep/3) * sp->zobj_step - Pico_param.dna_molecule.dsDNA_molecule_extension;
   sp->im[i] = im + nf;



  for (i = 0, k = 0; i < track_info->n_b; i++)
    {
      bt = track_info->bd[i];
      if ((bt->calib_im == NULL) && (bt->not_lost > 0) && (bt->in_image > 0))
	{
	  oic = create_and_attach_oi_to_imr(imr, track_info->cl, 32, IS_FLOAT_IMAGE);
	  if (oic == NULL)	return win_printf_OK("cannot create calibration image !");
	  oic->filename = my_sprintf(oic->filename, "Calib%04dbd%01d.gr",n_calib,i);
	  
	  create_attach_select_y_un_to_oi(oic, IS_METER, tmp, obj_step, -6, 0,"\\mu m");
	  create_attach_select_x_un_to_oi(oic, IS_METER, -(oi->im.nx/2)*oi->dy, 
	  oi->dy, -6, 0,"\\mu m");
	  //create_attach_select_x_un_to_oi(oic, IS_METER, -(oidp->im.nx/2)*oi->dy/ay, 
	  //oi->dy/ay, -6, 0,"\\mu m");
	  
	  
	  
	  set_zmin_zmax_values(oic, oi->z_min, oi->z_max);
	  set_z_black_z_white_values(oic, oi->z_black, oi->z_white);
	  
	  oic->oi_idle_action = calibration_oi_idle_action;
	  fill_next_available_job_spot(im, sp->im[1], GRAB_CALIBRATION_IMAGE, i, imr, oic, NULL, NULL, (void*)sp, calibration_image_job,0);


	  fill_next_available_job_spot(im+nf+32+sp->zero_dead, im+nf+32+sp->zero_dead+n_zerof, GRAB_CALIBRATION_IMAGE, i, imr, oic, NULL, NULL, (void*)sp, adj_calibration_image_job, 0);


	  k++;
	}
    }
  
  for (i = 0; i <= nstep; i++)
    {
      j = fill_next_available_action(sp->im[i], MV_OBJ_ABS, sp->zo[i]);
      if (j < 0)	win_printf_OK("Could not add pending action!");
    }

  d = find_dialog_associated_to_imr(imr, NULL);
  write_and_blit_im_box( plt_buffer, d, imr);



  k = fill_next_available_action(im+nf+32, MV_ZMAG_ABS, sp->zmag_ground);
  if (k < 0)	win_printf_OK("Could not add pending action!");


  j = fill_next_available_action(im+nf+52+sp->zero_dead+n_zerof, MV_OBJ_ABS, sp->zo[3]);
  if (j < 0)	win_printf_OK("Could not add pending action!");

  k = fill_next_available_action(im+nf+64+sp->zero_dead+n_zerof, MV_ZMAG_ABS, sp->zmag_after);
  if (k < 0)	win_printf_OK("Could not add pending action!");
  win_title_used = 1;
  return D_O_K;
}



# endif
