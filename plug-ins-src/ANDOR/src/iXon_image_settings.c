/** \file iXon_image_settings.c
    \brief Plug-in program for iXon image settings control in Xvin.
    This is the a program that controls the iXon camera image settings.
    \author JF Allemand & A Meglio
*/
#ifndef _IXON_IMAGE_SETTINGS_C_
#define _IXON_IMAGE_SETTINGS_C_

#include "allegro.h"
#include "winalleg.h"
#include <windowsx.h>
#include "ctype.h"
#include "xvin.h"
#include "ATMCD32D.h" 
#include <im_oi.h>
#include "global_define.h"
#include "global_variables.h"
#include "global_functions.h"

//#include "ifcapi.h"
//#include "track_ifc.h"


#define BUILDING_PLUGINS_DLL
#include "iXon_image_settings.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Image size functions

/** Function to update the unit sets when they might have been modified.

Changes the unit sets according to the values stored in iXon.
\param O_i* oi The oi.
\return int Errot code.
\author Adrien Meglio
@version 10/05/06
*/
int update_unit_sets(O_i* src_oi)
{
    if(updating_menu_state != 0) return D_O_K;
        
    //win_report("Unit sets update");
    
    if (src_oi == NULL) return D_O_K;
    
    /* Sets the unit sets */
    if (src_oi->n_xu == 1 || src_oi->n_yu == 1)
    {
        create_and_attach_unit_set_to_oi(src_oi,IS_METER,0,MICRONS_PER_PIXEL*iXon.bin_x,-6,0,"\\mu m",IS_X_UNIT_SET); 
        set_oi_x_unit_set(src_oi,src_oi->n_xu-1);
        create_and_attach_unit_set_to_oi(src_oi,IS_METER,0,MICRONS_PER_PIXEL*iXon.bin_y,-6,0,"\\mu m",IS_Y_UNIT_SET);
        set_oi_y_unit_set(src_oi,src_oi->n_yu-1);
    }    
    else 
    {
        set_unit_increment(src_oi->yu[src_oi->n_yu-1],MICRONS_PER_PIXEL*iXon.bin_y); 
        set_unit_increment(src_oi->xu[src_oi->n_xu-1],MICRONS_PER_PIXEL*iXon.bin_x);    
    }  
    
    //win_report("Space unit sets done");
        
    if (src_oi->n_tu == 1) 
        {
            create_and_attach_unit_set_to_oi(src_oi,IS_SECOND,0,iXon.exposure_time,-3,0,"ms",IS_T_UNIT_SET); 
            set_oi_t_unit_set(src_oi,src_oi->n_tu-1);
        }
    else set_unit_increment(src_oi->tu[src_oi->n_tu-1],iXon.exposure_time);
    
    //win_report("Time unit sets done");
    /* */
    
    return D_O_K;
}

/** Clean function to set image size and binning

Sets image size and binning, changes the global iXon variables if successful and recalculates
image width, height and total pixels number
\param Same as SetImage.
\return unsigned int SetImage error code.
\author Adrien Meglio
*/
int set_image_size_on_iXon(int bin_x,int bin_y,int subregion_x1,int subregion_x2,int subregion_y1,int subregion_y2)
{
    unsigned int error_code; // The iXon-type error code returned by SetImage
    
    if(updating_menu_state != 0) return D_O_K;
    
    //win_report("<<set image size on ixon>>");
    
    /* Freezes the display */
    Stop_acquisition();     
    
    //win_report("<<set image size on ixon>> : stop acquisition passed");
    
    error_code = SetImage(bin_x,bin_y,subregion_x1,subregion_x2,subregion_y1,subregion_y2); 
    /* Changes the image parameters to the requested value
    NOTE : no error correction (like : is subregion_x1 < subregion_x2 ?) is performed at this point
    BEWARE : image begins at 1 and NOT at 0 */
    
    //win_report("<<set image size on ixon>> : SetImage passed");
    
    if (error_code != DRV_SUCCESS) // If the image settings have not been successfully changed
    {
        error_report(error_code);
        return error_code;
    }
    
    /* The global variable iXon is refrshed */
    iXon.bin_x = bin_x;
    iXon.bin_y = bin_y;
    iXon.subregion_x1 = subregion_x1;
    iXon.subregion_x2 = subregion_x2;
    iXon.subregion_y1 = subregion_y1;
    iXon.subregion_y2 = subregion_y2;
        
    iXon.total_pixels_number = (iXon.subregion_x2 - iXon.subregion_x1 + 1)*(iXon.subregion_y2 - iXon.subregion_y1 + 1)/(iXon.bin_x*iXon.bin_y);
    iXon.total_width = (iXon.subregion_x2 - iXon.subregion_x1 + 1)/iXon.bin_x;
    iXon.total_height = (iXon.subregion_y2 - iXon.subregion_y1 + 1)/iXon.bin_y;
    /* */
    
    //win_report("<<set image size on ixon>> : iXon refresh passed");
    
    /* Timings will change when image size is changed */
    GetAcquisitionTimings(&iXon.exposure_time,&iXon.accumulation_time,&iXon.kinetic_time);
    
    //win_report("<<set image size on ixon>> : GetAcquisitionTimings passed");
    
    /* Unit sets update */
    update_unit_sets(oi_iXon);
       
    return error_code;
}

////////////////////////////////////////////////////////////////////////////////

/** Efficient way to set the binning using a single argument.

\param int bin Binning coded as a 2-digit hexadecimal number
\return int Error code.
\author Adrien Meglio
\version 09/05/06
*/
int set_binning(int bin)
{   
    if(updating_menu_state != 0) return D_O_K;
    
    // Freezes the display
    Stop_acquisition();
    
    /* Binning may span from 1 to 16 on each direction. So we may code it as a 2-digit hexadecimal number
    The first digit ("left" is x-binning and the second ("right") is y-binning */
    iXon.bin_x = (bin&0x0F)+1; 
    // Hexadecimal way to retrieve the left digit. Since F is 15 and 0 binning is useless, we add 1 to span the 1-16 range
    iXon.bin_y = (bin>>4)+1;
    // Hexadecimal way to retrieve the right digit. Since F is 15 and 0 binning is useless, we add 1 to span the 1-16 range
    
    //win_report("You requested %d x %d binning",iXon.bin_x,iXon.bin_y); 
    
    /* Prints into the log file */
    win_event("Binning changed for : %d x %d",iXon.bin_x,iXon.bin_y);
    
    /* Reinitializes the display and camera settings (uses the new values of iXon.bin_x and bin_y) */
    //set_image_for_live();
    Set_iXon_parameters();
    
    /* Back to live */
    iXon_real_time_imaging();
    
    return D_O_K;
}

////////////////////////////////////////////////////////////////////////////////

/** A zoom-out function.

Back to maximum image size.
\param None.
\return int Error code.
\author JF Allemand
@version 10/05/06
*/    
int zoom_out_iXon(void)
{
    if(updating_menu_state != 0) return D_O_K;
    
    Stop_acquisition();
       
    iXon.bin_x = 1;
    iXon.bin_y = 1;
    iXon.subregion_x1 = 1;
    iXon.subregion_x2 = iXon.pixels_x;
    iXon.subregion_y1 = 1;
    iXon.subregion_y2 = iXon.pixels_y;
    
    /* Prints into the log file */
    win_event("Zoomed out and set binning to 1x1");
    
    /* Reinitializes the display and camera settings (uses the new values in iXon) */
    //set_image_for_live();
    Set_iXon_parameters();

    /* Back to live */
    iXon_real_time_imaging();
    
    return D_O_K;
}    

////////////////////////////////////////////////////////////////////////////////

/** A zoom-in function.

Defines a region of interest in a given image and resets image parameters to this new bounding box.
\param None.
\return int Error code.
\author JF Allemand
@version 09/05/06
*/
int zoom_in_iXon(void)
{
  int start_x; 
  int start_y;
  int present_x =0;
  int present_y=0;
  int start_mouse_x;
  int start_mouse_y;
  int dx;
  int dy;
  int ldx;
  int  ldy;
  int color;
  BITMAP* imb=NULL;
  
  if(updating_menu_state != 0) return D_O_K;
    
  if (imr_iXon == NULL)		return win_report("iXon oi NULL");
	
  color = makecol(255, 64, 64); // Bright red	
		
  Stop_acquisition();
	
  /* Wait for mouse selection or escape */
  while (!( mouse_b & 1 ) || key[KEY_ESC])
    {
      Sleep(100); /* So as not to overuse the processor */
    }
	
  /* Escape */
  if (key[KEY_ESC])	
    {
      clear_keybuf();
      return D_REDRAWME;	//REFRESH
    }

  /* Copies the position of the mouse at the beginning of the rectangle */
  start_mouse_x = mouse_x;
  start_mouse_y = mouse_y;		
  /* Converts that position from screen in pixels units */
  start_x = (int) x_imr_2_imdata(imr_iXon, start_mouse_x);
  start_y = (int) y_imr_2_imdata(imr_iXon, start_mouse_y);

  ldx = 0;
  ldy = 0;
  while (mouse_b & 1)
    {
      if (key[KEY_ESC])	
	{
	  clear_keybuf();
	  return D_REDRAWME;	//REFRESH
	}

      dx = mouse_x - start_mouse_x;
      dy = mouse_y - start_mouse_y;
		
      if  (ldy != dy || ldx != dx)
	{
	  imb = (BITMAP*)oi_iXon->bmp.stuff;
	  blit(imb,screen,0,0,imr_iXon->x_off + d_iXon->x, imr_iXon->y_off - imb->h + d_iXon->y, imb->w, imb->h); 
	  rect(screen,start_mouse_x,start_mouse_y,start_mouse_x + dx,start_mouse_y + dy,color);		
	    
	  present_x = (int) x_imr_2_imdata(imr_iXon, mouse_x);
	  present_y = (int) y_imr_2_imdata(imr_iXon, mouse_y);	
	}
      else 
	{
	  Sleep(100); /* Avoids using too much processor time */
	}
      
      ldx = dx;
      ldy = dy;
    }
		
  //win_report("start_x %d \n start_y %d \n present_x %d \n present_y %d",start_x,start_y,present_x,present_y);
        
  /* Ensures a correct orientation of the subregion */
  iXon.subregion_x1 = (start_x < present_x) ? start_x : present_x;
  iXon.subregion_x2 = (start_x > present_x) ? start_x : present_x;
  iXon.subregion_y1 = (start_y < present_y) ? start_y : present_y;
  iXon.subregion_y2 = (start_y > present_y) ? start_y : present_y;
    
  /* Prints into the log file */
  win_event("Zoomed to region x : %d - %d & y : %d - %d",iXon.subregion_x1,iXon.subregion_x2,iXon.subregion_y1,iXon.subregion_y2);
    
  /* Reinitializes the display and camera settings (uses the new values in iXon) */
  //set_image_for_live();
  Set_iXon_parameters();

  /* Back to live */
  iXon_real_time_imaging();
    
  return D_O_K;
}

////////////////////////////////////////////////////////////////////////////////

/** Allows the user to set the iXon EMCCD gain.

\param None.
\return int Error code.
\author Adrien Meglio
@version 10/05/06
*/ 
int change_gain(void)
{
  int current_gain;
    int iXon_gain_target = iXon.gain;
    
    if(updating_menu_state != 0) return D_O_K;
           
    /* Freezes the display */
    Stop_acquisition();
              
    /* Retrieves the allowed gain range */   
    if (GetEMGainRange(&iXon.gain_min,&iXon.gain_max) != DRV_SUCCESS) return win_report("Could not retrieve current gain range !");
    
    /* Retrieves the current gain */
    if (GetEMCCDGain(&current_gain) != DRV_SUCCESS) return win_report("Could not retrive current gain !");
    
    /* Displays this information */
    win_report("Current gain is %d \nRange spans from %d to %d",current_gain,iXon.gain_min,iXon.gain_max);  
    if (win_scanf("Gain target value : %d",&iXon_gain_target) == CANCEL) return D_O_K;;
    
    if (iXon_gain_target < iXon.gain_min) iXon_gain_target = iXon.gain_min;
    else if (iXon_gain_target > iXon.gain_max) iXon_gain_target = iXon.gain_max;
    
    /* Check compatibility of requested value. Not necessary since SetEMCCDGain would simply return an error */
    //if (iXon_gain_target > iXon.gain_max || iXon_gain_target < iXon.gain_min) return win_report("Gain out of range %d:%d !",iXon.gain_min,iXon.gain_max);
    
    /* Tries to set gain to the specified value */
    if (SetEMCCDGain(iXon_gain_target) != DRV_SUCCESS) return win_report("Could not set gain to specified value !");
    else iXon.gain = iXon_gain_target; // Refreshes the global variable iXon if successful
          
    /* Prints into the log file */
    win_event("Changed gain to : %d",iXon.gain);
          
    /* Back to live */
    iXon_real_time_imaging();
          
    return D_O_K;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Image acquisition functions

/** Allows the user to set the display refreshing period.

\param None.
\return int Error code.
\author Adrien Meglio
@version 09/05/06
*/
int change_display_refreshing_period(void)
{    
    if(updating_menu_state != 0) return D_O_K;
    
    win_scanf("Refresh display every %d frame",&iXon.refresh_period);
    
    return D_O_K;
}

/** Automatically sets the refreshing frequency.

Automatically adjusts the refreshing frequency as close as possible to that defined in iXon.screen_frequency

\param None.
\return int Error code.
\author Adrien Meglio
@version 13/11/06
*/
int set_screen_refresh_freq(void)
{    
    if(updating_menu_state != 0) return D_O_K;
    
    switch (iXon.exposure_time*iXon.screen_frequency > 1)
    {
        case 1 : /* If slower than the screen */
        iXon.refresh_period = 1 ; /* Max speed */
        break ;
        
        case 0 : /* If faster than speed */
        iXon.refresh_period = (int) 1/(iXon.exposure_time*iXon.screen_frequency) ; /* Display one in every NNN frames */
        break ;
    }
    
    return D_O_K;
}

/** Allows the user to set the iXon acquisition time.

\param None.
\return int Error code.
\author JF Allemand
@version 10/05/06
*/ 
int set_aq_time(void)
{
    if(updating_menu_state != 0) return D_O_K;
    
    /* Freezes the display */
    Stop_acquisition(); 
     
    win_scanf("Integration time %f",&iXon.exposure_time);
    
    SetExposureTime(iXon.exposure_time); /* Asks iXon to set exposure time as close as possible 
     to the requested value */
    
    GetAcquisitionTimings(&iXon.exposure_time,&iXon.accumulation_time,&iXon.kinetic_time); /*Retrieves the actual timings */
    
    /* Prints into the log file */
    win_event("Changed acquision time to %f ms/frame",1000*iXon.exposure_time);
    
    /* Gets the ACTUAL exposure time (and other timings) matching the requested value */
    //set_image_for_live();
    Set_iXon_parameters();
    
    /* Back to live */
    iXon_real_time_imaging();
                
    return D_O_K;
}

////////////////////////////////////////////////////////////////////////////////

/** Sets the vertical CCD speed to the maximum possible.

\param none
\return int Error code.
\author Adrien Meglio
\version 09/05/06
*/
int init_VSSpeed_to_max(void)
{   
    int index;
    float speed;
    
    if(updating_menu_state != 0) return D_O_K;
    
    GetFastestRecommendedVSSpeed(&index,&speed);
    SetVSSpeed(index);
    
    iXon.VSSpeed = speed;
    
    return D_O_K;
}

////////////////////////////////////////////////////////////////////////////////

/** Sets the horizontal CCD speed to the maximum possible.

\param none
\return int Error code.
\author Adrien Meglio
@version 09/05/06
*/
int init_HSSpeed(void)
{
    float speed;
    
    if(updating_menu_state != 0) return D_O_K;
    
    SetHSSpeed(iXon.is_electron_multiplication,0);
    
    GetHSSpeed(iXon.AD_channel,iXon.is_electron_multiplication,0,&speed);
    
    iXon.HSSpeed = speed;
    
    return D_O_K;
} 

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
// Base functions
      
/** Retrieves the \a imr (typically to get the film in the \a oi) in the current display */      
imreg* get_imr(void)
{
    if(updating_menu_state != 0) return D_O_K;
    
    imreg *imr = NULL;
    
    /* Finds the set of frames (i.e; the film) as loaded by the sif2gr function in the current window */
    imr = find_imr_in_current_dialog(NULL);
    
    return imr;
} 

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// General camera setting functions

/** Proceeds to the camera parameters modification.

\input none
\return Error code.
\author Adrien Meglio
@version 09/05/06
*/
int Set_iXon_parameters(void)
{
    int VSSpeed_index;
        
    if(updating_menu_state != 0) return D_O_K;
 
    if (SetAcquisitionMode(iXon.aquisition_mode) != DRV_SUCCESS) return win_report("Failed in setting acquisition mode");
    
    if (SetReadMode(iXon.image_mode) != DRV_SUCCESS) return win_report("Failed in setting image mode");

    if (GetDetector(&iXon.pixels_x,&iXon.pixels_y) != DRV_SUCCESS) return win_report("Could not read detector size !");

    if (set_image_size_on_iXon(iXon.bin_x,iXon.bin_y,iXon.subregion_x1,iXon.subregion_x2,iXon.subregion_y1,iXon.subregion_y2) != DRV_SUCCESS) return win_report("Failed in setting image size");//modifier par la suite

    allocate_live_buffer(iXon.total_width,iXon.total_height,iXon.image_type); // These width and height are determined by set_image_size_on_iXon */

    if (SetFrameTransferMode(iXon.transfer_mode)!= DRV_SUCCESS) return win_report("Failed in setting Transfer mode");

    if (GetEMGainRange(&iXon.gain_min,&iXon.gain_max) != DRV_SUCCESS) return win_report("Could not retrieve current gain range !");
    iXon.gain = (iXon.gain > iXon.gain_max) ? iXon.gain_max : iXon.gain;
    if (SetEMCCDGain(iXon.gain) != DRV_SUCCESS) return win_report("Failed in setting gain !");
    
    if (GetSizeOfCircularBuffer(&(iXon.size_circular_buffer)) != DRV_SUCCESS) return win_report("Failed in getting circular buffer size");
    
    if (SetHSSpeed(iXon.is_electron_multiplication,0) != DRV_SUCCESS) return win_report("Failed in setting HSSpeed !"); // Sets to max
    GetHSSpeed(iXon.AD_channel,iXon.is_electron_multiplication,0,&iXon.HSSpeed); // Retrieves HSSpeed value
    
    GetFastestRecommendedVSSpeed(&VSSpeed_index,&iXon.VSSpeed); // Retrieves max speed index and value
    if (SetVSSpeed(VSSpeed_index) != DRV_SUCCESS) return win_report("Failed in setting VSSpeed !"); // Sets to max

    if (SetExposureTime(iXon.exposure_time) != DRV_SUCCESS) return win_report("Failed in setting Exposure time");

    if (SetTriggerMode(iXon.trigger_mode) != DRV_SUCCESS) return win_report("Failed in setting Trigger mode");

    if (GetAcquisitionTimings(&iXon.exposure_time,&iXon.accumulation_time,&iXon.kinetic_time) != DRV_SUCCESS) return win_report("Failed in getting timings");  

    set_screen_refresh_freq(); // Adapts refreshing frequency to a predefined standard

    PrepareAcquisition();
    
    imr_iXon->screen_scale = PIXEL_1;
    set_oi_horizontal_extend(oi_iXon,1.0);//(float)iXon.total_width/512);
    set_oi_vertical_extend(oi_iXon,1.0);//(float)iXon.total_height/512);
    /* This makes the display size independent of iXon.total_width and total_height, 
    whatever the binning or the zoom level. If you prefer to take this into account replace 
    1.0 by (float)iXon.total_width/512) and (float)iXon.total_height/512) respectively. */
    oi_iXon->need_to_refresh |= ALL_NEED_REFRESH;
    broadcast_dialog_message(MSG_DRAW,0);

    //win_report("iXon parameters setting done");
    
    return D_O_K;
}   

////////////////////////////////////////////////////////////////////////////////

/** Completely resets the image parameters for a new live display.

\param none
\return int Error code.
\author JF Allemand
@version 09/05/06
*/
//int set_image_for_live(void)
//{
//    if(updating_menu_state != 0) return D_O_K;
    
//    iXon.aquisition_mode = RUN_TILL_ABORT;

    /* Useless */    
    //Stop_acquisition(); // Useless ?
    
    //win_report("Now in set-image-for-live");
    
    /* Setting image size 
       if (set_image_size_on_iXon(iXon.bin_x,iXon.bin_y,iXon.subregion_x1,iXon.subregion_x2,iXon.subregion_y1,iXon.subregion_y2) != DRV_SUCCESS)
       {
       return win_report("Failed in setting image size");
       }
     */
//    Set_iXon_parameters();

    //win_report("Image size setting done");
    
    /* Already done in Set_iXon_parameters */
    //allocate_live_buffer(iXon.total_width,iXon.total_height,iXon.image_type);
    
    //win_report("Buffer allocated");
    
    /* Already done in Set_iXon_parameters */
    //PrepareAcquisition(); 
    
    //win_report("Acquisition prepared");
          
    /* Already done in Set_iXon_parameters */
    //imr_iXon->screen_scale = PIXEL_1;
    //set_oi_horizontal_extend(oi_iXon,1.0);//(float)iXon.total_width/512);
    //set_oi_vertical_extend(oi_iXon,1.0);//(float)iXon.total_height/512);
    ///* This makes the display size independent of iXon.total_width and total_height, 
    //whatever the binning or the zoom level. If you prefer to take this into account replace 
    //1.0 by (float)iXon.total_width/512) and (float)iXon.total_height/512) respectively. */
    //oi_iXon->need_to_refresh |= ALL_NEED_REFRESH;
    //broadcast_dialog_message(MSG_DRAW,0);
    
//    iXon.is_live = LIVE;
    
//    return D_O_K;
//}

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Housekeeping functions

/** Main function of \a ixon.c . 

This function loads the menus. 
\param none
\return Error code.
\author Adrien Meglio
\version 09/05/06
*/
int	ixon_image_settings_main(void)
{
    return D_O_K;
}

#endif
