#ifndef _BROWN_UTIL_H_
#define _BROWN_UTIL_H_


# define 	WRONG_ARGUMENT		-1
# define 	OUT_MEMORY			-2
# define 	FFT_NOT_SUPPORTED	-15
# define	PB_WITH_EXTREMUM	2
# define	TOO_MUCH_NOISE		4
# define	GETTING_REF_PROFILE	1

# define        clip_X_cross_in_image(oi, x, y, l2, w2) \
		(x) = ((x) < (oi)->im.nx - (l2) - (w2)) ? (x) : (oi)->im.nx - (l2) - (w2);\
		(y) = ((y) < (oi)->im.ny - (l2) - (w2)) ? (y) : (oi)->im.ny - (l2) - (w2);\
		(x) = ((x) < (l2)+(w2)) ? (l2)+(w2) : (x);\
		(y) = ((y) < (l2)+(w2)) ? (l2)+(w2) : (y)

# define        clip_cross_in_image(oi, x, y, l2) \
		(x) = ((x) < (oi)->im.nx - (l2)) ? (x) : (oi)->im.nx - (l2);\
		(y) = ((y) < (oi)->im.ny - (l2)) ? (y) : (oi)->im.ny - (l2);\
		(x) = ((x) < (l2)) ? (l2) : (x);\
		(y) = ((y) < (l2)) ? (l2) : (y)


# define        is_cross_in_image(oi, x, y) \
		(((x) < (oi)->im.nx) && ((y) < (oi)->im.ny)\
                 && ((x) > 0) && ((y) > 0)) ? 1 : 0



# define NB_FRAMES_BEF_LOST 8

PXV_FUNC(int,	fill_avg_profiles_from_im, (O_i *oi, int xc, int yc, int cl, int cw, int *x, int *y));
PXV_FUNC(int,	check_bead_not_lost, (int *x, float *x1, int *y, float *y1, int cl, float bd_mul));
PXV_FUNC(int, erase_around_black_circle, (int *x, int *y, int cl));
PXV_FUNC(int,     deplace, (float *x, int nx));
PXV_FUNC(int,     fftwindow_flat_top1, (int npts, float *x, float smp));
PXV_FUNC(int,     reset_mouse, (void));
PXV_FUNC(int,	find_max1, (float *x, int nx, float *Max_pos, float *Max_val));

PXV_FUNC(int,	correlate_1d_sig_and_invert, (float *x1, int nx, int window, float *fx1, int filter, int remove_dc));

PXV_FUNC(float,	find_distance_from_center, (float *x1, int cl, int flag, int filter, int black_circle, float *corr));
PXV_FUNC(int,	draw_cross_in_screen_unit, (int xc, int yc, int length, int width, int color, BITMAP* bmp, int sc));

PXV_FUNC(int, fft_band_pass_in_real_out_complex, (float *x, int nx, int w_flag, int fc, int width));
PXV_FUNC(float, find_phase_shift_between_profile, (float *zr, float *zp, int npc, int flag, int filter, int width, int rc));

PXV_FUNC(O_i*, image_band_pass_in_real_out_complex, (O_i *dst, O_i *oi,int w_flag, int filter, int width, int rc));
PXV_FUNC(int, find_z_profile_by_mean_square_and_phase, (float *zp, int nx, O_i *oir, int w_flag, int filter, int width, int rc, float *z));


PXV_FUNC(float*, radial_non_pixel_square_image_sym_profile_in_array, (float *rz, O_i *ois, float xc, float yc,  int r_max, float ax, float ay));

PXV_FUNC(float*, radial_non_pixel_square_image_sym_profile_in_array_sse, (float *rz, O_i *ois, float xc, float yc,  int r_max, float ax, float ay));


PXV_FUNC(int, change_mouse_to_cross,(int cl, int cw));
PXV_FUNC(int, reset_mouse, (void));


# endif
