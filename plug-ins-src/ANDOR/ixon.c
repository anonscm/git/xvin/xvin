/** \file ixon.c
    \brief Plug-in program for iXon control in Xvin.
    This is the master program that controls the iXon camera.
    \sa movie_statistics.c
    \author JF Allemand & A Meglio
*/
#ifndef _IXON_C_ 
#define _IXON_C_

#include "allegro.h"
#include "winalleg.h"
#include <windowsx.h>
#include "ctype.h"
#include "xvin.h"
#include "ATMCD32D.h"

#include "global_define.h"
#include "global_variables.h"
#include "global_functions.h"

#define BUILDING_PLUGINS_DLL
#include "iXon.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Error function

/** Broadcasts the meaning of the iXon error message.

\param unsigned int error_code The error code.
\return The input error code.
\author Adrien Meglio
\version 09/05/06
*/
int error_report(unsigned int error_code)
{
  char error_message[768];
  char error_specific[256];
  char error_generic_begin[256];
  char error_generic_end[256];
    
  if(updating_menu_state != 0) return D_O_K;
    
  strcpy(error_generic_begin,"Error : ");
  strcpy(error_generic_end," !");

  switch (error_code)
    {
    case DRV_ERROR_CODES : strcpy(error_specific,"DRV-ERROR-CODES (General Error Code)"); break ; // 20001
    case DRV_SUCCESS : strcpy(error_specific,"DRV-SUCCESS (Successful)"); break ; // 20002
    case DRV_VXDNOTINSTALLED : strcpy(error_specific,"DRV-VXDNOTINSTALLED"); break ; //	20003
    case DRV_ERROR_SCAN : strcpy(error_specific,"DRV-ERROR-SCAN"); break ; //	20004
    case DRV_ERROR_CHECK_SUM : strcpy(error_specific,"DRV-ERROR-CHECK_SUM"); break ; //	20005
    case DRV_ERROR_FILELOAD : strcpy(error_specific,"DRV-ERROR-FILELOAD"); break ; //	20006
    case DRV_UNKNOWN_FUNCTION : strcpy(error_specific,"DRV-UNKNOWN-FUNCTION"); break ; //	20007
    case DRV_ERROR_VXD_INIT : strcpy(error_specific,"DRV-ERROR-VXD-INIT"); break ; //	20008
    case DRV_ERROR_ADDRESS : strcpy(error_specific,"DRV-ERROR-ADDRESS"); break ; //	20009
    case DRV_ERROR_PAGELOCK : strcpy(error_specific,"DRV-ERROR-PAGELOCK"); break ; //	20010
    case DRV_ERROR_PAGEUNLOCK : strcpy(error_specific,"DRV-ERROR-PAGEUNLOCK	"); break ; //20011
    case DRV_ERROR_BOARDTEST : strcpy(error_specific,"DRV-ERROR-BOARDTEST"); break ; //	20012
    case DRV_ERROR_ACK : strcpy(error_specific,"DRV-ERROR-ACK"); break ; //	20013
    case DRV_ERROR_UP_FIFO : strcpy(error_specific,"DRV-ERROR-UP-FIFO"); break ; //	20014
    case DRV_ERROR_PATTERN : strcpy(error_specific,"DRV-ERROR-PATTERN"); break ; //	20015
    case DRV_ACQUISITION_ERRORS : strcpy(error_specific,"DRV-ACQUISITION-ERRORS"); break ; //	20017
    case DRV_ACQ_BUFFER : strcpy(error_specific,"DRV-ACQ-BUFFER"); break ; //	20018
    case DRV_ACQ_DOWNFIFO_FULL : strcpy(error_specific,"DRV-ACQ-DOWNFIFO-FULL"); break ; //	20019
    case DRV_PROC_UNKONWN_INSTRUCTION : strcpy(error_specific,"DRV-PROC-UNKONWN-INSTRUCTION"); break ; //	20020
    case DRV_ILLEGAL_OP_CODE : strcpy(error_specific,"DRV-ILLEGAL-OP-CODE"); break ; //	20021
    case DRV_KINETIC_TIME_NOT_MET : strcpy(error_specific,"DRV-KINETIC-TIME-NOT-MET"); break ; //	20022
    case DRV_ACCUM_TIME_NOT_MET : strcpy(error_specific,"DRV-ACCUM-TIME-NOT-MET"); break ; //	20023
    case DRV_NO_NEW_DATA : strcpy(error_specific,"DRV-NO-NEW-DATA"); break ; //	20024
    case DRV_SPOOLERROR : strcpy(error_specific,"DRV-SPOOLERROR"); break ; //	20026
    case DRV_TEMP_CODES : strcpy(error_specific,"DRV-TEMP-CODES"); break ; //	20033
    case DRV_TEMP_OFF : strcpy(error_specific,"DRV-TEMP-OFF"); break ; //	20034
    case DRV_TEMP_NOT_STABILIZED : strcpy(error_specific,"DRV-TEMP_-NOT-STABILIZED"); break ; //	20035
    case DRV_TEMP_STABILIZED : strcpy(error_specific,"DRV-TEMP-STABILIZED"); break ; //	20036
    case DRV_TEMP_NOT_REACHED : strcpy(error_specific,"DRV-TEMP-NOT-REACHED"); break ; //	20037
    case DRV_TEMP_OUT_RANGE : strcpy(error_specific,"DRV-TEMP-OUT-RANGE"); break ; //	20038
    case DRV_TEMP_NOT_SUPPORTED : strcpy(error_specific,"DRV-TEMP-NOT-SUPPORTED"); break ; //	20039
    case DRV_TEMP_DRIFT : strcpy(error_specific,"DRV-TEMP-DRIFT"); break ; //	20040
    case DRV_GENERAL_ERRORS : strcpy(error_specific,"DRV-GENERAL-ERRORS"); break ; //	20049
    case DRV_INVALID_AUX : strcpy(error_specific,"DRV-INVALID-AUX"); break ; //	20050
    case DRV_COF_NOTLOADED : strcpy(error_specific,"DRV-COF-NOTLOADED"); break ; //	20051
    case DRV_FPGAPROG : strcpy(error_specific,"DRV-FPGAPROG"); break ; //	20052
    case DRV_FLEXERROR : strcpy(error_specific,"DRV-FLEXERROR"); break ; //	20053
    case DRV_GPIBERROR : strcpy(error_specific,"DRV-GPIBERROR"); break ; //	20054
    case DRV_DATATYPE : strcpy(error_specific,"DRV-DATATYPE"); break ; //	20064
    case DRV_DRIVER_ERRORS : strcpy(error_specific,"DRV-DRIVER-ERRORS"); break ; //	20065
    case DRV_P1INVALID : strcpy(error_specific,"DRV-P1INVALID"); break ; //	20066
    case DRV_P2INVALID : strcpy(error_specific,"DRV-P2INVALID"); break ; //	20067
    case DRV_P3INVALID : strcpy(error_specific,"DRV-P3INVALID"); break ; //	20068
    case DRV_P4INVALID : strcpy(error_specific,"DRV-P4INVALID"); break ; //	20069
    case DRV_INIERROR : strcpy(error_specific,"DRV-INIERROR"); break ; //	20070
    case DRV_COFERROR : strcpy(error_specific,"DRV-COFERROR"); break ; //	20071
    case DRV_ACQUIRING : strcpy(error_specific,"DRV-ACQUIRING"); break ; //	20072
    case DRV_IDLE : strcpy(error_specific,"DRV-IDLE"); break ; //	20073
    case DRV_TEMPCYCLE : strcpy(error_specific,"DRV-TEMPCYCLE"); break ; //	20074
    case DRV_NOT_INITIALIZED : strcpy(error_specific,"DRV-NOT-INITIALIZED"); break ; //	20075
    case DRV_P5INVALID : strcpy(error_specific,"DRV-P5INVALID"); break ; //	20076
    case DRV_P6INVALID : strcpy(error_specific,"DRV-P6INVALID"); break ; //	20077
    case DRV_INVALID_MODE : strcpy(error_specific,"DRV-INVALID-MODE"); break ; //	20078
    case DRV_INVALID_FILTER : strcpy(error_specific,"DRV-INVALID-FILTER"); break ; //	20079
    case DRV_I2CERRORS : strcpy(error_specific,"DRV-I2CERRORS"); break ; //	20080
    case DRV_I2CDEVNOTFOUND : strcpy(error_specific,"DRV-I2CDEVNOTFOUND"); break ; //	20081
    case DRV_I2CTIMEOUT : strcpy(error_specific,"DRV-I2CTIMEOUT"); break ; //	20082
    case DRV_P7INVALID : strcpy(error_specific,"DRV-P7INVALID"); break ; //	20083
    case DRV_USBERROR : strcpy(error_specific,"DRV-USBERROR"); break ; //	20089
    case DRV_IOCERROR : strcpy(error_specific,"DRV-IOCERROR"); break ; //	20090
    case DRV_NOT_SUPPORTED : strcpy(error_specific,"DRV-NOT-SUPPORTED"); break ; //	20991  
    } 
    
  strcpy(error_message,error_generic_begin);
  strcat(error_message,error_specific);
  strcat(error_message,error_generic_end);
    
  win_report(error_message);
    
  return error_code;
}       

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Initialization functions

////////////////////////////////////////////////////////////////////////////////
// Shortcut functions

/** Defines a shortcut to switch between manual and automatic set of z min and z max.

\author Adrien Meglio
\version 4/12/06
*/
int short__set_z_auto(void)
{
  if(updating_menu_state != 0)      
    {
      add_keyboard_short_cut(0, SHORTCUT_IXON_Z_AUTO, 0, short__set_z_auto); /* On AZERTY keyboards : Z is called KEY_W */
      return D_O_K;
    }
    
  if (iXon.contrast == AUTOMATIC)
    {
      iXon.contrast = MANUAL;
    }
  else if (iXon.contrast == MANUAL)
    {
      iXon.contrast = AUTOMATIC;
    }
  
    
  return D_O_K;
}

/** Defines a shortcut to switch between LIVE and FREEZE.

\author Adrien Meglio
\version 4/12/06
*/
int short__pause(void)
{
  if(updating_menu_state != 0)      
    {
      add_keyboard_short_cut(0, SHORTCUT_IXON_PAUSE, 0, short__pause);
      return D_O_K;
    }
    
  if (flag__display_yes_no == YES) 
    {
      flag__display_yes_no = NO;
      launcher_iXon_standby_thread(); /* Launches the thread that will switch the camera to standby mode after timeout is reached */
      //win_report("Stop displaying");
    }
  else if (flag__display_yes_no == NO) 
    {
      flag__display_yes_no = YES;
      if (iXon.standby_action == WAIT_FOR_WAKEUP) /* If standby mode executed and waiting for wakeup */
	{
	  iXon_real_time_imaging();
	  iXon.standby_action = WAKEUP; /* Just signals that no standby mode is active */
	}
      else /* If countdown running but still not in standby mode */
	{
	  iXon.standby_action = WAKEUP; /* Stops the standby thread and returns to active mode */
	}
      //win_report("Resume displaying");

    }

/*   if (iXon.is_live == LIVE)  */
/*     { */
/*       Stop_acquisition(); */
/*     } */
/*   else if (iXon.is_live == FREEZE)  */
/*     { */
/*       iXon_real_time_imaging(); */
/*     } */
/*   else  */
/*     { */
/*       win_report("Invalid iXon.is-live status : %d",iXon.is_live); */
/*       iXon.is_live == FREEZE; */
/*     } */
    
  return D_O_K;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
/** What to do when exiting.

\param None.
\return None.
\author JF Allemand
@version 10/05/06
*/ 
int Exit_iXon(void)
{
  int status;
    
  if(updating_menu_state != 0) return D_O_K;
    
  Stop_acquisition();
    
  status = win_report("Exiting iXon");
  if (status == CANCEL) 
    {
      return D_O_K;
    }
       
  set_temperature(20);

  //SetCoolerMode(0); /* The fan will be switched off at the end */
    
  //FreeInternalMemory(); /* Frees the camera memory */

  ShutDown(); /* Shuts the camera down */

  win_event("Exited normally");
        
  return D_CLOSE;
}  

int standby_action(void)
{
  if(updating_menu_state != 0) return D_O_K;

  Stop_acquisition();

  /* Set gain to min so that not to overuse gain amplifier */
  if (SetEMCCDGain(iXon.gain_min) != DRV_SUCCESS) win_report("Could not set gain to min in function 'standby-action' !");


  return D_O_K;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Housekeeping functions

/** Main function of \a ixon.c . 

This function initializes the system, loads the menus. 
\param none
\return Error code.
\author Adrien Meglio
\version 09/05/06
*/
int iXon_main(void)
{
  imreg *imr = NULL;
  int current_temperature;
    
  //win_report("iXon Startup");

  flag__display_yes_no = YES;

  imr = create_and_register_new_image_project( 0,   32,  900,  668);
  if (imr == NULL) win_report("Could not allocate image region !");
             	
  //win_report("Beginning initialization");
         
  // Log files creation        
  message__create_log_file();
  action__create_log_file();  
             	
  initialize_ixon(); // Initializes iXon camera
	
  //win_report("Camera initialization done");
	
  /* This prevents the camera from re-heating on startup. 
     Useful if Xvin just previously crashed while camera was cooled (-70C for instance) */
  GetTemperature(&current_temperature);
  current_temperature = (current_temperature > 20) ? 20 : current_temperature;
  current_temperature = (current_temperature < -70) ? -70 : current_temperature;
  CoolerON(); /* Switches the cooling system on */
  set_temperature(current_temperature);
  //win_report("Startup temperature : %d",current_temperature);

  /* Must acquire at least one image to fill the image region */
  Set_iXon_parameters(); 
  iXon_real_time_imaging(); // Starts in real-time acquisition
  flag__display_yes_no = NO;
  
  //win_report("Hello ! Just after first acquisition, iXon.is-live is : %d",iXon.is_live);

  menu_ixon_main(); // iXon user menus
   
  launcher__iXon_display_thread(); // Opens the thread that reports the acquisition settings on screen

  launcher_iXon_standby_thread(); /* Launches the thread that will switch the camera to standby mode after timeout is reached */

  return D_O_K;
}

/** Unload function for the \a ixon.c menu.*/
int	iXon_unload(void)
{
  return D_O_K;
}
#endif





