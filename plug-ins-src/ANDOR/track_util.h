#ifndef _TRACK_UTIL_H_
#define _TRACK_UTIL_H_


# define TRACKING_BUFFER_SIZE 4096
# define PROFILE_BUFFER_SIZE 128

# define OBJ_MOVING 32
# define ZMAG_MOVING 64
# define ROT_MOVING 128

typedef struct bead_tracking
{
  int xc, yc, x0, y0, not_lost, im_prof_ref;    // bead pixel position
  int in_image, mouse_draged, mx, my; 
  int start_im;                    // im number for tracking start
  int black_circle;                // flag modifying the bead tracking extend
  int bead_number;                 // a unique bead identifier
  int bp_center;                   // the bp center frequency
  int bp_width;                    // the bp widt frequency
  int rc;                          // the critical radius 
  int integral;                    // a local variable containinin the integral of PID
  float sat_int;                   // the maxium integral value in PID
  float gain;                      // the PID parameters
  float integral_gain;
  float derivative_gain;
  float x[TRACKING_BUFFER_SIZE], y[TRACKING_BUFFER_SIZE], z[TRACKING_BUFFER_SIZE];     // bead position
  float xt[TRACKING_BUFFER_SIZE], yt[TRACKING_BUFFER_SIZE], zt[TRACKING_BUFFER_SIZE];  // bead true position
  char n_l[TRACKING_BUFFER_SIZE];                       // not lost idicator
  float rad_prof_ref[PROFILE_BUFFER_SIZE];             // reference radial profile
  float rad_prof_ref_fft[PROFILE_BUFFER_SIZE];         // reference radial profile fourrier transform
  float rad_prof_fft[PROFILE_BUFFER_SIZE];             // radial profile fourrier transform
  float rad_prof[TRACKING_BUFFER_SIZE][PROFILE_BUFFER_SIZE];                // radial profile
  O_i *calib_im;		   // calibration image
  O_i *calib_im_fil;	           // filtered calibration image
  O_p *opt;                        // trajectory of the last force point just acquired
  float mxm;                        // mean value of x coordinate
  float dmxm;                       // error in mean value of x coordinate
  float sx2;                       // mean variance of x coordinate
  float fcx;                       // cutoff frequency in x
  float dfcx;                      // error in cutoff frequency in x
  float ax;                        // integral of fluctuation in x 
  float dax;                       // error in integral of fluctuation in x 
  float chisqx;                    // chi square in x
  float mym;                        // mean value of y coordinate
  float dmym;                       // error in mean value of y coordinate
  float sy2;                       // mean variance of y coordinate
  float fcy;                       // cutoff frequency in y
  float dfcy;                      // error in cutoff frequency in y
  float ay;                        // integral of fluctuation in y 
  float day;                       // error in integral of fluctuation in y 
  float chisqy;                    // chi square in y
  float mzm;                        // mean value of z coordinate
  float dmzm;                       // error in mean value of z coordinate
  float sz2;                       // mean variance of z coordinate
  float fcz;                       // cutoff frequency in z
  float dfcz;                      // error in cutoff frequency in z
  float az;                        // integral of fluctuation in z 
  float daz;                       // error in integral of fluctuation in z 
  float chisqz;                    // chi square in z
} b_track;


# define BUF_FREEZED 2
# define BUF_CHANGING 1
# define BUF_UNLOCK 0

typedef struct gen_tracking
{
  int (*user_tracking_action)(struct gen_tracking *gt, DIALOG *d, void *data);  // a user callback function       
  int c_i, n_i, m_i, lc_i;          // c_i, index of the current image, n_i size of buffer, m_i size allocated
  int ac_i, lac_i;                  // ac_i actual number of frame since acquisition started + local
  int cl, cw, dis_filter, bd_mul;   // cross size, filter def, and bd_mul define the loss of bead condition 
  int c_b, n_b, m_b;                // the current bead nb and the max bead nb
  float max_obj_mov;                // Maximum relative objective displacement in servo mode
  float z_obj_servo_start;          // objective position at the start of servo mode
  int bead_servo_nb;                // the bead used to servo the objective
  int n_bead_display;               // the bead number corresponding to the plot display
  int local_lock;                   // 0 unlock, 1 changing, 2 freezed
  int xi[256], yi[256];             // tmp int buffers to avg profile
  float xf[256], yf[256];           // tmp float buffers to do fft
  b_track **bd;                     // the bead pointer
  int imi[TRACKING_BUFFER_SIZE];                         // the image nb
  int limi[TRACKING_BUFFER_SIZE];                         // local buffer the image nb
  int imit[TRACKING_BUFFER_SIZE];                        // the image nb by timer
  int limit[TRACKING_BUFFER_SIZE];                        // the image nb by timer
  long long imt[TRACKING_BUFFER_SIZE];                   // the image absolute time
  long long limt[TRACKING_BUFFER_SIZE];                   // local the image absolute time
  long long imtt[TRACKING_BUFFER_SIZE];                  // the image absolute time obtained by timer 
  unsigned long imdt[TRACKING_BUFFER_SIZE];              // the time spent in the previous function call
  unsigned long limdt[TRACKING_BUFFER_SIZE];              // local the time spent in the previous function call
  unsigned long imtdt[TRACKING_BUFFER_SIZE];             // the time spent before image flip in timer mode
  float zmag[TRACKING_BUFFER_SIZE], rot_mag[TRACKING_BUFFER_SIZE], obj_pos[TRACKING_BUFFER_SIZE];  // the magnet position, the objective position measured
  //float *user_val1, user_val2;    // user parameters
  int status_flag[TRACKING_BUFFER_SIZE];
  float zmag_cmd[TRACKING_BUFFER_SIZE], rot_mag_cmd[TRACKING_BUFFER_SIZE], obj_pos_cmd[TRACKING_BUFFER_SIZE];  // the magnet position, the objective position asked for

} g_track;


# ifdef _TRACK_UTIL_C_
g_track *track_info = NULL;
int px0,px1,py0,py1;
float intensity[TRACKING_BUFFER_SIZE];
# else
PXV_VAR(g_track*, track_info);
PXV_VAR(int, px0);
PXV_VAR(int, px1);
PXV_VAR(int, py0);
PXV_VAR(int, py1);
PXV_VAR(int,x_center);
PXV_VAR(int,y_center);
# endif



PXV_FUNC(int, grab_bead_info_from_calibration, (O_i *oi, float *xb, float *yb, float *zb, int *nx, int *cw));
PXV_FUNC(O_i*, do_load_calibration, (int n));
PXV_FUNC(int, show_bead_cross, (BITMAP *imb, imreg *imr, DIALOG *d));
PXV_FUNC(int, record_calibration, (void));

PXV_FUNC(int, get_present_image_nb, (void));
PXV_FUNC(int, move_bead_cross, (BITMAP *imb, imreg *imr, DIALOG *d, int x0, int y0));
# endif



