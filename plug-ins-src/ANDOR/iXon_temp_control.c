/** \file iXon_temp_control.c
    \brief Plug-in program for temperature control of iXon camera.
    This is a subprogram that monitors temperature, displays related information and allows
    one to control it.
    \sa ixon.c
    \author A Meglio
*/
#ifndef _IXON_TEMP_CONTROL_C_ 
#define _IXON_TEMP_CONTROL_C_

#include "allegro.h"
#include "winalleg.h"
#include <windowsx.h>
#include <stdlib.h>
#include <string.h>
#include "box2alfn.h"
#include "ctype.h"
#include "xvin.h"
#include "ATMCD32D.h"
#include "global_define.h"
#include "global_variables.h"
#include "global_functions.h"

#define BUILDING_PLUGINS_DLL
#include "iXon_temp_control.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Temperature return functions

/** Returns the current temperature (closest integer).

\param None.
\return int The current temperature;
\author Adrien Meglio
*/ 
int give_me_temp_in(void)
{
  int in_temp;
  int error_code;
    
  if(updating_menu_state != 0) return D_O_K;
    
  // Freezes the display
  Stop_acquisition();
    
  error_code = GetTemperature(&in_temp);
    
  if (error_code != DRV_TEMP_STABILIZED && error_code != DRV_TEMP_NOT_REACHED) in_temp =  OUT_OF_RANGE;
    
  iXon.temperature_lastread = in_temp; /* Last value of the temperature read from the camera */
    
  // Back to live
  iXon_real_time_imaging();
    
  return in_temp;
}

/** Returns the current temperature (float).

\param None.
\return float The current temperature;
\author Adrien Meglio
*/ 
float give_me_temp_fl(void)
{
  float fl_temp;
  int error_code;
    
  if(updating_menu_state != 0) return D_O_K;
    
  // Freezes the display
  Stop_acquisition();
    
  error_code = GetTemperatureF(&fl_temp);
    
  if (error_code != DRV_TEMP_STABILIZED && error_code != DRV_TEMP_NOT_REACHED) fl_temp =  OUT_OF_RANGE;
    
  iXon.temperature_lastread = (int) fl_temp; /* Last value of the temperature read from the camera */
    
  // Back to live
  iXon_real_time_imaging();
    
  return fl_temp;
}    

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Temperature user functions

/** Sets the iXon temperature according to the user target.

This function tries to set the temperature at temperature_target. 
\param int Temperature target.
\return int Error code.

\author Adrien Meglio
\version 23/08/07
*/ 
int set_temperature(int temperature_target)
{
  unsigned int get_temperature_error_code;
  int current_temperature;
  unsigned int set_temperature_error_code = DRV_TEMP_NOT_REACHED;
      
  if(updating_menu_state != 0) return D_O_K;
    
  // Freezes the display
  Stop_acquisition();
    
  iXon.temperature_target = temperature_target; /* Stores the target temperature (int format) */
    
  get_temperature_error_code = GetTemperature(&current_temperature);
    
  iXon.temperature_lastread = current_temperature; /* Last value of the temperature read from the camera */
    
  set_temperature_error_code = SetTemperature(temperature_target);

  /* Prints into the log file */
  win_event("Temperature target set to : %d �C",temperature_target);
    
  /* EXTREMELY IMPORTANT ! 
     at NO COST should the acquisition thread be launched from here, because it will cause Set_ixon_parameters() and then set_temperature() 
     to be called, thus causing an infinite loop */
    
  return set_temperature_error_code;
}               

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Menu void functions

int set_temperature_void(void)
{
  int temperature_target = 20;
  unsigned int set_temperature_error;
            
  if(updating_menu_state != 0) return D_O_K;
    
  // Freezes the display
  Stop_acquisition();
   
  win_scanf("What's the temperature target ? %d",&temperature_target);
    
  set_temperature_error = set_temperature(temperature_target);
    
  // Back to live
  iXon_real_time_imaging();
    
  return D_O_K;
}

int give_me_temp_void(void)
{
  float temp;
    
  if(updating_menu_state != 0) return D_O_K;
    
  // Freezes the display
  Stop_acquisition();
    
  temp = give_me_temp_fl();
    
  if (temp == OUT_OF_RANGE) return win_report("OUT_OF_RANGE");
  else win_report("Temperature is : %f",temp);
    
  // Back to live
  iXon_real_time_imaging();
    
  return D_O_K;
}         
 
int give_me_temperature_range_void(void)
{
  int min_temp;
  int max_temp;
    
  int error_code;
    
  if(updating_menu_state != 0) return D_O_K;
    
  // Freezes the display
  Stop_acquisition();
    
  error_code = GetTemperatureRange(&min_temp,&max_temp);
    
  if (error_code == DRV_SUCCESS) win_report("Temperature range is set from %d to %d",min_temp,max_temp);
  else win_report("Could not retrieve temperature range !");
    
  // Back to live
  iXon_real_time_imaging();
    
  return error_code;
    
}

#endif




