#ifndef _FOCUS_C_
#define _FOCUS_C_


# include "ctype.h"
# include "allegro.h"
# include "winalleg.h"
# include "xvin.h"

# include "xv_plugins.h"	// for inclusions of libraries that control dynamic loading of libraries.


# define BUILDING_PLUGINS_DLL
# include "../cfg_file/Pico_cfg.h"


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "focus.h"
# include "action.h"


int    set_focus_device(Focus_param *f_p)
{
  int ret = 0, i;
  char *argvp[2];
  void*	hinstLib;
  char pl_name[256];

  //if (strncmp("Dummy",f_p->Focus_driver,6) == 0)
  //  {
  sprintf(pl_name,"%s_focus",f_p->Focus_driver);
  for (i=0; pl_name[i]; i++)
       pl_name[i] = tolower(pl_name[i]);

  //argvp[0] = "dummy_focus";
  argvp[0] = pl_name;
      argvp[1] = NULL;
      load_plugin(1, argvp);
      hinstLib = grep_plug_ins_hinstLib(argvp[0]);
      if (hinstLib == NULL)
	return win_printf_OK("Could not find plugins %s",argvp[0]);
      read_Z_value = (int(*)(void))GetProcAddress(hinstLib, "_read_Z_value");
      if (read_Z_value == NULL) win_printf("Could not find: read_Z_value()\nin%s",argvp[0]); 
      read_Z_value_accurate_in_micron = (float (*)(void))GetProcAddress(hinstLib,"_read_Z_value_accurate_in_micron");
      if (read_Z_value_accurate_in_micron == NULL) win_printf("Could not find: read_Z_value_accurate_in_micron()\nin%s",argvp[0]); 
      read_Z_value_OK = (float (*)(int *error))GetProcAddress(hinstLib,"_read_Z_value_OK"); 
      if (read_Z_value_OK == NULL) win_printf("Could not find: read_Z_value_OK()\nin%s",argvp[0]); 
      read_last_Z_value = (float(*)(void))GetProcAddress(hinstLib, "_read_last_Z_value");
      if (read_last_Z_value == NULL) win_printf("Could not find: read_last_Z_value()\nin%s",argvp[0]); 
      set_Z_value_raw = (int (*)(float z))GetProcAddress(hinstLib,"_set_Z_value");
      if (set_Z_value_raw == NULL) win_printf("Could not find: set_Z_value()\nin%s",argvp[0]); 
      set_Z_obj_accurate_raw = (int	(*)(float zstart))GetProcAddress(hinstLib,"_set_Z_obj_accurate");
      if (set_Z_obj_accurate_raw == NULL) win_printf("Could not find: set_Z_obj_accurate()\nin%s",argvp[0]); 
      set_Z_value_OK_raw = (int (*)(float z))GetProcAddress(hinstLib,"_set_Z_value_OK");
      if (set_Z_value_OK_raw == NULL) win_printf("Could not find: set_Z_value_OK()\nin%s",argvp[0]); 
      set_Z_step = (int (*)(float z))GetProcAddress(hinstLib,"_set_Z_step");
      if (set_Z_step == NULL) win_printf("Could not find: set_Z_step()\nin%s",argvp[0]); 
      inc_Z_value_raw = (int (*)(int step))GetProcAddress(hinstLib,"_inc_Z_value");
      if (inc_Z_value_raw == NULL) win_printf("Could not find: inc_Z_value()\nin%s",argvp[0]); 
      small_inc_Z_value = (int (*)(int up))GetProcAddress(hinstLib,"_small_inc_Z_value");
      if (small_inc_Z_value == NULL) win_printf("Could not find: small_inc_Z_value()\nin%s",argvp[0]); 
      big_inc_Z_value = (int (*)(int up))GetProcAddress(hinstLib,"_big_inc_Z_value");
      if (big_inc_Z_value == NULL) win_printf("Could not find: big_inc_Z_value()\nin%s",argvp[0]); 
      //win_printf_OK("Focus plugins loaded");
      //  }
      //else
      // { 
      //win_printf("Unknowng focusing device");
      //return 1;
      //}
  return ret;
}

int	set_Z_value(float r)
{
  set_Z_value_raw(r);
  Open_Pico_config_file();
  set_config_float("STATE", "ObjZ", r);
  Close_Pico_config_file();
  return 0;
}
int	set_Z_value_OK(float r)
{
  set_Z_value_OK_raw(r);
  Open_Pico_config_file();
  set_config_float("STATE", "ObjZ", r);
  Close_Pico_config_file();
  return 0;
}
int set_Z_obj_accurate(float r)
{
  set_Z_obj_accurate_raw(r);
  Open_Pico_config_file();
  set_config_float("STATE", "ObjZ", r);
  Close_Pico_config_file();
  return 0;
}
int	inc_Z_value(int st)
{
  float z = read_last_Z_value();
  inc_Z_value_raw(st);
  Open_Pico_config_file();
  set_config_float("STATE", "ObjZ", z+st);
  Close_Pico_config_file();
  return 0;
}
int read_Z(void)
{
  float z;
  if(updating_menu_state != 0)	return D_O_K;
  
  z = read_Z_value_accurate_in_micron();
  win_printf("The objective position in z is %f \\mu m\n",z);
  return D_O_K;
    
}
int set_Z(void)
{
  register int i;
  static float z = 0;
	
  if(updating_menu_state != 0)	return D_O_K;
  
  z = read_last_Z_value();
  i = win_scanf("Where do you want to go in \\mu m %f",&z);
  if (i == CANCEL)	return D_O_K;
  set_Z_value(z);
  return D_O_K;
}

int move_obj_small_up(void)
{
  if(updating_menu_state != 0)	
    {
      return D_O_K;	
    }
  //z_obj += 1;
  //big_inc_Z_value(1);
  immediate_next_available_action(MV_OBJ_REL, 0.1);
  //*focus_position += 1; // z_obj
  return D_O_K;
}

int move_obj_up(void)
{
  if(updating_menu_state != 0)	
    {
      add_keyboard_short_cut(0, KEY_UP, 0, move_obj_up);
      add_keyboard_short_cut(0, KEY_F1, 0, move_obj_up);
      add_keyboard_short_cut(0, KEY_F2, 0, move_obj_small_up);

      return D_O_K;	
    }
  //z_obj += 1;
  //big_inc_Z_value(1);
  immediate_next_available_action(MV_OBJ_REL, 1);
  //*focus_position += 1; // z_obj
  return D_O_K;
}

int move_obj_small_dwn(void)
{

  if(updating_menu_state != 0)	
    {
      return D_O_K;	
    }	
  //z_obj -= 1;
  immediate_next_available_action(MV_OBJ_REL, -0.1);
  //big_inc_Z_value(-1);
  //*focus_position -= 1;  // z_obj
  return D_O_K;
}

int move_obj_dwn(void)
{

  if(updating_menu_state != 0)	
    {
      add_keyboard_short_cut(0, KEY_DOWN, 0, move_obj_dwn);
      add_keyboard_short_cut(0, KEY_F4, 0, move_obj_dwn);
      add_keyboard_short_cut(0, KEY_F3, 0, move_obj_small_dwn);
      return D_O_K;	
    }	
  //z_obj -= 1;
  immediate_next_available_action(MV_OBJ_REL, -1);
  //big_inc_Z_value(-1);
  //*focus_position -= 1;  // z_obj
  return D_O_K;
}


MENU *focus_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"Read Objective position", read_Z,NULL,0,NULL);
	add_item_to_menu(mn,"Set Objective position", set_Z,NULL,0,NULL);
	add_item_to_menu(mn,"Move up", move_obj_up,NULL,0,NULL);
	add_item_to_menu(mn,"Move down", move_obj_dwn,NULL,0,NULL);

	return mn;
}

int	focus_main(int argc, char **argv)
{
	set_focus_device(&(Pico_param.focus_param));
	add_plot_treat_menu_item ( "focus", NULL, focus_plot_menu(), 0, NULL);
	add_image_treat_menu_item ( "focus", NULL, focus_plot_menu(), 0, NULL);
	return D_O_K;
}


# endif /* _ZOBJ_C_ */
