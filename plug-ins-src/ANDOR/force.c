
#ifndef _FORCE_C_
#define _FORCE_C_

# include "allegro.h"
# include "winalleg.h"

# include "xvin.h"
# include "fftl32n.h"
# include "fillib.h"





/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "../cfg_file/Pico_cfg.h"
# include "../cardinal/cardinal.h"


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "focus.h"
# include "magnetscontrol.h"
# include "trackBead.h"
# include "brown_util.h"
# include "track_util.h"
# include "force.h"
# include "scan_zmag.h"
# include "action.h"

int force_op_idle_action(O_p *op, DIALOG *d)
{
  if (d == NULL || d->dp == NULL || d->proc == NULL)    return 1;
  if (op->need_to_refresh)      d->proc(MSG_DRAW,d,0);	
  return 0;
}

int force_job_xyz(int im, struct future_job *job)
{
  register int j;
  int ci, nf1, istep;
  b_track *bt = NULL;
  d_s *dsx, *dsy, *dsz, *ds, *dsm;
  O_p *op;
  force_param *sp = NULL; 
  char name[256];
  float ax, dx, ay, dy, y_over_x, y_over_z;
  un_s *un;


  if (job == NULL || job->oi == NULL || job->in_progress == 0 || job->op->n_dat < 3) return 0;
  if (im < job->imi)  return 0;

  ci = track_info->c_i;
  sp = (force_param *)job->more_data;
  dsx = job->op->dat[0];
  dsy = job->op->dat[1];
  dsz = job->op->dat[2];
  dsm = job->op->dat[3];
  bt = track_info->bd[job->bead_nb];
  istep = sp->istep;

  un = job->oi->xu[job->oi->c_xu];
  get_afine_param_from_unit(un, &ax, &dx);
  un = job->oi->yu[job->oi->c_yu];
  get_afine_param_from_unit(un, &ay, &dy);
  y_over_x = (dy != 0) ? dx/dy : 1;
  y_over_z = 1e-6;
  y_over_z = (dy != 0) ? y_over_z/dy : 1;



  while (track_info->imi[ci] > job->imi)
    {
      ci--;
      if (ci < 0) ci += TRACKING_BUFFER_SIZE;
    }

  while (track_info->imi[ci] >= (job->imi-32))
    {
      j = track_info->imi[ci] - job->in_progress;
      if (j < dsx->mx)
	{
	  dsx->yd[j] = y_over_x * bt->x[ci];
	  dsy->yd[j] = bt->y[ci];
	  dsz->yd[j] = y_over_z * bt->z[ci];
	  dsm->yd[j] = track_info->zmag[ci];
	  dsx->xd[j] = dsy->xd[j] = dsz->xd[j] = dsm->xd[j] = j;
	}
      ci--;
      if (ci < 0) ci += TRACKING_BUFFER_SIZE;
    }
  j = job->imi - job->in_progress;
  if (j <= dsx->mx)
    dsx->nx = dsx->ny = dsy->nx = dsy->ny = dsz->nx = dsz->ny = dsm->nx = dsm->ny = j;
  if (j >= dsx->mx)
        dsx->nx = dsx->ny = dsy->nx = dsy->ny = dsz->nx = dsz->ny = dsm->nx = dsm->ny = dsx->mx;
  job->op->need_to_refresh = 1;
  if (dsx->nx < dsx->mx) job->imi += 32;
  else 
    {
      istep++;
      bt->opt = job->op;
      if (istep > sp->nstep) job->in_progress = 0; 
      else if ((sp->im[istep+1] - sp->im[istep] - sp->dead) > 0)
	{
	  sprintf(name,"Force curve %d bead %d step %d",n_force,job->bead_nb, istep);
	  nf1 =  sp->im[istep+1] - sp->im[istep] - sp->dead;
	  op = create_and_attach_one_plot(job->pr, nf1, nf1, 0);
	  op->data_changing = 1;
	  ds = op->dat[0];
	  ds->nx = ds->ny = 0;
	  set_ds_source(ds,"Bead tracking at %d Hz Acquistion %d for bead %d \n "
			"X coordinate l = %d, w = %d, nim = %d\n"
			"objective %f, zoom factor %f, sample %s Rotation %g\n"
			"Z magnets %g mm\n"
			"Calibration from %s"
			,Pico_param.camera_param.camera_frequency_in_Hz,n_force,job->bead_nb
			,track_info->cl,track_info->cw,nf1
			,Pico_param.obj_param.objective_magnification
			,Pico_param.micro_param.zoom_factor
			,sample
			,sp->ro[istep]
			,sp->zm[istep]
			,bt->calib_im->filename);
	  if ((ds = create_and_attach_one_ds(op, nf1, nf1, 0)) == NULL)
	    return win_printf_OK("I can't create plot !");
	  ds->nx = ds->ny = 0;
	  set_ds_source(ds,"Bead tracking at %d Hz Acquistion %d for bead %d \n "
			"Y coordinate l = %d, w = %d, nim = %d\n"
			"objective %f, zoom factor %f, sample %s Rotation %g\n"
			"Z magnets %g mm\n"
			"Calibration from %s"
			,Pico_param.camera_param.camera_frequency_in_Hz,n_force,job->bead_nb
			,track_info->cl,track_info->cw,nf1
			,Pico_param.obj_param.objective_magnification
			,Pico_param.micro_param.zoom_factor
			,sample
			,sp->ro[istep]
			,sp->zm[istep]
			,bt->calib_im->filename);
	  if ((ds = create_and_attach_one_ds(op, nf1, nf1, 0)) == NULL)
	    return win_printf_OK("I can't create plot !");
	  ds->nx = ds->ny = 0;
	  set_ds_source(ds,"Bead tracking at %d Hz Acquistion %d for bead %d \n "
			"Z coordinate l = %d, w = %d, nim = %d\n"
			"objective %f, zoom factor %f, sample %s Rotation %g\n"
			"Z magnets %g mm\n"
			"Calibration from %s"
			,Pico_param.camera_param.camera_frequency_in_Hz,n_force,job->bead_nb
			,track_info->cl,track_info->cw,nf1
			,Pico_param.obj_param.objective_magnification
			,Pico_param.micro_param.zoom_factor
			,sample
			,sp->ro[istep]
			,sp->zm[istep]
			,bt->calib_im->filename);
	  
	  uns_op_2_op(op, job->op);
	  op->op_idle_action = force_op_idle_action; //xyz_scan_idle_action;
	  set_op_filename(op, "X(t)Y(t)Z(t)bd%dforce%d-%d.gr",job->bead_nb,n_force,istep);
	  set_plot_title(op, "Bead %d trajectory %d-%d", job->bead_nb,n_force,istep);
	  set_plot_x_title(op, "Time");
	  set_plot_y_title(op, "Position");			
	  
	  if ((ds = create_and_attach_one_ds(op, nf1, nf1, 0)) == NULL)
	    return win_printf_OK("I can't create plot !");
	  ds->nx = ds->ny = 0;

	  op->x_lo = (-nf1/64);
	  op->x_hi = (nf1 + nf1/64);
	  set_plot_x_fixed_range(op);
	  op->data_changing = 0;
	  job->op = op;
	  job->imi = sp->im[istep] + sp->dead + 32;
	  job->in_progress = sp->im[istep] + sp->dead;
	  if (job->bead_nb == track_info->n_b - 1) sp->istep = istep; 
	}
      else
	{
	  win_printf("istep %d, im[istep+1] %d,\n im[istep] %d, dead %d", istep, sp->im[istep+1], sp->im[istep], sp->dead); 
	}
    }
  return 0;
}


int force_job_scankx(int im, struct future_job *job)
{
  register int i;
  int  ci;
  b_track *bt = NULL;
  d_s *dsx, *dsy, *dsz, *dsfx, *dsfy;
  force_param *sp = NULL; 
  double mx = 0, my = 0, mz = 0, sx = 0, sy = 0, sz = 0, tmp;
  float ay, dy;
  un_s *un;

  if (job == NULL || job->oi == NULL || job->in_progress == 0 || job->more_data == NULL) return 0;
  if (im < job->imi)  return 0;

  ci = track_info->c_i;
  dsfx = job->op->dat[0];
  dsfy = job->op->dat[1];
  sp = (force_param *)job->more_data;
  bt = track_info->bd[job->bead_nb];

  if (bt->opt == NULL)   return 0;
  dsx = bt->opt->dat[0];
  dsy = bt->opt->dat[1];
  dsz = bt->opt->dat[2];
  un = bt->opt->yu[bt->opt->c_yu];
  get_afine_param_from_unit(un, &ay, &dy);



  for (i = 0; i < dsx->nx; i++)
    {
      mx += dsx->yd[i];
      my += dsy->yd[i];
      mz += dsz->yd[i];
    }
  if (dsx->nx > 0)
    {
      mx /= dsx->nx;
      my /= dsx->nx;
      mz /= dsx->nx;
    }
  for (i = 0; i < dsx->nx; i++)
    {
      tmp = dsx->yd[i] - mx;
      tmp *= tmp;
      sx += tmp;
      tmp = dsy->yd[i] - my;
      tmp *= tmp;
      sy += tmp;
      tmp = dsz->yd[i] - mz;
      tmp *= tmp;
      sz += tmp;
    }
  if ((dsx->nx - 1) > 0)
    {
      sx /= (dsx->nx - 1);
      sy /= (dsx->nx - 1);
      sz /= (dsx->nx - 1);
    }

  dy *= 1e6;

  bt->mzm = dsfx->xd[dsfx->nx] = dsfy->xd[dsfx->nx] = dy * mz;
  bt->mxm = dy * mx;
  bt->mym = dy * my;

  dsfx->yd[dsfx->nx] = dy * sqrt(sx);
  bt->sx2 = dy * dy * sx;

  dsfx->yd[dsfx->nx] = (1.38e-11 * 298)/bt->sx2;
  dsfx->yd[dsfx->nx] *= bt->mzm * 1e6;

  dsfy->yd[dsfx->nx] = dy * sqrt(sy);
  bt->sy2 = dy * dy * sy;

  dsfy->yd[dsfx->nx] = (1.38e-11 * 298)/bt->sy2;
  dsfy->yd[dsfx->nx] *= bt->mzm * 1e6;

  if (dsfx->nx < dsfx->mx)
    dsfx->nx = dsfx->ny = dsfy->nx = dsfy->ny = dsfx->nx + 1;

  for (i = 2; i <= dsx->nx; i *= 2);
  i /= 2;
  /*  pb using fft ....
  if (i == dsx->nx)
    {
      alternate_lorentian_fit_acq_spe_with_error(dsx->yd, dsx->ny, dy, 10, dsx->ny/2, 
					     1, 1, 0.97, 0, &(bt->fcx), &(bt->ax), 
						 &(bt->dfcx), &(bt->dax), &(bt->chisqx), &(bt->dmxm));
      alternate_lorentian_fit_acq_spe_with_error(dsy->yd, dsy->ny, dy, 10, dsy->ny/2, 
					     1, 1, 0.97, 0, &(bt->fcy), &(bt->ay), 
						 &(bt->dfcy), &(bt->day), &(bt->chisqy), &(bt->dmym));
      alternate_lorentian_fit_acq_spe_with_error(dsz->yd, dsz->ny, dy, 10, dsz->ny/2, 
					     1, 1, 0.97, 0, &(bt->fcz), &(bt->az), 
						 &(bt->dfcz), &(bt->daz), &(bt->chisqz), &(bt->dmzm));
    }
  */







  job->op->need_to_refresh = 1;
  if (dsfx->nx < dsfx->mx) job->imi = sp->im[dsfx->nx+1];
  else 
    {
      job->in_progress = 0; 
    }
  return 0;
}

int force_job_scanz(int im, struct future_job *job)
{
  int  ci;
  b_track *bt = NULL;
  d_s *dsz;
  force_param *sp = NULL; 

  if (job == NULL || job->oi == NULL || job->in_progress == 0 || job->more_data == NULL) return 0;
  if (im < job->imi)  return 0;

  ci = track_info->c_i;
  dsz = job->op->dat[0];
  sp = (force_param *)job->more_data;
  bt = track_info->bd[job->bead_nb];
  if (bt->opt == NULL)   return 0;


  dsz->xd[dsz->nx] = bt->mzm;
  dsz->yd[dsz->nx] = sp->zm[dsz->nx];

  if (dsz->nx < dsz->mx)
    dsz->nx = dsz->ny = dsz->nx + 1;

  job->op->need_to_refresh = 1;
  if (dsz->nx < dsz->mx) job->imi = sp->im[dsz->nx+1] + 1;
  else 
    {
      job->in_progress = 0; 
      n_force++;
    }
  return 0;
}



int record_force_curve(void)
{
   int i, j, k, im, nf1, li;
   imreg *imr;
   O_i *oi;
   b_track *bt = NULL;
   pltreg *pr;
   O_p *op;
   d_s *ds;
   static int nf = 2048, nstep = 8,  dead = 10;
   static float fbr = ((float)10)/4096, fdr = 0.5, accuracy = 10;
   static float zmag_start = 9, zmag_step = -.5, rot_start, zmag,  zmag_finish = 8, z_cor = 0.878;
   char question[1024], name[256];
   force_param *sp; 

   if(updating_menu_state != 0)	
     {
       if (track_info->n_b == 0) 
	 active_menu->flags |=  D_DISABLED;
       else
	 {
	   for (i = 0, k = 0; i < track_info->n_b; i++)
	     {
	       bt = track_info->bd[i];
	       k += (bt->calib_im != NULL) ? 1 : 0;
	     }
	   if (k) active_menu->flags &= ~D_DISABLED;	
	   else active_menu->flags |=  D_DISABLED;
	 }
       return D_O_K;
     }
   if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)	return OFF;

   rot_start = what_is_present_rot_value();
   zmag = read_magnet_z_value();
   sprintf(question,"{\\pt12 Starting a new Force curve recording}\n"
	   "This function will create two new projets per active bead, one containing force\n"
	   "curves and related plots, the second containing the x,y,z trajectories for each force\n"
	   "Z_{mag} present value is %g rotation position is %g\n" 
	   "Starting zmag %%6f with step of %%6f nb of step %%6d\n"
	   "Rotation position for curve %%8f\n" 
	   "typical accuracy %%8f(%%%%)\n"
	   "Number of frames skipped after each force change %%8d\n" 
	   "zmag to stand when acquisition(s) are finished %%8f\n"
	   "z correction factor%%8f\n"
	   ,rot_start,zmag);

   i = win_scanf(question,&zmag_start,&zmag_step,&nstep,&rot_start,&accuracy,&dead,&zmag_finish,&z_cor);	

   if (i == CANCEL)	return OFF; 

   //  win_printf("1");
   set_window_title("force 1");
   sp = (force_param*)calloc(1,sizeof(force_param));
   if (sp == NULL)  win_printf_OK("Could no allocte force parameters!");
   sp->im = (int*)calloc(nstep+2,sizeof(int));
   sp->ro = (float*)calloc(nstep+2,sizeof(float));
   sp->zm = (float*)calloc(nstep+2,sizeof(float));
   sp->fm = (float*)calloc(nstep+2,sizeof(float));
   if (sp->im == NULL || sp->ro == NULL || sp->zm == NULL || sp->fm == NULL)  
     win_printf_OK("Could no allocte force parameters!");

  sp->nstep = nstep;
  sp->istep = 0;
  sp->dead = dead;
  sp->zmag_start = zmag_start;
  sp->zmag_step = zmag_step;
  sp->rot_start = rot_start;
  sp->fbr = fbr;
  sp->fdr = fdr;
  sp->accuracy = (accuracy > 0) ? accuracy : 10;

  //win_printf("1a");
  im = track_info->imi[track_info->c_i];                                 

  //  win_printf("1b");
      /*
  k = fill_next_available_action(im+4, MV_ZMAG_ABS, sp->zmag_start);
  if (k < 0)	win_printf_OK("Could not add pending action!");

  im += 64;

  k = fill_next_available_action(im, MV_ROT_ABS, sp->rot_start);
  if (k < 0)	win_printf_OK("Could not add pending action!");
      */
  im += 64;
  //win_printf("1c");
  set_window_title("force 2");
  // win_printf("2");
  for (i = 0, nf = 0; i <= nstep ; i++)
    {
      sp->ro[i] = rot_start;
      sp->zm[i] = zmag_start + (i * zmag_step);
      sp->fm[i] = zmag_to_force(0, sp->zm[i]);
      sp->im[i] = im + nf;
      k = (int)(((2*M_PI*force_to_tau_x_in_fr(sp->fm[i], 0))*10000/(sp->accuracy*sp->accuracy))+0.5);
      for (j = 2; j < k; j *= 2);
      //win_printf(" zmag %f nf %d",sp->zm[i],j);
      nf += j;
      nf += sp->dead;
    }
  sp->ro[i] = rot_start;
  sp->zm[i] = zmag_start + (nstep * zmag_step);
  sp->im[i] = im + nf;

  //win_printf("i-1 %d n %d, i %d n %d",i-1,sp->im[i-1],i,sp->im[i]);

  set_window_title("force 3");
  //#ifdef BUG
   for (i = 0; i < track_info->n_b; i++)
     {
       bt = track_info->bd[i];
       if ((bt->calib_im == NULL) || (bt->not_lost <= 0) || (bt->in_image == 0))
	 continue;
       sprintf(name,"Force recordings %d bead %d",n_force,i);
       nf1 =  sp->im[1] - im - sp->dead;
       pr = create_hidden_pltreg_with_op(&op, nf1, nf1, 0,name);
       if (pr == NULL)  win_printf_OK("Could not find or allocte plot region!");
       op = pr->one_p;
       ds = op->dat[0];
       ds->nx = ds->ny = 0;
       set_ds_source(ds,"Bead tracking at %d Hz Acquistion %d for bead %d \n "
		     "X coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_force,i
		     ,track_info->cl,track_info->cw,nf1
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,sample
		     ,what_is_present_rot_value()
		     ,what_is_present_z_mag_value()
		     ,bt->calib_im->filename);


       if ((ds = create_and_attach_one_ds(op, nf1, nf1, 0)) == NULL)
	 return win_printf_OK("I can't create plot !");
       ds->nx = ds->ny = 0;
       set_ds_source(ds,"Bead tracking at %d Hz Acquistion %d for bead %d \n "
		     "Y coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_force,i
		     ,track_info->cl,track_info->cw,nf1
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,sample
		     ,what_is_present_rot_value()
		     ,what_is_present_z_mag_value()
		     ,bt->calib_im->filename);
       if ((ds = create_and_attach_one_ds(op, nf1, nf1, 0)) == NULL)
	 return win_printf_OK("I can't create plot !");
       ds->nx = ds->ny = 0;
       set_ds_source(ds,"Bead tracking at %d Hz Acquistion %d for bead %d \n "
		     "Z coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_force,i
		     ,track_info->cl,track_info->cw,nf1
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,sample
		     ,what_is_present_rot_value()
		     ,what_is_present_z_mag_value()
		     ,bt->calib_im->filename);

       uns_oi_2_op(oi, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
       create_attach_select_x_un_to_op(op, IS_SECOND, 0
				       ,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");
       
       op->op_idle_action = force_op_idle_action; //xyz_scan_idle_action;
       set_op_filename(op, "X(t)Y(t)Z(t)bd%dforce%d-0.gr",i,n_force);
       set_plot_title(op, "Bead %d trajectory %d-0", i,n_force);
       set_plot_x_title(op, "Time");
       set_plot_y_title(op, "Position");			

       op->x_lo = (-nf1/64);
       op->x_hi = (nf1 + nf1/64);
       set_plot_x_fixed_range(op);

       if ((ds = create_and_attach_one_ds(op, nf1, nf1, 0)) == NULL)
	 return win_printf_OK("I can't create plot !");
       ds->nx = ds->ny = 0;

       fill_next_available_job_spot(im + sp->dead, im+sp->dead+32, COLLECT_DATA, i, imr, oi, pr, op, (void*)sp, force_job_xyz, 0);

       // We create a plot region to display bead position, force  etc.
       sprintf(name,"Force curve %d bead %d",n_force,i);
       pr = create_hidden_pltreg_with_op(&op, nstep+1, nstep+1, 0,name);
       if (pr == NULL)  win_printf_OK("Could not find or allocte plot region!");
       set_plot_title(op, "Beads %d Force curve %d, rot %f Zmag", i, n_force,sp->ro[0]);
       set_plot_x_title(op,"Lengh (\\mu m)");
       set_plot_y_title(op,"Expected Force (pN)");
       set_plot_file(op,"Forcebd%01d%03d%s",i,n_force,".gr");
       ds = op->dat[0];   
       ds->nx = ds->ny = 0;       
       set_plot_symb(ds, "\\pt5\\oc");
       set_dot_line(ds);
       set_ds_source(ds,"DNA force extension curve f_x 3D length bead %d\n",i);
       ds = create_and_attach_one_ds(op, nstep+1, nstep+1, 0);
       if (ds == NULL)  win_printf_OK("cannot allocate ds!");    
       ds->nx = ds->ny = 0;       
       set_plot_symb(ds, "\\pt5\\oc");
       set_dot_line(ds);
       set_ds_source(ds,"DNA force extension curve f_y 3D length bead %d\n",i);
       op->op_idle_action = force_op_idle_action; 
       fill_next_available_job_spot(1, sp->im[1], COLLECT_DATA, i, imr, oi, pr, op, (void*)sp, force_job_scankx,0);



       op = create_and_attach_one_plot(pr, nstep + 1,  nstep + 1, 0);
       ds = op->dat[0];
       ds->nx = ds->ny = 0;       
       //set_dot_line(ds);
       set_plot_symb(ds, "\\pt5\\oc");
       set_plot_title(op, "Beads %d Force %d, rot %f", i, n_force,sp->ro[0]);
       set_plot_x_title(op,"Lengh (\\mu m)");
       set_plot_y_title(op,"Z_{mag} (mm)");
       set_plot_file(op,"scanzbd%01d%03d%s",i,n_force,".gr");
       
       /*
       set_ds_source(ds,"Scan Z\nZ mag start = %g for %d frames scan by steps of %g"
		     " for %d steps \nrot = %g dead period %d\n"
		     "Bead  calbration %s \n"
		     ,zstart,nper,zstep,nstep,what_is_present_rot_value()
		     ,dead,bt->calib_im->filename);	
       */
       op->op_idle_action = force_op_idle_action; 
       fill_next_available_job_spot(1, sp->im[1]+1, COLLECT_DATA, i, imr, oi, pr, op, (void*)sp, force_job_scanz,0);




       //       op = create_and_attach_one_plot(pr, 2*nstep,  2*nstep, 0);
     }
   //#endif
   //win_printf("3");
  set_window_title("force 4");

   for (i = 0, nf = 0; i <= nstep ; i++)
     {
       k = fill_next_available_action(sp->im[i], MV_ZMAG_ABS, sp->zm[i]);
       if (k < 0)	win_printf_OK("Could not add pending action!");
     }
   //win_title_used = 1;
   // win_printf("4");
  set_window_title("force 4");
   return D_O_K;

}


# endif
