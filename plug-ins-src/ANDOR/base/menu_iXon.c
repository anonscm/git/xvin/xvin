/** \file menu_iXon.c
    \brief Plug-in program for menu settings and display of iXon camera.
    This is a subprogram that creates, displays and interfaces the menus with iXon capabilities.
    \sa ixon.c
    \author Adrien Meglio
*/
#ifndef _MENU_IXON_C
#define _MENU_IXON_C

#include "allegro.h"
#include "winalleg.h"
#include <windowsx.h>
#include "ctype.h"
#include "xvin.h"
#include "ATMCD32D.h"
#include "math.h"
#include "global_define.h"
#include "global_variables.h"
#include "global_functions.h"

//#include "iXon.h"
//#include "iXon_tape.h"

# define BUILDING_PLUGINS_DLL
# include "menu_iXon.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

/** Creates a submenu for ADC readout speed selection.

This function retrieves the number of available ADC readout speeds, compiles a menu
allowing the user to select the one he wants, and sends the information to the appropriate function.
\param None
\return int Error code
\author Adrien Meglio
*/
int create_HSpeed_submenu(void)
{
    char HSpeed_text[128];
    int number, index;
    float speed;
    
    if(updating_menu_state != 0) return D_O_K;
    
    if (SetADChannel(iXon.AD_channel) != DRV_SUCCESS) return win_report("HSpeed menu ERROR (AD channel)");
    if (GetNumberHSSpeeds(iXon.AD_channel,iXon.is_electron_multiplication,&number) != DRV_SUCCESS) return win_report("HSpeed menu ERROR (GetNumberHSSpeeds)");
    
    for (index = 0 ; index <  number  ; index++)
    {
        GetHSSpeed(iXon.AD_channel,iXon.is_electron_multiplication,index,&speed);
        sprintf(HSpeed_text,"Set to %.1f MHz",speed);
        HSpeed_submenu[index].text = strdup(HSpeed_text);
        HSpeed_submenu[index].proc = set_HSpeed_submenu_void;
        HSpeed_submenu[index].child = NULL;
        HSpeed_submenu[index].flags = MENU_INDEX(index);
        HSpeed_submenu[index].dp = NULL;
    }
    HSpeed_submenu[number].text = NULL;
    HSpeed_submenu[number].proc = NULL;
    HSpeed_submenu[number].child = NULL;
    HSpeed_submenu[number].flags = 0;
    HSpeed_submenu[number].dp = NULL;
    
    return D_O_K;
}

/** Creates a submenu for vertical readout speed selection.

This function retrieves the number of available vertical readout speeds (as well as the maximum speed), compiles a menu
allowing the user to select the one he wants, and sends the information to the appropriate function.
\param None
\return int Error code
\author Adrien Meglio
*/
int create_VSpeed_submenu(void)
{
    char VSpeed_text[128];
    int number, index;
    float speed;
    
    if(updating_menu_state != 0) return D_O_K;
    
    if (GetNumberVSSpeeds(&number) != DRV_SUCCESS) return win_report("VSpeed menu ERROR (GetNumberVSSpeeds)");
    
    for (index = 0 ; index <  number  ; index++)
    {
        GetVSSpeed(index,&speed);
        sprintf(VSpeed_text,"Set to %.2f",speed);
        VSpeed_submenu[index].text = strdup(VSpeed_text);
        VSpeed_submenu[index].proc = set_VSpeed_submenu_void;
        VSpeed_submenu[index].child = NULL;
        VSpeed_submenu[index].flags = MENU_INDEX(index);
        VSpeed_submenu[index].dp = NULL;
    }
    VSpeed_submenu[number].text = "Maximum";
    VSpeed_submenu[number].proc = init_VSSpeed_to_max;
    VSpeed_submenu[number].child = NULL;
    VSpeed_submenu[number].flags = 0;
    VSpeed_submenu[number].dp = NULL;
    
    VSpeed_submenu[number+1].text = NULL;
    VSpeed_submenu[number+1].proc = NULL;
    VSpeed_submenu[number+1].child = NULL;
    VSpeed_submenu[number+1].flags = 0;
    VSpeed_submenu[number+1].dp = NULL;
    
    return D_O_K;
}

/** Creates a submenu for EMCCD gain selection.

This function retrieves the gain range, compiles a menu
allowing the user to select the one he wants, and sends the information to the appropriate function.
\param None
\return int Error code
\author Adrien Meglio
*/
int create_gain_submenu(void)
{
    char gain_text[128];
    int number, index;
    
    if(updating_menu_state != 0) return D_O_K;
       
    number = (iXon.gain_max >= 8) ? 8 : iXon.gain_max;
    
    for (index = 0 ; index <=  number  ; index++)
    {
        sprintf(gain_text,"Set to %d",(int)floor((iXon.gain_max+1)*((float)index)/8));
        gain_submenu[index].text = strdup(gain_text);
        gain_submenu[index].proc = set_gain_submenu_void;
        gain_submenu[index].child = NULL;
        gain_submenu[index].flags = MENU_INDEX((int)floor((iXon.gain_max+1)*((float)index)/8));
        gain_submenu[index].dp = NULL;
    }
//    sprintf(gain_text,"Current : %d",iXon.gain);
//    gain_submenu[number+1].text = strdup(gain_text);
//    gain_submenu[number+1].proc = set_gain_submenu_void;
//    gain_submenu[number+1].child = NULL;
//    gain_submenu[number+1].flags = MENU_INDEX(-1);
//    gain_submenu[number+1].dp = NULL;
    
//    sprintf(gain_text,"Max : %d",iXon.gain_max);
//    gain_submenu[number+2].text = strdup(gain_text);
//    gain_submenu[number+2].proc = set_gain_submenu_void;
//    gain_submenu[number+2].child = NULL;
//    gain_submenu[number+2].flags = MENU_INDEX(-1);
//    gain_submenu[number+2].dp = NULL;
    
    return D_O_K;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Menus minifunctions

int set_HSpeed_submenu_void(void)
{
    if(updating_menu_state != 0) return D_O_K;

	SetHSSpeed(iXon.is_electron_multiplication,RETRIEVE_MENU_INDEX);
	GetHSSpeed(iXon.AD_channel,iXon.is_electron_multiplication,RETRIEVE_MENU_INDEX,&iXon.HSSpeed); 
	
	//win_report("ADC readout speed set to %.0f MHz",iXon.HSSpeed);
	
	return D_O_K;
}

int set_VSpeed_submenu_void(void)
{
    if(updating_menu_state != 0) return D_O_K;

	SetVSSpeed(RETRIEVE_MENU_INDEX);
	GetVSSpeed(RETRIEVE_MENU_INDEX,&iXon.VSSpeed); 
	
	//win_report("Vertical readout speed set to %.2f",iXon.VSSpeed);
	
	return D_O_K;
}

int set_temperature_submenu_void(void)
{
  if(updating_menu_state != 0) return D_O_K;
  
  set_temperature(RETRIEVE_MENU_INDEX);
  
  // Back to live
  iXon_real_time_imaging();
  
  return D_O_K;
}

int set_binning_submenu_void(void)
{
    if(updating_menu_state != 0) return D_O_K;

	set_binning(RETRIEVE_MENU_INDEX);
	
	return D_O_K;
}

int set_gain_submenu_void(void)
{
    int iXon_gain_target;
    
    if(updating_menu_state != 0) return D_O_K;

    if (RETRIEVE_MENU_INDEX == -1) return D_O_K;

    // Freezes the display
    Stop_acquisition();
    iXon.is_live = FREEZE;
    Sleep(500);

    // Retrieves the allowed gain range       
    if (GetEMGainRange(&iXon.gain_min,&iXon.gain_max) != DRV_SUCCESS) 
    {
        iXon.is_live = LIVE;
        iXon_real_time_imaging();
        return win_report("Could not retrieve current gain range (in gain submenu)!");
    }
    
    if (RETRIEVE_MENU_INDEX <= iXon.gain_max && RETRIEVE_MENU_INDEX >= iXon.gain_min) iXon_gain_target = RETRIEVE_MENU_INDEX;
    else if (RETRIEVE_MENU_INDEX < iXon.gain_min) iXon_gain_target = iXon.gain_min;
    else if (RETRIEVE_MENU_INDEX > iXon.gain_max) iXon_gain_target = iXon.gain_max;

    //win_report("Target : %d \nMax : %d \nIndex : %d",iXon_gain_target,iXon.gain_max,RETRIEVE_MENU_INDEX);

    // Tries to set gain to the specified value
	if (SetEMCCDGain(iXon_gain_target) != DRV_SUCCESS) 
    {
        iXon.is_live = LIVE;
        iXon_real_time_imaging();
        return win_report("Could not set gain to specified value !");
    }
    
    // Refreshes the global variable iXon if successful
    iXon.gain = iXon_gain_target; 
          
    // Back to live
    iXon.is_live = LIVE;
    iXon_real_time_imaging();
	
	return D_O_K;
}
                           

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Menus

/** Acquisition menu for \a ixon.c */
MENU *ixon_acq_menu(void)
{
	static MENU mn[32];
	
	if (mn[0].text != NULL)	return mn;
	
	add_item_to_menu(mn,"LIVE",iXon_real_time_imaging,NULL,0,NULL);
	add_item_to_menu(mn,"FREEZE",Stop_acquisition,NULL,0,NULL);
	add_item_to_menu(mn,"iXon Movie",iXon_RAM_movie,NULL,0,NULL);
	add_item_to_menu(mn,"Zoom in",zoom_in_iXon,NULL,0,NULL);
	add_item_to_menu(mn,"Zoom out",zoom_out_iXon,NULL,0,NULL);
    add_item_to_menu(mn,"Display period",change_display_refreshing_period,NULL,0,NULL);
	add_item_to_menu(mn,"Clean exit",Exit_iXon,NULL,0,NULL);
    
	//Useless menu : only for shortcuts
    add_item_to_menu(mn,"Set zmin zmax",short__set_z_auto,NULL,0,NULL);
    add_item_to_menu(mn,"Pause",short__pause,NULL,0,NULL);
	
	return mn;
}

/** Image settings menu for \a ixon.c */
MENU *ixon_img_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	
	add_item_to_menu(mn,"Integration time",set_aq_time,NULL,0,NULL);
	add_item_to_menu(mn,"Change gain",change_gain,NULL,0,NULL);
	add_item_to_menu(mn,"Change gain",NULL,gain_submenu,0,NULL);
	add_item_to_menu(mn,"Set binning",NULL,binning_submenu,0,NULL);
	add_item_to_menu(mn,"Set HSpeed",NULL,HSpeed_submenu,0,NULL);
	add_item_to_menu(mn,"Set VSpeed",NULL,VSpeed_submenu,0,NULL);
    
	return mn;
}

MENU binning_submenu[32] =
{
   {"Set to 1x1",  set_binning_submenu_void,NULL,MENU_INDEX(BIN_1_1),  NULL},
   {"Set to 2x2",  set_binning_submenu_void,NULL,MENU_INDEX(BIN_2_2),  NULL},
   {"Set to 4x4",  set_binning_submenu_void,NULL,MENU_INDEX(BIN_4_4),  NULL},
   {"Set to 8x8",  set_binning_submenu_void,NULL,MENU_INDEX(BIN_8_8),  NULL},
   {"Set to 16x16",set_binning_submenu_void,NULL,MENU_INDEX(BIN_16_16),NULL},
   {NULL,NULL,NULL,0,NULL}
};

/** Temperature settings menu for \a ixon.c */
MENU *ixon_temp_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	
    add_item_to_menu(mn,"Set temperature to...",NULL,temperature_submenu,0,NULL);
    add_item_to_menu(mn,"Set temperature",set_temperature_void,NULL,0,NULL);
	add_item_to_menu(mn,"Give the temperature range",give_me_temperature_range_void,NULL,0,NULL);
	
	return mn;
}

MENU temperature_submenu[32] =
{
   {"Set to -70 C",set_temperature_submenu_void,NULL,MENU_INDEX(TEMP_MINUS_70),NULL},
   {"Set to 0 C",  set_temperature_submenu_void,NULL,MENU_INDEX(TEMP_0),       NULL},
   {"Set to +20 C",set_temperature_submenu_void,NULL,MENU_INDEX(TEMP_PLUS_20), NULL},
   {NULL,NULL,NULL,0,NULL}
};
  
/** Main menu for \a ixon.c */     
int menu_ixon_main(void)
{
    create_HSpeed_submenu();
    create_VSpeed_submenu();
    create_gain_submenu();
    
    add_image_treat_menu_item("iXon acquisition menu",NULL,ixon_acq_menu(),0,NULL);
    add_image_treat_menu_item("iXon temperature menu",NULL,ixon_temp_menu(),0,NULL);
    add_image_treat_menu_item("iXon image menu",NULL,ixon_img_menu(),0,NULL);   
	
	return D_O_K;    
}

#endif




