/** \file gui_caps.c
    \brief Extended Allegro GUI capabilities.
    \sa ixon.c
    \author Adrien Meglio
*/
#ifndef _GUI_CAPS_C_
#define _GUI_CAPS_C_

#include "allegro.h"
#include "winalleg.h"
#include <windowsx.h>
#include "box2alfn.h"
#include "xvin.h"
#include "ATMCD32D.h"
#include "global_define.h"
#include "global_variables.h"
#include "global_functions.h"

#define BUILDING_PLUGINS_DLL
#include "gui_caps.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
/** Test for the new bubble function.

\param None
\return int Error code.
\author Adrien Meglio
*/
int bubble_test(void)
{
    if(updating_menu_state != 0) return D_O_K;
    
    /* *  
    int color;
    win_scanf("Couleur : %d",&color);
    win_report("%d, %d, %d",getr(color),getg(color),getb(color));
    /* */
    
    /* *
    int red, green, blue;
    win_scanf("Red : %d Green : %d Blue : %d",&red,&green,&blue);    
    win_report("Color %d",rgb2c(red,green,blue));
    /* */
    
    //special_bubble(200,75,RED,HUGESIZE,"ALERTE !");
    
    //temperature_button(200,75,100,20,RED,BLUE,"Coucou !");
    
    //textout_ex(screen, font, "v4.2.0-beta2", 100, 100,makecol(0, 0, 255), -1);
    
    //textprintf_ex(screen, font, 100, 100, makecol(255, 100, 200),-1,"Score");
     
    /* *   
    BITMAP *temp_button = new_button(2,40,2,"TEMPERATURE : -999 C");
    textprintf_ex(temp_button,font,0,0,GREY_96,-1,"TEMPERATURE : 18 C");
    textprintf_ex(temp_button,font,0,text_height(font),RED,-1,"STABILIZED");
    /* */
                                    
    return D_O_K;
}    

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
  
/** New type of customizable display [generalized bubble] (with Allegro).

\param int \a x_pos The x left position of the display.
\param int \a y_pos The y upper position of the display.
\param int \a n_line The number of font lines to be displayed in the display.
\param char \a example_test A char string with the same size as the longer text in the display.
\return BITMAP The bitmap handle to the bubble display.
\author Adrien Meglio
*/
BITMAP* new_button(int x_pos, int y_pos, int n_lines, char *example_test)
{
    BITMAP* bmp; /* The sub_bitmap of the screen */
    int width; /* Its width */
    int height; /* Its height */
    int x1, y1, x2, y2; /* The coordinates of the bitmap box */
       
    if(updating_menu_state != 0) return bmp;
    
    width = text_length(font,example_test); /* Calculates the width of the sub_bitmap
    according to the size of the example_test */
    height = n_lines*text_height(font); /* Calculates the height according to the number of lines */
    
    bmp = create_sub_bitmap(screen,x_pos,y_pos,width+10,height); /* Creates the bitmap at the given position*/
    get_clip_rect(bmp,&x1,&y1,&x2,&y2); /* Finds the coordinates of the bitmap bounding box */

    rectfill(bmp,x1,y1,x2,y2,GREY_160); /* Fills the bitmap with grey color */
    
    return bmp;

}

/** Synonymous of makecol(red,green,blue).

\param int red The red amplitude (over [0;255]).
\param int green The green amplitude (over [0;255]).
\param int blue The blue amplitude (over [0;255]).
\return int The color in 3-bytes equivalent.
\author Adrien Mglio
 */  
int rgb2c(int red, int green, int blue)
{
    int color;
    
    color = blue + 256*green + 256*256*red;
     
    return color;
}     

/** New type of customizable bubble (with Allegro).

\param int \a x The x left position of the bubble.
\param int \a y The y upper position of the bubble.
\param int \a color The 3-byte color of the text to be displayed in the bubble.
\param int \a size The text height in the bubble.
\param char \a format The text to be displayed.
\return int Error code.
\author Adrien Meglio
*/
int special_bubble(int x,int y,int color,int size,char *format, ...)
{
    int background_color;
    static char *message;
    va_list ap;
    
    int red, green, blue ;
    red = getr(color);
    green = getg(color);
    blue = getb(color);
        
    if (message == NULL)    
    {
        message = (char *)calloc(2048,sizeof(char));
    }
    if (message == NULL)
    {
        return 1;
    }    
    
    va_start(ap,format);
    vsprintf(message,format,ap);
    va_end(ap);        
    
    if (user_font == NULL)        
    {
        ttf_init();
    }    
    background_color = alfont_text_mode(makecol(0,0,0));
    alfont_set_font_size(user_font,size);

    y -= alfont_text_height(user_font)/2;

    /* output in the middle of the screen, upper part, without antialiasing */

    acquire_bitmap(screen);
    
    alfont_textout(screen,user_font,message,x,y,makecol(red,green,blue));
    
    release_bitmap(screen);    
    alfont_text_mode(background_color);
    
    return 0;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

/** Menu function for \a gui_caps.c */
MENU *gui_test_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	
    add_item_to_menu(mn,"Test 1", bubble_test,NULL,0,NULL);
	
    return mn;
}

/** Main function of \&a gui_caps.c */
int	gui_test_main(int argc, char **argv)
{
	add_plot_treat_menu_item ( "gui_test", NULL, gui_test_plot_menu(), 0, NULL);
	return D_O_K;
}

/** Menu unload function for \a gui_caps.c */
int	gui_test_unload(int argc, char **argv)
{
	remove_item_to_menu(plot_treat_menu, "gui_test", NULL, NULL);
	return D_O_K;
}
#endif


