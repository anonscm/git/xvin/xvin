
#ifndef _CALIBRATION_H_
#define _CALIBRATION_H_




typedef struct _calib_param
{
  int nf;                  // the total number of frames requires to perform calibration
  int nstep;               // the number of steps i.e. of lines in the calib image
  int dead;                // the number of frames diregard just after focus has changed
  int nper;                // the number of frames between two focus changes
  int n_zerof;             // the number of frames while the z position is measured 
                           // with bead at F = 0
  int zero_dead;           // the number of frames disregard after F is set to zero
  int *im;                 // the array of images number 
  float zobj_start;        // the starting objective position
  float zobj_step;         // the step size of focus step
  float zmag_cal;          // the z magnet position during calibration
  float zmag_ground;       // the z magnet leading to bead on ground
  float zmag_after;        // the z magnet position after calibration
  float *zo;               // the array of objective position
} calib_param;



PXV_FUNC(int, record_calibration, (void));
PXV_FUNC(int, calibration_oi_idle_action, (struct  one_image *oi, DIALOG *d));



# ifndef _CALIBRATION_C_


# else


# endif

# endif
