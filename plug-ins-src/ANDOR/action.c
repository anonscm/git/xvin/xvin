
#ifndef _ACTION_C_
#define _ACTION_C_

# include "allegro.h"
# include "winalleg.h"

# include "xvin.h"
# include "fftl32n.h"
# include "fillib.h"





/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "../cfg_file/Pico_cfg.h"
#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "focus.h"
# include "magnetscontrol.h"

# include "trackBead.h"
# include "brown_util.h"
# include "track_util.h"
# include "im_fluo.h"
# include "fluo_util.h"
# include "action.h"




int find_next_available_action(void)
{
  int i;

  if (action_pending == NULL) return -1;
  for (i = 0; i < n_action; i++)  
    if (action_pending[i].proceeded == 1) break; // we look for used action to recycle 
  if (i == n_action)
    {
        n_action = (n_action < m_action) ? n_action + 1 : n_action;
    }
  if (i >= m_action) return -2;                 // the buffer is full
  return i;
}


int fill_next_available_action(int im, int type, float value)
{
  int i;

  i = find_next_available_action();
  if (i < 0) return -1;
  action_pending[i].proceeded = 0;
  action_pending[i].imi = im;
  action_pending[i].type = type;
  action_pending[i].value = value;
  return i;
}

int immediate_next_available_action(int type, float value)
{
  register im;

  im = fluo_info->imi[fluo_info->c_i];                                 
  return fill_next_available_action(im+1, type, value);
}

int find_next_action(int imi)
{
  int i, j;

  if (action_pending == NULL) return -1;
  for (i = 0, j = -1; i < n_action; i++)
    {
      if (action_pending[i].proceeded) continue;
      else j = i;
      if (action_pending[i].imi < 0) break;
      if (action_pending[i].imi <= imi) break;
    }
  if (i < n_action) return i;
  else  n_action = j+1;
  return -2;
}

int find_remaining_action(int imi)
{
  int i, j;

  if (action_pending == NULL) return -1;
  for (i = 0, j = 0; i < n_action; i++)
      j += (action_pending[i].proceeded == 0) ? 1 : 0;
  return j;
}

# endif
