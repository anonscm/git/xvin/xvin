/** \file brown.c
    \brief Plug-in program for bead tracking 
    
    This is a subprogram that creates, displays and interfaces the menus with the plug-in functions.
    
    \author V. Croquette, J-F. Allemand
*/

#ifndef _TRACK_UTIL_C_
#define _TRACK_UTIL_C_

# include "allegro.h"
# include "winalleg.h"

# include "xvin.h"
# include "fftl32n.h"
# include "fillib.h"





/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "../cfg_file/Pico_cfg.h"
#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "focus.h"
# include "magnetscontrol.h"
# include "trackBead.h"
# include "brown_util.h"
# include "track_util.h"
# include "action.h"
# include "calibration.h"



MENU bead_active_menu[32];

/** this function initialize the tracking info structure

This function is started as soon as the video acquisition is launch so that we can
check the timing

\param none
\return A tracking info structure.
\version 19/05/06
*/



int timing_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  register int i, j, k;
  static int  immax = -1; // last_c_i = -1,
  d_s *dsx, *dsy, *dsz;
  int nf, last_fr = 0, lost_fr, lf;
  unsigned long   dtm, dtmax, dts;
  float   fdtm, fdtmax;
  long long last_ti = 0;

  if (track_info == NULL) return D_O_K;
  dts = get_my_uclocks_per_sec();
  dsx = find_source_specific_ds_in_op(op,"Timing rolling buffer");
  dsy = find_source_specific_ds_in_op(op,"Period rolling buffer");
  dsz = find_source_specific_ds_in_op(op,"Timer rolling buffer");
  if (dsx == NULL || dsy == NULL || dsz == NULL) return D_O_K;	
  /*
  if (sscanf(dsx->source,"X rolling buffer bead %d",&bead_num) != 1)
    return win_printf_OK("Cannot find bead number");
  */
  //if (track_info->n_b < 1)    return win_printf_OK("No bead to display");
  i = track_info->lc_i;
  if (track_info->limi[i] <= immax) return D_O_K;   // we are up todate
  if (track_info->local_lock & BUF_CHANGING) return D_O_K;   // we have to wait
  track_info->local_lock |= BUF_FREEZED; // we forbid buffer changes
  i = track_info->lc_i;
  immax = track_info->limi[i];               
  lf = immax - track_info->lac_i;
  if (track_info->lac_i > TRACKING_BUFFER_SIZE) 
    {
      nf = TRACKING_BUFFER_SIZE;
      j = track_info->lc_i + 1;  //last_c_i;
    }
  else
    {
      nf = track_info->lac_i-1;
      j = 1;
    }
  dsx->nx = dsx->ny = nf;
  dsy->nx = dsy->ny = nf - 1;
  dsz->nx = dsz->ny = nf;
  

  for (i = 0, lost_fr = 0, dtm = dtmax = 0, fdtm = fdtmax = 0, k = 0; i < nf; i++, j++, k++)
    {
      j = (j < TRACKING_BUFFER_SIZE) ? j : j - TRACKING_BUFFER_SIZE;
      dsx->xd[i] = track_info->limi[j];
      dsx->yd[i] = ((float)(1000*track_info->limdt[j]))/dts; 
      //dsx->yd[i] = track_info->limdt[j]; 
      dsz->xd[i] = 1000*(float)track_info->limt[j]/get_my_ulclocks_per_sec(); 
      dsz->yd[i] = track_info->limit[j];
      if (i > 0) 
	{
	  dsy->xd[i-1] = track_info->limi[j];
	  dsy->yd[i-1] = 1000*(double)(track_info->limt[j]-last_ti)/get_my_ulclocks_per_sec(); 
	  lost_fr += (track_info->limi[j] - last_fr) - 1;
	}
      last_ti = track_info->limt[j];
      last_fr = track_info->limi[j];
      dtm += track_info->limdt[j];
      fdtm += dsx->yd[i];
      dtmax = (track_info->limdt[j] > dtmax) ? track_info->limdt[j] : dtmax;
      fdtmax = (dsx->yd[i] > fdtmax) ? dsx->yd[i] : fdtmax;
    }

  op->need_to_refresh = 1; 
  if (k) set_plot_title(op, "\\stack{{%d fr position lac_i %d stat %d}{%d missed images}"
			"{\\pt8 dt mean %6.3f ms (%6.3f ms) max %6.3f (%6.3f)}"
			"{MY_UCLOCKS_PER_SEC %d 0->%d}}",bid.first_im +track_info->lac_i 
			,immax,0,lf,fdtm/k,
			1000*((double)(dtm/k))/dts,
			fdtmax, 1000*((double)(dtmax))/dts,(int)dts,(int)track_info->limdt[1]);
  track_info->local_lock = BUF_UNLOCK; // we allow new buffer changes

  i = TRACKING_BUFFER_SIZE/4;
  j = ((int)dsx->xd[1])/i;
  op->x_lo = j*i;
  op->x_hi = (j+5)*i;
  set_plot_x_fixed_range(op);
  return refresh_plot(pr_TRACK, UNCHANGED);
}



int z_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  register int i, j, k;
  static int last_c_i = -1, immax = -1;
  d_s *dsx, *dsy, *dsz;
  int nf, last_fr = 0, lost_fr;
  unsigned long  dtm, dtmax;
  float ax, dx, ay, dy, y_over_x, y_over_z;
  un_s *un;

  if (track_info == NULL) return D_O_K;
  un = oi_TRACK->xu[oi_TRACK->c_xu];
  get_afine_param_from_unit(un, &ax, &dx);
  un = oi_TRACK->yu[oi_TRACK->c_yu];
  get_afine_param_from_unit(un, &ay, &dy);
  y_over_x = (dy != 0) ? dx/dy : 1;
  y_over_z = 1e-6;
  y_over_z = (dy != 0) ? y_over_z/dy : 1;

  dsx = find_source_specific_ds_in_op(op,"X rolling buffer");
  dsy = find_source_specific_ds_in_op(op,"Y rolling buffer");
  dsz = find_source_specific_ds_in_op(op,"Z rolling buffer");
  if (dsx == NULL || dsy == NULL || dsz == NULL) return D_O_K;	
  /*
  if (sscanf(dsx->source,"X rolling buffer bead %d",&bead_num) != 1)
    return win_printf_OK("Cannot find bead number");
  */
  if (track_info->n_b < 1)    return D_O_K;	//win_printf_OK("No bead to display");
  i = track_info->c_i;
  if (track_info->imi[i] <= immax) return D_O_K;   // we are uptodate
  immax = track_info->imi[i];               
  nf = (track_info->ac_i > TRACKING_BUFFER_SIZE) ? TRACKING_BUFFER_SIZE : track_info->ac_i;
  nf = (nf > (TRACKING_BUFFER_SIZE>>1)) ? TRACKING_BUFFER_SIZE>>1 : nf;
  dsx->nx = dsx->ny = dsy->nx = dsy->ny = dsz->nx = dsz->ny = nf;
  last_c_i = (track_info->c_i > 0) ? track_info->c_i - 1 : 0;  // c_i is not yet valid
  for (i = nf-1, j = last_c_i, lost_fr = 0, dtm = dtmax = 0, k = 0; i >= 0; i--, j--, k++)
    {
      j = (j < 0) ? TRACKING_BUFFER_SIZE + j: j;
      j = (j < TRACKING_BUFFER_SIZE) ? j : TRACKING_BUFFER_SIZE;
      dsx->xd[i] = dsy->xd[i] = dsz->xd[i] = track_info->imi[j];
      dsx->yd[i] = y_over_x * track_info->bd[track_info->n_bead_display]->x[j];
      dsy->yd[i] = track_info->bd[track_info->n_bead_display]->y[j];
      dsz->yd[i] = y_over_z * track_info->bd[track_info->n_bead_display]->z[j];
      if (i < nf-1) lost_fr += (last_fr - track_info->imi[j]) - 1;
      last_fr = track_info->imi[j];
      dtm += track_info->imdt[j];
      dtmax = (track_info->imdt[j] > dtmax) ? track_info->imdt[j] : dtmax;
    }
  op->need_to_refresh = 1; 
  if (k) set_plot_title(op, "\\stack{{Bead %d, %d missed images}{\\pt8 dt mean %6.3f ms max %6.3f}}",
			track_info->n_bead_display, lost_fr,1000*((double)(dtm/k))/get_my_uclocks_per_sec(),
			1000*((double)(dtmax))/get_my_uclocks_per_sec());
  i = TRACKING_BUFFER_SIZE/8;
  //i = 512;
  j = ((int)dsx->xd[i])/i;
  op->x_lo = (j-1)*i;
  op->x_hi = (j+4)*i;
  set_plot_x_fixed_range(op);
  return refresh_plot(pr_TRACK, UNCHANGED);
}


int obj_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  register int i, j;
  static int last_c_i = -1, immax = -1;
  d_s *dsx, *dsz;
  int nf;
  
  if (track_info == NULL) return D_O_K;

  dsx = find_source_specific_ds_in_op(op,"Obj. rolling buffer");
  dsz = find_source_specific_ds_in_op(op,"Z rolling buffer");
  if (dsx == NULL || dsz == NULL) return D_O_K;	

  if (track_info->n_b < 1)    return D_O_K;	//win_printf_OK("No bead to display");
  i = track_info->c_i;
  if (track_info->imi[i] <= immax) return D_O_K;   // we are uptodate
  immax = track_info->imi[i];               
  nf = (track_info->ac_i > TRACKING_BUFFER_SIZE) ? TRACKING_BUFFER_SIZE : track_info->ac_i;
  nf = (nf > (TRACKING_BUFFER_SIZE>>1)) ? TRACKING_BUFFER_SIZE>>1 : nf;
  dsx->nx = dsx->ny = dsz->nx = dsz->ny = nf;
  last_c_i = (track_info->c_i > 0) ? track_info->c_i - 1 : 0;  // c_i is not yet valid
  for (i = nf-1, j = last_c_i; i >= 0; i--, j--)
    {
      j = (j < 0) ? TRACKING_BUFFER_SIZE + j: j;
      j = (j < TRACKING_BUFFER_SIZE) ? j : TRACKING_BUFFER_SIZE;
      dsx->xd[i] = dsz->xd[i] = track_info->imi[j];
      dsx->yd[i] = track_info->obj_pos[j];
      dsz->yd[i] = track_info->bd[track_info->n_bead_display]->z[j];
    }
  i = TRACKING_BUFFER_SIZE/8;
  j = ((int)dsx->xd[i])/i;
  op->x_lo = (j-1)*i;
  op->x_hi = (j+4)*i;
  set_plot_x_fixed_range(op);

  op->need_to_refresh = 1; 
  set_plot_title(op, "\\stack{{image %d bead servo %d}{Obj start %g obj %g}}"
		 ,track_info->ac_i,  track_info->bead_servo_nb,
  		 track_info->z_obj_servo_start,
  		 track_info->obj_pos[last_c_i]);
  return refresh_plot(pr_TRACK, UNCHANGED);
}

int x_y_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  register int i, j;
  static int last_c_i = -1, immax = -1;
  d_s *dsx;
  int nf;
  b_track *bd;
  
  if (track_info == NULL) return D_O_K;

  dsx = find_source_specific_ds_in_op(op,"X VS Y rolling buffer");
  if (dsx == NULL ) return D_O_K;	

  if (track_info->n_b < 1)    return D_O_K;	//win_printf_OK("No bead to display");
  i = track_info->c_i;
  if (track_info->imi[i] <= immax) return D_O_K;   // we are uptodate
  immax = track_info->imi[i];
  bd = track_info->bd[track_info->n_bead_display];
  nf = ((track_info->ac_i - bd->start_im) > (TRACKING_BUFFER_SIZE>>1)) ? 
    (TRACKING_BUFFER_SIZE>>1) : track_info->ac_i - bd->start_im;
  dsx->nx = dsx->ny = nf;
  last_c_i = (track_info->c_i > 0) ? track_info->c_i - 1 : 0;  // c_i is not yet valid
  for (i = nf-1, j = last_c_i; i >= 0; i--, j--)
    {
      j = (j < 0) ? TRACKING_BUFFER_SIZE + j: j;
      j = (j < TRACKING_BUFFER_SIZE) ? j : TRACKING_BUFFER_SIZE;
      dsx->xd[i] = bd->x[j];
      dsx->yd[i] = bd->y[j];
    }
  op->need_to_refresh = 1; 
  set_plot_title(op, "\\stack{{image %d bead servo %d}{Obj start %g obj %g start %d}}"
		 ,track_info->ac_i,  track_info->bead_servo_nb,
		 track_info->z_obj_servo_start,
  		 track_info->obj_pos[last_c_i],bd->start_im);
  return refresh_plot(pr_TRACK, UNCHANGED);
}

int x_z_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  register int i, j;
  static int last_c_i = -1, immax = -1;
  float x_over_z, ax, dx;
  un_s *un;
  d_s *dsx;
  int nf;
  b_track *bd;
  
  if (track_info == NULL) return D_O_K;
  un = oi_TRACK->xu[oi_TRACK->c_xu];
  get_afine_param_from_unit(un, &ax, &dx);
  x_over_z = 1e-6;
  x_over_z = (dx != 0) ? x_over_z/dx : 1;

  dsx = find_source_specific_ds_in_op(op,"X VS Z rolling buffer");
  if (dsx == NULL ) return D_O_K;	

  if (track_info->n_b < 1)    return D_O_K;	//win_printf_OK("No bead to display");
  i = track_info->c_i;
  if (track_info->imi[i] <= immax) return D_O_K;   // we are uptodate
  immax = track_info->imi[i];
  bd = track_info->bd[track_info->n_bead_display];
  nf = ((track_info->ac_i - bd->start_im) > (TRACKING_BUFFER_SIZE>>1)) ? 
    (TRACKING_BUFFER_SIZE>>1) : track_info->ac_i - bd->start_im;
  dsx->nx = dsx->ny = nf;
  last_c_i = (track_info->c_i > 0) ? track_info->c_i - 1 : 0;  // c_i is not yet valid
  for (i = nf-1, j = last_c_i; i >= 0; i--, j--)
    {
      j = (j < 0) ? TRACKING_BUFFER_SIZE + j: j;
      j = (j < TRACKING_BUFFER_SIZE) ? j : TRACKING_BUFFER_SIZE;
      dsx->xd[i] = bd->x[j];
      dsx->yd[i] = x_over_z * bd->z[j];
    }
  op->need_to_refresh = 1; 
  set_plot_title(op, "\\stack{{image %d bead servo %d}{Obj start %g obj %g start %d}}"
		 ,track_info->ac_i,  track_info->bead_servo_nb,
		 track_info->z_obj_servo_start,
  		 track_info->obj_pos[last_c_i],bd->start_im);
  return refresh_plot(pr_TRACK, UNCHANGED);
}

int y_z_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  register int i, j;
  static int last_c_i = -1, immax = -1;
  float y_over_z, ay, dy;
  un_s *un;
  d_s *dsx;
  int nf;
  b_track *bd;
  
  if (track_info == NULL) return D_O_K;
  un = oi_TRACK->xu[oi_TRACK->c_yu];
  get_afine_param_from_unit(un, &ay, &dy);
  y_over_z = 1e-6;
  y_over_z = (dy != 0) ? y_over_z/dy : 1;

  dsx = find_source_specific_ds_in_op(op,"Y VS Z rolling buffer");
  if (dsx == NULL ) return D_O_K;	

  if (track_info->n_b < 1)    return D_O_K;	//win_printf_OK("No bead to display");
  i = track_info->c_i;
  if (track_info->imi[i] <= immax) return D_O_K;   // we are uptodate
  immax = track_info->imi[i];
  bd = track_info->bd[track_info->n_bead_display];
  nf = ((track_info->ac_i - bd->start_im) > (TRACKING_BUFFER_SIZE>>1)) ? 
    (TRACKING_BUFFER_SIZE>>1) : track_info->ac_i - bd->start_im;
  dsx->nx = dsx->ny = nf;
  last_c_i = (track_info->c_i > 0) ? track_info->c_i - 1 : 0;  // c_i is not yet valid
  for (i = nf-1, j = last_c_i; i >= 0; i--, j--)
    {
      j = (j < 0) ? TRACKING_BUFFER_SIZE + j: j;
      j = (j < TRACKING_BUFFER_SIZE) ? j : TRACKING_BUFFER_SIZE;
      dsx->xd[i] = bd->y[j];
      dsx->yd[i] = y_over_z * bd->z[j];
    }
  op->need_to_refresh = 1; 
  set_plot_title(op, "\\stack{{image %d bead servo %d}{Obj start %g obj %g start %d}}"
		 ,track_info->ac_i,  track_info->bead_servo_nb,
		 track_info->z_obj_servo_start,
  		 track_info->obj_pos[last_c_i],bd->start_im);
  return refresh_plot(pr_TRACK, UNCHANGED);
}


int profile_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  register int i;
  static int last_c_i = -1, immax = -1;
  d_s *ds, *dsr;
  float dz, min, max, tmp;
  int maxi, mini, tmpi;
  b_track *bd;

  if (track_info == NULL) return D_O_K;

  ds = find_source_specific_ds_in_op(op,"Instantaneous radial profile");
  dsr = find_source_specific_ds_in_op(op,"Reference radial profile");
  if (ds == NULL || dsr == NULL) return D_O_K;	

  if (track_info->n_b < 1)    return D_O_K;	//win_printf_OK("No bead to display");
  i = track_info->c_i;
  if (track_info->imi[i] <= immax) return D_O_K;   // we are uptodate
  immax = track_info->imi[i];               
  bd = track_info->bd[track_info->n_bead_display];
  last_c_i = track_info->c_i - 1;
  last_c_i = (last_c_i < 0) ? 4095 : last_c_i;
  for (i = 0; i < track_info->cl && i < ds->nx ; i++)
    {
      ds->xd[i] = dsr->xd[i] = i;
      ds->yd[i] = bd->rad_prof[last_c_i][i];
      dsr->yd[i] = bd->rad_prof_ref[i];
    }



  if (fft_band_pass_in_real_out_complex(dsr->yd, track_info->cl, 0, 
	 track_info->bd[0]->bp_center, track_info->bd[0]->bp_width))
    return win_printf("pb in filtering!"); 

  dz = find_phase_shift_between_profile(dsr->yd, ds->yd, track_info->cl, 0, 
	 bd->bp_center, bd->bp_width, bd->rc);

  for (i = 0, min = 1e34, max = -1; i < track_info->cl && i < ds->nx ; i++)
    {
      ds->xd[i] = dsr->xd[i] = i;
      ds->yd[i] = tmp = bd->rad_prof[last_c_i][i];
      min = (tmp < min) ? tmp : min;
      max = (tmp < max) ? max : tmp;
      dsr->yd[i] = bd->rad_prof_ref[i];
    }
  mini = (int)min;
  maxi = (int)max;
  for (i = 2; i < maxi; i *= 2);
  tmpi = i/16;
  for (i = tmpi; i < maxi; i += tmpi);
  maxi = i+tmpi;
  for (i = maxi; i > mini; i -= tmpi);
  mini = i-tmpi;

  op->x_lo = mini;
  op->y_hi = maxi;
  set_plot_y_fixed_range(op);


  op->need_to_refresh = 1; 
  set_plot_title(op, "\\stack{{Bead %d, image %d bead servo %d}{Obj start %6.3f dz %6.3f obj %6.3f}{calibration image %s %f}}"
		 ,track_info->n_bead_display, track_info->ac_i,  track_info->bead_servo_nb,
		 track_info->z_obj_servo_start,dz, //track_info->bd[0]->z[last_c_i],
		 track_info->obj_pos[last_c_i],(bd->calib_im_fil != NULL)?"yes":"no",(bd->calib_im_fil != NULL)?bd->calib_im_fil->dy:-1);
  return refresh_plot(pr_TRACK, UNCHANGED);
}



int grab_profile_rolling_buffer(void)
{
  register int i, j, k;
  static int last_c_i = -1, immax = -1;
  int nf;
  b_track *bd;
  imreg *imr;
  O_i *oi, *oic = NULL;
  union pix  *pd;

  if(updating_menu_state != 0)	return D_O_K;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)	return OFF;
  
  if (track_info == NULL) return D_O_K;

  if (track_info->n_b < 1)    return D_O_K;	//win_printf_OK("No bead to display");
  i = track_info->c_i;
  if (track_info->imi[i] <= immax) return D_O_K;   // we are uptodate
  immax = track_info->imi[i];               
  bd = track_info->bd[track_info->n_bead_display];
  last_c_i = track_info->c_i - 1;
  last_c_i = (last_c_i < 0) ? TRACKING_BUFFER_SIZE-1 : last_c_i;

  nf = ((track_info->ac_i - bd->start_im) > TRACKING_BUFFER_SIZE) ? 
    TRACKING_BUFFER_SIZE : track_info->ac_i - bd->start_im;


  oic = create_and_attach_oi_to_imr(imr, track_info->cl, nf, IS_FLOAT_IMAGE);
  if (oic == NULL)	return win_printf_OK("cannot create calibration image !");
  
  set_zmin_zmax_values(oic, oi->z_min, oi->z_max);
  set_z_black_z_white_values(oic, oi->z_black, oi->z_white);

  pd = oic->im.pixel;
  for (i = nf-1, j = last_c_i; i >= 0; i--, j--)
    {
      j = (j < 0) ? TRACKING_BUFFER_SIZE + j: j;
      j = (j < TRACKING_BUFFER_SIZE) ? j : TRACKING_BUFFER_SIZE;
      for (k = 0; k < track_info->cl; k++)
	  pd[i].fl[k] = bd->rad_prof[j][k];
    }


  return refresh_image(imr, imr->n_oi - 1);
}

// this running in the timer thread
int proceed_bead(b_track *bt, g_track *gt, BITMAP *imb, O_i *oi)
{
  register int i;
  int xc, yc, cl, cw, cl2, di, tmp = 0;
  int  black_circle, *xi, *yi;
  float dz = 0, dz_1, zobjn, xcf, ycf, dx = 0, dy = 0, corr, *xf, *yf;
  float ax = 1, ay = 1;
  //  int red = makecol(255,0,0);
  // int greeen = makecol(0,255,0);

  cl = gt->cl;                   // cross arm length	
  cl2 = cl/2;
  cw = gt->cw;                   // cross arm width	
  xi = gt->xi;	yi = gt->yi;     // these are temporary int buffers for averaged profile
  xf = gt->xf;	yf = gt->yf;     // these are temporary float buffers for averaged profile
  cl2 = cl/2;
  di = find_dialog_focus(active_dialog);                // I think this does not work
  bt->n_l[gt->c_i] = (char)bt->not_lost;
  black_circle = bt->black_circle;
  if (bt->mouse_draged)
    { xc = bt->mx;		yc = bt->my;  }         // last valid position
  else if (bt->not_lost <= 0)                            // the bead is lost
    { xc = bt->x0;		yc = bt->y0;  }         // last valid position
  else                                              // the bead is not lost
    {	xc = bt->xc;		yc = bt->yc;  } // previous position
  bt->in_image = is_cross_in_image(oi, xc, yc);
  xcf = xc; 	        ycf = yc;
  if (bt->in_image)
    {
      fill_avg_profiles_from_im(oi, xc, yc, cl, cw, xi, yi);
      if (black_circle)	    erase_around_black_circle(xi, yi, cl);
      tmp = check_bead_not_lost(xi, xf, yi, yf, cl, gt->bd_mul);
      if (bt->not_lost <= 0 && tmp > 0)                 // the bead is still lost
	{
	  if (bt->mouse_draged == 0) clip_cross_in_image(oi, bt->xc, bt->yc, cl2);
	  //if (imb) draw_cross_in_screen_unit(bt->xc, bt->yc, cl, cw, red, imb, imr_TRACK->screen_scale);
	  return 1;
	}
      else if (bt->not_lost <= 0 && tmp == 0) 	        // the bead is recovered we reset its state
	bt->not_lost = NB_FRAMES_BEF_LOST;
      else bt->not_lost -= tmp;                         // the bead was not lost
    }
  bt->n_l[gt->c_i] = (char)bt->not_lost;            // we save its present state
  //for (i = 0; i < cl;  i++) bt->rad_prof[gt->c_i][i] = xf[i];
  //for (i = 0; i < cl/2;  i++) bt->rad_prof[gt->c_i][i] = yf[i];
  if (tmp == 0 && bt->in_image)                          // the bead is present for this frame
    {
      //for (i = 0; i < cl;  i++) bt->rad_prof[gt->c_i][i] = xf[i];
      if (bt->mouse_draged == 0 && bt->in_image)
	{
	  dx = find_distance_from_center(xf, cl, 0, gt->dis_filter, !black_circle,&corr);
	  dy = find_distance_from_center(yf, cl, 0, gt->dis_filter, !black_circle,&corr);
	  bt->x[gt->c_i] = xcf = dx + xc;               // we save its new x, y position
	  bt->y[gt->c_i] = ycf = yc + dy;
	  if (bt->start_im <= 0) bt->start_im = gt->ac_i;
	}
      //display_title_message("filter %d cl %d dx %f",gt->dis_filter,cl,dx);
      else 
	{
	  bt->x[gt->c_i] = xcf = bt->mx;               // we save its new x, y position
	  bt->y[gt->c_i] = ycf = bt->my;
	}
      bt->xc = (int)(xcf + .5);	bt->yc = (int)(ycf + .5);

      if (bt->mouse_draged) bt->im_prof_ref = -1; 

      if (bt->mouse_draged == 0 && bt->in_image)
	radial_non_pixel_square_image_sym_profile_in_array_sse(bt->rad_prof[gt->c_i], oi, 
      					     xcf, ycf, cl>>1, ax, ay);



      /*
      if (starting_one == active_dialog && di != index_menu_dialog && do_refresh_overlay == 1)    
	display_title_message("1 status %u dz %06.4f",_statusfp(),dx); 
      */
      if (bt->in_image)
	{
	  if ((gt->bead_servo_nb == (0x100 + gt->c_b)) || (bt->im_prof_ref < 0))
	    {  // we grab the reference profile 
	      //display_title_message("init servo loop");
	      
	      for (i = 0; i < cl;  i++)
		bt->rad_prof_ref_fft[i] = bt->rad_prof_ref[i] = bt->rad_prof[gt->c_i][i];
	      gt->z_obj_servo_start = read_last_Z_value();
	      if (fft_band_pass_in_real_out_complex(bt->rad_prof_ref_fft, cl, 0, bt->bp_center, bt->bp_width))
		return win_printf("pb in filtering!"); 
	      for (i = 0; i < cl;  i++)
		bt->rad_prof_ref_fft[i] = bt->rad_prof_ref[i];
	      i = gt->c_i - 1;
	      i = (i < 0) ?  TRACKING_BUFFER_SIZE + i : i;
	      gt->obj_pos[i] = gt->z_obj_servo_start;
	      if (fft_band_pass_in_real_out_complex(bt->rad_prof_ref_fft, cl, 0, bt->bp_center, bt->bp_width))
		return win_printf("pb in filtering!"); 
	      //keep = bt->rad_prof_ref[15];
	      //keep_1 = bt->rad_prof_ref_fft[15];
	      bt->z[gt->c_i] = 0;
	      if (bt->im_prof_ref < 0)  bt->im_prof_ref = gt->ac_i;
	      else gt->bead_servo_nb = gt->c_b;
	    }
	  
	  for (i = 0; i < cl;  i++)
	    bt->rad_prof_fft[i] = bt->rad_prof[gt->c_i][i];
	  
	  dz = find_phase_shift_between_profile(bt->rad_prof_ref_fft, bt->rad_prof_fft, cl, 0, 
	  				bt->bp_center, bt->bp_width, bt->rc);
	  bt->z[gt->c_i] = dz;
	  if (bt->calib_im_fil != NULL)
	    {
	      for (i = 0; i < cl;  i++)
		bt->rad_prof_fft[i] = bt->rad_prof[gt->c_i][i];
	      i = find_z_profile_by_mean_square_and_phase(bt->rad_prof_fft, cl, bt->calib_im_fil, 0, 
			          bt->bp_center, bt->bp_width, bt->rc, &dz);
	      //dz = (bt->calib_im_fil->dy != 0) ? (dz - bt->calib_im_fil->ay)/ bt->calib_im_fil->dy : dz;
	      bt->z[gt->c_i] = dz;
	    }
	  if ((gt->bead_servo_nb&0xff) == gt->c_b ) // we servo the objective 
	    {
	      
	      i = gt->c_i - 1;
	      i = (i < 0) ?  TRACKING_BUFFER_SIZE + i : i;
	      zobjn = gt->obj_pos[i];
	      dz_1 = bt->z[i];
	      //to move
	      //if (starting_one == active_dialog && di != index_menu_dialog && do_refresh_overlay == 1)    
	      //  display_title_message("cl %d bp %d w %d rc %d zobjn = %06.4f dz %06.4f",cl,bt->bp_center, 
	      //   		  bt->bp_width, bt->rc,zobjn,dz);
	      
	      if (-bt->sat_int < dz && dz < bt->sat_int) bt->integral += bt->integral_gain * dz;
	      if (3*bt->integral >= gt->max_obj_mov) bt->integral = gt->max_obj_mov/3;
	      if (3*bt->integral <= - gt->max_obj_mov) bt->integral = - gt->max_obj_mov/3;
	      
	      zobjn += bt->gain*(dz) + bt->integral + bt->derivative_gain * (dz - dz_1);
	      
	      if (zobjn >= gt->z_obj_servo_start + gt->max_obj_mov) 
		zobjn = gt->z_obj_servo_start + gt->max_obj_mov;
	      if (zobjn <= gt->z_obj_servo_start - gt->max_obj_mov) 
		zobjn = gt->z_obj_servo_start - gt->max_obj_mov;
	      set_Z_value(zobjn);
	      gt->obj_pos_cmd[gt->c_i] =zobjn;
	      gt->obj_pos[gt->c_i] = zobjn;
	    }
	  else 
	    {
	      i = gt->c_i - 1;
	      i = (i < 0) ?  TRACKING_BUFFER_SIZE + i : i;
	      //gt->obj_pos[gt->c_i] = gt->obj_pos_cmd[i];
	      //gt->status_flag[gt->c_i] &= ~OBJ_MOVING;
	    }
	} // if (in_image)
    }
  else                                              // we save its prevoius position
    {
      bt->x[gt->c_i] = bt->x[(gt->c_i) ? gt->c_i-1 : 0];
      bt->y[gt->c_i] = bt->y[(gt->c_i) ? gt->c_i-1 : 0];
    }
  if (bt->mouse_draged == 0 && bt->in_image) 
    clip_cross_in_image(oi, bt->xc, bt->yc, cl2);     // we prevent cross to escape from frame
  //  if (imb) draw_cross_in_screen_unit(bt->xc, bt->yc, cl, cw, (tmp > 0)? red: greeen, imb, imr_TRACK->screen_scale);
  return 0;
}



int	x_imdata_2_imr_nl(imreg *imr, int x, DIALOG *d)
{
	int pw;
	if (imr == NULL || imr->one_i == NULL)	return -xvin_error(Wrong_Argument);
	pw = imr->one_i->im.nxe - imr->one_i->im.nxs;
	if (imr->s_nx == 0 || pw == 0) return 0;
	return (imr->x_off + d->x + ((x - imr->one_i->im.nxs) * imr->s_nx)/pw);
}
int	y_imdata_2_imr_nl(imreg *imr, int y, DIALOG *d)
{
	int ph;
	if (imr == NULL || imr->one_i == NULL)	return -xvin_error(Wrong_Argument);
	ph = imr->one_i->im.nye - imr->one_i->im.nys;		
	if (imr->s_ny == 0 || ph == 0) return 0;	
	return (d->h - imr->y_off  + ((y - imr->one_i->im.nys) * imr->s_ny)/ph); // + 
}


int move_bead_cross(BITMAP *imb, imreg *imr, DIALOG *d, int x0, int y0)
{
  int cl, cw, pxl, pyl, xb, yb;
  b_track *bt = NULL;
  int red = makecol(255,0,0);
  int green = makecol(0,255,0);
  int in_image = 0;

  if (track_info == NULL || imb == NULL || imr == NULL) return 0;
  if (track_info->n_b < 1) return 0;

  for (track_info->c_b = 0; track_info->c_b < track_info->n_b; track_info->c_b++)
    {
      cl = track_info->cl;                   // cross arm length	
      cw = track_info->cw;                   // cross arm width	
      bt = track_info->bd[track_info->c_b];
      /*
      if (bt->not_lost == NB_FRAMES_BEF_LOST)
	draw_cross_in_screen_unit(bt->xc, bt->yc, cl, cw, greeen, imb, imr->screen_scale);
      else 
	{
      */

	  pxl = (int)(0.5+x_imr_2_imdata(imr, x0));    
	  pyl = (int)(0.5+y_imr_2_imdata(imr, y0));    

	  if (bt->mouse_draged == 0
	      && (pxl > bt->xc - cw)
	      && (pxl < bt->xc + cw)
	      && (pyl > bt->yc - cw)
	      && (pyl < bt->yc + cw))
	    {
	      bt->mouse_draged = 1;
	    }
	  if (bt->mouse_draged && mouse_b == 1)   
	    {
	      draw_bubble(screen, B_LEFT, 512, 100, "x %d(%d) y %d(%d)",pxl,bt->xc,pyl,bt->yc);

	      bt->mx = bt->xc = bt->x0 = pxl;
	      bt->my = bt->yc = bt->y0 = pyl;
	    }
	  if (mouse_b == 0)   bt->mouse_draged = 0;
	  xb = x_imdata_2_imr_nl(imr, bt->xc, d);
	  yb = y_imdata_2_imr_nl(imr, bt->yc, d);
	  //draw_bubble(screen, B_LEFT, 100, 100, "x %d(%d) y %d(%d)",xb,bt->xc,yb,bt->yc);
	  in_image = is_cross_in_image(imr->one_i, xb, yb);
	  if (in_image && bt->not_lost > 0 && bt->mouse_draged == 0) 
	    draw_cross_in_screen_unit(xb, yb, cl, cw, green, imb, imr->screen_scale);
	  else draw_cross_in_screen_unit(xb, yb, cl, cw, red, imb, imr->screen_scale); 

	  //}

    }
  return 0;

}



int show_bead_cross(BITMAP *imb, imreg *imr, DIALOG *d)
{
  int cl, cw, pxl, pyl, redraw = 0;
  b_track *bt = NULL;
  int red = makecol(255,0,0);
  int greeen = makecol(0,255,0);

  if (track_info == NULL || imb == NULL || imr == NULL) return 0;
  if (track_info->n_b < 1) return 0;

  for (track_info->c_b = 0; track_info->c_b < track_info->n_b; track_info->c_b++)
    {
      cl = track_info->cl;                   // cross arm length	
      cw = track_info->cw;                   // cross arm width	
      bt = track_info->bd[track_info->c_b];
      if (bt->not_lost > 0) //== NB_FRAMES_BEF_LOST)
	draw_cross_in_screen_unit(bt->xc, bt->yc, cl, cw, greeen, imb, imr->screen_scale);
      else
	{
	  pxl = (int)(0.5+x_imr_2_imdata(imr, mouse_x));    
	  pyl = (int)(0.5+y_imr_2_imdata(imr, mouse_y));    
	  if ((mouse_b & 0x1) 
	      && (pxl > bt->xc - cw)
	      && (pxl < bt->xc + cw)
	      && (pyl > bt->yc - cw)
	      && (pyl < bt->yc + cw))

	    {
	      draw_bubble(screen, B_LEFT, 512, 100, "x %d(%d) y %d(%d)",pxl,bt->xc,pyl,bt->yc);
	      bt->mouse_draged = 1;
	      //	      bt->mx = bt->xc = bt->x0 = pxl;
	      //bt->my = bt->yc = bt->y0 = pyl;
	    }
	  else 
	    {
	      if (mouse_b == 0)		  bt->mouse_draged = 0;
	    }
	  draw_cross_in_screen_unit(bt->xc, bt->yc, cl, cw, red, imb, imr->screen_scale);
	}
      if  (bt->mouse_draged) redraw = 1;
    }
  return redraw;
}



// this running in the timer thread
int track_in_x_y_timer(imreg *imr, O_i *oi, int n,  DIALOG *d, long long t, unsigned long dt, void *p, int n_inarow) 
{
  int cl, cw, cl2, ci, i, ci_1;
  int dis_filter = 0, *xi, *yi;
  float  *xf, *yf;
  BITMAP *imb = NULL;
  g_track *gt = NULL;
  b_track *bt = NULL;
	/* we grab the general structure */
  
  gt = (g_track*)p;

  gt->ac_i++;                     // we increment the image number and wrap it eventually
  //gt->c_i++;                     // we increment the image number and wrap it eventually
  ci_1 = ci = gt->c_i;
  ci++;
  ci = (ci < gt->n_i) ? ci : 0;



  gt->imi[ci] = n;                                 // we save image" number
  gt->imit[ci] = n_inarow;                                 
  gt->imt[ci] = t;                                 // image time
  gt->imdt[ci] = dt;
  gt->c_i = ci;

  if (oi->bmp.stuff != NULL) imb = (BITMAP*)oi->bmp.stuff;
  // we grab the video bitmap of the IFC image
  prepare_image_overlay(oi);

  gt->obj_pos[ci] = read_last_Z_value();	    
  if (gt->n_b > 0)
    {
      cl = gt->cl;                   // cross arm length	
      cl2 = cl/2;
      cw = gt->cw;                   // cross arm width	
      xi = gt->xi;	yi = gt->yi;     // these are temporary int buffers for averaged profile
      xf = gt->xf;	yf = gt->yf;     // these are temporary float buffers for averaged profile
      dis_filter = gt->dis_filter;   // this is the filter description
  

      // loop scanning beads tracked
      for (gt->c_b = 0; gt->c_b < gt->n_b; gt->c_b++)
	{
	  bt = gt->bd[gt->c_b];
	  proceed_bead(bt, gt, imb, oi);
	  bt->z[ci] = gt->obj_pos[ci] - bt->z[ci]; // new
	  // we draw bead in green if not lost in red otherwise
	  //if (gt->c_b == gt->n_b -1) draw_bubble(screen,0,550,50,"%d xc %f yc %f",gt->c_b,xcf,ycf);	    
	}
      /*
      if (gt->obj_pos_cmd[gt->c_i] != gt->obj_pos[gt->c_i])    
	{
	  set_Z_value(gt->obj_pos_cmd[gt->c_i]);
	  gt->status_flag[gt->c_i] |= OBJ_MOVING;
	}
      */
    }

  i = find_remaining_action(n);
  //draw_bubble(screen,0,1000,450,"    %d actions to do     ",i);

  while ((i = find_next_action(n)) >= 0)
    {
      //draw_bubble(screen,0,1000,390,"action ");	    
      if (action_pending[i].type == MV_OBJ_ABS)
	{
	  set_Z_value(action_pending[i].value);
	  gt->status_flag[ci] |= OBJ_MOVING;
	  gt->obj_pos[ci] = action_pending[i].value;
	  //draw_bubble(screen,0,1000,390,"setting abs Z to %f",gt->obj_pos[ci]);	    
	}
      else if (action_pending[i].type == MV_OBJ_REL)
	{
	  gt->obj_pos[ci] = gt->obj_pos[ci] + action_pending[i].value;
	  set_Z_value(gt->obj_pos[ci]);
	  gt->status_flag[ci] |= OBJ_MOVING;
	  //draw_bubble(screen,0,1000,390,"setting rel Z to %f",gt->obj_pos[ci]);	    
	}
      else if (action_pending[i].type == MV_ZMAG_REL)
	{
	  gt->zmag_cmd[ci] = gt->zmag_cmd[ci_1] + action_pending[i].value;
	  set_magnet_z_value(gt->zmag_cmd[ci]);
	  gt->status_flag[ci] |= ZMAG_MOVING;
	  do_update_menu();
	  //draw_bubble(screen,0,1000,390,"setting rel Z to %f",gt->obj_pos[ci]);	    
	}
      else if (action_pending[i].type == MV_ZMAG_ABS)
	{
	  gt->zmag_cmd[ci] = action_pending[i].value;
	  set_magnet_z_value(gt->zmag_cmd[ci]);
	  gt->status_flag[ci] |= ZMAG_MOVING;
	  do_update_menu();
	  //draw_bubble(screen,0,900,390,"setting rel Z to %f",gt->zmag_cmd[ci]);	    
	}
      else if (action_pending[i].type == MV_ROT_REL)
	{
	  gt->rot_mag_cmd[ci] = gt->rot_mag_cmd[ci_1] + action_pending[i].value;
	  set_rot_value(gt->rot_mag_cmd[ci]);
	  gt->status_flag[ci] |= ROT_MOVING;
	  do_update_menu();
	  //draw_bubble(screen,0,1000,390,"setting rel Z to %f",gt->obj_pos[ci]);	    
	}
      else if (action_pending[i].type == MV_ROT_ABS)
	{
	  gt->rot_mag_cmd[ci] = action_pending[i].value;
	  set_rot_value(gt->rot_mag_cmd[ci]);
	  gt->status_flag[ci] |= ROT_MOVING;
	  do_update_menu();
	  //draw_bubble(screen,0,1000,390,"setting rel Z to %f",gt->obj_pos[ci]);	    
	}
      action_pending[i].proceeded = 1;
    }


  gt->zmag[ci] = (gt->status_flag[ci] & ZMAG_MOVING) 
    ? read_magnet_z_value() : what_is_present_z_mag_value(); 
  gt->rot_mag[ci] = (gt->status_flag[ci] & ROT_MOVING) 
    ? read_rot_value() : what_is_present_rot_value();
  
  if (fabs(gt->zmag_cmd[ci] - gt->zmag[ci]) < 0.005)    
      gt->status_flag[ci] &= ~ZMAG_MOVING;
  if (fabs(gt->rot_mag_cmd[ci] - gt->rot_mag[ci]) < 0.005)    
    gt->status_flag[gt->c_i] &= ~ROT_MOVING;



  gt->imdt[ci] = my_uclock() - dt;
  if ((gt->local_lock & BUF_FREEZED) == 0)
    {
      gt->local_lock |= BUF_CHANGING;  // we prevent data changes
      for ( ;gt->lac_i < gt->ac_i; gt->lac_i++)
	{   // we update local buffer
	  i = gt->lac_i%TRACKING_BUFFER_SIZE;
	  gt->limi[i] = gt->imi[i];  
	  gt->limit[i] = gt->imit[i];  
	  gt->limt[i] = gt->imt[i];  
	  gt->limdt[i] = gt->imdt[i];
	  gt->lc_i = i;
	} 
      gt->local_lock = 0;
    }


  // no bead to track

  return 0; 
}
END_OF_FUNCTION(track_in_x_y_timer)


int get_present_image_nb(void)
{
 return (track_info != NULL) ?  track_info->imi[track_info->c_i] : -1;
}                                 


g_track *creating_track_info(void)
{
  O_p *op;
  d_s *ds;

  if (track_info == NULL)
    {
      track_info = (g_track*)calloc(1,sizeof(g_track));
      if (track_info == NULL) return win_printf_ptr("cannot alloc track info!");	
      track_info->cl = -1;	    // so that we know it was not initialized
      track_info->m_b = 16;	    
      track_info->n_b = track_info->c_b = 0;
      track_info->bead_servo_nb = -1;
      track_info->n_bead_display = -1;
      track_info->bd = (b_track**)calloc(track_info->m_b,sizeof(b_track*));
      if (track_info->bd == NULL) return win_printf_ptr("cannot alloc track info!");	
      //starting_one = active_dialog; // to check if windows change
      track_info->user_tracking_action = NULL;

      // plots to check tracking
      op = pr_TRACK->o_p[0];
      set_op_filename(op, "Timing.gr");
      ds = op->dat[0]; // the first data set was already created
      set_ds_source(ds, "Timing rolling buffer");
      if ((ds = create_and_attach_one_ds(op, TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0)) == NULL)
		return win_printf_ptr("I can't create plot !");
      set_ds_source(ds, "Period rolling buffer");
      if ((ds = create_and_attach_one_ds(op, TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0)) == NULL)
		return win_printf_ptr("I can't create plot !");
      set_ds_source(ds, "Timer rolling buffer");
      op->op_idle_action = timing_rolling_buffer_idle_action;

      track_info->m_i = track_info->n_i = TRACKING_BUFFER_SIZE;
      track_info->c_i = track_info->ac_i = track_info->lac_i = track_info->lc_i = 0;
      track_info->local_lock = 0;
      //      LOCK_FUNCTION(track_in_x_y_IFC);
      LOCK_FUNCTION(track_in_x_y_timer);
      LOCK_VARIABLE(track_info);
      bid.param = (void*)track_info;
      bid.to_do = NULL;
      bid.timer_do = track_in_x_y_timer;
    }
  return track_info;
}


int init_track_info(void)
{
  if (track_info == NULL) 
    {
      track_info = creating_track_info();
      m_action = 256;
      n_action = 0;
      action_pending = (f_action*)calloc(m_action,sizeof(f_action));
      if (action_pending == NULL)
	win_printf_OK("Could not allocate action_pendind!");
    }
  return 0;
}



int trajectories_op_idle_action(O_p *op, DIALOG *d)
{
  // we put in this routine all the screen display stuff
  pltreg *pr;

  if (d->dp == NULL)    return 1;
  pr = (pltreg*)d->dp;        /* the image region is here */
  if (pr->one_p == NULL || pr->n_op == 0)        return 1;

  if (op->need_to_refresh)
      refresh_plot(pr_TRACK, UNCHANGED);
  return 0;
}


int x_y_trajectories_job(int im, struct future_job *job)
{
  int imil, ci;
  b_track *bt1 = NULL, *bt2 = NULL;;
  d_s *ds1 = NULL, *ds2 = NULL;


  if (job == NULL || job->op == NULL || job->in_progress == 0) return 0;
  if (im < job->imi)  return 0;
  ci = track_info->c_i;
  imil = track_info->imi[ci];
  //todo = imil - job->in_progress;
  if (track_info->n_b > 0)
    bt1 = track_info->bd[0];
  if (track_info->n_b > 1)
    bt2 = track_info->bd[1];

  if (job->op->n_dat > 0) ds1 = job->op->dat[0];
  if (job->op->n_dat > 1) ds2 = job->op->dat[1];

  while (track_info->imi[ci] > job->in_progress)
    {
      ci--;
      if (ci < 0) ci += TRACKING_BUFFER_SIZE;
    }
  while (track_info->imi[ci] < imil)
    {
      if (bt1 != NULL && ds1 != NULL)
	add_new_point_to_ds(ds1,bt1->x[ci],bt1->y[ci]);
      if (bt2 != NULL && ds2 != NULL)
	add_new_point_to_ds(ds2,bt2->x[ci],bt2->y[ci]);
      
      ci++;
      if (ci >= TRACKING_BUFFER_SIZE) ci -= TRACKING_BUFFER_SIZE;
    }

  job->in_progress = imil;
  job->imi = imil + 25;
  job->op->need_to_refresh = 1;
  return 0;
}

int angle_op_idle_action(O_p *op, DIALOG *d)
{
  // we put in this routine all the screen display stuff
  pltreg *pr;

  if (d->dp == NULL)    return 1;
  pr = (pltreg*)d->dp;        /* the image region is here */
  if (pr->one_p == NULL || pr->n_op == 0)        return 1;

  if (op->need_to_refresh)
      refresh_plot(pr_TRACK, UNCHANGED);
  return 0;
}

//not working yet
int x_y_angle_job(int im, struct future_job *job)
{
  int imil, ci;
  float t, theta;
  static int angle_loops=0;
  static float oldtheta;
  b_track *bt1 = NULL, *bt2 = NULL;
  d_s *ds = NULL;

  if (job == NULL || job->op == NULL || job->in_progress == 0) return 0;
  if (im < job->imi)  return 0;
  ci = track_info->c_i;
  imil = track_info->imi[ci];
  //todo = imil - job->in_progress;
  if (track_info->n_b > 0)
    bt1 = track_info->bd[0];
  if (track_info->n_b > 1)
    bt2 = track_info->bd[1];

  if (job->op->n_dat > 0) ds = job->op->dat[0];

  while (track_info->imi[ci] > job->in_progress)
    {
      ci--;
      if (ci < 0) {ci += TRACKING_BUFFER_SIZE;}
    }
  while (track_info->imi[ci] < imil)
    {
      if (bt1 != NULL && bt2 != NULL && ds != NULL){
	theta = (float) atan2(bt1->y[ci] - bt2->y[ci],bt1->x[ci] - bt2->x[ci]);
	if((theta - oldtheta) > PI){
	  angle_loops ++;
	}
	else if((theta - oldtheta) < -PI){
	  angle_loops--;
	}
	t = theta + angle_loops*2*PI;
	add_new_point_to_ds(ds , track_info->imi[ci] , t);
	oldtheta = theta;
	ci++;
	if (ci >= TRACKING_BUFFER_SIZE){ ci -= TRACKING_BUFFER_SIZE;}
      }
    }
  
  job->in_progress = imil;
  job->imi = imil + 25;
  job->op->need_to_refresh = 1;
  return 0;
}


int stop_record_x_y_trajectories(void)
{
   pltreg *pr;
   O_p *op;
   f_job* job;

   if(updating_menu_state != 0)	return D_O_K;

   if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return D_O_K;

   job = find_job_associated_to_plot(op);
   if (job)       job->in_progress = 0;
   return D_O_K;
}

int record_x_y_trajectories(void)
{
   int im;
   imreg *imr;
   O_i *oi, *oic = NULL;
   O_p *op;
   d_s *ds;

   if(updating_menu_state != 0)	return D_O_K;

   if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)	return OFF;



   op = create_and_attach_one_plot(pr_TRACK,  16, 16, 0);
   ds = op->dat[0];
   ds->nx = ds->ny = 0;
   set_ds_source(ds, "XY recording of bead 1");
   if ((ds = create_and_attach_one_ds(op, 16, 16, 0)) == NULL)
     return win_printf_OK("I can't create plot !");
   set_ds_source(ds, "XY recording of bead 2");
   ds->nx = ds->ny = 0;
   
   im = track_info->imi[track_info->c_i];                                 
   
   fill_next_available_job_spot(im, im + 25, COLLECT_DATA, 0, imr, oic, pr_TRACK, op, NULL, x_y_trajectories_job, 0);
   op->op_idle_action = trajectories_op_idle_action;
   return D_O_K;
}

int stop_record_x_y_angle(void)
{
   pltreg *pr;
   O_p *op;
   f_job* job;

   if(updating_menu_state != 0)	return D_O_K;

   if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return D_O_K;

   job = find_job_associated_to_plot(op);
   if (job)       job->in_progress = 0;
   return D_O_K;
}

int record_x_y_angle(void)
{
   int im;
   imreg *imr;
   O_i *oi, *oic = NULL;
   O_p *op;
   d_s *ds;

   if(updating_menu_state != 0)	return D_O_K;

   if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)	return OFF;

   op = create_and_attach_one_plot(pr_TRACK,  16, 16, 0);
   ds = op->dat[0];
   ds->nx = ds->ny = 0;
   set_ds_source(ds, "XY recording of angle");

   im = track_info->imi[track_info->c_i];                                 

   fill_next_available_job_spot(im, im + 25, COLLECT_DATA, 0, imr, oic, pr_TRACK, op, NULL, x_y_angle_job, 0);
   op->op_idle_action = angle_op_idle_action;
 return D_O_K;
}



int switch_bead_of_interest(void)
{
  int tmp = 0;

  if (active_menu->text == NULL) return D_O_K;	
  sscanf((char*)active_menu->text,"Bead %d",&tmp); // != 1) return D_O_K;	
  if(updating_menu_state != 0)	
    {
      if (tmp == track_info->n_bead_display)
	active_menu->flags |=  D_SELECTED;
      else active_menu->flags &= ~D_SELECTED;			
      return D_O_K;	
    }

  if (tmp >= track_info->n_b || tmp < 0) return D_O_K;	
  track_info->n_bead_display = tmp;    
  return D_O_K;
}

int prepare_bead_menu(void)
{
  char st[128];
  register int i;
  for (i = 0; i < track_info->n_b; i++)
    { 
      if (bead_active_menu[i].text)
	{
	  free(bead_active_menu[i].text);
	  bead_active_menu[i].text = NULL;
	}
      sprintf(st,"Bead %d",i);
      bead_active_menu[i].text = strdup(st); 
      bead_active_menu[i].proc = switch_bead_of_interest;

    }
  if (bead_active_menu[i].text)
      free(bead_active_menu[i].text);
  bead_active_menu[i].text = NULL;
  return 0;
}

int follow_bead_in_x_y(void)
{
  register int i, di;
  imreg *imrs;	
  O_p *op;
  d_s *ds;
  O_i *ois, *oi = NULL;
  int xc = 256, yc = 256, color, bnx = 0, bcw = 0, dis_state;
  static int cl = 128, cw = 16, nf = 2048, imstart = 10, prof = 0, cl_size_selected = 0;
  b_track *bd = NULL;
  int mode = 0;
  float xb = 0, yb = 0, zb = 0, zobj;
  DIALOG *d;
	
  if(updating_menu_state != 0)	return D_O_K;
  di = find_dialog_focus(active_dialog);	
  mode = RETRIEVE_MENU_INDEX;
  if (ac_grep(NULL,"%im%oi",&imrs,&ois) != 2)	
    return win_printf_OK("cannot find image in acreg!");

  //if (ois != oi_IFC) return win_printf_OK("this is not the IFC image!");
  
  dis_state = do_refresh_overlay;   do_refresh_overlay = 0;
  //freeze_video();
  color = makecol(255,64,64);
  /* check for menu in win_scanf */
  if (mode == IMAGE_CAL)
    {
      //freeze_video();
      oi = do_load_calibration(0);
      if (grab_bead_info_from_calibration(oi, &xb, &yb, &zb, &bnx, &bcw))
	win_printf("error in retrieving bead data");
      cl = bnx;
      cw = bcw;
      //live_video(0);
    }
  else if (cl_size_selected == 0)
    {
      i = win_scanf("this routine track a bead in X,Y\n"
		    " using a cross shaped pattern \n"
		    "arm length %3d arm width %3d\n"
		    "how many frames %6d imstart %2d\n"
		    "profile %b\nkey {\\it b} black circle {\\it w} to stop\n"
		    ,&cl,&cw,&nf,&imstart,&prof); // %m ,select_power_of_2() 
      if (i == CANCEL)	return OFF; 
      cl_size_selected = 1;
    }
  if (cl > 128)	return win_printf_OK("cannot use arm bigger\nthan 128");
  if (cw > 32)	return win_printf_OK("cannot use width bigger\nthan 32");
  
  if (fft_init(cl) || filter_init(cl))	return win_printf_OK("cannot init fft!");	
  do_refresh_overlay = dis_state; 
  
  //live_video(0);
  xc = yc = 0;
  if ( mode == IMAGE_CAL)
    {
      xc = (int)(xb - oi->ax)/oi->dx;
      yc = (int)(yb - oi->ay)/oi->dy;
    }
  else
    {
      /*
      for(change_mouse_to_cross(cl, cw); !key[KEY_ENTER]; )
	{	
	  xc = mouse_x;
	  yc = mouse_y;
	  //d_draw_Im_proc(MSG_IDLE,d_TRACK,0);
	}
      reset_mouse();
      xc = (int)x_imr_2_imdata(imrs, xc);	
      yc = (int)y_imr_2_imdata(imrs, yc);	
      clear_keybuf();
      */
      xc = ois->im.nx + 2*cl/3;
      yc = cl/2 + track_info->n_b*cl;
      ois->need_to_refresh |= BITMAP_NEED_REFRESH;
    }					
  
  if (track_info == NULL) track_info = creating_track_info();
  if (track_info == NULL) return win_printf_OK("cannot alloc track info!");	
  if (track_info->cl == -1)	    
    {
      // we define tracking parameters
      track_info->cl = cl;	    
      track_info->cw = cw;	    
      track_info->bd_mul = 16;	    
      track_info->dis_filter = cl>>2;
      track_info->m_b = 16;	    
      track_info->n_b = track_info->c_b = 0;
      track_info->bead_servo_nb = -1;
      //starting_one = active_dialog; // to check if windows change
      //index_menu_dialog = retrieve_item_from_dialog(d_menu_proc, NULL);
      //index_menu_dialog = 1;
      track_info->user_tracking_action = NULL;

      // second plots to check tracking
      op = create_and_attach_one_plot(pr_TRACK,  TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0);
      ds = op->dat[0];
      set_ds_source(ds, "X rolling buffer");
      if ((ds = create_and_attach_one_ds(op, TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0)) == NULL)
		return win_printf_OK("I can't create plot !");
      set_ds_source(ds, "Y rolling buffer");
      if ((ds = create_and_attach_one_ds(op, TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0)) == NULL)
		return win_printf_OK("I can't create plot !");
      set_ds_source(ds, "Z rolling buffer");
      uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);

      op->op_idle_action = z_rolling_buffer_idle_action;
      set_op_filename(op, "X(t)Y(t)Z(t).gr");

      // third plot
      //win_printf("index %d %d",index_menu_dialog,di);
      op = create_and_attach_one_plot(pr_TRACK, track_info->cl, track_info->cl, 0);
      ds = op->dat[0];
      set_ds_source(ds, "Instantaneous radial profile");
      if ((ds = create_and_attach_one_ds(op,  track_info->cl, track_info->cl, 0)) == NULL)
		return win_printf_OK("I can't create plot !");
      set_ds_source(ds, "Reference radial profile");
      op->op_idle_action = profile_rolling_buffer_idle_action;
      set_op_filename(op, "Profiles.gr");
      // fouth plot

      op = create_and_attach_one_plot(pr_TRACK,  TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0);
      ds = op->dat[0];
      set_ds_source(ds, "Obj. rolling buffer");
      if ((ds = create_and_attach_one_ds(op, TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0)) == NULL)
		return win_printf_OK("I can't create plot !");
      set_ds_source(ds, "Z rolling buffer");
      op->op_idle_action = obj_rolling_buffer_idle_action;
      set_op_filename(op, "Objective.gr");
      //fifth plot
      op = create_and_attach_one_plot(pr_TRACK,  TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0);
      ds = op->dat[0];
      set_ds_source(ds, "X VS Y rolling buffer");
      op->op_idle_action = x_y_rolling_buffer_idle_action;
      uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
      uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
      set_op_filename(op, "X VS Y.gr");

      op = create_and_attach_one_plot(pr_TRACK,  TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0);
      ds = op->dat[0];
      set_ds_source(ds, "X VS Z rolling buffer");
      op->op_idle_action = x_z_rolling_buffer_idle_action;
      uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
      uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_Y_UNIT_SET);
      set_op_filename(op, "X VS Z.gr");

      op = create_and_attach_one_plot(pr_TRACK,  TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0);
      ds = op->dat[0];
      set_ds_source(ds, "Y VS Z rolling buffer");
      op->op_idle_action = y_z_rolling_buffer_idle_action;
      uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_X_UNIT_SET);
      uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
      set_op_filename(op, "Y VS Z.gr");


      for (i = 0, zobj = read_Z_value_accurate_in_micron(); i < TRACKING_BUFFER_SIZE; i++)
	track_info->obj_pos_cmd[i] = zobj;

      track_info->m_i = track_info->n_i = TRACKING_BUFFER_SIZE;
      //track_info->c_i = 0;
      //LOCK_FUNCTION(track_in_x_y_IFC);
      //LOCK_FUNCTION(track_in_x_y_timer);
      //LOCK_VARIABLE(track_info);
    }
  else
    {
      if (track_info->cl != cl || track_info->cw != cw)
	{
	  win_printf("Your arms do not have the right size!\n");
	  if (oi) free_one_image(oi);
	  return D_O_K;
	}	    
    }
  bd = (b_track*)calloc(1,sizeof(b_track));
  if (bd == NULL) return win_printf_OK("cannot alloc bead info!");	
  if (track_info->n_b >= track_info->m_b)
    {
      track_info->m_b += 16;	    
      track_info->bd = (b_track**)realloc(track_info->bd,track_info->m_b*sizeof(b_track*));
      if (track_info->bd == NULL) return win_printf_OK("cannot alloc track info!");	
    }
  for (i = 0; i < track_info->cl; i++) bd->rad_prof_ref[i] = -1; // cannot be a profile
  
  if (mode == IMAGE_CAL)
    {
      bd->calib_im = oi;
      bd->calib_im_fil = NULL;
    }
  else 
    {
      bd->calib_im = NULL;
      bd->calib_im_fil = NULL;
      bd->not_lost = -1;
    }
  bd->bp_center = 16;
  bd->bp_width = 12;
  bd->rc = 12;
  track_info->n_bead_display = track_info->n_b;    
  track_info->bd[track_info->n_b++] = bd;
  prepare_bead_menu();
  // win_printf("bead added %d",track_info->n_b);
  LOCK_VARIABLE(bd);
  bd->xc = bd->x0 = xc;
  bd->yc = bd->y0 = yc;
  bd->im_prof_ref = -1;
  bd->mouse_draged = 0;
  bd->not_lost = NB_FRAMES_BEF_LOST;
  bd->start_im = 0;
  bid.param = (void*)track_info;
  //  bid.to_do = track_in_x_y_IFC;
  bid.timer_do = track_in_x_y_timer;

  d = find_dialog_associated_to_imr(imrs, NULL);
  write_and_blit_im_box( plt_buffer, d, imrs);
  move_bead_cross(screen, imrs, d, xc, yc);

  return 0;
}



O_i	*do_load_calibration(int n)
{
  register int i;
  char path[512], file[256], name[128];
  static char fullfile[512], *fu = NULL;
  O_i *oi = NULL;
  
  
  if (fu == NULL)
    {
      sprintf (name,"last_calibration_%d",n);
      fu = (char*)get_config_string("IMAGE-GR-FILE",name,NULL);
      if (fu != NULL)	
	{
	  strcpy(fullfile,fu);
	  fu = fullfile;
	}
      else
	{
	  my_getcwd(fullfile, 512);
	  strcat(fullfile,"\\");
	}
    }
#ifdef XV_WIN32
  if (full_screen)
    {
#endif
      switch_allegro_font(1);
      i = file_select_ex("Load calibration image (*.gr)", fullfile, "gr", 512, 0, 0);
      switch_allegro_font(0);
#ifdef XV_WIN32
    }
  else
    {
      extract_file_path(path, 512, fullfile);
      put_backslash(path);
      if (extract_file_name(file, 256, fullfile) == NULL)
	fullfile[0] = file[0] = 0;
      else strcpy(fullfile,file);
      fu = DoFileOpen("Load calibration image (*.gr)", path, fullfile, 512, "Image Files (*.gr)\0*.gr\0All Files (*.*)\0*.*\0\0", "gr");
      i = (fu == NULL) ? 0 : 1;
      
    }
#endif
  
  if (i != 0) 	
    {
      fu = fullfile;
      set_config_string("IMAGE-GR-FILE",name,fu);
      extract_file_name(file, 256, fullfile);
      extract_file_path(path, 512, fullfile);
      strcat(path,"\\");
      oi = load_im_file_in_oi(file, path);
      if (oi == NULL)
	{   
	  win_printf("could not load image\n%s\n"
		     "from path %s",file, backslash_to_slash(path));
	  return NULL;
	}
      i++;
    }
  return oi;
}

int grab_bead_info_from_calibration(O_i *oi, float *xb, float *yb, float *zb, int *nx, int *cw)
{
  char *co, *cs;
  
  if (oi == NULL) return 1;
  co = cs = (oi->im.source != NULL) ? oi->im.source : oi->im.treatement; 
  if (cs == NULL)
    {
      win_printf("wrong souce!");	
      return 1;
    }
  if (strncmp(cs,"equally spaced reference profile",32) != 0)
    win_printf("image must be :\\sl  equally spaced reference profile!");
  cs = strstr(co,"Bead xcb");
  if (cs != NULL && xb != NULL)
    {
      if (sscanf(cs+9,"%f",xb) != 1)
	win_printf("cannot read bead xc!");
    }
  cs = strstr(cs,"ycb");
  if (cs != NULL && yb != NULL)
    {
      if (sscanf(cs+3,"%f",yb) != 1)
	win_printf("cannot read bead yc!");
    }
  cs = strstr(cs,"zcb");
  if (cs != NULL && zb != NULL)
    {
      if (sscanf(cs+3,"%f",zb) != 1)
	win_printf("cannot read bead zc!");
    }	
  cs = strstr(cs,"nxb");
  if (cs != NULL && nx != NULL)
    {
      if (sscanf(cs+3,"%d",nx) != 1)
	win_printf("cannot read bead ncd!");
    }	
  cs = strstr(cs,"cwb");
  if (cs != NULL && cw != NULL)
    {
      if (sscanf(cs+3,"%d",cw) != 1)
	win_printf("cannot read bead cwd!");
    }
  return 0;
}



int stop_spectrum_profile(void)
{

  return D_O_K;
}
int spectrum_profile(void)
{
  int spectrum_idle_action(O_p *op, DIALOG *d);
   char  *xun, *yun;
   int x, y, xw, yw, np, ndi;
  int old_x = 0, old_y, nx, color,x_0,y_0;
  float tmpx, tmpy;//,  px0, py0, px1, py1;
  
  O_i *oi = NULL;
  O_p *op = NULL;
  d_s *ds = NULL;
  imreg *imr = NULL;
  
  if(updating_menu_state != 0)	return D_O_K;
  
  imr = imr_TRACK;
  oi = oi_TRACK;
  nx = oi->im.nx;
  
  np = 1 + (int)sqrt(oi->im.nx * oi->im.nx + oi->im.ny * oi->im.ny);    
  
 
  op = create_and_attach_one_plot(pr_TRACK,  np , np , 0);
  
  ds = op->dat[0];
  set_ds_source(ds, "Spectrum on CCD camera");
  
  
  color = makecol(255, 64, 64);
  ndi = find_imr_index_in_current_dialog(NULL);    
 
  if (ndi < 0) return win_printf_OK("Could not find image dialog");            
  
  while (!(mouse_b & 0x3));
  x_0 = old_x = x = mouse_x; 
  y_0 = old_y = y = mouse_y;
  px1 = px0 = x_imr_2_imdata(imr, mouse_x);    
  py1 = py0 = y_imr_2_imdata(imr, mouse_y);    
  
  while (mouse_b & 0x3)
    {
      xw = x = mouse_x  - imr->x_off;
      yw = y = imr->y_off - mouse_y;
      if (x  < 0)         xw = 0;
      if (x > imr->s_nx)     xw = imr->s_nx;
      if (y  < 0)         yw = 0; 
      if (y > imr->s_ny)    yw = imr->s_ny; 
      if ( x != xw || y != yw)
        {
	  x = xw; y = yw;
	  xw += imr->x_off;
	  yw = imr->y_off - yw;
	  mouse_x = xw;
	  mouse_y = yw;
        }    
      if(x != old_x || x != old_y)
        {
	  px1 = x_imr_2_imdata(imr, mouse_x);    
	  py1 = y_imr_2_imdata(imr, mouse_y);
	  
	  tmpx = oi->dx * (px1 - px0);
	  tmpy = oi->dy * (py1 - py0);
	  xun = (oi->x_unit != NULL) ? oi->x_unit : " ";
	  yun = (oi->y_unit != NULL) ? oi->y_unit : " ";
	  
	  
	  oi->need_to_refresh |= PLOTS_NEED_REFRESH;
	  do_one_image (imr);
	  oi->need_to_refresh &= BITMAP_NEED_REFRESH;
	  write_and_blit_im_box(plt_buffer, active_dialog + ndi, imr);                
	  draw_bubble(screen, B_CENTER, mouse_x, mouse_y-20,
	                  "px0 %d py0 %d px1 %d py1 %d",px0,py0,px1,py1);
	  //    "w = %g %s h = %g %s",tmpx,xun,tmpy,yun);
	  
	  line(screen,x_0,y_0,mouse_x,mouse_y,color);                
	  old_x = x;
	  old_y = y;
	  
        }    
    }
  op->op_idle_action = spectrum_idle_action;
  set_op_filename(op, "CCDspectrum.gr");
  //set_formated_string (&(ds->source), "Tilted profile of image %s %s\n"
  //		       "starting at (%d,%d) ending at (%d,%d)", oi->filename, oi->dir,
  //		       px0,py0,px1,py1);
  return D_O_K;
}

int spectrum_idle_action(O_p *op, DIALOG *d)
{
  register int i;
  //static int last_c_i = -1, immax = -1;
  d_s *ds;
  float  *zt = NULL;
  
  // b_track *bd;
  
  if (track_info == NULL) return D_O_K;

  ds = find_source_specific_ds_in_op(op,"Spectrum on CCD camera");
  if (ds == NULL ) return D_O_K;
  
  if (IS_DATA_IN_USE(oi_TRACK)) return 1;
  zt = extract_raw_tilted_profile(oi_TRACK, ds->yd, ds->mx, px0, py0, 
				  px1, py1, &(ds->nx));//passer les parametres
  if (zt != NULL)
    {
      for (i = 0; i < ds->nx && i < ds->mx; i++) ds->xd[i] = i;         
      ds->yd = zt;
      ds->ny = ds->nx;
    }	
  op->need_to_refresh = 1; 
  set_plot_title(op, "Spectrum image %d " ,track_info->ac_i);
  return refresh_plot(pr_TRACK, UNCHANGED);
}

int stop_spectrum_idle_action(void)
{
  int n_op;
  if(updating_menu_state != 0)	return D_O_K;
  n_op = find_op_nb_of_source_specific_ds_in_pr(pr_TRACK, "Spectrum on CCD camera");
  
  (pr_TRACK->o_p[n_op])->op_idle_action = NULL;
    
  return D_O_K;
}

int reset_mouse_rect(void)
{
    scare_mouse();    
    set_mouse_sprite(NULL);
    set_mouse_sprite_focus(0, 0);
    unscare_mouse();
    return 0;
}

int change_mouse_to_rect(int cl, int cw)
{
    BITMAP *ds_bitmap = NULL;
    int color, col;


    ds_bitmap = create_bitmap(cl+1,cw+1);
    col = Lightmagenta;
    color = makecol(EXTRACT_R(col), EXTRACT_G(col), EXTRACT_B(col));
    clear_to_color(ds_bitmap, color);

    color = makecol(255, 0, 0);                        

    line(ds_bitmap,0,0,cl,0,color);
    line(ds_bitmap,0,0,0,cw,color);
    line(ds_bitmap,0,cw,cl,cw,color);
    line(ds_bitmap,cl,0,cl,cw,color);
    scare_mouse();    
    set_mouse_sprite(ds_bitmap);
    set_mouse_sprite_focus(cl/2, cw/2);
    unscare_mouse();
    return 0;    
}

int intensity_vs_time(void)
{
  int collect_intensity(imreg *imr, O_i *oi, int n, DIALOG *d, long long t, unsigned long dt, void *p, int n_inarow);
  int intensity_vs_time_idle_action(O_p *op, DIALOG *d);
  O_i *oi = NULL;
  O_p *op = NULL;
  d_s *ds = NULL;
  imreg *imr = NULL;
  int pix_size = 4;
  int *position = NULL;

  if(updating_menu_state != 0)	return D_O_K;
  
  imr = imr_TRACK;
  oi = oi_TRACK;
  op = create_and_attach_one_plot(pr_TRACK,  TRACKING_BUFFER_SIZE , TRACKING_BUFFER_SIZE , 0);
  win_scanf("You want to average on how many pixels?%d",&pix_size);
  change_mouse_to_rect(pix_size, pix_size);
  op = create_and_attach_one_plot(pr_TRACK,  TRACKING_BUFFER_SIZE , TRACKING_BUFFER_SIZE , 0);
  ds = op->dat[0];
  set_ds_source(ds, "Intensity vs time");
  set_op_filename(op, "Intensity.gr");
  
  while (!(mouse_b & 0x3));
  position = (int *)calloc(3,sizeof(int));
  position[0] = pix_size;
  position[1] = x_imr_2_imdata(imr, mouse_x);    
  position[2] = y_imr_2_imdata(imr, mouse_y); 

  reset_mouse_rect();

  if (bid.timer_do == NULL) return win_printf_OK("Bid NULL");
  
  if (bid.next == NULL)
    {
      bid.next = (Bid *)calloc(1, sizeof(Bid));
      bid.next->param = (void *)position;
      bid.next->timer_do = collect_intensity;
    }
  
  op->op_idle_action = intensity_vs_time_idle_action;
  return D_O_K;
}

int collect_intensity(imreg *imr, O_i *oi, int n, DIALOG *d, long long t, unsigned long dt, void *p, int n_inarow)
{
  int x_pos_center;
  int y_pos_center;
  int pix_size = 4;
  float average = 0;
  int l,k;
  int *j = NULL;
  
  j=(int *)bid.next->param;
  pix_size = j[0];  
  x_pos_center = j[1];
  y_pos_center = j[2];
  
  for( k = 0; k < pix_size ; k++ )
    {
      for (l = 0; l < pix_size ; l++)
	{
 	  average += (float)oi_TRACK->im.pixel[y_pos_center+k-(int)(pix_size/2)].ch[x_pos_center+l-(int)(pix_size/2)]; 
	}
    }
  intensity[track_info->c_i%TRACKING_BUFFER_SIZE] = average/pix_size/pix_size;
  return D_O_K;
}


int intensity_vs_time_idle_action(O_p *op, DIALOG *d)
{
  int i;
  
  d_s *ds = NULL;

    
  if (track_info == NULL) return D_O_K;

  ds = find_source_specific_ds_in_op(op,"Intensity vs time");
  if (ds == NULL ) return D_O_K;
  for (i = 0 ; i < TRACKING_BUFFER_SIZE ; i++)
    {
      ds->xd[i] = i;
      ds->yd[i] = intensity[i];
    }
  set_plot_title(op, "\\stack{{Intensity vs time image %d}{x %d y %d}}",track_info->c_i,*((int*)bid.next->param),*((int*)bid.next->param+1));
  op->need_to_refresh = 1; 
  return refresh_plot(pr_TRACK, UNCHANGED);

}

int stop_intensity_vs_time_idle_action(void)
{
  int n_op;
  if(updating_menu_state != 0)	return D_O_K;
  n_op = find_op_nb_of_source_specific_ds_in_pr(pr_TRACK, "Intensity vs time");
  //bid.next = NULL;
  //bid.next->param = NULL;
  (pr_TRACK->o_p[n_op])->op_idle_action = NULL;
  bid.next = NULL;
  return D_O_K;
}


int save_finite_movie_in_thread(imreg *imr, O_i *oi, int n, DIALOG *d, long long t, unsigned long dt, void *p, int n_inarow)
{
  static int i=0;
  O_i *oi_movie = (O_i*)p;

  if (i < oi->im.n_f)
    {
      memcpy(oi_movie->im.mem[i],oi_TRACK->im.mem[track_info->imi[track_info->c_i]%oi_TRACK->im.n_f],oi_TRACK->im.nx*oi_TRACK->im.ny*((oi_TRACK->im.data_type == IS_CHAR_IMAGE)?1:2)); 
      i++;
    }
  else bid.next = NULL;

  return D_O_K;
}
int save_finite_movie(void)
{
  static int n_images_to_save = 256;
  O_i *oi = NULL;

  if(updating_menu_state != 0)	return D_O_K;
  win_scanf("You want to save how many images?%d\n Think about RAM size...\n Only for CHAR for the moment \nRecording starts when mouse is pressed",&n_images_to_save);
  
  //  oi = create_one_movie(oi_TRACK->im.nx,oi_TRACK->im.nx, IS_CHAR_IMAGE , n_images_to_save);
  oi = create_and_attach_oi_to_imr (imr_TRACK, oi_TRACK->im.nx,oi_TRACK->im.nx, IS_CHAR_IMAGE);//modify for other type
  if (oi == NULL) return win_printf_OK("Oi could not be created");
  if (bid.timer_do == NULL) return win_printf_OK("Bid NULL");

  while (!(mouse_b & 0x3));
  
  if (bid.next == NULL)
    {
      bid.next = (Bid *)calloc(1, sizeof(Bid));
      bid.next->param = (void *)oi;
      bid.next->timer_do = save_finite_movie_in_thread;
    }
  //creer fonction addnewbid
  return D_O_K;
}


#endif
