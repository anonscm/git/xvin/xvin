
#ifndef _HAT_H_
#define _HAT_H_


typedef struct _hat_param
{
   int nf, nstep,  dead, nper, *im;
   float rot_start, rot_step, zmag, force, z_cor, zmag_finish, *rot;
} hat_param;

# ifndef _HAT_C_

//PXV_VAR(char, sample[]);
PXV_VAR(int, n_hat);

# else

//char sample[] = "test"; // _PXV_DLL
int n_hat = 0;// _PXV_DLL

# endif

# endif
