
#ifndef _FORCE_H_
#define _FORCE_H_


typedef struct _force_param
{
  int nf;                  // the total number of frames requires to perform calibration
  int nstep;               // the number of force acquisition points
  int istep;               // the present acquisition point
  int dead;                // the number of frames diregard just after focus has changed
  int nper;                // the number of frames between two focus changes
  int *im;                 // the array of images number 
  float zmag_start;        // the starting zmag position
  float zmag_step;         // the step size of zmag step
  float rot_start;         // the starting zmag position
  float rot_step;          // the step size of zmag step
  float *zm;               // the array of zmag position
  float *ro;               // the array of rotation position
  float *fm;               // the array of estimated forces
  float fbr;               // the low frequency mode suppression 
  float fdr;               // the high frequency mode suppression 
  float accuracy;          // accuracy for the force measurements 
} force_param;


# ifndef _FORCE_C_

//PXV_VAR(char, sample[]);
PXV_VAR(int, n_force);

# else

//char sample[] = "test"; // _PXV_DLL
int n_force = 0;// _PXV_DLL

# endif

# endif
