
#ifndef _HAT_C_
#define _HAT_C_

# include "allegro.h"
# include "winalleg.h"

# include "xvin.h"
# include "fftl32n.h"
# include "fillib.h"





/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "../cfg_file/Pico_cfg.h"
#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "focus.h"
# include "magnetscontrol.h"
# include "trackBead.h"
# include "brown_util.h"
# include "track_util.h"
# include "hat.h"
# include "scan_zmag.h"
# include "action.h"
int trajectories_op_idle_action(O_p *op, DIALOG *d);


int hat_op_idle_action(O_p *op, DIALOG *d)
{
  if (d == NULL || d->dp == NULL || d->proc == NULL)    return 1;
  if (op->need_to_refresh)      d->proc(MSG_DRAW,d,0);	
  return 0;
}



int hat_job_xyz(int im, struct future_job *job)
{
  register int j;
  int ci;
  b_track *bt = NULL;
  d_s *dsx, *dsy, *dsz;
  float ax, dx, ay, dy, y_over_x, y_over_z;
  un_s *un;

  if (job == NULL || job->oi == NULL || job->in_progress == 0 || job->op->n_dat < 3) return 0;
  if (im < job->imi)  return 0;

  ci = track_info->c_i;
  dsx = job->op->dat[0];
  dsy = job->op->dat[1];
  dsz = job->op->dat[2];
  bt = track_info->bd[job->bead_nb];

  un = job->oi->xu[job->oi->c_xu];
  get_afine_param_from_unit(un, &ax, &dx);
  un = job->oi->yu[job->oi->c_yu];
  get_afine_param_from_unit(un, &ay, &dy);
  y_over_x = (dy != 0) ? dx/dy : 1;
  y_over_z = 1e-6;
  y_over_z = (dy != 0) ? y_over_z/dy : 1;


  while (track_info->imi[ci] > job->imi)
    {
      ci--;
      if (ci < 0) ci += TRACKING_BUFFER_SIZE;
    }

  while (track_info->imi[ci] >= (job->imi-32))
    {
      j = track_info->imi[ci] - job->in_progress;
      if (j < dsx->mx)
	{
	  dsx->yd[j] = y_over_x * bt->x[ci];
	  dsy->yd[j] = bt->y[ci];
	  dsz->yd[j] = y_over_z * bt->z[ci];
	  dsx->xd[j] = dsy->xd[j] = dsz->xd[j] = j;
	}
      ci--;
      if (ci < 0) ci += TRACKING_BUFFER_SIZE;
    }
  j = job->imi - job->in_progress;
  if (j <= dsx->mx)
    dsx->nx = dsx->ny = dsy->nx = dsy->ny = dsz->nx = dsz->ny = j;
  if (j >= dsx->mx)
        dsx->nx = dsx->ny = dsy->nx = dsy->ny = dsz->nx = dsz->ny = dsx->mx;
  job->op->need_to_refresh = 1;
  if (dsx->nx < dsx->mx) job->imi += 32;
  else job->in_progress = 0; 
  return 0;
}




int hat_job_rot(int im, struct future_job *job)
{
  register int j;
  int ci;
  b_track *bt = NULL;
  d_s *ds;

  if (job == NULL || job->oi == NULL || job->in_progress == 0 || job->op->n_dat < 1) return 0;
  if (im < job->imi)  return 0;

  ci = track_info->c_i;
  while (track_info->imi[ci] > job->imi)
    {
      ci--;
      if (ci < 0) ci += TRACKING_BUFFER_SIZE;
    }
  ds = job->op->dat[0];

  bt = track_info->bd[job->bead_nb];

  while (track_info->imi[ci] >= (job->imi-32))
    {
      j = track_info->imi[ci] - job->in_progress;
      if (j < ds->mx)
	{
	  ds->yd[j] = track_info->rot_mag[ci];
	  ds->xd[j] = j;
	}
      ci--;
      if (ci < 0) ci += TRACKING_BUFFER_SIZE;
    }
  j = job->imi - job->in_progress;
  if (j <= ds->mx)
    ds->nx = ds->ny = j;
  if (j >= ds->mx)
        ds->nx = ds->ny = ds->mx;
  job->op->need_to_refresh = 1;
  if (ds->nx < ds->mx) job->imi += 32;
  else job->in_progress = 0; 
  return 0;
}


int hat_job_hat(int im, struct future_job *job)
{
  int  ci;
  b_track *bt = NULL;
  d_s *ds;
  hat_param *sp = NULL; 
  char buf[1024];

  if (job == NULL || job->oi == NULL || job->in_progress == 0 || job->more_data == NULL) return 0;
  if (im < job->imi)  return 0;

  ci = track_info->c_i;
  ds = job->op->dat[0];
  sp = (hat_param *)job->more_data;
  bt = track_info->bd[job->bead_nb];
  //  sprintf(buf,"Hat %d coucou %03d/%d ",n_hat,ds->nx,ds->mx);
  //set_window_title(buf);

  while (track_info->imi[ci] >= sp->im[ds->nx])
    {
      ci--;
      if (ci < 0) ci += TRACKING_BUFFER_SIZE;
    }
  while (track_info->imi[ci] <= job->imi && ds->nx < ds->mx)
    {
      if ((track_info->imi[ci] > (sp->im[ds->nx] + sp->dead))
	  && (track_info->imi[ci] < sp->im[ds->nx+1]))
	{
	  ds->yd[ds->nx] += bt->z[ci];
	  ds->xd[ds->nx] += 1;
	}
      else if (track_info->imi[ci] >= sp->im[ds->nx+1])
	{
	  if (ds->xd[ds->nx] > 0) ds->yd[ds->nx] /=ds->xd[ds->nx];
	  ds->xd[ds->nx] = sp->rot[ds->nx];
	  sprintf(buf,"Hat %d %03d/%d Rot %6.3f tr. <z> %6.3f microns",n_hat,ds->nx,ds->mx,sp->rot[ds->nx],ds->yd[ds->nx]);
	  ds->nx++;
	  ds->ny++;
	  set_window_title(buf);
	}
      ci++;
      if (ci >= TRACKING_BUFFER_SIZE) ci -= TRACKING_BUFFER_SIZE;
    }
  job->op->need_to_refresh = 1;
  if (ds->nx < ds->mx) job->imi = sp->im[ds->nx+1];
  else 
    {
      job->in_progress = 0; 
      win_title_used = 0;
      n_hat++;
    }
  return 0;
}



int record_hat(void)
{
   int i, j, k, im, li;
   imreg *imr;
   O_i *oi;
   b_track *bt = NULL;
   pltreg *pr;
   O_p *op;
   d_s *ds;
   static int nf = 2048, nstep = 33,  dead = 10, nper = 64;
   static float rot_start = 10, rot_step = 5, zmag,  zmag_finish = 8, z_cor = 0.878;
   char question[1024], name[64];
   hat_param *sp; 

   if(updating_menu_state != 0)	
     {
       if (track_info->n_b == 0) 
	 active_menu->flags |=  D_DISABLED;
       else
	 {
	   for (i = 0, k = 0; i < track_info->n_b; i++)
	     {
	       bt = track_info->bd[i];
	       k += (bt->calib_im != NULL) ? 1 : 0;
	     }
	   if (k) active_menu->flags &= ~D_DISABLED;	
	   else active_menu->flags |=  D_DISABLED;
	 }
       return D_O_K;
     }

   /*
   if(updating_menu_state != 0)	
     {
       if (track_info->n_b == 0) 
	 active_menu->flags |=  D_DISABLED;
       else active_menu->flags &= ~D_DISABLED;			
       return D_O_K;
     }
   */
   if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)	return OFF;

   rot_start = what_is_present_rot_value();
   zmag = read_magnet_z_value();
   sprintf(question,"Hat curve at Z_{mag} present val %g angular position %g\n" 
	   "starting rotation at %%8f with step of %%8f nb of step %%8d\n"
	   "period %%8d dead period(ms) %%8d\n" 
	   "Z_{mag}  %%8f\n" 
	   "zmag finish %%8f z correction factor%%8f\n"
	   ,rot_start,zmag);

   i = win_scanf(question,&rot_start,&rot_step,&nstep,&nper,&dead,&zmag,&zmag_finish,&z_cor);	

   if (i == CANCEL)	return OFF; 

   sp = (hat_param*)calloc(1,sizeof(hat_param));
   if (sp == NULL)  win_printf_OK("Could no allocte hat parameters!");
   sp->im = (int*)calloc(2*nstep+1,sizeof(int));
   sp->rot = (float*)calloc(2*nstep+1,sizeof(float));
   if (sp->im == NULL || sp->rot == NULL)  win_printf_OK("Could no allocte hat parameters!");

   for (i = 0, nf = 0; i < 2*nstep ; i++)
     {
       j = i;
       j %= 2*nstep;
       j = (j < nstep) ? j : 2 * nstep -1 - j;
       nf += nper;
     }

   sp->nf = nf;
   sp->nstep = nstep;
   sp->dead = dead;
   sp->nper = nper;

   sp->rot_start = rot_start;
   sp->rot_step = rot_step;
   sp->zmag_finish = zmag_finish;
   sp->z_cor = z_cor;
   sp->zmag = zmag;
   sprintf(name,"Hat curve %d",n_hat);

   // We create a plot region to display bead position, hat  etc.
   pr = create_hidden_pltreg_with_op(&op, nf, nf, 0,name);
   if (pr == NULL)  win_printf_OK("Could not find or allocte plot region!");
   //op = pr->one_p;
   ds = op->dat[0];   

   //win_printf("nf %d",nf);


   im = track_info->imi[track_info->c_i];                                 

   k = fill_next_available_action(im+4, MV_ZMAG_ABS, sp->zmag);
   if (k < 0)	win_printf_OK("Could not add pending action!");


   im += 64;


   for (i = 0, nf = 0; i < 2*nstep ; i++)
     {
       j = i;
       j %= 2*nstep;
       j = (j < nstep) ? j : 2 * nstep -1 - j;
       sp->rot[i] = rot_start + (j * rot_step);
       sp->im[i] = im + nf;
       nf += nper;
     }
   sp->rot[i] = rot_start;
   sp->im[i] = im + nf;




   //we create one plot for each bead
   for (i = li = 0; i < track_info->n_b; i++)
     {
       bt = track_info->bd[i];
       if ((bt->calib_im == NULL) || (bt->not_lost <= 0) || (bt->in_image == 0))
	 continue;
       if (li)
	 {
	   op = create_and_attach_one_plot(pr, nf, nf, 0);
	   ds = op->dat[0];
	 }
       li++;

       ds->nx = ds->ny = 0;
       set_ds_source(ds,"Bead tracking at %d Hz Acquistion %d for bead %d \n "
		     "X coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_hat,i
		     ,track_info->cl,track_info->cw,nf
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,sample
		     ,what_is_present_rot_value()
		     ,what_is_present_z_mag_value()
		     ,bt->calib_im->filename);
       if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
	 return win_printf_OK("I can't create plot !");
       ds->nx = ds->ny = 0;
       set_ds_source(ds,"Bead tracking at %d Hz Acquistion %d for bead %d \n "
		     "Y coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_hat,i
		     ,track_info->cl,track_info->cw,nf
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,sample
		     ,what_is_present_rot_value()
		     ,what_is_present_z_mag_value()
		     ,bt->calib_im->filename);
       if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
	 return win_printf_OK("I can't create plot !");
       ds->nx = ds->ny = 0;
       set_ds_source(ds,"Bead tracking at %d Hz Acquistion %d for bead %d \n "
		     "Z coordinate l = %d, w = %d, nim = %d\n"
		     "objective %f, zoom factor %f, sample %s Rotation %g\n"
		     "Z magnets %g mm\n"
		     "Calibration from %s"
		     ,Pico_param.camera_param.camera_frequency_in_Hz,n_hat,i
		     ,track_info->cl,track_info->cw,nf
		     ,Pico_param.obj_param.objective_magnification
		     ,Pico_param.micro_param.zoom_factor
		     ,sample
		     ,what_is_present_rot_value()
		     ,what_is_present_z_mag_value()
		     ,bt->calib_im->filename);

       uns_oi_2_op(oi, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
       create_attach_select_x_un_to_op(op, IS_SECOND, 0
				       ,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");
       
       op->op_idle_action = hat_op_idle_action; //xyz_hat_idle_action;
       set_op_filename(op, "X(t)Y(t)Z(t)bd%dhat%d.gr",i,n_hat);
       set_plot_title(op, "Bead %d trajectory %d", i,n_hat);
       set_plot_x_title(op, "Time");
       set_plot_y_title(op, "Position");			

       op->x_lo = (-nf/64); ///Pico_param.camera_param.camera_frequency_in_Hz;
       op->x_hi = (nf + nf/64); ///Pico_param.camera_param.camera_frequency_in_Hz;
       set_plot_x_fixed_range(op);
         fill_next_available_job_spot(im, im+32, COLLECT_DATA, i, imr, oi, pr, op, (void*)sp, hat_job_xyz, 0);
       //win_printf("bd %d",i);
     }
   


   op = create_and_attach_one_plot(pr, nf, nf, 0);
   ds = op->dat[0];
   set_plot_title(op, "Dz and rot %d", n_hat);
   set_plot_file(op,"bddzr%03d.gr",n_hat);
   set_plot_x_title(op, "Real time");
   set_plot_y_title(op, "dZ and rotation");			

   /*
   set_ds_source(ds,"Bead tracking at %d Hz Acquistion %d \n "
		 "Magnet Z position l = %d, w = %d, nim = %d\n"
		 "objective %d, zoom factor %f, sample %s Rotation %g\n"
		 "Calibration from %s"
		 ,Pico_param.camera_param.camera_frequency_in_Hz,n_hat
		 ,track_info->cl,track_info->cw,nf
		 ,Pico_param.obj_param.objective_magnification
		 ,Pico_param.micro_param.zoom_factor
		 ,sample
		 ,what_is_present_rot_value()
		 ,bt->calib_im->filename); 
   */
   create_attach_select_x_un_to_op(op, IS_SECOND, 0
				       ,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");

   op->x_lo = (-nf/64); //*op->dx;
   op->x_hi = (nf + nf/64);//*op->dx;
   set_plot_x_fixed_range(op);
   ds->nx = ds->ny = 0;
   op->op_idle_action = hat_op_idle_action; 

   fill_next_available_job_spot(im, im+32, COLLECT_DATA, i, imr, oi, pr, op, NULL, hat_job_rot, 0);

   for (i = 0; i < track_info->n_b; i++)
     {
       bt = track_info->bd[i];
       if ((bt->calib_im == NULL) || (bt->not_lost <= 0) || (bt->in_image == 0))
	 continue;

       op = create_and_attach_one_plot(pr, 2*nstep,  2*nstep, 0);
       ds = op->dat[0];
       ds->nx = ds->ny = 0;       
       //set_dot_line(ds);
       set_plot_symb(ds, "\\pt5\\oc");
       set_plot_title(op, "Beads %d Zmag Hat %d, rot %f", i, n_hat,what_is_present_rot_value());
       set_plot_x_title(op,"Lengh (\\mu m)");
       set_plot_y_title(op,"Rotation (turn)");
       set_plot_file(op,"hatzbd%01d%03d%s",i,n_hat,".gr");
       
       /*
       set_ds_source(ds,"Hat Z\nZ mag start = %g for %d frames hat by steps of %g"
		     " for %d steps \nrot = %g dead period %d\n"
		     "Bead  calbration %s \n"
		     ,zstart,nper,zstep,nstep,what_is_present_rot_value()
		     ,dead,bt->calib_im->filename);	
       */
       op->op_idle_action = hat_op_idle_action; 
       j =  nper;
       // there is a bug in hat_job_hat
       fill_next_available_job_spot(im, sp->im[1], COLLECT_DATA, i, imr, oi, pr, op, (void*)sp, hat_job_hat, 0);
     }
   for (i = 0, nf = 0; i <= 2*nstep ; i++)
     {
       k = fill_next_available_action(sp->im[i], MV_ROT_ABS, sp->rot[i]);
       if (k < 0)	win_printf_OK("Could not add pending action!");
     }

   k = fill_next_available_action(sp->im[2*nstep]+nper, MV_ZMAG_ABS, sp->zmag_finish);
   if (k < 0)	win_printf_OK("Could not add pending action!");
   win_title_used = 1;

 return D_O_K;
}


# endif
