
#ifndef _SCAN_ZMAG_H_
#define _SCAN_ZMAG_H_


typedef struct _scan_param
{
   int nf, nstep,  dead, nper, *im;
   float zstart, zstep, zlow, z_cor, lambdazmag, zmag0, *zm, *fm;
} scan_param;

# ifndef _SCAN_ZMAG_C_

PXV_VAR(char, sample[]);
PXV_VAR(int, n_scan);

# else

char sample[] = "test"; // _PXV_DLL
int n_scan = 0;// _PXV_DLL

# endif

# endif
