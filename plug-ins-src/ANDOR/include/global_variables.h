#ifndef _GLOBAL_VARIABLES_H_
#define _GLOBAL_VARIABLES_H_

int flag__display_yes_no;
O_i *oi_buffer;

// From iXon.h
PXV_VAR(AndorCapabilities, caps); 
extern IXON_PARAM iXon;

PXV_VAR(pltreg, *pr_iXon);
PXV_VAR(long, *Img_buff);
extern O_i *oi_iXon;
extern imreg *imr_iXon;
extern DIALOG *d_iXon;

// From iXon_tape.h
extern O_i *oi_iXon_tape;
extern O_i *iXon_movie; // For RAM movie acquisition
PXV_VAR(int, n_images_to_be_saved);
PXV_VAR(long int, previous_image_for_tape_saving);
PXV_VAR(FILE, *fp_images);
PXV_VAR(long int, present_image_number_for_tape_saving);
PXV_VAR(int, n_images_saved);

// From menu_iXon.h
PXV_VAR(MENU, binning_submenu[32]);
PXV_VAR(MENU, temperature_submenu[32]);
PXV_VAR(MENU, HSpeed_submenu[32]);
PXV_VAR(MENU, VSpeed_submenu[32]);
PXV_VAR(MENU, gain_submenu[32]);

#endif
