#ifndef _GLOBAL_FUNCTIONS_H_
#define _GLOBAL_FUNCTIONS_H_

//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
// From iXon.c

// Config file functions
PXV_FUNC(void, create_config_file, (void));
PXV_FUNC(int, write_me, (char* buffer, char* string, ...));

// General camera functions
PXV_FUNC(int, set_image_size_on_iXon, (int bin_x,int bin_y,int subregion_x1,int subregion_x2,int subregion_y1,int subregion_y2));
PXV_FUNC(int, set_aq_time, (void));
PXV_FUNC(int, change_gain, (void));
PXV_FUNC(int, binning_test, (void));
PXV_FUNC(int, set_binning, (int bin));
PXV_FUNC(int, init_VSSpeed_to_max, (void));
PXV_FUNC(int, init_HSSpeed, (void));

// Error function
PXV_FUNC(int, error_report, (unsigned int error_code));
PXV_FUNC(int, test_error, (void));
PXV_FUNC(int, action__create_log_file, (void));
PXV_FUNC(int, error__create_log_file, (void));
PXV_FUNC(char*, formatted_time, (void));

// Initialization functions
PXV_FUNC(void, erase_parameters, (void));
PXV_FUNC(int, init_parameters, (void));
PXV_FUNC(int, Set_iXon_parameters, (void));
PXV_FUNC(int, initialize_ixon, (void));

// Shortcut functions

PXV_FUNC(int, short__set_z_auto, (void));
PXV_FUNC(int, short__pause, (void));


PXV_FUNC(int, Exit_iXon, (void));

// Housekeeping functions
PXV_FUNC(int, iXon_main, (void));
PXV_FUNC(int, iXon_unload, (void));

//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
// From iXon_image_settings.c

// Image size functions
PXV_FUNC(int, update_unit_sets, (O_i* src_oi));

// Image acquisition functions
PXV_FUNC(int, remove_background_from_video__void, (void));
PXV_FUNC(int, remove_background_from_current_oi__void, (void));
PXV_FUNC(int, black_reference, (O_i* oi, float weight));
PXV_FUNC(int, adjust_pixels, (void));

// Base functions 
PXV_FUNC(imreg*, get_imr, (void));

//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
// From iXon_acq_control.c

// Live functions
PXV_FUNC(int, change_display_refreshing_period,(void));
PXV_FUNC(int, set_screen_refresh_freq, (void));
PXV_FUNC(int, allocate_live_buffer, (int sizeX,int sizeY,int pixelDepth));
PXV_FUNC(int, iXon_real_time_imaging, (void));
//PXV_FUNC(int, set_image_for_live, (void));

// Image functions
PXV_FUNC(int, zoom_out_iXon, (void));
PXV_FUNC(int, zoom_in_iXon, (void));

// Acquisition functions
PXV_FUNC(int, Stop_acquisition, (void));

// Single-image functions
PXV_FUNC(int, iXon_2_Xvin, (void));

// Housekeeping functions
PXV_FUNC(int, ixon_acq_main, (void));
PXV_FUNC(int, ixon_acq_unload, (void));

//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
// From iXon_tape.c

PXV_FUNC(int, iXon_RAM_movie, (void));

// Functions
PXV_FUNC(int, From_iXon_2_Xvin_Movie, (void));
PXV_FUNC(int, create_and_adapt_tape_iXon_buffer_image, (void));
PXV_FUNC(int, Kill_timer, (void));

// Tape functions
PXV_FUNC(void, CALLBACK automatic_saving, (HWND hwnd,UINT uMsg,UINT_PTR idEvent,DWORD dwTime));
PXV_FUNC(int, save_tape_mode, (void));
PXV_FUNC(int, save_image_header_JF, (O_i* oi, char *head, int size));
PXV_FUNC(int, close_movie_on_disk_JF, (O_i *oi, int nf));

//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
// From iXon_temp_control.c

// Temperature return functions
PXV_FUNC (int, give_me_temp_in,(void));
PXV_FUNC (float, give_me_temp_fl,(void));

// Temperature user functions
PXV_FUNC (int, set_temperature,(int temperature_target));
PXV_FUNC (int ,emergency_temperature_control,(void));

// Menu void functions
PXV_FUNC (int, give_me_temp_void,(void));
PXV_FUNC (int, give_me_temperature_range_void,(void));
PXV_FUNC (int, set_temperature_void,(void));

//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
// From iXon_threads.c

// Thread functions
PXV_FUNC(DWORD WINAPI, StandbyThreadProc, (LPVOID lpParam));
PXV_FUNC(DWORD WINAPI, RetrieveImages, (LPVOID lpParam));
PXV_FUNC(DWORD WINAPI, TempThreadProc, (LPVOID lpParam));
PXV_FUNC(DWORD WINAPI, InfoThreadProc, (LPVOID lpParam));
PXV_FUNC(DWORD WINAPI, StatusThreadProc, (LPVOID lpParam));
PXV_FUNC(DWORD WINAPI, thread__iXon_RAM_movie, (LPVOID lpParam));

// Initialization functions
PXV_FUNC(int, launcher_iXon_standby_thread, (void));
PXV_FUNC(int, create_iXon_retrieve_images_thread, (void));
PXV_FUNC(int, create_iXon_info_thread, (void));
PXV_FUNC(int, create_iXon_temp_thread, (void));
PXV_FUNC(int, launcher__iXon_display_thread, (void));
PXV_FUNC(int, launcher__iXon_RAM_movie, (void));

//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
// From menu_iXon.c

PXV_FUNC(int, create_HSpeed_submenu, (void));
PXV_FUNC(int, create_VSpeed_submenu, (void));
PXV_FUNC(int, create_gain_submenu,   (void));

// Menus minifunctions
PXV_FUNC(int, set_HSpeed_submenu_void, (void));
PXV_FUNC(int, set_VSpeed_submenu_void, (void));
PXV_FUNC(int, set_temperature_submenu_void, (void));
PXV_FUNC(int, set_binning_submenu_void, (void));
PXV_FUNC(int, set_gain_submenu_void, (void));

// Menus
PXV_FUNC(MENU, *ixon_acq_menu, (void));
PXV_FUNC(MENU, *ixon_img_menu, (void));
PXV_FUNC(MENU, *ixon_temp_menu, (void));
PXV_FUNC(int, menu_ixon_main, (void));


//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
// From gui_caps.c

PXV_FUNC(int, bubble_test, (void));
PXV_FUNC(BITMAP*, new_button, (int x_pos, int y_pos, int n_lines, char *example_test));
PXV_FUNC(int, rgb2c, (int red, int green, int blue));
PXV_FUNC(int, special_bubble, (int x,int y,int color,int size,char *format, ...));

//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
// Imported from plugin "logfile"

PXV_FUNC(FILE*, fopen__event, (void));
PXV_FUNC(int, win_event, (char *message, ...));
PXV_FUNC(int, win_report, (char *message, ...));
PXV_FUNC(int, action__create_log_file, (void));
PXV_FUNC(int, message__create_log_file, (void));

// Imported from plugin "nidaq_commander_adrien"

PXV_FUNC(void, IO__give_line_level, (int line, float *level));
PXV_FUNC(void, IO__give_line_status, (int line, int *status));

// Imported from plugin "motor_adrien"

//PXV_FUNC(int, IO__whatis__motor_p_position_in_microns, (float* position_in_microns));
//PXV_FUNC(int, IO__whatis__motor_p_position_in_percent, (float* position_in_percent));
PXV_FUNC(int, IO__whatis__motor_p_consigne_in_microns, (float* position_in_microns));
PXV_FUNC(int, IO__whatis__motor_r_consigne_in_turns, (float* position_in_turns));
PXV_FUNC(int, IO__whatis__motor_t_consigne_in_mm, (float* position_in_mm));

// Imported from plugin "shortcuts_adrien"

PXV_FUNC(int,  shortcut_main, (void));

#endif
