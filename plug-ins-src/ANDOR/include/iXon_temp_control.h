#ifndef _IXON_TEMP_CONTROL_H_
#define _IXON_TEMP_CONTROL_H_

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Global declarations

//// From gui_caps.c
//extern BITMAP* new_button(int x_pos, int y_pos, int n_lines, char *example_test);

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Functions declaration

//// Temperature return functions
//PXV_FUNC (int, give_me_temp_in,(void));
//PXV_FUNC (float, give_me_temp_fl,(void));
//
//// Temperature user functions
//PXV_FUNC (int, set_temperature,(int temperature_target));
//PXV_FUNC (int ,emergency_temperature_control,(void));
//
//// Menu void functions
//PXV_FUNC (int, give_me_temp_void,(void));
//PXV_FUNC (int, give_me_temperature_range_void,(void));
//PXV_FUNC (int, set_temperature_void,(void));

#endif

