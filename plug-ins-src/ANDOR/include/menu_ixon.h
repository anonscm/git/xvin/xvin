#ifndef _MENU_IXON_H_
#define _MENU_IXON_H_          

MENU HSpeed_submenu[32];
MENU VSpeed_submenu[32];
MENU binning_submenu[32];
MENU temperature_submenu[32];
MENU gain_submenu[32];

///////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Global variables

//extern O_i *oi_iXon_tape ;
//extern int n_images_to_be_saved ;
//extern long int previous_image_for_tape_saving;
//extern FILE *fp_images ;
//extern long int present_image_number_for_tape_saving ;
//extern int n_images_saved ;
//extern pltreg *pr_iXon;
//extern long *Img_buff;
//extern O_i *oi_iXon;
//extern imreg *imr_iXon;
//extern DIALOG *d_iXon;

//// From ixon.c
//extern int binning_test(void);
//extern int test_error(void);
//extern int set_binning(int bin);
//extern int init_VSSpeed_to_max(void);
//extern int change_gain(void);
//
//// From iXon_acq_control.c
//extern int iXon_real_time_imaging(void);
//extern int Stop_acquisition(void);
//extern int save_tape_mode(void);
//extern int set_aq_time(void);
//extern int iXon_2_Xvin(void);
//extern int zoom_in_iXon(void);
//
//// From iXon_temp_control.c
//extern int set_temperature_void(void);
//extern int give_me_temperature_range_void(void);
//extern int give_me_temp_void(void);
//extern int set_temperature(int);

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Function declarations

//PXV_FUNC(int, create_HSpeed_submenu, (void));
//PXV_FUNC(int, create_VSpeed_submenu, (void));
//
//// Menus minifunctions
//PXV_FUNC(int, set_HSpeed_submenu_void, (void));
//PXV_FUNC(int, set_VSpeed_submenu_void, (void));
//PXV_FUNC(int, set_temperature_submenu_void, (void));
//PXV_FUNC(int, set_binning_submenu_void, (void));
//
//// Menus
//PXV_FUNC(MENU, *ixon_acq_menu, (void));
//PXV_FUNC(MENU, *ixon_img_menu, (void));
//PXV_VAR(MENU, binning_submenu[32]);
//PXV_FUNC(MENU, *ixon_temp_menu, (void));
//PXV_VAR(MENU, temperature_submenu[32]);
//PXV_FUNC(int, menu_ixon_main, (void));

#endif
