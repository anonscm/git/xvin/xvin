#ifndef _SIF2GR_C_
#define _SIF2GR_C_

/*
 *    Plug-in program to convert sif files into gr images or movies 
 *
 *   
 *      JF Allemand
 */
#include "allegro.h"
#include "winalleg.h"
#include <windowsx.h>
#include "ctype.h"
#include "xvin.h"
#include "andorsif.h"
#include "atmcd32dpf.h"

#define BUILDING_PLUGINS_DLL
#include "sif2gr.h"



int prasun_threshold(void)
{
  int i, j, k;
  int  nfi, ROI_set, lx0, lnx, ly0, lny, size, tmpi;
  O_i   *ois;
  O_p *op = NULL;
  d_s *ds = NULL;
  imreg *imr;
  float tmp;
  static float thres = 150;
  static int x0 = 0, nx = 64, y0 = 0, ny = 128;
  static int start = 0, end = 1;
  double val;


  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) < 1)	return OFF;
  nfi = abs(ois->im.n_f); 
  nfi = (nfi == 0) ? 1 : nfi;
  if(updating_menu_state != 0)	
    {
      if (nfi <= 1) 	 active_menu->flags |=  D_DISABLED;
      else               active_menu->flags &= ~D_DISABLED;
      return D_O_K;		
    }

  end = nfi;
  ROI_set = (ois->im.nxs > 0 || ois->im.nys > 0 || ois->im.nxe < ois->im.nx || ois->im.nye < ois->im.ny) ? 1 : 0;
    
  if (ROI_set)
    {
      lx0 = ois->im.nxs; lnx = ois->im.nxe; 
      ly0 = ois->im.nys; lny = ois->im.nye;
    }
  else 
    {
      lx0 = x0; lnx = nx; ly0 = y0; lny = ny;
    }

  i = win_scanf("Extract light intensity signal above a noise threshold\n"
		"Specify the center and size of the sub image in the movie:\n"
		"X0 %6d X1 %6d Y0 %6d Y1 %6d\n"
		"Threshold above which signal is count %6f\n"
		"Grab profiles starting at frame %6d ending at %6d\n"
		,&lx0,&lnx,&ly0,&lny,&thres,&start,&end);
  if (i == CANCEL) return  D_O_K;		



  size = end - start;
  //size = (size > ois->im.ny) ? ois->im.ny : size;
  op = create_and_attach_op_to_oi(ois, size, size, 0, 0);
  if (op == NULL) return (win_printf_OK("cant create plot!"));
  ds = op->dat[0];
  inherit_from_im_to_ds(ds, ois);
  uns_oi_2_op(ois, IS_T_UNIT_SET, op, IS_X_UNIT_SET);
  op->filename = strdup(ois->filename);
  for (i = start; i < end; i++)
    {
      switch_frame(ois,i);
      ds->xd[i] = i;
      ds->yd[i] = 0;
      tmpi = 0;
      for (k = ly0; k < lny ; k++)	
	{
	  if (k < 0 || k >= ois->im.ny) continue;
	  for (j = lx0; j < lnx; j++)
	    {
	      if (j < 0 || j >= ois->im.nx) continue;
	      if (get_raw_pixel_value(ois, j, k, &val) == 0)
		{
		  tmp = (float)val - thres;
		  if (tmp > 0) 
		    {
		      ds->yd[i] += tmp;
		      tmpi++;
		    }
		}
	    }
	}
      if (tmpi) ds->yd[i] /= tmpi;
    }
  if (ROI_set == 0)
    {
      x0 = lx0; nx = lnx; y0 = ly0; ny = lny;
    }
  set_plot_title(op,"\\stack{{Signal averaged over X in [%d,%d[}"
		 "{and Y in [%d,%d[ (%d pts),}{with frames in [%d,%d[}"
		 "{signal is substract with %g}"
		 "{negative values excluded}}",lx0,lnx,ly0,lny,tmpi,start, end,thres);
  set_ds_treatement(ds,"Signal averaged over X in [%d,%d[ and Y in [%d,%d[, (%d pts) with frames in [%d,%d["
		    "signal is substract with %g, negative values excluded",lx0,lnx,ly0,lny,tmpi,start, end,thres);

  ois->need_to_refresh |= PLOT_NEED_REFRESH;			
  return refresh_image(imr, imr->n_oi - 1);

}

int load_sif_file(void)
{
  static char fullfile[512] = {0};
  imreg *imr = NULL;
  //O_i *oi = NULL;
  // register int i = 0;
  //FILE *in;
    
  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("Test for sif file ANDOR parser");
    }
  //On ouvre une fenetre
  /*
  imr = create_and_register_new_image_project( 0,   32,  900,  668);
  if (imr == NULL) win_printf("Patate t'es mal barre!");
  */	
 
  if (ac_grep(cur_ac_reg,"%im",&imr) < 1)	
    return win_printf_OK("Cannot find imr");
  //On cherche le nom du fichier


  if (do_select_file(fullfile, sizeof(fullfile), "ANDOR-FILE", "*.sif", "Andor image File \0", 1))
    return win_printf_OK("Cannot select output file");

  /*
        
  switch_allegro_font(1);
  i = file_select_ex("Load SIF File (*.sif)", fullfile, "sif", 512, 0, 0);
  switch_allegro_font(0);
  if (i == 0) return D_O_K;
  */	
  if((in = fopen(fullfile, "rb"))== NULL)
    {
      win_printf("Cannot open input file: Please check the file path\n");
      return 1;
    }
	    

  lImage = (ImageStruct *)calloc(1,sizeof(ImageStruct));
  lInstaImage = (InstaImageStruct *)calloc(1,sizeof(InstaImageStruct));
  lCalibImage = (CalibImageStruct *)calloc(1,sizeof(CalibImageStruct));
	
  if (lImage == NULL || lInstaImage == NULL || lCalibImage == NULL)
    return       win_printf("Cannot allocate Andor image!\n");
  read_all_data(imr , lInstaImage, lCalibImage, lImage);
  fclose(in);
  imr->o_i[imr->n_oi-1]->filename = fullfile;
  find_zmin_zmax(imr->o_i[imr->n_oi-1]);
  imr->o_i[imr->n_oi-1]->need_to_refresh |= BITMAP_NEED_REFRESH;
  //remove_image (imr, IS_FLOAT_IMAGE, (void *)imr->o_i[0])	;
  return refresh_image(imr, imr->n_oi - 1);
  //return D_O_K;
}          

MENU *sif2gr_menu(void) 
{
  static MENU mn[32];
 
    
  if (mn[0].text != NULL)	return mn;
  

  add_item_to_menu(mn,"Prasun threshold",prasun_threshold  ,NULL,0,NULL);
  add_item_to_menu(mn,"Load SIF", load_sif_file,NULL,0,NULL);
  return mn;
}



int	sif2gr_main(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  add_image_treat_menu_item("SIF2GR", NULL, sif2gr_menu(), 0, NULL);
	
  
  return D_O_K;
}

int	parallel_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  remove_item_to_menu(image_treat_menu, "SIF2GR", NULL, NULL);
  return D_O_K;
}

#endif
