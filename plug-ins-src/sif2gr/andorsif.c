#ifndef _ANDORSIF_C_
#define _ANDORSIF_C_
/***********************************************************************
CLASS:					NA
NAME:						Alan Murray modified JF Allemand
PROTECTION: 		NA
RETURN:       	NA
Version 				1.2
Last Modified		01/09/1999
DESCRIPTION:		This program will read in any Andor SIF file
								with the prupose of storing the structure and
                identifying what the structure is.
**********************************************************************/
#include <io.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#include <conio.h>
#include <dir.h>
#include "allegro.h"
#include "winalleg.h"
#include <windowsx.h>
#include "xvin.h"
#include "andorsif.h"


//#define HIWORD(l)   ((short) (((long) (l) >> 16) & 0xFFFF))
//#define LOWORD(l)   ((short) (l))

extern FILE *in;


/*****************************************************************************/
//Function: read_string
//Inputs: Terminating charachter
//Outputs: String
//The purpose of this function is to read in string to a buffer
// and return the string to the calling function
/*****************************************************************************/

void read_string(char terminator,char *buffer, int buf_size)
{
  char ch;
  int i;
  i=0;
  ch ='a';
  if(!buffer){
    win_printf("Buffer array was not created\n");
    exit(0);
  }
  for (i = 0, ch = 'a' ;(ch != terminator) && (i < buf_size-1); )
    {
      fscanf(in,"%c",&ch);
      if (ch != terminator)  buffer[i++]= ch; 		      // Add the charachter to the buffer
    }
  buffer[i] = 0;//NULL;
}



/*****************************************************************************/
//Function: read_int
//Inputs: Terminating charachter
//Outputs: A number of type 'long'
//The purpose of this function is to read in a number and
// return the number to the calling function
/*****************************************************************************/

long read_int(char terminator)
{
  long value;
  char ch, int_buffer[1024];
  unsigned int i, j;
  i=0;
  ch ='a';

  // Skip leading spaces
  for (i = 0, ch = ' ', j = 1; (j == 1) && (i < sizeof(int_buffer)-1) &&  (isspace(ch)); )
     j = fscanf(in,"%c",&ch);
  //Captures first char
  if (isspace(ch) == 0)   int_buffer[i++]= ch;  


  for ( ; (j == 1) && (i < sizeof(int_buffer)-1) && (ch != terminator);  )
    {
     j = fscanf(in,"%c",&ch); //Don't store spaces or letters
     if ((j == 1) && (!isalpha(ch)) && (!isspace(ch)))   int_buffer[i++]= ch;  
    }

  int_buffer[i] = 0;
  value = (atol(int_buffer));                  //convert value to a long integer
  return value;
}



/*****************************************************************************/
//Function: read_float
//Inputs: Terminating charachter
//Outputs: A number of type 'float'
//The purpose of this function is to read in a number and
// return the number to the calling function
/*****************************************************************************/

float read_float(char terminator)
{
  float value;
  char ch, float_buffer[1024];
  unsigned int i, j;
  i=0;
  ch ='a';

  // Skip leading spaces
  for (i = 0, ch = ' ', j = 1; (j == 1) && (i < sizeof(float_buffer)-1) &&  (isspace(ch)); )
     j = fscanf(in,"%c",&ch);
  //Captures first char
  if (isspace(ch) == 0)   float_buffer[i++]= ch;  

  for ( ; (j == 1) && (i < sizeof(float_buffer)-1) && (ch != terminator);  )
    {
     j = fscanf(in,"%c",&ch); //Don't store spaces or letters
     if (j == 1)   float_buffer[i++]= ch;  
    }

  float_buffer[i] = 0;//NULL;
  if (strcmp(float_buffer, "+INF") == 0) strcpy(float_buffer, "0");
  value = (atof(float_buffer));               // Convert value to a float
  return value;
}



/*****************************************************************************/
//Function: read_byte
//Inputs: none
//Outputs: A number of type 'int'
//The purpose of this function is to do a single read of a byte
//It does this by reading a charachter and outputting its integer value
//i.e. the Integer equivalent of the ascii value.
/*****************************************************************************/

int read_byte_and_skip_terminator(void)

{
  char ch,termin_ch;
  int i;
  ch ='a';
  fscanf(in,"%c",&ch);
  fscanf(in,"%c",&termin_ch);             //Skips the terminater
  i = (int)(ch);                          // gives integer value of ascii code
  return i;
}

/*****************************************************************************/
//Function: read_char
//Inputs: none
//Outputs: A character
//The purpose of this function is to read in a character and
//return the character to the calling function
//It is provided for values of type char
/*****************************************************************************/

char read_char_and_skip_terminator(void)

{
  char termin_ch,ch;
  ch ='a';
  fscanf(in,"%c",&ch);
  fscanf(in,"%c",&termin_ch);             //Skips the terminater
  return ch;
}


/*****************************************************************************/
//Function: read_len_chars
//Inputs: The length of the string
//Outputs: The string
//The purpose of this function is to read in a predefined number
//of charachters and return them as a string
/*****************************************************************************/

void read_len_chars(int string_length,char *len_chars_buffer, int buf_size)
{
  char ch;
  int i;
  ch ='a';
  if(!len_chars_buffer){
    win_printf("lens_char array was not created");
    exit(0);
  }
  for(i = 0; (i < string_length) && (i < buf_size-1);i++)
    {
      fscanf(in,"%c",&ch);         // Reads in a string of length string_length
      len_chars_buffer[i] = ch;
    }
  len_chars_buffer[i] = 0;//NULL;
}


/*****************************************************************************/
//Function: read_image
//Inputs: The total number of values stored for the image i.e. image length
//Outputs: A buffer full of floating point numbers( The image values )
//The purpose of this function is to read in the image as
//a buffer of floating point numbers
/*****************************************************************************/
void read_image(long rd_image_length,float *image_buffer)
{
  fread(image_buffer,4,(size_t)rd_image_length,in);
  //Reads the image 4 bytes at a time

}// End of read_image

int load_pixels(imreg *imr, InstaImageStruct StructInstaImage, CalibImageStruct StructCalibImage, ImageStruct StructImage)
{
  O_i *oi = NULL;
  int nx = 0;
  int ny = 0;
  int nf = 0;
  int i, k ;
    
    
  if (imr == NULL) win_printf("IMR pb");
    

  nx = StructImage.position[0].right - StructImage.position[0].left +1;
  ny = StructImage.position[0].top - StructImage.position[0].bottom +1;
  nf = StructImage.no_images;    

  if (nf < 0 || nx > 4096 || nx < 0 || ny < 0 || ny > 4096)
    i = win_printf("Strange  movie of %d frame %dx%d\nTo strop press CANCEL",nf,nx,ny);
  
  if (i == CANCEL) return D_O_K;

  oi = create_and_attach_movie_to_imr (imr, nx, ny, IS_FLOAT_IMAGE, nf);
  //free_one_image(imr->o_i[0]);
    
  //oi->im.movie_on_disk = 1;
  if (create_and_attach_unit_set_to_oi (oi,IS_SECOND, 0, StructInstaImage.kinetic_cycle_time, 1, 0, "s", 
					IS_T_UNIT_SET) == NULL) 
    win_printf("Failed");
  set_oi_t_unit_set(oi, oi->n_tu-1); 
  for ( k = 0 ; k < nf ; k++)
    {
      for ( i = 0 ; i < ny ; i++)
        {
	  if (fread(oi->im.pxl[k][i].fl,4*sizeof(char),nx,in) != nx) win_printf("bad");
            
        }
    }            
    
         
  return D_O_K;
}       
 			
/*****************************************************************************/
//Function: read_instaimage
//Inputs: none
//Outputs: none
//The purpose of this function is to read in the instaimage structure data
/*****************************************************************************/
void read_instaimage(InstaImageStruct *InstaImage)
{
  char *instaimage_title;
  long version[4];
  long result;
  int len;
  instaimage_title = (char*)malloc(MAXPATH);
  InstaImage->user_text.text = malloc(MAXPATH);
  version[0] = read_int(' '); //Version
  InstaImage->type = (unsigned int)read_int(' ');
  InstaImage->active = (unsigned int)read_int(' ');
  InstaImage->structure_version = (unsigned int)read_int(' ');
  InstaImage->timedate = read_int(' ');
  InstaImage->temperature = read_float(' ');
  InstaImage->head = read_byte_and_skip_terminator();
  InstaImage->store_type = read_byte_and_skip_terminator();
  InstaImage->data_type = read_byte_and_skip_terminator();
  InstaImage->mode = read_byte_and_skip_terminator();
  InstaImage->trigger_source = read_byte_and_skip_terminator();
  InstaImage->trigger_level = read_float(' ');
  InstaImage->exposure_time = read_float(' ');
  InstaImage->delay = read_float(' ');
  InstaImage->integration_cycle_time = read_float(' ');
  InstaImage->no_integrations = (int)read_int(' ');
  InstaImage->sync = read_byte_and_skip_terminator();
  InstaImage->kinetic_cycle_time = read_float(' ');
  InstaImage->pixel_readout_time = read_float(' ');
  InstaImage->no_points = (int)read_int(' ');
  InstaImage->fast_track_height = (int)read_int(' ');
  InstaImage->gain = (int)read_int(' ');
  InstaImage->gate_delay = read_float(' ');
  InstaImage->gate_width = read_float(' ');

  if( (HIWORD(version[0]) >= 1) && (LOWORD(version[0]) >=6) )
    InstaImage->GateStep = read_float(' ');

  InstaImage->track_height = (int)read_int(' ');
  InstaImage->series_length =(int) read_int(' ');
  InstaImage->read_pattern = read_byte_and_skip_terminator();
  InstaImage->shutter_delay = read_byte_and_skip_terminator();

  if( (HIWORD(version[0]) >= 1) && (LOWORD(version[0]) >=7) ) {
    InstaImage->st_centre_row = (int)read_int(' ');
    InstaImage->mt_offset = (int)read_int(' ');
    InstaImage->operation_mode = (int)read_int(' ');
  }

  if( (HIWORD(version[0]) >= 1) && (LOWORD(version[0]) >=8) ) {
    InstaImage->FlipX = (int)read_int(' ');
    InstaImage->FlipY = (int)read_int(' ');
    InstaImage->Clock = (int)read_int(' ');
    InstaImage->AClock = (int)read_int(' ');
    InstaImage->MCP = (int)read_int(' ');
    InstaImage->Prop = (int)read_int(' ');
    InstaImage->IOC = (int)read_int(' ');
    InstaImage->Freq = (int)read_int(' ');
  }

  if((HIWORD(version[0]) >= 1) && (LOWORD(version[0]) >=9)) {
    InstaImage->VertClockAmp = (int)read_int(' ');
    InstaImage->data_v_shift_speed = read_float(' ');
  }

  if((HIWORD(version[0]) >= 1) && (LOWORD(version[0]) >=10)) {
    InstaImage->OutputAmp = (int)read_int(' ');
    InstaImage->PreAmpGain = read_float(' ');
  }

  if((HIWORD(version[0]) >= 1) && (LOWORD(version[0]) >=11)) {
    InstaImage->Serial = (int)read_int(' ');
  }

  if((HIWORD(version[0]) >= 1) && (LOWORD(version[0]) >=13)) {
    InstaImage->NumPulses = (int)read_int(' ');
  }

  if((HIWORD(version[0]) >= 1) && (LOWORD(version[0]) >=14)) {
    InstaImage->mFrameTransferAcqMode = (int)read_int(' ');
  }

  if( (HIWORD(version[0]) >= 1) && (LOWORD(version[0]) >=5) ){
    read_int('\n');//
    read_string('\n',instaimage_title,MAXPATH);
    strcpy(InstaImage->head_model,instaimage_title);
    InstaImage->detector_format_x = (int)read_int(' ');
    InstaImage->detector_format_z = (int)read_int(' ');
  }
  else if( (HIWORD(version[0]) >= 1) && (LOWORD(version[0]) >=3) ){
    unsigned long head_model = (int)read_int(' ');
    sprintf(InstaImage->head_model,"%lu",head_model);
    InstaImage->detector_format_x = (int)read_int(' ');
    InstaImage->detector_format_z = (int)read_int(' ');
  }
  else {
    strcpy(InstaImage->head_model,"Unknown");
    InstaImage->detector_format_x = 1024u;
    InstaImage->detector_format_z = 256u;
  }

  read_int('\n');
  read_string('\n',instaimage_title,MAXPATH);
  strcpy(InstaImage->filename,instaimage_title);

  //Start of TUserText
  version[1] = read_int(' ');
  result=read_int('\n');
  read_len_chars((int)result,InstaImage->user_text.text,MAXPATH);
  //End of TUserText

  //Start of TShutter
  if( (HIWORD(version[0]) >= 1) && (LOWORD(version[0]) >=4) ) {
    version[2] = read_int(' ');
    InstaImage->shutter.type = read_char_and_skip_terminator();
    InstaImage->shutter.mode = read_char_and_skip_terminator();
    InstaImage->shutter.custom_bg_mode = read_char_and_skip_terminator();
    InstaImage->shutter.custom_mode = read_char_and_skip_terminator();
    InstaImage->shutter.closing_time = read_float(' ');
    InstaImage->shutter.opening_time = read_float('\n');
  }
  // End of TShutter

  //Start of TShamrockSave
  if( (HIWORD(version[0])>1) || ((HIWORD(version[0])==1) && (LOWORD(version[0]) >=12)) ){
    version[3] = read_int(' ');
    InstaImage->shamrock_save.IsActive = read_int(' ');
    InstaImage->shamrock_save.WavePresent = read_int(' ');
    InstaImage->shamrock_save.Wave = read_float(' ');
    InstaImage->shamrock_save.GratPresent = read_int(' ');
    InstaImage->shamrock_save.GratIndex = read_int(' ');
    InstaImage->shamrock_save.GratLines = read_float(' ');
    read_string('\n',InstaImage->shamrock_save.GratBlaze,sizeof(InstaImage->shamrock_save.GratBlaze));
    InstaImage->shamrock_save.SlitPresent = read_int(' ');
    InstaImage->shamrock_save.SlitWidth = read_float(' ');
    InstaImage->shamrock_save.FlipperPresent = read_int(' ');
    InstaImage->shamrock_save.FlipperPort = read_int(' ');
    InstaImage->shamrock_save.FilterPresent = read_int(' ');
    InstaImage->shamrock_save.FilterIndex = read_int(' ');
    len = read_int(' ');
    read_len_chars(len, InstaImage->shamrock_save.FilterString,
		   sizeof(InstaImage->shamrock_save.FilterString));
    InstaImage->shamrock_save.AccessoryPresent = read_int(' ');
    InstaImage->shamrock_save.Port1State = read_int(' ');
    InstaImage->shamrock_save.Port2State = read_int(' ');
    InstaImage->shamrock_save.Port3State = read_int(' ');
    InstaImage->shamrock_save.Port4State = read_int(' ');
    InstaImage->shamrock_save.OutputSlitPresent = read_int(' ');
    InstaImage->shamrock_save.OutputSlitWidth = read_float(' ');
  }
  //End of TShamrockSave

  //  print_instaimage(version,InstaImage->user_text.text);
  //  free(instaimage_title);
  //  free(InstaImage.user_text.text);
}// End of read_instaimage



/*****************************************************************************/
//Function: read_calibimage
//Inputs: none
//Outputs: none
//The purpose of this function is to read in the calibimage structure data
/*****************************************************************************/

void read_calibimage(CalibImageStruct *CalibImage)

{
  char *calibimage_title;
  long version;
  int len;
  calibimage_title = (char*)malloc(MAXPATH);
  CalibImage->x_text = (char*)malloc(MAXPATH);
  CalibImage->y_text = (char*)malloc(MAXPATH);
  CalibImage->z_text = (char*)malloc(MAXPATH);
  version = read_int(' ');
  //win_printf("cali 1");
  CalibImage->x_type = read_byte_and_skip_terminator();
  CalibImage->x_unit = read_byte_and_skip_terminator();
  CalibImage->y_type = read_byte_and_skip_terminator();
  CalibImage->y_unit = read_byte_and_skip_terminator();
  CalibImage->z_type = read_byte_and_skip_terminator();
  CalibImage->z_unit = read_byte_and_skip_terminator();
  //win_printf("cali 10");
  CalibImage->x_cal[0] = read_float(' ');
  CalibImage->x_cal[1] = read_float(' ');
  CalibImage->x_cal[2] = read_float(' ');
  CalibImage->x_cal[3] = read_float('\n');
  CalibImage->y_cal[0] = read_float(' ');
  CalibImage->y_cal[1] = read_float(' ');
  CalibImage->y_cal[2] = read_float(' ');
  CalibImage->y_cal[3] = read_float('\n');
  CalibImage->z_cal[0] = read_float(' ');
  CalibImage->z_cal[1] = read_float(' ');
  CalibImage->z_cal[2] = read_float(' ');
  CalibImage->z_cal[3] = read_float('\n');
  //win_printf("cali 20");
  if( (HIWORD(version) >= 1) && (LOWORD(version) >=3) ){
    CalibImage->rayleigh_wavelength = read_float('\n');
    CalibImage->pixel_length = read_float('\n');
    CalibImage->pixel_height = read_float('\n');
  }
  //win_printf("cali 30");
  len = (int)read_int('\n');
  read_len_chars(len,calibimage_title,MAXPATH);
  strcpy(CalibImage->x_text,calibimage_title);
  len = (int)read_int('\n');
  read_len_chars(len,calibimage_title,MAXPATH);
  strcpy(CalibImage->y_text,calibimage_title);
  len = (int)read_int('\n');
  read_len_chars(len,calibimage_title,MAXPATH);
  strcpy(CalibImage->z_text,calibimage_title);
  //win_printf("cali 40");
  //  print_calib_image(version,CalibImage.x_text,CalibImage.y_text,CalibImage.z_text);
  //  free(calibimage_title);
  //  free(CalibImage.x_text);
  //  free(CalibImage.y_text);
  //  free(CalibImage.z_text);
} //End of TCalibImage


/*****************************************************************************/
//Function: read_image_structure
//Inputs: none
//Outputs: none
//The purpose of this function is to read the image structure data
/*****************************************************************************/

void read_image_structure(ImageStruct *Image)

{
  long version1,*version2;
  //  float *image_buff;
  int j;
  int k;
  int debug = 0;
  version1 = read_int(' ');
  Image->image_format.left = (int)read_int(' ');
  Image->image_format.top = (int)read_int(' ');
  Image->image_format.right = (int)read_int(' ');
  Image->image_format.bottom = (int)read_int(' ');
  Image->no_images = (int)read_int(' ');
  Image->no_subimages = (int)read_int(' ');
  Image->total_length = read_int(' ');
  Image->image_length = read_int('\n');

  if (debug != CANCEL) debug = win_printf("left %d top %d right %d bottom %d\n"
					  "Nb im %d Nb sub images %d\n"
					  "Total len %d im len %d"
					  ,Image->image_format.left
					  ,Image->image_format.top
					  ,Image->image_format.right
					  ,Image->image_format.bottom
					  ,Image->no_images
					  ,Image->no_subimages
					  ,Image->total_length
					  ,Image->image_length
					  );

  version2 = (long*)malloc((sizeof(long))*Image->no_subimages);
  if (!version2) win_printf("Cannot create version2 buffer array ");
  Image->position = malloc((sizeof(int))*Image->no_subimages*6);
  if (!Image->position) win_printf("Cannot create Image->position buffer array ");
  Image->subimage_offset = malloc((sizeof(unsigned long))*Image->no_subimages);
  if (!Image->subimage_offset) win_printf("Cannot create Image->subimage_offset buffer array ");
  for(j=0;j<Image->no_subimages;j++)
    { //Repeat no_subimages times
    version2[j] = read_int(' ');
    Image->position[j].left= (int)read_int(' ');
    Image->position[j].top = (int)read_int(' ');
    Image->position[j].right = (int)read_int(' ');
    Image->position[j].bottom = (int)read_int(' ');
    Image->position[j].vertical_bin = (int)read_int(' ');
    Image->position[j].horizontal_bin = (int)read_int(' ');
    Image->subimage_offset[j] = read_int('\n');
    if (debug != CANCEL) debug = win_printf("subimage %d\nleft %d top %d right %d bottom %d\n"
					    "Y bin %d X bin %d\n"
					    "Toffset %d",j
					    ,Image->position[j].left
					    ,Image->position[j].top
					    ,Image->position[j].right
					    ,Image->position[j].bottom
					    ,Image->position[j].vertical_bin
					    ,Image->position[j].horizontal_bin
					    ,Image->subimage_offset[j]
					    );


    } // End of for(j=0;j<no_subimages;j++)


  Image->time_stamps = malloc((sizeof(unsigned long))*Image->no_images);
  for(k=0;k<Image->no_images;k++){
    Image->time_stamps[k] = read_int('\n');
  }
  //win_printf("Step out %d",Image->position[0].right);
  //  image_buff = (float*)malloc(4*(size_t)Image.total_length);
  //  read_image(Image.total_length,image_buff);

  //  print_image_structure(version1,image_buff,version2);
  //  free(version2);
  //  free(image_buff);
  //  free(Image.subimage_offset);
  //  free(Image.time_stamps);
  //  free(Image.position);
}//read_image_structure;


/*****************************************************************************/
//Function: read_all_data
//Inputs: The number of images
//Outputs: Error
//The purpose of this function is to make all the function calls
//for reading in the data
/*****************************************************************************/

int read_all_data(imreg *imr,InstaImageStruct *pStructInstaImage,CalibImageStruct *pStructCalibImage, ImageStruct *pStructImage)
{
  char ch, *title;
  long version,is_present;
  int p;
  title = (char*)malloc(MAXPATH);   // Allocate dynamic memory for the string
  if (in == NULL) win_printf("BAD");
  read_string('\n',title,MAXPATH);
  //win_printf("File title %s",title);
  if (strcmp("Andor Technology Multi-Channel File",title) != 0 &&
      strcmp("Oriel Instruments Multi-Channel File",title) != 0) {
    win_printf("This is not a proper SIF file: The file may be corrupted\n");
    free(title);
    return 1;
  }
  version = read_int(' '); //Version
  for(p=0;p<5;p++){
    if(!feof(in)){
      is_present = read_int('\n');
      // Following section only is repeated number_of_images times.
      if(is_present==1){
	//win_printf("instaimage");
	read_instaimage(pStructInstaImage);
	//win_printf("calibimage");  	
	read_calibimage(pStructCalibImage);
	//win_printf("stucture");
	read_image_structure(pStructImage);
    
	load_pixels(imr,*pStructInstaImage,*pStructCalibImage,*pStructImage);
      }//End if(is_present =1)
    }//if(!feof(in))
  }//End for(p=0;p<4;p++)
  fscanf(in,"%c",&ch);
  if(feof(in)) ;//win_printf("File has now been successfully read\n");
  else win_printf("End of file has not been reached\n");
  free(title);
  return 0;
}// End of read_all_data


#endif
