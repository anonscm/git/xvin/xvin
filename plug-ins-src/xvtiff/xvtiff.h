#ifndef _TIFF_H_
#define _TIFF_H_
PXV_FUNC(int, do_load_tiff_stack_on_disk,(void));
PXV_FUNC(int, do_load_tiff, (void));
PXV_FUNC(int, load_tiff_stack_file_in_imreg_as_movie, (imreg *imr, char *file, char *path));
PXV_FUNC(int, save_tiff_image, (void));
PXV_FUNC(MENU*, tiff_image_menu, (void));
PXV_FUNC(int, xvtiff_main, (int argc, char **argv));
PXV_FUNC(int, xvtiff_unload, (int argc, char **argv));

#endif
