

PXV_FUNC(int, load_tiff_stack_file_in_imreg_as_movie, (imreg *imr, char *file, char *path));
PXV_FUNC(int, load_tiff_stack_file_in_imreg_as_movie_on_disk, (imreg *imr, char *file, char *path));

PXV_FUNC(int, write_movie_in_tiff_file, (O_i *oi, char *fullname));
