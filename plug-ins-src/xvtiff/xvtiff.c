/*
 *    Example program for the Allegro library, by Shawn Hargreaves.
 *
 *    This program demonstrates how to use an offscreen part of the
 *    video memory to store source graphics for a hardware accelerated
 *    graphics driver.
get_desktop_resolution(int *width, int *height);
 */


#include "allegro.h"
#include "xvin.h"

#include "viewtiff.h"

#include <tiffio.h>

int load_tiff_file_in_imreg(imreg *imr, char *file, char *path);
int write_tiff_file (O_i *oi, char * filename);
static char fullfile[512], tiffilename[2048]; // , *fu = NULL
static int first = 1;


int	do_load_tiff(void)
{
  char path[512] = {0}, file[256] = {0};
  imreg *imr = NULL;
  //imreg	*create_and_register_new_image_project(int x, int y, int w, int h);
  //int d_draw_Im_proc(int msg, DIALOG *d, int c);

  if(updating_menu_state != 0)	return D_O_K;
  /*
  if (fu == NULL)
    {
      my_getcwd(fullfile, 512);
      strcat(fullfile,"\\");
    }
  */
  if (first) tiffilename[0] = first = 0;

  if (do_select_file(tiffilename, sizeof(tiffilename), "TIFF-FILE", "*.tif;*.tiff", "Tiff images to load", 1))
    return win_printf_OK("Cannot select input file");

  imr = find_imr_in_current_dialog(NULL);
  if (imr == NULL) imr = create_and_register_hidden_new_image_project(0,   32,  900,  668,"Tiff image");
  if (imr == NULL) return win_printf_OK("could not create imreg");


  /*
	switch_allegro_font(1);
	i = file_select_ex("Load file", fullfile, "tif;tiff", 512, 0, 0);
	switch_allegro_font(0);
	if (i != 0)
	{
		fu = fullfile;
		extract_file_name(file, 256, fullfile);
		extract_file_path(path, 512, fullfile);
		strcat(path,"\\");
		load_tiff_file_in_imreg(imr, file, path);
		win_printf("loaded %s\n from %s",pr->one_p->filename,
			backslash_to_slash(pr->one_p->dir));
	}
	switch_allegro_font(0);
  */

  extract_file_name(file, 256, tiffilename);
  extract_file_path(path, 512, tiffilename);
  #ifdef XV_WIN32
  strcat(path,"\\");
  #endif
  load_tiff_file_in_imreg(imr, file, path);
  switch_project_to_this_imreg(imr);
  broadcast_dialog_message(MSG_DRAW,0);
  return 0;
}

int	do_load_tiff_stack(void)
{
  char path[512] = {0}, file[256] = {0};
  imreg *imr = NULL;
  //imreg	*create_and_register_new_image_project(int x, int y, int w, int h);
  //int d_draw_Im_proc(int msg, DIALOG *d, int c);

  if(updating_menu_state != 0)	return D_O_K;
  /*
  if (fu == NULL)
    {
      my_getcwd(fullfile, 512);
      strcat(fullfile,"\\");
    }
  */
  imr = find_imr_in_current_dialog(NULL);
  if (imr == NULL)	  imr = create_and_register_new_image_project( 0,   32,  900,  668);
  if (imr == NULL)	return win_printf_OK("could not create imreg");
  if (first) tiffilename[0] = first = 0;

  if (do_select_file(tiffilename, sizeof(tiffilename), "TIFF-FILE", "*.tif;*.tiff", "Tiff images to load\0", 1))
    return win_printf_OK("Cannot select input file");
  /*
  switch_allegro_font(1);
  i = file_select_ex("Load file", fullfile, "tif;tiff", 512, 0, 0);
  switch_allegro_font(0);
  if (i != 0)
    {
      fu = fullfile;
      extract_file_name(file, 256, fullfile);
      extract_file_path(path, 512, fullfile);
      strcat(path,"\\");
      load_tiff_stack_file_in_imreg_as_movie(imr, file, path);
      //		win_printf("loaded %s\n from %s",pr->one_p->filename,
	//		backslash_to_slash(pr->one_p->dir));
    }
  switch_allegro_font(0);
  */
  extract_file_name(file, 256, tiffilename);
  extract_file_path(path, 512, tiffilename);
  #ifdef XV_WIN32
  strcat(path,"\\");
  #endif
  load_tiff_stack_file_in_imreg_as_movie(imr, file, path);
  broadcast_dialog_message(MSG_DRAW,0);
  return 0;
}
int	do_load_tiff_stack_on_disk(void)
{
  char path[512] = {0}, file[256] = {0};
  imreg *imr = NULL;
  //imreg	*create_and_register_new_image_project(int x, int y, int w, int h);
  //int d_draw_Im_proc(int msg, DIALOG *d, int c);

  if(updating_menu_state != 0)	return D_O_K;
  /*
  if (fu == NULL)
    {
      my_getcwd(fullfile, 512);
      strcat(fullfile,"\\");
    }
  */
  imr = find_imr_in_current_dialog(NULL);
  if (imr == NULL)	  imr = create_and_register_new_image_project( 0,   32,  900,  668);
  if (imr == NULL)	return win_printf_OK("could not create imreg");
  if (first) tiffilename[0] = first = 0;

  if (do_select_file(tiffilename, sizeof(tiffilename), "TIFF-FILE", "*.tif;*.tiff", "Tiff images to load\0", 1))
    return win_printf_OK("Cannot select input file");
  /*
  switch_allegro_font(1);
  i = file_select_ex("Load file", fullfile, "tif;tiff", 512, 0, 0);
  switch_allegro_font(0);
  if (i != 0)
    {
      fu = fullfile;
      extract_file_name(file, 256, fullfile);
      extract_file_path(path, 512, fullfile);
      strcat(path,"\\");
      load_tiff_stack_file_in_imreg_as_movie(imr, file, path);
      //		win_printf("loaded %s\n from %s",pr->one_p->filename,
	//		backslash_to_slash(pr->one_p->dir));
    }
  switch_allegro_font(0);
  */
  extract_file_name(file, 256, tiffilename);
  extract_file_path(path, 512, tiffilename);
  #ifdef XV_WIN32
  strcat(path,"\\");
  #endif
  movie_buffer_on_disk = 1;
  load_tiff_stack_file_in_imreg_as_movie_on_disk(imr, file, path);
  movie_buffer_on_disk = 0;

  broadcast_dialog_message(MSG_DRAW,0);
  return 0;
}


int save_tiff_image(void)
{
  register int i, j;
  char s[512] = {0}, c[256] = {0}, c_dir[256] = {0};
  static char *im_dir = NULL;
  imreg *imr = NULL;
  //char *my_getcwd(char *path, int size);

  if(updating_menu_state != 0)	return D_O_K;
  imr = find_imr_in_current_dialog(NULL);
  if (imr == NULL || imr->one_i == NULL || imr->one_i->im.pixel == NULL)
    return win_printf_OK("No valid imr!");
  if (imr->one_i->filename != NULL)
    {
      strcpy(c,imr->one_i->filename);
      for (i = 0; i < 256 && c[i] != 0 && c[i] != '.'; i++);
      if (i < 252) sprintf(c+i,".tif");

    }
  else 				strcpy(c,"untitled.tif");
  if (imr->one_i->dir != NULL) 	strcpy(c_dir,imr->one_i->dir);
  else
    {
      if (im_dir == NULL)
	{
	  my_getcwd(c_dir ,DIR_LEN);
	  slash_to_backslash(c_dir);
	  im_dir = (char *)calloc(256,sizeof(char));
	  strcpy(im_dir,c_dir);
	}
      else strcpy(c_dir,im_dir);
    }
  sprintf(fullfile,"%s\\%s",c_dir,c);

  /*
  switch_allegro_font(1);
  i = file_select_ex("Save image in TIFF format", fullfile, "tif;tiff", 512, 0, 0);
  switch_allegro_font(0);
  */

  if (do_select_file(fullfile, sizeof(fullfile), "TIFF-FILE", "*.tif;*.tiff", "Saving Tiff images\0", 0))
    return win_printf_OK("Cannot select input file");

  //  if (i != 0)    {
  extract_file_name(c, 256, fullfile);
  extract_file_path(c_dir, 256, fullfile);
  #ifdef XV_WIN32
  strcat(c_dir,"\\");
  #endif
  i =	do_you_want_to_overwrite(fullfile, NULL,NULL);
  if (i == WIN_CANCEL) return win_printf_OK(("Refuse overwrite"));

  j = 0;
  printf("j = %d wf = %d\n",j,switch_frame(imr->one_i,j));
  while (switch_frame(imr->one_i,j) == j)
    {
      for (i=0 ; c[i] != 0 && c[i] != '.'; i++);
      c[i] = 0;
	    display_title_message("image %d",j);
	    sprintf(s,"%s%s%05d.tif",c_dir,c,j);
	    write_tiff_file (imr->one_i, s);
      j++;
	}

  //}
  //switch_allegro_font(0);
  broadcast_dialog_message(MSG_DRAW,0);
  return D_O_K;
}



int save_tiff_movie(void)
{
  register int i;
  char  c[256] = {0}, c_dir[256] = {0};
  static char *im_dir = NULL;
  imreg *imr = NULL;
  //char *my_getcwd(char *path, int size);

  if(updating_menu_state != 0)	return D_O_K;
  imr = find_imr_in_current_dialog(NULL);
  if (imr == NULL || imr->one_i == NULL || imr->one_i->im.pixel == NULL)
    return win_printf_OK("No valid imr!");
  if (imr->one_i->filename != NULL)
    {
      strcpy(c,imr->one_i->filename);
      for (i = 0; i < 256 && c[i] != 0 ; i++);
      for ( ; i >= 0 && c[i] != '.'; i--);
      win_printf("file %s",c);
      if (i < 252) sprintf(c+i,".tif");
      win_printf("file %s",c);
    }
  else 				strcpy(c,"untitled.tif");
  if (imr->one_i->dir != NULL) 	strcpy(c_dir,imr->one_i->dir);
  else
    {
      if (im_dir == NULL)
	{
	  my_getcwd(c_dir ,DIR_LEN);
	  slash_to_backslash(c_dir);
	  im_dir = (char *)calloc(256,sizeof(char));
	  strcpy(im_dir,c_dir);
	}
      else strcpy(c_dir,im_dir);
    }
  sprintf(fullfile,"%s\\%s",c_dir,c);

  if (do_select_file(fullfile, sizeof(fullfile), "TIFF-FILE", "*.tif;*.tiff", "Saving Tiff images\0", 0))
    return win_printf_OK("Cannot select input file");

  //  if (i != 0)    {
  extract_file_name(c, 256, fullfile);
  extract_file_path(c_dir, 256, fullfile);
  #ifdef XV_WIN32
  strcat(c_dir,"\\");
  #endif

  i =	do_you_want_to_overwrite(fullfile, NULL,NULL);
  if (i == WIN_CANCEL) return D_O_K;
  write_movie_in_tiff_file(imr->one_i, fullfile);
  //}
  //switch_allegro_font(0);
  broadcast_dialog_message(MSG_DRAW,0);
  return D_O_K;
}


XV_FUNC(int, xvtiff_main, (int argc, char** argv))
{
  (void)argc;
  (void)argv;
  add_item_to_menu(image_file_import_menu,"Load TIFF\t(ctrl+T)", do_load_tiff, NULL, 0, NULL);
  add_item_to_menu(image_file_import_menu,"Load TIFF stack\t(ctrl+T)", do_load_tiff_stack, NULL, 0, NULL);
  add_item_to_menu(image_file_import_menu,"Load TIFF stack on disk \t(ctrl+T)", do_load_tiff_stack_on_disk, NULL, 0, NULL);

  add_item_to_menu(image_file_export_menu,"Save TIFF", save_tiff_image, NULL, 0, NULL);
  add_item_to_menu(image_file_export_menu,"Save TIFF movie", save_tiff_movie, NULL, 0, NULL);


  return 0;
}

int	xvtiff_unload(int argc, char **argv)
{
  remove_item_to_menu(image_file_import_menu,"Load TIFF\t(ctrl+I)", NULL, NULL);
  remove_item_to_menu(image_file_import_menu,"Load TIFF stack\t(ctrl+T)",  NULL, NULL);
  remove_item_to_menu(image_file_export_menu,"Save TIFF", NULL, NULL);
  remove_item_to_menu(image_file_export_menu,"Save TIFF movie",  NULL, NULL);
  (void)argc;
  (void)argv;
  return D_O_K;
}
