#include <allegro.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <tiff.h>
#include <tiffio.h>
//#include "tiffiop.h"
# include "xvin.h"


#include "xvtiff.h"

#define	streq(a,b)	(strcmp((a),(b)) == 0)
#define	strneq(a,b,n)	(strncmp(a,b,n) == 0)


static unsigned short compression = COMPRESSION_NONE;
static unsigned short fillorder = FILLORDER_MSB2LSB ;





// int  get_nb_of_frames(char *file, int *same)
// {
//   int dircount = 0;
//   unsigned long imagelength, imagewidth;
//   uint16 bitspersample;
//   int w = 0, h = 0, d = 0, alike = 1;
//
//   TIFF* tif = TIFFOpen(file, "r");
//   if (tif) {
//
//     do {
//
//       TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &imagelength);
//       TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &imagewidth);
//       TIFFGetField(tif, TIFFTAG_BITSPERSAMPLE, &bitspersample);
//       if (dircount == 0)
// 	{
// 	  h = imagelength;
// 	  w = imagewidth;
// 	  d = bitspersample;
// 	}
//       else
// 	{
// 	  if (h != (int)imagelength || w != (int)imagewidth || d != (int)bitspersample)
// 	    alike = 0;
// 	}
//       dircount++;
//     } while (TIFFReadDirectory(tif));
//     TIFFClose(tif);
//   }
//   *same = alike;
//   return dircount;
// }
int tiff_read_rgba_file(O_i *oi, TIFF *tif)
{
  //int onx, ony;
  union pix *pd = NULL;
  uint32 imagelength, imagewidth;
  uint16 bitspersample;
  char title[512] = {0};
  /*	TIFFDirectory *td; */


  TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &imagelength);
  TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &imagewidth);
  TIFFGetField(tif, TIFFTAG_DOCUMENTNAME, title);
  TIFFGetField(tif, TIFFTAG_BITSPERSAMPLE, &bitspersample);
  alloc_one_image (oi, imagewidth, imagelength, IS_RGBA_PICTURE);
  pd = oi->im.pixel;
  //onx = oi->im.nx;	ony = oi->im.ny;
  TIFFReadRGBAImage(tif, imagewidth, imagelength, (uint32*)(pd[0].rgba), 0);
  oi->width = (float)imagewidth/512;
  oi->height = (float)imagelength/512;
  oi->iopt2 &= ~X_NUM;		oi->iopt2 &= ~Y_NUM;
  //find_zmin_zmax(oi);
  set_RGB_zmin_zmax_values(oi, 0, 255, 0, 255, 0, 255);
  return 0;
}
static int checkcmap(TIFF* tif, int n, uint16* r, uint16* g, uint16* b)
{
  while (n-- > 0)
    if (*r++ >= 256 || *g++ >= 256 || *b++ >= 256)
      return (16);
  TIFFWarning(TIFFFileName(tif), "Assuming 8-bit colormap");
  return (8);
}

static void compresspalette(unsigned char* out, unsigned char* data, uint32 n, uint16* rmap, uint16* gmap, uint16* bmap)
{
  while (n-- > 0) {
    unsigned int ix = *data++;
    *out++ = rmap[ix];
    *out++ = gmap[ix];
    *out++ = bmap[ix];
  }
}
static void compresscontig(unsigned char* out, unsigned char* rgb, uint32 n)
{
  while (n-- > 0) {
    *out++ = *rgb++;
    *out++ = *rgb++;
    *out++ = *rgb++;
  }
}

static void compresssep(unsigned char* out,
			unsigned char* r, unsigned char* g, unsigned char* b, uint32 n)
{
  while (n-- > 0)
    {
      *out++ = *r++;
      *out++ = *g++;
      *out++ = *b++;
    }
}

int tiffreadbwfile(O_i *oi, TIFF *tif)
{
  int ony, ni = 0; // onx,
  union pix *pd = NULL;
  uint32 imagelength, imagewidth;
  uint16 bitspersample;
  int row;
  //char title[512];
  unsigned char *buf;
  /*	TIFFDirectory *td; */


    TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &imagelength);
    TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &imagewidth);
    TIFFGetField(tif, TIFFTAG_BITSPERSAMPLE, &bitspersample);
    if (ni == 0)
      {
	//win_printf("Allocating %d image %ldx%ld of  %d depth",nf,imagewidth, imagelength, bitspersample);
	if (bitspersample == 16)
	  {
	    if ((alloc_one_image (oi, imagewidth, imagelength, IS_UINT_IMAGE) != 0)
		|| (oi->im.ny != (int)imagelength))
	      {

		return  1;
	      }
	  }
	else  if (bitspersample == 8)
	  {
	    if ((alloc_one_image (oi, imagewidth, imagelength, IS_CHAR_IMAGE) != 0)
		|| (oi->im.ny != (int)imagelength))
	      {

		return 1;
	      }
	  }
      }
    pd = oi->im.pixel;
    //onx = oi->im.nx;
    ony = oi->im.ny;
    if (bitspersample == 16)
      {
	my_set_window_title("Reading TIFF 16 bits im %d",ni);
	for (row = 0; row < (int)imagelength; row++)
	  {
	    buf =  (unsigned char *)pd[ony - (int)row - 1].ui;
	    TIFFReadScanline(tif, buf, (long)row, 0);
	  }
      }
    else  if (bitspersample == 8)
      {
	my_set_window_title("Reading TIFF 8 bits im %d",ni);
	for (row = 0; row < (int)imagelength; row++)
	  {
	    buf =  pd[ony - (int)row - 1].ch;
	    TIFFReadScanline(tif, buf, (long)row, 0);
	  }
      }

# ifdef OLD

  TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &imagelength);
  TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &imagewidth);
  TIFFGetField(tif, TIFFTAG_DOCUMENTNAME, title);
  TIFFGetField(tif, TIFFTAG_BITSPERSAMPLE, &bitspersample);
  if (bitspersample == 16)
    {
      alloc_one_image (oi, imagewidth, imagelength, IS_UINT_IMAGE);
      pd = oi->im.pixel;
      //onx = oi->im.nx;	ony = oi->im.ny;
      for (row = 0; row < imagelength; row++)
	{
	  buf =  pd[ony - (int)row - 1].ch;
	  TIFFReadScanline(tif, buf, row, 0);
	}
    }
  else  if (bitspersample == 8)
    {
      /*printf("%s w = %d h = %d\n",argv[i],(int)imagewidth, (int)imagelength);*/
      alloc_one_image (oi, imagewidth, imagelength, IS_CHAR_IMAGE);
      pd = oi->im.pixel;
      //onx = oi->im.nx;	ony = oi->im.ny;
      if (title[0] != 0) oi->title = Mystrdupre(oi->title,title);
      //	set_cursor_glass();
      for (row = 0; row < imagelength; row++)
	{
	  buf =  pd[ony - (int)row - 1].ch;
	  TIFFReadScanline(tif, buf, row, 0);
	}
      //	reset_cursor();
    }
  /*
	td = &tif->tif_dir;
	if (TIFFFieldSet(tif,FIELD_IMAGEDESCRIPTION))
	set_oi_source(oi, "%s",td->td_imagedescription);
	if (TIFFFieldSet(tif,FIELD_DATETIME))
	set_oi_treatement(oi, "Date %s", td->td_datetime);
  */
# endif
  oi->width = (float)imagewidth/512;
  oi->height = (float)imagelength/512;
  oi->iopt2 &= ~X_NUM;		oi->iopt2 &= ~Y_NUM;
  if (bitspersample == 8) set_zmin_zmax_values(oi, 0, 255);
  else find_zmin_zmax(oi);
  return 0;
}
// int tiffreadbwfile(O_i *oi, TIFF *tif, int nf)
// {
//   int ony, ni = 0; // onx,
//   union pix *pd = NULL;
//   unsigned long imagelength, imagewidth;
//   uint16 bitspersample;
//   int row;
//   //char title[512];
//   unsigned char *buf;
//   /*	TIFFDirectory *td; */
//
//   do {
//     TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &imagelength);
//     TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &imagewidth);
//     TIFFGetField(tif, TIFFTAG_BITSPERSAMPLE, &bitspersample);
//     if (ni == 0)
//       {
// 	oi->im.n_f = nf;
// 	if (nf > 128) oi->im.multi_page = 1;
// 	win_printf("Allocating %d image %ldx%ld of  %d depth",nf,imagewidth, imagelength, bitspersample);
// 	if (bitspersample == 16)
// 	  {
// 	    if ((alloc_one_image (oi, imagewidth, imagelength, IS_UINT_IMAGE) != 0)
// 		|| (oi->im.ny != (int)imagelength))
// 	      {
// 		win_printf_OK("Could not allocate %d image %dx%d of  %d depth"
// 			      ,nf,(int)imagewidth, (int)imagelength, (int)bitspersample);
// 		return  1;
// 	      }
// 	  }
// 	else  if (bitspersample == 8)
// 	  {
// 	    if ((alloc_one_image (oi, imagewidth, imagelength, IS_CHAR_IMAGE) != 0)
// 		|| (oi->im.ny != (int)imagelength))
// 	      {
// 		win_printf_OK("Could not allocate %d image %dx%d of  %d depth"
// 			      ,nf,(int)imagewidth, (int)imagelength, (int)bitspersample);
// 		return 1;
// 	      }
// 	  }
//       }
//     switch_frame(oi,ni);
//     pd = oi->im.pixel;
//     //onx = oi->im.nx;
//     ony = oi->im.ny;
//     if (bitspersample == 16)
//       {
// 	my_set_window_title("Reading TIFF 16 bits im %d",ni);
// 	for (row = 0; row < (int)imagelength; row++)
// 	  {
// 	    buf =  (unsigned char *)pd[ony - (int)row - 1].ui;
// 	    TIFFReadScanline(tif, buf, (long)row, 0);
// 	  }
//       }
//     else  if (bitspersample == 8)
//       {
// 	my_set_window_title("Reading TIFF 8 bits im %d",ni);
// 	for (row = 0; row < (int)imagelength; row++)
// 	  {
// 	    buf =  pd[ony - (int)row - 1].ch;
// 	    TIFFReadScanline(tif, buf, (long)row, 0);
// 	  }
//       }
//     ni++;
//   } while (TIFFReadDirectory(tif) && ni < nf);
//   win_printf("Finish Reading %d fr in %d",ni,nf);
// # ifdef OLD
//
//   TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &imagelength);
//   TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &imagewidth);
//   TIFFGetField(tif, TIFFTAG_DOCUMENTNAME, title);
//   TIFFGetField(tif, TIFFTAG_BITSPERSAMPLE, &bitspersample);
//   if (bitspersample == 16)
//     {
//       alloc_one_image (oi, imagewidth, imagelength, IS_UINT_IMAGE);
//       pd = oi->im.pixel;
//       //onx = oi->im.nx;	ony = oi->im.ny;
//       for (row = 0; row < imagelength; row++)
// 	{
// 	  buf =  pd[ony - (int)row - 1].ch;
// 	  TIFFReadScanline(tif, buf, row, 0);
// 	}
//     }
//   else  if (bitspersample == 8)
//     {
//       /*printf("%s w = %d h = %d\n",argv[i],(int)imagewidth, (int)imagelength);*/
//       alloc_one_image (oi, imagewidth, imagelength, IS_CHAR_IMAGE);
//       pd = oi->im.pixel;
//       //onx = oi->im.nx;	ony = oi->im.ny;
//       if (title[0] != 0) oi->title = Mystrdupre(oi->title,title);
//       //	set_cursor_glass();
//       for (row = 0; row < imagelength; row++)
// 	{
// 	  buf =  pd[ony - (int)row - 1].ch;
// 	  TIFFReadScanline(tif, buf, row, 0);
// 	}
//       //	reset_cursor();
//     }
//   /*
// 	td = &tif->tif_dir;
// 	if (TIFFFieldSet(tif,FIELD_IMAGEDESCRIPTION))
// 	set_oi_source(oi, "%s",td->td_imagedescription);
// 	if (TIFFFieldSet(tif,FIELD_DATETIME))
// 	set_oi_treatement(oi, "Date %s", td->td_datetime);
//   */
// # endif
//   oi->width = (float)imagewidth/512;
//   oi->height = (float)imagelength/512;
//   oi->iopt2 &= ~X_NUM;		oi->iopt2 &= ~Y_NUM;
//   if (bitspersample == 8) set_zmin_zmax_values(oi, 0, 255);
//   else find_zmin_zmax(oi);
//   return 0;
// }



int tiffreadfile(O_i *oi, char *file)
{
  int onx, ony, alike, im;
  union pix *pd = NULL;
  TIFF *in = NULL;
  uint32 w, h;
  uint16 samplesperpixel;
  uint16 bitspersample;
  uint16 config;
  uint16 photometric, nbframes = 1;
  uint16* redl = NULL;
  uint16* greenl = NULL;
  uint16* bluel = NULL;
  tsize_t rowsize;
  register uint32 row;
  register tsample_t s;
  unsigned char *inbuf = NULL;
  //int debug = 0;



  //if (nf > 1 && alike == 1)
  //if (debug != CANCEL) debug = win_printf("this is a TIFF movie having %d frames",nf);

  //_fmode = O_BINARY;
  in = TIFFOpen(file, "r");
  if (in == NULL)
    return 5;
  photometric = 0;

  TIFFGetField(in, TIFFTAG_FRAMECOUNT, &nbframes);
  if (im == WIN_CANCEL) return 0;
  TIFFGetField(in, TIFFTAG_PHOTOMETRIC, &photometric);


  if (photometric == PHOTOMETRIC_MINISBLACK || photometric == PHOTOMETRIC_MINISWHITE )
    {
      //if (debug != CANCEL) debug = win_printf("we jump to black and white read",nf);
      tiffreadbwfile(oi, in);
      TIFFClose(in);
      oi->im.source = Mystrdupre(oi->im.source,backslash_to_slash(file));
      if (oi->im.movie_on_disk) oi->im.n_f = -abs(oi->im.n_f);

    //_fmode = O_TEXT;
      return 0;
    }


  if (photometric != PHOTOMETRIC_RGB && photometric != PHOTOMETRIC_PALETTE )
    {
      //if (debug != CANCEL) debug = win_printf("we jump to RGBA read",nf);
      tiff_read_rgba_file(oi, in);
      win_printf("%s: \nBad photometric %d ;\n"
		 "can only handle RGB and Palette images.\n",
		 backslash_to_slash(file),photometric);
      //_fmode = O_TEXT;
      return 0;
    }
  TIFFGetField(in, TIFFTAG_SAMPLESPERPIXEL, &samplesperpixel);
  if (samplesperpixel != 1 && samplesperpixel != 3)
    {
      win_printf( "%s: Bad samples/pixel %u.\n", backslash_to_slash(file), samplesperpixel);
      tiff_read_rgba_file(oi, in);
      //_fmode = O_TEXT;
      return 0;
    }
  TIFFGetField(in, TIFFTAG_BITSPERSAMPLE, &bitspersample);
  if (bitspersample != 8)
    {
      win_printf(" %s: Sorry, only handle 8-bit samples.\n", backslash_to_slash(file));
      tiff_read_rgba_file(oi, in);
      //_fmode = O_TEXT;
      return 0;
    }
  TIFFGetField(in, TIFFTAG_IMAGEWIDTH, &w);
  TIFFGetField(in, TIFFTAG_IMAGELENGTH, &h);
  TIFFGetField(in, TIFFTAG_PLANARCONFIG, &config);
  win_printf("bef alloc w %d h %d config %d",w,h,config);

  //oi->im.n_f = nf; // new 17/01/2014
  if (oi->im.movie_on_disk) oi->im.n_f = -abs(oi->im.n_f);

  alloc_one_image (oi, w, h, IS_RGB_PICTURE);
  pd = oi->im.pixel;
  onx = oi->im.nx;	ony = oi->im.ny;
  //	set_cursor_glass();
  //win_printf("after alloc ");
#define	pack(a,b)	((a)<<8 | (b))
  switch (pack(photometric, config)) {
  case pack(PHOTOMETRIC_PALETTE, PLANARCONFIG_CONTIG):
  case pack(PHOTOMETRIC_PALETTE, PLANARCONFIG_SEPARATE):
    TIFFGetField(in, TIFFTAG_COLORMAP, &redl, &greenl, &bluel);
    /*
     * Convert 16-bit colormap to 8-bit (unless it looks
     * like an old-style 8-bit colormap).
     */
    if (checkcmap(in, 1<<bitspersample, redl, greenl, bluel) == 16) {
      int i;
#define	CVT(x)		(((x) * 255L) / ((1L<<16)-1))
      for (i = (1<<bitspersample)-1; i >= 0; i--) {
	redl[i] = CVT(redl[i]);
	greenl[i] = CVT(greenl[i]);
	bluel[i] = CVT(bluel[i]);
      }
#undef CVT
    }
    inbuf = (unsigned char *)_TIFFmalloc(TIFFScanlineSize(in));

	pd = oi->im.pixel;
	for (row = 0; row < h; row++) {
	  if (TIFFReadScanline(in, inbuf, row, 0) < 0)
	    break;
	  compresspalette(pd[ony - row - 1].ch, inbuf, w, redl, greenl, bluel);
	}

    //win_printf("after compresspalette ");
    break;
  case pack(PHOTOMETRIC_RGB, PLANARCONFIG_CONTIG):
    inbuf = (unsigned char *)_TIFFmalloc(TIFFScanlineSize(in));

	pd = oi->im.pixel;
	for (row = 0; row < h; row++) {
	  if (TIFFReadScanline(in, inbuf, row, 0) < 0)
	    break;
	  compresscontig(pd[ony - row - 1].ch, inbuf, w);
	}
	TIFFReadDirectory(in);

    //win_printf("after compresscontig ");
    break;
  case pack(PHOTOMETRIC_RGB, PLANARCONFIG_SEPARATE):
    rowsize = TIFFScanlineSize(in);
    inbuf = (unsigned char *)_TIFFmalloc(3*rowsize);

	pd = oi->im.pixel;
	for (row = 0; row < h; row++) {
	  for (s = 0; s < 3; s++)
	    if (TIFFReadScanline(in,
				 inbuf+s*rowsize, row, s) < 0)
	      return (-1);
	  compresssep(pd[ony - row - 1].ch, inbuf, inbuf+rowsize, inbuf+2*rowsize, w);
	}
	TIFFReadDirectory(in);

    //win_printf("after compresssep ");
    break;
  default :
    tiff_read_rgba_file(oi, in);
    break;
  }
#undef pack
  TIFFClose(in);
  if (inbuf != NULL)  _TIFFfree(inbuf);
  //_fmode = O_TEXT;
  //	reset_cursor();
  oi->width = (float)onx/512;
  oi->height = (float)ony/512;
  oi->iopt2 &= ~X_NUM;		oi->iopt2 &= ~Y_NUM;
  oi->im.source = Mystrdupre(oi->im.source,backslash_to_slash(file));
  set_RGB_zmin_zmax_values(oi, 0, 255, 0, 255, 0, 255);
  //find_zmin_zmax(oi);
  return (0);
}


int	switch_frame_of_disk_TIFF_movie(O_i *oi, int f)
{
	register int i, j;
	int knx, kny, ret;
	long long filesize, offset, tmpl;
  union pix *pd = NULL, *ps = NULL;

  O_i *oit;
  oit = (O_i *)calloc(1,sizeof(O_i));
  init_one_image(oit, 0);
	FILE *fp = NULL;
	char filename[512] = {0};

	if (oi == NULL || oi->im.movie_on_disk == 0
	    || oi->dir == NULL || oi->filename == NULL)	return 1;
  f = f+oi->filename_is;
  snprintf(filename,sizeof(filename),"%s%d.%s",oi->filename,f,oi->fileext);
	build_full_file_name(filename, 512, oi->dir, filename);
  ret = tiffreadfile(oit, filename);
  if (ret == 5)
  {
    snprintf(filename,sizeof(filename),"%s0%d.%s",oi->filename,f,oi->fileext);
    build_full_file_name(filename, 512, oi->dir, filename);
    ret = tiffreadfile(oit, filename);
    if (ret == 5)
    {
      snprintf(filename,sizeof(filename),"%s00%d.%s",oi->filename,f,oi->fileext);
      build_full_file_name(filename, 512, oi->dir, filename);
      ret = tiffreadfile(oit, filename);
      if (ret == 5)
      {
        snprintf(filename,sizeof(filename),"%s000%d.%s",oi->filename,f,oi->fileext);
        build_full_file_name(filename, 512, oi->dir, filename);
        ret = tiffreadfile(oit, filename);
      }
      if (ret == 5)
      {
        snprintf(filename,sizeof(filename),"%s0000%d.%s",oi->filename,f,oi->fileext);
        build_full_file_name(filename, 512, oi->dir, filename);
        ret = tiffreadfile(oit, filename);
        if (ret == 5)
        {
          if (oit != NULL) free_one_image(oit);
          return(win_printf_OK("Not able to open file %s\n",filename));
        }
      }
    }
  }

  display_title_message("this is a TIFF movie of file %s\n",filename);


  pd = oi->im.pixel;
  ps = oit->im.pixel;
  for (int li = 0; li < oit->im.ny; li++)
    {
      for (int co = 0; co < oit->im.nx; co++)
  {
    switch (oit->im.data_type)
      {
      case  IS_CHAR_IMAGE:
        pd[li].ch[co] = ps[li].ch[co];
        break;
      case  IS_RGB_PICTURE:
        pd[li].rgb[co] = ps[li].rgb[co];
        break;
      case  IS_RGBA_PICTURE:
        pd[li].rgba[co] = ps[li].rgba[co];
        break;
      case  IS_RGB16_PICTURE:
        pd[li].rgb16[co] = ps[li].rgb16[co];
        break;
      case  IS_RGBA16_PICTURE:
        pd[li].rgba16[co] = ps[li].rgba16[co];
        break;
      case  IS_INT_IMAGE:
        pd[li].in[co] = ps[li].in[co];
        break;
      case  IS_UINT_IMAGE:
        pd[li].ui[co] = ps[li].ui[co];
        break;
      case  IS_LINT_IMAGE:
        pd[li].li[co] = ps[li].li[co];
        break;
      case  IS_FLOAT_IMAGE:
        pd[li].fl[co] = ps[li].fl[co];
        break;
      case  IS_COMPLEX_IMAGE:
        pd[li].cp[co] = ps[li].cp[co];
        break;
      case  IS_DOUBLE_IMAGE:
        pd[li].db[co] = ps[li].db[co];
        break;
      case  IS_COMPLEX_DOUBLE_IMAGE:
        pd[li].dcp[co] = ps[li].dcp[co];
        break;
      }
  }
    }
    if (oit != NULL) free_one_image(oit);


	return ret;
}
//
// int tiffreadfile(O_i *oi, char *file)
// {
//   int onx, ony, nf, alike, im;
//   union pix *pd = NULL;
//   TIFF *in = NULL;
//   uint32 w, h;
//   uint16 samplesperpixel;
//   uint16 bitspersample;
//   uint16 config;
//   uint16 photometric, nbframes = 1;
//   uint16* redl = NULL;
//   uint16* greenl = NULL;
//   uint16* bluel = NULL;
//   tsize_t rowsize;
//   register uint32 row;
//   register tsample_t s;
//   unsigned char *inbuf = NULL;
//   //int debug = 0;
//
//
//
//   nf = get_nb_of_frames(file, &alike);
//   //if (nf > 1 && alike == 1)
//   display_title_message("this is a TIFF movie having %d frames",nf);
//   //if (debug != CANCEL) debug = win_printf("this is a TIFF movie having %d frames",nf);
//
//   //_fmode = O_BINARY;
//   in = TIFFOpen(file, "r");
//   if (in == NULL)
//     return win_printf("cannot open file:\n%s",backslash_to_slash(file));
//   photometric = 0;
//
//   TIFFGetField(in, TIFFTAG_FRAMECOUNT, &nbframes);
//   if (nbframes > 1 || nf > 1) im = win_scanf("nb of frames %d %d",&nbframes,&nf);
//   if (im == WIN_CANCEL) return 0;
//   TIFFGetField(in, TIFFTAG_PHOTOMETRIC, &photometric);
//
//   if (photometric == PHOTOMETRIC_MINISBLACK || photometric == PHOTOMETRIC_MINISWHITE )
//     {
//       //if (debug != CANCEL) debug = win_printf("we jump to black and white read",nf);
//       tiffreadbwfile(oi, in, nf);
//       TIFFClose(in);
//       oi->im.source = Mystrdupre(oi->im.source,backslash_to_slash(file));
//
//       //_fmode = O_TEXT;
//       return 0;
//     }
//
//
//   if (photometric != PHOTOMETRIC_RGB && photometric != PHOTOMETRIC_PALETTE )
//     {
//       //if (debug != CANCEL) debug = win_printf("we jump to RGBA read",nf);
//       tiff_read_rgba_file(oi, in);
//       win_printf("%s: \nBad photometric %d ;\n"
// 		 "can only handle RGB and Palette images.\n",
// 		 backslash_to_slash(file),photometric);
//       //_fmode = O_TEXT;
//       return 0;
//     }
//   TIFFGetField(in, TIFFTAG_SAMPLESPERPIXEL, &samplesperpixel);
//   if (samplesperpixel != 1 && samplesperpixel != 3)
//     {
//       win_printf( "%s: Bad samples/pixel %u.\n", backslash_to_slash(file), samplesperpixel);
//       tiff_read_rgba_file(oi, in);
//       //_fmode = O_TEXT;
//       return 0;
//     }
//   TIFFGetField(in, TIFFTAG_BITSPERSAMPLE, &bitspersample);
//   if (bitspersample != 8)
//     {
//       win_printf(" %s: Sorry, only handle 8-bit samples.\n", backslash_to_slash(file));
//       tiff_read_rgba_file(oi, in);
//       //_fmode = O_TEXT;
//       return 0;
//     }
//   TIFFGetField(in, TIFFTAG_IMAGEWIDTH, &w);
//   TIFFGetField(in, TIFFTAG_IMAGELENGTH, &h);
//   TIFFGetField(in, TIFFTAG_PLANARCONFIG, &config);
//   win_printf("bef alloc w %d h %d config %d",w,h,config);
//
//   oi->im.n_f = nf; // new 17/01/2014
//   alloc_one_image (oi, w, h, IS_RGB_PICTURE);
//   pd = oi->im.pixel;
//   onx = oi->im.nx;	ony = oi->im.ny;
//   //	set_cursor_glass();
//   //win_printf("after alloc ");
// #define	pack(a,b)	((a)<<8 | (b))
//   switch (pack(photometric, config)) {
//   case pack(PHOTOMETRIC_PALETTE, PLANARCONFIG_CONTIG):
//   case pack(PHOTOMETRIC_PALETTE, PLANARCONFIG_SEPARATE):
//     TIFFGetField(in, TIFFTAG_COLORMAP, &redl, &greenl, &bluel);
//     /*
//      * Convert 16-bit colormap to 8-bit (unless it looks
//      * like an old-style 8-bit colormap).
//      */
//     if (checkcmap(in, 1<<bitspersample, redl, greenl, bluel) == 16) {
//       int i;
// #define	CVT(x)		(((x) * 255L) / ((1L<<16)-1))
//       for (i = (1<<bitspersample)-1; i >= 0; i--) {
// 	redl[i] = CVT(redl[i]);
// 	greenl[i] = CVT(greenl[i]);
// 	bluel[i] = CVT(bluel[i]);
//       }
// #undef CVT
//     }
//     inbuf = (unsigned char *)_TIFFmalloc(TIFFScanlineSize(in));
//     for (im = 0; im < nf; im++)
//       {
// 	switch_frame(oi,im);
// 	pd = oi->im.pixel;
// 	for (row = 0; row < h; row++) {
// 	  if (TIFFReadScanline(in, inbuf, row, 0) < 0)
// 	    break;
// 	  compresspalette(pd[ony - row - 1].ch, inbuf, w, redl, greenl, bluel);
// 	}
// 	TIFFReadDirectory(in);
//       }
//     //win_printf("after compresspalette ");
//     break;
//   case pack(PHOTOMETRIC_RGB, PLANARCONFIG_CONTIG):
//     inbuf = (unsigned char *)_TIFFmalloc(TIFFScanlineSize(in));
//     for (im = 0; im < nf; im++)
//       {
// 	switch_frame(oi,im);
// 	pd = oi->im.pixel;
// 	for (row = 0; row < h; row++) {
// 	  if (TIFFReadScanline(in, inbuf, row, 0) < 0)
// 	    break;
// 	  compresscontig(pd[ony - row - 1].ch, inbuf, w);
// 	}
// 	TIFFReadDirectory(in);
//       }
//     //win_printf("after compresscontig ");
//     break;
//   case pack(PHOTOMETRIC_RGB, PLANARCONFIG_SEPARATE):
//     rowsize = TIFFScanlineSize(in);
//     inbuf = (unsigned char *)_TIFFmalloc(3*rowsize);
//     for (im = 0; im < nf; im++)
//       {
// 	switch_frame(oi,im);
// 	pd = oi->im.pixel;
// 	for (row = 0; row < h; row++) {
// 	  for (s = 0; s < 3; s++)
// 	    if (TIFFReadScanline(in,
// 				 inbuf+s*rowsize, row, s) < 0)
// 	      return (-1);
// 	  compresssep(pd[ony - row - 1].ch, inbuf, inbuf+rowsize, inbuf+2*rowsize, w);
// 	}
// 	TIFFReadDirectory(in);
//       }
//     //win_printf("after compresssep ");
//     break;
//   default :
//     tiff_read_rgba_file(oi, in);
//     break;
//   }
// #undef pack
//   TIFFClose(in);
//   if (inbuf != NULL)  _TIFFfree(inbuf);
//   //_fmode = O_TEXT;
//   //	reset_cursor();
//   oi->width = (float)onx/512;
//   oi->height = (float)ony/512;
//   oi->iopt2 &= ~X_NUM;		oi->iopt2 &= ~Y_NUM;
//   oi->im.source = Mystrdupre(oi->im.source,backslash_to_slash(file));
//   set_RGB_zmin_zmax_values(oi, 0, 255, 0, 255, 0, 255);
//   //find_zmin_zmax(oi);
//   return (0);
// }
//
int load_tiff_file_in_imreg(imreg *imr, char *file, char *path)
{
  O_i *oi = NULL;
  char fi_in[256] = {0};

  if (imr == NULL || file == NULL || path == NULL)		return OFF;
  if (imr->n_oi == 1 && imr->one_i->im.pixel == NULL) 	oi = imr->one_i;
  else
    {
      oi = (O_i *)calloc(1,sizeof(O_i));
      init_one_image(oi, IS_CHAR_IMAGE);
      add_image (imr, IS_CHAR_IMAGE, (void *)oi);
    }
  oi->iopt |= NOAXES;
  sprintf(fi_in,"%s%s",path,file);
  imr->cur_oi = imr->n_oi - 1;
  imr->one_i = imr->o_i[imr->cur_oi];
  if ( tiffreadfile(oi,fi_in) == 0)
    {
      oi->im.n_f = 1;
      oi->filename = Mystrdupre(oi->filename,file);
      oi->dir = Mystrdupre(oi->dir,path);
      /*		win_printf("source %s",(oi->im.source == NULL)?"null":oi->im.source);*/
      if (oi->im.source == NULL)		oi->im.source = strdup(file);
      /*		if (image_special_2_use != NULL) image_special_2_use(oi);*/
      do_one_image(imr);
      /*		refresh_image(imr, imr->n_oi - 1);	 */
    }
  else
    {
      remove_from_image (imr, oi->im.data_type, (void *)oi);
      win_printf("file %s \nwas not loaded ...!",backslash_to_slash(fi_in));
      return 1;
    }
  find_zmin_zmax(oi);

  return 0;
}

int load_tiff_stack_file_in_imreg_as_movie_on_disk(imreg *imr, char *file, char *path)
{
  int i, j, li, co, index, index_end;
  O_i *oi = NULL, *oit = NULL;
  union pix *pd = NULL, *ps = NULL;
  char fi_in[1024] = {0}, dir[512] = {0}, basename[256] = {0}, ex[128] = {0};
  FILE *fpi = NULL;

  if (imr == NULL || file == NULL || path == NULL)		return OFF;

  for (i = strlen(file)-1; i > 0 && file[i] != '.'; i--);
  if (file[i] != '.' || i < 1)
    return win_printf_OK("cannot find image number for %s !",file);
  for (i = i-1; i > 0 && isdigit(file[i]); i--);
  if (i < 1)
    return win_printf_OK("cannot find begining of image number for %s !",file);

  sscanf(file+i+1,"%d.%s",&index,ex);
  for (j = 0; j <= i; j++) basename[j] = file[j];
  basename[j] = 0;
  strcpy(dir, path);
  put_backslash(dir);

  for (index_end = index; ;index_end++)
    {
      snprintf(fi_in,1024,"%s%s%d.%s",dir,basename,index_end,ex);
      fpi = fopen(fi_in,"r");
      if (fpi == NULL)
	{
	  snprintf(fi_in,1024,"%s%s0%d.%s",dir,basename,index_end,ex);
	  fpi = fopen(fi_in,"r");
	  if (fpi == NULL)
	    {
	      snprintf(fi_in,1024,"%s%s00%d.%s",dir,basename,index_end,ex);
	      fpi = fopen(fi_in,"r");
	    }
	  if (fpi == NULL)
	    {
	      snprintf(fi_in,1024,"%s%s000%d.%s",dir,basename,index_end,ex);
	      fpi = fopen(fi_in,"r");
	    }
	}
      if (fpi == NULL) break;
      fclose(fpi);
    }
  index_end--;

  if (index_end <= 0)     return win_printf_OK("cannot find image nstack for %s !",file);

  snprintf(fi_in,1024,"I am going to read files starting at %s%d.%s"
	   " until final index  %s%d.%s \nending index %%d",basename,index,ex,basename,index_end,ex);
  i = win_scanf(fi_in,&index_end);
  if (i == WIN_CANCEL) return 0;

i  = index;

      if (oit != NULL) free_one_image(oit);
      oit = (O_i *)calloc(1,sizeof(O_i));
      init_one_image(oit, 0);
      snprintf(fi_in,1024,"%s%s%d.%s",dir,basename,i,ex);
      fpi = fopen(fi_in,"r");
      if (fpi == NULL)
	{
	  snprintf(fi_in,1024,"%s%s0%d.%s",dir,basename,i,ex);
	  fpi = fopen(fi_in,"r");
	  if (fpi == NULL)
	    {
	      snprintf(fi_in,1024,"%s%s00%d.%s",dir,basename,i,ex);
	      fpi = fopen(fi_in,"r");
	    }
	  if (fpi == NULL)
	    {
	      snprintf(fi_in,1024,"%s%s000%d.%s",dir,basename,i,ex);
	      fpi = fopen(fi_in,"r");
	    }
      if (fpi == NULL)
       {
         snprintf(fi_in,1024,"%s%s0000%d.%s",dir,basename,i,ex);
         fpi = fopen(fi_in,"r");
       }
	}
      if (fpi != NULL) fclose(fpi);
      if ( tiffreadfile(oit,fi_in) == 0)
	{

	      oi = create_one_movie(oit->im.nx, oit->im.ny, oit->im.data_type, 1);
	      oi->filename = Mystrdupre(oi->filename,basename);
        oi->filename_is = index;
        oi->fileext = strdup(ex);
	      oi->dir = Mystrdupre(oi->dir,path);
        oi->im.n_f = index_end - index + 1;
        if (movie_buffer_on_disk)
        {
          oi->im.movie_on_disk = 2;
          oi->im.n_f = -abs(oi->im.n_f);
          switch_frame_of_disk_movie_c = switch_frame_of_disk_TIFF_movie;

        }


	  pd = oi->im.pixel;
	  ps = oit->im.pixel;
	  for (li = 0; li < oit->im.ny; li++)
	    {
	      for (co = 0; co < oit->im.nx; co++)
		{
		  switch (oit->im.data_type)
		    {
		    case  IS_CHAR_IMAGE:
		      pd[li].ch[co] = ps[li].ch[co];
		      break;
		    case  IS_RGB_PICTURE:
		      pd[li].rgb[co] = ps[li].rgb[co];
		      break;
		    case  IS_RGBA_PICTURE:
		      pd[li].rgba[co] = ps[li].rgba[co];
		      break;
		    case  IS_RGB16_PICTURE:
		      pd[li].rgb16[co] = ps[li].rgb16[co];
		      break;
		    case  IS_RGBA16_PICTURE:
		      pd[li].rgba16[co] = ps[li].rgba16[co];
		      break;
		    case  IS_INT_IMAGE:
		      pd[li].in[co] = ps[li].in[co];
		      break;
		    case  IS_UINT_IMAGE:
		      pd[li].ui[co] = ps[li].ui[co];
		      break;
		    case  IS_LINT_IMAGE:
		      pd[li].li[co] = ps[li].li[co];
		      break;
		    case  IS_FLOAT_IMAGE:
		      pd[li].fl[co] = ps[li].fl[co];
		      break;
		    case  IS_COMPLEX_IMAGE:
		      pd[li].cp[co] = ps[li].cp[co];
		      break;
		    case  IS_DOUBLE_IMAGE:
		      pd[li].db[co] = ps[li].db[co];
		      break;
		    case  IS_COMPLEX_DOUBLE_IMAGE:
		      pd[li].dcp[co] = ps[li].dcp[co];
		      break;
		    }
		}
	    }
	}
      else
	{
	  win_printf("file %s \nwas not loaded ...!",backslash_to_slash(fi_in));
	}
      my_set_window_title("loading %s",fi_in);

  if (oit != NULL) free_one_image(oit);
  if (imr->n_oi == 1 && imr->one_i->im.pixel == NULL)
    {
      add_image (imr, oi->im.data_type, (void *)oi);
      remove_from_image (imr, imr->o_i[0]->im.data_type, (void *)imr->o_i[0]);
    }
  else
    {
      add_image (imr, oi->im.data_type, (void *)oi);
    }
  oi->iopt |= NOAXES;
  select_image_of_imreg(imr, imr->n_oi - 1);

  map_pixel_ratio_of_image_and_screen(oi, 1.0, 1.0);
  find_zmin_zmax(oi);
  return 0;
}



int load_tiff_stack_file_in_imreg_as_movie(imreg *imr, char *file, char *path)
{
  int i, j, li, co, index, index_end;
  O_i *oi = NULL, *oit = NULL;
  union pix *pd = NULL, *ps = NULL;
  char fi_in[1024] = {0}, dir[512] = {0}, basename[256] = {0}, ex[128] = {0};
  FILE *fpi = NULL;

  if (imr == NULL || file == NULL || path == NULL)		return OFF;

  for (i = strlen(file)-1; i > 0 && file[i] != '.'; i--);
  if (file[i] != '.' || i < 1)
    return win_printf_OK("cannot find image number for %s !",file);
  for (i = i-1; i > 0 && isdigit(file[i]); i--);
  if (i < 1)
    return win_printf_OK("cannot find begining of image number for %s !",file);

  sscanf(file+i+1,"%d.%s",&index,ex);
  for (j = 0; j <= i; j++) basename[j] = file[j];
  basename[j] = 0;
  strcpy(dir, path);
  put_backslash(dir);

  for (index_end = index; ;index_end++)
    {
      snprintf(fi_in,1024,"%s%s%d.%s",dir,basename,index_end,ex);
      fpi = fopen(fi_in,"r");
      if (fpi == NULL)
	{
	  snprintf(fi_in,1024,"%s%s0%d.%s",dir,basename,index_end,ex);
	  fpi = fopen(fi_in,"r");
	  if (fpi == NULL)
	    {
	      snprintf(fi_in,1024,"%s%s00%d.%s",dir,basename,index_end,ex);
	      fpi = fopen(fi_in,"r");
	    }
	  if (fpi == NULL)
	    {
	      snprintf(fi_in,1024,"%s%s000%d.%s",dir,basename,index_end,ex);
	      fpi = fopen(fi_in,"r");
	    }
	}
      if (fpi == NULL) break;
      fclose(fpi);
    }
  index_end--;

  if (index_end <= 0)     return win_printf_OK("cannot find image nstack for %s !",file);

  snprintf(fi_in,1024,"I am going to read files starting at %s%d.%s"
	   " until final index  %s%d.%s \nending index %%d",basename,index,ex,basename,index_end,ex);
  i = win_scanf(fi_in,&index_end);
  if (i == WIN_CANCEL) return 0;

  for (i = index ; i <= index_end; i++)
    {
      if (oit != NULL) free_one_image(oit);
      oit = (O_i *)calloc(1,sizeof(O_i));
      init_one_image(oit, 0);
      snprintf(fi_in,1024,"%s%s%d.%s",dir,basename,i,ex);
      fpi = fopen(fi_in,"r");
      if (fpi == NULL)
	{
	  snprintf(fi_in,1024,"%s%s0%d.%s",dir,basename,i,ex);
	  fpi = fopen(fi_in,"r");
	  if (fpi == NULL)
	    {
	      snprintf(fi_in,1024,"%s%s00%d.%s",dir,basename,i,ex);
	      fpi = fopen(fi_in,"r");
	    }
	  if (fpi == NULL)
	    {
	      snprintf(fi_in,1024,"%s%s000%d.%s",dir,basename,i,ex);
	      fpi = fopen(fi_in,"r");
	    }
	}
      if (fpi != NULL) fclose(fpi);
      if ( tiffreadfile(oit,fi_in) == 0)
	{
	  if (i == index)
	    {
	      oi = create_one_movie(oit->im.nx, oit->im.ny, oit->im.data_type, (index_end - index + 1));
	      oi->filename = Mystrdupre(oi->filename,basename);
	      oi->dir = Mystrdupre(oi->dir,path);
	    }
	  switch_frame(oi,i - index);
	  pd = oi->im.pixel;
	  ps = oit->im.pixel;
	  for (li = 0; li < oit->im.ny; li++)
	    {
	      for (co = 0; co < oit->im.nx; co++)
		{
		  switch (oit->im.data_type)
		    {
		    case  IS_CHAR_IMAGE:
		      pd[li].ch[co] = ps[li].ch[co];
		      break;
		    case  IS_RGB_PICTURE:
		      pd[li].rgb[co] = ps[li].rgb[co];
		      break;
		    case  IS_RGBA_PICTURE:
		      pd[li].rgba[co] = ps[li].rgba[co];
		      break;
		    case  IS_RGB16_PICTURE:
		      pd[li].rgb16[co] = ps[li].rgb16[co];
		      break;
		    case  IS_RGBA16_PICTURE:
		      pd[li].rgba16[co] = ps[li].rgba16[co];
		      break;
		    case  IS_INT_IMAGE:
		      pd[li].in[co] = ps[li].in[co];
		      break;
		    case  IS_UINT_IMAGE:
		      pd[li].ui[co] = ps[li].ui[co];
		      break;
		    case  IS_LINT_IMAGE:
		      pd[li].li[co] = ps[li].li[co];
		      break;
		    case  IS_FLOAT_IMAGE:
		      pd[li].fl[co] = ps[li].fl[co];
		      break;
		    case  IS_COMPLEX_IMAGE:
		      pd[li].cp[co] = ps[li].cp[co];
		      break;
		    case  IS_DOUBLE_IMAGE:
		      pd[li].db[co] = ps[li].db[co];
		      break;
		    case  IS_COMPLEX_DOUBLE_IMAGE:
		      pd[li].dcp[co] = ps[li].dcp[co];
		      break;
		    }
		}
	    }
	}
      else
	{
	  win_printf("file %s \nwas not loaded ...!",backslash_to_slash(fi_in));
	  break;
	}
      my_set_window_title("loading %s",fi_in);
    }
  if (oit != NULL) free_one_image(oit);
  if (imr->n_oi == 1 && imr->one_i->im.pixel == NULL)
    {
      add_image (imr, oi->im.data_type, (void *)oi);
      remove_from_image (imr, imr->o_i[0]->im.data_type, (void *)imr->o_i[0]);
    }
  else
    {
      add_image (imr, oi->im.data_type, (void *)oi);
    }
  oi->iopt |= NOAXES;
  map_pixel_ratio_of_image_and_screen(oi, 1.0, 1.0);
  switch_frame(oi,0);
  find_zmin_zmax(oi);
  return 0;
}


int write_tiff_file (O_i *oi, char *fullname)
{
  TIFF *out = NULL;
  union pix *pd = NULL;
  tdata_t *buf = NULL;
  int ony, row;

  if ( oi->im.data_type == IS_DOUBLE_IMAGE
      || oi->im.data_type == IS_COMPLEX_DOUBLE_IMAGE
      || oi->im.data_type == IS_COMPLEX_IMAGE
      //|| oi->im.data_type == IS_INT_IMAGE
      || oi->im.data_type == IS_LINT_IMAGE)
    {
      win_printf("Cannot save yet this image type!");
      return 1;
    }

  //_fmode = O_BINARY;
  out = TIFFOpen(fullname, "w");
  if (out == NULL)
    {
      win_printf("Cannot open tiff image!\n%s",backslash_to_slash(fullname));
      //_fmode = O_TEXT;
      return D_O_K;
    }

  TIFFSetField(out, TIFFTAG_IMAGEWIDTH,  oi->im.nx);
  TIFFSetField(out, TIFFTAG_IMAGELENGTH, oi->im.ny);
  TIFFSetField(out, TIFFTAG_PLANARCONFIG, 1);
  if (oi->im.data_type == IS_CHAR_IMAGE || oi->im.data_type == IS_RGB_PICTURE
      || oi->im.data_type == IS_RGBA_PICTURE)
    TIFFSetField(out, TIFFTAG_BITSPERSAMPLE, 8);
  else 	if (oi->im.data_type == IS_UINT_IMAGE || oi->im.data_type == IS_INT_IMAGE)
    TIFFSetField(out, TIFFTAG_BITSPERSAMPLE, 16);
  else if (oi->im.data_type == IS_FLOAT_IMAGE)
    TIFFSetField(out,TIFFTAG_BITSPERSAMPLE,8*sizeof(float));

  TIFFSetField(out, TIFFTAG_COMPRESSION, compression);
  TIFFSetField(out, TIFFTAG_FILLORDER, fillorder);
  TIFFSetField(out, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);
  if (oi->im.data_type == IS_FLOAT_IMAGE) TIFFSetField(out,TIFFTAG_SAMPLEFORMAT,SAMPLEFORMAT_IEEEFP);
  if (oi->im.data_type == IS_CHAR_IMAGE || oi->im.data_type == IS_UINT_IMAGE || oi->im.data_type == IS_INT_IMAGE || oi->im.data_type == IS_FLOAT_IMAGE)
    {
      TIFFSetField(out, TIFFTAG_SAMPLESPERPIXEL, 1);
      TIFFSetField(out, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);
    }
  else if (oi->im.data_type == IS_RGB_PICTURE)
    {
      TIFFSetField(out, TIFFTAG_SAMPLESPERPIXEL, 3);
      TIFFSetField(out, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB);
    }
  else if (oi->im.data_type == IS_RGBA_PICTURE)
    {
      //    win_printf("RGBA picture");
      TIFFSetField(out, TIFFTAG_SAMPLESPERPIXEL, 4);
      TIFFSetField(out, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB);
    }

  TIFFSetField(out, TIFFTAG_MINSAMPLEVALUE, (short) oi->z_min);
  TIFFSetField(out, TIFFTAG_MAXSAMPLEVALUE, (short) oi->z_max);
  TIFFSetField(out, TIFFTAG_SOFTWARE, "Xvin 0.2");
  if (oi->title != NULL)
    {
      TIFFSetField(out, TIFFTAG_IMAGEDESCRIPTION, oi->title);
      TIFFSetField(out, TIFFTAG_DOCUMENTNAME, oi->title);
    }
  ony = oi->im.ny;
  pd = oi->im.pixel;
  for (row = 0; row < ony; row++)
    {
      if (oi->im.data_type == IS_FLOAT_IMAGE)
        buf =  (tdata_t)(pd[ony - (int)row - 1].fl);
      else
        buf =  (tdata_t)(pd[ony - (int)row - 1].ch);
      if (TIFFWriteScanline(out, buf, (uint32)row, 0) < 0)
	return 1;
    }

  (void) TIFFClose(out);
  //_fmode = O_TEXT;
  return D_O_K;
}







int write_movie_in_tiff_file (O_i *oi, char *fullname)
{
  TIFF *out = NULL;
  union pix *pd = NULL;
  tdata_t *buf = NULL;
  int ony, row, im;
  int start_frame = 0;

  //_fmode = O_BINARY;
  out = TIFFOpen(fullname, "w8");
  if (out == NULL)
    {
      win_printf("Cannot open tiff image!\n%s",backslash_to_slash(fullname));
      //_fmode = O_TEXT;
      return D_O_K;
    }



  if (oi == NULL || fullname == NULL) return 1;
  int i = sscanf(oi->im.source,"started at frame %d",&start_frame);
  if (i != 1) win_printf_OK("no starting frame found for sync\n");
  for (im = 0; im < abs(oi->im.n_f); im++)
    {
      display_title_message("image %d",im);

       switch_frame(oi,im);
      //TIFFSetField(out, TIFFTAG_PAGENUMBER, im, im);
      //TIFFSetField(tiff, TIFFTAG_SUBFILETYPE, FILETYPE_PAGE);

      if (oi->im.data_type == IS_FLOAT_IMAGE
	  || oi->im.data_type == IS_DOUBLE_IMAGE
	  || oi->im.data_type == IS_COMPLEX_DOUBLE_IMAGE
	  || oi->im.data_type == IS_COMPLEX_IMAGE
	  //|| oi->im.data_type == IS_INT_IMAGE
	  || oi->im.data_type == IS_LINT_IMAGE)
	{
	  win_printf("Cannot save yet this image type!");
	  return 1;
	}





      TIFFSetField(out, TIFFTAG_IMAGEWIDTH,  oi->im.nx);
      TIFFSetField(out, TIFFTAG_IMAGELENGTH, oi->im.ny);
      TIFFSetField(out, TIFFTAG_PLANARCONFIG, 1);
      if (oi->im.data_type == IS_CHAR_IMAGE || oi->im.data_type == IS_RGB_PICTURE
	  || oi->im.data_type == IS_RGBA_PICTURE)
	TIFFSetField(out, TIFFTAG_BITSPERSAMPLE, 8);
      else 	if (oi->im.data_type == IS_UINT_IMAGE || oi->im.data_type == IS_INT_IMAGE)
	TIFFSetField(out, TIFFTAG_BITSPERSAMPLE, 16);
      TIFFSetField(out, TIFFTAG_COMPRESSION, compression);
      TIFFSetField(out, TIFFTAG_FILLORDER, fillorder);
      TIFFSetField(out, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);
      if (oi->im.data_type == IS_CHAR_IMAGE || oi->im.data_type == IS_UINT_IMAGE || oi->im.data_type == IS_INT_IMAGE)
	{
	  TIFFSetField(out, TIFFTAG_SAMPLESPERPIXEL, 1);
	  TIFFSetField(out, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);
	}
      else if (oi->im.data_type == IS_RGB_PICTURE)
	{
	  TIFFSetField(out, TIFFTAG_SAMPLESPERPIXEL, 3);
	  TIFFSetField(out, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB);
	}
      else if (oi->im.data_type == IS_RGBA_PICTURE)
	{
	  //    win_printf("RGBA picture");
	  TIFFSetField(out, TIFFTAG_SAMPLESPERPIXEL, 4);
	  TIFFSetField(out, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB);
	}

      TIFFSetField(out, TIFFTAG_MINSAMPLEVALUE, (short) oi->z_min);
      TIFFSetField(out, TIFFTAG_MAXSAMPLEVALUE, (short) oi->z_max);
      TIFFSetField(out, TIFFTAG_SOFTWARE, "Xvin 0.2");
      if (oi->title != NULL)
	{
	  TIFFSetField(out, TIFFTAG_IMAGEDESCRIPTION,oi->im.source);
	  TIFFSetField(out, TIFFTAG_DOCUMENTNAME, oi->title);
	}
      ony = oi->im.ny;
      pd = oi->im.pixel;
      for (row = 0; row < ony; row++)
	{
	  buf =  (tdata_t)(pd[ony - (int)row - 1].ch);
	  if (TIFFWriteScanline(out, buf, (uint32)row, 0) < 0)
	    return 1;
	}
      TIFFWriteDirectory(out);
    }
  (void) TIFFClose(out);
  //_fmode = O_TEXT;
  return D_O_K;
}
