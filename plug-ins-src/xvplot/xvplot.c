/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _XVPLOT_C_
#define _XVPLOT_C_

# include "stdlib.h"
# include "string.h"
# include "ctype.h"

# include "allegro.h"
# include "xvin.h"
# include "xvplot.h"



float **xvpdata = NULL;
char **xvpname = NULL;
int n_col = 0, n_line = 0;
int cur_x_index = 0, cur_y_index = 0;
FILE *fp;

int last_x_index = 0;
int last_y_index = 1;
int last_y_index_e = -1;

char *current_write_open_xvfile = NULL;
MENU XVplot_mn[32] = {{0}}, *XVplot_plot_menu = NULL;
char XVplot_mn_str[32][64] = {{0}};


int    load_xvplot_file_as_gr(void);
int    load_xvplot_file_as_gr_multi(void);
int    do_xvplot_n(void);
int    add_data_to_multi_d_s(multi_d_s *mds, float *f);

char *my_fgets(char *line, int ln_line, FILE *lfp)
{
  int i, j;
  if (lfp == NULL || line == NULL || ln_line <= 0) return NULL;
  for (i = 0, j = fgetc(lfp); i < ln_line-1 && j != '\n' && j != '\r' && j != EOF; j = fgetc(lfp), i++)
    line[i] = (unsigned char)j;
  if (i == 0 && j == EOF) return NULL;
  line[i] = 0;
  return line;
}
int    xvplot_file(char *file, int mode)
{
  register int i, j, k;
    char line[MAX_LINE_LENGTH] = {0}, tline[MAX_LINE_LENGTH] = {0}, vline[MAX_LINE_LENGTH] = {0}, *pl = NULL, comastr[MAX_LINE_LENGTH] = {0};
    char *pline, *tok, *f_l;
    int ncol, nline, haspercent; // ok
    float *fline;
    pltreg *pr = NULL;
    multi_d_s *mds = NULL;

    if (file == NULL)
    {
        return 1;
    }

    if ((fp = fopen(file, "r")) == NULL)
    {
        return win_printf_OK("Cannot find file %s", file);
    }

    if (my_fgets(line, MAX_LINE_LENGTH, fp) == NULL)
    {
        return win_printf_OK("empty file");
    }
    //win_printf("Opening\n %s\n%s\n",backslash_to_slash(file),line);
    if ((i = strcspn(line, "%")) >= (int)strlen(line))
      {  // no % in line
        /*        return win_printf_OK("first line should start with percent"); */
        strcpy(tline, line);

        /* just a number table - simple format */
        if (!strpbrk(line, "0123456789.,"))       /* i add the point vc */
        {
            win_printf_OK("skipping empty line");

            if (my_fgets(line, MAX_LINE_LENGTH, fp) == NULL)
            {
                return win_printf_OK("empty file");
            }
        }

        /* finding number of ds */
        for (tok = strtok(tline, SEPCHARS), ncol = 0 ; tok;
                tok = strtok(NULL, SEPCHARS), ncol++);

        ncol += 1;

	//win_printf("Found %d col",ncol);
        if ((mds = create_multi_d_s(ncol)) == NULL)
        {
            return win_printf_OK("pas de mem");
        }

        mds->source = strdup(backslash_to_slash(file));
        sprintf(mds->name[0], "# index");

        for (j = 1; j < ncol; j++)
        {
            sprintf(mds->name[j], "data %d", j);
        }
	//win_printf("last col %d %s",ncol-1,mds->name[ncol-1]);
	
        if (!(fline = (float *) calloc(mds->n_d_s, sizeof(float))))
        {
            return win_printf_OK("a pu de memoire");
        }

        if (!strpbrk(line, "0123456789.,"))       /* i add the point vc */
        {
            return win_printf_OK("No readable line");
        }

        nline = 0;

        do
        {
            for (tok = strtok(line, SEPCHARS), ncol = 1 ; tok &&
                    ncol < mds->n_d_s ; tok = strtok(NULL, SEPCHARS), ncol++)
            {
	      for(k = 0; tok[k] > 0; k++) // comma => point
		comastr[k] = (tok[k] == ',') ? '.' : tok[k];
	      comastr[k] = 0;
	      fline[ncol] = atof(comastr); // tok
            }

            fline[0] = nline++;
            add_data_to_multi_d_s(mds, fline); // ok =

            for (f_l = my_fgets(line, MAX_LINE_LENGTH, fp);
                    (f_l != NULL) && (strpbrk(line, "0123456789.,") == NULL);
                    f_l = my_fgets(line, MAX_LINE_LENGTH, fp));

            /* i add the point vc */
        }
        while (f_l != NULL && !feof(fp));

        display_title_message("file %s scanned\n %d columns found\n"
                              "added index as first one\n %d lines", backslash_to_slash(file),
                              mds->n_d_s - 1, nline);
    }
    else
    {
      //win_printf("found %% in first line");
      strcpy(tline, line);

        for (pl = line, haspercent = 1; haspercent == 1 && pl != NULL
                ; pl = my_fgets(line, MAX_LINE_LENGTH, fp))
        {
            for (j = 0; j < (int)strlen(line) && isspace(line[j]); j++);

            if (line[j] == '%')
            {
                haspercent = 1;
                strcpy(tline, line);
            }
            else
            {
                haspercent = 0;
		break;
            }
	    //win_printf("read line of %d ch: haspercent %d\n%s",(int)strlen(line),haspercent,line+1);
        }

        for (j = 0; j < (int)strlen(tline) && isspace(tline[j]); j++);

        pline = (tline[j] == '%') ? tline + j + 1 : tline + j;
        strcpy(vline, pline);

        for (j = 0; j < (int)strlen(vline); j++)
        {
            vline[j] = (vline[j] == ':') ? ' ' : vline[j];
        }

        for (tok = my_strtok(pline), ncol = 0; tok ; tok = my_strtok(NULL), ncol++);

        ncol += 1; // for index

        //win_printf("%d col line\n %s",ncol,pline);
        if ((mds = create_multi_d_s(ncol)) == NULL)
        {
            return win_printf_OK("pas de mem");
        }

        mds->source = strdup(backslash_to_slash(file));
        sprintf(mds->name[0], "# index");

        for (tok = my_strtok(vline), ncol = 1 ; tok ; tok = my_strtok(NULL), ncol++)
        {
            strcpy(mds->name[ncol], tok);
        }

        /* take care of second line (should skip blank lines)*/
        //  if ( fgets(line, MAX_LINE_LENGTH, fp) == NULL)
        //     return win_printf_OK("empty file");

        if (!(fline = (float *) malloc(mds->n_d_s * sizeof(float))))
        {
            return win_printf_OK("a pu de memoire");
        }

        if (!strpbrk(line, "0123456789.,"))       /* i add the point vc */
        {
            win_printf_OK("empty line skipped");
        }

        nline = 0;

        do
        {
            fline[0] = nline;
	    //win_printf("treating %d ch:\n%s",(int)strlen(line),line);
            for (tok = strtok(line, SEPCHARS), ncol = 1 ; tok &&
                    ncol < mds->n_d_s ; tok = strtok(NULL, SEPCHARS), ncol++)
            {
	      for(k = 0; tok[k] > 0; k++) // comma => point
		comastr[k] = (tok[k] == ',') ? '.' : tok[k];
	      comastr[k] = 0;

	      fline[ncol] = atof(comastr); // tok
            }
	    //win_printf ("adding line %d with %g; %g; %g",nline, fline[0], fline[1], fline[2]);
            add_data_to_multi_d_s(mds, fline); // ok =
            nline++;

            for (f_l = my_fgets(line, MAX_LINE_LENGTH, fp);
                    (f_l != NULL) && (strpbrk(line, "0123456789.") == NULL);
                    f_l = my_fgets(line, MAX_LINE_LENGTH, fp));

            /* i add the point vc */
        }
        while (f_l != NULL && !feof(fp));

        display_title_message("file %s scanned\n %d columns found \n %d lines",
                              backslash_to_slash(file), mds->n_d_s, nline);
    }

    if (mode == 0)
    {
        if ((pr = create_multi_d_s_pltreg(mds)) == NULL)
        {
            return win_printf_OK("cannot create good plot reg");
        }
    }
    else
    {
        create_Op_from_multi_d_s(pr, mds);
        free_multi_d_s(mds);
    }

    fclose(fp);
    broadcast_dialog_message(MSG_DRAW, 0);
    return 0;
}

/*
int debug_mn_index(void)
{
}
*/

MENU *xvplot_plot_menu(multi_d_s *mds)
{
    register int i, j, i_mnx, i_mny;
    char *xvfile = NULL;
    static MENU mnx[16] = {0}, mny[16] = {0};
    static char mnx_str[16][64] = {0}, mny_str[16][64] = {0};
    static MENU mnxi[16][16] = {0}, mnyi[16][16] = {0};
    
    XVplot_mn[0].text = NULL;
    for (i = 0; i < 16; i++)
    {
        if ((mnx[i].text != NULL) && (strcmp(mnx[i].text, "X groupe") == 0))
        {
            free(mnx[i].text);
        }

        if ((mny[i].text != NULL) && (strcmp(mny[i].text, "Y groupe") == 0))
        {
            free(mny[i].text);
        }

        XVplot_mn[i].text = mnx[i].text = mny[i].text = NULL;

        for (j = 0; j < 16; j++)
        {
            mnxi[i][j].text = mnyi[i][j].text = NULL;
        }
    }

    add_item_to_menu(XVplot_mn, "load XV speadsheet in a new project", load_xvplot_file, NULL, 0, NULL);
    add_item_to_menu(XVplot_mn, "load XV speadsheet(s) as simple plot(s)", load_xvplot_file_as_gr_multi, NULL, 0, NULL);

    if (mds == NULL)
    {
        return XVplot_mn;
    }

    xvfile = get_filename(mds->source);

    if (mds->n_d_s < 16)
    {
        add_item_to_menu(mnx, mds->name[0], set_x_data, NULL, MENU_INDEX(0), NULL);

        for (i = 1 ; i < mds->n_d_s ; i++)
        {
            add_item_to_menu(mnx, mds->name[i], set_x_data, NULL, MENU_INDEX(i), NULL);
        }
    }
    else
    {
        for (i = 0, i_mnx = 0 ; i < mds->n_d_s ; i++)
        {
            if ((i % 16) == 0)
            {
                snprintf(mnx_str[i_mnx], 64, "X groupe %d", i / 16);
                add_item_to_menu(mnx, mnx_str[i_mnx], NULL, mnxi[i / 16], 0, NULL);
                add_item_to_menu(mnxi[i / 16], mds->name[i], set_x_data, NULL,
                                 MENU_INDEX(i), NULL);
            }
            else    add_item_to_menu(mnxi[i / 16], mds->name[i], set_x_data,
                                         NULL, MENU_INDEX(i), NULL);
        }
    }

    if (mds->n_d_s < 16)
    {
        add_item_to_menu(mny, mds->name[0], set_y_data, NULL, MENU_INDEX(0), NULL);

        for (i = 1 ; i < mds->n_d_s ; i++)
        {
            add_item_to_menu(mny, mds->name[i], set_y_data, NULL, MENU_INDEX(i), NULL);
        }
    }
    else
    {
        for (i = 0, i_mny = 0 ; i < mds->n_d_s ; i++)
        {
            if ((i % 16) == 0)
            {
                snprintf(mny_str[i_mny], 64, "Y groupe %d", i / 16);
                add_item_to_menu(mny, mny_str[i_mny], NULL, mnyi[i / 16], 0, NULL);
                add_item_to_menu(mnyi[i / 16], mds->name[i], set_y_data, NULL,
                                 MENU_INDEX(i), NULL);
            }
            else    add_item_to_menu(mnyi[i / 16], mds->name[i], set_y_data,
                                         NULL, MENU_INDEX(i), NULL);
        }
    }

    snprintf(XVplot_mn_str[4], 64, "Select X axis for %s", xvfile);
    add_item_to_menu(XVplot_mn, XVplot_mn_str[4], NULL, mnx, 0, NULL);
    snprintf(XVplot_mn_str[5], 64, "Select Y axis for %s", xvfile);
    add_item_to_menu(XVplot_mn, XVplot_mn_str[5], NULL, mny, 0, NULL);
    snprintf(XVplot_mn_str[6], 64, "Plot (%s) vs (%s) of %s", mds->name[last_x_index], mds->name[last_y_index], xvfile);
    add_item_to_menu(XVplot_mn, XVplot_mn_str[6], do_xvplot, NULL, 0, NULL);
    snprintf(XVplot_mn_str[6], 64, "Plot (%s) vs (%s) of %s", mds->name[last_x_index], mds->name[last_y_index], xvfile);
    add_item_to_menu(XVplot_mn, XVplot_mn_str[6], do_xvplot, NULL, 0, NULL);
    add_item_to_menu(XVplot_mn, "Multi columns plot", do_xvplot_n, NULL, 0, NULL);


    //   add_item_to_menu(XVplot_mn,"debug mn index", debug_mn_index, NULL,0,NULL);
    return XVplot_mn;
}

int do_xvplot(void)
{
    register int i;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    multi_d_s *mds = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (ac_grep(cur_ac_reg, "%pr", &pr) != 1)
    {
        return win_printf_OK("ouk la pltreg ?");
    }

    if ((mds = find_multi_d_s_in_pltreg(pr)) == NULL)
    {
        return win_printf_OK("not a xv_plot plot region");
    }

    if ((op = create_and_attach_one_plot(pr, mds->n, mds->n, 0)) == NULL)
    {
        return OFF;
    }

    if ((ds = op->dat[0]) == NULL)
    {
        return OFF;
    }

    for (i = 0 ; i < mds->n ; i++)
    {
        ds->xd[i] = mds->x[mds->x_index][i];
        ds->yd[i] = mds->x[mds->y_index][i];
    }

    ds->source = my_sprintf(NULL, "file %s x-> col %d y -> col %d",
                            (mds->source != NULL) ? mds->source : "unknown", mds->x_index, mds->y_index);
    ds->nx = ds->ny = mds->n;
    set_plot_x_title(op, "%s", mds->name[mds->x_index]);
    set_plot_y_title(op, "%s", mds->name[mds->y_index]);
    set_op_filename(op, "%s",get_filename(mds->source));
    return refresh_plot(pr, pr->n_op - 1);
}

int do_xvplot_n(void)
{
    register int i;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    multi_d_s *mds = NULL;
    int x_ind, y_ind_s, y_ind_e, ids;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (ac_grep(cur_ac_reg, "%pr", &pr) != 1)
    {
        return win_printf_OK("ouk la pltreg ?");
    }

    if ((mds = find_multi_d_s_in_pltreg(pr)) == NULL)
    {
        return win_printf_OK("not a xv_plot plot region");
    }
    x_ind = mds->x_index;
    y_ind_s = mds->y_index;
    y_ind_e = (mds->y_index_e < 0) ? mds->n_d_s -1 : mds->y_index_e;
    i = win_scanf("Multi columns plot:"
		  "Choose the columns index of X %4d\n"
		  "Choose the index range of Y [%4d,%4d}\n"
		  ,&x_ind,&y_ind_s,&y_ind_e);
    if (i == WIN_CANCEL)  return 0;
    if (x_ind < 0 || x_ind >= mds->n_d_s) 
      return win_printf_OK("Idex x %d out of range [0,%d[\n",x_ind, mds->n_d_s);
    if (y_ind_e < 0 || y_ind_e >= mds->n_d_s) 
      last_y_index_e = mds->y_index_e = y_ind_e;
    for (ids = y_ind_s; ids <= y_ind_e; ids++)
      {
	if (ids < 0 || ids >= mds->n_d_s) continue;
	if (op == NULL)
	  {
	    if ((op = create_and_attach_one_plot(pr, mds->n, mds->n, 0)) == NULL)
	      {
		return OFF;
	      }
	    
	    if ((ds = op->dat[0]) == NULL)
	      {
		return OFF;
	      }
	  }
	if (ds == NULL) 
	  {
	    if ((ds = create_and_attach_one_ds(op, mds->n, mds->n, 0)) == NULL)
	      {
		return OFF;
	      }
	  }
	
	for (i = 0 ; i < mds->n ; i++)
	  {
	    ds->xd[i] = mds->x[x_ind][i];
	    ds->yd[i] = mds->x[ids][i];
	  }
	
	ds->source = my_sprintf(NULL, "file %s x-> col %d y -> col %d",
				(mds->source != NULL) ? mds->source : "unknown", x_ind, ids);
	ds->nx = ds->ny = mds->n;
	ds = NULL;
      }
    set_plot_x_title(op, "%s", mds->name[x_ind]);
    set_plot_y_title(op, "[%d(%s),%d(%s)]", y_ind_s,mds->name[y_ind_s], 
    y_ind_e,mds->name[y_ind_e]);
    set_op_filename(op, "%s", get_filename(mds->source));
    return refresh_plot(pr, pr->n_op - 1);
}

int         set_x_data(void)
{
    multi_d_s *mds = NULL;
    pltreg *pr = NULL;
    int index;
    pr = find_pr_in_current_dialog(NULL);
    mds = find_multi_d_s_in_pltreg(pr);
    index = RETRIEVE_MENU_INDEX;

    if (updating_menu_state != 0)
    {
        if (mds != NULL && mds->x_index == index)
        {
            active_menu->flags |=  D_SELECTED;
        }
        else
        {
            active_menu->flags &= ~D_SELECTED;
        }

        if (mds != NULL && mds->x_index > 16 && XVplot_mn[2].child != NULL)
        {
            if (mds->x_index >= index && mds->x_index < index + 16)
            {
                XVplot_mn[2].child[index / 16].flags |=  D_SELECTED;
            }

            XVplot_mn[2].child[index / 16].flags &=  ~D_SELECTED;
        }

        return D_O_K;
    }

    if (mds != NULL)
    {
        //win_printf("last %d mdsx %d index %d",last_x_index, mds->x_index, index);
        last_x_index = mds->x_index = index;
        XVplot_plot_menu = xvplot_plot_menu(mds);
    }

    return D_O_K;
}

int         set_y_data(void)
{
    multi_d_s *mds = NULL;
    pltreg *pr = NULL;
    int index;
    pr = find_pr_in_current_dialog(NULL);
    mds = find_multi_d_s_in_pltreg(pr);
    index = RETRIEVE_MENU_INDEX;

    if (updating_menu_state != 0)
    {
        if (mds != NULL && mds->y_index == index)
        {
            active_menu->flags |=  D_SELECTED;
        }
        else
        {
            active_menu->flags &= ~D_SELECTED;
        }

        if (mds != NULL && mds->y_index > 16 && XVplot_mn[3].child != NULL)
        {
            if (mds->y_index >= index && mds->y_index < index + 16)
            {
                XVplot_mn[3].child[index / 16].flags |=  D_SELECTED;
            }

            XVplot_mn[3].child[index / 16].flags &=  ~D_SELECTED;
        }

        return D_O_K;
    }

    if (mds != NULL)
    {
        //win_printf("last %d mdsy %d index %d",last_y_index, mds->y_index, index);
        last_y_index = mds->y_index = index;
        XVplot_plot_menu = xvplot_plot_menu(mds);
    }

    //else win_printf("mds = NULL");
    return D_O_K;
}

multi_d_s    *find_multi_d_s_in_pltreg(pltreg *pr)
{
    if (!pr)
    {
        return NULL;
    }

    if (pr->use.to == IS_MULTI_D_S)
    {
        return (multi_d_s *)pr->use.stuff;
    }

    return NULL;
}

multi_d_s    *create_multi_d_s(int n_d_s)
{
    register int    i, j;
    multi_d_s    *mds = NULL;
    float        **x = NULL;
    char        **name = NULL, *y_ind = NULL;
    int   *sel = NULL;
    
    mds = (multi_d_s *) calloc(1, sizeof(multi_d_s));

    if (!mds)
    {
        return NULL;
    }

    x = (float **) calloc(n_d_s, sizeof(float *));
    name = (char **) calloc(n_d_s,  sizeof(char *));
    sel = (int *) calloc(n_d_s, sizeof(int));
    y_ind = (char *) calloc(n_d_s,  sizeof(char));
    
    if (x == NULL || name == NULL || sel == NULL || y_ind == NULL)
    {
      if (mds != NULL) free(mds);
      if (x != NULL) free(x);
      if (name != NULL) free(name);
      if (sel != NULL) free(sel);
      if (y_ind == NULL) free(y_ind);
      return NULL;
    }
    
    for (i = 0 ; i < n_d_s ; i++)
    {
        x[i] = (float *) calloc(1024, sizeof(float));
        name[i] = (char *) calloc(32, sizeof(char));

        if (!x[i] || !name[i])
        {
            for (j = i ; j >= 0 ; j--)
            {
	      if (x[j] != NULL) free(x[j]);
              if (name[j] != NULL)  free(name[j]);
            }
	    if (mds != NULL) free(mds);
	    if (x != NULL) free(x);
	    if (name != NULL) free(name);
	    if (sel != NULL) free(sel);
	    if (y_ind == NULL) free(y_ind);
            return NULL;
        }
    }

    mds->n_d_s    = n_d_s;
    mds->n        = 0;
    mds->m        = 1024;
    mds->x_index = (last_x_index < n_d_s) ? last_x_index : 0;
    mds->y_index = (last_y_index < n_d_s) ? last_y_index : (n_d_s > 1) ? 1 : 0;
    mds->y_index_e = (last_y_index_e < n_d_s) ? last_y_index_e : -1;
    mds->source    = NULL;
    mds->name    = name;
    mds->y_ind    = y_ind;
    mds->x        = x;
    mds->sel        = sel;
    return mds;
}

void free_multi_d_s(multi_d_s *mds)
{
    register int i;

    if (!mds)
    {
        return;
    }

    for (i = 0 ; i < mds->n_d_s ; i++)
    {
      if (mds->x[i] != NULL) free(mds->x[i]);
      if (mds->name[i] != NULL)  free(mds->name[i]);
    }

    if (mds->source != NULL) free(mds->source);
    if (mds->sel != NULL) free(mds->sel);
    if (mds->y_ind != NULL) free(mds->y_ind);
    free(mds);
    return;
}

pltreg        *create_multi_d_s_pltreg(multi_d_s *mds)
{
    register int i;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    char file[512] = {0}, *f = NULL;

    if (!mds)
    {
        return NULL;
    }

    pr = create_and_register_new_plot_project(0,   32,  900,  668);

    if (pr == NULL)
    {
        return NULL;
    }

    op = pr->one_p;
    pr->use.to = IS_MULTI_D_S;
    pr->use.stuff = (void *)mds;
    f = extract_file_name(file, 512, mds->source);

    if (f != NULL)
    {
        pr->filename = strdup(f);
    }

    f = extract_file_path(file, 512, mds->source);

    if (f != NULL)
    {
        strcat(file, "\\");
        pr->path = strdup(f);
    }

    ds = build_adjust_data_set(NULL, mds->n, mds->n);

    if (ds == NULL)
    {
        return NULL;
    }

    add_data(pr, IS_DATA_SET, (void *)ds);

    /*
        bt = (Bto *) find_stuff(ac, IS_KNOB1, KILL_B);
        if (!bt) bt->db.action = on_yes_kill_multi_d_s_acreg;
    */

    if ((ds = op->dat[0]) == NULL)
    {
        return OFF;
    }

    for (i = 0 ; i < mds->n ; i++)
    {
        ds->xd[i] = mds->x[mds->x_index][i];
        ds->yd[i] = mds->x[mds->y_index][i];
    }

    ds->nx = ds->ny = mds->n;
    ds->source = my_sprintf(NULL, "file %s x-> col %d y -> col %d",
                            (mds->source != NULL) ? mds->source : "unknown", mds->x_index, mds->y_index);
    set_plot_x_title(op, "%s", mds->name[mds->x_index]);
    set_plot_y_title(op, "%s", mds->name[mds->y_index]);
    op->need_to_refresh = 1;
    return pr;
}


O_p        *create_Op_from_multi_d_s(pltreg *pr, multi_d_s *mds)
{
    register int i;
    O_p *op = NULL;
    d_s *ds = NULL;
    char file[512] = {0}, *f, question[2048] = {0};//, sel[256];
    static int x_i = 0, all = 1 , y_s = 1, y_e = 2;
    if (!mds)
    {
        return NULL;
    }

    if (mds->n_d_s > 32)
      {
	i = win_scanf("Define the data to use for the X axis index %4d\n"
		      "Define the columns that you want to plot in Y:\n"
		      "%R->all columns; %r->all excepted the one choose in X;\n"
		      "%r-> or in the range [%4d,%4d]\n",&x_i,&all,&y_s,&y_e);
      }
    else
      {
	snprintf(question,sizeof(question),"Define the data to use for the X axis:\n");
	for (i = 0; i < mds->n_d_s; i++)
	  {
	    x_i = last_x_index;
            if (i == 0)
	      {
		snprintf(question + strlen(question), sizeof(question) - strlen(question), "[%%R ->%d %s]  ", i,mds->name[i]);
	      }
            else
	      {
                snprintf(question + strlen(question), sizeof(question) - strlen(question),"[%%r ->%d %s]  ", i,mds->name[i]);
	      }
	    
            if (i % 4 == 3)
	      {
                snprintf(question + strlen(question), sizeof(question) - strlen(question), "\n");
	      }
	  }
	snprintf(question + strlen(question), sizeof(question) - strlen(question),
		 "\nDefine the data(s) to plot in Y:\n"
		 "%%R->all columns; %%r->all excepted the one choose in X; %%r-> or select below\n\n");
	for (i = 0; i < mds->n_d_s; i++)
	  {
	    if (i == last_y_index) 	      mds->y_ind[i] = 1;
	    if (i == 0)
	      {
		snprintf(question + strlen(question), sizeof(question) - strlen(question), "[%%Q ->%d %s]  ", i,mds->name[i]);
		
	      }
	    else
	      {
		snprintf(question + strlen(question), sizeof(question) - strlen(question),"[%%q ->%d %s]  ", i,mds->name[i]);
	      }
	    
	    if (i % 4 == 3)
	      {
		snprintf(question + strlen(question), sizeof(question) - strlen(question), "\n");
	      }
	  }
	i = win_scanf(question,&x_i,&all,mds->y_ind);
      }
    if (i == WIN_CANCEL) return NULL;


    
    pr = (pr == NULL) ? find_pr_in_current_dialog(NULL) : pr;

    if (pr == NULL)
    {
        pr = create_and_register_new_plot_project(0,   32,  900,  668);
    }

    if (pr == NULL)
    {
        return NULL;
    }

    if ((op = create_and_attach_one_plot(pr, mds->n, mds->n, 0)) == NULL)
    {
        win_printf("cannot create plot !");
        return NULL;
    }

    f = extract_file_name(file, 512, mds->source);

    if (f != NULL)
    {
        replace_extension(file, file, "gr", sizeof(file));
        set_plot_file(op, "%s", file);
    }

    f = extract_file_path(file, 512, mds->source);

    if (f != NULL)
    {
        strcat(file, "\\");
        set_plot_path(op, "%s", f);
    }

    if ((ds = op->dat[0]) == NULL)
    {
        return OFF;
    }
    last_x_index = mds->x_index = x_i;
    int k;
    for (k = 0 ; k < mds->n_d_s ; k++)
    {
      if (all == 1 && k == x_i) continue;
      if (all == 2)
	{
	  if (mds->n_d_s > 32 && (k < y_s || k > y_e))  continue;
	  if (mds->n_d_s <= 32 && mds->y_ind[k] != 1) continue;
	}
      if (ds != NULL)
	{
	  last_y_index = mds->y_index = k;
	  snprintf(question, sizeof(question), "%s",mds->name[k]);
	}
      else  snprintf(question + strlen(question), sizeof(question) - strlen(question), ", %s",mds->name[k]);

      if (ds == NULL)
	{
	  if ((ds = create_and_attach_one_ds(op, mds->n, mds->n, 0)) == NULL)
	    return win_printf_ptr("Cannot create ds");
	}
      for (i = 0 ; i < mds->n ; i++)
	{
	  ds->xd[i] = mds->x[x_i][i];
	  ds->yd[i] = mds->x[k][i];
	}
      ds->nx = ds->ny = mds->n;
      ds->source = my_sprintf(NULL, "file %s x-> col %d y -> col %d",
			      (mds->source != NULL) ? mds->source : "unknown", mds->x_index, k);
      ds->user_ispare[0] = mds->x_index;
      ds->user_ispare[1] = k;
      
      ds = NULL;
    }
    set_plot_x_title(op, "%s", mds->name[mds->x_index]);
    set_plot_y_title(op, "%s", question);
    op->need_to_refresh = 1;
    return op;
}

int         on_yes_kill_multi_d_s_pltreg(void)
{
//    answer_yes = 0;
    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (win_printf("Do you really want \nto destroy this window ?") == WIN_OK)
    {
        kill_multi_d_s_pltreg();
    }

    return 0;
}

int kill_multi_d_s_pltreg(void)
{
    pltreg *lpr = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    lpr = find_pr_in_current_dialog(NULL);
    free_multi_d_s(find_multi_d_s_in_pltreg(lpr));
    /*    return kill_acreg(ch); */
    return 0;
}


int add_data_to_multi_d_s(multi_d_s *lmds, float *f)
{
    register int i;
    float *t = NULL;

    if (!lmds || !f)
    {
        return 0;
    }

    if (lmds->n >= lmds->m)
    {
        for (i = 0, lmds->m += 1024; i < lmds->n_d_s;  i++)
        {
            if ((t = (float *)realloc(lmds->x[i], lmds->m * sizeof(float))) != NULL)
            {
                lmds->x[i] = t;
            }

            if (t == NULL)
            {
                return 0;
            }
        }
    }

    for (i = 0 ; i < lmds->n_d_s ; i++)
    {
        lmds->x[i][lmds->n] = f[i];
    }

    lmds->n++;
    return 1;
}
int remove_data_from_multi_d_s(multi_d_s *lmds, int index)
{
    register int i, j;

    if (!lmds || index < 0 || index >= lmds->n)
    {
        return 0;
    }

    for (j = index ; j < lmds->n - 1 ; j++)
    {
        for (i = 0 ; i < lmds->n_d_s ; i++) /* it was for ( i=0 ; i<lmds->n_d_s-1 ; i++) */
        {
            lmds->x[i][j] = lmds->x[i][j + 1];
        }
    }

    lmds->n--;
    return 1;
}

char *my_strtok(char *s)
{
    static char *s0 = NULL, *s1 = NULL;
    static char *sepchar0 = " \t,";
    static char *sepchar1 = "\"";
    static char *sepchar2 = " \t\"\n";

    if (s)
    {
        s0 = s;
    }

    if (s0 == NULL || *s0 == '\0')
    {
        s0 = NULL;
        return NULL;
    }

    s0 = s0 + strspn(s0, sepchar2);

    /* si tous les caracteres sont des separateurs, alors s0==NULL */
    if (s0 == NULL || *s0 == '\0')
    {
        s0 = NULL;
        return NULL;
    }

    if (*(s0 - 1) == '\"')
    {
        s1 = s0;
        s0 = s0 + strcspn(s0, sepchar1); /* si pas d'autre cote va a NULL */

        if (*s0 != '\0')
        {
            *s0 = '\0';
            s0++;
        }
        else
        {
            s0 = NULL;
        }

        return s1;
    }

    s1 = s0;
    s0 = s0 + strcspn(s0 , sepchar0);

    /*        if (!*s0)     *s0++ = '\0';*/
    if (*s0 != '\0')
    {
        *s0 = '\0';
        s0++;
    }
    else
    {
        s0 = NULL;
    }

    return s1;
}



int    load_xvplot_file(void)
{
    register int i;
    char path[512] = {0}, file[256] = {0};
    static char fullfile[1024] = {0}, *fi = NULL, ffile[512] = {0};
    multi_d_s *mds = NULL;
    pltreg *pr = NULL;
    int win_open = 0;
    const char *fu = NULL;
    pr = find_pr_in_current_dialog(NULL);
    mds = find_multi_d_s_in_pltreg(pr);

    if (updating_menu_state != 0)
    {
        XVplot_plot_menu = xvplot_plot_menu(mds);
        return D_O_K;
    }

    if (fu == NULL)
    {
        fu = (const char *) get_config_string("XVPLOT-FILE", "last_loaded", NULL);

        if (fu != NULL)
        {
            strcpy(fullfile, fu);
            fu = fullfile;
        }
        else
        {
            my_getcwd(fullfile, 512);
            strcat(fullfile, "\\");
        }
    }

#ifdef XV_WIN32

    if (full_screen)
    {
#endif
        switch_allegro_font(1);
        i = file_select_ex("Load xv-plot", fullfile, "xv;txt;mgr;up;dn;dat", 512, 0, 0);
        switch_allegro_font(0);
#ifdef XV_WIN32
    }
    else
    {
        extract_file_path(path, 512, fullfile);
        put_backslash(path);

        if (extract_file_name(file, 256, fullfile) == NULL)
        {
            fullfile[0] = file[0] = 0;
        }
        else
        {
            strcpy(fullfile, file);
        }

        fu = DoFileOpen("Load XvPlots (*.xv;*.txt,*.mgr;*.up;*.dn;*.dat)", path, fullfile, 4096,
                        "XvPlots (*.xv)\0*.xv;*.txt;*.mgr;*.up;*.dn;*.dat\0All Files (*.*)\0*.*\0\0", "xv");
        i = (fu == NULL) ? 0 : 1;
        win_open = 1;

        if (fullfile[1 + strlen(fullfile)] != 0)
        {
            win_open = 2;
        }
    }

#endif

    if (i != 0 && win_open < 2)
    {
        fu = fullfile;
        set_config_string("XVPLOT-FILE", "last_loaded", fu);
        extract_file_name(file, 256, fullfile);
        extract_file_path(path, 512, fullfile);
        strcat(path, "\\");
	//win_printf("jumping to xvplot_file\n%s\n",backslash_to_slash(fullfile));	
        xvplot_file(fullfile, 0);
    }

    if (i != 0 && win_open > 1)
    {
        strcpy(path, fullfile);
        strcat(path, "\\");
        fi = fullfile;

        for (fi = fi + 1 + strlen(fi); fi[0] != 0; fi = fi + 1 + strlen(fi))
        {
            strcpy(file, fi);
            sprintf(ffile, "%s%s", path, file);
            set_config_string("XVPLOT-FILE", "last_loaded", ffile);
	    //win_printf("jumping to xvplot_file\n%s\n",backslash_to_slash(file));
            xvplot_file(ffile, 0);
        }
    }

    return 0;
}



    

int    load_xvplot_file_as_gr_multi(void)
{
    const char *fu = NULL;


    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    file_list_t *file_list = open_files_config("Load plot (*.gr)", fu,
					       "XvPlot Files (*.xv)\0*.xv\0All Files (*.*)\0*.*\0\0",
					       "XVPLOT-FILE", "last_loaded");
    file_list_t *cursor = file_list;

    while (cursor != NULL)
    {
        set_config_string("XVPLOT-FILE", "last_loaded", cursor->data);
	xvplot_file(cursor->data, 1);
        cursor = cursor->next;
    }

    free_file_list(file_list);
    return D_REDRAW;
}
      

int    load_xvplot_file_as_gr(void)
{
    register int i;
    char path[512] = {0}, file[256] = {0};
    static char fullfile[512] = {0}, *fi = NULL, ffile[512] = {0};
    const char *fu = NULL;
    //pltreg* pr;
    int win_open = 0;

    //pr = find_pr_in_current_dialog(NULL);

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (fu == NULL)
    {
        fu = (const char *)get_config_string("XVPLOT-FILE", "last_loaded", NULL);

        if (fu != NULL)
        {
            strcpy(fullfile, fu);
            fu = fullfile;
        }
        else
        {
            my_getcwd(fullfile, 512);
            strcat(fullfile, "\\");
        }
    }
    /*
    i = win_scanf("Select X index %4d\n"
                  "Select Y index %4d\n", &last_x_index, &last_y_index);

    if (i == WIN_CANCEL)
    {
        return D_O_K;
    }
    */
    
#ifdef XV_WIN32

    if (full_screen)
    {
#endif
        switch_allegro_font(1);
        i = file_select_ex("Load xv-plot", fullfile, "xv;mgr;up;dn;dat", 512, 0, 0);
        switch_allegro_font(0);
#ifdef XV_WIN32
    }
    else
    {
        extract_file_path(path, 512, fullfile);
        put_backslash(path);

        if (extract_file_name(file, 256, fullfile) == NULL)
        {
            fullfile[0] = file[0] = 0;
        }
        else
        {
            strcpy(fullfile, file);
        }

        fu = DoFileOpen("Load XvPlots (*.xv;*.mgr;*.up;*.dn;*.dat)", path, fullfile, 4096,
                        "XvPlots (*.xv)\0*.xv;*.mgr;*.up;*.dn;*.dat\0All Files (*.*)\0*.*\0\0", "xv");
        i = (fu == NULL) ? 0 : 1;
        win_open = 1;

        if (fullfile[1 + strlen(fullfile)] != 0)
        {
            win_open = 2;
        }
    }

#endif

    if (i != 0 && win_open < 2)
    {
        fu = fullfile;
        set_config_string("XVPLOT-FILE", "last_loaded", fu);
        extract_file_name(file, 256, fullfile);
        extract_file_path(path, 512, fullfile);
        strcat(path, "\\");
        xvplot_file(fullfile, 1);
    }

    if (i != 0 && win_open > 1)
    {
        strcpy(path, fullfile);
        strcat(path, "\\");
        fi = fullfile;

        for (fi = fi + 1 + strlen(fi); fi[0] != 0; fi = fi + 1 + strlen(fi))
        {
            strcpy(file, fi);
            sprintf(ffile, "%s%s", path, file);
            set_config_string("XVPLOT-FILE", "last_loaded", ffile);
            xvplot_file(ffile, 1);
            //	    reorder_loaded_plot_file_list(file, path);
            //reorder_visited_path_list(path);
        }
    }

    return 0;
}


char    *do_create_xvplot_file(char *xvfile)
{
    register int i;
    const char *fu = NULL;
    char path[512] = {0}, file[256] = {0}, fullfile[512] = {0};
    //int  win_file = 0;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (xvfile != NULL)
    {
        //win_printf("xvfile %s",backslash_to_slash(xvfile));
        fix_filename_slashes(xvfile);
    }

    if ((xvfile == NULL) || (extract_file_path(path, 512, xvfile) == NULL))
    {
        fu = (const char *)get_config_string("XVPLOT-FILE", "last_created", NULL);

        if (fu != NULL)
        {
            strcpy(fullfile, fu);
            fu = fullfile;
        }
        else
        {
            my_getcwd(fullfile, 512);
            strcat(fullfile, "\\");

            if (extract_file_name(file, 256, fullfile) == NULL)
            {
                strcat(fullfile, "Untitled.xv");
            }
            else
            {
                strcat(fullfile, file);
            }
        }
    }
    else
    {
        strcpy(fullfile, xvfile);
    }

#ifdef XV_WIN32

    if (full_screen)
    {
#endif
        switch_allegro_font(1);
        i = file_select_ex("Create xvplot (*.xv) file", fullfile, "xv;mgr;up;dn;dat", 512, 0, 0);
        switch_allegro_font(0);
#ifdef XV_WIN32
    }
    else
    {
        extract_file_path(path, 512, fullfile);

        if (extract_file_name(file, 256, fullfile) != NULL)
        {
            strcpy(fullfile, file);
        }
        else
        {
            fullfile[0] = file[0] = 0;
        }

        fu = DoFileSave("Create xvplot (*.xv) file", path, fullfile, 512,
                        "speadsheet data Files (*.xv)\0*.xv\0*.mgr\0*.txt\0All Files (*.*)\0*.*\0\0", "xv");
        i = (fu == NULL) ? 0 : 1;
        //win_file = 1;
    }

#endif

    if (i != 0)
    {
        set_config_string("XVPLOT-FILE", "last_created", fullfile);
        fp = fopen(fullfile, "w");

        if (fp != NULL)
        {
            fclose(fp);

            if (current_write_open_xvfile != NULL)
            {
                free(current_write_open_xvfile);
            }

            current_write_open_xvfile = strdup(fullfile);
        }
        else
        {
            return NULL;
        }

        return current_write_open_xvfile;
    }

    return NULL;
}
int    dump_to_current_xvplot_file(const char *fmt, ...)
{
    va_list ap;
    FILE *fpl = NULL;
    char c[2048] = {0};

    if (fmt == NULL)
    {
        return 1;
    }

    if (current_write_open_xvfile == NULL)
        if ((do_create_xvplot_file(NULL)) == NULL)
        {
            return 1;
        }

    fpl = fopen(current_write_open_xvfile, "a");

    if (fpl == NULL)
    {
        return 1;
    }

    va_start(ap, fmt);
    vsprintf(c, fmt, ap);
    va_end(ap);
    fprintf(fpl, "%s\n", c);
    return fclose(fpl);
}
int save_xvplot_file(void)
{
    register int i, j;
    multi_d_s *mds = NULL;
    pltreg *pr = NULL;
    FILE *fpl = NULL;
    char *prev_current_write_open_xvfile = NULL, *f, file[512] = {0};
    
    pr = find_pr_in_current_dialog(NULL);
    mds = find_multi_d_s_in_pltreg(pr);

    if (updating_menu_state != 0)
    {
        XVplot_plot_menu = xvplot_plot_menu(mds);
        return D_O_K;
    }

    if (mds == NULL)
    {
        return win_printf_OK("could not find XVplot data");
    }

    prev_current_write_open_xvfile = current_write_open_xvfile;
    fix_filename_slashes(mds->source);
    f = replace_extension(file, mds->source, "xv", 512);

    if (do_create_xvplot_file(f) == NULL)
    {
        return win_printf_OK("XVplot file not created");
    }

    fpl = fopen(current_write_open_xvfile, "w");

    if (fpl == NULL)
    {
        return win_printf_OK("cannot write in XVplot file");
    }

    fprintf(fpl, "%%\"%s\"\t", mds->name[0]);

    for (i = 1; i < mds->n_d_s; i++)
    {
        fprintf(fpl, "\"%s\"\t", mds->name[i]);
    }

    for (i = j = 0; j < mds->n; j++)
    {
        i = 0;
        fprintf(fpl, "\n%g\t", mds->x[i][j]);

        for (i = 1; i < mds->n_d_s; i++)
        {
            fprintf(fpl, "%g\t", mds->x[i][j]);
        }
    }

    fprintf(fpl, "\n");
    fclose(fpl);
    current_write_open_xvfile = prev_current_write_open_xvfile;
    return D_O_K;
}

int retrieve_mds_index_for(multi_d_s *mds, char *name)
{
    register int i;

    if (mds == NULL || name == NULL)
    {
        return -1;
    }

    for (i = 0; i < mds->n_d_s; i++)
        if ((strcmp(mds->name[i], name) == 0) && (strlen(mds->name[i]) == strlen(name)))
        {
            return i;
        }

    return -1;
}

int retrieve_and_check_mds_index_for(multi_d_s *mds, char *name)
{
    register int i;

    if (mds == NULL || name == NULL)
    {
        return -1;
    }

    for (i = 0; i < mds->n_d_s; i++)
        if ((strcmp(mds->name[i], name) == 0) && (strlen(mds->name[i]) == strlen(name)))
        {
            return i;
        }

    win_printf("the string :\"%s\" was not found!\nin multi column plot :\n%s",
               name,backslash_to_slash(mds->source));
    return -1;
}

int        xvplot_main(int argc, char **argv)
{
    int first = 1;
    (void)argc;
    (void)argv;

    if (first)
    {
        add_item_to_menu(plot_file_import_menu, "load XV speadsheet in a new project", load_xvplot_file, NULL, 0, NULL);
        add_item_to_menu(plot_file_import_menu, "load XV speadsheet(s) as simple plot(s)", load_xvplot_file_as_gr_multi, NULL, 0, NULL);
        add_item_to_menu(plot_file_export_menu, "Save Xv Plots", save_xvplot_file, NULL, 0, NULL);
        XVplot_plot_menu = xvplot_plot_menu(NULL);
        add_item_to_menu(plot_treat_menu, "Xv Plots", NULL, XVplot_plot_menu, 0, NULL);
        first = 0;
    }

    if (argc < 2)
    {
        return D_O_K;    //    return    load_xvplot_file();
    }
    else
    {
        return xvplot_file(backslash_to_slash(argv[1]), 0);
    }
}
int    xvplot_unload(int argc, char **argv)
{
    (void)argc;
    (void)argv;
    remove_item_to_menu(plot_file_import_menu, "load XV speadsheet in a new project", NULL, NULL);
    remove_item_to_menu(plot_file_import_menu, "load XV speadsheet in a simple plot", NULL, NULL);
    remove_item_to_menu(plot_file_export_menu, "Save Xv Plots", NULL, NULL);
    remove_item_to_menu(plot_file_import_menu, "Xv Plots", NULL, NULL);
    return D_O_K;
}



#endif





