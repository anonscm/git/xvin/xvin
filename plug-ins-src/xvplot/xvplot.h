#ifndef _XVPLOT_H_
#define _XVPLOT_H_

#ifndef _XVPLOT_C_
PXV_VAR(char*, current_write_open_xvfile);
# endif

#define IS_MULTI_D_S		5000
#define SEPCHARS			" ;\t\n"

#define MAX_LINE_LENGTH		32768
#define MAX_DATA_NUM		2048

#define ERR_NULL_POINTER	2048
#define ERR_ABSURD_VALUE	2049
#define ERR_IMAGE_TYPE		2050
#define ERR_DIVIDE_BY_ZERO	2051
#define ERR_PARAMETERS		2052

/* en fait, ce serait mieux de l'accrocher a une plotregion. */
/* mais celles-ci n'ont pas de hook !*/

typedef struct _multi_d_s
{
  int		n_d_s;		/* number of data sets	*/
  int		n;			/* number of points in each data set	*/
  int		m;			/* size allocated for each data set */
  int		x_index;	/* current x_index */
  int		y_index;	/* current y_index */
  char *y_ind; /* list of y_index */
  int		y_index_e;	/* last y_index */
  char	*source;	/* name of the file	*/
  char	**name;		/* names of data sets */
  float	**x;		/* data	*/
  int *sel;             /* a flag for multiselection */
} multi_d_s;

PXV_FUNC(multi_d_s*,	find_multi_d_s_in_pltreg, (pltreg* pr));
PXV_FUNC(multi_d_s*,	create_multi_d_s, (int n_d_s));
PXV_FUNC(void,		free_multi_d_s, (multi_d_s *mds));
PXV_FUNC(pltreg,	*create_multi_d_s_pltreg, (multi_d_s *mds));
PXV_FUNC(int,		on_yes_kill_multi_d_s_pltreg, (void));
PXV_FUNC(int,		kill_multi_d_s_pltreg, (void));
PXV_FUNC(char, 		*my_strtok, (char *s));
PXV_FUNC(int, 		add_data_to_multi_d_s, (multi_d_s *mds, float *f));
PXV_FUNC(int, 		remove_data_from_multi_d_s, (multi_d_s *mds, int index));
PXV_FUNC(MENU*,		xvplot_plot_menu, (multi_d_s *mds));
PXV_FUNC(int,		do_xvplot, (void));
PXV_FUNC(int,		set_x_data, (void));
PXV_FUNC(int,		set_y_data, (void));
PXV_FUNC(int,		xvplot_file, (char *file, int mode));
PXV_FUNC(int,		load_xvplot_file, (void));
PXV_FUNC(char*,		do_create_xvplot_file, (char *xvfile));
PXV_FUNC(int,		dump_to_current_xvplot_file, (const char *fmt, ...) __attribute__ ((format (printf, 1, 2))));
PXV_FUNC(int, 		xvplot_main, (int argc, char **argv));
PXV_FUNC(int,           retrieve_mds_index_for, (multi_d_s *mds, char *name));
PXV_FUNC(int,           retrieve_and_check_mds_index_for, (multi_d_s *mds, char *name));
PXV_FUNC(O_p*,          create_Op_from_multi_d_s, (pltreg *pr, multi_d_s *mds));
#endif
