#ifndef _MOVIE_AVG_H_
#define _MOVIE_AVG_H_

PXV_FUNC(int, do_movie_avg_average_images, (void));
PXV_FUNC(MENU*, movie_avg_image_menu, (void));
PXV_FUNC(int, movie_avg_main, (int argc, char **argv));
#endif

