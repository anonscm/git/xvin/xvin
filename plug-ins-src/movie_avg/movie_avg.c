/*
*    Plug-in program for image treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _MOVIE_AVG_C_
#define _MOVIE_AVG_C_


/* If you include other plug-ins header do it here*/ 
# include "allegro.h"
# include "xvin.h"
//# include "gsl_randist.h"
//# include "gsl_rng.h"
//# include "winalleg.h"
# include "fftl32n.h"
# include "fillib.h"
//# include "largeint.h"
//# include "windows.h"
//# include "nrutil.h"
//#include <stdio.h>
/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "movie_avg.h"

 
float *radial_non_pixel_square_image_sym_profile_in_array(float *rz, O_i *ois, float xc, float yc, int r_max, float ax, float ay)
{
	int i, j;
	union pix *pd;
	int nx, ny, ri, type;
	float z = 0, z1, x, y, r, p, p0 = 0, pr0 = 0, radial_dx[256];
	int xci, yci, imin, imax, jmin,jmax;

	for (i = 0; i < 2*r_max && i < 256; i++)	radial_dx[i] = 0;
	if (ois == NULL)	return NULL;
	if (rz == NULL)		rz = (float*)calloc(2*r_max,sizeof(float));
	if (rz == NULL)		return NULL;
	pd = ois->im.pixel;
	ny = ois->im.ny;	nx = ois->im.nx;
	type = ois->im.data_type;
	for (i = 0; i < 2*r_max; i++)	rz[i] = 0;
	xci = (int)xc;
	yci = (int)yc;
	imin = (int)(yc - (r_max/ay));
	imax = (int)(0.5 + yc + (r_max/ay));	
	jmin = (int)(xc -  r_max/ax);
	jmax = (int)(0.5 + xc + r_max/ax);
	imin = (imin < 0) ? 0 : imin;
	imax = (imax > ny) ? ny : imax;
	jmin = (jmin < 0) ? 0 : jmin;
	jmax = (jmax > nx) ? nx : jmax;	
    for (i = imin; i < imax; i++)
	    {
		y = ay * ((float)i - yc);
		y *= y;
		for (j = jmin; j < jmax; j++)
		{
			x = ax * ((float)j - xc);
			x *= x;
			r = (float)sqrt(x + y);
			ri = (int)r;
			if (ri > r_max)	continue;	/* bug break;*/
			z = pd[i].fl[j];
			p = r - ri;
			if (ri == r_max)
			{
				p0 += (1-p)*z;
				pr0 += (1-p);
				radial_dx[r_max] -= 1-p;
			}
			else 
			{
				rz[ri] += (1-p)*z;
				rz[ri+r_max] += (1-p);
				radial_dx[ri] += p;
			}
			ri++;
			if (ri < r_max)
			{
				rz[ri] += p*z;
				rz[ri+r_max] += p;
				radial_dx[ri] -= 1-p;
			}
		}
	    }
	 

	for (i = 0; i < r_max; i++)
	{
		rz[i] = (rz[i+r_max] == 0) ? 0 : rz[i]/rz[i+r_max];
		radial_dx[i] = (rz[i+r_max] == 0) ? 0 : radial_dx[i]/rz[i+r_max];
	}
	p0 = (pr0 == 0) ? 0 : p0/pr0;  
	for (i = 0; i < r_max; i++)		rz[i+r_max] = rz[i]; 
	for (i = 1; i < r_max; i++)		rz[r_max-i] = rz[r_max+i]; 
	rz[0] = p0;
	return rz;
}  

/*int add_gaussien_noise(void)
{
  O_i *ois, *oid;
  imreg *imr;
  int i, j, nx, ny;
  static float sigma0 = 0.1;
  float *z = NULL;
  union pix *pds, *pdd;
  double sigma;
  long int idum;
  static int idum0=2;
  gsl_rng *r_double;
  
 if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;	
    }		  
  if(updating_menu_state != 0)	return D_O_K;
  nx = ois->im.nx;
  ny = ois->im.ny;

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine add gaussien noise to each pixel");
    }
  if (i == WIN_CANCEL)	return D_O_K;

  oid = create_and_attach_oi_to_imr(imr, nx, ny, IS_FLOAT_IMAGE);
  if (oid == NULL)      
    return win_printf_OK("Can't create dest movie");
    
  pds = ois->im.pixel;  
  pdd = oid->im.pixel;
  
  i = win_scanf("which std deviation in noise %8f\n"
                "choose your start number for random %d\n"
                 ,&sigma0, &idum0);
  idum = (long int)idum0;
  r_double = gsl_rng_alloc(gsl_rng_cmrg);
  gsl_rng_set(r_double,idum);
  
  sigma= (double)sigma0;
  for (i=0; i<ny; i++)
     {
            for (j=0; j<nx; j++) pdd[i].fl[j] = pds[i].fl[j] + (float)gsl_ran_gaussian (r_double, sigma);
     }  
    
  
 inherit_from_im_to_im(oid,ois);
 uns_oi_2_oi(oid,ois);
 oid->im.win_flag = ois->im.win_flag;
 if (ois->title != NULL)	set_im_title(oid, "gaussien noise %s ", ois->title);
 set_formated_string(&oid->im.treatement,"gaussien noise %s ", ois->filename);
 find_zmin_zmax(oid);
 free(z);
 return (refresh_image(imr, imr->n_oi - 1)); 
  
}
*/

int do_movie_avg_mean_and_std(void)
{
  O_i *ois, *oid;
  imreg *imr;
  int nx, ny, nf, i, im, j;
  static int navg = 16;
  float *z = NULL, avg, std;
  union pix *pd; 

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;	
    }		
  nf = abs(ois->im.n_f);		
  if (nf <= 0)	
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;
    }
  else   active_menu->flags &= ~D_DISABLED;

  if(updating_menu_state != 0)	return D_O_K;
  i = ois->im.c_f;
  if (ois->im.data_type == IS_COMPLEX_IMAGE 
      || ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)	
    {
      win_printf("I do not handle complexe images ! yet");
      return D_O_K;
    }
  nx = ois->im.nx;
  ny = ois->im.ny;

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine take the variation of each pixel and compute either its mean or variance");
    }

  i = win_scanf("do you want the variance %b\n(otherwise you get the mean)",&navg );
  if (i == WIN_CANCEL)	return D_O_K;

  oid = create_and_attach_oi_to_imr(imr, nx, ny, IS_FLOAT_IMAGE);
  if (oid == NULL)      
    return win_printf_OK("Can't create dest movie");


  for (i = 0, pd = oid->im.pixel; i < ny ; i++)
    {
      for (j=0; j< nx; j++)
	{
	  z = extract_z_profile_from_movie(ois, j, i, z);
	  for (im = 0, avg = 0; im < nf; im++)
	      avg += z[im]; 
	  avg /= nf;
	  if (navg != 1) 	    pd[i].fl[j] = avg;
	  else 
	    {
	      for (im = 0, std = 0; im < nf; im++)
		std += (z[im] - avg)* (z[im] - avg);
	      std /= (nf > 1) ? (nf-1) : 1;
	      pd[i].fl[j] = std;
	    }
	}
    }
  inherit_from_im_to_im(oid,ois);
  uns_oi_2_oi(oid,ois);
  oid->im.win_flag = ois->im.win_flag;
  if (ois->title != NULL)	set_im_title(oid, "%s ", ois->title);
  if (navg != 1) set_formated_string(&oid->im.treatement,"mean movie of %s ", ois->filename);
  else set_formated_string(&oid->im.treatement,"variance movie of %s ", ois->filename);
  find_zmin_zmax(oid);
  free(z);
  
  return (refresh_image(imr, imr->n_oi - 1));
}




int do_movie_avg_images(void)
{
  O_i *ois, *oid;
  imreg *imr;
  int nx, ny, nf, i, nf2, im, im2, k, j;
  static int navg = 16;
  float *z;
  union pix *pd; 

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;	
    }		
  nf = abs(ois->im.n_f);		
  if (nf <= 0)	
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;
    }
  else   active_menu->flags &= ~D_DISABLED;

  if(updating_menu_state != 0)	return D_O_K;
  i = ois->im.c_f;
  if (ois->im.data_type == IS_COMPLEX_IMAGE 
      || ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)	
    {
      win_printf("I do not handle complexe images ! yet");
      return D_O_K;
    }
  nx = ois->im.nx;
  ny = ois->im.ny;

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine average a movie over a subset of images");
    }

  i = win_scanf("define the number of frames for averaging %d",&navg);
  if (i == WIN_CANCEL)	return D_O_K;

  nf2 = nf/navg;
  nf2 += (nf%navg) ? 1 : 0;

  if (nf2 == 1) oid = create_and_attach_oi_to_imr(imr, nx, ny, IS_FLOAT_IMAGE);
  else    oid = create_and_attach_movie_to_imr(imr, nx, ny, IS_FLOAT_IMAGE, nf2);

  z = (float *)calloc(nx,sizeof(float));
  if (oid == NULL || z == NULL)      
    return win_printf_OK("Can't create dest movie");

  for (im = 0; im < nf; im++)
    {
      switch_frame(ois,im);
      im2 = im/navg;
      switch_frame(oid,im2);
      for (i = 0, pd = oid->im.pixel; i < ny ; i++)
	{
	  extract_raw_line (ois, i, z);
	  for (j=0; j< nx; j++)
	    pd[i].fl[j] += z[j];
	}
      if (((im+1)%navg == 0) || (im+1) == nf)
	{
	  k = (im%navg)+1;
	  for (i = 0, pd = oid->im.pixel; i < ny ; i++)
	    {
	      for (j=0; j< nx; j++)
		pd[i].fl[j] /= k;
	    }
	}

    }

  inherit_from_im_to_im(oid,ois);
  uns_oi_2_oi(oid,ois);
  oid->im.win_flag = ois->im.win_flag;
  if (ois->title != NULL)	set_im_title(oid, "%s ", ois->title);
  set_formated_string(&oid->im.treatement,"Avg movie of %s ", ois->filename);
  find_zmin_zmax(oid);
  free(z);
  return (refresh_image(imr, imr->n_oi - 1));
}

        
 int do_movie_avg_truefit(void)
 
{
  O_i *ois = NULL, *oid = NULL;
  imreg *imr;
  int nx=3, ny=3, i, j;
  float *z = NULL , a1=0, b1=0, c1=0, d1=0, e1=0, f1=0;
  union pix *pd, *pd1; 
  static float a=1, b=-1, c=-1, d=1, e=-1, f=1;
  

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;	
    } 
      if(updating_menu_state != 0)	return D_O_K;
      
    
    oid = create_and_attach_oi_to_imr(imr, nx, ny, IS_FLOAT_IMAGE);	
    
    i = win_scanf("This routine create a specific image by choosing coefficient a%8f\n b%8f\n c%8f\n d%8f\n e%8f\n f%8f\n ",&a,&b,&c,&d,&e,&f);
    	
    for (i=-1, pd = ois->im.pixel; i < 2 ; i++)
       {
           for (j=-1; j< 2; j++) pd[i+1].fl[j+1]= a + b*j + c*j*j + d*i + e*i*i + f*i*j;
       }
    for (i=-1; i < 2; i++)
                       {
                           for (j = -1; j < 2; j++)
                              {
                               a1 += (float)5/9 * pd[i+1].fl[j+1]  - (float)1/3 * ( j*j * pd[i+1].fl[j+1] + i*i * pd[i+1].fl[j+1] );
                               b1 +=(float) 1/6 * j * pd[i+1].fl[j+1];
                               c1 += (float)-1/3 * pd[i+1].fl[j+1] + (float)1/2 * j*j * pd[i+1].fl[j+1];
                               d1 += (float)1/6 * i * pd[i+1].fl[j+1];
                               e1 += (float)-1/3 * pd[i+1].fl[j+1] + (float)1/2 * i*i * pd[i+1].fl[j+1];
                               f1 += (float)1/4 * i*j * pd[i+1].fl[j+1];  
                              }
                       }  
                       
     win_printf("valeur de a1  %8f\n b1  %8f\n c1  %8f\n d1  %8f\n e1  %8f\n f1  %8f\n", a1, b1, c1, d1, e1, f1);  
           
     for (i=-1, pd1 = oid->im.pixel; i < 2; i++)
       {
           for (j=-1; j< 2; j++)
   	          {
                pd1[i+1].fl[j+1]= a1 + b1*j + c1*j*j + d1*i + e1*i*i + f1*i*j - (a + b*j + c*j*j + d*i + e*i*i + f*i*j);
                pd1[i+1].fl[j+1] = fabs(pd1[i+1].fl[j+1]);  
              }
       }     
 find_zmin_zmax(oid);
 return (refresh_image(imr, imr->n_oi - 1));     
 }  
 
                  
int do_count_particles(void)
{
  O_i *ois;
  imreg *imr;
  int nx, ny, i, j;
  float  diffd, diffu, diffr, diffl, diffru, diffrd, difflu, diffld;
  static float  grd=100;
  static int  nps=0;
  union pix *pd; 
  
  O_p *opn = NULL;
  d_s *ds;
  
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;	
    }		

  if(updating_menu_state != 0)	return D_O_K;
  if (ois->im.data_type == IS_COMPLEX_IMAGE 
      || ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)	
    {
      win_printf("I do not handle complexe images ! yet");
      return D_O_K;
    }
  nx = ois->im.nx;
  ny = ois->im.ny;

  if (key[KEY_LSHIFT]) return win_printf_OK("This routine add and point maxima on image, fits maxima on each NP image and gives fit properties");
  
  i = win_scanf("What threshold value do you want %8f\n", &grd);
  if (i == WIN_CANCEL)	return D_O_K;
  
   if ((opn = create_and_attach_op_to_oi (ois, 16, 16, 0,IM_SAME_X_AXIS | IM_SAME_Y_AXIS)) == NULL)
		return win_printf_OK("cannot create plot !");

  ds = opn->dat[0]; 
  
  set_ds_point_symbol(ds, "\\oc");
  set_ds_dot_line(ds);

  ds->nx = ds->ny = 0; 	
   
  if(updating_menu_state != 0)	return D_O_K;
  
  for (i=1, pd = ois->im.pixel; i < ny-1; i++)
    {
      for (j=1; j< nx-1; j++)
       	{
          if  (pd[i].fl[j] > grd) 
            { 
              diffr= pd[i].fl[j] - pd[i].fl[j+1]; 
              diffl= pd[i].fl[j] - pd[i].fl[j-1];
              diffu= pd[i].fl[j] - pd[i+1].fl[j];
              diffd= pd[i].fl[j] - pd[i-1].fl[j];
              diffru= pd[i].fl[j] - pd[i+1].fl[j+1]; 
              diffrd= pd[i].fl[j] - pd[i-1].fl[j+1];
              difflu= pd[i].fl[j] - pd[i+1].fl[j-1];
              diffld= pd[i].fl[j] - pd[i-1].fl[j-1];
              
              if (diffr*diffl > 0 && diffu*diffd > 0 && diffru*diffld > 0  && difflu*diffrd > 0 )
                {   
                    nps+= 1;
                    add_new_point_to_ds(ds, (float)j, (float) i);
                } 
             } 
         }
    }     
win_printf("NPs number %d\n", nps);

return (refresh_image(imr, imr->n_oi - 1));                    
}



                       
int do_movie_avg_fit(void)

{
  O_i *ois;
  imreg *imr;
  int nx, ny, i, j, l , k, m, n, u, v;
  float  diffd, diffu, diffr, diffl, diffru, diffrd, difflu, diffld, q, det, nymax, nxmax, delta1, delta2, a, b, c, d, e, f;
  static float  grd=100;
  static int  nps=0;
  float x=0, y=0;
  union pix *pd; 
  
  O_p *opn = NULL;
  d_s *ds;
  
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;	
    }		

  if(updating_menu_state != 0)	return D_O_K;
  if (ois->im.data_type == IS_COMPLEX_IMAGE 
      || ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)	
    {
      win_printf("I do not handle complexe images ! yet");
      return D_O_K;
    }
  nx = ois->im.nx;
  ny = ois->im.ny;

  if (key[KEY_LSHIFT]) return win_printf_OK("This routine add and point maxima on image, fits maxima on each NP image and gives fit properties");
  
  i = win_scanf("What threshold value do you want %8f\n", &grd);
  if (i == WIN_CANCEL)	return D_O_K;
  
   if ((opn = create_and_attach_op_to_oi (ois, 16, 16, 0,IM_SAME_X_AXIS | IM_SAME_Y_AXIS)) == NULL)
		return win_printf_OK("cannot create plot !");

  ds = opn->dat[0]; 
  
  set_ds_point_symbol(ds, "\\oc");
  set_ds_dot_line(ds);

  ds->nx = ds->ny = 0; 	
   
  for (i=1, pd = ois->im.pixel, delta1 = 0, delta2 = 0, nps=0; i < ny-1; i++)
    {
      for (j=1; j< nx-1; j++)
       	{
          if  (pd[i].fl[j] > grd) 
            { 
              diffr= pd[i].fl[j] - pd[i].fl[j+1]; 
              diffl= pd[i].fl[j] - pd[i].fl[j-1];
              diffu= pd[i].fl[j] - pd[i+1].fl[j];
              diffd= pd[i].fl[j] - pd[i-1].fl[j];
              diffru= pd[i].fl[j] - pd[i+1].fl[j+1]; 
              diffrd= pd[i].fl[j] - pd[i-1].fl[j+1];
              difflu= pd[i].fl[j] - pd[i+1].fl[j-1];
              diffld= pd[i].fl[j] - pd[i-1].fl[j-1];
              
              if (diffr*diffl > 0 && diffu*diffd > 0 && diffru*diffld > 0  && difflu*diffrd > 0 )
                {   
                    nps+= 1;
                    for (l = i-1, a=b=c=d=e=f=0; l < i+2; l++)
                       {
                           for (k = j-1; k < j+2; k++)
                              {
                               x=(float)(k-j);
                               y=(float)(l-i);
                               a += (float)1/9 * pd[l].fl[k] + (float)4/9 * pd[l].fl[k] - (float)1/3 * ( x*x * pd[l].fl[k] + y*y * pd[l].fl[k] );
                               b +=(float) 1/6 * x * pd[l].fl[k];
                               c += (float)-1/3 * pd[l].fl[k] + (float)1/2 * x*x * pd[l].fl[k];
                               d += (float)1/6 * y * pd[l].fl[k];
                               e += (float)-1/3 * pd[l].fl[k] + (float)1/2 * y*y * pd[l].fl[k];
                               f += (float)1/4 * x*y * pd[l].fl[k];  
                              }
                       }    
                    det= e*c*4 - f*f;
                    
                    if ( c > 0 || e > 0) return win_printf_OK("i shut john wayne");
                    if ( det == 0 ) return win_printf_OK("problem with max");
                           
                    nxmax = j - (-f*d+(float)2*e*b)/det;
                    nymax = i - (-c*d*2+f*b)/det;
                    delta1= fabs ((a-pd[i].fl[j])/pd[i].fl[j]);
                    
                    for ( m = i-1; m < i+2; m++)
                       { 
                          for ( n = j-1; n < j+2; n++)  
                             { 
                               u=n-j;
                               v=m-i;  
                               q = a + b*u + c*u*u + d*v + e*v*v + f*u*v; 
                               delta2 += fabs( (pd[m].fl[n] - q)/pd[m].fl[n]);
                               delta2 /= 9;
                             }
                       }  
                    win_printf("NPs number %d\n"
                               "valeur de i %d\n"
                               "valeur de j %d\n"
                               "valeur xmaxcorrig� %8f\n"
                               "valeur ymaxcorrig� %8f\n"
                               "relativ error on max %8f\n"
                               "relativ global error %8f\n" 
                               , nps, i, j, nxmax, nymax, delta1, delta2);     
                     
                     add_new_point_to_ds(ds, (float)nxmax, (float) nymax);  
                      
                } 
            }   
        } 
    }  

return (refresh_image(imr, imr->n_oi - 1));
}   

int do_movie_avg_datamax(void)
{
  O_i *ois;
  imreg *imr;
  int nx, ny, i, j, l , k;
  float  diffd, diffu, diffr, diffl, diffru, diffrd, difflu, diffld, fmax, det , nymax, nxmax;
  static float  grd=20; 
  static int  q=0; 
  float x=0, y=0, a=0, b=0, c=0, d=0, e=0, f=0;
  union pix *pd; 
  
 // pltreg *pr = NULL;
  O_p *opn = NULL;
  d_s *ds;
  
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;	
    }		

  if(updating_menu_state != 0)	return D_O_K;
  if (ois->im.data_type == IS_COMPLEX_IMAGE 
      || ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)	
    {
      win_printf("I do not handle complexe images ! yet");
      return D_O_K;
    }
  nx = ois->im.nx;
  ny = ois->im.ny;

  if (key[KEY_LSHIFT]) return win_printf_OK("This routine create a dataset for maximum");
  
    
  if ((opn = create_and_attach_op_to_oi (ois, 16, 16, 0,IM_SAME_X_AXIS | IM_SAME_Y_AXIS)) == NULL)
		return win_printf_OK("cannot create plot !"); 
  ds = opn->dat[0];
  
  i = win_scanf("What threshold value do you want %8f\n", &grd);
  if (i == WIN_CANCEL)	return D_O_K;
  
  ds->nx = ds->ny = 0; 	  
  for (i=1, pd = ois->im.pixel; i < ny-1; i++)
    {
      for (j=1; j< nx-1; j++)
       	{
          if  (pd[i].fl[j] > grd) 
            { 
              diffr= pd[i].fl[j] - pd[i].fl[j+1]; 
              diffl= pd[i].fl[j] - pd[i].fl[j-1];
              diffu= pd[i].fl[j] - pd[i+1].fl[j];
              diffd= pd[i].fl[j] - pd[i-1].fl[j];
              diffru= pd[i].fl[j] - pd[i+1].fl[j+1]; 
              diffrd= pd[i].fl[j] - pd[i-1].fl[j+1];
              difflu= pd[i].fl[j] - pd[i+1].fl[j-1];
              diffld= pd[i].fl[j] - pd[i-1].fl[j-1];
              
              if (diffr*diffl > 0 && diffu*diffd > 0 && diffru*diffld > 0  && difflu*diffrd > 0 )
                {     
                    q+=1;
                    for (l = i-1, a=b=c=d=e=f=0; l < i+2; l++)
                       {
                           for (k = j-1; k < j+2; k++)
                              {
                               x=(float)(k-j);
                               y=(float)(l-i);
                               a += (float)1/9 * pd[l].fl[k] + (float)4/9 * pd[l].fl[k] - (float)1/3 * ( x*x * pd[l].fl[k] + y*y * pd[l].fl[k] );
                               b +=(float) 1/6 * x * pd[l].fl[k];
                               c += (float)-1/3 * pd[l].fl[k] + (float)1/2 * x*x * pd[l].fl[k];
                               d += (float)1/6 * y * pd[l].fl[k];
                               e += (float)-1/3 * pd[l].fl[k] + (float)1/2 * y*y * pd[l].fl[k];
                               f += (float)1/4 * x*y * pd[l].fl[k];  
                              }
                       }  
                    det= e*c*(float)4 - f*f;
                    
                    if ( c > 0 || e > 0) return win_printf_OK("i shut john wayne");
                    if ( det == 0 ) return win_printf_OK("problem with max");
                          
                    nxmax = (-f*d+(float)2*e*b)/det;
                    nymax = (-c*d*(float)2+f*b)/det;
                    fmax = a + b*nxmax+ c*nxmax*nxmax + d*nymax + e*nymax*nymax + f*nxmax*nymax;
                    add_new_point_to_ds(ds, (float)q, (float) fmax); 
                    
                    //win_printf("lalala %d", q); 
                } 
            }   
        } 
    }  

return (refresh_image(imr, imr->n_oi - 1));
} 


                                          
float do_movie_fil_tabofmax(float **tab)
{
  O_i *ois;
  imreg *imr;
  int nx, ny, i, j, l , k;
  float  diffd, diffu, diffr, diffl, diffru, diffrd, difflu, diffld, det, nymax, nxmax, a, b, c, d, e, f;
  static float  grd=20;
  static int  nps_c=0, nps=1;
  float x=0, y=0;
  union pix *pd; 
  
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;	
    }		

  if(updating_menu_state != 0)	return D_O_K;
  if (ois->im.data_type == IS_COMPLEX_IMAGE 
      || ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)	
    {
      win_printf("I do not handle complexe images ! yet");
      return D_O_K;
    }
  nx = ois->im.nx;
  ny = ois->im.ny;

  if (key[KEY_LSHIFT]) return win_printf_OK("This routine add and point maxima ");
  
  
  
  i = win_scanf("What threshold value do you want %8f\n"
                "particles' number %d\n", &grd, &nps);
  if (i == WIN_CANCEL)	return D_O_K;
  
  tab = calloc (2,sizeof(float *));
  tab[0] = calloc (nps, sizeof(float));
  tab[1] = calloc (nps, sizeof(float));
   
  for (i=1, pd = ois->im.pixel; i < ny-1; i++)
    {
      for (j=1; j< nx-1; j++)
       	{
          if  (pd[i].fl[j] > grd) 
            { 
              diffr= pd[i].fl[j] - pd[i].fl[j+1]; 
              diffl= pd[i].fl[j] - pd[i].fl[j-1];
              diffu= pd[i].fl[j] - pd[i+1].fl[j];
              diffd= pd[i].fl[j] - pd[i-1].fl[j];
              diffru= pd[i].fl[j] - pd[i+1].fl[j+1]; 
              diffrd= pd[i].fl[j] - pd[i-1].fl[j+1];
              difflu= pd[i].fl[j] - pd[i+1].fl[j-1];
              diffld= pd[i].fl[j] - pd[i-1].fl[j-1];
              
              if (diffr*diffl > 0 && diffu*diffd > 0 && diffru*diffld > 0  && difflu*diffrd > 0 )
                {   
                    nps_c += 1;
                    for (l = i-1, a=b=c=d=e=f=0; l < i+2; l++)
                       {
                           for (k = j-1; k < j+2; k++)
                              {
                               x=(float)(k-j);
                               y=(float)(l-i);
                               a += (float)1/9 * pd[l].fl[k] + (float)4/9 * pd[l].fl[k] - (float)1/3 * ( x*x * pd[l].fl[k] + y*y * pd[l].fl[k] );
                               b +=(float) 1/6 * x * pd[l].fl[k];
                               c += (float)-1/3 * pd[l].fl[k] + (float)1/2 * x*x * pd[l].fl[k];
                               d += (float)1/6 * y * pd[l].fl[k];
                               e += (float)-1/3 * pd[l].fl[k] + (float)1/2 * y*y * pd[l].fl[k];
                               f += (float)1/4 * x*y * pd[l].fl[k];  
                              }
                       }    
                    det= e*c*4 - f*f;
                    
                    if ( c > 0 || e > 0) return win_printf_OK("i shut john wayne");
                    if ( det == 0 ) return win_printf_OK("problem with max");
                           
                    nxmax = j - (-f*d+(float)2*e*b)/det;
                    nymax = i - (-c*d*2+f*b)/det;
                    tab[0][nps_c]= nxmax;
                    tab[1][nps_c]= nymax;                     
                } 
            }   
        } 
     }     
  return **tab;     
}               

int do_movie_avg_calibration_new(void)
{
   O_i *ois1, *ois2, *oid;/*each frame correspond to a diffferent objectiv position !!!!*/
   imreg *imr;
   int  im, i, j, k;
   union pix *pd; 
   static float ax=1, ay=1, xc = 32, yc = 32 /*dfi=1*/; //dark-field intensity -> dfi   ; 
   static int r_max=30, nf=64, nps=1; /*steps number in objectif => nf frames' number*/ 
   
   O_p *opn;
   d_s *ds;
   
  if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;	
    }	
  if(updating_menu_state != 0)	return D_O_K;
  
  if (key[KEY_LSHIFT]) return win_printf_OK("This routine create a calibration image i-e radial profil for different focusing");
  
   i = win_scanf( "give a maximum radius %d \n"
                  "give the number of different objectiv positions %d \n"
                  "enter tne particles' number %d \n"
                  //"give the dark-field intensity collected by camera %8f \n"
                  , &r_max, &nf, &nps/*, &dfi*/);
   if (i == WIN_CANCEL) return D_O_K;  
    
   ois1 = imr -> o_i[1];
   ois2 = imr -> o_i[imr->cur_oi];
   
   opn = find_im_cur_op(imr);
   ds = opn->dat[0];
   
   for (k=0 ; k < nps; k++)
      { 
         if ((oid = create_and_attach_oi_to_imr(imr, 2*r_max, nf, IS_FLOAT_IMAGE)) == NULL)
         return win_printf_OK("cannot create image!");   
         pd = oid->im.pixel;
         for (im=0; im < nf; im++)
            {
               switch_frame(ois1,im);
               xc = ds -> xd[k];
               yc = ds -> yd[k];
               radial_non_pixel_square_image_sym_profile_in_array(pd[im].fl, ois1, xc, yc, r_max, ax, ay); 
              
            }
         for (i=0; i < nf; i++)
                  { 
                      for (j=0; j < 2*r_max; j++)
                         {
                            //pd[i].fl[j] -= dfi;    
                            pd[i].fl[j] /= pd[i].fl[2*r_max-1];
                         }    
                  }
         inherit_from_im_to_im(oid,ois2);
         set_oi_horizontal_extend(oid, 1);
         set_oi_vertical_extend(oid, 1);
         set_formated_string(&oid->im.treatement,"radial profile %s", ois2->filename);
         find_zmin_zmax(oid);  
      }
  
 return (refresh_image(imr, imr->n_oi - 1));     
}


int do_calibration_xi2_slidding_correlate(void)                                                                                                                                                                                                                                                       
{
 imreg *imr; 
 O_i *ois1, *ois2;               
 static int nf=64,  r_max=30;           
 union pix *pd1, *pd2; 
 int i, j, i1, i2;//z_s zshift in pixels between "center" of calibration image
 float xi2, alpha, num, dem, err;
 static int  zsmax=10;
 int ptn, zs, np; // ptn number of points
 
 
 O_p *opn;
 d_s *ds, *ds1, *ds2;

 if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;	
    }
 	                
 if(updating_menu_state != 0)	return D_O_K;                
 
 if (key[KEY_LSHIFT]) return win_printf_OK("This routine find the coefficient beetween two calibration images for different NPs size");

 i = win_scanf("number of steps in calibration images %d\n"
               "enter r_max of  calibration images %d\n"
               "enter zsmax in pixels %d\n"
               , &nf, &r_max, &zsmax);
 if (i == WIN_CANCEL)	return D_O_K;
 
  ois1 = imr -> o_i[imr->cur_oi-1];
  ois2 = imr -> o_i[imr->cur_oi];
  
  pd1 = ois1->im.pixel;                                            
  pd2 = ois2->im.pixel;
  
 if ((opn = create_and_attach_op_to_oi (ois2, 16, 16, 0,IM_SAME_X_AXIS | IM_SAME_Y_AXIS)) == NULL)
		return win_printf_OK("cannot create plot !"); 
 ds = opn->dat[0];
 set_ds_dot_line(ds);
 set_ds_point_symbol(ds, "\\oc");
 ds->nx = ds->ny = 0; 
 
 np=2*r_max;
 if ((ds1 = create_and_attach_one_ds(opn, np, np, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
 set_ds_dot_line(ds1);
 set_ds_point_symbol(ds1, "\\oc");
 ds1->nx = ds1->ny = 0;	
 
 if ((ds2 = create_and_attach_one_ds(opn, np, np, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
 set_ds_dot_line(ds2);
 set_ds_point_symbol(ds2, "\\oc");
 ds2->nx = ds2->ny = 0;	
 
 for (zs = -zsmax; zs < zsmax; zs++)
  { 
     num=0;
     dem=0;
     xi2=0;
     err=0; 
     
     ptn = 2*r_max*(nf-abs(zs));//don't forget normalisation here!!!!!!
     
     if (zs < 0)
        {  
           for (i2=0;  i2 < nf+zs; i2++)  
              {
                i1=i2-zs;
                for (j=0; j < 2*r_max; j++)
                   {
                     num += pd1[i1].fl[j]*pd2[i2].fl[j];
                     dem += pd1[i1].fl[j]*pd1[i1].fl[j];
                   }
              } 
           alpha = num/dem;   
           add_new_point_to_ds(ds, (float)zs, (float) alpha);
           
           for (i2=0; i2 < nf+zs; i2++)  
              {
                i1=i2-zs;
                for (j=0; j < 2*r_max; j++) xi2 +=(pd2[i2].fl[j] - alpha*pd1[i1].fl[j]) * (pd2[i2].fl[j] - alpha * pd1[i1].fl[j]);     
              } 
           xi2 /= (float)ptn;
           add_new_point_to_ds(ds1, (float)zs, (float) xi2);
           
           for (i2=0; i2 < nf+zs; i2++)  
              {
                i1=i2-zs;
                for (j=0; j < 2*r_max; j++) 
                    {
                          err += fabs((pd2[i2].fl[j] - alpha*pd1[i1].fl[j]))/(alpha*pd1[i1].fl[j]);         
                    }                     
              } 
           err /= (float)ptn; 
           add_new_point_to_ds(ds2, (float)zs, (float) err);  
         } 
         
         
      else if (zs == 0)
         { 
           
           for (i=0; i < nf; i++)  
              {  
                for (j=0; j < 2*r_max; j++) 
                   {
                     num += pd1[i].fl[j]*pd2[i].fl[j];
                     dem += pd1[i].fl[j]*pd1[i].fl[j];
                   }
              } 
           alpha = num/dem; 
           add_new_point_to_ds(ds, (float)zs, (float) alpha);
           
           for (i=0; i < nf; i++)  
              {
                for (j=0; j < 2*r_max; j++) xi2 +=  (pd2[i].fl[j] - alpha*pd1[i].fl[j]) * (pd2[i].fl[j] - alpha * pd1[i].fl[j]);       
              }  
          xi2 /= (float)ptn;     
          add_new_point_to_ds(ds1, (float)zs, (float) xi2);
          
           for (i=0; i < nf; i++)  
              {
                for (j=0; j < 2*r_max; j++) 
                    {
                          err += fabs((pd2[i].fl[j] - alpha*pd1[i].fl[j]))/(alpha*pd1[i].fl[j]);
                    }             
              }  
          err /= (float)ptn; 
          add_new_point_to_ds(ds2, (float)zs, (float) err);
         }  
         
             
      else  
        {
           for (i1=0; i1 < nf-zs; i1++)  
              {
                i2=i1+zs;
                for (j=0; j < 2*r_max; j++) 
                   {
                     num += pd1[i1].fl[j]*pd2[i2].fl[j];
                     dem += pd1[i1].fl[j]*pd1[i1].fl[j];
                   }
              }   
           alpha = num/dem;
           add_new_point_to_ds(ds, (float)zs, (float) alpha);
           
           for (i1=0; i1 < nf-zs; i1++)  
              {
                i2=i1+zs;
                for (j=0; j < 2*r_max; j++) xi2 +=  (pd2[i2].fl[j] - alpha*pd1[i1].fl[j]) * (pd2[i2].fl[j] - alpha * pd1[i1].fl[j]);       
              } 
           xi2 /= (float)ptn;    
           add_new_point_to_ds(ds1, (float)zs, (float) xi2);  
           
           for (i1=0; i1 < nf-zs; i1++)  
              {
                i2=i1+zs;
                for (j=0; j < 2*r_max; j++) 
                    {
                         err += fabs((pd2[i2].fl[j] - alpha*pd1[i1].fl[j]))/(alpha*pd1[i1].fl[j]);
                    }           
              } 
           err /= (float)ptn; 
           add_new_point_to_ds(ds2, (float)zs, (float) err);  
         }   
  }

return (refresh_image(imr, imr->n_oi - 1));
                          
}
int do_calibration_xi2_slidding_nocorrelate(void)                                                                                                                                                                                                                                                       
{
 imreg *imr; 
 O_i *ois1, *ois2;               
 static int nf=64,  r_max=30;           
 union pix *pd1, *pd2; 
 int i, j, i1, i2;//z_s zshift in pixels between "center" of calibration image
 float xi2, alpha, num, dem, err;
 static int  zsmax=10;
 int ptn, zs, np; // ptn number of points
 
 
 O_p *opn;
 d_s *ds, *ds1, *ds2;

 if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;	
    }
 	                
 if(updating_menu_state != 0)	return D_O_K;                
 
 if (key[KEY_LSHIFT]) return win_printf_OK("This routine find the coefficient beetween two calibration images for different NPs size");

 i = win_scanf("number of steps in calibration images %d\n"
               "enter r_max of  calibration images %d\n"
               "enter zsmax in pixels %d\n"
               , &nf, &r_max, &zsmax);
 if (i == WIN_CANCEL)	return D_O_K;
 
  ois1 = imr -> o_i[imr->cur_oi-1];
  ois2 = imr -> o_i[imr->cur_oi];
  
  pd1 = ois1->im.pixel;                                            
  pd2 = ois2->im.pixel;
  
 if ((opn = create_and_attach_op_to_oi (ois2, 16, 16, 0,IM_SAME_X_AXIS | IM_SAME_Y_AXIS)) == NULL)
		return win_printf_OK("cannot create plot !"); 
 ds = opn->dat[0];
 set_ds_dot_line(ds);
 set_ds_point_symbol(ds, "\\oc");
 ds->nx = ds->ny = 0; 
 
 np=2*r_max;
 if ((ds1 = create_and_attach_one_ds(opn, np, np, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
 set_ds_dot_line(ds1);
 set_ds_point_symbol(ds1, "\\oc");
 ds1->nx = ds1->ny = 0;	
 
 if ((ds2 = create_and_attach_one_ds(opn, np, np, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
 set_ds_dot_line(ds2);
 set_ds_point_symbol(ds2, "\\oc");
 ds2->nx = ds2->ny = 0;	
 
 
 for (zs = -zsmax; zs < zsmax; zs++)
  { 
     num=0;
     dem=0;
     xi2=0;
     err=0; 
     
     ptn = 2*r_max*(nf-abs(zs));//don't forget normalisation here!!!!!!
     
     if (zs < 0)
        {  
           for (i2=0;  i2 < nf+zs; i2++)  
              {
                i1=i2-zs;
                for (j=0; j < 2*r_max; j++)
                   {
                     num += pd2[i2].fl[j]*pd2[i2].fl[j];
                     dem += pd1[i1].fl[j]*pd1[i1].fl[j];
                   }
              } 
           alpha = num/dem;   
           add_new_point_to_ds(ds, (float)zs, (float) alpha);
           
           for (i2=0; i2 < nf+zs; i2++)  
              {
                i1=i2-zs;
                for (j=0; j < 2*r_max; j++) xi2 +=(pd2[i2].fl[j] - alpha*pd1[i1].fl[j]) * (pd2[i2].fl[j] - alpha * pd1[i1].fl[j]);     
              } 
           xi2 /= (float)ptn;
           add_new_point_to_ds(ds1, (float)zs, (float) xi2);
           
           for (i2=0; i2 < nf+zs; i2++)  
              {
                i1=i2-zs;
                for (j=0; j < 2*r_max; j++) 
                    {
                          err += fabs((pd2[i2].fl[j] - alpha*pd1[i1].fl[j]))/(alpha*pd1[i1].fl[j]);         
                    }                     
              } 
           err /= (float)ptn; 
           add_new_point_to_ds(ds2, (float)zs, (float) err);  
         } 
         
         
      else if (zs == 0)
         { 
           
           for (i=0; i < nf; i++)  
              {  
                for (j=0; j < 2*r_max; j++) 
                   {
                     num += pd2[i].fl[j]*pd2[i].fl[j];
                     dem += pd1[i].fl[j]*pd1[i].fl[j];
                   }
              } 
           alpha = num/dem; 
           add_new_point_to_ds(ds, (float)zs, (float) alpha);
           
           for (i=0; i < nf; i++)  
              {
                for (j=0; j < 2*r_max; j++) xi2 +=  (pd2[i].fl[j] - alpha*pd1[i].fl[j]) * (pd2[i].fl[j] - alpha * pd1[i].fl[j]);       
              }  
          xi2 /= (float)ptn;     
          add_new_point_to_ds(ds1, (float)zs, (float) xi2);
          
           for (i=0; i < nf; i++)  
              {
                for (j=0; j < 2*r_max; j++) 
                    {
                          err += fabs((pd2[i].fl[j] - alpha*pd1[i].fl[j]))/(alpha*pd1[i].fl[j]);
                    }             
              }  
          err /= (float)ptn; 
          add_new_point_to_ds(ds2, (float)zs, (float) err);
         }  
         
             
      else  
        {
           for (i1=0; i1 < nf-zs; i1++)  
              {
                i2=i1+zs;
                for (j=0; j < 2*r_max; j++) 
                   {
                     num += pd2[i2].fl[j]*pd2[i2].fl[j];
                     dem += pd1[i1].fl[j]*pd1[i1].fl[j];
                   }
              }   
           alpha = num/dem;
           add_new_point_to_ds(ds, (float)zs, (float) alpha);
           
           for (i1=0; i1 < nf-zs; i1++)  
              {
                i2=i1+zs;
                for (j=0; j < 2*r_max; j++) xi2 +=  (pd2[i2].fl[j] - alpha*pd1[i1].fl[j]) * (pd2[i2].fl[j] - alpha * pd1[i1].fl[j]);       
              } 
           xi2 /= (float)ptn;    
           add_new_point_to_ds(ds1, (float)zs, (float) xi2);  
           
           for (i1=0; i1 < nf-zs; i1++)  
              {
                i2=i1+zs;
                for (j=0; j < 2*r_max; j++) 
                    {
                         err += fabs((pd2[i2].fl[j] - alpha*pd1[i1].fl[j]))/(alpha*pd1[i1].fl[j]);
                    }           
              } 
           err /= (float)ptn; 
           add_new_point_to_ds(ds2, (float)zs, (float) err);  
         }   
  }

return (refresh_image(imr, imr->n_oi - 1));
                          
}
int do_calibration_xi2_slidding_correlate_substract(void)                                                                                                                                                                                                                                                       
{
 imreg *imr; 
 O_i *ois1, *ois2;               
 static int nf=64,  r_max=30;           
 union pix *pd1, *pd2; 
 int i, j, i1, i2, nmin, delta;//z_s zshift in pixels between "center" of calibration image
 float xi2, alpha, num, dem, a, b, c, d, f_2, f_1, f0, f1, f2, xm = 0, xc, yc, y2;
 static int  zsmax=10, io = -5, imax = 5;
 int ptn, zs, np; // ptn number of points
 

 O_p *opn;
 d_s *ds, *ds1, *ds2;

     if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
     {
        active_menu->flags |=  D_DISABLED;
        return D_O_K;	
      }
 	                
      if(updating_menu_state != 0)	return D_O_K;                
 
      if (key[KEY_LSHIFT]) 
         return win_printf_OK("This routine find the coefficient beetween two calibration images for different NPs size");

         i = win_scanf("number of steps in calibration images %d\n"
               "enter r_max of  calibration images %d\n"
               "enter zsmax in pixels %d\n"
               "chose to start to find minimum from x-coordinate = io (absio < zmax) %d\n"
               "chose to stop to find minimum from x-coordinate = imax (imax < zmax) %d\n"
               , &nf, &r_max, &zsmax, &io, &imax);      
         if (i == WIN_CANCEL)	return D_O_K;
         
         i = imr->cur_oi-1;
         i = (i < 0) ? i+ imr->n_oi : i;
         ois1 = imr -> o_i[i];
         ois2 = imr -> o_i[imr->cur_oi];
  
         pd1 = ois1->im.pixel;                                            
         pd2 = ois2->im.pixel;
      np=2*zsmax+1;
      
      if ((opn = create_and_attach_op_to_oi (ois2, 1, 1, 0,IM_SAME_X_AXIS | IM_SAME_Y_AXIS)) == NULL)
		return win_printf_OK("cannot create plot !"); 
      ds = opn->dat[0];
      set_ds_dot_line(ds);
      set_ds_point_symbol(ds, "\\oc");
      ds->nx = ds->ny = 0; 
 
      
      if ((ds1 = create_and_attach_one_ds(opn, np, np, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
     set_ds_dot_line(ds1);
     set_ds_point_symbol(ds1, "\\oc");
     ds1->nx = ds1->ny = 0;	
     
      if ((ds2 = create_and_attach_one_ds(opn, np, np, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
     set_ds_dot_line(ds2);
     set_ds_point_symbol(ds2, "\\oc");
     ds2->nx = ds2->ny = 0;	

     for (zs = -zsmax; zs < zsmax + 1; zs++)
     { 
       num=0;     dem=0;        xi2=0; 
       ptn = 2*r_max*(nf-abs(zs));//don't forget normalisation here!!!!!!
     
       if (zs < 0)
        {  
           for (i2=0;  i2 < nf+zs; i2++)  
              {
                i1=i2-zs;
                for (j=0; j < 2*r_max; j++)
                   {
                     num += (pd1[i1].fl[j] - 1)*(pd2[i2].fl[j] - 1);
                     dem += (pd1[i1].fl[j] - 1)*(pd1[i1].fl[j] - 1);
                   }
              } 
           alpha = num/dem;   
           for (i2=0; i2 < nf+zs; i2++)  
              {
                i1=i2-zs;
                for (j=0; j < 2*r_max; j++) 
                    xi2 +=((pd2[i2].fl[j] - 1) - alpha*(pd1[i1].fl[j] - 1)) 
                        * ((pd2[i2].fl[j] - 1) - alpha * (pd1[i1].fl[j] - 1));     
              }
           xi2 = pow(xi2, 0.5);   
           xi2 /= (float)ptn;
           add_new_point_to_ds(ds1, (float)zs, (float) xi2);
           
         } 
       else if (zs == 0)
         { 
           for (i=0; i < nf; i++)  
              {  
                for (j=0; j < 2*r_max; j++) 
                   {
                     num += (pd1[i].fl[j] - 1) * (pd2[i].fl[j] - 1);
                     dem += (pd1[i].fl[j] - 1) * (pd1[i].fl[j] - 1);
                   }
              } 
           alpha = num/dem; 
           for (i=0; i < nf; i++)  
              {
                for (j=0; j < 2*r_max; j++) 
                    xi2 +=  ((pd2[i].fl[j] - 1) - alpha*(pd1[i].fl[j] - 1)) * ((pd2[i].fl[j] - 1) - alpha*(pd1[i].fl[j] - 1));       
              } 
          xi2 = pow(xi2, 0.5); 
          xi2 /= (float)ptn; 
          add_new_point_to_ds(ds1, (float)zs, (float) xi2);
         }  
       else  
         {
           for (i1=0; i1 < nf-zs; i1++)  
              {
                i2=i1+zs;
                for (j=0; j < 2*r_max; j++) 
                   {
                     num += (pd1[i1].fl[j] - 1)*(pd2[i2].fl[j] - 1);
                     dem += (pd1[i1].fl[j] - 1)*(pd1[i1].fl[j] - 1);
                   }
              }   
           alpha = num/dem;
           for (i1=0; i1 < nf-zs; i1++)  
              {
                i2=i1+zs;
                for (j=0; j < 2*r_max; j++) 
                 xi2 +=((pd2[i2].fl[j] - 1) - alpha*(pd1[i1].fl[j] - 1)) 
                        * ((pd2[i2].fl[j] - 1) - alpha * (pd1[i1].fl[j] - 1));     
              }
           xi2 = pow(xi2, 0.5);  
           xi2 /= (float)ptn;    
           add_new_point_to_ds(ds1, (float)zs, (float) xi2);   
         }
      } 
         
     for (i = io+zsmax, nmin = io+zsmax; i < zsmax+imax; i++) nmin = (ds1->yd[i] < ds1->yd[nmin]) ? i : nmin; 
         
         f_2 = ds1->yd[(nmin - 2)];
         f_1 = ds1->yd[(nmin - 1)];
   	     f0 = ds1->yd[nmin];
	     f1 = ds1->yd[(nmin + 1)];
	     f2 = ds1->yd[(nmin + 2)];
	     if (f_1 > f1)
	       { 
             a = -f_1/6 + f0/2 - f1/2 + f2/6;
		     b = f_1/2 - f0 + f1/2;
      	     c = -f_1/3 - f0/2 + f1 - f2/6;
		     d = f0;
	       }	
	     else
           {
            a=b=c=d=0;
	        a = -f_2/6 + f_1/2 - f0/2 + f1/6;
		    b = f_2/2 - f_1 + f0/2;
		    c = -f_2/3 - f_1/2 + f0 - f1/6;
	        d = f_1;
           }
	     if (f_1 > f1)
	        {
              nmin -= zsmax; 
              for (i = -1; i < 3; i++)
                 {
                   y2 = a * (float)(i * i * i) + b * (float)(i * i) + c * (float)i + d;
                   add_new_point_to_ds(ds2, (float)(i + nmin), (float)y2);                
                 }   
              xm = (-b + sqrt(b*b - 3*a*c))/(3*a);
              xc = xm;
              yc = a * (xc*xc*xc) + b * (xc*xc) + c * xc + d;
              add_new_point_to_ds(ds2, (float)(xc + nmin), (float)yc);     
            }
         else
	        { 
              delta = 1;
              nmin -= zsmax;
              for (i = -2; i < 2; i++)
                 {
                   j = i + delta;
                   y2 = a * (float)(j * j * j) + b * (float)(j * j) + c * (float)(j) + d;
                   add_new_point_to_ds(ds2, (float)(i + nmin), (float)y2);
                 } 
              xm = (-b + sqrt(b*b - 3*a*c))/(3*a);   
        	  yc = a * (xm*xm*xm) + b * (xm*xm) + c * xm + d;
        	  xc = (float)(nmin + xm - delta);
              add_new_point_to_ds(ds2, (float)xc, (float)yc);           
            }
	
    
return (refresh_image(imr, imr->n_oi - 1));
                          
}

int do_calibration_xi2_slidding_nocorrelate_substract(void)                                                                                                                                                                                                                                                       
{
 imreg *imr; 
 O_i *ois1, *ois2;               
 static int nf=64,  r_max=30;           
 union pix *pd1, *pd2; 
 int i, j, i1, i2;//z_s zshift in pixels between "center" of calibration image
 float xi2, alpha, num, dem, numr, demr, err;
 static int  zsmax=10;
 int ptn, zs, np; // ptn number of points
 
 
 O_p *opn;
 d_s *ds, *ds1, *ds2;

 if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;	
    }
 	                
 if(updating_menu_state != 0)	return D_O_K;                
 
 if (key[KEY_LSHIFT]) return win_printf_OK("This routine find the coefficient beetween two calibration images for different NPs size");

 i = win_scanf("number of steps in calibration images %d\n"
               "enter r_max of  calibration images %d\n"
               "enter zsmax in pixels %d\n"
               , &nf, &r_max, &zsmax);
 if (i == WIN_CANCEL)	return D_O_K;
 
  ois1 = imr -> o_i[imr->cur_oi-1];
  ois2 = imr -> o_i[imr->cur_oi];
  
  pd1 = ois1->im.pixel;                                            
  pd2 = ois2->im.pixel;
   for (i=0;  i < nf; i++)  
              {
                for (j=0; j < 2*r_max; j++)
                   {
                        pd1[i].fl[j] -= (float)1;
                        pd2[i].fl[j] -= (float)1;
                   }
              }   
 if ((opn = create_and_attach_op_to_oi (ois2, 16, 16, 0,IM_SAME_X_AXIS | IM_SAME_Y_AXIS)) == NULL)
		return win_printf_OK("cannot create plot !"); 
 ds = opn->dat[0];
 set_ds_dot_line(ds);
 set_ds_point_symbol(ds, "\\oc");
 ds->nx = ds->ny = 0; 
 
 np=2*r_max;
 if ((ds1 = create_and_attach_one_ds(opn, np, np, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
 set_ds_dot_line(ds1);
 set_ds_point_symbol(ds1, "\\oc");
 ds1->nx = ds1->ny = 0;	
 
 if ((ds2 = create_and_attach_one_ds(opn, np, np, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
 set_ds_dot_line(ds2);
 set_ds_point_symbol(ds2, "\\oc");
 ds2->nx = ds2->ny = 0;	
 
 for (zs = -zsmax; zs < zsmax; zs++)
  { 
     num=0;
     dem=0;
     numr=0;
     demr=0;
     xi2=0; 
     
     ptn = 2*r_max*(nf-abs(zs));//don't forget normalisation here!!!!!!
     
     if (zs < 0)
        {  
           for (i2=0;  i2 < nf+zs; i2++)  
              {
                i1=i2-zs;
                for (j=0; j < 2*r_max; j++)
                   {
                     num += pd1[i1].fl[j]*pd1[i1].fl[j];
                     dem += pd2[i2].fl[j]*pd2[i2].fl[j];
                   }
              } 
           alpha = num/dem;   
           add_new_point_to_ds(ds, (float)zs, (float) alpha);
           
           for (i2=0; i2 < nf+zs; i2++)  
              {
                i1=i2-zs;
                for (j=0; j < 2*r_max; j++) xi2 +=(pd2[i2].fl[j] - alpha*pd1[i1].fl[j]) * (pd2[i2].fl[j] - alpha * pd1[i1].fl[j]);     
              } 
           xi2 /= (float)ptn;
           add_new_point_to_ds(ds1, (float)zs, (float) xi2);
           
           for (i2=0; i2 < nf+zs; i2++)  
              {
                i1=i2-zs;
                for (j=0; j < 2*r_max; j++) 
                    {
                          numr += (pd2[i2].fl[j] - alpha*pd1[i1].fl[j])*(pd2[i2].fl[j] - alpha*pd1[i1].fl[j]);
                          demr += (alpha*pd1[i1].fl[j])*(alpha*pd1[i1].fl[j]);     
                    }                     
              } 
           err = numr/demr;
           err = pow(err,0.5);
           err /= (float)ptn; 
           add_new_point_to_ds(ds2, (float)zs, (float) err);  
         } 
         
         
      else if (zs == 0)
         { 
           
           for (i=0; i < nf; i++)  
              {  
                for (j=0; j < 2*r_max; j++) 
                   {
                     num += pd1[i].fl[j]*pd1[i].fl[j];
                     dem += pd2[i].fl[j]*pd2[i].fl[j];
                   }
              } 
           alpha = num/dem; 
           add_new_point_to_ds(ds, (float)zs, (float) alpha);
           
           for (i=0; i < nf; i++)  
              {
                for (j=0; j < 2*r_max; j++) xi2 +=  (pd2[i].fl[j] - alpha*pd1[i].fl[j]) * (pd2[i].fl[j] - alpha * pd1[i].fl[j]);       
              }  
          xi2 /= (float)ptn;     
          add_new_point_to_ds(ds1, (float)zs, (float) xi2);
          
           for (i=0; i < nf; i++)  
              {
                for (j=0; j < 2*r_max; j++) 
                    {
                          numr += (pd2[i].fl[j] - alpha*pd1[i].fl[j])*(pd2[i].fl[j] - alpha*pd1[i].fl[j]);
                          demr += (alpha*pd1[i].fl[j])*(alpha*pd1[i].fl[j]);
                    }             
              }  
          err = numr/demr;
          err = pow(err,0.5);
          err /= (float)ptn; 
          add_new_point_to_ds(ds2, (float)zs, (float) err);
         }  
         
             
      else  
        {
           for (i1=0; i1 < nf-zs; i1++)  
              {
                i2=i1+zs;
                for (j=0; j < 2*r_max; j++) 
                   {
                     num += pd1[i1].fl[j]*pd1[i1].fl[j];
                     dem += pd2[i2].fl[j]*pd2[i2].fl[j];
                   }
              }   
           alpha = num/dem;
           add_new_point_to_ds(ds, (float)zs, (float) alpha);
           
           for (i1=0; i1 < nf-zs; i1++)  
              {
                i2=i1+zs;
                for (j=0; j < 2*r_max; j++) xi2 +=  (pd2[i2].fl[j] - alpha*pd1[i1].fl[j]) * (pd2[i2].fl[j] - alpha * pd1[i1].fl[j]);       
              } 
           xi2 /= (float)ptn;    
           add_new_point_to_ds(ds1, (float)zs, (float) xi2);  
           
           for (i1=0; i1 < nf-zs; i1++)  
              {
                i2=i1+zs;
                for (j=0; j < 2*r_max; j++) 
                    {
                         numr += (pd2[i2].fl[j] - alpha*pd1[i1].fl[j])*(pd2[i2].fl[j] - alpha*pd1[i1].fl[j]);
                         demr += (alpha*pd1[i1].fl[j])*(alpha*pd1[i1].fl[j]);
                    }           
              } 
           err = numr/demr;
           err = pow(err,0.5);
           err /= (float)ptn; 
           add_new_point_to_ds(ds2, (float)zs, (float) err);  
         }   
  }

return (refresh_image(imr, imr->n_oi - 1));
                          
}
int do_find_alpha_byminimumxi2(void)
{
 
 imreg *imr; 
 O_i *ois1, *ois2;              
 static int nf=64,  r_max=30;           
 union pix *pd1, *pd2; 
 int i, j;     
 float num, dem, alpha;       

 
 if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;	
    }
 
                       
 if(updating_menu_state != 0)	return D_O_K;                
 
 if (key[KEY_LSHIFT]) return win_printf_OK("This routine find the coefficient beetween two calibration images for different NPs size without slidding");
 

	
 //i = win_scanf("number of steps in z in calibration images %d\n"
             // "enter r_max of  calibration images %d\n"
     //          ,&nf, &r_max);
 //if (i == WIN_CANCEL)	return D_O_K;
 
    ois1 = imr -> o_i[imr->cur_oi-1];
    ois2 = imr -> o_i[imr->cur_oi];
  
 
 for (i=0,num=dem=0,pd1 = ois1->im.pixel,
   pd2 = ois2->im.pixel; i < nf; i++)
     {
           for (j=0; j < 2*r_max; j++)
              {
                //pd1[i].fl[j] -= (float)1;
                //pd2[i].fl[j] -= (float)1;
                num += pd1[i].fl[j]*pd2[i].fl[j];
                dem += pd1[i].fl[j]*pd1[i].fl[j];   
              }  
     } 
     
 alpha = num/dem;  
 win_printf("and alpha is %8f\n", alpha);
 
 return D_O_K;                               
}
int do_calibration_substract_image(void)   
{
  imreg *imr; 
  O_i *ois1, *ois2, *oid;
  union pix *pd1, *pd2, *pd; 
  int i, j;
  static int nf=64,  r_max=30;
  static float alpha = 0.9;
  
    if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
     {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;	
     }
 	                
    if(updating_menu_state != 0)	return D_O_K;                
    
    if (key[KEY_LSHIFT]) return win_printf_OK("This routine create a new image by substracting two");
    
         i = imr->cur_oi-1;
         i = (i < 0) ? i+ imr->n_oi : i;
         ois1 = imr -> o_i[i];
         ois2 = imr -> o_i[imr->cur_oi];
  
        
    i = win_scanf("number of steps in calibration images %d\n"
               "enter r_max of  calibration images %d\n"
               "coefficient on image 2 %8f\n", &nf, &r_max, &alpha);
    if (i == WIN_CANCEL)	return D_O_K;
     
    if ((oid = create_and_attach_oi_to_imr(imr, 2*r_max,  nf, IS_FLOAT_IMAGE)) == NULL)
         return win_printf_OK("cannot create image!");   
         pd = oid->im.pixel;    
         pd1 = ois1->im.pixel;                                            
         pd2 = ois2->im.pixel;
         
         for (i=0; i < nf; i++)  
              {  
                for (j=0; j < 2*r_max; j++) 
                   {
                     pd[i].fl[j] = (pd2[i].fl[j] - 1) - alpha * (pd1[i].fl[j] - 1);
                   }
              }     
    inherit_from_im_to_im(oid,ois2);
    set_oi_horizontal_extend(oid, 1);
    set_oi_vertical_extend(oid, 1);
    set_formated_string(&oid->im.treatement,"radial profile %s", ois2->filename);
    find_zmin_zmax(oid);
    return (refresh_image(imr, imr->n_oi - 1));     
}
int do_find_min_dataset(void)
{
	O_p *opn = NULL;
	int i, nmax, delta;
    static int io = -5, imax = 5;
	d_s *dsi, *ds;
	pltreg *pr = NULL;
	float a, b, c, d, f_2, f_1, f0, f1, f2, xm = 0, err, xc;

	if(updating_menu_state != 0)	return D_O_K;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine find minimum on dataset");
	}

	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&opn,&dsi) != 3)
		return win_printf_OK("cannot find data");
		
	if ((ds = create_and_attach_one_ds(opn, 4, 4, 0)) == NULL)
		return win_printf_OK("cannot create plot !");	
	    set_ds_dot_line(ds);
        set_ds_point_symbol(ds, "\\oc");
	
    i = win_scanf("chose to start to find minimum from x-coordinate = io %d\n"
                  "chose to stop to find minimum from x-coordinate = imax %d\n"
                  ,&io ,&imax);
    
         for (i=io, nmax=io; i < imax; i++) nmax = (dsi->yd[i] < dsi->yd[nmax]) ? i : nmax;
         f_2 = dsi->yd[(nmax - 2)];
  	     f_1 = dsi->yd[(nmax - 1)];
       	 f0 = dsi->yd[nmax];
	     f1 = dsi->yd[(nmax + 1)];
	     f2 = dsi->yd[(nmax + 2)];
	     if (f_1 < f1)
	       {
		     a = -f_1/6 + f0/2 - f1/2 + f2/6;
		     b = f_1/2 - f0 + f1/2;
		     c = -f_1/3 - f0/2 + f1 - f2/6;
		     d = f0;
    		 delta = 0;
	       }	
	     else
           {
		     a = b = c = 0;	
		     a = -f_2/6 + f_1/2 - f0/2 + f1/6;
		     b = f_2/2 - f_1 + f0/2;
		     c = -f_2/3 - f_1/2 + f0 - f1/6;
		     d = f_1;
		     delta = -1;
           }
	     if (fabs(a) < 1e-8)
	       {
		      if (b != 0)	xm =  - c/(2*b) - (3 * a * c * c)/(4 * b * b * b);
		      else
		         {
			       xm = 0;
			       return win_printf_OK("problem with max");
		         }
           }
         else if ((b*b - 3*a*c) < 0)
	             {
                   return win_printf_OK("problem with max");
		           xm = 0;
	             }
         else
		           xm = (-b - sqrt(b*b - 3*a*c))/(3*a);
	     if (f_1 < f1)
	        {
              for (i = nmax - 2; i < nmax + 2; i++)
                 {
                   ds->xd[i] = dsi->xd[i];
                   ds->yd[i] = a * (float)(i * i * i) + b * (float)(i * i) + c * (float)(i) + d;
                   err = 0;
                   err +=  (ds->yd[i] - dsi->yd[i])/dsi->yd[i]; 
                   err /= (float)4; 
                 }      
            }
         else
	        { 
              for (i = nmax - 1; i < nmax + 3; i++)
                 {
                   ds->xd[i] = dsi->xd[i];
                   ds->yd[i] = a * (float)(i * i * i) + b * (float)(i * i) + c * (float)(i) + d;
                   err = 0;
                   err +=  (ds->yd[i] - dsi->yd[i])/(dsi->yd[i]); 
                   err /= (float)4; 
                 }            
            }
            
	xc = (float)(xm + nmax + delta);
	i = win_printf("x-cooordinate of minimum %8f\n"
                   "average relative error %8f\n"
                   , xc, err);
    if (i == WIN_CANCEL)	return D_O_K;
    
	refresh_plot(pr, UNCHANGED);
	return D_O_K;  	
}


int do_movie_to_movie(void)
{
  O_i *ois, *oid;
  imreg *imr;
  static int nxd = 128, nyd = 64, nfd = 1024, xc = 100, yc = 100;
  int nx, ny, nf, i, j, l, k, im;
  union pix  *pds, *pd;
  //float *z = NULL;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;     
    } 
  nf = abs(ois->im.n_f); 
  if (nf <= 0)     
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;
    }
  else   active_menu->flags &= ~D_DISABLED;

  if(updating_menu_state != 0)    return D_O_K;
  i = ois->im.c_f;
  if (ois->im.data_type == IS_COMPLEX_IMAGE
     || ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)  
    {
      win_printf("I do not handle complexe images ! yet");
      return D_O_K;
    }
  nx = ois->im.nx;
  ny = ois->im.ny;

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine create a box of define size in a movie ");
    }
   
  i = win_scanf("choose the number of frames for new movie  %d\n"
                "choose the center cooordinates in pixel xc %d and yc %d\n"
                "choose the width in pixel (pair) %d\n"
                "choose the height in pixel (pair)%d\n", &nfd, &xc, &yc, &nxd, &nyd);
  if (i == WIN_CANCEL)	return D_O_K;
  
  if ((oid = create_and_attach_movie_to_imr(imr, nxd, nyd, IS_CHAR_IMAGE, nfd)) == NULL)
         return win_printf_OK("cannot create image!");  

 for (im = 0; im < nfd ; im++)
       { 
         //pd = oid->im.pixel; 
         //pds = ois->im.pixel;
         switch_frame(ois,im); 
         switch_frame(oid,im);
         //pd = oid->im.pxl[im]; 
         //pds = ois->im.pxl[im];
         pd = oid->im.pixel; 
         pds = ois->im.pixel;
         for (i = 0; i < nyd; i++) 
            {
               l = i + yc - nyd/2;
               for (j = 0; j < nxd; j++) 
               {
                 k = j + xc - nxd/2;
                   pd[i].fl[j] = pds[l].fl[k];
               }
            }       
       }
 
 
 /* for (i = 0; i < nyd; i++) 
      {
           l = i + yc - nyd/2;
           for (j = 0; j < nxd; j++) 
           {
                   k = j + xc - nxd/2;
                   z = extract_z_profile_from_movie(ois, k, l, z);
                   for (im = 0; im < nfd ; im++)
           {
                   pd = oid->im.pixel;
                   switch_frame(oid,im); 
                   pd[i].fl[j] = z[im];
           }
           }
      }       
  */
 inherit_from_im_to_im(oid,ois);
 create_attach_select_x_un_to_oi(oid, IS_METER, 0, 0.098, -6, 0, "\\mu m");
 create_attach_select_y_un_to_oi(oid, IS_METER, 0, 0.098, -6, 0, "\\mu m");
 set_oi_horizontal_extend(oid, 1);
 set_oi_vertical_extend(oid, 1);
 oid->im.win_flag = ois->im.win_flag;
 if (ois->title != NULL)    set_im_title(oid, "%s ", ois->title);
 set_formated_string(&oid->im.treatement," box in movie of %s ", ois->filename);
 find_zmin_zmax(oid);
 //free(z);
 return (refresh_image(imr, imr->n_oi - 1));
}		

int do_movie_avg_ood_and_even_images_2phase(void)
{
 O_i *ois, *oid;
 imreg *imr;
 int nx, ny, nf, i, nf2, im, im2, k, j;
 static int navg = 16;
 float *z;
 union pix *pd;

 if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
   {
     active_menu->flags |=  D_DISABLED;
     return D_O_K;     
   } 
 nf = abs(ois->im.n_f); 
 if (nf <= 0)     
   {
     active_menu->flags |=  D_DISABLED;
     return D_O_K;
   }
 else   active_menu->flags &= ~D_DISABLED;

 if(updating_menu_state != 0)    return D_O_K;
 i = ois->im.c_f;
 if (ois->im.data_type == IS_COMPLEX_IMAGE
     || ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)  
   {
     win_printf("I do not handle complexe images ! yet");
     return D_O_K;
   }
 nx = ois->im.nx;
 ny = ois->im.ny;

 if (key[KEY_LSHIFT])
   {
     return win_printf_OK("This routine average a movie over a subset of images");
   }


 nf2 = 2;
 // two phase with 2 frames consecutivly for each phase//
 oid = create_and_attach_movie_to_imr(imr, nx, ny, IS_FLOAT_IMAGE, nf2);

 z = (float *)calloc(nx,sizeof(float));
 if (oid == NULL || z == NULL)        return win_printf_OK("Can't create dest movie");
 win_printf ("nf %d", nf);
 for (im = 0; im < nf; im+=4)
   { 
     im2=im+1;
     switch_frame(ois,im);
     for (i = 0, pd = oid->im.pxl[0]; i < ny ; i++)
   {
     extract_raw_line (ois, i, z);
     for (j=0; j < nx; j++) pd[i].fl[j] += z[j];
   }
     switch_frame(ois,im2);
     for (i = 0, pd = oid->im.pxl[0]; i < ny ; i++)
   {
     extract_raw_line (ois, i, z);
     for (j=0; j < nx; j++) pd[i].fl[j] += z[j];
   }
   }

 for (im = 2; im < nf; im+=4)
   { 
     im2=im+1;
     switch_frame(ois,im);
     for (i = 0, pd = oid->im.pxl[1]; i < ny ; i++)
   {
     extract_raw_line (ois, i, z);
     for (j=0; j < nx; j++) pd[i].fl[j] += z[j];
   }
     switch_frame(ois,im2);
     for (i = 0, pd = oid->im.pxl[1]; i < ny ; i++)
   {
     extract_raw_line (ois, i, z);
     for (j=0; j < nx; j++) pd[i].fl[j] += z[j];
   }   
   } 
 k = nf/2;
 for (i = 0, pd = oid->im.pxl[0]; i < ny ; i++)
       {
         for (j=0; j < nx; j++)  pd[i].fl[j] /= k;      
       }
 for (i = 0, pd = oid->im.pxl[1]; i < ny ; i++)
       {
         for (j=0; j < nx; j++) pd[i].fl[j] /= k;
       }

 inherit_from_im_to_im(oid,ois);
 uns_oi_2_oi(oid,ois);
 oid->im.win_flag = ois->im.win_flag;
 if (ois->title != NULL)    set_im_title(oid, "%s ", ois->title);
 set_formated_string(&oid->im.treatement,"Avg movie of %s ", ois->filename);
 find_zmin_zmax(oid);
 free(z);
 return (refresh_image(imr, imr->n_oi - 1));
}
int do_movie_avg_2phase_singleframe(void)
{
 O_i *ois, *oid;
 imreg *imr;
 int nx, ny, nf, i, nf2, im, k, j;
 float *z;
 union pix *pd;

 if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
   {
     active_menu->flags |=  D_DISABLED;
     return D_O_K;     
   } 
 nf = abs(ois->im.n_f); 
 if (nf <= 0)     
   {
     active_menu->flags |=  D_DISABLED;
     return D_O_K;
   }
 else   active_menu->flags &= ~D_DISABLED;

 if(updating_menu_state != 0)    return D_O_K;
 i = ois->im.c_f;
 if (ois->im.data_type == IS_COMPLEX_IMAGE
     || ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)  
   {
     win_printf("I do not handle complexe images ! yet");
     return D_O_K;
   }
 nx = ois->im.nx;
 ny = ois->im.ny;

 if (key[KEY_LSHIFT])
   {
     return win_printf_OK("This routine average a movie over a subset of images");
   }


 nf2 = 2;
 // phase pushed every frame//
 oid = create_and_attach_movie_to_imr(imr, nx, ny, IS_FLOAT_IMAGE, nf2);

 z = (float *)calloc(nx,sizeof(float));
 if (oid == NULL || z == NULL)        return win_printf_OK("Can't create dest movie");
 
 if ( nf%2 != 0 ) nf -= 1;
 win_printf ("nf %d", nf);

 for (im = 0; im < nf; im+=2)
   { 
     switch_frame(ois,im);
     for (i = 0, pd = oid->im.pxl[0]; i < ny ; i++)
   {
     extract_raw_line (ois, i, z);
     for (j=0; j < nx; j++) pd[i].fl[j] += z[j];
   }
   }
 for (im = 1; im < nf; im+=2)
   { 
     switch_frame(ois,im);
     for (i = 0, pd = oid->im.pxl[1]; i < ny ; i++)
   {
     extract_raw_line (ois, i, z);
     for (j=0; j < nx; j++) pd[i].fl[j] += z[j];
   }

   } 
 k = nf/2;
 for (i = 0, pd = oid->im.pxl[0]; i < ny ; i++)
       {
         for (j=0; j < nx; j++)  pd[i].fl[j] /= k;      
       }
 for (i = 0, pd = oid->im.pxl[1]; i < ny ; i++)
       {
         for (j=0; j < nx; j++) pd[i].fl[j] /= k;
       }

 inherit_from_im_to_im(oid,ois);
 uns_oi_2_oi(oid,ois);
 oid->im.win_flag = ois->im.win_flag;
 if (ois->title != NULL)    set_im_title(oid, "%s ", ois->title);
 set_formated_string(&oid->im.treatement,"Avg movie of %s ", ois->filename);
 find_zmin_zmax(oid);
 free(z);
 return (refresh_image(imr, imr->n_oi - 1));
}

int do_movie_substractimage_2phase(void)
{
 O_i *ois, *oid;
 imreg *imr;
 int nx, ny, nf, i, j;
 float *z;
 union pix *pd0, *pd, *pd1;

 if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
   {
     active_menu->flags |=  D_DISABLED;
     return D_O_K;     
   } 
 nf = abs(ois->im.n_f); 
 if (nf <= 0)     
   {
     active_menu->flags |=  D_DISABLED;
     return D_O_K;
   }
 else   active_menu->flags &= ~D_DISABLED;

 if(updating_menu_state != 0)    return D_O_K;
 i = ois->im.c_f;
 if (ois->im.data_type == IS_COMPLEX_IMAGE
     || ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)  
   {
     win_printf("I do not handle complexe images ! yet");
     return D_O_K;
   }
 nx = ois->im.nx;
 ny = ois->im.ny;

 if (key[KEY_LSHIFT])
   {
     return win_printf_OK("This routine create an image of two-phases frames' difference over a subset of images");
   }
 if ((oid = create_and_attach_oi_to_imr(imr, nx, ny, IS_FLOAT_IMAGE)) == NULL)
         return win_printf_OK("cannot create image!");  

 
 

 z = (float *)calloc(nx,sizeof(float));
 if (oid == NULL || z == NULL)        return win_printf_OK("Can't create dest movie");
 win_printf ("nf %d", nf);
 
 
 pd0 = ois->im.pxl[0];
 pd1 = ois->im.pxl[1];
 for (i = 0, pd = oid->im.pixel; i < ny ; i++)
       {
         for (j=0; j < nx; j++)  pd[i].fl[j] = pd0[i].fl[j] - pd1[i].fl[j];      
       }
 
 inherit_from_im_to_im(oid,ois);
 uns_oi_2_oi(oid,ois);
 oid->im.win_flag = ois->im.win_flag;
 if (ois->title != NULL)    set_im_title(oid, "%s ", ois->title);
 set_formated_string(&oid->im.treatement,"Avg movie of %s ", ois->filename);
 find_zmin_zmax(oid);
 free(z);
 return (refresh_image(imr, imr->n_oi - 1));
}
int do_movie_avg_ood_and_even_images_4phase(void)
{
 O_i *ois, *oid;
 imreg *imr;
 int nx, ny, nf, i, nf2, im0, im1, im2, im3, k, j;
 float *z;
 union pix *pd;

 if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
   {
     active_menu->flags |=  D_DISABLED;
     return D_O_K;     
   } 
 nf = abs(ois->im.n_f); 
 if (nf <= 0)     
   {
     active_menu->flags |=  D_DISABLED;
     return D_O_K;
   }
 else   active_menu->flags &= ~D_DISABLED;

 if(updating_menu_state != 0)    return D_O_K;
 i = ois->im.c_f;
 if (ois->im.data_type == IS_COMPLEX_IMAGE
     || ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)  
   {
     win_printf("I do not handle complexe images ! yet");
     return D_O_K;
   }
 nx = ois->im.nx;
 ny = ois->im.ny;

 if (key[KEY_LSHIFT])
   {
     return win_printf_OK("This routine average a movie over a subset of images");
   }


 nf2 = 4;
 // 4 phases with 1 frame for each phase//
 oid = create_and_attach_movie_to_imr(imr, nx, ny, IS_FLOAT_IMAGE, nf2);

 z = (float *)calloc(nx,sizeof(float));

 if (oid == NULL || z == NULL )      return win_printf_OK("Can't create dest movie");
 win_printf ("nf %d", nf);
 for (im0 = 0; im0 < nf; im0+=4)
   { 
     switch_frame(ois,im0);
     for (i = 0, pd = oid->im.pxl[0]; i < ny ; i++)
   {
     extract_raw_line (ois, i, z);
     for (j=0; j < nx; j++) pd[i].fl[j] += z[j];
   }
   
     im1 = im0+1;
     switch_frame(ois,im1);
     for (i = 0, pd = oid->im.pxl[1]; i < ny ; i++)
   {
     extract_raw_line (ois, i, z);
     for (j=0; j < nx; j++) pd[i].fl[j] += z[j];
   }
   
     im2 = im1+2;
     switch_frame(ois,im2);
     for (i = 0, pd = oid->im.pxl[2]; i < ny ; i++)
   {
     extract_raw_line (ois, i, z);
     for (j=0; j < nx; j++) pd[i].fl[j] += z[j];
   }
   
     im3 = im0+3;
     switch_frame(ois,im3);
     for (i = 0, pd = oid->im.pxl[3]; i < ny ; i++)
   {
     extract_raw_line (ois, i, z);
     for (j=0; j < nx; j++) pd[i].fl[j] += z[j];
   } 
   }

 
 k = nf/4;
 for (i = 0, pd = oid->im.pxl[0]; i < ny ; i++)
       {
         for (j=0; j < nx; j++)  pd[i].fl[j] /= k;      
       }
 for (i = 0, pd = oid->im.pxl[1]; i < ny ; i++)
       {
         for (j=0; j < nx; j++) pd[i].fl[j] /= k;
       }
 for (i = 0, pd = oid->im.pxl[2]; i < ny ; i++)
       {
         for (j=0; j < nx; j++)  pd[i].fl[j] /= k;      
       }
 for (i = 0, pd = oid->im.pxl[3]; i < ny ; i++)
       {
         for (j=0; j < nx; j++) pd[i].fl[j] /= k;
       }

 inherit_from_im_to_im(oid,ois);
 uns_oi_2_oi(oid,ois);
 oid->im.win_flag = ois->im.win_flag;
 if (ois->title != NULL)    set_im_title(oid, "%s ", ois->title);
 set_formated_string(&oid->im.treatement,"Avg movie of %s ", ois->filename);
 find_zmin_zmax(oid);
 free(z);
 
 return (refresh_image(imr, imr->n_oi - 1));
}

int do_substract_imagefrom2(void)   
{
  imreg *imr; 
  O_i *ois1, *ois2, *oid;
  union pix *pd1, *pd2, *pd; 
  int i, j, nx, ny;
  
  
    if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
     {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;	
     }
 	                
    if(updating_menu_state != 0)	return D_O_K;                
    
    if (key[KEY_LSHIFT]) return win_printf_OK("This routine create a new image by substracting two");
    
         i = imr->cur_oi-1;
         i = (i < 0) ? i+ imr->n_oi : i;
         ois1 = imr -> o_i[i];
         ois2 = imr -> o_i[imr->cur_oi];
    nx = ois2->im.nx;
    ny = ois2->im.ny;
    nx = ois1->im.nx;
    ny = ois1->im.ny; 
    
    if ((oid = create_and_attach_oi_to_imr(imr, nx,  ny, IS_FLOAT_IMAGE)) == NULL)
         return win_printf_OK("cannot create image!");   
    pd = oid->im.pixel;    
    pd1 = ois1->im.pixel;                                            
    pd2 = ois2->im.pixel;
    
    for (i=0; i < ny; i++)  
      {  
	for (j=0; j < nx; j++) 
	  {
	    pd[i].fl[j] = pd2[i].fl[j] - pd1[i].fl[j] ;
	  }
      }     
    inherit_from_im_to_im(oid,ois2);
    set_formated_string(&oid->im.treatement,"radial profile %s", ois2->filename);
    find_zmin_zmax(oid);
    return (refresh_image(imr, imr->n_oi - 1));     
}

int do_movie_zprofile_xaverage(void)
{
  O_i *ois, *oid;
  imreg *imr;
  int nx, ny, nf, i, j, im;
  float *z;
  float zavg;
  union pix *pd; 

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;	
    }		
  nf = abs(ois->im.n_f);		
  if (nf <= 0)	
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;
    }
  else   active_menu->flags &= ~D_DISABLED;

  if(updating_menu_state != 0)	return D_O_K;
  i = ois->im.c_f;
  if (ois->im.data_type == IS_COMPLEX_IMAGE 
      || ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)	
    {
      win_printf("I do not handle complexe images ! yet");
      return D_O_K;
    }
  nx = ois->im.nx;
  ny = ois->im.ny;

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine average a movie in one coordinate and build a profile in 1D fonction of time");
    }

  oid = create_and_attach_oi_to_imr(imr, ny, nf, IS_FLOAT_IMAGE);

  z = (float *)calloc(nx,sizeof(float));
  
  if (oid == NULL || z == NULL)      
    return win_printf_OK("Can't create dest movie");
    
  for (im = 0, pd = oid->im.pixel; im < nf; im++)
    {
      switch_frame(ois,im); 
      for (i = 0, pd = oid->im.pixel; i < ny ; i++)
    { 
      extract_raw_line (ois, i, z);
      for (j = 0; j < nx; j++)
         {
             zavg += z[j];
             if (j == nx-1) zavg /= nx; 
         }   
      pd[im].fl[i] = zavg;
    }           
    }
  inherit_from_im_to_im(oid,ois);
  uns_oi_2_oi(oid,ois);
  oid->im.win_flag = ois->im.win_flag;
  if (ois->title != NULL)	set_im_title(oid, "%s ", ois->title);
  set_formated_string(&oid->im.treatement,"Profile fonction of time of %s ", ois->filename);
  find_zmin_zmax(oid);
  free(z);
  return (refresh_image(imr, imr->n_oi - 1));        
}

int do_movie_avg_get_one_mode(void)
{
  O_i *ois, *oid;
  imreg *imr;
  int nx, ny, nf, i, im, j;
  static int k_mode = 16;
  float *z, *co, *si, rel, ima; 
  union pix *pd; 

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;	
    }		
  nf = abs(ois->im.n_f);		
  if (nf <= 0)	
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;
    }
  else   active_menu->flags &= ~D_DISABLED;

  if(updating_menu_state != 0)	return D_O_K;
  i = ois->im.c_f;
  if (ois->im.data_type == IS_COMPLEX_IMAGE 
      || ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)	
    {
      win_printf("I do not handle complexe images ! yet");
      return D_O_K;
    }
  nx = ois->im.nx;
  ny = ois->im.ny;

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine average a movie by grabbing one fourier mode and drop it in a complex image");
    }


  i = win_scanf("define the mode number that you want to grab %d",&k_mode);
  if (i == WIN_CANCEL)	return D_O_K;

  oid = create_and_attach_oi_to_imr(imr, nx, ny, IS_COMPLEX_IMAGE);

  z = (float *)calloc(nf,sizeof(float));
  co = (float *)calloc(nf,sizeof(float));
  si = (float *)calloc(nf,sizeof(float));

  if (oid == NULL || z == NULL || co == NULL || si == NULL)      
    return win_printf_OK("Can't create dest movie");


  for (im = 0; im < nf; im++)
    {
      co[im] = cos((M_PI*2*im*k_mode)/nf);
      si[im] = sin((M_PI*2*im*k_mode)/nf);
    }



  for (j = 0, pd = oid->im.pixel; j < nx; j++)
    {
      for (i = 0; i < ny ; i++)
	{
	  z = extract_z_profile_from_movie(ois, j, i, z);
	  for (im = 0, rel = ima = 0; im < nf; im++)
	    {
	      rel += z[im] * co[im];
	      ima += z[im] * si[im];
	    }
	  pd[i].fl[2*j] = (rel*2)/nf;
	  pd[i].fl[2*j+1] = (ima*2)/nf;
	  extract_raw_line (ois, i, z);
	}
    }

  inherit_from_im_to_im(oid,ois);
  uns_oi_2_oi(oid,ois);
  oid->im.win_flag = ois->im.win_flag;
  if (ois->title != NULL)	set_im_title(oid, "%s ", ois->title);
  set_formated_string(&oid->im.treatement,"Avg movie on mode %d ", k_mode);
  find_zmin_zmax(oid);
  free(z);
  free(co);
  free(si);
  return (refresh_image(imr, imr->n_oi - 1));
}


int do_movie_avg_get_one_mode_bis(void)
{
  O_i *ois, *oid;
  imreg *imr;
  d_s   *dsi;
  int nx, ny, nf, i, im, j;
  static int k_mode = 16;
  union pix *pd, *ps; 
  float *z;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;	
    }		
  nf = abs(ois->im.n_f);		
  if (nf <= 0)	
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;
    }
  else   active_menu->flags &= ~D_DISABLED;

  if(updating_menu_state != 0)	return D_O_K;
  i = ois->im.c_f;
  if (ois->im.data_type == IS_COMPLEX_IMAGE 
      || ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)	
    {
      win_printf("I do not handle complexe images ! yet");
      return D_O_K;
    }
  nx = ois->im.nx;
  ny = ois->im.ny;

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine average a movie by grabbing one fourier mode and drop it in a complex image");
    }
  i = win_scanf("define the mode number that you want to grab %d",&k_mode);
  if (i == WIN_CANCEL)	return D_O_K;

  if ((oid = create_and_attach_oi_to_imr(imr, nx, ny, IS_COMPLEX_IMAGE)== NULL))      
    return win_printf_OK("Can't create movie");


  for (j = 0, pd = oid->im.pixel; j < nx; j++)
    {
      for (i = 0; i < ny ; i++)
	{    
         z = extract_z_profile_from_movie(ois, j, i, z);
	     if (fft_init(nf)) return win_printf("Cannot init fft");
	     realtr1(nf, z);
	     fft(nf, z, 1);
	     realtr2(nf, z, 1);
	     pd[i].fl[2*j] = z[2*k_mode];
	     pd[i].fl[2*j+1] = z[2*k_mode+1];
	}
    }
  /*for (j = 0, pd = oid->im.pixel; j < nx; j++)
    {
      for (i = 0; i < ny ; i++)
	{    
	     for (im = 0, ; im < nf; im++)
         { 
             switch_frame(ois,im);
             ps = ois->im.pixel;
             dsi->yd = ps[i].fl[j];
         } 
	     if (fft_init(nf)) return win_printf("Cannot init fft");
	     realtr1(nf, dsi->yd);
	     fft(nf, dsi->yd, 1);
	     realtr2(nf, dsi->yd, 1);
	     pd[i].fl[2*j] = dsi->yd[2*k_mode];
	     pd[i].fl[2*j+1] = dsi->yd->[2*k_mode+1];
	}
    }
  */
  inherit_from_im_to_im(oid,ois);
  uns_oi_2_oi(oid,ois);
  oid->im.win_flag = ois->im.win_flag;
  if (ois->title != NULL)	set_im_title(oid, "%s ", ois->title);
  set_formated_string(&oid->im.treatement,"Avg movie on mode %d ", k_mode);
  find_zmin_zmax(oid);
  
  return (refresh_image(imr, imr->n_oi - 1));
}

int do_movie_maxima_fortimeradialprofile(void)

{
  O_i *ois;
  imreg *imr;
  int nx, ny, i, j, l , k, m, n, u, v;
  float  diffd, diffu, diffr, diffl, diffru, diffrd, difflu, diffld, q, det, nymax, nxmax, delta1, delta2, a, b, c, d, e, f;
  static float  grd=100;
  static int  nps=0, imin=1, imax=200, jmin=1, jmax=200;
  float x=0, y=0;
  union pix *pd; 
  
  O_p *opn = NULL;
  d_s *ds;
  
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;	
    }		

  if(updating_menu_state != 0)	return D_O_K;
  if (ois->im.data_type == IS_COMPLEX_IMAGE 
      || ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)	
    {
      win_printf("I do not handle complexe images ! yet");
      return D_O_K;
    }
  nx = ois->im.nx;
  ny = ois->im.ny;

  if (key[KEY_LSHIFT]) return win_printf_OK("This routine add and point maxima on image, fits maxima on each NP image and gives fit properties");
  
  i = win_scanf("What threshold value do you want %8f\n"
                "Choose j-pixel for start %d and j-pixel for finish %d\n"
                "Choose i-pixel for start %d and i-pixel for finish %d\n"
                , &grd, &jmin, &jmax, &imin, &imax);
  if (i == WIN_CANCEL)	return D_O_K;
  
   if ((opn = create_and_attach_op_to_oi (ois, 16, 16, 0,IM_SAME_X_AXIS | IM_SAME_Y_AXIS)) == NULL)
		return win_printf_OK("cannot create plot!");

  ds = opn->dat[0]; 
  
  set_ds_point_symbol(ds, "\\oc");
  set_ds_dot_line(ds);

  ds->nx = ds->ny = 0; 	
   
  for (i=imin, pd = ois->im.pixel, delta1 = 0, delta2 = 0, nps=0; i < imax; i++)
    {
      for (j=jmin; j< jmax; j++)
       	{
          if  (pd[i].fl[j] > grd) 
            { 
              diffr= pd[i].fl[j] - pd[i].fl[j+1]; 
              diffl= pd[i].fl[j] - pd[i].fl[j-1];
              diffu= pd[i].fl[j] - pd[i+1].fl[j];
              diffd= pd[i].fl[j] - pd[i-1].fl[j];
              diffru= pd[i].fl[j] - pd[i+1].fl[j+1]; 
              diffrd= pd[i].fl[j] - pd[i-1].fl[j+1];
              difflu= pd[i].fl[j] - pd[i+1].fl[j-1];
              diffld= pd[i].fl[j] - pd[i-1].fl[j-1];
              
              if (diffr*diffl > 0 && diffu*diffd > 0 && diffru*diffld > 0  && difflu*diffrd > 0 )
                {   
                    nps+= 1;
                    for (l = i-1, a=b=c=d=e=f=0; l < i+2; l++)
                       {
                           for (k = j-1; k < j+2; k++)
                              {
                               x=(float)(k-j);
                               y=(float)(l-i);
                               a += (float)1/9 * pd[l].fl[k] + (float)4/9 * pd[l].fl[k] - (float)1/3 * ( x*x * pd[l].fl[k] + y*y * pd[l].fl[k] );
                               b +=(float) 1/6 * x * pd[l].fl[k];
                               c += (float)-1/3 * pd[l].fl[k] + (float)1/2 * x*x * pd[l].fl[k];
                               d += (float)1/6 * y * pd[l].fl[k];
                               e += (float)-1/3 * pd[l].fl[k] + (float)1/2 * y*y * pd[l].fl[k];
                               f += (float)1/4 * x*y * pd[l].fl[k];  
                              }
                       }    
                    det= e*c*4 - f*f;
                    
                    if ( c > 0 || e > 0) return win_printf_OK("i shut john wayne");
                    if ( det == 0 ) return win_printf_OK("problem with max");
                           
                    nxmax = j - (-f*d+(float)2*e*b)/det;
                    nymax = i - (-c*d*2+f*b)/det;
                    delta1= fabs ((a-pd[i].fl[j])/pd[i].fl[j]);
                    
                    for ( m = i-1; m < i+2; m++)
                       { 
                          for ( n = j-1; n < j+2; n++)  
                             { 
                               u=n-j;
                               v=m-i;  
                               q = a + b*u + c*u*u + d*v + e*v*v + f*u*v; 
                               delta2 += fabs( (pd[m].fl[n] - q)/pd[m].fl[n]);
                               delta2 /= 9;
                             }
                       }  
                    win_printf("NPs number %d\n"
                               "valeur de i %d\n"
                               "valeur de j %d\n"
                               "valeur xmaxcorrig� %8f\n"
                               "valeur ymaxcorrig� %8f\n"
                               "relativ error on max %8f\n"
                               "relativ global error %8f\n" 
                               , nps, i, j, nxmax, nymax, delta1, delta2);     
                     
                     add_new_point_to_ds(ds, (float)nxmax, (float)nymax);  
                      
                } 
            }   
        } 
    }  

return (refresh_image(imr, imr->n_oi - 1));
}   

int do_movie_avg_time_radialprofile(void)
{
  O_i *ois, *oid;
  imreg *imr;
  int  im, i, j;
  union pix *pd; 
  static float ax=1, ay=1, xc = 32, yc = 32 /*dfi=1*/; //dark-field intensity -> dfi   ; 
  static int r_max=30, nf=1024; 
  
   
  if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
  {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;	
  }
  
  ois = imr -> o_i[imr->cur_oi];		
  nf = abs(ois->im.n_f);		
  if (nf <= 0)	
  {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;
  }
  else   active_menu->flags &= ~D_DISABLED;

  if(updating_menu_state != 0)	return D_O_K;
  
  if (ois->im.data_type == IS_COMPLEX_IMAGE 
      || ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)	
  {
      win_printf("I do not handle complexe images ! yet");
      return D_O_K;
  }
  if(updating_menu_state != 0)	return D_O_K;
  
  if (key[KEY_LSHIFT]) return win_printf_OK("This routine create a time-radial profil");
  
   i = win_scanf( "give a maximum radius %d \n"
                  "x-center coordinate %8f and y-center coordinate %8f\n"
                  //give the dark-field intensity collected by camera %8f \n
                  , &r_max, &xc, &yc/*, &dfi*/);
  if (i == WIN_CANCEL) return D_O_K;  
     
  if ((oid = create_and_attach_oi_to_imr(imr, 2*r_max, nf, IS_FLOAT_IMAGE)) == NULL)
       return win_printf_OK("cannot create image!");   
  pd = oid->im.pixel;
  
  for (im=0; im < nf; im++)
  { 
      switch_frame(ois,im);
      radial_non_pixel_square_image_sym_profile_in_array(pd[im].fl, ois, xc, yc, r_max, ax, ay);     
  }
  for (i=0; i < nf; i++)
  { 
      for (j=0; j < 2*r_max; j++)
  {
         //pd[i].fl[j] -= dfi;    
         pd[i].fl[j] /= pd[i].fl[2*r_max-1];
  }    
  }
  inherit_from_im_to_im(oid,ois);
  set_oi_horizontal_extend(oid, 1);
  set_oi_vertical_extend(oid, 1);
  set_formated_string(&oid->im.treatement,"time radial profile %s", ois->filename);
  find_zmin_zmax(oid); 
  
  return(refresh_image(imr,imr->n_oi - 1));     
}

float find_zero_of_3_points_polynome1(float y_1, float y0, float y1)
{
	double a, b, c, x = 0, delta;
	
	a = ((y_1+y1)/2) - y0;
	b = (y1-y_1)/2;
	c = y0;
	delta = b*b - 4*a*c;
	if (a == 0)
	{
		x = (b != 0) ? -c/b : 0;
	}
	else if (delta >=0)
	{
		delta = sqrt(delta);
		x = (fabs(-b + delta) < fabs(-b - delta)) ? -b + delta : -b - delta ;
		x /= 2*a; 
	}
	return (float)x;
}

int get_complex_x_zero_phase_lines(void)
{
	register int i, j, k;
	O_i *ois = NULL;
	O_p *op = NULL;
	d_s *ds = NULL;
	imreg *imr = NULL;
	union pix *ps;     /* data des images (matrice) 	       */
	static int n_ref = 0, pok;
	static float ampratio = 0.05;
	float phiref = 0, reref = 0, imref = 0, re, im, phin, *amp, ampmax, *l1, *l2, tmp;
	int n_line = 0, l = 0;

	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	{
		active_menu->flags |=  D_DISABLED;
		return D_O_K;	
	}	
	if(updating_menu_state != 0)
	  {
	    if (ois->im.data_type != IS_COMPLEX_IMAGE)	
	      active_menu->flags |=  D_DISABLED;
	    else  active_menu->flags &=  ~D_DISABLED;
	    return D_O_K;
	  }
	i = win_scanf("This routine extract the phase of a complex image\n"
		      "along X provide le signal amplitude is strong enough\n"
		      "specify the amplitude ratio below which phase is not computed \n"
		      "%f",&ampratio);
	if (i == WIN_CANCEL) return D_O_K;

	n_ref = 0;
	amp = (float*) calloc(ois->im.nx,sizeof(float));
	l1 = (float*) calloc(ois->im.nx,sizeof(float));
	l2 = (float*) calloc(ois->im.nx,sizeof(float));
	if (l1 == NULL || l2 == NULL || amp == NULL)	return win_printf_OK("cannot create phase image !");
	ps  = ois->im.pixel;		

	// we look for average amplitude 
	for (i = 0; i < ois->im.ny; i++)
	  {
	    for (j = 0; j < ois->im.nx; j++)
	      {
		re = ps[i].fl[2*j];
		im = ps[i].fl[2*j+1];
		amp[j] += re * re + im * im;
	      }
	  }
	// we find the max average amplitude 
	for (j = 0, ampmax = 0; j < ois->im.nx; j++)
	  ampmax = (amp[j] > ampmax) ? amp[j] : ampmax;
	//win_printf("Ampmax %g",ampmax);

	// we compute phase of the first line if amplitude is large enough
	i = 0;
	for (j = 0, pok = -1; j < ois->im.nx; j++)
	  {
	    if (amp[j] > ampmax*ampratio)
	      {
		if (pok == -1)   // first point with significant amplitude: ref phase
		  {
		    pok = 1;
		    reref = re = ps[0].fl[2*j];
		    imref = im = ps[0].fl[2*j+1];
		    phiref = l1[j] = (im != 0 && re != 0) ? atan2(im,re) : 0;
		  }
		else
		  {
		    re = ps[0].fl[2*j];
		    im = ps[0].fl[2*j+1];
		    l1[j] = (im != 0 && re != 0) ? atan2(im,re) : 0; // phase between -pi and pi
		    // we find right phase with low accuracy
		    phin = phiref + atan2(reref * im - imref * re, reref * re + imref * im);
		    // we adjust accurate phase to be exact
		    for (;phin - l1[j] > M_PI_2; l1[j] += M_PI);  
		    for (;l1[j] - phin > M_PI_2; l1[j] -= M_PI);  
		    // we keep last good point
		    phiref = l1[j];
		    reref = re;
		    imref = im;
		  }
	      }
	    else 
	      {
		l1[j] = -M_PI * 2;
		pok = (pok < 0) ? pok : 0;
	      }                                                                                                                
	  }



	n_line = (int)(phiref/(M_PI));// we compute the number oof data set of phase lines

	win_printf("n lines %d",n_line);

	for (i = 0; i < n_line; i++)
	  {  // allocation of the phase lines
	    if (i == 0)
	      {
		op = create_and_attach_op_to_oi(ois, ois->im.ny, ois->im.ny, 0, IM_SAME_X_AXIS | IM_SAME_Y_AXIS);
		ds = op->dat[0];
	      }
	    else
	      ds = create_and_attach_one_ds(op, ois->im.ny, ois->im.ny, 0);
	    //ds = create_and_attach_ds_to_oi(ois, ois->im.ny, ois->im.ny, 0);

	    inherit_from_im_to_ds(ds, ois);	
	    set_formated_string(&ds->treatement,"Phase line %d \\pi",i);

	    // we record the first points of each phase lines
	    for (j = 1, k = 0, tmp =  M_PI; j < ois->im.nx-1; j++)
	      {  // we find the point index of phase line
		if (l1[j-1] > - M_PI && l1[j] > - M_PI && l1[j+1] > - M_PI)
		  {
		    if (fabs (((l1[j-1] + l1[j] + l1[j+1])/3) -  i * M_PI) < tmp)
		      {
			tmp = fabs (((l1[j-1] + l1[j] + l1[j+1])/3) - i * M_PI);
			k = j;
		      }
		  }
	      }
	    //win_printf("phase %d found at %d",i,k); 
	    ds = op->dat[i];
	    if (k > 0)
	      {  // we extrapolate the phase line position
		ds->yd[0] = 0;
		ds->xd[0] = find_zero_of_3_points_polynome1(l1[k-1]-  i * M_PI, 
							    l1[k]-  i * M_PI, l1[k+1]- i * M_PI) + k;
		//ds->xd[0] = k;
	      }
	    else
	      {
		ds->yd[0] = 0;
		ds->xd[0] = 0;
	      } 
	  }
	//win_printf("%d phase lines",n_line);
	for (i = 1; i < ois->im.ny; i++)
	  { // we scan all remaining lines of image
	    display_title_message ("Line %d",i);
	    for (j = 0; j < ois->im.nx; j++)
	      {
		if (amp[j] > ampmax*ampratio)
		  {
		    phiref = l1[j]; // the reference phase comes from previous line
		    reref = ps[i-1].fl[2*j];
		    imref = ps[i-1].fl[2*j+1];
		    // we find right phase with low accuracy
		    re = reref * ps[i].fl[2*j] + imref * ps[i].fl[2*j+1];
		    im = reref * ps[i].fl[2*j+1] - imref * ps[i].fl[2*j];
		    phin = phiref + atan2(im,re);
		    // phase between -pi and pi
		    re = ps[i].fl[2*j];
		    im = ps[i].fl[2*j+1];
		    l2[j] = (im != 0 && re != 0) ? atan2(im,re) : 0;
		    // we adjust accurate phase to be exact
		    for (;phin - l2[j] > M_PI_2; l2[j] += M_PI);  
		    for (;l2[j] - phin > M_PI_2; l2[j] -= M_PI);  
		  }
		else 
		  {
		    l2[j] = -M_PI * 2;
		  }
	      }
	    for (l = 0; l < n_line; l++)
	      {
		for (j = 1, k = 0, tmp =  M_PI; j < ois->im.nx-1; j++)
		  {   // we find the point index of phase line
		    if (l2[j-1] > - M_PI && l2[j] > - M_PI && l2[j+1] > - M_PI)
		      {
			if (fabs (((l2[j-1] + l2[j] + l2[j+1])/3) - l * M_PI) < tmp)
			  {
			    tmp = fabs (((l2[j-1] + l2[j] + l2[j+1])/3) -  l * M_PI);
			    k = j;
			  }
		      }
		  }
		ds = op->dat[l];// we extrapolate the phase line position
		if (k > 0)
		  {
		    ds->yd[i] = i;
		    ds->xd[i] = find_zero_of_3_points_polynome1(l2[k-1]-  l * M_PI, 
								l2[k]-  l* M_PI, l2[k+1]-  l * M_PI) + k;
		    //ds->xd[i] = k;
		  }
		else
		  {
		    ds->yd[i] = i;
		    ds->xd[i] = 0;
		  } 
	      }
	    for (j = 0; j < ois->im.nx; j++)
	      l1[j] = l2[j]; // we swap phase info from one line to the next
	  }
	for (i=0; i < n_line; i++)
      {       
              ds = op->dat[i];
              win_printf("position-lines %f",ds->xd[0]);
      }
    
    
     
	free(amp);	
	free(l1);
	free(l2);
	op->filename = Transfer_filename(ois->filename);	
	set_plot_title(op,"Phase lines");
	uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
	uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
	ois->cur_op = ois->n_op - 1;
	ois->need_to_refresh |= PLOT_NEED_REFRESH  | EXTRA_PLOT_NEED_REFRESH;	
	broadcast_dialog_message(MSG_DRAW,0);	
	return D_REDRAWME;		
}
int get_complex_x_zero_phase_lines_more(void)
{
	register int i, j, k;
	O_i *ois = NULL;
	O_p *op = NULL;
	d_s *ds = NULL;
	imreg *imr = NULL;
	union pix *ps;     /* data des images (matrice) 	       */
	static int n_ref = 0, pok;
	static float ampratio = 0.05;
	float phiref = 0, reref = 0, imref = 0, re, im, phin, *amp, ampmax, *l1, *l2, tmp;
	int n_line = 0, l = 0;

	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	{
		active_menu->flags |=  D_DISABLED;
		return D_O_K;	
	}	
	if(updating_menu_state != 0)
	  {
	    if (ois->im.data_type != IS_COMPLEX_IMAGE)	
	      active_menu->flags |=  D_DISABLED;
	    else  active_menu->flags &=  ~D_DISABLED;
	    return D_O_K;
	  }
	i = win_scanf("This routine extract the phase of a complex image\n"
		      "along X provide le signal amplitude is strong enough\n"
		      "specify the amplitude ratio below which phase is not computed \n"
		      "%f",&ampratio);
	if (i == WIN_CANCEL) return D_O_K;

	n_ref = 0;
	amp = (float*) calloc(ois->im.nx,sizeof(float));
	l1 = (float*) calloc(ois->im.nx,sizeof(float));
	l2 = (float*) calloc(ois->im.nx,sizeof(float));
	if (l1 == NULL || l2 == NULL || amp == NULL)	return win_printf_OK("cannot create phase image !");
	ps  = ois->im.pixel;		

	// we look for average amplitude 
	for (i = 0; i < ois->im.ny; i++)
	  {
	    for (j = 0; j < ois->im.nx; j++)
	      {
		re = ps[i].fl[2*j];
		im = ps[i].fl[2*j+1];
		amp[j] += re * re + im * im;
	      }
	  }
	// we find the max average amplitude 
	for (j = 0, ampmax = 0; j < ois->im.nx; j++)
	  ampmax = (amp[j] > ampmax) ? amp[j] : ampmax;
	//win_printf("Ampmax %g",ampmax);

	// we compute phase of the first line if amplitude is large enough
	i = 0;
	for (j = 0, pok = -1; j < ois->im.nx; j++)
	  {
	    if (amp[j] > ampmax*ampratio)
	      {
		if (pok == -1)   // first point with significant amplitude: ref phase
		  {
		    pok = 1;
		    reref = re = ps[0].fl[2*j];
		    imref = im = ps[0].fl[2*j+1];
		    phiref = l1[j] = (im != 0 && re != 0) ? atan2(im,re) : 0;
		  }
		else
		  {
		    re = ps[0].fl[2*j];
		    im = ps[0].fl[2*j+1];
		    l1[j] = (im != 0 && re != 0) ? atan2(im,re) : 0; // phase between -pi and pi
		    // we find right phase with low accuracy
		    phin = phiref + atan2(reref * im - imref * re, reref * re + imref * im);
		    // we adjust accurate phase to be exact
		    for (;phin - l1[j] > M_PI_2; l1[j] += M_PI);  
		    for (;l1[j] - phin > M_PI_2; l1[j] -= M_PI);  
		    // we keep last good point
		    phiref = l1[j];
		    reref = re;
		    imref = im;
		  }
	      }
	    else 
	      {
		l1[j] = -M_PI * 2;
		pok = (pok < 0) ? pok : 0;
	      }                                                                                                                
	  }



	n_line = (int)(phiref/(M_PI));// we compute the number oof data set of phase lines

	win_printf("n lines %d",n_line);

	for (i = 0; i < n_line; i++)
	  {  // allocation of the phase lines
	    if (i == 0)
	      {
		op = create_and_attach_op_to_oi(ois, ois->im.ny, ois->im.ny, 0, IM_SAME_X_AXIS | IM_SAME_Y_AXIS);
		ds = op->dat[0];
	      }
	    else
	      ds = create_and_attach_one_ds(op, ois->im.ny, ois->im.ny, 0);
	    //ds = create_and_attach_ds_to_oi(ois, ois->im.ny, ois->im.ny, 0);

	    inherit_from_im_to_ds(ds, ois);	
	    set_formated_string(&ds->treatement,"Phase line %d \\pi",i);

	    // we record the first points of each phase lines
	    for (j = 1, k = 0, tmp =  M_PI/2; j < ois->im.nx-1; j++)
	      {  // we find the point index of phase line
		if (l1[j-1] > - 2*M_PI && l1[j] > - 2*M_PI && l1[j+1] > - 2*M_PI)
		  {
		    if (fabs (((l1[j-1] + l1[j] + l1[j+1])/3) -  i * M_PI/2) < tmp)
		      {
			tmp = fabs (((l1[j-1] + l1[j] + l1[j+1])/3) - i * M_PI/2);
			k = j;
		      }
		  }
	      }
	    //win_printf("phase %d found at %d",i,k); 
	    ds = op->dat[i];
	    if (k > 0)
	      {  // we extrapolate the phase line position
		ds->yd[0] = 0;
		ds->xd[0] = find_zero_of_3_points_polynome1(l1[k-1]-  i * M_PI/2, 
							    l1[k]-  i * M_PI/2, l1[k+1]- i * M_PI/2) + k;
		//ds->xd[0] = k;
	      }
	    else
	      {
		ds->yd[0] = 0;
		ds->xd[0] = 0;
	      } 
	  }
	//win_printf("%d phase lines",n_line);
	for (i = 1; i < ois->im.ny; i++)
	  { // we scan all remaining lines of image
	    display_title_message ("Line %d",i);
	    for (j = 0; j < ois->im.nx; j++)
	      {
		if (amp[j] > ampmax*ampratio)
		  {
		    phiref = l1[j]; // the reference phase comes from previous line
		    reref = ps[i-1].fl[2*j];
		    imref = ps[i-1].fl[2*j+1];
		    // we find right phase with low accuracy
		    re = reref * ps[i].fl[2*j] + imref * ps[i].fl[2*j+1];
		    im = reref * ps[i].fl[2*j+1] - imref * ps[i].fl[2*j];
		    phin = phiref + atan2(im,re);
		    // phase between -pi and pi
		    re = ps[i].fl[2*j];
		    im = ps[i].fl[2*j+1];
		    l2[j] = (im != 0 && re != 0) ? atan2(im,re) : 0;
		    // we adjust accurate phase to be exact
		    for (;phin - l2[j] > M_PI_2; l2[j] += M_PI);  
		    for (;l2[j] - phin > M_PI_2; l2[j] -= M_PI);  
		  }
		else 
		  {
		    l2[j] = -M_PI * 2;
		  }
	      }
	    for (l = 0; l < n_line; l++)
	      {
		for (j = 1, k = 0, tmp =  M_PI/2; j < ois->im.nx-1; j++)
		  {   // we find the point index of phase line
		    if (l2[j-1] > - 2*M_PI && l2[j] > - 2*M_PI && l2[j+1] > - 2*M_PI)
		      {
			if (fabs (((l2[j-1] + l2[j] + l2[j+1])/3) - l * M_PI/2) < tmp)
			  {
			    tmp = fabs (((l2[j-1] + l2[j] + l2[j+1])/3) -  l * M_PI/2);
			    k = j;
			  }
		      }
		  }
		ds = op->dat[l];// we extrapolate the phase line position
		if (k > 0)
		  {
		    ds->yd[i] = i;
		    ds->xd[i] = find_zero_of_3_points_polynome1(l2[k-1]-  l * M_PI/2, 
								l2[k]-  l* M_PI/2, l2[k+1]-  l * M_PI/2) + k;
		    //ds->xd[i] = k;
		  }
		else
		  {
		    ds->yd[i] = i;
		    ds->xd[i] = 0;
		  } 
	      }
	    for (j = 0; j < ois->im.nx; j++)
	      l1[j] = l2[j]; // we swap phase info from one line to the next
	  }
	for (i=0; i < n_line; i++)
      {       
              ds = op->dat[i];
              //win_printf("position-lines %f",ds->xd[0]);
      }
	free(amp);	
	free(l1);
	free(l2);
	op->filename = Transfer_filename(ois->filename);	
	set_plot_title(op,"Phase lines");
	uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
	uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
	ois->cur_op = ois->n_op - 1;
	ois->need_to_refresh |= PLOT_NEED_REFRESH  | EXTRA_PLOT_NEED_REFRESH;	
	broadcast_dialog_message(MSG_DRAW,0);	
	return D_REDRAWME;		
}

int get_complex_x_zero_phase_lines_less_more(void)
{
	register int i, j, k;
	O_i *ois = NULL;
	O_p *op = NULL;
	d_s *ds = NULL;
	imreg *imr = NULL;
	union pix *ps;     /* data des images (matrice) 	       */
	static int n_ref = 0, pok;
	static float ampratio = 0.05;
	float phiref = 0, reref = 0, imref = 0, re, im, phin, *amp, ampmax, *l1, *l2, tmp;
	int n_line = 0, l = 0;

	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	{
		active_menu->flags |=  D_DISABLED;
		return D_O_K;	
	}	
	if(updating_menu_state != 0)
	  {
	    if (ois->im.data_type != IS_COMPLEX_IMAGE)	
	      active_menu->flags |=  D_DISABLED;
	    else  active_menu->flags &=  ~D_DISABLED;
	    return D_O_K;
	  }
	i = win_scanf("This routine extract the phase of a complex image\n"
		      "along X provide le signal amplitude is strong enough\n"
		      "specify the amplitude ratio below which phase is not computed \n"
		      "%f",&ampratio);
	if (i == WIN_CANCEL) return D_O_K;

	n_ref = 0;
	amp = (float*) calloc(ois->im.nx,sizeof(float));
	l1 = (float*) calloc(ois->im.nx,sizeof(float));
	l2 = (float*) calloc(ois->im.nx,sizeof(float));
	if (l1 == NULL || l2 == NULL || amp == NULL)	return win_printf_OK("cannot create phase image !");
	ps  = ois->im.pixel;		

	// we look for average amplitude 
	for (i = 0; i < ois->im.ny; i++)
	  {
	    for (j = 0; j < ois->im.nx; j++)
	      {
		re = ps[i].fl[2*j];
		im = ps[i].fl[2*j+1];
		amp[j] += re * re + im * im;
	      }
	  }
	// we find the max average amplitude 
	for (j = 0, ampmax = 0; j < ois->im.nx; j++)
	  ampmax = (amp[j] > ampmax) ? amp[j] : ampmax;
	//win_printf("Ampmax %g",ampmax);

	// we compute phase of the first line if amplitude is large enough
	i = 0;
	for (j = 0, pok = -1; j < ois->im.nx; j++)
	  {
	    if (amp[j] > ampmax*ampratio)
	      {
		if (pok == -1)   // first point with significant amplitude: ref phase
		  {
		    pok = 1;
		    reref = re = ps[0].fl[2*j];
		    imref = im = ps[0].fl[2*j+1];
		    phiref = l1[j] = (im != 0 && re != 0) ? atan2(im,re) : 0;
		  }
		else
		  {
		    re = ps[0].fl[2*j];
		    im = ps[0].fl[2*j+1];
		    l1[j] = (im != 0 && re != 0) ? atan2(im,re) : 0; // phase between -pi and pi
		    // we find right phase with low accuracy
		    phin = phiref + atan2(reref * im - imref * re, reref * re + imref * im);
		    // we adjust accurate phase to be exact
		    for (;phin - l1[j] > M_PI_2; l1[j] += M_PI);  
		    for (;l1[j] - phin > M_PI_2; l1[j] -= M_PI);  
		    // we keep last good point
		    phiref = l1[j];
		    reref = re;
		    imref = im;
		  }
	      }
	    else 
	      {
		l1[j] = -M_PI * 2;
		pok = (pok < 0) ? pok : 0;
	      }                                                                                                                
	  }



	n_line = (int)(phiref/(M_PI));// we compute the number oof data set of phase lines

	win_printf("n lines %d",n_line);

	for (i = 0; i < n_line; i++)
	  {  // allocation of the phase lines
	    if (i == 0)
	      {
		op = create_and_attach_op_to_oi(ois, ois->im.ny, ois->im.ny, 0, IM_SAME_X_AXIS | IM_SAME_Y_AXIS);
		ds = op->dat[0];
	      }
	    else
	      ds = create_and_attach_one_ds(op, ois->im.ny, ois->im.ny, 0);
	    //ds = create_and_attach_ds_to_oi(ois, ois->im.ny, ois->im.ny, 0);

	    inherit_from_im_to_ds(ds, ois);	
	    set_formated_string(&ds->treatement,"Phase line %d \\pi",i);

	    // we record the first points of each phase lines
	    for (j = 1, k = 0, tmp =  M_PI/2; j < ois->im.nx-1; j++)
	      {  // we find the point index of phase line
		if (l1[j-1] > - 2*M_PI && l1[j] > - 2*M_PI && l1[j+1] > - 2*M_PI)
		  {
		    if (fabs (((l1[j-1] + l1[j] + l1[j+1])/3) -  i * M_PI) < tmp)
		      {
			tmp = fabs (((l1[j-1] + l1[j] + l1[j+1])/3) - i * M_PI);
			k = j;
		      }
		  }
	      }
	    //win_printf("phase %d found at %d",i,k); 
	    ds = op->dat[i];
	    if (k > 0)
	      {  // we extrapolate the phase line position
		ds->yd[0] = 0;
		ds->xd[0] = find_zero_of_3_points_polynome1(l1[k-1]-  i * M_PI, 
							    l1[k]-  i * M_PI, l1[k+1]- i * M_PI) + k;
		//ds->xd[0] = k;
	      }
	    else
	      {
		ds->yd[0] = 0;
		ds->xd[0] = 0;
	      } 
	  }
	//win_printf("%d phase lines",n_line);
	for (i = 1; i < ois->im.ny; i++)
	  { // we scan all remaining lines of image
	    display_title_message ("Line %d",i);
	    for (j = 0; j < ois->im.nx; j++)
	      {
		if (amp[j] > ampmax*ampratio)
		  {
		    phiref = l1[j]; // the reference phase comes from previous line
		    reref = ps[i-1].fl[2*j];
		    imref = ps[i-1].fl[2*j+1];
		    // we find right phase with low accuracy
		    re = reref * ps[i].fl[2*j] + imref * ps[i].fl[2*j+1];
		    im = reref * ps[i].fl[2*j+1] - imref * ps[i].fl[2*j];
		    phin = phiref + atan2(im,re);
		    // phase between -pi and pi
		    re = ps[i].fl[2*j];
		    im = ps[i].fl[2*j+1];
		    l2[j] = (im != 0 && re != 0) ? atan2(im,re) : 0;
		    // we adjust accurate phase to be exact
		    for (;phin - l2[j] > M_PI_2; l2[j] += M_PI);  
		    for (;l2[j] - phin > M_PI_2; l2[j] -= M_PI);  
		  }
		else 
		  {
		    l2[j] = -M_PI * 2;
		  }
	      }
	    for (l = 0; l < n_line; l++)
	      {
		for (j = 1, k = 0, tmp =  M_PI/2; j < ois->im.nx-1; j++)
		  {   // we find the point index of phase line
		    if (l2[j-1] > - 2*M_PI && l2[j] > - 2*M_PI && l2[j+1] > - 2*M_PI)
		      {
			if (fabs (((l2[j-1] + l2[j] + l2[j+1])/3) - l * M_PI) < tmp)
			  {
			    tmp = fabs (((l2[j-1] + l2[j] + l2[j+1])/3) -  l * M_PI);
			    k = j;
			  }
		      }
		  }
		ds = op->dat[l];// we extrapolate the phase line position
		if (k > 0)
		  {
		    ds->yd[i] = i;
		    ds->xd[i] = find_zero_of_3_points_polynome1(l2[k-1]-  l * M_PI, 
								l2[k]-  l* M_PI, l2[k+1]-  l * M_PI) + k;
		    //ds->xd[i] = k;
		  }
		else
		  {
		    ds->yd[i] = i;
		    ds->xd[i] = 0;
		  } 
	      }
	    for (j = 0; j < ois->im.nx; j++)
	      l1[j] = l2[j]; // we swap phase info from one line to the next
	  }
	for (i=0; i < n_line; i++)
      {       
              ds = op->dat[i];
              //win_printf("position-lines %f",ds->xd[0]);
      }
	free(amp);	
	free(l1);
	free(l2);
	op->filename = Transfer_filename(ois->filename);	
	set_plot_title(op,"Phase lines");
	uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
	uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
	ois->cur_op = ois->n_op - 1;
	ois->need_to_refresh |= PLOT_NEED_REFRESH  | EXTRA_PLOT_NEED_REFRESH;	
	broadcast_dialog_message(MSG_DRAW,0);	
	return D_REDRAWME;		
}
int get_complex_x_phase(void)
{
	register int i, j, ki, kf, k;
	O_i *ois = NULL, *oid = NULL;
	imreg *imr = NULL;
	union pix *ps, *pd;     /* data des images (matrice) */
	O_p *op = NULL;
	d_s *ds = NULL;
	static int n_ref = 0, n_lines = 10,  pok, n_lines_max, r;
	static float ampratio = 0.05;
	float phiref = 0, reref = 0, imref = 0, re, im, phin, *amp, ampmax;

	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	{
		active_menu->flags |=  D_DISABLED;
		return D_O_K;	
	}	
	if(updating_menu_state != 0)
	  {
	    if (ois->im.data_type != IS_COMPLEX_IMAGE)	
	      active_menu->flags |=  D_DISABLED;
	    else  active_menu->flags &=  ~D_DISABLED;
	    return D_O_K;
	  }
	i = win_scanf("This routine extract the phase of a complex image\n"
		      "along X provide le signal amplitude is strong enough\n"
		      "specify the amplitude ratio below which phase is not computed \n"
		      "%f",&ampratio);
	if (i == WIN_CANCEL) return D_O_K;

	n_ref = 0;
	oid = create_and_attach_oi_to_imr(imr, ois->im.nx, ois->im.ny, IS_FLOAT_IMAGE);
	amp = (float*) calloc(ois->im.nx,sizeof(float));
	if (oid == NULL || amp == NULL)	return win_printf_OK("cannot create phase image !");
	oid->filename = my_sprintf(oid->filename, "Phase-%s.gr",ois->filename);
	pd  = oid->im.pixel;		
	ps  = ois->im.pixel;		

	for (i = 0; i < ois->im.ny; i++)
	  {
	    for (j = 0; j < ois->im.nx; j++)
	      {
		re = ps[i].fl[2*j];
		im = ps[i].fl[2*j+1];
		pd[i].fl[j] = (im != 0 && re != 0) ? atan2(im,re) : 0;
		amp[j] += re * re + im * im;
	      }
	  }
	for (j = 0, ampmax = 0; j < ois->im.nx; j++)
	 ampmax = (amp[j] > ampmax) ? amp[j] : ampmax;
	
	// we record the starting raw repered by ki (reference phase) and the finishing raw repered by kf
	for (j = 0, pok = 0; j < ois->im.nx; j++)
      { 
        if ( amp[j] > ampmax*ampratio  && pok == 0) 
        {
             ki = j;
             pok = 1;
        }
        if ( amp[j] < ampmax*ampratio && pok == 1) 
        {    
             kf = j;
             pok = 2;
        }
      }
    //win_printf("%d first phase-line index\n"  "%d  final phase-line index\n", ki, kf);
    
	// we compute the phase-map by starting fulling the first line i=0 corresponding to t=0s 
	pok = 0;
	i = 0;
	for (j = 0; j < ois->im.nx; j++)
	  {
	    if (amp[j] > ampmax*ampratio)
	      {
		if (pok == 0)
		  {
		    pok = 1;
		    phiref = pd[i].fl[j];
		    reref = ps[i].fl[2*j];
		    imref = ps[i].fl[2*j+1];
		  }
		else
		  {
		    re = reref * ps[i].fl[2*j] + imref * ps[i].fl[2*j+1];
		    im = reref * ps[i].fl[2*j+1] - imref * ps[i].fl[2*j];
		    phin = phiref + atan2(im,re);
		    for (;phin - pd[i].fl[j] > M_PI_2; pd[i].fl[j] += M_PI);  
		    for (;pd[i].fl[j] - phin > M_PI_2; pd[i].fl[j] -= M_PI);  
		    phiref = pd[i].fl[j];
		    reref = ps[i].fl[2*j];
		    imref = ps[i].fl[2*j+1];
		  }
	      }
	    else 
	      {
		pd[i].fl[j] = -M_PI * 2;
		pok = 0;
	      }
	  }
   
	for (j = 0; j < ois->im.nx; j++)
	  {
	    if (amp[j] > ampmax*ampratio)
	      {
		phiref = pd[0].fl[j];
		reref = ps[0].fl[2*j];
		imref = ps[0].fl[2*j+1];
		for (i = 1; i < ois->im.ny; i++)
		  {
		    re = reref * ps[i].fl[2*j] + imref * ps[i].fl[2*j+1];
		    im = reref * ps[i].fl[2*j+1] - imref * ps[i].fl[2*j];
		    phin = phiref + atan2(im,re);
		    for (;phin - pd[i].fl[j] > M_PI_2; pd[i].fl[j] += M_PI);  
		    for (;pd[i].fl[j] - phin > M_PI_2; pd[i].fl[j] -= M_PI);  
		    phiref = pd[i].fl[j];
		    reref = ps[i].fl[2*j];
		    imref = ps[i].fl[2*j+1];
		  }
	      }
	    else 
	      {
		for (i = 1; i < ois->im.ny; i++)
		  pd[i].fl[j] = -M_PI * 2;
	      }
	  }
    // we extract now the phase-lines (time variation at a fixed position for each lines)
    
    // we first take a number of lines n_lines
    n_lines_max = kf-ki;
    i = win_scanf("give the number of lines %d\n",&n_lines);
	if (i == WIN_CANCEL) return D_O_K;
    if (n_lines > n_lines_max) return win_printf_OK("chose a smaller lines number");
    
    r = (int)(n_lines_max/n_lines);
    for (i = 0; i < n_lines; i++)
	  {  // allocation of the phase lines
	    if (i == 0)
	      {
		op = create_and_attach_op_to_oi(ois, ois->im.ny, ois->im.ny, 0, IM_SAME_X_AXIS | IM_SAME_Y_AXIS);
		ds = op->dat[0];
	      }
	    else
	      ds = create_and_attach_one_ds(op, ois->im.ny, ois->im.ny, 0);
	    //ds = create_and_attach_ds_to_oi(ois, ois->im.ny, ois->im.ny, 0);

	    inherit_from_im_to_ds(ds, ois);	
	    set_formated_string(&ds->treatement,"Phase line %d \\pi",i);
        ds = op->dat[i];
	    // we draw each dataset
	    k = ki+i*r;
	    win_printf("phase-lines index\n", k);
	    for (j=0 ; j < ois->im.ny; j++)
	      { 
		ds->yd[j] = pd[j].fl[k];
		ds->xd[j] = j;
	      }
	  }

	inherit_from_im_to_im(oid,ois);		/* heritage global des proprietes de l'image 1 		*/
	uns_oi_2_oi(oid,ois);				/* heritage des unites de l'image 1 			*/
	oid->im.win_flag = ois->im.win_flag;		/* heritage des boundary condition de l'image 1 	*/
	find_zmin_zmax(oid);
	select_image_of_imreg(imr, imr->n_oi-1);
	oid->need_to_refresh = ALL_NEED_REFRESH;				
	free(amp);
	return D_REDRAWME;		
}

int do_image_periodic_avg_in_y(void)
{
  O_i *ois, *oid;
  imreg *imr;
  int nx, ny, mode_initial, i, ny2, j, data, data_out;
  static int nper = 16;
  float *z = NULL;
  union pix *pd; 

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;	
    }		
  else   active_menu->flags &= ~D_DISABLED;

  if(updating_menu_state != 0)	return D_O_K;

  nx = ois->im.nx;
  ny = ois->im.ny;

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine average a priodic image in y");
    }

  i = win_scanf("define the number of points for one period %d",&nper);
  if (i == WIN_CANCEL)	return D_O_K;

  if (nper <= 0 || nper > ny) 
          return win_printf_OK("Inproper number of points");
  ny2 = ny/nper;
  ny2 *= nper;


  data = ois->im.data_type;
  if (data == IS_CHAR_IMAGE)                   data_out = IS_FLOAT_IMAGE;
  else if (data == IS_INT_IMAGE)               data_out = IS_FLOAT_IMAGE;
  else if (data == IS_FLOAT_IMAGE)             data_out = IS_FLOAT_IMAGE;
  else if (data == IS_UINT_IMAGE)              data_out = IS_FLOAT_IMAGE;
  else if (data == IS_LINT_IMAGE)              data_out = IS_FLOAT_IMAGE;
  else if (data == IS_RGB_PICTURE)             data_out = IS_RGB16_PICTURE;
  else if (data == IS_RGB16_PICTURE)           data_out = IS_RGB16_PICTURE;
  else if (data == IS_RGBA_PICTURE)            data_out = IS_RGBA16_PICTURE;
  else if (data == IS_RGBA16_PICTURE)          data_out = IS_RGBA16_PICTURE;
  else if (data == IS_DOUBLE_IMAGE)            data_out = IS_DOUBLE_IMAGE;
  else if (data == IS_COMPLEX_IMAGE)           data_out = IS_COMPLEX_IMAGE;
  else if (data == IS_COMPLEX_DOUBLE_IMAGE)    data_out = IS_COMPLEX_DOUBLE_IMAGE;
  else return win_printf_OK("Image type not yet suupported");

  if (data_out == IS_FLOAT_IMAGE)
    z = (float *)calloc(nx,sizeof(float));
  else if (data_out == IS_COMPLEX_IMAGE)
    z = (float *)calloc(2*nx,sizeof(float));
  else return win_printf_OK("Image type not yet suupported");

  if (data_out == IS_RGB16_PICTURE || data_out == IS_RGBA16_PICTURE || 
      data_out == IS_DOUBLE_IMAGE || data_out == IS_COMPLEX_DOUBLE_IMAGE) 
    return win_printf_OK("Image type not yet suupported");

  oid = create_and_attach_oi_to_imr(imr, nx, nper, data_out);

  if (oid == NULL || z == NULL)      
    return win_printf_OK("Can't create dest movie");
  mode_initial = ois->im.mode;
  for (i = 0, pd = oid->im.pixel; i < ny2 ; i++)
    {
      if (data_out == IS_FLOAT_IMAGE)
	{
	  extract_raw_line (ois, i, z);
	  for (j=0; j< nx; j++)
	    pd[i%nper].fl[j] += z[j];
	}
      else if (data_out == IS_COMPLEX_IMAGE)
	{
	  ois->im.mode = RE;
	  extract_raw_line (ois, i, z);
	  for (j=0; j< nx; j++)
	    pd[i%nper].fl[2*j] += z[j];
	  ois->im.mode = IM;
	  extract_raw_line (ois, i, z);
	  for (j=0; j< nx; j++)
	    pd[i%nper].fl[2*j+1] += z[j];
	}
    }
  ois->im.mode = mode_initial;
  // normalizing 
  ny2 = ny/nper;
  for (i = 0, pd = oid->im.pixel; i < nper ; i++)
    {
      if (data_out == IS_FLOAT_IMAGE)
	{
	  for (j=0; j< nx; j++)
	    pd[i%nper].fl[j] /= ny2;
	}
      else if (data_out == IS_COMPLEX_IMAGE)
	{
	  for (j=0; j< 2*nx; j++)
	    pd[i%nper].fl[j]  /= ny2;
	}
    }
  inherit_from_im_to_im(oid,ois);
  uns_oi_2_oi(oid,ois);
  oid->im.win_flag = ois->im.win_flag;
  if (ois->title != NULL)	set_im_title(oid, "%s ", ois->title);
  set_formated_string(&oid->im.treatement,"Peiriodic avgeraged image in y over %d of %s ", nper, ois->filename);
  find_zmin_zmax(oid);
  free(z);
  return (refresh_image(imr, imr->n_oi - 1));
}

MENU *movie_avg_image_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"average images in time", do_movie_avg_images,NULL,0,NULL);
	add_item_to_menu(mn,"compute mean and standard deviation in time", do_movie_avg_mean_and_std,NULL,0,NULL);
	add_item_to_menu(mn,"box in movie", do_movie_to_movie,NULL,0,NULL);
	add_item_to_menu(mn,"Avg. periodic image in y",do_image_periodic_avg_in_y ,NULL,0,NULL); 
   // add_item_to_menu(mn,"gaussien noise", add_gaussien_noise,NULL,0,NULL);
    add_item_to_menu(mn,"substract result from 2 images", do_substract_imagefrom2,NULL,0,NULL);
    //add_item_to_menu(mn,"control at first", do_movie_avg_truefit,NULL,0,NULL);
    add_item_to_menu(mn,"point maxima on image", do_movie_avg_fit,NULL,0,NULL);
    add_item_to_menu(mn,"dataset of maxima", do_movie_avg_datamax,NULL,0,NULL);
    add_item_to_menu(mn,"calibration imagenew", do_movie_avg_calibration_new,NULL,0,NULL);
    add_item_to_menu(mn,"xi2 as fonction of zshiftnew correlate", do_calibration_xi2_slidding_correlate,NULL,0,NULL); 
    add_item_to_menu(mn,"xi2 as fonction of zshiftnew nocorrelate", do_calibration_xi2_slidding_nocorrelate,NULL,0,NULL); 
    add_item_to_menu(mn,"xi2 as fonction of zshiftnew correlate_substract", do_calibration_xi2_slidding_correlate_substract,NULL,0,NULL); 
    add_item_to_menu(mn,"xi2 as fonction of zshift nocorrelate_substract ", do_calibration_xi2_slidding_nocorrelate_substract,NULL,0,NULL);     
	add_item_to_menu(mn,"difference between images", do_calibration_substract_image,NULL,0,NULL); 
	add_item_to_menu(mn,"minimum of a dataset", do_find_min_dataset,NULL,0,NULL); 
	add_item_to_menu(mn,"average odd even 2 phase", do_movie_avg_ood_and_even_images_2phase,NULL,0,NULL);
	add_item_to_menu(mn,"image substract from two phases", do_movie_substractimage_2phase,NULL,0,NULL);
    add_item_to_menu(mn,"average odd even 4 phase", do_movie_avg_ood_and_even_images_4phase,NULL,0,NULL);
    add_item_to_menu(mn,"zprofile_xaverage", do_movie_zprofile_xaverage,NULL,0,NULL); 
    add_item_to_menu(mn,"TF on each pixel", do_movie_avg_get_one_mode,NULL,0,NULL); 
    add_item_to_menu(mn,"point maxima on image for time-radial profile", do_movie_maxima_fortimeradialprofile,NULL,0,NULL);
    add_item_to_menu(mn,"time-radial profile", do_movie_avg_time_radialprofile,NULL,0,NULL);
    add_item_to_menu(mn,"phase_lines", get_complex_x_zero_phase_lines,NULL,0,NULL);
    add_item_to_menu(mn,"phase_lines_less_more", get_complex_x_zero_phase_lines_less_more,NULL,0,NULL);
    add_item_to_menu(mn,"phase_lines more", get_complex_x_zero_phase_lines_more,NULL,0,NULL);  
    add_item_to_menu(mn,"phase map", get_complex_x_phase,NULL,0,NULL);   
      
    return mn;
}

int	movie_avg_main(int argc, char **argv)
{
  (void)argc;
  (void)argv;    
	add_image_treat_menu_item ( "movie_avg", NULL, movie_avg_image_menu(), 0, NULL);
	return D_O_K;
}

int	movie_avg_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;    
	remove_item_to_menu(image_treat_menu, "movie_avg", NULL, NULL);
	return D_O_K;
}
#endif

