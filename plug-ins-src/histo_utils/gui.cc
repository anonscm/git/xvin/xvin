#include "gui.hh"
#include "histo_utils.hh"
int do_shift_between_two_ds(void);
MENU *histo_utils_plot_menu(void)
{
    static MENU mn[32];

    if (mn[0].text != NULL)
        return mn;

    add_item_to_menu(mn, "get point from all ds with same x", do_get_all_ds_points_by_x, NULL, 0, NULL);
    add_item_to_menu(mn, "average_all_ds_of_op_by_x_by_histo", do_average_all_ds_of_op_by_x_by_histo, NULL, 0, NULL);
    add_item_to_menu(mn, "do_generate_ds_of_points_with_same_x", do_generate_ds_of_points_with_same_x, NULL, 0, NULL);
    add_item_to_menu(mn, "one bead drift correction", do_non_linear_drift_correction_between_cycle_by_histogram_alignment,
                     NULL, 0, NULL);
    add_item_to_menu(mn, "one bead drift correction v2", do_non_linear_drift_correction_between_cycle_by_histogram_alignment_v2,
                     NULL, 0, NULL);
    add_item_to_menu(mn, "shift", do_shift_between_two_ds, NULL, 0, NULL);
    return mn;
}

int histo_utils_main(int argc, char **argv)
{
    (void) argc;
    (void) argv;

    //testRestEnzParser();

    add_plot_treat_menu_item("histo_utils", NULL, histo_utils_plot_menu(), 0, NULL);
    return D_O_K;
}

int hist_utils_unload(int argc, char **argv)
{
    (void) argc;
    (void) argv;

    remove_item_to_menu(plot_treat_menu, "histo_utils", NULL, NULL);
    return D_O_K;
}

int do_generate_ds_of_points_with_same_x(void)
{
    pltreg *pr = NULL;
    O_p *op = NULL;
    O_p *opo = NULL;
    d_s *ds = NULL;
    int singlem = 0;
    float mavg = 0.;
    bool all_error = 0;
    bool all_no_error = 0;

    ac_grep(cur_ac_reg, "%pr%op", &pr, &op);

    if (updating_menu_state != 0)
    {
//        if (fingerprints.size() > 0 || needles.size() > 0)
//            active_menu->flags &= ~D_DISABLED;
//        else
//            active_menu->flags |=  D_DISABLED;
//
        return D_O_K;
    }

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("");
    }

    opo  = create_and_attach_one_plot(pr, 1, 1, 0);
    ds = generate_ds_of_points_with_same_x(op, 0, &singlem, &mavg, &all_error, &all_no_error);
    add_data_to_one_plot(opo, IS_DATA_SET, ds);
    remove_ds_from_op(opo, opo->dat[0]);
    opo->need_to_refresh = 1;
    refresh_plot(pr, UNCHANGED);

    win_printf("number of single points %d\n"
               "average number of dataset per point %f\n"
               "%s", singlem, mavg, all_error ? "all ds has error bar" : all_no_error ? "no ds has error bar" :
               "mixed error / no error bar");

    return D_O_K;

}

int do_average_all_ds_of_op_by_x_by_histo(void)
{
    pltreg  *pr = NULL;
    O_p     *src_op = NULL;
    int win_alleg_ret = 0;
    static float discard_point_thres = 10.;
    static float biny = 0.1;
    static float thres = 0.5;
    static int keeph = 0;
    static int multi_peak = 0;

    if (updating_menu_state != 0)
    {
        return (D_O_K);
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine performs averaging oy x and y of all data sets\n"
                             "having the same point index");
    }

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &src_op) != 2)
    {
        return win_printf_OK("cannot find data!");
    }

    win_alleg_ret = win_scanf("Average all data sets of a plot at the same abscisse\n"
                              "using an histogram along Y to average and peak maximum value.\n"
                              "Bin size in Y %8f for the histogramm. Select:\n"
                              "\\oc  %R a single point corresponding to the maximum of the histogram\n"
                              "\\oc  %r all maxima greater than a threshold value\n"
                              "\\oc  %r the combination of both\n"
                              "Define the treshold value %12f\n"
                              "keep histogram plot %b\n"
                              "discard point present in less than %5f %% of ds"
                              "max d"
                              , &biny,
                              &multi_peak,
                              &thres,
                              &keeph,
                              &discard_point_thres);

    if (win_alleg_ret == WIN_CANCEL)
    {
        return D_O_K;
    }

    return average_all_ds_of_op_by_x_by_histo(pr, src_op, biny, keeph, thres, discard_point_thres, multi_peak);

}
int do_non_linear_drift_correction_between_cycle_by_histogram_alignment_v2(void)
{
    pltreg  *pr = NULL;
    O_p     *src_op = NULL;
    int win_alleg_ret = 0;
    static float discard_point_thres = 10.;
    static float biny = 0.003;
    static int keep_intermediate_histo = 0;
    static int keep_shift_data = 0;
    static int keep_histo_mult_derivate_plot = 0;
    static int poly_fit_degree = 5;
    static int num_iteration = 20;


    if (updating_menu_state != 0)
    {
        return (D_O_K);
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("Do a non linear drift correction between cycle by histogram alignment");
    }

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &src_op) != 2)
    {
        return win_printf_OK("cannot find data!");
    }

    win_alleg_ret = win_scanf("correct all data sets of a plot at the same abscisse\n"
                              "using an histogram along Y to average and peak maximum value.\n"
                              "Bin size in Y %8f for the histogramm.\n"
                              "Polynome fit degree %5d \n"
                              "Number of iteration to find zero %5d \n"
                              //"Define the minimum treshold to keep a local maximum %12f\n"
                              "keep intermediate histogram plot %b\n"
                              "keep shift between histogram %b\n"
                              "keep  histogram \n times derivate for each shift (Warning : huge) %b\n"
                              "discard point present in less than %5f percent of datasets"
                              , &biny,
                              &poly_fit_degree,
                              &num_iteration,
                              &keep_intermediate_histo,
                              &keep_shift_data,
                              &keep_histo_mult_derivate_plot,
                              &discard_point_thres);

    if (win_alleg_ret == WIN_CANCEL)
    {
        return D_O_K;
    }

    non_linear_drift_correction_between_cycle_by_histogram_alignment_v2(pr, src_op, biny, poly_fit_degree, num_iteration,
            discard_point_thres, keep_intermediate_histo, keep_shift_data, keep_histo_mult_derivate_plot);
    return refresh_plot(pr, UNCHANGED);
}
int do_non_linear_drift_correction_between_cycle_by_histogram_alignment(void)
{
    pltreg  *pr = NULL;
    O_p     *src_op = NULL;
    int win_alleg_ret = 0;
    static float discard_point_thres = 10.;
    static float biny = 0.003;
    static int keep_intermediate_histo = 0;
    static int keep_shift_data = 0;
    static int keep_histo_mult_derivate_plot = 0;

    if (updating_menu_state != 0)
    {
        return (D_O_K);
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("Do a non linear drift correction between cycle by histogram alignment");
    }

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &src_op) != 2)
    {
        return win_printf_OK("cannot find data!");
    }

    win_alleg_ret = win_scanf("correct all data sets of a plot at the same abscisse\n"
                              "using an histogram along Y to average and peak maximum value.\n"
                              "Bin size in Y %8f for the histogramm.\n"
                              //"Define the minimum treshold to keep a local maximum %12f\n"
                              "keep intermediate histogram plot %b\n"
                              "keep shift between histogram %b\n"
                              "keep  histogram \n\times derivate for each shift (Warning : huge) %b\n"
                              "discard point present in less than %5f %% of datasets"
                              , &biny,
                              &keep_intermediate_histo,
                              &keep_shift_data,
                              &keep_histo_mult_derivate_plot,
                              &discard_point_thres);

    if (win_alleg_ret == WIN_CANCEL)
    {
        return D_O_K;
    }

    non_linear_drift_correction_between_cycle_by_histogram_alignment(pr, src_op, biny,
            discard_point_thres, keep_intermediate_histo, keep_shift_data, keep_histo_mult_derivate_plot);
    return refresh_plot(pr, UNCHANGED);
}

int do_get_all_ds_points_by_x(void)
{
    pltreg  *pr = NULL;
    O_p     *src_op = NULL;
    O_p     *opo = NULL;
    d_s      *ds = NULL;
    int singlem = 0;
    float mavg = 0.;
    bool all_error = 0;
    bool all_no_error = 0;

    if (updating_menu_state != 0)
    {
        return (D_O_K);
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine performs averaging oy x and y of all data sets\n"
                             "having the same point index");
    }

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &src_op) != 2)
    {
        return win_printf_OK("cannot find data!");
    }


    opo  = create_and_attach_one_plot(pr, 1, 1, 0);
    ds = generate_ds_of_points_with_same_x(src_op, src_op->n_dat, &singlem, &mavg, &all_error, &all_no_error);
    get_all_ds_points_by_x(pr, src_op, ds)->need_to_refresh = 1;
    refresh_plot(pr, UNCHANGED);

    return D_O_K;
}

