#pragma once
#include "xvin.h"

PXV_FUNC(MENU, *histo_utils_plot_menu, (void));
PXV_FUNC(int, histo_utils_main, (int argc, char **argv));
PXV_FUNC(int, hist_utils_unload, (int argc, char **argv));
PXV_FUNC(int, do_generate_ds_of_points_with_same_x, (void));
PXV_FUNC(int, do_average_all_ds_of_op_by_x_by_histo, (void));
PXV_FUNC(int, do_non_linear_drift_correction_between_cycle_by_histogram_alignment, (void));
PXV_FUNC(int, do_non_linear_drift_correction_between_cycle_by_histogram_alignment_v2, (void));
PXV_FUNC(int, do_get_all_ds_points_by_x, (void));
