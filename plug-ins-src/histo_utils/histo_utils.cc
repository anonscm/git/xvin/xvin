#include "histo_utils.hh"
#include "p_treat_basic.h"
#include "p_treat_math.h"
#include <float.h>
#include "../fft2d/fftbtl32n.h"
#include <assert.h>

std::vector<float> *get_local_maxima_x_sorted_data(d_s *data, float mthres)
{
    float max_pos = 0;
    float x_val_of_y_max = 0;
    float max_y_val = 0;
    int find_max_ret = 0;
    std::vector<float> *maxvect = new std::vector<float>();

    for (int k = 1; k < data->nx - 1; k++)
    {
        if ((data->yd[k] > data->yd[k - 1]) && (data->yd[k] >= data->yd[k + 1])
                && (data->yd[k] > mthres))
        {
            //win_printf("k = %d y %g",k,dsi->yd[k]);
            find_max_ret = find_max_around(data->yd, data->nx, k, &max_pos, &max_y_val, NULL);

            if (find_max_ret == 0)
            {
                int pk = (int)max_pos;
                int pk1 = pk + 1;
                float p = (float)pk1 - max_pos;
                pk = (pk < 0) ? 0 : pk;
                pk = (pk < data->nx) ? pk : data->nx - 1;
                pk1 = (pk1 < 0) ? 0 : pk1;
                pk1 = (pk1 < data->nx) ? pk1 : data->nx - 1;
                x_val_of_y_max = p * data->xd[pk] + (1 - p) * data->xd[pk1];
                maxvect->push_back(x_val_of_y_max);
            }
        }
    }

    if (maxvect->size() == 0)
    {
        delete maxvect;
        return 0;
    }

    return maxvect;
}

/**
 * @brief take an histogram and extract the local maxima peaks positions and theirs values
 *
 * @param data input histogram
 * @param mthres threashold for maximum
 *
 * @return a vector of pair containing : first : x position of the peak and second : y value
 */
std::vector<std::pair<float, float>> *get_local_maxima_pos_and_val_sorted_data(d_s *data, float mthres)
{
    float max_pos = 0;
    float x_val_of_y_max = 0;
    float max_y_val = 0;
    int find_max_ret = 0;
    std::vector<std::pair<float, float>> *maxvect = new std::vector<std::pair<float, float>>();

    for (int k = 1; k < data->nx - 1; k++)
    {
        if ((data->yd[k] > data->yd[k - 1]) && (data->yd[k] >= data->yd[k + 1])
                && (data->yd[k] > mthres))
        {
            //win_printf("k = %d y %g",k,dsi->yd[k]);
            find_max_ret = find_max_around(data->yd, data->nx, k, &max_pos, &max_y_val, NULL);

            if (find_max_ret == 0)
            {
                int pk = (int)max_pos;
                int pk1 = pk + 1;
                float p = (float)pk1 - max_pos;
                pk = (pk < 0) ? 0 : pk;
                pk = (pk < data->nx) ? pk : data->nx - 1;
                pk1 = (pk1 < 0) ? 0 : pk1;
                pk1 = (pk1 < data->nx) ? pk1 : data->nx - 1;
                x_val_of_y_max = p * data->xd[pk] + (1 - p) * data->xd[pk1];
                maxvect->push_back(std::make_pair(x_val_of_y_max, max_y_val));
            }
        }
    }

    if (maxvect->size() == 0)
    {
        delete maxvect;
        return 0;
    }

    return maxvect;
}

d_s **allocate_output_histograms(O_p *op, int maxh, int histo_count)
{
    d_s **histos = NULL;

    if (op)
    {
        for (int i = 1; i < histo_count; ++i)
        {
            create_and_attach_one_ds(op, maxh, maxh, 0);
        }

        histos = op->dat;
    }
    else
    {
        histos = (d_s **) calloc(histo_count, sizeof(d_s *));

        for (int i = 0; i < histo_count; ++i)
        {
            histos[i] = build_data_set(maxh, maxh);
        }
    }

    return histos;
}

int non_linear_drift_correction_between_cycle_by_histogram_alignment(pltreg *pr, O_p *src_op,
        float biny,
        int discard_point_thres,
        bool keep_intermediate_histo,
        bool keep_shift_data,
        bool keep_histo_mult_derivate_plot)
{
    O_p *histo_op = NULL;
    O_p *derivate_op = NULL;
    O_p *cumulated_histo_op = NULL;
    O_p *shifts_op = NULL;
    O_p *drift_op = NULL;
    O_p *projection_op = NULL;
    d_s *x_count_ds = NULL;
    d_s *fit_ds = NULL;
    d_s *drift_ds = NULL;
    double cur_b_fit = 0;
    float drift_min_val = 0;
    float max_offset = biny * 3; // 3 sigma
    int max_offset_index = (max_offset * 5) / biny; // five point per biny
    int histo_count = 0;
    int range_min_idx = 0;
    int range_max_idx = 0;
    int last_shift_idx = 0;
    x_count_ds = generate_ds_of_points_with_same_x(src_op, (src_op->n_dat * discard_point_thres) / 100, NULL, NULL, NULL,
                 NULL);

    if (!x_count_ds)
    {
        return CANCEL;
    }

    histo_count = x_count_ds->nx;
    histo_op = get_all_ds_histo_by_x(pr, src_op, x_count_ds, biny);
    cumulated_histo_op = create_and_attach_one_plot(pr, histo_op->dat[histo_op->n_dat - 1]->nx,
                         histo_op->dat[histo_op->n_dat - 1]->nx, 0);

    //reference to sum histograms
    duplicate_data_set(histo_op->dat[histo_op->n_dat - 1], cumulated_histo_op->dat[0]);
    shifts_op = create_and_attach_one_plot(pr, max_offset_index * 2 + 1, max_offset_index * 2 + 1, 0);
    remove_all_point_from_data_set(shifts_op->dat[0]);
    drift_op = create_and_attach_one_plot(pr, histo_count, histo_count, 0);
    drift_ds = drift_op->dat[0];

    //add_ds_to_op(drift_op, x_count_ds);

    remove_all_point_from_data_set(drift_ds);
    derivate_op = true_y_derivative(histo_op, histo_count - 1, NULL);

    for (int i = histo_count - 2; i >= 0; --i)
    {
        d_s *cur_histo = histo_op->dat[i];
        d_s *cur_cumul_histo = cumulated_histo_op->dat[cumulated_histo_op->n_dat - 1];
        d_s *cur_cumul_derivate = true_y_derivative_one_ds(cur_cumul_histo, cumulated_histo_op->dx, cumulated_histo_op->dy);
        d_s *cur_shift = create_and_attach_one_ds(shifts_op, max_offset_index * 2 + 1, max_offset_index * 2 + 1, 0);

        add_data_to_one_plot(derivate_op, IS_DATA_SET, (void *)cur_cumul_derivate);
        remove_all_point_from_data_set(cur_shift);
        cur_shift->source = strdup(cur_histo->source);

        if (keep_histo_mult_derivate_plot)
        {
            projection_op = create_and_attach_one_plot(pr, cur_histo->nx, cur_histo->ny, 0);
        }

        for (int j = -max_offset_index + last_shift_idx; j <= max_offset_index + last_shift_idx; ++j)
        {
            d_s *cur_res = multiply_on_two_ds_by_index_with_offset(cur_histo, cur_cumul_derivate, 0, j);

            add_new_point_to_ds(cur_shift, (j * biny) / 5, sum_y_along_x_ds(cur_res));

            if (keep_histo_mult_derivate_plot)
            {
                add_data_to_one_plot(projection_op, IS_DATA_SET, (void *) cur_res);
            }
            else
            {
                free_data_set(cur_res);
            }
        }

        //find_bigest_y_monotone_range(shifts_op, cur_shift, &range_min_idx, &range_max_idx);
        find_bigest_y_almost_linear_range(shifts_op, cur_shift, &range_min_idx, &range_max_idx);
        swap_x_y_in_plot(shifts_op);
        fit_ds = least_square_fit_on_ds_between_indexes(cur_shift, true, range_min_idx, range_max_idx, NULL, &cur_b_fit);
        swap_x_y_in_plot(shifts_op);

        // for display
        remove_point_using_range(shifts_op, &cur_shift,
                                 cur_shift->xd[range_min_idx], cur_shift->xd[range_max_idx],
                                 -FLT_MAX, FLT_MAX, false);

        add_new_point_to_ds(drift_ds, x_count_ds->xd[i], -cur_b_fit);

        last_shift_idx = round((cur_b_fit * 5 / biny));

        cur_cumul_histo = add_on_2_ds_by_index_with_offset(cur_cumul_histo, cur_histo, 0, round((cur_b_fit * 5 / biny)));
        cur_cumul_histo->source = strdup(cur_histo->source);

        add_data_to_one_plot(cumulated_histo_op, IS_DATA_SET, (void *) cur_cumul_histo);
    }

    sort_ds_along_x(drift_ds);
    drift_min_val = find_min_y_value(drift_ds);

    if (drift_min_val < 0)
    {
        for (int i = 0; i < drift_ds->ny; ++i)
        {
            drift_ds->yd[i] -= drift_min_val;
        }
    }

    uns_op_2_op_by_type(src_op, IS_Y_UNIT_SET, cumulated_histo_op, IS_X_UNIT_SET);
    uns_op_2_op_by_type(src_op, IS_Y_UNIT_SET, derivate_op, IS_X_UNIT_SET);
    uns_op_2_op_by_type(src_op, IS_Y_UNIT_SET, shifts_op, IS_X_UNIT_SET);
    uns_op_2_op_by_type(src_op, IS_Y_UNIT_SET, drift_op, IS_Y_UNIT_SET);
    uns_op_2_op_by_type(src_op, IS_X_UNIT_SET, drift_op, IS_X_UNIT_SET);

    cumulated_histo_op->title = strdup("cumulated histograms\n");
    shifts_op->title = strdup("shifts between histograms\n");
    derivate_op->title = strdup("derivative of cumulated histograms");


    remove_data(pr, IS_ONE_PLOT, (void *)(derivate_op));

    if (!keep_intermediate_histo)
    {
        remove_data(pr, IS_ONE_PLOT, (void *)(histo_op));

        while (cumulated_histo_op->n_dat > 1)
        {
            remove_ds_n_from_op(cumulated_histo_op, 0);
        }
    }

    if (!keep_shift_data)
    {
        remove_data(pr, IS_ONE_PLOT, (void *)(shifts_op));
    }

    free(x_count_ds);

    return 0;
}

int non_linear_drift_correction_between_cycle_by_histogram_alignment_v2(pltreg *pr, O_p *src_op,
        float biny,
        int poly_fit_degree,
        int zero_approx_iteration_count,
        int discard_point_thres,
        bool keep_intermediate_histo,
        bool keep_shift_data,
        bool keep_histo_mult_derivate_plot)
{
    O_p *slices_op = NULL;
    O_p *intermediate_op = NULL;
    O_p *shifts_op = NULL;
    O_p *drift_op = NULL;
    O_p *projection_op = NULL;
    O_p *cumul_slice_histo_derivate_op = NULL;
    d_s *x_count_ds = NULL;
    d_s *drift_ds = NULL;
    double *coefs = NULL;
    double cur_x_fit = 0;
    float drift_min_val = 0;
    float max_offset = biny * 3; // 3 sigma
    int max_offset_index = (max_offset * 5) / biny; // five point per biny
    int slice_count = 0;
    int last_shift_idx = 0;
    int err = 0;
    int autoconv_err_count = 0;
    x_count_ds = generate_ds_of_points_with_same_x(src_op, (src_op->n_dat * discard_point_thres) / 100, NULL, NULL, NULL,
                 NULL);
    float ymin = 0;
    float ymax = 0;
    get_plot_y_range(src_op, &ymin, &ymax, NULL, NULL);

    if (!x_count_ds)
    {
        return CANCEL;
    }

    slice_count = x_count_ds->nx;
    slices_op = get_all_ds_points_by_x(pr, src_op, x_count_ds);

    intermediate_op = create_and_attach_one_plot(pr, x_count_ds->yd[x_count_ds->nx - 1],
                      x_count_ds->yd[x_count_ds->nx - 1], 0);
    d_s *cur_cumul_slice = intermediate_op->dat[0];
    duplicate_data_set(slices_op->dat[slices_op->n_dat - 1], cur_cumul_slice);
    d_s *cumul_slice_histo = create_and_attach_one_ds(intermediate_op, 1, 1, 0);
    cumul_slice_histo = get_histo_convolution(slices_op->dat[slices_op->n_dat - 1], cumul_slice_histo, biny, ymin, ymax);

    cumul_slice_histo_derivate_op = create_and_attach_one_plot(pr, 1, 1, 0);

    //reference to sum histograms
    shifts_op = create_and_attach_one_plot(pr, max_offset_index * 2 + 1, max_offset_index * 2 + 1, 0);
    remove_all_point_from_data_set(shifts_op->dat[0]);

    drift_op = create_and_attach_one_plot(pr, slice_count, slice_count, 0);
    drift_ds = drift_op->dat[0];
    remove_all_point_from_data_set(drift_ds);

    //add_ds_to_op(drift_op, x_count_ds);

    for (int i = slice_count - 2; i >= 0; --i)
    {
        d_s *cur_slice = slices_op->dat[i];

        cumul_slice_histo = get_histo_convolution(cur_slice, cumul_slice_histo, biny, ymin, ymax);
        d_s *cur_cumul_derivate = true_y_derivative_one_ds(cumul_slice_histo, cumul_slice_histo_derivate_op->dx,
                                  cumul_slice_histo_derivate_op->dy);
        d_s *cur_shift = create_and_attach_one_ds(shifts_op, max_offset_index * 2 + 1, max_offset_index * 2 + 1, 0);
        remove_all_point_from_data_set(cur_shift);
        cur_shift->source = strdup(cur_slice->source);


        if (keep_histo_mult_derivate_plot)
        {
            projection_op = create_and_attach_one_plot(pr, cumul_slice_histo->nx, cumul_slice_histo->ny, 0);
        }

        for (int j = -max_offset_index + last_shift_idx; j <= max_offset_index + last_shift_idx; ++j)
        {
            float fitness = get_ds_fitness_derivate_projection_with_offset(cur_slice, cur_cumul_derivate, (j * biny) / 5);

            add_new_point_to_ds(cur_shift, (j * biny) / 5, fitness);
        }

        //find_bigest_y_monotone_range(shifts_op, cur_shift, &range_min_idx, &range_max_idx);
        //find_bigest_y_almost_linear_range(shifts_op, cur_shift, &range_min_idx, &range_max_idx);
        //fit_ds = //least_square_fit_on_ds_between_indexes(cur_shift, true, range_min_idx, range_max_idx, NULL, &cur_b_fit);

        fit_ds_to_xn_polynome(cur_shift, poly_fit_degree, -FLT_MAX, FLT_MAX, -FLT_MAX, FLT_MAX, &coefs);

        float zero_approx_x_up = -FLT_MAX;
        float zero_approx_x_lo = FLT_MAX;

        err = find_neighboors_x_of_zero(cur_shift, &zero_approx_x_lo, &zero_approx_x_up);


        // if cur_shift does not intersect 0, we take previous value
        if (err == 0)
        {
            for (int j = 0; j < zero_approx_iteration_count; ++j)
            {
                float cur_x = (zero_approx_x_up + zero_approx_x_lo) / 2;
                float cur_y = 0.;

                float xn = cur_x;
                cur_y = coefs[0];

                for (int k = 1; k < poly_fit_degree; ++k)
                {
                    cur_y += coefs[k] * xn;
                    xn *= cur_x;
                }

                if (cur_y >= 0.) //&&  cur_y < zero_approx_x_up)
                {
                    zero_approx_x_up = cur_x;
                }
                else if (cur_y < 0.) //&&  fabsf(cur_y) < fabsf(zero_approx_x_lo))
                {
                    zero_approx_x_lo = cur_x;
                }
            }

            cur_x_fit = (zero_approx_x_lo + zero_approx_x_up) / 2;

        }
        else
        {
            autoconv_err_count++;
        }

        add_new_point_to_ds(drift_ds, x_count_ds->xd[i], -cur_x_fit) ;
        //printf("(approx lo = %f  approx up = %f, res %f)\n", zero_approx_x_lo, zero_approx_x_up, -cur_x_fit);

        last_shift_idx = round((cur_x_fit * 5 / biny));


        cur_cumul_slice = duplicate_data_set(cur_cumul_slice, NULL);

        for (int j = 0; j < cur_slice->nx; ++j)
        {
            add_new_point_to_ds(cur_cumul_slice, cur_slice->xd[j] + cur_x_fit, cur_slice->yd[j]);
        }

        if (cur_slice->source)
        {
            cur_cumul_slice->source = strdup(cur_slice->source);
        }


        if (keep_intermediate_histo)
        {
            add_data_to_one_plot(cumul_slice_histo_derivate_op, IS_DATA_SET, (void *)cur_cumul_derivate);
        }
        else
        {
            free_data_set(cur_cumul_derivate);
        }
    }


    sort_ds_along_x(drift_ds);

    drift_min_val = find_min_y_value(drift_ds);

    if (drift_min_val < 0)
    {
        for (int i = 0; i < drift_ds->ny; ++i)
        {
            drift_ds->yd[i] -= drift_min_val;
        }
    }

    uns_op_2_op_by_type(src_op, IS_Y_UNIT_SET, slices_op, IS_X_UNIT_SET);
    uns_op_2_op_by_type(src_op, IS_Y_UNIT_SET, intermediate_op, IS_X_UNIT_SET);
    uns_op_2_op_by_type(src_op, IS_Y_UNIT_SET, cumul_slice_histo_derivate_op, IS_X_UNIT_SET);
    uns_op_2_op_by_type(src_op, IS_Y_UNIT_SET, shifts_op, IS_X_UNIT_SET);
    uns_op_2_op_by_type(src_op, IS_Y_UNIT_SET, drift_op, IS_Y_UNIT_SET);
    uns_op_2_op_by_type(src_op, IS_X_UNIT_SET, drift_op, IS_X_UNIT_SET);

    intermediate_op->title = strdup("cumulated temporal data \n");
    shifts_op->title = strdup("shifts between histograms\n");
    cumul_slice_histo_derivate_op->title = strdup("derivative of histogram of cumulated_slices_op");


    remove_data(pr, IS_ONE_PLOT, (void *)(cumul_slice_histo_derivate_op));

    if (!keep_intermediate_histo)
    {
        remove_data(pr, IS_ONE_PLOT, (void *)(slices_op));
    }

    if (!keep_shift_data)
    {
        remove_data(pr, IS_ONE_PLOT, (void *)(shifts_op));
    }

    free(x_count_ds);

    if (autoconv_err_count > 0)
    {
        warning_message("auto-convolution fail for %d points", autoconv_err_count);
    }
    return 0;
}

int find_neighboors_x_of_zero(d_s *src_ds, float *low_bound, float *up_bound)
{

    for (int i = 1; i < src_ds->nx; ++i)
    {
        float prev_y = src_ds->yd[i - 1];
        float cur_y = src_ds->yd[i];

        if (copysignf(1.0, prev_y) != copysignf(1.0, cur_y))
        {
            if (copysignf(1.0, cur_y) == -1.0)
            {
                if (low_bound != NULL)
                    *low_bound = src_ds->xd[i];

                if (up_bound != NULL)
                    *up_bound = src_ds->xd[i - 1];
            }
            else
            {
                if (low_bound != NULL)
                    *low_bound = src_ds->xd[i - 1];

                if (up_bound != NULL)
                    *up_bound = src_ds->xd[i];
            }

            return 0;
        }
    }

    return -1;
}

float find_min_x_value(d_s *ds)
{
    float min_value = FLT_MAX;

    for (int i = 0; i < ds->nx; ++i)
    {
        min_value = my_min(ds->xd[i], min_value);
    }

    return min_value;
}
float find_max_x_value(d_s *ds)
{
    float max_value = -FLT_MAX;

    for (int i = 0; i < ds->nx; ++i)
    {
        max_value = my_max(ds->xd[i], max_value);
    }

    return max_value;
}

float find_min_y_value(d_s *ds)
{
    float min_value = FLT_MAX;

    for (int i = 0; i < ds->nx; ++i)
    {
        min_value = my_min(ds->yd[i], min_value);
    }

    return min_value;
}
float find_max_y_value(d_s *ds)
{
    float max_value = -FLT_MAX;

    for (int i = 0; i < ds->nx; ++i)
    {
        max_value = my_max(ds->yd[i], max_value);
    }

    return max_value;
}

void find_bigest_y_monotone_range(O_p *op, d_s *src_ds, int *out_min_idx, int *out_max_idx)
{
    d_s *tmp_ds = true_y_derivative_one_ds(src_ds, op->dx, op->dy);
    int min_idx_biggest = 0;
    int max_idx_biggest = 0;
    int biggest_count = 0;
    int cur_min_idx = 0;
    int cur_count = 0;

    for (int i = 1; i < tmp_ds->nx; ++i)
    {
        float prev_y = tmp_ds->yd[i - 1];
        float cur_y = tmp_ds->yd[i];

        if (copysignf(1.0, prev_y) != copysignf(1.0, cur_y))
        {
            if (cur_count > biggest_count)
            {
                min_idx_biggest = cur_min_idx;
                max_idx_biggest = i - 1;
                biggest_count = cur_count;
            }

            cur_min_idx = i;
            cur_count = 0;
        }
        else
        {
            cur_count++;
        }
    }

    if (cur_count > biggest_count)
    {
        min_idx_biggest = cur_min_idx;
        max_idx_biggest = src_ds->nx - 1;
    }

    if (out_min_idx)
    {
        *out_min_idx = min_idx_biggest;
    }

    if (out_max_idx)
    {
        *out_max_idx = max_idx_biggest;
    }

    free_data_set(tmp_ds);
}

//TODO : find a better name !
float get_ds_fitness_derivate_projection_with_offset(d_s *src_ds, d_s *derivate_ds, float offset)
{
    float sum = 0;

    for (int i = 0; i < src_ds->nx; ++i)
    {
        sum += interpolate_point_by_poly_4_in_array(derivate_ds->xd, derivate_ds->yd, derivate_ds->nx, src_ds->xd[i] + offset);
        //printf("%f\n", pt);
    }

    return sum;

}
//TODO add condition to check if the range choosen cross 0
void find_bigest_y_almost_linear_range(O_p *op, d_s *src_ds, int *out_min_idx, int *out_max_idx)
{
    int min_idx_biggest = 0;
    int max_idx_biggest = 0;
    int biggest_count = 0;
    int cur_min_idx = 0;
    int cur_count = 0;
    d_s *speed_ds = true_y_derivative_one_ds(src_ds, op->dx, op->dy);
    d_s *accel_ds = true_y_derivative_one_ds(speed_ds, op->dx, op->dy);
    d_s *jerk_ds = true_y_derivative_one_ds(accel_ds, op->dx, op->dy);

    for (int i = 1; i < jerk_ds->nx; ++i)
    {
        float prev_y = jerk_ds->yd[i - 1];
        float cur_y = jerk_ds->yd[i];

        if (copysignf(1.0, prev_y) != copysignf(1.0, cur_y))
        {
            if (cur_count > biggest_count)
            {
                min_idx_biggest = cur_min_idx;
                max_idx_biggest = i - 1;
                biggest_count = cur_count;
            }

            cur_min_idx = i;
            cur_count = 0;
        }
        else
        {
            cur_count++;
        }
    }

    if (cur_count > biggest_count)
    {
        min_idx_biggest = cur_min_idx;
        max_idx_biggest = src_ds->nx - 1;
    }

    if (out_min_idx)
    {
        *out_min_idx = min_idx_biggest;
    }

    if (out_max_idx)
    {
        *out_max_idx = max_idx_biggest;
    }

    free_data_set(speed_ds);
    free_data_set(accel_ds);
    free_data_set(jerk_ds);
}




d_s *manual_shift_in_equ_spaced_ds(O_p *op, d_s *ds, unsigned int shift)
{
    d_s *res_ds = duplicate_data_set(ds, create_and_attach_one_ds(op, 1, 1, 0));

    for (int i = shift; i < res_ds->nx; ++i)
    {
        res_ds->yd[i - shift] = res_ds->yd[i];
    }

    return res_ds;
}

int do_shift_between_two_ds(void)
{
    pltreg *pr = NULL;
    O_p *src_op = NULL;
    O_p *shifts_op = NULL;
    d_s *histo_of0 = NULL;
    d_s *histo_of1 = NULL;
    d_s *derive_of1 = NULL;
    static float biny = 0.003;
    float max_offset = biny * 5; // 5 sigma
    int max_offset_index = ((max_offset * 5) / biny); // five point per biny

    if (updating_menu_state != 0)
    {
//        if (fingerprints.size() > 0 || needles.size() > 0)
//            active_menu->flags &= ~D_DISABLED;
//        else
//            active_menu->flags |=  D_DISABLED;
//
        return D_O_K;
    }

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &src_op) != 2 || src_op->n_dat < 2)
        return D_O_K;

    shifts_op = create_and_attach_one_plot(pr, 1, 1, 0);
    histo_of0 = src_op->dat[0];
    histo_of1 = src_op->dat[1]; //manual_shift_in_equ_spaced_ds(src_op, histo_of0, 10);//src_op->dat[1];
    true_y_derivative(src_op, 1, src_op);
    derive_of1 = src_op->dat[2];
    d_s *cur_shift = shifts_op->dat[0];
    d_s *cur_correlation = create_and_attach_one_ds(shifts_op, 1, 1, 0);
    cur_shift->nx = 0;
    cur_shift->ny = 0;
    cur_correlation->nx = 0;
    cur_correlation->ny = 0;

    for (int j = -max_offset_index; j <= max_offset_index; ++j)
    {
        //printf("j %d, ,xval %f, meth2%f \n", j, x_val, j * biny / 5);
        d_s *cur_res = multiply_on_two_ds_by_index_with_offset(derive_of1, histo_of0, 0, j);
        d_s *cur_res_correl = multiply_on_two_ds_by_index_with_offset(histo_of1, histo_of0, 0, j);
        add_new_point_to_ds(cur_shift, (j * biny) / 5, sum_y_along_x_ds(cur_res));
        add_new_point_to_ds(cur_correlation, (j * biny) / 5, sum_y_along_x_ds(cur_res_correl));
        //add_new_point_to_ds(cur_points, j);
        //add_one_plot_data(shifts_op, IS_DATA_SET, (void *)cur_res);
    }

    //uns_op_2_op_by_type(src_op, IS_X_UNIT_SET, shifts_op, IS_X_UNIT_SET);
    return refresh_plot(pr, UNCHANGED);
}

/**
 * @brief summing ds using Kahan summation algorithm, to minimize error
 *
 * @param src_ds
 *
 * @return
 */
float sum_y_along_x_ds(const d_s *src_ds)
{
    double sum = 0.;
    double c = 0.;

    if (src_ds == NULL)
        return 1;

    for (int i = 0; i < src_ds->nx; ++i)
    {
        double y = src_ds->yd[i] - c;
        double t = sum + y;
        c = (t - sum) - y;
        sum = t;
    }

    return sum;
}
/**
 * @brief
 *
 * @return
 */
int average_all_ds_of_op_by_x_by_histo(pltreg *pr, O_p *src_op, float biny, float keeph, float thres,
                                       int discard_point_thres, int multi_peak)
{
    O_p *histo_op = NULL;
    d_s **histos = NULL;
    d_s *dest_ds = NULL;
    d_s *x_count_ds = NULL;
    int histo_count = 0;
    int discard_point_count = 0;
    float maxmax;
    discard_point_count = (src_op->n_dat * discard_point_thres) / 100;
    x_count_ds = generate_ds_of_points_with_same_x(src_op, discard_point_count, NULL, NULL, NULL, NULL);
    histo_count = x_count_ds->nx;
    // detemine y points range and max and largest dataset size
    // creating resulting dataset containing maximums
    dest_ds = create_and_attach_one_ds(src_op, src_op->dat[0]->nx, src_op->dat[0]->nx, 0);

    if (dest_ds == NULL)
    {
        return win_printf("Cannot allocate plot");
    }

    set_ds_dot_line(dest_ds);
    set_plot_symb(dest_ds, "\\pt5\\oc ");
    dest_ds->nx = dest_ds->ny = 0;
    histo_op = get_all_ds_histo_by_x(pr, src_op, x_count_ds, biny);
    histos = histo_op->dat;

    // getting all maximums
    for (int i = 0; i < histo_count; ++i)
    {
        d_s *cur_histo = histos[i];
        float cur_histo_pos = x_count_ds->xd[i]; // x position of the current histogram into the src plot
        // Some C++, sorry :/
        maxmax = -FLT_MAX;
        std::vector<std::pair<float, float>> *maxs  = get_local_maxima_pos_and_val_sorted_data(cur_histo, thres);

        if (maxs)
        {
            for (auto cur_max_pair : *maxs)
            {
                if (multi_peak > 0)
                {
                    add_new_point_to_ds(dest_ds, cur_histo_pos, cur_max_pair.first);
                }
                else if (cur_max_pair.first > maxmax)
                {
                    maxmax = cur_max_pair.first;
                }
            }

            delete maxs;
        }

        // end of c++

        if (multi_peak == 0)
        {
            add_new_point_to_ds(dest_ds, cur_histo_pos, maxmax);
        }
    }

    if (keeph == 0)
    {
        remove_data(pr, IS_ONE_PLOT, (void *)(histo_op));
    }

    set_ds_source(dest_ds, "Average data sets over %d data sets", src_op->n_dat);
    src_op->need_to_refresh = 1;
    return refresh_plot(pr, UNCHANGED);
}

d_s *get_histo_convolution(d_s *src_ds, d_s *dest_ds, float biny, float xmin, float xmax)
{
    float maxh = 0;
    float tmp = 0;

    maxh = (int)(0.5 + ((xmax - xmin) * 5) / biny);

    if (dest_ds == NULL)
    {
        dest_ds = build_data_set(maxh, maxh);
    }
    else
    {
        dest_ds = build_adjust_data_set(dest_ds, maxh, maxh);
    }


    assert(dest_ds);


    for (int k = 0; k < dest_ds->nx; k++)
    {
        dest_ds->xd[k] = xmin + ((k - 25) * biny / 5);
    }

    // we add point to the histogram for each source dataset that contains a point at this abscisse
    for (int j = 0; j < src_ds->nx; ++j)
    {

        int cur_pt_median_idx = (int)(0.5 + ((src_ds->xd[j] - xmin) * 5) / biny) + 25;

        for (int k = cur_pt_median_idx - 25; k <= cur_pt_median_idx + 25; ++k)
        {
            if (k >= 0 && k < dest_ds->nx)
            {
                tmp = xmin + ((k - 25) * biny / 5) - src_ds->xd[j];
                tmp /= biny;
                tmp = exp(-tmp * tmp);
                dest_ds->yd[k] += tmp;
            }
        }
    }

    dest_ds->nx = maxh;
    dest_ds->ny = maxh;

    return dest_ds;
}

d_s *get_ds_histo_by_x(O_p *src_op, int pos, float biny)
{
    float maxh = 0;
    float ymin = -FLT_MAX;
    float ymax = FLT_MAX;
    int max_pts = INT_MIN;
    d_s *dest_ds = NULL;
    float tmp = 0;

    get_plot_y_range(src_op, &ymin, &ymax, NULL, &max_pts);
    maxh = (int)(0.5 + ((ymax - ymin) * 5) / biny);
    dest_ds = build_data_set(maxh, maxh);

    for (int k = 0; k < dest_ds->nx; k++)
    {
        dest_ds->xd[k] = ymin + ((k - 25) * biny / 5);
    }

    // we add point to the histogram for each source dataset that contains a point at this abscisse
    for (int j = 0; j < src_op->n_dat; ++j)
    {
        d_s *cur_src_ds = src_op->dat[j];
        int cur_idx = 0;

        while (cur_idx < cur_src_ds->nx && cur_src_ds->xd[cur_idx] < pos)
        {
            ++cur_idx;
        }

        // if the point exist on this data set, we add the point to the histogram, in a fuzzy
        // way by using a gaussian udistribution.
        if (cur_src_ds->xd[cur_idx] == pos)
        {
            int cur_pt_median_idx = (int)(0.5 + ((cur_src_ds->yd[cur_idx] - ymin) * 5) / biny) + 25;

            for (int k = cur_pt_median_idx - 25; k <= cur_pt_median_idx + 25; ++k)
            {
                if (k >= 0 && k < dest_ds->nx)
                {
                    tmp = ymin + ((k - 25) * biny / 5) - cur_src_ds->yd[cur_idx];
                    tmp /= biny;
                    tmp = exp(-tmp * tmp);
                    dest_ds->yd[k] += tmp;
                }
            }
        }

    }

    return dest_ds;
}

O_p *get_all_ds_points_by_x(pltreg *pr, O_p *src_op, d_s *x_count_ds)
{
    O_p     *points_op = NULL;
    d_s **points = NULL;
    int *ds_src_cursors = NULL;
    float ymin = FLT_MAX;
    float ymax = -FLT_MAX;
    int max_pts = INT_MIN;
    int maxh = 0;
    int src_count = 0;
    get_plot_y_range(src_op, &ymin, &ymax, NULL, &max_pts);
    ds_src_cursors = (int *) calloc(src_op->n_dat, sizeof(int));
    //printf("ymin %f, ymax %f\n", ymin, ymax);
    maxh += 50;
    src_count = x_count_ds->nx;
    points_op = create_and_attach_one_plot(pr, x_count_ds->nx, x_count_ds->nx, 0);
    uns_op_2_op_by_type(src_op, IS_Y_UNIT_SET, points_op, IS_X_UNIT_SET);
    points = allocate_output_histograms(points_op, maxh, src_count);
    points_op->title = strdup("histograms \n y values of all ds for a x value");

    // filling out all histograms
    for (int i = 0; i < src_count; ++i)
    {
        d_s *cur_slice = points[i];
        remove_all_point_from_data_set(cur_slice);
        float cur_slice_pos = x_count_ds->xd[i]; // x position of the current histogram into the src plot

        // we add point to the histogram for each source dataset that contains a point at this abscisse
        for (int j = 0; j < src_op->n_dat; ++j)
        {
            d_s *cur_src_ds = src_op->dat[j];
            int cur_idx = ds_src_cursors[j];

            while (cur_idx < cur_src_ds->nx && cur_src_ds->xd[cur_idx] < cur_slice_pos)
            {
                ++cur_idx;
            }

            // if the point exist on this data set, we add the point to the histogram, in a fuzzy
            // way by using a gaussian udistribution.
            if (cur_src_ds->xd[cur_idx] == cur_slice_pos)
            {
                add_new_point_to_ds(cur_slice, cur_src_ds->yd[cur_idx] , 1);
            }

            ds_src_cursors[j] = cur_idx;
        }

        set_ds_source(cur_slice, "Slice at t = %g", cur_slice_pos);
    }

    //for (int i = 0; i < histo_count; ++i)
    //{
    //    assert(histo_op->dat[i]->xd[0] == histo_op->dat[0]->xd[0]);
    //}
    return points_op;
}
O_p *get_all_ds_histo_by_x(pltreg *pr, O_p *src_op, d_s *x_count_ds, float biny)
{
    O_p     *histo_op = NULL;
    d_s **histos = NULL;
    int *ds_src_cursors = NULL;
    float ymin = FLT_MAX;
    float ymax = -FLT_MAX;
    int max_pts = INT_MIN;
    int maxh = 0;
    int histo_count = 0;
    float tmp = 0.;
    get_plot_y_range(src_op, &ymin, &ymax, NULL, &max_pts);
    ds_src_cursors = (int *) calloc(src_op->n_dat, sizeof(int));
    maxh = (int)(0.5 + ((ymax - ymin) * 5) / biny);
    //printf("ymin %f, ymax %f\n", ymin, ymax);
    maxh += 50;
    histo_count = x_count_ds->nx;
    histo_op = create_and_attach_one_plot(pr, maxh, maxh, 0);
    uns_op_2_op_by_type(src_op, IS_Y_UNIT_SET, histo_op, IS_X_UNIT_SET);
    histos = allocate_output_histograms(histo_op, maxh, histo_count);
    histo_op->title = strdup("histograms \n y values of all ds for a x value");

    // filling out all histograms
    for (int i = 0; i < histo_count; ++i)
    {
        d_s *cur_histo = histos[i];
        float cur_histo_pos = x_count_ds->xd[i]; // x position of the current histogram into the src plot

        // we fill the histogram abscisse
        for (int k = 0; k < cur_histo->nx; k++)
        {
            cur_histo->xd[k] = ymin + ((k - 25) * biny / 5);
        }

        // we add point to the histogram for each source dataset that contains a point at this abscisse
        for (int j = 0; j < src_op->n_dat; ++j)
        {
            d_s *cur_src_ds = src_op->dat[j];
            int cur_idx = ds_src_cursors[j];

            while (cur_idx < cur_src_ds->nx && cur_src_ds->xd[cur_idx] < cur_histo_pos)
            {
                ++cur_idx;
            }

            // if the point exist on this data set, we add the point to the histogram, in a fuzzy
            // way by using a gaussian udistribution.
            if (cur_src_ds->xd[cur_idx] == cur_histo_pos)
            {
                int cur_pt_median_idx = (int)(0.5 + ((cur_src_ds->yd[cur_idx] - ymin) * 5) / biny) + 25;

                for (int k = cur_pt_median_idx - 25; k <= cur_pt_median_idx + 25; ++k)
                {
                    if (k >= 0 && k < cur_histo->nx)
                    {
                        tmp = ymin + ((k - 25) * biny / 5) - cur_src_ds->yd[cur_idx];
                        tmp /= biny;
                        tmp = exp(-tmp * tmp);
                        cur_histo->yd[k] += tmp;
                    }
                }
            }

            ds_src_cursors[j] = cur_idx;
        }

        //we normalize the filled histogram dividing by the number of fuzzy point inserted.
        for (int k = 0; k < cur_histo->nx; k++)
        {
            cur_histo->yd[k] /= x_count_ds->yd[i];
        }

        set_ds_source(cur_histo, "Histogram at %g expo-convolution \\sigma = %g", cur_histo_pos, biny);
    }

    //for (int i = 0; i < histo_count; ++i)
    //{
    //    assert(histo_op->dat[i]->xd[0] == histo_op->dat[0]->xd[0]);
    //}
    return histo_op;
}

/**
 * @brief generate the dataset of point who share the same x from a plot containing several dataset
 *
 * @param op original plot
 * @param singlem number of single points
 * @param mavg average number of data set who share a particular x value
 * @param all_error does all dataset has errors bar ?
 * @param all_no_error does all dataset has not errors bar ?
 *
 * @return the resulting dataset
 */
d_s *generate_ds_of_points_with_same_x(const O_p *op, const int discard_point_count, int *singlem, float *mavg,
                                       bool *all_error, bool *all_no_error)
{
    int nx1, nt, singleml = 0;
    bool all_errorl = true;
    bool all_no_errorl = true;
    float mavgl;
    d_s *dstmp = NULL, *dsi1;

    if (op == NULL)
    {
        return NULL;
    }

    if (op->n_dat < 2)
    {
        return NULL;
    }

    // check that all ds hve same x coordinates
    dstmp = build_data_set(op->dat[0]->nx, op->dat[0]->nx);

    if (dstmp == NULL)
    {
        return NULL;
    }

    dstmp->nx = dstmp->ny = 0;
    nx1 = 0;
    nt = 0;

    for (int j = 0; j < op->n_dat; j++)
    {
        dsi1 = op->dat[j];
        all_errorl =  all_errorl && (dsi1->ye != NULL);
        all_no_errorl = all_no_errorl && (dsi1->ye == NULL);

        if (dsi1->nx > nx1)
        {
            nx1 = dsi1->nx;
        }

        nt += dsi1->nx;

        for (int k = 0; k < dsi1->nx; k++)
        {
            int i = 0;

            for (i = 0; i < dstmp->nx; i++)
            {
                if (dsi1->xd[k] == dstmp->xd[i])
                {
                    dstmp->yd[i] += 1;
                    break;
                }
            }

            if (i >= dstmp->nx)
            {
                add_new_point_to_ds(dstmp, dsi1->xd[k], 1);
            }
        }
    }

    singleml = 0;
    mavgl = 0;

    for (int i = 0 ; i < dstmp->nx; i++)
    {
        singleml += !!(dstmp->yd[i] <= 1);
        mavgl += dstmp->yd[i];
    }

    if (dstmp->nx > 0)
    {
        mavgl /= dstmp->nx;
    }

    if (discard_point_count > 0)
    {
        int i = 0;

        while (i < dstmp->nx)
        {
            if (dstmp->yd[i] < discard_point_count)
            {
                remove_point_from_data_set(dstmp, i);
            }
            else
            {
                ++i;
            }
        }
    }

    sort_ds_along_x(dstmp);

    if (singlem)
    {
        *singlem = singleml;
    }

    if (mavg)
    {
        *mavg = mavgl;
    }

    if (all_error)
    {
        *all_error = all_errorl;
    }

    if (all_no_error)
    {
        *all_no_error = all_no_errorl;
    }

    return dstmp;
}

void get_plot_y_range(const O_p *ops, float *out_ymin, float *out_ymax, int *out_min_points, int *out_max_points)
{
    int nds = ops->n_dat;
    d_s *dsi = NULL;
    float ymin = FLT_MAX;
    float ymax = -FLT_MAX;
    int min_pts = INT_MAX;
    int max_pts = 0;
    float cur_min = 0;
    float cur_max = 0;

    for (int k = 0; k < nds; k++)
    {
        dsi = ops->dat[k];
        cur_min = my_min(dsi->nx, dsi->ny);
        cur_max = my_max(dsi->nx, dsi->ny);

        if (max_pts < cur_max)
        {
            max_pts = cur_max;
        }

        if (min_pts > cur_min)
        {
            min_pts = cur_min;
        }

        for (int i = 0; i < dsi->nx; i++)
        {
            if (dsi->yd[i] > ymax)
            {
                ymax = dsi->yd[i];
            }

            if (dsi->yd[i] < ymin)
            {
                ymin = dsi->yd[i];
            }
        }
    }

    if (out_ymin)
    {
        *out_ymin = ymin;
    }

    if (out_ymax)
    {
        *out_ymax = ymax;
    }

    if (out_min_points)
    {
        *out_min_points = min_pts;
    }

    if (out_max_points)
    {
        *out_max_points = max_pts;
    }
}
















