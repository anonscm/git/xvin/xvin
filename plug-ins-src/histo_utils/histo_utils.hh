#pragma once

#include "xvin.h"
#include <vector>
std::vector<float> *get_local_maxima_x_sorted_data(d_s *data, float mthres);
std::vector<std::pair<float, float>> *get_local_maxima_pos_and_val_sorted_data(d_s *data, float mthres);

PXV_FUNC(d_s, *generate_ds_of_points_with_same_x, (const O_p *op, const int discard_point_count, int *singlem,
         float *mavg, bool *all_error, bool *all_no_error));
PXV_FUNC(int, average_all_ds_of_op_by_x_by_histo, (pltreg *pr, O_p *src_op, float biny, float keeph, float thres,
         int discard_point_thres,  int multi_peak));
PXV_FUNC(O_p, *get_all_ds_histo_by_x, (pltreg *pr, O_p *src_op, d_s *x_count_ds, float biny));
PXV_FUNC(O_p, *get_all_ds_points_by_x, (pltreg *pr, O_p *src_op, d_s *x_count_ds));
PXV_FUNC(void, get_plot_y_range, (const O_p *ops, float *out_ymin, float *out_ymax, int *out_min_points,
                                  int *out_max_points));
PXV_FUNC(int, non_linear_drift_correction_between_cycle_by_histogram_alignment, (pltreg *pr, O_p *src_op, float biny,
         int discard_point_thres, bool keep_intermediate_histo, bool keep_shift_data, bool keep_histo_mult_derivate_plot));
PXV_FUNC(int, non_linear_drift_correction_between_cycle_by_histogram_alignment_v2, (pltreg *pr, O_p *src_op,
         float biny,
         int poly_fit_degree,
         int zero_approx_iteration_count,
         int discard_point_thres,
         bool keep_intermediate_histo,
         bool keep_shift_data,
         bool keep_histo_mult_derivate_plot));
PXV_FUNC(float, sum_y_along_x_ds, (const d_s *src_ds));
PXV_FUNC(float, find_min_y_value, (d_s *ds));
PXV_FUNC(void, find_bigest_y_monotone_range, (O_p *op, d_s *src_ds, int *out_min_idx, int *out_max_idx));
PXV_FUNC(void, find_bigest_y_almost_linear_range, (O_p *op, d_s *src_ds, int *out_min_idx, int *out_max_idx));
PXV_FUNC(d_s, *get_histo_convolution, (d_s *src_ds, d_s *dest_ds, float biny, float ymin, float ymax));
PXV_FUNC(float, get_ds_fitness_derivate_projection_with_offset, (d_s *src_ds, d_s *derivate_ds, float ds2_offest));
PXV_FUNC(int, find_neighboors_x_of_zero, (d_s *src_ds, float *low_bound, float *up_bound));

