/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _CUT_DS_C_
#define _CUT_DS_C_

# include "allegro.h"
# include "xvin.h"

/* If you include other plug-ins header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "cut_ds.h"


int do_cut_ds_rescale_data_set(void)
{
	register int i, j, k;
	O_p *op = NULL;
	int nf,np, nfds, nds;
	static int np_chunk = 512, n_chunk = 2, mode = 0;
	d_s *ds, *dsi;
	pltreg *pr = NULL;
	char ask[1024];


	if(updating_menu_state != 0)	return D_O_K;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine cut a data set in pieces of equal size");
	}
	/* we first find the data that we need to transform */
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
		return win_printf_OK("cannot find data");
	nf = dsi->nx;	/* this is the number of points in the data set */
	np = (mode == 0) ? n_chunk : np_chunk; 
	sprintf(ask,"You are going to cut the %d data set of this plot\n"
		"having %d points in pieces\n"
		"You can either choose the number of pieces %%R or the size of each piece %%r\n"
		"Select now the number of pieces or the size of each piece %%d",op->cur_dat,nf);
	i = win_scanf(ask,&mode,&np);
	if (i == CANCEL)	return OFF;
	if (mode == 0) 
	  {
	    n_chunk = np;
	    nds = np;
	    nfds = (int)(nf/np);
	  }
	else    
	  {
	    np_chunk = np;
	    nfds = np;
	    nds = (int)(nf/np);
	  }
	for (k = 0; k < nf; k += nfds)
	  {
	    if ((ds = create_and_attach_one_ds(op, nfds, nfds, 0)) == NULL)
		return win_printf_OK("cannot create plot !");

	    for (j = 0; j < nfds; j++)
	      {
		ds->yd[j] = dsi->yd[k+j];
		ds->xd[j] = dsi->xd[k+j];
	      }

	/* now we must do some house keeping */
	    inherit_from_ds_to_ds(ds, dsi);
	    ds->treatement = my_sprintf(ds->treatement,"Piece %d starting at point %d ending at ",(int)(k/nfds),k,k+j);
	  }
	/* refisplay the entire plot */
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
}

MENU *cut_ds_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"Cut data set in pieces", do_cut_ds_rescale_data_set,NULL,0,NULL);
	return mn;
}

int	cut_ds_main(int argc, char **argv)
{
	add_plot_treat_menu_item ( "cut_ds", NULL, cut_ds_plot_menu(), 0, NULL);
	return D_O_K;
}

int	cut_ds_unload(int argc, char **argv)
{
	remove_item_to_menu(plot_treat_menu, "cut_ds", NULL, NULL);
	return D_O_K;
}
#endif

