#ifndef _CUT_DS_H_
#define _CUT_DS_H_

PXV_FUNC(int, do_cut_ds_rescale_plot, (void));
PXV_FUNC(MENU*, cut_ds_plot_menu, (void));
PXV_FUNC(int, do_cut_ds_rescale_data_set, (void));
PXV_FUNC(int, cut_ds_main, (int argc, char **argv));
#endif

