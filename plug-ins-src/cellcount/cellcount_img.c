#ifndef CELLCOUNT_IMG_C_
#define CELLCOUNT_IMG_C_

# include "allegro.h"
# include "xvin.h"
# include "cellcount.h"
# include "cellcount_img.h"

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

# include "../fft2d/fft2d.h"
# include "../trackBead/real_time_avg.h"
# include "../fluobead/fluobead.h"


/************************************/
/*  Simulate a new movie            */
/************************************/
double choose_random_radius(gsl_rng *rng, int r_distrib, double r_m, double r_s, double r_ci, double r_off, double r_In)
{
  double r = 0;                      // r is the particle radius

  if (r_distrib == 0 || r_distrib == 2)
    {
      r += r_m;
      if (r_s > CC_ERROR)
	r += gsl_ran_gaussian(rng, r_s);
      r = (r < 0) ? 0 : r;
    }
  if (r_distrib == 1 || r_distrib == 2)
    {
      if (r_ci > CC_ERROR)  // else exp = 0
	r += (gsl_ran_exponential(rng, 1/r_ci) + r_off) * r_In;
    }
  if (r_distrib == 3)
    {
      r = gsl_ran_lognormal(rng, r_m, r_s);
    }

  return r;
}

int add_gaussian_peak_on_oi(O_i *oi, double x, double y, double z, double s)
{
  int xi, yi, i, j, pk;
  double tmp, itmp, jtmp, inc;
  const double pi = 3.14159;//2653589793;
  
  xi = (int) floor(x);
  yi = (int) floor(y);

  for (i = xi-(int)(2*s); i < xi+(int)(2*s); i++)
    {
      if (i<0 || i>=oi->im.nx)
	continue;
      
      for (j = yi-(int)(2*s); j < yi+(int)(2*s); j++)
	{
	  if (j<0 || j>=oi->im.ny)
	    continue;

	  itmp = ((double) i - x) / s;
	  jtmp = ((double) j - y) / s;
	  inc = (double) 1 / s;

	  tmp = erf(itmp + inc) - erf(itmp);
	  tmp *= erf(jtmp + inc) - erf(jtmp);
	  tmp *= pi / 2 * (z / 2);
	  //win_printf_OK("i %d   j %d\nxi %g   yi %g\nxi+inc %g   yi+inc %g\ntmp %g", i, j, itmp, jtmp, itmp+inc, jtmp+inc, tmp);

	  pk = (int) oi->im.pixel[j].ch[i] + (int) tmp;
	  oi->im.pixel[j].ch[i] = pk < 256 ? (unsigned char) pk : 255;
	}
    }
  return 0;
}

int simul_beads_on_one_frame(O_i *oi, int nx, int ny, double sbm, double conc, double sbd_m, double sbd_s, int r_distrib, double exponent, double r_m, double r_s, double r_ci, double r_off, double r_In, float aggreg)
{
  const gsl_rng_type *T;
  gsl_rng *rng;
  static unsigned long int seed;
  double lambda;

  int i, j, nz, xi, yi;
  double x, y, z, r, v, peak, sbd, rn;
  unsigned int n;

  // Erase frame content
  for (j = 0; j < ny; j++)
    for (i = 0; i < nx; i++)
      oi->im.pixel[j].ch[i] = 0;

  // Init poisson generator
  // from http://www.gnu.org/software/gsl/manual/html_node/Random-Number-Distribution-Examples.html
  gsl_rng_env_setup(); 
  T = gsl_rng_default; 
  rng = gsl_rng_alloc (T);
  seed += 1;
  gsl_rng_set(rng, seed);

  nz = (int) sbm * Z_DEPTH;
  lambda = (double) nx * ny * nz;
  lambda *= conc;

  // Run simul
  n = gsl_ran_poisson (rng, lambda);  // nb of particle on this frame
  for (i = 0; i < n; i++)
    {
      x = gsl_ran_flat(rng, (double) 0, (double) nx);
      y = gsl_ran_flat(rng, (double) (-ny/2), (double) ny/2);	
      z = gsl_ran_flat(rng, (double) (-nz/2), (double) nz/2);
      
      r = choose_random_radius(rng, r_distrib, r_m, r_s, r_ci, r_off, r_In);
      // account for particle aggregation
      if (aggreg > CC_ERROR)
	{
	  rn = gsl_ran_flat(rng, 0, 1);
	  if (rn < (double) aggreg)
	    r += choose_random_radius(rng, r_distrib, r_m, r_s, r_ci, r_off, r_In);
	}

      v = pow(r, exponent) * 255;
      peak = (y/sbm)*(y/sbm) + (z/sbm)*(z/sbm);
      peak = v * exp(-peak/2);
      //win_printf("r %g\npeak %g", r, peak);
      //peak = (double) 255 * exp(-peak/2);
      
      xi = (int) floor(x);
      yi = (int) (floor(y) + ny/2);
      
      if (xi<0 || yi<0 || xi>=nx || yi>=ny)
	win_printf_OK("Warning\nxi = %d    yi = %d\nx = %g   y = %g", xi, yi, x, y);
      
      //oi->im.pixel[yi].ch[xi] += (unsigned char) peak;
      sbd = sbd_m;
      if (sbd_s > CC_ERROR)
	sbd += gsl_ran_gaussian(rng, sbd_s);
      sbd = (sbd < 0) ? 0 : sbd;

      add_gaussian_peak_on_oi(oi, x, (double) y + ny/2, peak, sbd);
    }
  gsl_rng_free(rng);

  return 0;
}

int do_simul_beam(void)
{
  imreg *imr = NULL;
  O_i *oi;

  static int nf = 32, nx = 768, ny = 480, intensityDistrib = 0;
  static float exponent = 2, fsbm = 20, fconc = 3e-4;
  static float fsbdm = 1.2, fsbds = 0, fr_m = 0.5, fr_s = 0.15;
  static float fr_ci = 2, fr_off = 0, fr_In = 150, aggreg = 0.0;
  int i, f;

  // The beam is centered on y=0, z=0
  // y width is set by user
  // z width is constant (corresponds to all field depth) = Z_DEPTH * sigma(beam)

  if(updating_menu_state != 0)
    {
      //add_keyboard_short_cut(0, KEY_G, 0, do_process_histo_float);
      return D_O_K;
    }


  // Request params
  i = win_scanf("Simulation parameters\n"
		"nf %5d   x %5d  y %5d\n\n"
		"sigma beam %5f  conc %5f\n\n"
		"Size distribution:\n%R gaussian  %r exponential  %r gaussian + exp\n"
		"%r lognormal\n\n"
		"Exponent of intensity dependency to radius %5f\n"
		"([0, 1] radius is mapped to intensity in [0, 255])\n\n"
		"bead radius mean %5f\n"
		"bead radius std dev %5f\n\n"
		// I ~ (E(1/ci) + m) * In
		"bead characteristic radius %5f\n"  //ci
		"bead radius exponential offset %5f\n"  // m
		"bead standard radius %5f\n\n"  // In
		"sigma bead mean %5f\n"
		"sigma bead std dev %5f\n\n"
		"Aggregation probability %5f\n",
		&nf, &nx, &ny, &fsbm, &fconc, &intensityDistrib, &exponent, &fr_m, &fr_s, &fr_ci, &fr_off, &fr_In, &fsbdm, &fsbds, &aggreg);
  if (i == CANCEL) return 0;

  imr = find_imr_in_current_dialog(NULL);
  if (imr == NULL) //(ac_grep(cur_ac_reg,"%im",&imr) != 1)
    imr = create_imreg_with_movie(nx, ny ,IS_CHAR_IMAGE, nf);
  else 
    {
      oi = create_and_attach_movie_to_imr(imr, nx, ny, IS_CHAR_IMAGE, nf);
      imr->cur_oi = imr->n_oi - 1;
      imr->one_i = imr->o_i[imr->cur_oi];
    }
  if (imr == NULL || ((oi = imr->one_i) == NULL))	
    return win_printf_OK("Cannot allocate image!");
  
  switch_project_to_this_imreg(imr);
  map_pixel_ratio_of_image_and_screen(oi, (float)1, (float)1);
  find_zmin_zmax(oi);
  smooth_interpol = 0;


  // Run simul
  for (f = 0; f < oi->im.n_f; f++)
    {
      switch_frame(oi, f);
      simul_beads_on_one_frame(oi, nx, ny, (double) fsbm, (double) fconc, (double) fsbdm, (double) fsbds, intensityDistrib, (double) exponent, (double) fr_m, (double) fr_s, (double) fr_ci, (double) fr_off, (double) fr_In, aggreg);
    }

  find_zmin_zmax(oi);
  refresh_image(imr, UNCHANGED); 
  return 0;
}


/************************************/
/*         Raw histogram            */
/************************************/

int append_raw_histo(O_i *oi, int fr, int *histo_raw, int xmin, int xmax, int ymin, int ymax)
// computes the pixel values histogram of ROI (xmin,xmax ymin,ymax) in frame fr of image oi
// and appends it to histo_raw.
// works only with char images.
{
  int i, j, itmp;

  if (oi->im.data_type != IS_CHAR_IMAGE)
    return win_printf("Histogram can be produced only with char images");

  for (i = xmin; i <xmax; i++)
    {
      for (j = ymin; j < ymax; j++)
	{
	  itmp = (int) oi->im.pxl[fr][j].ch[i];
	  histo_raw[0xff&itmp] += 1;
	}
    }
  return(0);
}

int display_img_histo(void)
// this is the gui function to compute an image histogram
{
  imreg *imr;
  O_i *oi;
  O_p *op;
  d_s *dsbar, *dsline;

  static int xmin = 0, xmax = 0, ymin = 0, ymax = 0;
  static int process_all = 1;

  int i;
  int histo_raw[256];

  if(updating_menu_state != 0)
    {
      add_keyboard_short_cut(0, KEY_H, 0, display_img_histo);
      return D_O_K;
    }

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)
    return win_printf("Cannot find current image.");	

  if (xmax == 0 || xmax > oi->im.nx)
    xmax = oi->im.nx;
  if (ymax == 0 || ymax > oi->im.ny)
    ymax = oi->im.ny;
  i = win_scanf("Compute histogram between\n\n"
	    "xmin %5d  xmax %5d\nymin %5d  ymax %5d",
	    &xmin, &xmax, &ymin, &ymax);
  if (i == CANCEL) return 0;

  if (oi->im.n_f > 1)
    i = win_scanf("%b Process all frames", &process_all);
  if (i == CANCEL) return 0;

  // Reset histo
  for (i = 0; i < 256; i++)
    histo_raw[i] = 0;

  // Compute histo
  if (process_all)
    for (i = 0; i < oi->im.n_f; i++)
      append_raw_histo(oi, i, histo_raw, xmin, xmax, ymin, ymax);
  else
    append_raw_histo(oi, oi->im.c_f, histo_raw, xmin, xmax, ymin, ymax);

  // Create ds
  if ((op = create_and_attach_op_to_oi(oi, 1024, 1024, 0, 0)) == NULL)
    return win_printf("I can't create plot !");
  dsbar = op->dat[0]; // the first data set was already created
  if ((dsline = create_and_attach_one_ds(op, 256, 256, 0)) == NULL)
    return win_printf("I can't create plot !");

  // Store histo in ds
  set_plot_y_log(op);
  display_histo_in_op(histo_raw, dsbar, dsline, 256, 1.0);
  oi->need_to_refresh = ALL_NEED_REFRESH;

  return 0;
}

/************************************/
/*  Band pass movie                 */
/************************************/
/* int copy_one_differential_subset_of_movie(O_i *dest, O_i *ois, int x_start, int y_start, int delay) */
/* { */
/*   union pix *pd, *ps, *psd;  */
/*   int i, j, onx, ony, donx, dony; */

/*   if (ois == NULL || dest == NULL)                       return 1; */
/*   if (ois->im.data_type != IS_CHAR_IMAGE)           return 2; */
/*   if (dest->im.data_type != IS_FLOAT_IMAGE)           return 2; */
/*   pd = dest->im.pixel; */
/*   ps = ois->im.pixel; */
/*   i = ois->im.c_f - delay;    */
/*   for (;i < 0; i += ois->im.n_f);  // rolling buffer */
/*   psd = ois->im.pxl[i]; */
/*   onx = ois->im.nx;  */
/*   ony = ois->im.ny; */
/*   donx = dest->im.nx;  */
/*   dony = dest->im.ny; */
/*   if (x_start < 0 || y_start < 0)                        return 3;                      */
/*   if ((x_start + donx > onx) || (y_start + dony > ony))  return 4;                      */

/*   if (ois->im.data_type == IS_CHAR_IMAGE) */
/*     { */
/*       for (i = 0; i< dony ; i++) */
/* 	{ */
/* 	  for (j = 0; j < donx ; j++) */
/* 	      pd[i].fl[j] = ps[i+y_start].ch[j+x_start] - psd[i+y_start].ch[j+x_start]; */
/* 	} */
/*     } */

/*   return 0; */
/* } */

int draw_square_roi(imreg *imr, int x0, int y0, int wx, int wy)
{
  int xp, yp;
  xp = x_imdata_2_imr((imreg*)imr, (int)x0);
  yp = y_imdata_2_imr((imreg*)imr, (int)y0)+16;

/*   my_set_window_title("x0 %d    xp %d    wx %d\n" */
/* 	     "y0 %d    yp %d    wy %d", x0, xp, wx, y0, yp, wy); */
  rect(screen, xp, yp, xp+wx, yp-wy, makecol(0,0,255));

  return 0;
}

int do_draw_roi(void)
{
  imreg *imr;
  static int x0=20, wx=400, y0=100, wy=30;

  if(updating_menu_state != 0)
    return D_O_K;
  if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
    return win_printf("Cannot fing image region");

  win_scanf("x0 %d    wx %d\n"
	    "y0 %d    wy %d", &x0, &wx, &y0, &wy);

  draw_square_roi(imr, x0, y0, wx, wy);
  return 0;
}

int copy_one_subset_of_movie_with_bg(O_i *dest, O_i *ois, O_i *oibg, int x_start, int y_start)
{
  // copy a subset of the current frame of ois in dest (fit  dest size) removing the level
  // of corresponding pixels in oibg
  // Ignore backgroung if oibg == NULL
  union pix *pd, *ps, *psd; 
  int i, j, onx, ony, donx, dony;

  if (ois == NULL || dest == NULL)                       return 1;
  if (ois->im.data_type != IS_CHAR_IMAGE)                return 2;
  if (dest->im.data_type != IS_FLOAT_IMAGE)              return 2;

  pd = dest->im.pixel;
  ps = ois->im.pixel;
  onx = ois->im.nx; 
  ony = ois->im.ny;
  donx = dest->im.nx; 
  dony = dest->im.ny;
  if (x_start < 0 || y_start < 0)                        return 3;                     
  if ((x_start + donx > onx) || (y_start + dony > ony))  return 4;                     

  if (oibg == NULL)
    {
      for (i = 0; i< dony ; i++)
	{
	  for (j = 0; j < donx ; j++)
	    pd[i].fl[j] = (float) ps[i+y_start].ch[j+x_start];
	}
    }
  else
    {
      if (oibg->im.data_type != IS_FLOAT_IMAGE)              return 2;
      if ((ois->im.nx != oibg->im.nx) || (ois->im.ny != oibg->im.ny)) return 3;
      psd = oibg->im.pixel;
      
      for (i = 0; i< dony ; i++)
	{
	  for (j = 0; j < donx ; j++)
	    pd[i].fl[j] = (float) ps[i+y_start].ch[j+x_start] - psd[i+y_start].fl[j+x_start];
	}
    }

  return 0;
}

int split_before_bp(O_i *ois, O_i *oim, O_i *oibg, int nf, int fsize, int x0, int y0, int wx, int wy)
// This function split the current frame of ois into fsize x fsize parts and store it in oim,
// possibly substracting a background image
{
  int im, x, y;

  for (im = 0; im < nf; im++)
    {
      switch_frame(oim,im);
      find_min_subset_pos_ROI(ois, fsize, fsize, im, &x, &y, x0, y0, wx, wy);
      copy_one_subset_of_movie_with_bg(oim, ois, oibg, x, y);
      //win_printf_OK("small image %d\nx %d y %d\n",j,x,y);
    }
  return 0;
}

int reconstruct_after_bp(O_i *ois, O_i *oifb, O_i *oid, int nf, int fsize, int wx, int wy, int x_offset, int y_offset)
// This function rearrange the band passed small images in oifb into a filtered global image
// stored in the current frame of oid, based on ois splitting
{
  int i, j, im;
  int x, y, xb, yb, wb, hb;

  for (im = 0; im < nf; im++)
    {
      switch_frame(oifb, im);
      find_min_subset_pos_ROI(ois, fsize, fsize, im, &x, &y, 0, 0, wx, wy);
      find_min_subset_good_pixel_pos_ROI(ois, fsize, fsize, im, &xb, &yb, &wb, &hb, wx, wy);
      for (i = 0; i < hb; i++)
	{
	  for (j = 0; j < wb; j++)
	    {
	      oid->im.pixel[yb+i+y_offset].fl[xb+j+x_offset] = oifb->im.pixel[yb-y+i].fl[xb-x+j];
	    }
	}
    }
  return 0;
}


int produce_histo_from_bp(O_i *oifb, int nf, int *histo_bp, int levels, float factor)
{
  int i, j, im, itmp;
  int xmin = 0, xmax = 0, ymin = 0, ymax = 0;
  float tmp;

  xmin = oifb->im.nx / 8;
  xmax = 7 * oifb->im.nx / 8; 
  ymin = oifb->im.ny / 8;
  ymax = 7 * oifb->im.ny / 8;
  
  for (im = 0, tmp = 0, itmp = 0; im < nf; im++)
    {
      // compute RMS of center of frame
      for (j = ymin; j < ymax; j++)
	{
	  for (i = xmin; i < xmax; i++)
	    {
	      tmp += oifb->im.pxl[im][j].fl[i] * oifb->im.pxl[im][j].fl[i];
	      itmp++;
	    }
	}
      tmp /= (float) itmp;
      tmp = sqrt(tmp) * factor;
      itmp = (tmp < (float) levels) ? (int) tmp : levels-1;
      
      histo_bp[itmp]++;
    }
  return 0;
}


int save_control_img(O_i *oifb, O_i *oic, int levels, float bp_factor, int control_factor)
{
  // This function compute rms of a bp filtered image and store it in the corresponding
  // frame of control movie oic (frame number = rms)
  int i, j, im, itmp;
  int xmin = 0, xmax = 0, ymin = 0, ymax = 0;
  float tmp;

  xmin = oifb->im.nx / 8;
  xmax = 7 * oifb->im.nx / 8; 
  ymin = oifb->im.ny / 8;
  ymax = 7 * oifb->im.ny / 8;

  for (im = 0, tmp = 0, itmp = 0; im < oifb->im.n_f; im++)
    {
      // compute RMS of center of frame
      for (j = ymin; j < ymax; j++)
	{
	  for (i = xmin; i < xmax; i++)
	    {
	      tmp += oifb->im.pxl[im][j].fl[i] * oifb->im.pxl[im][j].fl[i];
	      itmp++;
	    }
	}
      tmp /= (float) itmp;
      tmp = sqrt(tmp) * bp_factor;
      itmp = (tmp < (float) levels) ? (int) tmp : levels-1;

      //histo_bp[itmp]++;

      // Save control image
      itmp /= control_factor;
      for (i = 0; i < oic->im.nx ; i++)
	{
	  for (j = 0; j < oic->im.ny ; j++)
	    oic->im.pxl[itmp][j].fl[i] =  oifb->im.pxl[im][j].fl[i];
	}
    }
  oic->need_to_refresh = ALL_NEED_REFRESH;

  return 0;
}


int transfer_labels(O_i *ois, O_i *oid, int rot, int fwidth, int z0)
{
  int f, l, x, y;
  struct screen_label *s_l, *s_ld;
  
  for (f = 0; f < ois->im.n_f; f++)
    {
      switch_frame(ois, f);
      switch_frame(oid, f);
      for (l = 0; l < ois->im.n_sl[ois->im.c_f]; l++)
	{
	  s_l = ois->im.s_l[ois->im.c_f][l];
	  x = (rot == 0) ? s_l->xla : s_l->xla - z0 + fwidth/2;
	  y = (rot == 0) ? s_l->yla - z0 + fwidth/2 : s_l->yla;
	  //my_set_window_title("frame %d  label %d  x %5f  y %5f", f, l, x, y);
	  s_ld = add_screen_label(oid, x, y, 0, NULL, draw_ecoli_label);
	  s_ld->user_val = s_l->user_val;
	}
    }
  return 0;
}

int display_histo_in_op(int *histo, d_s *dsbar, d_s *dsline, int levels, float factor)
{
  int i, j;
  
  for (i = 0; i < 4*levels; i+=4)
    {
      j = i/4;
      dsbar->xd[i] = dsbar->xd[i+1] = ((float) j - 0.4) / factor;
      dsbar->xd[i+2] = dsbar->xd[i+3] = ((float) j + 0.4) / factor;
      dsline->xd[j] = (float) j / factor;
      dsbar->yd[i] = dsbar->yd[i+3] = 0;
      dsbar->yd[i+2] = dsbar->yd[i+1] = histo[j];
      dsline->yd[j] = histo[j];
    }
  return 0;
}

int do_bp_filter()
{
  imreg *imr;
  O_i *ois, *oibg, *oim, *oif, *oifb, *oic = NULL, *oid;
  O_i *apo;
  O_p *ophbp = NULL, *opsc = NULL;
  d_s *dsx = NULL, *dsy = NULL, *dssc = NULL, *dssci = NULL;

  static int is_histo = 1, is_oic = 1, is_scatter = 0, debug = 0;
  static int rot=0, z0=240, fsize=16, fwidth = 40, do_rm_bg = 0;
  float lp = 3, lw = 1, hp = 6, hw = 2;
  int i, nf, cf, nlab = 0;
  int x0, y0, wx, wy, xc, yc;
  int cx, cy, nx0, ny0;
  int histo_bp[BP_LEVELS];

  unsigned long t_c, to = 0;

  if(updating_menu_state != 0)	return D_O_K;

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine filters the image");
    }
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      win_printf("Cannot find image");
      return D_O_K;
    }
  if (ois == NULL || ois->im.data_type != IS_CHAR_IMAGE)	
    {
      win_printf("images must be unsigned char");
      return D_O_K;
    }

  // Ask params
  //  screen; ???
  z0 = (rot==0) ? (ois->im.ny/2) : (ois->im.nx/2);
  i = win_scanf("h/v (h:0 v:1) %10d\nbeam heigth %10d\n"
		"Remove avg as bg %b\n\n"
		"Fourier movie\nbin size %12d width %12d\n"
		"Band-pass filter cutoff lowpass  %10flowpass width %10f\n"
		"cutoff highpass  %10fhighpass width %10f\n\n"
		"Create histo %b  Store control images %b\n"
		"Create scatter plot %b",
		&rot,&z0,&do_rm_bg,&fsize,&fwidth,&lp,&lw,&hp,&hw,&is_histo,&is_oic,&is_scatter);
  if (i == CANCEL) return 0;

  if (is_scatter == 1)
    {
      // Check if any label
      nlab = get_number_screen_labels(ois);
      win_printf_OK("# labels  %d", nlab);
      if (nlab == 0)
	return win_printf_OK("You must have labels on your\n"
			     "movie to display a scatter plot.");
    }

  xc = (rot==0) ? (ois->im.nx/2) : z0;
  yc = (rot==0) ? z0 : (ois->im.ny/2);
  x0 = (rot==0) ? 0 : (xc - fwidth/2);
  y0 = (rot==0) ? (yc - fwidth/2) : 0;
  wx = (rot==0) ? ois->im.nx : fwidth;
  wy = (rot==0) ? fwidth : ois->im.ny;
  
/*   w_avg = 2.3; */
/*   w_bp = 0.45; */
  
  nf = compute_nb_of_min_subset_ROI(ois, fsize, fsize, x0, y0, wx, wy);
  // display nb of small images
  cx = 4*(wx - fsize);
  nx0 = cx/(3*fsize);
  nx0 += (cx%(3*fsize)) ? 2 : 1;
  cy = 4*(wy - fsize);
  ny0 = cy/(3*fsize);
  ny0 += (cy%(3*fsize)) ? 2 : 1;
  win_printf_OK("Nb of small images %d (%d x %d)", nf, nx0, ny0);


  // Allocate treatment images
  apo = create_appodisation_float_image(fsize, fsize, 0.05, 0);
  if (apo == NULL)	return win_printf_OK("Could not create apodisation image!");
      
/*   desapo = create_appodisation_float_image(fsize, fsize, 0.05, 1); */
/*   if (desapo == NULL)	return win_printf_OK("Could not create apodisation image!"); */

  if (do_rm_bg == 0)
    oibg = NULL;
  else
    {
      if ((oibg = create_and_attach_oi_to_imr(imr, ois->im.nx, ois->im.ny, IS_FLOAT_IMAGE)) == NULL)
	return win_printf_OK("I can't create image !");      
      set_z_black_z_white_values(oibg, -1, 3);
      set_zmin_zmax_values(oibg, -1, 3);
      set_im_title(oibg, "Background image");
    }

  if ((oim = create_and_attach_movie_to_imr(imr, fsize, fsize, IS_FLOAT_IMAGE, nf)) == NULL)
    return win_printf_OK("I can't create image !");      
  set_z_black_z_white_values(oim, -1, 25);
  set_zmin_zmax_values(oim, 0, 25);
  set_im_title(oim, "Subset images");

  if ((oif = create_and_attach_movie_to_imr(imr, fsize, fsize/2, IS_COMPLEX_IMAGE, nf)) == NULL)
    return win_printf_OK("I can't create image !");
  oif->height *= 0.5;
  set_z_black_z_white_values(oif, -10, 0);
  set_zmin_zmax_values(oif, -10, 0);

  if ((oifb = create_and_attach_movie_to_imr(imr, fsize, fsize, IS_FLOAT_IMAGE, nf)) == NULL)
    return win_printf_OK("I can't create image !");
  set_z_black_z_white_values(oifb, -10, 20);
  set_zmin_zmax_values(oifb, -10, 20);

  if (is_oic == 1)
    {
      if ((oic = create_and_attach_movie_to_imr(imr, fsize, fsize, IS_FLOAT_IMAGE, (int) (BP_LEVELS))) == NULL)
	return win_printf_OK("I can't create image !");
      set_z_black_z_white_values(oic, -10, 40);
      set_zmin_zmax_values(oic, -10, 40);
      set_im_title(oic, "Control images");
    }

  if ((oid = create_and_attach_movie_to_imr(imr, wx, wy, IS_FLOAT_IMAGE, ois->im.n_f)) == NULL)
    return win_printf_OK("I can't create image !");
  map_pixel_ratio_of_image_and_screen(oid, 1, 1);
  set_z_black_z_white_values(oid, -10, 20);
  set_zmin_zmax_values(oid, -10, 20);


/*   if ((oi = create_and_attach_oi_to_imr(imr, (int)BP_LEVELS, wy, IS_LINT_IMAGE)) == NULL) */
/*     return win_printf_OK("I can't create image !"); */
/*   oi->oi_idle_action = oi_hbp_idle_action; */
/*   map_pixel_ratio_of_image_and_screen(oi, 1, 1); */
/*   //set_z_black_z_white_values(oi, -10, 40); */
/*   set_zmin_zmax_values(oi, 0, 1000); */
/*   real_time_info->oihbp = oi; */


  // Allocate treatment plots
  if (is_histo == 1)
    {
      if ((ophbp = create_and_attach_op_to_oi(oid, BP_LEVELS*4, BP_LEVELS*4, 0, 0)) == NULL)
	return win_printf_OK("I can't create plot !");
      dsx = ophbp->dat[0]; // the first data set was already created
      set_ds_source(dsx, "Bar Histogram all band-passed pixels");

      if ((dsy = create_and_attach_one_ds(ophbp, BP_LEVELS, BP_LEVELS, 0)) == NULL)
		return win_printf_OK("I can't create plot !");
      set_ds_source(dsy, "Histogram all band-passed pixels");

      for (i = 0; i < BP_LEVELS; i++)
	  histo_bp[i] = 0;
    }
  if (is_scatter == 1)
    {
      if ((opsc = create_and_attach_op_to_oi(oid, nlab, nlab, 0, 0)) == NULL)
	return win_printf_OK("I can't create plot !");
      dssc = opsc->dat[0]; // the first data set was already created
      dssc->nx = 0;
      set_ds_source(dssc, "Scatter plot of intensity before and after bp filtering");

      if ((dssci = create_and_attach_one_ds(opsc, nlab, nlab, 0)) == NULL)
		return win_printf_OK("I can't create plot !");
      dssci->nx = 0;
      set_ds_source(dssci, "Scatter plot of intensity before and after bp filtering (interpolated)");
    }


  // Do filtering
  t_c = my_uclock();
  for (cf = 0; cf < ois->im.n_f; cf++)
    {
      switch_frame(ois, cf);
      switch_frame(oid, cf);
      split_before_bp(ois, oim, oibg, nf, fsize, x0, y0, wx, wy);

      forward_fftbt_2d_im_appo(oif, oim, 0, screen, apo, 0);
      //my_set_window_title("Treating image %d after fft",ims);
      band_pass_filter_fft_2d_im(oif, oif,  0, lp,  lw,  hp,  hw, 0, 0);
      //my_set_window_title("Treating image %d after band pass ",ims);
      backward_fftbt_2d_im_desappo(oifb, oif, 0, screen, NULL, 0);
      //my_set_window_title("Treating image %d after back",ims);

      reconstruct_after_bp(ois, oifb, oid, nf, fsize, wx, wy, 0, 0);
      if (is_histo == 1)
	produce_histo_from_bp(oifb, nf, histo_bp, BP_LEVELS, (float) BP_FACTOR);
      if (is_oic)
	save_control_img(oifb, oic, BP_LEVELS, (float) BP_FACTOR, 1);
    }
  t_c = my_uclock() - t_c;
  if (to == 0) to = MY_UCLOCKS_PER_SEC;
  win_printf_OK("FFT took %g s",(double)t_c/to);

  transfer_labels(ois, oid, rot, fwidth, z0);
  if (is_histo == 1)
    {
      display_histo_in_op(histo_bp, dsx, dsy, BP_LEVELS, (float) BP_FACTOR);
      if (ophbp != NULL) ophbp->need_to_refresh = 1;
    }
  if (is_scatter == 1)
    {
      scatter_plot_bp_peaks(oid, dssc, dssci, rot, debug);
      if (opsc != NULL) opsc->need_to_refresh = 1;
    }

  oim->need_to_refresh = ALL_NEED_REFRESH;
  oif->need_to_refresh = ALL_NEED_REFRESH;
  oifb->need_to_refresh = ALL_NEED_REFRESH;
  oid->need_to_refresh = ALL_NEED_REFRESH;

  return D_O_K;
}



/************************************/
/*   Iterate simul and analysis     */
/************************************/

int do_simul_and_filter_beam(void)
{
  imreg *imr = NULL;
  pltreg *pr = NULL;
  static int oi_nf = 32, nx = 768, ny = 40, conc_step = 5, sbds_step = 1, rs_step = 1, aggreg_step = 1, nrep = 1, keep_errbars = 0;
  static float exponent = 2, fsbm = 20, fsbdm = 1.2, sbds_ini = 0.2, sbds_factor = 2;
  static float fr_m = 0.5, rs_ini = 0.15, rs_factor = 1.5;
  static float conc_ini = 3e-4, conc_factor = 0.1;
  static float aggreg_ini = 0.0, aggreg_factor = 10;
  float conc, fsbds, fr_s, aggreg;
  int i, c, f, s, r, agg;

  O_i *ois, *oim, *oif, *oifb, *oid;
  O_i *apo;
  O_p *ophbp = NULL, *opallhbp = NULL, *opl;
  d_s *ds = NULL, *dsx = NULL, *dsy = NULL;
  d_s *dsl0 = NULL, *dslm = NULL;

  static int rot=0, z0=240, fsize=16, fwidth = 40;
  float lp = 3, lw = 1, hp = 6, hw = 2;
  int nr, nf, cf;
  int x0, y0, wx, wy, xc, yc;
  int cx, cy, nx0, ny0;
  int histo_bp[BP_LEVELS];

  //unsigned long t_c, to = 0;

  char fullname[1024];
  char basename[256] = "simul_";
  static float black_level = 0, wi = 0.45;


  // The beam is centered on y=0, z=0
  // y width is set by user
  // z width is constant (corresponds to all field depth) = Z_DEPTH * sigma(beam)

  if(updating_menu_state != 0)	return D_O_K;
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine simulates and analyze the beam"
			   "for a given range of parameters");
    }

  // Request beam params
  i = win_scanf("Simulation parameters\n"
		"nf %5d   x %5d  y %5d\n\n"
		"sigma beam %5f\n\n"
		"conc min %5f  nstep %5d  factor %5f\n\n"
		"Exponent of intensity dependency to radius %5f\n"
		"([0, 1] radius is mapped to intensity in [0, 255])\n\n"
		"Size distribution is gaussian\n"
		"bead radius mean %5f\n"
		"bead radius std dev\nmin %5f  step %5d  factor %5f\n\n"
		"sigma bead mean %5f\n"
		"sigma bead std dev\nmin %5f  step %5d  factor %5f\n\n"
		"Aggregation probability\nmin %5f step %5d factor %5f\n\n"
		"nb of replicates %5d   %b keep err bars\n"
		"basename %24s",
		&oi_nf, &nx, &ny, &fsbm, &conc_ini, &conc_step, &conc_factor, &exponent,
		&fr_m, &rs_ini, &rs_step, &rs_factor,
		&fsbdm, &sbds_ini, &sbds_step, &sbds_factor,
		&aggreg_ini, &aggreg_step, &aggreg_factor, &nrep, &keep_errbars, &basename);
  if (i == CANCEL) return 0;

  if ((pr = find_pr_in_current_dialog(NULL)) == NULL)
    return win_printf_OK("Cannot find plot region!");

  imr = find_imr_in_current_dialog(NULL);
  if (imr == NULL) //(ac_grep(cur_ac_reg,"%im",&imr) != 1)
    imr = create_imreg_with_movie(nx, ny ,IS_CHAR_IMAGE, oi_nf);
  else 
    {
      ois = create_and_attach_movie_to_imr(imr, nx, ny, IS_CHAR_IMAGE, oi_nf);
      imr->cur_oi = imr->n_oi - 1;
      imr->one_i = imr->o_i[imr->cur_oi];
    }
  if (imr == NULL || ((ois = imr->one_i) == NULL))	
    return win_printf_OK("Cannot allocate image!");
  
  switch_project_to_this_imreg(imr);
  map_pixel_ratio_of_image_and_screen(ois, 1, 1);
  find_zmin_zmax(ois);
  smooth_interpol = 0;

  // Request lambda params
  i = win_scanf("Black level: %4f\nWidth: %4f\n", &black_level, &wi);
  if (i == CANCEL)	return OFF;


  // Request BP params
  z0 = (rot==0) ? (ois->im.ny/2) : (ois->im.nx/2);
  i = win_scanf("h/v (h:0 v:1) %10d\nbeam heigth %10d\n"
		"Fourier movie\nbin size %12d width %12d\n"
		"Band-pass filter cutoff lowpass  %10flowpass width %10f\n"
		"cutoff highpass  %10fhighpass width %10f\n\n",
		&rot,&z0,&fsize,&fwidth,&lp,&lw,&hp,&hw);
  if (i == CANCEL) return 0;

  xc = (rot==0) ? (ois->im.nx/2) : z0;
  yc = (rot==0) ? z0 : (ois->im.ny/2);
  x0 = (rot==0) ? 0 : (xc - fwidth/2);
  y0 = (rot==0) ? (yc - fwidth/2) : 0;
  wx = (rot==0) ? ois->im.nx : fwidth;
  wy = (rot==0) ? fwidth : ois->im.ny;


  nf = compute_nb_of_min_subset_ROI(ois, fsize, fsize, x0, y0, wx, wy);
  // display nb of small images
  cx = 4*(wx - fsize);
  nx0 = cx/(3*fsize);
  nx0 += (cx%(3*fsize)) ? 2 : 1;
  cy = 4*(wy - fsize);
  ny0 = cy/(3*fsize);
  ny0 += (cy%(3*fsize)) ? 2 : 1;
  win_printf_OK("Nb of small images %d (%d x %d)", nf, nx0, ny0);


  // Allocate treatment images
  apo = create_appodisation_float_image(fsize, fsize, 0.05, 0);
  if (apo == NULL)	return win_printf_OK("Could not create apodisation image!");

  if ((oim = create_and_attach_movie_to_imr(imr, fsize, fsize, IS_FLOAT_IMAGE, nf)) == NULL)
    return win_printf_OK("I can't create image !");      
  set_z_black_z_white_values(oim, -1, 25);
  set_zmin_zmax_values(oim, 0, 25);
  set_im_title(oim, "Subset images");

  if ((oif = create_and_attach_movie_to_imr(imr, fsize, fsize/2, IS_COMPLEX_IMAGE, nf)) == NULL)
    return win_printf_OK("I can't create image !");
  oif->height *= 0.5;
  set_z_black_z_white_values(oif, -10, 0);
  set_zmin_zmax_values(oif, -10, 0);

  if ((oifb = create_and_attach_movie_to_imr(imr, fsize, fsize, IS_FLOAT_IMAGE, nf)) == NULL)
    return win_printf_OK("I can't create image !");
  set_z_black_z_white_values(oifb, -10, 20);
  set_zmin_zmax_values(oifb, -10, 20);

  if ((oid = create_and_attach_movie_to_imr(imr, wx, wy, IS_FLOAT_IMAGE, ois->im.n_f)) == NULL)
    return win_printf_OK("I can't create image !");
  map_pixel_ratio_of_image_and_screen(oid, 1, 1);
  set_z_black_z_white_values(oid, -10, 20);
  set_zmin_zmax_values(oid, -10, 20);

  // Allocate treatment plots
  if ((ophbp = create_and_attach_one_plot(pr, BP_LEVELS*4, BP_LEVELS*4, 0)) == NULL)
    return win_printf_OK("I can't create plot !");
  dsx = ophbp->dat[0]; // the first data set was already created
  set_ds_source(dsx, "Bar Histogram all band-passed pixels");

  if ((dsy = create_and_attach_one_ds(ophbp, BP_LEVELS, BP_LEVELS, 0)) == NULL)
    return win_printf_OK("I can't create plot !");
  set_ds_source(dsy, "Histogram all band-passed pixels");
  set_plot_y_log(ophbp);

  if ((opallhbp = create_and_attach_one_plot(pr, BP_LEVELS, BP_LEVELS, 0)) == NULL)
    return win_printf_OK("I can't create plot !");
  //remove_ds_n_from_op(opallhbp, 0);  // does not work
  set_plot_y_log(opallhbp);

  if ((opl = create_and_attach_one_plot(pr, conc_step, conc_step, 0)) == NULL)
    return win_printf_OK("I can't create lambda plot !");
  dsl0 = opl->dat[0];
  dsl0->nx = dsl0->ny = 0;  // mind ny init!
  alloc_data_set_y_error(dsl0);

  if ((dslm = create_and_attach_one_ds(opl, conc_step, conc_step, 0)) == NULL)
    return win_printf_OK("Cannot add ds to lambda plot.");
  dslm->nx = dslm->ny = 0;  // mind ny init!
  alloc_data_set_y_error(dslm);

  set_plot_title(opl, "blacklevel %1.2f width %1.2f", black_level, wi);
  set_plot_x_log(opl);
  set_plot_y_log(opl);

  switch_project_to_this_pltreg(pr);
  do_save_bin();
#ifdef XV_MAC
  opl->dir[strlen(opl->dir)-1] = 0;
#endif
  set_op_path(opallhbp, opl->dir);

  for (fsbds = sbds_ini, s = 0; s < sbds_step; fsbds *= sbds_factor, s++)
    for (fr_s = rs_ini, r = 0; r < rs_step; fr_s *= rs_factor, r++)
      for (aggreg = aggreg_ini, agg = 0; agg < aggreg_step; aggreg *= aggreg_factor, agg++)
	{
	  switch_plot(pr, get_op_number_in_pr(pr, opallhbp));

	  // Erase histo ds
	  for (i = opallhbp->n_dat-1; i > 0; i--)
	    remove_ds_n_from_op(opallhbp, i);
	
	  for (conc = conc_ini, c = 0; c < conc_step; conc *= conc_factor, c++)
	    {
	      // Run simul nrep times
	      for (nr = 0; nr < nrep; nr++)
		{
		  for (f = 0; f < ois->im.n_f; f++)
		    {
		      switch_frame(ois, f);
		      simul_beads_on_one_frame(ois, nx, ny, (double) fsbm, (double) conc, (double) fsbdm, (double) fsbds, 0, (double) exponent, (double) fr_m, (double) fr_s, 0, 0, 0, aggreg);
		    }
		  find_zmin_zmax(ois);
		  refresh_image(imr, UNCHANGED);
	    
		  // Run BP filter  
		  for (i = 0; i < BP_LEVELS; i++)
		    histo_bp[i] = 0;
	    
		  /* 	    t_c = my_uclock(); */
		  for (cf = 0; cf < ois->im.n_f; cf++)
		    {
		      switch_frame(ois, cf);
		      switch_frame(oid, cf);
		      split_before_bp(ois, oim, NULL, nf, fsize, x0, y0, wx, wy);
		
		      forward_fftbt_2d_im_appo(oif, oim, 0, screen, apo, 0);
		      band_pass_filter_fft_2d_im(oif, oif,  0, lp,  lw,  hp,  hw, 0, 0);
		      backward_fftbt_2d_im_desappo(oifb, oif, 0, screen, NULL, 0);
		
		      reconstruct_after_bp(ois, oifb, oid, nf, fsize, wx, wy, 0, 0);
		      produce_histo_from_bp(oifb, nf, histo_bp, BP_LEVELS, (float) BP_FACTOR);
		    }
		  /* 	    t_c = my_uclock() - t_c; */
		  /* 	    if (to == 0) to = MY_UCLOCKS_PER_SEC; */

		  display_histo_in_op(histo_bp, dsx, dsy, BP_LEVELS, (float) BP_FACTOR);
		  set_plot_title(ophbp, "\\stack{{blacklevel %1.2f width %1.2f}{n_{img} = %d  s_r = %1.2f  s_{sbd} = %1.2f agg = %1.2f}}", black_level, wi, ois->im.n_f, fr_s, fsbds, aggreg);

		  ds = duplicate_data_set(dsy, NULL);
		  set_ds_source(ds, "Dilution = %1.2e\nnb img = %d\ns_r = %1.2e\ns_sbd = %1.2e\nagg = %1.2f\nnr = %d", 1/conc, ois->im.n_f, fr_s, fsbds, aggreg, nr);
		  add_data_to_one_plot(opallhbp, IS_DATA_SET, (void*)ds);
		  refresh_plot(pr, UNCHANGED);
		}
	
	      set_plot_title(opallhbp, "\\stack{{nrep %d blacklevel %1.2f width %1.2f}{n_{img} = %d  s_r = %1.2f  s_{sbd} = %1.2f}}", nrep, black_level, wi, ois->im.n_f, fr_s, fsbds);
	      set_op_filename(opallhbp, "%sn%d_rs%1.2f_ss%1.2f_agg%1.2f_allhbp.gr", basename, ois->im.n_f, fr_s, fsbds);
	      append_filename(fullname, opallhbp->dir, opallhbp->filename, 1024);
	      save_one_plot_bin(opallhbp, fullname);
	    }

	  // Erase dsl0 and dslm
	  dslm->nx = dslm->ny = 0;  // mind ny init!
	  dsl0->nx = dsl0->ny = 0;  // mind ny init!
	  set_ds_source(dsl0, "Computed with lambda 0\nblacklevel %1.2f width %1.2f\nn_img = %d  s_r = %1.2f  s_sbd = %1.2f\nagg = %1.2f\nnrep = %d", black_level, wi, ois->im.n_f, fr_s, fsbds, aggreg, nrep);
	  set_ds_source(dslm, "Computed with lambda mean\nblacklevel %1.2f width %1.2f\nn_img = %d  s_r = %1.2f  s_sbd = %1.2f\nagg = %1.2f\nnrep = %d", black_level, wi, ois->im.n_f, fr_s, fsbds, aggreg, nrep);
	  process_all_histo_float(opallhbp, dsl0, dslm, black_level, wi, 1);

	  if (nrep > 1)
	    {
	      	sort_ds_along_x(dslm);
	      	sort_ds_along_x(dsl0);

		do_ds_decime_average(opl, dslm, nrep);
		do_ds_decime_average(opl, dsl0, nrep);

		set_ds_dot_line(dslm); set_ds_point_symbol(dslm, "\\pt4\\cr");
		set_ds_dot_line(dsl0); set_ds_point_symbol(dsl0, "\\pt4\\cr");

		if (keep_errbars == 0)
		  {
		    free_data_set_y_error(dslm);
		    free_data_set_y_error(dsl0);
		  }
	    }

	  set_plot_title(opl, "\\stack{{nrep %d blacklevel %1.2f width %1.2f}{n_{img} = %d  s_r = %1.2f  s_{sbd} = %1.2f agg = %1.2f}}", nrep, black_level, wi, ois->im.n_f, fr_s, fsbds, aggreg);
	  set_op_filename(opl, "%sn%d_rs%1.2f_ss%1.2f_agg%1.2f_lbd.gr", basename, ois->im.n_f, fr_s, fsbds, aggreg);
	  append_filename(fullname, opl->dir, opl->filename, 1024);
	  save_one_plot_bin(opl, fullname);
	  opl->need_to_refresh = 1;
	  switch_plot(pr, get_op_number_in_pr(pr, opl));
	  refresh_plot(pr, UNCHANGED);
	}

  return D_O_K;
}


/************************************/
/*   Create gaussian noise image    */
/************************************/

int do_add_gaussian_noise_to_image(void)
{
  imreg *imr;
  O_i *ois;

  const gsl_rng_type *T;
  gsl_rng *rng;
  static unsigned long int seed;
  static float fMean = 0, fVar = 0;
  float tmp;
  int i, j, f;

  if (updating_menu_state != 0)	
    return D_O_K;	

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    return D_O_K;	
  if (ois->im.data_type != IS_CHAR_IMAGE)
    return win_printf("Only char images are supported");

  i = win_scanf("Noise mean %5f\nNoise var %5f\n"
		"Warning: noise will be rounded to integer values", &fMean, &fVar);
  if (i == CANCEL) return OFF;

  // Init poisson generator
  // from http://www.gnu.org/software/gsl/manual/html_node/Random-Number-Distribution-Examples.html
  gsl_rng_env_setup(); 
  T = gsl_rng_default; 
  rng = gsl_rng_alloc (T);
  seed += 1;
  gsl_rng_set(rng, seed);

  // Add gaussian var to each pixel on each frame
  for (f=0; f<ois->im.n_f; f++)
    for (i=0; i<ois->im.ny; i++)
      for (j=0; j<ois->im.nx; j++)
	{
	  tmp = fMean + (float) gsl_ran_gaussian(rng, (double) fVar);
	  tmp = tmp < 0 ? 0 : tmp;
	  ois->im.pxl[f][i].ch[j] += (char) tmp;

	  /* tmp = (float) ois->im.pxl[f][i].ch[j] + fMean + (float) gsl_ran_gaussian(rng, (double) fVar); */
	  /* tmp = tmp < 0 ? 0 : tmp; */
	  /* ois->im.pxl[f][i].ch[j] = (char) tmp; */
	}

  find_zmin_zmax(ois);
  ois->need_to_refresh = ALL_NEED_REFRESH;				
  broadcast_dialog_message(MSG_DRAW,0);	
  return D_REDRAWME;		
}



/************************************/
/*  Movie avg over time (for bg)    */
/************************************/

#ifndef MAX_TITLE_STRING_SIZE
#define MAX_TITLE_STRING_SIZE 1024
#endif

int do_add_two_movies(void)
{
  register int i, j, f;             /* intermediaire compteurs de boucle    */
  int nf;
  imreg *imr;		       /* image region 			       */
  O_i 	*ois1, *ois2, *oid;    /* one image source (x2) et destination */
  union pix **ps1, **ps2, **pd;     /* data des images (matrice) 	       */
  int	nx1, nx2, nxn;         /* dimensions en x 		       */
  int 	ny1, ny2, nyn;	       /* dimensions en y 		       */
  char  	c1[MAX_TITLE_STRING_SIZE], 
    c2[MAX_TITLE_STRING_SIZE]; /* chaine pour l'info et l'heritage */

  if (updating_menu_state != 0)	
    {
      if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois1) != 2)  
	{active_menu->flags |=  D_DISABLED; 		return D_O_K;	}
      else active_menu->flags &= ~D_DISABLED;
      if (imr->n_oi < 2 || ois1->im.data_type == IS_COMPLEX_IMAGE) 
	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;	
    } 
		
  if (key[KEY_LSHIFT])
    return win_printf_OK("Frame by frame addition of two char images");

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois1) != 2)	return win_printf_OK("cannot find data!");
  if (ois1 == NULL || ois1->im.data_type == IS_COMPLEX_IMAGE)		return win_printf_OK("cannot treat complex image");
  if (imr->n_oi < 2)					return win_printf_OK("No enought images to perform that !");

  i = imr->cur_oi; 		/* on recupere le numero de l'image active 	*/
  ps1 = ois1->im.pxl;
  i = ( (i-1 < 0) ? (imr->n_oi-1) : (i-1) );
  ois2 = imr->o_i[i]; 		/* on recupere la deuxieme image 		*/
  ps2 = imr->o_i[i]->im.pxl;	/* on recupere les data de l'image n∞i-1 	*/

  if (ps1 == NULL || ps2 == NULL)				return (win_printf("I cannot find any data in image"));

  nx1 = ois1->im.nx;  ny1 = ois1->im.ny; /* dimensions de l'image 1 		*/
  nx2 = ois2->im.nx;  ny2 = ois2->im.ny; /* dimensions de l'image 2 		*/
  nxn = (nx1 > nx2) ? nx2 : nx1;			/* dimensions minimales (x) 		*/
  nyn = (ny1 > ny2) ? ny2 : ny1;			/* idem 		(y) 		*/

  if (ois1->im.data_type != ois2->im.data_type)
    return win_printf("Images are of different type !\n"
		      " This version of the procedure \n cannot operate with them.");

  if (nx1 != nx2)
    i = win_printf("images have different number of colomns %d and %d !\n"
		   " I propose to add them over the %d first ones",nx1,nx2,nxn);
  if (i == CANCEL) return D_O_K;   /* abandon par l'utilisateur */
  if (ny1 != ny2)
    i = win_printf("images have different number of lines %d and %d !\n"
		   " I propose to add them over the %d first ones",ny1,ny2,nyn);
  if (i == CANCEL)	return D_O_K;  /* abandon par l'utilisateur */

  if (ois1->im.data_type != IS_CHAR_IMAGE || ois2->im.data_type != IS_CHAR_IMAGE)
    return win_printf("One image is not of type char (unsupported)");

  nf = ois1->im.n_f;
  if (nf != ois2->im.n_f)
    return win_printf("The two image must have the same number of frames");

  i = IS_CHAR_IMAGE;
  oid = create_one_movie(nxn ,nyn, i, nf);	/* on cree une image de ce type i */
  pd  = oid->im.pxl;			/* affectations des datas de l 'image 	*/
  
  for (f=0; f<nf; f++)
    for (i=0; i<nyn; i++)
      for (j=0; j<nxn; j++)
	pd[f][i].ch[j] = ps1[f][i].ch[j] + ps2[f][i].ch[j];

  oid->ax = ois1->ax;  /*factor*/
  oid->dx = ois1->dx;  /*offset*/
  oid->ay = ois1->ay;  /*factor*/
  oid->dy = ois1->dy;  /*offset*/
  oid->az = ois1->az;  /*factor*/
  oid->dz = ois1->dz;  /*offset*/

  /* getting the title of the source image 1, and protection against too long names */
  if ( (ois1->title != NULL) && (strlen(ois1->title) < MAX_TITLE_STRING_SIZE/2) )
    {	strcpy(c1, ois1->title);	}
  else {
    if (ois1->im.source != NULL) 
      strcpy(c1, ois1->im.source);
    else	c1[0] = 0;     
  }
  /* getting the title of the source image 2, and protection against too long names */
  if ( (ois2->title != NULL) && (strlen(ois2->title) < MAX_TITLE_STRING_SIZE/2) )
    {	strcpy(c2, ois2->title);	}
  else {
    if (ois2->im.source != NULL) 
      strcpy(c2, ois2->im.source);
    else	c2[0] = 0;
  }

  set_im_title(oid,"\\stack{{{Added images}{%s and %s}}}", c1, c2);
  set_formated_string(&oid->im.treatement,"Added images %s and %s", c1, c2);

  inherit_from_im_to_im(oid,ois1);		/* heritage global des proprietes de l'image 1 		*/
  uns_oi_2_oi(oid,ois1);				/* heritage des unites de l'image 1 			*/
  oid->im.win_flag = ois1->im.win_flag;		/* heritage des boundary condition de l'image 1 	*/

  add_image(imr, oid->type, (void*)oid);
  find_zmin_zmax(oid);
  imr->cur_oi = imr->n_oi - 1;
  oid->need_to_refresh = ALL_NEED_REFRESH;				
  broadcast_dialog_message(MSG_DRAW,0);	
  return D_REDRAWME;		
}

int do_movie_avg(void)
{
  O_i *ois, *oid;
  imreg *imr;
  int nx, ny, nf, i, im, j;
  float *z = NULL, avg;
  union pix *pd; 

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;	
    }		
  nf = abs(ois->im.n_f);		
  if (nf <= 0)	
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;
    }
  else   active_menu->flags &= ~D_DISABLED;

  if(updating_menu_state != 0)	return D_O_K;
  i = ois->im.c_f;
  if (ois->im.data_type == IS_COMPLEX_IMAGE 
      || ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)	
    {
      win_printf("I do not handle complexe images ! yet");
      return D_O_K;
    }
  nx = ois->im.nx;
  ny = ois->im.ny;

  oid = create_and_attach_oi_to_imr(imr, nx, ny, IS_FLOAT_IMAGE);
  if (oid == NULL)      
    return win_printf_OK("Can't create dest movie");


  for (i = 0, pd = oid->im.pixel; i < ny ; i++)
    {
      for (j=0; j< nx; j++)
	{
	  z = extract_z_profile_from_movie(ois, j, i, z);
	  for (im = 0, avg = 0; im < nf; im++)
	      avg += z[im]; 
	  avg /= nf;
	  pd[i].fl[j] = avg;
	}
    }
  inherit_from_im_to_im(oid,ois);
  uns_oi_2_oi(oid,ois);
  oid->im.win_flag = ois->im.win_flag;
  if (ois->title != NULL)	set_im_title(oid, "%s ", ois->title);
  set_formated_string(&oid->im.treatement,"mean movie of %s over %d frames", ois->filename, ois->im.n_f);
  find_zmin_zmax(oid);
  free(z);
  
  return (refresh_image(imr, imr->n_oi - 1));
}




#endif
