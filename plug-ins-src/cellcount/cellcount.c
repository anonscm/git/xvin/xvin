/*
*    Plug-in program for image treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _CELLCOUNT_C_
#define _CELLCOUNT_C_

# include "allegro.h"
# include "xvin.h"

/* If you include other regular header do it here*/ 
#include <sys/types.h>
#include <dirent.h>

/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "cellcount.h"
# include "cellcount_img.h"
//place here other headers of this plugin 

int do_movie_average_along_line_of_big_pixel(void)
{
  register int i, k;
  int l, m, nx2, ny2, off, np0, n0, np1;
  static int l_s = -1, l_e = -1, black = 5;
  static float thres = 2.5;
  O_i *ois = NULL, *oid = NULL;
  int nf;
  union pix *ps, *pd;
  imreg *imr = NULL;
  double lambda = 0, mbump = 0, sig2, mean, bump;
  
  
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;	
    }		
  if(updating_menu_state != 0)	
    {
      if (ois->im.n_f == 1 || ois->im.n_f == 0)	
	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;	
    }	
  nf = abs(ois->im.n_f);		
  if (nf <= 0)	
    {
      win_printf("This is not a movie");
      return D_O_K;
    }
  i = ois->im.c_f;
  
  if (ois->im.data_type != IS_CHAR_IMAGE)
     win_printf_OK("I can only treat char image !");

  if (l_s == -1) l_s = 0; 
  if (l_e == -1) l_e = ois->im.ny;
  i = win_scanf("Average Between lines %12d and %12d\n"
		"Black level %12d threshold %12f\n"
		,&l_s,&l_e,&black,&thres);
  if (i == CANCEL)	return D_O_K;


  ny2 = l_e - l_s;
  nx2 = ois->im.nx/ny2;

  oid = create_and_attach_oi_to_imr(imr, nx2 ,nf, IS_FLOAT_IMAGE);	
  if (oid == NULL) return (win_printf_OK("cant create image!"));

  pd  = oid->im.pixel;		
  inherit_from_im_to_im(oid,ois);
  
  //uns_oi_2_oi_by_type(ois, IS_X_UNIT_SET, oid, IS_X_UNIT_SET);
  //uns_oi_2_oi_by_type(ois, IS_T_UNIT_SET, oid, IS_Y_UNIT_SET);
  for (k = 0, np0 = 0 ; k < nf; k++)
    {
      switch_frame(ois,k);
      ps  = ois->im.pixel;		
      for (i=0 ; i< nx2 ; i++)	
	{
	  off = (ois->im.nx%ny2)/2 + i * ny2;
	  for (l=l_s, n0 = 0, pd[k].fl[i] = 0 ; l< l_e ; l++)	
	    {
	      for (m=0; m < ny2 ; m++)
		{
		  pd[k].fl[i] += ps[l].ch[off+m] - black;
		  n0 += ((ps[l].ch[off+m] - black) > thres) ? 1 : 0;
		}	
	    }
	  np0 += (n0 == 0) ? 1 : 0;
	  pd[k].fl[i] /= ny2 * ny2;
	}
    }

  for (i=0, lambda = 0, mbump = 0; i< nx2 ; i++)	
    {  
      for (k = 0, mean = 0; k < nf; k++)
	mean += pd[k].fl[i];
      for (k = 0, mean /= nf, sig2 = 0; k < nf; k++)
	sig2 += (pd[k].fl[i] - mean) * (pd[k].fl[i] - mean);
      if (nf > 1) sig2 /= (nf-1);
      bump = sig2 / mean;
      mean /= bump;
      lambda += mean;
      mbump += bump;
    }
  lambda /= nx2;
  mbump /= nx2;

 
  for (i=0, mean = 0; i< nx2 ; i++)	
    {  
      for (k = 0; k < nf; k++)
	mean += pd[k].fl[i];
    }
  mean /= (nx2*nf);
  for (i=0, sig2 = 0; i< nx2 ; i++)	
    {  
      for (k = 0; k < nf; k++)
	sig2 += (pd[k].fl[i] - mean) * (pd[k].fl[i] - mean);
    }
  sig2 /= (nf*nx2);
  bump = sig2 / mean;
  mean /= bump;
     
  for (i=0, np1 = 0; i< nx2 ; i++)	
    {  
      for (k = 0; k < nf; k++)
	np1 += (pd[k].fl[i] > bump/5) ? 0 : 1;
    }


  set_im_title(oid,"\\stack{{Average profile. (%d, %d)}{P(0) = %d/%d => \\lambda  = %g}"
	       "{bump %g \\lambda  = %g}{bump_2 %g \\lambda_2  = %g}{P(0) = %d/%d => \\lambda  = %g}}", 
	       l_s,l_e, np0,nx2*nf, log((double)(nx2*nf)/np0),mbump,lambda,bump,mean,np1,nx2*nf, log((double)(nx2*nf)/np1));
  set_formated_string(&oid->im.treatement,"Average line profile. (%d, %d)", l_s,l_e);
  find_zmin_zmax(oid);
  oid->need_to_refresh = ALL_NEED_REFRESH;
  set_oi_horizontal_extend(oid, 1);
  set_oi_vertical_extend(oid, 1);
  select_image_of_imreg(imr, imr->n_oi-1);
  broadcast_dialog_message(MSG_DRAW,0);	
  return D_REDRAWME;		
}


int do_cellcount_average_along_y(void)
{
	register int i, j;
	int l_s, l_e;
	O_i *ois = NULL;
	O_p *op = NULL;
	d_s *ds;
	imreg *imr = NULL;


	if(updating_menu_state != 0)	return D_O_K;

	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine average the image intensity"
			"of several lines of an image");
	}
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	return D_O_K;
	/* we grap the data that we need, the image and the screen region*/
	l_s = 0; l_e = ois->im.ny;
	/* we ask for the limit of averaging*/
	i = win_scanf("Average Between lines%d and %d",&l_s,&l_e);
	if (i == CANCEL)	return D_O_K;
	/* we create a plot to hold the profile*/
	op = create_one_plot(ois->im.nx,ois->im.nx,0);
	if (op == NULL) return (win_printf_OK("cant create plot!"));
	ds = op->dat[0]; /* we grab the data set */
	op->y_lo = ois->z_black;	op->y_hi = ois->z_white;
	op->iopt2 |= Y_LIM;	op->type = IM_SAME_X_AXIS;
	inherit_from_im_to_ds(ds, ois);
	op->filename = Transfer_filename(ois->filename);
	set_plot_title(op,"cellcount averaged profile from line %d to %d",l_s, l_e);
	set_formated_string(&ds->treatement,"%s averaged profile from line %d to %d",l_s, l_e);
	uns_oi_2_op(ois, IS_Z_UNIT_SET, op, IS_Y_UNIT_SET);
	uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
	for (i=0 ; i< ds->nx ; i++)	ds->yd[i] = 0;
	for (i=l_s ; i< l_e ; i++)
	{
		extract_raw_line(ois, i, ds->xd);
		for (j=0 ; j< ds->nx ; j++)	ds->yd[j] += ds->xd[j];
	}
	for (i=0, j = l_e - l_s ; i< ds->nx && j !=0 ; i++)
	{
		ds->xd[i] = i;
		ds->yd[i] /= j;
	}
	add_one_image (ois, IS_ONE_PLOT, (void *)op);
	ois->cur_op = ois->n_op - 1;
	broadcast_dialog_message(MSG_DRAW,0);
	return D_REDRAWME;
}

O_i	*cellcount_image_multiply_by_a_scalar(O_i *ois, float factor)
{
	register int i, j;
	O_i *oid;
	float tmp;
	int onx, ony, data_type;
	union pix *ps, *pd;

	onx = ois->im.nx;	ony = ois->im.ny;	data_type = ois->im.data_type;
	oid =  create_one_image(onx, ony, data_type);
	if (oid == NULL)
	{
		win_printf("Can't create dest image");
		return NULL;
	}
	if (data_type == IS_CHAR_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].ch[j];
				pd[i].ch[j] = (tmp < 0) ? 0 :
					((tmp > 255) ? 255 : (unsigned char)tmp);
			}
		}
	}
	else if (data_type == IS_INT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].in[j];
				pd[i].in[j] = (tmp < -32768) ? -32768 :
					((tmp > 32767) ? 32767 : (short int)tmp);
			}
		}
	}
	else if (data_type == IS_UINT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].ui[j];
				pd[i].ui[j] = (tmp < 0) ? 0 :
					((tmp > 65535) ? 65535 : (unsigned short int)tmp);
			}
		}
	}
	else if (data_type == IS_LINT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].li[j];
				pd[i].li[j] = (tmp < 0x10000000) ? 0x10000000 :
					((tmp > 0x7FFFFFFF) ? 0x7FFFFFFF : (int)tmp);
			}
		}
	}
	else if (data_type == IS_FLOAT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].fl[j] = factor * ps[i].fl[j];
			}
		}
	}
	else if (data_type == IS_DOUBLE_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].db[j] = factor * ps[i].db[j];
			}
		}
	}
	else if (data_type == IS_COMPLEX_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].fl[2*j] = factor * ps[i].fl[2*j];
				pd[i].fl[2*j+1] = factor * ps[i].fl[2*j+1];
			}
		}
	}
	else if (data_type == IS_COMPLEX_DOUBLE_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].db[2*j] = factor * ps[i].db[2*j];
				pd[i].db[2*j+1] = factor * ps[i].db[2*j+1];
			}
		}
	}
	else if (data_type == IS_RGB_PICTURE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< 3*onx; j++)
			{
				tmp = factor * ps[i].ch[j];
				pd[i].ch[j] = (tmp < 0) ? 0 :
					((tmp > 255) ? 255 : (unsigned char)tmp);
			}
		}
	}
	else if (data_type == IS_RGBA_PICTURE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< 4*onx; j++)
			{
				tmp = factor * ps[i].ch[j];
				pd[i].ch[j] = (tmp < 0) ? 0 :
					((tmp > 255) ? 255 : (unsigned char)tmp);
			}
		}
	}
	inherit_from_im_to_im(oid,ois);
	uns_oi_2_oi(oid,ois);
	oid->im.win_flag = ois->im.win_flag;
	if (ois->title != NULL)	set_im_title(oid, "%s rescaled by %g",
		 ois->title,factor);
	set_formated_string(&oid->im.treatement,
		"Image %s rescaled by %g", ois->filename,factor);
	return oid;
}

int do_cellcount_image_multiply_by_a_scalar(void)
{
	O_i *ois, *oid;
	imreg *imr;
	int i;
	static float factor = 2.0;


	if(updating_menu_state != 0)	return D_O_K;

	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine multiply the image intensity"
		"by a user defined scalar factor");
	}
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	{
		win_printf("Cannot find image");
		return D_O_K;
	}
	i = win_scanf("define the factor of mutiplication %f",&factor);
	if (i == CANCEL)	return D_O_K;
	oid = cellcount_image_multiply_by_a_scalar(ois,factor);
	if (oid == NULL)	return win_printf_OK("unable to create image!");
	add_image(imr, oid->type, (void*)oid);
	find_zmin_zmax(oid);
	return (refresh_image(imr, imr->n_oi - 1));
}

int do_add_plots(void)
// add similar plots from the current directory
{
  pltreg *pr = NULL;
  O_p  *op = NULL, *opn = NULL;
  d_s *ds, *dsn = NULL;
  int itmp, first = 1;
  static int dsid = 0, keep_dilution = 0;
  float dilution;

  char basename[256], basename2[256], basename3[256];
  char appendix[256], stmp[256];
  char *source = NULL;
  char dir[512], file[256];            

  DIR *mydir = NULL;
  struct dirent *entry = NULL;

  if(updating_menu_state != 0)
    {
       add_keyboard_short_cut(0, KEY_B, 0, do_add_plots);
       return D_O_K;
    }

  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr, &op, &ds) != 3)
    return win_printf_OK("Cannot find data");    


  if (op->dir == NULL)
    return win_printf_OK("Current plot has no working directory.");
  else
    {
      strcpy(dir, op->dir);
      strcpy(file, op->filename);
      put_backslash(dir);
      //win_printf("%s",backslash_to_slash(dir)); 

      strcpy(basename, file);
      itmp = win_scanf("Base name %32s\n(NB: Wildcard is %%f or %%e)\n\n"
		       "Dataset index %5d\nExtract dilution from wildcard %b", &basename, &dsid, &keep_dilution);
      if (itmp == CANCEL)	return OFF; 

      if (strstr(basename, "%") != NULL)
	{
	  strcpy(appendix, strstr(basename, "%") + 2);  // 2 is for "%f" length
 	  itmp = strstr(basename, "%") - basename;
	  itmp = (itmp < 0) ? -itmp: itmp;       // index of wildcard first char
	  strncpy(basename2, basename, itmp+2);  // copy the basename including the wildcard
	  basename2[itmp+2] = 0;                 // result of strncpy is not null terminated! :(
	  sprintf(basename3, "%s%%s", basename2);
	}
      mydir = opendir(dir);
      while((entry = readdir(mydir))) /* If we get EOF, the expression is 0 and
				       * the loop stops. */
	{
	  //win_printf("file: %s\nbs: %s\nbs2: %s\nbs3: %s\napp: %s", entry->d_name, basename, basename2, basename3, appendix);
	  if ((sscanf(entry->d_name, basename3, &dilution, &stmp) == 2) && (strcmp(stmp, appendix) == 0))  // name matches the "regexp"
	    {
	      if (first)
		{
		  if ((op = create_and_attach_one_plot(pr, 1, 1, 0)) == NULL)
		    return 1;
		  set_plot_y_log(op);
		  set_plot_title(op, "blacklevel %1.2f width %1.2f", 0., 0.45);
		  switch_plot(pr, pr->n_op - 1);
		  op->need_to_refresh = 1;
		  first = 0;
		}

	      opn = create_plot_from_gr_file(entry->d_name, dir);
	      if (opn == NULL)  return win_printf_OK("could not load %s",entry->d_name);
	      else display_title_message("loaded: %s",entry->d_name);

	      //dsn = find_source_specific_ds_in_op(opn,"Histogram all band-passed pixels");
	      dsn = opn->dat[dsid];
	      if (dsn == NULL) return D_O_K;

	      ds = duplicate_data_set(dsn, NULL);
	      add_data_to_one_plot(op, IS_DATA_SET, (void*)ds);

	      if (keep_dilution)
		{
		  if (ds->source) source = get_ds_source(ds);
		  // win_printf("%s\n%s\n%s\n%d %f", basename, basename2, basename3, itmp, dilution);
		  set_ds_source(ds, "Dilution = %f\n%s", dilution, source);
		}
	      free_one_plot(opn);
	      refresh_plot(pr, UNCHANGED);
	      //auto_x_y_limit(); //not declared as PXV_FUNC
	      auto_x_limit();
	      auto_y_limit();
	    }
	  else
	    {
	      display_title_message("no match: %s\n", entry->d_name);	
	    }
	}
      
      closedir(mydir);
      if (opn != NULL)
	{
	  remove_ds_from_op (op, op->dat[0]);
	  refresh_plot(pr, UNCHANGED);
	}
    }
  return D_O_K;
}


int do_normalize_sum(void)
{
  pltreg *pr = NULL;
  O_p  *op = NULL;
  d_s *dsy;

  int i;
  float nc = 0;
  
  if(updating_menu_state != 0)
    {
//       add_keyboard_short_cut(0, KEY_H, 0, do_process_histo_int);
      return D_O_K;
    }

  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr, &op, &dsy) != 3)
    return win_printf_OK("Cannot find data");    

  for (i = 0, nc = 0; i < dsy->nx; i++)
	    nc = nc + dsy->yd[i];

  win_printf_OK("nc = %f", nc);

  if (nc != 0)
    for (i = 0; i < dsy->nx; i++)
      {
        dsy->yd[i] /= nc;
      }

  return D_O_K;
}


int do_process_histo_int(void)
{
  pltreg *pr = NULL;
  O_p  *op = NULL;
  d_s *dsy;

  int i=0, itmp, black_level, wi;
  double mean = 0, sig2 = 0, bump = 0, lambda = 0, nc = 0, dtmp, nc0 = 0, lambda0 = 0;
  

  if(updating_menu_state != 0)
    {
      add_keyboard_short_cut(0, KEY_H, 0, do_process_histo_int);
      return D_O_K;
    }

  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr, &op, &dsy) != 3)
    return win_printf_OK("Cannot find data");

/*   dsy = find_source_specific_ds_in_op(op,"Histogram all pixels"); */
/*   if (dsx == NULL) return D_O_K;	 */

  if (op->title != NULL)
    sscanf(op->title,"\\stack{{image %d  black_level %d width %d" ,&itmp,&black_level,&wi);
  i = win_scanf("Black level: %4d\nWidth: %4d", &black_level, &wi);
  if (i == CANCEL)	return OFF;

  for (i = 0, mean = 0, nc = 0; i < 256; i++)
    {
/*       dsy->xd[i] = i; */
/*       dsy->yd[i] = real_time_info->histotmp[i]; */
      if (i >= black_level)
	{
	  mean += (i-black_level) * dsy->yd[i];
	  nc = nc + (double)dsy->yd[i];
	}
    }
  if (nc > 0) mean /= nc; 
  for (i = 0, sig2 = 0; i < 256; i++)
    {
      dtmp = i-black_level - mean;
      dtmp *= dtmp;
      dtmp *= dsy->yd[i];
      sig2 += dtmp; 
    }
  if (nc > 0) sig2 /= nc; 
  bump = sig2/mean;
  lambda = mean /bump;
  for (i = 0, nc0 = 0; i < 256; i++)
    {
      if (i >= black_level)
	{
	  if ((i - black_level) < wi)
	    {
	      win_printf_OK("h[%d] = %f\nnc0 = %g", i, dsy->yd[i], nc0);
	      nc0 = nc0 + (double)dsy->yd[i];
	    }
	}
    }
  if (nc0 > 0) lambda0 = log(nc/nc0); 

  win_printf_OK("m = %g, s2 = %g\nbump = %g, l = %g, l0 = %g\nnc = %g, nc0 = %g", mean, sig2, bump, lambda, lambda0, nc, nc0);

/*   set_plot_title(op, "\\stack{{image %d  black_level %d width %d}{Bump %g \\lambda = %g \\lambda_0 = %g}{nc %g nc0 %g}}"  */
/* 		 ,real_time_info->ac_i,real_time_info->black_level,w,bump,lambda,lambda0,nc,nc0); */
/*   real_time_info->histo_update = 1; */
  return D_O_K;
}

int histo_mean_var(d_s *ds, float bl, float wi, double *mean, double *var, double *nc)
{
  // Computes mean and of an histo stored in a ds
  // points below bl are discarded; points between bl and bl+wi are counted as 0
  // points above bl+wi are normalized by substracting bl+wi
  int i;
  double dtmp;

  for (i = 0, *mean = 0, *nc = 0; i < ds->nx; i++)
    {
      if (ds->xd[i] >= bl)
	{
	  if (ds->xd[i] < bl + wi)
	    *nc = *nc + (double)ds->yd[i];  // counted as 0
	  else
	    {
	      *mean += (ds->xd[i] - bl-wi) * ds->yd[i];
	      *nc = *nc + (double)ds->yd[i];
	    }
	}
    }
  if (nc > 0) *mean /= *nc; 
  for (i = 0, *var = 0; i < ds->nx; i++)
    {
      if (ds->xd[i] >= bl)
	{
	  if (ds->xd[i] < bl + wi)
	    dtmp = 0 - *mean;  // counted as 0
	  else
	    dtmp = ds->xd[i]-bl-wi - *mean;
	  dtmp *= dtmp;
	  dtmp *= ds->yd[i];
	  *var += dtmp; 
	}
    }
  if (*nc > 0) *var /= *nc; 
  
  return 0; 
}

int histo_lambda_mean(d_s *ds, float bl, float wi, double *lambda, double *nc)
{
  double mean = 0, sig2 = 0, bump = 0;

  *lambda = 0;
  histo_mean_var(ds, bl, wi, &mean, &sig2, nc);
  if (mean > 0)
    bump = sig2/mean;
  if (bump > 0)    
    *lambda = mean/bump;
 
  return 0;
}

int histo_lambda_zero(d_s *ds, float bl, float wi, double *lambda0, double *nc, double *nc0)
{
  // Computes lambda0 of an histo stored in a ds
  int i;

  for (i = 0, *nc0 = 0, *nc = 0; i < ds->nx; i++)
    {
      if (ds->xd[i] >= bl)
	{
	  if (ds->xd[i] < bl + wi)
	    {
	      *nc0 = *nc0 + (double)ds->yd[i];
	      *nc = *nc + (double)ds->yd[i];
	    }
	  else
	    *nc = *nc + (double)ds->yd[i];
	}
    }
  if (*nc0 > 0) *lambda0 = log(*nc / *nc0); 

  return 0;
}

int raw_histo_mean_var(int *histo, int size, float factor, float bl, float wi, double *mean, double *var, double *nc)
// This function computes mean and var of an histo stored in a table of ints
// points below bl are discarded; points between bl and bl+wi are counted as 0
// points above bl+wi are normalized by substracting bl+wi
{
  int i;
  float tmp;
  double dtmp;

  for (i = 0, *mean = 0, *nc = 0; i < size; i++)
    {
      tmp = (float) i / factor;
      if (tmp >= bl)
	{
	  if (tmp < bl + wi)
	    *nc = *nc + (double)histo[i];  // counted as 0
	  else
	    {
	      *mean += ((double) tmp - bl-wi) * (double) histo[i];
	      *nc = *nc + (double)histo[i];
	    }
	}
    }
  if (nc > 0) *mean /= *nc; 
  for (i = 0, *var = 0; i < size; i++)
    {
      tmp = (float) i / factor;
      if (tmp >= bl)
	{
	  if (tmp < bl + wi)
	    dtmp = 0 - *mean;  // counted as 0
	  else
	    dtmp = (double)tmp-bl-wi - *mean;
	  dtmp *= dtmp;
	  dtmp *= (double) histo[i];
	  *var += dtmp; 
	}
    }
  if (*nc > 0) *var /= *nc; 
  
  return 0; 
}

int raw_histo_lambda_mean(int *histo, int size, float factor, float bl, float wi, double *lambda, double *nc)
// This function computes lambda mean of an histo stored in a table of ints
{
  double mean = 0, sig2 = 0, bump = 0;

  *lambda = 0;
  raw_histo_mean_var(histo, size, factor, bl, wi, &mean, &sig2, nc);
  if (mean > 0)
    bump = sig2/mean;
  if (bump > 0)    
    *lambda = mean/bump;
 
  return 0;
}

int raw_histo_lambda_zero(int *histo, int size, float factor, float bl, float wi, double *lambda0, double *nc, double *nc0)
// This function computes lambda 0 of an histo stored in a table of ints
{
  int i;
  float tmp;

  for (i = 0, *nc0 = 0, *nc = 0; i < size; i++)
    {
      tmp = (float) i / factor;
      if (tmp >= bl)
	{
	  if (tmp < bl + wi)
	    {
	      *nc0 = *nc0 + (double)histo[i];
	      *nc = *nc + (double)histo[i];
	    }
	  else
	    *nc = *nc + (double)histo[i];
	}
    }
  if (*nc0 > 0) *lambda0 = log(*nc / *nc0); 

  return 0;
}

float compute_lbdm_error(double lambda, double nc, int redundancy)
{
  float tmp;

  tmp = pow(10, 0.1556913) * pow((float)nc/(float)redundancy, -0.4977910) * pow((float)lambda, 0.5522067);
  if (tmp > 0)
    return tmp;
  return 0;
}

float compute_lbd0_error(double lambda, double nc, int redundancy)
{
  float tmp;

  tmp = pow(10, 0.05749482) * pow((float)nc/(float)redundancy, -0.50440390) * pow((float)lambda, 0.50458145);
  if (tmp > 0)
    return tmp;
  return 0;
}

float compute_lbd_generic_error(double lambda, double nc, int redundancy)
{
  double tmp;

  tmp = sqrt(lambda * (double)redundancy / nc);
  if (tmp > 0)
    return (float) tmp;
  return 0;
}

int do_test_err(void)
{
  double lambda = 0.01, nc = 1e6;
  int redundancy = 100;
  float tmp, tmp2;
  
  if(updating_menu_state != 0)
    return D_O_K;

  tmp = (float) lambda;
  tmp2 = (float) nc;
  win_scanf("l %12f\nn %12f\nr %12d", &tmp, &tmp2, &redundancy);
  lambda = (double) tmp;
  nc = (double) tmp2;

  return win_printf("m: %f\n0: %f\ng: %f\n\nrel: %g\nrel ok: %g", 
		    compute_lbdm_error(lambda, nc, redundancy),
		    compute_lbd0_error(lambda, nc, redundancy),
		    compute_lbd0_error(lambda, nc, redundancy),
		    compute_lbd_generic_error(lambda, nc, redundancy)/lambda,
		    (double)compute_lbd_generic_error(lambda, nc, redundancy)/lambda);
}

int do_process_histo_float(void)
{
  pltreg *pr = NULL;
  O_p  *op = NULL;
  d_s *dsy;

  int i=0, itmp;
  float black_level = 0, wi = 0.45  ;
  double nc0 = 0, nc = 0, mean = 0, sig2 = 0, bump = 0, lambda = 0, lambda0 = 0;
  

  if(updating_menu_state != 0)
    {
      add_keyboard_short_cut(0, KEY_F, 0, do_process_histo_float);
      return D_O_K;
    }

  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr, &op, &dsy) != 3)
    return win_printf_OK("Cannot find data");

/*   dsy = find_source_specific_ds_in_op(op,"Histogram all pixels"); */
/*   if (dsy == NULL) return D_O_K;	 */

  if (op->title != NULL)
    sscanf(op->title,"\\stack{{image %d  black_level %f width %f" ,&itmp,&black_level,&wi);
  i = win_scanf("Black level: %4f\nWidth: %4f", &black_level, &wi);
  if (i == CANCEL)	return OFF;

  histo_mean_var(dsy, black_level, wi, &mean, &sig2, &nc);
  bump = sig2/mean;
  lambda = mean/bump;

  histo_lambda_zero(dsy, black_level, wi, &lambda0, &nc, &nc0);

  win_printf_OK("bl = %f wi = %f\nm = %g, s2 = %g\nbump = %g, l = %g, l0 = %g",//\nnc = %g, nc0 = %g",
		black_level, wi, mean, sig2, bump, lambda, lambda0);//, nc, nc0);

  return D_O_K;
}

int process_all_histo_float(O_p *hop, d_s *dsl0, d_s *dslm, float bl, float wi, float red)
{
  int n = 0, tmp = -1;
  float  concentration = -1; // default value
  double nc0 = 0, nc = 0, mean = 0, sig2 = 0, bump = 0, lambda = 0, lambda0 = 0;

  for (n = 0; n < hop->n_dat; n++)
    {
      if (hop->dat[n]->source != NULL)
	if (sscanf(hop->dat[n]->source, "Dilution = %f", &concentration) == 1)
	  concentration = 1/concentration;

      if (concentration >= 0)
	{
	  histo_mean_var(hop->dat[n], bl, wi, &mean, &sig2, &nc);
	  bump = sig2/mean;
	  lambda = mean/bump;

	  histo_lambda_zero(hop->dat[n], bl, wi, &lambda0, &nc, &nc0);

	  if ((tmp=add_new_point_with_y_error_to_ds(dslm, concentration, (float) lambda, compute_lbdm_error(lambda, nc, red))) != 0)
	    win_printf("Add lbdm point error: %d", tmp);
	  if ((tmp=add_new_point_with_y_error_to_ds(dsl0, concentration, (float) lambda0, compute_lbd0_error(lambda0, nc, red))) != 0)
	    win_printf("Add lbd0 point error: %d", tmp);
	}
    }
  sort_ds_along_x(dslm);
  sort_ds_along_x(dsl0);

  return 0;
}

 int do_process_all_histo_float(void)
{
  pltreg *pr = NULL;
  O_p  *op = NULL, *opl = NULL;
  d_s *dslm, *dsl0;

  int i = 0;
  static float black_level = 0, wi = 0.45, redundancy = 100;
  

  if(updating_menu_state != 0)
    {
      add_keyboard_short_cut(0, KEY_G, 0, do_process_all_histo_float);
      return D_O_K;
    }

  if (ac_grep(cur_ac_reg,"%pr%op",&pr, &op) != 2)
    return win_printf_OK("Cannot find data");

  if (op->title != NULL)
    sscanf(op->title,"blacklevel %f width %f", &black_level, &wi);
  i = win_scanf("Black level: %4f\nWidth: %4f\nRedundancy: %4f\n"
		"(Redundancy is proposed at 100 as there is at\n"
		"least 3 x 3 overlapping images in space x time)",
		&black_level, &wi, &redundancy);
  if (i == CANCEL)	return OFF;
  
  if ((opl = create_and_attach_one_plot(pr, 2, 2, 0)) == NULL)
    return win_printf_OK("Cannot allocate results plot.");
  dsl0 = opl->dat[0];
  dsl0->nx = dsl0->ny = 0;  // mind ny init!
  dsl0->ny = 0;
  set_ds_source(dsl0, "Computed with lambda 0\nbl = %f\nwidth = %f", black_level, wi);
  alloc_data_set_y_error(dsl0);

  if ((dslm = create_and_attach_one_ds(opl, 2, 2, 0)) == NULL)
    return win_printf_OK("Cannot add ds to results plot.");
  dslm->nx = dslm->ny = 0;  // mind ny init!
  dslm->ny = 0;
  set_ds_source(dslm, "Computed with lambda mean");
  alloc_data_set_y_error(dslm);

  set_plot_title(opl, "blacklevel %1.2f width %1.2f", black_level, wi);
  set_plot_x_log(opl);
  set_plot_y_log(opl);

  process_all_histo_float(op, dsl0, dslm, black_level, wi, redundancy);
  opl->need_to_refresh = 1;
  refresh_plot(pr, UNCHANGED);

/*   set_plot_title(op, "\\stack{{image %d  black_level %d width %d}{Bump %g \\lambda = %g \\lambda_0 = %g}{nc %g nc0 %g}}"  */
/* 		 ,real_time_info->ac_i,real_time_info->black_level,w,bump,lambda,lambda0,nc,nc0); */
/*   real_time_info->histo_update = 1; */
  return D_O_K;
}



MENU *cellcount_image_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"Do BP filtering",do_bp_filter,NULL,0,NULL);
	add_item_to_menu(mn,"Simul a new experiment", do_simul_beam,NULL,0,NULL);
	add_item_to_menu(mn,"Simul and filter new experiment",  do_simul_and_filter_beam,NULL,0,NULL);
	add_item_to_menu(mn,"\0",NULL,NULL,0,NULL);

	add_item_to_menu(mn,"Movie avg over time", do_movie_avg,NULL,0,NULL);
	add_item_to_menu(mn,"Image histogram", display_img_histo,NULL,0,NULL);
	add_item_to_menu(mn,"Add gaussian noise to image", do_add_gaussian_noise_to_image,NULL,0,NULL);
	add_item_to_menu(mn,"Add two movies", do_add_two_movies,NULL,0,NULL);
	add_item_to_menu(mn,"average profile", do_cellcount_average_along_y,NULL,0,NULL);
	add_item_to_menu(mn,"image z rescaled", do_cellcount_image_multiply_by_a_scalar,NULL,0,NULL);
	add_item_to_menu(mn,"movie cell count", do_movie_average_along_line_of_big_pixel,NULL,0,NULL);

	return mn;
}

MENU *cellcount_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"Normalize sum to 1", do_normalize_sum,NULL,0,NULL);
	add_item_to_menu(mn,"Process histogram (int values)      (shortcut H)", do_process_histo_int,NULL,0,NULL);
	add_item_to_menu(mn,"Process histogram (float values)    (shortcut F)", do_process_histo_float,NULL,0,NULL);
	///
	add_item_to_menu(mn,"\0",NULL,NULL,0,NULL);
	add_item_to_menu(mn,"Add all plot from current directory (shortcut B)", do_add_plots,NULL,0,NULL);
	add_item_to_menu(mn,"Process all histos (float values)   (shortcut G)", do_process_all_histo_float,NULL,0,NULL);
	add_item_to_menu(mn,"\0",NULL,NULL,0,NULL);
	add_item_to_menu(mn,"Simul a new experiment", do_simul_beam,NULL,0,NULL);
	add_item_to_menu(mn,"Simul and filter new experiment",  do_simul_and_filter_beam,NULL,0,NULL);

	return mn;
}

int	cellcount_main(int argc, char **argv)
{
	add_image_treat_menu_item ("cellcount", NULL, cellcount_image_menu(), 0, NULL);
	add_plot_treat_menu_item ("cellcount", NULL, cellcount_plot_menu(), 0, NULL);
	return D_O_K;
}

int	cellcount_unload(int argc, char **argv)
{
	remove_item_to_menu(image_treat_menu, "cellcount", NULL, NULL);
	remove_item_to_menu(plot_treat_menu, "cellcount", NULL, NULL);
	return D_O_K;
}
#endif

