#ifndef _CELLCOUNT_H_
#define _CELLCOUNT_H_

#define Z_DEPTH   10
#define CC_ERROR   1e-7

PXV_FUNC(int, do_cellcount_average_along_y, (void));
PXV_FUNC(int, do_process_histo_int, (void));
PXV_FUNC(int, histo_mean_var, (d_s *ds, float bl, float wi, double *mean, double *var, double *nc));
PXV_FUNC(int, histo_lambda_mean, (d_s *ds, float bl, float wi, double *lambda, double *nc));
PXV_FUNC(int, histo_lambda_zero, (d_s *ds, float bl, float wi, double *lambda0, double *nc, double *nc0));
PXV_FUNC(int, raw_histo_mean_var, (int *histo, int size, float factor, float bl, float wi, double *mean, double *var, double *nc));
PXV_FUNC(int, raw_histo_lambda_mean, (int *histo, int size, float factor, float bl, float wi, double *lambda, double *nc));
PXV_FUNC(int, raw_histo_lambda_zero, (int *histo, int size, float factor, float bl, float wi, double *lambda0, double *nc, double *nc0));

PXV_FUNC(float, compute_lbdm_error, (double lambda, double nc, int redundancy));
PXV_FUNC(float, compute_lbd0_error, (double lambda, double nc, int redundancy));
PXV_FUNC(float, compute_lbd_generic_error, (double lambda, double nc, int redundancy));
PXV_FUNC(int, process_all_histo_float, (O_p *hop, d_s *dsl0, d_s *dslm, float bl, float wi, float red));
PXV_FUNC(int, do_process_histo_float, (void));

PXV_FUNC(MENU*, cellcount_image_menu, (void));
PXV_FUNC(int, cellcount_main, (int argc, char **argv));
#endif

