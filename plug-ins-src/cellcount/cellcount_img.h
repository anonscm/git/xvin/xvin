#ifndef _CELLCOUNT_IMG_H_
#define _CELLCOUNT_IMG_H_


PXV_FUNC(int, do_movie_avg, (void));
PXV_FUNC(int, display_img_histo, (void));
PXV_FUNC(int, do_add_two_movies, (void));
PXV_FUNC(int, do_add_gaussian_noise_to_image, (void));
PXV_FUNC(int, append_raw_histo, (O_i *oi, int fr, int *histo_raw, int xmin, int xmax, int ymin, int ymax));

PXV_FUNC(int, do_simul_beam, (void));
PXV_FUNC(int, do_simul_and_filter_beam, (void));

PXV_FUNC(int, do_draw_roi, (void));
PXV_FUNC(int, draw_square_roi, (imreg *imr, int x0, int y0, int wx, int wy));
PXV_FUNC(int, do_bp_filter, (void));
PXV_FUNC(int, copy_one_subset_of_movie_with_bg, (O_i *dest, O_i *ois, O_i *oibg, int x_start, int y_start));
PXV_FUNC(int, split_before_bp, (O_i *ois, O_i *oim, O_i *oibg, int nf, int fsize, int x0, int y0, int wx, int wy));
PXV_FUNC(int, reconstruct_after_bp, (O_i *ois, O_i *oifb, O_i *oid, int nf, int fsize, int wx, int wy, int x_offset, int y_offset));
PXV_FUNC(int, produce_histo_from_bp, (O_i *oifb, int nf, int *histo_bp, int levels, float factor));
PXV_FUNC(int, display_histo_in_op, (int *histo, d_s *dsbar, d_s *dsline, int levels, float factor));

#endif

