#ifndef _TURBULENCE_C_
#define _TURBULENCE_C_

#include "allegro.h"
#include "xvin.h"
#include "fft_filter_lib.h"
#include "xv_tools_lib.h"

#include <gsl/gsl_spline.h>
#include <gsl/gsl_statistics.h>
// #include <fftw3.h>
#include <gsl/gsl_rng.h>			// for white noise generation
#include <gsl/gsl_randist.h>		// for white noise generation

/* If you include other plug-ins header do it here*/ 
#include "../response/response.h"
// #include "../../include/fftw3.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL

#include "turbulence.h"
#include "fit_chi2.h"


// for the white noise generation:
const gsl_rng_type *random_generator_type;		/* type of the random generator 	*/

#define k_b 1.3806503e-23	// m2 kg s-2 K-1
#define Temperature 300		// K

double m     = 0.1;
double alpha = 1.0; // nu
double k     = 10.0;
double k_p   = 0.;
double omega_1 = 620; // cutoff frequency of the low-pass filter

double  kT = k_b*Temperature;

int f_to_d(float *in, double *out, int N)
{	register int i;

	for (i=0; i<N; i++)
	{	out[i] = (double)in[i];
	}
	
	return(0);
}





#ifdef _RESPONSE_H_
int do_fit_cantilever_response(void)
{
	register int i;
	O_p 	*op1=NULL;
	int	ny;
	d_s 	*ds1, *ds2;
	pltreg *pr = NULL;
	int	index;
	float     x_min, x_max;
static float f_min=48., f_max=49.;
	int	      N;
	double	iN, f, f2, sx4, sx3, sx2, sx1, syx2, syx1, syx0, szx0, szx1, delta;
	double	omega_0, f_0, df;
	fftw_complex H, iH;
	char   *message=NULL, *s=NULL; // for my_sprintf and then win_scanf
	int	response_discrete=0;
static int bool_inverse_response=0, bool_visco_elastic=0;
static int bool_plot=1, bool_impose_limits=1;
	int bool_negative_response=0;
	
	if(updating_menu_state != 0)	
	{	if (current_response->mode==0)	active_menu->flags |=  D_DISABLED;
		else	 			active_menu->flags &= ~D_DISABLED; 
		return D_O_K;	
	}
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine fits the current response, according to a damped-spring response\n"
        				"with possible viscous-elastic term\n"
					"-> frequency limits of the current plot are used !\n\n"
					"a new dataset is created with the modulus of the fitted response.\n"
					"The current response is NOT updated. Update it yourself if pleased with the fit.\n"
					"The default values of (m,\\alpha,k) are updated in this aim.");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)	return win_printf_OK("cannot find dataset");

    if (current_response->mode==0)  return(win_printf_OK("cannot find current response!"));
	ny = current_response->n;	// number of points in the response
    if (current_response->mode & RESPONSE_DISCRETE) response_discrete = 1;
    else                                            response_discrete = 0;

	x_min = op1->x_lo; // region from the active x-axis limits
	x_max = op1->x_hi;
	df = current_response->f[1]- current_response->f[0];
	N=0; // number of points to work on
	for (i=0; i<ny; i++)  if ( (current_response->f[i]>=x_min) && (current_response->f[i]<=x_max) ) N++;
   
	// now, a test on the real part: is it positive for small frequencies (ie, is there a (-) sign somewhere ?
	if ( (current_response->r[i][0]<0) ) 
	{	bool_negative_response=1;
		s=my_sprintf(s, "{\\color{lightred}response has a negative real part for small frequencies\n"
					"I will multiply the all response by (-1)}");
	}
	else							   
	{	bool_negative_response=0;
	}
	
	message=my_sprintf(message, "There are a total of %d complex points in the response function.\n\n"
			"Response function is %%R normal (out/in) or %%r already inversed (in/out).\n"
			"Response function is %%R continuous or %%r discrete.\n\n"
			"I will fit in compact interval [%g, %g], over %d points\n\n"
			"%%b fit also visco-elastic k''\n"
            "%%b plot data from the fit if successfull\n\n"
            "%%b don't use limits from plot, but impose [%%5f ; %%5f]\n"
            "(note that \\delta f = %g Hz)\n%s",
			ny, x_min, x_max, N, df, s);
	if (win_scanf(message, &bool_inverse_response, &response_discrete, &bool_visco_elastic,
                           &bool_plot, &bool_impose_limits, &f_min, &f_max)==CANCEL) return(D_O_K);
    free(message); message=NULL;
    if (s!=NULL) { free(s); s=NULL; }
	
    if (bool_impose_limits==1)
    {	x_min = f_min;
    	x_max = f_max;
	}
	
	if (x_min>=x_max-3.*df) 
	{	return(win_printf_OK("\\delta f = %g Hz\n"
					"selected domain [%g %g] is impossible to operate on.\n"
					"either f_{min}<=f_{max} or number of points is less than 3", df, x_min, x_max) );
	}
                           
	sx4=0.; sx3=0.; sx2=0.; sx1=0.; syx2=0.; syx1=0.; syx0=0.; szx0=0.; szx1=0.;
	for (i=0; i<ny; i++)
	{   f    = (double)(current_response->f[i]);
		
        if ( (f>=(double)x_min) && (f<=(double)x_max) ) // on est dans la zone de fit
	    {
		  f	   *= (double)2.*M_PI;	// now f is a pulsation, and no more a frequency
		  
          f2    = f*f;              // cahier V, p31
		  sx4  += f2*f2;
		  sx3  += f2*f;
		  sx2  += f2;
		  sx1  += f;
		  H[0]  = current_response->r[i][0];
		  H[1]  = current_response->r[i][1];
		  if (bool_inverse_response==0) // if H=out/in
		  {  iH[0] =  H[0]/(H[0]*H[0] + H[1]*H[1]); // inverse of the response function, real part
		     iH[1] = -H[1]/(H[0]*H[0] + H[1]*H[1]); // inverse of the response function, imaginary part
          }
          else // H=in/out
          {  iH[0] = H[0];
             iH[1] = H[1];
          }
		  syx0 += (double)iH[0];
		  syx2 += (double)iH[0] * f2;
		  syx1 += (double)iH[0] * f;
		  szx0 += (double)iH[1];
		  szx1 += (double)iH[1] * f;
        }
    }
    iN=(double)1./((double)N);
	delta   = (sx4-sx2*sx2*iN)*(sx2-sx1*sx1*iN) - (sx3-sx1*sx2*iN)*(sx3-sx1*sx2*iN);
	m = (syx2 - syx0*sx2*iN)/(sx4-sx2*sx2*iN);
	k = ((syx0*sx4-syx2*sx2)/(sx4-sx2*sx2*iN)) *iN;
	m*=-1.; // cf : k - m*omega^2
	// cahier Vp31
	
//	win_printf("m = %g \n k= %g", m, k);
	
	if (bool_visco_elastic==0)
	{	alpha = szx1 / sx2;
	    k_p   = (double)0.;
  	    message = my_sprintf(message, "assuming k'' = 0");
	}
	else	                                    // cahier Vp31
	{	delta = sx2 - sx1*sx1*iN;
	   if (fabs(delta)<1e-30) return(win_printf_OK("imaginary part : delta too small"));
		alpha = (szx1 - sx1*szx0*iN)/delta;           // b
		k_p	  = ( (sx2*szx0 - sx1*szx1)/delta ) *iN;  // c
		// fit was : y = b*x +c
		// and we have Im(H^{-1}) = + alpha*omega + k_p
		
		message = my_sprintf(message, "viscous k'' = %g", k_p);
	}
	
//	win_printf("alpha %g / k'' %g", alpha, k_p);
	
	if (fabs(m)<1e-30)	return(win_printf_OK("m=%g is too small!, \\omega_0 will be infinite !!!", m));
	
	omega_0     = sqrt(fabs(k/m));
	f_0 = omega_0/(2.*M_PI);
	
	if (bool_negative_response==1)
	{	k	  *= (double)(-1.);
		m 	  *= (double)(-1.);
		alpha *= (double)(-1.);
		k_p   *= (double)(-1.);
	}

//	win_printf("apres bonnes corrections par 2\\pi : \nm = %g \nalpha = %g\nk= %g\n k'' = %g", m, alpha, k, k_p);

	ds2 = create_and_attach_one_ds (op1, ny, ny, 0);
	for (i=0; i<ny; i++)
	{	f          = current_response->f[i];
        ds2->xd[i] = f;
        f          *= 2.*M_PI;  // attention, c'est la pulsation en fait !!!!
		f2         = f*f; 		// omega^2
		ds2->yd[i] = 1./(float)(sqrt( (k-m*f2)*(k-m*f2) + (alpha*f+k_p)*(alpha*f+k_p) ));
	}
	inherit_from_ds_to_ds(ds2, ds1);
	ds2->treatement = my_sprintf(NULL,"damped spring fit of %s response function, in [%f, %f]\n"
                    "m=%g, \\alpha=%g, k=%g %s", 
				(response_discrete==1) ? "discrete" : "continuous", x_min, x_max, m, alpha, k, message);
	
	set_ds_plot_label(ds2, ds2->xd[ds2->nx/2], ds2->yd[ds2->ny/2], USR_COORD,
				"\\fbox{\\stack{{\\omega_0 = %2.5g rad/s   f_0 = %2.5g Hz}"
				"{m=%2.5g  \\alpha=%2.5g}{k=%2.5g %s}}}", 
				omega_0, f_0, m, alpha, k, message);

    if (bool_plot==1)
    {   cantilever_plot_data(ds2);
    }

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the do_cantilever_response
#endif






#ifdef _RESPONSE_H_
int do_fit_cantilever_LP_response(void)
{
	register int i, j;
	O_p 	*op1=NULL;
	int	ny;
	d_s 	*ds1, *ds2;
	pltreg *pr = NULL;
	int	index;
	float     x_min, x_max;
static float f_min=48., f_max=49.;
	int	      N;
	double	*x, *y, *z, f, f2;
	double	omega_0, f_0, df, delta;
	float   *coefs_r, *coefs_i, alpha_2;
	fftw_complex H, iH;
	char   *message=NULL, *s=NULL; // for my_sprintf and then win_scanf
	int	response_discrete=0;
static int bool_inverse_response=0, bool_visco_elastic=0;
static int bool_plot=1, bool_impose_limits=1;
	int bool_negative_response=0;
	
	if(updating_menu_state != 0)	
	{	if (current_response->mode==0)	active_menu->flags |=  D_DISABLED;
		else	 			active_menu->flags &= ~D_DISABLED; 
		return D_O_K;	
	}
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine fits the current response, according to a damped-spring response\n"
        				"(with possible viscous-elastic term) AND a low pass filter.\n"
					"-> frequency limits of the current plot are used !\n\n"
					"a new dataset is created with the modulus of the fitted response.\n"
					"The current response is NOT updated. Update it yourself if pleased with the fit.\n"
					"The default values of (m,\\alpha,k) are updated in this aim.");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)	return win_printf_OK("cannot find dataset");

    if (current_response->mode==0)  return(win_printf_OK("cannot find current response!"));
	ny = current_response->n;	// number of points in the response
    if (current_response->mode & RESPONSE_DISCRETE) response_discrete = 1;
    else                                            response_discrete = 0;

	x_min = op1->x_lo; // region from the active x-axis limits
	x_max = op1->x_hi;
	df = current_response->f[1]- current_response->f[0];
	N=0; // number of points to work on
	for (i=0; i<ny; i++)  if ( (current_response->f[i]>=x_min) && (current_response->f[i]<=x_max) ) N++;
   
	// now, a test on the real part: is it positive for small frequencies (ie, is there a (-) sign somewhere ?
	if ( (current_response->r[i][0]<0) ) 
	{	bool_negative_response=1;
		s=my_sprintf(s, "{\\color{lightred}response has a negative real part for small frequencies\n"
					"I will multiply the all response by (-1)}");
	}
	else							   
	{	bool_negative_response=0;
	}
	
	message=my_sprintf(message, "There are a total of %d complex points in the response function.\n\n"
			"Response function is %%R normal (out/in) or %%r already inversed (in/out).\n"
			"Response function is %%R continuous or %%r discrete.\n\n"
			"I will fit in compact interval [%g, %g], over %d points\n\n"
			"%%b fit also visco-elastic k''\n"
            "%%b plot data from the fit if successfull\n\n"
            "%%b don't use limits from plot, but impose [%%5f ; %%5f]\n"
            "(note that \\delta f = %g Hz)\n%s",
			ny, x_min, x_max, N, df, s);
	if (win_scanf(message, &bool_inverse_response, &response_discrete, &bool_visco_elastic,
                           &bool_plot, &bool_impose_limits, &f_min, &f_max)==CANCEL) return(D_O_K);
    free(message); message=NULL;
    if (s!=NULL) { free(s); s=NULL; }
	
    if (bool_impose_limits==1)
    {	x_min = f_min;
    	x_max = f_max;
	}
	
	if (x_min>=x_max-3.*df) 
	{	return(win_printf_OK("\\delta f = %g Hz\n"
					"selected domain [%g %g] is impossible to operate on.\n"
					"either f_{min}<=f_{max} or number of points is less than 3", df, x_min, x_max) );
	}
                  
    x=(double*)calloc(N, sizeof(double));
    y=(double*)calloc(N, sizeof(double)); 
    z=(double*)calloc(N, sizeof(double));
     
    j=0;
	for (i=0; i<ny; i++)
	{   f    = (double)(current_response->f[i]);	
        if ( (f>=(double)x_min) && (f<=(double)x_max) ) // on est dans la zone de fit
	    {
		  x[j] = (double)2.*M_PI*f;	// now f is a pulsation, and no more a frequency

		  H[0]  = current_response->r[i][0];
		  H[1]  = current_response->r[i][1];
		  if (bool_inverse_response==0) // if H=out/in
		  {  iH[0] =  H[0]/(H[0]*H[0] + H[1]*H[1]); // inverse of the response function, real part
		     iH[1] = -H[1]/(H[0]*H[0] + H[1]*H[1]); // inverse of the response function, imaginary part
          }
          else // H=in/out
          {  iH[0] = H[0];
             iH[1] = H[1];
          }
		  y[j] = (double)iH[0];
		  z[j] = (double)iH[1];
		  
		  j++;
        }
    }
    if (j!=N) return(win_printf_OK("pb !"));
  
    if (bool_visco_elastic==0)
	{	for (j=0; j<N; j++)
	    {   z[j] /= x[j];
	        x[j] = x[j]*x[j];
        }
        // fit de la partie reelle :
        coefs_r = (float*)calloc(1+1, sizeof(float));
        my_polyfit(1, N, x, y, coefs_r);
        // fit de la partie imaginaire :
        coefs_i = (float*)calloc(1+1, sizeof(float));
        my_polyfit(1, N, x, z, coefs_i);
        
        k       = coefs_r[0];
        
  //      delta = coefs_i[0]*coefs_i[0] + 4.0*(coefs_i) // ???
        k_p     = coefs_i[0];
        omega_1 = k_p / coefs_r[1];
        m       = coefs_i[2]*omega_1;
        alpha   = (coefs_r[2]+m)*omega_1;
        alpha_2 = coefs_i[1] + k/omega_1;
        
	}
	else	                                    // cahier Vp31
	{   // fit de la partie reelle :
        coefs_r = (float*)calloc(2+1, sizeof(float));
        my_polyfit(2, N, x, y, coefs_r);
 
        // fit de la partie imaginaire :
        coefs_i = (float*)calloc(3, sizeof(float));
        fit_Im_HP(N, x, z, coefs_i);
       
        k       = coefs_r[0];
        k_p     = coefs_i[0];
        omega_1 = k_p / coefs_r[1];
        m       = coefs_i[2]*omega_1;
        alpha   = (coefs_r[2]+m)*omega_1;
        alpha_2 = coefs_i[1] + k/omega_1;

        message=my_sprintf(message, "other \\alpha = %2.5g // \\omega_1 = %2.5g", alpha_2, omega_1);
	}
     
	omega_0     = sqrt(fabs(k/m));
	f_0 = omega_0/(2.*M_PI);

	ds2 = create_and_attach_one_ds (op1, ny, ny, 0);
	for (i=0; i<ny; i++)
	{	f          = current_response->f[i];
        ds2->xd[i] = f;
        f          *= 2.*M_PI;  // attention, c'est la pulsation en fait !!!!
		f2         = f*f; 		// omega^2
		ds2->yd[i] = 1./(float)(sqrt( (k-m*f2)*(k-m*f2) + (alpha*f+k_p)*(alpha*f+k_p) ));
	}

//	win_printf("apres bonnes corrections par 2\\pi : \nm = %g \nalpha = %g\nk= %g\n k'' = %g", m, alpha, k, k_p);

	ds2 = create_and_attach_one_ds (op1, ny, ny, 0);
	for (i=0; i<ny; i++)
	{	f          = current_response->f[i];
        ds2->xd[i] = f;
        f          *= 2.*M_PI;  // attention, c'est la pulsation en fait !!!!
		f2         = f*f; 		// omega^2
		ds2->yd[i] = 1./(float)(sqrt( (k-m*f2)*(k-m*f2) + (alpha*f+k_p)*(alpha*f+k_p) ));
	}
	inherit_from_ds_to_ds(ds2, ds1);
	ds2->treatement = my_sprintf(NULL,"damped spring fit of %s response function, in [%f, %f]\n"
                    "m=%g, \\alpha=%g, k=%g %s", 
				(response_discrete==1) ? "discrete" : "continuous", x_min, x_max, m, alpha, k, message);
	
	set_ds_plot_label(ds2, ds2->xd[ds2->nx/2], ds2->yd[ds2->ny/2], USR_COORD,
				"\\fbox{\\stack{{\\omega_0 = %2.5g rad/s   f_0 = %2.5g Hz}"
				"{m=%2.5g  \\alpha=%2.5g}{k=%2.5g k''=%2.5g}{%s}}}", 
				omega_0, f_0, m, alpha, k, k_p, message);

    if (bool_plot==1)
    {   cantilever_plot_data(ds2);
    }

	refresh_plot(pr, UNCHANGED);
	
	free(coefs_r); free(coefs_i);
	return D_O_K;
} // end of the do_fit_cantilever_LP_response
#endif




int do_fit_cantilever_PSD(void)
{
	int i, i_min, i_max, N, do_plot_me=0;
	pltreg *pr = NULL;
	O_p 	*op1=NULL;
	d_s 	*ds1, *ds2;
	int	ny;
	int	index;
static float	f_min=48., f_max=49.;
	float 	x_min, x_max, df;
	double	f2, Sp2, sx1, sx2, sx3, sx4, sy, syx, syx2, delta, delta_a, delta_b;
	double	iN=1., a=0., b=0., c=0., omega_0=0., omega_1=0., maximum=1., moyenne=0., largeur=0.;
static int	bool_plot_on_all_points=1, bool_normalize_with_PSD=0, bool_plot_more=0, bool_impose_limits=1;
static int 	bool_plot_intermediate=0;
	char	*message=NULL;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;


	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine fits an amplitude (V/sqrt(Hz)) PSD\n"
					"and gives the (m,\\alpha,k) values\n\n"
					"a new dataset is created.\n\n"
                    "Note that it is usually a better idea to fit a response than a PSD...");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)	return win_printf_OK("cannot plot region");
	if ( (op1->title==NULL)	|| (op1->y_title==NULL) || (strcmp(op1->y_title,"V/\\sqrt{Hz}")!=0)	)
    {	i=win_printf("I work on amplitude PSDs, but current plot\n"
                        "doesn't seem to be one of them.\n\n"
                        "click OK to continue, or CANCEL to abort");
        if (i==CANCEL) return(D_O_K);
    }

	ny = ds1->ny;	// number of points in time series

	x_min = op1->x_lo; // region from the active x-axis limits
	x_max = op1->x_hi;
	df = ds1->xd[1]- ds1->xd[0];
	N=0; // number of points to work on
	for (i=0; i<ny; i++)  if ( (ds1->xd[i]>=x_min) && (ds1->xd[i]<=x_max) ) N++;
	i_min=0; 	while (ds1->xd[i_min]<f_min) 	i_min++;
	i_max=ny-1; while ( ds1->xd[i_max]>f_max )	i_max = i_max-1; 
	
	message=my_sprintf(message, "{\\pt14\\color{yellow}Fit (k,m,\\alpha) from a Larenzian PSD}\n\n"
            "There are a total of %d complex points in the current dataset.\n\n"
			"I will fit in compact interval [%g, %g], over %d points"
			"(which indices are [%d ; %d]\n\n"
			"%%R plot fit on visible points where it is performed\n"
			"%%r plot fit on all points of dataset\n"
			"%%b normalize by maximum\n"
			"%%b plot intermediate info on the fit\n"
			"%%b plot additional data from the fit if successfull\n\n"
            "%%b don't use limits from plot, but impose [%%5f ; %%5f]\n"
            "(note that \\delta f = %g Hz)",
			ny, x_min, x_max, N, i_min, i_max, df);
	if (win_scanf(message, &bool_plot_on_all_points, &bool_normalize_with_PSD, &bool_plot_intermediate,
					&bool_plot_more, &bool_impose_limits, &f_min, &f_max)==CANCEL) return(D_O_K);
	free(message); message=NULL;
	
    if (bool_impose_limits==1)
    {	x_min = f_min;
    	x_max = f_max;
	}
	else 
	{	f_min = x_min;
    	f_max = x_max;
	}
	
	if (x_min>=x_max-3.*df) 
	{	return(win_printf_OK("\\delta f = %g Hz\n"
					"selected domain [%g %g] is impossible to operate on.\n"
					"either f_{min}<=f_{max} or number of points is less than 3", df, x_min, x_max) );
	}

	// let's find again the limits, in case they have been changed by the user	
	N=0;
	for (i=0; i<ny; i++)  if ( (ds1->xd[i]>=x_min) && (ds1->xd[i]<=x_max) ) N++;
	i_min=0; 	while (ds1->xd[i_min]<f_min) 	i_min++;
	i_max=ny-1; while ( ds1->xd[i_max]>f_max )	i_max = i_max-1; 
			
	if (N!=(i_max-i_min+1)) return(win_printf_OK("pb with N"));
	
	
	// now we fit, according to the formulas in cahier V, p 31
	sx1=0.; sx2=0.; sx3=0.; sx4=0.; sy=0; syx=0.; syx2=0.;
	for (i=i_min; i<=i_max; i++)
	{	f2    = (double)(ds1->xd[i]*ds1->xd[i]);
		Sp2   = (double)1./(ds1->yd[i]*ds1->yd[i]);
		sx1 += (f2);
		sx2 += (f2*f2);
		sx3 += (f2*f2*f2);
		sx4 += (f2*f2*f2*f2);
		sy  += (Sp2);
		syx += (f2*Sp2);
		syx2+= (f2*f2*Sp2);
	}
	// fit y = ax^2 + bx + c
	// cf cahier V, p 31
	iN=(double)1./((double)N);
	delta   = (sx4-sx2*sx2*iN)*(sx2-sx1*sx1*iN) - (sx3-sx1*sx2*iN)*(sx3-sx1*sx2*iN);
	delta_a = (syx2-sy*sx2*iN)*(sx2-sx1*sx1*iN) - (sx3-sx1*sx2*iN)*(syx-sy*sx1*iN);
	delta_b = (sx4-sx2*sx2*iN)*(syx-sy*sx1*iN)  - (syx2-sy*sx2*iN)*(sx3-sx1*sx2*iN);
	
	if (delta==0) return(win_printf_OK("m and \\alpha will be infinite !"));
	a     = delta_a/delta;
	b     = delta_b/delta; 
	c	 = (sy-a*sx2-b*sx1)*iN;

	if (bool_plot_intermediate==1)
	{	message=my_sprintf(message,"\\fbox{\\stack{{H(\\omega) = \\frac{1}{\\sqrt{ax^2+bx+c}}, with x=\\omega^2}"
			"{a = m^2  b=-(2km-\\alpha^2)  c=k^2}"
			"{a=%g, b=%g, c=%g}}}", a, b, c);
		do_plot_me=win_printf("%s\n\n do you want to plot this label ? (CANCEL if NO)", message);
	}
				
	if (a<0)	return(win_printf_OK("m is imaginary !!!"));
	if (c<0)	return(win_printf_OK("k is imaginary !!!"));
	if (-b/(2.*a)<0) 
			return(win_printf_OK("polynomial will change sign!!!\n -b/2a is negative!"));
	if ((b*b-4.*a*c)>0) 
			return(win_printf_OK("discriminent \\Delta = b^2-4ac is positive !!!"));
			
	m = sqrt(a);
	k = sqrt(c);
	
	if ((b+2.*k*m)<0) 
	{		i=win_printf("\\alpha  is imaginary !!!\n\n"
					"cf \\alpha^2 = b+2km\n"
					"with b=%2.3g  and 2km = %2.3g\n"
					"cf k=%2.3g and m=%2.3g\n\nclick OK to continue or CANCEL to dismiss", b, 2.*k*m, k, m);
			if (i==CANCEL) return(D_O_K);
	}
		 
	alpha = sqrt( fabs(b+2.*k*m) );
	omega_1 = sqrt(-b/(2.*a));	// c'est une frequence et non pas une pulsation
	omega_0 = sqrt(k/m); 		// c'est une frequence et non pas une pulsation
	
	if (abs(b*b) < abs(-3.*(b*b-4.*a*c) ) ) 
			return(win_printf_OK("I cannot computes the width, |b| < |-3(b^2-4ac)|"));

	delta   = b*b -4.*a*c;	
	maximum = 1./sqrt( (-delta)/(4.*a) );
	largeur = sqrt( (-b + sqrt(-3.*delta) )/(2.*a) ) - sqrt( (-b - sqrt(-3.*delta) )/(2.*a) );
	
	if ( (alpha*alpha-4.*k*m) > 0 ) win_printf_OK("Discriminent is positive! polynomial will change sign\n this is bad!");

	// plot the fit on all points or just on fitted points ?
	if (bool_plot_on_all_points==1)
	{	i_min=0; 
		i_max=ny-1;	
		N = ny;
	}
	ds2 = create_and_attach_one_ds (op1, N, N, 0);
	if (bool_plot_intermediate==1)
    {  if (do_plot_me!=CANCEL) 
	   {  set_ds_plot_label(ds2, (f_min+f_max)/2., (op1->y_lo+op1->y_hi)/2., USR_COORD, "%s", message);
	   }
   	   free(message); message=NULL;
    }

	
	// plot the fit:
	for (i=i_min; i<=i_max; i++)
	{	ds2->xd[i-i_min] = ds1->xd[i];
		f2         = (double)ds1->xd[i]*(double)ds1->xd[i];
		ds2->yd[i-i_min] = // (float)(1./sqrt( m*m*f2*f2 - (2.*k*m-alpha*alpha)*f2 + k*k ));
					(float)sqrt(1./(a*f2*f2 + b*f2 + c));
					
	}
	m     /= 2.*M_PI*2.*M_PI; 
	alpha /= 2.*M_PI;
	omega_0 *= 2.*M_PI;
	omega_1 *= 2.*M_PI;
	
	inherit_from_ds_to_ds(ds2, ds1);
	ds2->treatement = my_sprintf(ds2->treatement,"cantilever fit using [%f, %f]\nm=%g \\alpha=%g k=%g", 
				ds1->xd[i_min], ds1->xd[i_max], m, alpha, k);
	
	set_ds_plot_label(ds2, ds2->xd[N/2], ds2->yd[N/2], USR_COORD,
				"\\fbox{\\stack{{\\omega_0 = \\sqrt{\\frac{k}{m}}= %2.5g rad/s  =>  f_0 = %2.5g Hz}"
                "{\\omega_1 = \\frac{-b}{2a}= %2.5g rad/s  =>  f_1 = %2.5g Hz}"
				"{m = %2.3g   \\alpha = %2.3g   k = %2.3g}}}", 
				omega_0, omega_0/(2.*M_PI), omega_1, omega_1/(2.*M_PI), m, alpha, k);
	set_ds_plot_label(ds2, f_min, ds2->yd[0], USR_COORD,
				"\\fbox{\\stack{{max PSD (amplitude) is h_{max} = %2.4g}{\\Delta f_{1/2} = %2.4g = \\frac{\\sqrt{3}}{2\\pi}\\alpha/m}}}", 
				maximum, largeur);
				
	if (bool_normalize_with_PSD==1)
	{	moyenne = 0.;
		for (i=3; i<ds1->ny; i++)
		{	moyenne += ds1->yd[i];
		}
		moyenne /= (ds1->ny-3);
		set_ds_plot_label(ds2, f_min, ds2->yd[0], USR_COORD,
				"\\fbox{\\stack{{mean PSD is h_1 = %2.4g}"
				"{h_1m = %2.4g   h_1\\alpha = %2.4g   h_1k = %2.4g}}}", 
				moyenne, m*moyenne, alpha*moyenne, k*moyenne);
				
	}
				
	refresh_plot(pr, UNCHANGED);
	
	cantilever_plot_data(ds1);
	return D_O_K;
} // end of the do_fit_cantilever_PSD






int do_fit_alpha_from_PSD(void)
{
	int i, i_min, i_max, N;
	pltreg *pr = NULL;
	O_p 	*op1=NULL;
	d_s 	*ds1, *ds2;
	int	ny;
	int	index;
static float	f_min=48., f_max=49.;
	float 	x_min, x_max, df, ml, kl;
	double	f2, Sp2, sx, sy;
	double	iN=1., omega_0=0.;
static int	bool_plot_on_all_points=1, bool_impose_limits=1;
	char	*message=NULL;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;


	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine fits an amplitude (V/sqrt(Hz)) PSD\n"
					"and gives \\alpha for (m,k) already known\n\n"
					"a new dataset is created.\n\n");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)	return win_printf_OK("cannot plot region");
	if ( (op1->title==NULL)	|| (op1->y_title==NULL) || (strcmp(op1->y_title,"V/\\sqrt{Hz}")!=0)	)
    {	i=win_printf("I work on amplitude PSDs, but current plot\n"
                        "doesn't seem to be one of them.\n\n"
                        "click OK to continue, or CANCEL to abort");
        if (i==CANCEL) return(D_O_K);
    }

	ny = ds1->ny;	// number of points in time series

	x_min = op1->x_lo; // region from the active x-axis limits
	x_max = op1->x_hi;
	df = ds1->xd[1]- ds1->xd[0];
	N=0; // number of points to work on
	for (i=0; i<ny; i++)  if ( (ds1->xd[i]>=x_min) && (ds1->xd[i]<=x_max) ) N++;
	i_min=0; 	while (ds1->xd[i_min]<f_min) 	i_min++;
	i_max=ny-1; while ( ds1->xd[i_max]>f_max )	i_max = i_max-1; 
	
	ml=(float)m;
	kl=(float)k;
	
	message=my_sprintf(message, "{\\pt14\\color{yellow}Fit \\alpha from a Lorenzian PSD}\n\n"
            "There are a total of %d points in the current dataset.\n\n"
			"I will fit in compact interval [%g, %g], over %d points"
			"(which indices are [%d ; %d]\n\n"
			"%%R plot fit on visible points where it is performed\n"
			"%%r plot fit on all points of dataset\n"
			"impose m = %%8f and k=%%8f\n"
            "%%b don't use limits from plot, but impose [%%5f ; %%5f]\n"
            "(note that \\delta f = %g Hz)",
			ny, x_min, x_max, N, i_min, i_max, df);
	if (win_scanf(message, &bool_plot_on_all_points,
					&ml, &kl, &bool_impose_limits, &f_min, &f_max)==CANCEL) return(D_O_K);
	free(message); message=NULL;
	k = (double)kl;
	m = (double)ml;
	
    if (bool_impose_limits==1)
    {	x_min = f_min;
    	x_max = f_max;
	}
	else 
	{	f_min = x_min;
    	f_max = x_max;
	}
	
	if (x_min>=x_max-3.*df) 
	{	return(win_printf_OK("\\delta f = %g Hz\n"
					"selected domain [%g %g] is impossible to operate on.\n"
					"either f_{min}<=f_{max} or number of points is less than 3", df, x_min, x_max) );
	}

	// let's find again the limits, in case they have been changed by the user	
	N=0;
	for (i=0; i<ny; i++)  if ( (ds1->xd[i]>=x_min) && (ds1->xd[i]<=x_max) ) N++;
	i_min=0; 	while (ds1->xd[i_min]<f_min) 	i_min++;
	i_max=ny-1; while ( ds1->xd[i_max]>f_max )	i_max = i_max-1; 
			
	if (N!=(i_max-i_min+1)) return(win_printf_OK("pb with N"));
	
	if (ds1->xd[i_min]<=0)  return(win_printf_OK("a frequency is zero or negative!"));
	
	// now we fit, according to the formulas in cahier V, p 31
	sx=0.; sy=0.;
	for (i=i_min; i<=i_max; i++)                              // cf cahier VIbis, p133
	{	f2    = (double)(ds1->xd[i]*ds1->xd[i])*(M_PI*M_PI*4.); // omega^2
		Sp2   = (double)1./(ds1->yd[i]*ds1->yd[i]);
	
        sx    = Sp2 - (k-m*f2)*(k-m*f2);
		sy   += sx/f2;
	}
	// cf cahier VI-bis, p133
	iN=(double)1./((double)N);
	alpha = sqrt(sy*iN);

	// plot the fit on all points or just on fitted points ?
	if (bool_plot_on_all_points==1)
	{	i_min=0; 
		i_max=ny-1;	
		N = ny;
	}
	ds2 = create_and_attach_one_ds (op1, N, N, 0);
	
	// plot the fit:
	for (i=i_min; i<=i_max; i++)
	{	ds2->xd[i-i_min] = ds1->xd[i];
		f2         = (double)ds1->xd[i]*(double)ds1->xd[i];
		f2        *= (double)(M_PI*M_PI*4.); // omega^2
		ds2->yd[i-i_min] = 
					(float)sqrt(1./((k-m*f2)*(k-m*f2) + (alpha*alpha*f2)));				
	}
	
	inherit_from_ds_to_ds(ds2, ds1);
	ds2->treatement = my_sprintf(ds2->treatement,"cantilever fit using [%f, %f]\nm=%g k=%g blocked, \\alpha=%g fitted", 
				ds1->xd[i_min], ds1->xd[i_max], m, k, alpha);
	
	omega_0 = sqrt(k/m);
	set_ds_plot_label(ds2, ds2->xd[N/2], ds2->yd[N/2], USR_COORD,
				"\\fbox{\\stack{{\\omega_0 = \\sqrt{\\frac{k}{m}}= %2.5g rad/s  =>  f_0 = %2.5g Hz}"
                "{m = %2.4g and k = %2.4g were blocked}"
                "{fitted \\alpha = %2.4g   }"
                "{so \\frac{\\sqrt{3}}{2\\pi}\\frac{\\alpha}{m} = %2.4g }}}", 
				omega_0, omega_0/(2.*M_PI), m, k, alpha, sqrt(3.)*alpha/(2.*M_PI*m));
				
	refresh_plot(pr, UNCHANGED);
	
	cantilever_plot_data(ds1);
	return D_O_K;
} // end of the do_fit_alpha_from_PSD



// ds1 is the inspiration dataset :
// we get the frequency limits from it, as well as inheritage
int cantilever_plot_data(d_s *ds1)
{	int     i, N;
	pltreg  *pr = NULL;
	O_p 	*op1=NULL, *op2=NULL;
	d_s 	*ds2, *ds3=NULL;
	float	f, l_omega_0;
	double	f2, re, im;
	double	omega_0=0.;
static	int	do_plot_iH2_vs_omega2=0, do_plot_iH2_vs_omega4=0,
            bool_plot_Re_Im=1;

	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op1) != 2)	return win_printf_OK("cannot find plot region");
	N=ds1->nx;
	
    omega_0 = sqrt(k/m);
	l_omega_0=(float)omega_0;
	i=win_scanf("{\\color{yellow}Plot a cantilever response}\n\n"
			"%b \\frac{1}{H^2} vs (\\omega^2-\\omega_0^2) ?\n"
			"%b \\frac{1}{H^2} vs (\\omega^2-\\omega_0^2)^2 ?\n"
			"%b Re(\\frac{1}{H}) and Im(\\frac{1}{H}) vs \\omega \n\n"
			"\\omega_0 = \\sqrt{\\frac{k}{m}} = %7f\n\n"
			"(new plots will be created)", 
            &do_plot_iH2_vs_omega2, &do_plot_iH2_vs_omega4, &bool_plot_Re_Im, &l_omega_0);
	if (i==CANCEL) return(D_O_K); 
	omega_0=(double)l_omega_0;
	

	// first part (useless indeed)
	if (do_plot_iH2_vs_omega2==1)
	{ if ((op2 = create_and_attach_one_plot(pr, N, N, 0)) == NULL)
		return win_printf_OK("cannot create plot !");	
	  ds2 = op2->dat[0];
	  for (i=0; i<N; i++)
	  {	f2         = (double)ds1->xd[i]*(double)ds1->xd[i] - omega_0*omega_0;
		ds2->xd[i] = (float)(f2); 
		ds2->yd[i] = (float)(1./(ds1->yd[i]*ds1->yd[i]));
	  }	
	  if ((ds2 = create_and_attach_one_ds (op2, N, N, 0)) ==NULL)
		return win_printf_OK("cannot create dataset !");
	  for (i=0; i<N; i++)
	  {	f2         = (double)ds1->xd[i]*(double)ds1->xd[i] - omega_0*omega_0;
		ds2->xd[i] = (float)(f2); 
		ds2->yd[i] = (float)(m*m*f2*f2 + alpha*alpha*(f2+omega_0*omega_0) );
	  }			
	  set_plot_title  (op2, "\\stack{{ds0: exp. points}{ds1: fit}}");
	  set_plot_x_title(op2, "(\\omega^2 - \\omega_0^2)  (Hz^2)");
	  set_plot_y_title(op2, "\\frac{1}{H^2} (Hz/V^2)");
	  op2->filename = Transfer_filename(op1->filename);
    	  op2->dir = Mystrdup(op1->dir);
	}

	
	// second part
	if (do_plot_iH2_vs_omega2==1)
	{ if ((op2 = create_and_attach_one_plot(pr, N, N, 0)) == NULL)
		return win_printf_OK("cannot create plot !");	
	  ds2 = op2->dat[0];
	  for (i=0; i<N; i++)
	  {	f2         = (double)ds1->xd[i]*(double)ds1->xd[i] - omega_0*omega_0;
		ds2->xd[i] = (float)(f2*f2); 
		ds2->yd[i] = (float)(1./(ds1->yd[i]*ds1->yd[i]));
	  }	
	  set_ds_plot_label(ds2, ds2->xd[N/4], ds2->yd[N/4], USR_COORD,
				"\\fbox{\\stack{{if you fit by Y=aX+b_0}"
				"{then slope should be a=m^2}"
				"{and b_0 should be \\alpha^2\\omega_0^2}}}");

	  if ((ds2 = create_and_attach_one_ds (op2, N, N, 0)) ==NULL)
		return win_printf_OK("cannot create dataset !");
	  for (i=0; i<N; i++)
	  {	f2         = (double)ds1->xd[i]*(double)ds1->xd[i] - omega_0*omega_0;
		ds2->xd[i] = (float)(f2*f2); 
		ds2->yd[i] = (float)(m*m*f2*f2 + alpha*alpha*(f2+omega_0*omega_0) );
	  }			
	  set_plot_title  (op2, "\\stack{{ds0: exp. points}{ds1: fit}}");
	  set_plot_x_title(op2, "(\\omega^2 - \\omega_0^2)^2  (Hz^2)");
	  set_plot_y_title(op2, "\\frac{1}{H^2} (Hz/V^2)");
	  op2->filename = Transfer_filename(op1->filename);
	  op2->dir = Mystrdup(op1->dir);
	}	
	
	
	if (bool_plot_Re_Im==1)
	{ if ((op2 = create_and_attach_one_plot(pr, N, N, 0)) == NULL) return win_printf_OK("cannot create plot !");	
	  ds2 = op2->dat[0];
	  if ((ds3 = create_and_attach_one_ds (op2, N, N, 0)) ==NULL)  return win_printf_OK("cannot create dataset !"); 
	  for (i=0; i<ds1->nx; i++)
	  {	f          = (double)ds1->xd[i];
	    re         = k - m * 2.*M_PI*2.*M_PI*f*f;
	    im         = alpha*2.*M_PI*f + k_p;
		ds2->xd[i] = ds1->xd[i];
		ds2->yd[i] = (float)(re);
		ds3->xd[i] = ds1->xd[i];
		ds3->yd[i] = (float)(im);
	  }	
	  set_plot_title  (op2, "Re and Im of 1/H (from fit)");
	  set_plot_x_title(op2, "freq (Hz)");
	  set_plot_y_title(op2, "Re, Im ");
	  op2->filename = Transfer_filename(op1->filename);
	  op2->dir = Mystrdup(op1->dir);
    }
	
	refresh_plot(pr, UNCHANGED);
    return(D_O_K);
}// end of the function "cantilever_plot_data"




#ifdef _RESPONSE_H_
int do_build_cantilever_response(void)
{
	register int i;
	O_p 	*op1 = NULL;
	int	ny;
	d_s 	*ds1;
	pltreg *pr = NULL;
	int	index;
	double	omega_0=sqrt(k/m), omega, modulus;
	float	lm=(float)m, lalpha=(float)alpha, lk=(float)k, lk_p=(float)k_p;;

	if(updating_menu_state != 0)	
	{	return D_O_K;	
	}	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine computes the response of a damped spring\n"
					"using frequencies from the x-axis of the current dataset\n\n"
					"no dataset is created, but the current response function is updated");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)	return win_printf_OK("cannot plot region");

	ny = ds1->ny;	// number of points in time series
	
	i=0;
	if (win_scanf("%R input the resonance frequency\n%r input the values of m and k",&index)==CANCEL) 
		return(D_O_K);
	if (index==1)
	{	if (win_scanf("What is the mass m ? %10f\nWhat is the spring constant k ? %10f\n"
					"What is the damping \\alpha ? %10f\nWhat is the visco-elastic k'' ? %10f", &lm, &lk, &lalpha, &lk_p)==CANCEL)
			return(D_O_K);
		m=(double)lm; k=(double)lk; alpha=(double)lalpha; k_p=(double)lk_p;
		if (m!=0) omega_0 = sqrt(k/m);
		else return(win_printf_OK("cannot work if mass is zero!"));
	}
	else if (index==0)
	{	if (win_scanf("What is the resonance frequency \\omega_0 ? %10f\n"
					"What is the damping \\alpha ? %10f\nWhat is the visco-elastic k'' ? %10f", &omega_0, &lalpha, &lk_p)==CANCEL)
			return(D_O_K);
		alpha=(double)(lalpha);
		k_p=(double)lk_p;
	}
	else return(D_O_K);

	//reset_response(ny, (ny-1)*2, ds1->xd[ny-1]*2);
	for (i=0; i<ny; i++)
	{	current_response->f[i] = ds1->xd[i];
		omega                  = (double)ds1->xd[i]*2.*M_PI;
		modulus                = (k-m*omega*omega)*(k-m*omega*omega) + (k_p+alpha*omega)*(k_p+alpha*omega);
		current_response->r[i][0] = (k-m*omega*omega)/modulus;
		current_response->r[i][1] = -(k_p+alpha*omega)/modulus;
	}
	current_response->mode = RESPONSE_PLOT_MODULUS;
	current_response->info = my_sprintf(current_response->info, 
			"damped spring response\nwith m=%g, \\alpha=%g, k=%g, k''=%g,\nf_0=%g Hz, \\omega_0=%g rad/s",
			m, alpha, k, k_p, omega_0/(2*M_PI), omega_0);

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the do_build_cantilever_response
#endif




/**********************************************************************/
/* build a dataset with a signal that is the sum of sinusoides		*/
/* NG, 21/06/2002											*/
/**********************************************************************/
int do_build_excitation_with_sines(void)
{
register int 	i, j;
	O_p 	*op2, *op1 = NULL;
static float amplitude=1., fs=1.;
	float factor=1.;
static	int	nx=100000;
	d_s 	*ds2, *ds1;
	pltreg	*pr = NULL;
	int	f, n_f;
	int	*f_index=NULL;
static char	f_string1[128]=",37";
static char	f_string[128]="2,3,7,11,13,17,19,23,29,31";
static int 	bool_normalize=0, f_mode=1;

	if(updating_menu_state != 0)	return D_O_K;	
//	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine builds a signal composed\n"
				"of a sum of sinusoides with different frequencies, but same amplitude\n"
				"and place it in a new plot\n\n"
				"(you then have to export it to feed NI4711 via F-GEN)");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)		return win_printf_OK("cannot plot region");
	
     i=win_scanf("frequencies ? %s\n"
     		   "frequencies ? %s\n"
     		  "%R frequencies given in Hertz\n"
     		  "%r frequencies given by integers (2\\pi/N units)\n\n"
     		  "number of points N : %10d\n"
     		  "sampling frequency : %10f\n\n"
			"amplitude ? %f %b normalize amplitude by the number of sines",
			&f_string,&f_string1, &f_mode, &nx, &fs, &amplitude, &bool_normalize);
	if (i==CANCEL) return OFF;
	
	strcat(f_string,f_string1);
	f_index = str_to_index(f_string, &n_f);	// malloc is done by str_to_index
	if ( (f_index==NULL) || (n_f<1) )	return(win_printf("bad values for the frequencies !"));

	if (f_mode==0) // frequencies given in Hertz
	{	factor = fs*2*M_PI/((float)nx);
	}
	else // frequencies given in integers, number of period in the box
	{	factor = 2*M_PI/((float)nx);
	}

	if ((op2 = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	ds2 = op2->dat[0];
	for (j=0; j<nx; j++)
	{	ds2->xd[j] = (float)j/(float)fs; 
		ds2->yd[j] = 0.;
	}
	
	for (i=0; i<n_f; i++)
	{	f=f_index[i];
		for (j=0; j<nx; j++)
		{	ds2->yd[j] += amplitude*sin(f*factor*j);
		}
	}
	
	if (bool_normalize==1)
	{	for (j=0; j<nx; j++)
		{	ds2->yd[j] /= (float)n_f;
		}
	}
	
	set_plot_title(op2,"\\stack{{generated signal with %d frequencies}{%s}}", n_f, f_string);
	op2->filename = Transfer_filename(op1->filename);
    	op2->dir = Mystrdup(op1->dir);

	return refresh_plot(pr, UNCHANGED);
}




/**********************************************************************/
/* build a dataset with a signal that is a white noise 			*/
/* in a given frequency range									*/
/* NG, 27/06/2002											*/
/**********************************************************************/
int do_build_white_noise(void)
{
register int 	i, j;
	O_p 	*op2, *op1 = NULL;
static float amplitude=1., fs=1024.;
	float factor=1., f, f_min=0., f_max=10., phase=0.;
static	int	nx=100000;
	d_s 	*ds2, *ds1;
	pltreg	*pr = NULL;
static int 		f_mode=0;
	double		*x;
	fftw_complex 	*xf=NULL;
	fftw_plan 	plan;
	gsl_rng *r=NULL;  			/* random generator 			*/
	int	bool_random_phase=1;
	int n_freq=0;
	

	if(updating_menu_state != 0)	return D_O_K;	
//	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine builds a signal composed\n"
				"of a white noise in a given frequency range\n"
				"and place it in a new plot\n\n"
				"(you then have to export it to feed NI4711 via F-GEN)");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)		return win_printf_OK("cannot plot region");
	
     i=win_scanf("min frequency ? %10f\n"
     		  "max frequency ? %10f\n"
     		  "%R frequencies given in Hertz\n"
     		  "%r frequencies given by integers (2\\pi/N units)\n\n"
     		  "number of points N : %10d\n"
     		  "sampling frequency : %10f\n\n"
			"amplitude ? %8f\n"
			"%b random phase",
			&f_min, &f_max, &f_mode, &nx, &fs, &amplitude, &bool_random_phase);
	if (i==CANCEL) return OFF;
	if (f_min>f_max) return(win_printf_OK("Don't you think you should ask for f_{min}=%g <= f_{max} = %g, Hu ?", f_min, f_max));

	if (f_mode==0) // frequencies given in Hertz
	{	factor = fs*2*M_PI/((float)nx);
	}
	else // frequencies given in integers, number of period in the box
	{	factor = 2*M_PI/((float)nx);
	}

	if ((op2 = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	x  = (double*)      fftw_malloc((nx)    *sizeof(double));
	xf = (fftw_complex*)fftw_malloc((nx/2+1)*sizeof(fftw_complex));
	ds2 = op2->dat[0];
	for (j=0; j<nx; j++)
	{	ds2->xd[j] = (float)j/(float)fs; 
		ds2->yd[j] = 0.;
		x[j]       = 0.;
	}
	
	plan = fftw_plan_dft_r2c_1d(nx, x, xf, FFTW_ESTIMATE);
	fftw_execute(plan);
	fftw_destroy_plan(plan);
        		
	random_generator_type = gsl_rng_default;
	gsl_rng_env_setup();	// 3 lines to init the random number generator
	r = gsl_rng_alloc(random_generator_type);
	  	
	n_freq=0;
	for (i=0; i<nx/2+1; i++)
	{	f=factor*(double)i/(2*M_PI);
		if ( (f_min<=f) && (f<=f_max) )
		{	if (bool_random_phase==1)
			{	phase = (float)gsl_ran_gaussian(r, (double)(1.0))*2*M_PI; 
			}
			else
			{	phase = 0.;
			}
			xf[i][0]=(float)cos(phase);
			xf[i][1]=(float)sin(phase);
			n_freq++;
		}
		else 
		{	xf[i][0]=0.;
			xf[i][1]=0.;
		}
	}
	
	if(n_freq==0) win_printf("no frequency in the signal ???");
	
	plan = fftw_plan_dft_c2r_1d(nx, xf, x, FFTW_ESTIMATE);
	fftw_execute(plan);
	fftw_destroy_plan(plan);
	
	for (j=0; j<nx; j++)
	{	ds2->yd[j] = (float)x[j]*amplitude/nx;
	}
	fftw_free(x); fftw_free(xf);
	
	set_plot_title(op2,"\\stack{{white noise}{in frequency range [%g, %g]}}", f_min, f_max);
	op2->filename = Transfer_filename(op1->filename);
    	op2->dir = Mystrdup(op1->dir);

	return refresh_plot(pr, UNCHANGED);
}
/* end of 'do_build_white_noise' */




#ifdef _RESPONSE_H_
/**********************************************************************/
/* build a dataset with a signal that is the sum of sinusoides		*/
/* NG, 21/06/2002											*/
/**********************************************************************/
int do_extract_response_on_sines(void)
{
register int 	i, j;
	O_p 	*op2, *op1 = NULL;
	double rp,ip;
	float fs=current_response->fs;
	float factor=1., factor_from_Hz_to_integer=1.;
	int	 n_fft=current_response->n_fft;
	d_s 	*ds2, *ds1;
	pltreg	*pr = NULL;
	int	n_f;
	int	*f_index=NULL;
static char	f_string[128]="2,3,7,11,13,17,19,23,29,31";
static char	f_string1[128]=",37";
char		message[512];
static int  f_mode=0;

	if(updating_menu_state != 0)	
	{	if (current_response->mode==0)	active_menu->flags |=  D_DISABLED;
		else	 			active_menu->flags &= ~D_DISABLED; 
		return D_O_K;	
	}

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine computes the discrete response function on selected frequencies\n"
				"when the excitation is a sum of sinusoides with different given frequencies\n"
				"\n\n"
				"The continuous response has to be already computed, and present in the memory\n"
				"4 new datasets are created corresponding respectively to\n"
				"  - the modulus\n  - the real part\n  - the imaginary part\n  - the phase\n"
                "the current response function is NOT updated.");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)		return win_printf_OK("cannot plot region");
	
	sprintf(message, "excited frequencies ? %%s\n"
			"excited frequencies ? %%s\n"
     		  "%%R frequencies given in Hertz\n"
     		  "%%r frequencies given by integers (2\\pi/N {FFT})\n\n"
     		  "number of points N_{FFT}in the FFT window : %%8d\n"
     		  "sampling frequency : %%10f\n\n");
	
     i=win_scanf(message,	&f_string,&f_string1, &f_mode, &n_fft, &fs);
	if (i==CANCEL) return OFF;

	strcat(f_string,f_string1);
	f_index = str_to_index(f_string, &n_f);	// malloc is done by str_to_index
	if ( (f_index==NULL) || (n_f<1) )	return(win_printf("bad values for the frequencies !"));

	if (f_mode==0) // frequencies given in Hertz
	{	factor_from_Hz_to_integer = (float)n_fft/fs;
		factor				 = 1.;
	}
	else // frequencies given in integers, number of periods in the box
	{	factor_from_Hz_to_integer = 1.;
		factor				 = (float)fs/n_fft;
	}
	
	// here we check the consistancy of frequencies definition:
	for (j=0; j<n_f; j++)
	{    i = (int)(f_index[j]*factor_from_Hz_to_integer); // i is the index of the frequency on the response that corresponds to f_index[j] Hz
		win_printf("frequency number %d/%d\n"
				"it corresponds to %d Hz which is %d in integer units (*2\\pi/N_{FFT})\n"
				"it corresponds to index %d/%d",
						j+1, n_f, f_index[j]*factor, (int)(factor_from_Hz_to_integer*f_index[j]), i, current_response->n);
		if (f_index[j]>current_response->n-1)
		return(win_printf_OK("frequency number %d is the %d/%d, ie too large!",
						j, f_index[j], current_response->n));	
		if (f_index[j]*factor != current_response->f[i]) // comparison of frequencies, in Hertz units
		return(win_printf_OK("working on frequency number %d\n"
						"which is the %d/%d and corresponds to %g Hz"
						"but doesn't match with the response frequency %g\n",
						j, f_index[j], n_fft/2-1, (float)f_index[j]*factor, current_response->f[i]));
	}

	if ((op2 = create_and_attach_one_plot(pr, n_f, n_f, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	ds2 = op2->dat[0];								// dataset with the modulus
	for (j=0; j<n_f; j++)		
	{	i = (int)(f_index[j]*factor_from_Hz_to_integer);
		rp = current_response->r[ i ][0]; // real part of the response
		ip = current_response->r[ i ][1]; // imaginary part
		ds2->xd[j] = (float)f_index[j]*factor; 
		ds2->yd[j] = (float)sqrt(rp*rp+ip*ip);
	}
	set_ds_dot_line(ds2);
	set_ds_point_symbol(ds2, "\\pt6\\oc");
	ds2->treatement = my_sprintf(ds2->treatement,"modulus of discrete response function");

	
	ds2 = create_and_attach_one_ds (op2, n_f, n_f, 0);	// dataset with the real part
	for (j=0; j<n_f; j++)
	{	i = (int)(f_index[j]*factor_from_Hz_to_integer);
		ds2->xd[j] = (float)f_index[j]*factor; 
		ds2->yd[j] = (float)current_response->r[ i ][0];
	}
	set_ds_dot_line(ds2);
	set_ds_point_symbol(ds2, "\\pt6\\oc");
	ds2->treatement = my_sprintf(ds2->treatement,"real part of discrete response function");
	
	ds2 = create_and_attach_one_ds (op2, n_f, n_f, 0);	// dataset with the imaginary part
	for (j=0; j<n_f; j++)
	{	i = (int)(f_index[j]*factor_from_Hz_to_integer);
		ds2->xd[j] = (float)f_index[j]*factor; 
		ds2->yd[j] = (float)current_response->r[ i ][1];
	}
	set_ds_dot_line(ds2);
	set_ds_point_symbol(ds2, "\\pt6\\oc");
	ds2->treatement = my_sprintf(ds2->treatement,"imaginary part of discrete response function");
	
	ds2 = create_and_attach_one_ds (op2, n_f, n_f, 0);	// dataset with the phase
	for (j=0; j<n_f; j++)
	{	i = (int)(f_index[j]*factor_from_Hz_to_integer);
		rp = current_response->r[ i ][0]; // real part of the response
		ip = current_response->r[ i ][1]; // imaginary part
		ds2->xd[j] = (float)f_index[j]*factor; 
		ds2->yd[j] = (float)atan2(ip, rp);
	}
	set_ds_dot_line(ds2);
	set_ds_point_symbol(ds2, "\\pt6\\oc");
	ds2->treatement = my_sprintf(ds2->treatement,"phase of discrete response function");
	
	set_plot_title(op2,"\\stack{{discrete response function with %d frequencies}{%s}}", n_f, f_string);
	op2->filename = Transfer_filename(op1->filename);
    	op2->dir = Mystrdup(op1->dir);

	return refresh_plot(pr, UNCHANGED);
}
/* end of "int do_extract_response_on_sines" */
#endif




int do_correct_turbulence_PSD(void)
{
	int i, i_min, i_max, i_s, i_e, N;
	pltreg *pr = NULL;
	O_p 	*op1=NULL;
	d_s 	*ds1, *ds2;
	int	ny;
	int	index;
	float	f_min, f_max;
	double	x, y, sx1, sx2, sy, syx, delta, delta_a, delta_b;
	double	iN=1., a=0., b=0.;
	int		bool_plot_on_all_points=0, bool_use_actual_PSD=1;
	char		s[256];

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;


	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine corrects an amplitude (V/sqrt(Hz)) PSD\n"
					"by dividing it with a power-law spectrum\n\n"
					"The slope p of the power law is measured on the PSD itself.\n"
					"a new dataset is created.");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)	return win_printf_OK("cannot plot region");
	if ( (op1->title==NULL)	|| (op1->y_title==NULL) || (strcmp(op1->y_title,"V/\\sqrt{Hz}")!=0)	)
    {	i=win_printf("I work on amplitude PSDs, but current plot\n"
                        "doesn't seem to be one of them.\n\n"
                        "click OK to continue, or CANCEL to abort");
        if (i==CANCEL) return(D_O_K);
    }

	ny = ds1->ny;	// number of points in time series

	f_min = op1->x_lo;	// visible plot limits in the X-direction
	f_max = op1->x_hi;

	N=0;		// N will ne the number of points to perform the fit over
	for (i=0; i<ny; i++)
	{	if ( (ds1->xd[i]>=f_min) && (ds1->xd[i]<=f_max) )
		{	N++;
		}
	}
	i_min=0;
	while (ds1->xd[i_min]<f_min) 	
	{	i_min++;
	}
	i_max=ny-1;
	while ( ds1->xd[i_max]>f_max )
	{	i_max = i_max-1; 
	}
		
	sprintf(s,"I will fit ia power law in the compact interval [%g, %g], over %d points\n"
			"which indices are [%d, %d]\n\n"
			"%%R plot fit on visible points where it is performed\n"
			"%%r plot fit on all points of dataset", 
			f_min, f_max, N, i_min, i_max);
	i=win_scanf(s, &bool_plot_on_all_points);
			
	if (N!=(i_max-i_min+1)) return(win_printf_OK("pb with N"));

	// now we fit the power law
	sx1=0.; sx2=0.; sy=0; syx=0.;
	for (i=i_min; i<=i_max; i++)
	{	x    = log((double)ds1->xd[i]);
		y    = log((double)ds1->yd[i]);
		sx1 += x;
		sx2 += x*x;
		sy  += y;
		syx += y*x;
	}
	// fit y = ax + b
	// cf cahier V, p 31
	iN=(double)1./((double)N);
	delta   = sx2 - sx1*sx1*iN;
	delta_a = syx  - sy*sx1*iN;
	delta_b = (sx2*sy - syx*sx1)*iN;
	
	if (delta==0) return(win_printf_OK("exponent p will be infinite !"));
	a     = delta_a/delta;
	b     = delta_b/delta; 
	
	sprintf(s,"\\fbox{\\stack{{A(\\omega) = A_0 \\omega^p}"
			"{p = %2.4g    A_0 = %2.4g}}}", a, exp(b));
				
	// plot the fit oon all points or just on fitted points ?
	if (bool_plot_on_all_points==1)
	{	i_s=0; 
		i_e=ny-1;	
		N = ny;
	}
	else
	{	i_s = i_min;
		i_e = i_max;
	}
	ds2 = create_and_attach_one_ds (op1, N, N, 0);
	set_ds_plot_label(ds2, (f_min+f_max)/2., (op1->y_lo+op1->y_hi)/2., USR_COORD, "%s", s);
	
	// plot the fit:
	for (i=i_s; i<=i_e; i++)
	{	ds2->xd[i-i_s] = ds1->xd[i];
		ds2->yd[i-i_s] = (float)exp( a*log(ds1->xd[i]) + b);
	}
	inherit_from_ds_to_ds(ds2, ds1);
	ds2->treatement = my_sprintf(ds2->treatement,"power-law fit using [%f, %f]\n p=%g", 
				f_min, f_max, a);
					
	refresh_plot(pr, UNCHANGED);
	
	// OK, now the fit is computed and plotted, we divide the PSD by it.
	i=win_scanf("Now, I will divide the PSD by the fit.\n\n"
			"%b use actual PSD instead of fit for \\omega < f_{min} \n",
			&bool_use_actual_PSD);
	if (i==CANCEL) return(D_O_K);
	
	ds2 = create_and_attach_one_ds (op1, ny, ny, 0);
	
	for (i=0; i<ny; i++)
	{	ds2->xd[i] = ds1->xd[i];
		if ( (i<i_min) && (bool_use_actual_PSD) )
		{	ds2->yd[i] = 1.;
		}
		else
		{	ds2->yd[i] = ds1->yd[i] / ((float)exp( a*log(ds1->xd[i]) + b));
		}
	}	
	inherit_from_ds_to_ds(ds2, ds1);
	ds2->treatement = my_sprintf(ds2->treatement,"divided by power-law fit with exponent p=%g", a);
	
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the do_correct_turbulence_PSD



/****************************************************************/
/* the function below computes the auto-correlation from a psd	*/
/****************************************************************/
int do_correct_turbulence_PSD_by_filter(void)
{
	int		ny, i;
	pltreg *pr = NULL;
	O_p 	*op1=NULL;
	d_s 	*ds1, *ds2;
	int		index;
static int		bool_use_omega=1;
static float	a=0, b=1., c=0., d=1.;
	float 		f, up, down;
    

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine corrects a PSD by multiplying it by\n"
					"the modulus of: \n\n"
					"\\frac{a+ib\\omega}{c+id\\omega}\n\n"
					"a new dataset is created.");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)	return win_printf_OK("cannot find data");
	if (op1->y_title==NULL) 
    {	i=win_printf("I work on amplitude or energy PSDs, but current plot\n"
                        "doesn't seem to be one of them.\n\n"
                        "click OK to continue, or CANCEL to abort");
        if (i==CANCEL) return(D_O_K);
    }
    
	ny      = ds1->ny;		// number of points in time series
	
	i=win_scanf("{\\color{yellow}\\pt14 Correctionof a PSD}\n\n"
			"I will multiply active PSD by the modulus of\n\n"
			"\\frac{a+ib\\omega}{c+id\\omega}\n\n"
			"a=%8f    b=%8f (Hz^{-1})\n"
			"c=%8f    d=%8f (Hz^{-1})\n\n"
			"%b use \\omega=2\\pi f instead of f.", 
			&a, &b, &c, &d, &bool_use_omega);
	if (i==CANCEL) return(D_O_K);
				
	if ( (c==0)	&& (d==0) ) return(win_printf_OK("c and d are zero !"));
			
    ds2 = duplicate_data_set(ds1, NULL);
    if (ds2 == NULL)    return (win_printf_OK("can't create data set"));
    
	for (i=0; i < ny; i++)
    {   f		    = ds1->xd[i]; // frequency
    	if (bool_use_omega==1) f *= 2.*M_PI;
	    up 		    = a*a + b*b*f*f;
    	down 		= c*c + d*d*f*f;
	    ds2->yd[i] *= up / down;
    }

    inherit_from_ds_to_ds(ds2, ds1);
	ds2->treatement = my_sprintf(ds2->treatement,"corrected by a filter (a+ibf)/(c+id)");
	
    add_one_plot_data (op1, IS_DATA_SET, (void *)ds2);
	
	refresh_plot(pr, UNCHANGED);
	return(D_O_K);
}/* end of 'do_correct_turbulence_PSD_by_filter' */

/****************************************************************/
/* the function below computes the auto-correlation from a psd	*/
/****************************************************************/
int do_autocorrelation_from_PSD(void)
{
	float 	f_acq;
	int		n_fft, n_fft_2, ny, i;
	pltreg *pr = NULL;
	O_p 	*op1=NULL, *op2=NULL;
	d_s 	*ds1, *ds2;
	int		index;
	int		bool_is_amplitude=1, bool_half_points=1, bool_normalize=1;
	double  		*back=NULL, *cor_d=NULL;
	fftw_complex	*out3=NULL;
    double 			moy1, factor;
    fftw_plan 		plan_back;     // fft plan for fftw3
    

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine computes the auto-correlation from a PSD\n"
					"\n"
					"a new plot is created.");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)	return win_printf_OK("cannot find data");
	if (op1->y_title==NULL) 
    {	i=win_printf("I work on amplitude or energy PSDs, but current plot\n"
                        "doesn't seem to be one of them.\n\n"
                        "click OK to continue, or CANCEL to abort");
        if (i==CANCEL) return(D_O_K);
    }
    else if (strcmp(op1->y_title,"V/\\sqrt{Hz}")==0)
    {	bool_is_amplitude=1;
	}
	else if (strcmp(op1->y_title,"V^2/Hz")==0)
	{	bool_is_amplitude=0;
	}
	ny      = ds1->ny;		// number of points in time series
	n_fft   = (ny-1)*2;
	n_fft_2 = ny-1;	
	f_acq   = ds1->xd[n_fft_2]*2.;
	
	i=win_scanf("{\\color{yellow}\\pt14 Autocorrelation from PSD}\n\n"
			"current PSD is in %R energy (V^2/Hz) or %r amplitude (V/\\sqrt{Hz}) units\n"
			"f_{acq} = %8f Hz", &bool_is_amplitude, &f_acq);
			
	out3 = fftw_malloc((n_fft/2+1)*sizeof(fftw_complex));
	back = fftw_malloc( n_fft	  *sizeof(double));
	cor_d= fftw_malloc( n_fft	  *sizeof(double));
	
    plan_back = fftw_plan_dft_c2r_1d(n_fft, out3,  back, FFTW_ESTIMATE);   
    	
	// ci-dessous, on calcule la moyenne:
    moy1 = 0.;	
    	
    // ci-dessous, multiplication de out1 par le complexe conjugue de out2:
    if (bool_is_amplitude)
    for (i=1; i<n_fft/2+1; i++)
    {   out3[i][0] = (double)ds1->yd[i]*(double)ds1->yd[i]; // module carre
       	out3[i][1] = (double)0.;
    }
    else
    {   out3[i][0] = (double)ds1->yd[i]; // module deja au carre
       	out3[i][1] = (double)0.;
    }
    out3[0][0] = 0.;
    out3[0][1] = 0.;
    	
    // ci-dessous, transformee de fourier inverse:
    fftw_execute(plan_back); 

    if (bool_half_points==1)
    op2 = create_and_attach_one_plot(pr, n_fft_2, n_fft_2, 0);	// dataset with the imaginary part
    else
    op2 = create_and_attach_one_plot(pr, n_fft, n_fft, 0);	// dataset with the imaginary part
    
	ds2 = op2->dat[0];
    
    for (i=0 ; i < n_fft/2 ; i++)
    {   cor_d[i+n_fft_2] = back[i];
       	cor_d[i]         = back[i+n_fft_2];
    }
    	
	if (bool_normalize==1)	factor = 1./cor_d[n_fft_2];
	else					factor=(double)1./((double)k*(double)n_fft*(double)n_fft);
	
	if (bool_half_points==1)
	for (i=0 ; i < n_fft_2 ; i++)
    {   ds2->yd[i] = (float)cor_d[i+n_fft_2] * factor;
        ds2->xd[i] = (float)i/f_acq;
    }
	else // all n_fft points, ie, negative values of time are also output
	for (i=0 ; i < n_fft_2 ; i++)
    {   ds2->yd[i+n_fft_2] = (float)(cor_d[i+n_fft_2] * factor);
        ds2->yd[i]         = (float)(cor_d[i] * factor);
        ds2->xd[i]         = (float)(i-n_fft_2)/f_acq;		// note : 1/f_acq = dt
        ds2->xd[i+n_fft_2] = (float)i/f_acq;
    }
    	
    fftw_free(out3);
    fftw_free(back);
    fftw_destroy_plan(plan_back);
    fftw_free(cor_d);
	
    set_plot_title(op2,"\\stack{{autocorrelation from PSD}{%s}}", op1->title);
	op2->filename = Transfer_filename(op1->filename);
    op2->dir = Mystrdup(op1->dir);

    inherit_from_ds_to_ds(ds2, ds1);
	ds2->treatement = my_sprintf(ds2->treatement,"autocorrelation from PSD");
	
	refresh_plot(pr, UNCHANGED);
	return(D_O_K);
}/* end of 'do_autocorrelation_from__PSD' */


/****************************************************************/
/* the function below computes the auto-correlation from a psd	*/
/****************************************************************/
int do_turbulence_insert_info(void)
{
	int		i;
static char texte[256]="Omega=";
	pltreg *pr = NULL;
	O_p 	*op1=NULL;
	d_s 	*ds1;
	int		index;
	int		bool_where=0, bool_cr=1;
	

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine inserts some text in ds->src or ds->history\n"
					"\n"
					"the active dataset is modified.");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)	return win_printf_OK("cannot find data");
	
	i=win_scanf("{\\color{yellow}\\pt14 Insert some info in a dataset}\n\n"
			"insert the following text %s"
			"in dataset's\n"
			"%R source\n"
			"%r history\n"
			"%r treatment\n"
            "(%b add carriage return before text)", &texte, &bool_where, &bool_cr);
	if (i==CANCEL) return(D_O_K);
	
	if (bool_cr==1) ds1->source = my_sprintf(ds1->source, "\n");
	
	if      (bool_where==0)     ds1->source     = my_sprintf(ds1->source,    "%s",texte);
	else if (bool_where==1)     ds1->history    = my_sprintf(ds1->history,   "%s",texte);
	else if (bool_where==2)     ds1->treatement = my_sprintf(ds1->treatement,"%s",texte);
	
	refresh_plot(pr, UNCHANGED);
	return(D_O_K);
}/* end of 'do_turbulence_insert_info' */






/****************************************************************/
/* the function below rescales X or Y with a power of a constant	*/
/****************************************************************/
int do_turbulence_scaling(void)
{	register int i, k;
	int	*ind;
	d_s 	*ds2=NULL, *ds;
static char	texte[128]="Omega=";
    char   *s=NULL;
	O_p	*op;
	pltreg *pr = NULL;
	int	index, n, n_ds=1, Re;
	float      Re_f;
static int  px=-3, qx=4, py=-2, qy=1;
static int	bool_Y=1, bool_X=0, bool_int_or_float=0;
	
	if(updating_menu_state != 0)	return D_O_K;		
       	index = active_menu->flags & 0xFFFFFF00;
	if ( (index!=TURBULENCE_ONE) && (index!=TURBULENCE_ALL) ) return(win_printf_OK("bad call!"));

	if (key[KEY_LSHIFT])	return win_printf_OK("This routine rescales one or all dataset(s)"
		"(the current dataset), and places it in a new dataset\n");
	
	if (win_scanf("{\\color{yellow}\\pt14 Rescale dataset(s)}\n\n"
		"%b in X such that X -> X.a^{p_x/q_x} \n\n"
		"with p_x=%6d and q_x=%6d\n"
		"%b in Y such that Y -> Y.a^{p_y/q_y} \n\n"
		"with p_y=%6d and q_y=%6d\n"
		"a is %R integer %r float taken from dataset info, using string %s",	
		&bool_X, &px, &qx, &bool_Y, &py, &qy, &bool_int_or_float, &texte) == CANCEL) return(D_O_K);

	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)		return win_printf_OK("cannot plot region");
	
	if (index&TURBULENCE_ONE)
	{ 	n_ds  = 1;
		ind   = (int*)calloc(n_ds,sizeof(int));
		ind[0]= op->cur_dat;
	}
	else
	{	n_ds  = op->n_dat;
		ind   = (int*)calloc(n_ds,sizeof(int));
		for (i=0; i<n_ds; i++) ind[i]=i;
	}

	for (k=0; k<n_ds; k++)
	{	ds =  op->dat[ ind[k] ];
		if (ds == NULL)   return (win_printf_OK("can't find data set"));

        if (bool_int_or_float==0)
		{  Re = grep_int_in_string(ds->source, texte);
		   if (Re==-1) 	
		   {	Re = grep_int_in_string(ds->history, texte);
			    if (Re==-1) 	
			    {	Re = grep_int_in_string(ds->treatement, texte);
			    }
		   }
		   Re_f = (float)Re;
        }
        else 
		{  Re_f = grep_float_in_string(ds->source, texte);
		   if (Re_f==0) 	
		   {	Re_f = grep_int_in_string(ds->history, texte);
			    if (Re_f==0) 	
			    {	Re_f = grep_int_in_string(ds->treatement, texte);
			    }
		   }
        }
        n=ds->nx;
// win_printf("dataset %d\n npts = %d\n, Re=%d", k, n, Re);

		if (Re_f>0)
		{  ds2 = create_and_attach_one_ds (op, n, n, 0);
           if (ds2==NULL)	return(win_printf_OK("cannot create dataset !"));
           for (i=0; i<n; i++) 
           {   ds2->xd[i] = ds->xd[i]; 
               ds2->yd[i] = ds->yd[i]; 
           }
		   inherit_from_ds_to_ds(ds2, ds);
           ds2->color = ds->color;
           ds2->m     = ds->m; // we keep the line style
		   set_ds_point_symbol(ds2, get_ds_point_symbol(ds));


		   if (bool_X==1)	
		   {	for (i=0; i<n; i++)
			{	ds2->xd[i] *= (float)exp( log((double)Re_f)*(double)px/(double)qx );
			}
			ds2->treatement = my_sprintf(ds2->treatement, "scaled in X by %g^{%d/%d}", Re_f, px, qx);
		   }
		
		   if (bool_Y==1)
		   {	for (i=0; i<n; i++)
			{	ds2->yd[i] *= (float)exp( log((double)Re_f)*(double)py/(double)qy );
			}
			ds2->treatement = my_sprintf(ds2->treatement, "scaled in Y by %g^{%d/%d}", Re_f, py, qy);
		   }

		   
		}
	}
	free(ind);
	
	if (n_ds>1)	// if we worked on all datasets...
	if (win_scanf("Remove old datasets ?")!=CANCEL)
	{	for (k=0; k<n_ds; k++) remove_ds_n_from_op (op, 0);	// always remove the 0th because counting is shifted after any one is deleted
		if (bool_X==1) 
		{	s=my_sprintf(s, "%s %s^{%d/%d}", op->x_title, texte, px, qx);
			set_plot_x_title(op, s);
			free(s); s=NULL;
		}
		if (bool_Y==1) 
		{	s=my_sprintf(s, "%s %s^{%d/%d}", op->y_title, texte, py, qy);
			set_plot_y_title(op, s);
			free(s); s=NULL;
		}
	}
	set_plot_x_auto_range(op);
	set_plot_y_auto_range(op);
	
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
}






MENU *turbulence_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	
	add_item_to_menu(mn,"Build white/colored noise",		do_build_white_noise,		 	NULL, 0, NULL);
	add_item_to_menu(mn,"Build Excitation with sines",		do_build_excitation_with_sines, NULL, 0, NULL);

#ifdef _RESPONSE_H_
	add_item_to_menu(mn,"Extract Response on sines",		do_extract_response_on_sines,	NULL, 0, NULL);	
#endif
	add_item_to_menu(mn,"\0",								0,								NULL, 0, NULL);
	add_item_to_menu(mn,"Fit PSD by theor. (k,m,a)",		do_fit_cantilever_PSD,			NULL, 0, NULL);
	add_item_to_menu(mn,"Fit PSD by theor. (a only)",	    do_fit_alpha_from_PSD,			NULL, 0, NULL);
	add_item_to_menu(mn,"correct turbulence PSD by K41",	do_correct_turbulence_PSD,		NULL, 0, NULL);
#ifdef _RESPONSE_H_
	add_item_to_menu(mn,"Fit Response Function by theor.",	do_fit_cantilever_response,		NULL, 0, NULL);
	add_item_to_menu(mn,"Fit Response Function + LP filter",do_fit_cantilever_LP_response,	NULL, 0, NULL);
	add_item_to_menu(mn,"Build theor. Response Function",	do_build_cantilever_response,	NULL, 0, NULL);
#endif		
	add_item_to_menu(mn,"\0",								0,								NULL, 0, NULL);
	add_item_to_menu(mn,"correct PSD by frequency",			do_correct_turbulence_PSD_by_filter,	NULL, 0, NULL);
	add_item_to_menu(mn,"autocorrelation from PSD",			do_autocorrelation_from_PSD,	NULL, 0, NULL);
	add_item_to_menu(mn,"\0",								0,								NULL, 0, NULL);
	add_item_to_menu(mn,"insert Re info",			        do_turbulence_insert_info,	NULL, 0, NULL);
	add_item_to_menu(mn,"scaling with Re (1 dataset)",		do_turbulence_scaling,	NULL, TURBULENCE_ONE, NULL);
	add_item_to_menu(mn,"scaling with Re (all dataset)",	do_turbulence_scaling,	NULL, TURBULENCE_ALL, NULL);
	
	return mn;
}


int	turbulence_main(int argc, char **argv)
{
	add_plot_treat_menu_item ("turbulence", NULL, turbulence_plot_menu(), 0, NULL);
	return D_O_K;
}


int	turbulence_unload(int argc, char **argv)
{ 
	remove_item_to_menu(plot_treat_menu,"turbulence",	NULL, NULL);
	return D_O_K;
}
#endif

