#ifndef _TURBULENCE_H_
#define _TURBULENCE_H_

// definitions for scaling dataset(s):
#define TURBULENCE_ONE		0x0100
#define TURBULENCE_ALL		0x0200

#ifdef _RESPONSE_H_
PXV_FUNC(int, do_fit_cantilever_response,		(void));
PXV_FUNC(int, do_build_cantilever_response,		(void));
PXV_FUNC(int, do_extract_response_on_sines,		(void));
#endif

PXV_FUNC(int, do_build_excitation_with_sines,	(void));
PXV_FUNC(int, do_extract_response_on_sines,		(void));
PXV_FUNC(int, do_fit_cantilever_PSD,			(void));
PXV_FUNC(int, cantilever_plot_data,			    (d_s *ds1));
PXV_FUNC(int, do_build_white_noise,			    (void));

PXV_FUNC(MENU*, turbulence_plot_menu, 			(void));
PXV_FUNC(int, turbulence_main, 				(int argc, char **argv));
PXV_FUNC(int, turbulence_unload,				(int argc, char **argv));

#endif

