#ifndef _FIT_CHI2_H_
#define _FIT_CHI2_H_

int my_polyfit (int n, int nx, double *xd, double *yd, float *coefs);
int fit_Im_HP  (       int nx, double *xd, double *yd, float *coefs);

#endif

