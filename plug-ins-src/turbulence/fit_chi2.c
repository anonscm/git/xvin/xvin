#ifndef _FIT_CHI2_C_
#define _FIT_CHI2_C_

#include <gsl/gsl_multifit.h>
     
#include "fit_chi2.h"

/* 
This function performs a quadratic fit y = c_0 + c_1 x + c_2 x^2 to a weighted dataset using the generalised linear fitting function gsl_multifit_wlinear. The model matrix X for a quadratic fit is given by,

     X = [ 1   , x_0  , x_0^2 ;
           1   , x_1  , x_1^2 ;
           1   , x_2  , x_2^2 ;
           ... , ...  , ...   ]

where the column of ones corresponds to the constant term c_0. The two remaining columns corresponds to the terms c_1 x and c_2 x^2.

*/
/* n is the degree of the polynomial                                       */
/* coefs contains the results : the (n+1) coefficients of the polynomial   */
int my_polyfit(int n, int nx, double *xd, double *yd, float *coefs)
{
       int i, j;
       double xi, yi, chisq, tmp;
       gsl_matrix *X, *cov;
       gsl_vector *y, *c;
     
       // nx is the number of points 
       // n  is the degree of the polynomial 
     
       X = gsl_matrix_alloc (nx, n+1);
       y = gsl_vector_alloc (nx);
      
       c = gsl_vector_alloc (n+1);
       cov = gsl_matrix_alloc (n+1, n+1);
     
       for (i = 0; i < nx; i++)
       {   xi = (double)xd[i];
           yi = (double)yd[i];
           
           gsl_matrix_set (X, i, 0, 1.0);
           
           tmp = xi;
           for (j=1; j<=n; j++)
           {  gsl_matrix_set (X, i, j, tmp);
              tmp *= xi;
           }
           
           gsl_vector_set (y, i, yi);
          }
           
         gsl_multifit_linear_workspace *work = gsl_multifit_linear_alloc (nx, n+1);
         gsl_multifit_linear (X, y, c, cov, &chisq, work);
         gsl_multifit_linear_free (work);

#define C(i) (gsl_vector_get(c,(i)))
#define COV(i,j) (gsl_matrix_get(cov,(i),(j)))
     
        for (j=0; j<=n; j++)
        {  coefs[j] = (float)C(j);
        }
           
/*       {
         printf ("# best fit: Y = %g + %g X + %g X^2\n", 
                 C(0), C(1), C(2));
     
         printf ("# covariance matrix:\n");
         printf ("[ %+.5e, %+.5e, %+.5e  \n",
                    COV(0,0), COV(0,1), COV(0,2));
         printf ("  %+.5e, %+.5e, %+.5e  \n", 
                    COV(1,0), COV(1,1), COV(1,2));
         printf ("  %+.5e, %+.5e, %+.5e ]\n", 
                    COV(2,0), COV(2,1), COV(2,2));
         printf ("# chisq = %g\n", chisq);
       }
*/     
       gsl_matrix_free (X);
       gsl_vector_free (y);
       gsl_vector_free (c);
       gsl_matrix_free (cov);
     
       return 0;
}






int fit_Im_HP(int nx, double *xd, double *yd, float *coefs)
{
       int i, j;
       double xi, yi, chisq;
       gsl_matrix *X, *cov;
       gsl_vector *y, *c;
     
       // nx is the number of points 
       // n  is the degree of the polynomial 
     
       X = gsl_matrix_alloc (nx, 3);
       y = gsl_vector_alloc (nx);
     
       c = gsl_vector_alloc (3);
       cov = gsl_matrix_alloc (3, 3);
     
       for (i = 0; i < nx; i++)
       {   xi = (double)xd[i];
           yi = (double)yd[i];
          
           gsl_matrix_set (X, i, 0, 1.0);
           gsl_matrix_set (X, i, 1, xi);
           gsl_matrix_set (X, i, 2, xi*xi*xi);
           
           gsl_vector_set (y, i, yi);
          
         }
           
         gsl_multifit_linear_workspace *work = gsl_multifit_linear_alloc (nx, 3);
         gsl_multifit_linear (X, y, c, cov, &chisq, work);
         gsl_multifit_linear_free (work);

#define C(i) (gsl_vector_get(c,(i)))
#define COV(i,j) (gsl_matrix_get(cov,(i),(j)))
     
        for (j=0; j<=2; j++)
        {  coefs[j] = (float)C(j);
        }
           
/*       {
         printf ("# best fit: Y = %g + %g X + %g X^2\n", 
                 C(0), C(1), C(2));
     
         printf ("# covariance matrix:\n");
         printf ("[ %+.5e, %+.5e, %+.5e  \n",
                    COV(0,0), COV(0,1), COV(0,2));
         printf ("  %+.5e, %+.5e, %+.5e  \n", 
                    COV(1,0), COV(1,1), COV(1,2));
         printf ("  %+.5e, %+.5e, %+.5e ]\n", 
                    COV(2,0), COV(2,1), COV(2,2));
         printf ("# chisq = %g\n", chisq);
       }
*/     
       gsl_matrix_free (X);
       gsl_vector_free (y);
       gsl_vector_free (c);
       gsl_matrix_free (cov);
     
       return 0;
} // end of "fit_Im_HP"

#endif
