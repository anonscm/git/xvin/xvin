/*
 *    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
 */
#ifndef XV_WIN32
# include "config.h"
#endif
#include "fasta_utils.h"

#include "allegro.h"
#include "../dna_utils/utils.h"
#include "../../include/xvin/file_picker_gui.h"
#include <locale.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
# define MAXLINE 256

#ifdef XV_WIN32
char *strtok_r(
    char *str,
    const char *delim,
    char **nextp)
{
    char *ret;

    if (str == NULL)
    {
        str = *nextp;
    }

    str += strspn(str, delim);

    if (*str == '\0')
    {
        return NULL;
    }

    ret = str;
    str += strcspn(str, delim);

    if (*str)
    {
        *str++ = '\0';
    }

    *nextp = str;
    return ret;
}
#endif


char fullfile1[16384] = {0};

char lineBuf[MAXLINE];
int n_asqd = 0; //nb sequences
int m_asqd = 0; // max sequences
int c_asqd = 0; // current index
sqd *lsqd = NULL; // current sequence
sqd **asqd = NULL; // all loaded sequences
oligo_t **g_oligos = NULL;
int g_nb_oligos = 0;

void get_loaded_sequences(sqd ***sequences, int *nb_sequences)
{
    *sequences = asqd;
    *nb_sequences = n_asqd;
}

void get_loaded_oligos(oligo_t ***ol, int *nb_ol)
{
    *ol = g_oligos;
    *nb_ol = g_nb_oligos;
}

sqd *get_selected_sequence(void)
{
    return lsqd;
}


int fasta_utils_push_oligo(oligo_t *oligo)
{
    g_oligos = (oligo_t **) realloc(g_oligos, sizeof(oligo_t *) * (g_nb_oligos + 1));
    g_oligos[g_nb_oligos] = oligo;
    g_nb_oligos++;
    return 0;
}

int do_add_simple_oligo(void)
{
    char olig[1024] = {0};
    float proba = 1.;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    win_scanf("Seq %s\nProba%f\n", olig, &proba);

    if (strlen(olig) > 0 && is_dna(olig))
    {
        oligo_t *cur_oligo  = (oligo_t *) malloc(sizeof(oligo_t));
        cur_oligo->seq = strdup(olig);
        cur_oligo->probability = proba;
        cur_oligo->behaviors = NULL;
        cur_oligo->behaviors_proba = NULL;
        cur_oligo->behaviors_size = 0;
        fasta_utils_push_oligo(cur_oligo);
        do_update_menu();
        return 0;
    }
    else
    {
        warning_message("%s is not DNA\n", olig);
    }

    return 0;
}

bool parse_oligo_file(const char *filename, oligo_t ***oligos, int *nb_oligos)
{
    FILE *file = fopen(filename, "r");
    char line[MAXLINE] = {0};
    bool are_oligos_all_correct = true;
    char *save_strtok_ptr = NULL;
    char *moligo = NULL;
    char *oligo_seq = NULL;
    double oligo_proba = 1;

    if (file == NULL)
    {
        return false;
    }

    setlocale(LC_ALL, "C");

    while (fscanf(file, "%s", line) != -1)
    {
        save_strtok_ptr = NULL;
        moligo = strtok_r(line, ";:", &save_strtok_ptr);
        oligo_seq = NULL;
        oligo_proba = 1;
        parse_oligo_struct(moligo, &oligo_seq, &oligo_proba);

        if (strlen(oligo_seq) > 0 && is_dna(oligo_seq))
        {
            int oligo_idx = *nb_oligos;
            (*nb_oligos)++;
            *oligos = (oligo_t **) realloc(*oligos, sizeof(oligo_t *) * (*nb_oligos));
            (*oligos)[oligo_idx] = (oligo_t *) malloc(sizeof(oligo_t));
            oligo_t *cur_oligo = (*oligos)[oligo_idx];
            cur_oligo->seq = oligo_seq;
            cur_oligo->probability = oligo_proba;
            cur_oligo->behaviors = NULL;
            cur_oligo->behaviors_proba = NULL;
            cur_oligo->behaviors_size = 0;

            while ((moligo = strtok_r(NULL, ";:", &save_strtok_ptr)) != NULL)
            {
                parse_oligo_struct(moligo, &oligo_seq, &oligo_proba);

                if (is_dna(oligo_seq))
                {
                    int behavior_idx = cur_oligo->behaviors_size;
                    cur_oligo->behaviors_size++;
                    cur_oligo->behaviors =
                        (char **) realloc(cur_oligo->behaviors, sizeof(char *) * cur_oligo->behaviors_size);
                    cur_oligo->behaviors_proba =
                        (double *) realloc(cur_oligo->behaviors_proba, sizeof(double) * cur_oligo->behaviors_size);
                    cur_oligo->behaviors[behavior_idx] = oligo_seq;
                    cur_oligo->behaviors_proba[behavior_idx] = oligo_proba;
                }
                else
                {
                    are_oligos_all_correct = false;
                }
            }
        }
        else
        {
            are_oligos_all_correct = false;
        }
    }

    fclose(file);
    return are_oligos_all_correct;
}

void parse_oligo_struct(const char *struc, char **oligo_seq, double *oligo_proba)
{
    char buf[1024] = { 0 };
    *oligo_seq = NULL;
    *oligo_proba = 1;

    if (strchr(struc, '~') != NULL)
    {
        sscanf(struc, "%[a-zA-Z]~%lf", buf, oligo_proba);
        *oligo_seq = strdup(buf);
    }
    else
    {
        *oligo_seq = strdup(struc);
    }
}



int do_select_fasta_seq(void)
{
    int i = 0;
    int nfast = 0;
    char message[512];

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine display the number of fasta sequences loaded");
    }

    nfast = c_asqd;
    snprintf(message, sizeof(message), "There is %d fasta sequences loaded\n"
             "current one is %d -> %s\n"
             "choose new one %%8d\n", n_asqd, c_asqd, backslash_to_slash(lsqd->filename));
    i = win_scanf(message, &nfast);

    if (i == WIN_CANCEL)
    {
        return D_O_K;
    }

    if (nfast < 0 || nfast >= n_asqd)
    {
        win_printf_OK("value %d out of range [0,%d[", nfast, n_asqd);
    }

    c_asqd = nfast;
    lsqd = asqd[c_asqd];
    return D_O_K;
}

int do_load_oligo_file(void)
{
    char *file = NULL;
    //int allegro_win_return = 0;
    int load_reversed_oligo = 0;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine loads oligos sequence");
    }

    if (!(file = open_one_file_config("Open oligo File", NULL, "Oligo Files\0*.oligo*\0\0", "OLIGO-FILE", "last_loaded")))
    {
        return OFF;
    }

    load_oligo_file(file, load_reversed_oligo);
    win_printf("%d oligos sequences loaded\n", g_nb_oligos);
    do_update_menu();
    return D_O_K;
}

int load_oligo_file(const char *file, bool load_reversed_oligo)
{
    if (!parse_oligo_file(file, &g_oligos, &g_nb_oligos))
    {
        win_printf_OK("Some incorrects oligo sequences has been dismissed\n"
                      "%d oligos sequences loaded\n", g_nb_oligos);
    }

    //win_scanf("Do you want to load complementary reverse oligo? %b\n", &load_reversed_oligo);
    if (load_reversed_oligo)
    {
        g_oligos = (oligo_t **) realloc(g_oligos, g_nb_oligos * 2 * sizeof(oligo_t *));

        for (int i = 0; i < g_nb_oligos; i++)
        {
            g_oligos[i + g_nb_oligos] = (oligo_t *) malloc(sizeof(oligo_t));
            g_oligos[i + g_nb_oligos]->seq = dna_complementary(g_oligos[i]->seq);
            g_oligos[i + g_nb_oligos]->behaviors_size = g_oligos[i]->behaviors_size;
            g_oligos[i + g_nb_oligos]->behaviors = (char **) malloc(sizeof(char *) * g_oligos[i]->behaviors_size);
            g_oligos[i + g_nb_oligos]->behaviors_proba = (double *) malloc(sizeof(double) * g_oligos[i]->behaviors_size);

            for (int j = 0; j < g_oligos[i + g_nb_oligos]->behaviors_size; ++j)
            {
                g_oligos[i + g_nb_oligos]->behaviors[j] = dna_complementary(g_oligos[i]->behaviors[j]);
                g_oligos[i + g_nb_oligos]->behaviors_proba[j] = g_oligos[i]->behaviors_proba[j];
            }
        }

        g_nb_oligos *= 2;
    }

    return 0;
}

int do_clear_oligo_list(void)
{
    if (updating_menu_state != 0)
    {
        if (g_nb_oligos < 1)
        {
            active_menu->flags |=  D_DISABLED;
        }
        else
        {
            active_menu->flags &= ~D_DISABLED;
        }

        return D_O_K;
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine clear oligos sequence");
    }

    clear_oligo_list();
    do_update_menu();
    return D_O_K;
}

int clear_oligo_list(void)
{
    for (int i = 0; i < g_nb_oligos; ++i)
    {
        free(g_oligos[i]->seq);

        for (int j = 0; j < g_oligos[i]->behaviors_size; ++j)
        {
            free(g_oligos[i]->behaviors[j]);
        }

        free(g_oligos[i]);
    }

    g_nb_oligos = 0;
    free(g_oligos);
    g_oligos = NULL;
    return 0;
}

int do_clear_sequence_list(void)
{
    if (updating_menu_state != 0)
    {
        if (n_asqd < 1)
        {
            active_menu->flags |=  D_DISABLED;
        }
        else
        {
            active_menu->flags &= ~D_DISABLED;
        }

        return D_O_K;
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine clear oligos sequence");
    }

    for (int i = 0; i < n_asqd; ++i)
    {
        free(asqd[i]);
    }

    free(asqd);
    lsqd = NULL;
    asqd = NULL;
    n_asqd = 0;
    m_asqd = 0;
    c_asqd = -1;
    do_update_menu();
    return D_O_K;
}
int do_load_fasta_file(void)
{
    char *fu = NULL;
    static int keep_Ns = 1;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine loads a fasta sequence");
    }

    if (!(fu = open_one_file_config("Open fasta File", NULL, "Fasta files\0*.fa*\0All files\0*.*\0", "FASTA-FILE",
                                    "last_loaded")))
    {
        return OFF;
    }

    win_scanf("Do you accept sequences containing Ns %b", &keep_Ns);
    lsqd = read_fasta_seq(fu, keep_Ns);
    win_printf("%d sequences loaded, last sequence size: %d", n_asqd, lsqd->totalSize);
    do_update_menu();
    return D_O_K;
}

/*   read a fasta sequence from file and put it in a structure
 *
 */
sqd *read_fasta_seq(const char *filename, int keep_Ns)
{
    FILE *fp = NULL;                 // the file descriptor
    int i, j, k, nr, ic, ip, il, as_Ns;      // nr # of char read,
    char ch, *test = NULL, *err_str = NULL;
    sqd *msqd = NULL;
    char *desc = NULL, file[512] = {0}, message[256] = {0};
    fp = fopen(filename, "r");     // open file  for text reading

    if (fp == NULL)              // we test if the file opens ok
    {
        err_str = strdup(filename);
        win_printf_ptr("Cannot open %s\n", backslash_to_slash(err_str));
        free(err_str);
        return NULL;
    }

    for (test = lineBuf, lineBuf[0] = 0, as_Ns = 0; test != NULL;)
    {
        if (lineBuf[0] == '>')
        {
            desc = strdup(lineBuf);

            for (test = lineBuf, nr = MAXLINE; test != NULL && nr == MAXLINE;
                    test = fgets(lineBuf, MAXLINE, fp))
            {
                nr = strlen(lineBuf);
            }
        }
        else
        {
            for (lineBuf[0] = '>'; lineBuf[0] == '>';)
            {
                // we skip line starting by a comment
                for (test = lineBuf, nr = MAXLINE; test != NULL && nr == MAXLINE;
                        test = fgets(lineBuf, MAXLINE, fp))
                {
                    nr = strlen(lineBuf);
                }

                if (lineBuf[0] == '>')
                {
                    desc = strdup(lineBuf);
                }
            }
        }

        msqd = (sqd *)calloc(1, sizeof(struct _sequence_data));

        if (msqd == NULL)
        {
            win_printf_ptr("Cannot allocate sqd");
        }

        msqd->nPage = 128;
        ip = 0;
        msqd->ln2PageSize = 12;
        msqd->pageSize = 1 << msqd->ln2PageSize;
        msqd->pageMask = 0x0FFF;
        msqd->totalSize = 0;

        if (desc != NULL)
        {
            msqd->description = desc;
        }

        as_Ns = 0;
        msqd->seq = (char **)calloc(msqd->nPage, sizeof(char *));

        if (msqd->seq == NULL)
        {
            win_printf_ptr("Cannot allocate seq");
        }

        msqd->seq[0] = (char *)calloc(msqd->pageSize, sizeof(char));

        if (msqd->seq[0] == NULL)
        {
            win_printf_ptr("Cannot allocate seq");
        }

        for (ip = ic = il = 0; (test != NULL) && (lineBuf[0] != '>'); il++) // , test = fgets(lineBuf,MAXLINE,fp)
        {
            // loop reading lines
            for (j = 0; (ch = lineBuf[j]) != 0; j++)
            {
                // loop scanning characters in line
                switch (ch)
                {
                // we test character
                case 'a':
                case 't':
                case 'g':
                case 'c':
                case 'A':
                case 'T':
                case 'G':
                case 'C':
                    msqd->seq[ip][ic++] = ch;
                    break;

                case 'n':
                case 'N':
                {
                    as_Ns = 1;
                    msqd->seq[ip][ic++] = ch;
                    break;
                }

                default:
                    break;
                }

                if (ic >= msqd->pageSize)
                {
                    // we need more memory
                    ic = 0;
                    ip++;

                    if (ip >= msqd->nPage)    // we need more pages
                    {
                        msqd->seq = (char **)realloc(msqd->seq, 2 * msqd->nPage * sizeof(char *));

                        if (msqd->seq == NULL)
                        {
                            win_printf_ptr("Cannot allocate seq");
                        }

                        msqd->nPage *= 2;
                    }  // we alloc a new page

                    msqd->seq[ip] = (char *)calloc(msqd->pageSize, sizeof(char));

                    if (msqd->seq[ip] == NULL)
                    {
                        win_printf_ptr("Cannot allocate seq");
                    }
                }
            } // end loop scanning characters in line

            test = fgets(lineBuf, MAXLINE, fp);

            if ((ip * msqd->pageSize) + ic - msqd->totalSize > 1000000)
            {
                msqd->totalSize = (ip * msqd->pageSize) + ic;
                my_set_window_title("Loading fasta %8.2fMb", (float)msqd->totalSize / 1000000);
            }
        } // end of loop reading lines

        msqd->totalSize = (ip * msqd->pageSize) + ic;

        for (i = 0; i < 32; i++)
        {
            k = i;
            ip = k >> msqd->ln2PageSize;
            ic = k & msqd->pageMask;
            message[i] = msqd->seq[ip][ic];
            k = msqd->totalSize - 32 + i;
            ip = k >> msqd->ln2PageSize;
            ic = k & msqd->pageMask;
            message[35 + i] = msqd->seq[ip][ic];
        }

        message[32] = '.';
        message[33] = '.';
        message[34] = '.';
        message[67] = 0;
        //win_printf
        my_set_window_title("%d lines read, %d pages filled => %d bases recorded\n "
                            "description: %s seq\n %s", il, ip, msqd->totalSize, msqd->description, message);

        if (extract_file_name(file, sizeof(file), filename) != NULL)
        {
            msqd->filename = strdup(file);
        }

        if (asqd == NULL)
        {
            asqd = (sqd **) calloc(16, sizeof(sqd *));

            if (asqd == NULL)
            {
                return (sqd *) win_printf_ptr("could not allocate memory");
            }

            m_asqd = 16;
        }

        if (n_asqd + 1 > m_asqd)
        {
            asqd = (sqd **)realloc(asqd, (m_asqd + 16) * sizeof(sqd *));

            if (asqd == NULL)
            {
                return (sqd *) win_printf_ptr("could not allocate memory");
            }

            m_asqd += 16;
        }

        if (keep_Ns == 1 || as_Ns == 0)
        {
            asqd[n_asqd++] = msqd;
        }
        else
        {
            for (i = 0; i < msqd->nPage; i++)
                if (msqd->seq[i] != NULL)
                {
                    free(msqd->seq[i]);
                }

            if (msqd->seq != NULL)
            {
                free(msqd->seq);
            }

            if (msqd->description != NULL)
            {
                free(msqd->description);
            }

            if (msqd->filename != NULL)
            {
                free(msqd->filename);
            }

            if (msqd != NULL)
            {
                free(msqd);
            }
        }
    }

    fclose(fp);
    return msqd;
}


int do_print_loaded_oligos(void)
{
    char buf[16000] = {0};

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine loads oligos sequence");
    }

    for (int i = 0; i < g_nb_oligos; ++i)
    {
        oligo_t *oligo = g_oligos[i];
        sprintf(buf, "oligo %d: %s, behaviors :", i, oligo->seq);

        for (int j = 0; j < oligo->behaviors_size; ++j)
        {
            sprintf(buf, "\t beh %d: %s", j, oligo->behaviors[j]);
        }
    }

    return 0;
}

MENU *fasta_utils_plot_menu(void)
{
    static MENU mn[32];

    if (mn[0].text != NULL)
    {
        return mn;
    }

    add_item_to_menu(mn, "Load fasta file", do_load_fasta_file, NULL, 0, NULL);
    add_item_to_menu(mn, "Clear fasta sequences", do_clear_sequence_list, NULL, 0, NULL);
    add_item_to_menu(mn, "select fasta sequence", do_select_fasta_seq, NULL, 0, NULL);
    add_item_to_menu(mn, "Load oligos sequence", do_load_oligo_file, NULL, 0, NULL);
    add_item_to_menu(mn, "Add oligo sequence", do_add_simple_oligo, NULL, 0, NULL);
    add_item_to_menu(mn, "Clear oligo sequences", do_clear_oligo_list, NULL, 0, NULL);
    add_item_to_menu(mn, "Print oligo sequences", do_print_loaded_oligos, NULL, 0, NULL);
    return mn;
}
int fasta_utils_main(int argc, char **argv)
{
    (void) argc;
    (void) argv;
    add_plot_treat_menu_item("fasta_utils", NULL, fasta_utils_plot_menu(), 0, NULL);
    setlocale(LC_ALL, "C");
    return D_O_K;
}

int fasta_utils_unload(int argc, char **argv)
{
    (void) argc;
    (void) argv;
    remove_item_to_menu(plot_treat_menu, "fasta_utils", NULL, NULL);
    return D_O_K;
}
