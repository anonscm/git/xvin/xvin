#pragma once
#ifndef XV_WIN32
# include "config.h"
#endif
#include "platform.h"
#include "xvin.h"
#include "../dna_utils/sequence.h"
#include <stdbool.h>

// public functions
PXV_FUNC(void, get_loaded_sequences, (sqd ***sequences, int *nb_sequences));
PXV_FUNC(void, get_loaded_oligos, (oligo_t ***oligos, int *nb_oligos));
PXV_FUNC(sqd, *get_selected_sequence, (void));

PXV_FUNC(int, fasta_utils_main, (int argc, char **argv));
PXV_FUNC(int, do_select_fasta_seq, (void));
PXV_FUNC(int, do_load_fasta_file, (void));
PXV_FUNC(int, do_load_oligo_file, (void));
PXV_FUNC(int, do_clear_oligo_list, (void));
PXV_FUNC(int, do_clear_sequence_list, (void));
PXV_FUNC(int, do_print_loaded_oligos, (void));

PXV_FUNC(bool, parse_oligo_file, (const char *filename, oligo_t ***oligos, int *nb_oligos));
PXV_FUNC(int, clear_oligo_list, (void));
PXV_FUNC(int, fasta_utils_push_oligo, (oligo_t *oligo));
PXV_FUNC(int, load_oligo_file, (const char *file, bool load_reversed_oligo));
PXV_FUNC(sqd, *read_fasta_seq,(const char *filename, int keep_Ns));


// private functions
PXV_FUNC(void, parse_oligo_struct, (const char *struc, char **oligo_seq, double *oligo_proba));

int fasta_utils_unload(int argc, char **argv);
int do_add_simple_oligo(void); //TODO
MENU *fasta_utils_plot_menu(void);
