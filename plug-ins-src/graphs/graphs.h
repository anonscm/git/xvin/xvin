#ifndef _GRAPHS_H_
#define _GRAPHS_H_

#define	GRAPHS_LOWER		0x0100
#define	GRAPHS_UPPER		0x0200
#define	GRAPHS_BEGIN		0x1000
#define	GRAPHS_MIDDLE		0x2000
#define	GRAPHS_END			0x4000

#define	FILE_BINARY		0x0100
#define	FILE_NI4472		0x0200


PXV_FUNC(MENU*, graphs_plot_menu, 		(void));
PXV_FUNC(int, graphs_main, 				(int argc, char **argv));
PXV_FUNC(int, graph_unload,				(int argc, char **argv));

// functions for work/heat theorems in pendulum:
PXV_FUNC(int, do_graph_sigma_ramps_stat,	(void));
PXV_FUNC(int, do_graph_sigma_ramps_Jarzynski_stat,	(void));
PXV_FUNC(int, do_graph_sigma_ramps_stat_1o,	(void));
PXV_FUNC(int, do_graph_sigma_sinus_stat,	(void));
PXV_FUNC(int, do_compute_U_vs_theta,	(void));
PXV_FUNC(int, do_compute_theta_vs_z,	(void));
PXV_FUNC(int, do_compute_approx_theta_vs_z,	(void));

#endif

