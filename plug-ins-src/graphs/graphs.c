#ifndef _GRAPHS_C_
#define _GRAPHS_C_

#include "allegro.h"
#include "xvin.h"
#include "xv_tools_lib.h"
#include <gsl/gsl_histogram.h>
#include <gsl/gsl_sf.h>
#include <fftw3.h>

/* If you include other plug-ins header do it here*/ 
#include "../hist/hist.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL
#include "graphs.h"

#ifndef k_b
#define k_b 1.3806503e-23	// m2 kg s-2 K-1
#endif

#ifndef Temperature
#define Temperature 298.		// K
#endif

#define kT_Fred 4.005443325e-021 // le kT utilis� par Fr�d�ric





int do_graph_sigma_ramps_stat(void)
{	int	 n;
	register int 	i;
	O_p 	*op = NULL;
	d_s 	*ds0, *ds1, *ds2;
	pltreg	*pr = NULL;
	double	omega_0;
static float f_0=326.25, tau_relax=23.5e-3, fec=2048;
	double	tau, psi, alpha, phi, epsilon;
static int bool_graph_epsilon=0;
	
	if(updating_menu_state != 0)	return D_O_K;	
	
	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This plots \\sigma(\\tau), the average entropy production\n"
					"computed from FT for stationnary states on ramps\n\n"
					"A new dataset is created");
	}
	
	i=win_scanf("{\\pt14\\color{yellow}\\sigma(\\tau) for ramps - Parameters}\n\n"
			"\\omega_0/2\\pi = f_0 = %8f Hz\n"
			"\\tau_{relax} = 2\\pi/\\alpha  = 4\\pi I/\\nu = %8f s\n"
			"sampling at f_{ec} = %8f Hz\n\n"
			"%b plot \\epsilon(\\tau)",
			&f_0, &tau_relax, &fec, &bool_graph_epsilon);
	if (i==CANCEL) return(D_O_K);
	
	omega_0 = (double)2.*M_PI*f_0;
	alpha   = (double)2.*M_PI/tau_relax/2./M_PI;
	psi     = sqrt((double)omega_0*omega_0 - (double)alpha*alpha);
	phi     = atan2((double)psi, (double)alpha);
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds0) != 3)		return win_printf_OK("cannot plot region");
	n=ds0->nx;
	
	if ((ds1 = create_and_attach_one_ds(op, n, n, 0)) == NULL)	return(win_printf_OK("cannot create dataset"));
	for (i=0; i<n; i++)
	{	tau = (double)ds0->xd[i]/fec;
		
		epsilon  = (  2.*sin(3.*phi)/(omega_0*omega_0*tau*tau) 
					- 2.*sin(2.*phi+psi*tau)/(omega_0*tau)*exp(-alpha*tau)
					- 2.*sin(3.*phi+psi*tau)/(omega_0*omega_0*tau*tau)*exp(-alpha*tau)
					);
		epsilon /= sin(phi);	
		
		ds1->xd[i] = ds0->xd[i];
		ds1->yd[i] = -(float)1./((float)1. - (float)epsilon);			
	}
	ds1->treatement = my_sprintf(ds1->treatement,"\\sigma(\\tau) for ramps with \\omega_0=%5.2f and \\alpha=%5.2f", 
						(float)(omega_0), (float)(alpha));
	inherit_from_ds_to_ds(ds1, ds0);
	
	if (bool_graph_epsilon==1)
	{	if ((ds2 = create_and_attach_one_ds(op, n, n, 0)) == NULL)	return(win_printf_OK("cannot create dataset"));
		for (i=0; i<n; i++)
		{	ds2->xd[i] = ds0->xd[i];	
			ds2->yd[i] = (float)1. + (float)1./ds1->yd[i];
		}
		ds2->treatement = my_sprintf(ds2->treatement,"(\\epsilon(\\tau)) with \\omega_0=%5.2f and \\alpha=%5.2f", 
						(float)(omega_0), (float)(alpha));
		inherit_from_ds_to_ds(ds2, ds0);
	}

	return refresh_plot(pr, UNCHANGED);
} /* end of function 'do_graph_sigma_ramps_stat' */


int do_graph_sigma_ramps_Jarzynski_stat(void)
{	int	 n;
	register int 	i;
	O_p 	*op = NULL;
	d_s 	*ds0, *ds1, *ds2;
	pltreg	*pr = NULL;
	double	omega_0;
static float f_0=226, tau_relax=23.5e-3, fec=2048;
	double	tau, psi, alpha, phi, epsilon;
static int bool_graph_epsilon=0;
	
	if(updating_menu_state != 0)	return D_O_K;	
	
	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This plots \\sigma(\\tau), the average entropy production\n"
					"computed from FT for stationnary states on ramps\n\n"
					"A new dataset is created");
	}
	
	i=win_scanf("{\\pt14\\color{yellow}\\sigma(\\tau) for ramps - Parameters}\n\n"
			"\\omega_0/2\\pi = f_0 = %8f Hz\n"
			"\\tau_{relax} = 2\\pi/\\alpha  = 4\\pi I/\\nu = %8f s\n"
			"sampling at f_{ec} = %8f Hz\n\n"
			"%b plot \\epsilon(\\tau)",
			&f_0, &tau_relax, &fec, &bool_graph_epsilon);
	if (i==CANCEL) return(D_O_K);
	
	omega_0 = (double)2.*M_PI*f_0;
	alpha   = (double)2.*M_PI/tau_relax/2./M_PI;
	psi     = sqrt((double)omega_0*omega_0 - (double)alpha*alpha);
	phi     = atan2((double)psi, (double)alpha);
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds0) != 3)		return win_printf_OK("cannot plot region");
	n=ds0->nx;
	
	if ((ds1 = create_and_attach_one_ds(op, n, n, 0)) == NULL)	return(win_printf_OK("cannot create dataset"));
	for (i=0; i<n; i++)
	{	tau = (double)ds0->xd[i]/fec;
		
		epsilon  = (  sin(3.*phi) - sin(3.*phi+psi*tau)*exp(-alpha*tau));
		epsilon /= sin(2*phi);
		epsilon /= omega_0*tau;	
		
		ds1->xd[i] = ds0->xd[i];
		ds1->yd[i] = -(float)1. * omega_0*1./psi*1./((float)1. - (float)epsilon);			
	}
	ds1->treatement = my_sprintf(ds1->treatement,"\\sigma(\\tau) for ramps with \\omega_0=%5.2f and \\alpha=%5.2f", 
						(float)(omega_0), (float)(alpha));
	inherit_from_ds_to_ds(ds1, ds0);
	
	if (bool_graph_epsilon==1)
	{	if ((ds2 = create_and_attach_one_ds(op, n, n, 0)) == NULL)	return(win_printf_OK("cannot create dataset"));
		for (i=0; i<n; i++)
		{	ds2->xd[i] = ds0->xd[i];	
			ds2->yd[i] = (float)1. + (float)1./ds1->yd[i];
		}
		ds2->treatement = my_sprintf(ds2->treatement,"(\\epsilon(\\tau)) with \\omega_0=%5.2f and \\alpha=%5.2f", 
						(float)(omega_0), (float)(alpha));
		inherit_from_ds_to_ds(ds2, ds0);
	}

	return refresh_plot(pr, UNCHANGED);
} /* end of function 'do_graph_sigma_ramps_stat' */



int do_graph_sigma_ramps_stat_1o(void)
{	int	 n;
	register int 	i;
	O_p 	*op = NULL;
	d_s 	*ds0, *ds1, *ds2;
	pltreg	*pr = NULL;
	double	tau_relax;
static float tau0=2.66,fec = 819.2;
	double	tau, epsilon;
static int bool_graph_epsilon=0;
	
	if(updating_menu_state != 0)	return D_O_K;	
	
	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This plots \\sigma(\\tau) overdamped, the average entropy production\n"
					"computed from FT for stationnary states on ramps\n\n"
					"A new dataset is created");
	}
	
	i=win_scanf("{\\pt14\\color{yellow}\\sigma(\\tau) overdamped for ramps - Parameters}\n\n"
			"\\tau_{0} = R C = %8f ms\n"
			"sampling at f_{ec} = %8f Hz\n\n"
			"%b plot \\epsilon(\\tau)",
			 &tau0, &fec, &bool_graph_epsilon);
	if (i==CANCEL) return(D_O_K);
	
	tau_relax = tau0*0.001;
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds0) != 3)		return win_printf_OK("cannot plot region");
	n=ds0->nx;
	
	if ((ds1 = create_and_attach_one_ds(op, n, n, 0)) == NULL)	return(win_printf_OK("cannot create dataset"));
	for (i=0; i<n; i++)
	{	tau = (double)ds0->xd[i]/fec;
		
		epsilon  = (  2*tau_relax*tau_relax*(1-exp(-tau/tau_relax))-2*tau*tau_relax*exp(-tau/tau_relax)
					);
		epsilon /= tau*tau;	
		
		ds1->xd[i] = ds0->xd[i];
		ds1->yd[i] = -(float)1./((float)1. - (float)epsilon);			
	}
	ds1->treatement = my_sprintf(ds1->treatement,"\\sigma(\\tau) overdamped for ramps with \\tau_0=%5.2f", 
						(float)tau0);
	inherit_from_ds_to_ds(ds1, ds0);
	
	if (bool_graph_epsilon==1)
	{	if ((ds2 = create_and_attach_one_ds(op, n, n, 0)) == NULL)	return(win_printf_OK("cannot create dataset"));
		for (i=0; i<n; i++)
		{	ds2->xd[i] = ds0->xd[i];	
			ds2->yd[i] = (float)1. + (float)1./ds1->yd[i];
		}
		ds2->treatement = my_sprintf(ds2->treatement,"(\\epsilon(\\tau)) overdamped for ramps with \\tau_0=%5.2f", 
						(float)(tau0));
		inherit_from_ds_to_ds(ds2, ds0);
	}

	return refresh_plot(pr, UNCHANGED);
} /* end of function 'do_graph_sigma_ramps_stat' */





int do_graph_sigma_sinus_stat(void)
{	int	 n;
	register int 	i;
	O_p 	*op = NULL;
	d_s 	*ds0, *ds1, *ds2;
	pltreg	*pr = NULL;
	double	omega_0, omega_1, omega_22;
static float f_0=326.25, tau_relax=23.5e-3, fec=1000.;
static float f_1=10.;
static int 		bool_graph_epsilon=0;
static int 		bool_average_phases=0;
static float 	starting_phase = 0.;
static float A=1, B=1;
	double  C=0, C2=0, D=0; 
	double	tau, alpha, psi, phi, delta, gamma;
	double  W, dW2, sigma ;
	
	if(updating_menu_state != 0)	return D_O_K;	
	
	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This plots \\sigma(\\tau), the average entropy production\n"
					"computed from FT for stationnary states on periodic sines\n\n"
					"A new dataset is created");
	}
	
	i=win_scanf("{\\pt14\\color{yellow}\\sigma(\\tau) for sines - Parameters}\n\n"
			"\\omega_0/2\\pi      = f_0 = %8f Hz\n"
			"\\tau_{relax} = 1/\\alpha  = 2I/\\nu  = %8f s\n"
			"\\omega_1/2\\pi      = f_1 = %8f Hz (forcing frequency)\n"
			"sampling at f_{ec}  = %8f Hz\n\n"
			"%R average over all phases or\n"
			"%r start at phase 2\\pi * %5f (phase is between 0 and 1)\n\n"
			"%b plot \\epsilon(\\tau)",
			&f_0, &tau_relax, &f_1, &fec, &bool_average_phases, &starting_phase, &bool_graph_epsilon);
	if (i==CANCEL) return(D_O_K);
	
	omega_0 = 2*M_PI*f_0;
	omega_1 = 2*M_PI*f_1;
	alpha   = (double)1./tau_relax;
	psi   	= sqrt(omega_0*omega_0 - alpha*alpha);
	phi     = atan2((double)psi, (double)alpha); // cahier V, p89
	omega_22= sqrt( (omega_0*omega_0-omega_1*omega_1)*(omega_0*omega_0-omega_1*omega_1) + 4.*alpha*alpha*omega_1*omega_1 );
	delta	= atan2( (double)2.*alpha*psi, (double)2.*alpha*alpha - omega_0*omega_0 + omega_1*omega_1);
	gamma	= atan2(-(double)2.*alpha*omega_1, omega_0*omega_0 - omega_1*omega_1);
	
	if (bool_average_phases==0)
	{	C  = 0.;  // average of cos
		C2 = 0.5; // average of (cos)^2
	}
	else 
	{	C  = cos(omega_1*starting_phase);
		C2 = C*C;
	}
	
	D = 2.*cos(delta) + cos(gamma);
			
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds0) != 3)		return win_printf_OK("cannot plot region");
	n=ds0->nx;
	
	if ((ds1 = create_and_attach_one_ds(op, n, n, 0)) == NULL)	return(win_printf_OK("cannot create dataset"));
	for (i=0; i<n; i++)
	{	tau = (double)ds0->xd[i]/fec;
	
		W = - B*B*omega_0*omega_0*omega_1*omega_1*alpha*tau/(omega_22*omega_22); // cahier V, p 92
		
		dW2 =  // formule cahier V page 90
				-2.*W
				+ 2.*(A*A - 2.*A*B*C + B*B*C2) 
				+ 2.*B*omega_1*omega_1/omega_22*(A*C - B*C2)*D
				+ B*B*omega_1*omega_1/(sin(phi)*omega_22)*( (1.-C)*sin(phi+delta) - 2. *omega_1*omega_1/(omega_22)*sin(phi+2.*delta) )
				+ exp(-alpha*tau)/sin(phi)*( -2.*(A*A - 2.*A*B*C + B*B*C2)*sin(phi+psi*tau)
											+ 4*B*(A*C - B*C2)*omega_1*omega_1/(omega_22*omega_22)*( -D*sin(phi+psi*tau)*omega_22 
																									+ 2.*alpha*omega_0*sin(psi*tau) )
											+ B*B*omega_1*omega_1/omega_22*( (C-1.)*sin(phi+delta+psi*tau) 
																			+ 2.*omega_1*omega_1/omega_22*sin(phi+2.*delta+psi*tau) )
											);
	
		sigma = 2.*W/dW2;
	
		ds1->xd[i] = ds0->xd[i];
		ds1->yd[i] = (float)(sigma);	
	}
	ds1->treatement = my_sprintf(ds1->treatement,"\\sigma(\\tau) with \\omega_0=%5.2f and \\alpha=%5.2f", 
						(float)(omega_0), (float)(alpha));
	inherit_from_ds_to_ds(ds1, ds0);
		
	if (bool_graph_epsilon==1)
	{	if ((ds2 = create_and_attach_one_ds(op, n, n, 0)) == NULL)	return(win_printf_OK("cannot create dataset"));
		for (i=0; i<n; i++)
		{	ds2->xd[i] = ds0->xd[i];	
			ds2->yd[i] = (float)1. + (float)1./ds1->yd[i];
		}
		ds2->treatement = my_sprintf(ds2->treatement,"(\\epsilon(\\tau)) with \\omega_0=%5.2f and \\alpha=%5.2f", 
						(float)(omega_0), (float)(alpha));
		inherit_from_ds_to_ds(ds2, ds0);
	}
	
	return refresh_plot(pr, UNCHANGED);
} /* end of function 'do_graph_sigma_sinus_stat' */








int do_graph_pdf_W_sinus_stat(void)
{	register int i, j;
	d_s 	*ds2, *ds1;
	O_p	*op;
	pltreg *pr = NULL;
	int	index, nh, n, tau;
	gsl_histogram *hist;
	double  mean, sigma, x, omega0, omega1, gamma, alpha, epsilon, scale;
static 	int 	bool_input_divided_by_tau=1, bool_output_divided_by_tau=1;
static 	float 	omega0_f=220., omega1_f=64., alpha_f=107., f_acq=8192;
	
	if(updating_menu_state != 0)	return D_O_K;		
       	index = active_menu->flags & 0xFFFFFF00;
	
	if (key[KEY_LSHIFT])	return win_printf_OK("This routine plots theoretical pdf(s) of W"
		"several pdf are plotted, getting tau from all datasets of current plot");
	
	if (win_scanf("{\\color{yellow}\\pt14 pdf of W for sinus, stat. case}\n\n"
		"damping frequency \\alpha %8f Hz\n"
		"resonant frequency \\omega_0 %8f Hz\n"
		"forcing  frequency   \\omega_1 %8f Hz\n"
		"acquisition  frequency  f_{acq} %8f Hz\n"
		"input is %R W_\\tau or %r {1\\over\\tau}W_\\tau\n"
		"output is %R W_\\tau or %r {1\\over\\tau}W_\\tau",	
		&alpha_f, &omega0_f, &omega1_f, &f_acq, &bool_input_divided_by_tau, &bool_output_divided_by_tau) == CANCEL) return(D_O_K);
	omega0 = (double)omega0_f*2.*M_PI;
	omega1 = (double)omega1_f*2.*M_PI;
	alpha  = (double)alpha_f*2.*M_PI;
	gamma  = atan(-2.*alpha*omega1/(omega0*omega0-omega1*omega1));

	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)		return win_printf_OK("cannot find plot");

	n = op->n_dat;
	for (j=0; j<n; j++)
	{	ds1 = op->dat[j];
		nh  = ds1->nx;

		tau = grep_int_in_string(ds1->treatement, "tau=");
		if (tau==-1) 	tau = grep_int_in_string(ds1->history, "tau=");
		scale = (double) tau/f_acq; //dimensionnalisation de tau en seconde
		
		ds_to_histogram(ds1,&hist);
		mean  	=	gsl_histogram_mean(hist); 
		free(hist);
		if (bool_input_divided_by_tau==0) mean /=scale; //version normalis�e par tau
		
		if (tau>0)
		{	if ((ds2 = create_and_attach_one_ds(op, nh, nh, 0)) == NULL)	return win_printf_OK("cannot create dataset !");
				
			
			epsilon = (double)(-1./(2.*alpha)*(omega0*omega0+omega1*omega1)/(omega1*omega1)*cos(2.*gamma));
			sigma   = (double)2.*fabs(mean)*((double)scale - epsilon); // this is sigma squared!
			sigma /= (float)(scale*scale);
			
		
			for (i=0; i<nh; i++)
			{	x = (double) ds1->xd[i];
				if (bool_input_divided_by_tau ==0) 	x /= (double) scale;
				ds2->yd[i] = (double)(exp(-(x-mean)*(x-mean)/(2.*sigma))/sqrt(2.*M_PI*sigma));
				
				ds2->xd[i] = (float) x;
				if (bool_output_divided_by_tau ==0) 	
					{	ds2->xd[i] *= (float) scale; 	
						ds2->yd[i] /= (float) scale;
					}
			}	
		
			inherit_from_ds_to_ds(ds2, ds1);
			ds2->treatement = my_sprintf(ds2->treatement,"theoretical pdf for %s, sinus stat, for \\tau=%d", 
							(bool_output_divided_by_tau==1) ? "1/\\tau W" : "W", tau);
			
		}
	
	}

	if (win_scanf("Remove old pdfs ?")!=CANCEL)
	for (j=0; j<n; j++)
	{	remove_ds_n_from_op (op, 0);	// always remove the 0th because counting is shifted after any one is deleted
	}

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
}	// end of "do_graph_pdf_W_sinus_stat"








int do_graph_pdf_Q_sinus_stat(void)
{	register int i, j;
	d_s 	*ds2, *ds1;
	O_p	*op;
	pltreg *pr = NULL;
	int	index, nh, n, tau;
	gsl_histogram *hist;
	double  mean, sigma, x, zp, zm, pdf, scale;
static int bool_opposite_mean=0, bool_input_divided_by_tau=1, bool_output_divided_by_tau=1;
static float f_acq=8192.;
	
	if(updating_menu_state != 0)	return D_O_K;		
       	index = active_menu->flags & 0xFFFFFF00;
	
	if (key[KEY_LSHIFT])	return win_printf_OK("This routine plots pdf(s) of Q"
		"several pdf are plotted, getting inspiration from all datasets of current plot");
	
	if (win_scanf("{\\color{yellow}\\pt14 pdf of Q for sinus, stat. case}\n\n"
		"acquisition  frequency  f_{acq} %8f Hz\n"
		"%b multiply mean by (-1)\n"
		"input is %R W_\\tau or %r {1\\over\\tau}W_\\tau\n"
		"output is %R Q_\\tau or %r {1\\over\\tau}Q_\\tau",	
		&f_acq, &bool_opposite_mean, &bool_input_divided_by_tau, &bool_output_divided_by_tau) == CANCEL) return(D_O_K);


	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)		return win_printf_OK("cannot find plot");

	n = op->n_dat;
	for (j=0; j<n; j++)
	{	ds1 = op->dat[j];
		nh  = ds1->nx;
	
	
		tau = grep_int_in_string(ds1->treatement, "tau=");
		if (tau==-1) 	tau = grep_int_in_string(ds1->history, "tau=");
		scale = (double) tau/f_acq; //dimensionnalisation de tau en seconde
		
		
		ds_to_histogram(ds1,&hist);
		mean  = gsl_histogram_mean(hist);
		if (bool_opposite_mean==1) mean = -mean;
		sigma = gsl_histogram_sigma(hist);
		sigma = sigma*sigma; // squared 
		free(hist);
		//on travaille avec q et non q/tau
		if (bool_input_divided_by_tau==1) 
			{	mean  *= scale;
				sigma *= scale*scale;
			}
		if ((ds2 = create_and_attach_one_ds(op, nh, nh, 0)) == NULL)	return win_printf_OK("cannot create dataset !");
		
		for (i=0; i<nh; i++)
		{	x		   = (double)(ds1->xd[i]*(bool_input_divided_by_tau*(scale-1.)+1.)-mean);
			zm         = x + sigma;
			zp         = x - sigma;
			pdf		   = exp(x)  * (1. - gsl_sf_erf(zm/sqrt((double)2.*sigma)) );
			pdf		  += exp(-x) * (1. + gsl_sf_erf(zp/sqrt((double)2.*sigma)) );
			ds2->yd[i] = (float)(pdf*exp(+sigma/2.)/(4.)); 
			ds2->xd[i] = x+mean;
			
			if (bool_output_divided_by_tau==1)
			{	ds2->yd[i] *= scale; 
				ds2->xd[i] /= scale;
			
			}
		}	
		
		inherit_from_ds_to_ds(ds2, ds1);
		ds2->treatement = my_sprintf(ds2->treatement,"theoretical pdf for %s Q, sinus stat", (bool_output_divided_by_tau==1) ? "\\frac{1}{\\tau}" : "");

	}

	if (win_scanf("Remove old pdfs ?")!=CANCEL)
	for (j=0; j<n; j++)
	{	remove_ds_n_from_op (op, 0);	// always remove the 0th because counting is shifted after any one is deleted
	}

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of "do_graph_pdf_Q_sinus_stat"









int do_graph_pdf_Q_sinus_stat_TF(void)
{	register int i, j, k;
	d_s 	*ds2, *ds1;
	O_p	*op;
	pltreg *pr = NULL;
	int	index, nh, n;
	char *s=NULL;
	gsl_histogram *hist;
	double  mean, sigma;
static int bool_opposite_mean=0, bool_Q=1, fft_factor=1;
	fftw_complex  *psi;
	double 	 *p, omega, omega2, tmp1, tmp2, scale;
	fftw_plan plan_back;     // fft plan for fftw3
	int n_fft, tau;
static float	f_acq=8192., omega0_f=220., tr=0.0095;
	double omega0, t0, dom;

	
	if(updating_menu_state != 0)	return D_O_K;		
       	index = active_menu->flags & 0xFFFFFF00;
	
	if (key[KEY_LSHIFT])	return win_printf_OK("This routine plots pdf(s) of Q"
		"several pdf are plotted, getting inspiration from all datasets of current plot");

	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)		return win_printf_OK("cannot find plot");
	nh = op->dat[0]->nx;
	n_fft = fft_factor*nh;
	
	s=my_sprintf(s, "{\\color{yellow}\\pt14 pdf of Q for ramps, stat. case}\n\n"
		"%%b multiply mean by (-1)\n"
		"%%4d points in Fourier transforms (multiple of %d (number of bins) please)\n"
		"f_{acq} = %%7f\n"
		"\\omega_0 = %%7f\n"
		"damping frequency \\alpha %%8f Hz\n\n"
		"output is %%R Q_\\tau or %%r {1\\over\\tau}Q_\\tau or %%r \\frac{Q\\tau}{<Q_\\tau>}", nh);
	if (win_scanf(s, &bool_opposite_mean, &n_fft, &f_acq, &omega0_f, &tr, &bool_Q) == CANCEL) return(D_O_K);
	free(s);
	
	if ((n_fft%nh)==0) fft_factor=n_fft/nh;
	else	return(win_printf_OK("Antoine est fatigue"));
	
	omega0 = (double)omega0_f*2.*M_PI;
	t0	= (double) 2./(tr*omega0*omega0);
	psi	= (fftw_complex*)fftw_malloc((n_fft/2+1)*sizeof(fftw_complex));
	p	= (double*)fftw_malloc(n_fft*sizeof(double));
	dom = 6.; //domaine sur lequel on fera varier omega par pas de 2Pi/nfft
				
  	plan_back = fftw_plan_dft_c2r_1d(n_fft, psi,  p, FFTW_ESTIMATE);   
  
	n = op->n_dat;
	for (j=0; j<n; j++)
	{	ds1 = op->dat[j];
		
		
		ds_to_histogram(ds1,&hist);
		mean  	=	gsl_histogram_mean(hist);
		if (bool_opposite_mean==1) mean = -mean;
		sigma = gsl_histogram_sigma(hist);
		sigma = sigma*sigma; // squared 
		free(hist);
		
			
		tau = grep_int_in_string(ds1->treatement, "tau=");
		if (tau==-1) 	tau = grep_int_in_string(ds1->history, "tau=");
		scale = (double)tau/(double)f_acq;
		
		//Normalisation par la valeur moyenne
		sigma	/=	(double)(mean*mean);
			
		
		for (i=0; i<n_fft/2+1; i++)
		{	omega   = (double)i/(double)(n_fft)*dom*2.*M_PI;
			omega2	= omega*omega;
			tmp1	= 0.;
			tmp2	= omega2*sigma/2.; 
			psi[i][0] = cos(tmp1)*exp(-tmp2)/(1.+omega2/(fabs(mean)*fabs(mean)*scale*scale));
			psi[i][1] = sin(tmp1)*exp(-tmp2)/(1.+omega2/(fabs(mean)*fabs(mean)*scale*scale));
		
		}
		
		fftw_execute(plan_back); 
  
		if ((ds2 = create_and_attach_one_ds(op, nh, nh, 0)) == NULL)	return win_printf_OK("cannot create dataset !");
		
		
		for (i=0; i<nh; i++)
		{	ds2->xd[i] = (float)(i-nh/2)/(float)(nh-1)*(float)n_fft/dom; //pas en x est donn� par heisenberg dxdomega=2pi
		
			//raccordement TF inverse
			if (i<nh/2)	
			{	ds2->yd[i] = 0.;
				for (k=0; k<fft_factor; k++)
					ds2->yd[i] += (float)p[i*fft_factor+k +n_fft/2]/(float)n_fft;
				ds2->yd[i] /= (float)(fft_factor);
			}
			else		
			{	ds2->yd[i] = 0.;
				for (k=0; k<fft_factor; k++)
					ds2->yd[i] += (float)p[i*fft_factor+k -n_fft/2]/(float)n_fft; 
				ds2->yd[i] /= (float)(fft_factor);
			}
			
			
			
			if (bool_Q==0) //cas de Q
			{	ds2->xd[i] *= (float)scale*fabs(mean);
				ds2->xd[i] += (float)scale*mean;
			}
			
			if (bool_Q==1) //cas de 1/tau Q
			{	ds2->xd[i] *= (float) fabs(mean);
				ds2->xd[i] += (float) mean;
			}
			
			if (bool_Q==2) //cas 1/<Q> Q
			{	ds2->xd[i] += (float) 1.;
			}
		}
		inherit_from_ds_to_ds(ds2, ds1);
		ds2->treatement = my_sprintf(ds2->treatement,"theoretical pdf for Q, sinus stat");

	}
	fftw_free(psi);
	fftw_free(p);
	
   	 fftw_destroy_plan(plan_back);

	if (win_scanf("Remove old pdfs ?")!=CANCEL)
	for (j=0; j<n; j++)
	{	remove_ds_n_from_op (op, 0);	// always remove the 0th because counting is shifted after any one is deleted
	}

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of "do_graph_pdf_Q_sinus_stat_TF"




int do_graph_pdf_WJVZ_ramps_stat(void)
{	register int i, j;
	d_s 	*ds2, *ds1;
	O_p	*op;
	pltreg *pr = NULL;
	int	index, nh, n, tau;
	double  mean, sigma, x;
static 	int 	bool_input_divided_by_tau=1, bool_output_divided_by_tau=1;
static 	float 	omega0_f=217., alpha_f=107., Mm_f=10.4,tau_m_f=100,C_f=4.3e-4, f_acq=8192;
	double omega0,alpha,Mm,tau_m,C, t0, d2, a, scale;
	
	if(updating_menu_state != 0)	return D_O_K;		
       	index = active_menu->flags & 0xFFFFFF00;
	
	if (key[KEY_LSHIFT])	return win_printf_OK("This routine plots theoretical pdf(s) of WJ \\int_{t}^{t+\\tau}\\frac{dM}{dt} (\\theta-\\theta*)dt"
		"several pdf are plotted, getting tau from all datasets of current plot");
	
	if (win_scanf("{\n  \\color{yellow}\\pt14 pdf of WJ \\int_{t}^{t+\\tau}\\frac{dM}{dt} (\\theta-\\theta*)dt  for ramps, stat. case}\n\n"
		"Values of \\tau are given by input\n"
		"input is %R W_\\tau or %r {1 \\over \\tau}W_\\tau\n"
		"resonance frequency ? 	   %8f Hz\n"
		"damping frequency \\alpha? %8f Hz\n"
		"amplitude of ramps (M_m) ?   %8f  pN.m\n"
		"duration of a ramp (\\tau_m) ? %8f ms\n"
		"stiffness of oscillator ? %8f     N.m/rad\n"
		"acquisition  frequency  f_{acq} %8f Hz\n"
		"output is %R W_\\tau or %r {1\\over\\tau}W_\\tau",	
		&bool_input_divided_by_tau, &omega0_f, &alpha_f, &Mm_f, &tau_m_f, &C_f, &f_acq, &bool_output_divided_by_tau) == CANCEL) return(D_O_K);
	omega0=(double)omega0_f*2*M_PI;
	alpha=(double)alpha_f;
	tau_m=(double)tau_m_f*(double)1.e-3;
	Mm =(double)Mm_f*(double)1.e-12;
	C=(double)C_f;
	
	t0 = (double)2.*alpha/(omega0*omega0);
	d2=(Mm*Mm)/(C*(double)k_b*(double)Temperature)*(double)1./(omega0*omega0_f*tau_m*tau_m);
	a=((double)1.-omega0*omega0*t0*t0);
	

	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)		return win_printf_OK("cannot find plot");

	n = op->n_dat;
	for (j=0; j<n; j++)
	{	ds1 = op->dat[j];
		nh  = ds1->nx;

		tau = grep_int_in_string(ds1->treatement, "tau=");
		if (tau==-1) 	tau = grep_int_in_string(ds1->history, "tau=");
		scale = (double) tau/f_acq; //dimensionnalisation de tau en seconde
		
				
		if (tau>0)
		{	if ((ds2 = create_and_attach_one_ds(op, nh, nh, 0)) == NULL)	return win_printf_OK("cannot create dataset !");
				
			mean=(double)d2*omega0*omega0*t0*scale;
			sigma=(double)2.*mean+2.*d2*a;
			
		
			for (i=0; i<nh; i++)
			{	x = (double) (-5.+(double)i*(double)10./(double)nh);
				ds2->yd[i] = (double)(exp(-(x-mean)*(x-mean)/(2.*sigma))/sqrt(2.*M_PI*sigma));
				
				ds2->xd[i] = (float) x;
				if (bool_output_divided_by_tau ==1) 	
					{	ds2->xd[i] /= (float) scale; 	
						ds2->yd[i] *= (float) scale;
					}
			}	
		
			inherit_from_ds_to_ds(ds2, ds1);
			ds2->treatement = my_sprintf(ds2->treatement,"theoretical pdf for %s, Jarzynski ramps stat, for \\tau=%d", 
							(bool_output_divided_by_tau==1) ? "1/\\tau W" : "W", tau);
			
		}
	
	}

	if (win_scanf("Remove old pdfs ?")!=CANCEL)
	for (j=0; j<n; j++)
	{	remove_ds_n_from_op (op, 0);	// always remove the 0th because counting is shifted after any one is deleted
	}

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
}	// end of "do_graph_pdf_WJVZ_ramps_stat"




int do_graph_pdf_Q_ramps_stat_TF(void)
{	register int i, j, k;
	d_s 	*ds2, *ds1;
	O_p	*op;
	pltreg *pr = NULL;
	int	index, nh, n;
	char *s=NULL;
static int bool_opposite_mean=0, bool_Q=1, fft_factor=1;
	fftw_complex  *psi;
	double 	 *p, omega, omega2, tmp1, tmp2, scale;
	fftw_plan plan_back;     // fft plan for fftw3
	int n_fft, tau;
static float	f_acq=8192., omega0_f=217., alpha_f=107., C_f=4.6e-4, tau_m_f=0.1, M_m_f=11.18e-12;
	double t0, omega0, a, b, d2, dom, mean,skew,kurt, alpha, mean_m, sigma_m, Mm, tau_m, C;

	
	if(updating_menu_state != 0)	return D_O_K;		
       	index = active_menu->flags & 0xFFFFFF00;
	
	if (key[KEY_LSHIFT])	return win_printf_OK("This routine plots pdf(s) of Q"
		"several pdf are plotted, getting inspiration from all datasets of current plot");

	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)		return win_printf_OK("cannot find plot");
	nh = op->dat[0]->nx;
	n_fft = fft_factor*nh;
	
	s=my_sprintf(s, "{\\color{yellow}\\pt14 pdf of Q for ramps, stat. case}\n\n"
		"%%b multiply mean by (-1)\n"
		"%%4d points in Fourier transforms (multiple of %d (number of bins) please)\n\n"
		"\\pt14 Parameters:\n"
		"f_{acq} = %%7f\n"
		"\\omega_0 = %%7f\n"
		"damping frequency \\alpha %%8f Hz\n"
		"amplitude of ramps (M_m) ?   %%8f  pN.m\n"
		"duration of a ramp (\\tau_m) ? %%8f s\n"
		"stiffness of oscillator ? %%8f     N.m/rad\n\n"
		"output is %%R Q_\\tau or %%r {1\\over\\tau}Q_\\tau or %%r \\frac{Q\\tau}{<Q_\\tau>}", nh);
	if (win_scanf(s, &bool_opposite_mean, &n_fft, &f_acq, &omega0_f, &alpha_f, &M_m_f, &tau_m_f, &C_f, &bool_Q) == CANCEL) return(D_O_K);
	free(s);
	
	if ((n_fft%nh)==0) fft_factor=n_fft/nh;
	else	return(win_printf_OK("Antoine est fatigue"));
	
	omega0 	= (double)omega0_f*2.*M_PI;
	alpha 	= (double)alpha_f;
	Mm=(double)M_m_f;
	C=(double)C_f;
	tau_m=(double)tau_m_f;
	
	
	//cahier calcul chaleur 2005/2006
	t0	= (double) 2.*alpha/(omega0*omega0);
	a	= (double) (1.-(omega0*omega0*t0*t0));
	b	= (double) (1.+(omega0*omega0*t0*t0));
	d2=(Mm*Mm)/(C*(double)k_b*(double)Temperature)*(double)1./(omega0*omega0*tau_m*tau_m);
	mean = (double) 2.*d2*omega0*omega0*t0;//moyenne de 1/tau Q_tau
	
	
	
	win_printf("distance a l equilibre d2=%8f", d2);	
	psi	= (fftw_complex*)fftw_malloc((n_fft/2+1)*sizeof(fftw_complex));
	p	= (double*)fftw_malloc(n_fft*sizeof(double));

	ds1 = op->dat[0];
	dom = (double) n_fft/(ds1->xd[nh-1]-ds1->xd[0])*mean*0.5; //domaine sur lequel on fera varier omega par pas de 2Pi/nfft reli� a l'echelle en x
			
  	plan_back = fftw_plan_dft_c2r_1d(n_fft, psi,  p, FFTW_ESTIMATE);   
  
	n = op->n_dat;
	for (j=0; j<n; j++)
	{	ds1 = op->dat[j];
		
		
		tau = grep_int_in_string(ds1->treatement, "tau=");
		if (tau==-1) 	tau = grep_int_in_string(ds1->history, "tau=");
		scale = (double)tau/(double)f_acq;
		
		
		mean_m=(double)mean*scale;
		sigma_m=(double)2.*mean_m+2.*d2*a+2.*d2*b;
		sigma_m	/=(double)(mean_m*mean_m);
		skew    = (double)(2.*d2*a/(fabs(mean_m*mean_m*mean_m)));
		kurt 	= (double)(2.*d2*b/(fabs(mean_m*mean_m*mean_m*mean_m)));
		
		if (j==0) 
		{win_printf("moyenne=%8f",mean);
		}					
		
		for (i=0; i<n_fft/2+1; i++)
		{	omega   = (double)i/(double)(n_fft)*dom*2.*M_PI;
			omega2	= omega*omega;
			tmp1	= -omega2*omega*skew/(1.+omega2/(fabs(mean_m)*fabs(mean_m)));
			tmp2	= omega2*sigma_m/2.+omega2*omega2*kurt/(1.+omega2/(fabs(mean_m)*fabs(mean_m))); 
			psi[i][0] = cos(tmp1)*exp(-tmp2)/(1.+omega2/(fabs(mean_m)*fabs(mean_m)));
			psi[i][1] = sin(tmp1)*exp(-tmp2)/(1.+omega2/(fabs(mean_m)*fabs(mean_m)));
		
		}
		
		fftw_execute(plan_back); 
  
		if ((ds2 = create_and_attach_one_ds(op, nh, nh, 0)) == NULL)	return win_printf_OK("cannot create dataset !");
		
		
		for (i=0; i<nh; i++)
		{	ds2->xd[i] = (float)(i-nh/2)/(float)(nh-1.)*(float)n_fft/dom;
			if (i<nh/2)	
			{	ds2->yd[i] = 0.;
				for (k=0; k<fft_factor; k++)
					ds2->yd[i] += (float)p[i*fft_factor+k +n_fft/2]/(float)n_fft;
				ds2->yd[i] /= (float)(fft_factor);
			}
			else		
			{	ds2->yd[i] = 0.;
				for (k=0; k<fft_factor; k++)
					ds2->yd[i] += (float)p[i*fft_factor+k -n_fft/2]/(float)n_fft; 
				ds2->yd[i] /= (float)(fft_factor);
			}
			if (bool_Q==0) 
			{	ds2->xd[i] *= (float)scale*fabs(mean);
				ds2->xd[i] += (float)scale*mean;
			}
			if (bool_Q==1)
			{	ds2->xd[i] *= (float) fabs(mean);
				ds2->xd[i] += (float) mean;
			}
			if (bool_Q==2)
			{	ds2->xd[i] += (float) 1.;
			}
		}
		inherit_from_ds_to_ds(ds2, ds1);
		ds2->treatement = my_sprintf(ds2->treatement,"theoretical pdf for Q, sinus stat");

	}
	fftw_free(psi);
	fftw_free(p);
	
   	 fftw_destroy_plan(plan_back);

	if (win_scanf("Remove old pdfs ?")!=CANCEL)
	for (j=0; j<n; j++)
	{	remove_ds_n_from_op (op, 0);	// always remove the 0th because counting is shifted after any one is deleted
	}

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of "do_graph_pdf_Q_ramps_stat"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int do_compute_U_vs_theta(void)
{	register int 	i,j;
	O_p 	*op = NULL, *op2 = NULL;
	d_s		*ds2;
	pltreg	*pr = NULL;
	float	tmpd;
static float kappa=0.5625;
static int ny=1000;
	
	if(updating_menu_state != 0)	return D_O_K;	
	
	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This plugins compute U vs theta for freedericksz transition\n\n"
					"for planar anchoring\n\n"
					"A new plot is created");
	}
	
	i=win_scanf("{\\pt14\\color{yellow} Parameters}\n\n"
			"anisotropy \\kappa= %8f Hz\n\n"
			"Number of points %d",
			&kappa, &ny);
	if (i==CANCEL) return(D_O_K);
	
		
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)				return win_printf_OK("cannot plot region");		
	if ((op2 = create_and_attach_one_plot(pr, ny, ny, 0)) == NULL)	return win_printf_OK("cannot create plot !");
	ds2 = op2->dat[0];
	for (i=0; i<ny; i++)
	{	tmpd = 0.;
		for (j=0; j<i; j++)
		{tmpd += M_PI/(2.*ny)*1./sqrt((cos(M_PI*j/(2.*ny))*cos(M_PI*j/(2.*ny))-cos(M_PI*i/(2.*ny))*cos(M_PI*i/(2.*ny)))/(1+kappa*sin(M_PI*j/(2.*ny))*sin(M_PI*j/(2.*ny))));
		}
		ds2->xd[i] = i*M_PI/(2.*ny);
		ds2->yd[i] =tmpd*2.*sqrt(6.4/(13*8.854187));
	}

	return refresh_plot(pr, UNCHANGED);
} /* end of function 'do_compute_U_vs_theta' */

int do_compute_theta_vs_z(void)
{	register int 	i,j, k;
	O_p 	*op = NULL, *op2 = NULL;
	d_s 	*ds2;
	pltreg	*pr = NULL;
	float	tmpd;
static float kappa=0.5625, U_max=1.5;
	float *theta_0;
static int ny=1000, n_E = 10.;
	int i_ref=0;
	
	if(updating_menu_state != 0)	return D_O_K;	
	
	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This plugins compute theta vs z for freedericksz transition\n\n"
					"for planar anchoring for some values of E\n\n"
					"A new plot is created. Each E corresponds to one dataset");
	}
	
	i=win_scanf("{\\pt14\\color{yellow} Parameters}\n\n"
			"anisotropy \\kappa= %8f\n\n"
			"Number of points %d\n\n"
			"Number of dataset %d\n\n"
			"max of voltage %8f",
			&kappa, &ny, &n_E, &U_max);
	if (i==CANCEL) return(D_O_K);
	

		
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)				return win_printf_OK("cannot plot region");		
	if ((op2 = create_and_attach_one_plot(pr, 2*ny, 2*ny, 0)) == NULL)	return win_printf_OK("cannot create plot !");
	ds2 = op2->dat[0];
	theta_0=(float*)calloc(ny, sizeof(float));
	for (i=0; i<ny; i++) theta_0[i] = 0.;
			
	for (i=0; i<ny; i++)
	{	tmpd = 0.;
		for (j=0; j<i; j++)
		{tmpd += M_PI/(2.*ny)*1./sqrt((cos(M_PI*j/(2.*ny))*cos(M_PI*j/(2.*ny))-cos(M_PI*i/(2.*ny))*cos(M_PI*i/(2.*ny)))/(1+kappa*sin(M_PI*j/(2.*ny))*sin(M_PI*j/(2.*ny))));
		}
		theta_0[i] = tmpd*2.*sqrt(6.4/(13*8.854187));
	}

	for (k=0; k<n_E; k++)
	{
	if (k!=0) // not the first index
		{	if ((ds2 = create_and_attach_one_ds(op2, 2*ny, 2*ny, 0)) == NULL)	
					return win_printf_OK("cannot create dataset !");
		}
	for (i=0; i<ny; i++)
	{	if(((float)(k+1)/n_E*U_max-theta_0[i])>0) i_ref=i+1;
	}
	for (i=0; i<ny; i++)
	{	tmpd = 0.;
		if (i+1<i_ref)
		{
		for (j=0; j<i; j++)
		{tmpd += M_PI/(2.*ny)*1./sqrt((cos(M_PI*j/(2.*ny))*cos(M_PI*j/(2.*ny))-cos(M_PI*i_ref/(2.*ny))*cos(M_PI*i_ref/(2.*ny)))/(1+kappa*sin(M_PI*j/(2.*ny))*sin(M_PI*j/(2.*ny))));
		}
		ds2->xd[i] = (float)M_PI*i/(2.*ny);
		ds2->yd[i] =tmpd*2.*sqrt(6.4/(13*8.854187))*(float)n_E/(k+1)*1./U_max;
		}
		else 
		{
		ds2->xd[i]=0;
		ds2->yd[i]=0;
		}
	}
	for (i=0; i<ny; i++)
	{	tmpd = 0.;
		if (i+1<i_ref)
		{
		for (j=0; j<i; j++)
		{tmpd += M_PI/(2.*ny)*1./sqrt((cos(M_PI*j/(2.*ny))*cos(M_PI*j/(2.*ny))-cos(M_PI*i_ref/(2.*ny))*cos(M_PI*i_ref/(2.*ny)))/(1+kappa*sin(M_PI*j/(2.*ny))*sin(M_PI*j/(2.*ny))));
		}
		ds2->xd[2*ny-i] = (float)M_PI*i/(2.*ny);
		ds2->yd[2*ny-i] =2-tmpd*2.*sqrt(6.4/(13*8.854187))*(float)n_E/(k+1)*1./U_max;
		}
		else 
		{
		ds2->xd[2*ny-i]=0;
		ds2->yd[2*ny-i]=0;
		}
	}
	ds2->treatement = my_sprintf(ds2->treatement,"theoretical theta vs z for U = %8f",(float)(k+1)/n_E*U_max);

}
free(theta_0);
	

	return refresh_plot(pr, UNCHANGED);
} /* end of function 'do_compute_theta_vs_z' */

int do_compute_approx_theta_vs_z(void)
{	register int 	i,j, k;
	O_p 	*op = NULL, *op2 = NULL;
	d_s 	*ds2;
	pltreg	*pr = NULL;
	float	tmpd;
static float kappa=0.5625, U_max=1.5;
	float *theta_0;
static int ny=1000, n_E = 10.;
	int i_ref=0;
	
	if(updating_menu_state != 0)	return D_O_K;	
	
	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This plugins compute theta vs z for freedericksz transition\n\n"
					"for planar anchoring for some values of E\n\n"
					"A new plot is created. Each E corresponds to one dataset");
	}
	
	i=win_scanf("{\\pt14\\color{yellow} Parameters}\n\n"
			"anisotropy \\kappa= %8f Hz\n\n"
			"Number of points %d\n\n"
			"Number of dataset %d\n\n"
			"max of voltage %8f",
			&kappa, &ny, &n_E, &U_max);
	if (i==CANCEL) return(D_O_K);
	
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)				return win_printf_OK("cannot plot region");		
	if ((op2 = create_and_attach_one_plot(pr, 2*ny, 2*ny, 0)) == NULL)	return win_printf_OK("cannot create plot !");
	ds2 = op2->dat[0];
	theta_0=(float*)calloc(ny, sizeof(float));
	for (i=0; i<ny; i++) theta_0[i] = 0.;
			
	for (i=0; i<ny; i++)
	{	tmpd = 0.;
		for (j=0; j<i; j++)
		{tmpd += M_PI/(2.*ny)*1./sqrt((cos(M_PI*j/(2.*ny))*cos(M_PI*j/(2.*ny))-cos(M_PI*i/(2.*ny))*cos(M_PI*i/(2.*ny)))/(1+kappa*sin(M_PI*j/(2.*ny))*sin(M_PI*j/(2.*ny))));
		}
		theta_0[i] = tmpd*2.*sqrt(6.4/(13*8.854187));
	}

	for (k=0; k<n_E; k++)
	{
	if (k!=0) // not the first index
		{	if ((ds2 = create_and_attach_one_ds(op2, 2*ny, 2*ny, 0)) == NULL)	
					return win_printf_OK("cannot create dataset !");
		}
	for (i=0; i<ny; i++)
	{	if(((float)(k+1)/n_E*U_max-theta_0[i])>0) i_ref=i+1;
	}
	for (i=0; i<(2*ny); i++)
	{	ds2->yd[i] =(float)M_PI*i_ref/(2.*ny)*sin((float)i*M_PI/(2.*ny));
		ds2->xd[i] =(float)i/ny;
	}
		ds2->treatement = my_sprintf(ds2->treatement,"theoretical theta vs z for U = %8f",(float)(k+1)/n_E*U_max);


}
free(theta_0);
	

	return refresh_plot(pr, UNCHANGED);
} /* end of function 'do_compute_approx_theta_vs_z' */

MENU *graphs_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;

	add_item_to_menu(mn,"sigma(tau) ramps / stat",			do_graph_sigma_ramps_stat,		NULL, 0, NULL);
	add_item_to_menu(mn,"sigma(tau) ramps / stat for Jarzynski",	do_graph_sigma_ramps_Jarzynski_stat,	NULL, 0, NULL);
	add_item_to_menu(mn,"sigma(tau) ramps overdamped / stat",	do_graph_sigma_ramps_stat_1o,		NULL, 0, NULL);
	add_item_to_menu(mn,"sigma(tau) W sinus / stat",		do_graph_sigma_sinus_stat,		NULL, 0, NULL);
	add_item_to_menu(mn,"\0",						0,				NULL, 0, NULL);	
	add_item_to_menu(mn,"pdf W sinus / stat",			do_graph_pdf_W_sinus_stat,		NULL, 0, NULL);
	add_item_to_menu(mn,"pdf Q sinus / stat (direct)",		do_graph_pdf_Q_sinus_stat,		NULL, 0, NULL);
	add_item_to_menu(mn,"pdf Q sinus / stat (TF)",			do_graph_pdf_Q_sinus_stat_TF,		NULL, 0, NULL);
	add_item_to_menu(mn,"pdf WJ ramps / stat",			do_graph_pdf_WJVZ_ramps_stat,		NULL, 0, NULL);
	add_item_to_menu(mn,"pdf Q ramps / stat (TF)",			do_graph_pdf_Q_ramps_stat_TF,		NULL, 0, NULL);
	add_item_to_menu(mn,"\0",						0,				NULL, 0, NULL);	
	add_item_to_menu(mn,"freedericksz transition theta vs U",			do_compute_U_vs_theta,		NULL, 0, NULL);
	add_item_to_menu(mn,"freedericksz transition theta vs z",			do_compute_theta_vs_z,		NULL, 0, NULL);
	add_item_to_menu(mn,"freedericksz transition cos aprox theta vs z",			do_compute_approx_theta_vs_z,		NULL, 0, NULL);

//	add_item_to_menu(mn,"\0",						0,				NULL, 0, NULL);	
	
	return(mn);
}

int	graphs_main(int argc, char **argv)
{
	add_plot_treat_menu_item ("graphs", NULL, graphs_plot_menu(), 0, NULL);
	return D_O_K;
}


int	graphs_unload(int argc, char **argv)
{ 
	remove_item_to_menu(plot_treat_menu,"graphs",	NULL, NULL);
	return D_O_K;
}
#endif

