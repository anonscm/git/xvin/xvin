/*
*    Plug-in  DeleteRegKey HKCR .gr
  DeleteRegKey HKCR XVingr
 program for image treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _TRMOV_C_
#define _TRMOV_C_

# include "allegro.h"
# include "winalleg.h"
# include "xvin.h"
# include "fftl32n.h"
# include "fillib.h"
//# include "largeint.h"
# include "windows.h"
# include "../nrutil/nrutil.h"



#include <stdio.h>
//typedef int v4sf __attribute__ ((mode(V4SF))); // vector of four single floats
typedef float v4sf __attribute__ ((vector_size (16))); // vector of four single floats
union f4vector 
{
  v4sf v;
  float f[4];
};
      
union f4vector f4, fax, fay, f0, fx, fy, fxc, fyc, fy2, fr[64], fx2[64];



// C#
/*
void CreateTimer()
{
   System.Timers.Timer Timer1 = new System.Timers.Timer();
   Timer1.Enabled = true;
   Timer1.Interval = 5000;
   Timer1.Elapsed += 
      new System.Timers.ElapsedEventHandler(Timer1_Elapsed);
}

void Timer1_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
{
  static n = 0;

  display_title_message("event %d",n++);
  //   System.Windows.Forms.MessageBox.Show("Elapsed!", 
  //    "Timer Event Raised!");
}
*/

O_p *idle_op = NULL;
long long idle_t0;
long long idle_dt;



/* If you include other plug-ins header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "trmov.h"


unsigned char ch_max = 255;
int	filter_ratio = 2;
float radial_dx[256];
int	bead_lost_mul = 16;







int translate_fft(float *x, int nx, float dx, float *xt)
{
	register int i,j;
	double phase, co, si, re, im;
	
	phase = -2*M_PI*dx/nx;
	for ( i=1 ; i<nx/2 ; i++)
	{
		j = 2*i;
		co = cos(phase*i);
		si = sin(phase*i);
		re = (double) x[j];
		im = (double) x[j+1];
		xt[j]   = (float)( co*re - si*im);
		xt[j+1] = (float)( co*im + si*re);
	}
	xt[0] = x[0];
	xt[1] = x[1];
	return 0;
}

int translate_in_real_space_using_fft(float *x, int nx, float dx)
{
  register int j;
  if ((j = fft_init(nx)) != 0)    return j;

  realtr1(nx, x);
  fft(nx, x, 1);
  realtr2(nx, x, 1);
  translate_fft(x, nx, dx, x);
  realtr2(nx, x, -1);
  fft(nx, x, -1);
  realtr1(nx, x);
  return 0;
}


int change_mouse_to_cross(int cl, int cw)
{
    BITMAP *ds_bitmap = NULL;
    int color, cw2, cl2;

    cl2 = cl/2;
    cw2 = cw/2;
    ds_bitmap = create_bitmap(cl+1,cl+1);
    cl = Lightmagenta;
    color = makecol(EXTRACT_R(cl), EXTRACT_G(cl), EXTRACT_B(cl));
    clear_to_color(ds_bitmap, color);

    color = makecol(255, 0, 0);                        

    line(ds_bitmap,0,cl2-cw2,0,cl2+cw2,color);
    line(ds_bitmap,cl,cl2-cw2,cl,cl2+cw2,color);

    line(ds_bitmap,cl2-cw2,0,cl2+cw2,0,color);
    line(ds_bitmap,cl2-cw2,cl,cl2+cw2,cl,color);

    line(ds_bitmap,0,cl2-cw2,cl2-cw2,cl2-cw2,color);
    line(ds_bitmap,cl,cl2-cw2,cl2-cw2,cl2-cw2,color);
    line(ds_bitmap,0,cl2+cw2,cl2-cw2,cl2+cw2,color);
    line(ds_bitmap,cl,cl2+cw2,cl2-cw2,cl2+cw2,color);

    line(ds_bitmap,cl2-cw2,0,cl2-cw2,cl2-cw2,color);
    line(ds_bitmap,cl2+cw2,0,cl2+cw2,cl2-cw2,color);
    line(ds_bitmap,cl2-cw2,cl,cl2-cw2,cl2+cw2,color);
    line(ds_bitmap,cl2+cw2,cl,cl2+cw2,cl2+cw2,color);

    scare_mouse();    
    set_mouse_sprite(ds_bitmap);
    set_mouse_sprite_focus(cl2, cl2);
    unscare_mouse();
    return 0;    
}
int reset_mouse(void)
{
    scare_mouse();    
    set_mouse_sprite(NULL);
    unscare_mouse();
    return 0;
}

int place_tracking_cross_with_mouse_movie(int *xc, int *yc, int cl, int cw, int color)
{
	int  xm, ym;
	imreg *imr;

    imr = find_imr_in_current_dialog(NULL);
	if (imr == NULL)	return 1;	
	
	xm = mouse_x = x_imdata_2_imr(imr, *xc);	
	ym = mouse_y = y_imdata_2_imr(imr, *yc);	
	for(; !key[KEY_ENTER]; )
	{
		if (xm != mouse_x || ym != mouse_y)
		{
			xm = mouse_x;
			ym = mouse_y;
			refresh_image(imr,UNCHANGED);
			//draw_cross_vga(xm, ym, cl, cw, color, imr, screen, find_dialog_associated_to_imr(imr, NULL));// _screen_unit	  
			draw_cross_vga_screen_unit(xm, ym, cl, cw, color, imr, screen); 	  
		}
	}
	*xc = x_imr_2_imdata(imr, xm);
	*yc = y_imr_2_imdata(imr, ym);
	return 0;
}
							


int	draw_cross_vga_screen_unit(int xc, int yc, int length, int width, int color, imreg *imrs, BITMAP* bm)
{
        int l = length >> 1;
	int w = width >> 1;
	int y = bm->h - yc;
	int x1 =  xc - l;
	int x4 =  xc + l;
	int x2 =  xc - w;
	int x3 =  xc + w;
	int y1 =  y + l;
	int y4 =  y - l;
	int y2 =  y + w;
	int y3 =  y - w;

	(void)imrs;
	line(bm, x1, y2, x2, y2, color);
	line(bm, x2, y2, x2, y1, color);
	line(bm, x2, y1, x3, y1, color);
	line(bm, x3, y1, x3, y2, color);
	line(bm, x3, y2, x4, y2, color);
	line(bm, x4, y2, x4, y3, color);
	line(bm, x3, y3, x4, y3, color);
	line(bm, x3, y3, x3, y4, color);
	line(bm, x2, y4, x3, y4, color);
	line(bm, x2, y3, x2, y4, color);
	line(bm, x1, y3, x2, y3, color);
	line(bm, x1, y2, x1, y3, color);
	return 0;						
}														

																			
int	draw_cross_vga(int xc, int yc, int length, int width, int color, imreg *imrs, BITMAP* bm, DIALOG *d)
{
	int x1 =  xc - (length >> 1);
	int x4 =  xc + (length >> 1);
	int x2 =  xc - (width >> 1);
	int x3 =  xc + (width >> 1);	
	int y1 =  yc - (length >> 1);
	int y4 =  yc + (length >> 1);
	int y2 =  yc - (width >> 1);
	int y3 =  yc + (width >> 1);

		
	x1 = x_imdata_2_imr(imrs,x1) + d->x;
	x2 = x_imdata_2_imr(imrs,x2) + d->x;
	x3 = x_imdata_2_imr(imrs,x3) + d->x;
	x4 = x_imdata_2_imr(imrs,x4) + d->x;
	y1 = y_imdata_2_imr(imrs,y1) + d->y;
	y2 = y_imdata_2_imr(imrs,y2) + d->y;
	y3 = y_imdata_2_imr(imrs,y3) + d->y;
	y4 = y_imdata_2_imr(imrs,y4) + d->y;


	line(bm, x1, y2, x2, y2, color);
	line(bm, x2, y2, x2, y1, color);
	line(bm, x2, y1, x3, y1, color);
	line(bm, x3, y1, x3, y2, color);
	line(bm, x3, y2, x4, y2, color);
	line(bm, x4, y2, x4, y3, color);
	line(bm, x3, y3, x4, y3, color);
	line(bm, x3, y3, x3, y4, color);
	line(bm, x2, y4, x3, y4, color);
	line(bm, x2, y3, x2, y4, color);
	line(bm, x1, y3, x2, y3, color);
	line(bm, x1, y2, x1, y3, color);
	return 0;						
}
int	draw_cross_X_vga(int xc, int yc, int length, int width, int color, imreg *imrs, BITMAP* bm, DIALOG *d)
{
	int x1 = xc - ((length+width) >> 1);
	int x2 = xc - ((length-width) >> 1);
	int x3 = xc - width;	
	int x4 = xc;
	int x5 = xc + width;		
	int x6 = xc + ((length-width) >> 1);
	int x7 = xc + ((length+width) >> 1);

	int y1 = yc - ((length+width) >> 1);
	int y2 = yc - ((length-width) >> 1);
	int y3 = yc - width;	
	int y4 = yc;
	int y5 = yc + width;		
	int y6 = yc + ((length-width) >> 1);
	int y7 = yc + ((length+width) >> 1);
			
	
	x1 = x_imdata_2_imr(imrs,x1) + d->x;
	x2 = x_imdata_2_imr(imrs,x2) + d->x;
	x3 = x_imdata_2_imr(imrs,x3) + d->x;
	x4 = x_imdata_2_imr(imrs,x4) + d->x;	
	x5 = x_imdata_2_imr(imrs,x5) + d->x;
	x6 = x_imdata_2_imr(imrs,x6) + d->x;
	x7 = x_imdata_2_imr(imrs,x7) + d->x;	

	y1 = y_imdata_2_imr(imrs,y1) + d->y;
	y2 = y_imdata_2_imr(imrs,y2) + d->y;
	y3 = y_imdata_2_imr(imrs,y3) + d->y;
	y4 = y_imdata_2_imr(imrs,y4) + d->y;
	y5 = y_imdata_2_imr(imrs,y5) + d->y;
	y6 = y_imdata_2_imr(imrs,y6) + d->y;
	y7 = y_imdata_2_imr(imrs,y7) + d->y;

	line(bm, x5, y4, x7, y6, color);
	line(bm, x7, y6, x6, y7, color);
	line(bm, x6, y7, x4, y5, color);
	line(bm, x4, y5, x2, y7, color);
	line(bm, x2, y7, x1, y6, color);
	line(bm, x1, y6, x3, y4, color);
	line(bm, x3, y4, x1, y2, color);
	line(bm, x1, y2, x2, y1, color);
	line(bm, x2, y1, x4, y3, color);
	line(bm, x4, y3, x6, y1, color);
	line(bm, x6, y1, x7, y2, color);
	line(bm, x7, y2, x5, y4, color);

	return 0;						
}

int	draw_cross_X_vga_screen_unit(int xc, int yc, int length, int width, int color,  BITMAP* bm)
{
        int l = (length + width) >> 1;
	int w = (length - width) >> 1;
	int y = bm->h - yc;

	int x1 = xc - l;
	int x2 = xc - w;
	int x3 = xc - width;	
	int x4 = xc;
	int x5 = xc + width;		
	int x6 = xc + w;
	int x7 = xc + l;

	int y1 = y + l;
	int y2 = y + w;
	int y3 = y + width;	
	int y4 = y;
	int y5 = y - width;		
	int y6 = y - w;
	int y7 = y - l;
			
	line(bm, x5, y4, x7, y6, color);
	line(bm, x7, y6, x6, y7, color);
	line(bm, x6, y7, x4, y5, color);
	line(bm, x4, y5, x2, y7, color);
	line(bm, x2, y7, x1, y6, color);
	line(bm, x1, y6, x3, y4, color);
	line(bm, x3, y4, x1, y2, color);
	line(bm, x1, y2, x2, y1, color);
	line(bm, x2, y1, x4, y3, color);
	line(bm, x4, y3, x6, y1, color);
	line(bm, x6, y1, x7, y2, color);
	line(bm, x7, y2, x5, y4, color);

	return 0;						
}

int	move_cross(int *xc, float xcf, int *yc, float ycf, int cl, int cw, 
			imreg *imrs, BITMAP* bm, DIALOG *d)
{
	draw_cross_vga(*xc, *yc, cl, cw, 0, imrs, bm, d);
	*xc = (int)xcf;	*yc = (int)ycf;
	*xc = (*xc < imrs->one_i->im.nx - cl/2) ? *xc : imrs->one_i->im.nx - cl/2;
	*yc = (*yc < imrs->one_i->im.ny - cl/2) ? *yc : imrs->one_i->im.ny - cl/2;
	*xc = (*xc < cl/2) ? cl/2 : *xc;
	*yc = (*yc < cl/2) ? cl/2 : *yc;
	draw_cross_vga(*xc, *yc, cl, cw, 1, imrs, bm, d);	
	return 0;
}
/* a revoir */
int	move_X_cross(int *xc, float xcf, int *yc, float ycf, int cl, int cw, 
			imreg *imrs, BITMAP* bm, DIALOG *d)
{
	draw_cross_X_vga(*xc, *yc, cl, cw, 0, imrs, bm, d);
	*xc = (int)(xcf+.5);	*yc = (int)(ycf+.5);
	*xc = (*xc < imrs->one_i->im.nx - (cl+cw)/2) ? 
		*xc : imrs->one_i->im.nx - (cl+cw)/2;
	*yc = (*yc < imrs->one_i->im.ny - (cl+cw)/2) ? 
		*yc : imrs->one_i->im.ny - (cl+cw)/2;
	*xc = (*xc < (cl+cw)/2) ? (cl+cw)/2 : *xc;
	*yc = (*yc < (cl+cw)/2) ? (cl+cw)/2 : *yc;
	draw_cross_X_vga(*xc, *yc, cl, cw, 1, imrs, bm, d);	
	return 0;
}
int     erase_around_black_circle(int *x, int *y, int cl)
{
	register int j;
	int xb0, xb1, zb0, zb1;
	
	/* 	for good bead there is a definite black circle around the image
		for rotation with a small bead you can suppress the image around
		this black circle to avoid perturbation by the small bead
	*/
		
	for (xb0 = j = 0, zb0 = x[0]; j < cl>>1; j++)
		zb0 = (zb0 < x[j]) ? zb0 : x[(xb0 = j)];
	for (xb1 = j = cl-1, zb1 = x[xb1]; j >= cl>>1; j--)
		zb1 = (zb1 < x[j]) ? zb1 : x[(xb1 = j)];
	zb0 = (zb0 > zb1) ? zb0 : zb1;
	for (j = 0; j < cl; j++)
		x[j] = (j < xb0 || j > xb1) ? 0 : x[j] - zb0;
	for (xb0 = j = 0, zb0 = y[0]; j < cl>>1; j++)
		zb0 = (zb0 < y[j]) ? zb0 : y[(xb0 = j)];
	for (xb1 = j = cl-1, zb1 = y[xb1]; j >= cl>>1; j--)
		zb1 = (zb1 < y[j]) ? zb1 : y[(xb1 = j)];
	zb0 = (zb0 > zb1) ? zb0 : zb1;
	for (j = 0; j < cl; j++)
		y[j] = (j < xb0 || j > xb1) ? 0 : y[j] - zb0;
	return 0;	
}	


int	check_bead_not_lost(int *x, float *x1, int *y, float *y1, int cl, float bd_mul)
{
	register  int i, j;
	int minx, maxx, miny, maxy;
	
	for (i = 0, minx = maxx = x[0], miny = maxy = y[0]; i < cl; i++)
	{
		j = x[i];
		minx = (j < minx) ? j : minx;
		maxx = (j > maxx) ? j : maxx;			
		x1[i] = (float)j;
		j = y[i];
		miny = (j < miny) ? j : miny;
		maxy = (j > maxy) ? j : maxy;						
		y1[i] = (float)j;			
	}
	return (((maxx-minx)*bd_mul) < (maxx+minx) && 
		((maxy-miny)*bd_mul) < (maxy+miny)) ? 1 : 0;
}



int	check_double_bead_image_not_lost(int *xi, float *x1, int *yi1, float *y1, 
					 int *yi2, float *y2, int cl, float bd_mul)
{
	register  int i, j;
	int minx, maxx, miny, maxy, clx2;
	
	clx2 = cl + cl;
	for (i = 0,  minx = maxx = xi[0]; i < clx2; i++)
	{
		j = xi[i];
		minx = (j < minx) ? j : minx;
		maxx = (j > maxx) ? j : maxx;			
		x1[i] = (float)j;
	}
	for (i = 0, miny = maxy = yi1[0]; i < cl; i++)
	{
		j = yi1[i];
		miny = (j < miny) ? j : miny;
		maxy = (j > maxy) ? j : maxy;			
		y1[i] = (float)j;
		j = yi2[i];
		miny = (j < miny) ? j : miny;
		maxy = (j > maxy) ? j : maxy;						
		y2[i] = (float)j;			
	}
	return (((maxx-minx)*bd_mul) < (maxx+minx) && 
		((maxy-miny)*bd_mul) < (maxy+miny)) ? 1 : 0;
}



/*	grab all odd line pixels in an image of char, 
 *	ytt is the position in y of the top line
 *	xll is the position in x of the leftmost pixel
 */
		  
int	grab_all_odd_line_from_movie(O_i *oi, int ytt, int xll, O_i *movie)
{
	register int i, j, k;
	int  onx, ony;
	union pix *pd, *pdm;
	unsigned char *choi;	
	unsigned char *cy;	
		
	if (oi == NULL) 		return 1;
	onx = oi->im.nx;	ony = oi->im.ny;
	if (oi->im.data_type != IS_CHAR_IMAGE || movie->im.nx < onx 
	|| movie->im.ny < ony)	return 2;
	pd = oi->im.pixel;	pdm = movie->im.pixel;
	/* save all odd lines */
	for (j = (ytt % 2) ? 0: 1; j< ony; j+= 2)
	{
		k = (ytt+j);
		cy = pdm[k].ch + xll;
		choi = pd[j].ch;
		for (i = 0; i < onx; i++)	
		  choi[i] = (cy[i] < ch_max) ? cy[i] : ch_max;
	}
	return 0;
}
/*	grab all even line pixels in an image of char, 
 *	ytt is the position in y of the top line
 *	xll is the position in x of the leftmost pixel
 */
		  
int	grab_all_even_line_from_movie(O_i *oi, int ytt, int xll, O_i *movie)
{
	register int i, j, k;
	int  onx, ony;
	union pix *pd, *pdm;
	unsigned char *choi;	
	unsigned char *cy;	
			
	if (oi == NULL) 		return 1;
	onx = oi->im.nx;	ony = oi->im.ny;
	if (oi->im.data_type != IS_CHAR_IMAGE || movie->im.nx < onx 
	|| movie->im.ny < ony)	return 2;
	pd = oi->im.pixel;	pdm = movie->im.pixel;
	/* save all odd lines */
	for (j = (ytt % 2) ? 1: 0; j< ony; j+= 2)
	{
		k = (ytt+j);
		cy = pdm[k].ch + xll;
		choi = pd[j].ch;
		for (i = 0; i < onx; i++)	
		  choi[i] = (cy[i] < ch_max) ? cy[i] : ch_max;
	}
	return 0;
}

int	fill_odd_avg_profile_from_im(O_i *oi, int xc, int yc, int cl, int cw, int *x, int *y)
{
	register int j, i;
	unsigned char *choi;	
	int ytt, xll, onx, ony;
	union pix *pd;
	
	onx = oi->im.nx;	ony = oi->im.ny;
	pd = oi->im.pixel;
	ytt = yc - cl/2;
	xll = xc - cl/2;
	ytt = (ytt < 0) ? 0 : ytt;
	ytt = (ytt >= ony - cl) ? ony - cl - 1 : ytt;
	xll = (xll < 0) ? 0 : xll;
	xll = (xll >= onx - cl) ? onx - cl - 1 : xll;	
	for (j = ((ytt+(cw>>1)) % 2) ? 0 : 1; j< cw; j+= 2)
	{	
		choi = pd[((cl-cw)>>1)+j].ch + xll;
		for (i = 0; i < cl; i++)		x[i] += (int)choi[i];	
	}
	for (j = (ytt % 2) ? 0 : 1; j< cl; j+= 2)
	{	
		choi = pd[j].ch;
		for (i = (cl-cw)>>1; i < (cl+cw)>>1; i++)	
			y[j] += (int)choi[i];	
	}
	return 0;		
}

int	fill_avg_profiles_from_im(O_i *oi, int xc, int yc, int cl, int cw, int *x, int *y)
{
	register int j, i;
	unsigned char *choi;	
	short int *inoi;
	unsigned short int *uioi;
	int ytt, xll, onx, ony, xco, yco, cl2, cw2;
	union pix *pd;
	
	if (oi == NULL || x == NULL || y == NULL)	return 1;
	onx = oi->im.nx;	ony = oi->im.ny;
	pd = oi->im.pixel;
	cl2 = cl >> 1;	cw2 = cw >> 1;
	yco = (yc - cl2 < 0) ? cl2 : yc;
	yco = (yco + cl2 <= ony) ? yco : ony - cl2;
	xco = (xc - cl2 < 0) ? cl2 : xc;
	xco = (xco + cl2 <= onx) ? xco : onx - cl2;
	for(i = 0; i < cl; i++)		x[i] = y[i] = 0;	
	if (oi->im.data_type == IS_CHAR_IMAGE)
	{
		for(j = 0, ytt = yco - cw2, xll = xco - cl2; j< cw; j++)
		{	
			choi = pd[ytt+j].ch + xll;
			for (i = 0; i < cl; i++)		x[i] += (int)choi[i];	
		}
		for(j = 0, ytt = yco - cl2, xll = xco - cw2; j <cl; j++)
		{	
			choi = pd[ytt+j].ch + xll;
			for (i = 0; i < cw; i++)		y[j] += (int)choi[i];	
		}
	}
	else if (oi->im.data_type == IS_INT_IMAGE)
	{
		for(j = 0, ytt = yco - cw2, xll = xco - cl2; j< cw; j++)
		{	
			inoi = pd[ytt+j].in + xll;
			for (i = 0; i < cl; i++)		x[i] += (int)inoi[i];	
		}
		for(j = 0, ytt = yco - cl2, xll = xco - cw2; j <cl; j++)
		{	
			inoi = pd[ytt+j].in + xll;
			for (i = 0; i < cw; i++)		y[j] += (int)inoi[i];	
		}
	}
	else if (oi->im.data_type == IS_UINT_IMAGE)
	{
		for(j = 0, ytt = yco - cw2, xll = xco - cl2; j< cw; j++)
		{	
			uioi = pd[ytt+j].ui + xll;
			for (i = 0; i < cl; i++)		x[i] += (int)uioi[i];	
		}
		for(j = 0, ytt = yco - cl2, xll = xco - cw2; j <cl; j++)
		{	
			uioi = pd[ytt+j].ui + xll;
			for (i = 0; i < cw; i++)		y[j] += (int)uioi[i];	
		}
	}
	else return 1;	
	return 0;		
}

/*
  average profiles of double beads images 
  with x-arm having 2*cl points along x
  + 2 y arms distant of dx extending of cl points 
  
 */

int	fill_avg_profiles_double_cross_from_im(O_i *oi, int xc, int yc, int dx,int cl, 
					       int cw, int *x, int *y1, int *y2)
{
	register int j, i;
	unsigned char *choi;	
	short int *inoi;
	unsigned short int *uioi;
	int ytt, xll, onx, ony, xco, yco, cl2, cw2, clx2;
	union pix *pd;
	
	if (oi == NULL || x == NULL || y1 == NULL || y2 == NULL)	return 1;
	onx = oi->im.nx;	ony = oi->im.ny;
	pd = oi->im.pixel;
	cl2 = cl >> 1;	cw2 = cw >> 1;
	clx2 = cl + cl;
	yco = (yc - cl2 < 0) ? cl2 : yc;
	yco = (yco + cl2 <= ony) ? yco : ony - cl2;
	xco = (xc - cl < 0) ? cl : xc;
	xco = (xco + cl <= onx) ? xco : onx - cl;
	for(i = 0; i < 2*cl; i++)		x[i] = 0;	
	for(i = 0; i < cl; i++)		y1[i] = y2[i] = 0;	
	if (oi->im.data_type == IS_CHAR_IMAGE)
	{
		for(j = 0, ytt = yco - cw2, xll = xco - cl; j< cw; j++)
		{	
			choi = pd[ytt+j].ch + xll;
			for (i = 0; i < clx2; i++)		x[i] += (int)choi[i];	

		}
		for(j = 0, ytt = yco - cl2, xll = xco - cw2; j <cl; j++)
		{	
			choi = pd[ytt+j].ch + xll - dx;
			for (i = 0; i < cw; i++)		y1[j] += (int)choi[i];	
			choi = pd[ytt+j].ch + xll + dx;
			for (i = 0; i < cw; i++)		y2[j] += (int)choi[i];	
		}
	}
	else if (oi->im.data_type == IS_INT_IMAGE)
	{
		for(j = 0, ytt = yco - cw2, xll = xco - cl; j< cw; j++)
		{	
			inoi = pd[ytt+j].in + xll;
			for (i = 0; i < clx2; i++)		x[i] += (int)inoi[i];	
		}
		for(j = 0, ytt = yco - cl2, xll = xco - cw2; j <cl; j++)
		{	
			inoi = pd[ytt+j].in + xll - dx;
			for (i = 0; i < cw; i++)		y1[j] += (int)inoi[i];	
			inoi = pd[ytt+j].in + xll + dx;
			for (i = 0; i < cw; i++)		y2[j] += (int)inoi[i];	
		}
	}
	else if (oi->im.data_type == IS_UINT_IMAGE)
	{
		for(j = 0, ytt = yco - cw2, xll = xco - cl; j< cw; j++)
		{	
			uioi = pd[ytt+j].ui + xll;
			for (i = 0; i < clx2; i++)		x[i] += (int)uioi[i];	
		}
		for(j = 0, ytt = yco - cl2, xll = xco - cw2; j <cl; j++)
		{	
			uioi = pd[ytt+j].ui + xll - dx;
			for (i = 0; i < cw; i++)		y1[j] += (int)uioi[i];	
			uioi = pd[ytt+j].ui + xll + dx;
			for (i = 0; i < cw; i++)		y2[j] += (int)uioi[i];	
		}
	}
	else return 1;	
	return 0;		
}



// find barycenter and angle


int	fill_bary_avg_profiles_from_im(O_i *oi, int xc, int yc, int cl, float *mean, float *x, float *y, float *theta)
{
	register int j, i;
	unsigned char *choi;	
	short int *inoi;
	int ytt, xll, onx, ony, xco, yco, cl2, cw2, ntmp;
	float tmp, tmpx, tmpy, magx, magy, lgrey, dx, dy, cox, siy, x2 = 0, y2 = 0, x21 = 0, y21 = 0, sq2;
	//float xl0, yl0, xl1, yl1;
	union pix *pd;
	
	if (oi == NULL || x == NULL || y == NULL)	return 1;
	onx = oi->im.nx;	ony = oi->im.ny;
	pd = oi->im.pixel;
	cl2 = cl >> 1;	cw2 = cl >> 1;
	yco = (yc - cl2 < 0) ? cl2 : yc;
	yco = (yco + cl2 <= ony) ? yco : ony - cl2;
	xco = (xc - cl2 < 0) ? cl2 : xc;
	xco = (xco + cl2 <= onx) ? xco : onx - cl2;
	sq2 = sqrt(2)/2;
	if (oi->im.data_type == IS_CHAR_IMAGE)
	{
		for(j = 0, ytt = yco - cw2, xll = xco - cl2, tmp = 0, ntmp = 0; j< cl; j++)
		{	
			choi = pd[ytt+j].ch + xll;
			for (i = 0; i < cl; i++)		
			  {
			    tmp += choi[i];
			    ntmp++;
			  }
		}
		*mean = lgrey = (ntmp) ? tmp/ntmp : tmp;
		for(j = 0, ytt = yco - cw2, xll = xco - cl2, tmpx = tmpy = 0, magx = magy = 0; j< cl; j++)
		{	
			choi = pd[ytt+j].ch + xll;
			for (i = 0; i < cl; i++)		
			  {
			    tmp = lgrey - choi[i];
			    tmp *= tmp;
			    magx += tmp;
			    tmpx += (i - cl2) * tmp;
			    tmpy += (j - cl2) * tmp;
			  }
		}
		*x = tmpx = (magx != 0) ? tmpx/magx : tmpx;
		*y = tmpy = (magx != 0) ? tmpy/magx : tmpy;
		x2 = 0; y2 = 0; x21 = 0; y21 = 0;
		for(j = 0, ytt = yco - cw2, xll = xco - cl2; j< cl; j++)
		{	
			choi = pd[ytt+j].ch + xll;
			for (i = 0; i < cl; i++)		
			  {
			    tmp = lgrey - choi[i];
			    tmp *= tmp;
			    dx = tmpx - i + cl2;
			    dx = -dx;
			    dy = tmpy - j + cl2;
			    dy = -dy;
			    x2 += dx * dx * tmp;
			    y2 += dy * dy * tmp;
			    x21 += tmp * (sq2 * dx + sq2 * dy) * (sq2 * dx + sq2 * dy);
			    y21 += tmp * (sq2 * dy - sq2 * dx) * (sq2 * dy - sq2 * dx);
			  }
		}
		cox = (x2 - y2)/(2*magx);
		siy = (x21 - y21)/(2*magx);
		//x21 = (x2 + y2)/2;
		//if (x21 != 0) y21 = sqrt(cox*cox+siy*siy)/x21;
		*theta = atan2(siy,cox)/2;
		//win_printf("x2 %f y2 %f sum %f\nx21 %f y21 %f sum %f\ncox %f siy %f\ntheta %f",x2/magx,y2/magx,(x2+y2)/magx,x21/magx,y21/magx,(x21+y21)/magx,cox,siy,*theta);
		cox = cos(*theta);
		siy = sin(*theta);
		x2 = 0; y2 = 0; x21 = 0; y21 = 0;
		for(j = 0, ytt = yco - cw2, xll = xco - cl2; j< cl; j++)
		{	
			choi = pd[ytt+j].ch + xll;
			for (i = 0; i < cl; i++)		
			  {
			    tmp = lgrey - choi[i];
			    tmp *= tmp;
			    dx = tmpx - i + cl2;
			    dx = -dx;
			    dy = tmpy - j + cl2;
			    dy = -dy;
			    x2 += tmp * (cox * dx + siy * dy) * (cox * dx + siy * dy) * (cox * dx + siy * dy);
			  }
		}
		x2 /= magx;
		//if (skew != NULL) *skew = x2; 
	}
	else if (oi->im.data_type == IS_INT_IMAGE)
	{
		for(j = 0, ytt = yco - cw2, xll = xco - cl2, tmp = 0, ntmp = 0; j< cl; j++)
		{	
			inoi = pd[ytt+j].in + xll;
			for (i = 0; i < cl; i++)		
			  {
			    tmp += inoi[i];
			    ntmp++;
			  }
		}
		*mean = lgrey = (ntmp) ? tmp/ntmp : tmp;
		for(j = 0, ytt = yco - cw2, xll = xco - cl2, tmpx = tmpy = 0, magx = magy = 0; j< cl; j++)
		{	
			inoi = pd[ytt+j].in + xll;
			for (i = 0; i < cl; i++)		
			  {
			    tmp = lgrey - inoi[i];
			    tmp *= tmp;
			    magx += tmp;
			    tmpx += (i - cl2) * tmp;
			    tmpy += (j - cl2) * tmp;
			  }
		}
		*x = tmpx = (magx != 0) ? tmpx/magx : tmpx;
		*y = tmpy = (magx != 0) ? tmpy/magx : tmpy;
		for(j = 0, ytt = yco - cw2, xll = xco - cl2; j< cl; j++)
		{	
			inoi = pd[ytt+j].in + xll;
			for (i = 0; i < cl; i++)		
			  {
			    tmp = lgrey - inoi[i];
			    tmp *= tmp;
			    dx = tmpx - i + cl2;
			    dx = -dx;
			    dy = tmpy - j + cl2;
			    dy = -dy;
			    x2 += dx * dx * tmp;
			    y2 += dy * dy * tmp;
			    x21 += tmp * (sq2 * dx + sq2 * dy) * (sq2 * dx + sq2 * dy);
			    y21 += tmp * (sq2 * dy - sq2 * dx) * (sq2 * dy - sq2 * dx);
			  }
		}
		cox = (x2 - y2)/2;
		siy = (x21 - y21)/2;
		//x21 = (x2 + y2)/2;
		//if (x21 != 0) y21 = sqrt(cox*cox+siy*siy)/x21;
		*theta = atan2(siy,cox)/2;
		cox = cos(*theta);
		siy = sin(*theta);
		x2 = 0; y2 = 0; x21 = 0; y21 = 0;
		for(j = 0, ytt = yco - cw2, xll = xco - cl2; j< cl; j++)
		{	
			inoi = pd[ytt+j].in + xll;
			for (i = 0; i < cl; i++)		
			  {
			    tmp = lgrey - inoi[i];
			    tmp *= tmp;
			    dx = tmpx - i + cl2;
			    dx = -dx;
			    dy = tmpy - j + cl2;
			    dy = -dy;
			    x2 += tmp * (cox * dx + siy * dy) * (cox * dx + siy * dy) * (cox * dx + siy * dy);
			  }
		}
		x2 = x2/magx;
		//if (skew != NULL) *skew = x2;
	}
	else return 1;	
	return 0;		
}




int	fill_odd_avg_profile(union pix *pd, int ytt, int cl, int cw, int *x, int *y)
{
	register int j, i;
	unsigned char *choi;	
	
	
	for (j = ((ytt+(cw>>1)) % 2) ? 0 : 1; j< cw; j+= 2)
/*	for (j = (ytt % 2) ? 0 : 1; j< cw; j+= 2)*/
	{	
		choi = pd[((cl-cw)>>1)+j].ch;
		for (i = 0; i < cl; i++)		x[i] += (int)choi[i];	
	}
/*	for (j = (ytt % 2) ? 1 : 0; j< cl; j+= 2)*/
	for (j = (ytt % 2) ? 0 : 1; j< cl; j+= 2)
	{	
		choi = pd[j].ch;
		for (i = (cl-cw)>>1; i < (cl+cw)>>1; i++)	
			y[j] += (int)choi[i];	
	}
	return 0;		
}
int	fill_even_avg_profile(union pix *pd, int ytt, int cl, int cw, int *x, int *y)
{
	register int j, i;
	unsigned char *choi;	
	

	for (j = ((ytt+(cw>>1)) % 2) ? 1 : 0; j< cw; j+= 2)
/*	for (j = (ytt % 2) ? 1 : 0; j< cw; j+= 2)*/
	{	
		choi = pd[((cl-cw)>>1)+j].ch;
		for (i = 0; i < cl; i++)		x[i] += (int)choi[i];	
	}
/*	for (j = (ytt % 2) ? 0 : 1; j< cl; j+= 2)*/
	for (j = (ytt % 2) ? 1 : 0; j< cl; j+= 2)
	{	
		choi = pd[j].ch;
		for (i = (cl-cw)>>1; i < (cl+cw)>>1; i++)	
			y[j] += (int)choi[i];	
	}
	return 0;		
}
int	fill_odd_avg_X_profile(union pix *pd, int ytt, int cl, int cw, int *x, int *y)
{
	register int j, i;
	
	for (i = 0; i < cl; i++)
	{
		for (j = ((ytt+i) % 2) ? 0 : 1; j< cw; j+= 2)
			x[i] += (int)pd[i+j].ch[i+cw-j-1];	
	}
	for (i = 0; i < cl; i++)
	{
		for (j = ((ytt+i) % 2) ? 0 : 1; j< cw; j+= 2)
			y[i] += (int)pd[cl+cw-i-j-2].ch[i+cw-j-1];	     
	}	
	return 0;	
}
int	fill_even_avg_X_profile(union pix *pd, int ytt, int cl, int cw, int *x, int *y)
{
	register int j, i;
	
	for (i = 0; i < cl; i++)
	{
		for (j = ((ytt+i) % 2) ? 1 : 0; j< cw; j+= 2)
			x[i] += (int)pd[i+j].ch[i+cw-j-1];	
	}
	for (i = 0; i < cl; i++)
	{
		for (j = ((ytt+i) % 2) ? 1 : 0; j< cw; j+= 2)
			y[i] += (int)pd[cl+cw-i-j-2].ch[i+cw-j-1];
	}		
	return 0;		
}

int	fill_X_avg_profiles_from_im(O_i *oi, int xc, int yc, int cl, int cw, int *x, int *y)
{
	register int j, i;
	int ytt, xll, onx, ony, xco, yco, cl2, cw2, d2;
	union pix *pd;
	
	if (oi == NULL || x == NULL || y == NULL)	return 1;
	onx = oi->im.nx;	ony = oi->im.ny;
	pd = oi->im.pixel;
	cl2 = cl >> 1;	cw2 = cw >> 1;
	d2 = cl2 + cw2 - 1;
	yco = (yc - d2 < 0) ? d2 : yc;
	yco = (yco + d2 <= ony) ? yco : ony - d2;
	xco = (xc - d2 < 0) ? d2 : xc;
	xco = (xco + d2 <= onx) ? xco : onx - d2;
	for(i = 0; i < cl; i++)		x[i] = y[i] = 0;	
	if (oi->im.data_type == IS_CHAR_IMAGE)
	{
	  ytt = yco - d2; xll = xco - d2; xll += cw - 1;
	  for (i = 0; i < cl; i++)
	    {
		for (j = 0; j< cw; j++)
			x[i] += (int)pd[ytt+i+j].ch[xll+i-j];	
	    }
	  ytt = yco - d2; ytt += cl + cw - 2; xll = xco - d2;; xll += cw - 1;
	  for (i = 0; i < cl; i++)
	    {
		for (j = 0; j< cw; j++)
			y[i] += (int)pd[ytt-i-j].ch[xll+i-j];
	    }		
	}
	else if (oi->im.data_type == IS_INT_IMAGE)
	{
	  ytt = yco - d2; xll = xco - d2; xll += cw - 1;
	  for (i = 0; i < cl; i++)
	    {
		for (j = 0; j< cw; j++)
			x[i] += (int)pd[ytt+i+j].in[xll+i-j];	
	    }
	  ytt = yco - d2; ytt += cl + cw - 2; xll = xco - d2;; xll += cw - 1;
	  for (i = 0; i < cl; i++)
	    {
		for (j = 0; j< cw; j++)
			y[i] += (int)pd[ytt-i-j].in[xll+i-j];
	    }		
	}
	else if (oi->im.data_type == IS_UINT_IMAGE)
	{
	  ytt = yco - d2; xll = xco - d2; xll += cw - 1;
	  for (i = 0; i < cl; i++)
	    {
		for (j = 0; j< cw; j++)
			x[i] += (int)pd[ytt+i+j].ui[xll+i-j];	
	    }
	  ytt = yco - d2; ytt += cl + cw - 2; xll = xco - d2;; xll += cw - 1;
	  for (i = 0; i < cl; i++)
	    {
		for (j = 0; j< cw; j++)
			y[i] += (int)pd[ytt-i-j].ui[xll+i-j];
	    }		
	}
	else return 1;	
	return 0;		
}

/*	take a radial image profile of an image with a different scaling in 
 *	x and y, real x = i_image * ax,  y = i_image * ay
 *
 *
 *
 */

float *radial_non_pixel_square_image_sym_profile_in_array(float *rz, O_i *ois, float xc, float yc, int r_max, float ax, float ay)
{
	int i, j;
	union pix *ps;
	int onx, ony, ri, type;
	float z = 0, z1, x, y, r, p, p0 = 0, pr0 = 0;
	int imin, imax, jmin,jmax; //  xci, yci

	for (i = 0; i < 2*r_max && i < 256; i++)	radial_dx[i] = 0;
	if (ois == NULL)	return NULL;
	if (rz == NULL)		rz = (float*)calloc(2*r_max,sizeof(float));
	if (rz == NULL)		return NULL;
	ps = ois->im.pixel;
	ony = ois->im.ny;	onx = ois->im.nx;
	type = ois->im.data_type;
	for (i = 0; i < 2*r_max; i++)	rz[i] = 0;
	//xci = (int)xc;
	//yci = (int)yc;
/*	imin = (int)(yc - (ay * r_max/3));
	imax = (int)(0.5 + yc + (r_max/(3*ay)));*/
/*	
	imin = (int)(yc - (r_max/(3*ay)));
	imax = (int)(0.5 + yc + (r_max/(3*ay)));*/
	imin = (int)(yc - (r_max/ay));
	imax = (int)(0.5 + yc + (r_max/ay));	
	jmin = (int)(xc -  r_max/ax);
	jmax = (int)(0.5 + xc + r_max/ax);
	imin = (imin < 0) ? 0 : imin;
	imax = (imax > ony) ? ony : imax;
	jmin = (jmin < 0) ? 0 : jmin;
	jmax = (jmax > onx) ? onx : jmax;	
	if (type == IS_COMPLEX_IMAGE)
	{  
	  for (i = imin; i < imax; i++)
	    {
		y = ay * ((float)i - yc);
		y *= y;
		for (j = jmin; j < jmax; j++)
		{
			x = ax * ((float)j - xc);
			x *= x;
			r = (float)sqrt(x + y);
			ri = (int)r;
			if (ri > r_max)	continue;	/* bug break;*/
			if ( ois->im.mode == RE)
			  z = ps[i].fl[2*j];
			else if ( ois->im.mode == IM)
			  z = ps[i].fl[2*j+1];
			else if ( ois->im.mode == AMP)
			{
				z = ps[i].fl[2*j];
				z1 = ps[i].fl[2*j+1];
				z = sqrt(z*z + z1*z1);
			}
			else if ( ois->im.mode == AMP_2)
			{
				z = ps[i].fl[2*j];
				z1 = ps[i].fl[2*j+1];
				z = z*z + z1*z1;
			}				
			else if ( ois->im.mode == LOG_AMP)
			{
				z = ps[i].fl[2*j];
				z1 = ps[i].fl[2*j+1];
				z = z*z + z1*z1;
				z = (z > 0) ? log10(z) : -40.0;
			}				
			else
			{
				win_printf("Unknown mode for complex image");
				return NULL;
			}
			p = r - ri;
			if (ri == r_max)
			{
				p0 += (1-p)*z;
				pr0 += (1-p);
				radial_dx[r_max] -= 1-p;
			}
			else 
			{
				rz[ri] += (1-p)*z;
				rz[ri+r_max] += (1-p);
				radial_dx[ri] += p;
			}
			ri++;
			if (ri < r_max)
			{
				rz[ri] += p*z;
				rz[ri+r_max] += p;
				radial_dx[ri] -= 1-p;
			}
		}
	    }
	}
	else if (type == IS_FLOAT_IMAGE) 		
	{
	  for (i = imin; i < imax; i++)
	    {
		y = ay * ((float)i - yc);
		y *= y;
		for (j = jmin; j < jmax; j++)
		{
			x = ax * ((float)j - xc);
			x *= x;
			r = (float)sqrt(x + y);
			ri = (int)r;
			if (ri > r_max)	continue;	/* bug break;*/
			z = ps[i].fl[j];
			p = r - ri;
			if (ri == r_max)
			{
				p0 += (1-p)*z;
				pr0 += (1-p);
				radial_dx[r_max] -= 1-p;
			}
			else 
			{
				rz[ri] += (1-p)*z;
				rz[ri+r_max] += (1-p);
				radial_dx[ri] += p;
			}
			ri++;
			if (ri < r_max)
			{
				rz[ri] += p*z;
				rz[ri+r_max] += p;
				radial_dx[ri] -= 1-p;
			}
		}
	    }
	}
	else if (type == IS_INT_IMAGE)			
	{
	  for (i = imin; i < imax; i++)
	    {
		y = ay * ((float)i - yc);
		y *= y;
		for (j = jmin; j < jmax; j++)
		{
			x = ax * ((float)j - xc);
			x *= x;
			r = (float)sqrt(x + y);
			ri = (int)r;
			if (ri > r_max)	continue;	/* bug break;*/
			z = (float)ps[i].in[j];
			p = r - ri;
			if (ri == r_max)
			{
				p0 += (1-p)*z;
				pr0 += (1-p);
				radial_dx[r_max] -= 1-p;
			}
			else 
			{
				rz[ri] += (1-p)*z;
				rz[ri+r_max] += (1-p);
				radial_dx[ri] += p;
			}
			ri++;
			if (ri < r_max)
			{
				rz[ri] += p*z;
				rz[ri+r_max] += p;
				radial_dx[ri] -= 1-p;
			}
		}
	    }
	}
	else if (type == IS_UINT_IMAGE)			
	{
	  for (i = imin; i < imax; i++)
	    {
		y = ay * ((float)i - yc);
		y *= y;
		for (j = jmin; j < jmax; j++)
		{
			x = ax * ((float)j - xc);
			x *= x;
			r = (float)sqrt(x + y);
			ri = (int)r;
			if (ri > r_max)	continue;	/* bug break;*/
			z = (float)ps[i].ui[j];
			p = r - ri;
			if (ri == r_max)
			{
				p0 += (1-p)*z;
				pr0 += (1-p);
				radial_dx[r_max] -= 1-p;
			}
			else 
			{
				rz[ri] += (1-p)*z;
				rz[ri+r_max] += (1-p);
				radial_dx[ri] += p;
			}
			ri++;
			if (ri < r_max)
			{
				rz[ri] += p*z;
				rz[ri+r_max] += p;
				radial_dx[ri] -= 1-p;
			}
		}
	    }
	}
	else if (type == IS_CHAR_IMAGE)			
	{
	  for (i = imin; i < imax; i++)
	    {
		y = ay * ((float)i - yc);
		y *= y;
		for (j = jmin; j < jmax; j++)
		{
			x = ax * ((float)j - xc);
			x *= x;
			r = (float)sqrt(x + y);
			ri = (int)r;
			if (ri > r_max)	continue;	/* bug break;*/
			z = (float)ps[i].ch[j];
			p = r - ri;
			if (ri == r_max)
			{
				p0 += (1-p)*z;
				pr0 += (1-p);
				radial_dx[r_max] -= 1-p;
			}
			else 
			{
				rz[ri] += (1-p)*z;
				rz[ri+r_max] += (1-p);
				radial_dx[ri] += p;
			}
			ri++;
			if (ri < r_max)
			{
				rz[ri] += p*z;
				rz[ri+r_max] += p;
				radial_dx[ri] -= 1-p;
			}
		}
	    }
	}
	for (i = 0; i < r_max; i++)
	{
		rz[i] = (rz[i+r_max] == 0) ? 0 : rz[i]/rz[i+r_max];
		radial_dx[i] = (rz[i+r_max] == 0) ? 0 : radial_dx[i]/rz[i+r_max];
	}
	p0 = (pr0 == 0) ? 0 : p0/pr0;  
	for (i = 0; i < r_max; i++)		rz[i+r_max] = rz[i]; 
	for (i = 1; i < r_max; i++)		rz[r_max-i] = rz[r_max+i]; 
	rz[0] = p0;
	return rz;
}

/*	take a radial image profile of an image with a different scaling in 
 *	x and y, real x = i_image * ax,  y = i_image * ay
 *
 *
 *
 */

float *radial_non_pixel_square_image_sym_profile_in_array_sse(float *rz, O_i *ois, float xc, float yc, int r_max, float ax, float ay)
{
  int i, j; //, debug = OK;
	union pix *ps;
	int onx, ony, ri, type, onx_4;
	float z = 0, *r, p, p0 = 0, pr0 = 0, rmax2, tmp;// , z1, x, y
	int imin, imax, jmin, jmax, jmin_4, jmax_4;//  xci, yci



	for (i = 0; i < 2*r_max && i < 256; i++)	radial_dx[i] = 0;
	if (ois == NULL)	return NULL;
	if (rz == NULL)		rz = (float*)calloc(2*r_max,sizeof(float));
	if (rz == NULL)		return NULL;
	ps = ois->im.pixel;
	ony = ois->im.ny;	onx = ois->im.nx;
	onx_4 = onx >> 2;
	type = ois->im.data_type;
	for (i = 0; i < 2*r_max; i++)	rz[i] = 0;
	rmax2 = r_max * r_max;
	//xci = (int)xc;
	//yci = (int)yc;
	imin = (int)(yc - (r_max/ay));
	imax = (int)(0.5 + yc + (r_max/ay));	
	jmin = (int)(xc -  r_max/ax);
	jmax = (int)(0.5 + xc + r_max/ax);
	imin = (imin < 0) ? 0 : imin;
	imax = (imax > ony) ? ony : imax;
	jmin = (jmin < 0) ? 0 : jmin;
	jmax = (jmax > onx) ? onx : jmax;
	//	jmin_4 = jmin = jmin >> 2;	jmin <<= 2;
	//jmax_4 = jmax = jmax >> 2;	jmax++; jmax <<= 2;
	jmin_4 = jmin >> 2;
	jmax_4 = 1+(jmax >> 2);
	jmax_4 = (jmax_4 > onx_4) ? onx_4 : jmax_4;


	/*win_printf("Imin %d imax %d, jmin %d jmax %d\njmin_4 %d jmax_4 %d"
	  ,imin,imax,jmin,jmax,jmin_4,jmax_4); */

	fxc.f[0] = fxc.f[1] = fxc.f[2] = fxc.f[3] = xc;
	fyc.f[0] = fyc.f[1] = fyc.f[2] = fyc.f[3] = yc;
	fax.f[0] = fax.f[1] = fax.f[2] = fax.f[3] = ax;
	fay.f[0] = fay.f[1] = fay.f[2] = fay.f[3] = ay;
	f4.f[0] = f4.f[1] = f4.f[2] = f4.f[3] = 4;
	r = fr[0].f;
	if (type == IS_INT_IMAGE)			
	{
	  f0.f[0] = 0; f0.f[1] = 1; f0.f[2] = 2; f0.f[3] = 3;  // pixel index 
	  fx.v = __builtin_ia32_subps(f0.v, fxc.v);            // distance in pixel from center
	  fx.v = __builtin_ia32_mulps (fax.v, fx.v);           // rescaling in x
	  f4.v = __builtin_ia32_mulps (fax.v, f4.v);           // we prepare rescaled substraction
	  for (j = 0; j < onx_4; j++)
	    {
	      fx2[j].v = __builtin_ia32_mulps (fx.v, fx.v);    // we compute X2
	      fx.v = __builtin_ia32_addps(fx.v, f4.v);         // we move index
	    }
    	  f0.f[0] = f0.f[1] = f0.f[2] = f0.f[3] = imin;        // starting y position
	  fy.v = __builtin_ia32_subps(f0.v, fyc.v);            // distance in pixel to yc
	  fy.v = __builtin_ia32_mulps (fay.v, fy.v);           // rescaled in y


	  for (i = imin; i < imax; i++)
	    {
	      fy2.v = __builtin_ia32_mulps (fy.v, fy.v);       // we compute y2 
	      fy.v = __builtin_ia32_addps(fy.v, fay.v);        // move by one rescaled pixel in y
	      tmp = rmax2 - fy2.f[0];
	      if (tmp < 0) continue;
	      tmp = sqrt(tmp);
	      tmp = (tmp+1)/ax;
	      jmin = (int)(xc - tmp);
	      jmax = (int)(xc + tmp + 1);
	      jmin = (jmin < 0) ? 0 : jmin;
	      jmax = (jmax > onx) ? onx : jmax;			
	      jmin_4 = jmin >> 2;
	      jmax_4 = 1+(jmax >> 2);
	      jmax_4 = (jmax_4 > onx_4) ? onx_4 : jmax_4;
	      for (j = jmin_4; j < jmax_4; j++)
		{
		  fr[j].v = __builtin_ia32_addps(fy2.v, fx2[j].v);  // we compute x2 + y2
		  fr[j].v = __builtin_ia32_sqrtps(fr[j].v);         // we get the distance
		}
	      for (j = jmin; j < jmax; j++)
		{
		  ri = (int)r[j];
		  if (ri > r_max)	continue;	/* bug break;*/
		  z = (float)ps[i].in[j];
		  p = r[j] - ri;
		  if (ri == r_max)
		    {
		      p0 += (1-p)*z;
		      pr0 += (1-p);
		      radial_dx[r_max] -= 1-p;
		    }
		  else 
		    {
		      rz[ri] += (1-p)*z;
		      rz[ri+r_max] += (1-p);
		      radial_dx[ri] += p;
		    }
		  ri++;
		  if (ri < r_max)
		    {
		      rz[ri] += p*z;
		      rz[ri+r_max] += p;
		      radial_dx[ri] -= 1-p;
		    }
		}
	    }
	}
	else if (type == IS_CHAR_IMAGE)			
	{
	  f0.f[0] = 0; f0.f[1] = 1; f0.f[2] = 2; f0.f[3] = 3;  // pixel index 
	  fx.v = __builtin_ia32_subps(f0.v, fxc.v);            // distance in pixel from center
	  fx.v = __builtin_ia32_mulps (fax.v, fx.v);           // rescaling in x
	  f4.v = __builtin_ia32_mulps (fax.v, f4.v);           // we prepare rescaled substraction
	  for (j = 0; j < onx_4; j++)
	    {
	      fx2[j].v = __builtin_ia32_mulps (fx.v, fx.v);    // we compute X2
	      fx.v = __builtin_ia32_addps(fx.v, f4.v);         // we move index
	    }
    	  f0.f[0] = f0.f[1] = f0.f[2] = f0.f[3] = imin;        // starting y position
	  fy.v = __builtin_ia32_subps(f0.v, fyc.v);            // distance in pixel to yc
	  fy.v = __builtin_ia32_mulps (fay.v, fy.v);           // rescaled in y

	  for (i = imin; i < imax; i++)
	    {
	      fy2.v = __builtin_ia32_mulps (fy.v, fy.v);       // we compute y2 
	      fy.v = __builtin_ia32_addps(fy.v, fay.v);        // move by one rescaled pixel in y
	      tmp = rmax2 - fy2.f[0];
	      if (tmp < 0) continue;
	      tmp = sqrt(tmp);
	      tmp = (tmp+1)/ax;
	      jmin = (int)(xc - tmp);
	      jmax = (int)(xc + tmp + 1);
	      jmin = (jmin < 0) ? 0 : jmin;
	      jmax = (jmax > onx) ? onx : jmax;			
	      jmin_4 = jmin >> 2;
	      jmax_4 = 1+(jmax >> 2);
	      jmax_4 = (jmax_4 > onx_4) ? onx_4 : jmax_4;
	      for (j = jmin_4; j < jmax_4; j++)
		{
		  fr[j].v = __builtin_ia32_addps(fy2.v, fx2[j].v);  // we compute x2 + y2
		  fr[j].v = __builtin_ia32_sqrtps(fr[j].v);         // we get the distance
		  /*
		    if ((debug != WIN_CANCEL) && (j == (onx >> 3)) && (i%16 == 0))
 		    debug = win_printf("y2 = %g i = %d j %d\n%g %g %g %g\n%g %g %g %g",
		    fy2.f[0],i,j,fr[j].f[0],fr[j].f[1],fr[j].f[2],fr[j].f[3],
		 		       r[4*j],r[(4*j)+1],r[(4*j)+2],r[(4*j)+3]);
		  */
		}

		for (j = jmin; j < jmax; j++)
		  {
			ri = (int)r[j];
			if (ri > r_max)	continue;	/* bug break;*/
			z = (float)ps[i].ch[j];
			p = r[j] - ri;
			if (ri == r_max)
			{
				p0 += (1-p)*z;
				pr0 += (1-p);
				radial_dx[r_max] -= 1-p;
			}
			else 
			{
				rz[ri] += (1-p)*z;
				rz[ri+r_max] += (1-p);
				radial_dx[ri] += p;
			}
			ri++;
			if (ri < r_max)
			{
				rz[ri] += p*z;
				rz[ri+r_max] += p;
				radial_dx[ri] -= 1-p;
			}
		}
	    }
	}
	for (i = 0; i < r_max; i++)
	{
		rz[i] = (rz[i+r_max] == 0) ? 0 : rz[i]/rz[i+r_max];
		radial_dx[i] = (rz[i+r_max] == 0) ? 0 : radial_dx[i]/rz[i+r_max];
	}
	p0 = (pr0 == 0) ? 0 : p0/pr0;  
	for (i = 0; i < r_max; i++)		rz[i+r_max] = rz[i]; 
	for (i = 1; i < r_max; i++)		rz[r_max-i] = rz[r_max+i]; 
	rz[0] = p0;
	return rz;
}


float *orthoradial_non_pixel_square_image_sym_profile_in_array_interpol(float *orz, 
       O_i *ois, float xc, float yc, int r_mean, int orz_size, float ax, float ay)
{
	int i;
	int type;
	float z, x, y, co, si, ax_1, ay_1;
	float theta;


	if (ois == NULL || orz_size == 0)	return NULL;
	if (orz == NULL)	orz = (float*)calloc(orz_size,sizeof(float));
	if (orz == NULL)	return NULL;
	type = ois->im.data_type;
	//for (i = 0; i < orz_size; i++)	orz[i] = 0;
	ay_1 = (ay != 0) ? (float)1/ay : 1;
	ax_1 = (ax != 0) ? (float)1/ax : 1;

	for (i = 0; i < orz_size; i++)
	  {
	    theta = (2 * M_PI * i)/orz_size;
	    co = cos(theta);
	    si = sin(theta);
	    x = xc + (r_mean * co)*ay_1;
	    y = yc + (r_mean * si)*ax_1;
	    if (type == IS_CHAR_IMAGE)			
	    z = ((float)fast_interpolate_image_point (ois, x, y, 0))/256;
	    else    z = interpolate_image_point (ois, x, y, &z, NULL);	    
	    orz[i] += z;
	  }
	return orz;
}

float *orthoradial_non_pixel_square_image_sym_profile_in_array_interpol_avg(float *orz, 
       O_i *ois, float xc, float yc, int r_mean, int r_width, int orz_size, float ax, float ay)
{
  register int i;
  for (i = 0; i < orz_size; i++)	orz[i] = 0;
  for (i = r_mean - r_width; i <= r_mean + r_width; i++)	
    orthoradial_non_pixel_square_image_sym_profile_in_array_interpol(orz, ois, xc, yc, i, orz_size, ax, ay);
  return orz;
}
float *orthoradial_non_pixel_square_image_sym_profile_in_array(float *orz, 
       O_i *ois, float xc, float yc, int r_mean, int r_width, int orz_size, float ax, float ay)
{
	int i, j;
	union pix *ps;
	static int last_size = 0;
	int onx, ony, ori, type;
	float z = 0, z1, x, y, x2, y2, r2, p;
	float r2f_max, r2f_min, theta, *ora = NULL;
	int imin, imax, jmin, jmax, r_max, r_min, nx2; //  xci, yci


	if (ois == NULL || orz_size == 0)	return NULL;
	if (last_size != orz_size)
	  {
	    if (ora) free(ora);
	    ora = NULL;
	  }
	if (ora == NULL)	ora = (float*)calloc(last_size=orz_size,sizeof(float));
	if (ora == NULL)	return NULL;
	if (orz == NULL)	orz = (float*)calloc(orz_size,sizeof(float));
	if (orz == NULL)	return NULL;
	ps = ois->im.pixel;
	ony = ois->im.ny;	onx = ois->im.nx;
	type = ois->im.data_type;
	for (i = 0; i < orz_size; i++)	orz[i] = ora[i] = 0;
	nx2 = orz_size/2;
	//xci = (int)xc;
	//yci = (int)yc;
	r_max = r_mean + r_width;
	r2f_max = (float)r_max * (float)r_max;
	r_min = r_mean - r_width;
	r2f_min = (float)r_min * (float)r_min;

	imin = (int)(yc - (r_max/ay));
	imax = (int)(0.5 + yc + (r_max/ay));	
	jmin = (int)(xc -  r_max/ax);
	jmax = (int)(0.5 + xc + r_max/ax);
	imin = (imin < 0) ? 0 : imin;
	imax = (imax > ony) ? ony : imax;
	jmin = (jmin < 0) ? 0 : jmin;
	jmax = (jmax > onx) ? onx : jmax;
	
	if (type == IS_COMPLEX_IMAGE)
	{  
	  for (i = imin; i < imax; i++)
	    {
		y = ay * ((float)i - yc);
		y2 = y * y;
		for (j = jmin; j < jmax; j++)
		{
			x = ax * ((float)j - xc);
			x2 = x * x;
			r2 = x2 + y2;
			if (r2 > r2f_max || r2 < r2f_min)   continue;
			theta = atan2(y,x);
			theta /= M_PI;
			theta *= nx2;
			theta += nx2;
			ori = (int)theta;
			if ( ois->im.mode == RE)
			  z = ps[i].fl[2*j];
			else if ( ois->im.mode == IM)
			  z = ps[i].fl[2*j+1];
			else if ( ois->im.mode == AMP)
			{
				z = ps[i].fl[2*j];
				z1 = ps[i].fl[2*j+1];
				z = sqrt(z*z + z1*z1);
			}
			else if ( ois->im.mode == AMP_2)
			{
				z = ps[i].fl[2*j];
				z1 = ps[i].fl[2*j+1];
				z = z*z + z1*z1;
			}				
			else if ( ois->im.mode == LOG_AMP)
			{
				z = ps[i].fl[2*j];
				z1 = ps[i].fl[2*j+1];
				z = z*z + z1*z1;
				z = (z > 0) ? log10(z) : -40.0;
			}				
			else
			{
				win_printf("Unknown mode for complex image");
				return NULL;
			}
			p = theta - ori;
			if (ori < 0  ) win_printf("index < 0");
			ori %= orz_size;
			if (ori >= orz_size ) win_printf("index out of range");
			orz[ori] += (1-p)*z;
			ora[ori] += (1-p);
			ori++;
			ori %= orz_size;
 			orz[ori] += p*z;
			ora[ori] += p;
		}
	    }
	}
	else if (type == IS_FLOAT_IMAGE) 		
	{
	  for (i = imin; i < imax; i++)
	    {
		y = ay * ((float)i - yc);
		y2 = y * y;
		for (j = jmin; j < jmax; j++)
		{
			x = ax * ((float)j - xc);
			x2 = x * x;
			r2 = x2 + y2;
			if (r2 > r2f_max || r2 < r2f_min)   continue;
			theta = atan2(y,x);
			theta /= M_PI;
			theta *= nx2;
			theta += nx2;
			ori = (int)theta;
			z = ps[i].fl[j];
			p = theta - ori;
			if (ori < 0  ) win_printf("index < 0");
			ori %= orz_size;
			if (ori >= orz_size ) win_printf("index out of range");
			orz[ori] += (1-p)*z;
			ora[ori] += (1-p);
			ori++;
			ori %= orz_size;
 			orz[ori] += p*z;
			ora[ori] += p;
		}
	    }
	}
	else if (type == IS_INT_IMAGE)			
	{
	  for (i = imin; i < imax; i++)
	    {
		y = ay * ((float)i - yc);
		y2 = y * y;
		for (j = jmin; j < jmax; j++)
		{
			x = ax * ((float)j - xc);
			x2 = x * x;
			r2 = x2 + y2;
			if (r2 > r2f_max || r2 < r2f_min)   continue;
			theta = atan2(y,x);
			theta /= M_PI;
			theta *= nx2;
			theta += nx2;
			ori = (int)theta;
			z = (float)ps[i].in[j];
			p = theta - ori;
			if (ori < 0  ) win_printf("index < 0");
			ori %= orz_size;
			if (ori >= orz_size ) win_printf("index out of range");
			orz[ori] += (1-p)*z;
			ora[ori] += (1-p);
			ori++;
			ori %= orz_size;
 			orz[ori] += p*z;
			ora[ori] += p;
		}
	    }
	}
	else if (type == IS_CHAR_IMAGE)			
	{
	  for (i = imin; i < imax; i++)
	    {
		y = ay * ((float)i - yc);
		y2 = y * y;
		for (j = jmin; j < jmax; j++)
		{
			x = ax * ((float)j - xc);
			x2 = x * x;
			r2 = x2 + y2;
			if (r2 > r2f_max || r2 < r2f_min)   continue;
			theta = atan2(y,x);
			//			win_printf("X %g Y %g \n\\theta  = %g -> %d",x,y,theta,(int)((orz_size*(theta+M_PI))/(2*M_PI))); 
			theta /= M_PI;
			theta *= nx2;
			theta += nx2;
			ori = (int)theta;
			z = (float)ps[i].ch[j];
			p = theta - ori;
			if (ori < 0  ) win_printf("index < 0");
			ori %= orz_size;
			if (ori >= orz_size ) win_printf("index out of range");
			orz[ori] += (1-p)*z;
			ora[ori] += (1-p);
			ori++;
			ori %= orz_size;
 			orz[ori] += p*z;
			ora[ori] += p;
		}
	    }
	}
	for (i = 0; i < orz_size; i++)
	{
		orz[i] = (ora[i] == 0) ? 0 : orz[i]/ora[i];
	}
	return orz;
}



# ifdef PREVIOUS
float *radial_non_pixel_square_image_sym_profile_in_array(float *rz, O_i *ois, float xc, float yc, int r_max, float ax, float ay)
{
	int i, j;
	union pix *ps;
	int onx, ony, ri, type;
	float z = 0, z1, x, y, r, p, p0 = 0, pr0 = 0;
	int xci, yci, imin, imax, jmin,jmax;

	for (i = 0; i < 2*r_max && i < 256; i++)	radial_dx[i] = 0;
	if (ois == NULL)	return NULL;
	if (rz == NULL)		rz = (float*)calloc(2*r_max,sizeof(float));
	if (rz == NULL)		return NULL;
	ps = ois->im.pixel;
	ony = ois->im.ny;	onx = ois->im.nx;
	type = ois->im.data_type;
	for (i = 0; i < 2*r_max; i++)	rz[i] = 0;
	xci = (int)xc;
	yci = (int)yc;
/*	imin = (int)(yc - (ay * r_max/3));
	imax = (int)(0.5 + yc + (r_max/(3*ay)));*/
/*	
	imin = (int)(yc - (r_max/(3*ay)));
	imax = (int)(0.5 + yc + (r_max/(3*ay)));*/
	imin = (int)(yc - (r_max/ay));
	imax = (int)(0.5 + yc + (r_max/ay));	
	jmin = (int)(xc -  r_max/ax);
	jmax = (int)(0.5 + xc + r_max/ax);
	imin = (imin < 0) ? 0 : imin;
	imax = (imax > ony) ? ony : imax;
	jmin = (jmin < 0) ? 0 : jmin;
	jmax = (jmax > onx) ? onx : jmax;	
	for (i = imin; i < imax; i++)
	{
		y = ay * ((float)i - yc);
		y *= y;
		for (j = jmin; j < jmax; j++)
		{
			x = ax * ((float)j - xc);
			x *= x;
			r = (float)sqrt(x + y);
			ri = (int)r;
			if (ri > r_max)	continue;	/* bug break;*/
			if (type == IS_COMPLEX_IMAGE)
			{
				if ( ois->im.mode == RE)
				  z = ps[i].fl[2*j];
				else if ( ois->im.mode == IM)
				  z = ps[i].fl[2*j+1];
				else if ( ois->im.mode == AMP)
				{
					z = ps[i].fl[2*j];
					z1 = ps[i].fl[2*j+1];
					z = sqrt(z*z + z1*z1);
				}
				else if ( ois->im.mode == AMP_2)
				{
					z = ps[i].fl[2*j];
					z1 = ps[i].fl[2*j+1];
					z = z*z + z1*z1;
				}				
				else if ( ois->im.mode == LOG_AMP)
				{
					z = ps[i].fl[2*j];
					z1 = ps[i].fl[2*j+1];
					z = z*z + z1*z1;
					z = (z > 0) ? log10(z) : -40.0;
				}				
				else
				{
					win_printf("Unknown mode for complex image");
					return NULL;
				}
			}
			else if (type == IS_FLOAT_IMAGE) 		
			  z = ps[i].fl[j];
			else if (type == IS_INT_IMAGE)			
			  z = (float)ps[i].in[j];
			else if (type == IS_CHAR_IMAGE)			
			  z = (float)ps[i].ch[j];
			p = r - ri;
			if (ri == r_max)
			{
				p0 += (1-p)*z;
				pr0 += (1-p);
				radial_dx[r_max] -= 1-p;
			}
			else 
			{
				rz[ri] += (1-p)*z;
				rz[ri+r_max] += (1-p);
				radial_dx[ri] += p;
			}
			ri++;
			if (ri < r_max)
			{
				rz[ri] += p*z;
				rz[ri+r_max] += p;
				radial_dx[ri] -= 1-p;
			}
		}
	}	
	for (i = 0; i < r_max; i++)
	{
		rz[i] = (rz[i+r_max] == 0) ? 0 : rz[i]/rz[i+r_max];
		radial_dx[i] = (rz[i+r_max] == 0) ? 0 : radial_dx[i]/rz[i+r_max];
	}
	p0 = (pr0 == 0) ? 0 : p0/pr0;  
	for (i = 0; i < r_max; i++)		rz[i+r_max] = rz[i]; 
	for (i = 1; i < r_max; i++)		rz[r_max-i] = rz[r_max+i]; 
	rz[0] = p0;
	return rz;
}

# endif

float *radial_non_pixel_square_image_sym_profile_in_array_fixed(float *rz, O_i *ois, float xc, float yc, int r_max, float ax, float ay)
{
	int i, j;
	union pix *ps;
	int onx, ony, ri, type;
	int imin, imax, jmin,jmax;// xci , yci
	fixed rf, axf, ayf, xcf, ycf, xf, yf, xf2, yf2, tmpf; // , rmf
	int z, p, p0 = 0, pr0 = 0, *rzf, *rzf2, pm;
	unsigned char *choi;
	float tmp;

	if (ois == NULL)	return NULL;
	if (rz == NULL)		rz = (float*)calloc(2*r_max,sizeof(float));
	if (rz == NULL)		return NULL;
	ps = ois->im.pixel;
	rzf = (int*)rz;
	rzf2 = rzf + r_max;
	ony = ois->im.ny;	onx = ois->im.nx;
	type = ois->im.data_type;
	if (type != IS_CHAR_IMAGE)	
	{
		win_printf("treating unsigneg char only!");
		return NULL;
	}
	for (i = 0; i < 2*r_max; i++)	rzf[i] = 0;
	axf = ftofix(ax);	ayf = ftofix(ay);	
	xcf = ftofix(xc); 	ycf = ftofix(yc);
	//rmf = itofix(r_max);
	yc = ony - yc;
	//xci = (int)(xc+0.5);
	//yci = (int)(yc+0.5);
	tmp =  r_max + 1;
	tmp /= (ay!=0)?ay:1;
	imin = (int)(yc - tmp);
	imax = (int)(1 + yc + tmp);
	imin = (imin < 0) ? 0 : imin;
	imax = (imax > ony) ? ony : imax;
	for (i = imin; i < imax; i++)
	{
		tmpf = 	ycf - itofix(i);
		yf = fmul(ayf, tmpf);
		yf2 = fmul(yf,yf);
		tmpf = itofix(r_max + 1);	
		rf = fmul(tmpf, tmpf);
		rf -= yf2;
		if (rf < 0)		continue;
		rf = fsqrt(rf);
/*		tmpf = fmul(axf, itofix(1) + rf);*/
		tmpf = fdiv(itofix(1) + rf, axf);
		jmin = fixtoi(xcf - tmpf);
		jmax = 1 + fixtoi(xcf + tmpf);
		jmin = (jmin < 0) ? 0 : jmin;
		jmax = (jmax > onx) ? onx : jmax;			
		choi = ps[i].ch;
		for (j = jmin, xf = fmul(axf, itofix(j) - xcf); j < jmax; j++,xf+=axf)
		{
			xf2 = fmul(xf,xf);
			rf = fsqrt(xf2+yf2);		
/*			ri = fixtoi(rf);	*/
			ri = rf >> 16;
			if (ri > r_max)	continue;	/* bug break;*/
			z = choi[j];
			p = (int)(rf & 0x8000FFFF)>>8;
			pm = p*z;
			if (ri == r_max)
			{
				p0 += (z<<8) - pm;
				pr0 += 256-p;
			}
			else 
			{
				rzf[ri] += (z<<8) - pm;
				rzf2[ri] += 256-p;
			}
			ri++;
			if (ri < r_max)
			{
				rzf[ri] += pm;
				rzf2[ri] += p;
			}
		}
	}	
	for (i = 0; i < r_max; i++)
		rz[i] = (rzf2[i] == 0) ? 0 : ((float)rzf[i])/rzf2[i];
	rz[0] = (rz[0] == 0) ? rz[1] : rz[0];
	for (i = 0; i < r_max; i++)		rz[i+r_max] = rz[i]; 
	for (i = 1; i < r_max; i++)		rz[r_max-i] = rz[r_max+i]; 
	rz[0] = (pr0 == 0) ? 0 : (float)p0/pr0;  
	return rz;
}

int prepare_filtered_profile(float *zp, int nx, int flag, int filter, int width)
{
	register int j, i;
	float moy, re, im;

	(void)flag;
	/* remove dc based upon the first and the last quarter of data */
	/*	we assume a peak in the middle !*/	
	for(i = 0, j = (3*nx)/4, moy = 0; i < nx/4; i++, j++)		
		moy += zp[i] + zp[j];	
	for(i = 0, moy = (2*moy)/nx; i < nx; i++)		zp[i] -= moy;
		
	/* no windowing, profile must be symmetrical */
	realtr1(nx, zp);		fft(nx, zp, 1);	realtr2(nx, zp, 1);
	/* I take the amplitude of the cosine of the fist mode to normalize */
	moy = zp[2]; 
	if (moy == 0) 
	{
	  /*		activate_acreg(create_plot_from_1d_array("profile", nx, zpk));*/
		win_printf("pb of normalisation zp[2] %f zp[3] %f",  zp[2],zp[3]);
	}
	else {for (j=0, moy = ((float)1)/moy; j< nx ; j++)   zp[j] *= moy;}
	zp[0] = zp[1] = 0; 
	bandpass_smooth_half (nx, zp, filter, width);
	fft(nx, zp, -1);

	/* first halh of profile ampliture, second half re im */
	for (j=0 ; j< nx/2 ; j += 2)
	{
		re = zp[j];
		im = zp[j+1];
		zp[j] = sqrt(re*re+im*im);
		zp[j+1] = 0;
	}
	return 0;
}
int prepare_filtered_profile_with_rc(float *zp, int nx, int flag, int filter, int width, int rc)
{
	register int j, i;
	float moy, re, im;

	(void)rc;
	(void)flag;
	/* remove dc based upon the first and the last quarter of data */
	/*	we assume a peak in the middle !*/	
	for(i = 0, j = (3*nx)/4, moy = 0; i < nx/4; i++, j++)		
		moy += zp[i] + zp[j];	
	for(i = 0, moy = (2*moy)/nx; i < nx; i++)		zp[i] -= moy;
		
	/* no windowing, profile must be symmetrical */
	realtr1(nx, zp);		fft(nx, zp, 1);	realtr2(nx, zp, 1);
	/* I take the amplitude of the cosine of the fist mode to normalize */
	moy = zp[2]; 
	if (moy == 0) 
	{
	  /*		activate_acreg(create_plot_from_1d_array("profile", nx, zpk));*/
		win_printf("pb of normalisation zp[2] %f zp[3] %f",  zp[2],zp[3]);
	}
	else {for (j=0, moy = ((float)1)/moy; j< nx ; j++)   zp[j] *= moy;}
	zp[0] = zp[1] = 0; 
	bandpass_smooth_half (nx, zp, filter, width);
	fft(nx, zp, -1);

	/* first halh of profile ampliture, second half re im */
	for (j=0 ; j< nx/2 ; j += 2)
	{
		re = zp[j];
		im = zp[j+1];
		zp[j] = sqrt(re*re+im*im);
		zp[j+1] = 0;
	}
	return 0;
}

float 	find_simple_phase_shift_between_filtered_profile(float *zr, float *zp, int nx, int rc)
{
	register int i, j;
	float im, re, amp, phi, w, ph;
	
	if (zr == NULL || zp == NULL)	return WRONG_ARGUMENT;
	for (j = 2, i = nx - j, phi = 0, w = 0; j < nx/2 - rc; i -= 2, j += 2)
	{
		re = zr[i] * zp[i] + zp[i+1] * zr[i+1];
		im = zp[i+1] * zr[i] - zp[i] * zr[i+1];
		ph = (im == 0.0 && re == 0.0) ? 0.0 : atan2(im,re);
		amp = zr[j] * zp[j];
		w += amp;
		phi += amp*ph;
	}
	return  (w != 0.0) ? phi/w : phi;
}

float 	find_simple_phase_shift_between_filtered_profile_omar(float *zr, float *zp, int nx, int rc)
{
	register int i, j, k;
	float im, re, amp, phi, w, ph;
	
	if (zr == NULL || zp == NULL)	return WRONG_ARGUMENT;
	for (j = 2, i = nx - j, k = (nx/4) - 1, phi = 0, w = 0; j < nx/2 - rc; i -= 2, j += 2, k--)
	{
		re = zr[i] * zp[i] + zp[i+1] * zr[i+1];
		im = zp[i+1] * zr[i] - zp[i] * zr[i+1];
		ph = (im == 0.0 && re == 0.0) ? 0.0 : atan2(im,re);
		amp = k * k * zr[j] * zp[j];
		w += amp;
		phi += amp*ph;
	}
	return  (w != 0.0) ? phi/w : phi;
}

O_i	*image_band_pass_in_real_out_complex(O_i *dst, O_i *oi,int w_flag, int filter, int width, int rc)
{
	register int i;
	int nx, ny;
	union pix *ps;
	O_p *op = NULL;
	d_s *ds = NULL;

	
	if (oi == NULL || oi->im.data_type == IS_COMPLEX_IMAGE)		return NULL;
	ny = oi->im.ny;	nx = oi->im.nx;
	
	if (dst == NULL)	dst = create_one_image( nx/2,  ny, IS_COMPLEX_IMAGE);
	else if (dst->im.data_type != IS_COMPLEX_IMAGE || dst->im.nx != oi->im.nx/2
		|| dst->im.ny != oi->im.ny || dst->im.n_f != 1)
	{
		free_one_image(dst);
		dst = create_one_image( nx/2,  ny, IS_COMPLEX_IMAGE);
	}
	if (dst == NULL)	return NULL;
	ps = dst->im.pixel;
	for (i=0 ; i< ny ; i++)
	{
	        display_title_message("doing line %d",i);
		extract_raw_line (oi, i, ps[i].fl);
		prepare_filtered_profile(ps[i].fl, nx, w_flag, filter, width);
	}
	inherit_from_im_to_im(dst,oi);
	uns_oi_2_oi(dst,oi);	
	dst->im.win_flag = oi->im.win_flag;
	if (oi->x_title != NULL)	set_im_x_title(dst,oi->x_title);
	if (oi->y_title != NULL)	set_im_y_title(dst,oi->y_title);
	set_im_title(dst, "\\stack{{Bandpass filter image}{%s}"
	"{\\pt8 filter %d width %d flag %s}}",(oi->title != NULL) ? oi->title 
	: "untitled",filter,width,(w_flag)?"periodic":"not periodic");
	set_formated_string(&dst->im.treatement,"Bandpass filter image %s"
	"filter %d width %d flag %s",(oi->title != NULL) ? oi->title 
	: "untitled",filter,width,(w_flag)?"periodic":"not periodic");
	op = create_and_attach_op_to_oi(dst, ny, ny, 0,0);
	if (op == NULL)		return win_printf_ptr("cannot create plot!");
	ds = op->dat[0];
	op->type = IM_SAME_Y_AXIS;
	uns_oi_2_op(dst, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
	set_formated_string(&ds->treatement,"Simple phase difference from n and n-1 profile");
	for (i=0 ; i< ny-1 ; i++)
	{	
		ds->xd[i] = find_simple_phase_shift_between_filtered_profile(ps[i].fl, ps[i+1].fl, nx, rc);
		ds->yd[i] = (float)i;
	}
	return dst;
}

int fftwindow_flat_top1(int npts, float *x, float smp)
{
	register int i, j;
	int n_2, n_4;
	float tmp, *offtsin;
	
	offtsin = get_fftsin();

	if ((j = fft_init(npts)) != 0)
		return j;

	n_2 = npts/2;
	n_4 = n_2/2;
	x[0] *= smp;
	smp += 1.0;
	for (i=1, j=n_4-2; j >=0; i++, j-=2)
	{
		tmp = (smp - offtsin[j]);
		x[i] *= tmp;
		x[npts-i] *= tmp;
	}
	for (i=n_4/2, j=0; i < n_4 ; i++, j+=2)
	{
		tmp = (smp + offtsin[j]);
		x[i] *= tmp;
		x[npts-i] *= tmp;
	}
	return 0;
}
int deplace(float *x, int nx)
{
	register int i, j;
	float tmp;
	
	for (i = 0, j = nx/2; j < nx ; i++, j++)
	{
		tmp = x[j];
		x[j] = x[i];
		x[i] = tmp;
	}
	return 0;
}

/*		correlate "x1" with its invert over "nx" points, returns the correlation
 *		in "fx1". "fx1" will be used to compute fft and result 
 *		setting them to NULL will allocate space. "window" will enable
 *		a windowing process if != 0 a double windowing if = to 2.
 */
int	correlate_1d_sig_and_invert(float *x1, int nx, int window, float *fx1, int filter, int remove_dc)
{
	register int i, j;
	float moy, smp = 0.05;
	float m1, re1, re2, im1, im2;
	
	if (x1 == NULL)				return WRONG_ARGUMENT;
	if (fft_init(nx) || filter_init(nx))	return FFT_NOT_SUPPORTED;
	if (fx1 == NULL)	fx1 = (float*)calloc(nx,sizeof(float));
	if (fx1 == NULL)			return OUT_MEMORY;

	/*	compute fft of x1 */
	for(i = 0; i < nx; i++)		fx1[i] = x1[i];
	for(i = 0, j = (3*nx)/4, moy = 0; i < nx/4; i++, j++)		
		moy += fx1[i] + fx1[j];	
	for(i = 0, moy = (2*moy)/nx; remove_dc && i < nx; i++)	
		fx1[i] = x1[i] - moy;
	if (window == 1)	fftwindow1(nx, fx1, smp);
	else if (window == 2)	fftwindow_flat_top1(nx, fx1, smp);
	realtr1(nx, fx1);
	fft(nx, fx1, 1);
	realtr2(nx, fx1, 1);
	
	/*	compute normalization */
	for (i=0, m1 = 0; i< nx; i+=2)
		m1 += fx1[i] * fx1[i] + fx1[i+1] * fx1[i+1];
	m1 /= 2;	
		
	/*	compute correlation in Fourier space */
	for (i=2; i< nx; i+=2)
	{
		re1 = fx1[i];		re2 = fx1[i];
		im1 = fx1[i+1];		im2 = -fx1[i+1];
		fx1[i] 		= (re1 * re2 + im1 * im2)/m1;
		fx1[i+1] 	= (im1 * re2 - re1 * im2)/m1;
	}
	fx1[0]	= 2*(fx1[0] * fx1[0])/m1;	/* these too mode are special */
	fx1[1] 	= 2*(fx1[1] * fx1[1])/m1;
	if (filter> 0)		lowpass_smooth_half (nx, fx1, filter);
	
	/*	get back to real world */
	realtr2(nx, fx1, -1);
	fft(nx, fx1, -1);
	realtr1(nx, fx1);
	deplace(fx1, nx);
	return 0;
}	
int	correlate_1d_sig_and_invert_in_fourier(float *x1, int nx, int window, float *fx1, int filter, int remove_dc)
{
	register int i, j;
	float moy, smp = 0.05;
	float m1, re1, re2, im1, im2;
	
	if (x1 == NULL)		return WRONG_ARGUMENT;
	if (fft_init(nx) || filter_init(nx))	return FFT_NOT_SUPPORTED;
	if (fx1 == NULL)	fx1 = (float*)calloc(nx,sizeof(float));
	if (fx1 == NULL)	return OUT_MEMORY;

	/*	compute fft of x1 */
	for(i = 0; i < nx; i++)		fx1[i] = x1[i];
	for(i = 0, j = (3*nx)/4, moy = 0; i < nx/4; i++, j++)		
		moy += fx1[i] + fx1[j];	
	for(i = 0, moy = (2*moy)/nx; remove_dc && i < nx; i++)	
		fx1[i] = x1[i] - moy;
	if (window == 1)		fftwindow1(nx, fx1, smp);
	else if (window == 2)	fftwindow_flat_top1(nx, fx1, smp);
	realtr1(nx, fx1);
	fft(nx, fx1, 1);
	realtr2(nx, fx1, 1);
	
	/*	compute normalization */
	for (i=0, m1 = 0; i< nx; i+=2)
		m1 += fx1[i] * fx1[i] + fx1[i+1] * fx1[i+1];
	m1 /= 2;	
		
	/*	compute correlation in Fourier space */
	for (i=2; i< nx; i+=2)
	{
		re1 = fx1[i];		re2 = fx1[i];
		im1 = fx1[i+1];		im2 = -fx1[i+1];
		fx1[i] 		= (re1 * re2 + im1 * im2)/m1;
		fx1[i+1] 	= (im1 * re2 - re1 * im2)/m1;
	}
	fx1[0]	= 2*(fx1[0] * fx1[0])/m1;	/* these too mode are special */
	fx1[1] 	= 2*(fx1[1] * fx1[1])/m1;
	if (filter> 0)		lowpass_smooth_half (nx, fx1, filter);
	return 0;
}	

/*	Find maximum position in a 1d array, the array is supposed periodic
 *	return 0 -> everything fine, TOO_MUCH_NOISE or PB_WITH_EXTREMUM
 *
 */
int	find_max1(float *x, int nx, float *Max_pos, float *Max_val)
{
	register int i;
	int  nmax, ret = 0, delta;
	double a, b, c, d, f_2, f_1, ff0, f1, f2, xm = 0;

	for (i = 0, nmax = 0; i < nx; i++) nmax = (x[i] > x[nmax]) ? i : nmax;
	f_2 = x[(nx + nmax - 2) % nx];
	f_1 = x[(nx + nmax - 1) % nx];
	ff0 = x[nmax];
	f1 = x[(nmax + 1) % nx];
	f2 = x[(nmax + 2) % nx];
	if (f_1 < f1)
	{
		a = -f_1/6 + ff0/2 - f1/2 + f2/6;
		b = f_1/2 - ff0 + f1/2;
		c = -f_1/3 - ff0/2 + f1 - f2/6;
		d = ff0;
		delta = 0;
	}	
	else
	{
		a = b = c = 0;	
		a = -f_2/6 + f_1/2 - ff0/2 + f1/6;
		b = f_2/2 - f_1 + ff0/2;
		c = -f_2/3 - f_1/2 + ff0 - f1/6;
		d = f_1;
		delta = -1;
	}
	if (fabs(a) < 1e-8)
	{
		if (b != 0)	xm =  - c/(2*b) - (3 * a * c * c)/(4 * b * b * b);
		else
		{
			xm = 0;
			ret |= PB_WITH_EXTREMUM;
		}
	}
	else if ((b*b - 3*a*c) < 0)
	{
		ret |= PB_WITH_EXTREMUM;
		xm = 0;
	}
	else
		xm = (-b - sqrt(b*b - 3*a*c))/(3*a);
	/*
	for (p = -2; p < 1; p++)
	{
		if (6*a*p+b > 0) ret |= TOO_MUCH_NOISE;
	}	
	*/
	*Max_pos = (float)(xm + nmax + delta); 
	*Max_val = (a * xm * xm * xm) + (b * xm * xm) + (c * xm) + d;
	return ret;
}


float	find_distance_from_center(float *x1, int cl, int flag, int filter, int black_circle, float *corr)
{
	static float fx1[512];
	float dx;
	
	correlate_1d_sig_and_invert(x1, cl, flag, fx1, filter, !black_circle);
	find_max1(fx1, cl, &dx,corr);
	dx -= cl/2;
	dx /=2;
	return dx;
}
int coarse_find_z_by_least_square_min(float *yt, int nxp, O_i *ois, float *min, int rc)
{
	register int i, j, k;
	int nx, ny;
	union pix *ps;
	float  tmp, *yr, val;
	
	if (ois->im.data_type != IS_COMPLEX_IMAGE)
		return win_printf_OK("image must be complex!");
	ny = ois->im.ny;	nx = ois->im.nx;
	if (2*nx != nxp)			return -1;
	ps = ois->im.pixel;
	for (i = k = 0; i< ny ; i++)
	{
		yr = ps[i].fl;
		/* first attemp pb !
		for (j = 0, val = 0; j < nx - rc/2; j +=2) 
		{
			tmp = yr[j] -  yt[j];
			val += tmp * tmp;
		}
		*/
		for (j = nx + rc, val = 0; j < nxp; j++) 
		{
			tmp = yr[j] -  yt[j];
			val += tmp * tmp;
		}
		if (i == 0 || val < *min)
		{
			k = i;
			*min = val;
		}
	}
	*min /= ((nx-rc)/2);
	return k;
}
float find_zero_of_3_points_polynome(float y_1, float y0, float y1)
{
	double a, b, c, x = 0, delta;
	
	a = ((y_1+y1)/2) - y0;
	b = (y1-y_1)/2;
	c = y0;
	delta = b*b - 4*a*c;
	if (a == 0)
	{
		x = (b != 0) ? -c/b : 0;
	}
	else if (delta >=0)
	{
		delta = sqrt(delta);
		x = (fabs(-b + delta) < fabs(-b - delta)) ? -b + delta : -b - delta ;
		x /= 2*a; 
	}
	return (float)x;
}

int find_z_profile_by_mean_square_and_phase(float *zp, int nx, O_i *oir, int w_flag, int filter, int width, int rc, float *z)
{
	register int k, j;
	int er = 0, ny;
	static int oldnx = 0;
	static float *yt = NULL;
	float min, tmp, ph_1, phi, ph1;
	union pix *psd;
	O_p *opz;
	d_s *dsrz;
	
	if (yt == NULL || oldnx != nx)
	{
		yt = (float*)realloc(yt,nx*sizeof(float));
		if (yt == NULL)		return -1;
		oldnx = nx;
	}
	psd = oir->im.pixel;
	ny = oir->im.ny;
	opz = (oir->n_op != 0) ? oir->o_p[0] : NULL;
	dsrz = (opz != NULL) ? opz->dat[0] : NULL;
	for (j=0; j< nx ; j++) yt[j] = zp[j];
	prepare_filtered_profile(yt, nx, w_flag, filter, width);
	j = coarse_find_z_by_least_square_min(yt, nx, oir, &min,  rc);
	j = (j == 0) ? j + 1: j;
	j = (j == ny-1) ? ny - 2: j;
	/*
	*z = oir->ay + oir->dy *  j;
	return er;	
	*/
	tmp = (float)j;
	phi = find_simple_phase_shift_between_filtered_profile(psd[j].fl, yt, nx, rc);
	tmp = (dsrz != NULL) ? dsrz->xd[j] : 0;
	tmp = (tmp != 0) ? (float)j + phi/tmp : (float)j;
	k = (int)(tmp+.5);  
	er = (abs(k - j) >= 2) ? 1 : 0;
	k = (k <= 0) ? 1: k;
	k = (k >= ny-1) ? ny - 2: k;
	
	ph_1 = (k-1 == j) ? phi : 
		find_simple_phase_shift_between_filtered_profile(psd[k-1].fl, yt, nx, rc);
	ph1 = (k+1 == j) ? phi :
		find_simple_phase_shift_between_filtered_profile(psd[k+1].fl, yt, nx, rc);
	phi = (k == j) ? phi :
		find_simple_phase_shift_between_filtered_profile(psd[k].fl, yt, nx, rc);
	*z = oir->ay + oir->dy * (find_zero_of_3_points_polynome(ph_1, phi, ph1) + k);
	return er;
}

int find_z_profile_by_mean_square_and_phase_omar(float *zp, int nx, O_i *oir, int w_flag, int filter, int width, int rc, float *z)
{
	register int k, j;
	int er = 0, ny;
	static int oldnx = 0;
	static float *yt = NULL;
	float min, tmp, ph_1, phi, ph1;
	union pix *psd;
	O_p *opz;
	d_s *dsrz;
	
	if (yt == NULL || oldnx != nx)
	{
		yt = (float*)realloc(yt,nx*sizeof(float));
		if (yt == NULL)		return -1;
		oldnx = nx;
	}
	psd = oir->im.pixel;
	ny = oir->im.ny;
	opz = (oir->n_op != 0) ? oir->o_p[0] : NULL;
	dsrz = (opz != NULL) ? opz->dat[0] : NULL;
	for (j=0; j< nx ; j++) yt[j] = zp[j];
	prepare_filtered_profile(yt, nx, w_flag, filter, width);
	j = coarse_find_z_by_least_square_min(yt, nx, oir, &min,  rc);
	j = (j == 0) ? j + 1: j;
	j = (j == ny-1) ? ny - 2: j;
	/*
	*z = oir->ay + oir->dy *  j;
	return er;	
	*/
	tmp = (float)j;
	phi = find_simple_phase_shift_between_filtered_profile_omar(psd[j].fl, yt, nx, rc);
	tmp = (dsrz != NULL) ? dsrz->xd[j] : 0;
	tmp = (tmp != 0) ? (float)j + phi/tmp : (float)j;
	k = (int)(tmp+.5);  
	er = (abs(k - j) >= 2) ? 1 : 0;
	k = (k <= 0) ? 1: k;
	k = (k >= ny-1) ? ny - 2: k;
	
	ph_1 = (k-1 == j) ? phi : 
		find_simple_phase_shift_between_filtered_profile_omar(psd[k-1].fl, yt, nx, rc);
	ph1 = (k+1 == j) ? phi :
		find_simple_phase_shift_between_filtered_profile_omar(psd[k+1].fl, yt, nx, rc);
	phi = (k == j) ? phi :
		find_simple_phase_shift_between_filtered_profile_omar(psd[k].fl, yt, nx, rc);
	*z = oir->ay + oir->dy * (find_zero_of_3_points_polynome(ph_1, phi, ph1) + k);
	return er;
}
		  

int refresh_screen_image_and_cross(imreg *imrs, O_i *movie, DIALOG *d, int xc, int yc, int cl, int cw, int color)
{
        BITMAP *imb;

	if (movie == NULL || imrs == NULL || d == NULL) return 1;
	movie->need_to_refresh |= BITMAP_NEED_REFRESH;
	display_image_stuff_16M(imrs,d);
	if ((movie->bmp.to == IS_BITMAP) &&  (movie->bmp.stuff != NULL))
	{
	  imb = (BITMAP*)movie->bmp.stuff;
	  acquire_bitmap(screen);
	  draw_cross_vga_screen_unit(xc, yc, cl, cw, color, imrs, imb);
	  blit(imb,screen,0,0,imrs->x_off + d->x, imrs->y_off - 
		imb->h + d->y, imb->w, imb->h); 
	  release_bitmap(screen);	
	}
	return 0;
}

int refresh_screen_image_and_X_cross(imreg *imrs, O_i *movie, DIALOG *d, int xc, int yc, int cl, int cw, int color)
{
        BITMAP *imb;

	if (movie == NULL || imrs == NULL || d == NULL) return 1;
	movie->need_to_refresh |= BITMAP_NEED_REFRESH;
	display_image_stuff_16M(imrs,d);
	if ((movie->bmp.to == IS_BITMAP) &&  (movie->bmp.stuff != NULL))
	{
	  imb = (BITMAP*)movie->bmp.stuff;
	  acquire_bitmap(screen);
	  draw_cross_X_vga_screen_unit(xc, yc, cl, cw, color, imb);
	  blit(imb,screen,0,0,imrs->x_off + d->x, imrs->y_off - 
		imb->h + d->y, imb->w, imb->h); 
	  release_bitmap(screen);	
	}
	return 0;
}



int track_in_x_y_X_from_movie(imreg *imrs, O_i *oid, int *xci, int *yci, int cl, 
	int cw, int nf, int imstart, int black_circle, float *xb, float *yb, O_i *oip)
{
	register int k;
	int  wait = 0, wait_min, im, ia;
	int xc, yc, cl2, cw2;
	static int dis_filter = 0;
	int bead_lost = 0, bd_mul = 8;
	int x[128], y[128];
	float x1[128], y1[128], fx1[128], fy1[128];
	float xcf, ycf, dx, dy, corr, *zp = NULL;
	int  done;
	clock_t start = 0, t0, tc, tms, start_ia0;
	O_i *movie;
	int  color;
	static float  ms = 0;
	DIALOG *othe_dialog;
	DIALOG *d;
	LARGE_INTEGER ti0, ti1, freq;
	long long dtm, dtm0 = 0;
	double dti0,  dfreq;
	O_p *op;
	d_s *ds;

	(void)oid;
	othe_dialog = get_the_dialog();
	d = othe_dialog + 2;
	xc = *xci;	yc = *yci;		
	xcf = xc; 	ycf = yc;
	movie = imrs->one_i;
	cl2 = cl/2; cw2 = cw/2;

	op = create_and_attach_op_to_oi(movie, nf, nf, 0,0);		
	if (op == NULL)		return win_printf_OK("cannot create plot !");
	ds = op->dat[0];	
	movie->cur_op = movie->n_op - 1;
	
	tms = (CLOCKS_PER_SEC*ms)/1000;
	color = makecol(255,64,64);
	start_ia0 = start = clock();

	for (im = 0, wait_min = 1000, done = 0; im < (nf + imstart) && done == 0 ; im++ )
	{
		t0 = im * tms;
		ia = (im < imstart) ? 0 : im - imstart;
		switch_frame(movie,ia);		/* no negative index */
		if (oip != NULL)	zp = oip->im.pixel[ia].fl;

	    	if (fill_X_avg_profiles_from_im(movie, xc, yc, cl, cw, x, y))
		  win_printf("profil error");

		if ( im == imstart || wait < wait_min )	wait_min = wait;
		for (k = 0;(oip != NULL) && (k < cl); k++)	zp[k] = (float)y[k];
		for (k = 0;(oip != NULL) && (k < cl); k++)	zp[k+cl] = (float)y[k];

		if (key[KEY_ESC]) done = 1;

		QueryPerformanceCounter(&ti0);
		
		if (wait < wait_min )	wait_min = wait;
		if (black_circle)		erase_around_black_circle(x, y, cl);
		bead_lost += check_bead_not_lost(x, x1, y, y1, cl, bd_mul);
		if (bead_lost > 10)		done = 2;

		dx = find_distance_from_center(x1, cl, 0, dis_filter, !black_circle,&corr);
		dy = find_distance_from_center(y1, cl, 0, dis_filter, !black_circle,&corr);

		xb[ia] = xcf = xc + (dx + dy);
		yb[ia] = ycf = yc + (dx - dy) - 1;
	
		xc = (int)(xcf + 0.5);	yc = (int)(ycf + 0.5);
		clip_X_cross_in_image(movie, xc, yc, cl2, cw2);
		QueryPerformanceCounter(&ti1);
		dtm = ti1.QuadPart - ti0.QuadPart;
		if (ia == 0) freq.QuadPart = dtm0 = dtm;
		else freq.QuadPart += dtm;
		dtm0 = (dtm > dtm0) ? dtm : dtm0; 		

		refresh_screen_image_and_X_cross(imrs, movie, d, xc, yc, cl, cw, color);
		for (k = 0;(oip != NULL) && (k < cl); k++)	
		{
			zp[k] = fx1[k];	
			zp[k+cl] = fy1[k];	
		}
		for (tc = clock() - start; tc < t0; tc = clock() - start);
		if (ia == 0) start_ia0 = tc;
		ds->yd[ia] = (float)dtm;
	}
	start =  clock() - start;
	start -= start_ia0;
	win_printf("wait min %d\n %d im in %g S\nsoit %g im/s",wait_min,nf,
		((float)start)/((float)CLOCKS_PER_SEC),((float)CLOCKS_PER_SEC*nf)/start);


	dti0 =(double)(freq.QuadPart)/nf;
	dti0 *= 1000000;
	QueryPerformanceFrequency(&freq);
	dfreq = (double)(freq.QuadPart);
	dtm0 *= 1000000;
	win_printf("X grabbing time = %g \\mu s\nmax %g \\mu s", (dfreq != 0) ? 
	      dti0/dfreq : dti0, (dfreq != 0)? (double)dtm0/dfreq : (double)dtm0);


	for (k = 0; (k < nf) && (dfreq != 0); k++) 
	{
		ds->xd[k] = k;
		ds->yd[k] /= dfreq;
	} 


//	reset_cursor();
//	draw_cross_vga(xc, yc, cl, cw, 0, imrs);
	*xci = xc;
	*yci = yc;
	return (done); 
}


int track_in_2x_y_from_movie(imreg *imrs, O_i *oid, int *xci, int *yci, int cl, 
	int cw, int nf, int imstart, int black_circle, float *xb, float *yb, O_i *oip)
{
	register int k;
	int  wait = 0, wait_min, im, ia;
	int xc, yc, cl2 = cl/2;
	int  bead_lost = 0, bd_mul = 8;
	static int dis_filter = 0;
	int x[256], y[256];
	float x1[256], y1[256];
	float xcf, ycf, dx, dy, corr, *zp = NULL;
	int  done;
	clock_t start = 0, t0, tc, tms, start_ia0;
	O_i *movie;
	int  color;
	static float  ms = 0;
	DIALOG *othe_dialog;
	DIALOG *d;
	LARGE_INTEGER ti0, ti1, freq;
	long long dtm, dtm0 = 0;
	double dti0,  dfreq;

	(void)oid;
	othe_dialog = get_the_dialog();
	d = othe_dialog + 2;
	xc = *xci;	yc = *yci;		
	xcf = xc; 	ycf = yc;
	movie = imrs->one_i;
	
	tms = (CLOCKS_PER_SEC*ms)/1000;
	color = makecol(255,64,64);
	start_ia0 = start = clock();

	QueryPerformanceCounter(&ti0);
	for (im = 0, wait_min = 1000, done = 0; im < (nf + imstart) && done == 0 ; im++ )
	{
		t0 = im * tms;
		ia = (im < imstart) ? 0 : im - imstart;
		switch_frame(movie,ia);		/* no negative index */
		if (oip != NULL)	zp = oip->im.pixel[ia].fl;

		if (fill_avg_profiles_from_im(movie, xc, yc, cl, cw, x, y))
		              win_printf("profil error");
		if ( im == imstart || wait < wait_min )	wait_min = wait;
		for (k = 0;(oip != NULL) && (k < cl); k++)	
		  {
		    zp[k] = (float)x[k];
		    zp[k+cl] = (float)y[k];
		  }

		if (key[KEY_ESC]) done = 1;
		QueryPerformanceCounter(&ti0);
		
		if (wait < wait_min )	wait_min = wait;
		if (black_circle)		erase_around_black_circle(x, y, cl);
		bead_lost += check_bead_not_lost(x, x1, y, y1, cl, bd_mul);
		if (bead_lost > 10)		done = 2;
		
		dx = find_distance_from_center(x1, cl, 0, dis_filter, !black_circle,&corr);
		dy = find_distance_from_center(y1, cl, 0, dis_filter, !black_circle,&corr);

		xb[ia] = xcf = dx + xc;
		yb[ia] = ycf = yc + dy;
		xc = (int)(xcf + .5);	yc = (int)(ycf + .5);
		clip_cross_in_image(movie, xc, yc, cl2);
		QueryPerformanceCounter(&ti1);
		dtm = ti1.QuadPart - ti0.QuadPart;
		if (ia == 0) freq.QuadPart = dtm0 = dtm;
		else freq.QuadPart += dtm;
		dtm0 = (dtm > dtm0) ? dtm : dtm0; 
		
		refresh_screen_image_and_cross(imrs, movie, d, xc, yc, cl, cw, color);
		for (tc = clock() - start; tc < t0; tc = clock() - start);
		if (ia == 0) 
		  {
		    start_ia0 = tc;
		    //		    QueryPerformanceCounter(&ti0);
		  }

	}
	start =  clock() - start;
	start -= start_ia0;
	win_printf("wait min %d\n %d im in %g S\nsoit %g im/s",wait_min,nf,
		((float)start)/((float)CLOCKS_PER_SEC),((float)CLOCKS_PER_SEC*nf)/start);

	dti0 =(double)(freq.QuadPart)/nf;
	dti0 *= 1000000;
	QueryPerformanceFrequency(&freq);
	dfreq = (double)(freq.QuadPart);
	dtm0 *= 1000000;
	win_printf("X grabbing time = %g \\mu s\nmax %g \\mu s", (dfreq != 0) ? 
	      dti0/dfreq : dti0, (dfreq != 0)? (double)dtm0/dfreq : (double)dtm0);



	*xci = xc;
	*yci = yc;
	return (done); 
}



int track_in_x_y_from_movie(imreg *imrs, O_i *oid, int *xci, int *yci, int cl, 
			    int cw, int nf, int imstart, int black_circle, 
			    float *xb, float *yb, O_i *oip, O_i *oiz)
{
	register int k;
	int  wait = 0, wait_min, im, ia;
	int xc, yc, cl2 = cl/2;
	int  bead_lost = 0, bd_mul = 8;
	static int dis_filter = 0;
	int x[256], y[256];
	float x1[256], y1[256];
	float xcf, ycf, dx, dy, corr, *zp = NULL;
	int  done;
	clock_t start = 0, t0, tc, tms, start_ia0;
	O_i *movie;
	int  color;
	static float  ms = 0;
	DIALOG *othe_dialog;
	DIALOG *d;
	LARGE_INTEGER ti0, ti1, freq;
	long long dtm, dtm0 = 0;
	double dti0,  dfreq;

	(void)oid;
	othe_dialog = get_the_dialog();
	d = othe_dialog + 2;
	xc = *xci;	yc = *yci;		
	xcf = xc; 	ycf = yc;
	movie = imrs->one_i;
	
	tms = (CLOCKS_PER_SEC*ms)/1000;
	color = makecol(255,64,64);
	start_ia0 = start = clock();

	QueryPerformanceCounter(&ti0);
	for (im = 0, wait_min = 1000, done = 0; im < (nf + imstart) && done == 0 ; im++ )
	{
		t0 = im * tms;
		ia = (im < imstart) ? 0 : im - imstart;
		switch_frame(movie,ia);		/* no negative index */
		if (oip != NULL)	zp = oip->im.pixel[ia].fl;

		if (fill_avg_profiles_from_im(movie, xc, yc, cl, cw, x, y))
		              win_printf("profil error");
		if ( im == imstart || wait < wait_min )	wait_min = wait;
		for (k = 0;(oip != NULL) && (k < cl); k++)	
		  {
		    zp[k] = (float)x[k];
		    zp[k+cl] = (float)y[k];
		  }

		if (key[KEY_ESC]) done = 1;
		QueryPerformanceCounter(&ti0);
		
		if (wait < wait_min )	wait_min = wait;
		if (black_circle)		erase_around_black_circle(x, y, cl);
		bead_lost += check_bead_not_lost(x, x1, y, y1, cl, bd_mul);
		if (bead_lost > 10)		done = 2;
		
		dx = find_distance_from_center(x1, cl, 0, dis_filter, !black_circle,&corr);
		dy = find_distance_from_center(y1, cl, 0, dis_filter, !black_circle,&corr);

		xb[ia] = xcf = dx + xc;
		yb[ia] = ycf = yc + dy;
		if (oiz)
		  {
		    radial_non_pixel_square_image_sym_profile_in_array(oiz->im.pixel[ia].fl, movie, xcf, ycf, oiz->im.nx/2, 1, 1);
		  }
		xc = (int)(xcf + .5);	yc = (int)(ycf + .5);
		clip_cross_in_image(movie, xc, yc, cl2);
		QueryPerformanceCounter(&ti1);
		dtm = ti1.QuadPart - ti0.QuadPart;
		if (ia == 0) freq.QuadPart = dtm0 = dtm;
		else freq.QuadPart += dtm;
		dtm0 = (dtm > dtm0) ? dtm : dtm0; 
		
		refresh_screen_image_and_cross(imrs, movie, d, xc, yc, cl, cw, color);
		for (tc = clock() - start; tc < t0; tc = clock() - start);
		if (ia == 0) 
		  {
		    start_ia0 = tc;
		    //		    QueryPerformanceCounter(&ti0);
		  }

	}
	start =  clock() - start;
	start -= start_ia0;
	win_printf("wait min %d\n %d im in %g S\nsoit %g im/s",wait_min,nf,
		((float)start)/((float)CLOCKS_PER_SEC),((float)CLOCKS_PER_SEC*nf)/start);

	dti0 =(double)(freq.QuadPart)/nf;
	dti0 *= 1000000;
	QueryPerformanceFrequency(&freq);
	dfreq = (double)(freq.QuadPart);
	dtm0 *= 1000000;
	win_printf("X grabbing time = %g \\mu s\nmax %g \\mu s", (dfreq != 0) ? 
	      dti0/dfreq : dti0, (dfreq != 0)? (double)dtm0/dfreq : (double)dtm0);



	*xci = xc;
	*yci = yc;
	return (done); 
}



int track_2bds_in_x_y_from_movie(imreg *imrs, O_i *oid, int *xci, int *yci, int cl, 
	int cw, int nf, int imstart, int black_circle, float *xb, float *yb, O_i *oip, float *theta, float *skew)
{
	int  wait = 0, wait_min, im, ia;
	int xc, yc, cl2 = cl/2;
	//	int x[128], y[128];
	//float x1[128], y1[128];
	float xcf, ycf, dx, dy, mean; // , *zp = NULL
	int  done;
	clock_t start = 0, t0, tc, tms, start_ia0, t, td;
	O_i *movie;
	int  color;
	static float  ms = 40;
	DIALOG *othe_dialog;
	DIALOG *d;
	LARGE_INTEGER ti0, ti1, freq;
	long long dtm, dtm0 = 0;
	double dti0,  dfreq;
	float cox, siy;
	int xl0, yl0, xl1, yl1;

	(void)skew;
	(void)black_circle;
	(void)oid;
	(void)oip;
	othe_dialog = get_the_dialog();
	d = othe_dialog + 2;
	xc = *xci;	yc = *yci;		
	xcf = xc; 	ycf = yc;
	movie = imrs->one_i;
	
	tms = (CLOCKS_PER_SEC*ms)/1000;
	color = makecol(255,64,64);
	start_ia0 = start = clock();

	QueryPerformanceCounter(&ti0);
	for (im = 0, wait_min = 1000, done = 0, td = clock(); im < (nf + imstart) && done == 0 ; im++ )
	{
		t0 = im * tms;
		ia = (im < imstart) ? 0 : im - imstart;
		switch_frame(movie,ia);		/* no negative index */
		//if (oip != NULL)	zp = oip->im.pixel[ia].fl;

		fill_bary_avg_profiles_from_im(movie, xc, yc, cl, &mean, &dx, &dy, theta + ia);
		//win_printf("Image %d mean %f, \ndx %f dy %f \ntheta %f xc %d yc %d",ia,mean,dx,dy,theta[ia],xc,yc);


		if (key[KEY_ESC]) done = 1;
		QueryPerformanceCounter(&ti0);
		
		if (wait < wait_min )	wait_min = wait;
		xb[ia] = xcf = dx + xc;
		yb[ia] = ycf = yc + dy;
		xc = (int)(xcf + .5);	yc = (int)(ycf + .5);
		//win_printf("2 Image %d mean %f, \ndx %f dy %f \ntheta %f xc %d yc %d",ia,mean,dx,dy,theta[ia],xc,yc);

		clip_cross_in_image(movie, xc, yc, cl2);
		QueryPerformanceCounter(&ti1);
		dtm = ti1.QuadPart - ti0.QuadPart;
		if (ia == 0) freq.QuadPart = dtm0 = dtm;
		else freq.QuadPart += dtm;
		dtm0 = (dtm > dtm0) ? dtm : dtm0; 
		
		refresh_screen_image_and_cross(imrs, movie, d, xc, yc, cl, cw, color);

		cox = cos(theta[ia]);
		siy = sin(theta[ia]);
		xl0 = x_imdata_2_imr(imrs,(0.5 + xcf + cox * 50)) + d->x;
		yl0 = y_imdata_2_imr(imrs,(0.5 + ycf + siy * 50)) + d->y;
		xl1 = x_imdata_2_imr(imrs,(0.5 + xcf - cox * 50)) + d->x;
		yl1 = y_imdata_2_imr(imrs,(0.5 + ycf - siy * 50)) + d->y;
		line(screen, xl0, yl0, xl1, yl1, color);
		//win_printf("end refresh");
		for (tc = clock() - start; tc < t0; tc = clock() - start);
		if (ia == 0) 
		  {
		    start_ia0 = tc;
		    //		    QueryPerformanceCounter(&ti0);
		  }
		for (t = clock() - td; t < t0; t = clock() - td);
	}
	start =  clock() - start;
	start -= start_ia0;
	win_printf("wait min %d\n %d im in %g S\nsoit %g im/s",wait_min,nf,
		((float)start)/((float)CLOCKS_PER_SEC),((float)CLOCKS_PER_SEC*nf)/start);

	dti0 =(double)(freq.QuadPart)/nf;
	dti0 *= 1000000;
	QueryPerformanceFrequency(&freq);
	dfreq = (double)(freq.QuadPart);
	dtm0 *= 1000000;
	win_printf("X grabbing time = %g \\mu s\nmax %g \\mu s", (dfreq != 0) ? 
	      dti0/dfreq : dti0, (dfreq != 0)? (double)dtm0/dfreq : (double)dtm0);



	*xci = xc;
	*yci = yc;
	return (done); 
}



int 	track_in_x_y_z_amp_phi_not_power_2_from_movie(imreg *imrs, O_i *oid, 
			int *xci, int *yci, int cl, int cw, int nf, int imstart, 
			int black_circle, float *xb, float *yb, sym_radial_profile *srp, 
			float *zb, int verbose, O_i *oir, float *error, int rout, O_i *oip)
{
	register int i;
	int  wait = 0, wait_min, im, ia, ner = 0, nx;
	int xc, yc;
	int bead_lost = 0, bd_mul = bead_lost_mul;
	int x[128], y[128];
	static int dis_filter = 0, mean_width_histo = 0;
	float x1[128], y1[128], *zp = NULL, *x1c, *y1c;
	float xcf, ycf, dx, dy, corr;
	int  done, mode;
	unsigned int *it = NULL;
	clock_t start = 0, t0, tc, tms, start_ia0;
	O_i *movie;
	int  color;
	static float  ms = 0;
	DIALOG *othe_dialog;
	DIALOG *d;//, *dip = NULL;
	pltreg *pr = NULL;
	O_p *op, *opdip = NULL;
	d_s *ds, *dsdip = NULL;
	LARGE_INTEGER ti0, ti1, freq, top0, top;
	long long dtm, dtm0 = 0;
	double dti0,  dfreq;
	unsigned long t_c, t_cm = 0;

	(void)verbose;
	(void)oid;
	othe_dialog = get_the_dialog();
	pr = find_pr_in_current_dialog(NULL);
	if (pr != NULL) 
	  {
	    //dip = find_dialog_associated_to_pr(pr, NULL);
	    opdip = pr->one_p;
	    dsdip = pr->one_p->dat[0];
	  }
	d = othe_dialog + 2;	
	xc = *xci;	yc = *yci;		
	xcf = xc; 	ycf = yc;
	movie = imrs->one_i;
	nx = srp->npc;
	if (zp == NULL)		zp = (float*)calloc(srp->npc,sizeof(float));
	if (zp == NULL)		return win_printf_OK("cannot allocate memeory!");	
	mode = (oir == NULL && srp->pz == NULL) ? GETTING_REF_PROFILE : 0;
	if (oir == NULL && srp->pz == NULL)	
		srp->pz = (float*)calloc(srp->npc,sizeof(float));
	if (oir == NULL && srp->pz == NULL)	
		return win_printf_OK("cannot allocate memory!");
	x1c = ((nx - cl) >= 0) ? x1 + (nx - cl)/2 : x1;
	y1c = ((nx - cl) >= 0) ? y1 + (nx - cl)/2 : y1;
	if (error != NULL) it = (unsigned int *)error;

	if (dis_filter == 0)
	{
		dis_filter = nx>>2;
		win_scanf("filtering at %dusing max histogram of z to remove\n"
			  " dc 0->no else yes \n used when dust are present%d",
			  &dis_filter,&mean_width_histo);
	}

	tms = (CLOCKS_PER_SEC*ms)/1000;
	color = makecol(255,64,64);
	start_ia0 = start = clock();

	for (im = 0, wait_min = 1000, done = 0; im < (nf + imstart) && done == 0 ; im++ )
	{
		t0 = im * tms;
		ia = (im < imstart) ? 0 : im - imstart;
		switch_frame(movie,ia);		/* no negative index */
		if (oip != NULL)	zp = oip->im.pixel[ia].fl;

		if (fill_avg_profiles_from_im(movie, xc, yc, cl, cw, x, y))
			win_printf("profil error");
		
		if ( ia == 0 || wait < wait_min )	wait_min = wait;
		for (i = 0;(oip != NULL) && (i < cl); i++)	zp[i] = (float)y[i];
		if (key[KEY_ESC]) done = 1;

		if (error != NULL)		error[ia] = wait;
		if (black_circle)		erase_around_black_circle(x, y, cl);
		bead_lost += check_bead_not_lost(x, x1c, y, y1c, cl, bd_mul);
		if (bead_lost > 10)		done = 2;

		/* fill ends of arrays */
		for (i = 0; i < (nx-cl)/2; i++)
		{
			x1[i] = x1c[0];		x1[nx-i-1] = x1c[cl-1];
			y1[i] = y1c[0];		y1[nx-i-1] = y1c[cl-1];
		}			
		dx = find_distance_from_center(x1, nx, 0, dis_filter, !black_circle,&corr);
		dy = find_distance_from_center(y1, nx, 0, dis_filter, !black_circle,&corr);

		xb[ia] = xcf = dx + xc;
		yb[ia] = ycf = yc + dy;
		xc = (int)(xcf + .5);	yc = (int)(ycf + .5);

		QueryPerformanceCounter(&ti0);
		if (rout == 0)
			radial_non_pixel_square_image_sym_profile_in_array_fixed(zp, movie, 
					       xc, yc, nx/2, srp->ax, srp->ay);
		else radial_non_pixel_square_image_sym_profile_in_array(zp, movie, xc, 
					       yc, nx/2, srp->ax, srp->ay);

		if ((mode == GETTING_REF_PROFILE) && (im - imstart >= 0))
		{
			for (i = 0; i < nx; i++)
				srp->pz[i] += zp[i];
		}

		else if (zb != NULL)
		{
			i = find_z_profile_by_mean_square_and_phase(zp, nx, oir, 0, 
			          srp->bp_center, srp->bp_width, srp->rc, zb + ia);
			/*draw_z_bar(imrs, oir, zb[ia]);*/
			ner += i;
		}
		QueryPerformanceCounter(&ti1);
		dtm = ti1.QuadPart - ti0.QuadPart;
		if (ia == 0) freq.QuadPart = dtm0 = dtm;
		else freq.QuadPart += dtm;
		dtm0 = (dtm > dtm0) ? dtm : dtm0; 
		t_c = my_uclock();
		refresh_screen_image_and_cross(imrs, movie, d, xc, yc, cl, cw, color);
		t_cm += my_uclock() - t_c;
		for (tc = clock() - start; tc < t0; tc = clock() - start);
		if (im < imstart)
		  {
		    start_ia0 = tc;
		    QueryPerformanceCounter(&top0);
		  }
		QueryPerformanceCounter(&top);
		top.QuadPart -= top0.QuadPart;
		if (error != NULL)		it[ia] = (unsigned int)(top.QuadPart>>4);
		if (pr != NULL)  
		  {
		    i = ia/64;
		    opdip->x_lo = (i > 0) ? (i-1) * 64 : i * 64 ;// * opdip->dx; 
		    opdip->x_hi = (i > 0) ? (i+1) * 64 : (i+2)* 64;// * opdip->dx;
		    set_plot_x_fixed_range(opdip);
		    dsdip->nx = dsdip->ny = ia; 
		    pr->one_p->need_to_refresh = 1;	
		    refresh_plot(pr, UNCHANGED);
		  }
/*		draw_z_advance(imrs, ia, nf); */
	}
	start =  clock() - start;
	start -= start_ia0;
	win_printf("wait min %d\n %d im in %g S\nsoit %g im/s",wait_min,nf,
		((float)start)/((float)CLOCKS_PER_SEC),((float)CLOCKS_PER_SEC*nf)/start);

	if (mode == GETTING_REF_PROFILE)
	{
		for (i = 0; i < nx; i++)	srp->pz[i] /= nf;
	}
	op = create_and_attach_op_to_oi(movie, nx, nx, 0,0);		
	if (op == NULL)		return win_printf_OK("cannot create plot !");
	ds = op->dat[0];	
	movie->cur_op = movie->n_op - 1;
	for (i = 0; i < nx; i++)	
	  {
	    ds->xd[i] = i;
	    ds->yd[i] = zp[i];
	  }
	free(zp);

	dti0 =(double)(freq.QuadPart)/nf;
	dti0 *= 1000000;
	QueryPerformanceFrequency(&freq);
	dfreq = (double)(freq.QuadPart);
	dtm0 *= 1000000;
	win_printf("Z profile grabbing time = %g \\mu s\nmax %g \\mu s", (dfreq != 0) ? 
	      dti0/dfreq : dti0, (dfreq != 0)? (double)dtm0/dfreq : (double)dtm0);

	dfreq = (double)MY_UCLOCKS_PER_SEC;
	dti0 = (double)t_cm;
	dti0 /= im;
	dti0 /= dfreq;
	dti0 *= 1000000;
	win_printf("image display  %g \\mu ms", dti0);

	*xci = xc;
	*yci = yc;
	if (ner == 0 && done == 2)	ner = 1;
	return (done == 0) ? ner : -ner; 
}


int track_in_x_y_from_movie_ortho(imreg *imrs, O_i *oid, int *xci, int *yci, int cl, 
	int cw, int nf, int imstart, int black_circle, float *xb, float *yb, O_i *oip, 
	int radius, int width, float ax, float ay)
{
	int  wait = 0, wait_min, im, ia;
	int xc, yc, cl2 = cl/2;
	int  bead_lost = 0, bd_mul = 8;
	static int dis_filter = 0;
	int x[128], y[128];
	float x1[128], y1[128];
	float xcf, ycf, dx, dy, corr, *zp = NULL;
	int  done, ortho_size;
	clock_t start = 0, t0, tc, tms, start_ia0;
	O_i *movie;
	int  color;
	static float  ms = 0;
	DIALOG *othe_dialog;
	DIALOG *d;
	LARGE_INTEGER ti0, ti1, freq;
	long long dtm, dtm0 = 0;
	double dti0,  dfreq;

	(void)oid;
	othe_dialog = get_the_dialog();
	d = othe_dialog + 2;
	xc = *xci;	yc = *yci;		
	xcf = xc; 	ycf = yc;
	movie = imrs->one_i;
	
	ortho_size = oip->im.nx;
	tms = (CLOCKS_PER_SEC*ms)/1000;
	color = makecol(255,64,64);
	start_ia0 = start = clock();

	QueryPerformanceCounter(&ti0);
	for (im = 0, wait_min = 1000, done = 0; im < (nf + imstart) && done == 0 ; im++ )
	{
		t0 = im * tms;
		ia = (im < imstart) ? 0 : im - imstart;
		switch_frame(movie,ia);		/* no negative index */
		zp = oip->im.pixel[ia].fl;

		if (fill_avg_profiles_from_im(movie, xc, yc, cl, cw, x, y))
		              win_printf("profil error");
		if ( im == imstart || wait < wait_min )	wait_min = wait;

		if (key[KEY_ESC]) done = 1;
		QueryPerformanceCounter(&ti0);
		
		if (wait < wait_min )	wait_min = wait;
		if (black_circle)		erase_around_black_circle(x, y, cl);
		bead_lost += check_bead_not_lost(x, x1, y, y1, cl, bd_mul);
		if (bead_lost > 10)		done = 2;
		
		dx = find_distance_from_center(x1, cl, 0, dis_filter, !black_circle,&corr);
		dy = find_distance_from_center(y1, cl, 0, dis_filter, !black_circle,&corr);

		xb[ia] = xcf = dx + xc;
		yb[ia] = ycf = yc + dy;


		//orthoradial_non_pixel_square_image_sym_profile_in_array(zp, movie, xcf, ycf, 
		//				radius, width, ortho_size, ax, ay);


		orthoradial_non_pixel_square_image_sym_profile_in_array_interpol_avg(zp, movie, xcf, ycf, 
						radius, width, ortho_size, ax, ay);


		xc = (int)(xcf + .5);	yc = (int)(ycf + .5);
		clip_cross_in_image(movie, xc, yc, cl2);
		QueryPerformanceCounter(&ti1);
		dtm = ti1.QuadPart - ti0.QuadPart;
		if (ia == 0) freq.QuadPart = dtm0 = dtm;
		else freq.QuadPart += dtm;
		dtm0 = (dtm > dtm0) ? dtm : dtm0; 
		
		refresh_screen_image_and_cross(imrs, movie, d, xc, yc, cl, cw, color);
		for (tc = clock() - start; tc < t0; tc = clock() - start);
		if (ia == 0) 
		  {
		    start_ia0 = tc;
		    //		    QueryPerformanceCounter(&ti0);
		  }

	}
	start =  clock() - start;
	start -= start_ia0;
	win_printf("wait min %d\n %d im in %g S\nsoit %g im/s",wait_min,nf,
		((float)start)/((float)CLOCKS_PER_SEC),((float)CLOCKS_PER_SEC*nf)/start);

	dti0 =(double)(freq.QuadPart)/nf;
	dti0 *= 1000000;
	QueryPerformanceFrequency(&freq);
	dfreq = (double)(freq.QuadPart);
	dtm0 *= 1000000;
	win_printf("X grabbing time = %g \\mu s\nmax %g \\mu s", (dfreq != 0) ? 
	      dti0/dfreq : dti0, (dfreq != 0)? (double)dtm0/dfreq : (double)dtm0);



	*xci = xc;
	*yci = yc;
	return (done); 
}



int track_in_x_y_from_movie_build_ortho_movie(imreg *imrs, O_i *oid, int *xci, int *yci, 
	int cl, int cw, int nf, int imstart, int black_circle, float *xb, float *yb, 
	O_i *oip, float ax, float ay)
{
	int  wait = 0, wait_min, im, ia, ir;
	int xc, yc, cl2 = cl/2;
	int  bead_lost = 0, bd_mul = 8, radius;
	static int dis_filter = 0;
	int x[128], y[128];
	float x1[128], y1[128];
	float xcf, ycf, dx, dy, corr;//, *zp = NULL;
	int  done, ortho_size;
	clock_t start = 0, t0, tc, tms, start_ia0;
	O_i *movie;
	int  color;
	static float  ms = 0;
	DIALOG *othe_dialog;
	DIALOG *d;
	LARGE_INTEGER ti0, ti1, freq;
	long long dtm, dtm0 = 0;
	double dti0,  dfreq;

	(void)oid;
	othe_dialog = get_the_dialog();
	d = othe_dialog + 2;
	xc = *xci;	yc = *yci;		
	xcf = xc; 	ycf = yc;
	movie = imrs->one_i;
	
	ortho_size = oip->im.nx;
	radius = oip->im.ny;
	tms = (CLOCKS_PER_SEC*ms)/1000;
	color = makecol(255,64,64);
	start_ia0 = start = clock();

	QueryPerformanceCounter(&ti0);
	for (im = 0, wait_min = 1000, done = 0; im < (nf + imstart) && done == 0 ; im++ )
	{
		t0 = im * tms;
		ia = (im < imstart) ? 0 : im - imstart;
		switch_frame(movie,ia);		/* no negative index */
		//zp = oip->im.pixel[ia].fl;

		if (fill_avg_profiles_from_im(movie, xc, yc, cl, cw, x, y))
		              win_printf("profil error");
		if ( im == imstart || wait < wait_min )	wait_min = wait;

		if (key[KEY_ESC]) done = 1;
		QueryPerformanceCounter(&ti0);
		
		if (wait < wait_min )	wait_min = wait;
		if (black_circle)		erase_around_black_circle(x, y, cl);
		bead_lost += check_bead_not_lost(x, x1, y, y1, cl, bd_mul);
		if (bead_lost > 10)		done = 2;
		
		dx = find_distance_from_center(x1, cl, 0, dis_filter, !black_circle,&corr);
		dy = find_distance_from_center(y1, cl, 0, dis_filter, !black_circle,&corr);

		xb[ia] = xcf = dx + xc;
		yb[ia] = ycf = yc + dy;
		display_title_message("frame %d/%d",ia,nf);
		switch_frame(oip,ia);		/* no negative index */
		for ( ir = 0; (im >= imstart) && ir < radius; ir++)
		  orthoradial_non_pixel_square_image_sym_profile_in_array_interpol(oip->im.pixel[ir].fl, 
							     movie, xcf, ycf, ir, ortho_size, ax, ay);

		display_title_message("frame %d/%d im %d",ia,nf,im);
		xc = (int)(xcf + .5);	yc = (int)(ycf + .5);
		clip_cross_in_image(movie, xc, yc, cl2);
		QueryPerformanceCounter(&ti1);
		dtm = ti1.QuadPart - ti0.QuadPart;
		if (ia == 0) freq.QuadPart = dtm0 = dtm;
		else freq.QuadPart += dtm;
		dtm0 = (dtm > dtm0) ? dtm : dtm0; 
		
		refresh_screen_image_and_cross(imrs, movie, d, xc, yc, cl, cw, color);
		for (tc = clock() - start; tc < t0; tc = clock() - start);
		if (ia == 0) 
		  {
		    start_ia0 = tc;
		    //		    QueryPerformanceCounter(&ti0);
		  }

	}
	start =  clock() - start;
	start -= start_ia0;
	win_printf("wait min %d\n %d im in %g S\nsoit %g im/s",wait_min,nf,
		((float)start)/((float)CLOCKS_PER_SEC),((float)CLOCKS_PER_SEC*nf)/start);

	dti0 =(double)(freq.QuadPart)/nf;
	dti0 *= 1000000;
	QueryPerformanceFrequency(&freq);
	dfreq = (double)(freq.QuadPart);
	dtm0 *= 1000000;
	win_printf("X grabbing time = %g \\mu s\nmax %g \\mu s", (dfreq != 0) ? 
	      dti0/dfreq : dti0, (dfreq != 0)? (double)dtm0/dfreq : (double)dtm0);



	*xci = xc;
	*yci = yc;
	return (done); 
}




int follow_bead_in_x_y_from_movie(void)
{
	register int i;
	imreg *imrs;	
	O_p *op;	
	O_i *ois, *oidp = NULL, *oiz = NULL;
	d_s *ds; 
	int xc = 256, yc = 256, color;
	static int cl = 64, cw = 16, nf = 2048, imstart = 10, prof = 0, zprof = 0;
	int  black_circle = 0;
	
	if(updating_menu_state != 0)	return D_O_K;
	
	if (ac_grep(NULL,"%im%oi",&imrs,&ois) != 2)	
		return win_printf_OK("cannot find image in acreg!");
	nf = ois->im.n_f;
	color = makecol(255,64,64);
	/* check for menu in win_scanf */
	i = win_scanf("this routine track a bead in X,Y using a cross shaped pattern\n"
		      "arm length %6d arm width %6d\n howmany frames %6d imstart %6d\n"
		      "generate XY profile image %b\n"
		      "generate Radial profile image %b\n key {\\it b} black circle {\\it w} to stop"
		      ,&cl,&cw,&nf,&imstart,&prof,&zprof); /* %m ,select_power_of_2() */
	if (i == WIN_CANCEL)	return OFF; 
	if (cl > 256)	return win_printf_OK("cannot use arm bigger\nthan 128");
	if (cw > 32)	return win_printf_OK("cannot use width bigger\nthan 32");

	if (fft_init(cl) || filter_init(cl))	return win_printf_OK("cannot init fft!");	
	xc = ois->im.nx/2;	yc = ois->im.ny/2;
	i = win_scanf("enter starting position in pixels \n xc %d yc %d",&xc,&yc);
	if (i == WIN_CANCEL) 	return OFF;
	if (xc < 0 || yc < 0)
	{
		xc = ois->im.nx/2;	yc = ois->im.ny/2;
		place_tracking_cross_with_mouse_movie(&xc, &yc, cl, cw, color);
	}

	op = create_and_attach_op_to_oi(ois, nf, nf, 0,IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
	if (op == NULL)		return win_printf_OK("cannot create plot !");
	ds = op->dat[0];

	ds->source = my_sprintf(ds->source,"Bead tracking at 25 Hz Acquistion %d"
		" de 1 billes \n from movie l = %d, w = %d, nim = %d\nobjective %d,"
		" zoom factor %f\nIC-PCI %g MHz",0,cl,cw,nf,63,1.6,14.18);		
	uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
	uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
	set_plot_title(op, "Trajectoire 0 bis");

	op->filename = my_sprintf(op->filename,"tracs000d%s",".gr");
	if (prof)
	{
		oidp = create_and_attach_oi_to_imr(imrs, 2*cl, nf, IS_FLOAT_IMAGE);
		oidp->im.source = strdup("MOVIE bead profile");
	}

	if (zprof)
	{
		oiz = create_and_attach_oi_to_imr(imrs, cl, nf, IS_FLOAT_IMAGE);
		oiz->im.source = strdup("MOVIE bead profile");
	}

	track_in_x_y_from_movie(imrs, NULL, &xc, &yc, cl, cw, nf, imstart, 
				black_circle, ds->xd, ds->yd, oidp, oiz);

	op->need_to_refresh = 1;	
	ois->need_to_refresh = PLOTS_NEED_REFRESH;
	if (ois->n_op) 	    ois->cur_op = ois->n_op - 1;
	if (prof)		
	{
		find_zmin_zmax(oidp);	
		oidp->need_to_refresh = ALL_NEED_REFRESH;	
	}
	if (zprof)		
	{
		find_zmin_zmax(oiz);	
		oiz->need_to_refresh = ALL_NEED_REFRESH;	
	}
        return broadcast_dialog_message(MSG_DRAW,0);			
}

int follow_bead_in_x_y_X_from_movie(void)
{
	register int i;
	imreg *imrs;	
	O_p *op;	
	O_i *ois, *oidp = NULL;
	d_s *ds; 
	int xc = 256, yc = 256, color;
	static int cl = 128, cw = 16, nf = 2048, imstart = 10, prof = 0;
	int  black_circle = 0;
	
	if(updating_menu_state != 0)	return D_O_K;
	
	if (ac_grep(NULL,"%im%oi",&imrs,&ois) != 2)	
		return win_printf_OK("cannot find image in acreg!");
	nf = ois->im.n_f;
	color = makecol(255,64,64);
	/* check for menu in win_scanf */
	i = win_scanf("this routine track a bead in X,Y\n"
		" using a cross shaped pattern\narm length %d"
		"arm width %dhowmany frames %d imstart %d"
		"profile %dkey {\\it b} black circle {\\it w} to stop"
		,&cl,&cw,&nf,&imstart,&prof); /* %m ,select_power_of_2() */
	if (i == WIN_CANCEL)	return OFF; 
	if (cl > 128)	return win_printf_OK("cannot use arm bigger\nthan 128");
	if (cw > 32)	return win_printf_OK("cannot use width bigger\nthan 32");

	if (fft_init(cl) || filter_init(cl))	return win_printf_OK("cannot init fft!");	
	xc = ois->im.nx/2;	yc = ois->im.ny/2;
	i = win_scanf("enter starting position in pixels \n xc %d yc %d",&xc,&yc);
	if (i == WIN_CANCEL) 	return OFF;
	if (xc < 0 || yc < 0)
	{
		xc = ois->im.nx/2;	yc = ois->im.ny/2;
		place_tracking_cross_with_mouse_movie(&xc, &yc, cl, cw, color);
	}

	op = create_and_attach_op_to_oi(ois, nf, nf, 0,IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
	if (op == NULL)		return win_printf_OK("cannot create plot !");
	ds = op->dat[0];

	ds->source = my_sprintf(ds->source,"Bead tracking at 25 Hz Acquistion %d"
		" de 1 billes \n from movie l = %d, w = %d, nim = %d\nobjective %d,"
		" zoom factor %f\nIC-PCI %g MHz",0,cl,cw,nf,63,1.6,14.18);		
	uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
	uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
	set_plot_title(op, "Trajectoire 0 bis");

	op->filename = my_sprintf(op->filename,"tracs000d%s",".gr");
	if (prof)
	{
		oidp = create_and_attach_oi_to_imr(imrs, 2*cl, nf, IS_FLOAT_IMAGE);
		oidp->im.source = strdup("MOVIE bead profile");
	}
	track_in_x_y_X_from_movie(imrs, NULL, &xc, &yc, cl, cw, nf, imstart, 
				black_circle, ds->xd, ds->yd, oidp);

	op->need_to_refresh = 1;	
	ois->need_to_refresh = PLOTS_NEED_REFRESH;
	if (ois->n_op) 	    ois->cur_op = ois->n_op - 1;
	if (prof)		
	{
		find_zmin_zmax(oidp);	
		oidp->need_to_refresh = ALL_NEED_REFRESH;	
	}
        return broadcast_dialog_message(MSG_DRAW,0);			
}



int follow_2_beads_in_x_y_from_movie(void)
{
	register int i, j;
	imreg *imrs;	
	O_p *op, *op2;	
	O_i *ois, *oidp = NULL;
	d_s *ds, *ds2, *ds4; 
	int xc = 256, yc = 256, color, dir;
	static int cl = 128, cw = 16, nf = 2048, imstart = 10, prof = 0;
	int  black_circle = 0;
	float v, v2, p;
	
	if(updating_menu_state != 0)	return D_O_K;
	
	if (ac_grep(NULL,"%im%oi",&imrs,&ois) != 2)	
		return win_printf_OK("cannot find image in acreg!");
	nf = ois->im.n_f;
	color = makecol(255,64,64);
	/* check for menu in win_scanf */
	i = win_scanf("this routine track 2 beads in X,Y\n"
		" using using barycenter of image in a square area\narm length %d"
		"arm width %dhowmany frames %d imstart %d"
		"profile %dkey {\\it b} black circle {\\it w} to stop"
		,&cl,&cw,&nf,&imstart,&prof); /* %m ,select_power_of_2() */
	if (i == WIN_CANCEL)	return OFF; 
	//if (cl > 128)	return win_printf_OK("cannot use arm bigger\nthan 128");
	//if (cw > 32)	return win_printf_OK("cannot use width bigger\nthan 32");

	if (fft_init(cl) || filter_init(cl))	return win_printf_OK("cannot init fft!");	
	xc = ois->im.nx/2;	yc = ois->im.ny/2;
	i = win_scanf("enter starting position in pixels \n xc %d yc %d",&xc,&yc);
	if (i == WIN_CANCEL) 	return OFF;
	if (xc < 0 || yc < 0)
	{
		xc = ois->im.nx/2;	yc = ois->im.ny/2;
		place_tracking_cross_with_mouse_movie(&xc, &yc, cl, cw, color);
	}


	op2 = create_and_attach_op_to_oi(ois, nf, nf, 0,0);
	if (op2 == NULL)		return win_printf_OK("cannot create plot !");
	ds2 = op2->dat[0];

	ds2->source = my_sprintf(ds2->source,"Bead tracking at 25 Hz Acquistion %d"
		" de 2 billes theta \n from movie l = %d, w = %d, nim = %d\nobjective %d,"
		" zoom factor %f\nIC-PCI %g MHz",0,cl,cw,nf,63,1.6,14.18);		


	op2->filename = my_sprintf(op2->filename,"trac2b-theta-000d%s",".gr");


	op = create_and_attach_op_to_oi(ois, nf, nf, 0,IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
	if (op == NULL)		return win_printf_OK("cannot create plot !");
	ds = op->dat[0];


	//ds3 = create_and_attach_one_ds(op2, nf, nf, 0);
	//if (ds3 == NULL)       return win_printf_OK("cannot create plot !");

	ds4 = create_and_attach_one_ds(op2, nf, nf, 0);
	if (ds4 == NULL)       return win_printf_OK("cannot create plot !");

	ds->source = my_sprintf(ds->source,"Bead tracking at 25 Hz Acquistion %d"
		" de 2 billes \n from movie l = %d, w = %d, nim = %d\nobjective %d,"
		" zoom factor %f\nIC-PCI %g MHz",0,cl,cw,nf,63,1.6,14.18);		
	uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
	uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
	set_plot_title(op, "Trajectoire 0 bis");

	op->filename = my_sprintf(op->filename,"trac2b000d%s",".gr");



	if (prof)
	{
		oidp = create_and_attach_oi_to_imr(imrs, 2*cl, nf, IS_FLOAT_IMAGE);
		oidp->im.source = strdup("MOVIE bead profile");
	}

	track_2bds_in_x_y_from_movie(imrs, NULL, &xc, &yc, cl, cw, nf, imstart, 
				black_circle, ds->xd, ds->yd, oidp, ds2->yd, NULL);

	op->need_to_refresh = 1;	
	op2->need_to_refresh = 1;	
	for (i = 0; i < nf; i++)  ds2->xd[i] = ds4->xd[i] = i;


	for (i = 0, dir = 0; i < nf-1; i++)
	    dir += ((ds2->yd[i+1] - ds2->yd[i]) > 0) ? 1 : -1;

	dir = (dir >= 0) ? 1 : -1;

	win_scanf("Rotation direction \n(1 -> counterclockwise)\n(-1 clockwise)%d",&dir);

	for (i = 0, j = 0, v = 0; i < nf-1; i++)
	  {
	    if ((dir * (ds2->yd[i+1] - ds2->yd[i])) >= 0)
	    {
	      v += ds2->yd[i+1] - ds2->yd[i];
	      j++;
	    }
	  }
	v = (j) ? v/j : v;

	ds4->yd[0] = ds2->yd[0];
	for (i = 0, j = 0, p = 0; i < nf-1; i++)
	  {
	    if ((dir * (ds2->yd[i+1] - ds2->yd[i])) < 0)
		p += dir * M_PI;
	    ds4->yd[i+1] = p + ds2->yd[i+1];
	  }

	for (i = 0, j = 0, v2 = 0; i < nf-1; i++)
	  {
	    if ((dir * (ds4->yd[i+1] - ds4->yd[i])) >= 0)
	    {
	      v2 += ds4->yd[i+1] - ds4->yd[i];
	      j++;
	    }
	  }
	v2 = (j) ? v2/j : v2;

	set_plot_title(op2, "Angle, %f rad/fr %f rad/fr rot = %f",v,v2,(v2*12.5)/M_PI);

	ois->need_to_refresh = PLOTS_NEED_REFRESH;
	if (ois->n_op) 	    ois->cur_op = ois->n_op - 1;
	if (prof)		
	{
		find_zmin_zmax(oidp);	
		oidp->need_to_refresh = ALL_NEED_REFRESH;	
	}
        return broadcast_dialog_message(MSG_DRAW,0);			
}





# ifdef IFC

int track_in_x_y_from_ifc(imreg *imrs, O_i *oid, int *xci, int *yci, int cl, 
	int cw, int nf, int imstart, int black_circle, float *xb, float *yb, O_i *oip)
{
	register int k;
	int  wait = 0, wait_min, im, ia, n0, ni;
	int ytt, xlt, xll, ylt, xc, yc;
	union pix *pd;
	int  oldxc, oldyc, bead_lost = 0, bd_mul = 8;
	int x[128], y[128];
	float x1[128], y1[128], fx1[128], fy1[128];
	float xcf, ycf, dx, dy, corr, *zp = NULL;
	int  done;
	clock_t start = 0, t0;
	O_i *oi;
	int  color;
	int	what_is_image_acquisition_number(void);
	int freeze_video();
	int live_video();
	
	
	pd = oid->im.pixel;
	xc = *xci;	yc = *yci;		
	xcf = xc; 	ycf = yc;
	oi = imrs->one_i;


	color = makecol(255,64,64);
	start = clock();
	n0 = what_is_image_acquisition_number() + 2;

	for (im = 0, wait_min = 1000, done = 0; im < (nf + imstart) && done == 0 ; im++ )
	{
		t0 = clock();
		ia = (im < imstart) ? 0 : im - imstart;
		
		ni = what_is_image_acquisition_number();
		for (t0 = clock(); ni < n0 + im; )
			ni = what_is_image_acquisition_number();
		wait = clock() - t0;
		
		if (oip != NULL)	zp = oip->im.pixel[ia].fl;
		ylt = yc - cw/2;		ytt = yc - cl/2;
		xll = xc - cl/2;		xlt = xc - cw/2;

		if (fill_avg_profiles_from_im(oi, xc, yc, cl, cw, x, y))
			win_printf("profil error");

		if ( im == imstart || wait < wait_min )	wait_min = wait;
		for (k = 0;(oip != NULL) && (k < cl); k++)	zp[k] = (float)y[k];

		if (key[KEY_ESC]) done = 1;

		for (k = 0;(oip != NULL) && (k < cl); k++)	zp[k+cl] = (float)y[k];
		
		if (wait < wait_min )	wait_min = wait;
		if (black_circle)		erase_around_black_circle(x, y, cl);
		bead_lost += check_bead_not_lost(x, x1, y, y1, cl, bd_mul);
		if (bead_lost > 10)		done = 2;
		
/*		for (i = 0; i < cl; i++)
			y1[i] = (oip != NULL) ? zp[i] + (float)j : (float)j;	*/
		for (k = 0;(oip != NULL) && (k < cl); k++)	zp[k+cl] = (float)y[k];
		correlate_1d_sig_and_invert(x1, cl, 0, fx1, cl>>filter_ratio, !black_circle);
		find_max1(fx1, cl, &dx,&corr);
		correlate_1d_sig_and_invert(y1, cl, 0, fy1, cl>>filter_ratio, !black_circle);
		find_max1(fy1, cl, &dy,&corr);
		dx -= cl/2;		dy -= cl/2;
		dx /=2;			dy /=2;
		
						
		xb[ia] = xcf = dx + xc;
		yb[ia] = ycf = yc + dy;
		oldxc = xc;		oldyc = yc;
		xc = (int)(xcf + .5);	yc = (int)(ycf + .5);  /* +((ia%8 <4)?0:1);*/
		for (k = 0;(oip != NULL) && (k < cl); k++)	
		{
			zp[k] = fx1[k];	
			zp[k+cl] = fy1[k];	
		}
		xc = (xc < oi->im.nx - cl/2) ? xc : oi->im.nx - cl/2;
		yc = (yc < oi->im.ny - cl/2) ? yc : oi->im.ny - cl/2;
		xc = (xc < cl/2) ? cl/2 : xc;
		yc = (yc < cl/2) ? cl/2 : yc;
		draw_cross_vga(xc, yc, cl, cw, color, imrs, screen);
	}
	start =  clock() - start;
	freeze_video();
	win_printf("wait min %d \n%d im in %g S\nsoit %g im/s",wait_min,nf,
		((float)start)/((float)CLOCKS_PER_SEC),((float)CLOCKS_PER_SEC*nf)/start);
	live_video();

	*xci = xc;
	*yci = yc;
	return (done); 
}



int follow_bead_in_x_y_from_ifc(void)
{
	register int i;
	imreg *imrs;	
	O_p *op;	
	O_i *ois, *oid, *oidp = NULL;
	union pix *pd;
	d_s *ds; 
	int xc = 256, yc = 256;
	static int cl = 64, cw = 16, nf = 2048, imstart = 10, prof = 0;
	int  black_circle = 0;
	int freeze_video();
	int live_video();
	
	if(updating_menu_state != 0)	return D_O_K;

	
	if (ac_grep(NULL,"%im%oi",&imrs,&ois) != 2)	
		return win_printf_OK("cannot find image in acreg!");
		
				
	/* check for menu in win_scanf */
	freeze_video();
	i = win_scanf("this routine track a bead in X,Y\n"
			" using a cross shaped pattern\narm length %d"
			"arm width %dhowmany frames %d imstart %d"
			"profile %dkey {\\it b} black circle {\\it w} to stop"
			,&cl,&cw,&nf,&imstart,&prof); /* %m ,select_power_of_2() */
	live_video();
	if (i == WIN_CANCEL)	return OFF; 
	if (cl > 128)	return win_printf_OK("cannot use arm bigger\nthan 128");
	if (cw > 32)	return win_printf_OK("cannot use width bigger\nthan 32");

	if (fft_init(cl) || filter_init(cl))	return win_printf_OK("cannot init fft!");	
	xc = ois->im.nx/2;	yc = ois->im.ny/2;
	freeze_video();	
	i = win_scanf("enter starting position in pixels \n xc %d yc %d",&xc,&yc);
	live_video();	
	if (i == WIN_CANCEL) 	return OFF;

	op = create_and_attach_op_to_oi(ois, nf, nf, 0,IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
	if (op == NULL)		return win_printf_OK("cannot create plot !");
	ds = op->dat[0];

	ds->source = my_sprintf(ds->source,"Bead tracking at 25 Hz Acquistion %d"
		" de 1 billes \n from movie l = %d, w = %d, nim = %d\nobjective %d,"
		" zoom factor %f\nIC-PCI %g MHz",0,cl,cw,nf,63,1.6,14.18);		
	uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
	uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
	set_plot_title(op, "Trajectoire 0 bis");

						
	
	op->filename = my_sprintf(op->filename,"tracs000d%s",".gr");
	if (prof)
	{
		oidp = create_and_attach_oi_to_imr(imrs, 2*cl, nf, IS_FLOAT_IMAGE);
		oidp->im.source = strdup("MOVIE bead profile");
	}
	oid = create_and_attach_oi_to_imr(imrs, cl, cl, IS_CHAR_IMAGE);
	if (oid == NULL)	
	{
		win_printf("Can't create dest image");
		return D_O_K;
	} 
	oid->im.source = strdup("IC-PCI ");
 	time(&(oid->im.time));
	pd = oid->im.pixel;
	inherit_from_im_to_im(oid, ois);
	uns_oi_2_oi(oid, ois);

	track_in_x_y_from_ifc(imrs, oid, &xc, &yc, cl, cw, nf, imstart, 
				black_circle, ds->xd, ds->yd, oidp);

	op->need_to_refresh = 1;	
	ois->need_to_refresh = PLOTS_NEED_REFRESH;											
 	oid->need_to_refresh = ALL_NEED_REFRESH;

	find_zmin_zmax(oid);	
	if (prof)		
	{
		find_zmin_zmax(oidp);	
		oidp->need_to_refresh = ALL_NEED_REFRESH;	
	}		
	return D_REDRAWME;
}
# endif





int follow_bead_in_x_y_from_movie_ortho(void)
{
	register int i;
	imreg *imrs;	
	O_p *op;	
	O_i *ois, *oidp = NULL;
	d_s *ds; 
	int xc = 256, yc = 256, color;
	static int cl = 128, cw = 16, nf = 2048, imstart = 10;
	static int prof_size = 64, prof_rad = 16, prof_width = 2;
	static float ax = 0, ay = 0;
	int  black_circle = 0;
	
	if(updating_menu_state != 0)	return D_O_K;
	
	if (ac_grep(NULL,"%im%oi",&imrs,&ois) != 2)	
		return win_printf_OK("cannot find image in acreg!");
	nf = ois->im.n_f;
	if (ay == 0)
	{
		ay = 1;
		ax = 14.31818/14.18;
	}

	color = makecol(255,64,64);
	/* check for menu in win_scanf */
	i = win_scanf("This routine track a bead in X,Y using a cross shaped pattern\n"
		      "arm length %6d arm width %6d howmany frames %8d\n" 
		      "imstart %6d profile size %6d radius %6d width %6d\n"
		      "pixel ratio in x %8F in y %8f\n"
		      "key {\\it b} black circle {\\it w} to stop"
		      ,&cl,&cw,&nf,&imstart,&prof_size,&prof_rad,&prof_width,&ax,&ay); 
	
	if (i == WIN_CANCEL)	return OFF; 
	if (cl > 128)	return win_printf_OK("cannot use arm bigger\nthan 128");
	if (cw > 32)	return win_printf_OK("cannot use width bigger\nthan 32");

	if (fft_init(cl) || filter_init(cl))	return win_printf_OK("cannot init fft!");	
	xc = ois->im.nx/2;	yc = ois->im.ny/2;
	i = win_scanf("enter starting position in pixels \n xc %d yc %d",&xc,&yc);
	if (i == WIN_CANCEL) 	return OFF;
	if (xc < 0 || yc < 0)
	{
		xc = ois->im.nx/2;	yc = ois->im.ny/2;
		place_tracking_cross_with_mouse_movie(&xc, &yc, cl, cw, color);
	}

	op = create_and_attach_op_to_oi(ois, nf, nf, 0,IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
	if (op == NULL)		return win_printf_OK("cannot create plot !");
	ds = op->dat[0];

	ds->source = my_sprintf(ds->source,"Bead tracking at 25 Hz Acquistion %d"
		" de 1 billes \n from movie l = %d, w = %d, nim = %d\nobjective %d,"
		" zoom factor %f\nIC-PCI %g MHz",0,cl,cw,nf,63,1.6,14.18);		
	uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
	uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
	set_plot_title(op, "Trajectoire 0 bis");

	op->filename = my_sprintf(op->filename,"tracs000d%s",".gr");
	oidp = create_and_attach_oi_to_imr(imrs, prof_size, nf, IS_FLOAT_IMAGE);
	oidp->im.source = strdup("MOVIE : orthoradial bead profile");


	track_in_x_y_from_movie_ortho(imrs, NULL, &xc, &yc, cl, cw, nf, imstart, 
				black_circle, ds->xd, ds->yd, oidp, prof_rad, prof_width, ax, ay);

	op->need_to_refresh = 1;	
	ois->need_to_refresh = PLOTS_NEED_REFRESH;
	if (ois->n_op) 	    ois->cur_op = ois->n_op - 1;
	find_zmin_zmax(oidp);	
	oidp->need_to_refresh = ALL_NEED_REFRESH;	
        return broadcast_dialog_message(MSG_DRAW,0);			
}


int follow_bead_in_x_y_from_movie_ortho_movie(void)
{
	register int i;
	imreg *imrs;	
	O_p *op;	
	O_i *ois, *oidp = NULL;
	d_s *ds; 
	int xc = 256, yc = 256, color;
	static int cl = 128, cw = 16, nf = 2048, imstart = 10;
	static int prof_size = 64, prof_rad = 32;
	static float ax = 0, ay = 0;
	int  black_circle = 0;
	
	if(updating_menu_state != 0)	return D_O_K;
	
	if (ac_grep(NULL,"%im%oi",&imrs,&ois) != 2)	
		return win_printf_OK("cannot find image in acreg!");
	nf = ois->im.n_f;
	if (ay == 0)
	{
		ay = 1;
		ax = 14.31818/14.18;
	}

	color = makecol(255,64,64);
	/* check for menu in win_scanf */
	i = win_scanf("This routine track a bead in X,Y using a cross shaped pattern\n"
		      "arm length %6d arm width %6d howmany frames %8d\n" 
		      "imstart %6d profile size %6d radius %6d\n"
		      "pixel ratio in x %8F in y %8f\n"
		      "key {\\it b} black circle {\\it w} to stop"
		      ,&cl,&cw,&nf,&imstart,&prof_size,&prof_rad,&ax,&ay); 
	
	if (i == WIN_CANCEL)	return OFF; 
	if (cl > 128)	return win_printf_OK("cannot use arm bigger\nthan 128");
	if (cw > 32)	return win_printf_OK("cannot use width bigger\nthan 32");

	if (fft_init(cl) || filter_init(cl))	return win_printf_OK("cannot init fft!");	
	xc = ois->im.nx/2;	yc = ois->im.ny/2;
	i = win_scanf("enter starting position in pixels \n xc %d yc %d",&xc,&yc);
	if (i == WIN_CANCEL) 	return OFF;
	if (xc < 0 || yc < 0)
	{
		xc = ois->im.nx/2;	yc = ois->im.ny/2;
		place_tracking_cross_with_mouse_movie(&xc, &yc, cl, cw, color);
	}

	op = create_and_attach_op_to_oi(ois, nf, nf, 0,IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
	if (op == NULL)		return win_printf_OK("cannot create plot !");
	ds = op->dat[0];

	ds->source = my_sprintf(ds->source,"Bead tracking at 25 Hz Acquistion %d"
		" de 1 billes \n from movie l = %d, w = %d, nim = %d\nobjective %d,"
		" zoom factor %f\nIC-PCI %g MHz",0,cl,cw,nf,63,1.6,14.18);		
	uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
	uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
	set_plot_title(op, "Trajectoire 0 bis");

	op->filename = my_sprintf(op->filename,"tracs000d%s",".gr");

	oidp = create_and_attach_movie_to_imr(imrs, prof_size, prof_rad, IS_FLOAT_IMAGE, nf);
	oidp->im.source = strdup("MOVIE : orthoradial bead profile movie");


	track_in_x_y_from_movie_build_ortho_movie(imrs, NULL, &xc, &yc, cl, cw, nf, imstart, 
				black_circle, ds->xd, ds->yd, oidp, ax, ay);

	op->need_to_refresh = 1;	
	ois->need_to_refresh = PLOTS_NEED_REFRESH;
	if (ois->n_op) 	    ois->cur_op = ois->n_op - 1;
	find_zmin_zmax(oidp);	
	oidp->need_to_refresh = ALL_NEED_REFRESH;	
        return broadcast_dialog_message(MSG_DRAW,0);			
}



imreg *load_z_calibration_image(imreg *imr, O_i **oi, int ref)
{
	register int i;
	char path[512], file[256];
	char fullfile[512];
	const char *fui = NULL;
	/*
	int load_im_file_in_imreg(imreg *imr, char *file, char *path);
	int d_draw_Im_proc(int msg, DIALOG *d, int c);
	imreg	*create_and_register_new_image_project(int x, int y, int w, int h);
	*/
	
	*oi = NULL;	
	switch_allegro_font(1);
	if (ref == 0)
	  fui = (const char*)get_config_string("PICOTWIST","last_bead_calibration_image",NULL);
	else 
	{
	  fui = (const char*)get_config_string("PICOTWIST","last_ref_calibration_image",NULL);
	  if (fui == NULL)	
	    fui = (const char*)get_config_string("PICOTWIST","last_bead_calibration_image",NULL);
	}  
	if (fui != NULL)	
	{
		strcpy(fullfile,fui);
		fui = fullfile;
	}
	else
	{
		my_getcwd(fullfile, 512);
		strcat(fullfile,"\\");
	}
        i = file_select_ex("Load calibration image (*.gr)", fullfile, "gr", 512, 0, 0);
	if (i != 0) 	
	{
		fui = fullfile;
		if (ref == 0)
		  set_config_string("PICOTWIST","last_bead_calibration_image",fullfile);
		else set_config_string("PICOTWIST","last_ref_calibration_image",fullfile);
		extract_file_name(file, 256, fullfile);
		extract_file_path(path, 512, fullfile);
		strcat(path,"\\");
		imr = find_imr_in_current_dialog(NULL);
		if (imr == NULL)	
		{
			imr = create_and_register_new_image_project( 0,   32,  900,  668);
		}
		if (imr == NULL)	
		{
			win_printf("could not create imreg");		
			return NULL;
		}		
		else load_im_file_in_imreg(imr, file, path);
		*oi = imr->one_i; 
	}
	switch_allegro_font(0);	
	broadcast_dialog_message(MSG_DRAW,0);
	return imr;
}



int follow_bead_in_x_y_z_amp_phi_not_power_2_from_movie(void)
{
	register int i;
	pltreg *pr;
	imreg *imrs = NULL;	
	O_p *op = NULL, *oper;	
	O_i *ois = NULL, *oir = NULL, *oidp = NULL, *oiro = NULL;
	d_s *ds = NULL, *dsz = NULL, *dser = NULL; 
	int xc = 256, yc = 256, index;
	static float  ax = 0, ay = 0;
	static int cl = 128, cw = 16, nf = 2048, imstart = 10, npc = 128, rc = 22;
	static int bp_center = 16, bp_width = 12, nstep = 10, zflag = 0;
	static float zstep = 0.1;
	int black_circle = 0, radial_profile = 0;
	sym_radial_profile *srp = NULL;
	unsigned int *it = NULL;
	LARGE_INTEGER freq;
	double dfreq;
	


	if(updating_menu_state != 0)	return D_O_K;
	index = RETRIEVE_MENU_INDEX;

	if (ac_grep(cur_ac_reg,"%im%oi",&imrs,&ois) != 2)	
		return win_printf_OK("cannot find image in acreg!");
	if (ay == 0)
	{
		ay = 1;
		ax = 14.31818/14.18;
	}
	if (index == 2)	radial_profile = 1;
	
	nf = ois->im.n_f;
	
	i = win_scanf("this routine tracks a bead in X,Y,Z using a cross shaped pattern\n"
		      "arm length %4d arm width %4d nb. of frames %6d\n"
		      "imstart %3d nb. of pts in profile %4d ax/ay %6f\n"
		      "a(x et y) %6f key {\\it b} black circle {\\it w} to stop\n"
		      "band pass center %4d band pass width %4d critical r %4d\n"
		      "step in Z %b (if yes) step in obj %4f nb of step %4d\n",
		      &cl,&cw,&nf,&imstart,&npc,&ax,&ay,&bp_center,&bp_width,&rc,&zflag,&zstep,&nstep);	

	if (i == WIN_CANCEL)	return OFF; 
	if (cl > 128)	return win_printf_OK("cannot use arm bigger\nthan 128");
	if (cw > 32)	return win_printf_OK("cannot use width bigger\nthan 32");

	if (fft_init(npc) || filter_init(npc))	
		return win_printf_OK("cannot init fft!");	
		
	srp = (sym_radial_profile*)calloc(1,sizeof(sym_radial_profile));
	if (srp == NULL)		return win_printf_OK("cannot create srp !");
	srp->ax = ax*ay;
	srp->ay = ay;
	srp->npc = npc;
	srp->pz = NULL;
	
	srp->bp_center = bp_center;
	srp->bp_width = bp_width;
	srp->rc = rc;

	i = imrs->cur_oi;
	if ((load_z_calibration_image(imrs, &oiro, 0) == NULL) || (oiro == NULL))
		return win_printf_OK("Can't load calibration image");
	oir = image_band_pass_in_real_out_complex(NULL, oiro, 0, srp->bp_center, 
						  srp->bp_width, srp->rc = rc);

	if (oir == NULL)	return win_printf_OK("Can't compute calibration image");
	pr = create_pltreg_with_op(&op, nf,  nf, 0);
	if (switch_project_to_this_imreg(imrs))
	  return win_printf_OK("cannot switch back to image !");

	select_image_of_imreg_and_display(imrs, i);

	if (pr == NULL || op == NULL)	return win_printf_OK("cannot create plot !");
	ds = op->dat[0];

	ds->source = my_sprintf(ds->source,"Bead tracking at 25 Hz Acquistion %d de 1 "
				"billes \n from movie l = %d, w = %d, nim = %d\n"
				"objective %d, zoom factor %f\nIC-PCI %g MHz",
				-1,cl,cw,nf,63,1.6,14);
		
	uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
	uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
	set_plot_title(op, "Trajectoire %d bis", -1);
	
	op->filename = my_sprintf(op->filename,"tracs%03d%s",999,".gr");
	op->need_to_refresh = 1;
	if ((op = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
		return  win_printf("cannot create plot !");

	dsz = op->dat[0];
	create_attach_select_x_un_to_op(op, IS_SECOND, 0, .04, 0, 0, "s");	
	create_attach_select_y_un_to_op(op, IS_METER, 0, 1, -6, 0, "\\mu m");
	
	dsz->source = my_sprintf(dsz->source,"Bead tracking at 25 Hz Acquistion %d de 1 "
				 "billes \n from movie l = %d, w = %d, nim = %d\n"
				 "objective %d, zoom factor %f\nIC-PCI %g MHz",
				 999,cl,cw,nf,63,1.6,14);

	if ((oper = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
		return  win_printf("cannot create plot !");
	dser = oper->dat[0];		
						
	for (i = 0; i < nf; i++)
		dser->xd[i] = dsz->xd[i] = i;
	
	if (radial_profile)
	{
		oidp = create_and_attach_oi_to_imr(imrs, 2*cl, nf, IS_FLOAT_IMAGE);
		if (oidp == NULL)	return win_printf_OK("cannot create profile !");
		oidp->im.source = strdup("MOVIE bead profile");
	}

	xc = ois->im.nx/2;	yc = ois->im.ny/2;
	i = win_scanf("enter starting position in pixels \n xc %d yc %d",&xc,&yc);
	if (i == WIN_CANCEL) 	return OFF;
	track_in_x_y_z_amp_phi_not_power_2_from_movie(imrs, NULL, &xc, &yc, cl, cw, nf, 
		 imstart, black_circle,  ds->xd, ds->yd, srp, dsz->yd,(zflag)?2:1,
		oir,dser->yd,index,oidp);

	QueryPerformanceFrequency(&freq);
	dfreq = (double)(freq.QuadPart);
	dfreq /= 16;
	for (i = 0, it = (unsigned int *)dser->yd; i < nf; i++)
	{
		dsz->yd[i] =  -dsz->yd[i];
		dser->yd[i] = (float)(((double)it[i])/dfreq);
		dsz->xd[i] = dser->xd[i] = i;
	}
	for (i = nf - 1; i > 0; i--) dser->yd[i] -= dser->yd[i-1];

	op->need_to_refresh = 1;
	oper->need_to_refresh = 1;
	pr->cur_op = 0;
	pr->one_p = pr->o_p[pr->cur_op];
	pr->stack = &(pr->one_p->bplt);	
	do_one_plot(pr);
	free_one_image(oir);
	/*
	if (switch_project_to_this_pltreg(pr))
	return win_printf_OK("cannot switch back to plot !");	*/
	//remove oiro
	free(srp);	
	if (radial_profile)
	{
	        oidp->need_to_refresh = ALL_NEED_REFRESH;
		find_zmin_zmax(oidp);	
	}

	broadcast_dialog_message(MSG_DRAW,0);
	return 0;
}


int follow_bead_in_x_y_z_amp_phi_not_power_2_from_movie_with_plot(void)
{
	register int i;
	pltreg *pr;
	imreg *imrs = NULL;	
	O_p *op = NULL, *oper;	
	O_i *ois = NULL, *oir = NULL, *oidp = NULL, *oiro = NULL;
	d_s *ds = NULL, *dsz = NULL, *dser = NULL; 
	int xc = 256, yc = 256, index, h;
	static float  ax = 0, ay = 0;
	static int cl = 128, cw = 16, nf = 2048, imstart = 10, npc = 128, rc = 22;
	static int bp_center = 16, bp_width = 12, nstep = 10, zflag = 0;
	static float zstep = 0.1;
	int black_circle = 0;
	sym_radial_profile *srp = NULL;
	unsigned int *it = NULL;
	DIALOG *dip = NULL, *di = NULL;
	LARGE_INTEGER freq;
	double dfreq;
	
	if(updating_menu_state != 0)	return D_O_K;
	index = RETRIEVE_MENU_INDEX;

	if (ac_grep(cur_ac_reg,"%im%oi",&imrs,&ois) != 2)	
		return win_printf_OK("cannot find image in acreg!");
	if (ay == 0)
	{
		ay = 1;
		ax = 14.31818/14.18;
	}
	
	nf = ois->im.n_f;
	
	i = win_scanf("this routine tracks a bead in X,Y,Z using a cross shaped pattern\n"
		      "arm length %4d arm width %4d nb. of frames %6d\n"
		      "imstart %3d nb. of pts in profile %4d ax/ay %6f\n"
		      "a(x et y) %6f key {\\it b} black circle {\\it w} to stop\n"
		      "band pass center %4d band pass width %4d critical r %4d\n"
		      "step in Z %b (if yes) step in obj %4f nb of step %4d\n",
		      &cl,&cw,&nf,&imstart,&npc,&ax,&ay,&bp_center,&bp_width,&rc,&zflag,&zstep,&nstep);	

	if (i == WIN_CANCEL)	return OFF; 
	if (cl > 128)	return win_printf_OK("cannot use arm bigger\nthan 128");
	if (cw > 32)	return win_printf_OK("cannot use width bigger\nthan 32");

	if (fft_init(npc) || filter_init(npc))	
		return win_printf_OK("cannot init fft!");	
		
	srp = (sym_radial_profile*)calloc(1,sizeof(sym_radial_profile));
	if (srp == NULL)		return win_printf_OK("cannot create srp !");
	srp->ax = ax*ay;
	srp->ay = ay;
	srp->npc = npc;
	srp->pz = NULL;
	
	srp->bp_center = bp_center;
	srp->bp_width = bp_width;
	srp->rc = rc;

	i = imrs->cur_oi;
	if ((load_z_calibration_image(imrs, &oiro, 0) == NULL) || (oiro == NULL))
		return win_printf_OK("Can't load calibration image");
	oir = image_band_pass_in_real_out_complex(NULL, oiro, 0, srp->bp_center, 
						  srp->bp_width, srp->rc = rc);

	if (oir == NULL)	return win_printf_OK("Can't compute calibration image");
	select_image_of_imreg_and_display(imrs, i);
        broadcast_dialog_message(MSG_DRAW,0);
	i = find_imr_index_in_current_dialog(the_dialog);
	if (i >= 0)
	  {	
	    di = the_dialog + i;
	    set_dialog_size_and_color(di, 0, 19, 400, h = di->h, 255, 0);
	  }
	else 	    win_printf_OK("Cannot find Im dialog");

        broadcast_dialog_message(MSG_DRAW,0);
	i = retrieve_index_of_menu_from_dialog();
	if (i < 0)     win_printf_OK("Cannot find menu dialog");
	pr = find_pr_in_current_dialog(NULL);
	if (di != NULL && di->dp != NULL && pr == NULL)
	{
		remove_all_keyboard_proc();	
		dip = attach_new_plot_region_to_dialog( 0, 0, 0, 0);
		if (dip == NULL)        allegro_message("dialog pb");
		set_dialog_size_and_color(dip, 400, 19, 500, di->h, 255, 0);
		scan_and_update((MENU*)di->dp);
	}
	if (pr == NULL)	   
	  {
	    pr = build_plot_region(0,   32,  500,  668);
	    if (pr == NULL)		
	      {
		show_mouse(screen);
		return win_printf_OK("Could not find or allocte plot region!");
	      }
	    pr->auto_scale = 1;    
	    op = create_one_plot(nf,  nf, 0);
	    ds = op->dat[0];
	    add_data( pr, IS_ONE_PLOT, (void*)op);    
	    remove_data (pr, IS_ONE_PLOT, (void *)pr->o_p[0]);
	    pr->cur_op = pr->n_op - 1;
	    pr->one_p = pr->o_p[pr->cur_op];
	    dip->dp = pr;
	    if (pr == NULL || op == NULL)	return win_printf_OK("cannot create plot !");
	  }
	else
	  {
	    if ((op = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
		return  win_printf("cannot create plot !");
	    ds = op->dat[0];		

	  }
        broadcast_dialog_message(MSG_DRAW,0);
	ds->source = my_sprintf(ds->source,"Bead tracking at 25 Hz Acquistion %d de 1 "
				"billes \n from movie l = %d, w = %d, nim = %d\n"
				"objective %d, zoom factor %f\nIC-PCI %g MHz",
				-1,cl,cw,nf,63,1.6,14);
		
	uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
	uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
	set_plot_title(op, "Trajectoire %d bis", -1);
	
	op->filename = my_sprintf(op->filename,"tracs%03d%s",999,".gr");
	op->need_to_refresh = 1;
	if ((op = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
		return  win_printf("cannot create plot !");

	dsz = op->dat[0];
	create_attach_select_x_un_to_op(op, IS_SECOND, 0, .04, 0, 0, "s");	
	create_attach_select_y_un_to_op(op, IS_METER, 0, 1, -6, 0, "\\mu m");
	
	dsz->source = my_sprintf(dsz->source,"Bead tracking at 25 Hz Acquistion %d de 1 "
				 "billes \n from movie l = %d, w = %d, nim = %d\n"
				 "objective %d, zoom factor %f\nIC-PCI %g MHz",
				 999,cl,cw,nf,63,1.6,14);

	if ((oper = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
		return  win_printf("cannot create plot !");
	dser = oper->dat[0];		
						
	for (i = 0; i < nf; i++)
		dser->xd[i] = dsz->xd[i] = i;
	pr->cur_op = 1;
	pr->one_p = pr->o_p[pr->cur_op];
	pr->stack = &(pr->one_p->bplt);	
	do_one_plot(pr);
	
	xc = ois->im.nx/2;	yc = ois->im.ny/2;
	i = win_scanf("enter starting position in pixels \n xc %d yc %d",&xc,&yc);
	if (i == WIN_CANCEL) 	return OFF;
	track_in_x_y_z_amp_phi_not_power_2_from_movie(imrs, NULL, &xc, &yc, cl, cw, nf, 
		 imstart, black_circle,  ds->xd, ds->yd, srp, dsz->yd,(zflag)?2:1,
		oir,dser->yd,index,oidp);

	QueryPerformanceFrequency(&freq);
	dfreq = (double)(freq.QuadPart);
	dfreq /= 16;

	for (i = 0, it = (unsigned int *)dser->yd; i < nf; i++)
	{
		dsz->yd[i] =  -dsz->yd[i];
		dser->yd[i] = (float)(((double)it[i])/dfreq);
		dsz->xd[i] = dser->xd[i] = i;
	}
	for (i = nf - 1; i > 0; i--) dser->yd[i] -= dser->yd[i-1];

	op->need_to_refresh = 1;
	oper->need_to_refresh = 1;
	pr->cur_op = 0;
	pr->one_p = pr->o_p[pr->cur_op];
	pr->stack = &(pr->one_p->bplt);	
	do_one_plot(pr);
	free_one_image(oir);
	free(srp);	
	broadcast_dialog_message(MSG_DRAW,0);
	return 0;
}




int	do_movie(void)
{
	register int i, j, k;
	static int nx = 200, ny = 200, nf = 16, amp = 64, ortho = 0;
	static float sh = 5, rmax = 32, per = 8, w = 10;
	float xc, yc, x, y, tmp, theta;
	imreg *imr;
	O_i *oi;	
	O_p *op, *op2;
	d_s *ds, *ds2;		

	if(updating_menu_state != 0)	return D_O_K;
	
	i = win_scanf("creating a movie with a shifted object. Movie with\n"
		      "nx %5d ny %5d nb of frames %5d\nBurst of size (float) %f"
		      "and amp in z %d period in frames(float) %f"
		      "amp of shift %frmax %f\n"
		      "orthoradial modulation (0 no) %4d\n"
		      ,&nx,&ny,&nf,&w,&amp,&per,&sh,&rmax, &ortho);
	if (i == WIN_CANCEL)	return OFF;
	if (ac_grep(cur_ac_reg,"%im",&imr) == 1) 
	{
	    oi = create_and_attach_movie_to_imr(imr, nx, ny, IS_CHAR_IMAGE, nf);
	    imr->cur_oi = imr->n_oi - 1;
	    imr->one_i = imr->o_i[imr->cur_oi];
	}
	else imr = create_imreg_with_movie(nx, ny ,IS_CHAR_IMAGE, nf);
	if (imr == NULL || ((oi = imr->one_i) == NULL))	
		return win_printf_OK("cannot find image!");
	op = create_and_attach_op_to_oi(oi, nf, nf, 0, IM_SAME_X_AXIS | IM_SAME_Y_AXIS);
	ds = op->dat[0];		
	ds->source = my_sprintf(ds->source,"Bead tracking at 25 Hz \n"
			"faked data nx %d ny %d nb of frames %dsize %d "
			"amp in z %d period in frames %f amp of shift %frmax %f",
			nx,ny,nf,w,amp,per,sh,rmax);	

	op2 = create_and_attach_op_to_oi(oi, nx, nx, 0, 0);
	ds2 = op2->dat[0];		
	ds2->source = my_sprintf(ds2->source,"Bead tracking at 25 Hz \n"
			 "faked data nx %d ny %d nb of frames %dsize %d "
			 "amp in z %d period in frames %f amp of shift %frmax %f",
			 nx,ny,nf,w,amp,per,sh,rmax);	
	for (j = 0; j < nx; j++)
	{
		x = (j - nx/2);
		tmp = sqrt(x*x);
		if (tmp > rmax) tmp = w;
		tmp = tmp*M_PI/w;
		tmp = (tmp != 0) ? sin(tmp)/tmp : 1;
		ds2->yd[j] = (128 + amp * tmp);
		ds2->xd[j] = j;
	}
	for (k = 0; k < nf; k++)
	{
		switch_frame(oi,k);
		xc = sh * sin(2*k*M_PI/per) + nx/2;
		yc = sh * cos(2*k*M_PI/per) + ny/2;
		ds->xd[k] = xc;
		ds->yd[k] = yc;		
		for(i = 0; i < ny; i++)
		{
			for (j = 0; j < nx; j++)
			{
				x = ((float)j - xc);
				y = ((float)i - yc);
				tmp = sqrt(x*x+y*y);
				if (tmp > rmax) tmp = w;
				tmp = tmp*M_PI/w;
				tmp = (tmp != 0) ? sin(tmp)/tmp : 1;
				theta = ((float)ortho)*atan2(y,x)/M_PI;
				oi->im.pixel[i].ch[j] = (unsigned char)(128.5 + theta + tmp * amp);
			}
		}
	}

	oi->im.source = Mystrdupre(oi->im.source,"movie test");
	oi->need_to_refresh = ALL_NEED_REFRESH;		
	find_zmin_zmax(oi);	
	do_one_image(imr);
	broadcast_dialog_message(MSG_DRAW,0);
	return D_O_K;
}
int	do_movie_odd_even(void)
{
	register int i, j, k;
	static int nx = 80, ny = 80, nf = 16, w = 10, amp = 64;
	static float sh = 5, rmax = 32, per = 8;
	float xc, yc, x, y, tmp;
	imreg *imr;
	O_i *oi;
	O_p *op;
	d_s *ds;	

	if(updating_menu_state != 0)	return D_O_K;
	i = win_scanf("create an odd even movie with shifted object\n"
		      "nx %d ny %d nb of frames %dsize %d amp in z %d "
		      "period in frames(float) %f amp of shift %frmax %f"
		      ,&nx,&ny,&nf,&w,&amp,&per,&sh,&rmax);
	if (i == WIN_CANCEL)	return OFF;
	
	if (ac_grep(cur_ac_reg,"%im",&imr) == 1)
	{
	    oi = create_and_attach_movie_to_imr(imr, nx, ny, IS_CHAR_IMAGE, nf);
	    imr->cur_oi = imr->n_oi - 1;
	    imr->one_i = imr->o_i[imr->cur_oi];
	}
	else imr = create_imreg_with_movie(nx, ny ,IS_CHAR_IMAGE, nf);
	if (imr == NULL || ((oi = imr->one_i) == NULL))	
		return win_printf_OK("cannot find image!");
	op = create_and_attach_op_to_oi(oi, 2*nf, 2*nf, 0, IM_SAME_X_AXIS | IM_SAME_Y_AXIS);
	ds = op->dat[0];
	ds->source = my_sprintf(ds->source,"Bead tracking at 50 Hz\n"
				"faked data nx %d ny %d nb of frames %dsize %d "
				"amp in z %d period in frames %f amp of shift %frmax %f"
				,nx,ny,nf,w,amp,per,sh,rmax);		
	for (k = 0; k < nf; k++)
	{
		switch_frame(oi,k);
		xc = sh * sin(2*k*M_PI/per) + nx/2;
		yc = sh * cos(2*k*M_PI/per) + ny/2;
		ds->xd[2*k] = xc;
		ds->yd[2*k] = yc;
		for(i = 1; i < ny; i+=2)
		{
			for (j = 0; j < nx; j++)
			{
				x = (j - xc);
				y = (i - yc);
				tmp = sqrt(x*x+y*y);
				if (tmp > rmax) tmp = w;
				tmp = tmp*M_PI/w;
				tmp = (tmp != 0) ? sin(tmp)/tmp : 1;
				oi->im.pixel[i].ch[j] = (unsigned char)(128.5 + tmp * amp);
			}
		}
		xc = sh * sin(2*(0.5+k)*M_PI/per) + nx/2;
		yc = sh * cos(2*(0.5+k)*M_PI/per) + ny/2;
		ds->xd[2*k+1] = xc;
		ds->yd[2*k+1] = yc;		
		for(i = 0; i < ny; i+=2)
		{
			for (j = 0; j < nx; j++)
			{
				x = (j - xc);
				y = (i - yc);
				tmp = sqrt(x*x+y*y);
				if (tmp > rmax) tmp = w;
				tmp = tmp*M_PI/w;
				tmp = (tmp != 0) ? sin(tmp)/tmp : 1;
				oi->im.pixel[i].ch[j] = (unsigned char)(128.5 + tmp * amp);
			}
		}		
	}
	oi->im.source = Mystrdupre(oi->im.source,"movie test");
	oi->need_to_refresh = ALL_NEED_REFRESH;		
	find_zmin_zmax(oi);	
	do_one_image(imr);
	broadcast_dialog_message(MSG_DRAW,0);
	return D_O_K;
}

int retrieve_item_from_dialog1(int (*proc)(int, DIALOG *, int), void* dp)
{
	register int i;
	
	if (the_dialog == NULL) return -1;
	for (i = 0; (the_dialog[i].proc != NULL) && (i < m_the_dialog); i++)
	{
		if (dp == NULL)
		{
			if (the_dialog[i].proc == proc) break;
			else win_printf("not found in dialog %d\n%u %u",i,(unsigned int)the_dialog[i].proc,(unsigned int)proc );
		}
		else
		{
			if ((the_dialog[i].proc == proc) && (the_dialog[i].dp == dp)) break;
		}
	}
	return (the_dialog[i].proc == NULL) ? -1 : i;
}

int test_idle_action(O_i *oi, DIALOG *d)
{
  register int i, j;
  d_s *ds;		
  long long dt;
  
  if (idle_op == NULL) return 0;
  ds = idle_op->dat[0];
  for (j = 0; j == 0; )
    {
      dt = my_ulclock() - idle_t0;
      i = dt/idle_dt;
      if (i >= 0 && i < ds->nx) ds->yd[i] += 1;
      else
	{
	  idle_op = NULL;
	  for (i = 0; i < ds->nx; i++)
	    ds->xd[i] = (float)i/10000;
	  oi->oi_idle_action = NULL;
	  if (d->dp == NULL)    return 0;
	  return refresh_im_plot((imreg*)d->dp, oi->n_op - 1);	
	  j = 1;
	}
    }
  return 1;
}

int do_check_timing(void)
{
  register int i;
  imreg *imr;
  O_i *ois;
  O_p *op;
  d_s *ds;		
  static int nf = 131072;
  long long dt;
  
  if(updating_menu_state != 0)	return D_O_K;
  
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
  i = win_scanf("how many points in ds %d",&nf);
  if (i == WIN_CANCEL)  return D_O_K;
  op = create_and_attach_op_to_oi(ois, nf, nf, 0, 0);
  ds = op->dat[0];
  idle_t0 = my_ulclock(); 
  if (idle_dt == 0) idle_dt = MY_ULCLOCKS_PER_SEC/10000;
  //ois->oi_idle_action = test_idle_action;

  for (i = 0; i < ds->nx; )
    {
      dt = my_ulclock() - idle_t0;
      i = dt/idle_dt;
      if (i >= 0 && i < ds->nx) ds->yd[i] += 1;
    }
  for (i = 0; i < ds->nx; i++)
    ds->xd[i] = (float)i/10000;
  return refresh_im_plot(imr, ois->n_op - 1);	
}


int do_radial(void)
{
	register int i;
	imreg *imr;
	O_i *ois;
	O_p *op;
	d_s *ds, *ds2;		
	static float xc = 256 , yc = 256, ax = 1, ay = 1;
	static int r_max = 64;
	unsigned long u_micro;
	int t, h = 668;
	union f4vector a, b, c;
	DIALOG *di, *dip = NULL, *dim = NULL;
	pltreg *pr;
	
	if(updating_menu_state != 0)	return D_O_K;

	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
	
	//i = find_dialog_focus(the_dialog); 	win_printf("focus on %d",i);
	i = find_imr_index_in_current_dialog(the_dialog);
	//i = retrieve_item_from_dialog1(d_draw_Im_proc, NULL);
	if (i >= 0)
	  {	
	    di = the_dialog + i;
	    set_dialog_size_and_color(di, 0, 19, 500, h = di->h, 255, 0);
	  }
	else 
	  {
	    win_printf_OK("Cannot find Im dialog");
	    di = the_dialog + 2;
	    set_dialog_size_and_color(di, 0, 19, 500, h = di->h, 255, 0);
	  }
        broadcast_dialog_message(MSG_DRAW,0);
	i = retrieve_index_of_menu_from_dialog();
	//i = retrieve_item_from_dialog1(d_menu_proc, NULL);
	if (i < 0)	
	  {
	    win_printf_OK("Cannot find menu dialog");;
	    di = the_dialog + 1;
	  }
	if (di != NULL && di->dp != NULL)
	{
		remove_all_keyboard_proc();	
		dip = attach_new_plot_region_to_dialog( 0, 0, 0, 0);
		//dip = attach_new_item_to_dialog(d_draw_Op_proc, 0, 0, 0, 0);
		if (dip == NULL)    
		    allegro_message("dialog pb");
		set_dialog_size_and_color(dip, 500, 19, 400, 400, 255, 0);
		dim = attach_new_image_region_to_dialog( 0, 0, 0, 0);
		if (dim == NULL)    
		    allegro_message("dialog pb");
		set_dialog_size_and_color(dim, 500, 419, 400, 300, 255, 0);


		scan_and_update((MENU*)di->dp);
	}
	xc = ois->im.nx/2;
	yc = ois->im.ny/2;
	i = win_scanf("center xc %f yc %f r_{max} %dax %f ay %f",&xc,&yc,&r_max,&ax,&ay);
	if (i == WIN_CANCEL)	return OFF;


         pr = build_plot_region(0,   32,  400,  668);

	 if (pr == NULL)		
	   {
	     show_mouse(screen);
	     return win_printf_OK("Could not find or allocte plot region!");
	   }
	 pr->auto_scale = 1;    

	op = create_one_plot(2*r_max, 2*r_max, 0);
//op = create_and_attach_op_to_oi(ois, 2*r_max, 2*r_max, 0, 0);
	ds = op->dat[0];
	//op->width = 0.5;

	 add_data( pr, IS_ONE_PLOT, (void*)op);    
	 remove_data (pr, IS_ONE_PLOT, (void *)pr->o_p[0]);
	 pr->cur_op = pr->n_op - 1;
	 pr->one_p = pr->o_p[pr->cur_op];
        dip->dp = pr;
        dim->dp = imr;

	u_micro = my_uclock();
	for (i = 0; i < 100; i++)
	  radial_non_pixel_square_image_sym_profile_in_array(ds->yd, ois, xc, yc, r_max, ax, ay);
	t = (int)(my_uclock() - u_micro);
	win_printf("dt = %g \\mu ms", 10000*((double)t/((int)MY_UCLOCKS_PER_SEC)));

	u_micro = my_uclock();
	for (i = 0; i < 100; i++)
	  radial_non_pixel_square_image_sym_profile_in_array_sse(ds->yd, ois, xc, yc, r_max, ax, ay);
	t = (int)(my_uclock() - u_micro);
	win_printf("dt sse = %g \\mu ms", 10000*((double)t/((int)MY_UCLOCKS_PER_SEC)));


	ds2 = create_and_attach_one_ds(op, 2*r_max, 2*r_max, 0);
	for (i = 0; i < r_max; i++)		
		ds2->yd[r_max-i] = ds2->yd[r_max+i] = radial_dx[i];
	
	set_plot_title(op,"do radial fixed dt = %g \\mu ms", 
		        10000*((double)t/((int)MY_UCLOCKS_PER_SEC)));
	ds->source = Mystrdupre(ds->source,op->title);	
	for (i = 0; i < 2*r_max; i++) ds2->xd[i] = ds->xd[i] = (i-r_max)/ay;
        broadcast_dialog_message(MSG_DRAW,0);
	(refresh_im_plot(imr, ois->n_op - 1));	


	a.f[0] = 1; a.f[1] = 2; a.f[2] = 3; a.f[3] = 4;
	b.f[0] = 5; b.f[1] = 6; b.f[2] = 7; b.f[3] = 8;
	u_micro = my_uclock();
	for (i = 0; i < 100000; i++)
	  c.v = a.v + b.v;
	t = (int)(my_uclock() - u_micro);
	win_printf("dt = %g \\mu ms", 1000000*((double)t/((int)MY_UCLOCKS_PER_SEC)));
	win_printf("%f, %f, %f, %f\n", c.f[0], c.f[1], c.f[2], c.f[3]);
	c.v = __builtin_ia32_sqrtps(c.v);
	win_printf("%f, %f, %f, %f\n", c.f[0], c.f[1], c.f[2], c.f[3]);
	return 0;
}



int do_radial_2(void)
{
  register int i;
  imreg *imr;
  O_i *ois;
  O_p *op;
  d_s *ds, *ds2;		
  static float xc = 256 , yc = 256, ax = 1, ay = 1;
  static int r_max = 64;


  
  if(updating_menu_state != 0)	return D_O_K;
  
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;

  i = win_scanf("center xc %f yc %f r_{max} %dax %f ay %f",&xc,&yc,&r_max,&ax,&ay);
  if (i == WIN_CANCEL)	return OFF;


  
  op = create_and_attach_op_to_oi(ois, 2*r_max, 2*r_max, 0, 0);
  ds = op->dat[0];
  radial_non_pixel_square_image_sym_profile_in_array(ds->yd, ois, xc, yc, r_max, ax, ay);
  //radial_non_pixel_square_image_sym_profile_in_array_sse(ds->yd, ois, xc, yc, r_max, ax, ay);

  
  ds2 = create_and_attach_one_ds(op, 2*r_max, 2*r_max, 0);
  for (i = 0; i < r_max; i++)		
    ds2->yd[r_max-i] = ds2->yd[r_max+i] = radial_dx[i];
  
  set_plot_title(op,"do radial fixed");
  ds->source = Mystrdupre(ds->source,op->title);	
  for (i = 0; i < 2*r_max; i++) ds2->xd[i] = ds->xd[i] = (i-r_max)/ay;
  broadcast_dialog_message(MSG_DRAW,0);
  return refresh_im_plot(imr, ois->n_op - 1);	
}




int do_radial_fixed(void)
{
	register int i;
	imreg *imr;
	O_i *ois;
	O_p *op;
	d_s *ds;		
	static float xc = 256 , yc = 256, ax = 1, ay = 1;
	static int r_max = 64;
	unsigned long u_micro;
	int t;
	
	if(updating_menu_state != 0)	return D_O_K;
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
	xc = ois->im.nx/2;
	yc = ois->im.ny/2;
	i = win_scanf("center xc %f yc %f r_{max} %dax %f ay %f",&xc,&yc,&r_max,&ax,&ay);
	if (i == WIN_CANCEL)	return OFF;
	op = create_and_attach_op_to_oi(ois, 2*r_max, 2*r_max, 0, 0);
	ds = op->dat[0];
	t = u_micro = my_uclock();
	radial_non_pixel_square_image_sym_profile_in_array_fixed(ds->yd, ois, xc, yc, r_max, ax, ay);
	t = (int)(my_uclock() - u_micro);

	win_printf("dt = %g \\mu ms", 1000000*((double)t/((int)MY_UCLOCKS_PER_SEC)));
	//	win_printf("t = %d freq %d", (int)t,(int)MY_UCLOCKS_PER_SEC);
	set_plot_title(op,"do radial fixed dt = %g \\mu ms", 
		        1000000*((double)t/((int)MY_UCLOCKS_PER_SEC)));
	ds->source = Mystrdupre(ds->source,op->title);	
	for (i = 0; i < 2*r_max; i++) ds->xd[i] = (i-r_max)/ay;
	return (refresh_im_plot(imr, ois->n_op - 1));		
}

int do_orthoradial(void)
{
	register int i;
	imreg *imr;
	O_i *ois;
	O_p *op;
	d_s *ds;		
	static float xc = 256 , yc = 256, ax = 1, ay = 1;
	static int or_mean = 16, or_width = 1, size = 128;
	unsigned long u_micro;
	int t;
	
	if(updating_menu_state != 0)	return D_O_K;
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
	xc = ois->im.nx/2;
	yc = ois->im.ny/2;
	i = win_scanf("center xc %8f yc %8f\nNb. of pixels in profile %6d\n"
		      "r_{mean} %4d width %4d\n ax %8f ay %8f\n"
		      ,&xc,&yc,&size,&or_mean,&or_width,&ax,&ay);
	if (i == WIN_CANCEL)	return OFF;
	op = create_and_attach_op_to_oi(ois, size, size, 0, 0);
	ds = op->dat[0];
	t = u_micro = my_uclock();


	orthoradial_non_pixel_square_image_sym_profile_in_array(ds->yd, ois, xc, yc, 
								or_mean, or_width, size, ax, ay);
	t = (int)(my_uclock() - u_micro);

	win_printf("dt = %g \\mu ms", 1000000*((double)t/((int)MY_UCLOCKS_PER_SEC)));
	//	win_printf("t = %d freq %d", (int)t,(int)MY_UCLOCKS_PER_SEC);

	for (i = 0; i < size; i++) ds->xd[i] = (2*M_PI*i)*ay;
	ds = create_and_attach_one_ds(op, size, size, 0);
	t = u_micro = my_uclock();



	orthoradial_non_pixel_square_image_sym_profile_in_array_interpol(ds->yd, ois, xc, yc, 
									 or_mean, size, ax, ay);


	set_plot_title(op,"\\stack{{do orthoradial dt = %g \\mu s}{R_{mean} %d +/- %d}}", 
		        1000000*((double)t/((int)MY_UCLOCKS_PER_SEC)),or_mean,or_width);
	ds->source = Mystrdupre(ds->source,op->title);	
	for (i = 0; i < size; i++) ds->xd[i] = (2*M_PI*i)*ay;
	return (refresh_im_plot(imr, ois->n_op - 1));		
}

int do_orthoradial_im(void)
{
	register int i;
	imreg *imr;
	O_i *ois, *oid;
	static float xc = 256 , yc = 256, ax = 1, ay = 1;
	static int or_mean = 16, or_width = 1, size = 128;
	unsigned long u_micro;
	int t;
	
	if(updating_menu_state != 0)	return D_O_K;
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
	xc = ois->im.nx/2;
	yc = ois->im.ny/2;
	i = win_scanf("build an orthoradial image of a bead\n"
		      "center xc %8f yc %8f\nNb. of pixels in profile %6d\n"
		      "r_{max} %4d  ax %8f ay %8f\n"
		      ,&xc,&yc,&size,&or_mean,&or_width,&ax,&ay);
	if (i == WIN_CANCEL)	return OFF;


	oid = create_and_attach_oi_to_imr(imr, size, or_mean, IS_FLOAT_IMAGE);
	if (oid == NULL)	return win_printf_OK("cannot create profile !");
	oid->im.source = strdup("Ortho radial bead profile");
	t = u_micro = my_uclock();

	for ( i = 0; i < or_mean; i++)
	  {
	    
	    orthoradial_non_pixel_square_image_sym_profile_in_array_interpol(oid->im.pixel[i].fl, 
									     ois, xc, yc, i, size, ax, ay);
	  }

	set_im_title(oid,"\\stack{{do orthoradial dt = %g \\mu s}{R_{mean} %d +/- %d}}", 
		        1000000*((double)t/((int)MY_UCLOCKS_PER_SEC)),or_mean,or_width);

	find_zmin_zmax(oid);	
	oid->need_to_refresh = ALL_NEED_REFRESH;	
        return broadcast_dialog_message(MSG_DRAW,0);			
}
int do_orthoradial_to_cartesian_im(void)
{
	register int i, j;
	imreg *imr;
	O_i *ois, *oid;
	//static float yc = 256; //  xc = 256
	static int onx = 32, ony = 32;
	//unsigned long u_micro;
	float x, y, z, theta, r;
	//int t;
	
	if(updating_menu_state != 0)	return D_O_K;
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
	//xc = ois->im.nx/2;
	//yc = ois->im.ny/2;
	i = win_scanf("build an cartesian image from a orthoradial one\n"
		      "size in x %5d in y %5d\n"
		      ,&onx,&ony);
	if (i == WIN_CANCEL)	return OFF;


	oid = create_and_attach_oi_to_imr(imr, onx, ony, IS_FLOAT_IMAGE);
	if (oid == NULL)	return win_printf_OK("cannot create profile !");
	oid->im.source = strdup("Cartesian from Ortho radial bead profile");
	//t = u_micro = my_uclock();

	for ( i = 0; i < ony; i++)
	  {
	    for ( j = 0; j < onx; j++)	    
	      {
		y = i - ony/2;
		x = j - onx/2;
		r = sqrt(x*x + y*y);
		theta = atan2(y,x)/M_PI;
		theta *= ois->im.nx/2;
		theta += ois->im.nx/2;
		if (ois->im.data_type == IS_CHAR_IMAGE)			
		  z = ((float)fast_interpolate_image_point (ois, theta, r, 0))/256;
		else    z = interpolate_image_point (ois, theta, r, &z, NULL);	    
		oid->im.pixel[i].fl[j] = z;

	      }
	  }

	find_zmin_zmax(oid);	
	oid->need_to_refresh = ALL_NEED_REFRESH;	
        return broadcast_dialog_message(MSG_DRAW,0);			
}

int calibration_profile_wavenumber(void)
{
	register int i, j, k;
	imreg *imrr = NULL;	
	O_p *op = NULL;	
	O_i *oi = NULL;
	d_s *ds = NULL, *ds2 = NULL; 
	char  *cs;
	static int bp_center = 16, bp_width = 14, rc = 12, pr_index = 0;
	int nx;//, ny;
	float amp, w, ph, phi, re, im, zp[256], ratio = 0.5;
		
	if(updating_menu_state != 0)	return D_O_K;
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine compute the wavenumber\n"
				     "of a profile of a calibration image\n");
	}

	if (ac_grep(cur_ac_reg,"%im%oi",&imrr,&oi) != 2)	
	  return win_printf_OK("Can't find image region");


	//ny = oi->im.ny;
	nx = oi->im.nx;
	if (nx > 256)	return win_printf("image too large");
	cs = (oi->im.source != NULL) ? oi->im.source : oi->im.treatement; 
	if (cs == NULL)		return win_printf_OK("wrong souce!");	
	if (strncmp(cs,"equally spaced reference profile",32) != 0)
		return win_printf("image must be :\\sl  equally spaced reference profile!");
	//full_size(CR);
 	cs = strstr(cs,"Forget circle");
 	if (cs != NULL)
 	{
 		if (sscanf(cs+13,"%d",&rc) != 1)
 			win_printf("cannot read forget_circle!");
 	}		
 	cs = strstr(cs,"bp_filter");
 	if (cs != NULL)
 	{
 		if (sscanf(cs+9,"%d",&bp_center) != 1)
 			win_printf("cannot read bp_filter!");
 	}	
 	cs = strstr(cs,"bp_width");
 	if (cs != NULL)
 	{
 		if (sscanf(cs+9,"%d",&bp_width) != 1)
 			win_printf("cannot read bp_width!");
 	}			
	i = win_scanf("measure pofile wavenumber index %5d\n"
		      "filter ratio of width to center %f\n"
		      "forget circle%6d ",&pr_index, &ratio, &rc);
	if (i == WIN_CANCEL)	return OFF;


	op = create_and_attach_op_to_oi(oi, nx/2, nx/2, 0,0);
	if (op == NULL)		return win_printf("cannot create plot!");
	ds = op->dat[0];
	set_formated_string(&ds->source,"Profile wavenumber bp %d w %d rc %d ",bp_center, bp_width, rc);
	ds2 = create_and_attach_one_ds(op, nx/2, nx/2, 0);
	set_formated_string(&ds2->source,"Profile wavenumber amplitude bp %d w %d rc %d ",bp_center, bp_width, rc);
	//win_printf("bef loop");
	if (fft_init(nx) || filter_init(nx))	
		return win_printf_OK("cannot init fft!");	

	for (k = 2; k < nx/2 ; k++)
	{
		extract_raw_line (oi, pr_index, zp);
		bp_center = k;
		bp_width = (k*ratio)+0.5;
		bp_width = (bp_width <= 0) ? 1 : bp_width;
		prepare_filtered_profile(zp, nx, 0, bp_center, bp_width);
		for (j = 4, i = nx - j, phi = 0, w = 0; j < nx/2 - rc; i -= 2, j += 2)
		  {
		    if ((i+3 >= nx) || (i < 0) || (j - 2 < 0) || (j >= nx))
		      win_printf("out of range i %d  j %d",i,j); 
		    re = zp[i+2] * zp[i] + zp[i+1] * zp[i+3];
		    im = zp[i+1] * zp[i+2] - zp[i] * zp[i+3];
		    ph = (im == 0.0 && re == 0.0) ? 0.0 : -atan2(im,re);
		    amp = zp[j-2] * zp[j];
		    w += amp;
		    phi += amp*ph;
		  }
		ds->yd[k] =  (w != 0.0) ? phi/w : phi;
		ds->yd[k] *= nx/(M_PI*4);
		ds2->yd[k] =  w;
		ds->xd[k] = ds2->xd[k] = k;
	}
	oi->need_to_refresh |= PLOT_NEED_REFRESH	| EXTRA_PLOT_NEED_REFRESH;	
	return refresh_im_plot(imrr, oi->cur_op);
}

int calibration_image_wavenumber(void)
{
	register int i, j, k;
	imreg *imrr = NULL;	
	O_p *op = NULL;	
	O_i *oi = NULL;
	d_s *ds = NULL, *ds2 = NULL; 
	char  *cs;
	static int bp_center = 16, bp_width = 14, rc = 12;
	int nx, ny;
	float amp, w, ph, phi, re, im, zp[256];
		
	if(updating_menu_state != 0)	return D_O_K;
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine compute the wavenumber\n"
				     "of a calibration image\n");
	}

	if (ac_grep(cur_ac_reg,"%im%oi",&imrr,&oi) != 2)	
	  return win_printf_OK("Can't find image region");


	ny = oi->im.ny;	nx = oi->im.nx;
	if (nx > 256)	return win_printf("image too large");
	cs = (oi->im.source != NULL) ? oi->im.source : oi->im.treatement; 
	if (cs == NULL)		return win_printf_OK("wrong souce!");	
	if (strncmp(cs,"equally spaced reference profile",32) != 0)
		return win_printf("image must be :\\sl  equally spaced reference profile!");
	//full_size(CR);
 	cs = strstr(cs,"Forget circle");
 	if (cs != NULL)
 	{
 		if (sscanf(cs+13,"%d",&rc) != 1)
 			win_printf("cannot read forget_circle!");
 	}		
 	cs = strstr(cs,"bp_filter");
 	if (cs != NULL)
 	{
 		if (sscanf(cs+9,"%d",&bp_center) != 1)
 			win_printf("cannot read bp_filter!");
 	}	
 	cs = strstr(cs,"bp_width");
 	if (cs != NULL)
 	{
 		if (sscanf(cs+9,"%d",&bp_width) != 1)
 			win_printf("cannot read bp_width!");
 	}			
	i = win_scanf("measure calibration image wavenumber\n"
		      "band pass %6d, width %6d\n"
		      "forget circle%6d ",&bp_center, &bp_width, &rc);
	if (i == WIN_CANCEL)	return OFF;


	op = create_and_attach_op_to_oi(oi, ny, ny, 0,0);
	if (op == NULL)		return win_printf("cannot create plot!");
	ds = op->dat[0];
	set_formated_string(&ds->source,"Profile wavenumber bp %d w %d rc %d ",bp_center, bp_width, rc);
	ds2 = create_and_attach_one_ds(op, ny, ny, 0);
	set_formated_string(&ds2->source,"Profile wavenumber amplitude bp %d w %d rc %d ",bp_center, bp_width, rc);
	//win_printf("bef loop");
	if (fft_init(nx) || filter_init(nx))	
		return win_printf_OK("cannot init fft!");	

	for (k = 0; k < ny ; k++)
	{
		extract_raw_line (oi, k, zp);
		//win_printf("bef filter %d",k);
		prepare_filtered_profile(zp, nx, 0, bp_center, bp_width);
		//win_printf("after filter %d",k);
		for (j = 4, i = nx - j, phi = 0, w = 0; j < nx/2 - rc; i -= 2, j += 2)
		  {
		    if ((i+3 >= nx) || (i < 0) || (j - 2 < 0) || (j >= nx))
		      win_printf("out of range i %d  j %d",i,j); 
		    re = zp[i+2] * zp[i] + zp[i+1] * zp[i+3];
		    im = zp[i+1] * zp[i+2] - zp[i] * zp[i+3];
		    ph = (im == 0.0 && re == 0.0) ? 0.0 : -atan2(im,re);
		    amp = zp[j-2] * zp[j];
		    w += amp;
		    phi += amp*ph;
		  }
		//win_printf("after second loop phi %g w %g ",phi,w);
		ds->yd[k] =  (w != 0.0) ? phi/w : phi;
		ds->yd[k] *= nx/(M_PI*4);
		//win_printf("1");
		ds2->yd[k] =  w;
		//win_printf("2");
		ds->xd[k] = ds2->xd[k] = k;
		//win_printf("i %d phi %g amp %g",k,ds->yd[k],w); 
	}
	oi->need_to_refresh |= PLOT_NEED_REFRESH	| EXTRA_PLOT_NEED_REFRESH;	
	return refresh_im_plot(imrr, oi->cur_op);
}
int test_calibration_image(void)
{
	register int i, j, k;
	imreg *imrr = NULL;	
	O_p *op = NULL;	
	O_i *oi = NULL, *oir = NULL;
	d_s *ds = NULL, *ds2 = NULL; 
	float zp[256];
	char  *cs;
	//acreg  *acref = NULL;
	static int nf = 2048, rc = 12;
	static int bp_center = 16, bp_width = 14;
	static float noise = 0.01;
	int nx, ny;//, status;
	float amp2, amp2o, tmp, zb;
	static int idum = 0;
		
	if(updating_menu_state != 0)	return D_O_K;
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine check a calibration image\n"
			"and compute its noise sensitivity");
	}

	if (ac_grep(cur_ac_reg,"%im%oi",&imrr,&oi) != 2)	
	  return win_printf_OK("Can't find image region");

	ny = oi->im.ny;	nx = oi->im.nx;
	cs = (oi->im.source != NULL) ? oi->im.source : oi->im.treatement; 
	if ((cs == NULL) || (strncmp(cs,"equally spaced reference profile",32) != 0) || (nx > 256) )
	  {
	    if ((load_z_calibration_image(imrr, &oi, 0) == NULL) || (oi == NULL))
	      return win_printf_OK("Can't load calibration image");

	    ny = oi->im.ny;	nx = oi->im.nx;
	    if (nx > 256)	return win_printf("image too large");
	    cs = (oi->im.source != NULL) ? oi->im.source : oi->im.treatement; 
	    if (cs == NULL)		return win_printf_OK("wrong souce!");	
	    if (strncmp(cs,"equally spaced reference profile",32) != 0)
	      return win_printf("image must be :\\sl  equally spaced reference profile!");
	    cs = strstr(cs,"Forget circle");
	    if (cs != NULL)
	      {
 		if (sscanf(cs+13,"%d",&rc) != 1)
		  win_printf("cannot read forget_circle!");
	      }		
	    cs = strstr(cs,"bp_filter");
	    if (cs != NULL)
	      {
 		if (sscanf(cs+9,"%d",&bp_center) != 1)
		  win_printf("cannot read bp_filter!");
	      }	
	    cs = strstr(cs,"bp_width");
	    if (cs != NULL)
	      {
 		if (sscanf(cs+9,"%d",&bp_width) != 1)
		  win_printf("cannot read bp_width!");
	      }			
	  }
	//full_size(CR);
	i = win_scanf("test calibration image with noise\n"
		      "nb of trial for one profile %6d noise amp %6f\n"
		      "fseed %16d bp %6d, width %6d\n"
		      "forget circle%6d ",&nf,&noise,&idum,&bp_center, &bp_width, &rc);
	if (i == WIN_CANCEL)	return OFF;
	if (fft_init(nx) || filter_init(nx))	
		return win_printf_OK("cannot init fft!");	

	oir = image_band_pass_in_real_out_complex(NULL, oi, 0, bp_center, bp_width, rc);
	if (oir == NULL)	return win_printf("Can't compute calibration image");

	//add_to_image (imrr, oir->im.data_type, (void *)oir);

	op = create_and_attach_op_to_oi(oi, ny, ny, 0,0);
	if (op == NULL)		return win_printf("cannot create plot!");
	uns_oi_2_op(oi, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
	ds = op->dat[0];
	set_formated_string(&ds->source,"Image test nf %d noise %f bp %d w %d rc %d",nf,noise,bp_center, bp_width, rc);
	ds2 = create_and_attach_one_ds(op, ny, ny, 0);
	set_formated_string(&ds2->source,"Image test omar nf %d noise %f bp %d w %d rc %d",nf,noise,bp_center, bp_width, rc);

	for (i = 0; i < ny; i++)
	{
	  ds->yd[i] = ds2->yd[i] = oi->ay + oi->dy * i;
	  for (j = 0, amp2 = amp2o = 0; j < nf; j++)
	    {
	      extract_raw_line (oi, i, zp);
	      for (k = 1; k < nx/2; k++)
		{
		  tmp = (k < rc) ? rc : k;
		  tmp = nx/(2*tmp);
		  tmp = sqrt(tmp);
		  tmp *= noise * gasdev(&idum);
		  zp[nx/2+k] += tmp;
		  zp[nx/2-k] += tmp;
		}
	      //status =
	      find_z_profile_by_mean_square_and_phase(zp, nx, oir, 0, bp_center, bp_width, rc, &zb);
	      //draw_z_bar(imrr, oir, zb);
	      //if (j == nf/2) win_printf("i %d zb %g zr %g noise %g",i,zb,ds->yd[i],tmp);
	      amp2 += (zb - ds->yd[i]) * (zb - ds->yd[i]);
			//status =
	      find_z_profile_by_mean_square_and_phase_omar(zp, nx, oir, 0, bp_center, bp_width, rc, &zb);
	      amp2o += (zb - ds->yd[i]) * (zb - ds->yd[i]);
	    }
	  ds->xd[i] = 1000*sqrt(amp2/nf);
	  ds2->xd[i] = 1000*sqrt(amp2o/nf);
	  display_title_message("prof %d noise %g 2 %g",i,ds->xd[i],ds2->xd[i]);
	  ds->yd[i] = ds2->yd[i] = i;

	}
	oi->need_to_refresh |= PLOT_NEED_REFRESH;
	//free_one_image(oir);
	//win_printf("bef return");
	refresh_image(imrr, UNCHANGED);
	return D_REDRAWME;
}


int do_trmov_image_resample(void)
{
	O_i *ois, *oid;
	imreg *imr;
	float r;
	char *cs = NULL;//  *co = NULL
	int i, j, onx, ony;
	union pix  *pd;

	if(updating_menu_state != 0)	return D_O_K;
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine resample the image intensity"
		"along r with r -> r^2");
	}
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	{
		win_printf("Cannot find image");
		return D_O_K;
	}
	ony = ois->im.ny;	onx = ois->im.nx;
	cs = (ois->im.source != NULL) ? ois->im.source : ois->im.treatement; 
	if ((cs == NULL) || (strncmp(cs,"equally spaced reference profile",32) != 0))
	  {
		win_printf("This is not a calibration image");
		return D_O_K;
	}

	oid = duplicate_image(ois, NULL);
	if (oid == NULL)	return win_printf_OK("unable to create image!");
	add_image(imr, oid->type, (void*)oid);

	for (i = 0, pd = oid->im.pixel; i < ony ; i++)
	  {
	    for (j=0; j< onx/2; j++)
	      {
		r = j - (onx/2);
		r *= 2*r/onx;
		interpolate_image_point(ois, r, i, pd[i].fl+j+onx/2, NULL);
		pd[i].fl[onx/2-j] = pd[i].fl[j+onx/2];
	      }
	  }
	find_zmin_zmax(oid);
	return (refresh_image(imr, imr->n_oi - 1));
}




int	correlate_2_1d_sig(float *x1, int nx, float *x2, int filter, int remove_dc, float *corr, int norm)
{
	register int i;
	float moy;
	float m1 = 0, m2 = 0, re1, re2, im1, im2;
	
	if (x1 == NULL)				return WRONG_ARGUMENT;
	if (fft_init(nx) || filter_init(nx))	return FFT_NOT_SUPPORTED;

	/*	compute fft of x1 */
	for(i = 0, moy = 0; i < nx; i++)	             moy += x1[i];	
	for(i = 0, moy = moy/nx; remove_dc && i < nx; i++)   x1[i] = x1[i] - moy;
	realtr1(nx, x1);
	fft(nx, x1, 1);
	realtr2(nx, x1, 1);
	for(i = 0, moy = 0; i < nx; i++)	             moy += x2[i];	
	for(i = 0, moy = moy/nx; remove_dc && i < nx; i++)   x2[i] = x2[i] - moy;
	realtr1(nx, x2);
	fft(nx, x2, 1);
	realtr2(nx, x2, 1);

	x1[0] = 0; x2[0] = 0;
	if (remove_dc > 1) 
	  {
	    x1[2] = x1[3] = 0; // we remove the mode 1 high pass filtering  
	    x2[2] = x2[3] = 0; // we remove the mode 1 high pass filtering  
	  }
	
	/*	compute normalization */
	if (norm)
	  {
	    for (i=0, m1 = m2 = 0; i< nx; i+=2)
	      {
		m1 += x1[i] * x1[i] + x1[i+1] * x1[i+1];
		m2 += x2[i] * x2[i] + x2[i+1] * x2[i+1];
	      }
	    m1 /= 2;	
	    m2 /= 2;	
	    m1 = sqrt(m1*m2);
	  }

	/*	compute correlation in Fourier space */
	for (i=2; i< nx; i+=2)
	{
		re1 = x1[i];		re2 = x2[i];
		im1 = x1[i+1];		im2 = x2[i+1];
		corr[i] 	= (re1 * re2 + im1 * im2);
		corr[i+1] 	= (im1 * re2 - re1 * im2);
	}
	corr[0]	= 2*(x1[0] * x2[0]);	/* these too mode are special */
	corr[1]	= 2*(x1[1] * x2[1]);
	for (i=0; norm && i< nx && m1 != 0; i++) corr[i] /= m1;
	//corr[2] = corr[3] = 0;
	if (filter> 0)		lowpass_smooth_half (nx, corr, filter);
	
	/*	get back to real world */
	realtr2(nx, corr, -1);
	fft(nx, corr, -1);
	realtr1(nx, corr);
	deplace(corr, nx);
	return 0;
}	



int image_correlation_profile(void)
{
	register int k, i;
	imreg *imrr = NULL;	
	O_p *op = NULL;	
	O_i *oi = NULL, *oid = NULL;
	d_s *ds = NULL, *ds2 = NULL; 
	int nx, ny, is, ie;
	static int nb, lp_fc = 16, hp = 1;
	float max;

		
	if(updating_menu_state != 0)	return D_O_K;
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine compute the correlation\n"
				     "between profiles\n");
	}

	if (ac_grep(cur_ac_reg,"%im%oi",&imrr,&oi) != 2)	
	  return win_printf_OK("Can't find image region");


	ny = oi->im.ny;	nx = oi->im.nx;
	nb = nx/10;
	i = win_scanf("On what range do you expect the maximum of correlation %5d\n"
		      "Low pass filtering %5d\nHigh pass filtering %b\n",&nb,&lp_fc,&hp);
	if (i == WIN_CANCEL) return D_O_K;

	op = create_and_attach_op_to_oi(oi, nx, nx, 0,0);
	if (op == NULL)		return win_printf("cannot create plot!");
	ds = op->dat[0];
	set_formated_string(&ds->source,"Profile ");
	//set_oi_plot_axes(oi,IM_SAME_X_AXIS);
	op->type |= IM_SAME_X_AXIS;
	if (fft_init(nx) || filter_init(nx))	
		return win_printf_OK("cannot init fft!");	
	oid = create_and_attach_oi_to_imr(imrr, nx, ny, IS_FLOAT_IMAGE);
	if (oid == NULL)		return win_printf("cannot create image!");

	for (k = 0; k < ny ; k++)
	{
	  extract_raw_line (oi, 0, ds->yd);
	  extract_raw_line (oi, k, ds->xd);
	  correlate_2_1d_sig(ds->yd, nx, ds->xd, lp_fc, hp+1, oid->im.pixel[k].fl, 1);
	}
	extract_raw_line (oi, 0, ds->yd);
	find_zmin_zmax(oid);	
	oid->need_to_refresh = ALL_NEED_REFRESH;
//set_oi_plot_axes(oid,IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
	oi->need_to_refresh |= PLOT_NEED_REFRESH	| EXTRA_PLOT_NEED_REFRESH;	
	op = create_and_attach_op_to_oi(oid, ny, ny, 0,IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
	if (op == NULL)		return win_printf("cannot create plot!");
	ds2 = op->dat[0];
	set_formated_string(&ds2->source,"max ");
	for (k = 0, is = 0, ie = nx; k < ny ; k++)
	{
	  extract_raw_line (oid, k, ds->xd);
	  for (i = is, max = ds->xd[is], ds2->xd[k] = is; i < ie ; i++)
	    {
	      if (ds->xd[i] > max)
		{
		  max = ds->xd[i];
		  ds2->xd[k] = i;
		}
	    }
	  is = ds2->xd[k] - nb + nx;
	  is %= nx;
	  ie = ds2->xd[k] + nb;
	  ie %= nx;
	  ds2->yd[k] = k;
	}
	for (k = 0; k < nx ; k++)  ds->xd[k] = k;

	return refresh_im_plot(imrr, oi->cur_op);
}
// en cours
int image_correlation_profile_cor(void)
{
	register int k, i;
	imreg *imrr = NULL;	
	O_p *op = NULL, *op2 = NULL;	
	O_i *oi = NULL, *oid = NULL;
	d_s *ds = NULL, *ds2 = NULL, *ds3 = NULL; 
	int nx, ny;
	static int nb, lp_fc = 16, hp = 1;
	static float dec = 0.9;
	float dx, corr;

		
	if(updating_menu_state != 0)	return D_O_K;
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine compute the correlation\n"
				     "between profiles\n");
	}

	if (ac_grep(cur_ac_reg,"%im%oi",&imrr,&oi) != 2)	
	  return win_printf_OK("Can't find image region");


	ny = oi->im.ny;	nx = oi->im.nx;
	nb = nx/10;
	i = win_scanf("On what range do you expect the maximum of correlation %5d\n"
		      "Low pass filtering %5d\nHigh pass filtering %b\nDecay factor %8f\n",&nb,&lp_fc,&hp,&dec);
	if (i == WIN_CANCEL) return D_O_K;

	op = create_and_attach_op_to_oi(oi, nx, nx, 0,0);
	if (op == NULL)		return win_printf("cannot create plot!");
	ds = op->dat[0];
	set_formated_string(&ds->source,"Profile ");
	//set_oi_plot_axes(oi,IM_SAME_X_AXIS);
	op->type |= IM_SAME_X_AXIS;
	ds2 = create_and_attach_one_ds(op, nx, nx, 0);


	if (fft_init(nx) || filter_init(nx))	
		return win_printf_OK("cannot init fft!");	
	oid = create_and_attach_oi_to_imr(imrr, nx, ny, IS_FLOAT_IMAGE);
	if (oid == NULL)		return win_printf("cannot create image!");

	op2 = create_and_attach_op_to_oi(oi, ny, ny, 0,IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
	if (op2 == NULL)		return win_printf("cannot create plot!");
	ds3 = op2->dat[0];
	set_formated_string(&ds3->source,"max ");


	extract_raw_line (oi, 0, ds2->yd);
	for (k = 0; k < ny ; k++)
	{
	  for (i = 0; i < nx ; i++) ds->yd[i] = ds2->yd[i];
	  extract_raw_line (oi, k, ds->xd);
	  correlate_2_1d_sig(ds->yd, nx, ds->xd, lp_fc, hp+1, oid->im.pixel[k].fl, 1);
	  find_max1(oid->im.pixel[k].fl, nx, &dx,&corr);
	  dx -= nx/2;
	  ds3->xd[k] = dx;
	  ds3->yd[k] = k;
	  extract_raw_line (oi, k, ds->xd);
	  translate_in_real_space_using_fft(ds->xd, nx, dx);
	  for (i = 0; i < nx ; i++) 
	    {
	      oid->im.pixel[k].fl[i] = ds->xd[i];
	      ds2->yd[i] = dec*ds2->yd[i] + (1-dec)*ds->xd[i];
	    }
	}
	extract_raw_line (oi, 0, ds->yd);
	find_zmin_zmax(oid);	
	oid->need_to_refresh = ALL_NEED_REFRESH;
//set_oi_plot_axes(oid,IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
	oi->need_to_refresh |= PLOT_NEED_REFRESH | EXTRA_PLOT_NEED_REFRESH;	
	for (k = 0; k < nx ; k++)  ds->xd[k] = k;
	return refresh_im_plot(imrr, oi->cur_op);
}

int image_correlation_profile_n_n_1(void)
{
	register int k, i;
	imreg *imrr = NULL;	
	O_p *op = NULL;	
	O_i *oi = NULL, *oid = NULL;
	d_s *ds = NULL, *ds2 = NULL; 
	int nx, ny, is, ie;
	static int nb, lp_fc = 16, hp = 1;
	float max, dx, corr;

		
	if(updating_menu_state != 0)	return D_O_K;
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine compute the correlation\n"
				     "between profiles\n");
	}

	if (ac_grep(cur_ac_reg,"%im%oi",&imrr,&oi) != 2)	
	  return win_printf_OK("Can't find image region");


	ny = oi->im.ny;	nx = oi->im.nx;
	nb = nx/10;
	i = win_scanf("On what range do you expect the maximum of correlation %5d\n"
		      "Low pass filtering %5d\nHigh pass filtering %b\n",&nb,&lp_fc,&hp);
	if (i == WIN_CANCEL) return D_O_K;

	op = create_and_attach_op_to_oi(oi, nx, nx, 0,0);
	if (op == NULL)		return win_printf("cannot create plot!");
	ds = op->dat[0];
	set_formated_string(&ds->source,"Profile ");
	//set_oi_plot_axes(oi,IM_SAME_X_AXIS);
	op->type |= IM_SAME_X_AXIS;
	if (fft_init(nx) || filter_init(nx))	
		return win_printf_OK("cannot init fft!");	
	oid = create_and_attach_oi_to_imr(imrr, nx, ny, IS_FLOAT_IMAGE);
	if (oid == NULL)		return win_printf("cannot create image!");

	for (k = 0; k < ny ; k++)
	{
	  extract_raw_line (oi, (k)?k-1:0, ds->yd);
	  extract_raw_line (oi, k, ds->xd);
	  correlate_2_1d_sig(ds->yd, nx, ds->xd, lp_fc, hp+1, oid->im.pixel[k].fl, 1);
	}
	extract_raw_line (oi, 0, ds->yd);
	find_zmin_zmax(oid);	
	oid->need_to_refresh = ALL_NEED_REFRESH;
//set_oi_plot_axes(oid,IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
	oi->need_to_refresh |= PLOT_NEED_REFRESH	| EXTRA_PLOT_NEED_REFRESH;	
	op = create_and_attach_op_to_oi(oid, ny, ny, 0,IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
	if (op == NULL)		return win_printf("cannot create plot!");
	ds2 = op->dat[0];
	set_formated_string(&ds2->source,"max ");
	for (k = 0, is = 0, ie = nx; k < ny ; k++)
	{
	  extract_raw_line (oid, k, ds->xd);
	  for (i = is, max = ds->xd[is], ds2->xd[k] = is; i < ie ; i++)
	    {
	      if (ds->xd[i] > max)
		{
		  max = ds->xd[i];
		  ds2->xd[k] = i;
		}
	    }
	  is = ds2->xd[k] -nb + nx;
	  is %= nx;
	  ie = ds2->xd[k] + nb;
	  ie %= nx;
	  ds2->yd[k] = k;
	}
	for (k = 0; k < nx ; k++)  ds->xd[k] = k;
	op = create_and_attach_op_to_oi(oid, ny, ny, 0,IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
	if (op == NULL)		return win_printf("cannot create plot!");
	ds2 = op->dat[0];
	set_formated_string(&ds2->source,"max float");
	for (k = 0, is = 0, ie = nx; k < ny ; k++)
	{
	  extract_raw_line (oid, k, ds->xd);
	  find_max1(ds->xd, nx, &dx,&corr);
	  dx -= nx/2;
	  ds2->xd[k] = dx;
	  ds2->yd[k] = k;
	}
	for (k = 0; k < nx ; k++)  ds->xd[k] = k;

	return refresh_im_plot(imrr, oi->cur_op);
}


int movie_correlation_imagee(void)
{
	register int k, i;
	imreg *imrr = NULL;	
	O_p *op = NULL;	
	O_i *oi = NULL, *oid = NULL;
	d_s *ds = NULL; 
	int nx, ny, nf;

		
	if(updating_menu_state != 0)	return D_O_K;
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine compute the correlation\n"
				     "between profiles\n");
	}
	if (ac_grep(cur_ac_reg,"%im%oi",&imrr,&oi) != 2)	
	  return win_printf_OK("Can't find image region");

	ny = oi->im.ny;	nx = oi->im.nx; nf = oi->im.n_f;
	if (nf < 2)  return win_printf_OK("this is not a movie!");

	op = create_and_attach_op_to_oi(oi, nx, nx, 0,IM_SAME_X_AXIS);
	if (op == NULL)		return win_printf("cannot create plot!");
	ds = op->dat[0];
	set_formated_string(&ds->source,"Profile ");
	//set_oi_plot_axes(oi,IM_SAME_X_AXIS);

	if (fft_init(nx) || filter_init(nx))	
		return win_printf_OK("cannot init fft!");	
	oid = create_and_attach_movie_to_imr(imrr, nx, ny, IS_FLOAT_IMAGE, nf);
	if (oi == NULL)		return win_printf("cannot create image!");
	set_formated_string(&oid->im.source,"Corr ");
	for (k = 0; k < nf ; k++)
	{
	  switch_frame(oid,k);
	  for (i = 1; i < ny ; i++)
	    {
	      switch_frame(oi,0);
	      extract_raw_line (oi, i, ds->yd);
	      switch_frame(oi,k);
	      extract_raw_line (oi, i, ds->xd);
	      correlate_2_1d_sig(ds->yd, nx, ds->xd, nx/4, 1, oid->im.pixel[i].fl,0);
	      //extract_raw_line (oi, i, oid->im.pixel[i].fl);
	    }
	  display_title_message("im %d",k);
	}
	win_printf("bef switch");
	switch_frame(oi,0);
	extract_raw_line (oi, ny/2, ds->yd);
	find_zmin_zmax(oid);	
	oid->need_to_refresh = ALL_NEED_REFRESH;	

	oi->need_to_refresh |= PLOT_NEED_REFRESH	| EXTRA_PLOT_NEED_REFRESH;	
	/*
	op = create_and_attach_op_to_oi(oid, ny, ny, 0,0);
	if (op == NULL)		return win_printf("cannot create plot!");
	ds2 = op->dat[0];
	set_formated_string(&ds->source,"max ");
	for (k = 0, is = 0, ie = nx; k < ny ; k++)
	{
	  extract_raw_line (oid, k, ds->xd);
	  for (i = is, max = ds->xd[is], ds2->xd[k] = is; i < ie ; i++)
	    {
	      if (ds->xd[i] > max)
		{
		  max = ds->xd[i];
		  ds2->xd[k] = i;
		}
	    }
	  is = ds2->xd[k] - 10 + nx;
	  is %= nx;
	  ie = ds2->xd[k] + 10;
	  ie %= nx;
	  ds2->yd[k] = k;
	}
	*/
	for (k = 0; k < nx ; k++)  ds->xd[k] = k;

	return refresh_im_plot(imrr, oi->cur_op);
}



int movie_correlation_profile(void)
{
	register int k, i, j;
	imreg *imrr = NULL;	
	O_p *op = NULL;	
	O_i *oi = NULL, *oid = NULL, *oidr = NULL;
	d_s *ds = NULL, *ds2 = NULL; 
	int nx, ny, is, ie, nf, nb;
	static int rmin = 10, rmax = 18,norm = 0;
	float max;

		
	if(updating_menu_state != 0)	return D_O_K;
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine compute the correlation\n"
				     "between profiles\n");
	}

	if (ac_grep(cur_ac_reg,"%im%oi",&imrr,&oi) != 2)	
	  return win_printf_OK("Can't find image region");


	ny = oi->im.ny;	nx = oi->im.nx; nf = oi->im.n_f;
	if (nf < 2)  return win_printf_OK("this is not a movie!");
    
    nb = nx/10;
	i = win_scanf("On what range do you expect the maximum of correlation %d",&nb);
	if (i == WIN_CANCEL) return D_O_K;

	i = win_scanf("Averaging correlation between r_{min} and r_{max}\n"
		      "r_{min} = %5d  r_{max} = %5d normalized correlation %b\n"
		      ,&rmin,&rmax,&norm);
	if (i == WIN_CANCEL) return OFF;

	if (rmin < 1 || rmin > ny-2) return win_printf_OK("rmin out of range!");
	if (rmax < 3 || rmax > ny) return win_printf_OK("rmax out of range!");

	op = create_and_attach_op_to_oi(oi, nx, nx, 0,0);
	if (op == NULL)		return win_printf("cannot create plot!");
	ds = op->dat[0];
	set_formated_string(&ds->source,"Profile ");
	//set_oi_plot_axes(oi,IM_SAME_X_AXIS);
	op->type |= IM_SAME_X_AXIS;
	if (fft_init(nx) || filter_init(nx))	
		return win_printf_OK("cannot init fft!");	
	oid = create_and_attach_oi_to_imr(imrr, nx, nf, IS_FLOAT_IMAGE);
	if (oid == NULL)		return win_printf("cannot create image!");

	oidr = create_and_attach_oi_to_imr(imrr, nx, ny, IS_FLOAT_IMAGE);
	if (oidr == NULL)		return win_printf("cannot create image!");
	// a single reference image
	for (i = 0, switch_frame(oi,0); i < ny ; i++)
	      extract_raw_line (oi, i, oidr->im.pixel[i].fl);

	set_formated_string(&oid->im.source,"Corr ");
	for (k = 0; k < nf ; k++)
	{
	  switch_frame(oi,k);
	  for (i = rmin; i < rmax ; i++)
	    {
	      extract_raw_line (oidr, i, ds->yd);
	      extract_raw_line (oi, i, ds->xd);
	      correlate_2_1d_sig(ds->yd, nx, ds->xd, nx/4, 1, oidr->im.pixel[0].fl,norm);
	      for (j = 0; j < nx; j++)
		oid->im.pixel[k].fl[j] += oidr->im.pixel[0].fl[j];
	    }
	  display_title_message("im %d",k);
	}
	switch_frame(oi,0);
	extract_raw_line (oi, 0, oidr->im.pixel[0].fl);
	
	extract_raw_line (oi, 0, ds->yd);
	find_zmin_zmax(oid);	
	find_zmin_zmax(oidr);	
	oid->need_to_refresh = ALL_NEED_REFRESH;
	oi->need_to_refresh |= PLOT_NEED_REFRESH	| EXTRA_PLOT_NEED_REFRESH;	
	op = create_and_attach_op_to_oi(oid, nf, nf, 0,IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
	if (op == NULL)		return win_printf("cannot create plot!");
	ds2 = op->dat[0];
	set_formated_string(&ds->source,"max ");
	for (k = 0, is = 0, ie = nx; k < nf ; k++)
	{
	  extract_raw_line (oid, k, ds->xd);
	  for (i = is, max = ds->xd[is], ds2->xd[k] = is; i < ie ; i++)
	    {
	      if (ds->xd[i] > max)
		{
		  max = ds->xd[i];
		  ds2->xd[k] = i;
		}
	    }
	  is = ds2->xd[k] - nb + nx;
	  is %= nx;
	  ie = ds2->xd[k] + nb;
	  ie %= nx;
	  ds2->yd[k] = k;
	}
	for (k = 0; k < nx ; k++)  ds->xd[k] = k;

	return refresh_im_plot(imrr, oi->cur_op);
}

int mean_y2_on_array(float *yd, int nx, int win_flag, float *meany, float *my2, float *my4)
{
	register int i;
	float tmp, dx, y2, mean, y4;

	if (win_flag)
	{
		dx = 2*M_PI/nx;
		for (i=0 ,mean = 0; i< nx ; i++)		
			mean += (1-cos(i*dx))*yd[i];
	}
	else
	{
		for (i=0 ,mean = 0; i< nx ; i++)			mean += yd[i];
	}	
	mean /= nx;
	if (win_flag)
	{
		dx = 2*M_PI/nx;
		for (i=0 ,y4 = y2 = 0; i< nx ; i++)
		{		
			tmp = yd[i] - mean;
			y2 += (1-cos(i*dx)) * tmp * tmp;
			y4 += (1-cos(i*dx)) * tmp * tmp * tmp * tmp;
		}
	}
	else
	{
		for (i=0 ,y4 = y2 = 0; i< nx ; i++)
		{
			tmp = yd[i] - mean;
			y2 += tmp * tmp;
			y4 +=  tmp * tmp * tmp * tmp;
		}
	}
	y2 /= nx;
	y4 /= nx;
	if (meany != NULL)	*meany = mean;
	if (my2 != NULL)	*my2 = y2;
	if (my4 != NULL)	*my4 = y4;
	return 0;
}


int movie_correlation_profile_modified(void)
{
	register int k, i, j;
	imreg *imrr = NULL;	
	O_p *op = NULL;	
	O_i *oi = NULL, *oid = NULL, *oidr = NULL;
	d_s *ds = NULL, *ds2 = NULL, *ds3 = NULL; 
	int nx, ny, nf, win_flag;
	static int rmin = 3, rmax = 30,norm = 0;
	float corr, meany, meany2;

		
	if(updating_menu_state != 0)	return D_O_K;
	
	win_flag = RETRIEVE_MENU_INDEX;
	
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine compute the correlation\n"
				     "between profiles\n");
	}

	if (ac_grep(cur_ac_reg,"%im%oi",&imrr,&oi) != 2)	
	  return win_printf_OK("Can't find image region");


	ny = oi->im.ny;	nx = oi->im.nx; nf = oi->im.n_f;
	if (nf < 2)  return win_printf_OK("this is not a movie!");
    

	i = win_scanf("Averaging correlation between r_{min} and r_{max}\n"
		      "r_{min} = %5d  r_{max} = %5d normalized correlation %b\n"
		      ,&rmin,&rmax,&norm);
	if (i == WIN_CANCEL) return OFF;

	if (rmin < 1 || rmin > ny-2) return win_printf_OK("rmin out of range!");
	if (rmax < 3 || rmax > ny) return win_printf_OK("rmax out of range!");

	op = create_and_attach_op_to_oi(oi, nx, nx, 0,0);
	if (op == NULL)		return win_printf("cannot create plot!");
	ds = op->dat[0];
	set_formated_string(&ds->source,"Profile ");
	//set_oi_plot_axes(oi,IM_SAME_X_AXIS);
	op->type |= IM_SAME_X_AXIS;
	if (fft_init(nx) || filter_init(nx))	
		return win_printf_OK("cannot init fft!");	
	oid = create_and_attach_oi_to_imr(imrr, nx, nf, IS_FLOAT_IMAGE);
	if (oid == NULL)		return win_printf("cannot create image!");

	oidr = create_and_attach_oi_to_imr(imrr, nx, ny, IS_FLOAT_IMAGE);
	if (oidr == NULL)		return win_printf("cannot create image!");
	// a single reference image
	for (i = 0, switch_frame(oi,0); i < ny ; i++)
	      extract_raw_line (oi, i, oidr->im.pixel[i].fl);

	set_formated_string(&oid->im.source,"Corr ");
	for (k = 0; k < nf-1 ; k++)
	{
	  for (i = rmin; i < rmax ; i++)
	    {
          switch_frame(oi,k);
	      extract_raw_line (oi, i, ds->yd);
	      switch_frame(oi,k+1);
	      extract_raw_line (oi, i, ds->xd);
	      correlate_2_1d_sig(ds->yd, nx, ds->xd, nx/4, 1, oidr->im.pixel[0].fl,norm);
	      for (j = 0; j < nx; j++)
	       { 
            mean_y2_on_array(oidr->im.pixel[0].fl, nx, win_flag, &meany, &meany2, NULL);
            if ( sqrt(meany2) > 4 )
		      oid->im.pixel[k].fl[j] += oidr->im.pixel[0].fl[j];
           }
	    }
	  display_title_message("im %d",k);
	}
	switch_frame(oi,0);
	extract_raw_line (oi, 0, oidr->im.pixel[0].fl);
	
	extract_raw_line (oi, 0, ds->yd);
	find_zmin_zmax(oid);	
	find_zmin_zmax(oidr);	
	oid->need_to_refresh = ALL_NEED_REFRESH;
	oi->need_to_refresh |= PLOT_NEED_REFRESH	| EXTRA_PLOT_NEED_REFRESH;	
	op = create_and_attach_op_to_oi(oid, nf, nf, 0,IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
	if (op == NULL)		return win_printf("cannot create plot!");
	ds2 = op->dat[0];
	set_formated_string(&ds->source,"max ");
	
	for (k = 0; k < nf-1 ; k++)
	{
	  extract_raw_line (oid, k, ds->xd);
	  ds2->xd[k] = find_distance_from_center(ds->xd, nx, 0, nx/4, 1, &corr) + nx/2;
	  ds2->yd[k] = k;
	}
	for (k = 0; k < nx ; k++)  ds->xd[k] = k;
	
	op = create_and_attach_op_to_oi(oid, nf, nf, 0,IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
	if (op == NULL)		return win_printf("cannot create plot!");
	ds3 = op->dat[0];
	set_formated_string(&ds->source,"max ");

	ds3->xd[0] = (ds2->xd[0] - nx/2)?(ds2->xd[0] - nx/2):(ds2->xd[0] + nx/2);
    for (k = 1; k < nf-1 ; k++)
	{
	  ds3->xd[k] = ds2->xd[k]- nx/2 + ds3->xd[k-1];
	  if (ds3->xd[k]>= nx) ds3->xd[k] -= nx;
	  if (ds3->xd[k] <0) ds3->xd[k] += nx;
      ds3->yd[k] = k;
	}
	
	return refresh_im_plot(imrr, oi->cur_op);
}


float	find_angel_from_center(float *x1, int nx, float *corr, float *dx, float rot_speed, float a, float b)
{
	float max, dx1;
	int i;
	
	(void)nx;
	(void)corr;
	max = 0;
	dx1 = *dx;
	for(i = *dx- a*rot_speed; i<*dx+ b*rot_speed; i++)
	{
    if(x1[i]>max) 
      {
       max = x1[i];
       dx1 = i;
	  }
    } 
    *dx = dx1;
	return *dx;
}

int angel_compare(void)
{
    register int i;
	imreg *imr;	
	O_i *ois;
    O_p *op = NULL;
    d_s *ds;
	union pix *ps; 
	int nx, ny, j, j_min=100, j_max=200; // , color
	float  xval = 0, rot_speed, a = 6, b = 0.5;
	static float dx;
	
	if(updating_menu_state != 0)	return D_O_K;
	
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	
		return win_printf_OK("cannot find image in acreg!");

	ps = ois->im.pixel;
	nx = ois->im.nx;
	ny = ois->im.ny;
	//color = makecol(255,64,64);
    rot_speed = nx/60;
		
    i = win_scanf("choose min line:%6d \n max line: %6d", &j_min, &j_max); 
    if (i == WIN_CANCEL)	return OFF; 
    if (j_min < 0)		return win_printf_OK("min line cannot be negative!");
    if (j_max > ny)		return win_printf_OK("max line cannot bigger than the total quantity!");
    if (j_max < j_min)	return win_printf_OK("Attention! Number opposite");
 
    
    i = win_scanf("choose angel%3f \n (on the right part of image)\n in the bright region", &dx); 
    if (i == WIN_CANCEL)	return OFF; 
    if (dx > nx)		return win_printf_OK("too big!");
    
    i = win_scanf("check region:\n left %3f \n right %3f", &a, &b);
    if (i == WIN_CANCEL)	return OFF; 
    
    op = create_and_attach_op_to_oi(ois, (int)ny, (int)ny, 0, IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
	if (op == NULL)		return win_printf_OK("cannot create plot !");
    ds = op->dat[0];
	
    uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
	uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
    set_plot_title(op, "Angel compare");
	op->filename = my_sprintf(op->filename,"angel_compare00%s",".gr");

    
    for(j = 0; j< (j_max-j_min); j++)
      {
       ds->xd[j]= find_angel_from_center(ps[j+j_min].fl, nx, &xval, &dx, rot_speed, a, b); 
       ds->yd[j]= (float)(j+j_min);
      }
      
    for(j = (j_max-j_min); j< ny; j++)
      {
       ds->xd[j] = dx;
       ds->yd[j] = (float)j_max;
      }

	op->need_to_refresh = 1;
    return broadcast_dialog_message(MSG_DRAW,0);			
}

int max_transfer(union pix *ps1, union pix *ps2, int nx, int j)
{
    int i, transfer, max, x;
    float *x1, *x2, max_sum = 0, sum = 0;
    
    x1 = ps1[j].fl;
    x2 = ps2[j].fl;
    max = 0;
    max_sum = 0;
    for (transfer = - (int)(nx/10); transfer < (int)(nx/10); transfer++)
      { sum = 0;
        for (i = 0; i < nx; i++)
            {
              x = i + transfer;
              if (x < 0) {x += nx;}
              if (x >= nx) {x -= nx;}
              sum += x1[i]* x2[x]/10000;   
             } 
        if (sum > max_sum) {max_sum = sum; max = transfer;} 
      }
        
    return max;
}

int angel_compare2(void)
{
    register int i;
	imreg *imrs;	
	O_i *ois;
    O_p *op = NULL;
    d_s *ds;
	union pix *ps1, *ps2;
    int nx, ny, j, j_min=0, j_max=20, im;
	static int nf = 2048;
	
	if(updating_menu_state != 0)	return D_O_K;
	
	if (ac_grep(NULL,"%im%oi",&imrs,&ois) != 2)	
		return win_printf_OK("cannot find image in acreg!");

    nf = ois->im.n_f;
	nx = ois->im.nx;
	ny = ois->im.ny;

	
    i = win_scanf("choose min line:%6d \n max line: %6d", &j_min, &j_max); 
    if (i == WIN_CANCEL)	return OFF; 
    if (j_min < 0)		return win_printf_OK("min line cannot be negative!");
    if (j_max > ny)		return win_printf_OK("max line cannot bigger than the total quantity!");
    if (j_max < j_min)	return win_printf_OK("Attention! Number opposite");
 	
    op = create_and_attach_op_to_oi(ois, (nf-1)*(j_max-j_min+1), (nf-1)*(j_max-j_min+1), 0, 0);
	if (op == NULL)		return win_printf_OK("cannot create plot !");
	ds = op->dat[0];
	
	uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
	uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
	set_plot_title(op, "Angle compare2");

	op->filename = my_sprintf(op->filename,"angle000d%s",".gr");
	
	for(im = 0; im < nf; im++)
      {
       switch_frame(ois, im); 
       ps1 = ois->im.pixel;
       switch_frame(ois, im+1); 
       ps2 = ois->im.pixel;
       for(j = j_min; j <= j_max; j++)
         {
          ds->xd[im+(nf-1)*(j-j_min)] = max_transfer(ps1, ps2, nx, j);
          ds->yd[im+(nf-1)*(j-j_min)] = im;
         }
      }
	
    op->need_to_refresh = 1;
    return broadcast_dialog_message(MSG_DRAW,0);
}





int angel_compare_avg(void)
{
    register int i;
	imreg *imrs;	
	O_i *ois;
    O_p *op = NULL;
    d_s *ds;
	union pix *ps1, *ps2;
	int nx, ny, j, j_min=5, j_max=15, im, num[2048], templ[128][1024]; // , color
	float limit= 2.5, sum;
    static int nf = 2048;
	
	if(updating_menu_state != 0)	return D_O_K;
	
	if (ac_grep(NULL,"%im%oi",&imrs,&ois) != 2)	
		return win_printf_OK("cannot find image in acreg!");

    nf = ois->im.n_f;
	nx = ois->im.nx;
	ny = ois->im.ny;
	//color = makecol(255,64,64);
	
    i = win_scanf("choose min line:%6d \n max line: %6d\n"
                  "accuracy limit: %6f", &j_min, &j_max, &limit); 
    if (i == WIN_CANCEL)	return OFF; 
    if (j_min < 0)		return win_printf_OK("min line cannot be negative!");
    if (j_max > ny)		return win_printf_OK("max line cannot bigger than the total quantity!");
    if (j_max < j_min)	return win_printf_OK("Attention! Number opposite");
 	
    op = create_and_attach_op_to_oi(ois, nx, nf-1, 0, 0);
	if (op == NULL)		return win_printf_OK("cannot create plot !");
	ds = op->dat[0];
	for(i = 0; i<= nf-1; i++)
	  {ds->xd[i]=0; ds->yd[i]=i; num[i]=0;}
	  
	uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
	uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
	set_plot_title(op, "Angle compare2");

	op->filename = my_sprintf(op->filename,"angle000d%s",".gr");
	
    for(im = 0; im < nf; im++)
      {
       switch_frame(ois, im); 
       ps1 = ois->im.pixel;
       switch_frame(ois, im+1); 
       ps2 = ois->im.pixel;
       for(j = j_min; j <= j_max; j++)
          templ[j-j_min][im] = max_transfer(ps1, ps2, nx, j); 
      }
    
    for(j = j_min; j <= j_max; j++)
       {
        sum = 0;
        for(im = 0; im < nf; im++)    sum += templ[j-j_min][im];
 
        if ((sum <= -limit*(nf-1))||(sum >= limit*(nf-1)))
            for(im = 0; im < nf; im++)
             {
               ds->xd[im] += templ[j-j_min][im];
               num[im]++;
              }
       }
 
    for(im = 0; im<= nf-1; im++)
      if(num[im]!=0) ds->xd[im]/= num[im]; 
    
    for(im = 1; im<= nf; im++)
     {
      ds->xd[im] += ds->xd[im-1];  
      if(ds->xd[im] >= nx) ds->xd[im]-= nx;
      if(ds->xd[im] < 0) ds->xd[im]+= nx;
     }
    
    
    op->need_to_refresh = 1;
    return broadcast_dialog_message(MSG_DRAW,0);
}

int project_tilted_bead_profiles(O_i *ois,    // the full image 
				 float x_r, float y_r,   // the center of the right bead image in pixels
				 float x_l, float y_l,   // the center of the left bead image in pixels
				 float theta,            // the angle of illumination in radian
				 int np, int w,          // the number of points in profiles, minimum width
				 float *xr,              // the profile along x of the right image
				 float *yr,              // the profile along y of the right image
				 float *xl,              // the profile along x of the left image
				 float *yl,              // the profile along y of the left image
				 int draw_elipse)        // draw elipse on image
{
  register int i, j;
  int iys, iye, jp, onx; // , ony
  double hc, OD, OE, OA, si, co, dx, val; // , xc, yc
  float w2, norm[256], x, y, dl, tmp;
  O_p *op = NULL;
  d_s *ds = NULL, *ds3 = NULL, *ds6 = NULL;
  d_s *ds2 = NULL, *ds23 = NULL, *ds26 = NULL;

  onx = ois->im.nx;
  //ony = ois->im.ny;
  //xc = (x_r + x_l)/2;
  //yc = (y_r + y_l)/2;
  dx = (x_r - x_l)/2;
  w2 = w * w;
  si = sin(theta);
  co = cos(theta);
  hc = dx * co;
  hc = (si != 0) ? hc/si : 0;
  OD = sqrt(hc*hc + dx*dx);

  iys = (int)(y_r+0.5) - np/2;
  iys = (iys < 0) ? 0 : iys;
  iye = iys + np;
  iye = (iye > ois->im.ny) ? ois->im.ny : iye;


  if (draw_elipse)
    {
      op = create_and_attach_op_to_oi(ois, 4*np, 4*np, 0, IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
      if (op == NULL)	return win_printf_OK("cannot create plot !");
      ds = op->dat[0];
      ds->nx = ds->ny = 0;
      set_plot_symb(ds,"\\pt2\\oc ");
      set_ds_dot_line(ds);
      ds3 = create_and_attach_one_ds(op, 4*np, 4*np, 0);
      if (ds3 == NULL)	return win_printf_OK("cannot create plot !");
      ds3->nx = ds3->ny = 0;
      set_plot_symb(ds3,"\\pt2\\oc ");
      set_ds_dot_line(ds3);
      ds6 = create_and_attach_one_ds(op, 4*np, 4*np, 0);
      if (ds6 == NULL)	return win_printf_OK("cannot create plot !");
      ds6->nx = ds6->ny = 0;
      set_plot_symb(ds6,"\\pt2\\oc ");
      set_ds_dot_line(ds6);


      ds2 = create_and_attach_one_ds(op, 4*np, 4*np, 0);
      if (ds2 == NULL)	return win_printf_OK("cannot create plot !");
      ds2->nx = ds2->ny = 0;
      set_plot_symb(ds2,"\\pt2\\oc ");
      set_ds_dot_line(ds2);
      ds23 = create_and_attach_one_ds(op, 4*np, 4*np, 0);
      if (ds23 == NULL)	return win_printf_OK("cannot create plot !");
      ds23->nx = ds23->ny = 0;
      set_plot_symb(ds23,"\\pt2\\oc ");
      set_ds_dot_line(ds23);
      ds26 = create_and_attach_one_ds(op, 4*np, 4*np, 0);
      if (ds26 == NULL)	return win_printf_OK("cannot create plot !");
      ds26->nx = ds26->ny = 0;
      set_plot_symb(ds26,"\\pt2\\oc ");
      set_ds_dot_line(ds26);
    }

  /*
  OA = sqrt(OD*OD + np*np);
  dl = fabs(OD - OA);


  OE = OD - x * si;
  OA = sqrt(OE*OE + x * x * co * co);
  np = fabs(OE - OA);
  */

  // we do the right image
  if (xr != NULL)
    {
      for (i = 0; i < np; i++) norm[i] = xr[i] = 0; // xl[i] = 0;
      for (i = iys; i < iye ; i++)
	{
	  y = -y_r + i;
	  for (j=0; j< onx; j++)
	    {
	      x = x_r - j;
	      OE = OD - x * si;
	      OA = sqrt(OE*OE + x * x * co * co + y * y);
	      dl = fabs(OE - OA);
	      tmp = w2 + (x * x) - (y * y);
	      if (dl < np && tmp >= 0) //  && (fabs(y) < (float)w/2)
		{
		  tmp = sqrt(dl * ((2*OD) + dl));
		  if (x >= 0) jp = (int)((float)np/2 - tmp + 0.5);  
		  else jp = (int)((float)np/2 + tmp + 0.5);
		  if (jp >= 0 && jp < np)
		    {
		      get_raw_pixel_value(ois, j, i, &val);
		      xr[jp] += val;
		      norm[jp]++;
		    } 
		}
	    }
	}
      for (i = 0; i < np; i++) 
	xr[i] = (norm[i] != 0) ? xr[i]/norm[i] : xr[i];
      /*
      for (i = (int)(y_r+0.5) - w/2; i < (int)(y_r+0.5) + w/2 ; i++)
	{
	  jp = (int)(x_r+0.5) - np/2;
	  for (j = 0; j< np; j++)
	    {
	      get_raw_pixel_value(ois, jp+j, i, &val);
	      xl[j] += val;
	    } 
	}
      for (i = 0; i < np; i++) 	xl[i] /= w;
      */
    }
  if (yr != NULL)
    {
      for (i = 0; i < np; i++) norm[i] = yr[i] = 0; //yl[i] = 0;
      for (i = iys; i < iye ; i++)
	{
	  y = -y_r + i;
	  for (j=0; j< onx; j++)
	    {
	      x = x_r - j;
	      OE = OD - x * si;
	      OA = sqrt(OE*OE + x * x * co * co + y * y);
	      dl = fabs(OE - OA);
	      tmp = w2 + (y * y) - (x * x);
	      if (dl < np && tmp >= 0)
		{
		  tmp = sqrt(dl * ((2*OD) + dl));
		  if (y < 0) jp = (int)((float)np/2 - tmp + 0.5);  
		  else jp = (int)((float)np/2 + tmp + 0.5);
		  if (jp >= 0 && jp < np)
		    {
		      get_raw_pixel_value(ois, j, i, &val);
		      yr[jp] += val;
		      norm[jp]++;
		    } 
		}
	      tmp = sqrt(dl * ((2*OD) + dl));
	      if (ds != NULL && ((int)tmp == np/4) && ds->nx < ds->mx)
		{
		  ds->xd[ds->nx] = j;
		  ds->yd[ds->nx] = i;
		  ds->nx++;
		  ds->ny = ds->nx;
		}
	      if (ds3 != NULL && ((int)tmp == np/3) && ds3->nx < ds3->mx)
		{
		  ds3->xd[ds3->nx] = j;
		  ds3->yd[ds3->nx] = i;
		  ds3->nx++;
		  ds3->ny = ds3->nx;
		}
	      if (ds6 != NULL && ((int)tmp == np/6) && ds6->nx < ds6->mx)
		{
		  ds6->xd[ds6->nx] = j;
		  ds6->yd[ds6->nx] = i;
		  ds6->nx++;
		  ds6->ny = ds6->nx;
		}

	    }
	}
      for (i = 0; i < np; i++) 
	yr[i] = (norm[i] != 0) ? yr[i]/norm[i] : yr[i];
      /*
      jp = (int)(y_r+0.5) - np/2;
      for (i = 0; i < np ; i++)
	{
	  for (j = (int)(x_r+0.5) - w/2; j< (int)(x_r+0.5) + w/2; j++)
	    {
	      get_raw_pixel_value(ois, j, i+jp, &val);
	      yl[i] += val;
	    } 
	}
      for (i = 0; i < np; i++) 	yl[i] /= w;
      */
    }
  iys = (int)(y_l+0.5) - np/2;
  iys = (iys < 0) ? 0 : iys;
  iye = iys + np;
  iye = (iye > ois->im.ny) ? ois->im.ny : iye;
  // we do the left image
  if (xl != NULL)
    {
      for (i = 0; i < np; i++) norm[i] = xl[i] = 0;
      for (i = iys; i < iye ; i++)
	{
	  y = -y_l + i;
	  for (j=0; j< onx; j++)
	    {
	      x = -x_l + j;
	      OE = OD - x * si;
	      OA = sqrt(OE*OE + x * x * co * co + y * y);
	      dl = fabs(OE - OA);
	      tmp = w2 + (x * x) - (y * y);
	      if (dl < np && tmp >= 0)
		{
		  tmp = sqrt(dl * ((2*OD) + dl));
		  if (x < 0) jp = (int)((float)np/2 - tmp + 0.5);  
		  else jp = (int)((float)np/2 + tmp + 0.5);
		  if (jp >= 0 && jp < np)
		    {
		      get_raw_pixel_value(ois, j, i, &val);
		      xl[jp] += val;
		      norm[jp]++;
		    } 
		}
	    }
	}
      for (i = 0; i < np; i++) 
	xl[i] = (norm[i] != 0) ? xl[i]/norm[i] : xl[i];
    }
  if (yl != NULL)
    {
      for (i = 0; i < np; i++) norm[i] = yl[i] = 0;
      for (i = iys; i < iye ; i++)
	{
	  y = -y_l + i;
	  for (j=0; j< onx; j++)
	    {
	      x = -x_l + j;
	      OE = OD - x * si;
	      OA = sqrt(OE*OE + x * x * co * co + y * y);
	      dl = fabs(OE - OA);
	      tmp = w2 + (y * y) - (x * x);
	      if (dl < np && tmp >= 0)
		{
		  tmp = sqrt(dl * ((2*OD) + dl));
		  if (y < 0) jp = (int)((float)np/2 - tmp + 0.5);  
		  else jp = (int)((float)np/2 + tmp + 0.5);
		  if (jp >= 0 && jp < np)
		    {
		      get_raw_pixel_value(ois, j, i, &val);
		      yl[jp] += val;
		      norm[jp]++;
		    } 
		}
	      tmp = sqrt(dl * ((2*OD) + dl));
	      if (ds2 != NULL && ((int)tmp == np/4) && ds2->nx < ds2->mx)
		{
		  ds2->xd[ds2->nx] = j;
		  ds2->yd[ds2->nx] = i;
		  ds2->nx++;
		  ds2->ny = ds2->nx;
		}
	      if (ds23 != NULL && ((int)tmp == np/3) && ds23->nx < ds23->mx)
		{
		  ds23->xd[ds23->nx] = j;
		  ds23->yd[ds23->nx] = i;
		  ds23->nx++;
		  ds23->ny = ds23->nx;
		}
	      if (ds26 != NULL && ((int)tmp == np/6) && ds26->nx < ds26->mx)
		{
		  ds26->xd[ds26->nx] = j;
		  ds26->yd[ds26->nx] = i;
		  ds26->nx++;
		  ds26->ny = ds26->nx;
		}

	    }
	}
      for (i = 0; i < np; i++) 
	yl[i] = (norm[i] != 0) ? yl[i]/norm[i] : yl[i];
    }
  return 0;
}


int project_tilted_bead_profiles_interpol(O_i *ois,    // the full image 
				 float x_r, float y_r,   // the center of the right bead image in pixels
				 float x_l, float y_l,   // the center of the left bead image in pixels
				 float theta,            // the angle of illumination in radian
				 int np, int w,          // the number of points in profiles, minimum width
				 float *xr,              // the profile along x of the right image
				 float *yr,              // the profile along y of the right image
				 float *xl,              // the profile along x of the left image
				 float *yl,              // the profile along y of the left image
				 int draw_elipse)        // draw elipse on image
{
  register int i, j, k;
  int iys, iye, jp, onx, jk; // , ony
  double hc, OD, OE, OA, si, co, dx, val, pos, frac, weight; //, xc, yc
  float w2, norm[256], x, y, dl, tmp;
  O_p *op = NULL;
  d_s *ds = NULL, *ds3 = NULL, *ds6 = NULL;
  d_s *ds2 = NULL, *ds23 = NULL, *ds26 = NULL;

  onx = ois->im.nx;
  //ony = ois->im.ny;
  //xc = (x_r + x_l)/2;
  //yc = (y_r + y_l)/2;
  dx = (x_r - x_l)/2;
  w2 = w * w;
  si = sin(theta);
  co = cos(theta);
  hc = dx * co;
  hc = (si != 0) ? hc/si : 0;
  OD = sqrt(hc*hc + dx*dx);

  iys = (int)(y_r+0.5) - np/2;
  iys = (iys < 0) ? 0 : iys;
  iye = iys + np;
  iye = (iye > ois->im.ny) ? ois->im.ny : iye;


  if (draw_elipse)
    {
      op = create_and_attach_op_to_oi(ois, 4*np, 4*np, 0, IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
      if (op == NULL)	return win_printf_OK("cannot create plot !");
      ds = op->dat[0];
      ds->nx = ds->ny = 0;
      set_plot_symb(ds,"\\pt2\\oc ");
      set_ds_dot_line(ds);
      ds3 = create_and_attach_one_ds(op, 4*np, 4*np, 0);
      if (ds3 == NULL)	return win_printf_OK("cannot create plot !");
      ds3->nx = ds3->ny = 0;
      set_plot_symb(ds3,"\\pt2\\oc ");
      set_ds_dot_line(ds3);
      ds6 = create_and_attach_one_ds(op, 4*np, 4*np, 0);
      if (ds6 == NULL)	return win_printf_OK("cannot create plot !");
      ds6->nx = ds6->ny = 0;
      set_plot_symb(ds6,"\\pt2\\oc ");
      set_ds_dot_line(ds6);


      ds2 = create_and_attach_one_ds(op, 4*np, 4*np, 0);
      if (ds2 == NULL)	return win_printf_OK("cannot create plot !");
      ds2->nx = ds2->ny = 0;
      set_plot_symb(ds2,"\\pt2\\oc ");
      set_ds_dot_line(ds2);
      ds23 = create_and_attach_one_ds(op, 4*np, 4*np, 0);
      if (ds23 == NULL)	return win_printf_OK("cannot create plot !");
      ds23->nx = ds23->ny = 0;
      set_plot_symb(ds23,"\\pt2\\oc ");
      set_ds_dot_line(ds23);
      ds26 = create_and_attach_one_ds(op, 4*np, 4*np, 0);
      if (ds26 == NULL)	return win_printf_OK("cannot create plot !");
      ds26->nx = ds26->ny = 0;
      set_plot_symb(ds26,"\\pt2\\oc ");
      set_ds_dot_line(ds26);
    }

  // we do the right image
  if (xr != NULL)
    {
      for (i = 0; i < np; i++) norm[i] = xr[i] = 0; // xl[i] = 0;
      for (i = iys; i < iye ; i++)
	{
	  y = -y_r + i;
	  for (j=0; j< onx; j++)
	    {
	      x = x_r - j;
	      OE = OD - x * si;
	      OA = sqrt(OE*OE + x * x * co * co + y * y);
	      dl = fabs(OE - OA);
	      tmp = w2 + (x * x) - (y * y);
	      if (dl < np && tmp >= 0) //  && (fabs(y) < (float)w/2)
		{
		  tmp = sqrt(dl * ((2*OD) + dl));
		  /*
		  if (x >= 0) jp = (int)((float)np/2 - tmp + 0.5);  
		  else jp = (int)((float)np/2 + tmp + 0.5);
		  */
		  get_raw_pixel_value(ois, j, i, &val);
		  pos = (x >= 0) ? (double)(np/2)-tmp : (double)(np/2)+tmp;
		  jp = (int)pos;
		  //if (fabs(x) < 2.0 && fabs(y) < 2.0)
		  //win_printf("0 x = %g y %g \njp %d pos %g",x,y,jp,pos);

		  for (k = -2; k <= 2; k++)
		    {
		      jk = jp + k;
		      frac = pos - jk;
		      if ((fabs(frac) < 2.0) && (jk >= 0) && (jk < np))
			{
			  //  if (fabs(x) < 2.0 && fabs(y) < 2.0)
			  //win_printf("1 x = %g y %g \njp %d jk %d frac %g",x,y,jp,jk,frac);

			  weight = cos(M_PI*frac/2) + 1;		      
			  xr[jk] += (float)(val*weight);
			  norm[jk] += (float)weight;
			}
		      //else if (fabs(x) < 2.0 && fabs(y) < 2.0)
		      //win_printf("2 x = %g y %g \njp %d jk %d frac %g",x,y,jp,jk,frac);

		    }
		}
	    }
	}
      for (i = 0; i < np; i++) 
	xr[i] = (norm[i] != 0) ? xr[i]/norm[i] : xr[i];
    }
  if (yr != NULL)
    {
      for (i = 0; i < np; i++) norm[i] = yr[i] = 0; //yl[i] = 0;
      for (i = iys; i < iye ; i++)
	{
	  y = -y_r + i;
	  for (j=0; j< onx; j++)
	    {
	      x = x_r - j;
	      OE = OD - x * si;
	      OA = sqrt(OE*OE + x * x * co * co + y * y);
	      dl = fabs(OE - OA);
	      tmp = w2 + (y * y) - (x * x);
	      if (dl < np && tmp >= 0)
		{
		  tmp = sqrt(dl * ((2*OD) + dl));
		  get_raw_pixel_value(ois, j, i, &val);
		  pos = np/2;
		  pos += (y < 0) ? -tmp : tmp;
		  jp = (int)pos;
		  for (k = -2; k <= 2; k++)
		    {
		      jk = jp + k;
		      frac = pos - jk;
		      if ((fabs(frac) < 2) && (jk >= 0) && (jk < np))
			{
			  weight = cos(M_PI*frac/2) + 1;		      
			  yr[jk] += (float)(val*weight);
			  norm[jk] += (float)weight;
			}
		    }
		  /*
		  if (y < 0) jp = (int)((float)np/2 - tmp + 0.5);  
		  else jp = (int)((float)np/2 + tmp + 0.5);
		  if (jp >= 0 && jp < np)
		    {
		      get_raw_pixel_value(ois, j, i, &val);
		      yr[jp] += val;
		      norm[jp]++;
		    }
		  */ 
		}
	      tmp = sqrt(dl * ((2*OD) + dl));
	      if (ds != NULL && ((int)tmp == np/4) && ds->nx < ds->mx)
		{
		  ds->xd[ds->nx] = j;
		  ds->yd[ds->nx] = i;
		  ds->nx++;
		  ds->ny = ds->nx;
		}
	      if (ds3 != NULL && ((int)tmp == np/3) && ds3->nx < ds3->mx)
		{
		  ds3->xd[ds3->nx] = j;
		  ds3->yd[ds3->nx] = i;
		  ds3->nx++;
		  ds3->ny = ds3->nx;
		}
	      if (ds6 != NULL && ((int)tmp == np/6) && ds6->nx < ds6->mx)
		{
		  ds6->xd[ds6->nx] = j;
		  ds6->yd[ds6->nx] = i;
		  ds6->nx++;
		  ds6->ny = ds6->nx;
		}

	    }
	}
      for (i = 0; i < np; i++) 
	yr[i] = (norm[i] != 0) ? yr[i]/norm[i] : yr[i];
    }
  iys = (int)(y_l+0.5) - np/2;
  iys = (iys < 0) ? 0 : iys;
  iye = iys + np;
  iye = (iye > ois->im.ny) ? ois->im.ny : iye;
  // we do the left image
  if (xl != NULL)
    {
      for (i = 0; i < np; i++) norm[i] = xl[i] = 0;
      for (i = iys; i < iye ; i++)
	{
	  y = -y_l + i;
	  for (j=0; j< onx; j++)
	    {
	      x = -x_l + j;
	      OE = OD - x * si;
	      OA = sqrt(OE*OE + x * x * co * co + y * y);
	      dl = fabs(OE - OA);
	      tmp = w2 + (x * x) - (y * y);
	      if (dl < np && tmp >= 0)
		{
		  tmp = sqrt(dl * ((2*OD) + dl));
		  get_raw_pixel_value(ois, j, i, &val);
		  pos = np/2;
		  pos += (x < 0) ? -tmp : tmp;
		  jp = (int)pos;
		  for (k = -2; k <= 2; k++)
		    {
		      jk = jp + k;
		      frac = pos - jk;
		      if ((fabs(frac) < 2) && (jk >= 0) && (jk < np))
			{
			  weight = cos(M_PI*frac/2) + 1;		      
			  xl[jk] += (float)(val*weight);
			  norm[jk] += (float)weight;
			}
		    }
		  /*
		  if (x < 0) jp = (int)((float)np/2 - tmp + 0.5);  
		  else jp = (int)((float)np/2 + tmp + 0.5);
		  if (jp >= 0 && jp < np)
		    {
		      get_raw_pixel_value(ois, j, i, &val);
		      xl[jp] += val;
		      norm[jp]++;
		    }
		  */ 
		}
	    }
	}
      for (i = 0; i < np; i++) 
	xl[i] = (norm[i] != 0) ? xl[i]/norm[i] : xl[i];
    }
  if (yl != NULL)
    {
      for (i = 0; i < np; i++) norm[i] = yl[i] = 0;
      for (i = iys; i < iye ; i++)
	{
	  y = -y_l + i;
	  for (j=0; j< onx; j++)
	    {
	      x = -x_l + j;
	      OE = OD - x * si;
	      OA = sqrt(OE*OE + x * x * co * co + y * y);
	      dl = fabs(OE - OA);
	      tmp = w2 + (y * y) - (x * x);
	      if (dl < np && tmp >= 0)
		{
		  tmp = sqrt(dl * ((2*OD) + dl));
		  get_raw_pixel_value(ois, j, i, &val);
		  pos = np/2;
		  pos += (y < 0) ? -tmp : tmp;
		  jp = (int)pos;
		  for (k = -2; k <= 2; k++)
		    {
		      jk = jp + k;
		      frac = pos - jk;
		      if ((fabs(frac) < 2) && (jk >= 0) && (jk < np))
			{
			  weight = cos(M_PI*frac/2) + 1;		      
			  yl[jk] += (float)(val*weight);
			  norm[jk] += (float)weight;
			}
		    }
		  /*
		  if (y < 0) jp = (int)((float)np/2 - tmp + 0.5);  
		  else jp = (int)((float)np/2 + tmp + 0.5);
		  if (jp >= 0 && jp < np)
		    {
		      get_raw_pixel_value(ois, j, i, &val);
		      yl[jp] += val;
		      norm[jp]++;
		    }
		  */ 
		}
	      tmp = sqrt(dl * ((2*OD) + dl));
	      if (ds2 != NULL && ((int)tmp == np/4) && ds2->nx < ds2->mx)
		{
		  ds2->xd[ds2->nx] = j;
		  ds2->yd[ds2->nx] = i;
		  ds2->nx++;
		  ds2->ny = ds2->nx;
		}
	      if (ds23 != NULL && ((int)tmp == np/3) && ds23->nx < ds23->mx)
		{
		  ds23->xd[ds23->nx] = j;
		  ds23->yd[ds23->nx] = i;
		  ds23->nx++;
		  ds23->ny = ds23->nx;
		}
	      if (ds26 != NULL && ((int)tmp == np/6) && ds26->nx < ds26->mx)
		{
		  ds26->xd[ds26->nx] = j;
		  ds26->yd[ds26->nx] = i;
		  ds26->nx++;
		  ds26->ny = ds26->nx;
		}

	    }
	}
      for (i = 0; i < np; i++) 
	yl[i] = (norm[i] != 0) ? yl[i]/norm[i] : yl[i];
    }
  return 0;
}



int get_double_cross_profiles(void)
{
  register int i, j;
  imreg *imrs;	
  O_i *ois, *oid;
  O_p *op = NULL;
  d_s *ds, *ds2, *dsxr, *dsxl, *dsyr, *dsyl;
  int nf, im, iter;
  static int cl = 128, cw = 16, xc = 128, yc = 128, domovie = 0, dxi, niter = 10;
  int xi[256], yi1[256], yi2[256];
  float x1[256], y1[256], y2[256], corr, dx1, dx2, dy1, dy2, tmp, xr, yr, xl, yl;
  static float dx = 32, h = 32;
  double theta, alpha, L;

  if(updating_menu_state != 0)	return D_O_K;
  
  if (ac_grep(NULL,"%im%oi",&imrs,&ois) != 2)	
    return win_printf_OK("cannot find image in acreg!");
  
  nf = ois->im.n_f;
  //nx = ois->im.nx;
  //ois->im.ny;
  //color = makecol(255,64,64);
  
  i = win_scanf("Cross size %8d arm width %8d separation %8f\n"
		"center xc %8d yc %8d bead height %8f niter %8d\n"
		"treat movie NO%R new%r old%r\n"
		,&cl,&cw,&dx,&xc,&yc,&h,&niter,&domovie);
  if (i == WIN_CANCEL)	return OFF; 

  dxi = (int)(dx+0.5);

  fill_avg_profiles_double_cross_from_im(ois,  xc, yc, dxi, cl, cw, xi, yi1, yi2);

  check_double_bead_image_not_lost(xi, x1, yi1, y1, yi2, y2, cl, 8);
  if (fft_init(2*cl) || filter_init(2*cl))	
    return win_printf_OK("cannot init fft!");	
  dx1 = find_distance_from_center(x1, 2*cl, 0, cl/2, 1,&corr);
  if (fft_init(cl) || filter_init(cl))	
    return win_printf_OK("cannot init fft!");	
  dy1 = find_distance_from_center(y1, cl, 0, cl/4, 1,&corr);
  dy2 = find_distance_from_center(y2, cl, 0, cl/4, 1,&corr);

  //if (fabs(dx1) > 10 || fabs(dy1+dy2) > 20) 
    win_printf_OK("strange shift dx = %g dy = %g",dx1,(dy1+dy2)/2);	
  xc += (int)(dx1+0.5);
  yc += (int)(((dy1+dy2)/2)+0.5);

  fill_avg_profiles_double_cross_from_im(ois,  xc, yc, dxi, cl, cw, xi, yi1, yi2);


  op = create_and_attach_op_to_oi(ois, 2*cl, 2*cl, 0, 0);
  if (op == NULL)	return win_printf_OK("cannot create plot !");
  ds = op->dat[0];
  uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
  set_plot_title(op, "\\stack{{Double cross profiles}{cw = %d dx = %g h = %g}}",cw,dx,h);
  op->filename = my_sprintf(op->filename,"double_cross_profiles.gr");
  for(i = 0; i< 2*cl; i++)
    {
      ds->xd[i] = i; 
      ds->yd[i] = xi[i]; 
    }
  ds = create_and_attach_one_ds(op, cl, cl, 0);
  if (ds == NULL)	return win_printf_OK("cannot create plot !");
  for(i = 0; i< cl; i++)
    {
      ds->xd[i] = i; 
      ds->yd[i] = yi1[i]; 
    }

  ds = create_and_attach_one_ds(op, cl, cl, 0);
  if (ds == NULL)	return win_printf_OK("cannot create plot !");
  theta = atan(dx/h);
  L = sqrt(dx*dx + h*h);
  win_printf("\\theta = %g x_0 = %g", theta, h * tan(theta));
  for(i = 0; i< cl; i++)
    {
      alpha = atan((double)(i - cl/2)/L);
      ds->xd[i] = h * tan(theta + alpha);
      ds->yd[i] = yi1[i]; 
    }

  ds = create_and_attach_one_ds(op, cl, cl, 0);
  if (ds == NULL)	return win_printf_OK("cannot create plot !");
  for(i = 0; i< cl; i++)
    {
      ds->xd[i] = i; 
      ds->yd[i] = yi2[i]; 
    }



  op->need_to_refresh = 1;  

  op = create_and_attach_op_to_oi(ois, cl, cl, 0, 0);
  if (op == NULL)	return win_printf_OK("cannot create plot !");
  ds = op->dat[0];
  uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
  op->filename = my_sprintf(op->filename,"double_cross_profiles.gr");
  for(i = 0; i< cl; i++)
    {
      fill_avg_profiles_double_cross_from_im(ois, xc, yc, i, cl, cw, xi, yi1, yi2);
      for(j = 0, tmp = 0; j< cl; j++) tmp += yi1[j];
      for(j = 0, tmp /= cl; j< cl; j++) y1[j] = (float)yi1[j] - tmp;
      for(j = 0, tmp = 0; j< cl; j++) tmp += yi2[j];
      for(j = 0, tmp /= cl; j< cl; j++) y2[j] = (float)yi2[j] - tmp;
      for(j = 0, tmp = 0; j< cl; j++) tmp += y1[j] * y2[j];
      tmp /= cl;
      ds->xd[i] = i; 
      ds->yd[i] = tmp; 
    }
  find_max1(ds->yd, cl, &tmp, &corr);
  set_plot_title(op, "Double cross Y profiles cross-corelation dx found %g",tmp);


  op = create_and_attach_op_to_oi(ois, cl, cl, 0, 0);
  if (op == NULL)	return win_printf_OK("cannot create plot !");
  dsxr = op->dat[0];
  uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
  op->filename = my_sprintf(op->filename,"double_cross_profiles_cor.gr");
  dsyr = create_and_attach_one_ds(op, cl, cl, 0);
  if (dsyr == NULL)	return win_printf_OK("cannot create plot !");
  dsxl = create_and_attach_one_ds(op, cl, cl, 0);
  if (dsxl == NULL)	return win_printf_OK("cannot create plot !");
  dsyl = create_and_attach_one_ds(op, cl, cl, 0);
  if (dsyl == NULL)	return win_printf_OK("cannot create plot !");

  xr = (float)(xc+dx);
  yr = (float)(yc+dy2);
  xl = (float)(xc-dx);
  yl = (float)(yc+dy2);
  project_tilted_bead_profiles(ois, xr, yr, xl, yl,   
			       (float)theta, cl, cw, dsxr->yd, dsyr->yd, dsxl->yd, dsyl->yd, 0);

  for (i = 0; i < cl; i++) dsxr->xd[i] = dsyr->xd[i] = dsxl->xd[i] = dsyl->xd[i] = i;


  if (fft_init(cl) || filter_init(cl))     return win_printf_OK("cannot init fft!");	

  for(iter = 0; iter < niter; iter++)
    {
      dx1 = find_distance_from_center(dsxr->yd, cl, 0, cl/4, 1,&corr);
      dy1 = find_distance_from_center(dsyr->yd, cl, 0, cl/4, 1,&corr);
      xr += dx1;
      yr += dy1;
      
      dx2 = find_distance_from_center(dsxl->yd, cl, 0, cl/4, 1,&corr);
      dy2 = find_distance_from_center(dsyl->yd, cl, 0, cl/4, 1,&corr);
      xl += dx2;
      yl += dy2;
      project_tilted_bead_profiles(ois, xr, yr, xl, yl,   
				   (float)theta, cl, cw, dsxr->yd, dsyr->yd, dsxl->yd, dsyl->yd, 0);
    }
  //if (fabs(dx1) > 10 || fabs(dy1+dy2) > 20) 
    win_printf_OK("Right image x = %g y = %g, dx1 %g dy1 %g\n"
		  "Left image x = %g y = %g, dx2 %g dy2 %g",xr,yr,dx1,dy1,xl,yl,dx2,dy2);	

  set_ds_source(dsxr,"X profile center on xr %g yr %g",xr, yr);
  set_ds_source(dsyr,"Y profile center on xr %g yr %g",xr, yr);
  set_ds_source(dsxl,"X profile center on xr %g yr %g",xl, yl);
  set_ds_source(dsyl,"Y profile center on xr %g yr %g",xl, yl);


  project_tilted_bead_profiles_interpol(ois, xr, yr, xl, yl,   
			       (float)theta, cl, cw, dsxr->yd, dsyr->yd, dsxl->yd, dsyl->yd, 1);




  op->need_to_refresh = 1;  
  if (nf < 2 || domovie == 0) return broadcast_dialog_message(MSG_DRAW,0);

  op = create_and_attach_op_to_oi(ois, nf, nf, 0, IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
  if (op == NULL)	return win_printf_OK("cannot create plot !");
  ds = op->dat[0];
  ds2 = create_and_attach_one_ds(op, nf, nf, 0);
  if (ds2 == NULL)	return win_printf_OK("cannot create plot !");

  for (im = 0; im < nf; im++)
    {
      my_set_window_title("Im %d",im);
      switch_frame(ois,im);		/* no negative index */
      //win_printf("image %d",im);
      for(iter = 0; iter < niter; iter++)
	{
	  project_tilted_bead_profiles(ois, xr, yr, xl, yl,   
				       (float)theta, cl, cw, dsxr->yd, dsyr->yd, dsxl->yd, dsyl->yd, 0);
	  dx1 = find_distance_from_center(dsxr->yd, cl, 0, cl/4, 1,&corr);
	  dy1 = find_distance_from_center(dsyr->yd, cl, 0, cl/4, 1,&corr);
	  xr += dx1;
	  yr += dy1;
	  
	  dx2 = find_distance_from_center(dsxl->yd, cl, 0, cl/4, 1,&corr);
	  dy2 = find_distance_from_center(dsyl->yd, cl, 0, cl/4, 1,&corr);
	  xl += dx2;
	  yl += dy2;

	}
      ds->xd[im] = xr;
      ds->yd[im] = yr;
      ds2->xd[im] = xl;
      ds2->yd[im] = yl;
      //win_printf_OK("image %d\nRight image x = %g y = %g, dx1 %g dy1 %g\n"
      //	    "Left image x = %g y = %g, dx2 %g dy2 %g",im,xr,yr,dx1,dy1,xl,yl,dx2,dy2);	
    }
  op->need_to_refresh = 1;  
  if (nf < 2 || domovie < 2) return broadcast_dialog_message(MSG_DRAW,0);



  oid = create_and_attach_oi_to_imr(imrs, cl, nf, IS_FLOAT_IMAGE);
  if (oid == NULL)		return win_printf("cannot create image!");
  op = create_and_attach_op_to_oi(oid, nf, nf, 0, IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
  if (op == NULL)	return win_printf_OK("cannot create plot !");
  ds = op->dat[0];

  for (im = 0; im < nf; im++)
    {
      my_set_window_title("Im %d",im);
      switch_frame(ois,im);		/* no negative index */
      fill_avg_profiles_double_cross_from_im(ois,  xc, yc, dx, cl, cw, xi, yi1, yi2);
      check_double_bead_image_not_lost(xi, x1, yi1, y1, yi2, y2, cl, 8);
      if (fft_init(2*cl) || filter_init(2*cl))	
	return win_printf_OK("cannot init fft!");	
      dx1 = find_distance_from_center(x1, 2*cl, 0, cl/2, 1,&corr);
      if (fft_init(cl) || filter_init(cl))	
	return win_printf_OK("cannot init fft!");	
      dy1 = find_distance_from_center(y1, cl, 0, cl/4, 1,&corr);
      dy2 = find_distance_from_center(y2, cl, 0, cl/4, 1,&corr);
      xc += (int)(dx1+0.5);
      yc += (int)(((dy1+dy2)/2)+0.5);
      for(i = 0; i< cl; i++)
	{
	  fill_avg_profiles_double_cross_from_im(ois, xc, yc, i, cl, cw, xi, yi1, yi2);
	  for(j = 0, tmp = 0; j< cl; j++) tmp += yi1[j];
	  for(j = 0, tmp /= cl; j< cl; j++) y1[j] = (float)yi1[j] - tmp;
	  for(j = 0, tmp = 0; j< cl; j++) tmp += yi2[j];
	  for(j = 0, tmp /= cl; j< cl; j++) y2[j] = (float)yi2[j] - tmp;
	  for(j = 0, tmp = 0; j< cl; j++) tmp += y1[j] * y2[j];
	  tmp /= cl;
	  oid->im.pixel[im].fl[i] = tmp;
	}
      find_max1(oid->im.pixel[im].fl, cl, &tmp, &corr);
      ds->xd[im] = tmp;
      ds->yd[im] = im;
    }

  find_zmin_zmax(oid);	
  oid->need_to_refresh = ALL_NEED_REFRESH;



  return broadcast_dialog_message(MSG_DRAW,0);
}


MENU *trmov_image_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"Movie track in X,Y", follow_bead_in_x_y_from_movie, NULL, 0, NULL);
	add_item_to_menu(mn,"Movie X track in X,Y", follow_bead_in_x_y_X_from_movie, NULL, 0, NULL);
	add_item_to_menu(mn,"Movie track 2 bds", follow_2_beads_in_x_y_from_movie, NULL, 0, NULL);



	add_item_to_menu(mn,"Track in X,Y,Z", follow_bead_in_x_y_z_amp_phi_not_power_2_from_movie, NULL, MENU_INDEX(0), NULL);
	add_item_to_menu(mn,"Track in X,Y,Z plot", follow_bead_in_x_y_z_amp_phi_not_power_2_from_movie_with_plot, NULL,  MENU_INDEX(0), NULL);

	add_item_to_menu(mn,"Movie track ortho", follow_bead_in_x_y_from_movie_ortho, NULL, 0, NULL);
	add_item_to_menu(mn,"Movie ortho movie", follow_bead_in_x_y_from_movie_ortho_movie, NULL, 0, NULL);

	add_item_to_menu(mn,"correl ortho", image_correlation_profile, NULL, 0, NULL);
	add_item_to_menu(mn,"correl ortho new", image_correlation_profile_cor, NULL, 0, NULL);
	add_item_to_menu(mn,"correl ortho n*n-1", image_correlation_profile_n_n_1, NULL, 0, NULL);


	add_item_to_menu(mn,"correl ortho mov", movie_correlation_imagee, NULL, 0, NULL);
	add_item_to_menu(mn,"correl ortho image", movie_correlation_profile, NULL, 0, NULL);
	add_item_to_menu(mn,"correl ortho image modified", movie_correlation_profile_modified, NULL, 0, NULL);

	add_item_to_menu(mn,"Radial profile", do_radial_2, NULL, 0, NULL);
	add_item_to_menu(mn,"Radial fixed", do_radial_fixed, NULL, 0, NULL);
	add_item_to_menu(mn,"Ortoradial profile", do_orthoradial, NULL, 0, NULL);
	add_item_to_menu(mn,"Ortoradial image", do_orthoradial_im, NULL, 0, NULL);
	add_item_to_menu(mn,"Ortho to cartesian", do_orthoradial_to_cartesian_im, NULL, 0, NULL);
	add_item_to_menu(mn,"Create Movie", do_movie, NULL, 0, NULL);
	add_item_to_menu(mn,"Create Movie O/E", do_movie_odd_even, NULL, 0, NULL);
	add_item_to_menu(mn,"Check calibration", test_calibration_image, NULL, 0, NULL);
	add_item_to_menu(mn,"Kx of calibration", calibration_image_wavenumber, NULL, 0, NULL);
	add_item_to_menu(mn,"Kx of profile", calibration_profile_wavenumber, NULL, 0, NULL);
	add_item_to_menu(mn,"resample calibration", do_trmov_image_resample, NULL, 0, NULL);
	add_item_to_menu(mn,"check timing", do_check_timing, NULL, 0, NULL);
    add_item_to_menu(mn,"Angel_compar", angel_compare, NULL, 0, NULL);
    add_item_to_menu(mn,"Angel_compar2", angel_compare2, NULL, 0, NULL);
    add_item_to_menu(mn,"Angel_compar_avg", angel_compare_avg, NULL, 0, NULL);

    add_item_to_menu(mn,"Double cross profiles", get_double_cross_profiles, NULL, 0, NULL);

	/*	add_item_to_menu(mn,"Track in X,Y", follow_bead_in_x_y_from_ifc, NULL, 0, NULL);*/
	return mn;
}


VOID CALLBACK TimerProc1(HWND hwnd, UINT uMsg, UINT_PTR idEvent,  DWORD dwTime)
{
  (void)hwnd;
  (void)uMsg;
  (void)idEvent;
  display_title_message("event %d",dwTime);
};






int	trmov_main(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  add_image_treat_menu_item ( "trmov", NULL, trmov_image_menu(), 0, NULL);
  //SetTimer( win_get_window(),  25,    2000, TimerProc1 );
  return D_O_K;
}
int	trmov_unload(int argc, char **argv)
{ 
  (void)argc;
  (void)argv;
  remove_item_to_menu(image_treat_menu, "trmov", NULL, NULL);
  return D_O_K;
}

#endif


#ifdef TEMPLATE



int do_trmov_average_along_y(void)
{
	register int i, j;
	int l_s, l_e;
	O_i *ois = NULL;
	O_p *op = NULL;
	d_s *ds;
	imreg *imr = NULL;


	if(updating_menu_state != 0)	return D_O_K;
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine average the image intensity"
			"of several lines of an image");
	}
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	return D_O_K;
	/* we grap the data that we need, the image and the screen region*/
	l_s = 0; l_e = ois->im.ny;
	/* we ask for the limit of averaging*/
	i = win_scanf("Average Between lines%d and %d",&l_s,&l_e);
	if (i == WIN_CANCEL)	return D_O_K;
	/* we create a plot to hold the profile*/
	op = create_one_plot(ois->im.nx,ois->im.nx,0);
	if (op == NULL) return (win_printf_OK("cant create plot!"));
	ds = op->dat[0]; /* we grab the data set */
	op->y_lo = ois->z_black;	op->y_hi = ois->z_white;
	op->iopt2 |= Y_LIM;	op->type = IM_SAME_X_AXIS;
	inherit_from_im_to_ds(ds, ois);
	op->filename = Transfer_filename(ois->filename);
	set_plot_title(op,"trmov averaged profile from line %d to %d",l_s, l_e);
	set_formated_string(&ds->treatement,"%s averaged profile from line %d to %d",l_s, l_e);
	uns_oi_2_op(ois, IS_Z_UNIT_SET, op, IS_Y_UNIT_SET);
	uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
	for (i=0 ; i< ds->nx ; i++)	ds->yd[i] = 0;
	for (i=l_s ; i< l_e ; i++)
	{
		extract_raw_line(ois, i, ds->xd);
		for (j=0 ; j< ds->nx ; j++)	ds->yd[j] += ds->xd[j];
	}
	for (i=0, j = l_e - l_s ; i< ds->nx && j !=0 ; i++)
	{
		ds->xd[i] = i;
		ds->yd[i] /= j;
	}
	add_one_image (ois, IS_ONE_PLOT, (void *)op);
	ois->cur_op = ois->n_op - 1;
	broadcast_dialog_message(MSG_DRAW,0);
	return D_REDRAWME;
}

O_i	*trmov_image_multiply_by_a_scalar(O_i *ois, float factor)
{
	register int i, j;
	O_i *oid;
	float tmp;
	int onx, ony, data_type;
	union pix *ps, *pd;

	onx = ois->im.nx;	ony = ois->im.ny;	data_type = ois->im.data_type;
	oid =  create_one_image(onx, ony, data_type);
	if (oid == NULL)
	{
		win_printf("Can't create dest image");
		return NULL;
	}
	if (data_type == IS_CHAR_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].ch[j];
				pd[i].ch[j] = (tmp < 0) ? 0 :
					((tmp > 255) ? 255 : (unsigned char)tmp);
			}
		}
	}
	else if (data_type == IS_INT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].in[j];
				pd[i].in[j] = (tmp < -32768) ? -32768 :
					((tmp > 32767) ? 32767 : (short int)tmp);
			}
		}
	}
	else if (data_type == IS_FLOAT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].fl[j] = factor * ps[i].fl[j];
			}
		}
	}
	else if (data_type == IS_COMPLEX_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].fl[2*j] = factor * ps[i].fl[2*j];
				pd[i].fl[2*j+1] = factor * ps[i].fl[2*j+1];
			}
		}
	}
	else if (data_type == IS_RGB_PICTURE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< 3*onx; j++)
			{
				tmp = factor * ps[i].ch[j];
				pd[i].ch[j] = (tmp < 0) ? 0 :
					((tmp > 255) ? 255 : (unsigned char)tmp);
			}
		}
	}
	inherit_from_im_to_im(oid,ois);
	uns_oi_2_oi(oid,ois);
	oid->im.win_flag = ois->im.win_flag;
	if (ois->title != NULL)	set_im_title(oid, "%s rescaled by %g",
		 ois->title,factor);
	set_formated_string(&oid->im.treatement,
		"Image %s rescaled by %g", ois->filename,factor);
	return oid;
}

# endif
