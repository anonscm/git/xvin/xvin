#ifndef _TRMOV_H_
#define _TRMOV_H_


# define 	WRONG_ARGUMENT		-1
# define 	OUT_MEMORY			-2
# define 	FFT_NOT_SUPPORTED	-15
# define	PB_WITH_EXTREMUM	2
# define	TOO_MUCH_NOISE		4
# define	GETTING_REF_PROFILE	1

# define        clip_X_cross_in_image(oi, x, y, l2, w2) \
		(x) = ((x) < (oi)->im.nx - (l2) - (w2)) ? (x) : (oi)->im.nx - (l2) - (w2);\
		(y) = ((y) < (oi)->im.ny - (l2) - (w2)) ? (y) : (oi)->im.ny - (l2) - (w2);\
		(x) = ((x) < (l2)+(w2)) ? (l2)+(w2) : (x);\
		(y) = ((y) < (l2)+(w2)) ? (l2)+(w2) : (y)

# define        clip_cross_in_image(oi, x, y, l2) \
		(x) = ((x) < (oi)->im.nx - (l2)) ? (x) : (oi)->im.nx - (l2);\
		(y) = ((y) < (oi)->im.ny - (l2)) ? (y) : (oi)->im.ny - (l2);\
		(x) = ((x) < (l2)) ? (l2) : (x);\
		(y) = ((y) < (l2)) ? (l2) : (y)




typedef struct _sym_radial_profile
{
	float 	ax;			/* x scaling factor */
	float	ay;			/* y scaling factor */
	int 	npc;		/* nb of point in profile */
	int		rc;			/* critical circle */
	int		bp_center;	/* band-pass center filter */
	int		bp_width;	/* band-pass width filter */	
	float	z_by_rad;	/* how many micron by radian */
	float	z0;			/* the position of profile */
	float 	*pz;		/* the array to store profile */
	float 	fb_p;		/* the proportionnal feedback */
	float 	fb_i;		/* the integral feedback */	
	float 	fb_d;		/* the derivate feedback */
	float	dac_max;	/* the max value for dac  (10V) */		
	float	dac_min;	/* the min value for dac  (0V) */		
	float   z_step;
} sym_radial_profile;




PXV_FUNC(int,	draw_cross_vga_screen_unit, (int xc, int yc, int length, int width, int color, imreg *imrs, BITMAP* bm));

PXV_FUNC(int, draw_cross_vga, (int xc, int yc, int length, int width, int color, imreg *imrs, BITMAP* bm, DIALOG *d));
PXV_FUNC(int,draw_cross_X_vga, (int xc, int yc, int length, int width, int color, imreg *imrs, BITMAP* bm, DIALOG *d));
PXV_FUNC(int, move_cross, (int *xc, float xcf, int *yc, float ycf, int cl, int cw, imreg *imrs, BITMAP* bm, DIALOG *d));
PXV_FUNC(int, move_X_cross, (int *xc, float xcf, int *yc, float ycf, int cl, int cw, imreg *imrs, BITMAP* bm, DIALOG *d));

PXV_FUNC(int, erase_around_black_circle, (int *x, int *y, int cl));
PXV_FUNC(int, check_bead_not_lost, (int *x, float *x1, int *y, float *y1, int cl, float bd_mul));
PXV_FUNC(int, grab_all_odd_line_from_movie, (O_i *oi, int ytt, int xll, O_i *movie));
PXV_FUNC(int, grab_all_even_line_from_movie, (O_i *oi, int ytt, int xll, O_i *movie));
PXV_FUNC(int, fill_odd_avg_profile_from_im, (O_i *oi, int xc, int yc, int cl, int cw, int *x, int *y));
PXV_FUNC(int, fill_avg_profiles_from_im, (O_i *oi, int xc, int yc, int cl, int cw, int *x, int *y));
PXV_FUNC(int, fill_odd_avg_profile, (union pix *pd, int ytt, int cl, int cw, int *x, int *y));
PXV_FUNC(int, fill_even_avg_profile, (union pix *pd, int ytt, int cl, int cw, int *x, int *y));
PXV_FUNC(int, fill_odd_avg_X_profile, (union pix *pd, int ytt, int cl, int cw, int *x, int *y));
PXV_FUNC(int, fill_even_avg_X_profile, (union pix *pd, int ytt, int cl, int cw, int *x, int *y));

PXV_FUNC(int, refresh_screen_image_and_X_cross, (imreg *imrs, O_i *movie, DIALOG *d, int xc, int yc, int cl, int cw, int color));

PXV_FUNC(int, refresh_screen_image_and_cross, (imreg *imrs, O_i *movie, DIALOG *d, int xc, int yc, int cl, int cw, int color));

PXV_FUNC(int, prepare_filtered_profile, (float *zp, int nx, int flag, int filter, int width));
PXV_FUNC(float, find_simple_phase_shift_between_filtered_profile, (float *zr, float *zp, int nx, int rc));
PXV_FUNC(O_i*, image_band_pass_in_real_out_complex, (O_i *dst, O_i *oi,int w_flag, int filter, int width, int rc));
PXV_FUNC(float,	find_distance_from_center, (float *x1, int cl, int flag, int filter, int black_circle, float *corr));
PXV_FUNC(float,	find_distance_from_center_by_phase, (float *x1, int cl, int flag, int filter, int black_circle, float *corr));
PXV_FUNC(float, *radial_non_pixel_square_image_sym_profile_in_array_fixed, (float *rz, O_i *ois, float xc, float yc, int r_max, float ax, float ay));
PXV_FUNC(int, fftwindow_flat_top1, (int npts, float *x, float smp));
PXV_FUNC(int, deplace, (float *x, int nx));
PXV_FUNC(int, correlate_1d_sig_and_invert, (float *x1, int nx, int window, float *fx1, int filter, int remove_dc));
PXV_FUNC(int, correlate_1d_sig_and_invert_in_fourier, (float *x1, int nx, int window, float *fx1, int filter, int remove_dc));
PXV_FUNC(int, correlate_2_1d_sig, (float *x1, int nx, float *x2, int filter, int remove_dc, float *corr, int norm));

PXV_FUNC(int, find_max1, (float *x, int nx, float *Max_pos, float *Max_val));
PXV_FUNC(int, track_in_x_y_from_movie, (imreg *imrs, O_i *oid, int *xci, int *yci, int cl, int cw, int nf, int imstart, int black_circle, float *xb, float *yb, O_i *oip, O_i *oiz));

PXV_FUNC(int, track_in_x_y_z_amp_phi_not_power_2_from_movie, (imreg *imrs, O_i *oid, int *xci, int *yci, int cl, int cw, int nf, int imstart, int black_circle, float *xb, float *yb, sym_radial_profile *srp, float *zb, int verbose, O_i *oir, float *error, int rout, O_i *oip));
PXV_FUNC(int, place_tracking_cross_with_mouse_movie, (int *xc, int *yc, int cl, int cw, int color));
PXV_FUNC(int, follow_bead_in_x_y_from_movie, (void));

PXV_FUNC(int,	do_movie, (void));
PXV_FUNC(int,	do_movie_odd_even, (void));

PXV_FUNC(int, do_radial, (void));
PXV_FUNC(int, do_radial_fixed, (void));

PXV_FUNC(int, do_trmov_average_along_y, (void));
PXV_FUNC(MENU*, trmov_image_menu, (void));
PXV_FUNC(int, trmov_main, (int argc, char **argv));
#endif

