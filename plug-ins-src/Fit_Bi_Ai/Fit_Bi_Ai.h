#pragma once
PXV_FUNC(int, do_Fit_Bi_Ai_rescale_plot, (void));
PXV_FUNC(MENU*, Fit_Bi_Ai_plot_menu, (void));
PXV_FUNC(int, do_Fit_Bi_Ai_rescale_data_set, (void));
PXV_FUNC(int, Fit_Bi_Ai_main, (int argc, char **argv));
