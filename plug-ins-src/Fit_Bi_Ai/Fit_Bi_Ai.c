/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _FIT_BI_AI_C_
#define _FIT_BI_AI_C_

#include <allegro.h>
# include "xvin.h"

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "Fit_Bi_Ai.h"


// E = (kG/2)*(dx^2 + 2*dx.x0i) + c
// Eb (sliding) dx = lambda
// EE (escaping) dx = lambda+delta
// Er (processivity) dx = lambda+gamma
// c = offset


static float delta = 1, offset = 1, gammav = 0.4, lambda = 1;
int fit_r = 1;

int Bi_Ai_compute_chi2(d_s *dse, // contains ln(tau_e)
		       d_s *dsb, // contains ln(tau_r)
		       d_s *dsr, // contains ln(tau_b)
		       d_s *xbi, // contains the enzyme specific shift
		       float deltal, // the distance in x between tau_e and tau_b
		       float off,  // the offset in y
		       float gam, // the distance in x between tau_r and tau_b = gam * delta
		       float lamb, 
		       double *chi2)
{
  double lchi2 = 0, A, erA, B, erB, C, erC, pA, pB, pC, x;
  int i;

  (void)off;
  if (dse == NULL || dsb == NULL || dsr == NULL || xbi == NULL) return 1;
  if (dse->ye == NULL || dsb->ye == NULL || dsr->ye == NULL) return 1;
  if (dse->nx != dsb->nx || dse->nx != xbi->nx || dse->nx != dsr->nx) return 2;
  for (i = 0; i < dse->nx; i++)
    {
      A = dse->yd[i];// - dsb->yd[i];
      B = dsb->yd[i];// + dsb->yd[i];
      C = dsr->yd[i];
      //erA = sqrt(dse->ye[i]*dse->ye[i] + dsb->ye[i]*dsb->ye[i]);
      erA = (dse->yed != NULL) ? (fabs(dse->ye[i]) + fabs(dse->yed[i]))/2 : dse->ye[i];
      erB = (dsb->yed != NULL) ? (fabs(dsb->ye[i]) + fabs(dsb->yed[i]))/2 : dsb->ye[i];      
      erC = (dsr->yed != NULL) ? (fabs(dsr->ye[i]) + fabs(dsr->yed[i]))/2 : dsr->ye[i];      
      if (erA <= 0 || erB <= 0 || erB <= 0)
	{
	  win_printf("Zero error ! cannot divide");
	  return 3;
	}
      x = xbi->yd[i];
      pB = x + (lamb*lamb)/2;
      pA = x + (lamb + deltal) * (lamb + deltal)/2;
      pC = x + (lamb + gam) * (lamb + gam)/2;
      pA -= A;
      pB -= B;
      pC -= C;      
      lchi2 += (pA * pA)/(erA*erA);
      lchi2 += (pB * pB)/(erB*erB);
      if (fit_r) lchi2 += (pC * pC)/(erC*erC);      
    }
  if (chi2) *chi2 = lchi2;
  return 0;
}

int update_fits(O_p *op, d_s *dsxbi)
{
  int i;
  float x;
  d_s *dsfb = NULL, *dsfr = NULL, *dsfe = NULL;

  if (op == NULL || dsxbi == NULL) return 1;
  
  dsfe = find_source_specific_ds_in_op(op, "Fit to ln(\\tau_{Ei})");
  if (dsfe == NULL)
    {
      dsfe = create_and_attach_one_ds(op, dsxbi->nx, dsxbi->nx, 0);
      if (dsfe != NULL) set_ds_source(dsfe, "%s", "Fit to ln(\\tau_{Ei})");  
    }
  dsfb = find_source_specific_ds_in_op(op, "Fit to ln(\\tau_{bi})");
  if (dsfb == NULL)    
    {
      dsfb = create_and_attach_one_ds(op, dsxbi->nx, dsxbi->nx, 0);
      if (dsfb != NULL) set_ds_source(dsfb, "%s", "Fit to ln(\\tau_{bi})");        
    }
  dsfr = find_source_specific_ds_in_op(op, "Fit to ln(\\tau_{ri})");
  if (dsfr == NULL)  
    {
      dsfr = create_and_attach_one_ds(op, dsxbi->nx, dsxbi->nx, 0);
      if (dsfr != NULL) set_ds_source(dsfr, "%s", "Fit to ln(\\tau_{ri})");        
    }

  if (dsfb != NULL)
    {
      for (i = 0; i < dsxbi->nx; i++)
	{
	  x = dsxbi->yd[i];
	  dsfb->yd[i] = x + (lambda * lambda)/2;
	  dsfb->xd[i] = i;
	}
    }
  if (dsfe != NULL)
    {
      for (i = 0; i < dsxbi->nx; i++)
	{
	  x = dsxbi->yd[i];
	  dsfe->yd[i] = x + ((lambda + delta) * (lambda + delta))/2;
	  dsfe->xd[i] = i;
	}
    }
  if (dsfr != NULL)
    {
      for (i = 0; i < dsxbi->nx; i++)
	{
	  x = dsxbi->yd[i];
	  dsfr->yd[i] = x + ((lambda + gammav) * (lambda + gammav))/2;
	  dsfr->xd[i] = i;
	}
    }
  return 0;
}

int do_Fit_Bi_Ai_compute_chi2(void)
{
  int i;
  O_p *op = NULL;
  d_s *dse, *dsb, *dsr, *dsxbi;
  pltreg *pr = NULL;
  static int idsei = 0, idsbi = 1, idsri = 2, idsxbi = 3;
  double chi2 = 0;
  float del, gam, off, lam;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number and place it in a new plot");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
    return win_printf_OK("cannot find data");
  if (op->n_dat < 3)     return win_printf_OK("not enough datasets !");
  del = delta;
  gam = gammav;
  off = offset;
  lam = lambda;
  i = win_scanf("For: ln(\\tau_{Ei}) - ln(\\tau_{bi}) = \\Delta^2 + 2\\Delta.x_{bi}\n"
		"ln(\\tau_{Ei}) + ln(\\tau_{bi}) = \\Delta^2 + 2\\Delta.x_{bi} + 2.x_{bi}^2\n"
		"%3d Index of ds containing ln(\\tau_{Ei})\n"
		"%3d Index of ds containing ln(\\tau_{bi})\n"
		"%3d Index of ds containing ln(\\tau_{ri})\n"		
		"%3d Index of ds containing x_{bi}\n"
		"Compute best \\chi^2 give \\Delta   %5f\n"
		"and \\gamma %5f\n"
		"offset %5f \\lambda = %5f\n"
		"fit middle point %b"
		,&idsei,&idsbi,&idsri,&idsxbi,&del,&gam,&off,&lam,&fit_r);
  if (i == WIN_CANCEL)	return OFF;
  dse = op->dat[idsei];
  dsb = op->dat[idsbi];
  dsr = op->dat[idsri];  
  dsxbi = op->dat[idsxbi];
  if (Bi_Ai_compute_chi2(dse, dsb, dsr, dsxbi, del, off, gam, lam, &chi2))
    {
      return win_printf_OK("Pb while computing \\chi^2 !");
    }
  win_printf("For \\Delta = %g and \\gamma = %g\n"
	     "\\lambda = %g  and offset = %g\n\\chi^2 = %g n = %d"
	     ,del,gam,lam,off,chi2,(2*dse->nx)-3);
  return 0;
}



int fit_delta(d_s *dse, // contains ln(tau_e)
	      d_s *dsb, // contains ln(tau_r)
	      d_s *dsr, // contains ln(tau_b)
	      d_s *xbi, // contains the enzyme specific shift
	      float *deltal, // the distance in x between tau_e and tau_b
	      float off,  // the offset in y
	      float gam, // the distance in x between tau_r and tau_b = gam * delta
	      float lam,
	      double *chi2r,
	      float range,
	      int nt)

{
  static d_s *dschi2 = NULL;
  int i,  imax;
  double chi2 = 0, chi2min = 0;
  float dl, pos = 0, val = 0;

  if (dse == NULL || dsb == NULL || dsr == NULL || xbi == NULL) return 1;
  if (dse->ye == NULL || dsb->ye == NULL || dsr->ye == NULL) return 1;
  if (dse->nx != dsb->nx || dse->nx != xbi->nx || dse->nx != dsr->nx) return 2;
  if (deltal == NULL) return 3;

  if (dschi2 == NULL || dschi2->nx < nt)
    dschi2 = build_adjust_data_set(dschi2, nt, nt);
  if (dschi2 == NULL) return 4;
  for (i = 0; i < nt; i++)
      dschi2->xd[i] =dschi2->yd[i] = 0;
  for (i = imax = 0; i < nt; i++)
    {
      dl = *deltal + range * *deltal * (2*i - nt)/nt;
      if (Bi_Ai_compute_chi2(dse, dsb, dsr, xbi, dl, off, gam, lam, &chi2))
	{
	  return win_printf_OK("Pb while computing \\chi^2 !");
	}
      dschi2->xd[i] = i;
      dschi2->yd[i] = -chi2;      
      if (i == 0 || chi2 < chi2min)
	{
	  chi2min = chi2;
	  imax = i;
	}
    }
  if (find_max_around(dschi2->yd, nt, imax, &pos, &val, NULL))
    {
      //win_printf_OK("Pb finding minimum delta = %g!",pos);
      pos = (*deltal) + range * (*deltal) * (2*imax - nt)/nt;
    }
  else pos = (*deltal) + range * (*deltal) * (2*pos - nt)/nt;
  *deltal = pos;
  if (Bi_Ai_compute_chi2(dse, dsb, dsr, xbi, pos, off, gam, lam, chi2r))
    return win_printf_OK("Pb while computing \\chi^2 !");
  return 0;
}  
int fit_lambda(d_s *dse, // contains ln(tau_e)
	       d_s *dsb, // contains ln(tau_r)
	       d_s *dsr, // contains ln(tau_b)
	       d_s *xbi, // contains the enzyme specific shift
	       float deltal, // the distance in x between tau_e and tau_b
	       float off,  // the offset in y
	       float gam, // the distance in x between tau_r and tau_b = gam * delta
	       float *lam,
	       double *chi2r,
	       float range,
	       int nt)

{
  static d_s *dschi2 = NULL;
  int i,  imax;
  double chi2 = 0, chi2min = 0;
  float dl, pos = 0, val = 0;

  if (dse == NULL || dsb == NULL || dsr == NULL || xbi == NULL) return 1;
  if (dse->ye == NULL || dsb->ye == NULL || dsr->ye == NULL) return 1;
  if (dse->nx != dsb->nx || dse->nx != xbi->nx || dse->nx != dsr->nx) return 2;
  if (lam == NULL) return 3;

  if (dschi2 == NULL || dschi2->nx < nt)
    dschi2 = build_adjust_data_set(dschi2, nt, nt);
  if (dschi2 == NULL) return 4;
  for (i = 0; i < nt; i++)
      dschi2->xd[i] =dschi2->yd[i] = 0;
  for (i = imax = 0; i < nt; i++)
    {
      dl = (*lam) + range * (*lam) * (2*i - nt)/nt;
      if (Bi_Ai_compute_chi2(dse, dsb, dsr, xbi, deltal, off, gam, dl, &chi2))
	{
	  return win_printf_OK("Pb while computing \\chi^2 !");
	}
      dschi2->xd[i] = i;
      dschi2->yd[i] = -chi2;      
      if (i == 0 || chi2 < chi2min)
	{
	  chi2min = chi2;
	  imax = i;
	}
    }
  if (find_max_around(dschi2->yd, nt, imax, &pos, &val, NULL))
    {
      //win_printf_OK("Pb finding minimum delta = %g!",pos);
      pos = (*lam) + range * (*lam) * (2*imax - nt)/nt;
    }
  else pos = (*lam) + range * (*lam) * (2*pos - nt)/nt;
  *lam = pos;
  if (Bi_Ai_compute_chi2(dse, dsb, dsr, xbi, deltal, off, gam, pos, chi2r))
    return win_printf_OK("Pb while computing \\chi^2 !");
  return 0;
}  

int fit_offset(d_s *dse, // contains ln(tau_e)
	       d_s *dsb, // contains ln(tau_r)
	       d_s *dsr, // contains ln(tau_b)
	       d_s *xbi, // contains the enzyme specific shift
	       float deltal, // the distance in x between tau_e and tau_b
	       float *off,  // the offset in y
	       float gam, // the distance in x between tau_r and tau_b = gam * delta
	       float lam,
	       double *chi2r,
	       float range,
	       int nt)

{
  static d_s *dschi2 = NULL;
  int i,  imax;
  double chi2 = 0, chi2min = 0;
  float dl, pos = 0, val = 0;

  if (dse == NULL || dsb == NULL || dsr == NULL || xbi == NULL) return 1;
  if (dse->ye == NULL || dsb->ye == NULL || dsr->ye == NULL) return 1;
  if (dse->nx != dsb->nx || dse->nx != xbi->nx || dse->nx != dsr->nx) return 2;
  if (off == NULL) return 3;

  if (dschi2 == NULL || dschi2->nx < nt)
    dschi2 = build_adjust_data_set(dschi2, nt, nt);
  if (dschi2 == NULL) return 4;
  for (i = 0; i < nt; i++)
      dschi2->xd[i] =dschi2->yd[i] = 0;
  for (i = imax = 0; i < nt; i++)
    {
      dl = *off + range * *off * (2*i - nt)/nt;
      if (Bi_Ai_compute_chi2(dse, dsb, dsr, xbi, deltal, dl, gam, lam, &chi2))
	{
	  return win_printf_OK("Pb while computing \\chi^2 !");
	}
      dschi2->xd[i] = i;
      dschi2->yd[i] = -chi2;      
      if (i == 0 || chi2 < chi2min)
	{
	  chi2min = chi2;
	  imax = i;
	}
    }
  if (find_max_around(dschi2->yd, nt, imax, &pos, &val, NULL))
    {
      //win_printf_OK("Pb finding minimum for %g !",offset);
      pos = (*off) + range * (*off) * (2*imax - nt)/nt;
    }
  else pos = (*off) + range * (*off) * (2*pos - nt)/nt;
  *off = pos;
  if (Bi_Ai_compute_chi2(dse, dsb, dsr, xbi, deltal, pos, gam, lam, chi2r))
    return win_printf_OK("Pb while computing \\chi^2 !");
  return 0;
}  


int fit_gamma(d_s *dse, // contains ln(tau_e)
	      d_s *dsb, // contains ln(tau_r)
	      d_s *dsr, // contains ln(tau_b)
	      d_s *xbi, // contains the enzyme specific shift
	      float deltal, // the distance in x between tau_e and tau_b
	      float off,  // the offset in y
	      float *gam, // the distance in x between tau_r and tau_b = gam * delta
	      float lam,
	      double *chi2r,
	      float range,
	      int nt)

{
  static d_s *dschi2 = NULL;
  int i,  imax;
  double chi2 = 0, chi2min = 0;
  float dl, pos = 0, val = 0;

  if (dse == NULL || dsb == NULL || dsr == NULL || xbi == NULL) return 1;
  if (dse->ye == NULL || dsb->ye == NULL || dsr->ye == NULL) return 1;
  if (dse->nx != dsb->nx || dse->nx != xbi->nx || dse->nx != dsr->nx) return 2;
  if (gam == NULL) return 3;

  if (dschi2 == NULL || dschi2->nx < nt)
    dschi2 = build_adjust_data_set(dschi2, nt, nt);
  if (dschi2 == NULL) return 4;
  for (i = 0; i < nt; i++)
      dschi2->xd[i] =dschi2->yd[i] = 0;
  for (i = imax = 0; i < nt; i++)
    {
      dl = (*gam) + (range * (*gam) * (2*i - nt)/nt);
      if (Bi_Ai_compute_chi2(dse, dsb, dsr, xbi, deltal, off, dl, lam, &chi2))
	{
	  return win_printf_OK("Pb while computing \\chi^2 !");
	}
      dschi2->xd[i] = i;
      dschi2->yd[i] = -chi2;      
      if (i == 0 || chi2 < chi2min)
	{
	  chi2min = chi2;
	  imax = i;
	}
    }
  if (find_max_around(dschi2->yd, nt, imax, &pos, &val, NULL))
    {
      //win_printf_OK("Pb finding minimum for \\gamma  = %g!",pos);
      pos = (*gam) + range * (*gam) * (2*imax - nt)/nt;
    }
  else pos = (*gam) + range * (*gam) * (2*pos - nt)/nt;
  *gam = pos;
  if (Bi_Ai_compute_chi2(dse, dsb, dsr, xbi, deltal, off, pos, lam, chi2r))
    return win_printf_OK("Pb while computing \\chi^2 !");
  return 0;
}  


int fit_xbi(d_s *dse, // contains ln(tau_e)
	    d_s *dsb, // contains ln(tau_r)
	    d_s *dsr, // contains ln(tau_b)
	    d_s *xbi, // contains the enzyme specific shift
	    float deltal, // the distance in x between tau_e and tau_b
	    float off,  // the offset in y
	    float gam, // the distance in x between tau_r and tau_b = gam * delta
	    float lam,
	    int ixbi,
	    float *yxbi,
	    double *chi2r,
	    float range,
	    int nt)
  
{
  static d_s *dschi2 = NULL, *xbit = NULL;
  int i,  imax;
  double chi2 = 0, chi2min = 0;
  float  pos = 0, val = 0;

  if (dse == NULL || dsb == NULL || dsr == NULL || xbi == NULL) return 1;
  if (dse->ye == NULL || dsb->ye == NULL || dsr->ye == NULL) return 1;
  if (dse->nx != dsb->nx || dse->nx != xbi->nx || dse->nx != dsr->nx) return 2;
  if (yxbi == NULL) return 3;

  if (dschi2 == NULL || dschi2->nx < nt)
    dschi2 = build_adjust_data_set(dschi2, nt, nt);
  if (dschi2 == NULL) return 4;

  if (xbit == NULL || xbit->nx < xbi->nx)
    xbit = build_adjust_data_set(xbit, xbi->nx, xbi->nx);
  if (xbit == NULL) return 4;  
  
  for (i = 0; i < nt; i++)
      dschi2->xd[i] =dschi2->yd[i] = 0;

  for (i = 0; i < xbi->nx; i++)
    {
      xbit->xd[i] = xbi->xd[i];
      xbit->yd[i] = xbi->yd[i];      
    }

  for (i = imax = 0; i < nt; i++)
    {
      xbit->yd[ixbi] = (xbi->yd[ixbi]) + (range * (xbi->yd[ixbi]) * (2*i - nt)/nt);
      if (Bi_Ai_compute_chi2(dse, dsb, dsr, xbit, deltal, off, gam, lam, &chi2))
	{
	  return win_printf_OK("Pb while computing \\chi^2 !");
	}
      dschi2->xd[i] = i;
      dschi2->yd[i] = -chi2;      
      if (i == 0 || chi2 < chi2min)
	{
	  chi2min = chi2;
	  imax = i;
	}
    }
  if (find_max_around(dschi2->yd, nt, imax, &pos, &val, NULL))
    {
      //win_printf_OK("Pb finding minimum at xbi = %d!",ixbi);
      pos = (xbi->yd[ixbi]) + range * (xbi->yd[ixbi]) * (2*imax - nt)/nt;
    }
  else pos = (xbi->yd[ixbi]) + range * (xbi->yd[ixbi]) * (2*pos - nt)/nt;
  xbit->yd[ixbi] = pos;
  if (Bi_Ai_compute_chi2(dse, dsb, dsr, xbit, deltal, off, gam, lam, chi2r))
    return win_printf_OK("Pb while computing \\chi^2 !");
  *yxbi = pos;
  return 0;
}  


int do_Fit_Bi_Ai_compute_chi2_to_fit_delta(void)
{
  int i, imax;
  O_p *op = NULL;
  d_s *dse, *dsb, *dsr, *dsxbi = NULL, *dschi2 = NULL;
  pltreg *pr = NULL;
  static int idsei = 0, idsbi = 1, idsri = 2, idsxbi = 3, nt = 512;  
  double chi2 = 0, chi2min = 0;
  float dl, bestdelta, pos = 0, val = 0;
  static float range = 0.3;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number and place it in a new plot");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
    return win_printf_OK("cannot find data");
  if (op->n_dat < 3)     return win_printf_OK("not enough datasets !");
  i = win_scanf("For: ln(\\tau_{Ei}) - ln(\\tau_{bi}) = \\Delta^2 + 2\\Delta.x_{bi}\n"
		"ln(\\tau_{Ei}) + ln(\\tau_{bi}) = \\Delta^2 + 2\\Delta.x_{bi} + 2.x_{bi}^2\n"
		"%3d Index of ds containing ln(\\tau_{Ei})\n"
		"%3d Index of ds containing ln(\\tau_{bi})\n"
		"%3d Index of ds containing ln(\\tau_{ri})\n"				
		"%3d Index of ds containing x_{bi}\n"
		"Compute best \\chi^2 give \\Delta   %5f\n"
		"Give variation range %5f Nb. of trial %5d\n"
		"and gamma %5f\n"
		"offset %5f lambda %5f\n"		
		,&idsei,&idsbi,&idsri,&idsxbi,&delta,&range,&nt,&gammav,&offset,&lambda);
  if (i == WIN_CANCEL)	return OFF;
  dse = op->dat[idsei];
  dsb = op->dat[idsbi];
  dsr = op->dat[idsri];    
  dsxbi = op->dat[idsxbi];


  double chi2r = 0;
  float deltal = delta;
  fit_delta(dse, dsb, dsr, dsxbi, &deltal, offset, gammav, lambda, &chi2r, range, nt);
  win_printf("fit delta %g (instead of %g) chi2 %g",deltal,delta,chi2r);

  
  dschi2 = build_data_set(nt,nt);
  for (i = imax = 0, bestdelta = delta; i < nt; i++)
    {
      dl = delta + range * delta * (2*i - nt)/nt;
      if (Bi_Ai_compute_chi2(dse, dsb, dsr, dsxbi, dl, offset, gammav, lambda, &chi2))
	{
	  return win_printf_OK("Pb while computing \\chi^2 !");
	}
      dschi2->xd[i] = i;
      dschi2->yd[i] = -chi2;      
      if (i == 0 || chi2 < chi2min)
	{
	  chi2min = chi2;
	  bestdelta = dl;
	  imax = i;
	}
    }

  if (find_max_around(dschi2->yd, nt, imax, &pos, &val, NULL))
    win_printf_OK("Pb finding minimum in delta!");
  pos = delta + range * delta * (2*pos - nt)/nt;
  i = win_printf("For \\Delta in %s[%g,%g[%s and \\gamma  = %g offset = %g lambda = %g\n"
		 "best \\chi^2 = %g (%g) at delta = %g (instead of %g and %g) n = %d\n"
		 "Pressing OK will use this value"
		 ,(i==0)?"b":"",delta*(1-range),delta*(1+range),(i==nt-1)?"b":""
		 ,gammav,offset,lambda,-val,chi2min,pos,bestdelta,delta,dse->nx);
  if (i != WIN_CANCEL) delta = pos;
  update_fits(op, dsxbi);
  refresh_plot(pr, UNCHANGED);
  free_data_set(dschi2);
  return 0;
}


int do_Fit_Bi_Ai_compute_chi2_to_fit_offset(void)
{
  int i;
  O_p *op = NULL;
  d_s *dse, *dsb, *dsr, *dsxbi;
  pltreg *pr = NULL;
  static int idsei = 0, idsbi = 1, idsri = 2, idsxbi = 3, nt = 512;    
  double chi2 = 0, chi2min = 0;
  float dl, bestdelta;
  static float range = 0.3;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number and place it in a new plot");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
    return win_printf_OK("cannot find data");
  if (op->n_dat < 3)     return win_printf_OK("not enough datasets !");
  i = win_scanf("For: ln(\\tau_{Ei}) - ln(\\tau_{bi}) = \\Delta^2 + 2\\Delta.x_{bi}\n"
		"ln(\\tau_{Ei}) + ln(\\tau_{bi}) = \\Delta^2 + 2\\Delta.x_{bi} + 2.x_{bi}^2\n"
		"%3d Index of ds containing ln(\\tau_{Ei})\n"
		"%3d Index of ds containing ln(\\tau_{bi})\n"
		"%3d Index of ds containing ln(\\tau_{ri})\n"				
		"%3d Index of ds containing x_{bi}\n"
		"Compute best \\chi^2 give \\Delta   %5f\n"
		"and \\gamma %5f and lambda %5f\n"
		"Fit offset %5f\n"
		"Give variation range %5f Nb. of trial %5d\n"		
		,&idsei,&idsbi,&idsri,&idsxbi,&delta,&gammav,&lambda,&offset,&range,&nt);
  if (i == WIN_CANCEL)	return OFF;
  dse = op->dat[idsei];
  dsb = op->dat[idsbi];
  dsr = op->dat[idsri];    
  dsxbi = op->dat[idsxbi];

    
  for (i = 0, bestdelta = offset; i < nt; i++)
    {
      dl = offset + range * offset * (2*i - nt)/nt;
      if (Bi_Ai_compute_chi2(dse, dsb, dsr, dsxbi, delta, dl, gammav, lambda,&chi2))      
	{
	  return win_printf_OK("Pb while computing \\chi^2 !");
	}
      if (i == 0 || chi2 < chi2min)
	{
	  chi2min = chi2;
	  bestdelta = dl;
	}
    }
  i = win_printf("For offset in [%g,%g[ and \\gamma = %g \\Delta = %g lambda = %g\n"
		 "best \\chi^2 = %g at offset = %g (instead of %g) n = %d\n"
		 "Pressing OK will use this value"
		 ,offset*(1-range),offset*(1+range),gammav,delta,lambda,chi2min
		 ,bestdelta,offset,dse->nx);
  if (i != WIN_CANCEL) offset = bestdelta;
  update_fits(op, dsxbi);
  refresh_plot(pr, UNCHANGED);  
  return 0;
}

int do_Fit_Bi_Ai_compute_chi2_to_fit_gamma(void)
{
  int i;
  O_p *op = NULL;
  d_s *dse, *dsb, *dsr, *dsxbi;
  pltreg *pr = NULL;
  static int idsei = 0, idsbi = 1, idsri = 2, idsxbi = 3, nt = 512;      
  double chi2 = 0, chi2min = 0;
  float dl, bestdelta;
  static float range = 0.3;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number and place it in a new plot");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
    return win_printf_OK("cannot find data");
  if (op->n_dat < 3)     return win_printf_OK("not enough datasets !");
  i = win_scanf("For: ln(\\tau_{Ei}) - ln(\\tau_{bi}) = \\Delta^2 + 2\\Delta.x_{bi}\n"
		"ln(\\tau_{Ei}) + ln(\\tau_{bi}) = \\Delta^2 + 2\\Delta.x_{bi} + 2.x_{bi}^2\n"
		"%3d Index of ds containing ln(\\tau_{Ei})\n"
		"%3d Index of ds containing ln(\\tau_{bi})\n"
		"%3d Index of ds containing ln(\\tau_{ri})\n"				
		"%3d Index of ds containing x_{bi}\n"
		"Compute best \\chi^2 give \\Delta   %5f\n"
		"and fit \\gamma %5f\n"
		"offset %5f lambda %5f\n"
		"Give variation range %5f Nb. of trial %5d\n"
		,&idsei,&idsbi,&idsri,&idsxbi,&delta,&gammav,&offset,&lambda,&range,&nt);
  if (i == WIN_CANCEL)	return OFF;
  dse = op->dat[idsei];
  dsb = op->dat[idsbi];
  dsr = op->dat[idsri];    
  dsxbi = op->dat[idsxbi];
  for (i = 0, bestdelta = gammav; i < nt; i++)
    {
      dl = gammav + range * gammav * (2*i - nt)/nt;
      if (Bi_Ai_compute_chi2(dse, dsb, dsr, dsxbi, delta, offset, dl, lambda, &chi2))            
	{
	  return win_printf_OK("Pb while computing \\chi^2 !");
	}
      if (i == 0 || chi2 < chi2min)
	{
	  chi2min = chi2;
	  bestdelta = dl;
	}
    }
  i = win_printf("For \\gamma in [%g,%g[ and \\Delta = %g lambda = %g\n"
		 "best \\chi^2 = %g at \\gamma = %g (instead of %g) n = %d\n"
		 "Pressing OK will use this value"
		 ,gammav*(1-range),gammav*(1+range),delta,lambda,chi2min,bestdelta
		 ,gammav,dse->nx);
  if (i != WIN_CANCEL) gammav = bestdelta;
  update_fits(op, dsxbi);
  refresh_plot(pr, UNCHANGED);    
  return 0;
}


int do_Fit_Bi_Ai_compute_chi2_to_fit_xbi(void)
{
  int i;
  O_p *op = NULL;
  d_s *dse, *dsb, *dsr, *dsxbi;
  pltreg *pr = NULL;
  static int idsei = 0, idsbi = 1, idsri = 2, idsxbi = 3, nt = 512, ixbi = 0;        
  double chi2 = 0, chi2min = 0;
  float bestdelta;
  static float range = 0.3;
  d_s *dstxbi = NULL;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number and place it in a new plot");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
    return win_printf_OK("cannot find data");

 
  if (op->n_dat < 3)     return win_printf_OK("not enough datasets !");
  i = win_scanf("For: ln(\\tau_{Ei}) - ln(\\tau_{bi}) = \\Delta^2 + 2\\Delta.x_{bi}\n"
		"ln(\\tau_{Ei}) + ln(\\tau_{bi}) = \\Delta^2 + 2\\Delta.x_{bi} + 2.x_{bi}^2\n"
		"%3d Index of ds containing ln(\\tau_{Ei})\n"
		"%3d Index of ds containing ln(\\tau_{bi})\n"
		"%3d Index of ds containing ln(\\tau_{ri})\n"				
		"%3d Index of ds containing x_{bi}\n"
		"Compute best \\chi^2 give \\Delta   %5f\n"
		"and \\gamma %5f\n"
		"offset %5f lambda %5f\n"
		"Fit x_{bi} with i = %3d\n"
		"Give variation range %5f Nb. of trials %5d\n"
		,&idsei,&idsbi,&idsri,&idsxbi,&delta,&gammav,&offset,&lambda
		,&ixbi,&range,&nt);
  if (i == WIN_CANCEL)	return OFF;
  dse = op->dat[idsei];
  dsb = op->dat[idsbi];
  dsr = op->dat[idsri];    
  dsxbi = op->dat[idsxbi];
  
  dstxbi = duplicate_data_set(dsxbi, NULL);
  for (i = 0, bestdelta = dsxbi->yd[ixbi]; i < nt; i++)
    {
      dstxbi->yd[ixbi] = dsxbi->yd[ixbi] * (1 + range * (2*i - nt)/nt);
      if (Bi_Ai_compute_chi2(dse, dsb, dsr, dstxbi, delta, offset, gammav, lambda, &chi2))
	{
	  return win_printf_OK("Pb while computing \\chi^2 !");
	}
      if (i == 0 || chi2 < chi2min)
	{
	  chi2min = chi2;
	  bestdelta = dstxbi->yd[ixbi];
	}
    }
  i = win_printf("For x_{b%d} in range [%g,%g[ and \\Delta = %g and \\gamma = %g lambda = %g\n"
		 "best \\chi^2 = %g for value = %g (instead of %g) n = %d\n"
		 "Pressing OK will use this value"
		 ,ixbi,dsxbi->yd[ixbi]*(1-range),dsxbi->yd[ixbi]*(1+range),delta
		 ,gammav,lambda,chi2min,bestdelta,dsxbi->yd[ixbi],dse->nx);
  if (i != WIN_CANCEL) dsxbi->yd[ixbi] = bestdelta;
  update_fits(op, dsxbi);
  refresh_plot(pr, UNCHANGED);      
  free_data_set(dstxbi);
  return 0;
}



int do_Fit_Bi_Ai_compute_chi2_to_fit_mag_xbi(void)
{
  int i, j;
  O_p *op = NULL;
  d_s *dse, *dsb, *dsr, *dsxbi;
  pltreg *pr = NULL;
  static int idsei = 0, idsbi = 1, idsri = 2, idsxbi = 3, nt = 512;        
  double chi2 = 0, chi2min = 0;
  float bestdelta;
  static float range = 0.3;
  d_s *dstxbi = NULL;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number and place it in a new plot");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
    return win_printf_OK("cannot find data");

 
  if (op->n_dat < 3)     return win_printf_OK("not enough datasets !");
  i = win_scanf("For: ln(\\tau_{Ei}) - ln(\\tau_{bi}) = \\Delta^2 + 2\\Delta.x_{bi}\n"
		"ln(\\tau_{Ei}) + ln(\\tau_{bi}) = \\Delta^2 + 2\\Delta.x_{bi} + 2.x_{bi}^2\n"
		"%3d Index of ds containing ln(\\tau_{Ei})\n"
		"%3d Index of ds containing ln(\\tau_{bi})\n"
		"%3d Index of ds containing ln(\\tau_{ri})\n"				
		"%3d Index of ds containing x_{bi}\n"
		"Compute best \\chi^2 give \\Delta   %5f\n"
		"and \\gamma %5f\n"
		"offset %5f lambda %5f\n"
		"Fit factor multiplying x_{bi} \n"
		"Give variation range %5f Nb. of trials %5d\n"
		,&idsei,&idsbi,&idsri,&idsxbi,&delta,&gammav,&offset,&lambda,&range,&nt);
  if (i == WIN_CANCEL)	return OFF;
  dse = op->dat[idsei];
  dsb = op->dat[idsbi];
  dsr = op->dat[idsri];    
  dsxbi = op->dat[idsxbi];
  
  dstxbi = duplicate_data_set(dsxbi, NULL);
  for (i = 0, bestdelta = 1; i < nt; i++)
    {
      for (j = 0; j < dsxbi->nx; j++)
	dstxbi->yd[j] = dsxbi->yd[j] * (1 + range * (2*i - nt)/nt);
      if (Bi_Ai_compute_chi2(dse, dsb, dsr, dstxbi, delta, offset, gammav, lambda, &chi2)) 
	{
	  return win_printf_OK("Pb while computing \\chi^2 !");
	}
      if (i == 0 || chi2 < chi2min)
	{
	  chi2min = chi2;
	  bestdelta = (1 + range * (2*i - nt)/nt);
	}
    }
  i = win_printf("For factor in range [%g,%g[ and \\Delta = %g and \\gamma = %g \\lambda  = %g\n"
		 "best \\chi^2 = %g for value = %g (instead of 1) n = %d\n"
		 "Pressing OK will use this value"
		 ,(1-range),(1+range),delta
		 ,gammav,lambda,chi2min,bestdelta,dse->nx);
  if (i != WIN_CANCEL)
    {
      for (j = 0; j < dsxbi->nx; j++)
	dsxbi->yd[j] *= bestdelta;
    }  
  update_fits(op, dsxbi);
  refresh_plot(pr, UNCHANGED);      
  free_data_set(dstxbi);
  return 0;
}

int do_Fit_Bi_Ai_compute_chi2_to_fit_all_xbi(void)
{
  int i;
  O_p *op = NULL;
  d_s *dse, *dsb, *dsr, *dsxbi;
  pltreg *pr = NULL;
  static int idsei = 0, idsbi = 1, idsri = 2, idsxbi = 3, nt = 512, ixbi = 0;          
  double chi2 = 0, chi2min = 0;
  float bestdelta;
  static float range = 0.3;
  d_s *dstxbi = NULL;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number and place it in a new plot");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
    return win_printf_OK("cannot find data");
  if (op->n_dat < 3)     return win_printf_OK("not enough datasets !");
  i = win_scanf("For: ln(\\tau_{Ei}) - ln(\\tau_{bi}) = \\Delta^2 + 2\\Delta.x_{bi}\n"
		"ln(\\tau_{Ei}) + ln(\\tau_{bi}) = \\Delta^2 + 2\\Delta.x_{bi} + 2.x_{bi}^2\n"
		"%3d Index of ds containing ln(\\tau_{Ei})\n"
		"%3d Index of ds containing ln(\\tau_{bi})\n"
		"%3d Index of ds containing ln(\\tau_{ri})\n"				
		"%3d Index of ds containing x_{bi}\n"
		"Compute best \\chi^2 give \\Delta   %5f\n"
		"and \\gamma = %5f\n"
		"offset %5f lambda %5f\n"
		"Fit all x_{bi}\n"
		"Give variation range %5f Nb. of trials %5d\n"
		,&idsei,&idsbi,&idsri,&idsxbi,&delta,&gammav,&offset,&lambda, &range,&nt);
  if (i == WIN_CANCEL)	return OFF;
  dse = op->dat[idsei];
  dsb = op->dat[idsbi];
  dsr = op->dat[idsri];    
  dsxbi = op->dat[idsxbi];
  dstxbi = duplicate_data_set(dsxbi, NULL);
  for (ixbi = 0; ixbi < dse->nx; ixbi++)
    {
      for (i = 0; i < dse->nx; i++)
	dstxbi->yd[i] = dsxbi->yd[i];
      for (i = 0, bestdelta = dsxbi->yd[ixbi]; i < nt; i++)
	{
	  dstxbi->yd[ixbi] = dsxbi->yd[ixbi] * (1 + range * (2*i - nt)/nt);
	  if (Bi_Ai_compute_chi2(dse, dsb, dsr, dstxbi, delta, offset, gammav, lambda, &chi2))      	  
	    {
	      return win_printf_OK("Pb while computing \\chi^2 !");
	    }
	  if (i == 0 || chi2 < chi2min)
	    {
	      chi2min = chi2;
	      bestdelta = dstxbi->yd[ixbi];
	    }
	}
      dsxbi->yd[ixbi] = bestdelta;
    }
  i = win_printf("For all x_{bi} in rel range %g and \\Delta = %g and gamma = %g \\lambda = %g\n"
		 "best \\chi^2 = %g  n = %d\n"
		 "Pressing OK will use this value"
		 ,range,delta,gammav,lambda,chi2min,dse->nx);
  update_fits(op, dsxbi);
  refresh_plot(pr, UNCHANGED);      
  free_data_set(dstxbi);
  return 0;
}



int do_Fit_Bi_Ai_compute_chi2_to_fit_all(void)
{
  int i, iter;
  O_p *op = NULL;
  d_s *dse, *dsb, *dsr, *dsxbi;
  pltreg *pr = NULL;
  static int idsei = 0, idsbi = 1, idsri = 2, idsxbi = 3, nt = 512, ixbi = 0, niter = 10;            
  static float range = 0.3, fac = 1.01;
  d_s *dstxbi = NULL;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number and place it in a new plot");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
    return win_printf_OK("cannot find data");
  if (op->n_dat < 3)     return win_printf_OK("not enough datasets !");
  i = win_scanf("For: ln(\\tau_{Ei}) - ln(\\tau_{bi}) = \\Delta^2 + 2\\Delta.x_{bi}\n"
		"ln(\\tau_{Ei}) + ln(\\tau_{bi}) = \\Delta^2 + 2\\Delta.x_{bi} + 2.x_{bi}^2\n"
		"%3d Index of ds containing ln(\\tau_{Ei})\n"
		"%3d Index of ds containing ln(\\tau_{bi})\n"
		"%3d Index of ds containing ln(\\tau_{ri})\n"				
		"%3d Index of ds containing x_{bi}\n"
		"Compute best \\chi^2 give \\Delta   %5f\n"
		"and gamma %5f and lambda %5f\n"
		"offset %5f\n"
		"Fit \\Delta  \\gamma  offset and all x_{bi}\n"
		"Give variation range %5f Nb. of trials %5d\n"
		"repeat on %4d iter\n"
		"Alter magnitude by %5f\n"
		,&idsei,&idsbi,&idsri,&idsxbi,&delta,&gammav,&lambda,&offset,&range
		,&nt,&niter,&fac);
  if (i == WIN_CANCEL)	return OFF;
  dse = op->dat[idsei];
  dsb = op->dat[idsbi];
  dsr = op->dat[idsri];    
  dsxbi = op->dat[idsxbi];
  dstxbi = duplicate_data_set(dsxbi, NULL);


  double chi2r = 0, bchi2r = 0,bchi2rt = 0;
  float deltal = delta, gam = gammav, off = offset, yxbi = 0, lam = lambda;
  int ret;
  Bi_Ai_compute_chi2(dse, dsb, dsr, dsxbi, delta, offset, gammav, lambda, &chi2r);
  for (ixbi = 0; ixbi < dsxbi->nx; ixbi++)
    dstxbi->yd[ixbi] = fac * dstxbi->yd[ixbi]; 
  for (iter = 0; iter < niter; iter++)
    {
      ret = fit_delta(dse, dsb, dsr, dstxbi, &deltal, off, gam, lam, &bchi2r, range, nt);
      if (fit_r)
	{
	  ret = fit_gamma(dse, dsb, dsr, dstxbi, deltal, off, &gam, lam, &bchi2r, range, nt);
	}
      //ret = fit_offset(dse, dsb, dsr, dstxbi, deltal, &off, gam, lam, &bchi2r, range, nt);
      ret = fit_lambda(dse, dsb, dsr, dstxbi, deltal, off, gam, &lam, &bchi2r, range, nt);
      for (ixbi = 0; ixbi < dse->nx; ixbi++)
	{
	  ret = fit_xbi(dse, dsb, dsr, dstxbi, deltal, off, gam, lam, ixbi, &yxbi, &bchi2rt, range, nt);
	  if (ret == 0 && bchi2rt < bchi2r)
	    {
	      dstxbi->yd[ixbi] = yxbi;
	      bchi2r = bchi2rt;
	    }
	}
    }

  
  /*      
      Bi_Ai_compute_chi2(dse, dsb, dsr, dsxbi, delta, offset, gammav, &chi20);      
      for (i = 0, bestdelta = delta; i < nt; i++)
	{
	  dl = delta + range * delta * (2*i - nt)/nt;
	  if (Bi_Ai_compute_chi2(dse, dsb, dsr, dsxbi, dl, offset, gammav, &chi2))
	    {
	      return win_printf_OK("Pb while computing \\chi^2 !");
	    }
	  if (i == 0 || chi2 < chi2min)
	    {
	      chi2min = chi2;
	      bestdelta = dl;
	    }
	}
      delta = (chi2min < chi20) ? bestdelta : delta;

      if (fit_r)
	{
	  Bi_Ai_compute_chi2(dse, dsb, dsr, dsxbi, delta, offset, gammav, &chi20);      
	  for (i = 0, bestdelta = gammav; i < nt; i++)
	    {
	      dl = gammav + range * gammav * (2*i - nt)/nt;
	      if (Bi_Ai_compute_chi2(dse, dsb, dsr, dsxbi, delta, offset, dl, &chi2))      	  	        
		{
		  return win_printf_OK("Pb while computing \\chi^2 !");
		}
	      if (i == 0 || chi2 < chi2min)
		{
		  chi2min = chi2;
		  bestdelta = dl;
		}
	    }
	  gammav = (chi2min < chi20) ? bestdelta : gammav;
	}
  
      Bi_Ai_compute_chi2(dse, dsb, dsr, dsxbi, delta, offset, gammav, &chi20);      
      for (i = 0, bestdelta = offset; i < nt; i++)
	{
	  dl = offset + range * offset * (2*i - nt)/nt;
	  if (Bi_Ai_compute_chi2(dse, dsb, dsr, dsxbi, delta, dl, gammav, &chi2))      	  	              
	    {
	      return win_printf_OK("Pb while computing \\chi^2 !");
	    }
	  if (i == 0 || chi2 < chi2min)
	    {
	      chi2min = chi2;
	      bestdelta = dl;
	    }
	}
      offset = (chi2min < chi20) ? bestdelta : offset;

  
  
      for (ixbi = 0; ixbi < dse->nx; ixbi++)
	{
	  for (i = 0; i < dse->nx; i++)
	    dstxbi->yd[i] = dsxbi->yd[i];
	  Bi_Ai_compute_chi2(dse, dsb, dsr, dstxbi, delta, offset, gammav, &chi20);      
	  for (i = 0, bestdelta = dsxbi->yd[ixbi]; i < nt; i++)
	    {
	      dstxbi->yd[ixbi] = dsxbi->yd[ixbi] * (1 + range * (2*i - nt)/nt);
	      if (Bi_Ai_compute_chi2(dse, dsb, dsr, dstxbi, delta, offset, gammav, &chi2))      	  	  
		{
		  return win_printf_OK("Pb while computing \\chi^2 !");
		}
	      if (i == 0 || chi2 < chi2min)
		{
		  chi2min = chi2;
		  bestdelta = dstxbi->yd[ixbi];
		}
	    }
	  dsxbi->yd[ixbi] = (chi2min < chi20) ? bestdelta : dsxbi->yd[ixbi];
	}
    }
  */
  int modify = 0;
  if (bchi2r < chi2r)
    {
      for (ixbi = 0; ixbi < dsxbi->nx; ixbi++)
	dsxbi->yd[ixbi] = dstxbi->yd[ixbi]; 
      delta = deltal;
      gammav = gam;
      offset = off;
      lambda = lam;
      modify = 1;
    }
  i = win_printf("For all x_{bi} in rel range %g %s\n"
		 "\\Delta = %g and \\gamma = %g \\lambda = %g and offset = %g\n"
		 "best \\chi^2 = %g  n = %d\n"
		 "Pressing OK will use this value"
		 ,range,(modify)?"Parameters were adjusted":"The adjustment was rejected"
		 ,delta,gammav,lambda,offset,bchi2r,dse->nx);
  update_fits(op, dsxbi);  
  refresh_plot(pr, UNCHANGED);
  free_data_set(dstxbi);
  return 0;
}



int do_Fit_Bi_Ai_compute_chi2_to_fit_draw_parabol(void)
{
  int i;
  O_p *op = NULL, *opn = NULL;
  d_s *dse, *dsb, *dsr, *dsxbi, *ds, *ds2 = NULL;
  pltreg *pr = NULL;
  static int idsei = 0, idsbi = 1, idsri = 2, idsxbi = 3;
  
  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number and place it in a new plot");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
    return win_printf_OK("cannot find data");
  if (op->n_dat < 3)     return win_printf_OK("not enough datasets !");
  i = win_scanf("For: ln(\\tau_{Ei}) - ln(\\tau_{bi}) = \\Delta^2 + 2\\Delta.x_{bi}\n"
		"ln(\\tau_{Ei}) + ln(\\tau_{bi}) = \\Delta^2 + 2\\Delta.x_{bi} + 2.x_{bi}^2\n"
		"%3d Index of ds containing ln(\\tau_{Ei})\n"
		"%3d Index of ds containing ln(\\tau_{bi})\n"
		"%3d Index of ds containing ln(\\tau_{ri})\n"				
		"%3d Index of ds containing x_{bi}\n"
		"Compute best \\chi^2 give \\Delta   %5f\n"
		"and gamma %5f and offset %5f\n"
		,&idsei,&idsbi,&idsri,&idsxbi,&delta,&gammav,&offset);
  if (i == WIN_CANCEL)	return OFF;
  dse = op->dat[idsei];
  dsb = op->dat[idsbi];
  dsr = op->dat[idsri];    
  dsxbi = op->dat[idsxbi];

  opn = create_and_attach_one_plot(pr, 3*dse->nx, 3*dse->nx, 0);
  if (opn == NULL) return win_printf_OK("cannot create data");
  ds = opn->dat[0];


  
  for (i = 0; i < dse->nx; i++)
    {
      ds2 = create_and_attach_one_ds(opn, 3, 3, 0);
      alloc_data_set_y_error(ds2);
      
      ds->xd[3*i] = ds2->xd[0] =   lambda;
      ds->yd[3*i] =  (lambda * lambda)/2;
      ds2->yd[0] = dsb->yd[i] - dsxbi->yd[i];
      ds2->ye[0] = dsb->ye[i];
      
      ds->xd[3*i+1] = ds2->xd[1] =  lambda + gammav;
      ds->yd[3*i+1] =  ((lambda + gammav) * (lambda + gammav))/2;
      ds2->yd[1] = dsr->yd[i] - dsxbi->yd[i];
      ds2->ye[1] = dsr->ye[i];      

      ds->xd[3*i+2] = ds2->xd[2] =  lambda + delta;
      ds->yd[3*i+2] =  ((lambda + delta) * (lambda + delta))/2;
      ds2->yd[2] = dse->yd[i] - dsxbi->yd[i];
      ds2->ye[2] = dse->ye[i];      


    }
  inherit_from_ds_to_ds(ds, dse);
  set_ds_treatement(ds,"delta = %g gamma  %f, offset %f lambda %g",delta,gammav,offset,lambda);
  set_plot_title(opn, "\\stack{{\\Delta = %g, \\gamma = %g (%sfitted)}{offset %g \\lambda = %g}}"
		 ,delta,gammav,(fit_r)?" ":"not ",offset,lambda);  
  if (op->x_title != NULL) set_plot_x_title(opn, "%s",op->x_title);
  if (op->y_title != NULL) set_plot_y_title(opn, "%s",op->y_title);
  opn->filename = Transfer_filename(op->filename);
  uns_op_2_op(opn, op);
  /* refisplay the entire plot */
  refresh_plot(pr, pr->n_op-1);
  
  return 0;
}


int do_Fit_Bi_Ai_compute_chi2_to_fit_draw_parabol2(void)
{
  int i, j;
  O_p *op = NULL, *opn = NULL;
  d_s *dse, *dsb, *dsr, *dsxbi, *dstxbi, *ds;
  pltreg *pr = NULL;
  float x;
  static int idsei = 0, idsbi = 1, idsri = 2, idsxbi = 3;
  
  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number and place it in a new plot");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
    return win_printf_OK("cannot find data");
  if (op->n_dat < 3)     return win_printf_OK("not enough datasets !");
  i = win_scanf("For: ln(\\tau_{Ei}) - ln(\\tau_{bi}) = \\Delta^2 + 2\\Delta.x_{bi}\n"
		"ln(\\tau_{Ei}) + ln(\\tau_{bi}) = \\Delta^2 + 2\\Delta.x_{bi} + 2.x_{bi}^2\n"
		"%3d Index of ds containing ln(\\tau_{Ei})\n"
		"%3d Index of ds containing ln(\\tau_{bi})\n"
		"%3d Index of ds containing ln(\\tau_{ri})\n"				
		"%3d Index of ds containing x_{bi}\n"
		"Compute best \\chi^2 give \\Delta  %5f \\lambda = %5f\n"
		"and gamma %5f and offset %5f\n"
		,&idsei,&idsbi,&idsri,&idsxbi,&delta,&lambda,&gammav,&offset);
  if (i == WIN_CANCEL)	return OFF;
  dse = op->dat[idsei];
  dsb = op->dat[idsbi];
  dsr = op->dat[idsri];    
  dsxbi = op->dat[idsxbi];

  dstxbi = duplicate_data_set(dsxbi, NULL);
  sort_ds_along_y(dstxbi);
  
  opn = create_and_attach_one_plot(pr, dse->nx, dse->nx, 0);
  if (opn == NULL) return win_printf_OK("cannot create data");
  ds = opn->dat[0];



  if (ds == NULL) ds = create_and_attach_one_ds(opn, dse->nx, dse->nx, 0);
  alloc_data_set_y_error(ds);
  inherit_from_ds_to_ds(ds, dsb);
  set_ds_treatement(ds,"delta = %g gamma  %f, offset %f lambda %g",delta,gammav,offset,lambda);  

  for (i = 0; i < dse->nx; i++)
    {
      j = (int)dstxbi->xd[i];
      ds->xd[i] = dstxbi->yd[i];
      ds->yd[i] = dsb->yd[j];
      ds->ye[i] = dsb->ye[j];
    }
  ds = create_and_attach_one_ds(opn, dse->nx, dse->nx, 0);
  inherit_from_ds_to_ds(ds, dsb);
  set_ds_treatement(ds,"delta = %g gamma  %f, offset %f lambda %g",delta,gammav,offset,lambda);  
  for (i = 0; i < dse->nx; i++)  
    {
      j = (int)dstxbi->xd[i];
      x = dstxbi->yd[i];
      ds->xd[i] = x;
      ds->yd[i] = x + ((lambda * lambda)/2);
    }
  ds = create_and_attach_one_ds(opn, dse->nx, dse->nx, 0);
  alloc_data_set_y_error(ds);
  inherit_from_ds_to_ds(ds, dsr);
  set_ds_treatement(ds,"delta = %g gamma  %f, offset %f lambda %g",delta,gammav,offset,lambda);
  for (i = 0; i < dse->nx; i++)
    {
      j = (int)dstxbi->xd[i];
      ds->xd[i] = dstxbi->yd[i];
      ds->yd[i] = dsr->yd[j];
      ds->ye[i] = dsr->ye[j];
    }
  ds = create_and_attach_one_ds(opn, dse->nx, dse->nx, 0);
  inherit_from_ds_to_ds(ds, dsr);
  set_ds_treatement(ds,"delta = %g gamma  %f, offset %f lambda %g",delta,gammav,offset,lambda);
  for (i = 0; i < dse->nx; i++)  
    {
      j = (int)dstxbi->xd[i];
      x = dstxbi->yd[i];
      ds->xd[i] = x;
      ds->yd[i] = x + (((lambda + gammav) * (lambda + gammav))/2);
  
    }
  ds = create_and_attach_one_ds(opn, dse->nx, dse->nx, 0);
  inherit_from_ds_to_ds(ds, dse);
  set_ds_treatement(ds,"delta = %g gamma  %f, offset %f lambda %g",delta,gammav,offset,lambda);
  alloc_data_set_y_error(ds);
  for (i = 0; i < dse->nx; i++)
    {
      j = (int)dstxbi->xd[i];
      ds->xd[i] = dstxbi->yd[i];
      ds->yd[i] = dse->yd[j];
      ds->ye[i] = dse->ye[j];
    }
  ds = create_and_attach_one_ds(opn, dse->nx, dse->nx, 0);
  inherit_from_ds_to_ds(ds, dse);
  set_ds_treatement(ds,"delta = %g gamma  %f, offset %f lambda %g",delta,gammav,offset,lambda);
  for (i = 0; i < dse->nx; i++)  
    {
      j = (int)dstxbi->xd[i];
      x = dstxbi->yd[i];
      ds->xd[i] = x;
      ds->yd[i] = x + (((lambda + delta) * (lambda + delta))/2);
    }

  set_plot_title(opn, "\\stack{{\\Delta = %g, \\gamma = %g (%sfitted)}{offset %g \\lambda = %g}}"
		 ,delta,gammav,(fit_r)?" ":"not ",offset,lambda);
  if (op->x_title != NULL) set_plot_x_title(opn, "%s",op->x_title);
  if (op->y_title != NULL) set_plot_y_title(opn, "%s",op->y_title);
  opn->filename = Transfer_filename(op->filename);
  uns_op_2_op(opn, op);
  /* refisplay the entire plot */
  refresh_plot(pr, pr->n_op-1);
  free_data_set(dstxbi);
  return 0;
}




MENU *Fit_Bi_Ai_plot_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"compute chi2",do_Fit_Bi_Ai_compute_chi2 ,NULL,0,NULL);
  add_item_to_menu(mn,"fit delta",do_Fit_Bi_Ai_compute_chi2_to_fit_delta ,NULL,0,NULL);
  add_item_to_menu(mn,"fit offset",do_Fit_Bi_Ai_compute_chi2_to_fit_offset ,NULL,0,NULL);  
  add_item_to_menu(mn,"fit gamma",do_Fit_Bi_Ai_compute_chi2_to_fit_gamma,NULL,0,NULL);
  add_item_to_menu(mn,"fit xbi",do_Fit_Bi_Ai_compute_chi2_to_fit_xbi,NULL,0,NULL);
  add_item_to_menu(mn,"fit magnitude of xbi",do_Fit_Bi_Ai_compute_chi2_to_fit_mag_xbi,NULL,0,NULL);  
  add_item_to_menu(mn,"fit all xbi",do_Fit_Bi_Ai_compute_chi2_to_fit_all_xbi,NULL,0,NULL);
  add_item_to_menu(mn,"niter fit all ",do_Fit_Bi_Ai_compute_chi2_to_fit_all,NULL,0,NULL);

  add_item_to_menu(mn,"draw parabolla",do_Fit_Bi_Ai_compute_chi2_to_fit_draw_parabol,NULL,0,NULL);
  add_item_to_menu(mn,"draw parabolla 2",do_Fit_Bi_Ai_compute_chi2_to_fit_draw_parabol2,NULL,0,NULL);            

  
  return mn;
}

int	Fit_Bi_Ai_main(int argc, char **argv)
{
  (void)argc;  (void)argv;  add_plot_treat_menu_item ( "Fit_Bi_Ai", NULL, Fit_Bi_Ai_plot_menu(), 0, NULL);
  return D_O_K;
}

int	Fit_Bi_Ai_unload(int argc, char **argv)
{
  (void)argc;  (void)argv;  remove_item_to_menu(plot_treat_menu, "Fit_Bi_Ai", NULL, NULL);
  return D_O_K;
}
#endif

