/*
*    Plug-in program for image treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _TESTIM_C_
#define _TESTIM_C_

# include "allegro.h"
# include "xvin.h"

/* If you include other plug-ins header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "testim.h"

O_i	*testim_image_new(int onx, int ony, int data_type, int period)
{
	register int i, j;
	O_i *oid;
	float tmpx, tmpy;
	double tmp;
	union pix  *pd;

  
	oid =  create_one_image(onx, ony, data_type);
	if (oid == NULL)
	{
		win_printf("Can't create dest image");
		return NULL;
	}
	period = (period <= 0)? 32 : period;
	if (data_type == IS_CHAR_IMAGE)
	{
		for (i = 0, pd = oid->im.pixel; i < ony ; i++)
		{
		        tmpy = cos((2*M_PI*i)/period);
			for (j=0; j< onx; j++)
			{
			        tmpx = cos((2*M_PI*j)/period);
				tmp = (tmpx*tmpy*127.5) + 127.5;
				pd[i].ch[j] = (tmp < 0) ? 0 :
					((tmp > 255) ? 255 : (unsigned char)tmp);
			}
		}
	}
	else if (data_type == IS_INT_IMAGE)
	{
		for (i = 0, pd = oid->im.pixel; i < ony ; i++)
		{
		        tmpy = cos((2*M_PI*i)/period);
			for (j=0; j< onx; j++)
			{
			        tmpx = cos((2*M_PI*j)/period);
				tmp = (tmpx*tmpy*32767.5) - 0.5;
				pd[i].in[j] = (tmp < -32768) ? -32768 :
					((tmp > 32767) ? 32767 : (short int)tmp);
			}
		}
	}
	else if (data_type == IS_UINT_IMAGE)
	{
		for (i = 0, pd = oid->im.pixel; i < ony ; i++)
		{
		        tmpy = cos((2*M_PI*i)/period);
			for (j=0; j< onx; j++)
			{
			        tmpx = cos((2*M_PI*j)/period);
				tmp = (tmpx*tmpy*32767.5) + 32767.5;
				pd[i].ui[j] = (tmp < 0) ? 0 :
					((tmp > 65535) ? 65535 : (unsigned short int)tmp);
			}
		}
	}
	else if (data_type == IS_LINT_IMAGE)
	{
		for (i = 0, pd = oid->im.pixel; i < ony ; i++)
		{
		        tmpy = cos((2*M_PI*i)/period);
			for (j=0; j< onx; j++)
			{
			        tmpx = cos((2*M_PI*j)/period);
				tmp = (float)0x7FFFFFFF + 0.5;
				tmp = (tmpx*tmpy*tmp) - .5;
				pd[i].li[j] = (int)tmp; 
				  //(tmp < (float)0x80000000) ? 0x80000000 :
				//	((tmp > (float)0x7FFFFFFF) ? 0x7FFFFFFF : (int)tmp);
			}
		}
	}
	else if (data_type == IS_FLOAT_IMAGE)
	{
		for (i = 0, pd = oid->im.pixel; i < ony ; i++)
		{
		        tmpy = cos((2*M_PI*i)/period);
			for (j=0; j< onx; j++)
			{
			        tmpx = cos((2*M_PI*j)/period);
				tmp = 1;
				tmp = (tmpx*tmpy*tmp);
				pd[i].fl[j] = tmp;
			}
		}
	}
	else if (data_type == IS_DOUBLE_IMAGE)
	{
		for (i = 0, pd = oid->im.pixel; i < ony ; i++)
		{
		        tmpy = cos((2*M_PI*i)/period);
			for (j=0; j< onx; j++)
			{
			        tmpx = cos((2*M_PI*j)/period);
				tmp = 1;
				tmp = (tmpx*tmpy*tmp);
				pd[i].db[j] = tmp;
			}
		}
	}
	else if (data_type == IS_COMPLEX_IMAGE)
	{
		for (i = 0, pd = oid->im.pixel; i < ony ; i++)
		{
		        tmpy = cos((2*M_PI*i)/period);
			for (j=0; j< onx; j++)
			{
			        tmpx = cos((2*M_PI*j)/period);
				tmp = 1;
				tmp = (tmpx*tmpy*tmp);
				pd[i].fl[2*j] = tmp;
				pd[i].fl[2*j+1] = tmp;
			}
		}
	}
	else if (data_type == IS_COMPLEX_DOUBLE_IMAGE)
	{
		for (i = 0, pd = oid->im.pixel; i < ony ; i++)
		{
		        tmpy = cos((2*M_PI*i)/period);
			for (j=0; j< onx; j++)
			{
			        tmpx = cos((2*M_PI*j)/period);
				tmp = 1;
				tmp = (tmpx*tmpy*tmp);
				pd[i].db[2*j] = tmp;
				pd[i].db[2*j+1] = tmp;
			}
		}
	}
	else if (data_type == IS_RGB_PICTURE)
	{
		for (i = 0, pd = oid->im.pixel; i < ony ; i++)
		{
		        tmpy = cos((2*M_PI*i)/period);
			for (j=0; j< onx; j++)
			{
			        tmpx = cos((2*M_PI*j)/period);
				tmp = (tmpx*tmpy*127.5) + 127.5;
				pd[i].rgb[j].r = pd[i].rgb[j].g = pd[i].rgb[j].b = 
				  (tmp < 0) ? 0 :((tmp > 255) ? 255 : (unsigned char)tmp);

			}
		}
	}
	else if (data_type == IS_RGBA_PICTURE)
	{
		for (i = 0, pd = oid->im.pixel; i < ony ; i++)
		{
		        tmpy = cos((2*M_PI*i)/period);
			for (j=0; j< onx; j++)
			{
			        tmpx = cos((2*M_PI*j)/period);
				tmp = (tmpx*tmpy*127.5) + 127.5;
				pd[i].rgba[j].r = pd[i].rgba[j].g = pd[i].rgba[j].b = 
				  (tmp < 0) ? 0 :((tmp > 255) ? 255 : (unsigned char)tmp);
				pd[i].rgba[j].a = 255;

			}
		}
	}
	set_im_title(oid, "periodic modulationy %d",period);
	set_formated_string(&oid->im.source,"Periodic Image %d", period);
	return oid;
}

int do_testim_image_new(void)
{
	O_i *oid;
	imreg *imr;
	static int i, onx = 256, ony = 256, type = 0, period = 32, data_type;

	if(updating_menu_state != 0)	return D_O_K;

	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine multiply the image intensity"
		"by a user defined scalar factor");
	}
	if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
	{
		win_printf("Cannot find image region");
		return D_O_K;
	}
	i = win_scanf("Creating a new image with a periodic modulation\n"
		      "size in x%dsize in y %d"
		      "Type Char %R Uint %r SInt %r Lint %r RGB %r\n"
		      "RGBA %r float %r double %r complex %r complex double %r\n"
		      "define the period in x and y %d",&onx,&ony,&type,&period);
	if (i == CANCEL)	return D_O_K;
	if (type == 0)  data_type = IS_CHAR_IMAGE;
	else if (type == 1)  data_type = IS_UINT_IMAGE;
	else if (type == 2)  data_type = IS_INT_IMAGE;
	else if (type == 3)  data_type = IS_LINT_IMAGE;
	else if (type == 4)  data_type = IS_RGB_PICTURE;
	else if (type == 5)  data_type = IS_RGBA_PICTURE;
	else if (type == 6)  data_type = IS_FLOAT_IMAGE;
	else if (type == 7)  data_type = IS_DOUBLE_IMAGE;
	else if (type == 8)  data_type = IS_COMPLEX_IMAGE;
	else if (type == 9)  data_type = IS_COMPLEX_DOUBLE_IMAGE;
	else   win_printf_OK("wrong image type");
	
	oid = testim_image_new(onx,ony,data_type,period);
	if (oid == NULL)	return win_printf_OK("unable to create image!");
	add_image(imr, oid->type, (void*)oid);
	find_zmin_zmax(oid);
	return (refresh_image(imr, imr->n_oi - 1));
}
int do_timeup_copy_image(void)
{
	O_i *oid, *ois;
	imreg *imr;
	static int  s;
	unsigned long start;

	if(updating_menu_state != 0)	return D_O_K;

	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine copy one image and measure the time"
		"require to copy the data");
	}
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	{
		win_printf("Cannot find image region");
		return D_O_K;
	}
	oid = duplicate_image(ois, NULL);
	if (oid == NULL)     return D_O_K;
	add_to_image(imr, IS_ONE_IMAGE, (void*)oid);
	imr->cur_oi = imr->n_oi-1;
	imr->one_i = imr->o_i[imr->cur_oi];
	imr->one_i->need_to_refresh |= ALL_NEED_REFRESH;            
	if (oid->im.data_type == IS_CHAR_IMAGE)                s = 1;
	else if (oid->im.data_type == IS_UINT_IMAGE)           s = 2;
	else if (oid->im.data_type == IS_INT_IMAGE)            s = 2;
	else if (oid->im.data_type == IS_LINT_IMAGE)           s = 4;
	else if (oid->im.data_type == IS_RGB_PICTURE)          s = 3;
	else if (oid->im.data_type == IS_RGBA_PICTURE)         s = 4;
	else if (oid->im.data_type == IS_FLOAT_IMAGE)          s = 4;
	else if (oid->im.data_type == IS_DOUBLE_IMAGE)         s = 8;
	else if (oid->im.data_type == IS_COMPLEX_IMAGE)        s = 8;
	else if (oid->im.data_type == IS_COMPLEX_DOUBLE_IMAGE) s = 16;


	s *= oid->im.nx*oid->im.ny;
	if (oid->im.n_f) s *= oid->im.n_f;
	start = my_uclock();
	memcpy(oid->im.mem[0],ois->im.mem[0],s);
	win_printf("memcopy of %d bytes in %g s",s,
		   (double)(my_uclock()-start)/get_my_uclocks_per_sec());
	return D_REDRAWME;    
}

MENU *testim_image_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"New modulated image", do_testim_image_new,NULL,0,NULL);
	add_item_to_menu(mn,"Time out im copy", do_timeup_copy_image,NULL,0,NULL);
	return mn;
}

int	testim_main(int argc, char **argv)
{
	add_image_treat_menu_item ( "testim", NULL, testim_image_menu(), 0, NULL);
	return D_O_K;
}

int	testim_unload(int argc, char **argv)
{
	remove_item_to_menu(image_treat_menu, "testim", NULL, NULL);
	return D_O_K;
}
#endif

