#ifndef _TESTIM_H_
#define _TESTIM_H_

PXV_FUNC(int, do_testim_image_new, (void));
PXV_FUNC(MENU*, testim_image_menu, (void));
PXV_FUNC(int, testim_main, (int argc, char **argv));
#endif

