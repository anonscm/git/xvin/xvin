// following functions are defined in 'response_math.c':
PXV_FUNC(r_func*, init_response,				(int N, int n_fft, float fs));
PXV_FUNC(int, reset_response,					(r_func *response, int N, int n_fft, float fs));
PXV_FUNC(void, free_response,					(r_func *response));
PXV_FUNC(int, compute_response_from_float_data, (r_func *response, float *x_f, float *y_f, 
												int nx, float f_acq, int n_fft, int n_overlap, int bool_hanning));
PXV_FUNC(int, compute_response_from_float_data_using_correlation, (r_func *response, float *x_f, float *y_f, 
												int nx, float f_acq, int n_fft, int n_overlap, int bool_hanning));
PXV_FUNC(int, inverse_response,					(r_func *response));
PXV_FUNC(int, normalize_response,				(r_func *response, double norm));
PXV_FUNC(int, multiply_responses,				(r_func *response, r_func *response2));


