#ifndef _NOISE_REDUCTION_C_
#define _NOISE_REDUCTION_C_

#include "allegro.h"
#include "xvin.h"
#include "fftw3_lib.h"
#include "fft_filter_lib.h"
#include "../../src/menus/plot/treat/p_treat_fftw.h"

/* If you include other plug-ins header do it here*/ 

/* But not below this define */
#define BUILDING_PLUGINS_DLL

#include "response.h"

extern r_func *current_response;




int noise_reduction_for_psd_float(float *x_f, float *y_f, float *psd_f, float *freq, int nx, float f_acq, int n_fft, int n_overlap, 
								int bool_hanning, int update_current_response)
{	int			i;
	int			n_window;
//	double 		modulus2;
	float		*psd_noise, *psd_cross_real, *psd_cross_imag;
	r_func      *H=NULL;
		
	psd_noise 	   = (float*)calloc((n_fft/2+1), sizeof(float));	
	psd_cross_real = (float*)calloc((n_fft/2+1), sizeof(float));	
	psd_cross_imag = (float*)calloc((n_fft/2+1), sizeof(float));	
	
	H        = init_response(n_fft/2+1, n_fft, f_acq);
	n_window = compute_response_from_float_data(H, x_f, y_f, nx, f_acq, n_fft, n_overlap, bool_hanning);
	
	if (update_current_response==1)
	{	reset_response(current_response, n_fft/2+1, n_fft, f_acq);
   		for (i=0 ; i<= n_fft/2 ; i++)
   		{	current_response->f[i]    = i*f_acq/n_fft;
			current_response->r[i][0] = H->r[i][0]; 
			current_response->r[i][1] = H->r[i][1];		
    	}   
		current_response->mode = RESPONSE_PLOT_MODULUS;
		current_response->info = my_sprintf(current_response->info, "Response function with %d points (N_{fft}=%d)", current_response->n, n_fft);
	}
	
	
	i=compute_psd_of_float_data(      x_f,      psd_f,     freq, f_acq, nx, n_fft, n_overlap, bool_hanning); // the initial, noisy, psd
	if (i!=n_window) win_printf_OK("number of points is inconsistant!!!");
	
	i=compute_psd_of_float_data(      y_f,  psd_noise,     freq, f_acq, nx, n_fft, n_overlap, bool_hanning); // the initial, noisy, psd
	if (i!=n_window) win_printf_OK("number of points is inconsistant!!!");
	
	i=compute_cross_psd_of_float_data(x_f, y_f, psd_cross_real, psd_cross_imag, freq, f_acq, nx, n_fft, n_overlap, bool_hanning); // noise psd
	if (i!=n_window) win_printf_OK("number of points is inconsistant!!!");

	// denoising of PSD:
	for (i=0; i<n_fft/2; i++)
	{	// modulus2 = H->r[i][0]*H->r[i][0] + H->r[i][1]*H->r[i][1];
		psd_f[i] -= (psd_cross_real[i]*psd_cross_real[i] + psd_cross_imag[i]*psd_cross_imag[i])/psd_noise[i];
	}
	
	free(psd_noise);	free(psd_cross_real);
	free(psd_cross_imag);
	free_response(H);
	
	return(n_window);
}






// following function is the one called from the menu
// it then calls math. function 'noise_reduction_for_psd_float'
int do_noise_reduction_psd(void)
{
	register int i, k;
	O_p 	*op1, *opd=NULL;
	int	nx, n2;
	static int n_fft, n_overlap;
	d_s 	*ds1, *ds2, *dsd=NULL;
	pltreg *pr = NULL;
	int	index;
	char	message[512];
	float	f_acq;
	static int		bool_hanning=1, bool_plot_psd_initial=1, bool_plot_psd_noise=1, bool_plot_response=1, bool_use_energy=0;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine tries a noise reduction technique on PSD\n\n"
					"previous and resulting PSD can be plotted, and so is the response function");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)	return win_printf_OK("cannot plot region");
	nx = ds1->nx;	// number of points in time series 
	if (op1->n_dat<2)					return(win_printf_OK("not enough datasets !"));
	i = op1->cur_dat;
	if (i>0) i--;
	else 	 i = op1->n_dat-1;
	ds2= op1->dat[i];
	if (ds2->nx != nx)					return(win_printf_OK("datasets do not have the same number of points !"));
	if (nx<4) 						return(win_printf_OK("not enough points !"));

    for (n2 = 1; n2 < nx; n2 <<= 1); n2>>=1;	// n2 is the closest power of 2 lower than or equal to nx 
	n_fft     = (n2>8192) ? 4096 : n2;
	n_overlap = n_fft*0.8;
	if (	( (ds1->xd[2]-ds1->xd[1])==(ds1->xd[1]-ds1->xd[0]) )
	    &&  ( (ds1->xd[2]-ds1->xd[1])!=0 ) ) 
	{  f_acq=1/(ds1->xd[1]-ds1->xd[0]);
	}
	sprintf(message, "{\\pt14\\color{yellow}Noise reduction (for PSD)}\n"
			"PSD of active ds%d will be computed, and noise from ds%d will be removed from it.\n"
			"datasets have %d points. lower or equal power of 2 is %d\n"
			"number of points of fft window ? %%d"
			"overlap (in points) between windows ? %%d"
			"acquisition frequency (in Hz, for normalization) ?%%f"
			"Hanning window ? (0 or 1) %%b\n"
			"%%b plot initial PSD of ds%d\n"
			"%%b plot noise PSD from ds%d\n"
/*			"%%b plot modulus of response function ds%d/ds%d\n" */
			"%%b use energy (A^2/Hz) instead of amplitude (A/\\sqrt{Hz})", 
			op1->cur_dat, i, nx, n2, op1->cur_dat, i /*, op1->cur_dat, i*/ );
	if (win_scanf(message, &n_fft, &n_overlap, &f_acq,&bool_hanning, 
			&bool_plot_psd_initial, &bool_plot_psd_noise, /* &bool_plot_response,*/ &bool_use_energy) == CANCEL) return D_O_K;
	if (n_overlap>=n_fft) return win_printf("overlap is too large compared to n\\_fft (%d>=%d)", n_overlap, n_fft);

	opd = create_and_attach_one_plot(pr, (n_fft/2)+1, (n_fft/2)+1, 0);
    if (opd == NULL)        return win_printf_OK("Cannot allocate plot");
    dsd = opd->dat[0];
    if (dsd == NULL)    	return win_printf_OK("Cannot allocate dataset");
    
    if (bool_plot_psd_initial==1)
	{	compute_psd_of_float_data(ds1->yd, dsd->yd, dsd->xd, f_acq, nx, n_fft, n_overlap, bool_hanning);
		inherit_from_ds_to_ds(dsd, ds1);
		dsd->treatement = my_sprintf(dsd->treatement,"P.S.D. (%s) over %d points, f_{acq}=%gHz, %s", 
			(bool_use_energy==0) ? "amplitude" : "energy", n_fft, f_acq, 
			(bool_hanning==1) ? "Hanning window" : "no windowing");
		dsd = create_and_attach_one_ds (opd, (n_fft/2)+1, (n_fft/2)+1, 0);
	}
	if (bool_plot_psd_noise==1)
	{	compute_psd_of_float_data(ds2->yd, dsd->yd, dsd->xd, f_acq, nx, n_fft, n_overlap, bool_hanning);
		inherit_from_ds_to_ds(dsd, ds2);
		dsd->treatement = my_sprintf(dsd->treatement,"P.S.D. (%s) over %d points, f_{acq}=%gHz, %s", 
			(bool_use_energy==0) ? "amplitude" : "energy", n_fft, f_acq, 
			(bool_hanning==1) ? "Hanning window" : "no windowing");
		dsd = create_and_attach_one_ds (opd, (n_fft/2)+1, (n_fft/2)+1, 0);
	}
/*	if (bool_plot_response==1)
	{	for (i=0; i<n_fft/2+1; i++)
		{	dsd->xd[i] = current_response->f[i];
			dsd->yd[i] = (float)( current_response->r[i][0]*current_response->r[i][0] 
							+ current_response->r[i][1]*current_response->r[i][1] );
		}
		set_ds_history(dsd, "%s", current_response->info);
		dsd->treatement = my_sprintf(dsd->treatement,"%s of response function",
								(bool_use_energy==0) ? "modulus" : "squared modulus");
		dsd = create_and_attach_one_ds (opd, (n_fft/2)+1, (n_fft/2)+1, 0);
	}
*/
	noise_reduction_for_psd_float(ds1->yd, ds2->yd, dsd->yd, dsd->xd, nx, f_acq, n_fft, n_overlap, 
								bool_hanning, bool_plot_response);

	inherit_from_ds_to_ds(dsd, ds1);
	dsd->treatement = my_sprintf(dsd->treatement,"noise reduced P.S.D. (%s) over %d points, f_{acq}=%gHz, %s", 
			(bool_use_energy==0) ? "amplitude" : "energy", n_fft, f_acq, 
			(bool_hanning==1) ? "Hanning window" : "no windowing");

	if (bool_use_energy==0)		// here, user asked for a psd in amplitude units instead of energy units
	{	for (k=0; k<opd->n_dat; k++)
		{	for (i=0 ; i<= n_fft/2 ; i++)
    		{      	opd->dat[k]->yd[i] = (float) sqrt(opd->dat[k]->yd[i]);
    		}
		}
	} 
	
    set_plot_title(opd, "PSD with noise reduction");
    set_plot_x_title(opd, "frequency (Hz)");
    set_plot_y_title(opd, (bool_use_energy==0) ? "V/\\sqrt{Hz}" : "V^2/Hz");
    opd->filename = Transfer_filename(op1->filename);
    opd->dir = Mystrdup(op1->dir);

    opd->iopt |= XLOG;
    opd->iopt |= YLOG;
	
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the do_noise_reduction_psd








int noise_reduction_float(float *x_f, float *y_f, float *xg_f, int nx, float f_acq, int n_fft, int n_overlap, 
								int bool_hanning, int update_current_response)
{	int			i, j, k;
	int			n_window, nx_dest;
	float		*psd_noise, *psd_cross_real, *psd_cross_imag, *freq;
	r_func      *H=NULL;
	double		*x_d, *y_d, tmp_r, tmp_i;
	short		*i_win;
	fftw_complex *xf, *yf;
	fftw_plan	plan_x_direct, plan_y_direct, plan_x_inverse;
		
	psd_noise 	   = (float*)calloc((n_fft/2+1), sizeof(float));	
	psd_cross_real = (float*)calloc((n_fft/2+1), sizeof(float));	
	psd_cross_imag = (float*)calloc((n_fft/2+1), sizeof(float));	
	freq		   = (float*)calloc((n_fft/2+1), sizeof(float));	
	
	n_window=compute_psd_of_float_data(      y_f,  psd_noise,    freq, f_acq, nx, n_fft, n_overlap, bool_hanning); // the noise psd
	i       =compute_cross_psd_of_float_data(x_f, y_f, psd_cross_real, psd_cross_imag, freq, f_acq, nx, n_fft, n_overlap, bool_hanning);
	if (i!=n_window) win_printf_OK("number of points is inconsistant!!!");
	
	H = init_response(n_fft/2+1, n_fft, f_acq);
	// here we compute the transfert function H = H1/H2 :
	for (i=0; i<n_fft/2; i++)
	{	H->r[i][0] = psd_cross_real[i]/psd_noise[i];
		H->r[i][1] = psd_cross_imag[i]/psd_noise[i];
	}
	
	// we can update the active response function with H, if asked for.
	if (update_current_response==1)
	{	reset_response(current_response, n_fft/2+1, n_fft, f_acq);
   		for (i=0 ; i<= n_fft/2 ; i++)
   		{	current_response->f[i]    = i*f_acq/n_fft;
			current_response->r[i][0] = H->r[i][0]; 
			current_response->r[i][1] = H->r[i][1];		
    	}   
		current_response->mode = RESPONSE_PLOT_MODULUS;
		current_response->info = my_sprintf(current_response->info, "Response function with %d points (N_{fft}=%d)", current_response->n, n_fft);
	}
		
	// here we apply the noise reduction:
	n_window = (nx-n_fft)/(n_fft-n_overlap) + 1;		// number of windows
	nx_dest  = nx - (nx-n_fft)%(n_fft-n_overlap);	// number of points in the resulting array
	
	i_win=(short*)calloc(nx_dest, sizeof(short));
	for (i=0; i<nx_dest; i++) {	xg_f[i]  = 0.;
								i_win[i] = 0;
							  }
	
	x_d = fftw_malloc((n_fft+(n_fft%2))*sizeof(double)); 
	y_d = fftw_malloc((n_fft+(n_fft%2))*sizeof(double)); 
	xf  = fftw_malloc((n_fft/2+1)*sizeof(fftw_complex)); // FT of noisy signal
	yf  = fftw_malloc((n_fft/2+1)*sizeof(fftw_complex)); // FT of noise alone
	if ( (x_d==NULL) || (xf==NULL) || (y_d==NULL) || (yf==NULL) ) return(win_printf_OK("malloc error")); 

	plan_x_direct  = fftw_plan_dft_r2c_1d(n_fft, x_d, xf, FFTW_ESTIMATE);
	plan_y_direct  = fftw_plan_dft_r2c_1d(n_fft, y_d, yf, FFTW_ESTIMATE);
	plan_x_inverse = fftw_plan_dft_c2r_1d(n_fft, xf, x_d, FFTW_ESTIMATE);
	
 	k=0;
	for (j=0; j<(nx-n_fft)+1; j+=(n_fft-n_overlap) )
	{	for (i=0; i<n_fft ; i++)	{	x_d[i] = (double)x_f[j+i];
										y_d[i] = (double)y_f[j+i];
									}
		if (bool_hanning==1)		{	fft_window_real(n_fft, x_d, 0.01);
										fft_window_real(n_fft, x_d, 0.01);
									}
       	fftw_execute(plan_x_direct);
       	fftw_execute(plan_y_direct);

		for (i=0; i<(n_fft/2+1); ++i)
        {	tmp_r    = yf[i][0];
        	tmp_i    = yf[i][1];
	        xf[i][0] -= (tmp_r*H->r[i][0] - tmp_i*H->r[i][1]);
			xf[i][1] -= (tmp_r*H->r[i][1] + tmp_i*H->r[i][0]);
		}

        fftw_execute(plan_x_inverse);
		if (bool_hanning==1)		fft_unwindow_real(n_fft, x_d, 0.01);

		for (i=0; i<n_fft ; i++)	{ 	xg_f[j+i]  += (float)x_d[i];
										i_win[j+i] ++;
									}

		k++;
	} // at the end of the loop, we have averaged $k$ spectra
	if (k==0) return win_printf_OK("nothing done with your values !");
	
   	for (i=0 ; i<nx_dest; i++)	
	{	xg_f[i] /= (i_win[i]*n_fft);
    }
	
	free(psd_noise);		free(freq);
	free(psd_cross_real);	free(psd_cross_imag);
	free(i_win);
	fftw_free(x_d);			fftw_free(y_d);
	fftw_free(xf);			fftw_free(yf);
	fftw_destroy_plan(plan_x_direct);
	fftw_destroy_plan(plan_y_direct);
	fftw_destroy_plan(plan_x_inverse);
	
	free_response(H);
		
	return(n_window);
}




// following function is the one called from the menu
// it then calls math. function 'noise_reduction_float'
int do_noise_reduction(void)
{
	int		i;
	O_p 	*op1;
	int		nx, n2, nx_dest;
	static int n_fft, n_overlap;
	d_s 	*ds1, *ds2, *dsd=NULL;
	pltreg *pr = NULL;
	int	index;
	char	message[512];
	float	f_acq;
	static int		bool_hanning=0, bool_hold_response=1;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine tries a noise reduction technique on real data\n\n"
					"a new dataset is created");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)	return win_printf_OK("cannot plot region");
	nx = ds1->nx;	// number of points in time series 
	if (op1->n_dat<2)				return(win_printf_OK("not enough datasets !"));
	i = op1->cur_dat;
	if (i>0) i--;
	else 	 i = op1->n_dat-1;
	ds2= op1->dat[i];
	if (ds2->nx != nx)				return(win_printf_OK("datasets do not have the same number of points !"));
	if (nx<4) 						return(win_printf_OK("not enough points !"));

    for (n2 = 1; n2 < nx; n2 <<= 1); n2>>=1;	// n2 is the closest power of 2 lower than or equal to nx 
	n_fft     = (n2>8192) ? 4096 : n2;
	n_overlap = n_fft*0.8;
	if (	( (ds1->xd[2]-ds1->xd[1])==(ds1->xd[1]-ds1->xd[0]) )
	    &&  ( (ds1->xd[2]-ds1->xd[1])!=0 ) ) 
	{  f_acq=1/(ds1->xd[1]-ds1->xd[0]);
	}
	sprintf(message, "{\\pt14\\color{yellow}Noise reduction}\n"
			"active ds%d will be processed on, assuming ds%d contains noise information to remove.\n"
			"datasets have %d points. lower or equal power of 2 is %d\n"
			"number of points of fft window ? %%d"
			"overlap (in points) between windows ? %%d"
			"acquisition frequency (in Hz, for normalization) ?%%f"
			"%%b Hanning window ?\n"
			"%%b update current response function with H1/H2=<ds%d ds%d*>/<ds%d ds%d*>\n", 
			op1->cur_dat, i, nx, n2, op1->cur_dat, i, i, i);
	if (win_scanf(message, &n_fft, &n_overlap, &f_acq, &bool_hanning, &bool_hold_response) == CANCEL) return D_O_K;
	if (n_overlap>=n_fft) return win_printf("overlap is too large compared to n\\_fft (%d>=%d)", n_overlap, n_fft);

	nx_dest  = nx - (nx-n_fft)%(n_fft-n_overlap);
	dsd = create_and_attach_one_ds (op1, nx_dest, nx_dest, 0);
	
	noise_reduction_float(ds1->yd, ds2->yd, dsd->yd, nx, f_acq, n_fft, n_overlap, bool_hanning, bool_hold_response);
	for (i=0; i<nx_dest; i++) dsd->xd[i] = ds1->xd[i];
	
	inherit_from_ds_to_ds(dsd, ds1);
	dsd->treatement = my_sprintf(dsd->treatement,"noise reduction, using windows of %d points, f_{acq}=%gHz, %s", 
			n_fft, f_acq, (bool_hanning==1) ? "Hanning window" : "no windowing");

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the do_noise_reduction_psd







#endif

