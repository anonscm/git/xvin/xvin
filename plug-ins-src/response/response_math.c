#ifndef _RESPONSE_MATH_C_
#define _RESPONSE_MATH_C_

#include "allegro.h"
#include "xvin.h"
#include "../../include/xvin/fftw3_lib.h"
#include "../../include/xvin/fft_filter_lib.h"
#include "../../src/menus/plot/treat/p_treat_fftw.h"

/* If you include other plug-ins header do it here*/ 

/* But not below this define */
#define BUILDING_PLUGINS_DLL

#include "response.h"
#include "response_math.h"

extern r_func *current_response;


r_func* init_response(int N, int n_fft, float fs)
{	r_func *response;
	int 	i;

	response = malloc(sizeof(r_func));
	
	response->f = fftw_malloc(N*sizeof(float));
	response->r = fftw_malloc(N*sizeof(fftw_complex));
	response->info = NULL;
	response->n = N;
	
	for (i=0; i<N; i++) 
	{	response->f[i]    = (float)0.;
		response->r[i][0] = (double)0.;
		response->r[i][1] = (double)0.;
	}
	
	response->n_fft  = n_fft;
	response->fs 	= fs;

	response->mode = 0;  // this value is tested to activate functions in the response menu

	return(response);
}




int reset_response(r_func *response, int N, int n_fft, float fs)
{	int i;

	if (response->n!=N)
	{	fftw_free(response->f);
		fftw_free(response->r);
		response->f = fftw_malloc(N*sizeof(float));
		response->r = fftw_malloc(N*sizeof(fftw_complex));
		response->n = N;
	}

	for (i=0; i<N; i++) 
	{	response->f[i]    = (float)0.;
		response->r[i][0] = (double)0.;
		response->r[i][1] = (double)0.;
	}
	
	response->n_fft  = n_fft;
	response->fs 	= fs;

	response->mode=1;
	if (response->info!=NULL) 
	{	free(response->info);
		response->info = NULL;
	}

	return(response->n);
}




void free_response(r_func *response)
{	fftw_free(response->f);
	fftw_free(response->r);
	if (response->info!=NULL) free(response->info);
	free(response);	
}





int compute_response_from_float_data(r_func *response, float *x_f, float *y_f, int nx, float f_acq, int n_fft, int n_overlap, int bool_hanning)
{	register int i,j,k;
	double modulus;
	double *x_d, *y_d;
	fftw_complex *xf, *yf, *RF;
	fftw_plan 	plan_x, plan_y;
		
	x_d =       (double*)fftw_malloc(n_fft*sizeof(double));
	y_d = 	    (double*)fftw_malloc(n_fft*sizeof(double));
	xf  = (fftw_complex*)fftw_malloc((n_fft/2+1)*sizeof(fftw_complex));
	yf  = (fftw_complex*)fftw_malloc((n_fft/2+1)*sizeof(fftw_complex));
	RF  = (fftw_complex*)fftw_malloc((n_fft/2+1)*sizeof(fftw_complex));

	for (i=0; i<(n_fft/2+1); i++) { RF[i][0]=0; RF[i][1]=0; }

	plan_x = fftw_plan_dft_r2c_1d(n_fft, x_d, xf, FFTW_ESTIMATE);
	plan_y = fftw_plan_dft_r2c_1d(n_fft, y_d, yf, FFTW_ESTIMATE);

	k=0;
	for (j=0; j<(nx-n_fft)+1; j+=(n_fft-n_overlap) )
	{	for (i=0; i<n_fft ; i++)
		{	x_d[i] = (double)x_f[j+i];
			y_d[i] = (double)y_f[j+i];
		}
		if (bool_hanning==1)
		{	fft_window_real(n_fft, x_d, 0.001);
			fft_window_real(n_fft, y_d, 0.001);
		}

        	fftw_execute(plan_x);
        	fftw_execute(plan_y);

		for (i=0; i<(n_fft/2+1); ++i)
        {	modulus = yf[i][0]*yf[i][0] + yf[i][1]*yf[i][1];
			RF[i][0] += ( xf[i][0]*yf[i][0] + xf[i][1]*yf[i][1])/modulus;
			RF[i][1] += (-xf[i][0]*yf[i][1] + xf[i][1]*yf[i][0])/modulus;
		}
		k++;
	} // at the end of the loop, we have averaged $k$ spectra
	if (k==0) return(0);

	for (i=0; i<(n_fft/2+1); ++i)
    {	RF[i][0] /= k;
		RF[i][1] /= k;
	}
	
	reset_response(response, n_fft/2+1, n_fft, f_acq);
   	for (i=0 ; i<= n_fft/2 ; i++)
   	{	response->f[i]    = i*f_acq/n_fft;
		response->r[i][0] = RF[i][0]; 
		response->r[i][1] = RF[i][1];		
    }   
	response->mode = RESPONSE_PLOT_MODULUS;
	response->info = my_sprintf(response->info, "Response function with %d points (N_{fft}=%d)", response->n, n_fft);

	fftw_free(x_d);				fftw_free(y_d);
	fftw_free(xf);				fftw_free(yf);
	fftw_free(RF);
	fftw_destroy_plan(plan_x);	fftw_destroy_plan(plan_y);
	return(k);
}




/* Amelioration de la fonction de r�ponse en utilisant la cross-correlation */
int compute_response_from_float_data_using_correlation(	r_func *response, float *x_f, float *y_f,
													 	int nx, float f_acq, int n_fft, int n_overlap, int bool_hanning)
{	int			i;
	int n_window;
	fftw_complex *RF;
	float		*psd_excitation, *psd_cross_real, *psd_cross_imag;
	r_func      *H=NULL;

	RF  = (fftw_complex*)fftw_malloc((n_fft/2+1)*sizeof(fftw_complex));
	reset_response(response, n_fft/2+1, n_fft, f_acq);
   	for (i=0 ; i<= n_fft/2 ; i++)
   	{	response->f[i]    = i*f_acq/n_fft;	
    }   
    
    	
	psd_excitation 	   = (float*)calloc((n_fft/2+1), sizeof(float));	
	psd_cross_real = (float*)calloc((n_fft/2+1), sizeof(float));	
	psd_cross_imag = (float*)calloc((n_fft/2+1), sizeof(float));	
	
	H        = init_response(n_fft/2+1, n_fft, f_acq);
	n_window = compute_response_from_float_data(H, x_f, y_f, nx, f_acq, n_fft, n_overlap, bool_hanning);
	
	i=compute_psd_of_float_data(      y_f,      psd_excitation,     response->f, f_acq, nx, n_fft, n_overlap, bool_hanning); // the initial, noisy, psd
	if (i!=n_window) win_printf_OK("number of points is inconsistant!!!");
	
	i=compute_cross_psd_of_float_data(x_f, y_f, psd_cross_real, psd_cross_imag, response->f, f_acq, nx, n_fft, n_overlap, bool_hanning); // noise psd
	if (i!=n_window) win_printf_OK("number of points is inconsistant!!!");
	
	
	
	for (i=0; i<(n_fft/2+1); ++i)
        {	RF[i][0]=psd_cross_real[i]/psd_excitation[i];
        	RF[i][1]=psd_cross_imag[i]/psd_excitation[i];
		}
	
	for (i=0 ; i<= n_fft/2 ; i++)
   	{	response->r[i][0] = RF[i][0]; 
		response->r[i][1] = RF[i][1];		
    }   
	response->mode = RESPONSE_PLOT_MODULUS;
	response->info = my_sprintf(response->info, "Response function with %d points (N_{fft}=%d)", response->n, n_fft);

	free(psd_excitation);	free(psd_cross_real);
	free(psd_cross_imag);
	return(i);
}





/* replace a response function H by its inverse 1/H */
int inverse_response(r_func *response)
{	int i;
	double modulus2;

	for (i=0; i<response->n; i++)
	{	modulus2 = ( response->r[i][0]*response->r[i][0] + response->r[i][1]*response->r[i][1] );
		response->r[i][0] /=  modulus2;
		response->r[i][1] /= -modulus2; 
	}
	response->info = my_sprintf(response->info, "\ninverted: H(\\omega) replaced by 1/H(\\omega)");
	return(response->n);
}


/* normalize a response function H, ie, makes it such that H(omega=0) = 1	*/
/* as the norm 'norm' is best computed in real space than in Fourier space, */
/* the function asks for it, and it should be computed outside.				*/
/* N.G., 2006/05/09															*/
int normalize_response(r_func *response, double norm)
{	int i;
	
	for (i=0; i<response->n; i++)
	{	response->r[i][0] /= norm;
		response->r[i][1] /= norm; 
	}
	response->info = my_sprintf(response->info, "\nnormalized: H(\\omega) divided by %g", norm);
	return(response->n);
}


/* multiply 2 response functions, and places the product in the first one	*/
/* N.G., 2006/05/05															*/
int multiply_responses(r_func *response, r_func *response2)
{	int i;
	double re, im;

	for (i=0; i<response->n; i++)
	{	re = response->r[i][0];
		im = response->r[i][1];
	
		response->r[i][0] = re * response2->r[i][0] - im * response2->r[i][1];
		response->r[i][1] = re * response2->r[i][1] + im * response2->r[i][0]; 
	}
	response->info = my_sprintf(response->info, "\nmultiplied by response %s", response2->info);
	return(response->n);
}


#endif

