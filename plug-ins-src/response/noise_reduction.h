

// following functions are defined in 'noise_reduction.c':
PXV_FUNC(int, do_noise_reduction_psd,			(void));
PXV_FUNC(int, do_noise_reduction,				(void));
PXV_FUNC(int, noise_reduction_for_psd_float,	(float *x_f, float *y_f, float *psd_f, float *freq, 
												int nx, float f_acq, int n_fft, int n_overlap, 
												int bool_hanning, int update_current_response));
