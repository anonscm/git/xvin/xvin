#ifndef _RESPONSE_C_
#define _RESPONSE_C_

#include "allegro.h"
#include "xvin.h"
#include "fft_filter_lib.h"
#include "../../src/menus/plot/treat/p_treat_fftw.h"

// #include <fftw3.h>

/* If you include other plug-ins header do it here*/ 

/* But not below this define */
#define BUILDING_PLUGINS_DLL

#include "response.h"

r_func	*current_response=NULL;




int do_correct_LP_filter(void)
{	double	fspe, modulus2, Rr, Ri, Fr, Fi;
static float fc=9500.;
static int   bool_divide_LP=0;
	int	i;
	
	if(updating_menu_state != 0)	
	{	if (current_response->mode==0)	active_menu->flags |=  D_DISABLED;
		else	 			active_menu->flags &= ~D_DISABLED; 
		return D_O_K;	
	}
	
	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK(	"This routine correct the response function\n"
        					     	"with the response function of the photodiode amplifier.\n\n"
        					     	"This must be applied to the inverse of H = output/input");
	}

	i=win_scanf("I will deconvolute with \n\n"
				"H_{LP} = \\frac{1}{1+i\\frac{f}{f_c}}\n\n"
				"cutoff frequency f_c ? %f\n"
                "%R multiply or %r divide by H_{LP}\n"
                "{\\pt8 (multiply for the inverse response, or divide for the non-inverse response}", 
                &fc, &bool_divide_LP);
	
	for (i=0; i<current_response->n; i++)
	{	fspe = current_response->f[i];
		/* modulus2 = (0.56 - 1.56668e-8 * (fspe*fspe))*(0.56 - 1.56668e-8 * (fspe*fspe))
					 + ( 1.3e-4 * fspe + 2.81205e-12 * fspe*fspe*fspe ) * ( 1.3e-4 * fspe + 2.81205e-12 * fspe*fspe*fspe );
		Rr = current_response->r[i][0] ;
		Ri = current_response->r[i][1] ;
		Fr = 0.56  *  ((0.56 - 1.56668e-8 * fspe*fspe)) / modulus2; 
		Fi = 0.56  * (-( 1.3e-4 * fspe + 2.81205e-12 * fspe*fspe*fspe )) / modulus2;
*/
		Rr = current_response->r[i][0] ;
		Ri = current_response->r[i][1] ;
		
        if (bool_divide_LP==0)
        { modulus2 = (1.+fspe*fspe/(fc*fc));
		  Fr = 1./modulus2;
		  Fi = - 1./fc*fspe/modulus2;
        }
        else 
        { Fr = 1.;
          Fi = fspe/fc;
        }
		current_response->r[i][0] = Rr*Fr - Ri*Fi;
		current_response->r[i][1] = Rr*Fi + Ri*Fr;
	}
	current_response->info = my_sprintf(current_response->info, "\ncorrected by LP filter f_c = %g", fc);
	return(0);
} // end of function "do_correct_LP_filter"






int do_correct_arb_response(void)
{	pltreg 	*pr = NULL;
	O_p 	*op1;
	r_func 	*correction_response;
	float	norm;
static	int	bool_inverse_correction=1, bool_normalize_correction=1;
	
	if(updating_menu_state != 0)	
	{	if (current_response->mode==0)	active_menu->flags |=  D_DISABLED;
		else	 			active_menu->flags &= ~D_DISABLED; 
		return D_O_K;	
	}
	
	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK(	"This routine corrects the active response function H\n"
        					     	"with another response function.\n\n"
        					     	"The correction H_{correction} is read from active plot using 2 datasets\n"
        					     	"(you will be prompted to indicate how to load the correction response function)\n"
        					     	"This must be applied to the inverse of H = output/input\n\n"
        					     	"\\frac{1}{H'} = \\frac{H_{correction}}{H} will be computed\n\n"
        					     	"(you can decide to invert and/or normalize the correction function)");
	}
			
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op1) != 2)	return win_printf_OK("cannot find plot data");
	
	correction_response = init_response(1, 1, current_response->fs);	// we impose n_fft=1
	ds_to_response_engine(op1, "correction", correction_response);
	if (correction_response->n_fft==1)	
				return(D_O_K); // nothing was done
	if (correction_response->n_fft!=current_response->n_fft)	
				return(win_printf_OK("current response, and loaded correction response don't have\n"
									"the same number of points.")); 
	
	// here, we cheat, and assume the norm of the response funciton is well given by the lowest (non-zero) frequency :
	norm = fsqrt(correction_response->r[1][0]*correction_response->r[1][0] + correction_response->r[1][1]*correction_response->r[1][1]);
	if (bool_inverse_correction==1) norm = 1./norm;
									
	if (win_scanf("H' = H*H_{correction}, you have H', and want H\n"
			"so \\frac{1}{H} = \\frac{H_{correction}}{H'}\n\n"
			"%b Inverse the correction response ?\n"
			"%b Normalize the correction response ?\n"
			"    if so, use norm %8f", 
			&bool_inverse_correction, &bool_normalize_correction, &norm) == CANCEL) 
			{	free_response(correction_response);
				return(D_O_K);
			}
			
	if (bool_inverse_correction==0)   inverse_response(correction_response); // this test may seem inverted, but
													// we multiply instead of dividing, so we use the inverse!
	if (bool_normalize_correction==1) normalize_response(correction_response, (double)norm);
		
	
	if (multiply_responses(current_response, correction_response) != current_response->n) win_printf_OK("problem ?");
	
	free_response(correction_response);
	current_response->info = my_sprintf(current_response->info, "\n(correction)");

	return(D_O_K);
}




	
int do_compute_response(void)
{
	register int i, k=0;
	O_p 	*op1, *opd=NULL;
	int	nx, n2, n_overlap;
static int	n_fft;
	d_s 	*ds1, *ds2, *dsd=NULL;
	pltreg *pr = NULL;
	int	index;
	char	*message=NULL, *message_warning=NULL;
	float	inv_dt=0.; // this is a local measure of the acquisition frequency, performed as 1/dt
static float f_acq=1, percent_overlap=0.8;
	int	bool_hanning=1;
static int bool_remember_values=1, bool_overlap_type=1, bool_crossresponse=1;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine computes the (complex) response function H\n\n"
					"defined in Fourier space by S = H.E\n"
					"with S being the active dataset\n"
					"and E being the previous dataset.\n\n"
					"Response function is plotted, and kept in memory.");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)	return win_printf_OK("cannot plot region");
	nx = ds1->nx;	// number of points in time series 
	if (op1->n_dat<2)				return(win_printf_OK("not enough datasets !"));
	i = op1->cur_dat;
	if (i>0) i--; else i=op1->n_dat-1; // the index of previous dataset
	ds2= op1->dat[i];
	if (ds2->nx != nx)				return(win_printf_OK("datasets do not have the same number of points !"));
	if (nx<4) 						return(win_printf_OK("not enough points !"));

	// a tentative measure of f_acq:
	if ( (ds1->xd[2]-ds1->xd[1])!=0 )
	{	inv_dt=(float)1./(ds1->xd[1]-ds1->xd[0]);
	    if ( (ds1->xd[2]-ds1->xd[1])!=(ds1->xd[1]-ds1->xd[0]) )
		message_warning=my_sprintf(message_warning, "\n{\\color{lightred}dt is not constant !}");  
	}
	else message_warning=my_sprintf(message_warning, "\n{\\color{lightred}dt=0 at t=0 ?}");
	
	for (n2 = 1; n2 < nx; n2 <<= 1); n2>>=1;	// n2 is the closest power of 2 lower than or equal to nx 
	
	if ( (bool_remember_values==0) || (f_acq==1) )
	{	n_fft     = (n2>8192) ? 4096 : n2;
		percent_overlap=0.8;
		f_acq=(float)inv_dt;
	}
	if (inv_dt!=f_acq)
	{	if (bool_remember_values==1) message_warning = my_sprintf(message_warning,"\n{\\color{lightred}f_acq has changed : I updated it.}");
		f_acq=(float)inv_dt;
	}

	if (bool_overlap_type==1)	n_overlap = (int)((float)100.*percent_overlap);
	else 						n_overlap = (int)((float)n_fft*percent_overlap);
	
	message = my_sprintf(message, "{\\pt14\\color{yellow}Response function H}\n\n"
			"such that ds%d = H.ds%d\n\n"
			"datasets have %d points. lower or equal power of 2 is %d\n"
			"- number of points of fft window : %%8d\n"
			"- overlap between windows       : %%8d\n"
			"     overlap in %%R points or %%r in percents\n"
			"- acquisition frequency (in Hz) : %%8f\n"
			"     (for normalization) %s\n\n"
			"%%b use Hanning window ?\n"
			"%%R Normal response H=ds%d/ds%d or %%r Cross-response H=<ds%d ds%d*> / (ds%d ds%d*)\n"
			"%%b remember values", op1->cur_dat, i, nx, n2, message_warning, op1->cur_dat, i, op1->cur_dat, i, i, i);
	k=win_scanf(message,&n_fft, &n_overlap, &bool_overlap_type, &f_acq, &bool_hanning, &bool_crossresponse, &bool_remember_values);
	free(message); if (message_warning!=NULL) free(message_warning);
	if (k==CANCEL) return(D_O_K);
		
	percent_overlap = (float)n_overlap/((float)(100.));
	if (bool_overlap_type==1)	n_overlap       =(int)(percent_overlap*(float)n_fft);
	else						percent_overlap =(float)n_overlap/(float)n_fft;
	
	if ( (n_overlap>=n_fft) || (percent_overlap>=1.) )	
	return(win_printf_OK("overlap is too large compared to n-fft\n"
					"in points    : %d>=%d\n"
					"in percents : %d>=100", n_overlap, n_fft, (int)((float)100.*percent_overlap)));
	
	if (bool_crossresponse==0)
		k=compute_response_from_float_data(current_response, ds1->yd, ds2->yd, nx, f_acq, n_fft, n_overlap, bool_hanning);
	if (bool_crossresponse==1)
		k=compute_response_from_float_data_using_correlation(current_response, ds1->yd, ds2->yd, nx, f_acq, n_fft, n_overlap, bool_hanning);
	if (k==0) return win_printf_OK("nothing done with your values !");

    current_response->mode |= RESPONSE_CONTINUOUS; // we just build a response function, it is continuous

 	opd = create_and_attach_one_plot(pr, (n_fft/2)+1, (n_fft/2)+1, 0);
    	if (opd == NULL)        return(win_printf_OK("Cannot allocate plot"));
    	dsd = opd->dat[0];
    	if (dsd == NULL)     	return(win_printf_OK("Cannot allocate dataset"));
	
	for (i=0 ; i<= n_fft/2 ; i++)
   	{  	dsd->xd[i] = current_response->f[i];
       	dsd->yd[i] = (float)sqrt( current_response->r[i][0]*current_response->r[i][0] + current_response->r[i][1]*current_response->r[i][1] );
    }   
	
    inherit_from_ds_to_ds(dsd, ds1);
	dsd->treatement = my_sprintf(dsd->treatement, "%s", current_response->info);	

    set_plot_title(opd, "%s", current_response->info);	
    opd->filename = Transfer_filename(op1->filename);	
    opd->dir = Mystrdup(op1->dir);	

    opd->iopt |= XLOG;
    opd->iopt |= YLOG;	
	
	refresh_plot(pr, UNCHANGED);
	return(D_O_K);
} // end of the do_compute_response







int ds_to_response_engine(O_p 	*op1, char *info, r_func *response)
{	register int i, k;
	float	f_acq=1.;
	int		n_fft=1024;
	int		nx, i2;
	d_s 	*ds1, *ds2=NULL;
	static int conversion_mode=1, bool_discrete=0;
	char 	*message=NULL;

	ds1=op1->dat[op1->cur_dat];	if (ds1==NULL) return(win_printf_OK("dataset not found"));
	nx = ds1->nx;

	message=my_sprintf(message, "{\\pt14\\color{yellow}Convert dataset(s) into {\\color{lightgreen}%s} response function}\n\n"
			"%%R use 1 dataset only (for real part and modulus, assuming zero imaginary part)\n"
			"%%r use 2 datasets for real and imaginary parts\n"
			"%%r use 2 datasets for modulus and phase\n\n\n"
			"%%R continuous (standard) or %%r discrete (from sines) response function", info);
	i=win_scanf(message, &conversion_mode, &bool_discrete);
	free(message);
	if (i==CANCEL) return(D_O_K);

	if (bool_discrete==1)
	{	i=win_scanf("Discrete response function : Additional informations are required\n\n"
				"acquisition frequency %f\n"
				"number of points in a FFT window %d",
				&f_acq, &n_fft);
		if (i==CANCEL) return(D_O_K);
	}
	else // continuous response
	{	f_acq = ds1->xd[nx-1]*2;
		n_fft = (nx-1)*2;
	}
	
	if (conversion_mode==0)
	{	reset_response(response, nx, n_fft, f_acq);
	 	for (i=0; i<nx; i++)
		{	response->f[i]    = ds1->xd[i];
			response->r[i][0] = ds1->yd[i];
			response->r[i][1] = 0;
		}
		response->mode = RESPONSE_PLOT_REAL;
		response->info = my_sprintf(response->info, 
					"Response function from dataset with %d points,\nreal part only", nx);
	}
	else
	{ 	if (op1->n_dat<2) 	return(win_printf_OK("not enough datasets !"));
		i2 = op1->cur_dat;
		if (i2>0) i2--; else i2 = op1->n_dat-1;
		ds2= op1->dat[i2];
		if (ds2->nx != nx)	return(win_printf_OK("datasets do not have the same number of points !"));

		k=0;
		reset_response(response, nx, n_fft, f_acq);

		if (conversion_mode==1)
		{ 	for (i=0; i<nx; i++)
			{	if (ds1->xd[i]!=ds2->xd[i]) k++;
				response->f[i]    = ds1->xd[i];
				response->r[i][0] = ds1->yd[i];
				response->r[i][1] = ds2->yd[i];
			}
			
			response->mode = (RESPONSE_PLOT_REAL | RESPONSE_PLOT_IMAG);
			response->info = my_sprintf(response->info, 
					"Response function from datasets with %d points,\nreal and imaginary parts", nx);

			win_printf_OK("{\\color{lightgreen}%s} response function updated\n"
					"using dataset %d for real part\n"
					"and dataset %d for imaginary part.", info, op1->cur_dat, i2);
		}
		else if (conversion_mode==2)
		{ 	for (i=0; i<nx; i++)
			{	if (ds1->xd[i]!=ds2->xd[i]) k++;
				response->f[i]    = ds1->xd[i];
				response->r[i][0] = ds1->yd[i]*cos(ds2->yd[i]);
				response->r[i][1] = ds1->yd[i]*sin(ds2->yd[i]);
			}
			response->mode = (RESPONSE_PLOT_MODULUS | RESPONSE_PLOT_PHASE);
			response->info = my_sprintf(response->info, 
					"Response function from datasets with %d points,\nmodulus and phase", nx);

			win_printf_OK("{\\color{lightgreen}%s} response function updated\n"
					"using dataset %d for modulus\n"
					"and dataset %d for phase.", info, op1->cur_dat, i2);
		}
		if (k!=0) win_printf_OK("\t\tN.B.\t\t\n"
				"The two datasets had different frequencies\nover %d points, for a total of %d points.",
				k, nx);
	}
	
	if (bool_discrete==1) 
    {  response->mode |= RESPONSE_DISCRETE;
       response->info = my_sprintf(response->info, "\nDiscrete response function from a sum of sines");
    }
    else
    {  response->mode |= RESPONSE_CONTINUOUS;
    }
					
	return(D_O_K);
}



int do_ds_to_response(void)
{
	O_p 	*op1 = NULL;
	pltreg *pr = NULL;
	int	index;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine converts the active dataset(s) into"
					"the current response function");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op1) != 2)	return win_printf_OK("cannot access plot data");
	
	ds_to_response_engine(op1, "active", current_response);
	
	return D_O_K;
} // end of the do_ds_to_response




int do_build_response_derivate(void)
{	register int i;
	O_p 	*op1 = NULL;
	d_s 	*ds1;
	int	ny, n2, n_fft = 4096;
	float	f_acq=1;
	pltreg *pr = NULL;
	int	index, f_type;
	char	*message=NULL;
	int	bool_is_psd=0;	// is the active ds a psd ?

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;
	if ( (index!=RESPONSE_DERIVATE) && (index!=RESPONSE_INTEGRATE) ) return(win_printf_OK("Bad call !"));

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine computes the response function in Fourier space\n"
					"corresponding to time-%s of a signal.\n\n"
					"no dataset is created, but the current response function is updated",
					(index==RESPONSE_DERIVATE) ? "derivation" : "integration");
	}

	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)	return win_printf_OK("cannot plot region");
	ny = ds1->ny;	// number of points in time series

	f_type=0;
	if (op1->y_title!=NULL)	
	{ if ( (strcmp(op1->y_title,"V/\\sqrt{Hz}")==0) || (strcmp(op1->y_title,"V^2/Hz")==0) )
		{ 	bool_is_psd=1; 
			f_type=1;
		} 
	}

	if (win_scanf("{\\pt14\\color{yellow}Response function H =d/dt or \\int.dt}\n\n"
		"%R select frequency range from time-series\n%r select frequency range from PSD",&f_type)==CANCEL) 
		return(D_O_K);

	if (f_type==0)
        {    	for (n2 = 1; n2 < ny; n2 <<= 1); n2>>=1;	// n2 is the closest power of 2 lower than or equal to ny
		n_fft     = (n2>8192) ? 4096 : n2;
		if (	( (ds1->xd[2]-ds1->xd[1])==(ds1->xd[1]-ds1->xd[0]) )
		    &&  ( (ds1->xd[2]-ds1->xd[1])!=0 ) ) 
		{  f_acq=1/(ds1->xd[1]-ds1->xd[0]);
		}
		message=my_sprintf(message, "{\\pt14\\color{yellow}Response function H for %s}\n\n"
			"current dataset has %d points. lower or equal power of 2 is %d\n"
			"%%6d points in fft window\n"
			"%%6f Hz is the acquisition frequency (for normalization)\n",
			(index==RESPONSE_DERIVATE) ? "derivation d/dt" : "integration \\int.dt", ny, n2);
		i=win_scanf(message, &n_fft, &f_acq);
		free(message);
		if (i==CANCEL) return D_O_K;

		ny = (n_fft/2+1);
		reset_response(current_response, ny, n_fft, f_acq);
		for (i=0; i<ny; i++)
		{	current_response->f[i]	  = i*f_acq/n_fft;
			current_response->r[i][0] = 0;
			if (index==RESPONSE_DERIVATE)	current_response->r[i][1] =    2*M_PI*current_response->f[i];
			else if (i!=0)			current_response->r[i][1] = 1/(2*M_PI*current_response->f[i]);
				else			current_response->r[i][1] = 0;
		}
		current_response->mode = RESPONSE_PLOT_MODULUS;
		current_response->info = my_sprintf(current_response->info, 
				"Response function for %s with %d points\n(N_{FFT}=%d, f_{acq}=%5.2f)", 
				(index==RESPONSE_DERIVATE) ? "derivation" : "integration", ny, n_fft, f_acq);

	}

	else if (f_type==1)
	{	if (bool_is_psd!=1) return(win_printf_OK("current dataset is not a PSD !"));
		ny = ds1->ny;

		reset_response(current_response, ny, (ny-1)*2, ds1->xd[ny-1]*2);
		for (i=0; i<ny; i++)
		{	current_response->f[i]	  = ds1->xd[i];
			current_response->r[i][0] = 0;
			if (index==RESPONSE_DERIVATE)	current_response->r[i][1] =   (2*M_PI*ds1->xd[i]);
			else if (ds1->xd[i]!=0)		current_response->r[i][1] = 1/(2*M_PI*ds1->xd[i]);
				else			current_response->r[i][1] = 0;
		}
		current_response->mode = RESPONSE_PLOT_MODULUS;
		current_response->info = my_sprintf(current_response->info, 
				"Response function for %s with %d points\n(N_{FFT}=%d, f_{acq}=%5.2f)", 
				(index==RESPONSE_DERIVATE) ? "derivation" : "integration", ny, 2*(ny-1), 2*ds1->xd[ny-1]);
	}

	return D_O_K;
}





int do_plot_response(void)
{
	register int i;
	O_p 	*op1 = NULL;
	int	ny;
	d_s 	*ds1, *ds2;
	pltreg *pr = NULL;
	int	index;

	if(updating_menu_state != 0)	
	{	if (current_response->mode==0)	active_menu->flags |=  D_DISABLED;
		else	 			active_menu->flags &= ~D_DISABLED; 
		return D_O_K;	
	}
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine plots the current response function\n\n"
					"a new dataset is created.");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)	return win_printf_OK("cannot access plot data");

	ny = current_response->n;

	if (index & RESPONSE_PLOT_REAL)
	{ 	ds2 = create_and_attach_one_ds (op1, ny, ny, 0);
		for (i=0; i<ny; i++)
		{	ds2->xd[i] = current_response->f[i];
			ds2->yd[i] = (float)current_response->r[i][0];
		}
		set_ds_history(ds2, "%s", current_response->info);
		ds2->treatement = my_sprintf(ds2->treatement,"Real part of response function");
	}
	if (index & RESPONSE_PLOT_IMAG)
	{ 	ds2 = create_and_attach_one_ds (op1, ny, ny, 0);
		for (i=0; i<ny; i++)
		{	ds2->xd[i] = current_response->f[i];
			ds2->yd[i] = (float)current_response->r[i][1];
		}
		set_ds_history(ds2, "%s", current_response->info);
		ds2->treatement = my_sprintf(ds2->treatement,"Imaginary part of response function");
	}
	if (index & RESPONSE_PLOT_MODULUS)
	{ 	ds2 = create_and_attach_one_ds (op1, ny, ny, 0);
		for (i=0; i<ny; i++)
		{	ds2->xd[i] = current_response->f[i];
			ds2->yd[i] = (float)sqrt( current_response->r[i][0]*current_response->r[i][0] 
						+ current_response->r[i][1]*current_response->r[i][1] );
		}
		set_ds_history(ds2, "%s", current_response->info);
		ds2->treatement = my_sprintf(ds2->treatement,"Modulus of response function");
	}
	if (index & RESPONSE_PLOT_PHASE)
	{ 	ds2 = create_and_attach_one_ds (op1, ny, ny, 0);
		for (i=0; i<ny; i++)
		{	ds2->xd[i] = current_response->f[i];
			ds2->yd[i] = (float)atan2(current_response->r[i][1],current_response->r[i][0]);
		}
		set_ds_history(ds2, "%s", current_response->info);
		ds2->treatement = my_sprintf(ds2->treatement,"Phase of response function");
	}

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the do_plot_response





int do_plot_FDT(void)
{
	register int i;
	O_p 	*op1 = NULL;
	int	ny;
	d_s 	*ds1, *ds2;
	pltreg *pr = NULL;
	int	index;
static int bool_use_pulsation=1, bool_take_absolute=1;

	if(updating_menu_state != 0)	
	{	if (current_response->mode==0)	active_menu->flags |=  D_DISABLED;
		else	 			active_menu->flags &= ~D_DISABLED; 
		return D_O_K;	
	}
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine plots imaginary part of the current response function,\n"
					"divided by the frequency (or pulsation)\n\n"
					"a new dataset is created.");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)	return win_printf_OK("cannot access plot data");
	ny = current_response->n;
	
	win_scanf("{\\color{yellow}TFD test : compute \\frac{Im(H(\\omega))}{\\omega}\n\n"
			"%b divide by \\omega=2\\pi f instead of f\n"
			"%b take absolute value of Im(\\omega)\n",
			&bool_use_pulsation, &bool_take_absolute);

	ds2 = create_and_attach_one_ds (op1, ny, ny, 0);
	for (i=0; i<ny; i++)
	{	ds2->xd[i] = current_response->f[i];
		if (ds2->xd[i]!=0.)	ds2->yd[i] = (float)current_response->r[i][1]/(float)current_response->f[i];
		else				ds2->yd[i] = (float)0.;
	}
	
	if (bool_use_pulsation==1)
	{	for (i=0; i<ny; i++)
		ds2->yd[i] /= (float)(2.*M_PI);
	}
	
	if (bool_take_absolute==1)
	{	for (i=0; i<ny; i++)
		ds2->yd[i] = (float)fabs(ds2->yd[i]);
	}
	
	set_ds_history(ds2, "%s", current_response->info);
	ds2->treatement = my_sprintf(ds2->treatement,"Imaginary part of response function, divided by %s",
					(bool_use_pulsation==1) ? "\\omega" : "f");

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the do_plot_response



int do_inverse_response(void)
{	
	if(updating_menu_state != 0)	
	{	if (current_response->mode==0)	active_menu->flags |=  D_DISABLED;
		else	 			active_menu->flags &= ~D_DISABLED; 
		return D_O_K;	
	}
	
	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return(win_printf_OK("This routine inverses the current response function H(\\omega)\n"
        				"i.e., it puts 1/H instead of H as the current response function in memory\n"
					"no dataset is created."));
	}

	if (inverse_response(current_response) != current_response->n) win_printf_OK("problem ?");
	
	return(D_O_K);
} // end of the do_inverse_response








int do_apply_response(void)
{
	register int i=0, j, k;
	O_p 	*op1;
	int	nx, nx_dest, k_loop=0, n2, n_fft, n_overlap;
	d_s 	*ds1, *dsd=NULL;
	pltreg *pr = NULL;
	int	index, bool_hanning=1;
	char	*message=NULL;
	float	f_acq;
	double	tmp_r, tmp_i;
	double	*x;
	fftw_complex 	*xf;
	fftw_plan 	plan_direct, plan_inverse;

	if(updating_menu_state != 0)	
	{	if (current_response->mode==0)	active_menu->flags |=  D_DISABLED;
		else	 			active_menu->flags &= ~D_DISABLED; 
		return D_O_K;	
	}
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine applies the current (complex) response function H\n\n"
					"to the active dataset\n\n"
					"A new dataset is created.");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)	return win_printf_OK("cannot access data !");
	nx = ds1->nx;	// number of points in time series 
	if (nx<4) 						return(win_printf_OK("not enough points !"));

	if (current_response->n != (current_response->n_fft/2+1) ) 
		return(win_printf_OK("Response function has not enough points\n"
						"It is probably a discrete response function, and therefore it cannot be applied"));
	
    	for (n2 = 1; n2 < nx; n2 <<= 1); n2>>=1;	// n2 is the closest power of 2 lower than or equal to nx 
	n_fft     = (n2>8192) ? 4096 : n2;
	n_overlap = n_fft*0.8;
	if (	( (ds1->xd[2]-ds1->xd[1])==(ds1->xd[1]-ds1->xd[0]) )
	    &&  ( (ds1->xd[2]-ds1->xd[1])!=0 ) ) 
	{  f_acq=1/(ds1->xd[1]-ds1->xd[0]);
	}

	n_fft = current_response->n_fft;
	message=my_sprintf(message, "Dataset has %d points.\n"
			"Current response function has %d points,\n so the FT window will have {\\color{Lightgreen}%d} points.\n"
			"overlap (in points) between windows ? %%d"
			"acquisition frequency (in Hz, for normalization) ?%%f"
			"Hanning window ? (0 or 1) %%b", nx, current_response->n, current_response->n_fft );
	i=win_scanf(message,&n_overlap,&f_acq,&bool_hanning);
	free(message);
	if (i==CANCEL) return D_O_K;
	if (n_fft!=current_response->n_fft) 
		i=win_printf("press OK to select %d points or CANCEL to abort", 2*(current_response->n-1));
	if (f_acq!=current_response->fs) 
		i=win_printf("press OK to select %f as the sampling frequency or CANCEL to abort", f_acq);
	if (i==CANCEL) return(D_O_K);
	if (n_overlap>=n_fft-1) 
		return(win_printf_OK("overlap is too large compared to n\\_fft (%d>=%d-1)", n_overlap, n_fft));

	k_loop = (nx-n_fft)/(n_fft-n_overlap) + 1;
	nx_dest = nx - (nx-n_fft)%(n_fft-n_overlap);
	if (nx!=nx_dest) 
	{	i=win_printf_OK("{\\pt14\\color{yellow}N.B.!}\n\nThere are %d points in the initial dataset\n"
				"There will be %d points in the resulting dataset\n\n"
				"This is due to the choice of the overlapping : %d points\n"
				"and the size of the FT window (%d points)\n"
				"There will be %d different overlapping windows", nx, nx_dest, n_overlap, n_fft, k_loop);
		if (i==CANCEL) return(D_O_K);
	}

	x  = fftw_malloc((n_fft+(n_fft%2))*sizeof(double));
	xf = fftw_malloc((n_fft/2+1)*sizeof(fftw_complex));
	if ( (x==NULL) || (xf==NULL) ) return(win_printf_OK("malloc error")); 

	plan_direct  = fftw_plan_dft_r2c_1d(n_fft, x, xf, FFTW_ESTIMATE);
	plan_inverse = fftw_plan_dft_c2r_1d(n_fft, xf, x, FFTW_ESTIMATE);

 	dsd = create_and_attach_one_ds(op1, nx_dest, nx_dest, 0);
    	if (dsd == NULL)     	return(win_printf_OK("Cannot allocate dataset"));

	k=0;
	for (j=0; j<(nx-n_fft)+1; j+=(n_fft-n_overlap) )
	{	for (i=0; i<n_fft ; i++)	x[i] = (double)ds1->yd[j+i];

		if (bool_hanning==1)		fft_window_real(n_fft, x, 0.01);

        	fftw_execute(plan_direct);

		for (i=0; i<(n_fft/2+1); ++i)
        	{	tmp_r    = xf[i][0];
			tmp_i    = xf[i][1];
			xf[i][0] = tmp_r*current_response->r[i][0] - tmp_i*current_response->r[i][1];
			xf[i][1] = tmp_r*current_response->r[i][1] + tmp_i*current_response->r[i][0];
		}

        	fftw_execute(plan_inverse);

		if (bool_hanning==1)		fft_unwindow_real(n_fft, x, 0.01);

		for (i=0; i<n_fft ; i++)	
		{				dsd->yd[j+i] += (float)x[i];
						dsd->xd[j+i] ++;	// a trick to count how many points were added here
		}

		k++;
	} // at the end of the loop, we have averaged $k$ spectra
	if (k==0) return win_printf_OK("nothing done with your values !");

   	for (i=0 ; i<nx_dest; i++)	
	{	dsd->yd[i] /= (dsd->xd[i]*n_fft);
		dsd->xd[i]  = ds1->xd[i];
    }

    inherit_from_ds_to_ds(dsd, ds1);
	dsd->treatement = my_sprintf(dsd->treatement,"convolution with %s", 
				(current_response->info==NULL) ? "unknown response function" : current_response->info);

	fftw_destroy_plan(plan_direct);	fftw_destroy_plan(plan_inverse);
	fftw_free(x);
	fftw_free(xf);
	
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the do_apply_response





int	do_print_response_informations(void)
{
	if(updating_menu_state != 0)	
	{//	if (current_response->mode==0)	active_menu->flags |=  D_DISABLED;
	//	else	 			active_menu->flags &= ~D_DISABLED; 
		return D_O_K;	
	}

	if (current_response->info==NULL) win_printf_OK("no information available");
	else win_printf_OK("{\\color{yellow}Current response function information}\n\n%s\n"
                "response mode : %d (%s%s)\n"
				"%d points in the response function\n"
				"%d points in the fft window\n"
				"sampling frequency was %g Hz",
				current_response->info, current_response->mode, 
                (current_response->mode & RESPONSE_CONTINUOUS) ? "continuous" : "",
                (current_response->mode & RESPONSE_DISCRETE)   ? "discrete"   : "",
                current_response->n, current_response->n_fft, current_response->fs);

	return(D_O_K);
}





MENU *response_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;

	add_item_to_menu(mn,"Compute response",			do_compute_response,		NULL, 0, NULL);	
	add_item_to_menu(mn,"ds -> response",	 		do_ds_to_response,			NULL, 0, NULL);	
	add_item_to_menu(mn,"Inverse response",	 		do_inverse_response,		NULL, 0, NULL);	
	add_item_to_menu(mn,"\0",						0,							NULL, 0, NULL);
	add_item_to_menu(mn,"Derivation",		 		do_build_response_derivate,	NULL, RESPONSE_DERIVATE, NULL);
	add_item_to_menu(mn,"Integration",		 		do_build_response_derivate,	NULL, RESPONSE_INTEGRATE, NULL);
	add_item_to_menu(mn,"\0",						0,							NULL, 0, NULL);
	add_item_to_menu(mn,"Response informations",	do_print_response_informations,NULL, 0, NULL);
	add_item_to_menu(mn,"Plot response (Re and Im)",do_plot_response,			NULL, RESPONSE_PLOT_REAL | RESPONSE_PLOT_IMAG,	NULL);	
	add_item_to_menu(mn,"Plot response (modulus)",	do_plot_response,			NULL, RESPONSE_PLOT_MODULUS,			NULL);	
	add_item_to_menu(mn,"Plot response (phase)",	do_plot_response,			NULL, RESPONSE_PLOT_PHASE,			NULL);	
	add_item_to_menu(mn,"Plot Im(f)/f (FDT)",		do_plot_FDT,				NULL, 0, NULL);	
	add_item_to_menu(mn,"\0",	 			 		0,							NULL, 0, NULL);	
	add_item_to_menu(mn,"Apply response",	 	 	do_apply_response,			NULL, 0, NULL);	
	add_item_to_menu(mn,"\0",	 			 		0,							NULL, 0, NULL);	
	add_item_to_menu(mn,"noise reduction on PSD",	do_noise_reduction_psd,		NULL, 0, NULL);	
	add_item_to_menu(mn,"noise reduction on data",	do_noise_reduction,			NULL, 0, NULL);	
	add_item_to_menu(mn,"\0",	 			 		0,							NULL, 0, NULL);	
	add_item_to_menu(mn,"correct by Low-Pass filter",		do_correct_LP_filter,	 NULL, 0, NULL);	
	add_item_to_menu(mn,"correct by arbitrary response",	do_correct_arb_response, NULL, 0, NULL);	
	
	return mn;
}

int	response_main(int argc, char **argv)
{
	current_response = init_response(2049, 4096, 1024.);
	add_plot_treat_menu_item ("response", NULL, response_plot_menu(), 0, NULL);

	return D_O_K;
}


int	response_unload(int argc, char **argv)
{ 
	remove_item_to_menu(plot_treat_menu,"response",	NULL, NULL);
	free_response(current_response);

	return D_O_K;
}
#endif
