#ifndef _RESPONSE_H_
#define _RESPONSE_H_

// definitions for the types of response functions:
#define	RESPONSE_PLOT_REAL			0x000100
#define	RESPONSE_PLOT_IMAG			0x001000
#define	RESPONSE_PLOT_MODULUS		0x010000
#define	RESPONSE_PLOT_PHASE			0x100000
#define	RESPONSE_CONTINUOUS			0x000001
#define	RESPONSE_DISCRETE		    0x000010

// definitions for building the response function of derivation or integration:
#define RESPONSE_DERIVATE			0x000100
#define RESPONSE_INTEGRATE			0x000100

/************************************************************************/
typedef struct response_function 
/* this structure contains the complete information on a response function */
{
	int mode;		/* (real and imag) or (modulus and phase)	*/
	char *info;	/* a string giving some information 		*/
	int n_fft;	/* number of points in the fft window		*/
	float fs;		/* sampling frequency					*/
	int n;		/* size of the array 					*/
	float *f;  	/* pointer to the frequencies				*/
	fftw_complex *r;/* pointer to the values (complexes)		*/
} r_func;

extern r_func *current_response;

PXV_FUNC(MENU*, response_plot_menu, 			(void));
PXV_FUNC(int, response_main,					(int argc, char **argv));
PXV_FUNC(int, response_unload,					(int argc, char **argv));
	
PXV_FUNC(int, do_compute_response,				(void));
PXV_FUNC(int, do_ds_to_response,				(void));
PXV_FUNC(int, do_build_response_derivate,		(void));
PXV_FUNC(int, do_print_response_informations,	(void));
PXV_FUNC(int, do_plot_response,					(void));
PXV_FUNC(int, do_apply_response,				(void));

PXV_FUNC(int, do_correct_LP_filter,				(void));
PXV_FUNC(int, do_correct_arb_response,			(void));
PXV_FUNC(int, ds_to_response_engine,			(O_p 	*op1, char *info, r_func *response));

#include "response_math.h"
#include "noise_reduction.h"

#endif

