#pragma once
#ifndef XV_WIN32
# include "config.h"
#endif
#include "sequence.h"
#include <xvin.h>
#undef PS
#include <stdbool.h>

typedef unsigned int uint;

/**
 * @brief Watson cricks complementary of a nucleotide
 *
 * @param nucleo
 *
 * @return 
 */
PXV_FUNC(char, nucleo_complementary, (const char nucleo));

/**
 * @brief make the complementary but without changing 3' to 5', so it is the reverse of the
 * complementary handle A,C,G,T,W,S,N
 *
 * @param sequence
 *
 * @return sequence in 3' to 5' direction 
 */
PXV_FUNC(char, *dna_complementary_reverse, (const char *sequence));

/**
 * @brief make the complementary of a sequence handle A,C,G,T,W,S,N
 *
 * @param sequence
 *
 * @return return sequence in 5' to 3' direction
 */
PXV_FUNC(char, *dna_complementary, (const char *sequence));

/**
 * @brief reverse sequence of DNA, handle A,C,G,T,R,Y,S,W,K,M,B,D,H,V,N

 *
 * @param sequence to reverse
 *
 * @return the newly allocate reversed sequence 
 */
PXV_FUNC(char, *dna_reverse, (const char *sequence));

/**
 * @brief return true if this sequence is dna (A,C,G,T,U,R,Y,S,W,K,M,B,D,H,V,N)
 *
 * @param sequence to test
 *
 * @return true if dna is pure, false otherwise. 
 */
PXV_FUNC(bool, is_dna, (const char* sequence));

/**
 * @brief return true if dna is only constituted of ATUCG 
 *
 * @param sequence to test
 *
 * @return true if dna is pure, false otherwise. 
 */
PXV_FUNC(bool, is_pure_dna, (const char* sequence));

/**
 * @brief Compare two nucleotide, handle A,C,G,T,R,Y,S,W,K,M,B,D,H,V,N
 * A 	Adenine
 * C 	Cytosine
 * G 	Guanine
 * T (or U) 	Thymine (or Uracil)
 * R 	A or G
 * Y 	C or T
 * S 	G or C
 * W 	A or T
 * K 	G or T
 * M 	A or C
 * B 	C or G or T
 * D 	A or G or T
 * H 	A or C or T
 * V 	A or C or G
 * N 	any base
 *
 * @param a first nucleotide
 * @param b second nucleotide
 *
 * @return true if nucleotide match, false otherwise. 
 */
PXV_FUNC(bool, nucleo_cmp, (char a, char b));

PXV_FUNC(bool, dna_cmp, (const char *a, const char *b));


PXV_FUNC(int, find_oligo_in_sequence, (const sqd *msqd, const char *oligo, int start, int noNs));
PXV_FUNC(int, get_oligo_by_index, (char *oligo, int oligo_size, int index));
int dump_oligos(int oligo_size);
