# include "allegro.h"
# include "xvin.h"

/* If you include other regular header do it here*/


/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "dna_utils.h"

MENU *dna_utils_plot_menu(void)
{
    static MENU mn[32];

    if (mn[0].text != NULL)
    {
        return mn;
    }

    //add_item_to_menu(mn,"plot rescale in Y", do_dna_utils_rescale_plot,NULL,0,NULL);
    return mn;
}

int	dna_utils_main(int argc, char **argv)
{
    //dump_oligos(4);
    add_plot_treat_menu_item("dna_utils", NULL, dna_utils_plot_menu(), 0, NULL);
    (void)argc;
    (void)argv;
    return D_O_K;
}
