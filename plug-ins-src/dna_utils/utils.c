#include "utils.h"

#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
char nucleo_complementary(const char nucleo)
{
    switch (nucleo & 0x5F)
    {
    case 'A':
        return 'T';

    case 'T':
        return 'A';

    case 'C':
        return 'G';

    case 'G':
        return 'C';

    case 'W':
        return 'W';

    case 'S':
        return 'S';

    case 'N':
        return 'N';

    default:
        return ' ';
    }
}

char *dna_complementary_reverse(const char *sequence)
{
    int seq_len = strlen(sequence);
    char *complem = (char *) calloc(sizeof(char), seq_len + 1);

    for (int i = 0; i < seq_len; i++)   // we loop on the sequence
    {
        complem[i] = nucleo_complementary(sequence[i] & 0x5F);

        if (complem[i] == ' ')
        {
            free(complem);
            return NULL;
        }
    }

    complem[seq_len] = '\0';
    return complem;
}
char *dna_complementary(const char *sequence)
{
    int seq_len = strlen(sequence);
    char *complem = (char *) calloc(sizeof(char), seq_len + 1);

    for (int i = 0; i < seq_len; i++)   // we loop on the sequence
    {
        complem[seq_len - i - 1] = nucleo_complementary(sequence[i] & 0x5F);

        if (complem[seq_len - i - 1] == ' ')
        {
            free(complem);
            return NULL;
        }
    }

    complem[seq_len] = '\0';
    return complem;
}

char *dna_reverse(const char *sequence)
{
    int seq_len = strlen(sequence);
    char *rev = (char *) calloc(sizeof(char), seq_len + 1);

    for (int i = 0; i < seq_len; i++)   // we loop on the sequence
    {
        rev[seq_len - i - 1] = sequence[i] & 0x5F;
    }

    rev[seq_len] = '\0';
    return rev;
}
bool is_valid_nucleotide(char nucleotide)
{
    bool is_nucleo = true;

    switch (nucleotide & 0x5F)
    {
    case 'A':
    case 'T':
    case 'U':
    case 'C':
    case 'G':
    case 'R':
    case 'Y':
    case 'S':
    case 'W':
    case 'K':
    case 'M':
    case 'B':
    case 'D':
    case 'H':
    case 'V':
    case 'N':
        break;

    default:
        is_nucleo = false;
        break;
    }

    return is_nucleo;
}

bool is_pure_valid_nucleotide(char nucleotide)
{
    bool is_pure_nucleo = true;

    switch (nucleotide & 0x5F)
    {
    case 'A':
    case 'T':
    case 'U':
    case 'C':
    case 'G':
        break;

    default:
        is_pure_nucleo = false;
        break;
    }

    return is_pure_nucleo;
}

bool is_dna(const char *sequence)
{
    bool is_dna = true;
    int seq_len = strlen(sequence);

    for (int i = 0; is_dna && i < seq_len; i++)
    {
        is_dna &= is_valid_nucleotide(sequence[i]);
    }

    return is_dna;
}

bool is_pure_dna(const char *sequence)
{
    bool is_pure = true;
    int seq_len = strlen(sequence);

    for (int i = 0; is_pure && i < seq_len; i++)
    {
        is_pure &= is_pure_valid_nucleotide(sequence[i]);
    }

    return is_pure;
}

int nucleo_switch(char a, char b)
{
    switch (a)
    {
    case 'A':
        return b == 'R' || b == 'W' || b == 'M'
               || b == 'D' || b == 'H' || b == 'V' || b == 'N';

    case 'T':
    case 'U':
        return b == 'Y' || b == 'W' || b == 'K'
               || b == 'B' || b == 'D' || b == 'H' || b == 'N';

    case 'C':
        return b == 'Y' || b == 'S' || b == 'M'
               || b == 'B' || b == 'H' || b == 'V' || b == 'N';

    case 'G':
        return b == 'Y' || b == 'S' || b == 'M'
               || b == 'B' || b == 'H' || b == 'V' || b == 'N';

    case 'N':
        return true;

    default:
        return false;
    }
}

bool nucleo_cmp(char a, char b)
{
    a = a & 0x5F;
    b = b & 0x5F;

    if (!(a >= 'A' && a <= 'Z') || !(b >= 'A' && b <= 'Z'))
    {
        return false;
    }

    if (a == b)
    {
        return true;
    }

    if (!nucleo_switch(a, b))
    {
        return nucleo_switch(b, a);
    }

    return true;
}
bool dna_cmp(const char *a, const char *b)
{
    uint len = strlen(a);
    bool is_eq = true;

    if (len != strlen(b))
    {
        return 0;
    }

    for (uint i = 0; i < len && is_eq; ++i)
    {
        is_eq &= nucleo_cmp(a[i], b[i]);
    }

    return is_eq;
}

int find_oligo_in_sequence(const sqd *msqd, const char *oligo, int start, int noNs)
{
    assert(msqd != NULL);
    assert(msqd->seq != NULL);
    assert(msqd->totalSize > 0);
    assert(oligo != NULL);
    int oligo_len = strlen(oligo);
    int k = 0;
    int ip = 0;
    int ic = 0;

    for (int i = start; i < msqd->totalSize - oligo_len; i++)
    {
        bool match = true;

        for (int j = 0; j < oligo_len && match; j++)
        {
            k = i + j;
            ip = k >> msqd->ln2PageSize;
            ic = k & msqd->pageMask;
            char curNucleo = msqd->seq[ip][ic];

            if (noNs && ((curNucleo & 0x5F) != 'N'))
            {
                match = nucleo_cmp(oligo[j], curNucleo);
            }
        }

        if (match)
        {
            return i;
        }
    }

    return -1;
}

int get_oligo_by_index(char *oligo, int oligo_size, int index)
{
    const static char *atgc = "ACGT";

    for (int j = 0; j < oligo_size; ++j)
    {
        oligo[j] = atgc[(index >> ((oligo_size - j - 1) << 1)) & 3];
    }

    oligo[oligo_size] = '\0';
    return 0;
}

int dump_oligos(int oligo_size)
{
    char oligo[256];

    for (int i = 0; i < pow(4, oligo_size); ++i)
    {
        get_oligo_by_index(oligo, oligo_size, i);
        printf("%s\n", oligo);
    }
    return 0;
}
