#pragma once
typedef struct _sequence_data
{
  char **seq;      // the sequence stuff
  int ln2PageSize; // typically 12
  int pageSize;    // 2^ln2PageSize = 4096
  int pageMask;    // 0xFFF 
  int totalSize;   // the total number of bases
  int nPage;       // the number of page allocated
  char *description;
  char *filename;
  int ref_position;
  int nnumber;
} sqd;

typedef struct
{
    char *seq;
    double probability;
    int behaviors_size;
    char **behaviors;
    double *behaviors_proba;
} oligo_t;
