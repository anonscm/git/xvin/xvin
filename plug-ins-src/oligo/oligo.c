/*
 *    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
 */
#ifndef _OLIGO_C_
#define _OLIGO_C_

# include "xvin.h"
# include "allegro.h"
/* If you include other regular header do it here*/ 
# include "../../src/menus/plot/treat/p_treat_basic.h"

/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled
# include "../nrutil/nrutil.h"

#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "oligo.h"
//place here other headers of this plugin 
# include "xvin.h"
# include "allegro.h"

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "oligo.h"
// oligo is assume to have the same sequence and orientation
int matching_oligo(char *seq, int len, char *oligo, int size, int pos, float *score)
{
    register int i, j;
    float tmp, w;

    if (seq == NULL || oligo == NULL) return -1; 
    if (size > len) return -2;
    for (i = 0, w = 1; i < size && i+pos < len; i++)
    {
        // the effect of the mismatch increase as it is centered
        j = (i < size/2) ? i : size - i - 1;
        for (tmp = 1; j >= 0 ; j--, tmp *= 0.25);
        if (seq[i+pos] != oligo[i]) w *= tmp; 
    }
    *score = w;
    return 0;
}


int do_find_oligos_number_covering_sequence(void)
{
    register int i, j;
    static int len = 80, oliglen = 8, isseq = 0, first = 1, minGC = 2, maxGC = 6, minhit = 1;
    unsigned int oli[4096], k;
    static char seq[4096], comp[4096], file[4096], oligo[32], inseq[128], oligo_comp_rev[32], b;
    pltreg *pr = NULL;
    O_p *opn;
    d_s *dso;
    int noli, nmax, val,contGC, hit, hitc;
    size_t size;
    FILE *fp;
    static float maxallow = 0.25, max, maxc, valm[4096], valmc[4096];

    if(updating_menu_state != 0)	return D_O_K;

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine determine the number of oligos needed\n"
                             "  to cover a specific sequence of an hairpin");
    }
    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
        return win_printf_OK("cannot find data");
    if (first)
    {
        file[0] = 0;
        inseq[0] = 0;
        i = win_scanf("This routine determines the number of oligos needed\n"
                      "to cover a specific hairpin sequence given \n"
                      "%R  choose a specify a file \n"
                      "or %r directly Indicate your sequence%ls\n"
                      "Oligo length %8d bases\n"
                      "Max for mismatch %10f\n"
                      "Minimum number of hybridization position %6d\n"
                      "min number of GC %5d max number of GC %5d in oligo\n"
                      ,&isseq,inseq,&oliglen,&maxallow,&minhit,&minGC,&maxGC);

        if (i == WIN_CANCEL)	return OFF;
    }
    else
    {
        i = win_scanf("This routine determines the number of oligos needed\n"
                      "to cover a specific hairpin sequence given \n"
                      "%Rreuse previous sequence \n"
                      "%r  choose a specify a file \n"
                      "or %r directly Indicate your sequence%ls\n"
                      "Oligo length %8d bases\n"
                      "Max for mismatch %10f\n"
                      "Minimum number of hybridization position %6d\n"
                      "min number of GC %5d max number of GC %5d in oligo\n"
                      ,&isseq,inseq,&oliglen,&maxallow,&minhit,&minGC,&maxGC);

        if (i == WIN_CANCEL)	return OFF;
        isseq -= 1;
    }


    if (oliglen > 16) return win_printf_OK("cannot handle oligos larger than 16b");

    if (isseq == 0)
    {
        i = do_select_file(file, sizeof(file), "ASCII_SEQ_FILE", "*.txt", "Ascii file containing DNA sequence", 1);
        if (i) return win_printf_OK("Pb selecting file");
        fp = fopen(file,"r");
        if (fp == NULL) return win_printf_OK("File %s does not exist !",backslash_to_slash(file));

        fseek(fp, 0, SEEK_END);
        size = ftell(fp);
        fseek(fp, 0, SEEK_SET);
        if (size > sizeof(seq)) size = sizeof(seq);
        i = fread(seq,sizeof(char),size,fp);
        if (i != size) win_printf_OK("Pb read %d char instead of %d in size\n in %s!",i,size,backslash_to_slash(file));
        fclose(fp);
        first = 0;
    }
    else if (isseq == 1)
    {
        len = strlen(inseq);
        for (i = 0; i < len; i++)
            seq[i] = inseq[i];
        seq[len] = 0;
    }
    len = strlen(seq);


    for (i = 0; i < len; i++)
    {
        if (seq[i] == 'A')       comp[i] = 'T';
        else if (seq[i] == 'C')  comp[i] = 'G';
        else if (seq[i] == 'G')  comp[i] = 'C';
        else if (seq[i] == 'T')  comp[i] = 'A';
        else { //win_printf("i = %d ch = %c",i,seq[i]);
            len = i;}
    }
    win_printf("seq len %d ",len);
    nmax = len - oliglen;
    comp[i] = 0;

    if ((opn = create_and_attach_one_plot(pr, 2*nmax, 2*nmax, 0)) == NULL)
        return win_printf_OK("cannot create plot !");
    dso = opn->dat[0];

    for (i = 0, noli = 0; i < nmax; i++)
    {
        for (j = 0, k = 0, contGC = 0; j < oliglen; j++)
        {
            b = seq[i+j];
            val = 0;
            if (b == 'A')       val = 0;
            else if (b == 'C')  val = 1;
            else if (b == 'G')  val = 2;
            else if (b == 'T')  val = 3;
            if (val > 0 && val < 3) contGC++;
            k <<=2;
            k |= val;
        }
        for (j = 0; j < noli; j++)
            if (k == oli[j]) break;
        if (k == oli[j] || contGC < minGC || contGC > maxGC) continue;
        if (j >= noli) 
        {
            oli[noli++] = k;
            for (j = 0; j < oliglen; j++)
            {
                oligo[j] = seq[i+j];
                if (oligo[j] == 'A')       oligo_comp_rev[oliglen-j-1] = 'T';
                else if (oligo[j] == 'C')  oligo_comp_rev[oliglen-j-1] = 'G';
                else if (oligo[j] == 'G')  oligo_comp_rev[oliglen-j-1] = 'C';
                else if (oligo[j] == 'T')  oligo_comp_rev[oliglen-j-1] = 'A';
            } 
            oligo_comp_rev[oliglen] = oligo[oliglen] = 0;
            for (j = 0, max = 0, hit = 0; j < nmax; j++)
            {
                matching_oligo(seq, len, oligo, oliglen, j, valm + j);
                if (valm[j] == 1.0) hit++;
                if (valm[j] < 1.0 && valm[j] > max)
                    max = valm[j];
            }
            for (j = 0, maxc = 0, hitc = 0; j < nmax; j++)
            {
                matching_oligo(comp, len, oligo_comp_rev, oliglen, j, valmc + j);
                if (valmc[j] == 1.0) hitc++;
                if (valmc[j] < 1.0 && valmc[j] > maxc)
                    maxc = valmc[j];
            }
            if ((max < maxallow)  && ((hit) >= minhit)) // && (maxc < maxallow)
            {
                if (dso == NULL) dso = create_and_attach_one_ds(opn,2*nmax, 2*nmax, 0);
                for (j = 0; j < nmax; j++)
                {
                    dso->yd[j] = valm[j];
                    dso->xd[j] = j;
                    dso->yd[dso->nx-j-1] = valmc[j];
                    dso->xd[dso->nx-j-1] = j;
                }
                for (j = nmax-1; j >= 0; j--)
                {
                    for (k = 0, max = 0; k < oliglen && j-k >= 0; k++)
                        max = (valm[j-k] > max) ? valm[j-k] : max;
                    dso->yd[j] = (max > dso->yd[j]) ? max : dso->yd[j];
                    for (k = 0, max = 0; k < oliglen && j-k >= 0; k++)
                        max = (valmc[j-k] > max) ? valmc[j-k] : max;
                    dso->yd[dso->nx-j-1] = (max > dso->yd[dso->nx-j-1]) ? max : dso->yd[dso->nx-j-1];
                }
                for (j = 0; j < nmax; j++) dso->yd[dso->nx-j-1] *= -1;
                set_ds_source(dso,"oligo %d len %d seq %s hit=%d,%d",k,oliglen,oligo,hit,hitc);
                dso = NULL;
            }
        }
    }
    set_plot_title(opn, "Sequence length %d\noligo %d number of oligo %d",len,oliglen,noli);
    set_plot_x_title(opn, "position");
    set_plot_y_title(opn, "type");
    refresh_plot(pr, UNCHANGED);
    return D_O_K;

}


int do_oligo_fake_data(void)
{
  register int i, j, k;
  static int len = 50, specific = 1, oliglen = 8, idum = 456789, exact = 0;
    char seq[4096];
    d_s *ds;
    pltreg *pr = NULL;
    static float sig = 1.5, bp_to_nm = 1.1;

    char seqfilename[2048];
    FILE *fp;


    if(updating_menu_state != 0)	return D_O_K;

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine generates histogramms of oligos binding\n"
                             " to an hairpin");
    }
    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
        return win_printf_OK("cannot find data");
    seq[0] = ' '; seq[1] = 0;
    i = win_scanf("This routine generates oligo versus position files to test reconstruction\n"
                  "for a DNA sequence. It start from a sequence that it completely covers\n"
		  "with oligo of the same size and then add a random noise to the oligo position\n"
                  "You can either generate a random sequence->%R\n"
                  "or indicate a specific one->%r, or load sequence from a file->%r\n"
                  "If you choose the random sequence specify it length %10d\n"
                  "Otherwise type your sequence %ls"
                  "Oligo length %8d\n"
		  "Base pair to nm concersion factor %5f\n"
                  "noise amplitude (bp) %12f\n"
		  "%b Add exact solution\n"
                  ,&specific,&len,seq,&oliglen,&bp_to_nm,&sig,&exact);
    if (i == WIN_CANCEL)	return OFF;

    if (bp_to_nm <= 0)
      return win_printf_OK("Impossible base pair to nm factor%g",bp_to_nm);
    if (specific == 1) len = strlen(seq);
    else if (specific == 2)
      {
	seqfilename[0] = 0;
	if (do_select_file(seqfilename, sizeof(seqfilename), "TXT-FILE", "*.txt", "File to load exact sequence", 1))
	  return win_printf_OK("Cannot select output file");
	fp = fopen (seqfilename,"r");
	if (fp == NULL)
	  return win_printf_OK("Impossible to open file %s",backslash_to_slash(seqfilename));
	for (i = 0, j = fgetc(fp); i < sizeof(seq) && j != EOF; j = fgetc(fp))
	  {
	    seq[i++] = (char)j;
	  }
	seq[i] = 0;
	fclose(fp);
	win_printf("Seq:\n%s",seq);
      }
    else
    {
        for (i = 0; i < len && i < sizeof(seq); i++)
        {
            j = rand()%4;
            if (j == 0) seq[i] = 'A';
            else if (j == 1) seq[i] = 'C';
            else if (j == 2) seq[i] = 'G';
            else if (j == 3) seq[i] = 'T';
            else win_printf("Impossible base type!%d",j);
        }
        seq[i] = 0;
        len = i;
    }
    ds = build_data_set(len-oliglen+1,len-oliglen+1);
    if (ds == NULL) return win_printf_OK("cannot allocate dataset");
    for (i = 0; i < ds->nx; i++)
      {
	ds->xd[i] = (sig * gasdev(&idum) + i)/bp_to_nm;
	ds->yd[i] = i;
      }
    sort_ds_along_x(ds);
    seqfilename[0] = 0;
    if (do_select_file(seqfilename, sizeof(seqfilename), "TXT-FILE", "*.txt", "File to save sequence", 0))
      return win_printf_OK("Cannot select output file");
    fp = fopen (seqfilename,"w");
    if (fp == NULL)  win_printf_OK("Cannot open file:\n%s",backslash_to_slash(seqfilename));
    fprintf(fp,"%s\nBasepair to nm factor %g",seq,bp_to_nm);
    fclose(fp);
    
    if (do_select_file(seqfilename, sizeof(seqfilename), "TXT-FILE", "*.txt", "File to save oligo and position", 0))
      return win_printf_OK("Cannot select output file");
    fp = fopen (seqfilename,"w");
    if (fp == NULL)  win_printf_OK("Cannot open file:\n%s",backslash_to_slash(seqfilename));
    for (i = 0; i < ds->nx; i++)
      {
	k = (int)ds->yd[i];
	if (exact)
	  {
	    fprintf(fp,"%g ",(float)k/bp_to_nm);
	  }
	for(j = 0; j < oliglen; j++)
	  fprintf(fp,"%c",seq[k+j]);
	fprintf(fp,"\t%g\n",ds->xd[i]);
      }
    fclose(fp);  
    return 0;
}

int do_compute_covering(void)
{
    register int i, j;
    O_p  *opn = NULL, *op = NULL;
    int  oliglen = 8, k;
    char oligo[4096];
    d_s *ds, *ds0, *dsA, *dsC, *dsG, *dsT;
    pltreg *pr = NULL;


    if(updating_menu_state != 0)	return D_O_K;

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine generates histogramms of oligos binding\n"
                             " to an hairpin");
    }
    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
        return win_printf_OK("cannot find data");

    for(i = 0, ds0 = op->dat[0]; i < op->n_dat; i++)
    {
        ds = op->dat[i];
        if (ds->nx != ds0->nx)
            return win_printf_OK("data set %d has %d points insttead of %d in 0",i,ds->nx,ds0->nx);
        if (ds->source == NULL)
            return win_printf_OK("data set %d has no source",i);
        if (sscanf(ds->source,"oligo %d len %d seq %s",&k,&oliglen,oligo) != 3)
            return win_printf_OK("data set %d has wrong  source :\n%s",i,ds->source);	      
    }

    if ((opn = create_and_attach_one_plot(pr, ds0->nx+oliglen, ds0->nx+oliglen, 0)) == NULL)
        return win_printf_OK("cannot create plot !");

    dsA = opn->dat[0];
    dsC = create_and_attach_one_ds(opn,ds0->nx+oliglen, ds0->nx+oliglen, 0);
    dsG = create_and_attach_one_ds(opn,ds0->nx+oliglen, ds0->nx+oliglen, 0);
    dsT = create_and_attach_one_ds(opn,ds0->nx+oliglen, ds0->nx+oliglen, 0);


    for(i = 0, ds0 = op->dat[0]; i < op->n_dat; i++)
    {
        ds = op->dat[i];
        if (sscanf(ds->source,"oligo %d len %d seq %s",&k,&oliglen,oligo) != 3)
            return win_printf_OK("data set %d has wrong  source :\n%s",i,ds->source);	      
        for (j = 0; j < ds->nx; j++)
        {
            if (ds->yd[j] == 1)
            {
                for (k = 0; k < oliglen; k++)
                {
                    if (oligo[k] == 'A') dsA->yd[j+k] = dsA->yd[j+k] + 1;
                    else if (oligo[k] == 'C') dsC->yd[j+k] = dsC->yd[j+k] + 1;
                    else if (oligo[k] == 'G') dsG->yd[j+k] = dsG->yd[j+k] + 1;
                    else if (oligo[k] == 'T') dsT->yd[j+k] = dsT->yd[j+k] + 1;
                }
            }
        }
    }
    for (j = 0; j < dsA->nx; j++)
        dsA->xd[j] = dsC->xd[j] = dsG->xd[j] = dsT->xd[j] = j;
    set_plot_x_title(opn, "position");
    set_plot_y_title(opn, "type");
    refresh_plot(pr, UNCHANGED);
    return D_O_K;

}

int do_oligo_plots(void)
{
    register int i, j;
    O_p  *opn = NULL, *op = NULL;
    static int len = 80, specific = 1, oliglen = 8, pos = 2;
    char seq[4096], comp[4096];
    d_s *ds, *dso;
    pltreg *pr = NULL;
    float sig2, tmp;
    static float sig = 1.5;


    if(updating_menu_state != 0)	return D_O_K;

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine generates histogramms of oligos binding\n"
                             " to an hairpin");
    }
    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
        return win_printf_OK("cannot find data");
    seq[0] = ' '; seq[1] = 0;
    i = win_scanf("This routine generates histogramms of oligos hybridization\n"
                  "to a DNA sequence forming an hairpin.\n"
                  "You can either generate a random sequence %R\n"
                  "or indicate a specific one %r\n"
                  "If you choose the random sequence specify it length %10d\n"
                  "Otherwise type your sequence %ls"
                  "Oligo length %8d base position %8d\n"
                  "sigma %12f\n"
                  ,&specific,&len,seq,&oliglen,&pos,&sig);
    if (i == WIN_CANCEL)	return OFF;

    sig2 = 2 * sig * sig;

    if (specific) len = strlen(seq);
    else
    {
        for (i = 0; i < len && i < sizeof(seq); i++)
        {
            j = rand()%4;
            if (j == 0) seq[i] = 'A';
            else if (j == 1) seq[i] = 'C';
            else if (j == 2) seq[i] = 'G';
            else if (j == 3) seq[i] = 'T';
            else win_printf("Impossible base type!%d",j);
        }
        seq[i] = 0;
        len = i;
    }
    for (i = 0; i < len; i++)
    {
        if (seq[i] == 'A')       comp[i] = 'T';
        else if (seq[i] == 'C')  comp[i] = 'G';
        else if (seq[i] == 'G')  comp[i] = 'C';
        else if (seq[i] == 'T')  comp[i] = 'A';
    }
    comp[i] = 0;


    if ((opn = create_and_attach_one_plot(pr, 2*len, 2*len, 0)) == NULL)
        return win_printf_OK("cannot create plot !");

    dso = opn->dat[0];

    for (j = 0; j < len; j++)
    {
        if (seq[j] == 'A')       {dso->yd[j] = 1; dso->yd[2*len-j-1] =  4;}
        else if (seq[j] == 'C')  {dso->yd[j] = 2; dso->yd[2*len-j-1] =  3;}
        else if (seq[j] == 'G')  {dso->yd[j] = 3; dso->yd[2*len-j-1] =  2;}
        else if (seq[j] == 'T')  {dso->yd[j] = 4; dso->yd[2*len-j-1] =  1;}
        dso->xd[j] = j;
        dso->xd[2*len-j-1] = 2*len-j-1;
    }

    /* now we must do some house keeping */
    set_ds_source(dso,"Hairpin len %d seq %s",len,seq);
    set_plot_title(opn, "\\stack{{Hairpin len %d seq}{\\pt4 %s}}",len,seq);
    set_plot_x_title(opn, "position");
    set_plot_y_title(opn, "type");
    opn->filename = strdup("haipin-seq.gr");


    if ((op = create_and_attach_one_plot(pr, len, len, 0)) == NULL)
        return win_printf_OK("cannot create plot !");

    ds = op->dat[0];
    set_ds_source(ds,"oligo NNANNNNN");
    for (j = 0; j < len; j++)
    {
        ds->xd[j] = j;
        if (j < len - oliglen) 
        {
            if (seq[j+pos] == 'A')                ds->yd[j+oliglen] = 1;
            if (comp[j+oliglen-pos-1] == 'A')     ds->yd[j+oliglen] = 1;
        }
    }
    for (j = 0; j < len; j++)  	    ds->xd[j] = 0; 
    for (j = 0; j < len; j++)
    {
        if (ds->yd[j] > 0.5)
        {
            for (i = -4; i < 5; i++)
            {
                tmp = i * i;
                tmp = exp(-tmp/sig2);
                if ((j + i) >= 0 && (j+i) < len)
                    ds->xd[j+i] += tmp;
            }
        }
    }
    for (j = 0; j < len; j++)  	    ds->yd[j] = ds->xd[j]; 
    for (j = 0; j < len; j++)  	    ds->xd[j] = j; 


    ds = create_and_attach_one_ds(op,len, len, 0);
    set_ds_source(ds,"oligo NNCNNNNN");
    for (j = 0; j < len; j++)
    {
        ds->xd[j] = j;
        if (j < len - oliglen) 
        {
            if (seq[j+pos] == 'C')                ds->yd[j+oliglen] = 1;
            if (comp[j+oliglen-pos-1] == 'C')     ds->yd[j+oliglen] = 1;
        }
    }
    for (j = 0; j < len; j++)  	    ds->xd[j] = 0; 
    for (j = 0; j < len; j++)
    {
        if (ds->yd[j] > 0.5)
        {
            for (i = -4; i < 5; i++)
            {
                tmp = i * i;
                tmp = exp(-tmp/sig2);
                if ((j + i) >= 0 && (j+i) < len)
                    ds->xd[j+i] += tmp;
            }
        }
    }
    for (j = 0; j < len; j++)  	    ds->yd[j] = ds->xd[j]; 
    for (j = 0; j < len; j++)  	    ds->xd[j] = j; 

    ds = create_and_attach_one_ds(op,len, len, 0);
    set_ds_source(ds,"oligo NNGNNNNN");
    for (j = 0; j < len; j++)
    {
        ds->xd[j] = j;
        if (j < len - oliglen) 
        {
            if (seq[j+pos] == 'G')                ds->yd[j+oliglen] = 1;
            if (comp[j+oliglen-pos-1] == 'G')     ds->yd[j+oliglen] = 1;
        }
    }
    for (j = 0; j < len; j++)  	    ds->xd[j] = 0; 
    for (j = 0; j < len; j++)
    {
        if (ds->yd[j] > 0.5)
        {
            for (i = -4; i < 5; i++)
            {
                tmp = i * i;
                tmp = exp(-tmp/sig2);
                if ((j + i) >= 0 && (j+i) < len)
                    ds->xd[j+i] += tmp;
            }
        }
    }
    for (j = 0; j < len; j++)  	    ds->yd[j] = ds->xd[j]; 
    for (j = 0; j < len; j++)  	    ds->xd[j] = j; 

    ds = create_and_attach_one_ds(op,len, len, 0);
    set_ds_source(ds,"oligo NNTNNNNN");
    for (j = 0; j < len; j++)
    {
        ds->xd[j] = j;
        if (j < len - oliglen) 
        {
            if (seq[j+pos] == 'T')                ds->yd[j+oliglen] = 1;
            if (comp[j+oliglen-pos-1] == 'T')     ds->yd[j+oliglen] = 1;
        }
    }
    for (j = 0; j < len; j++)  	    ds->xd[j] = 0; 
    for (j = 0; j < len; j++)
    {
        if (ds->yd[j] > 0.5)
        {
            for (i = -4; i < 5; i++)
            {
                tmp = i * i;
                tmp = exp(-tmp/sig2);
                if ((j + i) >= 0 && (j+i) < len)
                    ds->xd[j+i] += tmp;
            }
        }
    }
    for (j = 0; j < len; j++)  	    ds->yd[j] = ds->xd[j]; 
    for (j = 0; j < len; j++)  	    ds->xd[j] = j; 



    if ((op = create_and_attach_one_plot(pr, len, len, 0)) == NULL)
        return win_printf_OK("cannot create plot !");

    ds = op->dat[0];
    set_ds_source(ds,"oligo NNAANNNN");
    for (j = 0; j < len; j++)
    {
        ds->xd[j] = j;
        if (j < len - oliglen) 
        {
            if (seq[j+pos] == 'A' && seq[j+pos+1] == 'A')                
                ds->yd[j+oliglen] = 1;
            if (comp[j+oliglen-pos-1] == 'A' && comp[j+oliglen-pos-2] == 'A')     
                ds->yd[j+oliglen] = 1;
        }
    }
    /*
       for (j = 0; j < len; j++)  	    ds->xd[j] = 0; 
       for (j = 0; j < len; j++)
       {
       if (ds->yd[j] > 0.5)
       {
       for (i = -4; i < 5; i++)
       {
       tmp = i * i;
       tmp = exp(-tmp/sig2);
       if ((j + i) >= 0 && (j+i) < len)
       ds->xd[j+i] += tmp;
       }
       }
       }
       for (j = 0; j < len; j++)  	    ds->yd[j] = ds->xd[j]; 
       for (j = 0; j < len; j++)  	    ds->xd[j] = j; 
       */
    ds = create_and_attach_one_ds(op,len, len, 0);
    set_ds_source(ds,"oligo NNACNNNN");
    for (j = 0; j < len; j++)
    {
        ds->xd[j] = j;
        if (j < len - oliglen) 
        {
            if (seq[j+pos] == 'A' && seq[j+pos+1] == 'C')                
                ds->yd[j+oliglen] = 1;
            if (comp[j+oliglen-pos-1] == 'A' && comp[j+oliglen-pos-2] == 'C')     
                ds->yd[j+oliglen] = 1;
        }
    }
    /*
       for (j = 0; j < len; j++)  	    ds->xd[j] = 0; 
       for (j = 0; j < len; j++)
       {
       if (ds->yd[j] > 0.5)
       {
       for (i = -4; i < 5; i++)
       {
       tmp = i * i;
       tmp = exp(-tmp/sig2);
       if ((j + i) >= 0 && (j+i) < len)
       ds->xd[j+i] += tmp;
       }
       }
       }
       for (j = 0; j < len; j++)  	    ds->yd[j] = ds->xd[j]; 
       for (j = 0; j < len; j++)  	    ds->xd[j] = j; 
       */
    ds = create_and_attach_one_ds(op,len, len, 0);
    set_ds_source(ds,"oligo NNAGNNNN");
    for (j = 0; j < len; j++)
    {
        ds->xd[j] = j;
        if (j < len - oliglen) 
        {
            if (seq[j+pos] == 'A' && seq[j+pos+1] == 'G')                
                ds->yd[j+oliglen] = 1;
            if (comp[j+oliglen-pos-1] == 'A' && comp[j+oliglen-pos-2] == 'G')     
                ds->yd[j+oliglen] = 1;
        }
    }
    /*
       for (j = 0; j < len; j++)  	    ds->xd[j] = 0; 
       for (j = 0; j < len; j++)
       {
       if (ds->yd[j] > 0.5)
       {
       for (i = -4; i < 5; i++)
       {
       tmp = i * i;
       tmp = exp(-tmp/sig2);
       if ((j + i) >= 0 && (j+i) < len)
       ds->xd[j+i] += tmp;
       }
       }
       }
       for (j = 0; j < len; j++)  	    ds->yd[j] = ds->xd[j]; 
       for (j = 0; j < len; j++)  	    ds->xd[j] = j; 
       */
    ds = create_and_attach_one_ds(op,len, len, 0);
    set_ds_source(ds,"oligo NNATNNNN");
    for (j = 0; j < len; j++)
    {
        ds->xd[j] = j;
        if (j < len - oliglen) 
        {
            if (seq[j+pos] == 'A' && seq[j+pos+1] == 'T')                
                ds->yd[j+oliglen] = 1;
            if (comp[j+oliglen-pos-1] == 'A' && comp[j+oliglen-pos-2] == 'T')     
                ds->yd[j+oliglen] = 1;
        }
    }
    /*
       for (j = 0; j < len; j++)  	    ds->xd[j] = 0; 
       for (j = 0; j < len; j++)
       {
       if (ds->yd[j] > 0.5)
       {
       for (i = -4; i < 5; i++)
       {
       tmp = i * i;
       tmp = exp(-tmp/sig2);
       if ((j + i) >= 0 && (j+i) < len)
       ds->xd[j+i] += tmp;
       }
       }
       }
       for (j = 0; j < len; j++)  	    ds->yd[j] = ds->xd[j]; 
       for (j = 0; j < len; j++)  	    ds->xd[j] = j; 
       */


    ds = create_and_attach_one_ds(op,len, len, 0);
    set_ds_source(ds,"oligo NNCANNNN");
    for (j = 0; j < len; j++)
    {
        ds->xd[j] = j;
        if (j < len - oliglen) 
        {
            if (seq[j+pos] == 'C' && seq[j+pos+1] == 'A')                
                ds->yd[j+oliglen] = 1;
            if (comp[j+oliglen-pos-1] == 'C' && comp[j+oliglen-pos-2] == 'A')     
                ds->yd[j+oliglen] = 1;
        }
    }
    /*
       for (j = 0; j < len; j++)  	    ds->xd[j] = 0; 
       for (j = 0; j < len; j++)
       {
       if (ds->yd[j] > 0.5)
       {
       for (i = -4; i < 5; i++)
       {
       tmp = i * i;
       tmp = exp(-tmp/sig2);
       if ((j + i) >= 0 && (j+i) < len)
       ds->xd[j+i] += tmp;
       }
       }
       }
       for (j = 0; j < len; j++)  	    ds->yd[j] = ds->xd[j]; 
       for (j = 0; j < len; j++)  	    ds->xd[j] = j; 
       */
    ds = create_and_attach_one_ds(op,len, len, 0);
    set_ds_source(ds,"oligo NNCCNNNN");
    for (j = 0; j < len; j++)
    {
        ds->xd[j] = j;
        if (j < len - oliglen) 
        {
            if (seq[j+pos] == 'C' && seq[j+pos+1] == 'C')                
                ds->yd[j+oliglen] = 1;
            if (comp[j+oliglen-pos-1] == 'C' && comp[j+oliglen-pos-2] == 'C')     
                ds->yd[j+oliglen] = 1;
        }
    }
    /*
       for (j = 0; j < len; j++)  	    ds->xd[j] = 0; 
       for (j = 0; j < len; j++)
       {
       if (ds->yd[j] > 0.5)
       {
       for (i = -4; i < 5; i++)
       {
       tmp = i * i;
       tmp = exp(-tmp/sig2);
       if ((j + i) >= 0 && (j+i) < len)
       ds->xd[j+i] += tmp;
       }
       }
       }
       for (j = 0; j < len; j++)  	    ds->yd[j] = ds->xd[j]; 
       for (j = 0; j < len; j++)  	    ds->xd[j] = j; 
       */

    ds = create_and_attach_one_ds(op,len, len, 0);
    set_ds_source(ds,"oligo NNCGNNNN");
    for (j = 0; j < len; j++)
    {
        ds->xd[j] = j;
        if (j < len - oliglen) 
        {
            if (seq[j+pos] == 'C' && seq[j+pos+1] == 'G')                
                ds->yd[j+oliglen] = 1;
            if (comp[j+oliglen-pos-1] == 'C' && comp[j+oliglen-pos-2] == 'G')     
                ds->yd[j+oliglen] = 1;
        }
    }
    /*
       for (j = 0; j < len; j++)  	    ds->xd[j] = 0; 
       for (j = 0; j < len; j++)
       {
       if (ds->yd[j] > 0.5)
       {
       for (i = -4; i < 5; i++)
       {
       tmp = i * i;
       tmp = exp(-tmp/sig2);
       if ((j + i) >= 0 && (j+i) < len)
       ds->xd[j+i] += tmp;
       }
       }
       }
       for (j = 0; j < len; j++)  	    ds->yd[j] = ds->xd[j]; 
       for (j = 0; j < len; j++)  	    ds->xd[j] = j; 
       */
    ds = create_and_attach_one_ds(op,len, len, 0);
    set_ds_source(ds,"oligo NNCTNNNN");
    for (j = 0; j < len; j++)
    {
        ds->xd[j] = j;
        if (j < len - oliglen) 
        {
            if (seq[j+pos] == 'C' && seq[j+pos+1] == 'T')                
                ds->yd[j+oliglen] = 1;
            if (comp[j+oliglen-pos-1] == 'C' && comp[j+oliglen-pos-2] == 'T')     
                ds->yd[j+oliglen] = 1;
        }
    }
    /*
       for (j = 0; j < len; j++)  	    ds->xd[j] = 0; 
       for (j = 0; j < len; j++)
       {
       if (ds->yd[j] > 0.5)
       {
       for (i = -4; i < 5; i++)
       {
       tmp = i * i;
       tmp = exp(-tmp/sig2);
       if ((j + i) >= 0 && (j+i) < len)
       ds->xd[j+i] += tmp;
       }
       }
       }
       for (j = 0; j < len; j++)  	    ds->yd[j] = ds->xd[j]; 
       for (j = 0; j < len; j++)  	    ds->xd[j] = j; 
       */

    ds = create_and_attach_one_ds(op,len, len, 0);
    set_ds_source(ds,"oligo NNGANNNN");
    for (j = 0; j < len; j++)
    {
        ds->xd[j] = j;
        if (j < len - oliglen) 
        {
            if (seq[j+pos] == 'G' && seq[j+pos+1] == 'A')                
                ds->yd[j+oliglen] = 1;
            if (comp[j+oliglen-pos-1] == 'G' && comp[j+oliglen-pos-2] == 'A')     
                ds->yd[j+oliglen] = 1;
        }
    }
    /*
       for (j = 0; j < len; j++)  	    ds->xd[j] = 0; 
       for (j = 0; j < len; j++)
       {
       if (ds->yd[j] > 0.5)
       {
       for (i = -4; i < 5; i++)
       {
       tmp = i * i;
       tmp = exp(-tmp/sig2);
       if ((j + i) >= 0 && (j+i) < len)
       ds->xd[j+i] += tmp;
       }
       }
       }
       for (j = 0; j < len; j++)  	    ds->yd[j] = ds->xd[j]; 
       for (j = 0; j < len; j++)  	    ds->xd[j] = j; 
       */
    ds = create_and_attach_one_ds(op,len, len, 0);
    set_ds_source(ds,"oligo NNGCNNNN");
    for (j = 0; j < len; j++)
    {
        ds->xd[j] = j;
        if (j < len - oliglen) 
        {
            if (seq[j+pos] == 'G' && seq[j+pos+1] == 'C')                
                ds->yd[j+oliglen] = 1;
            if (comp[j+oliglen-pos-1] == 'G' && comp[j+oliglen-pos-2] == 'C')     
                ds->yd[j+oliglen] = 1;
        }
    }
    /*
       for (j = 0; j < len; j++)  	    ds->xd[j] = 0; 
       for (j = 0; j < len; j++)
       {
       if (ds->yd[j] > 0.5)
       {
       for (i = -4; i < 5; i++)
       {
       tmp = i * i;
       tmp = exp(-tmp/sig2);
       if ((j + i) >= 0 && (j+i) < len)
       ds->xd[j+i] += tmp;
       }
       }
       }
       for (j = 0; j < len; j++)  	    ds->yd[j] = ds->xd[j]; 
       for (j = 0; j < len; j++)  	    ds->xd[j] = j; 
       */
    ds = create_and_attach_one_ds(op,len, len, 0);
    set_ds_source(ds,"oligo NNGGNNNN");
    for (j = 0; j < len; j++)
    {
        ds->xd[j] = j;
        if (j < len - oliglen) 
        {
            if (seq[j+pos] == 'G' && seq[j+pos+1] == 'G')                
                ds->yd[j+oliglen] = 1;
            if (comp[j+oliglen-pos-1] == 'G' && comp[j+oliglen-pos-2] == 'G')     
                ds->yd[j+oliglen] = 1;
        }
    }
    /*
       for (j = 0; j < len; j++)  	    ds->xd[j] = 0; 
       for (j = 0; j < len; j++)
       {
       if (ds->yd[j] > 0.5)
       {
       for (i = -4; i < 5; i++)
       {
       tmp = i * i;
       tmp = exp(-tmp/sig2);
       if ((j + i) >= 0 && (j+i) < len)
       ds->xd[j+i] += tmp;
       }
       }
       }
       for (j = 0; j < len; j++)  	    ds->yd[j] = ds->xd[j]; 
       for (j = 0; j < len; j++)  	    ds->xd[j] = j; 
       */
    ds = create_and_attach_one_ds(op,len, len, 0);
    set_ds_source(ds,"oligo NNGTNNNN");
    for (j = 0; j < len; j++)
    {
        ds->xd[j] = j;
        if (j < len - oliglen) 
        {
            if (seq[j+pos] == 'G' && seq[j+pos+1] == 'T')                
                ds->yd[j+oliglen] = 1;
            if (comp[j+oliglen-pos-1] == 'G' && comp[j+oliglen-pos-2] == 'T')     
                ds->yd[j+oliglen] = 1;
        }
    }
    /*
       for (j = 0; j < len; j++)  	    ds->xd[j] = 0; 
       for (j = 0; j < len; j++)
       {
       if (ds->yd[j] > 0.5)
       {
       for (i = -4; i < 5; i++)
       {
       tmp = i * i;
       tmp = exp(-tmp/sig2);
       if ((j + i) >= 0 && (j+i) < len)
       ds->xd[j+i] += tmp;
       }
       }
       }
       for (j = 0; j < len; j++)  	    ds->yd[j] = ds->xd[j]; 
       for (j = 0; j < len; j++)  	    ds->xd[j] = j; 
       */


    ds = create_and_attach_one_ds(op,len, len, 0);
    set_ds_source(ds,"oligo NNTANNNN");
    for (j = 0; j < len; j++)
    {
        ds->xd[j] = j;
        if (j < len - oliglen) 
        {
            if (seq[j+pos] == 'T' && seq[j+pos+1] == 'A')                
                ds->yd[j+oliglen] = 1;
            if (comp[j+oliglen-pos-1] == 'T' && comp[j+oliglen-pos-2] == 'A')     
                ds->yd[j+oliglen] = 1;
        }
    }
    /*
       for (j = 0; j < len; j++)  	    ds->xd[j] = 0; 
       for (j = 0; j < len; j++)
       {
       if (ds->yd[j] > 0.5)
       {
       for (i = -4; i < 5; i++)
       {
       tmp = i * i;
       tmp = exp(-tmp/sig2);
       if ((j + i) >= 0 && (j+i) < len)
       ds->xd[j+i] += tmp;
       }
       }
       }
       for (j = 0; j < len; j++)  	    ds->yd[j] = ds->xd[j]; 
       for (j = 0; j < len; j++)  	    ds->xd[j] = j; 
       */
    ds = create_and_attach_one_ds(op,len, len, 0);
    set_ds_source(ds,"oligo NNTCNNNN");
    for (j = 0; j < len; j++)
    {
        ds->xd[j] = j;
        if (j < len - oliglen) 
        {
            if (seq[j+pos] == 'T' && seq[j+pos+1] == 'C')                
                ds->yd[j+oliglen] = 1;
            if (comp[j+oliglen-pos-1] == 'T' && comp[j+oliglen-pos-2] == 'C')     
                ds->yd[j+oliglen] = 1;
        }
    }
    /*
       for (j = 0; j < len; j++)  	    ds->xd[j] = 0; 
       for (j = 0; j < len; j++)
       {
       if (ds->yd[j] > 0.5)
       {
       for (i = -4; i < 5; i++)
       {
       tmp = i * i;
       tmp = exp(-tmp/sig2);
       if ((j + i) >= 0 && (j+i) < len)
       ds->xd[j+i] += tmp;
       }
       }
       }
       for (j = 0; j < len; j++)  	    ds->yd[j] = ds->xd[j]; 
       for (j = 0; j < len; j++)  	    ds->xd[j] = j; 
       */
    ds = create_and_attach_one_ds(op,len, len, 0);
    set_ds_source(ds,"oligo NNTGNNNN");
    for (j = 0; j < len; j++)
    {
        ds->xd[j] = j;
        if (j < len - oliglen) 
        {
            if (seq[j+pos] == 'T' && seq[j+pos+1] == 'G')                
                ds->yd[j+oliglen] = 1;
            if (comp[j+oliglen-pos-1] == 'T' && comp[j+oliglen-pos-2] == 'G')     
                ds->yd[j+oliglen] = 1;
        }
    }
    /*
       for (j = 0; j < len; j++)  	    ds->xd[j] = 0; 
       for (j = 0; j < len; j++)
       {
       if (ds->yd[j] > 0.5)
       {
       for (i = -4; i < 5; i++)
       {
       tmp = i * i;
       tmp = exp(-tmp/sig2);
       if ((j + i) >= 0 && (j+i) < len)
       ds->xd[j+i] += tmp;
       }
       }
       }
       for (j = 0; j < len; j++)  	    ds->yd[j] = ds->xd[j]; 
       for (j = 0; j < len; j++)  	    ds->xd[j] = j; 
       */
    ds = create_and_attach_one_ds(op,len, len, 0);
    set_ds_source(ds,"oligo NNTTNNNN");
    for (j = 0; j < len; j++)
    {
        ds->xd[j] = j;
        if (j < len - oliglen) 
        {
            if (seq[j+pos] == 'T' && seq[j+pos+1] == 'T')                
                ds->yd[j+oliglen] = 1;
            if (comp[j+oliglen-pos-1] == 'T' && comp[j+oliglen-pos-2] == 'T')     
                ds->yd[j+oliglen] = 1;
        }
    }
    /*
       for (j = 0; j < len; j++)  	    ds->xd[j] = 0; 
       for (j = 0; j < len; j++)
       {
       if (ds->yd[j] > 0.5)
       {
       for (i = -4; i < 5; i++)
       {
       tmp = i * i;
       tmp = exp(-tmp/sig2);
       if ((j + i) >= 0 && (j+i) < len)
       ds->xd[j+i] += tmp;
       }
       }
       }
       for (j = 0; j < len; j++)  	    ds->yd[j] = ds->xd[j]; 
       for (j = 0; j < len; j++)  	    ds->xd[j] = j; 
       */


    /* refisplay the entire plot */
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}


int gaussian_convolution(d_s *ds, float sig)
{
    register int i, j;
    int len;
    float tmp;

    if (ds == NULL) return 1;
    len = ds->nx;
    sig = 2 * sig * sig;
    for (j = 0; j < len; j++)  	    ds->xd[j] = 0; 
    for (j = 0; j < len; j++)
    {
        if (ds->yd[j] > 0)
        {
            for (i = -4; i < 5; i++)
            {
                tmp = i * i;
                tmp = exp(-tmp/sig);
                if ((j + i) >= 0 && (j+i) < len)
                    ds->xd[j+i] += tmp*ds->yd[j];
            }
        }
    }
    for (j = 0; j < len; j++)  	    ds->yd[j] = ds->xd[j]; 
    for (j = 0; j < len; j++)  	    ds->xd[j] = j; 
    return 0;
}

int hybridize_oligo(d_s *ds, char *seq, char *oligo)
{
    register int j, k;
    int len = 0, leno = 0, match;

    if (ds == NULL || seq == NULL || oligo == NULL) return 1;
    len = strlen(seq);
    leno = strlen(oligo);

    set_ds_source(ds,"oligo %s",oligo);
    for (j = 0; j < len && j < ds->nx; j++) ds->yd[j] = 0;
    for (j = 0; j < len && j < ds->nx; j++)
    {
        ds->xd[j] = j;
        if (j < len - leno) 
        {
            for (k = 0, match = 1; k < leno && match == 1; k++)
            {
                if (oligo[k] == 'N') match = 1;
                else if ((oligo[k] == 'A') && (seq[j+k] == 'T')) match = 1;
                else if ((oligo[k] == 'T') && (seq[j+k] == 'A')) match = 1;
                else if ((oligo[k] == 'C') && (seq[j+k] == 'G')) match = 1;
                else if ((oligo[k] == 'G') && (seq[j+k] == 'C')) match = 1;
                else 
                {
                    match = 0;
                    //win_printf("j %d k %d match set to 0-> %d s->%c o->%c\n",j,k,match,seq[j+k] ,oligo[k]);
                }
                //if (oligo[k] != 'N' && match == 1) 
                //win_printf("j %d k %d match %d s->%c o->%c\n",j,k,match,seq[j+k] ,oligo[k]);
            }
            if (match) ds->yd[j+leno] += 1;
        }
    }
    for (j = 0; j < len && j < ds->nx; j++)
    {
        ds->xd[j] = j;
        if (j < len - leno) 
        {
            for (k = 0, match = 1; k < leno && match == 1; k++)
            {
                if (oligo[leno-1-k] == 'N') match = 1;
                else if ((oligo[leno-1-k] == 'A') && (seq[j+k] == 'A')) match = 1;
                else if ((oligo[leno-1-k] == 'T') && (seq[j+k] == 'T')) match = 1;
                else if ((oligo[leno-1-k] == 'C') && (seq[j+k] == 'C')) match = 1;
                else if ((oligo[leno-1-k] == 'G') && (seq[j+k] == 'G')) match = 1;
                else 
                {
                    match = 0;
                    //win_printf("j %d k %d match set to 0-> %d s->%c o->%c\n",j,k,match,seq[j+k] ,oligo[k]);
                }
                //if (oligo[k] != 'N' && match == 1) 
                //win_printf("j %d k %d match %d s->%c o->%c\n",j,k,match,seq[j+k] ,oligo[k]);
            }
            if (match) ds->yd[j+leno] += 1;
        }
    }
    return 0;
}


int hybridize_oligo_debug(d_s *ds, char *seq, char *oligo)
{
    register int  j, k;
    int len = 0, leno = 0, match;
    char mseq[128], coligo[128];


    if (ds == NULL || seq == NULL || oligo == NULL) return 1;
    len = strlen(seq);
    leno = strlen(oligo);
    set_ds_source(ds,"oligo %s",oligo);
    for (j = 0; j < len && j < ds->nx; j++) ds->yd[j] = 0;
    for (j = 0; j < len && j < ds->nx; j++)
    {
        ds->xd[j] = j;
        if (j < len - leno) 
        {
            for (k = 0, match = 1; k < leno && match == 1; k++)
            {
                if (oligo[k] == 'N') match = 1;
                else if ((oligo[k] == 'A') && (seq[j+k] == 'T')) match = 1;
                else if ((oligo[k] == 'T') && (seq[j+k] == 'A')) match = 1;
                else if ((oligo[k] == 'C') && (seq[j+k] == 'G')) match = 1;
                else if ((oligo[k] == 'G') && (seq[j+k] == 'C')) match = 1;
                else match = 0;
            }
            if (match) 
            {
                ds->yd[j+leno] += .75;
                for (k = 0; k < leno; k++) mseq[k] = seq[j+k]; 
                mseq[k] = 0;
                win_printf("forward match at j %d\ns->%s\no->%s\n",j,mseq ,oligo);
            }
        }
    }
    for (j = 0; j < len && j < ds->nx; j++)
    {
        ds->xd[j] = j;
        if (j < len - leno) 
        {
            for (k = 0, match = 1; k < leno && match == 1; k++)
            {
                if (oligo[leno-1-k] == 'N') match = 1;
                else if ((oligo[leno-1-k] == 'A') && (seq[j+k] == 'A')) match = 1;
                else if ((oligo[leno-1-k] == 'T') && (seq[j+k] == 'T')) match = 1;
                else if ((oligo[leno-1-k] == 'C') && (seq[j+k] == 'C')) match = 1;
                else if ((oligo[leno-1-k] == 'G') && (seq[j+k] == 'G')) match = 1;
                else  match = 0;
            }
            if (match) 
            {
                ds->yd[j+leno] += 1.25;
                for (k = 0; k < leno; k++) 
                {
                    coligo[k] = oligo[leno-1-k];
                    if (seq[j+k] == 'A')      mseq[k] = 'T';
                    else if (seq[j+k] == 'T') mseq[k] = 'A';
                    else if (seq[j+k] == 'C') mseq[k] = 'G';
                    else if (seq[j+k] == 'G') mseq[k] = 'C';
                }
                mseq[k] = 0;
                coligo[k] = 0;
                win_printf("back match at j %d\ns->%s\no->%s\n",j,mseq ,coligo);
            }
        }
    }
    return 0;
}

int hybridize_oligo_convol(d_s *ds, char *seq, char *oligo, float sig)
{
    int ret;
    ret = hybridize_oligo(ds, seq, oligo);
    if (ret) return ret;
    ret = gaussian_convolution(ds, sig);
    return ret;
} 

int do_oligo_plots_debug(void)
{
    register int i;
    O_p  *op = NULL;
    static int len = 80;
    char seq[4096], comp[4096], oligo[128];
    d_s *ds;
    pltreg *pr = NULL;


    if(updating_menu_state != 0)	return D_O_K;

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine generates histogramms of oligos binding\n"
                             " to an hairpin");
    }
    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
        return win_printf_OK("cannot find data");
    seq[0] = ' '; seq[1] = 0;
    strcpy(seq,"CTGACAGGGACCCTCTTGTATAGCAGCAGTTG");

    i = win_scanf("This routine generates histogramms of oligos hybridization\n"
                  "to a DNA sequence forming an hairpin.\n"
                  "Type your hairpin sequence %ls"
                  "Type your oligo sequence (NNANNNN)%ls"
                  ,seq,oligo);
    if (i == WIN_CANCEL)	return OFF;

    len = strlen(seq);
    for (i = 0; i < len; i++)
    {
        if (seq[i] == 'A')       comp[i] = 'T';
        else if (seq[i] == 'C')  comp[i] = 'G';
        else if (seq[i] == 'G')  comp[i] = 'C';
        else if (seq[i] == 'T')  comp[i] = 'A';
    }
    comp[i] = 0;

    if ((op = create_and_attach_one_plot(pr, len, len, 0)) == NULL)
        return win_printf_OK("cannot create plot !");

    ds = op->dat[0];
    set_plot_title(op, "\\stack{{Hairpin len %d seq}{\\pt6 %s}{\\pt6 %s}{\\pt6 %s}}"
                   ,len,seq,comp,oligo);
    set_plot_x_title(op, "position");
    set_plot_y_title(op, "type");
    op->filename = strdup("haipin-seq.gr");
    hybridize_oligo_debug(ds, seq, oligo);
    /* refisplay the entire plot */
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}

int do_oligo_plots_2(void)
{
    register int i, j;
    O_p  *opn = NULL, *op = NULL;
    static int len = 80, specific = 1, oliglen = 8, pos = 2;
    char seq[4096], comp[4096];
    d_s *ds, *dso;
    pltreg *pr = NULL;
    static float sig = 1.5;


    if(updating_menu_state != 0)	return D_O_K;

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine generates histogramms of oligos binding\n"
                             " to an hairpin");
    }
    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
        return win_printf_OK("cannot find data");
    seq[0] = ' '; seq[1] = 0;
    i = win_scanf("This routine generates histogramms of oligos hybridization\n"
                  "to a DNA sequence forming an hairpin.\n"
                  "You can either generate a random sequence %R\n"
                  "or indicate a specific one %r\n"
                  "If you choose the random sequence specify it length %10d\n"
                  "Otherwise type your sequence %ls"
                  "Oligo length %8d base position %8d\n"
                  "sigma %12f\n"
                  ,&specific,&len,seq,&oliglen,&pos,&sig);
    if (i == WIN_CANCEL)	return OFF;

    if (specific) len = strlen(seq);
    else
    {
        for (i = 0; i < len && i < sizeof(seq); i++)
        {
            j = rand()%4;
            if (j == 0) seq[i] = 'A';
            else if (j == 1) seq[i] = 'C';
            else if (j == 2) seq[i] = 'G';
            else if (j == 3) seq[i] = 'T';
            else win_printf("Impossible base type!%d",j);
        }
        seq[i] = 0;
        len = i;
    }
    for (i = 0; i < len; i++)
    {
        if (seq[i] == 'A')       comp[i] = 'T';
        else if (seq[i] == 'C')  comp[i] = 'G';
        else if (seq[i] == 'G')  comp[i] = 'C';
        else if (seq[i] == 'T')  comp[i] = 'A';
    }
    comp[i] = 0;

    if ((opn = create_and_attach_one_plot(pr, 2*len, 2*len, 0)) == NULL)
        return win_printf_OK("cannot create plot !");

    dso = opn->dat[0];

    for (j = 0; j < len; j++)
    {
        if (seq[j] == 'A')       {dso->yd[j] = 1; dso->yd[2*len-j-1] =  4;}
        else if (seq[j] == 'C')  {dso->yd[j] = 2; dso->yd[2*len-j-1] =  3;}
        else if (seq[j] == 'G')  {dso->yd[j] = 3; dso->yd[2*len-j-1] =  2;}
        else if (seq[j] == 'T')  {dso->yd[j] = 4; dso->yd[2*len-j-1] =  1;}
        dso->xd[j] = j;
        dso->xd[2*len-j-1] = 2*len-j-1;
    }

    /* now we must do some house keeping */
    set_ds_source(dso,"Hairpin len %d seq %s",len,seq);
    set_plot_title(opn, "\\stack{{Hairpin len %d seq}{\\pt4 %s}{\\pt4 %s}}",len,seq,comp);
    set_plot_x_title(opn, "position");
    set_plot_y_title(opn, "type");
    opn->filename = strdup("haipin-seq.gr");


    if ((op = create_and_attach_one_plot(pr, len, len, 0)) == NULL)
        return win_printf_OK("cannot create plot !");
    set_plot_title(op, "Hairpin hybridization with NNXNNNNN");
    set_plot_x_title(op, "position");
    set_plot_y_title(op, "Strength");
    op->filename = strdup("hib-NNXNNNNN.gr");	

    ds = op->dat[0];
    hybridize_oligo(ds, seq, "NNANNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNCNNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNGNNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNTNNNNN");



    if ((op = create_and_attach_one_plot(pr, len, len, 0)) == NULL)
        return win_printf_OK("cannot create plot !");

    set_plot_title(op, "Hairpin hybridization with NNXXNNNN");
    set_plot_x_title(op, "position");
    set_plot_y_title(op, "Strength");
    op->filename = strdup("hib-NNXXNNNN.gr");	

    ds = op->dat[0];
    hybridize_oligo(ds, seq, "NNAANNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNACNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNAGNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNATNNNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNCANNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNCCNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNCGNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNCTNNNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNGANNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNGCNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNGGNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNGTNNNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNTANNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNTCNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNTGNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNTTNNNN");



    if ((op = create_and_attach_one_plot(pr, len, len, 0)) == NULL)
        return win_printf_OK("cannot create plot !");

    set_plot_title(op, "\\stack{{Hairpin hybridization with NNXXNNNN}{Smoothed over %f}}",sig);
    set_plot_x_title(op, "position");
    set_plot_y_title(op, "Strength");
    op->filename = strdup("hib-NNXXNNNN-smooth.gr");	

    ds = op->dat[0];
    hybridize_oligo_convol(ds, seq, "NNAANNNN",sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNACNNNN",sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNAGNNNN",sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNATNNNN",sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNCANNNN",sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNCCNNNN",sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNCGNNNN",sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNCTNNNN",sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNGANNNN",sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNGCNNNN",sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNGGNNNN",sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNGTNNNN",sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNTANNNN",sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNTCNNNN",sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNTGNNNN",sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNTTNNNN",sig);



    if ((op = create_and_attach_one_plot(pr, len, len, 0)) == NULL)
        return win_printf_OK("cannot create plot !");

    set_plot_title(op, "Hairpin hybridization with NNXXXNNN");
    set_plot_x_title(op, "position");
    set_plot_y_title(op, "Strength");
    op->filename = strdup("hib-NNXXXNNN.gr");	

    ds = op->dat[0];
    hybridize_oligo(ds, seq, "NNAAANNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNAACNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNAAGNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNAATNNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNACANNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNACCNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNACGNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNACTNNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNAGANNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNAGCNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNAGGNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNAGTNNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNATANNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNATCNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNATGNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNATTNNN");

    //
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNCAANNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNCACNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNCAGNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNCATNNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNCCANNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNCCCNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNCCGNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNCCTNNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNCGANNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNCGCNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNCGGNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNCGTNNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNCTANNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNCTCNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNCTGNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNCTTNNN");

    //
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNGAANNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNGACNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNGAGNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNGATNNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNGCANNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNGCCNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNGCGNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNGCTNNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNGGANNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNGGCNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNGGGNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNGGTNNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNGTANNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNGTCNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNGTGNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNGTTNNN");

    //
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNTAANNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNTACNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNTAGNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNTATNNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNTCANNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNTCCNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNTCGNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNTCTNNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNTGANNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNTGCNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNTGGNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNTGTNNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNTTANNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNTTCNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNTTGNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNTTTNNN");


    if ((op = create_and_attach_one_plot(pr, len, len, 0)) == NULL)
        return win_printf_OK("cannot create plot !");

    set_plot_title(op, "\\stack{{Hairpin hybridization with NNXXXNNN}{Smoothed over %f}}",sig);
    set_plot_x_title(op, "position");
    set_plot_y_title(op, "Strength");
    op->filename = strdup("hib-NNXXXNNN-smooth.gr");	

    ds = op->dat[0];
    hybridize_oligo_convol(ds, seq, "NNAAANNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNAACNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNAAGNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNAATNNN", sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNACANNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNACCNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNACGNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNACTNNN", sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNAGANNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNAGCNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNAGGNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNAGTNNN", sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNATANNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNATCNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNATGNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNATTNNN", sig);

    //
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNCAANNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNCACNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNCAGNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNCATNNN", sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNCCANNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNCCCNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNCCGNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNCCTNNN", sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNCGANNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNCGCNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNCGGNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNCGTNNN", sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNCTANNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNCTCNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNCTGNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNCTTNNN", sig);

    //
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNGAANNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNGACNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNGAGNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNGATNNN", sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNGCANNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNGCCNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNGCGNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNGCTNNN", sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNGGANNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNGGCNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNGGGNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNGGTNNN", sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNGTANNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNGTCNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNGTGNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNGTTNNN", sig);

    //
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNTAANNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNTACNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNTAGNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNTATNNN", sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNTCANNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNTCCNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNTCGNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNTCTNNN", sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNTGANNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNTGCNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNTGGNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNTGTNNN", sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNTTANNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNTTCNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNTTGNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNTTTNNN", sig);



    /* refisplay the entire plot */
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}


int do_oligo_plots_2_NX(void)
{
    register int i, j;
    O_p  *opn = NULL, *op = NULL;
    static int len = 80, specific = 1, oliglen = 8, pos = 2;
    char seq[4096], comp[4096];
    d_s *ds, *dso;
    pltreg *pr = NULL;
    static float sig = 1.5;


    if(updating_menu_state != 0)	return D_O_K;

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine generates histogramms of oligos binding\n"
                             " to an hairpin");
    }
    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
        return win_printf_OK("cannot find data");
    seq[0] = ' '; seq[1] = 0;
    i = win_scanf("This routine generates histogramms of oligos hybridization\n"
                  "to a DNA sequence forming an hairpin.\n"
                  "You can either generate a random sequence %R\n"
                  "or indicate a specific one %r\n"
                  "If you choose the random sequence specify it length %10d\n"
                  "Otherwise type your sequence %ls"
                  "Oligo length %8d base position %8d\n"
                  "sigma %12f\n"
                  ,&specific,&len,seq,&oliglen,&pos,&sig);
    if (i == WIN_CANCEL)	return OFF;

    if (specific) len = strlen(seq);
    else
    {
        for (i = 0; i < len && i < sizeof(seq); i++)
        {
            j = rand()%4;
            if (j == 0) seq[i] = 'A';
            else if (j == 1) seq[i] = 'C';
            else if (j == 2) seq[i] = 'G';
            else if (j == 3) seq[i] = 'T';
            else win_printf("Impossible base type!%d",j);
        }
        seq[i] = 0;
        len = i;
    }
    for (i = 0; i < len; i++)
    {
        if (seq[i] == 'A')       comp[i] = 'T';
        else if (seq[i] == 'C')  comp[i] = 'G';
        else if (seq[i] == 'G')  comp[i] = 'C';
        else if (seq[i] == 'T')  comp[i] = 'A';
    }
    comp[i] = 0;

    if ((opn = create_and_attach_one_plot(pr, 2*len, 2*len, 0)) == NULL)
        return win_printf_OK("cannot create plot !");

    dso = opn->dat[0];

    for (j = 0; j < len; j++)
    {
        if (seq[j] == 'A')       {dso->yd[j] = 1; dso->yd[2*len-j-1] =  4;}
        else if (seq[j] == 'C')  {dso->yd[j] = 2; dso->yd[2*len-j-1] =  3;}
        else if (seq[j] == 'G')  {dso->yd[j] = 3; dso->yd[2*len-j-1] =  2;}
        else if (seq[j] == 'T')  {dso->yd[j] = 4; dso->yd[2*len-j-1] =  1;}
        dso->xd[j] = j;
        dso->xd[2*len-j-1] = 2*len-j-1;
    }

    /* now we must do some house keeping */
    set_ds_source(dso,"Hairpin len %d seq %s",len,seq);
    set_plot_title(opn, "\\stack{{Hairpin len %d seq}{\\pt4 %s}{\\pt4 %s}}",len,seq,comp);
    set_plot_x_title(opn, "position");
    set_plot_y_title(opn, "type");
    opn->filename = strdup("haipin-seq.gr");


    if ((op = create_and_attach_one_plot(pr, len, len, 0)) == NULL)
        return win_printf_OK("cannot create plot !");
    set_plot_title(op, "Hairpin hybridization with NXNNNNNN");
    set_plot_x_title(op, "position");
    set_plot_y_title(op, "Strength");
    op->filename = strdup("hib-NXNNNNNN.gr");	

    ds = op->dat[0];
    hybridize_oligo(ds, seq, "NANNNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NCNNNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NGNNNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NTNNNNNN");



    if ((op = create_and_attach_one_plot(pr, len, len, 0)) == NULL)
        return win_printf_OK("cannot create plot !");

    set_plot_title(op, "Hairpin hybridization with NXXNNNNN");
    set_plot_x_title(op, "position");
    set_plot_y_title(op, "Strength");
    op->filename = strdup("hib-NXXNNNNN.gr");	

    ds = op->dat[0];
    hybridize_oligo(ds, seq, "NAANNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NACNNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NAGNNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NATNNNNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NCANNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NCCNNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NCGNNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NCTNNNNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NGANNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NGCNNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NGGNNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NGTNNNNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NTANNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NTCNNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NTGNNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NTTNNNNN");



    if ((op = create_and_attach_one_plot(pr, len, len, 0)) == NULL)
        return win_printf_OK("cannot create plot !");

    set_plot_title(op, "\\stack{{Hairpin hybridization with NXXNNNNN}{Smoothed over %f}}",sig);
    set_plot_x_title(op, "position");
    set_plot_y_title(op, "Strength");
    op->filename = strdup("hib-NXXNNNNN-smooth.gr");	

    ds = op->dat[0];
    hybridize_oligo_convol(ds, seq, "NAANNNNN",sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NACNNNNN",sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NAGNNNNN",sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NATNNNNN",sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NCANNNNN",sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NCCNNNNN",sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NCGNNNNN",sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NCTNNNNN",sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NGANNNNN",sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NGCNNNNN",sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NGGNNNNN",sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NGTNNNNN",sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NTANNNNN",sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NTCNNNNN",sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NTGNNNNN",sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NTTNNNNN",sig);



    if ((op = create_and_attach_one_plot(pr, len, len, 0)) == NULL)
        return win_printf_OK("cannot create plot !");

    set_plot_title(op, "Hairpin hybridization with NXXXNNNN");
    set_plot_x_title(op, "position");
    set_plot_y_title(op, "Strength");
    op->filename = strdup("hib-NXXXNNNN.gr");	

    ds = op->dat[0];
    hybridize_oligo(ds, seq, "NAAANNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NAACNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NAAGNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NAATNNNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NACANNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NACCNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NACGNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NACTNNNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NAGANNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NAGCNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NAGGNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NAGTNNNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NATANNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NATCNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NATGNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NATTNNNN");

    //
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NCAANNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NCACNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NCAGNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NCATNNNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NCCANNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NCCCNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NCCGNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NCCTNNNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NCGANNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NCGCNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NCGGNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NCGTNNNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NCTANNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NCTCNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NCTGNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NCTTNNNN");

    //
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NGAANNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NGACNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NGAGNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NGATNNNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NGCANNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NGCCNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NGCGNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NGCTNNNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NGGANNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NGGCNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NGGGNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NGGTNNNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NGTANNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NGTCNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NGTGNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NGTTNNNN");

    //
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NTAANNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NTACNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NTAGNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NTATNNNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NTCANNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NTCCNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NTCGNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NTCTNNNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NTGANNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NTGCNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NTGGNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NTGTNNNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NTTANNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NTTCNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NTTGNNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NTTTNNNN");


    if ((op = create_and_attach_one_plot(pr, len, len, 0)) == NULL)
        return win_printf_OK("cannot create plot !");

    set_plot_title(op, "\\stack{{Hairpin hybridization with NXXXNNNN}{Smoothed over %f}}",sig);
    set_plot_x_title(op, "position");
    set_plot_y_title(op, "Strength");
    op->filename = strdup("hib-NXXXNNNN-smooth.gr");	

    ds = op->dat[0];
    hybridize_oligo_convol(ds, seq, "NAAANNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NAACNNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NAAGNNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NAATNNNN", sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NACANNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NACCNNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NACGNNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NACTNNNN", sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NAGANNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NAGCNNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NAGGNNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NAGTNNNN", sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NATANNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NATCNNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NATGNNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NATTNNNN", sig);

    //
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NCAANNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NCACNNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NCAGNNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NCATNNNN", sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NCCANNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NCCCNNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NCCGNNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NCCTNNNN", sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NCGANNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NCGCNNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NCGGNNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NCGTNNNN", sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NCTANNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NCTCNNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NCTGNNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NCTTNNNN", sig);

    //
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NGAANNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NGACNNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NGAGNNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NGATNNNN", sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NGCANNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NGCCNNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NGCGNNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NGCTNNNN", sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NGGANNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NGGCNNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NGGGNNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NGGTNNNN", sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NGTANNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NGTCNNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NGTGNNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NGTTNNNN", sig);

    //
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NTAANNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NTACNNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NTAGNNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NTATNNNN", sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NTCANNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NTCCNNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NTCGNNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NTCTNNNN", sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NTGANNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NTGCNNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NTGGNNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NTGTNNNN", sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NTTANNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NTTCNNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NTTGNNNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NTTTNNNN", sig);



    /* refisplay the entire plot */
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}



int do_oligo_plots_2_NNXXXNN(void)
{
    register int i, j;
    O_p  *opn = NULL, *op = NULL;
    static int len = 80, specific = 1, oliglen = 8, pos = 2;
    char seq[4096], comp[4096];
    d_s *ds, *dso;
    pltreg *pr = NULL;
    static float sig = 1.5;


    if(updating_menu_state != 0)	return D_O_K; 

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine generates histogramms of oligos binding\n"
                             " to an hairpin");
    }
    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
        return win_printf_OK("cannot find data");
    seq[0] = ' '; seq[1] = 0;
    i = win_scanf("This routine generates histogramms of oligos hybridization\n"
                  "to a DNA sequence forming an hairpin.\n"
                  "You can either generate a random sequence %R\n"
                  "or indicate a specific one %r\n"
                  "If you choose the random sequence specify it length %10d\n"
                  "Otherwise type your sequence %ls"
                  "Oligo length %8d base position %8d\n"
                  "sigma %12f\n"
                  ,&specific,&len,seq,&oliglen,&pos,&sig);
    if (i == WIN_CANCEL)	return OFF;

    if (specific) len = strlen(seq);
    else
    {
        for (i = 0; i < len && i < sizeof(seq); i++)
        {
            j = rand()%4;
            if (j == 0) seq[i] = 'A';
            else if (j == 1) seq[i] = 'C';
            else if (j == 2) seq[i] = 'G';
            else if (j == 3) seq[i] = 'T';
            else win_printf("Impossible base type!%d",j);
        }
        seq[i] = 0;
        len = i;
    }
    for (i = 0; i < len; i++)
    {
        if (seq[i] == 'A')       comp[i] = 'T';
        else if (seq[i] == 'C')  comp[i] = 'G';
        else if (seq[i] == 'G')  comp[i] = 'C';
        else if (seq[i] == 'T')  comp[i] = 'A';
    }
    comp[i] = 0;

    if ((opn = create_and_attach_one_plot(pr, 2*len, 2*len, 0)) == NULL)
        return win_printf_OK("cannot create plot !");

    dso = opn->dat[0];

    for (j = 0; j < len; j++)
    {
        if (seq[j] == 'A')       {dso->yd[j] = 1; dso->yd[2*len-j-1] =  4;}
        else if (seq[j] == 'C')  {dso->yd[j] = 2; dso->yd[2*len-j-1] =  3;}
        else if (seq[j] == 'G')  {dso->yd[j] = 3; dso->yd[2*len-j-1] =  2;}
        else if (seq[j] == 'T')  {dso->yd[j] = 4; dso->yd[2*len-j-1] =  1;}
        dso->xd[j] = j;
        dso->xd[2*len-j-1] = 2*len-j-1;
    }

    /* now we must do some house keeping */
    set_ds_source(dso,"Hairpin len %d seq %s",len,seq);
    set_plot_title(opn, "\\stack{{Hairpin len %d seq}{\\pt4 %s}{\\pt4 %s}}",len,seq,comp);
    set_plot_x_title(opn, "position");
    set_plot_y_title(opn, "type");
    opn->filename = strdup("haipin-seq.gr");


    if ((op = create_and_attach_one_plot(pr, len, len, 0)) == NULL)
        return win_printf_OK("cannot create plot !");
    set_plot_title(op, "Hairpin hybridization with NNNXNNN");
    set_plot_x_title(op, "position");
    set_plot_y_title(op, "Strength");
    op->filename = strdup("hib-NNNXNNN.gr");	

    ds = op->dat[0];
    hybridize_oligo(ds, seq, "NNNANNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNNCNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNNGNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNNTNNN");



    if ((op = create_and_attach_one_plot(pr, len, len, 0)) == NULL)
        return win_printf_OK("cannot create plot !");

    set_plot_title(op, "Hairpin hybridization with NNNXXNNN");
    set_plot_x_title(op, "position");
    set_plot_y_title(op, "Strength");
    op->filename = strdup("hib-NNNXXNNN.gr");	

    ds = op->dat[0];
    hybridize_oligo(ds, seq, "NNNAANNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNNACNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNNAGNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNNATNNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNNCANNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNNCCNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNNCGNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNNCTNNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNNGANNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNNGCNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNNGGNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNNGTNNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNNTANNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNNTCNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNNTGNNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNNTTNNN");



    if ((op = create_and_attach_one_plot(pr, len, len, 0)) == NULL)
        return win_printf_OK("cannot create plot !");

    set_plot_title(op, "\\stack{{Hairpin hybridization with NNNXXNNN}{Smoothed over %f}}",sig);
    set_plot_x_title(op, "position");
    set_plot_y_title(op, "Strength");
    op->filename = strdup("hib-NNNXXNNN-smooth.gr");	

    ds = op->dat[0];
    hybridize_oligo_convol(ds, seq, "NNNAANNN",sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNNACNNN",sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNNAGNNN",sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNNATNNN",sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNNCANNN",sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNNCCNNN",sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNNCGNNN",sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNNCTNNN",sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNNGANNN",sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNNGCNNN",sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNNGGNNN",sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNNGTNNN",sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNNTANNN",sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNNTCNNN",sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNNTGNNN",sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNNTTNNN",sig);



    if ((op = create_and_attach_one_plot(pr, len, len, 0)) == NULL)
        return win_printf_OK("cannot create plot !");

    set_plot_title(op, "Hairpin hybridization with NNXXXNN");
    set_plot_x_title(op, "position");
    set_plot_y_title(op, "Strength");
    op->filename = strdup("hib-NNXXXNN.gr");	

    ds = op->dat[0];
    hybridize_oligo(ds, seq, "NNAAANN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNAACNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNAAGNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNAATNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNACANN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNACCNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNACGNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNACTNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNAGANN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNAGCNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNAGGNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNAGTNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNATANN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNATCNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNATGNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNATTNN");

    //
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNCAANN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNCACNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNCAGNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNCATNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNCCANN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNCCCNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNCCGNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNCCTNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNCGANN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNCGCNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNCGGNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNCGTNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNCTANN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNCTCNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNCTGNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNCTTNN");

    //
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNGAANN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNGACNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNGAGNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNGATNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNGCANN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNGCCNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNGCGNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNGCTNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNGGANN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNGGCNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNGGGNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNGGTNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNGTANN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNGTCNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNGTGNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNGTTNN");

    //
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNTAANN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNTACNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNTAGNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNTATNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNTCANN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNTCCNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNTCGNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNTCTNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNTGANN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNTGCNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNTGGNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNTGTNN");

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNTTANN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNTTCNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNTTGNN");
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo(ds, seq, "NNTTTNN");


    if ((op = create_and_attach_one_plot(pr, len, len, 0)) == NULL)
        return win_printf_OK("cannot create plot !");

    set_plot_title(op, "\\stack{{Hairpin hybridization with NNXXXNN}{Smoothed over %f}}",sig);
    set_plot_x_title(op, "position");
    set_plot_y_title(op, "Strength");
    op->filename = strdup("hib-NNXXXNN-smooth.gr");	

    ds = op->dat[0];
    hybridize_oligo_convol(ds, seq, "NNAAANN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNAACNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNAAGNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNAATNN", sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNACANN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNACCNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNACGNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNACTNN", sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNAGANN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNAGCNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNAGGNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNAGTNN", sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNATANN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNATCNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNATGNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNATTNN", sig);

    //
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNCAANN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNCACNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNCAGNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNCATNN", sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNCCANN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNCCCNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNCCGNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNCCTNN", sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNCGANN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNCGCNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNCGGNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNCGTNN", sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNCTANN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNCTCNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNCTGNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNCTTNN", sig);

    //
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNGAANN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNGACNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNGAGNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNGATNN", sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNGCANN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNGCCNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNGCGNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNGCTNN", sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNGGANN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNGGCNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNGGGNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNGGTNN", sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNGTANN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNGTCNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNGTGNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNGTTNN", sig);

    //
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNTAANN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNTACNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNTAGNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNTATNN", sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNTCANN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNTCCNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNTCGNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNTCTNN", sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNTGANN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNTGCNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNTGGNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNTGTNN", sig);

    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNTTANN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNTTCNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNTTGNN", sig);
    ds = create_and_attach_one_ds(op,len, len, 0);
    hybridize_oligo_convol(ds, seq, "NNTTTNN", sig);



    /* refisplay the entire plot */
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}




MENU *oligo_plot_menu(void)
{
    static MENU mn[32];

    if (mn[0].text != NULL)	return mn;


    add_item_to_menu(mn,"produce oligo pos files", do_oligo_fake_data,NULL,0,NULL);
    add_item_to_menu(mn,"plot hairpin", do_oligo_plots,NULL,0,NULL);
    add_item_to_menu(mn,"plot hairpin 2", do_oligo_plots_2,NULL,0,NULL);
    add_item_to_menu(mn,"plot hairpin 2 NX", do_oligo_plots_2_NX,NULL,0,NULL);
    add_item_to_menu(mn,"plot hairpin debug", do_oligo_plots_debug,NULL,0,NULL);
    add_item_to_menu(mn,"plot hairpin center", do_oligo_plots_2_NNXXXNN,NULL,0,NULL);
    add_item_to_menu(mn,"# of oligos covering hairpin", do_find_oligos_number_covering_sequence,NULL,0,NULL);
    add_item_to_menu(mn,"compute covering", do_compute_covering,NULL,0,NULL);



    return mn;
}

int	oligo_main(int argc, char **argv)
{
    (void)argc;
    (void)argv;
    add_plot_treat_menu_item ( "oligo", NULL, oligo_plot_menu(), 0, NULL);
    return D_O_K;
}

int	oligo_unload(int argc, char **argv)
{
    (void)argc;
    (void)argv;  
    remove_item_to_menu(plot_treat_menu, "oligo", NULL, NULL);
    return D_O_K;
}
#endif

