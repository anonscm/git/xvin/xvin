#ifndef _OLIGO_H_
#define _OLIGO_H_

PXV_FUNC(int, do_oligo_rescale_plot, (void));
PXV_FUNC(MENU*, oligo_plot_menu, (void));
PXV_FUNC(int, do_oligo_rescale_data_set, (void));
PXV_FUNC(int, oligo_main, (int argc, char **argv));
#endif

