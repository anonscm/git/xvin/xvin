// definitions for import of Labview created files
//
// Nicolas Garnier, January 19th 2005
#ifndef _XV_LABVIEW_H_
#define _XV_LABVIEW_H_

#define IS_NI_4472_FILE  0x001000
#define IS_NI_DAQmx_FILE 0x000010

#define IS_NI_modified_SF8 0x001000

// accessible from menu :
PXV_FUNC(int, do_inout_load_DT3005_data,			(void));
PXV_FUNC(int, do_inout_load_NI4472_data,			(void));
PXV_FUNC(int, do_inout_load_NI_acq_mx_data,			(void));
PXV_FUNC(int, do_inout_load_NI_streamfloat7mx_data, (void));

// non accessible from menu:
PXV_FUNC(int, read_header_NI4472,				(char *filename, long *position_in_file, long *size_of_data_in_floats, float *f_acq));
PXV_FUNC(long, get_header_informations_NI4472, 	(char *filename, char **channel_string, char **channel_cfg, int *N_channels, float *F_s, float *F_r, 
												char **date_stamp, char **user_header, long *N));
PXV_FUNC(long, write_header_informations_NI4472,(char *filename, char *channel_string, char *channel_cfg, int N_channels, float F_s, float F_r,
												char *date_stamp, char *user_header, long N));
PXV_FUNC(int, load_NI4472_data,					(char *filename, long header_length, int N, 
												int N_channels, int i_channel, int N_decimate, float **x));
PXV_FUNC(int, decimate_NI_file,					(char *filename_in, int file_type, // NI4472 or Daqmx file
												int N_channels, // number of different channels
												int N_decimate, // decimation: keep 1 point every N_decimate (the average is kept)
												char *filename_out));	 // the loaded data, pointer is allocated by the function itself	
PXV_FUNC(int, extract_NI_file,                  (char *filename_in, int file_type,
                                                int N_channels, // number of different channels
		                                        int i_channel, // number of channel of interest
		                                        int N_decimate, // decimation: keep 1 point every N_decimate (the average is kept) 
		                                        float factor, // multiply data by this factor
		                                        float offset, // and then add an offset
		                                        char *filename_out)); // the loaded data, pointer is allocated by the function itself

PXV_FUNC(long, get_header_informations_DAQMX,	(char *filename, char **channel_string, int *N_channels, float *F_s, 
												char **date_stamp, char **user_header, long *N));												
PXV_FUNC(long, write_header_informations_DAQMX,(char *filename, char *channel_string, int N_channels, float F_s, 
												char *date_stamp, char *user_header, long N));
void test_scan_file(char *filename);
												
#endif


