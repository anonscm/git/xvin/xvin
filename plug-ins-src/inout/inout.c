/************************************************************************/
/* input/output functions						*/
/*									*/
/************************************************************************/
/* Vincent Croquette, Nicolas Garnier	nicolas.garnier@ens-lyon.fr	*/
/************************************************************************/
#ifndef _INOUT_C_
#define _INOUT_C_

#include "allegro.h"
#include "xvin.h"
#include "xv_tools_lib.h"
#include <stdlib.h>
#include <math.h>
#include <malloc.h>
#include <string.h>	// for swap_bytes

#define BUILDING_PLUGINS_DLL
#include "inout.h"
#include "xv_matlab.h"
#include "xv_Labview.h"



/************************************************************************/
/* Implementation							*/
/************************************************************************/


int load_parameter(char *filename, char *param, float *x)
/* load parameter "param" from the text file named "filename" 		*/
/* very usefull for batch-runs						*/
{	FILE 	*param_file;
	int	err=0;
	char	*ch;
	char 	c;
	int 	go_on=1;

	param_file=fopen(filename, "rt");
	if (param_file==NULL) 
    { // win_printf("load_parameter: error opening file %s", backslash_to_slash(filename));
       return(-1);
    }

	ch=(char*)calloc(128,sizeof(char));
	while ( (!feof(param_file)) && go_on)
	{	err=fscanf(param_file,"%s",ch);
		if ( (strncmp(ch,"%",1)==0) || (strncmp(ch,"#",1)==0) || (strncmp(ch,"//",2)==0) ) 
		/* commentaire detecte : on ignore toute la fin de la ligne */
		{	c=ch[strlen(ch)-1]; 
			while ( (!feof(param_file)) && (c!='\n') )
			{	c = fgetc(param_file);
			}
		}
		if (strcmp(param,ch)==0) go_on=0;
	}
	if (err==EOF) 
	{ 	fclose(param_file); 
		return err; 
	}
	err=fscanf(param_file,"%f",x);
	fclose(param_file);
	free(ch);
	return err;
}


int load_parameter_string(char *filename, char *param, char *x)
/* load parameter "param" from the text file named "filename" 		*/
/* very usefull for batch-runs						*/
{	FILE 	*param_file;
	int	err=0;
	char	*ch;
	char 	c;
	int 	go_on=1;

	param_file=fopen(filename, "rt");
	if (param_file==NULL) 
    { // win_printf("load_parameter: error opening file %s", backslash_to_slash(filename));
       return(-1);
    }

	ch=(char*)calloc(128,sizeof(char));
	while ( (!feof(param_file)) && go_on)
	{	err=fscanf(param_file,"%s",ch);
		if ( (strncmp(ch,"%",1)==0) || (strncmp(ch,"#",1)==0) || (strncmp(ch,"//",2)==0) ) 
		/* commentaire detecte : on ignore toute la fin de la ligne */
		{	c=ch[strlen(ch)-1]; 
			while ( (!feof(param_file)) && (c!='\n') )
			{	c = fgetc(param_file);
			}
		}
		if (strcmp(param,ch)==0) go_on=0;
	}
	if (err==EOF) 
	{ 	fclose(param_file); 
		return err; 
	}
	err=fscanf(param_file,"%s",x);
	fclose(param_file);
	free(ch);
	return err;
}



/************************************************************************/
int get_data_length(char *filename)
/************************************************************************/
{	FILE 	*data_file;
	long int N=0;
	
	data_file=fopen(filename, "rb");
	if (data_file==NULL) 
    {   win_printf("get_data_length: error opening file %s", backslash_to_slash(filename));
        return(-1);
    }
	fseek(data_file, 0, SEEK_END);
	N=ftell(data_file);
	fclose(data_file);
	
	return(N/sizeof(float));
}




/************************************************************************/
int load_data_bin(char *filename, float **x1)
/* to load an array of float as a variable				*/
/* complex number are stored as 2 consecutive real numbers for real and */
/*	imaginary part. So a n-dimensionnal vector of complex contains  */
/*	2*n real numbers.						*/
/************************************************************************/
{	FILE 	*data_file;
	int	i,N;
	float 	*x;
	
	data_file=fopen(filename, "rb");
	if (data_file==NULL) 
    {   win_printf("load_data_set: error opening file %s", backslash_to_slash(filename));
        return(-1);
    }
	
	x =(float*)calloc(1,sizeof(float)); 
	

	N=-1;
	while (!feof(data_file)) 
	{ 	i=fread(x,sizeof(float),1,data_file);
//		if ( (i!=1) && (verbosity>2) ) 
//			printf("for the %dth element, value=%f with error %d\n",N,x[0],i);
		N++; 
	}
	
	free(x);
	rewind(data_file);
//	if (verbosity>1) printf("load_data_set: file %s contains %d floats.\n",filename,N);

	*x1=(float*)calloc(N,sizeof(float));
	i=fread(*x1,sizeof(float),N,data_file);	
#ifdef XV_MAC
#ifdef MAC_POWERPC
	swap_bytes(*x1,N,sizeof(float));
#endif
#endif
	fclose(data_file);
	return i;
}




/************************************************************************/
int load_data_bin_to_double(char *filename, double **x1)
/* to load an array of double from a file containing floats               */
/* complex number are stored as 2 consecutive real numbers for real and */
/*	imaginary part. So a n-dimensionnal vector of complex contains  */
/*	2*n real numbers.						*/
/************************************************************************/
{	FILE 	*data_file;
	int	i,N;
	float 	*x;
	
	data_file=fopen(filename, "rb");
	if (data_file==NULL) 
    {   win_printf("load_data_set: error opening file %s", backslash_to_slash(filename));
        return(-1);
    }
	
	x =(float*)calloc(1,sizeof(float)); 
	
	N=-1;
	while (!feof(data_file)) 
	{ 	i=fread(x,sizeof(float),1,data_file);
//		if ( (i!=1) && (verbosity>2) ) 
//			printf("for the %dth element, value=%f with error %d\n",N,x[0],i);
		N++; 
	}
	
	free(x);
	rewind(data_file);
//	if (verbosity>1) printf("load_data_set: file %s contains %d floats.\n",filename,N);

	x=(float*)calloc(N,sizeof(float));
	i=fread(x,sizeof(float),N,data_file);	
#ifdef XV_MAC
#ifdef MAC_POWERPC
	swap_bytes(x,N,sizeof(float));
#endif
#endif
	*x1=(double*)calloc(N,sizeof(double));
    for (i=0; i<N; i++) (*x1)[i]=(double)x[i];
    free(x);
    
	fclose(data_file);
	return i;
}



/************************************************************************/
int load_data_chunk(char *filename, float **x1, long int n, long int offset)
/* to load an array of float as a variable				*/
/* complex number are stored as 2 consecutive real numbers for real and */
/*	imaginary part. So a n-dimensionnal vector of complex contains  */
/*	2*n real numbers.						                          */
/*                                                                      */
/* offset makes the loading start at offset 'shift' instead of zero      */
/* n      makes the loading to get n points only                         */
/* offset and n are in 'float' units, not octets                          */
/* on success, it returns n                                             */
/* NG, 2006-12-21                                                       */
/************************************************************************/
{	FILE 	*data_file;
	int	    i;
    long int N;
	float 	*x;
	
	data_file=fopen(filename, "rb");
	if (data_file==NULL) 
    {   win_printf("load_data_set_offset: error opening file %s", backslash_to_slash(filename));
        return(-1);
    }
	if (fseek(data_file, offset*sizeof(float), SEEK_SET)!=0)
    {   win_printf("load_data_set_offset:\nfor file %s, offset %d is probably too large", 
                    backslash_to_slash(filename), offset);
        fclose(data_file);
        return(-1);
    }
	if (fseek(data_file, n*sizeof(float), SEEK_CUR)!=0)
    {   fseek(data_file, offset*sizeof(float), SEEK_SET);
        x =(float*)calloc(1, sizeof(float)); 
	
    	N=-1;
	    while (!feof(data_file)) 
	    {  i=fread(x,sizeof(float),1,data_file);
		   N++; 
	    }
	
	    free(x);
    }
    else
    {   N=n; // it is OK, we have enough points.
    }

    fseek(data_file, offset*sizeof(float), SEEK_SET);

	*x1=(float*)calloc((int)N,sizeof(float));
	i=fread(*x1,sizeof(float),N,data_file);	
#ifdef XV_MAC
#ifdef MAC_POWERPC
	swap_bytes(*x1,N,sizeof(float));
#endif
#endif
	fclose(data_file);
	return i;
}



/************************************************************************/
/* to save an array of float in a binary file				*/
/* see comments of load_data_set for more information			*/
/************************************************************************/
int save_data_bin(char *filename, float *x1, int N)
{	FILE 	*data_file;
	int	err;
	
	data_file=fopen(filename, "wb");
#ifdef XV_MAC
#ifdef MAC_POWERPC
	swap_bytes(x1,N,sizeof(float));
#endif	
#endif
	err=fwrite(x1,sizeof(float),N,data_file);
//	if (verbosity>1) printf("save_data_set: file %s save with %d/%d floats.\n",filename,err,N);
	fclose(data_file);
#ifdef XV_MAC
#ifdef MAC_POWERPC
	swap_bytes(x1,N,sizeof(float));
#endif
#endif
	return err;
}



/************************************************************************/
/* to save an array of doubles in a binary file	in float format !!!  */
/* see comments of load_data_set for more information			     */
/************************************************************************/
int save_data_double_bin(char *filename, double *x1, int N)
{	FILE 	*data_file;
    register int i;
	int	    err;
	float   *x;
	
	x=(float*)calloc((int)N, sizeof(float));
	for (i=0; i<N; i++) x[i] = (float)x1[i];
	
	data_file=fopen(filename, "wb");
#ifdef XV_MAC
#ifdef MAC_POWERPC
	swap_bytes(x,N,sizeof(float));
#endif	
#endif
	err=fwrite(x,sizeof(float),N,data_file);
//	if (verbosity>1) printf("save_data_set: file %s save with %d/%d floats.\n",filename,err,N);
	fclose(data_file);
    free(x);    

	return err;
}




/************************************************************************/
/* to save (append mode) an array of float in a binary file		*/
/* see comments of load_data_set for more information			*/
/************************************************************************/
int append_data_bin(char *filename, float *x1, int N)
{	FILE 	*data_file;
	int	err;
	
	data_file=fopen(filename, "a+b");
#ifdef XV_MAC
#ifdef MAC_POWERPC
	swap_bytes(x1,N,sizeof(float));
#endif
#endif
	err=fwrite(x1,sizeof(float),N,data_file);
//	if (verbosity>1) printf("append_data_set: file %s saved with %d/%d floats.\n",filename,err,N);
	fclose(data_file);
#ifdef XV_MAC
#ifdef MAC_POWERPC
	swap_bytes(x1,N,sizeof(float));
#endif
#endif
	return err;
}


/************************************************************************/
/* to save an array of float in a text file (ascii)			*/
/* see comments of load_data_set for more information			*/
/************************************************************************/
int save_data_ascii(char *filename, float *x1, int N)
{	FILE 	*data_file;
	register int i;
	int	err=0;
	
	data_file=fopen(filename, "wt");
	for (i=0; i<N; i++)
	{	err=fprintf(data_file, "%10.8f\n",x1[i]);
	}
	fclose(data_file);

	return err;
}



int do_inout_load_data_bin(void)
{
	register int i, j;
	O_p *op = NULL, *opn = NULL;
	int nf;
	float *x=NULL;
	d_s *ds, *dsi;
	pltreg *pr = NULL;
static char fullfile[512]="";
    char filename[256],pathname[512];

	if(updating_menu_state != 0)		return D_O_K;	
	
	if (key[KEY_LSHIFT]) return win_printf_OK("This routine loads a binary file .dat\n"
		"and places it in a new plot\n"
		"it is assumed that there is a single dataset in the file.");

 	i = file_select_ex("Load binary .dat file", fullfile, "dat", 512, 0, 0);
	if (i == 0) return D_O_K;

	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
		return win_printf_OK("cannot find data");

	nf=load_data_bin(fullfile, &x);
	
	if (nf==0) return win_printf_OK("nothing loaded!");
	if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	ds = opn->dat[0];

	for (j=0; j<nf; j++)
	{	ds->yd[j] = x[j];
		ds->xd[j] = j;
	}

	free(x);

	extract_file_name( filename,  256, fullfile);
	extract_file_path( pathname,	512, fullfile);
	opn->filename   = strdup(filename);
	opn->dir 	= Mystrdup(pathname);		

	set_plot_title(opn, "%s", opn->filename);
	set_plot_x_title(opn, "x (%d points)",nf);
	set_plot_y_title(opn, "y");

	set_ds_source(ds, "binary file %s", filename);

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
}

/*******************************************************/
/* following routine saves in raw format with floats	*/
/* NG, 21/06/2005								*/
/*******************************************************/
int do_inout_export_raw_file(void)
{	int i;
	O_p *op = NULL;
	int		n;
	d_s *ds;
	pltreg *pr = NULL;
	char fullfile[512],filename[256];

	if(updating_menu_state != 0)		return D_O_K;	
	
	if (key[KEY_LSHIFT]) return win_printf_OK("This routine saves the current data set\n"
								"in a binary file .bin\n");

 	i = file_select_ex("Export binary .bin file", fullfile, "bin", 512, 0, 0);
	if (i == 0) return D_O_K;
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
		return win_printf_OK("cannot find data");

	n=ds->ny;

	if (save_data_bin(fullfile, ds->yd, n) != n)
	return(win_printf_OK("Error saving file, only %d points saved!", n));

	extract_file_name(filename,  256, fullfile);
	win_printf_OK("file %s saved with %d float (32b)", filename, n);
	
	return D_O_K;	
}



/************************************************************************/
/* this functions loads a file created by Labview (big endian) and	*/
/* and containing data acquired through a NI4472 card			*/
/************************************************************************/
int do_inout_load_raw_image(void)
{
	register int i, j;
	O_i	*oid= NULL;
	imreg 	*imr = NULL;
	FILE 	*data_file;	
static char 	fullfilename[512];
	char	filename[256],pathname[512];
	int	*tampon_int=NULL;
	float	*tampon_float=NULL;
	char	*tampon_char=NULL;
static	int 	data_type=0;
static	int	nx=10,ny=10;
	union pix *pd;
	
	if (updating_menu_state != 0)		return D_O_K;	
	
	if (key[KEY_LSHIFT]) return win_printf_OK("This routine loads a binary image.");

 	if ( file_select_ex("Load raw image", fullfilename, "", 512, 0, 0) == 0) return D_O_K;

	if (ac_grep(cur_ac_reg,"%imr",&imr) != 1)		return win_printf_OK("cannot find data");

	i = win_scanf("{\\color{yellow}\\pt14 loading raw image}\n\nimage dimensions\n %4d nx\n %4d ny\n\n"
			"data type\n %R float\n %r int\n%r char", &nx, &ny, &data_type );
	if (i == WIN_CANCEL) return D_O_K;

	if (data_type==0)	tampon_float = (float*)calloc(nx,sizeof(float));
	else if (data_type==1)	tampon_int = (int*)calloc(nx,sizeof(int));
	else if (data_type==2)	tampon_char = (char*)calloc(nx,sizeof(char));
	else return D_O_K;

	data_file=fopen(fullfilename, "rb");
	if (data_file==NULL) return(win_printf_OK("error opening file %s",fullfilename));	

	if (data_type==0)	oid = create_and_attach_oi_to_imr(imr, nx, ny, IS_FLOAT_IMAGE);
	if (data_type==1)	oid = create_and_attach_oi_to_imr(imr, nx, ny, IS_INT_IMAGE);
	if (data_type==2)	oid = create_and_attach_oi_to_imr(imr, nx, ny, IS_CHAR_IMAGE);

	if (oid == NULL)	return(win_printf_OK("cannot create destination image!"));
	pd = oid->im.pixel;	

	oid->im.nx = oid->im.nxe = nx;
	oid->im.ny = oid->im.nye = ny;	
	oid->im.nxs = oid->im.nys = 0;
	if (data_type==0)
	{	for (i=0; i<ny; i++)
		{	fread(tampon_float, sizeof(float), nx, data_file);
			for (j=0; j<nx; j++)
			{	pd[i].fl[j] = tampon_float[j];
			}
		}
	}
	else if (data_type==1)
	{	for (i=0; i<ny; i++)
		{	fread(tampon_int, sizeof(int), nx, data_file);
			for (j=0; j<nx; j++)
			{	pd[i].in[j] = tampon_int[j];
			}
		}
	}
	else if (data_type==2)
	{	for (i=0; i<ny; i++)
		{	fread(tampon_char, sizeof(char), nx, data_file);
			for (j=0; j<nx; j++)
			{	pd[i].ch[j] = tampon_char[j];
			}
		}
	}
	else return OFF;

	extract_file_name( filename,    256, fullfilename);
	extract_file_path( pathname,	512, fullfilename);

	if (data_type==0)	free(tampon_float);
	if (data_type==1)	free(tampon_int);
	if (data_type==2)	free(tampon_char);
	fclose(data_file);

	oid->filename   = strdup(filename);
	oid->dir 	= Mystrdup(pathname);		
//	set_im_src(oid, "raw image %s", filename);
	set_im_title(oid, "%s", filename);

	find_zmin_zmax(oid);
	return (refresh_image(imr, imr->n_oi - 1));
}/* end of do_inout_load_raw_image */






/********************************************************************************/
/* loads just one colomn from a tabular ascii file				*/
/********************************************************************************/
int do_inout_load_column_from_tabular_file(void)
{	int 		i,j=0,ny=0;
	static int	bool_first=1;
	static int	nx=3,n=2;
	static char 	tabular_file_name[512]="toto.dat";
	char		s[256];
	FILE		*tabular_file;
        pltreg		*pr;
	O_p		*op;
	d_s		*ds;
	

	if(updating_menu_state != 0)		return D_O_K;	

	i = file_select_ex("Load one colomn of a tabular ASCII .dat file", tabular_file_name, "dat", 512, 0, 0);
	if (i==0) return OFF;

	tabular_file=fopen(tabular_file_name,"rt"); /* read text */
	if (tabular_file==NULL) return(win_printf("file not found !"));
	i=win_scanf("Tell me what's the number of columns\nin the file %d"
			"And tell me the number of the one\nyou want to extract %d",
			&nx,&n);
	if (i==WIN_CANCEL) return(OFF);

	while (!feof(tabular_file))
	{	for (i=1; i<=nx; i++) j=fscanf(tabular_file,"%s",s);
		if (j!=EOF) ny++;
	}
	fclose(tabular_file);
	sprintf(s,"There are %d lines in your file \n%s.\n\n"
		"The first one may contains text (titles...).\n"
		"Do you want to keep the first one ? %%d",ny,tabular_file_name);
	i=win_scanf(s,&bool_first);
	if (i==WIN_CANCEL) 		return(OFF);
	
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
				return win_printf_OK("cannot find a plot!");
	if (bool_first==1) 	ds = create_and_attach_one_ds(op, ny, ny, 0);
	else			ds = create_and_attach_one_ds(op, ny-1, ny-1, 0);
	if (ds==NULL)		return win_printf_OK("cannot create dataset !");
		
	tabular_file=fopen(tabular_file_name,"rt");
	if (tabular_file==NULL) return(win_printf("file not found !"));

	if (bool_first==0) for (i=1; i<=nx; i++) { fscanf(tabular_file,"%s",s);
					         /* printf("%s rejected in 1 line \n",s);  */}
	j=0;
	while (j<ny-1+bool_first)
	{	for (i=1; i<n; i++) { fscanf(tabular_file,"%s",s);
			 		/* printf("%s in column %d\n",s,i); */}
		fscanf(tabular_file,"%s",s);		 		
		ds->yd[j]=atof(s);
		/* printf("%s in column %d is to be kept to %f\n",s,i,atof(s)); */
	
		for (i=n+1; i<=nx; i++) { fscanf(tabular_file,"%s",s);
			 		/* printf("%s in column %d \n",s,i); */}
		j++;
	}
	fclose(tabular_file);	

	for (i=0; i<ds->ny; i++) ds->xd[i]=i;
	
	set_ds_source(ds, "colomn %d out of %d of ASCII tabular %s", n, nx, tabular_file_name);
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
}


/********************************************************************************/
/* loads a tabular file, assuming the first colomn represents the X-axis, 	*/
/* and other colomns are functions of the first colomn				*/
/********************************************************************************/
int do_inout_load_tabular_file(void)
{
	register int i, j, k;
	O_p 	*opn = NULL;
	int	n_col, n_lines;
	float 	*x=NULL;
	d_s 	*ds;
	pltreg	*pr = NULL;
	char 	fullfilename[512],filename[256],pathname[512];
	FILE	*file;
	char	s[256];

	if(updating_menu_state != 0)	return D_O_K;	

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT]) return win_printf_OK("This routine loads a tabular ASCII file .dat\n"
		"and places it in a new plot\n\n It will assumes the first column is for the X-axis\n"
		"and all additional columns are for different datasets that have the same X-axis.");

 	i = file_select_ex("Load ascii tabular .dat file", fullfilename, "dat", 512, 0, 0);
	if (i == 0) return D_O_K;

	if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)		return win_printf_OK("cannot find plot region!");

	n_col = count_number_of_colomns_in_ascii_file(fullfilename, 0); // assuming no header lines
	if (n_col<2)		return(win_printf_OK("there is only %d colomn(s), not enough!\n"
							"use other function (load 1 colomn)",n_col));
	n_lines = count_number_of_lines_in_ascii_file(fullfilename, n_col);

	
	if ((opn = create_and_attach_one_plot(pr, n_lines, n_lines, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	x = (float*)calloc(n_lines,sizeof(float));
	ds = opn->dat[0];

	file=fopen(fullfilename,"rt");
	if (file==NULL) return(win_printf("file not found !"));

	for (j=0; j<n_lines; j++)
	{	fscanf(file,"%s",s);		ds->xd[j]=x[j]=atof(s);
		fscanf(file,"%s",s);		ds->yd[j]=atof(s);

		for (i=2+1; i<=n_col; i++) 	fscanf(file,"%s",s);
	}
	fclose(file);	
	set_ds_source(ds, "colomn 2 vs colomn 1 of ASCII tabular %s", filename);

	for (k=3; k<=n_col; k++)		// 2003-01-08 : bug corrected
	{	file=fopen(fullfilename,"rt");
		ds = create_and_attach_one_ds(opn, n_lines, n_lines, 0);
		set_ds_source(ds, "colomn %d vs colomn 1 of ASCII tabular %s", k, filename);
		if (ds==NULL)		return win_printf_OK("cannot create dataset !");
		for (j=0; j<n_lines; j++)
		{	for (i=1; i<k; i++) { fscanf(file,"%s",s); }
			fscanf(file,"%s",s);		
			ds->yd[j]=atof(s);
			ds->xd[j]=x[j];

			for (i=k+1; i<=n_col; i++) { fscanf(file,"%s",s); }

		}
		fclose(file);	
	}

	free(x);

	set_plot_title(opn, "%d points in X, %d dataset(s)", n_lines, n_col-1);
	set_plot_x_title(opn, "x");
	set_plot_y_title(opn, "y");

	extract_file_name( filename,  256, fullfilename);
	extract_file_path( pathname,	512, fullfilename);
	opn->filename   = strdup(filename);
	opn->dir 	= Mystrdup(pathname);

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
}





/********************************************************************************/
/* load a tabular file, with 3 colomns						*/
/* it assumes the first 2 colomns represents the lower and upper bounds of bins */
/* and that the 3rd colomn is the number of realisations within that bin	*/
/********************************************************************************/
int do_inout_load_histogram(void)
{	register int 	i=0;
	static int	nx=0,ny=0;
	static char 	tabular_file_name[512];
	char		s[256], s1[128], s2[128];
	FILE		*tabular_file;
        pltreg		*pr;
	O_p		*op;
	d_s		*ds;
	
	if (updating_menu_state != 0)	return D_O_K;	

	if (key[KEY_LSHIFT]) return win_printf_OK("This routine loads an histogram from tabular ascii file .dat\n\n"
		"The histogram structure is such that the ascii file has 3 colomns:\n"
		" - the first and the second one give the limits [x_n x_{n+1}] of an interval\n"
		" - the 3rd colomn gives the number of realisations in that interval\n\n"
		"There is 1 resulting dataset, plotted within the same plot\n"
		"This dataset has X-values that are computed as : {{x_n + x_{n+1}} \\over {2}}");


	i = file_select_ex("Load histogram .dat file", tabular_file_name, "dat", 512, 0, 0);
	if (i==0) return OFF;

	nx = count_number_of_colomns_in_ascii_file(tabular_file_name, 0);
	ny = count_number_of_lines_in_ascii_file  (tabular_file_name, nx);
	if (nx*ny==0) return(win_printf("file not found or empty !"));
	if (nx!=3)    return(win_printf("not an histogram !"));
	
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
				return win_printf_OK("cannot find a plot!");
	ds = create_and_attach_one_ds(op, ny, ny, 0);
	if (ds==NULL)		return win_printf_OK("cannot create dataset !");
		
	tabular_file=fopen(tabular_file_name,"rt");

	for (i=0; i<ny; i++)
	{	fscanf(tabular_file,"%s",s1);
		fscanf(tabular_file,"%s",s2);
		ds->xd[i] = (float)((atof(s1)+atof(s2))/2);
		fscanf(tabular_file,"%s",s);		 		
		ds->yd[i] = atof(s);	
	}
	fclose(tabular_file);	

	extract_file_name( s,  256, tabular_file_name);
	set_ds_source(ds, "histogram %s", s);
		
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
}



/* n_col indicates the number of colomns 					*/
int	count_number_of_lines_in_ascii_file(char *filename, int n_col)
{	FILE	*file;
	int	length_s=2048;
	char	s[length_s];	// one value read
	register int	i, j=0, ny=0;

	file = fopen(filename,"rt");
	if (file==NULL) return(0);

	while (!feof(file))
	{	for (i=1; i<=n_col; i++) j=fscanf(file,"%s",s);
		if (j!=EOF) ny++;
	}
	fclose(file);

	return(ny);
}


/* n_header indicates how many lines to skip at the begining of the file (header) */
int	count_number_of_colomns_in_ascii_file(char *filename, int n_header)
{	FILE	*file;
const	int	length_s=2048;
	char	ms[length_s];	// one value read
	char	c=1;
	int	n, p;

	for (n=0; n<length_s-3; n++)	ms[n]=50; 

	file = fopen(filename,"rt");
	if (file==NULL) return(0);
	for (n=0; n<=n_header; n++)	fgets(ms, length_s, file);
	fclose(file);

	n=0;;
	for (p=1; p<(int)strlen(ms); p++)
	{	c=ms[p];
		if ( (c<42) && (ms[p-1]>42) )
		{	n++;
		}
	}	

	return(n);
}







/********************************************************************************/
/* exports all dataset in a single tabular file, 				*/
/* assuming X-axis of the first dataset is the same for all dataset,	 	*/
/********************************************************************************/
int do_inout_export_tabular_file(void)
{
	register int i, j;
	O_p 	*op;
	int	 n_lines;// n_col
	pltreg	*pr = NULL;
	char 	fullfilename[512];
	FILE	*file;

	if(updating_menu_state != 0)	return D_O_K;	

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT]) return win_printf_OK("This routine exports all the datasets of the current plot in a tabular ascii file .dat\n"
		"\nFor now (11/2003), it assumes the X-axis of all the datasets is the same\n"
		"and it saves it as the first colomn of the tabular file.");

	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)		return win_printf_OK("cannot find plot!");
	n_col 	= op->n_dat + 1;
	n_lines = op->dat[0]->nx;

	j=0;
	for (i=1; i<op->n_dat; i++) 
	{	if (op->dat[i]->nx != n_lines) 
			j++;
	}
	if (j!=0) return win_printf_OK("There are %d datasets which number of points differ !!!\n"
				       "Eliminate them from the current plot, and try again.",j);

 	i = file_select_ex("Export ascii tabular .dat file", fullfilename, "dat", 512, 0, 0);
	if (i == 0) return D_O_K;
	file=fopen(fullfilename,"wt");
	if (file==NULL) return(win_printf("cannot create file !"));

	for (j=0; j<n_lines; j++)
	{	fprintf(file, "%f %f", op->dat[0]->xd[j], op->dat[0]->yd[j]);
		for (i=1; i<op->n_dat; i++)
		{	fprintf(file, " %f", op->dat[i]->yd[j]);
		}
		fprintf(file, "\n");
	}
 
	fclose(file);	

	return D_O_K;
}

int do_inout_load_gnuplot(void)
{
	if(updating_menu_state != 0)	
	{	active_menu->flags |=  D_DISABLED;
		return D_O_K;
	}	

	return D_O_K;
}








int	inout_main(int argc, char **argv)
{	int bool_Labview=0, i;

	if (argc>1) 
	{	for (i=1; i<argc; i++)
		{	if ( (strcasecmp(argv[i], "Labview")==0) || (strcasecmp(argv[i], "-Labview")==0) )
			{	bool_Labview=1;
			}
		}
	}
	
	
	add_item_to_menu(plot_file_import_menu,".dat binary file",			do_inout_load_data_bin,			NULL,0,NULL);
	add_item_to_menu(plot_file_import_menu,".dat ascii: full tabular", 		do_inout_load_tabular_file, 		NULL,0,NULL);
	add_item_to_menu(plot_file_import_menu,".dat ascii: 1 colomn from tabular", 	do_inout_load_column_from_tabular_file, NULL,0,NULL);
	add_item_to_menu(plot_file_import_menu,".dat GSL histogram", 			do_inout_load_histogram, 		NULL,0,NULL);
//	add_item_to_menu(plot_file_import_menu,".dat gnuplot file",			do_inout_load_gnuplot, 			NULL,0,NULL);
	add_item_to_menu(plot_file_import_menu,"\0", 							NULL,			NULL,0,NULL);
	add_item_to_menu(plot_file_import_menu,".mat MatLab 5 file",			do_inout_load_Matlab5_data,		NULL,0,NULL);
	
	if (bool_Labview==1)
	{ add_item_to_menu(plot_file_import_menu,"\0", 							NULL,			NULL,0,NULL);
	  add_item_to_menu(plot_file_import_menu,"1 channel from DT3005 acq.",		do_inout_load_DT3005_data, 		NULL,0,NULL);
	  add_item_to_menu(plot_file_import_menu,"1 channel from NI4472 acq.",		do_inout_load_NI4472_data, 		NULL,0,NULL);
	  add_item_to_menu(plot_file_import_menu,"1 channel from NI acq_mx.",		do_inout_load_NI_acq_mx_data, 		NULL,0,NULL);
	  add_item_to_menu(plot_file_import_menu,"1 channel from NI streamfloat7mx.",	do_inout_load_NI_streamfloat7mx_data, 	NULL,0,NULL);
	  add_item_to_menu(plot_file_import_menu,"1 channel from NI streamfloat8 degueu.",	do_inout_load_NI_streamfloat7mx_data, 	NULL, IS_NI_modified_SF8,NULL);
	}

	add_item_to_menu(plot_file_export_menu,".dat ascii tabular",			do_inout_export_tabular_file, 		NULL,0,NULL);
	add_item_to_menu(plot_file_export_menu,".mat MatLab 5 file",			do_inout_save_all_datasets_matlab5,	NULL,0,NULL);
	add_item_to_menu(plot_file_export_menu,".bin raw file (binary)",		do_inout_export_raw_file,		NULL,0,NULL);

	add_item_to_menu(image_file_import_menu,"raw (binary) image",			do_inout_load_raw_image, 		NULL,0,NULL);

	return D_O_K;
}



int	inout_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;
	remove_item_to_menu(plot_file_import_menu,".dat binary file",			NULL, NULL);
	remove_item_to_menu(plot_file_import_menu,".dat ascii: full tabular", 		NULL, NULL);
	remove_item_to_menu(plot_file_import_menu,".dat ascii: 1 colomn from tabular",	NULL, NULL);
	remove_item_to_menu(plot_file_import_menu,".dat GSL histogram", 		NULL, NULL);
//	remove_item_to_menu(plot_file_import_menu,".dat gnuplot file",			NULL, NULL);
	remove_item_to_menu(plot_file_import_menu,"\0", 				NULL, NULL);
	remove_item_to_menu(plot_file_import_menu,".mat MatLab 5 file",			NULL, NULL);
	remove_item_to_menu(plot_file_import_menu,"\0", 				NULL, NULL);
	remove_item_to_menu(plot_file_import_menu,"1 channel from DT3005 acq.",		NULL, NULL);
	remove_item_to_menu(plot_file_import_menu,"1 channel from NI4472 acq.",		NULL, NULL);
	remove_item_to_menu(plot_file_import_menu,"1 channel from NI acq_mx.",		NULL, NULL);
	remove_item_to_menu(plot_file_import_menu,"1 channel from NI streamfloat7mx.",		NULL, NULL);
    remove_item_to_menu(plot_file_import_menu,"1 channel from NI streamfloat8 degueu.",		NULL, NULL);

	remove_item_to_menu(plot_file_export_menu,".dat ascii tabular",			NULL, NULL);
	remove_item_to_menu(plot_file_export_menu,".mat MatLab 5 file",			NULL, NULL);
	remove_item_to_menu(plot_file_export_menu,".bin raw file (binary)",		NULL, NULL);
	
	remove_item_to_menu(image_file_import_menu,"raw (binary) image",		NULL, NULL);

	return D_O_K;
}
 
#endif 


