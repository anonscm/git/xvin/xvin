// definitions for import of Labview created files
//
// Nicolas Garnier, January 19th 2005
#ifndef _XV_LABVIEW_C_
#define _XV_LABVIEW_C_

#include "allegro.h"
#include "xvin.h"
#include "xv_tools_lib.h" // for swap_bytes

#include <stdlib.h>
#include <math.h>
#include <malloc.h>

#include "inout.h"
#include "xv_Labview.h"


#define TIME_PRECISION 0.01



int do_inout_load_DT3005_data(void)
{
	register int i, j;
	O_p *op = NULL, *opn = NULL;
	d_s 	*ds;
	pltreg 	*pr = NULL;
	FILE 	*data_file;	
	char 	fullfilename[512],filename[256],pathname[512];
	char 	info[128], *s;
	int 	N_channels, N_slabs, slab_size, N=1, N_decimate=1;
	float 	*x=NULL;
	short	*y=NULL;
	float	F_s, F_r, range=10, moyenne;
	
	if (updating_menu_state != 0)		return D_O_K;	
	
	if (key[KEY_LSHIFT]) return win_printf_OK("This routine loads data corresponding to 1 channel\n"
		"of a DT3005 acquisition card data file (from LabView).\n It places it in a new plot");

 	if ( file_select_ex("Load DT3005 .dat file", fullfilename, "dat", 512, 0, 0) == 0) return D_O_K;

	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)		return win_printf_OK("cannot find data");

	
	data_file=fopen(fullfilename, "rb");
	if (data_file==NULL) win_printf_OK("load_data_set: error opening file %s",fullfilename);
	
	x = (float*)calloc(12,sizeof(float));
	i = fread(x, sizeof(float), 12, data_file);
	if (i!=12) {free(x); fclose(data_file); return(win_printf_OK("error in specified file!")); }

#ifndef MAC_POWERPC
	swap_bytes(x, 12, sizeof(float));
#endif

	sprintf(info,"%02.0f/%02.0f/%02.0f, %2.0f:%2.0f:%2.0f",x[0],x[1],x[2], x[3],x[4],x[5]);


	N_channels=(int)x[7];	N_slabs=(int)x[8];
	slab_size =(int)x[9];	F_s=x[10]; 		F_r=x[11];
	free(x);

	extract_file_name( filename,    256, fullfilename);
	extract_file_path( pathname,	512, fullfilename);
	s=(char*)calloc(512,sizeof(char));
	sprintf(s,"In file %s, there are:\n %d channels\n Which channel do you want (in [1..%d]) ? %%d\n\n\n"
		"f_{acq} is %g Hz and f_{scan} is %3.0f Hz\n"
		"There are %d slabs of %d points, so %d points\n "
		"keep 1 point every N points (decimate) N=? %%d\n\n\n"
		"16bits were used. range was ([-X; +X] Volts), what was X ? %%f",
		filename, N_channels, N_channels, F_s, F_r, N_slabs, slab_size, N_slabs*slab_size);
	if (win_scanf(s,&N,&N_decimate,&range)==WIN_CANCEL) { fclose(data_file); return(D_O_K); }
	if ( (N<1) || (N>N_channels) )	{ fclose(data_file); return(win_printf_OK("incorrect channel number!")); }
	N--; // channel number is not in C, but in french counting convention!
	if (N_decimate<1) return(win_printf_OK("Velly bad decimazion !"));
	if (N_slabs*slab_size/N_decimate>1000000) 
	{	sprintf(s,"You selected more than a million of points (%d)\n"
			"That may be too much for the computer\nClick WIN_CANCEL to abort",
			N_slabs*slab_size/N_decimate);
		if (win_scanf(s)==WIN_CANCEL)	
		{ 	free(s); s=NULL;
			fclose(data_file); 
			return(D_O_K); 
		}
	}
	free(s);s=NULL;
	y =(short*)calloc(N_channels, sizeof(short));

	if ((opn = create_and_attach_one_plot(pr, N_slabs*slab_size/N_decimate, N_slabs*slab_size/N_decimate, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	ds = opn->dat[0];

	for (j=0; j<N_slabs*slab_size/N_decimate; j++)
	{	moyenne=0;
		for (i=0; i<N_decimate; i++) 
		{	fread(y, sizeof(short), N_channels, data_file);
#ifndef MAC_POWERPC
	        	swap_bytes(y, N_channels, sizeof(short));
#endif
			moyenne+=y[N];
		}
		ds->yd[j] = (moyenne/N_decimate)*(range*2/(256*256));
		ds->xd[j] = j*N_decimate/F_s;
	}
	free(y);

	fclose(data_file);

	opn->filename   = strdup(filename);
	opn->dir 	= Mystrdup(pathname);		
	set_ds_source(ds, "DT3005 acquisition %s %s\nchannel %d/%d, f_{acq}=%gHz\n%d slabs of %d points\n"
			"decimation: 1/%d points\n16bits in [-%g +%g]", 
			filename, info, N+1, N_channels, F_s, N_slabs, slab_size, N_decimate, range, range);
	set_plot_title(opn, "%s %s", filename, info);
	set_plot_x_title(opn, "time (s)");
	set_plot_y_title(opn, "channel %d/%d (V)", N+1, N_channels);

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} /* ens of do_inout_load_DT3005_data() */





/************************************************************************/
/* this functions loads a file created by Labview (big endian) and		*/
/* and containing data acquired through a NI4472 card					*/
/* several channels can be loaded simultaneously						*/
/*																		*/
/* it uses function get_header_informations_NI4472() coded below		*/
/************************************************************************/
int do_inout_load_NI4472_data(void)
{
	register int i, j;
	O_p *op = NULL, *opn = NULL;
	d_s 	*ds;
	pltreg 	*pr = NULL;
	FILE 	*data_file=NULL;	
static char 	fullfilename[512];
	char	filename[256],pathname[512],*s=NULL;
	long 	N=1;
static int N_decimate=1;
	int		N_channels=1, n_chan, i_channel=1;
	int 	*channel_index;
	float	f_acq, F_r;
	float	*moyenne;
	float	*tampon_float;
	char	*channel_string, *channel_cfg, *user_header;
	char 	*date_stamp;
	long	current_position;
static int do_scan_all=1;
static size_t j_start, j_end;
static	char	selected_channel_string[32]="1,2";
	
	if (updating_menu_state != 0)		return D_O_K;	
	
	if (key[KEY_LSHIFT]) return win_printf_OK("This routine loads data corresponding to 1 channel\n"
		"of a NI4472 acquisition card data file (from LabView).\n It and places it in a new plot");

 	if ( file_select_ex("Load NI4472 file", fullfilename, "", 512, 0, 0) == 0) return D_O_K;

	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)		return win_printf_OK("cannot find data");

	current_position=get_header_informations_NI4472(fullfilename, &channel_string, &channel_cfg, &N_channels, &f_acq, &F_r,
								&date_stamp, &user_header, &N);
	if (current_position<0)		return(win_printf_OK("Error reader file header, aborting."));
	if (N_channels==1) sprintf(selected_channel_string,"1"); // fatalement, n'est-ce pas ?
	
	// begin copy 2006
	
	extract_file_name( filename,    256, fullfilename);
	extract_file_path( pathname,	512, fullfilename);
	s=my_sprintf(s,"File %s, created %s on a NI4472\n%s\nchannel names: %s\nchannel config: %s\n"
		"{\\color{lightgreen}f_{acq} is %g Hz} and f_{scan} is %3.0f Hz\n\n\n"
		"How many channels were there ?%%2d (I found %d ones)\nWhich channel(s) do you want ?  %%s\n\n"
		"There are %d points per channels\n"
		"There are %d points in total (over all channels)\n\n"
		"keep 1 point every %%4d points (decimate)\n"
		"scan %%R part of the file / %%r all the file",
		filename, date_stamp, user_header, channel_string, channel_cfg, 
        f_acq, F_r, N_channels, (int)(N/N_channels), (int)N);
	i=win_scanf(s, &N_channels, &selected_channel_string, &N_decimate, &do_scan_all);
	free(s); s=NULL;
	if (i==WIN_CANCEL) return(D_O_K);
		
	channel_index = str_to_index(selected_channel_string, &n_chan);	// malloc is done by str_to_index
	if ( (channel_index==NULL) || (n_chan<1) )	return(win_printf_OK("bad values for channels numbers !"));
	
	for (i_channel=0; i_channel<n_chan; i_channel++)
	{	if ( (channel_index[i_channel]<1) || (channel_index[i_channel]>N_channels) ) 
			return(win_printf_OK("incorrect channel number !")); 
	}
	if (N % N_channels !=0) //useless test, this is perform when reading the header!
		return(win_printf_OK("number of points in file %d is not a multiple of channel number %d !",N,N_channels));
	N=N/N_channels;
	if (N_decimate<1) 
		return(win_printf_OK("Very bad decimation !"));
	
	if (do_scan_all==0)
	{	s=my_sprintf(s,"There are %d points in time, for %d different channels\n"
			"(%d points in total)\n"
			"decimation by a factor %d will keep {\\color{red}%d points in time}\n\n"
			"load points in the interval [A B[ with\n"
			"A = %%8d (0<=A<%d)\n"
			"B = %%8d (0<B<=%d)", 
            (int)N, N_channels, (int)N*N_channels, N_decimate, 
            (int)N/N_decimate, (int)N/N_decimate, (int)N/N_decimate);
		win_scanf(s, &j_start, &j_end);
		free(s); s=NULL;
		if ( (j_start<0) || (j_start>=N/N_decimate) )
			return(win_printf_OK("bad interval!"));
	}
	else
	{	j_start = 0;
		j_end   = N/N_decimate;
	}

	if ((j_end-j_start)>1000000) 
	if (win_printf_OK("You are about to keep more than a million of points\n"
			"That may be too much for the computer\nClick WIN_CANCEL to abort")==WIN_CANCEL)	
		return(D_O_K);

    data_file=fopen(fullfilename, "rb");
	fseek(data_file, current_position, SEEK_SET); // we move from the beginning of the file, to the end of the header
	fseek(data_file, j_start*N_decimate*N_channels*sizeof(float), SEEK_CUR);

	tampon_float =(float*)calloc(N_channels, sizeof(float)); // size = number of channels in file
	moyenne      =(float*)calloc(n_chan,     sizeof(float)); // size = number of channels to load

	if ((opn = create_and_attach_one_plot(pr, (j_end-j_start), (j_end-j_start), 0)) == NULL)
		return(win_printf_OK("cannot create plot !"));
	ds = opn->dat[0]; // this is the first channel to load, there is at least one !
	for (i_channel=1; i_channel<n_chan; i_channel++) // additional channels to be loadeed are to be put in additional datasets
	{	if ((ds = create_and_attach_one_ds(opn, (j_end-j_start), (j_end-j_start), 0)) == NULL)	
			return(win_printf_OK("cannot create dataset !"));
	}

	for (j=j_start; j<j_end; j++)
	{	for (i_channel=0; i_channel<n_chan; i_channel++) moyenne[i_channel]=0.;
		for (i=0; i<N_decimate; i++) 
		{	fread(tampon_float, sizeof(float), N_channels, data_file);		
#ifndef MAC_POWERPC
	        swap_bytes(tampon_float, N_channels, sizeof(float));
#endif
			for (i_channel=0; i_channel<n_chan; i_channel++) moyenne[i_channel] += tampon_float[channel_index[i_channel]-1];
		}
		for (i_channel=0; i_channel<n_chan; i_channel++)
		{	opn->dat[i_channel]->yd[j-j_start] = moyenne[i_channel]/(float)N_decimate;
			opn->dat[i_channel]->xd[j-j_start] = (float)j*(float)N_decimate/(float)f_acq;
		}
	}

	free(moyenne);
	free(tampon_float);
	fclose(data_file);
	
	opn->filename = strdup(filename);
	opn->dir 	  = Mystrdup(pathname);
	for (i_channel=0; i_channel<n_chan; i_channel++) set_ds_source(opn->dat[i_channel], 
			"NI4472 acquisition, %s\n%s\nchannel %d/%d, f_{acq}=%gHz\n%s\ndecimation 1/%d points", 
			filename, date_stamp, channel_index[i_channel], N_channels, f_acq, user_header, N_decimate);
				
	set_plot_title(opn, "%s %s", filename, date_stamp);
	set_plot_x_title(opn, "time (s)");
	set_plot_y_title(opn, "channel %s/%d (V)", selected_channel_string, N_channels);

	// following lines creates a label not attach to a dataste, but to the plot itself:
	s=my_sprintf(s,"\\fbox{\\stack{{%s %s}{%s}}}", filename, date_stamp, user_header); 
	push_plot_label(opn, opn->dat[0]->xd[(j_end-j_start)/2], opn->dat[0]->yd[0], s, USR_COORD);
	free(s); 

	free(channel_cfg);
	free(channel_string);
	free(user_header);
	free(date_stamp);

	refresh_plot(pr, UNCHANGED);
	return(D_O_K);
} // end of 'do_inout_load_NI4472_data()'





/************************************************************************/
/* this function returns the file position in a file created by 		*/
/* Labview (big endian) and containing data acquired through a 			*/
/* NI4472 card															*/
/*																		*/
/* it uses function get_header_informations_NI4472() coded below		*/
/*																		*/
/* returns D_O_K on success, -1 on failure								*/
/************************************************************************/
int read_header_NI4472(char *filename, long *position_in_file, long *size_of_data_in_floats, float *f_acq)
{	float	F_r;
	int		N_channels=1;
	char	*channel_string, *channel_cfg, *date_stamp, *user_header;
	
	*position_in_file=get_header_informations_NI4472(filename, &channel_string, &channel_cfg, &N_channels, f_acq, &F_r,
								&date_stamp, &user_header, size_of_data_in_floats);
	if (*position_in_file<0)		
	{	win_printf_OK("read_header_NI4472: error opening file %s",filename);
		return(-1);
	}
	
	return(D_O_K);
}// end of read_header_NI4472()





/************************************************************************/
/* this function returns all available information a file created by 	*/
/* Labview (big endian) and containing data acquired through a 			*/
/* NI4472 card															*/
/*																		*/
/* returns the file position after the header (on success)				*/
/* returns -1 on any failure											*/
/************************************************************************/
long get_header_informations_NI4472(char *filename, char **channel_string, char **channel_cfg, int *N_channels, float *F_s, float *F_r,
									char **date_stamp, char **user_header, long *N)
{
	FILE 	*data_file;
	int		header_length, user_header_length, channel_string_length, channel_cfg_length;
	int		*tampon_int;
	float	*tampon_float;
	long	current_position;
	
	data_file=fopen(filename, "rb");
	if (data_file==NULL) 
	{	win_printf_OK("read_header_NI4472: error opening file %s",filename);
		return(-1);
	}

	*N_channels=1;

			
	tampon_int = (int*)calloc(2,sizeof(int));
	if (fread(tampon_int, sizeof(int), 2, data_file)!=2) 
	{	free(tampon_int); fclose(data_file); win_printf("error reading the file!\n(1st and 2nd elements)"); return(-1); 
	}

#ifndef MAC_POWERPC
	swap_bytes(tampon_int, 2, sizeof(int));
#endif
	header_length=tampon_int[0];
	channel_string_length=tampon_int[1];

	*channel_string = (char*)calloc(channel_string_length+1,sizeof(char));
	if (channel_string_length>0)
	if (fread(*channel_string, sizeof(char), channel_string_length, data_file)!=channel_string_length) 
	{	free(*channel_string); fclose(data_file); win_printf("error reading the file!\n(3rd element)"); return(-1); 
	}
	(*channel_string+channel_string_length)[0]='\0';
	*N_channels=(int)( (channel_string_length+2)/3 ); // channel_string is of the form '0, 1, 2' so, there is a magic formula... 

	fread(tampon_int, sizeof(int), 1, data_file);

#ifndef MAC_POWERPC
	swap_bytes(tampon_int, 1, sizeof(int));
#endif
	channel_cfg_length=tampon_int[0];

	*channel_cfg = (char*)calloc(channel_cfg_length+1,sizeof(char));
	if (channel_cfg_length>0)
	if (fread(*channel_cfg, sizeof(char), channel_cfg_length, data_file)!=channel_cfg_length) 
	{	free(*channel_cfg); fclose(data_file); win_printf("error reading the file!\n(5th element)"); return(-1); 
	}
	(*channel_cfg+channel_cfg_length)[0]='\0';

	tampon_float = (float*)calloc(2,sizeof(float));
	if (fread(tampon_float, sizeof(float), 2, data_file)!=2) 
	{	free(tampon_float); fclose(data_file); win_printf("error reading the file!\n(acquisition frequencies)"); return(-1); 
	}

#ifndef MAC_POWERPC
	swap_bytes(tampon_float, 2, sizeof(float));
#endif
	*F_s=tampon_float[0];
	*F_r=tampon_float[1];

	*date_stamp = (char*)calloc(19+1,sizeof(char));
	if (fread(*date_stamp, sizeof(char), 19, data_file)!=19) 
	{	fclose(data_file); win_printf_OK("error reading the file!\n(date stamp)"); return(-1); 
	}
	(*date_stamp+19)[0]='\0';

	
	fread(tampon_int, sizeof(int), 1, data_file);
#ifndef MAC_POWERPC
	swap_bytes(tampon_int, 1, sizeof(int));
#endif
	user_header_length=tampon_int[0];

	*user_header = (char*)calloc(user_header_length+1,sizeof(char));
	if (fread(*user_header, sizeof(char), user_header_length, data_file)!=user_header_length) 
	{	free(*user_header); fclose(data_file); win_printf_OK("error reading the file!\n(user header)"); return(-1); 
	}
	(*user_header+user_header_length)[0]='\0';

	free(tampon_int);
	free(tampon_float);	
	
	current_position = ftell(data_file);	// this is the position just after the header
	fseek(data_file, 0, SEEK_END);
	*N = (long)((ftell(data_file)-current_position)/sizeof(float));	// number of datapoints
	fclose(data_file);
	
	// some tests now :
	if ( (*N_channels<1)  ) 
	{	win_printf("incorrect channel number !"); 
		return(-1); 
	}
/*	if ((*N) % (*N_channels) !=0)
	{ 	win_printf_OK("number of points in file %d is not a multiple of channel number %d !",N,N_channels); 
		return(-1); 
	}
*/	
	// some work on the acquisition frequency : 
	if ( ((*F_s) - floorf(*F_s)) != 0)
	{	header_length=win_printf("Sampling frequency is indicated in the header to be %g Hz\n"
				"This is not an integer, which puzzles me\n\n"
				"click OK to use an integer, or WIN_CANCEL to keep the float value", (*F_s));
		if (header_length!=WIN_CANCEL) (*F_s) = floorf(*F_s);
	}
	
	return(current_position);
}// end of read_header_NI4472()





/************************************************************************/
/* this function creates a header containing exactly the same 			*/
/* information as a file created by Labview (big endian) and containing */
/* data in floats, as acquired through a NI4472 card					*/
/*																		*/
/* returns the file position after the header (on success)				*/
/* returns -1 on any failure											*/
/************************************************************************/
long write_header_informations_NI4472(char *filename, char *channel_string, char *channel_cfg, int N_channels, float F_s, float F_r,
									char *date_stamp, char *user_header, long N)
{	FILE 	*file_out;
	long	current_position;
	float	*tampon_float;
	int		*tampon_int;

	file_out=fopen(filename, "wb");
	
	tampon_int = (int*)calloc(2,sizeof(int));
	tampon_int[0] = 0;	// this sould be the total header length, but we don't know it yet !
	tampon_int[1] = strlen(channel_string);

#ifndef MAC_POWERPC
	swap_bytes(tampon_int, 2, sizeof(int));
#endif
	fwrite(tampon_int, sizeof(int), 2, file_out);
	fwrite(channel_string, sizeof(char), strlen(channel_string), file_out);

	tampon_int[0]=strlen(channel_cfg);

#ifndef MAC_POWERPC
	swap_bytes(tampon_int, 1, sizeof(int));
#endif
	fwrite(tampon_int, sizeof(int), 1, file_out);
	fwrite(channel_cfg, sizeof(char), strlen(channel_cfg), file_out);
	
	tampon_float = (float*)calloc(2,sizeof(float));
	tampon_float[0] = F_s;
	tampon_float[1] = F_r;

#ifndef MAC_POWERPC
	swap_bytes(tampon_float, 2, sizeof(float));
#endif
	fwrite(tampon_float, sizeof(float), 2, file_out);
	fwrite(date_stamp, sizeof(char), 19, file_out);
	
	
	tampon_int[0]=strlen(user_header);
#ifndef MAC_POWERPC
	swap_bytes(tampon_int, 1, sizeof(int));
#endif
	fwrite(tampon_int, sizeof(int), 1, file_out);
	fwrite(user_header, sizeof(char), strlen(user_header), file_out);

	current_position = ftell(file_out);	// this is the position just after the header
	
	// now we correct the header length : 
	fseek(file_out, 0, SEEK_SET);
	tampon_int[0] = (int)current_position;
#ifndef MAC_POWERPC
	swap_bytes(tampon_int, 1, sizeof(int));
#endif
	fwrite(tampon_int, sizeof(int), 1, file_out);
	
	
	fclose(file_out);
	free(tampon_int);
	free(tampon_float);
	
	return(current_position);
} /* end of function 'write_header_informations_NI4472' */





/************************************************************************/
/* this functions loads an array of float from a file created 	*/
/* by Labview (big endian) and containing data acquired with a NI4472 card			*/
/************************************************************************/
int load_NI4472_data(char *filename, long header_length, // the header size 
		int N,		 // total number of floats
		int N_channels, // number of different channels
		int i_channel,  // index of channel to load (ranging in [1..N_channels]
		int N_decimate, // decimation: keep 1 point every N_decimate (the average is kept)
		float **x)	 // the loaded data, pointer is allocated by the function itself
{
	register int i, j;
	FILE 	*data_file;	
	float	moyenne;
	float	*tampon_float;
	

	if ( (N_channels<1) || (i_channel<1) || (i_channel>N_channels) ) 
		return(win_printf_OK("incorrect channel number !"));
	if (N % N_channels !=0)
		return(win_printf_OK("number of points in file %d is not a multiple of channel number %d !",N,N_channels));
	N=N/N_channels;
	if ( (N_decimate<1) || (N%N_decimate!=0) )
		return(win_printf_OK("Very bad decimation !"));
	
	data_file=fopen(filename, "rb");
	if (data_file==NULL) 
		return(win_printf_OK("load_data_set: error opening file %s",filename));
	fseek(data_file, header_length, SEEK_SET);
	
	*x				= (float*)calloc(N/N_decimate,	sizeof(float));
	tampon_float 	= (float*)calloc(N_channels,	sizeof(float));
	for (j=0; j<N/N_decimate; j++)
	{	moyenne=0;
		for (i=0; i<N_decimate; i++) 
		{	fread(tampon_float, sizeof(float), N_channels, data_file);
#ifndef MAC_POWERPC
	        swap_bytes(tampon_float, N_channels, sizeof(float));
#endif
			moyenne += tampon_float[i_channel-1];
		}
		(*x)[j] = (float)moyenne/N_decimate;
	}

	free(tampon_float);
	fclose(data_file);

	return(N/N_decimate);
} // end of 'load_NI4472_data()'




/************************************************************************/
/* this functions loads an array of float from a file created 	*/
/* by Labview (big endian) and containing data acquired with a NI4472 card			*/
/************************************************************************/
int decimate_NI_file(char *filename_in, int file_type,
		int N_channels, // number of different channels
		int N_decimate, // decimation: keep 1 point every N_decimate (the average is kept)
		char *filename_out)	 // the loaded data, pointer is allocated by the function itself
{
	register int i, j, k;
	FILE 	*file_in, *file_out;	
	float	*moyenne;
	float	*tampon_float;
	char	*channel_string, *channel_cfg, *date_stamp, *user_header;
	long 	header_length_in=-1, header_length_out=-1;
	float	F_s, F_r;
	long    N;
	int		N_channels_found;
	
	if (strcmp(filename_in, filename_out)==0)
	{	win_printf("input and output filenames are identical !");
		return(-1);
	}
	
	// load infos from input file:
	if (file_type==IS_NI_4472_FILE)	
	{			header_length_in=get_header_informations_NI4472(filename_in, 
					&channel_string, &channel_cfg, &N_channels_found, &F_s, &F_r, &date_stamp, &user_header, &N);
	}
	else if (file_type==IS_NI_DAQmx_FILE)	
	{			header_length_in=get_header_informations_DAQMX(filename_in, 
					&channel_string, &N_channels_found, &F_s, &date_stamp, &user_header, &N);
					// N is the total number of points 
	}
	else 
	{	win_printf("undetermined file type !");
		return(-1);
	}
	if (header_length_in<0) return(win_printf_OK("error looking at (input) file %s\nthis file may not exist...", filename_in));

	// first, some tests:
	if (N_channels_found!=N_channels)
	{	if (win_printf("header indicates %d channels, but you told me %d\n\n"
						"click OK to continue with your value, or WIN_CANCEL to exit")==WIN_CANCEL);
		return(-1);
	}
	if ( (N_channels<1) || ( (N%N_channels) !=0) )
	{	win_printf("number of points in file %d is not a multiple of channel number %d !",N,N_channels);
		return(-1);
	}
	if ( (N_decimate<1) || ( ( (N/N_channels)%N_decimate )!=0) )
	{	win_printf("Bad decimation !");
		return(-1);
	}
	
	// modify the user header : 	
	user_header = my_sprintf(user_header, " decimated by %d", N_decimate);
	F_s /= N_decimate;
	F_r /= N_decimate;
	N   /= N_decimate;
	
	// then write the new header:
	if (file_type==IS_NI_4472_FILE)	
	{			header_length_out = write_header_informations_NI4472(filename_out, 
					channel_string, channel_cfg, N_channels, F_s, F_r, date_stamp, user_header, N);
	}
	else if (file_type==IS_NI_DAQmx_FILE)	
	{			header_length_out = write_header_informations_DAQMX(filename_out, 
					channel_string, N_channels, F_s, date_stamp, user_header, N);
	}
	
	// some cleaning:	
	free(channel_string);	free(channel_cfg);
	free(date_stamp);		free(user_header);

	// now the real stuff:
	file_in      = fopen(filename_in,  "rb"); fseek(file_in , header_length_in,  SEEK_SET);
	file_out     = fopen(filename_out, "ab"); fseek(file_out, header_length_out, SEEK_SET);
	tampon_float = (float*)calloc(N_channels,	sizeof(float));
	moyenne	 	 = (float*)calloc(N_channels,	sizeof(float));
	for (j=0; j<(N/N_channels); j++) // N is the total nb of points after decimation
	{	for (k=0; k<N_channels; k++) moyenne[k]=(float)0.;
		for (i=0; i<N_decimate; i++) 
		{	fread(tampon_float, sizeof(float), N_channels, file_in);
#ifndef MAC_POWERPC
	        swap_bytes(tampon_float, N_channels, sizeof(float));
#endif
			for (k=0; k<N_channels; k++)
				moyenne[k] += tampon_float[k];
		}
		for (k=0; k<N_channels; k++) moyenne[k] /= N_decimate;
#ifndef MAC_POWERPC
	    swap_bytes(moyenne, N_channels, sizeof(float));
#endif
		fwrite(moyenne, sizeof(float), N_channels, file_out);
	}

	free(tampon_float); free(moyenne);
	fclose(file_in);
	fclose(file_out);

	return(N/N_decimate);
} // end of 'decimate_NI4472_file()'







/************************************************************************/
/* this functions loads an array of float from a file created 	*/
/* by Labview (big endian) and containing data acquired with a NI4472 card			*/
/************************************************************************/
int extract_NI_file(char *filename_in, int file_type,
		int N_channels, // number of different channels
		int i_channel, // number of channel of interest
		int N_decimate, // decimation: keep 1 point every N_decimate (the average is kept) 
		float factor, // multiply data by this factor
		float offset, // add an offset after multiplication by the factor
		char *filename_out)	 // the loaded data, pointer is allocated by the function itself
{
	register int i, j;
	FILE 	*file_in, *file_out;	
	float	*moyenne;
	float	*tampon_float;
	char	*channel_string, *channel_cfg, *date_stamp, *user_header;
	long 	header_length_in=-1;
	float	F_s, F_r;
	long    N;
	int		N_channels_found;
	
	if (strcmp(filename_in, filename_out)==0)
	{	win_printf("input and output filenames are identical !");
		return(-1);
	}
	
	// load infos from input file:
	if (file_type==IS_NI_4472_FILE)	
	{			header_length_in=get_header_informations_NI4472(filename_in, 
					&channel_string, &channel_cfg, &N_channels_found, &F_s, &F_r, &date_stamp, &user_header, &N);
	}
	else if (file_type==IS_NI_DAQmx_FILE)	
	{			header_length_in=get_header_informations_DAQMX(filename_in, 
					&channel_string, &N_channels_found, &F_s, &date_stamp, &user_header, &N);
					// N is the total number of points 
	}
	else 
	{	win_printf("undetermined file type !");
		return(-1);
	}
	if (header_length_in<0) return(win_printf_OK("error looking at (input) file %s\nthis file may not exist...", filename_in));

	// first, some tests:
	if (N_channels_found!=N_channels)
	{	if (win_printf("header indicates %d channels, but you told me %d\n\n"
						"click OK to continue with your value %d, or WIN_CANCEL to exit",
                        N_channels, N_channels_found, N_channels)==WIN_CANCEL);
		return(-1);
	}
	if ( (N_channels<1) || ( (N%N_channels) !=0) )
	{	win_printf("number of points in file %d is not a multiple of channel number %d !",N,N_channels);
		return(-1);
	}
	if ( (N_decimate<1) || ( ( (N/N_channels)%N_decimate )!=0) )
	{	win_printf("Bad decimation !");
		return(-1);
	}
	if ( (i_channel<0) || (i_channel>=N_channels) )
	{	win_printf("you choose an incorrect channel number!");
		return(-1);
	}
	
	N   /= N_decimate;
	
	// now the real stuff:
	file_in      = fopen(filename_in,  "rb"); fseek(file_in , header_length_in,  SEEK_SET);
	file_out     = fopen(filename_out, "wb"); 
	tampon_float = (float*)calloc(N_channels,	sizeof(float));
	moyenne	 	 = (float*)calloc(1,          	sizeof(float));
	for (j=0; j<(N/N_channels); j++) // N is the total nb of points after decimation
	{	moyenne[0]=(float)0.;
		for (i=0; i<N_decimate; i++) 
		{	fread(tampon_float, sizeof(float), N_channels, file_in);
#ifndef MAC_POWERPC
	        swap_bytes(tampon_float, N_channels, sizeof(float));
#endif
			moyenne[0] += tampon_float[i_channel]; //  we are only interested in channel number i_channel
		}
		moyenne[0] /= (float)N_decimate;
		moyenne[0] *= factor;		
		moyenne[0] += offset;
#ifdef XV_MAC
#ifdef MAC_POWERPC
		swap_bytes(moyenne, 1, sizeof(float));
#endif		
#endif
		fwrite(moyenne, sizeof(float), 1, file_out); 
	}

	free(tampon_float); free(moyenne);
	fclose(file_in);
	fclose(file_out);

	return(N/N_decimate);
} // end of 'extract_NI4472_file()'







/************************************************************************/
/* this function returns the file position in a file created by 		*/
/* Labview (big endian) and containing data acquired with either		*/
/* - acq_mx.vi															*/
/* - streamfloat7mx.vi													*/
/*																		*/
/* note both the above .vi uses DAQ/MX 									*/
/************************************************************************/
long get_header_informations_DAQMX(char *filename, char **channel_string, int *N_channels, float *F_s, 
								char **date_stamp, char **user_header, long *N)
{
	FILE 	*data_file;
	int		channel_string_length, date_length, user_header_length;
    size_t  sample_number;
	long	current_position;
	size_t  Nl;
	int		*tampon_int;
	size_t  *tampon_size_t;
	double	*tampon_double;
	char    *texte=NULL;
static int bool_N=0;
	
	data_file=fopen(filename, "rb");
	if (data_file==NULL) 
	{	win_printf_OK("get_header_informations_DAQMX:\n error opening file %s", filename);
		return(-1);
	}

	*N_channels=1; // value by default

	tampon_int = (int*)calloc(2,sizeof(int));
	if (fread(tampon_int, sizeof(int), 1, data_file)!=1) 
	{	free(tampon_int); fclose(data_file); win_printf_OK("get_header_informations_DAQMX:\n"
														"error reading the file!\n(1st element)"); return(-1); 
	}
#ifndef MAC_POWERPC
	swap_bytes(tampon_int, 1, sizeof(int));
#endif
	channel_string_length=tampon_int[0];

	*channel_string = (char*)calloc(channel_string_length+1,sizeof(char));
	if (channel_string_length>0)
	if (fread(*channel_string, sizeof(char), channel_string_length, data_file)!=channel_string_length) 
	{	free(*channel_string); fclose(data_file); return(win_printf_OK("get_header_informations_DAQMX:\n"
									"error reading the file!\n(2nd element)")); 
	}
	// old version : *channel_string[channel_string_length]='\0';
	(*channel_string+channel_string_length)[0]='\0';
		
	tampon_double = (double*)calloc(3,sizeof(double));
	fread(tampon_double, sizeof(double), 3, data_file);
#ifndef MAC_POWERPC
	swap_bytes(tampon_double, 3, sizeof(double));
#endif
	*N_channels = (int)(tampon_double[1]);
	*F_s        = (float)(tampon_double[2]);
		
	current_position = ftell(data_file);	// this is the current position
	fseek(data_file, current_position + (*N_channels*(int)(tampon_double[0])-1)*sizeof(double), SEEK_SET);
	

	if (fread(tampon_int, sizeof(int), 1, data_file)!=1) 
	{	free(tampon_int); fclose(data_file); win_printf_OK("error reading the file!\n(date length)"); return(-1); 
	}
#ifndef MAC_POWERPC
	swap_bytes(tampon_int, 1, sizeof(int));
#endif
	date_length = tampon_int[0]; 
	*date_stamp  = (char*)calloc(date_length+1,sizeof(char));
	if (fread(*date_stamp, sizeof(char), date_length, data_file)!= date_length) 
	{	free(tampon_int); fclose(data_file); win_printf_OK("error reading the file!\n(date stamp)"); return(-1); 
	}
	(*date_stamp+date_length)[0]='\0';
	
	
	fread(tampon_int, sizeof(int), 1, data_file);
#ifndef MAC_POWERPC
	swap_bytes(tampon_int, 1, sizeof(int));
#endif
	user_header_length=tampon_int[0];

	*user_header = (char*)calloc(user_header_length+1,sizeof(char));
	if (fread(*user_header, sizeof(char), user_header_length, data_file)!=user_header_length) 
	{	free(*user_header); fclose(data_file); win_printf_OK("error reading the file!\n(user header)"); return(-1); 
	}
	(*user_header+user_header_length)[0]='\0';
	
	tampon_size_t = (size_t*)calloc(1,sizeof(size_t));
	fread(tampon_size_t, sizeof(size_t), 1, data_file);
#ifndef MAC_POWERPC
	swap_bytes(tampon_size_t, 1, sizeof(size_t));
#endif
	sample_number = tampon_size_t[0]; 
	
	free(tampon_int);
	free(tampon_double);
	free(tampon_size_t);

		
// chan_length=fread(fid,1,'int32');
// channels=fread(fid,chan_length,'uint8=>char');
// coeff_numb=fread(fid,1,'double');
// chan_numb=fread(fid,1,'double');
// coeff=fread(fid,coeff_numb*chan_numb,'double');
// date_length=fread(fid,1,'int32');
// dateStamp=fread(fid,date_length,'uint8=>char');
// userHeader_length=fread(fid,1,'int32');
// userHeader=fread(fid,userHeader_length,'uint8=>char');
// Samples_number=fread(fid,1,'int32');
// data=fread(fid,inf,'double');

	current_position = ftell(data_file);	// this is the position just after the header
	fseek(data_file, 0, SEEK_END);
	Nl = (size_t)((ftell(data_file)-current_position)/sizeof(float));	// number of datapoints
	*N = Nl;
	fseek(data_file, current_position, SEEK_SET);
	
	fclose(data_file);
	
	// some tests now :
	if ( (*N_channels<1)  ) 
	{	win_printf_OK("incorrect channel number !"); 
		return(-1); 
	}
	if ((*N) % (*N_channels) !=0)
	{ 	win_printf_OK("number of points in file %d is not a multiple of channel number %d !",N,N_channels); 
		return(-1); 
	}
	
	if (((*N)/(*N_channels))!= sample_number)
	{	texte = my_sprintf(texte, "I detected %u floats in the file, and because there are %d channels,\n"
				"this corresponds to %u floats per channel. But header indicates %u points\n\n"
				"This may be due to points being doubles instead of floats.\n"
				"Please select the number of points (per channel):\n"
                "%%R %u points (floats)\n"
                "%%r %u points (doubles)\n"
                "%%r %u points ({\\color{lightred}dangerous!!!})\n" 
                "or click WIN_CANCEL to abort loading of the file.", 
				*N, *N_channels, (Nl/(*N_channels)), sample_number, (Nl/(*N_channels)), (Nl/(*N_channels))/2, sample_number);
		if (win_scanf(texte, &bool_N)==WIN_CANCEL)	return(-1);
		
		if (bool_N==0) Nl = Nl;
		if (bool_N==1) Nl /= 2;
		if (bool_N==2) Nl = sample_number*(*N_channels);
            
        (*N) = Nl;
	}
	
	// some work on the acquisition frequency : 
	if ( fabs((*F_s) - floorf(*F_s)) >= TIME_PRECISION)
	{	date_length=win_printf("Sampling frequency is indicated in the header to be %g Hz\n"
				"This is not an integer, which puzzles me\n\n"
				"click OK to use an integer, or WIN_CANCEL to keep the float value", (*F_s));
		if (date_length!=WIN_CANCEL) (*F_s) = floorf(*F_s);
	}
	
	return(current_position);
}// end of get_header_informations_DAQMX()









/************************************************************************/
/* this function returns the file position in a file created by 		*/
/* Labview (big endian) and containing data acquired with either		*/
/* a modified version of streamfloat8mx.vi								*/
/*																		*/
/* note both the above .vi uses DAQ/MX 									*/
/************************************************************************/
long get_header_informations_DAQMX_modified(char *filename, char **channel_string, int *N_channels, float *F_s, 
								char **date_stamp, char **user_header, long *N)
{
	FILE 	*data_file;
	int	 // channel_string_length, 
            date_length, user_header_length;
    size_t  sample_number;
	long	current_position;
	size_t  Nl;
	int		*tampon_int;
	size_t  *tampon_size_t;
	double	*tampon_double;
	char    *texte=NULL;
static int bool_N=0;
	
	data_file=fopen(filename, "rb");
	if (data_file==NULL) 
	{	win_printf_OK("get_header_informations_DAQMX:\n error opening file %s", filename);
		return(-1);
	}

	*N_channels=1; // value by default


	tampon_int = (int*)calloc(2,sizeof(int));
/*	if (fread(tampon_int, sizeof(int), 1, data_file)!=1) 
	{	free(tampon_int); fclose(data_file); win_printf_OK("get_header_informations_DAQMX:\n"
														"error reading the file!\n(1st element)"); return(-1); 
	}
#ifndef MAC_POWERPC
	swap_bytes(tampon_int, 1, sizeof(int));
#endif
	channel_string_length=tampon_int[0];

	*channel_string = (char*)calloc(channel_string_length+1,sizeof(char));
	if (channel_string_length>0)
	if (fread(*channel_string, sizeof(char), channel_string_length, data_file)!=channel_string_length) 
	{	free(*channel_string); fclose(data_file); return(win_printf_OK("get_header_informations_DAQMX:\n"
									"error reading the file!\n(2nd element)")); 
	}
	// old version : *channel_string[channel_string_length]='\0';
	(*channel_string+channel_string_length)[0]='\0';
*/

    *channel_string = (char*)calloc(7+1,sizeof(char));
    sprintf(*channel_string,"no info");
    (*channel_string+7)[0]='\0';
		
	tampon_double = (double*)calloc(3,sizeof(double));
	fread(tampon_double, sizeof(double), 3, data_file);
#ifndef MAC_POWERPC
	swap_bytes(tampon_double, 3, sizeof(double));
#endif
	*N_channels = (int)(tampon_double[1]);
	*F_s        = (float)(tampon_double[2]);
		
	current_position = ftell(data_file);	// this is the current position
	fseek(data_file, current_position + (*N_channels*(int)(tampon_double[0])-1)*sizeof(double), SEEK_SET);
	

	if (fread(tampon_int, sizeof(int), 1, data_file)!=1) 
	{	free(tampon_int); fclose(data_file); win_printf_OK("error reading the file!\n(date length)"); return(-1); 
	}
#ifndef MAC_POWERPC
	swap_bytes(tampon_int, 1, sizeof(int));
#endif
	date_length = tampon_int[0]; 
	*date_stamp  = (char*)calloc(date_length+1,sizeof(char));
	if (fread(*date_stamp, sizeof(char), date_length, data_file)!= date_length) 
	{	free(tampon_int); fclose(data_file); win_printf_OK("error reading the file!\n(date stamp)"); return(-1); 
	}
	(*date_stamp+date_length)[0]='\0';
	
	
	fread(tampon_int, sizeof(int), 1, data_file);
#ifndef MAC_POWERPC
	swap_bytes(tampon_int, 1, sizeof(int));
#endif
	user_header_length=tampon_int[0];

	*user_header = (char*)calloc(user_header_length+1,sizeof(char));
	if (fread(*user_header, sizeof(char), user_header_length, data_file)!=user_header_length) 
	{	free(*user_header); fclose(data_file); win_printf_OK("error reading the file!\n(user header)"); return(-1); 
	}
	(*user_header+user_header_length)[0]='\0';
	
	tampon_size_t = (size_t*)calloc(1,sizeof(size_t));
	fread(tampon_size_t, sizeof(size_t), 1, data_file);
#ifndef MAC_POWERPC
	swap_bytes(tampon_size_t, 1, sizeof(size_t));
#endif
	sample_number = tampon_size_t[0]; 
	
	free(tampon_int);
	free(tampon_double);
	free(tampon_size_t);

		
// chan_length=fread(fid,1,'int32');
// channels=fread(fid,chan_length,'uint8=>char');
// coeff_numb=fread(fid,1,'double');
// chan_numb=fread(fid,1,'double');
// coeff=fread(fid,coeff_numb*chan_numb,'double');
// date_length=fread(fid,1,'int32');
// dateStamp=fread(fid,date_length,'uint8=>char');
// userHeader_length=fread(fid,1,'int32');
// userHeader=fread(fid,userHeader_length,'uint8=>char');
// Samples_number=fread(fid,1,'int32');
// data=fread(fid,inf,'double');

	current_position = ftell(data_file);	// this is the position just after the header
	fseek(data_file, 0, SEEK_END);
	Nl = (size_t)((ftell(data_file)-current_position)/sizeof(float));	// number of datapoints
	*N = Nl;
	fseek(data_file, current_position, SEEK_SET);
	
	fclose(data_file);
	
	// some tests now :
	if ( (*N_channels<1)  ) 
	{	win_printf_OK("incorrect channel number !"); 
		return(-1); 
	}
	if ((*N) % (*N_channels) !=0)
	{ 	win_printf_OK("number of points in file %d is not a multiple of channel number %d !",N,N_channels); 
		return(-1); 
	}
	
	if (((*N)/(*N_channels))!= sample_number)
	{	texte = my_sprintf(texte, "I detected %u floats in the file, and because there are %d channels,\n"
				"this corresponds to %u floats per channel. But header indicates %u points\n\n"
				"This may be due to points being doubles instead of floats.\n"
				"Please select the number of points (per channel):\n"
                "%%R %u points (floats)\n"
                "%%r %u points (doubles)\n"
                "%%r %u points ({\\color{lightred}dangerous!!!})\n" 
                "or click WIN_CANCEL to abort loading of the file.", 
				*N, *N_channels, (Nl/(*N_channels)), sample_number, (Nl/(*N_channels)), (Nl/(*N_channels))/2, sample_number);
		if (win_scanf(texte, &bool_N)==WIN_CANCEL)	return(-1);
		
		if (bool_N==0) Nl = Nl;
		if (bool_N==1) Nl /= 2;
		if (bool_N==2) Nl = sample_number*(*N_channels);
            
        (*N) = Nl;
	}
	
	// some work on the acquisition frequency : 
	if ( fabs((*F_s) - floorf(*F_s)) >= TIME_PRECISION)
	{	date_length=win_printf("Sampling frequency is indicated in the header to be %g Hz\n"
				"This is not an integer, which puzzles me\n\n"
				"click OK to use an integer, or WIN_CANCEL to keep the float value", (*F_s));
		if (date_length!=WIN_CANCEL) (*F_s) = floorf(*F_s);
	}
	
	return(current_position);
}// end of get_header_informations_DAQMX_modified()


/************************************************************************/
/* this function creates a header containing exactly the same 			*/
/* information as a file created by Labview (big endian) and containing */
/* data in floats, as acquired through the DAQmx program				*/
/*																		*/
/* returns the file position after the header (on success)				*/
/* returns -1 on any failure											*/
/************************************************************************/
long write_header_informations_DAQMX(char *filename, char *channel_string, int N_channels, float F_s,
									char *date_stamp, char *user_header, long N)
{	FILE 	*file_out;
	long	current_position;
	double	*tampon_double;
	int		*tampon_int, i;

	file_out=fopen(filename, "wb");
	
	tampon_int = (int*)calloc(2,sizeof(int));
	tampon_int[0] = strlen(channel_string);
#ifndef MAC_POWERPC
	swap_bytes(tampon_int, 1, sizeof(int));
#endif
	if (fwrite(tampon_int, sizeof(int), 1, file_out)!=1) return(-1);
	if (fwrite(channel_string, sizeof(char), strlen(channel_string), file_out)!=strlen(channel_string)) return(-1);

		
	tampon_double    = (double*)calloc(2+1*N_channels,sizeof(double));
	tampon_double[0] = (double)1.;
	tampon_double[1] = (double)N_channels;
	for (i=0; i<N_channels; i++) tampon_double[2+i] = (double)F_s;
#ifndef MAC_POWERPC
	swap_bytes(tampon_double, (2+1*N_channels), sizeof(double));
#endif	
	if (fwrite(tampon_double, sizeof(double), (2+1*N_channels), file_out)!=(2+1*N_channels)) return(-1);

	
	tampon_int[0] = strlen(date_stamp);
#ifndef MAC_POWERPC
	swap_bytes(tampon_int, 1, sizeof(int));
#endif
	if (fwrite(tampon_int, sizeof(int), 1, file_out)!=1) return(-1);
	if (fwrite(date_stamp, sizeof(char), strlen(date_stamp), file_out)!=strlen(date_stamp)) return(-1);

	
	tampon_int[0]=strlen(user_header);
#ifndef MAC_POWERPC
	swap_bytes(tampon_int, 1, sizeof(int));
#endif	
	if (fwrite(tampon_int, sizeof(int), 1, file_out)!=1) return(-1);
	if (fwrite(user_header, sizeof(char), strlen(user_header), file_out)!=strlen(user_header)) return(-1);

	tampon_int[0]=(int)(N/N_channels);
#ifndef MAC_POWERPC
	swap_bytes(tampon_int, 1, sizeof(int));
#endif	
	if (fwrite(tampon_int, sizeof(int), 1, file_out)!=1) return(-1);
	
	current_position = ftell(file_out);	// this is the position just after the header	
	
	fclose(file_out);
	free(tampon_int);
	free(tampon_double);
	
	return(current_position);
} /* end of function 'write_header_informations_DAQMX' */






/************************************************************************/
/* this functions loads a file created by Labview (big endian)			*/
/* using the .vi 'acq_mx'												*/
/************************************************************************/
int do_inout_load_NI_acq_mx_data(void)
{
	register int i, j;
	O_p *op = NULL, *opn = NULL;
	d_s 	*ds;
	pltreg 	*pr = NULL;
	FILE 	*data_file=NULL;	
static char fullfilename[512];
	char	filename[256],pathname[512],*s=NULL;
	long 	N=1;
static int 	N_decimate=1;
	int		N_channels=1, i_channel=1;
	float	F_s, moyenne;
	double *tampon_double;
	char	*channel_string, *user_header;
	char 	*date_stamp;
	long	current_position;
static int do_scan_all=1;
static size_t j_start, j_end;
	
	if (updating_menu_state != 0)		return D_O_K;	
	
	if (key[KEY_LSHIFT]) return win_printf_OK("This routine loads data corresponding to 1 channel\n"
		"of data file acquired with acq_mx.vi (in LabView).\n It and places it in a new plot");

 	if ( file_select_ex("Load a NI acq_mx file", fullfilename, "", 512, 0, 0) == 0) return D_O_K;

	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)		return win_printf_OK("cannot find data");

	current_position=get_header_informations_DAQMX(fullfilename, &channel_string, &N_channels, &F_s, 
								&date_stamp, &user_header, &N);
	if (current_position<=0)		return(win_printf_OK("Error reader file header, aborting."));
	
	
	N=N/2; // because we don't have floats in the file, but doubles !
	
	extract_file_name( filename,    256, fullfilename);
	extract_file_path( pathname,	512, fullfilename);
	s=my_sprintf(s,"File %s, created %s with acq_mx.vi\n%s\nchannel names: %s\n"
		"{\\color{lightgreen}f_{acq} is %g Hz}\n\n"
		"There are {\\color{yellow} %d channels}\nWhich channel do you want ?     %%2d\n\n"
		"There are %d points per channels\n"
		"There are %d points in total (over all channels)\n\n"
		"keep 1 point every %%4d points (decimate)\n"
		"scan %%R part of the file / %%r all the file",
		filename, date_stamp, user_header, channel_string, F_s, N_channels, (int)(N/N_channels), (int)N);
	i=win_scanf(s, &i_channel, &N_decimate, &do_scan_all);
	free(s); s=NULL;
	if (i==WIN_CANCEL) return(D_O_K);
	
	if ( (N_channels<1) || (i_channel<1) || (i_channel>N_channels) ) 
          return(win_printf_OK("incorrect channel number !"));
	if (N % N_channels !=0)
		  return(win_printf_OK("number of points in file %d is not a multiple of channel number %d !",(int)N,N_channels));
	N=N/N_channels;
	if (N_decimate<1) 
		  return(win_printf_OK("Very bad decimation !"));
				
	if (do_scan_all==0)
	{	s=my_sprintf(s,"There are %d points in time, for %d different channels\n"
			"(%d points in total)\n"
			"decimation by a factor %d will keep {\\color{red}%d points in time}\n\n"
			"load points in the interval [A B[ with\n"
			"A = %%8d (0<=A<%d)\n"
			"B = %%8d (0<B<=%d)", (int)N, N_channels, N*N_channels, N_decimate, 
                                  (int)N/N_decimate, (int)N/N_decimate, (int)N/N_decimate);
		i=win_scanf(s, &j_start, &j_end);
		free(s); s=NULL;
		if (i==WIN_CANCEL) return(D_O_K);
		if ( (j_start<0) || (j_start>=N/N_decimate) )
			return(win_printf_OK("bad interval!"));
	}
	else
	{	j_start = 0;
		j_end   = N/N_decimate;
	}

	if ((j_end-j_start)>1000000) 
	if (win_printf_OK("You are about to keep more than a million of points\n"
			"That may be too much for the computer\nClick WIN_CANCEL to abort")==WIN_CANCEL)	
		{   return(D_O_K); }
	
	data_file=fopen(fullfilename, "rb");
	fseek(data_file, current_position, SEEK_SET); // we move from the beginning of the file, to the end of the header
	fseek(data_file, (i_channel-1)*sizeof(double)*N + j_start*N_decimate*sizeof(double), SEEK_CUR);
	
	tampon_double =(double*)calloc(N_decimate, sizeof(double));

	if ((opn = create_and_attach_one_plot(pr, (j_end-j_start), (j_end-j_start), 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	ds = opn->dat[0];

	for (j=j_start; j<j_end; j++)
	{	moyenne=0;
		fread(tampon_double, sizeof(double), N_decimate, data_file);
#ifndef MAC_POWERPC
	    swap_bytes(tampon_double, N_decimate, sizeof(double));
#endif			
		for (i=0; i<N_decimate; i++) 
		{	moyenne += tampon_double[i];
		}
		ds->yd[j-j_start] = moyenne/(float)N_decimate;
		ds->xd[j-j_start] = j*N_decimate/F_s;
	}

	free(tampon_double);
	fclose(data_file);

	opn->filename   = strdup(filename);
	opn->dir 	= Mystrdup(pathname);		
	set_ds_source(ds, "acq_mx acquisition, %s\n%s\nchannel %d/%d, f_{acq}=%gHz\n%s\ndecimation 1/%d points", 
			filename, date_stamp, i_channel, N_channels, F_s, user_header, N_decimate);
	set_plot_title(opn, "%s %s", filename, date_stamp);
	set_plot_x_title(opn, "time (s)");
	set_plot_y_title(opn, "channel %d/%d (V)", i_channel, N_channels);

	free(channel_string);
	free(user_header);
	free(date_stamp);

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of 'do_inout_load_NI_acq_mx_data()'








/************************************************************************/
/* this functions loads a file created by Labview (big endian) and	      */
/* and containing data acquired with a DAQmx card and associated software */
/************************************************************************/
int do_inout_load_NI_streamfloat7mx_data(void)
{
	register int i, j;
	O_p *op = NULL, *opn = NULL;
	d_s 	*ds;
	pltreg 	*pr = NULL;
	FILE 	*data_file=NULL;	
static char fullfilename[512];
	char	filename[256],pathname[512];
	char	*s=NULL;
	int     index;
	long 	N=1;
static int 	N_decimate=1;
	int		N_channels=1, i_channel=1, n_chan;
	int 	*channel_index;
	float	F_s;
	float	*moyenne;
	float	*tampon_float;
	char	*channel_string, *user_header;
	char 	*date_stamp;
	long	current_position;
static int do_scan_all=1;
static size_t j_start, j_end;
static	char	selected_channel_string[32]="1,2";
	
	if (updating_menu_state != 0)		return D_O_K;	
	
	if (key[KEY_LSHIFT]) return win_printf_OK("This routine loads data corresponding to 1 channel\n"
		"of a data file created with streamfloat7mx (in LabView).\n It and places it in a new plot");

 	if ( file_select_ex("Load a NI streamfloat7mx file", fullfilename, "", 512, 0, 0) == 0) return D_O_K;

	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)		return win_printf_OK("cannot find data");


	index = active_menu->flags & 0xFFFFFF00;
	
	if (index==0)
	current_position=get_header_informations_DAQMX(fullfilename, &channel_string, &N_channels, &F_s, 
								&date_stamp, &user_header, &N);
	else if (index==IS_NI_modified_SF8)
    current_position=get_header_informations_DAQMX_modified(fullfilename, &channel_string, &N_channels, &F_s, 
								&date_stamp, &user_header, &N);
	else return(win_printf_OK("you specified a strange header type that I don't know..."));
            					
	if (current_position<0)		return(win_printf_OK("Error reading file header, aborting."));
	
	if (N_channels==1) sprintf(selected_channel_string,"1"); // fatalement, n'est-ce pas ?
	
	extract_file_name( filename,    256, fullfilename);
	extract_file_path( pathname,	512, fullfilename);
	s=my_sprintf(s,"File %s, created %s with streamfloat7mx.vi\n%s\nchannel names: %s\n"
		"{\\color{lightgreen}f_{acq} is %g Hz}\n\n"
		"There are {\\color{yellow} %d channels}\nWhich channel(s) do you want ?  %%s\n\n"
		"There are %d points per channels\n"
		"There are %d points in total (over all channels)\n\n"
		"keep 1 point every %%4d points (decimate)\n"
		"scan %%R part of the file / %%r all the file",
		filename, date_stamp, user_header, channel_string, F_s, N_channels, (int)(N/N_channels), (int)N);
	i=win_scanf(s, &selected_channel_string, &N_decimate, &do_scan_all);
	free(s); s=NULL;
	free(channel_string);
	if (i==WIN_CANCEL) return(D_O_K);
	
	channel_index = str_to_index(selected_channel_string, &n_chan);	// malloc is done by str_to_index
	if ( (channel_index==NULL) || (n_chan<1) )	return(win_printf_OK("bad values for channels numbers !"));
	
	for (i_channel=0; i_channel<n_chan; i_channel++)
	{	if ( (channel_index[i_channel]<1) || (channel_index[i_channel]>N_channels) ) 
			return(win_printf_OK("incorrect channel number !"));
	}
	if (N % N_channels !=0) //useless test, this is perform when reading the header!
		return(win_printf_OK("number of points in file %d is not a multiple of channel number %d !",N,N_channels));
	N=N/N_channels;
	if (N_decimate<1) 
		return(win_printf_OK("Very bad decimation !"));
				
	if (do_scan_all==0)
	{	s=my_sprintf(s,"There are %d points in time, for %d different channels\n"
			"(%d points in total)\n"
			"decimation by a factor %d will keep {\\color{red}%d points in time}\n\n"
			"load points in the interval [A B[ with\n"
			"A = %%8d (0<=A<%d)\n"
			"B = %%8d (0<B<=%d)", N, N_channels, N*N_channels, N_decimate, N/N_decimate, N/N_decimate, N/N_decimate);
		i=win_scanf(s, &j_start, &j_end);
		free(s); s=NULL;
		if (i==WIN_CANCEL) return(D_O_K);
		if ( (j_start<0) || (j_start>=N/N_decimate) )
			return(win_printf("bad interval!"));
	}
	else
	{	j_start = 0;
		j_end   = N/N_decimate;
	}
	
	if ((j_end-j_start)>1000000) 
	if (win_printf_OK("You are about to keep more than a million of points\n"
			"That may be too much for the computer\nClick WIN_CANCEL to abort")==WIN_CANCEL)	
		return(D_O_K);
		
	data_file=fopen(fullfilename, "rb");
	fseek(data_file, current_position, SEEK_SET); // we move from the beginning of the file, to the end of the header
	fseek(data_file, j_start*N_decimate*N_channels*sizeof(float), SEEK_CUR);
	
	tampon_float =(float*)calloc(N_channels, sizeof(float)); // size = number of channels in file
	moyenne      =(float*)calloc(n_chan,     sizeof(float)); // size = number of channels to load
	
	if ((opn = create_and_attach_one_plot(pr, (j_end-j_start), (j_end-j_start), 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	ds = opn->dat[0]; // this is the first channel to load, there is at least one !
	for (i_channel=1; i_channel<n_chan; i_channel++) // additional channels to be loadeed are to be put in additional datasets
	{	if ((ds = create_and_attach_one_ds(opn, (j_end-j_start), (j_end-j_start), 0)) == NULL)	
			return(win_printf_OK("cannot create dataset !"));
	}

	for (j=j_start; j<j_end; j++)
	{	for (i_channel=0; i_channel<n_chan; i_channel++) moyenne[i_channel]=0.;
		for (i=0; i<N_decimate; i++) 
		{	fread(tampon_float, sizeof(float), N_channels, data_file);
#ifndef MAC_POWERPC
	        	swap_bytes(tampon_float, N_channels, sizeof(float));
#endif		
			for (i_channel=0; i_channel<n_chan; i_channel++) moyenne[i_channel] += tampon_float[channel_index[i_channel]-1];
		}
		for (i_channel=0; i_channel<n_chan; i_channel++)
		{	opn->dat[i_channel]->yd[j-j_start] = moyenne[i_channel]/(float)N_decimate;
			opn->dat[i_channel]->xd[j-j_start] = (float)j*(float)N_decimate/(float)F_s;
		}
	}

	free(moyenne);
	free(tampon_float);
	fclose(data_file);

	opn->filename   = strdup(filename);
	opn->dir 	= Mystrdup(pathname);
	for (i_channel=0; i_channel<n_chan; i_channel++) set_ds_source(opn->dat[i_channel], 
			"streamfloat7mx acquisition, %s\n%s\nchannel %d/%d, f_{acq}=%gHz\n%s\ndecimation 1/%d points", 
			filename, date_stamp, channel_index[i_channel], N_channels, F_s, user_header, N_decimate);
				
	set_plot_title(opn, "%s %s", filename, date_stamp);
	set_plot_x_title(opn, "time (s)");
	set_plot_y_title(opn, "channel %s/%d (V)", selected_channel_string, N_channels);

	// following lines creates a label not attach to a dataste, but to the plot itself:
	s=my_sprintf(s,"\\fbox{\\stack{{%s %s}{%s}}}", filename, date_stamp, user_header); 
	push_plot_label(opn, opn->dat[0]->xd[(j_end-j_start)/2], opn->dat[0]->yd[0], s, USR_COORD);

	free(s); s=NULL;
	free(user_header);
	free(date_stamp);

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of 'do_inout_load_NI_streamfloat7mx_data()'




/************************************************************************/
/* this function creates a header containing exactly the same 			*/
/* information as a file created by Labview (big endian) and containing */
/* data in floats, as acquired through a NI4472 card					*/
/*																		*/
/* returns the file position after the header (on success)				*/
/* returns -1 on any failure											*/
/************************************************************************/
void test_scan_file(char *filename)
{	FILE 	*file;
	long	current_position;
	double	*tampon_double, *tmswap_double;
	float   *tampon_float, *tmswap_float;
	char	*tampon_char, *tmswap_char;
	int		*tampon_int, *tmswap_int, i, go_on=1;
	char	*s=NULL;

	file=fopen(filename, "rb");
	tampon_char   = (char*)  calloc(8,sizeof(char));
	tampon_int    = (int*)   calloc(2,sizeof(int));
	tampon_float  = (float*) calloc(2,sizeof(float));
	tampon_double = (double*)calloc(1,sizeof(int));
	tmswap_char   = (char*)  calloc(8,sizeof(char));
	tmswap_int    = (int*)   calloc(2,sizeof(int));
	tmswap_float  = (float*) calloc(2,sizeof(float));
	tmswap_double = (double*)calloc(1,sizeof(int));
	
	do
	{	current_position = ftell(file);	// this is the position just after the header	
	
		fread(tampon_char, sizeof(char), 8, file);	fseek(file, current_position, SEEK_SET); memcpy(tmswap_char, tampon_char, 8*sizeof(char));
		fread(tampon_int,  sizeof(int), 2, file);	fseek(file, current_position, SEEK_SET); memcpy(tmswap_int, tampon_int, 2*sizeof(int));
		fread(tampon_float, sizeof(float), 2, file);	fseek(file, current_position, SEEK_SET); memcpy(tmswap_float, tampon_float, 2*sizeof(float));
		fread(tampon_double, sizeof(double), 1, file);	memcpy(tmswap_double, tampon_double, 1*sizeof(double));
	
		swap_bytes(tampon_char, 8, sizeof(char));
		swap_bytes(tampon_int,   2, sizeof(int));
		swap_bytes(tampon_float, 2, sizeof(float));
		swap_bytes(tampon_double, 1, sizeof(double));
		
		s=my_sprintf(s, "at position %d, reading 8 bytes\n chars : ", current_position);
		for (i=0; i<8; i++) s=my_sprintf(s, "%c ", tampon_char[i]);
		s=my_sprintf(s, " swaped : ");
		for (i=0; i<8; i++) s=my_sprintf(s, "%c ", tmswap_char[i]);
		s=my_sprintf(s, "\n int : %d %d  swaped : %d %d", tampon_int[0], tampon_int[1], tmswap_int[0], tmswap_int[1]);
		s=my_sprintf(s, "\n float : %f %f  swaped : %f %f", tampon_float[0], tampon_float[1], tmswap_float[0], tmswap_float[1]);
		s=my_sprintf(s, "\n double : %g  swaped : %g", (float)tampon_double[0], (float)tmswap_int[0]);
			
		if (win_printf("%s", s)==WIN_CANCEL) go_on=0;
		
		free(s);
		s=NULL;	
	}
	while (go_on==1);

	fclose(file);
	free(tampon_char);
	free(tampon_int);
	free(tampon_float);
	free(tampon_double);
	
	return;
} /* end of function 'test_scan_file' */



#endif


