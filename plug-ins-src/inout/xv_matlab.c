#ifndef _XV_MATLAB_C_
#define _XV_MATLAB_C_

#include "allegro.h"
#include "xvin.h"

#include <stdlib.h>
#include <math.h>
#include <malloc.h>
#include <string.h>	
#include "xv_tools_lib.h" // for swap_bytes

// #define BUILDING_PLUGINS_DLL
#include "inout.h"
#include "xv_matlab.h"



// human_type : outputs a string containing human readible type
char *human_type(int type)
{	char	*str=NULL;

	str=(char*)calloc(12,sizeof(char));
	switch (type)
	{	case miINT8:	     sprintf(str, "miINT8"); break;
		case miUINT8:	sprintf(str, "miUINT8"); break;
		case miINT16:	sprintf(str, "miINT16"); break;
		case miUINT16: 	sprintf(str, "miUINT16"); break;
		case miINT32:	sprintf(str, "miINT32"); break;
		case miUINT32:	sprintf(str, "miUINT32"); break;
		case miSINGLE:	sprintf(str, "miSINGLE"); break;
		case miDOUBLE: 	sprintf(str, "miDOUBLE"); break;
		case miINT64:	sprintf(str, "miINT64"); break;
		case miUINT64:	sprintf(str, "miUINT64"); break;
		case miMATRIX: 	sprintf(str, "miMATRIX"); break;
		default:	sprintf(str, "(unknown)");
	}
	return str;
}


// machine_type : outputs the number of bytes used by type
char machine_type(int type)
{	char no=1;

	switch (type)
	{	case miINT8:	no=sizeof(char); break;
		case miUINT8:	no=sizeof(unsigned char); break;
		case miINT16:	no=sizeof(short); break;
		case miUINT16: 	no=sizeof(unsigned short); break;
		case miINT32:	no=sizeof(int); break;
		case miUINT32:	no=sizeof(unsigned int); break;
		case miSINGLE:	no=sizeof(float); break;
		case miDOUBLE: 	no=sizeof(double); break;
		case miINT64:	no=sizeof(long int); break;
		case miUINT64:	no=sizeof(unsigned long int); break;
		case miMATRIX: 	no=sizeof(-1); break;
		default:	no=-1;
	}
	return no;
}


float anything_to_float(void *a, int type)
{	float x=0;

	switch (type)
	{	case miINT8:	x=(float)((char*)a)[0]; break;
		case miUINT8:	x=(float)((unsigned char*)a)[0]; break;
		case miINT16:	x=(float)((short*)a)[0]; break;
		case miUINT16: 	x=(float)((unsigned short*)a)[0]; break;
		case miINT32:	x=(float)((int*)a)[0]; break;
		case miUINT32:	x=(float)((unsigned int*)a)[0]; break;
		case miSINGLE:	x=(float)((float*)a)[0]; break;
		case miDOUBLE: 	x=(float)((double*)a)[0]; break;
		case miINT64:	x=(float)((long int*)a)[0]; break;
		case miUINT64:	x=(float)((unsigned long int*)a)[0]; break;
		case miMATRIX: 	x=(float)(-1); break;
		default:	x=-1;
	}

	return(x);
}




// white_space : returns a string of adequate size to complete input string with white spaces, up to size N
char *white_space(char *chaine, int N)
{	char *str;
	int	n;
	register int i;
	
	n   = N-strlen(chaine);
	if (n<=0) return("");

	str = (char*)malloc(n+1);
	for (i=0; i<n; i++) str[i]=' ';
	str[n]='\0';

	return str;
}




// do_padding_8 : increase the pointer in the file up to the next multiple of 8bytes
int do_padding_8(FILE *data_file)
{	int position = ftell(data_file);

	if ((position%8)!=0)	fseek(data_file, 8-(position%8), SEEK_CUR);
	return ftell(data_file);
}






// get_header : read the header of a Matlab file
char *get_header(FILE *data_file)
{	char	*header=NULL;
	register int i;
	
	fseek(data_file, 0, SEEK_SET);
	header = (char*)calloc(124+1,sizeof(char));
	if (fread(header, sizeof(char), 124, data_file)!=124) 
	{	win_printf_OK("error reading header!\n(text)");
		return(NULL); 
	}
	i=124;
	do
	{	header[i]='\0';
		i--;
	}
	while ( (header[i-1]==' ') && (i>0) );

	if (header[0]*header[1]*header[2]*header[3]==0) win_printf_OK("Not a Matlab 5 file !");
		
	return(header);
}



/*********************************************************************/
// get_byte swapping : determines if a file needs byte swapping
/*********************************************************************/
int get_byte_swapping(FILE *data_file)
{	char	*tmp_char;
	int	byte_swapping=-1;

	fseek(data_file, 126, SEEK_SET);

	tmp_char = (char*)calloc(2,sizeof(char));
	if (fread(tmp_char, sizeof(char), 2, data_file)!=2) 
	{	free(tmp_char); 
		win_printf_OK("error reading header!\n(endian indicator)");
		return(-1); 
	}
	if (strncmp(tmp_char,"MI",2)==0)	byte_swapping=0;
	else if (strncmp(tmp_char,"IM",2)==0)	byte_swapping=1;
	else {	win_printf_OK("error reading header!\n(endian indicator not valid)\n%s", tmp_char);
		return(-1);
	     } 
	free(tmp_char);
	
	return(byte_swapping);	
}



/************************************************************************/
// get_number_of_elements : detects how many elements there is in a file
/************************************************************************/
int get_number_of_elements(FILE *data_file, int byte_swapping)
{	int	file_size;	// size of the file, in bytes
	int	N_elements=0;	// number of elements : will be returned
	int	tmp_int[2];
	int	N=0;		// size of the element;
	int	go_on=1;


	fseek(data_file, 0, SEEK_END);
	file_size = ftell(data_file);
	fseek(data_file, 128, SEEK_SET);

	while ( (!feof(data_file)) && go_on && (ftell(data_file)<file_size) )
	{
		if (fread(tmp_int, sizeof(int), 2, data_file)!=2)	go_on=0;
		else
		{   if ( (tmp_int[0]-(tmp_int[0]%(256*256)) )!=0)	// the element is compressed
									// because first 2 bytes of tag are not 0
			{	// there is nothing to do, element has been completely scanned
//				win_printf("element is compressed");
			}
			else
			{
//	 			win_printf("element is not compressed");
				if (byte_swapping==0) 	swap_bytes(tmp_int, 2, sizeof(int));
				N = tmp_int[1];	// number of elements

				if ( (N%8) == 0 ) 	fseek(data_file, N, SEEK_CUR); // we jump to the end of the element
				else			fseek(data_file, N+(8-N%8), SEEK_CUR);	// because data is 64bits aligned
			}
			N_elements++;
		}
	}// while

	return N_elements;
}






/****************************************************************/
// get vital informations about one element in a .mat file
/****************************************************************/
int get_info_for_one_element(FILE *data_file, int byte_swapping,
				long *position,
				long *position_data,	
				int  *size, 
				int  *nx, 
				int  *ny, 
				char *name,
				int  *type,
				int  *class,
				int  *flags,
				int  *format)
{
	int	tmp_int[2];	// tmp variable (buffer for reading in file)
	int	no;		// size of type of element (in bytes)
	char	go_on=1;	// boolean, saying to go on, or not (traces errors)
	int	n_dimensions=2; // dimensionality of the element
	int	name_size;
	int subelement_size;

	position[0] = ftell(data_file);
	if (fread(tmp_int, sizeof(int), 2, data_file)==2)
	{
		if ( (tmp_int[0]-(tmp_int[0]%(256*256)) )!=0)	// the element is compressed
		{	if (byte_swapping==0) 	swap_bytes(tmp_int, 2, sizeof(short int));
			type[0] =  tmp_int[0]%(256*256);
			size[0] = (tmp_int[0]-type[0])/(256*256);
		}		
		else 
		{
			if (byte_swapping==0) 	swap_bytes(tmp_int, 2, sizeof(int));

			type[0] = tmp_int[0];	// type of data
			size[0] = tmp_int[1];	// size in bytes

			if (tmp_int[0]!=miMATRIX)
			{	
				no = machine_type(tmp_int[0]);
				if (no<1) 
				{	win_printf("machine type not supported ?");
					return(-1);
				}
				nx[0] = tmp_int[1]/no;	// number of elements
				ny[0] = 1;
				sprintf(name, "[no name]");
			} // end for non-miMATRIX element

			else // we have a miMATRIX element:
			{

// now, we read the tag of the first sub-element:
				if (fread(tmp_int, sizeof(int), 2, data_file)!=2)	go_on=0;
				if (byte_swapping==0) 	swap_bytes(tmp_int, 2, sizeof(int));
// this tag is for saying INT32 and 8bytes:
				if ( (tmp_int[0]!=miUINT32) || (tmp_int[1]!=2*sizeof(unsigned int)) ) go_on=0;
// the next two values are used as follows:
				if (fread(tmp_int, sizeof(unsigned int), 2, data_file)!=2)	go_on=0;
			//	if (byte_swapping==0) 	swap_bytes(tmp_int, 2, sizeof(int));
				class[0] = ((unsigned int)tmp_int[0])%256;
				flags[0] = ((unsigned int)tmp_int[0]-class[0])/256;
// 				win_printf("after first subelement, go on = %d\n\nI found class %d and flags %d", go_on, class, flags);

// now, we read the tag of the second sub-element:
				if (fread(tmp_int, sizeof(int), 2, data_file)!=2)	go_on=0;
				if (byte_swapping==0) 	swap_bytes(tmp_int, 2, sizeof(int));
				if (tmp_int[0]!=miINT32) go_on=0;
				n_dimensions = tmp_int[1]/sizeof(int);
				if (n_dimensions!=2) 
				{	win_printf_OK("dimension of miMatrix element is too large (>2)... I cannot scan it");
					return(-1);
				}				
// now, we read the second element, containing dimensions:
				if (fread(tmp_int, sizeof(int), 2, data_file)!=2)	go_on=0;
				if (byte_swapping==0) 	swap_bytes(tmp_int, 2, sizeof(int));
				nx[0] = tmp_int[0];
				ny[0] = tmp_int[1];
//				win_printf("after second subelement, go on = %d\n\nI found nx = %d and ny = %d", go_on, nx[0], ny[0]);
				do_padding_8(data_file);

// now we read the 3rd element (the name):
				if (fread(tmp_int, sizeof(int), 2, data_file)!=2)	go_on=0;
				if ( (tmp_int[0]-(tmp_int[0]%(256*256)) )!=0)	// the element is compressed
				{	if (byte_swapping==0) 	swap_bytes(tmp_int, 4, sizeof(short int));
					name_size  = (tmp_int[0] - (tmp_int[0]%(256*256)))/(256*256);
					tmp_int[0] = tmp_int[0]%(256*256);
					fseek(data_file, -4, SEEK_CUR);
				}
				else	
				{	if (byte_swapping==0) 	swap_bytes(tmp_int, 2, sizeof(int));
					name_size  = tmp_int[1];
				}
				if (tmp_int[0]!=miINT8) go_on=0;
				if (fread(name, sizeof(char), name_size, data_file)!=(name_size) )	go_on=0;
				name[name_size] = '\0';

				do_padding_8(data_file);

// now, we read the header (tag) of the fourth sub-element
				if (fread(tmp_int, sizeof(int), 2, data_file)!=2)	go_on=0;
				if ( (tmp_int[0]-(tmp_int[0]%(256*256)) )!=0)	// the element is compressed
				{	if (byte_swapping==0) 	swap_bytes(tmp_int, 2, sizeof(short int));
					subelement_size  = (tmp_int[0] - (tmp_int[0]%(256*256)))/(256*256);
					format[0]        = tmp_int[0]%(256*256);
					fseek(data_file, -4, SEEK_CUR);
				}
				else // not compressed
				{
					format[0] 		= tmp_int[0];
					subelement_size = tmp_int[1];
				}

//				win_printf("fourth sub-element is format %d, and size %d", format[0], subelement_size);
				
				if (subelement_size/machine_type(format[0]) != nx[0]*ny[0])  
					win_printf("Error in dimensions\n"
							"%d elements are stored, but I expected %d x %d", subelement_size, nx[0], ny[0]);

				position_data[0]=ftell(data_file);
			
			} // end of miMATRIX element
			
		} // end for not compressed data
	}
	fseek(data_file, position[0] + (2*sizeof(int)) + size[0], SEEK_SET);

	return(ftell(data_file)); // we return the position in the file, just after the scanned element
}














/************************************************************************/
/* this functions scans a file created by Matlab			*/
/* it returns N, the number of read elements				*/
/************************************************************************/
int scan_Matlab5_data(FILE *data_file, 		// the file to scan 
				long *position,// pointer of size N, containing positions of elements in the file
				long *position_data,// pointer of size N, containing positions of binary data in the file
				int *size, 	// pointer of size N, containing sizes of elements (in bytes)
				int *nx, 	// pointer of size N, containing dimension 1 of arrays
				int *ny, 	// pointer of size N, containing dimension 2 of arrays
				char *name, 	// pointer of size N*128, containing names of elements
				int *type,	// pointer of size N, containing types of elements
				int *flags,
				int *class,
				int *format
				)
{
	register int i,j;
	int 	N_elements=0;
	int	*tmp_int;
	int	byte_swapping;	
	char	tmp_name[128]="[no name]";

	tmp_int = (int*)calloc(2,sizeof(int));

	byte_swapping   = get_byte_swapping(data_file);
	N_elements	= get_number_of_elements(data_file, byte_swapping);

//  win_printf("byte swapping : %d \n %d elements", byte_swapping, N_elements);

	fseek(data_file, 128, SEEK_SET);	// back to begining

	for (i=0; i<N_elements; i++)
	{
		get_info_for_one_element(data_file, byte_swapping, 
			&position[i], &position_data[i], &size[i], &nx[i], &ny[i], tmp_name, &type[i], &class[i], &flags[i], &format[i]);
		for (j=0; j<strlen(tmp_name); j++)
		{	name[128*i+j] = tmp_name[j];
		}
		name[128*i+strlen(tmp_name)]='\0';
		//free(tmp_name);
	}

	free(tmp_int);

	return(N_elements);
}// end of do_inout_scan_Matlab_data(void)











/************************************************************************/
/* this functions is the one called from the menu			*/
/************************************************************************/
int do_inout_load_Matlab5_data(void)
{
	register int i, j;
	O_p 	*op = NULL, *opn = NULL;
	d_s 	*ds = NULL;
	pltreg 	*pr = NULL;
	FILE 	*data_file;	
static char 	fullfilename[512];
	char	filename[256],pathname[512],*s;
	int 	N=0, N_elements=0, no=1;
	int	I=0;		// number of element to load
	int	n1, n2;
	void	*buffer;
	int	byte_swapping=1;
	int 	go_on = 1;
	
	long	*position=NULL, *position_data=NULL;	// position of elements in the file, position of their data
	char	*header=NULL, *name=NULL;
	int	*size=NULL, *nx=NULL, *ny=NULL, *type=NULL, *flags=NULL, *class=NULL, *format=NULL;
	int question=13;
	
	if (updating_menu_state != 0)		return D_O_K;	
	
	if (key[KEY_LSHIFT]) return win_printf_OK("This routine loads data from a .mat file (MatLab).\n"
					"It and places it in a new plot");

 	//if ( file_select_ex("Load MatLab file", fullfilename, "mat", 512, 0, 0) == 0) return D_O_K; 

	if (do_select_file(fullfilename, sizeof(fullfilename), "MatLab-FILE", "*.mat", "MatLab data File to load\0", 1))
        return win_printf_OK("Cannot select MatLab input file");

	
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)		return win_printf_OK("cannot find data");

	data_file=fopen(fullfilename, "rb");
	if (data_file==NULL) win_printf_OK("load Matlab data: error opening file %s",fullfilename);

	byte_swapping = get_byte_swapping(data_file);
	header		  = get_header(data_file);
 	N_elements	  = get_number_of_elements(data_file, byte_swapping);

	position	  = (long*)calloc(N_elements, sizeof(long));
	position_data = (long*)calloc(N_elements, sizeof(long));
	size		  = (int*) calloc(N_elements, sizeof(int));
	nx 		      = (int*) calloc(N_elements, sizeof(int));
	ny 		      = (int*) calloc(N_elements, sizeof(int));
	type 		  = (int*) calloc(N_elements, sizeof(int));
	flags 		  = (int*) calloc(N_elements, sizeof(int));
	class 	  	  = (int*) calloc(N_elements, sizeof(int));
	format		  = (int*) calloc(N_elements, sizeof(int));

	name		  = (char*)calloc(N_elements*128, sizeof(char));
	
    if (scan_Matlab5_data(data_file, position, position_data, size, nx, ny, name, type, flags, class, format) != N_elements) 
	{	win_printf("Error scanning file\n%s", fullfilename);
		return(D_O_K); 
	}

	s=(char*)calloc(256*(N_elements+1),sizeof(char));
	sprintf(s,"%s\n%d elements\n%s\n\n", 
		header, N_elements, (byte_swapping==1) ? "byte swapping is necessary" : "no byte swapping required");
	for (i=0; i<N_elements; i++)
	{	if (i==0) s=my_sprintf(s,"%%R {\\color{yellow}%s}%s %5dx%5d - %s (%d bytes)\n", 
					name+128*i, white_space(name+128*i, 16), nx[i], ny[i], human_type(type[i]), size[i] );
		else      s=my_sprintf(s,"%%r {\\color{yellow}%s}%s %5dx%5d - %s (%d bytes)\n", 
					name+128*i, white_space(name+128*i, 16), nx[i], ny[i], human_type(type[i]), size[i] );
	}
	if (win_scanf(s, &I)==WIN_CANCEL) return(D_O_K);
	free(s);
/*	if (win_printf("{\\color{yellow}loading %s} (element %d/%d)\nstarting position (header) %d\n"
			"data position %d\nsize %d x %d\ntype %d (%s)\nflags %d\nclass %d\n data stored as %s", 
			name+128*I, I+1, N_elements, position[I], position_data[I], nx[I], ny[I], type[I], 
			human_type(type[I]), flags[I], class[I], human_type(format[I]) ) == WIN_CANCEL)
		return(D_O_K);
*/

	fseek(data_file, position_data[I], SEEK_SET);
	no = machine_type(format[I]); // taille d'un element (sizeof())

	n1 = nx[I]; // dimension 1 du vecteur ou de la matrice a charger.
	n2 = ny[I]; // dimension 2 du vecteur ou de la matrice a charger.

	if (n1==1) // on transpose pour avoir un dataset plus long, au lieu d'un grand nombre de datasets
	{ 	n2 = nx[I];
		n1 = ny[I];
	}

	buffer = (char*)malloc(n1*n2*no);
	i=fread(buffer, sizeof(char), no*n1*n2, data_file);
	if (i!=(n1*n2*no))	
	{	go_on=0; 
		win_printf_OK("variable %s:\nimpossible to read enough elements\n"
			"%d instead of %d", name+128*I, i, n1*n2);
	}
	if (byte_swapping==0) 	swap_bytes(buffer, n1*n2, no);

	fclose(data_file);

	extract_file_name( filename,    256, fullfilename);
	extract_file_path( pathname,	512, fullfilename);

	if (n1*n2==1) // single value
	question=win_printf("from file %s\n {\\pt14\\color{yellow}%s = %f}\n\nOK to create a plot or WIN_CANCEL to quit", 
					filename, name+128*I, 
					anything_to_float(buffer, format[I]));
	
	if (question!=WIN_CANCEL)
	{ for (i=0; i<n2; i++) // create as many datasets as necessary
		{	if (i==0)
			{ 	if ((opn = create_and_attach_one_plot(pr, n1, n1, 0)) == NULL)
					return win_printf_OK("cannot create plot !");
		  		ds = opn->dat[0];
			}
			else    ds = create_and_attach_one_ds(opn, n1, n1, 0);

			for (j=0; j<n1; j++)
			{	ds->yd[j] = anything_to_float(buffer+(i*n1+j)*no, format[I]);
				ds->xd[j] = j+1;
			}

			set_ds_source(ds, "variable %s, from %s\n%s\n%d elements (%d-bytes data)", 
					name+128*I, filename, header, N, no);
		
			if (n1==1)		// there is just one point, one value
			{	set_ds_dot_line(ds);
				set_ds_point_symbol(ds, "\\pt12\\oc");
			}
	
		}

		if (opn!=NULL)
		{	opn->filename   = strdup(filename);
			opn->dir 	= Mystrdup(pathname);		
		}
		set_plot_title  (opn, "%s", filename);
		set_plot_x_title(opn, "%d dataset(s) of %d points", n2, n1);
		set_plot_y_title(opn, "%s", name+128*I);
		
	}// end of creation of plot
	
	if (buffer!=NULL) free(buffer);
	free(header);
	free(position);	free(position_data);
	free(size);	free(nx);	free(ny);	free(type);
	free(flags);	free(class);	free(format);
	free(name);

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
}




int append_one_element_in_matlab_file(FILE *data_file, float *x, int nx, char *name)
{	int	tmp_int[2];
	int	err;
	int	element_size, 	// binary size of element, in bytes
		data_size;	// binary size of data only
	long	initial_position;

	data_size	= (nx + nx%2)*machine_type(miSINGLE); /* the data, padded */
	element_size    = data_size + 4*8 /* 4 headers */ + 8 + 8 + 8 /* first 3 sub-elements */;

	initial_position=ftell(data_file);	

// global header of miMATRIX:	
	tmp_int[0] = miMATRIX;
	tmp_int[1] = element_size; 
	err=fwrite(tmp_int, sizeof(int), 2, data_file);

// first sub-element:
	tmp_int[0]=miUINT32;
	tmp_int[1]=2*sizeof(unsigned int);
	err=fwrite(tmp_int, sizeof(int), 2, data_file);

	tmp_int[0]=0;
	tmp_int[1]=mxSINGLE_CLASS;
	err=fwrite(tmp_int, sizeof(int), 2, data_file);

// second sub-element:
	tmp_int[0]=miUINT32;
	tmp_int[1]=2*sizeof(int); // 2 dimensions
	err=fwrite(tmp_int, sizeof(int), 2, data_file);

	tmp_int[0]=1;
	tmp_int[1]=nx; 
	err=fwrite(tmp_int, sizeof(int), 2, data_file);

// third sub-element:
	tmp_int[0]=miUINT8;
	tmp_int[1]=8; // 2 dimensions
	err=fwrite(tmp_int, sizeof(int), 2, data_file);

	err=fwrite(name, sizeof(char), 8, data_file);

// fourth sub-element (the data)
	tmp_int[0]=miSINGLE;
	tmp_int[1]=data_size;
	err=fwrite(tmp_int, sizeof(int), 2, data_file);

	fwrite(x, sizeof(float), nx, data_file);
	if ( (nx%2)!=0 )	{ tmp_int[0]=0; fwrite(tmp_int, sizeof(int), 1, data_file); }

	if ( (ftell(data_file)-initial_position) != (element_size + 8 /*8 is the header size!*/) )
	win_printf("I wrote %d bytes in the file for element %s\nbut I was expecting %d bytes...", 
		(int)(ftell(data_file)-initial_position), name, element_size);

	return(element_size);
}





int do_inout_save_all_datasets_matlab5(void)
{	register int i, j;
	int	i_start, i_end;
	O_p 	*op = NULL;
	d_s 	*ds = NULL;
	pltreg 	*pr = NULL;
	FILE 	*data_file;	
static char 	fullfilename[512];
	char	header[128]="MATLAB 5.0 MAT-file, created with XVin ";
	char	tmp_char[64];
//	short	tmp_short[2];
	int	bool_all=0;
	int	err;
	
	if (updating_menu_state != 0)		return D_O_K;	
	
	if (key[KEY_LSHIFT]) return win_printf_OK("This routine saves one or all datasets of current plot\n"
					" in a .mat file (MatLab v5).");
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)		return win_printf_OK("cannot find data");

	if (win_scanf("%R save active dataset\n%r save ALL datasets", &bool_all)==WIN_WIN_CANCEL) return(D_O_K);
	if (file_select_ex("Save .mat MatLab file", fullfilename, "mat", 512, 0, 0) == 0) return(D_O_K);

	data_file=fopen(fullfilename, "wb");
	if (data_file==NULL) win_printf_OK("error opening file %s",fullfilename);

	sprintf(tmp_char, "from file %s %d dataset(s)", op->filename, (bool_all==1) ? op->n_dat : 1);
	strcat(header, tmp_char);	
	for (i=strlen(header); i<124; i++) header[i]=' ';	// we complete up to 124;
	err=fwrite(header, sizeof(char), 124, data_file);	

	tmp_char[0]=1;
	tmp_char[1]=0;
	tmp_char[2]='I';
	tmp_char[3]='M';
	err=fwrite(tmp_char, sizeof(char), 4, data_file);

	if (bool_all==1)
	{	i_start = 0;
		i_end   = op->n_dat;
	}
	else
	{	i_start = op->cur_dat;
		i_end   = op->cur_dat+1;
	}

	for (i=i_start; i<i_end; i++)
	{	ds = op->dat[i];

		sprintf(tmp_char, "data%04d", i);
		for (j=strlen(tmp_char); j<8; j++) tmp_char[j]='0';

		append_one_element_in_matlab_file(data_file, ds->yd, ds->ny, tmp_char);
	}

	fclose(data_file);
	return(D_O_K);
}




#endif

