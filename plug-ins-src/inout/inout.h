#ifndef _INOUT_H_
#define _INOUT_H_

#include <stdio.h>
#include <string.h>

#include "xv_Labview.h"

/************************************************************************/
/* Procedures :								*/
/************************************************************************/
PXV_FUNC(int, get_data_length,  (char *filename));
PXV_FUNC(int, load_data_bin, 	(char *filename, float **x1));		/* load floats in a binary file		*/
PXV_FUNC(int, load_data_chunk,  (char *filename, float **x1, long int n, long int offset));
PXV_FUNC(int, save_data_bin, 	(char *filename, float *x1, int N)); 	/* save floats in a binary file		*/
PXV_FUNC(int, save_data_ascii, 	(char *filename, float *x1, int N));	/* save floats in a ascii file  	*/
PXV_FUNC(int, append_data_bin, 	(char *filename, float *x1, int N));

PXV_FUNC(int, load_data_bin_to_double, (char *filename, double **x1));  /* loads file of floats into array of doubles */
PXV_FUNC(int, save_data_double_bin,    (char *filename, double *x1, int N)); /* saves doubles in a file of floats */

PXV_FUNC(int, load_parameter, 	(char *filename, char *param, float *x));	/* load a parameter (number) from a text file */
PXV_FUNC(int, load_parameter_string, (char *filename, char *param, char *x));	/* load a parameter (string) from a text file */

PXV_FUNC(int, do_inout_load_tabular_file, (void) );
PXV_FUNC(int, do_inout_load_column_from_tabular_file, (void));
PXV_FUNC(int, do_inout_load_histogram, (void));

PXV_FUNC(int, do_inout_load_data_bin, 	(void));
PXV_FUNC(int, do_inout_load_raw_image, (void) );

PXV_FUNC(int, count_number_of_colomns_in_ascii_file, (char *filename, int n_header) );
PXV_FUNC(int, count_number_of_lines_in_ascii_file,   (char *filename, int n_col) );

/*
PXV_FUNC(MENU*, inout_plot_menu, (void));
PXV_FUNC(MENU*, inout_image_menu, (void));
*/
PXV_FUNC(int, inout_main, 	(int argc, char **argv));
PXV_FUNC(int, inout_unload, 	(int argc, char **argv));

#endif
