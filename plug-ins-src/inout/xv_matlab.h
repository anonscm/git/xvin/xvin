// definitions of Matlab variables types
// used to import/export matlab .mat files
//
// Nicolas Garnier, May 9th 2004
#ifndef _XV_MATLAB_H_
#define _XV_MATLAB_H_

// definitions of types for elements:
#define miINT8 		1
#define miUINT8		2
#define miINT16 	3
#define miUINT16 	4
#define miINT32 	5
#define miUINT32 	6
#define miSINGLE 	7
#define miDOUBLE 	9
#define miINT64 	12
#define miUINT64 	13
#define miMATRIX 	14

// definition of classes for subelements of a matrix:
// those are the ones to use because in MatLab 5, it seems to me
// that all variables are written in .mat files as matrix (miMATRIX) 
#define mxCELL_CLASS	1 	// Cell array
#define mxSTRUCT_CLASS	2	// Structure
#define mxOBJECT_CLASS	3 	// Object
#define mxCHAR_CLASS	4	// Character array
#define mxSPARSE_CLASS	5	// Sparse array
#define mxDOUBLE_CLASS	6 	// Double precision array
#define mxSINGLE_CLASS	7	// Single precision array
#define mxINT8_CLASS	8	// 8-bit, signed integer
#define	mxUINT8_CLASS	9	// 8-bit, unsigned integer 
#define mxINT16_CLASS	10	// 16-bit, signed integer
#define mxUINT16_CLASS	11	// 16-bit, unsigned integer
#define mxINT32_CLASS	12	// 32-bit, signed integer
#define mxUINT32_CLASS	13 	// 32-bit unsigned, integer


typedef struct Matlab_variable
{	char	*name;
	int	*dim;
	int	class;
	int	flags;
	void	*data;
} M_v;

// accessible from menu :

PXV_FUNC(int, do_inout_load_Matlab5_data,		(void) );
PXV_FUNC(int, do_inout_save_all_datasets_matlab5,	(void) );

// non accessible from menu:

PXV_FUNC(char*, human_type,  		(int type) ); // outputs a string containing human readible type
PXV_FUNC(char,  machine_type,		(int type) ); // outputs the number of bytes used by type
PXV_FUNC(float, anything_to_float,	(void *a, int type) );
PXV_FUNC(char*, white_space, 		(char *chaine, int N) ); // returns a string of adequate size to complete input string with white spaces, up to size N
PXV_FUNC(char*, get_header, 		(FILE *data_file) ); // read the header of a Matlab file
PXV_FUNC(int,   get_byte_swapping,	(FILE *data_file) ); // determines if a file needs byte swapping
PXV_FUNC(int,   get_number_of_elements, (FILE *data_file, int byte_swapping) ); //detects how many elements there is in a file
PXV_FUNC(int,   get_info_for_one_element, (FILE *data_file, int byte_swapping,
				long *position,	long *position_data,	
				int  *size, int  *nx, int  *ny, 
				char *name, int  *type,	int  *class,
				int  *flags, int  *format) ); // get vital informations about one element in a .mat file
PXV_FUNC(int,   scan_Matlab5_data, (FILE *data_file, 		// the file to scan 
				long *position,// pointer of size N, containing positions of elements in the file
				long *position_data,// pointer of size N, containing positions of binary data in the file
				int *size, 	// pointer of size N, containing sizes of elements (in bytes)
				int *nx, 	// pointer of size N, containing dimension 1 of arrays
				int *ny, 	// pointer of size N, containing dimension 2 of arrays
				char *name, 	// pointer of size N*128, containing names of elements
				int *type,	// pointer of size N, containing types of elements
				int *flags,
				int *class,
				int *format) );	// scans a file created by Matlab and returns N, the number of read elements
PXV_FUNC(int,   append_one_element_in_matlab_file, 
				(FILE *data_file, float *x, int nx, char *name) ); 

				
				
				
				

#endif


