/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _IGOR_C_
#define _IGOR_C_

# include "allegro.h"
# include "xvin.h"

/* If you include other plug-ins header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "igor.h"

int DoReadTest(const char* filePath, pltreg *pr);
int DoWriteTest(const char* filePath);

int	do_load_igor(void)
{
    register int i;
    char path[512], file[256];
    static char fullfile[512], *fu = NULL;
    pltreg* pr;
    

    pr = find_pr_in_current_dialog(NULL);

    if(updating_menu_state != 0)    
    {
        return D_O_K;    
    }
    switch_allegro_font(1);
    if (fu == NULL)
    {
        fu = (char*)get_config_string("IGOR-FILE","last_loaded",NULL);
        if (fu != NULL)    
        {
            strcpy(fullfile,fu);
            fu = fullfile;
        }
        else
        {
            my_getcwd(fullfile, 512);
            strcat(fullfile,"\\");
        }
    }

    i = file_select_ex("Load Igor file", fullfile, "ibw", 512, 0, 0);
    switch_allegro_font(0);
    if (i != 0)     
    {
        fu = fullfile;
        set_config_string("IGOR-FILE","last_loaded",fu);
        extract_file_name(file, 256, fullfile);
        extract_file_path(path, 512, fullfile);
        strcat(path,"\\");
	DoReadTest(fullfile, pr);
    }
    return 0;
}

int save_igor2_plot(void)
{
  //register int i;
  //char path[512], file[256], fullfile[512], 
  char fileigor[512];
  char *pa;
  pltreg *pr = NULL;
  char *DoFileSave(char *FileTitle, char *initialdir, char *szFileName, int szFileNameSize, char *ext_filters, char *extension);

  if(updating_menu_state != 0)	return D_O_K;	
  pr = find_pr_in_current_dialog(NULL);
  if (pr == NULL)		win_printf_OK("cannot find Pr");
  if (pr->one_p->filename != NULL) strcpy(fileigor,pr->one_p->filename);
  else fileigor[0] = 0;
  pa = DoFileSave("Exporting to Igor format",pr->one_p->dir, fileigor, 512, "Igor files (*.ibw)\0*.ibw\0All files (*.*)\0*.*\0\0", "ibw");
  win_printf("selected file \n%s",backslash_to_slash(pa));
  return 0;
}

int save_igor_plot(void)
{
	register int i;
	char path[512], file[256], fullfile[512], fileigor[512];
	char *pa;
	int aret;
	pltreg *pr = NULL;
	int DoWriteTestXv(const char* filePath, O_p *op, int dsn);

	if(updating_menu_state != 0)	return D_O_K;	
	switch_allegro_font(1);
	pr = find_pr_in_current_dialog(NULL);

	if (pr == NULL)		win_printf_OK("cannot find Pr");
	
	if (pr->one_p->dir == NULL)
	{
		pa = (char *)get_config_string("IGOR-FILE","last_saved",NULL);	
		if (pa != NULL)		extract_file_path(fullfile, 512, pa);
		else			my_getcwd(fullfile, 512);
		strcat(fullfile,"\\");
	}
	else 
	{
		strcpy(fullfile,pr->one_p->dir);
		strcat(fullfile,"\\");
	}
	if (pr->one_p->filename == NULL)
	{	
		strcat(fullfile,"Untitled.igw");
	}
	else strcat(fullfile,pr->one_p->filename);

	fix_filename_slashes(fullfile);
	replace_extension(fileigor, fullfile, "ibw", 512);
				
	i = file_select_ex("Define directory to save Igor files", fileigor, NULL, 512, 0, 0);
	if (i != 0) 	
	{
		set_config_string("IGOR-FILE","last_saved",fileigor);	
		extract_file_name(file, 256, fileigor);
		extract_file_path(path, 512, fileigor);
		put_backslash(path);
		if (file_exists(fileigor,FA_DIREC,&aret) == 0)  
		  {
		    if (mkdir(fileigor) != 0)
		      {
			win_printf("I cannot create path!\n%s",backslash_to_slash(fileigor));
			return D_O_K;		
		      }
		  } 
		for (i = 0; i < pr->one_p->n_dat; i++)
		  DoWriteTestXv(fileigor, pr->one_p, i);
		/*		save_one_plot(pr->one_p, fullfile);
		set_op_filename(pr->one_p, file);
		set_op_path(pr->one_p, path);*/
	}
	switch_allegro_font(0);
	broadcast_dialog_message(MSG_DRAW,0);
	return D_O_K;
}

MENU *igor_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	//	add_item_to_menu(mn,"data set rescale in Y", do_igor_rescale_data_set,NULL,0,NULL);
	return mn;
}

int	igor_main(int argc, char **argv)
{
  //	add_plot_treat_menu_item ( "igor", NULL, igor_plot_menu(), 0, NULL);

	add_item_to_menu(plot_file_import_menu,"Load Igor", do_load_igor, NULL, 0, NULL);
	add_item_to_menu(plot_file_export_menu,"Save as Igor", save_igor_plot, NULL, 0, NULL);	
	add_item_to_menu(plot_file_export_menu,"Save Igor", save_igor2_plot, NULL, 0, NULL);	
	return D_O_K;
}

int	igor_unload(int argc, char **argv)
{
        remove_item_to_menu(plot_file_import_menu,"Load Igor File", NULL, NULL);
        remove_item_to_menu(plot_file_export_menu,"Save Igor File", NULL, NULL);

	//	remove_item_to_menu(plot_treat_menu, "igor", NULL, NULL);
	return D_O_K;
}




#endif

