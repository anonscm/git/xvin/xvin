# include "allegro.h"
# include "winalleg.h"
# include "windows.h"


// ext_filter = "Text Files (*.txt)\0*.txt\0All Files (*.*)\0*.*\0\0";

char *DoFileSave(char *FileTitle, char *initialdir, char *szFileName, int szFileNameSize, char *ext_filters, char *extension)
{
   OPENFILENAME ofn;

   win_printf("base filename %s",szFileName);

   ZeroMemory(&ofn, sizeof(ofn));
   //szFileName[0] = 0;
   ofn.lStructSize = sizeof(ofn);
   ofn.hwndOwner = win_get_window();
   ofn.lpstrFilter = ext_filters;
   ofn.lpstrFile = szFileName;
   ofn.nMaxFile = szFileNameSize;
   ofn.lpstrDefExt = extension;
   ofn.lpstrInitialDir = initialdir; 
   ofn.lpstrTitle = FileTitle; 
   ofn.Flags = OFN_EXPLORER | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY |
         OFN_OVERWRITEPROMPT;
         
   if(GetSaveFileName(&ofn))  return szFileName;
   else return NULL;
}


char *DoFileOpen(char *FileTitle, char *initialdir, char *ext_filters, char *extension)
{
   OPENFILENAME ofn;
   char szFileName[MAX_PATH];

   ZeroMemory(&ofn, sizeof(ofn));
   szFileName[0] = 0;

   ofn.lStructSize = sizeof(ofn);
   ofn.hwndOwner = win_get_window();
   ofn.lpstrFilter = ext_filters;
   ofn.lpstrFile = szFileName;
   ofn.nMaxFile = MAX_PATH;
   ofn.lpstrDefExt = extension;
   ofn.lpstrInitialDir = initialdir; 
   ofn.lpstrTitle = FileTitle; 

   ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
   if(GetOpenFileName(&ofn))  return strdup(szFileName);
   else return NULL;
}










