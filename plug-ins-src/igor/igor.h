#ifndef _IGOR_H_
#define _IGOR_H_

PXV_FUNC(int, do_igor_rescale_plot, (void));
PXV_FUNC(MENU*, igor_plot_menu, (void));
PXV_FUNC(int, do_igor_rescale_data_set, (void));
PXV_FUNC(int, igor_main, (int argc, char **argv));
#endif

