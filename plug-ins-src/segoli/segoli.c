/*
*    Plug-in program for image treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _SEGOLI_C_
#define _SEGOLI_C_

# include "allegro.h"
# include "xvin.h"

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "segoli.h"
//place here other headers of this plugin 


double 	*p1 = NULL;
double 	*p2 = NULL;


double **po = NULL;
int n_po_order = 0;            // maximum order of polynome
int n_po_m = 0, n_po_nx = 0;   // nx = 2 * m +1



int m_points = 0;		/* the number of points is actually 2*m +1 */


int 	poly_init2(int m);
int	init_p1(double *p1, int m);
int	init_p2(double *p2, int m);

int	poly_fit2(float *y, int nx, double *y0, double *y1, double *y2);


int	init_p1(double *p1, int m)
{
	register int i, j;
	double s;
	
	if (m <= 0)	return -1;
	for(i = -m, j = 0, s = 0; i <= m; i++, j++)
	{
		p1[j] = ((double)i)/m;
		s += p1[j]*p1[j];
	}
	for(j = 0, s = sqrt(s); j <= 2*m; j++)	p1[j] /= s;		
	return 0;
}
int	init_p2(double *p2, int m)
{
	register int i, j;
	double tmp, tmp1, x, s; 
	
	if (m <= 0)	return -1;
	tmp = ((double)1)/(m*(2*m-1));
	tmp1 = m * (m + 1);
	for(i = -m, j = 0, s = 0; i <= m; i++, j++)
	{
		x = (double)i;
		p2[j] = (3*x*x - tmp1)*tmp;
		s += p2[j]*p2[j];
	}
	for(j = 0, s = sqrt(s); j <= 2*m; j++)	p2[j] /= s;		
	return 0;
}

int	poly_fit2(float *y, int nx, double *y0, double *y1, double *y2)
{
	register int i;
	double tmp;
	
	if (nx%2 == 0) 			return -1;
	if (poly_init2((nx-1)/2)) 	return -1;
	tmp = (double)1/nx;
	if (y0 != NULL)
	{
		for (i = 0, *y0 = 0; i < nx; i++)  *y0 += y[i];
		*y0 *= tmp;
	}
	if (y1 != NULL)		for (i = 0, *y1 = 0; i < nx; i++)  *y1 += y[i]*p1[i];
	if (y2 != NULL)		for (i = 0, *y2 = 0; i < nx; i++)  *y2 += y[i]*p2[i];

	return 0;
}



 int 	poly_init2(int m)
{
	if (m <= 0)			return -1;
	if (m == m_points)	return 0; 
	p1 = (double *)realloc(p1, (2*m+1)*sizeof(double));
	p2 = (double *)realloc(p2, (2*m+1)*sizeof(double));
	m_points = m;
	if (p1 == NULL || p2 == NULL) 		return 1;
	init_p1(p1, m);
	init_p2(p2, m);
	return 0;
}

int do_segoli_average_along_y(void)
{
	register int i, j;
	int l_s, l_e;
	O_i *ois = NULL;
	O_p *op = NULL;
	d_s *ds;
	imreg *imr = NULL;


	if(updating_menu_state != 0)	return D_O_K;

	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine average the image intensity"
			"of several lines of an image");
	}
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	return D_O_K;
	/* we grap the data that we need, the image and the screen region*/
	l_s = 0; l_e = ois->im.ny;
	/* we ask for the limit of averaging*/
	i = win_scanf("Average Between lines%d and %d",&l_s,&l_e);
	if (i == CANCEL)	return D_O_K;
	/* we create a plot to hold the profile*/
	op = create_one_plot(ois->im.nx,ois->im.nx,0);
	if (op == NULL) return (win_printf_OK("cant create plot!"));
	ds = op->dat[0]; /* we grab the data set */
	op->y_lo = ois->z_black;	op->y_hi = ois->z_white;
	op->iopt2 |= Y_LIM;	op->type = IM_SAME_X_AXIS;
	inherit_from_im_to_ds(ds, ois);
	op->filename = Transfer_filename(ois->filename);
	set_plot_title(op,"segoli averaged profile from line %d to %d",l_s, l_e);
	set_formated_string(&ds->treatement,"%s averaged profile from line %d to %d",l_s, l_e);
	uns_oi_2_op(ois, IS_Z_UNIT_SET, op, IS_Y_UNIT_SET);
	uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
	for (i=0 ; i< ds->nx ; i++)	ds->yd[i] = 0;
	for (i=l_s ; i< l_e ; i++)
	{
		extract_raw_line(ois, i, ds->xd);
		for (j=0 ; j< ds->nx ; j++)	ds->yd[j] += ds->xd[j];
	}
	for (i=0, j = l_e - l_s ; i< ds->nx && j !=0 ; i++)
	{
		ds->xd[i] = i;
		ds->yd[i] /= j;
	}
	add_one_image (ois, IS_ONE_PLOT, (void *)op);
	ois->cur_op = ois->n_op - 1;
	broadcast_dialog_message(MSG_DRAW,0);
	return D_REDRAWME;
}

O_i	*segoli_image_multiply_by_a_scalar(O_i *ois, float factor)
{
	register int i, j;
	O_i *oid;
	float tmp;
	int onx, ony, data_type;
	union pix *ps, *pd;

	onx = ois->im.nx;	ony = ois->im.ny;	data_type = ois->im.data_type;
	oid =  create_one_image(onx, ony, data_type);
	if (oid == NULL)
	{
		win_printf("Can't create dest image");
		return NULL;
	}
	if (data_type == IS_CHAR_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].ch[j];
				pd[i].ch[j] = (tmp < 0) ? 0 :
					((tmp > 255) ? 255 : (unsigned char)tmp);
			}
		}
	}
	else if (data_type == IS_INT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].in[j];
				pd[i].in[j] = (tmp < -32768) ? -32768 :
					((tmp > 32767) ? 32767 : (short int)tmp);
			}
		}
	}
	else if (data_type == IS_UINT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].ui[j];
				pd[i].ui[j] = (tmp < 0) ? 0 :
					((tmp > 65535) ? 65535 : (unsigned short int)tmp);
			}
		}
	}
	else if (data_type == IS_LINT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].li[j];
				pd[i].li[j] = (tmp < 0x10000000) ? 0x10000000 :
					((tmp > 0x7FFFFFFF) ? 0x7FFFFFFF : (int)tmp);
			}
		}
	}
	else if (data_type == IS_FLOAT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].fl[j] = factor * ps[i].fl[j];
			}
		}
	}
	else if (data_type == IS_DOUBLE_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].db[j] = factor * ps[i].db[j];
			}
		}
	}
	else if (data_type == IS_COMPLEX_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].fl[2*j] = factor * ps[i].fl[2*j];
				pd[i].fl[2*j+1] = factor * ps[i].fl[2*j+1];
			}
		}
	}
	else if (data_type == IS_COMPLEX_DOUBLE_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].db[2*j] = factor * ps[i].db[2*j];
				pd[i].db[2*j+1] = factor * ps[i].db[2*j+1];
			}
		}
	}
	else if (data_type == IS_RGB_PICTURE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< 3*onx; j++)
			{
				tmp = factor * ps[i].ch[j];
				pd[i].ch[j] = (tmp < 0) ? 0 :
					((tmp > 255) ? 255 : (unsigned char)tmp);
			}
		}
	}
	else if (data_type == IS_RGBA_PICTURE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< 4*onx; j++)
			{
				tmp = factor * ps[i].ch[j];
				pd[i].ch[j] = (tmp < 0) ? 0 :
					((tmp > 255) ? 255 : (unsigned char)tmp);
			}
		}
	}
	inherit_from_im_to_im(oid,ois);
	uns_oi_2_oi(oid,ois);
	oid->im.win_flag = ois->im.win_flag;
	if (ois->title != NULL)	set_im_title(oid, "%s rescaled by %g",
		 ois->title,factor);
	set_formated_string(&oid->im.treatement,
		"Image %s rescaled by %g", ois->filename,factor);
	return oid;
}


float find_first_contact(O_i *ois, int xc, int yc, int rc, int zc)
{
  int i, j;
  int  rc2, ri, onx, ony;
  float z, zm;
  union pix *ps;
  
  rc2 = rc * rc;
  onx = ois->im.nx;	ony = ois->im.ny;	
  if (ois->im.data_type != IS_CHAR_IMAGE)
    return win_printf_OK("This is not a char image!");
  for (i = -rc, ps = ois->im.pixel, zm = 0; i < rc ; i++)
    {
      for (j = -rc; j< rc; j++)
	{
	  if (yc + i >= ony || yc + i < 0) continue; 
	  if (xc + j >= onx || xc + j < 0) continue; 
	  ri = i*i + j*j;
	  if (ri > rc2) continue;
	  z = ps[yc+i].ch[xc+j] + (float)zc * (float)ri/rc2;
	  if (z > zm) zm = z;
	}
    }
  return zm;
}


float find_nb_of_pixels_in_contact(O_i *ois, int xc, int yc, int rc, int zc, float z, float dz)
{
  int i, j;
  int  rc2, ri, onx, ony;
  float zm, cts;
  union pix *ps;
  
  rc2 = rc * rc;
  onx = ois->im.nx;	ony = ois->im.ny;	
  if (ois->im.data_type != IS_CHAR_IMAGE)
    return win_printf_OK("This is not a char image!");
  for (i = -rc, ps = ois->im.pixel, zm = 255, cts = 0; i < rc ; i++)
    {
      for (j = -rc; j< rc; j++)
	{
	  if (yc + i >= ony || yc + i < 0) continue; 
	  if (xc + j >= onx || xc + j < 0) continue; 
	  ri = i*i + j*j;
	  if (ri > rc2) continue;
	  zm = z  + ((float)zc * (float)ri/rc2) - ps[yc+i].ch[xc+j];
	  zm /= dz;
	  zm *= zm;
	  cts += exp(-zm);
	}
    }
  return cts;
}


float find_nb_of_good_pixels_in_contact(O_i *ois, int xc, int yc, int rc, int zc, float z, float dz)
{
  int i, j;
  int  rc2, ri, onx, ony;
  float zm, cts;
  union pix *ps;
  
  rc2 = rc * rc;
  onx = ois->im.nx;	ony = ois->im.ny;	
  if (ois->im.data_type != IS_CHAR_IMAGE)
    return win_printf_OK("This is not a char image!");
  for (i = -rc, ps = ois->im.pixel, zm = 255, cts = 0; i < rc ; i++)
    {
      for (j = -rc; j< rc; j++)
	{
	  if (yc + i >= ony || yc + i < 0) continue; 
	  if (xc + j >= onx || xc + j < 0) continue; 
	  ri = i*i + j*j;
	  if (ri > rc2) continue;
	  zm = z  + ((float)zc * (float)ri/rc2) - ps[yc+i].ch[xc+j];
	  zm /= dz;
	  zm *= zm;
	  cts += exp(-zm) * ri;
	}
    }
  return cts;
}


float find_contact_depth(O_i *ois, int xc, int yc, int rc, int zc, float z)
{
  int i, j, k;
  int  rc2, ri, onx, ony;
  float  zm, zd;
  union pix *ps;
  
  rc2 = rc * rc;
  onx = ois->im.nx;	ony = ois->im.ny;	
  if (ois->im.data_type != IS_CHAR_IMAGE)
    return win_printf_OK("This is not a char image!");
  for (i = -rc, ps = ois->im.pixel, zm = 255, zd = 0, k = 0; i < rc ; i++)
    {
      for (j = -rc; j< rc; j++)
	{
	  if (yc + i >= ony || yc + i < 0) continue; 
	  if (xc + j >= onx || xc + j < 0) continue; 
	  ri = i*i + j*j;
	  if (ri > rc2) continue;
	  k++;
	  zm = z  + ((float)zc * (float)ri/rc2) - ps[yc+i].ch[xc+j];
	  if (zm < 0) 
	    {
	      zd += zm;
	    }
	}
    }
  return (k > 0) ? zd/k : zd;
}

/*
  Find contact to the image surface for 

 */
int find_bar_contact_by_exp(O_i *ois,             // the image to search
			    float xc, float yc,   // the segment center position
			    int r, float angle,   // half size of segment and angle
			    float curvature,      // the curvature of the segment in z
			    float  exp_depth,     // the exponential length scale defining the contact 
			    float *zpos,          // the position
			    float *contact,       // the number of contact points
			    float *cur)
{
  int i, nf;
  int  onx, ony;
  float zm, x, y, fr, zinter[65];
  double tmp = 0, cont = 0, tmp1 = 0;
  double y0 = 0, y1 = 0, y2 = 0;
  O_p *op;
  d_s *ds, *ds2, *ds3;

  onx = ois->im.nx;	ony = ois->im.ny;	

  if (r > 32) return 2;
  fr = (float)(onx - r);
  if (xc >= fr) return 1;
  fr = (float)(ony - r);
  if (yc >= fr) return 1;
  fr = (float)r;
  if (xc < fr || yc < fr) return 1;

  nf = (1 + 2 * r);
  op = create_and_attach_op_to_oi(ois, nf, nf, 0, 0);
  if (op == NULL)	return win_printf_OK("Could not create plot data");
  ds = op->dat[0];

  if ((ds2 = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf("I can't create ds !");

  if ((ds3 = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf("I can't create ds !");


  for (i = -r, zm = 0; i <= r; i++)
    {
      x = xc + (float)(i * cos(angle));
      y = yc + (float)(i * sin(angle));
      interpolate_image_point(ois, x, y, zinter + r + i, NULL);
      zm += zinter[r+i];
      ds->xd[r+i] = ds2->xd[r+i] = i;
      ds->yd[r+i] = zinter[r+i];
    }
  zm /= (1 + 2 * r);
  //zm = zinter[r] - 100;
  //win_printf("zm = %g",zm);
  set_plot_title(op,"Zm = %g",zm);
  poly_fit2(zinter, (1 + 2 * r), &y0, &y1, &y2);
  //curvature = -curvature;
  for (i = -r, tmp = 0; i <= r; i++)
  //for (i = 0, tmp = 0; i < 1; i++)
    {
      //ds2->yd[r+i] = zm - (curvature * i * i);
      ds3->yd[r+i] = tmp1 = (zinter[r+i] - zm + (curvature * i * i));
      //win_printf("i = %d dz = %g dznorm %g exp %g",i, tmp1, tmp1/exp_depth, exp(tmp1/exp_depth));
      tmp1 /= exp_depth;
      tmp = tmp + exp(-tmp1);
      ds3->xd[r+i] = i;
    }
  tmp /= (1 + 2 * r);
  //win_printf("bef div tmp = %g",(float)tmp);
  if (tmp <= 0.0) return 3;
  tmp = -log(tmp);
  //win_printf("- log tmp = %g",(float)tmp);
  tmp *= exp_depth;
  //win_printf("tmp = %g zpos = %g",(float)tmp,zm+tmp);

  for (i = -r, cont = 0; i <= r; i++)
    {
      tmp1 =  zm - (curvature * i * i) + tmp;
      ds2->yd[r+i] = tmp1;
      tmp1 -= zinter[r+i];
      cont += exp(-fabs(tmp1/exp_depth));
      //win_printf("i = %d dz = %g con %g",i, tmp1, exp(-fabs(tmp1/exp_depth)));
    }

  

  //win_printf("log tmp = %g",tmp);
  if (zpos) *zpos = zm + tmp;
  if (contact) *contact = cont;
  if (cur)  *cur = (float)((p2[r] - p2[r+1]) * y2);
  return 0;
}


int find_bar_contact_by_exp_2(O_i *ois,             // the image to search
			      float xc, float yc,   // the segment center position
			      int r, float angle,   // half size of segment and angle
			      float curvature,      // the curvature of the segment in z
			      float  exp_depth,     // the exponential length scale defining the contact 
			      float *zpos,          // the position
			      float *contact)       // the number of contact points

{
  int i, nf;
  int  onx, ony;
  float zm, x, y, fr, zinter[65];
  double tmp = 0, cont = 0, tmp1 = 0;

  onx = ois->im.nx;	ony = ois->im.ny;	

  if (r > 32) return 2;
  fr = (float)(onx - r);
  if (xc >= fr) return 1;
  fr = (float)(ony - r);
  if (yc >= fr) return 1;
  fr = (float)r;
  if (xc < fr || yc < fr) return 1;

  nf = (1 + 2 * r);

  for (i = -r, zm = 0; i <= r; i++)
    {
      x = xc + (float)(i * cos(angle));
      y = yc + (float)(i * sin(angle));
      interpolate_image_point(ois, x, y, zinter + r + i, NULL);
      zm += zinter[r+i];
    }
  zm /= (1 + 2 * r);
  for (i = -r, tmp = 0; i <= r; i++)
    {
      tmp1 = (zinter[r+i] - zm + (curvature * i * i));
      tmp1 /= exp_depth;
      tmp = tmp + exp(-tmp1);
    }
  tmp /= (1 + 2 * r);
  if (tmp <= 0.0) return 3;
  tmp = -log(tmp);
  tmp *= exp_depth;
  for (i = -r, cont = 0; i <= r; i++)
    {
      tmp1 =  zm - (curvature * i * i) + tmp;
      tmp1 -= zinter[r+i];
      cont += exp(-fabs(tmp1/exp_depth));
    }
  if (zpos) *zpos = zm + tmp;
  if (contact) *contact = cont;
  return 0;
}




int do_find_bar_contact_by_exp(void)
{
  imreg *imr;
  int i;
  O_i *ois;             // the image to search
  static float xc = 0, yc = 0;         // the segment center position
  static int r = 3;
  static float angle = 0;          // half size of segment and angle
  static float curvature = 2;      // the curvature of the segment in z
  static float exp_depth = 2;      // the exponential length scale defining the contact 
  static float zpos = 0, cont = 0, cur = 0; // the position

  if(updating_menu_state != 0)	return D_O_K;
  
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the image intensity"
			   "by a user defined scalar factor");
    }
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      win_printf("Cannot find image");
      return D_O_K;
    }
  i = win_scanf("define segment center xc %8f yc %8f\n"
		"half segment size %8d\n"
		"curvature %8f \n"
		"angle %8f\n"
		"depth exponential decay %8f\n"
		,&xc,&yc,&r,&curvature,&angle,&exp_depth);
  if (i == CANCEL)	return D_O_K;
  
  i = find_bar_contact_by_exp(ois, xc, yc, r, angle, curvature, exp_depth, &zpos, &cont, &cur);
  win_printf("Point (%g %g)\n segment size %d curvature %g\n"
	     "angle %g exponential depth %g\nerror %d zpos = %g contacts %g cur %g\n",
	     xc, yc, r, curvature, angle, exp_depth, i, zpos, cont, cur);
  return 0;
}


int find_tip_contact_by_exp(O_i *ois,             // the image to search
			    float xc, float yc,   // the paraboloid center position
			    int r,                // the paraboloid radius
			    float curvature,      // the curvature of the paraboloid
			    float  exp_depth,     // the exponential length scale defining the contact 
			    float *zpos,          // the position
			    float *contact,       // the number of contact points
			    float *angle,         // the angular diection of the contact points
			    int do_plot)
{
  int i, j, nf;
  int  onx, ony, ixs, ixe, iys, iye;
  float fr, frm, exp_cont;
  double tmp = 0, cont = 0, tmp1 = 0, zme, zm, val, ang;
  O_p *op;
  d_s *ds;

  onx = ois->im.nx;	ony = ois->im.ny;	

  if (r > 32) return 2;

  ixs = (int)xc - r;
  if (ixs < 0) return 1;
  ixe = (int)xc + r + 1;
  if (ixe > onx) return 1;

  iys = (int)yc - r;
  if (iys < 0) return 1;
  iye = (int)yc + r + 1;
  if (iye > ony) return 1;
  frm = r * r;
  exp_cont = exp_depth/3;
  for (i = iys, zm = 0, nf = 0; i < iye; i++)
    {
      for (j = ixs; j < ixe; j++)
	{
	  tmp = ((float)j) - xc;
	  fr = tmp * tmp;
	  tmp = ((float)i) - yc;
	  fr += tmp * tmp;
	  if (fr <= frm)
	    {
	      get_raw_pixel_value(ois, j, i, &val);
	      zm += val;
	      nf++;
	    }
	}
    }
  if (nf > 0) zm /= nf;
  //win_printf("Zm = %g",zm);
  for (i = iys, zme = 0; i < iye; i++)
    {
      for (j = ixs; j < ixe; j++)
	{
	  tmp = ((float)j) - xc;
	  fr = tmp * tmp;
	  tmp = ((float)i) - yc;
	  fr += tmp * tmp;
	  if (fr <= frm)
	    {
	      get_raw_pixel_value(ois, j, i, &val);
	      tmp1 = (val - zm + (curvature * fr));
	      tmp1 /= exp_cont;
	      zme += exp(-tmp1);
	    }
	}
    }
  if (nf > 0) zme /= nf;
  if (zme <= 0.0) return 3;
  zme = -log(zme);
  zme *= exp_cont;
  //win_printf("Zme = %g",zme);
  if (do_plot)
    {
      op = create_and_attach_op_to_oi(ois, 16, 16, 0, IM_SAME_Y_AXIS|IM_SAME_X_AXIS);
      if (op == NULL)	return win_printf_OK("Could not create plot data");
      ds = op->dat[0];
      set_dot_line(ds);
      set_plot_symb(ds, "\\pt2\\oc");
      set_ds_line_color(ds, Lightred);
      ds->nx = ds->ny = 0;
    }
  for (i = iys, cont = ang = 0; i < iye; i++)
    {
      for (j = ixs; j < ixe; j++)
	{
	  tmp = ((float)j) - xc;
	  fr = tmp * tmp;
	  tmp = ((float)i) - yc;
	  fr += tmp * tmp;
	  if (fr <= frm)
	    {
	      get_raw_pixel_value(ois, j, i, &val);
	      tmp1 = (val - zm + (curvature * fr) - zme);
	      tmp1 /= exp_depth;
	      tmp1 = fabs(tmp1);
	      tmp1 = exp(-tmp1);
	      cont += tmp1;
	      //ang += fr*tmp1;
	      ang += val*tmp1;
	      if (do_plot && tmp1 > 0.37)
		add_new_point_to_ds(ds, j, i);
	    }
	}
    }
  if (zpos) *zpos = zm + tmp;
  if (contact) *contact = cont;
  if (angle)  *angle = ang;
  return 0;
}

int do_find_tip_contact_by_exp(void)
{
  imreg *imr;
  int i;
  O_i *ois;                            // the image to search
  static float xc = 0, yc = 0;         // the paraboloid center position
  static int r = 3;                    // the paraboloid radius
  static float curvature = 2;          // the curvature of the segment in z
  static float exp_depth = 2;          // the exponential length scale defining the contact 
  static float zpos = 0, cont = 0;     // the position
  float angle = 0;          

  if(updating_menu_state != 0)	return D_O_K;
  
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the image intensity"
			   "by a user defined scalar factor");
    }
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      win_printf("Cannot find image");
      return D_O_K;
    }
  i = win_scanf("define segment center xc %8f yc %8f\n"
		"half segment size %8d\n"
		"curvature %8f \n"
		"angle %8f\n"
		"depth exponential decay %8f\n"
		,&xc,&yc,&r,&curvature,&angle,&exp_depth);
  if (i == CANCEL)	return D_O_K;
  


  i = find_tip_contact_by_exp(ois, xc, yc, r, curvature, exp_depth, &zpos, &cont, &angle, 1);
  win_printf("Point (%g %g)\n paraboloid size %d curvature %g\n"
	     "exponential depth %g\nerror %d zpos = %g contacts %g angle %g\n",
	     xc, yc, r, curvature, exp_depth, i, zpos, cont, angle);
  return 0;
}


int do_filter_by_tip_contact_by_exp(void)
{
  imreg *imr;
  int i, j, k;
  O_i *ois, *oid, *oid2;                      // the image to search
  static int over_x = 4;               // the oversampling 
  static int r = 3;                    // the paraboloid radius
  static float curvature = 2;          // the curvature of the segment in z
  static float exp_depth = 2;          // the exponential length scale defining the contact 
  static float zpos = 0, cont = 0;     // the position
  static float threshold = 26500; 
  float angle = 0;          

  if(updating_menu_state != 0)	return D_O_K;
  
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the image intensity"
			   "by a user defined scalar factor");
    }
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      win_printf("Cannot find image");
      return D_O_K;
    }
  i = win_scanf("define the oversampling in x and y %8d \n"
		"half tip size %8d\n"
		"curvature %8f \n"
		"depth exponential decay %8f\nThreshold %8f\n"
		,&over_x,&r,&curvature,&exp_depth,&threshold);
  if (i == CANCEL)	return D_O_K;
  
  oid = create_and_attach_oi_to_imr(imr, ois->im.nx * over_x, ois->im.ny * over_x, IS_FLOAT_IMAGE);
  if (oid == NULL)	return win_printf_OK("cannot create profile !");
  oid2 = create_and_attach_oi_to_imr(imr, ois->im.nx * over_x, ois->im.ny * over_x, IS_FLOAT_IMAGE);
  if (oid2 == NULL)	return win_printf_OK("cannot create profile !");
  for (i = 0; i < oid->im.ny; i++)
    {
      my_set_window_title("Doing %d / %d lines",i,oid->im.ny);
      for (j = 0; j < oid->im.nx; j++)
	{
	  k = find_tip_contact_by_exp(ois, ((float)j)/over_x, ((float)i)/over_x, r, curvature, exp_depth, &zpos, &cont, &angle, 0);
	  if (k == 0)     
	    {
	      oid->im.pixel[i].fl[j] = angle;
	      oid2->im.pixel[i].fl[j] = angle - cont * threshold;
	    }
	}
    }
  find_zmin_zmax(oid);
  refresh_image(imr, imr->n_oi - 1);
}




int do_segoli_image_multiply_by_a_scalar(void)
{
	O_i *ois;
	O_p *op;
	d_s *ds;
	imreg *imr;
	int i;
	static int xc = 0, yc = 0, rc = 5, zc = 5;
	static float dz = 1;


	if(updating_menu_state != 0)	return D_O_K;

	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine multiply the image intensity"
		"by a user defined scalar factor");
	}
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	{
		win_printf("Cannot find image");
		return D_O_K;
	}
	i = win_scanf("define xc %8d yc %8d\nrc %8d zc %8d \ndz %12f\n",&xc,&yc,&rc,&zc,&dz);
	if (i == CANCEL)	return D_O_K;
	if (ois->im.data_type != IS_CHAR_IMAGE)
	  return win_printf_OK("This is not a char image!");

	op = create_and_attach_op_to_oi(ois, 256, 256, 0, 0);
	if (op == NULL)	return win_printf_OK("Could not create plot data");
	ds = op->dat[0];
	inherit_from_im_to_ds(ds, ois);	
	for (i = 0; i < 256; i++)
	  {
	    ds->xd[i] = i;
	    ds->yd[i] = find_nb_of_pixels_in_contact(ois, xc, yc, rc, zc, i, dz);
	  }
	if ((ds = create_and_attach_one_ds(op, 256, 256, 0)) == NULL)
	return win_printf("I can't create ds !");
	for (i = 0; i < 256; i++)
	  {
	    ds->xd[i] = i;
	    ds->yd[i] = find_contact_depth(ois, xc, yc, rc, zc, i);
	  }

	op->filename = Transfer_filename(ois->filename);	
	set_plot_title(op,"Contact profile xc %d yc %d first %f",xc, yc,
		       find_first_contact(ois, xc, yc, rc, zc));
	set_formated_string(&ds->treatement,"Contact profile xc %d yc %d",xc, yc);
	uns_oi_2_op(ois, IS_Z_UNIT_SET, op, IS_Y_UNIT_SET);
	uns_oi_2_op(ois, IS_T_UNIT_SET, op, IS_X_UNIT_SET);
	ois->need_to_refresh |= PLOT_NEED_REFRESH  | EXTRA_PLOT_NEED_REFRESH;	
	return D_REDRAWME;		
	/*
	rc2 = rc * rc;
	onx = ois->im.nx;	ony = ois->im.ny;	

	for (i = -rc, ps = ois->im.pixel, zm = 255; i < rc ; i++)
	  {
	    for (j = -rc; j< rc; j++)
	      {
		if (yc + rc >= ony || yc - rc < 0) continue; 
		if (xc + rc >= onx || xc - rc < 0) continue; 
		ri = i*i + j*j;
		if (ri > rc2) continue;
		z = ps[yc+i].ch[xc+j] + (float)zc * (float)ri/rc2;
		if (z < zm) zm = z;
	      }
	  }
	win_printf("xc %d yc %d zm %f",xc,yc,zm);
	return (refresh_image(imr, imr->n_oi - 1));
	*/
}



int do_segoli_image_multiply_by_a_scalar_2(void)
{
  O_i *ois, *oid;
  O_p *op;
  d_s *ds, *ds2;
  imreg *imr;
  union pix *pd;
  int i, j, k;
  static int xc = 0, yc = 0, rc = 5, zc = 5;
  int  rc2, onx, ony;
  static float dz = 1;


	if(updating_menu_state != 0)	return D_O_K;

	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine multiply the image intensity"
		"by a user defined scalar factor");
	}
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	{
		win_printf("Cannot find image");
		return D_O_K;
	}
	i = win_scanf("define xc %8d yc %8d\nrc %8d zc %8d \ndz %12f\n",&xc,&yc,&rc,&zc,&dz);
	if (i == CANCEL)	return D_O_K;
	if (ois->im.data_type != IS_CHAR_IMAGE)
	  return win_printf_OK("This is not a char image!");

	op = create_and_attach_op_to_oi(ois, 256, 256, 0, 0);
	if (op == NULL)	return win_printf_OK("Could not create plot data");
	ds = op->dat[0];
	inherit_from_im_to_ds(ds, ois);	
	for (i = 0; i < 256; i++)
	  {
	    ds->xd[i] = i;
	    ds->yd[i] = find_nb_of_pixels_in_contact(ois, xc, yc, rc, zc, i, dz);
	  }
	if ((ds2 = create_and_attach_one_ds(op, 256, 256, 0)) == NULL)
	return win_printf("I can't create ds !");
	for (i = 0; i < 256; i++)
	  {
	    ds2->xd[i] = i;
	    ds2->yd[i] = find_contact_depth(ois, xc, yc, rc, zc, i);
	  }

	op->filename = Transfer_filename(ois->filename);	
	set_plot_title(op,"Contact profile xc %d yc %d first %f",xc, yc,
		       find_first_contact(ois, xc, yc, rc, zc));
	set_formated_string(&ds->treatement,"Contact profile xc %d yc %d",xc, yc);
	uns_oi_2_op(ois, IS_Z_UNIT_SET, op, IS_Y_UNIT_SET);
	uns_oi_2_op(ois, IS_T_UNIT_SET, op, IS_X_UNIT_SET);
	ois->need_to_refresh |= PLOT_NEED_REFRESH  | EXTRA_PLOT_NEED_REFRESH;	

	rc2 = rc * rc;
	onx = ois->im.nx;	ony = ois->im.ny;	

	oid = create_and_attach_oi_to_imr(imr, onx, ony, IS_FLOAT_IMAGE);
	if (oid == NULL)      	  return win_printf_OK("Can't create dest movie");

	inherit_from_im_to_im(oid,ois);
	uns_oi_2_oi(oid,ois);
	oid->im.win_flag = ois->im.win_flag;
	if (ois->title != NULL)	set_im_title(oid, "%s ", ois->title);



	for (i = 0, pd = oid->im.pixel; i < ony ; i++)
	  {
	    for (j = 0; j< onx; j++)
	      {
		for (k = (int)find_first_contact(ois, xc, yc, rc, zc); k > 0 
		       && find_contact_depth(ois, j, i, rc, zc, k) > -dz; k--);
		pd[i].fl[j] = find_nb_of_good_pixels_in_contact(ois, j, i, rc, zc, k, dz);
	      }
	    my_set_window_title("Doing %d / %d lines",i,ony);
	  }
	find_zmin_zmax(oid);
	return (refresh_image(imr, imr->n_oi - 1));

}


int	do_project_on_Z_profile(void)
{
  int i, j, nf, k;
  O_i   *ois, *oid;
  O_p *op;
  d_s *dsi, *dst;
  imreg *imr;
  float  mean;
  union pix  *pd;
	
  if(updating_menu_state != 0)	return D_O_K;		

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	
    return win_printf_OK("This routine project z profiles\n");

  nf = abs(ois->im.n_f);		
  if (nf <= 0)	
    {
      win_printf("This is not a movie");
      return D_O_K;
    }


  if (ois->cur_op < 0)    
    return win_printf_OK("This routine requires a plot as profile\n");
  op = ois->o_p[imr->one_i->cur_op];
  dsi = op->dat[0];

  if (dsi->nx != nf)  
    return win_printf_OK("your plot has not the same size than your movie\n");

  dst = build_data_set(nf,nf);
  if (dst == NULL)	return win_printf_OK("cannot create ds !");

  oid = create_and_attach_oi_to_imr(imr, ois->im.nx, ois->im.ny, IS_FLOAT_IMAGE);
  if (oid == NULL)	return win_printf_OK("cannot create profile !");

  for(i = 0, mean = 0; i < nf; i++)
    mean += dsi->yd[i];
  mean /= nf;
  for(i = 0; i < nf; i++)
    dsi->yd[i] -= mean;

  for(i = 0, pd = oid->im.pixel; i < ois->im.ny; i++)
    {
      for(j = 0; j < ois->im.nx; j++)
	{
	  extract_z_profile_from_movie(ois, j, i, dst->yd);	  
	  for(k = 0,pd[i].fl[j] = 0; k < nf; k++)
	    pd[i].fl[j] += dsi->yd[k] * dst->yd[k];
	  pd[i].fl[j] /= nf;
	}
      my_set_window_title("Processing line %d",i);
    }


  for(i = 0; i < nf; i++)
    dsi->yd[i] += mean;

  inherit_from_im_to_im(oid, ois);	
  oid->filename = Transfer_filename(ois->filename);	
  find_zmin_zmax(oid);
  uns_oi_2_oi(oid,ois);	/* heritage des unites de l'image 1 			*/
  oid->im.win_flag = ois->im.win_flag;	
  oid->need_to_refresh = ALL_NEED_REFRESH;		
  free_data_set(dst);
  return (refresh_image(imr, imr->n_oi - 1));
}


int	do_project_on_Z_profile_uc(void)
{
  int i, j, nf, k;
  O_i   *ois, *oid;
  O_p *op;
  d_s *dsi, *dst;
  int *xi;
  imreg *imr;
  float  mean;
  union pix  *pd, *ps;
	
  if(updating_menu_state != 0)	return D_O_K;		

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	
    return win_printf_OK("This routine project z profiles\n");

  nf = abs(ois->im.n_f);		
  if (nf <= 0)	
    {
      win_printf("This is not a movie");
      return D_O_K;
    }


  if (ois->cur_op < 0)    
    return win_printf_OK("This routine requires a plot as profile\n");
  op = ois->o_p[imr->one_i->cur_op];
  dsi = op->dat[0];

  if (dsi->nx != nf)  
    return win_printf_OK("your plot has not the same size than your movie\n");

  dst = build_data_set(nf,nf);
  if (dst == NULL)	return win_printf_OK("cannot create ds !");

  oid = create_and_attach_oi_to_imr(imr, ois->im.nx, ois->im.ny, IS_FLOAT_IMAGE);
  if (oid == NULL)	return win_printf_OK("cannot create profile !");


  xi = (int*)calloc(nf,sizeof(int));
  if (xi == NULL)	return win_printf_OK("cannot create int array !");

  for(i = 0; i < nf; i++) xi[i] = (int)(dsi->yd[i] + 0.5);

  for(i = 0, mean = 0; i < nf; i++)
    mean += xi[i];
  mean /= nf;
  for(i = 0; i < nf; i++)
    xi[i] -= mean;

  /*
  for(i = 0, pd = oid->im.pixel; i < ois->im.ny; i++)
    {
      for(j = 0; j < ois->im.nx; j++)
	{
	  for(k = 0, tmpi = 0; k < nf; k++)
	    tmpi += ois->im.pxl[k][i].ch[j]*xi[k];
	  pd[i].fl[j] = (float)tmpi;
	  pd[i].fl[j] /= nf;
	}
      my_set_window_title("Processing line %d",i);
    }
  */


  for(k = 0, pd = oid->im.pixel; k < nf; k++)
    {
      ps = ois->im.pxl[k];
      for(i = 0; i < ois->im.ny; i++)
	{
	  for(j = 0; j < ois->im.nx; j++)
	      pd[i].fl[j] += ps[i].ch[j]*xi[k];
	}
      my_set_window_title("Processing im %d/%d",k,nf);
    }

  for(i = 0; i < ois->im.ny; i++)
    {
      for(j = 0; j < ois->im.nx; j++)
	  pd[i].fl[j] /= nf;
    }
  
  inherit_from_im_to_im(oid, ois);	
  oid->filename = Transfer_filename(ois->filename);	
  find_zmin_zmax(oid);
  uns_oi_2_oi(oid,ois);	/* heritage des unites de l'image 1 			*/
  oid->im.win_flag = ois->im.win_flag;	
  oid->need_to_refresh = ALL_NEED_REFRESH;		
  free_data_set(dst);
  free(xi);
  return (refresh_image(imr, imr->n_oi - 1));
}



int	do_average_movie_on_Z_profile(void)
{
  int i, j, nf, k, nf2, k2, l;
  static int avg = 10;
  O_i   *ois, *oid;
  O_p *op;
  d_s  *dst;
  imreg *imr;
  union pix  *pd;
	
  if(updating_menu_state != 0)	return D_O_K;		

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	
    return win_printf_OK("This routine project z profiles\n");

  nf = abs(ois->im.n_f);		
  if (nf <= 0)	
    {
      win_printf("This is not a movie");
      return D_O_K;
    }

  i = win_scanf("Over how many frame do you want to average\n this movie %8d\n",&avg);
  if (i == CANCEL)   return 0;

  nf2 = nf/avg;
  //  dst = build_data_set(nf,nf);
  //if (dst == NULL)	return win_printf_OK("cannot create ds !");




  oid = create_and_attach_movie_to_imr(imr, ois->im.nx, ois->im.ny, ois->im.data_type, nf2);
  if (oid == NULL)	return win_printf_OK("cannot create profile !");

  op = create_and_attach_op_to_oi(oid, nf, nf, 0, 0);
  if (op == NULL)	return win_printf_OK("Could not create plot data");
  dst = op->dat[0];



  for(i = 0, pd = oid->im.pixel; i < ois->im.ny; i++)
    {
      for(j = 0; j < ois->im.nx; j++)
	{
	  extract_z_profile_from_movie(ois, j, i, dst->yd);	  
	  for(k = 0; k < nf2; k++)  dst->xd[k] = 0;
	  for(k = 0, k2 = 0, l = 0; k < nf; k++, l++)
	    {
	      if (l >= avg) 
		{
		  dst->xd[k2] /= l;
		  k2++;
		  l = 0;
		}
	      dst->xd[k2] += dst->yd[k];
	    }
	  if (l) dst->xd[k2] /= l;
	  for(k = 0; k < nf2; k++)  dst->xd[k] = (int)(dst->xd[k]+0.5);
	  set_z_profile_from_movie(oid, j, i, dst->xd, nf2);	  
	}
      my_set_window_title("Processing line %d",i);
    }

  inherit_from_im_to_im(oid, ois);	
  oid->filename = Transfer_filename(ois->filename);	
  find_zmin_zmax(oid);
  uns_oi_2_oi(oid,ois);	/* heritage des unites de l'image 1 			*/
  oid->im.win_flag = ois->im.win_flag;	
  oid->need_to_refresh = ALL_NEED_REFRESH;		
  return (refresh_image(imr, imr->n_oi - 1));
}



MENU *segoli_image_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"average profile", do_segoli_average_along_y,NULL,0,NULL);
	add_item_to_menu(mn,"image z rescaled", do_segoli_image_multiply_by_a_scalar,NULL,0,NULL);
	add_item_to_menu(mn,"image z rescaled 2", do_segoli_image_multiply_by_a_scalar_2,NULL,0,NULL);
	add_item_to_menu(mn,"project profile", do_project_on_Z_profile,NULL,0,NULL);
	add_item_to_menu(mn,"project profile (unsigned char)", do_project_on_Z_profile_uc,NULL,0,NULL);
	add_item_to_menu(mn,"average movie in Z", do_average_movie_on_Z_profile,NULL,0,NULL);
	add_item_to_menu(mn,"find Segment contact with eponential weight",do_find_bar_contact_by_exp,NULL,0,NULL);
	add_item_to_menu(mn,"find Tip contact with eponential weight",do_find_tip_contact_by_exp,NULL,0,NULL);
	add_item_to_menu(mn,"filyer by Tip contact",do_filter_by_tip_contact_by_exp,NULL,0,NULL);
	return mn;
}

int	segoli_main(int argc, char **argv)
{
	add_image_treat_menu_item ( "segoli", NULL, segoli_image_menu(), 0, NULL);
	return D_O_K;
}

int	segoli_unload(int argc, char **argv)
{
	remove_item_to_menu(image_treat_menu, "segoli", NULL, NULL);
	return D_O_K;
}
#endif

