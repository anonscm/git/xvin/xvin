#ifndef _SEGOLI_H_
#define _SEGOLI_H_

PXV_FUNC(int, do_segoli_average_along_y, (void));
PXV_FUNC(MENU*, segoli_image_menu, (void));
PXV_FUNC(int, segoli_main, (int argc, char **argv));
#endif

