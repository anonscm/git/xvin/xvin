#include "boxplot.hh"
#include <map>
#include <vector>
#include <algorithm>
#include <cmath>


int do_box_from_ds(void)
{
    int use_quartile = 1;
    int create_plot = 0;
    int win_ret = D_O_K;
    pltreg *pr = NULL;
    O_p *op = NULL;
    O_p *dest_op = NULL;
    if (updating_menu_state != 0)
    {
        active_menu->flags &= ~D_DISABLED;
        return D_O_K;
    }

    ac_grep(cur_ac_reg, "%pr%op", &pr, &op);
    if (pr == NULL || op == NULL)
    {
        return D_O_K;
    }
    win_ret = win_scanf("%R use standard deviation %r use quartile \n"
              "%b put result in a new plot", &use_quartile, &create_plot);

    if (win_ret != D_O_K)
    {
        return D_O_K;
    }

    if (create_plot)
    {
        dest_op = create_and_attach_one_plot(pr, 1, 1, 0);
    }

    boxplot_from_ds(op, op->cur_dat, dest_op, use_quartile);
    refresh_plot(pr, pr->n_op - 1);
    return D_O_K;
}

float dist (float x, float y)
{
   return std::sqrt(std::pow(x - y, 2));
}

int boxplot_from_ds(O_p *src_op,
                    int src_ds_idx,
                    O_p *dest_op,
                    bool use_quartile)
{
    std::map<int, std::vector<float>> xvals;
    std::map<int, float> xvalsum;
    int dsi = src_ds_idx;
    int src_dat_end = src_ds_idx + 1;
    d_s *dest_ds = NULL;
    std::map<int, std::vector<float>>::iterator it;

    if (dest_op == NULL)
    {
        dest_op = src_op;
    }

    if (src_ds_idx < 0)
    {
        dsi = 0;
        src_dat_end = src_op->n_dat;
    }

    for (; dsi < src_dat_end; ++dsi)
    {
        d_s *ds = src_op->dat[dsi];

        for (int i = 0; i < ds->nx; ++i)
        {
            it = xvals.find(round(ds->xd[i]));
            if (it == xvals.end())
            {
                auto vect = std::vector<float>();
                vect.push_back(ds->yd[i]);

                xvals.insert(std::pair<int,std::vector<float>>(ds->xd[i], vect));
            }
            else
            {
                it->second.push_back(ds->yd[i]);
            }
        }
    }

    dest_ds = create_and_attach_one_ds(dest_op, xvals.size(), xvals.size(), 0);
    alloc_data_set_y_box(dest_ds);
    alloc_data_set_y_error(dest_ds);
    alloc_data_set_y_down_error(dest_ds);
    dest_ds->boxplot_width = 0.3;
    int idx = 0;

    for (auto val_pair : xvals)
    {
        std::vector<float> &vals = val_pair.second;
        std::sort(vals.begin(), vals.end()); // FIXME : sort directly when filling in
        int vals_size = vals.size();
        dest_ds->xd[idx] = val_pair.first;

        if (use_quartile)
        {
            float median = vals_size % 2 == 0 ?
                           (vals[vals_size / 2 - 1] + vals[vals_size / 2]) / 2 :
                           vals[vals_size / 2];
            float first_quartile = vals_size % 4 == 0 ?
                                   (vals[vals_size / 4 - 1] + vals[vals_size / 4]) / 2 :
                                   vals[vals_size / 4];
            float third_quartile = vals_size % 4 == 0 ?
                                   (vals[(vals_size * 3) / 4 - 1] + vals[(vals_size * 3) / 4]) / 2 :
                                   vals[(vals_size * 3) / 4];
            dest_ds->yed[idx] = dist(vals.front(), median);
            dest_ds->ybd[idx] = dist(first_quartile, median);
            dest_ds->yd[idx] = median;
            dest_ds->ybu[idx] = dist(third_quartile, median);
            dest_ds->ye[idx] = dist(vals.back(), median);
        }
        else
        {
            float sum = 0;
            float sum_sq = 0;
            float min = FLT_MAX;
            float max = -FLT_MAX;
            float avg = 0;

            for (int i = 0; i < vals_size; ++i)
            {
                float val = vals[i];

                if (val < min)
                {
                    min = val;
                }

                if (val > max)
                {
                    max = val;
                }

                sum += val;
                sum_sq += std::pow(val, 2);
            }

            avg = sum / vals_size;
            dest_ds->yed[idx] = dist(min,avg);
            dest_ds->ybd[idx] = std::sqrt((sum_sq / vals_size - std::pow(sum / vals_size, 2)));
            dest_ds->yd[idx] = avg;
            dest_ds->ybu[idx] = dest_ds->ybd[idx];
            dest_ds->ye[idx] = dist(max,avg);
        }

        idx++;
    }

    dest_op->need_to_refresh = ALL_NEED_REFRESH;
    return 0;
}
int boxplot_main(int argc, char **argv)
{
    add_plot_treat_menu_item("create boxplot", do_box_from_ds, NULL, 0, NULL);
    return D_O_K;
}

int boxplot_unload(int /*argc*/, char ** /*argv*/)
{
    remove_item_to_menu(plot_treat_menu, "create boxplot", NULL, NULL);
    return D_O_K;
}
