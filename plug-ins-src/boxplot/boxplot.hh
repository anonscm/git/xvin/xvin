#pragma once
#include <xvin.h>

PXV_FUNC(int, boxplot_from_ds, (O_p *src_op,
                    int src_ds_idx,
                    O_p *dest_op,
                    bool use_quartile));
PXV_FUNC(int, do_box_from_ds, (void));
PXV_FUNC(int, boxplot_main, (int argc, char **argv));
PXV_FUNC(int, boxplot_unload, (int /*argc*/, char ** /*argv*/));
