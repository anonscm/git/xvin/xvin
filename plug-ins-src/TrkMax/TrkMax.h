#ifndef _TRKMAX_H_
#define _TRKMAX_H_

PXV_FUNC(int, do_TrkMax_average_along_y, (void));
PXV_FUNC(MENU*, TrkMax_image_menu, (void));
PXV_FUNC(int, TrkMax_main, (int argc, char **argv));
#endif

