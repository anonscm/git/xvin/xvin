/*
*    Plug-in program for image treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _TRKMAX_C_
#define _TRKMAX_C_

# include "float.h"
# include "allegro.h"
# include "xvin.h"
# include "unitset.h"

XV_VAR(un_s*, to_modify_un);
XV_FUNC(int, pop_unit_decade_mn, (void));
XV_FUNC(int, pop_unit_type_mn, (void));

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "TrkMax.h"
//place here other headers of this plugin 


# ifdef EN_COURS

double 	*p1 = NULL;
double 	*p2 = NULL;
double 	*p3 = NULL;
double 	*p4 = NULL;
double 	*p5 = NULL;

double 	*dp1 = NULL;
double 	*dp2 = NULL;
double 	*dp3 = NULL;
double 	*dp4 = NULL;
double 	*dp5 = NULL;


double **po = NULL;
int n_po_order = 0;            // maximum order of polynome
int n_po_m = 0, n_po_nx = 0;   // nx = 2 * m +1


int 	poly_init(int m);
int		init_p1(double *p1, int m);
int		init_p2(double *p2, int m);
int		init_p3(double *p3, int m);
int		init_p4(double *p4, int m);
int		init_p5(double *p5, int m);


int		init_p1(double *p1, int m)
{
	register int i, j;
	double s;
	
	if (m <= 0)	return -1;
	for(i = -m, j = 0, s = 0; i <= m; i++, j++)
	{
		p1[j] = ((double)i)/m;
		s += p1[j]*p1[j];
	}
	for(j = 0, s = sqrt(s); j <= 2*m; j++)	p1[j] /= s;		
	return 0;
}
int		init_p2(double *p2, int m)
{
	register int i, j;
	double tmp, tmp1, x, s; 
	
	if (m <= 0)	return -1;
	tmp = ((double)1)/(m*(2*m-1));
	tmp1 = m * (m + 1);
	for(i = -m, j = 0, s = 0; i <= m; i++, j++)
	{
		x = (double)i;
		p2[j] = (3*x*x - tmp1)*tmp;
		s += p2[j]*p2[j];
	}
	for(j = 0, s = sqrt(s); j <= 2*m; j++)	p2[j] /= s;		
	return 0;
}
int		init_p3(double *p3, int m)
{
	register int i, j;
	double tmp, tmp1, x, s; 
	
	if (m <= 0)	return -1;
	tmp = ((double)1)/(m*(2*m-1)*(m-1));
	tmp1 = 3*m*m + 3*m - 1;
	for(i = -m, j = 0, s = 0; i <= m; i++, j++)
	{
		x = (double)i;
		p3[j] = (5*x*x - tmp1)*x*tmp;
		s += p3[j]*p3[j];
	}
	for(j = 0, s = sqrt(s); j <= 2*m; j++)	p3[j] /= s;		
	return 0;
}
int		init_p4(double *p4, int m)
{
	register int i, j;
	double tmp, tmp1, tmp2, x, s; 
	
	if (m <= 0)	return -1;
	tmp = ((double)1)/(2*m*(2*m-1)*(m-1)*(2*m-3));
	tmp1 = 3*m*(m*m - 1)*(m + 2);
	tmp2 = 6*m*m +6*m -5;
	for(i = -m, j = 0, s = 0; i <= m; i++, j++)
	{
		x = (double)i;
		p4[j] = (35*x*x*x*x - 5*tmp2*x*x + tmp1)*tmp;
		s += p4[j]*p4[j];
	}
	for(j = 0, s = sqrt(s); j <= 2*m; j++)	p4[j] /= s;		
	return 0;
}
int		init_p5(double *p5, int m)
{
	register int i, j;
	double tmp, tmp1, tmp2, x, s; 
	
	if (m <= 0)	return -1;
	tmp = ((double)1)/(2*m*(2*m-1)*(m-1)*(2*m-3)*(m-2));
	tmp1 = 15*m*m*m*m + 30*m*m*m -35*m*m -50*m +12;
	tmp2 = 2*m*m +2*m -3;
	for(i = -m, j = 0, s = 0; i <= m; i++, j++)
	{
		x = (double)i;
		p5[j] = (65*x*x*x*x - 35*tmp2*x*x + tmp1)*x*tmp;
		s += p5[j]*p5[j];
	}
	for(j = 0, s = sqrt(s); j <= 2*m; j++)	p5[j] /= s;	
	return 0;
}


int		poly_fit5(float *y, int nx, double *yt)
{
	register int i;
	double tmp;
	
	if (nx%2 == 0)				return -1;
	if (poly_init((nx-1)/2)) 	return -1;
	tmp = (double)1/nx;
	for (i = 0, yt[0] = 0; i < nx; i++)  yt[0] += y[i];
	yt[0] *= tmp;
	for (i = 0, yt[1] = 0; i < nx; i++)  yt[1] += y[i]*p1[i];
	for (i = 0, yt[2] = 0; i < nx; i++)  yt[2] += y[i]*p2[i];
	for (i = 0, yt[3] = 0; i < nx; i++)  yt[3] += y[i]*p3[i];
	for (i = 0, yt[4] = 0; i < nx; i++)  yt[4] += y[i]*p4[i];
	for (i = 0, yt[5] = 0; i < nx; i++)  yt[5] += y[i]*p5[i];
	return 0;
}



int 	poly_init(int m)
{
	if (m <= 0)			return -1;
	if (m == m_points)	return 0; 
	p1 = (double *)realloc(p1, (2*m+1)*sizeof(double));
	p2 = (double *)realloc(p2, (2*m+1)*sizeof(double));
	p3 = (double *)realloc(p3, (2*m+1)*sizeof(double));
	p4 = (double *)realloc(p4, (2*m+1)*sizeof(double));
	p5 = (double *)realloc(p5, (2*m+1)*sizeof(double));
	dp1 = (double *)realloc(p1, (2*m+1)*sizeof(double));
	dp2 = (double *)realloc(p2, (2*m+1)*sizeof(double));
	dp3 = (double *)realloc(p3, (2*m+1)*sizeof(double));
	dp4 = (double *)realloc(p4, (2*m+1)*sizeof(double));
	dp5 = (double *)realloc(p5, (2*m+1)*sizeof(double));
	m_points = m;
	if (p1 == NULL || p2 == NULL || p3 == NULL || p4 == NULL || p5 == NULL)
		return 1;
	if (dp1 == NULL || dp2 == NULL || dp3 == NULL || dp4 == NULL || dp5 == NULL)
		return 1;
	init_p1(p1, m);
	init_p2(p2, m);
	init_p3(p3, m);
	init_p4(p4, m);
	init_p5(p5, m);
	return 0;
}
/*
*/


int do_project_on_polynome_ds(void)
{
  register int i, j;
  O_p *op = NULL;
  int nf;
  d_s *ds, *dsi;
  pltreg *pr = NULL;
  static int order = 3;
  double  yt[6];


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the y coordinate"
			   "of a data set by a number");
    }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  nf = dsi->nx;	/* this is the number of points in the data set */
 
  if (nf%2 == 0) 
    {
      nf = nf-1;
      i = win_printf("Projection can only be done on an Odd number\n"
			    "of points! do you agree to use %d",nf);
      if (i == CANCEL)	return OFF;
    }
  poly_fit5(dsi->yd, nf, yt);
  i = win_scanf("Order to use (<= 5) %d",&order);
  if (i == CANCEL)	return OFF;


  if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  for (j = 0; j < nf; j++)
    {
      ds->xd[j] = dsi->xd[j];
      ds->yd[j] = yt[0];
      if (order >= 1) ds->yd[j] += yt[1] * p1[j];
      if (order >= 2) ds->yd[j] += yt[2] * p2[j];
      if (order >= 3) ds->yd[j] += yt[3] * p3[j];
      if (order >= 4) ds->yd[j] += yt[4] * p4[j];
      if (order >= 5) ds->yd[j] += yt[5] * p5[j];
    }
  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"Projection on poly 5");
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}

# endif

int extract_raw_partial_line (O_i *oi, int n_line, float *z, int start, int size)
{
  register int i, nl, ii;
  int nx;
  float zr, zi;
  union pix *pi;
	
  if (oi == NULL || z == NULL)		return xvin_error(Wrong_Argument);	
  if (n_line >= oi->im.ny || n_line < 0)	return 1;
  pi = oi->im.pixel;
  nl = n_line;
  nx = oi->im.nx;

  if (oi->im.data_type == IS_CHAR_IMAGE)
    for (i=0; i< size; i++) 
      { 
	ii = start + i;
	ii = (ii < 0) ? 0 : ii;
	ii = (ii < nx) ? ii : nx;
	z[i] = (float)pi[nl].ch[ii];
      }
  else if (oi->im.data_type == IS_RGB_PICTURE)
    {
      for (i=0; i< size; i++)
	{ 
	  ii = start + i;
	  ii = (ii < 0) ? 0 : ii;
	  ii = (ii < nx) ? ii : nx;
	  if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
	    z[i] = ((float)(pi[nl].rgb[ii].r + pi[nl].rgb[ii].g 
			    + pi[nl].rgb[ii].b))/3;
	  else if (oi->im.mode & R_LEVEL)
	    z[i] = (float)pi[nl].rgb[ii].r;
	  else if (oi->im.mode & G_LEVEL)
	    z[i] = (float)pi[nl].rgb[ii].g;
	  else if (oi->im.mode & B_LEVEL)
	    z[i] = (float)pi[nl].rgb[ii].b;
	  else if (oi->im.mode == ALPHA_LEVEL)
	    z[i] = (float)255;
	  else return 1;
	}
    }
  else if (oi->im.data_type == IS_RGBA_PICTURE)
    {
      for (i=0; i< size; i++) 
	{
	  ii = start + i;
	  ii = (ii < 0) ? 0 : ii;
	  ii = (ii < nx) ? ii : nx;
	  //z[i] = ((float)pi[nl].rgba[i].b+(float)pi[nl].rgba[i].g+(float)pi[nl].rgba[i].r)/3;
	  if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
	    z[i] = ((float)(pi[nl].rgba[ii].r + pi[nl].rgba[ii].g 
			    + pi[nl].rgba[ii].b))/3;
	  else if (oi->im.mode & R_LEVEL)
	    z[i] = (float)pi[nl].rgba[ii].r;
	  else if (oi->im.mode & G_LEVEL)
	    z[i] = (float)pi[nl].rgba[ii].g;
	  else if (oi->im.mode & B_LEVEL)
	    z[i] = (float)pi[nl].rgba[ii].b;
	  else if (oi->im.mode == ALPHA_LEVEL)
	    z[i] = (float)pi[nl].rgba[ii].a;
	  else return 1;
	}
    }
  else if (oi->im.data_type == IS_RGB16_PICTURE)
    {
      for (i=0; i< size; i++)
	{ 
	  ii = start + i;
	  ii = (ii < 0) ? 0 : ii;
	  ii = (ii < nx) ? ii : nx;
	  //z[i] = ((float)pi[nl].ch[3*i]+(float)pi[nl].ch[(3*i)+1]+(float)pi[nl].ch[(3*i)+2])/3;
	  if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
	    z[i] = ((float)(pi[nl].rgb16[ii].r + pi[nl].rgb16[ii].g 
			    + pi[nl].rgb16[ii].b))/3;
	  else if (oi->im.mode & R_LEVEL)
	    z[i] = (float)pi[nl].rgb16[ii].r;
	  else if (oi->im.mode & G_LEVEL)
	    z[i] = (float)pi[nl].rgb16[ii].g;
	  else if (oi->im.mode & B_LEVEL)
	    z[i] = (float)pi[nl].rgb16[ii].b;
	  else if (oi->im.mode == ALPHA_LEVEL)
	    z[i] = (float)32767;
	  else return 1;
	}
    }
  else if (oi->im.data_type == IS_RGBA16_PICTURE)
    {
      for (i=0; i< size; i++) 
	{
	  ii = start + i;
	  ii = (ii < 0) ? 0 : ii;
	  ii = (ii < nx) ? ii : nx;
	  //z[i] = ((float)pi[nl].rgba[i].b+(float)pi[nl].rgba[i].g+(float)pi[nl].rgba[i].r)/3;
	  if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
	    z[i] = ((float)(pi[nl].rgba16[ii].r + pi[nl].rgba16[ii].g 
			    + pi[nl].rgba16[ii].b))/3;
	  else if (oi->im.mode & R_LEVEL)
	    z[i] = (float)pi[nl].rgba16[ii].r;
	  else if (oi->im.mode & G_LEVEL)
	    z[i] = (float)pi[nl].rgba16[ii].g;
	  else if (oi->im.mode & B_LEVEL)
	    z[i] = (float)pi[nl].rgba16[ii].b;
	  else if (oi->im.mode == ALPHA_LEVEL)
	    z[i] = (float)pi[nl].rgba16[ii].a;
	  else return 1;
	}
    }
  else if (oi->im.data_type == IS_INT_IMAGE)
    for (i=0; i< size; i++) 
      {
	ii = start + i;
	ii = (ii < 0) ? 0 : ii;
	ii = (ii < nx) ? ii : nx;
	z[i] =  (float)pi[nl].in[ii];
      }
  else if (oi->im.data_type == IS_UINT_IMAGE)
    for (i=0; i< size; i++) 
      {
	ii = start + i;
	ii = (ii < 0) ? 0 : ii;
	ii = (ii < nx) ? ii : nx;
	z[i] =  (float)pi[nl].ui[ii];
      }
  else if (oi->im.data_type == IS_LINT_IMAGE)
    for (i=0; i< size; i++) 
      {
	ii = start + i;
	ii = (ii < 0) ? 0 : ii;
	ii = (ii < nx) ? ii : nx;
	z[i] =  (float)pi[nl].li[ii];
      }
  else if (oi->im.data_type == IS_FLOAT_IMAGE)
    for (i=0; i< size; i++) 
      {
	ii = start + i;
	ii = (ii < 0) ? 0 : ii;
	ii = (ii < nx) ? ii : nx;
	z[i] = pi[nl].fl[ii];
      }
  else if (oi->im.data_type == IS_DOUBLE_IMAGE)
    for (i=0; i< size; i++) 
      {
	ii = start + i;
	ii = (ii < 0) ? 0 : ii;
	ii = (ii < nx) ? ii : nx;
	z[i] = pi[nl].db[ii];
      }
  else if (oi->im.data_type == IS_COMPLEX_IMAGE)
    {
      for (i=0; i< size; i++)
	{
	  ii = start + i;
	  ii = (ii < 0) ? 0 : ii;
	  ii = (ii < nx) ? ii : nx;
	  switch (oi->im.mode)
	    {
	    case AMP:
	      zr = pi[nl].fl[2*ii];
	      zi = pi[nl].fl[2*ii+1];
	      z[i] = sqrt(zr*zr+zi*zi);
	      break;
	    case LOG_AMP:
	      zr = pi[nl].fl[2*ii];
	      zi = pi[nl].fl[2*ii+1];
	      z[i] = zr*zr+zi*zi;
	      z[i] = (z[i] > 0) ? log10(z[i]) : -40;
	      break;				
	    case AMP_2:
	      zr = pi[nl].fl[2*ii];
	      zi = pi[nl].fl[2*ii+1];
	      z[i] = zr*zr+zi*zi;
	      break;
	    case RE:
	      z[i] = pi[nl].fl[2*ii];
	      break;
	    case IM:
	      z[i] = pi[nl].fl[2*ii+1];
	      break;
	    };
	}
    }
  else if (oi->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
    {
      for (i=0; i< size; i++)
	{
	  ii = start + i;
	  ii = (ii < 0) ? 0 : ii;
	  ii = (ii < nx) ? ii : nx;
	  switch (oi->im.mode)
	    {
	    case AMP:
	      zr = pi[nl].db[2*ii];
	      zi = pi[nl].db[2*ii+1];
	      z[i] = sqrt(zr*zr+zi*zi);
	      break;
	    case LOG_AMP:
	      zr = pi[nl].db[2*ii];
	      zi = pi[nl].db[2*ii+1];
	      z[i] = zr*zr+zi*zi;
	      z[i] = (z[i] > 0) ? log10(z[i]) : -40;
	      break;				
	    case AMP_2:
	      zr = pi[nl].db[2*ii];
	      zi = pi[nl].db[2*ii+1];
	      z[i] = zr*zr+zi*zi;
	      break;
	    case RE:
	      z[i] = pi[nl].db[2*ii];
	      break;
	    case IM:
	      z[i] = pi[nl].db[2*ii+1];
	      break;
	    };
	}
    }
  else	return 1;
  return 0;
}




int do_trk_local_max_along_y(void)
{
  register int i, j, k;
  int r_s, r_e, size, ret, ind, kmax;
  static int imax = 5, down = 0, lp = 3, range = 5, noper = 1, debug = 0;
  O_i *ois;
  O_p *op;
  d_s *ds, *dst = NULL;	
  imreg *imr;
  float Max_pos, Max_val, max;
	
  if(updating_menu_state != 0)	return D_O_K;	

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    return D_O_K;

  r_s = 0; r_e = ois->im.ny -1;
  i = win_scanf("look for maximum in X near position %8d +/- %6d\n"
		"Apply a FIR low-pass filter over 2n+1 points n=%6d\n"
		"Data periodic in X%R not periodic%r\n"
		"Process lines i [%6d, %6d]\n"
		"%Rupwards %rdownwards\n",&imax,&range,&lp,&noper,&r_s,&r_e,&down);
  if (i == CANCEL) return D_O_K;
  size = 1 + r_e - r_s;
  size = (size > ois->im.ny) ? ois->im.ny : size;
  op = create_and_attach_op_to_oi(ois, size, size, 0, IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
  if (op == NULL) return (win_printf_OK("cant create plot!"));
  ds = op->dat[0];
  set_plot_title(op,"Local max arround %d +/- %d low-pass FIR over %d in [%d to %d]"
		 ,imax,range,1+2*lp,r_s, r_e);
  set_ds_treatement(ds,"Local max arround %d +/- %d low-pass FIR over %d in [%d to %d]"
		    ,imax,range,1+2*lp,r_s, r_e);
  inherit_from_im_to_ds(ds, ois);
  uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
  uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
  op->filename = strdup(ois->filename);
  dst = build_data_set(ois->im.nx, ois->im.nx);
  if (dst == NULL) return (win_printf_OK("cant create dst!"));	


  for (i = (down)? r_e : r_s; i >= r_s && i<= r_e ; i = (down) ? i - 1 : i + 1)
    {
      if (i < 0 || i >= ois->im.ny) 
	{
	  continue;
	}
      extract_raw_line(ois, i, dst->yd);
      for(j = 0; j < dst->nx; j++) dst->xd[j] = 0;
      for (j = imax - 2 - range, kmax = -1; j < imax + 2 + range; j++)
	{
	  if (j < 0 || j >= dst->nx)		      continue;
	  for (k = -lp; k <= lp; k++) 
	    {
	      ind = j + k;
	      if (noper)
		{
		  ind = (ind < 0) ? 0 : ind; 
		  ind = (ind < dst->nx) ? ind : dst->nx-1; 
		}
	      else
		{
		  ind = (ind < 0) ? ind + dst->nx : ind; 
		  ind = (ind < dst->nx) ? ind : ind - dst->nx; 		      
		}
	      if (ind >= 0 && ind < dst->nx && j >= 0 && j < dst->nx)	
		dst->xd[j] += dst->yd[ind]; 
	      else if (debug != CANCEL) 
		debug = win_printf("ind out of range %d [0,%d[\n"
				   "or j out of range %d [0,%d[\n",ind,dst->nx,j,dst->nx);
	    }
	  if (kmax < 0 || dst->xd[j] > max)
	    {
	      kmax = j;
	      max = dst->xd[j];
	    }
	}
      ret = find_max_around(dst->xd, dst->nx, kmax, &Max_pos, &Max_val, NULL);
      if (ret == 0)
	{
	  j = i - r_s;
	  if (j < ds->nx && j >= 0)
	    {
	      ds->xd[j] = Max_pos;
	      ds->yd[j] = i;
	    }
	  imax = (int)Max_pos;
	}
    }
  free_data_set(dst);
  broadcast_dialog_message(MSG_DRAW,0);	
  return D_REDRAWME;		
}



int do_trk_local_zero_along_y(void)
{
  register int i, j;
  int r_s, r_e, size, ret, ext, imin, debug = 0;
  static int imax = 5, hsize = 3;
  O_i *ois;
  O_p *op;
  d_s *ds, *dst;	
  imreg *imr;
  double *a = NULL, min, xmin, ymin;
	
  if(updating_menu_state != 0)	return D_O_K;	

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    return D_O_K;

  r_s = 0; r_e = ois->im.ny;
  i = win_scanf("look for a zero in Y near position %8d\n"
		"Specify howmany points are used on each side %6d\n"
		"starting at line %8d and ending at line %8d",&imax,&hsize,&r_s,&r_e);
  if (i == CANCEL) return D_O_K;
  ext = 1 + 2 * hsize; 
  size = 1 + r_e - r_s;
  size = (size > ois->im.ny) ? ois->im.ny : size;
  op = create_and_attach_op_to_oi(ois, size, size, 0, IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
  if (op == NULL) return (win_printf_OK("cant create plot!"));
  ds = op->dat[0];
  set_plot_title(op,"Local Zero at %d (%d to %d)",imax,r_s, r_e);
  set_formated_string(&ds->treatement,"Local Zero at %d (%d to %d)",imax,r_s, r_e);
  inherit_from_im_to_ds(ds, ois);
  uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
  uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
  op->filename = strdup(ois->filename);
  dst = build_data_set(ext, ext);
  if (dst == NULL) return (win_printf_OK("cant create dst!"));	
	
  for (i=r_s ; i<= r_e ; i++)
    {
      extract_raw_partial_line(ois, i, dst->yd, (((imax - hsize) < 0) ? 0 : imax - hsize) , ext);
      for (j = 0; j < dst->nx; j++) dst->xd[j] = j - hsize;
      ret = fit_ds_to_xn_polynome(dst, 4, -FLT_MAX, FLT_MAX, -FLT_MAX, FLT_MAX, &a);
      if (debug != CANCEL) debug = win_printf("\\fbox{\\stack{{\\sl y = \\Sigma_{i=0}^{i<=%d} a_i x^i }"
			     "{a_0 =  %e}{a_1 = %e}{a_2 = %e}{a_3 = %e}{ret %d}}}",3,a[0],a[1],a[2],a[3],ret);
      for (j = 0, min = dst->yd[imin = 0]; j < dst->nx; j++)
	{
	  if (fabs(dst->yd[j]) < fabs(min))
	    {
	      imin = j;
	      min = dst->yd[j];
	    }
	}
      for (xmin = dst->xd[imin], j = 0; j < 10; j++)
	{
	  xmin -= (a[1] != 0) ? ymin/a[1] : 0;
	  ymin = a[0] + a[1] * xmin + a[2] * xmin * xmin + a[3] * xmin * xmin * xmin;
	  if (debug != CANCEL) debug = win_printf("iter %d xmin %g ymin %g",j,xmin,ymin);

	}
      if (i-r_s >= 0 && i-r_s < ds->nx)
	{
	  ds->xd[i-r_s] = xmin + imax;
	  ds->yd[i-r_s] = i;
	  imax = (int)(0.5 + ds->xd[i-r_s]);
	}
    }

  free_data_set(dst);
  broadcast_dialog_message(MSG_DRAW,0);	
  return D_REDRAWME;		
}




int do_trk_fit_local_gauss_max_along_y(void)
{
  register int i, j, k;
  int r_s, r_e, size, ret, l_poly;
  static int imax = 5, n_size = 6, debug = -1;
  static float offset = 10;
  O_i *ois;
  O_p *op;
  d_s *ds, *dst, *dst2;	
  imreg *imr;
  float  miny, maxy;
  double *a = NULL, x0, x1, delta;
	
  if(updating_menu_state != 0)	return D_O_K;	

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    return D_O_K;

  r_s = 0; r_e = ois->im.ny;
  i = win_scanf("look for maximum in X near position %8d\n"
		"starting at line %8d and ending at line %8d\n"
		"grab %8d points on each side (2*size+1 pts. total)\n"
		"remove offset %8f before applying ln(y^2)\n"
		,&imax,&r_s,&r_e,&n_size,&offset);
  if (i == CANCEL) return D_O_K;
  size = 1 + r_e - r_s;
  size = (size > ois->im.ny) ? ois->im.ny : size;
  l_poly = (2 * n_size) + 1;
  op = create_and_attach_op_to_oi(ois, size, size, 0, IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
  if (op == NULL) return (win_printf_OK("cant create plot!"));
  ds = op->dat[0];
  set_plot_title(op,"Local max at %d (%d to %d)",imax,r_s, r_e);
  set_formated_string(&ds->treatement,"Local max at %d (%d to %d)",imax,r_s, r_e);
  inherit_from_im_to_ds(ds, ois);
  uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
  uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
  op->filename = strdup(ois->filename);
  dst = build_data_set(ois->im.nx, ois->im.nx);
  dst2 = build_data_set(l_poly, l_poly);
  if (dst == NULL || dst2 == NULL) return (win_printf_OK("cant create dst!"));	
	
  //xlo = n_size + 1;
  for (i=r_s ; i<= r_e ; i++)
    {
      extract_raw_line(ois, i, dst->yd);
      for (j = 0; j < l_poly; j++)
	{
	  dst2->yd[j] = dst->yd[imax+j-n_size] - offset;
	  dst2->yd[j] *= dst2->yd[j];
	  dst2->yd[j] = (dst2->yd[j] > 0) ? log(dst2->yd[j]) : dst2->yd[j];
	  dst2->xd[j] = j - n_size;
	  /*
	  if (j == 0) miny = maxy = dst2->yd[j];
	  maxy = (dst2->yd[j] > maxy) ? dst2->yd[j] : maxy; 
	  miny = (dst2->yd[j] < miny) ? dst2->yd[j] : miny;
	  */
	  //if (debug != CANCEL) debug = win_printf("j %d x %g y %g",j,dst2->xd[j],dst2->yd[j]);
	}
      maxy += 1;
      miny -= 1;
      ret = fit_ds_to_xn_polynome(dst2, 4, -FLT_MAX, FLT_MAX, -FLT_MAX, FLT_MAX, &a);
      if (debug != CANCEL) debug = win_printf("\\fbox{\\stack{{\\sl y = \\Sigma_{i=0}^{i<=%d} a_i x^i }"
			     "{a_0 =  %e}{a_1 = %e}{a_2 = %e}{a_3 = %e}{ret %d}}}",3,a[0],a[1],a[2],a[3],ret);
      
      if (ret > 0)
	{
	  delta = a[2]*a[2] - 3*a[1]*a[3];
	  if (delta < 0) 
	    {
	      win_printf("Negative Delta");
	      break;
	    }
	  delta = sqrt(delta);
	  x0 = (a[2] - delta);
	  x1 = (a[2] + delta);
	  if (a[3] == 0) 
	    {
	      win_printf("a_3 == 0");
	      break;
	    }	  
	  x0 /= (3*a[3]);
	  x1 /= (3*a[3]);
	  x0 = (abs(x1) < abs(x0)) ? x1 : x0;
	  if (debug != CANCEL) debug = win_printf("x0 %g",x0);
	  k = (i - r_s < 0)? 0 : i - r_s;
	  k = (k < ds->nx) ? k : ds->nx-1; 
	  ds->xd[k] = imax + x0;
	  ds->yd[k] = i;
	  //imax = (int)ds->xd[k];
	}
      free(a);
    }
  free_data_set(dst);
  free_data_set(dst2);
  broadcast_dialog_message(MSG_DRAW,0);	
  return D_REDRAWME;		
}





int do_TrkMax_average_along_y(void)
{
	register int i, j;
	int l_s, l_e;
	O_i *ois = NULL;
	O_p *op = NULL;
	d_s *ds;
	imreg *imr = NULL;


	if(updating_menu_state != 0)	return D_O_K;

	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine average the image intensity"
			"of several lines of an image");
	}
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	return D_O_K;
	/* we grap the data that we need, the image and the screen region*/
	l_s = 0; l_e = ois->im.ny;
	/* we ask for the limit of averaging*/
	i = win_scanf("Average Between lines%d and %d",&l_s,&l_e);
	if (i == CANCEL)	return D_O_K;
	/* we create a plot to hold the profile*/
	op = create_one_plot(ois->im.nx,ois->im.nx,0);
	if (op == NULL) return (win_printf_OK("cant create plot!"));
	ds = op->dat[0]; /* we grab the data set */
	op->y_lo = ois->z_black;	op->y_hi = ois->z_white;
	op->iopt2 |= Y_LIM;	op->type = IM_SAME_X_AXIS;
	inherit_from_im_to_ds(ds, ois);
	op->filename = Transfer_filename(ois->filename);
	set_plot_title(op,"TrkMax averaged profile from line %d to %d",l_s, l_e);
	set_formated_string(&ds->treatement,"%s averaged profile from line %d to %d",l_s, l_e);
	uns_oi_2_op(ois, IS_Z_UNIT_SET, op, IS_Y_UNIT_SET);
	uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
	for (i=0 ; i< ds->nx ; i++)	ds->yd[i] = 0;
	for (i=l_s ; i< l_e ; i++)
	{
		extract_raw_line(ois, i, ds->xd);
		for (j=0 ; j< ds->nx ; j++)	ds->yd[j] += ds->xd[j];
	}
	for (i=0, j = l_e - l_s ; i< ds->nx && j !=0 ; i++)
	{
		ds->xd[i] = i;
		ds->yd[i] /= j;
	}
	add_one_image (ois, IS_ONE_PLOT, (void *)op);
	ois->cur_op = ois->n_op - 1;
	broadcast_dialog_message(MSG_DRAW,0);
	return D_REDRAWME;
}

O_i	*TrkMax_image_multiply_by_a_scalar(O_i *ois, float factor)
{
	register int i, j;
	O_i *oid;
	float tmp;
	int onx, ony, data_type;
	union pix *ps, *pd;

	onx = ois->im.nx;	ony = ois->im.ny;	data_type = ois->im.data_type;
	oid =  create_one_image(onx, ony, data_type);
	if (oid == NULL)
	{
		win_printf("Can't create dest image");
		return NULL;
	}
	if (data_type == IS_CHAR_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].ch[j];
				pd[i].ch[j] = (tmp < 0) ? 0 :
					((tmp > 255) ? 255 : (unsigned char)tmp);
			}
		}
	}
	else if (data_type == IS_INT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].in[j];
				pd[i].in[j] = (tmp < -32768) ? -32768 :
					((tmp > 32767) ? 32767 : (short int)tmp);
			}
		}
	}
	else if (data_type == IS_UINT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].ui[j];
				pd[i].ui[j] = (tmp < 0) ? 0 :
					((tmp > 65535) ? 65535 : (unsigned short int)tmp);
			}
		}
	}
	else if (data_type == IS_LINT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].li[j];
				pd[i].li[j] = (tmp < 0x10000000) ? 0x10000000 :
					((tmp > 0x7FFFFFFF) ? 0x7FFFFFFF : (int)tmp);
			}
		}
	}
	else if (data_type == IS_FLOAT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].fl[j] = factor * ps[i].fl[j];
			}
		}
	}
	else if (data_type == IS_DOUBLE_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].db[j] = factor * ps[i].db[j];
			}
		}
	}
	else if (data_type == IS_COMPLEX_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].fl[2*j] = factor * ps[i].fl[2*j];
				pd[i].fl[2*j+1] = factor * ps[i].fl[2*j+1];
			}
		}
	}
	else if (data_type == IS_COMPLEX_DOUBLE_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].db[2*j] = factor * ps[i].db[2*j];
				pd[i].db[2*j+1] = factor * ps[i].db[2*j+1];
			}
		}
	}
	else if (data_type == IS_RGB_PICTURE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< 3*onx; j++)
			{
				tmp = factor * ps[i].ch[j];
				pd[i].ch[j] = (tmp < 0) ? 0 :
					((tmp > 255) ? 255 : (unsigned char)tmp);
			}
		}
	}
	else if (data_type == IS_RGBA_PICTURE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< 4*onx; j++)
			{
				tmp = factor * ps[i].ch[j];
				pd[i].ch[j] = (tmp < 0) ? 0 :
					((tmp > 255) ? 255 : (unsigned char)tmp);
			}
		}
	}
	inherit_from_im_to_im(oid,ois);
	uns_oi_2_oi(oid,ois);
	oid->im.win_flag = ois->im.win_flag;
	if (ois->title != NULL)	set_im_title(oid, "%s rescaled by %g",
		 ois->title,factor);
	set_formated_string(&oid->im.treatement,
		"Image %s rescaled by %g", ois->filename,factor);
	return oid;
}

int do_TrkMax_image_multiply_by_a_scalar(void)
{
	O_i *ois, *oid;
	imreg *imr;
	int i;
	static float factor = 2.0;


	if(updating_menu_state != 0)	return D_O_K;

	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine multiply the image intensity"
		"by a user defined scalar factor");
	}
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	{
		win_printf("Cannot find image");
		return D_O_K;
	}
	i = win_scanf("define the factor of mutiplication %f",&factor);
	if (i == CANCEL)	return D_O_K;
	oid = TrkMax_image_multiply_by_a_scalar(ois,factor);
	if (oid == NULL)	return win_printf_OK("unable to create image!");
	add_image(imr, oid->type, (void*)oid);
	find_zmin_zmax(oid);
	return (refresh_image(imr, imr->n_oi - 1));
}




int 	copy_one_subset_of_image_in_a_bigger_one(O_i *dest, int posx, int posy, O_i *ois, int x_start, int y_start, int nx, int ny)
{
  union pix *pd, *ps; 
  int i, j, onx, ony, donx, dony;

  if (ois == NULL || dest == NULL)                       return 1;
  if (ois->im.data_type != dest->im.data_type)           return 2;
  pd = dest->im.pixel;
  ps = ois->im.pixel;
  onx = ois->im.nx; 
  ony = ois->im.ny;
  donx = dest->im.nx; 
  dony = dest->im.ny;
  if (posx < 0 || posy < 0 || x_start < 0 || y_start < 0)                        return 3;                     
  if ((x_start + nx > onx) || (y_start + ny > ony))  return 4;                     

  //win_printf("Desti -> %d %d\nori %d %d %d %d",posx, posy, x_start,  y_start, nx, ny);

  if (ois->im.data_type == IS_CHAR_IMAGE)
    {
      for (i = 0; i < ny && i + y_start< ony && i + posy < dony; i++)
	{
	  for (j = 0; j < nx && j + x_start < onx && j + posx < donx ; j++)
	      pd[posy+i].ch[posx+j] = ps[i+y_start].ch[j+x_start];
	}
    }
  else if (ois->im.data_type == IS_RGB_PICTURE)
    {
      for (i = 0; i < ny && i + y_start< ony && i + posy < dony; i++)
	{
	  for (j = 0; j < nx && j + x_start < onx && j + posx < donx ; j++)
	      pd[posy+i].rgb[posx+j] = ps[i+y_start].rgb[j+x_start];
	}
    }
  else if (ois->im.data_type == IS_RGBA_PICTURE)
    {
      for (i = 0; i < ny && i + y_start< ony && i + posy < dony; i++)
	{
	  for (j = 0; j < nx && j + x_start < onx && j + posx < donx ; j++)
	      pd[posy+i].rgba[posx+j] = ps[i+y_start].rgba[j+x_start];
	}
    }
  else if (ois->im.data_type == IS_RGB16_PICTURE)
    {
      for (i = 0; i < ny && i + y_start< ony && i + posy < dony; i++)
	{
	  for (j = 0; j < nx && j + x_start < onx && j + posx < donx ; j++)
	      pd[posy+i].rgb16[posx+j] = ps[i+y_start].rgb16[j+x_start];
	}
    }
  else if (ois->im.data_type == IS_RGBA16_PICTURE)
    {
      for (i = 0; i < ny && i + y_start< ony && i + posy < dony; i++)
	{
	  for (j = 0; j < nx && j + x_start < onx && j + posx < donx ; j++)
	      pd[posy+i].rgba16[posx+j] = ps[i+y_start].rgba16[j+x_start];
	}
    }
  else if (ois->im.data_type == IS_INT_IMAGE)
    {
      for (i = 0; i < ny && i + y_start< ony && i + posy < dony; i++)
	{
	  for (j = 0; j < nx && j + x_start < onx && j + posx < donx ; j++)
	      pd[posy+i].in[posx+j] = ps[i+y_start].in[j+x_start];
	}
    }
  else if (ois->im.data_type == IS_UINT_IMAGE)
    {
      for (i = 0; i < ny && i + y_start< ony && i + posy < dony; i++)
	{
	  for (j = 0; j < nx && j + x_start < onx && j + posx < donx ; j++)
	      pd[posy+i].ui[posx+j] = ps[i+y_start].ui[j+x_start];
	}
    }
  else if (ois->im.data_type == IS_LINT_IMAGE)
    {
      for (i = 0; i < ny && i + y_start< ony && i + posy < dony; i++)
	{
	  for (j = 0; j < nx && j + x_start < onx && j + posx < donx ; j++)
	      pd[posy+i].li[posx+j] = ps[i+y_start].li[j+x_start];
	}
    }
  else if (ois->im.data_type == IS_FLOAT_IMAGE)
    {
      for (i = 0; i < ny && i + y_start< ony && i + posy < dony; i++)
	{
	  for (j = 0; j < nx && j + x_start < onx && j + posx < donx ; j++)
	      pd[posy+i].fl[posx+j] = ps[i+y_start].fl[j+x_start];
	}
    }
  else if (ois->im.data_type == IS_COMPLEX_IMAGE)
    {
      for (i = 0; i < ny && i + y_start< ony && i + posy < dony; i++)
	{
	  for (j = 0; j < nx && j + x_start < onx && j + posx < donx ; j++)
	      pd[posy+i].cp[posx+j] = ps[i+y_start].cp[j+x_start];
	}
    }
  else if (ois->im.data_type == IS_DOUBLE_IMAGE)
    {
      for (i = 0; i < ny && i + y_start< ony && i + posy < dony; i++)
	{
	  for (j = 0; j < nx && j + x_start < onx && j + posx < donx ; j++)
	      pd[posy+i].db[posx+j] = ps[i+y_start].db[j+x_start];
	}
    }
  else if (ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
    {
      for (i = 0; i < ny && i + y_start< ony && i + posy < dony; i++)
	{
	  for (j = 0; j < nx && j + x_start < onx && j + posx < donx ; j++)
	      pd[posy+i].dcp[posx+j] = ps[i+y_start].dcp[j+x_start];
	}
    }
  return 0;
}

int 	copy_and_swap_one_subset_of_image_in_a_bigger_one(O_i *dest, int posx, int posy, O_i *ois, int x_start, int y_start, int nx, int ny)
{
  union pix *pd, *ps; 
  int i, j, onx, ony, donx, dony;

  if (ois == NULL || dest == NULL)                       return 1;
  if (ois->im.data_type != dest->im.data_type)           return 2;
  pd = dest->im.pixel;
  ps = ois->im.pixel;
  onx = ois->im.nx; 
  ony = ois->im.ny;
  donx = dest->im.nx; 
  dony = dest->im.ny;
  if (posx < 0 || posy < 0 || x_start < 0 || y_start < 0)                        return 3;                     
  if ((x_start + nx > onx) || (y_start + ny > ony))  return 4;                     

  //win_printf("Desti -> %d %d\nori %d %d %d %d",posx, posy, x_start,  y_start, nx, ny);

  if (ois->im.data_type == IS_CHAR_IMAGE)
    {
      for (i = 0; i < nx && i + x_start< onx && i + posy < dony; i++)
	{
	  for (j = 0; j < ny && j + y_start < ony && j + posx < donx ; j++)
	      pd[posy+i].ch[posx+j] = ps[j+y_start].ch[i+x_start];
	}
    }
  else if (ois->im.data_type == IS_RGB_PICTURE)
    {
      for (i = 0; i < nx && i + x_start< onx && i + posy < dony; i++)
	{
	  for (j = 0; j < ny && j + y_start < ony && j + posx < donx ; j++)
	      pd[posy+i].rgb[posx+j] = ps[j+y_start].rgb[i+x_start];
	}
    }
  else if (ois->im.data_type == IS_RGBA_PICTURE)
    {
      for (i = 0; i < nx && i + x_start< onx && i + posy < dony; i++)
	{
	  for (j = 0; j < ny && j + y_start < ony && j + posx < donx ; j++)
	      pd[posy+i].rgba[posx+j] = ps[j+y_start].rgba[i+x_start];
	}
    }
  else if (ois->im.data_type == IS_RGB16_PICTURE)
    {
      for (i = 0; i < nx && i + x_start< onx && i + posy < dony; i++)
	{
	  for (j = 0; j < ny && j + y_start < ony && j + posx < donx ; j++)
	      pd[posy+i].rgb16[posx+j] = ps[j+y_start].rgb16[i+x_start];
	}
    }
  else if (ois->im.data_type == IS_RGBA16_PICTURE)
    {
      for (i = 0; i < nx && i + x_start< onx && i + posy < dony; i++)
	{
	  for (j = 0; j < ny && j + y_start < ony && j + posx < donx ; j++)
	      pd[posy+i].rgba16[posx+j] = ps[j+y_start].rgba16[i+x_start];
	}
    }
  else if (ois->im.data_type == IS_INT_IMAGE)
    {
      for (i = 0; i < nx && i + x_start< onx && i + posy < dony; i++)
	{
	  for (j = 0; j < ny && j + y_start < ony && j + posx < donx ; j++)
	      pd[posy+i].in[posx+j] = ps[j+y_start].in[i+x_start];
	}
    }
  else if (ois->im.data_type == IS_UINT_IMAGE)
    {
      for (i = 0; i < nx && i + x_start< onx && i + posy < dony; i++)
	{
	  for (j = 0; j < ny && j + y_start < ony && j + posx < donx ; j++)
	      pd[posy+i].ui[posx+j] = ps[j+y_start].ui[i+x_start];
	}
    }
  else if (ois->im.data_type == IS_LINT_IMAGE)
    {
      for (i = 0; i < nx && i + x_start< onx && i + posy < dony; i++)
	{
	  for (j = 0; j < ny && j + y_start < ony && j + posx < donx ; j++)
	      pd[posy+i].li[posx+j] = ps[j+y_start].li[i+x_start];
	}
    }
  else if (ois->im.data_type == IS_FLOAT_IMAGE)
    {
      for (i = 0; i < nx && i + x_start< onx && i + posy < dony; i++)
	{
	  for (j = 0; j < ny && j + y_start < ony && j + posx < donx ; j++)
	      pd[posy+i].fl[posx+j] = ps[j+y_start].fl[i+x_start];
	}
    }
  else if (ois->im.data_type == IS_COMPLEX_IMAGE)
    {
      for (i = 0; i < nx && i + x_start< onx && i + posy < dony; i++)
	{
	  for (j = 0; j < ny && j + y_start < ony && j + posx < donx ; j++)
	      pd[posy+i].cp[posx+j] = ps[j+y_start].cp[i+x_start];
	}
    }
  else if (ois->im.data_type == IS_DOUBLE_IMAGE)
    {
      for (i = 0; i < nx && i + x_start< onx && i + posy < dony; i++)
	{
	  for (j = 0; j < ny && j + y_start < ony && j + posx < donx ; j++)
	      pd[posy+i].db[posx+j] = ps[j+y_start].db[i+x_start];
	}
    }
  else if (ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
    {
      for (i = 0; i < nx && i + x_start< onx && i + posy < dony; i++)
	{
	  for (j = 0; j < ny && j + y_start < ony && j + posx < donx ; j++)
	      pd[posy+i].dcp[posx+j] = ps[j+y_start].dcp[i+x_start];
	}
    }
  return 0;
}



int 	set_all_pixel_to_color(O_i *dest, int bw, double flt_dbl_val)
{
  union pix *pd; 
  int i, j, donx, dony, color;
  float fcolor;
  double dcolor;

  pd = dest->im.pixel;
  donx = dest->im.nx; 
  dony = dest->im.ny;
  if (dest->im.data_type == IS_CHAR_IMAGE)
    {
      color = bw;//(bw) ? 255 : 0;
      for (i = 0; i < dony; i++)
	{
	  for (j = 0; j < donx ; j++)
	    pd[i].ch[j] = color;
	}
    }
  else if (dest->im.data_type == IS_RGB_PICTURE)
    {
      color = bw;//(bw) ? 255 : 0;
      for (i = 0; i < dony; i++)
	{
	  for (j = 0; j < 3*donx ; j++)
	    pd[i].ch[j] = color;
	}
    }
  else if (dest->im.data_type == IS_RGBA_PICTURE)
    {
      color = bw;//(bw) ? 255 : 0;
      for (i = 0; i < dony; i++)
	{
	  for (j = 0; j < 4*donx ; j++)
	    pd[i].ch[j] = color;
	}
    }
  else if (dest->im.data_type == IS_RGB16_PICTURE)
    {
      color = bw;//(bw) ? 65535 : 0;
      for (i = 0; i < dony; i++)
	{
	  for (j = 0; j < 3*donx ; j++)
	    pd[i].ui[j] = color;
	}
    }
  else if (dest->im.data_type == IS_RGBA16_PICTURE)
    {
      color = bw;//(bw) ? 65535 : 0;
      for (i = 0; i < dony; i++)
	{
	  for (j = 0; j < 4*donx ; j++)
	    pd[i].ui[j] = color;
	}
    }
  else if (dest->im.data_type == IS_INT_IMAGE)
    {
      color = bw;//(bw) ? 32767 : -32768;
      for (i = 0; i < dony; i++)
	{
	  for (j = 0; j < donx ; j++)
	    pd[i].in[j] = color;
	}
    }
  else if (dest->im.data_type == IS_UINT_IMAGE)
    {
      color = bw;//(bw) ? 65535 : 0;
      for (i = 0; i < dony; i++)
	{
	  for (j = 0; j < donx ; j++)
	    pd[i].ui[j] = color;
	}
    }
  else if (dest->im.data_type == IS_LINT_IMAGE)
    {
      color = bw;//(bw) ? 0x7FFFFFFF : 0x10000000;
      for (i = 0; i < dony; i++)
	{
	  for (j = 0; j < donx ; j++)
	    pd[i].li[j] = color;
	}
    }
  else if (dest->im.data_type == IS_FLOAT_IMAGE)
    {
      fcolor = flt_dbl_val;
      for (i = 0; i < dony; i++)
	{
	  for (j = 0; j < donx ; j++)
	    pd[i].fl[j] = fcolor;
	}
    }
  else if (dest->im.data_type == IS_COMPLEX_IMAGE)
    {
      fcolor = flt_dbl_val;
      for (i = 0; i < dony; i++)
	{
	  for (j = 0; j < 2*donx ; j++)
	    pd[i].fl[j] = fcolor;
	}
    }
  else if (dest->im.data_type == IS_DOUBLE_IMAGE)
    {
      dcolor = flt_dbl_val;
      for (i = 0; i < dony; i++)
	{
	  for (j = 0; j < donx ; j++)
	    pd[i].db[j] = dcolor;
	}
    }
  else if (dest->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
    {
      dcolor = flt_dbl_val;
      for (i = 0; i < dony; i++)
	{
	  for (j = 0; j < 2*donx ; j++)
	    pd[i].db[j] = dcolor;
	}
    }
  return 0;
}




int	assemble_multiple_small_images_from_movie(void)
{
  int i;
  //int snx = 0, sny = 0;
  int  onx, ony, nfi, posx, posy, type, ROI_set, lx0, lnx, ly0, lny;
  O_i   *oid, *ois;
  p_l *pl;
  imreg *imr;
  static int x0 = 0, nx = 64, y0 = 0, ny = 32, bd = 5, bdc = 0, col = 0;
  static int nfx = 4, nfy = 3, start = 0, every = 3, end = 1, sw = 0;
  char color[64];

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) < 1)	return OFF;
  nfi = abs(ois->im.n_f); 
  nfi = (nfi == 0) ? 1 : nfi;
  if(updating_menu_state != 0)	
    {
      if (nfi <= 1) 	 active_menu->flags |=  D_DISABLED;
      else               active_menu->flags &= ~D_DISABLED;
      return D_O_K;		
    }


  if (ois != NULL)
    {
      //snx = ois->im.nx;
      //sny = ois->im.ny;
      type = ois->im.data_type;
    }
  end = nfi;
  ROI_set = (ois->im.nxs > 0 || ois->im.nys > 0 || ois->im.nxe < ois->im.nx || ois->im.nye < ois->im.ny) ? 1 : 0;
    
  if (ROI_set)
    {
      lx0 = ois->im.nxs; lnx = ois->im.nxe - ois->im.nxs; 
      ly0 = ois->im.nys; lny = ois->im.nye - ois->im.nys;
    }
  else 
    {
      lx0 = x0; lnx = nx; ly0 = y0; lny = ny;
    }

  i = win_scanf("Construction of a composite image from partial image taken from different movie frames\n"
		"Specify the origin and size of the sub image in the movie:\n"
		"X0 %6d size in X %6d Y0 %6d size in Y %6d\n"
		"Normal%R or swap XY%r \nBorder size %6d Grey level %6d\n"
		"How many sub-images in line %4d How many sub-images in row %4d\n"
		"Grab sub images starting at frame %6d Every %6d frames ending at %6d\n"
		"Label color White->%R Black->%r Red->%r Blue->%r Green->%r\n"
		,&lx0,&lnx,&ly0,&lny,&sw,&bd,&bdc,&nfx,&nfy,&start,&every,&end,&col);
  if (i == CANCEL) return  D_O_K;		

  onx = (sw) ? nfx*(lny + bd + bd) : nfx*(lnx + bd + bd);
  ony = (sw) ? nfy*(lnx + bd + bd) : nfy*(lny + bd + bd);
  ony += bd;
  oid = create_and_attach_oi_to_imr(imr, onx, ony, type);
  if (oid == NULL)	return win_printf_OK("cannot create profile !");

  if (col == 0) snprintf(color,sizeof(color),"white");
  else if (col == 1) snprintf(color,sizeof(color),"black");
  else if (col == 2) snprintf(color,sizeof(color),"lightred");
  else if (col == 3) snprintf(color,sizeof(color),"lightblue");
  else if (col == 4) snprintf(color,sizeof(color),"lightgreen");
  else snprintf(color,sizeof(color),"Lightred");

  set_all_pixel_to_color(oid, bdc, 0.0);	

  for (i = start; i < end; i += every)
    {
      switch_frame(ois,i);
      if (sw == 0)
	{
	  posx = bd + (bd + lnx + bd) * ((((i - start)/every))%nfx);
	  posy = bd + (bd + lny + bd) * ((((i - start)/every))/nfx);
	  posy = ony - posy - lny;
	  copy_one_subset_of_image_in_a_bigger_one(oid, posx, posy, ois, lx0, ly0, lnx, lny);
	  //create_and_attach_label_to_oi(oid, posx + lnx/2, posy -bd - lny, USR_COORD, "{\\pt5 %d}", i);
	  if (ois->n_tu == 0)	  pl = create_plot_label(USR_COORD, posx + lnx/2, posy -bd ,
							 "\\color{%s}\\center{\\pt5 %d}",color, i);
	  else pl = create_plot_label(USR_COORD, posx + lnx/2, posy -bd 
				      ,"\\color{%s}\\center{\\pt5 %g %s}",color, 
				      ois->tu[ois->c_tu]->ax+ois->tu[ois->c_tu]->dx*i
				      ,((ois->tu[ois->c_tu]->name)?ois->tu[ois->c_tu]->name:""));
	  add_to_one_image(oid,IS_PLOT_LABEL,(void*)pl);
	}
      else
	{
	  posx = bd + (bd + lny + bd) * ((((i - start)/every))%nfx);
	  posy = bd + (bd + lnx + bd) * ((((i - start)/every))/nfx);
	  posy = ony - posy - lnx;
	  copy_and_swap_one_subset_of_image_in_a_bigger_one(oid, posx, posy, ois, lx0, ly0, lnx, lny);
	  //create_and_attach_label_to_oi(oid, posx + lny/2, posy -bd - lnx, USR_COORD, "{\\pt5 %d}", i);
	  if (ois->n_tu == 0)	  pl = create_plot_label(USR_COORD, posx + lny/2, posy -bd 
							 ,"\\color{%s}\\center{\\pt5 %d}",color, i);
	  else pl = create_plot_label(USR_COORD, posx + lny/2, posy -bd 
				      ,"\\color{%s}\\center{\\pt5 %g %s}",color, 
				      ois->tu[ois->c_tu]->ax+ois->tu[ois->c_tu]->dx*i
				      ,((ois->tu[ois->c_tu]->name)?ois->tu[ois->c_tu]->name:""));
	  add_to_one_image(oid,IS_PLOT_LABEL,(void*)pl);
	}
    }

  if (ROI_set == 0)
    {
      x0 = lx0; nx = lnx; y0 = ly0; ny = lny;
    }


  //find_zmin_zmax(oid);
  oid->z_black = ois->z_black; 	oid->z_white = ois->z_white;
  oid->z_min = ois->z_min; 	oid->z_max = ois->z_max;	
  if (sw == 0)
    {
      uns_oi_2_oi_by_type(ois, IS_X_UNIT_SET, oid, IS_X_UNIT_SET);
      uns_oi_2_oi_by_type(ois, IS_Y_UNIT_SET, oid, IS_Y_UNIT_SET);
    }
  else
    {
      uns_oi_2_oi_by_type(ois, IS_X_UNIT_SET, oid, IS_Y_UNIT_SET);
      uns_oi_2_oi_by_type(ois, IS_Y_UNIT_SET, oid, IS_X_UNIT_SET);
    }
  inherit_from_im_to_im(oid,ois);
  map_pixel_ratio_of_image_and_screen(oid, 1, 1);
  oid->need_to_refresh = ALL_NEED_REFRESH;		
  return (refresh_image(imr, imr->n_oi - 1));
}
# define	T_AXIS 		3072	/* 2048 + 1024 */

int 	add_new_units_2(void)
{
  un_s *lux;
  register int i;
  char s[128];
  static char uname[64]="no_name";	
  int index;
  pltreg *pr;
  imreg *imr; 
  //	int type;
  static float ax = 0, dx = 1;
  
  if(updating_menu_state != 0)	return D_O_K;		
  pr = find_pr_in_current_dialog(NULL);		
  imr = find_imr_in_current_dialog(NULL);		
  if (pr == NULL && imr == NULL)	
    return (win_printf("cannot find plot or image"));
  index = mn_index;
  lux = build_unit_set(0, 0, 1, 0, 0, "no_name");
  strcpy(s,uname);
  if (lux == NULL)	return (win_printf("cannot create unit set"));
  to_modify_un = lux;
  /*
    i = win_scanf("Type of unit%d%m",&lux->type,unit_type_mn());
    if (i == CANCEL)	goto aborting;	
  */	
  last_popup_index = 0;
  lux->type = pop_unit_type_mn();
  if (lux->type < 0)  
    {
      free_unit_set(lux);
      return win_printf_OK("No unit set created");
    }
  if (lux->type == 0)
    {
      i = win_scanf("X unit set of type other\noffset %12f\nincrement%12f\n"
		    "name (no\\_name suppreses any indication) %ls",
		    &ax, &dx, s);
      if (i == CANCEL)	goto aborting;	
		lux->ax = ax;
		lux->dx = dx;
		strcpy(uname,s);
		lux->name = (strncmp(s,"no_name",7) == 0) ? NULL : Mystrdup(s);		
    }
  else 
    {
      /*		
			i = win_scanf("Choose decade%d%m",&lux->decade,unit_decade_mn());
			if (i == CANCEL)	goto aborting;			
      */		
      last_popup_index = to_modify_un->decade;
      lux->decade = pop_unit_decade_mn();	
      lux->name = generate_units(lux->type, lux->decade);
      if (lux->name == NULL)
	{
	  i = win_scanf("X unit set type%d offset%f increment%f"
			"name (no\\_name suppreses any indication) %ls",
			&lux->type,&ax, &dx, s);
	  if (i == CANCEL)	goto aborting;	
	  lux->ax = ax;
	  lux->dx = dx;
	  strcpy(uname,s);
	  if (lux->name != NULL)	free(lux->name);
	  lux->name = (strncmp(s,"no_name",7) == 0) ? NULL : Mystrdup(s);				}
      else
	{
	  i = win_scanf("offset%f increment%f",&ax,&dx);			
	  if (i == CANCEL)	goto aborting;	
	  lux->ax = ax;
	  lux->dx = dx;			
	}
    }
  to_modify_un = NULL;
  if (index == X_AXIS && pr != NULL)
    {
      add_one_plot_data (pr->one_p, IS_X_UNIT_SET, (void *)lux);
      return set_plt_x_unit_set(pr, pr->one_p->n_xu-1);	
    }
  else if (index == Y_AXIS && pr != NULL)
    {
      add_one_plot_data (pr->one_p, IS_Y_UNIT_SET, (void *)lux);
      return set_plt_y_unit_set(pr, pr->one_p->n_yu-1);	
    }	
  else if (index == X_AXIS && imr != NULL)
    {
      add_one_image (imr->one_i, IS_X_UNIT_SET, (void *)lux);
      return set_im_x_unit_set(imr, imr->one_i->n_xu-1);	
    }
  else if (index == Y_AXIS && imr != NULL)
    {
      add_one_image (imr->one_i, IS_Y_UNIT_SET, (void *)lux);
      return set_im_y_unit_set(imr, imr->one_i->n_yu-1);	
    }		
  else if (index == Z_AXIS && imr != NULL)
    {
      add_one_image (imr->one_i, IS_Z_UNIT_SET, (void *)lux);
      return set_im_z_unit_set(imr, imr->one_i->n_zu-1);	
    }				
  else if (index == T_AXIS && imr != NULL)
    {
      add_one_image (imr->one_i, IS_T_UNIT_SET, (void *)lux);
      return set_oi_t_unit_set(imr->one_i, imr->one_i->n_tu-1);	
    }				
  else return 1;
  return 0;
 aborting :	if (lux)  free(lux);
  to_modify_un = NULL;
  return 0;
}


int do_TrkMax_unit_z(void)
{
  O_i *ois = NULL;
  imreg *imr = NULL;
  int nfi;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) < 1)	return OFF;
  nfi = abs(ois->im.n_f); 
  nfi = (nfi == 0) ? 1 : nfi;
  if(updating_menu_state != 0)	
    {
      if (nfi <= 1) 	 active_menu->flags |=  D_DISABLED;
      else               active_menu->flags &= ~D_DISABLED;
      return D_O_K;		
    }
  mn_index = T_AXIS;
  add_new_units_2();
  
  win_printf("T unit set added\n# of T unit set %d current %d\n"
	     "Offset %g increment %g\n"
             "name %s\n", ois->n_tu,ois->c_tu
	     ,ois->tu[ois->c_tu]->ax
	     ,ois->tu[ois->c_tu]->dx
	     ,((ois->tu[ois->c_tu]->name)?ois->tu[ois->c_tu]->name:""));
  
  
//ois->c_zu = ois->n_zu -1;
  return 0;
}







MENU *TrkMax_image_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	
	add_item_to_menu(mn,"Follow local max in X", do_trk_local_max_along_y,NULL,0,NULL);
	add_item_to_menu(mn,"Follow local Zero in X", do_trk_local_zero_along_y,NULL,0,NULL);


	add_item_to_menu(mn,"Follow local max in X by poly", do_trk_fit_local_gauss_max_along_y,NULL,0,NULL);
	add_item_to_menu(mn,"Contact-sheet",assemble_multiple_small_images_from_movie ,NULL,0,NULL);
	add_item_to_menu(mn,"Add T unit to movie",do_TrkMax_unit_z,NULL,0,NULL);



	add_item_to_menu(mn,"average profile", do_TrkMax_average_along_y,NULL,0,NULL);
	add_item_to_menu(mn,"image z rescaled", do_TrkMax_image_multiply_by_a_scalar,NULL,0,NULL);
	return mn;
}

int	TrkMax_main(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  add_image_treat_menu_item ( "TrkMax", NULL, TrkMax_image_menu(), 0, NULL);
  return D_O_K;
}

int	TrkMax_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  remove_item_to_menu(image_treat_menu, "TrkMax", NULL, NULL);
  return D_O_K;
}
#endif

