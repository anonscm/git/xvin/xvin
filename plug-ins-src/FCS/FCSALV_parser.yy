%{
#include "xvin.h"
#include "gsl_statistics_double.h" 
int i = 0, NumberRuns = 0;
int j = 0, k = 0, npoints = 2048 , k_timetrace, j_timetrace;
extern char *filename;
d_s **ds_tmp = NULL;
double *data = NULL;
d_s *APD1 = NULL;
d_s *APD2 = NULL;



#define YY_DECL int yylex YY_PROTO(( O_p *op, d_s **ds , char *filename ))



%}
/*general definition*/


DIGIT    [0-9]
%option noyywrap

%x is_ALV
%x runs
%x values
%x is_count_rate

                                                        




%%

"ALV-6000" BEGIN(is_ALV);
<is_ALV>"/E-WIN"   {
                       // win_printf("%s",yytext);
//                        if (strcmp ( "/E-WIN" , yytext ) != (strlen("/E-WIN")+1))
//                                            {
//                                                                       win_printf("NOT AN ALV FILE");
//                                                                       yyterminate();
//                                            }
//                        else win_printf("POTATO");
                                          BEGIN(INITIAL);
                    }


"Runs            :" BEGIN(runs);

<runs>({DIGIT})+       {
                                                NumberRuns = atoi(yytext);
                                             	win_printf("NumberRuns %d et %s fin",NumberRuns,yytext);
												ds_tmp = (d_s**) calloc(NumberRuns +1 ,sizeof(d_s*));
												k = 0;
												k_timetrace = 0;
												j_timetrace = 0;
												j = 0;
												for (i = 0; i < NumberRuns + 1 ; i++)
												{
													
													ds_tmp[i] = build_data_set(npoints, npoints);
												}
												BEGIN(INITIAL);
                                                
                               }
                               
"Lag [ms]" BEGIN(values);

<values>[\-0-9]+"."[0-9E\+\-]*    { 
                                /* ds 0 moyenne*/
                                k = j /(NumberRuns + 1 +1);
                                //win_printf(" j = %d \n %s \n k = %d",j, yytext, k);
                                
                                 if ( j % (NumberRuns + 1 + 1 /*pour la moyenne*/) == 0)
								{
								    //win_printf("set x %f\n j = %d \n k = %d",atof(yytext),j,k);
									for (i = 0; i < NumberRuns +1 ; i++) ds_tmp[i]->xd[k] = atof(yytext);
								}
								else 
                                  {
                                   
    								  ds_tmp[j % (NumberRuns + 1 + 1) - 1]->yd[k] = atof(yytext);
						          }    
                                j++;
                                //BEGIN(INITIAL);
                                
                          }
<values>"Count Rate" BEGIN(is_count_rate);               


<is_count_rate>[\-0-9]+"."[0-9E\+\-]* { win_printf("Oh Oh set x %f\n j = %d \n k = %d",atof(yytext),j,k);
                                        ds = (d_s**) calloc(NumberRuns +1 ,sizeof(d_s*));
												for (i = 0; i < NumberRuns + 1 ; i++)
												{
													
													ds[i] = create_and_attach_one_ds (op, k , k , IS_FLOAT);
												}
						//				win_printf("OK");		
                                                    for (i = 0; i < NumberRuns +1 ; i++) //ds[i] = build_adjust_data_set(ds[i],k+1,k+1);
                                                    {
                                                        for (j = 0 ; j < k ; j ++)
                                                        {
                                                         ds[i]->xd[j] = ds_tmp[i]->xd[j];
                                                         ds[i]->yd[j] = ds_tmp[i]->yd[j];
                                                        }    
                                                    }   
                                                  
                                                   //penser free 
                                                   set_ds_source(ds[0], "ALV average"); 
                                                   for (i = 1 ; i < NumberRuns + 1 ; i++)
                                                   {
                                                         set_ds_source(ds[i], "ALV");      
                                                   }    
												   data = (double *) calloc(NumberRuns, sizeof(double));
												   	if (ds[0]->ye == NULL) 
													   	{
                                                                 if ((alloc_data_set_y_error(ds[0]) == NULL) || (ds[0]->ye == NULL))
                                                                 return win_printf_OK("I can't create errors !");
                                                        }
	   												   for ( i = 0 ; i < k  ; i++)
    	   												   {
            	   												   for ( j = 0 ; j < (NumberRuns)  ; j++)
                        	   												   {
                                        	   												   data[j] = (double) ds[j + 1]->yd[i];
                        	   												   }    
    
	                           	   												   ds[0]->yd[i] = (float) gsl_stats_mean (data, 1 , NumberRuns);
	                           	   												   ds[0]->ye[i] = (float) gsl_stats_variance (data, 1 , NumberRuns);
	                                                       }
//	                                                      
                                                     yyterminate();
                                                      BEGIN(INITIAL);
                                                      
                                 }    
                                 
                                    
%%
int plot_parser_main( argc, argv )
int argc;
char **argv;
{


    
    O_p* op1 =NULL;
    d_s **position_of_ds_bin = NULL;

    
    op1 = (O_p*) calloc(1, sizeof(O_p *));
    
    ++argv, --argc;  /* skip over program name */
    if ( argc > 0 )
            yyin = fopen( argv[0], "r" );
    else
            {   win_printf("");
                return D_O_K;
            }
    
    yylex(op1 , position_of_ds_bin , argv[0]);
    


    return 0;
}
