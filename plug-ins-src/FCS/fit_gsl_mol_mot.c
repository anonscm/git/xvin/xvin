#include <stdlib.h>
#include <stdio.h>
#include "xvin.h"
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_const_mksa.h>
#include <gsl/gsl_multifit_nlin.h>

#define FIT(i) gsl_vector_get(s->x, i)
#define ERR(i) sqrt(gsl_matrix_get(covar,i,i))
#define fitted(x,parameters) fitted_function(x,parameters)
#define deriv_fitted_func_param_i(i,x,parameters) deriv_fitted_func_param(i,x,parameters)
#define name_main fit_gsl_mol_mot_main

#define N 1024 /*number of points for the plotting procedure*/
#define P num_parameters_to_fit 
#define num_parameters_to_fit 3/*nombre de parametres a fitter a changer*/
#define	a(x,i) gsl_vector_get(x, i)

/* variable declaration*/
double *parameters_of_the_fit;
/*int num_parameters_to_fit;*/

struct data {
  size_t n;
  double * y;
  double * sigma;
  double * xdata;
};



/*beginning of the routines*/


double fitted_function(double x_dat,gsl_vector *x) /*to change for a new function, the parameters are in x and noted a(x,i)*/
{
	double y_dat =0;
	
	if (a(x,1) == 0) return win_printf("Division by zero");
	
	y_dat = a(x,2) * (1+a(x,0)) / (1 + a(x,0)* exp (x_dat/a(x,1)));
      
	return y_dat;
}

double deriv_fitted_func_param(int i, double x_dat,gsl_vector *x) /*to change for a new function*/
{
	double dy_d_dat =0;
	
	if (a(x,1) == 0) return win_printf("Division by zero");
	
	if (i == 0) /*derivative respect to the first parameter*/
	{
			dy_d_dat =  a(x,2)*(1-exp(x_dat/a(x,1)))/pow(1+a(x, 0)*exp(x_dat/a(x,1)),2);
 	}
    
    else if(i == 1) /*derivative respect to the second parameter*/
    	{
			dy_d_dat = - a(x,2)*(1+a(x,0))*a(x,0)*x_dat/a(x,1)/a(x,1)*exp (x_dat/a(x,1))/pow(1+a(x,0)*exp(x_dat/a(x,1)),2);
      	}

    else if (i ==2)  /*derivative respect to the 3rd parameter*/
    	{
     		dy_d_dat = (1+a(x,0)) / (1 + a(x,0)* exp (x_dat/a(x,1)));
           		
        }
	
         else return win_printf("You have a problem");
 
 return dy_d_dat;
}

/***********************************************************************/
/*****You need no change beyond this line...in principle***************/
/***********************************************************************/

/*gsl_vector *enter_parameters(void)
{
	gsl_vector *para = NULL;
	int i;
	double param_transient;

	for (i =0; i<P; i++)
	{
		win_scanf("Enter parameter number %d \n %g",i,&param_transient);
  		gsl_vector_set(para,i,param_transient);
    }
      
 return para;   
}*/


/*int ask_number_of_parameters_to_fit(void)
{	
	win_scanf("How many parameters do you want to fit?%d",&num_parameters_to_fit);
	return 0;
	
}*/

int attribute_memory_for_fitting_parameters(void)
{
	parameters_of_the_fit = (double *)calloc(num_parameters_to_fit,sizeof(double));
	return 0;
}

int func_f (const gsl_vector * x, void *params, gsl_vector * f)
{
  size_t n = ((struct data *)params)->n;
  double *y = ((struct data *)params)->y;
  double *xdata = ((struct data *)params)->xdata;
  double *sigma = ((struct data *) params)->sigma;
  size_t i;
  double Yi = 0;
  gsl_vector_view xview;
  
  for (i = 0; i<P ; i++)
  {
    *(parameters_of_the_fit+i) = gsl_vector_get (x, i);
  }
  xview = gsl_vector_view_array (parameters_of_the_fit, P);/*there must be a more efficient way of doing that*/
  
  for (i = 0; i < n; i++)
    {
   		Yi = fitted(xdata[i],&xview.vector);
      	gsl_vector_set (f, i, (Yi - y[i])/sigma[i]);
    }

  return GSL_SUCCESS;
}




int func_df (const gsl_vector * x, void *params, gsl_matrix * J)
{
  size_t n = ((struct data *)params)->n;
  double *sigma = ((struct data *) params)->sigma;
  double *xdata = ((struct data *) params)->xdata;
  size_t i,j;
  double sig;
  double *deriv_parameters_of_the_fit;
  gsl_vector_view xview;
  
  for (i = 0; i<P ; i++)
  {
    *(parameters_of_the_fit+i) = gsl_vector_get (x, i);
  }
  xview = gsl_vector_view_array (parameters_of_the_fit, P);/*there must be a more efficient way of doing that*/
  
  deriv_parameters_of_the_fit = (double *)calloc(num_parameters_to_fit,sizeof(double));
	
  

  for (i = 0; i < n; i++)
    {
      /* Jacobian matrix J(i,j) = dfi / dxj, */
      /* where fi = (Yi - yi)/sigma[i],      */
      /*       Yi = your function yi your data  */
      /* and the vectj are the parameters of the fit */
      
      sig = sigma[i];
      for (j =0; j<P; j++)
         {
      *(deriv_parameters_of_the_fit+j) = deriv_fitted_func_param_i(j,xdata[i],&xview.vector)/sig;
      gsl_matrix_set (J, i, j, *(deriv_parameters_of_the_fit+j)); 
         }

    }
  return GSL_SUCCESS;
}



int func_fdf (const gsl_vector * x, void *params, gsl_vector * f, gsl_matrix * J)
{
  func_f (x, params, f);
  func_df (x, params, J);

  return GSL_SUCCESS;
}




int fit_mol_mot_func (void)
{
  const gsl_multifit_fdfsolver_type *T;
  gsl_multifit_fdfsolver *s;
  int status = 0;
  size_t i, iter = 0;
  int num_points_to_be_fitted ;
  //const size_t p = P;
  gsl_matrix *covar; //= gsl_matrix_alloc (p, p);
  double *y = NULL,*sigma = NULL,*xdata = NULL;
  struct data d;
  gsl_multifit_function_fdf f;
  static double *x_init; 
  static float *x_test_init;
  gsl_vector_view x;
  double chi;
  static O_p *op = NULL;
  static pltreg *pr = NULL;
  char label[1024];
  //p_l *dp;
  d_s *ds_plot_fit = NULL;
  char schar [256];
  static int error_flag = 0;
  static int err_data_set = 2;
  static float err_ampl = 0;
  
  if(updating_menu_state != 0)	return D_O_K;
  
  	/*We allocate the memory*/
  	//ask_number_of_parameters_to_fit();
  	attribute_memory_for_fitting_parameters();
  	covar = gsl_matrix_alloc (P, P);
  	
  	x_init= (double *)calloc(num_parameters_to_fit,sizeof(double));
  	x_test_init = (float *)calloc(num_parameters_to_fit,sizeof(float));
  	
   /* first set init values*/
   	*x_test_init =  1e-15;
    *(x_test_init+1) =  100.;
    *(x_test_init+2) =  100.;
    
    
    
    for (i=0;i<P;i++)
    {	
    	sprintf(schar,"x init[ %d]",i);
        win_scanf("%s = %f",schar,&x_test_init[i]);
        x_init[i] =(double)x_test_init[i];
    }
    
    x = gsl_vector_view_array (x_init, P);
    
    /* we now set the errors*/
    
    win_scanf("You want to work with : \n constant error type (0) \n relative error type (1) \n error type in another data set (2) \n error type in error bars (3)\n%d",&error_flag); 
    
    if ((error_flag > 3)  || (error_flag <0)) return 0;
    
    if (error_flag == 2)
    {
    	i = win_scanf("Errors are in data set?\n%d ",&err_data_set);
        if (i == CANCEL)	return OFF;
        if (op->dat[err_data_set]->nx != op->dat[op->cur_dat]->nx)	
		return win_printf_OK("your data sets differs in size!");
	
    }
    	
    if (error_flag < 2)
    {
    	i = win_scanf("Amplitude of the error ?\n%f",&err_ampl);
    	if (i == CANCEL)	return OFF;
   	}
    

    /*we must find the data to be fitted*/
    
    pr = find_pr_in_current_dialog(NULL);
    
    if (pr == NULL)	return 1;
    if (pr->o_p[pr->cur_op] == NULL) return 0;	
 	
    else
	{
		op=pr->o_p[pr->cur_op];
		num_points_to_be_fitted = get_ds_nx(op->dat[op->cur_dat]);
	}
	/*allocate memory for the data*/
	y = (double *)calloc(num_points_to_be_fitted*2,sizeof(double));
	xdata = (double *)calloc(num_points_to_be_fitted*2,sizeof(double));
	sigma = (double *)calloc(num_points_to_be_fitted,sizeof(double));
		
	
	for (i=0;i<num_points_to_be_fitted;i++)
	{
		xdata[i] = (double)op->dat[op->cur_dat]->xd[i];
		y[i] = (double)op->dat[op->cur_dat]->yd[i];
		switch  (error_flag)
		{	
			case 0 : sigma[i] = err_ampl;
   						break;
			case 1 : sigma[i] = op->dat[op->cur_dat]->yd[i]*err_ampl;
   						break;
			case 2 : sigma[i] = op->dat[err_data_set]->yd[i];
   						break;
			case 3 : sigma[i] = op->dat[op->cur_dat]->ye[i];
   						break;
		}
	}
	/*put them in the gsl format*/
	d.n =  num_points_to_be_fitted;
 	d.y = y;
  	d.sigma = sigma;
   	d.xdata =xdata;
   	
	/*set the solver parameters*/	
  
  f.f = &func_f;
  f.df = &func_df;
  f.fdf = &func_fdf;
  f.n = num_points_to_be_fitted;
  f.p = P;
  f.params = &d;
   
  T = gsl_multifit_fdfsolver_lmsder;
  s = gsl_multifit_fdfsolver_alloc (T, num_points_to_be_fitted, P);
  gsl_multifit_fdfsolver_set (s, &f, &x.vector);
  
  /* fit procedure starts*/
  do
  {
      iter++;
      status = gsl_multifit_fdfsolver_iterate (s);

      if (status)
        break;

      status = gsl_multifit_test_delta (s->dx, s->x,1e-10, 1e-10);/*end of fit criterium*/
  }
  while ((status == GSL_CONTINUE) && (iter < 5000));
    
  gsl_multifit_covar (s->J, 0.0, covar);
  
  
  /*plot the fitted curve with the same x data*/
  
  
  if ((ds_plot_fit = create_and_attach_one_ds(op, num_points_to_be_fitted, num_points_to_be_fitted, 0)) == NULL)

					    	return 0;
					    	

  for (i = 0; i < num_points_to_be_fitted; i++)
    {
      
      ds_plot_fit->xd[i] = xdata[i];
      
      /* Here is the function*/
      
      ds_plot_fit->yd[i] = fitted(xdata[i],(s->x));/*FIT(2)* (1+FIT(0)) / (1 + FIT(0)* exp (FIT(1) * xdata[i] / (Temp )))            ;*/
      
      
    }

 
  
  chi = gsl_blas_dnrm2(s->f);
    
  
  sprintf (label, "\\fbox{\\pt8\\stack{{\\sl f(t) = a_2 * (1+a_0)/(1+a_0*exp(F/a_1)}"
	"{a_0 = %g \\stack{+-} %g}{a1 = %g \\stack{+-} %g}{a2 = %g \\stack{+-} %g}"
	"{\\chi^2_R = %g n = %d}}}",FIT(0),ERR(0),FIT(1),ERR(1),FIT(2),ERR(2),pow(chi, 2.0)/ (num_points_to_be_fitted - P),num_points_to_be_fitted);
 
  
	push_plot_label(op, op->x_lo + (op->x_hi - op->x_lo)/4, 
	op->y_hi - (op->y_hi - op->y_lo)/4, label, USR_COORD);
  
  
  gsl_multifit_fdfsolver_free (s);
  op->need_to_refresh = 1;
  refresh_plot(pr, UNCHANGED);
  broadcast_dialog_message(MSG_DRAW,0);
  
  return 0;
}



/* This function allows to simulate the required function by creating in the same plot a nexw data set*/
int simul_mol_motor (void)
{
  double y[N], sigma[N];
  int i;
  const gsl_rng_type * type;
  gsl_rng * r;
  static float noise_amp =.1;
  //static float A = 1e-15; 
  //static float delta = 100;
  //static float v_0 = 99;
  
  static double *x_init; 
  static float *x_test_init;
  gsl_vector_view x;
  
  static O_p *op = NULL;
  static pltreg *pr = NULL;
  char title[1024];
  char schar[1024];
  d_s *ds_sim_data = NULL;
  if(updating_menu_state != 0)	return D_O_K;
  	/*We allocate the memory*/
  	//ask_number_of_parameters_to_fit();
  	attribute_memory_for_fitting_parameters();
  	
  	
  	x_init= (double *)calloc(num_parameters_to_fit,sizeof(double));
  	x_test_init = (float *)calloc(num_parameters_to_fit,sizeof(float));
  	
   /* first set init values*/
   	*x_test_init =  1e-15;
    *(x_test_init+1) =  100.;
    *(x_test_init+2) =  100.;
    
    
    
    for (i=0;i<P;i++)
    {	
    	sprintf(schar,"x init[ %d]",i);
        win_scanf("%s = %f",schar,&x_test_init[i]);
        x_init[i] =(double)x_test_init[i];
    }
    
    x = gsl_vector_view_array (x_init, P);
  
    /*enter parameter to simulate data*/
    //win_scanf("Enter the parameters \n A= %f \n Delta= %f \n v_0 = %f \n amplitude of the noise",&A,&delta,&v_0,&noise_amp);
    
    /*do what has to be done to treat the windows*/
    pr = find_pr_in_current_dialog(NULL);
    
    if (pr == NULL)	return 1;
    
  	if (op == NULL) 
 	{
  
  		op = create_one_plot(N, N, 0);
  		add_data( pr, IS_ONE_PLOT, (void*)op);
  		
	
  		/*free_data_set(op->dat[op->cur_dat]);*/
  		
    }
  
  else
	{
		op=pr->o_p[pr->cur_op];
		
	}
	if ((ds_sim_data = create_and_attach_one_ds(op, N, N, 0)) == NULL)

					    	return 0;
		
	
	/*We now set the noise*/  
  gsl_rng_env_setup();

  type = gsl_rng_default;
  r = gsl_rng_alloc (type);

  /*we calculate the data*/

  
  for (i = 0; i < N; i++)
    {
      /*here is the function*/
      y[i] = fitted(i,&x.vector)+ gsl_ran_gaussian (r, noise_amp);
      ds_sim_data->xd[i] = i;
      ds_sim_data->yd[i] = y[i];      
      sigma[i] = (double) noise_amp;
      
    };
    /*and plot them*/
  sprintf (title, "A = %g\n Delta = %g\n V_0= %g\n noise %g",gsl_vector_get(&x.vector,0),gsl_vector_get(&x.vector,1),gsl_vector_get(&x.vector,2),noise_amp);
  
  set_plot_title(op, title);
  op->need_to_refresh = 1;
  refresh_plot(pr, UNCHANGED);
  broadcast_dialog_message(MSG_DRAW,0);
  return 0;
}


MENU *fit_gsl_mol_mot_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;

	

	add_item_to_menu(mn,"Fit molecular motor",    fit_mol_mot_func, NULL ,  0, NULL  ); 
	add_item_to_menu(mn,"Simulate molecular motor",    simul_mol_motor, NULL ,  0, NULL  );
	

	
			
	
	
	return mn;
}

int	name_main(int argc, char **argv)
{
	add_plot_treat_menu_item ( "fit gsl motor", NULL, fit_gsl_mol_mot_plot_menu(), 0, NULL);
	return D_O_K;
}


