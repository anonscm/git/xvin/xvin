# include "xvin.h"
# include "xvin2.h"
# include "xvadd.h"
# include <float.h>
    


char *read_avcor_data(char *path, char *file, FILE **fpi)
{
	FILE *fp;
	char filename[256];
	static char *line = NULL;
	char *c, *l;


	if (line == NULL)
	{
		line = (char *)calloc(4096,sizeof(unsigned char));
		if (line == NULL) return win_printf_ptr("Cannot alloc memory");
	}
	if (*fpi == NULL)
	{
		build_full_file_name(filename, 256, path, file);
		fp = fopen(filename,"rb");
		if (fp == NULL)
			return win_printf_ptr("Cannot open :\n%s",filename);
		*fpi = fp;
		c =  NULL;
		l = line;
		while (c == NULL && l != NULL)
		{
			l = fgets(line, 4096, *fpi);
			c = strstr(l,"Correlation (Multi, Averaged)");
		}
		if (l == NULL)
			return win_printf_ptr("File %s\nis not a correlation function",filename);
		l = fgets(line, 4096, *fpi);

	}	
	l = fgets(line, 4096, *fpi);
/*	fprintf(stderr,"%s\n",l);*/
	return l;
}



int	add_error_bars(O_p *op, d_s *dsi, d_s *dsr)
{
	register int i;
	d_s  *dsd;
	int nf;
	
	if (dsi->nx != dsr->nx)	return win_printf("data set have different size !");

	nf = dsi->nx;
	if ((dsd = create_and_attach_one_ds(op, 2*nf, 2*nf, 0)) == NULL)
		return win_printf("cannot create plot");	

	for (i = 0; i < nf; i++)
	{		
		if (dsi->xd[i] != dsr->xd[i])	
			win_printf("pts at index %d differ in X\nData X = %g error X %g!",i,dsi->xd[i],dsr->xd[i]);
		dsd->xd[2*i] = dsd->xd[2*i+1] = dsi->xd[i];
		dsd->yd[2*i] = dsi->yd[i] + dsr->yd[i];
		dsd->yd[2*i+1] = dsi->yd[i] - dsr->yd[i];
	}
	set_dash_line(dsd);
	set_plot_symb(dsd, "-");
	inherit_from_ds_to_ds(dsd, dsi);
	return 0;
}


acreg *create_avcor_plot(char *path, char *file)
{
	register int i, j;
	FILE *fpi = NULL;
	acreg *ac;
	pltreg *pr;
	O_p *op;
	d_s *ds, *dss;
	char *line, *bp;
	int ne;
	char filename[256];	
	float t, ga, g, gs = 0, tmp;
	
	line = read_avcor_data(path, file, &fpi);
	if (line == NULL)	return NULL;
	
	for (i = 0, bp = line, gs = 0; bp != NULL && i < 50 ; i++)
	{
		g = _strtold(bp, &bp);	
		if (i == 0) t = g;
		else if (i == 1) ga = g;
		else gs += (g - ga)*(g - ga);
		if (sscanf(bp,"%f",&tmp) != 1) bp = NULL;
	}
	if (i > 2) gs /= (i-2);
	gs = sqrt(gs);
	if (i > 2) gs /= sqrt(i-2); 
	ne = i;
		
	/* win_printf("number of averaging %d",ne-2);			*/
	
	ac = create_plot_ac(file, 512, 512, 0);
	if (ac_grep(ac,"%pr%op%ds",&pr,&op,&ds) != 3)
			return win_printf_ptr("cannot find plot!");
			
	if ((dss = create_and_attach_one_ds(op, 512, 512, 0)) == NULL)
		return win_printf_ptr("cannot create plot !");
				
	build_full_file_name(filename, 256, path, file);		
	backslash_to_slash(filename);
	ds->source = my_sprintf(NULL,"%s avcor",filename);
	for (i = 0; file[i] != 0 && file[i] != '.'; i++)
		filename[i] = file[i];
	if (file[i] == '.') filename[i] = 0;
	else filename[i+1] = 0;
	sprintf(filename,"%s.gr",filename);
	op->filename = strdup(filename);






	ds->xd[0] = dss->xd[0] = t;
	ds->yd[0] = ga;
	dss->yd[0] = gs;
	i = ne;
/*	win_printf("reading line %d t %g g %g s %g navg %d",0,t,ga,gs,i-2);*/
	
	j = 1;
	while (i == ne && j < 512)
	{
		line = read_avcor_data(path, file, &fpi);
		for (i = 0, bp = line, gs = 0; bp != NULL && i < 50 ; i++)
		{
			g = _strtold(bp, &bp);	
			if (i == 0) t = g;
			else if (i == 1) ga = g;
			else gs += (g - ga)*(g - ga);
			if (sscanf(bp,"%f",&tmp) != 1) bp = NULL;
		}
		if (i == ne)
		{
			if (i > 2) gs /= (i-2);
			gs = sqrt(gs);	
			if (i > 2) gs /= sqrt(i-2); 			
			ds->xd[j] = dss->xd[j] = t;
			ds->yd[j] = ga;
			dss->yd[j] = gs;	
			j++;
/*			win_printf("reading line %d t %g g %g s %g navg %d",j,t,ga,gs,i-2);*/
		}
	}
	op->dir = Mystrdup(path);
	ds->nx = ds->ny = dss->nx = dss->ny = j;
	
	
	set_plot_title(op, "\\stack{{fluoresence Correlation spectroscopie}{ average %d npt of points %d}}",ne-2,j);
	set_plot_x_title(op, "Time \\tau (ms)");
	set_plot_y_title(op, "g(\\tau)");
	set_plot_x_log(op);
	add_error_bars(op, ds, dss);
	fclose(fpi);
	do_one_plot(pr);							
	return ac;
}

int	load_avcor_movie(char ch)
{
	register int i;
	static char c_dir[256], *c = NULL, *prev_file = NULL, f[32];
	int	display_loading_win(char *path, char *pattern, char *file, char *purpose);	
	static char pattern[128], *pat = "*.asc";
	extern char	file_pattern[128];
	acreg *ac;
	
	if (ch != CR)	return OFF;
	i = display_loading_win(c,pat,prev_file,"loading avcor correlation");
	if (i == CANCEL)	return NULL;
	if (select_file.ff_name[0] == 0) 	return NULL;
	strcpy(c_dir,file_path);
	c = c_dir;	
	set_cursor_glass();
	strcpy(f,select_file.ff_name);
	prev_file = f;
	strncpy(pattern,file_pattern,127);	
	pat = pattern;	
	
/*	win_printf("file %s \npath %s",f,backslash_to_slash(file_path));*/
	ac = create_avcor_plot(file_path, f);
/*	win_printf("after create");*/
	activate_acreg(ac);
	

	reset_cursor();	
	return 0;
}





int		avcor_main(int argc, char **argv)
{
	load_avcor_movie(CR);
	return 0;
}
