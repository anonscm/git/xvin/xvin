#ifndef FCSALV_PARSER_H_
#define FCSALV_PARSER_H_

PXV_FUNC(int,FCSALV_parserlex,( O_p *op, d_s **position_of_ds_bin, char *filename));
PXV_FUNC(int,FCSALV_parserwrap,(void ));
PXV_FUNC(int,ALV_parserlex,( O_p *op, char *filename));
PXV_FUNC(int,ALV_parserwrap,(void ));

#endif

