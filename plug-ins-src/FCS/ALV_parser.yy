%{
#include "xvin.h"
#include "gsl_statistics_double.h" 
int io = 0;
int jo = 0, ko = 0, lo = 0, mo = 0;
extern char *filename;
d_s *ds = NULL, *APD1 = NULL, *APD2= NULL;
//double *datao = NULL;
float tmpx[32768];
float tmpy[32768];
float tmpAPDt[32768];
float tmpAPD1[32768];
float tmpAPD2[32768];
int dscount = 0;





#define YY_DECL int yylex YY_PROTO(( O_p *op_data, O_p *op_APD, char *filename,int first))



%}
/*general definition*/


DIGIT    [0-9]
%option noyywrap

%x is_ALV
%x Correlation
%x is_count_rate

                                                        




%%

"ALV-6000" BEGIN(is_ALV);
<is_ALV>"/E-WIN"   {
//                        win_printf("%s",yytext);
//                      if (strcmp ( "/E-WIN" , yytext ) != (strlen("/E-WIN")+1))
//                                            {
//                                                 win_printf("NOT AN ALV FILE");
//                                                 yyterminate();
//                                            }
//                      else win_printf("POTATO");
//                        BEGIN(INITIAL);
                        lo = 0;
                        mo = 0;
                        jo = 0;
                        if (first == 0) dscount = 0;
                       //draw_bubble(screen,0,800,250,"jo %d",jo);
//                        win_printf("jo = %d",jo);
                        ko = 0;
                        
                        
//                      win_printf("ko = %d",jo);
                   	    BEGIN(INITIAL);
                                    
                    }
                    


"Correlation" BEGIN(Correlation);

                               


<Correlation>[\-0-9]+"."[0-9E\+\-]* {
                                        //draw_bubble(screen,0,800,350,"YOP jo %d",jo);
                                        //win_printf("YOP %s",yytext);
                                        if ( jo%2 == 0)  tmpx[ko] = atof(yytext);
                                        if ( jo%2 == 1)  {
                                                           tmpy[ko] = atof(yytext);
                                                           ko++;
                                                         }    
                                        
                                        jo++;
                                    }        
<Correlation>"Count Rate"  BEGIN(is_count_rate);

<is_count_rate>[\-0-9]+"."[0-9\+\-]*              {
                                                    //draw_bubble(screen,0,600,350,"lo %d",lo);
                                                    if ( lo%3 == 0)  tmpAPDt[mo] = atof(yytext);
                                                    if ( lo%3 == 1)  tmpAPD1[mo] = atof(yytext);
                                                    if ( lo%3 == 2)  
                                                    {
                                                        tmpAPD2[mo] = atof(yytext);
                                                        mo++;
                                                    }    
                                                    
                                                    lo++;
                                                    
                                                  }   
                                                  
<is_count_rate>"StandardDeviation"         {
                                               
                                               ds = create_and_attach_one_ds (op_data, ko  , ko  , IS_FLOAT);
                                               if (ds == NULL)  return win_printf("NULL");
                                               set_ds_source(ds, "ALV-%d",dscount);
                                               APD1 = create_and_attach_one_ds (op_APD, mo  , mo  , IS_FLOAT);
                                               if (APD1 == NULL)  return win_printf("NULL");
                                               set_ds_source(APD1, "APD1-%d",dscount);
                                               APD2 = create_and_attach_one_ds (op_APD, mo  , mo  , IS_FLOAT);
                                               if (APD2 == NULL)  return win_printf("NULL");
                                               set_ds_source(APD2, "APD2-%d",dscount);
                                               //win_printf("jo = %d",jo);
                                               for (jo = 0; jo < ko ; jo++)
                                               {
                                                   ds->xd[jo] = tmpx[jo];
                                                   ds->yd[jo] = tmpy[jo];
                                               } 
                                               for (jo = 0; jo < mo ; jo++)
                                               {
                                                   APD1->xd[jo] = APD2->xd[jo] = tmpAPDt[jo];
                                                   APD1->yd[jo] = tmpAPD1[jo];
                                                   APD2->yd[jo] = -tmpAPD2[jo];
                                               }    
                                                 //win_printf("Affected");
                                      

	                                    
                                         jo=0;
                                         ko=0; 
                                         lo = 0;
                                         mo = 0;  
                                         dscount++;
                                         //win_printf("OUT"); 
                                        BEGIN(INITIAL);
                                        BEGIN(INITIAL);
                                        BEGIN(INITIAL);              
                                        yyterminate();
                                        
                                        
                                        
                                 }     
                                        
<<EOF>> {//EVITER CALCUL MOYENNE A CHAQUE FOIS
                                               //win_printf("EOF");
                                               ds = create_and_attach_one_ds (op_data, ko  , ko  , IS_FLOAT);
                                               if (ds == NULL)  return win_printf("NULL");
                                               set_ds_source(ds, "ALV-%d",dscount);
                                               APD1 = create_and_attach_one_ds (op_APD, mo  , mo  , IS_FLOAT);
                                               if (APD1 == NULL)  return win_printf("NULL");
                                               set_ds_source(APD1, "APD1-%d",dscount);
                                               APD2 = create_and_attach_one_ds (op_APD, mo  , mo  , IS_FLOAT);
                                               if (APD2 == NULL)  return win_printf("NULL");
                                               set_ds_source(APD2, "APD2-%d",dscount);
                                               //win_printf("jo = %d",jo);
                                               for (jo = 0; jo < ko ; jo++)
                                               {
                                                   ds->xd[jo] = tmpx[jo];
                                                   ds->yd[jo] = tmpy[jo];
                                               } 
                                               for (jo = 0; jo < mo ; jo++)
                                               {
                                                   APD1->xd[jo] = APD2->xd[jo] = tmpAPDt[jo];
                                                   APD1->yd[jo] = tmpAPD1[jo];
                                                   APD2->yd[jo] = -tmpAPD2[jo];
                                               }    
                                                 //win_printf("Affected");
                                      

	                                    
                                         jo=0;
                                         ko=0; 
                                         lo = 0;
                                         mo = 0;  
                                         dscount++;
                                         //win_printf("OUT"); 
                                         BEGIN(INITIAL);
                                        BEGIN(INITIAL);
                                        BEGIN(INITIAL);              
                                        yyterminate();
                                        
                                        
                                        
                                 }    
%%
int ALV_parser_main( argc, argv )
int argc;
char **argv;
{


    
    O_p* op1 =NULL;
    O_p* op2 =NULL;
    int i = 0;

    
    op1 = (O_p*) calloc(1, sizeof(O_p *));
    op2 = (O_p*) calloc(1, sizeof(O_p *)); 
    ++argv, --argc;  /* skip over program name */
    if ( argc > 0 )
            yyin = fopen( argv[0], "r" );
    else
            {   win_printf("");
                return D_O_K;
            }
    
    yylex(op1 , op1 , argv[0],i);
    


    return 0;
}
