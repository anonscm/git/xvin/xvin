/*
*    Plug-in program for plotting ALV files
*
*    JF Allemand
*
* some upgrades by Pierre Neveu
*/

# ifndef FCSALV_C
# define FCSALV_C

#include "../../include/allegro.h"
#include "../../include/winalleg.h"
#include "gsl_statistics_double.h" 
#include "../../include/xvin.h"

# define BUILDING_PLUGINS_DLL



#include "FCSALV_parser.h"

int calculate_FCS_average(void)
{
    O_p *op_avg = NULL;
    O_p *op_data = NULL;
    O_p *op_APD = NULL;
    pltreg *pr = NULL;
    d_s *ds_avg = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    double *datao = NULL;
    int i,j;
    
    
    if(updating_menu_state != 0)	return D_O_K;
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
		return win_printf_OK("cannot find data");
		//Je fais un mauvais test initial
    if (find_source_specific_ds_in_op(pr->o_p[2], "ALV-1") == NULL) return win_printf_OK("This is not an ALV plot");
    
    op_avg = pr->o_p[0] ;
    op_APD = pr->o_p[1] ; 
    op_data = pr->o_p[2] ;
    
    ds_avg = find_source_specific_ds_in_op(pr->o_p[0], "ALV average");
    
    if (ds_avg == NULL) return win_printf_OK("This is not an ALV plot");
    datao = (double *) calloc(op_data->n_dat, sizeof(double));
		if (ds_avg->ye == NULL) 
		{
               if ((alloc_data_set_y_error(ds_avg) == NULL) )
               return win_printf_OK("I can't create errors !");
        }
        for ( i = 0 ; i < op_data->dat[0]->nx  ; i++)
		{
            	 for ( j = 0 ; j < op_data->n_dat  ; j++)
                  {
                        datao[j] = (double) op_data->dat[j]->yd[i];
                  }    
                 
                 ds_avg->xd[i] = op_data->dat[0]->xd[i];
	             ds_avg->yd[i] = (float) gsl_stats_mean (datao, 1 , op_data->n_dat);
	             ds_avg->ye[i] = (float) gsl_stats_sd_m  (datao, 1 , op_data->n_dat, ds_avg->yd[i]);
        }
   free(datao); 
   return D_O_K;
}   

int calculate_APD_average(void)
{
    O_p *op_avg = NULL;
    O_p *op_data = NULL;
    O_p *op_APD = NULL;
    d_s *ds_APD1 = NULL ; 
    d_s *ds_APD2 = NULL;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    int i,j;
    double * APD = NULL;
    
    
    
    if(updating_menu_state != 0)	return D_O_K;
    
    //win_printf("in APD AVG");
    
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
		return win_printf_OK("cannot find data");
		//Je fais un mauvais test initial
    if (find_source_specific_ds_in_op(pr->o_p[2], "ALV-1") == NULL) return win_printf_OK("This is not an ALV plot");
    
    op_avg = pr->o_p[0] ;
    op_APD = pr->o_p[1] ; 
    op_data = pr->o_p[2] ;
    
    ds_APD1 = find_source_specific_ds_in_op(pr->o_p[1], "APD1 average");
    ds_APD2 = find_source_specific_ds_in_op(pr->o_p[1], "APD2 average");
    
    
    
    if (ds_APD1 == NULL)
    {
        ds_APD1 = create_and_attach_one_ds (op_APD, op_APD->dat[0]->nx  , op_APD->dat[0]->nx , IS_FLOAT);
        if (ds_APD1 == NULL) return win_printf_OK("Bad ds ");
        set_ds_source(ds_APD1, "APD1 average");
    }    
    if (ds_APD2 == NULL)
    {
        ds_APD2 = create_and_attach_one_ds (op_APD, op_APD->dat[0]->nx  , op_APD->dat[0]->nx , IS_FLOAT);    APD = (double *) calloc(op_APD->n_dat%2, sizeof(double));
        if (ds_APD2 == NULL) return win_printf_OK("Bad ds ");
        set_ds_source(ds_APD2, "APD2 average");
    }    
	
	//win_printf("ds_APD1->nx %d \n ds_APD2->nx %d \n op_APD->n_dat %d",ds_APD1->nx,ds_APD2->nx,op_APD->n_dat);
	
	APD = (double *) calloc(op_APD->n_dat, sizeof(double));
	
	if (APD == NULL) return win_printf_OK("Alloc failed");
	//win_printf("middle APD AVG");
	
    if (ds_APD1->ye == NULL) 
	{
         if ((alloc_data_set_y_error(ds_APD1) == NULL) )
         return win_printf_OK("I can't create errors !");
    }
    
    //win_printf("Before  AVG 1");
    for ( i = 0 ; i < op_APD->dat[0]->nx   ; i++)
    {
            	 for ( j = 0 ; j < op_APD->n_dat - 2  ; j=j+2)
                  {
                      
                        APD[j/2] = (double) op_APD->dat[j]->yd[i];
                        
                  }    
                 
                 ds_APD1->xd[i] = op_APD->dat[0]->xd[i];
                 ds_APD1->yd[i] = 0;
                 //draw_bubble(screen,0,700,250,"op_APD->dat[0]->nx %d i %d ds_APD1->xd[i] %f",op_APD->dat[0]->nx,i,ds_APD1->xd[i]);
	             ds_APD1->yd[i] = (float) gsl_stats_mean (APD, 1 , op_APD->n_dat/2);
	             ds_APD1->ye[i] = (float) gsl_stats_sd_m (APD, 1 , op_APD->n_dat/2, (double)ds_APD1->yd[i]);
    }
    
//    win_printf("After  AVG 1");
    if (ds_APD2->ye == NULL) 
	{
         if ((alloc_data_set_y_error(ds_APD2) == NULL) )
         return win_printf_OK("I can't create errors !");
    }
    //win_printf("Before  AVG 2");
    for ( i = 0 ; i < op_APD->dat[0]->nx  ; i++)
    {
            	 for ( j = 1 ; j < op_APD->n_dat - 2  ; j=j+2)
                  {
                        APD[(j-1)/2] = (double) op_APD->dat[j]->yd[i];
                  }    
                 
                 ds_APD2->xd[i] = op_APD->dat[0]->xd[i];
	             ds_APD2->yd[i] = (float) gsl_stats_mean (APD, 1 , op_APD->n_dat/2);
	             ds_APD2->ye[i] = (float) gsl_stats_sd_m (APD, 1 , op_APD->n_dat/2 , ds_APD2->yd[i]);
   }
   
   //win_printf("After  AVG 2");
    
   free(APD);

   //set_ds_source(ds_APD1, "APD1 average"); 
//   set_ds_source(ds_APD2, "APD2 average");  
//    
    //win_printf("OUT");
    
    return D_O_K;   
}    


int retreat_from_APD_signal(void)
{   
    pltreg * pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    int i,j,k;
    O_p *op_avg = NULL;
    O_p *op_data = NULL;
    O_p *op_APD = NULL;
    d_s *ds_APD1 = NULL ; 
    d_s *ds_APD2 = NULL;
    double *dist = NULL;
    int numb_to_remove;
    int To_Remove;
    
    if(updating_menu_state != 0)	return D_O_K;
    
    calculate_APD_average();
    
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
		return win_printf_OK("cannot find data");
		//Je fais un mauvais test initial
    if (find_source_specific_ds_in_op(pr->o_p[2], "ALV-1") == NULL) return win_printf_OK("This is not an ALV plot");
    
    op_avg = pr->o_p[0] ;
    op_APD = pr->o_p[1] ; 
    op_data = pr->o_p[2] ;
    
    ds_APD1 = find_source_specific_ds_in_op(pr->o_p[1], "APD1 average");
    ds_APD2 = find_source_specific_ds_in_op(pr->o_p[1], "APD2 average");
    
    numb_to_remove = (op_APD->n_dat -1) / 2 ;
    
    dist = (double *) calloc(op_APD->n_dat, sizeof(double));
    
    win_scanf("You want to remove how many datasets %d?",&numb_to_remove);
    
    if (numb_to_remove >= (pr->o_p[1]->n_dat/2)) return win_printf_OK("You ask too much");
    for ( k = 0 ; k <numb_to_remove ; k++)
    {
        for (i = 0 ; i < op_APD->n_dat ; i+=2)
            {
                    for (j = 0 ; j < op_APD->dat[0]->nx ; j++)
                               dist[i] += (double) op_APD->dat[i]->yd[j]*op_APD->dat[i]->yd[j] - ((i%2 == 0) ? ds_APD1->yd[j]*ds_APD1->yd[j] : ds_APD2->yd[j]*ds_APD2->yd[j]);
            }    
        To_Remove = gsl_stats_max_index (dist, 1 , op_APD->n_dat);
        //win_printf("To_Remove %d",To_Remove);
        remove_ds_from_op(op_APD, op_APD->dat[(To_Remove %2 == 0) ? To_Remove + 1 : To_Remove]);
        remove_ds_from_op(op_APD, op_APD->dat[(To_Remove %2 == 0) ? To_Remove : To_Remove +1]);
        remove_ds_from_op(op_data, op_data->dat[To_Remove /2]);
        calculate_APD_average();
        for (i = 0 ; i < op_APD->n_dat ; i+=2)
           dist[i] = 0;
    }    
    //win_printf_OK("Before average");
    calculate_FCS_average();
    
    return D_O_K;
}    
 


int retreat_from_APD_signal2_1(void)
{   
    pltreg * pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    int i,k;
    O_p *op_avg = NULL;
    O_p *op_data = NULL;
    O_p *op_APD = NULL;
    d_s *ds_APD1 = NULL ; 
    d_s *ds_APD2 = NULL;
    //double *dist = NULL;
    float average_min;
    float average_max;
    double avg_min;
    double avg_max;

    //int numb_to_remove;
   // int To_Remove;
     double *avg = NULL;
    
    if(updating_menu_state != 0)	return D_O_K;
    
    calculate_APD_average();
    
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
		return win_printf_OK("cannot find data");
		//Je fais un mauvais test initial
    if (find_source_specific_ds_in_op(pr->o_p[2], "ALV-1") == NULL) return win_printf_OK("This is not an ALV plot");
    
    op_avg = pr->o_p[0] ;
    op_APD = pr->o_p[1] ; 
    op_data = pr->o_p[2] ;
    
    ds_APD1 = find_source_specific_ds_in_op(pr->o_p[1], "APD1 average");
    ds_APD2 = find_source_specific_ds_in_op(pr->o_p[1], "APD2 average");
    
    //numb_to_remove = (op_APD->n_dat -1) / 2 ;
    
    //dist = (double *) calloc(op_APD->n_dat, sizeof(double));
    
    average_min=0;
    average_max=600;
    
    win_scanf("Minimum count on APD (kHz) %f?\n Maximum count on APD (kHz) %f?",&average_min,&average_max);
    
    
    //win_scanf("Minimum count on APD %f",&average_min);
    
    
    //win_scanf("Maximum count on APD %f",&average_max);
    
    avg_min= (double) average_min;
    avg_max= (double) average_max;
    
    
   // win_printf("Maximum count on APD %g",avg_max);
    
    
    avg = (double *) calloc(op_APD->dat[0]->nx, sizeof(double));
    
    
    //win_scanf("You want to remove how many datasets %d?",&numb_to_remove);
    
    //if (numb_to_remove >= (pr->o_p[1]->n_dat/2)) return win_printf_OK("You ask too much");
    for ( k = (op_APD->n_dat +1) / 2; k >0 ; k--)
    {
        //for (i = 0 ; i < op_APD->n_dat ; i+=2)
          //  {
            //        for (j = 0 ; j < op_APD->dat[0]->nx ; j++)
              //                 dist[i] += (double) op_APD->dat[i]->yd[j]*op_APD->dat[i]->yd[j] - ((i%2 == 0) ? ds_APD1->yd[j]*ds_APD1->yd[j] : ds_APD2->yd[j]*ds_APD2->yd[j]);
            //}    
        //To_Remove = gsl_stats_max_index (dist, 1 , op_APD->n_dat);
        //win_printf("To_Remove %d",To_Remove);
        
        

        for (i = 0 ; i < op_APD->dat[0]->nx ; i++)
            {
                    
                               avg[i] = (double) op_APD->dat[2*k-2]->yd[i];
            } 
        
                
        if ( (avg_max - gsl_stats_mean( avg,1, op_APD->dat[0]->nx))*(gsl_stats_mean( avg,1, op_APD->dat[0]->nx) - avg_min)<0)
        {    
             
             remove_ds_from_op(op_APD, op_APD->dat[2*k-1]);
             remove_ds_from_op(op_APD, op_APD->dat[2*k-2]);
             remove_ds_from_op(op_data, op_data->dat[k-1]);
        }
        
        //  for (i = 0 ; i < op_APD->n_dat ; i+=2)
       //    dist[i] = 0;
    }    
    //win_printf_OK("Before average");
    
     free(avg);
    
    calculate_APD_average();
      
    calculate_FCS_average();
    
    return D_O_K;
}    
 
 int retreat_from_APD_signal2_2(void)
{   
    pltreg * pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    int i,k;
    O_p *op_avg = NULL;
    O_p *op_data = NULL;
    O_p *op_APD = NULL;
    d_s *ds_APD1 = NULL ; 
    d_s *ds_APD2 = NULL;
    //double *dist = NULL;
    float average_min;
    float average_max;
    double avg_min;
    double avg_max;

    //int numb_to_remove;
   // int To_Remove;
     double *avg = NULL;
    
    if(updating_menu_state != 0)	return D_O_K;
    
    calculate_APD_average();
    
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
		return win_printf_OK("cannot find data");
		//Je fais un mauvais test initial
    if (find_source_specific_ds_in_op(pr->o_p[2], "ALV-1") == NULL) return win_printf_OK("This is not an ALV plot");
    
    op_avg = pr->o_p[0] ;
    op_APD = pr->o_p[1] ; 
    op_data = pr->o_p[2] ;
    
    ds_APD1 = find_source_specific_ds_in_op(pr->o_p[1], "APD1 average");
    ds_APD2 = find_source_specific_ds_in_op(pr->o_p[1], "APD2 average");
    
    //numb_to_remove = (op_APD->n_dat -1) / 2 ;
    
    //dist = (double *) calloc(op_APD->n_dat, sizeof(double));
    
    average_min=0;
    average_max=600;
    
    win_scanf("Minimum count on APD (kHz) %f?\n Maximum count on APD (kHz) %f?",&average_min,&average_max);
    
    
    //win_scanf("Minimum count on APD %f",&average_min);
    
    
    //win_scanf("Maximum count on APD %f",&average_max);
    
    avg_min= (double) average_min;
    avg_max= (double) average_max;
    
    
   // win_printf("Maximum count on APD %g",avg_max);
    
    
    avg = (double *) calloc(op_APD->dat[0]->nx, sizeof(double));
    
    
    //win_scanf("You want to remove how many datasets %d?",&numb_to_remove);
    
    //if (numb_to_remove >= (pr->o_p[1]->n_dat/2)) return win_printf_OK("You ask too much");
    for ( k = (op_APD->n_dat +1) / 2; k >0 ; k--)
    {
        //for (i = 0 ; i < op_APD->n_dat ; i+=2)
          //  {
            //        for (j = 0 ; j < op_APD->dat[0]->nx ; j++)
              //                 dist[i] += (double) op_APD->dat[i]->yd[j]*op_APD->dat[i]->yd[j] - ((i%2 == 0) ? ds_APD1->yd[j]*ds_APD1->yd[j] : ds_APD2->yd[j]*ds_APD2->yd[j]);
            //}    
        //To_Remove = gsl_stats_max_index (dist, 1 , op_APD->n_dat);
        //win_printf("To_Remove %d",To_Remove);
        
        

        for (i = 0 ; i < op_APD->dat[0]->nx ; i++)
            {
                    
                               avg[i] = (double) op_APD->dat[2*k-1]->yd[i];
            } 
        
                
        if ( (avg_max + gsl_stats_mean( avg,1, op_APD->dat[0]->nx))*(-gsl_stats_mean( avg,1, op_APD->dat[0]->nx) - avg_min)<0)
        {    
             
             remove_ds_from_op(op_APD, op_APD->dat[2*k-1]);
             remove_ds_from_op(op_APD, op_APD->dat[2*k-2]);
             remove_ds_from_op(op_data, op_data->dat[k-1]);
        }
        
        //  for (i = 0 ; i < op_APD->n_dat ; i+=2)
       //    dist[i] = 0;
    }    
    //win_printf_OK("Before average");
    
     free(avg);
    
    calculate_APD_average();
      
    calculate_FCS_average();
    
    return D_O_K;
}    
 
 
 
int retreat_from_correlation_function(void)
{   
    pltreg * pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    int i,j,jj,k;
    O_p *op_avg = NULL;
    O_p *op_data = NULL;
    O_p *op_APD = NULL;
    d_s *ds_APD1 = NULL ; 
    d_s *ds_APD2 = NULL;
    float correlation_max;
    double corr_max;
    //int numb_to_remove;
     double *avg = NULL;
    
    if(updating_menu_state != 0)	return D_O_K;
    
    calculate_APD_average();
    
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
		return win_printf_OK("cannot find data");
		//Je fais un mauvais test initial
    if (find_source_specific_ds_in_op(pr->o_p[2], "ALV-1") == NULL) return win_printf_OK("This is not an ALV plot");
    
    op_avg = pr->o_p[0] ;
    op_APD = pr->o_p[1] ; 
    op_data = pr->o_p[2] ;
    
    ds_APD1 = find_source_specific_ds_in_op(pr->o_p[1], "APD1 average");
    ds_APD2 = find_source_specific_ds_in_op(pr->o_p[1], "APD2 average");
    
    //numb_to_remove = (op_APD->n_dat -1) / 2 ;
    
    //dist = (double *) calloc(op_APD->n_dat, sizeof(double));
   
   correlation_max=0.1;
   jj=100;
    
    win_scanf("Correlation function amplitude max %f?\n for large tau(1, 10 or 100 ms) %d?",&correlation_max, &jj);
    
    
    //win_scanf("Minimum count on APD %f",&average_min);
    
    
    //win_scanf("Maximum count on APD %f",&average_max);
    
    j=128;
    
    if (jj==1){j=87;}
    if (jj==10){j=113;}
    if (jj==100){j=140;}
    
    corr_max= (double) correlation_max;
    
    
   // win_printf("Maximum count on APD %g",avg_max);
    
    
    avg = (double *) calloc(op_data->dat[0]->nx, sizeof(double));
    
    //win_scanf("You want to remove how many datasets %d?",&numb_to_remove);
    
    //if (numb_to_remove >= (pr->o_p[1]->n_dat/2)) return win_printf_OK("You ask too much");
    for ( k = (op_APD->n_dat +1) / 2; k >0 ; k--)
    {
        //for (i = 0 ; i < op_APD->n_dat ; i+=2)
          //  {
            //        for (j = 0 ; j < op_APD->dat[0]->nx ; j++)
              //                 dist[i] += (double) op_APD->dat[i]->yd[j]*op_APD->dat[i]->yd[j] - ((i%2 == 0) ? ds_APD1->yd[j]*ds_APD1->yd[j] : ds_APD2->yd[j]*ds_APD2->yd[j]);
            //}    
        //To_Remove = gsl_stats_max_index (dist, 1 , op_APD->n_dat);
        //win_printf("To_Remove %d",To_Remove);
        
        

        for (i = j ; i < op_data->dat[0]->nx ; i++)
            {
                    
                               avg[i] = corr_max - (double) op_data->dat[k-1]->yd[i];
                             
            } 
        
                
        if ( gsl_stats_min(avg,1, op_data->dat[0]->nx)<0)
        {    
             
             remove_ds_from_op(op_APD, op_APD->dat[2*k-1]);
             remove_ds_from_op(op_APD, op_APD->dat[2*k-2]);
             remove_ds_from_op(op_data, op_data->dat[k-1]);
        }
        
        //  for (i = 0 ; i < op_APD->n_dat ; i+=2)
       //    dist[i] = 0;
    }    
    //win_printf_OK("Before average");
    
     free(avg);
    
    calculate_APD_average();
      
    calculate_FCS_average();
    
    return D_O_K;
}    
 



int APD_signal_time(void)
{   
    pltreg * pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    int i,j;
    O_p *op_avg = NULL;
    O_p *op_data = NULL;
    O_p *op_APD = NULL;
    d_s *ds_APD1 = NULL ; 
    d_s *ds_APD2 = NULL;
    d_s *ds1 = NULL ;
    d_s *ds2 = NULL ;
    double *avg = NULL;
    
    
    if(updating_menu_state != 0)	return D_O_K;
    
    calculate_APD_average();
    
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
		return win_printf_OK("cannot find data");
		//Je fais un mauvais test initial
    if (find_source_specific_ds_in_op(pr->o_p[2], "ALV-1") == NULL) return win_printf_OK("This is not an ALV plot");
    
    op_avg = pr->o_p[0] ;
    op_APD = pr->o_p[1] ; 
    op_data = pr->o_p[2] ;
    
    ds_APD1 = find_source_specific_ds_in_op(pr->o_p[1], "APD1 average");
    ds_APD2 = find_source_specific_ds_in_op(pr->o_p[1], "APD2 average");
    
    op = create_and_attach_one_plot(pr, (op_APD->n_dat-2)/2, (op_APD->n_dat-2)/2, 0);
  	    if (op == NULL) return D_O_K;
    ds1 = create_and_attach_one_ds (op,(op_APD->n_dat-2)/2,(op_APD->n_dat-2)/2, IS_FLOAT);
    if (ds1 == NULL) return D_O_K;
    ds2 = create_and_attach_one_ds (op,(op_APD->n_dat-2)/2,(op_APD->n_dat-2)/2, IS_FLOAT);
    if (ds2 == NULL) return D_O_K;
    
    remove_ds_from_op(op, op->dat[0]);
    
    
    
    avg = (double *) calloc(op_APD->dat[0]->nx, sizeof(double));
    
    //win_printf("op_APD->n_dat %d \n op_APD->dat[0]->nx %d",op_APD->n_dat,op_APD->dat[0]->nx);
    
    for ( j = 0 ; j < (op_APD->n_dat-2); j++)
    {
        for (i = 0 ; i < op_APD->dat[0]->nx ; i++)
            {
                    
                               avg[i] = (double) op_APD->dat[j]->yd[i];
            } 
        if ( j % 2 == 0) 
        {
             ds1->xd[j/2] = j/2;   
             ds1->yd[j/2] = (float) gsl_stats_mean (avg, 1 , op_APD->dat[0]->nx);
             //draw_bubble(screen,0,700,250," j = %d ds1->xd[j/2] %f ds1->yd[j/2] %f ",j, ds1->xd[j/2],ds1->yd[j/2]);
        }
        else
        {
             ds2->xd[j/2] = j/2;   
             ds2->yd[j/2] = (float) gsl_stats_mean (avg, 1 , op_APD->dat[0]->nx);
             //draw_bubble(screen,0,700,450,"j %d ds2->xd[j/2] %f ds2->yd[j/2] %f ",j,ds2->xd[j/2],ds2->yd[j/2]);
        }
        
    }
    free(avg);
    return D_O_K;
}    
 


int APD_signal_time_full(void)
{   
    pltreg * pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    int i,j;
    O_p *op_avg = NULL;
    O_p *op_data = NULL;
    O_p *op_APD = NULL;
    d_s *ds_APD1 = NULL ; 
    d_s *ds_APD2 = NULL;
    d_s *ds1 = NULL ;
    d_s *ds2 = NULL ;
    double *avg = NULL;
    double run_duration;
    
    if(updating_menu_state != 0)	return D_O_K;
    
    calculate_APD_average();
    
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
		return win_printf_OK("cannot find data");
		//Je fais un mauvais test initial
    if (find_source_specific_ds_in_op(pr->o_p[2], "ALV-1") == NULL) return win_printf_OK("This is not an ALV plot");
    
    op_avg = pr->o_p[0] ;
    op_APD = pr->o_p[1] ; 
    op_data = pr->o_p[2] ;
    
    ds_APD1 = find_source_specific_ds_in_op(pr->o_p[1], "APD1 average");
    ds_APD2 = find_source_specific_ds_in_op(pr->o_p[1], "APD2 average");
    
    op = create_and_attach_one_plot(pr, (op_APD->n_dat-2)/2*op_APD->dat[0]->nx, (op_APD->n_dat-2)/2*op_APD->dat[0]->nx, 0);
  	    if (op == NULL) return D_O_K;
    ds1 = create_and_attach_one_ds (op,(op_APD->n_dat-2)/2*op_APD->dat[0]->nx,(op_APD->n_dat-2)/2*op_APD->dat[0]->nx, IS_FLOAT);
    if (ds1 == NULL) return D_O_K;
    ds2 = create_and_attach_one_ds (op,(op_APD->n_dat-2)/2*op_APD->dat[0]->nx,(op_APD->n_dat-2)/2*op_APD->dat[0]->nx, IS_FLOAT);
    if (ds2 == NULL) return D_O_K;
    
    remove_ds_from_op(op, op->dat[0]);
    
    run_duration=3;
    
     win_scanf("Run duration (in seconds) %f?",&run_duration);
   
    
   // avg = (double *) calloc(op_APD->dat[0]->nx, sizeof(double));
    
    //win_printf("op_APD->n_dat %d \n op_APD->dat[0]->nx %d",op_APD->n_dat,op_APD->dat[0]->nx);
    
    for ( j = 0 ; j < (op_APD->n_dat-2); j++)
    {
        for (i = 0 ; i < op_APD->dat[0]->nx ; i++)
            {
                    
                          //     avg[i] = (double) op_APD->dat[j]->yd[i];
             
        if ( j % 2 == 0) 
        {
             ds1->xd[j/2*op_APD->dat[0]->nx+i] = j/2*run_duration+op_APD->dat[j]->xd[i];   
             ds1->yd[j/2*op_APD->dat[0]->nx+i] = (double) op_APD->dat[j]->yd[i];
             //draw_bubble(screen,0,700,250," j = %d ds1->xd[j/2] %f ds1->yd[j/2] %f ",j, ds1->xd[j/2],ds1->yd[j/2]);
        }
        else
        {
             ds2->xd[j/2*op_APD->dat[0]->nx+i] = j/2*run_duration+op_APD->dat[j]->xd[i];   
             ds2->yd[j/2*op_APD->dat[0]->nx+i] = (double) op_APD->dat[j]->yd[i];
             //draw_bubble(screen,0,700,450,"j %d ds2->xd[j/2] %f ds2->yd[j/2] %f ",j,ds2->xd[j/2],ds2->yd[j/2]);
        }
        }        
    }
   // free(avg);
    return D_O_K;
}    


int retrieve_ds_ALV_number_in_src(d_s *ds)
{
    char *source = NULL;
    
    
    if (ds == NULL) return win_printf_OK("ds NULL");
    
    source = ds->source;
    win_printf("source %s",source);
    if (source == NULL) return win_printf_OK("ds source NULL");
    
    if (strncmp("ALV-",source,4) != 0) return win_printf_OK("Not ALV source");
    
    return atoi(source+4);
}

//int retrieve_ds_APD1_number_in_src(d_s *ds)
//{
//    char *source = NULL;
//    
//    
//    if (ds == NULL) return win_printf_OK("ds NULL");
//    
//    source = ds->source;
//    win_printf("source %s",source);
//    if (source == NULL) return win_printf_OK("ds source NULL");
//    
//    if (strncmp("APD1-",source,5) != 0) return win_printf_OK("Not APD1 source");
//    
//    return atoi(source+5);
//}
//
//int retrieve_ds_APD2_number_in_src(d_s *ds)
//{
//    char *source = NULL;
//    
//    
//    if (ds == NULL) return win_printf_OK("ds NULL");
//    
//    source = ds->source;
//    win_printf("source %s",source);
//    if (source == NULL) return win_printf_OK("ds source NULL");
//    
//    if (strncmp("APD2-",source,5) != 0) return win_printf_OK("Not APD2 source");
//    
//    return atoi(source+5);
//}


//int test_number(void)
//{
//    
//    O_p *op_data = NULL ;
//	d_s *ds_data = NULL ;
//	pltreg *pr = NULL;
//    
//    
//    if(updating_menu_state != 0)	return D_O_K;
//    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op_data,&ds_data) != 3)
//		return win_printf_OK("cannot find data");
//	win_printf("op_data->dat[3] = %d",retrieve_ds_ALV_number_in_src(op_data->dat[3]));
//	return D_O_K;
//}	
    
int recalculate(void)
{
    
    O_p *op_avg = NULL , *op = NULL , *op_APD = NULL , *op_data = NULL;
	d_s *ds_avg = NULL , *ds = NULL;
	pltreg *pr = NULL;
    int i,j;
    double *data = NULL;
    
    
    
    if(updating_menu_state != 0)	return D_O_K;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("Test for ALV Flex built parser");
	}
	/* we first find the data that we need to transform */
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
		return win_printf_OK("cannot find data");

    ds_avg = find_source_specific_ds_in_op(pr->o_p[0], "ALV average");
    if (ds_avg == NULL) return win_printf_OK("This is not an ALV plot");
    
    if (find_source_specific_ds_in_op(pr->o_p[2], "ALV") == NULL) return win_printf_OK("This is not an ALV plot");
    
    if (find_source_specific_ds_in_op(pr->o_p[1], "APD1") == NULL) return win_printf_OK("This is not an ALV plot");
    
    op_avg = pr->o_p[0] ;
    op_APD = pr->o_p[1] ; 
    op_data = pr->o_p[2] ;
    
    
    win_printf("op_data->n_dat %d",op_data->n_dat);
    
    data = (double *) calloc(op_data->n_dat, sizeof(double));
    
    for ( i = 0 ; i < op_data->dat[0]->nx  ; i++)
    {
        for ( j = 0 ; j < op_data->n_dat  ; j++)
        {
            data[j] = (double) op_data->dat[j]->yd[i];
        }
        ds_avg->yd[i] = (float) gsl_stats_mean (data, 1 , op_data->n_dat);
   	    ds_avg->ye[i] = (float) gsl_stats_variance (data, 1 , op_data->n_dat);
   	}
   	free (data);
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}
//int remove_wrong_ds(void)
//{
//    float fraction = 2;
//    d_s *ALV_ds_bin = NULL;
//    
//    O_p *op = NULL;
//	d_s *dsi = NULL;
//	pltreg *pr = NULL;
//    
//    
//    int *NumbALVds = NULL, *NumbALVdsToRemove = NULL;
//    int i,j,k;
//    double *data = NULL;
//    
//    
//    
//    if(updating_menu_state != 0)	return D_O_K;
//
//	/* display routine action if SHIFT is pressed */
//	if (key[KEY_LSHIFT])
//	{
//		return win_printf_OK("Test for ALV Flex built parser");
//	}
//	/* we first find the data that we need to transform */
//	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
//		return win_printf_OK("cannot find data");
//
//    ALV_ds_bin = find_source_specific_ds_in_op(op, "ALV average");
//    if (ALV_ds_bin == NULL) return win_printf_OK("This is not an ALV plot");
//    
//    //win_printf("ALV_ds_bin %f",ALV_ds_bin->yd[0]);
//    //ALV_ds_bin = op-
//    //ds_ALV = (d_s **) calloc(op->n_dat - 1, sizeof(d_s *));
//    NumbALVds = (int *) calloc(op->n_dat - 1, sizeof(int)); 
//    NumbALVdsToRemove = (int *) calloc(op->n_dat - 1, sizeof(int)); 
//    
//    for ( i = 1 , k = 0 ; i < op->n_dat ; i++)
//    {
//      if (strcmp ( op->dat[i]->source, "ALV" ) == 0)
//      {
//          
//          NumbALVds[k] = i;
//          //win_printf(" i = %d \n NumbALVds[%d] %d",i,k,NumbALVds[k]);
//          k++;
//          
//                    
//      }    
//    }    
//    
//    
//    i = win_scanf("You want to remove data with point out of what fraction of sigma %f?",&fraction);
//    
//    if (i == CANCEL) return D_O_K;
//    
//    for (i = 0 , k = 0 ; i < op->n_dat -1 && NumbALVds[i] != 0 ; i++)
//    {
//        win_printf("i = %d\n op->dat[NumbALVds[i]]->nx %d\n NumbALVds[i]%d",i,op->dat[NumbALVds[i]]->nx,NumbALVds[i]);
//        for (j = 0 ; j <  op->dat[NumbALVds[i]]->nx ; j++)
//            { 
//      //          if (i == 4) win_printf("%f - %f \n %f \n %f",op->dat[NumbALVds[i]]->yd[j],ALV_ds_bin->yd[j],op->dat[NumbALVds[i]]->yd[j] - ALV_ds_bin->yd[j],fraction * ALV_ds_bin->ye[j]);
//        //         if(i == 4 && j == 0) win_printf("%f \n %f",(op->dat[NumbALVds[i]]->yd[j] - ALV_ds_bin->yd[j]), (fraction * ALV_ds_bin->ye[j]));
//                if (fabs(op->dat[NumbALVds[i]]->yd[j] - ALV_ds_bin->yd[j]) > (fraction * ALV_ds_bin->ye[j])) 
//                {
//          //          win_printf(" i = %d NumbALVds[i] =%d \n NumbALVdsToRemove[%d] ",i,NumbALVds[i],k);
//                    NumbALVdsToRemove[k] = NumbALVds[i];
//                    win_printf(" i = %d \n j= %d NumbALVdsToRemove[%d] %d",i,j,k,NumbALVdsToRemove[k]);
//                    k++;
//                    break;
//                }
//            }
//    }                
//    
//    //win_printf("YOP");
//    for (i = 0  ; i < op->n_dat -1 ; i++)
//    {
//        if (NumbALVdsToRemove[i] > 0) remove_ds_from_op(op, op->dat[NumbALVdsToRemove[i]]);
//    }
//    //win_printf("YOP");
//    data = (double *) calloc(op->n_dat, sizeof(double));
//    for ( i = 0 ; i < op->dat[0]->nx  ; i++)
//    {
//        for ( j = 0 ; j < op->n_dat  ; j++)
//        {
//            data[j] = (double) op->dat[j + 1]->yd[i];
//        }
//        ALV_ds_bin->yd[i] = (float) gsl_stats_mean (data, 1 , op->n_dat-1);
//   	    ALV_ds_bin->ye[i] = (op->n_dat -1) *(float) gsl_stats_variance (data, 1 , op->n_dat-1);
//   	}
//    refresh_plot(pr, UNCHANGED);
//    return D_O_K;
//}
    

int load_ALV_file(void)
{
    
    
    O_p *op = NULL, *op_avg = NULL , *op_data = NULL , *op_APD = NULL;
	d_s *dsi = NULL , *ds_avg = NULL;
	pltreg *pr = NULL;
	int FileNumber = 0;
	int NumberOfFile = 0;
	int i = 0 ;
	int first = 0;
    extern FILE *ALV_parserin;
   
    char fullfile[512],FileName[512],common_frame_name[512],filenumber[512];
    char file[512],path[512],fullfilename[512];
    pltreg *pr_new = NULL;
    int maxfilenumber = 199;
  
    
    
    
    if(updating_menu_state != 0)	return D_O_K;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("Test for ALV Flex built parser");
	}
	/* we first find the data that we need to transform */
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
		return win_printf_OK("cannot find data");
    
   	    
   	    switch_allegro_font(1);
	    i = file_select_ex("Load first File ASC (*.ASC)", fullfile, "ASC", 512, 0, 0);
	    switch_allegro_font(0);
	    if (i == 0) return D_O_K;
	    
	    win_scanf("Max file number?%d",&maxfilenumber);

	    pr_new = create_and_register_new_plot_project(0,   32,  900,  668);
  	    if (pr == NULL)  return D_O_K;
  	    op_avg = create_and_attach_one_plot(pr_new, 8192, 8192, 0);
  	    if (op_avg == NULL) return D_O_K;
  	    op_APD = create_and_attach_one_plot(pr_new, 8192, 8192, 0);
  	    if (op_APD == NULL) return D_O_K;
  	    op_data = create_and_attach_one_plot(pr_new, 8192, 8192, 0);
    	if (op_data == NULL) return D_O_K;
        
        remove_data (pr_new, IS_ONE_PLOT, (void*)(pr_new->o_p[0]));
	    
	    
	    
        extract_file_name(file, 256, fullfile);
		extract_file_path(path, 512, fullfile);
		strcat(path,"\\");
		
        common_frame_name[0] = 0;
        strncat(common_frame_name, file, strlen(file)-8);
        //win_printf("common_frame_name %s \n file %s \n path %s",common_frame_name,file,path);
        strcpy(fullfilename,path);
        strcat(fullfilename,common_frame_name);
	    
	    strncat(filenumber,fullfile+strlen(fullfile)-8,4);
	    //sprintf(FileNumber,"%4d",filenumber);
	    FileNumber = atoi(filenumber);
	    
	 
//		
//		sprintf(f_in,"%s%s",path,file);

	    //win_printf("fullfilename %s \n FileNumber %d\n common_frame_name %s\n filenumber %s",backslash_to_slash(fullfilename),FileNumber,common_frame_name,filenumber);
	 for (NumberOfFile = FileNumber; NumberOfFile < maxfilenumber ; NumberOfFile++)
	 {
           strcpy(FileName,fullfilename);
           sprintf(filenumber,"%d",NumberOfFile);
           for ( i = strlen(filenumber)+1 ; i < 5 ; i++) strcat(FileName,"0");
           strcat(FileName,filenumber);
           strcat(FileName,".asc");
           //win_printf("FileName %s",backslash_to_slash(FileName));
           ALV_parserin = fopen(FileName,"r");
           if (ALV_parserin == NULL) break;//return win_printf("cannot open file");
           ALV_parserlex( op_data , op_APD , FileName, first);
           first ++;
           fclose(ALV_parserin);
    }
    
    remove_ds_from_op(op_data, op_data->dat[0]);
    remove_ds_from_op(op_APD, op_APD->dat[0]);
    
    ds_avg = create_and_attach_one_ds (op_avg, op_data->dat[0]->nx  , op_data->dat[0]->nx , IS_FLOAT);
    set_ds_source(ds_avg, "ALV average");
        //win_printf(" op->dat[0]->nx %d \n op->dat[1]->nx %d",op->dat[0]->nx,op->dat[1]->nx);                                
	remove_ds_from_op(op_avg, op_avg->dat[0]);	
	calculate_FCS_average();
   
   	
   set_plot_title(op_avg, "Average Single cross correlation") ;	
   set_plot_x_title(op_avg, "Time (ms)");
   set_plot_y_title(op_avg, "FCS");
   set_plot_x_log(op_avg);
   
   
   
   set_plot_title(op_data, "Individual Scan cross correlation") ;	
   set_plot_x_title(op_data, "Time (ms)");
   set_plot_y_title(op_data, "FCS");
   set_plot_x_log(op_data);
   
   set_plot_title(op_APD, "APD Signal") ;	
   set_plot_x_title(op_APD, "Time (s)");
   set_plot_y_title(op_APD, "Intensity");
    
    refresh_plot(pr_new, UNCHANGED);
    
    
    
    
    return D_O_K;
    
    
    
    
}



MENU *ALV_menu(void)
{
	static MENU mn[32];
	extern int fit_gsl_FCS_func(void);
	extern int  simul_FCS(void);
	if (mn[0].text != NULL)	return mn;

	
   //add_item_to_menu(mn, "Avg error and mean ", compute_avcor_error_and_mean,   NULL,       0, NULL  );
//   	
//   add_item_to_menu(mn, "grab avcor at a time ", grab_avcor_at_a_time,   NULL,       0, NULL  );
//   add_item_to_menu(mn, "load avcor movie ", load_avcor_movie,   NULL,       0, NULL  );
//       add_item_to_menu(mn, "ALV file ", parse_file,   NULL,       0, NULL  );
       add_item_to_menu(mn,"Fit FCS diffusion",    fit_gsl_FCS_func, NULL ,  0, NULL  ); 
       add_item_to_menu(mn,"Simulate FCS",    simul_FCS, NULL ,  0, NULL  );
       //add_item_to_menu(mn,"Retreat ALV",    remove_wrong_ds, NULL ,  0, NULL  );
//       add_item_to_menu(mn,"Recalculate ALV",    recalculate, NULL ,  0, NULL  );
       add_item_to_menu(mn,"Load ALV file",    load_ALV_file, NULL ,  0, NULL  );
//       add_item_to_menu(mn,"Test_number",    test_number, NULL ,  0, NULL  );
       add_item_to_menu(mn,"Retreat from APD",    retreat_from_APD_signal, NULL ,  0, NULL  );
        add_item_to_menu(mn,"Select runs with APD1 count within particular range",    retreat_from_APD_signal2_1, NULL ,  0, NULL  );
add_item_to_menu(mn,"Select runs with APD2 count within particular range",    retreat_from_APD_signal2_2, NULL ,  0, NULL  );
 add_item_to_menu(mn,"Retreat from correlation function",    retreat_from_correlation_function, NULL ,  0, NULL  );
      add_item_to_menu(mn,"AVG APD",    calculate_APD_average, NULL ,  0, NULL  );
      add_item_to_menu(mn,"APD function of time",    APD_signal_time, NULL ,  0, NULL  );
      add_item_to_menu(mn,"APD function of time full",    APD_signal_time_full, NULL ,  0, NULL  );
       
       
       
	
     
   return mn;
}


// main program
int FCSALV_main(int argc, char* argv[])
{
  
 
    add_plot_treat_menu_item("ALV Treatement", NULL, ALV_menu() , 0, NULL);
    
    broadcast_dialog_message(MSG_DRAW,0);
	
    
  return 0;
}
# endif    


//int		avcor_main(int argc, char **argv)
//{
//	load_avcor_movie(CR);
//	return 0;
//}
//
//





