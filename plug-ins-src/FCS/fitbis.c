/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _STAT_C_
#define _STAT_C_

# include "allegro.h"
# include "xvin.h"
# include "stat.h"
# include "nrutil.h"
# include "float.h"
/* fit exponential */

double Y_equal_a0_plus_a1_exp_moins_lt( double t, double a0, double a1, double l)
{
	double tmp;
	
	tmp = -l * t;
	if (tmp > FLT_MAX_EXP)
		tmp = FLT_MAX;
	else if (tmp < FLT_MIN_EXP)
		tmp = FLT_MIN;
	else	tmp = exp(tmp);
	tmp *= a1;
	tmp += a0;
	return (tmp);
}


void Y_equal_a0_plus_a1_exp_moins_lt_func(float t, float *a, float *y, float *dyda, int ma)
{
	double  a0, a1, l, dyda0, dyda1, dydl;
	double  tmp;


	a0 = a[1];
	a1 = a[2];
	l = a[3];
	*y = Y_equal_a0_plus_a1_exp_moins_lt( t, a0, a1, l);
	tmp += a0;
	tmp = -l * t;
	if (tmp > FLT_MAX_EXP)
		tmp = FLT_MAX;
	else if (tmp < FLT_MIN_EXP)
		tmp = FLT_MIN;
	else	tmp = exp(tmp);
	dyda0 = 1;
	dyda1 = tmp;
	dydl = -tmp*a1*t;
	dyda[1] = dyda0;
	dyda[2] = dyda1;	
	dyda[3] = dydl;	
}




int	do_fit_Y_equal_a0_plus_a1_exp_moins_lt(void)
{
	register int i;
	char st[2048];
	pltreg *pr;
	O_p *op;
	d_s *dsi, *dsd;
	float x, xmin, xmax, tmp;
	int nf = 1024;
	static float error = .1;
	static float a0 = 0, a1 = 1;	
	static float l = .02;	
	static int flag = 1, ia0 = 1, ia1 = 1, il = 1, nder = 0;
	float a[3], chisq, chisq0, alamda, *sig, **covar, **alpha; 
	int ia[3];	
	float gammq(float a, float x);
	
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine fits an exponential to a data set\n"
		"using a Levenberg Marquart algorithm\n"
		"\nf(t) = a_0 + a_1 exp(-lt)\n"
		"the parameter a_0, a_1, l may either be imposed or fitted\n"
		"you must provide good guessed values of these parameters\n"
		"otherwise this routine is lickly to crash the program\n"
		"special thanks to Numerical Reciepe...");
	}	
	
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)	return 1;
	for (i = 0, xmin = xmax = dsi->xd[0]; i < dsi->nx; i++)
	{
		xmin = (dsi->xd[i] < xmin) ? dsi->xd[i] : xmin;
		xmax = (dsi->xd[i] > xmax) ? dsi->xd[i] : xmax;
	}	
	i = win_scanf("Fit a_0, a_1, l, in f(t) = a_0 + a_1 exp(-lt) \n \n"
	"possible a_0 value %fa_1  value %fl value %f error type, const -> 0 "
	"relative 1 other data set 2%derror %ffit a_0 (yes 1, imposed 0) %d"
	"fit a_1 (yes 1, imposed 0) %dfit l (yes 1, imposed 0) %d",&a0,&a1,&l,
	&flag,&error,&ia0, &ia1, &il);
	if (i == CANCEL)	return OFF;

	ia[0] = ia0;
	ia[1] = ia1;
	ia[2] = il;
	a[0] = (a0 - op->ay)/op->dy;
	a[1] = a1/op->dy;
	a[2] = l*op->dx;
	covar = matrix(1, 3, 1, 3);
	alpha = matrix(1, 3, 1, 3);
	sig = vector(1, dsi->nx);
	for (i = 1; i <= dsi->nx; i++)
		sig[i] = (flag) ? dsi->yd[i-1] * error : error/op->dy;
	if (flag == 2)
	{
		nder = op->cur_dat+1;
		i = win_scanf("enter index of error data set %d",&nder);
		if (i == CANCEL)	return OFF;
		if (op->dat[nder]->nx != dsi->nx)	
			return win_printf_OK("your data sets differs in size!");
		for (i = 1; i <= dsi->nx; i++)
			sig[i] =  op->dat[nder]->yd[i-1];
	}
	alamda = -1;
	mrqmin(dsi->xd-1, dsi->yd-1, sig, dsi->nx, a-1, ia-1,3, covar, alpha, &chisq,
	 	Y_equal_a0_plus_a1_exp_moins_lt_func, &alamda);


	chisq0 = chisq;
	mrqmin(dsi->xd-1, dsi->yd-1, sig, dsi->nx, a-1, ia-1,3, covar, alpha, &chisq, 
		Y_equal_a0_plus_a1_exp_moins_lt_func, &alamda);

	for (i = 0 ; (chisq0 - chisq <= 0) || (chisq0 - chisq > 1e-2); i++)
	{
		chisq0 = chisq;
		mrqmin(dsi->xd-1, dsi->yd-1, sig, dsi->nx, a-1, ia-1,3, covar, alpha,
			 &chisq, Y_equal_a0_plus_a1_exp_moins_lt_func, &alamda);

		if (i >= 10000) break;
		if (alamda > 1e12) break;
	}
	
	alamda = 0;
	mrqmin(dsi->xd-1, dsi->yd-1, sig, dsi->nx, a-1, ia-1,3, covar, alpha, &chisq,
	 Y_equal_a0_plus_a1_exp_moins_lt_func, &alamda);	
/*	
	i = win_printf("%d iter a_0 = %g,\n a_1 = %g l = %g the \\chi^2  is %g\n"
	" covar \\xi \\xi %g \\xi L %g\nL \\xi %g LL %g\ncurvature \\xi \\xi %g"
	" \\xi L %g\n L \\xi %g LL %g",i,op->dy*a[0],op->dy*a[1],a[2]/op->dx,chisq,
	covar[1][1],covar[1][2],covar[2][1],covar[2][2],alpha[1][1],alpha[1][2],
	alpha[2][1],alpha[2][2]);
*/	
	a0 = a[0];
	a1 = a[1];
	l = a[2];
	if (i == CANCEL)	return OFF;
	if (dsi->nx > 256)	nf = dsi->nx;
	if ((dsd = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
		return win_printf_OK("cannot create data set");
	if (dsi->nx > 256)	
	{
		for (i = 0; i < dsd->nx && i < dsi->nx; i++)	
		{
			x = dsi->xd[i];
			dsd->xd[i] = dsi->xd[i];
			dsd->yd[i] = Y_equal_a0_plus_a1_exp_moins_lt( x, a0, a1, l);
		}
	}
	else
	{	
		tmp = xmin - 0.1*(xmax - xmin);
		xmax = xmax + 0.1*(xmax - xmin);
		xmin = tmp;
		for (i = 0; i < dsd->nx; i++)	
		{
			x = xmin + i*(xmax-xmin)/dsd->nx;
			dsd->xd[i] = x;
			dsd->yd[i] = Y_equal_a0_plus_a1_exp_moins_lt( x, a0, a1, l);
		}		
	}

	
	sprintf(st,"\\fbox{\\pt8\\stack{{\\sl f(t) = a_0 + a_1 exp(-lt)}"
	"{a_0 = %g \\stack{+-} %g}{a1 = %g \\stack{+-} %g}{l = %g \\stack{+-} %g}"
	"{\\tau  = %g %s \\stack{+-} %g %%}{\\chi^2 = %g n = %d}{p = %g}}}",
	op->ay+(op->dy*a0),op->dy*sqrt(covar[1][1]),op->dy*a1,
	op->dy*sqrt(covar[2][2]),l/op->dx,sqrt(covar[3][3])/op->dx,op->dx/l,
	(op->x_unit) ? op->x_unit:" ",100*sqrt(covar[3][3])/l,chisq,dsi->nx-3,
	gammq((float)(dsi->nx-3)/2, chisq/2));
	push_plot_label(op, op->x_lo + (op->x_hi - op->x_lo)/4, 
		op->y_hi - (op->y_hi - op->y_lo)/4, st, USR_COORD);
	a0 = op->ay+(op->dy*a0);
	a1 = a1*op->dy;
	l = l/op->dx;
	free_vector(sig, 1, dsi->nx);	
	free_matrix(alpha, 1, 3, 1, 3);
	free_matrix(covar, 1, 3, 1, 3);
	return refresh_plot(pr,pr->cur_op);			
}

/*
 *	O_p *op, the plot
 *  d_s *dsi, the data set to fit
 *  d_s *dsir, the error data set 
 *	double *a0, the baseline of the exponential
 *  int ia0, a flag to fit (1) or to impose a0
 *  double *a1, the amplitude of the exponential
 *  int ia1, a flag to fit (1) or to impose (0) a1
 *  double *l, the exponential decay factor
 *  int il, a flag to fit (1) or to impose (0) l  
 *  double *chisq the" chi^2 of the fit
*/
int	fit_Y_equal_a0_plus_a1_exp_moins_lt_to_op(O_p *op, d_s *dsi, d_s *dsir, 
	double *a0, int ia0, double *a1, int ia1, double *l, int il, 
	double *chisq, int verbose)
{
	register int i;
	float a[3], fchisq, chisq0, alamda, *sig, **covar, **alpha; 
	int ia[3];	
	float gammq(float a, float x);
	

	ia[0] = ia0;
	ia[1] = ia1;
	ia[2] = il;
	a[0] = ((*a0) - op->ay)/op->dy;
	a[1] = (*a1)/op->dy;
	a[2] = (*l)*op->dx;
	covar = matrix(1, 3, 1, 3);
	alpha = matrix(1, 3, 1, 3);
	sig = vector(1, dsi->nx);
	if (dsir->nx != dsi->nx)	
		return win_printf_OK("your data sets differs in size!");
	for (i = 1; i <= dsir->nx; i++)
			sig[i] =  dsir->yd[i-1];

	alamda = -1;
	mrqmin(dsi->xd-1, dsi->yd-1, sig, dsi->nx, a-1, ia-1,3, covar, alpha, &fchisq,
		Y_equal_a0_plus_a1_exp_moins_lt_func, &alamda);


	chisq0 = fchisq;
	mrqmin(dsi->xd-1, dsi->yd-1, sig, dsi->nx, a-1, ia-1,3, covar, alpha, &fchisq,
		Y_equal_a0_plus_a1_exp_moins_lt_func, &alamda);

	for (i = 0 ; (chisq0 - fchisq <= 0) || (chisq0 - fchisq > 1e-2); i++)
	{
		chisq0 = fchisq;
		mrqmin(dsi->xd-1, dsi->yd-1, sig, dsi->nx, a-1, ia-1,3, covar, alpha, 
			&fchisq, Y_equal_a0_plus_a1_exp_moins_lt_func, &alamda);

		if (i >= 10000) break;
		if (alamda > 1e12) break;
	}
	
	alamda = 0;
	mrqmin(dsi->xd-1, dsi->yd-1, sig, dsi->nx, a-1, ia-1,3, covar, alpha, &fchisq,
		Y_equal_a0_plus_a1_exp_moins_lt_func, &alamda);	
	
	if (verbose)
	{
		i = win_printf("%d iter a_0 = %g,\n a_1 = %g l = %g the \\chi^2  is %g\n"
			" covar \\xi \\xi %g \\xi L %g\nL \\xi %g LL %g\n"
			"curvature \\xi \\xi %g \\xi L %g\n L \\xi %g LL %g",i,op->dy*a[0],
			op->dy*a[1],a[2]/op->dx,fchisq,covar[1][1],covar[1][2],covar[2][1],
			covar[2][2],alpha[1][1],alpha[1][2],alpha[2][1],alpha[2][2]);
		if (i == CANCEL)	return OFF;
	}		
	*a0 = a[0];
	*a1 = a[1];
	*l = a[2];
	*a0 = op->ay+(op->dy*(*a0));
	*a1 = (*a1)*op->dy;
	*l = (*l)/op->dx;
	*chisq = fchisq;
	free_vector(sig, 1, dsi->nx);	
	free_matrix(alpha, 1, 3, 1, 3);
	free_matrix(covar, 1, 3, 1, 3);
	return 0;			
}


MENU *stat_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;

	
	add_item_to_menu(mn,"Histogram",do_histo_non_zero,NULL,0,NULL);
	add_item_to_menu(mn,"Construct error bars",draw_error_bars,NULL,0,NULL);
	add_item_to_menu(mn,"Extract error bars",do_extract_error,NULL,0,NULL);
	add_item_to_menu(mn,"Construct histo bars",draw_histo_bars,NULL,0,NULL);
	add_item_to_menu(mn,"\0",	NULL,	NULL,   0, NULL );
	add_item_to_menu(mn,"Error of correlated data",	do_sigma_stat,	NULL,   0, NULL );
	add_item_to_menu(mn,"fit of correlated data",	do_fit_sigma_stat,	NULL,   0, NULL );
	add_item_to_menu(mn,"\0",	NULL,	NULL,   0, NULL );
	add_item_to_menu(mn,"Fit exponential",do_fit_Y_equal_a0_plus_a1_exp_moins_lt,
		NULL,0,NULL);
	add_item_to_menu(mn,"\0",	NULL,	NULL,   0, NULL );	
	add_item_to_menu(mn,"Fit gaussian",do_fit_Y_equal_a_gaussian,NULL,0,NULL);	
	add_item_to_menu(mn,"Boostrap gaussian",do_fit_Y_equal_a_gaussian_boostrap,
		NULL,0,NULL);	
	

	
			
	
	
	return mn;
}

int	stat_main(int argc, char **argv)
{
	add_plot_treat_menu_item ( "statistics", NULL, stat_plot_menu(), 0, NULL);
	return D_O_K;
}
#endif
