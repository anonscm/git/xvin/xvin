/*
*    Plug-in program for plot treatement in Xvin.
*
*    V. Croquette JF Allemand
*/
#ifndef _fitfcs_C_
#define _fitfcs_C_

# include "allegro.h"
# include "xvin.h"

# include <float.h>
# include "../nrutil/nrutil.h"

int do_simulate_diffusion_3D_triplet_autocorrelation(void);
int	do_full_fit_fcs_window(void);
double fcs(float t, float a0, float a1,float a2,float a3,float a4,float a5);
void full_fcs_func(float t, float *a, float *y, float *dyda, int ma);



int	do_full_fit_fcs_window(void)
{
	int i,j;
	char st[256];
	float x/*, xmin, xmax, tmp*/;
	int nf = 0;
	pltreg *pr;
	O_p *op;
	d_s *dsi, *dsd = NULL, *dsitmp = NULL;
	static float  error = .1;
	static float a0 = 0, a1=0, a2 = 0, a3=1, a4=1, a5=0,tau_d=1;	/* starting parameters */
	static int ia0,ia1,ia2,ia5;
	static int ia3,ia4;
	/*static float n = 3;	*/
	static int flag = 1;
	float a[6], chisq, chisq0, alamda, *sig, **covar, **alpha; 
	int ia[6], nder = 1;	
	float gammq(float a, float x);
	
	
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)	return 1;
	
	nf=0;
	for (i=0;i<dsi->nx;i++)
	{
		
		if ( (dsi->xd[i] > op->x_lo) && (dsi->xd[i] < op->x_hi) && (dsi->yd[i] > op->y_lo)  &&  (dsi->yd[i] < op->y_hi)) nf++;
	}
	
	win_printf("Hello");
	
	if ((dsitmp = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
		return win_printf("cannot create data set");
	
	

	for (i=j=0;i<dsi->nx;i++)
	{	
		if ( (dsi->xd[i] > op->x_lo) && (dsi->xd[i] < op->x_hi) && (dsi->yd[i] > op->y_lo)  &&  (dsi->yd[i] < op->y_hi)) 
		{	
			dsitmp->xd[j]=dsi->xd[i];
			dsitmp->yd[j]=dsi->yd[i];
			j++;
		}
	}
		
	
	i = win_scanf("Fit data with g(t)=a_0 +a_1exp(-a_2t)+\\frac{a_3}{[(1+t/ \\tau_D)(1+a_4a_5t)^{\\frac{1}{2}]}} \n fit a_0 ? yes ->1 no -> 0) %d  possible a_0 %f  fit a_1 ?(yes ->1 no -> 0) %d  possible a_1 %f \n fit a_2 ?(yes ->1 no -> 0) %d  possible a_2 %f  fit a_3 ?(yes ->1 no -> 0) %d  possible a_3 %f",&ia0,&a0,&ia1,&a1,&ia2,&a2,&ia3,&a3);
	if (i == CANCEL)	return OFF;

	i=win_scanf("fit a_4 ?(yes ->1 no -> 0) %d  possible \\tau_D %f  fit a_5 ?(yes ->1 no -> 0) %d possible a_5 %f   error type  const -> 0 relative  error in data set ->2 %d error %f",&ia4,&tau_d,&ia5,&a5,&flag,&error);

	if (tau_d != 0)
		{a4 = 1/tau_d;}
	else 
		{	
			win_printf("attention � \\tau_D");
			return 0;
		}

	if (i == CANCEL)	return OFF;
	
	
	ia[0]=ia0;
	ia[1]=ia1;
	ia[2]=ia2;
	ia[3]=ia3;
	ia[4]=ia4;
	ia[5]=ia5;
	
	a[0]=a0;
	a[1]=a1;
	a[2]=a2;
	a[3]=a3;
	a[4]=a4;
	a[5]=a5;
	
	covar = matrix(1, 5, 1, 5);
	alpha = matrix(1, 5, 1, 5);
	
	
	sig = vector(1, dsitmp->nx);
	for (i = 1; i <= dsitmp->nx; i++)
		sig[i] = (flag) ? dsitmp->yd[i-1] * error : error;
	if (flag == 2)
	{
		i = win_scanf("enter index of error data set %d",&nder);
		if (i == CANCEL)	return OFF;
	
	
	nf=0;	
	
		for (i=0;i<dsi->nx;i++)
	{
		if ( (dsi->xd[i] > op->x_lo) && (dsi->xd[i] < op->x_hi) && (dsi->yd[i] > op->y_lo)  &&  (dsi->yd[i] < op->y_hi)) nf++;
	}
	
	if (nf != dsitmp->nx)		return win_printf("your data sets differs in size!");
	
 	i=1;	
	
  	for (j=0; j<(dsi->nx) ;j++)
	{	
		if ( (dsi->xd[j] > op->x_lo) && (dsi->xd[j] < op->x_hi) && (dsi->yd[j] > op->y_lo)  &&  (dsi->yd[j] < op->y_hi)) 
		{	
			sig[i]=op->dat[nder]->xd[j];
			i++;
		}
	}
	
	if ((i-1) != dsitmp->nx)
		return win_printf("your data sets differs in size!(2)");
	}
	
	win_printf("Hello");
	alamda = -1;
	
	
	
	mrqmin(dsitmp->xd-1, dsitmp->yd-1, sig, dsitmp->nx, a-1, ia-1,6, covar, alpha, &chisq, full_fcs_func, &alamda);

	
	
win_printf("Hello1");
	chisq0 = chisq;
	mrqmin(dsitmp->xd-1, dsitmp->yd-1, sig, dsitmp->nx, a-1, ia-1,6, covar, alpha, &chisq, full_fcs_func, &alamda);
win_printf("Hello2");
	
	for (i = 0 ; (chisq0 - chisq <= 0) || (chisq0 - chisq > 1e-2); i++)
	{
		chisq0 = chisq;
		mrqmin(dsitmp->xd-1, dsitmp->yd-1, sig, dsitmp->nx, a-1, ia-1,6, covar, alpha, &chisq, full_fcs_func, &alamda);

		if (i >= 10000) 
		{	win_printf("On revient bredouille mais on a beaucoup marche");
			break;
		}
		if (alamda > 1e12) 
		{	win_printf("On revient bredouille");
			break;
		};
	}
	win_printf("Hello3");
/*	alamda = 0;
	mrqmin(dsitmp->xd-1, dsitmp->yd-1, sig, dsitmp->nx, a-1, ia-1,6, covar, alpha, &chisq, full_fcs_func, &alamda);	
*/
	/*i = win_printf("%d iter a_0 = %g,\n a_1 = %g a_2 = %g,\n N = 1/a_0 %g,\n \\tau_D=1/a_4 = %g,\n a_5 = %g \n the \\chi^2  is %g \n \\frac{\\chi^2}{N_{points}} is %g covar a_0  %g covar a_1  %g \n covar a_2  %g covar a_3  %g covar a_4  %g covar a_5  %g",i,a[0],a[1],a[2],1/a[3],1/a[4],a[5],chisq,chisq/(dsitmp->nx),covar[1][1],covar[2][2],covar[3][3],covar[4][4],covar[5][5],covar[6][6]);
*/
	if (i == CANCEL)	return OFF;
	
	a0 = a[0];
	a1 = a[1];
	a2 = a[2];
	a3 = a[3];
	a4 = a[4];
	a5 = a[5];
	
	
	
	/*if (dsi->nx > 100)	nf = dsi->nx;*/
	if ((dsd = create_and_attach_one_ds(op, dsi->nx, dsi->nx, 0)) == NULL)
		return win_printf("cannot create data set");
	/*if (dsi->nx > 100)	
	{*/
		for (i = 0; i < dsd->nx /*&& i < dsi->nx*/; i++)	
		{
			x = dsi->xd[i];
			dsd->xd[i] = dsi->xd[i];
			dsd->yd[i] = fcs( x, a0,a1,a2,a3,a4,a5);
		}
	/*}
	else
	{	
		tmp = xmin - 0.1*(xmax - xmin);
		xmax = xmax + 0.1*(xmax - xmin);
		xmin = tmp;
		for (i = 0; i < dsd->nx; i++)	
		{
			x = xmin + i*(xmax-xmin)/dsd->nx;
			dsd->xd[i] = x;
			dsd->yd[i] = fcs(x, a0,a1,a2,a3,a4,a5);
		}		
	}*/
	
	sprintf(st,"\\fbox{\\pt8\\stack{{a_0 = %g }{ a_1 = %g }{a_2 = %g}{ N=1/a_3 = %g}{ \\tau_D =1/a_4 = %g}{ a_5 = %g }{ the \\chi^2  is %g}{the \\chi^2_R is %g}}} ",a[0],a[1],a[2],1/a[3],1/a[4],a[5],chisq,chisq/(dsitmp->nx));

	
	push_plot_label(op, op->x_lo + (op->x_hi - op->x_lo)/4, op->y_hi - (op->y_hi - op->y_lo)/4, st,USR_COORD);
	free_vector(sig, 1, dsitmp->nx);	
	free_matrix(alpha, 1, 5, 1, 5);
	free_matrix(covar, 1, 5, 1, 5);
	return refresh_plot(pr,pr->cur_op);			
}

void full_fcs_func(float t, float *a, float *y, float *dyda, int ma)
{
	float  a0,a1,a2,a3,a4,a5,dyda0,dyda1,dyda2,dyda3,dyda4,dyda5;


	a0=a[1];
	a1=a[2];
	a2=a[3];
	a3=a[4];
	a4=a[5];
	a5=a[6];
	
	*y = fcs( t, a0,a1,a2,a3,a4,a5);
	/* calculate the derivatives*/
	dyda0 = 1;
	dyda1=exp(-a2*t);
	dyda2=-t*a1*exp(-a2*t);
	dyda3=(float)(1)/(((float)(1)+a4*t)*pow(((float)(1)+a4*a5*t),0.5));
	dyda4=-t/(((float)(1)+a4*t)*((float)(1)+a4*t)*pow(((float)(1)+a4*a5*t),0.5))-0.5*a5*t/(((float)(1)+a4*t)*pow(((float)(1)+a4*a5*t),1.5));
	dyda5=-0.5*a4*t/(((float)(1)+a4*t)*pow(((float)(1)+a4*a5*t),1.5));
	
	dyda[1] = dyda0;
	dyda[2] = dyda1;	
	dyda[3] = dyda2;
	dyda[4] = dyda3;	
	dyda[5] = dyda4;
	dyda[6] = dyda5;	
	
	
	
	
}
double fcs(float t, float a0, float a1,float a2,float a3,float a4,float a5)
{
	double tmp;
	/* calculate the funct*/
	
	tmp=a0+a1*exp(-a2*t)+a3/(((float)(1)+a4*t)*pow(((float)(1)+a4*a5*t),0.5));
	
	
	return (tmp);
}



double Autocorrelation(double t, double n, double t_d)
{
	double tmp;
	
	if (t_d == 0)	 return win_printf("Mets des bretelles");
	tmp = 1 + (t/t_d);
	tmp *= n;
	if (tmp == 0)	 return win_printf("Mets des bretelles");
	tmp = ((double)1)/tmp; 
	return (tmp);
}

void Autocorrelation_func(float t, float *a, float *y, float *dyda, int ma)
{
	double  n, t_d, dydn, dydt_d;



	n = a[1];
	t_d = a[2];
	*y = Autocorrelation( t, n, t_d);
	if (n == 0)	 return (void)win_printf("Mets des bretelles");	
	dydn = -(*y)/n;
	if (t_d == 0)	 return (void)win_printf("Mets des bretelles");
	dydt_d = n*t/(t_d*t_d);
	if (*y == 0)	 return (void)win_printf("Mets des bretelles");
	dydt_d *= ((*y)*(*y));
	dyda[1] = dydn;
	dyda[2] = dydt_d;	
}
    

int	do_fit_gdet_fcs(void)
{
	int i;
	char st[256];
	pltreg *pr;
	O_p *op;
	d_s *dsi, *dsd;
	float x, xmin, xmax, tmp;
	int nf = 1024;
	static float error = .1;
	static float n = 0, t_d = 1;	
	static int flag = 1, in = 1, it_d = 1;
	float a[3], chisq, chisq0, alamda, *sig, **covar, **alpha; 
	int ia[2], nder;	
	float gammq(float a, float x);
	
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)	return 1;
	for (i = 0, xmin = xmax = dsi->xd[0]; i < dsi->nx; i++)
	{
		xmin = (dsi->xd[i] < xmin) ? dsi->xd[i] : xmin;
		xmax = (dsi->xd[i] > xmax) ? dsi->xd[i] : xmax;
	}	
	i = win_scanf("Fit n, \\tau_d, in g(t) = \\frac{1}{n(1+\\tau/\\tau_d)} \n \npossible n value %f\\tau_d  value %f error type, const -> 0 relative 1 other data set 2%derror %ffit n (yes 1, imposed 0) %dfit \\tau_d (yes 1, imposed 0) %d",&n,&t_d,&flag,&error,&in, &it_d);
	if (i == CANCEL)	return OFF;

	ia[0] = in;
	ia[1] = it_d;
	a[0] = n;
	a[1] = t_d;
	covar = matrix(1, 2, 1, 2);
	alpha = matrix(1, 2, 1, 2);
	sig = vector(1, dsi->nx);
	for (i = 1; i <= dsi->nx; i++)
		sig[i] = (flag) ? dsi->yd[i-1] * error : error;
	if (flag == 2)
	{
		i = win_scanf("enter index of error data set %d",&nder);
		if (i == CANCEL)	return OFF;
		if (op->dat[nder]->nx != dsi->nx)	
			return win_printf("your data sets differs in size!");
		for (i = 1; i <= dsi->nx; i++)
			sig[i] =  op->dat[nder]->yd[i-1];
	}
	alamda = -1;
	mrqmin(dsi->xd-1, dsi->yd-1, sig, dsi->nx, a-1, ia-1,2, covar, alpha, &chisq, Autocorrelation_func, &alamda);


	chisq0 = chisq;
	mrqmin(dsi->xd-1, dsi->yd-1, sig, dsi->nx, a-1, ia-1,2, covar, alpha, &chisq, Autocorrelation_func, &alamda);

	for (i = 0 ; (chisq0 - chisq <= 0) || (chisq0 - chisq > 1e-2); i++)
	{
		chisq0 = chisq;
		mrqmin(dsi->xd-1, dsi->yd-1, sig, dsi->nx, a-1, ia-1,2, covar, alpha, &chisq, Autocorrelation_func, &alamda);

		if (i >= 10000) break;
		if (alamda > 1e12) break;
	}
	
	alamda = 0;
	mrqmin(dsi->xd-1, dsi->yd-1, sig, dsi->nx, a-1, ia-1,2, covar, alpha, &chisq, Autocorrelation_func, &alamda);	
	i = win_printf("%d iter n = %g,\n \\tau_d = %g the \\chi^2  is %g\n covar \\xi \\xi %g \\xi L %g\nL \\xi %g LL %g\ncurvature \\xi \\xi %g \\xi L %g\n L \\xi %g LL %g",i,a[0],a[1],chisq,covar[1][1],covar[1][2],covar[2][1],covar[2][2],alpha[1][1],alpha[1][2],alpha[2][1],alpha[2][2]);
	n = a[0];
	t_d = a[1];
	if (i == CANCEL)	return OFF;
	if (dsi->nx > 100)	nf = dsi->nx;
	if ((dsd = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
		return win_printf("cannot create data set");
	if (dsi->nx > 100)	
	{
		for (i = 0; i < dsd->nx && i < dsi->nx; i++)	
		{
			x = dsi->xd[i];
			dsd->xd[i] = dsi->xd[i];
			dsd->yd[i] = Autocorrelation( x, n, t_d);
		}
	}
	else
	{	
		tmp = xmin - 0.1*(xmax - xmin);
		xmax = xmax + 0.1*(xmax - xmin);
		xmin = tmp;
		for (i = 0; i < dsd->nx; i++)	
		{
			x = xmin + i*(xmax-xmin)/dsd->nx;
			dsd->xd[i] = x;
			dsd->yd[i] = Autocorrelation( x, n, t_d);
		}		
	}

	
	sprintf(st,"\\fbox{\\pt8\\stack{{\\sl g(t) = \\frac{1}{n(1+\\tau/\\tau_d)}}{n =  %g  +/- %g}{t_d = %g  +/ %g}{\\chi^2 = %g n = %d}{p = %g}}}",n,sqrt(covar[1][1]),t_d,sqrt(covar[2][2]),chisq,dsi->nx-2,gammq((float)(dsi->nx-2)/2, chisq/2));
	push_plot_label(op, op->x_lo + (op->x_hi - op->x_lo)/4, op->y_hi - (op->y_hi - op->y_lo)/4, st,USR_COORD);
	free_vector(sig, 1, dsi->nx);	
	free_matrix(alpha, 1, 2, 1, 2);
	free_matrix(covar, 1, 2, 1, 2);
	return refresh_plot(pr,pr->cur_op);			
}



int do_simulate_diffusion_autocorrelation(void)
{	int i;
	pltreg *pr;
	O_p *op_sim,*op;
	d_s *dsi;
	static float t_d=1, n=1;
	static int nf = 1024;


	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)	return 1;
	
	win_scanf("Produce g(t) with n=%f and \\tau_d=%f with %d points",&n,&t_d,&nf);
	
	if ((op_sim = create_and_attach_one_plot(pr,nf,nf,0)) == NULL)
		return win_printf("cannot create plot");
	
	
	for (i = 0; i < op_sim->dat[0]->nx; i++)	
	{
		op_sim->dat[0]->xd[i] = i;
		op_sim->dat[0]->yd[i] = (float)1/(n*(1+((float)i/t_d)));
	}
	return refresh_plot(pr,pr->n_op-1);		
}



int do_simulate_diffusion_3D_triplet_autocorrelation(void)
{	int i;
	pltreg *pr;
	O_p *op,*op_sim;
	d_s *dsi;
	float j=.000125;
	static float a_1,a_2,a_3,a_4,a_5,a_0,r=1;
	static int nf = 1024,n=1024;


	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)	return 1;
	
	i=win_scanf("Produce g(t)=a_0+a_1exp(-a_2t)+\\frac{a_3}{[(1+a_4t)(1+a_4a_5)t)^{.5}]}\n  with a_0=(0) normally %f  a_1=%f a_2=%f (0) if no triplet a_3=%f \\ a_4=%f a_5=%f (0) if 2D \\ with n%d points",&a_0,&a_1,&a_2,&a_3,&a_4,&a_5,&n);

	if (i == CANCEL)	return OFF;

	r=pow(100000/j,1/(float)(n));
	if ((op_sim = create_and_attach_one_plot(pr,n,n,0)) == NULL)
		return win_printf("cannot create plot");
	j/=r;
	
	for (i = 0; i < op_sim->dat[0]->nx; i++)	
	{	
		j*=r;
		op_sim->dat[0]->xd[i] = j;
		op_sim->dat[0]->yd[i] = a_0+a_1*exp(-(a_2*j))+(float)a_3/((1+((float)j*a_4)))  /(sqrt(1+a_4*a_5*(float)j));
	
	}
	return refresh_plot(pr,pr->n_op-1);		

}


MENU *fit_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;

	
	add_item_to_menu(mn,"Fit g_0(t)",do_fit_gdet_fcs,	NULL,   0, NULL );	
	add_item_to_menu(mn,"Sim. g3D+triplet",do_simulate_diffusion_3D_triplet_autocorrelation,NULL,0,NULL);	
	add_item_to_menu(mn,"do full fit",do_full_fit_fcs_window,NULL,0,NULL);	
	
	return mn;
}

int	fitfcs_main(int argc, char **argv)
{
	add_plot_treat_menu_item ( "Fit FCS", NULL, fit_plot_menu(), 0, NULL);
	return D_O_K;
}

#endif
