# include "xvin.h"
# include "xvin2.h"
# include "xvadd.h"
# include <float.h>
    

char *read_avcor_data(char *path, char *file, FILE **fpi)
{
	FILE *fp;
	char filename[256];
	static char *line = NULL;
	char *c, *l;


	if (line == NULL)
	{
		line = (char *)calloc(4096,sizeof(unsigned char));
		if (line == NULL) return win_printf_ptr("Cannot alloc memory");
	}
	if (*fpi == NULL)
	{
		build_full_file_name(filename, 256, path, file);
		fp = fopen(filename,"rb");
		if (fp == NULL)
			return win_printf_ptr("Cannot open :\n%s",filename);
		*fpi = fp;
		c =  NULL;
		l = line;
		while (c == NULL && l != NULL)
		{
			l = fgets(line, 4096, *fpi);
			c = strstr(l,"Correlation (Multi, Averaged)");
		}
		if (l == NULL)
			return win_printf_ptr("File %s\nis not a correlation function",filename);
		l = fgets(line, 4096, *fpi);

	}	
	l = fgets(line, 4096, *fpi);
/*	fprintf(stderr,"%s\n",l);*/
	return l;
}



int	add_error_bars(O_p *op, d_s *dsi, d_s *dsr)
{
	register int i;
	d_s  *dsd;
	int nf;
	
	if (dsi->nx != dsr->nx)	return win_printf("data set have different size !");

	nf = dsi->nx;
	if ((dsd = create_and_attach_one_ds(op, 2*nf, 2*nf, 0)) == NULL)
		return win_printf("cannot create plot");	

	for (i = 0; i < nf; i++)
	{		
		if (dsi->xd[i] != dsr->xd[i])	
			win_printf("pts at index %d differ in X\nData X = %g error X %g!",i,dsi->xd[i],dsr->xd[i]);
		dsd->xd[2*i] = dsd->xd[2*i+1] = dsi->xd[i];
		dsd->yd[2*i] = dsi->yd[i] + dsr->yd[i];
		dsd->yd[2*i+1] = dsi->yd[i] - dsr->yd[i];
	}
	set_dash_line(dsd);
	set_plot_symb(dsd, "-");
	inherit_from_ds_to_ds(dsd, dsi);
	if (dsd->source != NULL)
	{
		free(dsd->source);
		dsd->source = NULL;
	}
	dsd->source = my_sprintf(NULL,"avcor error bars");	
	return 0;
}


acreg *create_avcor_plot(char *path, char *file, int mode)
{
	register int i, j;
	FILE *fpi = NULL;
	acreg *ac;
	pltreg *pr;
	O_p *op, *opr, *opxi2;
	d_s *ds, *dss, **dsr, *dsxi2;
	char *line, *bp;
	int ne, np;
	char filename[256], cha[256];	
	float t, ga, g, gs = 0, tmp, xi2;
	
	line = read_avcor_data(path, file, &fpi);
	if (line == NULL)	return NULL;
	
	for (i = 0, bp = line, gs = 0; bp != NULL && i < 50 ; i++)
	{
		g = _strtold(bp, &bp);	
		if (i == 0) t = g;
		else if (i == 1) ga = g;
		else gs += (g - ga)*(g - ga);
		if (sscanf(bp,"%f",&tmp) != 1) bp = NULL;
	}
	if (i > 2) gs /= (i-2);
	gs = sqrt(gs);
	if (i > 2) gs /= sqrt(i-2); 
	ne = i;
	np = i - 2;
		
	/* win_printf("number of averaging %d",ne-2);			*/
	
	ac = create_plot_ac(file, 512, 512, 0);
	if (ac_grep(ac,"%pr%op%ds",&pr,&op,&ds) != 3)
			return win_printf_ptr("cannot find plot!");
			
	if ((dss = create_and_attach_one_ds(op, 512, 512, 0)) == NULL)
		return win_printf_ptr("cannot create plot !");
				
	if ((opxi2 = create_and_attach_one_plot(pr, np, np, 0)) == NULL)
		return win_printf_ptr("cannot create \\chi^2 plot !");		
	dsxi2 = opxi2->dat[0]; 	
	set_plot_x_title(opxi2, "lot number");
	set_plot_y_title(opxi2, "\\chi^2");
	set_dot_line(dsxi2);
	set_plot_symb(dsxi2, "\\oc");
																
	build_full_file_name(filename, 256, path, file);		
	backslash_to_slash(filename);
	ds->source = my_sprintf(NULL,"avcor average of %s",filename);
	dss->source = my_sprintf(NULL,"avcor error of %s",filename);
	dsxi2->source = my_sprintf(NULL,"avcor chi2 of %s",filename);
	
	for (i = 0; file[i] != 0 && file[i] != '.'; i++)
		filename[i] = file[i];
	if (file[i] == '.') filename[i] = 0;
	else filename[i+1] = 0;
	sprintf(filename,"%s.gr",filename);
	op->filename = strdup(filename);

	if (mode) /* add plot for all runs */
	{
		dsr = (d_s**)calloc(np,sizeof(d_s*));
		if (dsr == NULL)
			return win_printf_ptr("cannot create data set array !");
		
		for (i = 0; i < np; i++)
		{
			if ((opr = create_and_attach_one_plot(pr, 512, 512, 0)) == NULL)
				return win_printf_ptr("cannot create run plot !");		
			dsr[i] = opr->dat[0]; 
			dsr[i]->source = my_sprintf(NULL,"avcor run %d %s",i,filename);
			sprintf(cha,"run%0d.gr",i);
			opr->filename = strdup(cha);	
			set_plot_title(opr, "\\stack{{Fluorescence Correlation Spectroscopie}{run %d }}",i);
			set_plot_x_title(opr, "Time \\tau (ms)");
			set_plot_y_title(opr, "g(\\tau)");
			set_plot_x_log(opr);			
			opr->dir = Mystrdup(path);
			
		}		
	}		




	ds->xd[0] = dss->xd[0] = t;
	ds->yd[0] = ga;
	dss->yd[0] = gs;
	i = ne;
/*	win_printf("reading line %d t %g g %g s %g navg %d",0,t,ga,gs,i-2);*/
	
	j = 0;
	while (i == ne && j < 512)
	{

		for (i = 0, bp = line, gs = 0; bp != NULL && i < 50 ; i++)
		{
			g = _strtold(bp, &bp);	
			if (i == 0) t = g;
			else if (i == 1) ga = g;
			else gs += (g - ga)*(g - ga);
			if (sscanf(bp,"%f",&tmp) != 1) bp = NULL;
			if (mode && i > 1) 
			{
				dsr[i-2]->yd[j] = g;
				dsr[i-2]->xd[j] = t;
			}
		}
		if (i == ne)
		{
			if (i > 2) gs /= (i-2);
			gs = sqrt(gs);	
			if (i > 2) gs /= sqrt(i-2); 			
			ds->xd[j] = dss->xd[j] = t;
			ds->yd[j] = ga;
			dss->yd[j] = gs;	
			j++;
/*			win_printf("reading line %d t %g g %g s %g navg %d",j,t,ga,gs,i-2);*/
		}
		line = read_avcor_data(path, file, &fpi);		
	}
	op->dir = Mystrdup(path);
	ds->nx = ds->ny = dss->nx = dss->ny = j;
	if (mode) 
	{
		for (i = 0; i < np; i++)
		{
			dsr[i]->nx = dsr[i]->ny = j;
			for (j = 0, xi2 = 0; j < ds->nx; j++)
			{
				tmp = (dsr[i]->yd[j] - ds->yd[j]);
				tmp /= (dss->yd[j] != 0) ? dss->yd[j] : 1;
				tmp *= tmp;
				xi2 += tmp;
			}
			dsxi2->xd[i] = i;
			dsxi2->yd[i] = xi2;
		}
	}
	set_plot_title(op, "\\stack{{Fluorescence Correlation Spectroscopie}{ average %d npt of points %d}}",ne-2,j);
	set_plot_x_title(op, "Time \\tau (ms)");
	set_plot_y_title(op, "g(\\tau)");
	set_plot_x_log(op);
	add_error_bars(op, ds, dss);
	fclose(fpi);
	do_one_plot(pr);							
	return ac;
}


d_s *find_next_source_specific_ds_in_op(O_p *op, char *source, int *n_prev)
{
	register int i, icp;
	int l;
	char *c;
	d_s *dss = NULL;
	
	if (op == NULL || source == NULL)	return NULL;
	l = strlen(source);
/*	*n_prev = (*n_prev >= op->n_dat-1) ? -1 : *n_prev;*/
	for (icp = *n_prev, i = (icp+1< 0) ? 0 : icp+1; i < op->n_dat && icp == *n_prev; i++)
	{
		if (op->dat[i] != NULL)
		{
			dss = op->dat[i];
			c = dss->source;
			icp = ((c != NULL) && strncmp(c,source,l) == 0) ? i : *n_prev;
		}
	}
	if (icp == *n_prev)
	{
		return NULL;
	}
	*n_prev = icp;
	return dss;
}


d_s *find_next_source_specific_ds_in_pr(pltreg *pr, char *source, int *nop_prev, int *nds_prev)
{
	register int i, j;
	int ip, id, notfound;
	int l;
	char *c;
	d_s *dss = NULL;
	
	if (pr == NULL || source == NULL )	return NULL;
	l = strlen(source);
	ip = *nop_prev;
	id = *nds_prev;
	id = (id+1 < 0) ? 0 : id+1;
	ip = (ip < 0) ? 0 : ip;
	for (i = ip, notfound = 1; i < pr->n_op && notfound; i++, id = 0)
	{
		for (j = id; j < pr->o_p[i]->n_dat && notfound; j++)
		{
			if (pr->o_p[i]->dat[j] != NULL)
			{
				dss = pr->o_p[i]->dat[j];
				c = dss->source;
				if ((c != NULL) && strncmp(c,source,l) == 0)
				{
					notfound = 0;
					*nop_prev = i;
					*nds_prev = j;
				} 
			}
		}
	}
	if (notfound)
	{
		return NULL;
	}
	return dss;
}


int	compute_avcor_error_and_mean(char ch)
{
	register int  j;
	d_s *dsa, *dser, *ds, *dsxi2;
	int np, nop = 0, nds = -1, itmp;
	pltreg *pr;
	O_p *op;
 
	
	if (ch != CR)	return OFF;
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
			return win_printf("cannot find plot!");


 	dsa = find_source_specific_ds_in_pr(pr,"avcor average of");
	if (dsa == NULL)
		return win_printf("I cannot find averagage correlation plot");
 	dser = find_source_specific_ds_in_pr(pr,"avcor error bars");	
	if (dser != NULL)
	{
		j =	find_op_nb_of_source_specific_ds_in_pr(pr, "avcor error bars");
		if (j >= 0 && j < pr->n_op)
			remove_one_plot_data (pr->o_p[j], IS_DATA_SET, (void *)dser);
	}	

		
 	dser = find_source_specific_ds_in_pr(pr,"avcor error of");
	if (dser == NULL)
		return win_printf("I cannot find error of correlation plot");	
	
	dsxi2 = find_source_specific_ds_in_pr(pr,"avcor chi2 of");
	if (dsxi2 == NULL)
		win_printf("I cannot find \\chi^2 of correlation plot");		
	
	
	for (j = 0; j < dsa->nx; j++) dsa->yd[j] = 0;
	np = 0;
	do
	{
		ds = find_next_source_specific_ds_in_pr(pr, "avcor run", &nop, &nds);
		if (ds != NULL)
		{
			np++;
			for (j = 0; j < dsa->nx; j++) 
			{
				if (dsa->xd[j] != ds->xd[j])
					win_printf("At point %d time differs for average\n"
					"and ds %d\nt(average) = %g t ds %g",j,np,dsa->xd[j],ds->xd[j]);	
				dsa->yd[j] += ds->yd[j];
			}
		}
	}while(ds != NULL);
	if (np <= 0)		return win_printf("I did not find any run");	
	for (j = 0; j < dsa->nx; j++) dsa->yd[j] /= np;	
	
	nop = 0; nds = -1;
	for (j = 0; j < dser->nx; j++) dser->yd[j] = 0;
	np = 0;
	do
	{
/*		win_printf("plot %d nop %d ds %d",np,nop,nds);*/
		ds = find_next_source_specific_ds_in_pr(pr, "avcor run", &nop, &nds);
		if (ds != NULL)
		{
			np++;
			for (j = 0; j < dser->nx; j++) 
			{
				if (dser->xd[j] != ds->xd[j])
					win_printf("At point %d time differs for error\n"
					"and ds %d\nt(error) = %g t ds %g",j,np,dser->xd[j],ds->xd[j]);	
				dser->yd[j] += (ds->yd[j] - dsa->yd[j])*(ds->yd[j] - dsa->yd[j]);
			}
		}
	}while(ds != NULL);
	if (np <= 0)		return win_printf("I did not find any run");	
	for (j = 0; j < dser->nx; j++) dser->yd[j] /= np;	
	for (j = 0; j < dser->nx; j++) dser->yd[j] = sqrt(dser->yd[j])/sqrt(np);
	
	j =	find_op_nb_of_source_specific_ds_in_pr(pr, "avcor average of");
	if (j >= 0 && j < pr->n_op)
		add_error_bars(pr->o_p[j], dsa, dser);	
	if (dsxi2 != NULL && np < dsxi2->mx)		
	{
		nop = 0; nds = -1;	np = 0;
		do
		{
			ds = find_next_source_specific_ds_in_pr(pr, "avcor run", &nop, &nds);
			if (ds != NULL)
			{
				for (j = 0, dsxi2->yd[np] = 0; j < dser->nx; j++) 
				{
					dsxi2->yd[np] += (ds->yd[j] - dsa->yd[j])*(ds->yd[j] - dsa->yd[j])/(dser->yd[j]*dser->yd[j]);
				}
				if (sscanf(ds->source,"avcor run %d",&itmp) == 1)
					dsxi2->xd[np] = itmp;
				np++;
			}
		}while(ds != NULL);	
		dsxi2->nx = dsxi2->ny = np;
	}
	return refresh_plot(pr,UNCHANGED);
}

int	load_avcor_movie(char ch)
{
	register int i;
	static char c_dir[256], *c = NULL, *prev_file = NULL, f[32];
	int	display_loading_win(char *path, char *pattern, char *file, char *purpos);	
	static char pattern[128], *pat = "*.asc";
	extern char	file_pattern[128];
	acreg *ac;
	
	if (ch != CR)	return OFF;
	i = display_loading_win(c,pat,prev_file,"loading avcor correlation");
	if (i == CANCEL)	return NULL;
	if (select_file.ff_name[0] == 0) 	return NULL;
	strcpy(c_dir,file_path);
	c = c_dir;	
	set_cursor_glass();
	strcpy(f,select_file.ff_name);
	prev_file = f;
	strncpy(pattern,file_pattern,127);	
	pat = pattern;	
	
/*	win_printf("file %s \npath %s",f,backslash_to_slash(file_path));*/
	ac = create_avcor_plot(file_path, f, 1);
/*	win_printf("after create");*/
	activate_acreg(ac);
	

	reset_cursor();	
	return 0;
}





int		avcor_main(int argc, char **argv)
{
	load_avcor_movie(CR);
	return 0;
}







