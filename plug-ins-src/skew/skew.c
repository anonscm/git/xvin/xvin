/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _SKEW_C_
#define _SKEW_C_

# include "allegro.h"
# include "xvin.h"
# include <gsl/gsl_sort.h>
# include <gsl/gsl_statistics.h>

/* If you include other plug-ins header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "skew.h"

int do_compute_skewness_of_data_set(void)
{
	O_p *op = NULL;
	int nf;
	d_s *ds;
	pltreg *pr = NULL;
	double mean, sd, largest, smallest, skew, kurtosis; /*, *ard; */
	/* display routine action if SHIFT is pressed */
	if(updating_menu_state != 0)	return D_O_K;	
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine compute the skewness of"
	"the y coordinate of a data set ");
	}
	/* we first find the data that we need to transform */
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
		return win_printf_OK("cannot find data");
	nf = ds->nx;	/* this is the number of points in the data set */

/*	ard = (double*)calloc(ds->ny,sizeof(double));
	if (ard == NULL)	win_printf_OK("no memory!");
	for (i = 0; i < ds->ny; i++)	ard[i] = ds->yd[i];*/
	mean     = gsl_stats_float_mean(ds->yd, 1, ds->ny);
	sd = gsl_stats_float_sd_m(ds->yd, 1, ds->ny, mean);
	largest  = gsl_stats_float_max(ds->yd, 1, ds->ny);
	smallest = gsl_stats_float_min(ds->yd, 1, ds->ny);
	skew = gsl_stats_float_skew_m_sd (ds->yd, 1, ds->ny,  mean, sd);
	kurtosis = gsl_stats_float_kurtosis_m_sd (ds->yd, 1, ds->ny,  mean, sd);
	

	mean = op->ay + op->dy * mean;
	sd = op->ay + op->dy * sd;
	largest = op->ay + op->dy * largest;
	smallest = op->ay + op->dy * smallest;
	
	set_ds_plot_label(ds, op->x_lo + (op->x_hi - op->x_lo)/4, 
		op->y_hi - (op->y_hi - op->y_lo)/4, USR_COORD, 
	"\\fbox{\\stack{{Statistics Y over %d points}{mean =  %g \\stack{+-} %g %s}"
	"{min %g, max %g %s}{skewness %g kurtosis %g}}}",
	ds->ny,mean,sd,(op->y_unit)?op->y_unit:" ",smallest, largest, 
	(op->y_unit)?op->y_unit:" ",skew, kurtosis );
	
	op->need_to_refresh = 1;	
	
	/* refisplay the entire plot */
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
}



MENU *skew_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"Skewness in Y", do_compute_skewness_of_data_set,NULL,0,NULL);

	return mn;
}

int	skew_main(int argc, char **argv)
{
  (void)argc;
    (void)argv;
	add_plot_treat_menu_item ( "skew", NULL, skew_plot_menu(), 0, NULL);
	return D_O_K;
}
int	skew_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;
        remove_item_to_menu(image_treat_menu, "skew", NULL, NULL);
	return D_O_K;
}

#endif
