#ifndef _SKEW_H_
#define _SKEW_H_

PXV_FUNC(MENU*, skew_plot_menu, (void));
PXV_FUNC(int, do_compute_skewness_of_data_set, (void));
PXV_FUNC(int, skew_main, (int argc, char **argv));
#endif
