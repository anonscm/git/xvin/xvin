/*
 *    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
 */
#ifndef _OMAR2_C_
#define _OMAR2_C_

# include "allegro.h"
# include "xvin.h"
# include "float.h"

/* If you include other plug-ins header do it here*/ 



/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "omar2.h"

double 	*p1 = NULL;

int switch_plot2(int c)
{
  return 0;
}

int switch_data_set(int c)
{
  return 0;
}

int do_pow_in_ds(void)
{
  register int i;
  O_p *op;
  d_s *ds,*dsi;
  pltreg *pr;
  static int n = -2;
  static float a = 1, b = 0, start = 1, end = 100, mult = 2;
  int count = 0;
  float tmp;
  
  if(updating_menu_state != 0)	return D_O_K;	
  if (key[KEY_LSHIFT])
    return win_printf("This routine multiply the y coordinate\n of a data set by a number");
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf("cannot find data");
	
  i = win_scanf("Generates y = a*x^n + b \n a = %f n = %d b = %f start = %f end = %f mult = %f", &a, &n, &b, &start,&end,&mult); /* don't forget the &*/
  if (i == WIN_CANCEL)	return OFF;
  for(i = 0, tmp = start; tmp <= end; i++) tmp *=mult;
  count = i;
  if ((ds = create_and_attach_one_ds(op, count+1, count+1, 0)) == NULL)
    return win_printf("cannot create plot !");
	
  for (i = 0,tmp = start; tmp <= end; i++)
    {
      ds->xd[i] = start*pow(mult,i);
      ds->yd[i] = a*pow(ds->xd[i],n) +b;
      tmp = ds->xd[i];
    }

  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);		
  switch_plot2(0);
  switch_data_set(0);
  return 0;
}

int fit_line(d_s *dsi, float *a, float *b)
{
  register int i;
  int npoints;
  double sx,sy,sxy,sx2,sy2, d;
	
  npoints = dsi->nx;
  if (npoints == 0)	return win_printf("No points to average !");
	
  for (i=0, sy2 = sy = sx = sx2 = sxy = 0 ; i< npoints ; i++)
    {
      sy += dsi->yd[i];
      sx += dsi->xd[i];
      sx2 += dsi->xd[i] * dsi->xd[i];
      sxy += dsi->xd[i] * dsi->yd[i];
    }
  d = (sx2 * npoints) - (sx * sx);
  if (d == 0)	return win_printf_OK("\\Delta  = 0 \n can't fit that!");
  *a = (sxy * npoints - sx * sy)/d;
  *b = (sx2 * sy - sx * sxy)/ d;
  win_printf("fit over %d pts a %g b %g",npoints,*a,*b);
  /*a *= op->dy/op->dx;*/
  return 0;
}

d_s *proc(d_s *dsi, d_s *dstc, d_s *dsp, int nb)
{
  register int i,j,k;
  int nf;
  float max, min,proc;
  nf = dsi->nx;
  for (i = 1, j = 0, max = 0, min = 0; i < nf; i++)
    {
      if (dstc->yd[i] == 1 && dstc->yd[i-1] == 0) max = min = dsi->yd[i];
      if (dstc->yd[i] == 1)
	{
	  if (max < dsi->yd[i]) max = dsi->yd[i];
	  if (min > dsi->yd[i]) min = dsi->yd[i];
	}
      if (dstc->yd[i] == 0 && dstc->yd[i-1] == 1)
	{
	  proc = max - min;
	  dsp->yd[j] = max - min;
	  for (k = 0; dstc->yd[i-1-k] == 1; k++) {};
	  dsp->xd[j] = dstc->xd[i-k];
	  j++;
	}
      if (i == nf - 1 && dstc->yd[i] == 1)
	{
	  proc = max - min;
	  dsp->yd[j] = max - min;
	  for (k = 0; dstc->yd[i-1-k] == 1; k++) {};
	  dsp->xd[j] = dstc->xd[i-k];
	}
			
    }
  return dsp;
}

int countbursts(d_s *dsi, int *nb, int *nbp)
{
  /*given a cleaned threshold file, with `1's representing data points inside bursts, this function outputs the # of bursts by counting the number of groups of `1's, and the total # of data points contained in bursts*/
  int nf;
  register int i;
  nf = dsi->nx;
  for (i = 0, *nb = 0, *nbp = 0; i < nf; i++)
    {
      if (i > 0)
	{
	  if (dsi->yd[i-1] == 0 && dsi->yd[i] == 1) ++*nb;
	}
      if (dsi->yd[i] == 1) ++*nbp;
    }
  return 0;
}

d_s *cleanthresh(d_s *dsi, d_s *dst, int cleaning)
{
  /*inputs binary data, removes all stretches of 1's less than 3, also windows the input processivity and std. dev.*/
  register int i, j, k, l;
  int nf;
  float max, min, proc, meanv, var, time;
  static int choice;
  static float minproc = 0.1, maxproc = 10, mindev = 0, maxdev = 10, maxvel = 0, 
    minvel = -10, mint = 10, maxt = 1000, startpoint = 0;
	
  nf = dsi->nx;

  if (cleaning < 1) return dst;

  /* for (i = 0; i < nf; i++)
     {
     ds->xd[i] = dsi->xd[i];
     ds->yd[i] = dsi->yd[i];
     } */
  for (i = 2; i < nf; i++)
    {
      if (dst->yd[i-2] == 0 && dst->yd[i-1] == 1 && dst->yd[i] == 0) dst->yd[i-1] = 0;
    }

  for (i = 3; i < nf; i++)
    {
      if (dst->yd[i-3] == 0 && dst->yd[i-2] == 1 && dst->yd[i-1] == 1 && dst->yd[i] == 0) 
	{
	  dst->yd[i-1] = 0;
	  dst->yd[i-2] = 0;
	}
    }

  if (cleaning < 2) return dst;

  win_scanf("Cleaning Events:\n"
	    "min proc = %f max proc = %f min std dev = %f max std dev = %f min vel = %f "
	    "max vel = %f min time = %f max time = %f Adjust endpoints (1=Y) %d"
	    "Start point > %f",&minproc, &maxproc, &mindev, &maxdev, &minvel, &maxvel, &mint, 
	    &maxt, &choice, &startpoint);

  for (i = 1, j = 0, max = 0, min = 0; i < nf; i++)
    {
      /*	if (dst->yd[i] == 1 && dst->yd[i-1] == 0) max = min = dsi->yd[i];
	if (dst->yd[i] == 1)
	{
	if (max < dsi->yd[i]) max = dsi->yd[i];
	if (min > dsi->yd[i]) min = dsi->yd[i];
	} */
      if (dst->yd[i] == 0 && dst->yd[i-1] == 1)
	{
	  for(k = 0; dst->yd[i-1-k] == 1;k++) {};
	  /*		proc = max - min; */
	  if (/* proc < minproc || proc > maxproc || */ k < 10)
	    {
	      for(l = 0; l < k; l++)
		{
		  dst->yd[i-k+l] = 0;
		}
	    }
	  else
	    {
	      if (choice != 0)
		{
		  dst->yd[i-1] = 0;
		  dst->yd[i-k] = 0;
		  time = dst->xd[i-2] - dst->xd[i-k+1];
		  meanv = (dsi->yd[i-2] - dsi->yd[i-k+1])/(dsi->xd[i-2] - dsi->xd[i-k+1]);
		  max = min = dsi->yd[i-k+1];
		  for (var = 0, l = i - k + 2; dst->yd[l] == 1; l++)
		    {
		      var +=pow(((dsi->yd[l] - dsi->yd[l-1])/(dsi->xd[l] - dsi->xd[l-1])) - meanv,2);
		      if (max < dsi->yd[l]) max = dsi->yd[l];
		      if (min > dsi->yd[l]) min = dsi->yd[l];
		    }
		  proc = max - min;
		  var /= k-3;
		  var = sqrt(var);
		 /*  if (var < mindev || var > maxdev || meanv < minvel || meanv > maxvel || time < mint || time > maxt || proc < minproc || proc > maxproc || dsi->yd[i-k+1] < startpoint) */
/* 		    { */
/* 		      for(l = 0; l < k; l++) */
/* 			{ */
/* 			  dst->yd[i-k+l] = 0; */
/* 			} */
/* 		    } */
		}
	      else
		{
		  time = dst->xd[i-1] - dst->xd[i-k];
		  meanv = (dsi->yd[i-1] - dsi->yd[i-k])/(dsi->xd[i-1] - dsi->xd[i-k]);
		  max = min = dsi->yd[i-k];
		  for (var = 0, l = i - k + 1; dst->yd[l] == 1; l++)
		    {
		      var +=pow(((dsi->yd[l] - dsi->yd[l-1])/(dsi->xd[l] - dsi->xd[l-1])) - meanv,2);
		      if (max < dsi->yd[l]) max = dsi->yd[l];
		      if (min > dsi->yd[l]) min = dsi->yd[l];
		    }
		  proc = max - min;
		  var /= k-1;
		  var = sqrt(var);
		  if (var < mindev || var > maxdev || meanv < minvel || meanv > maxvel || time < mint || time > maxt || proc < minproc || proc > maxproc || dsi->yd[i-k] < startpoint)
		    {
		      for(l = 0; l < k; l++)
			{
			  dst->yd[i-k+l] = 0;
			}
		    }
		}
	    }
	}
    }

  return dst;
}

d_s *jumpfit(d_s *dsi, d_s *ds)
{
  register int i, j, k,l;
  /*d_s *dstmp;*/
  int nf, out;
  static float no = 1;
  double y0, chi2, chi2n, tmp, totalchi, tmp2, ratio, /*transx,*/ *transy, *collx, *colly;
  double weight=0;
  static int width = 16, w = 2, pt_index = -1, repeat = 3;
  static int c=0, delta=1;
  static float p = 13;
	
  nf = dsi->nx;	/* this is the number of points in the data set */
  
  /*transx = (double *) calloc (nf,sizeof(double));*/
  transy = (double *) calloc (nf,sizeof(double));
  collx = (double *) calloc (nf,sizeof(double));
  colly = (double *) calloc (nf,sizeof(double));
	
  /*dstmp = build_data_set(nf, nf);
    dstmp->nx = dstmp->ny = nf;*/
  i = win_scanf("Weighting by  (\\chi^2/\\sigma^2)^{-p} using forward/backward algorithm."
		"Flat events expected."
		"Apply how many times? %8d. "
		"Define the bounds on the # points used per fit w= (2m + 1):\n"
		"max m= %8d min m=%8d Increment (i.e. 1,2,3... vs. 1,5,9..) %8d \nthe sigma of noise= %12f "
		"the weighting power p= %10f\n Display point %8d (-1 none)\n",
		&repeat,&w,&c,&delta,&no,&p,&pt_index); /* don't forget the &*/
  
  if (i == WIN_CANCEL)	return OFF;
  
  
  if (c>w) win_printf("YOU SCREWED UP: Min width > Max width!!!");
  for (i = 0; i < nf; i++)
    {
      /*dstmp->xd[i] = dsi->xd[i];
	dstmp->yd[i] = dsi->yd[i];*/
      
      transy[i] = (double) dsi->yd[i];
      collx[i] = 0;
      colly[i] = 0;
      ds->xd[i] = dsi->xd[i];	
    }
  for (l = 0; l < repeat; l++)
    {
      for (k = c; k <= w; k = k + delta)
	{
	  for (j = 0, width = 2*k +1; j < nf-width+1; j++)
	    {
	      /*if (poly_fit1(dsi->yd+j, width, &y0, &y1) < 0)
		win_printf("fit pb");*/
	      for (i = 0, y0 = 0; i < width; i++)
		{
		  /*y0 += dstmp->yd[j+i];*/
		  y0 += transy[j+i];
		}
	      y0 /= width;
	      for (i = 0, chi2 = 0; i < width; i++)
		{
		  /*tmp = y0 - dstmp->yd[j+i];*/
		  tmp = y0 - transy[j+i];
		  chi2 += tmp * tmp;
		}			
	      chi2n = chi2 / ((width-2)*no*no);
	      if (chi2n != 0)
		{
		  weight = pow(chi2n,(-p));
		  if ((i+j) == pt_index)
		    {
		      out = win_printf("weight %e",weight);
		      if (out == WIN_CANCEL) pt_index = -1;
		    }
		  /*out = win_printf("weight = %g chi2n %g p  %d k %d j %d",weight,chi2n,p,k,j);*/
		  if (weight > 1e300) weight=1e300;
		  if (weight < 1e-300) weight=1e-300;
		}
	      else
		{
		  weight = 1;
		}
	      for (i = 0; i < width; i++)
		{
		  if ((i+j) == pt_index)
		    {
		      out = win_printf("width = %d \\chi^2n %e\nweight %e j %d",
				       width,chi2n,weight,j);
		      if (out == WIN_CANCEL) pt_index = -1;
		    }
		}
	      /*ds->yd[j] += weight*y0;
		ds->xd[j] += weight;*/
	      colly[j] += weight*y0;
	      collx[j] += weight;
	      
	      /*ds->yd[j+width-1] += weight*y0;
		ds->xd[j+width-1] += weight;*/
	      colly[j+width-1] += weight*y0;
	      collx[j+width-1] += weight;
	      
	    }
	}
      for (j = 0, tmp2=0; j < nf; j++)
	{	
	  
	  /*if (ds->xd[j] != 0) ds->yd[j] /= ds->xd[j];*/
	  if (collx[j] != 0) colly[j] /= collx[j];
			
	  /*tmp2 += (dsi->yd[j] - ds->yd[j]) * (dsi->yd[j] - ds->yd[j]);*/
	  tmp2 += (dsi->yd[j] - colly[j]) * (dsi->yd[j] - colly[j]);

	  /*dstmp->yd[j] = ds->yd[j];*/
	  transy[j] = colly[j];
	  ds->yd[j] = colly[j];
	  colly[j] = 0;
	  collx[j] = 0;
	}
      totalchi = sqrt(tmp2/nf);
      ratio = totalchi/no;
      out = win_printf("totalchi %f noise %f ratio %f",totalchi,no,ratio);
    }
  free(transy);
  free(collx);
  free(colly);
  return ds;
}



/*
    Weighting by  (\\chi^2/\\sigma^2)^{-p} using forward/backward algorithm.
    Flat events expected.
    repeat specify the # of times to Apply filter
    Define the bounds on the # points used per fit w= (2m + 1):
    max m= %8d min m=%8d Increment (i.e. 1,2,3... vs. 1,5,9..) %8d \n
    the sigma of noise= %12f "
    "the weighting power p= %10f\n Display point %8d (-1 none)\n",
    &repeat,&w,&c,&delta,&no,&p,&pt_index); 
  
 */


 d_s    *jumpfit_vc(d_s *dsi, d_s *ds, int repeat, int max, int min, int delta, float sigm, float p, double *chi2t)
{
  register int i, j, k,l;
  int nf, width = 16;
  double y0, chi2, chi2n, tmp, totalchi = 0, tmp2, weight = 0;
  static double *collx = NULL, *colly = NULL;
  static int n_size = 0;

  if (dsi == NULL) return NULL;
  if (min > max) return NULL; // "YOU SCREWED UP: Min width > Max width!!!"
	
  nf = dsi->nx;	/* this is the number of points in the data set */
  if (ds == NULL || ds->mx < nf || ds->my < nf)    ds = build_adjust_data_set(ds, nf, nf);
  ds->nx = ds->ny = nf;
  if (ds == NULL) return NULL;
  
  if (nf > n_size)
    {
      if (collx) free(collx);
      if (colly) free(colly);
      collx = (double *) calloc (nf,sizeof(double));
      colly = (double *) calloc (nf,sizeof(double));
      if (collx == NULL || colly == NULL) return NULL;
      n_size = nf;
    }
  

  for (i = 0; i < nf; i++)
    {
      collx[i] =  colly[i] = 0;
      ds->xd[i] = dsi->xd[i];	
    }
  for (l = 0; l < repeat; l++)
    {
      for (k = min; k <= max; k = k + delta)
	{
	  for (j = 0, width = 2 * k + 1; j < nf - width + 1; j++)
	    {
	      for (i = 0, y0 = 0; i < width; i++)
		  y0 += dsi->yd[j+i];
	      y0 /= width;
	      for (i = 0, chi2 = 0; i < width; i++)
		{
		  tmp = y0 - dsi->yd[j+i];
		  chi2 += tmp * tmp;
		}			
	      chi2n = chi2 / ((width-2)*sigm*sigm);
	      if (chi2n != 0)
		{
		  weight = pow(chi2n,(-p));
		  if (weight > 1e300) weight=1e300;
		  if (weight < 1e-300) weight=1e-300;
		}
	      else
		{
		  weight = 1;
		}
	      colly[j] += weight*y0;
	      collx[j] += weight;
	      colly[j+width-1] += weight*y0;
	      collx[j+width-1] += weight;
	    }
	}
      for (j = 0, tmp2=0; j < nf; j++)
	{	
	  if (collx[j] != 0) colly[j] /= collx[j];
	  tmp2 += (dsi->yd[j] - colly[j]) * (dsi->yd[j] - colly[j]);
	  ds->yd[j] = colly[j];
	  colly[j] = 0;
	  collx[j] = 0;
	}
      totalchi = sqrt(tmp2/nf);
    }
  if (chi2t) *chi2t = totalchi;
  inherit_from_ds_to_ds(ds, dsi);
  return ds;
}

float get_sigma_of_derivative(d_s *dsi, float uperlimit)
{
  register int i;
  float tmp, sigma2, sigest2;
  int nf, ng;

  if (dsi == NULL) return -1;
  nf = dsi->nx;	/* this is the number of points in the data set */
  if (nf < 2) return -1;
  sigest2 = 4 * uperlimit * uperlimit;
  for (i = ng = 0, sigma2 = 0; i < nf-1; i++)
    {
      tmp = dsi->yd[i+1] - dsi->yd[i];
      tmp *= tmp;
      if (tmp < sigest2)
	{
	  sigma2 += tmp;
	  ng++;
	}
    } 
  if (ng < 1) return -1;
  sigma2 /= ng;
  sigest2 = 4 * sigma2;
  for (i = ng = 0, sigma2 = 0; i < nf-1; i++)
    {
      tmp = dsi->yd[i+1] - dsi->yd[i];
      tmp *= tmp;
      if (tmp < sigest2)
	{
	  sigma2 += tmp;
	  ng++;
	}
    } 
  if (ng < 1) return -1;
  sigma2 /= ng;
  return sqrt(sigma2);
}


d_s    *jumpfit_oi_vc(d_s *dsi, d_s *ds, O_i *oi_chi2, O_i *oi_val, int repeat, int max, int min, int delta, float sigm, float p, double *chi2t, d_s **ds_fpos)
{
  register int i, j, k,l;
  int nf, width = 16, oi_chi2_valid = 0, oi_val_valid = 0;
  double y0, chi2, chi2n, tmp, totalchi = 0, tmp2, weight = 0;
  static double *collx = NULL, *colly = NULL;
  static int n_size = 0;

  if (dsi == NULL) return NULL;
  if (min > max) return NULL; // "YOU SCREWED UP: Min width > Max width!!!"
	
  nf = dsi->nx;	/* this is the number of points in the data set */
  if (ds == NULL || ds->mx < nf || ds->my < nf)    ds = build_adjust_data_set(ds, nf, nf);
  ds->nx = ds->ny = nf;
  if (ds == NULL) return NULL;

  if (*ds_fpos == NULL || (*ds_fpos)->mx < nf || (*ds_fpos)->my < nf)    
    *ds_fpos = build_adjust_data_set(*ds_fpos, nf, nf);
  (*ds_fpos)->nx = (*ds_fpos)->ny = nf;
  if (*ds_fpos == NULL) return NULL;
  
  if (nf > n_size)
    {
      if (collx) free(collx);
      if (colly) free(colly);
      collx = (double *) calloc (nf,sizeof(double));
      colly = (double *) calloc (nf,sizeof(double));
      if (collx == NULL || colly == NULL) return NULL;
      n_size = nf;
    }
  //o_chi2 = create_one_image(nf, 2*max + 1, IS_FLOAT_IMAGE);
  //o_val = create_one_image(nf, 2*max + 1, IS_FLOAT_IMAGE);

  if (oi_chi2 != NULL  && oi_chi2->im.data_type == IS_FLOAT_IMAGE
      && oi_chi2->im.nx >= nf  && oi_chi2->im.ny >= (2*max+1)) 
    oi_chi2_valid = 1;
  if (oi_val != NULL && oi_val->im.data_type == IS_FLOAT_IMAGE
      && oi_val->im.nx >= nf && oi_val->im.ny >= (2*max+1)) 
    oi_val_valid = 1;

  for (i = 0; i < nf; i++)
    {
      collx[i] =  colly[i] = 0;
      ds->xd[i] = (*ds_fpos)->xd[i] = dsi->xd[i];	
    }
  for (l = 0; l < repeat; l++)
    {
      for (k = min; k <= max; k = k + delta)
	{
	  for (j = 0, width = 2 * k + 1; j < nf - width; j++)
	    {
	      for (i = 0, y0 = 0; i < width; i++)
		  y0 += dsi->yd[j+i];
	      y0 /= width;
	      for (i = 0, chi2 = 0; i < width; i++)
		{
		  tmp = y0 - dsi->yd[j+i];
		  chi2 += tmp * tmp;
		}		
	      if (width <= 1) chi2n = 4;
	      else 
		{
		  chi2n = chi2 / (sigm*sigm);
		  chi2n += 2*sqrt(width);
		  chi2n = chi2n / (width-1);
		}
	      if (chi2n < 1) chi2n = 1;    // we correct for fluctuation
	      //chi2n /= sqrt(width)/2;        // we favor long averaging
	      if (chi2n != 0)
		{
		  weight = pow(chi2n,(-p));
		  if (weight > 1e300) weight=1e300;
		  if (weight < 1e-300) weight=1e-300;
		}
	      else	  weight = 1;
	      colly[j] += weight*y0;
	      collx[j] += weight;
	      (*ds_fpos)->yd[j] += k * weight;
	      if (width > 1)
		{
		  colly[j+width-1] += weight*y0;
		  collx[j+width-1] += weight;
		  if (weight > FLT_MAX) weight=FLT_MAX;
		  if (weight < FLT_MIN) weight=FLT_MIN;
		  (*ds_fpos)->yd[j+width-1] += k * weight;
		}
	      if (oi_chi2_valid) 
		{
		  oi_chi2->im.pixel[k+max].fl[j] = chi2n;
		  oi_chi2->im.pixel[max-k].fl[j+width-1] = chi2n;
		}
	      if (oi_val_valid) 
		{
		  if (weight > FLT_MAX) weight=FLT_MAX;
		  if (weight < FLT_MIN) weight=FLT_MIN;
		  oi_val->im.pixel[k+max].fl[j] = weight;
		  oi_val->im.pixel[max-k].fl[j+width-1] = weight;
		}
	    }
	}
      for (j = 0, tmp2=0; j < nf; j++)
	{	
	  if (collx[j] != 0) 
	    {
	      colly[j] /= collx[j];
	      (*ds_fpos)->yd[j] /= collx[j];
	    }
	  if (oi_val_valid) 
	    {
	      for (i = 0; i < 2*max+1; i++)
		if (collx[j] != 0) oi_val->im.pixel[i].fl[j] /= collx[j];
	    }
	  tmp2 += (dsi->yd[j] - colly[j]) * (dsi->yd[j] - colly[j]);
	  ds->yd[j] = colly[j];
	  colly[j] = 0;
	  collx[j] = 0;
	}
      totalchi = sqrt(tmp2/nf);
    }
  if (chi2t) *chi2t = totalchi;
  inherit_from_ds_to_ds(ds, dsi);
  inherit_from_ds_to_ds(*ds_fpos, dsi);
  return ds;
}


d_s    *jumpfit_oi_vc_2(d_s *dsi, d_s *ds, O_i *oi_chi2, O_i *oi_val, int repeat, int max, float sigm, float p, float favor_long, double *chi2t, d_s **ds_fpos)
{
  register int i, j, l;
  int nf, width = 16, oi_chi2_valid = 0, oi_val_valid = 0;
  double y0, chi2, chi2n, tmp, totalchi = 0, tmp2, weight = 0, tp1, tpn, tpz, chi2s, chi2sn;
  static double *collx = NULL, *colly = NULL, *collv = NULL;
  static int n_size = 0;

  if (dsi == NULL) return NULL;
	
  nf = dsi->nx;	/* this is the number of points in the data set */
  if (ds == NULL || ds->mx < nf || ds->my < nf)    ds = build_adjust_data_set(ds, nf, nf);
  ds->nx = ds->ny = nf;
  if (ds == NULL) return NULL;

  if (*ds_fpos == NULL || (*ds_fpos)->mx < nf || (*ds_fpos)->my < nf)    
    *ds_fpos = build_adjust_data_set(*ds_fpos, nf, nf);
  (*ds_fpos)->nx = (*ds_fpos)->ny = nf;
  if (*ds_fpos == NULL) return NULL;
  
  if (nf > n_size)
    {
      if (collx) free(collx);
      if (colly) free(colly);
      if (collv) free(collv);
      collx = (double *) calloc (nf,sizeof(double));
      colly = (double *) calloc (nf,sizeof(double));
      collv = (double *) calloc (nf,sizeof(double));
      if (collx == NULL || colly == NULL || collv == NULL) return NULL;
      n_size = nf;
    }
  //o_chi2 = create_one_image(nf, 2*max + 1, IS_FLOAT_IMAGE);
  //o_val = create_one_image(nf, 2*max + 1, IS_FLOAT_IMAGE);

  if (oi_chi2 != NULL  && oi_chi2->im.data_type == IS_FLOAT_IMAGE
      && oi_chi2->im.nx >= nf  && oi_chi2->im.ny >= (4*max+1)) 
    oi_chi2_valid = 1;
  if (oi_val != NULL && oi_val->im.data_type == IS_FLOAT_IMAGE
      && oi_val->im.nx >= nf && oi_val->im.ny >= (4*max+1)) 
    oi_val_valid = 1;

  for (i = 0; i < nf; i++)
    {
      collx[i] =  colly[i] = collv[i] = 0;
      ds->xd[i] = (*ds_fpos)->xd[i] = dsi->xd[i];	
    }
  for (l = 0; l < repeat; l++)
    {
      for (width = 1; width <= max; width++)
	{
	  for (i = 0, tpn = 0; i < width; i++)
	    {
	      tp1 = i - (float)(width-1)/2;
	      tpn += tp1 * tp1;
	    }
	  for (j = 0; j < nf - width; j++)
	    {
	      for (i = 0, y0 = 0, tpz = 0; i < width; i++)
		{
		  y0 += dsi->yd[j+i];
		  tp1 = i - (float)(width-1)/2;
		  tpz += tp1 * dsi->yd[j+i];
		}
	      y0 /= width;
	      tpz = (tpn > 0) ? tpz/tpn : tpz;
	      for (i = 0, chi2 = chi2s = 0; i < width; i++)
		{
		  tmp = y0 - dsi->yd[j+i];
		  chi2 += tmp * tmp;
		  tmp = y0 + tpz * (i-(float)(width-1)/2) - dsi->yd[j+i];
		  chi2s += tmp * tmp;
		}
	      chi2n = chi2sn = 0;
	      if (width <= 1) chi2n = 4 + favor_long/sqrt(max-1);
	      else if (width == 2) 
		{
		  chi2n = chi2 / (sigm*sigm);
		  chi2n = chi2n + (favor_long*sqrt(width-1));
		  chi2n = chi2n / (width-1);
		  chi2sn = 4;
		}
	      else 
		{
		  chi2n = chi2 / (sigm*sigm);
		  //if (j == 200 && width == 10)
		  //  win_printf("j %d width %d \n chi2n %g ",j,width,chi2n);
		  chi2n = chi2n + ((double)favor_long*sqrt((double)(width-1)));
		  //if (j == 200 && width == 10)
		  //  win_printf("j %d width %d \n chi2n %g favor %g ",j,width,chi2n,favor_long);
		  chi2n = chi2n / (double)(width-1);
		  //if (j == 200 && width == 10)
		  //  win_printf("j %d width %d \n chi2n %g / width ",j,width,chi2n);
		  chi2sn = chi2s / (sigm*sigm);
		  chi2sn = chi2sn + ((double)favor_long*sqrt((double)(width-2)));
		  chi2sn = chi2sn / (double)(width-2);
		}
	      if (chi2n < 1) chi2n = 1;    // we correct for fluctuation
	      if (chi2sn < 1) chi2sn = 1;    // we correct for fluctuation
	      if (chi2n != 0)
		{
		  //weight = pow(chi2n,(-p));
		  weight = pow((double)chi2n,(double)p);
		  weight = -weight;
		  weight = exp((double)weight);
		  //weight = exp(-chi2n);
		  /*
		  if (j == 200 && width < 10)
		    {
		      win_printf("j %d width %d \n chi2n %g pow %g weight %g",j,width,chi2n,pow((double)chi2n,(double)p),weight);
		    }
		  */
		  if (weight > 1e300) weight=1e300;
		  if (weight < 1e-300) weight=1e-300;
		}
	      else	  weight = 1;
	      //colly[j] += weight*y0;
	      //collx[j] += weight;
	      (*ds_fpos)->yd[j] += width * weight;
	      if (width > 1)
		{
		  //colly[j+width-1] += weight*y0;
		  //collx[j+width-1] += weight;
		  if (weight > FLT_MAX) weight=FLT_MAX;
		  if (weight < FLT_MIN) weight=FLT_MIN;
		  (*ds_fpos)->yd[j+width-1] += width * weight;
		}
	      if (oi_chi2_valid) 
		{
		  oi_chi2->im.pixel[width+2*max].fl[j] = (float)chi2n;
		  oi_chi2->im.pixel[2*max-width].fl[j+width-1] = (float)chi2n;
		}
	      if (oi_val_valid) 
		{
		  if (weight > FLT_MAX) weight=FLT_MAX;
		  if (weight < FLT_MIN) weight=FLT_MIN;
		  oi_val->im.pixel[width+2*max].fl[j] = (float)weight;
		  oi_val->im.pixel[2*max-width].fl[j+width-1] = (float)weight;
		}
	      if (width == 1)
		{
		  colly[j] += weight*y0;
		  collx[j] += weight;
		  /*
		  if (oi_val_valid) 
		    {
		      oi_val->im.pixel[width+2*max].fl[j] = y0;
		      oi_val->im.pixel[2*max-width].fl[j+width-1] = y0;
		    }
		  */
		}
	      else if (width <= 2)
		{
		  colly[j] += weight*y0;
		  collx[j] += weight;
		  colly[j+width-1] += weight*y0;
		  collx[j+width-1] += weight;
		  /*
		  if (oi_val_valid) 
		    {
		      oi_val->im.pixel[width+2*max].fl[j] = y0;
		      oi_val->im.pixel[2*max-width].fl[j+width-1] = y0;
		    }
		  */
		}
	      if (width > 2) 
		{
		  if (chi2sn != 0)
		    {
		      //weight = pow(chi2sn,(-p));
		      weight = pow((double)chi2sn,(double)p);
		      weight = -weight;
		      weight = exp((double)weight);
		      //weight = exp(-chi2sn);
		      if (weight > 1e300) weight=1e300;
		      if (weight < 1e-300) weight=1e-300;
		    }
		  else	  weight = 1;
		  colly[j] += weight * (y0 + tpz * (float)(1-width)/2);
		  collv[j] += weight*tpz;
		  collx[j] += weight;
		  (*ds_fpos)->yd[j] += width * weight;
		  if (width > 1)
		    {
		      colly[j+width-1] += weight * (y0 + tpz * (float)(width-1)/2);
		      collx[j+width-1] += weight;
		      collv[j+width-1] += weight*tpz;
		      if (weight > FLT_MAX) weight=FLT_MAX;
		      if (weight < FLT_MIN) weight=FLT_MIN;
		      (*ds_fpos)->yd[j+width-1] += width * weight;
		    }
		  if (oi_chi2_valid) 
		    {
		      oi_chi2->im.pixel[width+3*max].fl[j] = (float)chi2sn;
		      oi_chi2->im.pixel[max-width].fl[j+width-1] = (float)chi2sn;
		    }
		  if (oi_val_valid) 
		    {
		      if (weight > FLT_MAX) weight=FLT_MAX;
		      if (weight < FLT_MIN) weight=FLT_MIN;
		      oi_val->im.pixel[width+3*max].fl[j] = (float)weight;
		      oi_val->im.pixel[max-width].fl[j+width-1] = (float)weight;
		      //oi_val->im.pixel[width+3*max].fl[j] = (y0 + tpz * (float)(1-width)/2);
		      //oi_val->im.pixel[max-width].fl[j+width-1] = (y0 + tpz * (float)(width-1)/2);
		    }
		}
	    }
	}
      for (j = 0, tmp2=0; j < nf; j++)
	{	
	  if (collx[j] != 0) 
	    {
	      colly[j] /= collx[j];
	      //(*ds_fpos)->yd[j] /= collx[j];
	      (*ds_fpos)->yd[j] = collv[j]/collx[j];;
	    }
	  if (oi_val_valid) 
	    {
	      for (i = 0; i < 4*max+1; i++)
		if (collx[j] != 0) 
		  {
		    tmp = oi_val->im.pixel[i].fl[j]/collx[j];
		    oi_val->im.pixel[i].fl[j] = (float)tmp;
		  }
	    }
	  tmp2 += (dsi->yd[j] - colly[j]) * (dsi->yd[j] - colly[j]);
	  ds->yd[j] = colly[j];
	  colly[j] = 0;
	  collx[j] = 0;
	}
      totalchi = sqrt(tmp2/nf);
    }
  if (chi2t) *chi2t = totalchi;
  inherit_from_ds_to_ds(ds, dsi);
  inherit_from_ds_to_ds(*ds_fpos, dsi);
  return ds;
}



d_s    *jumpfit_vc_2(d_s *dsi, d_s *ds, int repeat, int max, float sigm, float p, double *chi2t)
{
  register int i, j, k,l;
  int nf, width = 16;
  double y0, chi2, chi2n, tmp, totalchi = 0, tmp2, weight = 0;
  static double *collx = NULL, *colly = NULL;
  static int n_size = 0;

  if (dsi == NULL) return NULL;
	
  nf = dsi->nx;	/* this is the number of points in the data set */
  if (ds == NULL || ds->mx < nf || ds->my < nf)    ds = build_adjust_data_set(ds, nf, nf);
  ds->nx = ds->ny = nf;
  if (ds == NULL) return NULL;

  if (nf > n_size)
    {
      if (collx) free(collx);
      if (colly) free(colly);
      collx = (double *) calloc (nf,sizeof(double));
      colly = (double *) calloc (nf,sizeof(double));
      if (collx == NULL || colly == NULL) return NULL;
      n_size = nf;
    }

  for (i = 0; i < nf; i++)
    {
      collx[i] =  colly[i] = 0;
      ds->xd[i] = dsi->xd[i];	
    }
  for (l = 0; l < repeat; l++)
    {
      for (k = 0; k <= max; k++)
	{
	  for (j = 0, width = 2 * k + 1; j < nf - width; j++)
	    {
	      for (i = 0, y0 = 0; i < width; i++)
		  y0 += dsi->yd[j+i];
	      y0 /= width;
	      for (i = 0, chi2 = 0; i < width; i++)
		{
		  tmp = y0 - dsi->yd[j+i];
		  chi2 += tmp * tmp;
		}		
	      if (width <= 1) chi2n = 4;
	      else 
		{
		  chi2n = chi2 / (sigm*sigm);
		  chi2n = chi2n + 2*sqrt(width);
		  chi2n = chi2n / (width-1);
		}
	      if (chi2n < 1) chi2n = 1;    // we correct for fluctuation
	      if (chi2n != 0)
		{
		  weight = pow(chi2n,(-p));
		  if (weight > 1e300) weight=1e300;
		  if (weight < 1e-300) weight=1e-300;
		}
	      else	  weight = 1;
	      colly[j] += weight*y0;
	      collx[j] += weight;
	      if (width > 1)
		{
		  colly[j+width-1] += weight*y0;
		  collx[j+width-1] += weight;
		}
	    }
	}
      for (j = 0, tmp2=0; j < nf; j++)
	{	
	  if (collx[j] != 0) 
	    {
	      colly[j] /= collx[j];
	    }
	  tmp2 += (dsi->yd[j] - colly[j]) * (dsi->yd[j] - colly[j]);
	  ds->yd[j] = colly[j];
	  colly[j] = 0;
	  collx[j] = 0;
	}
      totalchi = sqrt(tmp2/nf);
    }
  if (chi2t) *chi2t = totalchi;
  inherit_from_ds_to_ds(ds, dsi);
  return ds;
}


d_s    *jumpfit_vc_2_exclude_edge(d_s *dsi, d_s *ds, int repeat, int max, int ex_size, float sigm, float p, double *chi2t)
{
  register int i, j, k,l;
  int nf, width = 16;
  double y0, chi2, chi2n, tmp, totalchi = 0, tmp2, weight = 0;
  static double *collx = NULL, *colly = NULL;
  static int n_size = 0;

  if (dsi == NULL) return NULL;
	
  nf = dsi->nx;	/* this is the number of points in the data set */
  if (ds == NULL || ds->mx < nf || ds->my < nf)    ds = build_adjust_data_set(ds, nf, nf);
  ds->nx = ds->ny = nf;
  if (ds == NULL) return NULL;

  if (nf > n_size)
    {
      if (collx) free(collx);
      if (colly) free(colly);
      collx = (double *) calloc (nf,sizeof(double));
      colly = (double *) calloc (nf,sizeof(double));
      if (collx == NULL || colly == NULL) return NULL;
      n_size = nf;
    }

  for (i = 0; i < nf; i++)
    {
      collx[i] =  colly[i] = 0;
      ds->xd[i] = 0;
    }
  for (l = 0; l < repeat; l++)
    {
      for (k = 0; k <= max; k++)
	{
	  for (j = 0, width = 2 * k + 1; j < nf - width; j++)
	    {
	      for (i = 0, y0 = 0; i < width; i++)
		  y0 += dsi->yd[j+i];
	      y0 /= width;
	      for (i = 0, chi2 = 0; i < width; i++)
		{
		  tmp = y0 - dsi->yd[j+i];
		  chi2 += tmp * tmp;
		}		
	      if (width <= 1) chi2n = 4;
	      else 
		{
		  chi2n = chi2 / (sigm*sigm);
		  chi2n += 2*sqrt(width);
		  chi2n = chi2n / (width-1);
		}
	      if (chi2n < 1) chi2n = 1;    // we correct for fluctuation
	      if (chi2n != 0)
		{
		  weight = pow(chi2n,(-p));
		  if (weight > 1e300) weight=1e300;
		  if (weight < 1e-300) weight=1e-300;
		}
	      else	  weight = 1;
	      colly[j] += weight*y0;
	      collx[j] += weight;
	      if (width > 1)
		{
		  colly[j+width-1] += weight*y0;
		  collx[j+width-1] += weight;
		}
	    }
	}
      for (j = 0, tmp2=0; j < nf; j++)
	{	
	  if (collx[j] != 0) 
	    {
	      colly[j] /= collx[j];
	    }
	  tmp2 += (dsi->yd[j] - colly[j]) * (dsi->yd[j] - colly[j]);
	}
      totalchi = sqrt(tmp2/nf);
    }
  for (i = 0; i < nf; i++)       ds->xd[i] = 1;
  sigm *= 4*M_SQRT2;
  for (i = 0; i < nf-1; i++)
    {
      collx[i] =  colly[i+1] - colly[i];
      if (fabs(collx[i]) > sigm)
	{
	  for (k = i-ex_size+1; k <= i+ex_size; k++)
	    if (k >= 0 && k < nf) ds->xd[k] = 0;
	}
    }
  for (i = 0, k = 0; i < nf; i++)       
    {
      if (ds->xd[i])
	{
	  ds->yd[k] = colly[i];
	  ds->xd[k++] = dsi->xd[i];
	}
    }
  ds->nx = ds->ny = k;
  if (chi2t) *chi2t = totalchi;
  inherit_from_ds_to_ds(ds, dsi);
  return ds;
}


int do_filter_nl(void)
{
  O_p *op;
  d_s *ds, *dsi, *dsp = NULL;
  pltreg *pr;
  static imreg *imr = NULL;
  O_i *oi_chi2 = NULL, *oi_val = NULL;
  register int i;
  double chi2;
  float sigm = 0.005;
  static int max = 8, repeat = 1;
  static int min =2, delta=1, d_im = 0;
  static float p = 13;

  if(updating_menu_state != 0)	return D_O_K;	
	
  if (key[KEY_LSHIFT])    return win_printf_OK("This routine filter data ");
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
	
  sigm = get_sigma_of_derivative(dsi, 0.01);
  sigm = sigm/sqrt(2);


  i = win_scanf("Non linear filter expecting flat events."
		"Weighting by  (\\chi^2/\\sigma^2)^{-p} using forward/backward algorithm.\n"
		"Apply how many times? %8d. "
		"Define the bounds on the # points used per fit w= (2m + 1):\n"
		"max m= %8d min m=%8d Increment (i.e. 1,2,3... vs. 1,5,9..) %8d \n"
		"the sigma of noise= %12f "
		"the weighting power p= %10f\n"
		"Output images for debugging %b\n"
		,&repeat,&max,&min,&delta,&sigm,&p,&d_im); /* don't forget the &*/
  
  if (i == WIN_CANCEL)	return OFF;
  
  
  if (min>max) win_printf("YOU SCREWED UP: Min width > Max width!!!");

  if (d_im)
    {
      if (imr == NULL)
	{
	  imr = create_and_register_hidden_new_image_project( 0,   32,  900,  668,"Debug");
	  if (imr == NULL)	win_printf_OK("could not create imreg");		
	  oi_chi2 = create_and_attach_oi_to_imr(imr, dsi->nx, 2*max+1, IS_FLOAT_IMAGE);
	  if (oi_chi2 == NULL)	return win_printf_OK("cannot create chi2 image !");
	  oi_val = create_and_attach_oi_to_imr(imr, dsi->nx, 2*max+1, IS_FLOAT_IMAGE);
	  if (oi_val == NULL)	return win_printf_OK("cannot create chi2 image !");
	  remove_from_image (imr, imr->o_i[0]->im.data_type, (void *)imr->o_i[0]);
	}
      else
	{
	  oi_chi2 = create_and_attach_oi_to_imr(imr, dsi->nx, 2*max+1, IS_FLOAT_IMAGE);
	  if (oi_chi2 == NULL)	return win_printf_OK("cannot create chi2 image !");
	  oi_val = create_and_attach_oi_to_imr(imr, dsi->nx, 2*max+1, IS_FLOAT_IMAGE);
	  if (oi_val == NULL)	return win_printf_OK("cannot create chi2 image !");
	}
      ds = jumpfit_oi_vc(dsi, NULL, oi_chi2, oi_val, repeat, max, min, delta, sigm,  p, &chi2, &dsp);
    }
  else   ds = jumpfit_vc(dsi, NULL, repeat, max, min, delta, sigm,  p, &chi2);

  if (ds == NULL)    return win_printf_OK("cannot create plot !");
  if (add_one_plot_data(op, IS_DATA_SET, (void*)ds))	
    return win_printf_OK("cannot find data");

  if (dsp != NULL)    
    if (add_one_plot_data(op, IS_DATA_SET, (void*)dsp))	
      return win_printf_OK("cannot find data");
	
	
	
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);		
  switch_plot2(0);
  switch_data_set(0);
  return 0;
}




int do_filter_nl_slope_and_jump(void)
{
  O_p *op;
  d_s *ds, *dsi, *dsp = NULL;
  pltreg *pr;
  static imreg *imr = NULL;
  O_i *oi_chi2 = NULL, *oi_val = NULL;
  register int i;
  double chi2;
  float sigm = 0.005;
  static int max = 8, repeat = 1;
  static int d_im = 1;
  static float p = 8, favor_long = 2;

  if(updating_menu_state != 0)	return D_O_K;	
	
  if (key[KEY_LSHIFT])    return win_printf_OK("This routine filter data ");
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
	
  sigm = get_sigma_of_derivative(dsi, 0.01);
  sigm = sigm/sqrt(2);


  i = win_scanf("Non linear filter expecting flat and slope  events"
		"Weighting by  (\\chi^2/\\sigma^2)^{-p} using forward/backward algorithm.\n"
		"Apply how many times? %8d. \n"
		"Define the bounds on the # points used per fit w= (2m + 1):\n"
		"max m= %8d long predicator bias %12f \nthe sigma of noise= %12f \n"
		"the weighting power p= %10f\n"
		//"Output images for debugging %b\n"
		,&repeat,&max,&favor_long,&sigm,&p);//,&d_im); 
  
  if (i == WIN_CANCEL)	return OFF;
  
  

  if (d_im)
    {
      if (imr == NULL)
	{
	  imr = create_and_register_hidden_new_image_project( 0,   32,  900,  668,"Debug");
	  if (imr == NULL)	win_printf_OK("could not create imreg");		
	  oi_chi2 = create_and_attach_oi_to_imr(imr, dsi->nx, 4*max+1, IS_FLOAT_IMAGE);
	  if (oi_chi2 == NULL)	return win_printf_OK("cannot create chi2 image !");
	  oi_val = create_and_attach_oi_to_imr(imr, dsi->nx, 4*max+1, IS_FLOAT_IMAGE);
	  if (oi_val == NULL)	return win_printf_OK("cannot create chi2 image !");
	  remove_from_image (imr, imr->o_i[0]->im.data_type, (void *)imr->o_i[0]);
	}
      else
	{
	  oi_chi2 = create_and_attach_oi_to_imr(imr, dsi->nx, 4*max+1, IS_FLOAT_IMAGE);
	  if (oi_chi2 == NULL)	return win_printf_OK("cannot create chi2 image !");
	  oi_val = create_and_attach_oi_to_imr(imr, dsi->nx, 4*max+1, IS_FLOAT_IMAGE);
	  if (oi_val == NULL)	return win_printf_OK("cannot create chi2 image !");
	}

      ds = jumpfit_oi_vc_2(dsi, NULL, oi_chi2, oi_val, repeat, max, sigm, p, favor_long, &chi2, &dsp);
    }

  if (ds == NULL)    return win_printf_OK("cannot create plot !");
  if (add_one_plot_data(op, IS_DATA_SET, (void*)ds))	
    return win_printf_OK("cannot find data");

  if (dsp != NULL)    
    if (add_one_plot_data(op, IS_DATA_SET, (void*)dsp))	
      return win_printf_OK("cannot find data");
	
	
	
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);		
  switch_plot2(0);
  switch_data_set(0);
  return 0;
}



int do_filter_nl_ex(void)
{
  O_p *op;
  d_s *ds, *dsi;
  pltreg *pr;
   register int i;
  double chi2;
  float sigm = 0.005;
  static int max = 8, repeat = 1, ex_avg = 4;
  static int min =2, delta=1;
  static float p = 10;

  if(updating_menu_state != 0)	return D_O_K;	
	
  if (key[KEY_LSHIFT])    return win_printf_OK("This routine filter data ");
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
	
  sigm = get_sigma_of_derivative(dsi, 0.01);
  sigm = sigm/sqrt(2);


  i = win_scanf("Non linear filter expecting flat events."
		"Weighting by  (\\chi^2/\\sigma^2)^{-p} using forward/backward algorithm.\n"
		"Apply how many times? %8d. "
		"Define the bounds on the # points used per fit w= (2m + 1):\n"
		"max m= %8d min m=%8d Increment (i.e. 1,2,3... vs. 1,5,9..) %8d \n"
		"the sigma of noise= %12f "
		"the weighting power p= %10f\n"
		"Exclude points with averaging saller than %8d\n"
		,&repeat,&max,&min,&delta,&sigm,&p,&ex_avg); /* don't forget the &*/
  
  if (i == WIN_CANCEL)	return OFF;
  
  
  if (min>max) win_printf("YOU SCREWED UP: Min width > Max width!!!");

  ds = jumpfit_vc_2_exclude_edge(dsi, NULL, repeat, max, ex_avg, sigm, p, &chi2);

  if (ds == NULL)    return win_printf_OK("cannot create plot !");
  if (add_one_plot_data(op, IS_DATA_SET, (void*)ds))	
    return win_printf_OK("cannot find data");

 	
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);		
  switch_plot2(0);
  switch_data_set(0);
  return 0;
}



d_s		*slopefilt(d_s *dsi, d_s *ds)
{
  register int i, j, k;
  /*d_s *dstmp;*/
  int nf, out;
  static float no = 1;
  double y0, y1, chi2, chi2n, tmp, totalchi, tmp2, ratio, /*transx, *transy,*/ *collx, *colly;
  double weight=0;
  static int width = 16, w = 2, pt_index = -1;
  static int c=0, delta=1;
  static double p = 13;
  static int repeat = 3;
  int l=0;
	
  nf = dsi->nx;	/* this is the number of points in the data set */
	
  /*transx = (double *) calloc (nf,sizeof(double));*/
  /*transy = (double *) calloc (nf,sizeof(double));*/
  collx = (double *) calloc (nf,sizeof(double));
  colly = (double *) calloc (nf,sizeof(double));
	
  /*dstmp = build_data_set(nf, nf);
    dstmp->nx = dstmp->ny = nf;*/
  i = win_scanf("Weighting by  (\\chi^2/\\sigma^2)^{-p} using forward/backward algorithm.\n"
		"Linefits used for predictors. Expecting linear events.\n"
		"Apply how many times? %d"
		"Define the bounds on the # points used per fit w= (2m + 1):\n"
		"min m=%d max m= %d Increment (i.e. 1,2,3... vs. 1,5,9..) %d the sigma of noise= %f"
		"the weighting power p= %lf pt to dislay (-1 -> no display) %d",&repeat,&c,&w,&delta,&no,&p,&pt_index); /* don't forget the &*/
	
  if (i == WIN_CANCEL)	return OFF;
	
	
  if (c>w) win_printf("YOU SCREWED UP: Min width > Max width!!!");
  for (i = 0; i < nf; i++)
    {
      /*dstmp->xd[i] = dsi->xd[i];
	dstmp->yd[i] = dsi->yd[i];*/
		
      /*transy[i] = (double) dsi->yd[i];*/
      collx[i] = 0;
      colly[i] = 0;
      ds->xd[i] = dsi->xd[i];	
    }
  for (l = 0; l < repeat; l++)
    {
      for (k = c; k <= w; k = k + delta)
	{
	  for (j = 0, width = 2*k +1; j < nf-width+1; j++)
	    {
	      /*if (poly_fit1(dsi->yd+j, width, &y0, &y1) < 0)
		win_printf("fit pb");*/
	      
	      poly_fit1(dsi->yd+j, width, &y0, &y1);
	      
	      for (i = 0, chi2 = 0; i < width; i++)
		{
		  /*tmp = y0 - dstmp->yd[j+i];*/
		  tmp = y0 + y1*(i-k) - dsi->yd[j+i];
		  chi2 += tmp * tmp;
		}			
	      chi2n = chi2 / ((width-2)*no*no);
	      if (chi2n != 0)
		{
		  weight = pow(chi2n,(-p));
		  if ((i+j) == pt_index)
		    {
		      out = win_printf("weight %e",weight);
		      if (out == WIN_CANCEL) pt_index = -1;
		    }
		  /*out = win_printf("weight = %g chi2n %g p  %d k %d j %d",weight,chi2n,p,k,j);*/
		  if (weight > 1e300) weight=1e300;
		  if (weight < 1e-300) weight=1e-300;
		}
	      else
		{
		  weight = 1;
		}
	      for (i = 0; i < width; i++)
		{
		  if ((i+j) == pt_index)
		    {
		      out = win_printf("width = %d \\chi^2n %e\nweight %e j %d",
				       width,chi2n,weight,j);
		      if (out == WIN_CANCEL) pt_index = -1;
		    }
		}
	      /*ds->yd[j] += weight*y0;
		ds->xd[j] += weight;*/
	      colly[j] += weight*(y0 + y1*(-k));
	      collx[j] += weight;
	      
	      /*ds->yd[j+width-1] += weight*y0;
		ds->xd[j+width-1] += weight;*/
	      colly[j+width-1] += weight*(y0 + y1*(k));
	      collx[j+width-1] += weight;
	      
	    }
	}
      for (j = 0, tmp2=0; j < nf; j++)
	{	
	  
      /*if (ds->xd[j] != 0) ds->yd[j] /= ds->xd[j];*/
	  if (collx[j] != 0) colly[j] /= collx[j];
	  
	  /*tmp2 += (dsi->yd[j] - ds->yd[j]) * (dsi->yd[j] - ds->yd[j]);*/
	  tmp2 += (dsi->yd[j] - colly[j]) * (dsi->yd[j] - colly[j]);
	  
	  /*dstmp->yd[j] = ds->yd[j];*/
	  /*transy[j] = colly[j];*/
	  ds->yd[j] = colly[j];
	  /*colly[j] = 0;
	    collx[j] = 0;*/
	}
      totalchi = sqrt(tmp2/nf);
      ratio = totalchi/no;
      out = win_printf("totalchi %f noise %f ratio %f",totalchi,no,ratio);
    }
  /*free(transy);*/
  free(collx);
  free(colly);
  return ds;
}

d_s *histogram(d_s *dsi, d_s *ds)
{
  register int i, j;
  int nf,nmin,nmax,nbin,place/*,total=0*/;
  static float bin = 0.1;
  float max=0,min=0,binmin=0, binmax=1/* a*/;
	
  nf = dsi->nx;	/* this is the number of points in the data set */
  i = win_scanf("What is the bin width? %f",&bin); /* don't forget the &*/
  if (i == WIN_CANCEL)	return OFF;
	
  max=dsi->yd[0];
  min=dsi->yd[0];
  for (j = 1; j < nf; j++)
    {
      if(max < dsi->yd[j]) max=dsi->yd[j];
      if(min > dsi->yd[j]) min=dsi->yd[j];
    }
  nmin =  (min/bin);
  nmax =  (max/bin);
  if (max > 0) nmax++;
  if (min < 0) nmin--;
  binmin = nmin * bin;
  binmax = nmax * bin;
  nbin = abs(nmax - nmin);
  if (nbin > ds->mx || nbin > ds->my)
    {
      ds = build_adjust_data_set(ds, nbin,nbin);
      if (ds == NULL)	win_printf("memory problem");
    }
  ds->nx = ds->ny = nbin;
  for (i = 0; i < nbin; i++)
    {
      ds->xd[i] = binmin + (bin/2) + i*bin;
      ds->yd[i] = 0;
    }
  for (j = 0; j < nf; j++)
    {
      place = (dsi->yd[j] - binmin)/bin;
      ++ds->yd[place];
    }
  /*for (i = 0; i < nbin; i++)
    {
    total += ds->yd[i];
    }*/
  /*win_printf("total %d nf %d,nbin %d, 
    min %f,max %f 
    binmin %f, binmax %f,
    nmin %d, nmax %d
    a %f",total,nf,nbin,min,max,binmin,binmax,nmin,nmax,a);*/
  return ds;
}

d_s *derivative(d_s *dsi, d_s *ds)
{
  register int j;
  int nf;
  float tmp;
  nf = dsi->nx;
  for (j = 1, tmp; j < nf; j++)
    {
      ds->yd[j-1] =  (dsi->yd[j] - dsi->yd[j-1]);
      /*ds->xd[j-1] =  (dsi->xd[j] + dsi->xd[j-1])/2;*/
      ds->xd[j-1] =   dsi->xd[j-1];
      tmp =  (dsi->xd[j] - dsi->xd[j-1]);
      if (tmp != 0) ds->yd[j-1] /= tmp;
      else win_printf("X values at point %d are bad", j);
    }
  ds->xd[nf-1] = dsi->xd[nf-1];
  ds->yd[nf-1] = ds->yd[nf-2];
  ds->nx = nf;
  ds->ny = nf;
  return ds;
}

d_s *threshold(d_s *dsraw, d_s *dsfilt, d_s *dst)
{
  register int i,j, k;
  static float min = -10, max = -0.005, uppermax = 10;
  int nf, nxi;
  nf = dsfilt->nx;	/* this is the number of points in the data set */
  nxi = dst->nx;
  i = win_scanf("Creates data with y = 1 for input data between the bounds"
		"and y = 0 otherwise. lower bound = %f"
		"upper bound = %f upper bound on raw data = %f",&min,&max, &uppermax); /* don't forget the &*/
  if (i == WIN_CANCEL)	return OFF;
  dst->yd[0] = 0;
  dst->xd[0] = dsfilt->xd[0];
  dst->yd[nf -1] = 0;
  dst->xd[nf- 1] = dsfilt->xd[nf - 1];
  for (j = 1; j < nf -1  && j < nxi -1 ; j++)
    {
      dst->xd[j] = dsfilt->xd[j];
      if (dsfilt->yd[j-1] > min && dsfilt->yd[j-1] < max && dsraw->yd[j-1] < uppermax) i = 1;
      else i = 0;
      if (dsfilt->yd[j] > min && dsfilt->yd[j] < max && dsraw->yd[j] < uppermax) k = 1;
      else k = 0;
      if (i == 1 || k == 1) dst->yd[j] = 1;
      else dst->yd[j] = 0;
    }
	
  return dst;
}

int poly_fit1(float *y, int nx, double *y0, double *y1)
{
  register int i, j;
  double tmp;
	
  if (nx%2 == 0)				return -1;
  j = (nx-1)/2;
  if (poly_init(j)) 	return -1;
  tmp = (double)1/nx;
  if (y0 != NULL)
    {
      for (i = 0, *y0 = 0; i < nx; i++)  *y0 += y[i];
      *y0 *= tmp;
    }
  if (y1 != NULL)
    {
      for (i = 0, *y1 = 0; i < nx; i++)  
	{
	  *y1 += y[i]*p1[i];
	}
      *y1 /= j;
    }
  return 0;
}

int poly_init(int m)
{
  static int m_points = 0;	
	
  if (m <= 0)			return -1;
  if (m == m_points)	return 0; 
  p1 = (double *)realloc(p1, (2*m+1)*sizeof(double));

  m_points = m;
  if (p1 == NULL )		return 1;
  init_p1(p1, m);
  return 0;
}

int init_p1(double *p1, int m)
{
  register int i, j;
  double s;
  	
  if (m <= 0)	return -1;
  for(i = -m, j = 0, s = 0; i <= m; i++, j++)
    {
      p1[j] = ((double)i)/m;
      s += p1[j]*p1[j];
    }
  for(j = 0; j <= 2*m; j++)	p1[j] /= s;		/* , s = sqrt(s) */
  return 0;
}

/** Main analysis function.

\author Omar Saleh
\author Adrien Meglio
*/
int do_analyze_in_op(void)
{
  register int i,j, k;
  O_p *op = NULL, *op_original = NULL, *opvvsontime = NULL, *opd = NULL,/* *oph,*/ *opp = NULL,/* *opph, */ *opvel = NULL, *optime = NULL, *opvpos = NULL, *opalign = NULL;
  d_s *dsi, *dsi_original = NULL, *dsvvsontime = NULL, *dsd, *dsf,/* *dsh,*/ *dst = NULL, /**dsb,*/ *dsp,/* *dsph,*/ *dstmp = NULL, *dstmp2 = NULL /* *dstc */, *ds1, *ds2,/* *dsh1, *dsh2, */ *dsvel, *dsvar, *dstimein, *dstimeout/*, *ds3, *ds3err*/;
  pltreg *pr = NULL, *pr_original = NULL;
  int nf,nb,nbp, l, m/*, nmin, nmax, nbin, place*/;
  /*static float bin = 0.1;*/
  /*float max = 0, min = 0, binmin = 0, binmax = 1;*/
  float timespan,eventfreq, meanv, var, lasttime;
  static int plotmarks = 1, plotderiv = 1, plotproc = 1, plottimes = 0, 
    plotvpos = 0, plotburstvel = 1, plotalign = 1, cleaning = 1;
  static int eventtype;
  static int vvsontime = 1;
	
  if(updating_menu_state != 0)	return D_O_K;	

  if (key[KEY_LSHIFT])
    return win_printf_OK("This routine does it all.");

  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr_original,&op_original,&dsi_original) != 3)
    return win_printf_OK("cannot find data");
		
  /* Then we copy the data to a new plot project */
  pr = create_and_register_new_plot_project(pr_original->def.x0,pr_original->def.y0,pr_original->def.w,pr_original->def.h);
  op = duplicate_plot_data(op_original,NULL);
  add_data_to_pltreg(pr,IS_ONE_PLOT,(void*)op);

  pr->o_p[0] = pr->o_p[1]; /* Replace empty op with actual one */
  pr->n_op=1;
  op = pr->o_p[pr->cur_op];
   dsi = op->dat[op->cur_dat];
  dsi->color = 5592405; /* Dark Gray */

  //return 0;

  i = win_scanf("Show which plots ? (1=Yes)\n"
		"Decorate input ?%d"
		"Derivative ?%d Processivity ?%d Times ?%d V vs. position ? %d V vs burst # ?%d"
		"Burst align ?%d"
		"Speed vs. on-time ?%d"
		"Type of event (0=jumps,1=linear)%d"
		"Clean events ? (0=No, 1=1-2 pts events, 2=advanced) %d",&plotmarks,&plotderiv,&plotproc,&plottimes,&plotvpos,
		&plotburstvel, &plotalign,&vvsontime,&eventtype,&cleaning);
  if (i == WIN_CANCEL)	return OFF;
	
  nf = dsi->nx;	/* this is the number of points in the data set */

  if (plotderiv == 1)
    {
      opd = create_and_attach_one_plot(pr, nf, nf, 0);
      dsd = opd->dat[0];
      dsf = create_and_attach_one_ds(opd, nf, nf, 0);
      dst = create_and_attach_one_ds(opd, nf, nf, 0);
    }
  else
    {
      dsd = build_data_set(nf,nf);
      dsd->nx = dsd->ny = nf;
      dsf = build_data_set(nf,nf);
      dsf->nx = dsf->ny = nf;
      //dst = create_and_attach_one_ds(opd, nf, nf, 0);
      dst = build_data_set(nf,nf);
      dst->nx = dst->ny = nf;
    }
        
  timespan = dsi->xd[nf-1] - dsi->xd[0];/* Finds the total time interval of the data*/
  dsd = derivative(dsi, dsd);/*finds the derivative of the raw data; adds a final point*/
  if (eventtype == 0)
    {
      dsf = jumpfit(dsd, dsf);/*performs the forw/backward filter*/
    }
  else if (eventtype == 1)
    {
      dsf =slopefilt(dsd, dsf);/*performs the forw/backward filter*/
    }
  else
    {
      return win_printf_OK("The options are either jumps (0) or linear events (1)"); 
    }
 
  dst = threshold(dsd, dsf, dst);/*creates a threshold data set with 1s at events, and 0s elsewhere*/

  dst->yd[0] = 0;
  dst->yd[nf-1] = 0;/* insures that the first and last data points are not included in events*/

  dst = cleanthresh(dsi, dst,cleaning);/*in thresholded data dst, removes 1s from dsi of length 1 and 2 points, then selects events based on processivity and std. dev.*/
  countbursts(dst,&nb,&nbp); 
  win_printf("nb = %d, nbp = %d", nb, nbp);
  if (nb == 0) return win_printf_OK("No Bursts Found");

  if (plotproc == 1)
    {
      opp = create_and_attach_one_plot(pr, nb, nb, 0);
      dsp = opp->dat[0];
    }
  else
    {
      dsp = build_data_set(nb,nb);
      dsp->nx = dsp->ny = nb;
    }    
  dsp = proc(dsi, dst, dsp, nb);/*calculates the processivity of each event, simple min max*/
 
  if(plotvpos == 1)
    {
      opvpos = create_and_attach_one_plot(pr, nbp, nbp, 0);
      ds1 = opvpos->dat[0];
      ds2 = create_and_attach_one_ds(opvpos, nbp, nbp, 0);
    }
  else
    {
      ds1 = build_data_set(nbp,nbp);
      ds1->nx = ds1->ny = nbp;
      ds2 = build_data_set(nbp,nbp);
      ds2->nx = ds2->ny = nbp;
    }
 
  for (i = 0,j = 0; i < nf; i++)/*this loop creates 2 new data sets which are subsets of the raw and filtered derivative data, and contain only those points corresponding to events*/
    {
      if (dst->yd[i] == 1)
	{
	  ds1->yd[j] = dsf->yd[i];
	  ds2->yd[j] = dsd->yd[i];
	  ds1->xd[j] = dsi->yd[i];
	  ds2->xd[j] = dsi->yd[i];
	  j++;
	}
    }
 	
  if (plotburstvel == 1)
    {
      opvel = create_and_attach_one_plot(pr, nb, nb, 0);
      dsvel = opvel->dat[0];
      dsvar = create_and_attach_one_ds(opvel, nb, nb, 0);
    }
  else
    {
      dsvel = build_data_set(nb,nb);
      dsvel->nx = dsvel->ny = nb;
      dsvar = build_data_set(nb,nb);
      dsvar->nx = dsvar->ny = nb;
    }        
  if (plottimes == 1)
    {
      optime = create_and_attach_one_plot(pr, nb, nb, 0);
      dstimein = optime->dat[0];
      dstimeout = create_and_attach_one_ds(optime, nb, nb, 0);
    }
  else
    {
      dstimein = build_data_set(nb,nb);
      dstimein->nx = dstimein->ny = nb;
      dstimeout = build_data_set(nb,nb);
      dstimeout->nx = dstimeout->ny = nb;
    }
        
  if (vvsontime == 1)
    {
      opvvsontime = create_and_attach_one_plot(pr,nb,nb,0);
      dsvvsontime = opvvsontime->dat[0];
    }
  else
    {
      dsvvsontime = build_data_set(nb,nb);
      dsvvsontime->nx = dsvvsontime->ny = nb;
    }
      
  lasttime = dsi->xd[0];
  if (plotalign == 1)	opalign = create_and_attach_one_plot(pr, nbp, nbp, 0);
	
  for (i = 1, k = 0; i < nf; i++)/*this loop creates a new data set for each event, and plots it*/
    {
		
      if ( dst->yd[i-1] == 0 && dst->yd[i] == 1)
	{
	  if (plotmarks == 1) dstmp = create_and_attach_one_ds(op, 2, 2, 0);
	  else
	    {
	      dstmp = build_data_set(2,2);
	      dstmp->nx = dstmp->ny = 2;
	    }
	  dstmp->xd[0] = dsi->xd[i];
	  dstmp->yd[0] = dsi->yd[i];
	  for (j = i; dst->yd[j] == 1; j++) {};
	  dstmp->xd[1] = dsi->xd[j-1];
	  dstmp->yd[1] = dsi->yd[j-1];
			
	  if (plotalign == 1) 
	    {
	      if (k == 0) dstmp2 = opalign->dat[0];
	      else dstmp2 = create_and_attach_one_ds(opalign, j-1, j-1, 0);
	    }
	  else
	    {
	      dstmp2 = build_data_set(j-1,j-1);
	      dstmp2->nx = dstmp2->ny = j-1;
	    }
	  for (m = 0; m < j - i ; m++)
	    {
	      dstmp2->xd[m] = dsi->xd[i+m] - dsi->xd[i];
	      dstmp2->yd[m] = dsi->yd[i+m];
	    }
	  dstmp2->nx = m;
	  dstmp2->ny = m;
			
	  meanv = (dstmp->yd[1] - dstmp->yd[0])/(dstmp->xd[1] - dstmp->xd[0]);
	  for(var = 0, l = i+1; l < j; l++)
	    {
	      var +=pow(((dsi->yd[l] - dsi->yd[l-1])/(dsi->xd[l] - dsi->xd[l-1])) - meanv,2);
	    }
	  var /= j - i - 1;
	  dsvar->xd[k] = dstmp->xd[0];
	  dsvar->yd[k] = sqrt(var);
	  dsvel->xd[k] = dstmp->xd[0];
	  dsvel->yd[k] = meanv;
	  dsvvsontime->xd[k] = dstmp->xd[1] - dstmp->xd[0];
	  dsvvsontime->yd[k] = meanv;
	  dstimein->yd[k] = dstmp->xd[1] - dstmp->xd[0];
	  dstimein->xd[k] = dstmp->xd[0];
	  dstimeout->xd[k] = dstmp->xd[0];
	  dstimeout->yd[k] = dstmp->xd[0] - lasttime;
	  lasttime = dstmp->xd[1];
	  dstmp->source = my_sprintf(dstmp->source,"Omar burst %d", k++);
	  i = j;
	}
    }
  eventfreq = nb / timespan;/*calculates the frequency of events*/
  win_printf("# of bursts %d"
	     " timespan %f"
	     " event frequency %f",nb, timespan, eventfreq);
  /* now we must do some house keeping */
  inherit_from_ds_to_ds(dsd, dsi);
  inherit_from_ds_to_ds(dsf, dsi);
  inherit_from_ds_to_ds(dst, dsi);
  inherit_from_ds_to_ds(dsp, dsi);
  inherit_from_ds_to_ds(dsvel, dsi);
  inherit_from_ds_to_ds(dsvar, dsi);
  inherit_from_ds_to_ds(dstimein, dsi);
  inherit_from_ds_to_ds(dstimeout, dsi);
  inherit_from_ds_to_ds(dsvvsontime, dsi);
  refresh_plot(pr, UNCHANGED);		
  switch_plot2(0);
  switch_data_set(0);
  if (plotderiv != 1)
    {
      free_data_set(dsd);
      free_data_set(dsf);
      free_data_set(dst);
    }
  else
    {
      set_plot_title(opd, "\\stack{{Derivative}{%s}}",(op->title != NULL) ? op->title :"undefined");
      if (op->x_title != NULL) set_plot_x_title(opd, op->x_title);
      opd->filename = strdup("deriv.gr");
    }
  if (plotmarks != 1) free_data_set(dstmp);
  if (plotproc != 1) free_data_set(dsp);
  else
    {
      set_plot_title(opp, "\\stack{{Processivities}{%s}}",(op->title != NULL) ? op->title :"undefined");
      set_plot_x_title(opp, "Occurence time");
      set_plot_y_title(opp, "burst length, \\mu m");
      opp->filename = strdup("proc.gr");
    }
  if (plottimes != 1)
    {
      free_data_set(dstimein);
      free_data_set(dstimeout);
    }
  else
    {
      set_plot_title(optime, "\\stack{{Time in and between events}{%s}}",(op->title != NULL) ? op->title :"undefined");
      set_plot_x_title(optime, "Occurence time");
      set_plot_y_title(optime, "Time (s)");
      optime->filename = strdup("times.gr");
    }
  if (plotvpos != 1)
    {
      free_data_set(ds1);
      free_data_set(ds2);
    }
  else
    {
      set_plot_title(opvpos, "\\stack{{Velocity vs. Position}{%s}}",(op->title != NULL) ? op->title :"undefined");
      set_plot_x_title(opvpos, "Position, \\mu m");
      set_plot_y_title(opvpos, "Velocity, \\mu m/s");
      opvpos->filename = strdup("vbypos.gr");
    }
  if (plotburstvel != 1)
    {
      free_data_set(dsvel);
      free_data_set(dsvar);
    }
  else
    {
      set_plot_title(opvel, "\\stack{{V and stdev(V) by burst}{%s}}",(op->title != NULL) ? op->title :"undefined");
      set_plot_x_title(opvel, "Occurence time");
      set_plot_y_title(opvel, "Velocity, \\mu m/s");
      opvel->filename = strdup("vbyburst.gr");
    }
  if (vvsontime != 1)
    {
      free_data_set(dsvvsontime);
    }
  else
    {
      set_plot_title(opvvsontime, "\\stack{{V vs. processivity}{%s}}",(op->title != NULL) ? op->title :"undefined");
      set_plot_x_title(opvvsontime, "On Time (frames)");
      set_plot_y_title(opvvsontime, "Speed (\\mu m/s)");
      opvvsontime->filename = strdup("vvsontime.gr");
    }
  if (plotalign != 1) free_data_set(dstmp2);
  else
    {
      set_plot_title(opalign, "\\stack{{Burst alignment}{%s}}",(op->title != NULL) ? op->title :"undefined");
      opalign->filename = strdup("align.gr");
    }

  return 0;
}

int do_slopefilt_in_ds(void)
{
  register int i;
  O_p *op;
  d_s *ds, *dsi, *dstrans = NULL;
  pltreg *pr;
  int nf, n_op;
  static int repeat=1;

  if(updating_menu_state != 0)	return D_O_K;	
	
  if (key[KEY_LSHIFT])
    return win_printf_OK("This routine multiply the y coordinate\n of a data set by a number");
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
	
  nf = dsi->nx;	/* this is the number of points in the data set */
  n_op = pr->cur_op;
	
  i = win_scanf("repeat filter how many times? %d",&repeat); /* don't forget the &*/
	
  if (i == WIN_CANCEL)	return OFF;
	
  if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");

  dstrans = build_data_set(nf,nf);
  dstrans->nx = dstrans->ny = nf;
	
  dstrans = slopefilt(dsi, dstrans);
	
  for (i = 1; i < repeat; i++) dstrans = slopefilt(dstrans, dstrans);
	
  for (i = 0; i < nf; i ++)
    {
      ds->xd[i] = dstrans->xd[i];
      ds->yd[i] = dstrans->yd[i];
    }
	   
	
  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
  free_data_set(dstrans);
	
  /* refisplay the entire plot */
  refresh_plot(pr, n_op);		
  switch_plot2(0);
  switch_data_set(0);
  return 0;
}

int do_jumpfit2_in_ds(void)
{
  O_p *op;
  d_s *ds, *dsi;
  pltreg *pr;
  int nf, n_op;

  if(updating_menu_state != 0)	return D_O_K;	
	
  if (key[KEY_LSHIFT])
    return win_printf_OK("This routine multiply the y coordinate\n of a data set by a number");
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
	
  nf = dsi->nx;	/* this is the number of points in the data set */
  n_op = pr->cur_op;
	
	
  if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
	
  ds = jumpfit(dsi, ds);
	
  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
	
  /* refisplay the entire plot */
  refresh_plot(pr, n_op);		
  switch_plot2(0);
  switch_data_set(0);
  return 0;
}

int do_slidefit_in_ds(void)
{
  register int i, j, k;
  O_p *op, *opc, *opm;
  d_s *ds, *dsi, *dsc, *dsm;
  pltreg *pr;
  int nf, n_op, out;
  static float no = 0.01;
  double y0, y1, chi2, chi2n, tmp, totalchi, tmp2, tmp3, ratio;
  double weight=0;
  static int width = 16, w = 8, pt_index = -1;
  static int p=1, c=2, weightchoice=1, f=1, delta=1;

  if(updating_menu_state != 0)	return D_O_K;	
  if (key[KEY_LSHIFT])
    return win_printf_OK("This routine multiply the y coordinate\n of a data set by a number");
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
	
  nf = dsi->nx;	/* this is the number of points in the data set */
  n_op = pr->cur_op;
  i = win_scanf("Weighting by exp(-\\chi^2/\\sigma^2) [0] or (\\chi^2/\\sigma^2)^{-p} [1]? %d"
		"Expect flat events [0] or sloped events [1]? %d"
		"Define the bounds on the # points used per fit w= (2m + 1):"
		"max m= %d min m=%d Increment (i.e. 1,2,3... vs. 1,5,9..) %d the sigma of noise= %f"
		"the weighting power p= %d pt to dislay (-1 -> no display) %d",&weightchoice,&f,&w,&c,&delta,&no,&p,&pt_index); /* don't forget the &*/
	
  if (i == WIN_CANCEL)	return OFF;
	
  if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
	
  if ((opc = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  dsc = opc->dat[0];
	
  if ((opm = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  dsm = opm->dat[0]; 
	
  if (c>w) win_printf("YOU SCREWED UP: Min width > Max width!!!");
  for (k = c; k <= w; k = k + delta)
    {
      for (j = 0, width = 2*k +1; j < nf-width+1; j++)
	{
	  if (poly_fit1(dsi->yd+j, width, &y0, &y1) < 0)
	    win_printf("fit pb");
	  for (i = 0, chi2 = 0; i < width; i++)
	    {
	      tmp = y0 + f*y1*(i-k) - dsi->yd[j+i];
	      chi2 += tmp * tmp;
	    }			
	  chi2n = chi2 / ((width-2)*no*no);
	  dsc->yd[j+k] += chi2n;
	  if (weightchoice)
	    {
	      if (chi2n != 0)
		{
		  weight = pow(chi2n,(double)(-p));
		  /*out = win_printf("weight = %g chi2n %g p  %d k %d j %d",weight,chi2n,p,k,j);*/
		  if (weight > 1e30) weight=1e30;
		}
	      else
		{
		  weight = 1;
		}
	    }
	  else
	    {
	      weight = exp(-chi2n);
	    }
	  for (i = 0; i < width; i++)
	    {
	      if ((i+j) == pt_index)
		{
		  out = win_printf("width = %d \\chi^2n %f\nweight %f j %d",
				   width,chi2n,weight,j);
		  if (out == WIN_CANCEL) pt_index = -1;
		}
	      ds->yd[j+i] += weight*(y0 + f*y1*(i-k));
	      ds->xd[j+i] += weight;
	      dsm->yd[j+i] += weight*width;
	      dsm->xd[j+i] += weight;				
	    }
	}
    }
  for (j = 0, tmp2=0,tmp3=0; j < nf; j++)
    {	
      /*out = win_printf("w-width %g weight %g j %d",dsm->yd[j],dsm->xd[j],j);*/
      if (ds->xd[j] != 0) ds->yd[j] /= ds->xd[j];	
      if (dsm->xd[j] != 0) dsm->yd[j] /= dsm->xd[j];	
      dsm->xd[j] = dsc->xd[j] = ds->xd[j] = dsi->xd[j];
      tmp2 = (dsi->yd[j] - ds->yd[j]) * (dsi->yd[j] - ds->yd[j]);
      dsc->yd[j] = tmp2;
      tmp3 += tmp2;
    }
  totalchi = sqrt(tmp2/nf);
  ratio = totalchi/no;
  out = win_printf("totalchi %f noise %f ratio %f",totalchi,no,ratio);
  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
  ds->treatement = my_sprintf(ds->treatement,"fit over %d",width);
  dsc->treatement = my_sprintf(dsc->treatement,"fit over %d",width);
  dsm->treatement = my_sprintf(dsm->treatement,"fit over %d",width);
	
  set_plot_y_title(opc, "\\chi^2");
  set_plot_y_title(opm, "width");
	
  /* refisplay the entire plot */
  refresh_plot(pr, n_op);		
  switch_plot2(0);
  switch_data_set(0);
  return 0;
}

int do_histo_in_op(void)
{
  O_p *op, *opn;
  d_s *ds, *dsi;
  pltreg *pr;
  int nf;

  if(updating_menu_state != 0)	return D_O_K;	
  if (key[KEY_LSHIFT])
    return win_printf_OK("This routine multiply the y coordinate\n of a data set by a number and place it in a new plot");
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
	
  nf = dsi->nx;	/* this is the number of points in the data set */
	
  if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  ds = opn->dat[0];
  ds = histogram(dsi, ds);
  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
  set_plot_title(opn, "Histogram");
  if (op->y_title != NULL) set_plot_x_title(opn, op->y_title);
  set_plot_y_title(opn, "# of counts");
  opn->filename = Transfer_filename(op->filename);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);		
  switch_plot2(0);
  switch_data_set(0);
  return 0;
}

int do_deriv_in_op(void)
{
  O_p *op, *opn;
  d_s *ds, *dsi;
  pltreg *pr;
  int nf;

  if(updating_menu_state != 0)	return D_O_K;	
  if (key[KEY_LSHIFT])
    return win_printf_OK("This routine multiply the y coordinate\n of a data set by a number and place it in a new plot");
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
	
  nf = dsi->nx;	/* this is the number of points in the data set */
	
  if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  ds = opn->dat[0];
  ds = derivative(dsi, ds);
	
  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
  set_plot_title(opn, "Derivative");
  if (op->x_title != NULL) set_plot_x_title(opn, op->x_title);
  set_plot_y_title(opn, "\\u m/s");
  opn->filename = Transfer_filename(op->filename);
  /*uns_op_2_op(opn, op);*/
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);		
  switch_plot2(0);
  switch_data_set(0);
  return 0;
}

int do_thresh_in_ds(void)
{
  O_p *op;
  d_s *ds, *dsi;
  pltreg *pr;
  int nf;

  if(updating_menu_state != 0)	return D_O_K;	
  if (key[KEY_LSHIFT])
    return win_printf_OK("This routine multiply the y coordinate\n of a data set by a number");
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  nf = dsi->nx;
  if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
	
  ds = threshold(dsi,dsi,ds);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);		
  switch_plot2(0);
  switch_data_set(0);
  return 0;
}

int do_linefit_in_ds(void)
{
  O_p *op;
  d_s *ds, *dsi;
  pltreg *pr;
  float a=0,b=0;
  int nf;
  register int i;

  if(updating_menu_state != 0)	return D_O_K;	
  if (key[KEY_LSHIFT])
    return win_printf_OK("This routine multiply the y coordinate\n of a data set by a number");
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  nf = dsi->nx;
  if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
	
  fit_line(dsi, &a, &b);
  win_printf("a = %f, b = %f", a, b);
  /*a *= (op->dy)/(op->dx);
    b *= op->dy;*/
  for (i = 0; i < nf; i++)
    {
      ds->xd[i] = dsi->xd[i];
      ds->yd[i] = a * dsi->xd[i] + b;
    }
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);		
  switch_plot2(0);
  switch_data_set(0);
  return 0;
}

int do_steps_in_ds(void)
{
  register int i, j;
  O_p *op;
  d_s *ds, *dsi;
  pltreg *pr;
  int nf;
  static float amp = 1;
  float tmp,tmp2;

  if(updating_menu_state != 0)	return D_O_K;	
  if (key[KEY_LSHIFT])
    return win_printf_OK("This routine multiply the y coordinate\n of a data set by a number");
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
	
  nf = dsi->nx;	/* this is the number of points in the data set */
  i = win_scanf("This funny program will create a data set "
		"of x length and values equal to input data,"
		" and with y data =0 except for a few "
		"pulses of width 3,7,15,31,63,127"
		" and amplitude= %f",&amp); /* don't forget the &*/
  if (i == WIN_CANCEL)	return OFF;
	
  if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");

  for (j = 0; j < nf; j++)
    {
      ds->xd[j] = dsi->xd[j];
      for (i = 2; i < 8; i++)
	{
	  tmp = pow(2,i);
	  tmp2 = (i-1)*(nf/7);
	  tmp += tmp2;
	  if (j > tmp2) 
	    {
	      if (j < tmp) ds->yd[j]=amp;
	    }
	}
    }
	
  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
  ds->treatement = my_sprintf(ds->treatement,"pulses");
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);		
  switch_plot2(0);
  switch_data_set(0);
  return 0;
}

int do_constantzr_in_op(void)
{
  register int j;
  O_p *op, *opn;
  d_s *ds = NULL, *dsi;
  pltreg *pr;
  int nf,nd;
  int zrot[256][2];

  if(updating_menu_state != 0)	return D_O_K;	
  if (key[KEY_LSHIFT])
    return win_printf_OK("Sorts l vs t scans into separate datasets for each change of zmag or rot");

  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
	
  nf = dsi->nx;	/* this is the number of points in the data set */
  /*i = win_scanf("");  don't forget the &*/
  /*if (i == WIN_CANCEL)	return OFF;*/
	

  zrot[0][0] = 0;/* the zrot matrix tracks changes in the zmag and rot values*/
	
  for (j = 1,nd=0; j < nf - 1; j++)
    {/*steps through the input zmag and rot values to find changes*/
      if (op->dat[0]->yd[j] == op->dat[0]->yd[j-1] && op->dat[1]->yd[j] == op->dat[1]->yd[j-1])
	{
	  zrot[nd][1] = j;
	}
      else
	{
	  if (op->dat[0]->yd[j+1]==op->dat[0]->yd[j] && op->dat[1]->yd[j+1]==op->dat[1]->yd[j])
	    {
	      ++nd;
	      zrot[nd][0] = j;
	    }
	}
    }
  zrot[nd][1] = nf - 1;
  opn = create_and_attach_one_plot(pr,nd+1,nd+1,0);
  ds = build_data_set(nd+1,nd+1);
  ds->nx = nd+1;
  ds->ny = nd+1;
  ds = opn->dat[0];
  for (j = 0; j <= nd; j++)
    {
      ds->xd[j] = j;
      ds->yd[j] = zrot[j][1] - zrot[j][0];
    }
	
  /* redisplay the entire plot */
  refresh_plot(pr, UNCHANGED);		
  switch_plot2(0);
  switch_data_set(0);
  return 0;
}

int do_sortdz_in_op(void)
{
  register int i,j;
  O_p *op, *opn;
  d_s *ds = NULL, *dsi;
  pltreg *pr;
  int nf,nd;
  static int min = 0;
  int zrot[256][2];

  if(updating_menu_state != 0)	return D_O_K;	
  if (key[KEY_LSHIFT])
    return win_printf_OK("Sorts l vs t scans into separate datasets for each change of zmag or rot");
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
	
  nf = dsi->nx;	/* this is the number of points in the data set */
  i = win_scanf("Sorts a normal dz scan (0-zmag,1-rot,2-dz)"
		"min # data points/data set = %d", &min); /* don't forget the &*/
  if (i == WIN_CANCEL)	return OFF;
	

  zrot[0][0] = 0;/* the zrot matrix tracks changes in the zmag and rot values*/
	
  for (j = 1,nd=0; j < nf - 1; j++)
    {/*steps through the input zmag and rot values to find changes*/
      if (op->dat[0]->yd[j] == op->dat[0]->yd[j-1] && op->dat[1]->yd[j] == op->dat[1]->yd[j-1])
	{
	  zrot[nd][1] = j;
	}
      else
	{
	  if (op->dat[0]->yd[j+1]==op->dat[0]->yd[j] && op->dat[1]->yd[j+1]==op->dat[1]->yd[j])
	    {
	      ++nd;
	      zrot[nd][0] = j;
	    }
	}
    }
  zrot[nd][1] = nf - 1;
  for (j = 0; j <= nd; j++)
    {/*this loop does the actual sorting*/
      /* successively attaches opn to the current window, defines correct size*/
      if(zrot[j][1] - zrot[j][0] >= min)
	{
	  opn = create_and_attach_one_plot(pr,1+zrot[j][1]-zrot[j][0],1+zrot[j][1]-zrot[j][0],0);
	  ds =  build_data_set(1+zrot[j][1]-zrot[j][0],1+zrot[j][1]-zrot[j][0]);
	  ds->nx =  1+zrot[j][1]-zrot[j][0];
	  ds->ny =  1+zrot[j][1]-zrot[j][0];
	  ds = opn->dat[0];
	  for (i = zrot[j][0]; i <= zrot[j][1]; i++)
	    {/*sets ds1,2,3 equal to the proper slice of input data*/
	      ds->yd[i-zrot[j][0]] = op->dat[2]->yd[i];
	      ds->xd[i-zrot[j][0]] = op->dat[2]->xd[i];
	    }
		
	  set_plot_title(opn, "Zmag = %f, rot = %f",op->dat[0]->yd[zrot[j][1]],op->dat[1]->yd[zrot[j][1]]);
	  if (op->x_title != NULL) set_plot_x_title(opn, op->x_title);
	  set_plot_y_title(opn, "Bead Tracking");
	  /* now we must do some house keeping */
	  inherit_from_ds_to_ds(ds, op->dat[2]);
	  opn->filename = Transfer_filename(op->filename);
	  uns_op_2_op(opn, op);
	}
		
    }
	
  /* redisplay the entire plot */
  refresh_plot(pr, UNCHANGED);		
  switch_plot2(0);
  switch_data_set(0);
  return 0;
}

int do_sorttrac_in_op(void)
{
  register int i,j;
  O_p *op, *opn;
  d_s *ds0 = NULL, *ds1 = NULL, *ds2 = NULL, *dsi;
  pltreg *pr;
  int nf,nd, size;
  static int min = 0;
  int zrot[256][2];

  if(updating_menu_state != 0)	return D_O_K;	
  if (key[KEY_LSHIFT])
    return win_printf_OK("Sorts l vs t scans into separate datasets for each change of zmag or rot");
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
	
  nf = dsi->nx;	/* this is the number of points in the data set */
  i = win_scanf("Sorts a traca or trac b scan \n ds0,1,2=x,y,z, ds3=zmag, ds4=rot \n"
		"min # data points/data set = %d", &min); /* don't forget the &*/
  if (i == WIN_CANCEL)	return OFF;
	

  zrot[0][0] = 0;/* the zrot matrix tracks changes in the zmag and rot values*/
	
  for (j = 1,nd=0; j < nf - 1; j++)
    {/*steps through the input zmag and rot values to find changes*/
      if (op->dat[3]->yd[j] == op->dat[3]->yd[j-1] && op->dat[4]->yd[j] == op->dat[4]->yd[j-1])
	{
	  zrot[nd][1] = j;
	}
      else
	{
	  if (op->dat[3]->yd[j+1]==op->dat[3]->yd[j] && op->dat[4]->yd[j+1]==op->dat[4]->yd[j])
	    {
	      ++nd;
	      zrot[nd][0] = j;
	    }
	}
    }
  zrot[nd][1] = nf - 1;
  for (j = 0; j <= nd; j++)
    {/*this loop does the actual sorting*/
      /* successively attaches opn to the current window, defines correct size*/
      if(zrot[j][1] - zrot[j][0] >= min)
	{
	  size = 1 + zrot[j][1] - zrot[j][0];
	  opn = create_and_attach_one_plot(pr,size,size,0);
	  ds0 =  build_data_set(size,size);
	  ds0->nx =  size;
	  ds0->ny =  size;
	  ds0 = opn->dat[0];
	  ds1 = create_and_attach_one_ds(opn, size, size, 0);
	  ds2 = create_and_attach_one_ds(opn, size, size, 0);
	  for (i = zrot[j][0]; i <= zrot[j][1]; i++)
	    {/*sets ds0,1,2 equal to the proper slice of input data*/
	      ds0->yd[i-zrot[j][0]] = op->dat[0]->yd[i];
	      ds0->xd[i-zrot[j][0]] = op->dat[0]->xd[i];
	      ds1->yd[i-zrot[j][0]] = op->dat[1]->yd[i];
	      ds1->xd[i-zrot[j][0]] = op->dat[1]->xd[i];
	      ds2->yd[i-zrot[j][0]] = op->dat[2]->yd[i];
	      ds2->xd[i-zrot[j][0]] = op->dat[2]->xd[i];
	    }
		
	  set_plot_title(opn, "Zmag = %f, rot = %f",op->dat[3]->yd[zrot[j][1]],op->dat[4]->yd[zrot[j][1]]);
	  if (op->x_title != NULL) set_plot_x_title(opn, op->x_title);
	  if (op->y_title != NULL) set_plot_y_title(opn, op->y_title);
	  /* now we must do some house keeping */
	  inherit_from_ds_to_ds(ds0, op->dat[0]);
	  inherit_from_ds_to_ds(ds1, op->dat[1]);
	  inherit_from_ds_to_ds(ds2, op->dat[2]);
	  opn->filename = Transfer_filename(op->filename);
	  uns_op_2_op(opn, op);
	}
		
    }
	
  /* redisplay the entire plot */
  refresh_plot(pr, UNCHANGED);		
  switch_plot2(0);
  switch_data_set(0);
  return 0;
}

int do_slice_in_ds(void)
{
  register int i, j;
  O_p *op;
  d_s *ds, *dsi;
  pltreg *pr;
  int nf,ns;
  static int index, choice;

  if(updating_menu_state != 0)	return D_O_K;	
  if (key[KEY_LSHIFT])
    return win_printf_OK("This routine multiply the y coordinate\n of a data set by a number");
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
	
  nf = dsi->nx;	/* this is the number of points in the data set */
  i = win_scanf("remove points from index %d to start (0) or end (1) %d",&index,&choice); /* don't forget the &*/
  if (i == WIN_CANCEL)	return OFF;
	
  if (choice == 0)
    {
      ns = nf - index;
      ds = create_and_attach_one_ds(op, ns, ns, 0);
      for (j = 0; j < ns; j++)
	{
	  ds->yd[j] = dsi->yd[j+index];
	  ds->xd[j] = dsi->xd[j+index];
	}
    }
  else
    {
      ns = index;
      ds = create_and_attach_one_ds(op, ns, ns, 0);
      for (j = 0; j < ns; j++)
	{
	  ds->yd[j] = dsi->yd[j];
	  ds->xd[j] = dsi->xd[j];
	}
    }
  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);		
  switch_plot2(0);
  switch_data_set(0);
  return 0;
}

int do_meanp_in_ds(void)
{
  register int i, j;
  O_p *op;
  d_s *dsi;
  pltreg *pr;
  int nf;
  char st[1024];
  float proc, sig, tmp, under, over, total;
  static float lone=0.1, ltwo=1;

  if(updating_menu_state != 0)	return D_O_K;	
  if (key[KEY_LSHIFT])
    return win_printf_OK("This routine multiply the y coordinate\n of a data set by a number");
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
	
  nf = dsi->nx;	/* this is the number of points in the data set */
  i = win_scanf("Given a badly distributed set of procs, I find the mean proc."
		"l(1) = %f l(2) = %f",&lone,&ltwo); /* don't forget the &*/
  if (i == WIN_CANCEL)	return OFF;
	
  for (under = 0, over = 0, j = 0; j < nf; j++)
    {
      if(dsi->yd[j] > lone && dsi->yd[j] < ltwo) ++under;
      if(dsi->yd[j] >= ltwo) ++over;
    }
  tmp = (under + over)/over;
  tmp = log(tmp);
  proc = (ltwo - lone)/tmp;
  sig = proc*sqrt( (1/(over + under)) + (1/over) ) / tmp;
  /*total = over * proc*exp(ltwo/proc);*/
  total = under+over;
	
  sprintf(st,"\\pt8\\fbox{\\stack{{<proc> = %f}{sig(proc) = %f}{Total# = %.0f}{#>l2 = %.0f, #>l1,<l2 = %.0f}}}",proc, sig, total, over, under);
  push_plot_label(op, op->x_lo + (op->x_hi - op->x_lo)/4, op->y_hi - (op->y_hi - op->y_lo)/4, st, USR_COORD);
	
  /* now we must do some house keeping */
	
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);		
  switch_plot2(0);
  switch_data_set(0);
  return 0;
}

int do_int_in_op(void)
{
  register int i, j;
  O_p *op, *opn;
  d_s *ds, *dsi, *dsig;
  pltreg *pr;
  float nt;
  static float shift = 0;
  int nf;

  if(updating_menu_state != 0)	return D_O_K;	
  if (key[KEY_LSHIFT])
    return win_printf_OK("This routine multiply the y coordinate\n of a data set by a number and place it in a new plot");
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
	
  nf = dsi->nx;	/* this is the number of points in the data set */
  i = win_scanf("Input an UNNORMALIZED histogram, shift x by %f", &shift); /* don't forget the &*/
  if (i == WIN_CANCEL)	return OFF;
	
  if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  ds = opn->dat[0];
  dsig = create_and_attach_one_ds(opn, nf, nf, 0);
  for (j = 0, nt = 0; j < nf; j++) nt += dsi->yd[j];
  ds->yd[nf-1] = (1/nt)*dsi->yd[nf-1];
  ds->xd[nf-1] = dsi->xd[nf-1] + shift;
  dsig->yd[nf-1] = (1/nt)*sqrt(nt*ds->yd[nf-1] + 1);
  dsig->xd[nf-1] = dsi->xd[nf-1] + shift;
  for (j = nf - 2; j >= 0; j--)
    {
      ds->yd[j] = ds->yd[j+1] + (1/nt)*dsi->yd[j];
      ds->xd[j] = dsi->xd[j] + shift;
      dsig->xd[j] = dsi->xd[j] + shift;
      dsig->yd[j] = (1/nt)*sqrt(nt*ds->yd[j] + 1);
    }
	
  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
  inherit_from_ds_to_ds(ds, dsig);
  ds->treatement = my_sprintf(ds->treatement,"integrated histo");
  dsig->treatement = my_sprintf(dsig->treatement,"error in integrated histo");
  set_plot_title(opn, "Integrated histogram");
  if (op->x_title != NULL) set_plot_x_title(opn, op->x_title);
  if (op->y_title != NULL) set_plot_y_title(opn, op->y_title);
  opn->filename = Transfer_filename(op->filename);
  uns_op_2_op(opn, op);	
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);		
  switch_plot2(0);
  switch_data_set(0);
  return 0;
}

MENU *omar2_plot_menu(void)
{
  static MENU mn[32], stupid_submenu[32];


  if (mn[0].text != NULL)	return mn;

  add_item_to_menu(stupid_submenu,"<proc>",do_meanp_in_ds,NULL,0,NULL);
  add_item_to_menu(stupid_submenu,"Int histo",do_int_in_op,NULL,0,NULL);
  add_item_to_menu(mn,"proc", NULL,stupid_submenu,0,NULL);
	

  add_item_to_menu(mn,"Power",do_pow_in_ds,NULL,0,NULL);
  add_item_to_menu(mn,"sliding fit filter",do_slidefit_in_ds,NULL,0,NULL);
  add_item_to_menu(mn,"linear predictor f/b filter",do_slopefilt_in_ds,NULL,0,NULL);
  add_item_to_menu(mn,"mean predictor f/b filter",do_jumpfit2_in_ds,NULL,0,NULL);
  add_item_to_menu(mn,"Histogram",do_histo_in_op,NULL,0,NULL);
  add_item_to_menu(mn,"Derivative",do_deriv_in_op,NULL,0,NULL);
  add_item_to_menu(mn,"Threshold",do_thresh_in_ds,NULL,0,NULL);
  add_item_to_menu(mn,"Fit Line",do_linefit_in_ds,NULL,0,NULL);
  add_item_to_menu(mn,"Create Pulses",do_steps_in_ds,NULL,0,NULL);
  add_item_to_menu(mn,"Constant Z,R",do_constantzr_in_op,NULL,0,NULL);
  add_item_to_menu(mn,"Sort dz",do_sortdz_in_op,NULL,0,NULL);
  add_item_to_menu(mn,"Sort trac",do_sorttrac_in_op,NULL,0,NULL);
  add_item_to_menu(mn,"Slice",do_slice_in_ds,NULL,0,NULL);
  add_item_to_menu(mn,"Analyze",do_analyze_in_op,NULL,0,NULL);
  add_item_to_menu(mn,"mean predictor f/b filter 2",do_filter_nl,NULL,0,NULL);	
  add_item_to_menu(mn,"mean predictor f/b filter ex",do_filter_nl_ex,NULL,0,NULL);	
  add_item_to_menu(mn,"f/b jump/slope filter ",do_filter_nl_slope_and_jump,NULL,0,NULL);	


	
  return mn;
}

int omar2_main(int argc, char **argv)
{
  add_plot_treat_menu_item ( "omar2", NULL, omar2_plot_menu(), 0, NULL);
  return D_O_K;
}
#endif
