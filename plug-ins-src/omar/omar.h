#ifndef _OMAR2_H_
#define _OMAR2_H_


PXV_FUNC(MENU*, omar2_plot_menu, (void));

PXV_FUNC(int, 	poly_init, (int m));
PXV_FUNC(int,	init_p1, (double *p1, int m));

PXV_FUNC(int,	poly_fit1, (float *y, int nx, double *y0, double *y1));
PXV_FUNC(d_s*,  threshold, (d_s *dsraw, d_s *dsfilt, d_s *dst));
PXV_FUNC(d_s*, derivative, (d_s *dsi, d_s *ds));
PXV_FUNC(d_s*, histogram, (d_s *dsi, d_s *ds));
PXV_FUNC(d_s*, jumpfit, (d_s *dsi, d_s *ds));

PXV_FUNC(d_s*, jumpfit_vc_2_exclude_edge,(d_s *dsi, d_s *ds, int repeat, int max, int ex_size, float sigm, float p, double *chi2t));
PXV_FUNC(d_s*, cleanthresh, (d_s *dsi, d_s *dst, int cleaning));
PXV_FUNC(int,	countbursts, (d_s *dsi, int *nb, int *nbp));
PXV_FUNC(d_s*, proc, (d_s *dsi, d_s *dstc, d_s *dsp, int nb));
PXV_FUNC(int, 	fit_line, (d_s *dsi, float *a, float *b));



PXV_FUNC(int, fit_line, (d_s *dsi, float *a, float *b));
PXV_FUNC(int, omar2_main, (int argc, char **argv));
#endif
