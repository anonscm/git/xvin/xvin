/*
*    Plug-in program for image treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _MOVIE_PHASE_C_
#define _MOVIE_PHASE_C_

# include "allegro.h"
# include "xvin.h"

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "movie_phase.h"
//place here other headers of this plugin 

int do_movie_to_movie(void)
{
  O_i *ois, *oid;
  imreg *imr;
  static int nxd = 256, nyd = 64, nfd = 1024, xc = 320, yc = 242;
  int nx, ny, nf, i, j, l, k, im;
  union pix   *pd, *pds;
  float *z = NULL;
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;     
    } 
  nf = abs(ois->im.n_f); 
  if (nf <= 0)     
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;
    }
  else   active_menu->flags &= ~D_DISABLED;

  if(updating_menu_state != 0)    return D_O_K;
  i = ois->im.c_f;
  if (ois->im.data_type == IS_COMPLEX_IMAGE
     || ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)  
    {
      win_printf("I do not handle complexe images ! yet");
      return D_O_K;
    }
  nx = ois->im.nx;
  ny = ois->im.ny;

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine create a box of define size in a movie ");
    }
   
  i = win_scanf("choose the number of frames for new movie  %d\n"
                "choose the center cooordinates in pixel xc %d and yc %d\n"
                "choose the width in pixel (pair) %d\n"
                "choose the height in pixel (pair)%d\n", &nfd, &xc, &yc, &nxd, &nyd);
  if (i == CANCEL)	return D_O_K;
  
  if ((oid = create_and_attach_movie_to_imr(imr, nxd, nyd, IS_CHAR_IMAGE, nfd)) == NULL)
         return win_printf_OK("cannot create image!");  

/* for (im = 0; im < nfd ; im++)
       { 
           
        switch_frame(ois,im); 
        switch_frame(oid,im);
        pds = ois->im.pixel;
        pd = oid->im.pixel; 
         
         for (i = 0; i < nyd; i++) 
            {
               l = i + yc - nyd/2;
               for (j = 0; j < nxd; j++) 
               {
                 k = j + xc - nxd/2;
                   //win_printf("k is %d\n", k);
                 pd[i].fl[j] = pds[l].fl[k];
               }
            }       
       }*/
  for (i = 0; i < nyd; i++) 
    {
      l = i + yc - nyd/2;
      for (j = 0; j < nxd; j++) 
	{
	  k = j + xc - nxd/2;
	  z = extract_z_profile_from_movie(ois, k, l, z);
	  for (im = 0; im < nfd ; im++)
	    {
	      switch_frame(oid,im); 
	      pd = oid->im.pixel;
	      pd[i].ch[j] = (unsigned char)(z[im]);
	    }
	}
    }    
  inherit_from_im_to_im(oid,ois);
  uns_oi_2_oi(oid,ois);
  set_oi_horizontal_extend(oid, 1);
  set_oi_vertical_extend(oid, 1);
  oid->im.win_flag = ois->im.win_flag;
  if (ois->title != NULL)    set_im_title(oid, "%s ", ois->title);
  set_formated_string(&oid->im.treatement," box in movie of %s ", ois->filename);
  find_zmin_zmax(oid);
  free(z);
 return (refresh_image(imr, imr->n_oi - 1));
}		

int do_movie_avg_ood_and_even_images(void)
{
  O_i *ois, *oid;
  imreg *imr;
  int nx, ny, nf, i, im, k, j;
  float *z;
  union pix *pd; 

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;	
    }		
  nf = abs(ois->im.n_f);		
  if (nf <= 0)	
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;
    }
  else   active_menu->flags &= ~D_DISABLED;

  if(updating_menu_state != 0)	return D_O_K;
  i = ois->im.c_f;
  if (ois->im.data_type == IS_COMPLEX_IMAGE 
      || ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)	
    {
      win_printf("I do not handle complexe images ! yet");
      return D_O_K;
    }
  nx = ois->im.nx;
  ny = ois->im.ny;

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine average a movie over a subset of images");
    }

  oid = create_and_attach_oi_to_imr(imr, nx, ny, IS_FLOAT_IMAGE);

  z = (float *)calloc(nx,sizeof(float));
  if (oid == NULL || z == NULL)      
    return win_printf_OK("Can't create dest movie");
  
  if (nf%2 != 0) nf -=1;//check a odd/even number of frame in movie
  for (im = 0; im < nf; im++)
    {
      switch_frame(ois,im);
      for (i = 0, pd = oid->im.pixel; i < ny ; i++)
    {
	  extract_raw_line (ois, i, z);
	  for (j=0; j < nx; j++)
	  {
          if (im%2 == 0) pd[i].fl[j] += z[j];
	      else pd[i].fl[j] -= z[j];
      }
	}
      if ((im+1) == nf)
	{
	  k = nf/2;
	  for (i = 0; i < ny; i++)
	    {
	      for (j=0; j < nx; j++) pd[i].fl[j] /= k;
	    }
	}
    }

  inherit_from_im_to_im(oid,ois);
  uns_oi_2_oi(oid,ois);
  oid->im.win_flag = ois->im.win_flag;
  if (ois->title != NULL)	set_im_title(oid, "%s ", ois->title);
  set_formated_string(&oid->im.treatement,"Avg movie of %s ", ois->filename);
  find_zmin_zmax(oid);
  free(z);
  return (refresh_image(imr, imr->n_oi - 1));
}

int do_movie_yavg_ood_even(void)
{ 
  O_i *ois;
  imreg *imr;
  register int i, j;
  static int i_start = 0, i_stop = 465;
  
  O_p *op;
  d_s *ds_odd, *ds_even; 
  float *z; 
  
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;	
    }		
  if(updating_menu_state != 0)	return D_O_K;
  
  i = ois->im.c_f;
  if (ois->im.data_type == IS_COMPLEX_IMAGE 
      || ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)	
    {
      win_printf("I do not handle complexe images ! yet");
      return D_O_K;
    }

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine averages along lines en distinguant odd and even lines");
    }

  i = win_scanf("define line index (even number) to start averaging %d"
                "define line index (odd number) to stop averaging %d"
                , &i_start, &i_stop);
  if (i == CANCEL)	return D_O_K;
  if (i_start%2 != 0) return win_printf_OK("Try again silly boy!");
  if (i_stop%2 == 0) return win_printf_OK("Try again silly boy!");
  
  if ((op = create_and_attach_op_to_oi (ois, ois->im.nx, ois->im.nx, 0,IM_SAME_X_AXIS | IM_SAME_Y_AXIS)) == NULL)
		return win_printf_OK("cannot create plot !"); 
  ds_even = op->dat[0];
  if ((ds_odd = create_and_attach_one_ds(op, ois->im.nx, ois->im.nx, 0))== NULL)
		return win_printf_OK("cannot create plot !"); 
  ds_odd = op->dat[1];
  
  z = (float *)calloc(ois->im.ny,sizeof(float));
  
  for (j = 0; j < ois->im.nx; j++)
  
    {   
         ds_even->xd[j] = j;
         ds_odd->xd[j] = j;
         extract_raw_row (ois, j, z);
         for (i = i_start; i < i_stop + 1; i++) 
            {
                    if (i%2 == 0) ds_even->yd[j] += z[i];
                    if (i%2 != 0) ds_odd->yd[j] += z[i];
            }
         ds_even->yd[j] /= (i_stop - i_start + 1)/2;
         ds_odd->yd[j] /= (i_stop - i_start + 1)/2;
     }
  inherit_from_im_to_ds(ds_even,ois);
  inherit_from_im_to_ds(ds_odd,ois);       
  free(z);
  
  uns_oi_2_op(ois,IS_X_UNIT_SET,op,IS_X_UNIT_SET);
  return (refresh_image(imr, imr->n_oi - 1));
}

int do_movie_avg_ood_and_even_plot_correct(void)
{
  O_i *ois;
  imreg *imr;
  int  i, j, k, npt, nx, ny; 
  static int i1_start = 0, i1_stop = 199, i2_start = 250, i2_stop = 451;
  union pix *ps; 
  
  O_p *op;
  d_s *ds_even, *ds_odd;
  
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;	
    }	
    
  if(updating_menu_state != 0)	return D_O_K;
  	
  i = ois->im.c_f;
  if (ois->im.data_type == IS_COMPLEX_IMAGE 
      || ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)	
    {
      win_printf("I do not handle complexe images ! yet");
      return D_O_K;
    }
    
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine average pixels over odd and even lines and create 2 datasets for odd and even intensity along lines");
    }
   
  nx = ois->im.nx;
  ny = ois->im.ny;
  if (ny%2 != 0) ny -=1; //check a odd or even number of lines in movie and impose an odd number
  
  i = win_scanf("choose the line index to first start (even number) %d\n"
                "choose the line index to first stop (odd number) %d\n"
                "choose the line index to second start (even number) %d\n"
                "choose the line index to second stop (odd number) %d\n"
                , &i1_start, &i1_stop, &i2_start, &i2_stop );
  if (i == CANCEL)	return D_O_K;
  if (i1_start%2 != 0) return win_printf_OK("Try again silly boy!");
  if (i1_stop%2 == 0) return win_printf_OK("Try again silly boy!");
  if (i2_start%2 != 0) return win_printf_OK("Try again silly boy!");
  if (i2_stop%2 == 0) return win_printf_OK("Try again silly boy!");
  
  
  npt = (i1_stop - i1_start + i2_stop - i2_start)/2 + 1;
  if ((op = create_and_attach_op_to_oi(ois, npt, npt, 0, IM_SAME_X_AXIS | IM_SAME_Y_AXIS)) == NULL)
         return win_printf_OK("cannot create plot!");  
  ds_even = op->dat[0];
  if ((ds_odd = create_and_attach_one_ds(op, npt, npt, 0)) == NULL)
         return win_printf_OK("cannot create dataset!");
  
  
  
  for (k = 0, ps = ois->im.pixel; k < npt; k++)
    { 
      ds_even->xd[k] = k;
      if (k < (i1_stop - i1_start + 1)/2)
    {
      i = i1_start + 2*k;
      for (j=0, ds_even->yd[k] = 0; j < nx; j++) ds_even->yd[k] += ps[i].fl[j];   
      ds_even->yd[k] /= nx;    
    }
      else 
    {
      i = i2_start - i1_stop + i1_start + 2*k - 1;
      for (j=0, ds_even->yd[k] = 0; j < nx; j++) ds_even->yd[k] += ps[i].fl[j];   
      ds_even->yd[k] /= nx;    
    }
    }  
  for (k = 0, ps = ois->im.pixel; k < npt; k++)
    { 
      ds_odd->xd[k] = k;
      if (k < (i1_stop - i1_start + 1)/2)
    {
      i = i1_start + 2*k + 1;
      for (j=0, ds_odd->yd[k] = 0; j < nx; j++) ds_odd->yd[k] += ps[i].fl[j];   
      ds_odd->yd[k] /= nx;    
    }
      else 
    {
      i = i2_start - i1_stop + i1_start + 2*k;
      for (j=0, ds_odd->yd[k] = 0; j < nx; j++) ds_odd->yd[k] += ps[i].fl[j];   
      ds_odd->yd[k] /= nx;    
    }
    }    
  return (refresh_image(imr, imr->n_oi - 1));
}
int do_avg_ood_and_even_frame_correct(void)
{
  O_i *ois, *oid;
  imreg *imr;
  union pix  *ps, *pd;
  int nx, ny, i, j;
  static float avg_even = 1.0, avg_odd = 1.0;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;     
    } 
  
  if(updating_menu_state != 0)    return D_O_K;
  i = ois->im.c_f;
  if (ois->im.data_type == IS_COMPLEX_IMAGE
     || ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)  
    {
      win_printf("I do not handle complexe images ! yet");
      return D_O_K;
    }
  nx = ois->im.nx;
  ny = ois->im.ny;

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine corrects odd/even lines modulation ");
    }
   
  i = win_scanf("give correction factor for even lines %8f\n"
                "give correction factor for odd lines %8f\n"
                 , &avg_even, &avg_odd);
  if (i == CANCEL)	return D_O_K;
  
  if ((oid = create_and_attach_oi_to_imr(imr, nx, ny, IS_FLOAT_IMAGE)) == NULL)
         return win_printf_OK("cannot create image!");  
  
  for (i = 0, pd = oid->im.pixel, ps = ois->im.pixel; i < ny ; i++)
     { 
        if (i%2 == 0) 
     {
        for (j=0; j < nx; j++)  pd[i].fl[j] = ps[i].fl[j] - avg_even;
     }
        else  
     {
        for (j=0; j < nx; j++)  pd[i].fl[j] = ps[i].fl[j] - avg_odd;
     }   
     }
     
 inherit_from_im_to_im(oid,ois);
 //set_oi_horizontal_extend(oid, 1);
 //set_oi_vertical_extend(oid, 1);
 oid->im.win_flag = ois->im.win_flag;
 if (ois->title != NULL)    set_im_title(oid, "%s ", ois->title);
 set_formated_string(&oid->im.treatement," box in movie of %s ", ois->filename);
 find_zmin_zmax(oid);
 
 return (refresh_image(imr, imr->n_oi - 1));
}

int do_movie_avg_ood_and_even_images_short(void)
{
  O_i *ois, *oid;
  imreg *imr;
  int nx, ny, nf, i, im, k, j;
  static int navg = 2;
  float *z;
  union pix *pd; 

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;	
    }		
  nf = abs(ois->im.n_f);		
  if (nf <= 0)	
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;
    }
  else   active_menu->flags &= ~D_DISABLED;

  if(updating_menu_state != 0)	return D_O_K;
  i = ois->im.c_f;
  if (ois->im.data_type == IS_COMPLEX_IMAGE 
      || ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)	
    {
      win_printf("I do not handle complexe images ! yet");
      return D_O_K;
    }
  nx = ois->im.nx;
  ny = ois->im.ny;

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine average a movie over a subset of images");
    }

  oid = create_and_attach_oi_to_imr(imr, nx, ny, IS_FLOAT_IMAGE);

  z = (float *)calloc(nx,sizeof(float));
  if (oid == NULL || z == NULL)      
    return win_printf_OK("Can't create dest movie");
  
  i = win_scanf("choose a number of frame in movie  %d\n", &navg);
  if (i == CANCEL)	return D_O_K;
  
  for (im = 0; im < navg; im++)
    {
      switch_frame(ois,im);
      for (i = 0, pd = oid->im.pixel; i < ny ; i++)
    {
	  extract_raw_line (ois, i, z);
	  for (j=0; j < nx; j++)
	  {
          if (im%2 == 0) pd[i].fl[j] += z[j];
	      else pd[i].fl[j] -= z[j];
      }
	}
      if ((im+1) == navg)
	{
	  k = navg/2;
	  for (i = 0; i < ny; i++)
	    {
	      for (j=0; j < nx; j++) pd[i].fl[j] /= k;
	    }
	}
    }

  inherit_from_im_to_im(oid,ois);
  uns_oi_2_oi(oid,ois);
  oid->im.win_flag = ois->im.win_flag;
  if (ois->title != NULL)	set_im_title(oid, "%s ", ois->title);
  set_formated_string(&oid->im.treatement,"Avg movie of %s ", ois->filename);
  find_zmin_zmax(oid);
  free(z);
  return (refresh_image(imr, imr->n_oi - 1));
}

MENU *movie_phase_image_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"box in movie",do_movie_to_movie,NULL,0,NULL);    
    add_item_to_menu(mn,"movie avg ood and even images",do_movie_avg_ood_and_even_images,NULL,0,NULL);  
    add_item_to_menu(mn,"movie avg ood and even images short",do_movie_avg_ood_and_even_images_short,NULL,0,NULL);
    add_item_to_menu(mn,"y average odd/even",do_movie_yavg_ood_even,NULL,0,NULL); 
    add_item_to_menu(mn,"plot for correction of odd/even",do_movie_avg_ood_and_even_plot_correct,NULL,0,NULL);
    add_item_to_menu(mn,"correction of odd/even modulation on frame",do_avg_ood_and_even_frame_correct,NULL,0,NULL); 
      
	return mn;
}

int	movie_phase_main(int argc, char **argv)
{
	add_image_treat_menu_item ( "movie_phase", NULL, movie_phase_image_menu(), 0, NULL);
	return D_O_K;
}

int	movie_phase_unload(int argc, char **argv)
{
	remove_item_to_menu(image_treat_menu, "movie_phase", NULL, NULL);
	return D_O_K;
}
#endif

