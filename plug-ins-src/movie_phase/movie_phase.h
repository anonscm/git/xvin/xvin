#ifndef _MOVIE_PHASE_H_
#define _MOVIE_PHASE_H_

PXV_FUNC(int, do_movie_phase_average_along_y, (void));
PXV_FUNC(MENU*, movie_phase_image_menu, (void));
PXV_FUNC(int, movie_phase_main, (int argc, char **argv));
#endif

