#ifndef _RLC_RESO_H_
#define _RLC_RESO_H_

PXV_FUNC(int, do_rlc_reso, (void));
PXV_FUNC(MENU*, rlc_reso_plot_menu, (void));
PXV_FUNC(int, rlc_reso_main, (int argc, char **argv));
#endif

