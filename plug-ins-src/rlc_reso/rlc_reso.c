/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _RLC_RESO_C_
#define _RLC_RESO_C_

# include "allegro.h"
# include "xvin.h"

# include "gsl_complex.h"
# include "gsl_complex_math.h"
/* If you include other plug-ins header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "rlc_reso.h"


int do_rlc_reso(void)
{
	register int i, j;
	O_p *opn = NULL;
	int nf;
	d_s *ds, *ds2, *ds3, *ds4;
	pltreg *pr = NULL;
	float f, omega;
	static float C = 47, Gamma = 7, R = 1, L = 1.5, f0 = 10, f1 = 50, df = 10; 
	gsl_complex Zg, Zc, Zl, Zt, Z, rap;


	if(updating_menu_state != 0)	return D_O_K;



	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine compute the impedence"
				     "of a RLC circuit");
	}

	if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
		return win_printf_OK("cannot find data");

	i = win_scanf("This routine compute the impedence of a RLC circuit\n"
		      "Coil L (\\mu H) %8f Capacity C (pF) %8f\n"
		      "resistance R (in \\Omega) %8f coupling capacity (pF) %8f\n"
		      "Sweep frequency from %10f (MHz) to %10f (MHz) \n"
		      "with step of %10f (KHz)\n",&L,&C,&R,&Gamma,&f0,&f1,&df);
	if (i == CANCEL)	return OFF;

	nf = 1+(int)((f1-f0)*1000)/df;
	if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	ds = opn->dat[0];

	if ((ds2 = create_and_attach_one_ds(opn, nf, nf, 0)) == NULL)
		return win_printf_OK("cannot create plot !");

	if ((ds3 = create_and_attach_one_ds(opn, nf, nf, 0)) == NULL)
		return win_printf_OK("cannot create plot !");

	if ((ds4 = create_and_attach_one_ds(opn, nf, nf, 0)) == NULL)
		return win_printf_OK("cannot create plot !");


	for (j = 0; j < nf; j++)
	{
	  f = 1000*f0 + j*df;
	  f *= 1000;
	  omega = 2 * M_PI * f;
	  GSL_SET_COMPLEX(&Zl,R,L*omega*1e-6);
	  GSL_SET_COMPLEX(&Zc,0,((double)-1)/(C*omega*1e-12));
	  GSL_SET_COMPLEX(&Zg,0,((double)-1)/(Gamma*omega*1e-12));
	  Z = gsl_complex_add(gsl_complex_inverse(Zl),gsl_complex_inverse(Zc));
	  Z = gsl_complex_inverse (Z);
	  Zt = gsl_complex_add(Zg,Z);
	  ds->yd[j] = ((float)50)/gsl_complex_abs(Zt);//GSL_REAL(Zt);
	  ds2->yd[j] = gsl_complex_arg(Zt);//GSL_IMAG(Zt);
	  rap = gsl_complex_div(Z,Zt);
	  ds3->yd[j] = gsl_complex_abs(rap);
	  ds4->yd[j] = gsl_complex_arg(rap);
	  ds->xd[j] = ds2->xd[j] = ds3->xd[j] = ds4->xd[j] = f/1e6;
	}

	/* now we must do some house keeping */
	ds->source = my_sprintf(ds->source,"RLC Gamma circuit");
	/* refisplay the entire plot */
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
}




double do_rlc_reso_xi2(d_s *ds, float C, float Gamma, float R, float L, float P, float dp, float dy)
{
	register int  j;
	int nf;
	float f, omega;
	double tmp, xi2, si;
	gsl_complex Zg, Zc, Zl, Zt, Z;

	nf = ds->nx;
	for (j = 0, xi2 = 0; j < nf; j++)
	{
	  f = ds->xd[j]*1e6;
	  si = (ds->yd[j]* dp) + dy;
	  omega = 2 * M_PI * f;
	  GSL_SET_COMPLEX(&Zl,R,L*omega*1e-6);
	  GSL_SET_COMPLEX(&Zc,0,((double)-1)/(C*omega*1e-12));
	  GSL_SET_COMPLEX(&Zg,0,((double)-1)/(Gamma*omega*1e-12));
	  Z = gsl_complex_add(gsl_complex_inverse(Zl),gsl_complex_inverse(Zc));
	  Z = gsl_complex_inverse (Z);
	  Zt = gsl_complex_add(Zg,Z);
	  tmp = (GSL_REAL(Zt)-50)*(GSL_REAL(Zt)-50) + GSL_IMAG(Zt) * GSL_IMAG(Zt);
	  tmp /= (GSL_REAL(Zt)+50)*(GSL_REAL(Zt)+50) + GSL_IMAG(Zt) * GSL_IMAG(Zt);
	  si *= si;
	  xi2 += (tmp*P-ds->yd[j])*(tmp*P-ds->yd[j])/si;
	}
	return xi2;
}



double do_rlc_reso_find_best_P_by_xi2(d_s *ds, float C, float Gamma, float R, float L, float dp, float dy)
{
	register int  j;
	int nf;
	float f, omega;
	double tmp, si, Nu, De;
	gsl_complex Zg, Zc, Zl, Zt, Z;

	nf = ds->nx;
	for (j = 0, Nu = De = 0; j < nf; j++)
	{
	  f = ds->xd[j]*1e6;
	  si = (ds->yd[j]* dp) + dy;
	  omega = 2 * M_PI * f;
	  GSL_SET_COMPLEX(&Zl,R,L*omega*1e-6);
	  GSL_SET_COMPLEX(&Zc,0,((double)-1)/(C*omega*1e-12));
	  GSL_SET_COMPLEX(&Zg,0,((double)-1)/(Gamma*omega*1e-12));
	  Z = gsl_complex_add(gsl_complex_inverse(Zl),gsl_complex_inverse(Zc));
	  Z = gsl_complex_inverse (Z);
	  Zt = gsl_complex_add(Zg,Z);
	  tmp = (GSL_REAL(Zt)-50)*(GSL_REAL(Zt)-50) + GSL_IMAG(Zt) * GSL_IMAG(Zt);
	  tmp /= (GSL_REAL(Zt)+50)*(GSL_REAL(Zt)+50) + GSL_IMAG(Zt) * GSL_IMAG(Zt);
	  si *= si;
	  Nu += (tmp*ds->yd[j])/si;
	  De += (tmp*tmp)/si;
	}
	return (De > 0) ? Nu/De : Nu;
}

double adjust_C(d_s *dsi, float C, float dC, float Gamma, float R, float L, float P, float dp, float dy, int niter)
{
  register int i;
  double xi21, xi22, xi2m, xi2l, xi2r, Cr, Cl, Cm, C1, C2;

  C1 = C - dC;
  C2 = C + dC;
  Cm = C;
  Cl = C - 2*dC;
  Cr = C + 2*dC;

  xi21 = do_rlc_reso_xi2(dsi, C1, Gamma, R, L, P, 0.05, 0.001);
  xi22 = do_rlc_reso_xi2(dsi, C2, Gamma, R, L, P, 0.05, 0.001);
  Cm = (C1+C2)/2;
  xi2m = do_rlc_reso_xi2(dsi, Cm, Gamma, R, L, P, 0.05, 0.001);
  xi2l = do_rlc_reso_xi2(dsi, Cl, Gamma, R, L, P, 0.05, 0.001);  
  xi2r = do_rlc_reso_xi2(dsi, Cr, Gamma, R, L, P, 0.05, 0.001);
  for(i = 0; (i < niter) && (fabs(xi21-xi22) > 0.01) ; i++)
    {
      if (xi2r < xi22)	           C2 = Cr;
      else if (xi2l < xi21)	   C1 = Cl;
      else if (xi21 > xi22)
	{
	  if (xi2m > xi21)
	    return win_printf_OK("You are not heading to a minimun");
	  C1 = Cm;
	  
	}
      else
	{
	  if (xi2m > xi22)
	    return win_printf_OK("You are not heading to a minimun");
	  C2 = Cm;
	}
      xi21 = do_rlc_reso_xi2(dsi, C1, Gamma, R, L, P, 0.05, 0.001);
      xi22 = do_rlc_reso_xi2(dsi, C2, Gamma, R, L, P, 0.05, 0.001);
      Cm = (C1+C2)/2;
      Cl = Cm + (C2 - C1); 
      Cr = Cm - (C2 - C1); 
      xi2m = do_rlc_reso_xi2(dsi, Cm, Gamma, R, L, P, 0.05, 0.001);
      xi2l = do_rlc_reso_xi2(dsi, Cl, Gamma, R, L, P, 0.05, 0.001);  
      xi2r = do_rlc_reso_xi2(dsi, Cr, Gamma, R, L, P, 0.05, 0.001);
      //display_title_message("Iter %d xi2 = %g",i,xi2m);
    } 
  return Cm;
}

double adjust_Gamma(d_s *dsi, float C, float Gamma, float dGamma, float R, float L, float P, float dp, float dy, int niter)
{
  register int i;
  double xi21, xi22, xi2m, xi2l, xi2r, Gr, Gl, Gm, G1, G2, Cr, Cl, Cm, C1, C2;

  G1 = Gamma - dGamma;
  G2 = Gamma + dGamma;
  Gm = Gamma;
  Gl = Gamma - 2*dGamma;
  Gr = Gamma + 2*dGamma;


  Cm = adjust_C(dsi, C,  dGamma, Gm, R, L, P, dp, dy, niter);
  xi2m = do_rlc_reso_xi2(dsi, Cm, Gm, R, L, P, 0.05, 0.001);
  C1 = adjust_C(dsi, Cm,  dGamma, G1, R, L, P, dp, dy, niter);
  xi21 = do_rlc_reso_xi2(dsi, C1, G1, R, L, P, 0.05, 0.001);
  C2 = adjust_C(dsi, Cm,  dGamma, G2, R, L, P, dp, dy, niter);
  xi22 = do_rlc_reso_xi2(dsi, C2, G2, R, L, P, 0.05, 0.001);
  Cl = adjust_C(dsi, C1,  dGamma, Gl, R, L, P, dp, dy, niter);
  xi2l = do_rlc_reso_xi2(dsi, Cl, Gl, R, L, P, 0.05, 0.001);
  Cr = adjust_C(dsi, C2,  dGamma, Gr, R, L, P, dp, dy, niter);  
  xi2r = do_rlc_reso_xi2(dsi, Cr, Gr, R, L, P, 0.05, 0.001);
  for(i = 0; (i < niter) && (fabs(xi21-xi22) > 0.01) ; i++)
    {
      if (xi2r < xi22)	           G2 = Gr;
      else if (xi2l < xi21)	   G1 = Gl;
      else if (xi21 > xi22)
	{
	  if (xi2m > xi21)
	    return win_printf_OK("You are not heading to a minimun");
	  G1 = Gm;
	  
	}
      else
	{
	  if (xi2m > xi22)
	    return win_printf_OK("You are not heading to a minimun");
	  G2 = Gm;
	}


      Gm = (G1+G2)/2;
      Gl = Gm + (G2 - G1); 
      Gr = Gm - (G2 - G1); 
      Cm = adjust_C(dsi, C,  dGamma, Gm, R, L, P, dp, dy, niter);
      xi2m = do_rlc_reso_xi2(dsi, Cm, Gm, R, L, P, 0.05, 0.001);
      C1 = adjust_C(dsi, Cm,  dGamma, G1, R, L, P, dp, dy, niter);
      xi21 = do_rlc_reso_xi2(dsi, C1, G1, R, L, P, 0.05, 0.001);
      C2 = adjust_C(dsi, Cm,  dGamma, G2, R, L, P, dp, dy, niter);
      xi22 = do_rlc_reso_xi2(dsi, C2, G2, R, L, P, 0.05, 0.001);
      Cl = adjust_C(dsi, C1,  dGamma, Gl, R, L, P, dp, dy, niter);
      xi2l = do_rlc_reso_xi2(dsi, Cl, Gl, R, L, P, 0.05, 0.001);
      Cr = adjust_C(dsi, C2,  dGamma, Gr, R, L, P, dp, dy, niter);  
      xi2r = do_rlc_reso_xi2(dsi, Cr, Gr, R, L, P, 0.05, 0.001);
      display_title_message("G Iter %d xi2 = %g",i,xi2m);
    } 
  return Gm;
}


int do_rlc_reso_2(void)
{
	register int i, j;
	O_p *opn = NULL;
	int nf;
	d_s *ds, *dsi;
	pltreg *pr = NULL;
	float f, omega;
	static float C = 47, Gamma = 7, R = 1, L = 1.5, f0 = 10, f1 = 50, df = 10, P = 1; 
	double tmp, xi2;
	gsl_complex Zg, Zc, Zl, Zt, Z;


	if(updating_menu_state != 0)	return D_O_K;



	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine compute the impedence"
				     "of a RLC circuit");
	}

	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&opn,&dsi) != 3)
		return win_printf_OK("cannot find data");

	ds = find_source_specific_ds_in_op(opn, "RLC Gamma circuit reflection");

	if (ds != NULL)
	  {
	    f0 = ds->xd[0];
	    nf = ds->nx;
	    f1 = ds->xd[nf-1];
	    df = 1000 * (ds->xd[1] - ds->xd[0]);
	    i = sscanf(ds->source,"RLC Gamma circuit reflection L = %f C = %f R = %f "
		       "Gamma = %f P = %f",&L,&C,&R,&Gamma,&P);
	    if (i != 5)
	      return win_printf_OK("I could not recover circuit paramet!");
	    i = win_scanf("This routine compute the impedence of a RLC circuit\n"
			  "Coil L (\\mu H) %8f Capacity C (pF) %8f\n"
			  "resistance R (in \\Omega) %8f coupling capacity (pF) %8f\n"
			  "Power %8f\n"
			  ,&L,&C,&R,&Gamma,&P);
	    if (i == CANCEL)	return OFF;
	  }
	else
	  {
	    i = win_scanf("This routine compute the impedence of a RLC circuit\n"
			  "Coil L (\\mu H) %8f Capacity C (pF) %8f\n"
			  "resistance R (in \\Omega) %8f coupling capacity (pF) %8f\n"
			  "Power %8f Sweep frequency from %10f (MHz) to %10f (MHz) \n"
			  "with step of %10f (KHz)\n",&L,&C,&R,&Gamma,&P,&f0,&f1,&df);
	    if (i == CANCEL)	return OFF;


	    nf = 1+(int)((f1-f0)*1000)/df;
	    if ((ds = create_and_attach_one_ds(opn, nf, nf, 0)) == NULL)
	      return win_printf_OK("cannot create plot !");
	  }


	for (j = 0; j < nf; j++)
	{
	  f = 1000*f0 + j*df;
	  f *= 1000;
	  omega = 2 * M_PI * f;
	  GSL_SET_COMPLEX(&Zl,R,L*omega*1e-6);
	  GSL_SET_COMPLEX(&Zc,0,((double)-1)/(C*omega*1e-12));
	  GSL_SET_COMPLEX(&Zg,0,((double)-1)/(Gamma*omega*1e-12));
	  Z = gsl_complex_add(gsl_complex_inverse(Zl),gsl_complex_inverse(Zc));
	  Z = gsl_complex_inverse (Z);
	  Zt = gsl_complex_add(Zg,Z);
	  tmp = (GSL_REAL(Zt)-50)*(GSL_REAL(Zt)-50) + GSL_IMAG(Zt) * GSL_IMAG(Zt);
	  tmp /= (GSL_REAL(Zt)+50)*(GSL_REAL(Zt)+50) + GSL_IMAG(Zt) * GSL_IMAG(Zt);
	  ds->yd[j] = (float)tmp*P;
	  ds->xd[j] = f/1e6;
	}

	xi2 = do_rlc_reso_xi2(dsi, C, Gamma, R, L, P, 0.05, 0.001);

	/* now we must do some house keeping */
	if (ds->source != NULL) 
	  {
	    free(ds->source);
	    ds->source = NULL; 
	  }
	ds->source = my_sprintf(ds->source,"RLC Gamma circuit reflection L = %g "
				"C = %g R = %g Gamma = %g P = %g \\xi^2 = %g",L,C,R,Gamma,P,xi2);
	/* refisplay the entire plot */


	if (ds->n_lab > 0)
	  {
	    if (ds->lab[0]->text) free(ds->lab[0]->text);
	    ds->lab[0]->text = my_sprintf(NULL,"\\stack{{L = %g \\mu H}{C = %g pF}{R = %g \\Omega}"
			       "{\\Gamma = %g pF}{P = %g}{\\xi^2 = %g n = %d}}",L,C,R,Gamma,P,xi2,dsi->nx);
	  }
	else set_ds_plot_label(ds, opn->x_lo + (opn->x_hi - opn->x_lo)/4, 
		opn->y_hi - (opn->y_hi - opn->y_lo)/4, USR_COORD, 
			       "\\stack{{L = %g \\mu H}{C = %g pF}{R = %g \\Omega}"
			       "{\\Gamma = %g pF}{P = %g}{\\xi^2 = %g n = %d}}",L,C,R,Gamma,P,xi2,dsi->nx);

	display_title_message("xi square %g ",xi2);
	pr->one_p->need_to_refresh = 1;		
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
}




int do_rlc_reso_find_P(void)
{
	register int i, j;
	O_p *opn = NULL;
	int nf;
	d_s *ds, *dsi;
	pltreg *pr = NULL;
	float f, omega;
	static float C = 47, Gamma = 7, R = 1, L = 1.5, f0 = 10, f1 = 50, df = 10, P = 1; 
	double tmp, xi2;
	gsl_complex Zg, Zc, Zl, Zt, Z;


	if(updating_menu_state != 0)	return D_O_K;



	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine compute the impedence"
				     "of a RLC circuit");
	}

	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&opn,&dsi) != 3)
		return win_printf_OK("cannot find data");

	ds = find_source_specific_ds_in_op(opn, "RLC Gamma circuit reflection");

	if (ds != NULL)
	  {
	    f0 = ds->xd[0];
	    nf = ds->nx;
	    f1 = ds->xd[nf-1];
	    df = 1000 * (ds->xd[1] - ds->xd[0]);
	    i = sscanf(ds->source,"RLC Gamma circuit reflection L = %f C = %f R = %f "
		       "Gamma = %f P = %f",&L,&C,&R,&Gamma,&P);
	    if (i != 5)
	      return win_printf_OK("I could not recover circuit parameter !");
	    i = win_scanf("Automatic adjustement of P in \n"
			  "This routine compute the impedence of a RLC circuit\n"
			  "Coil L (\\mu H) %8f Capacity C (pF) %8f\n"
			  "resistance R (in \\Omega) %8f coupling capacity (pF) %8f\n"
			  "Power %8f\n"
			  ,&L,&C,&R,&Gamma,&P);
	    if (i == CANCEL)	return OFF;
	  }
	else
	  {
	    i = win_scanf("Automatic adjustement of P in \n"
			  "This routine compute the impedence of a RLC circuit\n"
			  "Coil L (\\mu H) %8f Capacity C (pF) %8f\n"
			  "resistance R (in \\Omega) %8f coupling capacity (pF) %8f\n"
			  "Power %8f Sweep frequency from %10f (MHz) to %10f (MHz) \n"
			  "with step of %10f (KHz)\n",&L,&C,&R,&Gamma,&P,&f0,&f1,&df);
	    if (i == CANCEL)	return OFF;


	    nf = 1+(int)((f1-f0)*1000)/df;
	    if ((ds = create_and_attach_one_ds(opn, nf, nf, 0)) == NULL)
	      return win_printf_OK("cannot create plot !");
	  }

	P = do_rlc_reso_find_best_P_by_xi2(dsi, C, Gamma, R, L, 0.05, 0.001);

	for (j = 0; j < nf; j++)
	{
	  f = 1000*f0 + j*df;
	  f *= 1000;
	  omega = 2 * M_PI * f;
	  GSL_SET_COMPLEX(&Zl,R,L*omega*1e-6);
	  GSL_SET_COMPLEX(&Zc,0,((double)-1)/(C*omega*1e-12));
	  GSL_SET_COMPLEX(&Zg,0,((double)-1)/(Gamma*omega*1e-12));
	  Z = gsl_complex_add(gsl_complex_inverse(Zl),gsl_complex_inverse(Zc));
	  Z = gsl_complex_inverse (Z);
	  Zt = gsl_complex_add(Zg,Z);
	  tmp = (GSL_REAL(Zt)-50)*(GSL_REAL(Zt)-50) + GSL_IMAG(Zt) * GSL_IMAG(Zt);
	  tmp /= (GSL_REAL(Zt)+50)*(GSL_REAL(Zt)+50) + GSL_IMAG(Zt) * GSL_IMAG(Zt);
	  ds->yd[j] = (float)tmp*P;
	  ds->xd[j] = f/1e6;
	}

	xi2 = do_rlc_reso_xi2(dsi, C, Gamma, R, L, P, 0.05, 0.001);

	/* now we must do some house keeping */
	if (ds->source != NULL) 
	  {
	    free(ds->source);
	    ds->source = NULL; 
	  }
	ds->source = my_sprintf(ds->source,"RLC Gamma circuit reflection L = %g "
				"C = %g R = %g Gamma = %g P = %g \\xi^2 = %g",L,C,R,Gamma,P,xi2);
	/* refisplay the entire plot */


	if (ds->n_lab > 0)
	  {
	    if (ds->lab[0]->text) free(ds->lab[0]->text);
	    ds->lab[0]->text = my_sprintf(NULL,"\\stack{{L = %g \\mu H}{C = %g pF}{R = %g \\Omega}"
			       "{\\Gamma = %g pF}{P = %g}{\\xi^2 = %g n = %d}}",L,C,R,Gamma,P,xi2,dsi->nx);
	  }
	else set_ds_plot_label(ds, opn->x_lo + (opn->x_hi - opn->x_lo)/4, 
		opn->y_hi - (opn->y_hi - opn->y_lo)/4, USR_COORD, 
			       "\\stack{{L = %g \\mu H}{C = %g pF}{R = %g \\Omega}"
			       "{\\Gamma = %g pF}{P = %g}{\\xi^2 = %g n = %d}}",L,C,R,Gamma,P,xi2,dsi->nx);

	pr->one_p->need_to_refresh = 1;		
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
}




int do_rlc_reso_find_C(void)
{
	register int i, j;
	O_p *opn = NULL;
	int nf;
	d_s *ds, *dsi;
	pltreg *pr = NULL;
	float f, omega;
	static float C = 47, Gamma = 7, R = 1, L = 1.5, f0 = 10, f1 = 50, df = 10, P = 1, dC = 1;
	static int niter = 100;
	double tmp, xi2;
	gsl_complex Zg, Zc, Zl, Zt, Z;


	if(updating_menu_state != 0)	return D_O_K;



	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine compute the impedence"
				     "of a RLC circuit");
	}

	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&opn,&dsi) != 3)
		return win_printf_OK("cannot find data");

	ds = find_source_specific_ds_in_op(opn, "RLC Gamma circuit reflection");

	if (ds != NULL)
	  {
	    f0 = ds->xd[0];
	    nf = ds->nx;
	    f1 = ds->xd[nf-1];
	    df = 1000 * (ds->xd[1] - ds->xd[0]);
	    i = sscanf(ds->source,"RLC Gamma circuit reflection L = %f C = %f R = %f "
		       "Gamma = %f P = %f",&L,&C,&R,&Gamma,&P);
	    if (i != 5)
	      return win_printf_OK("I could not recover circuit parameter !");
	    i = win_scanf("Automatic adjustement of C by dichotomy \n"
			  "This routine compute the impedence of a RLC circuit\n"
			  "Coil L (\\mu H) %8f Capacity C (pF) %8f dC %8f\n"
			  "resistance R (in \\Omega) %8f coupling capacity (pF) %8f\n"
			  "Power %8f\nMax Nb of iter %d\n"
			  ,&L,&C,&dC,&R,&Gamma,&P,&niter);
	    if (i == CANCEL)	return OFF;
	  }
	else
	  {
	    i = win_scanf("Automatic adjustement of P in \n"
			  "This routine compute the impedence of a RLC circuit\n"
			  "Coil L (\\mu H) %8f Capacity  C (pF) %8f dC %8f\n"
			  "resistance R (in \\Omega) %8f coupling capacity (pF) %8f\n"
			  "Power %8f Sweep frequency from %10f (MHz) to %10f (MHz) \n"
			  "with step of %10f (KHz)\nMax Nb of iter %d\n",
			  &L,&C,&dC,&R,&Gamma,&P,&f0,&f1,&df,&niter);
	    if (i == CANCEL)	return OFF;


	    nf = 1+(int)((f1-f0)*1000)/df;
	    if ((ds = create_and_attach_one_ds(opn, nf, nf, 0)) == NULL)
	      return win_printf_OK("cannot create plot !");
	  }
	C = adjust_C(dsi, C, dC, Gamma, R, L, P,  0.05, 0.001, niter);


	for (j = 0; j < nf; j++)
	{
	  f = 1000*f0 + j*df;
	  f *= 1000;
	  omega = 2 * M_PI * f;
	  GSL_SET_COMPLEX(&Zl,R,L*omega*1e-6);
	  GSL_SET_COMPLEX(&Zc,0,((double)-1)/(C*omega*1e-12));
	  GSL_SET_COMPLEX(&Zg,0,((double)-1)/(Gamma*omega*1e-12));
	  Z = gsl_complex_add(gsl_complex_inverse(Zl),gsl_complex_inverse(Zc));
	  Z = gsl_complex_inverse (Z);
	  Zt = gsl_complex_add(Zg,Z);
	  tmp = (GSL_REAL(Zt)-50)*(GSL_REAL(Zt)-50) + GSL_IMAG(Zt) * GSL_IMAG(Zt);
	  tmp /= (GSL_REAL(Zt)+50)*(GSL_REAL(Zt)+50) + GSL_IMAG(Zt) * GSL_IMAG(Zt);
	  ds->yd[j] = (float)tmp*P;
	  ds->xd[j] = f/1e6;
	}

	xi2 = do_rlc_reso_xi2(dsi, C, Gamma, R, L, P, 0.05, 0.001);

	/* now we must do some house keeping */
	if (ds->source != NULL) 
	  {
	    free(ds->source);
	    ds->source = NULL; 
	  }
	ds->source = my_sprintf(ds->source,"RLC Gamma circuit reflection L = %g "
				"C = %g R = %g Gamma = %g P = %g \\xi^2 = %g",L,C,R,Gamma,P,xi2);
	/* refisplay the entire plot */


	if (ds->n_lab > 0)
	  {
	    if (ds->lab[0]->text) free(ds->lab[0]->text);
	    ds->lab[0]->text = my_sprintf(NULL,"\\stack{{L = %g \\mu H}{C = %g pF}{R = %g \\Omega}"
			       "{\\Gamma = %g pF}{P = %g}{\\xi^2 = %g n = %d}}",L,C,R,Gamma,P,xi2,dsi->nx);
	  }
	else set_ds_plot_label(ds, opn->x_lo + (opn->x_hi - opn->x_lo)/4, 
		opn->y_hi - (opn->y_hi - opn->y_lo)/4, USR_COORD, 
			       "\\stack{{L = %g \\mu H}{C = %g pF}{R = %g \\Omega}"
			       "{\\Gamma = %g pF}{P = %g}{\\xi^2 = %g n = %d}}",L,C,R,Gamma,P,xi2,dsi->nx);

	pr->one_p->need_to_refresh = 1;		
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
}




int do_rlc_reso_find_Gamma(void)
{
	register int i, j;
	O_p *opn = NULL;
	int nf;
	d_s *ds, *dsi;
	pltreg *pr = NULL;
	float f, omega;
	static float C = 47, Gamma = 7, R = 1, L = 1.5, f0 = 10, f1 = 50, df = 10, P = 1, dC = 1;
	static int niter = 100;
	double tmp, xi2;
	gsl_complex Zg, Zc, Zl, Zt, Z;


	if(updating_menu_state != 0)	return D_O_K;



	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine compute the impedence"
				     "of a RLC circuit");
	}

	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&opn,&dsi) != 3)
		return win_printf_OK("cannot find data");

	ds = find_source_specific_ds_in_op(opn, "RLC Gamma circuit reflection");

	if (ds != NULL)
	  {
	    f0 = ds->xd[0];
	    nf = ds->nx;
	    f1 = ds->xd[nf-1];
	    df = 1000 * (ds->xd[1] - ds->xd[0]);
	    i = sscanf(ds->source,"RLC Gamma circuit reflection L = %f C = %f R = %f "
		       "Gamma = %f P = %f",&L,&C,&R,&Gamma,&P);
	    if (i != 5)
	      return win_printf_OK("I could not recover circuit parameter !");
	    i = win_scanf("Automatic adjustement of C by dichotomy \n"
			  "This routine compute the impedence of a RLC circuit\n"
			  "Coil L (\\mu H) %8f Capacity C (pF) %8f\n"
			  "resistance R (in \\Omega) %8f coupling capacity (pF) %8f d\\Gamma %8f\n"
			  "Power %8f\nMax Nb of iter %d\n"
			  ,&L,&C,&R,&Gamma,&dC,&P,&niter);
	    if (i == CANCEL)	return OFF;
	  }
	else
	  {
	    i = win_scanf("Automatic adjustement of P in \n"
			  "This routine compute the impedence of a RLC circuit\n"
			  "Coil L (\\mu H) %8f Capacity  C (pF) %8f\n"
			  "resistance R (in \\Omega) %8f coupling capacity (pF) %8f d\\Gamma %8f\n"
			  "Power %8f Sweep frequency from %10f (MHz) to %10f (MHz) \n"
			  "with step of %10f (KHz)\nMax Nb of iter %d\n",
			  &L,&C,&R,&Gamma,&dC,&P,&f0,&f1,&df,&niter);
	    if (i == CANCEL)	return OFF;


	    nf = 1+(int)((f1-f0)*1000)/df;
	    if ((ds = create_and_attach_one_ds(opn, nf, nf, 0)) == NULL)
	      return win_printf_OK("cannot create plot !");
	  }
	Gamma = adjust_Gamma(dsi, C, Gamma, dC, R, L, P,  0.05, 0.001, niter);
	C = adjust_C(dsi, C, dC, Gamma, R, L, P,  0.05, 0.001, niter);

	for (j = 0; j < nf; j++)
	{
	  f = 1000*f0 + j*df;
	  f *= 1000;
	  omega = 2 * M_PI * f;
	  GSL_SET_COMPLEX(&Zl,R,L*omega*1e-6);
	  GSL_SET_COMPLEX(&Zc,0,((double)-1)/(C*omega*1e-12));
	  GSL_SET_COMPLEX(&Zg,0,((double)-1)/(Gamma*omega*1e-12));
	  Z = gsl_complex_add(gsl_complex_inverse(Zl),gsl_complex_inverse(Zc));
	  Z = gsl_complex_inverse (Z);
	  Zt = gsl_complex_add(Zg,Z);
	  tmp = (GSL_REAL(Zt)-50)*(GSL_REAL(Zt)-50) + GSL_IMAG(Zt) * GSL_IMAG(Zt);
	  tmp /= (GSL_REAL(Zt)+50)*(GSL_REAL(Zt)+50) + GSL_IMAG(Zt) * GSL_IMAG(Zt);
	  ds->yd[j] = (float)tmp*P;
	  ds->xd[j] = f/1e6;
	}

	xi2 = do_rlc_reso_xi2(dsi, C, Gamma, R, L, P, 0.05, 0.001);

	/* now we must do some house keeping */
	if (ds->source != NULL) 
	  {
	    free(ds->source);
	    ds->source = NULL; 
	  }
	ds->source = my_sprintf(ds->source,"RLC Gamma circuit reflection L = %g "
				"C = %g R = %g Gamma = %g P = %g \\xi^2 = %g",L,C,R,Gamma,P,xi2);
	/* refisplay the entire plot */


	if (ds->n_lab > 0)
	  {
	    if (ds->lab[0]->text) free(ds->lab[0]->text);
	    ds->lab[0]->text = my_sprintf(NULL,"\\stack{{L = %g \\mu H}{C = %g pF}{R = %g \\Omega}"
			       "{\\Gamma = %g pF}{P = %g}{\\xi^2 = %g n = %d}}",L,C,R,Gamma,P,xi2,dsi->nx);
	  }
	else set_ds_plot_label(ds, opn->x_lo + (opn->x_hi - opn->x_lo)/4, 
		opn->y_hi - (opn->y_hi - opn->y_lo)/4, USR_COORD, 
			       "\\stack{{L = %g \\mu H}{C = %g pF}{R = %g \\Omega}"
			       "{\\Gamma = %g pF}{P = %g}{\\xi^2 = %g n = %d}}",L,C,R,Gamma,P,xi2,dsi->nx);

	pr->one_p->need_to_refresh = 1;		
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
}



MENU *rlc_reso_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"RLC circuit", do_rlc_reso,NULL,0,NULL);
	add_item_to_menu(mn,"RLC refexion", do_rlc_reso_2,NULL,0,NULL);
	add_item_to_menu(mn,"RLC refexion find P", do_rlc_reso_find_P,NULL,0,NULL);
	add_item_to_menu(mn,"RLC refexion find C", do_rlc_reso_find_C,NULL,0,NULL);
	add_item_to_menu(mn,"RLC refexion find G", do_rlc_reso_find_Gamma,NULL,0,NULL);


	return mn;
}

int	rlc_reso_main(int argc, char **argv)
{
	add_plot_treat_menu_item ( "rlc_reso", NULL, rlc_reso_plot_menu(), 0, NULL);
	return D_O_K;
}

int	rlc_reso_unload(int argc, char **argv)
{
	remove_item_to_menu(plot_treat_menu, "rlc_reso", NULL, NULL);
	return D_O_K;
}
#endif

