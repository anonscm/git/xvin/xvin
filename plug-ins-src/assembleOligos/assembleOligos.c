/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _ASSEMBLEOLIGOS_C_
#define _ASSEMBLEOLIGOS_C_

# include "allegro.h"
# include "xvin.h"
# include "float.h"
# include <assert.h>

/* If you include other regular header do it here*/
# include "../../src/menus/plot/treat/p_treat_basic.h"

/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
//place here other headers of this plugin
# include "allegro.h"
# include "xvin.h"

/* If you include other regular header do it here*/
# include "../nrutil/nrutil.h"

/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "assembleOligos.h"

char hplag[] = "CGGAGACATATAGCTACAGGGGCCCGACGTCGCATGCTCCCGGCCGCCATGGCCGCGGGATTCGCGGATCCGCTGAGGTCTACTTTCTGCCCGGTAGGTCCAATAGCACGAAATCCAGTATGCTGGGAATCATTTTCAGAGCAACCGAACCAA";

/*
"CGGAGACATATAGCTACAGGGGCCCGACGTCGCATGCTCCCGGCCGCCATGGCCGCGGGATTCGCGGATCCGCTGAGGTCTACTTTCTGCCCGGTAGGTCCAATAGCACGAAATCCAGTATGCTGGGAATCATTTTCAGAGCAACCGAACCAATTATATCCAGTGATTTCAATATTAGTAAAACCACTTGAAGACAAAACTTTGGAAGCATTATCAGCATCAGTACATCCTATTAAAGACACTGCTAATACTAATGCTGCAATAGAACGATTAATATATTTCATAATTACCCTTTAAGCAAGTCGTAAAATCCACCATTCACATGCTTAGGAGCGGAAACTAACCGAACAGCCAGCCGATGACAATCAGGACATACATCAGTATCTCTTTCAGAAATTTTCTTGATTTTTTCGTATTCTTTTGCACAGTCTTTGGATTGACATTTATAATCATAAAGCGGCATAATTATTCCTTAAAGTAAGCTTTCAACATCTGATATAAAGACCACGCCTGATCATTATTTTCAATAGTAACCTTCATGACTGGGAATTCTGTGAAATCTTTCTGGCTAAAGAACTCTCGCCTGATATGCTCAAGAATTTCACCAGTATTTTCTTCGCACATCCAAGGAATCAAATCAATCAAACACCCGTCAAAATCTACTGTAGTAGGCTTATCATACACTTTAACATTAGGATATTTAGCCAAAAGCTCAGTAGAAGCATTTGGATGCATTACATTTTTATAGTGGAGATCGTGATTTCCTACAATAGTGTGTAATGTAATTCCAGCATCATCAAGCGAATGAACTATTTCACGGGCGAACTCCATAGAATTATGTGTGATCGCTTTTCGCACATCAAAAATATCACCGTATTGAATCCAGGTAGTAATTCCATTTTTCTTAGAATATTCTATCGCTTGCTTAATTCCATCAATTTGAATACCGCGAATCCACTCATCATCAGCTTTAACGCCTAAGTGCCAATCACCTAAATTTAAAATTTTCATATATCAAGAACCGTCATTGAAATGCAAAATAAAATTATTGAAATAAATCCATCTGGAGTGCTAAAGAACCCAATCCAACATGCTCTAGTGAATAGATAAAATGCAAGAAAAAGTATCACATATCCAAGAAATATCATTATATCAAACTCCGTATAAAGCTAAAGGGCCGAAGCCCTTTATTTTGTAATAATGTGGATCCCGCAATCACTAGTGCGGCCGTCAGATGCC";
*/

typedef struct _oligo
{
  int len;
  char seq[16];
  int used;
} oli;

oli *olset = NULL;
int nset = 0, mset = 0;


typedef struct _oligo_hit
{
  int noli;
  float z0;
  float z;
  float ze;  // exact position
  int i_ze;  // oligo exact position if known
  double Edz2;
  int next, nhnext, dnext;
  double Enext;
} olihit;

olihit *seq = NULL;
olihit *test = NULL;
olihit *bestseq = NULL;
olihit *exactseq = NULL;

int nseq = 0, itert = 0, flip = 0, nb_iter_at_T = 1000000, last_iter_of_T_step = 0;
int last_best_iter = 0, nb_iter_to_restart_from_last_best = 100000, packet_size = 3;
int nbreak;
float bp_to_nm = 1.100, dz_trial = 5, ture = 3, expected_nm_bp = 0.9;//0.025;
float Tdec = 0.95, break_energy = 3;
float Ez_to_Eh = .01, error_in_z = 3, Ebp_conv_Eh = 0.1;
double Edz2l, Ehtl, Emin;
double nm_to_bp = 0;
int debug = 0;
int exact_sequence_known = 0;
int seq_found = 0;

O_i *imres = NULL;
int n_acq_im = 0, max_acq_im = 0;

int idum = 4587;
int domt = 0;

O_p *ope = NULL; // plot of energy
d_s *dsEt = NULL, *dsEz = NULL, *dsEh = NULL;

O_p *opbpz = NULL; // plot of Z ves base position
d_s *dsbpz = NULL, *dsbpzb = NULL, *dsbpze = NULL;

O_p *ople = NULL; // plot of local energy
d_s *dslez = NULL, *dslet = NULL, *dshy = NULL, *dslem = NULL;

O_p *oppa = NULL; // pattern oligo nb vs pos
d_s *dspa = NULL, *dspab = NULL, *dspae = NULL;

O_p *opflip = NULL; // pattern oligo nb vs pos
d_s *dsflip = NULL;




int convert_oligoID_to_seq(char *message, int npid, int size)
{
  register int i, j;

  message[0] = 0;
  for(i = 0; i < size; i++)
    {
      j = (npid & 0x03);
      if (j == 0) message[size-i-1] = 'A';
      else if (j == 1) message[size-i-1] = 'C';
      else if (j == 2) message[size-i-1] = 'G';
      else if (j == 3) message[size-i-1] = 'T';
      npid >>= 2;
    }
  message[i] = 0;
  return 0;
}

int convert_oligo_seq_to_ID(char *seql, int size)
{
  register int i, j;
  int npid;

  for(i = 0, npid = 0; i < size; i++)
    {
      if ((seql[i]&0x5F) == 'A') j = 0;
      else if ((seql[i]&0x5F) == 'C') j = 1;
      else if ((seql[i]&0x5F) == 'G') j = 2;
      else if ((seql[i]&0x5F) == 'T') j = 3;
      npid <<= 2;
      npid |= j;
    }
  return npid;
}


int QuickSort_oligo_hit(olihit *xd, int l, int r)
{
  register int  i, j;
  float   x;//, ze1, ze2;
  olihit y;

  i = l;  j = r;
  x = xd[(int)((l+r)/2)].z;
  while (i<j)
    {	while (xd[i].z < x) { i += 1; }
      while (x < xd[j].z) { j -= 1; }
      if (i <= j)
	{
	  //ze1 = xd[i].ze;
	  //ze2 = xd[j].ze;
	  y = xd[i]; xd[i] = xd[j]; xd[j] = y;
	  //xd[i].ze = ze1;
	  //xd[j].ze = ze2;
	  i += 1;
	  j -= 1;
	}
    }/*while*/
  if (l<j) QuickSort_oligo_hit(xd, l, j);
  if (i<r) QuickSort_oligo_hit(xd, i, r);
  return 0;
} /* end of the "QuickSort_oligo_hit" function */


char *assembleOligos_idle_point_add_display(struct one_plot *op, DIALOG *d, int nearest_ds, int nearest_point)
{
  static char message[1024];
  d_s *ds;
  int i, k, npid;

  (void)d;
  message[0] = 0;
  if (op == NULL) return message;
  if (nearest_ds >= op->n_dat) return message;
  if (nearest_ds != 0 && nearest_ds != 4 && nearest_ds != 5) return message;
  ds = op->dat[nearest_ds];
  npid = (int)ds->yd[nearest_point];
  if (npid < 0 || npid >= nset) return message;
  for(i = 0; i < olset[npid].len; i++)
      message[i] = olset[npid].seq[i];
  message[i] = 0;
  k =(int)ds->xd[nearest_point];
  if (k < 0 || k >= nseq) return message;
  message[i] = ' ';
  if (nearest_ds == 0)
    snprintf(message+olset[npid].len,sizeof(message)-olset[npid].len," seq Ze %g z0 %g z %g",
	     seq[k].ze,seq[k].z0,seq[k].z);
  else if (nearest_ds == 4)
    snprintf(message+olset[npid].len,sizeof(message)-olset[npid].len," exactseq Ze %g z0 %g z %g",
	     exactseq[k].ze,exactseq[k].z0,exactseq[k].z);
  else if (nearest_ds == 5)
    snprintf(message+olset[npid].len,sizeof(message)-olset[npid].len," bestseq Ze %g z0 %g z %g",
	     bestseq[k].ze,bestseq[k].z0,bestseq[k].z);

  return message;
}




int do_assembleOligos_param(void)
{
  register int i;

  if(updating_menu_state != 0)	return D_O_K;


  i = win_scanf("Modify parameter\n"
		"Typical distance of move %8f\n"
		"Starting tmperature %8f error in Z %6f\n"
		"Packet size of oligo to move %5d\n"
		"Expected nm per bp conversion %6f\n"
		"Nb. of iter before T decrease %8d\n"
		"Ratio of temperature decrease at each step %5f\n"
		"Resume to last best set if no improvement after %12d\n"
		"Ratio Ez to Eh %8f Ration E conv to Eh %6f\n"
		"Break energy %5f\n"
		"Bp to nm ratio %8f\n"
		"Stop MC %R run %r\n"
		"Debug Stop%R flip only %r all %r"
		,&dz_trial,&ture,&error_in_z,&packet_size,&expected_nm_bp,&nb_iter_at_T,&Tdec
		,&nb_iter_to_restart_from_last_best,&Ez_to_Eh,&Ebp_conv_Eh,&break_energy
		,&bp_to_nm,&domt,&debug);
 if (i == WIN_CANCEL) return 0;

  return 0;
}

int do_assembleOligos_create_image_result(void)
{
  register int i, k;
  imreg *imr;
  O_i *oi;

   if(updating_menu_state != 0)	return D_O_K;


  imr = create_and_register_new_image_project( 0,   32,  900,  668);
  if (imr == NULL) return(win_printf_OK("could not create imreg"));
  oi = create_and_attach_oi_to_imr(imr, nseq, nset, IS_COMPLEX_IMAGE);
  remove_from_image (imr, imr->o_i[0]->im.data_type, (void *)imr->o_i[0]);
  select_image_of_imreg(imr, imr->n_oi -1);
  find_zmin_zmax(oi);
  set_im_x_title(oi,"Oligo position");
  set_im_y_title(oi,"Oligo index");
  oi->need_to_refresh = ALL_NEED_REFRESH;
  set_im_title(oi, "Possible oligos");
  for (k = 0; k < nseq && k < oi->im.nx; k++)
    {
      for (i = 0; i < oi->im.ny; i++)
	oi->im.pixel[i].fl[2*k] = 0.1;
    }
  max_acq_im = 1000000;
  imres = oi;
  return refresh_image(imr, imr->n_oi - 1);
}

int line_least_square_fit_on_ds_between_indexes_chi2(const d_s *src_ds, bool is_affine_fit, int min_idx, int max_idx, float error_in_y_if_needed,
						double *out_a, double *out_b, double *chi2)
{
  int nx, i;
  bool has_good_error_bars = true;
  float minx = FLT_MAX;
  float maxx = -FLT_MAX;
  float miny = FLT_MAX;
  float maxy = -FLT_MAX;
  double sy2 = 0, sy = 0, sx = 0, sx2 = 0, sxy = 0, sig2 = 0, sig_2 = 0, ny = 0;
  double a = 0, b = 0, lchi2 = 0;

  assert(src_ds);

    min_idx = (min_idx < 0) ? 0 : min_idx;
    max_idx = (max_idx < 0) ? src_ds->nx : max_idx;
    nx = max_idx - min_idx;
    error_in_y_if_needed = (error_in_y_if_needed <= 0) ? 1 : error_in_y_if_needed;
    if (max_idx - min_idx < 2)
    {
        (win_printf_OK("no fit on less than 2 points"));
        return 1;
    }
    if (src_ds->ye == NULL)
    {
        has_good_error_bars = false;
    }
    else
    {
        for (i = min_idx, has_good_error_bars = true; i < max_idx && has_good_error_bars == true; i++)
	  has_good_error_bars = (src_ds->ye[i] == 0) ? false : has_good_error_bars;
    }
    minx = miny = FLT_MAX;
    maxx = maxy = -FLT_MAX;
    if (has_good_error_bars == 0)
    {
        for (i = min_idx; i < max_idx ; i++)
        {
            sy += src_ds->yd[i];
            sy2 += src_ds->yd[i] * src_ds->yd[i];
            sx += src_ds->xd[i];
            sx2 += src_ds->xd[i] * src_ds->xd[i];
            sxy += src_ds->xd[i] * src_ds->yd[i];
            minx = (src_ds->xd[i] < minx) ? src_ds->xd[i] : minx;
            maxx = (src_ds->xd[i] > maxx) ? src_ds->xd[i] : maxx;
            miny = (src_ds->yd[i] < miny) ? src_ds->yd[i] : miny;
            maxy = (src_ds->yd[i] > maxy) ? src_ds->yd[i] : maxy;
        }
        if (!is_affine_fit)
        {
            if (sx2 == 0)
            {
                win_printf_OK("\\Sigma x_i^2 = 0   => cannot perform fit.");
                return 2;
            }
            a = (sxy / sx2);
            b = 0.;
        }
        else
        {
            b = (sx2 * nx) - sx * sx;
            if (b == 0)
            {
                win_printf_OK("\\Delta  = 0 \n can't fit that!");
                return 3;
            }
            a = sxy * nx - sx * sy;
            a /= b;
            b = (sx2 * sy - sx * sxy) / b;
        }
        for (i = 0, lchi2 = 0; i < nx; i++)
        {
            sig2 = error_in_y_if_needed * error_in_y_if_needed;
            sig_2 = (double)1 / sig2;
            lchi2 += (a * src_ds->xd[i] + b - src_ds->yd[i]) * (a * src_ds->xd[i] + b - src_ds->yd[i]) * sig_2;
        }
    }
    else  // with error bars in y
    {
      for (i = 0; i < nx ; i++)
        {
	  sig2 = src_ds->ye[i] * src_ds->ye[i];
	  sig_2 = (double)1 / sig2;
	  sy += src_ds->yd[i] * sig_2;
	  sy2 += src_ds->yd[i] * src_ds->yd[i] * sig_2;
	  sx += src_ds->xd[i] * sig_2;
	  sx2 += src_ds->xd[i] * src_ds->xd[i] * sig_2;
	  sxy += src_ds->xd[i] * src_ds->yd[i] * sig_2;
	  ny += sig_2;
	  minx = (src_ds->xd[i] < minx) ? src_ds->xd[i] : minx;
	  maxx = (src_ds->xd[i] > maxx) ? src_ds->xd[i] : maxx;
	  miny = (src_ds->yd[i] < miny) ? src_ds->yd[i] : miny;
	  maxy = (src_ds->yd[i] > maxy) ? src_ds->yd[i] : maxy;
        }
      if (!is_affine_fit)
        {
	  if (sx2 == 0)
            {
	      win_printf_OK("\\Sigma x_i^2 = 0   => cannot perform fit.");
	      return 4;
            }
	  a = (sxy / sx2);
	  b = 0.;
        }
      else
        {
	  b = (sx2 * ny) - sx * sx;

	  if (b == 0)
            {
                win_printf_OK("\\Delta  = 0 \n can't fit that!");
                return 5;
            }
            a = sxy * ny - sx * sy;
            a /= b;
            b = (sx2 * sy - sx * sxy) / b;
        }
        for (i = 0, lchi2 = 0; i < nx; i++)
        {
	  sig2 = src_ds->ye[i] * src_ds->ye[i];
	  sig_2 = (double)1 / sig2;
	  lchi2 += (a * src_ds->xd[i] + b - src_ds->yd[i]) * (a * src_ds->xd[i] + b - src_ds->yd[i]) * sig_2;
        }
    }

    if (out_a) *out_a = a;
    if (out_b) *out_b = b;
    if (chi2)  *chi2 = lchi2;
    return 0;
}

int diff_olihit(olihit *xd, olihit *yd, int nseql)
{
  int i, ndiff;
  if (xd == NULL || yd == NULL) return nseql;
  for (i = 0, ndiff = 0; i < nseql; i++)
    ndiff += (xd[i].noli != yd[i].noli) ? 1 : 0;
  return ndiff;
}


double compute_energies(olihit *xd, int nseql)
{
  register int i, j, k;
  int match, nh, ii, jj, pos, noli, len;//, error;
  double Et = 0, a = 0, b = 0, chi2 = 0;
  static d_s *dsfit = NULL;


  for (i = 1, nbreak = 0; i < nseql; i++)
     {
       ii = xd[i-1].noli;
       jj = xd[i].noli;
       for(k = 1, match = 0;(match == 0) && (k < olset[ii].len); k++)
	 {
	   for(j = 0,  match = 1;(match == 1) && (k + j < olset[ii].len) && (j < olset[jj].len); j++)
	     match = ((olset[ii].seq[k+j]&0x5F) != (olset[jj].seq[j]&0x5F)) ? 0 : match;
	   if (match) break;
	 }
       //k--;
       if (match == 0)
	 {
	   nbreak++;
	   xd[i-1].dnext = 0;
	   xd[i-1].nhnext = 0;
	   xd[i-1].Enext = break_energy;
	 }
       else
	 {
	   nh = olset[ii].len - k;
	   xd[i-1].nhnext = nh;
	   xd[i-1].dnext = k;
	   xd[i-1].Enext = -nh*nh;
	 }
     }
  xd[nseql-1].nhnext = 0;
  xd[nseql-1].dnext = 0;
  xd[nseql-1].Enext = 0;

  if (dsfit == NULL || nseql > dsfit->mx || nseql > dsfit->my)
    dsfit = build_adjust_data_set(dsfit, nseql, nseql);
  if (dsfit == NULL)
    {
      win_printf("Cannot allocate ds!");
      return 0;
    }
  for (j = pos = 0; j < nseql; j++)
    {
      noli = seq[j].noli;
      len = (seq[j].dnext == 0) ? olset[noli].len : seq[j].dnext;
      dsfit->yd[j] = seq[j].z0;
      dsfit->xd[j] = pos;
      pos += len;
    }
  //error =
  line_least_square_fit_on_ds_between_indexes_chi2(dsfit, 1, 0, nseql, error_in_z, &a, &b, &chi2);



  nm_to_bp = a;
  float tmp;
  for (i = 0, Et = 0, Edz2l = 0, Ehtl = 0; i < nseql; i++)
    {
      //xd[i].Edz2 = Ez_to_Eh * bp_to_nm * bp_to_nm *(xd[i].z - xd[i].z0)*(xd[i].z - xd[i].z0)
      //	* bp_to_nm * bp_to_nm* (xd[i].z - xd[i].z0)*(xd[i].z - xd[i].z0);
      tmp = (a * dsfit->xd[i]) + b;
      tmp -= dsfit->yd[i];
      tmp /=error_in_z;
      xd[i].Edz2 = Ez_to_Eh * tmp * tmp;
      //Et += xd[i].Edz2 + xd[i].Enext;
      Et += xd[i].Enext;
      Edz2l += xd[i].Edz2;
      Ehtl += xd[i].Enext;
    }
  Et += Ez_to_Eh * chi2;
  tmp = expected_nm_bp - a; // we want a good conversion factor
  tmp *= tmp * nseql * Ebp_conv_Eh;
  Et += tmp;
  return Et;
}




double compute_energies_old(olihit *xd, int nseql)
{
  register int i, j, k;
  int match, nh, ii, jj;
  double Et;


  for (i = 1, nbreak = 0; i < nseql; i++)
     {
       ii = xd[i-1].noli;
       jj = xd[i].noli;
       for(k = 1, match = 0;(match == 0) && (k < olset[ii].len); k++)
	 {
	   for(j = 0,  match = 1;(match == 1) && (k + j < olset[ii].len) && (j < olset[jj].len); j++)
	     match = ((olset[ii].seq[k+j]&0x5F) != (olset[jj].seq[j]&0x5F)) ? 0 : match;
	   if (match) break;
	 }
       //k--;
       if (match == 0)
	 {
	   nbreak++;
	   xd[i-1].dnext = 0;
	   xd[i-1].nhnext = 0;
	   xd[i-1].Enext = break_energy;
	 }
       else
	 {
	   nh = olset[ii].len - k;
	   xd[i-1].nhnext = nh;
	   xd[i-1].dnext = k;
	   xd[i-1].Enext = -nh*nh;
	 }
     }
  xd[nseql-1].nhnext = 0;
  xd[nseql-1].dnext = 0;
  xd[nseql-1].Enext = 0;
  for (i = 0, Et = 0, Edz2l = 0, Ehtl = 0; i < nseql; i++)
    {
      xd[i].Edz2 = Ez_to_Eh*bp_to_nm*bp_to_nm*(xd[i].z - xd[i].z0)*(xd[i].z - xd[i].z0)*bp_to_nm*bp_to_nm*(xd[i].z - xd[i].z0)*(xd[i].z - xd[i].z0);
      Et += xd[i].Edz2 + xd[i].Enext;
      Edz2l += xd[i].Edz2;
      Ehtl += xd[i].Enext;
    }
  return Et;
}

double compute_energies_2(olihit *xd, int nseql, int noli)
{
  register int i, j, k;
  int match, nh, ii, jj;
  double Et;


  for (i = 1, nbreak = 0; i < nseql; i++)
     {
       ii = xd[i-1].noli;
       jj = xd[i].noli;
       for(k = 1, match = 0;(match == 0) && (k < olset[ii].len); k++)
	 {
	   for(j = 0,  match = 1;(match == 1) && (k + j < olset[ii].len) && (j < olset[jj].len); j++)
	     match = ((olset[ii].seq[k+j]&0x5F) != (olset[jj].seq[j]&0x5F)) ? 0 : match;
	   if (match) break;
	 }
       //k--;
       if (match == 0)
	 {
	   nbreak++;
	   xd[i-1].dnext = 0;
	   xd[i-1].nhnext = 0;
	   xd[i-1].Enext = break_energy;
	 }
       else
	 {
	   nh = olset[ii].len - k;
	   if ((nh + 1) == noli)
	     {
	       xd[i-1].nhnext = nh;
	       xd[i-1].dnext = k;
	       xd[i-1].Enext = -nh*nh;
	     }
	   else
	     {
	       xd[i-1].nhnext = nh;
	       xd[i-1].dnext = k;
	       xd[i-1].Enext = (noli-nh-1)*10;
	     }
	 }
     }
  xd[nseql-1].nhnext = 0;
  xd[nseql-1].dnext = 0;
  xd[nseql-1].Enext = 0;
  for (i = 0, Et = 0, Edz2l = 0, Ehtl = 0; i < nseql; i++)
    {
      xd[i].Edz2 = Ez_to_Eh*bp_to_nm*bp_to_nm*(xd[i].z - xd[i].z0)*(xd[i].z - xd[i].z0)*bp_to_nm*bp_to_nm*(xd[i].z - xd[i].z0)*(xd[i].z - xd[i].z0);
      Et += xd[i].Edz2 + xd[i].Enext;
      Edz2l += xd[i].Edz2;
      Ehtl += xd[i].Enext;
    }
  return Et;
}




int alocate_olihit(d_s *ds)
{
  register int  i, j;
  int min, max, no;
  char  *cht;// , olig[32] question[512],

  if (ds == NULL) return 0;
  for (i = 0, min = max = (int)ds->xd[0]; i < ds->nx; i++)
    {
      min = ((int)ds->xd[i] < min) ? (int)ds->xd[i] : min;
      max = ((int)ds->xd[i] > max) ? (int)ds->xd[i] : max;
    }
  no = (int)max - (int)min;
  if (no > 64)
    {
      i = win_printf("You will have to enter %d oligos!\nDo you want to do that\n",max-min);
      if (i == WIN_CANCEL) return 1;
    }
  if (nseq > 0)
    {
      i = win_printf("There is already a set of oligo registered!\nDo you want to overide this data\n");
      if (i == WIN_CANCEL) return 1;
    }
  if (nseq > 0)
    {
      if (seq) free(seq);
      if (test) free(test);
      if (bestseq) free(bestseq);
      if (exactseq) free(exactseq);
      bestseq = test = seq = exactseq = NULL;
      nseq = 0;
    }
  if (nset > 0)
    {
      if (olset) free(olset);
      olset = NULL;
      nset = 0;
      mset = 0;
    }
  seq = (olihit*)calloc(ds->nx,sizeof(olihit));
  test = (olihit*)calloc(ds->nx,sizeof(olihit));
  bestseq = (olihit*)calloc(ds->nx,sizeof(olihit));
  exactseq = (olihit*)calloc(ds->nx,sizeof(olihit));
  if (seq == NULL || test == NULL || bestseq == NULL || exactseq == NULL)
      return win_printf_OK("Cannot allocate memory\n");
  nseq = ds->nx;
  nset = no+1;
  win_printf("Found no %d nset %d",no,nset);

  //nset = 64;
  olset = (oli*)calloc(nset,sizeof(oli));
  if (olset == NULL)     return win_printf_OK("Cannot allocate memory\n");


  for (i = 0; i < ds->nx; i++)
    {
      exactseq[i].z0 = exactseq[i].ze = exactseq[i].z = seq[i].z0 = test[i].z0 = seq[i].z = test[i].z = ds->yd[i];
      seq[i].noli = test[i].noli = (int)ds->xd[i];
      olset[(int)ds->xd[i]].used = 1;
    }
  for (i = 0; i < ds->nx; i++)
    {
      j = i + 1;
      j = (j < ds->nx) ? j : ds->nx - 1;
      seq[i].next = test[i].next = seq[j].noli;
    }
  for (i = 0; i < nset; i++)
    {
      /*
      snprintf(question,sizeof(question),"Define oligo %d sequence \n:%%20s",i);
      j = win_scanf(question,olig);
      if (j == WIN_CANCEL) return 1;
      */

      /*
      if (i == 0) cht = "AAA";
      else if (i == 1) cht = "AAC";
      else if (i == 2) cht = "AAG";
      else if (i == 3) cht = "AAT";
      else if (i == 4) cht = "ACA";
      else if (i == 5) cht = "ACC";
      else if (i == 6) cht = "ACG";
      else if (i == 7) cht = "ACT";
      else if (i == 8) cht = "AGA";
      else if (i == 9) cht = "AGC";
      else if (i == 10) cht = "AGG";
      else if (i == 11) cht = "AGT";
      else if (i == 12) cht = "ATA";
      else if (i == 13) cht = "ATC";
      else if (i == 14) cht = "ATG";
      else if (i == 15) cht = "ATT";
      else if (i == 16) cht = "CAA";
      else if (i == 17) cht = "CAC";
      else if (i == 18) cht = "CAG";
      else if (i == 19) cht = "CAT";
      else if (i == 20) cht = "CCA";
      else if (i == 21) cht = "CCC";
      else if (i == 22) cht = "CCG";
      else if (i == 23) cht = "CCT";
      else if (i == 24) cht = "CGA";
      else if (i == 25) cht = "CGC";
      else if (i == 26) cht = "CGG";
      else if (i == 27) cht = "CGT";
      else if (i == 28) cht = "CTA";
      else if (i == 29) cht = "CTC";
      else if (i == 30) cht = "CTG";
      else if (i == 31) cht = "CTT";
      else if (i == 32) cht = "GAA";
      else if (i == 33) cht = "GAC";
      else if (i == 34) cht = "GAG";
      else if (i == 35) cht = "GAT";
      else if (i == 36) cht = "GCA";
      else if (i == 37) cht = "GCC";
      else if (i == 38) cht = "GCG";
      else if (i == 39) cht = "GCT";
      else if (i == 40) cht = "GGA";
      else if (i == 41) cht = "GGC";
      else if (i == 42) cht = "GGG";
      else if (i == 43) cht = "GGT";
      else if (i == 44) cht = "GTA";
      else if (i == 45) cht = "GTC";
      else if (i == 46) cht = "GTG";
      else if (i == 47) cht = "GTT";
      else if (i == 48) cht = "TAA";
      else if (i == 49) cht = "TAC";
      else if (i == 50) cht = "TAG";
      else if (i == 51) cht = "TAT";
      else if (i == 52) cht = "TCA";
      else if (i == 53) cht = "TCC";
      else if (i == 54) cht = "TCG";
      else if (i == 55) cht = "TCT";
      else if (i == 56) cht = "TGA";
      else if (i == 57) cht = "TGC";
      else if (i == 58) cht = "TGG";
      else if (i == 59) cht = "TGT";
      else if (i == 60) cht = "TTA";
      else if (i == 61) cht = "TTC";
      else if (i == 62) cht = "TTG";
      else if (i == 63) cht = "TTT";
      */
      /*
      if (i == 0) cht = "CACACTTACA";
      else if (i == 1) cht = "CTTACACCC";
      else if (i == 2) cht = "CCCACCCTT";
      else if (i == 3) cht = "CCTTACATCT";
      else if (i == 4) cht = "ACATCTACCT";
      else if (i == 5) cht = "TACCTCCAC";
      else if (i == 6) cht = "CTCACACCC";
      else if (i == 7) cht = "ACCCCCCCT";
      else if (i == 8) cht = "TCCACACCC";
      else if (i == 9) cht = "ACTCCCACA";
      else if (i == 10) cht = "CCCACCCAC";
      else if (i == 11) cht = "CCACACTCA";
      */
      // David Salthouse <david.salthouse@depixus.com> test march 2017
      /*
      if (i == 0) cht = "GCATTTTGAC";
      else if (i == 1) cht = "TTGACGTAGG";
      else if (i == 2) cht = "GTAGGACCAT";
      else if (i == 3) cht = "ACCATAGCCG";
      else if (i == 4) cht = "AGCCGTCGTG";
      else if (i == 5) cht = "TCGTGCGGTG";
      else if (i == 6) cht = "CGGTGCCATC";
      else if (i == 7) cht = "CCATCACTTA";
      else if (i == 8) cht = "ACTTAACTGT";
      else if (i == 9) cht = "ACTGTGCTTT";
      else if (i == 10) cht = "GCTTTCAGTT";
      else if (i == 11) cht = "CAGTTTAATT";
      else if (i == 12) cht = "TAATTACCTG";
      else if (i == 13) cht = "ACCTGTTTGA";
      else if (i == 14) cht = "TTTGACCCAC";
      else if (i == 15) cht = "CCCACTAAGC";
      else if (i == 16) cht = "TAAGCGTTTG";
      else if (i == 17) cht = "GTTTGCAAGC";
      else if (i == 18) cht = "CAAGCAGCCA";
      else if (i == 19) cht = "AGCCA";
      */

      // David Salthouse <david.salthouse@depixus.com> test march 2017
      if (i == 0) cht =	"aagt";
      else if (i == 1) cht =	"aata";
      else if (i == 2) cht =	"acat";
      else if (i == 3) cht =	"actc";
      else if (i == 4) cht =	"actt";
      else if (i == 5) cht =	"agac";
      else if (i == 6) cht =	"aggt";
      else if (i == 7) cht =	"agta";
      else if (i == 8) cht =	"agtg";
      else if (i == 9) cht =	"atag";
      else if (i == 10) cht =	"atcc";
      else if (i == 11) cht =	"cact";
      else if (i == 12) cht =	"caga";
      else if (i == 13) cht =	"cagg";
      else if (i == 14) cht =	"catc";
      else if (i == 15) cht =	"ccag";
      else if (i == 16) cht =	"ctca";
      else if (i == 17) cht =	"ctta";
      else if (i == 18) cht =	"cttt";
      else if (i == 19) cht =	"gaag";
      else if (i == 20) cht =	"gaat";
      else if (i == 21) cht =	"gaca";
      else if (i == 22) cht =	"gcac";
      else if (i == 23) cht =	"ggtc";
      else if (i == 24) cht =	"ggtt";
      else if (i == 25) cht =	"gtac";
      else if (i == 26) cht =	"gtct";
      else if (i == 27) cht =	"gtga";
      else if (i == 28) cht =	"gtgt";
      else if (i == 29) cht =	"gttg";
      else if (i == 30) cht =	"tact";
      else if (i == 31) cht =	"tagt";
      else if (i == 32) cht =	"tcac";
      else if (i == 33) cht =	"tcag";
      else if (i == 34) cht =	"tcca";
      else if (i == 35) cht =	"tctt";
      else if (i == 36) cht =	"tgaa";
      else if (i == 37) cht =	"tgca";
      else if (i == 38) cht =	"tggt";
      else if (i == 39) cht =	"tgtg";
      else if (i == 40) cht =	"ttgc";
      else if (i == 41) cht =	"ttgg";
      else if (i == 42) cht =	"tttg";
      else
	{
	 win_printf_OK("wrong number of oligos i = %d nset %d\n",i,nset);
	}
      olset[i].len = strlen(cht);
      for (j = 0; j < olset[i].len; j++)
	olset[i].seq[j] = cht[j];
      olset[i].seq[j] = 0;
    }
  return 0;
}


d_s *alocate_oligo_and_olihit(char *file)
{
  register int  i, j;
  char  line[256], oli1[32];// , olig[32] question[512],
  FILE *fp;
  float pos, za, exact_pos;
  //static float noise = 3;
  d_s *dsi = NULL, *dst = NULL, *dst2 = NULL;

  if (file == NULL) return NULL;
  fp = fopen(file,"r");
  if (fp == NULL) return NULL;

  if (nset > 0)
    {
      i = win_scanf("A set of oligo is already recorded\nContinuing will erase it!");
      if (i == WIN_CANCEL) return NULL;
      if (olset) free(olset);
      olset = NULL;
      nset = 0;
      mset = 0;
    }
  if (nseq > 0)
    {
      i = win_printf("There is already a set of oligo registered!\nDo you want to overide this data\n");
      if (i == WIN_CANCEL) return NULL;
    }
  if (nseq > 0)
    {
      if (seq) free(seq);
      if (test) free(test);
      if (bestseq) free(bestseq);
      if (exactseq) free(exactseq);
      bestseq = test = seq = exactseq = NULL;
      nseq = 0;
    }

  dsi = build_data_set(16,16);
  if (dsi == NULL) return NULL;
  dsi->nx = dsi->ny = 0;
  dst = build_data_set(16,16);
  if (dst == NULL) return NULL;
  dst->nx = dst->ny = 0;
  dst2 = build_data_set(16,16);
  if (dst2 == NULL) return NULL;
  dst2->nx = dst2->ny = 0;
  //i = win_scanf("Add noise to position %5f nm",&noise);
  int k, jj;
  for(i = 1; i == 1; )
    {
      i = (fgets(line, sizeof(line), fp) == NULL) ? 0 : 1;
      if (i)
        {
	  j = sscanf(line,"%f %s %f",&exact_pos,oli1,&pos);
	  if (j < 3)
	    {
	      // we look for sequence oligo_position and eventually move_position
	      j = sscanf(line,"%s %f %f",oli1,&pos,&za);
	      exact_pos = pos;
	      if (j == 2) za = pos;
	      exact_sequence_known = 0;
	    }
	  else
	    {
	      exact_sequence_known = 1;
	      za = pos;
	    }
	  if (j >= 2)
	    {
	      //if (noise > 0)
	      //pos += noise*gasdev(&idum);
	      //if (j >= 2) za = pos;
	      if (strlen(oli1) > 0)
		{
		  for (k = 0; k < nset; k++)
		    {
		      if (strncmp(oli1,olset[k].seq,olset[k].len) == 0)
			break;
		    }
		  if (k < nset)
		    {

		      add_new_point_to_ds(dsi, k, pos);
		      add_new_point_to_ds(dst, k, za);
		      add_new_point_to_ds(dst2, k, exact_pos);
		      //win_printf("oligo %d already known seq %s pos %f nset %d",k,oli1,pos,nset);
		    }
		  else
		    {
		      mset += 16;
		      if (mset == 16)
			olset = (oli*)calloc(mset,sizeof(oli));
		      else
			olset = (oli*)realloc(olset,mset*sizeof(oli));
		      if (olset == NULL)
			return win_printf_ptr("Cannot allocate memory\n");
		      olset[k].len = strlen(oli1);
		      for (jj = 0; jj < olset[k].len; jj++)
			olset[k].seq[jj] = oli1[jj];
		      olset[k].seq[jj] = 0;
		      add_new_point_to_ds(dsi, k, pos);
		      add_new_point_to_ds(dst, k, za);
		      add_new_point_to_ds(dst2, k, exact_pos);
		      nset = k+1;
		      //win_printf("new oligo %d seq %s pos %f nset %d",k,oli1,pos,nset);

		    }
		}
	    }
	}
    }
  fclose(fp);
  seq = (olihit*)calloc(dsi->nx,sizeof(olihit));
  test = (olihit*)calloc(dsi->nx,sizeof(olihit));
  bestseq = (olihit*)calloc(dsi->nx,sizeof(olihit));
  exactseq = (olihit*)calloc(dsi->nx,sizeof(olihit));
  if (seq == NULL || test == NULL || bestseq == NULL || exactseq == NULL)
      return win_printf_ptr("Cannot allocate memory\n");
  nseq = dsi->nx;

  for (i = 0; i < dsi->nx; i++)
    {
      exactseq[i].z0 = seq[i].z0 = test[i].z0 = dsi->yd[i];
      exactseq[i].z = exactseq[i].ze = seq[i].ze = test[i].ze = dst2->yd[i];
      seq[i].z = test[i].z = dst->yd[i];
      exactseq[i].noli = seq[i].noli = test[i].noli = (int)dsi->xd[i];
      olset[(int)dsi->xd[i]].used = 1;
    }
  sort_ds_along_y(dst2);
  QuickSort_oligo_hit(exactseq, 0, dsi->nx-1);
  compute_energies(exactseq, dsi->nx);
  for (i = 0; i < dst2->nx; i++)
    {
      //exactseq[(int)dst2->xd[i]].i_ze = i;
      exactseq[i].i_ze = i;
      seq[(int)dst2->xd[i]].i_ze = i;
      test[(int)dst2->xd[i]].i_ze = i;
    }
  int l;
  for (k = 0; k < dsi->nx-1; k++)
    {
      l = win_printf("Oli %d -> %g (%g), seq %s Id %d xe %d\n"
		     "oli %d -> %g (%g), seq %s Id %d xe %d\n"
		     ,k,exactseq[k].ze,exactseq[k].z0, olset[exactseq[k].noli].seq
		     ,exactseq[k].noli, exactseq[k].i_ze
		     ,k+1,exactseq[k+1].ze, exactseq[k+1].z0,olset[exactseq[k+1].noli].seq
		     ,exactseq[k+1].noli, exactseq[k+1].i_ze
		     );
      if (l == WIN_CANCEL) break;
    }

  for (i = 0; i < dsi->nx; i++)
    {
      j = i + 1;
      j = (j < dsi->nx) ? j : dsi->nx - 1;
      seq[i].next = test[i].next = seq[j].noli;
    }

  if (dst) free_data_set(dst);
  if (dst2) free_data_set(dst2);
  return dsi;
}


int alocate_olihit_from_sequence(char *toseq, int size, int olisize, float sim_er)
{
  register int  i, j;
  char  cht[32];// , olig[32] question[512],


  if (nseq > 0)
    {
      i = win_printf("There is already a set of oligo registered!\nDo you want to overide this data\n");
      if (i == WIN_CANCEL) return 0;
    }
  if (nseq > 0)
    {
      if (seq) free(seq);
      if (test) free(test);
      if (bestseq) free(bestseq);
      if (exactseq) free(exactseq);
      bestseq = test = seq = exactseq = NULL;
      nseq = 0;
    }
  if (nset > 0)
    {
      if (olset) free(olset);
      olset = NULL;
      nset = 0;
    }
  seq = (olihit*)calloc(size-olisize,sizeof(olihit));
  test = (olihit*)calloc(size-olisize,sizeof(olihit));
  bestseq = (olihit*)calloc(size-olisize,sizeof(olihit));
  exactseq = (olihit*)calloc(size-olisize,sizeof(olihit));
  if (seq == NULL || test == NULL || bestseq == NULL || exactseq == NULL)
      return win_printf_OK("Cannot allocate memory\n");
  nseq = size-olisize;
  for (i = 0, j = 1; i < olisize; i++, j *= 4);
  nset = j;
  olset = (oli*)calloc(nset,sizeof(oli));
  if (olset == NULL)     return win_printf_OK("Cannot allocate memory\n");

  for (i = 0; i < size-olisize; i++)
    {
      seq[i].z0 = test[i].z0 = seq[i].ze = test[i].ze = seq[i].z = test[i].z = i;
      exactseq[i].z0 = exactseq[i].ze = exactseq[i].z = i;
      exactseq[i].z0 = seq[i].z0 = seq[i].z = test[i].z0 = test[i].z = test[i].ze + (sim_er * gasdev(&idum));
      j = convert_oligo_seq_to_ID(toseq+i, olisize);
      exactseq[i].noli = seq[i].noli = test[i].noli = j;
      olset[j].used = 1;
    }
  for (i = 0; i < size-olisize; i++)
    {
      j = i + 1;
      j = (j < size-olisize) ? j : size-olisize - 1;
      seq[i].next = test[i].next = seq[j].noli;
    }
  for (i = 0; i < nset; i++)
    {
      convert_oligoID_to_seq(cht, i, olisize);
      olset[i].len = strlen(cht);
      for (j = 0; j < olset[i].len; j++)
	olset[i].seq[j] = cht[j];
      olset[i].seq[j] = 0;
    }
  return 0;
}







int do_assembleOligos_hello(void)
{
  register int i;

  if(updating_menu_state != 0)	return D_O_K;

  i = win_printf("Hello from assembleOligos");
  if (i == WIN_CANCEL) win_printf_OK("you have press CANCEL");
  else win_printf_OK("you have press OK");
  return 0;
}



int assembleOligos_idle_action(O_p *op, DIALOG *d)
{
  register int i, j, k;
  d_s *ds;
  pltreg *pr = NULL;
  float testp, prob;
  double E, Et, Fz, Fh;
  char message[256];
  int nb, nbt, flip_break = 0, i_1, i1, l, ii, packet_n; // , ntarget
  static int *oli_to_mov = NULL;
  static int n_oli_to_mov = 0;
  //olihit y;
  int reb_pos = 0, mov_zero = 0, mov_ref = 0, flip_T = 0, flip_E = 0, p_s = 0, n_ps = 0, mv_s = 0, nmv = 0;
  unsigned long t_c, to = 0;

  (void)d;
  if (seq == NULL) return 0;
  if (domt == 0) return 0;

  if (n_oli_to_mov < nseq)
    {
      free(oli_to_mov);
      oli_to_mov = NULL;
      n_oli_to_mov = 0;
    }
  if (n_oli_to_mov == 0)
    {
      oli_to_mov = calloc(nseq,sizeof(int));
      if (oli_to_mov == NULL) return 0;
      n_oli_to_mov = nseq;
    }
  if (oli_to_mov == NULL) return 0;

  E = compute_energies(seq, nseq);
  Fz = Edz2l;
  Fh = Ehtl;
  nb = nbreak;

  if ((itert - last_iter_of_T_step) > nb_iter_at_T)
    {
      last_iter_of_T_step = itert;
      ture *= Tdec;
    }

  if ((nb_iter_to_restart_from_last_best > 0)
      && ((itert - last_best_iter) > nb_iter_to_restart_from_last_best))
    {
      E = Emin;
      for (k = 0; k < nseq; k++)
	seq[k] = bestseq[k];
      last_best_iter = itert;
   }
  for (k = 0; k < nseq; k++)
    test[k] = seq[k];

   E = compute_energies(seq, nseq);
   Fz = Edz2l;
   Fh = Ehtl;
   nb = nbreak;


   if (opflip != NULL && dsflip != NULL)
     {
       ds = dsflip;
       for (k = 0; k < nseq && k < dsflip->nx; k++)
	 {
	   ds->yd[k] = 0;
	   ds->xd[k] = k;
	 }
       opflip->need_to_refresh = 1;
     }

   // action will not last more than 1/10s
  for (j = 0, t_c = my_uclock() + (MY_UCLOCKS_PER_SEC/10); j < 10000  && my_uclock() < t_c; j++)
    {
      /*
      for (m = k = 0; m < nseq-1; m++)
	{
	  oli_to_mov[m] = (seq[m].Enext >= -1) ? 1 : 0;
	  k += oli_to_mov[m];
	}
      oli_to_mov[nseq-1] = 1; // we move the last one (no hibryd)
      ntarget = k;
      k+=1;
      k = (k <= nseq) ? k : nseq;
      i = (int)(ran1(&idum)*k);// (int)(ran1(&idum)*nseq);
      if (i >= nseq) continue;
      */
      //      zwas = test[i].z;
      //if (j%2)
      //test[i].z = test[i].z0 + (dz_trial * gasdev(&idum))/bp_to_nm;

      //else
      //{

      /*
      for (m = k = 0; m < nseq-1; m++)
	{
	  k += oli_to_mov[m];
	  if (k == i) break;
	}
      i = (k != i) ? (int)(ran1(&idum)*nseq) : m;
      */
      i = (int)(ran1(&idum)*nseq);
      i = (i >= nseq) ? nseq-1 : i;

      if (opflip != NULL && dsflip != NULL)
	{
	  ds = dsflip;
	  ds->yd[i] += 1;
	  opflip->need_to_refresh = 1;
	}
      packet_n = (int)( packet_size * fabs(gasdev(&idum)));
      packet_n += 1;
      ii = (int)(dz_trial * gasdev(&idum)); // (dz_trial * gasdev(&idum)/packet_n);

      if (ii == 0) continue;
      //win_printf("j %d: i = %d, ii %d, packet %d",j,i,ii,packet_n);

      if (ii > 0)
	{
	  if (((i + packet_n + ii + packet_n) > nseq)
	      || ((i + packet_n + ii) > nseq)
	      || ((i + packet_n + packet_n) > nseq))
	    {
	      reb_pos++;
	      continue;
	    }
	  // we remove the packet
	  for (k = 0; k < i; k++)
	    test[k] = seq[k];
	  for (k = i+packet_n; k < nseq; k++)
	    test[k-packet_n] = seq[k];
	  // we make room for the packet in new position
	  for (k = nseq-1; k >= i+ii+packet_n; k--)
	    test[k] = test[k-packet_n];
	  // we incorporate the packet
	  for (k = 0; k < packet_n; k++)
	    test[i+ii+k] = seq[i+k];
	}

      if (ii < 0)
	{
	  if ((i - packet_n + ii < 0)
	      || ((i + ii) < 0)
	      ||  ((i + packet_n) > nseq))
	    {
	      reb_pos++;
	      continue;
	    }
	  // we remove the packet
	  for (k = 0; k < i; k++)
	    test[k] = seq[k];
	  for (k = i+packet_n; k < nseq; k++)
	    test[k-packet_n] = seq[k];
	  // we make room for the packet in new position
	  for (k = nseq-1; k >= i+ii+packet_n; k--)
	    test[k] = test[k-packet_n];
	  // we incorporate the packet
	  for (k = 0; k < packet_n; k++)
	    test[i+ii+k] = seq[i+k];
	}

      /*
	  if (ii < 0 || ii >= nseq || ((ii + packet_n) >= nseq))
	    {
	      reb_pos++;
	      continue;
	    }
	  y = test[i]; test[i] = test[ii]; test[ii] = y;
      */
	  //}

	  //QuickSort_oligo_hit(test, 0, nseq-1);
      /*
      if (diff_olihit(seq, test, nseq) == 0)
	{  // move without energy change
	  Et = compute_energies(test, nseq);
	  for (k = 0; k < nseq; k++)
	    seq[k] = test[k];
	  E = Et;
	  mov_zero++;
	  continue;
	}
      */
      Et = compute_energies(test, nseq);
      nbt = nbreak;
      i_1 = (i > 0) ? i-1 : 0;
      i1 = (i < nseq-1) ? i+1 : nseq-1;

      //if (j < 3)  win_printf("i = %d dz = %g",i,test[i].z - test[i].z0);
      /*
      if (debug == 2)
	{
	  snprintf(message,sizeof(message),"iter %d flip %d aiming seq %d dz %g\n"
		   "Et %g (dz %g hyb %g) -> %d(%d) E %g (dz %g hyb %g) -> %d(%d)\n"
		   "Debug Stop%%R flip only %%r all %%r"
		   ,itert,flip,i,test[i].z - test[i].z0,Et,Edz2l, Ehtl,test[i].noli,nbreak,E,Fz,Fh,seq[i].noli,nb);
	  win_scanf(message,&debug);
	}
      */

      if (Et >= E)
	{
	    testp = ran1(&idum);
	    prob = (ture > 0) ? exp(-(Et-E)/(nseq*ture)) : 0;
	    if (prob > testp)
	      {
		if (debug == 1)
		  {
		    snprintf(message,sizeof(message),"temperature induced flip\n iter %d flip %d aiming seq %d dz %g\n"
			     "Et %lg (dz %lg hyb %lg) -> [%d,%d,%d](%d) \n"
			     "E %lg (dz %lg hyb %lg) -> [%d,%d,%d](%d)\n"
			     "Debug Stop%%R flip only %%r all %%r"
			     ,itert,flip,i,test[i].z - test[i].z0
			     ,Et,Edz2l,Ehtl,test[i_1].noli,test[i].noli,test[i1].noli,nbreak
			     ,E,Fz,Fh,seq[i_1].noli,seq[i].noli,seq[i1].noli,nb);
		    k = win_scanf(message,&debug);

		    if (k != WIN_CANCEL)
		      {
			for (k = 0; k < nseq-1; k++)
			  {
			    l = win_printf("Test Oli %d -> %d, z %g z+1 %g\nseq %d %s nhib %d\n"
					   "seq %d %s shited by %d\n"
					   "Seq  Oli %d -> %d, z %g z+1 %g\nseq %d %s nhib %d\n"
					   "seq %d %s shited by %d"
					   ,k,k+1,test[k].z,test[k+1].z,test[k].noli,olset[test[k].noli].seq,test[k].nhnext
					   ,test[k+1].noli,olset[test[k+1].noli].seq,test[k].dnext
					   ,k,k+1,seq[k].z,seq[k+1].z,seq[k].noli,olset[seq[k].noli].seq,seq[k].nhnext
					   ,seq[k+1].noli,olset[seq[k+1].noli].seq,seq[k].dnext
					   );
			    if (l == WIN_CANCEL) break;
			  }
		      }
		  }
		for (k = 0; k < nseq; k++)
		    seq[k] = test[k];
		E = compute_energies(seq, nseq);
		Fz = Edz2l;
		Fh = Ehtl;
		if (nbt > nb) flip_break++;
		nb = nbreak;
		p_s += packet_n;
		n_ps++;
		mv_s += abs(ii);
		nmv++;
		if (dsEt != NULL)
		  {
		    if ((dsEt->nx == 0) || (dsEt->yd[dsEt->nx-1] > (float)E))
		      {
			add_new_point_to_ds(dsEt, 1e-6*(itert+j), E);
			if (dsEz != NULL)   add_new_point_to_ds(dsEz, 1e-6*(itert+j), Fz);
			if (dsEh != NULL)   add_new_point_to_ds(dsEh, 1e-6*(itert+j), Fh);
		      }
		  }

		if (ope != NULL)   ope->need_to_refresh = 1;

		flip++;
		flip_T++;
		if (imres != NULL && n_acq_im < max_acq_im && nb == 0)
		  {
		    for (k = 0; k < nseq && k < imres->im.nx; k++)
		      {
			if (seq[k].noli >= 0 && seq[k].noli < imres->im.ny)
			  imres->im.pixel[seq[k].noli].fl[2*k] += 1;
		      }
		    n_acq_im++;
		    set_im_title(imres, "Possible oligos (%d/%d)",n_acq_im,max_acq_im);
		    imres->need_to_refresh = ALL_NEED_REFRESH;
		  }

		if (ople != NULL && dslem != NULL && nbreak == 0)
		  {
		    ds = dslem;
		    for (k = 0; k < nseq && k < ds->nx; k++)
		      {
			ds->yd[k] = (0.9999*ds->yd[k]) + (0.0001*(seq[k].z - seq[k].z0)*(seq[k].z - seq[k].z0));
			ds->xd[k] = k;
		      }
		    ople->need_to_refresh = 1;
		  }


		//if (testw != WIN_CANCEL)
		//testw = win_printf("up hill j %d E = %g E test = %g, nbreak %d Edz2 %g Eh %g",j,E,Et,nbreak,  Edz2l, Ehtl);
	      }
	    else
	      {
		if (debug == 2)
		  {
		    snprintf(message,sizeof(message),"refused flip\n iter %d flip %d aiming seq %d dz %g\n"
			     "Et %lg (dz %lg hyb %lg) -> [%d,%d,%d](%d) E %lg (dz %lg hyb %lg) -> [%d,%d,%d](%d)\n"
			     "testp %g prob %g\n"
			     "Debug Stop%%R flip only %%r all %%r"
			     ,itert,flip,i,test[i].z - test[i].z0
			     ,Et,Edz2l,Ehtl,test[i_1].noli,test[i].noli,test[i1].noli,
			     nbreak,E,Fz,Fh,seq[i_1].noli,seq[i].noli,seq[i1].noli,nb,testp,prob);
		    k = win_scanf(message,&debug);

		    if (k != WIN_CANCEL)
		      {
			for (k = 0; k < nseq-1; k++)
			  {
			    l = win_printf("Test Oli %d -> %d, z %g z+1 %g\nseq %d %s nhib %d\n"
					   "seq %d %s shited by %d\n"
					   "Seq  Oli %d -> %d, z %g z+1 %g\nseq %d %s nhib %d\n"
					   "seq %d %s shited by %d"
					   ,k,k+1,test[k].z,test[k+1].z,test[k].noli,olset[test[k].noli].seq,test[k].nhnext
					   ,test[k+1].noli,olset[test[k+1].noli].seq,test[k].dnext
					   ,k,k+1,seq[k].z,seq[k+1].z,seq[k].noli,olset[seq[k].noli].seq,seq[k].nhnext
					   ,seq[k+1].noli,olset[seq[k+1].noli].seq,seq[k].dnext
					   );
			    if (l == WIN_CANCEL) break;
			  }
		      }

		  }
		mov_ref++;
		for (k = 0; k < nseq; k++)
		    test[k] = seq[k];
	      }
	}
      else
	{
	  if (debug == 1)
	    {
	      snprintf(message,sizeof(message),"direct flip\n iter %d flip %d aiming seq %d dz %g\n"
		       "Et %lg (dz %lg hyb %lg) -> [%d,%d,%d](%d) E %lg (dz %lg hyb %lg) -> [%d,%d,%d](%d)\n"
		       "Debug Stop%%R flip only %%r all %%r"
		       ,itert,flip,i,test[i].z - test[i].z0
		       ,Et,Edz2l,Ehtl,test[i_1].noli,test[i].noli,test[i1].noli,
		       nbreak,E,Fz,Fh,seq[i_1].noli,seq[i].noli,seq[i1].noli,nb);
	      k = win_scanf(message,&debug);

	      if (k != WIN_CANCEL)
		{
		  for (k = 0; k < nseq-1; k++)
		    {
		      l = win_printf("Test Oli %d -> %d, z %g z+1 %g\nseq %d %s nhib %d\n"
				     "seq %d %s shited by %d\n"
				     "Seq  Oli %d -> %d, z %g z+1 %g\nseq %d %s nhib %d\n"
				     "seq %d %s shited by %d"
				     ,k,k+1,test[k].z,test[k+1].z,test[k].noli,olset[test[k].noli].seq,test[k].nhnext
				     ,test[k+1].noli,olset[test[k+1].noli].seq,test[k].dnext
				     ,k,k+1,seq[k].z,seq[k+1].z,seq[k].noli,olset[seq[k].noli].seq,seq[k].nhnext
				     ,seq[k+1].noli,olset[seq[k+1].noli].seq,seq[k].dnext
				     );
		      if (l == WIN_CANCEL) break;
		    }
		}

	    }
	  for (k = 0; k < nseq; k++)
	    seq[k] = test[k];
	  E = compute_energies(seq, nseq);
	  Fz = Edz2l;
	  Fh = Ehtl;
	  nb = nbreak;
	  p_s += packet_n;
	  n_ps++;
	  mv_s += abs(ii);
	  nmv++;
	  if (E < Emin)
	    {
	      Emin = E;
	      for (k = 0; k < nseq; k++)
		bestseq[k] = seq[k];
	      last_best_iter = itert + j;
	      if (exact_sequence_known == 1)
		{
		  for (k = 0; k < nseq; k++)
		    if (bestseq[k].noli != exactseq[k].noli)
		      break;
		  if (k == nseq)
		    {
		      seq_found = 1;
		      domt = 0;
		    }
		}

	    }
	  if (dsEt != NULL)
	    {
	      if ((dsEt->nx == 0) || (dsEt->yd[dsEt->nx-1] > (float)E))
		{
		  add_new_point_to_ds(dsEt, 1e-6*(itert + j), E);
		  if (dsEz != NULL)    add_new_point_to_ds(dsEz, 1e-6*(itert + j), Fz);
		  if (dsEh != NULL)    add_new_point_to_ds(dsEh, 1e-6*(itert + j), Fh);
		}
	    }
	  if (ope != NULL)   ope->need_to_refresh = 1;
	  flip++;
	  flip_E++;
	  if (ople != NULL && dslem != NULL && nbreak == 0)
	    {
	      ds = dslem;
	      for (k = 0; k < nseq && k < ds->nx; k++)
		{
		  ds->yd[k] = (0.9999*ds->yd[k]) + (0.0001*(seq[k].z - seq[k].z0)*(seq[k].z - seq[k].z0));
		  ds->xd[k] = k;
		}
	      ople->need_to_refresh = 1;
	    }

	  if (imres != NULL && n_acq_im < max_acq_im && nb == 0)
	    {
	      for (k = 0; k < nseq && k < imres->im.nx; k++)
		{
		  if (seq[k].noli >= 0 && seq[k].noli < imres->im.ny)
		    imres->im.pixel[seq[k].noli].fl[2*k] += 1;
		}
	      n_acq_im++;
	      set_im_title(imres, "Possible oligos (%d/%d)",n_acq_im,max_acq_im);
	      imres->need_to_refresh = ALL_NEED_REFRESH;
	    }
	  //if (testw != WIN_CANCEL)
	    //testw = win_printf("j %d E = %g E test = %g, nbreak %d Edz2 %g Eh %g",j,E,Et,nbreak,  Edz2l, Ehtl);
	}

    }
  itert += j;
  t_c = my_uclock() - t_c;
  to = MY_UCLOCKS_PER_SEC;
  E = compute_energies(seq, nseq);
  //win_printf("j %d E = %g E test = %g, nbreak %d Edz2 %g Eh %g",j,E,Et,nbreak,  Edz2l, Ehtl);



  if (oppa != NULL && dspa != NULL)
    {
      ds = dspa;
      for (j = 0; j < nseq && j < ds->nx; j++)
	{
	  ds->yd[j] = seq[j].noli;//z0 noli;
	  ds->xd[j] = j;//seq[j].z;
	}
      oppa->need_to_refresh = 1;
    }

  if (oppa != NULL && dspab != NULL)
    {
      ds = dspab;
      for (j = 0; j < nseq && j < ds->nx; j++)
	{
	  ds->yd[j] = bestseq[j].noli;//z0 noli;
	  ds->xd[j] = j;//seq[j].z;
	}
      oppa->need_to_refresh = 1;
    }

  if (oppa != NULL && dspae != NULL)
    {
      ds = dspae;
      for (j = 0; j < nseq && j < ds->nx; j++)
	{
	  ds->yd[j] = exactseq[j].noli;//noli;
	  ds->xd[j] = exactseq[j].i_ze;//seq[j].z;
	}
      oppa->need_to_refresh = 1;
    }



  if (ople != NULL && dslez != NULL)
    {
      ds = dslez;
      for (j = 0; j < nseq && j < ds->nx; j++)
	{
	  ds->yd[j] = seq[j].Edz2;
	  ds->xd[j] = j;//seq[j].z;
	}
      ople->need_to_refresh = 1;
    }
  if (ople != NULL && dshy != NULL)
    {
      ds = dshy;
      for (j = 0; j < nseq-1 && j < ds->nx; j++)
	{
	  ds->yd[j] = bestseq[j].Enext;//seq[j].Enext;
	  ds->xd[j] = j;//seq[j].z;
	}
      ds->nx = j;
      ople->need_to_refresh = 1;
    }
  if (ople != NULL && dslet != NULL)
    {
      ds = dslet;
      for (j = 0; j < nseq && j < ds->nx; j++)
	{
	  ds->yd[j] = bestseq[j].Enext + bestseq[j].Edz2;
	  ds->xd[j] = j;
	}
      ople->need_to_refresh = 1;
    }

  if (opbpz != NULL && dsbpz != NULL)
    {
      ds = dsbpz;
      int noli, len, pos;
      for (j = 0, pos = 0; j < nseq; j++)
	{
	  noli = seq[j].noli;
	  len = (seq[j].dnext == 0) ? olset[noli].len : seq[j].dnext;
	  ds->yd[j] = seq[j].z0;//noli;
	  ds->xd[j] = pos;//j;//test[j].z;
	  pos += len;
	}
      opbpz->need_to_refresh = 1;
    }
  if (opbpz != NULL && dsbpze != NULL)
    {
      ds = dsbpze;
      int noli, len, pos;
      for (j = 0, pos = 0; j < nseq; j++)
	{
	  noli = exactseq[j].noli;
	  len = (exactseq[j].dnext == 0) ? olset[noli].len : exactseq[j].dnext;
	  ds->yd[j] = exactseq[j].z0;//noli;
	  ds->xd[j] = pos;//j;//test[j].z;
	  pos += len;
	}
    }

  if (opbpz != NULL && dsbpzb != NULL)
    {
      ds = dsbpzb;
      int noli, len, pos;
      for (j = 0, pos = 0; j < nseq; j++)
	{
	  noli = bestseq[j].noli;
	  len = (bestseq[j].dnext == 0) ? olset[noli].len : bestseq[j].dnext;
	  ds->yd[j] = bestseq[j].z0;//noli;
	  ds->xd[j] = pos;//j;//test[j].z;
	  pos += len;
	}
      opbpz->need_to_refresh = 1;
    }


  set_plot_title(op, "\\stack{{\\pt8 Break %d Edz^2 %lg E_{hyb} %lg E = %lg nmbp %g}"
		 "{\\pt7 iter %d flipT %d flip break %d flipE %d neut. mov %d packet %g mv %g}"
		 "{\\pt7 trermal refused %d pos refused %d (%g ms) T = %g %s}}"
		 ,nbreak, Edz2l, Ehtl,E,nm_to_bp,itert,flip_T,flip_break,flip_E,mov_zero
		 ,(n_ps > 0) ? (float)p_s/n_ps : 0, (nmv > 0) ? (float)mv_s/nmv : 0
		 , mov_ref ,reb_pos,(double)(t_c*1000)/to,ture, (seq_found) ? "Found Seq":"");

  op->need_to_refresh = 1;
  if ((pr = find_pr_in_current_dialog(NULL)) == NULL)   return 0;
  refresh_plot(pr, UNCHANGED);
  return 0;
}
int assembleOligos_idle_action_simul(O_p *op, DIALOG *d)
{
  register int i, j, k;
  d_s *ds;
  pltreg *pr = NULL;
  float testp, prob, dz;
  double E, Et, Fz, Fh;
  char message[256];
  int nb, i_1, i1, l, ii, dk;
  olihit y;

  (void)d;
  if (seq == NULL) return 0;
  if (domt == 0) return 0;

  E = compute_energies_2(seq, nseq, olset[0].len);
  Fz = Edz2l;
  Fh = Ehtl;
  nb = nbreak;
  for (j = 0; j < 10000; j++)
    {
      i = (int)(ran1(&idum)*nseq);
      if (i >= nseq) continue;
      //      zwas = test[i].z;
      if (j%2)      test[i].z = test[i].z0 + (dz_trial * gasdev(&idum))/bp_to_nm;
      else
	{
	  ii = i + (int)(dz_trial * gasdev(&idum));
	  if (ii < 0 || ii >= nseq || ii == i)  continue;
	  if (j%4 == 0)
	    {
	      y = test[i]; test[i] = test[ii]; test[ii] = y;
	    }
	  else
	    {
	      dk = (ii-i >= 0) ? 1 : -1;
	      dz = (dz_trial * gasdev(&idum))/bp_to_nm;
	      if (dk >0)
		{
		  for (k = i; k <= ii; k++) test[k].z = test[k].z + dz;
		}
	      else
		{
		  for (k = ii; k <= i; k++) test[k].z = test[k].z + dz;
		}
	    }
	}
      QuickSort_oligo_hit(test, 0, nseq-1);
      Et = compute_energies_2(test, nseq, olset[0].len);
      i_1 = (i > 0) ? i-1 : 0;
      i1 = (i < nseq-1) ? i+1 : nseq-1;

      //if (j < 3)  win_printf("i = %d dz = %g",i,test[i].z - test[i].z0);
      /*
      if (debug == 2)
	{

	  snprintf(message,sizeof(message),"iter %d flip %d aiming seq %d dz %g\n"
		   "Et %g (dz %g hyb %g) -> %d(%d) E %g (dz %g hyb %g) -> %d(%d)\n"
		   "Debug Stop%%R flip only %%r all %%r"
		   ,itert,flip,i,test[i].z - test[i].z0,Et,Edz2l, Ehtl,test[i].noli,nbreak,E,Fz,Fh,seq[i].noli,nb);
	  win_scanf(message,&debug);
	}
      */

      if (Et >= E)
	{
	    testp = ran1(&idum);
	    prob = exp(-(Et-E)/(nseq*ture));
	    if (prob > testp)
	      {
		if (debug == 1)
		  {
		    snprintf(message,sizeof(message),"temperature induced flip\n iter %d flip %d aiming seq %d dz %g\n"
			     "Et %g (dz %g hyb %g) -> [%d,%d,%d](%d) \n"
			     "E %g (dz %g hyb %g) -> [%d,%d,%d](%d)\n"
			     "Debug Stop%%R flip only %%r all %%r"
			     ,itert,flip,i,test[i].z - test[i].z0
			     ,Et,Edz2l,Ehtl,test[i_1].noli,test[i].noli,test[i1].noli,nbreak
			     ,E,Fz,Fh,seq[i_1].noli,seq[i].noli,seq[i1].noli,nb);
		    k = win_scanf(message,&debug);

		    if (k != WIN_CANCEL)
		      {
			for (k = 0; k < nseq-1; k++)
			  {
			    l = win_printf("Test Oli %d -> %d, z %g z+1 %g\nseq %d %s nhib %d\n"
					   "seq %d %s shited by %d\n"
					   "Seq  Oli %d -> %d, z %g z+1 %g\nseq %d %s nhib %d\n"
					   "seq %d %s shited by %d"
					   ,k,k+1,test[k].z,test[k+1].z,test[k].noli,olset[test[k].noli].seq,test[k].nhnext
					   ,test[k+1].noli,olset[test[k+1].noli].seq,test[k].dnext
					   ,k,k+1,seq[k].z,seq[k+1].z,seq[k].noli,olset[seq[k].noli].seq,seq[k].nhnext
					   ,seq[k+1].noli,olset[seq[k+1].noli].seq,seq[k].dnext
					   );
			    if (l == WIN_CANCEL) break;
			  }
		      }
		  }
		for (k = 0; k < nseq; k++)
		  {
		    seq[k].noli = test[k].noli;
		    seq[k].z0 = test[k].z0;
		    seq[k].z = test[k].z;
		    seq[k].ze = test[k].ze;
		    seq[k].Edz2 = test[k].Edz2;
		    seq[k].next = test[k].next;
		    seq[k].nhnext = test[k].nhnext;
		    seq[k].dnext = test[k].dnext;
		    seq[k].Enext = test[k].Enext;
		  }
		E = compute_energies_2(seq, nseq, olset[0].len);
		Fz = Edz2l;
		Fh = Ehtl;
		nb = nbreak;
		flip++;
		if (imres != NULL && n_acq_im < max_acq_im && nb == 0)
		  {
		    for (k = 0; k < nseq && k < imres->im.nx; k++)
		      {
			if (seq[k].noli >= 0 && seq[k].noli < imres->im.ny)
			  imres->im.pixel[seq[k].noli].fl[2*k] += 1;
		      }
		    n_acq_im++;
		    set_im_title(imres, "Possible oligos (%d/%d)",n_acq_im,max_acq_im);
		    imres->need_to_refresh = ALL_NEED_REFRESH;
		  }

		if (op->n_dat > 3 && nbreak == 0)
		  {
		    ds = op->dat[3];
		    for (k = 0; k < nseq && k < ds->nx; k++)
		      {
			ds->yd[k] = (0.9999*ds->yd[k]) + (0.0001*(seq[k].z - seq[k].z0)*(seq[k].z - seq[k].z0));
			ds->xd[k] = k;
		      }
		  }


		//if (testw != WIN_CANCEL)
		//testw = win_printf("up hill j %d E = %g E test = %g, nbreak %d Edz2 %g Eh %g",j,E,Et,nbreak,  Edz2l, Ehtl);
	      }
	    else
	      {
		if (debug == 2)
		  {
		    snprintf(message,sizeof(message),"refused flip\n iter %d flip %d aiming seq %d dz %g\n"
			     "Et %g (dz %g hyb %g) -> [%d,%d,%d](%d) E %g (dz %g hyb %g) -> [%d,%d,%d](%d)\n"
			     "testp %g prob %g\n"
			     "Debug Stop%%R flip only %%r all %%r"
			     ,itert,flip,i,test[i].z - test[i].z0
			     ,Et,Edz2l,Ehtl,test[i_1].noli,test[i].noli,test[i1].noli,
			     nbreak,E,Fz,Fh,seq[i_1].noli,seq[i].noli,seq[i1].noli,nb,testp,prob);
		    k = win_scanf(message,&debug);

		    if (k != WIN_CANCEL)
		      {
			for (k = 0; k < nseq-1; k++)
			  {
			    l = win_printf("Test Oli %d -> %d, z %g z+1 %g\nseq %d %s nhib %d\n"
					   "seq %d %s shited by %d\n"
					   "Seq  Oli %d -> %d, z %g z+1 %g\nseq %d %s nhib %d\n"
					   "seq %d %s shited by %d"
					   ,k,k+1,test[k].z,test[k+1].z,test[k].noli,olset[test[k].noli].seq,test[k].nhnext
					   ,test[k+1].noli,olset[test[k+1].noli].seq,test[k].dnext
					   ,k,k+1,seq[k].z,seq[k+1].z,seq[k].noli,olset[seq[k].noli].seq,seq[k].nhnext
					   ,seq[k+1].noli,olset[seq[k+1].noli].seq,seq[k].dnext
					   );
			    if (l == WIN_CANCEL) break;
			  }
		      }

		  }
		for (k = 0; k < nseq; k++)
		  {
		    test[k].noli = seq[k].noli;
		    test[k].z0 = seq[k].z0;
		    test[k].z = seq[k].z;
		    test[k].ze = seq[k].ze;
		    test[k].Edz2 = seq[k].Edz2;
		    test[k].next = seq[k].next;
		    test[k].nhnext = seq[k].nhnext;
		    test[k].dnext = seq[k].dnext;
		    test[k].Enext = seq[k].Enext;
		  }
		/*
		for (k = 0; k < nseq; k++)
		  test[k] = seq[k];
		*/
	      }
	}
      else
	{
	  if (debug == 1)
	    {
	      snprintf(message,sizeof(message),"direct flip\n iter %d flip %d aiming seq %d dz %g\n"
		       "Et %g (dz %g hyb %g) -> [%d,%d,%d](%d) E %g (dz %g hyb %g) -> [%d,%d,%d](%d)\n"
		       "Debug Stop%%R flip only %%r all %%r"
		       ,itert,flip,i,test[i].z - test[i].z0
		       ,Et,Edz2l,Ehtl,test[i_1].noli,test[i].noli,test[i1].noli,
		       nbreak,E,Fz,Fh,seq[i_1].noli,seq[i].noli,seq[i1].noli,nb);
	      k = win_scanf(message,&debug);

	      if (k != WIN_CANCEL)
		{
		  for (k = 0; k < nseq-1; k++)
		    {
		      l = win_printf("Test Oli %d -> %d, z %g z+1 %g\nseq %d %s nhib %d\n"
				     "seq %d %s shited by %d\n"
				     "Seq  Oli %d -> %d, z %g z+1 %g\nseq %d %s nhib %d\n"
				     "seq %d %s shited by %d"
				     ,k,k+1,test[k].z,test[k+1].z,test[k].noli,olset[test[k].noli].seq,test[k].nhnext
				     ,test[k+1].noli,olset[test[k+1].noli].seq,test[k].dnext
				     ,k,k+1,seq[k].z,seq[k+1].z,seq[k].noli,olset[seq[k].noli].seq,seq[k].nhnext
				     ,seq[k+1].noli,olset[seq[k+1].noli].seq,seq[k].dnext
				     );
		      if (l == WIN_CANCEL) break;
		    }
		}

	    }
		for (k = 0; k < nseq; k++)
		  {
		    seq[k].noli = test[k].noli;
		    seq[k].z0 = test[k].z0;
		    seq[k].z = test[k].z;
		    seq[k].ze = test[k].ze;
		    seq[k].Edz2 = test[k].Edz2;
		    seq[k].next = test[k].next;
		    seq[k].nhnext = test[k].nhnext;
		    seq[k].dnext = test[k].dnext;
		    seq[k].Enext = test[k].Enext;
		  }
		/*
	  for (k = 0; k < nseq; k++)
	    seq[k] = test[k];
		*/
	  E = compute_energies_2(seq, nseq, olset[0].len);
	  Fz = Edz2l;
	  Fh = Ehtl;
	  nb = nbreak;
	  if (E < Emin)
	    {
	      Emin = E;
	      for (k = 0; k < nseq; k++)
		{
		  bestseq[k].noli = seq[k].noli;
		  bestseq[k].z0 = seq[k].z0;
		  bestseq[k].z = seq[k].z;
		  bestseq[k].ze = seq[k].ze;
		  bestseq[k].Edz2 = seq[k].Edz2;
		  bestseq[k].next = seq[k].next;
		  bestseq[k].nhnext = seq[k].nhnext;
		  bestseq[k].dnext = seq[k].dnext;
		  bestseq[k].Enext = seq[k].Enext;
		}
	      /*
	      for (k = 0; k < nseq; k++)
		bestseq[k] = seq[k];
	      */
	    }
	  flip++;
	  if (op->n_dat > 3 && nbreak == 0)
	    {
	      ds = op->dat[3];
	      for (k = 0; k < nseq && k < ds->nx; k++)
		{
		  ds->yd[k] = (0.9999*ds->yd[k]) + (0.0001*(seq[k].z - seq[k].z0)*(seq[k].z - seq[k].z0));
		  ds->xd[k] = k;
		}
	    }

	  if (imres != NULL && n_acq_im < max_acq_im && nb == 0)
	    {
	      for (k = 0; k < nseq && k < imres->im.nx; k++)
		{
		  if (seq[k].noli >= 0 && seq[k].noli < imres->im.ny)
		    imres->im.pixel[seq[k].noli].fl[2*k] += 1;
		}
	      n_acq_im++;
	      set_im_title(imres, "Possible oligos (%d/%d)",n_acq_im,max_acq_im);
	      imres->need_to_refresh = ALL_NEED_REFRESH;
	    }
	  //if (testw != WIN_CANCEL)
	    //testw = win_printf("j %d E = %g E test = %g, nbreak %d Edz2 %g Eh %g",j,E,Et,nbreak,  Edz2l, Ehtl);
	}

    }
  itert += j;
  E = compute_energies_2(seq, nseq, olset[0].len);
  //win_printf("j %d E = %g E test = %g, nbreak %d Edz2 %g Eh %g",j,E,Et,nbreak,  Edz2l, Ehtl);

  if (op->n_dat > 2)
    {
      ds = op->dat[0];
	for (j = 0; j < nseq && j < ds->nx; j++)
	  {
	    ds->yd[j] = seq[j].noli;
	    ds->xd[j] = j;//seq[j].z;
	  }
    }
  if (op->n_dat > 2)
    {
      ds = op->dat[1];
	for (j = 0; j < nseq && j < ds->nx; j++)
	  {
	    ds->yd[j] = seq[j].Edz2;
	    ds->xd[j] = j;//seq[j].z;
	  }
    }
  if (op->n_dat > 2)
    {
      ds = op->dat[2];
	for (j = 0; j < nseq && j < ds->nx; j++)
	  {
	    ds->yd[j] = seq[j].Enext;
	    ds->xd[j] = j;//seq[j].z;
	  }
    }


  if (op->n_dat > 4)
    {
      ds = op->dat[4];
      for (j = 0; j < nseq; j++)
	{
	  ds->yd[j] = bestseq[j].noli;
	  ds->xd[j] = j;//test[j].z;
	}
    }


  set_plot_title(op, "\\stack{{Break %d Edz^2 %g E_{hyb} %g}{\\pt7 iter %d flip %d}}",nbreak, Edz2l, Ehtl,itert,flip);
  op->need_to_refresh = 1;
  if ((pr = find_pr_in_current_dialog(NULL)) == NULL)   return 0;
  refresh_plot(pr, UNCHANGED);
  return 0;
}
# ifdef ENCOURS
int eval_next_bases_local(void)
{
  static int i, olinb = 2, extend = 5, npos = 1;
  int usedoli[32], mask;
  O_p *op = NULL, *opn = NULL;
  d_s  *ds;
  pltreg *pr = NULL;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
    return win_printf_OK("cannot find data");


  i = win_scanf("Evaluation of all possible paths starting from:"
		"Oligo # %8d and extend < 8 %8d",&olinb,&extend);
  if (i == WIN_CANCEL) return 0;

  for(npos = 1, i = 0; i < extend; i++) npos *= 4;
  mask = nsel-1;

  if ((opn = create_and_attach_one_plot(pr, npos, npos, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  ds = opn->dat[0];
  set_ds_source(ds,"Oligo path proba");

  usedoli[0] = olinb;
  for (i = 0, leftID = mask&(bestseq[olinb].noli << 2), k = 0; i < extend; i++)
    {
      for (b = 0; b < 4; b++)
	{
	  nextID = leftID | b;
	  for (l = 0, zmin = MAX_FLT, imin = -1; l < nseq; l++)
	    {
	      for (jj = 0, good = 1; jj <= i; jj++)
		if (l == usedoli[jj]) good = 0;
	      if ((good == 1) && (bestseq[l].noli == nextID))
		{
		  tmp = bestseq[l].z - olinb - extend - 1;
		  tmp *= tmp;
		  if (imin < 0 || tmp < zmin)
		    {
		      imin = l;
		      zmin = tmp;
		    }
		}
	    }
	  if (imin < 0) zmin = nseg * nseg;
	}
    }
  set_plot_title(opn, "Iter %d Break %d Edz^2 %g E_{hyb} %g",j,nbreak, Edz2l, Ehtl);
  set_plot_x_title(opn, "Position");
  opn->filename = Transfer_filename(op->filename);
  opn->op_idle_action = assembleOligos_idle_action_simul;
  // refisplay the entire plot
  refresh_plot(pr, UNCHANGED);

  return D_O_K;
}

# endif
int do_assembleOligos_dump_seq(void)
{
  register int i, j, k;
  int noli, len, pos;
  char seqfilename[2048];
  FILE *fp;

  if(updating_menu_state != 0)	return D_O_K;

  seqfilename[0] = 0;

  if (do_select_file(seqfilename, sizeof(seqfilename), "TXT-FILE", "*.txt", "File to save sequence", 0))
    return win_printf_OK("Cannot select output file");
  fp = fopen (seqfilename,"w");
  if (fp == NULL)  win_printf_OK("Cannot open file:\n%s",backslash_to_slash(seqfilename));
  for (j = 0; j < nseq; j++)
    {
      noli = bestseq[j].noli;
      len = (bestseq[j].dnext == 0) ? olset[noli].len : bestseq[j].dnext;
      for (i = 0; i < len; i++)
	fputc(olset[noli].seq[i],fp);
    }
  fprintf(fp,"\n\n\n");
  for (j = k = 0; j < nseq; j++)
    {
      noli = bestseq[j].noli;
      len = olset[noli].len;
      for (i = 0; i < k; i++)
	fputc(' ',fp);
      k += (bestseq[j].dnext == 0) ? olset[noli].len : bestseq[j].dnext;
      for (i = 0; i < olset[noli].len; i++)
	fputc(olset[noli].seq[i],fp);
      fprintf(fp,"\n");
    }
  fprintf(fp,"\n\n\n");


  for (j = pos = 0; j < nseq; j++)
    {
      noli = bestseq[j].noli;
      len = (bestseq[j].dnext == 0) ? olset[noli].len : bestseq[j].dnext;
      fprintf(fp,"%g\t%d\n",bestseq[j].z0,pos);
      pos += len;
    }
  fclose(fp);
  return 0;
}


int do_assembleOligos_dump_seq_to_go(void)
{
  register int  j;
  int noli, len, pos;
  char seqfilename[2048];
  FILE *fp;

  if(updating_menu_state != 0)	return D_O_K;

  seqfilename[0] = 0;

  if (do_select_file(seqfilename, sizeof(seqfilename), "TXT-FILE", "*.txt", "File to save work", 0))
    return win_printf_OK("Cannot select output file");
  fp = fopen (seqfilename,"w");
  if (fp == NULL)  win_printf_OK("Cannot open file:\n%s",backslash_to_slash(seqfilename));

  for (j = pos = 0; j < nseq; j++)
    {
      noli = bestseq[j].noli;
      len = (bestseq[j].dnext == 0) ? olset[noli].len : bestseq[j].dnext;
      fprintf(fp,"%s\t%g\t%g\n",olset[noli].seq,seq[j].z0,seq[j].z);
      pos += len;
    }
  fclose(fp);
  return 0;
}

int do_assembleOligos_dump_org_seq(void)
{
  register int j;
  int noli, len, pos;
  char seqfilename[2048];
  FILE *fp;

  if(updating_menu_state != 0)	return D_O_K;

  seqfilename[0] = 0;

  if (do_select_file(seqfilename, sizeof(seqfilename), "TXT-FILE", "*.txt", "File to save sequence", 0))
    return win_printf_OK("Cannot select output file");
  fp = fopen (seqfilename,"w");
  if (fp == NULL)  win_printf_OK("Cannot open file:\n%s",backslash_to_slash(seqfilename));
  fprintf(fp,"i\tno\tseq\tz\tze\tpos\n");
  for (j = pos = 0; j < nseq; j++)
    {
      noli = seq[j].noli;
      len = (seq[j].dnext == 0) ? olset[noli].len : seq[j].dnext;
      fprintf(fp,"%d\t%d\t%s\t%g\t%g\t%d\n",j,noli,olset[noli].seq,seq[j].z0,seq[j].ze,pos);
      pos += len;
    }
  fclose(fp);
  return 0;
}



int do_assembleOligos_simulate(void)
{
  register int i, j, k;
  O_p *op = NULL, *opn = NULL;
  d_s  *ds;
  pltreg *pr = NULL;
  static float sim_er = 2.5;
  static int olisize = 4;
  double E, Et = 0;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
    return win_printf_OK("cannot find data");


  i = win_scanf("Simulating oligo sequencing experiment on lagging hairpin\n"
		"Define the searching oligo size %8d\nand the peaks error %8f\n"
		,&olisize,&sim_er);

  if (i == WIN_CANCEL) return 0;
  alocate_olihit_from_sequence(hplag, sizeof(hplag), olisize, sim_er);
  for (k = 0; k < nseq; k++)
    bestseq[k] = seq[k];

  QuickSort_oligo_hit(seq, 0, nseq-1);
  Emin = E = compute_energies_2(seq, nseq, olset[0].len);


  for (j = 0; j < nseq-1; j++)
    {
      i = win_printf("Oli %d -> %d\nseq %d %s nhib %d\n"
		     "seq %d %s shited by %d"
		     ,j,j+1,seq[j].noli,olset[seq[j].noli].seq,seq[j].nhnext
		     ,seq[j+1].noli,olset[seq[j+1].noli].seq,seq[j].dnext);
      if (i == WIN_CANCEL) break;
    }


  win_printf("%d hit %d break E = %g, Edz2 %g Eh %g",nseq,nbreak, E,  Edz2l, Ehtl);

  E = compute_energies_2(seq, nseq, olset[0].len);
  win_printf("j %d E = %g E test = %g, nbreak %d Edz2 %g Eh %g",j,E,Et,nbreak,  Edz2l, Ehtl);

  do_assembleOligos_dump_org_seq();
  if ((opn = create_and_attach_one_plot(pr, nseq, nseq, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  opn->op_idle_point_add_display = assembleOligos_idle_point_add_display;
  ds = opn->dat[0];
  opn->width = 1.7;
  set_ds_source(ds,"Oligo # vs z");
  for (j = 0; j < nseq; j++)
  {
    ds->yd[j] = seq[j].noli;
    ds->xd[j] = seq[j].z;
  }

  if ((ds = create_and_attach_one_ds(opn, nseq, nseq, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  set_ds_source(ds,"Edz2");
  for (j = 0; j < nseq; j++)
  {
    ds->yd[j] = seq[j].Edz2;
    ds->xd[j] = seq[j].z;
  }

  if ((ds = create_and_attach_one_ds(opn, nseq, nseq, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  set_ds_source(ds,"Edh");
  for (j = 0; j < nseq; j++)
  {
    ds->yd[j] = seq[j].Enext;
    ds->xd[j] = seq[j].z;
  }

  if ((ds = create_and_attach_one_ds(opn, nseq, nseq, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  set_ds_source(ds,"Avg \\Delta z^2");
  for (j = 0; j < nseq; j++)
  {
    ds->yd[j] = 0;
    ds->xd[j] = seq[j].z;
  }
  if ((ds = create_and_attach_one_ds(opn, nseq, nseq, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  set_ds_source(ds,"Real Oligo # vs z");
  set_plot_symb(ds, "\\pt5\\di");
  set_ds_line_color(ds, Magenta);
  for (j = 0; j < nseq; j++)
  {
    ds->yd[j] = exactseq[j].noli;
    ds->xd[j] = j;//test[j].z;
  }


  QuickSort_oligo_hit(test, 0, nseq-1);
  compute_energies_2(test, nseq, olset[0].len);

  if ((ds = create_and_attach_one_ds(opn, nseq, nseq, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  set_ds_source(ds,"Best Oligo # vs z");
  set_plot_symb(ds, "\\pt5\\di");
  for (j = 0; j < nseq; j++)
  {
    ds->yd[j] = bestseq[j].noli;
    ds->xd[j] = j;//test[j].z;
  }



  // now we must do some house keeping
  set_plot_title(opn, "Iter %d Break %d Edz^2 %g E_{hyb} %g",j,nbreak, Edz2l, Ehtl);
  set_plot_x_title(opn, "Position");
  opn->filename = Transfer_filename(op->filename);
  opn->op_idle_action = assembleOligos_idle_action_simul;
  // refisplay the entire plot
  refresh_plot(pr, UNCHANGED);

  return D_O_K;

}

int creating_plots_for_assembling(pltreg *pr, d_s *dsi, int lnseq)
{
 d_s *ds = NULL;
 int j;
 int noli, len, pos;

  if ((ope = create_and_attach_one_plot(pr, 16, 16, 0)) == NULL)
    return win_printf_OK("cannot create plot ope!");
  ope->op_idle_action = assembleOligos_idle_action;
  dsEt = ope->dat[0];
  inherit_from_ds_to_ds(dsEt, dsi);
  dsEt->nx = dsEt->ny = 0;
  set_ds_treatement(dsEt,"Total energy vs iter");

  if ((dsEz = create_and_attach_one_ds(ope, 16, 16, 0)) == NULL)
    return win_printf_OK("cannot create ds dsEz !");
  inherit_from_ds_to_ds(dsEz, dsi);
  dsEz->nx = dsEz->ny = 0;
  set_ds_treatement(dsEz,"Edz2");

  if ((dsEh = create_and_attach_one_ds(ope, 16, 16, 0)) == NULL)
    return win_printf_OK("cannot create ds dsEh !");
  inherit_from_ds_to_ds(dsEh, dsi);
  dsEh->nx = dsEh->ny = 0;
  set_ds_treatement(dsEh,"E hybrid");
  set_plot_title(ope, "Iter %d Break %d Edz^2 %g E_{hyb} %g",0,nbreak, Edz2l, Ehtl);
  set_plot_x_title(ope, "# of iter (x10^6)");
  set_plot_y_title(ope, "Energy");

  if ((opbpz = create_and_attach_one_plot(pr, lnseq, lnseq, 0)) == NULL)
    return win_printf_OK("cannot create plot opbpz!");
  opbpz->op_idle_action = assembleOligos_idle_action;
  dsbpz = ds = opbpz->dat[0];
  inherit_from_ds_to_ds(ds, dsi);
  if (exact_sequence_known == 1)
    {
      dsbpze = ds;
      set_ds_treatement(ds,"Oligo bp vs Z exact");
      for (j = 0, pos = 0; j < lnseq; j++)
	{
	  noli = exactseq[j].noli;
	  len = (exactseq[j].dnext == 0) ? olset[noli].len : exactseq[j].dnext;
	  ds->xd[j] = pos;//exactseq[j].noli;
	  ds->yd[j] = exactseq[j].z0;
	  pos += len;
	}
    }
  else
    {
      set_ds_treatement(ds,"Oligo bp vs Z");
      for (j = 0, pos = 0; j < lnseq; j++)
	{
	  noli = seq[j].noli;
	  len = (seq[j].dnext == 0) ? olset[noli].len : seq[j].dnext;
	  ds->yd[j] = seq[j].z;
	  ds->xd[j] = pos;//seq[j].z;
	  pos += len;
	}
    }
  if (exact_sequence_known == 1)
    {
      if ((ds = create_and_attach_one_ds(opbpz, lnseq, lnseq, 0)) == NULL)
	return win_printf_OK("cannot create ds dsbpz !");
      dsbpz = ds;
      inherit_from_ds_to_ds(ds, dsi);
      set_ds_treatement(ds,"Oligo bp vs Z");
      for (j = 0, pos = 0; j < lnseq; j++)
	{
	  noli = seq[j].noli;
	  len = (seq[j].dnext == 0) ? olset[noli].len : seq[j].dnext;
	  ds->yd[j] = seq[j].z;
	  ds->xd[j] = pos;//seq[j].z;
	  pos += len;
	}
    }

  if ((ds = create_and_attach_one_ds(opbpz, lnseq, lnseq, 0)) == NULL)
    return win_printf_OK("cannot create ds dspbzb !");
  dsbpzb = ds;
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"Oligo bp vs Z best");
  for (j = 0, pos = 0; j < lnseq; j++)
    {
      noli = bestseq[j].noli;
      len = (bestseq[j].dnext == 0) ? olset[noli].len : bestseq[j].dnext;
      ds->yd[j] = seq[j].z;
      ds->xd[j] = pos;//seq[j].z;
      pos += len;
    }
  set_plot_title(opbpz, "Iter %d Break %d Edz^2 %g E_{hyb} %g",j,nbreak, Edz2l, Ehtl);
  set_plot_y_title(opbpz, "Position Z");
  set_plot_x_title(opbpz, "Bp index");

  if ((ople = create_and_attach_one_plot(pr, lnseq, lnseq, 0)) == NULL)
    return win_printf_OK("cannot create plot ople!");
  ople->op_idle_action = assembleOligos_idle_action;
  dslet = ople->dat[0];
  inherit_from_ds_to_ds(dslet, dsi);
  set_ds_treatement(dslet,"Oligo E vs #");
  for (j = 0; j < lnseq; j++)
  {
    dslet->yd[j] = bestseq[j].Enext + bestseq[j].Edz2;
    dslet->xd[j] = j;
  }

  if ((ds = create_and_attach_one_ds(ople, lnseq, lnseq, 0)) == NULL)
    return win_printf_OK("cannot create plot dslez!");
  dslez = ds;
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"E \\Delta z^2");
  for (j = 0; j < lnseq; j++)
  {
    ds->yd[j] = seq[j].Edz2;
    ds->xd[j] = j;//seq[j].z;
  }
  set_plot_title(ople, "Iter %d Break %d Edz^2 %g E_{hyb} %g",j,nbreak, Edz2l, Ehtl);
  set_plot_x_title(ople, "Position Z");
  set_plot_y_title(ople, "Energy");

  if ((ds = create_and_attach_one_ds(ople, lnseq, lnseq, 0)) == NULL)
    return win_printf_OK("cannot create ds dshy !");
  dshy = ds;
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"E hybrid");
  for (j = 0; j < lnseq; j++)
  {
    ds->yd[j] = bestseq[j].Enext;//seq[j].Enext;
    ds->xd[j] = j;//seq[j].z;
  }


  set_plot_title(ople, "Iter %d Break %d Edz^2 %g E_{hyb} %g",j,nbreak, Edz2l, Ehtl);
  set_plot_x_title(ople, "Position Z");
  set_plot_y_title(ople, "Energy");


  if ((oppa = create_and_attach_one_plot(pr, lnseq, lnseq, 0)) == NULL)
    return win_printf_OK("cannot create plot oppa!");
  oppa->op_idle_action = assembleOligos_idle_action;
  dspab = ds = oppa->dat[0];
  inherit_from_ds_to_ds(ds, dsi);
  if (exact_sequence_known == 1)
  {
    dspae = ds;
    set_ds_treatement(ds,"Oligo # vs z exact");
    for (j = 0; j < lnseq; j++)
      {
	ds->yd[j] = exactseq[j].noli;
	ds->xd[j] = exactseq[j].z;
      }
  }
  else
  {
    set_ds_treatement(ds,"Oligo # vs z best");
    for (j = 0; j < lnseq; j++)
      {
	ds->yd[j] = seq[j].noli;
	ds->xd[j] = seq[j].z;
      }
  }

  if (exact_sequence_known == 1)
    {
      if ((ds = create_and_attach_one_ds(oppa, lnseq, lnseq, 0)) == NULL)
	return win_printf_OK("cannot create ds dspa !");
      dspab = ds;
      inherit_from_ds_to_ds(ds, dsi);
      set_ds_treatement(ds,"Oligo # vs z best");
      for (j = 0; j < lnseq; j++)
	{
	  ds->yd[j] = seq[j].noli;
	  ds->xd[j] = j;//seq[j].z;
	}
    }
  if ((ds = create_and_attach_one_ds(oppa, lnseq, lnseq, 0)) == NULL)
    return win_printf_OK("cannot create ds dspa !");
  dspa = ds;
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"Oligo # vs z");
  for (j = 0; j < lnseq; j++)
    {
      ds->yd[j] = seq[j].noli;
      ds->xd[j] = j;//seq[j].z;
    }


  set_plot_title(oppa, "Iter %d Break %d Edz^2 %g E_{hyb} %g",j,nbreak, Edz2l, Ehtl);
  set_plot_x_title(oppa, "Position");
  set_plot_y_title(oppa, "Oligo ID");


  if ((opflip = create_and_attach_one_plot(pr, lnseq, lnseq, 0)) == NULL)
    return win_printf_OK("cannot create plot oppa!");
  opflip->op_idle_action = assembleOligos_idle_action;
  dsflip = ds = opflip->dat[0];
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"Oligo flip vs index");
  for (j = 0; j < lnseq; j++)
    {
      ds->yd[j] = 0;
      ds->xd[j] = j;
    }
  set_plot_x_title(opflip, "oligo #");
  set_plot_y_title(opflip, "Flips");


  return 0;
}

int do_assembleOligos_rescale_data_set(void)
{
  register int i, j, k;
  O_p *op = NULL;
  d_s *dsi;
  pltreg *pr = NULL;
  double E, Et = 0;
  //long idum = 4587;
  //  float zwas, testp, prob;
  //int testw = D_O_K;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");

  if (alocate_olihit(dsi))    return D_O_K;
  QuickSort_oligo_hit(seq, 0, nseq-1);
  QuickSort_oligo_hit(test, 0, nseq-1);
  compute_energies(test, nseq);
  Emin = E = compute_energies(seq, nseq);
  for (k = 0; k < nseq; k++)
    bestseq[k] = seq[k];

  for (j = 0; j < nseq-1; j++)
    {
      i = win_printf("Oli %d -> %d\nseq %d %s nhib %d\n"
		     "seq %d %s shited by %d"
		     ,j,j+1,seq[j].noli,olset[seq[j].noli].seq,seq[j].nhnext
		     ,seq[j+1].noli,olset[seq[j+1].noli].seq,seq[j].dnext);
      if (i == WIN_CANCEL) break;
    }


  win_printf("%d hit %d break E = %g, Edz2 %g Eh %g",nseq,nbreak, E,  Edz2l, Ehtl);

# ifdef OLD
  for (j = 0; j < 10000000; j++)
    {
      i = (int)(ran1(&idum)*(nseq-1));
      zwas = test[i].z;
      test[i].z = test[i].z0 + (10 * ran1(&idum))/bp_to_nm;
      if (j < 3)  win_printf("i = %d dz = %g",i,test[i].z - test[i].z0);
      QuickSort_oligo_hit(test, 0, nseq-1);
      Et = compute_energies(test, nseq);
      if (Et >= E)
	{
	    testp = ran1(&idum);
	    prob = exp(-(Et-E)/3);
	    if (prob > testp)
	      {
		for (k = 0; k < nseq; k++)
		  seq[k] = test[k];
		E = compute_energies(seq, nseq);
		if (testw != WIN_CANCEL)
		testw = win_printf("up hill j %d E = %g E test = %g, nbreak %d Edz2 %g Eh %g",j,E,Et,nbreak,  Edz2l, Ehtl);
	      }
	    else  test[i].z = zwas;
	}
      else
	{
	  for (k = 0; k < nseq; k++)
	    seq[k] = test[k];
	  E = compute_energies(seq, nseq);
	  if (testw != WIN_CANCEL)
	  testw = win_printf("j %d E = %g E test = %g, nbreak %d Edz2 %g Eh %g",j,E,Et,nbreak,  Edz2l, Ehtl);
	}

    }
# endif
  E = compute_energies(seq, nseq);
  win_printf("j %d E = %g E test = %g, nbreak %d Edz2 %g Eh %g",j,E,Et,nbreak,  Edz2l, Ehtl);

  /*



  for (i = 0; i < nsel; i++)
    {
      for (no = (int)(ran1(&idum)*(noli-1)), invalid = 1; invalid ;no = (int)(ran1(&idum)*(noli-1));
	{
	  invalid = (oligo_nhib_invalid[no] > n_asqd) 1 : 0; //invalid;
	  if (invalid == 0) invalid = (fill_fingerprint(fgp, i, no))?1 : 0;
	}
    }

  i = win_scanf("By what factor do you want to multiply y %f",&factor);
  if (i == WIN_CANCEL)	return OFF;
*/


  creating_plots_for_assembling(pr, dsi, nseq);
  /*
  if ((opn = create_and_attach_one_plot(pr, nseq, nseq, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  opn->op_idle_point_add_display = assembleOligos_idle_point_add_display;
  ds = opn->dat[0];
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"Oligo # vs z");
  for (j = 0; j < nseq; j++)
  {
    ds->yd[j] = seq[j].noli;
    ds->xd[j] = seq[j].z;
  }

  if ((ds = create_and_attach_one_ds(opn, nseq, nseq, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"Edz2");
  for (j = 0; j < nseq; j++)
  {
    ds->yd[j] = seq[j].Edz2;
    ds->xd[j] = seq[j].z;
  }

  if ((ds = create_and_attach_one_ds(opn, nseq, nseq, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"Edh");
  for (j = 0; j < nseq; j++)
  {
    ds->yd[j] = seq[j].Enext;
    ds->xd[j] = seq[j].z;
  }

  if ((ds = create_and_attach_one_ds(opn, nseq, nseq, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"Avg \\Delta z^2");
  for (j = 0; j < nseq; j++)
  {
    ds->yd[j] = 0;
    ds->xd[j] = seq[j].z;
  }

  if ((ds = create_and_attach_one_ds(opn, nseq, nseq, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  set_ds_treatement(ds,"Best Oligo # vs z");
  for (j = 0; j < nseq; j++)
  {
    ds->yd[j] = bestseq[j].noli;
    ds->xd[j] = j;//test[j].z;
  }


  // now we must do some house keeping
  set_plot_title(opn, "Iter %d Break %d Edz^2 %g E_{hyb} %g",j,nbreak, Edz2l, Ehtl);
  set_plot_x_title(opn, "Position");
  opn->filename = Transfer_filename(op->filename);
  opn->op_idle_action = assembleOligos_idle_action;
  */
  // refisplay the entire plot
  refresh_plot(pr, UNCHANGED);

  return D_O_K;
}

int do_assembleOligos_rescale_data_from_file(void)
{
  register int k;
  O_p *op = NULL;
  d_s *dsi;
  pltreg *pr = NULL;
  double E, Et = 0;
  static char seqfilename[2048] = {0};
    //long idum = 4587;
  //  float zwas, testp, prob;
  //int testw = D_O_K;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
    return win_printf_OK("cannot find data");

  if (do_select_file(seqfilename, sizeof(seqfilename), "TXT-FILE", "*.txt", "File of oligos with positions\0", 1))
    return win_printf_OK("Cannot select output file");


  dsi = alocate_oligo_and_olihit(seqfilename);
  if (dsi == NULL)    return D_O_K;
  QuickSort_oligo_hit(seq, 0, nseq-1);
  QuickSort_oligo_hit(test, 0, nseq-1);
  compute_energies(test, nseq);
  Emin = E = compute_energies(seq, nseq);
  for (k = 0; k < nseq; k++)
    bestseq[k] = seq[k];

  /*
  for (j = 0; j < nseq-1; j++)
    {
      i = win_printf("Oli %d -> %d\nseq %d %s nhib %d\n"
		     "seq %d %s shited by %d"
		     ,j,j+1,seq[j].noli,olset[seq[j].noli].seq,seq[j].nhnext
		     ,seq[j+1].noli,olset[seq[j+1].noli].seq,seq[j].dnext);
      if (i == WIN_CANCEL) break;
    }
  */

  //win_printf("%d hit %d break E = %g, Edz2 %g Eh %g",nseq,nbreak, E,  Edz2l, Ehtl);

  E = compute_energies(seq, nseq);
  win_printf("E = %g E test = %g, nbreak %d Edz2 %g Eh %g",E,Et,nbreak,  Edz2l, Ehtl);
  creating_plots_for_assembling(pr, dsi, nseq);
  // refisplay the entire plot
  refresh_plot(pr, UNCHANGED);
  if (dsi) free_data_set(dsi);
  return D_O_K;
}

int do_assembleOligos_rescale_plot(void)
{
  register int i, j;
  O_p *op = NULL, *opn = NULL;
  int nf;
  static float factor = 1;
  d_s *ds, *dsi;
  pltreg *pr = NULL;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number and place it in a new plot");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  nf = dsi->nx;	/* this is the number of points in the data set */
  i = win_scanf("By what factor do you want to multiply y %f",&factor);
  if (i == WIN_CANCEL)	return OFF;

  if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  ds = opn->dat[0];

  for (j = 0; j < nf; j++)
  {
    ds->yd[j] = factor * dsi->yd[j];
    ds->xd[j] = dsi->xd[j];
  }

  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"y multiplied by %f",factor);
  set_plot_title(opn, "Multiply by %f",factor);
  if (op->x_title != NULL) set_plot_x_title(opn, op->x_title);
  if (op->y_title != NULL) set_plot_y_title(opn, op->y_title);
  opn->filename = Transfer_filename(op->filename);
  uns_op_2_op(opn, op);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}



int do_assembleOligos_test_mix_vs_noise_erf(void)
{
  register int i, k;
  O_p *op = NULL, *opn = NULL;
  int nf;
  static int npt = 50;
  static float ni = 0.5;
  d_s *ds = NULL, *dsi = NULL, *ds2 = NULL;
  pltreg *pr = NULL;
  float noise;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number and place it in a new plot");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  nf = dsi->nx;	/* this is the number of points in the data set */
  i = win_scanf("Testing dataset for noise:\n"
		"I shall do that  at each noise ampitude and then increase noise\n"
		"By %5f amount for %5d trial\n"
		,&ni,&npt);
  if (i == WIN_CANCEL)	return OFF;

  if ((opn = create_and_attach_one_plot(pr, npt, npt, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  ds = opn->dat[0];
  if ((ds2 = create_and_attach_one_ds(opn, npt, npt, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  float tmp;
  for (k = 0, tmp = 0; k < nf; k++)
    tmp += dsi->yd[k] * dsi->yd[k];
  tmp = sqrt(tmp);
  for (i = 0; i < npt; i++)
    {
      noise = ni * i * sqrt(2);
      /*
      for (k = 0, ds->yd[i] = 1; k < nf; k++)
      {
	  ds->yd[i] *= (1 - erf(fabs(dsi->yd[k])/(2*noise)));
      }
      */
      if (i == 0)
          ds->yd[i] = ds2->yd[i] = 1;
      else
          {
              ds->yd[i] = (1 - erf(fabs(tmp)/(2*noise)));
              ds->yd[i] = 1 - ds->yd[i]/2;
              ds2->yd[i] = (1 - erf(fabs(2*tmp)/(2*noise)));
              ds2->yd[i] = 1 - ds2->yd[i]/2;
          }
      ds->xd[i] = ds2->xd[i] = noise/sqrt(2);
    }
  inherit_from_ds_to_ds(ds, dsi);
  if (op->x_title != NULL) set_plot_x_title(opn, op->x_title);
  if (op->y_title != NULL) set_plot_y_title(opn, op->y_title);
  opn->filename = Transfer_filename(op->filename);
  uns_op_2_op(opn, op);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}
int do_assembleOligos_test_mix_vs_noise(void)
{
  register int i, j, k;
  O_p *op = NULL, *opn = NULL;
  int nf;
  static int Nt = 1000, npt = 50, mean0 = 0;
  static float ni = 0.5;
  d_s *ds = NULL, *dsi = NULL, *dst = NULL, *ds2 = NULL;
  pltreg *pr = NULL;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number and place it in a new plot");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  nf = dsi->nx;	/* this is the number of points in the data set */
  i = win_scanf("Testing dataset for noise:\n"
		"I will compute a gaussian noise of amplitute n\n"
		"compute its variance and compare to the variance of the data set plus noise\n"
		"I shall do that %5d times at each noise amplitude and then increase noise\n"
		"By %5f amount for %5d trial\n"
		"%b->Impose mean = 0"
		,&Nt,&ni,&npt,&mean0);
  if (i == WIN_CANCEL)	return OFF;

  if ((opn = create_and_attach_one_plot(pr, npt, npt, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  ds = opn->dat[0];
  if ((ds2 = create_and_attach_one_ds(opn, npt, npt, 0)) == NULL)
    return win_printf_OK("cannot create plot !");

  dst = build_data_set(nf,nf);
  if (dst == NULL) return win_printf_OK("cannot create tmp ds !");
  float noise, amp0, ampd, mean;
  for (i = 0; i < npt; i++)
    {
      noise = ni * i;
      for (k = 0; k < Nt; k++)
          {
              for (j = 0, mean = 0; j < nf; j++)
                  {
                      // we place in dst->yd  noise ony
                      dst->yd[j] = noise * gasdev(&idum);
                      mean += dst->yd[j];
                      // we place in dst->xd signal + noise
                      dst->xd[j] = dsi->yd[j] + dst->yd[j];
                  }
              mean /= nf;
              if (mean0) mean = 0;
              //we compute noise variance
              for (j = 0, amp0 = 0; j < nf; j++)
                  amp0 += (dst->yd[j] - mean) * (dst->yd[j] - mean);
              // we compute mean of signal + noise
              for (j = 0, mean = 0; mean0 > 0 && j < nf; j++)
                  mean += dst->xd[j];
              mean /= nf;
              if (mean0) mean = 0;
              //we compute signal+noise variance
              for (j = 0, ampd = 0; j < nf; j++)
                  ampd += (dst->xd[j] - mean) * (dst->xd[j] - mean);
              ds->yd[i] += (amp0 < ampd) ? 1 : 0;
              ds->xd[i] = noise;
              for (j = 0; j < nf; j++)
                  dst->xd[j] = dsi->yd[j] * (dsi->yd[j] + dst->yd[j]);
              for (j = 0, amp0 = 0, ampd = 0; j < nf; j++)
                  {
                      amp0 += dst->xd[j];
                      ampd += dsi->yd[j] * dst->yd[j];
                  }
              ds2->yd[i] += (amp0 > 0) ? 1 : 0;
              ds2->xd[i] = noise;
          }
    }
  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
  if (op->x_title != NULL) set_plot_x_title(opn, op->x_title);
  if (op->y_title != NULL) set_plot_y_title(opn, op->y_title);
  opn->filename = Transfer_filename(op->filename);
  uns_op_2_op(opn, op);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}




int  does_oligos_ovetlap(int oli1, int oli2, int size)
{
  register int i;
  int over = 0, mask, b, size2;

  size2 = 2*size;
  for (i = 0, mask = 0; i < size2; i += 2) mask |= (0x3 << i);
  b = mask;
  for (i = 0; over == 0 && i < size2; i += 2)
    {
      over = ((mask & oli1) == (oli2 >> i)) ? 1 : over;
      mask >>= 2;
    }
  if (over) return 1;
  mask = b;
  for (i = 0; over == 0 && i < size2; i += 2)
    {
      over = ((mask & oli2) == (oli1 >> i)) ? 1 : over;
      mask >>= 2;
    }
  return over;

}



int ABC_in_array(int A, int B, int C, int possi[16][3], int n_possi)
{
  int i, j, k[3], pos, tot;

  for (pos = 0; pos < n_possi; pos++)
    {
      k[0] = k[1] = k[2] = 0;
      tot = 0;
      for (i = j = 0; j == 0 && i < 3; i++)
	{
	  if (A == possi[pos][i])
	    {
	      k[i] = j = 1;
	      tot = 1;
	      break;
	    }
	}
      for (i = j = 0; j == 0 && i < 3; i++)
	{
	  if (k[i]) continue;
	  if (B == possi[pos][i])
	    {
	      k[i] = j = 2;
	      tot |= 0x02;
	      break;
	    }
	}
      for (i = j = 0; j == 0 && i < 3; i++)
	{
	  if (k[i]) continue;
	  if (C == possi[pos][i])
	    {
	      k[i] = j = 3;
	      tot |= 0x04;
	      break;
	    }
	}
      if (tot == 7)
	{
	  //win_printf("found %d-%d-%d at pos %d",A,B,C,pos);
	  return 1;
	}
    }
  return 0;
}

int do_find_ceb25_multiplex_set(void)
{
  int A,B,C,x[12], possi[16][3];
  int is_cancel = 0;

  if(updating_menu_state != 0)	return D_O_K;


  possi[0][0] = 0;   possi[0][1] = 3;   possi[0][2] = 6;
  possi[1][0] = 0;   possi[1][1] = 3;   possi[1][2] = 11;
  possi[2][0] = 1;   possi[2][1] = 4;   possi[2][2] = 9;
  possi[3][0] = 1;   possi[3][1] = 4;   possi[3][2] = 11;
  possi[4][0] = 2;   possi[4][1] = 5;   possi[4][2] = 7;
  possi[5][0] = 2;   possi[5][1] = 5;   possi[5][2] = 10;
  possi[6][0] = 2;   possi[6][1] = 7;   possi[6][2] = 10;
  possi[7][0] = 7;   possi[7][1] = 5;   possi[7][2] = 10;
  possi[8][0] = 0;   possi[8][1] = 3;   possi[8][2] = 9;
  possi[9][0] = 1;   possi[9][1] = 5;   possi[9][2] = 11;
  possi[10][0] = 2;   possi[10][1] = 6;   possi[10][2] = 7;
  possi[11][0] = 8;   possi[11][1] = 7;   possi[11][2] = 2;
  possi[12][0] = 8;   possi[12][1] = 9;   possi[12][2] = 10;
  possi[13][0] = 7;   possi[13][1] = 8;   possi[13][2] = 9;
  possi[14][0] = 0;   possi[14][1] = 8;   possi[14][2] = 9;
  possi[15][0] = 1;   possi[15][1] = 8;   possi[15][2] = 9;


  for (x[0] = 0; x[0] < 12; x[0]++)
    {
      for (x[1] = 0; x[1] < 12; x[1]++)
	{
	  if (x[1] == x[0]) continue;
	  for (x[2] = 0; x[2] < 12; x[2]++)
	    {
	      if (x[2] == x[0]) continue;
	      if (x[2] == x[1]) continue;
	      for (x[3] = 0; x[3] < 12; x[3]++)
		{
		  if (x[3] == x[0]) continue;
		  if (x[3] == x[1]) continue;
		  if (x[3] == x[2]) continue;
		  for (x[4] = 0; x[4] < 12; x[4]++)
		    {
		      if (x[4] == x[0]) continue;
		      if (x[4] == x[1]) continue;
		      if (x[4] == x[2]) continue;
		      if (x[4] == x[3]) continue;
		      for (x[5] = 0; x[5] < 12; x[5]++)
			{
			  if (x[5] == x[0]) continue;
			  if (x[5] == x[1]) continue;
			  if (x[5] == x[2]) continue;
			  if (x[5] == x[3]) continue;
			  if (x[5] == x[4]) continue;
			  for (x[6] = 0; x[6] < 12; x[6]++)
			    {
			      if (x[6] == x[0]) continue;
			      if (x[6] == x[1]) continue;
			      if (x[6] == x[2]) continue;
			      if (x[6] == x[3]) continue;
			      if (x[6] == x[4]) continue;
			      if (x[6] == x[5]) continue;
			      for (x[7] = 0; x[7] < 12; x[7]++)
				{
				  if (x[7] == x[0]) continue;
				  if (x[7] == x[1]) continue;
				  if (x[7] == x[2]) continue;
				  if (x[7] == x[3]) continue;
				  if (x[7] == x[4]) continue;
				  if (x[7] == x[5]) continue;
				  if (x[7] == x[6]) continue;
				  for (x[8] = 0; x[8] < 12; x[8]++)
				    {
				      if (x[8] == x[0]) continue;
				      if (x[8] == x[1]) continue;
				      if (x[8] == x[2]) continue;
				      if (x[8] == x[3]) continue;
				      if (x[8] == x[4]) continue;
				      if (x[8] == x[5]) continue;
				      if (x[8] == x[6]) continue;
				      if (x[8] == x[7]) continue;
				      for (x[9] = 0; x[9] < 12; x[9]++)
					{
					  if (x[9] == x[0]) continue;
					  if (x[9] == x[1]) continue;
					  if (x[9] == x[2]) continue;
					  if (x[9] == x[3]) continue;
					  if (x[9] == x[4]) continue;
					  if (x[9] == x[5]) continue;
					  if (x[9] == x[6]) continue;
					  if (x[9] == x[7]) continue;
					  if (x[9] == x[8]) continue;
					  for (x[10] = 0; x[10] < 12; x[10]++)
					    {
					      if (x[10] == x[0]) continue;
					      if (x[10] == x[1]) continue;
					      if (x[10] == x[2]) continue;
					      if (x[10] == x[3]) continue;
					      if (x[10] == x[4]) continue;
					      if (x[10] == x[5]) continue;
					      if (x[10] == x[6]) continue;
					      if (x[10] == x[7]) continue;
					      if (x[10] == x[8]) continue;
					      if (x[10] == x[9]) continue;
					      for (x[11] = 0; x[11] < 12; x[11]++)
						{
						  if (x[11] == x[0]) continue;
						  if (x[11] == x[1]) continue;
						  if (x[11] == x[2]) continue;
						  if (x[11] == x[3]) continue;
						  if (x[11] == x[4]) continue;
						  if (x[11] == x[5]) continue;
						  if (x[11] == x[6]) continue;
						  if (x[11] == x[7]) continue;
						  if (x[11] == x[8]) continue;
						  if (x[11] == x[9]) continue;
						  if (x[11] == x[10]) continue;
						  A = x[0]; B = x[2]; C = x[7];
						  if (ABC_in_array(A, B, C, possi, 16) == 0) continue;
						  if (is_cancel != WIN_CANCEL)
						    is_cancel = win_printf("Parial match 0->%d 1->%d 2->%d 3->%d 4->%d"
							     "5->%d 6->%d 7->%d 8->%d 9->%d 10->%d 11->%d\n"
							     ,x[0],x[1],x[2],x[3],x[4],x[5],x[6],x[7],x[8],x[9],x[10],x[11]);


						  A = x[1]; B = x[2]; C = x[8];
						  if (ABC_in_array(A, B, C, possi, 16) == 0) continue;
						  A = x[3]; B = x[5]; C = x[9];
						  if (ABC_in_array(A, B, C, possi, 16) == 0) continue;
						  A = x[6]; B = x[7]; C = x[8];
						  if (ABC_in_array(A, B, C, possi, 16) == 0) continue;
						  A = x[4]; B = x[5]; C = x[10];
						  if (ABC_in_array(A, B, C, possi, 16) == 0) continue;
						  A = x[4]; B = x[9]; C = x[11];
						  if (ABC_in_array(A, B, C, possi, 16) == 0) continue;

						  return win_printf_OK("Found match 0->%d 1->%d 2->%d 3->%d 4->%d"
							     "5->%d 6->%d 7->%d 8->%d 9->%d 10->%d 11->%d\n"
							     ,x[0],x[1],x[2],x[3],x[4],x[5],x[6],x[7],x[8],x[9],x[10],x[11]);
						}
					    }
					}
				    }
				}
			    }
			}
		    }
		}
	    }
	}

    }
  return win_printf_OK("Found no match!");
}

# ifdef ENCOURS
int do_find_ceb25_multiplex_set_2(void)
{
  int A,B,C,x[12], oliID[12], oli_overlap[12][12];
  int is_cancel = 0;
  char *oili[16];

   char oli1[0] = "CACACTTACA";
   char oli2[1] = "CTTACACCC";
   char oli1[3] = "CCCACCCTT";
   char oli1[4] = "CCTTACATCT";
   char oli1[5] = "ACATCTACCT";
   char oli1[6] = "TACCTCCAC";
   char oli1[7] = "CTCACACCC";
   char oli1[8] = "ACCCCCCCT";
   char oli1[9] = "TCCACACCC";
   char oli1[10] = "ACTCCCACA";
   char oli1[11] = "CCCACCCAC";
   char oli1[12] = "CCACACTCA";




  if(updating_menu_state != 0)	return D_O_K;

  for (x[0] = 0; x[0] < 12; x[0]++)
    {
      for (x[1] = 0; x[1] < 12; x[1]++)
	{
	  if (x[1] == x[0]) continue;
	  for (x[2] = 0; x[2] < 12; x[2]++)
	    {
	      if (x[2] == x[0]) continue;
	      if (x[2] == x[1]) continue;
	      for (x[3] = 0; x[3] < 12; x[3]++)
		{
		  if (x[3] == x[0]) continue;
		  if (x[3] == x[1]) continue;
		  if (x[3] == x[2]) continue;
		  for (x[4] = 0; x[4] < 12; x[4]++)
		    {
		      if (x[4] == x[0]) continue;
		      if (x[4] == x[1]) continue;
		      if (x[4] == x[2]) continue;
		      if (x[4] == x[3]) continue;
		      for (x[5] = 0; x[5] < 12; x[5]++)
			{
			  if (x[5] == x[0]) continue;
			  if (x[5] == x[1]) continue;
			  if (x[5] == x[2]) continue;
			  if (x[5] == x[3]) continue;
			  if (x[5] == x[4]) continue;
			  for (x[6] = 0; x[6] < 12; x[6]++)
			    {
			      if (x[6] == x[0]) continue;
			      if (x[6] == x[1]) continue;
			      if (x[6] == x[2]) continue;
			      if (x[6] == x[3]) continue;
			      if (x[6] == x[4]) continue;
			      if (x[6] == x[5]) continue;
			      for (x[7] = 0; x[7] < 12; x[7]++)
				{
				  if (x[7] == x[0]) continue;
				  if (x[7] == x[1]) continue;
				  if (x[7] == x[2]) continue;
				  if (x[7] == x[3]) continue;
				  if (x[7] == x[4]) continue;
				  if (x[7] == x[5]) continue;
				  if (x[7] == x[6]) continue;
				  for (x[8] = 0; x[8] < 12; x[8]++)
				    {
				      if (x[8] == x[0]) continue;
				      if (x[8] == x[1]) continue;
				      if (x[8] == x[2]) continue;
				      if (x[8] == x[3]) continue;
				      if (x[8] == x[4]) continue;
				      if (x[8] == x[5]) continue;
				      if (x[8] == x[6]) continue;
				      if (x[8] == x[7]) continue;
				      for (x[9] = 0; x[9] < 12; x[9]++)
					{
					  if (x[9] == x[0]) continue;
					  if (x[9] == x[1]) continue;
					  if (x[9] == x[2]) continue;
					  if (x[9] == x[3]) continue;
					  if (x[9] == x[4]) continue;
					  if (x[9] == x[5]) continue;
					  if (x[9] == x[6]) continue;
					  if (x[9] == x[7]) continue;
					  if (x[9] == x[8]) continue;
					  for (x[10] = 0; x[10] < 12; x[10]++)
					    {
					      if (x[10] == x[0]) continue;
					      if (x[10] == x[1]) continue;
					      if (x[10] == x[2]) continue;
					      if (x[10] == x[3]) continue;
					      if (x[10] == x[4]) continue;
					      if (x[10] == x[5]) continue;
					      if (x[10] == x[6]) continue;
					      if (x[10] == x[7]) continue;
					      if (x[10] == x[8]) continue;
					      if (x[10] == x[9]) continue;
					      for (x[11] = 0; x[11] < 12; x[11]++)
						{
						  if (x[11] == x[0]) continue;
						  if (x[11] == x[1]) continue;
						  if (x[11] == x[2]) continue;
						  if (x[11] == x[3]) continue;
						  if (x[11] == x[4]) continue;
						  if (x[11] == x[5]) continue;
						  if (x[11] == x[6]) continue;
						  if (x[11] == x[7]) continue;
						  if (x[11] == x[8]) continue;
						  if (x[11] == x[9]) continue;
						  if (x[11] == x[10]) continue;
						  A = x[0]; B = x[2]; C = x[7];
						  if (ABC_in_array(A, B, C, possi, 16) == 0) continue;
						  if (is_cancel != WIN_CANCEL)
						    is_cancel = win_printf("Parial match 0->%d 1->%d 2->%d 3->%d 4->%d"
							     "5->%d 6->%d 7->%d 8->%d 9->%d 10->%d 11->%d\n"
							     ,x[0],x[1],x[2],x[3],x[4],x[5],x[6],x[7],x[8],x[9],x[10],x[11]);


						  A = x[1]; B = x[2]; C = x[8];
						  if (ABC_in_array(A, B, C, possi, 16) == 0) continue;
						  A = x[3]; B = x[5]; C = x[9];
						  if (ABC_in_array(A, B, C, possi, 16) == 0) continue;
						  A = x[6]; B = x[7]; C = x[8];
						  if (ABC_in_array(A, B, C, possi, 16) == 0) continue;
						  A = x[4]; B = x[5]; C = x[10];
						  if (ABC_in_array(A, B, C, possi, 16) == 0) continue;
						  A = x[4]; B = x[9]; C = x[11];
						  if (ABC_in_array(A, B, C, possi, 16) == 0) continue;

						  return win_printf_OK("Found match 0->%d 1->%d 2->%d 3->%d 4->%d"
							     "5->%d 6->%d 7->%d 8->%d 9->%d 10->%d 11->%d\n"
							     ,x[0],x[1],x[2],x[3],x[4],x[5],x[6],x[7],x[8],x[9],x[10],x[11]);
						}
					    }
					}
				    }
				}
			    }
			}
		    }
		}
	    }
	}

    }
  return win_printf_OK("Found no match!");
}

# endif




MENU *assembleOligos_plot_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"change param", do_assembleOligos_param,NULL,0,NULL);
  add_item_to_menu(mn,"assemble oligos from ds", do_assembleOligos_rescale_data_set,NULL,0,NULL);
  add_item_to_menu(mn,"assemble oligos from file",do_assembleOligos_rescale_data_from_file ,NULL,0,NULL);



  add_item_to_menu(mn,"Simulate assemble oligos", do_assembleOligos_simulate,NULL,0,NULL);
  add_item_to_menu(mn,"dump best sequence", do_assembleOligos_dump_seq,NULL,0,NULL);
  add_item_to_menu(mn,"dump current sequence", do_assembleOligos_dump_seq_to_go,NULL,0,NULL);
  add_item_to_menu(mn,"Test noise robustness", do_assembleOligos_test_mix_vs_noise,NULL,0,NULL);
  add_item_to_menu(mn,"Predict noise robustness", do_assembleOligos_test_mix_vs_noise_erf,NULL,0,NULL);



  add_item_to_menu(mn,"Create statistic image",do_assembleOligos_create_image_result,NULL,0,NULL);
  add_item_to_menu(mn,"find_ceb25_multiplex",do_find_ceb25_multiplex_set,NULL,0,NULL);

  //add_item_to_menu(mn,"plot rescale in Y", do_assembleOligos_rescale_plot,NULL,0,NULL);
  return mn;
}

int	assembleOligos_main(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  add_plot_treat_menu_item ( "assembleOligos", NULL, assembleOligos_plot_menu(), 0, NULL);
  return D_O_K;
}

int	assembleOligos_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  remove_item_to_menu(plot_treat_menu, "assembleOligos", NULL, NULL);
  return D_O_K;
}
#endif
