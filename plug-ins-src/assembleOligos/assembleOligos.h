#ifndef _ASSEMBLEOLIGOS_H_
#define _ASSEMBLEOLIGOS_H_

PXV_FUNC(int, do_assembleOligos_rescale_plot, (void));
PXV_FUNC(MENU*, assembleOligos_plot_menu, (void));
PXV_FUNC(int, do_assembleOligos_rescale_data_set, (void));
PXV_FUNC(int, assembleOligos_main, (int argc, char **argv));
#endif

