/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _NIDAQMX_FOCUS_C_
#define _NIDAQMX_FOCUS_C_

#include "allegro.h"
#include "winalleg.h"
#include "xvin.h"


/* If you include other plug-ins header do it here*/ 
#include <NIDAQmx.h>
#include "../logfile/logfile.h"

#include "xv_plugins.h"	// for inclusions of libraries that control dynamic loading of libraries.

XV_FUNC(int, grep_plug_ins, (char *name));

/* But not below this define */
#define BUILDING_PLUGINS_DLL
#include "../trackBead/magnetscontrol.h"


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
#include "nidaqmx_focus.h"

#define DAQmxErrChk(functionCall) if( DAQmxFailed(error=(functionCall)) ) goto Error; else


int _init_focus_OK(void)
{ 
  return 0;
}



///////////////////////////////////////////////////////////////////
//////////Functions controlling the pifoc position/////////////////
///////////////////////////////////////////////////////////////////

/** This is the task to set the pifoc position. 
It controls directly the pifoc line and it sends a single value.

\param float value the value of pifoc position in volts.

\author Francesco Mosconi
\version 
*/
int task__pifoc_task(float value)
{
  TaskHandle taskHandle;
  int32   error=0;
  char    errBuff[2048]={'\0'};
  float64     data[1] ;
  data[0] = (float64) value;

  /*********************************************/
  // DAQmx Configure Code
  /*********************************************/

  DAQmxErrChk (DAQmxCreateTask("",&taskHandle));
  DAQmxErrChk (DAQmxCreateAOVoltageChan(taskHandle, PIFOC_LINE,
					"",-10.0,10.0,DAQmx_Val_Volts,""));

  /*********************************************/
  // DAQmx Start Code
  /*********************************************/
  DAQmxErrChk (DAQmxStartTask(taskHandle));

  /*********************************************/
  // DAQmx Write Code
  /*********************************************/
  DAQmxErrChk (DAQmxWriteAnalogF64(taskHandle,1,1,10.0,
				   DAQmx_Val_GroupByChannel,data,NULL,NULL));
   
 Error:
  if( DAQmxFailed(error) )
    DAQmxGetExtendedErrorInfo(errBuff,2048);
  if( taskHandle ) {
    /*********************************************/
    // DAQmx Stop Code
    /*********************************************/
    DAQmxStopTask(taskHandle);
    DAQmxClearTask(taskHandle);
    taskHandle = 0;
  }
  if( DAQmxFailed(error) )
    win_report("DAQmx Error: %s\n",errBuff);
  return 0;
}


/** Sets the PIFOC value.

\param float value the value of pifoc position in microns.

\author Francesco Mosconi
\version 18/04/07
 */
int pifoc__set(float value)
{
  /* Checks that the value is not off-limits */
  nidaqmx_obj_position = value;
  nidaqmx_obj_position = (nidaqmx_obj_position/PIFOC_TOTAL_COURSE > PIFOC_MAX_VOLT) ? PIFOC_MAX_VOLT*PIFOC_TOTAL_COURSE : nidaqmx_obj_position;
  nidaqmx_obj_position = (nidaqmx_obj_position/PIFOC_TOTAL_COURSE < PIFOC_MIN_VOLT) ? PIFOC_MIN_VOLT*PIFOC_TOTAL_COURSE : nidaqmx_obj_position;
  task__pifoc_task(nidaqmx_obj_position/PIFOC_TOTAL_COURSE);
  return 0;
}

/** Directly sets the PIFOC to a given position.

\author Adrien Meglio
\version 16/01/07
*/
int pifoc__set_interface(void)
{
    float pifocTemp;

    if(updating_menu_state != 0) return D_O_K;

    pifocTemp = 0;
    win_scanf("Position du PIFOC : %f \\mu m",&pifocTemp);

    pifoc__set(pifocTemp);

    return D_O_K;
}

///////////////////////////////////////////////////////////////////
//////////Functions needed for interface in trackBead//////////////
///////////////////////////////////////////////////////////////////
int _read_Z_value(void)
{
  return (int)(10*nidaqmx_obj_position);
}

int _set_Z_step(float z)  /* in micron */
{
  nidaqmx_Z_step = z;
  return 0;
}

float _read_Z_value_accurate_in_micron(void)
{
  return nidaqmx_obj_position;	
}

float _read_Z_value_OK(int *error) /* in microns */
{
  return nidaqmx_obj_position;	
}

float _read_last_Z_value(void) /* in microns */
{
	return nidaqmx_obj_position;	
}

int _set_Z_value(float z)  /* in microns */
{
  pifoc__set(z);
  return 0;
}

int _set_Z_obj_accurate(float zstart)
{
  int er = 0;	
  _set_Z_step(0.045);
  _set_Z_value(zstart);
  return er;
}

int _set_Z_value_OK(float z) 
{
  pifoc__set(z);
  return 0;
}

int _inc_Z_value(int step)  /* in micron */
{
  _set_Z_value(nidaqmx_obj_position + nidaqmx_Z_step*step);
  return 0;
}

int _small_inc_Z_value(int up)  
{
  int nstep;
	
  _set_Z_step(0.1);
  nstep = (up>=0)? 1:-1;
  _set_Z_value(nidaqmx_obj_position + nidaqmx_Z_step*nstep);
  //display_title_message("Z obj = %g \\mu m Rot %g Z mag %g mm",
  //  (float)dummy_read_Z_value()/10,n_rota,n_magnet_z);
  return 0;
}

int _big_inc_Z_value(int up)  
{
  int nstep;
	
  _set_Z_step(0.1);
  nstep = (up>=0)? 10:-10;
  _set_Z_value(nidaqmx_obj_position + nidaqmx_Z_step*nstep);

  //	display_title_message("Z obj = %g \\mu m Rot %g Z mag %g mm",
  //  (float)dummy_read_Z_value()/10,n_rota,n_magnet_z);

  return 0;
}



int	nidaqmx_focus_main(int argc, char **argv)
{
  char pl_name[256];
  char *argvp[2];
/*   sprintf(pl_name,"nidaqmxwrap_AIAO"); */
/*   if (grep_plug_ins(pl_name) < 0) */
/*     { */
/*       argvp[0] = pl_name; */
/*       argvp[1] = NULL; */
/*       load_plugin(1, argvp); */
/*     } */
  return D_O_K;
}

int	nidaqmx_focus_unload(int argc, char **argv)
{
	return D_O_K;
}
#endif

