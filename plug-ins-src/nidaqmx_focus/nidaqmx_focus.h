#ifndef _NIDAQMX_FOCUS_H_
#define _NIDAQMX_FOCUS_H_

///These definitions should be included in the config file, ask JF
#define PIFOC_TOTAL_COURSE_MICRONS 100.0
#define PIFOC_TOTAL_COURSE 10.0 // The percentage-of-course to input-volts multiplication factor
#define PIFOC_MAX_VOLT 10.0
#define PIFOC_MIN_VOLT 0.0

#ifdef _NIDAQMX_FOCUS_C_
float   nidaqmx_obj_position = 0, nidaqmx_Z_step = 0.1;
char    PIFOC_LINE[16] = "Dev1/ao3";
#endif

#ifndef _NIDAQMX_FOCUS_C_
PXV_VAR(float, nidaqmx_obj_position);
PXV_VAR(float, nidaqmx_Z_step);
#endif

PXV_FUNC(MENU*, nidaqmx_focus_plot_menu, (void));
PXV_FUNC(int, nidaqmx_focus_main, (int argc, char **argv));
#endif

