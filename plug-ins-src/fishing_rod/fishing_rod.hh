#pragma once
#include "restriction-enzyme.hh"
#include "../fasta_utils/fasta_utils.h"
#include <vector>

int plot_rest_enzyme_variable_site(const sqd *msqd, RestrictionEnzyme &restEnz);
