#include "fishing_rod.hh"
#include "../dna_utils/utils.h"
#include <regex>
#include <string>
#include <set>
//GAAGAC (2/6)
//GGTCTC (1/5)

//typedef std::set<Sequence> SequenceSet;

int load_red_file(std::istream & is)
{
    std::string enzinfo = "";
    std::regex reg = std::regex("(.+):(.+)");
    std::smatch match;

    while (is.good())
    {
        std::getline(is, enzinfo);
    }
    return 0;
} 

int plot_rest_enzyme_variable_site(const sqd *msqd, RestrictionEnzyme &restEnz)
{
    std::stringstream ss;
    bool varSites[4] = {0};
    int obsVarSites[4] = {0};

    ss << "choose variable sites for this enzyme :\n";
    ss << "%%Q"; 
    restEnz.printRestrictionSite(ss);

    ss << "%%q" << std::endl;
    ss << "%%q"; 
    restEnz.printComplementaryRestrictionSite(ss);
    ss << "%%q" << std::endl;

    if (restEnz.isCleavingLeft())
    {
        varSites[0] = restEnz.leftCleave() > 0;
        varSites[1] = restEnz.rightCleave() > 0;
    }
    if (restEnz.isCleavingRight())
    {
        varSites[2] = restEnz.leftCleaveOpStrand() > 0;
        varSites[3] = restEnz.rightCleaveOpStrand() > 0;
    }


    char *scan = protect_string(ss.str().c_str(), ss.str().size());
    win_scanf(scan, &obsVarSites);
    free(scan);

    return D_O_K;
}
