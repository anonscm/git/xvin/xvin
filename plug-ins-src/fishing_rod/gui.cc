#include "gui.hh"
#include "restriction-enzyme.hh"
#include "fishing_rod.hh"
#include <iostream>
#include "sequence.hh"

SequenceSet g_sequences; 

void testRestEnzParser(void)
{

    std::string name ="Bbs1";
    std::string recognitionSite = std::string("GAAGAC (2/6)");
    RestrictionEnzyme en = RestrictionEnzyme(name, recognitionSite);
    name = "Bsa1";
    recognitionSite = "GGTCTC (1/5)";
    en = RestrictionEnzyme(name, recognitionSite);
    std::cout << recognitionSite << " | "<< en << std::endl;
    name = "AjuI";
    recognitionSite = "(7/12) GAANNNNNNNTTGG (11/6)";
    en = RestrictionEnzyme(name, recognitionSite);
    std::cout << recognitionSite << " | "<< en << std::endl;
    name = "Ama87I";
    recognitionSite = "C^YCGRG";
    en = RestrictionEnzyme(name, recognitionSite);
    std::cout << recognitionSite << " | "<< en << std::endl;
    name = "Conflict";
    recognitionSite = "C^YCGRG (2/3)";
    en = RestrictionEnzyme(name, recognitionSite);
    std::cout << recognitionSite << " | "<< en << std::endl;
}
int do_import_sequence(void)
{
    if (updating_menu_state != 0)
    {
        return D_O_K;
    }


}
int do_calculate_fragment_count_with_overhang(void)
{
    int form_ret = 0;
    char rest_enz1[128] = {0};
    char rest_enz2[128] = {0};
    char overhang1[128] = {0};
    char overhang2[128] = {0};

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }
    strncpy(rest_enz1, get_config_string("FISHING-ROD", "restriction-enzyme1", ""),sizeof(rest_enz1));
    strncpy(rest_enz2, get_config_string("FISHING-ROD", "restriction-enzyme2", ""),sizeof(rest_enz2));
    strncpy(overhang1, get_config_string("FISHING-ROD", "overhang1", ""),sizeof(overhang1));
    strncpy(overhang2, get_config_string("FISHING-ROD", "overhang2", ""),sizeof(overhang2));

    form_ret = win_scanf("restriction enzyme 1 %s\n restriction enzyme 2 %s\n overhang 1 %s\n overhang 2 %s",
                         rest_enz1, rest_enz2, overhang1, overhang2);

    if (form_ret != D_O_K)
        return form_ret;

    set_config_string("FISHING-ROD", "restriction-enzyme1", rest_enz1);
    set_config_string("FISHING-ROD", "restriction-enzyme2", rest_enz2);
    set_config_string("FISHING-ROD", "overhang1", overhang1);
    set_config_string("FISHING-ROD", "overhang2", overhang2);



    
}

MENU *fishing_rod_plot_menu(void)
{
    static MENU mn[32];

    if (mn[0].text != NULL)
        return mn;
    add_item_to_menu(mn, "calculate_fragment_count_with_overhang", do_calculate_fragment_count_with_overhang, NULL, 0, NULL);
    return mn;
}

int fishing_rod_main(int argc, char **argv)
{
    (void) argc;
    (void) argv;

    //testRestEnzParser();
    fasta_utils_main(0, NULL);

    add_plot_treat_menu_item ( "fishing_rod", NULL, fishing_rod_plot_menu(), 0, NULL);
    return D_O_K;
}

int fishing_rod_unload(int argc, char **argv)
{
    (void) argc;
    (void) argv;

    remove_item_to_menu(plot_treat_menu, "fishing_rod", NULL, NULL);
    return D_O_K;
}

int do_plot(void)
{
    sqd **sequences = NULL;
    int nb_sequences = 0;
    std::string name ="Bbs1";
    std::string recognitionSite = std::string("GAAGAC (2/6)");
    RestrictionEnzyme en = RestrictionEnzyme(name, recognitionSite);

    if (updating_menu_state != 0)
    {
//        if (fingerprints.size() > 0 || needles.size() > 0)
//            active_menu->flags &= ~D_DISABLED;
//        else
//            active_menu->flags |=  D_DISABLED;
//
        return D_O_K;
    }

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("Create an random subsequence of a barcode");
    }

    get_loaded_sequences(&sequences, &nb_sequences);

    if (nb_sequences > 0)
        plot_rest_enzyme_variable_site(*sequences, en);
    else
        win_printf("no sequences loaded");
    return D_O_K;
}
