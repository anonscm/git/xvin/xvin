#pragma once
#include "xvin.h"

PXV_FUNC(MENU, *fishing_rod_plot_menu, (void));
PXV_FUNC(int, fishing_rod_main, (int argc, char **argv));
PXV_FUNC(int, fishing_rod_unload, (int argc, char **argv));
PXV_FUNC(int, do_calculate_fragment_count_with_overhang,(void));
int do_plot(void);
