#include "restriction-enzyme.hh"
#include "xvin.h"
#include "../dna_utils/utils.h"
#include <regex>
#include <iostream>

RestrictionEnzyme::RestrictionEnzyme(std::string& name, std::string& recognitionSite)
{
    name_ = name;
    std::smatch match;
    std::regex reg = std::regex("(?:\\(([0-9]+)\\/([0-9]+)\\))?\\s*([ACGTURYSWKMBDHVN]*)(\\^)?([ACGTURYSWKMBDHVN]+)\\s*(?:\\(([0-9]+)\\/([0-9]+)\\))?");

    std::regex_match(recognitionSite, match, reg);

    isCleavingLeft_ = match[1] != "";

    if (isCleavingLeft_)
    {
        leftCleave_ = std::stoi(match[1]);
        leftCleaveOpStrand_ = std::stoi(match[2]);
    }
    recognizedSequence_ = match[3].str() + match[5].str();

    isCleavingRight_ = match[4] != "";

    if (isCleavingRight_)
    {
        rightCleave_ = -match[5].str().size();
        rightCleaveOpStrand_ = rightCleave_;
    }

    if (match[6] != "")
    {
        int rcl = stoi(match[6]);
        int rclop = stoi(match[7]);

        if (isCleavingRight_ && (rcl != rightCleave_ || rclop != rightCleaveOpStrand_))
        {
            win_printf("Warning : conflicting data between ^ cursor (%d/%d)"
                       " and right parentheses (%d/%d) \n cursor informations"
                       " will be discarded", rightCleave_, rightCleaveOpStrand_, rcl, rclop);
        }

        isCleavingRight_ = true;
        rightCleave_ = rcl;
        rightCleaveOpStrand_ = rclop;
    }
}

RestrictionEnzyme::~RestrictionEnzyme()
{
}

std::ostream &operator<< (std::ostream & os, const RestrictionEnzyme& res)
{
    if (res.isCleavingLeft_)
        os << "(" << res.leftCleave_ << "/" << res.leftCleaveOpStrand_ << ") ";
    os << res.recognizedSequence_;
    if (res.isCleavingRight_)
        os << " (" << res.rightCleave_ << "/" << res.rightCleaveOpStrand_ << ")";

    return os;
}

std::ostream &RestrictionEnzyme::printRestrictionSite (std::ostream & os, std::string &recognizedSequence)
{

    int posInSeq = 0;
    int maxPos = recognizedSequence.size();
    int leftCleavePos = maxPos + 1; // by default, no cleavage
    int rightCleavePos = maxPos + 1; // by default, no cleavage

    if (isCleavingLeft_ && leftCleave_ >= 0)
    {
        posInSeq = -leftCleave_;
        os << "^";
    }
    else
    {
        leftCleavePos = -leftCleave_;
    }

    if (isCleavingRight_ && rightCleave_ > 0)
    {
        maxPos += rightCleave_;
        leftCleavePos = recognizedSequence.size() + rightCleave_;
    }


    for(;posInSeq < maxPos; ++posInSeq)
    {
        if(posInSeq == leftCleavePos || posInSeq == rightCleavePos)
        {
            os << "\\^";
        }
        if (posInSeq < 0 || posInSeq >= (int) recognizedSequence.size())
        {
            os << "N";
        }
        else
        {
            os << recognizedSequence[posInSeq];
        }
    }
    return os;
}

std::ostream &RestrictionEnzyme::printComplementaryRestrictionSite(std::ostream & os)
{
    std::string comp = std::string(dna_complementary_reverse(recognizedSequence_.c_str()));
    return printRestrictionSite(os, comp);
}

std::ostream &RestrictionEnzyme::printRestrictionSite(std::ostream & os)
{
    return printRestrictionSite(os, recognizedSequence_);
}

