#pragma once
#include "restriction-enzyme.hh"
#include "sequence.hh"
class RestrictionSite
{
public:
    RestrictionSite (RestrictionEnzyme& re, Sequence& se);
    virtual ~RestrictionSite ();

private:
    RestrictionEnzyme& restrictionEnzyme_;
    Sequence& sequence_;
};
