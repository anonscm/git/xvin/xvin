#pragma once
inline bool RestrictionEnzyme::isCleavingLeft()
{
    return isCleavingLeft_;
}

inline bool RestrictionEnzyme::isCleavingRight()
{
    return isCleavingRight_;
}

inline const std::string& RestrictionEnzyme::name()
{
    return name_;
}

inline const std::string& RestrictionEnzyme::recognizedSequence()
{
    return recognizedSequence_;
}

inline int RestrictionEnzyme::leftCleave()
{
    return leftCleave_;
}

inline int RestrictionEnzyme::leftCleaveOpStrand()
{
    return leftCleaveOpStrand_;
}

inline int RestrictionEnzyme::rightCleave()
{
    return rightCleave_;
}

inline int RestrictionEnzyme::rightCleaveOpStrand()
{
    return rightCleaveOpStrand_;
}
