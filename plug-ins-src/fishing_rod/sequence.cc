#include "sequence.hh"
#include "../dna_utils/utils.h"
#include <cassert>
Sequence::Sequence(const sqd& seq)
    : name_(seq.filename),
      description_(seq.description),
      origin_(0),
      originStart_(0),
      originEnd_(0),
      pageSize_(seq.pageSize),
      pageCount_(seq.nPage),
      size_(seq.totalSize)
{
    for (int i = 0; i < pageCount_; ++i)
    {
        int curSize = (size_ - i * pageSize_) > pageSize_ ? pageSize_ : size_ - i * pageSize_;
        seq_.push_back(std::string(seq.seq[i], curSize));
    }
}

Sequence::Sequence(std::string name, std::string description, std::string origin)
    : name_(name),
      description_(description),
      origin_(new std::string(origin)),
      originStart_(0),
      originEnd_(0),
      pageSize_(SQ_DEFAULT_PAGE_SIZE),
      pageCount_(0),
      size_(0)
{
}

Sequence::Sequence(const Sequence& seq)
    : seq_(seq.seq_),
      name_(seq.name_),
      description_(seq.description_),
      origin_(seq.origin_),
      originStart_(seq.originStart_),
      originEnd_(seq.originEnd_),
      pageSize_(seq.pageSize_),
      pageCount_(seq.pageCount_),
      size_(seq.size_)
{
}

Sequence::~Sequence()
{
}

char Sequence::operator[](const int relIdx)
{
    int absIdx = relIdx - originStart_;
    assert(absIdx >= 0);

    return seq_[(absIdx) / pageSize_][(absIdx % pageSize_)];
}


std::vector<int> *Sequence::findSequence(const std::string& oligo)
{
    std::vector<int> *matchs = new std::vector<int>();

    for (int i = originStart_; i < (originStart_ + size_) - (int) oligo.length(); i++)
    {
        bool match = true;

        for (int j = 0; j < (int) oligo.length() && match; j++)
        {
            int k = i + j;
            char curNucleo = this->operator[](k);

            if (((curNucleo & 0x5F) != 'N'))
            {
                match = nucleo_cmp(oligo[j], curNucleo);
            }
        }

        if (match)
        {
            matchs->push_back(i);
        }
    }

    return matchs;
}
