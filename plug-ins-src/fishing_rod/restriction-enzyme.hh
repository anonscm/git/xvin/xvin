#pragma once

#include <string>
#include <iostream>

class RestrictionEnzyme
{
  public:
    RestrictionEnzyme(std::string& name, std::string& recognitionSite);
    virtual ~RestrictionEnzyme();

    std::ostream &printRestrictionSite(std::ostream & os);
    std::ostream &printComplementaryRestrictionSite(std::ostream & os);
    std::ostream &printRestrictionSite(std::ostream & os, std::string &recognizedSequence);

    const std::string& name();
    const std::string& recognizedSequence();
    bool isCleavingLeft();
    bool isCleavingRight();

    int leftCleave();
    int leftCleaveOpStrand();
    int rightCleave();
    int rightCleaveOpStrand();

    friend std::ostream& operator<<(std::ostream& os, const RestrictionEnzyme& dt);


  protected:
    std::string name_;
    std::string recognizedSequence_;
    bool isCleavingLeft_;
    bool isCleavingRight_;

    int leftCleave_;
    int leftCleaveOpStrand_;
    int rightCleave_;
    int rightCleaveOpStrand_;
};
 
#include "restriction-enzyme.hxx"
