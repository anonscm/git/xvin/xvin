#pragma once
#include "../dna_utils/sequence.h"
#include <string>
#include <set>
#include <vector>

#define SQ_DEFAULT_PAGE_SIZE 4096

class Sequence
{
  public:
    Sequence(const sqd& seq);
    Sequence(const Sequence& seq);
    Sequence(std::string name, std::string description, std::string origin);
    virtual ~Sequence();

    Sequence *subSequence(int start, int end);
    bool indexInBounds(int idx);
    std::vector<int> *findSequence(const std::string &oligo);
    //std::vector<int> *findSequenceMismatchThreshold(const std::string &oligo);

    char operator[] (const int relIdx);
 
  protected:
    std::vector<std::string> seq_;
    std::string name_;
    std::string description_;
    std::string* origin_;
    int originStart_;
    int originEnd_;
    int pageSize_;
    int pageCount_;
    int size_;

};


typedef std::set<Sequence *> SequenceSet;
