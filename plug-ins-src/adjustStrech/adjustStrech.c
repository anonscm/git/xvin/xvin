/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _ADJUSTSTRECH_C_
#define _ADJUSTSTRECH_C_

#include <allegro.h>
# include "xvin.h"

/* If you include other regular header do it here*/ 
# include "../nrutil/nrutil.h"

/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "adjustStrech.h"

d_s *ds_peak_nts = NULL;
O_p *opds_peak_nts = NULL;




int ds_peak_nts_copy(void)
{
    int i;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    pr = find_pr_in_current_dialog(NULL);

    if (pr == NULL)
    {
        return win_printf_OK("plot not found");
    }

    op = pr->one_p;

    if (op == NULL)
    {
        return D_O_K;
    }

    ds = op->dat[op->cur_dat];

    if (opds_peak_nts != NULL)
    {
        free_one_plot(opds_peak_nts);
        opds_peak_nts = NULL;
        ds_peak_nts = NULL;
    }

    i = op->n_dat;
    op->n_dat = 0;

    if ((opds_peak_nts = duplicate_plot_data(op, NULL)) == NULL)
    {
        op->n_dat = i;
        win_printf_OK("Could not duplicate plot");
    }
    else
    {
        ds_peak_nts = duplicate_data_set(ds, NULL);

        if (ds_peak_nts == NULL)
        {
            op->n_dat = i;
            win_printf_OK("Could not duplicate ds");
        }
        else
        {
            if (add_one_plot_data(opds_peak_nts, IS_DATA_SET, (void *)ds_peak_nts))
            {
                op->n_dat = i;
                win_printf_OK("Could not add ds");
            }
        }
    }

    op->n_dat = i;
    remove_all_keyboard_proc();
    scan_and_update(plot_menu);
    return D_O_K;
}



int do_adjustStrech_hello(void)
{
  int i;

  if(updating_menu_state != 0)	return D_O_K;

  i = win_printf("Hello from adjustStrech");
  if (i == WIN_CANCEL) win_printf_OK("you have press WIN_CANCEL");
  else win_printf_OK("you have press OK");
  return 0;
}

int do_adjustStrech_rescale_data_set(void)
{
  int i, j;
  O_p *op = NULL;
  int nf;
  static float factor = 1;
  d_s *ds, *dsi, *dsfit = NULL;
  pltreg *pr = NULL;
  static int puse[6] = {1,1,1,1,1,1};
  static int rid[6] = {0,1,2,3,4,5};
  static int did[6] = {0,1,2,3,4,5};
  static int dsid[4] = {0,1,2,3};
  double fit_a = 0, fit_b = 0;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");

  i = win_scanf("Reajusting data stretching between two histograms using reference oligos\n"
		"Specify the datasets index: \n"
		"%4d->For reference histogram; %4d->for peaks of reference histogram\n"
		"%4d->For data histogram; %4d->for peaks of data histogram\n"
		"Specify matching peaks index for both peaks ds:\n"
		"Peak 1: Used->%b %4d->ref index; %4d->data index\n"
		"Peak 2: Used->%b %4d->ref index; %4d->data index\n"
		"Peak 3: Used->%b %4d->ref index; %4d->data index\n"
		"Peak 4: Used->%b %4d->ref index; %4d->data index\n"
		"Peak 5: Used->%b %4d->ref index; %4d->data index\n"
		"Peak 6: Used->%b %4d->ref index; %4d->data index\n"		
		,dsid,dsid+1,dsid+2,dsid+3
		,puse,rid,did
		,puse+1,rid+1,did+1
		,puse+2,rid+2,did+2
		,puse+3,rid+3,did+3
		,puse+4,rid+4,did+4
		,puse+5,rid+5,did+5);
  if (i == WIN_CANCEL)	return OFF;

  for (i = 0; i < 4; i++)
    {
      if ((dsid[i] < 0) || (dsid[i] >= op->n_dat))
	return win_printf_OK("ds out of range %d [0,%d[!",dsid[i],op->n_dat);
      for (j = 0; j < 6; j++)
	{
	  if ((puse[j] > 0) && (rid[j] < 0 || rid[j] >= op->dat[dsid[1]]->nx))
	    return win_printf_OK("Reference peak point index out of range %d [0,%d[!"
				 ,rid[j],op->dat[dsid[1]]->nx);
	  if ((puse[j] > 0) && (did[j] < 0 || did[j] >= op->dat[dsid[3]]->nx))
	    return win_printf_OK("Data peak point index out of range %d [0,%d[!"
				 ,did[j],op->dat[dsid[3]]->nx);	  
	}
    }
  
  for (i = 0, nf = 0; i < 6; i++)
    nf += (puse[i])? 1 : 0;
  
  if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");

  if (op->dat[dsid[3]]->xe != NULL)
    alloc_data_set_x_error(ds);
  if (op->dat[dsid[1]]->xe != NULL)
    alloc_data_set_y_error(ds);  
  
  for (j = i = 0; j < 6; j++)
  {
    if (puse[j] && i < nf)
      {
	ds->xd[i] = op->dat[dsid[3]]->xd[did[j]];
	if (op->dat[dsid[3]]->xe != NULL && ds->ye != NULL)
	  ds->xe[i] = op->dat[dsid[3]]->xe[did[j]];
	ds->yd[i] = op->dat[dsid[1]]->xd[rid[j]];
	if (op->dat[dsid[1]]->xe != NULL && ds->xe != NULL)
	  ds->ye[i] = op->dat[dsid[1]]->xe[rid[j]];
	i++;
      }
  }

  dsfit = least_square_fit_on_ds(ds, 1, &fit_a, &fit_b);
  add_one_plot_data(op, IS_DATA_SET, (void*)dsfit);

  for (j = 0; j < op->dat[dsid[2]]->nx; j++)
    op->dat[dsid[2]]->xd[j] = fit_b + fit_a * op->dat[dsid[2]]->xd[j];
  for (j = 0; j < op->dat[dsid[3]]->nx; j++)
    op->dat[dsid[3]]->xd[j] = fit_b + fit_a * op->dat[dsid[3]]->xd[j];   

  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"y multiplied by %f",factor);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}

struct cm_peak
{
  int idx;
  int idy;
  float dist;
  char ok;
};

int do_adjustStrech_draw_X_ds1_vs_ds2_compatible_points(void)
{
  int i, j, np;
  O_p *op = NULL, *opn = NULL;
  static int nds1 = 0, nds2 = 1;
  static float thres = 1;
  struct cm_peak *cmp = NULL;
  int i_cmp = 0;
  d_s *ds, *dsi = NULL, *dsi2 = NULL;
  pltreg *pr = NULL;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");

  nds1 = op->cur_dat;
  i = win_scanf("Drawing X coord. of points comaptible between two datasets using error bars\n"
		"Define the index of first ds %4d\n"
		"Define the index of second ds %4d\n"
		"Define the minimum Y value of both points %4f\n"
		,&nds1,&nds2,&thres);
  
  if (i == WIN_CANCEL)	return OFF;

  if (nds1 < 0 || nds1 >= op->n_dat)
    return win_printf_OK("first ds %d out of [0,%d[",nds1,op->n_dat);
  if (nds2 < 0 || nds2 >= op->n_dat)
    return win_printf_OK("second ds %d out of [0,%d[",nds2,op->n_dat);  

  dsi = op->dat[nds1];
  dsi2 = op->dat[nds2];
  if (dsi->xe == NULL)
    return win_printf_OK("ds 1 has no error bars in x");
  if (dsi2->xe == NULL)
    return win_printf_OK("ds 2 has no error bars in x");  

  np = (dsi->nx > dsi2->nx) ? dsi->nx : dsi2->nx;
  cmp = (struct cm_peak *)calloc(np,sizeof(struct cm_peak));
  if (cmp == NULL) return win_printf_OK("cannot create peak data structure !");
  if ((opn = create_and_attach_one_plot(pr, 16, 16, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  ds = opn->dat[0];
  alloc_data_set_x_error(ds);
  alloc_data_set_y_error(ds);
  
  ds->nx = ds->ny = 0;
  int jbest;
  float xmin, xmax, xmin2, xmax2, erx, ery, dmin;
  for (i = i_cmp = 0; i < dsi->nx; i++)
    {
      if (dsi->yd[i] < thres) continue;
      xmin = (dsi->xed != NULL) ? dsi->xd[i] - dsi->xed[i] : dsi->xd[i] - dsi->xe[i];
      xmax = dsi->xd[i] + dsi->xe[i];
      for (j = 0, jbest = -1, dmin = -1; j < dsi2->nx; j++)
	{
	  if (dsi2->yd[j] < thres) continue;
	  xmin2 = (dsi2->xed != NULL) ? dsi2->xd[j] - dsi2->xed[j] : dsi2->xd[j] - dsi2->xe[j];
	  xmax2 = dsi2->xd[j] + dsi2->xe[j];
	  if ((xmin2 >= xmin && xmin2 < xmax) || (xmax2 >= xmin && xmax2 < xmax)
	    || (xmin >= xmin2 && xmin < xmax2) || (xmax >= xmin2 && xmax < xmax2))
	    {
	      if ((dmin  < 0) || (fabs(dsi->xd[i] - dsi2->xd[j]) < dmin))
		dmin = fabs(dsi->xd[i] - dsi2->xd[jbest = j]);
	    }
	}
      if (dmin >= 0 && i_cmp < np)
	{
	  cmp[i_cmp].idx = i;
	  cmp[i_cmp].idy = jbest;
	  cmp[i_cmp++].dist = fabs(dsi->xd[i] - dsi2->xd[jbest]);
	}      
    }
  for (i = 0; i < i_cmp; i++)
    { // we may have the same idy for different idx
      for (j = 0, jbest = i, dmin = -1; j < i_cmp; j++)
	{
	  if (cmp[j].idy == cmp[i].idy)
	    {
	      cmp[j].ok = 0;
	      if (dmin < 0 || cmp[j].dist < cmp[jbest].dist)
		dmin = cmp[jbest = j].dist;
	    }
	}
      cmp[jbest].ok = 1;
    }

  for (i = 0; i < i_cmp; i++)
    {
      if (cmp[i].ok == 1)
	{
	  erx = (dsi->xbu != NULL) ? dsi->xbu[cmp[i].idx] : dsi->xe[cmp[i].idx];
	  ery = (dsi2->xbu != NULL) ? dsi2->xbu[cmp[i].idy] : dsi->xe[cmp[i].idy];
	  add_new_point_with_xy_error_to_ds(ds, dsi->xd[cmp[i].idx], erx , dsi2->xd[cmp[i].idy], ery);
	}
    }
  
  free(cmp);
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"y(x) common points in x with y > %g",thres);
  if (op->x_title != NULL) set_plot_x_title(opn, "%s", op->x_title);
  if (op->y_title != NULL) set_plot_y_title(opn, "%s", op->y_title);
  opn->filename = Transfer_filename(op->filename);
  uns_op_2_op(opn, op);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
  
}


int do_adjustStrech_draw_X_ds_vs_expected_peak_compatible_points(void)
{
  int i, j, np;
  O_p *op = NULL, *opn = NULL;
  static int nds1 = 0;
  static float thres = 1;
  struct cm_peak *cmp = NULL;
  int i_cmp = 0;
  d_s *ds = NULL, *dsi = NULL, *dsi2 = NULL;
  pltreg *pr = NULL;
  static float dzdn = 0.9, fdzdn = 10;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");

  if (ds_peak_nts == NULL)
    return win_printf_OK("You need to copy first a dataset containing the expected peak position");
  
  i = win_scanf("Drawing X coord. of points comaptible between the cuurent dataset \n"
		"and expected peak position using error bars\n"
		"Define the minimum Y value of data points %4f\n"
		"Define the stretching mean value %4f and tolerence in %% %3f\n"		
		,&thres,&dzdn,&fdzdn);
  
  if (i == WIN_CANCEL)	return OFF;


  dsi = op->dat[nds1];
  if (dsi->xe == NULL)
    return win_printf_OK("ds 1 has no error bars in x");

  np = (dsi->nx > dsi2->nx) ? dsi->nx : dsi2->nx;
  cmp = (struct cm_peak *)calloc(np,sizeof(struct cm_peak));
  if (cmp == NULL) return win_printf_OK("cannot create peak data structure !");
  if ((opn = create_and_attach_one_plot(pr, 16, 16, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  ds = opn->dat[0];
  alloc_data_set_x_error(ds);
  alloc_data_set_y_error(ds);
  
  ds->nx = ds->ny = 0;
  int jbest;
  float xmin, xmax, xmin2, xmax2, erx, ery, dmin;
  for (i = i_cmp = 0; i < dsi->nx; i++)
    {
      if (dsi->yd[i] < thres) continue;
      xmin = (dsi->xed != NULL) ? dsi->xd[i] - dsi->xed[i] : dsi->xd[i] - dsi->xe[i];
      xmax = dsi->xd[i] + dsi->xe[i];
      for (j = 0, jbest = -1, dmin = -1; j < dsi2->nx; j++)
	{
	  if (dsi2->yd[j] < thres) continue;
	  xmin2 = (dsi2->xed != NULL) ? dsi2->xd[j] - dsi2->xed[j] : dsi2->xd[j] - dsi2->xe[j];
	  xmax2 = dsi2->xd[j] + dsi2->xe[j];
	  if ((xmin2 >= xmin && xmin2 < xmax) || (xmax2 >= xmin && xmax2 < xmax)
	    || (xmin >= xmin2 && xmin < xmax2) || (xmax >= xmin2 && xmax < xmax2))
	    {
	      if ((dmin  < 0) || (fabs(dsi->xd[i] - dsi2->xd[j]) < dmin))
		dmin = fabs(dsi->xd[i] - dsi2->xd[jbest = j]);
	    }
	}
      if (dmin >= 0 && i_cmp < np)
	{
	  cmp[i_cmp].idx = i;
	  cmp[i_cmp].idy = jbest;
	  cmp[i_cmp++].dist = fabs(dsi->xd[i] - dsi2->xd[jbest]);
	}      
    }
  for (i = 0; i < i_cmp; i++)
    { // we may have the same idy for different idx
      for (j = 0, jbest = i, dmin = -1; j < i_cmp; j++)
	{
	  if (cmp[j].idy == cmp[i].idy)
	    {
	      cmp[j].ok = 0;
	      if (dmin < 0 || cmp[j].dist < cmp[jbest].dist)
		dmin = cmp[jbest = j].dist;
	    }
	}
      cmp[jbest].ok = 1;
    }

  for (i = 0; i < i_cmp; i++)
    {
      if (cmp[i].ok == 1)
	{
	  erx = (dsi->xbu != NULL) ? dsi->xbu[cmp[i].idx] : dsi->xe[cmp[i].idx];
	  ery = (dsi2->xbu != NULL) ? dsi2->xbu[cmp[i].idy] : dsi->xe[cmp[i].idy];
	  add_new_point_with_xy_error_to_ds(ds, dsi->xd[cmp[i].idx], erx , dsi2->xd[cmp[i].idy], ery);
	}
    }
  
  free(cmp);
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"y(x) common points in x with y > %g",thres);
  if (op->x_title != NULL) set_plot_x_title(opn, "%s", op->x_title);
  if (op->y_title != NULL) set_plot_y_title(opn, "%s", op->y_title);
  opn->filename = Transfer_filename(op->filename);
  uns_op_2_op(opn, op);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
  
}




int do_adjustStrech_draw_Y_ds1_vs_ds2_compatible_points(void)
{
  int i, j;
  O_p *op = NULL, *opn = NULL;
  static int nds1 = 0, nds2 = 1;
  static float sca = 1;
  d_s *ds = NULL, *dsi = NULL, *dsi2 = NULL;
  pltreg *pr = NULL;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");

  nds1 = op->cur_dat;
  i = win_scanf("Drawing Y coord. of points comaptible between two datasets using error bars\n"
		"Define the index of first ds %4d\n"
		"Define the index of second ds %4d\n"
		"Define the scaling factor bewtween the error bars %4f\n"
		,&nds1,&nds2,&sca);
  
  if (i == WIN_CANCEL)	return OFF;

  if (nds1 < 0 || nds1 >= op->n_dat)
    return win_printf_OK("first ds %d out of [0,%d[",nds1,op->n_dat);
  if (nds2 < 0 || nds2 >= op->n_dat)
    return win_printf_OK("second ds %d out of [0,%d[",nds2,op->n_dat);  

  dsi = op->dat[nds1];
  dsi2 = op->dat[nds2];
  if (dsi->xe == NULL)
    return win_printf_OK("ds 1 has no error bars in x");
  if (dsi2->xe == NULL)
    return win_printf_OK("ds 2 has no error bars in x");  
  
  if ((opn = create_and_attach_one_plot(pr, 16, 16, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  ds = opn->dat[0];
  alloc_data_set_x_error(ds);
  alloc_data_set_y_error(ds);
  
  ds->nx = ds->ny = 0;
  float ymin, ymax, ymin2, ymax2;
  for (i = 0; i < dsi->nx; i++)
    {
      ymin = (dsi->yed != NULL) ? dsi->yd[i] - dsi->yed[i] : dsi->yd[i] - dsi->ye[i];
      ymax = dsi->yd[i] + dsi->ye[i];
      for (j = 0; j < dsi2->nx; j++)
	{
	  ymin2 = (dsi2->yed != NULL) ? dsi2->yd[j] - dsi2->yed[j] : dsi2->yd[j] - dsi2->ye[j];
	  ymax2 = dsi2->yd[j] + dsi2->ye[j];	  
	  if ((ymin2 >= ymin && ymin2 < ymax) || (ymax2 >= ymin && ymax2 < ymax)
	    || (ymin >= ymin2 && ymin < ymax2) || (ymax >= ymin2 && ymax < ymax2))
	    {
	      add_new_point_with_xy_error_to_ds(ds, dsi->yd[i], dsi->ye[i] , dsi2->yd[j], dsi2->ye[j]);	      
	    }
	}      
    }
  
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"y(x) common points");
  if (op->x_title != NULL) set_plot_x_title(opn, "%s", op->x_title);
  if (op->y_title != NULL) set_plot_y_title(opn, "%s", op->y_title);
  opn->filename = Transfer_filename(op->filename);
  uns_op_2_op(opn, op);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
  
}

d_s *substract_common_points_in_X_to_2_ds(d_s *dsi, d_s *dsi2)
{
  register int i, j;
  float dx, tmp, minx, maxx, minx2, maxx2;
  d_s *ds = NULL;

  if (dsi == NULL || dsi2 == NULL) return NULL;

  
  for (i = 1, dx = 0, minx = maxx = dsi->xd[0]; i < dsi->nx; i++)
    {
      tmp = fabs(dsi->xd[i] - dsi->xd[i-1]);
      dx = (i == 1 || tmp < dx) ? tmp : dx;
      minx = (dsi->xd[i] < minx) ? dsi->xd[i] : minx;
      maxx = (dsi->xd[i] > maxx) ? dsi->xd[i] : maxx;      
    }

  for (i = 1, minx2 = maxx2 = dsi2->xd[0]; i < dsi2->nx; i++)
    {
      tmp = fabs(dsi2->xd[i] - dsi2->xd[i-1]);
      dx = (i == 1 || tmp < dx) ? tmp : dx;
      minx2 = (dsi2->xd[i] < minx2) ? dsi2->xd[i] : minx2;
      maxx2 = (dsi2->xd[i] > maxx2) ? dsi2->xd[i] : maxx2;            
    }
  dx /= 2;
  if (dx == 0.0)
    return win_printf_ptr("One dataset has x coordinate identical");  

  ds = build_data_set(16,16);
  if (ds == NULL)    return win_printf_ptr("cannot create ds !");
  ds->nx = ds->ny = 0;
  float x1, x2;
  int found, last = 0;
  for (i = 0; i < dsi->nx; i++)
    {
      x1 = dsi->xd[i];
      if (x1 < minx2 || x1 > maxx2) continue;
      for (j = last - 5, found = 0; found == 0 && j < last+10; j++)
	{ // we try starting from the fast found point
	  if (j < 0) continue;
	  x2 = dsi2->xd[j];
	  if (x2 < minx || x2 > maxx) continue;	  
	  if (fabs(x1 - x2) < dx)
	    {
	      add_new_point_to_ds(ds, dsi->xd[i], dsi->yd[i] - dsi2->yd[j]);
	      found = 1;
	      last = j;
	    }
	}
      if (found == 0)
	{  // we do the full search
	  for (j = 0; found == 0 && j < dsi2->nx; j++)
	    {
	      x2 = dsi2->xd[j];
	      if (x2 < minx || x2 > maxx) continue;	  
	      if (fabs(x1 - x2) < dx)
		{
		  add_new_point_to_ds(ds, dsi->xd[i], dsi->yd[i] - dsi2->yd[j]);
		  found = 1;
		  last = j;
		}
	    }
	}
    }
  
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"y1(x) - y2(x) common points in < %g",dx);
  return ds;
}  

int do_substract_Y_of_ds1_vs_ds2_compatible_points(void)
{
  int i;
  O_p *op = NULL, *opn = NULL;
  int mode;
  static int nds1 = 0, nds2 = 1, insert = 0;
  d_s *ds = NULL, *dsi = NULL, *dsi2 = NULL;
  pltreg *pr = NULL;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");

  nds1 = op->cur_dat;
  i = win_scanf("Drawing Y coord. of points comaptible between two datasets using error bars\n"
		"Define the index of first ds %4d\n"
		"Define the index of second ds %4d\n"
		"%b insert after this plot\n"
		,&nds1,&nds2,&insert);
  
  if (i == WIN_CANCEL)	return OFF;

  if (nds1 < 0 || nds1 >= op->n_dat)
    return win_printf_OK("first ds %d out of [0,%d[",nds1,op->n_dat);
  if (nds2 < 0 || nds2 >= op->n_dat)
    return win_printf_OK("second ds %d out of [0,%d[",nds2,op->n_dat);  

  dsi = op->dat[nds1];
  dsi2 = op->dat[nds2];
  mode = (insert) ? INSERT_HERE : 0;
  ds = substract_common_points_in_X_to_2_ds(dsi, dsi2);
  if (ds == NULL)    win_printf_OK("No data generated");
  else
    {
      opn = create_and_attach_one_empty_plot(pr, mode);
      if (opn == NULL) return win_printf_OK("cannot create plot");
      add_one_plot_data(opn, IS_DATA_SET, (void*)ds);
      if (op->x_title != NULL) set_plot_x_title(opn, "%s", op->x_title);
      if (op->y_title != NULL) set_plot_y_title(opn, "%s", op->y_title);
      opn->filename = Transfer_filename(op->filename);
      uns_op_2_op(opn, op);
    }
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
  
}

int do_substract_Y_of_all_ds_minus_ds2_compatible_points(void)
{
  int i;
  O_p *op = NULL, *opn = NULL;
  int mode;
  static int nds1 = 0, nds2 = 1, insert = 0;
  d_s *ds = NULL, *dsi = NULL, *dsi2 = NULL;
  pltreg *pr = NULL;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");

  nds2 = op->cur_dat;
  i = win_scanf("Drawing Y coord. of points comaptible between two datasets using error bars\n"
		"Define the index of ds to substract %4d\n"
		"%b insert after this plot\n"
		,&nds2,&insert);
  
  if (i == WIN_CANCEL)	return OFF;

  if (nds2 < 0 || nds2 >= op->n_dat)
    return win_printf_OK("second ds %d out of [0,%d[",nds2,op->n_dat);  

  for (nds1 = 0; nds1 < op->n_dat; nds1++)
    {
      if (nds1 == nds2) continue;
      dsi = op->dat[nds1];
      dsi2 = op->dat[nds2];
      mode = (insert) ? INSERT_HERE : 0;
      ds = substract_common_points_in_X_to_2_ds(dsi, dsi2);
      if (ds == NULL)    win_printf_OK("No data generated");
      else
	{
	  if (opn == NULL)
	    {
	      opn = create_and_attach_one_empty_plot(pr, mode);
	      if (op->x_title != NULL) set_plot_x_title(opn, "%s", op->x_title);
	      if (op->y_title != NULL) set_plot_y_title(opn, "%s", op->y_title);
	      opn->filename = Transfer_filename(op->filename);
	      uns_op_2_op(opn, op);
	    }
	  if (opn == NULL) return win_printf_OK("cannot create plot");
	  add_one_plot_data(opn, IS_DATA_SET, (void*)ds);
	}
    }
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
  
}


int do_adjustStrech_rescale_plot(void)
{
  int i, j;
  O_p *op = NULL, *opn = NULL;
  int nf;
  static float factor = 1;
  d_s *ds = NULL, *dsi = NULL;
  pltreg *pr = NULL;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number and place it in a new plot");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  nf = dsi->nx;	/* this is the number of points in the data set */
  i = win_scanf("By what factor do you want to multiply y %f",&factor);
  if (i == WIN_CANCEL)	return OFF;

  if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  ds = opn->dat[0];

  for (j = 0; j < nf; j++)
  {
    ds->yd[j] = factor * dsi->yd[j];
    ds->xd[j] = dsi->xd[j];
  }
  
  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"y multiplied by %f",factor);
  set_plot_title(opn, "Multiply by %f",factor);
  if (op->x_title != NULL) set_plot_x_title(opn, "%s", op->x_title);
  if (op->y_title != NULL) set_plot_y_title(opn, "%s", op->y_title);
  opn->filename = Transfer_filename(op->filename);
  uns_op_2_op(opn, op);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}

int do_dy_dx_last_points_minus_first_all_ds(void)
{
  int  i, j, k;
  O_p *op = NULL, *opn = NULL;;
  int nf;
  float tmpx, tmpy;
  d_s *ds = NULL, *dsi = NULL, *dsb = NULL;
  pltreg *pr = NULL;
  float ran;
  int idum_int = 673489, nbstnx = 500;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
    return win_printf_OK("cannot find data");

  i = win_scanf("Boostrap mean howmany times %5d\n"
		"Idum %5d\n",&nbstnx,&idum_int);
  if (i == WIN_CANCEL) return 0;
  nf = op->n_dat;
  if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  ds = opn->dat[0];
  ds->nx = ds->ny = 0;

  for (j = 0; j < op->n_dat; j++)
  {
    dsi = op->dat[j];
    tmpx = op->dx*(dsi->xd[dsi->nx-1] - dsi->xd[0]);
    tmpy = op->dy*(dsi->yd[dsi->nx-1] - dsi->yd[0]);
    if (tmpx != 0)
      {
	add_new_point_to_ds(ds, j, tmpy/tmpx);
      }
  }

  if ((dsb = create_and_attach_one_ds(opn, nbstnx, nbstnx, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  float mean;
  for (i = 0; i < nbstnx; i++)
    {
      for (j = 0, mean = 0; j < ds->nx; j++)
	  {
	    ran = ran1(&idum_int);
	    k = (int)(ds->nx*ran);
	    k = (k < 0) ? 0 : k;
	    k = (k < ds->nx) ? k : ds->nx-1;
	    mean += ds->yd[k];
	  }
      mean = (ds->nx > 0) ? mean/ds->nx : mean;
      dsb->yd[i] = mean;
      dsb->xd[i] = i;
    }
  
  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"last point minus first one");
  if (op->x_title != NULL) set_plot_x_title(opn, "%s", op->x_title);
  if (op->y_title != NULL) set_plot_y_title(opn, "%s", op->y_title);
  opn->filename = Transfer_filename(op->filename);
  //uns_op_2_op(opn, op);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}



int do_slope_of_ds_all_ds(void)
{
  int  i, j, k;
  O_p *op = NULL, *opn = NULL;;
  int nf;
  d_s *ds = NULL, *dsi = NULL, *dsb = NULL;
  pltreg *pr = NULL;
  float ran;
  int idum_int = 673489, nbstnx = 500;
  double out_a = 0, out_b = 0;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
    return win_printf_OK("cannot find data");

  i = win_scanf("Boostrap mean howmany times %5d\n"
		"Idum %5d\n",&nbstnx,&idum_int);
  if (i == WIN_CANCEL) return 0;
  nf = op->n_dat;
  if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  ds = opn->dat[0];
  ds->nx = ds->ny = 0;

  for (j = 0; j < op->n_dat; j++)
  {
    dsi = op->dat[j];
    if (compute_least_square_fit_on_ds(dsi, 1, &out_a, &out_b) == 0)
      {
	add_new_point_to_ds(ds, j, (out_a*op->dy)/op->dx);
      }
  }
  if ((dsb = create_and_attach_one_ds(opn, nbstnx, nbstnx, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  float mean;
  for (i = 0; i < nbstnx; i++)
    {
      for (j = 0, mean = 0; j < ds->nx; j++)
	  {
	    ran = ran1(&idum_int);
	    k = (int)(ds->nx*ran);
	    k = (k < 0) ? 0 : k;
	    k = (k < ds->nx) ? k : ds->nx-1;
	    mean += ds->yd[k];
	  }
      mean = (ds->nx > 0) ? mean/ds->nx : mean;
      dsb->yd[i] = mean;
      dsb->xd[i] = i;
    }
  
  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"last point minus first one");
  if (op->x_title != NULL) set_plot_x_title(opn, "%s", op->x_title);
  if (op->y_title != NULL) set_plot_y_title(opn, "%s", op->y_title);
  opn->filename = Transfer_filename(op->filename);
  //uns_op_2_op(opn, op);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}




int do_compute_chi2_for_linear_fit_with_error_in_x_and_y(void)
{
  int i, j, nchi2 = 0;
  O_p *op = NULL;
  static int nf = 512, inb = 0, ert = 0; 
  static float a = 1, b = 0, dv = 0.5;
  d_s *ds = NULL, *dsi = NULL;
  pltreg *pr = NULL;
  float erx, ery, erxb, eryb, x, y;
  double tmpd = 0, chi2 = 0;
  char c[256] = {0};
  float minx, maxx;
  float at = a, bt = b, ab = a, bb = b, amax, amin, bmin, bmax;
  double chi2m = 0;
  


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number and place it in a new plot");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");


  if (dsi->xe == NULL)
    return win_printf_OK("ds 1 has no error bars in x");

  if (dsi->ye == NULL)
    return win_printf_OK("ds 1 has no error bars in y");  


  i = win_scanf("Y = a.X + b\nPropose values for: a %5f b %5f \n"
		"Search minimun in (%R->a)  (%r->b)\n"
		"Relative variation %5f\n"
		"Fit with errors in [%R->x and y] [%r->x] [%r->y]\n"
		"Specify the number of points for line drawing %4d\n"
		,&a,&b,&inb,&dv,&ert,&nf);
  if (i == WIN_CANCEL)	return OFF;


  for (j = 0; j < nf; j++)
    {
      at = (inb == 0) ? a + (dv * a * 2 *(j -(nf/2)))/nf : a;
      bt = (inb == 1) ? b + (dv * b * 2 *(j -(nf/2)))/nf : b;
      if (j == 0)
	{
	  amin = at;
	  bmin = bt;
	}
      if (j == nf - 1)
	{
	  amax = at;
	  bmax = bt;
	}
      for (i = 0, chi2 = 0, nchi2 = 0; i < dsi->nx; i++)
	{
	  erxb = erx = fabs(dsi->xe[i]);
	  if (dsi->xed) erxb = fabs(dsi->xed[i]);
	  eryb = ery = fabs(dsi->ye[i]);
	  if (dsi->yed) eryb = fabs(dsi->yed[i]);
	  if (ert == 0 || ert == 2)
	    {
	      y = at * dsi->xd[i] + bt;
	      y -= dsi->yd[i];
	      tmpd = (y > 0) ? ery * ery : eryb * eryb;
	      tmpd = (tmpd == 0) ? 1 : tmpd;
	      chi2+= (y * y)/tmpd;
	      nchi2++;
	    }
	  if (ert == 0 || ert == 1)
	    {	  
	      x = dsi->yd[i] - bt;
	      x = (at != 0) ? x/at : x;
	      x -= dsi->xd[i];
	      tmpd = (x > 0) ? erx * erx : erxb * erxb;
	      tmpd = (tmpd == 0) ? 1 : tmpd;
	      chi2+= (x * x)/tmpd;
	      nchi2++;
	    }
	}
      if(j == 0 || chi2 < chi2m)
	{
	  chi2m = chi2;
	  ab = at;
	  bb = bt;
	}
    }
  a = ab;
  b = bb;
  chi2 = chi2m;
  i = win_printf("Best a = %g in [%g, %g]\n b = %g in [%g, %g]=>\n"
		 " \\chi^2 = %g n = %d \nTo draw fit press Ok\n"
		 ,ab,amin,amax,bb,bmin,bmax,chi2m,nchi2);
  if (i == WIN_OK)
    {
      if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
	return win_printf_OK("cannot create plot !");
      for (i = 0, minx = maxx = 0; i < dsi->nx; i++)
	{
	  erxb = erx = fabs(dsi->xe[i]);
	  if (dsi->xed) erxb = fabs(dsi->xed[i]);
	  if (i == 0 || (minx > dsi->xd[i] - erxb))
	    minx = dsi->xd[i] - erxb;
	  if (i == 0 || (maxx < dsi->xd[i] + erx))
	    maxx = dsi->xd[i] + erx;		
	}
      tmpd = (maxx - minx)/10;
      minx -= tmpd;
      maxx += tmpd;
      tmpd = maxx - minx;      
      for (j = 0; j < nf; j++)
	{
	  x = minx + j * tmpd/nf;
	  y = a * x + b;
	  ds->xd[j] = x;
	  ds->yd[j] = y;
	}
      snprintf(c, sizeof(c), "\\fbox{\\stack{{\\sl y = a.x + b}{a = %g, b = %g}"
	       "{\\chi^2 = %g, n = %d}}}", a,b,chi2,nchi2);
      set_ds_plot_label(ds, (maxx + minx) / 2, a * (maxx + minx) / 2 + b, USR_COORD, "%s",c);
    }

  /* now we must do some house keeping */
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}




MENU *adjustStrech_plot_menu(void)
{
  static MENU mn[32] = {0};

  if (mn[0].text != NULL)	return mn;
  //add_item_to_menu(mn,"Hello example", do_adjustStrech_hello,NULL,0,NULL);
  add_item_to_menu(mn,"data set rescale in Y", do_adjustStrech_rescale_data_set,NULL,0,NULL);
  add_item_to_menu(mn,"Draw X of 2 ds common points", do_adjustStrech_draw_X_ds1_vs_ds2_compatible_points,NULL,0,NULL);
  add_item_to_menu(mn,"Draw Y of 2 ds common points", do_adjustStrech_draw_Y_ds1_vs_ds2_compatible_points,NULL,0,NULL);
  add_item_to_menu(mn,"Substract Y of common points in X of 2 ds",do_substract_Y_of_ds1_vs_ds2_compatible_points ,NULL,0,NULL);

  add_item_to_menu(mn,"Substract Y of common points in X of all ds",do_substract_Y_of_all_ds_minus_ds2_compatible_points,NULL,0,NULL);
  add_item_to_menu(mn,"Copy ds containing expected oligo blocking position",ds_peak_nts_copy,NULL,0,NULL);

  add_item_to_menu(mn,"Slope from last - first point",  do_dy_dx_last_points_minus_first_all_ds,NULL,0,NULL);
  add_item_to_menu(mn,"Slope from least square fit",   do_slope_of_ds_all_ds,NULL,0,NULL);  

  add_item_to_menu(mn,"compute chi2 linear fit_error(x,y)",  do_compute_chi2_for_linear_fit_with_error_in_x_and_y,NULL,0,NULL);


  //add_item_to_menu(mn,"plot rescale in Y", do_adjustStrech_rescale_plot,NULL,0,NULL);
  return mn;
}


#define SEPCHARS     ";,"



int do_adjustStrech_im_manual_find_peaks(void)
{
  imreg *imr = NULL;
  O_i *ois = NULL;
  char *tok = NULL, *treat = NULL;
  int i = 0, j, k, l, ii;
  static d_s **oli_peaks = NULL, *dzf = NULL;
  static char **oli = NULL, stoli[32] = {0};
  static O_i *oih = NULL, *oid = NULL;
  static int noli = 0, moli = 0, ref = -1, peak_nb = 4, seq_pos_s = 32, seq_pos_e,  kmax = 0, *inoli = NULL, previous = 0, kmin = 0;
  static O_p *op = NULL;
  static int onx = 0, ony = 0;
  float min, tmp;
  
  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number and place it in a new plot");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%imr%oi",&imr,&ois) != 2)
    return win_printf_OK("cannot find data");

  if (oih == NULL)
    {
      if (ois->im.treatement == NULL || strlen(ois->im.treatement) < 1)
	return win_printf_OK("No treatement data !");
      
      treat = strdup(ois->im.treatement);
      moli = 16;
      oli = (char **)calloc(moli,sizeof(char*));
      if (oli == NULL) return win_printf_OK("cannot create oli !");
      for (tok = strtok(treat, SEPCHARS), noli = 0 ; tok;
	   tok = strtok(NULL, SEPCHARS), noli++)
	{
	  if (noli >= moli)
	    {
	      moli *= 2;
	      oli = (char **)realloc(oli,moli*sizeof(char*));
	      if (oli == NULL) return win_printf_OK("cannot create oli !");	  
	    }
	  oli[noli] = strdup(tok);
	  if (strstr(oli[noli],"ref") != NULL) ref = noli;
	}
      if (noli != ois->im.ny)
	  return win_printf_OK("Eroor Found %d oli ref at %d\n0->%s 1->%s 2->%s 3->%s ..."
			       ,noli,ref,oli[0],oli[1],oli[2],oli[3]);
      else oih = ois;
      ony = ois->im.nx;
      dzf = build_data_set(ony,ony);
      if (dzf == NULL) return win_printf_OK("Cannot create tmp dzf");
      for(i = 0; i < ony; i++) dzf->xd[i] = i;
      oli_peaks = (d_s**)calloc(ois->im.ny,sizeof(d_s*));
      if (oli_peaks == NULL) return win_printf_OK("Cannot create olipeaks");
      for(ii = 0; ii < ois->im.ny; ii++)
	{
	  extract_raw_line(ois, ii, dzf->yd);
	  oli_peaks[ii] = find_max_peaks(dzf, 0.1);
	  if (oli_peaks[ii] == NULL || oli_peaks[ii]->nx <= 0)
	    return warning_message("null dsp");
	}
      
    }
  if (oid == NULL)
    {
      onx = 64;
      i = win_scanf("Define the number of bases %5d\n"
		    "The starting olig %6s\n"
		    "The oligo position in sequence %4d\n"
		    "The starting peak postion %5d\n"
		    ,&onx,stoli,&seq_pos_s,&peak_nb);
      if (i == WIN_CANCEL) return 0;
      oid = create_and_attach_oi_to_imr(imr, onx, ony, IS_FLOAT_IMAGE);
      if (oid == NULL) return win_printf_OK("Cannot create dest image");
      op = create_and_attach_op_to_oi(oid, onx, onx, 0, IM_SAME_X_AXIS | IM_SAME_Y_AXIS);
      if (op == NULL) return win_printf_OK("Cannot create image plot");
      inoli = (int*)calloc(onx,sizeof(int));
      if (inoli == NULL) return win_printf_OK("Cannot create inoli");      
      for (i = 0; i < onx; i++)	  op->dat[0]->xd[i] = i;

      for (i = 0; i < noli; i++)
	{
	  if (oli[i][0] == stoli[0] && oli[i][1] == stoli[1] && oli[i][2] == stoli[2])
	    {
	      extract_raw_line(ois, i, dzf->yd);
	      op->dat[0]->yd[seq_pos_s] = oli_peaks[i]->xd[peak_nb];
	      for(ii = 0; ii < ony; ii++)
		oid->im.pixel[ii].fl[seq_pos_s] = dzf->yd[ii];
	      create_and_attach_label_to_oi(oid, 0.5+seq_pos_s, ony-20, VERT_LABEL_USR
					    , "\\pt7 %s", oli[i]);  	      	      		  
	      inoli[seq_pos_s] = i;
	      break;
	    }
	}
      seq_pos_e = seq_pos_s + 1;
      k = seq_pos_s + 1;
      for (j = 0, l = 0; j < noli; j++)
	{
	  if (j == ref || j == i) continue;
	  if (oli[j][0] == oli[i][1] && oli[j][1] == oli[i][2])
	    {
	      if (l != 0)
		{
		  extract_raw_line(ois, ref, dzf->yd);
		  for(ii = 0; ii < ony; ii++)
		    oid->im.pixel[ii].fl[k] = dzf->yd[ii];		  
		  k = (k < onx) ? k+1 : onx-1;
		  extract_raw_line(ois, i, dzf->yd);
		  for(ii = 0; ii < ony; ii++)
		    oid->im.pixel[ii].fl[k] = dzf->yd[ii];
		  create_and_attach_label_to_oi(oid, 0.5+k, ony-20, VERT_LABEL_USR
					    , "\\pt7 %s", oli[i]);
		  k = (k < onx) ? k+1 : onx-1;
		}
	      l++;
	      extract_raw_line(ois, j, dzf->yd);
	      for(ii = 0; ii < ony; ii++)
		oid->im.pixel[ii].fl[k] = dzf->yd[ii];
	      create_and_attach_label_to_oi(oid, 0.5+k, ony-20, VERT_LABEL_USR
					    , "\\pt7\\color{lightblue} %s", oli[j]);  	      	      
	      k = (k < onx) ? k+1 : onx-1;
	    }
	}
      kmax = k;
      k = seq_pos_s - 1;
      for (j = 0, l = 0; j < noli; j++)
	{
	  if (j == ref || j == i) continue;
	  if (oli[j][1] == oli[i][0] && oli[j][2] == oli[i][1])
	    {
	      if (l != 0)
		{
		  extract_raw_line(ois, ref, dzf->yd);
		  for(ii = 0; ii < ony; ii++)
		    oid->im.pixel[ii].fl[k] = dzf->yd[ii];		  
		  k = (k > 0) ? k-1 : 0;
		  extract_raw_line(ois, i, dzf->yd);
		  for(ii = 0; ii < ony; ii++)
		    oid->im.pixel[ii].fl[k] = dzf->yd[ii];
		  create_and_attach_label_to_oi(oid, 0.5+k, ony-20, VERT_LABEL_USR
					    , "\\pt7 %s", oli[i]);
		  k = (k > 0) ? k-1 : 0;
		}
	      l++;
	      extract_raw_line(ois, j, dzf->yd);
	      for(ii = 0; ii < ony; ii++)
		oid->im.pixel[ii].fl[k] = dzf->yd[ii];
	      create_and_attach_label_to_oi(oid, 0.5+k, ony-20, VERT_LABEL_USR
					    , "\\pt7\\color{lightblue} %s", oli[j]);  	      	      
	      k = (k > 0) ? k-1 : 0;
	    }
	}
      kmin = k;
      find_zmin_zmax(oid);
      return (refresh_image(imr, imr->n_oi - 1));
    }


  i = win_scanf("Define the %R next or %r previous oligo %6s\n"
		,&previous,stoli);
  if (i == WIN_CANCEL) return 0;  

  if (previous == 0)
    {
      for (j = seq_pos_e; j < kmax; j++)
	{ // we erase draft stuff
	  for(ii = 0; ii < ony; ii++)
	    oid->im.pixel[ii].fl[j] = 0;
	}
      for (ii = oid->n_lab-1; ii >= 0; ii--)
	{
	  if (oid->lab[ii]->xla > (float)seq_pos_e)
	    remove_from_one_image(oid, IS_PLOT_LABEL, (void*)oid->lab[ii]);	     
	}
      for (i = 0; i < noli; i++)
	{
	  if (oli[i][0] == stoli[0] && oli[i][1] == stoli[1] && oli[i][2] == stoli[2])
	    {
	      extract_raw_line(oih, i, dzf->yd);
	      for(ii = 0; ii < ony; ii++)
		oid->im.pixel[ii].fl[seq_pos_e] = dzf->yd[ii];
	      create_and_attach_label_to_oi(oid, 0.5+seq_pos_e, ony-20, VERT_LABEL_USR
					    , "\\pt7 %s", oli[i]);  	      	      		  	      
	      for (ii = 0, l = 0, min = 0; ii < oli_peaks[i]->nx; ii++)
		{
		  tmp = fabs(oli_peaks[i]->xd[ii] - op->dat[0]->yd[seq_pos_e-1]);
		  if (ii == 0 || tmp < min)
		    {
		      min = tmp;
		      l = ii;
		    }
		}
	      inoli[seq_pos_e] = i;
	      op->dat[0]->yd[seq_pos_e++] = oli_peaks[i]->xd[l];	      
	      break;
	    }
	}
      k = seq_pos_e;
      for (j = 0, l = 0; j < noli; j++)
	{
	  if (j == ref || j == i) continue;
	  if (oli[j][0] == oli[inoli[seq_pos_e-1]][1] && oli[j][1] == oli[inoli[seq_pos_e-1]][2])
	    {
	      if (l != 0)
		{
		  extract_raw_line(oih, ref, dzf->yd);
		  for(ii = 0; ii < ony; ii++)
		    oid->im.pixel[ii].fl[k] = dzf->yd[ii];		  
		  k = (k < onx) ? k+1 : onx-1;
		  extract_raw_line(oih, inoli[seq_pos_e-1], dzf->yd);
		  for(ii = 0; ii < ony; ii++)
		    oid->im.pixel[ii].fl[k] = dzf->yd[ii];
		  create_and_attach_label_to_oi(oid, 0.5+k, ony-20, VERT_LABEL_USR
					    , "\\pt7 %s", oli[inoli[seq_pos_e-1]]);
		  k = (k < onx) ? k+1 : onx-1;
		}
	      l++;
	      extract_raw_line(oih, j, dzf->yd);
	      for(ii = 0; ii < ony; ii++)
		oid->im.pixel[ii].fl[k] = dzf->yd[ii];
	      create_and_attach_label_to_oi(oid, 0.5+k, ony-20, VERT_LABEL_USR
					    , "\\pt7\\color{lightblue} %s", oli[j]);  	      	      
	      k = (k < onx) ? k+1 : onx-1;
	    }
	}
      kmax = k;
    }
  find_zmin_zmax(oid);
  return (refresh_image(imr, imr->n_oi - 1));  
}
int do_adjustStrech_im_find_peaks(void)
{
  imreg *imr = NULL;
  O_i *ois = NULL, *oid = NULL;
  char *tok = NULL, **oli = NULL, message[512] = {0}, *treat = NULL;
  int i = 0, j, k, l, ii, noli = 0, moli, ref = -1, onx, ony;
  float *zf = NULL;
  
  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number and place it in a new plot");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%imr%oi",&imr,&ois) != 2)
    return win_printf_OK("cannot find data");

  if (ois->im.treatement == NULL || strlen(ois->im.treatement) < 1)
    return win_printf_OK("No treatement data !");

  treat = strdup(ois->im.treatement);
  moli = 16;
  oli = (char **)calloc(moli,sizeof(char*));
  if (oli == NULL) return win_printf_OK("cannot create oli !");
  for (tok = strtok(treat, SEPCHARS), noli = 0 ; tok;
       tok = strtok(NULL, SEPCHARS), noli++)
    {
      if (noli >= moli)
	{
	  moli *= 2;
	  oli = (char **)realloc(oli,moli*sizeof(char*));
	  if (oli == NULL) return win_printf_OK("cannot create oli !");	  
	}
      oli[noli] = strdup(tok);
      if (strstr(oli[noli],"ref") != NULL) ref = noli;
    }
  win_printf("Found %d oli ref at %d\n0->%s 1->%s 2->%s 3->%s ..."
	     ,noli,ref,oli[0],oli[1],oli[2],oli[3]);

  message[0] = 0;
  for(i = 0, k = 0; i < noli; i++)
    {
      if (i == ref)
	{
	  k += 2;
	  continue;
	}
      snprintf(message+strlen(message),sizeof(message)-strlen(message)
	       ,"%sFor oligo %s:",(k)?"\n":"",oli[i]);
      for (j = 0, l = 0; j < noli; j++)
	{
	  if (j == ref || j == i) continue;
	  if (oli[j][1] == oli[i][0] && oli[j][2] == oli[i][1])
	    {
	      if (l == 0)
		{
		  snprintf(message+strlen(message),sizeof(message)-strlen(message)
			   ,"\nprevious matching oligo %s:",oli[j]);
		  k++;
		}
	      else
		snprintf(message+strlen(message),sizeof(message)-strlen(message)
			 ,", %s:",oli[j]);		
	      l++;
	      k++;
	      k++;	      
	    }
	}
      for (j = 0, l = 0; j < noli; j++)
	{
	  if (j == ref || j == i) continue;
	  if (oli[j][0] == oli[i][1] && oli[j][1] == oli[i][2])
	    {
	      if (l == 0)
		{
		  snprintf(message+strlen(message),sizeof(message)-strlen(message)
			   ,"\nnext matching oligo %s:",oli[j]);
		  k++;
		}
	      else
		snprintf(message+strlen(message),sizeof(message)-strlen(message)
			 ,", %s:",oli[j]);		
	      l++;
	      k++;
	      k++;	      	      
	    }
	}
      //if (sizeof(message)-strlen(message) < 128) break;
    }
  win_printf("k = %d\n%s",k,message);
  onx = k;
  ony = ois->im.nx;

  oid = create_and_attach_oi_to_imr(imr, onx, ony, IS_FLOAT_IMAGE);
  if (oid == NULL) return win_printf_OK("Cannot create dest image");
  zf = (float*)calloc(ony,sizeof(float));
  if (zf == NULL) return win_printf_OK("Cannot create dest image");  
  extract_raw_line(ois, ref, zf);
  for(ii = 0; ii < ony; ii++)
    oid->im.pixel[ii].fl[0] = oid->im.pixel[ii].fl[onx-1] = zf[ii];
  for(i = 0, k = 1; i < noli; i++)
    {
      if (i == ref) continue;
      for (j = 0, l = 0; j < noli; j++)
	{
	  if (j == ref || j == i) continue;
	  if (oli[j][1] == oli[i][0] && oli[j][2] == oli[i][1])
	    {
	      if (l == 0)
		{
		  extract_raw_line(ois, ref, zf);
		  for(ii = 0; ii < ony; ii++)
		    oid->im.pixel[ii].fl[k] = zf[ii];		  
		  k = (k < onx) ? k+1 : onx-1;
		}
	      extract_raw_line(ois, j, zf);
	      for(ii = 0; ii < ony; ii++)
		oid->im.pixel[ii].fl[k] = zf[ii];
	      create_and_attach_label_to_oi(oid, 0.5+k, ony-20, VERT_LABEL_USR
					    , "\\pt7\\color{lightgreen} %s", oli[j]);  	      
	      l++;
	      k = (k < onx) ? k+1 : onx-1;
	      extract_raw_line(ois, i, zf);
	      for(ii = 0; ii < ony; ii++)
		oid->im.pixel[ii].fl[k] = zf[ii];
	      create_and_attach_label_to_oi(oid, 0.5+k, ony-20, VERT_LABEL_USR
					    , "\\pt7 %s", oli[i]);  	      	      
	      k = (k < onx) ? k+1 : onx-1;
	    }
	}
      for (j = 0, l = 0; j < noli; j++)
	{
	  if (j == ref || j == i) continue;
	  if (oli[j][0] == oli[i][1] && oli[j][1] == oli[i][2])
	    {
	      if (l == 0)
		{
		  extract_raw_line(ois, ref, zf);
		  for(ii = 0; ii < ony; ii++)
		    oid->im.pixel[ii].fl[k] = zf[ii];		  
		  k = (k < onx) ? k+1 : onx-1;
		}
	      extract_raw_line(ois, i, zf);
	      for(ii = 0; ii < ony; ii++)
		oid->im.pixel[ii].fl[k] = zf[ii];
	      create_and_attach_label_to_oi(oid, 0.5+k, ony-20, VERT_LABEL_USR
					    , "\\pt7 %s", oli[i]);  	      	      
	      l++;
	      k = (k < onx) ? k+1 : onx-1;
	      extract_raw_line(ois, j, zf);
	      for(ii = 0; ii < ony; ii++)
		oid->im.pixel[ii].fl[k] = zf[ii];
	      create_and_attach_label_to_oi(oid, 0.5+k, ony-20, VERT_LABEL_USR
					    , "\\pt7\\color{lightblue} %s", oli[j]);  	      	      
	      k = (k < onx) ? k+1 : onx-1;
	    }
	}
    }

  inherit_from_im_to_im(oid, ois);
  uns_oi_2_oi(oid, ois);

  
  for (i = 0; i < noli; i++)
    if (oli[i]) free(oli[i]);
  free(oli);
  free(treat);
  free(zf);
  find_zmin_zmax(oid);
  return (refresh_image(imr, imr->n_oi - 1));
}



MENU *adjustStrech_image_menu(void)
{
  static MENU mn[32] = {0};

  if (mn[0].text != NULL)	return mn;
  //add_item_to_menu(mn,"Hello example", do_adjustStrech_hello,NULL,0,NULL);
  add_item_to_menu(mn,"find matching peaks", do_adjustStrech_im_find_peaks,NULL,0,NULL);
  add_item_to_menu(mn,"manual matching peaks", do_adjustStrech_im_manual_find_peaks,NULL,0,NULL);  

  return mn;
}



int	adjustStrech_main(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  add_image_treat_menu_item("adjustStrech", NULL, adjustStrech_image_menu(), 0, NULL);
  
  add_plot_treat_menu_item ( "adjustStrech", NULL, adjustStrech_plot_menu(), 0, NULL);
  return D_O_K;
}

int	adjustStrech_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;

  remove_item_to_menu(image_treat_menu, "adjustStrech", NULL, NULL);  
  remove_item_to_menu(plot_treat_menu, "adjustStrech", NULL, NULL);
  return D_O_K;
}
#endif

