#pragma once
PXV_FUNC(int, do_adjustStrech_rescale_plot, (void));
PXV_FUNC(MENU*, adjustStrech_plot_menu, (void));
PXV_FUNC(int, do_adjustStrech_rescale_data_set, (void));
PXV_FUNC(int, adjustStrech_main, (int argc, char **argv));
