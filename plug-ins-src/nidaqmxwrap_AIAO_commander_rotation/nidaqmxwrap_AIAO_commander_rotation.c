/*    Wrapper Plug-in for Continuous Synchronous Analog and Digital input in Xvin winth NIDAQMX.
 *
 *    F. Mosconi
  */
#ifndef _NIDAQMXWRAP_AIAO_COMMANDER_ROTATION_C_
#define _NIDAQMXWRAP_AIAO_COMMANDER_ROTATION_C_

#include <allegro.h>
#include <winalleg.h>
#include <xvin.h>

/* If you include other plug-ins header do it here*/ 
#include <NIDAQmx.h>
#include "../logfile/logfile.h"
#include "../nidaqmxwrap_AIAO/nidaqmxwrap_AIAO.h"


/* But not below this define */
# define BUILDING_PLUGINS_DLL
#include "nidaqmxwrap_AIAO_commander_rotation.h"

extern new_rfp *grf;

////////////////////////////////////////////////////////////////////////////////////////////////////////
/** An example of AO_data function
    Arbitrary function.

\param float x The independent variable
\param void *p This is the duty cycle. It is cast on a float and it indicates the ratio of 'up' to 'up+down'.
\return float64 the function value

\author Francesco Mosconi
\version 21/03/07
*/    


int waveform__testarbitrary_function_menu(void)
{
    int j;
    float *dc =  NULL;

    dc = (float*)  calloc(1,sizeof(float));
    *dc =0.5;
    
    if(updating_menu_state != 0) return D_O_K;

    for (j=0 ; j<NUMBER_OF_AI_LINES ; j++)
    {
        lio->lines[j].amplitude = 2.5;
        lio->lines[j].offset = 0.;
        lio->lines[j].phase = (float) j/NUMBER_OF_AI_LINES;  

        lio->lines[j].function = waveform__testarbitrary_function;
        lio->lines[j].params = dc;
	
    }

    return noChangeInLines = 0;

}

////////////////////////////////////////////////////////////////////////////////////////////////////////
/** An example of AO_data function
    Square function.

\param float x The independent variable
\param void *p This is the duty cycle. It is cast on a float and it indicates the ratio of 'up' to 'up+down'.
\return float64 the function value

\author Francesco Mosconi
\version 21/03/07
*/    


int waveform__square_function_menu(void)
{
    int j;
    float *dc =  NULL;

    dc = (float*)  calloc(1,sizeof(float));
    *dc =0.5;
    
    if(updating_menu_state != 0) return D_O_K;

    for (j=0 ; j<NUMBER_OF_AI_LINES ; j++)
    {
        lio->lines[j].amplitude = 2.5;
        lio->lines[j].offset = 0.;
        lio->lines[j].phase = (float) j/NUMBER_OF_AI_LINES;  

        lio->lines[j].function = waveform__square_function;
        lio->lines[j].params = dc;
	
    }

    return noChangeInLines = 0;

}


////////////////////////////////////////////////////////////////////////////////////////////////////////
/** An example of AO_data function
    Triangle function.

\param float x The independent variable
\param void *p This is the duty cycle. It is cast on a float and it indicates the ratio of 'upward' to the period length.
\return float64 the function value

\author Francesco Mosconi
\version 21/03/07
*/    


int waveform__triangle_function_menu(void)
{
    int j;
    float *dc =  NULL;

    dc = (float*)  calloc(1,sizeof(float));
    *dc =0.5;

    if(updating_menu_state != 0) return D_O_K;
  
    for (j=0 ; j<NUMBER_OF_AI_LINES ; j++)
    {
        lio->lines[j].amplitude = 2.5;
        lio->lines[j].offset = 0.;
        lio->lines[j].phase = (float) j/NUMBER_OF_AI_LINES;  
        
        lio->lines[j].function = waveform__triangle_function;
        lio->lines[j].params = dc;
    }

    return noChangeInLines = 0;

}

////////////////////////////////////////////////////////////////////////////////////////////////////////
/** An example of AO_data function
    Sine function.

\param float x The independent variable
\param void *p This is NULL;
\return float64 sin(x)

\author Francesco Mosconi
\version 21/03/07
*/    


int waveform__sin_function_menu(void)
{
    int j;
    
    if(updating_menu_state != 0) return D_O_K;
  
    for (j=0 ; j<NUMBER_OF_AI_LINES ; j++)
    {
        lio->lines[j].amplitude = 2.5;
        lio->lines[j].offset = 0.;
        lio->lines[j].phase = (float) j/NUMBER_OF_AI_LINES;  
        
        lio->lines[j].function = waveform__sin_function;
        lio->lines[j].params = (void*) NULL;
    }

    return noChangeInLines = 0;

}

////////////////////////////////////////////////////////////////////////////////////////////////////////
/** An example of AO_data function
    Cosine function
    
\param float x The independent variable
\param void *p This is NULL;
\return float64 cos(x)

\author Francesco Mosconi
\version 21/03/07
*/    



int waveform__cos_function_menu(void)
{
    int j;
    
    if(updating_menu_state != 0) return D_O_K;
  
    for (j=0 ; j<NUMBER_OF_AI_LINES ; j++)
    {
        lio->lines[j].amplitude = 2.5;
        lio->lines[j].offset = 0.;
        lio->lines[j].phase = (float) j/NUMBER_OF_AI_LINES;  
        
        lio->lines[j].function = waveform__cos_function;
        lio->lines[j].params = (void*) NULL;
    }

    return noChangeInLines = 0;

}

////////////////////////////////////////////////////////////////////////////////////////////////////////
/** An example of AO_data function
    Constant function
    
\param float x The independent variable
\param void *p This is NULL;
\return float64 1

\author Francesco Mosconi
\version 21/03/07
*/    



int waveform__const_function_menu(void)
{
    int j;
    
    if(updating_menu_state != 0) return D_O_K;
  
    for (j=0 ; j<NUMBER_OF_AI_LINES ; j++)
    {
        lio->lines[j].amplitude = 2.5;
        lio->lines[j].offset = 0.;
        lio->lines[j].phase = (float) j/NUMBER_OF_AI_LINES;  
        
        lio->lines[j].function = waveform__const_function;
        lio->lines[j].params = (void*) NULL;
    }

    return noChangeInLines = 0;

}

int lio__set_menu(void)
{
  if(updating_menu_state != 0) return D_O_K;

  if(lio == NULL)
    lines__create_data_from_lines();

  win_scanf("frequency %6f\n"
	    "num_points %6d\n"
	    "line0:\n"
	    "amplitude = %6f\n"
	    "offset = %6f\n"
	    "phase = %6f\n"
	    "line1:\n"
	    "amplitude = %6f\n"
	    "offset = %6f\n"
	    "phase = %6f\n"
	    "line2:\n"
	    "amplitude = %6f\n"
	    "offset = %6f\n"
	    "phase = %6f\n"
	    "line3:\n"
	    "amplitude = %6f\n"
	    "offset = %6f\n"
	    "phase = %6f\n",
	    &(lio->frequency),
	    &(lio->num_points),
	    &(lio->lines[0].amplitude),
	    &(lio->lines[0].offset),
	    &(lio->lines[0].phase),
	    &(lio->lines[1].amplitude),
	    &(lio->lines[1].offset),
	    &(lio->lines[1].phase),
	    &(lio->lines[2].amplitude),
	    &(lio->lines[2].offset),
	    &(lio->lines[2].phase),
	    &(lio->lines[3].amplitude),
	    &(lio->lines[3].offset),
	    &(lio->lines[3].phase));       
 
  return noChangeInLines = 0;
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Menu functions

/** A menu containing a clickable item and a static submenu */
MENU *nidaqmxwrap_AIAO_commander_rotation_waveform_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	
    add_item_to_menu(mn,"Start",worker__start,NULL,0,NULL);
    add_item_to_menu(mn,"Stop",worker__stop,NULL,0,NULL);
    add_item_to_menu(mn,"Square",waveform__square_function_menu,NULL,0,NULL);
    add_item_to_menu(mn,"Triangle",waveform__triangle_function_menu,NULL,0,NULL);
    add_item_to_menu(mn,"Sine",waveform__sin_function_menu,NULL,0,NULL);
    add_item_to_menu(mn,"Cosine",waveform__cos_function_menu,NULL,0,NULL);
    add_item_to_menu(mn,"Constant",waveform__const_function_menu,NULL,0,NULL);
    add_item_to_menu(mn,"Arbitrary",waveform__testarbitrary_function_menu,NULL,0,NULL);
    add_item_to_menu(mn,"Set params",lio__set_menu,NULL,0,NULL);
    add_item_to_menu(mn,"B",rotation__rotating_B_all_interface,NULL,0,NULL);

    return mn;
}
    
int nidaqmxwrap_AIAO_commander_rotation_main(void)
{   
  if(updating_menu_state != 0) return D_O_K;
  
  rotation_rfp_functions_main();
  add_plot_treat_menu_item("Nidaq AIAO commander Menu",NULL,nidaqmxwrap_AIAO_commander_rotation_waveform_menu(),0,NULL);
  add_image_treat_menu_item("Nidaq AIAO commander Menu",NULL,nidaqmxwrap_AIAO_commander_rotation_waveform_menu(),0,NULL);
  
  return D_O_K;    
}

#endif

