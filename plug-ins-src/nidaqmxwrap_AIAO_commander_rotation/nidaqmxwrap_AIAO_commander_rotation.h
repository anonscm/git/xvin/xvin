#ifndef _NIDAQMXWRAP_AIAO_COMMANDER_ROTATION_H_
#define _NIDAQMXWRAP_AIAO_COMMANDER_ROTATION_H_

PXV_FUNC(int, rotation_rfp_functions_main,(void));
PXV_FUNC(int, rotation__rotating_B_all_interface,(void));
PXV_FUNC(float64, waveform__rf_function,(float y,void* p));

typedef struct new_rotating_field_parameters
{
  int num_turns; //number of periods the whole sequence must be repeated
  int meanvalue; //mean value for oscillating amplitude and fixed direction field
  float frequency; //frequency (in turns per second)
  int num_points; //spatial frequency (points per turn)
  float level; //field amplitude
  int phasenum; //phase numerator
  int phaseden; //phase denominator
  int stepflag; //flag for stepped motion
  int epsilon; //epsilon parameter for doing a bit more than a turn
  int num_pos_turns; //number of turns in each direction
} new_rfp;


#endif

