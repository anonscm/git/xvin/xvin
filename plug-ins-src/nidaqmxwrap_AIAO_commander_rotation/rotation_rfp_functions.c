/** \file AOTF_functions.c
    \brief Sub-plug-in program for AOTF control with NIDAQ-M 6229 in Xvin.
    
    Controls function generator for different AOTF control situations.
    BEWARE : requests the NIDAQ plugin !
    
    \sa NIDAQ.c
    \author Adrien Meglio
    \version 01/11/2006
*/
#ifndef ROTATION_RFP_FUNCTIONS_C_
#define ROTATION_RFP_FUNCTIONS_C_

#include <allegro.h>
#include <winalleg.h>
#include <xvin.h>

/* If you include other plug-ins header do it here*/ 
#include <NIDAQmx.h>
#include "../logfile/logfile.h"
#include "../nidaqmxwrap_AIAO/nidaqmxwrap_AIAO.h"
#include "nidaqmxwrap_AIAO_commander_rotation.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL


new_rfp *grf = NULL;

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
//new_rfp structure
int init_new_rfp(new_rfp *rf)
{
  rf->num_turns = 1;
  rf->meanvalue = 0;
  rf->frequency = 0.1;
  rf->num_points = 250;
  rf->level = 0.1;
  rf->phasenum = 0;
  rf->phaseden = 1;
  rf->stepflag = 0;
  rf->epsilon = 0.;
  rf->num_pos_turns = rf->num_turns;  
  return 0;;
}

int check_new_rfp(new_rfp *rf)
{
  int ret = 0;
  if(rf->num_turns < 0) return ret = -1;
  if(rf->meanvalue != 0 && rf->meanvalue != 1) return ret = -22;
  if(rf->frequency < 0) return ret = -3;
  if(rf->num_points < 0) return ret = -4;
  if(rf->level > 0.3 && rf->level < -0.3) return ret = -5;
  if(rf->stepflag != 0 && rf->stepflag != 1) return ret = -23;
  if(rf->num_pos_turns < 0) return ret = -24;
  return ret;
}

int set_new_rfp(new_rfp *rf)
{
  int i=0;
  if(win_scanf("Rotation Parameters Setting:\n"
	       "rf->num turns: %5d\n"
	       "rf->mean value(only useful for alternating): %R 0 %r 1/2\n"
	       "rf->frequency (turns per s): %6f\n"
	       "rf->num points: %6d\n"
	       "rf->amplitude (V): %6f\n"
	       "Phase = %4d/%4d*PI\n"
	       "stepped? %R NO %r YES\n"
	       "rf->epsilon %6f\n"
	       "rf->num pos turns %6d\n",
	       &(rf->num_turns),
	       &(rf->meanvalue),
	       &(rf->frequency),
	       &(rf->num_points),
	       &(rf->level),
	       &(rf->phasenum),
	       &(rf->phaseden),
	       &(rf->stepflag),
	       &(rf->epsilon),
	       &(rf->num_pos_turns)) == CANCEL) return CANCEL;
  if((i = check_new_rfp(rf))!=0) {
    win_printf("something is wrong!\ncheck rf returned %d",i);
    i =CANCEL;
  }
  return i;
}

///
//new_rfp to lio convertion

float64 waveform__rf_function(float y,void* p)
{
  
  int *jj = (int *) p;
  int j = *jj;
  static int times =0;
  double duty_cycle;
  double f = grf->num_turns + 2.*grf->epsilon/PI;
  double x = (y - floor(y));


  if(grf->num_turns != 0)
    duty_cycle =  (double) (grf->num_pos_turns + grf->epsilon/PI)/(grf->num_turns + 2.*grf->epsilon/PI);
  else duty_cycle = 1;

  if( x <= duty_cycle)
    return sin(2*PI*f*x 
	       - grf->epsilon  
	       - (double) grf->phasenum*PI/grf->phaseden 
	       + (double) j/NUMBER_OF_AI_LINES );

  return sin(2*PI*f*x 
	     - 3. * grf->epsilon 
	     + PI
	     - (double) grf->phasenum*PI/grf->phaseden 
	     + (double) j/NUMBER_OF_AI_LINES);
}

int rotation__new_rfp_to_lio()
{
  int *j = 0;
  int e = 0;
  j = (int*)  calloc(1,sizeof(int));
  *j =0;
  
  if(lio == NULL)
    lines__initialize_lio();

  e = (int) (grf->num_points*(grf->epsilon/PI));
  
  lio->num_points = grf->num_turns*grf->num_points + 2*e;
  lio->frequency = (float64) grf->num_points/lio->num_points*grf->frequency;

  for (*j=0 ; *j<NUMBER_OF_AI_LINES ; *j++)
    { 
      lio->lines[*j].amplitude = grf->level;
      lio->lines[*j].offset = 0;
      lio->lines[*j].phase = 0;
      lio->lines[*j].function = waveform__rf_function;
      lio->lines[*j].params = (void*) j;
    }
 
  return 0;
} 



//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
// Control sequences

/** Rotating magnetic field.

All lines are sinus-modulated each with a phase shift of 2PI/3.

\author Francesco Mosconi
\version 12/02/2007
*/
int rotation__rotating_B_all_interface(void)
{
  int ret = 0;
  if(updating_menu_state != 0)	return D_O_K;
  if((ret = set_new_rfp(grf)) != 0) return win_printf("ret = %d", ret);

  rotation__new_rfp_to_lio();
  noChangeInLines = 0;
  return D_O_K;
}

int rotation_rfp_functions_main(void)
{
  if(grf == NULL)
    if((grf = (new_rfp *) calloc(1,sizeof(new_rfp))) == NULL) return win_report("Could not allocate rfp structure !");

  init_new_rfp(grf);

  return D_O_K;
}

#endif
