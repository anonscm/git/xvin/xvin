/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _SDITRK_C_
#define _SDITRK_C_

#include <allegro.h>
# include "xvin.h"

/* If you include other regular header do it here*/


/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "sditrk.h"

int do_filter_crazy_dataset(void)
{
  if(updating_menu_state!=0) return D_O_K;
  pltreg *plr=NULL;
  O_p *op=NULL;
  if (ac_grep(cur_ac_reg,"%pr%op",&plr,&op) != 2)  return D_O_K;
  int n_ds = op->n_dat;
  float threshold = 20;
  if (win_scanf("%d datasets in the plot. Specify the threshold %f", &n_ds, &threshold)==WIN_CANCEL) return D_O_K;
  int i=0;
  int j;
  int count =0;
  bool removed_one = false;
  while (i<op->n_dat)
  {
    removed_one = false;
    j=0;
    while (j<op->dat[i]->nx && removed_one == false)
    {
      if (isnan( fabs(op->dat[i]->yd[j])) || isinf(fabs(op->dat[i]->yd[j]))) op->dat[i]->yd[j] =threshold +1;
      if( fabs(op->dat[i]->yd[j]) > threshold)
      {
        remove_ds_from_op(op, op->dat[i]);
        i--;
        removed_one = true;
        count ++;
      }
      j++;
    }
    i++;
  }
  win_printf_OK("removed %d", count);
  return 0;
}

int do_filter_crazy_jumps(void)
{
  if(updating_menu_state!=0) return D_O_K;
  pltreg *plr=NULL;
  O_p *op=NULL;
  if (ac_grep(cur_ac_reg,"%pr%op",&plr,&op) != 2)  return D_O_K;
  int i=0;
  int j;
  int count =0;
  int n_ds = op->n_dat;
  float threshold = 20;
  if (win_scanf("%d datasets in plot. specify jump threshold %f", &n_ds, &threshold)==WIN_CANCEL) return D_O_K;
  bool removed_one = false;
  while (i<op->n_dat)
  {
    removed_one = false;
    j=0;
    while (j<op->dat[i]->nx-1 && removed_one == false)
    {
      if (isnan( fabs(op->dat[i]->yd[j])) || isinf(fabs(op->dat[i]->yd[j]))) op->dat[i]->yd[j] = 9;
      if( fabs( op->dat[i]->yd[j] - op->dat[i]->yd[j+1]) > threshold)
      {
        remove_ds_from_op(op, op->dat[i]);
        i--;
        removed_one = true;
        count ++;
      }
      j++;
    }
    i++;
  }
  win_printf_OK("removed %d", count);
  return 0;
}

int sdi_v2_estimate_mean(float *tab, int n, float *mean)
{
  int i;
  double mean_tmp;
  mean_tmp=0;
  for (i=0;i<n;i++) mean_tmp+=*(tab+i);
  mean_tmp = mean_tmp / n;
  *mean = (float) mean_tmp;
  return 0;
}

int sdi_v2_estimate_std(float *tab, int n, float *std)
{
  int i;
  float mean;
  double var_tmp;
  var_tmp=0;
  mean=0;
  sdi_v2_estimate_mean(tab,n,&mean);
  for (i=0;i<n;i++) var_tmp += (*(tab+i) - mean) * (*(tab+i) - mean);
  var_tmp = var_tmp / (n-1) ;
  *std = (float) sqrt(var_tmp);
  return 0;
}

int ds_mean_over_roi(d_s *ds, int x_i_min, int x_i_max, float *mean_roi, float *sigma_roi)
{
  int i, size;
  float *roi;
  size = x_i_max - x_i_min;
  roi = (float *) calloc(size, sizeof(float));
  for (i=0 ;i<size ;i++) roi[i] = ds->yd[i + x_i_min];
  sdi_v2_estimate_mean(roi, size, mean_roi);
  sdi_v2_estimate_std(roi, size, sigma_roi);
  free(roi);
  return 0;
}
/*
int ds_extract_periodic_points(d_s *ds_in, d_s *ds_out, int period, int subset_length, int offset, int number)
{
  int i;
  d_s out = build_adjust_data_set(ds_out, number, number);
  while(i<ds_in->nx && i<number)
  {
    if
  }
  return D_O_K;
}*/

int ds_rescale_from_two_plateaux(d_s *ds, float expected_delta, int x_roi1_min, int x_roi1_max, int x_roi2_min, int x_roi2_max)
{
  int i;
  float mean_roi1, sigma_roi1;
  float mean_roi2, sigma_roi2;
  float scale_factor, measured_delta;
  ds_mean_over_roi( ds, x_roi1_min, x_roi1_max, &mean_roi1, &sigma_roi1);
  ds_mean_over_roi( ds, x_roi2_min, x_roi2_max, &mean_roi2, &sigma_roi2);
  measured_delta = mean_roi2 - mean_roi1;
  scale_factor = expected_delta / measured_delta;
  win_printf_OK("mean 1 %f 2 %f delta mes %f scale %f", mean_roi1, mean_roi2, measured_delta, scale_factor);
  for (i=0 ;i<ds->nx ;i++) ds->yd[i] = scale_factor * ds->yd[i];
  return 0;
}


//all ds of the op should have the same length, unit etc
int substract_averaged_ds_to_selected_one_of_plot(pltreg *plr, O_p *op_in, int selected_one, float *sigma_raw, float *sigma_diff)
{
  O_p *op = NULL;
  d_s *diff = NULL;
  d_s *avg = NULL;
  int i, nx, j;
  nx = op_in->dat[0]->nx;
  if (nx < 2) return D_O_K;
  op = create_and_attach_one_plot(plr, nx, nx, 0);
  avg = op->dat[0];
  avg = build_adjust_data_set(avg, nx, nx);
  diff = build_adjust_data_set(diff, nx, nx);
  for(i=0 ; i<nx ;i++)
  {
    avg->xd[i] = i;// op_in->dat[0]->xd[i];
    avg->yd[i] = 0;
    diff->xd[i] = i;//op_in->dat[0]->xd[i];
  }
  for(i=0 ;i<op_in->n_dat ;i++)  if(i != selected_one) for(j=0 ; j<nx ;j++) avg->yd[j] += op_in->dat[i]->yd[j];
  for(i=0 ;i<nx ;i++)
  {
    avg->yd[i]=avg->yd[i] / (float)(op_in->n_dat-1);
    diff->yd[i] = op_in->dat[selected_one]->yd[i] - avg->yd[i];
  }
  add_data_to_one_plot(op ,IS_DATA_SET ,diff);
  add_data_to_one_plot(op ,IS_DATA_SET ,op_in->dat[selected_one]);
  sdi_v2_estimate_std( op_in->dat[selected_one]->yd, nx, sigma_raw);
  sdi_v2_estimate_std( diff->yd, nx, sigma_diff);
  return 0;
}

int do_rescale_ds_with_2_plateaux(void)
{
  if(updating_menu_state!=0) return D_O_K;
  pltreg *plr=NULL;
  O_p *op=NULL;
  static int x_roi1_min = 0, x_roi1_max = 0, x_roi2_min = 0, x_roi2_max = 0;
  static float expected_delta = 2;
  if (ac_grep(cur_ac_reg,"%pr%op",&plr,&op) != 2)  return D_O_K;
  if (win_scanf("this rescales the datasests of current op with the mean valeu of 2 plateaux\n"
        "enter the roi indexes of the 2 plateaux %d %d and %d %d\n"
        "and the expected delta %f", &x_roi1_min, &x_roi1_max, &x_roi2_min, &x_roi2_max, &expected_delta)==WIN_CANCEL) return D_O_K;
  int j;
  for (j=0; j<op->n_dat;j++)
  {
    ds_rescale_from_two_plateaux( op->dat[j], expected_delta, x_roi1_min, x_roi1_max, x_roi2_min, x_roi2_max);
    //win_printf_OK("for ds %d, sigmaraw = %f, sigmadiff = %f", j, sigma_raw, sigma_diff);
  }
  return 0;
}

int do_substraction_of_other_average(void)
{
  if(updating_menu_state!=0) return D_O_K;
  pltreg *plr=NULL;
  O_p *op=NULL;
  if (ac_grep(cur_ac_reg,"%pr%op",&plr,&op) != 2)  return D_O_K;
  if (win_scanf("this sustracts to each the average of the others, creating a plot for each op")==WIN_CANCEL) return D_O_K;
  int j;
  float sigma_raw, sigma_diff;
  for (j=0; j<op->n_dat;j++)
  {
    substract_averaged_ds_to_selected_one_of_plot( plr, op, j, &sigma_raw, &sigma_diff);
    win_printf_OK("for ds %d, sigmaraw = %f, sigmadiff = %f", j, sigma_raw, sigma_diff);
  }
  return 0;
}

MENU *sditrk_plot_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn, "remove ds with crazy values", do_filter_crazy_dataset, NULL, 0, NULL);
  add_item_to_menu(mn, "remove ds with threshold in jumps", do_filter_crazy_jumps, NULL, 0, NULL);
  add_item_to_menu(mn, "differential study of current plot", do_substraction_of_other_average, NULL, 0, NULL);
  add_item_to_menu(mn, "rescale with expected delta", do_rescale_ds_with_2_plateaux, NULL, 0, NULL);
  return mn;
}

int	sditrk_main(int argc, char **argv)
{
  add_plot_treat_menu_item ( "sditrk", NULL, sditrk_plot_menu(), 0, NULL);
  return D_O_K;
}

int	sditrk_unload(int argc, char **argv)
{
  remove_item_to_menu(plot_treat_menu, "sditrk", NULL, NULL);
  return D_O_K;
}
#endif
