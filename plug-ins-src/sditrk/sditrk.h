#pragma once
PXV_FUNC(int, do_sditrk_rescale_plot, (void));
PXV_FUNC(MENU*, sditrk_plot_menu, (void));
PXV_FUNC(int, do_sditrk_rescale_data_set, (void));
PXV_FUNC(int, sditrk_main, (int argc, char **argv));
