/*
 *    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
 */
#ifndef _STAT_C_
#define _STAT_C_

# include "allegro.h"
# include "xvin.h"
# include "float.h"

/* If you include other plug-ins header do it here*/
# include "../nrutil/nrutil.h"

/* But not below this define */

# define BUILDING_PLUGINS_DLL
# include "stat.h"

#include <time.h>
#include <gsl/gsl_sf_gamma.h>


int	init_sigma_stat(s_t *st)
{
    int i;

    if (st == NULL)	return 1;
    for (i = 0; i < 32; i++)
    {
        st->ct[i] = st->av[i] = st->av2[i] = 0;
        st->nt[i] = st->avn[i] = 0;
    }
    return 0;
}

s_t	*create_sigma_stat(char *type)
{
    s_t *st = NULL;

    st = (s_t *)calloc(1,sizeof(s_t));
    init_sigma_stat(st);
    if (type != NULL) snprintf(st->title,128,"%s",type);
    return st;
}

int	add_point_to_sigma_stat(s_t *st, float pt)
{
    int i, j;
    double tmp;

    if (st == NULL)	return 1;
    for (i = 0, j = 1; i < 30; i++, j*=2)
    {
        st->ct[i] += (double)pt;
        st->nt[i]++;
        if (st->nt[i] >= j)
        {
            tmp = st->ct[i]/j;
            st->av[i] += tmp;
            st->av2[i] += tmp*tmp;
            st->nt[i] = 0;
            st->ct[i] = 0;
            st->avn[i]++;
        }
    }
    return 0;
}

int spit_sigma_stat(s_t *st)
{
    int i, j;
    double mean, mean2;
    char out[2048] = {0};

    if (st == NULL)	return 1;
    for (i = 0, j = 1; i < 32 && st->avn[i] >= 8; i++, j*=2)
    {
        mean = st->av[i]/st->avn[i];
        if (i == 0)	sprintf(out,"mean %g over %d\n",mean,st->avn[i]);
        mean2 = (st->av2[i]/st->avn[i]) - mean*mean;
        sprintf(out,"%d %g\n",j,sqrt(mean2/(st->avn[i])));
    }
    win_printf("%s",out);
    return 0;
}
# ifdef EN_COURS
int do_fit_sigma_stat_in_ds(s_t *st,  float *meanval, float *errmean, float *n0)
{
    int i, j;
    d_s *ds = NULL, *dserr = NULL, *dsfit = NULL;
    double mean = 0, mean2, chi2, a, nc, chi20, a0, nc0, chi21, a1, nc1, tmp;

    if (st == NULL)	return 1;
    ds = build_data_set(32,32);
    if (ds == NULL)	return 1;
    dserr = build_data_set(64,64);
    if (dserr == NULL)	return 1;
    *meanval = ((meanval != NULL) && (st->avn[0] != 0)) ? st->av[0]/st->avn[0] : 0;
    for (i = 0, j = 1; i < 32 && st->avn[i] >= 8 && i < ds->mx && i < dserr->mx; i++, j*=2)
    {
        mean = st->av[i]/st->avn[i];                       // this is the mean value
        mean2 = (st->av2[i]/st->avn[i]) - mean*mean;       // this is sigma^2
        dserr->xd[2*i+1] = dserr->xd[2*i] = ds->xd[i] = j;
        ds->yd[i] = mean2/(st->avn[i]);                    // this is sigma^2 of the full avg
        dserr->yd[2*i] = ds->yd[i];
        ds->yd[i] = sqrt(ds->yd[i]);                       // this is sigma of the full avg
        dserr->yd[2*i+1] = ds->yd[i]/st->avn[i]/2;
    }
    ds->nx = ds->ny = i;
    dserr->nx = dserr->ny = 2*i;
    set_dot_line(ds);
    set_dash_line(dserr);
    set_plot_symb(dserr, "-");
    set_plot_symb(ds, "\\oc");

    nc0 = (double)1;
    chi20 = compute_chi2_and_fit_a(nc0, &a0, ds, dserr);
    nc1 = ds->xd[ds->nx-1];
    chi21 = compute_chi2_and_fit_a(nc1, &a1, ds, dserr);
    for (nc = exp((log(nc0)+log(nc1))/2); fabs(nc1/nc0) > 1.05 ; )
    {
        nc = exp((log(nc0)+log(nc1))/2);
        chi2 = compute_chi2_and_fit_a(nc, &a, ds, dserr);
        display_title_message("\\chi^2 = %g n0 = %g a %g",chi2,nc,a);
        if (chi20 < chi21)
        {
            chi21 = chi2;
            nc1 = nc;
            a1 = a;
        }
        else
        {
            chi20 = chi2;
            nc0 = nc;
            a0 = a;
        }
    }
    for (i = 0; i < ds->nx; i++)
    {
        dserr->yd[2*i] /= (ds->xd[i]*st->avn[i])/(2*(ds->xd[i]+nc));
        /*		dserr->yd[2*i] *= (nc+ds->xd[i])/nc; */
        dserr->yd[2*i] = sqrt(dserr->yd[2*i]);
        dserr->yd[2*i+1] = ds->yd[i] - dserr->yd[2*i];
        dserr->yd[2*i] += ds->yd[i];
    }
    tmp = log(ds->xd[ds->nx-1]);
    for (i = 0; i < dsfit->nx; i++)
    {
        dsfit->xd[i] = exp((i*tmp)/dsfit->nx);
        dsfit->yd[i] = sqrt(1 + nc/dsfit->xd[i]);
        dsfit->yd[i] = a /dsfit->yd[i];
    }
    *n0 = nc;

    //*meanval = mean;
    *errmean = a;
    return ds;
}

# endif

double compute_chi2_and_fit_a(double n0, double *a, d_s *ds, d_s *dserr)
{
    int i;
    double n, d, tmp, sig2, fita = 0, ssig2;

    for(i = 0, n = d = 0; i < ds->nx; i++)
    {
        tmp = sqrt(1 + n0/ds->xd[i]);
        sig2 = dserr->yd[2*i]*(n0+ds->xd[i])/n0;
        if (sig2 == 0 || tmp == 0) 	win_printf("division by zero at pt %d",i);
        else
        {
            n += ds->yd[i]/(tmp*sig2);
            d += (double)1/(tmp*tmp*sig2);
        }
    }
    if (d == 0) 	win_printf("division by zero in fit");
    else	fita = n/d;
    for(i = 0, n = ssig2 = 0; i < ds->nx; i++)
    {
        tmp = sqrt(1 + n0/ds->xd[i]);
        sig2 = dserr->yd[2*i]*(n0+ds->xd[i])/n0;
        if (sig2 == 0 || tmp == 0) 	win_printf("division by zero at pt %d",i);
        else
        {
            ssig2 += sig2;
            tmp = ds->yd[i] - (fita/tmp);
            n += (tmp*tmp)/sig2;
        }
    }
    if (a != NULL)	*a = fita;
    return n*ssig2;
}
d_s *fit_sigma_stat_in_ds(s_t *st, d_s *ds, d_s *dserr, d_s *dsfit,
                          float *meanval, float *errmean, float *n0)
{
    int i, j;
    double mean = 0, mean2, chi2, a, nc, chi20, a0, nc0, chi21, a1, nc1, tmp;

    if (st == NULL)	return NULL;
    if (ds == NULL)	ds = build_data_set(32,32);
    if (ds == NULL)	return NULL;
    if (dserr == NULL)	dserr = build_data_set(64,64);
    if (dserr == NULL)	return NULL;
    *meanval = ((meanval != NULL) && (st->avn[0] != 0)) ? st->av[0]/st->avn[0] : 0;
    for (i = 0, j = 1; i < 32 && st->avn[i] >= 8 && i < ds->mx && i < dserr->mx; i++, j*=2)
    {
        mean = st->av[i]/st->avn[i];
        mean2 = (st->av2[i]/st->avn[i]) - mean*mean;
        dserr->xd[2*i+1] = dserr->xd[2*i] = ds->xd[i] = j;
        ds->yd[i] = mean2/(st->avn[i]);
        dserr->yd[2*i] = ds->yd[i];
        ds->yd[i] = sqrt(ds->yd[i]);
        dserr->yd[2*i+1] = ds->yd[i]/st->avn[i]/2;
    }
    ds->nx = ds->ny = i;
    dserr->nx = dserr->ny = 2*i;
    set_dot_line(ds);
    set_dash_line(dserr);
    set_plot_symb(dserr, "-");
    set_plot_symb(ds, "\\oc");

    nc0 = (double)1;
    chi20 = compute_chi2_and_fit_a(nc0, &a0, ds, dserr);
    nc1 = ds->xd[ds->nx-1];
    chi21 = compute_chi2_and_fit_a(nc1, &a1, ds, dserr);
    for (nc = exp((log(nc0)+log(nc1))/2); fabs(nc1/nc0) > 1.05 ; )
    {
        nc = exp((log(nc0)+log(nc1))/2);
        chi2 = compute_chi2_and_fit_a(nc, &a, ds, dserr);
        display_title_message("\\chi^2 = %g n0 = %g a %g",chi2,nc,a);
        if (chi20 < chi21)
        {
            chi21 = chi2;
            nc1 = nc;
            a1 = a;
        }
        else
        {
            chi20 = chi2;
            nc0 = nc;
            a0 = a;
        }
    }
    for (i = 0; i < ds->nx; i++)
    {
        dserr->yd[2*i] /= (ds->xd[i]*st->avn[i])/(2*(ds->xd[i]+nc));
        /*		dserr->yd[2*i] *= (nc+ds->xd[i])/nc; */
        dserr->yd[2*i] = sqrt(dserr->yd[2*i]);
        dserr->yd[2*i+1] = ds->yd[i] - dserr->yd[2*i];
        dserr->yd[2*i] += ds->yd[i];
    }
    tmp = log(ds->xd[ds->nx-1]);
    for (i = 0; i < dsfit->nx; i++)
    {
        dsfit->xd[i] = exp((i*tmp)/dsfit->nx);
        dsfit->yd[i] = sqrt(1 + nc/dsfit->xd[i]);
        dsfit->yd[i] = a /dsfit->yd[i];
    }
    *n0 = nc;

    //*meanval = mean;
    *errmean = a;
    return ds;
}
d_s *spit_sigma_on_avg_stat_in_ds(s_t *st, d_s *ds, d_s *dserr, float *meanval)
{
    int i, j;
    double mean, mean2;

    if (st == NULL)	return NULL;
    if (ds == NULL)	ds = build_data_set(32,32);
    if (ds == NULL)	return NULL;
    if (dserr == NULL)	dserr = build_data_set(64,64);
    if (dserr == NULL)	return NULL;
    *meanval = ((meanval != NULL) && (st->avn[0] != 0)) ? st->av[0]/st->avn[0] : 0;
    for (i = 0, j = 1; i < 32 && st->avn[i] >= 8 && i < ds->mx && i < dserr->mx; i++, j*=2)
    {
        mean = st->av[i]/st->avn[i];
        mean2 = (st->av2[i]/st->avn[i]) - mean*mean;
        dserr->xd[2*i+1] = dserr->xd[2*i] = ds->xd[i] = j;
        ds->yd[i] = sqrt(mean2/(st->avn[i]));
        dserr->yd[2*i] = ds->yd[i] + ds->yd[i]/sqrt(st->avn[i]/2);
        dserr->yd[2*i+1] = ds->yd[i] - ds->yd[i]/sqrt(st->avn[i]/2);
    }
    ds->nx = ds->ny = i;
    dserr->nx = dserr->ny = 2*i;
    set_dot_line(ds);
    set_dash_line(dserr);
    set_plot_symb(dserr, "-");
    set_plot_symb(ds, "\\oc");
    return ds;
}

d_s *spit_sigma_stat_in_ds(s_t *st, d_s *ds, d_s *dserr, float *meanval)
{
    int i, j;
    double mean, mean2;

    if (st == NULL)	return NULL;
    if (ds == NULL)	ds = build_data_set(32,32);
    if (ds == NULL)	return NULL;
    if (dserr == NULL)	dserr = build_data_set(64,64);
    if (dserr == NULL)	return NULL;
    *meanval = ((meanval != NULL) && (st->avn[0] != 0)) ? st->av[0]/st->avn[0] : 0;
    for (i = 0, j = 1; i < 32 && st->avn[i] >= 8 && i < ds->mx && i < dserr->mx; i++, j*=2)
    {
        mean = st->av[i]/st->avn[i];
        mean2 = (st->av2[i]/st->avn[i]) - mean*mean;
        dserr->xd[2*i+1] = dserr->xd[2*i] = ds->xd[i] = j;
        ds->yd[i] = sqrt(mean2/(st->avn[i]));
        dserr->yd[2*i] = ds->yd[i] + ds->yd[i]/sqrt(st->avn[i]/2);
        dserr->yd[2*i+1] = ds->yd[i] - ds->yd[i]/sqrt(st->avn[i]/2);
    }
    ds->nx = ds->ny = i;
    dserr->nx = dserr->ny = 2*i;
    set_dot_line(ds);
    set_dash_line(dserr);
    set_plot_symb(dserr, "-");
    set_plot_symb(ds, "\\oc");
    return ds;
}

d_s *spit_sigma_stat_in_ds_with_error(s_t *st, d_s *ds, float *meanval)
{
    int i, j;
    double mean, mean2;

    if (st == NULL)	return NULL;
    if (ds == NULL)	ds = build_data_set(32,32);
    if (ds->nx < 32) ds = build_adjust_data_set(ds, 32, 32);
    if (ds == NULL)	return NULL;
    alloc_data_set_y_error(ds);
    *meanval = ((meanval != NULL) && (st->avn[0] != 0)) ? st->av[0]/st->avn[0] : 0;
    for (i = 0, j = 1; i < 32 && st->avn[i] >= 8 && i < ds->mx; i++, j*=2)
    {
        mean = st->av[i]/st->avn[i];
        mean2 = (st->av2[i]/st->avn[i]) - mean*mean;
        ds->xd[i] = j;
        ds->yd[i] = sqrt(mean2/(st->avn[i]));
        ds->ye[i] = ds->yd[i]/sqrt(st->avn[i]/2);
    }
    ds->nx = ds->ny = i;
    set_dot_line(ds);
    set_plot_symb(ds, "\\di");
    return ds;
}

int	do_sigma_stat(void)
{
    int i;
    pltreg *pr = NULL;
    O_p *op = NULL, *opd = NULL;
    d_s *ds = NULL, *dsd = NULL;
    static s_t *st = NULL;
    float mean;

    if(updating_menu_state != 0)	return D_O_K;
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
        return win_printf_OK("cannot find data");

    opd = create_and_attach_one_plot(pr, 32,32, 0);
    if (opd == NULL)
        return win_printf_OK("Cannot allocate plot");
    dsd = opd->dat[0];
    if (dsd == NULL)		return 1;

    //if ((dserr = create_and_attach_one_ds(opd, 64, 64, 0)) == NULL)
    //return win_printf_OK("cannot create plot");


    if (st == NULL)	st = create_sigma_stat("plot");
    else init_sigma_stat(st);
    if (st == NULL)
        return win_printf_OK("cannot allocate memory for zst\n");

    for (i = 0; i < ds->nx; i++)
        add_point_to_sigma_stat(st, op->ay+op->dy*ds->yd[i]);
    spit_sigma_stat_in_ds_with_error(st, dsd, &mean);

    set_plot_title(opd, "\\stack{{Correlation decay mean %g}{%s}}",mean,st->title);
    set_plot_x_title(opd, "Bin size");
    set_plot_y_title(opd, "\\sigma");
    //free(st);
    return refresh_plot(pr,pr->n_op-1);
}


int	do_fit_sigma_stat(void)
{
    int i;
    pltreg *pr = NULL;
    O_p *op = NULL, *opd = NULL;
    d_s *ds = NULL, *dsd = NULL, *dserr = NULL, *dsfit = NULL;
    static s_t *st = NULL;
    float mean, errmean, n0;

    if(updating_menu_state != 0)	return D_O_K;
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
        return win_printf_OK("cannot find data");

    opd = create_and_attach_one_plot(pr, 32,32, 0);
    if (opd == NULL)		return win_printf_OK("Cannot allocate plot");
    dsd = opd->dat[0];
    if (dsd == NULL)		return 1;

    if ((dserr = create_and_attach_one_ds(opd, 64, 64, 0)) == NULL)
        return win_printf_OK("cannot create plot");

    if ((dsfit = create_and_attach_one_ds(opd, 256, 256, 0)) == NULL)
        return win_printf_OK("cannot create plot");

    if (st == NULL)	st = create_sigma_stat("plot");
    else init_sigma_stat(st);
    if (st == NULL)
        return win_printf_OK("cannot allocate memory for zst\n");

    for (i = 0; i < ds->nx; i++)
        add_point_to_sigma_stat(st, op->ay+op->dy*ds->yd[i]);
    fit_sigma_stat_in_ds(st, dsd, dserr, dsfit, &mean, &errmean, &n0);

    set_plot_title(opd, "\\stack{{Correlation decay}{mean value %g, +/- %g}"
                   "{correlation size %g}{%s}}",mean, errmean,n0,st->title);
    set_plot_x_title(opd, "Bin size");
    set_plot_y_title(opd, "\\sigma");
    set_plot_x_log(opd);
    set_plot_y_log(opd);
    //free(st);
    return refresh_plot(pr,pr->n_op-1);
}


double compute_chi2_and_fit_a_sqrt_1_n_over_n0(double n0, double *a, d_s *ds)
{
    int i;
    double n, d, tmp, sig2, fita = 0;

    for(i = 0, n = d = 0; i < ds->nx; i++)
    {
        tmp = sqrt(1 + ds->xd[i]/n0);
        sig2 = ds->ye[i]*ds->ye[i];
        if (sig2 == 0 || tmp == 0) 	win_printf("division by zero at pt %d",i);
        else
        {
            n += ds->yd[i]/(tmp*sig2);
            d += (double)1/(tmp*tmp*sig2);
        }
    }
    if (d == 0) 	win_printf("division by zero in fit");
    else	fita = n/d;
    for(i = 0, n = 0; i < ds->nx; i++)
    {
        tmp = sqrt(1 + ds->xd[i]/n0);
        sig2 = ds->ye[i]*ds->ye[i];
        if (sig2 == 0 || tmp == 0) 	win_printf("division by zero at pt %d",i);
        else
        {
            tmp = ds->yd[i] - (fita/tmp);
            n += (tmp*tmp)/sig2;
        }
    }
    if (a != NULL)	*a = fita;
    return n;
}



double compute_chi2_and_fit_a_sqrt_1_over_n( double *a, d_s *ds)
{
    int i;
    double n, d, tmp, sig2, fita = 0;

    for(i = 0, n = d = 0; i < ds->nx; i++)
    {
        tmp = sqrt(ds->xd[i]);
        sig2 = ds->ye[i]*ds->ye[i];
        if (sig2 == 0 || tmp == 0) 	win_printf("division by zero at pt %d",i);
        else
        {
            n += ds->yd[i]/(tmp*sig2);
            d += (double)1/(tmp*tmp*sig2);
        }
    }
    if (d == 0) 	win_printf("division by zero in fit");
    else	fita = n/d;
    for(i = 0, n = 0; i < ds->nx; i++)
    {
        tmp = sqrt(ds->xd[i]);
        sig2 = ds->ye[i]*ds->ye[i];
        if (sig2 == 0 || tmp == 0) 	win_printf("division by zero at pt %d",i);
        else
        {
            tmp = ds->yd[i] - (fita/tmp);
            n += (tmp*tmp)/sig2;
        }
    }
    if (a != NULL)	*a = fita;
    return n;
}


float p_value_from_chi2(float chi2, float dof)
{
    if (dof <=0 || chi2 < 0) return(win_printf_OK("Invalid chi2 or degrees of freedom\n"));
    return((float) gsl_sf_gamma_inc_Q((double) (dof/2.0), (double) chi2/2.0));
}


int	do_bin_stat(void)
{
    int i, j, k;
    pltreg *pr = NULL;
    O_p *op = NULL, *opd = NULL, *opchi2 = NULL;
    d_s *ds = NULL, *dsd = NULL, *dstmp = NULL, *dsfit = NULL, *dsst = NULL;
    d_s *dschi2a = NULL, *dschi2b = NULL;
    static s_t *st = NULL;
    int navg;
    double mean = 0, mean2, chi2 = 0, a, nc, chi20, a0, nc0, chi21, a1, nc1, tmp, sig2, mean_0, sig;
    static int nmin = 8;

    if(updating_menu_state != 0)	return D_O_K;
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
        return win_printf_OK("cannot find data");

    if (st == NULL)	st = create_sigma_stat("plot");
    else init_sigma_stat(st);


    i = win_scanf("Minimum number of samples allowed\nto compute \\sigma %8d\n",&nmin);
    if (i == WIN_CANCEL)   return D_O_K;


    opchi2 = create_and_attach_one_plot(pr, 32,32, 0);
    if (opchi2 == NULL)
        return win_printf_OK("Cannot allocate plot");
    dschi2a = opchi2->dat[0];
    dschi2a->nx = dschi2a->ny = 0;

    if ((dschi2b = create_and_attach_one_ds(opchi2, 256, 256, 0)) == NULL)
        return win_printf_OK("cannot create plot");

    opd = create_and_attach_one_plot(pr, 32,32, 0);
    if (opd == NULL)
        return win_printf_OK("Cannot allocate plot");
    dsd = opd->dat[0];
    if (dsd == NULL)   return win_printf_OK("Cannot allocate plot");
    alloc_data_set_y_error(dsd);

    if ((dsst = create_and_attach_one_ds(opd, 32, 32, 0)) == NULL)
        return win_printf_OK("cannot create plot");
    alloc_data_set_y_error(dsst);

    dstmp = build_data_set(ds->nx, ds->nx);
    if (dstmp == NULL)   return win_printf_OK("Cannot allocate plot");

    if ((dsfit = create_and_attach_one_ds(opd, 256, 256, 0)) == NULL)
        return win_printf_OK("cannot create plot");




    for (k = 0, mean_0 = 0; k < ds->nx; k++)
        mean_0 += op->ay + op->dy * ds->yd[k];
    mean_0 = (ds->nx > 0) ? mean_0/ds->nx: mean_0;

    for (i = 0, j = 1; i < 32 && j <= ds->nx/nmin; i++, j*=2)
    {
        for (k = 0; k < ds->nx; k++) dstmp->yd[k] = 0;
        navg = ds->nx/j;
        for (k = 0; k < ds->nx; k++)
            dstmp->yd[k/j] += (op->ay + op->dy * ds->yd[k])/j;
        for (k = 0, mean = 0; k < navg; k++)
            mean += dstmp->yd[k];
        mean = (navg > 0) ? mean/navg : mean;
        for (k = 0, sig2 = 0; k < navg; k++)
            sig2 += (dstmp->yd[k] - mean) * (dstmp->yd[k] - mean);
        sig2 = (navg > 0) ? sig2/navg : sig2;
        dsd->xd[i] = j;
        dsd->yd[i] = sqrt(sig2);
        dsd->ye[i] = dsd->yd[i];
        if (navg > 0) 	dsd->ye[i] /= sqrt(navg);
        dsd->nx = dsd->ny = i+1;
    }

    for (k = 0; k < ds->nx; k++)
        add_point_to_sigma_stat(st, op->ay+op->dy*ds->yd[k]);

    for (i = 0, j = 1; i < 32 && st->avn[i] >= nmin && i < dsst->mx; i++, j*=2)
    {
        mean = st->av[i]/st->avn[i];
        mean2 = (st->av2[i]/st->avn[i]) - mean*mean;
        dsst->xd[i] = j;
        dsst->yd[i] = sqrt(mean2);
        dsst->ye[i] = dsst->yd[i]/sqrt(st->avn[i]);
        dsst->nx = dsst->ny = i+1;
    }




    nc0 = (double)1;
    chi2 = chi20 = compute_chi2_and_fit_a_sqrt_1_n_over_n0(nc0, &a0, dsd);
    nc1 = dsd->xd[dsd->nx-1];
    chi21 = compute_chi2_and_fit_a_sqrt_1_n_over_n0(nc1, &a1, dsd);
    for (nc = exp((log(nc0)+log(nc1))/2); fabs(nc1/nc0) > 1.0005 ; )
    {
        nc = exp((log(nc0)+log(nc1))/2);
        chi2 = compute_chi2_and_fit_a_sqrt_1_n_over_n0(nc, &a, dsd);
        display_title_message("\\chi^2 = %g n0 = %g a %g",chi2,nc,a);
        add_new_point_to_ds(dschi2a, nc, chi2);
        if (chi20 < chi21)
        {
            chi21 = chi2;
            nc1 = nc;
            a1 = a;
        }
        else
        {
            chi20 = chi2;
            nc0 = nc;
            a0 = a;
        }
    }

    nc0 = (double)1;
    chi2 = chi20 = compute_chi2_and_fit_a_sqrt_1_n_over_n0(nc0, &a0, dsd);

    tmp = log(dsd->xd[dsd->nx-1]);
    for (i = 0; i < dsfit->nx; i++)
    {
        dsfit->xd[i] = exp((i*tmp)/dsfit->nx);
        chi21 = compute_chi2_and_fit_a_sqrt_1_n_over_n0(dsfit->xd[i], &a1, dsd);
        display_title_message("\\chi^2 = %g n0 = %g a %g",chi21,dsfit->xd[i],a1);
        dschi2b->xd[i] = dsfit->xd[i];
        dschi2b->yd[i] = chi21;
        if (chi21 < chi2)
        {
            chi2 = chi21;
            nc = dsfit->xd[i];
            a = a1;
        }
    }


    tmp = log(dsd->xd[dsd->nx-1]);
    for (i = 0; i < dsfit->nx; i++)
    {
        dsfit->xd[i] = exp((i*tmp)/dsfit->nx);
        dsfit->yd[i] = sqrt(1 + dsfit->xd[i]/nc);
        dsfit->yd[i] = a /dsfit->yd[i];
    }


    sig = sqrt(1 + (float)ds->nx/nc);
    if (sig > 0) sig = a/sig;


    if (compute_chi2_and_fit_a_sqrt_1_over_n( &a1, dsd) < chi2)
    {
        nc = 0;
        a = a1;
        tmp = log(dsd->xd[dsd->nx-1]);
        for (i = 0; i < dsfit->nx; i++)
        {
            dsfit->xd[i] = exp((i*tmp)/dsfit->nx);
            dsfit->yd[i] = sqrt(dsfit->xd[i]);
            dsfit->yd[i] = (dsfit->yd[i] != 0) ? a /dsfit->yd[i] : a;
        }
    }

    set_plot_title(opd, "\\stack{{Correlation decay}{mean %g \\stack{+-} %g}{a %g nc %g}}"
                   ,mean_0,sig,a,nc);
    set_plot_x_title(opd, "Bin size b");
    set_plot_y_title(opd, "\\sigma_b");
    set_dot_line(dsd);
    set_plot_symb(dsd, "\\di");
    set_dot_line(dsst);
    set_plot_symb(dsst, "\\pt6\\oc");
    set_plot_x_log(opd);
    set_plot_y_log(opd);

    set_plot_x_title(opchi2, "Nc size");
    set_plot_y_title(opchi2, "\\chi^2");

    //free(st);
    return refresh_plot(pr,pr->n_op-1);
}



int min_sup_0(float *x, int n, float *m)
{
    int i, j;

    for (j = 0, i=0 ; i< n ; i++)
    {
        if (x[i] > 0)
        {
            if (j == 0)	*m = x[i];
            if ( x[i] < *m )	*m = x[i];
            j++;
        }
    }
    return (j);
}
int max_sup_0(float *x, int n, float *m)
{
    int i, j;

    for (j = 0, i=0 ; i< n ; i++)
    {
        if (x[i] > 0)
        {
            if (j == 0)	*m = x[i];
            if ( x[i] > *m )	*m = x[i];
            j++;
        }
    }
    return (j);
}
int min_sup_to(float *x, int n, float *m, float thres)
{
    int i, j;

    for (j = 0, i=0 ; i< n ; i++)
    {
        if (x[i] > thres)
        {
            if (j == 0)	*m = x[i];
            if ( x[i] < *m )	*m = x[i];
            j++;
        }
    }
    return (j);
}
int max_sup_to(float *x, int n, float *m, float thres)
{
    int i, j;

    for (j = 0, i=0 ; i< n ; i++)
    {
        if (x[i] > thres)
        {
            if (j == 0)	*m = x[i];
            if ( x[i] > *m )	*m = x[i];
            j++;
        }
    }
    return (j);
}
int min_norm(float *x, int n, float *m)
{
    int i, j;

    for (j = 0, i=0 ; i< n ; i++)
    {
        if (j == 0)	*m = x[i];
        if ( x[i] < *m )	*m = x[i];
        j++;
    }
    return (j);
}
int max_norm(float *x, int n, float *m)
{
    int i, j;

    for (j = 0, i=0 ; i< n ; i++)
    {
        if (j == 0)	*m = x[i];
        if ( x[i] > *m )	*m = x[i];
        j++;
    }
    return (j);
}

int add_gaussian_noise(d_s *ds, d_s *dsr, d_s *dsd, d_s *dsdr, long *idum)
{
    int i, nx;
    int idum_int = *idum;

    if (ds == NULL || dsr == NULL || dsr == NULL || dsdr == NULL || idum == NULL)
        return 1;
    nx = ds->nx;
    for (i = 0; i < nx; i++)
    {
        dsd->xd[i] = ds->xd[i];
        dsd->yd[i] = ds->yd[i] + dsr->yd[i] * gasdev(&idum_int);
        *idum = idum_int;
        dsdr->xd[i] = dsr->xd[i];
        dsdr->yd[i] = dsr->yd[i];
    }
    return 0;
}
int bootstrap(d_s *ds, d_s *dsr, d_s *dsd, d_s *dsdr, long *idum)
{
    int i, j, nx;
    float ran;
    int idum_int = *idum;

    nx = ds->nx;
    for (i = 0; i < nx; i++)
    {
        ran = ran1(&idum_int);
        *idum = idum_int;
        j = (int)(nx*ran);
        j = (j < 0) ? 0 : j;
        j = (j < nx) ? j : nx-1;
        dsd->xd[i] = ds->xd[j];
        dsd->yd[i] = ds->yd[j];
        dsdr->xd[i] = dsr->xd[j];
        dsdr->yd[i] = dsr->yd[j];
    }
    return 0;
}

/*dsi data, dsr error data set*/
int	draw_error_bars(O_p *op, d_s *dsi, d_s *dsr)
{
    int i;
    pltreg *pr = NULL;
    //O_p *op;
    d_s //*dsi, *dsr,
        *dsd = NULL;
    int nf;
    //static int ndat = 0;
    //nder = 1;

    if(updating_menu_state != 0)	return D_O_K;
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine draw error bars\n"
                             "spanning from y - \\delta y to y + \\delta y\n"
                             "given two data sets first one is y second is \\delta y");
    }

    if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)	return 1;
    //ndat = op->cur_dat;
    //i = win_scanf("enter data set index of data %d and error data set %d",
    //	&ndat,&nder);
    //if (i == WIN_CANCEL)	return OFF;
    /*if (ndat < 0 || nder < 0 || ndat >= op->n_dat || ndat >= op->n_dat)
      return win_printf_OK("data set index out of range !");*/

    /*dsi = op->dat[ndat];
      dsr = op->dat[nder];*/
    if (dsi->nx != dsr->nx)
        return win_printf_OK("data set have different size !");

    nf = dsi->nx;
    if ((dsd = create_and_attach_one_ds(op, 2*nf, 2*nf, 0)) == NULL)
        return win_printf_OK("cannot create plot");

    for (i = 0; i < nf; i++)
    {
        if (dsi->xd[i] != dsr->xd[i])
            win_printf_OK("pts at index %d differ in X\nData X = %g error X %g!"
                          ,i,dsi->xd[i],dsr->xd[i]);
        dsd->xd[2*i] = dsd->xd[2*i+1] = dsi->xd[i];
        dsd->yd[2*i] = dsi->yd[i] + dsr->yd[i];
        dsd->yd[2*i+1] = dsi->yd[i] - dsr->yd[i];
    }
    set_dash_line(dsd);
    set_plot_symb(dsd, "-");
    inherit_from_ds_to_ds(dsd, dsi);
    //	return refresh_plot(pr,pr->cur_op);
    return WIN_OK;
}

int	do_draw_error_bars(void)
{
    int i;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *dsi = NULL, *dsr = NULL, *dsd = NULL;
    int nf;
    static int ndat = 0, nder = 1;

    if(updating_menu_state != 0)	return D_O_K;
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine draw error bars\n"
                             "spanning from y - \\delta y to y + \\delta y\n"
                             "given two data sets first one is y second is \\delta y");
    }

    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)	return 1;
    ndat = op->cur_dat;
    i = win_scanf("enter data set index of data %d and error data set %d",
                  &ndat,&nder);
    if (i == WIN_CANCEL)	return OFF;
    if (ndat < 0 || nder < 0 || ndat >= op->n_dat || ndat >= op->n_dat)
        return win_printf_OK("data set index out of range !");

    dsi = op->dat[ndat];
    dsr = op->dat[nder];
    if (dsi->nx != dsr->nx)
        return win_printf_OK("data set have different size !");

    nf = dsi->nx;
    if ((dsd = create_and_attach_one_ds(op, 2*nf, 2*nf, 0)) == NULL)
        return win_printf_OK("cannot create plot");

    for (i = 0; i < nf; i++)
    {
        if (dsi->xd[i] != dsr->xd[i])
            win_printf_OK("pts at index %d differ in X\nData X = %g error X %g!"
                          ,i,dsi->xd[i],dsr->xd[i]);
        dsd->xd[2*i] = dsd->xd[2*i+1] = dsi->xd[i];
        dsd->yd[2*i] = dsi->yd[i] + dsr->yd[i];
        dsd->yd[2*i+1] = dsi->yd[i] - dsr->yd[i];
    }
    set_dash_line(dsd);
    set_plot_symb(dsd, "-");
    inherit_from_ds_to_ds(dsd, dsi);
    return refresh_plot(pr,pr->cur_op);
}
int	do_extract_error(void)
{
    int i, j;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *dsi = NULL, *dsd = NULL;

    if(updating_menu_state != 0)	return D_O_K;
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine extrats the error bars\n"
                             "\\delta y for a data set spanning from y - \\delta y to y + \\delta y");
    }

    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)	return 1;

    if (dsi->nx%2)	return win_printf_OK("number of points must be even");
    for (i = 0; i < dsi->nx; i+=2)
    {
        if (dsi->xd[i] != dsi->xd[i+1])
            return win_printf_OK("points do not correspond to error bars");
    }
    if ((dsd = create_and_attach_one_ds(op, dsi->nx/2, dsi->nx/2, 0)) == NULL)
        return win_printf_OK("cannot create data set");

    for (i = 0, j = 0; i < dsi->nx; i+=2)
    {
        dsd->xd[j] = dsi->xd[i];
        dsd->yd[j] = fabs(dsi->yd[i+1] - dsi->yd[i])/2;
        j++;
    }
    return refresh_plot(pr, UNCHANGED);
}
int	draw_histo_bars(void)
{
    int i;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *dsi = NULL,  *dsd = NULL;
    int nf;
    float x, dx;
    static float sp = 0.1;

    if(updating_menu_state != 0)	return D_O_K;
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine draw histogram vertical bars\n"
                             "with a adjustable factional width");
    }

    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)	return 1;
    i = win_scanf("Build histo bars with a spacing\n"
                  "enter the relative amount of spacing %f ",&sp);
    if (i == WIN_CANCEL)	return OFF;

    nf = dsi->nx;
    if ((dsd = create_and_attach_one_ds(op, 4*nf+1, 4*nf+1, 0)) == NULL)
        return win_printf("cannot create plot");

    sp = 1 - sp;
    for (i = 0, dx = (dsi->xd[1] - dsi->xd[0])/2; i < nf; i++)
    {
        x = dsi->xd[i];
        dsd->xd[4*i] = dsd->xd[4*i+1] = x - dx*sp;
        dsd->xd[4*i+2] = dsd->xd[4*i+3] = x + dx*sp;
        dsd->yd[4*i+1] = dsd->yd[4*i+2] = dsi->yd[i];
        dsd->yd[4*i] = dsd->yd[4*i+3] = 0;
        if (i)	dx = (dsi->xd[i] - dsi->xd[i-1])/2;
    }
    dsd->xd[4*i] = dsd->xd[0];
    dsd->yd[4*i] = 0;
    inherit_from_ds_to_ds(dsd, dsi);
    sp = 1 - sp;
    return refresh_plot(pr,pr->cur_op);

}

int simulate_double_exponential(d_s *ds,int n1, int n2, float t1, float t2, float lim1, float lim2)
{
  if (ds->mx < n1+n2 || ds->my <n1+n2) return(win_printf_OK("data set to small\n"));
  for (int i =0;i<n1;i++)
  {
    ds->xd[i] = i;
    ds->yd[i] = (float) (rand()) / (RAND_MAX + 1.0);
    if (ds->yd[i] == 0) ds->yd[i] = 1;
    ds->yd[i] = -log(ds->yd[i])*t1;
    if (ds->yd[i] < lim1) ds->yd[i] = lim1;
  }

  for (int i =n1;i<n1+n2;i++)
  {
    ds->xd[i] = i;
    ds->yd[i] = (float) (rand()) / (RAND_MAX + 1.0);
    if (ds->yd[i] == 0) ds->yd[i] = 1;
    ds->yd[i] = -log(ds->yd[i])*t2;
    if (ds->yd[i] < lim2) ds->yd[i] = lim2;
  }


  return 0;
}

int simulate_n_exponential_mixture(d_s *ds,int n,int n_points,float **terms, float *aa, float *ta)
{
    //int tot_numb = 0;
  int nn = 0;
  int i =0;
  int ind = 0;
  float cumsum[n];

  float randint = 0;

  (void)**terms;
  cumsum[0] = aa[0];
  for (int ii = 1;ii<n;ii++)
  {
    cumsum[ii] = cumsum[ii-1]+ aa[ii];
    printf(" cumsum %d %f\n",ii,cumsum[ii]);
  }
  cumsum[n-1] = 1;

  if (ds->mx < nn || ds->my <nn) return(win_printf_OK("data set to small\n"));

  for (i=0;i<n_points;i++)
  {

    randint = (float) (rand()) / (RAND_MAX + 1.0);
    ind = n-1;
    for (int ii = 0;ii<n;ii++)
    {
      if (randint < cumsum[ii])
      {
        ind = ii;
        break;
      }
    }

    ds->xd[i] = i;
    ds->yd[i] = (float) (rand()) / (RAND_MAX + 1.0);
    if (ds->yd[i] == 0) ds->yd[i] = 1;
    ds->yd[i] = -log(ds->yd[i])*ta[ind];
  }
  ds->nx=ds->ny=n_points;

  return 0;
}

int do_simulate_double_exponential(void)
{
  int i;
  pltreg *pr;
  O_p *op = NULL;
  d_s *ds = NULL;
  float t1 = 0, t2 =0;
  int n1 = 5, n2=100;
  float lim1 = 0, lim2 = 0;
  if(updating_menu_state != 0)	return D_O_K;
  if (key[KEY_LSHIFT])
  {
      return win_printf_OK("This routine simulates double exponentials \n");
  }

    if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
        return win_printf("cannot find data");

    i = win_scanf("Population 1 : nb of points %6d, time %6f, low limit %6f\n"
  "Population 2 : nb of points %6d, time %6f, low limit %6f \n"
  ,&n1,&t1,&lim1,&n2,&t2,&lim2);
  if (i == WIN_CANCEL) return 0;



   op=create_and_attach_one_plot(pr, n1+n2,n1+n2,INSERT_HERE);
   ds=op->dat[0];
   ds->nx = ds->ny = n1+n2;
   simulate_double_exponential(ds,n1,n2,t1,t2,lim1,lim2);
   return 0;

}

int	do_logarithmic_binning(void)
{
    int i, k;
    static float bin=1.0, binu = 1.0;
    static int norm = 1, adj_x = 0, xwieghted = 0, insert = 1; // , cumul = 0
    float mi, ma, m, s2;//, f, f1, tmp, tmptot;
    int  npt, index = 1, n_dat, curop = 0;// nh0, nh1
    pltreg *pr = NULL;
    d_s *dss = NULL, *dsd = NULL, *dsd2 = NULL;//, *dsdc = NULL; //, *dsder;
    O_p *op = NULL, *ops = NULL;
    //  un_s *uny;
    char s[2048] = {0};
    static float factor =1.4;

    if(updating_menu_state != 0)	return D_O_K;
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine construct an histogram from a data\n"
                             "set, together with a data set of associated errors\n"
                             "assuming uncorrelated data");
    }


    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&ops,&dss) != 3)
        return win_printf("cannot find data");
    n_dat = ops->n_dat;

    for (i = 0, m = 0, mi = ma = dss->yd[0]; i < dss->nx; i++)
    {
        if (dss->yd[i] > ma) ma = dss->yd[i];
        if (dss->yd[i] < mi) mi = dss->yd[i];
        m += dss->yd[i];
    }
    for (i = 0, m /= (dss->nx) ? dss->nx : 1, s2 = 0; i < dss->nx; i++)
        s2 += (dss->yd[i] - m)*(dss->yd[i] - m);
    s2 /= (dss->nx) ? dss->nx : 1;
    s2 = sqrt(s2);
    bin = (3.5*s2 )/pow(dss->nx,0.333); // keir formula
    binu = ops->dy * bin;
    mi = 0;
    if (ops->y_unit == NULL)
	  snprintf(s,sizeof(s),"compute normal histogram \n"
		   "points Bin size %%6f \n"
       "Binning factor %%f\n"

		   "Position points X to %%R->1/2 of bin %%r mean of data\n"
       "Smaller limit %%6f\n"
		   );
        else
            snprintf(s,sizeof(s),"compute normal histogram \n"
                    "points Bin size in %s %%6f \n"
                    "Binning factor %%f\n"
		                       "Position points X to %%R->1/2 of bin %%r mean of data\n"
                    "Smaller limit %%6f\n"
                    ,ops->y_unit);
    if (win_scanf(s, &binu,&factor,&adj_x,&mi)==WIN_CANCEL) return 0;
    //  uny = ops->yu[ops->c_yu];
    curop = pr->cur_op;
    bin =  (ops->dy != 0) ?	 binu/ops->dy : binu;
    if (index == 0)	npt = min_sup_0 (dss->yd, dss->nx, &mi);
    else 	npt = min_norm (dss->yd, dss->nx, &mi);
    if (npt == 0)	return win_printf("I find no points !");

    if (index == 0)	npt = max_sup_0 (dss->yd, dss->nx, &ma);
    else 	npt = max_norm (dss->yd, dss->nx, &ma);
    if (npt == 0)	return win_printf("I find no points !");
    int nlimits = 64;
    float *limits;
    limits = calloc(nlimits,sizeof(float));
    limits[0] = mi;
    int iji = 0;
    float binsize=bin;
    for (iji = 1;limits[iji-1]<=ma;iji++)
    {
      printf("iji = %d;limits[iji]=%f,ma=%f\n",iji,limits[iji],ma);
      if (iji >= nlimits)
      {
        nlimits += 64;
        limits=realloc(limits,nlimits*sizeof(float));
      }
      limits[iji] = limits[iji - 1] + binsize;
      binsize = binsize*factor;

    }

    op = create_and_attach_one_plot( pr, iji - 1, iji - 1,  INSERT_HERE );
    if (op == NULL)	return (win_printf("cannot create plot"));

    dsd2 = op->dat[0];


    alloc_data_set_x_error(dsd2);
    for (k=0;k<iji-1;k++)
    {
      dsd2->xd[k] = dsd2->yd[k] = dsd2->xe[k] = 0;
    }
    set_ds_treatement(dsd2, "%sHistogram with bin size:%g  over %d pts"
		      ,(xwieghted)?"Weighted ":"", binu, npt);
    //if ((alloc_data_set_y_error(dsd) == NULL) || (dsd->ye == NULL))
    //  return win_printf_OK("I can't create errors !");

    if ((alloc_data_set_y_error(dsd2) == NULL) || (dsd2->ye == NULL))
        return win_printf_OK("I can't create errors !");
    //	dsder = create_and_attach_one_ds( op, nh1-1, nh1-1, 0);


    for (i=0 ; i< dss->nx ; i++)
      {  // we average the x position in the bin
        //if (index != 0 || dss->yd[i] > 0)
	//{
	for (k=0;k<iji && limits[k] < dss->yd[i];){k++;}
  k--;
  if (k<0) k=0;
	if (k >= 0 && k < dsd2->nx)
	  {
	    dsd2->yd[k] += 1;
      dsd2->ye[k] +=1;
      dsd2->xd[k] += dss->yd[i];



	  }
	//}
      }

  for (k=0;k<iji-1;k++)
  {

    dsd2->ye[k] = sqrt(dsd2->ye[k]);
    if (adj_x && dsd2->yd[k] != 0) dsd2->xd[k] /= dsd2->yd[k];
    else dsd2->xd[k] = 0.5 * (limits[k] + limits[k+1]);
    dsd2->yd[k]/=(limits[k+1] - limits[k])*(dss->nx);
    dsd2->ye[k]/=(limits[k+1] - limits[k])*(dss->nx);
  }


    op->filename = my_sprintf(op->filename,"histo-%s",ops->filename);

    if (ops->dy != 0)	bin *= ops->dy;




    set_plot_title(op, "\\stack{{histogram from %s ds %d}"
                   "{bin: %g %s with %d points}}", (ops->filename!=NULL) ? ops->filename : " ",
                   n_dat, bin, (op->x_unit != NULL)?op->x_unit:" ",npt);
    set_ds_source(dsd,"%s",(dss->source!= NULL)?dss->source:" ");
    set_ds_source(dsd2,"%s",(dss->source!= NULL)?dss->source:" ");




    uns_op_2_op_by_type(ops, IS_Y_UNIT_SET, op, IS_X_UNIT_SET);
    refresh_plot(pr, (insert) ? curop+1 : pr->n_op - 1);
    return 0;
}


int	do_histo_non_zero(void)
{
    int i, k;
    static float bin=1.0, binu = 1.0;
    static int norm = 1, adj_x = 0, cumul = 0, xwieghted = 0, insert = 1;
    float mi, ma, m, s2, f, f1, tmp, tmptot;
    int nh0, nh1, npt, index = 1, n_dat, curop = 0;
    pltreg *pr = NULL;
    d_s *dss = NULL, *dsd = NULL, *dsd2 = NULL, *dsdc = NULL; //, *dsder;
    O_p *op = NULL, *ops = NULL;
    //  un_s *uny;
    char s[2048] = {0};

    if(updating_menu_state != 0)	return D_O_K;
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine construct an histogram from a data\n"
                             "set, together with a data set of associated errors\n"
                             "assuming uncorrelated data");
    }


    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&ops,&dss) != 3)
        return win_printf("cannot find data");
    n_dat = ops->n_dat;

    for (i = 0, m = 0, mi = ma = dss->yd[0]; i < dss->nx; i++)
    {
        if (dss->yd[i] > ma) ma = dss->yd[i];
        if (dss->yd[i] < mi) mi = dss->yd[i];
        m += dss->yd[i];
    }
    for (i = 0, m /= (dss->nx) ? dss->nx : 1, s2 = 0; i < dss->nx; i++)
        s2 += (dss->yd[i] - m)*(dss->yd[i] - m);
    s2 /= (dss->nx) ? dss->nx : 1;
    s2 = sqrt(s2);
    bin = (3.5*s2 )/pow(dss->nx,0.333); // keir formula
    binu = ops->dy * bin;
    for (f = f1 = 20e37, i = 0; f > FLT_MIN && f > binu; f = f1 = f1/10)
    {
        f = f1 * 0.75;
        if (f < FLT_MIN || f < binu) break;
        f = f1/2;
        if (f < FLT_MIN || f < binu) break;
        f = 2*f1/5;
        if (f < FLT_MIN || f < binu) break;
        f = 3*f1/10;
        if (f < FLT_MIN || f < binu) break;
        f = f1/4;
        if (f < FLT_MIN || f < binu) break;
        f = 3.5*f1/20;
        if (f < FLT_MIN || f < binu) break;
    }
    f1 = binu;
    binu = f;
    if (index == 0)
    {
        if (ops->y_unit == NULL)
	  snprintf(s,sizeof(s),"compute histo of stricly positive\n"
		   "points Bin size (%g) %%6f use X data for weighting %%b\n"
		   "Normalization:\n"
		   "None->%%R (# pts) %%r (Probability desnsity) %%r\n"
		   "Position points X to %%R->1/2 of bin %%r mean of data\n"
		   "Add Cumulative histogram:\n"
		   "%%R->no, %%r->integrate forward %%r->integrate backward\n"
		   "Insert plot [%%R at the end] or [%%r after this one]\n"
                    ,f1);
        else
	  snprintf(s,sizeof(s),"compute histo of stricly positive\n"
                    "points Bin size (%g) in %s %%6f use X data for weighting %%b\n"
		    "Normalization:\n"
                    "None->%%R (# pts) %%r (Probability density) %%r\n"
                    "Position points X to %%R->1/2 of bin %%r mean of data\n"
                    "Add Cumulative histogram:\n"
                    "%%R->no, %%r->integrate forward %%r->integrate backward\n"
		   "Insert plot [%%R at the end] or [%%r after this one]\n"
                    ,f1,ops->y_unit);
    }
    else
    {
        if (ops->y_unit == NULL)
	  snprintf(s,sizeof(s),"compute normal histogram \n"
		   "points Bin size %%6f  use X data for weighting %%b\n"
		   "Normalization:\n"
		   "None->%%R (# pts) %%r (Probability density) %%r\n"
		   "Position points X to %%R->1/2 of bin %%r mean of data\n"
		   "Add Cumulative histogram:\n"
		   "%%R->no, %%r->integrate forward %%r->integrate backward\n"
		   "Insert plot [%%R at the end] or [%%r after this one]\n");
        else
            snprintf(s,sizeof(s),"compute normal histogram \n"
                    "points Bin size in %s %%6f  use X data for weighting %%b\n"
		    "Normalization:\n"
                    "None->%%R (# pts) %%r (Probability density) %%r\n"
                    "Position points X to %%R->1/2 of bin %%r mean of data\n"
                    "Add Cumulative histogram:\n"
                    "%%R->no, %%r->integrate forward %%r->integrate backward\n"
		     "Insert plot [%%R at the end] or [%%r after this one]\n"
                    ,ops->y_unit);
    }
    if (win_scanf(s, &binu,&xwieghted,&norm,&adj_x,&cumul,&insert)==WIN_CANCEL) return 0;
    //  uny = ops->yu[ops->c_yu];
    curop = pr->cur_op;
    bin =  (ops->dy != 0) ?	 binu/ops->dy : binu;
    if (index == 0)	npt = min_sup_0 (dss->yd, dss->nx, &mi);
    else 	npt = min_norm (dss->yd, dss->nx, &mi);
    if (npt == 0)	return win_printf("I find no points !");

    if (index == 0)	npt = max_sup_0 (dss->yd, dss->nx, &ma);
    else 	npt = max_norm (dss->yd, dss->nx, &ma);
    if (npt == 0)	return win_printf("I find no points !");
    nh0 = (int)floor( mi/bin);
    nh1 = (int)floor( ma/bin) + 2 - nh0;

    op = create_and_attach_one_plot( pr, (2*nh1-2), (2*nh1-2), (insert) ? INSERT_HERE : 0);
    if (op == NULL)	return (win_printf("cannot create plot"));
    dsd = op->dat[0];
    if (dsd == NULL) return (win_printf("Cannot create data set"));
    dsd2 = create_and_attach_one_ds( op, nh1-1, nh1-1, 0);
    set_ds_treatement(dsd2, "%sHistogram with bin size:%g  over %d pts"
		      ,(xwieghted)?"Weighted ":"", binu, npt);
    //if ((alloc_data_set_y_error(dsd) == NULL) || (dsd->ye == NULL))
    //  return win_printf_OK("I can't create errors !");

    if ((alloc_data_set_y_error(dsd2) == NULL) || (dsd2->ye == NULL))
        return win_printf_OK("I can't create errors !");
    //	dsder = create_and_attach_one_ds( op, nh1-1, nh1-1, 0);


    for (i=0 ; i< dss->nx ; i++)
      {  // we average the x position in the bin
        //if (index != 0 || dss->yd[i] > 0)
	//{
	k = ((int)floor(dss->yd[i]/bin) - nh0);
	if (k >= 0 && k < dsd2->nx)
	  {
	    dsd2->yd[k] += 1;
	    dsd2->xd[k] += dss->yd[i];
	  }
	//}
      }
    for (i = 0; i < dsd2->nx ; i++)
      {
	dsd2->xd[i] = (dsd2->yd[i] > 0) ? dsd2->xd[i]/dsd2->yd[i] : 0;
	if (adj_x == 0 || dsd2->yd[i] == 0)
	  dsd2->xd[i] = (bin * (2*nh0+2*i+1))/2;
	dsd2->yd[i] = 0;
      }


    for (i=0 ; i< dss->nx ; i++)
    {
      //if (index != 0 || dss->yd[i] > 0)
      //{
      k = ((int)floor(dss->yd[i]/bin) - nh0);
      if (k >= 0 && k < dsd2->nx)
	{
	  dsd2->yd[k] += (xwieghted) ? dss->xd[i] : 1;
	  dsd2->ye[k] += (xwieghted) ? dss->xd[i] * dss->xd[i] : 1;
	}
      //}
    }

    for (i = 0; i < dsd2->nx; i++)
      {
        dsd2->ye[i] = sqrt(dsd2->ye[i]);
        if (norm)
	  {
            dsd2->yd[i] /= npt;
            dsd2->ye[i] /= npt;
	  }
	dsd->xd[2*i] = bin * (nh0+i);
	dsd->xd[2*i+1] = bin * (nh0+i+1);
	dsd->yd[2*i] = dsd->yd[2*i+1] = dsd2->yd[i];
      }


    if (norm > 1)
    {
        for ( i=0 ; i<dsd->nx ; i++)
        {
            dsd->yd[i] /= bin*ops->dy;
        }
        for ( i=0 ; i<dsd2->nx ; i++)
        {
            dsd2->yd[i] /= bin*ops->dy;
            dsd2->ye[i] /= bin*ops->dy;
        }
    }




# ifdef KEEP

    //dx1 = 1.0 / (npt);
    for (i=0 ; i< dss->nx ; i++)
    {
        if (index != 0 || dss->yd[i] > 0)
        {
            k = 1+2*((int)floor(dss->yd[i]/bin) - nh0);
            dsd->yd[k] += (xwieghted) ? dss->xd[i] : 1;
	    dsd->ye[k] += (xwieghted) ? dss->xd[i] * dss->xd[i] : 1;
            dsd->xd[k] += dss->yd[i];
        }
    }

    for ( i=0 ; i<nh1-1 ; i++)
        dsd2->xd[i] = (dsd->yd[2*i+1] > 0) ? dsd->xd[2*i+1]/dsd->yd[2*i+1] : 0;

    dsd->xd[0]  = bin * nh0;	dsd->yd[0] = 0;
    for ( i=1 ; i<nh1 ; i++)
    {
        dsd->xd[2*i-1] = bin * (nh0+i-1);
        dsd->xd[ 2*i ] = bin * (nh0+i);
        dsd->yd[ 2*i ] = dsd->yd[2*i-1];
	dsd->ye[ 2*i ] = dsd->ye[2*i-1];
    }

    dsd->xd[2*nh1-1] = bin * (nh0+nh1-1);
    dsd->yd[2*nh1-1]=0;
    for ( i=0 ; i<nh1-1 ; i++)
    {
        //dsder->xd[i] =
        dsd2->yd[i] = (dsd->yd[2*i+1] + dsd->yd[2*i+2])/2;
        if (adj_x == 0 || dsd2->yd[i] == 0)
            dsd2->xd[i] = (dsd->xd[2*i+1] + dsd->xd[2*i+2])/2;
        dsd2->ye[i] = 1 + (dsd->ye[2*i+1] + dsd->ye[2*i+2])/2;;
        dsd2->ye[i] = sqrt(dsd2->ye[i]);
        if (norm)
        {
            dsd2->yd[i] /= npt;
            dsd2->ye[i] /= npt;
        }
    }
    free_data_set_y_error(dsd);
    for (i = 0; i<2*nh1 && norm > 0 ; i++)	dsd->yd[i] /= npt;
    if (norm > 1)
    {
        for ( i=0 ; i<2*nh1 ; i++)
        {
            dsd->yd[i] /= bin*ops->dy;
        }
        for ( i=0 ; i<nh1-1 ; i++)
        {
            dsd2->yd[i] /= bin*ops->dy;
            dsd2->ye[i] /= bin*ops->dy;
        }
    }

# endif

    op->filename = my_sprintf(op->filename,"histo-%s",ops->filename);
    if (nh1 < 30) set_plot_symb(dsd2, "\\oc");
    else if (nh1 < 100) set_plot_symb(dsd2, "\\pt6\\oc");
    else set_plot_symb(dsd2, "\\pt4\\oc");
    set_dot_line(dsd2);

    if (ops->dy != 0)	bin *= ops->dy;
    set_plot_title(op, "\\stack{{histogram from %s ds %d}"
                   "{bin: %g %s with %d points}}", (ops->filename!=NULL) ? ops->filename : " ",
                   n_dat, bin, (op->x_unit != NULL)?op->x_unit:" ",npt);
    set_ds_source(dsd,"%s",(dss->source!= NULL)?dss->source:" ");
    set_ds_source(dsd2,"%s",(dss->source!= NULL)?dss->source:" ");

    if (cumul == 1)
    {
        dsdc = duplicate_data_set(dsd2,NULL);
        for (i = 1; i < dsdc->nx; i++)
        {
            dsdc->yd[i] = dsdc->yd[i-1] + dsdc->yd[i];
            //if (dsdc->ye)  dsdc->ye[i] = sqrt(dsdc->ye[i-1]*dsdc->ye[i-1] + dsdc->ye[i]*dsdc->ye[i]);
        }
        for (i = 0; i < dsdc->nx; i++)
        {
            tmp = dsdc->yd[i];
            tmptot = dsdc->yd[dsdc->nx-1];
            if (tmptot > 0) tmp /= tmptot;
            if (dsdc->ye)  dsdc->ye[i] = tmptot*sqrt(tmp*(1-tmp)/npt);
            if (dsdc->ye[i] == 0)
            {
                tmp = (float)1/npt;
                dsdc->ye[i] = tmptot*sqrt(tmp*(1-tmp)/npt);
            }
        }
        inherit_from_ds_to_ds(dsdc, dsd2);
        set_ds_treatement(dsdc, "Forward cumulative histogram bin %g, # points %d",bin,npt);
        add_one_plot_data (op, IS_DATA_SET, (void *)dsdc);
    }
    if (cumul == 2)
    {
        dsdc = duplicate_data_set(dsd2,NULL);
        for (i = dsdc->nx - 2; i >= 0; i--)
        {
            dsdc->yd[i] = dsdc->yd[i+1] + dsdc->yd[i];
            //if (dsdc->ye)  dsdc->ye[i] = sqrt(dsdc->ye[i+1]*dsdc->ye[i+1] + dsdc->ye[i]*dsdc->ye[i]);
        }
        for (i = 0; i < dsdc->nx; i++)
        {
            tmp = dsdc->yd[i];
            tmptot = dsdc->yd[0];
            if (tmptot > 0) tmp /= tmptot;
            if (dsdc->ye)  dsdc->ye[i] = tmptot*sqrt(tmp*(1-tmp)/npt);
            if (dsdc->ye[i] == 0)
            {
                tmp = (float)1/npt;
                dsdc->ye[i] = tmptot*sqrt(tmp*(1-tmp)/npt);
            }
        }
        inherit_from_ds_to_ds(dsdc, dsd2);
        set_ds_treatement(dsdc, "Backward cumulative histogram bin %g, # points %d",bin,npt);
        add_one_plot_data (op, IS_DATA_SET, (void *)dsdc);
    }



    uns_op_2_op_by_type(ops, IS_Y_UNIT_SET, op, IS_X_UNIT_SET);
    refresh_plot(pr, (insert) ? curop+1 : pr->n_op - 1);
    return 0;
}


//following function by mr
d_s*	histo_background(d_s *dss,float bin, float *weights)
{
    int i, k;
    int norm = 1, adj_x = 0;//, cumul = 0, xwieghted = 0;//, insert = 1;
    float mi, ma, m, s2;//, f, f1, tmp, tmptot;
    int nh0, nh1, npt, index = 1;//, n_dat;

    d_s *dsd = NULL, *dsd2 = NULL;//, *dsdc = NULL; //, *dsder;

    //  un_s *uny;
    //char s[2048] = {0};
    float weights_sum  = 0;



    norm = 0;
    adj_x = 0;
    //cumul = 0;
    //insert = 0;


    for (i = 0, m = 0, mi = ma = dss->yd[0]; i < dss->nx; i++)
    {
        if (dss->yd[i] > ma) ma = dss->yd[i];
        if (dss->yd[i] < mi) mi = dss->yd[i];
        m += dss->yd[i];
    }
    for (i = 0, m /= (dss->nx) ? dss->nx : 1, s2 = 0; i < dss->nx; i++)
        s2 += (dss->yd[i] - m)*(dss->yd[i] - m);
    s2 /= (dss->nx) ? dss->nx : 1;
    s2 = sqrt(s2);


    //  uny = ops->yu[ops->c_yu];


    if (index == 0)	npt = min_sup_0 (dss->yd, dss->nx, &mi);
    else 	npt = min_norm (dss->yd, dss->nx, &mi);
    if (npt == 0)	return win_printf_ptr("I find no points !");

    if (index == 0)	npt = max_sup_0 (dss->yd, dss->nx, &ma);
    else 	npt = max_norm (dss->yd, dss->nx, &ma);
    if (npt == 0)	return win_printf_ptr("I find no points !");
    nh0 = (int)( mi/bin);
    nh1 = (int)(( ma/bin) + 2 - nh0);


    dsd = build_data_set((2*nh1-2), (2*nh1-2));
    dsd2 = build_data_set(nh1-1, nh1-1);
    if (dsd == NULL || dsd2 == NULL) return (win_printf_ptr("Cannot create data set"));

    //if ((alloc_data_set_y_error(dsd) == NULL) || (dsd->ye == NULL))
    //  return win_printf_OK("I can't create errors !");

    if ((alloc_data_set_y_error(dsd2) == NULL) || (dsd2->ye == NULL))
        return win_printf_ptr("I can't create errors !");
    //	dsder = create_and_attach_one_ds( op, nh1-1, nh1-1, 0);


    for (i=0 ; i< dss->nx ; i++)
      {  // we average the x position in the bin
        //if (index != 0 || dss->yd[i] > 0)
	//{
	k = ((int)((dss->yd[i]/bin) - nh0));
	if (k >= 0 && k < dsd2->nx)
	  {
	    if (weights != NULL) dsd2->yd[k] += weights[i];
      else dsd2->yd[k] += 1;
	    dsd2->xd[k] += dss->yd[i];
	  }
	//}
      }
    for (i = 0; i < dsd2->nx ; i++)
      {
	dsd2->xd[i] = (dsd2->yd[i] > 0) ? dsd2->xd[i]/dsd2->yd[i] : 0;
	if (adj_x == 0 || dsd2->yd[i] == 0)
	  dsd2->xd[i] = (bin * (2*nh0+2*i+1))/2;
	dsd2->yd[i] = 0;
      }
      weights_sum = 0;

    for (i=0 ; i< dss->nx ; i++)
    {
      //if (index != 0 || dss->yd[i] > 0)
      //{
      k = ((int)((dss->yd[i]/bin) - nh0));
      if (k >= 0 && k < dsd2->nx)
	{
	  if (weights == NULL)
    {
      dsd2->yd[k] +=  1;
      dsd2->ye[k] +=  1;
    }

    else
    {
      dsd2->ye[k] +=  weights[i]*weights[i];
      dsd2->yd[k] +=  weights[i];
      weights_sum +=  weights[i];
    }

	}
      //}
    }

    for (i = 0; i < dsd2->nx; i++)
      {
        dsd2->ye[i] = sqrt(dsd2->ye[i]);
        if (norm)
	  {
      if (weights)
      {
        dsd2->yd[i] /= weights_sum;
        dsd2->ye[i] /= weights_sum;
      }
      else {
            dsd2->yd[i] /= npt;
            dsd2->ye[i] /= npt;
          }
	  }
	dsd->xd[2*i] = bin * (nh0+i);
	dsd->xd[2*i+1] = bin * (nh0+i+1);
	dsd->yd[2*i] = dsd->yd[2*i+1] = dsd2->yd[i];
      }


    if (norm > 1)
    {
        for ( i=0 ; i<dsd->nx ; i++)
        {
            dsd->yd[i] /= bin;
        }
        for ( i=0 ; i<dsd2->nx ; i++)
        {
            dsd2->yd[i] /= bin;
            dsd2->ye[i] /= bin;
        }
    }


    free_data_set(dsd);
    return dsd2;
}

/**
 * @brief Make an histogram
 *
 * @param pr plot region in which create the histogram plot
 * @param ops the O_p from which are from
 * @param dss the data
 * @param bin size of a bin if bin < 0 compute bin auto
 * @param norm Normalisation : norm 0->None #pts)->1 (Probability density)->2"
 * @param adj_x Position of points X : adj_x = 0 ->to 1/2 of bin 1-> to mean of data\n"
 * @param cumul cumul = 0 -> do nothing, cumul = 1 add a cumulative forward dataset cumul = 2 add a
 * cumulative backward dataset
 *
 * @return the plot of the computed histogram
 */

O_p	*histogram_vc(pltreg *pr, O_p *ops, d_s *dss, float bin, int norm, int adj_x, int cumul)
{
    int i, k;
    static float binu = 1.0;
    float mi, ma, m, s2, f, f1, tmp, tmptot;
    int nh0, nh1, npt, n_dat; // , index = 1
    d_s *dsd = NULL, *dsd2 = NULL, *dsdc = NULL; //, *dsder;
    O_p  *op = NULL;
    //  char s[256];


    if (ops == NULL || ops->dat[ops->cur_dat]->nx < 2)
        return NULL;
    if (dss == NULL)
        dss = ops->dat[ops->cur_dat];

    n_dat = ops->n_dat;

    for (i = 0, m = 0, mi = ma = dss->yd[0]; i < dss->nx; i++)
    {
        if (dss->yd[i] > ma) ma = dss->yd[i];
        if (dss->yd[i] < mi) mi = dss->yd[i];
        m += dss->yd[i];
    }
    for (i = 0, m /= (dss->nx) ? dss->nx : 1, s2 = 0; i < dss->nx; i++)
        s2 += (dss->yd[i] - m)*(dss->yd[i] - m);
    s2 /= (dss->nx) ? dss->nx : 1;
    s2 = sqrt(s2);

    if (bin < 0)
    {
        bin = (3.5*s2 )/pow(dss->nx,0.333); // keir formula
        binu = ops->dy * bin;
        for (f = f1 = 20e37, i = 0; f > FLT_MIN && f > binu; f = f1 = f1/10)
        {
            f = f1 * 0.75;
            if (f < FLT_MIN || f < binu) break;
            f = f1/2;
            if (f < FLT_MIN || f < binu) break;
            f = 2*f1/5;
            if (f < FLT_MIN || f < binu) break;
            f = 3*f1/10;
            if (f < FLT_MIN || f < binu) break;
            f = f1/4;
            if (f < FLT_MIN || f < binu) break;
            f = 3.5*f1/20;
            if (f < FLT_MIN || f < binu) break;
        }
        f1 = binu;
        binu = f;
        bin = binu;
    }
    bin =  (ops->dy != 0) ?	 bin/ops->dy : bin;

    npt = min_norm (dss->yd, dss->nx, &mi);
    if (npt == 0)	return (O_p *) win_printf_ptr("I find no points !");

    npt = max_norm (dss->yd, dss->nx, &ma);
    if (npt == 0)	return (O_p *)win_printf_ptr("I find no points !");
    nh0 = (int)floor( mi/bin);
    nh1 = (int)floor( ma/bin) + 2 - nh0;

    op = create_and_attach_one_plot( pr, 2*nh1, 2*nh1, 0);
    if (op == NULL)	return (O_p *)win_printf_ptr("cannot create plot");
    dsd = op->dat[0];
    if (dsd == NULL) return (O_p *)win_printf_ptr("Cannot create data set");
    dsd2 = create_and_attach_one_ds( op, nh1-1, nh1-1, 0);

    if ((alloc_data_set_y_error(dsd2) == NULL) || (dsd2->ye == NULL))
        return (O_p *)win_printf_ptr("I can't create errors !");
    for (i=0 ; i< dss->nx ; i++)
    {
        k = 1+2*((int)floor( dss->yd[i]/bin) - nh0);
        dsd->yd[k] += 1;
        dsd->xd[k] += dss->yd[i];
    }

    for ( i=0 ; i<nh1-1 ; i++)
        dsd2->xd[i] = (dsd->yd[2*i+1] > 0) ? dsd->xd[2*i+1]/dsd->yd[2*i+1] : 0;

    dsd->xd[0]  = bin * nh0;	dsd->yd[0] = 0;
    for ( i=1 ; i<nh1 ; i++)
    {
        dsd->xd[2*i-1] = bin * (nh0+i-1);
        dsd->xd[ 2*i ] = bin * (nh0+i);
        dsd->yd[ 2*i ] = dsd->yd[2*i-1];
    }

    dsd->xd[2*nh1-1] = bin * (nh0+nh1-1);
    dsd->yd[2*nh1-1]=0;
    for ( i=0 ; i<nh1-1 ; i++)
    {
        dsd2->yd[i] = (dsd->yd[2*i+1] + dsd->yd[2*i+2])/2;
        if (adj_x == 0 || dsd2->yd[i] == 0)
            dsd2->xd[i] = (dsd->xd[2*i+1] + dsd->xd[2*i+2])/2;
        dsd2->ye[i] = dsd2->yd[i] + 1;
        dsd2->ye[i] = sqrt(dsd2->ye[i]);
        if (norm)
        {
            dsd2->yd[i] /= npt;
            dsd2->ye[i] /= npt;
        }
    }
    for (i = 0; i<2*nh1 && norm > 0 ; i++)	dsd->yd[i] /= npt;
    if (norm > 1)
    {
        for ( i=0 ; i<2*nh1 ; i++)
        {
            dsd->yd[i] /= bin*ops->dy;
        }
        for ( i=0 ; i<nh1-1 ; i++)
        {
            dsd2->yd[i] /= bin*ops->dy;
            dsd2->ye[i] /= bin*ops->dy;
        }
    }
    op->filename = my_sprintf(op->filename,"histo-%s",ops->filename);
    if (nh1 < 30) set_plot_symb(dsd2, "\\oc");
    else if (nh1 < 100) set_plot_symb(dsd2, "\\pt6\\oc");
    else set_plot_symb(dsd2, "\\pt4\\oc");
    set_dot_line(dsd2);

    if (ops->dy != 0)	bin *= ops->dy;
    op->title = my_sprintf(NULL, "\\stack{{histogram from %s ds %d}"
                           "{bin: %g %s with %d points}}", (ops->filename!=NULL) ? ops->filename : " ",
                           n_dat, bin, (op->x_unit != NULL)?op->x_unit:" ",npt);
    dsd->source = my_sprintf(NULL,"%s",(dss->source!= NULL)?dss->source:" ");
    dsd2->source = my_sprintf(NULL,"%s",(dss->source!= NULL)?dss->source:" ");

    if (cumul == 1)
    {
        dsdc = duplicate_data_set(dsd2,NULL);
        for (i = 1; i < dsdc->nx; i++)
        {
            dsdc->yd[i] = dsdc->yd[i-1] + dsdc->yd[i];
            //if (dsdc->ye)  dsdc->ye[i] = sqrt(dsdc->ye[i-1]*dsdc->ye[i-1] + dsdc->ye[i]*dsdc->ye[i]);
        }
        for (i = 0; i < dsdc->nx; i++)
        {
            tmp = dsdc->yd[i];
            tmptot = dsdc->yd[dsdc->nx-1];
            if (tmptot > 0) tmp /= tmptot;
            if (dsdc->ye)  dsdc->ye[i] = tmptot*sqrt(tmp*(1-tmp)/npt);
            if (dsdc->ye[i] == 0)
            {
                tmp = (float)1/npt;
                dsdc->ye[i] = tmptot*sqrt(tmp*(1-tmp)/npt);
            }
        }
        inherit_from_ds_to_ds(dsdc, dsd2);
        set_ds_treatement(dsdc, "Forward cumulative histogram bin %g, # points %d",bin,npt);
        add_one_plot_data (op, IS_DATA_SET, (void *)dsdc);
    }
    if (cumul == 2)
    {
        dsdc = duplicate_data_set(dsd2,NULL);
        for (i = dsdc->nx - 2; i >= 0; i--)
        {
            dsdc->yd[i] = dsdc->yd[i+1] + dsdc->yd[i];
            //if (dsdc->ye)  dsdc->ye[i] = sqrt(dsdc->ye[i+1]*dsdc->ye[i+1] + dsdc->ye[i]*dsdc->ye[i]);
        }
        for (i = 0; i < dsdc->nx; i++)
        {
            tmp = dsdc->yd[i];
            tmptot = dsdc->yd[0];
            if (tmptot > 0) tmp /= tmptot;
            if (dsdc->ye)  dsdc->ye[i] = tmptot*sqrt(tmp*(1-tmp)/npt);
            if (dsdc->ye[i] == 0)
            {
                tmp = (float)1/npt;
                dsdc->ye[i] = tmptot*sqrt(tmp*(1-tmp)/npt);
            }
        }
        inherit_from_ds_to_ds(dsdc, dsd2);
        set_ds_treatement(dsdc, "Backward cumulative histogram bin %g, # points %d",bin,npt);
        add_one_plot_data (op, IS_DATA_SET, (void *)dsdc);
    }



    uns_op_2_op_by_type(ops, IS_Y_UNIT_SET, op, IS_X_UNIT_SET);
    return op;
}




/* fit exponential */

double Y_equal_a0_plus_a1_exp_moins_lt( double t, double a0, double a1, double l)
{
    double tmp;

    tmp = -l * t;
    if (tmp > FLT_MAX_EXP)
        tmp = FLT_MAX;
    else if (tmp < FLT_MIN_EXP)
        tmp = FLT_MIN;
    else	tmp = exp(tmp);
    tmp *= a1;
    tmp += a0;
    return (tmp);
}

void Y_equal_a0_plus_a1_exp_moins_lt_func(float t, float *a, float *y, float *dyda, int ma)
{
    double  a0, a1, l, dyda0, dyda1, dydl;
    double  tmp = 0;

    (void)ma;
    a0 = a[1];
    a1 = a[2];
    l = a[3];
    *y = Y_equal_a0_plus_a1_exp_moins_lt( t, a0, a1, l);
    tmp = a0;
    tmp += -l * t;
    if (tmp > FLT_MAX_EXP)
        tmp = FLT_MAX;
    else if (tmp < FLT_MIN_EXP)
        tmp = FLT_MIN;
    else	tmp = exp(tmp);
    dyda0 = 1;
    dyda1 = tmp;
    dydl = -tmp*a1*t;
    dyda[1] = dyda0;
    dyda[2] = dyda1;
    dyda[3] = dydl;
}

int	do_fit_Y_equal_a0_plus_a1_exp_moins_lt(void)
{
    int i;
    char st[2048] = {0};
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *dsi = NULL, *dsd = NULL;
    float x, xmin, xmax, tmp;
    int nf = 1024;
    static float error = .1;
    static float a0 = 0, a1 = 1;
    static float l = .02, x_val = 0;
    static int flag = 1, ia0 = 1, ia1 = 1, il = 1, nder = 0, push_grabbing = 0;
    float a[3] = {0}, chisq, chisq0, alamda, *sig, **covar, **alpha;
    int ia[3] = {0};
    //float gammq(float a, float x);

    if(updating_menu_state != 0)	return D_O_K;
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine fits an exponential to a data set\n"
                             "using a Levenberg Marquart algorithm\n"
                             "\nf(t) = a_0 + a_1 exp(-lt)\n"
                             "the parameter a_0, a_1, l may either be imposed or fitted\n"
                             "you must provide good guessed values of these parameters\n"
                             "otherwise this routine is lickly to crash the program\n"
                             "special thanks to Numerical Reciepe...");
    }


    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)	return 1;
    for (i = 0, xmin = xmax = dsi->xd[0]; i < dsi->nx; i++)
    {
        xmin = (dsi->xd[i] < xmin) ? dsi->xd[i] : xmin;
        xmax = (dsi->xd[i] > xmax) ? dsi->xd[i] : xmax;
    }
    if (dsi->ye == NULL)
    {
        i = win_scanf("Fit a_0, a_1, l, in f(t) = a_0 + a_1 exp(-lt) \n \n"
                      "guessed values of a_0 =%8fa_1 =%8f l = %8f\nerror type, const -> 0 "
                      "relative 1 other data set 2%derror %f\n"
		      "fit a_0 %b, a_1 %b l %b (or impose)\n"
		      "Push \\tau = 1/l in grabbing plot %b\n"
		      ,&a0,&a1,&l,
                      &flag,&error,&ia0, &ia1, &il,&push_grabbing);
        if (i == WIN_CANCEL)	return OFF;
    }
    else
    {
        i = win_scanf("Fit a_0, a_1, l, in f(t) = a_0 + a_1 exp(-lt) \n \n"
                      "first guessed value:\n a_0 =%8f a_1 =%8fl =%8f\n"
                      "\n{\\color{yellow}error is taken from error bars}\n"
                      "Paramer a_0 (%R->imposed or %r->fitted)\n"
                      "Paramer a_1 (%R->imposed or %r->fitted)\n"
                      "Paramer a_2 (%R->imposed or %r->fitted)\n"
		      "Push \\tau = 1/l in grabbing plot %b\n"
                      ,&a0,&a1,&l,
                      &ia0, &ia1, &il,&push_grabbing);
        if (i == WIN_CANCEL)	return OFF;
    }
    ia[0] = ia0;
    ia[1] = ia1;
    ia[2] = il;
    a[0] = (a0 - op->ay)/op->dy;
    a[1] = a1/op->dy;
    a[2] = l*op->dx;
    covar = matrix(1, 3, 1, 3);
    alpha = matrix(1, 3, 1, 3);
    sig = vector(1, dsi->nx);
    if (dsi->ye == NULL)
    {
        for (i = 1; i <= dsi->nx; i++)
            sig[i] = (flag) ? dsi->yd[i-1] * error : error/op->dy;
        if (flag == 2)
        {
            nder = op->cur_dat+1;
            i = win_scanf("enter index of error data set %d",&nder);
            if (i == WIN_CANCEL)	return OFF;
            if (op->dat[nder]->nx != dsi->nx)
                return win_printf_OK("your data sets differs in size!");
            for (i = 1; i <= dsi->nx; i++)
                sig[i] =  op->dat[nder]->yd[i-1];
        }
    }
    else
    {
        for (i = 1; i <= dsi->nx; i++)
            sig[i] = dsi->ye[i-1];
    }
    alamda = -1;
    mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, a, ia,3, covar, alpha, &chisq,
           Y_equal_a0_plus_a1_exp_moins_lt_func, &alamda);
    chisq0 = chisq;
    mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, a, ia,3, covar, alpha, &chisq,
           Y_equal_a0_plus_a1_exp_moins_lt_func, &alamda);

    for (i = 0 ; (chisq0 - chisq <= 0) || (chisq0 - chisq > 1e-2); i++)
    {
        chisq0 = chisq;
        mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, a, ia,3, covar, alpha,
               &chisq, Y_equal_a0_plus_a1_exp_moins_lt_func, &alamda);
        if (i >= 10000) break;
        if (alamda > 1e12) break;
    }

    alamda = 0;
    mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, a, ia,3, covar, alpha, &chisq,
           Y_equal_a0_plus_a1_exp_moins_lt_func, &alamda);
    /*
        i = win_printf("%d iter a_0 = %g,\n a_1 = %g l = %g the \\chi^2  is %g\n"
        " covar \\xi \\xi %g \\xi L %g\nL \\xi %g LL %g\ncurvature \\xi \\xi %g"
        " \\xi L %g\n L \\xi %g LL %g",i,op->dy*a[0],op->dy*a[1],a[2]/op->dx,chisq,
        covar[1][1],covar[1][2],covar[2][1],covar[2][2],alpha[1][1],alpha[1][2],
        alpha[2][1],alpha[2][2]);
        */
    a0 = a[0];
    a1 = a[1];
    l = a[2];
    if (i == WIN_CANCEL)	return OFF;
    if (dsi->nx > 256)	nf = dsi->nx;
    if ((dsd = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("cannot create data set");
    if (dsi->nx > 256)
    {
        for (i = 0; i < dsd->nx && i < dsi->nx; i++)
        {
            x = dsi->xd[i];
            dsd->xd[i] = dsi->xd[i];
            dsd->yd[i] = Y_equal_a0_plus_a1_exp_moins_lt( x, a0, a1, l);
        }
    }
    else
    {
        tmp = xmin - 0.1*(xmax - xmin);
        xmax = xmax + 0.1*(xmax - xmin);
        xmin = tmp;
        for (i = 0; i < dsd->nx; i++)
        {
            x = xmin + i*(xmax-xmin)/dsd->nx;
            dsd->xd[i] = x;
            dsd->yd[i] = Y_equal_a0_plus_a1_exp_moins_lt( x, a0, a1, l);
        }
    }
    sprintf(st,"\\fbox{\\pt8\\stack{{\\sl f(t) = a_0 + a_1 exp(-lt)}"
            "{a_0 = %g \\stack{+-} %g}{a1 = %g \\stack{+-} %g}{l = %g \\stack{+-} %g}"
            "{\\tau  = %g %s \\stack{+-} %g %%}{\\chi^2 = %g n = %d}{p = %g}}}",
            op->ay+(op->dy*a0),op->dy*sqrt(covar[1][1]),op->dy*a1,
            op->dy*sqrt(covar[2][2]),l/op->dx,sqrt(covar[3][3])/op->dx,op->dx/l,
            (op->x_unit) ? op->x_unit:" ",100*sqrt(covar[3][3])/l,chisq,dsi->nx-3,
            gammq((float)(dsi->nx-3)/2, chisq/2));
    push_plot_label(op, op->x_lo + (op->x_hi - op->x_lo)/4,
                    op->y_hi - (op->y_hi - op->y_lo)/4, st, USR_COORD);
    if (push_grabbing && (ds_grabbing != NULL))
      {
	i = win_scanf("adding \\tau value to grabbing plot\n"
		      "specify the x value of the point %8f\n",&x_val);
	if (i != WIN_CANCEL)
	  add_new_point_with_y_error_to_ds(ds_grabbing, x_val, op->dx/l, op->dx*sqrt(covar[3][3])/(l*l));
      }
    a0 = op->ay+(op->dy*a0);
    a1 = a1*op->dy;
    l = l/op->dx;
    free_vector(sig, 1, dsi->nx);
    free_matrix(alpha, 1, 3, 1, 3);
    free_matrix(covar, 1, 3, 1, 3);
    return refresh_plot(pr,pr->cur_op);
}




int do_fit_double_exponential_bootstrap(void)
{

  O_p *op = NULL, *opdebug = NULL;
  d_s *ds=NULL, *dst1 = NULL, *dst0 = NULL, *dsa = NULL;
  pltreg *pr = NULL;
  int *famille = NULL;
  static float t0=0,t1=0,a=0.5,t0n,t1n,an,freq, mean = 0;
  static int debug = 0, use_frq_unit_set = 1;
  static int iter_max=1000;
  int n_iter = 0;
  int i = 0;
  float urne,term0,term1,proba0;
    if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)	return 1;

  for (int jj =0;jj<ds->nx;jj++)
  {
    mean+=ds->yd[jj]/ds->nx;
  }

  if (t0==0) t0 = mean/2.0;
  if (t1==0) t1 = mean*2.0;

  i = win_scanf("First time approximation %6f\n"
                "Second time approximation %6f\n"
                "Proportion of first time %6f\n"
                "Debug %b\n"
                "N iterations %6d\n"
                "%b Use frequency from unit set \n"
                "If not enter the frequency of acquisition %6f\n",&t0,&t1,&a,&debug,&iter_max,&use_frq_unit_set,&freq);
  if (i==WIN_CANCEL) return 0;

  if (use_frq_unit_set)
  {
    freq = 1/op->yu[op->c_yu]->dx;
    win_printf_OK("Read frequency is %6f\n",freq);
  }

  if (debug)
  {
    opdebug = create_and_attach_one_plot(pr,64,64,0);
    dst0 = opdebug->dat[0];
    inherit_from_ds_to_ds(dst0,ds);
    inherit_from_ds_to_ds(dst1,ds);
    inherit_from_ds_to_ds(dsa,ds);
    set_ds_treatement(dst0,"first time convergence");
    set_ds_treatement(dst1,"second time convergence");
    set_ds_treatement(dsa,"propotion convergence");
    dst1 = create_and_attach_one_ds(opdebug,64,64,0);
    dsa = create_and_attach_one_ds(opdebug,64,64,0);
    set_plot_title(opdebug,"convergence check of the two modes algorithm");
    set_plot_x_title(opdebug,"steps");
    set_plot_y_title(opdebug,"relative convegence");
    dst1->nx = dst0->ny = dst1->ny = dst0->nx = dsa->nx = dsa->ny = 0;
  }




  srand(time(NULL));
  famille = calloc(ds->nx,sizeof(int));
  for (n_iter=0;n_iter<iter_max;n_iter++)
  {
  an=0;
  t0n=0;
  t1n=0;
  for (i=0;i<ds->nx;i++)
  {
    term0 = (a/t0)*exp(-ds->yd[i]/t0);
    term1 = ((1-a)/t1)*exp(-ds->yd[i]/t1);
    proba0 = term0/(term0+term1);
    urne=rand()/(float)RAND_MAX;
    if (urne<proba0)
    {
      an+=1.0/ds->nx;
      t0n+=ds->yd[i];
    }
    else
    {
      t1n+=ds->yd[i];
    }
  }
  if (an != 0) t0n /= an*ds->nx;

  if (an != 1) t1n /= (1-an)*ds->nx;

  t0 = t0n;
  t1 = t1n;
  a=an;
  if (debug)
  {
    add_new_point_to_ds(dst0,n_iter,t0);
    add_new_point_to_ds(dst1,n_iter,t1);
    add_new_point_to_ds(dsa,n_iter,a);

  }
  }

  if (debug)
  {
    for (int j = 0;j<dst0->nx;j++)
    {
      dst0->yd[j] /= dst0->yd[dst0->nx-1];
      dst1->yd[j] /= dst1->yd[dst1->nx-1];
      dsa->yd[j] /= dsa->yd[dsa->nx-1];
    }
  }

  win_printf_OK("T0 : %6f, T1 : %6f , a : %6f\n",t0/freq,t1/freq,a);
  if (famille) free(famille);
  return 0;


}


int fit_double_exponential(pltreg *pr, O_p *op, d_s *dsi, float t0, float t1, float a, int debug, int iter_max, int use_freq_unit_set, float freq, float low_cut_off, float high_cut_off, float bin0, float bin1, int recompute)
{

  O_p *opdebug = NULL, *ophistos=NULL;
  d_s *dst1 = NULL, *dst0 = NULL, *dsa = NULL, *dshist1 = NULL, *dshist2 = NULL, *ds = NULL;
  static float t0n,t1n,an, weight0 = 0, weight1 =0 ;
  float mean = 0;

  int n_iter = 0;
  int i = 0;
  float term0,term1,proba0;// urne
  float *probas = NULL;
  int *exclude = NULL;
  float likelihood_mono = 0, likelihood_bi = 0, lr =0;
  int pt_nb = 0;
  int low_cut = 1, high_cut = 1;

  (void)use_freq_unit_set;
  (void)recompute;
    ds = duplicate_data_set(dsi,NULL);
    probas = calloc(ds->nx,sizeof(float));
    exclude = calloc(ds->nx,sizeof(int));
    if (!probas) return(win_printf_OK("I can't allocate data\n"));
    if (debug)
    {
      opdebug = create_and_attach_one_plot(pr,64,64,INSERT_HERE);
      dst0 = opdebug->dat[0];
      inherit_from_ds_to_ds(dst0,dsi);
      inherit_from_ds_to_ds(dst1,dsi);
      inherit_from_ds_to_ds(dsa,dsi);
      set_ds_treatement(dst0,"first time convergence");
      set_ds_treatement(dst1,"second time convergence");
      set_ds_treatement(dsa,"propotion convergence");
      dst1 = create_and_attach_one_ds(opdebug,64,64,0);
      dsa = create_and_attach_one_ds(opdebug,64,64,0);
      set_plot_title(opdebug,"convergence check of the two modes algorithm");
      set_plot_x_title(opdebug,"steps");
      set_plot_y_title(opdebug,"relative convegence");
      dst1->nx = dst0->ny = dst1->ny = dst0->nx = dsa->nx = dsa->ny = 0;
    }


      ophistos = create_and_attach_one_plot(pr,64,64,INSERT_HERE);

      set_plot_title(ophistos,"%s",op->title);
      set_plot_x_title(ophistos,"time");
      set_plot_y_title(ophistos,"number of points");
      create_attach_select_x_un_to_op(ophistos, IS_SECOND, 0
                                      , (float)1 /freq, 0, 0, "s");

    if (low_cut_off < 0 ) low_cut = 0;
    if (high_cut_off < 0 ) high_cut = 0;

    for (i=0,pt_nb = 0;i<ds->nx;i++)
    {

      if (((ds->yd[i] < low_cut_off) && low_cut) || (high_cut && (ds->yd[i] > high_cut_off)))
      {
        probas[i] = 0;
        exclude[i] = 1;
        continue;
      }

      if (low_cut) ds->yd[i] -= low_cut_off;
    }


    for (n_iter=0;n_iter<iter_max;n_iter++)
    {
    an=0;
    t0n=0;
    t1n=0;
    weight0 = weight1 = 0;
    pt_nb = 0;
    mean = 0;

    for (i=0,pt_nb = 0;i<ds->nx;i++)
    {
      if (exclude[i]) continue;
      mean += ds->yd[i];
      pt_nb += 1;
      term0 = (a/t0)*exp(-(ds->yd[i])/t0);
      term1 = ((1-a)/t1)*exp(-(ds->yd[i])/t1);
      proba0 = term0/(term0+term1);
      probas[i] = proba0;
      weight0+=proba0;
      weight1+=1-proba0;
      t0n+=ds->yd[i]*proba0;
      t1n+=ds->yd[i]*(1-proba0);
    }

    t0n/=weight0;
    t1n/=weight1;




    t0 = t0n;
    t1 = t1n;

    a = weight0/pt_nb;
    if (debug)
    {
      add_new_point_to_ds(dst0,n_iter,t0);
      add_new_point_to_ds(dst1,n_iter,t1);
      add_new_point_to_ds(dsa,n_iter,a);

    }
    }

    mean/= pt_nb;



    if (debug)
    {
      for (int j = 0;j<dst0->nx;j++)
      {
        dst0->yd[j] /= dst0->yd[dst0->nx-1];
        dst1->yd[j] /= dst1->yd[dst1->nx-1];
        dsa->yd[j]  /= dsa->yd[dsa->nx-1];
      }
    }

    if (bin0 == 0)  bin0 = t0 / 2;
    dshist1 = histo_background(dsi,bin0,probas);
    set_ds_line_color(dshist1,Lightred);
    add_ds_to_op(ophistos,dshist1);

    for (int jjj=0;jjj<ds->nx;jjj++)
    {
      if (exclude[jjj] == 0)  probas[jjj] = 1 - probas[jjj] ;
    }

    if (bin1 == 0)  bin1 = t1 / 2;
    dshist2 = histo_background(dsi,bin1,probas);
    set_ds_line_color(dshist2,lightgreen);
    add_ds_to_op(ophistos,dshist2);
    remove_one_plot_data(ophistos, IS_DATA_SET, op->dat[0]);



    likelihood_mono = likelihood_bi = 0;

    for (int iji = 0;iji<ds->nx;iji++)
    {
      likelihood_mono -= ds->yd[iji] * (1-exclude[iji]) /mean;
      likelihood_mono += log(1/mean) * (1-exclude[iji]);
      likelihood_bi += (1-exclude[iji]) * log((a/t0)*exp(-ds->yd[iji]/t0) + ((1-a)/t1)*exp(-ds->yd[iji]/t1));
    }


    lr = 2 * (likelihood_bi - likelihood_mono);

    if (low_cut)
    {
      a = (a*exp(-low_cut_off/t1)) / ((1-a)*exp(-low_cut_off/t0) + a*exp(-low_cut_off/t1));
    }


    if (ophistos != NULL)
    {
        ophistos->user_vspare[0] = (void *) dsi;
        ophistos->user_fspare[0] = t0;
        ophistos->user_fspare[1] = t1;
        ophistos->user_fspare[2] = a;
        ophistos->user_fspare[3] = freq;
        ophistos->user_ispare[0] = 47891;
        ophistos->user_fspare[4] = bin0;
        ophistos->user_fspare[5] = bin1;
        ophistos->user_fspare[6] = low_cut_off;
        ophistos->user_fspare[7] = high_cut_off;


        refresh_plot(pr,pr->cur_op );
        char st[4096];
        sprintf(st,"\\fbox{\\pt8\\stack{{\\sl y = A * (\\alpha.exp(-t/\\tau_0) + (1-\\alpha).exp(-t/\\tau_1))}"
                "{\\alpha =  %g}{\\tau_0  = %g%s}{\\tau_1  = %g%s}"
                "{LR = %g}{low cut off = %g}{high cut off = %g}}}"
                ,a,t0/freq
                ,(ophistos->x_unit) ? ophistos->x_unit:"",t1/freq,(ophistos->x_unit) ? ophistos->x_unit:"",lr,low_cut_off/freq,high_cut_off/freq);
        set_ds_plot_label(dshist1, ophistos->x_lo + (ophistos->x_hi - ophistos->x_lo)/4,
                          ophistos->y_hi - (ophistos->y_hi - ophistos->y_lo)/4, USR_COORD,"%s",st);
                          refresh_plot(pr,pr->cur_op);

    }

  else win_printf_OK("T0 : %6f, T1 : %6f , a : %6f, lr : %6f\n",t0/freq,t1/freq,a,lr);
  free_data_set(ds);

  return 0;
}

int fit_double_gaussian(pltreg *pr, O_p *op, d_s *ds, float t0, float t1, float sigma0, float sigma1, float a, int debug, int plot, int iter_max, float threshold, float bin0, float bin1, float* aret, int start, int end, float *probas_ret)
{

  O_p *opdebug = NULL, *ophistos=NULL;
  d_s *dst1 = NULL, *dst0 = NULL, *dsa = NULL, *dshist1 = NULL, *dshist2 = NULL, *dsdown = NULL, *dsup = NULL;
  static float t0n,t1n,sigma0n,sigma1n,an, weight0 = 0, weight1 =0 ;
  float mean = 0;
  int invert = 0;
  int n_iter = 0;
  int i = 0;
  float term0,term1,proba0;// urne
  float *probas = NULL;
  int *exclude = NULL;
  float likelihood_mono = 0, likelihood_bi = 0, lr =0;
  int pt_nb = 0;

  int np = end-start +1;
  printf("np %d\n",np);
  probas = calloc(np,sizeof(float));



    // probas = calloc(np,sizeof(float));
    // exclude = calloc(np,sizeof(int));
    // if (!probas) return(win_printf_OK("I can't allocate data\n"));
    // if (debug)
    // {
    //   opdebug = create_and_attach_one_plot(pr,64,64,INSERT_HERE);
    //   dst0 = opdebug->dat[0];
    //   inherit_from_ds_to_ds(dst0,dsi);
    //   inherit_from_ds_to_ds(dst1,dsi);
    //   inherit_from_ds_to_ds(dsa,dsi);
    //   set_ds_treatement(dst0,"first time convergence");
    //   set_ds_treatement(dst1,"second time convergence");
    //   set_ds_treatement(dsa,"propotion convergence");
    //   dst1 = create_and_attach_one_ds(opdebug,64,64,0);
    //   dsa = create_and_attach_one_ds(opdebug,64,64,0);
    //   set_plot_title(opdebug,"convergence check of the two modes algorithm");
    //   set_plot_x_title(opdebug,"steps");
    //   set_plot_y_title(opdebug,"relative convegence");
    //   dst1->nx = dst0->ny = dst1->ny = dst0->nx = dsa->nx = dsa->ny = 0;
    // }

if (plot == 1){
      ophistos = create_and_attach_one_plot(pr,64,64,INSERT_HERE);

      set_plot_title(ophistos,"%s",op->title);
      set_plot_x_title(ophistos,"time");
      set_plot_y_title(ophistos,"number of points");
      create_attach_select_x_un_to_op(ophistos, IS_METER, 0
                                      , (float)1 , -6, 0, "um");
                                    }
              if (plot == 2){
                                          ophistos = create_and_attach_one_plot(pr,64,64,INSERT_HERE);

                                          set_plot_title(ophistos,"%s",op->title);
                                          set_plot_x_title(ophistos,op->x_title);
                                          set_plot_y_title(ophistos,op->y_title);
                                          uns_op_2_op(ophistos, op);

                                                                        }


    int converged = 0;
    for (n_iter=0;(n_iter<iter_max) && (converged == 0);n_iter++)
    {
    an=0;
    t0n=0;
    t1n=0;
    weight0 = weight1 = 0;
    pt_nb = 0;
    sigma0n = 0;
    sigma1n = 0;

    for (i=start,pt_nb = 0;i<start+np;i++)
    {

      pt_nb += 1;
      term0 = (a/sqrt(2*PI*sigma0))*exp(-(ds->yd[i]-t0)*(ds->yd[i]-t0)/(2*sigma0));
      term1 = ((1-a)/sqrt(2*PI*sigma1))*exp(-(ds->yd[i]-t1)*(ds->yd[i]-t1)/(2*sigma1));
    //  printf("term0 %6f term1 %6f\n",term0,term1);

      if (term0 ==0 && term1 == 0) proba0 = 1;
      else   proba0 = term0/(term0+term1);
      probas[i-start] = proba0;


      weight0+=proba0;
      weight1+=1-proba0;
      t0n+=ds->yd[i]*proba0;
      sigma0n+=ds->yd[i]*ds->yd[i]*proba0;
      sigma1n+=ds->yd[i]*ds->yd[i]*(1-proba0);
      t1n+=ds->yd[i]*(1-proba0);

    }
  //  printf("weight0 %6f weight1 %6f\n",weight0,weight1);
  //  return 0;
    t0n/=weight0;
    t1n/=weight1;
    sigma0n = (sigma0n/weight0) -t0n*t0n;
    sigma1n = (sigma1n/weight1) -t1n*t1n;



    if ((fabs(sigma0n-sigma0)<threshold*fabs(sigma0)) && (fabs(t1n-t1)<threshold*fabs(t1)) && (fabs(t0n-t0)<threshold*fabs(t0)) && (fabs(sigma1n-sigma1)<threshold*fabs(sigma1))) converged = 1;

    t0 = t0n;
    t1 = t1n;
    sigma0 = sigma0n;
    sigma1 = sigma1n;

    a = weight0/pt_nb;
    if (debug)
    {
      add_new_point_to_ds(dst0,n_iter,t0);
      add_new_point_to_ds(dst1,n_iter,t1);
      add_new_point_to_ds(dsa,n_iter,a);

    }
    }




    if (debug)
    {
      for (int j = 0;j<dst0->nx;j++)
      {
        dst0->yd[j] /= dst0->yd[dst0->nx-1];
        dst1->yd[j] /= dst1->yd[dst1->nx-1];
        dsa->yd[j]  /= dsa->yd[dsa->nx-1];
      }
    }

    if (plot == 1)
    {

    if (bin0 == 0)  bin0 = sigma0 / 4;
    printf("bin0 = %f\n",bin0);
    dshist1 = histo_background(ds,bin0,probas);
    set_ds_line_color(dshist1,Lightred);
    add_ds_to_op(ophistos,dshist1);

    for (i=start,pt_nb = 0;i<start+np;i++) probas[i-start] = 1 - probas[i-start];

    if (bin1 == 0)  bin1 = sigma1 / 4;
    printf("bin1 = %f\n",bin1);
    dshist2 = histo_background(ds,bin1,probas);
    set_ds_line_color(dshist2,lightgreen);
    add_ds_to_op(ophistos,dshist2);
    remove_one_plot_data(ophistos, IS_DATA_SET, op->dat[0]);

    for (i=start,pt_nb = 0;i<start+np;i++) probas[i-start] = 1 - probas[i-start];

  }

  if (plot == 2)
  {

  dsdown = ophistos->dat[0];
  dsup = create_and_attach_one_ds(ophistos,16,16,0);
  dsdown->nx = dsdown->ny = dsup->nx = dsup->ny = 0;
  if (t0<t1) invert = 0;
  else invert = 1;


  for (i=start,pt_nb = 0;i<start+np;i++)
  {
      if (probas[i-start] > 0.5)
      {
        if (invert == 0) add_new_point_to_ds(dsdown,ds->xd[i],0);
        if (invert == 1) add_new_point_to_ds(dsup,ds->xd[i],1);
      }
      else
      {
        if (invert == 1) add_new_point_to_ds(dsdown,ds->xd[i],0);
        if (invert == 0) add_new_point_to_ds(dsup,ds->xd[i],1);
      }
  }

}

    if (t0<t1)
    {
      *aret = a;
      if (probas_ret != NULL)
      {
      for (i=start,pt_nb = 0;i<start+np;i++) probas_ret[i-start] = probas[i-start];
    }



    }
    else
    {
      *aret = 1-a;
      if (probas_ret != NULL)
      {
      for (i=start,pt_nb = 0;i<start+np;i++) probas_ret[i-start] = 1- probas[i-start];
    }


    }

    if (plot)
    {
        ophistos->user_vspare[0] = (void *) ds;
        ophistos->user_fspare[0] = t0;
        ophistos->user_fspare[1] = t1;
        ophistos->user_fspare[2] = a;
        ophistos->user_ispare[0] = 47894;
        ophistos->user_fspare[4] = bin0;
        ophistos->user_fspare[5] = bin1;
        ophistos->user_fspare[6] = sigma0;
        ophistos->user_fspare[7] = sigma1;


        refresh_plot(pr,pr->cur_op );
        char st[4096];
        sprintf(st,"\\fbox{\\pt8\\stack{{\\alpha =  %g}{\\mu_0  = %g%s}{\\mu_1  = %g%s}"
                "{LR = %g}{sigma0 = %g}{sigma1 = %g}}}"
                ,a,t0
                ,(ophistos->x_unit) ? ophistos->x_unit:"",t1,(ophistos->x_unit) ? ophistos->x_unit:"",lr,sqrt(sigma0),sqrt(sigma1));
        set_ds_plot_label(dshist1, ophistos->x_lo + (ophistos->x_hi - ophistos->x_lo)/4,
                          ophistos->y_hi - (ophistos->y_hi - ophistos->y_lo)/4, USR_COORD,"%s",st);
                          refresh_plot(pr,pr->cur_op);

    }



  return 0;
}

O_p *fit_Poisson_n_mixture(pltreg *pr, O_p *op, d_s *dsi, int n, float* t, float* a,d_s *no_falls, int debug, float convergence_criterium, float freq, float low_cut_off, float high_cut_off, float *bin, int recompute, int displayhistos, float* likelihood, float **terms)
{
  O_p *opdebugt = NULL, *opdebuga = NULL,*ophistos=NULL;
  d_s **dst = NULL, **dsa = NULL, *ds = NULL; // , *dshist2 = NULL, *dshist1 = NULL
  //static float t0n,t1n,an, weight0 = 0, weight1 =0 ;
  //float mean = 0;

  int converged = 0;
  float rel_change = 0;

  (void)recompute;
  (void)low_cut_off;
  (void)high_cut_off;
  if (n < 1) return(win_printf_ptr("n trop petit\n"));
  dst = calloc(n,sizeof(*ds));
  dsa = calloc(n,sizeof(*ds));
  int iter_max = 10000;
  int c_iter = 0;
  int i = 0;
  //float urne,term0,term1,proba0;
  //float *probas = NULL;
  //int *exclude = NULL;
  //float likelihood_mono = 0, likelihood_bi = 0, lr =0;
  float sum_terms = 0;
  //int pt_nb = 0;
  float a_temp[n];
  float to_sum = 0;
  d_s **dshist = NULL;
  //int low_cut = 1, high_cut = 1;
  float term = 0;
  int k_max = 0;
  float max_t = 0;
  ds = duplicate_data_set(dsi,NULL);
  opdebugt = create_and_attach_one_plot(pr,64,64,INSERT_HERE);
  opdebuga = create_and_attach_one_plot(pr,64,64,INSERT_HERE);
  dst[0] = opdebugt->dat[0];
  dsa[0] = opdebuga->dat[0];
  set_ds_treatement(dst[0],"time %d convergence",0);
  set_ds_treatement(dsa[0],"time %d conveagence",0);
  inherit_from_ds_to_ds(dst[0],dsi);
  inherit_from_ds_to_ds(dsa[0],dsi);
  dst[0]->nx = dsa[0]->nx = dst[0]->ny = dsa[0]->ny = 0;
  for (int ii=1;ii<n;ii++)
      {
          dst[ii] = create_and_attach_one_ds(opdebugt,64,64,0);
          dsa[ii] = create_and_attach_one_ds(opdebuga,64,64,0);
          set_ds_treatement(dst[ii],"time %d convergence",ii);
          set_ds_treatement(dsa[ii],"time %d conveagence",ii);
          inherit_from_ds_to_ds(dst[ii],dsi);
          inherit_from_ds_to_ds(dsa[ii],dsi);
          dst[ii]->nx = dsa[ii]->nx = dst[ii]->ny = dsa[ii]->ny = 0;
      }

      set_plot_title(opdebugt,"convergence check of the times");
      set_plot_title(opdebuga,"convergence check of the ratio");
      set_plot_x_title(opdebugt,"steps");
      set_plot_y_title(opdebugt,"relative convegence");
      set_plot_x_title(opdebuga,"steps");
      set_plot_y_title(opdebuga,"relative convegence");


    if (displayhistos)
    {
      ophistos = create_and_attach_one_plot(pr,64,64,INSERT_HERE);

      set_plot_title(ophistos,"%s",op->title);
      set_plot_x_title(ophistos,"time");
      set_plot_y_title(ophistos,"number of points");
      create_attach_select_x_un_to_op(ophistos, IS_SECOND, 0
                                      , (float)1 /freq, 0, 0, "s");
      dshist = calloc(n,sizeof(d_s*));
    }

    //if (low_cut_off < 0 ) low_cut = 0;
    //if (high_cut_off < 0 ) high_cut = 0;







    while (converged == 0 && c_iter<iter_max)
    {

    for (i=0;i<ds->nx;i++)
    {
      sum_terms=0;

      for (int ii = 0; ii<n; ii++)
      {
        if (no_falls->yd[i]) terms[ii][i] = (a[ii])*exp(-(ds->yd[i])/t[ii]);
        else terms[ii][i] = (a[ii]/t[ii])*exp(-(ds->yd[i])/t[ii]);
        if (isnan(terms[ii][i])) terms[ii][i] = 0;
        sum_terms += terms[ii][i];
      }
      for (int ii = 0; ii<n; ii++)
      {
        if (sum_terms == 0)
        {
          max_t = 0;
          for (int k =0;k<n;k++)
          {
            if (t[k] > max_t)
            {
              k_max = k;
              max_t=t[k];
           }

          }
          terms[k_max][i] = 1.0;
        }
        else terms[ii][i] /= sum_terms;

      }
    }

    for (int ii = 0; ii<n; ii++)
    {
      t[ii] = 0;
      a[ii] = 0;
      a_temp[ii] = 0;
        for (i=0;i<ds->nx;i++)
        {
      t[ii] += ds->yd[i] * terms[ii][i];
      a[ii] += terms[ii][i];
      a_temp[ii] += terms[ii][i] * (1-no_falls->yd[i]);

      }
    t[ii] /= a_temp[ii];
    a[ii] /= ds->nx;
    }



      converged = 1;
      for (int ii = 0; ii<n; ii++)
      {
        if (converged && c_iter > 0)
        {
      rel_change = fabs(t[ii] - dst[ii]->yd[dst[ii]->nx-1]) / t[ii] ;
      if (rel_change > convergence_criterium) converged = 0;
      rel_change = fabs(a[ii] - dsa[ii]->yd[dsa[ii]->nx-1]) / a[ii] ;
      if (rel_change > convergence_criterium) converged = 0;
        }


      add_new_point_to_ds(dst[ii],c_iter,t[ii]);
      add_new_point_to_ds(dsa[ii],c_iter,a[ii]);
    }
    if (c_iter == 0) converged = 0;

    c_iter++;
    }




      for (int ii = 0; ii<n; ii++)
      {
      for (int j = 0;j<dst[ii]->nx;j++)
      {
        dst[ii]->yd[j] /= dst[ii]->yd[dst[ii]->nx-1];
        dsa[ii]->yd[j]  /= dsa[ii]->yd[dsa[ii]->nx-1];
      }

   }

 if (displayhistos)
 {

    for (int ii = 0; ii<n;ii++)
    {
      if (isnan(t[ii]) || isinf(t[ii])) continue;
    if (bin[ii] == 0)  bin[ii] = t[ii] / 3.0;
    dshist[ii] = histo_background(ds,bin[ii],terms[ii]);
    if (ii==0) set_ds_line_color(dshist[ii],Lightred);
    if (ii==1) set_ds_line_color(dshist[ii],Lightgreen);
    if (ii==2) set_ds_line_color(dshist[ii],lightyellow);
    if (ii==3) set_ds_line_color(dshist[ii],Lightblue);
    add_ds_to_op(ophistos,dshist[ii]);
  }
}

  remove_one_plot_data(ophistos, IS_DATA_SET, op->dat[0]);



  //
  //   likelihood_mono = likelihood_bi = 0;
  //
  //   for (int iji = 0;iji<ds->nx;iji++)
  //   {
  //     likelihood_mono -= ds->yd[iji] * (1-exclude[iji]) /mean;
  //     likelihood_mono += log(1/mean) * (1-exclude[iji]);
  //     likelihood_bi += (1-exclude[iji]) * log((a/t0)*exp(-ds->yd[iji]/t0) + ((1-a)/t1)*exp(-ds->yd[iji]/t1));
  //   }
  //
  //
  //   lr = 2 * (likelihood_bi - likelihood_mono);
  //
  //   if (low_cut)
  //   {
  //     a = (a*exp(-low_cut_off/t1)) / ((1-a)*exp(-low_cut_off/t0) + a*exp(-low_cut_off/t1));
  //   }
  //
  //
  //   if (ophistos != NULL)
  //   {
  //       ophistos->user_vspare[0] = (void *) dsi;
  //       ophistos->user_fspare[0] = t0;
  //       ophistos->user_fspare[1] = t1;
  //       ophistos->user_fspare[2] = a;
  //       ophistos->user_fspare[3] = freq;
  //       ophistos->user_ispare[0] = 47891;
  //       ophistos->user_fspare[4] = bin0;
  //       ophistos->user_fspare[5] = bin1;
  //       ophistos->user_fspare[6] = low_cut_off;
  //       ophistos->user_fspare[7] = hcigh_cut_off;
  //
  //
  //       refresh_plot(pr,pr->cur_op );
  //       char st[4096];
  //       sprintf(st,"\\fbox{\\pt8\\stack{{\\sl y = A * (\\alpha.exp(-t/\\tau_0) + (1-\\alpha).exp(-t/\\tau_1))}"
  //               "{\\alpha =  %g}{\\tau_0  = %g%s}{\\tau_1  = %g%s}"
  //               "{LR = %g}{low cut off = %g}{high cut off = %g}}}"
  //               ,a,t0/freq
  //               ,(ophistos->x_unit) ? ophistos->x_unit:"",t1/freq,(ophistos->x_unit) ? ophistos->x_unit:"",lr,low_cut_off/freq,high_cut_off/freq);
  //       set_ds_plot_label(dshist1, ophistos->x_lo + (ophistos->x_hi - ophistos->x_lo)/4,
  //                         ophistos->y_hi - (ophistos->y_hi - ophistos->y_lo)/4, USR_COORD,"%s",st);
  //                         refresh_plot(pr,pr->cur_op);
  //
  //   }
  //
  // else win_printf_OK("T0 : %6f, T1 : %6f , a : %6f, lr : %6f\n",t0/freq,t1/freq,a,lr);
  // free_data_set(ds);

  if (debug)
  {
  refresh_plot(pr,pr->n_op-1);
  refresh_plot(pr,pr->n_op-2);
}
  *likelihood = 0;
  for (i = 0; i<ds->nx;i++)
  {
    to_sum = 0;
    for (int ii = 0;ii<n;ii++)
    {
      term = (a[ii]/(no_falls->yd[i]+t[ii]*(1-no_falls->yd[i])))*exp(-ds->yd[i]/t[ii]);
      if (isnan(term)) term = 0;
      to_sum+=term;
    }
    *likelihood+=log(to_sum);

    if (isnan(*likelihood)) return(win_printf_ptr("nan likelihood\n"));
  }
  free_data_set(ds);
  if (!debug)
  {
    remove_data(pr, IS_ONE_PLOT, (void *)opdebuga);
    remove_data(pr, IS_ONE_PLOT, (void *)opdebugt);
  }

  int ind = n-1;
  float max = 0;
  int indmax = 0;
  float *tsorted = calloc(n,sizeof(float));
  float *asorted = calloc(n,sizeof(float));

  while (ind>=0)
  {
    max = 0;
  for (int kk=0;kk<n;kk++)
  {
    if (t[kk] > max)
    {
      max = t[kk];
      indmax = kk;
    }
  }
    tsorted[ind] = t[indmax];
    asorted[ind] = a[indmax];
    t[indmax] = -1;
    ind -= 1;
  }

  for (int kk=0;kk<n;kk++)
  {
    t[kk] = tsorted[kk];
    a[kk] = asorted[kk];

  }

  free(tsorted);
  free(asorted);


  return ophistos;
}

int is_G4_no_falled(O_p *op, d_s *no_falls, d_s *ds)
{

	d_s *ds1 = NULL,*ds2 = NULL,*ds3 = NULL,*ds4 = NULL;
	//float sum_block_times= 0;
	//float sum_g4_lifes = 0;
	//int nb_on_events = 0;
	//int nb_off_events = 0;
	//float frequency;
  char st[4096] = {0};
  int isoligo = 0;

	if (updating_menu_state != 0)
	{

			return D_O_K;
	}

	ac_grep(cur_ac_reg, "%op", &op);
	if (op == NULL) return win_printf_OK("No current op\n");

  no_falls = build_adjust_data_set(no_falls,ds->nx,ds->ny);

	ds1 = find_source_specific_ds_in_op(op,"G4 event off by bead");
	ds2 = find_source_specific_ds_in_op(op,"G4 event on by bead");
	ds3 = find_source_specific_ds_in_op(op,"Oligo blocking time before G4");
	ds4 = find_source_specific_ds_in_op(op,"G4 life time");
	if ((ds1 == NULL) || (ds2 == NULL) || (ds3 == NULL) || (ds4 == NULL)) return win_printf_OK("Please run this function on a valid G4 statistics graph\n");
	if (ds != ds3 && ds != ds4) return(win_printf_OK("You have not entered a valid G4 ds\n"));
  if (ds == ds3) isoligo = 1;
  snprintf(st,4096,isoligo ? "No falled oligo : ":"No falled G4 : ");
	int ind = 0;
	float current_x = -1.345671898765;
  int nb_cur_ds = 0, nb_cur_on = 0, nb_cur_off = 0;
  int safe = 0;
	while (ind <= ds->nx)
	{
    safe = (ind == ds->nx) ? ind-1:ind;
		if ((ind != ds->nx) && (ds->xd[safe] == current_x)) // look if it is the same bead and in this case count the times
		{
			nb_cur_ds +=1;
      ind++;
		}
		else  //if it is a new bead count off and on events
		{
      nb_cur_on = nb_cur_off =0; // put the number of off events and on events to 0  for this bead
			for (int j=0;j<ds1->nx;j++)
			{
				if (ds1->xd[j] == current_x)  //if current bead
				{
					nb_cur_off = ds1->yd[j]; //save number off events
					continue;
				}
			}
			for (int j=0;j<ds2->nx;j++)
			{
				if (ds2->xd[j] == current_x)
				{
					nb_cur_on = ds2->yd[j]; //save numbr on evnts
					continue;
				}
			}

      if (isoligo == 0) //if it is lifetime
      {
			if (nb_cur_ds == nb_cur_off +1) //si il y a un temps de plus, dernier g4 n'est pas tombé
			{
				no_falls->yd[ind-1] = 1;
        snprintf(st+strlen(st),sizeof(st)-strlen(st),",%4f",current_x);
			}
			else if (nb_cur_ds != nb_cur_off) //if different poblem to signal
			{
				win_printf_OK("Pb nb cur %f,%d,%d,%d\n",current_x,nb_cur_on,nb_cur_ds,nb_cur_off);
			}
    }

    else // si oligo blocking
{
  if (nb_cur_ds == nb_cur_on +1) //si un temps de plus
  {
    no_falls->yd[ind-1] = 1;
    snprintf(st+strlen(st),sizeof(st)-strlen(st),",%4f",current_x);
  }
  else if (nb_cur_ds != nb_cur_on) // sinon problème
  {
    win_printf_OK("Pb nb cur %f,%d,%d,%d\n",current_x,nb_cur_on,nb_cur_ds,nb_cur_off);
  }
}
		if (ind != ds->nx)	current_x = ds->xd[ind]; // on change le nouvel x
			nb_cur_ds= 1; // on a un temps pour ce nouvel x
			ind+=1; // on passe au point suivant dans le ds des temps
		}
	}



    win_printf_OK("%s",st);
	return 0;

}




int do_fit_N_poisson_mixture(void)
{
  O_p *op = NULL;
  d_s *ds=NULL;
  pltreg *pr = NULL;
  int i =0;
  float freq = 0;


  float *t = NULL, *a = NULL;
  int n = 2;
  static int debug = 1, use_frq_unit_set = 1;
  float mean =0;
  float convergence_criterium = 1e-4;
  static int take_no_falled = 0;
  d_s *no_falls = NULL;
  float likelihood = 0;
  float *bin;
  float **terms = NULL;


  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)	return 1;

  for (int jj =0;jj<ds->nx;jj++)
  {
    mean+=ds->yd[jj]/ds->nx;
  }



  i = win_scanf("Enter the number of Poisson law to look for %d\n"
                "Use freq unit set %b\n"
                "Conv criterium %f \n"
                "Take in account no falled G4 %b \n",&n,&use_frq_unit_set,&convergence_criterium,&take_no_falled);
  if (i==WIN_CANCEL) return 0;

  t = calloc(n,sizeof(float));
  a = calloc(n,sizeof(float));
  for (int ii=0;ii<n;ii++)
  {
    t[ii] = 2*(ii+1)*mean/n;
    a[ii] = 1.0/n;
  }

  no_falls = build_data_set(ds->nx,ds->ny);
  if (take_no_falled)
  {
    is_G4_no_falled(op,no_falls,ds);
  }

  if (use_frq_unit_set)
  {
    freq = 1/op->yu[op->c_yu]->dx;
    win_printf_OK("Read frequency is %6f\n",freq);
  }
  bin = calloc(n,sizeof(bin));
  terms = calloc(n,sizeof(float*));
  for (i = 0;i<n;i++) terms[i] = calloc(ds->nx,sizeof(float));

  fit_Poisson_n_mixture(pr, op, ds, n, t, a, no_falls, debug, convergence_criterium, freq, 0,0,bin,0,1,&likelihood, terms);
  for (i = 0;i<n;i++) free(terms[i]);
  free_data_set(no_falls);
  free(terms);
  free(a);
  free(t);
  free(bin);

  return 0;
}

int test_Poisson_mixture_fits(pltreg *pr,O_p *op,d_s *dsi, int ntest,int nmax, int take_no_falled, int debug, float convergence_criterium, float freq,int starting_time_mode,float *starting_times, float low_cut_off)
{

    float **t_array = calloc(nmax+1,sizeof(float*));
    float **a_array = calloc(nmax+1,sizeof(float*));
    float *ttemp = calloc(nmax,sizeof(float));
    float *atemp = calloc(nmax,sizeof(float));
    float *tmean = calloc(nmax,sizeof(float));
    float *amean = calloc(nmax,sizeof(float));
    float *tsquare = calloc(nmax,sizeof(float));
    float *asquare = calloc(nmax,sizeof(float));
    float *stdt = calloc(nmax,sizeof(float));
    float *stda = calloc(nmax,sizeof(float));
    float *t,*a;
    d_s *ds;

    float mean = 0;
    d_s* no_falls = NULL;
    d_s *dstemp;
    float likelihoodt1,likelihoodt2;
    float *likelihoods;
    //int *na;
    d_s *dslab = NULL;
    O_p *oplab = NULL;
    char st[4096];
    float pvalue[nmax];
    float likelihood_ratio = 0;
    float **terms = NULL;
    int low_cut =0;

    terms = calloc(nmax,sizeof(float*));
    for (int i = 0;i<nmax;i++) terms[i] = calloc(dsi->nx,sizeof(float));



    no_falls = build_data_set(dsi->nx,dsi->ny);
    likelihoods = calloc(nmax+1,sizeof(float));
    dstemp = build_data_set(dsi->nx+64,dsi->ny+64);

    float *likelihood_ratio_array = calloc(ntest,sizeof(float));
    float *bin=calloc(nmax,sizeof(float));
    if (take_no_falled)
    {
      is_G4_no_falled(op,no_falls,dsi);
    }


    ds = duplicate_data_set(dsi,NULL);
    if (low_cut_off > 0) low_cut =1;
    if (low_cut) {
      for (int k=0;k<ds->nx;)
      {

        if (ds->yd[k] < low_cut_off)
        {
          remove_point_from_data_set(ds,k);
          remove_point_from_data_set(no_falls,k);
        }
        else
        {
          ds->yd[k] -= low_cut_off;
          k++;
        }
      }
    }



    for (int jj =0;jj<ds->nx;jj++)
    {
      mean+=ds->yd[jj]/ds->nx;
    }







    for (int n = 1;n<=nmax;n++)
    {
      for (int ii = 0;ii<n;ii++)
      {
        tsquare[ii] =0;
        asquare[ii] =0;
        tmean[ii] =0;
        amean[ii] =0;
      }


      t_array[n] = calloc(n,sizeof(float));
      a_array[n] = calloc(n,sizeof(float));
      t = t_array[n];
      a = a_array[n];
      for (int ii=0;ii<n;ii++)
      {
        t[ii] = 2*(ii+1)*mean/n;
        a[ii] = 1.0/n;
        if (n==starting_time_mode)   t[ii] = starting_times[ii]*freq;
      }

      for (int ii=0;ii<nmax;ii++)
      {
        bin[ii] = 0;
      }
      for (int k=0;k<n;k++) win_printf("t[%d]=%f a[%d]=%f\n",k,t[k],k,a[k]);
      oplab = fit_Poisson_n_mixture(pr, op, ds, n, t, a, no_falls, debug, convergence_criterium, freq, 0,0,bin,0,1,likelihoods+n,terms);

      t = ttemp;
      a = atemp;

      for (int i =0;i<ntest;i++)
      {
        for (int ii=0;ii<n;ii++)
        {
          t[ii] = t_array[n][ii];
          a[ii] = a_array[n][ii];
        }

        printf("%d",i);


        simulate_n_exponential_mixture(dstemp,n,ds->nx,terms,a_array[n],t_array[n]);

        fit_Poisson_n_mixture(pr, op, dstemp, n, t, a, no_falls, 0, convergence_criterium, freq, 0,0,bin,0,0,&likelihoodt1,terms);


        for (int ii = 0;ii<n;ii++)
        {
          tsquare[ii] += t[ii]*t[ii]/ntest;
          asquare[ii] += a[ii]*a[ii]/ntest;
          tmean[ii] += t[ii]/ntest;
          amean[ii] += a[ii]/ntest;
        }

      if ( n != nmax)
      {

        for (int ii=0;ii<n+1;ii++)
        {
          t[ii] = 2*(ii+1)*mean/(n+1);
          a[ii] = 1.0/(n+1);
          if (n+1==starting_time_mode)   t[ii] = starting_times[ii]*freq;
        }

      fit_Poisson_n_mixture(pr, op, dstemp, n+1, t, a, no_falls, 0, convergence_criterium,  freq, 0,0,bin,0,0,&likelihoodt2,terms);
      likelihood_ratio_array[i] = likelihoodt2 - likelihoodt1;
     }
   }
      if (n != 1)
      {
        pvalue[n] = 0;
        likelihood_ratio = likelihoods[n] - likelihoods[n-1];
        for (int j = 0;j<ntest;j++)
        {
          printf("n = %d, likelihood_ratio = %f,  likelihood_ratio_array[%d] = %f\n",n,likelihood_ratio,j,likelihood_ratio_array[j]);
          if (likelihood_ratio > likelihood_ratio_array[j]) pvalue[n]+=1.0/ntest;
        }

      }


      for (int ii = 0;ii<n;ii++)
      {
        stdt[ii] = sqrt((tsquare[ii]-tmean[ii]*tmean[ii]));
        stda[ii] = sqrt((asquare[ii]-amean[ii]*amean[ii]));
      }


      if (oplab)
      {
      dslab = oplab->dat[0];

      if (dslab != NULL)
      {

        sprintf(st,"\\fbox{\\pt8\\stack{{\\sl y = A * (\\alpha.exp(-t/\\tau_0) + (1-\\alpha).exp(-t/\\tau_1))}");


        for (int ii =0;ii<n;ii++)
        {
          sprintf(st+strlen(st),"{\\tau_%d  = %f +- %f s}",ii,(t_array[n][ii])/freq,stdt[ii]/freq);
          sprintf(st+strlen(st),"{a_%d  = %f +- %f }",ii,a_array[n][ii],stda[ii]);
        }
        sprintf(st+strlen(st),"{Likelihood  = %f}",likelihoods[n]);
        if ( n != 1) sprintf(st+strlen(st),"{pvalue  = %f}",pvalue[n]);
        sprintf(st+strlen(st),"}}");


        refresh_plot(pr,pr->n_op-1);


        set_ds_plot_label(dslab, oplab->x_lo + (oplab->x_hi - oplab->x_lo)/4,
                          oplab->y_hi - (oplab->y_hi - oplab->y_lo)/4, USR_COORD,"%s",st);
        refresh_plot(pr,pr->n_op-1);


      }
    }



    }
    for (int iji = 0;iji<nmax;iji++) free(terms[iji]);
    free(terms);
    free(ttemp);
    free(atemp);
    free(tmean);
    free(amean);
    free(tsquare);
    free(asquare);
    free(stdt);
    free(stda);
    free_data_set(no_falls);
    free(likelihood_ratio_array);
    free(bin);
    free_data_set(dstemp);
    for (int n = 1;n<nmax;n++)
    {
      free(t_array[n]);
      free(a_array[n]);
    }
    free(t_array);
    free(a_array);
    return 0;

}

int do_test_poisson_mixtures(void)
{
  O_p *op = NULL;
  d_s *ds=NULL;
  pltreg *pr = NULL;
  int i =0;
  float freq = 0;



  static int debug = 1, use_frq_unit_set = 1;
  float convergence_criterium = 1e-3;
  static int take_no_falled = 0;
  float likelihood = 0;
  static int nmax = 4;
  static int ntest = 100;
  float starting_times[24] = {0};
  int choose_starting_time = 0;
  int starting_time_mode = 0;
  char str[1024] = {0};
  float low_cut_off = -1;

  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)	return 1;



  i = win_scanf("Enter the max number of Poisson law to look for %d\n"
                  "Enter the number of simulated data%d\n"
                "Use freq unit set %b\n"
                "Conv criterium %f \n"
                "Debug %b\n"
                "Take in account no falled G4 %b \n"
                "Choose starting time for a mode %b\n"
                "Low cut off %f\n",&nmax,&ntest,&use_frq_unit_set,&convergence_criterium,&debug,&take_no_falled,&choose_starting_time,&low_cut_off);

  if (i==WIN_CANCEL) return 0;
  if (choose_starting_time)
  {
    win_scanf("mode for starting times %d\n",&starting_time_mode);
    for (int k=0;k<starting_time_mode;k++)
    {
      snprintf(str,1024,"starting time %d = %%f",k+1);
      win_scanf(str,&starting_times[k]);
    }
  }


  if (use_frq_unit_set)
  {
    freq = 1/op->yu[op->c_yu]->dx;
    win_printf_OK("Read frequency is %6f\n",freq);
  }
  low_cut_off = low_cut_off * freq;
  test_Poisson_mixture_fits(pr,op,ds, ntest,nmax, take_no_falled, debug, convergence_criterium, freq,starting_time_mode,starting_times,low_cut_off);
  return 0;
}




int do_fit_double_exponential(void)
{
  O_p *op = NULL;
  d_s *ds=NULL;
  pltreg *pr = NULL;
  int i =0;
  float freq = 0;
  static float t0=0,t1=0,a=0.5;
  static int debug = 0, use_frq_unit_set = 1;
  static int iter_max=1000;
  float mean = 0;


  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)	return 1;

  for (int jj =0;jj<ds->nx;jj++)
  {
    mean+=ds->yd[jj]/ds->nx;
  }

  t0 = mean/2.0;
  t1 = mean*2.0;

  i = win_scanf("First time approximation %6f\n"
                "Second time approximation %6f\n"
                "Proportion of first time %6f\n"
                "Convergence plot %b\n"
                "N iterations %6d\n"
                "%b Use frequency from unit set \n"
                "If not enter the frequency of acquisition %6f\n",&t0,&t1,&a,&debug,&iter_max,&use_frq_unit_set,&freq);
  if (i==WIN_CANCEL) return 0;


  if (use_frq_unit_set)
  {
    freq = 1/op->yu[op->c_yu]->dx;
    win_printf_OK("Read frequency is %6f\n",freq);
  }

  fit_double_exponential(pr,op,ds,t0,t1,a,debug,iter_max,use_frq_unit_set,freq,-freq,-freq,0,0,0);

  return 0;
}

int do_fit_double_gaussian(void)
{
  O_p *op = NULL;
  d_s *ds=NULL;
  pltreg *pr = NULL;
  int i =0;
  static float t0=0,t1=0,a=0.5;
  static int debug = 0;
  static int iter_max=1000;
  float mean = 0;
  float sigma = 0;
  float sigma1 = 0;
  float sigma0 = 0;
  static int plot = 1;
  static float threshold = 0.001;


  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)	return 1;

  for (int jj =0;jj<ds->nx;jj++)
  {
    mean+=ds->yd[jj]/ds->nx;
    sigma += ds->yd[jj]*ds->yd[jj]/ds->nx;
  }
  sigma = sigma - mean*mean;

  t0 = mean + sqrt(sigma)/2;
  t1 = mean - sqrt(sigma)/2;
  sigma0 = sqrt(sigma)/2;
  sigma1 = sqrt(sigma)/2;

  i = win_scanf("First mean approximation %6f\n"
                "Second mean approximation %6f\n"
                "First sigma %12f\n"
                "Second sigma %12f\n"
                "Proportion of first mean %6f\n"
                "Convergence plot %b\n"
                "N iterations %6d\n"
                "Convergence threshold %6f\n"
                "Plot data %R -> no %r -> histo %r -> classify\n" ,&t0,&t1,&sigma0,&sigma1,&a,&debug,&iter_max,&threshold,&plot);
  if (i==WIN_CANCEL) return 0;

  sigma0 *= sigma0;
  sigma1 *= sigma1;
  fit_double_gaussian(pr, op, ds,  t0,  t1,  sigma0,  sigma1,  a,  debug,  plot,  iter_max,  threshold, 0 ,0,&a,0,ds->nx-1,NULL);

  return 0;
}

int do_classify_double_gaussian_and_draw_times(void)
{
  O_p *op = NULL;
  O_p *opup = NULL;
  O_p *opdown = NULL;
  d_s *dsdown = NULL;
  d_s *dsup = NULL;

  d_s *ds=NULL;
  pltreg *pr = NULL;
  int i =0;
  static float t0=0,t1=0;
  float a = 0.5;
  static int debug = 0;
  static int iter_max=1000;
  float mean = 0;
  float sigma = 0;
  float sigma1 = 0;
  float sigma0 = 0;
  static int plot = 0;
  static float threshold = 0.001;
  static int keep = 0;


  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)	return 1;

  for (int jj =0;jj<ds->nx;jj++)
  {
    mean+=ds->yd[jj]/ds->nx;
    sigma += ds->yd[jj]*ds->yd[jj]/ds->nx;
  }
  sigma = sigma - mean*mean;

  t0 = mean + sqrt(sigma)/2;
  t1 = mean - sqrt(sigma)/2;
  sigma0 = sqrt(sigma)/2;
  sigma1 = sqrt(sigma)/2;

  i = win_scanf("First mean approximation %6f\n"
                "Second mean approximation %6f\n"
                "First sigma %12f\n"
                "Second sigma %12f\n"
                "Proportion of first mean %6f\n"
                "Convergence plot %b\n"
                "N iterations %6d\n"
                "Keep these parameters but translate %d\n"
                "Convergence threshold %6f\n"
                "Plot data %R -> no %r -> histo %r -> classify\n" ,&t0,&t1,&sigma0,&sigma1,&a,&debug,&iter_max,&keep,&threshold,&plot);
  if (i==WIN_CANCEL) return 0;

  sigma0 *= sigma0;
  sigma1 *= sigma1;
  float *probas_ret = calloc(ds->nx,sizeof(float));
  //if (keep == 0)
  fit_double_gaussian(pr, op, ds,  t0,  t1,  sigma0,  sigma1,  a,  debug,  plot,  iter_max,  threshold, 0 ,0,&a,0,ds->nx-1,probas_ret);
  // else fit_fixed_double_gaussian(pr,op,ds,t0,t1,sigma0,sigma1,a,debug,plot,iter_max,threshold,0,0,&a,ds->nx-1,probas_ret);



  opup = create_and_attach_one_plot(pr,16,16,0);
  uns_op_2_op_by_type(op, IS_X_UNIT_SET, opup, IS_Y_UNIT_SET);
  uns_op_2_op_by_type(op, IS_X_UNIT_SET, opup, IS_X_UNIT_SET);
  dsup = opup->dat[0];
  dsup->nx = dsup->ny = 0;
  set_plot_title(opup,"Times spent in high extension state");
  set_plot_x_title(opup,"Time");
  set_plot_x_title(opup,"Duration");

  opdown = create_and_attach_one_plot(pr,16,16,0);
  uns_op_2_op_by_type(op, IS_X_UNIT_SET, opdown, IS_Y_UNIT_SET);
  uns_op_2_op_by_type(op, IS_X_UNIT_SET, opdown, IS_X_UNIT_SET);
  dsdown = opdown->dat[0];
  dsdown->nx = dsdown->ny = 0;
  set_plot_title(opdown,"Times spent in low extension state");
  set_plot_x_title(opdown,"Time");
  set_plot_x_title(opdown,"Duration");






  float frame_dur = ds->xd[1]-ds->xd[0];
  int nb_of_frames_last_state = 0;
  int state = 0;

  win_printf_OK("Read freq : %f Hz\n",1.0/(frame_dur*op->xu[op->c_xu]->dx));

  for (int i = 0;i<ds->nx;i++)
  {
    if (probas_ret[i] > 0.5) // low state
    {
      if (state == 0) nb_of_frames_last_state += 1; //if already in low state
       else { //if was in up state
        if (nb_of_frames_last_state != 0) add_new_point_to_ds(dsup,ds->xd[i],nb_of_frames_last_state*frame_dur);
        nb_of_frames_last_state = 1;
        state = 0;
      }
    }
    else
    {
      if (state == 1) nb_of_frames_last_state += 1;
      else {
        if (nb_of_frames_last_state != 0) add_new_point_to_ds(dsdown,ds->xd[i],nb_of_frames_last_state*frame_dur);
        nb_of_frames_last_state = 1;
        state = 1;
      }
    }
  }

  free(probas_ret);


  return 0;
}



int redo_fit_double_exponential(void)
{

  O_p *op = NULL;
  d_s *ds=NULL;
  pltreg *pr = NULL;
  int i =0;


  static int debug = 0, use_frq_unit_set = 1;
  static int iter_max=1000;


  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)	return 1;


  if (op->user_vspare[0] != 0 && op->user_ispare[0] == 47891) ds = (d_s *) op->user_vspare[0];
  else return(win_printf_OK("I could not find any linked original data set for this plot. You can not use this function if the plot has been saved and reloaded.\n"));

  float bin0 = op->user_fspare[4];
  float bin1 = op->user_fspare[5];
  float t0 = op->user_fspare[0];
  float t1 = op->user_fspare[1];
  float a = op->user_fspare[2];
  float freq = op->user_fspare[3];
  float old_low_cut_off = op->user_fspare[6]/freq;
  float old_high_cut_off = op->user_fspare[7]/freq;

  float low_cut_off = op->user_fspare[6]/freq;
  float high_cut_off = op->user_fspare[7]/freq;
  int recompute = 0;

  bin0= bin0/freq;
  bin1=bin1/freq;


  i = win_scanf("This routine allow you to add cut-offs or change the bins of a two_plots_histogram\n"
                "new bin 1 (s)%6f\n"
                "New bin 2 (s)%6f\n"
                "New low cut off (s)(-1 if none) %6f\n"
                "New high cut off (s) (-1 if none)%6f \n"
                "New freq %6f\n"
                "if you changed the cutoff \n"
                "Iterations %d\n"
                "Debug %b\n",
              &bin0, &bin1,&low_cut_off,&high_cut_off,&freq,&iter_max,&debug);
  if (i==WIN_CANCEL) return 0;

  if ((low_cut_off != old_low_cut_off) || (high_cut_off != old_high_cut_off)) recompute = 1;

  bin0*=freq;
  bin1*=freq;
  low_cut_off*=freq;
  high_cut_off*=freq;

  fit_double_exponential(pr,op,ds,t0,t1,a,debug,iter_max,use_frq_unit_set,freq,low_cut_off,high_cut_off,bin0,bin1,recompute);

  return 0;


}





/* fit gaussian */

double Y_equal_a_gaussian( double x, double x0, double a, double b)
{
    double tmp;

    tmp = x - x0;
    tmp /= (a!=0) ? a : 1;
    tmp *= tmp;
    tmp /= 2;
    tmp = -tmp;
    tmp = b * exp(tmp);
    tmp /= sqrt(2*M_PI*a*a);
    return (tmp);
}

void Y_equal_a_gaussian_func(float x, float *aa, float *y, float *dydaa, int ma)
{
    double  x0, a, b, dydx0, dyda, dydb;
    double  tmp;

    (void)ma;
    x0 = aa[1];
    a = aa[2];
    b = aa[3];
    *y = Y_equal_a_gaussian( x, x0, a, b);
    dydb = *y/b;
    tmp = x - x0;
    tmp /= (a!=0) ? a*a : 1;
    tmp *= *y;
    dydx0 = tmp;
    tmp = (x - x0)*(x - x0);
    tmp = (a!=0) ? (tmp/(a*a*a) - (float)1/(a)) : 1;
    tmp *= *y;
    dyda = tmp;
    dydaa[1] = dydx0;
    dydaa[2] = dyda;
    dydaa[3] = dydb;
}
int	do_fit_Y_equal_a_gaussian(void)
{
    int i;
    char st[2048] = {0};
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *dsi = NULL, *dsd = NULL;
    float x, xmin, xmax, tmp;
    int nf = 1024;
    static float error = .1;
    static float x0 = 0, a = 1;
    static float b = 1;
    static int flag = 1, ix0 = 1, ia = 1, ib = 1;
    float aa[3] = {0}, chisq, chisq0, alamda, *sig, **covar, **alpha;
    int iaa[3] = {0}, nder;
    //float gammq(float a, float x);

    if(updating_menu_state != 0)	return D_O_K;
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine fits a gaussian to a data set\n"
                             "using a Levenberg Marquart algorithm\n"
                             "\n\nf(t) = \\frac{b}{\\sqrt{2\\pi a}} exp-[\\frac{(x - x_0)^2}{2.a^2}]\n"
                             "the parameter x_0, a, b may either be imposed or fitted\n"
                             "you must provide good guessed values of these parameters\n"
                             "otherwise this routine is lickly to crash the program\n"
                             "special thanks to Numerical Reciepe...");
    }


    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)	return 1;
    for (i = 0, xmin = xmax = dsi->xd[0]; i < dsi->nx; i++)
    {
        xmin = (dsi->xd[i] < xmin) ? dsi->xd[i] : xmin;
        xmax = (dsi->xd[i] > xmax) ? dsi->xd[i] : xmax;
    }

    if (dsi->ye == NULL)
    {
        i = win_scanf("Fit x_0, a, b, in f(t) = \\frac{b}{\\sqrt{2\\pi a}}"
                      " exp-[\\frac{(x - x_0)^2}{2.a^2}] \n \npossible x_0 value %fa  value %f"
                      "b value %f error type, const -> 0 relative 1 other data set 2%d"
                      "error\n %ffit x_0 %bfit a %b"
                      "fit b %b",&x0,&a,&b,&flag,&error,&ix0, &ia, &ib);
        if (i == WIN_CANCEL)	return OFF;
    }
    else
    {
        i = win_scanf("Fit x_0, a, b, in f(t) = \\frac{b}{\\sqrt{2\\pi a}}"
                      " exp-[\\frac{(x - x_0)^2}{2.a^2}] \n \npossible x_0 value %fa  value %f"
                      "b value %f  error is taken from error bars\n"
                      "fit x_0  %b fit a  %b fit b %b\n",&x0,&a,&b,&ix0, &ia, &ib);
        if (i == WIN_CANCEL)	return OFF;
    }



    iaa[0] = ix0;
    iaa[1] = ia;
    iaa[2] = ib;
    aa[0] = (x0 - op->ax)/op->dx;
    aa[1] = a/op->dx;
    aa[2] = b/(op->dy*op->dx);
    covar = matrix(1, 3, 1, 3);
    alpha = matrix(1, 3, 1, 3);
    sig = vector(1, dsi->nx);

    if (dsi->ye == NULL)
    {
        for (i = 1; i <= dsi->nx; i++)
            sig[i] = (flag) ? dsi->yd[i-1] * error : error/op->dy;
        if (flag == 2)
        {
            nder = op->cur_dat+1;
            i = win_scanf("enter index of error data set %d",&nder);
            if (i == WIN_CANCEL)	return OFF;
            if (op->dat[nder]->nx != dsi->nx)
                return win_printf_OK("your data sets differs in size!");
            for (i = 1; i <= dsi->nx; i++)
                sig[i] =  op->dat[nder]->yd[i-1];
        }
    }
    else
    {
        for (i = 1; i <= dsi->nx; i++)
            sig[i] = dsi->ye[i-1];
    }
    alamda = -1;
    mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, aa, iaa,3, covar, alpha,
           &chisq, Y_equal_a_gaussian_func, &alamda);

    chisq0 = chisq;
    mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, aa, iaa,3, covar, alpha,
           &chisq, Y_equal_a_gaussian_func, &alamda);

    for (i = 0 ; (chisq0 - chisq <= 0) || (chisq0 - chisq > 1e-2); i++)
    {
        chisq0 = chisq;
        mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, aa, iaa,3, covar, alpha,
               &chisq, Y_equal_a_gaussian_func, &alamda);

        if (i >= 10000) break;
        if (alamda > 1e12) break;
    }

    alamda = 0;
    mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, aa, iaa,3, covar, alpha,
           &chisq, Y_equal_a_gaussian_func, &alamda);
    /*
            i = win_printf("%d iter a_0 = %g,\n a_1 = %g l = %g the \\chi^2  is %g\n"
            "covar \\xi \\xi %g \\xi L %g\nL \\xi %g LL %g\ncurvature \\xi \\xi %g"
            "\\xi L %g\n L \\xi %g LL %g",i,op->dx*aa[0],op->dy*aa[1],aa[2]*op->dy,
            chisq,covar[1][1],covar[1][2],covar[2][1],covar[2][2],alpha[1][1],
            alpha[1][2],alpha[2][1],alpha[2][2]);*/
    x0 = aa[0];
    a = aa[1];
    b = aa[2];
    if (i == WIN_CANCEL)	return OFF;
    if (dsi->nx > 256)	nf = dsi->nx;
    if ((dsd = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("cannot create data set");
    if (dsi->nx > 256)
    {
        for (i = 0; i < dsd->nx && i < dsi->nx; i++)
        {
            x = dsi->xd[i];
            dsd->xd[i] = dsi->xd[i];
            dsd->yd[i] = Y_equal_a_gaussian( x, x0, a, b);
        }
    }
    else
    {
        tmp = xmin - 0.1*(xmax - xmin);
        xmax = xmax + 0.1*(xmax - xmin);
        xmin = tmp;
        for (i = 0; i < dsd->nx; i++)
        {
            x = xmin + i*(xmax-xmin)/dsd->nx;
            dsd->xd[i] = x;
            dsd->yd[i] = Y_equal_a_gaussian( x, x0, a, b);
        }
    }

    //	win_printf("bef x0 %g a = %g, b %g\n dx %g, dy %g",x0,a,b,op->dx,op->dy);
    x0 = op->ax+(op->dx*x0);
    a = a*op->dx;
    b = b*op->dy*op->dx;
    //win_printf("after x0 %g a = %g, b %g\n dx %g, dy %g",x0,a,b,op->dx,op->dy);

    sprintf(st,"\\fbox{\\pt8\\stack{{\\sl f(t) = \\frac{b}{\\sqrt{2\\pi a^2}}"
            " exp-[\\frac{(x - x_0)^2}{2.a^2}]}"
            "{x_0 = %g \\stack{+-} %g}"
            "{a = %g \\stack{+-} %g}"
            "{b = %g \\stack{+-} %g}"
            "{\\tau  = %g %s \\stack{+-} %g %%}"
            "{\\chi^2 = %g n = %d}{p = %g}}}",
            x0,op->dx*sqrt(covar[1][1]),a,op->dx*sqrt(covar[2][2]),
            b,sqrt(covar[3][3])*op->dy*op->dx,(a!=0)?1/a:0,(op->x_unit) ? op->x_unit:" ",
            100*sqrt(covar[2][2])/a,chisq,dsi->nx-3,gammq((float)(dsi->nx-3)/2, chisq/2));
    push_plot_label(op, op->x_lo + (op->x_hi - op->x_lo)/4,
                    op->y_hi - (op->y_hi - op->y_lo)/4, st, USR_COORD);

    free_vector(sig, 1, dsi->nx);
    free_matrix(alpha, 1, 3, 1, 3);
    free_matrix(covar, 1, 3, 1, 3);
    return refresh_plot(pr,pr->cur_op);
}

int	fit_Y_equal_a_gaussian(d_s *dsi, float *xx0, float a)
{
    int i;
    char st[2048] = {0};
    pltreg *pr = NULL;
    d_s  *dsd = NULL;
    float x, xmin, xmax, tmp;
    int nf = 1024;
    static float error = 2 ;
    static float x0 = 0;
    static float b = 1;
    static int flag = 0, ix0 = 1, ia = 0, ib = 1;
    float aa[3] = {0}, chisq, chisq0, alamda, *sig, **covar, **alpha;
    int iaa[3] = {0}, nder;
    //float gammq(float a, float x);

    for (i = 0, xmin = xmax = dsi->xd[0]; i < dsi->nx; i++)
    {
        xmin = (dsi->xd[i] < xmin) ? dsi->xd[i] : xmin;
        xmax = (dsi->xd[i] > xmax) ? dsi->xd[i] : xmax;
    }

    iaa[0] = ix0;
    iaa[1] = ia;
    iaa[2] = ib;
    aa[0] = dsi->nx/2.0;
    aa[1] = a;
    aa[2] = b;
    covar = matrix(1, 3, 1, 3);
    alpha = matrix(1, 3, 1, 3);
    sig = vector(1, dsi->nx);

    if (dsi->ye == NULL)
    {
        for (i = 1; i <= dsi->nx; i++)
            sig[i] = (flag) ? dsi->yd[i-1] * error : error;

    }
    else
    {
        for (i = 1; i <= dsi->nx; i++)
            sig[i] = dsi->ye[i-1];
    }
    alamda = -1;
    mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, aa, iaa,3, covar, alpha,
           &chisq, Y_equal_a_gaussian_func, &alamda);

    chisq0 = chisq;
    mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, aa, iaa,3, covar, alpha,
           &chisq, Y_equal_a_gaussian_func, &alamda);

    for (i = 0 ; (chisq0 - chisq <= 0) || (chisq0 - chisq > 1e-2); i++)
    {
        chisq0 = chisq;
        mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, aa, iaa,3, covar, alpha,
               &chisq, Y_equal_a_gaussian_func, &alamda);

        if (i >= 10000) break;
        if (alamda > 1e12) break;
    }

    alamda = 0;
    mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, aa, iaa,3, covar, alpha,
           &chisq, Y_equal_a_gaussian_func, &alamda);
    /*
            i = win_printf("%d iter a_0 = %g,\n a_1 = %g l = %g the \\chi^2  is %g\n"
            "covar \\xi \\xi %g \\xi L %g\nL \\xi %g LL %g\ncurvature \\xi \\xi %g"
            "\\xi L %g\n L \\xi %g LL %g",i,op->dx*aa[0],op->dy*aa[1],aa[2]*op->dy,
            chisq,covar[1][1],covar[1][2],covar[2][1],covar[2][2],alpha[1][1],
            alpha[1][2],alpha[2][1],alpha[2][2]);*/
    x0 = aa[0];
    a = aa[1];
    b = aa[2];

    *xx0 = x0;

    free_vector(sig, 1, dsi->nx);
    free_matrix(alpha, 1, 3, 1, 3);
    free_matrix(covar, 1, 3, 1, 3);
    return 0;
}



int	do_fit_Y_equal_a_gaussian_boostrap(void)
{
    int i, j;
    char st[2040] = {0};
    pltreg *pr = NULL;
    O_p *op = NULL, *opb = NULL;
    d_s *dsi = NULL, *dsd = NULL, *dsir = NULL, *dsib = NULL, *dsirb = NULL;
    d_s *dsx = NULL, *dsa = NULL, *dsb = NULL, *dsc = NULL;
    float x, xmin, xmax, tmp;
    int nf = 1024;
    static float x0 = 0, a = 1;
    static float b = 1;
    static int ix0 = 1, ia = 1, ib = 1, niter = 512;
    float aa[3] = {0}, chisq, chisq0, alamda, *sig, **covar, **alpha;
    int iaa[3] = {0}, nder;
    //float gammq(float a, float x);
    static long dum;

    if(updating_menu_state != 0)	return D_O_K;
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine fits a gaussian to a data set\n"
                             "using a Levenberg Marquart algorithm and boostraps input data\n"
                             "\n\nf(t) = \\frac{b}{\\sqrt{2\\pi a}} exp-[\\frac{(x - x_0)^2}{2.a^2}]\n"
                             "the parameter x_0, a, b may either be imposed or fitted\n"
                             "you must provide good guessed values of these parameters\n"
                             "otherwise this routine is lickly to crash the program\n"
                             "special thanks to Numerical Reciepe...\n"
                             "The boostrap allows to estimate the errors in the fitted\n"
                             "parameters");
    }

    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)	return 1;
    for (i = 0, xmin = xmax = dsi->xd[0]; i < dsi->nx; i++)
    {
        xmin = (dsi->xd[i] < xmin) ? dsi->xd[i] : xmin;
        xmax = (dsi->xd[i] > xmax) ? dsi->xd[i] : xmax;
    }
    nder = op->cur_dat+1;

    i = win_scanf("Boostrap Fit x_0, a, b, in f(t) = \\frac{b}"
                  "{\\sqrt{2\\pi a}} exp-[\\frac{(x - x_0)^2}{2.a^2}] \n \n"
                  "possible x_0 value %fa  value %fb value %f error data set %d"
                  "fit x_0 (yes 1, imposed 0) %dfit a (yes 1, imposed 0) %d"
                  "fit b (yes 1, imposed 0) %dnb. of boostrap %dseed %ld",&x0,&a,&b,&nder
                  ,&ix0, &ia, &ib,&niter,&dum);
    if (i == WIN_CANCEL)	return OFF;

    dsir = op->dat[nder];
    if (dsir->nx != dsi->nx)
        return win_printf_OK("your data sets differs in size!");

    nf = dsi->nx;
    if ((dsib = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("cannot create plot");
    if ((dsirb = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("cannot create data set");

    if ((opb = create_and_attach_one_plot(pr, niter, niter, 0)) == NULL)
        return win_printf_OK("cannot create plot");
    dsx = opb->dat[0];
    if ((dsa = create_and_attach_one_ds(opb, niter, niter, 0)) == NULL)
        return win_printf_OK("cannot create data set");
    if ((dsb = create_and_attach_one_ds(opb, niter, niter, 0)) == NULL)
        return win_printf_OK("cannot create data set");
    if ((dsc = create_and_attach_one_ds(opb, niter, niter, 0)) == NULL)
        return win_printf_OK("cannot create data set");




    covar = matrix(1, 3, 1, 3);
    alpha = matrix(1, 3, 1, 3);
    sig = vector(1, dsib->nx);

    for (j = 0; j < niter; j++)
    {
        iaa[0] = ix0;
        iaa[1] = ia;
        iaa[2] = ib;
        aa[0] = (x0 - op->ax)/op->dx;
        aa[1] = a/op->dy;
        aa[2] = b/op->dy;

        add_gaussian_noise(dsi, dsir, dsib, dsirb, &dum);

        for (i = 1; i <= dsirb->nx; i++)
            sig[i] =  dsirb->yd[i-1];
        alamda = -1;
        mrqmin_c(dsib->xd, dsib->yd, sig, dsib->nx, aa, iaa,3, covar, alpha,
               &chisq, Y_equal_a_gaussian_func, &alamda);


        chisq0 = chisq;
        mrqmin_c(dsib->xd, dsib->yd, sig, dsib->nx, aa, iaa,3, covar, alpha,
               &chisq, Y_equal_a_gaussian_func, &alamda);

        for (i = 0 ; (chisq0 - chisq <= 0) || (chisq0 - chisq > 1e-2); i++)
        {
            chisq0 = chisq;
            mrqmin_c(dsib->xd, dsib->yd, sig, dsib->nx, aa, iaa,3, covar,
                   alpha, &chisq, Y_equal_a_gaussian_func, &alamda);
            if (i >= 10000) break;
            if (alamda > 1e12) break;
        }

        alamda = 0;
        mrqmin_c(dsib->xd, dsib->yd, sig, dsib->nx, aa, iaa,3, covar, alpha,
               &chisq, Y_equal_a_gaussian_func, &alamda);

        dsx->yd[j] = op->ax+(op->dx*aa[0]);
        dsa->yd[j] = aa[1]*op->dy;
        dsb->yd[j] = aa[2]*op->dy;
        dsc->yd[j] = chisq;
        dsx->xd[j] = dsa->xd[j] = dsb->xd[j] = dsc->xd[j] = j;

        display_title_message("%03d x_0 = %06.2g, a = %06.2g  b = %06.2g"
                              " \\chi^2  is %06.2g",j,dsx->yd[j],dsa->yd[j],dsb->yd[j],chisq);
    }


    i = win_printf("%d iter a_0 = %g,\n a_1 = %g l = %g the \\chi^2  is %g\n"
                   "covar \\xi \\xi %g \\xi L %g\nL \\xi %g LL %g\n"
                   "curvature \\xi \\xi %g \\xi L %g\n L \\xi %g LL %g",i,op->dx*aa[0],
                   op->dy*aa[1],aa[2]*op->dy,chisq,covar[1][1],covar[1][2],
                   covar[2][1],covar[2][2],alpha[1][1],alpha[1][2],alpha[2][1],alpha[2][2]);
    if (i == WIN_CANCEL)	return OFF;
    x0 = aa[0];
    a = aa[1];
    b = aa[2];
    x0 = op->ax+(op->dx*x0);
    a = a*op->dy;
    b = b*op->dy;

    if (dsib->nx > 256)	nf = dsib->nx;
    if ((dsd = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("cannot create data set");
    if (dsib->nx > 256)
    {
        for (i = 0; i < dsd->nx && i < dsib->nx; i++)
        {
            x = dsib->xd[i];
            dsd->xd[i] = dsib->xd[i];
            dsd->yd[i] = Y_equal_a_gaussian( x, x0, a, b);
        }
    }
    else
    {
        tmp = xmin - 0.1*(xmax - xmin);
        xmax = xmax + 0.1*(xmax - xmin);
        xmin = tmp;
        for (i = 0; i < dsd->nx; i++)
        {
            x = xmin + i*(xmax-xmin)/dsd->nx;
            dsd->xd[i] = x;
            dsd->yd[i] = Y_equal_a_gaussian( x, x0, a, b);
        }
    }

    sprintf(st,"\\fbox{\\pt8\\stack{{\\sl f(t) = \\frac{b}{\\sqrt{2\\pi a^2}}"
            " exp-[\\frac{(x - x_0)^2}{2.a^2}]}{x_0 =  %g  \\stack{+-} %g}"
            "{a = %g  \\stack{+-} %g}{b = %g \\stack{+-} %g}"
            "{\\tau  = %g %s \\stack{+-} %g %%}{\\chi^2 = %g n = %d}{p = %g}}}"
            ,op->ax+(op->dx*x0),op->dx*sqrt(covar[1][1]),
            op->dy*a,op->dy*sqrt(covar[2][2]),
            b*op->dy,sqrt(covar[3][3])*op->dy,op->dy*b,(op->x_unit) ? op->x_unit:" ",
            100*sqrt(covar[3][3])/b,chisq,dsi->nx-3,gammq((float)(dsi->nx-3)/2,
                                                          chisq/2));
    push_plot_label(op, op->x_lo + (op->x_hi - op->x_lo)/4,
                    op->y_hi - (op->y_hi - op->y_lo)/4, st, USR_COORD);



    free_vector(sig, 1, dsi->nx);
    free_matrix(alpha, 1, 3, 1, 3);
    free_matrix(covar, 1, 3, 1, 3);
    return refresh_plot(pr,pr->cur_op);
}
/*
 *	O_p *op, the plot
 *  d_s *dsi, the data set to fit
 *  d_s *dsir, the error data set
 *	double *x0, the position of the gaussian
 *  int ix0, a flag to fit (1) or to impose x0
 *  double *a, the extension of the gaussian
 *  int ia, a flag to fit (1) or to impose (0) a
 *  double *b, the normalization factor
 *  int ib, a flag to fit (1) or to impose (0) b
 *  double *chisq the" chi^2 of the fit
 */


int fit_Y_equal_a_gaussian_boostrap_to_op(O_p *op, d_s *dsi, d_s *dsir,
                                          double *x0, int ix0, double *a, int ia, double *b, int ib,
                                          double *chisq, int verbose)
{
    int i;
    float aa[3] = {0}, chisq0, alamda, *sig = NULL, **covar = NULL, **alpha = NULL, fchisq;
    int iaa[3] = {0};

    if (dsi == NULL || dsir == NULL) return 1;
    if (dsir->nx != dsi->nx)
        return win_printf_OK("your data sets differs in size!");

    covar = matrix(1, 3, 1, 3);
    alpha = matrix(1, 3, 1, 3);
    sig = vector(1, dsi->nx);

    iaa[0] = ix0;
    iaa[1] = ia;
    iaa[2] = ib;
    aa[0] = ((*x0) - op->ax)/op->dx;
    aa[1] = (*a)/op->dy;
    aa[2] = (*b)/op->dy;

    for (i = 1; i <= dsir->nx; i++)
        sig[i] =  dsir->yd[i-1];
    alamda = -1;
    mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, aa, iaa,3, covar, alpha,
           &fchisq, Y_equal_a_gaussian_func, &alamda);

    chisq0 = fchisq;
    mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, aa, iaa,3, covar, alpha,
           &fchisq, Y_equal_a_gaussian_func, &alamda);

    for (i = 0 ; (chisq0 - fchisq <= 0) || (chisq0 - fchisq > 1e-2); i++)
    {
        chisq0 = fchisq;
        mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, aa, iaa,3, covar,
               alpha, &fchisq, Y_equal_a_gaussian_func, &alamda);
        if (i >= 10000) break;
        if (alamda > 1e12) break;
    }

    alamda = 0;
    mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, aa, iaa,3, covar, alpha,
           &fchisq, Y_equal_a_gaussian_func, &alamda);

    if (verbose)
    {
        i = win_printf("%d iter a_0 = %g,\n a_1 = %g l = %g the \\chi^2  is %g\n"
                       "covar \\xi \\xi %g \\xi L %g\nL \\xi %g LL %g\n"
                       "curvature \\xi \\xi %g \\xi L %g\n L \\xi %g LL %g",i,op->dx*aa[0],
                       op->dy*aa[1],aa[2]*op->dy,fchisq,covar[1][1],covar[1][2],
                       covar[2][1],covar[2][2],alpha[1][1],alpha[1][2],alpha[2][1],alpha[2][2]);
        if (i == WIN_CANCEL)	return OFF;
    }
    *x0 = aa[0];
    *a = aa[1];
    *b = aa[2];
    *x0 = op->ax+(op->dx*(*x0));
    *a = (*a)*op->dy;
    *b = (*b)*op->dy;
    *chisq = fchisq;
    display_title_message("x_0 = %06.2g, a = %06.2g  b = %06.2g"
                          " \\chi^2  is %06.2g",*x0,*a,*b,*chisq);
    free_vector(sig, 1, dsi->nx);
    free_matrix(alpha, 1, 3, 1, 3);
    free_matrix(covar, 1, 3, 1, 3);
    return 0;
}

/*
 *	O_p *op, the plot
 *  d_s *dsi, the data set to fit
 *  d_s *dsir, the error data set
 *	double *a0, the baseline of the exponential
 *  int ia0, a flag to fit (1) or to impose a0
 *  double *a1, the amplitude of the exponential
 *  int ia1, a flag to fit (1) or to impose (0) a1
 *  double *l, the exponential decay factor
 *  int il, a flag to fit (1) or to impose (0) l
 *  double *chisq the" chi^2 of the fit
 */




int	fit_Y_equal_a0_plus_a1_exp_moins_lt_to_op(O_p *op, d_s *dsi, d_s *dsir,
                                              double *a0, int ia0, double *a1, int ia1, double *l, int il,
                                              double *chisq, int verbose)
{
    int i;
    float a[3] = {0}, fchisq, chisq0, alamda, *sig = NULL, **covar = NULL, **alpha = NULL;
    int ia[3] = {0};
    //float gammq(float a, float x);


    ia[0] = ia0;
    ia[1] = ia1;
    ia[2] = il;
    a[0] = ((*a0) - op->ay)/op->dy;
    a[1] = (*a1)/op->dy;
    a[2] = (*l)*op->dx;
    covar = matrix(1, 3, 1, 3);
    alpha = matrix(1, 3, 1, 3);
    sig = vector(1, dsi->nx);
    if (dsir->nx != dsi->nx)
        return win_printf_OK("your data sets differs in size!");
    for (i = 1; i <= dsir->nx; i++)
        sig[i] =  dsir->yd[i-1];
    alamda = -1;
    mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, a, ia,3, covar, alpha, &fchisq,
           Y_equal_a0_plus_a1_exp_moins_lt_func, &alamda);

    chisq0 = fchisq;
    mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, a, ia,3, covar, alpha, &fchisq,
           Y_equal_a0_plus_a1_exp_moins_lt_func, &alamda);

    for (i = 0 ; (chisq0 - fchisq <= 0) || (chisq0 - fchisq > 1e-2); i++)
    {
        chisq0 = fchisq;
        mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, a, ia,3, covar, alpha,
               &fchisq, Y_equal_a0_plus_a1_exp_moins_lt_func, &alamda);

        if (i >= 10000) break;
        if (alamda > 1e12) break;
    }

    alamda = 0;
    mrqmin_c(dsi->xd, dsi->yd, sig, dsi->nx, a, ia,3, covar, alpha, &fchisq,
           Y_equal_a0_plus_a1_exp_moins_lt_func, &alamda);

    if (verbose)
    {
        i = win_printf("%d iter a_0 = %g,\n a_1 = %g l = %g the \\chi^2  is %g\n"
                       " covar \\xi \\xi %g \\xi L %g\nL \\xi %g LL %g\n"
                       "curvature \\xi \\xi %g \\xi L %g\n L \\xi %g LL %g",i,op->dy*a[0],
                       op->dy*a[1],a[2]/op->dx,fchisq,covar[1][1],covar[1][2],covar[2][1],
                       covar[2][2],alpha[1][1],alpha[1][2],alpha[2][1],alpha[2][2]);
        if (i == WIN_CANCEL)	return OFF;
    }
    *a0 = a[0];
    *a1 = a[1];
    *l = a[2];
    *a0 = op->ay+(op->dy*(*a0));
    *a1 = (*a1)*op->dy;
    *l = (*l)/op->dx;
    *chisq = fchisq;
    free_vector(sig, 1, dsi->nx);
    free_matrix(alpha, 1, 3, 1, 3);
    free_matrix(covar, 1, 3, 1, 3);
    return 0;
}




int do_gen_flat(void)
{
    int i;
    static float var = 1, off = 0;
    static int sop = 0, iidum = 0, nx = 1024, alter = 0;
    int idum;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL, *dsi = NULL;
    int c_op;


    if(updating_menu_state != 0)	return D_O_K;
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine generate a flat noise \n"
                             "using numerical reciepe algorithm");
    }

    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)	return 1;
    nx = dsi->nx;
    c_op = pr->cur_op;
    i = win_scanf("Flat noise generator \n"
                  "%R->Create a new plot or %r->add a data set or %r->add noise in Y or %r->add noise in X\n"
                  "In the two first cases indicate the Nb of samples %8d\n"
                  "For the two last cases click here %b to alter the present dataset\n"
                  "Indicate the noise extent %12f offset %12f\n seed %15d\n",&sop,&nx,&alter,&var,&off,&iidum);
    if (i == WIN_CANCEL)      return D_O_K;


    if (sop == 0)
    {
        if ((op = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
            return win_printf_OK("cannot create plot");
        ds = op->dat[0];
        set_plot_title(op, "\\stack{{Gaussian noise}"
                       "{offset %g variance %g seed %d}}",off,var,iidum);
        set_ds_source(ds,"Flat noise offset %g extend %g seed %d",off,var,iidum);
    }
    else if (sop == 1)
    {
        if ((ds = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
            return win_printf_OK("cannot create data set");
        set_ds_source(ds,"Flat noise offset %g extend %g seed %d",off,var,iidum);
        //win_printf("adding a ds %d",nx);
    }
    else if (sop > 1)
    {
        if (alter == 0)
        {
            if ((ds = duplicate_data_set(dsi,NULL))  == NULL)
                return win_printf_OK("cannot create data set");
            //win_printf("duplicating a ds %d",dsi->nx);
            inherit_from_ds_to_ds(ds, dsi);
            add_one_plot_data (op, IS_DATA_SET, (void *)ds);

        }
        else 	  ds = dsi;
        set_ds_treatement(ds, "Added flat noise offset %g extend %g seed %d",off,var,iidum);
    }

    idum = iidum;

    if (sop < 2)
    {
        for (i = 0; ds != NULL && i < nx; i++)
        {
            ds->xd[i] = i;
            ds->yd[i] = off + var * (ran1(&idum)-0.5);
        }
    }
    else if (sop == 2)
    {
        //win_printf("Adding noise in Y");
        for (i = 0; i < ds->nx; i++)
        {
            ds->yd[i] += off + var * (ran1(&idum)-0.5);
        }
    }
    else if (sop == 3)
    {
        //win_printf("Adding noise in X");
        for (i = 0; i < ds->nx; i++)
        {
            ds->xd[i] += off + var * (ran1(&idum)-0.5);
        }
    }

    iidum = (int)idum;
    op->need_to_refresh = 1;
    if (sop == 0)  return refresh_plot(pr,pr->n_op-1);
    else return refresh_plot(pr,c_op);
}





int do_gen_gaussian(void)
{
    int i;
    static float var = 1, off = 0;
    static int sop = 0, iidum = 0, nx = 1024, alter = 0;
    int idum;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL, *dsi = NULL;
    int c_op;


    if(updating_menu_state != 0)	return D_O_K;
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine generate a gaussian noise \n"
                             "using numerical reciepe algorithm");
    }

    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)	return 1;
    nx = dsi->nx;
    c_op = pr->cur_op;
    i = win_scanf("Gaussian noise generator \n"
                  "%R->Create a new plot or %r->add a data set or %r->add noise in Y or %r->add noise in X\n"
                  "In the two first cases indicate the Nb of samples %8d\n"
                  "For the two last cases click here %b to alter the present dataset\n"
                  "Indicate the noise variance %12f offset %12f\n seed %15d\n",&sop,&nx,&alter,&var,&off,&iidum);
    if (i == WIN_CANCEL)      return D_O_K;


    if (sop == 0)
    {
        if ((op = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
            return win_printf_OK("cannot create plot");
        ds = op->dat[0];
        set_plot_title(op, "\\stack{{Gaussian noise}"
                       "{offset %g variance %g seed %d}}",off,var,iidum);
        set_ds_source(ds,"Gaussian noise offset %g variance %g seed %d",off,var,iidum);
    }
    else if (sop == 1)
    {
        if ((ds = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
            return win_printf_OK("cannot create data set");
        set_ds_source(ds,"Gaussian noise offset %g variance %g seed %d",off,var,iidum);
        //win_printf("adding a ds %d",nx);
    }
    else if (sop > 1)
    {
        if (alter == 0)
        {
            if ((ds = duplicate_data_set(dsi,NULL))  == NULL)
                return win_printf_OK("cannot create data set");
            //win_printf("duplicating a ds %d",dsi->nx);
            inherit_from_ds_to_ds(ds, dsi);
            add_one_plot_data (op, IS_DATA_SET, (void *)ds);

        }
        else 	  ds = dsi;
        set_ds_treatement(ds, "Added Gaussian noise offset %g variance %g seed %d",off,var,iidum);
    }

    idum = iidum;

    if (sop < 2)
    {
        for (i = 0; i < nx && ds != NULL; i++)
        {
            ds->xd[i] = i;
            ds->yd[i] = off + var * gasdev(&idum);
        }
    }
    else if (sop == 2)
    {
        //win_printf("Adding noise in Y");
        for (i = 0; i < ds->nx && ds != NULL; i++)
        {
            ds->yd[i] += off + var * gasdev(&idum);
        }
    }
    else if (sop == 3)
    {
        //win_printf("Adding noise in X");
        for (i = 0; i < ds->nx && ds != NULL; i++)
        {
            ds->xd[i] += off + var * gasdev(&idum);
        }
    }

    iidum = (int)idum;
    op->need_to_refresh = 1;
    if (sop == 0)  return refresh_plot(pr,pr->n_op-1);
    else return refresh_plot(pr,c_op);
}


int 	generate_signal(void)
{
    int i;
    static float fre = 128, amp = 1.0, fm = 1024;
    static int nx = 1024, type = 0, new_plot = 0, dosweep = 0;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    float fi = 0;

    if(updating_menu_state != 0)	return D_O_K;
    if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return 1;
    i = win_scanf("Generate a dummy signal \n"
                  "%R Sinus %r cosine\n"
                  "%R in this plot %r in a new plot\n"
                  "Number of points %10d \n"
                  "with a frequency f_0 = %6f and an amplitude %6f\n"
		  "%b->Sweep frequency from f_0 to f_m = %5f\n"
                  ,&type,&new_plot,&nx, &fre,&amp,&dosweep,&fm);
    if (i == WIN_CANCEL)	return D_O_K;
    if (new_plot)
    {
        if ((op = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
            return win_printf_OK("cannot create plot");
        ds = op->dat[0];
	if (dosweep == 0)
	  set_plot_title(op, "\\stack{{%s signal}"
			 "{freq %g amplitude %g}}",(type==0)?"Sinus":"Cosine",fre,amp);
	else
	  set_plot_title(op, "\\stack{{%s signal}"
			 "{freq sweep [%g,%g] amplitude %g}}"
			 ,(type==0)?"Sinus":"Cosine",fre,fm,amp);
    }
    else
    {
        if ((ds = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
            return win_printf_OK("cannot create data set");
    }
    if (type == 0)
    {
        for (i=0 ; i< nx ; i++)
        {
            ds->xd[i] = i;
	    if (dosweep == 0)
	      ds->yd[i] = amp * sin((M_PI*2*i*fre)/nx);
	    else
	      {
		fi = fre + i*(fm-fre)/nx;
		ds->yd[i] = amp * sin((M_PI*2*i*fi)/nx);
	      }
        }
    }
    else
    {
        for (i=0 ; i< nx ; i++)
        {
            ds->xd[i] = i;
	    if (dosweep == 0)
	      ds->yd[i] = amp * cos((M_PI*2*i*fre)/nx);
	    else
	      {
		fi = fre + i*(fm-fre)/nx;
		ds->yd[i] = amp * cos((M_PI*2*i*fi)/nx);
	      }

        }
    }
    if (dosweep == 0)
      set_ds_source(ds, "%s signal freq %g amplitude %g",(type==0)?"Sinus":"Cosine",fre,amp);
    else
      set_ds_source(ds, "%s signal freq sweep [%g,%g] amplitude %g"
		    ,(type==0)?"Sinus":"Cosine",fre,fm,amp);
    return refresh_plot(pr,pr->n_op-1);
}




int 	dirac_comb_signal(void)
{
    int i;
    static float amp = 1.0;
    static int nx = 1024, per = 32, new_plot = 0;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;

    if(updating_menu_state != 0)	return D_O_K;
    if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return 1;
    i = win_scanf("Generate a dummy signal \n"
                  "of Dirac comb\n"
                  "%R in this plot %r in a new plot\n"
                  "Total number of points %10d \n"
                  "with a period %8d and an amplitude %8f\n"
                  ,&new_plot,&nx, &per,&amp);
    if (i == WIN_CANCEL)	return D_O_K;
    if (new_plot)
    {
        if ((op = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
            return win_printf_OK("cannot create plot");
        ds = op->dat[0];
        set_plot_title(op, "\\stack{{Dirac comb signal}"
                       "{Per %d amplitude %g}}",per,amp);
    }
    else
    {
        if ((ds = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
            return win_printf_OK("cannot create data set");
    }
    for (i=0 ; i< nx ; i++)
    {
        ds->xd[i] = i;
        ds->yd[i] = amp * ((i%per) ? 0 : 1);
    }
    ds->source = my_sprintf(NULL, "Dirac com signal period %d amplitude %g",per,amp);
    return refresh_plot(pr,pr->n_op-1);
}




int 	rand_comb_signal(void)
{
    int i, j, k;
    static float amp = 1.0, prob, tmp, val;
    static int nx = 1024, per = 32, new_plot = 0, np = 1, do_histo = 0;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL, *dsh = NULL, *dsp = NULL;
    static int idum = 4587;

    if(updating_menu_state != 0)	return D_O_K;
    if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return 1;
    i = win_scanf("Generate a dummy signal \n"
                  "of Dirac pics with multi poisson distribution\n"
                  "%R in this plot %r in a new plot\n"
                  "Total number of points %10d \n"
                  "with a mean period %8d a Poisson order %8d\n"
                  "and an amplitude %8f seed %8d\n"
                  "produce duration plot %b\n"
                  ,&new_plot,&nx, &per,&np,&amp,&idum,&do_histo);
    if (i == WIN_CANCEL)	return D_O_K;
    if (new_plot)
    {
        if ((op = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
            return win_printf_OK("cannot create plot");
        ds = op->dat[0];
        set_plot_title(op, "\\stack{{Dirac random signal}"
                       "{Per %d poisson order %d amplitude %g}}",per,np,amp);
    }
    else
    {
        if ((ds = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
            return win_printf_OK("cannot create data set");
    }
    prob = ((float)np)/per;
    if (do_histo)
    {
        if ((dsh = create_and_attach_one_ds(op, 16, 16, 0)) == NULL)
            return win_printf_OK("cannot create data set");
        if ((dsp = create_and_attach_one_ds(op, 16, 16, 0)) == NULL)
            return win_printf_OK("cannot create data set");
        dsh->source = my_sprintf(NULL, "Dirac random signal duration"
                                 " Per %d poisson order %d amplitude %g",per,np,amp);
        dsp->source = my_sprintf(NULL, "Dirac random signal proba"
                                 " Per %d poisson order %d amplitude %g",per,np,amp);

    }
    if (dsh)   dsh->nx = dsh->ny = 0;
    if (dsp)   dsp->nx = dsp->ny = 0;
    for (i=0, j = 0, k = 0 ; i< nx ; i++, k++)
    {
        ds->xd[i] = i;
        j += (ran1(&idum) < prob) ? 1 : 0;
        if (j >= np)
        {
            ds->yd[i] = amp;
            if (dsh)   add_new_point_to_ds(dsh,dsh->nx,k);
            j = k = 0;
        }
        else ds->yd[i] = 0;
    }
    for (i=0 ; dsp != NULL && i< 10*per ; i++)
    {
        val = 1;
        tmp = (float)i/per;
        for(j = 1; j < np; j++)
            val *= tmp/j;
        if (np == 1)  val = (float)1/per;
        val *= exp(-tmp);
        add_new_point_to_ds(dsp,i,val);
    }
    ds->source = my_sprintf(NULL, "Dirac random signal"
                            " Per %d poisson order %d amplitude %g",per,np,amp);
    return refresh_plot(pr,pr->n_op-1);
}



int 	rand_comb_square_signal(void)
{
  int i, j, k, l;
    static float amp = 1.0, prob, tmp, val, probd;
    static int nx = 1024, per = 32, dur = 16, new_plot = 0, np = 1, do_histo = 0;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL, *dsh = NULL, *dsp = NULL;
    static int idum = 4587;

    if(updating_menu_state != 0)	return D_O_K;
    if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return 1;
    i = win_scanf("Generate a dummy signal \n"
                  "of rectangular pics with multi poisson distribution\n"
                  "%R in this plot %r in a new plot\n"
                  "Total number of points %10d \n"
                  "with a mean period %8d a Poisson order %8d\n"
                  "with a mean duration %8d\n"
                  "and an amplitude %8f seed %8d\n"
                  "produce duration plot %b\n"
                  ,&new_plot,&nx, &per,&np,&dur,&amp,&idum,&do_histo);
    if (i == WIN_CANCEL)	return D_O_K;
    if (new_plot)
    {
        if ((op = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
            return win_printf_OK("cannot create plot");
        ds = op->dat[0];
        set_plot_title(op, "\\stack{{Dirac random signal}"
                       "{Per %d poisson order %d amplitude %g}}",per,np,amp);
    }
    else
    {
        if ((ds = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
            return win_printf_OK("cannot create data set");
    }
    prob = ((float)np)/per;
    probd = ((float)np)/dur;
    if (do_histo)
    {
        if ((dsh = create_and_attach_one_ds(op, 16, 16, 0)) == NULL)
            return win_printf_OK("cannot create data set");
        if ((dsp = create_and_attach_one_ds(op, 16, 16, 0)) == NULL)
            return win_printf_OK("cannot create data set");
        dsh->source = my_sprintf(NULL, "Dirac random signal duration"
                                 " Per %d poisson order %d amplitude %g",per,np,amp);
        dsp->source = my_sprintf(NULL, "Dirac random signal proba"
                                 " Per %d poisson order %d amplitude %g",per,np,amp);

    }
    if (dsh)   dsh->nx = dsh->ny = 0;
    if (dsp)   dsp->nx = dsp->ny = 0;
    for (i=0, j = 0, k = l = 0 ; i< nx ; i++, k++)
    {
        ds->xd[i] = i;
	if (l == 0)
	  {
	    j += (ran1(&idum) < prob) ? 1 : 0;
	    if (j >= np)
	      {
		ds->yd[i] = amp;
		if (dsh)   add_new_point_to_ds(dsh,dsh->nx,k);
		j = k = 0;
		l = 1;
	      }
	    else ds->yd[i] = 0;
	  }
	else
	  {
	    j += (ran1(&idum) < probd) ? 1 : 0;
	    if (j >= np)
	      {
		ds->yd[i] = 0;
		if (dsh)   add_new_point_to_ds(dsh,dsh->nx,k);
		j = k = 0;
		l = 0;
	      }
	    else ds->yd[i] = amp;
	  }
    }
    for (i=0 ; dsp != NULL && i< 10*per ; i++)
    {
        val = 1;
        tmp = (float)i/per;
        for(j = 1; j < np; j++)
            val *= tmp/j;
        if (np == 1)  val = (float)1/per;
        val *= exp(-tmp);
        add_new_point_to_ds(dsp,i,val);
    }
    ds->source = my_sprintf(NULL, "Dirac random signal"
                            " Per %d poisson order %d amplitude %g",per,np,amp);
    return refresh_plot(pr,pr->n_op-1);
}




int 	rand_comb_signal_2(void)
{
    int i, j, k;
    int l, nk;
    static float amp = 1.0, prob, tmp, val, per = 32;
    static int nx = 1024, new_plot = 0, np = 1, do_histo = 0;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL, *dsh = NULL, *dsp = NULL;
    static int idum = 4587;
    float *fp = NULL, *sfp = NULL;

    if(updating_menu_state != 0)	return D_O_K;
    if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return 1;
    i = win_scanf("Generate a dummy signal \n"
                  "of Dirac pics with poisson distribution\n"
                  "%R in this plot %r in a new plot\n"
                  "Total number of points %10d \n"
                  "with a mean period %8f\n"
                  "and an amplitude %8f seed %8d\n"
                  "produce duration plot %b\n"
                  ,&new_plot,&nx, &per,&amp,&idum,&do_histo);
    if (i == WIN_CANCEL)	return D_O_K;
    if (new_plot)
    {
        if ((op = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
            return win_printf_OK("cannot create plot");
        ds = op->dat[0];
        set_plot_title(op, "\\stack{{Dirac random signal}"
                       "{Per %g poisson amplitude %g}}",per,amp);
    }
    else
    {
        if ((ds = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
            return win_printf_OK("cannot create data set");
    }
    prob = ((float)np)/per;
    if (do_histo)
    {
        if ((dsh = create_and_attach_one_ds(op, 16, 16, 0)) == NULL)
            return win_printf_OK("cannot create data set");
        if ((dsp = create_and_attach_one_ds(op, 16, 16, 0)) == NULL)
            return win_printf_OK("cannot create data set");
        dsh->source = my_sprintf(NULL, "Dirac random signal duration"
                                 " Per %g  amplitude %g",per,amp);
        dsp->source = my_sprintf(NULL, "Dirac random signal proba"
                                 " Per %g poisson amplitude %g",per,amp);
    }
    if (dsh)   dsh->nx = dsh->ny = 0;
    if (dsp)   dsp->nx = dsp->ny = 0;

    nk = (int)(2*prob) + 10;

    fp = (float*)calloc(nk,sizeof(float));
    sfp = (float*)calloc(nk,sizeof(float));

    if(fp == NULL || sfp == NULL)
        return win_printf_OK("cannot create arrays");

    for (i=0, j = 1; i< nk ; i++)
    {
        if (i == 0)
        {
            fp[i] = exp(-prob);
            sfp[i] = fp[i];
        }
        else
        {
            j *= i;
            fp[i] = pow(prob,i) * exp(-prob)/j;
            sfp[i] = sfp[i-1] + fp[i];
        }
    }
    if (sfp[nk-1] < 0.999)
        win_printf("sfp[%d] = %g",nk-1,sfp[nk-1]);
    for (i=0, j = 0, k = 0 ; i< nx ; i++, k++)
    {
        ds->xd[i] = i;
        for (l = 0, tmp = ran1(&idum); l < nk && tmp > sfp[l]; l++);
        ds->yd[i] = l*amp;
        if (l > 0 && do_histo)
        {
            if (dsh)   add_new_point_to_ds(dsh,dsh->nx,k);
            j = k = 0;
        }
    }
    for (i=0 ; dsp != NULL && i< 10*per ; i++)
    {
        val = 1;
        tmp = (float)i/per;
        for(j = 1; j < np; j++)
            val *= tmp/j;
        if (np == 1)  val = (float)1/per;
        val *= exp(-tmp);
        add_new_point_to_ds(dsp,i,val);
    }
    ds->source = my_sprintf(NULL, "Dirac random signal"
                            " Per %g poisson amplitude %g",per,amp);

    free(fp);
    free(sfp);
    return refresh_plot(pr,pr->n_op-1);
}



int 	generate_gaussian(void)
{
    int i;
    static float x0 = 0, a = 1.0, b = 1.0, xs = -5, xe = 5;
    static int nx = 1024,  new_plot = 0;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;

    if(updating_menu_state != 0)	return D_O_K;
    if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return 1;
    i = win_scanf("Generate a Gaussian signal \n"
                  "%R in this plot %r in a new plot\n"
                  "\n\nf(t) = \\frac{b}{\\sqrt{2\\pi a}} exp-[\\frac{(x - x_0)^2}{2.a^2}]\n\n"
                  "Parameter x_0 = %12f a = %12f \nb = %12f\n"
                  "Total number of points %10d \n"
                  "Starting a x_s = %12f ending at x_e = %12f\n"
                  ,&new_plot,&x0,&a,&b,&nx, &xs,&xe);
    if (i == WIN_CANCEL)	return D_O_K;
    if (new_plot)
    {
        if ((op = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
            return win_printf_OK("cannot create plot");
        ds = op->dat[0];
        set_plot_title(op, "\\stack{{f(t) = \\frac{b}{\\sqrt{2\\pi a}} exp-[\\frac{(x - x_0)^2}{2.a^2}]}"
                       "{x_0 %g, a %g, b %g}}",x0,a,b);
    }
    else
    {
        if ((ds = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
            return win_printf_OK("cannot create data set");
    }
    for (i=0 ; i< nx ; i++)
    {
        ds->xd[i] = xs + (i*(xe-xs))/nx;
        ds->yd[i] = Y_equal_a_gaussian(ds->xd[i], x0, a, b);
    }
    ds->source = my_sprintf(NULL, "f(t) = \\frac{b}{\\sqrt{2\\pi a}} exp-[\\frac{(x - x_0)^2}{2.a^2}] "
                            "x_0 %g, a %g, b %g",x0,a,b);
    return refresh_plot(pr,pr->n_op-1);
}

/*
   fit parameter a in y = a*exp(-x/tau)
   by minimisation of chi^2 with the last point corresponding to the cumulative of all x

*/



int	find_best_a_of_exp_fit_of_bound_histogram(d_s *dsi,    // input data set
						  int verbose, // issue explicit error message
						  double tau,  // the imposed tau value
						  double *a,   // the best a value
						  double *E,   // the chi^2
						  double bin,  // the histogram bin
						  double max_xx) // the saturation point in x
{
    int i, j;
    double fy, f, x, y, fx, er, la = 0, xb;


    if (dsi == NULL)
    {
        if (verbose) win_printf("No valid data \n");
        return 1;
    }
    if (dsi->ye == NULL)
    {
        if (verbose) win_printf("No valid data error in Y \n");
        return 2;
    }
    if (bin <= 0)
    {
      if (verbose) win_printf("No a valid bin value %g\n",bin);
        return 2;
    }
    for (i = 0, f = fy = 0; i < dsi->nx; i++)
    {
        er = dsi->ye[i];
        x = dsi->xd[i];
        y = dsi->yd[i];
        xb = bin*(int)(x/bin);
        fx = tau * exp(-xb/tau);
        fx -= ((int)(x/bin) < (int)(max_xx/bin)) ? tau * exp(-(xb+bin)/tau) : 0;
        if (er > 0)
            {
                fy += y*fx/(er*er);
                f += (fx*fx)/(er*er);
            }
    }
    if (f != 0)    la = fy/f;
    if (a) *a = la;
    else return 2;
    for (i = 0, j = 0, *E = 0; i < dsi->nx; i++)
    {
        x = dsi->xd[i];
        y = dsi->yd[i];
        xb = bin*(int)(x/bin);
        fx = la * tau * exp(-xb/tau);
        fx -= ((int)(x/bin) < (int)(max_xx/bin)) ? la*tau*exp(-(xb+bin)/tau) : 0;
        er = dsi->ye[i];
        f = y - fx;
        if (er > 0)
            {
                *E += (f*f)/(er*er);
                j++;
            }
    }
    return 0;
}



d_s *fit_a_exp_t_over_tau_for_bound_histo_with_er_full(O_p *op, d_s *dsi, double *af, double *tau, double tau_err_rel, double bin, double max_xx, int match_x, double *chi2, bool prompt)
{
    int i;
    char st[256] = {0};
    d_s *dsd = NULL;
    float x, tmp, xmin, xmax;
    int nf = 1024;
    double a, dt, E = 0, E1 = 0, ert, tauu, xb, fx, binu, max_xxu;

    if (dsi == NULL) return NULL;

    for (i = 0, xmin = xmax = dsi->xd[0]; i < dsi->nx; i++)
    {
        x = dsi->xd[i];
        xmin = (x < xmin) ? x : xmin;
        xmax = (x > xmax) ? x : xmax;
    }
    if (op != NULL)
      {
	tauu = (op->dx > 0) ? *tau/op->dx : *tau;
	binu = (op->dx > 0) ? bin/op->dx : bin;
	max_xxu = (op->dx > 0) ? max_xx/op->dx : max_xx;
      }
    else
      {
	tauu = *tau;
	binu = bin;
	max_xxu = max_xx;
      }
    //win_printf("\\tau  =%g  \\tau_r = %g, bin = %g bin_r = %g\nmax %g max_r %g"
	//       ,*tau,tauu,bin,binu,max_xx, max_xxu);
    i = find_best_a_of_exp_fit_of_bound_histogram(dsi, 1, tauu, &a, &E, binu, max_xxu);
    if (i)  return (d_s *) win_printf_ptr("Error %d",i);

    dt = tauu/10;
    ert = tauu*tau_err_rel;

    for (i = 0; fabs(dt) > ert && i < 1024; i++)
    {
        tauu += dt;
        find_best_a_of_exp_fit_of_bound_histogram(dsi, 1, tauu, &a, &E1, binu, max_xxu);
        if (E1 > E)   dt = -dt/2;
        E = E1;
    }

    if (prompt && i > 1023) win_printf("%d iter \\tau = %g, \\chi^2 = %g",i,tauu,E);
    if (match_x) nf = dsi->nx;
    if (op != NULL)
    {
        if ((dsd = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
            return (d_s *) win_printf_ptr("cannot create data set");
        *tau = tauu*op->dx;
    }
    else
    {
        if ((dsd = build_data_set(nf, nf)) == NULL)
            *tau = tauu;
    }

    //win_printf("found \\tau  =%g  \\tau_r = %g, bin = %g bin_r = %g\nmax %g max_r %g"
	//       ,*tau,tauu,bin,binu,max_xx, max_xxu);

//win_scanf("\\tau_r %6lf a = %6lf",&tauu,&a);
    if (match_x)
    {
        for (i = 0; i < dsi->nx; i++)
        {
            x = dsi->xd[i];
            dsd->xd[i] = x;
	    xb = binu*(int)(x/binu);
	    if ((int)(x/binu) >= (int)(max_xxu/binu))
	      {
		fx = a * tauu * exp(-xb/tauu);
	      }
	    else
	      {
		fx = a * tauu * (exp(-xb/tauu) - exp(-(xb+binu)/tauu));
	      }
            dsd->yd[i] = fx;
        }
    }
    else
    {
        tmp = (xmax-xmin)/40;
        xmin -= tmp;
        xmax += tmp;
        for (i = 0, tmp = xmin ; i < dsd->nx; i++, tmp += (xmax-xmin)/dsd->nx)
        {
            x = dsd->xd[i] = (float)(tmp);
	    xb = binu*(int)(x/binu);
	    if ((int)(x/binu) >= (int)(max_xxu/binu))
	      {
		fx = a * tauu * exp(-xb/tauu);
	      }
	    else
	      {
		fx = a * tauu * (exp(-xb/tauu) - exp(-(xb+binu)/tauu));
	      }
            dsd->yd[i] = fx;
        }
    }
    if (op != NULL)
    {
        a = a*op->dy*tauu;
        sprintf(st,"\\fbox{\\pt8\\stack{{\\sl y = a.exp(-t/\\tau)}"
                "{a =  %g%s}{\\tau  = %g%s}{1/\\tau  = %g}"
                "{\\chi^2 = %g, n = %d}}}"
                ,a,(op->y_unit) ? op->y_unit:"",*tau
                ,(op->x_unit) ? op->x_unit:"",((*tau!=0)?(double)1/(*tau):0),E,dsi->nx-2);
        set_ds_plot_label(dsd, op->x_lo + (op->x_hi - op->x_lo)/4,
                          op->y_hi - (op->y_hi - op->y_lo)/4, USR_COORD,"%s",st);
    }
    *af = a;
    if (chi2)
        *chi2 = E;
    return dsd;

}
d_s *fit_a_exp_t_over_tau_for_bound_histo_with_er(O_p *op, d_s *dsi, double *af, double *tau, double tau_err_rel, double bin, double max_xx, int match_x)
{
  return fit_a_exp_t_over_tau_for_bound_histo_with_er_full(op, dsi, af, tau, tau_err_rel, bin, max_xx, match_x, NULL, true);
}



int	do_fit_a_exp_t_over_tau_for_bound_histo_with_er(void)
{
    int i;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *dsi = NULL;
    double a;
    static int match_x = 0, imax_xx = 0;
    static double tau = 0.1, err = 0.001, bin = 0.1;
    double max_xx = 10;

    if(updating_menu_state != 0)	return D_O_K;
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
        return win_printf_OK("cannot find data");
    bin = (dsi->nx > 1) ? op->dx * (dsi->xd[1] - dsi->xd[0]) : bin;
    tau = (dsi->nx > 2) ? op->dx * (dsi->xd[dsi->nx-1] - dsi->xd[0]) : tau;
    i = win_scanf("Fit y = a.exp (-x/\\tau) bounded\n"
                  "Histogram bin size %6lf \n"
                  "Index of maximum boundary counting backward\n"
                  "(<= 0, last point =>0) %5d\n"
                  "\\tau  starting value %5lf\n"
                  "Error on \\tau  %5lf\n"
                  "fitted curve x matching data %b\n"
                  ,&bin,&imax_xx,&tau,&err,&match_x);
    if (i == WIN_CANCEL)	return OFF;
    i = dsi->nx - 1 - imax_xx;
    max_xx = (i >= 0 && i < dsi->nx) ? dsi->xd[i] : dsi->xd[dsi->nx-1];
    max_xx *= op->dx;
    fit_a_exp_t_over_tau_for_bound_histo_with_er(op, dsi, &a, &tau, err, bin, max_xx, match_x);

    return refresh_plot(pr,pr->cur_op);
}




/*
   fit parameter a in y = a*exp(-x/tau)
   by minimisation of chi^2

*/


int	find_best_a_of_exp_fit(d_s *dsi,    // input data set
                           int verbose, // issue explicit error message
                           double tau,  // the imposed tau value
                           double *a,   // the best a value
                           double *E)   // the chi^2
{
    int i, j;
    double fy, f, x, y, fx, er, la = 0;


    if (dsi == NULL)
    {
        if (verbose) win_printf("No valid data \n");
        return 1;
    }
    if (dsi->ye == NULL)
    {
        if (verbose) win_printf("No valid data error in Y \n");
        return 2;
    }
    for (i = 0, f = fy = 0; i < dsi->nx; i++)
    {
        er = dsi->ye[i];
        x = dsi->xd[i];
        y = dsi->yd[i];
        fx = exp(-x/tau);
        if (er > 0)
        {
            fy += y*fx/(er*er);
            f += (fx*fx)/(er*er);
        }
    }
    if (f != 0)    la = fy/f;
    if (a) *a = la;
    else return 2;
    for (i = 0, j = 0, *E = 0; i < dsi->nx; i++)
    {
        x = dsi->xd[i];
        y = dsi->yd[i];
        fx = la*exp(-x/tau);
        er = dsi->ye[i];
        f = y - fx;
        if (er > 0)
        {
            *E += (f*f)/(er*er);
            j++;
        }
    }
    return 0;
}


int	do_fit_a_exp_t_over_tau_with_er(void)
{
    int i;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *dsi = NULL;
    double a;
    static int match_x = 0;
    static double tau = 0.1, err = 0.001;

    if(updating_menu_state != 0)	return D_O_K;
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
        return win_printf_OK("cannot find data");
    i = win_scanf("Fit y = a.exp (-x/\\tau) \n"
                  "\\tau  starting value %12lf\n"
                  "Error on \\tau  %12lf\n"
                  "fitted curve x matching data %b\n"
                  ,&tau,&err,&match_x);
    if (i == WIN_CANCEL)	return OFF;

    fit_a_exp_t_over_tau_with_er(op, dsi, &a, &tau, err, match_x);

    return refresh_plot(pr,pr->cur_op);
}

int	do_fit_a_exp_t_over_tau_with_er_multiple(void)
{
    int i;
    float m = 0, mi=0,ma = 0;
    float s2 = 0;
    pltreg *pr = NULL;
    O_p *op = NULL, *ophisto = NULL, *optimes = NULL;
    d_s *dsi = NULL, *dss = NULL, *dshisto = NULL, *dstimes = NULL;
    double *ar = NULL;
    double *tauar = NULL;
    static int low_count = 0;
    static float min_bin_size = 0.005;
    static double time_tolerance = 0.001;
    float bin = 0;
    d_s *ds2 = NULL;
    float median = 0 ;
    static float min_detect = 0.005;
    float zmag  = 0;
    if(updating_menu_state != 0)	return D_O_K;
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
        return win_printf_OK("cannot find data");
    i = win_scanf("Do histograms and fit y = a.exp (-x/\\tau) \n"
                  "Histo minimal bin size %f \n"
                  "Do not fit bins with counts smaller than %d \n"
                  "Minimum detection value %f\n"
                  "Fit params : \n"
                  "Error on \\tau  %12lf\n"
                  ,&min_bin_size,&low_count,&min_detect,&time_tolerance);
    if (i == WIN_CANCEL)	return OFF;
    ar = calloc(op->n_dat,sizeof(double));
    tauar = calloc(op->n_dat,sizeof(double));
    for (int j = 0;j<op->n_dat;j++)
    {
      dss = op->dat[j];
      find_ds_median_in_y(dss,&median);
      bin = median / 3;
      if (bin < min_bin_size) bin = min_bin_size;
      ophisto = create_and_attach_one_plot(pr,16,16,0);
      if (sscanf(op->dat[j]->treatement,"Zmag = %f",&zmag) == 1)  set_plot_title(ophisto,"Zmag =  %f",zmag);
      else    set_plot_title(ophisto,"Data set %d",j);
      dshisto = histo_background(dss,bin,NULL);
      for (int k = 0;k<dshisto->nx;)
      {
        if ((dshisto->xd[k] < min_detect) || (dshisto->yd[k] < low_count) )
        {
          remove_point_from_data_set(dshisto,k);
        }
        else k++;
      }
      dshisto->color = lightgreen;
      ophisto->dat[0] = dshisto;
      ar[j] = dss->nx;
      tauar[j] = median/3;
      fit_a_exp_t_over_tau_with_er(ophisto, dshisto, ar+j, tauar+j, time_tolerance, 1);
      //free_data_set(dshisto);
    }
    optimes = create_and_attach_one_plot(pr,16,16,0);
    set_plot_title(optimes,"Fitted time for each data set");
    set_plot_x_title(optimes,"Zmag (mm)");
    set_plot_y_title(optimes,"Times (s)");
    create_attach_select_y_un_to_op(optimes, IS_SECOND, 0
                                    , (float)1, 0, 0, "s");
    create_attach_select_x_un_to_op(optimes, IS_METER, 0 , (float)1, -3, 0, "mm");

    dstimes = optimes->dat[0];
    dstimes->nx = dstimes->ny = 0;
    for (int j =0;j<op->n_dat;j++)
    {
      if (sscanf(op->dat[j]->treatement,"Zmag = %f",&zmag) == 1)  add_new_point_to_ds(dstimes,zmag,(float) tauar[j]);
        else add_new_point_to_ds(dstimes,j,(float) tauar[j]);
    }
    free(ar);
    free(tauar);


    return refresh_plot(pr,ophisto);
}



d_s *fit_a_exp_t_over_tau_with_er_full(O_p *op, d_s *dsi, double *af, double *tau, double tau_err_rel, int match_x, double *chi2, bool prompt)
{
    int i;
    char st[256] = {0};
    d_s *dsd = NULL;
    float x, tmp, xmin, xmax;
    int nf = 1024;
    double a, dt, E = 0, E1 = 0, ert, tauu;

    if (dsi == NULL) return NULL;

    for (i = 0, xmin = xmax = dsi->xd[0]; i < dsi->nx; i++)
    {
        x = dsi->xd[i];
        xmin = (x < xmin) ? x : xmin;
        xmax = (x > xmax) ? x : xmax;
    }
    if (op != NULL) tauu = (op->dx > 0) ? *tau/op->dx : *tau;
    else tauu = *tau;

    i = find_best_a_of_exp_fit(dsi, 1, tauu, &a, &E);
    if (i)  return (d_s *) win_printf_ptr("Error %d",i);

    dt = tauu/10;
    ert = tauu*tau_err_rel;

    for (i = 0; fabs(dt) > ert && i < 1024; i++)
    {
        tauu += dt;
        find_best_a_of_exp_fit(dsi, 1, tauu, &a, &E1);
        if (E1 > E)   dt = -dt/2;
        E = E1;
    }

    if (prompt && i > 1023) win_printf("%d iter \\tau = %g, \\chi^2 = %g",i,tauu,E);
    if (match_x) nf = dsi->nx;
    if (op != NULL)
    {
        if ((dsd = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
            return (d_s *) win_printf_ptr("cannot create data set");
        *tau = tauu*op->dx;
    }
    else
    {
        if ((dsd = build_data_set(nf, nf)) == NULL)
            *tau = tauu;
    }

    if (match_x)
    {
        for (i = 0; i < dsi->nx; i++)
        {
            x = dsi->xd[i];
            dsd->xd[i] = x;
            dsd->yd[i] = a*exp(-x/tauu);
        }
    }
    else
    {
        tmp = (xmax-xmin)/40;
        xmin -= tmp;
        xmax += tmp;
        for (i = 0, tmp = xmin ; i < dsd->nx; i++, tmp += (xmax-xmin)/dsd->nx)
        {
            dsd->xd[i] = (float)(tmp);
            dsd->yd[i] = a*exp(-tmp/tauu);
        }
    }
    if (op != NULL)
    {
        a = a*op->dy;
        sprintf(st,"\\fbox{\\pt8\\stack{{\\sl y = a.exp(-t/\\tau)}"
                "{a =  %g%s}{\\tau  = %g%s}{1/\\tau  = %g}"
                "{\\chi^2 = %g, n = %d}}}"
                ,a,(op->y_unit) ? op->y_unit:"",*tau
                ,(op->x_unit) ? op->x_unit:"",((*tau!=0)?(double)1/(*tau):0),E,dsi->nx-2);
        set_ds_plot_label(dsd, op->x_lo + (op->x_hi - op->x_lo)/4,
                          op->y_hi - (op->y_hi - op->y_lo)/4, USR_COORD,"%s",st);
    }
    *af = a;
    if (chi2)
        *chi2 = E;
    return dsd;

}
d_s *fit_a_exp_t_over_tau_with_er(O_p *op, d_s *dsi, double *af, double *tau, double tau_err_rel, int match_x)
{
    return fit_a_exp_t_over_tau_with_er_full(op, dsi, af, tau, tau_err_rel, match_x, NULL, true);
}



/*
   fit parameter a in y = a*exp(-x/tau) + b
   by minimisation of chi^2
now in plot_math

int	find_best_a_an_b_of_exp_fit(d_s *dsi,    // input data set
                                int verbose, // issue explicit error message
                                int error_type, // the type of error in data
                                double tau,  // the imposed tau value
                                double *a,   // the best a value
                                double *b,   // the best b value
                                double *E)   // the chi^2
{
    int i, j;
    double fy, f, x, y, fx, er, la, lb;
    double sy = 0, sy2 = 0, sfx = 0, sfx2 = 0, sfxy = 0, sig_2, ny = 0;

    if (dsi == NULL)
    {
        if (verbose) win_printf("No valid data \n");
        return 1;
    }
    for (i = 0, f = fy = 0; i < dsi->nx; i++)
    {
        x = dsi->xd[i];
        y = dsi->yd[i];
        fx = exp(-x/tau);
        if (error_type == 0 && dsi->ye != NULL)   er = dsi->ye[i];
        else if (error_type == 1) er = fx;
        else er = 1;
        if (er > 0)
        {
            sig_2 = (double)1/(er*er);
            sy += y*sig_2;
            sy2 += y*y*sig_2;
            sfx += fx*sig_2;
            sfx2 += fx*fx*sig_2;
            sfxy += y*fx*sig_2;
            ny += sig_2;
        }
    }
    lb = (sfx2 * ny) - sfx * sfx;
    if (lb == 0)
    {
        if (verbose) win_printf_OK("\\Delta  = 0 \n can't fit that!");
        return 3;
    }
    la = sfxy * ny - sfx * sy;
    la /= lb;
    lb = (sfx2 * sy - sfx * sfxy)/lb;
    if (a) *a = la;
    if (b) *b = lb;
    for (i = 0, j = 0, *E = 0; i < dsi->nx; i++)
    {
        x = dsi->xd[i];
        y = dsi->yd[i];
        fx = la*exp(-x/tau) + lb;
        if (error_type == 0 && dsi->ye != NULL)   er = dsi->ye[i];
        else if (error_type == 1) er = fx;
        else er = 1;
        f = y - fx;
        if (er > 0)
        {
            *E += (f*f)/(er*er);
            j++;
        }
    }
    return 0;
}

*/


int	do_fit_a_exp_t_over_tau_plus_b_with_er(void)
{
    int i;
    char st[256] = {0};
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *dsi = NULL, *dsd = NULL;
    float x, tmp, xmin, xmax;
    int nf = 1024;
    double a, b, dt, E = 0, E1 = 0, ert, tauu;
    static int match_x = 0, error_type = 0;
    static double tau = 0.1, err = 0.0001;

    if(updating_menu_state != 0)	return D_O_K;
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
        return win_printf_OK("cannot find data");
    if (dsi->ye != NULL)
    {
        error_type = 0;
        i = win_scanf("Fit y = a.exp (-x/\\tau) +b \n"
                      "\\tau  starting value %12lf\n"
                      "Relative error on \\tau  %12lf\n"
                      "Error type in data: error bars %R f(x) %r constant %r\n"
                      "fitted curve x matching data %b\n"
                      ,&tau,&err,&error_type,&match_x);
        if (i == WIN_CANCEL)	return OFF;
    }
    else
    {
        i = win_scanf("Fit y = a.exp (-x/\\tau) +b \n"
                      "\\tau  starting value %12lf\n"
                      "Relative error on \\tau  %12lf\n"
                      "Error type in data: f(x) %R constant %r\n"
                      "fitted curve x matching data %b\n"
                      ,&tau,&err,&error_type,&match_x);
        if (i == WIN_CANCEL)	return OFF;
        error_type++;
    }


    for (i = 0, xmin = xmax = dsi->xd[0]; i < dsi->nx; i++)
    {
        x = dsi->xd[i];
        xmin = (x < xmin) ? x : xmin;
        xmax = (x > xmax) ? x : xmax;
    }

    tauu = (op->dx > 0) ? tau/op->dx : tau;


    i = find_best_a_an_b_of_exp_fit(dsi, 1, error_type, tauu, &a, &b, &E);
    if (i)
    {
        win_printf("Error %d",i);
        return D_O_K;
    }
    dt = tauu/10;
    ert = tauu*err;
    for (i = 0; fabs(dt) > ert && i < 1024; i++)
    {
        tauu += dt;
        find_best_a_an_b_of_exp_fit(dsi, 1, error_type, tauu, &a, &b, &E1);
        if (E1 > E)   dt = -dt/2;
        E = E1;
    }
    if (i > 1023)
        win_printf("%d iter \\tau = %g, a = %g b = %g \\chi^2 = %g",i,tauu,a,b,E);
    if (match_x) nf = dsi->nx;
    if ((dsd = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("cannot create data set");
    tau = tauu*op->dx;
    win_printf("\\tau = %g, \\tau_u %g",tau, tauu);
    if (match_x)
    {
        for (i = 0; i < dsi->nx; i++)
        {
            x = dsi->xd[i];
            dsd->xd[i] = x;
            dsd->yd[i] = a*exp(-x/tauu) +b;
        }
    }
    else
    {
        tmp = (xmax-xmin)/40;
        xmin -= tmp;
        xmax += tmp;
        for (i = 0, tmp = xmin ; i < dsd->nx; i++, tmp += (xmax-xmin)/dsd->nx)
        {
            dsd->xd[i] = (float)(tmp);
            dsd->yd[i] = a*exp(-tmp/tauu) +b;
        }
    }
    b = op->ay+(op->dy*b);
    a = a*op->dy;

    sprintf(st,"\\fbox{\\pt8\\stack{{\\sl y = a.exp(-t/\\tau) + b}"
            "{a =  %g%s}{b = %g%s}{\\tau  = %g%s}{1/\\tau  = %g}"
            "{\\chi^2 = %g, n = %d}}}"
            ,a,(op->y_unit) ? op->y_unit:"",b,(op->y_unit) ? op->y_unit:" ",tau
            ,(op->x_unit) ? op->x_unit:"",((tau!=0)?(double)1/tau:0),E,dsi->nx-3);
    set_ds_plot_label(dsd, op->x_lo + (op->x_hi - op->x_lo)/4,
                      op->y_hi - (op->y_hi - op->y_lo)/4, USR_COORD,"%s",st);
    return refresh_plot(pr,pr->cur_op);
}


int	do_cumulative_histogram(void)
{
    int i, npt = 0, tmpi;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *dsi = NULL, *dsd = NULL;
    float tmp, tmptot, a;


    if(updating_menu_state != 0)	return D_O_K;
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
        return win_printf_OK("cannot find data");

    for (i = npt = 0; i < dsi->nx && dsi->ye != NULL; i++)
    {
        if (dsi->yd[i] != 0)
        {
            a = (dsi->ye[i]*dsi->ye[i])/(dsi->yd[i]*dsi->yd[i]);
            tmp = ( a != 0) ? (sqrt(1+4*a) + 1)/(2*a) : 0;
            tmpi = (int)(tmp+0.5);
            npt += (tmpi > 0) ? tmpi : 0;
        }
    }


    dsd = duplicate_data_set(dsi,NULL);
    for (i = dsd->nx - 2; i >= 0; i--)
    {
        dsd->yd[i] = dsd->yd[i+1] + dsd->yd[i];
    }
    for (i = 0; i < dsd->nx; i++)
    {
        tmp = dsd->yd[i];
        tmptot = dsd->yd[0];
        if (tmptot > 0) tmp /= tmptot;
        if ((dsd->ye) && (npt > 0))
        {
            dsd->ye[i] = tmptot*sqrt(tmp*(1-tmp)/npt);
            if (dsd->ye[i] == 0)
            {
                tmp = (float)1/npt;
                dsd->ye[i] = tmptot*sqrt(tmp*(1-tmp)/npt);
            }
        }
    }
    inherit_from_ds_to_ds(dsd, dsi);
    set_ds_treatement(dsd, "Backward cumulative histogram with %d retrieved point",npt);
    add_one_plot_data (op, IS_DATA_SET, (void *)dsd);
    return refresh_plot(pr,pr->cur_op);
}


//** - array of data points (double **data)
//** - number of data points (int n)
// ** - dimension (int m)
// ** - desired number of clusters (int k)
// ** - error tolerance (double t)
// **   - used as the stopping criterion, i.e. when the sum of
// **     squared euclidean distance (standard error for k-means)
// **     of an iteration is within the tolerable range from that
// **     of the previous iteration, the clusters are considered
// **     "stable", and the function returns
// **   - a suggested value would be 0.0001


int *k_means(float **data, int n, int m, int k, float t, float * * centroids) {
		/* output cluster label for each data point */
		int * labels = (int * ) calloc(n, sizeof(int));

		int h, i, j; /* loop counters, of course :) */
		int * counts = (int * ) calloc(k, sizeof(int)); /* size of each cluster */
		float old_error, error = DBL_MAX; /* sum of squared euclidean distance */
		float * * c = centroids ? centroids : (float * * ) calloc(k, sizeof(float * ));
		float * * c1 = (float * * ) calloc(k, sizeof(float * )); /* temp centroids */


		/****
		 ** initialization */

		for (h = i = 0; i < k; h += n / k, i++) {
				c1[i] = (float * ) calloc(m, sizeof(float));
				if (!centroids) {
						c[i] = (float * ) calloc(m, sizeof(float));
				}
				/* pick k points as initial centroids */
				for (j = m; j-- > 0; c[i][j] = data[h][j]);
		}

		/****
		 ** main loop */

		do {
				/* save error from last step */
				old_error = error, error = 0;

				/* clear old counts and temp centroids */
				for (i = 0; i < k; counts[i++] = 0) {
						for (j = 0; j < m; c1[i][j++] = 0);
				}

				for (h = 0; h < n; h++) {
						/* identify the closest cluster */
						float min_distance = DBL_MAX;
						for (i = 0; i < k; i++) {
								float distance = 0;
								for (j = m; j-- > 0; distance += pow(data[h][j] - c[i][j], 2));
								if (distance < min_distance) {
										labels[h] = i;
										min_distance = distance;
								}
						}
						/* update size and temp centroid of the destination cluster */
						for (j = m; j-- > 0; c1[labels[h]][j] += data[h][j]);
						counts[labels[h]]++;
						/* update standard error */
						error += min_distance;
				}

				for (i = 0; i < k; i++) { /* update all centroids */
						for (j = 0; j < m; j++) {
								c[i][j] = counts[i] ? c1[i][j] / counts[i] : c1[i][j];
						}
				}

		} while (fabs(error - old_error) > t);

		/****
		 ** housekeeping */

		for (i = 0; i < k; i++) {
				if (!centroids) {
						free(c[i]);
				}
				free(c1[i]);
		}

		if (!centroids) {
				free(c);
		}
		free(c1);

		free(counts);

		return labels;
}


int *k_means_on_dataset(d_s *ds, int k, float t, int start, int end, float ** centroids)
{
      int *res = NULL ;
      float **data = NULL;
      data = calloc(end-start+1,sizeof(float*));
      for (int i =0;i<end-start+1;i++)
      {
        data[i] = calloc(1,sizeof(float));
        data[i][0] = ds->yd[start+i];
      }
      res = k_means(data,end-start+1,1,k,t,centroids);
      for (int i =0;i<end-start+1;i++)
      {
        free(data[i]);
      }
      free(data);
			return res;
}


int do_k_mean_on_dataset(void)
{
  if(updating_menu_state != 0)	return D_O_K;
	int i;
	static int k = 2;
	static float t = 0.0001;
	O_p *op = NULL;
	d_s *ds = NULL, *dsclus = NULL;
	pltreg *pr = NULL;
	int *cluster = NULL;

	i = win_scanf("Please enter the number of clusters you want to find : %6d \n"
										"Please enter the threshold for convergency %6f\n",&k,&t);

	if (i == WIN_CANCEL) return 0;

	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3) return win_printf_OK("cannot find data");


	cluster = k_means_on_dataset(ds,k,t,0,ds->nx-1,NULL);
	dsclus = create_and_attach_one_ds(op,ds->nx,ds->ny,0);
	inherit_from_ds_to_ds(dsclus,ds);
	for (i = 0;i<ds->nx;i++)
	{
		dsclus->yd[i] = (float) cluster[i];
		dsclus->xd[i] = ds->xd[i];
	}
	set_ds_treatement(dsclus,"k mean with k = %d\n",k);
	free(cluster);
	return 0;
}

int running_two_levels_k_mean_on_datasset(d_s* ds, d_s*dsr, int start, int end, float t, int k, int window, int overlap)
{
  int nwindow = 0;
  float tot0 = 0;
  int *res = NULL;
  float xmoy = 0;
  if (end<start) return 1;
  if (end > ds->nx) return 1;
  if (start < 0) start = 0;
  for (nwindow =0;nwindow*overlap+window<end;nwindow++);
  dsr = build_adjust_data_set(dsr,nwindow,nwindow);
  dsr->nx = dsr-> ny = 0;
  res = calloc(window,sizeof(int));
  float **centroids;
  centroids = calloc(k,sizeof(float*));
  int choose = 0;
  for (int j=0;j<k;j++) centroids[j] = calloc(1,sizeof(float));
  for (int j = 0;j<nwindow;j++)
  {
    tot0 = 0;
    xmoy = 0;
    res = k_means_on_dataset(ds,k,t,start+overlap*j,start+overlap*j+window-1,centroids); //kmean on data
    if (centroids[0][0] < centroids[1][0]) choose = 1;
    else choose = 0;
    for (int kk=0;kk<window;kk++)
    {
      xmoy += ds->xd[start+j*overlap+kk]/window ;
      if (res[kk] == choose) tot0 += 1.0/window;
    }
    add_new_point_to_ds(dsr,xmoy,fabs(centroids[1][0]-centroids[0][0]));
}
for (int j=0;j<k;j++) free(centroids[j]);
free(centroids);
free(res);
return 0;
}

int do_draw_first_proba_kmean_on_dataset_by_zmag(void)
{
  if(updating_menu_state != 0)	return D_O_K;
  int i;
  static int k = 2;
  float tot0 = 0;
  int *res = NULL;
  O_p *op = NULL, *opzmagprop = NULL;
  d_s *ds = NULL, *dsr = NULL, *dszmag = NULL;
  pltreg *pr = NULL;
  static  float t = 0.0001;
  float zmag = 0;
  i = win_scanf("Please enter the number of cluster %d\n"
  "The threshold for convergency %6f\n",&k,&t);

  if (i == WIN_CANCEL) return 0;

  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3) return win_printf_OK("cannot find data");

  if (op->n_dat != 2) return (win_printf_OK("bull shit function take only plots with only to ds one for zmag and the other(the selected) for data\n"));
  if (ds == op->dat[0])  dszmag = op->dat[1];
  else dszmag = op->dat[0];
  if (dszmag->nx != ds->nx) win_printf_OK("no good zmag point number");
  opzmagprop = create_and_attach_one_plot(pr,16,16,0);
  dsr = opzmagprop->dat[0];
  alloc_data_set_y_error(dsr);
  dsr->nx = dsr->ny = 0;
  zmag = dszmag->yd[0];
  int inds = 0;
  int inde = 0;
  float **centroids;
  centroids = calloc(k,sizeof(float*));
  int choose = 0;
  for (int j=0;j<k;j++) centroids[j] = calloc(1,sizeof(float));
  for (i = 0;i<dszmag->nx;i++)
  {
      if (fabs((dszmag->yd[i] - zmag)) > 0.01*fabs(zmag))
      {
        inde = i-1;
        if ((inde-inds+1) > 5000)
        {
        res = realloc(res,(inds-inde+1)*sizeof(int));
        res = k_means_on_dataset(ds,k,t,inds,inde,centroids);
        if (centroids[0][0] < centroids[1][0]) choose = 1;
        else choose = 0;
        tot0 = 0;
        for (int kk=0;kk<inde-inds;kk++)
        {
          if (res[kk] == choose) tot0 += 1.0/(inde-inds);
        }
        add_new_point_to_ds(dsr,zmag,tot0);
        dsr->ye[dsr->nx-1] = i;
      }
        zmag = dszmag->yd[i];
        inds = inde;
      }
  }
  for (int j=0;j<k;j++) free(centroids[j]);
  free(centroids);
  free(res);

  return 0;
}

int do_draw_first_proba_normal_on_dataset_by_zmag(void)
{
  if(updating_menu_state != 0)	return D_O_K;
  int i;

  O_p *op = NULL, *opzmagprop = NULL;
  d_s *ds = NULL, *dsr = NULL, *dszmag = NULL;
  pltreg *pr = NULL;
  static  float threshold = 0.01;
  float mean =0;
  float sigma = 0;
  float t0,t1,sigma0,sigma1 = 0;
  float aret=0;
  float zmag = 0;


  if (i == WIN_CANCEL) return 0;

  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3) return win_printf_OK("cannot find data");

  if (op->n_dat != 2) return (win_printf_OK("bull shit function take only plots with only to ds one for zmag and the other(the selected) for data\n"));
  if (ds == op->dat[0])  dszmag = op->dat[1];
  else dszmag = op->dat[0];
  if (dszmag->nx != ds->nx) win_printf_OK("no good zmag point number");
  opzmagprop = create_and_attach_one_plot(pr,16,16,0);
  dsr = opzmagprop->dat[0];
  alloc_data_set_y_error(dsr);
  dsr->nx = dsr->ny = 0;
  zmag = dszmag->yd[0];
  int inds = 0;
  int inde = 0;

  for (i = 0;i<dszmag->nx;i++)
  {
      if (fabs((dszmag->yd[i] - zmag)) > 0.01*fabs(zmag))
      {
        inde = i-1;
        mean = 0;
        sigma = 0;
        if (inde-inds+1>1000)
        {

        for (int jj =inds;jj<inde;jj++)
        {
          mean+=ds->yd[jj]/(inde-inds+1);
          sigma += ds->yd[jj]*ds->yd[jj]/(inde-inds+1);
        }
        sigma = sigma - mean*mean;

        t0 = mean + sqrt(sigma)/2;
        t1 = mean - sqrt(sigma)/2;
        sigma0 = sigma/2;
        sigma1 = sigma/2;



        fit_double_gaussian(pr,op,ds, t0, t1,  sigma0,  sigma1,  0.5,  0,0,1000, threshold,0,0 ,&aret, inds,inde,NULL);

        add_new_point_to_ds(dsr,zmag,aret);
        dsr->ye[dsr->nx-1] = i;
      }
        zmag = dszmag->yd[i];
        inds = inde;
      }
  }

  return 0;
}

int do_draw_first_proba_kmean_on_dataset(void)
{
  if(updating_menu_state != 0)	return D_O_K;
  int i;
  static int k = 2;
  O_p *op = NULL;
  d_s *ds = NULL, *dsclus = NULL;
  pltreg *pr = NULL;
  static  float t = 0.0001;
  static int window = 1024;
  static int overlap = 128;

  i = win_scanf("Please enter the number of cluser %d\n"
  "The threshold for convergency %6f\n"
  "The window in frame  %d\n"
  "The overlap in frame %d\n",&k,&t,&window,&overlap);

  if (i == WIN_CANCEL) return 0;

  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3) return win_printf_OK("cannot find data");

  dsclus = create_and_attach_one_ds(op,16,16,0);

  running_two_levels_k_mean_on_datasset(ds,dsclus,0,ds->nx-1,t,k,window,overlap);
  set_ds_treatement(dsclus,"k mean with k = %d overlap = %d, window = %d\n",k,overlap,window);

  return 0;
}









MENU *stat_plot_menu(void)
{
    static MENU mn[64] = {0};

    if (mn[0].text != NULL)	return mn;

    add_item_to_menu(mn,"Gaussian noise",do_gen_gaussian,NULL,0,NULL);
    add_item_to_menu(mn,"Flat noise",do_gen_flat,NULL,0,NULL);
    add_item_to_menu(mn,"Sinus signal",generate_signal,NULL,0,NULL);
    add_item_to_menu(mn,"Dirac signal",dirac_comb_signal,NULL,0,NULL);
    add_item_to_menu(mn,"Gaussian plot",generate_gaussian,NULL,0,NULL);
    add_item_to_menu(mn,"Random dirac",rand_comb_signal,NULL,0,NULL);
    add_item_to_menu(mn,"Random dirac 2",rand_comb_signal_2,NULL,0,NULL);
    add_item_to_menu(mn,"Random rect",rand_comb_square_signal,NULL,0,NULL);
      add_item_to_menu(mn,"Fit gaussian",do_fit_Y_equal_a_gaussian,NULL,0,NULL);


    add_item_to_menu(mn,"Histogram",do_histo_non_zero,NULL,0,NULL);
    add_item_to_menu(mn,"Logarithmic histogram",do_logarithmic_binning,NULL,0,NULL);
    add_item_to_menu(mn,"Kmean",do_k_mean_on_dataset,NULL,0,NULL);
    add_item_to_menu(mn,"Running kmean",do_draw_first_proba_kmean_on_dataset,NULL,0,NULL);
    add_item_to_menu(mn,"Kmean zmag",do_draw_first_proba_kmean_on_dataset_by_zmag,NULL,0,NULL);
          add_item_to_menu(mn,"Zmag normal",do_draw_first_proba_normal_on_dataset_by_zmag,NULL,0,NULL);
    add_item_to_menu(mn,"Construct error bars", do_draw_error_bars,NULL,0,NULL);
    add_item_to_menu(mn,"Extract error bars",do_extract_error,NULL,0,NULL);
    add_item_to_menu(mn,"Construct histo bars",draw_histo_bars,NULL,0,NULL);
    add_item_to_menu(mn,"Cumulative histogram",do_cumulative_histogram,NULL,0,NULL);
    add_item_to_menu(mn,"Error of correlated data",	do_sigma_stat,	NULL,   0, NULL );
    add_item_to_menu(mn,"fit of correlated data",	do_fit_sigma_stat,	NULL,   0, NULL );
    add_item_to_menu(mn,"sigma(bin) correlated data",	do_bin_stat,	NULL,   0, NULL );
    add_item_to_menu(mn,"Fit double normal law",do_fit_double_gaussian,
                                                                        NULL,0,NULL);
    add_item_to_menu(mn,"Draw two level times from double normal",do_classify_double_gaussian_and_draw_times,NULL,0,NULL);
        add_item_to_menu(mn,"test double Poisson mixture",do_test_poisson_mixtures,NULL,0,NULL);

    add_item_to_menu(mn,"Fit y = a.exp(t/T)", do_fit_a_exp_t_over_tau_with_er,NULL,0,NULL);
    add_item_to_menu(mn,"Fit multiple y = a.exp(t/T)", do_fit_a_exp_t_over_tau_with_er_multiple,NULL,0,NULL);
    add_item_to_menu(mn,"Fit bounded histogram y = a.exp(t/T)",     do_fit_a_exp_t_over_tau_for_bound_histo_with_er,NULL,0,NULL);


    add_item_to_menu(mn,"Fit y = a.exp(t/T) + b",do_fit_a_exp_t_over_tau_plus_b_with_er,NULL,0,NULL);
    add_item_to_menu(mn,"Fit exponential",do_fit_Y_equal_a0_plus_a1_exp_moins_lt,
                     NULL,0,NULL);

    add_item_to_menu(mn,"Simualte double expo",  do_simulate_double_exponential,
                                                                                                                               NULL,0,NULL);
    add_item_to_menu(mn,"Fit double poisson law",do_fit_double_exponential,
                                      NULL,0,NULL);
    add_item_to_menu(mn,"Fit double normal law",do_fit_double_gaussian,
                                                                        NULL,0,NULL);
    add_item_to_menu(mn,"Fit double Poisson mixture",do_fit_N_poisson_mixture,
                                                                        NULL,0,NULL);


  add_item_to_menu(mn,"Redraw double poisson law",redo_fit_double_exponential,
                                                                        NULL,0,NULL);

  add_item_to_menu(mn,"Fit gaussian",do_fit_Y_equal_a_gaussian,NULL,0,NULL);
    add_item_to_menu(mn,"Boostrap gaussian",do_fit_Y_equal_a_gaussian_boostrap,
                     NULL,0,NULL);






    return mn;
}

int	stat_main(int argc, char **argv)
{
    (void)argc;
    (void)argv;
    add_plot_treat_menu_item ( "statistics", NULL, stat_plot_menu(), 0, NULL);
    return D_O_K;
}
int	stat_unload(int argc, char **argv)
{
    (void)argc;
    (void)argv;
    remove_item_to_menu(plot_treat_menu, "statistics", NULL, NULL);
    return D_O_K;
}

#endif
