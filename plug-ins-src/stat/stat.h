#ifndef _STAT_H_
#define _STAT_H_

#include "xvin.h"

typedef struct sigma_stat
{
  double	ct[32];		/* temporary memory used for averaging 1*/
  int		nt[32];		/* nb of samples in temporary counter 1*/
  double	av[32];		/* mean value averaging counter */
  double	av2[32];	/* averaging counter of square value */
  int		avn[32];	/* nb of averaged values in av and av2 */
  char title[128];
} s_t;

PXV_FUNC(int, init_sigma_stat, (s_t *st));
PXV_FUNC(s_t, *create_sigma_stat, (char *type));
PXV_FUNC(int, add_point_to_sigma_stat, (s_t *st, float pt));
PXV_FUNC(int, spit_sigma_stat, (s_t *st));
PXV_FUNC(d_s, *spit_sigma_stat_in_ds, (s_t *st, d_s *ds, d_s *dserr, float *meanval));

PXV_FUNC(int, do_sigma_stat, (void));
PXV_FUNC(int, do_fit_sigma_stat, (void));

PXV_FUNC(int, min_sup_0, (float *x, int n, float *m));
PXV_FUNC(int, max_sup_0, (float *x, int n, float *m));
PXV_FUNC(int, min_sup_to, (float *x, int n, float *m, float thres));
PXV_FUNC(int, max_sup_to, (float *x, int n, float *m, float thres));
PXV_FUNC(int, min_norm, (float *x, int n, float *m));
PXV_FUNC(int, max_norm, (float *x, int n, float *m));


PXV_FUNC(int, add_gaussian_noise,
	(d_s *ds, d_s *dsr, d_s *dsd, d_s *dsdr, long *idum));
PXV_FUNC(int, bootstrap, (d_s *ds, d_s *dsr, d_s *dsd, d_s *dsdr, long *idum));
PXV_FUNC(int,	do_extract_error, (void));
PXV_FUNC(int,	draw_error_bars, (O_p *op, d_s *dsi, d_s *dsr));
PXV_FUNC(int,	do_draw_error_bars, (void));
PXV_FUNC(int,	do_histo_non_zero, (void));
PXV_FUNC(int,	draw_histo_bars, (void));

PXV_FUNC(double, Y_equal_a0_plus_a1_exp_moins_lt,
	(double t, double a0, double a1, double l));
PXV_FUNC(void, Y_equal_a0_plus_a1_exp_moins_lt_func,
	(float t, float *a, float *y, float *dyda, int ma));
PXV_FUNC(int,	do_fit_Y_equal_a0_plus_a1_exp_moins_lt, (void));
PXV_FUNC(double, Y_equal_a_gaussian, ( double x, double x0, double a, double b));
PXV_FUNC(void, Y_equal_a_gaussian_func,
	(float x, float *aa, float *y, float *dydaa, int ma));
PXV_FUNC(int,	do_fit_Y_equal_a_gaussian, ());
PXV_FUNC(int,	do_fit_Y_equal_a_gaussian_boostrap, (void));
PXV_FUNC(int, fit_Y_equal_a_gaussian_boostrap_to_op,
	(O_p *op, d_s *dsi, d_s *dsir,
	double *x0, int ix0, double *a, int ia, double *b, int ib,
	double *chisq, int verbose));
PXV_FUNC(int,	fit_Y_equal_a0_plus_a1_exp_moins_lt_to_op,
	(O_p *op, d_s *dsi, d_s *dsir,
	double *a0, int ia0, double *a1, int ia1, double *l, int il,
	double *chisq, int verbose));
PXV_FUNC(float,p_value_from_chi2,(float chi2, float dof));
PXV_FUNC(MENU*, stat_plot_menu, (void));
PXV_FUNC(int, stat_main, (int argc, char **argv));
PXV_FUNC(int, is_G4_no_falled,(O_p *op, d_s *no_falls, d_s *ds));
PXV_FUNC(O_p*, histogram_vc,(pltreg *pr, O_p *op, d_s *dss, float bin, int norm, int adj_x, int cumul));
PXV_FUNC(d_s*, fit_a_exp_t_over_tau_with_er,(O_p *op, d_s *dsi, double *af, double *tau, double tau_err_rel, int match_x));
PXV_FUNC(d_s*, fit_a_exp_t_over_tau_with_er_full,(O_p *op, d_s *dsi, double *af, double *tau, double tau_err_rel, int match_x, double *chi2, bool prompt));
PXV_FUNC(int, fit_Y_equal_a_gaussian,(d_s *dsi, float *xx0, float a));
#endif
