/*
*    Plug-in for statistical operations in Xvin.
*
*    V. Croquette
*    N. Garnier
*/
#ifndef _STATISTICS_C_
#define _STATISTICS_C_

#include "allegro.h"
#include "xvin.h"
#include "xv_tools_lib.h"
#include <gsl/gsl_statistics.h>
#include <gsl/gsl_histogram.h>

/* If you include other plug-ins header do it here*/ 
#include "../hist/hist.h"
/* But not below this define */
#define BUILDING_PLUGINS_DLL
#include "statistics.h"



int do_compute_moments_from_ds(void)
{	O_p *op = NULL;
	d_s *ds;
	pltreg *pr = NULL;
	double mean, sigma, largest, smallest, skew, kurtosis;
	int nf;
	
		/* display routine action if SHIFT is pressed */
	if(updating_menu_state != 0)	return D_O_K;	
	if (key[KEY_LSHIFT])	return win_printf_OK("This routine compute some moments of"
										"the y coordinate of a data set ");

	/* we first find the data that we need to transform */
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
		return win_printf_OK("cannot find data");
	nf = ds->nx;	/* this is the number of points in the data set */

	mean     = gsl_stats_float_mean(ds->yd, 1, ds->ny);
	sigma	 = gsl_stats_float_sd_m(ds->yd, 1, ds->ny, mean);
	largest  = gsl_stats_float_max(ds->yd, 1, ds->ny);
	smallest = gsl_stats_float_min(ds->yd, 1, ds->ny);
	skew 	 = gsl_stats_float_skew_m_sd (ds->yd, 1, ds->ny,  mean, sigma);
	kurtosis = gsl_stats_float_kurtosis_m_sd (ds->yd, 1, ds->ny,  mean, sigma);
	
	mean		= op->ay + op->dy * mean;
	sigma	 = op->ay + op->dy * sigma;
	largest  = op->ay + op->dy * largest;
	smallest = op->ay + op->dy * smallest;
	
	set_ds_plot_label(ds, op->x_lo + (op->x_hi - op->x_lo)/4, 
					op->y_hi - (op->y_hi - op->y_lo)/4, USR_COORD, 
					"\\fbox{\\stack{{Statistics Y over %d points}{mean =  %g \\stack{+-} %g %s}"
					"{min %g, max %g %s}{skewness %g kurtosis deviation %g}}}",
					ds->ny, mean, sigma, (op->y_unit)?op->y_unit:" ", smallest, largest, 
					(op->y_unit)?op->y_unit:" ", skew, kurtosis );
	
	op->need_to_refresh = 1;	
	/* redisplay the entire plot */
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
}



int do_compute_extreme_time_lags(void)
{	O_p *op=NULL, *op2=NULL;
	d_s *ds, *ds2=NULL;
	pltreg *pr = NULL;
	double mean, sigma, largest, smallest;
	int n1, n3, nf, old=0, new=0;
register int i;
	char *s=NULL;
	gsl_histogram *hist;
static float	distance=3.0;
static int nh=40;
static int bool_time_in_seconds=0;
	float f_acq=1.0;
	
	
		/* display routine action if SHIFT is pressed */
	if(updating_menu_state != 0)	return D_O_K;	
	if (key[KEY_LSHIFT])	return win_printf_OK("This routine compute the histogram"
										"of time lags between extreme events in the active data set ");

	/* we first find the data that we need to transform */
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
		return win_printf_OK("cannot find data");
	nf = ds->nx;	/* this is the number of points in the data set */

	if (	( (ds->xd[2]-ds->xd[1])==(ds->xd[1]-ds->xd[0]) )
	    &&  ( (ds->xd[2]-ds->xd[1])!=0 ) ) 
	{  f_acq=1/(ds->xd[1]-ds->xd[0]);
	}
	
	mean     = gsl_stats_float_mean(ds->yd, 1, ds->ny);
	sigma	 = gsl_stats_float_sd_m(ds->yd, 1, ds->ny, mean);
	largest  = gsl_stats_float_max(ds->yd, 1, ds->ny);
	smallest = gsl_stats_float_min(ds->yd, 1, ds->ny);
	
	// following is to adapt to current unit set:
	mean	 = op->ay + op->dy * mean;
	sigma	 = op->ay + op->dy * sigma;
	largest  = op->ay + op->dy * largest;
	smallest = op->ay + op->dy * smallest;
	
	// but we don't use it further. so let's warn the user...
	if (op->dy!=1) return(win_printf_OK("You are using a special unit set. I abort"));
	
	n1=0;
	n3=0;
	for (i=0; i<nf; i++)
	{	if ((double)ds->yd[i]>(mean+1.5*sigma)) n1++;
		if ((double)ds->yd[i]>(mean+3.0*sigma)) n3++;
		if ((double)ds->yd[i]<(mean-1.5*sigma)) n1++;
		if ((double)ds->yd[i]<(mean-3.0*sigma)) n3++;
	}
	s=my_sprintf(s,"active dataset has %d points\n"
		"mean is %g  /  variance \\sigma is %g\n"
		"smallest is %g  /  largest is %g\n"
		"{\\color{lightgreen}%d} points are 1.5 \\sigma  away from the mean\n"
		"{\\color{lightgreen}%d} points are 3 \\sigma  away from the mean\n\n"
		"use points which are %%6f \\sigma  away from the mean\n"
		"build histogram with N_h = %%5d bins\n"
		"between times 0 and N_h ({\\color{red}in points !!!})\n\n"
		"%%b use f_{acq}=%%6f Hz to normalize times}",
		nf, mean, sigma, smallest, largest, n1, n3);
	i=win_scanf(s, &distance, &nh, &bool_time_in_seconds, &f_acq);
	free(s); s=NULL;
	if (i==CANCEL)	return(D_O_K);
	if (f_acq<=0)	return(win_printf_OK("f_{acq} cannot be zero or negative!!!"));
	if (nh<=0)  	return(win_printf_OK("N_h cannot be zero or negative!!!"));
	
	hist = gsl_histogram_alloc(nh);
	gsl_histogram_set_ranges_uniform (hist, (double)0.5, (double)nh+0.5);

	n1=0; // will count the number of rare events
	for (i=0; i<nf; i++)
	{	if ( fabs(ds->yd[i]-mean) > distance*sigma) // then we have an extreme event
		{	n1++;
			if (n1==1) // this is the first rare event
			{	new=i;
			}
			else	// if this is not the first rare event, then we can compute a time lag with the previous:
			{	old = new;
				new =i;
				gsl_histogram_increment(hist, (double)(new-old)); 
			}
		}
	}

	if ((op2 = create_and_attach_one_plot(pr, nh, nh, 0)) == NULL)
	return(win_printf_OK("cannot create plot !"));
	ds2 = op2->dat[0];
	
	histogram_to_ds(hist, ds2, nh);
	gsl_histogram_free(hist);
	
	if (bool_time_in_seconds==1)
	{	for (i=0; i<nh; i++)	ds2->xd[i] /= f_acq;
	}
		
	inherit_from_ds_to_ds(ds2, ds);
	ds2->treatement = my_sprintf(ds2->treatement,"hist. of time lags of rare events (%4f \\sigma)", distance);
	set_ds_point_symbol(ds2, "\\oc");

	
	set_plot_title(op2,"\\stack{{time lags between rare events}{%5f \\sigma}}", distance);
	if (bool_time_in_seconds==1) set_plot_x_title(op2, "\\tau (in points, %d events)", n1);
	else						 set_plot_x_title(op2, "\\tau (f_{acq}=%4.0f Hz, %d events)", f_acq, n1);
	set_plot_y_title(op2, "pdf");
	op2->filename = Transfer_filename(op->filename);
    op2->dir = Mystrdup(op->dir);
    op2->iopt |= YLOG;

	return(refresh_plot(pr, UNCHANGED));
}




int do_compute_moments_from_averaged_ds(void)
{
register int 	i, j, j2, k;
	O_p 	*op2, *op1 = NULL;
	int	ny;
	double a_tau, inv_tau, mean, sigma;
	double *y_tau;
	d_s 	*ds1, *ds_mean=NULL, *ds_sigma=NULL, *ds_skewness=NULL, *ds_kurtosis=NULL, *ds_GC=NULL;
	pltreg	*pr = NULL;
	int	tau=1, n_tau, n_intervals;
	int	*tau_index=NULL;
static	int	shift=1;
static int bool_mean=0, bool_sigma=1, bool_skew=0, bool_kurtosis=0, 
       bool_substract_3_to_kurtosis=0, bool_GC=1, bool_GC_divide_by_tau=1;
	int	index;
static	char	tau_string[128]="1,2,5,10:5:50";

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine computes selected moments\n"
				"out of the current dataset, and place it/them in a new plot\n\n"
				"data a(t) is first averaged using a sliding average:\n\n"
				"a_\\tau(t) = {1 \\over \\tau} \\int_t^{t+\\tau} a(t') dt'\n\n"
				"(this is for example used in the study of FTs)");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)		return win_printf_OK("cannot find data");
	ny=ds1->nx;	

	i=win_scanf("{\\pt14\\color{yellow}Compute moments}\n"
			"%b mean\n"
			"%b \\sigma\n"
			"%b skewness\n"
			"%b kurtosis (%b substract 3)\n"
			"%b 2<x>/\\sigma^2 (Gallavotti-Cohen), %b divide it by \tau\n\n"
			"value(s) of \\tau : %s\n"
			"shift between integrations : %5d",
			&bool_mean, &bool_sigma, &bool_skew, &bool_kurtosis, &bool_substract_3_to_kurtosis, 
            &bool_GC, &bool_GC_divide_by_tau, &tau_string, &shift);
	if (i==CANCEL) return(OFF);
	if ( (bool_mean + bool_sigma + bool_skew + bool_kurtosis + bool_GC) == 0) return(OFF);
	
	if (index==STAT_NO_AVERAGE)
		tau_index = str_to_index("1", &n_tau);
	else	tau_index = str_to_index(tau_string, &n_tau);	// malloc is done by str_to_index
	if ( (tau_index==NULL) || (n_tau<1) )	return(win_printf("bad values for \\tau !"));

	if ((op2 = create_and_attach_one_plot(pr, n_tau, n_tau, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	if (bool_mean==1)
	{	ds_mean=create_and_attach_one_ds (op2, n_tau, n_tau, 0);
		inherit_from_ds_to_ds(ds_mean, ds1);
		ds_mean->treatement = my_sprintf(ds_mean->treatement,"mean vs \\tau");
	}
	if (bool_sigma==1)
	{	ds_sigma=create_and_attach_one_ds (op2, n_tau, n_tau, 0);
		inherit_from_ds_to_ds(ds_sigma, ds1);
		ds_sigma->treatement = my_sprintf(ds_sigma->treatement,"sigma vs \\tau");
	}
	if (bool_skew==1) 	
	{	ds_skewness=create_and_attach_one_ds (op2, n_tau, n_tau, 0);
		inherit_from_ds_to_ds(ds_skewness, ds1);
		ds_skewness->treatement = my_sprintf(ds_skewness->treatement,"skewness vs \\tau");
	}
	if (bool_kurtosis==1)
	{ 	ds_kurtosis=create_and_attach_one_ds (op2, n_tau, n_tau, 0);
		inherit_from_ds_to_ds(ds_kurtosis, ds1);
		ds_kurtosis->treatement = my_sprintf(ds_kurtosis->treatement,"kurtosis%s vs \\tau", (bool_substract_3_to_kurtosis==1) ? "-3" : " ");
	}
	if (bool_GC==1)
	{ 	ds_GC=create_and_attach_one_ds (op2, n_tau, n_tau, 0);
		inherit_from_ds_to_ds(ds_GC, ds1);
		ds_GC->treatement = my_sprintf(ds_GC->treatement,"GC estimator 2<x>/\\sigma^2 vs \\tau");
		if (bool_GC_divide_by_tau==1) ds_GC->treatement = my_sprintf(ds_GC->treatement,"divided by \\tau");
	}
	remove_ds_n_from_op(op2, 0);
	
	for (i=0; i<n_tau; i++)
	{	tau=tau_index[i];
		inv_tau = (double)1./tau;
		
		n_intervals = (ny-tau+1)/shift;
		y_tau = (double*)calloc(n_intervals, sizeof(double));
		for (j=0, k=0; j<ny-tau+1; j+=shift)
		{	a_tau=0;
			for (j2=0; j2<tau; j2++)
			{	a_tau+=(double)ds1->yd[(j+j2)];		// average over tau points
			}
			y_tau[k]=a_tau*inv_tau;
			k++;
		}
		mean		= gsl_stats_mean(y_tau, 1, n_intervals);
		sigma	= gsl_stats_sd_m(y_tau, 1, n_intervals, mean);
		
		if (bool_mean==1)
		{	ds_mean->xd[i] = tau;
			ds_mean->yd[i] = (float)mean;
		}
		if (bool_sigma==1)
		{	ds_sigma->xd[i] = tau;
			ds_sigma->yd[i] = (float)sigma;  
		}
		if (bool_skew==1)
		{	ds_skewness->xd[i] = tau;
			ds_skewness->yd[i] = (float)gsl_stats_skew_m_sd (y_tau, 1, n_intervals, mean, sigma);
		}
		if (bool_kurtosis==1)
		{	ds_kurtosis->xd[i] = tau;
			ds_kurtosis->yd[i] = (float)gsl_stats_kurtosis_m_sd (y_tau, 1, n_intervals, mean, sigma);
			if (bool_substract_3_to_kurtosis==0) { ds_kurtosis->yd[i] += 3.; }
		}
		if (bool_GC==1)
		{	ds_GC->xd[i] = tau;
			ds_GC->yd[i] = (double)2.*mean/(sigma*sigma);
			if (bool_GC_divide_by_tau==1) ds_GC->yd[i] /= (double)tau;
		}
		free(y_tau);
	}

	set_plot_title(op2,"moments of distribution");
	set_plot_x_title(op2, "\\tau (points, 1/f_{ec})");
	set_plot_y_title(op2, "moments");
	op2->filename = Transfer_filename(op1->filename);
    	op2->dir = Mystrdup(op1->dir);

	return refresh_plot(pr, UNCHANGED);
}






int is_local_max(float *y, int n_y, int i, int n_search)
{	int bool_is_max = 1;
	int j=0;
	float lm = y[i];
	
	while ( (j<n_search) && (bool_is_max==1) )
	{	j++;
		if (y[i-j] > lm) bool_is_max=0; 
		if (y[i+j] > lm) bool_is_max=0;
	}
	
	return bool_is_max;
}


int is_local_min(float *y, int n_y, int i, int n_search)
{	int bool_is_min = 1;
	int j=0;
	float lm = y[i];
	
	while ( (j<n_search) && (bool_is_min==1) )
	{	j++;
		if (y[i-j] < lm) bool_is_min=0; 
		if (y[i+j] < lm) bool_is_min=0;
	}
	
	return bool_is_min;
}


int is_extreme_value_max(float *y, int n_y, float threshold, int i, int n_search)
{	
	if (y[i] >= threshold) return(is_local_max(y, n_y, i, n_search));
	else   	               return(0);
}

int is_extreme_value_min(float *y, int n_y, float threshold, int i, int n_search)
{	
	if (y[i] >= threshold) return(is_local_min(y, n_y, i, n_search));
	else   	               return(0);
}



int do_extrema_dissymetry(void)
{
	register int i,k,n2;
	O_p 	*op1, *op2=NULL, *op3=NULL;
	int	ny;
static	int	bool_normalize=1, bool_asym_coef, bool_max_min=0;
	d_s 	*ds1, *ds2=NULL, *ds3=NULL;
	pltreg *pr = NULL;
	int	index;
static	int	n_search=100, n_plot=100;
static float ex_factor = 2.5;
	float dt, mean, var, threshold;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine plots the the vicinity of extrema \n"
								"to check for possible dissymetry in time.\n"
                                "Extrema are detected as local maxima, \n"
                                "and eventually rare events, far enough from the mean.");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)	return win_printf_OK("cannot plot region");

	if (win_scanf("Time dissymetry around extrema\n"
	                "search extreme values above mean + %4f \\sigma \n"    
					"search local extrema on time interval with 2*%5d+1 points \n"
					"plot local extrema with 2*%5d+1 points \n\n"
					"study %R maxima or %r minima\n"
					"%b normalize\n"
                    "%b plot asymetry coefficient\n", 
					&ex_factor, &n_search, &n_plot, 
                    &bool_max_min, &bool_normalize, &bool_asym_coef)==CANCEL) return D_O_K;
	ny = ds1->ny;	// number of points in time series 
	if (ny<2*(n_search+n_plot+1)) 			return(win_printf_OK("not enough points !"));
    
//	dt = find_dt_start_from_time_array(ds1->xd, ny);
    dt = find_dt_average_from_time_array(ds1->xd, ny);
    if (dt<1e-6) dt=(float)1.0;
	mean = gsl_stats_float_mean(ds1->yd, 1, ny);
	var  = gsl_stats_float_sd_m(ds1->yd, 1, ny, mean);
	if (bool_max_min==0) threshold = mean + var*ex_factor;
	else                 threshold = mean - var*ex_factor;

	// how many local extrema do we have ?
	n2 = 0;
	if (bool_max_min==0)
	{ for (i=n_plot; i<ny-n_plot; i++)
	  {	if (is_extreme_value_max(ds1->yd, ny, threshold, i, n_search) == 1)
		{ n2++;
		  i += n_search;
		}
	  }
    }
    else
	{ for (i=n_plot; i<ny-n_plot; i++)
	  {	if (is_extreme_value_min(ds1->yd, ny, threshold, i, n_search) == 1)
		{ n2++;
		  i += n_search;
		}
	  }
    }



	if (win_printf("%d local extrema found\n\n click CANCEL to exit", n2)==CANCEL) return(D_O_K);

	op2 = create_and_attach_one_plot (pr,n2*(2*n_plot+1), n2*(2*n_plot+1), 0);
	ds2= op2->dat[0];
	set_ds_treatement (ds2, "zoom on %d local %s", n2, (bool_max_min==0) ? "maxima" : "minima" );
	set_plot_title  (op2, "fluctuation paths around %s", (bool_max_min==0) ? "maxima" : "minima" );
	set_plot_x_title(op2, op1->x_title);
	set_plot_y_title(op2, op1->y_title);
	
	if (bool_asym_coef==1)
	{ op3 = create_and_attach_one_plot (pr,n2*(n_plot+1), n2*(n_plot+1), 0);
	  ds3 = op3->dat[0];
	  set_ds_treatement (ds3, "asymetry coefficient");
	  set_plot_title  (op3, "asymetry coefficient, %d %s", n2, (bool_max_min==0) ? "maxima" : "minima" );
	  set_plot_x_title(op3, op1->x_title);
	  set_plot_y_title(op3, op1->y_title);
    }

	
	// copy the data correctly :
	n2 = 0;
	for (i=n_plot; i<ny-n_plot; i++)
	{	if ( ( (bool_max_min==0) && (is_extreme_value_max(ds1->yd, ny, threshold, i, n_search)==1) )
	      || ( (bool_max_min==1) && (is_extreme_value_min(ds1->yd, ny, threshold, i, n_search)==1) ) )
		{	ds2->yd[(n2*(2*n_plot+1)) + n_plot] = ds1->yd[i]; // the max
			ds2->xd[(n2*(2*n_plot+1)) + n_plot] = 0;
			for (k=1; k<=n_plot; k++)
			{	ds2->xd[n2*(2*n_plot+1) + n_plot - k] = -(float)dt*(float)k;
				ds2->yd[n2*(2*n_plot+1) + n_plot - k] = ds1->yd[i-k];
				ds2->xd[n2*(2*n_plot+1) + n_plot + k] =  (float)dt*(float)k;
				ds2->yd[n2*(2*n_plot+1) + n_plot + k] = ds1->yd[i+k];
				
				if (bool_normalize==1)
				{ ds2->yd[n2*(2*n_plot+1) + n_plot - k] -= ds1->yd[i];
				  ds2->yd[n2*(2*n_plot+1) + n_plot + k] -= ds1->yd[i];
				}
			}
			
			if (bool_asym_coef==1)
			{   ds3->yd[(n2*(n_plot+1)) ] = 0; // the max
			    ds3->xd[(n2*(n_plot+1)) ] = 0;
			    for (k=1; k<=n_plot; k++)
			    { ds3->xd[n2*(n_plot+1) + k] = (float)dt*(float)k;
				  ds3->yd[n2*(n_plot+1) + k] = ds1->yd[i-k] - ds1->yd[i+k];
  
			    }
            } // end if plot assymetry coefficient
			
			n2++;
			i += n_search;
		}// enf if extreme value
	}//end for
	
	inherit_from_ds_to_ds(ds2, ds1);
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the do_extrema_dissymetry






MENU *statistics_plot_menu(void)
{	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"moments of ds",				do_compute_moments_from_ds, 		NULL, 0, NULL);
	add_item_to_menu(mn,"moments of averaged ds",		do_compute_moments_from_averaged_ds,NULL, 0, NULL);
	add_item_to_menu(mn,"\0", 							NULL, 								NULL, 0, NULL);
	add_item_to_menu(mn,"time distr. of extreme events", do_compute_extreme_time_lags,		NULL, 0, NULL);
	add_item_to_menu(mn,"examine extreme maxima for time diss.", do_extrema_dissymetry,     NULL, 0, NULL);

	return mn;
}


int	statistics_main(int argc, char **argv)
{
	add_plot_treat_menu_item ("statistics", NULL, statistics_plot_menu(), 0, NULL);
	return D_O_K;
}
int	statistics_unload(int argc, char **argv)
{ 
    remove_item_to_menu(plot_treat_menu, "statistics", NULL, NULL);
	return D_O_K;
}

#endif
