#ifndef _SKEW_H_
#define _SKEW_H_

// definitions for averaging or not before estimating statistical properties:
#define STAT_AVERAGE	0x0100
#define STAT_NO_AVERAGE	0x0200

PXV_FUNC(MENU*, statistics_plot_menu,	(void));
PXV_FUNC(int, 	statistics_main,		(int argc, char **argv));

PXV_FUNC(int, 	do_compute_moments_from_ds,  (void));
PXV_FUNC(int, 	do_compute_extreme_time_lags,(void));
PXV_FUNC(int, 	do_compute_moments_from_averaged_ds,  (void));
PXV_FUNC(int, 	do_extrema_dissymetry,  (void));

// inside functions : 
int is_local_max(float *y, int n_y, int i, int n_search);
int is_local_min(float *y, int n_y, int i, int n_search);
int is_extreme_value_max(float *y, int n_y, float threshold, int i, int n_search);
int is_extreme_value_min(float *y, int n_y, float threshold, int i, int n_search);



#endif
