

/*

wavefile library C header file

written by Joshua Haddick, August 2003

This file is part of Wavefile Input Output C Library.

Wavefile Input Output C Library is free software; you can redistribute it and/or modify  it under the terms of the GNU General Public License as published bythe Free Software Foundation; either version 2 of the License, or(at your option) any later version.
Wavefile Input Output C Library is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wavefile Input Output C Library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/



#ifndef _wavefile_h
#define _wavefile_h


/* the following structure is used for creating a new wave */

typedef struct
{
	unsigned short int wChannels;
	unsigned int nSamplesPerSec;
} wave_info;

/* the following structure represents the wave file, but should not need to be accessed directly (assuming I made a good interface)*/

typedef struct
{
	char *filename;

	unsigned int nPosition;
	char *data;
	unsigned int nArray_size;
	unsigned int nData_size;

	unsigned short wChannels;
	unsigned int nSamplesPerSec;

	unsigned int nBlockAlign;
} wave_file;


/* the following structure represents what gets placed, or what must be found, at the beginning of a wave file, in the context of this code */

typedef struct
{
	char riff[4]; /* constant: 'R' 'I' 'F' 'F' */
	unsigned int nRiff_size; /* dependent: nData_size + 38 */
	char wave[4]; /* constant: 'W' 'A' 'V' 'E' */
	char fmt[4]; /* constant: 'f' 'm' 't' ' ' */
	unsigned int fmt_size; /* constant: 16 */
	unsigned short int wFormatTag; /* constant: WAVE_FORMAT_PCM, 1*/
	unsigned short int wChannels; /* independent: 1 or 2 */
	unsigned int nSamplesPerSec; /* independent: standard values are 11025, 22050, 44100 */
	unsigned int nAvgBytesPerSec; /* dependent: nSamplesPerSec*(wBitsPerSample/8) */
	unsigned short int wBlockAlign; /* dependent: wBitsPerSample/8 */
	unsigned short int wBitsPerSample; /* dependent: 16*nChannels */
	char data[4]; /* constant: 'd' 'a' 't' 'a' */
	unsigned int nData_size; /* independent */
} wave_header;


/* file operations */

wave_file *new_wave_file(const char *filename, wave_info *wi, unsigned int nByte_size);
wave_file *open_wave_file(const char *filename);
char wave_save(wave_file *wf);
void wave_free(wave_file *wf);


/* data buffer operations */
char wave_set_data_size(unsigned int nData_size, wave_file *wf);
char wave_set_buffer_size(unsigned int nArray_size, wave_file *wf);


/* input - output 1 channel 16 bits */

short int wave_get(wave_file *wf);
char wave_put(short int i, wave_file *wf);

short int wave_ram_get(unsigned int nLocation, wave_file *wf);
char wave_ram_put(short int i, unsigned int nLocation, wave_file *wf);


/* input - output 2 channel 16 bits */

typedef struct
{
	short int val1;
	short int val2;
} stereo_int;

char wave_get2(wave_file *wf, stereo_int *si);
char wave_put2(stereo_int *i, wave_file *wf);

char wave_ram_get2(unsigned int nLocation, wave_file *wf, stereo_int *si);
char wave_ram_put2(stereo_int *i, unsigned int nLocation, wave_file *wf);



/* file positioning */

unsigned int wave_getpos(wave_file *wf);
char wave_setpos(wave_file *wf, unsigned int pos);
char wave_rewind(wave_file *wf);
char wave_fastforward(wave_file *wf);


/* extra */

void print_wave_info(wave_file *wf);


#endif
