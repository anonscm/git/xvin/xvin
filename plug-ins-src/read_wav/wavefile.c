




/*

wavefile library C source file

Copyright 2003, 2004 Joshua Haddick

Written by Joshua Haddick, August 2003.

This file is part of Wavefile Input Output C Library.

Wavefile Input Output C Library is free software; you can redistribute it and/or modify  it under the terms of the GNU General Public License as published bythe Free Software Foundation; either version 2 of the License, or(at your option) any later version.
Wavefile Input Output C Library is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wavefile Input Output C Library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/




#include "wavefile.h"
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


/* file operations */


/*
upon initialization, nData_size of wave_file structure is set to 0, starts an important relation: nPosition*scale < nData_size + scaleand another important requirement: if wave_file is allocated, it must also have an allocate data buffer.nArray_size is the explicitly requested size of data buffer.  Must be bigger than 3.
file creation embodies 16 bit wave file simplification assumption, of which nBlockAlign is implied by wChannels*/

wave_file *new_wave_file(const char *filename, wave_info *wi, unsigned int nArray_size){
	wave_file *wf;

	/* sanity checks */
	if(wi==(wave_info *)NULL)
	{
		fprintf(stderr, "Could not create wave file.  NULL wave_info input.\n");
		return (wave_file *)NULL;
	}
	if((wi->wChannels!=1)&&(wi->wChannels!=2))
	{
		fprintf(stderr, "Could not create wave file.  wChannels input must be 1 or 2.\n");		return (wave_file *)NULL;
	}
	if(nArray_size < 4)
	{
		fprintf(stderr, "Could not create wave file.  nByte_size input must be greater than 3.\n");		return (wave_file *)NULL;
	}

	wf = (wave_file *)malloc(sizeof(wave_file));
	if(wf == (wave_file *)NULL)
	{
		fprintf(stderr, "Could not create wave file.  Out of Memory Error.\n");
		return (wave_file *)NULL;
	}

	wf->wChannels = wi->wChannels;
	wf->nSamplesPerSec = wi->nSamplesPerSec;

	wf->nArray_size = nArray_size;
	wf->data = (char *)malloc(wf->nArray_size);
	if(wf->data==(char *)NULL)
	{
		fprintf(stderr, "Could not create wave file.  Out of Memory Error.\n");
		free(wf);
		return (wave_file *)NULL;
	}

	wf->nData_size = 0;
	wf->nPosition = 0;
	
	if(wf->wChannels == 1) wf->nBlockAlign = 2;
	else wf->nBlockAlign = 4;

	wf->filename = (char *)malloc(strlen(filename)+1);
	if(wf->filename==(char *)NULL)
	{
		fprintf(stderr, "Could not create wave file.  Out of Memory Error.\n");
		free(wf->data);
		free(wf);
		return (wave_file *)NULL;
	}

	strcpy(wf->filename, filename);

	return wf;
}


/* this strictly depends on file being in simplified wave file format i.e., a wave_header header, and only 16 bit data width allowed */
/* note that if file is too big, the default buffer may not fit in main memory, and will not be able to open it. */
/*
position is initially set at zero, the first element of data in file.
nData_size set to the size of the wave data in the file, nArray is set to (nData_size + 4).
thus, if you plan to add data to opened file, you should immediately call wave_set_buffer_sizeotherwise, file will automatically be implicitly reallocated if data becomes larger than initial buffer size.
the returned wave_file is immediately ready for reading from the beginning - so long the file had any data in it.
buffer_scale allows user to set how large data buffer will be with respect to wave file.*/
wave_file *open_wave_file(const char *filename)
{
	FILE *f;
	wave_header wh;
	wave_file *wf;

	if( (f = fopen(filename, "rb"))==(FILE *)NULL)
	{
		fprintf(stderr, "error: unable to open file for reading.\n");
		return (wave_file *)NULL;
	}
	
	/* read the header */
	if(fread((void *)&wh , 1, 44, f)!=44)
	{
		fprintf(stderr, "error reading file.\n");
		fclose(f);
		return (wave_file *)NULL;
	}

	/* check the header */
	
	if	(
		(wh.riff[0]!='R')||(wh.riff[1]!='I')||(wh.riff[2]!='F')||(wh.riff[3]!='F')||
		(wh.wave[0]!='W')||(wh.wave[1]!='A')||(wh.wave[2]!='V')||(wh.wave[3]!='E')||
		(wh.fmt[0]!='f')||(wh.fmt[1]!='m')||(wh.fmt[2]!='t')||(wh.fmt[3]!=' ')||
		(wh.data[0]!='d')||(wh.data[1]!='a')||(wh.data[2]!='t')||(wh.data[3]!='a')||
		(wh.fmt_size!=16)||(wh.wFormatTag!=1)||((wh.nRiff_size - wh.nData_size)!= 36)||		!(((wh.wChannels==1)&&(wh.wBitsPerSample==16))||((wh.wChannels==2)&&(wh.wBitsPerSample==32)))		)	{
		fprintf(stderr, "error: file is not in simplified wave format, open aborted.\n");		fclose(f);
		return (wave_file *)NULL;
	}
	
	wf = (wave_file *)malloc(sizeof(wave_file));
	if(wf==(wave_file *)NULL)
	{
		fprintf(stderr, "out of memory error.  could not open wave file.\n");
		fclose(f);
		return (wave_file *)NULL;
	}

	wf->filename = (char *)malloc(strlen(filename)+1);
	if(wf->filename==(char *)NULL)
	{
		fprintf(stderr, "out of memory error.  could not open wave file.\n");
		free((void *)wf);
		fclose(f);
		return (wave_file *)NULL;
	}
	
	strcpy(wf->filename, filename);

	if((wf->wChannels = wh.wChannels)==1) wf->nBlockAlign = 2;
	else wf->nBlockAlign = 4;
	
	wf->nSamplesPerSec = wh.nSamplesPerSec;
	
	wf->nPosition = 0;
	wf->nArray_size = (wf->nData_size = wh.nData_size) + 4; /* 4 preserves the relation: nArray_size > 3 */
	wf->data = (char *)malloc(wf->nArray_size);
	if(wf->data==(char *)NULL)
	{
		fprintf(stderr, "file was too large to fit in memory.  could not open.\n");
		free((void *)wf);
		fclose(f);
		return (wave_file *)NULL;
	}

	/* at this point, only need to read the data into the buffer */
	if(fread((void *)wf->data , 1, wf->nData_size, f)!=wf->nData_size)
	{
		fprintf(stderr, "error reading file.\n");
		free((void *)wf->data);
		free((void *)wf);
		fclose(f);
		return (wave_file *)NULL;
	}
	
	fclose(f);
	
	return wf;
}

/* this function embodies assumptions of little endian */
char wave_save(wave_file *wf)
{
	FILE *f;
	wave_header wh;

	if(wf==(wave_file *)NULL)
	{
		fprintf(stderr, "error:cannot save wave file, NULL wave_file input.\n");
		return 1;
	}

	f = fopen(wf->filename, "wb+");

	if(f==(FILE *)NULL)
	{
		fprintf(stderr, "error: cannot save wave file, error opening disk file for writing.\n");		return 1;
	}

	/* at this point, fill in all of the constants in the header... */
	wh.riff[0] = 'R'; wh.riff[1] = 'I'; wh.riff[2] = 'F'; wh.riff[3] = 'F';
	wh.wave[0] = 'W'; wh.wave[1] = 'A'; wh.wave[2] = 'V'; wh.wave[3] = 'E';
	wh.fmt[0] = 'f'; wh.fmt[1] = 'm'; wh.fmt[2] = 't'; wh.fmt[3] = ' ';

	wh.fmt_size = 16;
	wh.wFormatTag = 1; /* WAVE_FORMAT_PCM */

	wh.data[0] = 'd'; wh.data[1] = 'a'; wh.data[2] = 't'; wh.data[3] = 'a';

	/* at this point, fill in all the independents in the header... */
	wh.wChannels = wf->wChannels;
	wh.nSamplesPerSec = wf->nSamplesPerSec;

	wh.nData_size = wf->nData_size;


	/* now fill in the dependent variables */
	wh.wBitsPerSample = 16*wh.wChannels; /* this instruction completely assumes 16 bit sound precision*/	wh.nRiff_size = wh.nData_size + 36;
	wh.wBlockAlign = wh.wBitsPerSample/8; /* do not round up if 8 or 16 assumed */
	wh.nAvgBytesPerSec = wh.nSamplesPerSec*wh.wBlockAlign;


	/* attempt to write the header */
	if(fwrite((void *)&wh, 1, 44, f)!=44)
	{
		fprintf(stderr, "error writing wave to disk file.\n");
		fclose(f);
		return 1;
	}

	/* attempt to write the data */
	if(fwrite((void *)wf->data, 1, wh.nData_size, f)!=wh.nData_size)
	{
		fprintf(stderr, "error writing wave to disk file.\n");
		fclose(f);
		return 1;
	}

	fclose(f);
	return 0;
}

/* make sure to set wf = NULL after calling this function to avoid dangling pointer. */void wave_free(wave_file *wf)
{
	if(wf==(wave_file *)NULL) return;
	if(wf->filename!=(char *)NULL) free((void *)wf->filename);
	if(wf->data!=(char *)NULL) free((void *)wf->data);
	free((void *)wf);
}



/* data buffer, array functions... */



/*
overriding relations: 1) 0 <= nData_size <= nArray_size, 2) nData_size is divisible by scale 3) nPosition*scale < nData_size + scale	4) 3 < nArray_size
scale is figured out using wChannels
*/


/*
	this function assumes wf is not NULL, and wf->data is not NULL. It is a very nasty reallocation function.	to avoid this function, user should make explicit request for data buffer size, and stay in the bounds of that buffer size.*/
static char wave_reallocate(wave_file *wf) /* in this case, relations are preserved because array only gets bigger. */{
	unsigned int i, old_size;
	char *old_array;

	old_array = wf->data;
	old_size = wf->nArray_size;

	wf->nArray_size*=2;
	wf->data = (char *)malloc(wf->nArray_size);
	if(wf->data==(char *)NULL)
	{
		fprintf(stderr, "Out of memory error.  Cannot reallocate data buffer.\n");
		wf->data = (char *)old_array;
		wf->nArray_size = old_size;
		return 1;
	}

	for(i = 0; i < wf->nData_size; i++)
		wf->data[i] = old_array[i];

	free((void *)old_array);

	return 0;
}

/*
in this case, must preserve relations if shrinking array
remember, buffer must be positive size (otherwise, reallocate wouldn't work.)
also, buffer can't be less than data size.  must call wave_set_data size first.

note another relation, if wave_file is allocated, then it must have a buffer.
*/
char wave_set_buffer_size(unsigned int nArray_size, wave_file *wf)
{
	unsigned int i, old_size;
	char *old_array;

	if(wf==(wave_file *)NULL)
	{
		fprintf(stderr, "error: cannot set buffer size, wave_file input is NULL.\n");
		return 1;
	}

	if(nArray_size<4)
	{
		fprintf(stderr, "error: illegal nArray_size, input must be greater than 3.\n");		return 1;
	}
	
	if(wf->nData_size > nArray_size)
	{
		fprintf(stderr, "error: cannot set buffer size, nArray_size input is less than data size.\n");		return 1;
	}

	old_array = wf->data;
	old_size = wf->nArray_size;

	wf->nArray_size = nArray_size;
	wf->data = (char *)malloc(wf->nArray_size);
	if(wf->data==(char *)NULL)
	{
		fprintf(stderr, "Out of memory error.  Cannot reallocate data buffer.\n");
		wf->data = (char *)old_array;
		wf->nArray_size = old_size;
		return 1;
	}

	for(i = 0; i < wf->nData_size; i++)
		wf->data[i] = old_array[i];

	free((void *)old_array);

	return 0;
}


/*
always remember, nData_size must be divisible by nBlockAlign
and most importantly, choose an appropriate buffer size prior to using this call.otherwise, have a size chosen at random that may not fit in memory, thus call will be aborted.
This call ideally is for shrinking the amount of data in the memory buffer, perhaps to later shrink the size of the data buffer.*/
char wave_set_data_size(unsigned int nData_size, wave_file *wf)
{
	unsigned int i;

	if(wf==(wave_file *)NULL)
	{
		fprintf(stderr, "error: cannot set data size, wave_file input is NULL.\n");
		return 1;
	}

	if(nData_size%wf->nBlockAlign!=0)
	{
		fprintf(stderr, "error: illegal nData_size, input must be divisible by %u.\n", wf->nBlockAlign);		return 1;
	}

	if(nData_size <= wf->nArray_size)
	{
		wf->nData_size = nData_size;
		if(wf->nPosition*wf->nBlockAlign > wf->nData_size) wf->nPosition = wf->nData_size/wf->nBlockAlign;		return 0;
	}

	i = nData_size*2; /* 2 was chosen at random to ensure nData_size < nArray_size */
	if(i > wf->nArray_size)
	{
		if(wave_set_buffer_size(i, wf))
		{
			fprintf(stderr, "error: unable to set data size.\n");
			return 1;
		}
		wf->nData_size = nData_size;
		return 0;
	}

	fprintf(stderr, "overflow error, could not set data size.\n");
	return 1;
}



/* input - output 1 channel 16 bits */


/* assumes input wave file is NOT NULL.  Segmentation fault will occur otherwise */short int wave_get(wave_file *wf)
{
	if(wf->nPosition*wf->nBlockAlign<wf->nData_size) return ((short int *)wf->data)[wf->nPosition++];
	fprintf(stderr, "error: attempted to read at location out of bounds of data array.\n");	return 0;
}


/* note that in ram access, the nPosition (used in stream access) is untouched.*//* assumes input wave file is NOT NULL.  Segmentation fault will occur otherwise */short int wave_ram_get(unsigned int nLocation, wave_file *wf)
{
	if(nLocation*wf->nBlockAlign>=wf->nData_size)
	{
		fprintf(stderr, "error: attempted to read at location out of bounds of data array.\n");		return 0;
	}

	return ((short int *)wf->data)[nLocation];;
}

/* assumes input wave file is NOT NULL.  Segmentation fault will occur otherwise */char wave_put(short int i, wave_file *wf)
{
	if(wf->nPosition*wf->nBlockAlign>=wf->nData_size)
	{
		if((wf->nData_size+=wf->nBlockAlign)>wf->nArray_size)
		{
			/* since previous line broke the relation: nData_size <= nArray_size */
			wf->nData_size-=wf->nBlockAlign; /* this restores the relation */
			if(wave_reallocate(wf))
			{
				fprintf(stderr, "error: attempted to write at location out of bounds of data array.\n");				return 1;
			}
			wf->nPosition = (wf->nData_size+=wf->nBlockAlign)/wf->nBlockAlign;
		}
	}

	((short int *)wf->data)[wf->nPosition++] = i;

	return 0;
}

/* assumes input wave file is NOT NULL.  Segmentation fault will occur otherwise */char wave_ram_put(short int i, unsigned int nLocation, wave_file *wf)
{
	if(nLocation*wf->nBlockAlign>=wf->nData_size)
	{
		fprintf(stderr, "error: attempted to write at location out of bounds of data array.\n");		return 1;
	}

	((short int *)wf->data)[nLocation] = i;

	return 0;
}




/*
input - output 2 channel 16 bits
*/
/* assumes input wave file is NOT NULL.  Segmentation fault will occur otherwise */char wave_get2(wave_file *wf, stereo_int *si)
{
	short int *p;

	if(wf->nPosition*wf->nBlockAlign<wf->nData_size)
	{
		p = (short int *)((int *)wf->data + wf->nPosition++);
		si->val1 = *p++;
		si->val2 = *p;
		return 0;
	}

	fprintf(stderr, "error: attempted to read at location out of bounds of data array.\n");	si->val1 = 0;
	si->val2 = 0;
	return 1;
}

/* assumes input wave file is NOT NULL.  Segmentation fault will occur otherwise */char wave_ram_get2(unsigned int nLocation, wave_file *wf, stereo_int *si)
{
	short int *p;

	if(nLocation*wf->nBlockAlign>=wf->nData_size)
	{
		fprintf(stderr, "error: attempted to read at location out of bounds of data array.\n");		return 1;
	}

	p = (short int *)((int *)wf->data + nLocation);
	si->val1 = *p++;
	si->val2 = *p;

	return 0;
}

/* assumes input wave file is NOT NULL.  Segmentation fault will occur otherwise */char wave_put2(stereo_int *i, wave_file *wf)
{
	if(wf->nPosition*wf->nBlockAlign>=wf->nData_size)
	{
		if((wf->nData_size+=wf->nBlockAlign)>wf->nArray_size)
		{
			/* since previous line broke the relation: nData_size <= nArray_size */
			wf->nData_size-=wf->nBlockAlign; /* this restores the relation */
			if(wave_reallocate(wf))
			{
				fprintf(stderr, "error: attempted to write at location out of bounds of data array.\n");				return 1;
			}
			wf->nPosition = (wf->nData_size+=wf->nBlockAlign)/wf->nBlockAlign;
		}
	}

	((int *)wf->data)[wf->nPosition++] = *(int *)i;

	return 0;
}

/* assumes input wave file is NOT NULL.  Segmentation fault will occur otherwise */char wave_ram_put2(stereo_int *i, unsigned int nLocation, wave_file *wf)
{
	if(nLocation*wf->nBlockAlign>=wf->nData_size)
	{
		fprintf(stderr, "error: attempted to write at location out of bounds of data array.\n");		return 1;
	}

	((int *)wf->data)[nLocation] = *(int *)i;

	return 0;
}


/* file positioning */

/* piece of cake */
unsigned int wave_getpos(wave_file *wf)
{
	if(wf==(wave_file *)NULL)
	{
		fprintf(stderr, "error:cannot get wave_file position, NULL wave_file input.\n");		return 0;
	}

	return wf->nPosition;
}

/* this one trickier, must keep the relation: nPosition*nBlockAlign <= nData_size */char wave_setpos(wave_file *wf, unsigned int pos)
{
	if(wf==(wave_file *)NULL)
	{
		fprintf(stderr, "error:cannot set wave_file position, NULL wave_file input.\n");		return 1;
	}

	if(pos*wf->nBlockAlign > wf->nData_size)
	{
		fprintf(stderr, "error: attempted to set position outside of bounds of data array.\n");		return 1;
	}

	wf->nPosition = pos;
	return 0;
}


/* puts position to beginning of data */
char wave_rewind(wave_file *wf)
{
	if(wf==(wave_file *)NULL)
	{
		fprintf(stderr, "error:cannot rewind wave_file, NULL wave_file input.\n");
		return 1;
	}

	return wf->nPosition = 0;
}

/* puts position to end of data */
char wave_fastforward(wave_file *wf)
{
	if(wf==(wave_file *)NULL)
	{
		fprintf(stderr, "error:cannot fastforward wave_file, NULL wave_file input.\n");		return 1;
	}

	wf->nPosition = wf->nData_size;
	return 0;
}


/* extra */

void print_wave_info(wave_file *wf)
{
	if(wf==(wave_file *)NULL)
	{
		fprintf(stderr, "error:cannot print wave_file info, NULL wave_file input.\n");
		return;
	}

	printf("wave filename: %s\n", wf->filename);
	printf("size of data: %u\n", wf->nData_size);
	printf("number of channels: %hu\n", wf->wChannels);
	printf("samples per second: %u\n", wf->nSamplesPerSec);
	printf("Block Align (number of bytes per sample): %u\n", wf->nBlockAlign);
	printf("current stream position: %u\n", wf->nPosition);
	printf("---\n");
}



/* end of file */

