
#include "../wavefile.h"
#include <math.h>
#include <stdio.h>

int main(void)
{
	unsigned int i;
	short int j;
	wave_info wi;
	wave_file *wf;

	wi.wChannels = 1;
	wi.nSamplesPerSec = 44100;

	/* thus if time is 't' and the sample is 'i', then t = i/44100 */

	/* want to have 441000 samples, for ten seconds of sound */
	/* in next instruction set aside 2*nSamples of data storage, because each sample is 2 bytes */
	if((wf = new_wave_file("stream1channel.wav", &wi, 882000))==(wave_file *)NULL)
	{
		printf("unable to create wave file, program aborted.\n");
		return 0;
	}


	for(i = 0; i < 441000; i++)
	{
		/*
		j: 25000*sin(2*PI*(100*t*t+440*t))
		*/
		j = (short int)(25000*sin(.000000323*(double)i*(double)i + .062657596*(double)i));

		wave_put(j, wf);
	}

	if(wave_save(wf)) printf("error saving it!\n");
	wave_free(wf);


	return 0;
}








/* end of file */

