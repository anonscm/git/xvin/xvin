#ifndef _READ_WAV_H_
#define _READ_WAV_H_

PXV_FUNC(int, do_read_wav_rescale_plot, (void));
PXV_FUNC(MENU*, read_wav_plot_menu, (void));
PXV_FUNC(int, do_read_wav_rescale_data_set, (void));
PXV_FUNC(int, read_wav_main, (int argc, char **argv));
#endif

