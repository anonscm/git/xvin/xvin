/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _READ_WAV_C_
#define _READ_WAV_C_

# include "allegro.h"
# include "winalleg.h"
# include "xvin.h"
# include "fftl32n.h"
# include "fillib.h"

/* If you include other plug-ins header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "read_wav.h"


char fullfile[4096], *fu = NULL;  // space for multiple files

# define IS_SOUND_SAMPLE 9876543 


// the plot data structure contains a user array to store integer parameters
// the previous defines specify the index used

// A sound sample from a wav file has some specific properties : Sampling freq, stereo or mono etc
// when we load a sound sample we use the Allegro library functions whict store samples in a special 
// data structure where sample are described by 8 bits or 16 bits numbers. However plots handle float
// data more convenient. We keep a memory of the original audio samples by saving the properties in 
// the user_ispare array associate with the dataset
// 

# define CHANNEL_TYPE 0
# define SAMPLING_FREQ 1
# define PRIORITY 2

// CHANNEL_TYPE may take values

# define IS_MONO_SAMPLE 0
# define IS_RIGHT_CHANNEL 1
# define IS_LEFT_CHANNEL 2



# ifdef WAVEFILE


# include "wavefile.h"

O_p *create_plot_from_wave_file(char *fullfile)
{
  register int i;
  wave_file *wf;
  stereo_int si;
  O_p *op = NULL;
  d_s *ds, *ds2;
  int nf;
  
  wf = open_wave_file(fullfile);
  if (wf == NULL) win_printf_OK("Could not load wave file :\n%s",backslash_to_slash(fullfile));

  if (wf->wChannels == 1)
    {
      nf = wf->nData_size/2;
      op = create_one_plot(nf, nf, 0);
      if (op == NULL) win_printf_OK("Could not create plot");
      ds = op->dat[0];
      for (i = 0; i < nf; i++)
	{
	  ds->yd[i] = wave_get(wf);
	  ds->xd[i] = i;
	}
    }
  else
    {
      nf = wf->nData_size/4;
      op = create_one_plot(nf, nf, 0);
      if (op == NULL) win_printf_OK("Could not create plot");
      ds = op->dat[0];
      ds2 = create_and_attach_one_ds(op, nf, nf, 0);
      if (ds2 == NULL) win_printf_OK("Could not create ds");
      for (i = 0; i < nf; i++)
	{
	  wave_get2(wf, &si);
	  ds->yd[i] = si.val1;
	  ds2->yd[i] = si.val2;
	  ds2->xd[i] = ds->xd[i] = i;
	}

    }
  set_plot_title(op,"\\stack{{File %s,}{ch %d, npts %d rate %d}}",backslash_to_slash(fullfile),
	       wf->wChannels, nf, wf->nSamplesPerSec);
  create_attach_select_x_un_to_op(op, IS_SECOND, 0, (float)1/wf->nSamplesPerSec, 0, 0, "s");	
  wave_free(wf);
  
  return op;
}

# endif

O_p *create_plot_from_al_wave_file(char *fullfile)
{
  register int i;
  SAMPLE *spl;
  unsigned short int *si;
  unsigned char *u8;
  O_p *op = NULL;
  d_s *ds, *ds2;
  int nf;
  
  spl = load_sample(fullfile);
  if (spl == NULL) return win_printf_ptr("Could not load wave file :\n%s",backslash_to_slash(fullfile));
  nf = spl->len;
  op = create_one_plot(nf, nf, 0);
  if (op == NULL) win_printf_OK("Could not create plot");
  ds = op->dat[0];
  ds->use.to = IS_SOUND_SAMPLE;
  ds->use.stuff = (void*)spl;
  if (spl->stereo == FALSE)
    {
      if (spl->bits == 8)
	{
	  u8 = (unsigned char *)spl->data;
	  for (i = 0; i < nf; i++)
	    {
	      ds->yd[i] =  u8[i];
	      ds->xd[i] = i;
	    }

	}
      else
	{
	  si = (unsigned short int *)spl->data;
	  for (i = 0; i < nf; i++)
	    {
	      ds->yd[i] =  si[i];
	      ds->xd[i] = i;
	    }
	}
      ds->user_fspare[CHANNEL_TYPE] = IS_MONO_SAMPLE;
      ds->user_fspare[SAMPLING_FREQ] = spl->freq;
    }
  else
    {
      ds2 = create_and_attach_one_ds(op, nf, nf, 0);
      if (ds2 == NULL) win_printf_OK("Could not create ds");
      ds->user_fspare[CHANNEL_TYPE] = IS_RIGHT_CHANNEL;
      ds->user_fspare[SAMPLING_FREQ] = spl->freq;
      ds2->user_fspare[CHANNEL_TYPE] = IS_RIGHT_CHANNEL;
      ds2->user_fspare[SAMPLING_FREQ] = spl->freq;
      if (spl->bits == 8)
	{
	  u8 = (unsigned char *)spl->data;
	  for (i = 0; i < nf; i++)
	    {
	      ds->yd[i] = u8[2*i];
	      ds2->yd[i] = u8[2*i+1];
	      ds2->xd[i] = ds->xd[i] = i;
	    }
	}
      else
	{
	  si = (unsigned short int *)spl->data;
	  for (i = 0; i < nf; i++)
	    {
	      ds->yd[i] =  si[2*i];
	      ds2->yd[i] =  si[2*i+1];
	      ds2->xd[i] = ds->xd[i] = i;
	    }
	}
    }
  set_plot_title(op,"\\stack{{File %s,}{ch %d, npts %d rate %d}}",backslash_to_slash(fullfile),
	       (spl->stereo == FALSE)?1:2, nf, spl->freq);
  create_attach_select_x_un_to_op(op, IS_SECOND, 0, (float)1/spl->freq, 0, 0, "s");	
  ds2->use.to = IS_SOUND_SAMPLE;
  ds2->use.stuff = (void*)spl;
  return op;
}

int	do_play_wav(void)
{
	O_p *op = NULL;
	d_s *ds;
	SAMPLE *spl;
	pltreg *pr = NULL;


	if(updating_menu_state != 0)	return D_O_K;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine plays a wave file associated "
				     "to a data set if it exists");
	}
	/* we first find the data that we need to transform */
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
		return win_printf_OK("cannot find data");



	if (ds->use.to != IS_SOUND_SAMPLE)
	  return win_printf_OK("Could find sound sample !");
	spl = (SAMPLE *)ds->use.stuff;
	play_sample(spl, 255, 128, 1000, FALSE);
	win_printf("bits %d stereo %d freq %d len %d prority %d\n lp_start %d end %d"
		   ,spl->bits,spl->stereo,spl->freq,spl->len,spl->priority,spl->loop_start,spl->loop_end);
	return 0;
}


int	do_ds_to_wav(void)
{
  register int i;
  O_p *op = NULL;
  d_s *ds;
  SAMPLE *spl;
  pltreg *pr = NULL;
  unsigned short int *ui;
  static int freq = 44100, rfr;
  int volume = 255; // max = 255, min = 0
  int balance = 128; // 0 -> left 128 right = left 255 -> right
  float  freqf, min = 0, max = 0, mean, mean2, gain, dt; 
  un_s *un;
  //char question[512];

  if(updating_menu_state != 0)	return D_O_K;
  
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine convert a plot to a sound wave and\n"
			   "plays this wave ");
    }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
    return win_printf_OK("cannot find data");

  if (ds->use.to == IS_SOUND_SAMPLE)
    return win_printf_OK("ds already contains an audio sample !");


  //question[0] = 0;
  un = op->xu[op->c_xu];
  if (un->type != IS_SECOND || un->decade != 0)
    win_printf("warning the x unit is not seconds !\nnumerical value will be wrong");
  dt = un->dx * (ds->xd[ds->nx-1] - ds->xd[0]);
  dt = (ds->nx > 1) ? dt/(ds->nx-1) : dt;
  freqf = (dt > 0) ? ((float)1)/dt : 44100;
  
  freq = 0;
  if (freqf > 44050 && freqf < 44150) rfr = 2;
  else if (freqf > 22000 && freqf < 22100) rfr = 1;
  else if (freqf > 11050 && freqf < 11000) rfr = 0;
  else        rfr = 0;


  for (i = 0, mean = 0, min = max = ds->yd[0]; i < ds->nx; i++)
    {
      min = (ds->yd[i] < min) ? ds->yd[i] : min;
      max = (ds->yd[i] > max) ? ds->yd[i] : max;
      mean += ds->yd[i];
    }
  for (i = 0, mean /= ds->nx, mean2 = 0; i < ds->nx; i++)
    mean2 += (ds->yd[i] - mean)* (ds->yd[i] - mean);
  mean2 /= ds->nx;
  mean2 = sqrt(mean2);

  gain = 3*mean2;
  gain = ((max - mean) > gain) ? (max - mean) : gain;  
  gain = ((mean - min) > gain) ? (mean - min) : gain; 
  gain = (gain != 0) ? ((float)33000)/gain : 1;
  i = win_scanf("Sampling freg (11025Hz %R 22050Hz %r 44100Hz %r\n"
		"Gain %12f offset %12f\n",&rfr,&gain,&mean);
  if (i == CANCEL) return D_O_K;
  if (rfr == 0) freq = 11025;
  else if (rfr == 1) freq = 22050;
  else if (rfr == 2) freq = 44100;
  win_printf("freq %d",freq);
  spl = create_sample(16, 0, freq, ds->nx);

  for (i = 0, ui = (unsigned short int *)spl->data; i < ds->nx; i++)
        ui[i] = 32768+(short int)(gain*(ds->yd[i]-mean));

  win_printf("playing sample of %d pts %d",ds->nx,spl->len);
  //save_sample("d:\\sample.wav", spl);

  play_sample(spl, volume, balance, 1000, FALSE);
  //win_printf("bits %d stereo %d freq %d len %d prority %d\n lp_start %d end %d"
  //	     ,spl->bits,spl->stereo,spl->freq,spl->len,spl->priority,spl->loop_start,spl->loop_end);

  ds2->use.to = IS_SOUND_SAMPLE;
  ds2->use.stuff = (void*)spl;
  //destroy_sample(spl);
  return 0;
}

int	do_sono_gram(void)
{
  register int i;
  O_p *op = NULL;
  O_i *oi = NULL;
  d_s *dsi;
  pltreg *pr = NULL;
  imreg *imr;
  union pix *pd;
  static int onx, ony = 256, nsi, ns;
  float *sp, mean, freqf, dt;
  un_s *un;
  //float min = 0, max = 0; 


  if(updating_menu_state != 0)	return D_O_K;
  
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine convert a sound wave in a \n"
			   "sonogram ");
    }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");

  i = win_scanf("Enter slidding window size (a power of 2) %d ",&ony);
  if ( i == CANCEL) return D_O_K;
  if (fft_init(ony)) return win_printf("Cannot init fft");
  //  onx = (int)((2*(dsi->nx - ony))/ony);


  un = op->xu[op->c_xu];
  if (un->type != IS_SECOND || un->decade != 0)
    win_printf("warning the x unit is not seconds !\nnumerical value will be wrong");
  dt = un->dx * (dsi->xd[dsi->nx-1] - dsi->xd[0]);
  dt = (dsi->nx > 1) ? dt/(dsi->nx-1) : dt;
  freqf = (dt > 0) ? ((float)1)/dt : 44100;


  for (ns = nsi = 0; (ns + ony) <= dsi->nx; nsi++, ns += ony/2);
  onx = nsi;    

 
  imr = find_imr_in_current_dialog(NULL);
  if (imr == NULL)	imr = create_and_register_new_image_project( 0,   32,  900,  668);
  if (imr == NULL)	return win_printf("could not create imreg");		
  oi = create_one_image(onx, ony/2, IS_COMPLEX_IMAGE);
  sp = (float*)calloc(ony,sizeof(float));
  if (oi == NULL || sp == NULL) return win_printf("Cannot alloc mem");
  pd = oi->im.pixel;
  for (ns = nsi = 0; (ns + ony) <= dsi->nx && nsi < onx; nsi++, ns += ony/2)
    {
      for (i=0 ; i< ony ; i++) sp[i] = dsi->yd[ns + i];
      fftwindow(ony, sp);
      for (i=0, mean = 0; i< ony ; i++)	mean += sp[i];
      for (i=0 ; i< ony ; i++) 	     sp[i] = dsi->yd[ns + i];
      mean /= ony;
      for (i=0; i< ony ; i++)	     sp[i] -= mean;
      fftwindow(ony, sp);
      realtr1(ony, sp);
      fft(ony, sp, 1);
      realtr2(ony, sp, 1);
      for (i=0 ; i< ony/2 ; i++) 	
	{
	  pd[i].fl[2*nsi] = sp[2*i];
	  pd[i].fl[2*nsi+1] = sp[2*i+1];
	}
    }

      //inherit_from_ds_to_im(dsi,oi);
  oi->im.mode = LOG_AMP;
  oi->need_to_refresh = ALL_NEED_REFRESH;		
  set_im_x_title(oi,"time");
  set_im_y_title(oi,"frequency");

  add_image(imr, oi->type, (void*)oi);
  remove_from_image(imr, imr->o_i[0]->im.data_type, (void*)imr->o_i[0]);
  find_zmin_zmax(oi);
  create_attach_select_y_un_to_oi(oi, 0, 0, freqf/ony, 0, 0, "Hz");	
  create_attach_select_x_un_to_oi(oi, IS_SECOND, 0, (float)ony/(2*freqf), 0, 0, "s");	
  return (refresh_image(imr, imr->n_oi - 1));
}

unsigned long dg[32];
int n_dg = 0;

void my_digi_recorder(void)
{
  if (n_dg < 32) dg[n_dg++] = my_uclock();
}


int start_recording(void)
{
  register int i, j, k;
  pltreg *pr;
  O_p *op = NULL;
  d_s *ds, *ds2;
  void *buff;
  unsigned short int *ui;
  //float twait, bufdt;
  static int fr = 0, in = 0, dur = 10;
  int nf, freq = 44100, src = 0, bufsize, nsp;
  unsigned long t_c, mucs = 0, waitms;

  if(updating_menu_state != 0)	return D_O_K;
  
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
      return win_printf_OK("This routine starts sound recording");

  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return win_printf_OK("cannot find data");

  i = win_scanf("Sampling freg (11025Hz %R 22050Hz %r 44100Hz %r\n"
		"Input source Mic %R Line %r CD %r\n"
		"Duration of record (in seconds) %d",&fr,&in,&dur);
  if (i == CANCEL) return D_O_K;

  if (fr == 0) freq = 11025;
  else if (fr == 1) freq = 22050;
  else if (fr == 2) freq = 44100;

  if (in == 0) src = SOUND_INPUT_MIC;
  else if (in == 1) src = SOUND_INPUT_LINE;
  else if (in == 2) src = SOUND_INPUT_CD;

  nf = freq * dur;

  op = create_and_attach_one_plot(pr, nf, nf, 0);
  if (op == NULL) win_printf_OK("Could not create plot");
  ds = op->dat[0];

  ds2 = create_and_attach_one_ds(op, 4096, 4096, 0);
  if (ds2 == NULL) win_printf_OK("Could not create plot");

  nsp = freq;
  mucs = (mucs == 0) ?get_my_uclocks_per_sec() : mucs;
  waitms = (mucs*nsp);
  win_printf("waitms %u / %d",waitms,mucs);
  set_sound_input_source(src);
  bufsize = start_sound_input(freq, 16, 0);

  if (bufsize == 0)  return win_printf_OK("Could not start recording");
  buff = calloc(bufsize,sizeof(char));
  if (buff == NULL)  return win_printf_OK("Could allocate buffer");
  ui = (unsigned short int *)buff;
  nsp = bufsize/2;
  //bufdt = (float)nsp/freq;

  n_dg = 0;
  digi_recorder = my_digi_recorder;
  t_c = my_uclock();
  waitms = mucs; // (mucs*nsp)/freq;
  for (i = 0, ds2->nx = 0; i < ds->nx; )// , ds2->nx = 0
    {
      //if (my_uclock() - t_c > waitms)
      //{
	  j = read_sound_input(buff);
	  ds2->yd[ds2->nx] += 1;
	  if (j)
	    {
	      if (ds2->nx < ds2->mx)
		{
		  //ds2->yd[ds2->nx] = j;
		  ds2->xd[ds2->nx++] = freq*(double)(my_uclock() - t_c)/mucs;
		}
	      for(k = 0; k < nsp; k++) 
		{
		  ds->yd[i+k] = ui[k];
		  ui[k] = 0;
		}
	      i += nsp;
	      waitms = (unsigned long)(((int)mucs*(i+nsp))/freq);
	    }
	  //}
    }
  ds2->ny = ds2->nx;
  stop_sound_input();
  //t_c = my_uclock() - t_c;
  for (i = 0; i < ds->nx; i++) ds->xd[i] = i;
  create_attach_select_x_un_to_op(op, IS_SECOND, 0, (float)1/freq, 0, 0, "s");	
  win_printf_OK(" buffer %d samples %f s",nsp,((double)(my_uclock() - t_c))/mucs);
  if (n_dg > 0)
    win_printf_OK("n_dg = %d dt 1 %f s",n_dg,((double)(dg[0] - t_c)/mucs));
  win_printf("waitms %u",waitms);  


  return D_REDRAW;
}

int	do_load_wav(void)
{
  register int i = 0, j;
  char path[512], file[256];
  pltreg *pr = NULL;
  O_p *op;
  int win_open = 0;
  
  if(updating_menu_state != 0)	return D_O_K;	
  if (fu == NULL)
    {
      fu = (char*)get_config_string("WAVE-FILE","last_loaded",NULL);
      if (fu != NULL)	
	{
	  strcpy(fullfile,fu);
	  fu = fullfile;
	}
      else
	{
	  my_getcwd(fullfile, 512);
	  strcat(fullfile,"\\");
	}
    }
#ifdef XV_WIN32
  if (full_screen)
    {
#endif
      switch_allegro_font(1);
      i = file_select_ex("Load sound (*.wav)", fullfile, "wav", 512, 0, 0);
      switch_allegro_font(0);
#ifdef XV_WIN32
    }
  else
    {
      extract_file_path(path, 512, fullfile);
      put_backslash(path);
      if (extract_file_name(file, 256, fullfile) == NULL)
	fullfile[0] = file[0] = 0;
      else strcpy(fullfile,file);
      fu = DoFileOpen("Load sound (*.wav)", path, fullfile, 512, "Wave Files (*.wav)\0*.wav\0All Files (*.*)\0*.*\0\0", "gr");
      i = (fu == NULL) ? 0 : 1;
      win_open = 1;
      if (fullfile[1+strlen(fullfile)] != 0)
	win_open = 2;
      
    }
#endif
  if (i != 0 && win_open < 2) 	
    {
      fu = fullfile;
      set_config_string("WAVE-FILE","last_loaded",fu);
      extract_file_name(file, 256, fullfile);
      extract_file_path(path, 512, fullfile);
      strcat(path,"\\");
      
      op = create_plot_from_al_wave_file(fullfile);
      if (op == NULL) return win_printf_OK("Could not loaded %s\n from %s",
				    backslash_to_slash(file), backslash_to_slash(path));
      pr = find_pr_in_current_dialog(NULL);
      if (pr == NULL)
	pr = create_and_register_new_plot_project(0,   32,  900,  668);
      
      if (pr == NULL)		
	{
	  free_one_plot(op);
	  win_printf_OK("Could not find or allocte plot region!");
	}
      j = pr->n_op;
      add_data_to_pltreg (pr, IS_ONE_PLOT, (void *)op);
      if (j == 0) remove_data_from_pltreg (pr, IS_ONE_PLOT, (void *)pr->o_p[j]);
      switch_plot(pr, pr->n_op - 1);
      
    }
# ifdef ENCOURS
  if (i != 0 && win_open > 1) 	
    {
      strcpy(path,fullfile);
      strcat(path,"\\");
      fi = fullfile;
      for (fi = fi + 1 + strlen(fi); fi[0] != 0; fi = fi + 1 + strlen(fi))
	{
	  strcpy(file,fi);
	  sprintf(ffile,"%s%s",path,file);
	  set_config_string("PLOT-GR-FILE","last_loaded",ffile);
	  op = create_plot_from_gr_file(file, path);
	  if (op == NULL) return win_printf_OK("Could not loaded %s\n from %s",
					backslash_to_slash(file), backslash_to_slash(path));
	  //else win_printf("loaded \n%s",backslash_to_slash(fullfile));
	  pr = (pr == NULL) ? find_pr_in_current_dialog(NULL) : pr;
	  if (pr == NULL)
	    pr = create_and_register_new_plot_project(0,   32,  900,  668);
	  
	  if (pr == NULL)		
	    {
	      free_one_plot(op);
	      win_printf_OK("Could not find or allocte plot region!");
	    }
	  j = pr->n_op;
	  add_data_to_pltreg (pr, IS_ONE_PLOT, (void *)op);
	  if (j == 0) remove_data_from_pltreg (pr, IS_ONE_PLOT, (void *)pr->o_p[j]);
	  switch_plot(pr, pr->n_op - 1);
	  reorder_loaded_plot_file_list(file, path);
	  reorder_visited_path_list(path);
	}
      
    }
# endif
  return D_REDRAW;
}



int do_read_wav_rescale_data_set(void)
{
	register int i, j;
	O_p *op = NULL;
	int nf;
	static float factor = 1;
	d_s *ds, *dsi;
	pltreg *pr = NULL;


	if(updating_menu_state != 0)	return D_O_K;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine multiply the y coordinate"
	"of a data set by a number");
	}
	/* we first find the data that we need to transform */
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
		return win_printf_OK("cannot find data");
	nf = dsi->nx;	/* this is the number of points in the data set */
	i = win_scanf("By what factor do you want to multiply y %f",&factor);
	if (i == CANCEL)	return OFF;

	if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
		return win_printf_OK("cannot create plot !");

	for (j = 0; j < nf; j++)
	{
		ds->yd[j] = factor * dsi->yd[j];
		ds->xd[j] = dsi->xd[j];
	}

	/* now we must do some house keeping */
	inherit_from_ds_to_ds(ds, dsi);
	ds->treatement = my_sprintf(ds->treatement,"y multiplied by %f",factor);
	/* refisplay the entire plot */
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
}

int do_read_wav_rescale_plot(void)
{
	register int i, j;
	O_p *op = NULL, *opn = NULL;
	int nf;
	static float factor = 1;
	d_s *ds, *dsi;
	pltreg *pr = NULL;


	if(updating_menu_state != 0)	return D_O_K;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine multiply the y coordinate"
	"of a data set by a number and place it in a new plot");
	}
	/* we first find the data that we need to transform */
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
		return win_printf_OK("cannot find data");
	nf = dsi->nx;	/* this is the number of points in the data set */
	i = win_scanf("By what factor do you want to multiply y %f",&factor);
	if (i == CANCEL)	return OFF;

	if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	ds = opn->dat[0];

	for (j = 0; j < nf; j++)
	{
		ds->yd[j] = factor * dsi->yd[j];
		ds->xd[j] = dsi->xd[j];
	}

	/* now we must do some house keeping */
	inherit_from_ds_to_ds(ds, dsi);
	ds->treatement = my_sprintf(ds->treatement,"y multiplied by %f",factor);
	set_plot_title(opn, "Multiply by %f",factor);
	if (op->x_title != NULL) set_plot_x_title(opn, op->x_title);
	if (op->y_title != NULL) set_plot_y_title(opn, op->y_title);
	opn->filename = Transfer_filename(op->filename);
	uns_op_2_op(opn, op);
	/* refisplay the entire plot */
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
}

MENU *read_wav_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	//	add_item_to_menu(mn,"data set rescale in Y", do_read_wav_rescale_data_set,NULL,0,NULL);
	//add_item_to_menu(mn,"plot rescale in Y", do_read_wav_rescale_plot,NULL,0,NULL);
	add_item_to_menu(mn,"load wave in plot", do_load_wav,NULL,0,NULL);
	add_item_to_menu(mn,"play wave from plot", do_play_wav,NULL,0,NULL);
	add_item_to_menu(mn,"convert plot to wave", do_ds_to_wav,NULL,0,NULL);
	add_item_to_menu(mn,"sonogram", do_sono_gram,NULL,0,NULL);
	add_item_to_menu(mn,"recording", start_recording,NULL,0,NULL);

	return mn;
}

int	read_wav_main(int argc, char **argv)
{
  static int sound_init = 0;
  /* install a digital sound driver */
  (void)argc;
  if (sound_init == 0)
    {
      if (install_sound(DIGI_AUTODETECT, MIDI_NONE, argv[0]) != 0) 
	return win_printf_OK("Error initialising sound system\n%s\n", allegro_error);
      sound_init = 1;
      if (install_sound_input(DIGI_AUTODETECT, MIDI_NONE) != 0)
	win_printf_OK("Error initialising recording system\n%s\n", allegro_error);
      else
	{
	  display_title_message("Bits %d stereo %d freq %d",
			get_sound_input_cap_bits(),
			get_sound_input_cap_stereo(),
			get_sound_input_cap_rate(16, 1));
	}

    }
  add_plot_treat_menu_item ( "read_wav", NULL, read_wav_plot_menu(), 0, NULL);
  return D_O_K;
}

int	read_wav_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  remove_item_to_menu(plot_treat_menu, "read_wav", NULL, NULL);
  return D_O_K;
}
#endif

