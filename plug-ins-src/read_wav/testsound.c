# include "allegro.h"
# include <stdio.h>
# include <math.h>

/*  typedef struct SAMPLE
   int bits;                   - 8 or 16
   int stereo;                 - sample type flag
   int freq;                   - sample frequency
   int priority;               - 0-255
   unsigned long len;          - length (in samples)
   unsigned long loop_start;   - loop start position
   unsigned long loop_end;     - loop finish position
   void *data;                 - raw sample data
*/


int	main(int argc, char **argv)
{
  SAMPLE *spl;
  int i, fsample = 44100, freq = 1000;
  int len = 5*fsample;  // we produce a 5s sample sound
  int volume = 255; // max = 255, min = 0
  int balance = 128; // 0 -> left 128 right = left 255 -> right
  unsigned short int *usi = NULL;

  (void)argc;
  (void)argv;
  if (allegro_init() != 0)        exit(1);
  install_keyboard(); 
  install_timer();
  /* install a digital sound driver */  
  if (install_sound(DIGI_AUTODETECT, MIDI_NONE, NULL) != 0) 
    return printf("Error initialising sound system\n%s\n", allegro_error);
  if (install_sound_input(DIGI_AUTODETECT, MIDI_NONE) != 0)
    return printf("Error initialising recording system\n%s\n", allegro_error);
  else
    {
     printf("Bits %d stereo %d freq %d",get_sound_input_cap_bits(),
	    get_sound_input_cap_stereo(),get_sound_input_cap_rate(16, 1));
    }

  spl = create_sample(16, 0, fsample, len); // fabrique un échantillon de son en mono sur 16 bits

  for (i = 0, usi = (unsigned short int *)spl->data; i < len; i++)
    {
      usi[i] = 32768 + (int) ((30000*(i%freq))/freq)*sin((2*M_PI*i*freq)/fsample);
    }
  play_sample(spl, volume, balance, 1000, FALSE);
  {} while ((!key[KEY_ESC]) && (!key[KEY_SPACE]));  // attend que l'utilisateur tappe escape
  return 0;
}
END_OF_MAIN()
