
#include "../wavefile.h"
#include <math.h>
#include <stdio.h>

int main(void)
{
	unsigned int i;
	wave_info wi;
	wave_file *wf;

	stereo_int si;

	wi.wChannels = 2;
	wi.nSamplesPerSec = 22050;

	/* thus if time is 't' and the sample is 'i', then t = i/22050 */

	
	/* want to have 220500 samples, for ten seconds of sound */
	/* in next instruction set aside 4*nSamplesPerSec of data storage, because each sample is 4 bytes */
	if((wf = new_wave_file("stream2channel.wav", &wi, 882000))==(wave_file *)NULL)
	{
		printf("unable to create wave file, program aborted.\n");
		return 0;
	}

	for(i = 0; i < 220500; i++)
	{
		/*
		val1: 25000*sin(2*PI*440*t*sin(2*t))
		*/
		si.val1 = (short int)(25000*sin(sin(.000090703*(double)i)*.125315193*(double)i));

		/*
		val2: 25000*sin(2*PI*330*t*sin(3*t))
		*/
		si.val2 = (short int)(25000*sin(sin(.000136054*(double)i)*.093986395*(double)i));
		
		wave_put2(&si, wf);
	}

	if(wave_save(wf)) printf("error saving it!\n");
	wave_free(wf);


	return 0;
}








/* end of file */

