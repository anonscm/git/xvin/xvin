
#include "../wavefile.h"
#include <math.h>
#include <stdio.h>

int main(void)
{
	wave_file *wf;

	wf = open_wave_file("wave1.wav");
	print_wave_info(wf);
	wave_free(wf);

	wf = open_wave_file("wave2.wav");
	print_wave_info(wf);
	wave_free(wf);

	wf = open_wave_file("wave3.wav");
	print_wave_info(wf);
	wave_free(wf);


	return 0;
}








/* end of file */

