
#include "../wavefile.h"
#include <math.h>
#include <stdio.h>


short int square_wave(unsigned int k)
{
	if(k%26<13) return 0;
	else return 15000;
}

int main(void)
{
	unsigned int i;
	short int j;
	wave_info wi;
	wave_file *wf;

	wi.wChannels = 1;
	wi.nSamplesPerSec = 11025;

	/* thus if time is 't' and the sample is 'i', then t = i/44100 */

	/* want to have 110250 samples, for ten seconds of sound */
	/* in next instruction set aside 2*110250 of data storage, because each sample is 2 bytes */
	if((wf = new_wave_file("ram1channel.wav", &wi, 220500))==(wave_file *)NULL)
	{
		printf("unable to create wave file, program aborted.\n");
		return 0;
	}


	wave_set_data_size(220500,wf);

	for(i = 0; i < 110250; i++)
	{
		/*
		j: see above definition of square wave, frequency ~ 440 hz
		*/
		j = square_wave(i);

		wave_ram_put(j, i, wf);
	}

	if(wave_save(wf)) printf("error saving it!\n");
	wave_free(wf);


	return 0;
}








/* end of file */

