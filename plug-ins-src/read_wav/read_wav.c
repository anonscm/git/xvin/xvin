/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _READ_WAV_C_
#define _READ_WAV_C_

# include "allegro.h"
# include "winalleg.h"
# include "xvin.h"
# include "fftl32n.h"
# include "fillib.h"

/* If you include other plug-ins header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "read_wav.h"


char fullfile[4096], *fu = NULL;  // space for multiple files

# define IS_SOUND_SAMPLE 9876543 


// the plot data structure contains a user array to store integer parameters
// the previous defines specify the index used

// A sound sample from a wav file has some specific properties : Sampling freq, stereo or mono etc
// when we load a sound sample we use the Allegro library functions whict store samples in a special 
// data structure where sample are described by 8 bits or 16 bits numbers. However plots handle float
// data more convenient. We keep a memory of the original audio samples by saving the properties in 
// the user_ispare array associate with the dataset
// 

# define CHANNEL_TYPE 0
# define SAMPLING_FREQ 1
# define PRIORITY 2

// CHANNEL_TYPE may take values

# define IS_MONO_SAMPLE 0
# define IS_RIGHT_CHANNEL 1
# define IS_LEFT_CHANNEL 2



O_p *create_plot_from_al_wave_file(char *fullfile)
{
  register int i;
  SAMPLE *spl;
  unsigned short int *si;
  unsigned char *u8;
  O_p *op = NULL;
  d_s *ds = NULL, *ds2 = NULL;
  int nf;
  
  spl = load_sample(fullfile);
  if (spl == NULL) return win_printf_ptr("Could not load wave file :\n%s",backslash_to_slash(fullfile));
  nf = spl->len;
  op = create_one_plot(nf, nf, 0);
  if (op == NULL) win_printf_OK("Could not create plot");
  ds = op->dat[0];
  ds->use.to = IS_SOUND_SAMPLE;
  ds->use.stuff = (void*)spl;
  if (spl->stereo == FALSE)
    {
      if (spl->bits == 8)
	{
	  u8 = (unsigned char *)spl->data;
	  for (i = 0; i < nf; i++)
	    {
	      ds->yd[i] =  u8[i];
	      ds->xd[i] = i;
	    }

	}
      else
	{
	  si = (unsigned short int *)spl->data;
	  for (i = 0; i < nf; i++)
	    {
	      ds->yd[i] =  si[i];
	      ds->xd[i] = i;
	    }
	}
      ds->user_fspare[CHANNEL_TYPE] = IS_MONO_SAMPLE;
      ds->user_fspare[SAMPLING_FREQ] = spl->freq;
    }
  else
    {
      ds2 = create_and_attach_one_ds(op, nf, nf, 0);
      if (ds2 == NULL) win_printf_OK("Could not create ds");
      ds->user_fspare[CHANNEL_TYPE] = IS_RIGHT_CHANNEL;
      ds->user_fspare[SAMPLING_FREQ] = spl->freq;
      ds2->user_fspare[CHANNEL_TYPE] = IS_RIGHT_CHANNEL;
      ds2->user_fspare[SAMPLING_FREQ] = spl->freq;
      if (spl->bits == 8)
	{
	  u8 = (unsigned char *)spl->data;
	  for (i = 0; i < nf; i++)
	    {
	      ds->yd[i] = u8[2*i];
	      ds2->yd[i] = u8[2*i+1];
	      ds2->xd[i] = ds->xd[i] = i;
	    }
	}
      else
	{
	  si = (unsigned short int *)spl->data;
	  for (i = 0; i < nf; i++)
	    {
	      ds->yd[i] =  si[2*i];
	      ds2->yd[i] =  si[2*i+1];
	      ds2->xd[i] = ds->xd[i] = i;
	    }
	}
    }
  set_plot_title(op,"\\stack{{File %s,}{ch %d, npts %d rate %d}}",backslash_to_slash(fullfile),
	       (spl->stereo == FALSE)?1:2, nf, spl->freq);
  create_attach_select_x_un_to_op(op, IS_SECOND, 0, (float)1/spl->freq, 0, 0, "s");	
  if (ds2 != NULL)
    {
      ds2->use.to = IS_SOUND_SAMPLE;
      ds2->use.stuff = (void*)spl;
    }
  return op;
}

int	do_play_wav(void)
{
	O_p *op = NULL;
	d_s *ds;
	SAMPLE *spl;
	pltreg *pr = NULL;


	if(updating_menu_state != 0)	return D_O_K;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine plays a wave file associated "
				     "to a data set if it exists");
	}
	/* we first find the data that we need to transform */
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
		return win_printf_OK("cannot find data");



	if (ds->use.to != IS_SOUND_SAMPLE)
	  return win_printf_OK("Could find sound sample !");
	spl = (SAMPLE *)ds->use.stuff;
	play_sample(spl, 255, 128, 1000, FALSE);
	win_printf("bits %d stereo %d freq %d len %d prority %d\n lp_start %d end %d"
		   ,spl->bits,spl->stereo,spl->freq,spl->len,spl->priority,spl->loop_start,spl->loop_end);
	return 0;
}


int	do_ds_to_wav(void)
{
  register int i;
  O_p *op = NULL;
  d_s *ds;
  SAMPLE *spl;
  pltreg *pr = NULL;
  unsigned short int *ui;
  static int freq = 44100, rfr;
  int volume = 255; // max = 255, min = 0
  int balance = 128; // 0 -> left 128 right = left 255 -> right
  float  freqf, min = 0, max = 0, mean, mean2, gain, dt; 
  un_s *un;
  //char question[512];

  if(updating_menu_state != 0)	return D_O_K;
  
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine convert a plot to a sound wave and\n"
			   "plays this wave ");
    }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
    return win_printf_OK("cannot find data");

  if (ds->use.to == IS_SOUND_SAMPLE)
    return win_printf_OK("ds already contains an audio sample !");


  //question[0] = 0;
  un = op->xu[op->c_xu];
  if (un->type != IS_SECOND || un->decade != 0)
    win_printf("warning the x unit is not seconds !\nnumerical value will be wrong");
  dt = un->dx * (ds->xd[ds->nx-1] - ds->xd[0]);
  dt = (ds->nx > 1) ? dt/(ds->nx-1) : dt;
  freqf = (dt > 0) ? ((float)1)/dt : 44100;
  
  freq = 0;
  if (freqf > 44050 && freqf < 44150) rfr = 2;
  else if (freqf > 22000 && freqf < 22100) rfr = 1;
  else if (freqf > 11050 && freqf < 11000) rfr = 0;
  else        rfr = 0;


  for (i = 0, mean = 0, min = max = ds->yd[0]; i < ds->nx; i++)
    {
      min = (ds->yd[i] < min) ? ds->yd[i] : min;
      max = (ds->yd[i] > max) ? ds->yd[i] : max;
      mean += ds->yd[i];
    }
  for (i = 0, mean /= ds->nx, mean2 = 0; i < ds->nx; i++)
    mean2 += (ds->yd[i] - mean)* (ds->yd[i] - mean);
  mean2 /= ds->nx;
  mean2 = sqrt(mean2);

  gain = 3*mean2;
  gain = ((max - mean) > gain) ? (max - mean) : gain;  
  gain = ((mean - min) > gain) ? (mean - min) : gain; 
  gain = (gain != 0) ? ((float)33000)/gain : 1;
  i = win_scanf("Sampling freg (11025Hz %R 22050Hz %r 44100Hz %r\n"
		"Gain %12f offset %12f\n",&rfr,&gain,&mean);
  if (i == CANCEL) return D_O_K;
  if (rfr == 0) freq = 11025;
  else if (rfr == 1) freq = 22050;
  else if (rfr == 2) freq = 44100;
  win_printf("freq %d",freq);
  spl = create_sample(16, 0, freq, ds->nx);

  for (i = 0, ui = (unsigned short int *)spl->data; i < ds->nx; i++)
        ui[i] = 32768+(short int)(gain*(ds->yd[i]-mean));

  win_printf("playing sample of %d pts %d",ds->nx,spl->len);
  //save_sample("d:\\sample.wav", spl);

  play_sample(spl, volume, balance, 1000, FALSE);
  //win_printf("bits %d stereo %d freq %d len %d prority %d\n lp_start %d end %d"
  //	     ,spl->bits,spl->stereo,spl->freq,spl->len,spl->priority,spl->loop_start,spl->loop_end);

  ds->use.to = IS_SOUND_SAMPLE;
  ds->use.stuff = (void*)spl;
  //destroy_sample(spl);
  return 0;
}


int	do_load_wav(void)
{
  register int j;
  char path[512], file[256], wavfilename[1024];
  pltreg *pr = NULL;
  O_p *op;
  
  if(updating_menu_state != 0)	return D_O_K;	



  wavfilename[0] = 0;

  if (do_select_file(wavfilename, sizeof(wavfilename), "Load sound (*.wav)\0", "wav\0", "loading audio sample\0", 1))
    return win_printf_OK("Cannot select input file");

  fu = fullfile;
  set_config_string("WAVE-FILE","last_loaded",wavfilename);
  extract_file_name(file, 256, wavfilename);
  extract_file_path(path, 512, wavfilename);
  strcat(path,"\\");
  
  op = create_plot_from_al_wave_file(wavfilename);
  if (op == NULL) return win_printf_OK("Could not loaded %s\n from %s",
				       backslash_to_slash(file), backslash_to_slash(path));
  pr = find_pr_in_current_dialog(NULL);
  if (pr == NULL)
    pr = create_and_register_new_plot_project(0,   32,  900,  668);
  
  if (pr == NULL)		
    {
      free_one_plot(op);
      win_printf_OK("Could not find or allocte plot region!");
    }
  j = pr->n_op;
  add_data_to_pltreg (pr, IS_ONE_PLOT, (void *)op);
  if (j == 0) remove_data_from_pltreg (pr, IS_ONE_PLOT, (void *)pr->o_p[j]);
  switch_plot(pr, pr->n_op - 1);
  return D_REDRAW;
}


MENU *read_wav_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	//	add_item_to_menu(mn,"data set rescale in Y", do_read_wav_rescale_data_set,NULL,0,NULL);
	//add_item_to_menu(mn,"plot rescale in Y", do_read_wav_rescale_plot,NULL,0,NULL);
	add_item_to_menu(mn,"load wave in plot", do_load_wav,NULL,0,NULL);
	add_item_to_menu(mn,"play wave from plot", do_play_wav,NULL,0,NULL);
	add_item_to_menu(mn,"convert plot to wave", do_ds_to_wav,NULL,0,NULL);

	return mn;
}

int	read_wav_main(int argc, char **argv)
{
  static int sound_init = 0;
  /* install a digital sound driver */
  (void)argc;
  if (sound_init == 0)
    {
      if (install_sound(DIGI_AUTODETECT, MIDI_NONE, argv[0]) != 0) 
	return win_printf_OK("Error initialising sound system\n%s\n", allegro_error);
      sound_init = 1;
      if (install_sound_input(DIGI_AUTODETECT, MIDI_NONE) != 0)
	win_printf_OK("Error initialising recording system\n%s\n", allegro_error);
      else
	{
	  display_title_message("Bits %d stereo %d freq %d",
			get_sound_input_cap_bits(),
			get_sound_input_cap_stereo(),
			get_sound_input_cap_rate(16, 1));
	}

    }
  add_plot_treat_menu_item ( "read_wav", NULL, read_wav_plot_menu(), 0, NULL);
  return D_O_K;
}

int	read_wav_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  remove_item_to_menu(plot_treat_menu, "read_wav", NULL, NULL);
  return D_O_K;
}
#endif

