/*
*    Plug-in program for image treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _TSTFREEIMAGE_C_
#define _TSTFREEIMAGE_C_

# include "allegro.h"
# include "xvin.h"
# include <ctype.h>

/* If you include other regular header do it here*/ 
#include "FreeImage.h"

/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "TstFreeImage.h"
//place here other headers of this plugin 

/*
 cp -uvp /mingw32/bin/libfreeimage-3.dll ../../bin
 cp -uvp /mingw32/bin/libHalf-2_2.dll ../../bin
 cp -uvp /mingw32/bin/libIex-2_2.dll ../../bin
 cp -uvp /mingw32/bin/libIlmImf-2_2.dll ../../bin
 cp -uvp /mingw32/bin/libIlmThread-2_2.dll ../../bin
 cp -uvp /mingw32/bin/libImath-2_2.dll ../../bin
 cp -uvp /mingw32/bin/libopenjp2-7.dll ../../bin
 cp -uvp /mingw32/bin/libraw-10.dll ../../bin
 cp -uvp /mingw32/bin/libjasper-1.dll ../../bin
 cp -uvp /mingw32/bin/liblcms2-2.dll ../../bin
 cp -uvp /mingw32/bin/libwebp-5.dll ../../bin
 cp -uvp /mingw32/bin/libwebpdecoder-1.dll ../../bin
 cp -uvp /mingw32/bin/libwebpmux-1.dll ../../bin
*/


// ----------------------------------------------------------

char menuname[64] = {0};

FIBITMAP *convert_oi_char_image(O_i *ois, int keep_selected_BW_limit);
FIBITMAP *convert_oi_rgb_image(O_i *ois);
FIBITMAP *convert_oi_rgba_image(O_i *ois);




/**
	FreeImage error handler
	@param fif Format / Plugin responsible for the error 
	@param message Error message
*/
void FreeImageErrorHandler(FREE_IMAGE_FORMAT fif, const char *message) 
{
  win_printf("FreeImage Error\n%s Format\n%s\n"
	     ,((fif != FIF_UNKNOWN)? FreeImage_GetFormatFromFIF(fif):"Unknown"),message);
}

// ----------------------------------------------------------




int do_TstFreeImage_average_along_y(void)
{
	register int i, j;
	int l_s, l_e;
	O_i *ois = NULL;
	O_p *op = NULL;
	d_s *ds;
	imreg *imr = NULL;


	if(updating_menu_state != 0)	return D_O_K;

	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine average the image intensity"
			"of several lines of an image");
	}
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	return D_O_K;
	/* we grap the data that we need, the image and the screen region*/
	l_s = 0; l_e = ois->im.ny;
	/* we ask for the limit of averaging*/
	i = win_scanf("Average Between lines%d and %d",&l_s,&l_e);
	if (i == WIN_CANCEL)	return D_O_K;
	/* we create a plot to hold the profile*/
	op = create_one_plot(ois->im.nx,ois->im.nx,0);
	if (op == NULL) return (win_printf_OK("cant create plot!"));
	ds = op->dat[0]; /* we grab the data set */
	op->y_lo = ois->z_black;	op->y_hi = ois->z_white;
	op->iopt2 |= Y_LIM;	op->type = IM_SAME_X_AXIS;
	inherit_from_im_to_ds(ds, ois);
	op->filename = Transfer_filename(ois->filename);
	set_plot_title(op,"FreeImage averaged profile from line %d to %d",l_s, l_e);
	set_formated_string(&ds->treatement,"Freeimage averaged profile from line %d to %d",l_s, l_e);
	uns_oi_2_op(ois, IS_Z_UNIT_SET, op, IS_Y_UNIT_SET);
	uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
	for (i=0 ; i< ds->nx ; i++)	ds->yd[i] = 0;
	for (i=l_s ; i< l_e ; i++)
	{
		extract_raw_line(ois, i, ds->xd);
		for (j=0 ; j< ds->nx ; j++)	ds->yd[j] += ds->xd[j];
	}
	for (i=0, j = l_e - l_s ; i< ds->nx && j !=0 ; i++)
	{
		ds->xd[i] = i;
		ds->yd[i] /= j;
	}
	add_one_image (ois, IS_ONE_PLOT, (void *)op);
	ois->cur_op = ois->n_op - 1;
	broadcast_dialog_message(MSG_DRAW,0);
	return D_REDRAWME;
}

O_i	*TstFreeImage_image_multiply_by_a_scalar(O_i *ois, float factor)
{
	register int i, j;
	O_i *oid;
	float tmp;
	int onx, ony, data_type;
	union pix *ps, *pd;

	onx = ois->im.nx;	ony = ois->im.ny;	data_type = ois->im.data_type;
	oid =  create_one_image(onx, ony, data_type);
	if (oid == NULL)
	{
		win_printf("Can't create dest image");
		return NULL;
	}
	if (data_type == IS_CHAR_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].ch[j];
				pd[i].ch[j] = (tmp < 0) ? 0 :
					((tmp > 255) ? 255 : (unsigned char)tmp);
			}
		}
	}
	else if (data_type == IS_INT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].in[j];
				pd[i].in[j] = (tmp < -32768) ? -32768 :
					((tmp > 32767) ? 32767 : (short int)tmp);
			}
		}
	}
	else if (data_type == IS_UINT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].ui[j];
				pd[i].ui[j] = (tmp < 0) ? 0 :
					((tmp > 65535) ? 65535 : (unsigned short int)tmp);
			}
		}
	}
	else if (data_type == IS_LINT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].li[j];
				pd[i].li[j] = (tmp < 0x10000000) ? 0x10000000 :
					((tmp > 0x7FFFFFFF) ? 0x7FFFFFFF : (int)tmp);
			}
		}
	}
	else if (data_type == IS_FLOAT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].fl[j] = factor * ps[i].fl[j];
			}
		}
	}
	else if (data_type == IS_DOUBLE_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].db[j] = factor * ps[i].db[j];
			}
		}
	}
	else if (data_type == IS_COMPLEX_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].fl[2*j] = factor * ps[i].fl[2*j];
				pd[i].fl[2*j+1] = factor * ps[i].fl[2*j+1];
			}
		}
	}
	else if (data_type == IS_COMPLEX_DOUBLE_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].db[2*j] = factor * ps[i].db[2*j];
				pd[i].db[2*j+1] = factor * ps[i].db[2*j+1];
			}
		}
	}
	else if (data_type == IS_RGB_PICTURE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< 3*onx; j++)
			{
				tmp = factor * ps[i].ch[j];
				pd[i].ch[j] = (tmp < 0) ? 0 :
					((tmp > 255) ? 255 : (unsigned char)tmp);
			}
		}
	}
	else if (data_type == IS_RGBA_PICTURE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< 4*onx; j++)
			{
				tmp = factor * ps[i].ch[j];
				pd[i].ch[j] = (tmp < 0) ? 0 :
					((tmp > 255) ? 255 : (unsigned char)tmp);
			}
		}
	}
	inherit_from_im_to_im(oid,ois);
	uns_oi_2_oi(oid,ois);
	oid->im.win_flag = ois->im.win_flag;
	if (ois->title != NULL)	set_im_title(oid, "%s rescaled by %g",
		 ois->title,factor);
	set_formated_string(&oid->im.treatement,
		"Image %s rescaled by %g", ois->filename,factor);
	return oid;
}


O_i	*TstFreeImage_image_rescale(O_i *ois, int dst_width, int dst_height, int filter)
{
	register int x, y;
	O_i *oid;
	int im, nfi;
	int  data_type;
	unsigned int bytespp;
	union pix *pd;
	FIBITMAP *dib =  NULL, *dibr = NULL;
	BYTE *bits = NULL;


	if (ois == NULL) return NULL;
	data_type = ois->im.data_type;

	if (data_type != IS_CHAR_IMAGE && data_type != IS_RGB_PICTURE
	    && data_type != IS_RGBA_PICTURE)
	  return NULL;

	nfi = abs(ois->im.n_f);// cf = ois->im.c_f;
	nfi = (nfi == 0) ? 1 : nfi;

	if (nfi < 2) oid = create_one_image(dst_width, dst_height, data_type);
	else         oid = create_one_movie(dst_width, dst_height, data_type, nfi);

	if (oid == NULL)
	  {
	    win_printf("Can't create dest image");
	    return NULL;
	  }
	for (im = 0; im < nfi; im++)
	  {
	    if (nfi > 1)
	      {
		switch_frame(ois,im);
		switch_frame(oid,im);
	      }
	    if (data_type == IS_CHAR_IMAGE)          dib = convert_oi_char_image(ois, 0);
	    else if (data_type == IS_RGB_PICTURE)    dib = convert_oi_rgb_image(ois);
	    else if (data_type == IS_RGBA_PICTURE)   dib = convert_oi_rgba_image(ois);
	    else dib = NULL;
	    if (dib == NULL)     return NULL;

	    dibr = FreeImage_Rescale(dib, dst_width, dst_height, filter);
	    if (dibr == NULL)     return NULL;


	    bytespp = FreeImage_GetLine(dibr) / dst_width;
	    if (data_type == IS_CHAR_IMAGE)
	      {
		for(pd = oid->im.pixel, y = 0; y < dst_height; y++) 
		    {
		      for(x = 0, bits = FreeImage_GetScanLine(dibr, y); x < dst_width; x++) 
			pd[y].ch[x] = bits[x];
		    }
		}
	    else if (data_type == IS_RGB_PICTURE)
	      {
		// Calculate the number of bytes per pixel (3 for 24-bit or 4 for 32-bit)
		for(pd = oid->im.pixel, y = 0; y < dst_height; y++) 
		  {
		    for(x = 0, bits = FreeImage_GetScanLine(dibr, y); x < dst_width; x++) 
		      {
			pd[y].rgb[x].r = bits[FI_RGBA_RED];
			pd[y].rgb[x].g = bits[FI_RGBA_GREEN];
			pd[y].rgb[x].b = bits[FI_RGBA_BLUE];
			bits += bytespp; 
		      }
		  }
	      }
	    else if (data_type == IS_RGBA_PICTURE)
		{
		  // Calculate the number of bytes per pixel (3 for 24-bit or 4 for 32-bit)
		  for(pd = oid->im.pixel, y = 0; y < dst_height; y++) 
		    {
		      for(x = 0, bits = FreeImage_GetScanLine(dibr, y); x < dst_width; x++) 
			{
			  pd[y].rgba[x].r = bits[FI_RGBA_RED];
			  pd[y].rgba[x].g = bits[FI_RGBA_GREEN];
			  pd[y].rgba[x].b = bits[FI_RGBA_BLUE];
			  pd[y].rgba[x].a = bits[FI_RGBA_ALPHA];
			  bits += bytespp; 
			}
		    }
		}
	    FreeImage_Unload(dib);
	    dib = NULL;
	    FreeImage_Unload(dibr);
	    dibr = NULL;
	  }
	inherit_from_im_to_im(oid,ois);
	uns_oi_2_oi(oid,ois);
	oid->im.win_flag = ois->im.win_flag;
	if (ois->title != NULL)	set_im_title(oid, "%s rescaled from %dx%d to %dx%d",ois->title,ois->im.nx,ois->im.ny,dst_width, dst_height);
	set_formated_string(&oid->im.treatement,"Image %s  rescaled from %dx%d to %dx%d", ois->filename,ois->im.nx,ois->im.ny,dst_width, dst_height);
	return oid;
}


O_i	*TstFreeImage_color_image_to_bw(O_i *ois)
{
	register int i, j;
	O_i *oid;
	int type, im, nfi;
	int onx, ony, data_type;
	union pix *ps, *pd;

	onx = ois->im.nx;	ony = ois->im.ny;	
	data_type = ois->im.data_type;
	nfi = abs(ois->im.n_f);// cf = ois->im.c_f;
	nfi = (nfi == 0) ? 1 : nfi;
	if (data_type == IS_RGB_PICTURE || data_type == IS_RGBA_PICTURE) type = IS_CHAR_IMAGE;
	else if (data_type == IS_RGB16_PICTURE || data_type == IS_RGBA16_PICTURE) type = IS_INT_IMAGE;
	else 
	  {
	    win_printf ("the image is not of the right type!\n"
			"Cannot perform backward fft!");
	    return(NULL);
	  }
        if (nfi < 2) oid = create_one_image(onx, ony, type);
        else         oid = create_one_movie(onx, ony, type, nfi);
	if (oid == NULL)
	{
		win_printf("Can't create dest image");
		return NULL;
	}
	
	for (im = 0; im < nfi; im++)
	  {
	    if (nfi > 1)
	      {
		switch_frame(ois,im);
		switch_frame(oid,im);
	      }
	    if (data_type == IS_RGB_PICTURE)
	      {
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		  {
		    for (j=0; j< onx; j++)
		      {
			pd[i].ch[j] = (ps[i].rgb[j].r + ps[i].rgb[j].g + ps[i].rgb[j].b)/3;
		      }
		  }
	      }
	    else if (data_type == IS_RGBA_PICTURE)
	      {
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		  {
		    for (j=0; j< onx; j++)
		      {
			pd[i].ch[j] = (ps[i].rgba[j].r + ps[i].rgba[j].g + ps[i].rgba[j].b)/3;
		      }
		  }
	      }
	    else if (data_type == IS_RGB16_PICTURE)
	      {
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		  {
		    for (j=0; j< onx; j++)
		      {
			pd[i].in[j] = (ps[i].rgb16[j].r + ps[i].rgb16[j].g + ps[i].rgb16[j].b)/3;
		      }
		  }
	      }
	    else if (data_type == IS_RGBA_PICTURE)
	      {
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		  {
		    for (j=0; j< onx; j++)
		      {
			pd[i].in[j] = (ps[i].rgba16[j].r + ps[i].rgba16[j].g + ps[i].rgba16[j].b)/3;
		      }
		  }
	      }
	  }
	inherit_from_im_to_im(oid,ois);
	uns_oi_2_oi(oid,ois);
	oid->im.win_flag = ois->im.win_flag;
	if (ois->title != NULL)	set_im_title(oid, "%s convert to bw",ois->title);
	set_formated_string(&oid->im.treatement,"Image %s convert to bw", ois->filename);
	return oid;
}


O_i	*TstFreeImage_copy_and_shift_image_in_x(O_i *ois, int nx, int offset)
{
	register int i, j;
	O_i *oid;
	int onx, ony, data_type;
	union pix *ps, *pd;

	onx = nx;	ony = ois->im.ny;	data_type = ois->im.data_type;
	oid =  create_one_image(onx, ony, data_type);
	if (oid == NULL)
	{
		win_printf("Can't create dest image");
		return NULL;
	}
	if (data_type == IS_CHAR_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
			  if (j < offset) pd[i].ch[j] = ps[i].ch[0];
			  else if (j < offset + ois->im.nx) 
			    pd[i].ch[j] = ps[i].ch[j-offset];
			  else pd[i].ch[j] = ps[i].ch[ois->im.nx-1];
			}
		}
	}
	else if (data_type == IS_INT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
			  if (j < offset) pd[i].in[j] = ps[i].in[0];
			  else if (j < offset + ois->im.nx) 
			    pd[i].in[j] = ps[i].in[j-offset];
			  else pd[i].in[j] = ps[i].in[ois->im.nx-1];
			}
		}
	}
	else if (data_type == IS_UINT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
			  if (j < offset) pd[i].ui[j] = ps[i].ui[0];
			  else if (j < offset + ois->im.nx) 
			    pd[i].ui[j] = ps[i].ui[j-offset];
			  else pd[i].ui[j] = ps[i].ui[ois->im.nx-1];
			}
		}
	}
	else if (data_type == IS_LINT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
			  if (j < offset) pd[i].li[j] = ps[i].li[0];
			  else if (j < offset + ois->im.nx) 
			    pd[i].li[j] = ps[i].li[j-offset];
			  else pd[i].li[j] = ps[i].li[ois->im.nx-1];
			}
		}
	}
	else if (data_type == IS_FLOAT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
			  if (j < offset) pd[i].fl[j] = ps[i].fl[0];
			  else if (j < offset + ois->im.nx) 
			    pd[i].fl[j] = ps[i].fl[j-offset];
			  else pd[i].fl[j] = ps[i].fl[ois->im.nx-1];
			}
		}
	}
	else if (data_type == IS_DOUBLE_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
			  if (j < offset) pd[i].db[j] = ps[i].db[0];
			  else if (j < offset + ois->im.nx) 
			    pd[i].db[j] = ps[i].db[j-offset];
			  else pd[i].db[j] = ps[i].db[ois->im.nx-1];
			}
		}
	}
	else if (data_type == IS_COMPLEX_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
			  if (j < offset) 
			    {
			      pd[i].fl[2*j] = ps[i].fl[0];
			      pd[i].fl[2*j+1] = ps[i].fl[1];
			    }
			  else if (j < offset + ois->im.nx) 
			    {
			      pd[i].fl[2*j] = ps[i].fl[2*(j-offset)];
			      pd[i].fl[2*j+1] = ps[i].fl[2*(j-offset)+1];
			    }
			  else 
			    {
			      pd[i].fl[2*j] = ps[i].fl[2*(ois->im.nx-1)];
			      pd[i].fl[2*j+1] = ps[i].fl[2*(ois->im.nx-1)+1];
			    }
			}
		}
	}
	else if (data_type == IS_COMPLEX_DOUBLE_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
			  if (j < offset) 
			    {
			      pd[i].db[2*j] = ps[i].db[0];
			      pd[i].db[2*j+1] = ps[i].db[1];
			    }
			  else if (j < offset + ois->im.nx) 
			    {
			      pd[i].db[2*j] = ps[i].db[2*(j-offset)];
			      pd[i].db[2*j+1] = ps[i].db[2*(j-offset)+1];
			    }
			  else 
			    {
			      pd[i].db[2*j] = ps[i].db[2*(ois->im.nx-1)];
			      pd[i].db[2*j+1] = ps[i].db[2*(ois->im.nx-1)+1];
			    }
			}
		}
	}
	else if (data_type == IS_RGB_PICTURE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
			  if (j < offset)      pd[i].rgb[j] = ps[i].rgb[0];
			  else if (j < offset + ois->im.nx) 
			    pd[i].rgb[j] = ps[i].rgb[j-offset];
			  else  pd[i].rgb[j] = ps[i].rgb[ois->im.nx-1]; 			  
			}
		}
	}
	else if (data_type == IS_RGBA_PICTURE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< 4*onx; j++)
			{
			  if (j < offset)      pd[i].rgba[j] = ps[i].rgba[0];
			  else if (j < offset + ois->im.nx) 
			    pd[i].rgba[j] = ps[i].rgba[j-offset];
			  else  pd[i].rgba[j] = ps[i].rgba[ois->im.nx-1];     				}
		}
	}
	inherit_from_im_to_im(oid,ois);
	uns_oi_2_oi(oid,ois);
	oid->im.win_flag = ois->im.win_flag;
	if (ois->title != NULL)	set_im_title(oid, "Image %s extended to %d and shifted by %d",
					     ois->title,nx,offset);
	set_formated_string(&oid->im.treatement,
		"Image %s extended to %d and shifted by %d",ois->filename,nx,offset);
	return oid;
}



int do_TstFreeImage_rescale_image(void)
{
  int i, filter;
  char question[1024] = {0};
	O_i *ois = NULL, *oid = NULL;
	static int onx = -1, ony = -1, n_filter = 0;
	imreg *imr = NULL;

	if(updating_menu_state != 0)	return D_O_K;

	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine multiply the image intensity"
		"by a user defined scalar factor");
	}
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	{
		win_printf("Cannot find image");
		return D_O_K;
	}
	snprintf(question,sizeof(question),"Original image size %d x %d\n"
		 "Define image new size %%6d x %%6d\n"
		 "Choose filtering type : \n%%R->Box, pulse, Fourier window, 1st order (constant) B-Spline\n"
		 "%%r->Bilinear filter\n"
		 "%%r->4th order (cubic) B-Spline\n"
		 "%%r->Mitchell and Netravali's two-param cubic filter\n"
		 "%%r->Catmull-Rom spline, Overhauser spline\n"
		 "%%r->Lanczos-windowed sinc filter\n",ois->im.nx,ois->im.ny);
	i = win_scanf(question,&onx,&ony,&n_filter);
	if (i == WIN_CANCEL) return 0;

	if (n_filter == 0)      filter = FILTER_BOX;
	else if (n_filter == 1) filter = FILTER_BILINEAR;
	else if (n_filter == 2) filter = FILTER_BSPLINE;
	else if (n_filter == 3) filter = FILTER_BICUBIC;
	else if (n_filter == 4) filter = FILTER_CATMULLROM;
	else if (n_filter == 5) filter = FILTER_LANCZOS3;
	else       filter = FILTER_BOX;

	oid = TstFreeImage_image_rescale(ois, onx, ony, filter);
	if (oid == NULL)	return win_printf_OK("unable to create image!");
	add_image(imr, oid->type, (void*)oid);
	find_zmin_zmax(oid);
	return (refresh_image(imr, imr->n_oi - 1));
}




int do_TstFreeImage_color_image_to_bw(void)
{
	O_i *ois, *oid;
	imreg *imr;

	if(updating_menu_state != 0)	return D_O_K;

	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine multiply the image intensity"
		"by a user defined scalar factor");
	}
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	{
		win_printf("Cannot find image");
		return D_O_K;
	}
	oid = TstFreeImage_color_image_to_bw(ois);
	if (oid == NULL)	return win_printf_OK("unable to create image!");
	add_image(imr, oid->type, (void*)oid);
	find_zmin_zmax(oid);
	return (refresh_image(imr, imr->n_oi - 1));
}


int do_TstFreeImage_image_shifted_and_expended(void)
{
	O_i *ois, *oid;
	imreg *imr;
	int i;
	static int nx = 512, offset = 10;


	if(updating_menu_state != 0)	return D_O_K;

	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine multiply the image intensity"
		"by a user defined scalar factor");
	}
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	{
		win_printf("Cannot find image");
		return D_O_K;
	}
	i = win_scanf("define the new x size %6d\nand the offset %6d",&nx,&offset);
	if (i == WIN_CANCEL)	return D_O_K;
	oid = TstFreeImage_copy_and_shift_image_in_x(ois, nx, offset);
	//oid = TstFreeImage_image_multiply_by_a_scalar(ois,factor);
	if (oid == NULL)	return win_printf_OK("unable to create image!");
	add_image(imr, oid->type, (void*)oid);
	find_zmin_zmax(oid);
	return (refresh_image(imr, imr->n_oi - 1));
}
FIBITMAP *convert_oi_char_image(O_i *ois, int keep_selected_BW_limit)
{
  FIBITMAP *dib =  NULL;
  union pix *ps;
  int y, x;
  BYTE *bits = NULL;
  int zwi, zbi;
  int zg, zgi, zi;

    
  if (ois == NULL) return NULL;
  if (ois->im.data_type != IS_CHAR_IMAGE)  return NULL;
  dib = FreeImage_Allocate(ois->im.nx, ois->im.ny, 8, FI_RGBA_RED_MASK, 
			   FI_RGBA_GREEN_MASK, FI_RGBA_BLUE_MASK);
  if (dib == NULL) return NULL;


  zg = 255;
  if ((ois->z_max < ois->z_white && ois->z_max < ois->z_black)
      || (ois->z_min > ois->z_white && ois->z_min >ois->z_black))
    {
      ois->z_max = (ois->z_white > ois->z_black) ? ois->z_white : ois->z_black;
      ois->z_min = (ois->z_white > ois->z_black) ? ois->z_black : ois->z_white;
    }
  
  if ((ois->z_white - ois->z_black) != 0)    zg /= (ois->z_white - ois->z_black);
  zwi = (ois->dz != 0) ? (int)((ois->z_white - ois->az)/ois->dz) :
    (int)(ois->z_white - ois->az);
  zbi = (ois->dz != 0) ? (int)((ois->z_black - ois->az)/ois->dz) :
    (int)(ois->z_black - ois->az);
  zgi = 65536 * 255;
  if ( (zwi - zbi) != 0)     zgi /= (zwi - zbi);



  for(ps = ois->im.pixel, y = 0; y < ois->im.ny; y++) 
    {
      if (keep_selected_BW_limit)
	{
	  for(x = 0, bits = FreeImage_GetScanLine(dib, y); x < ois->im.nx; x++)
	    {
	      zi = (ps[y].ch[x] - zbi)* zgi;
	      zi = (zi < 0 ) ? 0 : (zi >> 16);
	      bits[x] = (zi > 255) ? 255 :(unsigned char)zi;
	    }
	}
      else
	{
	  for(x = 0, bits = FreeImage_GetScanLine(dib, y); x < ois->im.nx; x++) 
	    bits[x] = ps[y].ch[x];
	}
    }
  return dib;
}
FIBITMAP *convert_oi_float_image(O_i *ois)
{
  FIBITMAP *dib =  NULL;
  union pix *ps;
  int y, x;
  float *bits = NULL;

  if (ois == NULL) return NULL;
  if (ois->im.data_type != IS_FLOAT_IMAGE)  return NULL;
  dib = FreeImage_AllocateT(FIT_FLOAT, ois->im.nx, ois->im.ny, 32, 0, 0, 0);
  if (dib == NULL) return NULL;
  for(ps = ois->im.pixel, y = 0; y < ois->im.ny; y++) 
    {
      for(x = 0, bits = (float*)FreeImage_GetScanLine(dib, y); x < ois->im.nx; x++) 
	bits[x] = ps[y].fl[x]; 
    }
  return dib;
}



FIBITMAP *convert_oi_double_complex_image(O_i *ois)
{
  FIBITMAP *dib =  NULL;
  union pix *ps;
  int y, x;
  FICOMPLEX *bits = NULL;

  if (ois == NULL) return NULL;
  if (ois->im.data_type != IS_COMPLEX_DOUBLE_IMAGE)  return NULL;
  dib = FreeImage_AllocateT(FIT_COMPLEX, ois->im.nx, ois->im.ny, 128, 0, 0, 0);
  if (dib == NULL) return NULL;
  for(ps = ois->im.pixel, y = 0; y < ois->im.ny; y++) 
    {
      for(x = 0, bits = (FICOMPLEX*)FreeImage_GetScanLine(dib, y); x < ois->im.nx; x++)
	{
	  bits[x].r = ps[y].dcp[x].dre;
	  bits[x].i = ps[y].dcp[x].dim;	  
	}
    }
  return dib;
}

FIBITMAP *convert_oi_double_image(O_i *ois)
{
  FIBITMAP *dib =  NULL;
  union pix *ps;
  int y, x;
  double *bits = NULL;

  if (ois == NULL) return NULL;
  if (ois->im.data_type != IS_DOUBLE_IMAGE)  return NULL;
  dib = FreeImage_AllocateT(FIT_DOUBLE, ois->im.nx, ois->im.ny, 64, 0, 0, 0);
  if (dib == NULL) return NULL;
  for(ps = ois->im.pixel, y = 0; y < ois->im.ny; y++) 
    {
      for(x = 0, bits = (double*)FreeImage_GetScanLine(dib, y); x < ois->im.nx; x++)
	bits[x] = ps[y].db[x];
    }
  return dib;
}

FIBITMAP *convert_oi_uint16_image(O_i *ois)
{
  FIBITMAP *dib =  NULL;
  union pix *ps;
  int y, x;
  unsigned short int *bits = NULL;

  if (ois == NULL) return NULL;
  if (ois->im.data_type != IS_UINT_IMAGE)  return NULL;
  dib = FreeImage_AllocateT(FIT_UINT16, ois->im.nx, ois->im.ny, 16, 0, 0, 0);
  if (dib == NULL) return NULL;
  for(ps = ois->im.pixel, y = 0; y < ois->im.ny; y++) 
    {
      for(x = 0, bits = (unsigned short int*)FreeImage_GetScanLine(dib, y); x < ois->im.nx; x++)
	bits[x] = ps[y].ui[x];
    }
  return dib;
}

FIBITMAP *convert_oi_int16_image(O_i *ois)
{
  FIBITMAP *dib =  NULL;
  union pix *ps;
  int y, x;
  short int *bits = NULL;

  if (ois == NULL) return NULL;
  if (ois->im.data_type != IS_INT_IMAGE)  return NULL;
  dib = FreeImage_AllocateT(FIT_INT16, ois->im.nx, ois->im.ny, 16, 0, 0, 0);
  if (dib == NULL) return NULL;
  for(ps = ois->im.pixel, y = 0; y < ois->im.ny; y++) 
    {
      for(x = 0, bits = (short int*)FreeImage_GetScanLine(dib, y); x < ois->im.nx; x++)
	bits[x] = ps[y].in[x];
    }
  return dib;
}


FIBITMAP *convert_oi_int32_image(O_i *ois)
{
  FIBITMAP *dib =  NULL;
  union pix *ps;
  int y, x;
  int *bits = NULL;

  if (ois == NULL) return NULL;
  if (ois->im.data_type != IS_LINT_IMAGE)  return NULL;
  dib = FreeImage_AllocateT(FIT_INT32, ois->im.nx, ois->im.ny, 32, 0, 0, 0);
  if (dib == NULL) return NULL;
  for(ps = ois->im.pixel, y = 0; y < ois->im.ny; y++) 
    {
      for(x = 0, bits = (int *)FreeImage_GetScanLine(dib, y); x < ois->im.nx; x++)
	bits[x] = ps[y].li[x];
    }
  return dib;
}



FIBITMAP *convert_oi_rgb_image(O_i *ois)
{
  FIBITMAP *dib =  NULL;
  union pix *ps;
  int y, x;
  BYTE *bits = NULL;

  if (ois == NULL) return NULL;
  if (ois->im.data_type != IS_RGB_PICTURE)  return NULL;
  dib = FreeImage_Allocate(ois->im.nx, ois->im.ny, 24, FI_RGBA_RED_MASK, 
			   FI_RGBA_GREEN_MASK, FI_RGBA_BLUE_MASK);
  if (dib == NULL) return NULL;
  for(ps = ois->im.pixel, y = 0; y < ois->im.ny; y++) 
    {
      for(x = 0, bits = FreeImage_GetScanLine(dib, y); x < ois->im.nx; x++) 
	{
	  bits[FI_RGBA_RED] = ps[y].rgb[x].r;
	  bits[FI_RGBA_GREEN] = ps[y].rgb[x].g; 
	  bits[FI_RGBA_BLUE] = ps[y].rgb[x].b; 
	  bits += 3;
	}
    }
  return dib;
}

FIBITMAP *convert_oi_rgba_image(O_i *ois)
{
  FIBITMAP *dib =  NULL;
  union pix *ps;
  int y, x;
  BYTE *bits = NULL;

  if (ois == NULL) return NULL;
  if (ois->im.data_type != IS_RGBA_PICTURE)  return NULL;
  dib = FreeImage_Allocate(ois->im.nx, ois->im.ny, 32, FI_RGBA_RED_MASK, 
			   FI_RGBA_GREEN_MASK, FI_RGBA_BLUE_MASK);
  if (dib == NULL) return NULL;
  for(ps = ois->im.pixel, y = 0; y < ois->im.ny; y++) 
    {
      for(x = 0, bits = FreeImage_GetScanLine(dib, y); x < ois->im.nx; x++) 
	{
	  bits[FI_RGBA_RED] = ps[y].rgb[x].r;
	  bits[FI_RGBA_GREEN] = ps[y].rgb[x].g; 
	  bits[FI_RGBA_BLUE] = ps[y].rgb[x].b; 
	  bits[FI_RGBA_ALPHA] = ps[y].rgba[x].a; 
	  bits += 4;
	}
    }
  return dib;
}


FIBITMAP *convert_oi_rgb16_image(O_i *ois)
{
  FIBITMAP *dib =  NULL;
  union pix *ps;
  int y, x;
  FIRGB16 *bits = NULL;

  if (ois == NULL) return NULL;
  if (ois->im.data_type != IS_RGB16_PICTURE)  return NULL;

  
  dib = FreeImage_AllocateT(FIT_RGB16, ois->im.nx, ois->im.ny, 48, FI_RGBA_RED_MASK, 
			   FI_RGBA_GREEN_MASK, FI_RGBA_BLUE_MASK);
  if (dib == NULL) return NULL;
  for(ps = ois->im.pixel, y = 0; y < ois->im.ny; y++) 
    {
      for(x = 0, bits = (FIRGB16 *)FreeImage_GetScanLine(dib, y); x < ois->im.nx; x++) 
	{
	  bits[x].red = ps[y].rgb16[x].r;
	  bits[x].green = ps[y].rgb16[x].g; 
	  bits[x].blue = ps[y].rgb16[x].b; 
	}
    }
  return dib;
}
FIBITMAP *convert_oi_rgba16_image(O_i *ois)
{
  FIBITMAP *dib =  NULL;
  union pix *ps;
  int y, x;
  FIRGBA16 *bits = NULL;

  if (ois == NULL) return NULL;
  if (ois->im.data_type != IS_RGBA16_PICTURE)  return NULL;

  
  dib = FreeImage_AllocateT(FIT_RGBA16, ois->im.nx, ois->im.ny, 64, FI_RGBA_RED_MASK, 
			   FI_RGBA_GREEN_MASK, FI_RGBA_BLUE_MASK);
  if (dib == NULL) return NULL;
  for(ps = ois->im.pixel, y = 0; y < ois->im.ny; y++) 
    {
      for(x = 0, bits = (FIRGBA16 *)FreeImage_GetScanLine(dib, y); x < ois->im.nx; x++) 
	{
	  bits[x].red = ps[y].rgb16[x].r;
	  bits[x].green = ps[y].rgb16[x].g; 
	  bits[x].blue = ps[y].rgb16[x].b; 
	  bits[x].alpha = ps[y].rgba16[x].a; 
	}
    }
  return dib;
}


int save_freeimage(void)
{
  char seqfilename[2048];
  O_i *ois = NULL;
  imreg *imr = NULL; 
  FIBITMAP *dib = NULL;
  static int ty = 6, co = 0, it = 0, keep_selected_BW_limit = 1;
  int i, flag;
  char *type[] = {"bmp","exr","j2k","jp2","jpg", "jxr", "png","pbm,pgm,ppm", "tiff", "targa", "webp"};

  if(updating_menu_state != 0)	return D_O_K;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2) return D_O_K;

  if (ois->im.data_type != IS_CHAR_IMAGE && ois->im.data_type != IS_RGB_PICTURE
      && ois->im.data_type != IS_RGBA_PICTURE)
    return win_printf_OK("Cannot save this type of image!");

  i = win_scanf("In what format to you want to save this image?\n"
		"%RBMP %rEXR %rJ2K %rJP2 %rJPEG %rJXR %rPNG\n"
		"%rPBM,PGM,PPM %rTIFF %rTARGA %rWEBP"
		"For black and white 8-bits limits\n%R->raw image data\n"
		"%r->or use selected image limit",&ty,&keep_selected_BW_limit);

 
  if (ois->im.data_type == IS_CHAR_IMAGE)          dib = convert_oi_char_image(ois,keep_selected_BW_limit);
  else if (ois->im.data_type == IS_RGB_PICTURE)    dib = convert_oi_rgb_image(ois);
  else if (ois->im.data_type == IS_RGBA_PICTURE)   dib = convert_oi_rgba_image(ois);
  else dib = NULL;
  if (dib == NULL)     return win_printf_OK("Cannot convert image to dib!");

  build_full_file_name(seqfilename,sizeof(seqfilename), ois->dir, ois->filename);
  replace_extension(seqfilename, seqfilename, type[ty], sizeof(seqfilename));
  if (do_select_file(seqfilename, sizeof(seqfilename), "Save Image-FILE", type[ty], "Save File image", 0))
    return win_printf_OK("Cannot select ouput file");

  if (ty == 0)     // FIF_BMP  
    {
      if (FreeImage_Save(FIF_BMP, dib, seqfilename, 0) == 0) 
	{
	  win_printf_OK("Cannot select save file:\n%s",backslash_to_slash(seqfilename));
	}
    }
  else if (ty == 1)  // FIF_EXR 
    {
    }
  else if (ty == 2)  // FIF_J2K 
    {
      if (FreeImage_Save(FIF_J2K, dib, seqfilename, J2K_DEFAULT) == 0) 
	{
	  win_printf_OK("Cannot select save file:\n%s",backslash_to_slash(seqfilename));
	}
    }
  else if (ty == 3)  // FIF_JP2 
    {
      if (FreeImage_Save(FIF_JP2, dib, seqfilename, JP2_DEFAULT) == 0) 
	{
	  win_printf_OK("Cannot select save file:\n%s",backslash_to_slash(seqfilename));
	}
    }
  else if (ty == 4)  // FIF_JPEG 
    {
      if (FreeImage_Save(FIF_JPEG, dib, seqfilename, JPEG_DEFAULT) == 0) 
	{
	  win_printf_OK("Cannot select save file:\n%s",backslash_to_slash(seqfilename));
	}
    }
  else if (ty == 5)  // FIF_JXR 
    {
      if (FreeImage_Save(FIF_JXR, dib, seqfilename, JXR_DEFAULT) == 0) 
	{
	  win_printf_OK("Cannot select save file:\n%s",backslash_to_slash(seqfilename));
	}
    }
  else if (ty == 6)  // FIF_PNG 
    {
      i = win_scanf("PNG saving options:"
		    "%R Save with ZLib level 6 compression (default)\n"
		    "%r ave using ZLib level 1 compression (fastest)\n"
		    "%r Save using ZLib level 9 compression\n"
		    "%r Save without ZLib compression\n"
		    "%b Save using Adam7 interlacing\n",&co,&it);
      if (i == WIN_CANCEL) return 0;
      if (co == 0) flag = PNG_DEFAULT;
      else if (co == 1) flag = PNG_Z_BEST_SPEED;
      else if (co == 2) flag = PNG_Z_BEST_COMPRESSION;
      else if (co == 3) flag = PNG_Z_NO_COMPRESSION;
      else return win_printf_OK("Wrong PNG option\n");
      if (it) flag |= PNG_INTERLACED; 
      if (FreeImage_Save(FIF_PNG, dib, seqfilename, flag) == 0) 
	{
	  win_printf_OK("Cannot select save file:\n%s",backslash_to_slash(seqfilename));
	}

    }
  else if (ty == 7)  // FIF_PBM,FIF_PGM,FIF_PPM 
    {
    }
  else if (ty == 8)  // FIF_TIFF 
    {
      if (FreeImage_Save(FIF_TIFF, dib, seqfilename, TIFF_DEFAULT) == 0) 
	{
	  win_printf_OK("Cannot select save file:\n%s",backslash_to_slash(seqfilename));
	}
    }
  else if (ty == 9)  // FIF_TARGA 
    {
      if (FreeImage_Save(FIF_TARGA, dib, seqfilename, TARGA_DEFAULT) == 0) 
	{
	  win_printf_OK("Cannot select save file:\n%s",backslash_to_slash(seqfilename));
	}
    }
  else if (ty == 10)  // FIF_WEBP
    {
      if (FreeImage_Save(FIF_WEBP, dib, seqfilename, WEBP_DEFAULT) == 0) 
	{
	  win_printf_OK("Cannot select save file:\n%s",backslash_to_slash(seqfilename));
	}
    }
  if (dib != NULL) FreeImage_Unload(dib);  

  //FIF_BMP  FIF_EXR FIF_J2K FIF_JP2 FIF_JPEG FIF_JXR FIF_PNG FIF_PBM,FIF_PGM,FIF_PPM FIF_TIFF FIF_TARGA FIF_WEBP

  return 0;
}


int save_freeimage_float(void)
{
  char seqfilename[2048];
  O_i *ois = NULL;
  imreg *imr = NULL; 
  FIBITMAP *dib = NULL;
  //static int ty = 6, co = 0, it = 0;
  //int i; //, flag;
  //char *type[] = {"bmp","exr","j2k","jp2","jpg", "jxr", "png","pbm,pgm,ppm", "tiff", "targa", "webp"};

  if(updating_menu_state != 0)	return D_O_K;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2) return D_O_K;

  if (ois->im.data_type != IS_FLOAT_IMAGE &&
      ois->im.data_type != IS_COMPLEX_DOUBLE_IMAGE &&
      ois->im.data_type != IS_DOUBLE_IMAGE)
    
    return win_printf_OK("Cannot save this type of image!");

  if (ois->im.data_type == IS_FLOAT_IMAGE)
    dib = convert_oi_float_image(ois);
  else if(ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
    dib = convert_oi_double_complex_image(ois);
  else if(ois->im.data_type == IS_DOUBLE_IMAGE)
    dib = convert_oi_double_image(ois);
 

  if (dib == NULL)     return win_printf_OK("Cannot convert image to dib!");

  build_full_file_name(seqfilename,sizeof(seqfilename), ois->dir, ois->filename);
  replace_extension(seqfilename, seqfilename, "tiff", sizeof(seqfilename));
  if (do_select_file(seqfilename, sizeof(seqfilename), "Save Image-FILE", "tiff", "Save File image", 0))
    return win_printf_OK("Cannot select ouput file");
  if (FreeImage_Save(FIF_TIFF, dib, seqfilename, TIFF_NONE) == 0) 
    {
      win_printf_OK("Cannot select save file:\n%s",backslash_to_slash(seqfilename));
    }
  if (dib != NULL) FreeImage_Unload(dib);  

  //FIF_BMP  FIF_EXR FIF_J2K FIF_JP2 FIF_JPEG FIF_JXR FIF_PNG FIF_PBM,FIF_PGM,FIF_PPM FIF_TIFF FIF_TARGA FIF_WEBP

  return 0;
}



int save_freeimage_16_or_32_PNG_or_TIFF(void)
{
  int i;
  char seqfilename[2048];
  O_i *ois = NULL;
  imreg *imr = NULL; 
  FIBITMAP *dib = NULL;
  int forced_tiff = 0, png_or_tiff = 0;
  //static int ty = 6, co = 0, it = 0;
  //int i; //, flag;
  //char *type[] = {"bmp","exr","j2k","jp2","jpg", "jxr", "png","pbm,pgm,ppm", "tiff", "targa", "webp"};

  if(updating_menu_state != 0)	return D_O_K;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2) return D_O_K;

  if (ois->im.data_type != IS_INT_IMAGE &&
      ois->im.data_type != IS_UINT_IMAGE &&
      ois->im.data_type != IS_RGB16_PICTURE &&
      ois->im.data_type != IS_RGBA16_PICTURE &&
      ois->im.data_type != IS_LINT_IMAGE)
    
    return win_printf_OK("Cannot save this type of image!");

  forced_tiff = (ois->im.data_type == IS_INT_IMAGE || ois->im.data_type == IS_LINT_IMAGE) ? 1 : 0;
  if (forced_tiff == 0)
    {
      i = win_scanf("Save image in:\n %RPNG\n %rTIFF\n",&png_or_tiff);
      if (i == WIN_CANCEL) return 0;
    }
  else png_or_tiff = 1;

  if (ois->im.data_type == IS_INT_IMAGE)
    dib = convert_oi_int16_image(ois);
  else if(ois->im.data_type == IS_UINT_IMAGE)
    dib = convert_oi_uint16_image(ois);
  else if(ois->im.data_type == IS_LINT_IMAGE)
    dib = convert_oi_int32_image(ois);
  else if(ois->im.data_type == IS_RGB16_PICTURE)
    dib = convert_oi_rgb16_image(ois);
  else if(ois->im.data_type == IS_RGBA16_PICTURE)
    dib = convert_oi_rgba16_image(ois);    
 
  if (dib == NULL)     return win_printf_OK("Cannot convert image to dib!");

  if (png_or_tiff == 1)
    {
      build_full_file_name(seqfilename,sizeof(seqfilename), ois->dir, ois->filename);
      replace_extension(seqfilename, seqfilename, "tiff", sizeof(seqfilename));
      if (do_select_file(seqfilename, sizeof(seqfilename), "Save Image-FILE", "tiff", "Save File image", 0))
	return win_printf_OK("Cannot select ouput file");
      if (FreeImage_Save(FIF_TIFF, dib, seqfilename, TIFF_NONE) == 0) 
	{
	  win_printf_OK("Cannot select save file:\n%s",backslash_to_slash(seqfilename));
	}
    }
  else
    {
      build_full_file_name(seqfilename,sizeof(seqfilename), ois->dir, ois->filename);
      replace_extension(seqfilename, seqfilename, "png", sizeof(seqfilename));
      if (do_select_file(seqfilename, sizeof(seqfilename), "Save Image-FILE", "png", "Save File image", 0))
	return win_printf_OK("Cannot select ouput file");
      if (FreeImage_Save(FIF_PNG, dib, seqfilename, PNG_Z_BEST_COMPRESSION) == 0) 
	{
	  win_printf_OK("Cannot select save file:\n%s",backslash_to_slash(seqfilename));
	}
    }
  if (dib != NULL) FreeImage_Unload(dib);  
  return 0;
}


int save_freeimage_double_complex(void)
{
  char seqfilename[2048];
  O_i *ois = NULL;
  imreg *imr = NULL; 
  FIBITMAP *dib = NULL;
  //static int ty = 6, co = 0, it = 0;
  //int i; //, flag;
  //char *type[] = {"bmp","exr","j2k","jp2","jpg", "jxr", "png","pbm,pgm,ppm", "tiff", "targa", "webp"};

  if(updating_menu_state != 0)	return D_O_K;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2) return D_O_K;

  if (ois->im.data_type != IS_COMPLEX_DOUBLE_IMAGE)
    return win_printf_OK("Cannot save this type of image!");

 
  dib = convert_oi_double_complex_image(ois);
  if (dib == NULL)     return win_printf_OK("Cannot convert image to dib!");

  build_full_file_name(seqfilename,sizeof(seqfilename), ois->dir, ois->filename);
  replace_extension(seqfilename, seqfilename, "tiff", sizeof(seqfilename));
  if (do_select_file(seqfilename, sizeof(seqfilename), "Save Image-FILE", "tiff", "Save File image", 0))
    return win_printf_OK("Cannot select ouput file");
  if (FreeImage_Save(FIF_TIFF, dib, seqfilename, TIFF_NONE) == 0) 
    {
      win_printf_OK("Cannot select save file:\n%s",backslash_to_slash(seqfilename));
    }
  if (dib != NULL) FreeImage_Unload(dib);  

  //FIF_BMP  FIF_EXR FIF_J2K FIF_JP2 FIF_JPEG FIF_JXR FIF_PNG FIF_PBM,FIF_PGM,FIF_PPM FIF_TIFF FIF_TARGA FIF_WEBP

  return 0;
}



int load_freeimage(void)
{
  char seqfilename[2048], *type = NULL, path[2048], *ctype = NULL;
  FREE_IMAGE_FORMAT fif = FIF_UNKNOWN;
  FREE_IMAGE_COLOR_TYPE color_t = 0;
  FIBITMAP *dib =  NULL;
  unsigned int y, x, nx, ny, nd, ty, bytespp;
  BYTE *bits = NULL;
  O_i *oid = NULL;
  imreg *imr = NULL; 
  union pix *pd;
  unsigned short *ui = NULL;
  short *in = NULL;
  int *li = NULL;
  float *fl = NULL;
  double *db = NULL;
  FICOMPLEX *dbc = NULL;
  FIRGB16 *rgb16 = NULL;
  FIRGBA16 *rgba16 = NULL;
  
  if(updating_menu_state != 0)	return D_O_K;

  if (ac_grep(cur_ac_reg,"%im",&imr) != 1) return D_O_K;

  seqfilename[0] = 0;
  
  if (do_select_file(seqfilename, sizeof(seqfilename), "Image-FILE", "*.*", "File image", 1))
    return win_printf_OK("Cannot select input file");
  
  
  fif = FreeImage_GetFileType(seqfilename, 0);
  if(fif == FIF_UNKNOWN)  // no signature ? 
    { // try to guess the file format from the file extension
      fif = FreeImage_GetFIFFromFilename(seqfilename);
      win_printf("File : %s\nFile format not supported !\n",backslash_to_slash(seqfilename));
    }
  // check that the plugin has reading capabilities ...
  if((fif != FIF_UNKNOWN) && FreeImage_FIFSupportsReading(fif)) 
    { // ok, let's load the file
      dib = FreeImage_Load(fif, seqfilename, 0);
      if (dib != NULL)
	{
	  nx = FreeImage_GetWidth(dib);
	  ny = FreeImage_GetHeight(dib);
	  nd = FreeImage_GetBPP(dib);
	  ty = FreeImage_GetImageType(dib);
	  color_t = FreeImage_GetColorType(dib);
	  if (ty == FIT_BITMAP) type = "Standard image: 1-, 4-, 8-, 16-, 24-, 32-bit";
	  else if (ty == FIT_UINT16) type = "Array of unsigned short: unsigned 16-bit";
	  else if (ty == FIT_INT16) type = "Array of short: signed 16-bit";
	  else if (ty == FIT_UINT32) type = "Array of unsigned long: unsigned 32-bit";
	  else if (ty == FIT_INT32) type = "Array of long: signed 32-bit";
	  else if (ty == FIT_FLOAT) type = "Array of float: 32-bit IEEE floating point";
	  else if (ty == FIT_DOUBLE) type = "Array of double: 64-bit IEEE floating point";
	  else if (ty == FIT_COMPLEX) type = "Array of FICOMPLEX: 2 x 64-bit IEEE floating point";
	  else if (ty == FIT_RGB16) type = "48-bit RGB image: 3 x unsigned 16-bit";
	  else if (ty == FIT_RGBA16) type = "64-bit RGBA image: 4 x unsigned 16-bit";
	  else if (ty == FIT_RGBF) type = "96-bit RGB float image: 3 x 32-bit IEEE floating point";
	  else if (ty == FIT_RGBAF) type = "128-bit RGBA float image: 4 x 32-bit IEEE floating point";

	  if (color_t == FIC_MINISBLACK) 
	    ctype = "Monochrome bitmap (1-bit) : first palette entry is black.\n"
	      "Palletised bitmap (4 or 8-bit) and single channel non standard bitmap:\n"
	      "the bitmap has a greyscale palette";

	  else if (color_t == FIC_MINISWHITE) 
	    ctype = "Monochrome bitmap (1-bit) : first palette entry is white.\n"
	      "Palletised bitmap (4 or 8-bit) : the bitmap has an inverted greyscale palette\n";
	  else if (color_t == FIC_PALETTE) 
	    ctype = "Palettized bitmap (1, 4 or 8 bit)\n";
	  else if (color_t == FIC_RGB) 
	    ctype = "High-color bitmap (16, 24 or 32 bit), RGB16 or RGBF\n";
	  else if (color_t == FIC_RGBALPHA) 
	    ctype = "High-color bitmap with an alpha channel (32 bit bitmap, RGBA16 or RGBAF)\n";
	  else if (color_t == FIC_CMYK) ctype = "CMYK bitmap (32 bit only";
	  else ctype = "Unknown\n";
	  bytespp = FreeImage_GetLine(dib) / nx;
	  
	  win_printf("File : %s\nFile loaded fine!\n"
		     "Image %dx%d depth %d\n%s\nColor type :\n%s"
		     ,backslash_to_slash(seqfilename),nx,ny,nd,type,ctype);
	  if (ty == FIT_BITMAP)
	    {
	      if (nd == 8)
		{
		  oid = create_and_attach_oi_to_imr(imr, nx, ny, IS_CHAR_IMAGE);
		  if (oid == NULL)    win_printf_OK("Cannot create image!\n");
		  for(pd = oid->im.pixel, y = 0; y < ny; y++) 
		    {
		      for(x = 0, bits = FreeImage_GetScanLine(dib, y); x < nx; x++) 
			pd[y].ch[x] = bits[x];
		    }
		}
	      else if (nd == 24)
		{
		  // Calculate the number of bytes per pixel (3 for 24-bit or 4 for 32-bit)
		  oid = create_and_attach_oi_to_imr(imr, nx, ny, IS_RGB_PICTURE);
		  if (oid == NULL)    win_printf_OK("Cannot create image!\n");
		  for(pd = oid->im.pixel, y = 0; y < ny; y++) 
		    {
		      for(x = 0, bits = FreeImage_GetScanLine(dib, y); x < nx; x++) 
			{
			  pd[y].rgb[x].r = bits[FI_RGBA_RED];
			  pd[y].rgb[x].g = bits[FI_RGBA_GREEN];
			  pd[y].rgb[x].b = bits[FI_RGBA_BLUE];
			  bits += bytespp; 
			}
		    }
		}
	      else if (nd == 32)
		{
		  // Calculate the number of bytes per pixel (3 for 24-bit or 4 for 32-bit)
		  oid = create_and_attach_oi_to_imr(imr, nx, ny, IS_RGBA_PICTURE);
		  if (oid == NULL)    win_printf_OK("Cannot create image!\n");
		  for(pd = oid->im.pixel, y = 0; y < ny; y++) 
		    {
		      for(x = 0, bits = FreeImage_GetScanLine(dib, y); x < nx; x++) 
			{
			  pd[y].rgba[x].r = bits[FI_RGBA_RED];
			  pd[y].rgba[x].g = bits[FI_RGBA_GREEN];
			  pd[y].rgba[x].b = bits[FI_RGBA_BLUE];
			  pd[y].rgba[x].a = bits[FI_RGBA_ALPHA];
			  bits += bytespp; 
			}
		    }
		}
	    }
	  if (ty == FIT_UINT16)
	    {
	      oid = create_and_attach_oi_to_imr(imr, nx, ny, IS_UINT_IMAGE);
	      if (oid == NULL)    win_printf_OK("Cannot create image!\n");
	      for(pd = oid->im.pixel, y = 0; y < ny; y++) 
		{
		  for(x = 0, ui = (unsigned short*)FreeImage_GetScanLine(dib, y); x < nx; x++) 
		    pd[y].ui[x] = ui[x];
		}
	    }
	  if (ty == FIT_INT16)
	    {
	      oid = create_and_attach_oi_to_imr(imr, nx, ny, IS_INT_IMAGE);
	      if (oid == NULL)    win_printf_OK("Cannot create image!\n");
	      for(pd = oid->im.pixel, y = 0; y < ny; y++) 
		{
		  for(x = 0, in = (short int*)FreeImage_GetScanLine(dib, y); x < nx; x++) 
		    pd[y].in[x] = in[x];
		}
	    }
	  if (ty == FIT_INT32)
	    {
	      oid = create_and_attach_oi_to_imr(imr, nx, ny, IS_LINT_IMAGE);
	      if (oid == NULL)    win_printf_OK("Cannot create image!\n");
	      for(pd = oid->im.pixel, y = 0; y < ny; y++) 
		{
		  for(x = 0, li = (int*)FreeImage_GetScanLine(dib, y); x < nx; x++) 
		    pd[y].li[x] = li[x];
		}
	    }
	  if (ty == FIT_FLOAT)
	    {
	      oid = create_and_attach_oi_to_imr(imr, nx, ny, IS_FLOAT_IMAGE);
	      if (oid == NULL)    win_printf_OK("Cannot create image!\n");
	      for(pd = oid->im.pixel, y = 0; y < ny; y++) 
		{
		  for(x = 0, fl = (float*)FreeImage_GetScanLine(dib, y); x < nx; x++) 
		    pd[y].fl[x] = fl[x];
		}
	    }
	  if (ty == FIT_DOUBLE)
	    {
	      oid = create_and_attach_oi_to_imr(imr, nx, ny, IS_DOUBLE_IMAGE);
	      if (oid == NULL)    win_printf_OK("Cannot create image!\n");
	      for(pd = oid->im.pixel, y = 0; y < ny; y++) 
		{
		  for(x = 0, db = (double*)FreeImage_GetScanLine(dib, y); x < nx; x++) 
		    pd[y].db[x] = db[x];
		}
	    }
	  if (ty == FIT_COMPLEX)
	    {
	      oid = create_and_attach_oi_to_imr(imr, nx, ny, IS_COMPLEX_IMAGE);
	      if (oid == NULL)    win_printf_OK("Cannot create image!\n");
	      for(pd = oid->im.pixel, y = 0; y < ny; y++) 
		{
		  for(x = 0, dbc = (FICOMPLEX *)FreeImage_GetScanLine(dib, y); x < nx; x++) 
		    {
		      pd[y].cp[x].re = dbc[x].r;
		      pd[y].cp[x].im = dbc[x].i;
		    }
		}
	    }
	  if (ty == FIT_RGB16)
	    {
	      oid = create_and_attach_oi_to_imr(imr, nx, ny, IS_RGB16_PICTURE);
	      if (oid == NULL)    win_printf_OK("Cannot create image!\n");
	      for(pd = oid->im.pixel, y = 0; y < ny; y++) 
		{
		  for(x = 0, rgb16 = (FIRGB16 *)FreeImage_GetScanLine(dib, y); x < nx; x++) 
		    {
		      pd[y].rgb16[x].r = rgb16[x].red;
		      pd[y].rgb16[x].g = rgb16[x].green;
		      pd[y].rgb16[x].b = rgb16[x].blue;
		    }
		}
	    }
	  if (ty == FIT_RGBA16)
	    {
	      oid = create_and_attach_oi_to_imr(imr, nx, ny, IS_RGBA16_PICTURE);
	      if (oid == NULL)    win_printf_OK("Cannot create image!\n");
	      for(pd = oid->im.pixel, y = 0; y < ny; y++) 
		{
		  for(x = 0, rgba16 = (FIRGBA16 *)FreeImage_GetScanLine(dib, y); x < nx; x++) 
		    {
		      pd[y].rgba16[x].r = rgba16[x].red;
		      pd[y].rgba16[x].g = rgba16[x].green;
		      pd[y].rgba16[x].b = rgba16[x].blue;
		      pd[y].rgba16[x].a = rgba16[x].alpha;
		    }
		}
	    }
	}
      if (oid != NULL) 
	{
	  set_oi_source(oid, "loaded from %s", backslash_to_slash(seqfilename));
	  if (extract_file_path(path, sizeof(path), seqfilename) != NULL)
	    set_oi_path(oid, path);
	  if (extract_file_name(path, sizeof(path), seqfilename) != NULL)
	    {
	      replace_extension(path, path, "gr", sizeof(path));
	      set_oi_filename(oid, path);
	    }
	  find_zmin_zmax(oid);
	  smart_map_pixel_ratio_of_image_and_screen(oid);
	  if (dib != NULL) FreeImage_Unload(dib);
	}
    }
  return (refresh_image(imr, imr->n_oi - 1));
}
int load_freeimage_stack(void)
{
  char seqfilename[2048], *type = NULL, path[2048], ext[64] = {0}, base[64] = {0}, file[256] = {0};
  char namef[512] = {0}, form[64] = {0};
  char question[1024] = {0};
  int i, j, k, nzero, start = 0, inc = 1, last = 1, nf = 1;
  FREE_IMAGE_FORMAT fif = FIF_UNKNOWN;
  FIBITMAP *dib =  NULL;
  float ratiox = 4, ratioy = 4;
  unsigned int y, x, nx, ny, nd, ty, bytespp;
  BYTE *bits = NULL;
  O_i *oid = NULL;
  imreg *imr = NULL; 
  union pix *pd;
  unsigned short *ui = NULL;
  short *in = NULL;
  int *li = NULL;
  float *fl = NULL;
  double *db = NULL;
  FICOMPLEX *dbc = NULL;
  FIRGB16 *rgb16 = NULL;
  FIRGBA16 *rgba16 = NULL;
  
  if(updating_menu_state != 0)	return D_O_K;

  if (ac_grep(cur_ac_reg,"%im",&imr) != 1) return D_O_K;

  seqfilename[0] = 0;
  
  if (do_select_file(seqfilename, sizeof(seqfilename), "First Image-FILE", "*.*", "First File image", 1))
    return win_printf_OK("Cannot select input file");

  fif = FreeImage_GetFileType(seqfilename, 0);
  if(fif == FIF_UNKNOWN || !FreeImage_FIFSupportsReading(fif))  // no signature ? 
    { // try to guess the file format from the file extension
      fif = FreeImage_GetFIFFromFilename(seqfilename);
      win_printf("File : %s\nFile format not supported !\n",backslash_to_slash(seqfilename));
    }
  if (extract_file_path(path, sizeof(path), seqfilename) == NULL)  
    {
      win_printf("File : Could not etract path!");
      return 0;
    }
  if (extract_file_name(file, sizeof(file), seqfilename) == NULL)  
    {
      win_printf("File : Could not etract filename!");
      return 0;
    }
  for (i = strlen(file) - 1; i >= 0 && file[i] != '.'; i--);
  if (file[i] != '.')
    {
      win_printf("File : Could not etract extension!");
      return 0;
    }
  for (k = 0, j = i + 1; file[j]; ext[k++] = file[j++]);
  ext[j] = 0;
  file[i] = 0;
  //win_printf("i = %d file %s",i,file);
  for (j = i - 1; j >= 0 && isdigit(file[j]); j--);
  //win_printf("file %s j %d",file,j);
  if (sscanf(file+j+1,"%d",&start) != 1)
    return win_printf_OK("Cannot find starting index in %s",file+j+1);
  nzero = i - 1 - j;
  for (k = 0; k <= j; k++)
    base[k] = file[k];
  base[k] = 0;
  sprintf(form,"%%0%dd",nzero); 
  snprintf(question,sizeof(question),"I will load %s files with  name as: %sXXX.%s \n"
	   "starting at %d\n"
	   "Specify the increment %%3d\n""The last number %%6d (included)\n"
	   "and format %%10s\n",ext,base,ext,start);
  i = win_scanf(question,&inc,&last,form);
  if (i == WIN_CANCEL) return D_O_K;

  nf = 1 + abs(last - start)/abs(inc); 

  sprintf(namef,"%s%s.%s",base,form,ext);
  for (i = start, k = 0; i <= last; i += inc, k++)
    {
      snprintf(file,sizeof(file),namef,i);
      if (build_full_file_name(seqfilename, sizeof(seqfilename), path, file) == NULL)
	{
	  win_printf("Could not create filename from %s and %s!",backslash_to_slash(path),file);
	  continue;
	}
      // check that the plugin has reading capabilities ...
      dib = FreeImage_Load(fif, seqfilename, 0);
      if (dib != NULL)
	{
	  nx = FreeImage_GetWidth(dib);
	  ny = FreeImage_GetHeight(dib);
	  nd = FreeImage_GetBPP(dib);
	  ty = FreeImage_GetImageType(dib);
	  if (ty == FIT_BITMAP) type = "Standard image: 1-, 4-, 8-, 16-, 24-, 32-bit";
	  else if (ty == FIT_UINT16) type = "Array of unsigned short: unsigned 16-bit";
	  else if (ty == FIT_INT16) type = "Array of short: signed 16-bit";
	  else if (ty == FIT_UINT32) type = "Array of unsigned long: unsigned 32-bit";
	  else if (ty == FIT_INT32) type = "Array of long: signed 32-bit";
	  else if (ty == FIT_FLOAT) type = "Array of float: 32-bit IEEE floating point";
	  else if (ty == FIT_DOUBLE) type = "Array of double: 64-bit IEEE floating point";
	  else if (ty == FIT_COMPLEX) type = "Array of FICOMPLEX: 2 x 64-bit IEEE floating point";
	  else if (ty == FIT_RGB16) type = "48-bit RGB image: 3 x unsigned 16-bit";
	  else if (ty == FIT_RGBA16) type = "64-bit RGBA image: 4 x unsigned 16-bit";
	  else if (ty == FIT_RGBF) type = "96-bit RGB float image: 3 x 32-bit IEEE floating point";
	  else if (ty == FIT_RGBAF) type = "128-bit RGBA float image: 4 x 32-bit IEEE floating point";

	  bytespp = FreeImage_GetLine(dib) / nx;
	  
	  //win_printf("File : %s\nFile loaded fine!\n"
	  //         "Image %dx%d depth %d\n%s\n"
	  // 	     ,backslash_to_slash(seqfilename),nx,ny,nd,type);
	  if (ty == FIT_BITMAP)
	    {
	      if (nd == 8)
		{
		  if (oid == NULL)    oid = create_and_attach_movie_to_imr(imr, nx, ny, IS_CHAR_IMAGE, nf);
		  if (oid == NULL)    return win_printf_OK("Cannot create image!\n");
		  for(switch_frame(oid,k), pd = oid->im.pixel, y = 0; y < ny; y++) 
		    {
		      for(x = 0, bits = FreeImage_GetScanLine(dib, y); x < nx; x++) 
			pd[y].ch[x] = bits[x];
		    }
		}
	      else if (nd == 24)
		{
		  // Calculate the number of bytes per pixel (3 for 24-bit or 4 for 32-bit)
		  if (oid == NULL)    oid = create_and_attach_movie_to_imr(imr, nx, ny, IS_RGB_PICTURE, nf);
		  if (oid == NULL)    win_printf_OK("Cannot create image!\n");
		  for(switch_frame(oid,k), pd = oid->im.pixel, y = 0; y < ny; y++) 
		    {
		      for(x = 0, bits = FreeImage_GetScanLine(dib, y); x < nx; x++) 
			{
			  pd[y].rgb[x].r = bits[FI_RGBA_RED];
			  pd[y].rgb[x].g = bits[FI_RGBA_GREEN];
			  pd[y].rgb[x].b = bits[FI_RGBA_BLUE];
			  bits += bytespp; 
			}
		    }
		}
	      else if (nd == 32)
		{
		  // Calculate the number of bytes per pixel (3 for 24-bit or 4 for 32-bit)
		  if (oid == NULL)    oid = create_and_attach_movie_to_imr(imr, nx, ny, IS_RGBA_PICTURE, nf);
		  if (oid == NULL)    win_printf_OK("Cannot create image!\n");
		  for(switch_frame(oid,k), pd = oid->im.pixel, y = 0; y < ny; y++) 
		    {
		      for(x = 0, bits = FreeImage_GetScanLine(dib, y); x < nx; x++) 
			{
			  pd[y].rgba[x].r = bits[FI_RGBA_RED];
			  pd[y].rgba[x].g = bits[FI_RGBA_GREEN];
			  pd[y].rgba[x].b = bits[FI_RGBA_BLUE];
			  pd[y].rgba[x].a = bits[FI_RGBA_ALPHA];
			  bits += bytespp; 
			}
		    }
		}
	    }
	  if (ty == FIT_UINT16)
	    {
	      if (oid == NULL)    oid = create_and_attach_movie_to_imr(imr, nx, ny, IS_UINT_IMAGE, nf);
	      if (oid == NULL)    win_printf_OK("Cannot create image!\n");
	      for(switch_frame(oid,k), pd = oid->im.pixel, y = 0; y < ny; y++) 
		{
		  for(x = 0, ui = (unsigned short*)FreeImage_GetScanLine(dib, y); x < nx; x++) 
		    pd[y].ui[x] = ui[x];
		}
	    }
	  if (ty == FIT_INT16)
	    {
	      if (oid == NULL)    oid = create_and_attach_movie_to_imr(imr, nx, ny, IS_INT_IMAGE, nf);
	      if (oid == NULL)    win_printf_OK("Cannot create image!\n");
	      for(switch_frame(oid,k), pd = oid->im.pixel, y = 0; y < ny; y++) 
		{
		  for(x = 0, in = (short int*)FreeImage_GetScanLine(dib, y); x < nx; x++) 
		    pd[y].in[x] = in[x];
		}
	    }
	  if (ty == FIT_INT32)
	    {
	      if (oid == NULL)    oid = create_and_attach_movie_to_imr(imr, nx, ny, IS_LINT_IMAGE, nf);
	      if (oid == NULL)    win_printf_OK("Cannot create image!\n");
	      for(switch_frame(oid,k), pd = oid->im.pixel, y = 0; y < ny; y++) 
		{
		  for(x = 0, li = (int*)FreeImage_GetScanLine(dib, y); x < nx; x++) 
		    pd[y].li[x] = li[x];
		}
	    }
	  if (ty == FIT_FLOAT)
	    {
	      if (oid == NULL)    oid = create_and_attach_movie_to_imr(imr, nx, ny, IS_FLOAT_IMAGE, nf);
	      if (oid == NULL)    win_printf_OK("Cannot create image!\n");
	      for(switch_frame(oid,k), pd = oid->im.pixel, y = 0; y < ny; y++) 
		{
		  for(x = 0, fl = (float*)FreeImage_GetScanLine(dib, y); x < nx; x++) 
		    pd[y].fl[x] = fl[x];
		}
	    }
	  if (ty == FIT_DOUBLE)
	    {
	      if (oid == NULL)    oid = create_and_attach_movie_to_imr(imr, nx, ny, IS_DOUBLE_IMAGE, nf);
	      if (oid == NULL)    win_printf_OK("Cannot create image!\n");
	      for(switch_frame(oid,k), pd = oid->im.pixel, y = 0; y < ny; y++) 
		{
		  for(x = 0, db = (double*)FreeImage_GetScanLine(dib, y); x < nx; x++) 
		    pd[y].db[x] = db[x];
		}
	    }
	  if (ty == FIT_COMPLEX)
	    {
	      if (oid == NULL)    oid = create_and_attach_movie_to_imr(imr, nx, ny, IS_COMPLEX_IMAGE, nf);
	      if (oid == NULL)    win_printf_OK("Cannot create image!\n");
	      for(switch_frame(oid,k), pd = oid->im.pixel, y = 0; y < ny; y++) 
		{
		  for(x = 0, dbc = (FICOMPLEX *)FreeImage_GetScanLine(dib, y); x < nx; x++) 
		    {
		      pd[y].cp[x].re = dbc[x].r;
		      pd[y].cp[x].im = dbc[x].i;
		    }
		}
	    }
	  if (ty == FIT_RGB16)
	    {
	      if (oid == NULL)    oid = create_and_attach_movie_to_imr(imr, nx, ny, IS_RGB16_PICTURE, nf);
	      if (oid == NULL)    win_printf_OK("Cannot create image!\n");
	      for(switch_frame(oid,k), pd = oid->im.pixel, y = 0; y < ny; y++) 
		{
		  for(x = 0, rgb16 = (FIRGB16 *)FreeImage_GetScanLine(dib, y); x < nx; x++) 
		    {
		      pd[y].rgb16[x].r = rgb16[x].red;
		      pd[y].rgb16[x].g = rgb16[x].green;
		      pd[y].rgb16[x].b = rgb16[x].blue;
		    }
		}
	    }
	  if (ty == FIT_RGBA16)
	    {
	      if (oid == NULL)    oid = create_and_attach_movie_to_imr(imr, nx, ny, IS_RGBA16_PICTURE, nf);
	      if (oid == NULL)    win_printf_OK("Cannot create image!\n");
	      for(switch_frame(oid,k), pd = oid->im.pixel, y = 0; y < ny; y++) 
		{
		  for(x = 0, rgba16 = (FIRGBA16 *)FreeImage_GetScanLine(dib, y); x < nx; x++) 
		    {
		      pd[y].rgba16[x].r = rgba16[x].red;
		      pd[y].rgba16[x].g = rgba16[x].green;
		      pd[y].rgba16[x].b = rgba16[x].blue;
		      pd[y].rgba16[x].a = rgba16[x].alpha;
		    }
		}
	    }
	  if (dib != NULL) FreeImage_Unload(dib);
	  dib = NULL;
	}
    }
  if (oid != NULL) 
    {
      sprintf(namef,"%s%s-%s.%s",base,form,form,ext);
      snprintf(file,sizeof(file),namef,start,last);
      if (build_full_file_name(seqfilename, sizeof(seqfilename), path, file) == NULL)
	{
	  win_printf("Could not create filename from %s and %s!",backslash_to_slash(path),file);
	}
      set_oi_source(oid, "loaded from %s", backslash_to_slash(seqfilename));
      if (extract_file_path(path, sizeof(path), seqfilename) != NULL)
	set_oi_path(oid, path);
      if (extract_file_name(path, sizeof(path), seqfilename) != NULL)
	{
	  replace_extension(path, path, "gr", sizeof(path));
	  set_oi_filename(oid, path);
	}
      find_zmin_zmax(oid);
      for (ratiox = 4; (int)(ratiox * nx) > SCREEN_W; ratiox /= 2); 
      for (ratioy = 4; (int)(ratioy * ny) > SCREEN_H; ratioy /= 2);
      ratiox = (ratiox < ratioy) ? ratiox : ratioy;
      map_pixel_ratio_of_image_and_screen(oid, ratiox, ratiox);
    }
  return (refresh_image(imr, imr->n_oi - 1));
}

/*
case FIT_UINT32:
for(y = 0; y < FreeImage_GetHeight(image); y++) {
DWORD *bits = (DWORD *)FreeImage_GetScanLine(image, y);
for(x = 0; x < FreeImage_GetWidth(image); x++) {
bits[x] = 128;
}
break;
case FIT_RGBF:
for(y = 0; y < FreeImage_GetHeight(image); y++) {
FIRGBF *bits = (FIRGBF *)FreeImage_GetScanLine(image, y);
for(x = 0; x < FreeImage_GetWidth(image); x++) {
bits[x].red = 128;
bits[x].green = 128;
bits[x].blue = 128;
}
}
break;

break;
case FIT_RGBAF:
for(y = 0; y < FreeImage_GetHeight(image); y++) {
FIRGBAF *bits = (FIRGBAF *)FreeImage_GetScanLine(image, y);
for(x = 0; x < FreeImage_GetWidth(image); x++) {
bits[x].red = 128;
bits[x].green = 128;
bits[x].blue = 128;
bits[x].alpha = 128;
}
}
break;
*/


/** Generic image loader
@param lpszPathName Pointer to the full file name
@param flag Optional load flag constant
@return Returns the loaded dib if successful, returns NULL otherwise
*/
FIBITMAP* GenericLoader(const char* lpszPathName, int flag) {
FREE_IMAGE_FORMAT fif = FIF_UNKNOWN;
// check the file signature and deduce its format
// (the second argument is currently not used by FreeImage)
fif = FreeImage_GetFileType(lpszPathName, 0);
if(fif == FIF_UNKNOWN) {
// no signature ?
// try to guess the file format from the file extension
fif = FreeImage_GetFIFFromFilename(lpszPathName);
}
// check that the plugin has reading capabilities ...
if((fif != FIF_UNKNOWN) && FreeImage_FIFSupportsReading(fif)) {
// ok, let's load the file
FIBITMAP *dib = FreeImage_Load(fif, lpszPathName, flag);
// unless a bad file format, we are done !
return dib;
}
return NULL;
}

MENU *TstFreeImage_image_menu(void)
{
  static MENU mn[32];
  
  if (mn[0].text != NULL)	return mn;

  add_item_to_menu(mn,"Load image", load_freeimage,NULL,0,NULL);
  add_item_to_menu(mn,"Load image stack", load_freeimage_stack,NULL,0,NULL);
  add_item_to_menu(mn,"Save image", save_freeimage,NULL,0,NULL);
  add_item_to_menu(mn,"Save float/complex image in TIFF", save_freeimage_float,NULL,0,NULL);
  add_item_to_menu(mn,"Save 16 or 32 bits image in PNG or TIFF", save_freeimage_16_or_32_PNG_or_TIFF,NULL,0,NULL);  


  add_item_to_menu(mn,"average profile", do_TstFreeImage_average_along_y,NULL,0,NULL);
  add_item_to_menu(mn,"Convert to BW",do_TstFreeImage_color_image_to_bw,NULL,0,NULL);
  add_item_to_menu(mn,"Rescale image",do_TstFreeImage_rescale_image,NULL,0,NULL);
  add_item_to_menu(mn,"resize in X and offset", do_TstFreeImage_image_shifted_and_expended,NULL,0,NULL);

  return mn;
}

int	TstFreeImage_main(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  char *ver = NULL;
  FreeImage_SetOutputMessage(FreeImageErrorHandler);
  // print version & copyright infos
  ver = FreeImage_GetVersion();
  //win_printf("TstFreeImage %s\n",ver);
  snprintf(menuname,sizeof(menuname), "FreeImage %s",ver);
  add_image_treat_menu_item (menuname , NULL, TstFreeImage_image_menu(), 0, NULL);


  add_item_to_menu(image_file_import_menu,"Load using FreeImage", load_freeimage, NULL, 0, NULL);
  add_item_to_menu(image_file_import_menu,"Load stack using FreeImage", load_freeimage_stack, NULL, 0, NULL);
  add_item_to_menu(image_file_export_menu,"Save using FreeImage", save_freeimage, NULL, 0, NULL);

  
  return D_O_K;
}

int	TstFreeImage_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  remove_item_to_menu(image_treat_menu, menuname, NULL, NULL);
  remove_item_to_menu(image_file_import_menu,"Load using FreeImage", NULL, NULL);
  remove_item_to_menu(image_file_import_menu,"Load stack using FreeImage", NULL, NULL);
  remove_item_to_menu(image_file_export_menu,"Save using FreeImage", NULL, NULL);
  
  return D_O_K;
}
#endif

