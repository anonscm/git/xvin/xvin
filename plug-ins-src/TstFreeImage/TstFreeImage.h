#ifndef _TSTFREEIMAGE_H_
#define _TSTFREEIMAGE_H_

PXV_FUNC(int, do_TstfreeImage_average_along_y, (void));
PXV_FUNC(MENU*, TstfreeImage_image_menu, (void));
PXV_FUNC(int, TstfreeImage_main, (int argc, char **argv));
#endif

