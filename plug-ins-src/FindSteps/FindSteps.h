#pragma once

#include "../plug-ins-src/stat/stat.h"

PXV_FUNC(int, do_FindSteps_rescale_plot, (void));
PXV_FUNC(MENU*, FindSteps_plot_menu, (void));
PXV_FUNC(int, do_FindSteps_rescale_data_set, (void));
PXV_FUNC(int, FindSteps_main, (int argc, char **argv));
PXV_FUNC(int,FindSteps_median_filter,(d_s *dsi,d_s *dsr,int window));
