/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _FINDSTEPS_C_
#define _FINDSTEPS_C_

#include <allegro.h>
# include "xvin.h"

/* If you include other regular header do it here*/
#include  <gsl/gsl_multifit.h>

/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "FindSteps.h"





int compute_p_value_fit_vs_length(d_s *dsi, int np, d_s *pvalues, int degree, int in, float sigma)
{
  if (in < 0 || in >= dsi->nx ) return 1;
  int stop = in + np;
  int temp = 0;
  if (stop < 0) stop = 0;
  if (stop > dsi->nx-1) stop = dsi->nx - 1;
  np = stop -in ;
  double *a = NULL;
  double er = 0;
  double pval = 0;
  int nx = 0;
  pvalues->nx = pvalues->ny = 0;

  if (np > 0)
  {
  for (int j = 2;j<np;j++)
  {
    nx = fit_ds_to_xn_polynome(dsi, degree + 1, dsi->xd[in], dsi->xd[in+j], -FLT_MAX, FLT_MAX, &a);
    if (nx != j) return(win_printf_OK("problem \n"));
    er = chi2_in_fit_ds_to_xn_polynome(dsi, degree + 1, dsi->xd[in], dsi->xd[in+j], -FLT_MAX, FLT_MAX, a,&nx);
    if (nx != j) return(win_printf_OK("problem j = %d nx = %d \n",j,nx));

    pval = p_value_from_chi2(er/(sigma*sigma),nx);
    printf("nx = %d, er = %f, pval = %f\n",nx,er,pval);
    add_new_point_to_ds(pvalues,dsi->xd[in+j],pval);
  }
  }
  else if(np < 0)
  {
      for (int j = 2;j<-np;j++)
  {
    nx = fit_ds_to_xn_polynome(dsi, degree + 1, dsi->xd[in-j], dsi->xd[in], -FLT_MAX, FLT_MAX, &a);
    if (nx != j) return(win_printf_OK("problem \n"));
    er = chi2_in_fit_ds_to_xn_polynome(dsi, degree + 1, dsi->xd[in-j], dsi->xd[in], -FLT_MAX, FLT_MAX, a,&nx);
    if (nx != j) return(win_printf_OK("problem j = %d nx = %d \n",j,nx));

    pval = p_value_from_chi2(er/(sigma*sigma),nx);
    add_new_point_to_ds(pvalues,dsi->xd[in-j],pval);

  }
  }
  free(a);

  return 0;
}



int  FindSteps_median_filter(d_s *dsi,d_s *dsr,int window)
{
  if (dsi == NULL || dsi->nx == 0) return NULL;
  d_s *dssort = NULL;
  int w2 = window / 2;
  dssort = build_data_set(window,window);
  dsr = build_adjust_data_set(dsr,dsi->nx,dsi->nx);
  dsr->nx = dsr-> ny = 0;

  int start = 0;
  int stop = 0;
  for (int i = 0; i<dsi->nx;i++)
  {
    dssort->nx = 0;
    dssort->ny = 0;

    start = (i -w2 > 0) ? i - w2 : 0;
    stop = (i + w2 < dsi->nx) ? i + w2 :  dsi->nx - 1;
    for (int j = 0;j<stop-start + 1;j++)
    {
      add_new_point_to_ds(dssort,j,dsi->yd[start+j]);
    }
    sort_ds_along_y(dssort);
    add_new_point_to_ds(dsr,dsi->xd[i],dssort->yd[dssort->ny/2]);

  }
  free_data_set(dssort);
  return 0;
}

int FindSteps_mean_filter(d_s *dsi, d_s *dsr, int window) // my version of FIR
{
  if (dsi == NULL || dsi->nx == 0) return NULL;
  int w2 = window / 2;
  dsr = build_adjust_data_set(dsr,dsi->nx,dsi->nx);
  dsr->nx = dsr-> ny = 0;
  float mean;
  int start = 0;
  int stop = 0;

  for (int i = 0; i<dsi->nx;i++)
  {
    mean = 0;
    start = (i -w2 > 0) ? i - w2 : 0;
    stop = (i + w2 < dsi->nx) ? i + w2 :  dsi->nx - 1;
    for (int j = 0;j<stop-start + 1;j++)
    {
      mean += dsi->yd[start+j];
    }
    mean /= (stop-start + 1);
    add_new_point_to_ds(dsr,dsi->xd[i],mean);

  }
  return 0;
}


int FindSteps_remove_glitch(d_s *dsi,d_s *dsr,int window, float tolerance)
{
  if (dsi == NULL || dsi->nx == 0) return 1;
  FindSteps_median_filter(dsi, dsr,window);
  d_s *dsret = NULL;

  for (int i = 0; i<dsi->nx;i++)
  {
    if (fabs(dsr->yd[i] - dsi->yd[i]) < tolerance) // on ne garde la médiane que si c'est un glich
    {
        dsr->yd[i] = dsi->yd[i];
    }
  }
  return 0;
}

int find_y_maxima_in_ds(d_s *dsi, d_s *dsr,float threshold, float rel_threshold, d_s *ds2, float ds2threshold, d_s *ds2r)
{
  float max = -FLT_MAX;
  int maxpos = 0;

  dsr->nx = dsr-> ny = 0;
  for (int i = 0;i<dsi->nx;i++)
  {
    if (dsi->yd[i] > max && dsi->yd[i] > threshold)
    {
      max = dsi->yd[i];
      maxpos = i;
    }
    if (dsi->yd[i] < rel_threshold * max)
    {

      if (ds2r)
      {
      if (fabs(ds2->yd[maxpos]) > ds2threshold)
      {
        add_new_point_to_ds(dsr,dsi->xd[maxpos],max);
        add_new_point_to_ds(ds2r,dsi->xd[maxpos],ds2->yd[maxpos]);
      }
    }
    else add_new_point_to_ds(dsr,dsr->xd[maxpos],max);

      max = -FLT_MAX;
    }
  }
  return 0;
}
int fit_jump_chi_square_ratio(d_s *dsi, d_s *dsres, d_s*dsrjumpsizes, int window, int start, int end, int degree, int difference, int multiplybyjumpsize, float threshold, float score_threshold , int all_score)
{
  int i ,j, n;
 double xi, yi, ei, chisq,chisqnojump;
 gsl_matrix *X, *cov,*Xnojump,*covnojump;
 gsl_vector *y,  *c, *cnojump;
 int w2 = window/2;
 d_s *dsr = NULL;
 d_s *dsjumpsizes = NULL;

 if (start < w2 ) start = w2;
 if (end >= dsi->nx-w2) end = dsi->nx-1-w2;
 if (start >= end ) return NULL;

 n = end-start + 1;
 dsr = build_data_set(n,n);
 dsr->nx = dsr->ny = 0;
 dsres->nx = dsres->ny = 0;
 dsjumpsizes = build_data_set(16,16);
 dsjumpsizes -> nx = dsjumpsizes -> ny = 0;


if(dsrjumpsizes)
{
   dsrjumpsizes->nx = dsrjumpsizes->ny = 0;
 }


 X = gsl_matrix_alloc (2*w2+1, 2 + degree);
 Xnojump = gsl_matrix_alloc (2*w2+1, 1 + degree);

 y = gsl_vector_alloc (2*w2+1);

 c = gsl_vector_alloc (2 + degree);
 cnojump = gsl_vector_alloc (1 + degree);
 cov = gsl_matrix_alloc (2 + degree, 2 + degree);
 covnojump = gsl_matrix_alloc (1 + degree,1+degree);

 gsl_multifit_linear_workspace *work = gsl_multifit_linear_alloc (2*w2+1, 2+degree);
 gsl_multifit_linear_workspace *worknojump = gsl_multifit_linear_alloc (2*w2+1, 1+degree);


 float meanscore = 0;
 for (i = start; i <= end; i++)
   {

     for (j = i-w2;j<=i+w2;j++)
     {

       gsl_matrix_set(Xnojump,j-i+w2,0,1.0);
       if (j<=i)
       {
         gsl_matrix_set (X, j-i+w2, 0, 1.0);
         gsl_matrix_set (X, j-i+w2, 1, 0.0);
     }

     else
     {
      gsl_matrix_set (X, j-i+w2, 1, 1.0);
      gsl_matrix_set (X, j-i+w2, 0, 0.0);
      }

      for (int k = 2;k<2+degree;k++)
      {
     gsl_matrix_set (X, j-i+w2, k, pow(dsi->xd[j],k-1));
     gsl_matrix_set (Xnojump ,j-i+w2, k-1, pow(dsi->xd[j],k-1));
      }
     gsl_vector_set (y, j-i+w2, dsi->yd[j]);
   }

   gsl_multifit_linear (X,  y, c, cov,  &chisq, work);
   gsl_multifit_linear (Xnojump,  y, cnojump, covnojump, &chisqnojump, worknojump);


   if (difference) add_new_point_to_ds(dsr,dsi->xd[i],chisqnojump-chisq);
   else  add_new_point_to_ds(dsr,dsi->xd[i],chisq/chisqnojump);
   if (multiplybyjumpsize) dsr->yd[dsr->nx-1] *= fabs(gsl_vector_get(c,1) - gsl_vector_get(c,0));
   if (dsjumpsizes) add_new_point_to_ds(dsjumpsizes,dsi->xd[i],gsl_vector_get(c,1) - gsl_vector_get(c,0));

   meanscore += dsr->yd[dsr->ny-1];
 }
 meanscore /= n;
 if (all_score)
 {
   for (int jj = 0;jj<dsr->nx;jj++)
   {
     add_new_point_to_ds(dsres,dsr->xd[jj],dsr->yd[jj]);
   }
   if(dsrjumpsizes)
   {
   for (int jj = 0;jj<dsjumpsizes->nx;jj++)
   {
     add_new_point_to_ds(dsres,dsrjumpsizes->xd[jj],dsrjumpsizes->yd[jj]);
   }
 }
 }
 else find_y_maxima_in_ds(dsr,dsres,score_threshold,0.1,dsjumpsizes,threshold,dsrjumpsizes);
 gsl_multifit_linear_free(work);
 gsl_multifit_linear_free(worknojump);

 gsl_matrix_free (X);
 gsl_matrix_free (Xnojump);

 gsl_vector_free (y);
 gsl_vector_free (c);
 gsl_vector_free (cnojump);

 gsl_matrix_free (cov);
 gsl_matrix_free (covnojump);


 return 0;
}





int polynomefit_ds1_between_points_of_ds2(d_s *ds1, d_s *ds2, d_s *dsr,int degree)
{
  double *a;
  win_printf_OK("If not the case, you might need to sort both ds in growing x order\n");
  dsr = build_adjust_data_set(dsr,ds1->nx,ds1->ny);
  float yt = 0;
  int np = 0;
  float x1 = 0;
  float xend;
  int j = 0;
  for (int i = 0;i<ds2->nx;i++)
  {
    if (i == ds2->nx -1) xend = ds1->xd[ds1->nx-1];
    else xend = ds2->xd[i+1];
    fit_ds_to_xn_polynome(ds1, degree+1, ds2->xd[i]+1, xend, -FLT_MAX, FLT_MAX, &a);
    while (ds1->xd[j] < xend && (j<ds1->nx))
    {
      dsr->xd[j] = ds1->xd[j];
      yt = 0;

      for (int k = 0; k <= degree; k++)
      {
          for ( np = 0, x1 = a[k]; np < k; np++)
              x1 *= ds1->xd[j];
          yt += x1;
      }
      dsr->yd[j] = yt;
      dsr->nx++;
      dsr->ny++;
      j++;
    }
    free(a);
  }
  return 0;

}
int do_polyfit_between_points_of_ds2(void)
{
  char message[4096];
  int i = 0;
  d_s *ds;
  O_p *op;
  pltreg *pr;
  d_s *dsi = NULL;
  static int dsnumber = 0;
  static int degree = 0;
  if(updating_menu_state != 0)	return D_O_K;

  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  snprintf(message,sizeof(message),"This will polyfit with degree %%d the values of dataset %d between the points of dataset %%d\n",op->cur_dat);
  i = win_scanf(message,&degree,&dsnumber);
  if (dsnumber <0 || dsnumber >= op->n_dat) return(win_printf_OK("You chose a dataset that does not exist\n"));
  if (i == WIN_CANCEL)	return OFF;
  ds = create_and_attach_one_ds(op,dsi->nx,dsi->ny,0);
  polynomefit_ds1_between_points_of_ds2(dsi, op->dat[dsnumber],ds,degree );
  inherit_from_ds_to_ds(ds, dsi);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}




int fit_jump_least_squares(d_s *dsi, d_s *dsr, int window, int start, int end, int degree )
{
  int i ,j, n;
 double xi, yi, ei, chisq;
 gsl_matrix *X, *cov;
 gsl_vector *y, *c;
 int w2 = window/2;


 if (start < w2 ) start = w2;
 if (end >= dsi->nx-w2) end = dsi->nx-1-w2;
 if (start >= end ) return NULL;

 n = end-start + 1;
 dsr = build_adjust_data_set(dsr,n,n);
 dsr->nx = dsr->ny = 0;


 X = gsl_matrix_alloc (2*w2+1, 2 + degree);
 y = gsl_vector_alloc (2*w2+1);

 c = gsl_vector_alloc (2 + degree);
 cov = gsl_matrix_alloc (2 + degree, 2 + degree);
 gsl_multifit_linear_workspace *work = gsl_multifit_linear_alloc (2*w2+1, 2+degree);


 for (i = start; i <= end; i++)
   {

     for (j = i-w2;j<=i+w2;j++)
     {
       if (j<=i)
       {
         gsl_matrix_set (X, j-i+w2, 0, 1.0);
         gsl_matrix_set (X, j-i+w2, 1, 0.0);
     }

     else
     {
      gsl_matrix_set (X, j-i+w2, 1, 1.0);
      gsl_matrix_set (X, j-i+w2, 0, 0.0);
      }

      for (int k = 2;k<2+degree;k++)
      {
     gsl_matrix_set (X, j-i+w2, k, pow(dsi->xd[j],k-1));
      }

     gsl_vector_set (y, j-i+w2, dsi->yd[j]);
   }

   gsl_multifit_linear (X,  y, c, cov,          &chisq, work);

    add_new_point_to_ds(dsr,dsi->xd[i],gsl_vector_get(c,1)-gsl_vector_get(c,0));
 }

 gsl_multifit_linear_free(work);
 gsl_matrix_free (X);
 gsl_vector_free (y);
 gsl_vector_free (c);
 gsl_matrix_free (cov);

 return 0;
}

int do_FindSteps_fit_jump(void)
{
  int i;
  O_p *op = NULL;
  d_s *ds, *dsi;
  pltreg *pr = NULL;
  static int degree = 1;
  static int start = 0;
  static int end = 0;
  static int window = 64;


  if(updating_menu_state != 0)	return D_O_K;

  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  if (end == 0) end = dsi->nx;	/* this is the number of points in the data set */
  i = win_scanf("This will draw the jumpfit of degree %d between index start %d\n"
  "and index end %d with window %d\n",&degree,&start,&end,&window);
  if (i == WIN_CANCEL)	return OFF;
  ds = create_and_attach_one_ds(op,16,16,0);
  fit_jump_least_squares(dsi, ds, window, start, end, degree );
  inherit_from_ds_to_ds(ds, dsi);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}




int do_FindSteps_jump_chisqratio(void)
{
  int i;
  O_p *op = NULL;
  d_s *ds, *dsi, *dsjumpsizes, *dsp;
  pltreg *pr = NULL;
  static int degree = 1;
  static int start = 0;
  static int end = 0;
  static int window = 64;
  static int difference = 1;
  static int multiplybyjumpsize = 1;
  static float threshold = 0.5;
  static float score_threshold = 1;
  static int all_score = 0;
  static int dump_data_set = 0, dump_fitted_data = 1;


  if(updating_menu_state != 0)	return D_O_K;

  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  if (end == 0) end = dsi->nx;	/* this is the number of points in the data set */
  i = win_scanf("This will draw the jumpfit of degree %d between index start %d\n"
  "and index end %d with window %d\n"
  "Multiply by jump size %b\n"
  "Draw ratio %R or difference %r"
  "Only keep jumps above the threshold of %f nm\n"
  "Only keep jumps above the score threshold of %f X 10-6 \n"
  "Dump all scores %b \n"
  "Dump scores graphs %b\n"
  "Dump fitted data %b\n",&degree,&start,&end,&window,&multiplybyjumpsize,&difference,&threshold,&score_threshold,&all_score,&dump_data_set,&dump_fitted_data);
  if (i == WIN_CANCEL)	return OFF;
  score_threshold *= 1e-6;
  threshold *= 1e-3;

  if (dump_data_set == 0) dump_fitted_data = 1;

  if (dump_data_set)
  {
  ds = create_and_attach_one_ds(op,16,16,0);
  dsjumpsizes = create_and_attach_one_ds(op,16,16,0);
  set_ds_source(ds,"Chi2 test for jumps");
  set_ds_source(dsjumpsizes,"Jumpsizes from chi2 test");
}
else
{
  ds = build_data_set(16,16);
  dsjumpsizes = build_data_set(16,16);
}
  fit_jump_chi_square_ratio(dsi, ds, dsjumpsizes,window, start, end, degree,difference,multiplybyjumpsize,threshold,score_threshold,all_score );
  if (dump_fitted_data)
  {
  dsp = create_and_attach_one_ds(op,dsi->nx,dsi->ny,0);
  dsp->nx = dsp->ny = 0;
  polynomefit_ds1_between_points_of_ds2(dsi, dsjumpsizes,dsp,degree );
}
if (dump_data_set == 0)
{
  free_data_set(ds);
  free_data_set(dsjumpsizes);
}
  inherit_from_ds_to_ds(ds, dsi);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  threshold *= 1e3;
  score_threshold *= 1e6;
  return D_O_K;
}

int do_FindSteps_median_filter(void)
{
  int i;
  O_p *op = NULL;
  d_s *ds, *dsi;
  pltreg *pr = NULL;
  static int window = 64;


  if(updating_menu_state != 0)	return D_O_K;

  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  i = win_scanf("This will draw the median filter with window %d\n",&window);
  if (i == WIN_CANCEL)	return OFF;
  ds = create_and_attach_one_ds(op,16,16,0);
   FindSteps_median_filter(dsi,ds,window);
  inherit_from_ds_to_ds(ds, dsi);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}

int do_FindSteps_mean_filter(void)
{
  int i;
  O_p *op = NULL;
  d_s *ds, *dsi;
  pltreg *pr = NULL;
  static int window = 64;


  if(updating_menu_state != 0)	return D_O_K;

  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  i = win_scanf("This will draw the mean filter with window %d\n",&window);
  if (i == WIN_CANCEL)	return OFF;
  ds = create_and_attach_one_ds(op,16,16,0);
   FindSteps_mean_filter(dsi,ds,window);
  inherit_from_ds_to_ds(ds, dsi);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}

int do_FindSteps_glitch(void)
{
  int i;
  O_p *op = NULL;
  d_s *ds, *dsi;
  pltreg *pr = NULL;
  static int window = 64;
  static float tolerance = 0.003;


  if(updating_menu_state != 0)	return D_O_K;

  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  i = win_scanf("This will draw the glitch filter with window %d and tolerance %f\n",&window,&tolerance);
  if (i == WIN_CANCEL)	return OFF;
  ds = create_and_attach_one_ds(op,16,16,0);
   FindSteps_remove_glitch(dsi,ds,window,tolerance);
  inherit_from_ds_to_ds(ds, dsi);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}



int do_FindSteps_pvalue(void)
{
  int i;
  O_p *op = NULL, *opn = NULL;
  int nf;
  d_s *ds, *dsi;
  pltreg *pr = NULL;
  static int degree = 1;
  static int np = 32;
  static int in = 64;
  static float sigma_min = 0.0005;
  static float sigma_max = 0.002;
  static float sigma_range = 0.0001;


  if(updating_menu_state != 0)	return D_O_K;

  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  nf = dsi->nx;	/* this is the number of points in the data set */
  i = win_scanf("This will draw the p value for fits starting at index %d \n"
  "in selected dataset from length 2 to %d for polyfit of degrees %d and sigma min= %f sigma max = %f range = %f\n",&in,&np,
  &degree,&sigma_min,&sigma_max,&sigma_range);
  if (i == WIN_CANCEL)	return OFF;

int nsig = (sigma_max - sigma_min)/sigma_range;
if ((opn = create_and_attach_one_plot(pr, abs(np), abs(np), 0)) == NULL)
      return win_printf_OK("cannot create plot !");
    ds = opn->dat[0];

    ds->nx = ds-> ny = 0;
    compute_p_value_fit_vs_length(dsi, np, ds, degree, in,sigma_min);
    set_ds_source(ds,"Sigma = %f, P value as a function of fit length starting for point at %d\n",sigma_min,in);

  for (int j = 1;j<nsig - 1;j++)
  {
    ds = create_and_attach_one_ds(opn,16,16,0);
    ds->nx = ds-> ny = 0;
    compute_p_value_fit_vs_length(dsi, np, ds, degree, in,sigma_min+j*sigma_range);
    set_ds_source(ds,"Sigma = %f, P value as a function of fit length starting for point at %d\n",sigma_min+j*sigma_range,in);


  }

  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}



MENU *FindSteps_plot_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"Pvalue from polyfit", do_FindSteps_pvalue,NULL,0,NULL);
  add_item_to_menu(mn,"FindStep Jump Polyfit ", do_FindSteps_fit_jump,NULL,0,NULL);
  add_item_to_menu(mn,"Median filter", do_FindSteps_median_filter,NULL,0,NULL);
  add_item_to_menu(mn,"Fir", do_FindSteps_mean_filter,NULL,0,NULL);
  add_item_to_menu(mn,"Unglitch", do_FindSteps_glitch,NULL,0,NULL);
  add_item_to_menu(mn,"Jump Chi2 ratio",do_FindSteps_jump_chisqratio,NULL,0,NULL);
  add_item_to_menu(mn,"Polyfit between second ds",do_polyfit_between_points_of_ds2,NULL,0,NULL);





  return mn;
}

int	FindSteps_main(int argc, char **argv)
{
  (void)argc;  (void)argv;  add_plot_treat_menu_item ( "FindSteps", NULL, FindSteps_plot_menu(), 0, NULL);
  return D_O_K;
}

int	FindSteps_unload(int argc, char **argv)
{
  (void)argc;  (void)argv;  remove_item_to_menu(plot_treat_menu, "FindSteps", NULL, NULL);
  return D_O_K;
}
#endif
