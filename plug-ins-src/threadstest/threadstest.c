/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _THREADSTEST_C_
#define _THREADSTEST_C_

# include "allegro.h"
# include "winalleg.h"
# include "xvin.h"


# include "pthread/pthread.h"

/* If you include other plug-ins header do it here*/ 
# include "nrutil.h"

/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "threadstest.h"



double dt_max = 0;
imreg *local_movie = NULL;
imreg *local_bead = NULL;
O_i *oi_simul = NULL;
float x_bead = 30, y_bead = 20, z_bead = 5;
int n_tim = 0;
DIALOG *starting_one = NULL;
int index_menu_dialog = -1;

unsigned long t0 = 0, *tim = NULL;
unsigned long *dtim = NULL;

# define END_SIMUL  8
int co_set = 0;
int sim_on = 0, sim_n_frame = 0;
float co_dl_ld[2048];
float my_sqrt[2048];
float my_exp_1[512];



void *fill_in_ds(void *stuff)
{
  d_s *ds;
  unsigned long *t;
  register int i;

  if (stuff == NULL) return NULL;
  ds = (d_s*)stuff;
  while (ds->nx < ds->mx)
    {
      if (ds->nx == 0)
	{
	  t = (unsigned long *)(ds->xd + ds->mx-1);
	  t[0] = (unsigned long)my_ulclock();
	  ds->nx++;
	}
      else if (ds->nx < ds->mx)
	{
	  t = (unsigned long *)(ds->xd + ds->mx-1);
	  ds->xd[ds->nx] = (float)(((unsigned long)my_ulclock()) - t[0]); 
	  ds->yd[ds->nx] = 1;
	  ds->nx++;
	}
      
      if (ds->nx == ds->mx)
	{
	  for (i = 0; i < ds->nx; i++)
	      ds->xd[i] /= (MY_ULCLOCKS_PER_SEC/1000);
	  win_printf_OK("thread finish");
	}
      
    }
  return (void *)ds;
}

int do_threadstest_prepare_plot(void)
{
	register int i, j;
	O_p *op = NULL;
	static int nf = 1000;
	d_s *ds, *ds2;
	pltreg *pr = NULL;
	pthread_t threads[2];
	int rc;


	if(updating_menu_state != 0)	return D_O_K;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine implement a simple threading teast");
	}
	/* we first find the data that we need to transform */
	if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
		return win_printf_OK("cannot find data");

	i = win_scanf("Specify the number of milliseconds while two threads are running %d",&nf);
	if (i == CANCEL)	return OFF;

	nf *= 10;
	if ((op = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	ds = op->dat[0];

	if ((ds2 = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
		return win_printf_OK("cannot create plot !");


	for (j = 0; j < nf; j++)
	{
		ds2->yd[j] = ds->yd[j] = 0;
		ds->xd[j] = ds2->xd[j] = j;
	}

	/* now we must do some house keeping */
	ds->treatement = my_sprintf(ds->treatement,"Thread 1");
	set_plot_title(op, "Threads");
	ds->treatement = my_sprintf(ds->treatement,"Thread 2");

	/* refisplay the entire plot */
	refresh_plot(pr, UNCHANGED);

	ds->nx = ds2->nx = 0;
	rc = pthread_create(&threads[0], NULL, fill_in_ds, (void *)ds);
	if (rc)	  return win_printf_OK("ERROR; return code from pthread_create() is %d\n", rc);
	rc = pthread_create(&threads[1], NULL, fill_in_ds, (void *)ds2);
	if (rc)	  return win_printf_OK("ERROR; return code from pthread_create() is %d\n", rc);




	return D_O_K;
}

void  allegro_movie_frame_flip(void)
{
  static unsigned long dt = 0, t1;
  static int im = 0;
  double dtm;
  t1 = my_uclock();
  if (t0)  
    {
      dt = t1 - t0;
      dtm = 1000*((double)dt)/get_my_uclocks_per_sec();
      if (dtm > dt_max) dt_max = dtm;
    }
  t0 = t1;
  tim[im%n_tim] = t1;
  dtim[im%n_tim] = my_uclock() - t1;
  im++;
  if (im >= n_tim)
    remove_int(allegro_movie_frame_flip);
  return;
}

void  allegro_movie_frame_flip1(void)
{
  imreg *imr, *ac_imr;
  O_i *oi;
  DIALOG *d;
  BITMAP *imb;
  static unsigned long dt = 0, t1;
  double dtm;

  if (local_movie == NULL) return;
  t1 = my_uclock();
  if (t0)  
    {
      dt = t1 - t0;
      dtm = 1000*((double)dt)/get_my_uclocks_per_sec();
      if (dtm > dt_max) dt_max = dtm;
    }
  t0 = t1;
  imr = local_movie;
  oi = imr->one_i;
  d = find_dialog_associated_to_imr(imr, NULL);
  switch_frame(oi,oi->im.c_f+1);
  tim[oi->im.c_f] = t1;
  oi->need_to_refresh |= BITMAP_NEED_REFRESH;
  display_image_stuff_16M(imr,d);
  if (win_get_window()!= GetForegroundWindow()) 
    {
      dtim[oi->im.c_f] = my_uclock() - t1;
      return;
    }
  ac_imr = find_imr_in_current_dialog(NULL);
  if ((oi->bmp.to == IS_BITMAP) &&  (oi->bmp.stuff != NULL) && (ac_imr == imr))
    {
      imb = (BITMAP*)oi->bmp.stuff;
      acquire_bitmap(screen);
      blit(imb,screen,0,0,imr->x_off + d->x, imr->y_off - 
	   imb->h + d->y, imb->w, imb->h); 
      release_bitmap(screen);	
      display_title_message("im %d",oi->im.c_f);
    }
  dtim[oi->im.c_f] = my_uclock() - t1;
  if (oi->im.c_f == oi->im.n_f -1)
    remove_int(allegro_movie_frame_flip1);

  return;
}



void CALLBACK movie_frame_flip(HWND hwnd, UINT uMsg, UINT_PTR idEvent,  DWORD dwTime)
{
  imreg *imr, *ac_imr;
  O_i *oi;
  DIALOG *d;
  BITMAP *imb;
  static unsigned long dt = 0, t1;
  double dtm;

  if (local_movie == NULL) return;
  t1 = my_uclock();
  if (t0)  
    {
      dt = t1 - t0;
      dtm = 1000*((double)dt)/get_my_uclocks_per_sec();
      if (dtm > dt_max) dt_max = dtm;
    }
  t0 = t1;
  imr = local_movie;
  oi = imr->one_i;
  d = find_dialog_associated_to_imr(imr, NULL);
  switch_frame(oi,oi->im.c_f+1);
  tim[oi->im.c_f] = t1;
  oi->need_to_refresh |= BITMAP_NEED_REFRESH;
  display_image_stuff_16M(imr,d);
  if (win_get_window()!= GetForegroundWindow()) 
    {
      dtim[oi->im.c_f] = my_uclock() - t1;
      return;
    }
  ac_imr = find_imr_in_current_dialog(NULL);
  if ((oi->bmp.to == IS_BITMAP) &&  (oi->bmp.stuff != NULL) && (ac_imr == imr))
    {
      imb = (BITMAP*)oi->bmp.stuff;
      acquire_bitmap(screen);
      blit(imb,screen,0,0,imr->x_off + d->x, imr->y_off - 
	   imb->h + d->y, imb->w, imb->h); 
      release_bitmap(screen);	
      display_title_message("im %d",oi->im.c_f);
    }
  dtim[oi->im.c_f] = my_uclock() - t1;
  if (oi->im.c_f == oi->im.n_f -1)
    KillTimer(win_get_window(),25);

  return;
}



VOID CALLBACK TimerAPCProc(LPVOID lpArgToCompletionRoutine,
  DWORD dwTimerLowValue,
  DWORD dwTimerHighValue
)
{
  imreg *imr, *ac_imr;
  O_i *oi;
  DIALOG *d;
  BITMAP *imb;
  static unsigned long dt = 0, t1;
  double dtm;

  t1  = my_uclock();
  if (t0)  
    {
      dt = t1 - t0;
      dtm = 1000*((double)dt)/get_my_uclocks_per_sec();
      if (dtm > dt_max) dt_max = dtm;
    }
  t0 = t1;
  imr = (imreg *)lpArgToCompletionRoutine;
  oi = imr->one_i;
  d = find_dialog_associated_to_imr(imr, NULL);
  switch_frame(oi,oi->im.c_f+1);
  tim[oi->im.c_f] = t1;
  oi->need_to_refresh |= BITMAP_NEED_REFRESH;
  display_image_stuff_16M(imr,d);
  if (win_get_window()!= GetForegroundWindow()) 
    {
      dtim[oi->im.c_f+1] = my_uclock() - t1;
      return;
    }
  ac_imr = find_imr_in_current_dialog(NULL);
  if ((oi->bmp.to == IS_BITMAP) && (oi->bmp.stuff != NULL) && (ac_imr == imr))
    {
      imb = (BITMAP*)oi->bmp.stuff;
      acquire_bitmap(screen);
      blit(imb,screen,0,0,imr->x_off + d->x, imr->y_off - 
	   imb->h + d->y, imb->w, imb->h); 
      release_bitmap(screen);	
      display_title_message("im %d",oi->im.c_f);
    }
  dtim[oi->im.c_f] = my_uclock() - t1;
  return;
}


DWORD WINAPI ThreadProc( LPVOID lpParam ) 
{
    HANDLE hTimer = NULL;
    LARGE_INTEGER liDueTime;
    imreg *imr;
    O_i *oi;
    int nf, i;

    liDueTime.QuadPart=-300000; // 30 ms
    imr = (imreg*)lpParam;
    oi = imr->one_i;
    nf = abs(oi->im.n_f);		
    dt_max = 0;
    t0 = 0;

    // Create a waitable timer.
    hTimer = CreateWaitableTimer(NULL, TRUE, "WaitableTimer");
    if (!hTimer)   win_printf_OK("CreateWaitableTimer failed (%d)\n", GetLastError());

    for (i = 0; i < nf; i++)
      {
	// Set a timer to wait for 10 seconds.
	if (!SetWaitableTimer(hTimer, &liDueTime, 0, TimerAPCProc, (void*)imr, 0))
	  win_printf_OK("SetWaitableTimer failed (%d)\n", GetLastError());

	// Wait for the timer.
	SleepEx (INFINITE, TRUE);
      }
      //if (WaitForSingleObject(hTimer, INFINITE) != WAIT_OBJECT_0)
      // win_printf_OK("WaitForSingleObject failed (%d)\n", GetLastError());
      //else 
      win_printf_OK("Timer was signaled.\n");
      return 0;
}


int set_my_timer(void)
{
    HANDLE hThread = NULL;
    DWORD dwThreadId;
    imreg *imr;
    O_i *oi;
    int nf, i;


    if(updating_menu_state != 0)	return D_O_K;

    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)
      {
	active_menu->flags |=  D_DISABLED;
	return D_O_K;	
      }		
    
    if(updating_menu_state != 0)	
      {
	if (oi->im.n_f == 1 || oi->im.n_f == 0)	
	  active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;	
      }	
    nf = abs(oi->im.n_f);		
    if (nf <= 0)       	win_printf_OK("This is not a movie");

    tim = (unsigned long*)realloc(tim,nf*sizeof(unsigned long));
    dtim = (unsigned long*)realloc(dtim,nf*sizeof(unsigned long));
    if (tim == NULL || dtim == NULL)      win_printf("Pb allocation");
    n_tim = nf;
    for (i = 0; i < n_tim; i++) tim[i] = 0;
    switch_frame(oi,0);

    dt_max = 0;
    t0 = 0;

    hThread = CreateThread( 
            NULL,              // default security attributes
            0,                 // use default stack size  
            ThreadProc,        // thread function 
            (void*)imr,        // argument to thread function 
            0,                 // use default creation flags 
            &dwThreadId);      // returns the thread identifier 

    if (hThread == NULL) 	win_printf_OK("No thread created");
    SetThreadPriority(hThread,  THREAD_PRIORITY_TIME_CRITICAL);


    return 0;
}



int wait_oi_idle_action(struct  one_image *oi, DIALOG *d)
{
  // we put in this routine all the screen display stuff
  imreg *imr;
  BITMAP *imb;

  if (d->dp == NULL)    return D_O_K;
  imr = (imreg*)d->dp;        /* the image region is here */
  oi = imr->one_i;         /* the plot is here */
  if (imr->one_i == NULL || imr->n_oi == 0)        return D_O_K;

  if (oi->need_to_refresh & BITMAP_NEED_REFRESH)
    {
      screen_used = 1;
      SET_BITMAP_IN_USE(oi);
      imb = (BITMAP*)oi->bmp.stuff;
      acquire_bitmap(screen);
      blit(imb,screen,0,0,imr->x_off + d->x, imr->y_off - 
	   imb->h + d->y, imb->w, imb->h); 
      release_bitmap(screen);
      screen_used = 0;
      oi->need_to_refresh &= ~BITMAP_NEED_REFRESH;
      SET_BITMAP_NO_MORE_IN_USE(oi);
      display_title_message("movie im %d dt %6.3f ms",oi->im.c_f,  1000*(double)(d->d1)/get_my_uclocks_per_sec());
    }
  

  return 1;
}


VOID CALLBACK dummyAPCProc(LPVOID lpArgToCompletionRoutine,
  DWORD dwTimerLowValue,
  DWORD dwTimerHighValue
)
{
  return;
}

DWORD WINAPI ThreadProcFull( LPVOID lpParam ) 
{
    HANDLE hTimer = NULL;
    LARGE_INTEGER liDueTime;
    imreg *imr, *ac_imr;
    DIALOG *d;
    O_i *oi;
    int nf, i;
    BITMAP *imb;
    unsigned long dt = 0, t0 = 0, t1 = 0, dtmax;
    DIALOG *starting_one = NULL;

    liDueTime.QuadPart=-300000; // 30 ms
    imr = (imreg*)lpParam;
    d = find_dialog_associated_to_imr(imr, NULL);
    oi = imr->one_i;
    nf = abs(oi->im.n_f);		
    // Create a waitable timer.
    hTimer = CreateWaitableTimer(NULL, TRUE, "WaitableTimer2");
    if (!hTimer)   win_printf_OK("CreateWaitableTimer failed (%d)\n", GetLastError());

    starting_one = active_dialog; // to check if windows change

    for (i = 0, dtmax = 0; i < nf; i++)
      {
	// Set a timer to wait for 10 seconds.
	if (!SetWaitableTimer(hTimer, &liDueTime, 0, dummyAPCProc, NULL, 0))
	  win_printf_OK("SetWaitableTimer failed (%d)\n", GetLastError());

	// Wait for the timer.
	SleepEx (INFINITE, TRUE);
	//if (WaitForSingleObject(hTimer, INFINITE) == WAIT_OBJECT_0)
	//{
	t1 = my_uclock();
	if (i)  
	  {
	    dt = t1 - t0;
	    if (dt > dtmax) dtmax = dt;
	  }
	t0 = t1;
	
	
	switch_frame(oi,i);
	tim[oi->im.c_f] = t1;
	oi->need_to_refresh |= BITMAP_NEED_REFRESH;
	display_image_stuff_16M(imr,d);
	oi->need_to_refresh |= BITMAP_NEED_REFRESH;
	d->d1 = (int)t0-t1;

	ac_imr = find_imr_in_current_dialog(NULL);
# ifdef OLD
	if (win_get_window() == GetForegroundWindow()) 
	  {
	    if ((oi->bmp.to == IS_BITMAP) &&  (oi->bmp.stuff != NULL) 
		&& (ac_imr == imr))
	      {
		imb = (BITMAP*)oi->bmp.stuff;
		acquire_bitmap(screen);
		blit(imb,screen,0,0,imr->x_off + d->x, imr->y_off - 
		     imb->h + d->y, imb->w, imb->h); 
		release_bitmap(screen);	
		display_title_message("im %d",oi->im.c_f);
	      }
	  }
# endif
	dtim[oi->im.c_f] = my_uclock() - t1;
	//}
      }
    dt_max = 1000*((double)dtmax)/get_my_uclocks_per_sec();
    //if (WaitForSingleObject(hTimer, INFINITE) != WAIT_OBJECT_0)
    // win_printf_OK("WaitForSingleObject failed (%d)\n", GetLastError());
    //else 
    //win_printf_OK("Timer was signaled.\n");

    return 0;
}




int set_my_timer_wait(void)
{
    HANDLE hThread = NULL;
    DWORD dwThreadId;
    imreg *imr;
    O_i *oi;
    int nf, i;


    if(updating_menu_state != 0)	return D_O_K;

    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)
      {
	active_menu->flags |=  D_DISABLED;
	return D_O_K;	
      }		
    
    if(updating_menu_state != 0)	
      {
	if (oi->im.n_f == 1 || oi->im.n_f == 0)	
	  active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;	
      }	
    nf = abs(oi->im.n_f);		
    if (nf <= 0)       	win_printf_OK("This is not a movie");

    tim = (unsigned long*)realloc(tim,nf*sizeof(unsigned long));
    dtim = (unsigned long*)realloc(dtim,nf*sizeof(unsigned long));
    if (tim == NULL || dtim == NULL)      win_printf("Pb allocation");
    n_tim = nf;
    for (i = 0; i < n_tim; i++) tim[i] = 0;
    switch_frame(oi,0);
    oi->oi_idle_action = wait_oi_idle_action;
    dt_max = 0;


    hThread = CreateThread( 
            NULL,              // default security attributes
            0,                 // use default stack size  
            ThreadProcFull,        // thread function 
            (void*)imr,        // argument to thread function 
            0,                 // use default creation flags 
            &dwThreadId);      // returns the thread identifier 

    if (hThread == NULL) 	win_printf_OK("No thread created");
    SetThreadPriority(hThread,  THREAD_PRIORITY_TIME_CRITICAL);


    return 0;
}

int set_my_timer2(void)
{
    imreg *imr;
    O_i *oi;
    int nf, i;


    if(updating_menu_state != 0)	return D_O_K;

    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)
      {
	active_menu->flags |=  D_DISABLED;
	return D_O_K;	
      }		
    
    if(updating_menu_state != 0)	
      {
	if (oi->im.n_f == 1 || oi->im.n_f == 0)	
	  active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;	
      }	
    nf = abs(oi->im.n_f);		
    if (nf <= 0)	
      {
	win_printf("This is not a movie");
	return D_O_K;
      }
    dt_max = 0;
    tim = (unsigned long*)realloc(tim,nf*sizeof(unsigned long));
    dtim = (unsigned long*)realloc(dtim,nf*sizeof(unsigned long));
    if (tim == NULL || dtim == NULL)   win_printf("Pb allocation");
    n_tim = nf;
    for (i = 0; i < n_tim; i++) tim[i] = 0;
    switch_frame(oi,0);
    local_movie = imr;

    SetTimer( win_get_window() , 25 , 25 , movie_frame_flip );


    return 0;
}


int set_allegro_timer(void)
{
    imreg *imr;
    O_i *oi;
    int nf, i;


    if(updating_menu_state != 0)	return D_O_K;

    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)
      {
	active_menu->flags |=  D_DISABLED;
	return D_O_K;	
      }		
    
    if(updating_menu_state != 0)	
      {
	if (oi->im.n_f == 1 || oi->im.n_f == 0)	
	  active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;	
      }	
    nf = abs(oi->im.n_f);		
    if (nf <= 0)	
      {
	win_printf("This is not a movie");
	return D_O_K;
      }
    dt_max = 0;
    tim = (unsigned long*)realloc(tim,nf*sizeof(unsigned long));
    dtim = (unsigned long*)realloc(dtim,nf*sizeof(unsigned long));
    if (tim == NULL || dtim == NULL)   win_printf("Pb allocation");
    n_tim = nf;
    for (i = 0; i < n_tim; i++) tim[i] = 0;
    switch_frame(oi,0);
    local_movie = imr;

    install_int_ex(allegro_movie_frame_flip1,MSEC_TO_TIMER(30));


    return 0;
}

int max_dt(void)
{
  register int i, j;
  O_p *op;
  d_s *ds, *ds2;
  imreg *imr;
  O_i *oi;

  if(updating_menu_state != 0)	return D_O_K;
  
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;	
    }		

  win_printf("maximum dt %g",dt_max);
  op = create_and_attach_op_to_oi(oi, n_tim, n_tim, 0,0);		
  if (op == NULL)   return win_printf_OK("cannot create plot !");
  ds = op->dat[0];	
  ds2 = create_and_attach_one_ds(op,n_tim,n_tim,0);
  if (ds2 == NULL) return win_printf("Cannot create data set");
  for (i = j = 0; i < n_tim-1; i++)
    {
      if (tim[i] != 0)
	{
	  ds->xd[j] = ds2->xd[j] = j;
	  ds->yd[j] = 1000*((float)tim[i+1] -tim[i])/get_my_uclocks_per_sec();
	  ds2->yd[j] = 1000*((float)dtim[i])/get_my_uclocks_per_sec();
	  j++;
	}
    }
  ds->nx = ds->ny = ds2->nx = ds2->ny = j;

  op->need_to_refresh = 1; 
  set_plot_title(op, "movie timing");
  return refresh_im_plot(imr, oi->n_op - 1);	
}

int simul_oi_idle_action(struct  one_image *oi, DIALOG *d)
{
  // we put in this routine all the screen display stuff
  imreg *imr;
  BITMAP *imb;

  if (d->dp == NULL)    return D_O_K;
  imr = (imreg*)d->dp;        /* the image region is here */
  oi = imr->one_i;         /* the plot is here */
  if (imr->one_i == NULL || imr->n_oi == 0)        return D_O_K;

  if (oi->need_to_refresh & BITMAP_NEED_REFRESH)
    {
      screen_used = 1;
      SET_BITMAP_IN_USE(oi);
      imb = (BITMAP*)oi->bmp.stuff;
      acquire_bitmap(screen);
      blit(imb,screen,0,0,imr->x_off + d->x, imr->y_off - 
	   imb->h + d->y, imb->w, imb->h); 
      release_bitmap(screen);
      screen_used = 0;
      oi->need_to_refresh &= ~BITMAP_NEED_REFRESH;
      SET_BITMAP_NO_MORE_IN_USE(oi);
      display_title_message("im %d dt %6.3f ms",oi->im.c_f,  1000*(double)(d->d1)/get_my_uclocks_per_sec());
    }
  

  return 0;
}



int add_bead_talbot_image(O_i *oi,                         // the image to put the bead in 
			  float xcf, float ycf, float zcf, // bead position in microns
			  float dx,                        // the extend of one pixel in microns
			  float lambda,         // the light wavelentgth in microns
			  float l_d,            // the coherence decay length of light in microns
			  float Id,             // the ratio of diffuse light
			  float theta_max,      // the maximum angle that the light ray can have
			  int rc)               // the radius of the bead
{
  register int i, j, k;
  float x, y, x2, y2, z2, r, z, l, dl, It, Imin, Ic, pxl, rcp, lambda_100, l_d_64;
  int onx, ony, xci, yci, xis, xie, yis, yie, in;
  union pix  *pd;

  Imin = cos(M_PI*theta_max/180);
  if (oi == NULL) return 1;
  onx = oi->im.nx;   ony = oi->im.ny;


 if (co_set == 0)
   {
     for (i = 0; i < 512; i++) 
       my_exp_1[i] = exp(-((float)i)/64); 
     for (i = 0; i < 2048; i++) 
       co_dl_ld[i] = 2*cos((M_PI*(float)i)/100); 
     for (i = 0; i < 2048; i++)
       my_sqrt[i] = sqrt((float)i/4); 
   } 

  xci = (int)((xcf/dx)+.5);   
  xis = xci - 99;
  xis = (xis < 0) ? 0 : xis;
  xis = (xis < onx) ? xis : onx - 1;
  xie = xci + 100;
  xie = (xie < 0) ? 0 : xie;
  xie = (xie < onx) ? xie : onx;
  yci = (int)((ycf/dx)+.5);
  yis = yci - 99;
  yis = (yis < 0) ? 0 : yis;
  yis = (yis < ony) ? yis : ony - 1;
  yie = yci + 100;
  yie = (yie < 0) ? 0 : yie;
  yie = (yie < ony) ? yie : ony;

  rcp = rc*dx;
  lambda_100 = (float)100/lambda;
  l_d_64 = (float)64/l_d;
  z = zcf;
  z2 = 4*z*z; // 3*z*z is interesting
  for (i = yis, pd = oi->im.pixel; i < yie ; i++)
    {
      y = fabs(ycf - (i*dx));
      y2 = 4*y*y+0.5;
      for (j = xis; j< xie; j++)
	{
	  x = fabs(xcf - (j*dx));                 // x and y are from pixel to bead center in microns
	  //r = sqrt(x*x + y*y);                   // r is distance from pixel to bead center in microns
	  x2 = 4*x*x;
	  r = my_sqrt[(int)(x2 + y2)]; // r is distance from pixel to bead center in microns
	  //k = (int)(r/dx);                             // its integer part
	  Ic = (r < rcp) ? 0 : 1;                  //
	  //r = dx * r;                             // this is the distance in microns
	  //l = sqrt(z*z + r*r);
	  l = my_sqrt[(int)(z2+x2+y2)];  // this is the optical ray path
	  It = (l == 0) ? 1: fabs(z/l);           // Id*cos \theta, simulate the decay of diffusion   
	  It = (It < Imin) ? 0 : It;  
	  dl = l - fabs(z);                       // the difference of ray travel 
	  //It *= Id * exp(-dl/l_d);                // It now contains deifuse light level
	  It *= Id * my_exp_1[(int)((dl*l_d_64)+.5)];
	  pxl = Ic-It;
	  in = (int)(0.5+dl*lambda_100);
	  in = (in < 2048) ? in : 2047;
	  in = (in < 0) ? 0 : in;
	  It *= co_dl_ld[in];
	  //It *= 2 * cos(M_PI*dl/lambda);//co_dl_ld[in]; //cos(M_PI*dl/lambda);
	  pxl += It;
	  pd[i].ch[j] = (unsigned char)(pxl*130);
	}
    }
  return 0;
}




#define IA 16807
#define RIM 2147483647
//#define AM (1.0/RIM)
#define IQ 127773
#define IR 2836
#define MASK 123459876
long ran0(long idum) // Minimal random number generator of Park and Miller. 
                     // Returns a uniform random deviate between 0 and 2^31 - 1
{
  long k;
  idum ^= MASK;                 // XORing with MASK allows use of zero and other simple bit patterns for 
  k=(idum)/IQ;                  //idum.
  idum = IA*(idum-k*IQ)-IR*k;  // Compute idum=(IA*idum) % IM without over
  if (idum < 0) idum += RIM;     //flows by Schrage�s method.
                                //ans=AM*(*idum); Convert idum to a floating result.
  idum ^= MASK;                //Unmask before return.
  return idum;
}


long simulate_video_noise(O_i *oi, long idum)
{
  register int i, j;
  int onx, ony;
  long l = 0;

  if (oi == NULL)  return 1;
  onx = oi->im.nx; ony = oi->im.ny;
  for ( i = 0; i < ony; i++)
    {
      for ( j = 0; j < onx; j++)	    
	{
	  if (j%15 == 0) l = idum = ran0(idum);
	  oi->im.pixel[i].ch[j] = 128 | (l & 0x3);
	  l >>= 2;
	}
    }
  return idum;
}



void CALLBACK simulate_one_frame(HWND hwnd, UINT uMsg, UINT_PTR idEvent,  DWORD dwTime)
{
  imreg *imr, *ac_imr;
  O_i *oi;
  DIALOG *d, *di;
  BITMAP *imb;
  static long idum = 87, n = 0;
  static unsigned long dt = 0, t1;
  double dtm;

  if (local_bead == NULL || sim_on == 0) return;
  t1 = my_uclock();
  if (t0)  
    {
      dt = t1 - t0;
      dtm = 1000*((double)dt)/get_my_uclocks_per_sec();
      if (dtm > dt_max) dt_max = dtm;
    }
  t0 = t1;
  imr = local_bead;
  oi = oi_simul;
  d = find_dialog_associated_to_imr(imr, NULL);
  if (IS_DATA_IN_USE(oi))   return;
  SET_DATA_IN_USE(oi);
  idum = simulate_video_noise(oi, idum);
  //t1 = my_uclock();
  x_bead += 0.2*gasdev(&idum);
  y_bead += 0.2*gasdev(&idum);
  z_bead += 0.2*gasdev(&idum);
  add_bead_talbot_image(oi, x_bead, y_bead, z_bead, 0.1, 0.45, 1.5, 0.3, 70, 0);
  SET_DATA_NO_MORE_IN_USE(oi);
  //tim[oi->im.c_f] = t1;
  oi->need_to_refresh |= BITMAP_NEED_REFRESH;
  display_image_stuff_16M(imr,d);
  t0 = my_uclock();

  oi->need_to_refresh |= BITMAP_NEED_REFRESH;
  sim_n_frame++;
  oi->im.c_f++;
  d->d1 = (int)t0-t1;
# ifdef OLD
  if (win_get_window()!= GetForegroundWindow()) 
    {
      //dtim[oi->im.c_f] = my_uclock() - t1;
      return;
    }
  ac_imr = find_imr_in_current_dialog(NULL);
  di = find_dialog_focus(active_dialog);                
  if ((oi->bmp.to == IS_BITMAP) &&  (oi->bmp.stuff != NULL) && (ac_imr == imr)
      && starting_one == active_dialog && di != index_menu_dialog
      && !IS_BITMAP_IN_USE(oi) && !screen_used)
    {
      screen_used = 1;
      SET_BITMAP_IN_USE(oi);
      imb = (BITMAP*)oi->bmp.stuff;
      /*
      acquire_bitmap(screen);
      blit(imb,screen,0,0,imr->x_off + d->x, imr->y_off - 
	   imb->h + d->y, imb->w, imb->h); 
      release_bitmap(screen);
      */
      screen_used = 0;
      SET_BITMAP_NO_MORE_IN_USE(oi);
      //t0 = my_uclock();	
      display_title_message("im %d dt %6.3f ms",n++,  1000*(double)(t0-t1)/get_my_uclocks_per_sec());
    }
# endif
  //dtim[oi->im.c_f] = my_uclock() - t1;

  if (sim_on == END_SIMUL) KillTimer(win_get_window(),25);
  return;
}


int freeze_dummy_beads(void)
{
  imreg *imr;
  if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;	
    }		
  if(updating_menu_state != 0)	return D_O_K;
  sim_on = 0;
  return D_O_K;
}

int generate_dummy_beads(void)
{
  register int i, j;
  imreg *imr;
  O_i *oi;
  int onx = 756, ony = 572;
  long idum = 45, l = 0;
  float dx = 0.1;
  
  if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;	
    }		
  if(updating_menu_state != 0)	return D_O_K;
  if (local_bead  != NULL)
    {
      sim_on = 1;
      return D_O_K;
    }
  
  oi = create_and_attach_oi_to_imr(imr, onx, ony, IS_CHAR_IMAGE);
  if (oi == NULL)	return win_printf_OK("cannot create profile !");
  oi->im.source = strdup("Dummy beads");
  oi_simul = oi;
  for ( i = 0; i < ony; i++)
    {
      for ( j = 0; j < onx; j++)	    
	{
	  if (j%15 == 0) l = idum = ran0(idum);
	  oi->im.pixel[i].ch[j] = 128 | (l & 0x3);
	  l >>= 2;
	}
    }
  oi->width = (float)onx/512;
  oi->height = (float)ony/512;
  oi->iopt2 &= ~X_NUM;		oi->iopt2 &= ~Y_NUM;		

  oi->oi_idle_action = simul_oi_idle_action;
  add_bead_talbot_image(oi, 20, 15, 5, 0.1, 0.6, 3, 0.1, 70, 0);
  //  find_zmin_zmax(oi);	
  set_zmin_zmax_values(oi, 50, 150);
  set_z_black_z_white_values(oi, 50, 150);
  oi->need_to_refresh = ALL_NEED_REFRESH;	
  create_attach_select_x_un_to_oi(oi, IS_METER, 0, dx, -6, 0, "\\mu m");
  create_attach_select_y_un_to_oi(oi, IS_METER, 0, dx, -6, 0, "\\mu m");
  refresh_image(imr, imr->n_oi - 1);
  starting_one = active_dialog; // to check if windows change
  index_menu_dialog = retrieve_item_from_dialog(d_menu_proc, NULL);
  index_menu_dialog = 1;
  local_bead = imr;
  sim_on = 1;
  SetTimer( win_get_window() , 50 , 50 , simulate_one_frame );
  return D_O_K;
}

MENU *threadstest_image_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"timer movie", set_my_timer,NULL,0,NULL);
	add_item_to_menu(mn,"timer movie 2", set_my_timer2,NULL,0,NULL);
	add_item_to_menu(mn,"timer wait", set_my_timer_wait,NULL,0,NULL);
	add_item_to_menu(mn,"allegro timer", set_allegro_timer,NULL,0,NULL);
	add_item_to_menu(mn,"simulate beads", generate_dummy_beads,NULL,0,NULL);
	add_item_to_menu(mn,"freeze simulate beads", freeze_dummy_beads,NULL,0,NULL);
	

	add_item_to_menu(mn,"max dt", max_dt,NULL,0,NULL);
	return mn;
}


MENU *threadstest_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"Thread test", do_threadstest_prepare_plot,NULL,0,NULL);
	return mn;
}

int	threadstest_main(int argc, char **argv)
{
	add_plot_treat_menu_item ( "threadstest", NULL, threadstest_plot_menu(), 0, NULL);
	add_image_treat_menu_item ( "threadstest", NULL, threadstest_image_menu(), 0, NULL);
	return D_O_K;
}

int	threadstest_unload(int argc, char **argv)
{
	remove_item_to_menu(plot_treat_menu, "threadstest", NULL, NULL);
	return D_O_K;
}
#endif

