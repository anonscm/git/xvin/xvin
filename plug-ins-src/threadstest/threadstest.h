#ifndef _THREADSTEST_H_
#define _THREADSTEST_H_

PXV_FUNC(int, do_threadstest_rescale_plot, (void));
PXV_FUNC(MENU*, threadstest_plot_menu, (void));
PXV_FUNC(int, do_threadstest_rescale_data_set, (void));
PXV_FUNC(int, threadstest_main, (int argc, char **argv));
#endif

