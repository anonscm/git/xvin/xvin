/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _POLYSEQ_C_
#define _POLYSEQ_C_

# include "allegro.h"
# include "xvin.h"

/* If you include other regular header do it here*/


/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled
# include "../nrutil/nrutil.h"


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
//place here other headers of this plugin
# include "allegro.h"
# include "xvin.h"

/* If you include other regular header do it here*/


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "PolySeq.h"


char seq[] = "GGAGACGAGCTCAGGCCTTAGAGTCAAGACTACAGTAGATACTAATGACTGACAGTACCATCAGCATCTCTATATGTAAC"
  "AGAGTCTTAGAACTGTAGTACTCATGTCATGATCAGATCCATCTTGATGTACTGTCAGACTATCCTCATAAGTCTAGTGA"
  "GTACTGAAGATACATAGCTCAGATCTGATCATACACGACAGGAGTAGAAGTATACACTGAAGTAGATAGATCGATTGACT"
  "ATGACCATGCTATGACTGACTGCTACTATTCTAGAGTGACAGTATTACTAAGTACGAGCATCTAACAAGATGAACCTGAC"
  "AGACACTGACATAGCGATTATCATAGCTAGTCACTCTGACGTATCTCAGTCTAACATCAGCACATGAATGTATGACTACA"
  "GTTACTATCAAGTACACTCTGAGTCAGCATCATCAGTAGATCGATCAGTACAGTATCTAGGACATGACTACTTGAGTGAG"
  "AGAATCTTCTACTATCTGTAGTCAGCTTCGTAACACTCTGATGTCATCTTAATGCTATGATTGTCTACTTGATCGACTAC"
  "TAGATAACTATAGTTCAGCAGTACGAATTCTCATAGCATGATACCAATGAAATCATCGAAGTAGCCAATGCATTGG";





int do_PolySeq_hello(void)
{
  register int i;

  if(updating_menu_state != 0)	return D_O_K;

  i = win_printf("Hello from PolySeq");
  if (i == WIN_CANCEL) win_printf_OK("you have press WIN_CANCEL");
  else win_printf_OK("you have press OK");
  return 0;
}

int do_PolySeq_rescale_data_set(void)
{
  register int i, j;
  O_p *op = NULL;
  int nf;
  static float factor = 1;
  d_s *ds, *dsi;
  pltreg *pr = NULL;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  nf = dsi->nx;	/* this is the number of points in the data set */
  i = win_scanf("By what factor do you want to multiply y %f",&factor);
  if (i == WIN_CANCEL)	return OFF;

  if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");

  for (j = 0; j < nf; j++)
  {
    ds->yd[j] = factor * dsi->yd[j];
    ds->xd[j] = dsi->xd[j];
  }

  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"y multiplied by %f",factor);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}

int do_PolySeq_rescale_plot(void)
{
  register int i, j;
  O_p *op = NULL, *opn = NULL;
  int nf;
  static float factor = 1;
  d_s *ds, *dsi;
  pltreg *pr = NULL;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number and place it in a new plot");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  nf = dsi->nx;	/* this is the number of points in the data set */
  i = win_scanf("By what factor do you want to multiply y %f",&factor);
  if (i == WIN_CANCEL)	return OFF;

  if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  ds = opn->dat[0];

  for (j = 0; j < nf; j++)
  {
    ds->yd[j] = factor * dsi->yd[j];
    ds->xd[j] = dsi->xd[j];
  }

  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"y multiplied by %f",factor);
  set_plot_title(opn, "Multiply by %f",factor);
  if (op->x_title != NULL) set_plot_x_title(opn, "%s",op->x_title);
  if (op->y_title != NULL) set_plot_y_title(opn, "%s",op->y_title);
  opn->filename = Transfer_filename(op->filename);
  uns_op_2_op(opn, op);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}





int 	do_PolySeq_signal(void)
{
  int i, len, iseq;
  static float amp = 1.0, prob;
  static int nx = 16384, new_plot = 0, repeat = 1;
  pltreg *pr;
  O_p *op;
  d_s *ds, *ds2;
  static int idum = 4587;
  static float perA = 0.5, perC = 0.5, perG = 200, perT = 0.5;
  float probA, probC, probG, probT;

  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return 1;
  i = win_scanf("Generate a polymerase signal of sequencing by one dNTP depletion\n"
		"assuming a poisson distribution for base incorporation time\n"
		"%R in this plot %r in a new plot\n"
		"Mean incorporation time for A %8f\n"
		"Mean incorporation time for C %8f\n"
		"Mean incorporation time for G %8f\n"
		"Mean incorporation time for T %8f\n"
		"Amplitude of noise added at each step %8f\n"
		"Number of repeats of the process process %6d\n"
		"Random seed %8d\n"
		,&new_plot,&perA,&perC,&perG,&perT,&amp,&repeat,&idum);
  if (i == WIN_CANCEL)	return D_O_K;
  if (new_plot)
    {
      if ((op = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create plot");
      ds = op->dat[0];
      ds2 = create_and_attach_one_ds(op, nx, nx, 0);
      if (ds2 == NULL)  return win_printf_OK("cannot create ds");
      ds->nx = ds->ny = 0;
      ds2->nx = ds2->ny = 0;
      set_plot_title(op, "\\stack{{Polymerase sequecing}"
		     "{\\pt8 PerA %g PerC %g PerG %g PerT %g}{Noise amplitude %g}}"
		     ,perA,perC,perG,perT,amp);
    }
  else
    {
      if ((ds = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      if ((ds2 = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      ds->nx = ds->ny = 0;
      ds2->nx = ds2->ny = 0;
    }
  set_dot_line(ds);
  set_ds_source(ds, "\\stack{{Polymerase sequecing}"
		     "{PerA %g PerC %g PerG %g PerT %g}{Noise amplitude %g}}"
		     ,perA,perC,perG,perT,amp);
  set_ds_source(ds2, "\\stack{{Polymerase sequecing}"
		     "{PerA %g PerC %g PerG %g PerT %g}{No Noise}}"
		     ,perA,perC,perG,perT);


  probA = ((float)1)/perA;
  probC = ((float)1)/perC;
  probG = ((float)1)/perG;
  probT = ((float)1)/perT;
  len = strlen(seq);
  int ir;
  for (ir = i = 0; ir < repeat; ir++)
    {
      for (iseq = 0; iseq < len; i++)
	{
	  if (seq[iseq] == 'A') prob = probA;
	  else if (seq[iseq] == 'C') prob = probC;
	  else if (seq[iseq] == 'G') prob = probG;
	  else if (seq[iseq] == 'T') prob = probT;
	  else win_printf("wrong DNA seq %c",seq[iseq]);
	  if (ran1(&idum) < prob)
	    {
	      iseq++;
	    }
	  add_new_point_to_ds(ds, i, iseq + amp * gasdev(&idum));
	  add_new_point_to_ds(ds2, i, iseq);
	}
      i += 10;
      add_new_point_to_ds(ds, i, -10);
      add_new_point_to_ds(ds2, i, -10);
    }
  return refresh_plot(pr,pr->n_op-1);
}





int 	do_helicase_pause_signal(void)
{
  int i, iseq;
  static float amp = 1.0;
  static int nx = 16384, new_plot = 0, repeat = 1;
  pltreg *pr;
  O_p *op;
  d_s *ds, *ds2;
  static int idum = 4587;
  static float prob_p = 0.05, ptau = 25, dz = 0.43;
  float prob, z;
  int pausing = 0;

  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return 1;
  i = win_scanf("Generate a helicase signal with pausing\n"
		"assuming a poisson distribution for pausing time\n"
		"%R in this plot %r in a new plot\n"
		"Total number of steps %8d\n"
		"Pausing probability at each step %8f\n"
		"Mean pausing time %8f\n"
		"step size %8f\n"
		"Amplitude of noise added at each step %8f\n"
		"Number of repeats of the process process %6d\n"
		"Random seed %8d\n"
		,&new_plot,&nx,&prob_p,&ptau,&dz,&amp,&repeat,&idum);
  if (i == WIN_CANCEL)	return D_O_K;
  if (new_plot)
    {
      if ((op = create_and_attach_one_plot(pr, nx*repeat, nx*repeat, 0)) == NULL)
	return win_printf_OK("cannot create plot");
      ds = op->dat[0];
      ds2 = create_and_attach_one_ds(op, nx*repeat, nx*repeat, 0);
      if (ds2 == NULL)  return win_printf_OK("cannot create ds");
      ds->nx = ds->ny = 0;
      ds2->nx = ds2->ny = 0;
      set_plot_title(op, "\\stack{{helicase pausing}"
		     "{\\pt8 Prob_p %g \\tau  %g dz %g}{Noise amplitude %g}}"
		     ,prob_p,ptau,dz,amp);
    }
  else
    {
      if ((ds = create_and_attach_one_ds(op, nx*repeat, nx*repeat, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      if ((ds2 = create_and_attach_one_ds(op, nx*repeat, nx*repeat, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      ds->nx = ds->ny = 0;
      ds2->nx = ds2->ny = 0;
    }
  set_dot_line(ds);
  set_ds_source(ds, "\\stack{{helicase pausing}"
		     "{\\pt8 Prob_p %g \\tau  %g dz %g}{Noise amplitude %g}}"
		     ,prob_p,ptau,dz,amp);

  prob = ((float)1)/ptau;
  int ir;
  for (ir = i = 0; ir < repeat; ir++)
    {
      for (iseq = 0, pausing = 0, z = 0; iseq < nx; iseq++, i++)
	{
	  if (pausing > 0)
	    {
	      pausing = (ran1(&idum) < prob) ? 0 : pausing;
	    }
	  else
	    {
	      z += dz;
	      pausing = (ran1(&idum) < prob_p) ? 1 : pausing;
	    }
	  add_new_point_to_ds(ds, i, z + amp * gasdev(&idum));
	  add_new_point_to_ds(ds2, i, z);
	}
      i += 10;
      add_new_point_to_ds(ds, i, 0);
      add_new_point_to_ds(ds2, i, 0);
    }
  return refresh_plot(pr,pr->n_op-1);
}

int 	do_unzip_signal(void)
{
  int i, j, i_1, z, z_1, k;
  static int nx = 16384, new_plot = 0, mlen = 10, timin = 0;
  pltreg *pr;
  O_p *op;
  d_s *ds = NULL, *dst = NULL;
  static int idum = 4587;
  static float prob_u = 0.01, prob_r = 0.05;
  float p2 = 0, tmp;
  int *timing = NULL, *done = NULL;;


  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return 1;
  i = win_scanf("Generate an unzipping signal of a fork\n"
		"assuming a poisson distribution for pausing time\n"
		"%R in this plot %r in a new plot\n"
		"Total number of steps %8d\n"
		"Probability of the fork to unzip %6f\n"
		"Probability of the fork to rezip %6f\n"
		"Size of unzipp region leading to reset %d\n"
		"Random seed %8d\n"
		"%R->Generate nothing %r->time between event %r segments\n"
		,&new_plot,&nx,&prob_u,&prob_r,&mlen,&idum,&timin);
  if (i == WIN_CANCEL)	return D_O_K;
  if (timin)
    {
      timing = (int*)calloc(mlen+1,sizeof(int));
      done = (int*)calloc(mlen+1,sizeof(int));
      if (timing == NULL || done == NULL)
	return win_printf_OK("cannot create timing");
    }
  if (new_plot)
    {
      if ((op = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create plot");
      ds = op->dat[0];
      ds->nx = ds->ny = 0;
      set_plot_title(op, "\\stack{{Fork unzipping}"
		     "{\\pt8 Prob unzip %g Prob rezip  %g}}"
		     ,prob_u,prob_r);
    }
  else
    {
      if ((ds = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      ds->nx = ds->ny = 0;
    }
  if (timin)
    {
      for (i = 1; i <= mlen; i++)
	{
	  if ((dst = create_and_attach_one_ds(op, 16, 16, 0)) == NULL)
	    return win_printf_OK("cannot create data set");
	  dst->nx = dst->ny = 0;
	  if (timin == 2)     set_dash_line(dst);
	}
    }

  //set_dot_line(ds);
  set_ds_source(ds,"Fork unzipping Prob unzip %g Prob rezip  %g",prob_u,prob_r);
  p2 = prob_u + prob_r;
  for (i = j = 0, z = 0, z_1 = -1, i_1 = -1; i < nx; j++)
    {
      if (z < mlen)
	{
	  tmp = ran1(&idum);
	  if (tmp < prob_u)
	    {
	      z += 1;
	      if (z > 0 && timin && done[z] == 0)
		{
		  dst = op->dat[z];
		  if (timin == 1)
		    add_new_point_to_ds(dst, dst->nx, j - timing[z] - z);
		  else if (timin == 2)
		    {
		      add_new_point_to_ds(dst, timing[z], z);
		      add_new_point_to_ds(dst, j, z);
		    }
		  done[z] = 1;
		}
	    }
	  else if (tmp > prob_u && tmp < p2)
	    {
	      z = (z > 0) ? z - 1 : z;
	    }
	}
      else z = 0;
      if (z == 0)
	{
	  for (k = 0; k <= mlen; k++)
	    {
	      if (done[k] == 1)
		{
		  timing[k] = j;
		  done[k] = 0;
		}
	    }

	}
      if (z != z_1)
	{
	  if (i_1 < j - 1)
	    {
	      add_new_point_to_ds(ds, j-1, z_1);
	      i++;
	    }
	  add_new_point_to_ds(ds, i_1 = j, z_1 = z);
	  i++;
	}
    }
  if (timing) free(timing);
  if (done) free(done);
  return refresh_plot(pr,pr->n_op-1);
}


# define A 0
# define C 1
# define G 2
# define T 3

# define AA 0
# define AC 1
# define AG 2
# define AT 3

# define CA 4
# define CC 5
# define CG 6
# define CT 7

# define GA 8
# define GC 9
# define GG 10
# define GT 11

# define TA 12
# define TC 13
# define TG 14
# define TT 15

# define IN 16 // initialization
# define TP 17 // Terminal AT penalty
# define SC 18 // Symmetry correction

# define INGC 19  // initialization with G or C
# define INAT 20  // initialization with A or T

# define IN_2 21 // 1/2 initialization

// for DNA LNA all ends with _

# define AA_	0
# define AC_ 	1
# define AG_ 	2
# define AT_ 	3

# define A_A_	4
# define A_C_	5
# define A_G_	6
# define A_T_	7

# define CA_ 	8
# define CC_ 	9
# define CG_ 	10
# define CT_ 	11

# define C_A_	12
# define C_C_	13
# define C_G_	14
# define C_T_	15

# define GA_ 	16
# define GC_ 	17
# define GG_ 	18
# define GT_ 	19

# define G_A_	20
# define G_C_	21
# define G_G_	22
# define G_T_	23

# define TA_ 	24
# define TC_ 	25
# define TG_ 	26
# define TT_ 	27

# define T_A_	28
# define T_C_	29
# define T_G_	30
# define T_T_	31

# define _AA_	32
# define _AC_	33
# define _AG_	34
# define _AT_	35

# define _A_A_	36
# define _A_C_	37
# define _A_G_	38
# define _A_T_	39

# define _CA_	40
# define _CC_	41
# define _CG_	42
# define _CT_	43

# define _C_A_	44
# define _C_C_	45
# define _C_G_	46
# define _C_T_	47

# define _GA_	48
# define _GC_	49
# define _GG_	50
# define _GT_	51

# define _G_A_	52
# define _G_C_	53
# define _G_G_	54
# define _G_T_	55

# define _TA_	56
# define _TC_	57
# define _TG_	58
# define _TT_	59

# define _T_A_	60
# define _T_C_	61
# define _T_G_	62
# define _T_T_	63




# define IN_ 64 // initialization
# define TP_ 65 // Terminal AT penalty
# define SC_ 66 // Symmetry correction

# define INGC_ 67 // initialization with GC
# define INAT_ 68 // initialization with AT




float DHMF[32] = {0}, DSMF[32] = {0}, DGMF[32] = {0}; // Mfold 3.1
float DH_RD[32] = {0}, DS_RD[32] = {0}, DG_RD[32] = {0}; // Mfold 3.1
int array_init = 0;

float dsCharge = 0.5;

float D_H[70] = {0}, D_S[70] = {0}, D_G[70] = {0}; // unified parameter santaLucia 1998 PNAS +


void init_NN3_array(float Na, float Mg, float Ti, int verbose, int no_salt_correction, float ldsCharge)
{
  float T0 = 273.15;
  float dss = (no_salt_correction) ? 0 : ldsCharge*0.368*log(0.001*Na+3.2*sqrt(0.001*Mg));
  float R = 1.9872;
  float cor = 1000.0/(R*(T0+Ti)); // convert cal/K-mol in kBT

  DHMF[AA] = DHMF[TT] = -7.9;
  DSMF[AA] = DSMF[TT] = -22.2 + dss;
  DGMF[AA] = DGMF[TT] = DHMF[AA]-(DSMF[AA]*(T0+Ti))/1000;

  DHMF[AT] = -7.2;
  DSMF[AT] = -21.3 + dss;
  DGMF[AT] = DHMF[AT]-(DSMF[AT]*(T0+Ti))/1000;

  DHMF[TA] = -7.2;
  DSMF[TA] = -21.3 + dss;
  DGMF[TA] = DHMF[TA]-(DSMF[TA]*(T0+Ti))/1000;

  DHMF[CA] = DHMF[TG] = -8.5;
  DSMF[CA] = DSMF[TG] = -22.7 + dss;
  DGMF[CA] = DGMF[TG] = DHMF[TG]-(DSMF[TG]*(T0+Ti))/1000;

  DHMF[GT] = DHMF[AC] = -8.4;
  DSMF[GT] = DSMF[AC] = -22.4 + dss;
  DGMF[GT] = DGMF[AC] = DHMF[GT]-(DSMF[GT]*(T0+Ti))/1000;

  DHMF[CT] = DHMF[AG] = -7.8;
  DSMF[CT] = DSMF[AG] = -21.0 + dss;
  DGMF[CT] = DGMF[AG] = DHMF[CT]-(DSMF[CT]*(T0+Ti))/1000;

  DHMF[GA] = DHMF[TC] = -8.2;
  DSMF[GA] = DSMF[TC] = -22.2 + dss;
  DGMF[GA] = DGMF[TC] = DHMF[GA]-(DSMF[GA]*(T0+Ti))/1000;

  DHMF[CG] = -10.6;
  DSMF[CG] = -27.2 + dss;
  DGMF[CG] = DHMF[CG]-(DSMF[CG]*(T0+Ti))/1000;

  DHMF[GC] = -9.8;
  DSMF[GC] = -24.4 + dss;
  DGMF[GC] = DHMF[GC]-(DSMF[GC]*(T0+Ti))/1000;

  DHMF[GG] = DHMF[CC] = -8.0;
  DSMF[GG] = DSMF[CC] = -19.9 + dss;
  DGMF[GG] = DGMF[CC] = DHMF[GG]-(DSMF[GG]*(T0+Ti))/1000;

  DHMF[IN] = 0.2;
  DSMF[IN] = -5.6;// + dss;
  DGMF[IN] = DHMF[IN]-(DSMF[IN]*(T0+Ti))/1000;

  DHMF[TP] = 2.2;
  DSMF[TP] = 6.8;// + dss;
  DGMF[TP] = DHMF[TP]-(DSMF[TP]*(T0+Ti))/1000;

  DHMF[SC] = 0;
  DSMF[SC] = -1.4;// + dss;
  DGMF[SC] = DHMF[SC]-(DSMF[SC]*(T0+Ti))/1000;

  DHMF[INGC] = 0.1;
  DSMF[INGC] = -2.8;// + dss;
  DGMF[INGC] = DHMF[INGC]-(DSMF[INGC]*(T0+Ti))/1000;

  DHMF[INAT] = 2.3;
  DSMF[INAT] = 4.1;// + dss;
  DGMF[INAT] = DHMF[INAT]-(DSMF[INAT]*(T0+Ti))/1000;

  DHMF[IN_2] = 0.1;
  DSMF[IN_2] = -2.85;
  DGMF[IN_2] = DHMF[IN_2]-(DSMF[IN_2]*(T0+Ti))/1000;

  if (verbose)
      {
          win_printf("dss = %g\n"
             "AA & TT: DH = %g DS = %g dG = %g\n"
             "     AT: DH = %g DS = %g dG = %g\n"
             "     TA: DH = %g DS = %g dG = %g\n"
             "CA & TG: DH = %g DS = %g dG = %g\n"
             "GT & AC: DH = %g DS = %g dG = %g\n"
             "CT & AG: DH = %g DS = %g dG = %g\n"
             "GA & TC: DH = %g DS = %g dG = %g\n"
             "     CG: DH = %g DS = %g dG = %g\n"
             "     GC: DH = %g DS = %g dG = %g\n"
             "GG & CC: DH = %g DS = %g dG = %g\n"
	     "Cor GC DH = %g DS = %g dG = %g\n"
	     "Cor AT DH = %g DS = %g dG = %g\n"
	     "Cor IN/2 DH = %g DS = %g dG = %g\n"
	     ,dss
             , DHMF[AA], DSMF[AA], DGMF[AA]
             , DHMF[AT], DSMF[AT], DGMF[AT]
             , DHMF[TA], DSMF[TA], DGMF[TA]
             , DHMF[CA], DSMF[CA], DGMF[CA]
             , DHMF[GT], DSMF[GT], DGMF[GT]
             , DHMF[CT], DSMF[CT], DGMF[CT]
             , DHMF[GA], DSMF[GA], DGMF[GA]
             , DHMF[CG], DSMF[CG], DGMF[CG]
             , DHMF[GC], DSMF[GC], DGMF[GC]
             , DHMF[GG], DSMF[GG], DGMF[GG]
	     , DHMF[INGC], DSMF[INGC], DGMF[INGC]
	     , DHMF[INAT], DSMF[INAT], DGMF[INAT]
  	     , DHMF[IN_2], DSMF[IN_2], DGMF[IN_2]);
          printf("dss = %6.4f\n"
                 "AA & TT: DH = %6.4f DS = %6.4f dG = %6.4f = %6.4f k_BT\n"
                 "     AT: DH = %6.4f DS = %6.4f dG = %6.4f = %6.4f k_BT\n"
                 "     TA: DH = %6.4f DS = %6.4f dG = %6.4f = %6.4f k_BT\n"
                 "CA & TG: DH = %6.4f DS = %6.4f dG = %6.4f = %6.4f k_BT\n"
                 "GT & AC: DH = %6.4f DS = %6.4f dG = %6.4f = %6.4f k_BT\n"
                 "CT & AG: DH = %6.4f DS = %6.4f dG = %6.4f = %6.4f k_BT\n"
                 "GA & TC: DH = %6.4f DS = %6.4f dG = %6.4f = %6.4f k_BT\n"
                 "     CG: DH = %6.4f DS = %6.4f dG = %6.4f = %6.4f k_BT\n"
                 "     GC: DH = %6.4f DS = %6.4f dG = %6.4f = %6.4f k_BT\n"
                 "GG & CC: DH = %6.4f DS = %6.4f dG = %6.4f = %6.4f k_BT\n"
                 "Cor GC DH = %6.4f DS = %6.4f dG = %6.4f = %6.4f k_BT\n"
                 "Cor AT DH = %6.4f DS = %6.4f dG = %6.4f = %6.4f k_BT\n"
                 "Cor IN/2 DH = %6.4f DS = %6.4f dG = %6.4f = %6.4f k_BT\n"
                 ,dss
                 , DHMF[AA], DSMF[AA], DGMF[AA], cor*DGMF[AA]
                 , DHMF[AT], DSMF[AT], DGMF[AT], cor*DGMF[AT]
                 , DHMF[TA], DSMF[TA], DGMF[TA], cor*DGMF[TA]
                 , DHMF[CA], DSMF[CA], DGMF[CA], cor*DGMF[CA]
                 , DHMF[GT], DSMF[GT], DGMF[GT], cor*DGMF[GT]
                 , DHMF[CT], DSMF[CT], DGMF[CT], cor*DGMF[CT]
                 , DHMF[GA], DSMF[GA], DGMF[GA], cor*DGMF[GA]
                 , DHMF[CG], DSMF[CG], DGMF[CG], cor*DGMF[CG]
                 , DHMF[GC], DSMF[GC], DGMF[GC], cor*DGMF[GC]
                 , DHMF[GG], DSMF[GG], DGMF[GG], cor*DGMF[GG]
                 , DHMF[INGC], DSMF[INGC], DGMF[INGC], cor*DGMF[INGC]
                 , DHMF[INAT], DSMF[INAT], DGMF[INAT], cor*DGMF[INAT]
                 , DHMF[IN_2], DSMF[IN_2], DGMF[IN_2], cor*DGMF[IN_2]);
          fflush(stdout);
      }

  DHMF[SC] = 0.0;
  DSMF[SC] = -1.4;// + dss;
  DGMF[SC] = DHMF[SC]-(DSMF[SC]*(T0+Ti))/1000;
  return;
}

void init_Mfold31_array(float Na, float Mg, float Ti, int verbose, int no_salt_correction, float ldsCharge)
{
  float T0 = 273.15;
  float dss = (no_salt_correction) ? 0 : ldsCharge*0.368*log(0.001*Na+3.2*sqrt(0.001*Mg));

  DHMF[AA] = DHMF[TT] = -7.9;
  DSMF[AA] = DSMF[TT] = -22.25 + dss;
  DGMF[AA] = DGMF[TT] = DHMF[AA]-(DSMF[AA]*(T0+Ti))/1000;

  DHMF[AT] = -7.2;
  DSMF[AT] = -20.375 + dss;
  DGMF[AT] = DHMF[AT]-(DSMF[AT]*(T0+Ti))/1000;

  DHMF[TA] = -7.2;
  DSMF[TA] = -21.35 + dss;
  DGMF[TA] = DHMF[TA]-(DSMF[TA]*(T0+Ti))/1000;

  DHMF[CA] = DHMF[TG] = -8.5;
  DSMF[CA] = DSMF[TG] = -22.725 + dss;
  DGMF[CA] = DGMF[TG] = DHMF[TG]-(DSMF[TG]*(T0+Ti))/1000;

  DHMF[GT] = DHMF[AC] = -8.4;
  DSMF[GT] = DSMF[AC] = -22.45 + dss;
  DGMF[GT] = DGMF[AC] = DHMF[GT]-(DSMF[GT]*(T0+Ti))/1000;

  DHMF[CT] = DHMF[AG] = -7.8;
  DSMF[CT] = DSMF[AG] = -21.025 + dss;
  DGMF[CT] = DGMF[AG] = DHMF[CT]-(DSMF[CT]*(T0+Ti))/1000;

  DHMF[GA] = DHMF[TC] = -8.2;
  DSMF[GA] = DSMF[TC] = -22.25 + dss;
  DGMF[GA] = DGMF[TC] = DHMF[GA]-(DSMF[GA]*(T0+Ti))/1000;

  DHMF[CG] = -10.6;
  DSMF[CG] = -27.2 + dss;
  DGMF[CG] = DHMF[CG]-(DSMF[CG]*(T0+Ti))/1000;

  DHMF[GC] = -9.8;
  DSMF[GC] = -24.375 + dss;
  DGMF[GC] = DHMF[GC]-(DSMF[GC]*(T0+Ti))/1000;

  DHMF[GG] = DHMF[CC] = -8.0;
  DSMF[GG] = DSMF[CC] = -19.85 + dss;
  DGMF[GG] = DGMF[CC] = DHMF[GG]-(DSMF[GG]*(T0+Ti))/1000;

  DHMF[IN] = 0.2;
  DSMF[IN] = -5.7;// + dss;
  DGMF[IN] = DHMF[IN]-(DSMF[IN]*(T0+Ti))/1000;

  DHMF[TP] = 2.2;
  DSMF[TP] = 6.8;// + dss;
  DGMF[TP] = DHMF[TP]-(DSMF[TP]*(T0+Ti))/1000;

  DHMF[SC] = 0;
  DSMF[SC] = -1.4;// + dss;
  DGMF[SC] = DHMF[SC]-(DSMF[SC]*(T0+Ti))/1000;

  DHMF[INGC] = 0.1;
  DSMF[INGC] = -2.8;// + dss;
  DGMF[INGC] = DHMF[INGC]-(DSMF[INGC]*(T0+Ti))/1000;

  DHMF[INAT] = 2.3;
  DSMF[INAT] = 4.1;// + dss;
  DGMF[INAT] = DHMF[INAT]-(DSMF[INAT]*(T0+Ti))/1000;

  DHMF[IN_2] = 0.1;
  DSMF[IN_2] = -2.85;
  DGMF[IN_2] = DHMF[IN_2]-(DSMF[IN_2]*(T0+Ti))/1000;

  if (verbose)
          win_printf("dss = %g\n"
             "AA & TT: DH = %g DS = %g dG = %g\n"
             "     AT: DH = %g DS = %g dG = %g\n"
             "     TA: DH = %g DS = %g dG = %g\n"
             "CA & TG: DH = %g DS = %g dG = %g\n"
             "GT & AC: DH = %g DS = %g dG = %g\n"
             "CT & AG: DH = %g DS = %g dG = %g\n"
             "GA & TC: DH = %g DS = %g dG = %g\n"
             "     CG: DH = %g DS = %g dG = %g\n"
             "     GC: DH = %g DS = %g dG = %g\n"
             "GG & CC: DH = %g DS = %g dG = %g\n"
	     "Cor GC DH = %g DS = %g dG = %g\n"
	     "Cor AT DH = %g DS = %g dG = %g\n"
	     "Cor IN/2 DH = %g DS = %g dG = %g\n"
	     ,dss
             , DHMF[AA], DSMF[AA], DGMF[AA]
             , DHMF[AT], DSMF[AT], DGMF[AT]
             , DHMF[TA], DSMF[TA], DGMF[TA]
             , DHMF[CA], DSMF[CA], DGMF[CA]
             , DHMF[GT], DSMF[GT], DGMF[GT]
             , DHMF[CT], DSMF[CT], DGMF[CT]
             , DHMF[GA], DSMF[GA], DGMF[GA]
             , DHMF[CG], DSMF[CG], DGMF[CG]
             , DHMF[GC], DSMF[GC], DGMF[GC]
             , DHMF[GG], DSMF[GG], DGMF[GG]
	     , DHMF[INGC], DSMF[INGC], DGMF[INGC]
	     , DHMF[INAT], DSMF[INAT], DGMF[INAT]
  	     , DHMF[IN_2], DSMF[IN_2], DGMF[IN_2]);


  DHMF[SC] = 0.0;
  DSMF[SC] = -1.4;// + dss;
  DGMF[SC] = DHMF[SC]-(DSMF[SC]*(T0+Ti))/1000;
  return;
}


void init_RNA_DNA_array(float Na, float Mg, float Ti)
{
  float T0 = 273.15;
  float dss = 0.368*log(0.001*Na+3.2*sqrt(0.001*Mg));

  DH_RD[AA] = -7.8;
  DS_RD[AA] = -21.9 + dss;
  DG_RD[AA] = DH_RD[AA]-(DS_RD[AA]*(T0+Ti))/1000;

  DH_RD[AC] = -5.9;
  DS_RD[AC] = -12.3 + dss;
  DG_RD[AC] = DH_RD[AC]-(DS_RD[AC]*(T0+Ti))/1000;

  DH_RD[AG] = -9.1;
  DS_RD[AG] = -23.5 + dss;
  DG_RD[AG] = DH_RD[AG]-(DS_RD[AG]*(T0+Ti))/1000;

  DH_RD[AT] = -8.3;
  DS_RD[AT] = -23.9 + dss;
  DG_RD[AT] = DH_RD[AT]-(DS_RD[AT]*(T0+Ti))/1000;

  DH_RD[CA] = -9.0;
  DS_RD[CA] = -26.1 + dss;
  DG_RD[CA] = DH_RD[CA]-(DS_RD[CA]*(T0+Ti))/1000;

  DH_RD[CC] = -9.3;
  DS_RD[CC] = -23.2 + dss;
  DG_RD[CC] = DH_RD[CC]-(DS_RD[CC]*(T0+Ti))/1000;

  DH_RD[CG] = -16.3;
  DS_RD[CG] = -47.1 + dss;
  DG_RD[CG] = DH_RD[CG]-(DS_RD[CG]*(T0+Ti))/1000;

  DH_RD[CT] = -7.0;
  DS_RD[CT] = -19.7 + dss;
  DG_RD[CT] = DH_RD[CT]-(DS_RD[CT]*(T0+Ti))/1000;

  DH_RD[GA] = -5.5;
  DS_RD[GA] = -13.5 + dss;
  DG_RD[GA] = DH_RD[GA]-(DS_RD[GA]*(T0+Ti))/1000;

  DH_RD[GC] = -8.0;
  DS_RD[GC] = -17.1 + dss;
  DG_RD[GC] = DH_RD[GC]-(DS_RD[GC]*(T0+Ti))/1000;

  DH_RD[GG] = -12.8;
  DS_RD[GG] = -31.9 + dss;
  DG_RD[GG] = DH_RD[GG]-(DS_RD[GG]*(T0+Ti))/1000;

  DH_RD[GT] = -7.8;
  DS_RD[GT] = -21.6 + dss;
  DG_RD[GT] = DH_RD[GT]-(DS_RD[GT]*(T0+Ti))/1000;

  DH_RD[TA] = -7.8;
  DS_RD[TA] = -23.2 + dss;
  DG_RD[TA] = DH_RD[TA]-(DS_RD[TA]*(T0+Ti))/1000;

  DH_RD[TC] = -8.6;
  DS_RD[TC] = -22.9 + dss;
  DG_RD[TC] = DH_RD[TC]-(DS_RD[TC]*(T0+Ti))/1000;

  DH_RD[TG] = -10.4;
  DS_RD[TG] = -28.4 + dss;
  DG_RD[TG] = DH_RD[TG]-(DS_RD[TG]*(T0+Ti))/1000;

  DH_RD[TT] = -11.5;
  DS_RD[TT] = -36.4 + dss;
  DG_RD[TT] = DH_RD[TT]-(DS_RD[TT]*(T0+Ti))/1000;

  //initiation DH 1.9, DS -3.9

  DH_RD[INGC] = 0.1;
  DS_RD[INGC] = -2.8 + dss;
  DG_RD[INGC] = DH_RD[INGC]-(DS_RD[INGC]*(T0+Ti))/1000;

  DH_RD[INAT] = 2.3;
  DS_RD[INAT] = 4.1 + dss;
  DG_RD[INAT] = DH_RD[INAT]-(DS_RD[INAT]*(T0+Ti))/1000;

  /*
  win_printf("dss = %g\nFor GC DH = %g DS = %g dG = %g\n"
	     "Cor GC DH = %g DS = %g dG = %g\n"
	     "Cor AT DH = %g DS = %g dG = %g\n"
	     ,dss, DH_RD[GC], DS_RD[GC], DG_RD[GC]
	     , DH_RD[INGC], DS_RD[INGC], DG_RD[INGC]
	     , DH_RD[INAT], DS_RD[INAT], DG_RD[INAT]);





  */

  win_printf("dss = %g\n"
             "AA : DH = %g DS = %g dG = %g AC : DH = %g DS = %g dG = %g\n"
             "AG : DH = %g DS = %g dG = %g AT : DH = %g DS = %g dG = %g\n"
             "CA : DH = %g DS = %g dG = %g CC : DH = %g DS = %g dG = %g\n"
             "CG : DH = %g DS = %g dG = %g CT : DH = %g DS = %g dG = %g\n"
             "GA : DH = %g DS = %g dG = %g GC : DH = %g DS = %g dG = %g\n"
             "GG : DH = %g DS = %g dG = %g GT : DH = %g DS = %g dG = %g\n"
             "TA : DH = %g DS = %g dG = %g TC : DH = %g DS = %g dG = %g\n"
             "TG : DH = %g DS = %g dG = %g TT : DH = %g DS = %g dG = %g\n"
	     "Cor GC DH = %g DS = %g dG = %g Cor AT DH = %g DS = %g dG = %g\n"
	     ,dss
             ,DH_RD[AA], DS_RD[AA], DG_RD[AA]
             ,DH_RD[AC], DS_RD[AC], DG_RD[AC]
             ,DH_RD[AG], DS_RD[AG], DG_RD[AG]
             ,DH_RD[AT], DS_RD[AT], DG_RD[AT]
             ,DH_RD[CA], DS_RD[CA], DG_RD[CA]
             ,DH_RD[CC], DS_RD[CC], DG_RD[CC]
             ,DH_RD[CG], DS_RD[CG], DG_RD[CG]
             ,DH_RD[CT], DS_RD[CT], DG_RD[CT]
             ,DH_RD[GA], DS_RD[GA], DG_RD[GA]
             ,DH_RD[GC], DS_RD[GC], DG_RD[GC]
             ,DH_RD[GG], DS_RD[GG], DG_RD[GG]
             ,DH_RD[GT], DS_RD[GT], DG_RD[GT]
             ,DH_RD[TA], DS_RD[TA], DG_RD[TA]
             ,DH_RD[TC], DS_RD[TC], DG_RD[TC]
             ,DH_RD[TG], DS_RD[TG], DG_RD[TG]
             ,DH_RD[TT], DS_RD[TT], DG_RD[TT]
	     , DH_RD[INGC], DS_RD[INGC], DG_RD[INGC]
	     , DH_RD[INAT], DS_RD[INAT], DG_RD[INAT]);



  DH_RD[SC] = 0.0;
  DS_RD[SC] = -1.4 + dss;
  DG_RD[SC] = DH_RD[SC]-(DS_RD[SC]*(T0+Ti))/1000;
  return;
}





 void  init_LNA_array(float Na, float Mg, float Ti)
 {
  float T0 = 273.15;
  float dss = 0.368*log(0.001*Na+3.2*sqrt(0.001*Mg));
  // cgttga
  // Dh = 0.2 + 0 -10.6 -8.4 -7.6 -8.5 -8.2 +2.2 = -40.9
  // dS = -5.7 +0  -27.2 -22.4 -21.3 -22.7 -22.2 +6.9 = -114.6
  // for LNA

  D_H[AA_] = D_H[TT_] = -7.9;
  D_S[AA_] = D_S[TT_] = -22.25 + dss;
  D_G[AA_] = D_G[TT_] = D_H[AA_] - (D_S[AA_] *(T0+Ti))/1000;

  D_H[AT_] = -7.2;
  D_S[AT_] = -20.375 + dss;
  D_G[AT_] = D_H[AT_] - (D_S[AT_]*(T0+Ti))/1000;

  D_H[TA_] = -7.2;
  D_S[TA_] = -21.35 + dss;
  D_G[TA_] = D_H[TA_] - (D_S[TA_]*(T0+Ti))/1000;

  D_H[CA_] = D_H[TG_] = -8.5;
  D_S[CA_] = D_S[TG_] = -22.725 + dss;
  D_G[CA_] = D_G[TG_] = D_H[CA_] - (D_S[CA_]*(T0+Ti))/1000;

  D_H[GT_] = D_H[AC_] = -8.4;
  D_S[GT_] = D_S[AC_] = -22.45 + dss;
  D_G[GT_] = D_G[AC_] = D_H[GT_] - (D_S[GT_]*(T0+Ti))/1000;

  D_H[CT_] = D_H[AG_] = -7.8;
  D_S[CT_] = D_S[AG_] = -21.025 + dss;
  D_G[CT_] = D_G[AG_] = D_H[CT_] - (D_S[CT_]*(T0+Ti))/1000;

  D_H[GA_] = D_H[TC_] = -8.2;
  D_S[GA_] = D_S[TC_] = -22.25 + dss;
  D_G[GA_] = D_G[TC_] = D_H[GA_] - (D_S[GA_]*(T0+Ti))/1000;

  D_H[CG_] = -10.6;
  D_S[CG_] = -27.2 + dss;
  D_G[CG_] = D_H[CG_] - (D_S[CG_]*(T0+Ti))/1000;

  D_H[GC_] = -9.8;
  D_S[GC_] = -24.375 + dss;
  D_G[GC_] = D_H[GC_] - (D_S[GC_]*(T0+Ti))/1000;

  D_H[GG_] = D_H[CC_] = -8.0;
  D_S[GG_] = D_S[CC_] = -19.85 + dss;
  D_G[GG_] = D_G[CC_] = D_H[GG_] - (D_S[GG_]*(T0+Ti))/1000;


  win_printf("dss = %g\n"
             "aa : DH = %g DS = %g dG = %g ac : DH = %g DS = %g dG = %g\n"
             "ag : DH = %g DS = %g dG = %g at : DH = %g DS = %g dG = %g\n"
             "ca : DH = %g DS = %g dG = %g cc : DH = %g DS = %g dG = %g\n"
             "cg : DH = %g DS = %g dG = %g ct : DH = %g DS = %g dG = %g\n"
             "ga : DH = %g DS = %g dG = %g gc : DH = %g DS = %g dG = %g\n"
             "gg : DH = %g DS = %g dG = %g gt : DH = %g DS = %g dG = %g\n"
             "ta : DH = %g DS = %g dG = %g tc : DH = %g DS = %g dG = %g\n"
             "tg : DH = %g DS = %g dG = %g tt : DH = %g DS = %g dG = %g\n"
	     ,dss
             ,D_H[AA_], D_S[AA_], D_G[AA_]
             ,D_H[AC_], D_S[AC_], D_G[AC_]
             ,D_H[AG_], D_S[AG_], D_G[AG_]
             ,D_H[AT_], D_S[AT_], D_G[AT_]
             ,D_H[CA_], D_S[CA_], D_G[CA_]
             ,D_H[CC_], D_S[CC_], D_G[CC_]
             ,D_H[CG_], D_S[CG_], D_G[CG_]
             ,D_H[CT_], D_S[CT_], D_G[CT_]
             ,D_H[GA_], D_S[GA_], D_G[GA_]
             ,D_H[GC_], D_S[GC_], D_G[GC_]
             ,D_H[GG_], D_S[GG_], D_G[GG_]
             ,D_H[GT_], D_S[GT_], D_G[GT_]
             ,D_H[TA_], D_S[TA_], D_G[TA_]
             ,D_H[TC_], D_S[TC_], D_G[TC_]
             ,D_H[TG_], D_S[TG_], D_G[TG_]
             ,D_H[TT_], D_S[TT_], D_G[TT_]);


  D_H[_A_A_] = -9.991;
  D_S[_A_A_] = -27.175 + dss;
  D_G[_A_A_] = D_H[_A_A_] - (D_S[_A_A_]*(T0+Ti))/1000;

  D_H[_A_C_] = -11.389;
  D_S[_A_C_] = -28.963 + dss;
  D_G[_A_C_] = D_H[_A_C_] - (D_S[_A_C_]*(T0+Ti))/1000;

  D_H[_A_G_] = -12.793;
  D_S[_A_G_] = -31.607 + dss;
  D_G[_A_G_] = D_H[_A_G_] - (D_S[_A_G_]*(T0+Ti))/1000;

  D_H[_A_T_] = -14.703;
  D_S[_A_T_] = -40.750 + dss;
  D_G[_A_T_] = D_H[_A_T_] - (D_S[_A_T_]*(T0+Ti))/1000;

  D_H[_C_A_] = -14.177;
  D_S[_C_A_] = -35.498 + dss;
  D_G[_C_A_] = D_H[_C_A_] - (D_S[_C_A_]*(T0+Ti))/1000;

  D_H[_C_C_] = -15.399;
  D_S[_C_C_] = -36.375 + dss;
  D_G[_C_C_] = D_H[_C_C_] - (D_S[_C_C_]*(T0+Ti))/1000;

  D_H[_C_G_] = -14.558;
  D_S[_C_G_] = -35.239 + dss;
  D_G[_C_G_] = D_H[_C_G_] - (D_S[_C_G_]*(T0+Ti))/1000;

  D_H[_C_T_] = -15.737;
  D_S[_C_T_] = -41.218 + dss;
  D_G[_C_T_] = D_H[_C_T_] - (D_S[_C_T_]*(T0+Ti))/1000;

  D_H[_G_A_] = -13.959;
  D_S[_G_A_] = -35.097 + dss;
  D_G[_G_A_] = D_H[_G_A_] - (D_S[_G_A_]*(T0+Ti))/1000;

  D_H[_G_C_] = -16.109;
  D_S[_G_C_] = -40.738 + dss;
  D_G[_G_C_] = D_H[_G_C_] - (D_S[_G_C_]*(T0+Ti))/1000;

  D_H[_G_G_] = -13.022;
  D_S[_G_G_] = -29.673 + dss;
  D_G[_G_G_] = D_H[_G_G_] - (D_S[_G_G_]*(T0+Ti))/1000;

  D_H[_G_T_] = -17.361;
  D_S[_G_T_] = -45.858 + dss;
  D_G[_G_T_] = D_H[_G_T_] - (D_S[_G_T_]*(T0+Ti))/1000;

  D_H[_T_A_] = -10.318;
  D_S[_T_A_] = -26.108 + dss;
  D_G[_T_A_] = D_H[_T_A_] - (D_S[_T_A_]*(T0+Ti))/1000;

  D_H[_T_C_] = -9.166;
  D_S[_T_C_] = -21.535 + dss;
  D_G[_T_C_] = D_H[_T_C_] - (D_S[_T_C_]*(T0+Ti))/1000;

  D_H[_T_G_] = -10.046;
  D_S[_T_G_] = -22.591 + dss;
  D_G[_T_G_] = D_H[_T_G_] - (D_S[_T_G_]*(T0+Ti))/1000;

  D_H[_T_T_] = -10.419;
  D_S[_T_T_] = -27.683 + dss;
  D_G[_T_T_] = D_H[_T_T_] - (D_S[_T_T_]*(T0+Ti))/1000;



  win_printf("dss = %g\n"
             "AA : DH = %g DS = %g dG = %g AC : DH = %g DS = %g dG = %g\n"
             "AG : DH = %g DS = %g dG = %g AT : DH = %g DS = %g dG = %g\n"
             "CA : DH = %g DS = %g dG = %g CC : DH = %g DS = %g dG = %g\n"
             "CG : DH = %g DS = %g dG = %g CT : DH = %g DS = %g dG = %g\n"
             "GA : DH = %g DS = %g dG = %g GC : DH = %g DS = %g dG = %g\n"
             "GG : DH = %g DS = %g dG = %g GT : DH = %g DS = %g dG = %g\n"
             "TA : DH = %g DS = %g dG = %g TC : DH = %g DS = %g dG = %g\n"
             "TG : DH = %g DS = %g dG = %g TT : DH = %g DS = %g dG = %g\n"
	     ,dss
             ,D_H[_A_A_], D_S[_A_A_], D_G[_A_A_]
             ,D_H[_A_C_], D_S[_A_C_], D_G[_A_C_]
             ,D_H[_A_G_], D_S[_A_G_], D_G[_A_G_]
             ,D_H[_A_T_], D_S[_A_T_], D_G[_A_T_]
             ,D_H[_C_A_], D_S[_C_A_], D_G[_C_A_]
             ,D_H[_C_C_], D_S[_C_C_], D_G[_C_C_]
             ,D_H[_C_G_], D_S[_C_G_], D_G[_C_G_]
             ,D_H[_C_T_], D_S[_C_T_], D_G[_C_T_]
             ,D_H[_G_A_], D_S[_G_A_], D_G[_G_A_]
             ,D_H[_G_C_], D_S[_G_C_], D_G[_G_C_]
             ,D_H[_G_G_], D_S[_G_G_], D_G[_G_G_]
             ,D_H[_G_T_], D_S[_G_T_], D_G[_G_T_]
             ,D_H[_T_A_], D_S[_T_A_], D_G[_T_A_]
             ,D_H[_T_C_], D_S[_T_C_], D_G[_T_C_]
             ,D_H[_T_G_], D_S[_T_G_], D_G[_T_G_]
             ,D_H[_T_T_], D_S[_T_T_], D_G[_T_T_]);


  D_H[_AA_] = -7.193;
  D_S[_AA_] = -19.723 + dss;
  D_G[_AA_] = D_H[_AA_] - (D_S[_AA_]*(T0+Ti))/1000;

  D_H[_AC_] = -7.269;
  D_S[_AC_] = -18.336 + dss;
  D_G[_AC_] = D_H[_AC_] - (D_S[_AC_]*(T0+Ti))/1000;

  D_H[_AG_] = -7.536;
  D_S[_AG_] = -18.387 + dss;
  D_G[_AG_] = D_H[_AG_] - (D_S[_AG_]*(T0+Ti))/1000;

  D_H[_AT_] = -4.918;
  D_S[_AT_] = -12.943 + dss;
  D_G[_AT_] = D_H[_AT_] - (D_S[_AT_]*(T0+Ti))/1000;

  D_H[_CA_] = -7.451;
  D_S[_CA_] = -18.380 + dss;
  D_G[_CA_] = D_H[_CA_] - (D_S[_CA_]*(T0+Ti))/1000;

  D_H[_CC_] = -5.904;
  D_S[_CC_] = -11.904 + dss;
  D_G[_CC_] = D_H[_CC_] - (D_S[_CC_]*(T0+Ti))/1000;

  D_H[_CG_] = -9.815;
  D_S[_CG_] = -23.491 + dss;
  D_G[_CG_] = D_H[_CG_] - (D_S[_CG_]*(T0+Ti))/1000;

  D_H[_CT_] = -7.092;
  D_S[_CT_] = -16.825 + dss;
  D_G[_CT_] = D_H[_CT_] - (D_S[_CT_]*(T0+Ti))/1000;

  D_H[_GA_] = -5.038;
  D_S[_GA_] = -11.656 + dss;
  D_G[_GA_] = D_H[_GA_] - (D_S[_GA_]*(T0+Ti))/1000;

  D_H[_GC_] = -10.160;
  D_S[_GC_] = -24.651 + dss;
  D_G[_GC_] = D_H[_GC_] - (D_S[_GC_]*(T0+Ti))/1000;

  D_H[_GG_] = -10.844;
  D_S[_GG_] = -26.580 + dss;
  D_G[_GG_] = D_H[_GG_] - (D_S[_GG_]*(T0+Ti))/1000;

  D_H[_GT_] = -8.612;
  D_S[_GT_] = -22.327 + dss;
  D_G[_GT_] = D_H[_GT_] - (D_S[_GT_]*(T0+Ti))/1000;

  D_H[_TA_] = -7.246;
  D_S[_TA_] = -19.738 + dss;
  D_G[_TA_] = D_H[_TA_] - (D_S[_TA_]*(T0+Ti))/1000;

  D_H[_TC_] = -6.307;
  D_S[_TC_] = -15.515 + dss;
  D_G[_TC_] = D_H[_TC_] - (D_S[_TC_]*(T0+Ti))/1000;

  D_H[_TG_] = -10.040;
  D_S[_TG_] = -25.744 + dss;
  D_G[_TG_] = D_H[_TG_] - (D_S[_TG_]*(T0+Ti))/1000;

  D_H[_TT_] = -6.372;
  D_S[_TT_] = -16.902 + dss;
  D_G[_TT_] = D_H[_TT_] - (D_S[_TT_]*(T0+Ti))/1000;


  win_printf("dss = %g\n"
             "Aa : DH = %g DS = %g dG = %g Ac : DH = %g DS = %g dG = %g\n"
             "Ag : DH = %g DS = %g dG = %g At : DH = %g DS = %g dG = %g\n"
             "Ca : DH = %g DS = %g dG = %g Cc : DH = %g DS = %g dG = %g\n"
             "Cg : DH = %g DS = %g dG = %g Ct : DH = %g DS = %g dG = %g\n"
             "Ga : DH = %g DS = %g dG = %g Gc : DH = %g DS = %g dG = %g\n"
             "Gg : DH = %g DS = %g dG = %g Gt : DH = %g DS = %g dG = %g\n"
             "Ta : DH = %g DS = %g dG = %g Tc : DH = %g DS = %g dG = %g\n"
             "Tg : DH = %g DS = %g dG = %g Tt : DH = %g DS = %g dG = %g\n"
	     ,dss
             ,D_H[_AA_], D_S[_AA_], D_G[_AA_]
             ,D_H[_AC_], D_S[_AC_], D_G[_AC_]
             ,D_H[_AG_], D_S[_AG_], D_G[_AG_]
             ,D_H[_AT_], D_S[_AT_], D_G[_AT_]
             ,D_H[_CA_], D_S[_CA_], D_G[_CA_]
             ,D_H[_CC_], D_S[_CC_], D_G[_CC_]
             ,D_H[_CG_], D_S[_CG_], D_G[_CG_]
             ,D_H[_CT_], D_S[_CT_], D_G[_CT_]
             ,D_H[_GA_], D_S[_GA_], D_G[_GA_]
             ,D_H[_GC_], D_S[_GC_], D_G[_GC_]
             ,D_H[_GG_], D_S[_GG_], D_G[_GG_]
             ,D_H[_GT_], D_S[_GT_], D_G[_GT_]
             ,D_H[_TA_], D_S[_TA_], D_G[_TA_]
             ,D_H[_TC_], D_S[_TC_], D_G[_TC_]
             ,D_H[_TG_], D_S[_TG_], D_G[_TG_]
             ,D_H[_TT_], D_S[_TT_], D_G[_TT_]);



  D_H[A_A_] = -6.908;
  D_S[A_A_] = -18.135 + dss;
  D_G[A_A_] = D_H[A_A_] - (D_S[A_A_]*(T0+Ti))/1000;

  D_H[A_C_] = -5.510;
  D_S[A_C_] = -11.824 + dss;
  D_G[A_C_] = D_H[A_C_] - (D_S[A_C_]*(T0+Ti))/1000;

  D_H[A_G_] = -9.000;
  D_S[A_G_] = -22.826 + dss;
  D_G[A_G_] = D_H[A_G_] - (D_S[A_G_]*(T0+Ti))/1000;

  D_H[A_T_] = -5.384;
  D_S[A_T_] = -13.537 + dss;
  D_G[A_T_] = D_H[A_T_] - (D_S[A_T_]*(T0+Ti))/1000;

  D_H[C_A_] = -7.142;
  D_S[C_A_] = -18.333 + dss;
  D_G[C_A_] = D_H[C_A_] - (D_S[C_A_]*(T0+Ti))/1000;

  D_H[C_C_] = -5.937;
  D_S[C_C_] = -12.335 + dss;
  D_G[C_C_] = D_H[C_C_] - (D_S[C_C_]*(T0+Ti))/1000;

  D_H[C_G_] = -10.876;
  D_S[C_G_] = -27.918 + dss;
  D_G[C_G_] = D_H[C_G_] - (D_S[C_G_]*(T0+Ti))/1000;

  D_H[C_T_] = -9.471;
  D_S[C_T_] = -25.070 + dss;
  D_G[C_T_] = D_H[C_T_] - (D_S[C_T_]*(T0+Ti))/1000;

  D_H[G_A_] = -7.756;
  D_S[G_A_] = -19.302 + dss;
  D_G[G_A_] = D_H[G_A_] - (D_S[G_A_]*(T0+Ti))/1000;

  D_H[G_C_] = -10.725;
  D_S[G_C_] = -25.511 + dss;
  D_G[G_C_] = D_H[G_C_] - (D_S[G_C_]*(T0+Ti))/1000;

  D_H[G_G_] = -8.943;
  D_S[G_G_] = -20.833 + dss;
  D_G[G_G_] = D_H[G_G_] - (D_S[G_G_]*(T0+Ti))/1000;

  D_H[G_T_] = -9.035;
  D_S[G_T_] = -22.742 + dss;
  D_G[G_T_] = D_H[G_T_] - (D_S[G_T_]*(T0+Ti))/1000;

  D_H[T_A_] = -5.609;
  D_S[T_A_] = -16.019 + dss;
  D_G[T_A_] = D_H[T_A_] - (D_S[T_A_]*(T0+Ti))/1000;

  D_H[T_C_] = -7.591;
  D_S[T_C_] = -19.031 + dss;
  D_G[T_C_] = D_H[T_C_] - (D_S[T_C_]*(T0+Ti))/1000;

  D_H[T_G_] = -6.335;
  D_S[T_G_] = -15.537 + dss;
  D_G[T_G_] = D_H[T_G_] - (D_S[T_G_]*(T0+Ti))/1000;

  D_H[T_T_] = -5.574;
  D_S[T_T_] = -14.149 + dss;
  D_G[T_T_] = D_H[T_T_] - (D_S[T_T_]*(T0+Ti))/1000;

  win_printf("dss = %g\n"
             "aA : DH = %g DS = %g dG = %g aC : DH = %g DS = %g dG = %g\n"
             "aG : DH = %g DS = %g dG = %g aT : DH = %g DS = %g dG = %g\n"
             "cA : DH = %g DS = %g dG = %g cC : DH = %g DS = %g dG = %g\n"
             "cG : DH = %g DS = %g dG = %g cT : DH = %g DS = %g dG = %g\n"
             "gA : DH = %g DS = %g dG = %g gC : DH = %g DS = %g dG = %g\n"
             "gG : DH = %g DS = %g dG = %g gT : DH = %g DS = %g dG = %g\n"
             "tA : DH = %g DS = %g dG = %g tC : DH = %g DS = %g dG = %g\n"
             "tG : DH = %g DS = %g dG = %g tT : DH = %g DS = %g dG = %g\n"
	     ,dss
             ,D_H[A_A_], D_S[A_A_], D_G[A_A_]
             ,D_H[A_C_], D_S[A_C_], D_G[A_C_]
             ,D_H[A_G_], D_S[A_G_], D_G[A_G_]
             ,D_H[A_T_], D_S[A_T_], D_G[A_T_]
             ,D_H[C_A_], D_S[C_A_], D_G[C_A_]
             ,D_H[C_C_], D_S[C_C_], D_G[C_C_]
             ,D_H[C_G_], D_S[C_G_], D_G[C_G_]
             ,D_H[C_T_], D_S[C_T_], D_G[C_T_]
             ,D_H[G_A_], D_S[G_A_], D_G[G_A_]
             ,D_H[G_C_], D_S[G_C_], D_G[G_C_]
             ,D_H[G_G_], D_S[G_G_], D_G[G_G_]
             ,D_H[G_T_], D_S[G_T_], D_G[G_T_]
             ,D_H[T_A_], D_S[T_A_], D_G[T_A_]
             ,D_H[T_C_], D_S[T_C_], D_G[T_C_]
             ,D_H[T_G_], D_S[T_G_], D_G[T_G_]
             ,D_H[T_T_], D_S[T_T_], D_G[T_T_]);


  D_H[INGC_] = 0.1;
  D_S[INGC_] = -2.8 + dss;
  D_G[INGC_] = D_H[INGC_] - (D_S[INGC_]*(T0+Ti))/1000;

  D_H[INAT_] = 2.3;
  D_S[INAT_] = 4.1 + dss;
  D_G[INAT_] = D_H[INAT_] - (D_S[INAT_]*(T0+Ti))/1000;

  D_H[SC_] = 0.0;
  D_S[SC_] = -1.4 + dss;
  D_G[SC_] = D_H[SC_] - (D_S[SC_]*(T0+Ti))/1000;

}


int 	do_unzip_HP_signal(void)
{
        int i, j, k, dur_op = 0; // , i_1, io_1, io
  static int nx = 16384, new_plot = 1;//, timin = 0;
  pltreg *pr;
  O_p *op = NULL;
  d_s  *dsf = NULL;
  static int idum = 4587, dur = 0,  gplo = 0, gpll = 0, debug = 0, verbose = 0, no_salt_correction = 1, unpeeling = 0, avgE = 0;
  static char oligo[128] = {0};
  static float Na = 150, Mg = 10, Ti = 25, F = 8.6, T0 = 273.15, mdt = 1.08;
  float dg[128] = {0};
  float roh[128] = {0};
  float thres[10] = {0};
  float R = 1.9872;
  float cor = 1000.0/(R*(T0+Ti)); // cal/K-mol
  float tmp;
  int len, index; //, hp, ol;
  float d, bp, ft, u, gss, gds, rch, rco, S, gsso;
  char stuff[2048] = {0};
  int disp = WIN_OK;


  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return 1;
  if (strlen(oligo) == 0) sprintf(oligo,"TTTGGCTAAATATCCTAATGTTAAAGTGTATGATAAGCCTACTACAGTAGATTTTGACGGGTGTTTGATTGATTTGATTCCTTGGATGTGCGAAG");
				  //"TTTGGCTAAATATCCTAATGTTAAAGTGTATGATAAGCCTACTACAGTAGATTTTGACGGGTGTTTGATTGATTTGATTCCTTGGATGTGCGAAG"); //TTTGGCTAAATATCCTAATGTTAAAGTGTATGATAAGCCTACTACAGTAGATTTTGACGGGTGTTTGATTGATTTGATTCCTTGGATGTGCGAAG"); // ACAGCCAGCCGA "GGGTGTTTGATTGATTTGATTCCTTGGATGTGCGAAG"

  i = win_scanf("Generate an %R unzipping or %r unpeeling signal of a fork"
		"Following Simona Cocco model\n"
		"Specify the HP sequence %s\n"
		"Average Based energy%b\n"
		"%b->No salt entropy correction \n"
		"[Na+] = %4f mM; [Mg2+] = %4f mM; T = %4f \\oc C\n"
		"Force = %4f pN\n"
		"%R in this plot %r in a new plot\n"
		"Total number of steps %8d\n"
		"Random seed %8d\n"
		"%R real time or %r duration \n"
                "%b Generate plot with average openning probability\n"
                "%b Genertae a plot with statitics of last base attached\n"
                "%b debugging\n"
                "Time conversion in \\mu s %4f\n"
                "%b Display energies\n"
		,&unpeeling,oligo,&avgE,&no_salt_correction,&Na,&Mg,&Ti,&F
		,&new_plot,&nx,&idum,&dur,&gplo,&gpll,&debug,&mdt,&verbose);
  if (i == WIN_CANCEL)	return D_O_K;

  cor = 1000.0/(R*(T0+Ti)); // convert cal/K-mol in kBT
  init_Mfold31_array(Na, Mg, Ti, verbose, no_salt_correction, dsCharge);
  if (debug == 0)  disp = WIN_CANCEL;
  len = strlen(oligo);

  for(i = 0, tmp = 0; i < 16; i++)
    {
      tmp += DGMF[i];
    }
  tmp /= 16;
  tmp *= -cor;
  for (i = 1; i < len; i++)
    {
      index = 0;
      if ((oligo[i-1]&0x5F) == 'A')  index = 0;
      if ((oligo[i-1]&0x5F) == 'C')  index = 4;
      if ((oligo[i-1]&0x5F) == 'G')  index = 8;
      if ((oligo[i-1]&0x5F) == 'T')  index = 12;
      if ((oligo[i]&0x5F) == 'A')  index += 0;
      if ((oligo[i]&0x5F) == 'C')  index += 1;
      if ((oligo[i]&0x5F) == 'G')  index += 2;
      if ((oligo[i]&0x5F) == 'T')  index += 3;
      dg[i-1] = -cor*DGMF[index];
      if (i < 12)
              sprintf(stuff+strlen(stuff),"DeltaG %c%c %g Kcal/mol %g kBT\n",oligo[i-1],oligo[i],-DGMF[index],dg[i-1]);
    }

  for(i = 0; i < len-1; i++)
    {
      roh[i] = exp(-dg[i]);
      if (avgE) roh[i] = exp(-tmp);
    }

  // no correction on hp


  //elasticity models for the closing rates for the ds and ss DNA

  d = 0.542; // nm
  bp = 2.14; // nm
  S = 216;  // pN
  ft = 4.1*(T0+Ti)/(T0+25);
  u = bp*F/ft;
  gss = d/bp*(log(sinh(u)/u)+ 0.5*F*F/(ft*S));
  gsso = d/bp*log(sinh(u)/u);
  gds = (F/ft - sqrt(F/ft/50) + 0.5*F*F/ft/1230)*0.34;


  //closing rates:only depend on the force
  rch = exp(-2*gss);
  rco = exp(-gss+gds);

  win_printf("rch new = %g old = %g\n",rch,exp(-2*gsso));

  for (i = 0; i < len-1 && i < 12; i++)
    sprintf(stuff+strlen(stuff),"roh[%d] = %g;\n",i, roh[i]);
  if (i < len ) sprintf(stuff+strlen(stuff),"...\nrch = %g;\n", rch);
  else sprintf(stuff+strlen(stuff),"rch = %g;rco %g\n",rch,rco);

  win_printf("%s",stuff);


  if (new_plot)
    {
      if ((op = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create plot");
      dsf = op->dat[0];
      dsf->nx = dsf->ny = 0;
      dur_op = pr->n_op-1;
      set_plot_title(op, "\\stack{{HP %s %s}"
		     "{seq:%s}"
		     "{\\pt8 [Na+] = %g mM, [mg2+] = %g mM, T=%g^\\oc C; F = %g pN}}"
		     ,((unpeeling)?"unpeeling":"unzipping")
		     ,((avgE)?"Base energy averaged":""),oligo,Na,Mg,Ti,F);

      create_attach_select_x_un_to_op(op, IS_SECOND, 0, mdt, -6, 0, "\\mu s");
      if (dur)       create_attach_select_y_un_to_op(op, IS_SECOND, 0, mdt, -6, 0, "\\mu s");

    }
  else
    {
      if ((dsf = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      dsf->nx = dsf->ny = 0;
      dur_op = pr->cur_op;
    }




  //set_dot_line(ds);
  set_ds_source(dsf, "->(Fork)/oligo blocking;  seq:%s; [Na+] = %g mM, [mg2+] = %g mM, T=%g C; F = %g pN"
		,oligo,Na,Mg,Ti,F);

  float t = 0, t_1 = 0, dt = 0, den = 0, ttot = 0;
  int fork, fork_1 = 0, len_1 = len - 1;

  for (i = j = 0, fork = len-2; i < nx; j++)
    {
      //fork = len - hp; // measure the nb of base closed in the hairpin
      //fork starts at 0 and increases it is always <= oli_open
      // oli2_open = len - oli2 ; oli2_open = len no breathing
      // 0 <= fork <= oli_open < oli2_open <= len
      // detaching when oli_open == oli2_open

      if (fork > 0)  // we are still attached
	{      // we try to close oligo
          if (fork < 0 || fork >= len_1)
                  win_printf("fork %d out of range [0,%d]",fork,len_1);

	  thres[0] = (fork == 0) ? 0 : roh[fork];
          // closing fork only if not blocked by oligo
          thres[1] = thres[0];
	  thres[1] += (fork < len - 2) ? ((unpeeling) ? rco : rch) : 0;
          den = thres[1];


          tmp = ran1(&idum);
          dt = -log(tmp)/den;
          t += dt;
          ttot += dt;
          tmp = den*ran1(&idum);
          if (disp != WIN_CANCEL)
            {
              disp = win_printf("Fork %d; tmp = %g\n"
                                "    Fork    %c -- if in [0,%g]\n"
                                "    Fork    %c ++ if in [%g,%g]\n"
                                ,fork,tmp
                                ,(tmp < thres[0])? 'y':'n',thres[0]
                                ,(tmp >= thres[0] && tmp < thres[1])? 'y':'n',thres[0],thres[1]);

            }
          if (tmp < thres[0]) fork--;
          if (tmp >= thres[0] && tmp < thres[1]) fork++;

	  if (dur == 0)
	    {
	      add_new_point_to_ds(dsf, t_1, fork_1);
	      add_new_point_to_ds(dsf, t, fork_1);
	      fork_1 = fork;
              t_1 = t;
	      add_new_point_to_ds(dsf, t, fork);
	      i++;
	    }
          else
            {
	      fork_1 = fork;
              t_1 = t;
            }
	  if (j%65536 == 0)
                my_set_window_title("j %d pos %d", j,fork);
	}
      else
	{
	  if (dur == 0)
	    {
	      add_new_point_to_ds(dsf, t, fork);
	      i++;
	      fork_1 = fork = 0;
	      add_new_point_to_ds(dsf, t, fork);
	    }
	  if (dur == 1)
	    {
	      fork = len;
	      my_set_window_title("Event %d duration %f %3.1f%%", i,t,(float)(100*i)/nx);
	      add_new_point_to_ds(dsf, i++, t);
              for (k = 0, tmp = 0; k < dsf->nx; k++) tmp += dsf->yd[k];
              if (dsf->nx > 0) tmp/=dsf->nx;
              set_plot_title(op, "\\stack{{Fork %s %s}"
                             "{seq:%s}"
                             "{\\pt8 [Na+] = %g mM, [mg2+] = %g mM, T=%g^\\oc C; F = %g pN}{Mean duration %g}}"

                             ,((unpeeling)?"unpeeling":"unzipping")
			     ,((avgE)?"Base energy averaged":"")
			     ,oligo,Na,Mg,Ti,F,tmp);

              op->need_to_refresh = 1;
              refresh_plot(pr,dur_op);
	      t = 0;
	    }
	}
    }
  return refresh_plot(pr,pr->n_op-1);
}





// simulate Simona Cocco algo
int 	do_unzip_signal_SC(void)
{
  int i, j, j_1; // , i_1, io_1, io
  static int nx = 16384, new_plot = 0;//, timin = 0;
  pltreg *pr;
  O_p *op = NULL, *opt = NULL;
  d_s *ds = NULL, *dso = NULL, *dso2 = NULL, *dst = NULL;
  static int idum = 4587, allow_oli2 = 1, no_salt_correction = 0;
  static char oligo[64] = {0};
  static float Na = 150, Mg = 10, Ti = 25, F = 8.6, T0 = 273.15, factor = 2;
  float dg[64] = {0};
  float roh[64] = {0};
  float roo[64] = {0};
  float roo2[64] = {0};
  float R = 1.9872;
  float cor = 1000.0/(R*(T0+Ti)); // cal/K-mol
  float tmp;
  int len, index; //, hp, ol;
  float d, bp, ft, u, gss, gds, rco, rch, dgt;
  char stuff[2048] = {0};
  int disp = WIN_OK, verbose = 1;


  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return 1;
  i = win_scanf("Generate an unzipping signal of a fork with a blocking oligo\n"
		"Following Simona Cocco model where fork and oligo openning\n"
		"are not bound but where the fork is behing the oligo opening\n"
		"Specify the oligo sequence %s\n"
		"%b->No salt entropy correction \n"
		"[Na+] = %4f mM; [Mg2+] = %4f mM; T = %4f \\oc C\n"
		"Force = %4f pN\n"
		"%R in this plot %r in a new plot\n"
		"Total number of steps %8d\n"
		"Random seed %8d\n"
		"%b Allow second end of oligo to fluctuate\n"
		"Multipling factor for random gen %4f\n"
                "%b Display energiies\n"
		,oligo,&no_salt_correction,&Na,&Mg,&Ti,&F
		,&new_plot,&nx,&idum,&allow_oli2,&factor, &verbose);
  if (i == WIN_CANCEL)	return D_O_K;
  cor = 1000.0/(R*(T0+Ti)); // convert cal/K-mol in kBT
  init_Mfold31_array(Na, Mg, Ti,verbose, no_salt_correction, dsCharge);

  len = strlen(oligo);

  for (i = 1; i < len; i++)
    {
      index = 0;
      if ((oligo[i-1]&0x5F) == 'A')  index = 0;
      if ((oligo[i-1]&0x5F) == 'C')  index = 4;
      if ((oligo[i-1]&0x5F) == 'G')  index = 8;
      if ((oligo[i-1]&0x5F) == 'T')  index = 12;
      if ((oligo[i]&0x5F) == 'A')  index += 0;
      if ((oligo[i]&0x5F) == 'C')  index += 1;
      if ((oligo[i]&0x5F) == 'G')  index += 2;
      if ((oligo[i]&0x5F) == 'T')  index += 3;
      dg[i-1] = -cor*DGMF[index];
      sprintf(stuff+strlen(stuff),"DeltaG %c%c %g Kcal/mol %g kBT\n",oligo[i-1],oligo[i],-DGMF[index],dg[i-1]);
    }
  for(i = 1; i < len; i++)
    {
      roo[i] = exp(-dg[i]);
      roh[i] = exp(-dg[len-i-1]);
      roo2[i]= exp(-dg[len-i-1]);
    }

  roo2[len-1] = roh[len-1] = exp(-dg[0]);
  dgt =(((oligo[0]&0x5F) == 'A') || ((oligo[0]&0x5F) == 'T')) ? -cor*DGMF[INAT] : -cor*DGMF[INGC];
  roo[0]= exp(-dg[0]-dgt);

  if ((oligo[len-1]&0x5F) == 'A')
    roo[len-1]= exp(cor*(DGMF[AC] + DGMF[AG] + DGMF[AT] + DGMF[AA])/8);
  else if ((oligo[len-1]&0x5F) == 'C')
    roo[len-1]= exp(cor*(DGMF[CC] + DGMF[CG] + DGMF[CT] + DGMF[CA])/8);
  else if ((oligo[len-1]&0x5F) == 'G')
    roo[len-1]= exp(cor*(DGMF[GC] + DGMF[GG] + DGMF[GT] + DGMF[GA])/8);
  else if ((oligo[len-1]&0x5F) == 'T')
    roo[len-1]= exp(cor*(DGMF[TC] + DGMF[TG] + DGMF[TT] + DGMF[TA])/8);

  if ((oligo[len-1]&0x5F) == 'A')
    roo2[0] = roh[0] = exp(cor*(DGMF[AC] + DGMF[AG] + DGMF[AT] + DGMF[AA])/8);
  else if ((oligo[len-1]&0x5F) == 'C')
    roo2[0] = roh[0] = exp(cor*(DGMF[CC] + DGMF[CG] + DGMF[CT] + DGMF[CA])/8);
  else if ((oligo[len-1]&0x5F) == 'G')
    roo2[0] = roh[0] = exp(cor*(DGMF[GC] + DGMF[GG] + DGMF[GT] + DGMF[GA])/8);
  else if ((oligo[len-1]&0x5F) == 'T')
    roo2[0] = roh[0] = exp(cor*(DGMF[TC] + DGMF[TG] + DGMF[TT] + DGMF[TA])/8);

  //elasticity models for the closing rates for the ds and ss DNA
  d = 0.56*1.12;
  bp = 1.5*1.125;
  d = 0.605;// # 0.567 #0.56*1.12 new 0.605
  bp = 1.828; // # 1.78 # 1.5*1.125 new 1.828
  ft = 4.1*(T0+Ti)/(T0+25);
  u = bp*F/ft;
  gss = d/bp*log(sinh(u)/u) ;
  gds = (F/ft - sqrt(F/ft/50) + 0.5*F*F/ft/1230)*0.34;


  //closing rates:only depend on the force
  rch = exp(-2*gss);
  rco = exp(-gss+gds);

  for (i = 0; i < len && i < 12; i++)
    sprintf(stuff+strlen(stuff),"roo[%d] = %g; roh[%d] = %g;\n",i,roo[i],i, roh[i]);
  if (i < len ) sprintf(stuff+strlen(stuff),"...\nrco = %g; rch = %g;\n",rco, rch);
  else sprintf(stuff+strlen(stuff),"rco = %g; rch = %g;\n",rco, rch);

  win_printf("%s",stuff);


  if (new_plot)
    {
      if ((op = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create plot");
      ds = op->dat[0];
      ds->nx = ds->ny = 0;
      if ((dso = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      dso->nx = dso->ny = 0;
      if ((dso2 = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      dso2->nx = dso2->ny = 0;
      set_plot_title(op, "\\stack{{Fork/oligo blocking}"
		     "{seq:%s}"
		     "{\\pt8 [Na+] = %g mM, [mg2+] = %g mM, T=%g^\\oc C; F = %g pN}}"
		     ,oligo,Na,Mg,Ti,F);
    }
  else
    {
      if ((ds = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      ds->nx = ds->ny = 0;
      if ((dso = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      dso->nx = dso->ny = 0;
      if ((dso2 = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      dso2->nx = dso2->ny = 0;
    }

  if ((opt = create_and_attach_one_plot(pr, 16, 16, 0)) == NULL)
    return win_printf_OK("cannot create plot");
  dst = opt->dat[0];
  dst->nx = dst->ny = 0;



  //set_dot_line(ds);
  set_ds_source(ds, "->Fork/oligo blocking;  seq:%s; [Na+] = %g mM, [mg2+] = %g mM, T=%g C; F = %g pN"
		,oligo,Na,Mg,Ti,F);
  set_ds_source(dso, "Fork/->oligo blocking;  seq:%s; [Na+] = %g mM, [mg2+] = %g mM, T=%g C; F = %g pN"
		,oligo,Na,Mg,Ti,F);
  set_ds_source(dso2, "Fork/->oligo2 blocking;  seq:%s; [Na+] = %g mM, [mg2+] = %g mM, T=%g C; F = %g pN"
		,oligo,Na,Mg,Ti,F);
  //int k = len, l = 0;

  int fork, fork_1 = -1, fork_i_1 = -1, oli_open, oli_open_1 = -1, oli_open_i_1 = -1, oli2_open, oli2_open_1 = -1, oli2_open_i_1 = -1;

  //j_1 = 0, ol2, io2 = 0, ol2_1, io2_1, detach = 0, fork;
  for (j_1 = -1, i = j = 0, fork = 0, oli_open = 0, oli2_open = len; i < nx; j++)
    {
      //fork = len - hp; // measure the nb of base closed in the hairpin
      //fork starts at 0 and increases it is always <= oli_open
      // oli2_open = len - oli2 ; oli2_open = len no breathing
      // 0 <= fork <= oli_open < oli2_open <= len
      // detaching when oli_open == oli2_open
      if (oli_open < oli2_open)  // we are still attached
	{      // we try to close oligo
	  //den = rco + roo[oli_open] +
	  tmp = factor*ran1(&idum);
          if (oli_open > fork)
	    {  // test if closing oligo
	      if (tmp < rco)
		{
		  if (disp != WIN_CANCEL || j%1024 == 0)
		    disp = win_printf("j %d i %d: oli close rco %g\n fork = %d, oli1 = %d -> %d, oli2 -> %d"
				      ,j,i,rco,fork,oli_open,oli_open-1,oli2_open);
		  oli_open--;
		}
            }
	  // we try to open oligo
	  tmp -= rco;
	  if (tmp >= 0 && tmp < roo[oli_open])
	    {
	      if (disp != WIN_CANCEL || j%1024 == 0)
		disp = win_printf("j %d i %d: oli open roo %g\n fork = %d, oli1 = %d -> %d, oli2 -> %d"
				  ,j,i,roo[oli_open],fork,oli_open,oli_open+1,oli2_open);
	      oli_open++;
	    }
	  if (allow_oli2)
	    { // we try to close oligo side opposite to fork
	      tmp = factor*ran1(&idum);
              if (oli2_open  < len)  // we are still attached
		{  // test if closing oligo
		  if (tmp < rco)
		    {
		      //win_printf("j %d: hp = %d, ol = %d -> %d",j,hp,ol,ol-1);
		      if (disp != WIN_CANCEL || j%1024 == 0)
			disp = win_printf("j %d i %d: oli2 close rco %g\n fork = %d, oli1 = %d, oli2 %d -> %d"
					  ,j,i,rco,fork,oli_open,oli2_open,oli2_open+1);
		      oli2_open++;
		    }
		}
	      if (oli_open < oli2_open)
		{  // test if openning oligo
		  tmp -= rco;
		  if (tmp >= 0 && tmp < roo2[len - oli2_open])
		    {
		      if (disp != WIN_CANCEL || j%1024 == 0)
			disp = win_printf("j %d i %d: oli2 open roo2 %g\n fork = %d, oli1 = %d, oli2 %d -> %d"
					  ,j,i,roo2[len - oli2_open],fork,oli_open,oli2_open,oli2_open-1);
		      if ((oli_open >= oli2_open-1) && (disp != WIN_CANCEL))
			disp = win_printf("j %d i %d: fork = %d, oli = %d ol2 %d -> %d"
					  ,j,i,fork,oli_open,oli2_open,oli2_open-1);
		      oli2_open--;
		    }
		}
            }
	  tmp = factor*ran1(&idum);
	  if (fork > 0)
	    {
	      if (tmp < rch)
		{
		  if (disp != WIN_CANCEL || j%1024 == 0)
		    disp = win_printf("j %d i %d: fork close rch %g\n fork = %d -> %d, oli1 = %d, oli2 %d"
				      ,j,i,rch,fork,fork-1,oli_open,oli2_open);
		  fork--;
		}
	    }
	  if ((fork < oli_open))
	    { // hairpin may move
	      tmp -= rch;
	      if (tmp >= 0 && tmp < roh[len-fork-1])
		{
		  if (disp != WIN_CANCEL || j%1024 == 0)
		    disp = win_printf("j %d i %d: fork open roh %g\n fork = %d -> %d, oli1 = %d, oli2 %d"
				      ,j,i,roh[len-fork-1],fork,fork+1,oli_open,oli2_open);
		  fork++;
		}
	    }

        }


      if (fork != fork_1)
	{
	  if (fork_i_1 < j - 1)
	    {
	      add_new_point_to_ds(ds, j-1, fork_1);
	      i++;
	    }
	  add_new_point_to_ds(ds, fork_i_1 = j, fork_1 = fork);
	  i++;
	}
      if (oli_open != oli_open_1)
	{
	  if (oli_open_i_1 < j - 1)
	    {
	      add_new_point_to_ds(dso, j-1, oli_open_1);
	      i++;
	    }
	  add_new_point_to_ds(dso, oli_open_i_1 = j, oli_open_1 = oli_open);
	      i++;
	  if (oli_open >= oli2_open)
	    {
	      if (j_1 < j - 1)
		{
		  add_new_point_to_ds(ds, j-1, fork_1);
		  i++;
		}
	      add_new_point_to_ds(ds, j_1 = j, 0);
	      i++;
	      oli_open = 0;
	      add_new_point_to_ds(dst, dst->nx, j - j_1);
	      j_1 = j;
	      fork_1 = fork = 0;
	    }



	}
      if (oli2_open != oli2_open_1)
	{
	  if (oli2_open_i_1 < j - 1)
	    {
	      add_new_point_to_ds(dso2, j-1, oli2_open_1);
	      i++;
	    }
	  add_new_point_to_ds(dso, oli2_open_i_1 = j, oli2_open_1 = oli2_open);
	  i++;
	}
    }

  /*
  int j_1 = 0, ol2, io2 = 0, ol2_1, io2_1, detach = 0, fork;

  for (i = j = io = io2 = 0, hp = len,  ol = ol2 = 0, hp_1 = ol_1 = ol2_1 = -1, i_1 = -1, detach = 0; i < nx; j++)
    {
      fork = len - hp; // measure the nb of base closed in the hairpin
      //fork starts at 0 and increases it is always <= oli_open
      // oli2_open = len - oli2 ; oli2_open = len no breathing
      // 0 <= fork <= oli_open < oli2_open <= len
       if (ol < len - ol2)
	{
	  // we try to close oligo
	  if ((ol > 0) && (hp - len+ol > 0))
	    {  // test if closing oligo
	      if (ran1(&idum) < rco)
		{
		  //win_printf("j %d: hp = %d, ol = %d -> %d",j,hp,ol,ol-1);
		  ol--;
		}
	    }
	  // we try to open oligo
	  if (ran1(&idum) < roo[ol])
	    {
	      //win_printf("j %d: hp = %d, ol = %d -> %d",j,hp,ol,ol+1);
	      ol++;
	    }
	  if (allow_oli2)
	    {
	      // we try to close oligo side opposite to fork
	      if ((ol2 > 0) && (ol < len - 1))
		{  // test if closing oligo
		  if (ran1(&idum) < rco)
		    {
		      //win_printf("j %d: hp = %d, ol = %d -> %d",j,hp,ol,ol-1);
		      ol2--;
		    }
		}
	      if ((ol < len - 1) && (ol2 <= hp - ol))
		{  // test if openning oligo
		  if (ran1(&idum) < roo2[ol2])
		    {
		      if (ol2+1 > hp - ol)
			win_printf("j %d: hp = %d, ol = %d ol2 %d -> %d",j,hp,ol,ol2,ol2+1);
		      ol2++;
		    }
		}
	    }

	  if ((hp > 1) && (hp - len + ol > 0))
	    { // hairpin may move
	      if (ran1(&idum) < roh[hp-1])
		{
		  //win_printf("j %d: hp = %d -> %d, ol = %d -> %d",j,hp,hp-1,ol);
		  hp--;
		}
	    }
	  if ((hp < len) && (hp - len + ol > 0))
	    {
	      if (ran1(&idum) < rch)
		{
		  //win_printf("j %d: hp = %d -> %d, ol = %d -> %d",j,hp,hp+1,ol);
		  hp++;
		}
	    }
	}
      if (hp != hp_1)
	{
	  if (i_1 < j - 1)
	    {
	      add_new_point_to_ds(ds, j-1, hp_1);
	      i++;
	    }
	  add_new_point_to_ds(ds, i_1 = j, hp_1 = hp);
	  i++;
	}
      if (ol != ol_1)
	{
	  if (io_1 < j - 1)
	    {
	      add_new_point_to_ds(dso, j-1, ol_1);
	      io++;
	    }
	  add_new_point_to_ds(dso, io_1 = j, ol_1 = ol);
	  io++;
	}
      if (ol2 != ol2_1)
	{
	  if (io2_1 < j - 1)
	    {
	      add_new_point_to_ds(dso2, j-1, ol2_1);
	      io2++;
	    }
	  add_new_point_to_ds(dso2, io2_1 = j, ol2_1 = ol2);
	  io2++;
	}


      if (ol >= len - ol2)
	{

	  if (i_1 < j - 1)
	    {
	      add_new_point_to_ds(ds, j-1, hp_1);
	      i++;
	    }
	  add_new_point_to_ds(ds, i_1 = j, 0);
	  i++;
	  add_new_point_to_ds(ds, i_1 = j, len);
	  i++;
	  ol = 0;
	  add_new_point_to_ds(dst, dst->nx, j - j_1);
	  j_1 = j;
	  hp_1 = hp = len;
	}


    }
  */
  return refresh_plot(pr,pr->n_op-1);
}



// simulate Simona Cocco algo
/*
int 	do_apex_oligo_1side(void)
{
  int i, j; // , i_1, io_1, io
  static int nx = 16384, new_plot = 0;
  pltreg *pr;
  O_p *op = NULL, *opt = NULL;
  d_s  *dso = NULL, *dst = NULL;
  static int idum = 4587, dur = 0;
  static char oligo[64] = {0};
  static float Na = 150, Mg = 10, Ti = 25, F = 8.6, T0 = 273.15;
  float dg[64] = {0};
  float roo[64] = {0};
  float R = 1.9872;
  float cor = 1000.0/(R*(T0+Ti)); // cal/K-mol
  float tmp;
  int len, index; //, hp, ol;
  float d, bp, ft, u, gss, gds, rco, dgt;
  char stuff[2048] = {0};
  int disp = WIN_OK, verbose = 1;


  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return 1;
  i = win_scanf("Generate a oligo unbinding signal with opening on one side only\n"
		"Specify the oligo sequence %s\n"
		"[Na+] = %4f mM; [Mg2+] = %4f mM; T = %4f \\oc C\n"
		"Force = %4f pN\n"
		"%R in this plot %r in a new plot\n"
		"Total number of steps %8d\n"
		"Random seed %8d\n"
		"%R real time or %r duration \n"
                "%b Display energiies\n"
		,oligo,&Na,&Mg,&Ti,&F
		,&new_plot,&nx,&idum,&dur,&verbose);
  if (i == WIN_CANCEL)	return D_O_K;
  cor = 1000.0/(R*(T0+Ti)); // convert cal/K-mol in kBT
  init_Mfold31_array(Na, Mg, Ti, verbose);

  len = strlen(oligo);

  for (i = 1; i < len; i++)
    {
      index = 0;
      if ((oligo[i-1]&0x5F) == 'A')  index = 0;
      if ((oligo[i-1]&0x5F) == 'C')  index = 4;
      if ((oligo[i-1]&0x5F) == 'G')  index = 8;
      if ((oligo[i-1]&0x5F) == 'T')  index = 12;
      if ((oligo[i]&0x5F) == 'A')  index += 0;
      if ((oligo[i]&0x5F) == 'C')  index += 1;
      if ((oligo[i]&0x5F) == 'G')  index += 2;
      if ((oligo[i]&0x5F) == 'T')  index += 3;
      dg[i-1] = -cor*DGMF[index];
      sprintf(stuff+strlen(stuff),"DeltaG %c%c %g Kcal/mol %g kBT\n",oligo[i-1],oligo[i],-DGMF[index],dg[i-1]);
    }
  for(i = 1; i < len-1; i++)
    {
      roo[i] = exp(-dg[i]);
    }

  dgt =(((oligo[0]&0x5F) == 'A') || ((oligo[0]&0x5F) == 'T')) ? -cor*DGMF[INAT] : -cor*DGMF[INGC];
  roo[0]= exp(-dg[0]-dgt);


  dgt =(((oligo[len-1]&0x5F) == 'A') || ((oligo[len-1]&0x5F) == 'T')) ? -cor*DGMF[INAT] : -cor*DGMF[INGC];

  roo[len-1]= exp(-dg[len-1]-dgt);


  //elasticity models for the closing rates for the ds and ss DNA
  d = 0.56*1.12;
  bp = 1.5*1.125;
  ft = 4.1*(T0+Ti)/(T0+25);
  u = bp*F/ft;
  gss = d/bp*log(sinh(u)/u) ;
  gds = (F/ft - sqrt(F/ft/50) + 0.5*F*F/ft/1230)*0.34;


  //closing rates:only depend on the force
  rco = exp(-gss+gds);

  for (i = 0; i < len && i < 12; i++)
    sprintf(stuff+strlen(stuff),"roo[%d] = %g;\n",i,roo[i]);
  if (i < len ) sprintf(stuff+strlen(stuff),"...\nrco = %g;\n",rco);
  else sprintf(stuff+strlen(stuff),"rco = %g;\n",rco);

  win_printf(stuff);


  if (new_plot)
    {
      if ((op = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create plot");
      dso = op->dat[0];
      dso->nx = dso->ny = 0;
      set_plot_title(op, "\\stack{{Fork/oligo blocking}"
		     "{seq:%s}"
		     "{\\pt8 [Na+] = %g mM, [mg2+] = %g mM, T=%g^\\oc C; F = %g pN}}"
		     ,oligo,Na,Mg,Ti,F);
    }
  else
    {
      if ((dso = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      dso->nx = dso->ny = 0;
    }
  //set_dot_line(ds);
  set_ds_source(dso, "Fork/->oligo blocking;  seq:%s; [Na+] = %g mM, [mg2+] = %g mM,"
		" T=%g C; F = %g pN" ,oligo,Na,Mg,Ti,F);

  float t = 0, dt = 0, den = 0;
  int  oli_open;
  for (i = 0, j = 0, oli_open = 0; i < nx; j++)
    {
      // 0 <= fork <= oli_open < oli2_open <= len
      // detaching when oli_open == oli2_open
      if (oli_open < len)  // we are still attached
	{      // we try to close oligo
	  if (oli_open == 0)
	    {  // we can only open the oligo
	      den = roo[oli_open];
	      tmp = ran1(&idum);
	      dt = -log(tmp)/den;
	      t += dt;
	      oli_open = 1;
	    }
	  else
	    {  // we can open or close the oligo
	      den = rco + roo[oli_open];
	      tmp = ran1(&idum);
	      dt = -log(tmp)/den;
	      t += dt;
	      tmp = den*ran1(&idum);
	      oli_open = (tmp < rco) ? oli_open-1 : oli_open+1;
	    }
	  if (dur == 0)
	    {
	      add_new_point_to_ds(dso, t, oli_open);
	      i++;
	    }
	  if (j%65536 == 0)
	    my_set_window_title("j %d pos %d", j,oli_open);
	}
      else
	{
	  oli_open = 0;
	  if (dur == 0)
	    {
	      add_new_point_to_ds(dso, t, oli_open);
	      i++;
	    }
	  if (dur == 1)
	    {
	      my_set_window_title("Event %d duration %f", i,t);
	      add_new_point_to_ds(dso, i++, t);
	      t = 0;
	    }
	}
    }
  return refresh_plot(pr,pr->n_op-1);
}


*/




int 	do_unzip_signal_SC_1side_n(void)
{
  int i, j, k; // , i_1, io_1, io
  static int nx = 16384, new_plot = 1;//, timin = 0;
  pltreg *pr;
  O_p *op = NULL;
  d_s *dso = NULL, *dsf = NULL, *dsb = NULL;
  static int idum = 4587, end_oligo = 0, dur = 0, verbose = 0, no_salt_correction = 0, at_penalty = 3;
  static char oligo[64] = {0};
  static float Na = 150, Mg = 10, Ti = 25, F = 8.6, T0 = 273.15;
  float dg[64] = {0};
  float roh[64] = {0};
  float roo[64] = {0};
  float thres[10] = {0};
  float R = 1.9872;
  float cor = 1000.0/(R*(T0+Ti)); // cal/K-mol
  float tmp;
  int len, index; //, hp, ol;
  float d, bp, ft, u, gss, gds, rco, rch, dgt;
  char stuff[2048] = {0};





  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return 1;
  i = win_scanf("Generate an unzipping signal of a fork with a blocking oligo\n"
		"Following Simona Cocco model where fork and oligo openning\n"
		"are not bound but where the fork is behing the oligo opening\n"
		"Specify the oligo sequence %s\n"
		"Oligo ends correction:\n"
		"%R->no %r->on 5' %r->on 3'only %r->on both 5' and 3'\n"
		"else Oligo AT/GC penalty at the ends:\n"
		"%R->no %r->on 5' %r->on 3'only %r->on both 5' and 3'\n"
		"%b->No salt entropy correction \n"
		"[Na+] = %4f mM; [Mg2+] = %4f mM; T = %4f \\oc C\n"
		"Force = %4f pN\n"
		"%R in this plot %r in a new plot\n"
		"Total number of steps %8d\n"
		"Random seed %8d\n"
		"%R real time or %r duration \n"
                "%b Display energiies\n"
		,oligo,&end_oligo,&at_penalty,&no_salt_correction,&Na,&Mg,&Ti,&F
		,&new_plot,&nx,&idum,&dur,&verbose);
  if (i == WIN_CANCEL)	return D_O_K;

  cor = 1000.0/(R*(T0+Ti)); // convert cal/K-mol in kBT
  init_Mfold31_array(Na, Mg, Ti, verbose, no_salt_correction, dsCharge);

  len = strlen(oligo);

  for (i = 1; i < len; i++)
    {
      index = 0;
      if ((oligo[i-1]&0x5F) == 'A')  index = 0;
      if ((oligo[i-1]&0x5F) == 'C')  index = 4;
      if ((oligo[i-1]&0x5F) == 'G')  index = 8;
      if ((oligo[i-1]&0x5F) == 'T')  index = 12;
      if ((oligo[i]&0x5F) == 'A')  index += 0;
      if ((oligo[i]&0x5F) == 'C')  index += 1;
      if ((oligo[i]&0x5F) == 'G')  index += 2;
      if ((oligo[i]&0x5F) == 'T')  index += 3;
      dg[i-1] = -cor*DGMF[index];
      sprintf(stuff+strlen(stuff),"DeltaG %c%c %g Kcal/mol %g kBT\n",oligo[i-1],oligo[i],-DGMF[index],dg[i-1]);
    }

  for(i = 0; i < len-1; i++)
    {
      roo[i] = exp(-dg[i]);
      roh[i] = exp(-dg[i]);
    }

  if (end_oligo & 0x1)
    {
      dgt =(((oligo[0]&0x5F) == 'A') || ((oligo[0]&0x5F) == 'T')) ? -cor*DGMF[INAT] : -cor*DGMF[INGC];
    }
  else if (at_penalty & 0x1)
    {
      dgt =(((oligo[0]&0x5F) == 'A') || ((oligo[0]&0x5F) == 'T')) ? -cor*DGMF[TP] : 0;
    }
  else dgt = 0;
  roo[0]= exp(-dg[0]-dgt);
  // no correction on hp

  if (end_oligo & 0x2)
    {
      dgt =(((oligo[len-1]&0x5F) == 'A') || ((oligo[len-1]&0x5F) == 'T')) ? -cor*DGMF[INAT] : -cor*DGMF[INGC];
    }
  else if (at_penalty & 0x2)
    {
      dgt =(((oligo[len-1]&0x5F) == 'A') || ((oligo[len-1]&0x5F) == 'T')) ? -cor*DGMF[TP] : 0;
    }
  else dgt = 0;
  roo[len-2]= exp(-dg[len-2]-dgt);
  // no correction on hp

  //elasticity models for the closing rates for the ds and ss DNA
  d = 0.56*1.12;
  bp = 1.5*1.125;
  d = 0.605;// # 0.567 #0.56*1.12 new 0.605
  bp = 1.828; // # 1.78 # 1.5*1.125 new 1.828
  ft = 4.1*(T0+Ti)/(T0+25);
  u = bp*F/ft;
  gss = d/bp*log(sinh(u)/u) ;
  gds = (F/ft - sqrt(F/ft/50) + 0.5*F*F/ft/1230)*0.34;

  //closing rates:only depend on the force
  rch = exp(-2*gss);
  rco = exp(-gss+gds);

  for (i = 0; i < len-1 && i < 12; i++)
    sprintf(stuff+strlen(stuff),"roo[%d] = %g; roh[%d] = %g;\n",i,roo[i],i, roh[i]);
  if (i < len ) sprintf(stuff+strlen(stuff),"...\nrco = %g; rch = %g;\n",rco, rch);
  else sprintf(stuff+strlen(stuff),"rco = %g; rch = %g;\n",rco, rch);

  win_printf("%s",stuff);


  if (new_plot)
    {
      if ((op = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create plot");
      dsf = op->dat[0];
      dsf->nx = dsf->ny = 0;

      set_plot_title(op, "\\stack{{Fork/oligo blocking}"
		     "{seq:%s}"
		     "{\\pt8 [Na+] = %g mM, [mg2+] = %g mM, T=%g^\\oc C; F = %g pN}}"
		     ,oligo,Na,Mg,Ti,F);
    }
  else
    {
      if ((dsf = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      dsf->nx = dsf->ny = 0;
    }

  if (dur == 0)
    {
      if ((dso = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      dso->nx = dso->ny = 0;
      set_ds_source(dso, "Fork/->(oligo) blocking;  seq:%s; [Na+] = %g mM, [mg2+] = %g mM, T=%g C; F = %g pN"
                    ,oligo,Na,Mg,Ti,F);
      if ((dsb = create_and_attach_one_ds(op, len-1, len-1, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      for(k = 0; k < dsb->nx; k++) dsb->yd[k] = 0.5+k;
    }


  //set_dot_line(ds);
  set_ds_source(dsf, "->(Fork)/oligo blocking;  seq:%s; [Na+] = %g mM, [mg2+] = %g mM, T=%g C; F = %g pN"
		,oligo,Na,Mg,Ti,F);


  float t = 0, t_1 = 0, dt = 0, den = 0, ttot = 0;
  int fork, fork_1 = 0, oli_open, oli_open_1 = 0, len_1 = len - 1;

  for (i = j = 0, fork = 0, oli_open = 0; i < nx; j++)
    {
      //fork = len - hp; // measure the nb of base closed in the hairpin
      //fork starts at 0 and increases it is always <= oli_open
      // oli2_open = len - oli2 ; oli2_open = len no breathing
      // 0 <= fork <= oli_open < oli2_open <= len
      // detaching when oli_open == oli2_open
      if (oli_open < len_1)  // we are still attached
	{
          if (oli_open < 0 || oli_open >= len_1)
                  win_printf("oli_open out %d",oli_open);
          if (oli_open < fork)
                  win_printf("oli_open %d < fork %d",oli_open,fork);
	  if (oli_open == 0)
	    {  // we can only open the oligo
	      den = thres[0] = roo[oli_open];
	      tmp = ran1(&idum);
	      dt = -log(tmp)/den;
	      t += dt;
	      ttot += dt;
	      oli_open++;
	    }
	  else if (fork == 0)
	    {  // we can only open/close the oligo but only close hp
              thres[0] = roo[oli_open];
              thres[1] = thres[0] + rco;
              den = thres[2] = thres[1] + rch;
	      tmp = ran1(&idum);
	      dt = -log(tmp)/den;
	      t += dt;
	      ttot += dt;
	      tmp = den*ran1(&idum);
	      if (tmp < thres[0])  oli_open++;
	      else if (tmp >= thres[0] && tmp < thres[1]) oli_open--;
	      else fork++;
	    }
	  else if (oli_open - fork > 0)
            {  //fork is not stuck to oligo
              thres[0] = roo[oli_open];
              thres[1] = thres[0] + rco;
              thres[2] = thres[1] + rch;
              den = thres[3] = thres[2] + roh[fork-1];
	      tmp = ran1(&idum);
	      dt = -log(tmp)/den;
	      t += dt;
	      ttot += dt;
	      tmp = den*ran1(&idum);
	      if (tmp < thres[0])  oli_open++;
	      else if (tmp >= thres[0] && tmp < thres[1]) oli_open--;
	      else if (tmp >= thres[1] && tmp < thres[2]) fork++;
	      else fork--;
            }
          else
            { //fork is stuck to oligo, oligon can only open, fork might only recced
              thres[0] = roo[oli_open];
              den = thres[1] = thres[0] + roh[fork-1];
	      tmp = ran1(&idum);
	      dt = -log(tmp)/den;
	      t += dt;
	      ttot += dt;
	      tmp = den*ran1(&idum);
	      if (tmp < thres[0])  oli_open++;
	      else fork--;
            }
	  if (dur == 0)
	    {
	      add_new_point_to_ds(dso, t_1, oli_open_1);
	      add_new_point_to_ds(dsf, t_1, fork_1);
	      add_new_point_to_ds(dso, t, oli_open_1);
	      add_new_point_to_ds(dsf, t, fork_1);
	      oli_open_1 = oli_open;
	      fork_1 = fork;
              t_1 = t;
	      add_new_point_to_ds(dso, t, oli_open);
	      add_new_point_to_ds(dsf, t, fork);
	      i++;
	    }
	  for(k = 0; k < oli_open_1 && k < len_1; k++)
            {
                if (k >= 0 && k < dsb->nx)       dsb->xd[k] += dt;
                else win_printf("K out of range %d",k);
            }
	  if (j%65536 == 0)
                  my_set_window_title("j %d pos %d %d", j,fork,oli_open);
	}
      else
	{
	  if (dur == 0)
	    {
	      add_new_point_to_ds(dso, t, oli_open);
	      add_new_point_to_ds(dsf, t, fork);
	      i++;
	      oli_open_1 = oli_open = 0;
	      fork_1 = fork = 0;
	      add_new_point_to_ds(dso, t, oli_open);
	      add_new_point_to_ds(dsf, t, fork);
	    }
	  if (dur == 1)
	    {
	      oli_open = 0;
	      fork = 0;
	      my_set_window_title("Event %d duration %f", i,t);
	      add_new_point_to_ds(dsf, i++, t);
	      t = 0;
	    }
	}
    }
  return refresh_plot(pr,pr->n_op-1);
}

# ifdef ENCOURS

int 	do_unzip_signal_SC_2oligo_2sides_n(void)
{
        int i, j, k, dur_op = 0; // , i_1, io_1, io
  static int nx = 16384, new_plot = 1;//, timin = 0;
  pltreg *pr;
  O_p *op = NULL, *opo = NULL, *opl = NULL;
  d_s *dso15 = NULL, *dso13 = NULL, *dsf = NULL, *dso25 = NULL, *dso23 = NULL, *dsop = NULL, *dslb = NULL;
  static int idum = 4587, end_oligo = 0, dur = 0, afork = 1, a3prime = 1, gplo = 0, gpll = 0, debug = 0, no_salt_correction = 0, at_penalty = 3;
  static char oligo[128] = {0};
  static float Na = 150, Mg = 10, Ti = 25, F = 8.6, T0 = 273.15;
  float dg[128] = {0};
  float roh[128] = {0};
  float roo[128] = {0};
  float thres[10] = {0};
  float R = 1.9872;
  float cor = 1000.0/(R*(T0+Ti)); // cal/K-mol
  float tmp;
  int len, index; //, hp, ol;
  float d, bp, ft, u, gss, gds, rco, rch, dgt;
  char stuff[2048] = {0};
  int disp = WIN_OK;


  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return 1;
  if (strlen(oligo) == 0) sprintf(oligo,"GGGTGTTTGATTGATTTGATTCCTTGGATGTGCGAAG"); //TTTGGCTAAATATCCTAATGTTAAAGTGTATGATAAGCCTACTACAGTAGATTTTGACGGGTGTTTGATTGATTTGATTCCTTGGATGTGCGAAG"); // ACAGCCAGCCGA

  i = win_scanf("Generate an unzipping signal of a fork with 2 blocking oligos "
		"Following Simona Cocco model\nwhere fork and oligo openning "
		"are not bound but where the fork is behind the oligo opening\n"
                "Allows the second end of the oligo to open\n"
		"Specify the oligo sequence %s\n"
		"Oligo ends correction:\n"
		"%R->no %r->on 5' %r->on 3'only %r->on both 5' and 3'\n"
		"else Oligo AT/GC penalty at the ends:\n"
		"%R->no %r->on 5' %r->on 3'only %r->on both 5' and 3'\n"
		"%b->No salt entropy correction \n"
		"[Na+] = %4f mM; [Mg2+] = %4f mM; T = %4f \\oc C\n"
		"Force = %4f pN\n"
		"%R in this plot %r in a new plot\n"
		"Total number of steps %8d\n"
		"Random seed %8d\n"
		"%R real time or %r duration \n"
                "%bFork active %b3-end of oligo 1 and 5'end of oligo2 active\n"
                "%b Generate plot with average openning probability\n"
                "%b Genertae a plot with statitics of last base attached\n"
                "%b debugging"
		,oligo,&end_oligo,&at_penalty,&no_salt_correction,&Na,&Mg,&Ti,&F
		,&new_plot,&nx,&idum,&dur,&afork,&a3prime,&gplo,&gpll,&debug);
  if (i == WIN_CANCEL)	return D_O_K;

  cor = 1000.0/(R*(T0+Ti)); // convert cal/K-mol in kBT
  init_Mfold31_array(Na, Mg, Ti, no_salt_correction, dsCharge);
  if (debug == 0)  disp = WIN_CANCEL;
  len = strlen(oligo);

  for (i = 1; i < len; i++)
    {
      index = 0;
      if ((oligo[i-1]&0x5F) == 'A')  index = 0;
      if ((oligo[i-1]&0x5F) == 'C')  index = 4;
      if ((oligo[i-1]&0x5F) == 'G')  index = 8;
      if ((oligo[i-1]&0x5F) == 'T')  index = 12;
      if ((oligo[i]&0x5F) == 'A')  index += 0;
      if ((oligo[i]&0x5F) == 'C')  index += 1;
      if ((oligo[i]&0x5F) == 'G')  index += 2;
      if ((oligo[i]&0x5F) == 'T')  index += 3;
      dg[i-1] = -cor*DGMF[index];
      if (i < 12)
              sprintf(stuff+strlen(stuff),"DeltaG %c%c %g Kcal/mol %g kBT\n",oligo[i-1],oligo[i],-DGMF[index],dg[i-1]);
    }

  for(i = 0; i < len-1; i++)
    {
      roo[i] = exp(-dg[i]);
      roh[i] = exp(-dg[i]);
    }

  if (end_oligo & 0x1)
    {
      dgt =(((oligo[0]&0x5F) == 'A') || ((oligo[0]&0x5F) == 'T')) ? -cor*DGMF[INAT] : -cor*DGMF[INGC];
    }
  else if (at_penalty & 0x1)
    {
      dgt =(((oligo[0]&0x5F) == 'A') || ((oligo[0]&0x5F) == 'T')) ? -cor*DGMF[TP] : 0;
    }
  else dgt = 0;
  roo[0]= exp(-dg[0]-dgt);
  // no correction on hp

  if (end_oligo & 0x2)
    {
      dgt =(((oligo[len-1]&0x5F) == 'A') || ((oligo[len-1]&0x5F) == 'T')) ? -cor*DGMF[INAT] : -cor*DGMF[INGC];
    }
  else if (at_penalty & 0x2)
    {
      dgt =(((oligo[len-1]&0x5F) == 'A') || ((oligo[len-1]&0x5F) == 'T')) ? -cor*DGMF[TP] : 0;
    }
  else dgt = 0;
  roo[len-2]= exp(-dg[len-2]-dgt);
  // no correction on hp

  //elasticity models for the closing rates for the ds and ss DNA
  d = 0.56*1.12;
  bp = 1.5*1.125;
  d = 0.605;// # 0.567 #0.56*1.12 new 0.605
  bp = 1.828; // # 1.78 # 1.5*1.125 new 1.828
  ft = 4.1*(T0+Ti)/(T0+25);
  u = bp*F/ft;
  gss = d/bp*log(sinh(u)/u) ;
  gds = (F/ft - sqrt(F/ft/50) + 0.5*F*F/ft/1230)*0.34;

  //closing rates:only depend on the force
  rch = exp(-2*gss);
  rco = exp(-gss+gds);

  for (i = 0; i < len-1 && i < 12; i++)
    sprintf(stuff+strlen(stuff),"roo[%d] = %g; roh[%d] = %g;\n",i,roo[i],i, roh[i]);
  if (i < len ) sprintf(stuff+strlen(stuff),"...\nrco = %g; rch = %g;\n",rco, rch);
  else sprintf(stuff+strlen(stuff),"rco = %g; rch = %g;\n",rco, rch);

  win_printf(stuff);


  if (new_plot)
    {
      if ((op = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create plot");
      dsf = op->dat[0];
      dsf->nx = dsf->ny = 0;
      dur_op = pr->n_op-1;
      set_plot_title(op, "\\stack{{%soligo blocking %s}"
		     "{seq:%s}"
		     "{\\pt8 [Na+] = %g mM, [mg2+] = %g mM, T=%g^\\oc C; F = %g pN}}"
		     ,((afork)?"Fork/":""),((a3prime)?"5' and 3' end active":"5' end onlt"),oligo,Na,Mg,Ti,F);
    }
  else
    {
      if ((dsf = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      dsf->nx = dsf->ny = 0;
      dur_op = pr->cur_op;
    }

  if (dur == 0)
    {
      if ((dso15 = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      dso15->nx = dso15->ny = 0;
      set_ds_source(dso15, "Fork/->(oligo) blocking;  seq:%s; [Na+] = %g mM, [mg2+] = %g mM, T=%g C; F = %g pN"
                    ,oligo,Na,Mg,Ti,F);
      if ((dso13 = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      dso13->nx = dso13->ny = 0;
      set_ds_source(dso13, "Fork/->(oligo) blocking;  seq:%s; [Na+] = %g mM, [mg2+] = %g mM, T=%g C; F = %g pN"
                    ,oligo,Na,Mg,Ti,F);

      if ((dso23 = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      dso23->nx = dso23->ny = 0;
      set_ds_source(dso23, "Fork/->(oligo) blocking;  seq:%s; [Na+] = %g mM, [mg2+] = %g mM, T=%g C; F = %g pN"
                    ,oligo,Na,Mg,Ti,F);
      if ((dso25 = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      dso25->nx = dso25->ny = 0;
      set_ds_source(dso25, "Fork/->(oligo) blocking;  seq:%s; [Na+] = %g mM, [mg2+] = %g mM, T=%g C; F = %g pN"
                    ,oligo,Na,Mg,Ti,F);

    }


  //set_dot_line(ds);
  set_ds_source(dsf, "->(Fork)/oligo blocking;  seq:%s; [Na+] = %g mM, [mg2+] = %g mM, T=%g C; F = %g pN"
		,oligo,Na,Mg,Ti,F);

  if (gplo)
     {
      if ((opo = create_and_attach_one_plot(pr, len-1, len-1, 0)) == NULL)
	return win_printf_OK("cannot create plot");
      dsop = opo->dat[0];
       for (k = 0; k < dsop->nx; k++) dsop->yd[k] = 0.5+k;
      set_plot_title(opo, "\\stack{{%soligo blocking %s}"
		     "{seq:%s}"
		     "{\\pt8 [Na+] = %g mM, [mg2+] = %g mM, T=%g^\\oc C; F = %g pN}}"
		     ,((afork)?"Fork/":""),((a3prime)?"5' and 3' end active":"5' end onlt"),oligo,Na,Mg,Ti,F);
                                                                                                                         set_plot_x_title(opo,"Probability of openning");
      set_plot_y_title(opo,"Position along oligo");
     }
  if (gpll)
     {
      if ((opl = create_and_attach_one_plot(pr, len, len, 0)) == NULL)
	return win_printf_OK("cannot create plot");
      dslb = opl->dat[0];
       for (k = 0; k < dslb->nx; k++) dslb->yd[k] = k;
      set_plot_title(opl, "\\stack{{%soligo blocking %s}"
		     "{seq:%s}"
		     "{\\pt8 [Na+] = %g mM, [mg2+] = %g mM, T=%g^\\oc C; F = %g pN}}"
		     ,((afork)?"Fork/":""),((a3prime)?"5' and 3' end active":"5' end onlt"),oligo,Na,Mg,Ti,F);
                                                                                                                         set_plot_x_title(opl,"Nunber of exit events");
      set_plot_y_title(opl,"Base Nb where oligo detached");
     }

  float t = 0, t_1 = 0, dt = 0, den = 0, ttot = 0;
  int fork, fork_1 = 0, oli15_open, oli15_open_1 = 0, len_1 = len - 1;
  int oli13_open = len_1, oli13_open_1 = len_1;
  int oli25_open = -len_1, oli25_open_1 = -len_1;
  int oli23_open = 0, oli23_open_1 = 0;

  for (i = j = 0, fork = 0, oli15_open = 0, oli13_open = len_1; i < nx; j++)
    {
      //fork = len - hp; // measure the nb of base closed in the hairpin
      //fork starts at 0 and increases it is always <= oli15_open
      // oli13_open = len - oli2 ; oli13_open = len no breathing
      // 0 <= fork <= oli15_open < oli13_open <= len
      // detaching when oli15_open == oli13_open


      if (oli15_open < oli13_open)  // we are still attached
	{      // we try to close oligo
          if (oli15_open < 0 || oli15_open >= len_1)
                  win_printf("oli15_open out %d",oli15_open);
          if (oli13_open < 1 || oli13_open > len_1)
                  win_printf("oli13_open out %d",oli13_open);
          if (oli15_open < fork)
                  win_printf("oli15_open %d < fork %d",oli15_open,fork);
          if (fork < 0 || fork > len_1)
                  win_printf("fork %d out of range [0,%d]",fork,len_1);

          // closing oligo 3' end
	  thres[0] = (oli13_open >= len_1 || a3prime == 0) ? 0 : rco;
          // openning oligo 3' end
          thres[1] = thres[0] + ((oli13_open == oli15_open || a3prime == 0) ? 0 : roo[oli13_open - 1]);
          // closing oligo 5' end
          thres[2] = thres[1] + ((oli15_open == 0 || oli15_open <= fork) ? 0 : rco);
          // openning oligo 5' end
          thres[3] = thres[2] + roo[oli15_open];
          // openning fork only if not fully open
          thres[4] = thres[3] + ((fork == 0 || afork == 0) ? 0 : roh[fork-1]);
          // closing fork only if not blocked by oligo
          thres[5] = thres[4] + ((oli15_open <= fork || afork == 0) ? 0 : rch);
          den = thres[5];


          tmp = ran1(&idum);
          dt = -log(tmp)/den;
          t += dt;
          ttot += dt;
          tmp = den*ran1(&idum);
          if (disp != WIN_CANCEL)
            {
              disp = win_printf("Fork %d, Oli 5' %d, oli3' %d\n"
                                "tmp = %g\n"
                                "Oligo 3'end %c ++ if in [0,%g]\n"
                                "Oligo 3'end %c -- if in [%g,%g]\n"
                                "Oligo 5'end %c -- if in [%g,%g]\n"
                                "Oligo 5'end %c ++ if in [%g,%g]\n"
                                "    Fork    %c -- if in [%g,%g]\n"
                                "    Fork    %c ++ if in [%g,%g]\n"
                                ,fork,oli15_open,oli13_open,tmp
                                ,(tmp < thres[0])? 'y':'n',thres[0]
                                ,(tmp >= thres[0] && tmp < thres[1])? 'y':'n',thres[0],thres[1]
                                ,(tmp >= thres[1] && tmp < thres[2])? 'y':'n',thres[1],thres[2]
                                ,(tmp >= thres[2] && tmp < thres[3])? 'y':'n',thres[2],thres[3]
                                ,(tmp >= thres[3] && tmp < thres[4])? 'y':'n',thres[3],thres[4]
                                ,(tmp >= thres[4] && tmp < thres[5])? 'y':'n',thres[4],thres[5]);
            }
          if (tmp < thres[0]) oli13_open++;
          if (tmp >= thres[0] && tmp < thres[1]) oli13_open--;
          if (tmp >= thres[1] && tmp < thres[2]) oli15_open--;
          if (tmp >= thres[2] && tmp < thres[3]) oli15_open++;
          if (tmp >= thres[3] && tmp < thres[4]) fork--;
          if (tmp >= thres[4] && tmp < thres[5]) fork++;

	  for(k = 0; dsop != NULL && k < oli15_open_1 && k < len_1; k++)
            {
                if (k >= 0 && k < dsop->nx)       dsop->xd[k] += dt;
                else win_printf("K out of range %d",k);
            }
	  for(k = oli13_open_1; dsop != NULL && k < len_1; k++)
            {
                if (k >= 0 && k < dsop->nx)       dsop->xd[k] += dt;
                else win_printf("K out of range %d",k);
            }


	  if (dur == 0)
	    {
	      add_new_point_to_ds(dso15, t_1, oli15_open_1);
	      add_new_point_to_ds(dso13, t_1, oli13_open_1);
	      add_new_point_to_ds(dsf, t_1, fork_1);
	      add_new_point_to_ds(dso15, t, oli15_open_1);
	      add_new_point_to_ds(dso13, t, oli13_open_1);
	      add_new_point_to_ds(dsf, t, fork_1);
	      oli15_open_1 = oli15_open;
	      oli13_open_1 = oli13_open;
	      fork_1 = fork;
              t_1 = t;
	      add_new_point_to_ds(dso15, t, oli15_open);
	      add_new_point_to_ds(dso13, t, oli13_open);
	      add_new_point_to_ds(dsf, t, fork);
	      i++;
	    }
          else
            {
	      oli15_open_1 = oli15_open;
	      oli13_open_1 = oli13_open;
	      fork_1 = fork;
              t_1 = t;
            }
	  if (j%65536 == 0)
                my_set_window_title("j %d pos %d %d %d", j,fork,oli15_open,oli13_open);
	}
      else
	{
          if (dslb != NULL)
            {
               dslb->xd[oli15_open] += 1;
            }
	  if (dur == 0)
	    {
	      add_new_point_to_ds(dso15, t, oli15_open);
	      add_new_point_to_ds(dso13, t, oli13_open);
	      add_new_point_to_ds(dsf, t, fork);
	      i++;
	      oli15_open_1 = oli15_open = 0;
	      oli13_open_1 = oli13_open = len_1;
	      fork_1 = fork = 0;
	      add_new_point_to_ds(dso15, t, oli15_open);
	      add_new_point_to_ds(dsf, t, fork);
	    }
	  if (dur == 1)
	    {
	      oli15_open = 0;
	      fork = 0;
              oli13_open = len_1;
	      my_set_window_title("Event %d duration %f %3.1f%%", i,t,(float)(100*i)/nx);
	      add_new_point_to_ds(dsf, i++, t);
              op->need_to_refresh = 1;
              refresh_plot(pr,dur_op);
	      t = 0;
	    }
	}
    }
  for (k = 0; dsop != NULL && ttot > 0 && k < dsop->nx; k++) dsop->xd[k] /= ttot;
  return refresh_plot(pr,pr->n_op-1);
}

#endif

int 	do_unzip_signal_SC_2sides_n(void)
{
        int i, j, k, dur_op = 0; // , i_1, io_1, io
  static int nx = 16384, new_plot = 1;//, timin = 0;
  pltreg *pr;
  O_p *op = NULL, *opo = NULL, *opl = NULL;
  d_s *dso = NULL, *dso2 = NULL, *dsf = NULL, *dsop = NULL, *dslb = NULL;
  static int idum = 4587, end_oligo = 2, dur = 0, afork = 1, a3prime = 1, gplo = 0, gpll = 0, debug = 0, verbose = 0, no_salt_correction = 1, at_penalty = 3, useNN3 = 1;
  static char oligo[128] = {0};
  static float Na = 150, Mg = 10, Ti = 25, F = 8.6, T0 = 273.15, mdt = 1.08;
  float dg[128] = {0};
  float roh[128] = {0};
  float roo[128] = {0};
  float roo_e[128] = {0}; // transition rate when only one or two bases remains
  float thres[10] = {0};
  float R = 1.9872;
  float cor = 1000.0/(R*(T0+Ti)); // cal/K-mol
  float tmp;
  int len, index; //, hp, ol;
  float d, bp, ft, u, gss, gds, rco, rch, dgt, S, gsso;
  char stuff[2048] = {0};
  int disp = WIN_OK;


  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return 1;
  if (strlen(oligo) == 0) sprintf(oligo,"GGGTGTTTGATTGATTTGATTCCTTGGATGTGCGAAG");
				  //"TTTGGCTAAATATCCTAATGTTAAAGTGTATGATAAGCCTACTACAGTAGATTTTGACGGGTGTTTGATTGATTTGATTCCTTGGATGTGCGAAG"); //TTTGGCTAAATATCCTAATGTTAAAGTGTATGATAAGCCTACTACAGTAGATTTTGACGGGTGTTTGATTGATTTGATTCCTTGGATGTGCGAAG"); // ACAGCCAGCCGA "GGGTGTTTGATTGATTTGATTCCTTGGATGTGCGAAG" "ACAGCCAGCCGA"

  i = win_scanf("Generate an unzipping signal of a fork with a blocking oligo "
                "Following Simona Cocco model\nwhere fork and oligo openning "
                "are not bound but where the fork is behind the oligo opening\n"
                "Allows the second end of the oligo to open\n"
                "Specify the oligo sequence %s\n"
                "Oligo 1/2-initiation correction at:\n"
                "%R->none %r->on 5' only %r->on 3'only %r->on both 5' and 3' %r->on the last 2 bases\n"
                "Oligo AT penalty at the ends:\n"
                "%R->none %r->on 5' only %r->on 3'only %r->on both 5' and 3' \n"
                "%b->No salt entropy correction \n"
                "specify the ratio of charge for dsDNA %4f\n"
                "%R->Mfold %r->NN3 parameters\n"
                //"To take in account Ionic cloud add 125 mM to [Na+]\n"
                "[Na+] = %4f mM; [Mg2+] = %4f mM; T = %4f \\oc C\n"
                "Force = %4f pN\n"
                "%R in this plot %r in a new plot\n"
                "Total number of steps %8d\n"
                "Random seed %8d\n"
                "%R real time or %r duration \n"
                "%bFork active %b3-end of oligo active\n"
                "%b Generate plot with average openning probability\n"
                "%b Genertae a plot with statitics of last base attached\n"
                "%b debugging\n"
                "Time conversion in \\mu s %4f\n"
                "%b Display energies\n"
                ,oligo,&end_oligo,&at_penalty,&no_salt_correction,&dsCharge,&useNN3,&Na,&Mg,&Ti,&F
                ,&new_plot,&nx,&idum,&dur,&afork,&a3prime,&gplo,&gpll,&debug,&mdt,&verbose);
  if (i == WIN_CANCEL)	return D_O_K;

  cor = 1000.0/(R*(T0+Ti)); // convert cal/K-mol in kBT
  if (useNN3)   init_NN3_array(Na, Mg, Ti, verbose, no_salt_correction, dsCharge);
  else          init_Mfold31_array(Na, Mg, Ti, verbose, no_salt_correction, dsCharge);
  if (debug == 0)  disp = WIN_CANCEL;
  len = strlen(oligo);

  for (i = 1; i < len; i++)
    {
      index = 0;
      if ((oligo[i-1]&0x5F) == 'A')  index = 0;
      if ((oligo[i-1]&0x5F) == 'C')  index = 4;
      if ((oligo[i-1]&0x5F) == 'G')  index = 8;
      if ((oligo[i-1]&0x5F) == 'T')  index = 12;
      if ((oligo[i]&0x5F) == 'A')  index += 0;
      if ((oligo[i]&0x5F) == 'C')  index += 1;
      if ((oligo[i]&0x5F) == 'G')  index += 2;
      if ((oligo[i]&0x5F) == 'T')  index += 3;
      dg[i-1] = -cor*DGMF[index];
      if (i < 12)
              sprintf(stuff+strlen(stuff),"DeltaG %c%c %g Kcal/mol %g kBT\n",oligo[i-1],oligo[i],-DGMF[index],dg[i-1]);
    }

  for(i = 0; i < len-1; i++)
    {
      roo[i] = roo_e[i] = exp(-dg[i]);
      roh[i] = exp(-dg[i]);
    }
  dgt = 0;
  if (end_oligo & 0x1)
    {
        dgt = -cor*DGMF[INGC];//-cor*DGMF[IN_2];
    }
  if (at_penalty & 0x1)
    {
        dgt +=(((oligo[0]&0x5F) == 'A') || ((oligo[0]&0x5F) == 'T')) ? -cor*(DGMF[INAT] - DGMF[INGC]) : 0;
    }
  roo[0]= exp(-dg[0]-dgt);
  roo_e[0]= exp(-dg[0]-dgt);
  // no correction on hp

  printf("roo[0] = %g kBt dgt = %g kBt\n",-dg[0]-dgt,dgt);
  fflush(stdout);
  dgt = 0;
  if (end_oligo & 0x2)
    {
      dgt = -cor*DGMF[INGC];//-cor*DGMF[IN_2];
    }
  if (at_penalty & 0x2)
    {
        dgt +=(((oligo[len-1]&0x5F) == 'A') || ((oligo[len-1]&0x5F) == 'T')) ? -cor*(DGMF[INAT] - DGMF[INGC]) : 0;
    }
  roo[len-2]= exp(-dg[len-2]-dgt);
  roo_e[len-2]= exp(-dg[len-2]-dgt);

  printf("roo[%d] = %g kBt, dgt = %g kBt\n",len-2,-dg[len-2]-dgt,dgt);
  fflush(stdout);
  float dg0, dg1, dg2, dg3, dg4;
  for(i = 0; i < len-1; i++)
    {
        dgt = dg0 = -cor*DGMF[INGC];//-cor*DGMF[IN_2];
        dg1 = dg2 = dg3 = dg4 =0;
        if ((i == 0) && (at_penalty & 0x1))
            {
                dg1 = (((oligo[0]&0x5F) == 'A') || ((oligo[0]&0x5F) == 'T')) ? -cor*(DGMF[INAT] - DGMF[INGC]) : 0;
                dgt += dg1;
            }
        if ((i == 0) && end_oligo & 0x1)
            {
                dg2 = -cor*DGMF[INGC];//-cor*DGMF[IN_2];
                dgt += dg2;
            }
        if ((i == len - 2) && (at_penalty & 0x2))
            {
                dg3 = (((oligo[i+1]&0x5F) == 'A') || ((oligo[i+1]&0x5F) == 'T')) ? -cor*(DGMF[INAT] - DGMF[INGC]) : 0;
                dgt += dg3;
            }
        if ((i == len -2) && end_oligo & 0x2)
            {
                dg4 = -cor*DGMF[INGC];//-cor*DGMF[IN_2];
                dgt += dg4;
            }
        roo_e[i] = exp(-dg[i]-dgt);
        printf("For roo_e[%d] = %g kBt, dg[%d] %g kBt + %g + %g + %g + %g + %g= %g\n"
               ,i,roo_e[i],i,dg[i] ,dg0,dg1,dg2,dg3,dg4,dg[i]+dgt);
    }
  fflush(stdout);
  // no correction on hp

  //elasticity models for the closing rates for the ds and ss DNA
  //d = 0.56*1.12;
  //bp = 1.5*1.125;
  //d = 0.605;// # 0.567 #0.56*1.12 new 0.605
  //bp = 1.828; // # 1.78 # 1.5*1.125 new 1.828
  d = 0.542; // nm
  bp = 2.14; // nm
  S = 216;  // pN
  ft = 4.1*(T0+Ti)/(T0+25);
  u = bp*F/ft;
  gss = d/bp*(log(sinh(u)/u)+ 0.5*F*F/(ft*S));
  gsso = d/bp*log(sinh(u)/u);
  gds = (F/ft - sqrt(F/ft/50) + 0.5*F*F/ft/1230)*0.34;


  //closing rates:only depend on the force
  rch = exp(-2*gss);
  rco = exp(-gss+gds);

  win_printf("rch new = %g old = %g\n"
	     "rco new = %g old = %g\n",rch,exp(-2*gsso),rco,exp(-gsso+gds));

  printf("rch new = %g old = %g\n"
	     "rco new = %g old = %g\n",rch,exp(-2*gsso),rco,exp(-gsso+gds));
  fflush(stdout);
  for (i = 0; i < len-1 && i < 12; i++)
    sprintf(stuff+strlen(stuff),"roo[%d] = %g; roo_e[%d] = %g roh[%d] = %g;\n",i,roo[i],i,roo_e[i],i, roh[i]);
  if (i < len ) sprintf(stuff+strlen(stuff),"...\nrco = %g; rch = %g;\n",rco, rch);
  else sprintf(stuff+strlen(stuff),"rco = %g; rch = %g;\n",rco, rch);

  win_printf("%s",stuff);
  printf("%s",stuff);
  fflush(stdout);

  if (new_plot)
    {
      if ((op = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create plot");
      dsf = op->dat[0];
      dsf->nx = dsf->ny = 0;
      dur_op = pr->n_op-1;
      set_plot_title(op, "\\stack{{%soligo blocking %s}"
		     "{seq:%s}"
		     "{\\pt8 [Na+] = %g mM, [mg2+] = %g mM, T=%g^\\oc C; F = %g pN}}"
		     ,((afork)?"Fork/":""),((a3prime)?"5' and 3' end active":"5' end onlt"),oligo,Na,Mg,Ti,F);

      create_attach_select_x_un_to_op(op, IS_SECOND, 0, mdt, -6, 0, "\\mu s");
      if (dur)       create_attach_select_y_un_to_op(op, IS_SECOND, 0, mdt, -6, 0, "\\mu s");

    }
  else
    {
      if ((dsf = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      dsf->nx = dsf->ny = 0;
      dur_op = pr->cur_op;
    }

  if (dur == 0)
    {
      if ((dso = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      dso->nx = dso->ny = 0;
      set_ds_source(dso, "Fork/->(oligo) blocking;  seq:%s; [Na+] = %g mM, [mg2+] = %g mM, T=%g C; F = %g pN"
                    ,oligo,Na,Mg,Ti,F);
      if ((dso2 = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      dso2->nx = dso2->ny = 0;
      set_ds_source(dso2, "Fork/->(oligo) blocking;  seq:%s; [Na+] = %g mM, [mg2+] = %g mM, T=%g C; F = %g pN"
                    ,oligo,Na,Mg,Ti,F);
    }


  //set_dot_line(ds);
  set_ds_source(dsf, "->(Fork)/oligo blocking;  seq:%s; [Na+] = %g mM, [mg2+] = %g mM, T=%g C; F = %g pN"
		,oligo,Na,Mg,Ti,F);

  if (gplo)
     {
      if ((opo = create_and_attach_one_plot(pr, len-1, len-1, 0)) == NULL)
	return win_printf_OK("cannot create plot");
      dsop = opo->dat[0];
       for (k = 0; k < dsop->nx; k++) dsop->yd[k] = 0.5+k;
      set_plot_title(opo, "\\stack{{%soligo blocking %s}"
		     "{seq:%s}"
		     "{\\pt8 [Na+] = %g mM, [mg2+] = %g mM, T=%g^\\oc C; F = %g pN}}"
		     ,((afork)?"Fork/":""),((a3prime)?"5' and 3' end active":"5' end onlt"),oligo,Na,Mg,Ti,F);
      set_plot_x_title(opo,"Probability of openning");
      set_plot_y_title(opo,"Position along oligo");
     }
  if (gpll)
     {
      if ((opl = create_and_attach_one_plot(pr, len, len, 0)) == NULL)
	return win_printf_OK("cannot create plot");
      dslb = opl->dat[0];
       for (k = 0; k < dslb->nx; k++) dslb->yd[k] = k;
      set_plot_title(opl, "\\stack{{%soligo blocking %s}"
		     "{seq:%s}"
		     "{\\pt8 [Na+] = %g mM, [mg2+] = %g mM, T=%g^\\oc C; F = %g pN}}"
		     ,((afork)?"Fork/":""),((a3prime)?"5' and 3' end active":"5' end onlt"),oligo,Na,Mg,Ti,F);
      set_plot_x_title(opl,"Nunber of exit events");
      set_plot_y_title(opl,"Base Nb where oligo detached");
     }

  float t = 0, t_1 = 0, dt = 0, den = 0, ttot = 0;
  int fork, fork_1 = 0, oli_open, oli_open_1 = 0, len_1 = len - 1;
  int oli2_open = len_1, oli2_open_1 = len_1, bp_remain = 0;

  for (i = j = 0, fork = 0, oli_open = 0, oli2_open = len_1; i < nx; j++)
    {
      //fork = len - hp; // measure the nb of base closed in the hairpin
      //fork starts at 0 and increases it is always <= oli_open
      // oli2_open = len - oli2 ; oli2_open = len no breathing
      // 0 <= fork <= oli_open < oli2_open <= len
      // detaching when oli_open == oli2_open

      bp_remain = oli2_open - oli_open;
      if (oli_open < oli2_open)  // we are still attached
	{      // we try to close oligo
          if (oli_open < 0 || oli_open >= len_1)
                  win_printf("oli_open out %d",oli_open);
          if (oli2_open < 1 || oli2_open > len_1)
                  win_printf("oli2_open out %d",oli2_open);
          if (oli_open < fork)
                  win_printf("oli_open %d < fork %d",oli_open,fork);
          if (fork < 0 || fork >= len_1)
                  win_printf("fork %d out of range [0,%d[",fork,len_1);

          // closing oligo 3' end
	  thres[0] = (oli2_open >= len_1 || a3prime == 0) ? 0 : rco;
          // openning oligo 3' end
	  tmp = (end_oligo == 4 && bp_remain < 3) ? roo_e[oli2_open-1] : roo[oli2_open - 1];
	  tmp = (oli2_open == oli_open || a3prime == 0) ? 0 : tmp;
	  tmp = (tmp < 0) ? 0 : tmp;
          thres[1] = thres[0] + tmp;
	  if (thres[1] < thres[0])
	    win_printf("thres[1]=%g < thres[0]=%g !",thres[1],thres[0]);
          // closing oligo 5' end
	  thres[2] = thres[1];
          thres[2] += (oli_open == 0 || oli_open <= fork) ? 0 : rco;
	  if (thres[2] < thres[1])
	    win_printf("thres[2]=%g < thres[1]=%g !",thres[2],thres[1]);
          // openning oligo 5' end
	  tmp = (end_oligo == 4 && bp_remain < 3) ? roo_e[oli_open] : roo[oli_open];
	  tmp = (tmp < 0) ? 0 : tmp;
          thres[3] = thres[2] + tmp;
	  if (thres[3] < thres[2])
	    win_printf("thres[3]=%g < thres[2]=%g !",thres[3],thres[2]);
          // openning fork only if not fully open
          thres[4] = thres[3];
	  thres[4] += (fork == 0 || afork == 0) ? 0 : roh[fork-1];
	  if (thres[4] < thres[3])
	    win_printf("thres[4]=%g < thres[3]=%g !",thres[4],thres[3]);
          // closing fork only if not blocked by oligo
          thres[5] = thres[4];
	  thres[5] += ((oli_open <= fork || afork == 0) ? 0 : rch);
	  if (thres[5] < thres[4])
	    win_printf("thres[5]=%g < thres[4]=%g !",thres[5],thres[4]);
          den = thres[5];


          tmp = ran1(&idum);
          dt = -log(tmp)/den;
          t += dt;
          ttot += dt;
          tmp = den*ran1(&idum);
          if (disp != WIN_CANCEL)
            {
              disp = win_printf("Fork %d, Oli 5' %d, oli3' %d\n"
                                "tmp = %g\n"
                                "Oligo 3'end %c ++ if in [0,%g]\n"
                                "Oligo 3'end %c -- if in [%g,%g]\n"
                                "Oligo 5'end %c -- if in [%g,%g]\n"
                                "Oligo 5'end %c ++ if in [%g,%g]\n"
                                "    Fork    %c -- if in [%g,%g]\n"
                                "    Fork    %c ++ if in [%g,%g]\n"
                                ,fork,oli_open,oli2_open,tmp
                                ,(tmp < thres[0])? 'y':'n',thres[0]
                                ,(tmp >= thres[0] && tmp < thres[1])? 'y':'n',thres[0],thres[1]
                                ,(tmp >= thres[1] && tmp < thres[2])? 'y':'n',thres[1],thres[2]
                                ,(tmp >= thres[2] && tmp < thres[3])? 'y':'n',thres[2],thres[3]
                                ,(tmp >= thres[3] && tmp < thres[4])? 'y':'n',thres[3],thres[4]
                                ,(tmp >= thres[4] && tmp < thres[5])? 'y':'n',thres[4],thres[5]);
            }
          if (tmp < thres[0]) oli2_open++;
          if (tmp >= thres[0] && tmp < thres[1]) oli2_open--;
          if (tmp >= thres[1] && tmp < thres[2]) oli_open--;
          if (tmp >= thres[2] && tmp < thres[3]) oli_open++;
          if (tmp >= thres[3] && tmp < thres[4]) fork--;
          if (tmp >= thres[4] && tmp < thres[5]) fork++;

	  for(k = 0; dsop != NULL && k < oli_open_1 && k < len_1; k++)
            {
                if (k >= 0 && k < dsop->nx)       dsop->xd[k] += dt;
                else win_printf("K out of range %d",k);
            }
	  for(k = oli2_open_1; dsop != NULL && k < len_1; k++)
            {
                if (k >= 0 && k < dsop->nx)       dsop->xd[k] += dt;
                else win_printf("K out of range %d",k);
            }


	  if (dur == 0)
	    {
	      add_new_point_to_ds(dso, t_1, oli_open_1);
	      add_new_point_to_ds(dso2, t_1, oli2_open_1);
	      add_new_point_to_ds(dsf, t_1, fork_1);
	      add_new_point_to_ds(dso, t, oli_open_1);
	      add_new_point_to_ds(dso2, t, oli2_open_1);
	      add_new_point_to_ds(dsf, t, fork_1);
	      oli_open_1 = oli_open;
	      oli2_open_1 = oli2_open;
	      fork_1 = fork;
              t_1 = t;
	      add_new_point_to_ds(dso, t, oli_open);
	      add_new_point_to_ds(dso2, t, oli2_open);
	      add_new_point_to_ds(dsf, t, fork);
	      i++;
	    }
          else
            {
	      oli_open_1 = oli_open;
	      oli2_open_1 = oli2_open;
	      fork_1 = fork;
              t_1 = t;
            }
	  if (j%65536 == 0)
                my_set_window_title("j %d pos %d %d %d", j,fork,oli_open,oli2_open);
	}
      else
	{
          if (dslb != NULL)
            {
               dslb->xd[oli_open] += 1;
            }
	  if (dur == 0)
	    {
	      add_new_point_to_ds(dso, t, oli_open);
	      add_new_point_to_ds(dso2, t, oli2_open);
	      add_new_point_to_ds(dsf, t, fork);
	      i++;
	      oli_open_1 = oli_open = 0;
	      oli2_open_1 = oli2_open = len_1;
	      fork_1 = fork = 0;
	      add_new_point_to_ds(dso, t, oli_open);
	      add_new_point_to_ds(dsf, t, fork);
	    }
	  if (dur == 1)
	    {
	      oli_open = 0;
	      fork = 0;
              oli2_open = len_1;
	      my_set_window_title("Event %d duration %f %3.1f%%", i,t,(float)(100*i)/nx);
	      add_new_point_to_ds(dsf, i++, t);
              for (k = 0, tmp = 0; k < dsf->nx; k++) tmp += dsf->yd[k];
              if (dsf->nx > 0) tmp/=dsf->nx;
	      tmp *= op->dy;
              set_plot_title(op, "\\stack{{%soligo blocking %s}"
                             "{seq:%s}"
                             "{\\pt8 [Na+] = %g mM, [mg2+] = %g mM, T=%g^\\oc C; F = %g pN}{Mean duration %g}}"
                             ,((afork)?"Fork/":"")
                             ,((a3prime)?"5' and 3' end active":"5' end onlt")
                             ,oligo,Na,Mg,Ti,F,tmp);

              op->need_to_refresh = 1;
              refresh_plot(pr,dur_op);
	      t = 0;
	    }
	}
    }
  for (k = 0; dsop != NULL && ttot > 0 && k < dsop->nx; k++) dsop->xd[k] /= ttot;
  return refresh_plot(pr,pr->n_op-1);
}

int 	do_unzip_signal_SC_2sides_n_sense_antisense(void)
{
    int i, j, k, dur_op = 0, oli_attach = 1, olia_attach = 1; // , i_1, io_1, io
    static int nx = 16384, new_plot = 1;//, timin = 0;
    pltreg *pr;
    O_p *op = NULL, *opo = NULL, *opl = NULL;
    d_s *dso = NULL, *dso2 = NULL, *dsf = NULL, *dsop = NULL, *dslb = NULL, *dsoa = NULL, *dsoa2 = NULL;
    static int idum = 4587, end_oligo = 4, dur = 0, afork = 1, a3prime = 1, gplo = 0, gpll = 0, debug = 0;
    static int verbose = 0, no_salt_correction = 0, at_penalty = 3, useNN3 = 1, oli_attach_0 = 1, olia_attach_0 = 1;
    static char oligo[128] = {0};
    static float Na = 150, Mg = 0, Ti = 25, F = 8.6, T0 = 273.15, mdt = 2.5;
    float dg[128] = {0};
    float roh[128] = {0};
    float roo[128] = {0};
    float roo_e[128] = {0}; // transition rate when only one or two bases remains
    float thres[10] = {0};
    float R = 1.9872;
    float cor = 1000.0/(R*(T0+Ti)); // cal/K-mol
    float tmp;
    int len, index; //, hp, ol;
    float d, bp, ft, u, gss, gds, rco, rch, dgt, S, gsso;
    char stuff[2048] = {0};
    int disp = WIN_OK;

    //"TTTGGCTAAATATCCTAATGTTAAAGTGTATGATAAGCCTACTACAGTAGATTTTGACGGGTGTTTGATTGATTTGATTCCTTGGATGTGCGAAG");
    //TTTGGCTAAATATCCTAATGTTAAAGTGTATGATAAGCCTACTACAGTAGATTTTGACGGGTGTTTGATTGATTTGATTCCTTGGATGTGCGAAG");
    // ACAGCCAGCCGA "GGGTGTTTGATTGATTTGATTCCTTGGATGTGCGAAG" "ACAGCCAGCCGA"

    if(updating_menu_state != 0)	return D_O_K;
    if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return 1;
    if (strlen(oligo) == 0) sprintf(oligo,"ACGTACGT");//"GGGTGTTTGATTGATTTGATTCCTTGGATGTGCGAAG");
    i = win_scanf("Generate an unzipping signal of a fork with sense and antisense blocking oligos "
                  "Following Simona Cocco model\nwhere fork and oligo openning "
                  "are not bound but where the fork is behind the oligo opening\n"
                  "Allows the second end of the oligo to open\n"
                  "Specify the oligo sequence %s\n"
                  "Initial condition: %b->sense Oligo attached %b->antisense Oligo attached"
                  "Oligo 1/2-initiation correction at:\n"
                  "%R->none %r->on 5' only %r->on 3'only %r->on both 5' and 3' %r->on the last 2 bases\n"
                  "Oligo AT penalty at the ends:\n"
                  "%R->none %r->on 5' only %r->on 3'only %r->on both 5' and 3' \n"
                  "%b->No salt entropy correction \n"
                  "specify the ratio of charge for dsDNA %4f\n"
                  "%R->Mfold %r->NN3 parameters\n"
                  //"To take in account Ionic cloud add 125 mM to [Na+]\n"
                  "[Na+] = %4f mM; [Mg2+] = %4f mM; T = %4f \\oc C\n"
                  "Force = %4f pN\n"
                  "%R in this plot %r in a new plot\n"
                  "Total number of steps %8d\n"
                  "Random seed %8d\n"
                  "%R real time or %r duration \n"
                  "%bFork active %b3-end of oligo active\n"
                  "%b Generate plot with average openning probability\n"
                  "%b Genertae a plot with statitics of last base attached\n"
                  "%b debugging\n"
                  "Time conversion in \\mu s %4f\n"
                  "%b Display energies\n"
                  ,oligo,&oli_attach_0, &olia_attach_0,&end_oligo,&at_penalty,&no_salt_correction,&dsCharge,&useNN3,&Na,&Mg,&Ti,&F
                  ,&new_plot,&nx,&idum,&dur,&afork,&a3prime,&gplo,&gpll,&debug,&mdt,&verbose);
    if (i == WIN_CANCEL)	return D_O_K;

    cor = 1000.0/(R*(T0+Ti)); // convert cal/K-mol in kBT
    if (useNN3)   init_NN3_array(Na, Mg, Ti, verbose, no_salt_correction, dsCharge);
    else          init_Mfold31_array(Na, Mg, Ti, verbose, no_salt_correction, dsCharge);
    if (debug == 0)  disp = WIN_CANCEL;
    len = strlen(oligo);

    for (i = 1; i < len; i++)
        {
            index = 0;
            if ((oligo[i-1]&0x5F) == 'A')  index = 0;
            if ((oligo[i-1]&0x5F) == 'C')  index = 4;
            if ((oligo[i-1]&0x5F) == 'G')  index = 8;
            if ((oligo[i-1]&0x5F) == 'T')  index = 12;
            if ((oligo[i]&0x5F) == 'A')  index += 0;
            if ((oligo[i]&0x5F) == 'C')  index += 1;
            if ((oligo[i]&0x5F) == 'G')  index += 2;
            if ((oligo[i]&0x5F) == 'T')  index += 3;
            dg[i-1] = -cor*DGMF[index];
            if (i < 12)
                sprintf(stuff+strlen(stuff),"DeltaG %c%c %g Kcal/mol %g kBT\n",oligo[i-1],oligo[i],-DGMF[index],dg[i-1]);
        }

    for(i = 0; i < len-1; i++)
        {
            roo[i] = roo_e[i] = exp(-dg[i]);
            roh[i] = exp(-dg[i]);
        }
    dgt = 0;
    if (end_oligo & 0x1)
        {
            dgt = -cor*DGMF[INGC];//-cor*DGMF[IN_2];
        }
    if (at_penalty & 0x1)
        {
            dgt +=(((oligo[0]&0x5F) == 'A') || ((oligo[0]&0x5F) == 'T')) ? -cor*(DGMF[INAT] - DGMF[INGC]) : 0;
        }
    roo[0]= exp(-dg[0]-dgt);
    roo_e[0]= exp(-dg[0]-dgt);
    // no correction on hp

    printf("roo[0] = %g kBt dgt = %g kBt\n",-dg[0]-dgt,dgt);
    fflush(stdout);
    dgt = 0;
    if (end_oligo & 0x2)
        {
            dgt = -cor*DGMF[INGC];//-cor*DGMF[IN_2];
        }
    if (at_penalty & 0x2)
        {
            dgt +=(((oligo[len-1]&0x5F) == 'A') || ((oligo[len-1]&0x5F) == 'T')) ? -cor*(DGMF[INAT] - DGMF[INGC]) : 0;
        }
    roo[len-2]= exp(-dg[len-2]-dgt);
    roo_e[len-2]= exp(-dg[len-2]-dgt);

    printf("roo[%d] = %g kBt, dgt = %g kBt\n",len-2,-dg[len-2]-dgt,dgt);
    fflush(stdout);
    float dg0, dg1, dg2, dg3, dg4;
    for(i = 0; i < len-1; i++)
        {
            dgt = dg0 = -cor*DGMF[INGC];//-cor*DGMF[IN_2];
            dg1 = dg2 = dg3 = dg4 =0;
            if ((i == 0) && (at_penalty & 0x1))
                {
                    dg1 = (((oligo[0]&0x5F) == 'A') || ((oligo[0]&0x5F) == 'T')) ? -cor*(DGMF[INAT] - DGMF[INGC]) : 0;
                    dgt += dg1;
                }
            if ((i == 0) && end_oligo & 0x1)
                {
                    dg2 = -cor*DGMF[INGC];//-cor*DGMF[IN_2];
                    dgt += dg2;
                }
            if ((i == len - 2) && (at_penalty & 0x2))
                {
                    dg3 = (((oligo[i+1]&0x5F) == 'A') || ((oligo[i+1]&0x5F) == 'T')) ? -cor*(DGMF[INAT] - DGMF[INGC]) : 0;
                    dgt += dg3;
                }
            if ((i == len -2) && end_oligo & 0x2)
                {
                    dg4 = -cor*DGMF[INGC];//-cor*DGMF[IN_2];
                    dgt += dg4;
                }
            roo_e[i] = exp(-dg[i]-dgt);
            printf("For roo_e[%d] = %g kBt, dg[%d] %g kBt + %g + %g + %g + %g + %g= %g\n"
                   ,i,roo_e[i],i,dg[i] ,dg0,dg1,dg2,dg3,dg4,dg[i]+dgt);
        }
    fflush(stdout);
    // no correction on hp

    //elasticity models for the closing rates for the ds and ss DNA
    //d = 0.56*1.12;
    //bp = 1.5*1.125;
    //d = 0.605;// # 0.567 #0.56*1.12 new 0.605
    //bp = 1.828; // # 1.78 # 1.5*1.125 new 1.828
    d = 0.542; // nm
    bp = 2.14; // nm
    S = 216;  // pN
    ft = 4.1*(T0+Ti)/(T0+25);
    u = bp*F/ft;
    gss = d/bp*(log(sinh(u)/u)+ 0.5*F*F/(ft*S));
    gsso = d/bp*log(sinh(u)/u);
    gds = (F/ft - sqrt(F/ft/50) + 0.5*F*F/ft/1230)*0.34;


    //closing rates:only depend on the force
    rch = exp(-2*gss);
    rco = exp(-gss+gds);

    win_printf("rch new = %g old = %g\n"
               "rco new = %g old = %g\n",rch,exp(-2*gsso),rco,exp(-gsso+gds));

    printf("rch new = %g old = %g\n"
           "rco new = %g old = %g\n",rch,exp(-2*gsso),rco,exp(-gsso+gds));
    fflush(stdout);
    for (i = 0; i < len-1 && i < 12; i++)
        sprintf(stuff+strlen(stuff),"roo[%d] = %g; roo_e[%d] = %g roh[%d] = %g;\n",i,roo[i],i,roo_e[i],i, roh[i]);
    if (i < len ) sprintf(stuff+strlen(stuff),"...\nrco = %g; rch = %g;\n",rco, rch);
    else sprintf(stuff+strlen(stuff),"rco = %g; rch = %g;\n",rco, rch);

    win_printf("%s",stuff);
    printf("%s",stuff);
    fflush(stdout);

    if (new_plot)
        {
            if ((op = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
                return win_printf_OK("cannot create plot");
            dsf = op->dat[0];
            dsf->nx = dsf->ny = 0;
            dur_op = pr->n_op-1;
            set_plot_title(op, "\\stack{{%s%s%s}{blocking %s}"
                           "{seq:%s}"
                           "{\\pt8 [Na+] = %g mM, [mg2+] = %g mM, T=%g^\\oc C; F = %g pN}}"
                           ,((afork)?"Fork/":""),(oli_attach_0)?" Sense oligo":""
                           ,(olia_attach_0)?" Antisense oligo":""
                           ,((a3prime)?"5' and 3' end active":"5' end onlt"),oligo,Na,Mg,Ti,F);

            create_attach_select_x_un_to_op(op, IS_SECOND, 0, mdt, -6, 0, "\\mu s");
            if (dur)       create_attach_select_y_un_to_op(op, IS_SECOND, 0, mdt, -6, 0, "\\mu s");

        }
    else
        {
            if ((dsf = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
                return win_printf_OK("cannot create data set");
            dsf->nx = dsf->ny = 0;
            dur_op = pr->cur_op;
        }

    if (dur == 0)
        {
            if ((dso = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
                return win_printf_OK("cannot create data set");
            dso->nx = dso->ny = 0;
            set_ds_source(dso, "Fork/->(oligo) blocking;  seq:%s; [Na+] = %g mM, [mg2+] = %g mM, T=%g C; F = %g pN"
                          ,oligo,Na,Mg,Ti,F);
            if ((dso2 = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
                return win_printf_OK("cannot create data set");
            dso2->nx = dso2->ny = 0;
            set_ds_source(dso2, "Fork/->(oligo) blocking;  seq:%s; [Na+] = %g mM, [mg2+] = %g mM, T=%g C; F = %g pN"
                          ,oligo,Na,Mg,Ti,F);
            if ((dsoa = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
                return win_printf_OK("cannot create data set");
            dsoa->nx = dsoa->ny = 0;
            set_ds_source(dsoa, "Fork/->(oligo antisense) blocking;  seq:%s; [Na+] = %g mM, [mg2+] = %g mM, T=%g C; F = %g pN"
                          ,oligo,Na,Mg,Ti,F);
            if ((dsoa2 = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
                return win_printf_OK("cannot create data set");
            dsoa2->nx = dsoa2->ny = 0;
            set_ds_source(dsoa2, "Fork/->(oligo antisense) blocking;  seq:%s; [Na+] = %g mM, [mg2+] = %g mM, T=%g C; F = %g pN"
                          ,oligo,Na,Mg,Ti,F);
        }


    //set_dot_line(ds);
    set_ds_source(dsf, "->(Fork)/oligo blocking;  seq:%s; [Na+] = %g mM, [mg2+] = %g mM, T=%g C; F = %g pN"
                  ,oligo,Na,Mg,Ti,F);

    if (gplo)
        {
            if ((opo = create_and_attach_one_plot(pr, len-1, len-1, 0)) == NULL)
                return win_printf_OK("cannot create plot");
            dsop = opo->dat[0];
            for (k = 0; k < dsop->nx; k++) dsop->yd[k] = 0.5+k;
            set_plot_title(opo, "\\stack{{%s%s%s}{blocking %s}"
                           "{seq:%s}"
                           "{\\pt8 [Na+] = %g mM, [mg2+] = %g mM, T=%g^\\oc C; F = %g pN}}"
                           ,((afork)?"Fork/":""),(oli_attach_0)?" Sense oligo":""
                           ,(olia_attach_0)?" Antisense oligo":""
                           ,((a3prime)?"5' and 3' end active":"5' end onlt"),oligo,Na,Mg,Ti,F);
            set_plot_x_title(opo,"Probability of openning");
            set_plot_y_title(opo,"Position along oligo");
        }
    if (gpll)
        {
            if ((opl = create_and_attach_one_plot(pr, len, len, 0)) == NULL)
                return win_printf_OK("cannot create plot");
            dslb = opl->dat[0];
            for (k = 0; k < dslb->nx; k++) dslb->yd[k] = k;
            set_plot_title(opl, "\\stack{{%s%s%s}{blocking %s}"
                           "{seq:%s}"
                           "{\\pt8 [Na+] = %g mM, [mg2+] = %g mM, T=%g^\\oc C; F = %g pN}}"
                           ,((afork)?"Fork/":""),(oli_attach_0)?" Sense oligo":""
                           ,(olia_attach_0)?" Antisense oligo":""
                           ,((a3prime)?"5' and 3' end active":"5' end onlt"),oligo,Na,Mg,Ti,F);
            set_plot_x_title(opl,"Nunber of exit events");
            set_plot_y_title(opl,"Base Nb where oligo detached");
        }

    float t = 0, t_1 = 0, dt = 0, den = 0, ttot = 0;
    int fork, fork_1 = 0, oli_open, oli_open_1 = 0, len_1 = len - 1;
    int oli2_open = len_1, oli2_open_1 = len_1, bp_remain = 0;
    int olia_open, olia_open_1 = 0, olia2_open = len_1, olia2_open_1 = len_1; // antisense oligo
    int  bpa_remain = 0;
    oli_attach = oli_attach_0; olia_attach = olia_attach_0;
    for (i = j = 0, fork = 0, oli_open = 0, olia_open = 0, oli2_open = len_1, olia2_open = len_1; i < nx; j++)
        {
            //fork = len - hp; // measure the nb of base closed in the hairpin
            //fork starts at 0 and increases it is always <= oli_open
            // oli2_open = len - oli2 ; oli2_open = len no breathing
            // 0 <= fork <= oli_open && olia_open < oli2_open && olia2_open <= len
            // detaching when oli_open == oli2_open && olia_open == olia2_open

            bp_remain = oli2_open - oli_open;
            bpa_remain = olia2_open - olia_open;
            oli_attach = (oli_open < oli2_open) ? oli_attach : 0;
            olia_attach = (olia_open < olia2_open) ? olia_attach : 0;
            if (oli_attach > 0 || olia_attach > 0) //oli_open < oli2_open && olia_open < olia2_open)  // we are still attached
                {      // we try to close oligo
                    if (oli_attach && (oli_open < 0 || oli_open >= len_1))
                        win_printf("oli_open out %d oli attached %d",oli_open,oli_attach);
                    if (oli_attach && (oli2_open < 1 || oli2_open > len_1))
                        win_printf("oli2_open out %d oli attached %d",oli2_open,oli_attach);
                    if (olia_attach && (olia_open < 0 || olia_open >= len_1))
                        win_printf("olia_open out %d oli attached %d",olia_open,olia_attach);
                    if (olia_attach && (olia2_open < 1 || olia2_open > len_1))
                        win_printf("olia2_open out %d oli attached %d",olia2_open,olia_attach);
                    if (oli_attach && (oli_open < fork))
                        win_printf("oli_open %d < fork %d oli attached %d",oli_open,fork,olia_attach);
                    if (olia_attach && (olia_open < fork))
                        win_printf("olia_open %d < fork %d",olia_open,fork);
                    if (fork < 0 || fork >= len_1)
                        win_printf("fork %d out of range [0,%d[",fork,len_1);

                    // closing oligo 3' end
                    thres[0] = (oli2_open >= len_1 || a3prime == 0 || oli_attach == 0) ? 0 : rco;
                    // openning oligo 3' end
                    tmp = (end_oligo == 4 && bp_remain < 3) ? roo_e[oli2_open-1] : roo[oli2_open - 1];
                    tmp = (oli2_open == oli_open || a3prime == 0|| oli_attach == 0) ? 0 : tmp;
                    tmp = (tmp < 0) ? 0 : tmp;
                    thres[1] = thres[0] + tmp;
                    if (thres[1] < thres[0])
                        win_printf("thres[1]=%g < thres[0]=%g !",thres[1],thres[0]);
                    // closing oligo 5' end
                    thres[2] = thres[1];
                    thres[2] += (oli_open == 0 || oli_open <= fork || oli_attach == 0) ? 0 : rco;
                    if (thres[2] < thres[1])
                        win_printf("thres[2]=%g < thres[1]=%g !",thres[2],thres[1]);
                    // openning oligo 5' end
                    tmp = (end_oligo == 4 && bp_remain < 3) ? roo_e[oli_open] : roo[oli_open];
                    tmp = (tmp < 0 || oli_attach == 0) ? 0 : tmp;
                    thres[3] = thres[2] + tmp;
                    if (thres[3] < thres[2])
                        win_printf("thres[3]=%g < thres[2]=%g !",thres[3],thres[2]);

                    // closing oligo 5' end antisense
                    tmp = (olia2_open >= len_1 || a3prime == 0 || olia_attach == 0) ? 0 : rco;
                    thres[4] = thres[3] + tmp;
                    // openning oligo 5' end antisense
                    tmp = (end_oligo == 4 && bpa_remain < 3) ? roo_e[olia2_open-1] : roo[olia2_open - 1];
                    tmp = (olia2_open == olia_open || a3prime == 0 || olia_attach == 0) ? 0 : tmp;
                    tmp = (tmp < 0) ? 0 : tmp;
                    thres[5] = thres[4] + tmp;
                    if (thres[5] < thres[4])
                        win_printf("thres[5]=%g < thres[4]=%g !",thres[5],thres[4]);
                    // closing oligo 3' end antisense
                    thres[6] = thres[5];
                    thres[6] += (olia_open == 0 || olia_open <= fork || olia_attach == 0) ? 0 : rco;
                    if (thres[6] < thres[5])
                        win_printf("thres[6]=%g < thres[5]=%g !",thres[6],thres[5]);
                    // openning oligo 3' end antisense
                    tmp = (end_oligo == 4 && bpa_remain < 3) ? roo_e[olia_open] : roo[olia_open];
                    tmp = (tmp < 0 || olia_attach == 0) ? 0 : tmp;
                    thres[7] = thres[6] + tmp;
                    if (thres[7] < thres[6])
                        win_printf("thres[7]=%g < thres[6]=%g !",thres[7],thres[6]);

                    // openning fork only if not fully open
                    thres[8] = thres[7];
                    thres[8] += (fork == 0 || afork == 0) ? 0 : roh[fork-1];
                    if (thres[8] < thres[7])
                        win_printf("thres[8]=%g < thres[7]=%g !",thres[8],thres[7]);
                    // closing fork only if not blocked by oligo
                    thres[9] = thres[8];
                    tmp = ((oli_attach > 0) && (oli_open <= fork)) ? 0 : rch;
                    tmp = ((olia_attach > 0) && (olia_open <= fork)) ? 0 : tmp;
                    tmp = (afork == 0) ? 0 : tmp;
                    thres[9] += tmp;
                    if (thres[9] < thres[8])
                        win_printf("thres[9]=%g < thres[8]=%g !",thres[9],thres[8]);
                    den = thres[9];
                    tmp = ran1(&idum);
                    dt = -log(tmp)/den;
                    t += dt;
                    ttot += dt;
                    tmp = den*ran1(&idum);
                    if (disp != WIN_CANCEL)
                        {
                            disp = win_printf("Fork %d, Oli sense 5' %d, 3' %d Oli antisense 3' %d, 5' %d\n"
                                              "tmp = %g Sense oligo attached->%c, Antisense oligo attached->%c\n"
                                              "Oligo sense 3'end %c ++ if in [0,%g]\n"
                                              "Oligo sense 3'end %c -- if in [%g,%g]\n"
                                              "Oligo sense 5'end %c -- if in [%g,%g]\n"
                                              "Oligo sense 5'end %c ++ if in [%g,%g]\n"
                                              "Oligo antisense 5'end %c ++ if in [%g,%g]\n"
                                              "Oligo antisense 5'end %c -- if in [%g,%g]\n"
                                              "Oligo antisense 3'end %c -- if in [%g,%g]\n"
                                              "Oligo antisense 3'end %c ++ if in [%g,%g]\n"
                                              "    Fork    %c -- if in [%g,%g]\n"
                                              "    Fork    %c ++ if in [%g,%g]\n"
                                              ,fork,oli_open,oli2_open,olia_open,olia2_open,tmp
                                              ,(oli_attach)? 'y':'n',(olia_attach)? 'y':'n'
                                              ,(tmp < thres[0])? 'y':'n',thres[0]
                                              ,(tmp >= thres[0] && tmp < thres[1])? 'y':'n',thres[0],thres[1]
                                              ,(tmp >= thres[1] && tmp < thres[2])? 'y':'n',thres[1],thres[2]
                                              ,(tmp >= thres[2] && tmp < thres[3])? 'y':'n',thres[2],thres[3]
                                              ,(tmp >= thres[3] && tmp < thres[4])? 'y':'n',thres[3],thres[4]
                                              ,(tmp >= thres[4] && tmp < thres[5])? 'y':'n',thres[4],thres[5]
                                              ,(tmp >= thres[5] && tmp < thres[6])? 'y':'n',thres[5],thres[6]
                                              ,(tmp >= thres[6] && tmp < thres[7])? 'y':'n',thres[6],thres[7]
                                              ,(tmp >= thres[7] && tmp < thres[8])? 'y':'n',thres[7],thres[8]
                                              ,(tmp >= thres[8] && tmp < thres[9])? 'y':'n',thres[8],thres[9]);
                        }
                    if (tmp < thres[0]) oli2_open++;
                    else if (tmp >= thres[0] && tmp < thres[1]) oli2_open--;
                    else if (tmp >= thres[1] && tmp < thres[2]) oli_open--;
                    else if (tmp >= thres[2] && tmp < thres[3]) oli_open++;
                    else if (tmp >= thres[3] && tmp < thres[4]) olia2_open++;
                    else if (tmp >= thres[4] && tmp < thres[5]) olia2_open--;
                    else if (tmp >= thres[5] && tmp < thres[6]) olia_open--;
                    else if (tmp >= thres[6] && tmp < thres[7]) olia_open++;
                    else if (tmp >= thres[7] && tmp < thres[8]) fork--;
                    else if (tmp >= thres[8] && tmp < thres[9]) fork++;

                    for(k = 0; dsop != NULL && k < oli_open_1 && k < len_1; k++)
                        {
                            if (k >= 0 && k < dsop->nx)       dsop->xd[k] += dt;
                            else win_printf("K out of range %d",k);
                        }
                    for(k = oli2_open_1; dsop != NULL && k < len_1; k++)
                        {
                            if (k >= 0 && k < dsop->nx)       dsop->xd[k] += dt;
                            else win_printf("K out of range %d",k);
                        }


                    if (dur == 0)
                        {
                            add_new_point_to_ds(dso, t_1, oli_open_1);
                            add_new_point_to_ds(dso, t, oli_open_1);
                            add_new_point_to_ds(dso2, t_1, oli2_open_1);
                            add_new_point_to_ds(dso2, t, oli2_open_1);
                            add_new_point_to_ds(dsoa, t_1, olia_open_1);
                            add_new_point_to_ds(dsoa, t, olia_open_1);
                            add_new_point_to_ds(dsoa2, t_1, olia2_open_1);
                            add_new_point_to_ds(dsoa2, t, olia2_open_1);
                            add_new_point_to_ds(dsf, t_1, fork_1);
                            add_new_point_to_ds(dsf, t, fork_1);
                            oli_open_1 = oli_open;
                            oli2_open_1 = oli2_open;
                            olia_open_1 = olia_open;
                            olia2_open_1 = olia2_open;
                            fork_1 = fork;
                            t_1 = t;
                            add_new_point_to_ds(dso, t, oli_open);
                            add_new_point_to_ds(dso2, t, oli2_open);
                            add_new_point_to_ds(dsf, t, fork);
                            add_new_point_to_ds(dsoa, t, olia_open);
                            add_new_point_to_ds(dsoa2, t, olia2_open);
                            i++;
                        }
                    else
                        {
                            oli_open_1 = oli_open;
                            oli2_open_1 = oli2_open;
                            olia_open_1 = olia_open;
                            olia2_open_1 = olia2_open;
                            fork_1 = fork;
                            t_1 = t;
                        }
                    if (j%65536 == 0)
                        my_set_window_title("j %d pos %d %d %d", j,fork,oli_open,oli2_open);
                }
            else
                {
                    if (dslb != NULL)
                        {
                            dslb->xd[oli_open] += 1;
                        }
                    if (dur == 0)
                        {
                            add_new_point_to_ds(dso, t, oli_open);
                            add_new_point_to_ds(dso2, t, oli2_open);
                            add_new_point_to_ds(dsoa, t, olia_open);
                            add_new_point_to_ds(dsoa2, t, olia2_open);
                            add_new_point_to_ds(dsf, t, fork);
                            i++;
                            oli_attach = oli_attach_0;
                            olia_attach = olia_attach_0;
                            oli_open_1 = oli_open = 0;
                            oli2_open_1 = oli2_open = len_1;
                            olia_open_1 = olia_open = 0;
                            olia2_open_1 = olia2_open = len_1;
                            fork_1 = fork = 0;
                            add_new_point_to_ds(dso, t, oli_open);
                            add_new_point_to_ds(dso2, t, oli2_open);
                            add_new_point_to_ds(dsoa, t, olia_open);
                            add_new_point_to_ds(dsoa2, t, olia2_open);
                            add_new_point_to_ds(dsf, t, fork);
                        }
                    if (dur == 1)
                        {
                            oli_attach = oli_attach_0;
                            olia_attach = olia_attach_0;
                            oli_open = 0;
                            olia_open = 0;
                            fork = 0;
                            oli2_open = len_1;
                            olia2_open = len_1;
                            my_set_window_title("Event %d duration %f %3.1f%%", i,t,(float)(100*i)/nx);
                            add_new_point_to_ds(dsf, i++, t);
                            for (k = 0, tmp = 0; k < dsf->nx; k++) tmp += dsf->yd[k];
                            if (dsf->nx > 0) tmp/=dsf->nx;
                            tmp *= op->dy;
                            set_plot_title(op, "\\stack{{%s%s%s}{blocking %s}"
                                           "{seq:%s}"
                                           "{\\pt8 [Na+] = %g mM, [mg2+] = %g mM, T=%g^\\oc C; F = %g pN}{Mean duration %g}}"
                                           ,((afork)?"Fork/":""),(oli_attach_0)?" Sense oligo":""
                                           ,(olia_attach_0)?" Antisense oligo":""
                                           ,((a3prime)?"5' and 3' end active":"5' end onlt")
                                           ,oligo,Na,Mg,Ti,F,tmp);
                            op->need_to_refresh = 1;
                            refresh_plot(pr,dur_op);
                            t = 0;
                        }
                }
        }
    for (k = 0; dsop != NULL && ttot > 0 && k < dsop->nx; k++) dsop->xd[k] /= ttot;
    return refresh_plot(pr,pr->n_op-1);
}



int 	do_unzip_signal_SC_2sides_no(void)
{
        int i, j, k, dur_op = 0; // , i_1, io_1, io
  static int nx = 16384, new_plot = 1;//, timin = 0;
  pltreg *pr;
  O_p *op = NULL, *opo = NULL, *opl = NULL;
  d_s *dso = NULL, *dso2 = NULL, *dsf = NULL, *dsop = NULL, *dslb = NULL;
  static int idum = 4587, end_oligo = 2, dur = 0, afork = 1, a3prime = 1, gplo = 0, gpll = 0, debug = 0, verbose = 0, no_salt_correction = 1, at_penalty = 3;
  static char oligo[128] = {0};
  static float Na = 150, Mg = 10, Ti = 25, F = 8.6, T0 = 273.15, mdt = 1.08;
  float dg[128] = {0};
  float roh[128] = {0};
  float roo[128] = {0};
  float thres[10] = {0};
  float R = 1.9872;
  float cor = 1000.0/(R*(T0+Ti)); // cal/K-mol
  float tmp;
  int len, index; //, hp, ol;
  float d, bp, ft, u, gss, gds, rco, rch, dgt;
  char stuff[2048] = {0};
  int disp = WIN_OK;


  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return 1;
  if (strlen(oligo) == 0) sprintf(oligo,"ACAGCCAGCCGA");
				  //"TTTGGCTAAATATCCTAATGTTAAAGTGTATGATAAGCCTACTACAGTAGATTTTGACGGGTGTTTGATTGATTTGATTCCTTGGATGTGCGAAG"); //TTTGGCTAAATATCCTAATGTTAAAGTGTATGATAAGCCTACTACAGTAGATTTTGACGGGTGTTTGATTGATTTGATTCCTTGGATGTGCGAAG"); // ACAGCCAGCCGA "GGGTGTTTGATTGATTTGATTCCTTGGATGTGCGAAG"

  i = win_scanf("Generate an unzipping signal of a fork with a blocking oligo "
		"Following Simona Cocco model\nwhere fork and oligo openning "
		"are not bound but where the fork is behind the oligo opening\n"
                "Allows the second end of the oligo to open\n"
		"Specify the oligo sequence %s\n"
		"Oligo initiation and AT/GC  correction at the ends:\n"
		"%R->no %r->on 5' %r->on 3'only %r->on both 5' and 3'\n"
		"else Oligo AT/GC penalty at the ends:\n"
		"%R->no %r->on 5' %r->on 3'only %r->on both 5' and 3'\n"
		"%b->No salt entropy correction \n"
		"[Na+] = %4f mM; [Mg2+] = %4f mM; T = %4f \\oc C\n"
		"Force = %4f pN\n"
		"%R in this plot %r in a new plot\n"
		"Total number of steps %8d\n"
		"Random seed %8d\n"
		"%R real time or %r duration \n"
                "%bFork active %b3-end of oligo active\n"
                "%b Generate plot with average openning probability\n"
                "%b Genertae a plot with statitics of last base attached\n"
                "%b debugging\n"
                "Time conversion in \\mu s %4f\n"
                "%b Display energies\n"
		,oligo,&end_oligo,&at_penalty,&no_salt_correction,&Na,&Mg,&Ti,&F
		,&new_plot,&nx,&idum,&dur,&afork,&a3prime,&gplo,&gpll,&debug,&mdt,&verbose);
  if (i == WIN_CANCEL)	return D_O_K;

  cor = 1000.0/(R*(T0+Ti)); // convert cal/K-mol in kBT
  init_Mfold31_array(Na, Mg, Ti, verbose, no_salt_correction, dsCharge);
  if (debug == 0)  disp = WIN_CANCEL;
  len = strlen(oligo);

  for (i = 1; i < len; i++)
    {
      index = 0;
      if ((oligo[i-1]&0x5F) == 'A')  index = 0;
      if ((oligo[i-1]&0x5F) == 'C')  index = 4;
      if ((oligo[i-1]&0x5F) == 'G')  index = 8;
      if ((oligo[i-1]&0x5F) == 'T')  index = 12;
      if ((oligo[i]&0x5F) == 'A')  index += 0;
      if ((oligo[i]&0x5F) == 'C')  index += 1;
      if ((oligo[i]&0x5F) == 'G')  index += 2;
      if ((oligo[i]&0x5F) == 'T')  index += 3;
      dg[i-1] = -cor*DGMF[index];
      if (i < 12)
              sprintf(stuff+strlen(stuff),"DeltaG %c%c %g Kcal/mol %g kBT\n",oligo[i-1],oligo[i],-DGMF[index],dg[i-1]);
    }

  for(i = 0; i < len-1; i++)
    {
      roo[i] = exp(-dg[i]);
      roh[i] = exp(-dg[i]);
    }
  if (end_oligo & 0x1)
    {
      dgt =(((oligo[0]&0x5F) == 'A') || ((oligo[0]&0x5F) == 'T')) ? -cor*DGMF[INAT] : -cor*DGMF[INGC];
    }
  else if (at_penalty & 0x1)
    {
      dgt =(((oligo[0]&0x5F) == 'A') || ((oligo[0]&0x5F) == 'T')) ? -cor*DGMF[TP] : 0;
    }
  else dgt = 0;
  roo[0]= exp(-dg[0]-dgt);
  // no correction on hp

  if (end_oligo & 0x2)
    {
      dgt =(((oligo[len-1]&0x5F) == 'A') || ((oligo[len-1]&0x5F) == 'T')) ? -cor*DGMF[INAT] : -cor*DGMF[INGC];
    }
  else if (at_penalty & 0x2)
    {
      dgt =(((oligo[len-1]&0x5F) == 'A') || ((oligo[len-1]&0x5F) == 'T')) ? -cor*DGMF[TP] : 0;
    }
  else dgt = 0;
  roo[len-2]= exp(-dg[len-2]-dgt);
  // no correction on hp

  //elasticity models for the closing rates for the ds and ss DNA
  d = 0.56*1.12;
  bp = 1.5*1.125;
  d = 0.605;// # 0.567 #0.56*1.12 new 0.605
  bp = 1.828; // # 1.78 # 1.5*1.125 new 1.828

  ft = 4.1*(T0+Ti)/(T0+25);
  u = bp*F/ft;
  gss = d/bp*log(sinh(u)/u) ;
  gds = (F/ft - sqrt(F/ft/50) + 0.5*F*F/ft/1230)*0.34;

  //closing rates:only depend on the force
  rch = exp(-2*gss);
  rco = exp(-gss+gds);

  for (i = 0; i < len-1 && i < 12; i++)
    sprintf(stuff+strlen(stuff),"roo[%d] = %g; roh[%d] = %g;\n",i,roo[i],i, roh[i]);
  if (i < len ) sprintf(stuff+strlen(stuff),"...\nrco = %g; rch = %g;\n",rco, rch);
  else sprintf(stuff+strlen(stuff),"rco = %g; rch = %g;\n",rco, rch);

  win_printf("%s",stuff);


  if (new_plot)
    {
      if ((op = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create plot");
      dsf = op->dat[0];
      dsf->nx = dsf->ny = 0;
      dur_op = pr->n_op-1;
      set_plot_title(op, "\\stack{{%soligo blocking %s}"
		     "{seq:%s}"
		     "{\\pt8 [Na+] = %g mM, [mg2+] = %g mM, T=%g^\\oc C; F = %g pN}}"
		     ,((afork)?"Fork/":""),((a3prime)?"5' and 3' end active":"5' end onlt"),oligo,Na,Mg,Ti,F);

      create_attach_select_x_un_to_op(op, IS_SECOND, 0, mdt, -6, 0, "\\mu s");
      if (dur)       create_attach_select_y_un_to_op(op, IS_SECOND, 0, mdt, -6, 0, "\\mu s");

    }
  else
    {
      if ((dsf = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      dsf->nx = dsf->ny = 0;
      dur_op = pr->cur_op;
    }

  if (dur == 0)
    {
      if ((dso = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      dso->nx = dso->ny = 0;
      set_ds_source(dso, "Fork/->(oligo) blocking;  seq:%s; [Na+] = %g mM, [mg2+] = %g mM, T=%g C; F = %g pN"
                    ,oligo,Na,Mg,Ti,F);
      if ((dso2 = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      dso2->nx = dso2->ny = 0;
      set_ds_source(dso2, "Fork/->(oligo) blocking;  seq:%s; [Na+] = %g mM, [mg2+] = %g mM, T=%g C; F = %g pN"
                    ,oligo,Na,Mg,Ti,F);
    }


  //set_dot_line(ds);
  set_ds_source(dsf, "->(Fork)/oligo blocking;  seq:%s; [Na+] = %g mM, [mg2+] = %g mM, T=%g C; F = %g pN"
		,oligo,Na,Mg,Ti,F);

  if (gplo)
     {
      if ((opo = create_and_attach_one_plot(pr, len-1, len-1, 0)) == NULL)
	return win_printf_OK("cannot create plot");
      dsop = opo->dat[0];
       for (k = 0; k < dsop->nx; k++) dsop->yd[k] = 0.5+k;
      set_plot_title(opo, "\\stack{{%soligo blocking %s}"
		     "{seq:%s}"
		     "{\\pt8 [Na+] = %g mM, [mg2+] = %g mM, T=%g^\\oc C; F = %g pN}}"
		     ,((afork)?"Fork/":""),((a3prime)?"5' and 3' end active":"5' end onlt"),oligo,Na,Mg,Ti,F);
                                                                                                                         set_plot_x_title(opo,"Probability of openning");
      set_plot_y_title(opo,"Position along oligo");
     }
  if (gpll)
     {
      if ((opl = create_and_attach_one_plot(pr, len, len, 0)) == NULL)
	return win_printf_OK("cannot create plot");
      dslb = opl->dat[0];
       for (k = 0; k < dslb->nx; k++) dslb->yd[k] = k;
      set_plot_title(opl, "\\stack{{%soligo blocking %s}"
		     "{seq:%s}"
		     "{\\pt8 [Na+] = %g mM, [mg2+] = %g mM, T=%g^\\oc C; F = %g pN}}"
		     ,((afork)?"Fork/":""),((a3prime)?"5' and 3' end active":"5' end onlt"),oligo,Na,Mg,Ti,F);
                                                                                                                         set_plot_x_title(opl,"Nunber of exit events");
      set_plot_y_title(opl,"Base Nb where oligo detached");
     }

  float t = 0, t_1 = 0, dt = 0, den = 0, ttot = 0;
  int fork, fork_1 = 0, oli_open, oli_open_1 = 0, len_1 = len - 1;
  int oli2_open = len_1, oli2_open_1 = len_1;

  for (i = j = 0, fork = 0, oli_open = 0, oli2_open = len_1; i < nx; j++)
    {
      //fork = len - hp; // measure the nb of base closed in the hairpin
      //fork starts at 0 and increases it is always <= oli_open
      // oli2_open = len - oli2 ; oli2_open = len no breathing
      // 0 <= fork <= oli_open < oli2_open <= len
      // detaching when oli_open == oli2_open


      if (oli_open < oli2_open)  // we are still attached
	{      // we try to close oligo
          if (oli_open < 0 || oli_open >= len_1)
                  win_printf("oli_open out %d",oli_open);
          if (oli2_open < 1 || oli2_open > len_1)
                  win_printf("oli2_open out %d",oli2_open);
          if (oli_open < fork)
                  win_printf("oli_open %d < fork %d",oli_open,fork);
          if (fork < 0 || fork > len_1)
                  win_printf("fork %d out of range [0,%d]",fork,len_1);

          // closing oligo 3' end
	  thres[0] = (oli2_open >= len_1 || a3prime == 0) ? 0 : rco;
          // openning oligo 3' end
          thres[1] = thres[0] + ((oli2_open == oli_open || a3prime == 0) ? 0 : roo[oli2_open - 1]);
          // closing oligo 5' end
          thres[2] = thres[1] + ((oli_open == 0 || oli_open <= fork) ? 0 : rco);
          // openning oligo 5' end
          thres[3] = thres[2] + roo[oli_open];
          // openning fork only if not fully open
          thres[4] = thres[3] + ((fork == 0 || afork == 0) ? 0 : roh[fork-1]);
          // closing fork only if not blocked by oligo
          thres[5] = thres[4] + ((oli_open <= fork || afork == 0) ? 0 : rch);
          den = thres[5];


          tmp = ran1(&idum);
          dt = -log(tmp)/den;
          t += dt;
          ttot += dt;
          tmp = den*ran1(&idum);
          if (disp != WIN_CANCEL)
            {
              disp = win_printf("Fork %d, Oli 5' %d, oli3' %d\n"
                                "tmp = %g\n"
                                "Oligo 3'end %c ++ if in [0,%g]\n"
                                "Oligo 3'end %c -- if in [%g,%g]\n"
                                "Oligo 5'end %c -- if in [%g,%g]\n"
                                "Oligo 5'end %c ++ if in [%g,%g]\n"
                                "    Fork    %c -- if in [%g,%g]\n"
                                "    Fork    %c ++ if in [%g,%g]\n"
                                ,fork,oli_open,oli2_open,tmp
                                ,(tmp < thres[0])? 'y':'n',thres[0]
                                ,(tmp >= thres[0] && tmp < thres[1])? 'y':'n',thres[0],thres[1]
                                ,(tmp >= thres[1] && tmp < thres[2])? 'y':'n',thres[1],thres[2]
                                ,(tmp >= thres[2] && tmp < thres[3])? 'y':'n',thres[2],thres[3]
                                ,(tmp >= thres[3] && tmp < thres[4])? 'y':'n',thres[3],thres[4]
                                ,(tmp >= thres[4] && tmp < thres[5])? 'y':'n',thres[4],thres[5]);
            }
          if (tmp < thres[0]) oli2_open++;
          if (tmp >= thres[0] && tmp < thres[1]) oli2_open--;
          if (tmp >= thres[1] && tmp < thres[2]) oli_open--;
          if (tmp >= thres[2] && tmp < thres[3]) oli_open++;
          if (tmp >= thres[3] && tmp < thres[4]) fork--;
          if (tmp >= thres[4] && tmp < thres[5]) fork++;

	  for(k = 0; dsop != NULL && k < oli_open_1 && k < len_1; k++)
            {
                if (k >= 0 && k < dsop->nx)       dsop->xd[k] += dt;
                else win_printf("K out of range %d",k);
            }
	  for(k = oli2_open_1; dsop != NULL && k < len_1; k++)
            {
                if (k >= 0 && k < dsop->nx)       dsop->xd[k] += dt;
                else win_printf("K out of range %d",k);
            }


	  if (dur == 0)
	    {
	      add_new_point_to_ds(dso, t_1, oli_open_1);
	      add_new_point_to_ds(dso2, t_1, oli2_open_1);
	      add_new_point_to_ds(dsf, t_1, fork_1);
	      add_new_point_to_ds(dso, t, oli_open_1);
	      add_new_point_to_ds(dso2, t, oli2_open_1);
	      add_new_point_to_ds(dsf, t, fork_1);
	      oli_open_1 = oli_open;
	      oli2_open_1 = oli2_open;
	      fork_1 = fork;
              t_1 = t;
	      add_new_point_to_ds(dso, t, oli_open);
	      add_new_point_to_ds(dso2, t, oli2_open);
	      add_new_point_to_ds(dsf, t, fork);
	      i++;
	    }
          else
            {
	      oli_open_1 = oli_open;
	      oli2_open_1 = oli2_open;
	      fork_1 = fork;
              t_1 = t;
            }
	  if (j%65536 == 0)
                my_set_window_title("j %d pos %d %d %d", j,fork,oli_open,oli2_open);
	}
      else
	{
          if (dslb != NULL)
            {
               dslb->xd[oli_open] += 1;
            }
	  if (dur == 0)
	    {
	      add_new_point_to_ds(dso, t, oli_open);
	      add_new_point_to_ds(dso2, t, oli2_open);
	      add_new_point_to_ds(dsf, t, fork);
	      i++;
	      oli_open_1 = oli_open = 0;
	      oli2_open_1 = oli2_open = len_1;
	      fork_1 = fork = 0;
	      add_new_point_to_ds(dso, t, oli_open);
	      add_new_point_to_ds(dsf, t, fork);
	    }
	  if (dur == 1)
	    {
	      oli_open = 0;
	      fork = 0;
              oli2_open = len_1;
	      my_set_window_title("Event %d duration %f %3.1f%%", i,t,(float)(100*i)/nx);
	      add_new_point_to_ds(dsf, i++, t);
              for (k = 0, tmp = 0; k < dsf->nx; k++) tmp += dsf->yd[k];
              if (dsf->nx > 0) tmp/=dsf->nx;
              set_plot_title(op, "\\stack{{%soligo blocking %s}"
                             "{seq:%s}"
                             "{\\pt8 [Na+] = %g mM, [mg2+] = %g mM, T=%g^\\oc C; F = %g pN}{Mean duration %g}}"
                             ,((afork)?"Fork/":"")
                             ,((a3prime)?"5' and 3' end active":"5' end onlt")
                             ,oligo,Na,Mg,Ti,F,tmp);

              op->need_to_refresh = 1;
              refresh_plot(pr,dur_op);
	      t = 0;
	    }
	}
    }
  for (k = 0; dsop != NULL && ttot > 0 && k < dsop->nx; k++) dsop->xd[k] /= ttot;
  return refresh_plot(pr,pr->n_op-1);
}



int 	do_unzip_signal_SC_2sides_n_hp_extended(void)
{
  int i, j, k, dur_op = 0; // , i_1, io_1, io
  static int nx = 16384, new_plot = 1;//, timin = 0;
  pltreg *pr;
  O_p *op = NULL, *opo = NULL, *opl = NULL;
  d_s *dso = NULL, *dso2 = NULL, *dsf = NULL, *dsop = NULL, *dslb = NULL;
  static int idum = 4587, end_oligo = 2, dur = 0, afork = 1, a3prime = 1, gplo = 0, gpll = 0, debug = 0, verbose = 0, no_salt_correction = 1, at_penalty = 3;
  static char oligo[128] = {0};
  static float Na = 150, Mg = 10, Ti = 25, F = 8.6, T0 = 273.15, mdt = 1.08;
  float dg[128] = {0};
  float roh[128] = {0};
  float roo[128] = {0};
  float thres[10] = {0};
  float R = 1.9872;
  float cor = 1000.0/(R*(T0+Ti)); // cal/K-mol
  float tmp;
  int len, index; //, hp, ol;
  float d, bp, ft, u, gss, gds, rco, rch, dgt;
  char stuff[2048] = {0};
  int disp = WIN_OK;


  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return 1;
  if (strlen(oligo) == 0) sprintf(oligo,"ACAGCCAGCCGA");
				  //"TTTGGCTAAATATCCTAATGTTAAAGTGTATGATAAGCCTACTACAGTAGATTTTGACGGGTGTTTGATTGATTTGATTCCTTGGATGTGCGAAG"); //TTTGGCTAAATATCCTAATGTTAAAGTGTATGATAAGCCTACTACAGTAGATTTTGACGGGTGTTTGATTGATTTGATTCCTTGGATGTGCGAAG"); // ACAGCCAGCCGA "GGGTGTTTGATTGATTTGATTCCTTGGATGTGCGAAG"

  i = win_scanf("Generate an unzipping signal of a fork with a blocking oligo "
		"Following Simona Cocco model\nwhere fork and oligo openning "
		"are not bound but where the fork is behind the oligo opening\n"
                "Allows the second end of the oligo to open\n"
		"Specify the oligo sequence %s\n"
		"Oligo initiation and AT/GC  correction at the ends:\n"
		"%R->no %r->on 5' %r->on 3'only %r->on both 5' and 3'\n"
		"else Oligo AT/GC penalty at the ends:\n"
		"%R->no %r->on 5' %r->on 3'only %r->on both 5' and 3'\n"
		"%b->No salt entropy correction \n"
		"[Na+] = %4f mM; [Mg2+] = %4f mM; T = %4f \\oc C\n"
		"Force = %4f pN\n"
		"%R in this plot %r in a new plot\n"
		"Total number of steps %8d\n"
		"Random seed %8d\n"
		"%R real time or %r duration \n"
                "%bFork active %b3-end of oligo active\n"
                "%b Generate plot with average openning probability\n"
                "%b Genertae a plot with statitics of last base attached\n"
                "%b debugging\n"
                "Time conversion in \\mu s %4f\n"
                "%b Display energies\n"
		,oligo,&end_oligo,&at_penalty,&no_salt_correction,&Na,&Mg,&Ti,&F
		,&new_plot,&nx,&idum,&dur,&afork,&a3prime,&gplo,&gpll,&debug,&mdt,&verbose);
  if (i == WIN_CANCEL)	return D_O_K;

  cor = 1000.0/(R*(T0+Ti)); // convert cal/K-mol in kBT
  init_Mfold31_array(Na, Mg, Ti, verbose, no_salt_correction, dsCharge);
  if (debug == 0)  disp = WIN_CANCEL;
  len = strlen(oligo);

  for (i = 1; i < len; i++)
    {
      index = 0;
      if ((oligo[i-1]&0x5F) == 'A')  index = 0;
      if ((oligo[i-1]&0x5F) == 'C')  index = 4;
      if ((oligo[i-1]&0x5F) == 'G')  index = 8;
      if ((oligo[i-1]&0x5F) == 'T')  index = 12;
      if ((oligo[i]&0x5F) == 'A')  index += 0;
      if ((oligo[i]&0x5F) == 'C')  index += 1;
      if ((oligo[i]&0x5F) == 'G')  index += 2;
      if ((oligo[i]&0x5F) == 'T')  index += 3;
      dg[i-1] = -cor*DGMF[index];
      if (i < 12)
              sprintf(stuff+strlen(stuff),"DeltaG %c%c %g Kcal/mol %g kBT\n",oligo[i-1],oligo[i],-DGMF[index],dg[i-1]);
    }

  for(i = 0; i < len-1; i++)
    {
      roo[i] = exp(-dg[i]);
      roh[i] = exp(-dg[i]);
      roh[i+len-1] = exp(-dg[i]);
    }
  if (end_oligo & 0x1)
    {
      dgt =(((oligo[0]&0x5F) == 'A') || ((oligo[0]&0x5F) == 'T')) ? -cor*DGMF[INAT] : -cor*DGMF[INGC];
    }
  else if (at_penalty & 0x1)
    {
      dgt =(((oligo[0]&0x5F) == 'A') || ((oligo[0]&0x5F) == 'T')) ? -cor*DGMF[TP] : 0;
    }
  else dgt = 0;
  roo[0]= exp(-dg[0]-dgt);
  // no correction on hp

  if (end_oligo & 0x2)
    {
      dgt =(((oligo[len-1]&0x5F) == 'A') || ((oligo[len-1]&0x5F) == 'T')) ? -cor*DGMF[INAT] : -cor*DGMF[INGC];
    }
  else if (at_penalty & 0x2)
    {
      dgt =(((oligo[len-1]&0x5F) == 'A') || ((oligo[len-1]&0x5F) == 'T')) ? -cor*DGMF[TP] : 0;
    }
  else dgt = 0;
  roo[len-2]= exp(-dg[len-2]-dgt);
  // no correction on hp

  //elasticity models for the closing rates for the ds and ss DNA
  d = 0.56*1.12;
  bp = 1.5*1.125;
  d = 0.605;// # 0.567 #0.56*1.12 new 0.605
  bp = 1.828; // # 1.78 # 1.5*1.125 new 1.828

  ft = 4.1*(T0+Ti)/(T0+25);
  u = bp*F/ft;
  gss = d/bp*log(sinh(u)/u) ;
  gds = (F/ft - sqrt(F/ft/50) + 0.5*F*F/ft/1230)*0.34;

  //closing rates:only depend on the force
  rch = exp(-2*gss);
  rco = exp(-gss+gds);

  for (i = 0; i < len-1 && i < 12; i++)
    sprintf(stuff+strlen(stuff),"roo[%d] = %g; roh[%d] = %g;\n",i,roo[i],i, roh[i]);
  if (i < len ) sprintf(stuff+strlen(stuff),"...\nrco = %g; rch = %g;\n",rco, rch);
  else sprintf(stuff+strlen(stuff),"rco = %g; rch = %g;\n",rco, rch);

  win_printf("%s",stuff);


  if (new_plot)
    {
      if ((op = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create plot");
      dsf = op->dat[0];
      dsf->nx = dsf->ny = 0;
      dur_op = pr->n_op-1;
      set_plot_title(op, "\\stack{{%soligo blocking %s}"
		     "{seq:%s}"
		     "{\\pt8 [Na+] = %g mM, [mg2+] = %g mM, T=%g^\\oc C; F = %g pN}}"
		     ,((afork)?"Fork/":""),((a3prime)?"5' and 3' end active":"5' end onlt"),oligo,Na,Mg,Ti,F);

      create_attach_select_x_un_to_op(op, IS_SECOND, 0, mdt, -6, 0, "\\mu s");
      if (dur)       create_attach_select_y_un_to_op(op, IS_SECOND, 0, mdt, -6, 0, "\\mu s");

    }
  else
    {
      if ((dsf = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      dsf->nx = dsf->ny = 0;
      dur_op = pr->cur_op;
    }

  if (dur == 0)
    {
      if ((dso = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      dso->nx = dso->ny = 0;
      set_ds_source(dso, "Fork/->(oligo) blocking;  seq:%s; [Na+] = %g mM, [mg2+] = %g mM, T=%g C; F = %g pN"
                    ,oligo,Na,Mg,Ti,F);
      if ((dso2 = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      dso2->nx = dso2->ny = 0;
      set_ds_source(dso2, "Fork/->(oligo) blocking;  seq:%s; [Na+] = %g mM, [mg2+] = %g mM, T=%g C; F = %g pN"
                    ,oligo,Na,Mg,Ti,F);
    }


  //set_dot_line(ds);
  set_ds_source(dsf, "->(Fork)/oligo blocking;  seq:%s; [Na+] = %g mM, [mg2+] = %g mM, T=%g C; F = %g pN"
		,oligo,Na,Mg,Ti,F);

  if (gplo)
     {
      if ((opo = create_and_attach_one_plot(pr, len-1, len-1, 0)) == NULL)
	return win_printf_OK("cannot create plot");
      dsop = opo->dat[0];
       for (k = 0; k < dsop->nx; k++) dsop->yd[k] = 0.5+k;
      set_plot_title(opo, "\\stack{{%soligo blocking %s}"
		     "{seq:%s}"
		     "{\\pt8 [Na+] = %g mM, [mg2+] = %g mM, T=%g^\\oc C; F = %g pN}}"
		     ,((afork)?"Fork/":""),((a3prime)?"5' and 3' end active":"5' end onlt"),oligo,Na,Mg,Ti,F);
                                                                                                                         set_plot_x_title(opo,"Probability of openning");
      set_plot_y_title(opo,"Position along oligo");
     }
  if (gpll)
     {
      if ((opl = create_and_attach_one_plot(pr, len, len, 0)) == NULL)
	return win_printf_OK("cannot create plot");
      dslb = opl->dat[0];
       for (k = 0; k < dslb->nx; k++) dslb->yd[k] = k;
      set_plot_title(opl, "\\stack{{%soligo blocking %s}"
		     "{seq:%s}"
		     "{\\pt8 [Na+] = %g mM, [mg2+] = %g mM, T=%g^\\oc C; F = %g pN}}"
		     ,((afork)?"Fork/":""),((a3prime)?"5' and 3' end active":"5' end onlt"),oligo,Na,Mg,Ti,F);
                                                                                                                         set_plot_x_title(opl,"Nunber of exit events");
      set_plot_y_title(opl,"Base Nb where oligo detached");
     }

  float t = 0, t_1 = 0, dt = 0, den = 0, ttot = 0;
  int fork, len_1 = len - 1, fork_1 = len_1, oli_open, oli_open_1 = 0;
  int oli2_open = len_1, oli2_open_1 = len_1;

  for (i = j = 0, fork = len_1, oli_open = 0, oli2_open = len_1; i < nx; j++)
    {
      //fork = len - hp; // measure the nb of base closed in the hairpin
      //fork starts at 0 and increases it is always <= oli_open
      // oli2_open = len - oli2 ; oli2_open = len no breathing
      // 0 <= fork <= oli_open < oli2_open <= len
      // detaching when oli_open == oli2_open


      if (oli_open < oli2_open)  // we are still attached
	{      // we try to close oligo
          if (oli_open < 0 || oli_open >= len_1)
                  win_printf("oli_open out %d",oli_open);
          if (oli2_open < 1 || oli2_open > len_1)
                  win_printf("oli2_open out %d",oli2_open);
          if (oli_open < (fork-len_1))
                  win_printf("oli_open %d < fork %d",oli_open,fork-len_1);
          if (fork < 0 || fork > 2*len_1)
                  win_printf("fork %d out of range [0,%d]",fork,len_1);

          // closing oligo 3' end
	  thres[0] = (oli2_open >= len_1 || a3prime == 0) ? 0 : rco;
          // openning oligo 3' end
          thres[1] = thres[0] + ((oli2_open == oli_open || a3prime == 0) ? 0 : roo[oli2_open - 1]);
          // closing oligo 5' end
          thres[2] = thres[1] + (((oli_open == 0) || (oli_open <= (fork-len_1))) ? 0 : rco);
          // openning oligo 5' end
          thres[3] = thres[2] + roo[oli_open];
          // openning fork only if not fully open
          thres[4] = thres[3] + (((fork == 0) || (afork == 0)) ? 0 : roh[fork-1]);
          // closing fork only if not blocked by oligo
          thres[5] = thres[4] + (((oli_open <= (fork-len_1)) || (afork == 0)) ? 0 : rch);
          den = thres[5];


          tmp = ran1(&idum);
          dt = (tmp > 0) ? -log(tmp)/den :0;
          t += dt;
          ttot += dt;
          tmp = den*ran1(&idum);
          if (disp != WIN_CANCEL)
            {
              disp = win_printf("Fork %d, Oli 5' %d, oli3' %d\n"
                                "tmp = %g\n"
                                "Oligo 3'end %c ++ if in [0,%g]\n"
                                "Oligo 3'end %c -- if in [%g,%g]\n"
                                "Oligo 5'end %c -- if in [%g,%g]\n"
                                "Oligo 5'end %c ++ if in [%g,%g]\n"
                                "    Fork    %c -- if in [%g,%g]\n"
                                "    Fork    %c ++ if in [%g,%g]\n"
                                ,fork-len_1,oli_open,oli2_open,tmp
                                ,(tmp < thres[0])? 'y':'n',thres[0]
                                ,(tmp >= thres[0] && tmp < thres[1])? 'y':'n',thres[0],thres[1]
                                ,(tmp >= thres[1] && tmp < thres[2])? 'y':'n',thres[1],thres[2]
                                ,(tmp >= thres[2] && tmp < thres[3])? 'y':'n',thres[2],thres[3]
                                ,(tmp >= thres[3] && tmp < thres[4])? 'y':'n',thres[3],thres[4]
                                ,(tmp >= thres[4] && tmp < thres[5])? 'y':'n',thres[4],thres[5]);
            }
          if (tmp < thres[0]) oli2_open++;
          if (tmp >= thres[0] && tmp < thres[1]) oli2_open--;
          if (tmp >= thres[1] && tmp < thres[2]) oli_open--;
          if (tmp >= thres[2] && tmp < thres[3]) oli_open++;
          if (tmp >= thres[3] && tmp < thres[4]) fork--;
          if (tmp >= thres[4] && tmp < thres[5]) fork++;

	  for(k = 0; dsop != NULL && k < oli_open_1 && k < len_1; k++)
            {
                if (k >= 0 && k < dsop->nx)       dsop->xd[k] += dt;
                else win_printf("K out of range %d",k);
            }
	  for(k = oli2_open_1; dsop != NULL && k < len_1; k++)
            {
                if (k >= 0 && k < dsop->nx)       dsop->xd[k] += dt;
                else win_printf("K out of range %d",k);
            }


	  if (dur == 0)
	    {
	      add_new_point_to_ds(dso, t_1, oli_open_1);
	      add_new_point_to_ds(dso2, t_1, oli2_open_1);
	      add_new_point_to_ds(dsf, t_1, (fork_1-len_1));
	      add_new_point_to_ds(dso, t, oli_open_1);
	      add_new_point_to_ds(dso2, t, oli2_open_1);
	      add_new_point_to_ds(dsf, t, (fork_1-len_1));
	      oli_open_1 = oli_open;
	      oli2_open_1 = oli2_open;
	      fork_1 = fork;
              t_1 = t;
	      add_new_point_to_ds(dso, t, oli_open);
	      add_new_point_to_ds(dso2, t, oli2_open);
	      add_new_point_to_ds(dsf, t, (fork-len_1));
	      i++;
	    }
          else
            {
	      oli_open_1 = oli_open;
	      oli2_open_1 = oli2_open;
	      fork_1 = fork;
              t_1 = t;
            }
	  if (j%65536 == 0)
                my_set_window_title("j %d pos %d %d %d", j,fork,oli_open,oli2_open);
	}
      else
	{
          if (dslb != NULL)
            {
               dslb->xd[oli_open] += 1;
            }
	  if (dur == 0)
	    {
	      add_new_point_to_ds(dso, t, oli_open);
	      add_new_point_to_ds(dso2, t, oli2_open);
	      add_new_point_to_ds(dsf, t, fork);
	      i++;
	      oli_open_1 = oli_open = 0;
	      oli2_open_1 = oli2_open = len_1;
	      fork_1 = fork = 0;
	      add_new_point_to_ds(dso, t, oli_open);
	      add_new_point_to_ds(dsf, t, fork);
	    }
	  if (dur == 1)
	    {
	      oli_open = 0;
	      fork = 0;
              oli2_open = len_1;
	      my_set_window_title("Event %d duration %f %3.1f%%", i,t,(float)(100*i)/nx);
	      add_new_point_to_ds(dsf, i++, t);
              for (k = 0, tmp = 0; k < dsf->nx; k++) tmp += dsf->yd[k];
              if (dsf->nx > 0) tmp/=dsf->nx;
              set_plot_title(op, "\\stack{{%soligo blocking %s}"
                             "{seq:%s}"
                             "{\\pt8 [Na+] = %g mM, [mg2+] = %g mM, T=%g^\\oc C; F = %g pN}{Mean duration %g}}"
                             ,((afork)?"Fork/":"")
                             ,((a3prime)?"5' and 3' end active":"5' end onlt")
                             ,oligo,Na,Mg,Ti,F,tmp);

              op->need_to_refresh = 1;
              refresh_plot(pr,dur_op);
	      t = 0;
	    }
	}
    }
  for (k = 0; dsop != NULL && ttot > 0 && k < dsop->nx; k++) dsop->xd[k] /= ttot;
  return refresh_plot(pr,pr->n_op-1);
}




int 	do_unzip_LNA_signal_SC_2sides_n(void)
{
        int i, j, k, dur_op = 0; // , i_1, io_1, io
  static int nx = 16384, new_plot = 1;//, timin = 0;
  pltreg *pr;
  O_p *op = NULL, *opo = NULL, *opl = NULL;
  d_s *dso = NULL, *dso2 = NULL, *dsf = NULL, *dsop = NULL, *dslb = NULL;
  static int idum = 4587, end_oligo = 0, dur = 0, afork = 1, a3prime = 1, gplo = 0, gpll = 0, debug = 0, verbose = 0, at_penalty = 1;
  static char oligo[128] = {0};
  static float Na = 150, Mg = 10, Ti = 25, F = 8.6, T0 = 273.15;
  float dg[128] = {0};
  float dgh[128] = {0};
  float roh[128] = {0};
  float roo[128] = {0};
  float thres[10] = {0};
  float R = 1.9872;
  float cor = 1000.0/(R*(T0+Ti)); // cal/K-mol
  float tmp;
  int len, index, indexadn; //, hp, ol;
  float d, bp, ft, u, gss, gds, rco, rch, dgt;
  char stuff[2048] = {0};
  int disp = WIN_OK;


  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return 1;
  if (strlen(oligo) == 0) sprintf(oligo,"ACAGCCAGCCGA");

  i = win_scanf("Generate an unzipping signal of a fork with a blocking LNA oligo "
		"Following Simona Cocco model\nwhere fork and oligo openning "
		"are not bound but where the fork is behind the oligo opening\n"
                "Allows the second end of the oligo to open\n"
		"Specify the oligo sequence (upper case -> LNA) %s\n"
		"Oligo ends correction:\n"
		"%R->no %r->on 5' %r->on 3'only %r->on both 5' and 3'\n"
		"[Na+] = %4f mM; [Mg2+] = %4f mM; T = %4f \\oc C\n"
		"Force = %4f pN\n"
		"%R in this plot %r in a new plot\n"
		"Total number of steps %8d\n"
		"Random seed %8d\n"
		"%R real time or %r duration \n"
                "%bFork active %b3-end of oligo active\n"
                "%b Generate plot with average openning probability\n"
                "%b Genertae a plot with statitics of last base attached\n"
                "%b debugging\n"
                "%b Display energiies\n"
		,oligo,&end_oligo,&Na,&Mg,&Ti,&F
		,&new_plot,&nx,&idum,&dur,&afork,&a3prime,&gplo,&gpll,&debug,&verbose);
  if (i == WIN_CANCEL)	return D_O_K;

  cor = 1000.0/(R*(T0+Ti)); // convert cal/K-mol in kBT
  init_LNA_array(Na, Mg, Ti);
  if (debug == 0)  disp = WIN_CANCEL;
  len = strlen(oligo);

  for (i = 1; i < len; i++)
    {
      index = indexadn = 0;
      if ((oligo[i-1]) == 'a')  indexadn = index = 0;
      if ((oligo[i-1]) == 'c')  indexadn = index = 8;
      if ((oligo[i-1]) == 'g')  indexadn = index = 16;
      if ((oligo[i-1]) == 't')  indexadn = index = 24;
      if ((oligo[i-1]) == 'A')  {index = 32; indexadn = 0;}
      if ((oligo[i-1]) == 'C')  {index = 40; indexadn = 8;}
      if ((oligo[i-1]) == 'G')  {index = 48; indexadn = 16;}
      if ((oligo[i-1]) == 'T')  {index = 56; indexadn = 24;}
      if ((oligo[i]) == 'a')  {index += 0; indexadn += 0;}
      if ((oligo[i]) == 'c')  {index += 1; indexadn += 1;}
      if ((oligo[i]) == 'g')  {index += 2; indexadn += 2;}
      if ((oligo[i]) == 't')  {index += 3; indexadn += 3;}
      if ((oligo[i]) == 'A')  {index += 4; indexadn += 0;}
      if ((oligo[i]) == 'C')  {index += 5; indexadn += 1;}
      if ((oligo[i]) == 'G')  {index += 6; indexadn += 2;}
      if ((oligo[i]) == 'T')  {index += 7; indexadn += 3;}

      dg[i-1] = -cor*D_G[index];
      dgh[i-1] = -cor*D_G[indexadn];
      if (i < 12)
              sprintf(stuff+strlen(stuff),"DeltaG %c%c %g Kcal/mol %g kBT HP %g kBT\n",oligo[i-1],oligo[i],-D_G[index],dg[i-1],dgh[i-1]);
    }

  for(i = 0; i < len-1; i++)
    {
      roo[i] = exp(-dg[i]);
      roh[i] = exp(-dgh[i]);
    }

  if (end_oligo & 0x1)
    {
      dgt =(((oligo[0]&0x5F) == 'A') || ((oligo[0]&0x5F) == 'T')) ? -cor*DGMF[INAT] : -cor*DGMF[INGC];
    }
  else if (at_penalty & 0x1)
    {
      dgt =(((oligo[0]&0x5F) == 'A') || ((oligo[0]&0x5F) == 'T')) ? -cor*DGMF[TP] : 0;
    }
  else dgt = 0;
  roo[0]= exp(-dg[0]-dgt);
  // no correction on hp

  if (end_oligo & 0x2)
    {
      dgt =(((oligo[len-1]&0x5F) == 'A') || ((oligo[len-1]&0x5F) == 'T')) ? -cor*DGMF[INAT] : -cor*DGMF[INGC];
    }
  else if (at_penalty & 0x2)
    {
      dgt =(((oligo[len-1]&0x5F) == 'A') || ((oligo[len-1]&0x5F) == 'T')) ? -cor*DGMF[TP] : 0;
    }
  else dgt = 0;
  roo[len-2]= exp(-dg[len-2]-dgt);
  // no correction on hp

  //elasticity models for the closing rates for the ds and ss DNA
  d = 0.56*1.12;
  bp = 1.5*1.125;
  d = 0.605;// # 0.567 #0.56*1.12 new 0.605
  bp = 1.828; // # 1.78 # 1.5*1.125 new 1.828
  ft = 4.1*(T0+Ti)/(T0+25);
  u = bp*F/ft;
  gss = d/bp*log(sinh(u)/u) ;
  gds = (F/ft - sqrt(F/ft/50) + 0.5*F*F/ft/1230)*0.34;

  //closing rates:only depend on the force
  rch = exp(-2*gss);
  rco = exp(-gss+gds);

  for (i = 0; i < len-1 && i < 12; i++)
    sprintf(stuff+strlen(stuff),"roo[%d] = %g; roh[%d] = %g;\n",i,roo[i],i, roh[i]);
  if (i < len ) sprintf(stuff+strlen(stuff),"...\nrco = %g; rch = %g;\n",rco, rch);
  else sprintf(stuff+strlen(stuff),"rco = %g; rch = %g;\n",rco, rch);

  win_printf("%s",stuff);


  if (new_plot)
    {
      if ((op = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create plot");
      dsf = op->dat[0];
      dsf->nx = dsf->ny = 0;
      dur_op = pr->n_op-1;
      set_plot_title(op, "\\stack{{%soligo LNA blocking %s}"
		     "{seq:%s}"
		     "{\\pt8 [Na+] = %g mM, [mg2+] = %g mM, T=%g^\\oc C; F = %g pN}}"
		     ,((afork)?"Fork/":""),((a3prime)?"5' and 3' end active":"5' end onlt"),oligo,Na,Mg,Ti,F);
    }
  else
    {
      if ((dsf = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      dsf->nx = dsf->ny = 0;
      dur_op = pr->cur_op;
    }

  if (dur == 0)
    {
      if ((dso = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      dso->nx = dso->ny = 0;
      set_ds_source(dso, "Fork/->(oligo) blocking;  seq:%s; [Na+] = %g mM, [mg2+] = %g mM, T=%g C; F = %g pN"
                    ,oligo,Na,Mg,Ti,F);
      if ((dso2 = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      dso2->nx = dso2->ny = 0;
      set_ds_source(dso2, "Fork/->(oligo) blocking;  seq:%s; [Na+] = %g mM, [mg2+] = %g mM, T=%g C; F = %g pN"
                    ,oligo,Na,Mg,Ti,F);
    }


  //set_dot_line(ds);
  set_ds_source(dsf, "->(Fork)/oligo blocking;  seq:%s; [Na+] = %g mM, [mg2+] = %g mM, T=%g C; F = %g pN"
		,oligo,Na,Mg,Ti,F);

  if (gplo)
     {
      if ((opo = create_and_attach_one_plot(pr, len-1, len-1, 0)) == NULL)
	return win_printf_OK("cannot create plot");
      dsop = opo->dat[0];
       for (k = 0; k < dsop->nx; k++) dsop->yd[k] = 0.5+k;
      set_plot_title(opo, "\\stack{{%soligo LNA blocking %s}"
		     "{seq:%s}"
		     "{\\pt8 [Na+] = %g mM, [mg2+] = %g mM, T=%g^\\oc C; F = %g pN}}"
		     ,((afork)?"Fork/":""),((a3prime)?"5' and 3' end active":"5' end onlt"),oligo,Na,Mg,Ti,F);
                                                                                                                         set_plot_x_title(opo,"Probability of openning");
      set_plot_y_title(opo,"Position along oligo");
     }
  if (gpll)
     {
      if ((opl = create_and_attach_one_plot(pr, len, len, 0)) == NULL)
	return win_printf_OK("cannot create plot");
      dslb = opl->dat[0];
       for (k = 0; k < dslb->nx; k++) dslb->yd[k] = k;
      set_plot_title(opl, "\\stack{{%soligo LNA blocking %s}"
		     "{seq:%s}"
		     "{\\pt8 [Na+] = %g mM, [mg2+] = %g mM, T=%g^\\oc C; F = %g pN}}"
		     ,((afork)?"Fork/":""),((a3prime)?"5' and 3' end active":"5' end onlt"),oligo,Na,Mg,Ti,F);
                                                                                                                         set_plot_x_title(opl,"Nunber of exit events");
      set_plot_y_title(opl,"Base Nb where oligo detached");
     }

  float t = 0, t_1 = 0, dt = 0, den = 0, ttot = 0;
  int fork, fork_1 = 0, oli_open, oli_open_1 = 0, len_1 = len - 1;
  int oli2_open = len_1, oli2_open_1 = len_1;

  for (i = j = 0, fork = 0, oli_open = 0, oli2_open = len_1; i < nx; j++)
    {
      //fork = len - hp; // measure the nb of base closed in the hairpin
      //fork starts at 0 and increases it is always <= oli_open
      // oli2_open = len - oli2 ; oli2_open = len no breathing
      // 0 <= fork <= oli_open < oli2_open <= len
      // detaching when oli_open == oli2_open


      if (oli_open < oli2_open)  // we are still attached
	{      // we try to close oligo
          if (oli_open < 0 || oli_open >= len_1)
                  win_printf("oli_open out %d",oli_open);
          if (oli2_open < 1 || oli2_open > len_1)
                  win_printf("oli2_open out %d",oli2_open);
          if (oli_open < fork)
                  win_printf("oli_open %d < fork %d",oli_open,fork);
          if (fork < 0 || fork > len_1)
                  win_printf("fork %d out of range [0,%d]",fork,len_1);

          // closing oligo 3' end
	  thres[0] = (oli2_open >= len_1 || a3prime == 0) ? 0 : rco;
          // openning oligo 3' end
          thres[1] = thres[0] + ((oli2_open == oli_open || a3prime == 0) ? 0 : roo[oli2_open - 1]);
          // closing oligo 5' end
          thres[2] = thres[1] + ((oli_open == 0 || oli_open <= fork) ? 0 : rco);
          // openning oligo 5' end
          thres[3] = thres[2] + roo[oli_open];
          // openning fork only if not fully open
          thres[4] = thres[3] + ((fork == 0 || afork == 0) ? 0 : roh[fork-1]);
          // closing fork only if not blocked by oligo
          thres[5] = thres[4] + ((oli_open <= fork || afork == 0) ? 0 : rch);
          den = thres[5];


          tmp = ran1(&idum);
          dt = -log(tmp)/den;
          t += dt;
          ttot += dt;
          tmp = den*ran1(&idum);
          if (disp != WIN_CANCEL)
            {
              disp = win_printf("Fork %d, Oli 5' %d, oli3' %d\n"
                                "tmp = %g\n"
                                "Oligo 3'end %c ++ if in [0,%g]\n"
                                "Oligo 3'end %c -- if in [%g,%g]\n"
                                "Oligo 5'end %c -- if in [%g,%g]\n"
                                "Oligo 5'end %c ++ if in [%g,%g]\n"
                                "    Fork    %c -- if in [%g,%g]\n"
                                "    Fork    %c ++ if in [%g,%g]\n"
                                ,fork,oli_open,oli2_open,tmp
                                ,(tmp < thres[0])? 'y':'n',thres[0]
                                ,(tmp >= thres[0] && tmp < thres[1])? 'y':'n',thres[0],thres[1]
                                ,(tmp >= thres[1] && tmp < thres[2])? 'y':'n',thres[1],thres[2]
                                ,(tmp >= thres[2] && tmp < thres[3])? 'y':'n',thres[2],thres[3]
                                ,(tmp >= thres[3] && tmp < thres[4])? 'y':'n',thres[3],thres[4]
                                ,(tmp >= thres[4] && tmp < thres[5])? 'y':'n',thres[4],thres[5]);
            }
          if (tmp < thres[0]) oli2_open++;
          if (tmp >= thres[0] && tmp < thres[1]) oli2_open--;
          if (tmp >= thres[1] && tmp < thres[2]) oli_open--;
          if (tmp >= thres[2] && tmp < thres[3]) oli_open++;
          if (tmp >= thres[3] && tmp < thres[4]) fork--;
          if (tmp >= thres[4] && tmp < thres[5]) fork++;

	  for(k = 0; dsop != NULL && k < oli_open_1 && k < len_1; k++)
            {
                if (k >= 0 && k < dsop->nx)       dsop->xd[k] += dt;
                else win_printf("K out of range %d",k);
            }
	  for(k = oli2_open_1; dsop != NULL && k < len_1; k++)
            {
                if (k >= 0 && k < dsop->nx)       dsop->xd[k] += dt;
                else win_printf("K out of range %d",k);
            }


	  if (dur == 0)
	    {
	      add_new_point_to_ds(dso, t_1, oli_open_1);
	      add_new_point_to_ds(dso2, t_1, oli2_open_1);
	      add_new_point_to_ds(dsf, t_1, fork_1);
	      add_new_point_to_ds(dso, t, oli_open_1);
	      add_new_point_to_ds(dso2, t, oli2_open_1);
	      add_new_point_to_ds(dsf, t, fork_1);
	      oli_open_1 = oli_open;
	      oli2_open_1 = oli2_open;
	      fork_1 = fork;
              t_1 = t;
	      add_new_point_to_ds(dso, t, oli_open);
	      add_new_point_to_ds(dso2, t, oli2_open);
	      add_new_point_to_ds(dsf, t, fork);
	      i++;
	    }
          else
            {
	      oli_open_1 = oli_open;
	      oli2_open_1 = oli2_open;
	      fork_1 = fork;
              t_1 = t;
            }
	  //if (j%65536 == 0)
          //    my_set_window_title("j %d pos %d %d %d", j,fork,oli_open,oli2_open);
	}
      else
	{
          if (dslb != NULL)
            {
               dslb->xd[oli_open] += 1;
            }
	  if (dur == 0)
	    {
	      add_new_point_to_ds(dso, t, oli_open);
	      add_new_point_to_ds(dso2, t, oli2_open);
	      add_new_point_to_ds(dsf, t, fork);
	      i++;
	      oli_open_1 = oli_open = 0;
	      oli2_open_1 = oli2_open = len_1;
	      fork_1 = fork = 0;
	      add_new_point_to_ds(dso, t, oli_open);
	      add_new_point_to_ds(dsf, t, fork);
	    }
	  if (dur == 1)
	    {
	      oli_open = 0;
	      fork = 0;
              oli2_open = len_1;
	      my_set_window_title("Event %d duration %f %3.1f%%", i,t,(float)(100*i)/nx);
	      add_new_point_to_ds(dsf, i++, t);
              for (k = 0, tmp = 0; k < dsf->nx; k++) tmp += dsf->yd[k];
              if (dsf->nx > 0) tmp/=dsf->nx;
              set_plot_title(op, "\\stack{{%soligo LNA blocking %s}"
		     "{seq:%s}"
		     "{\\pt8 [Na+] = %g mM, [mg2+] = %g mM, T=%g^\\oc C; F = %g pN}{mean time %g}}"
                             ,((afork)?"Fork/":""),((a3prime)?"5' and 3' end active":"5' end onlt"),oligo,Na,Mg,Ti,F,tmp);

              op->need_to_refresh = 1;
              refresh_plot(pr,dur_op);
	      t = 0;
	    }
	}
    }
  for (k = 0; dsop != NULL && ttot > 0 && k < dsop->nx; k++) dsop->xd[k] /= ttot;
  return refresh_plot(pr,pr->n_op-1);
}


int 	do_unzip_RNA_oligo_from_DNA_template_SC_2sides_n(void)
{
        int i, j, k, dur_op = 0; // , i_1, io_1, io
  static int nx = 16384, new_plot = 1;//, timin = 0;
  pltreg *pr;
  O_p *op = NULL, *opo = NULL, *opl = NULL;
  d_s *dso = NULL, *dso2 = NULL, *dsf = NULL, *dsop = NULL, *dslb = NULL;
  static int idum = 4587, end_oligo = 0, dur = 0, afork = 1, a3prime = 1, gplo = 0, gpll = 0, debug = 0, no_salt_correction = 0, at_penalty = 3;
  static char oligo[128] = {0};
  static float Na = 150, Mg = 10, Ti = 25, F = 8.6, T0 = 273.15;
  float dg[128] = {0};
  float dgh[128] = {0};
  float roh[128] = {0};
  float roo[128] = {0};
  float thres[10] = {0};
  float R = 1.9872;
  float cor = 1000.0/(R*(T0+Ti)); // cal/K-mol
  float tmp;
  int len, index; //, hp, ol;
  float d, bp, ft, u, gss, gds, rco, rch, dgt;
  char stuff[2048] = {0};
  int disp = WIN_OK;


  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return 1;
  if (strlen(oligo) == 0) sprintf(oligo,"ACAGCCAGCCGA");

  i = win_scanf("Generate an unzipping signal of a fork with a blocking pure RNA oligo "
		"Following Simona Cocco model\nwhere fork and oligo openning "
		"are not bound but where the fork is behind the oligo opening\n"
                "Allows the second end of the oligo to open\n"
		"Specify the oligo sequence in RNA %s\n"
		"Oligo ends correction:\n"
		"%R->no %r->on 5' %r->on 3'only %r->on both 5' and 3'\n"
		"else Oligo AT/GC penalty at the ends:\n"
		"%R->no %r->on 5' %r->on 3'only %r->on both 5' and 3'\n"
		"%b->No salt entropy correction \n"
		"[Na+] = %4f mM; [Mg2+] = %4f mM; T = %4f \\oc C\n"
		"Force = %4f pN\n"
		"%R in this plot %r in a new plot\n"
		"Total number of steps %8d\n"
		"Random seed %8d\n"
		"%R real time or %r duration \n"
                "%bFork active %b3-end of oligo active\n"
                "%b Generate plot with average openning probability\n"
                "%b Genertae a plot with statitics of last base attached\n"
                "%b debugging"
		,oligo,&end_oligo,&at_penalty,&no_salt_correction,&Na,&Mg,&Ti,&F
		,&new_plot,&nx,&idum,&dur,&afork,&a3prime,&gplo,&gpll,&debug);
  if (i == WIN_CANCEL)	return D_O_K;

  cor = 1000.0/(R*(T0+Ti)); // convert cal/K-mol in kBT
  init_RNA_DNA_array(Na, Mg, Ti);
  init_Mfold31_array(Na, Mg, Ti, 1, no_salt_correction, dsCharge);
  if (debug == 0)  disp = WIN_CANCEL;
  len = strlen(oligo);

  for (i = 1; i < len; i++)
    {
      index = 0;
      if ((oligo[i-1]&0x5F) == 'A')  index = 0;
      if ((oligo[i-1]&0x5F) == 'C')  index = 4;
      if ((oligo[i-1]&0x5F) == 'G')  index = 8;
      if ((oligo[i-1]&0x5F) == 'T' || (oligo[i-1]&0x5F) == 'U')  index = 12;
      if ((oligo[i]&0x5F) == 'A')  index += 0;
      if ((oligo[i]&0x5F) == 'C')  index += 1;
      if ((oligo[i]&0x5F) == 'G')  index += 2;
      if ((oligo[i]&0x5F) == 'T' || (oligo[i-1]&0x5F) == 'U')  index += 3;
      dg[i-1] = -cor*DG_RD[index];
      dgh[i-1] = -cor*DGMF[index];
      if (i < 12)
              sprintf(stuff+strlen(stuff),"\\Delta G %c%c RNA/DNA %g HP %g Kcal/mol %g; %g kBT\n"
                      ,oligo[i-1],oligo[i],-DG_RD[index],-DGMF[index],dg[i-1],dgh[i-1]);
    }

  for(i = 0; i < len-1; i++)
    {
      roo[i] = exp(-dg[i]);
      roh[i] = exp(-dgh[i]);
    }

  if (end_oligo & 0x1)
    {
      dgt =(((oligo[0]&0x5F) == 'A') || ((oligo[0]&0x5F) == 'T')) ? -cor*DGMF[INAT] : -cor*DGMF[INGC];
    }
  else if (at_penalty & 0x1)
    {
      dgt =(((oligo[0]&0x5F) == 'A') || ((oligo[0]&0x5F) == 'T')) ? -cor*DGMF[TP] : 0;
    }
  else dgt = 0;
  roo[0]= exp(-dg[0]-dgt);
  // no correction on hp

  if (end_oligo & 0x2)
    {
      dgt =(((oligo[len-1]&0x5F) == 'A') || ((oligo[len-1]&0x5F) == 'T')) ? -cor*DGMF[INAT] : -cor*DGMF[INGC];
    }
  else if (at_penalty & 0x2)
    {
      dgt =(((oligo[len-1]&0x5F) == 'A') || ((oligo[len-1]&0x5F) == 'T')) ? -cor*DGMF[TP] : 0;
    }
  else dgt = 0;
  roo[len-2]= exp(-dg[len-2]-dgt);
  // no correction on hp

  //elasticity models for the closing rates for the ds and ss DNA
  d = 0.56*1.12;
  bp = 1.5*1.125;
  d = 0.605;// # 0.567 #0.56*1.12 new 0.605
  bp = 1.828; // # 1.78 # 1.5*1.125 new 1.828
  ft = 4.1*(T0+Ti)/(T0+25);
  u = bp*F/ft;
  gss = d/bp*log(sinh(u)/u) ;
  gds = (F/ft - sqrt(F/ft/50) + 0.5*F*F/ft/1230)*0.34;

  //closing rates:only depend on the force
  rch = exp(-2*gss);
  rco = exp(-gss+gds);

  for (i = 0; i < len-1 && i < 12; i++)
    sprintf(stuff+strlen(stuff),"roo[%d] = %g; roh[%d] = %g;\n",i,roo[i],i, roh[i]);
  if (i < len ) sprintf(stuff+strlen(stuff),"...\nrco = %g; rch = %g;\n",rco, rch);
  else sprintf(stuff+strlen(stuff),"rco = %g; rch = %g;\n",rco, rch);

  win_printf("%s",stuff);


  if (new_plot)
    {
      if ((op = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create plot");
      dsf = op->dat[0];
      dsf->nx = dsf->ny = 0;
      dur_op = pr->n_op-1;
      set_plot_title(op, "\\stack{{%soligo RNA blocking %s}"
		     "{seq:%s}"
		     "{\\pt8 [Na+] = %g mM, [mg2+] = %g mM, T=%g^\\oc C; F = %g pN}}"
		     ,((afork)?"Fork/":""),((a3prime)?"5' and 3' end active":"5' end onlt"),oligo,Na,Mg,Ti,F);
    }
  else
    {
      if ((dsf = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      dsf->nx = dsf->ny = 0;
      dur_op = pr->cur_op;
    }

  if (dur == 0)
    {
      if ((dso = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      dso->nx = dso->ny = 0;
      set_ds_source(dso, "Fork/->(oligo) blocking;  seq:%s; [Na+] = %g mM, [mg2+] = %g mM, T=%g C; F = %g pN"
                    ,oligo,Na,Mg,Ti,F);
      if ((dso2 = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      dso2->nx = dso2->ny = 0;
      set_ds_source(dso2, "Fork/->(oligo) blocking;  seq:%s; [Na+] = %g mM, [mg2+] = %g mM, T=%g C; F = %g pN"
                    ,oligo,Na,Mg,Ti,F);
    }


  //set_dot_line(ds);
  set_ds_source(dsf, "->(Fork)/oligo blocking;  seq:%s; [Na+] = %g mM, [mg2+] = %g mM, T=%g C; F = %g pN"
		,oligo,Na,Mg,Ti,F);

  if (gplo)
     {
      if ((opo = create_and_attach_one_plot(pr, len-1, len-1, 0)) == NULL)
	return win_printf_OK("cannot create plot");
      dsop = opo->dat[0];
       for (k = 0; k < dsop->nx; k++) dsop->yd[k] = 0.5+k;
      set_plot_title(opo, "\\stack{{%soligo RNA blocking %s}"
		     "{seq:%s}"
		     "{\\pt8 [Na+] = %g mM, [mg2+] = %g mM, T=%g^\\oc C; F = %g pN}}"
		     ,((afork)?"Fork/":""),((a3prime)?"5' and 3' end active":"5' end onlt"),oligo,Na,Mg,Ti,F);
                                                                                                                         set_plot_x_title(opo,"Probability of openning");
      set_plot_y_title(opo,"Position along oligo");
     }
  if (gpll)
     {
      if ((opl = create_and_attach_one_plot(pr, len, len, 0)) == NULL)
	return win_printf_OK("cannot create plot");
      dslb = opl->dat[0];
       for (k = 0; k < dslb->nx; k++) dslb->yd[k] = k;
      set_plot_title(opl, "\\stack{{%soligo LNA blocking %s}"
		     "{seq:%s}"
		     "{\\pt8 [Na+] = %g mM, [mg2+] = %g mM, T=%g^\\oc C; F = %g pN}}"
		     ,((afork)?"Fork/":""),((a3prime)?"5' and 3' end active":"5' end onlt"),oligo,Na,Mg,Ti,F);
                                                                                                                         set_plot_x_title(opl,"Nunber of exit events");
      set_plot_y_title(opl,"Base Nb where oligo detached");
     }

  float t = 0, t_1 = 0, dt = 0, den = 0, ttot = 0;
  int fork, fork_1 = 0, oli_open, oli_open_1 = 0, len_1 = len - 1;
  int oli2_open = len_1, oli2_open_1 = len_1;

  for (i = j = 0, fork = 0, oli_open = 0, oli2_open = len_1; i < nx; j++)
    {
      //fork = len - hp; // measure the nb of base closed in the hairpin
      //fork starts at 0 and increases it is always <= oli_open
      // oli2_open = len - oli2 ; oli2_open = len no breathing
      // 0 <= fork <= oli_open < oli2_open <= len
      // detaching when oli_open == oli2_open


      if (oli_open < oli2_open)  // we are still attached
	{      // we try to close oligo
          if (oli_open < 0 || oli_open >= len_1)
                  win_printf("oli_open out %d",oli_open);
          if (oli2_open < 1 || oli2_open > len_1)
                  win_printf("oli2_open out %d",oli2_open);
          if (oli_open < fork)
                  win_printf("oli_open %d < fork %d",oli_open,fork);
          if (fork < 0 || fork > len_1)
                  win_printf("fork %d out of range [0,%d]",fork,len_1);

          // closing oligo 3' end
	  thres[0] = (oli2_open >= len_1 || a3prime == 0) ? 0 : rco;
          // openning oligo 3' end
          thres[1] = thres[0] + ((oli2_open == oli_open || a3prime == 0) ? 0 : roo[oli2_open - 1]);
          // closing oligo 5' end
          thres[2] = thres[1] + ((oli_open == 0 || oli_open <= fork) ? 0 : rco);
          // openning oligo 5' end
          thres[3] = thres[2] + roo[oli_open];
          // openning fork only if not fully open
          thres[4] = thres[3] + ((fork == 0 || afork == 0) ? 0 : roh[fork-1]);
          // closing fork only if not blocked by oligo
          thres[5] = thres[4] + ((oli_open <= fork || afork == 0) ? 0 : rch);
          den = thres[5];


          tmp = ran1(&idum);
          dt = -log(tmp)/den;
          t += dt;
          ttot += dt;
          tmp = den*ran1(&idum);
          if (disp != WIN_CANCEL)
            {
              disp = win_printf("Fork %d, Oli 5' %d, oli3' %d\n"
                                "tmp = %g\n"
                                "Oligo 3'end %c ++ if in [0,%g]\n"
                                "Oligo 3'end %c -- if in [%g,%g]\n"
                                "Oligo 5'end %c -- if in [%g,%g]\n"
                                "Oligo 5'end %c ++ if in [%g,%g]\n"
                                "    Fork    %c -- if in [%g,%g]\n"
                                "    Fork    %c ++ if in [%g,%g]\n"
                                ,fork,oli_open,oli2_open,tmp
                                ,(tmp < thres[0])? 'y':'n',thres[0]
                                ,(tmp >= thres[0] && tmp < thres[1])? 'y':'n',thres[0],thres[1]
                                ,(tmp >= thres[1] && tmp < thres[2])? 'y':'n',thres[1],thres[2]
                                ,(tmp >= thres[2] && tmp < thres[3])? 'y':'n',thres[2],thres[3]
                                ,(tmp >= thres[3] && tmp < thres[4])? 'y':'n',thres[3],thres[4]
                                ,(tmp >= thres[4] && tmp < thres[5])? 'y':'n',thres[4],thres[5]);
            }
          if (tmp < thres[0]) oli2_open++;
          if (tmp >= thres[0] && tmp < thres[1]) oli2_open--;
          if (tmp >= thres[1] && tmp < thres[2]) oli_open--;
          if (tmp >= thres[2] && tmp < thres[3]) oli_open++;
          if (tmp >= thres[3] && tmp < thres[4]) fork--;
          if (tmp >= thres[4] && tmp < thres[5]) fork++;

	  for(k = 0; dsop != NULL && k < oli_open_1 && k < len_1; k++)
            {
                if (k >= 0 && k < dsop->nx)       dsop->xd[k] += dt;
                else win_printf("K out of range %d",k);
            }
	  for(k = oli2_open_1; dsop != NULL && k < len_1; k++)
            {
                if (k >= 0 && k < dsop->nx)       dsop->xd[k] += dt;
                else win_printf("K out of range %d",k);
            }


	  if (dur == 0)
	    {
	      add_new_point_to_ds(dso, t_1, oli_open_1);
	      add_new_point_to_ds(dso2, t_1, oli2_open_1);
	      add_new_point_to_ds(dsf, t_1, fork_1);
	      add_new_point_to_ds(dso, t, oli_open_1);
	      add_new_point_to_ds(dso2, t, oli2_open_1);
	      add_new_point_to_ds(dsf, t, fork_1);
	      oli_open_1 = oli_open;
	      oli2_open_1 = oli2_open;
	      fork_1 = fork;
              t_1 = t;
	      add_new_point_to_ds(dso, t, oli_open);
	      add_new_point_to_ds(dso2, t, oli2_open);
	      add_new_point_to_ds(dsf, t, fork);
	      i++;
	    }
          else
            {
	      oli_open_1 = oli_open;
	      oli2_open_1 = oli2_open;
	      fork_1 = fork;
              t_1 = t;
            }
	  //if (j%65536 == 0)
          //    my_set_window_title("j %d pos %d %d %d", j,fork,oli_open,oli2_open);
	}
      else
	{
          if (dslb != NULL)
            {
               dslb->xd[oli_open] += 1;
            }
	  if (dur == 0)
	    {
	      add_new_point_to_ds(dso, t, oli_open);
	      add_new_point_to_ds(dso2, t, oli2_open);
	      add_new_point_to_ds(dsf, t, fork);
	      i++;
	      oli_open_1 = oli_open = 0;
	      oli2_open_1 = oli2_open = len_1;
	      fork_1 = fork = 0;
	      add_new_point_to_ds(dso, t, oli_open);
	      add_new_point_to_ds(dsf, t, fork);
	    }
	  if (dur == 1)
	    {
	      oli_open = 0;
	      fork = 0;
              oli2_open = len_1;
	      my_set_window_title("Event %d duration %f %3.1f%%", i,t,(float)(100*i)/nx);
	      add_new_point_to_ds(dsf, i++, t);
              for (k = 0, tmp = 0; k < dsf->nx; k++) tmp += dsf->yd[k];
              if (dsf->nx > 0) tmp/=dsf->nx;
              set_plot_title(op, "\\stack{{%soligo RNA blocking %s}"
		     "{seq:%s}"
		     "{\\pt8 [Na+] = %g mM, [mg2+] = %g mM, T=%g^\\oc C; F = %g pN}{mean time %g}}"
                             ,((afork)?"Fork/":""),((a3prime)?"5' and 3' end active":"5' end onlt"),oligo,Na,Mg,Ti,F,tmp);

              op->need_to_refresh = 1;
              refresh_plot(pr,dur_op);
	      t = 0;
	    }
	}
    }
  for (k = 0; dsop != NULL && ttot > 0 && k < dsop->nx; k++) dsop->xd[k] /= ttot;
  return refresh_plot(pr,pr->n_op-1);
}



int 	do_unzip_manual_cor_signal_SC_2sides_n(void)
{
        int i, j, k, dur_op = 0; // , i_1, io_1, io
  static int nx = 16384, new_plot = 1;//, timin = 0;
  pltreg *pr;
  O_p *op = NULL, *opo = NULL, *opl = NULL;
  d_s *dso = NULL, *dso2 = NULL, *dsf = NULL, *dsop = NULL, *dslb = NULL;
  static int idum = 4587, end_oligo = 0, dur = 0, afork = 1, a3prime = 1, gplo = 0, gpll = 0, debug = 0, no_salt_correction = 0, at_penalty = 3;
  static char oligo[128] = {0};
  static float Na = 150, Mg = 10, Ti = 25, F = 8.6, T0 = 273.15;
  static float e_cor[128] = {0}, common_cor = 0;
  float dg[128] = {0};
  float dgh[128] = {0};
  float roh[128] = {0};
  float roo[128] = {0};
  float thres[10] = {0};
  float R = 1.9872;
  float cor = 1000.0/(R*(T0+Ti)); // cal/K-mol
  float tmp;
  int len, index, indexadn; //, hp, ol;
  float d, bp, ft, u, gss, gds, rco, rch, dgt;
  char stuff[2048] = {0}, ener[2048] = {0};
  int disp = WIN_OK;


  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return 1;
  if (strlen(oligo) == 0) sprintf(oligo,"ACAGCCAGCCGA");

  i = win_scanf("Generate an unzipping signal of a fork with a blocking special oligo "
		"Following Simona Cocco model\nwhere fork and oligo openning "
		"are not bound but where the fork is behind the oligo opening\n"
                "Allows the second end of the oligo to open\n"
		"Specify the oligo sequence (upper case -> special base with corection) %s\n"
		"Oligo ends correction:\n"
		"%R->no %r->on 5' %r->on 3'only %r->on both 5' and 3'\n"
		"else Oligo AT/GC penalty at the ends:\n"
		"%R->no %r->on 5' %r->on 3'only %r->on both 5' and 3'\n"
		"%b->No salt entropy correction \n"
		"[Na+] = %4f mM; [Mg2+] = %4f mM; T = %4f \\oc C\n"
		"Force = %4f pN\n"
                "Energy correction added to all oligo bases %5f (Kcal/mol)\n"
		"%R in this plot %r in a new plot\n"
		"Total number of steps %8d\n"
		"Random seed %8d\n"
		"%R real time or %r duration \n"
                "%bFork active %b3-end of oligo active\n"
                "%b Generate plot with average openning probability\n"
                "%b Genertae a plot with statitics of last base attached\n"
                "%b debugging"
		,oligo,&end_oligo,&at_penalty,&no_salt_correction,&Na,&Mg,&Ti,&F,&common_cor
		,&new_plot,&nx,&idum,&dur,&afork,&a3prime,&gplo,&gpll,&debug);
  if (i == WIN_CANCEL)	return D_O_K;

  cor = 1000.0/(R*(T0+Ti)); // convert cal/K-mol in kBT
  init_LNA_array(Na, Mg, Ti);
  init_Mfold31_array(Na, Mg, Ti, 1, no_salt_correction, dsCharge);
  if (debug == 0)  disp = WIN_CANCEL;
  len = strlen(oligo);

  for (i = 1; i < len; i++)
    {
       if ((oligo[i-1] == 'A') || (oligo[i-1] == 'C') ||
           (oligo[i-1] == 'G') || (oligo[i-1] == 'T') ||
           (oligo[i] == 'A') || (oligo[i] == 'C') ||
           (oligo[i] == 'G') || (oligo[i] == 'T'))
         {
                 sprintf(stuff,"Specific energy corrections\n"
                         "For dinucleotide %d: %c%c correction%%5f (Kcal/mol)\n",i-1,oligo[i-1],oligo[i]);
                 win_scanf(stuff,e_cor+i-1);
         }
    }

  stuff[0] = 0;
  sprintf(ener,"DeltaG in Kcal/mol: ");
  for (i = 1; i < len; i++)
    {
      index = indexadn = 0;
      if ((oligo[i-1]) == 'a')  indexadn = index = 0;
      if ((oligo[i-1]) == 'c')  indexadn = index = 8;
      if ((oligo[i-1]) == 'g')  indexadn = index = 16;
      if ((oligo[i-1]) == 't')  indexadn = index = 24;
      if ((oligo[i-1]) == 'A')  {index = 32; indexadn = 0;}
      if ((oligo[i-1]) == 'C')  {index = 40; indexadn = 8;}
      if ((oligo[i-1]) == 'G')  {index = 48; indexadn = 16;}
      if ((oligo[i-1]) == 'T')  {index = 56; indexadn = 24;}
      if ((oligo[i]) == 'a')  {index += 0; indexadn += 0;}
      if ((oligo[i]) == 'c')  {index += 1; indexadn += 1;}
      if ((oligo[i]) == 'g')  {index += 2; indexadn += 2;}
      if ((oligo[i]) == 't')  {index += 3; indexadn += 3;}
      if ((oligo[i]) == 'A')  {index += 4; indexadn += 0;}
      if ((oligo[i]) == 'C')  {index += 5; indexadn += 1;}
      if ((oligo[i]) == 'G')  {index += 6; indexadn += 2;}
      if ((oligo[i]) == 'T')  {index += 7; indexadn += 3;}

      tmp = (index != indexadn) ? e_cor[i-1] : 0;
      dg[i-1] = -cor*(D_G[indexadn]+common_cor+tmp);
      dgh[i-1] = -cor*D_G[indexadn];
      if (i < 12)
        {
          sprintf(stuff+strlen(stuff),"DeltaG %c%c %g + %g Kcal/mol %g kBT HP %g kBT\n"
                  ,oligo[i-1],oligo[i],-D_G[indexadn],common_cor+e_cor[i-1],dg[i-1],dgh[i-1]);
          sprintf(ener+strlen(ener),"%c%c: %g + %g;"
                  ,oligo[i-1],oligo[i],-D_G[indexadn],common_cor+e_cor[i-1]);
        }
    }

  for(i = 0; i < len-1; i++)
    {
      roo[i] = exp(-dg[i]);
      roh[i] = exp(-dgh[i]);
    }
  if (end_oligo & 0x1)
    {
      dgt =(((oligo[0]&0x5F) == 'A') || ((oligo[0]&0x5F) == 'T')) ? -cor*DGMF[INAT] : -cor*DGMF[INGC];
    }
  else if (at_penalty & 0x1)
    {
      dgt =(((oligo[0]&0x5F) == 'A') || ((oligo[0]&0x5F) == 'T')) ? -cor*DGMF[TP] : 0;
    }
  else dgt = 0;
  roo[0]= exp(-dg[0]-dgt);
  // no correction on hp

  if (end_oligo & 0x2)
    {
      dgt =(((oligo[len-1]&0x5F) == 'A') || ((oligo[len-1]&0x5F) == 'T')) ? -cor*DGMF[INAT] : -cor*DGMF[INGC];
    }
  else if (at_penalty & 0x2)
    {
      dgt =(((oligo[len-1]&0x5F) == 'A') || ((oligo[len-1]&0x5F) == 'T')) ? -cor*DGMF[TP] : 0;
    }
  else dgt = 0;
  roo[len-2]= exp(-dg[len-2]-dgt);
  // no correction on hp

  //elasticity models for the closing rates for the ds and ss DNA
  d = 0.56*1.12;
  bp = 1.5*1.125;
  d = 0.605;// # 0.567 #0.56*1.12 new 0.605
  bp = 1.828; // # 1.78 # 1.5*1.125 new 1.828
  ft = 4.1*(T0+Ti)/(T0+25);
  u = bp*F/ft;
  gss = d/bp*log(sinh(u)/u) ;
  gds = (F/ft - sqrt(F/ft/50) + 0.5*F*F/ft/1230)*0.34;

  //closing rates:only depend on the force
  rch = exp(-2*gss);
  rco = exp(-gss+gds);

  for (i = 0; i < len-1 && i < 12; i++)
    sprintf(stuff+strlen(stuff),"roo[%d] = %g; roh[%d] = %g;\n",i,roo[i],i, roh[i]);
  if (i < len ) sprintf(stuff+strlen(stuff),"...\nrco = %g; rch = %g;\n",rco, rch);
  else sprintf(stuff+strlen(stuff),"rco = %g; rch = %g;\n",rco, rch);

  win_printf("%s",stuff);


  if (new_plot)
    {
      if ((op = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create plot");
      dsf = op->dat[0];
      dsf->nx = dsf->ny = 0;
      dur_op = pr->n_op-1;
      set_plot_title(op, "\\stack{{%soligo special blocking %s}"
		     "{seq:%s}"
		     "{\\pt8 [Na+] = %g mM, [mg2+] = %g mM, T=%g^\\oc C; F = %g pN}{\\pt7 %s}}"
		     ,((afork)?"Fork/":""),((a3prime)?"5' and 3' end active":"5' end onlt"),oligo,Na,Mg,Ti,F,ener);
    }
  else
    {
      if ((dsf = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      dsf->nx = dsf->ny = 0;
      dur_op = pr->cur_op;
    }

  if (dur == 0)
    {
      if ((dso = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      dso->nx = dso->ny = 0;
      set_ds_source(dso, "Fork/->(oligo) blocking;  seq:%s; [Na+] = %g mM, [mg2+] = %g mM, T=%g C; F = %g pN"
                    ,oligo,Na,Mg,Ti,F);
      if ((dso2 = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      dso2->nx = dso2->ny = 0;
      set_ds_source(dso2, "Fork/->(oligo) blocking;  seq:%s; [Na+] = %g mM, [mg2+] = %g mM, T=%g C; F = %g pN"
                    ,oligo,Na,Mg,Ti,F);
    }


  //set_dot_line(ds);
  set_ds_source(dsf, "->(Fork)/oligo blocking;  seq:%s; [Na+] = %g mM, [mg2+] = %g mM, T=%g C; F = %g pN"
		,oligo,Na,Mg,Ti,F);

  if (gplo)
     {
      if ((opo = create_and_attach_one_plot(pr, len-1, len-1, 0)) == NULL)
	return win_printf_OK("cannot create plot");
      dsop = opo->dat[0];
       for (k = 0; k < dsop->nx; k++) dsop->yd[k] = 0.5+k;
      set_plot_title(opo, "\\stack{{%soligo special blocking %s}"
		     "{seq:%s}"
		     "{\\pt8 [Na+] = %g mM, [mg2+] = %g mM, T=%g^\\oc C; F = %g pN}}"
		     ,((afork)?"Fork/":""),((a3prime)?"5' and 3' end active":"5' end onlt"),oligo,Na,Mg,Ti,F);
                                                                                                                         set_plot_x_title(opo,"Probability of openning");
      set_plot_y_title(opo,"Position along oligo");
     }
  if (gpll)
     {
      if ((opl = create_and_attach_one_plot(pr, len, len, 0)) == NULL)
	return win_printf_OK("cannot create plot");
      dslb = opl->dat[0];
       for (k = 0; k < dslb->nx; k++) dslb->yd[k] = k;
      set_plot_title(opl, "\\stack{{%soligo special blocking %s}"
		     "{seq:%s}"
		     "{\\pt8 [Na+] = %g mM, [mg2+] = %g mM, T=%g^\\oc C; F = %g pN}}"
		     ,((afork)?"Fork/":""),((a3prime)?"5' and 3' end active":"5' end onlt"),oligo,Na,Mg,Ti,F);
                                                                                                                         set_plot_x_title(opl,"Nunber of exit events");
      set_plot_y_title(opl,"Base Nb where oligo detached");
     }

  float t = 0, t_1 = 0, dt = 0, den = 0, ttot = 0;
  int fork, fork_1 = 0, oli_open, oli_open_1 = 0, len_1 = len - 1;
  int oli2_open = len_1, oli2_open_1 = len_1;

  for (i = j = 0, fork = 0, oli_open = 0, oli2_open = len_1; i < nx; j++)
    {
      //fork = len - hp; // measure the nb of base closed in the hairpin
      //fork starts at 0 and increases it is always <= oli_open
      // oli2_open = len - oli2 ; oli2_open = len no breathing
      // 0 <= fork <= oli_open < oli2_open <= len
      // detaching when oli_open == oli2_open


      if (oli_open < oli2_open)  // we are still attached
	{      // we try to close oligo
          if (oli_open < 0 || oli_open >= len_1)
                  win_printf("oli_open out %d",oli_open);
          if (oli2_open < 1 || oli2_open > len_1)
                  win_printf("oli2_open out %d",oli2_open);
          if (oli_open < fork)
                  win_printf("oli_open %d < fork %d",oli_open,fork);
          if (fork < 0 || fork > len_1)
                  win_printf("fork %d out of range [0,%d]",fork,len_1);

          // closing oligo 3' end
	  thres[0] = (oli2_open >= len_1 || a3prime == 0) ? 0 : rco;
          // openning oligo 3' end
          thres[1] = thres[0] + ((oli2_open == oli_open || a3prime == 0) ? 0 : roo[oli2_open - 1]);
          // closing oligo 5' end
          thres[2] = thres[1] + ((oli_open == 0 || oli_open <= fork) ? 0 : rco);
          // openning oligo 5' end
          thres[3] = thres[2] + roo[oli_open];
          // openning fork only if not fully open
          thres[4] = thres[3] + ((fork == 0 || afork == 0) ? 0 : roh[fork-1]);
          // closing fork only if not blocked by oligo
          thres[5] = thres[4] + ((oli_open <= fork || afork == 0) ? 0 : rch);
          den = thres[5];


          tmp = ran1(&idum);
          dt = -log(tmp)/den;
          t += dt;
          ttot += dt;
          tmp = den*ran1(&idum);
          if (disp != WIN_CANCEL)
            {
              disp = win_printf("Fork %d, Oli 5' %d, oli3' %d\n"
                                "tmp = %g\n"
                                "Oligo 3'end %c ++ if in [0,%g]\n"
                                "Oligo 3'end %c -- if in [%g,%g]\n"
                                "Oligo 5'end %c -- if in [%g,%g]\n"
                                "Oligo 5'end %c ++ if in [%g,%g]\n"
                                "    Fork    %c -- if in [%g,%g]\n"
                                "    Fork    %c ++ if in [%g,%g]\n"
                                ,fork,oli_open,oli2_open,tmp
                                ,(tmp < thres[0])? 'y':'n',thres[0]
                                ,(tmp >= thres[0] && tmp < thres[1])? 'y':'n',thres[0],thres[1]
                                ,(tmp >= thres[1] && tmp < thres[2])? 'y':'n',thres[1],thres[2]
                                ,(tmp >= thres[2] && tmp < thres[3])? 'y':'n',thres[2],thres[3]
                                ,(tmp >= thres[3] && tmp < thres[4])? 'y':'n',thres[3],thres[4]
                                ,(tmp >= thres[4] && tmp < thres[5])? 'y':'n',thres[4],thres[5]);
            }
          if (tmp < thres[0]) oli2_open++;
          if (tmp >= thres[0] && tmp < thres[1]) oli2_open--;
          if (tmp >= thres[1] && tmp < thres[2]) oli_open--;
          if (tmp >= thres[2] && tmp < thres[3]) oli_open++;
          if (tmp >= thres[3] && tmp < thres[4]) fork--;
          if (tmp >= thres[4] && tmp < thres[5]) fork++;

	  for(k = 0; dsop != NULL && k < oli_open_1 && k < len_1; k++)
            {
                if (k >= 0 && k < dsop->nx)       dsop->xd[k] += dt;
                else win_printf("K out of range %d",k);
            }
	  for(k = oli2_open_1; dsop != NULL && k < len_1; k++)
            {
                if (k >= 0 && k < dsop->nx)       dsop->xd[k] += dt;
                else win_printf("K out of range %d",k);
            }


	  if (dur == 0)
	    {
	      add_new_point_to_ds(dso, t_1, oli_open_1);
	      add_new_point_to_ds(dso2, t_1, oli2_open_1);
	      add_new_point_to_ds(dsf, t_1, fork_1);
	      add_new_point_to_ds(dso, t, oli_open_1);
	      add_new_point_to_ds(dso2, t, oli2_open_1);
	      add_new_point_to_ds(dsf, t, fork_1);
	      oli_open_1 = oli_open;
	      oli2_open_1 = oli2_open;
	      fork_1 = fork;
              t_1 = t;
	      add_new_point_to_ds(dso, t, oli_open);
	      add_new_point_to_ds(dso2, t, oli2_open);
	      add_new_point_to_ds(dsf, t, fork);
	      i++;
	    }
          else
            {
	      oli_open_1 = oli_open;
	      oli2_open_1 = oli2_open;
	      fork_1 = fork;
              t_1 = t;
            }
	  //if (j%65536 == 0)
          //    my_set_window_title("j %d pos %d %d %d", j,fork,oli_open,oli2_open);
	}
      else
	{
          if (dslb != NULL)
            {
               dslb->xd[oli_open] += 1;
            }
	  if (dur == 0)
	    {
	      add_new_point_to_ds(dso, t, oli_open);
	      add_new_point_to_ds(dso2, t, oli2_open);
	      add_new_point_to_ds(dsf, t, fork);
	      i++;
	      oli_open_1 = oli_open = 0;
	      oli2_open_1 = oli2_open = len_1;
	      fork_1 = fork = 0;
	      add_new_point_to_ds(dso, t, oli_open);
	      add_new_point_to_ds(dsf, t, fork);
	    }
	  if (dur == 1)
	    {
	      oli_open = 0;
	      fork = 0;
              oli2_open = len_1;
	      my_set_window_title("Event %d duration %f %3.1f%%", i,t,(float)(100*i)/nx);
	      add_new_point_to_ds(dsf, i++, t);
              for (k = 0, tmp = 0; k < dsf->nx; k++) tmp += dsf->yd[k];
              if (dsf->nx > 0) tmp/=dsf->nx;
              set_plot_title(op, "\\stack{{%soligo special blocking %s}"
		     "{seq:%s}"
		     "{\\pt8 [Na+] = %g mM, [mg2+] = %g mM, T=%g^\\oc C; F = %g pN}{\\pt7 %s}{mean time %g}}"
                             ,((afork)?"Fork/":""),((a3prime)?"5' and 3' end active":"5' end only"),oligo,Na,Mg,Ti,F,ener,tmp);

              op->need_to_refresh = 1;
              refresh_plot(pr,dur_op);
	      t = 0;
	    }
	}
    }
  for (k = 0; dsop != NULL && ttot > 0 && k < dsop->nx; k++) dsop->xd[k] /= ttot;
  return refresh_plot(pr,pr->n_op-1);
}




// simulate Simona Cocco oligo
int 	do_apex_oligo_2sides(void)
{
  int i, j, k; // , i_1, io_1, io
  static int nx = 16384, new_plot = 1, end_oligo = 0;
  pltreg *pr;
  O_p *op = NULL;
  d_s  *dso = NULL, *dso2 = NULL, *dsb = NULL, *dsb1 = NULL, *dsb2 = NULL, *dsd = NULL;
  static int idum = 4587, dur = 0, no_salt_correction = 0, at_penalty = 3;
  static char oligo[64] = {0};
  static float Na = 150, Mg = 10, Ti = 25, F = 8.6, T0 = 273.15;
  float dg[64] = {0};
  float roo[64] = {0};
  float roo2[64] = {0};
  float roo_e[64] = {0}; // transition rate when only one or two bases remains
  float roo2_e[64] = {0};  // transition rate when only one or two bases remains
  float thres[10] = {0};
  float R = 1.9872;
  float cor = 1000.0/(R*(T0+Ti)); // cal/K-mol
  float tmp;
  int len, index; //, hp, ol;
  float d, bp, ft, u, gss, gds, rco, dgt, S;
  char stuff[2048] = {0};
  //int disp = WIN_OK;


  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return 1;
  i = win_scanf("Generate a oligo unbinding signal with opening on both sides\n"
		"Specify the oligo sequence %s\n"
		"Oligo ends correction:\n"
		"%R->no %r->on 5' %r->on 3'only %r->on both 5' and 3' %r->on last two nbases\n"
		"else Oligo AT/GC penalty at the ends:\n"
		"%R->no %r->on 5' %r->on 3'only %r->on both 5' and 3'\n"
		"%b->No salt entropy correction \n"
		"[Na+] = %4f mM; [Mg2+] = %4f mM; T = %4f \\oc C\n"
		"Force = %4f pN\n"
		"%R in this plot %r in a new plot\n"
		"Total number of steps %8d\n"
		"Random seed %8d\n"
		"%R real time or %r duration \n"
		,oligo,&end_oligo,&at_penalty,&no_salt_correction,&Na,&Mg,&Ti,&F
		,&new_plot,&nx,&idum,&dur);
  if (i == WIN_CANCEL)	return D_O_K;
  cor = 1000.0/(R*(T0+Ti)); // convert cal/K-mol in kBT
  init_Mfold31_array(Na, Mg, Ti, 1, no_salt_correction, dsCharge);

  len = strlen(oligo);

  for (i = 1; i < len; i++)
    {
      index = 0;
      if ((oligo[i-1]&0x5F) == 'A')  index = 0;
      if ((oligo[i-1]&0x5F) == 'C')  index = 4;
      if ((oligo[i-1]&0x5F) == 'G')  index = 8;
      if ((oligo[i-1]&0x5F) == 'T')  index = 12;
      if ((oligo[i]&0x5F) == 'A')  index += 0;
      if ((oligo[i]&0x5F) == 'C')  index += 1;
      if ((oligo[i]&0x5F) == 'G')  index += 2;
      if ((oligo[i]&0x5F) == 'T')  index += 3;
      dg[i-1] = -cor*DGMF[index];
      sprintf(stuff+strlen(stuff),"DeltaG %c%c %g Kcal/mol %g kBT\n",oligo[i-1],oligo[i],-DGMF[index],dg[i-1]);
    }

  for(i = 0; i < len-1; i++)
    {
      roo[i] = roo_e[i] = exp(-dg[i]);
      roo2[i] = roo2_e[i] = exp(-dg[i]);
    }

  if (end_oligo & 0x1)
    {
      dgt =(((oligo[0]&0x5F) == 'A') || ((oligo[0]&0x5F) == 'T')) ? -cor*DGMF[INAT] : -cor*DGMF[INGC];
    }
  else if (at_penalty & 0x1)
    {
      dgt =(((oligo[0]&0x5F) == 'A') || ((oligo[0]&0x5F) == 'T')) ? -cor*DGMF[TP] : 0;
    }
  else dgt = 0;
  roo[0]= exp(-dg[0]-dgt);
  roo2[0] =  exp(-dg[0]-dgt);
  // no correction on hp

  if (end_oligo & 0x2)
    {
      dgt =(((oligo[len-1]&0x5F) == 'A') || ((oligo[len-1]&0x5F) == 'T')) ? -cor*DGMF[INAT] : -cor*DGMF[INGC];
    }
  else if (at_penalty & 0x2)
    {
      dgt =(((oligo[len-1]&0x5F) == 'A') || ((oligo[len-1]&0x5F) == 'T')) ? -cor*DGMF[TP] : 0;
    }
  else dgt = 0;
  roo[len-2]= exp(-dg[len-2]-dgt);
  roo2[len-2] =  exp(-dg[len-2]-dgt);


  for(i = 0; i < len-1; i++)
    {
      dgt =(((oligo[i]&0x5F) == 'A') || ((oligo[i]&0x5F) == 'T')) ? -cor*DGMF[INAT] : -cor*DGMF[INGC];
      roo_e[i] = exp(-dg[i]-dgt);
      roo2_e[i] = exp(-dg[i]-dgt);
    }



  //elasticity models for the closing rates for the ds and ss DNA
  d = 0.56*1.12;
  bp = 1.5*1.125;
  d = 0.605;// # 0.567 #0.56*1.12 new 0.605
  bp = 1.828; // # 1.78 # 1.5*1.125 new 1.828
  d = 0.542; // nm
  bp = 2.14; // nm
  S = 216;  // pN
  ft = 4.1*(T0+Ti)/(T0+25);
  u = bp*F/ft;

  gss = d/bp*log(sinh(u)/u)+ 0.5*F*F/(ft*S);
// was // gss = d/bp*log(sinh(u)/u) ;
  gds = (F/ft - sqrt(F/ft/50) + 0.5*F*F/ft/1230)*0.34;


  //closing rates:only depend on the force
  rco = exp(-gss+gds);

  for (i = 0; i < len-1 && i < 12; i++)
          sprintf(stuff+strlen(stuff),"roo[%d] = %g; roo2[%d] = %g\n",i,roo[i],i,roo2[i]);
  if (i < len ) sprintf(stuff+strlen(stuff),"...\nrco = %g;\n",rco);
  else sprintf(stuff+strlen(stuff),"rco = %g;\n",rco);

  win_printf("%s",stuff);


  if (new_plot)
    {
      if ((op = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create plot");
      dso = op->dat[0];
      dso->nx = dso->ny = 0;
      if (dur == 0)
	{
	  if ((dso2 = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	    return win_printf_OK("cannot create data set");
	  dso2->nx = dso2->ny = 0;
	}

      set_plot_title(op, "\\stack{{Fork/oligo blocking}"
		     "{seq:%s}"
		     "{\\pt8 [Na+] = %g mM, [mg2+] = %g mM, T=%g^\\oc C; F = %g pN}}"
		     ,oligo,Na,Mg,Ti,F);
    }
  else
    {
      if ((dso = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      dso->nx = dso->ny = 0;
      if (dur == 0)
	{
	  if ((dso2 = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	    return win_printf_OK("cannot create data set");
	  dso2->nx = dso2->ny = 0;
	}
    }
  if (dur == 0)
    {
       if ((dsb = create_and_attach_one_ds(op, len-1, len-1, 0)) == NULL)
         return win_printf_OK("cannot create data set");
       for (k = 0; k < dsb->nx; k++) dsb->yd[k] = 0.5+k;
       if ((dsb1 = create_and_attach_one_ds(op, len, len, 0)) == NULL)
          return win_printf_OK("cannot create data set");
       for (k = 0; k < len; k++) dsb1->yd[k] = k;
       if ((dsb2 = create_and_attach_one_ds(op, len, len, 0)) == NULL)
          return win_printf_OK("cannot create data set");
       for (k = 0; k < len; k++) dsb2->yd[k] = k;
    }

  if ((dsd = create_and_attach_one_ds(op, len, len, 0)) == NULL)
          return win_printf_OK("cannot create data set");
  for (k = 0; k < dsd->nx; k++) dsd->yd[k] = k;

  /*
  if ((opt = create_and_attach_one_plot(pr, 16, 16, 0)) == NULL)
    return win_printf_OK("cannot create plot");
  dst = opt->dat[0];
  dst->nx = dst->ny = 0;
  */
  //set_dot_line(ds);
  set_ds_source(dso, "Fork/->oligo blocking;  seq:%s; [Na+] = %g mM, [mg2+] = %g mM,"
		" T=%g C; F = %g pN" ,oligo,Na,Mg,Ti,F);

  float t = 0, t_1 = 0, dt = 0, den = 0, ttot = 0;
  int len_1 = len - 1;
  int  oli_open = 0, oli_open_1 = 0, oli2_open = len_1, oli2_open_1 = len_1, bp_remain = 0;
  for (i = 0, j = 0, oli_open = oli_open_1 = 0, oli2_open = oli2_open_1 = len_1; i < nx; j++)
    {
      // 0 <= fork <= oli_open < oli2_open <= len
      // oli2_open = len - oli2 ; oli2_open = len -1 no breathing
      // detaching when ol2_open - oli1_open >= 1
      bp_remain = oli2_open - oli_open;
      if (oli_open < oli2_open)  // we are still attached
	{      // we try to close oligo
          if (oli_open < 0 || oli_open >= len_1)
                  win_printf("oli_open out %d",oli_open);
          if (oli2_open < 1 || oli2_open > len_1)
                  win_printf("oli2_open out %d",oli2_open);
          //sprintf(stuff, "oli = %d && oli2 = %d,\n",oli_open,oli2_open);
	  if (oli_open == 0 && oli2_open == len_1)
	    {  // we can only open the oligo
                    //sprintf(stuff+strlen(stuff), "we can only open the oligo on both sides\n");
	      den = thres[0] = (end_oligo == 4 && bp_remain < 3) ? roo_e[oli_open] : roo[oli_open];
	      den +=  (end_oligo == 4 && bp_remain < 3) ? roo2_e[oli2_open - 1] : roo2[oli2_open - 1];
	      tmp = ran1(&idum);
	      dt = -log(tmp)/den;
	      t += dt;
	      ttot += dt;
	      tmp = den*ran1(&idum);
	      if (tmp < thres[0]) oli_open++;
	      else oli2_open--;
	    }
	  else if (oli_open == 0)
	    {  // we can open oligo but close it on side 2
                    //sprintf(stuff+strlen(stuff), " we can open oligo on both sides but close it only on side 2\n");
	      thres[0] = rco;
	      den = thres[1] = thres[0] + (end_oligo == 4 && bp_remain < 3) ? roo_e[oli_open] : roo[oli_open];
	      den += (end_oligo == 4 && bp_remain < 3) ? roo2_e[oli2_open - 1] : roo2[oli2_open - 1];
	      tmp = ran1(&idum);
	      dt = -log(tmp)/den;
	      t += dt;
	      ttot += dt;
	      tmp = den*ran1(&idum);
	      if (tmp < thres[0])  oli2_open++;
	      else if (tmp >= thres[0] && tmp < thres[1]) oli_open++;
	      else oli2_open--;
	    }
	  else if (oli2_open == len_1)
	    {  // we can open oligo but close it on side 1
                    //sprintf(stuff+strlen(stuff), "we can open oligo  on both sides but close it only on side 1\n");
	      thres[0] = rco;
	      den = thres[1] = thres[0] + (end_oligo == 4 && bp_remain < 3) ? roo_e[oli_open] : roo[oli_open];
	      den += (end_oligo == 4 && bp_remain < 3) ? roo2_e[oli2_open - 1] : roo2[oli2_open - 1];
	      tmp = ran1(&idum);
	      dt = -log(tmp)/den;
	      t += dt;
	      ttot += dt;
	      tmp = den*ran1(&idum);
	      if (tmp < thres[0])  oli_open--;
	      else if (tmp >= thres[0] && tmp < thres[1]) oli_open++;
	      else oli2_open--;
	    }
	  else if (oli2_open - oli_open > 0)
	    {  // we can open and close oligo on both sides
                    //sprintf(stuff+strlen(stuff), "we can open and close oligo on both sides\n");
	      thres[0] = rco;
	      thres[2] = thres[1] = thres[0] + rco;
	      den = thres[2] += (end_oligo == 4 && bp_remain < 3) ? roo_e[oli_open] : roo[oli_open];
	      den += (end_oligo == 4 && bp_remain < 3) ? roo2_e[oli2_open - 1] : roo2[oli2_open - 1];
	      tmp = ran1(&idum);
	      dt = -log(tmp)/den;
	      t += dt;
	      ttot += dt;
	      tmp = den*ran1(&idum);
	      if (tmp < thres[0])  oli_open--;
	      else if (tmp >= thres[0] && tmp < thres[1])  oli2_open++;
	      else if (tmp >= thres[1] && tmp < thres[2]) oli_open++;
	      else oli2_open--;
	    }
	  else
	    {  // we can open and close oligo on both sides
                    //sprintf(stuff+strlen(stuff), "there is only one base still bound\n");
	      thres[0] = rco;
	      den = thres[1] = thres[0] + rco;
	      den += (end_oligo == 4 && bp_remain < 3) ? roo_e[oli_open] : roo[oli_open];
	      tmp = ran1(&idum);
	      dt = -log(tmp)/den;
	      t += dt;
	      ttot += dt;
	      tmp = den*ran1(&idum);
	      if (tmp < thres[0])  oli_open--;
	      else if (tmp >= thres[0] && tmp < thres[1])   oli2_open++;
	      else  oli_open++;
	    }
          //sprintf(stuff+strlen(stuff),"oli = %d && oli2 = %d",oli_open,oli2_open);
          //win_printf(stuff);
	  for(k = 0; k < oli_open_1 && k < len_1; k++)
            {
                if (k >= 0 && k < dsb->nx)       dsb->xd[k] += dt;
                else win_printf("K out of range %d",k);
            }
	  for(k = oli2_open_1; k < len_1; k++)
            {
                if (k >= 0 && k < dsb->nx)       dsb->xd[k] += dt;
                else win_printf("K out of range %d",k);
            }

	  if (dur == 0)
	    {
	      add_new_point_to_ds(dso, t_1, oli_open_1);
	      add_new_point_to_ds(dso2, t_1, oli2_open_1);
              if (dsb1 != NULL) dsb1->xd[oli_open_1] += dt;
              if (dsb2 != NULL) dsb2->xd[oli2_open_1] += dt;

	      add_new_point_to_ds(dso, t, oli_open_1);
	      add_new_point_to_ds(dso2, t, oli2_open_1);
	      oli_open_1 = oli_open;
	      oli2_open_1 = oli2_open;
              t_1 = t;
	      add_new_point_to_ds(dso, t, oli_open);
	      add_new_point_to_ds(dso2, t, oli2_open);
	      i++;
	    }
	  if (j%65536 == 0)
	    my_set_window_title("j %d pos %d %d", j,oli_open,oli2_open);
	}
      else
	{
          if (dsd != NULL)
            {
               dsd->xd[oli_open] += 1;
            }
	  if (dur == 0)
	    {
	      add_new_point_to_ds(dso, t, oli_open);
	      add_new_point_to_ds(dso2, t, oli2_open);
	      i++;
	      oli_open_1 = oli_open = 0;
	      oli2_open_1 = oli2_open = len_1;
	      add_new_point_to_ds(dso, t, oli_open);
	      add_new_point_to_ds(dso2, t, oli2_open);
	    }
	  if (dur == 1)
	    {
	      oli_open = 0;
	      oli2_open = len_1;
	      my_set_window_title("Event %d duration %f", i,t);
	      add_new_point_to_ds(dso, i++, t);
	      //add_new_point_to_ds(dso2, i++, t);
	      t = 0;
	    }
	}
    }
  for (k = 0; dsb != NULL && ttot > 0 && k < dsb->nx; k++) dsb->xd[k] /= ttot;
  for (k = 0; dsb1 != NULL && ttot > 0 && k < dsb1->nx; k++) dsb1->xd[k] /= ttot;
  for (k = 0; dsb2 != NULL && ttot > 0 && k < dsb2->nx; k++) dsb2->xd[k] /= ttot;
  return refresh_plot(pr,pr->n_op-1);
}



// simulate Simona Cocco oligo
int 	do_apex_oligo_1side(void)
{
  int i, j, k; // , i_1, io_1, io
  static int nx = 16384, new_plot = 1, end_oligo = 0;
  pltreg *pr;
  O_p *op = NULL;
  d_s  *dso = NULL, *dsb = NULL, *dsb1 = NULL;
  static int idum = 4587, dur = 0, no_salt_correction = 0, at_penalty = 3;
  static char oligo[64] = {0};
  static float Na = 150, Mg = 10, Ti = 25, F = 8.6, T0 = 273.15;
  float dg[64] = {0};
  float roo[64] = {0};
  float thres[10] = {0};
  //int n_thres = 0;
  float R = 1.9872;
  float cor = 1000.0/(R*(T0+Ti)); // cal/K-mol
  float tmp;
  int len, index; //, hp, ol;
  float d, bp, ft, u, gss, gds, rco, dgt;
  char stuff[2048] = {0};
  //int disp = WIN_OK;


  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return 1;
  if (strlen(oligo) == 0) sprintf(oligo,"ACAGCCAGCCGA");
  i = win_scanf("Generate a oligo unbinding signal with opening on 1 sides\n"
		"Specify the oligo sequence %s\n"
		"Oligo ends correction:\n"
		"%R->no %r->on 5' %r->on 3'only %r->on both 5' and 3'\n"
		"else Oligo AT/GC penalty at the ends:\n"
		"%R->no %r->on 5' %r->on 3'only %r->on both 5' and 3'\n"
		"%b->No salt entropy correction \n"
		"[Na+] = %4f mM; [Mg2+] = %4f mM; T = %4f \\oc C\n"
		"Force = %4f pN\n"
		"%R in this plot %r in a new plot\n"
		"Total number of steps %8d\n"
		"Random seed %8d\n"
		"%R real time or %r duration \n"
		,oligo,&end_oligo,&at_penalty,&no_salt_correction,&Na,&Mg,&Ti,&F
		,&new_plot,&nx,&idum,&dur);
  if (i == WIN_CANCEL)	return D_O_K;
  cor = 1000.0/(R*(T0+Ti)); // convert cal/K-mol in kBT
  init_Mfold31_array(Na, Mg, Ti, 0, no_salt_correction, dsCharge);

  len = strlen(oligo);

  for (i = 1; i < len; i++)
    {
      index = 0;
      if ((oligo[i-1]&0x5F) == 'A')  index = 0;
      if ((oligo[i-1]&0x5F) == 'C')  index = 4;
      if ((oligo[i-1]&0x5F) == 'G')  index = 8;
      if ((oligo[i-1]&0x5F) == 'T')  index = 12;
      if ((oligo[i]&0x5F) == 'A')  index += 0;
      if ((oligo[i]&0x5F) == 'C')  index += 1;
      if ((oligo[i]&0x5F) == 'G')  index += 2;
      if ((oligo[i]&0x5F) == 'T')  index += 3;
      dg[i-1] = -cor*DGMF[index];
      sprintf(stuff+strlen(stuff),"DeltaG %c%c %g Kcal/mol %g kBT\n",oligo[i-1],oligo[i],-DGMF[index],dg[i-1]);
    }

  for(i = 0; i < len-1; i++)
      roo[i] = exp(-dg[i]);


  if (end_oligo & 0x1)
    {
      dgt =(((oligo[0]&0x5F) == 'A') || ((oligo[0]&0x5F) == 'T')) ? -cor*DGMF[INAT] : -cor*DGMF[INGC];
    }
  else if (at_penalty & 0x1)
    {
      dgt =(((oligo[0]&0x5F) == 'A') || ((oligo[0]&0x5F) == 'T')) ? -cor*DGMF[TP] : 0;
    }
  else dgt = 0;
  roo[0]= exp(-dg[0]-dgt);
  // no correction on hp

  if (end_oligo & 0x2)
    {
      dgt =(((oligo[len-1]&0x5F) == 'A') || ((oligo[len-1]&0x5F) == 'T')) ? -cor*DGMF[INAT] : -cor*DGMF[INGC];
    }
  else if (at_penalty & 0x2)
    {
      dgt =(((oligo[len-1]&0x5F) == 'A') || ((oligo[len-1]&0x5F) == 'T')) ? -cor*DGMF[TP] : 0;
    }
  else dgt = 0;
  roo[len-2]= exp(-dg[len-2]-dgt);


  //elasticity models for the closing rates for the ds and ss DNA
  d = 0.56*1.12;
  bp = 1.5*1.125;
  d = 0.605;// # 0.567 #0.56*1.12 new 0.605
  bp = 1.828; // # 1.78 # 1.5*1.125 new 1.828
  ft = 4.1*(T0+Ti)/(T0+25);
  u = bp*F/ft;
  gss = d/bp*log(sinh(u)/u) ;
  gds = (F/ft - sqrt(F/ft/50) + 0.5*F*F/ft/1230)*0.34;


  //closing rates:only depend on the force
  rco = exp(-gss+gds);

  for (i = 0; i < len-1 && i < 12; i++)
          sprintf(stuff+strlen(stuff),"roo[%d] = %g;\n",i,roo[i]);
  if (i < len ) sprintf(stuff+strlen(stuff),"...\nrco = %g;\n",rco);
  else sprintf(stuff+strlen(stuff),"rco = %g;\n",rco);

  win_printf("%s",stuff);


  if (new_plot)
    {
      if ((op = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create plot");
      dso = op->dat[0];
      dso->nx = dso->ny = 0;

      set_plot_title(op, "\\stack{{Fork/oligo blocking}"
		     "{seq:%s}"
		     "{\\pt8 [Na+] = %g mM, [mg2+] = %g mM, T=%g^\\oc C; F = %g pN}}"
		     ,oligo,Na,Mg,Ti,F);
    }
  else
    {
      if ((dso = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      dso->nx = dso->ny = 0;
    }
  if (dur == 0)
    {
       if ((dsb = create_and_attach_one_ds(op, len-1, len-1, 0)) == NULL)
         return win_printf_OK("cannot create data set");
       for (k = 0; k < dsb->nx; k++) dsb->yd[k] = 0.5+k;
       if ((dsb1 = create_and_attach_one_ds(op, len, len, 0)) == NULL)
          return win_printf_OK("cannot create data set");
       for (k = 0; k < len; k++) dsb1->yd[k] = k;
    }

  //set_dot_line(ds);
  set_ds_source(dso, "Fork/->oligo blocking;  seq:%s; [Na+] = %g mM, [mg2+] = %g mM,"
		" T=%g C; F = %g pN" ,oligo,Na,Mg,Ti,F);

  float t = 0, t_1 = 0, dt = 0, den = 0, ttot = 0, len_1 = len - 1;
  int  oli_open = 0, oli_open_1 = 0;
  for (i = 0, j = 0, oli_open = oli_open_1 = 0; i < nx; j++)
    {
      // 0 <= fork <= oli_open < oli2_open <= len
      // oli2_open = len - oli2 ; oli2_open = len -1 no breathing
      // detaching when ol2_open - oli1_open >= 1
      if (oli_open < len_1)  // we are still attached
	{      // we try to close oligo
          if (oli_open < 0 || oli_open >= len_1)
                  win_printf("oli_open out %d",oli_open);
	  if (oli_open == 0)
	    {  // we can only open the oligo
	      thres[0] = roo[oli_open];
	      den = roo[oli_open];
	      tmp = ran1(&idum);
	      dt = -log(tmp)/den;
	      t += dt;
	      ttot += dt;
	      oli_open++;
	    }
	  else
	    {  // we can open and close oligo on both sides
	      thres[0] = rco;
	      den = thres[1] = thres[0] + roo[oli_open];
	      tmp = ran1(&idum);
	      dt = -log(tmp)/den;
	      t += dt;
	      ttot += dt;
	      tmp = den*ran1(&idum);
	      if (tmp < thres[0])  oli_open--;
	      else  oli_open++;
	    }
	  for(k = 0; k < oli_open_1 && k < len_1; k++)
            {
                if (k >= 0 && k < dsb->nx)       dsb->xd[k] += dt;
                else win_printf("K out of range %d",k);
            }

	  if (dur == 0)
	    {
	      add_new_point_to_ds(dso, t_1, oli_open_1);
              if (dsb1 != NULL) dsb1->xd[oli_open_1] += dt;
	      add_new_point_to_ds(dso, t, oli_open_1);
	      oli_open_1 = oli_open;
              t_1 = t;
	      add_new_point_to_ds(dso, t, oli_open);
	      i++;
	    }
	  if (j%65536 == 0)
	    my_set_window_title("j %d pos %d", j,oli_open);
	}
      else
	{
	  if (dur == 0)
	    {
	      add_new_point_to_ds(dso, t, oli_open);
	      i++;
	      oli_open_1 = oli_open = 0;
	      add_new_point_to_ds(dso, t, oli_open);
	    }
	  if (dur == 1)
	    {
	      oli_open = 0;
	      my_set_window_title("Event %d duration %f", i,t);
	      add_new_point_to_ds(dso, i++, t);
	      t = 0;
	    }
	}
    }
  for (k = 0; dsb != NULL && ttot > 0 && k < dsb->nx; k++) dsb->xd[k] /= ttot;
  for (k = 0; dsb1 != NULL && ttot > 0 && k < dsb1->nx; k++) dsb1->xd[k] /= ttot;
  return refresh_plot(pr,pr->n_op-1);
}




int 	do_apex_loop_closing(void)
{
  int i, j, k; // , i_1, io_1, io
  static int nx = 16384, new_plot = 1, end_oligo = 0;
  pltreg *pr;
  O_p *op = NULL;
  d_s  *dso = NULL, *dso2 = NULL, *dsb = NULL, *dsb1 = NULL;
  static int idum = 4587, dur = 0, loopsize = 12, verbose = 0, closing_only = 0, no_salt_correction = 0, at_penalty = 3;
  static char oligo[64] = {0};
  static float Na = 150, Mg = 10, Ti = 25, F = 8.6, T0 = 273.15, mdt = 3, ldt = 9, mulloop = 0.5;
  float dg[64] = {0};
  float roo[64] = {0};
  float thres[10] = {0};
  float rcl[64] = {0};
  float rch = 0, rchl = 0; //rate of loop closing
  //int n_thres = 0;
  float R = 1.9872;
  float cor = 1000.0/(R*(T0+Ti)); // cal/K-mol
  float tmp;
  int len, index, rf_plot_nb; //, hp, ol;
  float d, bp, ft, u, gss, gds, rco, dgt;
  char stuff[2048] = {0};
  //int disp = WIN_OK;


  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return 1;
  if (strlen(oligo) == 0) sprintf(oligo,"GAGTCCTGGATCCTG");
  i = win_scanf("Generate a hairpin loop closing signal with movment on 1 sides\n"
		"Specify the hairpin loop size in bases %4d\n"
                "and the sequence after the loop%s\n"
		"Oligo ends correction:\n"
		"%R->no %r->on 5' %r->on 3'only %r->on both 5' and 3'\n"
		"else Oligo AT/GC penalty at the ends:\n"
		"%R->no %r->on 5' %r->on 3'only %r->on both 5' and 3'\n"
		"%b->No salt entropy correction \n"
		"[Na+] = %4f mM; [Mg2+] = %4f mM; T = %4f \\oc C\n"
		"Force = %4f pN\n"
		"%R in this plot %r in a new plot\n"
		"Total number of steps %8d\n"
		"Random seed %8d\n"
		"%R real time or %r duration \n"
                "%R Closing and opening %r Closing only\n"
                "%b Display energiies\n"
                "Hybridation rate %6f 10^{-6}s\n"
                "Looping rate %6f 10^{-6}s\n"
                "Loop energy multiplier %6f\n"
		,&loopsize,oligo,&end_oligo,&at_penalty,&no_salt_correction,&Na,&Mg,&Ti,&F
		,&new_plot,&nx,&idum,&dur,&closing_only, &verbose,&mdt,&ldt,&mulloop);
  if (i == WIN_CANCEL)	return D_O_K;
  cor = 1000.0/(R*(T0+Ti)); // convert cal/K-mol in kBT
  init_Mfold31_array(Na, Mg, Ti, verbose, no_salt_correction, dsCharge);

  len = strlen(oligo);

  for (i = 1; i < len; i++)
    {
      index = 0;
      if ((oligo[i-1]&0x5F) == 'A')  index = 0;
      if ((oligo[i-1]&0x5F) == 'C')  index = 4;
      if ((oligo[i-1]&0x5F) == 'G')  index = 8;
      if ((oligo[i-1]&0x5F) == 'T')  index = 12;
      if ((oligo[i]&0x5F) == 'A')  index += 0;
      if ((oligo[i]&0x5F) == 'C')  index += 1;
      if ((oligo[i]&0x5F) == 'G')  index += 2;
      if ((oligo[i]&0x5F) == 'T')  index += 3;
      dg[i-1] = -cor*DGMF[index];
      sprintf(stuff+strlen(stuff),"DeltaG %c%c %g Kcal/mol %g kBT\n",oligo[i-1],oligo[i],-DGMF[index],dg[i-1]);
    }

  for(i = 0; i < len-1; i++)
      roo[i] = exp(-dg[i]);

  if (end_oligo & 0x1)
    {
      dgt =(((oligo[0]&0x5F) == 'A') || ((oligo[0]&0x5F) == 'T')) ? -cor*DGMF[INAT] : -cor*DGMF[INGC];
    }
  else if (at_penalty & 0x1)
    {
      dgt =(((oligo[0]&0x5F) == 'A') || ((oligo[0]&0x5F) == 'T')) ? -cor*DGMF[TP] : 0;
    }
  else dgt = 0;
  roo[0]= exp(-dg[0]-dgt);
  // no correction on hp

  if (end_oligo & 0x2)
    {
      dgt =(((oligo[len-1]&0x5F) == 'A') || ((oligo[len-1]&0x5F) == 'T')) ? -cor*DGMF[INAT] : -cor*DGMF[INGC];
    }
  else if (at_penalty & 0x2)
    {
      dgt =(((oligo[len-1]&0x5F) == 'A') || ((oligo[len-1]&0x5F) == 'T')) ? -cor*DGMF[TP] : 0;
    }
  else dgt = 0;
  roo[len-2]= exp(-dg[len-2]-dgt);


  //elasticity models for the closing rates for the ds and ss DNA
  d = 0.56*1.12;
  bp = 1.5*1.125;
  d = 0.605;// # 0.567 #0.56*1.12 new 0.605
  bp = 1.828; // # 1.78 # 1.5*1.125 new 1.828
  ft = 4.1*(T0+Ti)/(T0+25);
  u = bp*F/ft;
  gss = d/bp*log(sinh(u)/u) ;
  gds = (F/ft - sqrt(F/ft/50) + 0.5*F*F/ft/1230)*0.34;


  //closing rates:only depend on the force
  rco = exp(-gss+gds);
  rchl = exp(-gss*loopsize);
  rch = exp(-gss*2);

  // loop energy E = 2 *\pi^2 * Lp/ (n * \alpha ss)
  for (i = 0; i < len-1; i++)
          rcl[i] = exp(-((2 * mulloop * M_PI * M_PI * bp)/d) * (((float)1/(loopsize + i))  - ((float)1/(loopsize + i + 1))));

  if (verbose) win_printf("First loop closing rate %g k_BT",rcl[0]);
  //rcl = 1;

  for (i = 0; i < len-1 && i < 12; i++)
          sprintf(stuff+strlen(stuff),"roo[%d] = %g;\n",i,roo[i]);
  if (i < len ) sprintf(stuff+strlen(stuff),"...\nrco = %g;\n",rco);
  else sprintf(stuff+strlen(stuff),"rco = %g;\n",rco);

  if (verbose) win_printf("%s",stuff);


  if (new_plot)
    {
      if ((op = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create plot");
      dso = op->dat[0];
      dso->nx = dso->ny = 0;

      set_plot_title(op, "\\stack{{loop %d nts, blocking}"
		     "{seq:%s}"
		     "{\\pt8 [Na+] = %g mM, [mg2+] = %g mM, T=%g^\\oc C; F = %g pN}}"
		     ,loopsize,oligo,Na,Mg,Ti,F);
      if (dur == 0) create_attach_select_x_un_to_op(op, IS_SECOND, 0, mdt, -6, 0, "\\mu s");
      else create_attach_select_y_un_to_op(op, IS_SECOND, 0, mdt, -6, 0, "\\mu s");
      rf_plot_nb = pr->n_op-1;
    }
  else
    {
      if ((dso = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      dso->nx = dso->ny = 0;
      rf_plot_nb = pr->cur_op;
    }
  if (dur == 0)
    {
        if ((dso2 = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	    return win_printf_OK("cannot create data set");
        dso2->nx = dso2->ny = 0;
       if ((dsb = create_and_attach_one_ds(op, len-1, len-1, 0)) == NULL)
         return win_printf_OK("cannot create data set");
       for (k = 0; k < dsb->nx; k++) dsb->yd[k] = 0.5+k;
       if ((dsb1 = create_and_attach_one_ds(op, len, len, 0)) == NULL)
          return win_printf_OK("cannot create data set");
       for (k = 0; k < len; k++) dsb1->yd[k] = k;
    }

  //set_dot_line(ds);
  set_ds_source(dso, "Fork/->oligo blocking;  seq:%s; [Na+] = %g mM, [mg2+] = %g mM,"
		" T=%g C; F = %g pN" ,oligo,Na,Mg,Ti,F);

  float t = 0, t_1 = 0, dt = 0, den = 0, ttot = 0, len_1 = len - 1;
  int loop_open = 1, loop_open_1 = 1; // define the state of the hairpin
  int fork = 0, fork_1 = 0; // this is the hairpin fork position : fork = 1 :1 dinucleotide hybridized
  // fork = len_1 : the hairpin is closed
  int loop_fork = 0, loop_fork_1 = 0;
  // this is the loop fork position : loop_fork = 0 the stem is close up to the unpaired llop bases
  // loop_fork = 1 : the loop is opened by 1 bp
  int pos, fully_close = 0, fully_close_1 = 0;
  for (i = 0, j = 0, loop_open = 1, fork = fork_1 = 0, loop_fork = loop_fork_1 = 0; i < nx; j++)
    {
      // 0 <= fork <= oli_open < oli2_open <= len
      // oli2_open = len - oli2 ; oli2_open = len -1 no breathing
      // detaching when ol2_open - oli1_open >= 1
      //if (fork < len_1)  // we are still not closed
        if (loop_open == 1)
          {
             thres[0] = den = rchl;
             tmp = ran1(&idum);
             dt = -log(tmp)/den;
             dt *= ldt/mdt;
             t += dt;
             ttot += dt;
             loop_open = 0;
             fork = 1;
             loop_fork = 0;
          }
        else if ((loop_fork <= 0) && (fork == len_1))
          {  // we can only open the oligo
             thres[0] = roo[loop_fork];
             thres[1] = thres[0] + roo[fork-1];
             den = thres[1];
             tmp = ran1(&idum);
             dt = -log(tmp)/den;
             t += dt;
             ttot += dt;
             tmp = den*ran1(&idum);
             if (tmp < thres[0])  loop_fork++;
             else  fork--;
          }
        else if (loop_fork <= 0)
	  {  // we can only open the oligo
             thres[0] = roo[loop_fork];
             thres[1] = thres[0] + roo[fork-1];
             thres[2] = thres[1] + rch;
             den = thres[2];
             tmp = ran1(&idum);
             dt = -log(tmp)/den;
             t += dt;
             ttot += dt;
             tmp = den*ran1(&idum);
             if (tmp < thres[0])  loop_fork++;
             else if (tmp < thres[1])  fork--;
             else  fork++;
          }
        else if (fork == len_1)
	  {  // we cannot zip more
             thres[0] = roo[loop_fork];
             thres[1] = thres[0] + roo[fork-1];
             thres[2] = thres[1] + rcl[loop_fork];
             den = thres[2];
             tmp = ran1(&idum);
             dt = -log(tmp)/den;
             t += dt;
             ttot += dt;
             tmp = den*ran1(&idum);
             if (tmp < thres[0])  loop_fork++;
             else if (tmp < thres[1])  fork--;
             else loop_fork--;
          }
        else
	  {  // we can open and close oligo on both sides
             thres[0] = roo[loop_fork];
             thres[1] = thres[0] + roo[fork-1];
             thres[2] = thres[1] + rch;
             thres[3] = thres[2] + rcl[loop_fork];
             den = thres[3];
             tmp = ran1(&idum);
             dt = -log(tmp)/den;
             t += dt;
             ttot += dt;
             tmp = den*ran1(&idum);
             if (tmp < thres[0])  loop_fork++;
             else if (tmp < thres[1])  fork--;
             else  if (tmp < thres[1]) fork++;
             else loop_fork--;
          }

        for(k = 0; k < fork_1 && k < fork_1; k++)
          {
             if (k >= 0 && k < dsb->nx)       dsb->xd[k] += dt;
             else win_printf("K out of range %d",k);
          }
        if (dur == 0)
	  {
             pos = (loop_open_1) ? -loopsize : 0;
             add_new_point_to_ds(dso, t_1, pos + fork_1);
             add_new_point_to_ds(dso2, t_1, pos + loop_fork_1);
             if (dsb1 != NULL) dsb1->xd[fork_1] += dt;
             add_new_point_to_ds(dso, t, pos + fork_1);
             add_new_point_to_ds(dso2, t, pos + loop_fork_1);
             fork_1 = fork;
             loop_open_1 = loop_open;
             t_1 = t;
             pos = (loop_open) ? -loopsize : 0;
             add_new_point_to_ds(dso, t, pos + fork);
             add_new_point_to_ds(dso2, t, pos + loop_fork);
             i++;
          }

        if (loop_open == 0 && fork <= loop_fork)
          {
             fork = 0;
             loop_open = 1;
             loop_fork = 0;
             fork_1 = fork;
             loop_open_1 = loop_open;
             loop_fork_1 = loop_fork;
             if (dur == 0)
               {
                  pos = (loop_open) ? -loopsize : 0;
                  add_new_point_to_ds(dso, t, pos + fork);
                  add_new_point_to_ds(dso2, t, pos + loop_fork);
                  i++;
               }
          }

        if (j%65536 == 0)
                my_set_window_title("j %d pos %d", j,fork);
        fully_close = (fork == len_1) ? 1 : fully_close;
        fully_close = (loop_open == 1) ? 0 : fully_close;
        if (dur == 1 && fully_close_1 != fully_close)
           {
              float tc = 0, to = 0;
              int itc = 0, ito = 0;
              add_new_point_to_ds(dso, i++, (fully_close_1) ? t : -t);
	      t = 0;
              fully_close_1 = fully_close;
              for (k = 0, tc = to = 0, itc = ito = 0; k < dso->nx; k++)
                {
                   if (dso->yd[k] >= 0)
                      {
                         tc += dso->yd[k];
                         itc++;
                      }
                   else
                      {
                         to += -dso->yd[k];
                         ito++;
                      }
                }
              if (itc > 0) tc/=itc;
              if (ito > 0) to/=ito;
              tc = mdt*tc/1e6;
              to = mdt*to/1e6;
	      my_set_window_title("Event %d duration %f", i,t);
              set_plot_title(op, "\\stack{{loop %d nts blocking}"
		     "{\\pt7 stem seq:%s}"
                     "{\\pt7 Energy correction on 5' end %s, on 3' %s}"
		     "{\\pt8 [Na+] = %g mM, [mg2+] = %g mM, T=%g^\\oc C; F = %g pN}"
                             "{\\pt7<\\tau_{open}> = %g s, <\\tau_{close}> = %g s}"
                             "{\\pt7 dt_{loop} = %g, dt_{hyb.} = %g (x10^{-6}s); Loop E %g}}"
                             ,loopsize,oligo,(end_oligo & 0x1)?"yes":"no",(end_oligo & 0x2)?"yes":"no"
                             ,Na,Mg,Ti,F,to, tc,ldt,mdt,mulloop);
              if (i%10 == 1) refresh_plot(pr,rf_plot_nb);
           }
        if (closing_only && fully_close)
           {

             if (dur == 0)
                {
                  pos = (loop_open_1) ? -loopsize : 0;
                  add_new_point_to_ds(dso, t_1, pos + fork_1);
                  add_new_point_to_ds(dso2, t_1, pos + loop_fork_1);
                  if (dsb1 != NULL) dsb1->xd[fork_1] += dt;
                  add_new_point_to_ds(dso, t, pos + fork_1);
                  add_new_point_to_ds(dso2, t, pos + loop_fork_1);
                  fork_1 = fork = 0;
                  loop_open_1 = loop_open = 1;
                  loop_fork = 0;
                  fully_close = 0;
                  t_1 = t;
                  pos = (loop_open) ? -loopsize : 0;
                  add_new_point_to_ds(dso, t, pos + fork);
                  add_new_point_to_ds(dso2, t, pos + loop_fork);
                  i++;
                }
             else
                {
                  fork = 0;
                  loop_open = 1;
                  fully_close = 0;
                  loop_open = 1;
                  fork = 0;
                }
           }
    }
  for (k = 0; dsb != NULL && ttot > 0 && k < dsb->nx; k++) dsb->xd[k] /= ttot;
  for (k = 0; dsb1 != NULL && ttot > 0 && k < dsb1->nx; k++) dsb1->xd[k] /= ttot;
  return refresh_plot(pr,rf_plot_nb);
}





MENU *PolySeq_plot_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
  /*
  add_item_to_menu(mn,"Hello example", do_PolySeq_hello,NULL,0,NULL);
  add_item_to_menu(mn,"data set rescale in Y", do_PolySeq_rescale_data_set,NULL,0,NULL);
<  add_item_to_menu(mn,"plot rescale in Y", do_PolySeq_rescale_plot,NULL,0,NULL);
  */
  add_item_to_menu(mn,"polymerase signal", do_PolySeq_signal,NULL,0,NULL);
  add_item_to_menu(mn,"helicase pausing signal", do_helicase_pause_signal,NULL,0,NULL);
  add_item_to_menu(mn,"Unzipping signal",do_unzip_signal,NULL,0,NULL);
  add_item_to_menu(mn,"Hairpin/oligo blocking SC",do_unzip_signal_SC_1side_n,NULL,0,NULL);

  add_item_to_menu(mn,"Hairpin/oligo blocking SC 2 sides",do_unzip_signal_SC_2sides_n,NULL,0,NULL);
  add_item_to_menu(mn,"Hairpin/oligo blocking SC 2 sides sense/antisense",do_unzip_signal_SC_2sides_n_sense_antisense,NULL,0,NULL); 

  add_item_to_menu(mn,"Hairpin/oligo blocking SC 2 sides long HP",do_unzip_signal_SC_2sides_n_hp_extended,NULL,0,NULL);



  add_item_to_menu(mn,"Hairpin/oligo LNA blocking SC 2 sides",do_unzip_LNA_signal_SC_2sides_n,NULL,0,NULL);
  add_item_to_menu(mn,"Hairpin/oligo pure RNA blocking SC 2 sides",do_unzip_RNA_oligo_from_DNA_template_SC_2sides_n,NULL,0,NULL);





  add_item_to_menu(mn,"Hairpin/oligo special blocking SC 2 sides",do_unzip_manual_cor_signal_SC_2sides_n,NULL,0,NULL);




  add_item_to_menu(mn,"Oligo apex 1 side",do_apex_oligo_1side,NULL,0,NULL);
  add_item_to_menu(mn,"Oligo apex 2 side",do_apex_oligo_2sides,NULL,0,NULL);

  add_item_to_menu(mn,"loop apex",do_apex_loop_closing,NULL,0,NULL);
  add_item_to_menu(mn,"Unzip/unpeel HP",do_unzip_HP_signal,NULL,0,NULL);



  return mn;
}

int	PolySeq_main(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  add_plot_treat_menu_item ( "PolySeq", NULL, PolySeq_plot_menu(), 0, NULL);
  return D_O_K;
}

int	PolySeq_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  remove_item_to_menu(plot_treat_menu, "PolySeq", NULL, NULL);
  return D_O_K;
}
#endif
