typedef struct _tracking_data
{
    int image_num;
    long long image_t;                                  // the image absolute time
    unsigned long image_dt;                               // the time spent in the previous function call
    float image_zmag, image_rot_mag, image_obj_pos;     // the magnet position, the objective position
    float   *x;
    float   *y;
    float   *z;
    char    *n_l;           // not lost indicator
                                              
} tracking_data;


typedef struct _acquisition_plt
{
	int		n_bead;		/* number of data sets	*/
	int		n;			/* number of points in each data set	*/
	int		m;			/* size allocated for each data set */
	int		x_index;	/* current x_index */
	int		y_index;	/* current y_index */
	char	*source;	/* name of the file	*/
	char	**name;		/* names of data sets */
	char    **calib_image_name; /*calibration image file*/
	tracking_data	*data;		/* data	*/
	un_s	*x_unit;			/*  unit sets */
	un_s	*y_unit;			/*  unit sets */
	un_s	*z_unit;			/*  unit sets */
	un_s	*t_unit;			/*  unit sets */
	int     *n_x_unit, *m_x_unit, *c_x_unit;
	int     *n_y_unit, *m_y_unit, *c_y_unit;
	int     *n_z_unit, *m_z_unit, *c_z_unit;
	unsigned long time;		/* date of creation */
} acq_plt;
#define BUFFER_PACK_SIZE  64
#define SAVING_INTERVAL 100

