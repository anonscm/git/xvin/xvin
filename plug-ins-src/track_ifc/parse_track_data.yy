%{
#include <unitset.h>
#include <xvin.h>

#include <track_data.h>
int us_type = 0, us_sub_type = 0, axis_us = 0;
float axus = 0, dxus = 1;
char us_decade, mode_us, *us_name = NULL;
un_s	*un = NULL;
int count = 0, count_name = 0;
#define YY_DECL int yylex YY_PROTO(( acq_plt *acqdata,char *filename ))

%}
/*general definition*/

DIGIT    [0-9]
%option noyywrap

%x is_npoints
%x is_nbeads
%x acqplt_src
%x acqplt_name
%x calib_image_filename_number
%x calib_image_filename

%%
"-n points" {   
               count = 0;
               count_name = 0; 
               BEGIN(is_npoints);
            }
<is_npoints>{DIGIT}+$   {
                            acqdata->n = atoi(yytext);
                            //win_printf("acqdata->n %d",acqdata->n);
                            BEGIN(INITIAL);
                        }
"-n beads" {    
                BEGIN(is_nbeads);
            }
<is_nbeads>.+$   {    /*win_printf("yytex %s",yytext);*/
                            acqdata->n_bead = atoi(yytext);
                            /*win_printf("acqdata->nbead %d",acqdata->n_bead);*/
                            BEGIN(INITIAL);
                        }

"-calib_image_filename "    {
								BEGIN(calib_image_filename_number);
							}
<calib_image_filename_number>{DIGIT}+      {
                                                count++;
                                                BEGIN(calib_image_filename);
                                            }


<calib_image_filename_number>.+$    {
                                           
                                        acqdata->calib_image_name = (char **) realloc( acqdata->calib_image_name , acqdata->n_bead * sizeof(char *));
                                        acqdata->calib_image_name[count] = (char *)calloc(strlen(yytext)+1,sizeof(char));
                                        if (acqdata->calib_image_name[count] == NULL) return 1;
                                        BEGIN(INITIAL);
                                     }  

"-src \"" BEGIN(acqplt_src);
<acqplt_src>[^"\"$"]+        {
                            
                            
                            acqdata->source = (char *)calloc(strlen(yytext)+1,sizeof(char));
                            if (acqdata->source == NULL) return 1;
                            strcpy(acqdata->source,yytext);
/*                          win_printf("source %d\n %d %d  ",strcpy(ds->source,yytext),number_ds,strlen(yytext));*/
                            BEGIN(INITIAL);
                          }

    	
"-name \""                    {     
                                    count_name++;
                                    BEGIN(acqplt_name);
                              }
<acqplt_name>[^"\"$"]+        { 
                                
                                acqdata->name = (char **) realloc(acqdata->name,count_name*sizeof(char *)); 
                                if (acqdata->name == NULL) return 1;  
                                
                                acqdata->name[count_name-1] = (char *)calloc(strlen(yytext)+1,sizeof(char));
                                if (acqdata->name[count_name-1] == NULL) return 1;
                                
                                strcpy(acqdata->name[count_name-1],yytext);
                                /*win_printf("%s\n",acqdata->name[count_name-1]);*/
                                BEGIN(INITIAL);
                              }

  




%%

int parse_track_data_main( argc, argv )
int argc;
char **argv;
{
    acq_plt *acqdata = NULL;

    acqdata = (acq_plt *)calloc(1,sizeof(acq_plt));

    ++argv, --argc;  /* skip over program name */
    if ( argc > 0 )
            yyin = fopen( argv[0], "r" );
    else
            {   win_printf("Et alors que fais tu?");
                return D_O_K;
            }
    
    yylex(acqdata,argv[0]);
    


    return 0;
}
