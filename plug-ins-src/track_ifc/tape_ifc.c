/*    Plug_in program for IFC image treatement in Xvin.
*
*    
*      JF Allemand
*/
#ifndef _TAPE_IFC_C
#define _TAPE_IFC_C


# include "allegro.h"
# include "winalleg.h"
# include "ifcapi.h"
# include <windowsx.h>
# include "ctype.h"
//# include "i_file_mn.h"
# include "xvin.h"
# include "track_ifc.h"

# define BUILDING_PLUGINS_DLL
# include "tape_ifc.h"

extern int num_image_IFC;
extern int do_load_movie_on_disk(void);
extern IFC_disp disp_IFC;
extern pCICamera cam;
extern CAM_ATTR attr;
extern HIFCGRAB grabID;
extern IFC_GRAB_STATS stats;

int num_frames_in_movie = 1024;
O_i *oi_movie = NULL;
O_i *oi_small_movie = NULL;
int num_pts =128;
int num_frames_average = 8;
int present_image_number = 0;

//op->op_idle_action = average_image_idle_action;
//int (*oi_idle_action)(struct one_image *oi, DIALOG *d); 



int average_image_idle_action(O_i *oi, DIALOG *d)
{
    register int k = 0, j = 0, i = 0;
    int number_of_images_to_average = 8;
    float pix_val = 0;
    
    draw_bubble(screen,0,550,200,"oi->im.n_f %d ",present_image_number);
    
    if (present_image_number > number_of_images_to_average)
    {
    /*detecter le type d'image*/
        for ( j = 0; j < oi->im.ny; j++)
        {
	        //draw_bubble(screen,0,450,200,"j %d ",j);
		    for ( k = 0; k < oi->im.nx; k++)
		    {
                pix_val = 0;
                if (oi->im.data_type == IS_CHAR_IMAGE)
                
                   {
                                 for (i = 0; i < number_of_images_to_average; i++)
                                 {
            
                                     pix_val +=  oi_buffer_tape_IFC->im.pxl[(present_image_number - i)%nframes_in_IFC_buffer][j].ch[k];
                                 }
                                 oi->im.pixel[j].ch[k] = (unsigned char)(pix_val/number_of_images_to_average);
                                 //if (j%50)draw_bubble(screen,0,450,200,"pix %d ",oi->im.pixel[j].ch[k]);
                   }                  
                   if (oi->im.data_type == IS_UINT_IMAGE) 
                   
                
                   {
                                 for (i = 0; i < number_of_images_to_average; i++)
                                 {
            
                                     pix_val += (int ) oi_buffer_tape_IFC->im.pxl[(present_image_number - i)%nframes_in_IFC_buffer][j].in[k];
                                 }
                                 oi->im.pixel[j].in[k] = (int)(pix_val/number_of_images_to_average);
              }
            // MODIFY IF NEEDED if (oi->im.data_type == IS_RGB_PICTURE) oi->im.pixel[j].ch[k] = (unsigned char)((num_frames_average *oi->im.pixel[j].ch[k]) - oi_buffer_tape_IFC->im.pxl[(present_image_number - num_frames_average -1)%nframes_in_IFC_buffer][j].ch[k]+ oi_buffer_tape_IFC->im.pxl[present_image_number%nframes_in_IFC_buffer][j].ch[k])/number_of_images_to_average);



  		}
    }
    }
  //imr->one_i->need_to_refresh |= ALL_NEED_REFRESH;
  oi->need_to_refresh  |= ALL_NEED_REFRESH;
  return d->proc(MSG_DRAW,d,0);
  //find_dialog_focus(active_dialog);  
  //broadcast_dialog_message(MSG_DRAW,0);
  //return D_REDRAWME;               
  //return REDRAW_ME;/*refresh_plot(pr_IFC, UNCHANGED);*/
}

int average_real_time(void)
{   
    O_i *oi = NULL;
    extern int num_image_IFC;
    imreg *imr;

    if(updating_menu_state != 0)	return D_O_K;

    
    imr = find_imr_in_current_dialog(NULL);
    if (imr == NULL)	return FALSE;
    
    if (attr.dwBytesPerPixel == 1)
	{
    	oi = create_and_attach_oi_to_imr (imr, attr.dwWidth, attr.dwHeight, IS_CHAR_IMAGE);
    }
	else if (attr.dwBytesPerPixel == 2)
    {
    		oi = create_and_attach_oi_to_imr (imr, attr.dwWidth, attr.dwHeight, IS_UINT_IMAGE);
    }
    else if (attr.dwBytesPerPixel == 3)
    {
    	oi = create_and_attach_oi_to_imr (imr, attr.dwWidth, attr.dwHeight, IS_RGB_PICTURE);
  	}
    
    if (oi == NULL) return FALSE;
    set_background_transfer_image_data();
    //disp_IFC.to_do = transfer_data;//We fill the data in real time from the interrupt function
 	//disp_IFC.param = NULL; 
    select_image_of_imreg_and_display(imr,imr->n_oi - 1);
    oi->oi_idle_action = average_image_idle_action;//we set the averaging and visualisation function ON as a background task
    return 0;   
}

int wait_n_millisecond(unsigned long time)
{
	DWORD time_in;
 	
  	time_in  = CIFCOS_GetSystimeMillisecs();
  	while ((CIFCOS_GetSystimeMillisecs()-time_in) < time);
   	return 0;
	
}


int play_movie_real_time(void)
{
	O_i *oi = NULL;
	imreg *imr = NULL;
	
	
	if(updating_menu_state != 0)	return D_O_K;
 	
 	do_load_movie_on_disk();
  	broadcast_dialog_message(MSG_DRAW,0);
	imr = find_imr_in_current_dialog(NULL);//si squeeze raccourci! Il faudra alors passer aussi imr en parametre
	if (imr == NULL)	return FALSE;
	
	select_image_of_imreg_and_display(imr,imr->n_oi - 1);
	oi = imr->one_i ;
	
	tape_play_simulation(imr,oi);
	return 0;
  	
}
			
int find_present_automatic_filenumber(char *file)
{
	int file_number,nchar;
	
	nchar= strlen(file);
	file_number = atoi(file+(nchar-7));
	
	return file_number;
}

char *find_present_automatic_string_filename(char *file)
{	char *common_name=NULL;
	int nchar = 0;
	
	nchar= strlen(file);
	common_name = (char *) calloc(nchar-6,sizeof(char));
	common_name = strncpy(common_name,file,nchar-7);
	return common_name;
}
	
int play_from_disk(O_i *oi, int speed, int *image_number, int action,int num_selected_pixel)
{
	char *common_name;
	static int file_number;
	char fullfilename[128],filenumb[128],file[128];
	
	//draw_bubble(screen,0,550,200,"oi->im.n_f %d *image_number %d action %d num_selected_pixel %d",abs(oi->im.n_f),  *image_number, action, num_selected_pixel);
	
	fullfilename[0]='\0';
	file[0]='\0';
	filenumb[0]='\0';
	//if (action==MEASUREMENT)	win_printf("*image_number %d \n abs(oi->im.n_f)-1) %d",*image_number,abs(oi->im.n_f)-1);
 	if ((*image_number == (abs(oi->im.n_f)-1)) && (action==MEASUREMENT)) 
  	{
		win_printf("EOF");
   		return END_OF_FILE;
    } 
 	//wait_n_millisecond(5);
 	if (*image_number > (abs(oi->im.n_f)-1) ) 
 	
  		{	
    		if ( num_selected_pixel!=0) 
    		{
      				win_printf("You have to stop here\n You can not select points on differents sequences!");
      				*image_number--;
      				return END_OF_FILE;
	        }
    		common_name = find_present_automatic_string_filename(oi->filename);
    		
    		file_number = find_present_automatic_filenumber(oi->filename);
    		//win_printf("file_number %d \n o-inf %d",file_number,oi->im.n_f);
    		file_number ++;
    	    strcat( fullfilename,oi->dir);         	
         	strcat(file,common_name);
         	
         	sprintf(filenumb,"%04d.gr",file_number);
         	strcat(file,filenumb);
         	strcat(fullfilename,file);
         	
          	if (imreadfile(oi, fullfilename) == MAX_ERROR) 
           	{
            	win_printf("This is the end");
      			return -3;
   			}
		   oi->filename = strdup(file);
		   set_oi_t_unit_set(oi, oi->n_tu-1);
		    *image_number = 0;
    		return switch_frame(oi,*image_number); 
  		}
	else if (*image_number < 0) 
  		{
  			if ( num_selected_pixel!=0) 
    		{
      				win_printf("You have to stop here \n You can not select points on differents sequences!");
      				*image_number++;
      				return END_OF_FILE;
	        }
    		common_name = find_present_automatic_string_filename(oi->filename);
    		file_number = find_present_automatic_filenumber(oi->filename);
    		file_number --;
    		
    	    strcat( fullfilename,oi->dir);         	
         	strcat(file,common_name);
         	sprintf(filenumb,"%04d.gr",file_number);
         	strcat(file,filenumb);
         	strcat(fullfilename,file);
         	
    		if (imreadfile(oi, fullfilename) == MAX_ERROR) {win_printf("patate");
      														return -3;
      														}
	        //oi->filename = strdup(file);
	        oi->filename = strdup(file);
	        //win_printf("Filename %s \n num %d \n image number %d",backslash_to_slash(oi->filename),oi->im.n_f,*image_number);
    		*image_number = abs(oi->im.n_f) -1;
    		set_oi_t_unit_set(oi, oi->n_tu-1);
    		return switch_frame(oi,*image_number); 
 	
  		}
	else {	set_oi_t_unit_set(oi, oi->n_tu-1);
 			//win_printf("file_number %d \n oi->nf %d",file_number,oi->im.n_f);
    		return switch_frame(oi,*image_number); 
    		
  		}

}


int do_measurement_process(imreg *imr, O_i *oi,int speed,int image_number,int *action,int pix_size,int num_pix,int start_image_number,pix_position *pix_pos)
{
	static O_p *op ;
	static d_s *ds[1024];
	int i,j,k;
	
	if (oi == NULL)  return win_printf("Do not think about going further! There is no image");
	draw_bubble(screen,0,550,200,"MEASUREMENT ON ");
	if ((image_number-start_image_number) == 0)
	{
		//draw_bubble(screen,0,550,225,"merde patate %d",oi->im.n_f);
		//**ds = ((ds *)*) calloc(num_pix,sizeof(_ds));
		op = create_and_attach_op_to_oi (oi, abs(oi->im.n_f) ,abs(oi->im.n_f),IS_INT,0);
 		if (op ==NULL)return win_printf("Do not think about going further! op");
 		
 		/*create_attach_select_x_un_to_op(op, IS_X_UNIT_SET, float ax, float dx, char decade, char mode, char *name);
 		create_attach_select_y_un_to_op, (op, int type, float ax, float dx, char decade, char mode, char *name);*/
 		for (i = 1; i < (num_pix+1) ; i++)
     	{
     		ds[i] = create_and_attach_one_ds (op,	abs(oi->im.n_f), abs(oi->im.n_f), IS_INT);
     		set_ds_source(ds[i], "Intensity signal around point x= %d,y=%d over %d pixel from file %s starting at image %d Acquisition period  %f",(*(pix_pos+i)).x_pos,(*(pix_pos+i)).y_pos,pix_size,oi->filename,start_image_number,oi->tu[oi->n_tu-1]->dx);
 		
 		}
 		remove_ds_from_op(op,op->dat[0]);
	}
 
      for (i = 1; i < (num_pix+1) ; i++)
     	{
    	     	ds[i]->xd[image_number-start_image_number] = image_number-start_image_number;
    	     	ds[i]->yd[image_number-start_image_number] = 0; //initialise par securite
    	     	for (j=0; j< pix_size;j++)
    	     	{
    	     		for (k=0; k < pix_size; k++)
    		    	     		{
    			    		    	     		if (oi->im.data_type == IS_CHAR_IMAGE) ds[i]->yd[image_number-start_image_number] += oi->im.pixel[(*(pix_pos+i)).y_pos+j].ch[(*(pix_pos+i)).x_pos+k];//IL FAUDRA VERIFIER LE CENTRAGE
    		    	     		
                                                else if (oi->im.data_type == IS_UINT_IMAGE) ds[i]->yd[image_number-start_image_number] += oi->im.pixel[(*(pix_pos+i)).y_pos+j].in[(*(pix_pos+i)).x_pos+k];//IL FAUDRA VERIFIER LE CENTRAGE
                  
                  
    		    	     		}
   		
   		        }
   		        ds[i]->yd[image_number-start_image_number] =(pix_size !=0) ? ds[i]->yd[image_number-start_image_number]/(pix_size*pix_size): 0;
       }
	return 0;
}

int remove_unused_points(imreg *imr,O_i *oi,int image_number,int num_selected_pix,int start_image_number)
{
	//O_p *op = NULL;
	int i=0, k=0;
//	win_printf("we get in");
	
	if (oi == NULL) return win_printf("On n'ira pas loin comme cela!");
	
	//if (oi->o_p[oi->cur_op] != NULL) op = oi->o_p[oi->cur_op];
//	else return win_printf("On n'ira pas loin comme cela!");
	
//	win_printf("oi->o_p[oi->cur_op].ndat %d",oi->o_p[oi->n_op-1]->n_dat);
	for (i =0 ; i < num_selected_pix; i++)
    {	
    		for (k=image_number-start_image_number; k < (abs(oi->im.n_f)) ; k++) 
      		{
        		remove_point_from_data_set (oi->o_p[oi->n_op-1]->dat[i], image_number-start_image_number);
        	
         	}
    		
	}
	return 0;
}

int draw_selected_pixels(imreg *imr, int num_selected_pix,pix_position *pix_pos,int pix_size)
{
	
    int i;
    float scx, scy;
    DIALOG *d;
    
    d = find_dialog_associated_to_imr(imr, NULL);
    scx = (float)(get_oi_horizontal_extend(imr->one_i)*512)/(imr->one_i)->im.nx;
	scy = (float)(get_oi_vertical_extend(imr->one_i)*512)/(imr->one_i)->im.ny;
	acquire_screen();
	scare_mouse();  
	
            
    for (i = 1; i<num_selected_pix+1; i++)
    {
    	rectfill(screen, d->x+x_imdata_2_imr(imr,(int)(scx*pix_pos[i].x_pos-(int)(pix_size/2))), d->y+y_imdata_2_imr(imr,(int) (scy*pix_pos[i].y_pos+(int)(pix_size/2))), d->x+x_imdata_2_imr(imr,(int) (scx*pix_pos[i].x_pos+(int)(pix_size/2))), d->y+y_imdata_2_imr(imr,(int) (scy*pix_pos[i].y_pos-(int)(pix_size/2))), makecol(128, 128, 128));
    
    }
    release_screen();  
    unscare_mouse();  
    return 0;
	
	
}

int tape_play_simulation(imreg *imr,O_i *oi)
{
	int speed = 1;
	int action = PLAY;
	int out_variable = 0;
	int image_number = 0;
	int playfromdisk = 0;
	char question[128];
	int desired_image_number=0;
	unsigned long frame_time = 0 ;
	int num_selected_pix = 0;
	pix_position  *pix_pos = NULL;
	int pix_size =8;
	int start_image_number = 0;
	
	if (oi == NULL) return win_printf("Oi NULL!!!!");
	set_oi_t_unit_set(oi, oi->n_tu-1);
	
	pix_pos = (pix_position *) calloc(1024,sizeof(pix_position));
	if (pix_pos == NULL) return win_printf("pix_pos NULL!!!!");
	//change_mouse_to_rect(pix_size, pix_size);
	//select_pixels(pix_pos,pix_size);
	
	while (out_variable != -1)
   {
        if (keypressed() == TRUE || (mouse_b & 2) || (mouse_b & 1))     out_variable = tape_function(imr,oi->im.nx,oi->im.ny,&speed,&action,&pix_pos, &num_selected_pix, &pix_size,&start_image_number,oi,image_number);
        //draw_bubble(screen,0,550,25,"action =%d",action);
        //if (action == MEASUREMENT) do_measurement_process(imr,oi,speed,&image_number,&action,&pix_pos,&num_selected_pix);
        
        if ((action != STOP) || (out_variable != -1))
                {
             			   if (action == PREVIOUS_FRAME) image_number-=speed;
                           if (action == JUMP)
                           {
                                      sprintf(question,"This movie has %d images \n ",oi->im.n_f);   
                                      strcat(question,"You want to jump to image number %d ");
                                                 
                                                                      
                                      win_scanf(question,&desired_image_number);
                                      if (desired_image_number > (abs(oi->im.n_f)-1) || desired_image_number < 0)
                                      {
                                                             action = PLAY;
                                                             win_printf("You do not understand!");
                                      }
                                      else
                                      {
                                          image_number = desired_image_number;
                                          action = STOP;
                                      }
                           }
                           
                           //if (num_selected_pix !=0) win_printf("on est de retour!");
                           playfromdisk =play_from_disk(oi,speed, &image_number,action,num_selected_pix);
                           
                           //draw_bubble(screen,0,550,225,"playfromdisk %d ",playfromdisk);
                           
                           if (playfromdisk == END_OF_FILE) 
                           {
                                	win_printf("It is the end of the video sequence \n File ended");
                                	action = STOP; //ENVISAGER UN DECALAGE DE 1
                                	remove_unused_points(imr,oi,image_number,num_selected_pix,start_image_number);
                                	start_image_number = 0;
                                	playfromdisk = abs(oi->im.n_f)-1;
                                    
                           			
                                	//return 0;
                  	       }
                  	        if (num_selected_pix !=0) 
                           {
                           		draw_selected_pixels(imr,num_selected_pix,pix_pos,pix_size);
                           		//draw_bubble(screen,0,550,50,"Patate pix_x[%d] %d  pix_y[%d] %d",num_selected_pix,(*(pix_pos+num_selected_pix)).x_pos, num_selected_pix,(*(pix_pos+num_selected_pix)).y_pos);
                 		}
                  	      // if (num_selected_pix !=0) win_printf("on est de retour!");
                           refresh_image(imr, UNCHANGED);
                          
                            
                           
                           if (action == MEASUREMENT) 
                           {	
                           		if (start_image_number == 0) start_image_number = image_number;
                           	//	if (num_selected_pix !=0) win_printf("RETOUR!");
                           		do_measurement_process(imr,oi,speed,image_number,&action,pix_size,num_selected_pix,start_image_number,pix_pos);
                           		
                           		
             		       }
                           
                           
                           
                           
                           //win_printf("oi->im.time+image_number*(unsigned long)(oi->tu[oi->n_tu-1]->dx) %d",oi->im.time+image_number*(unsigned long)(oi->tu[oi->n_tu-1]->dx));
                           frame_time = oi->im.time+image_number*((oi->tu[oi->n_tu-1]->dx)/1000);
                           draw_bubble(screen,0,550,125,"Image %d ",image_number);
                           draw_bubble(screen,0,550,75,"filename %s ",oi->filename);
                           draw_bubble(screen,0,550,150," Time %s,",ctime(&frame_time));
                           
                           
                           if (playfromdisk == -3) return win_printf("Filename not found sequence finished");
                           else if (playfromdisk != image_number) return win_printf("Reading problem!");
                           if (action == PLAY || action == NEXT_FRAME || action == MEASUREMENT) image_number+=speed;
                           else if (action == PLAYBACK) 										image_number-=speed;
                           if (action == NEXT_FRAME) 											action = STOP;
                           if (action == PREVIOUS_FRAME) 										action = STOP;
                }
   }
   	reset_mouse_rect();
    return 0;

}

int from_op_2_image(O_p *op)
{
	
		imreg *imr = NULL;
		int n_points_in_image = 1000000/*mettre l'entier max*/ ;
		int i,j;
		O_i *oi;
		
		if (op == NULL) return win_printf("Op est NULL!");
		
	
    	imr = find_imr_in_current_dialog(NULL);
    	if (imr == NULL)	
    	{
     			imr = create_and_register_new_image_project( 0,   32,  900,  668);
     			if (imr == NULL) return win_printf("T'es mal barre MEC!");
		}
		
		for (i=0; i< op->n_dat; i++)
		{
			
			n_points_in_image = MIN(n_points_in_image , MIN(op->dat[i]->nx,op->dat[i]->ny));
		}
		
		oi = create_and_attach_oi_to_imr (imr, n_points_in_image, op->n_dat, IS_CHAR_IMAGE);
		
		for (i = 0 ; i < op->n_dat; i++)
		{
			for (j = 0; j < n_points_in_image; j++)
			{
						oi->im.pixel[i].ch[j] = op->dat[i]->yd[j];
			}
		}
		
		set_oi_source(oi, "Patate");
		return 0;


}


int tape_function(imreg *imr,int nx_pix,int ny_pix,int *speed,int *previous_action, pix_position **pix_pos, int *num_selected_pix, int *pix_size, int *start_image_number, O_i *oi, int image_number)
{
	int intermediate_mouse_pos_x;
	int intermediate_mouse_pos_y;
	
	intermediate_mouse_pos_x = mouse_x;
	intermediate_mouse_pos_y = mouse_y;
	
	if ((mouse_b & 1) && (*previous_action != MEASUREMENT))
	{
		*previous_action = PLAY;
 	    *speed = 1;
        
   }
   else if ((mouse_b & 2) && (*previous_action != MEASUREMENT))
   {
   		if (*num_selected_pix == 0)
      			{
      				win_scanf("Size of region around pixels ?%d", pix_size);
      				change_mouse_to_rect(*pix_size, *pix_size);
 				}
   		if ((int) x_imr_2_imdata(imr, mouse_x) < (int)((*pix_size)/2) || (int) x_imr_2_imdata(imr, mouse_x) > nx_pix-(int)((*pix_size)/2) || (int) y_imr_2_imdata(imr, mouse_y) < (int)((*pix_size/2)) || (int) y_imr_2_imdata(imr, mouse_y) > ny_pix-(int)((*pix_size)/2))
      	{
        	draw_bubble(screen,0,550,50,"OUT OF REGION");
        	
      	}
      	else{
      			(*num_selected_pix)++;
      			//win_printf("selected pixel %d",*num_selected_pix);
      			/*if (*num_selected_pix == 1)
      			{
      				
          			*pix_pos = (pix_position *) calloc(*num_selected_pix,sizeof(pix_position));
          			
             	}		      	
		      	 
		      	
		      	else (*pix_pos = (pix_position *)realloc(*pix_pos,(*num_selected_pix)*sizeof(pix_position)));
         		if (*pix_pos == NULL)
           		{
             		 win_printf("Cela merdouille dans la r�allocation m�moire");
             		 return 1;
           		}*/
		      	(*((*pix_pos)+(*num_selected_pix))).x_pos = (int) x_imr_2_imdata(imr, intermediate_mouse_pos_x);	
		      	(*((*pix_pos)+(*num_selected_pix))).y_pos = (int) y_imr_2_imdata(imr, intermediate_mouse_pos_y);	
		      	draw_bubble(screen,0,550,250,"pix_x[%d] %d \n pix_y[%d] %d",*num_selected_pix,(*(*pix_pos+*num_selected_pix)).x_pos, *num_selected_pix,(*(*pix_pos+*num_selected_pix)).y_pos);
		      	wait_n_millisecond(500);
      	    //}
  	    *previous_action = STOP;
        *speed = 1;
   		}
	}
   		
   else if (keypressed() == TRUE)
   {
		
	switch(readkey()>>8) 
	{
	           case  KEY_SPACE:
	      				
                               if (*previous_action == STOP) 
                               {
                			      	*previous_action = PLAY;
                			      	*speed = 1;
                			       	break;
                				
  			      	           } 
  			      	           if (*previous_action == MEASUREMENT) 
      			      	       {//	win_printf("image number %d \n num_selected_pix %d \n start_image_number %d",image_number, *num_selected_pix ,*start_image_number);
      			      	       		remove_unused_points(imr,oi,image_number,*num_selected_pix,*start_image_number);
      			      	       		
      			      	       		from_op_2_image(oi->o_p[oi->n_op-1]);
                       				*start_image_number = 0;
      			      	       		*num_selected_pix = 0;
                   			    	*previous_action = STOP;
                   			    	
                   			    	*speed = 1;
                       			    break;
                			   }
      			      	       if (*previous_action == PLAY) 
      			      	       {
                   			    	*previous_action = STOP;
                   			    	*speed = 1;
                       			    break;
                			   }
                			   if (*previous_action == PLAYBACK) 
                			   {
                   			    	*previous_action = STOP;
                   				    *speed = 1;
                       			    break;
                			   }
                			   break;
                   			
          		case KEY_F:	if (*previous_action == MEASUREMENT)
                                {
                                		*previous_action = MEASUREMENT;
                                		*speed = 1;
                                		
                                }
                            if (*previous_action == PLAY)
                  			{
                     			*speed=(*speed)*4%33;

                  			}
                 			break;
                case KEY_D:	   
                                *previous_action = MEASUREMENT;
                                *speed =1;
                                
              				    break;
          				
          		case KEY_B : if (*previous_action == MEASUREMENT)
                                {
                                		*previous_action = MEASUREMENT;
                                		*speed = 1;
                                		
                                }
                              if (*previous_action == PLAYBACK)
          					{
                  				*speed=(*speed)*4%33;
 
              				}
              				else if (*previous_action == PLAY ||*previous_action == STOP) 
              				{	
                  				*speed = 1;
 
      				            *previous_action = PLAYBACK;
  				            }
  				             
  				            
              			              				
              				break;
               case KEY_N     : if (*previous_action == MEASUREMENT)
                                {
                                		*previous_action = MEASUREMENT;
                                		*speed = 1;
                                		
                                }
                                else if (*previous_action == PLAY)
               					{                
        						               					*previous_action = NEXT_FRAME;
        						               					*speed = 1;
                                }
                                else if (*previous_action == STOP)
                                {
                                		*previous_action = NEXT_FRAME;
                                		*speed = 1;
                                }
                                break;
               case KEY_P     : if (*previous_action == MEASUREMENT)
                                {
                                		*previous_action = MEASUREMENT;
                                		*speed = 1;
                                		
                                }
                                else if (*previous_action == PLAY)
               					{                
        						               					*previous_action = PREVIOUS_FRAME;
        						               					*speed = 1;
                                }
                                else if (*previous_action == STOP)
                                {
                                		*previous_action = PREVIOUS_FRAME;
                                		*speed = 1;
                                }
                                break;

               case KEY_J     :  if (*previous_action == MEASUREMENT)
                                {
                                		*previous_action = MEASUREMENT;
                                		*speed = 1;
                                		
                                }
                                else
                                {                                          
             					                                *previous_action = JUMP;
             					                                *speed = 1;
                                }
                                 break;

              /* case KEY_A     :                 
           				       *previous_action = PLAY_AVERAGED;
                               *speed = 1;
                                break;*/
              
              			
              	case KEY_S: if (*previous_action == MEASUREMENT)
                                {
                                		*previous_action = MEASUREMENT;
                                		*speed = 1;
                                		break;  
                                }
                                else
                                		return -1;
              				break;
              				
                
  			
  			
     }
     }  
     clear_keybuf();

     //win_printf("On sort");
     return 0;

}
int create_and_adapt_tape_IFC_buffer_image(void)
{
	register int i = 0, j = 0; 
	void *buf1;
	unsigned char * buf;
	
	if(updating_menu_state != 0)	return D_O_K;
	
	if (oi_buffer_tape_IFC == NULL)
	{
		if((oi_buffer_tape_IFC = (O_i *)calloc(1,sizeof(O_i))) == NULL)	
		return win_printf("Our are out of memory guy");
		if (init_one_image(oi_buffer_tape_IFC, IS_CHAR_IMAGE)) 		return win_printf("You have a little problem guy");
		set_oi_source(oi_buffer_tape_IFC, "BUFFER TAPE IFC");
	}
	
	oi_buffer_tape_IFC->im.n_f = nframes_in_IFC_buffer;
	
	
	if (attr.dwBytesPerPixel == 1)
	{
    	if (alloc_one_image (oi_buffer_tape_IFC, attr.dwWidth, attr.dwHeight, IS_CHAR_IMAGE))return win_printf("XVin merdouille");
	}
	else if (attr.dwBytesPerPixel == 2)
    {
    		if (alloc_one_image (oi_buffer_tape_IFC, attr.dwWidth, attr.dwHeight, IS_UINT_IMAGE))return win_printf("XVin merdouille");
    }
    else if (attr.dwBytesPerPixel == 3)
    {
    	if (alloc_one_image (oi_buffer_tape_IFC, attr.dwWidth, attr.dwHeight, IS_RGB_PICTURE))return win_printf("XVin merdouille");
  	}
	for (i = 0,  buf = oi_buffer_tape_IFC->im.mem[0]; i < oi_buffer_tape_IFC->im.n_f; i++)//verifier**********
    {
    	switch_frame(oi_buffer_tape_IFC,i);
    	for (j = 0, buf1 = oi_buffer_tape_IFC->im.mem[i]; j < attr.dwHeight; j++)
                  	oi_buffer_tape_IFC->im.pixel[attr.dwHeight -1 -j].ch = buf1 + j * attr.dwWidth*attr.dwBytesPerPixel;

   	}
   	
   	//draw_bubble(screen,0,550,250,"Alloc done");
	return 0;	
}



int transfer_data(imreg *imr, O_i *oi, int n,  DIALOG *d, long long t, unsigned long dt, void *p) 
{
    present_image_number = n;
 	memcpy(oi_buffer_tape_IFC->im.mem[n %nframes_in_IFC_buffer],oi->im.mem[0],attr.dwHeight*attr.dwWidth*attr.dwBytesPerPixel);/*!= oi->im.mem[0]) return FALSE*/
	//draw_bubble(screen,0,550,200,"%d toto %d",n,attr.dwHeight);   
	return 0;
}


int transfer_to_movie(imreg *imr, O_i *oi, int n,  DIALOG *d, long long t, unsigned long dt, void *p) 
{
    static int j=0,k =0;
    
    if (j ==0) k=n;
     j++;
     
    if ((n - k) > num_frames_in_movie-1) 
    {
            disp_IFC.to_do = NULL;
            j = 0;
            return 0; 
    }
 	memcpy(oi_movie->im.mem[n-k],oi->im.mem[0],attr.dwHeight*attr.dwWidth*attr.dwBytesPerPixel);/*!= oi->im.mem[0]) return FALSE*/
	draw_bubble(screen,0,550,100,"num_frames %d ",n-k);   
	return 0;
}

int transfer_to_small_movie(imreg *imr, O_i *oi, int n,  DIALOG *d, long long t, unsigned long dt, void *p) 
{
    static int i=0,j=0,k =0;
    static int m=0, count_missed = 0;
    
    if (j ==0) k=n;
    
    if (n-m !=1 && j!=0) draw_bubble(screen,0,50,100,"Missed frame %d",++count_missed); 
	 j++;
    if ((n - k) > num_frames_in_movie-1) 
    {
            disp_IFC.to_do = NULL;
            j = 0;
            return 0; 
    }
    
    for (i = 0; i<num_pts; i++)
 	{
 	    memcpy(oi_small_movie->im.pxl[n-k][i].ch,oi->im.pixel[i].ch,num_pts);
 	    draw_bubble(screen,0,250,100,"num_frames %d i %d pix %d image %d",n-k,i,oi_small_movie->im.pxl[n-k][i].ch[0],oi->im.pixel[i].ch[0]); 
	
 	}
 	m=n;
	
	
	
	//draw_bubble(screen,0,550,100,"num_frames %d ",n-k);   
	return 0;
}


int set_background_transfer_image_data(void)
{
    //extern int determine_time_between_frames(imreg *imr, O_i *oi, int n,  DIALOG *d, long long t, unsigned long dt, void *p) ;
    if(updating_menu_state != 0)	return D_O_K;
    
    //if (disp_IFC.to_do == NULL)  win_printf("NULL");

    if (oi_buffer_tape_IFC == NULL) create_and_adapt_tape_IFC_buffer_image();
 	disp_IFC.to_do = transfer_data;
 	disp_IFC.param = NULL;
 	return 0;
}

int get_movie(void)
{
    HWND hWnd;
	imreg *imr;
	int i,j;
    void *buf1;
	unsigned char * buf;
	
	
    if(updating_menu_state != 0)	return D_O_K;
    
    imr = find_imr_in_current_dialog(NULL);//si squeeze raccourci! Il faudra alors passer aussi imr en parametre
	if (imr == NULL)	return FALSE;
    
    win_scanf("Number of frames in movie %d",&num_frames_in_movie);

    if (attr.dwBytesPerPixel == 1)
	{
	    oi_movie = create_and_attach_movie_to_imr (imr, attr.dwWidth, attr.dwHeight, IS_CHAR_IMAGE, num_frames_in_movie);
	    
    	
	}
	else if (attr.dwBytesPerPixel == 2)
    {
    		oi_movie = create_and_attach_movie_to_imr (imr, attr.dwWidth, attr.dwHeight, IS_UINT_IMAGE, num_frames_in_movie);
    }
    else if (attr.dwBytesPerPixel == 3)
    {
    	oi_movie = create_and_attach_movie_to_imr (imr, attr.dwWidth, attr.dwHeight, IS_RGB_PICTURE, num_frames_in_movie);
    }
    
    if (oi_movie == NULL) return 0;
    
	for (i = 0,  buf = oi_movie->im.mem[0]; i < oi_movie->im.n_f; i++)
    {
    	switch_frame(oi_movie,i);
    	for (j = 0, buf1 = oi_movie->im.mem[i]; j < attr.dwHeight; j++)
                  	oi_movie->im.pixel[attr.dwHeight -1 -j].ch = buf1 + j * attr.dwWidth*attr.dwBytesPerPixel;

   	}

    
    disp_IFC.to_do = transfer_to_movie;
    disp_IFC.param = NULL;
    
    return 0;   
}



int get_small_movie(void)
{
    HWND hWnd;
	imreg *imr;
	int i,j;
    void *buf1;
	unsigned char * buf;
	
	
	
    if(updating_menu_state != 0)	return D_O_K;
    
    oi_small_movie = NULL;
    
    imr = find_imr_in_current_dialog(NULL);//si squeeze raccourci! Il faudra alors passer aussi imr en parametre
	if (imr == NULL)	return FALSE;
    
    win_scanf("Number of frames in movie %d \n Number of pixels %d",&num_frames_in_movie,&num_pts);
    //select_rect();

    if (attr.dwBytesPerPixel == 1)
	{
	    oi_small_movie = create_and_attach_movie_to_imr (imr, num_pts, num_pts, IS_CHAR_IMAGE, num_frames_in_movie);
	    
    	
	}
	else if (attr.dwBytesPerPixel == 2)
    {
    		oi_small_movie = create_and_attach_movie_to_imr (imr, num_pts, num_pts, IS_UINT_IMAGE, num_frames_in_movie);
    }
    else if (attr.dwBytesPerPixel == 3)
    {
    	oi_small_movie = create_and_attach_movie_to_imr (imr, num_pts, num_pts, IS_RGB_PICTURE, num_frames_in_movie);
    }
    
    if (oi_small_movie == NULL) return 0;
    
	/*for (i = 0,  buf = oi_movie->im.mem[0]; i < oi_movie->im.n_f; i++)
    {
    	switch_frame(oi_movie,i);
    	for (j = 0, buf1 = oi_movie->im.mem[i]; j < attr.dwHeight; j++)
                  	oi_movie->im.pixel[attr.dwHeight -1 -j].ch = buf1 + j * attr.dwWidth*attr.dwBytesPerPixel;

   	}*/

    
    disp_IFC.to_do = transfer_to_small_movie;
    disp_IFC.param = NULL;
    
    return 0;   
}

int save_tape_mode(void)
{
	FILE *fp_images = NULL;
	int n_images_saved = 1;
	HWND hWnd;
	imreg *imr;
	O_i *oi = NULL;
    //unsigned char *buf;
    int num_files = 0;
    
    char filenamend[16];
    char common_name[20]="Yokota";
    int previous_grab_stat = 0;  
   
    char fullfile[512];
    register int i;
	char path[512], file[256], *pa; 
	int n_images_to_be_saved = 0;
    
    if(updating_menu_state != 0)	return D_O_K;
	
    hWnd = win_get_window();
    imr = find_imr_in_current_dialog(NULL);
	if (imr == NULL)	return 1;

    set_background_transfer_image_data();	
	/* a modifier*/
	//if (num_image_IFC != imr->cur_oi)	select_image_of_imreg_and_display(imr,num_image_IFC);
	
	movie_buffer_on_disk = 1;
	
	
 	
	
	
		
	switch_allegro_font(1);
	pa = (char*)get_config_string("IMAGE-GR-FILE","last_saved",NULL);	
	if (pa != NULL)		extract_file_path(fullfile, 512, pa);
	else				my_getcwd(fullfile, 512);
	strcat(fullfile,"\\");//Jusqu'ici c'est le path 
		
		
	num_files = get_config_int("TAPE_SAVE_MODE","filenumber",-1);
	win_scanf("Characteristic string for the files? %s\n Initial number for file%d?",common_name,&num_files);
		
  	strcat(fullfile,common_name);//on rajoute le debut du nom specifique
		
  	sprintf(filenamend,"%04d.gr",num_files);//on rajoute le nombre a incrementer et le format
	strcat(fullfile,filenamend);
    	
				
    i = file_select_ex("Save real time movie", fullfile, "gr", 512, 0, 0);
    switch_allegro_font(0);
    
     if (i != 0) 	
    	{
	    		set_config_string("IMAGE-GR-FILE","last_saved",fullfile);			
	    		extract_file_name(file, 256, fullfile);
	    		extract_file_path(path, 512, fullfile);
	    		strcat(path,"\\");
	    		
		}
	else 
 		{	
 			movie_buffer_on_disk = 1;
   			return 0;
		}
	
	set_config_int("TAPE_SAVE_MODE","filenumber",num_files);
	
	
 	//win_printf("background fixed");
  	
  	
 	
 	//buf =  oi->im.mem[0];  	
 	//LOCK_DATA( buf , oi->im.nx * oi->im.ny * attr.dwBytesPerPixel * nframes_in_buffer );
	//grabID = CICamera_Grab_HostBufEx(cam,0,buf,1 ,IFC_INFINITE_FRAMES, 0, 0, attr.dwWidth, attr.dwHeight);
    //if (grabID == NULL)  {
  //  								movie_buffer_on_disk = 0;
  //          						win_printf("GrabID failed");
  //          						return 0;
  //				         }
    //period = measure_aquisition_period(oi);
	
	
 	while (keypressed()!=TRUE)
 	{   if (mouse_b & 1)
  
        {     
        draw_bubble(screen,0,550,100,"%d",num_files);     
    	/*while (!(mouse_b & 1) )  //start saving with the mouse left button
          { 
                    if (keypressed()==TRUE) return freeze_video();
          } */
    	fullfile[0] = '\0';
    	//file = NULL;
    	num_files = get_config_int("TAPE_SAVE_MODE","filenumber",-1);
    	if (num_files < 0) num_files = 0;
    	sprintf(file,"%s%04d.gr",common_name,num_files++);//on rajoute le nombre a incrementer et le format
		strcat(fullfile,path);
		strcat(fullfile,file);
		//win_printf("oie");
		oi_buffer_tape_IFC->im.movie_on_disk = 1;
 		oi_buffer_tape_IFC->dir = strdup(path);
 		oi_buffer_tape_IFC->filename = strdup(file);
 		oi_buffer_tape_IFC->im.time = 0;
 		oi_buffer_tape_IFC->im.n_f= 0;
 		//oi->x_title = common_name;
 		set_image_starting_time(oi_buffer_tape_IFC);
		set_image_ending_time (oi_buffer_tape_IFC);
		//win_printf("before save");
  		save_one_image(oi_buffer_tape_IFC, fullfile);/***sauve en tete et les parametres**/
		//win_printf("after save");
		set_config_int("TAPE_SAVE_MODE","filenumber",num_files);
    	//return 0;
    	
     	fp_images = fopen (fullfile, "ab");
    	
    	if ( fp_images == NULL ) 	
    	{        
                win_printf("Cannot open the file!");
                movie_buffer_on_disk = 0;
                return 1;
        }
        
        previous_image_for_tape_saving = 0;
    	CICamera_GetGrabStats(cam,grabID,&stats);
    	previous_grab_stat = stats.CurrentFrameSeqNum; //A VOIR
    	n_images_saved = 0;
    	//win_printf("before save time");
		set_image_starting_time(oi_buffer_tape_IFC);
        //win_printf("After  save time");
        while (!(mouse_b & 2))//stop saving with the mouse right button 
        {
        	CICamera_GetGrabStats(cam,grabID,&stats);
        	n_images_to_be_saved = stats.CurrentFrameSeqNum - previous_grab_stat - n_images_saved;
        	
        
        	if (n_images_to_be_saved > 0)
        	{
        	  //win_printf("stats.CurrentFrameSeqNum %d n_images_to_be_saved %d nframes_in_IFC_buffer %d",stats.CurrentFrameSeqNum,n_images_to_be_saved,nframes_in_IFC_buffer);
 		      fwrite (oi_buffer_tape_IFC->im.mem[(stats.CurrentFrameSeqNum - n_images_to_be_saved)%nframes_in_IFC_buffer ],attr.dwBytesPerPixel,oi_buffer_tape_IFC->im.nx * oi_buffer_tape_IFC->im.ny,fp_images);
 		      n_images_saved++;
 		      draw_bubble(screen,0,150,100,"Currently saving image %d",n_images_saved+1);
       		  /*draw_bubble(screen,0,50,100,"Saved %d",n_images_saved);
 		      draw_bubble(screen,0,550,100,"Image %d",stats.CurrentFrameSeqNum - previous_grab_stat);
 		      draw_bubble(screen,0,175,100,"Image to be saved %d",n_images_to_be_saved);*/
 		      if (n_images_to_be_saved > nframes_in_IFC_buffer)
 		      { 
         			freeze_video();
         			return win_printf("One image lost");
      		  }
      		}
   		}
   		
   		fclose(fp_images); 
   		
   		
   		set_image_ending_time (oi_buffer_tape_IFC);
   		close_movie_on_disk(oi_buffer_tape_IFC,n_images_saved);
   		}
	}
	movie_buffer_on_disk = 0;
	//freeze_video();
    return 0;
}
int start_aquisition_tape(void)
{
    if(updating_menu_state != 0)	return D_O_K;
	save_tape_mode();
	return 0;
}
int change_nb_images_in_buffer(void)//A MODIFIER AUSSI OU SUPPRIMER!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
{
	//HWND hWnd;
	//imreg *imr;
	//O_i *oi;
    //unsigned char *buf;
    if(updating_menu_state != 0)	return D_O_K;
    
    win_scanf("How many frames in buffer %d",&nframes_in_IFC_buffer);
	
	
 /*   hWnd = win_get_window();
    imr = find_imr_in_current_dialog(NULL);
	if (imr == NULL)	return 1;
	if (num_image_IFC != imr->cur_oi)	select_image_of_imreg_and_display(imr,num_image_IFC);
	oi = imr->one_i ;*/
	create_and_adapt_tape_IFC_buffer_image();
 	//buf =  oi->im.mem[0];
  	//resize_image(previous_board,imr, &buf);
	
   	return 0;	
}



int	fill_avg_line_profiles_from_im(O_i *oi, int xc, int yc, int cl, int cw, int *x, O_i *oit)
{
	register int j, i;
	unsigned char *choi;	
	short int *inoi;
	unsigned short int *uioi;
	int ytt, xll, onx, ony, xco, yco, cl2, cw2;
	union pix *pd, *pt;
	
	if (oi == NULL || x == NULL)	return 1;
	onx = oi->im.nx;	ony = oi->im.ny;
	pd = oi->im.pixel;
	pt = oit->im.pixel;
	cl2 = cl >> 1;	cw2 = cw >> 1;
	yco = (yc - cw2 < 0) ? cw2 : yc;
	yco = (yco + cw2 <= ony) ? yco : ony - cw2;
	xco = (xc - cl2 < 0) ? cl2 : xc;
	xco = (xco + cl2 <= onx) ? xco : onx - cl2;
	for(i = 0; i < cl; i++)		x[i] = 0;	
	if (oi->im.data_type == IS_CHAR_IMAGE)
	{
		for(j = 0, ytt = yco - cw2, xll = xco - cl2; j< cw; j++)
		{	
			choi = pd[ytt+j].ch + xll;
			
			for (i = 0; i < cl; i++)
   			{
      			x[i] += (int)choi[i];	
      			pt[j].ch[i] = choi[i];	
   			}
		}
	}
	else if (oi->im.data_type == IS_INT_IMAGE)
	{
		for(j = 0, ytt = yco - cw2, xll = xco - cl2; j< cw; j++)
		{	
			inoi = pd[ytt+j].in + xll;
			for (i = 0; i < cl; i++)
   			{
      			x[i] += (int)inoi[i];
         		pt[j].in[i] = inoi[i];	
   			}
		}
	}
	else if (oi->im.data_type == IS_UINT_IMAGE)
	{
		for(j = 0, ytt = yco - cw2, xll = xco - cl2; j< cw; j++)
		{	
			uioi = pd[ytt+j].ui + xll;
			for (i = 0; i < cl; i++)		
   			{
      			x[i] += (int)uioi[i];
         		pt[j].ui[i] = uioi[i];	
   			}
		}
	}
	else return 1;	
	return 0;		
}

int average_image_in_real_time (void)
{
    int starting_avg_frame = 0;
    int current_avg_frame =0;
    //f4vector *f4vect = NULL;
    
    
	
    if(updating_menu_state != 0)	return D_O_K;
    
    if (oi_buffer_tape_IFC == NULL) return win_printf("The buffering image is NULL");
    
	
    
    CICamera_GetGrabStats(cam,grabID,&stats);
   	starting_avg_frame = stats.CurrentFrameSeqNum;
   	
   //if (current_avg_frame > starting_avg_frame)
   	return 0;
}
/*


int change_mouse_to_rect(int cl, int cw)
{
    BITMAP *ds_bitmap = NULL;
    int color, col;


    ds_bitmap = create_bitmap(cl+1,cw+1);
    col = Lightmagenta;
    color = makecol(EXTRACT_R(col), EXTRACT_G(col), EXTRACT_B(col));
    clear_to_color(ds_bitmap, color);

    color = makecol(255, 0, 0);                        

    line(ds_bitmap,0,0,cl,0,color);
    line(ds_bitmap,0,0,0,cw,color);
    line(ds_bitmap,0,cw,cl,cw,color);
    line(ds_bitmap,cl,0,cl,cw,color);
    scare_mouse();    
    set_mouse_sprite(ds_bitmap);
    set_mouse_sprite_focus(cl/2, cw/2);
    unscare_mouse();
    return 0;    
}
int reset_mouse_rect(void)
{
    scare_mouse();    
    set_mouse_sprite(NULL);
    set_mouse_sprite_focus(0, 0);
    unscare_mouse();
    return 0;
}
int	select_rect(void)
{
	static int cl = 128, cw = 16, i;
	
	if(updating_menu_state != 0)	return D_O_K;	
	i = win_scanf("Rectangle size cl %d cw %d",&cl,&cw);
	
 	if (i == CANCEL)	 return reset_mouse_rect();
 	return change_mouse_to_rect(cl, cw);
}
*/


MENU *tape_IFC_image_menu(void)
{
	static MENU mn[32];
	
	if (mn[0].text != NULL)	return mn;
	//add_item_to_menu(mn,"Select rectangle", select_rect, NULL, 0, NULL);
	add_item_to_menu(mn,"Adjust buffer", change_nb_images_in_buffer, NULL, 0, NULL);
	add_item_to_menu(mn,"Save video tape", save_tape_mode, NULL, 0, NULL);
	add_item_to_menu(mn,"Play video tape", play_movie_real_time, NULL, 0, NULL);
	add_item_to_menu(mn,"Background task",set_background_transfer_image_data, NULL, 0, NULL);
	
    
	return mn;
}

int tape_ifc_main(int argc, char **argv)
{
	imreg *imr = NULL;
	
	int n;
	
	
	
 	//imr = create_and_register_new_image_project( 0,   32,  900,  668);
 	//if (imr == NULL) win_printf("Patate t'es mal barre!");
   
 	
 	//imr->image_got_mouse = image_ifc_got_mouse;
    //imr->image_lost_mouse = image_ifc_lost_mouse; 	
	//win_printf("Potateeee");
	add_image_treat_menu_item("TAPE IFC ", NULL, tape_IFC_image_menu(), 0, NULL);
	//win_printf("Potato");
    broadcast_dialog_message(MSG_DRAW,0);
	return 0;    
}

#endif
