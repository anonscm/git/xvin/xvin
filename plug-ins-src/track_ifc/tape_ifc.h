#ifndef _TAPE_IFC_H_
#define _TAPE_IFC_H_

#define SAVE2FILE           1
#define PLAY				1
#define	PLAYBACK			-1
#define STOP				0
#define JUMP				2
#define NEXT_FRAME		    3
#define	OPEN				1
#define CLOSE				0
#define PREVIOUS_FRAME		4
#define	MEASUREMENT			5
#define END_OF_FILE			-5

typedef struct localize_pix
{
 		int x_pos;
 		int y_pos;
 		unsigned char z_level;
}pix_position;

#ifndef _TAPE_IFC_C
PXV_VAR(int,nframes_in_IFC_buffer);
PXV_VAR(int , previous_image_for_tape_saving);
PXV_VAR(IFC_disp, tape_disp_IFC);
PXV_VAR(O_i* , oi_buffer_tape_IFC);
#else
int nframes_in_IFC_buffer = 64;
int previous_image_for_tape_saving = 0;
O_i *oi_buffer_tape_IFC = NULL;
IFC_disp disp_tape_IFC  ={NULL,NULL,NULL};
#endif
PXV_FUNC(int,set_background_transfer_image_data,(void));
PXV_FUNC(int,transfer_data,(imreg *imr, O_i *oi, int n,  DIALOG *d, long long t, unsigned long dt, void *p)); 
PXV_FUNC(int,reset_mouse_rect,(void));
PXV_FUNC(int,save_tape_mode,(void));
PXV_FUNC(int,play_movie_real_time,(void));
PXV_FUNC(int,change_nb_images_in_buffer,(void));
PXV_FUNC(int,do_load_im,(void));
PXV_FUNC(int,tape_play_simulation,(imreg *imr,O_i *oi));
PXV_FUNC(int,tape_function,(imreg *imr,int nx_pix,int ny_pix,int *speed,int *previous_action, pix_position **pix_pos, int *num_selected_pix, int *pix_size, int *start_image_number, O_i *oi, int image_number));
PXV_FUNC(int,draw_selected_pixels,(imreg *imr, int num_selected_pix,pix_position *pix_pos,int pix_size));
PXV_FUNC(int,do_measurement_process,(imreg *imr, O_i *oi,int speed,int image_number,int *action,int pix_size,int num_pix,int start_image_number,pix_position *pix_pos));
PXV_FUNC(int, remove_unused_points,(imreg *imr,O_i *oi,int image_number,int num_selected_pix,int start_image_number));
PXV_FUNC(int, tape_ifc_main,(int argc, char **argv));
#endif
