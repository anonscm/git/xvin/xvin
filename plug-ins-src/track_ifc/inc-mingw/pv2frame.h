/******************************************************************************
 *
 * MODULE   
 *		PCDFrame.h
 *
 * REVISION INFORMATION  
 *		$Logfile: /ifc/include/pv2frame.h $
 *		$Revision: 2 $
 *		$Modtime: 10/02/02 3:06p $
 *
 * ABSTRACT  
 *		IFC interface of the CP2VFrame class.
 *
 * TECHNICAL NOTES 
 *
 *
 * Copyright (c) 1998-2002 Coreco Imaging , Inc.  All rights reserved.
 *
 ******************************************************************************/

#if !defined(AFX_P2VFRAME_H__970BD353_AF79_11D1_AE7A_00A0C959E757__INCLUDED_)
#define AFX_P2VFRAME_H__970BD353_AF79_11D1_AE7A_00A0C959E757__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#include <ifcstd.h>
#include <ifcclass.h>
#include <IFrame.h>
#include <IFCTyp.h>

class IFC_EXT_API CP2VFrame;

#include <PCVision2.h>
#include <Pv2Cam.h>

#ifdef __cplusplus
class IFC_EXT_API CP2VFrame : public CIFrame  
{
public:
	static void DeleteAllFrames(pCModule mod);
	BOOL CreateSeq(pCModule mod,int frameCnt,CP2VFrame **frames,DWORD dx, DWORD dy, IFC_DEPTH depth, IFC_COLOR color);
	virtual BOOL Create(pCModule mod, DWORD dx, DWORD dy, IFC_DEPTH depth, IFC_COLOR color, DWORD bytesPerFrame=0);
	CP2VFrame();
	virtual ~CP2VFrame();
	void SetFlex(CPCVision2	*mod, DWORD dx, DWORD dy, IFC_DEPTH depth, IFC_COLOR color, DWORD offset);
	virtual CIFrame *Create(CIFrame *frame);

protected:

private:
	static CIFrame *newFrame(pCModule mod,DWORD dx, DWORD dy, IFC_DEPTH depth, IFC_COLOR color);
	static BOOL newSeqFrame(pCModule mod,int frameCnt,CP2VFrame **frames,DWORD dx, DWORD dy, IFC_DEPTH depth, IFC_COLOR color);
	CP2VFrame(CP2VFrame *fr);
	friend class CPCVision2;
};
#endif

#endif // !defined(AFX_P2VFRAME_H__970BD353_AF79_11D1_AE7A_00A0C959E757__INCLUDED_)
