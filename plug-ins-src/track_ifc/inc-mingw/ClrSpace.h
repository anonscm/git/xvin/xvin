/******************************************************************************
 *
 * MODULE   
 *		ClrSpace.h
 *
 * REVISION INFORMATION  
 *		$Logfile: /ifc/include/ClrSpace.h $
 *		$Revision: 18 $
 *		$Modtime: 1/30/02 5:07p $
 *
 * ABSTRACT  
 *		IFC interface of the CClrSpace class.
 *
 * TECHNICAL NOTES 
 *
 *
 * Copyright 1998 (c) 1998 Imaging Technology, Inc.  All rights reserved.
 *
 ******************************************************************************/

#if !defined(AFX_CLRSPACE_H__E36F2682_B43B_11D1_AE7B_00A0C959E757__INCLUDED_)
#define AFX_CLRSPACE_H__E36F2682_B43B_11D1_AE7B_00A0C959E757__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <ifctyp.h>
#include <ifcos.h>
#include <camera.h>

typedef enum {
	IFC_YCRCBTORGB16,
	IFC_YCRCBTORGB24,
	IFC_YCRCBTORGB32
} IFC_YCRCBTORGB;

typedef enum {
	IFC_RGB16TOYCRCB,
	IFC_RGB24TOYCRCB,
	IFC_RGB32TOYCRCB,
} IFC_RGBTOYCRCB;


typedef struct {
	float redGain;
	float greenGain;
	float blueGain;

} IFC_BAYER_EXT_ATTR,*PIFC_BAYER_EXT_ATTR;


#ifdef __cplusplus
class IFC_EXT_API CClrSpace : public CIobject 
{
public:
	static BOOL RGBToYCrCb(pBYTE dest, pBYTE src, DWORD src_bytes, IFC_RGBTOYCRCB mode);
	static BOOL YCrCbToRGB(pBYTE dest, pBYTE src, DWORD src_bytes, IFC_YCRCBTORGB mode, BOOL redMostSig=FALSE);
	static BOOL YCrCbToRGBReverse(pBYTE iDest, pBYTE src, DWORD width, DWORD height, IFC_YCRCBTORGB mode);


	CClrSpace();
	virtual ~CClrSpace();

};

extern "C" {
#endif

#define IFC_BAYOPT_LINE_REVERSE 1
#define IFC_BAYOPT_YCRCB 2
#define IFC_BAYOPT_10BIT_SRC 4


BOOL IFCCALL IfxBayerFilterConvert(DWORD options,IFC_BAYER_FILTER_TYPE filterType, pBYTE dest, pBYTE src, DWORD width,DWORD height, IFC_COLOR targetColor, PIFC_BAYER_EXT_ATTR extAttr);

#ifdef __cplusplus
}
#endif


#endif // !defined(AFX_CLRSPACE_H__E36F2682_B43B_11D1_AE7B_00A0C959E757__INCLUDED_)
