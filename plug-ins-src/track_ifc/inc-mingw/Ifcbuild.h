/******************************************************************************
 *
 * MODULE
 *    ifcbuild.h
 *
 * REVISION INFORMATION
 *    $Logfile: /ifc/include/Ifcbuild.h $
 *    $Revision: 31 $
 *    $Modtime: 2/12/03 1:13p $
 *
 * ABSTRACT
 *    IFC Build Version Include File
 *
 * TECHNICAL NOTES
 *
 *
 * Copyright (c) 1999-2002 Imaging Technology Incorporated  All rights reserved.
 *
 ******************************************************************************/

#ifndef _IFCBUILD_DEF
#define _IFCBUILD_DEF

#define VER_INTERNALNAME_STR        "Build 75.06"

#define VER_CORE_VERSION             5,6,0,6
#define VER_CORE_VERSION_STR        "5.6.0.6"

#define VER_CM_STUB_VERSION          5,6,0,0
#define VER_CM_STUB_VERSION_STR     "5.6.0.0"

#define VER_PA_STUB_VERSION          5,6,0,0
#define VER_PA_STUB_VERSION_STR     "5.6.0.0"

#define IFC_MAX_VER_CMPT_FILES	10	/* Max number of files in a component */
#define IFC_MAX_VER_LEN		20

#define IFC_CORE_LIBNAME "ifc20.lib"
#define IFC_DISPLAY_LIBNAME "ifcdsp20.lib"

#define IFC_DRIVER_DLL_NAME "ifcdrv20.dll"

#define IFC_CSC_SERVICE_NAME "ifccsc20"
#define IFC_CSC_SERVICE_NAMEW L"ifccsc20"

#define IFC_IFCUI_DLL_NAME "ifcui20.dll"


#endif
