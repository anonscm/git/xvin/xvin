/******************************************************************************
 *
 * MODULE
 *    pv2reg.h
 *
 * REVISION INFORMATION
 *    $Logfile: /ifc/include/Pv2Reg.h $
 *    $Revision: 4 $
 *    $Modtime: 12/20/02 2:34p $
 *
 * ABSTRACT
 *    IFC PCVision2 Register names
 *
 * TECHNICAL NOTES
 *
 *
 * Copyright (c) 2002 Coreco Imaging , Inc.  All rights reserved.
 *
 ******************************************************************************/

#ifndef _P2V_REG
#define _P2V_REG




enum {                    /* PCI configuration space registers */

    P2V_VENDOR_ID_32,


	P2V_MAX_REG1
};





#endif

