/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _C843_MAGNETS_C_
#define _C843_MAGNETS_C_

# include "allegro.h"
#ifdef XV_WIN32
# include "winalleg.h"
# include <windowsx.h>
#endif
# include "xvin.h"
# include "C843_GCS_DLL.h"
/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled
# include "../trackBead/magnetscontrol.h"


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "C843_magnets.h"

#define BAD -1


#define PI_TRUE 1
#define PI_FALSE 0
#define ROT_MOTOR 1
#define TRANSLATION_MOTOR 2

int id_C843 = 512;
char * name1 = "C-150.PD";
char * name2 = "M-126.PD";
char *rot_mot = "1";
char *trans_mot = "2";
char *both_mot = "12";
int mag_pos_mot = 0;
int rot_pos_mot = 0;

float _read_magnet_z_value(void)
{
	double trans_position_test;
    
    
    if ( C843_qPOS (id_C843, trans_mot, &trans_position_test) == PI_TRUE) return (float)trans_position_test;
    
	return BAD;
}


float _read_rot_value(void)
{
	double rot_position_test;
    
    
    if ( C843_qPOS (id_C843, rot_mot, &rot_position_test) == PI_TRUE) return (float)rot_position_test;
    
	return BAD;
}


int	_set_rot_value(float rot)
{
  double rot_position;

  rot -= n_rot_offset;
  //  n_rota = rot + n_rot_offset;

  rot_position = (double) rot;
  C843_MOV (id_C843, rot_mot, &rot_position);
  return 0;
}

int _set_rot_ref_value(float rot)  /* in tour */
{
  double new_ref;

  n_rot_offset = rot;
  n_rota = rot;
  new_ref = (double)rot;
  //C843_DFH (id_C843, rot_mot);
  C843_POS (id_C843, rot_mot, &new_ref);
  return 0;
}

int   _set_magnet_z_value(float pos)
{
  double trans_position;

  pos = (pos > absolute_maximum_zmag) ? absolute_maximum_zmag : pos; 
  pos -=  n_magnet_offset;
  n_magnet_z = pos + n_magnet_offset;
  trans_position = (double) pos;//check
  C843_MOV (id_C843, trans_mot, &trans_position);
  return 0;
}

int _set_magnet_z_ref_value(float z)  /* in mm */
{
  n_magnet_offset = z;
  n_magnet_z = z;
  C843_DFH (id_C843, trans_mot);
  return 0;
}



int init_C843(void)
{
    char mot_names[128];
    BOOL Is_move  = PI_TRUE, Is_ref_OK = PI_TRUE, Is_ref  = PI_TRUE;
    int iCmdarray = 17;
    double dValarray = -1;
    
    if(updating_menu_state != 0)	return D_O_K;
    //win_printf("motor id %d",id_C843);               
    id_C843 = C843_Connect(1);
    if (id_C843 == -  1) return win_printf("Connection of C843 board failed");
    //win_printf("motor id %d",id_C843);
    //win_printf ("controller connected %d TRUE %d",C843_IsConnected (0), PI_TRUE);
    if (C843_IsConnected (id_C843) == PI_TRUE);// win_printf ("controller connected %d",C843_IsConnected (0));
    if (C843_CST (id_C843, "1", name1) != PI_TRUE)return win_printf("Motor 1 name affectation failed");
    if (C843_CST (id_C843, "2", name2) != PI_TRUE)return win_printf("Motor 2 name affectation failed");
    
    //win_printf("names %d",C843_qCST (id_C843, "2", mot_names, 128));
    
    if (C843_INI (id_C843, "") != PI_TRUE)return win_printf("Init failed");
    if (C843_REF (id_C843, rot_mot)!= PI_TRUE)return win_printf("Rotation init position failed");

    while (Is_ref_OK == PI_TRUE) C843_IsReferencing (id_C843, rot_mot,&Is_ref_OK);

    if (C843_IsReferenceOK (id_C843, rot_mot,&Is_ref)!= PI_TRUE)return win_printf("Rotation init position failed");
    
/*     #ifdef _PICO_CFG_H_ */
/*     //Check translation MOTOR orientation// */
/*     if (Pico_micro_param.translation_motor_inverted == -1) */
/*     { */
/*         iCmdarray = 17; */
/*         dValarray = -1; */
/*         C843_SPA (id_C843, rot_mot, &iCmdarray, &dValarray, NULL); */
/*         if (C843_MPL (id_C843, trans_mot)!= PI_TRUE)return win_printf("Trans init position failed"); */
    
/*     }     */
/*     else  */
    //   #endif
/*     { */
         if (C843_MPL (id_C843, trans_mot)!= PI_TRUE)return win_printf("Trans init position failed"); 
/*     } */


/*     while (Is_move == PI_TRUE) C843_IsMoving (id_C843, trans_mot, &Is_move); */
/*     if (C843_REF (id_C843, trans_mot)!= PI_TRUE)return win_printf("Translation init position failed"); */
/*     while (Is_ref_OK == PI_TRUE) C843_IsReferencing (id_C843, trans_mot,&Is_ref_OK); */
/*     if (C843_IsReferenceOK (id_C843, trans_mot,&Is_ref)!= PI_TRUE)return win_printf("Translation init position failed"); */


    //We must set the parameters to get 4000counts == 1 turn default values do not correspond to this value
    //Default Parameters OK for M126PD
    
    
/*     iCmdarray = 14; */
/*     dValarray = 4000; */
/*     C843_SPA (id_C843, rot_mot, &iCmdarray, &dValarray, NULL); */
/*     iCmdarray = 15; */
/*     dValarray = 1; */
/*     C843_SPA (id_C843, rot_mot, &iCmdarray, &dValarray, NULL); */
    
/*     //Set maximum translation value */
/*     #ifdef _PICO_CFG_H_ */
     iCmdarray = 21; 
     dValarray = 5;//(double)Pico_micro_param.translation_max_pos; 
     if ( C843_SPA (id_C843, trans_mot, &iCmdarray, &dValarray, NULL) == PI_FALSE) return win_printf("SPA %s failed",trans_mot); 
/*     #endif */
    
    
    return D_O_K;
    
}


int is_motor_moving(void)
{
       int i = 0;
       char szAxes[1];
       BOOL Is_mov;
       
       if(updating_menu_state != 0)	return D_O_K;
 
       i = RETRIEVE_MENU_INDEX;
       sprintf(szAxes,"%d",i);
       
       C843_IsMoving (id_C843, szAxes, &Is_mov);
       
       if (Is_mov == PI_TRUE)win_printf("The Motor %s is moving",szAxes);
       else win_printf("The Motor %s is NOT moving",szAxes);
       return 0;
}


int set_motor_home(void)
{
       int i = 0;
       char szAxes[1];
       
       
       if(updating_menu_state != 0)	return D_O_K;
 
       i = RETRIEVE_MENU_INDEX;
       sprintf(szAxes,"%d",i);
       if (C843_GOH (id_C843, szAxes) != PI_TRUE) win_printf("Go Home failed");
       
       return 0;
}


int relative_mov(void)
{
       int i = 0;
       char szAxes[1];
       double increment =0;
       float transient_pos = 0;
       
       if(updating_menu_state != 0)	return D_O_K;
 
       i = RETRIEVE_MENU_INDEX;
       sprintf(szAxes,"%d",i);

       win_scanf("What position %f ?",&transient_pos);
       increment = (double) transient_pos;
       if (C843_MVR (id_C843,szAxes,&increment) != PI_TRUE) return win_printf("Increment failed");
       
       return 0;
}

int set_rot_pos(void)
{
    double rot_position = 0;
    float transient_pos = 0;
    
    if(updating_menu_state != 0)	return D_O_K;
    
    C843_qPOS (id_C843, rot_mot, &rot_position);
    transient_pos = (float) rot_position;
    win_scanf("What position %f ?",&transient_pos);
    rot_position = (double) transient_pos;
    //win_printf("position required %g",rot_position);
    //win_printf("returned value %d with %g param",C843_MOV (id_C843, rot_mot, &rot_position),rot_position);
    //win_printf("mag pos %g",rot_position);
    C843_MOV (id_C843, rot_mot, &rot_position);
 return 0;   
}



int set_trans_pos(void)
{
    double trans_position = 0;
    float transient_pos = 0;
    
    if(updating_menu_state != 0)	return D_O_K;
    
    C843_qPOS (id_C843, trans_mot, &trans_position); 
    transient_pos = (float)trans_position;
    win_scanf("What position %f ?",&transient_pos);
    trans_position = (double) transient_pos;
    //win_printf("position required %g",trans_position);
    //win_printf("returned value %d with %g param",C843_MOV (id_C843, trans_mot, &trans_position),trans_position);
    if ( C843_MOV (id_C843, "2"/*trans_mot*/, &trans_position) == PI_FALSE) return win_printf("Failed");
 return 0;   
}



int what_rot_pos(void)
{
    double rot_position_test;
    if(updating_menu_state != 0)	return D_O_K;
    
    C843_qPOS (id_C843, rot_mot, &rot_position_test);
    win_printf("rot pos %g",rot_position_test);
 return D_O_K;   
}

int what_trans_pos(void)
{
    double trans_position;
    
    
    if(updating_menu_state != 0)	return D_O_K;
    C843_qPOS (id_C843, trans_mot, &trans_position);
    win_printf("mag pos %g",trans_position);
    return D_O_K;
}

int set_PID_param_rot(void)
{
    if(updating_menu_state != 0)	return D_O_K;
    return 0;
}

int set_PID_param_trans(void)
{
    
    if(updating_menu_state != 0)	return D_O_K;
    return 0;
}

int origine(void)
{
    int j = 0, i = 0; 
    char szAxes[1];
    if(updating_menu_state != 0)	return D_O_K;
 
    i = RETRIEVE_MENU_INDEX;
    sprintf(szAxes,"%d",i);

    win_scanf("origin plus (0) or minus (1) %d?",&j);
    if (j == 0)    C843_MPL (id_C843, szAxes);
    else if (j == 1) C843_MNL (id_C843, szAxes);
    else win_printf("Learn how to read, dummy!");
    
    return 0;
}

int abrupt_stop(void)
{
                     
    if(updating_menu_state != 0)	return D_O_K;
    return C843_STP (id_C843);
}

int constant_velocity_setting(void)
{
    int j = 0, i = 0; 
    char szAxes[1];
    if(updating_menu_state != 0)	return D_O_K;
 
    i = RETRIEVE_MENU_INDEX;
    sprintf(szAxes,"%d",i);
    C843_SVO (id_C843, szAxes, PI_FALSE);
    win_scanf("Seed value [-32767 to 32767] %f?",&j);
    if (( j > -32768 ) && ( j < 32768 )  )  
    C843_SMO (id_C843, szAxes, &j);
    else win_printf("Bad value!");
    return 0;   
}

int set_feedback_loop(void)
{
    
    int i = 0; 
    char szAxes[1];
    BOOL param = PI_TRUE;
    if(updating_menu_state != 0)	return D_O_K;
 
    i = RETRIEVE_MENU_INDEX;
    sprintf(szAxes,"%d",i);
    C843_SVO (id_C843, szAxes, &param);
    return 0;
}

int find_PID_param(void)
{
    
    int i = 0; 
    char szAxes[1];
    //int Cmdarray = 1;
    double Valarray;
    char szStageNames[10];
    
    if(updating_menu_state != 0)	return D_O_K;
 
    i = RETRIEVE_MENU_INDEX;
    sprintf(szAxes,"%d",i);
    
    for (i = 1; i < 10; i++)
    {
        if (i == 9) i=59;
        if (C843_qSPA (id_C843,szAxes, &i,&Valarray, szStageNames, 5) != PI_TRUE) return win_printf("failed in retrieving the parameters");
    
        win_printf("%d %s= %g",i,szStageNames,Valarray);
        
    }

          return 0;
}
int clear_status(void)
{
    int  i = 0; 
    char szAxes[1];
    if(updating_menu_state != 0)	return D_O_K;
 
    i = RETRIEVE_MENU_INDEX;
    sprintf(szAxes,"%d",i);


    C843_CLR (id_C843,szAxes);
    return 0;
}
int define_motor_home(void)
{
    int i = 0; 
    char szAxes[1];
    if(updating_menu_state != 0)	return D_O_K;
 
    i = RETRIEVE_MENU_INDEX;
    sprintf(szAxes,"%d",i);



    C843_DFH (id_C843, szAxes);
    return 0;
}
int close_connection(void)
{
       if(updating_menu_state != 0)	return D_O_K;
       C843_CloseConnection (id_C843);
       return 0;
}

#ifdef _PICO_CFG_H_

int max_rot_velocity_setting(void)
{
    int iCmdarray = 10;
    double  dValarray;
    int iMax_vel =  Pico_micro_param.rotation_max_velocity;
    
      
    if(updating_menu_state != 0)	return D_O_K;
    win_scanf("Maximum rot velocity? (0<	<360000) %d" ,  &iMax_vel);
    if ( 0 < iMax_vel && iMax_vel < 360000)
    {
       dValarray = (double) iMax_vel;
       if (C843_SPA (id_C843, rot_mot, &iCmdarray, &dValarray, NULL) == PI_TRUE)
       {
           Pico_micro_param.rotation_max_velocity = iMax_vel;
           set_config_int("PICO_CFG", "rotation_max_velocity", Pico_micro_param.rotation_max_velocity);
           Open_Pico_config_file();
           write_Pico_config_file();
           Close_Pico_config_file();
       }  
   }      
       
       return D_O_K;
}

int max_translation_velocity_setting(void)
{
    int iCmdarray = 10;
    double  dValarray;
    int iMax_vel =  Pico_micro_param.translation_max_velocity;
    
      
    if(updating_menu_state != 0)	return D_O_K;
    win_scanf("Maximum rot velocity? (0<	<360000)",   &iMax_vel);
    if ( 0 < iMax_vel && iMax_vel< 240000)
    {
       dValarray = (double) iMax_vel;
       if (C843_SPA (id_C843, trans_mot, &iCmdarray, &dValarray, NULL) == PI_TRUE)
       {
           Pico_micro_param.translation_max_velocity = iMax_vel;
           set_config_int("PICO_CFG", "rotation_max_velocity", Pico_micro_param.translation_max_velocity);
           Open_Pico_config_file();
           write_Pico_config_file();
           Close_Pico_config_file();
       } 
   }       
       
       return D_O_K;
}


int set_rot_speed(void)
{
    
	 
    int iCmdarray = 10;
    double  dValarray;
    float fMax_vel =  0;//Pico_micro_param.translation_max_velocity;
    static float fVel = 0;
    char *string = NULL;
    
    if(updating_menu_state != 0)	return D_O_K;
    
    C843_qSPA (id_C843, rot_mot, &iCmdarray, &dValarray, NULL, 0);
    
    fMax_vel = (float) dValarray; 
      
    sprintf(string,"Maximum rotation velocity? (0<	<%f)\n ",   fMax_vel);
    strcat(string,"\%f");
    win_scanf(string,   &fVel);
    if ( 0 < fVel && fVel< fMax_vel)
    {
       dValarray = (double) fVel;
       if (C843_VEL (id_C843,rot_mot, &dValarray,) == PI_TRUE)
       {
           Pico_micro_param.rotation_velocity = fVel;
           set_config_float("PICO_CFG", "rotation_velocity", Pico_micro_param.rotation_velocity);
           Open_Pico_config_file();
           write_Pico_config_file();
           Close_Pico_config_file();
       }    
    }       
       return D_O_K;
}    


int set_trans_speed(void)
{
    
	 
    int iCmdarray = 10;
    double  dValarray;
    float fMax_vel =  0;//Pico_micro_param.translation_max_velocity;
    static float fVel = 0;
    char *string = NULL;
    
    if(updating_menu_state != 0)	return D_O_K;
    
    C843_qSPA (id_C843, trans_mot, &iCmdarray, &dValarray, NULL, 0);
    
    fMax_vel = (float) dValarray; 
      
    sprintf(string,"Maximum translation velocity? (0<	<%f)\n ",   fMax_vel);
    strcat(string,"\%f");
    win_scanf(string,   &fVel);
    if ( 0 < fVel && fVel< fMax_vel)
    {
       dValarray = (double) fVel;
       if (C843_VEL (id_C843,trans_mot, &dValarray,) == PI_TRUE)
       {
           Pico_micro_param.translation_velocity = fVel;
           set_config_float("PICO_CFG", "translation_velocity", Pico_micro_param.translation_velocity);
           Open_Pico_config_file();
           write_Pico_config_file();
           Close_Pico_config_file();
       }    
       
       return D_O_K;
}    


#endif


/*************PICO functions***********************/


int set_rot_value_pi_c843_dll(float rot_position)
{
    double rot_pos = 0;
    
    
    
    rot_pos = (double) rot_position;
    
    
    if (C843_MOV (id_C843, rot_mot, &rot_pos) != PI_TRUE) return BAD;
    #ifdef _PICO_CFG_H_
    Pico_micro_param.rotation_motor_position = rot_position;
    set_config_float("PICO_CFG", "rotation_motor_position", Pico_micro_param.rotation_motor_position);
    Open_Pico_config_file();
    write_Pico_config_file();
    Close_Pico_config_file();
    #endif
     return D_O_K;
} 



int set_magnet_z_value_pi_c843_dll(float trans_position)
{
    double trans_pos = 0;
    int iCmdarray = 21;
    double dValarray;
    
    //Test maximum range first...try PicoParam instead?
    C843_qSPA (id_C843,trans_mot,&iCmdarray,&dValarray, NULL,0);
    
    if (trans_position < 0 || trans_position > (float)dValarray) return BAD;
    
    trans_pos = (double) trans_position;
    
    if (C843_MOV (id_C843, trans_mot, &trans_pos) != PI_TRUE) return BAD;
    #ifdef _PICO_CFG_H_
    Pico_micro_param.translation_motor_position = trans_position;
    //Check disk writing
    set_config_float("PICO_CFG", "translation_motor_position", Pico_micro_param.translation_motor_position);
    Open_Pico_config_file();
    write_Pico_config_file();
    Close_Pico_config_file();
    #endif
     return D_O_K;
}       
    


int set_rot_value_and_wait_pi_c843_dll(float rot_position)
{
    
    BOOL Is_mov;
       
    
    set_rot_value_pi_c843_dll(rot_position);
    
       
    while (Is_mov == PI_TRUE) C843_IsMoving (id_C843, rot_mot, &Is_mov);
    
    
    return D_O_K;
}       

int set_magnet_z_value_and_wait_pi_c843_dll(float trans_position)
{
    BOOL Is_mov;
       
    set_magnet_z_value_pi_c843_dll(trans_position);   
    while (Is_mov == PI_TRUE) C843_IsMoving (id_C843, trans_mot, &Is_mov);
    
    
    return D_O_K;
}    

int set_rot_ref_value_pi_c843_dll(void)
{
    BOOL Is_move  = PI_TRUE, Is_ref_OK = PI_TRUE, Is_ref  = PI_TRUE;

    C843_IsMoving (id_C843, rot_mot, &Is_move);
    if (Is_move == PI_TRUE) return win_printf_OK("I do not reference while moving");
    
	if (C843_REF (id_C843, rot_mot)!= PI_TRUE)return win_printf("Rotation init position failed");
    while (Is_ref_OK == PI_TRUE) C843_IsReferencing (id_C843, rot_mot,&Is_ref_OK);
    if (C843_IsReferenceOK (id_C843, rot_mot,&Is_ref)!= PI_TRUE)return win_printf("Rotation init position failed");
    
    return D_O_K;
}    

int Stop_motors(void)
{
  if(updating_menu_state != 0)	return D_O_K;

  C843_HLT (id_C843, both_mot);
  return D_O_K;
}


int set_magnet_z_ref_value_pi_c843_dll(void)
{
    BOOL Is_move  = PI_TRUE, Is_ref_OK = PI_TRUE, Is_ref  = PI_TRUE;
    //PENSER A MODIFIER LES BORNES si ne prend pas les bords!
//    if (C843_MNL (id_C843, trans_mot)!= PI_TRUE)return win_printf("Trans init position failed");
//Prendre borne suivant l'orientation
    C843_IsMoving (id_C843, trans_mot, &Is_move);
    if (Is_move == PI_TRUE) return win_printf_OK("I do not reference while moving");
    
	if (C843_REF (id_C843, trans_mot)!= PI_TRUE)return win_printf("Rotation init position failed");
    while (Is_ref_OK == PI_TRUE) C843_IsReferencing (id_C843, trans_mot,&Is_ref_OK);
    if (C843_IsReferenceOK (id_C843, trans_mot,&Is_ref)!= PI_TRUE)return win_printf("Rotation init position failed");
    
    return D_O_K;
}

int go_and_dump_z_magnet_pi_c843_dll(float z)
{
    //DUMP TWICE IN CFG BUT NOT LOG...MODIFY
    set_magnet_z_value_pi_c843_dll(z);
    #ifdef _PICO_CFG_H_
    Open_Pico_config_file();
    set_config_float("PICO_CFG", "translation_motor_position", z);
    write_Pico_config_file();
    Close_Pico_config_file();
    #endif
    return D_O_K;
} 

int go_and_dump_rot_pi_c843_dll(float rot)
{
    set_rot_value_pi_c843_dll(rot);
    #ifdef _PICO_CFG_H_
    Open_Pico_config_file();
    set_config_float("PICO_CFG", "rotation_motor_position", rot);
    write_Pico_config_file();
    Close_Pico_config_file();
    #endif
    return D_O_K;
}

/* /\*    */
/* int set_pi_c843_DLL_magnets_device(void) */
/* { */
/*   set_rot_value = set_rot_value_pi_c843_dll;// */
/*   set_rot_ref_value = set_rot_ref_value_pi_c843_dll;// */
/*   read_rot_value = read_rot_value_pi_c843_dll;// */
/*   set_magnet_z_value = set_magnet_z_value_pi_c843_dll;// */
/*   set_magnet_z_ref_value = set_magnet_z_ref_value_pi_c843_dll;// */
/*   read_magnet_z_value = read_magnet_z_value_pi_c843_dll;// */
/*   set_magnet_z_value_and_wait = set_magnet_z_value_and_wait_pi_c843_dll;// */
/*   set_rot_value_and_wait = set_rot_value_and_wait_pi_c843_dll;// */
/*   set_motors_speed = set_motors_speed_pi_c843_dll; */
/*   get_motors_speed = get_motors_speed_pi_c843_dll; */
/*   go_and_dump_z_magnet = go_and_dump_z_magnet_pi_c843_dll;// */
/*   go_wait_and_dump_z_magnet = go_wait_and_dump_z_magnet_pi_c843_dll; */
/*   go_and_dump_rot = go_and_dump_rot_pi_c843_dll;// */
/*   go_wait_and_dump_rot = go_wait_and_dump_rot_pi_c843_dll;// */
/*   go_wait_and_dump_log_specific_rot = go_wait_and_dump_log_specific_rot_pi_c843_dll; */
/*   go_wait_and_dump_log_specific_z_magnet = go_wait_and_dump_log_specific_z_magnet_pi_c843_dll; */
/*   motors_menu = pi_c843_dll; */
/*   describe_magnets_device = describe_magnets_device_pi_c843_dll;	 */
/*   return 0; */
/* } */
/* *\/ */
//MENU *Speed_motor_menu(void)
//{
//	static MENU mn[32];
//	
//	if (mn[0].text != NULL)	return mn;
//
//	
//   add_item_to_menu(mn, "Set rotation speed", set_rot_speed,   NULL,       0, NULL  );
//   add_item_to_menu(mn, "Set translation speed", set_trans_speed,   NULL,       0, NULL  );
//   
//     
//   return mn;
//}

MENU *C843_magnets_menu(void)
{
  static MENU mn[32];
 
    
  if (mn[0].text != NULL)	return mn;
  
  
    add_item_to_menu(mn,"Init C843", init_C843,NULL,0,NULL);
    add_item_to_menu(mn,"Close C843", close_connection,NULL,0,NULL);
    add_item_to_menu(mn,"Trans pos", what_trans_pos,NULL,0,NULL);
    add_item_to_menu(mn,"Rot pos", what_rot_pos,NULL,0,NULL);
    add_item_to_menu(mn,"Set Trans pos", set_trans_pos,NULL,0,NULL);
    add_item_to_menu(mn,"Set Rot pos", set_rot_pos,NULL,0,NULL);
    add_item_to_menu(mn,"STOP", abrupt_stop,NULL,0,NULL);
    //    add_item_to_menu(mn,"Rot Speed", set_rot_speed,NULL,0,NULL);
    //add_item_to_menu(mn,"Trans Speed", set_trans_speed,NULL,0,NULL);
    
    
    
    add_item_to_menu(mn,"IS rot Moving",	is_motor_moving, NULL ,  MENU_INDEX(ROT_MOTOR) , NULL );
    add_item_to_menu(mn,"IS trans Moving",	is_motor_moving, NULL ,  MENU_INDEX(TRANSLATION_MOTOR) , NULL );
    
    add_item_to_menu(mn,"Rot Home",	set_motor_home, NULL ,  MENU_INDEX(ROT_MOTOR) , NULL );
    add_item_to_menu(mn,"Trans Home",	set_motor_home, NULL ,  MENU_INDEX(TRANSLATION_MOTOR) , NULL );
    
    
    add_item_to_menu(mn,"Relative mvt Rot ",	relative_mov, NULL ,  MENU_INDEX(ROT_MOTOR) , NULL );
    add_item_to_menu(mn,"Relative mvt Trans",	relative_mov, NULL ,  MENU_INDEX(TRANSLATION_MOTOR) , NULL );
    
    add_item_to_menu(mn,"Rot Origine",	origine, NULL ,  MENU_INDEX(ROT_MOTOR) , NULL );
    add_item_to_menu(mn,"Trans Origine",	origine, NULL ,  MENU_INDEX(TRANSLATION_MOTOR) , NULL );
    
    add_item_to_menu(mn,"Rot Get PID",	find_PID_param, NULL ,  MENU_INDEX(ROT_MOTOR) , NULL );
    add_item_to_menu(mn,"Trans Get PID",	find_PID_param, NULL ,  MENU_INDEX(TRANSLATION_MOTOR) , NULL );
    
    
    /*add_item_to_menu(mn,"Rot feedback ON",	set_feedback_loop, NULL ,  MENU_INDEX(ROT_MOTOR) , NULL );
    add_item_to_menu(mn,"Trans feedback ON",	set_feedback_loop, NULL ,  MENU_INDEX(TRANSLATION_MOTOR) , NULL );
    
    add_item_to_menu(mn,"Rot Ct speed ON",	constant_velocity_setting, NULL ,  MENU_INDEX(ROT_MOTOR) , NULL );
    add_item_to_menu(mn,"Trans Ct speed ON",	constant_velocity_setting, NULL ,  MENU_INDEX(TRANSLATION_MOTOR) , NULL );*/
    
    
    add_item_to_menu(mn,"Clear Rot status",	clear_status, NULL ,  MENU_INDEX(ROT_MOTOR) , NULL );
    add_item_to_menu(mn,"Clear Trans status",	clear_status, NULL ,  MENU_INDEX(TRANSLATION_MOTOR) , NULL );
    
    
    add_item_to_menu(mn,"Define Rot home",	define_motor_home, NULL ,  MENU_INDEX(ROT_MOTOR) , NULL );
    add_item_to_menu(mn,"Define Trans home",	define_motor_home, NULL ,  MENU_INDEX(TRANSLATION_MOTOR) , NULL );
    add_item_to_menu(mn,"Stop motors smoothly",	Stop_motors, NULL ,  0 , NULL );

    
    
    
  return mn;
}


int	c843_magnets_main(int argc, char **argv)
{
  if (init_C843() != D_O_K) return win_printf_OK("Failed in C843");
  add_plot_treat_menu_item("C843 ", NULL, C843_magnets_menu(), 0, NULL);
  add_image_treat_menu_item("C843 ", NULL, C843_magnets_menu(), 0, NULL);	
  //init_C843();
  return D_O_K;
}

int	c843_magnets_unload(int argc, char **argv)
{
	remove_item_to_menu(plot_treat_menu, "C843", NULL, NULL);
	return D_O_K;
}

#endif

