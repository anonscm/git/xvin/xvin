/*************************************************************
                   STEMMER IMAGING GmbH                       
--------------------------------------------------------------

	Filename    : icvcdisp.h
	Date        : 12.05.97 15:32:13
	Description	: 
	Created by	: Martin Kersting
	Revision    : 1.1

*************************************************************/
#include "iCVCImg.h"

#ifdef __cplusplus
  #define IMPORT(t) extern "C" t __stdcall
#else
  #define IMPORT(t) t __stdcall
#endif

#ifndef CVCDISPLAY_INCLUDE
  #define CVCDISPLAY_INCLUDE

  // CVC object types
  typedef void* DISPLAY;

  // User Paint Callback
  typedef long ( __stdcall *TUserPaint) (void * pPrivate, 
                                         long ID,
                                         long NumVertices,
                                         POINT *Vertices,
                                         BOOL Dragging,
                                         HDC hDC);
  // left mouse button action
  typedef enum
  {
    LB_NONE       = 0,
    LB_RUBBER     = 1,
    LB_FRAME      = 2,
    LB_AREAMODE   = 3,
    LB_SETORIGIN  = 4,
    LB_DRAWPOINT  = 5,
    LB_DRAWFILL   = 6,
    LB_DRAWLINE   = 7,
    LB_DRAWRECT   = 8
  } LEFTBUTTONACTION;
  // right mouse button action
  typedef enum
  {
    RB_NONE       = 0,
    RB_ZOOM       = 1,
    RB_MENU       = 2
  } RIGHTBUTTONACTION;

  // Display style
  typedef enum
  {
    DS_AUTOSIZE   = 0,
    DS_SUBWINDOW  = 1
  } DISPLAYSTYLE;

  // status line style
  typedef enum
  {
    SL_EMPTY      = 1,
    SL_IMGSIZE    = 2,
    SL_SCALE      = 4,
    SL_CURPOS     = 8,
    SL_VALUE      =16
  } STATUSLINESTYLE;

  // Coordinate Display style
  typedef enum
  {
    CS_NORMAL     = 0,
    CS_CROSSHAIR  = 1
  } CSSTYLE;

  // Notification messages
  //  These are sent in the WParam part of the message sent to HWindow, when the
  //  Updatemessage parameter is different from zero. 
  typedef enum
  {
    IN_IMAGEUPDATE      = 0,  /* New Image data. LParam insignificant */
    IN_RUBBERDRAG       = 2,  /* Rubberband measurement. */
    IN_RUBBERENDDRAG    = 3,  /* Rubberband measurement. */
    IN_RECTAOIDRAG      = 4,  /* Rectaoi is being dragged */
    IN_RECTAOIENDDRAG   = 5,  /* EndDrag of RectAOI */
    IN_ROTAOIDRAG       = 6,  /* Rectaoi is being dragged */
    IN_ROTAOIENDDRAG    = 7,  /* EndDrag of RotAOI */
    IN_IMAGESNAP        = 8,  /* Image is snapped */
    IN_CSCHANGED        = 9,  /* Coordinate system changed interactively */
    IN_CSCLICKED        =10,  /* Coordinate system clicked */
    IN_ENDDRAW          =11,  /* Overlay Painted */
    IN_LABELCLICK       =12,  /* Label Clicked   */
    IN_LABELDRAG        =13,  /* Label is being dragged */
    IN_LABELENDDRAG     =14,  /* Label end drag   */
    IN_USEROBJECTCLICKED=15,  /* User Object clicked  */
    IN_USEROBJECTDRAG   =16,  /* User Object is being dragged  */
    IN_USEROBJECTENDDRAG=17,  /* User Object EndDrag  */
    IN_LAYOUTCHANGED    =18   /* User Object EndDrag  */
  }  NOTIFICATIONMESSAGE;
#endif	// CVCDISPLAY_INCLUDE

IMPORT(BOOL)  ReleaseDisplay          (DISPLAY Display);
IMPORT(BOOL)  ShareDisplay            (DISPLAY Display);
IMPORT(BOOL)  CreateDisplay           (DISPLAY* Display );
IMPORT(BOOL)  SetDisplayWindow        (DISPLAY Display, HWND HWindow );
IMPORT(BOOL)  IsDisplayHandle         (DISPLAY Display );
IMPORT(BOOL)  SetDisplayImage         (DISPLAY Display, IMG Image );
IMPORT(BOOL)  GetDisplayImage         (DISPLAY Display, IMG* Image );
IMPORT(BOOL)  GetDisplayGrabber       (DISPLAY Display, long* Grabber );

IMPORT(BOOL)  SetDisplayGainOffset    (DISPLAY Display, double AGain, long AOffset );
IMPORT(BOOL)  SetDisplayLButtonAction (DISPLAY Display, long AButtonMode );
IMPORT(BOOL)  SetDisplaySelectArea    (DISPLAY Display, TArea Area );
IMPORT(BOOL)  GetDisplaySelectArea    (DISPLAY Display, TArea* Area );
IMPORT(BOOL)  SetDisplayStyle         (DISPLAY Display, long ADispStyle, 
                                       long BL, long BT, long BR, long BB );
IMPORT(BOOL)  SetDisplayBevels        (DISPLAY Display, BOOL ABevelInner, BOOL ABevelOuter );
IMPORT(BOOL)  SetDisplayStatusBar     (DISPLAY Display, long AStatusStyle );
IMPORT(BOOL)  SetDisplayUserStatus    (DISPLAY Display, PCHAR lpszUserStatus);

IMPORT(BOOL)  SetDisplayScrollBars    (DISPLAY Display, BOOL AScrollMode );
IMPORT(BOOL)  SetDisplayPages         (DISPLAY Display, 
                                       long RedPlane, long GreenPlane, long BluePlane);
IMPORT(BOOL)  GetDisplayPages         (DISPLAY Display, 
                                       long* RedPlane, long* GreenPlane, long* BluePlane);
IMPORT(BOOL)  SetDisplayDrawingColor  (DISPLAY Display, BOOL Erase );
IMPORT(BOOL)  DisplayImageCoordinates (DISPLAY Display, BOOL ShowIt );

IMPORT(BOOL)  SetCoordDisplayStyle    (DISPLAY Display, long Style );
IMPORT(OBJ)	  AddDisplayLabel         (DISPLAY Display, PCHAR pstrText, 
                                       BOOL CanDrag, long AColor, long AID, 
                                       long AX, long AY );
IMPORT(BOOL)  DisplayLabelPosition    (DISPLAY Display, long AID, 
                                       long* X, long* Y );
IMPORT(BOOL)  RemoveDisplayLabel      (DISPLAY Display, long AID );
IMPORT(BOOL)  RemoveDisplayLabels     (DISPLAY Display );
IMPORT(BOOL)  HighlightDisplayLabel   (DISPLAY Display, long AID, 
                                       BOOL AHighlight );
IMPORT(OBJ)   AddDisplayUserObject    (DISPLAY Display, void* PPrivate, 
                                       PCHAR Text, 
                                       BOOL CanDrag, BOOL XorOnly,    
                                       long ID, long NumVertices,
                                       POINT* Vertices,
                                       TUserPaint UserPaint);

IMPORT(BOOL)  RemoveDisplayObject     (DISPLAY Display, long AID );
IMPORT(BOOL)  DisplayObjectPosition   (DISPLAY Display, long AID, 
                                       long Index, long* x, long*y );
IMPORT(BOOL)  RemoveDisplayObjects    (DISPLAY Display);
IMPORT(BOOL)  SetDisplayRuler         (DISPLAY Display, BOOL Enabled);
IMPORT(BOOL)  EnableDisplayZoom       (DISPLAY Display, BOOL Enabled);

IMPORT(BOOL)  DisplayStartGrab        (DISPLAY Display );
IMPORT(BOOL)  DisplayStopGrab         (DISPLAY Display );

IMPORT(BOOL)  DisplayMessageHandler   (DISPLAY Display, 
                                       long Msg, long WParam, long LParam );
IMPORT(BOOL)  SetDisplayUpdateMessage (DISPLAY Display, long UpdateMessage );

IMPORT(BOOL)  DisplayImageToClient    (DISPLAY Display, 
                                       long  lXImage, long  YImage,
                                       long* XClient, long* YClient);
IMPORT(BOOL)  DisplayClientToImage    (DISPLAY Display, 
                                       long  XClient, long YClient,
                                       long* XImage,  long *YImage);
IMPORT(BOOL)  SetDisplayDirectDraw    (DISPLAY Display, BOOL ADDEnabled);
IMPORT(BOOL)  GetDisplayDirectDraw    (DISPLAY Display, BOOL* ADDEnabled);
IMPORT(BOOL)  GetDisplayPaintMethod   (DISPLAY Display, long* Method );
IMPORT(BOOL)  GetDisplayDirectDrawObjects   (DISPLAY Display, void** pDD, void** pPrimary, void** pOffscreen, void** pClipper);
IMPORT(BOOL)  SetDisplaySyncRefreshToMonitor(DISPLAY Display, BOOL bSync);
IMPORT(BOOL)  GetDisplaySyncRefreshToMonitor(DISPLAY Display);
IMPORT(BOOL)  GetDisplayZoomState     (DISPLAY Display, long* zx, long* zy, long* zcode); 
IMPORT(BOOL)  SetDisplayZoomState     (DISPLAY Display, long zx, long zy, long zcode); 
IMPORT(BOOL)  SetDisplayRectPercentage(DISPLAY Display, unsigned long lDisplayRectPercentage );
IMPORT(BOOL)  GetDisplayRectPercentage(DISPLAY Display, unsigned long *lDisplayRectPercentage );
IMPORT(BOOL)  SetManualDisplayRefresh (DISPLAY Display, BOOL bEnable );
IMPORT(BOOL)  GetManualDisplayRefresh (DISPLAY Display, BOOL *bEnable );
IMPORT(BOOL)  SetDisplayBackColor     (DISPLAY Display, COLORREF crBackColor );
IMPORT(BOOL)  GetDisplayBackColor     (DISPLAY Display, COLORREF *crBackColor );
IMPORT(BOOL)  SetDisplayZoomFactor    (DISPLAY Display, long zx, long zy, double dAZoomFactor);
IMPORT(BOOL)  GetDisplayZoomFactor    (DISPLAY Display, long* zx, long* zy, double* dAZoomFactor);
IMPORT(BOOL)  GetDisplayPanoramaZoomFactor (DISPLAY Display, double* dAZoomFactor);
