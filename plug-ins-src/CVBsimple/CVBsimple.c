/*
*    Plug-in program for plot treatement in Xvin.
*
*    JFA
*/
#ifndef _CVBSIMPLE_C_
#define _CVBSIMPLE_C_


#include "allegro.h"
#include "winalleg.h"
//#include <windowsx.h>
#define GetPixel GetPixelW
#define SetPixel SetPixelW
#include "wingdi.h"
#undef GetPixel
#undef SetPixel

#define INCLUDE_PIXEL_GET_SET
#include "icvcimg.h"
#include "icvcDisp.h"
#include "icvcDriver.h"
#include "icvcUtilities.h"




#undef INCLUDE_PIXEL_GET_SET

#include "xvin.h"

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "CVBsimple.h"
//place here other headers of this plugin 
# include "allegro.h"
# include "xvin.h"

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "CVBsimple.h"

//#include "XVCVB.h"
IMG CVB_img;
imreg   	*imr_CVB = NULL;
pltreg  	*pr_CVB = NULL;
O_i     	*oi_CVB = NULL;
DIALOG  	*d_CVB = NULL;
IMG     	Img_CVB;
IMG     	Old_Img_CVB;
IMG     	New_Img_CVB;
//DISPLAY 	CVB_Display;
long  	CVB_number_images_buffer = 64;
int 		num_image_CVB = -1;
BOOL 		allegro_display_on = TRUE;
long		lNumBuff = 128;
double	absolute_image_number = 0;
BOOL 		overlay = TRUE;
int 		do_refresh_overlay = 1;
int GO_LIVE_CVB =1;

int freeze_video(void);

#define CVB_FALSE 0
#define CVB_TRUE 1
#define FREEZE 0
#define LIVE 1


int CVB_oi_idle_action(struct  one_image *oi, DIALOG *d)
{
  // we put in this routine all the screen display stuff
  imreg *imr;
  //int redraw_background = 0;
  BITMAP *imb;
  unsigned long t0 = 0;
  //float zo, zm;

  if (d->dp == NULL)    return 1;
  imr = (imreg*)d->dp;        /* the image region is here */
  if (imr->one_i == NULL || imr->n_oi == 0)        return 1;

  t0 = my_uclock();
  if (oi->need_to_refresh & BITMAP_NEED_REFRESH)
    {
      display_image_stuff_16M(imr,d);
      oi->need_to_refresh |= INTERNAL_BITMAP_NEED_REFRESH;
    }



  imb = (BITMAP*)oi->bmp.stuff;
  //redraw_background = show_bead_cross(imb, imr, d);

  //if (redraw_background)  // slow redraw
  //  {
      write_and_blit_im_box( plt_buffer, d, imr);
      //show_bead_cross(plt_buffer, imr, d);
      t0 = my_uclock() - t0;

      
      display_title_message("im %d dt %6.3f ms",oi->im.c_f,  1000*(double)(d->d1)/get_my_uclocks_per_sec());
      //  }
      //else  if (oi->need_to_refresh & INTERNAL_BITMAP_NEED_REFRESH)
      //{
      //screen_used = 1;
      SET_BITMAP_IN_USE(oi);
      acquire_bitmap(screen);
      blit(imb,screen,0,0,imr->x_off //+ d->x
	   , imr->y_off - imb->h + d->y, imb->w, imb->h); 
      release_bitmap(screen);
      //screen_used = 0;
      oi->need_to_refresh &= ~INTERNAL_BITMAP_NEED_REFRESH;
      t0 = my_uclock() - t0;
      
      SET_BITMAP_NO_MORE_IN_USE(oi);
      
      display_title_message("im %d dt %6.3f ms",oi->im.c_f,  1000*(double)(d->d1)/get_my_uclocks_per_sec());
      
      //}
  // this routine switches supress the image idle action, 
  // in particular info display by xvin on image by default 

  return 0;
}


int CVB_before_menu(int msg, DIALOG *d, int c)
{
  /* this routine switches the display flag so that no magic color is drawn 
     when menu are activated */
   
  if (msg == MSG_GOTMOUSE)
    {
      do_refresh_overlay = 0;
      //display_title_message("menu got mouse");
    }
    
  return 0;
}
int CVB_after_menu(int msg, DIALOG *d, int c)
{
  return 0;
}

int CVB_oi_got_mouse(struct  one_image *oi, int xm_s, int ym_s, int mode)
{
  /* this routine switches the display flag so that the magic color is drawn again
     after menu are activated */
  do_refresh_overlay = 1;
  //display_title_message("Image got mouse");
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
// New file saving functions

////////////////////////////////////////////////////////////////////////////////

/** Modified .gr header saver.

IMPORTANT : superseds all previous (bugged) versions of save_image_header.
\param O_i The oi containing the information.
\param char A header text ??
\param int ??
\return int Error code.
\author JF Allemand
*/
int save_image_header_JF(O_i* oi, char *head, int size)
{
  FILE *fp;
  int nh;
  char filename[512];

  if (oi == NULL || oi->dir == NULL || oi->filename == NULL)	return 1;
  build_full_file_name(filename,512,oi->dir,oi->filename);

  fp = fopen(filename,"r+b");
  if (fp == NULL)
    {
      error_in_file("%s\nfile not found!...",filename);
      return 1;
    }
  nh = find_CTRL_Z_offset(oi,fp);

  if (nh != size)
    {
      win_printf("the size of headers disagree!");
      return 1;
    }
	  
  if (fp == NULL)
    {
      error_in_file("%s\nfile not found!...",filename);
      return 1;
    }
	
  fseek(fp,0,SEEK_SET);
  if (fwrite (head,sizeof(char),size,fp) != size)
    {
      win_printf("Pb while writing header!");
      return 1;
    }
  fclose(fp);
  return 0;
}

////////////////////////////////////////////////////////////////////////////////
/** Modified file saver.

IMPORTANT : superseds all previous (bugged) versions of close_movie_on_disk.
\param O_i The oi containing the information.
\param int The number of frames in the movie.
\return int Error code.
\author JF Allemand
*/
int  close_movie_on_disk_JF(O_i *oi, int nf)
{
  char *head, *found, tmp[256];
  int size = 0, nff = -1, cff = -1, i;
  extern char *read_image_header(O_i* oi, int *size);
  head = read_image_header(oi, &size);
  if (head == NULL)  return 1;
  found = strstr(head,"-nf");
  if (found != NULL)    
    {
      sprintf(tmp,"-nf %010d %010d\r\n",nf, oi->im.c_f);
      for (i = 0; (i < 256) && (tmp[i] != 0); i++) found[i] = tmp[i]; 
    }
  else win_printf("did not found -nf symbol");
  sscanf(found,"-nf %d %d",&nff,&cff);

  if (oi->im.record_duration != 0)
    {
      found = strstr(head,"-duration");
      if (found != NULL) 	
	{
      	  sprintf(tmp,"-duration %016.6f \r\n",oi->im.record_duration);
      	  for (i = 0; (i < 256) && (tmp[i] != 0); i++) found[i] = tmp[i]; 
	}
      else win_printf("did not found -duration symbol");
    }
  save_image_header_JF(oi, head,  size);
  if (head) {free(head); head = NULL;}
  return 0;
}

DWORD WINAPI Saving_Movie_Thread(LPVOID lpParam) 
{
  FILE *fp_images = NULL;
  int n_images_saved = 1;
  HWND hWnd;
  imreg *imr;
  O_i *oi_buffer_tape_CVB = NULL;
  int num_files = 0;
  char filenamend[16];
  char common_name[20]="CVB";
  int previous_grab_stat = 0;  
  char fullfile[512];
  register int i;
  char path[512], file[256], *pa; 
  int n_images_to_be_saved = 0;
    
  if(updating_menu_state != 0)	return D_O_K;
	
  overlay = FALSE;
  Sleep(500);
	
	
  hWnd = win_get_window();
  imr = find_imr_in_current_dialog(NULL);
  if (imr == NULL)	return 1;

  movie_buffer_on_disk = 1;
  switch_allegro_font(1);
  pa = (char*)get_config_string("IMAGE-GR-FILE","last_saved",NULL);	
  if (pa != NULL)		extract_file_path(fullfile, 512, pa);
  else				my_getcwd(fullfile, 512);
  strcat(fullfile,"\\");//Jusqu'ici c'est le path 
  num_files = get_config_int("TAPE_SAVE_MODE","filenumber",-1);
  win_scanf("Characteristic string for the files? %s\n Initial number for file%d?",common_name,&num_files);
  strcat(fullfile,common_name);//on rajoute le debut du nom specifique
  sprintf(filenamend,"%04d.gr",num_files);//on rajoute le nombre a incrementer et le format
  strcat(fullfile,filenamend);
  i = file_select_ex("Save real time movie", fullfile, "gr", 512, 0, 0);
  switch_allegro_font(0);
    
  if (i != 0) 	
    {
      set_config_string("IMAGE-GR-FILE","last_saved",fullfile);			
      extract_file_name(file, 256, fullfile);
      extract_file_path(path, 512, fullfile);
      strcat(path,"\\");
	    		
    }
  else 
    {	
      movie_buffer_on_disk = 1;
      return 0;
    }
	
  set_config_int("TAPE_SAVE_MODE","filenumber",num_files);
	
		
  if (oi_buffer_tape_CVB == NULL)
    {
      if((oi_buffer_tape_CVB = (O_i *)calloc(1,sizeof(O_i))) == NULL)	
	return win_printf("Our are out of memory guy");
      if (init_one_image(oi_buffer_tape_CVB, oi_CVB->im.data_type)) 		return win_printf("You have a little problem guy");

      uns_oi_2_oi(oi_buffer_tape_CVB, oi_CVB);
      inherit_from_im_to_im(oi_buffer_tape_CVB, oi_CVB);
    }
	
  while (keypressed()!=TRUE)
    {   if (mouse_b & 1)
  
      {     
	draw_bubble(screen,0,550,100,"%d",num_files);     
    		
	fullfile[0] = '\0';
	num_files = get_config_int("TAPE_SAVE_MODE","filenumber",-1);
	if (num_files < 0) num_files = 0;
	sprintf(file,"%s%04d.gr",common_name,num_files++);//on rajoute le nombre a incrementer et le format
	strcat(fullfile,path);
	strcat(fullfile,file);
		
	oi_buffer_tape_CVB->im.movie_on_disk = 1;
	oi_buffer_tape_CVB->dir = strdup(path);
	oi_buffer_tape_CVB->filename = strdup(file);
	oi_buffer_tape_CVB->im.time = 0;
	oi_buffer_tape_CVB->im.n_f= 0;
	oi_buffer_tape_CVB->im.data_type = oi_CVB->im.data_type;
	oi_buffer_tape_CVB->im.nx = oi_CVB->im.nx;
	oi_buffer_tape_CVB->im.ny = oi_CVB->im.ny;
 		
 		
	set_image_starting_time(oi_buffer_tape_CVB);
	set_image_ending_time (oi_buffer_tape_CVB);
		
	save_one_image(oi_buffer_tape_CVB, fullfile);/***sauve en tete et les parametres**/
	//win_printf("After image");
	set_config_int("TAPE_SAVE_MODE","filenumber",num_files);
    		
    	
	fp_images = fopen (fullfile, "ab");
    	
	if ( fp_images == NULL ) 	
	  {        
	    win_printf("Cannot open the file!");
	    movie_buffer_on_disk = 0;
	    return 1;
	  }
        
	//    	previous_image_for_tape_saving = 0;
        	
	G2GetGrabStatus (Img_CVB, GRAB_INFO_NUMBER_IMAGES_AQCUIRED, &absolute_image_number);
	//RBBufferSeq ( Img_CVB, lSequenceIndex, &lBufferIndex); 
	//switch_frame(oi_CVB,(int) error%lNumBuff); 
	//draw_bubble(screen,0,550,75,"error  %f  i %d index %d",absolute_image_number ,i,(int)lBufferIndex);
            
	previous_grab_stat = (int) absolute_image_number; //A VOIR
	n_images_saved = 0;
    		
	set_image_starting_time(oi_buffer_tape_CVB);
		
	overlay = TRUE;
	Sleep(50);
	while (!(mouse_b & 2))//stop saving with the mouse right button 
	  {
	    G2GetGrabStatus (Img_CVB, GRAB_INFO_NUMBER_IMAGES_AQCUIRED, &absolute_image_number);
	    n_images_to_be_saved = (int)absolute_image_number - previous_grab_stat - n_images_saved;
        	
        
	    if (n_images_to_be_saved > 0)
	      {
		//Passer avec RBBufferSeq ( IMG Image, long lSequenceIndex, long &lBufferIndex); 

		//win_printf("stats.CurrentFrameSeqNum %d n_images_to_be_saved %d nframes_in_IFC_buffer %d",stats.CurrentFrameSeqNum,n_images_to_be_saved,nframes_in_IFC_buffer);
		fwrite (oi_CVB->im.mem[((int)absolute_image_number - n_images_to_be_saved)%((int)lNumBuff) ],ImageDimension (Img_CVB) * ImageDatatype(Img_CVB, 0)/8,oi_CVB->im.nx * oi_CVB->im.ny,fp_images);
		n_images_saved++;
		draw_bubble(screen,0,150,100,"Currently saving image %d",n_images_saved+1);
		if (n_images_to_be_saved > lNumBuff)
		  { 
		    fclose(fp_images); 
		    set_image_ending_time (oi_buffer_tape_CVB);
		    overlay = FALSE;
		    Sleep(50);
		    //win_printf("Saved %d",n_images_saved);
		    overlay = TRUE;
		    Sleep(50);
		    close_movie_on_disk_JF(oi_buffer_tape_CVB,n_images_saved);
		    freeze_video();
		    return win_printf("Could not go fast enough!");
		  }
	      }
	  }
   		
	fclose(fp_images); 
	set_image_ending_time (oi_buffer_tape_CVB);
	overlay = FALSE;
	Sleep(50);
	//win_printf("Saved %d",n_images_saved);
	overlay = TRUE;
	Sleep(50);
	close_movie_on_disk_JF(oi_buffer_tape_CVB,n_images_saved);
      }
    }
  movie_buffer_on_disk = 0;
  return 0;
}




DWORD WINAPI RecordFiniteMovieThread( LPVOID lpParam )
{
  O_i* movie_oi= NULL;
  int i, k , iStarting_image;
  int iImages_in_movie;
  //O_p *op = NULL;
  double dNimages_Acquired;
  long lDatatype;

  movie_oi = (O_i *)lpParam;

  if (movie_oi == NULL) return win_printf_OK("Movie could not be created");
  iImages_in_movie = movie_oi->im.n_f;
  
  //  op = movie_oi->o_p[0];
  ImageDatatype(Img_CVB,lDatatype);
  while (!(mouse_b & 1));

  G2GetGrabStatus (Img_CVB, GRAB_INFO_NUMBER_IMAGES_AQCUIRED , &dNimages_Acquired);

  iStarting_image = (int)dNimages_Acquired;

  for (i = iStarting_image , k = 0; i < iStarting_image + iImages_in_movie ; i++ , k++ )
   {
     while ((int)dNimages_Acquired < iStarting_image+ k)
       G2GetGrabStatus (Img_CVB, GRAB_INFO_NUMBER_IMAGES_AQCUIRED , &dNimages_Acquired);
     
     memcpy(movie_oi->im.mem[k]/*i-iStarting_image]*/,oi_CVB->im.mem[(iStarting_image + k)%((oi_CVB->im.n_f == 0 )? 1 : oi_CVB->im.n_f)],oi_CVB->im.nx*oi_CVB->im.ny*(lDatatype/8+(lDatatype%8 == 0)? 0 :1));
     //op->dat[0]->xd[i-iStarting_image] = i-iStarting_image;
     //op->dat[0]->yd[i-iStarting_image] = ;
   }
  win_printf("Finished");
  return D_O_K;
}

int iCreate_Record_Finite_Movie_Thread(O_i*oi)
{
    HANDLE hThread = NULL;
    DWORD dwThreadId;

    hThread = CreateThread( 
            NULL,              // default security attributes
            0,                 // use default stack size  
            RecordFiniteMovieThread,// thread function 
            (void*)oi,       // argument to thread function 
            0,                 // use default creation flags 
            &dwThreadId);      // returns the thread identifier 

    if (hThread == NULL) 	return win_printf_OK("No thread created");
    SetThreadPriority(hThread,  THREAD_PRIORITY_TIME_CRITICAL);
    return 0;
}

int iRecord_Finite_Movie(void)
{
  int i;
  static int iImages_in_movie = 128;
  O_i *movie_oi = NULL;
 
  if(updating_menu_state != 0)	return D_O_K;

  i = win_scanf("You want to save a movie of how many images?%d \n Click on MOUSE Button to Start" ,&iImages_in_movie);
  if (i == CANCEL) return D_O_K;
  if (oi_CVB == NULL) return win_printf_OK("No CVB image");
 
  movie_oi = create_and_attach_movie_to_imr (imr_CVB, oi_CVB->im.nx, oi_CVB->im.ny, oi_CVB->type, iImages_in_movie);
  if (movie_oi == NULL) return win_printf_OK("Movie could not be created");

  uns_oi_2_oi(movie_oi, oi_CVB);
  inherit_from_im_to_im(movie_oi, oi_CVB);

  create_and_attach_op_to_oi (movie_oi , iImages_in_movie ,iImages_in_movie , 0 , 0);
    
  iCreate_Record_Finite_Movie_Thread(movie_oi);

  return D_O_K;
}



/*************************************************END OF RECORD PART*********************************************************************************/


int set_oi_pointers(O_i *oi, void * p_CVB_data_pointer, int x_inc, int y_inc)
{
	
  register int i = 0;
  int data_len = 1, nf=0;
  void *buf = NULL;
  int nx = -1; 
  int ny = -1;
  int type = -1;
	
  if (oi == NULL) return win_printf_OK("Oi NULL");
  data_len = ImageDimension (Img_CVB) * x_inc; //CHECK valid even for rgb
	
  nx = ImageWidth (Img_CVB);
  ny = ImageHeight (Img_CVB);

  type = oi->im.data_type ; 
	
  if (oi->im.pixel != NULL && oi->im.ny != 0 && oi->im.nx != 0) 		buf = oi->im.mem[0]; //oi->im.pixel[0].ch;
	
  nf = (oi->im.n_f > 0) ? oi->im.n_f : 1;
  oi->im.pixel = (union pix *)realloc(oi->im.pixel, ny*nf * sizeof(union pix));
  if (oi->im.pixel == NULL) return xvin_error(Out_Of_Memory);
  //win_printf("Oi  pixel NULL");
  if (nf > 1) 
    {
      //win_printf_OK("nf %d \n data length %d \n Type %d", oi->im.n_f , data_len , oi->im.data_type);	
      oi->im.pxl = (union pix **)realloc(oi->im.pxl, nf*sizeof(union pix*));
      oi->im.mem = (void **)realloc(oi->im.mem, nf*sizeof(void*));
      if (oi->im.pxl == NULL  || oi->im.mem == NULL) return xvin_error(Out_Of_Memory);
      //	win_printf_OK("Oi CVB not NULL");		
		  
    }
	
  else oi->im.mem = (void **)realloc(oi->im.mem, sizeof(void*));
  //win_printf_OK("Oi CVB mem NULL");
  oi->im.m_f = nf;
  buf = p_CVB_data_pointer; // already allocated by CVB realloc(buf, nf*nx*ny * data_len * sizeof(char));
  if (oi->im.pixel == NULL || buf == NULL)	return xvin_error(Out_Of_Memory);
  oi->im.mem[0] = buf;
  for (i=0 ; i< ny*nf ; i++)
    {
      switch (type)
	{
	case IS_CHAR_IMAGE:
	  oi->im.pixel[i].ch = (unsigned char*)buf;
			
	  break;
	case IS_RGB_PICTURE:
	  oi->im.pixel[i].ch = (unsigned char*)buf;
			
	  break;			
	case IS_RGBA_PICTURE:
	  oi->im.pixel[i].ch = (unsigned char*)buf;
			
	case IS_RGB16_PICTURE:
	  oi->im.pixel[i].in = (short int*)buf;
			
	  break;			
	case IS_RGBA16_PICTURE:
	  oi->im.pixel[i].in = (short int*)buf;
			
	  break;			
	case IS_INT_IMAGE:
	  oi->im.pixel[i].in = (short int *)buf;
			
	  break;
	case IS_UINT_IMAGE:
	  oi->im.pixel[i].ui = (unsigned short int *)buf;
			
	  break;
	case IS_LINT_IMAGE:
	  oi->im.pixel[i].li = (int *)buf;
			
	  break;
	case IS_FLOAT_IMAGE:
	  oi->im.pixel[i].fl = (float *)buf;
			
	  break;
	case IS_COMPLEX_IMAGE:
	  oi->im.pixel[i].fl = (float *)buf;
			
	  break;			
	case IS_DOUBLE_IMAGE:
	  oi->im.pixel[i].db = (double *)buf;
			
	  break;
	case IS_COMPLEX_DOUBLE_IMAGE:
	  oi->im.pixel[i].db = (double *)buf;
			
	  break;			

	};
      if ( (i % ny == 0) && (oi->im.n_f > 0))
	{
	  oi->im.pxl[i/ny] = oi->im.pixel + i;
	  oi->im.mem[i/ny] = (void*)buf;
	}
      buf += nx * data_len;
    }
  oi->im.nxs = oi->im.nys = 0;
		
  if (type == IS_COMPLEX_IMAGE  || type == IS_COMPLEX_DOUBLE_IMAGE)	
    oi->im.mode = RE;

  return D_O_K;
}

	
	
	

int add_CVB_oi_2_imr (imreg *imr,O_i* oi)
{
  long lInfo = -1;
  void* CVB_linear_pointer = NULL;
  long XInc = -1; 
  long YInc = -1;
  long xpix,ypix;
  int type;
  int testXVPAT,testYVPAT;
  
  if ( imr == NULL ) return win_printf_OK("IMR NULL");//xvin_error(Wrong_Argument);
  //if (CVB_linear_pointer) return xvin_error(Wrong_Argument);
  testXVPAT = AnalyseXVPAT(Img_CVB, 0, &xpix);  
  testYVPAT = AnalyseYVPAT(Img_CVB, 0, &ypix);  
  if  (testXVPAT == XVPAT_LINEAR_WITH_DATATYPE ) 
    {
      //win_printf("PAT perfect with increment %d",(int)pix); 
      if (GetLinearAccess(Img_CVB, 0, &CVB_linear_pointer, &XInc, &YInc)!= CVB_TRUE) return win_printf_OK("Linear acces failed"); 
      //win_printf("ImageBits %d",CVB_linear_pointer);
      //win_printf("XInc %d \n YInc %d...to be exploited",XInc,YInc);
    }
  else return win_printf_OK("NOT LINEAR ACCESS IMAGE");
  
  
  if 	  (ImageDimension (Img_CVB) == 1 && ImageDatatype(Img_CVB, 0) == 8) type = IS_CHAR_IMAGE;
  else if (ImageDimension (Img_CVB) == 3 && ImageDatatype(Img_CVB, 0) == 8) type = IS_RGB_PICTURE;
  else if (ImageDimension (Img_CVB) == 1 && ImageDatatype(Img_CVB, 0) == 16) type = IS_UINT_IMAGE;
  else if (ImageDimension (Img_CVB) == 1 && ImageDatatype(Img_CVB, 0) == 10) type = IS_UINT_IMAGE;
  else if (ImageDimension (Img_CVB) == 1 && ImageDatatype(Img_CVB, 0) == 32) type = IS_INT_IMAGE;
  else return win_printf_OK("FORMAT NOT IMPLEMENTED YET");
	
  if (RBNumBuffer ( Img_CVB , RINGBUFFER_NUMBUFFER_CMD_GET, &lInfo, NULL) < 0)return win_printf_OK("Ring Numb pb! %d",(int)lInfo);; 
  //win_printf("before oi_track creation");
  if (oi == NULL)
    {
      if((oi = (O_i *)calloc(1,sizeof(O_i))) == NULL)	return win_printf_OK("Calloc oi pb!");
    }
  //win_printf("Init NOT done \n buffer size %d",(int)lInfo);
  if (init_one_image(oi, type)) 		return win_printf_OK("Init pb!");
  oi->im.nx = ImageWidth (Img_CVB);
  oi->im.ny = ImageHeight (Img_CVB);
  oi->im.nxs = oi->im.nys = 0;
  oi->im.nxe = oi->im.nx;
  oi->im.nye = oi->im.ny;
  
  
  set_oi_source(oi , "CVB");
  
  oi->im.n_f = (int)lInfo;
  //win_printf("Init done \n buffer size %d",(int)lInfo);
  oi->im.data_type = type ;
  //win_printf("CVB Type %d \n Type %d \n dimension %d \n type %d",oi->im.data_type,type, ImageDimension (Img_CVB),ImageDatatype(Img_CVB, 0));
  //win_printf("Source %s",get_oi_source(oi_CVB));
  if (add_image(imr, type, (void*)oi) != 0) 
    {
      //remove_image (imr_CVB, IS_CHAR_IMAGE, (void *) imr_CVB->o_i[0]);
      win_printf("Pb in Image added");
    }
  set_oi_pointers(oi,CVB_linear_pointer, xpix , ypix);
  if ((ImageWidth (Img_CVB) <1024) && (ImageHeight (Img_CVB)<1024))
    {
      oi->width  = ((float)ImageWidth (Img_CVB))/512;
      oi->height = ((float)ImageHeight (Img_CVB))/512;
    }
  else
    {
      oi->width  = ((float)ImageWidth (Img_CVB))/1024;
      oi->height = ((float)ImageHeight (Img_CVB))/1024;
    }
  oi->iopt2 &= ~X_NUM;		oi->iopt2 &= ~Y_NUM;		
  set_zmin_zmax_values(oi, 0, 255);
  set_z_black_z_white_values(oi, 0, 255);
	
  //oi_CVB->need_to_refresh |= ALL_NEED_REFRESH;
  //win_printf("XInc %d \n YInc %d...to be exploited",XInc,YInc);	
  return 0;
}

char	*do_load_grabber_driver(void)
{
  register int i = 0;
  char path[512], file[256];
  static char fullfile[512], *fu = NULL;
  int full=1;

  if (fu == NULL)
    {
      fu = (char*)get_config_string("Grabberdriver","last_loaded",NULL);
      if (fu != NULL)	
	{
	  strcpy(fullfile,fu);
	  fu = fullfile;
	}
      else
	{
	  my_getcwd(fullfile, 512);
	  strcat(fullfile,"\\");
	}
    }
#ifdef XV_WIN32
  if (full) // full_screen
    {
#endif
      switch_allegro_font(1);
      i = file_select_ex("Load driver (*.vin)", fullfile, "vin", 512, 0, 0);
      switch_allegro_font(0);
#ifdef XV_WIN32
    }
  else
    {
      extract_file_path(path, 512, fullfile);
      put_backslash(path);
      if (extract_file_name(file, 256, fullfile) == NULL)
	fullfile[0] = file[0] = 0;
      else strcpy(fullfile,file);
      fu = DoFileOpen("Load Grabber Driver (*.vin)", path, fullfile, 512, "Driver Files (*.vin)\0*.vin\0All Files (*.*)\0*.*\0\0", "vin");
      i = (fu == NULL) ? 0 : 1;
      win_printf("loading %d\n%s",backslash_to_slash(fullfile));
      slash_to_backslash(fullfile);
    }
#endif
  if (i != 0) 	
    {
      fu = fullfile;
      set_config_string("Grabberdriver","last_loaded",fu);
      return fullfile;;
    }
  return NULL;
}



int initialize_CVB(void)
{   
  long value = -1;
  char message[256];

  if(updating_menu_state != 0)	return D_O_K;
 
  if (LoadImageFile (do_load_grabber_driver(), &Img_CVB) == FALSE) win_printf("This board is not in the computer"); 
  if (RBNumBuffer ( Img_CVB, RINGBUFFER_NUMBUFFER_CMD_SET , &lNumBuff,&Img_CVB) < 0) return win_printf_OK("Number images in buffer failed"); 
  return D_O_K;   
} 

/* long long MyAcquisitionPeriod(void) */
/* { */
/*   //   register int i; */
/*   long long t0; */
/*   double dTotalImageNumber = 0; */
/*   long lBufferIndex = 0; */
/*   double dNumberOfPendingImages = -1; */


/*   G2GetGrabStatus (Img_CVB, GRAB_INFO_NUMBER_IMAGES_PENDIG, &dNumberOfPendingImages); */
/*   //if (RBBufferSeq ( Img_CVB, 0 , &lBufferIndex) < 0) return win_printf_OK("Error");  */
/*   //draw_bubble(screen,0,550,75,"index %f",lBufferIndex); */
/*   dTotalImageNumber = dNumberOfPendingImages + 1; */
	
		
/*   // wait the next frame acquired */
/*   while(dTotalImageNumber > dNumberOfPendingImages) */
/*     {G2GetGrabStatus (Img_CVB, GRAB_INFO_NUMBER_IMAGES_PENDIG, &dNumberOfPendingImages); */
/*     //draw_bubble(screen,0,550,125,"index %f",dNumberOfPendingImages); */
/*     } */
/*   t0 = my_ulclock(); */
    
/*   while(dTotalImageNumber +10 > dNumberOfPendingImages) */
/*     {G2GetGrabStatus (Img_CVB, GRAB_INFO_NUMBER_IMAGES_PENDIG, &dNumberOfPendingImages); */
/*     //draw_bubble(screen,0,550,125,"index %f",dNumberOfPendingImages); */
      
/*     } */
/*   //draw_bubble(screen,0,550,75,"index %f",lBufferIndex); */
/*   t0 = my_ulclock() - t0; */
/*   return t0/10;  */
/* } */



int freeze_video(void)
{
  imreg *imr;
  if(updating_menu_state != 0)	return D_O_K;
  allegro_display_on = FALSE;
  GO_LIVE_CVB = FREEZE;
  Sleep(100);
  if (G2Freeze (Img_CVB, CVB_TRUE) <0) return win_printf_OK("Freeze failed");

  imr = find_imr_in_current_dialog(NULL);  
  imr->one_i->need_to_refresh &= BITMAP_NEED_REFRESH;  
  find_zmin_zmax(imr->one_i);
  refresh_image(imr, UNCHANGED); 
  return 0;
}

int  iWhat_to_perform_when_new_image(double dNimages_acquired)
{
  switch_frame(oi_CVB,((int)dNimages_acquired)%oi_CVB->im.n_f);

  return D_O_K;
}



DWORD WINAPI G2Wait_Thread(LPVOID lpParam) 
{
  double dNimages_acquired;

  if (Img_CVB == NULL) return win_printf_OK("No Img CVB");

  while (GO_LIVE_CVB == LIVE)
    {
      if(G2Wait(Img_CVB) >= 0) 
	{
	  G2GetGrabStatus (Img_CVB, GRAB_INFO_NUMBER_IMAGES_AQCUIRED , &dNimages_acquired);

	  iWhat_to_perform_when_new_image(dNimages_acquired);
	}
    }
  return D_O_K;
}


int iCreate_G2Wait_Thread(void)
{
  HANDLE hThread = NULL;
  DWORD dwThreadId;

  hThread = CreateThread( 
			 NULL,             // default security attributes
			 0,                // use default stack size  
			 G2Wait_Thread,// thread function 
			 NULL,       	// argument to thread function 
			 0,                // use default creation flags 
			 &dwThreadId);     // returns the thread identifier 

  if (hThread == NULL) 	win_printf_OK("No thread created");
  SetThreadPriority(hThread,  THREAD_PRIORITY_TIME_CRITICAL);
  return 0;
}

int iCreate_Saving_Movie_Thread(void)
{
  HANDLE hThread = NULL;
  DWORD dwThreadId;

  if(updating_menu_state != 0)	return D_O_K;

  hThread = CreateThread( 
			 NULL,             // default security attributes
			 0,                // use default stack size  
			 Saving_Movie_Thread,// thread function 
			 NULL,       	// argument to thread function 
			 0,                // use default creation flags 
			 &dwThreadId);     // returns the thread identifier 

  if (hThread == NULL) 	win_printf_OK("No thread created");
  SetThreadPriority(hThread,  THREAD_PRIORITY_TIME_CRITICAL);
  return 0;
}


/* int iCreate_Finite_Image_Number_Saving_Movie_Thread(void) */
/* { */
/*   HANDLE hThread = NULL; */
/*   DWORD dwThreadId; */
/*   static  int iNimages_to_save = 256; */

/*   if(updating_menu_state != 0)	return D_O_K; */

/*   win_scanf("You want to save how many images?%d",&iNimages_to_save); */

/*   hThread = CreateThread(  */
/* 			 NULL,             // default security attributes */
/* 			 0,                // use default stack size   */
/* 			 Finite_Image_Number_Saving_Movie_Thread,// thread function  */
/* 			 (void *)&iNimages_to_save,       	// argument to thread function  */
/* 			 0,                // use default creation flags  */
/* 			 &dwThreadId);     // returns the thread identifier  */

/*   if (hThread == NULL) 	win_printf_OK("No thread created"); */
/*   SetThreadPriority(hThread,  THREAD_PRIORITY_TIME_CRITICAL); */
/*   return 0; */
/* } */


int CVB_live(void)
{	
  int is_live = 0;

  if(updating_menu_state != 0)	return D_O_K;
  
  is_live  = 1;
  allegro_display_on = TRUE;
  
  if (G2Grab(Img_CVB) <0) return win_printf_OK("Grab could not start");
  GO_LIVE_CVB = LIVE;
  iCreate_G2Wait_Thread();
  
  return D_O_K;//live_video(-1);	
}

MENU *CVB_image_menu(void)
{
  static MENU mn[32];
    
  if (mn[0].text != NULL)	return mn;

  add_item_to_menu(mn,"Init CVB", initialize_CVB ,NULL,0,NULL);
  add_item_to_menu(mn,"Live CVB", CVB_live ,NULL,0,NULL);
  add_item_to_menu(mn,"Freeze CVB", freeze_video ,NULL,0,NULL);
  add_item_to_menu(mn,"Tape CVB", iCreate_Saving_Movie_Thread ,NULL,0,NULL);
  add_item_to_menu(mn,"Finite Tape CVB", iRecord_Finite_Movie , NULL, 0, NULL);

  return mn;
    
}


int init_image_source(void)
{
  imreg *imr; 
  imr = create_and_register_new_image_project( 0,   32,  900,  668);
  if (imr == NULL) return win_printf_OK("Patate t'es mal barre!");
  //  add_image_treat_menu_item ( "CVB", NULL, CVB_image_menu(), 0, NULL);
  initialize_CVB(); // Initializes CVB 
  add_CVB_oi_2_imr (imr,NULL);
  //win_printf("Number of images %d",imr->n_oi);
  remove_from_image (imr, imr->o_i[0]->im.data_type, (void *)imr->o_i[0]);
  //imr_CVB = NULL;
  
	
  return 0;
}

/* MENU *CVBsimple_plot_menu(void) */
/* { */
/* 	static MENU mn[32]; */

/* 	if (mn[0].text != NULL)	return mn; */
/* 	add_item_to_menu(mn,"data set rescale in Y", do_CVBsimple_rescale_data_set,NULL,0,NULL); */
/* 	add_item_to_menu(mn,"plot rescale in Y", do_CVBsimple_rescale_plot,NULL,0,NULL); */
/* 	return mn; */
/* } */

int	CVBsimple_main(int argc, char **argv)
{
  imreg *imr = NULL;
  pltreg *pr = NULL;
  O_p *op;
  O_i *oi;
  //int init_track_info(void);
  //  extern O_i *oi_CVB;



    // if a movie is load already we use this movie as a data source 
  imr = find_imr_in_current_dialog(NULL);
  if (imr == NULL)    init_image_source();
  else
    {
        if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)
	  init_image_source();
	if (oi->im.movie_on_disk == 0 || oi->im.n_f < 2)
	  init_image_source();

    }
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)
    return win_printf_OK("You must load a movie in the active window");

  // We create a plot region to display bead position, timing  etc.
  pr = create_hidden_pltreg_with_op(&op, 4096, 4096, 0,"Tracking status");
  if (pr == NULL)  win_printf_OK("Could not find or allocte plot region!");
  pr_CVB = pr;
  //  add_plot_treat_menu_item ( "CVB simple", NULL, CVB_plot_menu(), 0, NULL);
  
  //if (op == NULL)  win_printf_OK("Could not find or allocate plot !");
  //refresh_plot(pr,0);
  switch_project_to_this_imreg(imr);
  broadcast_dialog_message(MSG_DRAW,0);    


  //we attach image menu
  if (imr_CVB == NULL)
    {
      add_image_treat_menu_item ( "CVB simple", NULL, CVB_image_menu(), 0, NULL);
      
    }

  imr_CVB = imr;
  oi_CVB = imr->one_i;
	
/*   d_CVB = find_dialog_associated_to_imr(imr_CVB, NULL); */
/*   dtid.imr = imr; */
/*   dtid.pr = pr; */
/*   dtid.oi = oi_CVB; */
/*   dtid.op = op; */
/*   dtid.dimr = d_CVB; */
/*   dtid.dpr = find_dialog_associated_to_pr(pr_CVB, NULL); */
/*   dtid.dbid = &bid; */
  before_menu_proc = CVB_before_menu;
  after_menu_proc = CVB_after_menu;
  oi_CVB->oi_got_mouse = CVB_oi_got_mouse;
    //  create_display_thread();
  //return D_O_K;
  //win_printf("END ATTRIB");
  //init_track_info();
  //go_track = TRACK_ON;
  //create_tracking_thread(&dtid);
  //  atexit(stop_source_thread);
  //general_end_action = source_end_action;
  //general_idle_action = source_idle_action;
  
  add_image_treat_menu_item ( "CVBsimple", NULL, CVB_image_menu(), 0, NULL);
  return D_O_K;
}

/* int	CVBsimple_unload(int argc, char **argv) */
/* { */
/* 	remove_item_to_menu(plot_treat_menu, "CVBsimple", NULL, NULL); */
/* 	return D_O_K; */
/* } */
#endif

