/*******************************************************************************
                         STEMMER Imaging GmbH
--------------------------------------------------------------------------------

  Datei    : ICVCFilter.h
  Autor    : 
  Datum    : 02.06.98
  Text     : Definitionen zur CVCFilter DLL
  Revision :

*******************************************************************************/
#include <windows.h>
#include "iCVCImg.h"

#define IMPORT(t) extern "C" t __stdcall

#ifndef FILTER_INCLUDE
  #define FILTER_INCLUDE

  // filter object types
  typedef struct  
  {
    OBJ Reserved;         // Reserved for internal use 
    long cLLTT;		
    long c0LTT;
    long c00TT;
    long c0RTT;
    long cRRTT;
    long cLL0T;
    long c0L0T;
    long c000T;
    long c0R0T;
    long cRR0T;
    long cLL00;
    long c0L00;
    long c0000;
    long c0R00;
    long cRR00;
    long cLL0B;
    long c0L0B;
    long c000B;
    long c0R0B;
    long cRR0B;
    long cLLBB;
    long c0LBB;
    long c00BB;
    long c0RBB;
    long cRRBB;
    long Offst;
  } TFilterDef;
 
#endif	// FILTER_INCLUDE

IMPORT(BOOL)  FilterLaplace       (IMG I, IMG& O );
IMPORT(BOOL)  FilterSharpen       (IMG I, IMG& O );
IMPORT(BOOL)  FilterLow2x2        (IMG I, IMG& O );
IMPORT(BOOL)  FilterLow3x3        (IMG I, IMG& O );
IMPORT(BOOL)  FilterLow5x5        (IMG I, IMG& O );
IMPORT(BOOL)  FilterDilate        (IMG I, IMG& O );
IMPORT(BOOL)  FilterErode         (IMG I, IMG& O );
IMPORT(BOOL)  FilterEdge2x2       (IMG I, IMG& O );
IMPORT(BOOL)  FilterEdge3x3       (IMG I, IMG& O );
IMPORT(BOOL)  FilterPyramid3x3    (IMG I, IMG& O );
IMPORT(BOOL)  FilterPyramid4x4    (IMG I, IMG& O );
IMPORT(BOOL)  FilterPyramid5x5    (IMG I, IMG& O );
IMPORT(BOOL)  FilterUser2x2       (IMG I, TFilterDef& FilterDef, IMG& O );
IMPORT(BOOL)  FilterUser3x3       (IMG I, TFilterDef& FilterDef, IMG& O );
IMPORT(BOOL)  FilterUser5x5       (IMG I, TFilterDef& FilterDef, IMG& O );
IMPORT(long)  ButterWorth         (IMG ImgIn, short HighPass, double Gain, long AddOffset, long Order, double TC, IMG& ImgOut);
