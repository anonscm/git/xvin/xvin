#pragma once
PXV_FUNC(int, do_select_events_man_rescale_plot, (void));
PXV_FUNC(MENU*, select_events_man_plot_menu, (void));
PXV_FUNC(int, do_select_events_man_rescale_data_set, (void));
PXV_FUNC(int, select_events_man_main, (int argc, char **argv));
PXV_FUNC(int,do_select_send_events_to_file,(void));
int find_selected_mean_max(pltreg *pr, int x_0, int y_0, DIALOG *d);
