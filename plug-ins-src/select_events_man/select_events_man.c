/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _SELECT_EVENTS_MAN_C_
#define _SELECT_EVENTS_MAN_C_

#include <allegro.h>
#include "xvin.h"
#include "../../../plug-ins-src/trackBead/select_rec.h"
//#include "xv_plugins.c"

/* If you include other regular header do it here*/


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 #include "select_events_man.h"

 static char fout_event_mean[64];
 static int *define_fout_mean_max=NULL;
 // filter parameter
 static int maxs=128,min=16,s_noise=256 ;
 static float p=10,mu=1.2,n_mu_l=1.1;
 static int  do_segments_min_mean(pltreg *pr, O_p *op, d_s *dsi, int n_seg, float *x, float *y, char *type);
 static int remove_data_set_after_filtering=1;
 static int save_events_to_file=1;
 static char select_events_man[20]="select_events_man";

int change_file_name(void){
  if (updating_menu_state==0) {  strcpy(&fout_event_mean,save_one_file_config("choose were to save  your events _ do not CANCEL !", NULL, "events.txt", "txt\0*.txt*\0All file\0*.*\0\0", "DNA_MANIPULATION", "last_saved"));
  return 0;
}
return D_O_K;
}


 int  change_NL_parameters(void){
   if(updating_menu_state == 0){
   int i;
   i = win_scanf("Non linear filter expecting flat and slope events."
  "Weighting by  (\\chi^2/\\sigma^2)^{-p} using forward/backward algorithm.\n"
  "Define the bounds on the # points used per fit w= (2m + 1):\n"
  "max(m) = %4d min(m) m=%4d\n"
  "the sigma of noise is compute automatically over %4d points\n"
  "the weighting power p= %10f\n"
  "Multiplicative factor favoring long average %4f\n"
  "Multiplicative factor on noise %4f\n"
  "Remove data set after filtering ? %d\n"
  "Save events to file ? %d\n"

  ,&maxs,&min,&s_noise,&p,&mu,&n_mu_l,&remove_data_set_after_filtering,&save_events_to_file); /* don't forget the &*/

if (i == WIN_CANCEL)	return OFF;
}
   return 0;
 }

//ajouter : demander le fichier de sortie
 int 	do_select_send_events_to_file(void)
 {
   extern bool check_z_average;


   if(updating_menu_state != 0)
     {
       if ((general_pr_action == find_selected_mean_max)
 	  && strstr(active_menu->text, "Unselect Mean/Max speed to file") == NULL)
    {
 	  snprintf(active_menu->text,128,"Unselect Mean/Max speed to file");
    check_z_average=0;
   }
       else  if ((general_pr_action != find_selected_mean_max)
 	  && strstr(active_menu->text, "Unselect Mean/Max speed to file") != NULL)
    {
 	  snprintf(active_menu->text,128,"Select Mean/Max speed to file");
    check_z_average=1;
    }
       return D_O_K;
     }

   if (general_pr_action == find_selected_mean_max)
     {
       general_pr_action = prev_general_pr_action;
       prev_general_pr_action = find_selected_mean_max;
     }
   else
     {
       prev_general_pr_action = general_pr_action;
       general_pr_action = find_selected_mean_max;
     }
   return 0;
 }
//save_one_file(const char *dialog_title, const char *current_dir, const char *filename, const char *filter)

 int find_selected_mean_max(pltreg *pr, int x_0, int y_0, DIALOG *d)
 {
     FILE *f;
     if (define_fout_mean_max==NULL && save_events_to_file==1){
     strcpy(&fout_event_mean,save_one_file_config("choose were to save  your events _ do not CANCEL !", NULL, "events.txt", "txt\0*.txt*\0All file\0*.*\0\0", "DNA_MANIPULATION", "last_saved"));
     define_fout_mean_max=&fout_event_mean;
    };


     int   dx = 0, dy = 0, ldx, ldy, color, iseg = 0, i, i_1, k, page_n, i_page, iseg_chg = 0;
     //float  xa, ya; // x, y,
     int xp[64] = {0}, yp[64] = {0}, lx, ly, i_ev, i_seg, end, i_seg2, xpr, ypr, bead_nb;
     float xf[64] = {0}, yf[64] = {0};
     char seg[64] = {0};
     //struct box *b;
     O_p *op = NULL;
     d_s *ds = NULL;
     g_record *g_r = NULL;
     b_record *b_r = NULL;

     if (pr->one_p == NULL)		return D_O_K;
     g_r = find_g_record_in_pltreg(pr);
     if (g_r == NULL)   win_printf_OK("No gen record found");
     op = pr->one_p;
     bead_nb = op->user_ispare[ISPARE_BEAD_NB];
     if (bead_nb < 0 || bead_nb >= g_r->n_bead)  win_printf_OK("bead nb out of range");
     b_r = g_r->b_r[bead_nb];
     op = pr->one_p;
     //sc = pr->screen_scale;
     //b = pr->stack;
     //ya = y_pr_2_pltdata_raw(pr, y_0);// y =
     //xa = x_pr_2_pltdata_raw(pr, x_0);//x =

     ds = find_source_specific_ds_in_op(op,"event polyline");
     for (i = 0, i_ev = i_seg = end = -1; ds != NULL && i < ds->nx; i++)
       {
         if (abs(x_pltdata_2_pr(pr, ds->xd[i])+d->x - x_0) < 5
   	  && abs(y_pltdata_2_pr(pr, ds->yd[i])+d->y - y_0) < 5)
   	{
   	  if (find_segment_from_point(pr, op, ds->xd[i], ds->yd[i], &i_ev, &i_seg, &end))
   	    //win_printf("did not find Polyline modif i %d",i);

   	  //win_printf("Found event %d segment %d end %d ds i %d",i_ev, i_seg, end,i);
   	  break;
   	}
         if (i == 0)
   	{
   	  display_title_message("x0 %d y0 %d -> x %d y %d", x_0,y_0,x_pltdata_2_pr(pr, ds->xd[0]),
   		     y_pltdata_2_pr(pr, ds->yd[i]));
   	}
       }

     xp[0] = x_0;   // this is the starting point
     yp[0] = y_0;
     ldy = 0;
     ldx = 0;
     color = makecol(255, 64, 64);
     iseg_chg = 1;
     while ((mouse_b & 0x02) == 0)
       {     // as long as no right click
         dx = mouse_x - x_0;
         dy = mouse_y - y_0;
         if  (ldy != dy || dx != ldx)
   	{   // if mouse moving we update line
   	  blit_plot_reg(d);
   	  acquire_bitmap(screen);
   	  circle(screen, xp[0],yp[0], 2, color);
   	  for (i = 1; i <= iseg; i++)
   	    {
   	      line(screen,xp[i-1],yp[i-1],xp[i],yp[i],color);
   	      circle(screen, xp[i],yp[i], 2, color);
   	    }
   	  line(screen,xp[iseg],yp[iseg],x_0 + dx,y_0 + dy,color);
   	  release_bitmap(screen);
   	  // we display point values
   	  draw_bubble(screen, B_LEFT, 20, 64, "x0 = %g y0 %g iseg %d",
   		      x_pr_2_pltdata(pr,x_0),y_pr_2_pltdata(pr,y_0),iseg);
   	  draw_bubble(screen, B_RIGHT, SCREEN_W-64, 64, "x1 = %g y2 %g",
   		      x_pr_2_pltdata(pr,x_0 + dx),y_pr_2_pltdata(pr,y_0 + dy));
   	  //y = y_pr_2_pltdata_raw(pr, y_0 + dy);
   	  //x = x_pr_2_pltdata_raw(pr, x_0 + dx);
   	}
         if (mouse_b & 0x01)
   	{   // on left click we record the point as a new segment
   	  if (iseg_chg == 0 && iseg < 64)
   	    {
   	      iseg++;
   	      xp[iseg] = x_0 + dx;
   	      yp[iseg] = y_0 + dy;
   	      iseg_chg = 1;
   	    }
   	}
         else iseg_chg = 0;
         if (key[KEY_ESC])	return D_REDRAWME;	// escape get out
         ldx = dx;
         ldy = dy;
       }
     if ( abs(dy) > 5 || abs(dx) > 5)
       {
         if (iseg == 0) return 0;
         for (i = 1; i <= iseg; i++)
   	{
   	  i_1 = i - 1;
   	  lx = xp[i] - xp[i_1];
   	  ly = yp[i] - yp[i_1];
   	  if (8*abs(ly) < abs(lx))         seg[i_1] = 'N';
   	  else if (8*abs(lx) < ly)         seg[i_1] = 'H';
   	  else if (8*abs(lx) < -ly)        seg[i_1] = 'R';
   	  else if (8*ly > abs(lx))         seg[i_1] = 'Z';
   	  else if (8*ly < -abs(lx))        seg[i_1] = 'U';
   	  else
   	    {
   	      seg[i_1] = 'x';
   	      win_printf("x seg %d \nx0 %d y0 %d\n lx %d ly %d ",i,xp[i_1],yp[i_1],lx,ly);
   	    }
   	}
         seg[i-1] = 0;
         //win_printf("poly %d seg val %s",iseg,seg);
         for (i = 0; i <= iseg; i++)
   	{
   	  yf[i] = y_pr_2_pltdata_raw(pr, yp[i]);
   	  xf[i] = x_pr_2_pltdata_raw(pr, xp[i]);

   	}
         //do_simple_segment(pr, op, op->dat[op->cur_dat], xa, ya, x, y);
         do_segments_min_mean(pr, op, op->dat[op->cur_dat], iseg, xf, yf, seg);

         //lo = xf[0];
         //hi = xf[1];







         //find maximum negative speed

         int marges=64;
         float begin_actual_ds=(op->dat[op->cur_dat])->xd[0];
         d_s *event_ds = duplicate_partial_data_set(op->dat[op->cur_dat], NULL,(int)(xf[0]-marges-begin_actual_ds),(int)(xf[1]-xf[0]+2*marges));

         printf("Cur xd beg = %f; index_begin=%f\n",begin_actual_ds,xf[0]-16-(pr->one_p)->x_lo);
         double chi_ff;
         if (event_ds==NULL) {win_printf("Empty dataset : you might have selected the wrong data set."); return 0;}
         else {
         //for (i=0;i<event_ds->nx;i++) printf("xi[%d]=%f\n",i,event_ds->xd[i]);

         printf("nl parameters before function p=%f mul=%f noise_mul=%f\n",p,mu,n_mu_l);

         //find HFsigma
               int j=0;
               int nf=event_ds->nx;
               int cx = 2*(nf - s_noise);
               int nx0 = (cx/s_noise);
               nx0 = (nx0 < 0) ? 0 : nx0;
               static double *sigHF = NULL;
               int i_sighf=0;
               int n_sigHF=0;
               float meany,my2;
               nx0 += (cx/s_noise) ? 2 : 1;
               if (nx0 > n_sigHF)
               {
                 sigHF = (double*)realloc(sigHF,nx0*sizeof(double));
                 n_sigHF = nx0;
               }

               mean_y2_on_array(event_ds->yd, event_ds->nx, 0, &meany, &my2, 0);
               for (i_sighf = 0; i_sighf < nx0; i_sighf++)
               {
                 j = (nx0 > 1) ? (cx * i_sighf)/(2 * (nx0-1)) : 1;
                 get_sigma_of_derivative_of_partial_ds(event_ds, j, j + s_noise, 4 * sqrt(my2), sigHF+i_sighf);
                 printf("sigHF[%d]=%f\n",i_sighf,*(sigHF+i_sighf));
               }


         d_s *ds_filtered = nl_fit_step_and_slope_vc_2_r(event_ds,NULL,NULL,maxs,min,s_noise,p,&chi_ff,mu,n_mu_l);
        ds_filtered->color = White ;
        add_ds_to_op(pr->one_p,ds_filtered);
        int nf_ori=event_ds->nx;
          nf = ds_filtered->nx;
          float time_ms=0;
        float dev_l,dx,dy,max=0;
        for (i=marges;i<nf-marges;i++)
         {
          printf("i=%d,xd[i]=%f\n",i,ds_filtered->xd[i]);
         dx=ds_filtered->xd[i+1]-ds_filtered->xd[i];
         dx*=pr->one_p->dx;

         dy=ds_filtered->yd[i+1]-ds_filtered->yd[i];
         dy*=1000*pr->one_p->dy;
         dev_l=-dy/dx;
         //printf("dx=%f,dy=%f, dev_l %d = %f\n",dx,dy,i,dev_l);
         if (max<dev_l){
         max = dev_l;
         time_ms= ds_filtered->xd[i];
       };
     };
         printf("max_speed=%f in time=%f\n ",max,time_ms);

         float delta_ti=(pr->one_p->dx)*(xf[1]-xf[0]);
         float delta_y=1000*(pr->one_p->dy)*(yf[1]-yf[0]);
         float debut=x_pr_2_pltdata(pr, xp[0]);
         float Delta_vitesse=1000*(sigHF[0]+sigHF[nx0-1])/delta_ti;
         printf("bead %d; debut %f; delta_x %f;delta_y %f;vitesse_moy;%f;vitesse_max%f\n",bead_nb,debut,delta_ti,delta_y,-delta_y/delta_ti,Delta_vitesse,max);
         if (save_events_to_file==1){
         f = fopen(fout_event_mean, "a");
          fprintf(f,"%d;%f;%f;%f;%f;%f\n",bead_nb,debut,delta_ti,delta_y,-delta_y/delta_ti,max);
         fclose(f);};
         if (remove_data_set_after_filtering==1)
         remove_one_plot_data(pr->one_p, IS_DATA_SET, (void *)ds_filtered);

         free_data_set(event_ds);
         return 0;
       }
     }
     return D_O_K;
   }


   int    do_segments_min_mean(pltreg *pr, O_p *op, d_s *dsi, int n_seg, float *x, float *y, char *type)
   {
     printf("n_seg=%d\n",n_seg);
     register int i, j, k;
     int  imin, imax, bead_nb, page_n, i_page;
     double  min, max;   // of segments data
     float max_yl, min_yl;
     g_record *g_r = NULL;
     b_record *b_r = NULL, *b_rf = NULL;
     float zmag = 0, rot = 0, zobj = 0, xavg = 0, yavg = 0, zavg = 0, zavg_s = 0, zavg_e = 0, chi2 = 0,  max_histo = 0;
     int param_cst = 0, profile_invalid = 0, n_ev = 0, xi[64] = {0}, seg_s = 0, seg_e = 0;

     //win_printf("entering poly segs");
     (void)y;
     g_r = find_g_record_in_pltreg(pr);
     if (g_r == NULL)   win_printf_OK("Ne gen record found");
     bead_nb = op->user_ispare[ISPARE_BEAD_NB];
     if (bead_nb < 0 || bead_nb >= g_r->n_bead)  win_printf_OK("bead nb out of range");
     b_r = g_r->b_r[bead_nb];
     for (i = 0, j = 0; i <= n_seg; i++)
         j = (x[i] < x[j]) ? i : j;
     min = x[j];
     for (i = 0, j = 0; i <= n_seg; i++)
         j = (x[i] > x[j]) ? i : j;
     max = x[j];
     //dsn = op->cur_dat;
     //op_n = pr->cur_op;
     dsi = op->dat[0];
     for (imin = dsi->nx -1, imax = i = 0;i < dsi->nx; i++)
       {  // we find point index bracketting segments
         if (dsi->xd[i] <= max && i > imax) 	  imax = i;
         if (dsi->xd[i] >= min && i < imin) 	  imin = i;
       }

     for (j = 0; j < g_r->n_page && ((g_r->imi[j][0] - g_r->imi[0][0]) < (int)min) ; j++);
     j = (j > 0) ? j-1 : j;
     for (k = 0; k < g_r->page_size && ((g_r->imi[j][k] - g_r->imi[0][0]) < (int)min) ; k++);
     k = (k > 0) ? k-1 : k;
     imin = g_r->imi[j][k] - g_r->imi[0][0];

     for (j = 0; j < g_r->n_page && ((g_r->imi[j][0] - g_r->imi[0][0]) < (int)max) ; j++);
     j = (j > 0) ? j-1 : j;
     for (k = 0; k < g_r->page_size && ((g_r->imi[j][k] - g_r->imi[0][0]) < (int)max) ; k++);
     k = (k > 0) ? k-1 : k;
     imax = g_r->imi[j][k] - g_r->imi[0][0];

     if (imax <= imin)
       return win_printf("imin = %d imax = %d impossible",imin,imax);
     for (i = 1, min_yl = max_yl = dsi->yd[0]; i < dsi->nx; i++)
       {   // we find min and max in y of dsi data
         max_yl = (dsi->yd[i] > max_yl) ? dsi->yd[i] : max_yl;
         min_yl = (dsi->yd[i] < min_yl) ? dsi->yd[i] : min_yl;
       }
     for (i = 0; i <= n_seg && i < 64; i++)
       {

         for (j = 0; j < g_r->n_page && ((g_r->imi[j][0] - g_r->imi[0][0]) < (int)x[i]) ; j++);
         j = (j > 0) ? j-1 : j;
         for (k = 0; k < g_r->page_size && ((g_r->imi[j][k] - g_r->imi[0][0]) < (int)x[i]) ; k++);
         //k = (k > 0) ? k-1 : k;
         xi[i] = g_r->imi[j][k] - g_r->imi[0][0];
       }
   /*
     for (i = 0; i <= n_seg && i < 64; i++)
       {
         for (j = 0, xi[i] = 0, tmp = fabs(x[i] - dsi->xd[0]); j < dsi->nx; j++)
   	{
   	  if (fabs(dsi->xd[j] - x[i]) < tmp)
   	    {   // we convert segment x position to index store in xi
   	      tmp = fabs(dsi->xd[j] - x[i]);
   	      xi[i] = j;
   	    }
   	}
       }
   */
     /*
     if (n_seg > 3)
       {
         win_printf("xi 0 %d %c 1 %d %c 2 %d %c 3 %d",
   		 xi[0],type[0],xi[1],type[1],xi[2],type[2],xi[3]);
       }
     */
     // this is the starting position of event
     j = op->user_ispare[ISPARE_STARTING_POSITION_SET];

     /*
     ims = (g_r->timing_mode == 1) ? (g_r->imi[j/g_r->page_size][j%g_r->page_size] - g_r->imi_start)
       : g_r->imi[j/g_r->page_size][j%g_r->page_size];

     j = ims;
     ims = j;
     */
     //j += imin;
     //win_printf("seg extends %d to %d",ims + imin,ims + imax);
     // we compute average values and check that params are ok
     average_param_during_part_trk(g_r, imin, imax - imin, &zmag, &rot, &zobj, &param_cst);
     if (param_cst != 0)
       win_printf_OK("Your experiment parameters\n were not constant during event %d\n",param_cst);

     if (do_substract_fixed_bead && fixed_bead_nb >= 0 &&  fixed_bead_nb < g_r->n_bead)
       {
         b_rf = g_r->b_r[fixed_bead_nb];
         average_bead_z_minus_fixed_during_part_trk_tolerant(g_r, bead_nb, fixed_bead_nb, imin, imax - imin, 1, NULL,
   					       NULL, &zavg, NULL, &profile_invalid);
       }
     else average_bead_z_during_part_trk_tolerant(g_r, bead_nb, imin, imax - imin, 1, NULL, NULL, &zavg, NULL, &profile_invalid);
     if (profile_invalid)
       {
         i = win_printf("Z tracking experience some troubles during this event\n"
   		    "%d points over %d points may have bad Z value\n"
   		     "Do you want to keep this event ?",profile_invalid,imax - imin);
         if (i == WIN_CANCEL) return 0;
       }

     for (i = 0; i < n_seg; i++)
       {

         if (type[i] == 'H' || type[i] == 'R')  // a front
   	{
   	  if (i == 0)
   	    return win_printf_OK("An event cannot start by a front!\n");
   	  else if (i == n_seg - 1)
   	    return win_printf_OK("An event cannot end by a front!\n");
   	  else if (type[i-1] == 'N' && type[i+1] == 'N')
   	    {  // need work here
   	      j = find_front_position(g_r, b_r, b_rf, j+xi[i-1], j+(xi[i]+xi[i+1])/2, j+xi[i+2], 5+abs(xi[i+2]-xi[i-1])/7);
   	      // win_printf("front adj j = %d start %d estimate %d \nend %d mov_allow %d ini %d"
   	      //	 ,j
   	      //	 ,ims  +xi[i-1]
   	      //	 ,ims +(xi[i]+xi[i+1])/2
   	      //	 ,ims+ xi[i+2]
   	      //	 , 5+abs(xi[i+2]-xi[i-1])/7,ims);
   	      //j -= ims;
   	      if (j > 0)
   		{
   		  //win_printf("0-> %d j = %d 1-> %d",xi[i],j,xi[i+1]);
   		  xi[i] = j;
   		  xi[i+1] = j+2;
   		}
   	    }
   	}
       }

     // we record the event
     //g_r->n_events++;
     n_ev = g_r->n_events;
     for (i = imin, k = 0; i < imax; i++)
       {
         if (k < n_seg && i >= xi[k+1]) k++;
         j = i; //op->user_ispare[ISPARE_STARTING_POSITION_SET] + i;
         page_n = j/g_r->page_size;
         i_page = j%g_r->page_size;
         b_r->event_nb[page_n][i_page] = n_ev;
         if (type[k] == 'H')        b_r->event_index[page_n][i_page] = H_SEGMENT;
         else if (type[k] == 'R')   b_r->event_index[page_n][i_page] = R_SEGMENT;
         else if (type[k] == 'Z')   b_r->event_index[page_n][i_page] = Z_SEGMENT;
         else if (type[k] == 'U')   b_r->event_index[page_n][i_page] = U_SEGMENT;
         else if (type[k] == 'N')   b_r->event_index[page_n][i_page] = N_SEGMENT;
       }
     // we record the event in the new structure

     i = (b_rf) ? fixed_bead_nb : -1;
     //win_printf("bef histo fixed = %d",i);
     find_bead_z_max_histo_during_part_trk(g_r, bead_nb, i,
   					(imin+imax)/2 -256,
   					512, 0.002, &max_histo, NULL, NULL, &profile_invalid);
     //op->user_ispare[ISPARE_STARTING_POSITION_SET] +
     //win_printf("after histo n_seg = %d",n_seg);
     for (;b_r->na_e >= b_r->ma_e; )
       {
         b_r->a_e = (analyzed_event*)realloc(b_r->a_e ,(b_r->ma_e+16)*sizeof(analyzed_event));
         if (b_r->a_e == NULL) return win_printf_OK("cannot allocate event data!\n");
         b_r->ma_e += 16;
       }
     b_r->a_e[b_r->na_e].e_s = (event_seg*)calloc(n_seg,sizeof(event_seg));
     if (b_r->a_e[b_r->na_e].e_s == NULL) return win_printf_OK("cannot allocate segments of event data!\n");
     b_r->ia_e = b_r->na_e;
     b_r->na_e++;
     b_r->a_e[b_r->ia_e].n_seg = n_seg;
     b_r->a_e[b_r->ia_e].i_ev = g_r->n_events;
     b_r->a_e[b_r->ia_e].ev_start = imin; // op->user_ispare[ISPARE_STARTING_POSITION_SET] +
     b_r->a_e[b_r->ia_e].ev_end = imax; // op->user_ispare[ISPARE_STARTING_POSITION_SET] +
     b_r->a_e[b_r->ia_e].zmag = zmag;
     b_r->a_e[b_r->ia_e].zobj = zobj;
     b_r->a_e[b_r->ia_e].rot = rot;
     b_r->a_e[b_r->ia_e].n_badz = profile_invalid;
     b_r->a_e[b_r->ia_e].type = EVENT_POLY_LINE;

     for (i = 0; i < n_seg; i++)
       {
         printf("y[0]=%f\n",y[0]);
         printf("y[1]=%f\n",y[1]);

         b_r->a_e[b_r->ia_e].e_s[i].i_seg = i;
         b_r->a_e[b_r->ia_e].e_s[i].i_ev = g_r->n_events;
         b_r->a_e[b_r->ia_e].e_s[i].seg_start = seg_s = xi[i]; // op->user_ispare[ISPARE_STARTING_POSITION_SET] +
         b_r->a_e[b_r->ia_e].e_s[i].seg_end = seg_e = xi[i+1]; // op->user_ispare[ISPARE_STARTING_POSITION_SET] +
         b_r->a_e[b_r->ia_e].e_s[i].dt = (float)(b_r->a_e[b_r->ia_e].e_s[i].seg_end -
   					    b_r->a_e[b_r->ia_e].e_s[i].seg_start)
   	/g_r->Pico_param_record.camera_param.camera_frequency_in_Hz;
         average_param_during_part_trk(g_r, seg_s, seg_e - seg_s, &zmag, &rot, &zobj, &param_cst);
         b_r->a_e[b_r->ia_e].e_s[i].par_chg = param_cst;//) ? 0 : 1;
         b_r->a_e[b_r->ia_e].e_s[i].zmag = zmag;
         b_r->a_e[b_r->ia_e].e_s[i].rot = rot;
         b_r->a_e[b_r->ia_e].e_s[i].zobj = zobj;
         b_r->a_e[b_r->ia_e].e_s[i].z_ref = max_histo;
         if (g_r->evanescent_mode <= 0)
   	b_r->a_e[b_r->ia_e].e_s[i].z_ref *= g_r->z_cor;
         b_r->a_e[b_r->ia_e].e_s[i].fixed_bead = (do_substract_fixed_bead && fixed_bead_nb >= 0
   					       &&  fixed_bead_nb < g_r->n_bead) ? fixed_bead_nb + g_r->start_bead : -1;
         b_r->a_e[b_r->ia_e].e_s[i].n_bead = bead_nb + g_r->start_bead;

         if (do_substract_fixed_bead && fixed_bead_nb >= 0 &&  fixed_bead_nb < g_r->n_bead)
   	{
   	  average_bead_z_minus_fixed_during_part_trk_tolerant(g_r, bead_nb, fixed_bead_nb, seg_s, seg_e - seg_s, 1,
   							   &xavg, &yavg, &zavg, NULL, &profile_invalid);
   	}
         else  average_bead_z_during_part_trk_tolerant(g_r, bead_nb, seg_s, seg_e - seg_s, 1,
   						 &xavg, &yavg, &zavg, NULL, &profile_invalid);

         b_r->a_e[b_r->ia_e].e_s[i].n_badz = profile_invalid;
         b_r->a_e[b_r->ia_e].e_s[i].zavg = 0.5*(y[0]+y[1]);//zavg;
    //      if (g_r->evanescent_mode <= 0)
   	// b_r->a_e[b_r->ia_e].e_s[i].zavg *= g_r->z_cor;
         b_r->a_e[b_r->ia_e].e_s[i].xavg = g_r->ax + g_r->dx * xavg;
         b_r->a_e[b_r->ia_e].e_s[i].yavg = g_r->ay + g_r->dy * yavg;

         if (type[i] == 'Z'  || type[i] == 'U')
   	{
   	  fit_bead_z_to_line_during_part_trk_tolerant(g_r, bead_nb, ((do_substract_fixed_bead) ? fixed_bead_nb : -1),
   						      seg_s, seg_e - seg_s, 1, &zavg_s, &zavg_e, &profile_invalid, &chi2); // va stocker dans zavg_s et zavg_e les points de début et de fin du segment.

   	  b_r->a_e[b_r->ia_e].e_s[i].zstart = y[0]; //zavg_s;
   	  b_r->a_e[b_r->ia_e].e_s[i].zend = y[1];   //zavg_e;
   	  // if (g_r->evanescent_mode <= 0)
   	  //   {
   	  //     b_r->a_e[b_r->ia_e].e_s[i].zstart *= g_r->z_cor;
   	  //     b_r->a_e[b_r->ia_e].e_s[i].zend *= g_r->z_cor;
   	  //   }
   	  b_r->a_e[b_r->ia_e].e_s[i].sig_z = ((seg_e - seg_s) > 0) ? sqrt(chi2) /(seg_e - seg_s) : 0;
   	}
         if (type[i] == 'H')        b_r->a_e[b_r->ia_e].e_s[i].type = H_SEGMENT;
         else if (type[i] == 'R')   b_r->a_e[b_r->ia_e].e_s[i].type = R_SEGMENT;
         else if (type[i] == 'Z')   b_r->a_e[b_r->ia_e].e_s[i].type = Z_SEGMENT;
         else if (type[i] == 'U')   b_r->a_e[b_r->ia_e].e_s[i].type = U_SEGMENT;
         else if (type[i] == 'N')
   	{
   	  b_r->a_e[b_r->ia_e].e_s[i].type = N_SEGMENT;
   	  b_r->a_e[b_r->ia_e].e_s[i].zstart_vertex = y[0]; //b_r->a_e[b_r->ia_e].e_s[i].zavg;
   	  b_r->a_e[b_r->ia_e].e_s[i].zend_vertex = y[1]; //b_r->a_e[b_r->ia_e].e_s[i].zavg;
   	}
         b_r->a_e[b_r->ia_e].e_s[i].taux = 0;
         b_r->a_e[b_r->ia_e].e_s[i].tauy = 0;
         b_r->a_e[b_r->ia_e].e_s[i].tauz = 0;
       }
       for (i = 0; i < n_seg; i++)
       {
         if (type[i] == 'Z'  || type[i] == 'U')
   	{
   	  if (i > 0 && type[i-1] == 'N')
   	    b_r->a_e[b_r->ia_e].e_s[i].zstart_vertex = y[0]; //b_r->a_e[b_r->ia_e].e_s[i-1].zavg;
   	  else b_r->a_e[b_r->ia_e].e_s[i].zstart_vertex = b_r->a_e[b_r->ia_e].e_s[i].zstart;

   	  if ((i+1) < n_seg && type[i+1] == 'N')
   	    b_r->a_e[b_r->ia_e].e_s[i].zend_vertex = y[1]; //b_r->a_e[b_r->ia_e].e_s[i+1].zavg;
   	  else b_r->a_e[b_r->ia_e].e_s[i].zend_vertex = b_r->a_e[b_r->ia_e].e_s[i].zend;
   	}
       }
       for (i = 0; i < n_seg; i++)
       {
         if (type[i] == 'H'  || type[i] == 'R')
   	{
   	  if (i > 0)
   	    b_r->a_e[b_r->ia_e].e_s[i].zstart_vertex = b_r->a_e[b_r->ia_e].e_s[i-1].zend_vertex;
   	  else b_r->a_e[b_r->ia_e].e_s[i].zstart_vertex = b_r->a_e[b_r->ia_e].e_s[i].zstart;

   	  if ((i+1) < n_seg)
   	    b_r->a_e[b_r->ia_e].e_s[i].zend_vertex = b_r->a_e[b_r->ia_e].e_s[i+1].zstart_vertex;
   	  else b_r->a_e[b_r->ia_e].e_s[i].zend_vertex = b_r->a_e[b_r->ia_e].e_s[i].zend;
   	}
       }

       g_r->n_events++;

       //win_printf("%d Event %d recorded with %d seg max histo %f",b_r->ia_e,b_r->a_e[b_r->ia_e].i_ev,b_r->a_e[b_r->ia_e].n_seg,g_r->z_cor*max_histo);
       //win_printf("Bead %d Simple segment ev %d\n starting at %d lasting %d",bead_nb,n_ev, op->user_ispare[ISPARE_STARTING_POSITION_SET]+imin,imax-imin);

     op->user_ispare[ISPARE_POSITION_UPDATE]--;
     return 0;
   }
//
//
// int reload_select_events_man(void)
// {
//      int argc=1;
//      char pl_name[64];
//
//     if(updating_menu_state != 0) return 0;
//
//     PL_MAIN ProcAdd;
//       //win_printf("bef grep");
//     int n = grep_plug_ins(select_events_man);
//
//
//
//       // If the handle is valid, try to get the function address.
//   //#ifdef XV_WIN32
//   //    ProcAdd = (PL_MAIN)GetProcAddress(plg_in[n].hinstLib, pl_name);
//   //#else
//   snprintf(pl_name, 64, "%s_unload", select_events_man);
//       ProcAdd = (PL_MAIN)dlsym(plg_in[n].hinstLib, pl_name);
//
//
//
//   //#endif
//
//       // If the function address is valid, call the function.
//
//       if (ProcAdd != NULL)
//       {
//           //      win_printf("bef proc");
//           (ProcAdd)(1, select_events_man);
//           //win_printf("after proc");
//           printf("ok\n");
//           set_config_string("PLUGINS", "last_loaded", select_events_man);
//           printf("ok\n");
//           //win_printf("bef free");
//   //#ifdef XV_WIN32
//   //
//   //        if (FreeLibrary(plg_in[n].hinstLib) == 0)
//   //        {
//   //            return win_printf_OK("could not remove plugins %s.dll\n maybe this test has to inversed...", argv[0]);
//   //        }
//   //
//   //#else
// printf("ok\n");
//           if (dlclose(plg_in[n].hinstLib) != 0)
//           {
//             printf("ok\n");
//               return (win_printf_OK("could not remove plugins %.\n(maybe it is still in use ?)", PLUGIN_EXT));
//           }
//
//   //#endif
// printf("ok\n");
//           //win_printf("bef remove");
//           if (remove_plug_ins_record(n))
//           {
//               return win_printf_OK("could not remove plugins from recorded list");
//           }
//       }
//       else
//       {
//           win_printf("could not find unload function for plugins.dll\n");
//           return D_O_K;
//       }
//
//       //win_printf("bef update");
//       do_update_menu();
//
//
//       load_plugin(1, select_events_man);
//       return D_O_K;
//   }// end of 'run_module'


MENU *select_events_man_plot_menu(void)
{
  static MENU mn[32];
  char *mnstr = NULL;

  if (mn[0].text != NULL)	return mn;
  mnstr = (char *) calloc(128,sizeof(char));
  if (mnstr == NULL) return NULL;
  snprintf(mnstr,128,"Select Multi segments line");
  add_item_to_menu(mn,mnstr,do_select_send_events_to_file,NULL,0,NULL);
  add_item_to_menu(mn,"change filter parameter",change_NL_parameters,NULL,0,NULL);
  add_item_to_menu(mn,"change file name",change_file_name,NULL,0,NULL);
//  add_item_to_menu(mn,"reload",reload_select_events_man,NULL,0,NULL);

  return mn;
}

int	select_events_man_main(int argc, char **argv)
{
  (void)argc;  (void)argv;  add_plot_treat_menu_item ( "select_events_man", NULL, select_events_man_plot_menu(), 0, NULL);
  return D_O_K;
}

int	select_events_man_unload(int argc, char **argv)
{
  (void)argc;  (void)argv;
  if (general_pr_action == find_selected_mean_max)
    {
      general_pr_action = prev_general_pr_action;
      prev_general_pr_action = find_selected_mean_max;
    }
  remove_item_to_menu(plot_treat_menu, "select_events_man", NULL, NULL);
  return D_O_K;
}
#endif
