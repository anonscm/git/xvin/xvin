/** Define and typedef declarations for the plug-in

This file contains the declaration of all define and typedef in the plug-in, 
sorted by the .c file where they appear.
This allows all these define to be used in any .c file in the plug-in 
as long as <<#define global_define.h>> is declared in the .c file.

\author Adrien Meglio
*/

#ifndef _GLOBAL_DEFINE_H_
#define _GLOBAL_DEFINE_H_

//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
// DEFINEs



#endif
