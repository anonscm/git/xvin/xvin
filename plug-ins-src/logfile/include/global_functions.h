/** Function declarations for the plug-in

This file contains the declaration of all functions in the plug-in, 
sorted by the .c file where they appear and then by the category 
within the file to which they belong.
This allows all the functions to be used in any .c file in the plug-in 
as long as <<#define global_functions.h>> is declared in the .c file.

\author Adrien Meglio
*/

#ifndef _GLOBAL_FUNCTIONS_H_
#define _GLOBAL_FUNCTIONS_H_

//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
// From logfile.c

PXV_FUNC(int, test, (void));

// File management functions
PXV_FUNC(FILE*, fopen__debug, (void));
PXV_FUNC(FILE*, fopen__event, (void));
PXV_FUNC(FILE*, fopen__message, (void));
PXV_FUNC(int, debug__create_log_file, (void));
PXV_FUNC(int, action__create_log_file, (void));
PXV_FUNC(int, error__create_log_file, (void));

// Timestamping functions
PXV_FUNC(char*, formatted_time, (void));
PXV_FUNC(char*, get_date, (void));

// Debugger
PXV_FUNC(int, win_debug, (char *message, ...));

// Event reporter
PXV_FUNC(int, win_event, (char *message, ...));

// Error functions
PXV_FUNC(int, old__win_report, (char *message));
PXV_FUNC(int, win_report, (char *message, ...));
PXV_FUNC(int, test_win_report, (void));

// Housekeeping functions
PXV_FUNC(int, logfile_main, (void));

#endif
