/** Global variables declarations for the plug-in

This file contains the declaration of all global variables in the plug-in, 
sorted by the .c file where they appear.
This allows all these variables to be used in any .c file in the plug-in 
as long as <<#define global_variables.h>> is declared in the .c file.

\author Adrien Meglio
*/

#ifndef _GLOBAL_VARIABLES_H_
#define _GLOBAL_VARIABLES_H_



#endif
