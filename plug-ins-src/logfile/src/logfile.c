/** \file logfile.c
    \brief Plug-in program that reports events and errors in log files.
    
    \author Adrien Meglio
    \version 22/01/07
*/
#ifndef _LOGFILE_C_
#define _LOGFILE_C_

#include "allegro.h"
#include "winalleg.h"
#include <windowsx.h>
#include "ctype.h"
#include "xvin.h"

#include <time.h>
#include "global_define.h"
#include "global_variables.h"
#include "global_functions.h"

#define BUILDING_PLUGINS_DLL


int test(void)
{
  char filename[128];
  FILE* file_handle;
  
  if(updating_menu_state != 0) return NULL;
  
  //win_printf("Date : %s",get_date());
  
  sprintf(filename,"%s-event.log",get_date());

  //win_printf("%s",filename);

  file_handle = fopen(filename,"a");
  fprintf(file_handle,"\nTEST\n");
  fclose(file_handle);

  return D_O_K;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// File management functions

/* Opens a log file for debug.

Opens a file named DATE-debug.log in XVINDIR/bin. If the file exists, data is appended. If not, it is created.

\author Adrien Meglio
\version 4/11/08
*/
FILE* fopen__debug(void)
{
  char filename[128];
  
  if(updating_menu_state != 0) return NULL;
  
  sprintf(filename,"%s-debug.log",get_date());

  return fopen(filename,"a");
  /* Opens the file, or creates it if it does not exist. 
     Also, will erase an existing file */
}

/* Opens a log file for events.

Opens a file named DATE-event.log in XVINDIR/bin. If the file exists, data is appended. If not, it is created.

\author Adrien Meglio
\version 19/02/08
*/
FILE* fopen__event(void)
{
  char filename[128];
  
  if(updating_menu_state != 0) return NULL;
  
  sprintf(filename,"%s-event.log",get_date());

  return fopen(filename,"a");
  /* Opens the file, or creates it if it does not exist. 
     Also, will append all content at the end of the file */
}

/* Opens a log file for messages.

Opens a file named DATE-message.log in XVINDIR/bin. If the file exists, data is appended. If not, it is created.

\author Adrien Meglio
\version 19/02/08
*/
FILE* fopen__message(void)
{
  char filename[128];
  
  if(updating_menu_state != 0) return NULL;
  
  sprintf(filename,"%s-message.log",get_date());

  return fopen(filename,"a");
  /* Opens the file, or creates it if it does not exist. 
     Also, will append all content at the end of the file */
}


/* Initializes the log file for debugging.

\author Adrien Meglio
\version 19/02/08
*/
int debug__create_log_file(void)
{
    time_t rawtime;
    FILE *DebugLogFile;
    
    if(updating_menu_state != 0) return D_O_K;
    
    DebugLogFile = fopen__debug();
    
    fprintf(DebugLogFile,"\n*******************************\n");
    
    /* Displays current time */
    struct tm *timeinfo;
    time (&rawtime);
    timeinfo = localtime(&rawtime);
    fprintf(DebugLogFile,"Session : %s\n\n",asctime(timeinfo));
   
    fclose(DebugLogFile);
    
    return D_O_K;
}

/* Initializes the log file for events.

\author Adrien Meglio
\version 19/02/08
*/
int action__create_log_file(void)
{
    time_t rawtime;
    FILE *ActionLogFile;
    
    if(updating_menu_state != 0) return D_O_K;
    
    ActionLogFile = fopen__event();
    
    fprintf(ActionLogFile,"\n*******************************\n");
    
    /* Displays current time */
    struct tm *timeinfo;
    time (&rawtime);
    timeinfo = localtime(&rawtime);
    fprintf(ActionLogFile,"Session : %s\n\n",asctime(timeinfo));
   
    fclose(ActionLogFile);
    
    return D_O_K;
}

/* Initializes the log file for messages.

\author Adrien Meglio
\version 19/02/08
*/
int message__create_log_file(void)
{
    FILE *ErrorLogFile;
    
    if(updating_menu_state != 0) return D_O_K;
    
    ErrorLogFile = fopen__message();
    
    fprintf(ErrorLogFile,"\n*******************************\n");
    
    /* Displays current time */
    fprintf(ErrorLogFile,"Session : %s\n",formatted_time());
   
    fclose(ErrorLogFile);
    
    return D_O_K;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Timestamping functions

/* Provides the current time.

Returns a char* of the form "Mon, Jan 1 2008, 12:00:00". Use it like win_printf("%s",formatted_time());

\author Adrien Meglio
\version 19/02/08
*/
char* formatted_time(void)
{
  time_t rawtime;
  
  if(updating_menu_state != 0) return NULL;
  
  struct tm *timeinfo;
  time (&rawtime); /* Get time */
  timeinfo = localtime(&rawtime);/* Convert to tm structure */
  
  return asctime(timeinfo); /* Convert to ascii string */
}

/* Provides the current time (short).

Returns a char* of the form 12:00:00 with ALWAYS 2 digits for hour, min and secs. Use it like win_printf("%s",get_time());

\author Adrien Meglio
\version 19/02/08
*/
char* get_time(void)
{
  time_t rawtime;
  char current_time[32];
  
  if(updating_menu_state != 0) return NULL;
  
  struct tm *timeinfo;
  time (&rawtime); /* Get time */
  timeinfo = localtime(&rawtime);/* Convert to tm structure */

  /* Write in the correct format, with always 2 digits for hour, min and sec */
  sprintf(current_time,"%02d:%02d:%02d",timeinfo->tm_hour,timeinfo->tm_min,timeinfo->tm_sec);

  //win_printf("Time : %s",time);

  return current_time;
}

/* Provides the current date (short).

Returns a char* of the form 2008-12-28 with ALWAYS 2 digits for day and month. Use it like win_printf("%s",get_date());

\author Adrien Meglio
\version 19/02/08
*/
char* get_date(void)
{
  time_t rawtime;
  char date[32];
  
  if(updating_menu_state != 0) return NULL;
  
  struct tm *timeinfo;
  time (&rawtime); /* Get time */
  timeinfo = localtime(&rawtime);/* Convert to tm structure */

  /* Write in the correct format, with always 2 digits for month and day */
  sprintf(date,"%d-%02d-%02d",(1900+timeinfo->tm_year),(timeinfo->tm_mon+1),timeinfo->tm_mday);

  //win_printf("Date : %s",date);

  return date;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Debugger

/** A fprintf replacement that writes in the debug log file.

Syntax is the same as win_printf and lets the user write complex messages such as
win_printf("One number : %d",integer);

\author Adrien Meglio
\verison 4/11/08
*/
int win_debug(char *message, ...)
{
    FILE *LogFile;
    va_list ap;
    char temp[16384];
    
    if(updating_menu_state != 0) return  D_O_K;
     
    va_start(ap, message);
    
    /////////////////////////////
    // Prints into the log file 
        
    vsprintf(temp,message,ap); 
    // BEWARE : use of va_list requires use of vsprintf and win_ti_vprintf !
    
    LogFile = fopen__debug();
    fprintf(LogFile,"* %s : ",get_time());
    fprintf(LogFile,temp);
    fprintf(LogFile,"\n");
    fclose(LogFile);
    
    /////////////////////////////
    // Displays a dialog box
     
    va_end(ap);    
     
    return D_O_K;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Event reporter

/** A fprintf replacement that writes in the event log file.

Syntax is the same as win_printf and lets the user write complex messages such as
win_printf("One number : %d",integer);

\author Adrien Meglio
\verison 23/01/07
*/
int win_event(char *message, ...)
{
    FILE *LogFile;
    va_list ap;
    char temp[16384];
    
    if(updating_menu_state != 0) return  D_O_K;
     
    va_start(ap, message);
    
    /////////////////////////////
    // Prints into the log file 
        
    vsprintf(temp,message,ap); 
    // BEWARE : use of va_list requires use of vsprintf and win_ti_vprintf !
    
    LogFile = fopen__event();
    fprintf(LogFile,"* %s : ",get_time());
    fprintf(LogFile,temp);
    fprintf(LogFile,"\n");
    fclose(LogFile);
    
    /////////////////////////////
    // Displays a dialog box
     
    va_end(ap);    
     
    return D_O_K;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Error function

/** A win_printf replacement that also writes in the message log file.

Syntax is the same as win_printf and lets the user write complex messages such as
win_printf("One number : %d",integer);

\author Adrien Meglio
\verison 22/01/07
*/
int win_report(char *message, ...)
{
    FILE *LogFile;
    int status;
    va_list ap;
    char temp[16384];
    
    if(updating_menu_state != 0) return  D_O_K;
     
    va_start(ap, message);
    
    /////////////////////////////
    // Prints into the log file 
        
    vsprintf(temp,message,ap); 
    // BEWARE : use of va_list requires use of vsprintf and win_ti_vprintf 
    // instead of the regular sprintf and win_printf !
    
    LogFile = fopen__message();
    fprintf(LogFile,"* %s : ",get_time());
    fprintf(LogFile,temp);
    fprintf(LogFile,"\n");
    fclose(LogFile);
    
    /////////////////////////////
    // Displays a dialog box
     
    status = win_ti_vprintf("Dialog Box",message,ap);
     
    va_end(ap);    
     
    return status;
}

int test_win_report(void)
{
    int i = 3;
    
    if(updating_menu_state != 0) return  D_O_K;
    
    //win_report("Essai de win-report");
    
    //win_report("Autre essai de win-report");
    
    win_report("Un essai plus complexe : %d, %d",i,i*i+1);
    
    return D_O_K;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Housekeeping functions

/** Main function of \a logfile.c . 

This function initializes the system, loads the menus. 
\param none
\return Error code.
\author Adrien Meglio
\version 09/05/06
*/
int logfile_main(void)
{     
    menu_plug_in_main();
    
    return D_O_K;
}

/** Unload function for the \a logfile.c menu.*/
int logfile_unload(void)
{
	return D_O_K;
}
#endif



