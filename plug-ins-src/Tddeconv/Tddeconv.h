#ifndef _TDDECONV_H_
#define _TDDECONV_H_

PXV_FUNC(int, do_Tddeconv_average_along_y, (void));
PXV_FUNC(MENU*, Tddeconv_image_menu, (void));
PXV_FUNC(int, Tddeconv_main, (int argc, char **argv));
#endif

