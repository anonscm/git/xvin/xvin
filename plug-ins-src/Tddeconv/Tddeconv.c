/*
*    Plug-in program for image treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _TDDECONV_C_
#define _TDDECONV_C_

# include "allegro.h"
# include "xvin.h"

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "Tddeconv.h"
//place here other headers of this plugin 

float zfil1[15][15] = {{4.51, 1.49, -4.04, -8.23, -9.61, -9.05, -7.19, -6.31, -7.19, -9.05, -9.61, -8.23, -4.04, 1.49, 4.51},
		      {1.47, -5.16, -9.38, -8.11, -2.12, 5.68, 10.39, 11.91, 10.39, 5.68, -2.12, -8.11, -9.38, -5.16, 1.47},
		      {-4.03, -9.38, -6.89, 3.49, 15.62, 21.58, 24.02, 24.50, 24.02, 21.58, 15.62, 3.49, -6.89, -9.38, -4.03},
		      {-8.24, -8.10, 3.47, 18.84, 23.60, 16.85, 1.88, -5.94, 1.88, 16.85, 23.60, 18.84, 3.47, -8.10, -8.24},
		      {-9.56, -2.16, 15.67, 23.56, 8.32, -31.86, -62.08, -68.09, -62.08, -31.86, 8.32, 23.56, 15.67, -2.16, -9.56},
		      {-8.86, 5.49, 21.77, 16.65, -31.62, -77.28, -65.10, -49.30, -65.10, -77.28, -31.62, 16.65, 21.77, 5.49, -8.86},
		      {-6.71, 9.92, 24.51, 1.41, -61.64, -65.77, 66.23, 201.13, 66.23, -65.77, -61.64, 1.41, 24.51, 9.92, -6.71},
		      {-5.44, 11.03, 25.37, -6.82, -67.16, -49.98, 202.49, 481.45, 202.49, -49.98, -67.16, -6.82, 25.37, 11.03, -5.44},
		      {-6.71, 9.92, 24.51, 1.41, -61.64, -65.77, 66.23, 201.13, 66.23, -65.77, -61.64, 1.41, 24.51, 9.92, -6.71},
		      {-8.86, 5.49, 21.77, 16.65, -31.62, -77.28, -65.10, -49.30, -65.10, -77.28, -31.62, 16.65, 21.77, 5.49, -8.86},
		      {-9.56, -2.16, 15.67, 23.56, 8.32, -31.86, -62.08, -68.09, -62.08, -31.86, 8.32, 23.56, 15.67, -2.16, -9.56},
		      {-8.24, -8.10, 3.47, 18.84, 23.60, 16.85, 1.88, -5.94, 1.88, 16.85, 23.60, 18.84, 3.47, -8.10, -8.24},
		      {-4.03, -9.38, -6.89, 3.49, 15.62, 21.58, 24.02, 24.50, 24.02, 21.58, 15.62, 3.49, -6.89, -9.38, -4.03},
		      {1.47, -5.16, -9.38, -8.11, -2.12, 5.68, 10.39, 11.91, 10.39, 5.68, -2.12, -8.11, -9.38, -5.16, 1.47},
		      {4.51, 1.49, -4.04, -8.23, -9.61, -9.05, -7.19, -6.31, -7.19, -9.05, -9.61, -8.23, -4.04, 1.49, 4.51}};


float zfil[15][15] = {{ 4,  1, -4, -8, -9,   -9,  -7,  -6,  -7,  -9,  -9, -8, -4,  1,  4},
		      { 1, -5, -9, -8, -2,    5,  10,  11,  10,   5,  -2, -8, -9, -5,  1},
		      {-4, -9, -6, 3,  15,   21,  24,  24,  24,  21,  15,  3, -6, -9, -4},
		      {-8, -8,  3, 18, 23,   16,   1,  -5,   1,  16,  23, 18,  3, -8, -8},
		      {-9, -2, 15, 23,  8,  -31, -62, -68, -62, -31,   8, 23, 15, -2, -9},
		      {-8,  5, 21, 16, -31, -77, -65, -49, -65, -77, -31, 16, 21,  5, -8},
		      {-6,  9, 24,  1, -61, -65,  66, 201,  66, -65, -61,  1, 24,  9, -6},
		      {-5, 11, 25, -6, -67, -49, 202, 481, 202, -49, -67, -6, 25, 11, -5},
		      {-6,  9, 24,  1, -61, -65,  66, 201,  66, -65, -61,  1, 24,  9, -6},
		      {-8,  5, 21, 16, -31, -77, -65, -49, -65, -77, -31, 16, 21,  5, -8},
		      {-9, -2, 15, 23,  8,  -31, -62, -68, -62, -31,   8, 23, 15, -2, -9},
		      {-8, -8,  3, 18, 23,   16,   1,  -5,   1,  16,  23, 18,  3, -8, -8},
		      {-4, -9, -6, 3,  15,   21,  24,  24,  24,  21,  15,  3, -6, -9, -4},
		      { 1, -5, -9, -8, -2,   5,   10,  11,  10,   5,  -2, -8, -9, -5,  1},
		      { 4,  1, -4, -8, -9,  -9,   -7,  -6,  -7,  -9,  -9, -8, -4,  1,  4}};


/*
 *		dest = ois1 - ois2
 *
 */
O_i 	*conv_2_images(O_i *dest, O_i *ois1, O_i *ois2)
{
  int i, j, i2, j2;
  int  onx, ony;
  int  onx2, ony2;
  int	dest_type = 0;
  union pix *pd;
  double tmp;
  float *z = NULL, *z2 = NULL;


    if (ois1 == NULL || ois1->im.pixel == NULL)
    {
        win_printf("No image found");
        return (NULL);
    }
    ony = ois1->im.ny;	onx = ois1->im.nx;
    if (ois1->im.data_type == IS_COMPLEX_IMAGE)
    {
        win_printf ("the image is not of the right type!\n"
                    "Cannot perform convolution!");
        return(NULL);
    }

    if (ois2 == NULL || ois2->im.pixel == NULL)
    {
        win_printf("No image found");
        return (NULL);
    }
    ony2 = ois2->im.ny;	onx2 = ois2->im.nx;
    if (ois2->im.data_type != IS_FLOAT_IMAGE)
    {
        win_printf ("the image is not of float type!\n"
                    "Cannot perform convolution!");
        return(NULL);
    }
    if (dest != NULL)
    {
        if (dest->im.data_type != IS_FLOAT_IMAGE)
        {
            win_printf ("The destination image is not of float type\n"
                        "Cannot perform multiplication!");
            return(NULL);
        }
        if (ony != dest->im.ny || onx != dest->im.nx)
        {
            win_printf ("The destination size differs from the source one \n"
                        "Cannot perform multiplication!");
            return(NULL);
        }
    }
    else
    {
        dest_type = IS_FLOAT_IMAGE;
    }
    dest = (dest != NULL) ? dest : create_one_image(onx, ony, dest_type);
    if (dest == NULL)
    {
        win_printf("Can't create dest image");
        return(NULL);
    }

    z = (float*)calloc(onx2,sizeof(float));
    z2 = (float*)calloc(onx2,sizeof(float));
    if (z == NULL || z2 == NULL) 
    {
        win_printf("Can't create temporary float arrays");
        return(NULL);
    }

    pd = dest->im.pixel;


    for (i=0; i< ony-ony2; i++)
    {
        for (j=0; j<onx-onx2; j++)
        {
	  tmp = 0;
	  for (i2=0; i2< ony2; i2++)
	    {
	      extract_raw_partial_line (ois1, i+i2, z, j, onx2);
	      extract_raw_line (ois2, i2, z2);
	      for (j2=0; j2< onx2; j2++)
		{
		  tmp += z[j2]*z2[j2];
		}
	    }
	  pd[i+ony2/2].fl[j+onx2/2] = tmp/(onx2*ony2);
        }
    }
    free(z);
    free(z2);
    inherit_from_im_to_im(dest,ois1);
    uns_oi_2_oi(dest,ois1);
    dest->im.win_flag = ois1->im.win_flag;

    set_formated_string(&dest->im.treatement,"Convolution of 2 images of %s",
                        (ois1->title != NULL) ? ois1->title : ((ois1->im.source != NULL)
                                                               ? ois1->im.source : "untitled"));
    set_im_title(dest, "\\stack{{Convolution of 2 images of}{%s and %s}}",
                 (ois1->im.source != NULL) ? 	ois1->im.source : "untitled",
                 (ois2->im.source != NULL) ? 	ois2->im.source : "untitled");
    dest->need_to_refresh = ALL_NEED_REFRESH;
    return dest;
}



/*
 *		dest = ois1 - ois2
 *
 */
O_i 	*convert_image_to_float(O_i *dest, O_i *ois1)
{
    int i, im;
    int  onx, ony, nf, nfd;
    int	dest_type = 0;
    union pix *pd; 

    if (ois1 == NULL || ois1->im.pixel == NULL)
    {
        win_printf("No image found");
        return (NULL);
    }
    ony = ois1->im.ny;	onx = ois1->im.nx;
    nf = ois1->im.n_f;
    if (nf < 1) nf = 1;
    if (ois1->im.data_type == IS_COMPLEX_IMAGE)
    {
        win_printf ("the image is of complex type!\n"
                    "Cannot transform!");
        return(NULL);
    }

    if (dest != NULL)
    {
        if (dest->im.data_type != IS_FLOAT_IMAGE)
        {
            win_printf ("The destination image is not of float type\n"
                        "Cannot perform conversion!");
            return(NULL);
        }
        if (ony != dest->im.ny || onx != dest->im.nx)
        {
            win_printf ("The destination size differs from the source one \n"
                        "Cannot perform conversion!");
            return(NULL);
        }
        nfd = dest->im.n_f;
        if (nfd < 1) nfd = 1;
        if (nf != nfd)
        {
            win_printf ("The destination nb of frame differs from the source one \n"
                        "Cannot perform conversion!");
            return(NULL);
        }

    }
    else
        dest_type = IS_FLOAT_IMAGE;

    if (nf == 1)
        dest = (dest != NULL) ? dest : create_one_image(onx, ony, dest_type);
    else dest = (dest != NULL) ? dest : create_one_movie(onx, ony, dest_type, nf);
    if (dest == NULL)
    {
        win_printf("Can't create dest image");
        return(NULL);
    }
    for (im = 0; im < nf; im++)
    {
        switch_frame(ois1,im);
        switch_frame(dest,im);
        pd = dest->im.pixel;
        for (i=0; i< ony; i++)
        {
	  extract_raw_line (ois1, i, pd[i].fl);
        }
    }
    inherit_from_im_to_im(dest,ois1);
    uns_oi_2_oi(dest,ois1);
    dest->im.win_flag = ois1->im.win_flag;
    dest->need_to_refresh = ALL_NEED_REFRESH;
    return dest;
}

int		do_convert_image_to_float(void)
{
    O_i   *oid, *ois;
    imreg *imr;

    if(updating_menu_state != 0)	return D_O_K;

    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
    oid = convert_image_to_float(NULL, ois);
    if (oid == NULL)	return D_O_K;
    add_image(imr, oid->type, (void*)oid);
    find_zmin_zmax(oid);
    oid->need_to_refresh = ALL_NEED_REFRESH;
    return (refresh_image(imr, imr->n_oi - 1));
}



int		do_conv_2_im(void)
{
    int i;
    O_i   *oid, *ois;
    int n_den;
    imreg *imr;

    if(updating_menu_state != 0)	return D_O_K;

    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
    if ((imr->cur_oi - 1) < 0)
        return win_printf_OK("I need at least two images!");
    n_den = imr->cur_oi + 1;
    i = win_scanf("I willfilter by convolution this image by a smaller one,\n"
                  " select this second image %d",&n_den);
    if (i == CANCEL)	return D_O_K;
    n_den = (n_den < 0) ? 0 : n_den;
    n_den %= imr->n_oi;

    oid = conv_2_images(NULL, ois, imr->o_i[n_den]);
    if (oid == NULL)	return D_O_K;
    add_image(imr, oid->type, (void*)oid);
    find_zmin_zmax(oid);
    oid->need_to_refresh = ALL_NEED_REFRESH;
    return (refresh_image(imr, imr->n_oi - 1));
}


MENU *Tddeconv_image_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"Filter one image by convolution of a small one", do_conv_2_im,NULL,0,NULL);
	add_item_to_menu(mn,"convert to float image", do_convert_image_to_float,NULL,0,NULL);


	add_image_treat_menu_item ( "Tddeconv", NULL, Tddeconv_image_menu(), 0, NULL);

	return mn;
}

int	Tddeconv_main(int argc, char **argv)
{
  (void)argc;
  (void)argv;
	add_image_treat_menu_item ( "Tddeconv", NULL, Tddeconv_image_menu(), 0, NULL);
	return D_O_K;
}

int	Tddeconv_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;
	remove_item_to_menu(image_treat_menu, "Tddeconv", NULL, NULL);
	return D_O_K;
}
#endif

