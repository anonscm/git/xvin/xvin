
float zfil[15][15] = {{ 4,  1, -4, -8, -9,   -9,  -7,  -6,  -7,  -9,  -9, -8, -4,  1,  4},
		      { 1, -5, -9, -8, -2,    5,  10,  11,  10,   5,  -2, -8, -9, -5,  1},
		      {-4, -9, -6, 3,  15,   21,  24,  24,  24,  21,  15,  3, -6, -9, -4},
		      {-8, -8,  3, 18, 23,   16,   1,  -5,   1,  16,  23, 18,  3, -8, -8},
		      {-9, -2, 15, 23,  8,  -31, -62, -68, -62, -31,   8, 23, 15, -2, -9},
		      {-8,  5, 21, 16, -31, -77, -65, -49, -65, -77, -31, 16, 21,  5, -8},
		      {-6,  9, 24,  1, -61, -65,  66, 201,  66, -65, -61,  1, 24,  9, -6},
		      {-5, 11, 25, -6, -67, -49, 202, 481, 202, -49, -67, -6, 25, 11, -5},
		      {-6,  9, 24,  1, -61, -65,  66, 201,  66, -65, -61,  1, 24,  9, -6},
		      {-8,  5, 21, 16, -31, -77, -65, -49, -65, -77, -31, 16, 21,  5, -8},
		      {-9, -2, 15, 23,  8,  -31, -62, -68, -62, -31,   8, 23, 15, -2, -9},
		      {-8, -8,  3, 18, 23,   16,   1,  -5,   1,  16,  23, 18,  3, -8, -8},
		      {-4, -9, -6, 3,  15,   21,  24,  24,  24,  21,  15,  3, -6, -9, -4},
		      { 1, -5, -9, -8, -2,   5,   10,  11,  10,   5,  -2, -8, -9, -5,  1},
		      { 4,  1, -4, -8, -9,  -9,   -7,  -6,  -7,  -9,  -9, -8, -4,  1,  4}};

