#pragma once
PXV_FUNC(int, do_notch_rescale_plot, (void));
PXV_FUNC(MENU*, notch_plot_menu, (void));
PXV_FUNC(int, do_notch_rescale_data_set, (void));
PXV_FUNC(int, notch_main, (int argc, char **argv));
