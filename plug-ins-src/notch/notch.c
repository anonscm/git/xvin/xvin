/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _NOTCH_C_
#define _NOTCH_C_

#include <allegro.h>
# include "xvin.h"

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "notch.h"

int do_notch_filter(void)
{
  int i, j, k;
  O_p *op = NULL;
  int nf;
  static int per = 20, nper = 2;
  static float damp = 0.9;
  d_s *ds = NULL, *dsi = NULL, *dst = NULL;
  pltreg *pr = NULL;
  float cc, re, ren, re0, dampc;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    return win_printf_OK("This routine compute a notch filter");

  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  nf = dsi->nx;	/* this is the number of points in the data set */
  i = win_scanf("Notch filter with projection on sin and cosine\n"
		"Define the period of the notch frequency %4d (in pts)\n"
		"The number of period to project on %4d\n"
		"The attenuation at notch %5f\n"
		,&per,&nper,&damp);
  if (i == WIN_CANCEL)	return OFF;

  dst = build_data_set(nper*per,nper*per);
  if (dst == NULL)    return win_printf_OK("cannot create tmp ds !");
  if ((ds = create_and_attach_one_ds(op, nf-nper*per, nf-nper*per, 0)) == NULL)
    return win_printf_OK("cannot create plot !");

  for (j = 0, ren = re0 = 0; j < dst->nx; j++)
    { // we prepare our projection kernel
      dst->xd[j] = (1+cos(j*M_PI/dst->nx)) * cos((M_PI*2*j)/per)/2;
      ren += dst->xd[j] * cos((M_PI*2*j)/per);
      dst->yd[j] = (1+cos(j*M_PI/dst->nx))/2;
      re0 += dst->xd[j];
    }
  re0 /= ren;
  dampc = damp/(1-re0);
  for (i = 0; i < ds->nx; i++)
  {
    k = i+dst->nx-1;
    // we project signal on kernel
    for (j = 0, re = 0, cc = 0; j < dst->nx; j++)
      {
	re += dst->xd[j] * dsi->yd[k-j];
	cc += dst->yd[j] * dsi->yd[k-j];
      }
    //we normalize
    re /= ren;
    cc /= re0;
    // we filter
    ds->yd[i] = (1+(dampc*re0))*dsi->yd[k] - dampc * (re);
    ds->xd[i] = dsi->xd[k];
  }
  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"y multiplied by %f",re0);
  /* refisplay the entire plot */
  if (dst != NULL) free_data_set(dst);
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}

MENU *notch_plot_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"Notch filter", do_notch_filter,NULL,0,NULL);
  return mn;
}

int	notch_main(int argc, char **argv)
{
  (void)argc;  (void)argv;  add_plot_treat_menu_item ( "notch", NULL, notch_plot_menu(), 0, NULL);
  return D_O_K;
}

int	notch_unload(int argc, char **argv)
{
  (void)argc;  (void)argv;  remove_item_to_menu(plot_treat_menu, "notch", NULL, NULL);
  return D_O_K;
}
#endif

