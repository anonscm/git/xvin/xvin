#include <limits>
#include <map>
# ifdef XV_WIN32
#include <boost/serialization/array_wrapper.hpp>
# endif
#include "filter_core.h"
#include "accumulators.h"

namespace filter
{
    namespace median
    {
        struct _RunningMedian
        {
            friend struct RunningMedian;
            friend void run(size_t, size_t, float*);
            friend void run(size_t, std::valarray<float> &);

            _RunningMedian(size_t s) : _values(s) {}
            void operator()(float x)
            {
                if(_burned)
                {
                    auto old      = _values[_ind];
                    _values[_ind] = x;
                    _ind          = _ind >= _values.size()-1 ? 0 : _ind+1;
                    if(old == x)
                        return;

                    (old <= _left.rbegin()->first ? _left : _right).erase(old);
                } else
                {
                    _values[_ind++] = x;
                    if(_ind == 0)
                    {
                        _left.add(x);
                        return;
                    }

                    _burned = _ind == _values.size();
                    if(_burned)
                        _ind = 0;
                }

                (x   <= _left.rbegin()->first ? _left : _right).add  (x);
                if(_left.sz+1 < _right.sz)
                {
                    auto tmp = _left.rbegin()->first;
                    _left .erase(tmp);
                    _right.add  (tmp);
                }
                else if(_left.sz > _right.sz+1)
                {
                    auto tmp = _right.begin()->first;
                    _right.erase(tmp);
                    _left .add  (tmp);
                }
            }

            auto operator*() const
            {
                if(_left.sz == _right.sz)
                    return .5f*(_left.rbegin()->first + _right.begin()->first);
                else if(_left.sz < _right.sz)
                    return _right.begin()->first;
                else
                    return _left.rbegin()->first;
            }

            private:
                struct _map: public std::map<float, size_t>
                {
                    void erase(value_type::first_type old)
                    {
                        auto   it   = this->find(old);
                        if(it->second == 1)
                            this->erase(old);
                        else
                            --it->second;
                        --this->sz;
                    }

                    void add(value_type::first_type x)
                    {
                        auto   it = this->find(x);
                        if(it != this->end())
                            ++it->second;
                        else
                            this->insert({x, 1});
                        ++this->sz;
                    }

                    size_t sz = 0;
                };

                _map                    _left;
                _map                    _right;
                std::valarray<float>    _values;
                size_t                  _ind    = 0;
                bool                    _burned = false;
        };

        RunningMedian::RunningMedian(size_t s) : _impl(new _RunningMedian(s)) {}
        RunningMedian::~RunningMedian ()                { delete _impl; }
        void  RunningMedian::operator ()(float x)       { (*_impl)(x); }
        float RunningMedian::operator*()          const { return **_impl; }

        void run(size_t wlen, size_t nx, float *xd)
        {
            _RunningMedian med(wlen);

            for(size_t i = 0; i < nx; ++i)
            {
                med(xd[i]);
                xd[i] = *med;
            }
        }

        void run(size_t wlen, std::valarray<float> & dt)
        {
            _RunningMedian med(wlen);
            for(auto & x: dt)
            {
                med(x);
                x = *med;
            }
        }
    }

    namespace
    {
        using namespace stats;
        struct _BaseQuality
        {
            _BaseQuality()                     = default;
            _BaseQuality(_BaseQuality const &) = default;
            template <typename T>
            _BaseQuality(T const & cf, size_t w, bool norm)
                : _prec  (std::abs(cf.precision))
                , _norm  (norm)
                , _pow   (-int(cf.power))
                , _est   (1)
                , _qual  (w)
                , _factor(1.)
                , _bias  (0.)
            {}

            protected:
                void setup(size_t wl, bool dir)
                {
                    _est.setup(wl, dir);
                    if(_norm)
                    {
                        _factor = wl <= 1 ? 0  : wl/(wl-1.);
                        _bias   = wl <= 1 ? 4. : 2.*std::sqrt(wl)/(wl-1.);
                    }
                }

                std::pair<double, double> add(double wgt, double mean) const
                {
                    constexpr auto nm = std::numeric_limits<float>::max();
                    wgt = wgt*_factor+_bias;
                    if(!std::isfinite(wgt))
                        return {0., mean};
                    else if(wgt <= std::numeric_limits<float>::min())
                        return {nm, mean};

                    auto pw = std::pow(wgt, _pow);
                    return {pw > nm || !std::isfinite(pw) ? nm : pw, mean};
                }

                double                              _prec;
                bool                                _norm;
                int                                 _pow;
                RARolling<bat::mean>                _est;
                Rolling  <bat::mean>                _qual;
                double                              _factor = 1.;
                double                              _bias   = 0.;
        };

        template <typename T>
        struct _CovQuality: public T
        {
            _CovQuality()                       = default;
            _CovQuality(_CovQuality<T> const &) = default;
            template <typename K>
            _CovQuality(K const & args)
                : T     (args)
                , _var  (1)
                , _covar(1)
            {}

            void setup(size_t wl, bool dir)
            {
                T::setup(wl, dir);
                _var  .setup(wl);
                _covar.setup(wl);
            }

            void operator()(size_t i, float const * xd)
            {
                _var(i);
                _covar(xd[0]*i);
                T::operator()(i, xd);
            }

            std::pair<double, double> get() const
            {
                auto r     = T::get();
                auto x     = ba::mean(_var);
                auto covar = ba::mean(_covar)-x*r.second;
                auto x2dev = ba::moment<2>(_var)-x*x;
                r.second   = x2dev != 0. ? covar/x2dev
                           : covar == 0. ? std::numeric_limits<float>::min()
                                         : std::numeric_limits<float>::max();
                return r;
            }

            private:
                Rolling<bat::mean, bat::moment<2>>  _var;
                Rolling<bat::mean>                  _covar;
        };

        struct _BaseFunc
        {
            _BaseFunc()                  = default;
            _BaseFunc(_BaseFunc &&)      = default;
            _BaseFunc(_BaseFunc const &) = default;

            _BaseFunc(size_t nx, float * xd, size_t nv)
                : _sz(nx), _xd(xd), _m0(0., nv), _m1(0., nv)
            {}

            size_t          size()                   const { return _sz;     }
            float const *   get (int i, int)         const { return _xd+i;   }
            void            set (int i, double v)          { _xd[i] = v;     }

            template <typename T>
            void   add (T const & qual, int i, int j)
            { add(qual.get(), i, j); }

            void   add (std::pair<double,double> pair, int i, int)
            {
                _m0[i]  += pair.first;
                auto rho = pair.first/_m0[i];
                _m1[i]   = pair.second*rho + _m1[i]*(1.-rho);
            }

            void finish()
            {
                if(_sz == _m1.size())
                    for(size_t i = 0; i < _sz; ++i)
                        _xd[i] = _m1[i];
            }

            protected:
                size_t                _sz;
                float               * _xd;
                std::valarray<double> _m0, _m1;
        };

        template <typename T0, typename T1>
        auto _qual(T0 const & cf, bool dir)
        {
            std::valarray<T1> res(T1(cf), cf.estimators.size());
            for(size_t i = 0; i < res.size(); ++i)
                res[i].setup(cf.estimators[i], dir);
            return res;
        }
    }

    namespace forwardbackward
    {
        namespace
        {
            struct _Quality: _BaseQuality
            {
                _Quality(Args const & cf) : _BaseQuality(cf, cf.window, cf.normalize) {}
                using _BaseQuality::_BaseQuality;
                using _BaseQuality::setup;

                void operator()(size_t, float const * xd)
                {
                    _est (xd);
                    auto x = (xd[0]-ba::mean(_est))/_prec;
                    _qual(x*x);
                }

                auto get() const
                {
                    auto mean = ba::mean(_est);
                    auto wgt  = ba::mean(_qual);
                    return _BaseQuality::add(wgt, mean);
                }
            };
        }

        template <typename T, typename Q>
        void run(Args cf, T fcn)
        {
            int const  nx  ((int)fcn.size());
            int const  ne  ((int)cf.estimators.size());
            auto       qual(_qual<Args,Q>(cf, true));

            auto apply  = [&qual,&fcn,ne](int i)
                        {
                            for(int j = 0; j < ne; ++j)
                            {
                                qual[j](i, fcn.get(i,j));
                                fcn.add(qual[j], i, j);
                            }
                        };

            for(int i = 0; i < nx; ++i)
                apply(i);

            qual = _qual<Args,Q>(cf, false);
            for(int i = nx-1; i >= 0; --i)
                apply(i);

            fcn.finish();
        }

        void run(Args cf, size_t nx, float *xd)
        {
            _BaseFunc func(nx, xd, nx);
            if(cf.derivate)
                run<_BaseFunc,_CovQuality<_Quality>>(cf, func);
            else
                run<_BaseFunc,_Quality>(cf, func);
        }

        void run(Args cf, std::valarray<float> & dt)
        {
            _BaseFunc func(dt.size(), &dt[0], dt.size());
            if(cf.derivate)
                run<_BaseFunc,_CovQuality<_Quality>>(cf, func);
            else
                run<_BaseFunc,_Quality>(cf, func);
        }
    }

    namespace xvnonlin
    {
        namespace
        {
            struct _Quality: _BaseQuality
            {
                _Quality(Args const & cf) : _BaseQuality(cf, 1, true) {}
                using _BaseQuality::_BaseQuality;

                void setup(size_t wl, bool dir)
                {
                    _BaseQuality::setup(wl, dir);
                    _qual.setup(wl);
                }

                void operator()(size_t, float const * xd)
                {
                    _est (xd);
                    auto x = xd[0]/_prec;
                    _qual(x*x);
                }

                auto get() const
                {
                    auto mean = ba::mean(_est);
                    auto wgt  = ba::mean(_qual)-(mean/_prec)*(mean/_prec);
                    return _BaseQuality::add(wgt, mean);
                }
            };

            struct _MovingFunc: public _BaseFunc
            {
                _MovingFunc(Args const & cf, size_t nx, float * xd)
                    : _BaseFunc(nx, xd, cf.estimators[cf.estimators.size()-1])
                    , _k  (0)
                    , _nv (cf.estimators[cf.estimators.size()-1])
                    , _inc(cf.estimators.size())
                {
                    for(size_t j = 0; j < _inc.size(); ++j)
                        _inc[j] = _nv-cf.estimators[j]+1;
                }

                template <typename T>
                void   add(T const & qual, size_t i, size_t j)
                {   add(qual.get(), i, j); }

                void   add(std::pair<double,double> val, size_t, size_t j)
                {
                    _BaseFunc::add(val, _k, j);
                    _BaseFunc::add(val, (_k+_inc[j]) % _nv, j);
                }

                auto compute()
                {
                    _k      = (_k+1) % _nv;
                    auto r  = _m1[_k];
                    _m1[_k] = _m0[_k] = 0.;
                    return r;
                }

                private:
                    size_t                _k, _nv;
                    std::valarray<size_t> _inc;
            };
        }

        template <typename T, typename Q>
        void run(Args const & cf, T & fcn)
        {
            size_t const ne = cf.estimators.size();
            size_t const nv = cf.estimators[ne-1];
            size_t const nx = fcn.size();
            if(nv >= nx)
                return;

            auto qual(_qual<Args,Q>(cf, true));

            auto update = [ne, &qual, &fcn](size_t i)
                        {
                            for(size_t j = 0; j < ne; ++j)
                                qual[j](i, fcn.get(i, j));
                        };
            auto apply  = [ne, &qual, &fcn](size_t i)
                        {
                            for(size_t j = 0; j < ne; ++j)
                                fcn.add(qual[j], i, j);
                            return fcn.compute();
                        };

            decltype(apply(0)) val = 0;
            for(size_t i = 0; i < nv; ++i)
            {
                update(i);
                val = apply (i);
            }

            for(size_t i = nv; i < nx; ++i)
            {
                update(i);
                fcn.set(i-nv, val);
                val = apply(i);
            }
            fcn.set(nx-nv, val);

            for(size_t i = nx-nv+1; i < nx; ++i)
                fcn.set(i, apply (i));

            fcn.finish();
        }

        void run(Args cf, size_t nx, float * xd)
        {
            _MovingFunc fcn(cf, nx, xd);
            if(cf.derivate)
                run<_MovingFunc,_CovQuality<_Quality>>(cf, fcn);
            else
                run<_MovingFunc,_Quality>(cf, fcn);
        }

        void run(Args cf, std::valarray<float> & dt)
        {
            _MovingFunc func(cf, dt.size(), &dt[0]);
            if(cf.derivate)
                run<_MovingFunc,_CovQuality<_Quality>>(cf, func);
            else
                run<_MovingFunc,_Quality>(cf, func);
        }
    }
}

void xvnonlin_run(int n, float * vals)
{
    filter::xvnonlin::Args args;
    stats::acc_t<stats::bat::mediandeviation> quant;
    for(int i = 1; i < n; ++i)
        if(std::isfinite((vals[i]-vals[i-1])))
            quant(vals[i]-vals[i-1]);

    args.precision = stats::compute(quant);
    if(args.precision <= 0 || !std::isfinite(args.precision))
        args.precision = 0.003;

    filter::xvnonlin::run(args, n, vals);
}
