#pragma once
#include <utility>

namespace samples
{
    namespace normal
    {
        struct Input
        {
            size_t count;
            double mean;
            double sigma;
        };

        namespace knownsigma
        {
            double  value       (bool, Input const &, Input const &);
            double  threshold   (bool, double, double);
            double  threshold   (bool, double, double, size_t, size_t);
            bool    isequal     (double, double, Input const &, Input const &);
        }

        namespace homoscedastic
        {
            std::pair<size_t, double> value(Input const &, Input const &);
            bool isequal  (double, Input const &, Input const &);
            bool islower  (double, Input const &, Input const &);
            bool isgreater(double, Input const &, Input const &);
        }

        namespace heteroscedastic
        {
            std::pair<double, double> value(Input const &, Input const &);
            bool isequal  (double, Input const &, Input const &);
            bool islower  (double, Input const &, Input const &);
            bool isgreater(double, Input const &, Input const &);
        }
    }
}
