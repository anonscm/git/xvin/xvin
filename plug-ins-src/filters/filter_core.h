#pragma once
#ifdef __cplusplus
#include <valarray>
namespace filter
{
    namespace median
    {
        struct _RunningMedian;
        struct RunningMedian
        {
            RunningMedian(size_t);
            ~RunningMedian();

            void  operator  ()(float);
            float operator* () const;

            private:
                _RunningMedian * _impl;
        };

        void run(size_t, size_t, float *);
        void run(size_t, std::valarray<float> &);
    }

    namespace forwardbackward
    {
        struct Args
        {
            bool                    derivate   = false;
            bool                    normalize  = true;
            float                   precision  = 0.003;
            size_t                  window     = 10;
            size_t                  power      = 20;
            std::valarray<size_t>   estimators = { 3,  5,  7,  9, 11, 13, 15, 17,
                                                  19, 21, 23, 25, 27, 29, 31, 33};
        };

        void run(Args, size_t, float *);
        void run(Args, std::valarray<float> &);
    }

    namespace xvnonlin
    {
        struct Args
        {
            bool                    derivate   = false;
            float                   precision  = 0.003;
            size_t                  power      = 20;
            std::valarray<size_t>   estimators = { 3,  5,  7,  9, 11, 13, 15, 17,
                                                  19, 21, 23, 25, 27, 29, 31, 33};
        };

        void run(Args, size_t, float *);
        void run(Args, std::valarray<float> &);
    }
}

extern "C"
{
#endif

void xvnonlin_run(int, float *);

#ifdef __cplusplus
}
#endif
