#include <boost/math/distributions/students_t.hpp>
#include <boost/math/distributions/normal.hpp>
#include "stattests.h"

namespace bm = boost::math;

namespace samples
{
    namespace normal
    {
        namespace
        {
            template <typename T>
            inline auto _level(std::pair<T,double> val)
            {
                bm::students_t dist(val.first);
                return bm::cdf(dist, std::abs(val.second));
            }

            template <typename T>
            inline bool _isequal(double alpha, std::pair<T,double> val)
            {
                auto lev = _level(val);
                return lev > alpha*.5 && lev < 1.-alpha*.5;
            }

            template <typename T>
            inline bool _islower(double alpha, std::pair<T,double> val)
            { return _level(val) < alpha; }

            double _cntnorm(double c1, double c2)
            { return std::sqrt(c1*c2/(c1+c2)); }
        }

        namespace knownsigma
        {
            double value(bool bequal, Input const & left, Input const & right)
            {
                auto val = (left.mean-right.mean) * _cntnorm(left.count, right.count);
                return bequal && val < 0. ? -val : val;
            }

            double threshold(bool bequal, double alpha, double sigma)
            { return bm::quantile(bm::normal(0., sigma), bequal ? 1. - alpha * .5 : alpha); }

            double threshold(bool bequal, double alpha, double sigma, size_t cnt1, size_t cnt2)
            { return threshold(bequal, alpha, sigma)/_cntnorm(cnt1, cnt2); }

            bool isequal(double alpha, double sigma, Input const & left, Input const & right)
            { return value(true, left, right) < threshold(true, alpha, sigma); }
        }

        namespace homoscedastic
        {
            std::pair<size_t, double> value(Input const & left, Input const & right)
            {
                auto oneS  = [](auto const x) { return x.sigma*x.sigma*(x.count-1); };
                auto free  = left.count+right.count-2;
                auto sigma = std::sqrt((oneS(left)+oneS(right))/free);
                auto t     = (left.mean-right.mean) / sigma
                           * _cntnorm(left.count, right.count);
                return {free, t};
            }

            bool isequal(double alpha, Input const & left, Input const & right)
            { return _isequal(alpha, value(left, right)); }

            bool islower(double alpha, Input const & left, Input const & right)
            { return _islower(alpha, value(left, right)); }

            bool isgreater(double alpha, Input const & left, Input const & right)
            { return !_islower(alpha, value(left, right)); }
        }

        namespace heteroscedastic
        {
            std::pair<double,double> value(Input const & left, Input const & right)
            {
                auto sigovern = [](auto const &x)           { return x.sigma*x.sigma/x.count; };
                auto div      = [](auto a, auto const &b)   { return a*a/(b.count-1); };

                auto sonL   = sigovern(left);
                auto sonR   = sigovern(right);
                auto sumson = sonL+sonR;
                auto free   = sumson*sumson/(div(sonL, left)+div(sonR, right));
                auto t      = (left.mean-right.mean) / std::sqrt(sumson);
                return {free, t};
            }

            bool isequal(double alpha, Input const & left, Input const & right)
            { return _isequal(alpha, value(left, right)); }

            bool islower(double alpha, Input const & left, Input const & right)
            { return _islower(alpha, value(left, right)); }

            bool isgreater(double alpha, Input const & left, Input const & right)
            { return !_islower(alpha, value(left, right)); }
        }
    }
}
