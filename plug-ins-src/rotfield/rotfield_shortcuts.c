/** \file rotfield_shortcuts.c
    
    These functions allow shortcut control through keyboard.
    
    \author Adrien Meglio
    
    \version 26/01/07
*/
#ifndef ROTFIELD_SHORTCUTS_C_
#define ROTFIELD_SHORTCUTS_C_

#include <allegro.h>
#include <xvin.h>

/* If you include other plug-ins header do it here*/ 
#include "rotfield.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
// Keyboard control functions

/** Defines a shortcut for key KEY_PLUS_PAD.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_PLUS_PAD(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_PLUS_PAD, 0, short__KEY_PLUS_PAD);
        return D_O_K; 
    }
     
    if (CURRENT_MODE == PIFOC_MODE)
    {   
        PIFOC__up();
    }
    else if (CURRENT_MODE == LINE_MODE)
    {
        AOTF__set_LINE_intensity(CURRENT_LINE,lines[CURRENT_LINE].level+AOTF_FINESSE);
    }
    
    return D_O_K;
}

/** Defines a shortcut for key KEY_MINUS_PAD.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_MINUS_PAD(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_MINUS_PAD, 0, short__KEY_MINUS_PAD);
        return D_O_K;
    }
     
    if (CURRENT_MODE == PIFOC_MODE)
    {   
        PIFOC__down();
    }
    else if (CURRENT_MODE == LINE_MODE)
    {
        AOTF__set_LINE_intensity(CURRENT_LINE,lines[CURRENT_LINE].level-AOTF_FINESSE);
    }
    
    
    return D_O_K;
}

/** Defines a shortcut for key KEY_P.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_P(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_P, 0, short__KEY_P);
        return D_O_K;
    }
    
    CURRENT_MODE = PIFOC_MODE;
        
    return D_O_K;
}

/** Defines a shortcut for key KEY_L.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_L(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_L, 0, short__KEY_L);
        return D_O_K;
    }
    
    CURRENT_MODE = LINE_MODE;
        
    return D_O_K;
}

/** Defines a shortcut for key KEY_1_PAD.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_1_PAD(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_1_PAD, 0, short__KEY_1_PAD);
        return D_O_K;
    }
    
    if (CURRENT_MODE == LINE_MODE)
    {
        CURRENT_LINE = LINE_1;
    }
    
    return D_O_K;
}

/** Defines a shortcut for key KEY_2_PAD.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_2_PAD(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_2_PAD, 0, short__KEY_2_PAD);
        return D_O_K;
    }
    
    if (CURRENT_MODE == LINE_MODE)
    {
        CURRENT_LINE = LINE_2;
    }
    
    return D_O_K;
}
/** Defines a shortcut for key KEY_3_PAD.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_3_PAD(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_3_PAD, 0, short__KEY_3_PAD);
        return D_O_K;
    }
    
    if (CURRENT_MODE == LINE_MODE)
    {
        CURRENT_LINE = LINE_3;
    }
    
    return D_O_K;
}

/** Defines a shortcut for key KEY_G.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_G(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_G, 0, short__KEY_G);
        return D_O_K;
    }
     
    if (CURRENT_MODE == PIFOC_MODE)
    {   
        PIFOC__set_MICRONS();
    }
    else if (CURRENT_MODE == LINE_MODE)
    {
        AOTF__get_LINE_intensity();
    }
    
    return D_O_K;
}

/** Defines a shortcut for key KEY_O.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_O(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_O, 0, short__KEY_O);
        return D_O_K;
    }
    
    if (CURRENT_MODE == LINE_MODE)
    {
        AOTF__switch_LINE();
    }
    
    return D_O_K;
}

/** Defines a shortcut for key KEY_F.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_F(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_F, 0, short__KEY_F);
        return D_O_K;
    }
     
    if (CURRENT_MODE == PIFOC_MODE)
    {   
        PIFOC_FINESSE_MICRONS /= 10;
    }
    else if (CURRENT_MODE == LINE_MODE)
    {   
        AOTF_FINESSE = 0.01;
    }
    
    return D_O_K;
}

/** Defines a shortcut for key KEY_C.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_C(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_C, 0, short__KEY_C);
        return D_O_K;
    }
     
    if (CURRENT_MODE == PIFOC_MODE)
    {   
        PIFOC_FINESSE_MICRONS *= 10;
    }
    else if (CURRENT_MODE == LINE_MODE)
    {   
        AOTF_FINESSE = 0.1;
    }
    
    return D_O_K;
}

/** Defines a shortcut for key KEY_R.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_R(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_R, 0, short__KEY_R);
        return D_O_K;
    }
     
    if (CURRENT_MODE == PIFOC_MODE)
    {   
        //launcher__PIFOC_reference(); /* Enters PIFOC reference mode */
        //win_report("In reference mode");
    }
    else if (CURRENT_MODE == LINE_MODE)
    {   
        
    }
    
    return D_O_K;
}


//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
// Base functions

/** Main function of shortcuts capabilities

\author Adrien Meglio
\version 15/11/06
*/
int	rotfield_shortcuts_main(void)
{
    if(updating_menu_state != 0) return D_O_K;
    
    AOTF_FINESSE = 0.1;
    CURRENT_MODE = PIFOC_MODE;
    
    return D_O_K;
}

#endif



