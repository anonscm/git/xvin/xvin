#ifndef ROTFIELD_H_
#define ROTFIELD_H_

#include "rotfield_menu.h"
#include "AOTF_functions.h"
#include "rotfield_user2nidaq.h"
#include "PIFOC_functions.h"
#include "rotfield_shortcuts.h"
#include "IO.h"

// TYPEDEFs

typedef struct LINE_STATUS
{
    float level; // Percentage of max value (1 is maximum)
    
    int num_points; // Number of data points
    float64* data; // Data points
    int status; // ON of OFF
} l_s ;


PXV_VAR(float64, g_frequency) ; // The rotation frequency
PXV_VAR(int, g_num_points) ; // The resolution of the curve (points per period)
PXV_VAR(float, g_level) ; // The level or amplitude (gives the force and the direction)
PXV_VAR(l_s*, lines);

////////////////////////////////////////////////////////////////
//Functions
////////////////////////////////////////////////////////////////

PXV_FUNC(int, rotfield_main, (int argc, char **argv));
PXV_FUNC(int, rotfield_unload,(int argc, char **argv));


#endif

