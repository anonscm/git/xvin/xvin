/** \file AOTF_functions.c
    \brief Sub-plug-in program for AOTF control with NIDAQ-M 6229 in Xvin.
    
    Controls function generator for different AOTF control situations.
    BEWARE : requests the NIDAQ plugin !
    
    \sa NIDAQ.c
    \author Adrien Meglio
    \version 01/11/2006
*/
#ifndef AOTF_functions_C_
#define AOTF_functions_C_

#include <allegro.h>
#include <xvin.h>

/* If you include other plug-ins header do it here*/ 
#include "rotfield.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
// Control sequences

/*
NOTE : The configuration is such that the user may change the frequency independently of the resolution. 
The user creates a 'lines' structure with a given number of points per period. It is the resolution of the signal. 
Then the user can change the signal frequency via the #define FREQUENCY.
*/

/** Rotating magnetic field.

All lines are sinus-modulated each with a phase shift of 2PI/3.

\author Francesco Mosconi
\version 12/02/2007
*/
int AOTF__rotating_B_all(void)
{
    int i,j;

    float max_frequency =0;
    float mylocfreq = g_frequency;
    static int switch_on[3] = {1,1,1};
    static int num_turns = 0;
    static int direction = 0;
    
    if(updating_menu_state != 0)	return D_O_K;
    
    win_scanf("This function starts a rotating magnetic field\n"
	      "on the 3 OUTPUT lines\n"
	      "Please set: \n"
	      "number of turns %R infinite %r one\n"
	      "direction %R CW %r CCW\n"
	      "frequency (in turns per second): %f\n"
	      "points per period: %d"
	      "lines intensity [-1:1] %f"
	      "line 1 %R OFF %r ON\n"
	      "line 2 %R OFF %r ON\n"
	      "line 3 %R OFF %r ON\n",
	      &num_turns,&direction,
	      &mylocfreq,&g_num_points,&g_level,
	      switch_on,switch_on+1,switch_on+2);

    g_frequency = (float64) mylocfreq; //this is needed because of float to float64 conversion.

    IO__NIDAQ_max_frequency(&max_frequency);

    if(g_frequency*g_num_points*NUMBER_OF_AO_LINES >max_frequency)
      {
	g_frequency = 5;
	g_num_points = 256;
	return win_printf("error! you exceeded the maximum output rate of %d!", max_frequency);
      }
    if(g_level > 1 || g_level < -1)
      {
	g_level = 1;
	return win_printf("error! level should be between -1 and 1! ");
      }


    if (lines == NULL) 
    {
        lines__create_lines_structure();
        win_report("lines structure was re-created");
    }
    
    /* Creates the data sequence for each line*/ 
    for (j = 0 ; j < NUMBER_OF_AO_LINES ; j++)
    {
        lines[j].level = g_level;
        lines[j].status = switch_on[j];
        
        lines[j].num_points = g_num_points;
                
        lines[j].data = (float64*) realloc(lines[j].data,lines[j].num_points*sizeof(float64));
        if (lines[j].data == NULL) return win_report("memory allocation error");
        
        for (i = 0 ; i < lines[j].num_points ; i++)
        {
            lines[j].data[i] = (float64) AOTF_MAX_VOLT*
	      (sin(
		   (double) 2*(direction-0.5)*2*PI*i/lines[j].num_points + //
		   (double) PI*j/NUMBER_OF_AO_LINES + //phase shift  between lines is PI/3
		   (double) PI/(2*NUMBER_OF_AO_LINES))); //add also a phase of PI/6 so that central line is line number 2
        }
    }
      
    //win_report("lines structure updated");  
      
    /* Recreates the complete sequence (all lines merged) to be sent to the card */  
    
    if(num_turns == 0)    DURATION = PLAY_FOREVER;
    else                  DURATION = PLAY_ONCE;

    lines__refresh_all_lines();
    //    lines__sequence__plot();

    return D_O_K;
}

/** Static magnetic field.

All lines are on with a constant I.

\author Francesco Mosconi
\version 12/02/2007
*/
int AOTF__static_B_all(void)
{
    int i,j;

    float max_frequency =0;
    float mylocfreq = g_frequency;
    int switch_on[3] = {1,1,1};

    if(updating_menu_state != 0)	return D_O_K;
    
    win_scanf("This function switches on the magnetic field\n"
	      "on the 3 OUTPUT lines\n"
	      "Please set: \n"
	      "central line intensity [-1:1] %f",
	      &g_level);

    if(g_level > 1 || g_level < -1)
      {
	g_level = 1;
	return win_printf("error! level should be between -1 and 1! ");
      }

    if (lines == NULL) 
    {
        lines__create_lines_structure();
        win_report("lines structure was re-created");
    }
    
    /* Creates the data sequence for each line*/ 
    for (j = 0 ; j < NUMBER_OF_AO_LINES ; j++)
    {
        lines[j].level = g_level;
        lines[j].status = switch_on[j];
        
        lines[j].num_points = 5;
                
        lines[j].data = (float64*) realloc(lines[j].data,lines[j].num_points*sizeof(float64));
        if (lines[j].data == NULL) return win_report("memory allocation error");
        
        for (i = 0 ; i < lines[j].num_points ; i++)
        {
            lines[j].data[i] = (float64) AOTF_MAX_VOLT*(sin((double) PI*j/NUMBER_OF_AO_LINES + (double) PI/(2*NUMBER_OF_AO_LINES)));
        }
    }
      
    //win_report("lines structure updated");  
      
    /* Recreates the complete sequence (all lines merged) to be sent to the card */  
    DURATION = PLAY_ONCE;
    lines__refresh_all_lines();

    return D_O_K;
}

/* Returns a square function.

Reference signal begins with 1 between 0 and duty_cycle*num_points and ends with 0.

\param int num_points The number of points.
\param int phase The actural signal will begin at the position 'phase' of the reference signal. 
\param float duty_cycle The ratio of 'up' to 'up+down'.

\author Adrien Meglio
\version 09/02/07
*/
float64* function__square(int num_points, int phase, float duty_cycle, float amplitude, float offset)
{
    int i;
    float64* function;
    int alpha;
    
    if(updating_menu_state != 0)	return function;
    
    function = (float64*) calloc(num_points,sizeof(float64));
    if (function == NULL) win_report("Memory allocation error !");
    alpha = (int) num_points*duty_cycle;
    
    //win_report("Alpha : %d",alpha);
    
    if (phase < alpha)
    {
        for (i=0 ; i<num_points ; i++)
        {
            if (i+phase<alpha) function[i] = amplitude+offset;
            else if (i+phase<num_points) function[i] = offset;
            else function[i] = amplitude+offset;
        }
    }
    else if (phase >= alpha)
    {
        for (i=0 ; i<num_points ; i++)
        {
            if (i+phase<num_points) function[i] = offset;
            else if (i+phase<num_points+alpha) function[i] = amplitude+offset;
            else function[i] = offset;
        }
    }
    
    return function;
}


/** Switches a line on or off.

this is a switch for lines. The result is either 0 (OFF) or lines.level (ON)

\param int line The number of the line. Beware Line 1 has number 0, etc.
\param int state The state to which switch the line. 0 is OFF and 1 is ON.

\author Adrien Meglio
\version 01/11/2006
*/
int AOTF__sequence__switch_line(int line,int state)
{
    int i;
    
    if(updating_menu_state != 0)	return D_O_K;

    if (lines == NULL) 
    {
        lines__create_lines_structure();
        win_report("lines structures was re-created");
    }
    
    /* Creates the data sequence for the line*/   
    lines[line].num_points = 10; 
    
    lines[line].data = (float64*) realloc(lines[line].data,lines[line].num_points*sizeof(float64));
    if (lines[line].data == NULL) return win_report("memory allocation error");
    
    for (i = 0 ; i < ((int)(lines[line].num_points)) ; i++)
    {
        lines[line].data[i] = (float64) AOTF_MAX_VOLT*lines[line].level; 
    }
    
    /* Recreates the complete sequence (all lines merged) to be sent to the card */  
    DURATION = PLAY_ONCE;
    lines__refresh_all_lines();

    /* Prints into the log file */
    switch (state)
    {
        case OFF :
        win_event("Switched line LINE %d off",line);
        break;
        case ON :
        win_event("Switched line LINE %d on",line);
        break;
    }
    
    return D_O_K;
}


//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
// Shortcut functions

/** Defines a shortcut to switch the current line.

\author Adrien Meglio
\version 26/01/07
*/
int AOTF__switch_LINE(void)
{
    if(updating_menu_state != 0) return D_O_K;
    
    if (lines[CURRENT_LINE].status == ON)
    {   
        lines[CURRENT_LINE].status = OFF;
        AOTF__sequence__switch_line(CURRENT_LINE,OFF);
    } else
    {
        lines[CURRENT_LINE].status = ON;
        AOTF__sequence__switch_line(CURRENT_LINE,ON);
    }
    
    return D_O_K;
} 

/** Sets the line intensity to a given value (between 0 and 1).

\author Adrien Meglio
\version 29/01/07
*/
int AOTF__set_LINE_intensity(int line, float level)
{
    if(updating_menu_state != 0) return D_O_K;
    
    lines[line].level = (float) level;
    win_event("Changed line LINE %d level to %d/100",line,100*level);
    
    DURATION = PLAY_ONCE;
    lines__refresh_all_lines();
    
    return D_O_K;
} 

/** Defines a shortcut to get the current line intensity.

\author Adrien Meglio
\version 26/01/07
*/
int AOTF__get_LINE_intensity(void)
{
    int level = 0;
    
    if(updating_menu_state != 0) return D_O_K;
    
    win_scanf("Set line LINE to what percentage of maximal intensity ? %d",&level);
    
    AOTF__set_LINE_intensity(CURRENT_LINE,level/100);
    
    return D_O_K;
} 

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
// Base functions

int	AOTF_functions_main(void)
{
    if(updating_menu_state != 0) return D_O_K;
    
    return D_O_K;
}

#endif
