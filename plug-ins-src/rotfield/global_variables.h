/** Global variables declarations for the plug-in

This file contains the declaration of all global variables in the plug-in, 
sorted by the .c file where they appear.
This allows all these variables to be used in any .c file in the plug-in 
as long as <<#define global_variables.h>> is declared in the .c file.

\author Adrien Meglio
*/

#ifndef _GLOBAL_VARIABLES_H_
#define _GLOBAL_VARIABLES_H_

// From NIDAQ.C

int CURRENT_MODE;
int CURRENT_LINE; 

// From menu_plug_in.c

MENU dynamic_submenu[32];
MENU switch_on_submenu[32];
MENU switch_off_submenu[32];

// From AOTF_NIDAQ_base.c
TaskHandle CURRENTtaskHandle;

// From AOTF_functions.c

pltreg* ptrg;

int DURATION;
int AOTF_FINESSE;

// From PIFOC_functions.c

float PIFOC_POSITION; /* The percentage of the total course (between 0 and 1) */
float PIFOC_FINESSE_MICRONS; /* The finesse of the displacement in microns */

#endif
