/** Define and typedef declarations for the plug-in

This file contains the declaration of all define and typedef in the plug-in, 
sorted by the .c file where they appear.
This allows all these define to be used in any .c file in the plug-in 
as long as <<#define global_define.h>> is declared in the .c file.

\author Adrien Meglio
*/

#ifndef _GLOBAL_DEFINE_H_
#define _GLOBAL_DEFINE_H_

//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
// DEFINEs

// From AOTF_NIDAQ_base.c

#define NUMBER_OF_LINES 4

#define PIFOC_MODE 1
#define LINE_MODE 2

// From NIDAQ_functions.c

#define AOTF_MAX_VOLT 10.0 // Max possible voltage
/* ATTENTION : Check that figure !!! */

#define PLAY_FOREVER DAQmx_Val_ContSamps
#define PLAY_ONCE DAQmx_Val_FiniteSamps

#define LINE_1 0
#define LINE_2 1
#define LINE_3 2
#define LINE_4 3

#define ON 1
#define OFF 0

// From PIFOC_functions.c

#define NUMBER_OF_AO_LINES 3
#define PIFOC_LINE LINE_4
#define PIFOC_MAX_VOLT 10.0 // Max possible voltage
#define PIFOC_MIN_VOLT 0.0 // Min possible voltage
#define PIFOC_TOTAL_COURSE 10.0 // The percentage-of-course to input-volts multiplication factor
#define PIFOC_COURSE_MICRONS 100 // The total PIFOC displacement range

//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////

#endif
