/** Function declarations for the plug-in

// From IO.c
*/

#ifndef IO_H_
#define IO_H_

//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////

// From IO.c

PXV_FUNC(void, IO__give_PIFOC_percent, (float *position));
PXV_FUNC(void, IO__give_PIFOC_microns, (float *position));

PXV_FUNC(void, IO__give_line_level, (int line, float *level));
PXV_FUNC(void, IO__give_line_status, (int line, int *status));

PXV_FUNC(int, IO_main, (void));


#endif
