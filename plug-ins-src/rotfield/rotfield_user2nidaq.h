/** Function declarations for the plug-in

// From rotfield_user2nidaq.c
*/

#ifndef ROTFIELD_USER2NIDAQ_H_
#define ROTFIELD_USER2NIDAQ_H_

PXV_FUNC(int, lines__sequence__plot, (void));
PXV_FUNC(int, lines__create_lines_structure, (void));
PXV_FUNC(int, lines__refresh_all_lines, (void));
PXV_FUNC(int, lines__initialize_NIDAQ, (void));
PXV_FUNC(int, rotfield_user2nidaq_main, (void));

#endif
