/** Function declarations for the plug-in

// From rotfield_shortcuts.c
*/


#ifndef ROTFIELD_SHORTCUTS_H_
#define ROTFIELD_SHORTCUTS_H_

//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////

// From rotfield_shortcuts.c

PXV_FUNC(int, short__KEY_PLUS_PAD, (void));
PXV_FUNC(int, short__KEY_MINUS_PAD, (void));
PXV_FUNC(int, short__KEY_P, (void));
PXV_FUNC(int, short__KEY_L, (void));
PXV_FUNC(int, short__KEY_1_PAD, (void));
PXV_FUNC(int, short__KEY_2_PAD, (void));
PXV_FUNC(int, short__KEY_3_PAD, (void));
PXV_FUNC(int, short__KEY_G, (void));
PXV_FUNC(int, short__KEY_O, (void));
PXV_FUNC(int, short__KEY_F, (void));
PXV_FUNC(int, short__KEY_C, (void));
PXV_FUNC(int, short__KEY_R, (void));

PXV_FUNC(int, shortcuts_main, (void));

#endif
