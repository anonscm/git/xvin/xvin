/** \file rotfield.c
    \brief main program for rotating magnetic field control with NIDAQ-M 6229 in Xvin.
    
    This file only contains the main function.
    BEWARE : requests the NIDAQ plugin !
    
    \sa NIDAQ.c
    \author Francesco Mosconi (from Adrien Meglio)
    \version 01/11/2006
*/
#ifndef ROTFIELD_C_
#define ROTFIELD_C_

# include <allegro.h>
# include <xvin.h>
#include <winalleg.h>
#include <NIDAQmx.h>

/* If you include other plug-ins header do it here*/ 

/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "rotfield.h"

int	rotfield_main(int argc, char **argv)
{
  if(updating_menu_state != 0) return D_O_K;
  
  rotfield_menu_main();
  
  g_frequency = 100;
  g_num_points = 256;
  g_level = 1;
  
  lines__initialize_NIDAQ(); // Creates the writing task
  
  //win_report("NIDAQ capabilities loaded");
  
  PIFOC_functions_main();
  AOTF_functions_main();
  shortcuts_main();
  IO_main();
  rotfield_user2nidaq_main();
  
  return D_O_K;
}

int	rotfield_unload(int argc, char **argv)
{
  remove_item_to_menu(plot_treat_menu, "rotfield", NULL, NULL);
  remove_item_to_menu(image_treat_menu, "rotfield", NULL, NULL);
  return D_O_K;
}
#endif

