/** Function declarations for the plug-in

This file contains the declaration of all functions in the plug-in, 
sorted by the .c file where they appear and then by the category 
within the file to which they belong.
This allows all the functions to be used in any .c file in the plug-in 
as long as <<#define global_functions.h>> is declared in the .c file.

\author Adrien Meglio
*/

#ifndef _GLOBAL_FUNCTIONS_H_
#define _GLOBAL_FUNCTIONS_H_

//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////

// From shortcuts.c

PXV_FUNC(int, short__KEY_PLUS_PAD, (void));
PXV_FUNC(int, short__KEY_MINUS_PAD, (void));
PXV_FUNC(int, short__KEY_P, (void));
PXV_FUNC(int, short__KEY_L, (void));
PXV_FUNC(int, short__KEY_1_PAD, (void));
PXV_FUNC(int, short__KEY_2_PAD, (void));
PXV_FUNC(int, short__KEY_3_PAD, (void));
PXV_FUNC(int, short__KEY_G, (void));
PXV_FUNC(int, short__KEY_O, (void));
PXV_FUNC(int, short__KEY_F, (void));
PXV_FUNC(int, short__KEY_C, (void));
PXV_FUNC(int, short__KEY_R, (void));

PXV_FUNC(int, shortcuts_main, (void));

// From PIFOC_functions.c

PXV_FUNC(int, PIFOC__set_MICRONS, (void));
PXV_FUNC(int, PIFOC__move, (float pifoc_move));

PXV_FUNC(DWORD WINAPI, thread__PIFOC_reference, (LPVOID lpParam));
PXV_FUNC(int, launcher__PIFOC_reference, (void));

PXV_FUNC(int, PIFOC__up, (void));
PXV_FUNC(int, PIFOC__down, (void));

PXV_FUNC(int, PIFOC_functions_main, (void));

// From IO.c

PXV_FUNC(void, IO__give_PIFOC_percent, (float *position));
PXV_FUNC(void, IO__give_PIFOC_microns, (float *position));

PXV_FUNC(void, IO__give_line_level, (int line, float *level));
PXV_FUNC(void, IO__give_line_status, (int line, int *status));

PXV_FUNC(int, IO_main, (void));


// Imported functions from NIDAQ plugin

PXV_FUNC(int, task__all_close, (void));

//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
// From menu_plug_in.c

// Menu functions
PXV_FUNC(MENU, *commander_base_menu, (void));
PXV_FUNC(int, menu_plug_in_main, (void));

#endif
