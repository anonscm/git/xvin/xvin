/** Function declarations for the plug-in

// From PIFOC_functions.c
*/

#ifndef PIFOC_FUNCTIONS_H_
#define PIFOC_FUNCTIONS_H_

PXV_FUNC(int, PIFOC__set_MICRONS, (void));
PXV_FUNC(int, PIFOC__move, (float pifoc_move));
PXV_FUNC(DWORD WINAPI, thread__PIFOC_reference, (LPVOID lpParam));
PXV_FUNC(int, launcher__PIFOC_reference, (void));
PXV_FUNC(int, PIFOC__up, (void));
PXV_FUNC(int, PIFOC__down, (void));
PXV_FUNC(int, PIFOC_functions_main, (void));

#endif
