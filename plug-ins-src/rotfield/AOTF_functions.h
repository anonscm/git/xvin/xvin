/** Function declarations for the plug-in

// From AOTF_functions.c
*/

#ifndef AOTF_FUNCTIONS_H_
#define AOTF_FUNCTIONS_H_

PXV_FUNC(int, AOTF__rotating_B_all, (void));
PXV_FUNC(int, AOTF__static_B_all, (void));
PXV_FUNC(int, AOTF__sequence__switch_line, (int line, int state));

PXV_FUNC(float64*, function__square, (int num_points, int phase, float duty_cycle, float amplitude, float offset));

PXV_FUNC(int, AOTF__switch_LINE, (void));
PXV_FUNC(int, AOTF__set_LINE_intensity, (int line, float level));
PXV_FUNC(int, AOTF__get_LINE_intensity, (void));

PXV_FUNC(int, AOTF_functions_main, (void));

#endif
