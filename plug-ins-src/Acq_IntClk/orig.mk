NIDAQMX_DIR = /c/Program\ Files/National\ Instruments/NI-DAQ/DAQmx\ ANSI\ C\ Dev
NAME	    		= Acq-IntClk
LCFLAGS 		= -I$(NIDAQMX_DIR)/include

all: $(NAME).c $(NIDAQMX_DIR)/lib/libnidaqmx.a
	gcc -o $(NAME).exe $(NAME).c $(LCFLAGS) $(NIDAQMX_DIR)/lib/libnidaqmx.a
