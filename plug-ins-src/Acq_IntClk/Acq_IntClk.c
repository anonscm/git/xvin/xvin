/*
*    Simple Plug-in program for Finite Analog Acquisition in Xvin winth NIDAQMX.
 *
 *    F. Mosconi
  */
#ifndef _ACQ_INTCLK_C_
#define _ACQ_INTCLK_C_

# include "allegro.h"
# include "xvin.h"

#include <stdio.h>
#include <NIDAQmx.h>

/* If you include other plug-ins header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "Acq_IntClk.h"

#define DAQmxErrChk(functionCall) if( DAQmxFailed(error=(functionCall)) ) goto Error; else


int Acq_IntClk(int np, float64 smplfreq, float64 *data)
{
	int32       error=0;
	TaskHandle  taskHandle=0;
	int32       read;
	char        errBuff[2048]={'\0'};
	int samps_per_chan = np;

	/*********************************************/
	// DAQmx Configure Code
	/*********************************************/
	DAQmxErrChk (DAQmxCreateTask("",&taskHandle));
	DAQmxErrChk (DAQmxCreateAIVoltageChan(taskHandle,"Dev1/ai7","",DAQmx_Val_Cfg_Default,-10.0,10.0,DAQmx_Val_Volts,NULL));
	DAQmxErrChk (DAQmxCfgSampClkTiming(taskHandle,"",smplfreq,DAQmx_Val_Rising,DAQmx_Val_FiniteSamps,np));

	/*********************************************/
	// DAQmx Start Code
	/*********************************************/
	DAQmxErrChk (DAQmxStartTask(taskHandle));

	/*********************************************/
	// DAQmx Read Code
	/*********************************************/
	DAQmxErrChk (DAQmxReadAnalogF64(taskHandle,samps_per_chan,10.0,DAQmx_Val_GroupByChannel,data,np,&read,NULL));

	win_printf("Acquired %d points\n",read);

Error:
	if( DAQmxFailed(error) )
		DAQmxGetExtendedErrorInfo(errBuff,2048);
	if( taskHandle!=0 )  {
		/*********************************************/
		// DAQmx Stop Code
		/*********************************************/
		DAQmxStopTask(taskHandle);
		DAQmxClearTask(taskHandle);
	}
	if( DAQmxFailed(error) )
		win_printf("DAQmx Error: %s\n",errBuff);
	return 0;
}

int do_Acq_IntClk(void)
{
	register int i;
	pltreg *pr = NULL;
	O_p *op = NULL;
	d_s *ds;
	float64 *data = NULL;
	int np = 250000;
	float64 smplfreq = 250000; //250kHz is maximum allowed with one channel

	if(updating_menu_state != 0)	return D_O_K;

	if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
	return D_O_K;

	data = (float64*) calloc(np,sizeof(float64));

	Acq_IntClk(np, smplfreq, data);
	
	op = create_and_attach_one_plot(pr,np,np,0);
	ds = op->dat[0];

	for(i = 0 ; i < np ; i++){
	  ds->xd[i] = (float) i/np;
	  ds->yd[i] = (float) data[i];
	}
	

	return D_O_K;
}


MENU *Acq_IntClk_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"Acq_IntClk", do_Acq_IntClk,NULL,0,NULL);
	return mn;
}

int	Acq_IntClk_main(int argc, char **argv)
{
	add_plot_treat_menu_item ( "Acq_IntClk", NULL, Acq_IntClk_menu(), 0, NULL);
	return D_O_K;
}

int	Acq_IntClk_unload(int argc, char **argv)
{
	remove_item_to_menu(plot_treat_menu, "Acq_IntClk", NULL, NULL);
	return D_O_K;
}
#endif

