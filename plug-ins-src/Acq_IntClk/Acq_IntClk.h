#ifndef _AACQ_H_
#define _AACQ_H_

PXV_FUNC(int, do_Acq_IntClk, (void));
PXV_FUNC(MENU*, Acq_IntClk_menu, (void));
PXV_FUNC(int, Acq_IntClk_main, (int argc, char **argv));
#endif

