#ifndef _FJCA_H_
#define _FJCA_H_


typedef struct polymer_chain
{
  int niter, n_test, i_iter, mode;
  int np, kp, rottrial, rottriale, rotrefa, rotrefe, cranktrial, crankref;			/* number of spere */
  float *x, *y, *z;			/* the x and y data */
  float *xn, *yn, *zn;			/* the x and y data */
  fixed *fx, *fy, *fz;			/* the x and y data */
  fixed *fxn, *fyn, *fzn;			/* the x and y data */
  float *xr, *yr, *zr;	
  double B; // bending energy
  double Eb, Ebn;
} p_c;


int fjca_3D_plot(p_c *pc);
int	do_3D_chain_draw(void);




PXV_FUNC(int,	selfavoid, (int pt_interest, fixed *x, fixed *y, fixed *z, int nn, fixed dmin, fixed dmin2));
PXV_FUNC(int,   partialrot, (p_c *pc));
PXV_FUNC(int,	crankshaft, (p_c *pc, int *pt0, int *pt1));
PXV_FUNC(int,   spit_length_of_each_segment, (p_c *pc));
PXV_FUNC(int,   Metropolis, (p_c *pc, float f));
PXV_FUNC(p_c*,  initial_configuration, (int np, float dmin));
PXV_FUNC(int,	monte_carlo, (p_c *pc, float f, float proba, fixed dmin, fixed dmin2));
PXV_FUNC(int,   fjca_plot, (s_t *st, p_c *pc));
PXV_FUNC(int,   do_fjca_rescale_data_set, (void));
PXV_FUNC(int,   do_fjca_rescale_plot, (void));

PXV_FUNC(double, compute_bending_energy_new_ch, (p_c *pc));
PXV_FUNC(double, compute_bending_energy_cur_ch, (p_c *pc));

PXV_FUNC(int, do_fjca_rescale_plot, (void));
PXV_FUNC(MENU*, fjca_plot_menu, (void));
PXV_FUNC(int, do_fjca_rescale_data_set, (void));
PXV_FUNC(int, fjca_main, (int argc, char **argv));
#endif



