/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _FJCA_C_
#define _FJCA_C_

# include "allegro.h"
# include "winalleg.h"
# include "xvin.h"
//# include "largeint.h"

/* If you include other plug-ins header do it here*/
# include "../stat/stat.h"
# include "../nrutil/nrutil.h"


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "fjca.h"

//# define LARGE_INTEGER (long long)
int call_3D_indirect = 0;

FILE *log_out = NULL;

long idum = 4587;
p_c *pc = NULL;
s_t *zst = NULL;
s_t *zst2 = NULL;

static float force = 1, proba = 0.3, dmin = 1;
static int nm = 100, niter = 1000, n_test = 100;//, i_iter = 0;
fixed fdmin, fdmin2;
float theta = 1,phi = 1;

int  nselfavoid = 0, npartialrot = 0, ncrankshaft = 0;

float	lselfavoid = 0;
int	nlselfavoid = 0;
float tau_rota, tau_rote, tau_crank;

int change_angle_in_3D_plot_region(void);

float volo_min = 0.001;

int plasmid = 0;

/* from start included to stop excluded */
int	multi_selfavoid(int start, int stop, fixed *x, fixed *y, fixed *z, int nn, fixed dmin,
			fixed dmin2)
{
	register int i, j, pti;
	fixed xi, yi,zi;
	fixed xj, yj, zj, d;

	for (pti = start; pti < stop; pti++)
	{
		xi = x[pti];
		yi = y[pti];
		zi = z[pti];
		for (i = 0; i < nn; ) /* i++  i <= j-2 && */
		{
			if (abs(i-pti) > 1)
			{
				xj = x[i] - xi;
				yj = y[i] - yi;
				zj = z[i] - zi;
				d = fmul(xj,xj) + fmul(yj,yj) + fmul(zj,zj);
				if (d < dmin2)
				{
					return 0;
				}
				else
				{
					j = fixtoi(fsqrt(d));
					i += (j>0) ? j : 1;
				}
			}
			else i = pti + 2;
		}
	}
	return 1;
}

double compute_bending_energy_new_ch(p_c *pc)
{
  register int i, j, k;
  double tmp, E;

  for (i = 0, E = 0; i < pc->np-2; i++)
    {
      j = i+1;
      k = j+1;
      tmp = 1;
      tmp -= (pc->xn[k] - pc->xn[j]) * (pc->xn[j] - pc->xn[i]);
      tmp -= (pc->yn[k] - pc->yn[j]) * (pc->yn[j] - pc->yn[i]);
      tmp -= (pc->zn[k] - pc->zn[j]) * (pc->zn[j] - pc->zn[i]);
      E += pc->B * tmp;
    }
  pc->Ebn = E;
  return E;
}


double compute_bending_energy_cur_ch(p_c *pc)
{
  register int i, j, k;
  double tmp, E;

  for (i = 0, E = 0; i < pc->np-2; i++)
    {
      j = i+1;
      k = j+1;
      tmp = 1;
      tmp -= (pc->x[k] - pc->x[j]) * (pc->x[j] - pc->x[i]);
      tmp -= (pc->y[k] - pc->y[j]) * (pc->y[j] - pc->y[i]);
      tmp -= (pc->z[k] - pc->z[j]) * (pc->z[j] - pc->z[i]);
      E += pc->B * tmp;
    }
  pc->Eb = E;
  return E;
}


int	selfavoid(int pt_interest, fixed *x, fixed *y, fixed *z, int nn, fixed dmin, fixed dmin2)
{
	register int i, j;
	fixed xi, yi,zi;
	fixed xj, yj, zj, d;

	xi = x[pt_interest];
	yi = y[pt_interest];
	zi = z[pt_interest];
	for (i = 0; i < nn; ) /* i++  i <= j-2 && */
	{
		if (abs(i-pt_interest) > 1)
		{
			xj = x[i] - xi;
			yj = y[i] - yi;
			zj = z[i] - zi;
			d = fmul(xj,xj) + fmul(yj,yj) + fmul(zj,zj);
/*
			lselfavoid += d;
			nlselfavoid++;
*/
			if (d < dmin2)
			{
				return 0;
			}
			else
			{
				j = fixtoi(fsqrt(d));
/*
				lselfavoid += j;
				nlselfavoid++;
*/
				i += (j>0) ? j : 1;
			}
		}
		else i = pt_interest + 2;
	}
	return 1;
}


int	hide_trial(void)
{
  pltreg *pr;
  O_p *op;

  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
    return win_printf("cannot find data");
  if (op->n_dat > 2)  op->n_dat = 2;
  return 0;
}

/*
     dynamique: on prend au hasard un maillon de la chaine : i, et une
     direction de rotation ; on ne tourne que les maillons pour
     lesquels j>i  autour de cette axe de rotation.
*/

double do_compute_wirthe(p_c *pc)
{
	register int l;
	int np;
	float *x, *y, *z;
	double re, im, p, x_p, y_p, z_p, x_n, y_n, z_n;

	x = pc->x;	y = pc->y;	z = pc->z;
	np = pc->np;

	for(l = 1, p = 0; l < np-1 ; l++)
	{
		x_p = x[l]-x[l-1];
		y_p = y[l]-y[l-1];
		z_p = z[l]-z[l-1];
		x_n = x[l+1]-x[l];
		y_n = y[l+1]-y[l];
		z_n = z[l+1]-z[l];
		re = x_p * x_n + y_n * y_p;
		im = y_p * x_n - x_p * y_n;
		p += atan2(im,re)*(1-(z_p+z_n)/2);
	}
	return	p/(2*M_PI);
}
double do_compute_wirthe_n(p_c *pc)
{
	register int l;
	int np;
	float *x, *y, *z;
	double re, im, p, x_p, y_p, z_p, x_n, y_n, z_n;

	x = pc->xn;	y = pc->yn;	z = pc->zn;
	np = pc->np;

	for(l = 1, p = 0; l < np-1 ; l++)
	{
		x_p = x[l]-x[l-1];
		y_p = y[l]-y[l-1];
		z_p = z[l]-z[l-1];
		x_n = x[l+1]-x[l];
		y_n = y[l+1]-y[l];
		z_n = z[l+1]-z[l];
		re = x_p * x_n + y_n * y_p;
		im = y_p * x_n - x_p * y_n;
		p += atan2(im,re)*(1-(z_p+z_n)/2);
	}
	return	p/(2*M_PI);
}
int wrn_(int *jr1, double *beta, double *x, double *y, double *z, double *dx, double *dy, double *dz)
{
    /* Initialized data */
    static int lend = 1;
    /* System generated locals */
    int i1, i2;
    /* Local variables */
    static int i, k;
    static double f1, f2, f3, f4, ab, ai, ak, rx, ry, rz, zn1, zn2, zn3,
	    zn4, abc, cal, abq, sql, www, bllb, blli, bllk;

/*     Writhing number calculation for linear chain. */
/*     Cartesian coordinates of chain segments are x(i),y(i),z(i). */
/*     DX, DY, and DZ are arrays of bond vectors, ie DX(i) = X(i+1)-X(i). */
    www = 0;
    i1 = *jr1 - lend - 2;
    for (i = lend; i < i1; ++i)
      {
	i2 = *jr1 - lend;
	for (k = i + 1; k < i2; ++k)
	  {
	    bllb = (x[i - 1] - x[k - 1]) * (dy[k - 1] * dz[i - 1] - dy[i - 1] * dz[k - 1])
	         + (y[i - 1] - y[k - 1]) * (dz[k - 1] * dx[i - 1] - dz[i - 1] * dx[k - 1])
	         + (z[i - 1] - z[k - 1]) * (dx[k - 1] * dy[i - 1] - dx[i - 1] * dy[k - 1]);
	    cal = dx[k - 1] * dx[i - 1] + dy[k - 1] * dy[i - 1] + dz[k - 1] * dz[i - 1];
	    sql = 1. - cal * cal;
	    if (sql <= .001)  continue;
	    rx = x[k] - x[i];
	    ry = y[k] - y[i];
	    rz = z[k] - z[i];
	    zn1 = sqrt(rx * rx + ry * ry + rz * rz);
	    rx = x[k] - x[i - 1];
	    ry = y[k] - y[i - 1];
	    rz = z[k] - z[i - 1];
	    zn2 = sqrt(rx * rx + ry * ry + rz * rz);
	    rx = x[k - 1] - x[i];
	    ry = y[k - 1] - y[i];
	    rz = z[k - 1] - z[i];
	    zn3 = sqrt(rx * rx + ry * ry + rz * rz);
	    rx = x[k - 1] - x[i - 1];
	    ry = y[k - 1] - y[i - 1];
	    rz = z[k - 1] - z[i - 1];
	    zn4 = sqrt(rx * rx + ry * ry + rz * rz);
	    bllk = (x[i - 1] - x[k - 1]) * dx[k - 1]
	         + (y[i - 1] - y[k - 1]) * dy[k - 1]
	         + (z[i - 1] - z[k - 1]) * dz[k - 1];
	    blli = (x[i - 1] - x[k - 1]) * dx[i - 1]
	         + (y[i - 1] - y[k - 1]) * dy[i - 1]
	         + (z[i - 1] - z[k - 1]) * dz[i - 1];
	    ak = (bllk - blli * cal) / sql;
	    ai = (bllk * cal - blli) / sql;
	    ab = bllb / sql;
	    abq = ab * ab;
	    abc = abq * cal;
	    f1 = -atan(((1.0 - ak) * (1.0 - ai) + abc) / (ab * zn1));
	    f2 = -atan(((1.0 - ak) * (-ai) + abc) / (ab * zn2));
	    f3 = -atan((-ak * (1.0 - ai) + abc) / (ab * zn3));
	    f4 = -atan((-ak * (-ai) + abc) / (ab * zn4));
	    www = www + f1 - f2 - f3 + f4;
	}
    }
    *beta = www / (2*M_PI);// 6.2831852f;
    return 0;
} /* wrn_ */
int wrn_gr(int *jr1, double *beta, double *x, double *y, double *z, double *dx, double *dy, double *dz)
{
    /* Initialized data */
    static int lend = 1;
    /* System generated locals */
    int i1, i2;
    /* Local variables */
    static int i, k, nr = 0;
    static double f1, f2, f3, f4, ab, ai, ak, rx, ry, rz, zn1, zn2, zn3,
	    zn4, abc, cal, abq, sql, www, bllb, blli, bllk;
    pltreg *pr;
    O_p *op;
    d_s *ds, *ds2, *ds3;


    if ((pr = find_pr_in_current_dialog(NULL)) == NULL)
      return win_printf("cannot find pr");

    op = create_and_attach_one_plot(pr, *jr1, *jr1, 0);
    ds = op->dat[0];
    if (ds == NULL)		return win_printf("can't create ds");


    ds2 = create_and_attach_one_ds(op,*jr1,*jr1,1);
    if (ds2 == NULL)		return win_printf("can't create ds");

    ds3 = create_and_attach_one_ds(op,*jr1,*jr1,1);
    if (ds3 == NULL)		return win_printf("can't create ds");

    i = win_scanf("enter the index of the integral to record %d",&nr);
    if (i == CANCEL)  return D_O_K;

/*     Writhing number calculation for linear chain. */
/*     Cartesian coordinates of chain segments are x(i),y(i),z(i). */
/*     DX, DY, and DZ are arrays of bond vectors, ie DX(i) = X(i+1)-X(i). */
    www = 0;
    i1 = *jr1 - lend - 2;
    for (i = lend; i < i1; ++i)
      {
	ds->yd[i] = 0;
	ds->xd[i] = ds3->xd[i] = i;
	ds3->yd[i] = dz[i];
	i2 = *jr1 - lend;
	for (k = lend; k < i2; ++k)
	  {
	    if (i == nr)
	      {
		ds2->xd[k] = k;
	      }

	    bllb = (x[i - 1] - x[k - 1]) * (dy[k - 1] * dz[i - 1] - dy[i - 1] * dz[k - 1])
	         + (y[i - 1] - y[k - 1]) * (dz[k - 1] * dx[i - 1] - dz[i - 1] * dx[k - 1])
	         + (z[i - 1] - z[k - 1]) * (dx[k - 1] * dy[i - 1] - dx[i - 1] * dy[k - 1]);
	    cal = dx[k - 1] * dx[i - 1] + dy[k - 1] * dy[i - 1] + dz[k - 1] * dz[i - 1];
	    sql = 1. - cal * cal;
	    if (sql <= .001)  continue;
	    rx = x[k] - x[i];
	    ry = y[k] - y[i];
	    rz = z[k] - z[i];
	    zn1 = sqrt(rx * rx + ry * ry + rz * rz);
	    rx = x[k] - x[i - 1];
	    ry = y[k] - y[i - 1];
	    rz = z[k] - z[i - 1];
	    zn2 = sqrt(rx * rx + ry * ry + rz * rz);
	    rx = x[k - 1] - x[i];
	    ry = y[k - 1] - y[i];
	    rz = z[k - 1] - z[i];
	    zn3 = sqrt(rx * rx + ry * ry + rz * rz);
	    rx = x[k - 1] - x[i - 1];
	    ry = y[k - 1] - y[i - 1];
	    rz = z[k - 1] - z[i - 1];
	    zn4 = sqrt(rx * rx + ry * ry + rz * rz);
	    bllk = (x[i - 1] - x[k - 1]) * dx[k - 1]
	         + (y[i - 1] - y[k - 1]) * dy[k - 1]
	         + (z[i - 1] - z[k - 1]) * dz[k - 1];
	    blli = (x[i - 1] - x[k - 1]) * dx[i - 1]
	         + (y[i - 1] - y[k - 1]) * dy[i - 1]
	         + (z[i - 1] - z[k - 1]) * dz[i - 1];
	    ak = (bllk - blli * cal) / sql;
	    ai = (bllk * cal - blli) / sql;
	    ab = bllb / sql;
	    abq = ab * ab;
	    abc = abq * cal;
	    f1 = -atan(((1.0 - ak) * (1.0 - ai) + abc) / (ab * zn1));
	    f2 = -atan(((1.0 - ak) * (-ai) + abc) / (ab * zn2));
	    f3 = -atan((-ak * (1.0 - ai) + abc) / (ab * zn3));
	    f4 = -atan((-ak * (-ai) + abc) / (ab * zn4));
	    www = www + f1 - f2 - f3 + f4;
	    ds->yd[i] += f1 - f2 - f3 + f4;
	    if (i == nr)
	      {
		ds2->yd[k] = f1 - f2 - f3 + f4;
	      }


	}
    }
    *beta = www / (4*M_PI);// 6.2831852f;
    return 0;
} /* wrn_ */
int wrn_gauss(int *np, double *beta, double *x, double *y, double *z, double *dx, double *dy, double *dz)
{
    /* Initialized data */
    static int lend = 0;
    /* System generated locals */
    int i1, i2;
    /* Local variables */
    static int i, k;
    static double rx, ry, rz, www, zn1, bllb;

/*     Writhing number calculation for linear chain. */
/*     Cartesian coordinates of chain segments are x(i),y(i),z(i). */
/*     DX, DY, and DZ are arrays of bond vectors, ie DX(i) = X(i+1)-X(i). */
    www = 0;
    i1 = *np - lend ;
    for (i = lend; i < i1; ++i)
      {
	i2 = *np - lend;
	for (k = lend; k < i2; ++k)
	  {
	    if (i == k) continue;
	    rx = x[k] - x[i];
	    ry = y[k] - y[i];
	    rz = z[k] - z[i];
	    zn1 = rx * rx + ry * ry + rz * rz;
	    bllb = dx[i] * (dy[k] * rz - dz[k] * ry)
	         + dy[i] * (dz[k] * rx - dx[k] * rz)
	         + dz[i] * (dx[k] * ry - dy[k] * rx);
	    zn1 *= sqrt(zn1);
	    if (zn1 != 0) www += bllb/zn1;
	}
    }
    *beta = www / (4*M_PI);// 6.2831852f;
    return 0;
} /* wrn_ */


int wrn_gauss_vologo(int *np, double *beta, double *x, double *y, double *z, double *dx, double *dy, double *dz)
{
    /* Initialized data */
    static int lend = 0;
    /* System generated locals */
    int i1, i2;
    /* Local variables */
    static int i, k;
    static double rx, ry, rz, www, zn1, bllb;

/*     Writhing number calculation for linear chain. */
/*     Cartesian coordinates of chain segments are x(i),y(i),z(i). */
/*     DX, DY, and DZ are arrays of bond vectors, ie DX(i) = X(i+1)-X(i). */
    www = 0;
    i2 = *np - lend;
    i1 = i2;
    for (i = lend; i < i1; ++i)
      {
	for (k = i+1; k < i2; ++k)
	  {
	    if (i == k) continue;
	    rx = x[k] - x[i];
	    ry = y[k] - y[i];
	    rz = z[k] - z[i];
	    zn1 = rx * rx + ry * ry + rz * rz;
	    bllb = dx[i] * (dy[k] * rz - dz[k] * ry)
	         + dy[i] * (dz[k] * rx - dx[k] * rz)
	         + dz[i] * (dx[k] * ry - dy[k] * rx);
	    zn1 *= sqrt(zn1);
	    if (zn1 != 0) www += bllb/zn1;
	}
    }
    *beta = -www / (2*M_PI);// 6.2831852f;
    return 0;
} /* wrn_ */

int wrn_gauss_gr(int *np, double *beta, double *x, double *y, double *z, double *dx, double *dy, double *dz)
{
    /* Initialized data */
    static int lend = 0, nr = 0;
    /* System generated locals */
    int i1, i2;
    /* Local variables */
    static int i, k;
    static double rx, ry, rz, www, zn1, bllb;
    pltreg *pr;
    O_p *op;
    d_s *ds, *ds2, *ds3;

/*     Writhing number calculation for linear chain. */
/*     Cartesian coordinates of chain segments are x(i),y(i),z(i). */
/*     DX, DY, and DZ are arrays of bond vectors, ie DX(i) = X(i+1)-X(i). */

    if ((pr = find_pr_in_current_dialog(NULL)) == NULL)
      return win_printf("cannot find pr");

    op = create_and_attach_one_plot(pr, *np, *np, 0);
    ds = op->dat[0];
    if (ds == NULL)		return win_printf("can't create ds");


    ds2 = create_and_attach_one_ds(op,*np,*np,1);
    if (ds2 == NULL)		return win_printf("can't create ds");

    ds3 = create_and_attach_one_ds(op,*np,*np,1);
    if (ds3 == NULL)		return win_printf("can't create ds");

    i = win_scanf("enter the index of the integral to record %d",&nr);
    if (i == CANCEL)  return D_O_K;

    www = 0;
    i1 = *np - lend ;
    for (i = lend; i < i1; ++i)
      {
	ds->yd[i] = 0;
	ds->xd[i] = ds3->xd[i] = i;
	ds3->yd[i] = dz[i];
	i2 = *np - lend;
	for (k = lend; k < i2; ++k)
	  {
	    if (i == nr)
	      {
		ds2->xd[k] = k;
	      }
	    if (i == k) continue;
	    rx = x[k] - x[i];
	    ry = y[k] - y[i];
	    rz = z[k] - z[i];
	    zn1 = rx * rx + ry * ry + rz * rz;

	    bllb = dx[k] * (dy[i] * rz - dz[i] * ry)
	         + dy[k] * (dz[i] * rx - dx[i] * rz)
	         + dz[k] * (dx[i] * ry - dy[i] * rx);
	    zn1 *= sqrt(zn1);
	    if (zn1 != 0) www += bllb/zn1;
	    if (zn1 != 0) ds->yd[i] += bllb/zn1;
	    if (i == nr)
	      {
		if (zn1 != 0) ds2->yd[k] = bllb/zn1;
		else ds2->yd[k] = -2;
	      }

	}
    }
    *beta = www / (4*M_PI);// 6.2831852f;
    return 0;
} /* wrn_ */


int wrn_gauss_gr1(int *np, double *beta, double *x, double *y, double *z, double *dx, double *dy, double *dz)
{
    /* Initialized data */
    static int lend = 0, nr = 0;
    /* System generated locals */
    int i1, i2;
    /* Local variables */
    static int i, k;
    static double rx, ry, rz, www, zn1, bllb;
    pltreg *pr;
    O_p *op;
    d_s *ds, *ds2, *ds3;

/*     Writhing number calculation for linear chain. */
/*     Cartesian coordinates of chain segments are x(i),y(i),z(i). */
/*     DX, DY, and DZ are arrays of bond vectors, ie DX(i) = X(i+1)-X(i). */

    if ((pr = find_pr_in_current_dialog(NULL)) == NULL)
      return win_printf("cannot find pr");

    op = create_and_attach_one_plot(pr, *np, *np, 0);
    ds = op->dat[0];
    if (ds == NULL)		return win_printf("can't create ds");


    ds2 = create_and_attach_one_ds(op,*np,*np,1);
    if (ds2 == NULL)		return win_printf("can't create ds");

    ds3 = create_and_attach_one_ds(op,*np,*np,1);
    if (ds3 == NULL)		return win_printf("can't create ds");

    i = win_scanf("enter the index of the integral to record %d",&nr);
    if (i == CANCEL)  return D_O_K;

    www = 0;
    i1 = *np - lend ;
    for (i = lend; i < i1; ++i)
      {
	ds->yd[i] = 0;
	ds->xd[i] = ds3->xd[i] = i;
	ds3->yd[i] = dz[i];
	i2 = *np - lend;
	for (k = lend; k < i2; ++k)
	  {
	    if (i == nr)
	      {
		ds2->xd[k] = k;
	      }
	    if (i == k) continue;
	    rx = x[k] - x[i];
	    ry = y[k] - y[i];
	    rz = z[k] - z[i];
	    zn1 = rx * rx + ry * ry + rz * rz;
	    bllb = dx[i] * (dy[k] * rz - dz[k] * ry)
	         + dy[i] * (dz[k] * rx - dx[k] * rz)
	         + dz[i] * (dx[k] * ry - dy[k] * rx);
	    zn1 *= sqrt(zn1);
	    if (zn1 != 0) www += bllb/zn1;
	    if (zn1 != 0) ds->yd[i] += bllb/zn1;
	    if (i == nr)
	      {
		if (zn1 != 0) ds2->yd[k] = bllb/zn1;
		else ds2->yd[k] = -2;
	      }

	}
    }
    *beta = www / (4*M_PI);// 6.2831852f;
    return 0;
} /* wrn_ */

int wrn_vc(int *jr1, double *beta, double *x, double *y, double *z, double *dx, double *dy, double *dz)
{
    /* Local variables */
    register int i, k;
    /* Initialized data */
    static int lend = 1; //1
    /* System generated locals */
    static int i1, i2;
    static int nttt = 0;
    static double f1, ab, ai, ak, rx, ry, rz, zn1, zn2, zn3, zn4, abc, cal, sql, bllb, blli, bllk;
    static double ret, imt, rett = 1, imtt = 0;
    static double *dxi1, *dyi1, *dzi1;
    static double *dxk1, *dyk1, *dzk1;
    static double *xi, *yi, *zi, *xi1, *yi1, *zi1;
    static double *xk1, *yk1, *zk1;


/*     Writhing number calculation for linear chain. */
/*     Cartesian coordinates of chain segments are x(i),y(i),z(i). */
/*     DX, DY, and DZ are arrays of bond vectors, ie DX(i) = X(i+1)-X(i). */
    nttt = 0;
    i2 = *jr1 - lend;
    i1 = i2 - 2;
    i = lend - 1;
    rett = 1;
    imtt = 0;
    xi = xi1 = x + i; yi = yi1 = y + i; zi = zi1 = z + i;
    dxi1 = dx + i; dyi1 = dy + i; dzi1 = dz + i;
    for (i = lend; i < i1; ++i)
      {
	xi++; yi++; zi++;

	dxk1 = dx + i; dyk1 = dy + i; dzk1 = dz + i;
        xk1 = xi; yk1 = yi; zk1 = zi;

	rx = *xk1 - *xi;   ry = *yk1 - *yi;   rz = *zk1 - *zi;
	 zn1 = sqrt(rx * rx + ry * ry + rz * rz);
        rx = *xi1 - *xk1;    ry = *yi1 - *yk1;    rz = *zi1 - *zk1;
	 zn2 = sqrt(rx * rx + ry * ry + rz * rz);


	for (k = i + 1; k < i2; ++k)
	  {
	    cal = *dxk1 * *dxi1 + *dyk1 * *dyi1 + *dzk1 * *dzi1;
	    sql = 1. - cal * cal;

	    zn4 = zn2; // computed before
	    zn3 = zn1; // same
	    // rx should be defined
	    bllk = rx * *dxk1 + ry * *dyk1 + rz * *dzk1;
	    blli = rx * *dxi1 + ry * *dyi1 + rz * *dzi1;
	    bllb = rx * (*dyk1 * *dzi1 - *dyi1 * *dzk1)
	         + ry * (*dzk1 * *dxi1 - *dzi1 * *dxk1)
	         + rz * (*dxk1 * *dyi1 - *dxi1 * *dyk1);

	    dxk1++; dyk1++; dzk1++;
	    xk1++; yk1++; zk1++;

	    rx = *xk1 - *xi;    ry = *yk1 - *yi;    rz = *zk1 - *zi;
	    zn1 = sqrt(rx * rx + ry * ry + rz * rz);

	    rx = *xi1 - *xk1;   ry = *yi1 - *yk1;   rz = *zi1 - *zk1;
	    zn2 = sqrt(rx * rx + ry * ry + rz * rz);

	    //if (sql <= .001)  continue;
	    if (sql <= volo_min)  continue;
	    sql = 1.0/sql;

	    ak = (bllk - blli * cal) * sql;
	    ai = (bllk * cal - blli) * sql;
	    ab = bllb * sql;
	    abc = (ab * ab) * cal;

	    ret = -((1.0 - ak) * (1.0 - ai) + abc);
	    imt = -(ab * zn1);

	    cal = -((ak - 1.0) * ai + abc);
	    sql = ab * zn2;
	    bllb = ret * cal - imt * sql;
	    imt = ret * sql + imt * cal;
	    ret = bllb;

	    cal = -(ak * (ai - 1.0) + abc);
	    sql = ab * zn3;
	    bllb = ret * cal - imt * sql;
	    imt = ret * sql + imt * cal;
	    ret = bllb;

	    cal = -(ak * ai + abc);
	    sql = -ab * zn4;

	    bllb = ret * cal - imt * sql;
	    imt = ret * sql + imt * cal;
	    ret = bllb;

	    bllb = ret * rett - imt * imtt;
	    blli = ret * imtt + imt * rett;

	    f1 = sqrt(bllb * bllb + blli * blli);
	    if (f1 != 0)
	    {
		f1 = 1.0/f1;
		bllb =  f1 * bllb;
		blli =  f1 * blli;
	    }
	    else
	    {
	      bllb = rett;
	      blli = imtt;
	    }
	    if (bllb < 0)
	      {
		if (imtt >= 0 && blli < 0) nttt++;
		else if (imtt <= 0 && blli > 0) nttt--;
	      }
	    rett = bllb; imtt = blli;
	}
	xi1++; yi1++; zi1++;
	dxi1++; dyi1++; dzi1++;
    }
    *beta = atan2(imtt,rett)/(2*M_PI);
    *beta += nttt;
    return 0;
} /* wrn_ */
int wrn_vcOK1(int *jr1, double *beta, double *x, double *y, double *z, double *dx, double *dy, double *dz)
{
    /* Initialized data */
    int lend = 1;
    /* System generated locals */
    int i1, i2;
    /* Local variables */
    register int i, k, i_1, k_1;
    double f1, ab, ai, ak, rx, ry, rz, zn1, zn2, zn3, zn4, abc, cal, sql, bllb, blli, bllk; // , f2, f3, f4,
    double *dxi1, *dyi1, *dzi1;
    double *dxk1, *dyk1, *dzk1;
    double *xi, *yi, *zi, *xi1, *yi1, *zi1;
    double *xk1, *yk1, *zk1;
    double ret, imt, re1, im1, rett = 1, imtt = 0;
    int nst, ns2, nstt = 0, nttt = 0; //, it = 0;
    //    static int display = 0;

/*     Writhing number calculation for linear chain. */
/*     Cartesian coordinates of chain segments are x(i),y(i),z(i). */
/*     DX, DY, and DZ are arrays of bond vectors, ie DX(i) = X(i+1)-X(i). */
    i1 = *jr1 - lend - 2;
    i2 = *jr1 - lend;
    for (i = lend; i < i1; ++i)
      {
	i_1 = i - 1;
	dxi1 = dx + i_1; dyi1 = dy + i_1; dzi1 = dz + i_1;
	xi = x + i; yi = y + i; zi = z + i;
	xi1 = x + i_1; yi1 = y + i_1; zi1 = z + i_1;

	k_1 = i;
	dxk1 = dx + k_1; dyk1 = dy + k_1; dzk1 = dz + k_1;
        xk1 = x + k_1; yk1 = y + k_1; zk1 = z + k_1;

	rx = *xk1 - *xi;   ry = *yk1 - *yi;   rz = *zk1 - *zi;
	 zn1 = sqrt(rx * rx + ry * ry + rz * rz);
        rx = *xi1 - *xk1;    ry = *yi1 - *yk1;    rz = *zi1 - *zk1;
	 zn2 = sqrt(rx * rx + ry * ry + rz * rz);


	for (k = i + 1; k < i2; ++k)
	  {
	    cal = *dxk1 * *dxi1 + *dyk1 * *dyi1 + *dzk1 * *dzi1;
	    sql = 1. - cal * cal;

	    zn4 = zn2; // computed before
	    zn3 = zn1; // same
	    // rx should be defined
	    bllk = rx * *dxk1 + ry * *dyk1 + rz * *dzk1;
	    blli = rx * *dxi1 + ry * *dyi1 + rz * *dzi1;
	    bllb = rx * (*dyk1 * *dzi1 - *dyi1 * *dzk1)
	         + ry * (*dzk1 * *dxi1 - *dzi1 * *dxk1)
	         + rz * (*dxk1 * *dyi1 - *dxi1 * *dyk1);

	    dxk1++; dyk1++; dzk1++;
	    xk1++; yk1++; zk1++;

	    rx = *xk1 - *xi;    ry = *yk1 - *yi;    rz = *zk1 - *zi;
	    zn1 = sqrt(rx * rx + ry * ry + rz * rz);

	    rx = *xi1 - *xk1;   ry = *yi1 - *yk1;   rz = *zi1 - *zk1;
	    zn2 = sqrt(rx * rx + ry * ry + rz * rz);

	    if (sql <= .001)  continue;
	    sql = 1.0/sql;

	    ak = (bllk - blli * cal) * sql;
	    ai = (bllk * cal - blli) * sql;
	    ab = bllb * sql;
	    abc = (ab * ab) * cal;

	    ret = -((1.0 - ak) * (1.0 - ai) + abc);
	    imt = -(ab * zn1);

	    re1 = -((ak - 1.0) * ai + abc);
	    im1 = ab * zn2;
	    bllb = ret * re1 - imt * im1;
	    imt = ret * im1 + imt * re1;
	    ret = bllb;

	    re1 = -(ak * (ai - 1.0) + abc);
	    im1 = ab * zn3;
	    bllb = ret * re1 - imt * im1;
	    imt = ret * im1 + imt * re1;
	    ret = bllb;

	    re1 = -(ak * ai + abc);
	    im1 = -ab * zn4;

	    bllb = ret * re1 - imt * im1;
	    imt = ret * im1 + imt * re1;
	    ret = bllb;
	    nst = (imt < 0)? ((ret < 0)? 2 : 3) : ((ret < 0)? 1 : 0);

	    bllb = ret * rett - imt * imtt;
	    blli = ret * imtt + imt * rett;

	    f1 = 1.0/sqrt(bllb * bllb + blli * blli);
	    bllb =  f1 * bllb;
	    blli =  f1 * blli;



	    ns2 = (blli < 0)? ((bllb < 0)? 2 : 3) : ((bllb < 0)? 1 : 0);
# ifdef OLD
	    //	    if (it)
	    // {
		if ((nst < 2) && ((ns2 - nstt) < 0))   nttt++;
		else if ((nst >= 2) && ((ns2 - nstt) > 0))
	      {
		nttt--;
		if (display < 10)
		  win_printf("ntt-- i %d k %d \nret %g imt %g nst %d\n"
			     "re1 %g im1 %g ns1 %d\n"
			     "re2 %g im2 %g ns2 %d\n",i,k,ret,imt,nst,
			     rett,imtt,nstt,bllb,blli,ns2);
		display++;
	      }
# endif
	    if (bllb < 0)
	      {
		if (imtt >= 0 && blli < 0) nttt++;
		else if (imtt <= 0 && blli > 0) nttt--;
	      }
	    rett = bllb; imtt = blli;

		//  }
		//else it++;
	    nstt = ns2;
	}
    }
    *beta = atan2(imtt,rett)/(2*M_PI);
    *beta += nttt;
    return 0;
} /* wrn_ */
int wrn_vcOK(int *jr1, double *beta, double *x, double *y, double *z, double *dx, double *dy, double *dz)
{
    /* Initialized data */
    int lend = 1;
    /* System generated locals */
    int i1, i2;
    /* Local variables */
    register int i, k, i_1, k_1;
    double f1, ab, ai, ak, rx, ry, rz, zn1, zn2, zn3, zn4, abc, cal, sql, www, bllb, blli, bllk; // , f2, f3, f4,
    double *dxi1, *dyi1, *dzi1;
    double *dxk1, *dyk1, *dzk1;
    double *xi, *yi, *zi, *xi1, *yi1, *zi1;
    double *xk, *yk, *zk, *xk1, *yk1, *zk1;
    double ret, imt, re1, im1, re2, im2, rett = 1, imtt = 0;
    int nst, ns2, nstt = 0, nttt = 0;
    //    static int display = 0;

/*     Writhing number calculation for linear chain. */
/*     Cartesian coordinates of chain segments are x(i),y(i),z(i). */
/*     DX, DY, and DZ are arrays of bond vectors, ie DX(i) = X(i+1)-X(i). */
    www = 0;
    i1 = *jr1 - lend - 2;

    for (i = lend; i < i1; ++i)
      {
	i2 = *jr1 - lend;
	i_1 = i - 1;
	dxi1 = dx + i_1; dyi1 = dy + i_1; dzi1 = dz + i_1;
	xi = x + i; yi = y + i; zi = z + i;
	xi1 = x + i_1; yi1 = y + i_1; zi1 = z + i_1;
	for (k = i + 1; k < i2; ++k)
	  {
	    k_1 = k - 1;
	    dxk1 = dx + k_1; dyk1 = dy + k_1; dzk1 = dz + k_1;
	    xk = x + k; yk = y + k; zk = z + k;
	    xk1 = x + k_1; yk1 = y + k_1; zk1 = z + k_1;
	    cal = *dxk1 * *dxi1 + *dyk1 * *dyi1 + *dzk1 * *dzi1;
	    sql = 1. - cal * cal;
	    if (sql <= .001)  continue;
	    sql = 1.0/sql;
	    rx = *xi1 - *xk1;    ry = *yi1 - *yk1;    rz = *zi1 - *zk1;
	    zn4 = sqrt(rx * rx + ry * ry + rz * rz);
	    bllk = rx * *dxk1 + ry * *dyk1 + rz * *dzk1;
	    blli = rx * *dxi1 + ry * *dyi1 + rz * *dzi1;
	    bllb = rx * (*dyk1 * *dzi1 - *dyi1 * *dzk1)
	         + ry * (*dzk1 * *dxi1 - *dzi1 * *dxk1)
	         + rz * (*dxk1 * *dyi1 - *dxi1 * *dyk1);

	    rx = *xk - *xi;    ry = *yk - *yi;    rz = *zk - *zi;
	    zn1 = sqrt(rx * rx + ry * ry + rz * rz);
	    rx = *xk - *xi1;   ry = *yk - *yi1;   rz = *zk - *zi1;
	    zn2 = sqrt(rx * rx + ry * ry + rz * rz);
	    rx = *xk1 - *xi;   ry = *yk1 - *yi;   rz = *zk1 - *zi;
	    zn3 = sqrt(rx * rx + ry * ry + rz * rz);

	    ak = (bllk - blli * cal) * sql;
	    ai = (bllk * cal - blli) * sql;
	    ab = bllb * sql;
	    abc = (ab * ab) * cal;


	    ret = -((1.0 - ak) * (1.0 - ai) + abc);
	    imt = -(ab * zn1);
	    //nst = (imt < 0)? ((ret < 0)? 2 : 3) : ((ret < 0)? 1 : 0);

	    re1 = -((ak - 1.0) * ai + abc);
	    im1 = ab * zn2;
	    //ns1 = (im1 < 0)? ((re1 < 0)? 2 : 3) : ((re1 < 0)? 1 : 0);
	    re2 = ret * re1 - imt * im1;
	    im2 = ret * im1 + imt * re1;
	    //ns2 = (im2 < 0)? ((re2 < 0)? 2 : 3) : ((re2 < 0)? 1 : 0);
	    ret = re2; imt = im2; nst = ns2;



	    re1 = -(ak * (ai - 1.0) + abc);
	    im1 = ab * zn3;
	    //ns1 = (im1 < 0)? ((re1 < 0)? 2 : 3) : ((re1 < 0)? 1 : 0);
	    re2 = ret * re1 - imt * im1;
	    im2 = ret * im1 + imt * re1;
	    //ns2 = (im2 < 0)? ((re2 < 0)? 2 : 3) : ((re2 < 0)? 1 : 0);
	    ret = re2; imt = im2; nst = ns2;


	    re1 = -(ak * ai + abc);
	    im1 = -ab * zn4;
	    //ns1 = (im1 < 0)? ((re1 < 0)? 2 : 3) : ((re1 < 0)? 1 : 0);
	    re2 = ret * re1 - imt * im1;
	    im2 = ret * im1 + imt * re1;
	    ns2 = (im2 < 0)? ((re2 < 0)? 2 : 3) : ((re2 < 0)? 1 : 0);
	    ret = re2; imt = im2; nst = ns2;
	    //display_title_message("%d pts display %d",*jr1,display++);

	    re2 = ret * rett - imt * imtt;
	    im2 = ret * imtt + imt * rett;

	    f1 = 1.0/sqrt(re2 * re2 + im2 * im2);
	    re2 =  f1 * re2;
	    im2 =  f1 * im2;

	    ns2 = (im2 < 0)? ((re2 < 0)? 2 : 3) : ((re2 < 0)? 1 : 0);
# ifdef PB
# endif
	    if ((nst < 2) && ((ns2 - nstt) < 0))   nttt++;
	    else if ((nst >= 2) && ((ns2 - nstt) > 0))    nttt--;
	    rett = re2; imtt = im2;
	    nstt = ns2;



	}
    }
    *beta = www / (2*M_PI);// 6.2831852f;
    *beta = atan2(imtt,rett)/(2*M_PI);
    *beta += nttt;

    return 0;
} /* wrn_ */

int wrn_vc1(int *jr1, double *beta, double *x, double *y, double *z, double *dx, double *dy, double *dz)
{
    /* Initialized data */
    static int lend = 1;
    /* System generated locals */
    int i1, i2;
    //static int display = 0;
    /* Local variables */
    register int i, k, i_1, k_1;
    static double  f2, ab, ai, ak, rx, ry, rz, zn1, zn2, zn3,
	    zn4, abc, cal, abq, sql, www, bllb, blli, bllk;
    double *dxi1, *dyi1, *dzi1;
    double *dxk1, *dyk1, *dzk1;
    double *xi, *yi, *zi, *xi1, *yi1, *zi1;
    double *xk, *yk, *zk, *xk1, *yk1, *zk1;
    double ret, imt, re1, im1, re2, im2, rett = 1, imtt = 0;
    int nst, ns1, ns2, ntt = 0, nstt = 0, nttt = 0;

/*     Writhing number calculation for linear chain. */
/*     Cartesian coordinates of chain segments are x(i),y(i),z(i). */
/*     DX, DY, and DZ are arrays of bond vectors, ie DX(i) = X(i+1)-X(i). */
    www = 0;
    i1 = *jr1 - lend - 2;

    for (i = lend; i < i1; ++i)
      {
	i2 = *jr1 - lend;
	i_1 = i - 1;
	dxi1 = dx + i_1; dyi1 = dy + i_1; dzi1 = dz + i_1;
	xi = x + i; yi = y + i; zi = z + i;
	xi1 = x + i_1; yi1 = y + i_1; zi1 = z + i_1;
	for (k = i + 1; k < i2; ++k)
	  {
	    k_1 = k - 1;
	    dxk1 = dx + k_1; dyk1 = dy + k_1; dzk1 = dz + k_1;
	    xk = x + k; yk = y + k; zk = z + k;
	    xk1 = x + k_1; yk1 = y + k_1; zk1 = z + k_1;
	    cal = *dxk1 * *dxi1 + *dyk1 * *dyi1 + *dzk1 * *dzi1;
	    sql = 1. - cal * cal;
	    if (sql <= .001)  continue;
	    sql = 1.0/sql;
	    rx = *xi1 - *xk1;
	    ry = *yi1 - *yk1;
	    rz = *zi1 - *zk1;
	    zn4 = sqrt(rx * rx + ry * ry + rz * rz);
	    bllk = rx * *dxk1 + ry * *dyk1 + rz * *dzk1;
	    blli = rx * *dxi1 + ry * *dyi1 + rz * *dzi1;
	    bllb = rx * (*dyk1 * *dzi1 - *dyi1 * *dzk1)
	         + ry * (*dzk1 * *dxi1 - *dzi1 * *dxk1)
	         + rz * (*dxk1 * *dyi1 - *dxi1 * *dyk1);

	    rx = *xk - *xi;
	    ry = *yk - *yi;
	    rz = *zk - *zi;
	    zn1 = sqrt(rx * rx + ry * ry + rz * rz);
	    rx = *xk - *xi1;
	    ry = *yk - *yi1;
	    rz = *zk - *zi1;
	    zn2 = sqrt(rx * rx + ry * ry + rz * rz);
	    rx = *xk1 - *xi;
	    ry = *yk1 - *yi;
	    rz = *zk1 - *zi;
	    zn3 = sqrt(rx * rx + ry * ry + rz * rz);

	    ak = (bllk - blli * cal) * sql;
	    ai = (bllk * cal - blli) * sql;
	    ab = bllb * sql;
	    abq = ab * ab;
	    abc = abq * cal;


	    /*
	    f1 = atan2((ab * zn1),((1.0 - ak) * (1.0 - ai) + abc));
	    f2 = atan2((ab * zn2),((1.0 - ak) * (-ai) + abc));
	    f3 = atan2((ab * zn3),(-ak * (1.0 - ai) + abc));
	    f4 = atan2((ab * zn4),(-ak * (-ai) + abc));
	    www = www + f1 - f2 - f3 + f4;
	    */

	    ntt = 0;
	    ret = -((1.0 - ak) * (1.0 - ai) + abc);
	    imt = -(ab * zn1);
	    //f1 = atan2(imt,ret);
	    nst = (imt < 0)? ((ret < 0)? 2 : 3) : ((ret < 0)? 1 : 0);

	    re1 = -((ak - 1.0) * ai + abc);
	    im1 = ab * zn2;
	    //f2 = atan2(im1,re1);
	    ns1 = (im1 < 0)? ((re1 < 0)? 2 : 3) : ((re1 < 0)? 1 : 0);
	    re2 = ret * re1 - imt * im1;
	    im2 = ret * im1 + imt * re1;
	    ns2 = (im2 < 0)? ((re2 < 0)? 2 : 3) : ((re2 < 0)? 1 : 0);
	    /*
	    if ((ns1 < 2) && ((ns2 - nst) < 0))
	      {
		ntt++;
		if (display < 10)
		  win_printf("ntt++ i %d k %d \nret %g imt %g nst %d\n"
			     "re1 %g im1 %g ns1 %d\n"
			     "re2 %g im2 %g ns2 %d\n",i,k,ret,imt,nst,
			     re1,im1,ns1, re2,im2,ns2);
		display++;
	      }
	    else if ((ns1 >= 2) && ((ns2 - nst) > 0))
	      {
		ntt--;
		if (display < 10)
		  win_printf("ntt-- i %d k %d \nret %g imt %g nst %d\n"
			     "re1 %g im1 %g ns1 %d\n"
			     "re2 %g im2 %g ns2 %d\n",i,k,ret,imt,nst,
			     re1,im1,ns1, re2,im2,ns2);
		display++;
	      }
	    */
	    ret = re2; imt = im2; nst = ns2;
	    /*
	    f1 = atan2(-mt,ret);
	    f2 = 0;
	    if (ntt != 0)      f1 += ntt*2*M_PI;
	    */
	    re1 = -(ak * (ai - 1.0) + abc);
	    im1 = ab * zn3;
	    //f3 = atan2(im1,re1);
	    ns1 = (im1 < 0)? ((re1 < 0)? 2 : 3) : ((re1 < 0)? 1 : 0);
	    re2 = ret * re1 - imt * im1;
	    im2 = ret * im1 + imt * re1;
	    ns2 = (im2 < 0)? ((re2 < 0)? 2 : 3) : ((re2 < 0)? 1 : 0);
	    if ((ns1 < 2) && ((ns2 - nst) < 0))   ntt++;
	    else if ((ns1 >= 2) && ((ns2 - nst) > 0))   ntt--;
	    ret = re2; imt = im2; nst = ns2;

	    //f1 = atan2(imt,ret);
	    //f3 = 0;

	    //if (ntt != 0)      f1 += ntt*2*M_PI;

	    re1 = -(ak * ai + abc);
	    im1 = -ab * zn4;
	    //f4 = atan2(im1,re1);
	    ns1 = (im1 < 0)? ((re1 < 0)? 2 : 3) : ((re1 < 0)? 1 : 0);
	    re2 = ret * re1 - imt * im1;
	    im2 = ret * im1 + imt * re1;
	    ns2 = (im2 < 0)? ((re2 < 0)? 2 : 3) : ((re2 < 0)? 1 : 0);
	    if ((ns1 < 2) && ((ns2 - nst) < 0))   ntt++;
	    else if ((ns1 >= 2) && ((ns2 - nst) > 0))   ntt--;
	    ret = re2; imt = im2; nst = ns2;
	    /*
		if (display < 10)
		  win_printf(" i %d k %d \nf1 %g f2 %g\n f3 %g f4 %g\n"
			     "tot %g fin %g",
			     i,k,f1/M_PI,f2/M_PI,f3/M_PI,f4/M_PI,(f1+f2+f3+f4),atan2(imt,ret));
		display++;

	    www = www + f1 + f2 + f3 + f4;
	    */

	    re2 = ret * rett - imt * imtt;
	    im2 = ret * imtt + imt * rett;
	    f2 = re2 * re2 + im2 * im2;
	    f2 = 1.0/sqrt(f2);
	    re2 *= f2;
	    im2 *= f2;
	    ns2 = (im2 < 0)? ((re2 < 0)? 2 : 3) : ((re2 < 0)? 1 : 0);
	    if ((nst < 2) && ((ns2 - nstt) < 0))   nttt++;
	    else if ((nst >= 2) && ((ns2 - nstt) > 0))   nttt--;
	    rett = re2; imtt = im2; nstt = ns2;


	    //www += atan2(imt,ret);
	    /*
	    f4 = atan2(imt,ret);
	    www += f4;
	    if (ntt != 0)
	      www += ntt*2*M_PI;

	    if (display < 20)
	      {
		f1 = atan2(imtt,rett)/(2*M_PI);
		win_printf("beta %g, imtt %g rett %g nttt %d + %g",
			  www / (2*M_PI) ,rett,imtt,nttt,f1);
		display++;
	      }
	    */
	}
    }
    *beta = www / (2*M_PI);// 6.2831852f;
    *beta = atan2(imtt,rett)/(2*M_PI);
    *beta += nttt;
    /*
    if (display == 0)
      {
	f1 = atan2(imtt,rett)/(2*M_PI);
	win_printf("beta %g, imtt %g rett %g nttt %d + %g",
		   *beta,rett,imtt,nttt,f1);
	display++;
      }
    */
    return 0;
} /* wrn_ */
int wrn_vc2(int *jr1, double *beta, double *x, double *y, double *z, double *dx, double *dy, double *dz)
{
    /* Initialized data */
    static int lend = 1;
    /* System generated locals */
    int i1, i2;
    /* Local variables */
    register int i, k, i_1, k_1;
    static double f1, f2, f3, f4, ab, ai, ak, rx, ry, rz, zn1, zn2, zn3,
	    zn4, abc, cal, abq, sql, www, bllb, blli, bllk;

/*     Writhing number calculation for linear chain. */
/*     Cartesian coordinates of chain segments are x(i),y(i),z(i). */
/*     DX, DY, and DZ are arrays of bond vectors, ie DX(i) = X(i+1)-X(i). */
    www = 0;
    i1 = *jr1 - lend - 2;
    for (i = lend; i < i1; ++i)
      {
	i2 = *jr1 - lend;
	i_1 = i-1;
	for (k = i + 1; k < i2; ++k)
	  {
	    k_1 = k - 1;
	    cal = dx[k_1] * dx[i_1] + dy[k_1] * dy[i_1] + dz[k_1] * dz[i_1];
	    sql = 1. - cal * cal;
	    if (sql <= .001)  continue;
	    rx = x[i_1] - x[k_1];    ry = y[i_1] - y[k_1];    rz = z[i_1] - z[k_1];
	    zn4 = sqrt(rx * rx + ry * ry + rz * rz);
	    bllb = rx * (dy[k_1] * dz[i_1] - dy[i_1] * dz[k_1])
	         + rx * (dz[k_1] * dx[i_1] - dz[i_1] * dx[k_1])
	         + rx * (dx[k_1] * dy[i_1] - dx[i_1] * dy[k_1]);

	    bllk = rx * dx[k_1] + ry * dy[k_1] + rz * dz[k_1];
	    blli = rx * dx[i_1] + ry * dy[i_1] + rz * dz[i_1];

	    rx = x[k] - x[i];    ry = y[k] - y[i];    rz = z[k] - z[i];
	    zn1 = sqrt(rx * rx + ry * ry + rz * rz);

	    rx = x[k] - x[i_1];  ry = y[k] - y[i_1];  rz = z[k] - z[i_1];
	    zn2 = sqrt(rx * rx + ry * ry + rz * rz);

	    rx = x[k_1] - x[i];  ry = y[k_1] - y[i];  rz = z[k_1] - z[i];
	    zn3 = sqrt(rx * rx + ry * ry + rz * rz);

	    ak = (bllk - blli * cal) / sql;
	    ai = (bllk * cal - blli) / sql;
	    ab = bllb / sql;
	    abq = ab * ab;
	    abc = abq * cal;
	    f1 = -atan(((1.0 - ak) * (1.0 - ai) + abc) / (ab * zn1));
	    f2 = -atan(((1.0 - ak) * (-ai) + abc) / (ab * zn2));
	    f3 = -atan((-ak * (1.0 - ai) + abc) / (ab * zn3));
	    f4 = -atan((-ak * (-ai) + abc) / (ab * zn4));
	    www = www + f1 - f2 - f3 + f4;
	}
    }
    *beta = www / (2*M_PI);// 6.2831852f;
    return 0;
} /* wrn_ */

int wrn_float(int *jr1, double *beta, float *x, float *y, float *z, float *dx, float *dy, float *dz)
{
    /* Initialized data */
    static int lend = 1;
    /* System generated locals */
    int i1, i2;
    /* Local variables */
    static int i, k;
    static float f1, f2, f3, f4, ab, ai, ak, rx, ry, rz, zn1, zn2, zn3,
	    zn4, abc, cal, abq, sql, www, bllb, blli, bllk;

/*     Writhing number calculation for linear chain. */
/*     Cartesian coordinates of chain segments are x(i),y(i),z(i). */
/*     DX, DY, and DZ are arrays of bond vectors, ie DX(i) = X(i+1)-X(i). */
    www = 0;
    i1 = *jr1 - lend - 2;
    for (i = lend; i < i1; ++i)
      {
	i2 = *jr1 - lend;
	for (k = i + 1; k < i2; ++k)
	  {
	    bllb = (x[i - 1] - x[k - 1]) * (dy[k - 1] * dz[i - 1] - dy[i - 1] * dz[k - 1])
	         + (y[i - 1] - y[k - 1]) * (dz[k - 1] * dx[i - 1] - dz[i - 1] * dx[k - 1])
	         + (z[i - 1] - z[k - 1]) * (dx[k - 1] * dy[i - 1] - dx[i - 1] * dy[k - 1]);
	    cal = dx[k - 1] * dx[i - 1] + dy[k - 1] * dy[i - 1] + dz[k - 1] * dz[i - 1];
	    sql = 1. - cal * cal;
	    if (sql <= .001)  continue;
	    rx = x[k] - x[i];
	    ry = y[k] - y[i];
	    rz = z[k] - z[i];
	    zn1 = sqrt(rx * rx + ry * ry + rz * rz);
	    rx = x[k] - x[i - 1];
	    ry = y[k] - y[i - 1];
	    rz = z[k] - z[i - 1];
	    zn2 = sqrt(rx * rx + ry * ry + rz * rz);
	    rx = x[k - 1] - x[i];
	    ry = y[k - 1] - y[i];
	    rz = z[k - 1] - z[i];
	    zn3 = sqrt(rx * rx + ry * ry + rz * rz);
	    rx = x[k - 1] - x[i - 1];
	    ry = y[k - 1] - y[i - 1];
	    rz = z[k - 1] - z[i - 1];
	    zn4 = sqrt(rx * rx + ry * ry + rz * rz);
	    bllk = (x[i - 1] - x[k - 1]) * dx[k - 1]
	         + (y[i - 1] - y[k - 1]) * dy[k - 1]
	         + (z[i - 1] - z[k - 1]) * dz[k - 1];
	    blli = (x[i - 1] - x[k - 1]) * dx[i - 1]
	         + (y[i - 1] - y[k - 1]) * dy[i - 1]
	         + (z[i - 1] - z[k - 1]) * dz[i - 1];
	    ak = (bllk - blli * cal) / sql;
	    ai = (bllk * cal - blli) / sql;
	    ab = bllb / sql;
	    abq = ab * ab;
	    abc = abq * cal;
	    f1 = -atan(((1.0 - ak) * (1.0 - ai) + abc) / (ab * zn1));
	    f2 = -atan(((1.0 - ak) * (-ai) + abc) / (ab * zn2));
	    f3 = -atan((-ak * (1.0 - ai) + abc) / (ab * zn3));
	    f4 = -atan((-ak * (-ai) + abc) / (ab * zn4));
	    www = www + f1 - f2 - f3 + f4;
	}
    }
    *beta = www / (2*M_PI);// 6.2831852f;
    return 0;
} /* wrn_ */
int wrn_f(int *jr1, double *beta, fixed *x, fixed *y, fixed *z, fixed *dx, fixed *dy, fixed *dz)
{
    /* Initialized data */
    static int lend = 1;
    /* System generated locals */
    int i1, i2;
    /* Local variables */
    register int i, k;
    fixed f1, f2, f3, f4, ab, ai, ak, rx, ry, rz, zn1, zn2, zn3,
	    zn4, abc, cal, abq, sql, www, bllb, blli, bllk, fix1, fix2pi;
    fixed *xi_1, *xk_1, *dxk_1, *dxi_1, *xi, *xk;
    fixed *yi_1, *yk_1, *dyk_1, *dyi_1, *yi, *yk;
    fixed *zi_1, *zk_1, *dzk_1, *dzi_1, *zi, *zk;

/*     Writhing number calculation for linear chain. */
/*     Cartesian coordinates of chain segments are x(i),y(i),z(i). */
/*     DX, DY, and DZ are arrays of bond vectors, ie DX(i) = X(i+1)-X(i). */
    www = ftofix(0.0);
    fix1 = ftofix(1.0);
    fix2pi = ftofix(2*M_PI);
    i1 = *jr1 - lend - 2;
    for (i = lend; i < i1; ++i)
      {
	i2 = *jr1 - lend;
	xi_1 = x + i - 1; yi_1 = y + i - 1; zi_1 = z + i - 1;
	xi = x + i; yi = y + i; zi = z + i;
	dxi_1 = dx + i - 1; dyi_1 = dy + i - 1; dzi_1 = dz + i - 1;
	for (k = i + 1; k < i2; ++k)
	  {
	    xk_1 = x + k - 1; yk_1 = y + k - 1; zk_1 = z + k - 1;
	    xk = x + k; yk = y + k; zk = z + k;
	    dxk_1 = dx + k - 1; dyk_1 = dy + k - 1; dzk_1 = dz + k - 1;
	    bllb = fixmul((*xi_1 - *xk_1),(fixmul(*dyk_1,*dzi_1) - fixmul(*dyi_1,*dzk_1)))
	         + fixmul((*yi_1 - *yk_1),(fixmul(*dzk_1,*dxi_1) - fixmul(*dzi_1,*dxk_1)))
	         + fixmul((*zi_1 - *zk_1),(fixmul(*dxk_1,*dyi_1) - fixmul(*dxi_1,*dyk_1)));
	    cal = fixmul(*dxk_1,*dxi_1) + fixmul(*dyk_1,*dyi_1) + fixmul(*dzk_1,*dzi_1);
	    sql = fix1 - fixmul(cal,cal);
	    if (fixtof(sql) <= .001)  continue;
	    rx = *xk - *xi;
	    ry = *yk - *yi;
	    rz = *zk - *zi;
	    zn1 = fixsqrt(fixmul(rx,rx) + fixmul(ry,ry) + fixmul(rz,rz));
	    rx = *xk - *xi_1;
	    ry = *yk - *yi_1;
	    rz = *zk - *zi_1;
	    zn2 = fixsqrt(fixmul(rx,rx) + fixmul(ry,ry) + fixmul(rz,rz));
	    rx = *xk_1 - *xi;
	    ry = *yk_1 - *yi;
	    rz = *zk_1 - *zi;
	    zn3 = fixsqrt(fixmul(rx,rx) + fixmul(ry,ry) + fixmul(rz,rz));
	    rx = *xk_1 - *xi_1;
	    ry = *yk_1 - *yi_1;
	    rz = *zk_1 - *zi_1;
	    zn4 = fixsqrt(fixmul(rx,rx) + fixmul(ry,ry) + fixmul(rz,rz));
	    bllk = fixmul((*xi_1 - *xk_1),*dxk_1)
	         + fixmul((*yi_1 - *yk_1),*dyk_1)
	         + fixmul((*zi_1 - *zk_1),*dzk_1);
	    blli = fixmul((*xi_1 - *xk_1),*dxi_1)
	         + fixmul((*yi_1 - *yk_1),*dyi_1)
	         + fixmul((*zi_1 - *zk_1),*dzi_1);
	    ak = fixdiv((bllk - fixmul(blli,cal)),sql);
	    ai = fixdiv((fixmul(bllk,cal) - blli),sql);
	    ab = fixdiv(bllb,sql);
	    abq = fixmul(ab,ab);
	    abc = fixmul(abq,cal);
	    f1 = -fixatan2((fixmul((fix1 - ak),(fix1 - ai)) + abc), fixmul(ab,zn1));
	    f2 = -fixatan2((fixmul((fix1 - ak),(-ai)) + abc), fixmul(ab,zn2));
	    f3 = -fixatan2((fixmul(-ak, (fix1 - ai)) + abc), fixmul(ab,zn3));
	    f4 = -fixatan2((fixmul(ak, ai) + abc), fixmul(ab,zn4));
	    www = www + f1 - f2 - f3 + f4;
	}
    }
    *beta = fixtof(fdiv(www,fix2pi));// 6.2831852f;
    return 0;
} /* wrn_f */

double do_compute_wirthe_gilles(p_c *pc)
{
  double beta, *x, *y, *z, *dx, *dy, *dz;
  register int i;
  int np, nf = 100;
  LARGE_INTEGER ti0, ti1, freq;
  long long dtm, dtm0 = 0, dwait;
  double dti0,  dfreq, tmp;

  np = pc->np;
  x = (double*)calloc(np,sizeof(double));
  y = (double*)calloc(np,sizeof(double));
  z = (double*)calloc(np,sizeof(double));
  dx = (double*)calloc(np,sizeof(double));
  dy = (double*)calloc(np,sizeof(double));
  dz = (double*)calloc(np,sizeof(double));

  if(x == NULL || y == NULL || z == NULL || dx == NULL || dy == NULL || dz == NULL)
    return 0;
  for(i = 0; i < np; i++)
    {
      x[i] = pc->x[i];	y[i] = pc->y[i]; z[i] = pc->z[i];
    }
  for(i = 0; i < np-1; i++)
    {
      dx[i] = x[i+1] - x[i];
      dy[i] = y[i+1] - y[i];
      dz[i] = z[i+1] - z[i];
    }

  if (plasmid == 1)
    {
      for(i = 1; i < np-1; i++)
	{
	  dx[i] = x[i+1] - x[i-1];
	  dy[i] = y[i+1] - y[i-1];
	  dz[i] = z[i+1] - z[i-1];
	  tmp = sqrt(dx[i]*dx[i]+dy[i]*dy[i]+dz[i]*dz[i]);
	  tmp = (tmp != 0) ? 1.0/tmp : tmp;
	  dx[i] *= tmp;
	  dy[i] *= tmp;
	  dz[i] *= tmp;
	}
      i = 0;
      dx[i] = x[i+1] - x[np-1];
      dy[i] = y[i+1] - y[np-1];
      dz[i] = z[i+1] - z[np-1];
      tmp = sqrt(dx[i]*dx[i]+dy[i]*dy[i]+dz[i]*dz[i]);
      tmp = (tmp != 0) ? 1.0/tmp : tmp;
      dx[i] *= tmp;
      dy[i] *= tmp;
      dz[i] *= tmp;

      i = np - 1;
      dx[i] = x[0] - x[i-1];
      dy[i] = y[0] - y[i-1];
      dz[i] = z[0] - z[i-1];
      tmp = sqrt(dx[i]*dx[i]+dy[i]*dy[i]+dz[i]*dz[i]);
      tmp = (tmp != 0) ? 1.0/tmp : tmp;
      dx[i] *= tmp;
      dy[i] *= tmp;
      dz[i] *= tmp;
    }

  QueryPerformanceFrequency(&freq);
  dwait = freq.QuadPart;

      wrn_gr(&np, &beta, x, y, z, dx, dy, dz);
  for(i = 0, freq.QuadPart = 0; i < nf && freq.QuadPart < dwait/2; i++)
    {
      QueryPerformanceCounter(&ti0);
      wrn_(&np, &beta, x, y, z, dx, dy, dz);
      QueryPerformanceCounter(&ti1);
      dtm = ti1.QuadPart - ti0.QuadPart;
      if (i == 0) freq.QuadPart = dtm0 = dtm;
      else freq.QuadPart += dtm;
      dtm0 = (dtm < dtm0) ? dtm : dtm0;
    }

  if (i)  dti0 =(double)(freq.QuadPart)/i;
  else dti0 =(double)(freq.QuadPart);
  dti0 *= 1000000;
  QueryPerformanceFrequency(&freq);
  dfreq = (double)(freq.QuadPart);
  dtm0 *= 1000000;
  win_printf("wirthe computing time = %g \\mu s\nmin %g \\mu s w = %g", (dfreq != 0) ?
	      dti0/dfreq : dti0, (dfreq != 0)? (double)dtm0/dfreq : (double)dtm0, beta);



  for(i = 0, nf = 100, freq.QuadPart = 0; i < nf && freq.QuadPart < dwait/2; i++)
    {
      QueryPerformanceCounter(&ti0);
      wrn_vc(&np, &beta, x, y, z, dx, dy, dz);
      QueryPerformanceCounter(&ti1);
      dtm = ti1.QuadPart - ti0.QuadPart;
      if (i == 0) freq.QuadPart = dtm0 = dtm;
      else freq.QuadPart += dtm;
      dtm0 = (dtm < dtm0) ? dtm : dtm0;
    }

  if (i)  dti0 =(double)(freq.QuadPart)/i;
  else dti0 =(double)(freq.QuadPart);
  dti0 *= 1000000;
  QueryPerformanceFrequency(&freq);
  dfreq = (double)(freq.QuadPart);
  dtm0 *= 1000000;
  win_printf("wirthe over %d points vc computing time = %g \\mu s (mean on %d\n"
	     "min %g \\mu s w = %g",np, (dfreq != 0) ? dti0/dfreq : dti0, i,
	     (dfreq != 0)? (double)dtm0/dfreq : (double)dtm0, beta);

  wrn_gauss_gr(&np, &beta, x, y, z, dx, dy, dz);

  for(i = 0, nf = 100, freq.QuadPart = 0; i < nf && freq.QuadPart < dwait/2; i++)
    {
      QueryPerformanceCounter(&ti0);
      wrn_gauss(&np, &beta, x, y, z, dx, dy, dz);
      QueryPerformanceCounter(&ti1);
      dtm = ti1.QuadPart - ti0.QuadPart;
      if (i == 0) freq.QuadPart = dtm0 = dtm;
      else freq.QuadPart += dtm;
      dtm0 = (dtm < dtm0) ? dtm : dtm0;
    }

  if (i) dti0 =(double)(freq.QuadPart)/i;
  else dti0 =(double)(freq.QuadPart);
  dti0 *= 1000000;
  QueryPerformanceFrequency(&freq);
  dfreq = (double)(freq.QuadPart);
  dtm0 *= 1000000;
  win_printf("wirthe over %d points gauss computing time = %g \\mu s (mean on %d\n"
	     "min %g \\mu s w = %g",np, (dfreq != 0) ? dti0/dfreq : dti0, i,
	     (dfreq != 0)? (double)dtm0/dfreq : (double)dtm0, beta);

# ifdef PB

  np = np/2 + 1;
  /*
  pc->xn[np-1] =   pc->x[np-1];
  pc->yn[np-1] =   pc->y[np-1];
  pc->zn[np-1] =   pc->z[np-1];
  */
  for(i = 0; i < np; i++)
    {
      x[i] = pc->x[2*i];
      y[i] = pc->y[2*i];
      z[i] = pc->z[2*i];
    }
  for(i = 0; i < np-1; i++)
    {
      dx[i] = x[i+1] - x[i];
      dy[i] = y[i+1] - y[i];
      dz[i] = z[i+1] - z[i];
      tmp = sqrt(dx[i]*dx[i]+dy[i]*dy[i]+dz[i]*dz[i]);
      tmp = (tmp != 0) ? 1.0/tmp : tmp;
      dx[i] *= tmp;
      dy[i] *= tmp;
      dz[i] *= tmp;
    }
  for(i = 1; i < np; i++)
    {
      x[i] = x[i-1] + dx[i-1];
      y[i] = y[i-1] + dy[i-1];
      z[i] = z[i-1] + dz[i-1];
      pc->xn[2*i] = pc->x[2*i];
      pc->yn[2*i] = pc->y[2*i];
      pc->zn[2*i] = pc->z[2*i];
      pc->xn[2*i-1] = (pc->xn[2*i-2] + pc->xn[2*i])/2;
      pc->yn[2*i-1] = (pc->yn[2*i-2] + pc->yn[2*i])/2;
      pc->zn[2*i-1] = (pc->zn[2*i-2] + pc->zn[2*i])/2;

    }
  for(i = 0, nf = 100, freq.QuadPart = 0; i < nf && freq.QuadPart < dwait/2; i++)
    {
      QueryPerformanceCounter(&ti0);
      wrn_vc(&np, &beta, x, y, z, dx, dy, dz);
      QueryPerformanceCounter(&ti1);
      dtm = ti1.QuadPart - ti0.QuadPart;
      if (i == 0) freq.QuadPart = dtm0 = dtm;
      else freq.QuadPart += dtm;
      dtm0 = (dtm < dtm0) ? dtm : dtm0;
    }

  if (i)  dti0 =(double)(freq.QuadPart)/i;
  else dti0 =(double)(freq.QuadPart);
  dti0 *= 1000000;
  QueryPerformanceFrequency(&freq);
  dfreq = (double)(freq.QuadPart);
  dtm0 *= 1000000;
  win_printf("wirthe over %d points vc computing time = %g \\mu s (mean on %d\n"
	     "min %g \\mu s w = %g",np, (dfreq != 0) ? dti0/dfreq : dti0, i,
	     (dfreq != 0)? (double)dtm0/dfreq : (double)dtm0, beta);

# endif

  free(x);  free(y);  free(z);
  free(dx);  free(dy);  free(dz);
  return beta;
}
double do_compute_wirthe_gilles_float(p_c *pc)
{
  double beta;
  float  *dx, *dy, *dz;
  register int i;
  int np, nf = 100;
  LARGE_INTEGER ti0, ti1, freq;
  long long dtm, dtm0 = 0;
  double dti0,  dfreq;

  np = pc->np;
  dx = (float*)calloc(np,sizeof(float));
  dy = (float*)calloc(np,sizeof(float));
  dz = (float*)calloc(np,sizeof(float));

  if(dx == NULL || dy == NULL || dz == NULL)
    return 0;
  for(i = 0; i < np-1; i++)
    {
      dx[i] = pc->x[i+1] - pc->x[i];
      dy[i] = pc->y[i+1] - pc->y[i];
      dz[i] = pc->z[i+1] - pc->z[i];
    }

  for(i = 0; i < nf; i++)
    {
      QueryPerformanceCounter(&ti0);
      wrn_float(&np, &beta, pc->x, pc->y, pc->z, dx, dy, dz);
      QueryPerformanceCounter(&ti1);
      dtm = ti1.QuadPart - ti0.QuadPart;
      if (i == 0) freq.QuadPart = dtm0 = dtm;
      else freq.QuadPart += dtm;
      dtm0 = (dtm > dtm0) ? dtm : dtm0;
    }

  dti0 =(double)(freq.QuadPart)/nf;
  dti0 *= 1000000;
  QueryPerformanceFrequency(&freq);
  dfreq = (double)(freq.QuadPart);
  dtm0 *= 1000000;
  win_printf("Float wirthe computing time = %g \\mu s\nmax %g \\mu s", (dfreq != 0) ?
	      dti0/dfreq : dti0, (dfreq != 0)? (double)dtm0/dfreq : (double)dtm0);


  free(dx);  free(dy);  free(dz);
  return beta;
}

double do_compute_wirthe_fixed(p_c *pc)
{
  double beta;
  fixed *fdx, *fdy, *fdz;
  register int i;
  int np, nf = 100;
  LARGE_INTEGER ti0, ti1, freq;
  long long dtm, dtm0 = 0;
  double dti0,  dfreq;

  np = pc->np;
  fdx = (fixed*)calloc(np,sizeof(fixed));
  fdy = (fixed*)calloc(np,sizeof(fixed));
  fdz = (fixed*)calloc(np,sizeof(fixed));

  if(fdx == NULL || fdy == NULL || fdz == NULL)
    return 0;
  for(i = 0; i < np-1; i++)
    {
      fdx[i] = pc->fx[i+1] - pc->fx[i];
      fdy[i] = pc->fy[i+1] - pc->fy[i];
      fdz[i] = pc->fz[i+1] - pc->fz[i];
    }

  for(i = 0; i < nf; i++)
    {
      QueryPerformanceCounter(&ti0);
      wrn_f(&np, &beta, pc->fx, pc->fy, pc->fz, fdx, fdy, fdz);
      QueryPerformanceCounter(&ti1);
      dtm = ti1.QuadPart - ti0.QuadPart;
      if (i == 0) freq.QuadPart = dtm0 = dtm;
      else freq.QuadPart += dtm;
      dtm0 = (dtm > dtm0) ? dtm : dtm0;
    }

  dti0 =(double)(freq.QuadPart)/nf;
  dti0 *= 1000000;
  QueryPerformanceFrequency(&freq);
  dfreq = (double)(freq.QuadPart);
  dtm0 *= 1000000;
  win_printf("Fixed wirthe computing time = %g \\mu s\nmax %g \\mu s", (dfreq != 0) ?
	      dti0/dfreq : dti0, (dfreq != 0)? (double)dtm0/dfreq : (double)dtm0);


  free(fdx);  free(fdy);  free(fdz);
  return beta;
}


int partialrot(p_c *pc)
{
	register int l, i, j;
	int np;
	float *x, *y, *z, *xn, *yn, *zn;
	fixed *fx, *fy, *fz, *fxn, *fyn, *fzn;
	float pvex, pvey, pvez, alpha, phi1, phi2;
	float xx ,yy ,zz;
	float coa, coa_1, sia, p, x_p, y_p, z_p;

	x = pc->x;	y = pc->y;	z = pc->z;
	xn = pc->xn;	yn = pc->yn;	zn = pc->zn;
	fx = pc->fx;	fy = pc->fy;	fz = pc->fz;
	fxn = pc->fxn;	fyn = pc->fyn;	fzn = pc->fzn;
	np = pc->np;
	nm = np-1;
	alpha = ran1(&idum)*2*M_PI;
	phi1 = acos(ran1(&idum)*2-1);
	phi2 = ran1(&idum)*2*M_PI;

	i = (int)(ran1(&idum)*(nm-2));
	j = (int)(ran1(&idum)*(nm-i-2)+i+2);

	zz = sin(phi1);
	xx = zz*cos(phi2);
	yy = zz*sin(phi2);
	zz = cos(phi1);
	coa = cos(alpha);
	coa_1 = 1 - coa;
	sia = sin(alpha);
	for(l = i+1; l < j ; l++)
	{
		x_p = x[l]-x[i];
		y_p = y[l]-y[i];
		z_p = z[l]-z[i];
		p = x_p*xx + y_p*yy + z_p*zz;
		pvex = yy*z_p - zz*y_p;
		pvey = zz*x_p - xx*z_p;
		pvez = xx*y_p - yy*x_p;
		xn[l] = xx*coa_1*p + coa*x_p + sia*pvex + x[i];
		yn[l] = yy*coa_1*p + coa*y_p + sia*pvey + y[i];
		zn[l] = zz*coa_1*p + coa*z_p + sia*pvez + z[i];
		fxn[l] = ftofix(xn[l]);
		fyn[l] = ftofix(yn[l]);
		fzn[l] = ftofix(zn[l]);
	}
	l--;
	x_p = xn[l];
	y_p = yn[l];
	z_p = zn[l];
	xx = x[l];
	yy = y[l];
	zz = z[l];
	for(l = j; l < np ; l++)
	{
		xn[l] = x_p + x[l] - xx;
		yn[l] = y_p + y[l] - yy;
		zn[l] = z_p + z[l] - zz;
		fxn[l] = ftofix(xn[l]);
		fyn[l] = ftofix(yn[l]);
		fzn[l] = ftofix(zn[l]);
	}
	for(l = 0; l <= i ; l++)
	{
		xn[l]=x[l];
		yn[l]=y[l];
		zn[l]=z[l];
		fxn[l]=fx[l];
		fyn[l]=fy[l];
		fzn[l]=fz[l];
	}
	return	i;
}

int	crankshaft(p_c *pc, int *pt0, int *pt1)
{
	register int l, i, j;
	float pvex,pvey,pvez;
	float xx, yy, zz, alpha, v;
	float coa, coa_1, sia, p, x_p, y_p, z_p;
	int np;
	float *x, *y, *z, *xn, *yn, *zn;
	fixed *fx, *fy, *fz, *fxn, *fyn, *fzn;

	x = pc->x;	y = pc->y;	z = pc->z;
	xn = pc->xn;	yn = pc->yn;	zn = pc->zn;
	fx = pc->fx;	fy = pc->fy;	fz = pc->fz;
	fxn = pc->fxn;	fyn = pc->fyn;	fzn = pc->fzn;
	np = pc->np;
	nm = np-1;
	alpha = ran1(&idum)*2*M_PI;
	*pt0 = i = (int)(ran1(&idum)*(nm-2));
	*pt1 = j = (int)(ran1(&idum)*(nm-i-2)+i+2);
	xx = (x[j]-x[i]);
	yy = (y[j]-y[i]);
	zz = (z[j]-z[i]);
	v = sqrt(xx*xx + yy*yy + zz*zz);
	xx /= v;
	yy /= v;
	zz /= v;
	coa = cos(alpha);
	coa_1 = 1 - coa;
	sia = sin(alpha);
	for(l = i+1; l < j ; l++)
	{
		x_p = x[l]-x[i];
		y_p = y[l]-y[i];
		z_p = z[l]-z[i];
		p = x_p*xx + y_p*yy + z_p*zz;
		pvex = yy*z_p - zz*y_p;
		pvey = zz*x_p - xx*z_p;
		pvez = xx*y_p - yy*x_p;
		xn[l] = xx*coa_1*p + coa*x_p + sia*pvex + x[i];
		yn[l] = yy*coa_1*p + coa*y_p + sia*pvey + y[i];
		zn[l] = zz*coa_1*p + coa*z_p + sia*pvez + z[i];
		fxn[l] = ftofix(xn[l]);
		fyn[l] = ftofix(yn[l]);
		fzn[l] = ftofix(zn[l]);
	}
	for(l = 0; l <= i; l++)
	{
		xn[l] = x[l];
		yn[l] = y[l];
		zn[l] = z[l];
		fxn[l] = fx[l];
		fyn[l] = fy[l];
		fzn[l] = fz[l];
	}
	for(l = j; l < np; l++)
	{
		xn[l] = x[l];
		yn[l] = y[l];
		zn[l] = z[l];
		fxn[l] = fx[l];
		fyn[l] = fy[l];
		fzn[l] = fz[l];
	}
	return	0;
}
int spit_length_of_each_segment(p_c *pc)
{
	register int i;
	float xx, yy, zz;

	for(i = 0; i < pc->np-1 ; i++)
	{
		xx = pc->x[i+1] - pc->x[i];
		yy = pc->y[i+1] - pc->y[i];
		zz = pc->z[i+1] - pc->z[i];
		fprintf(log_out,"%d %g\n",i,xx*xx+yy*yy+zz*zz);
	}
	return 0;
}
int	Metropolis(p_c *pc, float f)
{
/*	register int i; */
	float de, test, prob, *tmp;
	fixed *ftmp;
	double e;


	de = f*(pc->z[pc->np-1] - pc->zn[pc->np-1]);
	if (pc->B > 0)  // wlc
	  {
	    de += pc->Ebn - pc->Eb;
	  }
	if (de <= 0)
	{
/*
		for (i = 0; i < pc->np; i++)
		{
			pc->x[i] = pc->xn[i];
			pc->y[i] = pc->yn[i];
			pc->z[i] = pc->zn[i];
		}
*/
		tmp = pc->x; pc->x = pc->xn; pc->xn = tmp;
		tmp = pc->y; pc->y = pc->yn; pc->yn = tmp;
		tmp = pc->z; pc->z = pc->zn; pc->zn = tmp;
		ftmp = pc->fx; pc->fx = pc->fxn; pc->fxn = ftmp;
		ftmp = pc->fy; pc->fy = pc->fyn; pc->fyn = ftmp;
		ftmp = pc->fz; pc->fz = pc->fzn; pc->fzn = ftmp;
		e = pc->Eb; pc->Eb = pc->Ebn; pc->Ebn = pc->Eb;
		return 0;
	}
	else
	{
	    test = ran1(&idum);
	    prob = exp(-(de));
	    if (prob > test)
		{
/*
			for (i = 0; i < pc->np; i++)
			{
				pc->x[i] = pc->xn[i];
				pc->y[i] = pc->yn[i];
				pc->z[i] = pc->zn[i];
			}
*/
			tmp = pc->x; pc->x = pc->xn; pc->xn = tmp;
			tmp = pc->y; pc->y = pc->yn; pc->yn = tmp;
			tmp = pc->z; pc->z = pc->zn; pc->zn = tmp;
			ftmp = pc->fx; pc->fx = pc->fxn; pc->fxn = ftmp;
			ftmp = pc->fy; pc->fy = pc->fyn; pc->fyn = ftmp;
			ftmp = pc->fz; pc->fz = pc->fzn; pc->fzn = ftmp;
			e = pc->Eb; pc->Eb = pc->Ebn; pc->Ebn = pc->Eb;
			return 0;
		}
	    else return 1;
	}
	return 1;
}
int  realloc_chain_arrays(p_c *pc, int np)
{
	float *x, *y, *z, *xn, *yn, *zn, *xr, *yr, *zr;

	x = (float *)realloc(pc->x,np*sizeof(float));
	y = (float *)realloc(pc->y,np*sizeof(float));
	z = (float *)realloc(pc->z,np*sizeof(float));
	xn = (float *)realloc(pc->xn,np*sizeof(float));
	yn = (float *)realloc(pc->yn,np*sizeof(float));
	zn = (float *)realloc(pc->zn,np*sizeof(float));
	xr = (float *)realloc(pc->xr,np*sizeof(float));
	yr = (float *)realloc(pc->yr,np*sizeof(float));
	zr = (float *)realloc(pc->zr,np*sizeof(float));
	pc->fx = (fixed *)realloc(pc->fx,np*sizeof(fixed));
	pc->fy = (fixed *)realloc(pc->fy,np*sizeof(fixed));
	pc->fz = (fixed *)realloc(pc->fz,np*sizeof(fixed));
	pc->fxn = (fixed *)realloc(pc->fxn,np*sizeof(fixed));
	pc->fyn = (fixed *)realloc(pc->fyn,np*sizeof(fixed));
	pc->fzn = (fixed *)realloc(pc->fzn,np*sizeof(fixed));
	if (x == NULL || y == NULL || z == NULL || xn == NULL || yn == NULL || zn == NULL)
		return 1;
	if (xr == NULL || yr == NULL || zr == NULL) 		return 1;
	if (pc->fx == NULL || pc->fy == NULL || pc->fz == NULL) 	return 1;
	if (pc->fxn == NULL || pc->fyn == NULL || pc->fzn == NULL) 	return 1;
	pc->x = x;
	pc->y = y;
	pc->z = z;
	pc->xn = xn;
	pc->yn = yn;
	pc->zn = zn;
	pc->xr = xr;
	pc->yr = yr;
	pc->zr = zr;
	pc->np = np;
	return 0;
}

p_c *initial_configuration(int np, float dmin)
{
	register int i, j;
	float phi1, phi2;
	p_c *pc = NULL;
	fixed fdmin, fdmin2;

	pc = (p_c *)calloc(1,sizeof(p_c));
	if (pc == NULL)	return NULL;

	fdmin = ftofix(dmin);
	fdmin2 = ftofix(dmin*dmin);
	if (realloc_chain_arrays(pc, np) != 0)
	  {
	    win_printf_OK("Cannot alocate chain memory!");
	    return NULL;
	  }
	pc->np = np;
	pc->kp = 0;
	pc->rottrial = pc->rottriale = pc->rotrefa = pc->rotrefe = pc->cranktrial = pc->crankref = 0;
	/*
	for (i = 1; i < np; i++)
	{

		z[i] = i;
		pc->fz[i] = ftofix(z[i]);
		pc->fx[i] = pc->fy[i] = ftofix(0);
	}
	return pc;
*/
	i = 0;
	pc->fx[i] = pc->fy[i] = pc->fz[i] = ftofix(0);
	for (i = 1; i < np; )
	{
		phi1 = acos(ran1(&idum)*2-1);
		phi2 = ran1(&idum)*2*M_PI;
		pc->xr[i] = sin(phi1)*cos(phi2);
		pc->yr[i] = sin(phi1)*sin(phi2);
		pc->zr[i] = cos(phi1);
		for (j = 1, pc->x[i] = pc->y[i] = pc->z[i] = 0; j <= i; j++)
		{
		     pc->x[i] += pc->xr[j];
		     pc->y[i] += pc->yr[j];
		     pc->z[i] += pc->zr[j];
		}
		pc->fx[i] = ftofix(pc->x[i]);
		pc->fy[i] = ftofix(pc->y[i]);
		pc->fz[i] = ftofix(pc->z[i]);
		if(selfavoid(i,pc->fx,pc->fy,pc->fz,i+1,fdmin, fdmin2)) i++;
		display_title_message("init i = %d",i);
	}
	return pc;
}

int	random_configuration(p_c *pc, float dmin)
{
	register int i, j;
	int np;
	float *x, *y, *z, *xn, *yn, *zn,  phi1, phi2;
	static float *xr = NULL, *yr = NULL, *zr = NULL;
	static int npp = 0;
	fixed fdmin, fdmin2;
	fixed *fx, *fy, *fz, *fxn, *fyn, *fzn;

	np = pc->np;

	if (npp < np)
	  {
	    xr = (float *)realloc(xr,np*sizeof(float));
	    yr = (float *)realloc(yr,np*sizeof(float));
	    zr = (float *)realloc(zr,np*sizeof(float));
	    npp = np;
	  }
	if (xr == NULL || yr == NULL || zr == NULL)
	{
		win_printf("Malloc pb");
		return 1;
	}

	fdmin = ftofix(dmin);
	fdmin2 = ftofix(dmin*dmin);
	x = pc->x;	y = pc->y;	z = pc->z;
	xn = pc->xn;	yn = pc->yn;	zn = pc->zn;
	fx = pc->fx;	fy = pc->fy;	fz = pc->fz;
	fxn = pc->fxn;	fyn = pc->fyn;	fzn = pc->fzn;
	np = pc->np;
	i = 0;
	pc->fx[i] = pc->fy[i] = pc->fz[i] = ftofix(0);
	for (i = 1; i < np; )
	{
		phi1 = acos(ran1(&idum)*2-1);
		phi2 = ran1(&idum)*2*M_PI;
		xr[i] = sin(phi1)*cos(phi2);
		yr[i] = sin(phi1)*sin(phi2);
		zr[i] = cos(phi1);
		for (j = 1, x[i] = y[i] = z[i] = 0; j <= i; j++)
		{
		     x[i] += xr[j];
		     y[i] += yr[j];
		     z[i] += zr[j];
		}
		pc->fx[i] = ftofix(x[i]);
		pc->fy[i] = ftofix(y[i]);
		pc->fz[i] = ftofix(z[i]);
		if(selfavoid(i,pc->fx,pc->fy,pc->fz,i+1,fdmin, fdmin2)) i++;
	}

	return 0;
}


int	straight_helical_configuration(p_c *pc, float dmin, float phi1, float phi2, int size)
{
	register int i, j, k;
	int np;
	float *x, *y, *z, *xn, *yn, *zn;
	static float *xr = NULL, *yr = NULL, *zr = NULL;
	static int npp = 0;
	fixed fdmin, fdmin2;
	fixed *fx, *fy, *fz, *fxn, *fyn, *fzn;

	np = pc->np;
	win_printf("phi1 %g phi2 %g",phi1,phi2);

	if (npp < np)
	  {
	    xr = (float *)realloc(xr,np*sizeof(float));
	    yr = (float *)realloc(yr,np*sizeof(float));
	    zr = (float *)realloc(zr,np*sizeof(float));
	    npp = np;
	  }
	if (xr == NULL || yr == NULL || zr == NULL)
	{
		win_printf("Malloc pb");
		return 1;
	}

	fdmin = ftofix(dmin);
	fdmin2 = ftofix(dmin*dmin);
	x = pc->x;	y = pc->y;	z = pc->z;
	xn = pc->xn;	yn = pc->yn;	zn = pc->zn;
	fx = pc->fx;	fy = pc->fy;	fz = pc->fz;
	fxn = pc->fxn;	fyn = pc->fyn;	fzn = pc->fzn;
	np = pc->np;
	i = 0;
	pc->fx[i] = pc->fy[i] = pc->fz[i] = ftofix(0);
	for (i = 1; i < (np-size)/2;  )
	{
		xr[i] = 0;
		yr[i] = 0;
		zr[i] = 1;
		for (j = 1, x[i] = y[i] = z[i] = 0; j <= i; j++)
		{
		     x[i] += xr[j];
		     y[i] += yr[j];
		     z[i] += zr[j];
		}
		pc->fx[i] = ftofix(x[i]);
		pc->fy[i] = ftofix(y[i]);
		pc->fz[i] = ftofix(z[i]);
		//		if(selfavoid(i,pc->fx,pc->fy,pc->fz,i+1,fdmin, fdmin2)) i++;
		i++;
	}
	for (k = 0; k < size; k++)
	{
		xr[i] = sin(phi1)*cos(k*phi2);
		yr[i] = sin(phi1)*sin(k*phi2);
		zr[i] = cos(phi1);
		for (j = 1, x[i] = y[i] = z[i] = 0; j <= i; j++)
		{
		     x[i] += xr[j];
		     y[i] += yr[j];
		     z[i] += zr[j];
		}
		pc->fx[i] = ftofix(x[i]);
		pc->fy[i] = ftofix(y[i]);
		pc->fz[i] = ftofix(z[i]);
		//		if(selfavoid(i,pc->fx,pc->fy,pc->fz,i+1,fdmin, fdmin2)) i++;
		i++;
	}
	for (; i < np; )
	{
		xr[i] = 0;
		yr[i] = 0;
		zr[i] = 1;
		for (j = 1, x[i] = y[i] = z[i] = 0; j <= i; j++)
		{
		     x[i] += xr[j];
		     y[i] += yr[j];
		     z[i] += zr[j];
		}
		pc->fx[i] = ftofix(x[i]);
		pc->fy[i] = ftofix(y[i]);
		pc->fz[i] = ftofix(z[i]);
		//		if(selfavoid(i,pc->fx,pc->fy,pc->fz,i+1,fdmin, fdmin2)) i++;
		i++;
	}


	return 0;
}

int	curved_helical_configuration(p_c *pc, float dmin, float phi1, float phi2, int size, int curvesize)
{
	register int i, j, k;
	int np;
	float *x, *y, *z, *xn, *yn, *zn;
	static float *xr = NULL, *yr = NULL, *zr = NULL;
	static int npp = 0, ns;
	fixed fdmin, fdmin2;
	fixed *fx, *fy, *fz, *fxn, *fyn, *fzn;

	np = pc->np;
	win_printf("phi1 %g phi2 %g",phi1,phi2);

	if (npp < np)
	  {
	    xr = (float *)realloc(xr,np*sizeof(float));
	    yr = (float *)realloc(yr,np*sizeof(float));
	    zr = (float *)realloc(zr,np*sizeof(float));
	    npp = np;
	  }
	if (xr == NULL || yr == NULL || zr == NULL)
	{
		win_printf("Malloc pb");
		return 1;
	}

	fdmin = ftofix(dmin);
	fdmin2 = ftofix(dmin*dmin);
	x = pc->x;	y = pc->y;	z = pc->z;
	xn = pc->xn;	yn = pc->yn;	zn = pc->zn;
	fx = pc->fx;	fy = pc->fy;	fz = pc->fz;
	fxn = pc->fxn;	fyn = pc->fyn;	fzn = pc->fzn;
	np = pc->np;
	i = 0;
	ns = ((np - size)/2) - curvesize;
	pc->fx[i] = pc->fy[i] = pc->fz[i] = ftofix(0);
	for (i = 1; i < ns;  )
	{
		xr[i] = 0;
		yr[i] = 0;
		zr[i] = 1;
		for (j = 1, x[i] = y[i] = z[i] = 0; j <= i; j++)
		{
		     x[i] += xr[j];
		     y[i] += yr[j];
		     z[i] += zr[j];
		}
		pc->fx[i] = ftofix(x[i]);
		pc->fy[i] = ftofix(y[i]);
		pc->fz[i] = ftofix(z[i]);
		//		if(selfavoid(i,pc->fx,pc->fy,pc->fz,i+1,fdmin, fdmin2)) i++;
		i++;
	}
	for (k = 0; k < curvesize; k++)
	{
	        xr[i] = sin((phi1*k)/curvesize);
		yr[i] = 0;
		zr[i] = cos((phi1*k)/curvesize);
		for (j = 1, x[i] = y[i] = z[i] = 0; j <= i; j++)
		{
		     x[i] += xr[j];
		     y[i] += yr[j];
		     z[i] += zr[j];
		}
		pc->fx[i] = ftofix(x[i]);
		pc->fy[i] = ftofix(y[i]);
		pc->fz[i] = ftofix(z[i]);
		//		if(selfavoid(i,pc->fx,pc->fy,pc->fz,i+1,fdmin, fdmin2)) i++;
		i++;
	}
	for (k = 0; k < size; k++)
	{
		xr[i] = sin(phi1)*cos(k*phi2);
		yr[i] = sin(phi1)*sin(k*phi2);
		zr[i] = cos(phi1);
		for (j = 1, x[i] = y[i] = z[i] = 0; j <= i; j++)
		{
		     x[i] += xr[j];
		     y[i] += yr[j];
		     z[i] += zr[j];
		}
		pc->fx[i] = ftofix(x[i]);
		pc->fy[i] = ftofix(y[i]);
		pc->fz[i] = ftofix(z[i]);
		//		if(selfavoid(i,pc->fx,pc->fy,pc->fz,i+1,fdmin, fdmin2)) i++;
		i++;
	}
	for (k = curvesize; k > 0; k--)
	{
	        xr[i] = sin((phi1*k)/curvesize);
		yr[i] = 0;
		zr[i] = cos((phi1*k)/curvesize);
		for (j = 1, x[i] = y[i] = z[i] = 0; j <= i; j++)
		{
		     x[i] += xr[j];
		     y[i] += yr[j];
		     z[i] += zr[j];
		}
		pc->fx[i] = ftofix(x[i]);
		pc->fy[i] = ftofix(y[i]);
		pc->fz[i] = ftofix(z[i]);
		//		if(selfavoid(i,pc->fx,pc->fy,pc->fz,i+1,fdmin, fdmin2)) i++;
		i++;
	}
	for (; i < np; )
	{
		xr[i] = 0;
		yr[i] = 0;
		zr[i] = 1;
		for (j = 1, x[i] = y[i] = z[i] = 0; j <= i; j++)
		{
		     x[i] += xr[j];
		     y[i] += yr[j];
		     z[i] += zr[j];
		}
		pc->fx[i] = ftofix(x[i]);
		pc->fy[i] = ftofix(y[i]);
		pc->fz[i] = ftofix(z[i]);
		//		if(selfavoid(i,pc->fx,pc->fy,pc->fz,i+1,fdmin, fdmin2)) i++;
		i++;
	}


	return 0;
}
int	straight_loops_configuration(p_c *pc, float dmin, float phi1, float phi2, int size)
{
	register int i, j, k;
	int np;
	float *x, *y, *z, *xn, *yn, *zn;
	static float *xr = NULL, *yr = NULL, *zr = NULL;
	static int npp = 0;
	fixed fdmin, fdmin2;
	fixed *fx, *fy, *fz, *fxn, *fyn, *fzn;

	np = pc->np;
	win_printf("phi1 %g phi2 %g",phi1,phi2);

	if (npp < np)
	  {
	    xr = (float *)realloc(xr,np*sizeof(float));
	    yr = (float *)realloc(yr,np*sizeof(float));
	    zr = (float *)realloc(zr,np*sizeof(float));
	    npp = np;
	  }
	if (xr == NULL || yr == NULL || zr == NULL)
	{
		win_printf("Malloc pb");
		return 1;
	}

	fdmin = ftofix(dmin);
	fdmin2 = ftofix(dmin*dmin);
	x = pc->x;	y = pc->y;	z = pc->z;
	xn = pc->xn;	yn = pc->yn;	zn = pc->zn;
	fx = pc->fx;	fy = pc->fy;	fz = pc->fz;
	fxn = pc->fxn;	fyn = pc->fyn;	fzn = pc->fzn;
	np = pc->np;
	i = 0;
	pc->fx[i] = pc->fy[i] = pc->fz[i] = ftofix(0);
	for (i = 1; i < (np-size)/2;  )
	{
		xr[i] = 0;
		yr[i] = 0;
		zr[i] = 1;
		for (j = 1, x[i] = y[i] = z[i] = 0; j <= i; j++)
		{
		     x[i] += xr[j];
		     y[i] += yr[j];
		     z[i] += zr[j];
		}
		pc->fx[i] = ftofix(x[i]);
		pc->fy[i] = ftofix(y[i]);
		pc->fz[i] = ftofix(z[i]);
		//		if(selfavoid(i,pc->fx,pc->fy,pc->fz,i+1,fdmin, fdmin2)) i++;
		i++;
	}
	for (k = 0; k < size; k++)
	{
		zr[i] = sin(phi1)*cos(k*phi2);
		xr[i] = sin(phi1)*sin(k*phi2);
		yr[i] = cos(phi1);
		for (j = 1, x[i] = y[i] = z[i] = 0; j <= i; j++)
		{
		     x[i] += xr[j];
		     y[i] += yr[j];
		     z[i] += zr[j];
		}
		pc->fx[i] = ftofix(x[i]);
		pc->fy[i] = ftofix(y[i]);
		pc->fz[i] = ftofix(z[i]);
		//		if(selfavoid(i,pc->fx,pc->fy,pc->fz,i+1,fdmin, fdmin2)) i++;
		i++;
	}
	for (; i < np; )
	{
		xr[i] = 0;
		yr[i] = 0;
		zr[i] = 1;
		for (j = 1, x[i] = y[i] = z[i] = 0; j <= i; j++)
		{
		     x[i] += xr[j];
		     y[i] += yr[j];
		     z[i] += zr[j];
		}
		pc->fx[i] = ftofix(x[i]);
		pc->fy[i] = ftofix(y[i]);
		pc->fz[i] = ftofix(z[i]);
		//		if(selfavoid(i,pc->fx,pc->fy,pc->fz,i+1,fdmin, fdmin2)) i++;
		i++;
	}


	return 0;
}

int	plasmid_loops_configuration(p_c *pc, float dmin, int ndz, int nt, int size)
{
	register int i, j, k;
	int np, nc;
	float *x, *y, *z, *xn, *yn, *zn;
	static float *xr = NULL, *yr = NULL, *zr = NULL;
	double dz, sinp, phi2;
	static int npp = 0;
	fixed fdmin, fdmin2;
	fixed *fx, *fy, *fz, *fxn, *fyn, *fzn;

	np = pc->np;

	if (npp < np)
	  {
	    xr = (float *)realloc(xr,np*sizeof(float));
	    yr = (float *)realloc(yr,np*sizeof(float));
	    zr = (float *)realloc(zr,np*sizeof(float));
	    npp = np;
	  }
	if (xr == NULL || yr == NULL || zr == NULL)
	{
		win_printf("Malloc pb");
		return 1;
	}

	fdmin = ftofix(dmin);
	fdmin2 = ftofix(dmin*dmin);
	x = pc->x;	y = pc->y;	z = pc->z;
	xn = pc->xn;	yn = pc->yn;	zn = pc->zn;
	fx = pc->fx;	fy = pc->fy;	fz = pc->fz;
	fxn = pc->fxn;	fyn = pc->fyn;	fzn = pc->fzn;
	np = pc->np;
	nc = np - size - ndz;
	dz = (double)ndz/size;
	sinp = sqrt(1 - dz * dz);
	phi2 = (M_PI*2)/nt;

	i = 0;
	pc->fx[i] = pc->fy[i] = pc->fz[i] = ftofix(0);
	x[0] = y[0] = z[0] = 0;
	for (i = k = 1; k < nc/2; k++ )
	{
		yr[i] = sin((2*M_PI*k)/nc);
		xr[i] = 0;
		zr[i] = cos((2*M_PI*k)/nc);
		for (j = 1, x[i] = y[i] = z[i] = 0; j <= i; j++)
		{
		     x[i] += xr[j];
		     y[i] += yr[j];
		     z[i] += zr[j];
		}
		pc->fx[i] = ftofix(x[i]);
		pc->fy[i] = ftofix(y[i]);
		pc->fz[i] = ftofix(z[i]);
		//		if(selfavoid(i,pc->fx,pc->fy,pc->fz,i+1,fdmin, fdmin2)) i++;
		i++;
	}
	for (k = 0; k < size; k++)
	{
		zr[i] = -sinp*cos(k*phi2);
		xr[i] = sinp*sin(k*phi2);
		yr[i] = dz;
		for (j = 1, x[i] = y[i] = z[i] = 0; j <= i; j++)
		{
		     x[i] += xr[j];
		     y[i] += yr[j];
		     z[i] += zr[j];
		}
		pc->fx[i] = ftofix(x[i]);
		pc->fy[i] = ftofix(y[i]);
		pc->fz[i] = ftofix(z[i]);
		//		if(selfavoid(i,pc->fx,pc->fy,pc->fz,i+1,fdmin, fdmin2)) i++;
		i++;
	}
	for (k = 0; k < nc/4; k++  )
	{
		yr[i] = -sin((2*M_PI*k)/nc);
		xr[i] = 0;
		zr[i] = -cos((2*M_PI*k)/nc);
		for (j = 1, x[i] = y[i] = z[i] = 0; j <= i; j++)
		{
		     x[i] += xr[j];
		     y[i] += yr[j];
		     z[i] += zr[j];
		}
		pc->fx[i] = ftofix(x[i]);
		pc->fy[i] = ftofix(y[i]);
		pc->fz[i] = ftofix(z[i]);
		//		if(selfavoid(i,pc->fx,pc->fy,pc->fz,i+1,fdmin, fdmin2)) i++;
		i++;
	}
	for (k = 0; k < ndz; k++  )
	{
		yr[i] = -1;
		xr[i] = 0;
		zr[i] = 0;
		for (j = 1, x[i] = y[i] = z[i] = 0; j <= i; j++)
		{
		     x[i] += xr[j];
		     y[i] += yr[j];
		     z[i] += zr[j];
		}
		pc->fx[i] = ftofix(x[i]);
		pc->fy[i] = ftofix(y[i]);
		pc->fz[i] = ftofix(z[i]);
		//		if(selfavoid(i,pc->fx,pc->fy,pc->fz,i+1,fdmin, fdmin2)) i++;
		i++;
	}
	for (k = 0; i < np; k++  )
	{
		zr[i] = sin((2*M_PI*k)/nc);
		xr[i] = 0;
		yr[i] = -cos((2*M_PI*k)/nc);
		for (j = 1, x[i] = y[i] = z[i] = 0; j <= i; j++)
		{
		     x[i] += xr[j];
		     y[i] += yr[j];
		     z[i] += zr[j];
		}
		pc->fx[i] = ftofix(x[i]);
		pc->fy[i] = ftofix(y[i]);
		pc->fz[i] = ftofix(z[i]);
		//		if(selfavoid(i,pc->fx,pc->fy,pc->fz,i+1,fdmin, fdmin2)) i++;
		i++;
	}
	win_printf("X[0] %g y[0] %g z[0] %g\nX[%d] %g y[%d] %g z[%d] %g\n",
		   x[0],y[0],z[0],np-1,x[np-1],np-1,y[np-1],np-1,z[np-1]);
	plasmid = 1;
	return 0;
}

int	plasmid_helical_configuration(p_c *pc, float dmin, int ndz, int nt, int size)
{
	register int i, j, k;
	int np, nc;
	float *x, *y, *z, *xn, *yn, *zn;
	static float *xr = NULL, *yr = NULL, *zr = NULL;
	double dz, sinp, phi2;
	static int npp = 0;
	fixed fdmin, fdmin2;
	fixed *fx, *fy, *fz, *fxn, *fyn, *fzn;

	np = pc->np;

	if (npp < np)
	  {
	    xr = (float *)realloc(xr,np*sizeof(float));
	    yr = (float *)realloc(yr,np*sizeof(float));
	    zr = (float *)realloc(zr,np*sizeof(float));
	    npp = np;
	  }
	if (xr == NULL || yr == NULL || zr == NULL)
	{
		win_printf("Malloc pb");
		return 1;
	}

	fdmin = ftofix(dmin);
	fdmin2 = ftofix(dmin*dmin);
	x = pc->x;	y = pc->y;	z = pc->z;
	xn = pc->xn;	yn = pc->yn;	zn = pc->zn;
	fx = pc->fx;	fy = pc->fy;	fz = pc->fz;
	fxn = pc->fxn;	fyn = pc->fyn;	fzn = pc->fzn;
	np = pc->np;
	nc = np + 1 - size - ndz;
	dz = (double)ndz/size;
	sinp = sqrt(1 - dz * dz);
	phi2 = (M_PI*2)/nt;

	i = 0;
	pc->fx[i] = pc->fy[i] = pc->fz[i] = ftofix(0);
	for (i = 1; i < ndz/2;  )
	{
		xr[i] = 0;
		yr[i] = 0;
		zr[i] = 1;
		for (j = 1, x[i] = y[i] = z[i] = 0; j <= i; j++)
		{
		     x[i] += xr[j];
		     y[i] += yr[j];
		     z[i] += zr[j];
		}
		pc->fx[i] = ftofix(x[i]);
		pc->fy[i] = ftofix(y[i]);
		pc->fz[i] = ftofix(z[i]);
		//		if(selfavoid(i,pc->fx,pc->fy,pc->fz,i+1,fdmin, fdmin2)) i++;
		i++;
	}
	for (k = 0; k < nc/2; k++ )
	{
		xr[i] = sin((2*M_PI*k)/nc);
		yr[i] = 0;
		zr[i] = cos((2*M_PI*k)/nc);
		for (j = 1, x[i] = y[i] = z[i] = 0; j <= i; j++)
		{
		     x[i] += xr[j];
		     y[i] += yr[j];
		     z[i] += zr[j];
		}
		pc->fx[i] = ftofix(x[i]);
		pc->fy[i] = ftofix(y[i]);
		pc->fz[i] = ftofix(z[i]);
		//		if(selfavoid(i,pc->fx,pc->fy,pc->fz,i+1,fdmin, fdmin2)) i++;
		i++;
	}
	for (k = 0; k < size; k++)
	{
		xr[i] = sinp*cos(k*phi2);
		yr[i] = sinp*sin(k*phi2);
		zr[i] = -dz;
		for (j = 1, x[i] = y[i] = z[i] = 0; j <= i; j++)
		{
		     x[i] += xr[j];
		     y[i] += yr[j];
		     z[i] += zr[j];
		}
		pc->fx[i] = ftofix(x[i]);
		pc->fy[i] = ftofix(y[i]);
		pc->fz[i] = ftofix(z[i]);
		//		if(selfavoid(i,pc->fx,pc->fy,pc->fz,i+1,fdmin, fdmin2)) i++;
		i++;
	}
	for (k = 0; k < nc/2; k++  )
	{
		xr[i] = -sin((2*M_PI*k)/nc);
		yr[i] = 0;
		zr[i] = -cos((2*M_PI*k)/nc);
		for (j = 1, x[i] = y[i] = z[i] = 0; j <= i; j++)
		{
		     x[i] += xr[j];
		     y[i] += yr[j];
		     z[i] += zr[j];
		}
		pc->fx[i] = ftofix(x[i]);
		pc->fy[i] = ftofix(y[i]);
		pc->fz[i] = ftofix(z[i]);
		//		if(selfavoid(i,pc->fx,pc->fy,pc->fz,i+1,fdmin, fdmin2)) i++;
		i++;
	}
	for (; i < np; )
	{
		xr[i] = 0;
		yr[i] = 0;
		zr[i] = 1;
		for (j = 1, x[i] = y[i] = z[i] = 0; j <= i; j++)
		{
		     x[i] += xr[j];
		     y[i] += yr[j];
		     z[i] += zr[j];
		}
		pc->fx[i] = ftofix(x[i]);
		pc->fy[i] = ftofix(y[i]);
		pc->fz[i] = ftofix(z[i]);
		//		if(selfavoid(i,pc->fx,pc->fy,pc->fz,i+1,fdmin, fdmin2)) i++;
		i++;
	}
	win_printf("X[0] %g y[0] %g z[0] %g\nX[%d] %g y[%d] %g z[%d] %g\n",
		   x[0],y[0],z[0],np-1,x[np-1],np-1,y[np-1],np-1,z[np-1]);
	plasmid = 1;
	return 0;
}



int	helical_configuration(p_c *pc, float dmin, float phi1, float phi2)
{
	register int i, j;
	int np;
	float *x, *y, *z, *xn, *yn, *zn;
	static float *xr = NULL, *yr = NULL, *zr = NULL;
	fixed fdmin, fdmin2;
	fixed *fx, *fy, *fz, *fxn, *fyn, *fzn;

	np = pc->np;

	if (xr == NULL) xr = (float *)calloc(np,sizeof(float));
	if (yr == NULL) yr = (float *)calloc(np,sizeof(float));
	if (zr == NULL) zr = (float *)calloc(np,sizeof(float));
	if (xr == NULL || yr == NULL || zr == NULL)
	{
		win_printf("Malloc pb");
		return 1;
	}

	fdmin = ftofix(dmin);
	fdmin2 = ftofix(dmin*dmin);
	x = pc->x;	y = pc->y;	z = pc->z;
	xn = pc->xn;	yn = pc->yn;	zn = pc->zn;
	fx = pc->fx;	fy = pc->fy;	fz = pc->fz;
	fxn = pc->fxn;	fyn = pc->fyn;	fzn = pc->fzn;
	np = pc->np;
	i = 0;
	pc->fx[i] = pc->fy[i] = pc->fz[i] = ftofix(0);
	for (i = 1; i < np; )
	{
		xr[i] = sin(phi1)*cos(i*phi2);
		yr[i] = sin(phi1)*sin(i*phi2);
		zr[i] = cos(phi1);
		for (j = 1, x[i] = y[i] = z[i] = 0; j <= i; j++)
		{
		     x[i] += xr[j];
		     y[i] += yr[j];
		     z[i] += zr[j];
		}
		pc->fx[i] = ftofix(x[i]);
		pc->fy[i] = ftofix(y[i]);
		pc->fz[i] = ftofix(z[i]);
		//		if(selfavoid(i,pc->fx,pc->fy,pc->fz,i+1,fdmin, fdmin2)) i++;
		i++;
	}

	return 0;
}


int	plecto_configuration(p_c *pc, float dmin, float phi1, float phi2)
{
	register int i, j;
	int np;
	float *x, *y, *z, *xn, *yn, *zn;
	static float *xr = NULL, *yr = NULL, *zr = NULL;
	fixed fdmin, fdmin2;
	fixed *fx, *fy, *fz, *fxn, *fyn, *fzn;

	np = pc->np;

	if (xr == NULL) xr = (float *)calloc(np,sizeof(float));
	if (yr == NULL) yr = (float *)calloc(np,sizeof(float));
	if (zr == NULL) zr = (float *)calloc(np,sizeof(float));
	if (xr == NULL || yr == NULL || zr == NULL)
	{
		win_printf("Malloc pb");
		return 1;
	}

	fdmin = ftofix(dmin);
	fdmin2 = ftofix(dmin*dmin);
	x = pc->x;	y = pc->y;	z = pc->z;
	xn = pc->xn;	yn = pc->yn;	zn = pc->zn;
	fx = pc->fx;	fy = pc->fy;	fz = pc->fz;
	fxn = pc->fxn;	fyn = pc->fyn;	fzn = pc->fzn;
	np = pc->np;
	i = 0;
	pc->fx[i] = pc->fy[i] = pc->fz[i] = ftofix(0);
	for (i = 1; i < np; )
	{
		xr[i] = sin(phi1)*cos(i*phi2);
		yr[i] = sin(phi1)*sin(i*phi2);
		zr[i] = cos(phi1);
		for (j = 1, x[i] = y[i] = z[i] = 0; j <= i; j++)
		{
		     x[i] += xr[j];
		     y[i] += yr[j];
		     z[i] += zr[j];
		}
		pc->fx[i] = ftofix(x[i]);
		pc->fy[i] = ftofix(y[i]);
		pc->fz[i] = ftofix(z[i]);
		//		if(selfavoid(i,pc->fx,pc->fy,pc->fz,i+1,fdmin, fdmin2)) i++;
		i++;
	}

	return 0;
}


int	monte_carlo(p_c *pc, float f, float proba, fixed dmin, fixed dmin2)
{
	register int  j = 0;
	int imoved, imoved2, nottouching;
	float pt, *tmp;
	fixed *ftmp;

	pc->kp++;

	pt=ran1(&idum);
	if (pt > proba)
	{
		imoved = partialrot(pc);
		pc->rottrial++;
/*
		for (i = imoved+1,nottouching = 1; i < pc->np && nottouching == 1; i++)
			nottouching = selfavoid(i,pc->fxn,pc->fyn,pc->fzn,pc->np,dmin,dmin2);
*/
		nottouching = multi_selfavoid(imoved+1,pc->np,pc->fxn,pc->fyn,pc->fzn,pc->np,dmin,dmin2);
		if (nottouching == 0)	pc->rotrefa++;
		else
		{
		  if (pc->B > 0) compute_bending_energy_new_ch(pc);
		  j = Metropolis(pc, f);
		  pc->rotrefe += j;
		  j = (j == 1) ? 0 : 1;
		  pc->rottriale++;
		}
	}
	else
	{
		crankshaft(pc,&imoved,&imoved2);
		pc->cranktrial++;
/*
		for (i = imoved+1, nottouching = 1; i < imoved2 && nottouching == 1; i++)
			nottouching = selfavoid(i,pc->fxn,pc->fyn,pc->fzn,pc->np,dmin,dmin2);
*/
		nottouching = multi_selfavoid(imoved+1,imoved2,pc->fxn,pc->fyn,pc->fzn,pc->np,dmin,dmin2);
		if (nottouching == 0)	pc->crankref++;
		else
		{
		  if (pc->B > 0)
		    {
		      compute_bending_energy_new_ch(pc);
		      j = Metropolis(pc, f);
		      j = (j == 1) ? 0 : 1;
		    }
		  else
		    {
		      j = 0; /* we move, it was 1*/
		      tmp = pc->x; pc->x = pc->xn; pc->xn = tmp;
		      tmp = pc->y; pc->y = pc->yn; pc->yn = tmp;
		      tmp = pc->z; pc->z = pc->zn; pc->zn = tmp;
		      ftmp = pc->fx; pc->fx = pc->fxn; pc->fxn = ftmp;
		      ftmp = pc->fy; pc->fy = pc->fyn; pc->fyn = ftmp;
		      ftmp = pc->fz; pc->fz = pc->fzn; pc->fzn = ftmp;
		    }
		}
	}
	return j;
}
int   do_once(void)
{
  register int j;
  pc->i_iter++;
  if (pc->mode == 0)
    {
      if (pc->i_iter > pc->niter) return   change_angle_in_3D_plot_region();
    }
  pc->rottrial = pc->rottriale = pc->rotrefa = 0;
  pc->rotrefe = pc->cranktrial = pc->crankref = 0;
  lselfavoid = 0;
  nlselfavoid = 0;

  for (j = 0; j < pc->n_test; j++)
    {
      monte_carlo(pc, force, proba, fdmin, fdmin2);
    }
  tau_rota = (pc->rottrial) ? (float)pc->rotrefa/pc->rottrial : 0;
  tau_rote = (pc->rottriale) ? (float)pc->rotrefe/pc->rottriale : 0;
  tau_crank = (pc->cranktrial) ? (float)pc->crankref/pc->cranktrial : 0;

  call_3D_indirect = 1;
  do_3D_chain_draw();
  call_3D_indirect = 0;
  change_angle_in_3D_plot_region();
  return 0;
}
int   do_one(void)
{
    if(updating_menu_state != 0)	return D_O_K;
    do_once();
  return 0;
}

int   stop_pc(void)
{
    if(updating_menu_state != 0)	return D_O_K;
    pc->mode = 0;
  return 0;
}
int   go_pc(void)
{
    if(updating_menu_state != 0)	return D_O_K;
    pc->mode = 1;
  return 0;
}


int	fjca_main_new(void)
{
	register int i, k;
	static int first = 1;
	static int mode = 1;
	float b = 0;

    if(updating_menu_state != 0)	return D_O_K;

	if (first)
	{
	  //	  add_plot_treat_menu_item ( "fjca", NULL, fjca_plot_menu(), 0, NULL);
	  first = 0;


	  i = win_scanf("number of segment %8dforce %8f\n"
		      "d_{min}%8fproba%8f\n"
			"ntest (between display) %8d\n"
			"Finite nb of iter Yes %R Continuous %r\n"
			" if yes niter %8d\n"
			"Bending energy %8f\n"
		      ,&nm,&force,&dmin,
		      &proba,&n_test,&mode,&niter,&b);
	  if (i == CANCEL)	return 0;
	  fdmin = ftofix(dmin);
	  fdmin2 = ftofix(dmin*dmin);
	  if (pc == NULL)	   pc = initial_configuration(nm+1, dmin);
	  if (pc == NULL)	   return fprintf(stderr,"cannot allocate memory for pc\n");
	  pc->i_iter = 0;
	  pc->niter = niter;
	  pc->n_test = n_test;
	  pc->mode = mode;
	  pc->B = b;
	  if (pc->B > 0) compute_bending_energy_cur_ch(pc);
	  fjca_3D_plot(pc);
	}

	k = pc->np-1;
	hide_trial();
	do_once();
	return 0;
}


int	fjca_main1(int argc, char **argv)
{
	register int i, j, k;
	float tau_rota, tau_rote, tau_crank, lmean, x2mean, y2mean, r2mean, x2, y2;
	float xmean, ymean, lmeant, x2meant, y2meant;
	float lmean_2, x2mean_2, y2mean_2;
	FILE *fp;
	static char file[128];
	static int first = 1, file_index = 0, nmean = 0, nmeant = 0;
	static float force0 = 10, force_dec = 2;
	int force_iter = 8,force_niter = 8;
       	clock_t t0;
	double titer;

	if (first)
	{
	  log_out = fopen("fjca.log","w");
	  if (log_out == NULL)
	    return win_printf_OK("cannot create log file");
	  sprintf(file,"fjcamc.xv");
	  //	  add_plot_treat_menu_item ( "fjca", NULL, fjca_plot_menu(), 0, NULL);
	  first = 0;
	}

	file[5]=0;
	sprintf(file,"%s%03d.xv",file,file_index++);
	i = win_scanf("number of segment %dforce start %f force dividing factor%f"
		      " nb of force iteration %dd_{min}%fproba %fniter %d ntest %"
		      "dfile base name %s",&nm,&force0,&force_dec,&force_niter,&dmin,
		      &proba,&niter,&n_test,file);
	if (i == CANCEL)	return 0;
	fdmin = ftofix(dmin);
	fdmin2 = ftofix(dmin*dmin);
	if (pc == NULL)		pc = initial_configuration(nm+1, dmin);
	if (pc == NULL)		return fprintf(stderr,"cannot allocate memory for pc\n");
	if (zst == NULL)	zst = create_sigma_stat("z(N)");
	else init_sigma_stat(zst);
	if (zst == NULL)	return fprintf(stderr,"cannot allocate memory for zst\n");
	if (zst2 == NULL)	zst2 = create_sigma_stat("z(N/2)");
	else init_sigma_stat(zst2);
	if (zst2 == NULL)	return fprintf(stderr,"cannot allocate memory for zst\n");
	fjca_3D_plot(pc);
	win_printf("bef loop");
	for (force_iter = 0, force = force0; force_iter < force_niter;force_iter++,force /= force_dec)
	{
		file[5]=0;
		sprintf(file,"%s%03d.xv",file,file_index++);
		k = pc->np-1;

		fp = fopen(file,"w");
			if (fp == NULL)	return fprintf(stderr,"cannot open %s\n",file);
		fprintf(fp,"%%iter_{f=%g}\t<x>_{(n=%d)}\t<y>_{(n=%d)}\t<z>_{(n=%d)}\t"
			"l(d_{%g})/l_{N=%d}\t\\tau_{rota}(p=%g)\t\\tau_{rote}\t"
			"\\tau_{crank}\t<x^2>_{(n=%d)}\t<y^2>_{(n=%d)}\t<z(%d/2)>_{(n=%d)}\t"
			"<x^2(%d/2)>_{(n=%d)}\t<y^2(%d/2)>_{(n=%d)}\n",force,n_test,n_test,n_test,
			dmin,nm,proba,n_test,n_test,k,n_test,k,n_test,k,n_test);
		lmeant = x2meant = y2meant = 0;
		nmeant = 0;

		t0 = clock();

		for (i = 0,lmean = x2mean = y2mean = 0,nmean = 0; i < niter; i++)
		{
			pc->rottrial = pc->rottriale = pc->rotrefa = 0;
			pc->rotrefe = pc->cranktrial = pc->crankref = 0;
			lmean = x2mean = y2mean = xmean = ymean = 0;
			lmean_2 = x2mean_2 = y2mean_2 = 0;
			nmean = 0;
			lselfavoid = 0;
			nlselfavoid = 0;
			if (i < niter/2)
			{
				lmeant = x2meant = y2meant = 0;
				nmeant = 0;
				init_sigma_stat(zst);
				init_sigma_stat(zst2);
			}
			for (j = 0; j < n_test; j++)
			{
				if (monte_carlo(pc, force, proba, fdmin, fdmin2))
				{
					add_point_to_sigma_stat(zst, (k!=0)?(pc->z[k]/k):pc->z[k]);
					add_point_to_sigma_stat(zst2, (k!=0)?(2*pc->z[k/2]/k):pc->z[k/2]);
				}
				lmean += pc->z[k];
				x2 = pc->x[k];
				y2 = pc->y[k];
				xmean += x2;
				ymean += y2;
				x2 *= x2;
				y2 *= y2;
				x2mean += x2;
				y2mean += y2;
				r2mean += x2+y2;
				lmean_2 += pc->z[k/2];
				x2 = pc->x[k/2];
				y2 = pc->y[k/2];
				x2 *= x2;
				y2 *= y2;
				x2mean_2 += x2;
				y2mean_2 += y2;
				nmean++;
			}
			tau_rota = (pc->rottrial) ? (float)pc->rotrefa/pc->rottrial : 0;
			tau_rote = (pc->rottriale) ? (float)pc->rotrefe/pc->rottriale : 0;
			tau_crank = (pc->cranktrial) ? (float)pc->crankref/pc->cranktrial : 0;

			display_title_message("bef draw");
			call_3D_indirect = 1;
			do_3D_chain_draw();
			call_3D_indirect = 0;
			change_angle_in_3D_plot_region();
/*			display_title_message("rot %d ref %d rota %d ref %d crank %d ref %d",pc->rottrial,pc->rotrefa,pc->rottriale,pc->rotrefe,pc->cranktrial,pc->crankref);
			display_title_message("d_{mean} %g",(nlselfavoid)?lselfavoid/nlselfavoid:lselfavoid);*/

			lmean /= (nmean) ? nmean : 1;
			x2mean /= (nmean) ? nmean : 1;
			y2mean /= (nmean) ? nmean : 1;
			xmean /= (nmean) ? nmean : 1;
			ymean /= (nmean) ? nmean : 1;
			lmean_2 /= (nmean) ? nmean : 1;
			x2mean_2 /= (nmean) ? nmean : 1;
			y2mean_2 /= (nmean) ? nmean : 1;
			x2meant += x2mean;
			y2meant += y2mean;
			lmeant += lmean;
			nmeant++;
/*			win_printf("in loop %d",i); 	*/
			fprintf(fp,"%-4d %-8g %-8g %-8g %-8g %-8g %-8g %-8g %-8g %-8g %-8g "
				"%-8g %-8g\n",i,xmean,ymean,lmean,lmean/k,tau_rota,tau_rote,
				tau_crank,x2mean,y2mean,lmean_2,x2mean_2,y2mean_2);
		}
		fclose(fp);
		t0 = clock() - t0;
		titer = (double)(t0)/(niter*n_test);
		titer = (titer*1000)/CLOCKS_PER_SEC;
		fprintf(log_out,"one iter in %g ms nb of selavoid %g rot %g crank %g\n",
			    titer,(float)(nselfavoid)/(niter*n_test),
			    (float)(npartialrot)/(niter*n_test),
			    (float)(ncrankshaft)/(niter*n_test));
		lmeant /= (nmeant) ? nmeant : 1;
		x2meant /= (nmeant) ? nmeant : 1;
		y2meant /= (nmeant) ? nmeant : 1;
		fprintf(log_out,"f = %g l = %g l/x^2 = %g l/y^2 %g\n",force,lmeant/k,
			    lmeant/x2meant,lmeant/y2meant);

	}
	spit_sigma_stat(zst);
	spit_sigma_stat(zst2);
	spit_length_of_each_segment(pc);
	fjca_plot(zst, pc);
	fjca_plot(zst2, pc);

	return 0;
}


int	fjca_main2(int argc, char **argv)
{
	register int i, j, k;
	float tau_rota, tau_rote, tau_crank, lmean, x2mean, y2mean, r2mean, x2, y2;
	float xmean, ymean, lmeant, x2meant, y2meant;
	FILE *fp, *log_out = NULL;
	static char file[128];
	static int first = 1, file_index = 0, nmean = 0, nmeant = 0;
	static float force0 = 10, force_dec = 2;
	int force_iter = 8,force_niter = 8;
       	clock_t t0;
	double titer;

	(void)argc;
	(void)argv;
	if (first)
	{
	  log_out = fopen("fjca.log","w");
	  if (log_out == NULL)
	    return win_printf_OK("cannot create log file");
	  sprintf(file,"fjcamc.xv");
	  first = 0;
	}

	file[5]=0;
	sprintf(file,"%s%03d.xv",file,file_index++);
	i = win_scanf("number of segment %dforce start %f force dividing factor%f"
		      " nb of force iteration %dd_{min}%fproba %fniter %d"
		      " ntest %dfile base name %s",&nm,&force0,&force_dec,&force_niter,
		      &dmin,&proba,&niter,&n_test,file);
	if (i == CANCEL)	return 0;

	if (pc == NULL)
		pc = initial_configuration(nm+1, dmin);
	if (pc == NULL)
		return win_printf_OK("cannot allocate memory for pc\n");

	if (zst == NULL)	zst = create_sigma_stat("FJCA");
	else init_sigma_stat(zst);
	if (zst == NULL)
		return win_printf_OK("cannot allocate memory for zst\n");

	for (force_iter = 0, force = force0; force_iter < force_niter;force_iter++,force /= force_dec)
	{
		file[5]=0;
		sprintf(file,"%s%03d.xv",file,file_index++);

		fp = fopen(file,"w");
			if (fp == NULL)	return fprintf(stderr,"cannot open %s\n",file);
		fprintf(fp,"%%iter_{f=%g}\t<x>_{(n=%d)}\t<y>_{(n=%d)}\t<z>_{(n=%d)}"
			"\tl(d_{%g})/l_{N=%d}\t\\tau_{rota}(p=%g)\t\\tau_{rote}\t"
			"\\tau_{crank}\t<x^2>_{(n=%d)}\t<y^2>_{(n=%d)}\n",force,n_test,n_test,
			n_test,dmin,nm,proba,n_test,n_test);
		lmeant = x2meant = y2meant = 0;
		nmeant = 0;
		k = pc->np-1;

		t0 = clock();
		for (i = 0,lmean = x2mean = y2mean = 0,nmean = 0; i < niter; i++)
		{
			pc->rottrial = pc->rottriale = pc->rotrefa = 0;
			pc->rotrefe = pc->cranktrial = pc->crankref = 0;
			lmean = x2mean = y2mean = xmean = ymean = 0;
			nmean = 0;
			lselfavoid = 0;
			nlselfavoid = 0;
			if (i < niter/2)
			{
				lmeant = x2meant = y2meant = 0;
				nmeant = 0;
				init_sigma_stat(zst);
			}
			for (j = 0; j < n_test; j++)
			{
				if (monte_carlo(pc, force, proba, fdmin, fdmin2))
					add_point_to_sigma_stat(zst, (k!=0)?(pc->z[k]/k):pc->z[k]);
				lmean += pc->z[k];
				x2 = pc->x[k];
				y2 = pc->y[k];
				xmean += x2;
				ymean += y2;
				x2 *= x2;
				y2 *= y2;
				x2mean += x2;
				y2mean += y2;
				r2mean += x2+y2;
				nmean++;
			}
			tau_rota = (pc->rottrial) ? (float)pc->rotrefa/pc->rottrial : 0;
			tau_rote = (pc->rottriale) ? (float)pc->rotrefe/pc->rottriale : 0;
			tau_crank = (pc->cranktrial) ? (float)pc->crankref/pc->cranktrial : 0;
/*			display_title_message("rot %d ref %d rota %d ref %d crank %d ref %d",pc->rottrial,pc->rotrefa,pc->rottriale,pc->rotrefe,pc->cranktrial,pc->crankref);
			display_title_message("d_{mean} %g",(nlselfavoid)?lselfavoid/nlselfavoid:lselfavoid);*/
			lmean /= (nmean) ? nmean : 1;
			x2mean /= (nmean) ? nmean : 1;
			y2mean /= (nmean) ? nmean : 1;
			xmean /= (nmean) ? nmean : 1;
			ymean /= (nmean) ? nmean : 1;
			x2meant += x2mean;
			y2meant += y2mean;
			lmeant += lmean;
			nmeant++;
/*			win_printf("in loop %d",i); 	*/
			fprintf(fp,"%-4d %-8g %-8g %-8g %-8g %-8g %-8g %-8g %-8g %-8g\n",i,xmean,
				ymean,lmean,lmean/k,tau_rota,tau_rote,tau_crank,x2mean,y2mean);
		}
		fclose(fp);
		t0 = clock() - t0;
		titer = (double)(t0)/(niter*n_test);
		titer = (titer*1000)/CLOCKS_PER_SEC;

		fprintf(log_out,"one iter in %g ms nb of selavoid %g rot %g crank %g\n",titer,
			    (float)(nselfavoid)/(niter*n_test),(float)(npartialrot)/(niter*n_test),
			    (float)(ncrankshaft)/(niter*n_test));
		lmeant /= (nmeant) ? nmeant : 1;
		x2meant /= (nmeant) ? nmeant : 1;
		y2meant /= (nmeant) ? nmeant : 1;
		fprintf(log_out,"f = %g l = %g l/x^2 = %g l/y^2 %g\n",force,lmeant/k,
			    lmeant/x2meant,lmeant/y2meant);

	}

	spit_sigma_stat(zst);
	spit_length_of_each_segment(pc);
	fjca_plot(zst, pc);
	fclose(log_out);
	return 0;
}


int	convert_3D_pc_to_ds(p_c *pcl,d_s *ds,d_s *dsp,d_s *dspn,float thetal, float phil)
{
	register int i;
	static float xc[16], yc[16], zc[16];
	static int  npp = 0;// first = 1,
	float sit, cot, sip, cop;

	sit = sin(thetal);
	cot = cos(thetal);
	sip = sin(phil);
	cop = cos(phil);
	if (npp != pcl->np)
	{
		i = 0;
		xc[i] = -pcl->np/4; yc[i] = -pcl->np/4; zc[i++] = 0;
		xc[i] = -pcl->np/4; yc[i] = -pcl->np/4; zc[i++] = pcl->np;
		xc[i] = pcl->np/4; yc[i] = -pcl->np/4; zc[i++] = pcl->np;
		xc[i] = pcl->np/4; yc[i] = -pcl->np/4; zc[i++] = 0;
		xc[i] = -pcl->np/4; yc[i] = -pcl->np/4; zc[i++] = 0;
		xc[i] = -pcl->np/4; yc[i] = pcl->np/4; zc[i++] = 0;
		xc[i] = -pcl->np/4; yc[i] = pcl->np/4; zc[i++] = pcl->np;
		xc[i] = pcl->np/4; yc[i] = pcl->np/4; zc[i++] = pcl->np;
		xc[i] = pcl->np/4; yc[i] = pcl->np/4; zc[i++] = 0;
		xc[i] = -pcl->np/4; yc[i] = pcl->np/4; zc[i++] = 0;
		xc[i] = -pcl->np/4; yc[i] = pcl->np/4; zc[i++] = pcl->np;
		xc[i] = -pcl->np/4; yc[i] = -pcl->np/4; zc[i++] = pcl->np;
		xc[i] = pcl->np/4; yc[i] = -pcl->np/4; zc[i++] = pcl->np;
		xc[i] = pcl->np/4; yc[i] = pcl->np/4; zc[i++] = pcl->np;
		xc[i] = pcl->np/4; yc[i] = pcl->np/4; zc[i++] = 0;
		xc[i] = pcl->np/4; yc[i] = -pcl->np/4; zc[i++] = 0;
		npp = pcl->np;
		dsp = build_adjust_data_set(dsp, pcl->np, pcl->np);
		dspn = build_adjust_data_set(dspn, pcl->np, pcl->np);
		dsp->nx = dsp->ny = dspn->nx = dspn->ny = npp;
	}
	for (i = 0 ;ds != NULL && i < 16 && i < ds->nx; i++)
	{
		ds->xd[i] = -xc[i] * sit + yc[i] * cot;
		ds->yd[i] = -xc[i] * cot * sip - yc[i] * sit * sip + zc[i] * cop;
	}
	for (i = 0 ;dsp != NULL && i < pcl->np && i < dsp->nx; i++)
	{
		dsp->xd[i] = -pcl->x[i] * sit + pcl->y[i] * cot;
		dsp->yd[i] = -pcl->x[i] * cot * sip - pcl->y[i] * sit * sip + pcl->z[i] * cop;
	}
	for (i = 0 ;dspn != NULL && i < pcl->np && i < dspn->nx; i++)
	{
		dspn->xd[i] = -pcl->xn[i] * sit + pcl->yn[i] * cot;
		dspn->yd[i] = -pcl->xn[i] * cot * sip - pcl->yn[i] * sit * sip + pcl->zn[i] * cop;
	}
	return 0;
}
int	do_3D_chain_draw(void)
{
	register int i;
	pltreg *pr;
	O_p *op;
	d_s *ds, *dsp, *dspn;
	int n_op;

	if(updating_menu_state != 0)	return D_O_K;
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
		return win_printf("cannot find data");

	n_op = find_op_nb_of_source_specific_ds_in_pr(pr, "box chain display");
	ds = find_source_specific_ds_in_pr(pr, "box chain display");
	//if (ds == NULL) return win_printf("cannot find ds for box");
	dsp = find_source_specific_ds_in_pr(pr, "true chain display");
	//if (dsp == NULL) return win_printf("cannot find ds for chain");
	dspn = find_source_specific_ds_in_pr(pr, "new chain display");
	//if (dspn == NULL) return win_printf("cannot find ds for new chain");

	if (call_3D_indirect == 0)
	{
	  i = win_scanf("\\theta %f \\phi %f",&theta,&phi);
	  if (i == CANCEL)	return OFF;
	}
	convert_3D_pc_to_ds(pc,ds,dsp,dspn,theta,phi);
	pr->one_p->need_to_refresh = 1;
	return refresh_plot(pr,n_op);
}
int	do_crank(void)
{
	static int pt0 = 10, pt1 = 50;

	if(updating_menu_state != 0)	return D_O_K;
	crankshaft(pc, &pt0, &pt1);
	call_3D_indirect = 1;
	do_3D_chain_draw();
	call_3D_indirect = 0;
	display_title_message("imoved_0 %d imoved_1 %d",pt0,pt1);
	return 0;
}
int	compute_wirthe(void)
{
	if(updating_menu_state != 0)	return D_O_K;

	win_scanf("volo min %f",&volo_min);

	win_printf("wirthe %g\n on new config %g\ngilles %g ",
		   do_compute_wirthe(pc),do_compute_wirthe_n(pc),
		   do_compute_wirthe_gilles(pc));

	return 0;
}
int	change_force(void)
{
    register int i;
    float force1 = force;
    if(updating_menu_state != 0)	return D_O_K;

    i = win_scanf("new force %f",&force1);
    if (i == CANCEL) return OFF;
    force = force1;
    return 0;
}


int	change_bending(void)
{
    register int i;
    float b;
    if(updating_menu_state != 0)	return D_O_K;

    b = pc->B;
    i = win_scanf("new bending energy %f",&b);
    if (i == CANCEL) return OFF;
    pc->B = b;
    compute_bending_energy_new_ch(pc);
    compute_bending_energy_cur_ch(pc);
    return 0;
}

int	show_trial(void)
{
  pltreg *pr;
  O_p *op;

  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
    return win_printf("cannot find data");
  if (op->dat[2] != NULL)    op->n_dat = 3;
  return 0;
}


int	do_rot(void)
{
	register int i;

	if(updating_menu_state != 0)	return D_O_K;

	i = partialrot(pc);
	call_3D_indirect = 1;
	do_3D_chain_draw();
	call_3D_indirect = 0;
	display_title_message("imoved %d",i);
	return 0;
}
int	do_random(void)
{
	register int i;
	static int nx = 1024;

	if(updating_menu_state != 0)	return D_O_K;

	i = win_scanf("number of trial %d",&nx);
	if (i == CANCEL) return OFF;

	for (i = 0 ; i < nx; i++)
	{
		display_title_message("TRial %d",i);
		random_configuration(pc,  dmin);
		fprintf(log_out,"conf %d l %g\n",i,pc->z[pc->np-1]);
	}
	return OFF;
}


int	do_plasmid_helical_config(void)
{
	register int i;
	static int nx = 20, size = 60,  ndz = 10, np;

	if(updating_menu_state != 0)	return D_O_K;
	if (pc == NULL)
	  {
	    return win_printf_OK("chain is not yet define use New!");
	  }
	np = pc->np;
	i = win_scanf("This routine build a plasmid and helical mol\n"
		      "Define the size of the chain %d"
		      "chose ndz %dnumber of step for 1 turn %dsize of helix %d",
		      &np,&ndz,&nx,&size);
	if (i == CANCEL) return OFF;
	if (np != pc->np)
	  {
	    if (realloc_chain_arrays(pc, np) != 0)
	      return win_printf_OK("Cannot alocate chain memory!");

	  }

	//win_printf("phi1 %g phi2 %g atan2(1,0) %g",phi1,phi2,atan2(1,0));

	plasmid_helical_configuration(pc, dmin, ndz, nx, size);

	call_3D_indirect = 1;
	do_3D_chain_draw();
	call_3D_indirect = 0;
	change_angle_in_3D_plot_region();

	return OFF;
}

int	do_plasmid_loops_config(void)
{
	register int i;
	static int nx = 20, size = 60,  ndz = 10, np, np1;

	if(updating_menu_state != 0)	return D_O_K;
	if (pc == NULL)
	  {
	    return win_printf_OK("chain is not yet define use New!");
	  }
	np = pc->np;
	i = win_scanf("This routine builds a plasmid loops mol\n"
		      "the molecule is a half circle + a series of loops\n"
		      "having a helical shape advancing by ndz followed by\n"
		      "a quarter of a circle, a horizontal straight segment\n"
		      "extending over -ndz and close by a final quarter of circle\n"
		      "(np - size - ndz) should be a multiple of 4\n"
		      "Define the size of the chain %d"
		      "chose ndz %dnumber of step for 1 turn %dsize of helix %d",
		      &np,&ndz,&nx,&size);
	if (i == CANCEL) return OFF;
	for (np1 = np ; ((np1 - size - ndz)%4); np1++);
	if (np1 != np)
	  i = win_printf("The total extend of your plasmid does not satisfy\n"
			"(np - size - ndz) = %d is not a multiple of 4!\n"
			"I propose to use np = %d is that fine ?",np,np1);
	if (i == CANCEL) return OFF;
	np = np1;

	if (np != pc->np)
	  {
	    if (realloc_chain_arrays(pc, np) != 0)
	      return win_printf_OK("Cannot alocate chain memory!");

	  }

	//win_printf("phi1 %g phi2 %g atan2(1,0) %g",phi1,phi2,atan2(1,0));

	plasmid_loops_configuration(pc, dmin, ndz, nx, size);

	call_3D_indirect = 1;
	do_3D_chain_draw();
	call_3D_indirect = 0;
	change_angle_in_3D_plot_region();

	return OFF;
}



int	do_helical_config(void)
{
	register int i;
	static int nx = 20, np;
	static float dz = 0.06, phi1, phi2;

	if(updating_menu_state != 0)	return D_O_K;
	if (pc == NULL)
	  {
	    return win_printf_OK("chain is not yet define use New!");
	  }
	np = pc->np;
	i = win_scanf("Define the size of the chain %dchose dz %f"
		      "number of step for 1 turn %d",&np,&dz,&nx);
	if (i == CANCEL) return OFF;
	if (np != pc->np)
	  {
	    if (realloc_chain_arrays(pc, np) != 0)
	      return win_printf_OK("Cannot alocate chain memory!");

	  }

	phi1 = acos(dz);
	phi2 = 2 * M_PI/nx;
	//win_printf("phi1 %g phi2 %g atan2(1,0) %g",phi1,phi2,atan2(1,0));
	helical_configuration(pc, dmin, phi1, phi2);
	call_3D_indirect = 1;
	do_3D_chain_draw();
	call_3D_indirect = 0;
	change_angle_in_3D_plot_region();

	return OFF;
}


int	do_straight_loops_config(void)
{
	register int i;
	static int nx = 20, size = 60, np;
	static float dz = 0.06, phi1, phi2;

	if(updating_menu_state != 0)	return D_O_K;
	if (pc == NULL)
	  {
	    return win_printf_OK("chain is not yet define use New!");
	  }
	np = pc->np;
	i = win_scanf("Define the size of the chain %dchoose dy %f"
		      "number of step for 1 turn %dsize of helix %d",&np,&dz,&nx,&size);
	if (i == CANCEL) return OFF;
	if (np != pc->np)
	  {
	    if (realloc_chain_arrays(pc, np) != 0)
	      return win_printf_OK("Cannot alocate chain memory!");

	  }
	phi1 = acos(dz);
	phi2 = 2 * M_PI/nx;
	//win_printf("phi1 %g phi2 %g atan2(1,0) %g",phi1,phi2,atan2(1,0));
	straight_loops_configuration(pc, dmin, phi1, phi2, size);
	call_3D_indirect = 1;
	do_3D_chain_draw();
	call_3D_indirect = 0;
	change_angle_in_3D_plot_region();

	return OFF;
}


int	do_straight_helical_config(void)
{
	register int i;
	static int nx = 20, size = 60, np;
	static float dz = 0.06, phi1, phi2;

	if(updating_menu_state != 0)	return D_O_K;
	if (pc == NULL)
	  {
	    return win_printf_OK("chain is not yet define use New!");
	  }
	np = pc->np;
	i = win_scanf("Define the size of the chain %dchoose dz %f"
		      "number of step for 1 turn %dsize of helix %d",&np,&dz,&nx,&size);
	if (i == CANCEL) return OFF;
	if (np != pc->np)
	  {
	    if (realloc_chain_arrays(pc, np) != 0)
	      return win_printf_OK("Cannot alocate chain memory!");

	  }
	phi1 = acos(dz);
	phi2 = 2 * M_PI/nx;
	//win_printf("phi1 %g phi2 %g atan2(1,0) %g",phi1,phi2,atan2(1,0));
	straight_helical_configuration(pc, dmin, phi1, phi2, size);
	call_3D_indirect = 1;
	do_3D_chain_draw();
	call_3D_indirect = 0;
	change_angle_in_3D_plot_region();

	return OFF;
}


int	do_curve_helical_config(void)
{
	register int i;
	static int nx = 20, size = 60, np, nc = 10;
	static float dz = 0.06, phi1, phi2;

	if(updating_menu_state != 0)	return D_O_K;
	if (pc == NULL)
	  {
	    return win_printf_OK("chain is not yet define use New!");
	  }
	np = pc->np;
	i = win_scanf("Define the size of the chain %dchoose dz %f"
		      "number of step for 1 turn %dsize of helix %d"
		      "size of curved transition %d",&np,&dz,&nx,&size,&nc);
	if (i == CANCEL) return OFF;
	if (np != pc->np)
	  {
	    if (realloc_chain_arrays(pc, np) != 0)
	      return win_printf_OK("Cannot alocate chain memory!");

	  }
	phi1 = acos(dz);
	phi2 = 2 * M_PI/nx;
	//win_printf("phi1 %g phi2 %g atan2(1,0) %g",phi1,phi2,atan2(1,0));
	curved_helical_configuration(pc, dmin, phi1, phi2, size, nc);
	call_3D_indirect = 1;
	do_3D_chain_draw();
	call_3D_indirect = 0;
	change_angle_in_3D_plot_region();

	return OFF;
}


int change_angle_in_3D_plot_region(void)
{
	int x_m, y_m, dx, dy, dxp = 0, dyp = 0;
	pltreg *pr;
	O_p *op;
	d_s *ds, *dsp, *dspn;
	int n_op;
	//clock_t start = 0;
	float theta_0 = theta, phi_0 = phi;

	if(updating_menu_state != 0)	return D_O_K;
	if ((pr = find_pr_in_current_dialog(NULL)) == NULL)
		return win_printf("cannot find pr");

	op = pr->one_p;
	x_m = mouse_x;
	y_m = mouse_y;


	n_op = find_op_nb_of_source_specific_ds_in_pr(pr, "box chain display");
	ds = find_source_specific_ds_in_pr(pr, "box chain display");
	//if (ds == NULL) return win_printf("cannot find ds for box");
	dsp = find_source_specific_ds_in_pr(pr, "true chain display");
	//if (dsp == NULL) return win_printf("cannot find ds for chain");
	dspn = find_source_specific_ds_in_pr(pr, "new chain display");
	//if (dspn == NULL) return win_printf("cannot find ds for new chain");

	display_title_message("x_m %d ym %d",x_m,y_m);

	//start = clock();


	//	while ((mouse_b & 0x3) || ((5*(clock() - start)) < CLOCKS_PER_SEC ))
	while (mouse_b & 0x3)
	{
	    dx = mouse_x - x_m;
	    dy = mouse_y - y_m;
	    if (dx != dxp || dy != dyp)
	    {
		dxp = dx;
		dyp = dy;
		theta = theta_0 + (float)(3*dx)/menu_x0;
		phi = phi_0 + (float)(3*dy)/max_y;
		convert_3D_pc_to_ds(pc,ds,dsp,dspn,theta,phi);
		pr->one_p->need_to_refresh = 1;
		refresh_plot(pr,n_op);
		display_title_message("\\theta %g \\phi %g",theta,phi);
	    }
	    if (key[KEY_PGDN])
	    {
	        op->y_lo *= M_SQRT2;
		op->y_hi *= M_SQRT2;
		op->x_lo *= M_SQRT2;
		op->x_hi *= M_SQRT2;
		convert_3D_pc_to_ds(pc,ds,dsp,dspn,theta,phi);
		pr->one_p->need_to_refresh = 1;
		refresh_plot(pr,n_op);
	    }
	    if (key[KEY_PGUP])
	    {
		op->y_lo *= M_SQRT1_2;
		op->y_hi *= M_SQRT1_2;
		op->x_lo *= M_SQRT1_2;
		op->x_hi *= M_SQRT1_2;
		convert_3D_pc_to_ds(pc,ds,dsp,dspn,theta,phi);
		pr->one_p->need_to_refresh = 1;
		refresh_plot(pr,n_op);
	    }
      }
      clear_keybuf();
      return 0;
}

int fjca_plot_idle_action(pltreg *pr, O_p *op, DIALOG *d)
{
  static int n = 0;
  (void)d;
  (void)op;
  (void)pr;
  my_set_window_title("iter %d",n++);
  return do_once();
}

int fjca_op_idle_action(O_p *op, DIALOG *d)
{
  static int n = 0;
  (void)d;
  (void)op;
  my_set_window_title("iter %d",n++);
  return do_once();
}


int fjca_3D_plot(p_c *pcl)
{
	pltreg	*pr;
	O_p	*op;
	d_s *ds, *dsp, *dspn;

	if (pcl == NULL)	return OFF;


	pr = create_and_register_new_plot_project(0,   32,  900,  668);
	if (pr == NULL)
	{
	  win_printf("Could not find or allocte plot region!");
	  return D_O_K;
	}
	op = pr->one_p;
	ds = create_and_attach_one_ds(op,16,16,1);
	if ((dsp = create_and_attach_one_ds(op, pcl->np, pcl->np, 0)) == NULL)
		return win_printf_OK("cannot create plot");
	if ((dspn = create_and_attach_one_ds(op, pcl->np, pcl->np, 0)) == NULL)
		return win_printf_OK("cannot create plot");

	convert_3D_pc_to_ds(pc,ds,dsp,dspn,theta,phi);
	ds->source = my_sprintf(ds->source,"box chain display");
	dsp->source = my_sprintf(dsp->source,"true chain display");
	dspn->source = my_sprintf(dspn->source,"new chain display");

	op->y_lo = -pcl->np/6;
	op->y_hi = 1.1*pcl->np;
	op->x_lo = -(3*pcl->np)/4;
	op->x_hi = (3*pcl->np)/4;

	set_plot_x_fixed_range(op);
	set_plot_y_fixed_range(op);
	set_plot_symb(dspn, "\\pt2\\oc");

	op->iopt |= NOAXES;
	//	add_plot_treat_menu_item ( "fjca", NULL, fjca_plot_menu(), 0, NULL);
       	//plot_idle_action = fjca_plot_idle_action;
	op->op_idle_action = fjca_op_idle_action;
	do_one_plot(pr);
	broadcast_dialog_message(MSG_DRAW,0);
	return 0;
}



int fjca_plot(s_t *st, p_c *pcl)
{
	pltreg	*pr;
	O_p	*op;
	d_s *ds, *dserr;
	float mean;

	if (st == NULL)	return OFF;

	(void)pcl;
	pr = create_and_register_new_plot_project(0,   32,  900,  668);
	if (pr == NULL)
	{
	  win_printf("Could not find or allocte plot region!");
	  return D_O_K;
	}
	op = pr->one_p;
	ds = create_and_attach_one_ds(op,32,32,1);
	if ((dserr = create_and_attach_one_ds(op, 64, 64, 0)) == NULL)
		return win_printf_OK("cannot create plot");

	spit_sigma_stat_in_ds(st, ds, dserr, &mean);

	set_plot_title(op, "\\stack{{Corr�lation decay mean %g}{%s}}",mean,st->title);
	set_plot_x_title(op, "Bin size");
	set_plot_y_title(op, "\\sigma");

	do_one_plot(pr);
	broadcast_dialog_message(MSG_DRAW,0);
	return 0;
}
int  p4_is_in_use(void)
{

    if(updating_menu_state != 0)	return D_O_K;

   if ((cpu_capabilities & (CPU_FPU | CPU_MMX)) ==
          (CPU_FPU | CPU_MMX)) {
         win_printf("CPU has both an FPU and MMX instructions!");
      }

   if ((cpu_capabilities & (CPU_FPU | CPU_MMXPLUS)) ==
          (CPU_FPU | CPU_MMXPLUS)) {
         win_printf("CPU has both an FPU and MMXPLUS instructions!");
      }
   if ((cpu_capabilities & (CPU_FPU | CPU_SSE)) ==
          (CPU_FPU | CPU_SSE)) {
         win_printf("CPU has both an FPU and SSE instructions!");
      }
   if ((cpu_capabilities & (CPU_FPU | CPU_SSE2)) ==
          (CPU_FPU | CPU_SSE2)) {
         win_printf("CPU has both an FPU and SSE2 instructions!");
      }
   if ((cpu_capabilities & (CPU_FPU | CPU_3DNOW)) ==
          (CPU_FPU | CPU_3DNOW)) {
         win_printf("CPU has both an FPU and 3DNOW instructions!");
      }
   if ((cpu_capabilities & (CPU_FPU | CPU_ENH3DNOW)) ==
          (CPU_FPU | CPU_ENH3DNOW)) {
         win_printf("CPU has both an FPU and ENH3DNOW instructions!");
      }

   return 0;
}

MENU *fjca_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;

	add_item_to_menu(mn,"New",fjca_main_new,NULL,0,NULL);
	add_item_to_menu(mn,"Go",go_pc,NULL,0,NULL);
	add_item_to_menu(mn,"Stop",stop_pc,NULL,0,NULL);
	add_item_to_menu(mn,"CPU",p4_is_in_use,NULL,0,NULL);
	add_item_to_menu(mn,"partialrot",do_rot,NULL,0,NULL);


	add_item_to_menu(mn,"crackshaft",do_crank,NULL,0,NULL);
	add_item_to_menu(mn,"wirthe",compute_wirthe,NULL,0,NULL);
	add_item_to_menu(mn,"compute sigma mean",do_sigma_stat,NULL,0,NULL);
	add_item_to_menu(mn,"draw 3D",do_3D_chain_draw,NULL,0,NULL);
	add_item_to_menu(mn,"random",do_random,NULL,0,NULL);
	add_item_to_menu(mn,"helical",do_helical_config,NULL,0,NULL);
	add_item_to_menu(mn,"helical 2",do_straight_helical_config,NULL,0,NULL);
	add_item_to_menu(mn,"helical curved",do_curve_helical_config,NULL,0,NULL);
	add_item_to_menu(mn,"loops",do_straight_loops_config,NULL,0,NULL);

	add_item_to_menu(mn,"helical plasmid",do_plasmid_helical_config,NULL,0,NULL);
	add_item_to_menu(mn,"loops plasmid",do_plasmid_loops_config,NULL,0,NULL);


	add_item_to_menu(mn,"Change force",change_force,NULL,0,NULL);
	add_item_to_menu(mn,"Change bending",change_bending,NULL,0,NULL);
	add_item_to_menu(mn,"hide trial",hide_trial,NULL,0,NULL);
	add_item_to_menu(mn,"show trial",show_trial,NULL,0,NULL);

	/*
	add_item_to_menu(mn,"data set rescale in Y", do_fjca_rescale_data_set,NULL,0,NULL);
	add_item_to_menu(mn,"plot rescale in Y", do_fjca_rescale_plot,NULL,0,NULL);
	*/
	return mn;
}



int	fjca_main(int argc, char **argv)
{
  (void)argc;
  (void)argv;
	add_plot_treat_menu_item ( "fjca", NULL, fjca_plot_menu(), 0, NULL);
	return D_O_K;
}



#endif
