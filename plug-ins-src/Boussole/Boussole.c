/*
 *    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
 */
#ifndef _BOUSSOLE_C_
#define _BOUSSOLE_C_

#include <allegro.h>
# include "xvin.h"
# include "fftl32n.h"
# include "fillib.h"
# include "time.h"

/* If you include other regular header do it here*/


/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "Boussole.h"




# define  	IS_BOUSSOLE_DATA    3266



typedef struct _boussole
{
  int 	  niter;	// nb of time iteration
  int     n_asked;      // nb of asked point
  int 	  niter_npc;	// niter%npc (phase in omega)
  double  h;	        // the step
  double  hpi;	        // the step * 2 pi
  double  x;            // the position in x
  double  y;	        // the velocity	in x
  double  v;            // the phse of the rotating field
  double  x_1;          // the position in x of n-1
  double  y_1;	        // the velocity	in x of n-1
  double  v_1;          // the phse of the rotating field of n-1
  double  x0;            // the position in x0
  double  y0;	        // the velocity	in x0
  double  gamma1;	// dissipation factor
  int	  npc;		// nb de point pour faire 1 periode
  double  M;		// Bo  amplitude
  double  P;		// B1  amplitude
  int M_on;             // allow to switch M to 0 or not
  int P_on;             // allow to switch P to 0 or not
  int	  nph;		// phase of poincare [0, npc[
  int     live;         // continuous refresh
    struct timespec mswait;
} boussole;

int 		oscillate = 0;

# define ONCE 		1
# define PERMANENT 	2


boussole 		*find_boussole(O_p *op);
boussole 		*find_boussole_in_ds(d_s *ds);
boussole 		*create_boussole( double x0, double y0, double v0, double M, double P, double gamma1, int npc);

d_s  *poinca_spe_ds = NULL;
d_s  *poinca_signal_ds = NULL;
d_s  *poinca_ds_cur = NULL;

O_p  *poinca_spe_op = NULL;
O_p  *poinca_signal_op = NULL;
O_p  *poinca_op_cur = NULL;


boussole *find_boussole(O_p *op)
{
  if (op == NULL)				return NULL;
  if (op->dat[0]->use.to != IS_BOUSSOLE_DATA)	return NULL;
  return ((boussole*)op->dat[0]->use.stuff);
}
boussole *find_boussole_in_ds(d_s *ds)
{
  if (ds == NULL)				return NULL;
  if (ds->use.to != IS_BOUSSOLE_DATA)	return NULL;
  return ((boussole*)ds->use.stuff);
}


boussole *find_boussole_in_pr(pltreg *pr)
{
  if (pr == NULL)				return NULL;
  if (pr->use.to != IS_BOUSSOLE_DATA)	return NULL;
  return (boussole*)pr->use.stuff;
}

boussole *create_boussole( double x0, double y0, double v0, double M, double P, double gamma1, int npc)
{
  boussole *c;

  if (npc == 0) return NULL;
  c = (boussole *)calloc(1,sizeof(boussole));
  if (c == NULL)		return NULL;
  c->niter = c->niter_npc = 0;
  c->npc = npc;
  c->h = (double)1/c->npc;
  c->hpi = M_PI * 2 * c->h;
  c->x0 = c->x_1 = c->x = x0;
  c->y0 = c->y_1 = c->y = y0;
  c->v_1 = c->v = v0;
  c->M = 4 * M_PI * M_PI * M;
  c->P = 4 * M_PI * M_PI * P;
  c->M_on = 1;
  c->P_on = 1;
  c->gamma1 = -gamma1;
  c->live = 0;
  c->mswait.tv_sec = 0;
  c->mswait.tv_nsec = 0; 
  return c;
}



int do_simulate(boussole *bo)
{
  double k1x,k2x,k3x,k4x,k1y,k2y,k3y,k4y;
  double k1v, k2v, k3v, k4v;


  if (bo == NULL) return -1;

  bo->h = (double)1/bo->npc;
  bo->hpi = M_PI * 2 * bo->h;
  bo->x_1 = bo->x;
  bo->y_1 = bo->y;
  bo->v_1 = bo->v;
  if (bo->niter_npc >= bo->npc)
    {
      bo->niter_npc = 0;
      bo->v = 0;
    }
  k1v = bo->hpi;
  k1x = bo->h * bo->y;
  k1y = -bo->h * ( bo->M * bo->M_on * sin(bo->x) + bo->P * bo->P_on * sin(bo->x-bo->v) - bo->gamma1 *bo->y);

  k2v = bo->hpi;
  k2x = bo->h * (bo->y+k1y/2);
  k2y = -bo->h * ( bo->M * bo->M_on * sin(bo->x+k1x/2) + bo->P * bo->P_on * sin ((bo->x+k1x/2) - (bo->v+k1v/2))- bo->gamma1 *(bo->y+k1y/2));

  k3v = bo->hpi;
  k3x = bo->h * (bo->y+k2y/2);
  k3y = -bo->h * ( bo->M * bo->M_on * sin(bo->x+k2x/2) + bo->P * bo->P_on * sin ((bo->x+k2x/2) - (bo->v+k2v/2)) - bo->gamma1 *(bo->y+k2y/2));

  k4v = bo->hpi;
  k4x = bo->h * (bo->y+k3y);
  k4y = -bo->h * ( bo->M * bo->M_on * sin(bo->x+k3x) + bo->P * bo->P_on * sin ((bo->x+k3x) - (bo->v+k3v)) - bo->gamma1 *(bo->y+k3y));

  bo->x = bo->x + ((k1x+2*k2x+2*k3x+k4x)/6);
  //while ( bo->x > M_PI )		bo->x -= 2* M_PI;
  //while ( bo->x < - M_PI )		bo->x += 2* M_PI;
  bo->y = bo->y + ((k1y+2*k2y+2*k3y+k4y)/6);
  bo->v = bo->v + ((k1v+2*k2v+2*k3v+k4v)/6);
  bo->niter_npc++;
  bo->niter++;
  if (bo->niter_npc >= bo->npc)
    {
      bo->niter_npc = 0;
      bo->v = 0;
    }
  if (bo->mswait.tv_sec != 0 || bo->mswait.tv_nsec != 0) 
      nanosleep(&bo->mswait,NULL);
  return 0;
}

int sim_traject_ds(boussole *bou, d_s *ds, d_s *dsp, int niter)
{
  register int j;
  float tmp;

  if (bou == NULL || ds == NULL || dsp == NULL)return 1;
  for (j = 0; j < niter; j++)
    {
      do_simulate(bou);
      if (bou->x < M_PI && bou->x > -M_PI)
	{
	  if (bou->v < bou->v_1)
	    {
	      add_new_point_to_ds(ds, bou->x_1, bou->y_1/(2*M_PI));
	      add_new_point_to_ds(ds, bou->x, bou->y/(2*M_PI));
	      add_new_point_to_ds(dsp, bou->x_1, bou->v_1);
	      add_new_point_to_ds(dsp, bou->x, 2*M_PI);
	    }
	  else
	    {
	      add_new_point_to_ds(ds, bou->x_1, bou->y_1/(2*M_PI));
	      add_new_point_to_ds(ds, bou->x, bou->y/(2*M_PI));
	      add_new_point_to_ds(dsp, bou->x_1, bou->v_1);
	      add_new_point_to_ds(dsp, bou->x, bou->v);
	    }
	}
      else if (bou->x > M_PI)
	{
	  add_new_point_to_ds(ds, bou->x_1, bou->y_1/(2*M_PI));
	  add_new_point_to_ds(dsp, bou->x_1, bou->v_1);
	  tmp = (bou->y_1*(bou->x - M_PI) + bou->y*(M_PI - bou->x_1))/(bou->x - bou->x_1);
	  add_new_point_to_ds(ds, M_PI, tmp/(2*M_PI));
	  add_new_point_to_ds(ds, -M_PI, tmp/(2*M_PI));
	  tmp = (bou->v_1*(bou->x - M_PI) + bou->v*(M_PI - bou->x_1))/(bou->x - bou->x_1);
	  add_new_point_to_ds(dsp, M_PI, tmp);
	  add_new_point_to_ds(dsp, -M_PI, tmp);
	  bou->x -= 2* M_PI;
	  add_new_point_to_ds(ds, bou->x, bou->y/(2*M_PI));
	  add_new_point_to_ds(dsp, bou->x, bou->v);
	}
      else if (bou->x < -M_PI)
	{
	  add_new_point_to_ds(ds, bou->x_1, bou->y_1/(2*M_PI));
	  add_new_point_to_ds(dsp, bou->x_1, bou->v_1);
	  tmp = (bou->y_1*(bou->x + M_PI) + bou->y*(M_PI + bou->x_1))/(bou->x - bou->x_1);
	  add_new_point_to_ds(ds, -M_PI, tmp/(2*M_PI));
	  add_new_point_to_ds(ds, M_PI, tmp/(2*M_PI));
	  tmp = (bou->v_1*(bou->x - M_PI) + bou->v*(M_PI - bou->x_1))/(bou->x - bou->x_1);
	  add_new_point_to_ds(dsp, M_PI, tmp);
	  add_new_point_to_ds(dsp, -M_PI, tmp);
	  bou->x += 2* M_PI;
	  add_new_point_to_ds(ds, bou->x, bou->y/(2*M_PI));
	  add_new_point_to_ds(dsp, bou->x, bou->v);
	}
    }
  return 0;
}

int bou_trajectories_op_idle_simul_action(O_p *op, boussole *bou)
{
  int i, niter;
  d_s *ds = NULL, *dsp = NULL;

  if (op == NULL || bou == NULL) return 0;
  if (bou->live)
    {
      niter = op->dat[0]->nx/2;
      for (i = 0; i < op->n_dat; i += 2)
	{
	  ds = op->dat[i];
	  bou->x = ds->user_fspare[3];
	  bou->y = ds->user_fspare[4];
	  bou->v = ds->user_fspare[5];
	  dsp = op->dat[i+1];
	  ds->nx = ds->ny = 0;
	  dsp->nx = dsp->ny = 0;
	  sim_traject_ds(bou, ds, dsp , niter);
	  ds->user_fspare[3] = bou->x;
	  ds->user_fspare[4] = bou->y;
	  ds->user_fspare[5] = bou->v;
	}
      op->need_to_refresh = 1;
      /* refisplay the entire plot */
    }
  return 0;
}

int bou_trajectories_op_idle_action(O_p *op, DIALOG *d)
{
  int  x_m, y_m, dx = 0, dy = 0, niter, ok = 0;//, dxp = 0, dyp = 0;
  pltreg *pr;
  d_s *ds, *dsp;
  boussole *bou = NULL;
  float x, y;

  (void)d;
  //win_printf("bou_trajectories_op_idle_action");
  //if(updating_menu_state != 0)	return D_O_K;
  if ((pr = find_pr_in_current_dialog(NULL)) == NULL)
    return win_printf_OK("cannot find pr");
  bou = find_boussole_in_pr(pr);
  if (bou == NULL)  	return win_printf_OK("cannot find Boussole");
  x_m = mouse_x;
  y_m = mouse_y;
  niter = op->dat[0]->nx/2;
  display_title_message("x_m %d ym %d",x_m,y_m);

  while (mouse_b & 0x3)
    {
      ok = mouse_b & 0x3;
      dx = mouse_x - x_m;
      dy = mouse_y - y_m;
      display_title_message("x0 = %g y0 %g button %d", x_pr_2_pltdata(pr,mouse_x),y_pr_2_pltdata(pr,mouse_y),mouse_b & 3);
    }
  if (ok == 2)
    {
      x = x_pr_2_pltdata(pr,x_m + dx);
      y = y_pr_2_pltdata(pr,y_m + dy);
      if ((x - op->x_lo) < (op->x_hi - op->x_lo)/100) return 0;
      if ((op->x_hi - x) < (op->x_hi - op->x_lo)/100) return 0;
      if ((y - op->y_lo) < (op->y_hi - op->y_lo)/100) return 0;
      if ((op->y_hi - y) < (op->y_hi - op->y_lo)/100) return 0;
      if ((ds = create_and_attach_one_ds(op, niter, niter, 0)) == NULL)
	return win_printf_OK("cannot create plot !");
      ds->nx = ds->ny = 0;


      if ((dsp = create_and_attach_one_ds(op, niter, niter, 0)) == NULL)
	return win_printf_OK("cannot create plot !");
      dsp->nx = dsp->ny = 0;
      dsp->invisible = 1;

      bou->x0 = bou->x = x_pr_2_pltdata(pr,x_m + dx);
      bou->y0 = bou->y = y_pr_2_pltdata(pr,y_m + dy) * 2 *M_PI;

      set_ds_source(ds,"Boussole trajectory (\\theta,d\\theta /dt) with M = %g, P = %g, x0 = %g, v0 = %g"
		    ,(bou->M * bou->M_on)/(4*M_PI*M_PI),(bou->P * bou->P_on)/(4*M_PI*M_PI),bou->x,bou->y/(2*M_PI));
      set_ds_source(dsp,"Boussole trajectory (\\theta, \\phi ) with M = %g, P = %g, x0 = %g, v0 = %g"
		    ,(bou->M * bou->M_on) /(4*M_PI*M_PI),(bou->P * bou->P_on)/(4*M_PI*M_PI),bou->x,bou->y/(2*M_PI));

      set_ds_dash_line(ds);
      ds->user_fspare[0] = bou->x;
      ds->user_fspare[1] = bou->y;
      ds->user_fspare[2] = 0;
      sim_traject_ds(bou, ds, dsp, niter);
      ds->user_fspare[3] = bou->x;
      ds->user_fspare[4] = bou->y;
      ds->user_fspare[5] = bou->v;
      dx = dy = 0;
      op->need_to_refresh = 1;
      /* refisplay the entire plot */
      refresh_plot(pr, UNCHANGED);
    }
  if (ok == 1)
    {
      x = x_pr_2_pltdata(pr,x_m + dx);
      y = y_pr_2_pltdata(pr,y_m + dy);
      if ((x - op->x_lo) < (op->x_hi - op->x_lo)/100) return 0;
      if ((op->x_hi - x) < (op->x_hi - op->x_lo)/100) return 0;
      if ((y - op->y_lo) < (op->y_hi - op->y_lo)/100) return 0;
      if ((op->y_hi - y) < (op->y_hi - op->y_lo)/100) return 0;
      bou->x0 = bou->x = x_pr_2_pltdata(pr,x_m + dx);
      bou->y0 = bou->y = y_pr_2_pltdata(pr,y_m + dy) * 2 *M_PI;
      if (op->n_dat >= 2)
	{
	  ds = op->dat[op->n_dat-2];
	  ds->nx = ds->ny = 0;
	  ds->user_fspare[0] = bou->x;
	  ds->user_fspare[1] = bou->y;
	  ds->user_fspare[2] = 0;
	  dsp = op->dat[op->n_dat-1];
	  dsp->nx = dsp->ny = 0;
	  sim_traject_ds(bou, ds, dsp , niter);
	  ds->user_fspare[3] = bou->x;
	  ds->user_fspare[4] = bou->y;
	  ds->user_fspare[5] = bou->v;
	}
      dx = dy = 0;
      op->need_to_refresh = 1;
      /* refisplay the entire plot */
      refresh_plot(pr, UNCHANGED);
    }
  if (bou->live)
    {
      bou_trajectories_op_idle_simul_action(op, bou);
      /* refisplay the entire plot */
      refresh_plot(pr, UNCHANGED);
    }

  return 0;
}




int do_Boussole_trajectories_plot(void)
{
  int i;
  static int niter = 4096, npc = 32, delay = 0;
  O_p *op = NULL, *opn = NULL;
  d_s *ds = NULL, *dsp = NULL;
  pltreg *pr = NULL;
  boussole *bou = NULL;
  static float m = 0.1, p = 0.1, gam = 0, x0 = 1.1, y0 = 0;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the y coordinate"
			   "of a data set by a number and place it in a new plot");
    }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
    return win_printf_OK("cannot find data");

  bou = find_boussole_in_pr(pr);


  if (bou == NULL)
    {
      i = win_scanf("Create a boussole in this plot region\n"
		    "Define M = %6f, P = %6f\n"
		    "Viscous friction %6f\n"
		    "Number of iter during one turn %6d\n"
		    "X0 %6f V0 %6f Nb. od iter %6d\nDelay %8d (ms)\n"
                    ,&m,&p,&gam,&npc,&x0,&y0,&niter,&delay);
      if (i == WIN_CANCEL) return 0;
      bou = create_boussole( x0, y0 * 2 * M_PI, 0, m, p, gam, npc);
      if (bou == NULL) return win_printf_OK("cannot create boussol");
      bou->n_asked = niter;
      bou->mswait.tv_sec = delay/1000;
      delay = delay%1000;
      bou->mswait.tv_nsec = 1000000*delay;
      pr->use.to = IS_BOUSSOLE_DATA;
      pr->use.stuff = (void*)bou;
      if ((opn = create_and_attach_one_plot(pr, niter, niter, 0)) == NULL)
	return win_printf_OK("cannot create plot !");
      ds = opn->dat[0];
      ds->nx = ds->ny = 0;
      ds->user_fspare[0] = x0;
      ds->user_fspare[1] = y0 * 2 * M_PI;
      ds->user_fspare[2] = 0;
      set_ds_dash_line(ds);
      if ((dsp = create_and_attach_one_ds(opn, niter, niter, 0)) == NULL)
	return win_printf_OK("cannot create plot !");
      dsp->nx = dsp->ny = 0;
      dsp->invisible = 1;
      set_plot_title(opn, "\\partial\\theta^2 /\\partial t^2 = M sin\\theta + P sin(\\theta  -t)");
      set_plot_x_title(opn, "\\theta");
      set_plot_y_title(opn, "\\partial\\theta /\\partial t");
      opn->user_id = IS_BOUSSOLE_DATA;
      opn->op_idle_action = bou_trajectories_op_idle_action;
      //win_printf("setting idle to %p",opn->op_idle_action);

    }
  else
    {
        delay = bou->mswait.tv_nsec/1000000;
        delay += 1000*bou->mswait.tv_sec;
      i = win_scanf("Add a data set to boussole simulation\n"
		    "X0 %6f V0 %6f Nb. od iter %6d\nDelay %8d (ms)\n"
                    ,&x0,&y0,&niter,&delay);
      if (i == WIN_CANCEL) return 0;
      if ((ds = create_and_attach_one_ds(op, niter, niter, 0)) == NULL)
	return win_printf_OK("cannot create plot !");
      ds->nx = ds->ny = 0;
      if ((dsp = create_and_attach_one_ds(op, niter, niter, 0)) == NULL)
	return win_printf_OK("cannot create plot !");
      dsp->nx = dsp->ny = 0;
      dsp->invisible = 1;
      bou->mswait.tv_sec = delay/1000;
      delay = delay%1000;
      bou->mswait.tv_nsec = 1000000*delay;
      bou->x0 = bou->x = x0;
      bou->y0 = bou->y = y0 * 2 * M_PI;
      ds->user_fspare[0] = x0;
      ds->user_fspare[1] = y0 * 2 * M_PI;
      ds->user_fspare[2] = 0;
      bou->niter = niter;
      set_ds_dash_line(ds);
    }
  set_ds_source(ds,"Boussole trajectory (\\theta,d\\theta /dt) with M = %g, P = %g, x0 = %g, v0 = %g"
		,(bou->M * bou->M_on)/(4*M_PI*M_PI),(bou->P * bou->P_on)/(4*M_PI*M_PI),x0,y0);
  set_ds_source(dsp,"Boussole trajectory (\\theta, \\phi ) with M = %g, P = %g, x0 = %g, v0 = %g"
		,(bou->M * bou->M_on)/(4*M_PI*M_PI),(bou->P * bou->P_on)/(4*M_PI*M_PI),x0,y0);


  sim_traject_ds(bou, ds, dsp, niter);
  ds->user_fspare[3] = bou->x;
  ds->user_fspare[4] = bou->y;
  ds->user_fspare[5] = bou->v;
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}


int bou_poincare_signal_op_idle_action(O_p *op, DIALOG *d)
{
  int  j, niter;
  pltreg *pr;
  d_s *ds;
  boussole *bou = NULL;

  (void)d;
  //if(updating_menu_state != 0)	return D_O_K;
  if ((pr = find_pr_in_current_dialog(NULL)) == NULL)
    return win_printf_OK("cannot find pr");
  bou = find_boussole_in_pr(pr);
  if (bou == NULL)  	return win_printf_OK("cannot find Boussole");
  if (poinca_ds_cur == NULL) return 0;
  niter = poinca_ds_cur->nx;
  if (bou->live)
    {
      if (poinca_ds_cur != NULL)
	{
	  ds = poinca_ds_cur;
	  bou->x = ds->user_fspare[3];
	  bou->y = ds->user_fspare[4];
	  bou->v = ds->user_fspare[5];
	  ds->nx = ds->ny = 0;
	  if (poinca_signal_ds != NULL)
	    poinca_signal_ds->nx = poinca_signal_ds->ny = 0;
	  for (j = 0; j < niter; )
	    {
	      do_simulate(bou);
	      if (poinca_signal_ds != NULL)
		add_new_point_to_ds(poinca_signal_ds, poinca_signal_ds->nx, bou->y/(2*M_PI));
	      if (bou->niter_npc != bou->nph) continue;
	      while (bou->x > M_PI)	    bou->x -= 2* M_PI;
	      while (bou->x < -M_PI)	    bou->x += 2* M_PI;
	      add_new_point_to_ds(ds, bou->x, bou->y/(2*M_PI));
	      j++;
	    }
	  ds->user_fspare[3] = bou->x;
	  ds->user_fspare[4] = bou->y;
	  ds->user_fspare[5] = bou->v;
	  if (poinca_signal_ds != NULL)
	    {
	      poinca_signal_ds->user_fspare[3] = bou->x;
	      poinca_signal_ds->user_fspare[4] = bou->y;
	      poinca_signal_ds->user_fspare[5] = bou->v;
	    }
	}
      op->need_to_refresh = 1;
      /* refisplay the entire plot */
      refresh_plot(pr, UNCHANGED);
    }

  return 0;
}
int bou_poincare_spe_op_idle_action(O_p *op, DIALOG *d)
{
  int  j, niter, size;
  pltreg *pr;
  d_s *ds;
  boussole *bou = NULL;

  (void)d;
  //if(updating_menu_state != 0)	return D_O_K;
  if ((pr = find_pr_in_current_dialog(NULL)) == NULL)
    return win_printf_OK("cannot find pr");
  bou = find_boussole_in_pr(pr);
  if (bou == NULL)  	return win_printf_OK("cannot find Boussole");
  if (poinca_ds_cur == NULL) return 0;
  niter = poinca_ds_cur->nx;
  if (bou->live)
    {
      if (poinca_ds_cur != NULL)
	{
	  ds = poinca_ds_cur;
	  bou->x = ds->user_fspare[3];
	  bou->y = ds->user_fspare[4];
	  bou->v = ds->user_fspare[5];
	  ds->nx = ds->ny = 0;
	  if (poinca_signal_ds != NULL)
	    poinca_signal_ds->nx = poinca_signal_ds->ny = 0;
	  for (j = 0; j < niter; )
	    {
	      do_simulate(bou);
	      if (poinca_signal_ds != NULL)
		add_new_point_to_ds(poinca_signal_ds, poinca_signal_ds->nx, bou->y/(2*M_PI));
	      if (bou->niter_npc != bou->nph) continue;
	      while (bou->x > M_PI)	    bou->x -= 2* M_PI;
	      while (bou->x < -M_PI)	    bou->x += 2* M_PI;
	      add_new_point_to_ds(ds, bou->x, bou->y/(2*M_PI));
	      j++;
	    }
	  ds->user_fspare[3] = bou->x;
	  ds->user_fspare[4] = bou->y;
	  ds->user_fspare[5] = bou->v;
	  if (poinca_signal_ds != NULL)
	    {
	      poinca_signal_ds->user_fspare[3] = bou->x;
	      poinca_signal_ds->user_fspare[4] = bou->y;
	      poinca_signal_ds->user_fspare[5] = bou->v;
	    }
	  size = niter * bou->npc;
	  if (fft_init(size)) return 0; //win_printf("Cannot init fft\n%d not a power of 2!",size);
	  for (j = 0; j < size && j < poinca_signal_ds->nx; j++)
	      poinca_spe_ds->xd[j] = poinca_signal_ds->yd[j];
	  fftwindow(size, poinca_spe_ds->xd);
	  realtr1(size, poinca_spe_ds->xd);
	  fft(size, poinca_spe_ds->xd, 1);
	  realtr2(size, poinca_spe_ds->xd, 1);
	  spec_real (size, poinca_spe_ds->xd, poinca_spe_ds->xd);
	  poinca_spe_ds->nx = poinca_spe_ds->ny = 1 + (size/2);

	  for (j = 0; j < poinca_spe_ds->nx; j++)
	    {
	      poinca_spe_ds->yd[j] = poinca_spe_ds->xd[j];
	      poinca_spe_ds->xd[j] = (float)j/niter;
	    }
	}
      op->need_to_refresh = 1;
      /* refisplay the entire plot */
      refresh_plot(pr, UNCHANGED);
    }

  return 0;
}

int bou_poincare_op_idle_action(O_p *op, DIALOG *d)
{
  int x_m, y_m, dx = 0, dy = 0, i, j, niter, ok = 0;//, dxp = 0, <dyp = 0;
  pltreg *pr;
  d_s *ds;
  boussole *bou = NULL;
  float x, y;

  (void)d;
  //if(updating_menu_state != 0)	return D_O_K;
  if ((pr = find_pr_in_current_dialog(NULL)) == NULL)
    return win_printf_OK("cannot find pr");
  bou = find_boussole_in_pr(pr);
  if (bou == NULL)  	return win_printf_OK("cannot find Boussole");
  x_m = mouse_x;
  y_m = mouse_y;
  niter = op->dat[0]->nx;
  display_title_message("x_m %d ym %d",x_m,y_m);

  while (mouse_b & 0x3)
    {
      ok = mouse_b & 0x3;
      dx = mouse_x - x_m;
      dy = mouse_y - y_m;
      display_title_message("x0 = %g y0 %g", x_pr_2_pltdata(pr,mouse_x),y_pr_2_pltdata(pr,mouse_y));
    }
  if (ok == 2)
    {
      x = x_pr_2_pltdata(pr,x_m + dx);
      y = y_pr_2_pltdata(pr,y_m + dy);
      if ((x - op->x_lo) < (op->x_hi - op->x_lo)/100) return 0;
      if ((op->x_hi - x) < (op->x_hi - op->x_lo)/100) return 0;
      if ((y - op->y_lo) < (op->y_hi - op->y_lo)/100) return 0;
      if ((op->y_hi - y) < (op->y_hi - op->y_lo)/100) return 0;

      if ((ds = create_and_attach_one_ds(op, niter, niter, 0)) == NULL)
	return win_printf_OK("cannot create plot !");
      ds->nx = ds->ny = 0;
      bou->x = x_pr_2_pltdata(pr,x_m + dx);
      bou->y = y_pr_2_pltdata(pr,y_m + dy) * 2 *M_PI;
      set_ds_dot_line(ds);
      ds->user_fspare[0] = bou->x;
      ds->user_fspare[1] = bou->y;
      ds->user_fspare[2] = 0;
      set_ds_source(ds,"M = %g, P = %g",(bou->M * bou->M_on)/(4*M_PI*M_PI)
		    ,(bou->P * bou->P_on)/(4*M_PI*M_PI));//bou->M,bou->P);
      if (poinca_signal_ds != NULL)
	poinca_signal_ds->nx = poinca_signal_ds->ny = 0;

      for (j = 0; j < niter; )
	{
	  do_simulate(bou);
	  if (poinca_signal_ds != NULL)
	    add_new_point_to_ds(poinca_signal_ds, poinca_signal_ds->nx, bou->y/(2*M_PI));
	  if (bou->niter_npc != bou->nph) continue;
	  while (bou->x > M_PI)	    bou->x -= 2* M_PI;
	  while (bou->x < -M_PI)	    bou->x += 2* M_PI;
	  add_new_point_to_ds(ds, bou->x, bou->y/(2*M_PI));
	  j++;
	}
      ds->user_fspare[3] = bou->x;
      ds->user_fspare[4] = bou->y;
      ds->user_fspare[5] = bou->v;
      if (poinca_signal_ds != NULL)
	{
	  poinca_signal_ds->user_fspare[3] = bou->x;
	  poinca_signal_ds->user_fspare[4] = bou->y;
	  poinca_signal_ds->user_fspare[5] = bou->v;
	}
      poinca_ds_cur = ds;
      dx = dy = 0;
      op->need_to_refresh = 1;
      /* refisplay the entire plot */
      refresh_plot(pr, UNCHANGED);
    }
  if (ok == 1)
    {
      x = x_pr_2_pltdata(pr,x_m + dx);
      y = y_pr_2_pltdata(pr,y_m + dy);
      if ((x - op->x_lo) < (op->x_hi - op->x_lo)/100) return 0;
      if ((op->x_hi - x) < (op->x_hi - op->x_lo)/100) return 0;
      if ((y - op->y_lo) < (op->y_hi - op->y_lo)/100) return 0;
      if ((op->y_hi - y) < (op->y_hi - op->y_lo)/100) return 0;
      bou->x = x_pr_2_pltdata(pr,x_m + dx);
      bou->y = y_pr_2_pltdata(pr,y_m + dy) * 2 *M_PI;
      if (op->n_dat > 0)
	{
	  ds = op->dat[op->cur_dat];
	  ds->user_fspare[0] = bou->x;
	  ds->user_fspare[1] = bou->y;
	  ds->user_fspare[2] = 0;
	  ds->nx = ds->ny = 0;
	  if (poinca_signal_ds != NULL)
	    poinca_signal_ds->nx = poinca_signal_ds->ny = 0;
	  for (j = 0; j < niter; )
	    {
	      do_simulate(bou);
	      if (poinca_signal_ds != NULL)
		add_new_point_to_ds(poinca_signal_ds, poinca_signal_ds->nx, bou->y/(2*M_PI));
	      if (bou->niter_npc != bou->nph) continue;
	      while (bou->x > M_PI)	    bou->x -= 2* M_PI;
	      while (bou->x < -M_PI)	    bou->x += 2* M_PI;
	      add_new_point_to_ds(ds, bou->x, bou->y/(2*M_PI));
	      j++;
	    }
	  ds->user_fspare[3] = bou->x;
	  ds->user_fspare[4] = bou->y;
	  ds->user_fspare[5] = bou->v;
	  if (poinca_signal_ds != NULL)
	    {
	      poinca_signal_ds->user_fspare[3] = bou->x;
	      poinca_signal_ds->user_fspare[4] = bou->y;
	      poinca_signal_ds->user_fspare[5] = bou->v;
	    }
	  poinca_ds_cur = ds;
	}
      dx = dy = 0;
      op->need_to_refresh = 1;
      /* refisplay the entire plot */
      refresh_plot(pr, UNCHANGED);
    }
  if (bou->live)
    {
      for (i = 0; i < op->n_dat; i++)
	{
	  ds = op->dat[i];
	  bou->x = ds->user_fspare[3];
	  bou->y = ds->user_fspare[4];
	  bou->v = ds->user_fspare[5];
	  ds->nx = ds->ny = 0;
	  if (i == op->cur_dat && poinca_signal_ds != NULL)
	    poinca_signal_ds->nx = poinca_signal_ds->ny = 0;
	  for (j = 0; j < niter; )
	    {
	      do_simulate(bou);
	      if (i == op->cur_dat && poinca_signal_ds != NULL)
		add_new_point_to_ds(poinca_signal_ds, poinca_signal_ds->nx, bou->y/(2*M_PI));
	      if (bou->niter_npc != bou->nph) continue;
	      while (bou->x > M_PI)	    bou->x -= 2* M_PI;
	      while (bou->x < -M_PI)	    bou->x += 2* M_PI;
	      add_new_point_to_ds(ds, bou->x, bou->y/(2*M_PI));
	      j++;
	    }
	  ds->user_fspare[3] = bou->x;
	  ds->user_fspare[4] = bou->y;
	  ds->user_fspare[5] = bou->v;
	  if (i == op->cur_dat && poinca_signal_ds != NULL)
	    {
	      poinca_signal_ds->user_fspare[3] = bou->x;
	      poinca_signal_ds->user_fspare[4] = bou->y;
	      poinca_signal_ds->user_fspare[5] = bou->v;
	    }
	}
      op->need_to_refresh = 1;
      /* refisplay the entire plot */
      refresh_plot(pr, UNCHANGED);
    }

  return 0;
}

int do_Boussole_poicare(void)
{
  int i, j;
  static int niter = 1024, npc = 32, nph = 0;
  O_p *op = NULL, *opn = NULL, *ops = NULL, *opf = NULL;
  d_s *ds = NULL, *dss = NULL, *dsf = NULL;
  pltreg *pr = NULL;
  boussole *bou = NULL;
  static float m = 0.2, p = 0.2, gam = 0, x0 = 0.1, y0 = 0;

  if(updating_menu_state != 0)
    {
      remove_short_cut_from_dialog(0, KEY_N);
      add_keyboard_short_cut(0, KEY_N, 0, do_Boussole_poicare);
      return D_O_K;
    }
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the y coordinate"
			   "of a data set by a number and place it in a new plot");
    }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
    return win_printf_OK("cannot find data");

  bou = find_boussole_in_pr(pr);


  if (bou == NULL)
    {
      i = win_scanf("Create a boussole in this plot region\n"
		    "Define M = %6f, P = %6f\n"
		    "Viscous friction %6f (0.8 nice SA)\n"
		    "Number of iter during one turn %6d\n"
		    "X0 %6f V0 %6f Nb. od iter %6d\n"
		    "Poincare cut index %6d\n"
		    ,&m,&p,&gam,&npc,&x0,&y0,&niter,&nph);
      if (i == WIN_CANCEL) return 0;
      bou = create_boussole( x0, 2 * M_PI * y0, 0, m, p, gam, npc);
      if (bou == NULL) return win_printf_OK("cannot create boussol");
      bou->nph = nph;
      bou->niter = niter;
      bou->n_asked = niter;
      pr->use.to = IS_BOUSSOLE_DATA;
      pr->use.stuff = (void*)bou;


      if ((ops = create_and_attach_one_plot(pr, niter*npc, niter*npc, 0)) == NULL)
	return win_printf_OK("cannot create plot !");
      dss = ops->dat[0];
      dss->nx = dss->ny = 0;
      dss->user_fspare[0] = x0;
      dss->user_fspare[1] = y0 * 2 * M_PI;
      dss->user_fspare[2] = 0;

      set_plot_title(ops, "\\partial\\theta^2 /\\partial t^2 = M sin\\theta + P sin(\\theta  -t)");
      set_plot_x_title(ops, "Time");
      set_plot_y_title(ops, "\\partial\\theta /\\partial t");
      ops->op_idle_action = bou_poincare_signal_op_idle_action;
      ops->user_id = 4098;

      poinca_signal_ds = dss;
      poinca_signal_op = ops;


      if ((opf = create_and_attach_one_plot(pr, niter*npc, niter*npc, 0)) == NULL)
	return win_printf_OK("cannot create plot !");
      dsf = opf->dat[0];
      dsf->nx = dsf->ny = 0;
      dsf->user_fspare[0] = x0;
      dsf->user_fspare[1] = y0 * 2 * M_PI;
      dsf->user_fspare[2] = 0;

      set_plot_title(opf, "\\partial\\theta^2 /\\partial t^2 = M sin\\theta + P sin(\\theta  -t)");
      set_plot_x_title(opf, "Frequency");
      set_plot_y_title(opf, "\\partial\\theta /\\partial t");
      opf->op_idle_action = bou_poincare_spe_op_idle_action;
      opf->user_id = 4099;

      poinca_spe_ds = dsf;
      poinca_spe_op = opf;


      if ((opn = create_and_attach_one_plot(pr, niter, niter, 0)) == NULL)
	return win_printf_OK("cannot create plot !");
      ds = opn->dat[0];
      ds->nx = ds->ny = 0;
      set_ds_dot_line(ds);
      ds->user_fspare[0] = x0;
      ds->user_fspare[1] = y0 * 2 * M_PI;
      ds->user_fspare[2] = 0;

      set_plot_title(opn, "\\partial\\theta^2 /\\partial t^2 = M sin\\theta + P sin(\\theta  -t)");
      set_plot_x_title(opn, "\\theta");
      set_plot_y_title(opn, "\\partial\\theta /\\partial t");
      opn->op_idle_action = bou_poincare_op_idle_action;
      opn->user_id = 4097;

      poinca_ds_cur = ds;
      poinca_op_cur = opn;


    }
  else
    {
      i = win_scanf("Add a data set to boussole simulation\n"
		    "Modify M = %6f, P = %6f\n"
		    "and Viscous friction %6f (0.8 nice SA)\n"
		    "X0 %6f V0 %6f Nb. od iter %6d\n"
		    ,&m,&p,&gam,&x0,&y0,&niter);
      if (i == WIN_CANCEL) return 0;
      if ((ds = create_and_attach_one_ds(op, niter, niter, 0)) == NULL)
	return win_printf_OK("cannot create plot !");
      ds->nx = ds->ny = 0;
      bou->M = 4 * M_PI * M_PI * m;
      bou->P = 4 * M_PI * M_PI * p;
      bou->gamma1 = -gam;
      bou->x0 = bou->x = x0;
      bou->y0 = bou->y = y0 * 2 *M_PI;
      bou->niter = niter;
      set_ds_dot_line(ds);
      ds->user_fspare[0] = x0;
      ds->user_fspare[1] = y0 * 2 * M_PI;
      ds->user_fspare[2] = 0;
    }
  set_ds_source(ds,"M = %g, P = %g",(bou->M * bou->M_on)/(4*M_PI*M_PI),(bou->P * bou->P_on)/(4*M_PI*M_PI));//bou->M,bou->P);

  poinca_ds_cur = ds;
  for (j = 0; j < niter; )
    {
      do_simulate(bou);
      if (poinca_signal_ds != NULL)
	add_new_point_to_ds(poinca_signal_ds, poinca_signal_ds->nx, bou->y/(2*M_PI));
      if (bou->niter_npc != bou->nph) continue;
      while (bou->x > M_PI)	    bou->x -= 2* M_PI;
      while (bou->x < -M_PI)	    bou->x += 2* M_PI;
      add_new_point_to_ds(ds, bou->x, bou->y/(2*M_PI));
      j++;
    }
  ds->user_fspare[3] = bou->x;
  ds->user_fspare[4] = bou->y;
  ds->user_fspare[5] = bou->v;
  if (poinca_signal_ds != NULL)
    {
      poinca_signal_ds->user_fspare[3] = bou->x;
      poinca_signal_ds->user_fspare[4] = bou->y;
      poinca_signal_ds->user_fspare[5] = bou->v;
    }
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}

int do_move_phase(void)
{
  int i, j, niter;//, dxp = 0, dyp = 0;
  pltreg *pr;
  O_p *op;
  d_s *ds;
  boussole *bou = NULL;

  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
    return D_O_K;

  //if (strncmp(op->dat[0]->source,"M = ",4)) return D_O_K;
  if(updating_menu_state != 0)
    {
      if (op != NULL && (op->user_id == 4097 || op->user_id == 4098 || op->user_id == 4099))
	{
	  remove_short_cut_from_dialog(0, KEY_RIGHT);
	  add_keyboard_short_cut(0, KEY_RIGHT, 0, do_move_phase);
	}
      return D_O_K;
    }

  bou = find_boussole_in_pr(pr);
  if (bou == NULL) return D_O_K;
  bou->nph++;
  for (i = 0; i < op->n_dat; i++)
    {
      ds = op->dat[i];
      niter = ds->nx;
      if (bou->nph >= bou->npc) bou->nph = 0;
      bou->x = ds->user_fspare[3];
      bou->y = ds->user_fspare[4];
      bou->v = ds->user_fspare[5];
      ds->nx = ds->ny = 0;
      for (j = 0; j < niter; )
	{
	  do_simulate(bou);
	  if (bou->niter_npc != bou->nph) continue;
	  while (bou->x > M_PI)	    bou->x -= 2* M_PI;
	  while (bou->x < -M_PI)	    bou->x += 2* M_PI;
	  add_new_point_to_ds(ds, bou->x, bou->y/(2*M_PI));
	  j++;
	}
      ds->user_fspare[3] = bou->x;
      ds->user_fspare[4] = bou->y;
      ds->user_fspare[5] = bou->v;
      set_plot_title(op, "\\stack{{\\partial\\theta^2 /\\partial t^2 = M sin\\theta + P sin(\\theta  -t)}"
		     "{\\pt8 M = %g P = %g, n_{ph} = %d/%d}}",(bou->M * bou->M_on)/(4*M_PI*M_PI)
		     ,(bou->P * bou->P_on)/(4*M_PI*M_PI),bou->nph,bou->npc);
    }
  op->need_to_refresh = 1;
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}

int do_inc_S(void)
{
  int i, j, niter;//, dxp = 0, dyp = 0;
  pltreg *pr;
  O_p *op;
  d_s *ds, *dsp;
  boussole *bou = NULL;

  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
    return D_O_K;

  //if (strncmp(op->dat[0]->source,"M = ",4)) return D_O_K;
  if(updating_menu_state != 0)
    {
      if (op != NULL && (op->user_id == 4097 || op->user_id == 4098 || op->user_id == 4100
			 || op->user_id == 4099 || op->user_id == IS_BOUSSOLE_DATA))
	{
	  remove_short_cut_from_dialog(0, KEY_UP);
	  add_keyboard_short_cut(0, KEY_UP, 0, do_inc_S);
	}
      return D_O_K;
    }

  bou = find_boussole_in_pr(pr);
  if (bou == NULL) return D_O_K;

  if (key[KEY_LSHIFT])
      {
          bou->M *= 1.00001;
          bou->P *= 1.00001;
      }
  else
      {
          bou->M *= 1.0001;
          bou->P *= 1.0001;
      }
    if (op->user_id != 4100)
      set_plot_title(op, "\\stack{{\\partial\\theta^ /\\partial t^2 = M sin\\theta + P sin(\\theta  -t)}"
		 "{\\pt8 M = %g P = %g}}",(bou->M * bou->M_on)/(4*M_PI*M_PI),(bou->P * bou->P_on)/(4*M_PI*M_PI));//bou->M,bou->P);


  if (op->user_id == 4097)
    {
      for (i = 0; i < op->n_dat; i++)
	{
	  ds = op->dat[i];
	  bou->x = ds->user_fspare[3];
	  bou->y = ds->user_fspare[4];
	  bou->v = ds->user_fspare[5];
	  niter = ds->nx;
	  ds->nx = ds->ny = 0;
	  for (j = 0; j < niter; )
	    {
	      do_simulate(bou);
	      if (bou->niter_npc != bou->nph) continue;
	      while (bou->x > M_PI)	    bou->x -= 2* M_PI;
	      while (bou->x < -M_PI)	    bou->x += 2* M_PI;
	      add_new_point_to_ds(ds, bou->x, bou->y/(2*M_PI));
	      j++;
	    }
	  ds->user_fspare[3] = bou->x;
	  ds->user_fspare[4] = bou->y;
	  ds->user_fspare[5] = bou->v;
	}
    }
  else if (op->n_dat > 1)
    {
      for (j = 0; j < op->n_dat; j += 2)
	{
	  ds = op->dat[j];
	  dsp = op->dat[j+1];
	  bou->x = ds->user_fspare[3];
	  bou->y = ds->user_fspare[4];
	  bou->v = ds->user_fspare[5];
	  niter = bou->n_asked;
	  ds->nx = ds->ny = 0;
	  dsp->nx = dsp->ny = 0;
	  sim_traject_ds(bou, ds, dsp, niter);
	  ds->user_fspare[3] = bou->x;
	  ds->user_fspare[4] = bou->y;
	  ds->user_fspare[5] = bou->v;
	}
    }
  op->need_to_refresh = 1;
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}



int do_dec_S(void)
{
  int i, j, niter;//, dxp = 0, dyp = 0;
  pltreg *pr;
  O_p *op;
  d_s *ds, *dsp;
  boussole *bou = NULL;

  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
    return D_O_K;

  //if (strncmp(op->dat[0]->source,"M = ",4)) return D_O_K;
  if(updating_menu_state != 0)
    {
      if (op != NULL && (op->user_id == 4097 || op->user_id == 4098 || op->user_id == 4100
			 || op->user_id == 4099 || op->user_id == IS_BOUSSOLE_DATA))
	{
	  remove_short_cut_from_dialog(0, KEY_DOWN);
	  add_keyboard_short_cut(0, KEY_DOWN, 0, do_dec_S);
	}
      return D_O_K;
    }

  bou = find_boussole_in_pr(pr);
  if (bou == NULL) return D_O_K;


  if (key_shifts & __allegro_KB_CTRL_FLAG  || key_shifts & __allegro_KB_ALT_FLAG)
    {
      bou->M *= 0.99999;
      bou->P *= 0.99999;
    }
  else
    {
      bou->M *= 0.9999;
      bou->P *= 0.9999;
    }
    if (op->user_id != 4100)
      set_plot_title(op, "\\stack{{\\partial\\theta^2 /\\partial t^2 = M sin\\theta + P sin(\\theta  -t)}"
		 "{\\pt8 M = %g P = %g}}",(bou->M * bou->M_on)/(4*M_PI*M_PI),(bou->P * bou->P_on)/(4*M_PI*M_PI));//bou->M,bou->P);


  if (op->user_id == 4097)
    {
      for (i = 0; i < op->n_dat; i++)
	{
	  ds = op->dat[i];
	  bou->x = ds->user_fspare[3];
	  bou->y = ds->user_fspare[4];
	  bou->v = ds->user_fspare[5];
	  niter = ds->nx;
	  ds->nx = ds->ny = 0;
	  for (j = 0; j < niter; )
	    {
	      do_simulate(bou);
	      if (bou->niter_npc != bou->nph) continue;
	      while (bou->x > M_PI)	    bou->x -= 2* M_PI;
	      while (bou->x < -M_PI)	    bou->x += 2* M_PI;
	      add_new_point_to_ds(ds, bou->x, bou->y/(2*M_PI));
	      j++;
	    }
	  ds->user_fspare[3] = bou->x;
	  ds->user_fspare[4] = bou->y;
	  ds->user_fspare[5] = bou->v;
	}
    }
  else if (op->n_dat > 1)
    {
      for (j = 0; j < op->n_dat; j += 2)
	{
	  ds = op->dat[j];
	  dsp = op->dat[j+1];
	  bou->x = ds->user_fspare[3];
	  bou->y = ds->user_fspare[4];
	  bou->v = ds->user_fspare[5];
	  niter = bou->n_asked;
	  ds->nx = ds->ny = 0;
	  dsp->nx = dsp->ny = 0;
	  sim_traject_ds(bou, ds, dsp, niter);
	  ds->user_fspare[3] = bou->x;
	  ds->user_fspare[4] = bou->y;
	  ds->user_fspare[5] = bou->v;
	}
    }
  op->need_to_refresh = 1;
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}



int do_big_inc_S(void)
{
  int i, j, niter;//, dxp = 0, dyp = 0;
  pltreg *pr;
  O_p *op;
  d_s *ds, *dsp;
  boussole *bou = NULL;

  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
    return D_O_K;

  //if (strncmp(op->dat[0]->source,"M = ",4)) return D_O_K;
  if(updating_menu_state != 0)
    {
      if (op != NULL && (op->user_id == 4097 || op->user_id == 4100 || op->user_id == 4098
			 || op->user_id == 4099 || op->user_id == IS_BOUSSOLE_DATA))
	{
	  remove_short_cut_from_dialog(0, KEY_PGUP);
	  add_keyboard_short_cut(0, KEY_PGUP, 0, do_big_inc_S);
	}
      return D_O_K;
    }

  bou = find_boussole_in_pr(pr);
  if (bou == NULL) return D_O_K;

  bou->M *= 1.005;
  bou->P *= 1.005;
  if (op->user_id != 4100)
    set_plot_title(op, "\\stack{{\\partial\\theta^2 /\\partial t^2 = M sin\\theta + P sin(\\theta  -t)}"
		 "{\\pt8 M = %g P = %g}}",(bou->M * bou->M_on)/(4*M_PI*M_PI),(bou->P * bou->P_on)/(4*M_PI*M_PI));//bou->M,bou->P);

  if (op->user_id == 4097)
    {
      for (i = 0; i < op->n_dat; i++)
	{
	  ds = op->dat[i];
	  bou->x = ds->user_fspare[3];
	  bou->y = ds->user_fspare[4];
	  bou->v = ds->user_fspare[5];
	  niter = ds->nx;
	  ds->nx = ds->ny = 0;
	  for (j = 0; j < niter; )
	    {
	      do_simulate(bou);
	      if (bou->niter_npc != bou->nph) continue;
	      while (bou->x > M_PI)	    bou->x -= 2* M_PI;
	      while (bou->x < -M_PI)	    bou->x += 2* M_PI;
	      add_new_point_to_ds(ds, bou->x, bou->y/(2*M_PI));
	      j++;
	    }
	  ds->user_fspare[3] = bou->x;
	  ds->user_fspare[4] = bou->y;
	  ds->user_fspare[5] = bou->v;
	}
    }
  else if (op->n_dat > 1)
    {
      for (j = 0; j < op->n_dat; j += 2)
	{
	  ds = op->dat[j];
	  dsp = op->dat[j+1];
	  bou->x = ds->user_fspare[3];
	  bou->y = ds->user_fspare[4];
	  bou->v = ds->user_fspare[5];
	  niter = bou->n_asked;
	  ds->nx = ds->ny = 0;
	  dsp->nx = dsp->ny = 0;
	  sim_traject_ds(bou, ds, dsp, niter);
	  ds->user_fspare[3] = bou->x;
	  ds->user_fspare[4] = bou->y;
	  ds->user_fspare[5] = bou->v;
	}
    }
  op->need_to_refresh = 1;
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}



int do_big_dec_S(void)
{
  int i, j, niter;//, dxp = 0, dyp = 0;
  pltreg *pr;
  O_p *op;
  d_s *ds, *dsp;
  boussole *bou = NULL;

  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
    return D_O_K;

  //if (strncmp(op->dat[0]->source,"M = ",4)) return D_O_K;
  if(updating_menu_state != 0)
    {
      if (op != NULL && (op->user_id == 4097 || op->user_id == 4100 || op->user_id == 4098
			 || op->user_id == 4099 || op->user_id == IS_BOUSSOLE_DATA))
	{
	  remove_short_cut_from_dialog(0, KEY_PGDN);
	  add_keyboard_short_cut(0, KEY_PGDN, 0, do_big_dec_S);
	}
      return D_O_K;
    }

  bou = find_boussole_in_pr(pr);
  if (bou == NULL) return D_O_K;

  bou->M *= 0.995;
  bou->P *= 0.995;
  if (op->user_id != 4100)
    set_plot_title(op, "\\stack{{\\partial\\theta^2 /\\partial t^2 = M sin\\theta + P sin(\\theta  -t)}"
		 "{\\pt8 M = %g P = %g}}",(bou->M * bou->M_on)/(4*M_PI*M_PI),(bou->P * bou->P_on)/(4*M_PI*M_PI));//bou->M,bou->P);

  if (op->user_id == 4097)
    {
      for (i = 0; i < op->n_dat; i++)
	{
	  ds = op->dat[i];
	  bou->x = ds->user_fspare[3];
	  bou->y = ds->user_fspare[4];
	  bou->v = ds->user_fspare[5];
	  niter = ds->nx;
	  ds->nx = ds->ny = 0;
	  for (j = 0; j < niter; )
	    {
	      do_simulate(bou);
	      if (bou->niter_npc != bou->nph) continue;
	      while (bou->x > M_PI)	    bou->x -= 2* M_PI;
	      while (bou->x < -M_PI)	    bou->x += 2* M_PI;
	      add_new_point_to_ds(ds, bou->x, bou->y/(2*M_PI));
	      j++;
	    }
	  ds->user_fspare[3] = bou->x;
	  ds->user_fspare[4] = bou->y;
	  ds->user_fspare[5] = bou->v;
	}
    }
  else if (op->n_dat > 1)
    {
      for (j = 0; j < op->n_dat; j += 2)
	{
	  ds = op->dat[j];
	  dsp = op->dat[j+1];
	  bou->x = ds->user_fspare[3];
	  bou->y = ds->user_fspare[4];
	  bou->v = ds->user_fspare[5];
	  niter = bou->n_asked;
	  ds->nx = ds->ny = 0;
	  dsp->nx = dsp->ny = 0;
	  sim_traject_ds(bou, ds, dsp, niter);
	  ds->user_fspare[3] = bou->x;
	  ds->user_fspare[4] = bou->y;
	  ds->user_fspare[5] = bou->v;
	}
    }

  op->need_to_refresh = 1;
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}

int do_toggle_P(void)
{
  int i, j, niter;//, dxp = 0, dyp = 0;
  pltreg *pr;
  O_p *op;
  d_s *ds, *dsp;
  boussole *bou = NULL;

  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
    return D_O_K;

  //if (strncmp(op->dat[0]->source,"M = ",4)) return D_O_K;
  if(updating_menu_state != 0)
    {
      if (op != NULL && (op->user_id == 4097 || op->user_id == 4100 || op->user_id == 4098
			 || op->user_id == 4099 || op->user_id == IS_BOUSSOLE_DATA))
	{
	  remove_short_cut_from_dialog(0, KEY_P);
	  add_keyboard_short_cut(0, KEY_P, 0, do_toggle_P);
	}
      return D_O_K;
    }

  bou = find_boussole_in_pr(pr);
  if (bou == NULL) return D_O_K;

  bou->P_on = (bou->P_on) ? 0 : 1;
  if (op->user_id != 4100)
    set_plot_title(op, "\\stack{{\\partial\\theta^2 /\\partial t^2 = M sin\\theta + P sin(\\theta  -t)}"
		 "{\\pt8 M = %g P = %g}}",(bou->M * bou->M_on)/(4*M_PI*M_PI),(bou->P * bou->P_on)/(4*M_PI*M_PI));//bou->M,bou->P);

  if (op->user_id == 4097)
    {
      for (i = 0; i < op->n_dat; i++)
	{
	  ds = op->dat[i];
	  bou->x = ds->user_fspare[3];
	  bou->y = ds->user_fspare[4];
	  bou->v = ds->user_fspare[5];
	  niter = ds->nx;
	  ds->nx = ds->ny = 0;
	  for (j = 0; j < niter; )
	    {
	      do_simulate(bou);
	      if (bou->niter_npc != bou->nph) continue;
	      while (bou->x > M_PI)	    bou->x -= 2* M_PI;
	      while (bou->x < -M_PI)	    bou->x += 2* M_PI;
	      add_new_point_to_ds(ds, bou->x, bou->y/(2*M_PI));
	      j++;
	    }
	  ds->user_fspare[3] = bou->x;
	  ds->user_fspare[4] = bou->y;
	  ds->user_fspare[5] = bou->v;
	}
    }
  else if (op->n_dat > 1)
    {
      for (j = 0; j < op->n_dat; j += 2)
	{
	  ds = op->dat[j];
	  dsp = op->dat[j+1];
	  bou->x = ds->user_fspare[3];
	  bou->y = ds->user_fspare[4];
	  bou->v = ds->user_fspare[5];
	  niter = bou->n_asked;
	  ds->nx = ds->ny = 0;
	  dsp->nx = dsp->ny = 0;
	  sim_traject_ds(bou, ds, dsp, niter);
	  ds->user_fspare[3] = bou->x;
	  ds->user_fspare[4] = bou->y;
	  ds->user_fspare[5] = bou->v;
	}
    }

  op->need_to_refresh = 1;
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}



int do_toggle_M(void)
{
  int i, j, niter;//, dxp = 0, dyp = 0;
  pltreg *pr;
  O_p *op;
  d_s *ds, *dsp;
  boussole *bou = NULL;

  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
    return D_O_K;

  //if (strncmp(op->dat[0]->source,"M = ",4)) return D_O_K;
  if(updating_menu_state != 0)
    {
      if (op != NULL && (op->user_id == 4097 ||op->user_id == 4100 || op->user_id == 4098 || op->user_id == 4099 || op->user_id == IS_BOUSSOLE_DATA))
	{
	  remove_short_cut_from_dialog(0, KEY_M);
	  add_keyboard_short_cut(0, KEY_M, 0, do_toggle_M);
	  remove_short_cut_from_dialog(0, KEY_O);
	  add_keyboard_short_cut(0, KEY_O, 0, do_toggle_M);
	}
      return D_O_K;
    }

  bou = find_boussole_in_pr(pr);
  if (bou == NULL) return D_O_K;

  bou->M_on = (bou->M_on) ? 0 : 1;
  if (op->user_id != 4100)
    set_plot_title(op, "\\stack{{\\partial\\theta^2 /\\partial t^2 = M sin\\theta + P sin(\\theta  -t)}"
		 "{\\pt8 M = %g P = %g}}",(bou->M * bou->M_on)/(4*M_PI*M_PI),(bou->P * bou->P_on)/(4*M_PI*M_PI));//bou->M,bou->P);

  if (op->user_id == 4097)
    {
      for (i = 0; i < op->n_dat; i++)
	{
	  ds = op->dat[i];
	  bou->x = ds->user_fspare[3];
	  bou->y = ds->user_fspare[4];
	  bou->v = ds->user_fspare[5];
	  niter = ds->nx;
	  ds->nx = ds->ny = 0;
	  for (j = 0; j < niter; )
	    {
	      do_simulate(bou);
	      if (bou->niter_npc != bou->nph) continue;
	      while (bou->x > M_PI)	    bou->x -= 2* M_PI;
	      while (bou->x < -M_PI)	    bou->x += 2* M_PI;
	      add_new_point_to_ds(ds, bou->x, bou->y/(2*M_PI));
	      j++;
	    }
	  ds->user_fspare[3] = bou->x;
	  ds->user_fspare[4] = bou->y;
	  ds->user_fspare[5] = bou->v;
	}
    }
  else if (op->n_dat > 1)
    {
      for (j = 0; j < op->n_dat; j += 2)
	{
	  ds = op->dat[j];
	  dsp = op->dat[j+1];
	  bou->x = ds->user_fspare[3];
	  bou->y = ds->user_fspare[4];
	  bou->v = ds->user_fspare[5];
	  niter = bou->n_asked;
	  ds->nx = ds->ny = 0;
	  dsp->nx = dsp->ny = 0;
	  sim_traject_ds(bou, ds, dsp, niter);
	  ds->user_fspare[3] = bou->x;
	  ds->user_fspare[4] = bou->y;
	  ds->user_fspare[5] = bou->v;
	}
    }

  op->need_to_refresh = 1;
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}


int set_limits_to_op(void)
{
  pltreg *pr;
  O_p *op;

  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
    return D_O_K;

  if(updating_menu_state != 0)
    {
      remove_short_cut_from_dialog(0, KEY_B);
      add_keyboard_short_cut(0, KEY_B, 0, set_limits_to_op);

      return D_O_K;
    }
  op->x_lo = -3.2;
  op->x_hi = 3.2;
  op->y_lo = -1.5;
  op->y_hi = 2.5;
  set_plot_x_fixed_range(op);
  set_plot_y_fixed_range(op);
  op->need_to_refresh = 1;
  return refresh_plot(pr, UNCHANGED);
}


int	convert_3D_bou_op(O_p *op)
{
  register int i, j, k;
  static float xc[32], yc[32], zc[32];
  float sit, cot, sip, cop;
  float thetal, phil;
  pltreg *pr;
  O_p *opb = NULL;
  d_s *ds = NULL, *dst = NULL, *dsp = NULL;
  boussole *bou = NULL;

  if (op == NULL) return 1;

  if ((pr = find_pr_in_current_dialog(NULL)) == NULL)
    return win_printf("cannot find pr");

  bou = find_boussole_in_pr(pr);

  for (i = 0, j = -1; j < 0 && i < pr->n_op; i++)
    j = (pr->o_p[i]->user_id == IS_BOUSSOLE_DATA) ? i : j;

  j = find_op_nb_of_source_specific_ds_in_pr(pr, "Boussole trajectory (\\theta,d\\theta /dt) with M =");

  if (j < 0) return win_printf("wrong boussole data");

  opb = pr->o_p[j];
  bou_trajectories_op_idle_simul_action(opb, bou);
  //win_printf("plot found %d # ds %d ",j,opb->n_dat);
  //win_printf("plot found %d # ds %d source \n%s",j,opb->n_dat,opb->dat[0]->source);
  thetal = op->user_fspare[0];
  phil = op->user_fspare[1];

  sit = sin(thetal);
  cot = cos(thetal);
  sip = sin(phil);
  cop = cos(phil);

  i = 0;
  xc[i] = 0; yc[i] = 1; zc[i++] = 0;
  xc[i] = 0; yc[i] = 0; zc[i++] = 0;
  xc[i] = -1; yc[i] = 0; zc[i++] = 0;
  xc[i] = -1; yc[i] = 0; zc[i++] = 2;
  xc[i] = 1; yc[i] = 0; zc[i++] = 2;
  xc[i] = 1; yc[i] = 0; zc[i++] = 0;
  xc[i] = -1; yc[i] = 0; zc[i++] = 0;

  xc[i] = -1; yc[i] = 1; zc[i++] = 0;
  xc[i] = -1; yc[i] = 1; zc[i++] = 2;
  xc[i] = 1; yc[i] = 1; zc[i++] = 2;
  xc[i] = 1; yc[i] = 1; zc[i++] = 0;
  xc[i] = -1; yc[i] = 1; zc[i++] = 0;
  xc[i] = -1; yc[i] = 1; zc[i++] = 2;
  xc[i] = -1; yc[i] = 0; zc[i++] = 2;
  xc[i] = 1; yc[i] = 0; zc[i++] = 2;
  //pr->use.to = IS_BOUSSOLE_DATA;
  xc[i] = 1; yc[i] = 1; zc[i++] = 2;
  xc[i] = 1; yc[i] = 1; zc[i++] = 0;
  xc[i] = 1; yc[i] = 0; zc[i++] = 0;

  ds = op->dat[0];
  ds->nx = ds->ny = i;
  for (i = 0 ;ds != NULL && i < 32 && i < ds->mx; i++)
    {
      ds->xd[i] = -xc[i] * sit + (zc[i] - 1) * cot;
      ds->yd[i] = -xc[i] * cot * sip - (zc[i] - 1) * sit * sip + yc[i] * cop;
    }
  //win_printf("Bef op %d opb %d",op->n_dat,opb->n_dat);
  for (i = 0, j = 1; i+1 < opb->n_dat && j < op->n_dat; j++, i += 2)
    {
      ds = op->dat[j];
      dst = opb->dat[i];
      dsp = opb->dat[i+1];
      if (ds == NULL || dst == NULL || dsp == NULL) continue;
      for (k = 0 ;k < ds->nx && k < dst->nx && k < dsp->nx; k++)
	{
	  ds->xd[k] = -(dst->xd[k] * sit)/M_PI + ((dsp->yd[k] -M_PI)* cot)/M_PI;
	  ds->yd[k] = -(dst->xd[k] * cot * sip)/M_PI - ((dsp->yd[k] - M_PI) * sit * sip)/M_PI + dst->yd[k] * cop;
	}
      display_title_message("ds %d k %d",j,k);
      //win_printf("ds %d k %d",j,k);

    }
  op->need_to_refresh = 1;
  return 0;
}


int change_angle_in_3D_plot_region(void)
{
  int x_m, y_m, dx, dy, dxp = 0, dyp = 0, re;
  pltreg *pr;
  O_p *op;
  float theta_0, phi_0;

  if(updating_menu_state != 0)	return D_O_K;
  if ((pr = find_pr_in_current_dialog(NULL)) == NULL)
    return win_printf("cannot find pr");

  op = pr->one_p;
  if (strncmp(op->dat[0]->source,"box axis display",16))
    return win_printf_OK("Cannot find 3D plot");

  theta_0 = op->user_fspare[0];
  phi_0 = op->user_fspare[1];

  x_m = mouse_x;
  y_m = mouse_y;

  display_title_message("x_m %d ym %d",x_m,y_m);
  re = 0;
  while (mouse_b & 0x3)
    {
      re = 1;
      dx = mouse_x - x_m;
      dy = mouse_y - y_m;
      if (dx != dxp || dy != dyp)
	{
	  dxp = dx;
	  dyp = dy;
	  op->user_fspare[0] = theta_0 + (float)(3*dx)/menu_x0;
	  op->user_fspare[1] = phi_0 + (float)(3*dy)/max_y;
	  //display_title_message("\\theta %g \\phi %g",op->user_fspare[0],op->user_fspare[1]);
	  convert_3D_bou_op(op);
	  refresh_plot(pr,UNCHANGED);
	}
      if (key[KEY_PGDN])
	{
	  op->y_lo *= M_SQRT2;
	  op->y_hi *= M_SQRT2;
	  op->x_lo *= M_SQRT2;
	  op->x_hi *= M_SQRT2;
	  convert_3D_bou_op(op);
	  refresh_plot(pr,UNCHANGED);

	}
      if (key[KEY_PGUP])
	{
	  op->y_lo *= M_SQRT1_2;
	  op->y_hi *= M_SQRT1_2;
	  op->x_lo *= M_SQRT1_2;
	  op->x_hi *= M_SQRT1_2;
	  convert_3D_bou_op(op);
	  refresh_plot(pr,UNCHANGED);
	}
    }
  if (re == 0)
    {
      convert_3D_bou_op(op);
      refresh_plot(pr,UNCHANGED);
    }
  clear_keybuf();
  return 0;
}


int	do_3D_bou_draw(void)
{
  pltreg *pr;
  O_p *op;
  d_s *ds;

  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
    return win_printf("cannot find data");

  if (strncmp(op->dat[0]->source,"box axis display",16)) return D_O_K;

  convert_3D_bou_op(op);
  return refresh_plot(pr, UNCHANGED);
}



int bou_3D_op_idle_action(O_p *op, DIALOG *d)
{
  O_p *opb = NULL;
  pltreg *pr = NULL;
  boussole *bou = NULL;
  int i, j;
  (void)d;
  (void)op;


  pr = find_pr_in_current_dialog(NULL);

  for (i = 0, j = -1; pr != NULL && j < 0 && i < pr->n_op; i++)
    j = (pr->o_p[i]->user_id == IS_BOUSSOLE_DATA) ? i : j;

  if (pr != NULL && j >= 0)
    {
      opb = pr->o_p[j];
      bou = find_boussole_in_pr(pr);
      bou_trajectories_op_idle_simul_action(opb, bou);
    }
  change_angle_in_3D_plot_region();
  return 0;
}




int create_3D_plot(void)
{
  pltreg  *pr = NULL;
  int i, max;
  O_p	*op = NULL, *opbox = NULL;
  d_s *dsbox= NULL, *dsp = NULL;


  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
    return win_printf_OK("cannot find data");

  if (op->n_dat < 2) 	return D_O_K;
  if (op->dat[0]->source == NULL || op->dat[1]->source == NULL)  return win_printf_OK("cannot find ds source");
  /*
    if (strncmp(op->dat[0]->source,"Boussole trajectory (\\theta,d\\theta /dt) with M =",50) )
    return win_printf_OK("Wrong ds source\n%s\ninstead of \n"
    "Boussole trajectory (\\theta,d\\theta /dt) with M ="
    ,op->dat[0]->source);
    if (strncmp(op->dat[1]->source,"Boussole trajectory (\\theta, \\phi ) with M =",46))
    return win_printf_OK("Wrong ds source\n%s\ninstead of \n"
    "Boussole trajectory (\\theta, \\phi ) with M ="
    ,op->dat[1]->source);

  */
  if ((opbox = create_and_attach_one_plot(pr, 32, 32, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  dsbox = opbox->dat[0];
  //dsbox->nx = dsbox->ny = 0;
  opbox->user_fspare[0] = opbox->user_fspare[1] = 0;
  set_ds_source(dsbox,"box axis display");
  opbox->user_id = 4100;
  for(i = 0; i < op->n_dat; i += 2)
    {
      /*
	if (strncmp(op->dat[i]->source,"Boussole trajectory (\\theta,d\\theta /dt) with M =",50))
	continue;
	if (strncmp(op->dat[i+1]->source,"Boussole trajectory (\\theta, \\phi ) with M =",50))
	continue;
      */
      max = op->dat[i]->nx;
      max = (op->dat[i+1]->nx > max) ? op->dat[i+1]->nx : max;
      if ((dsp = create_and_attach_one_ds(opbox, max, max, 0)) == NULL)
	return win_printf_OK("cannot create plot");
      set_ds_source(dsp,"3D vue of %s",op->dat[i]->source);
      set_ds_dash_line(dsp);
    }
  convert_3D_bou_op(opbox);
  opbox->y_lo = -0.6;
  opbox->y_hi = 1.6;
  opbox->x_lo = -1.5;
  opbox->x_hi = 1.5;

  set_plot_x_fixed_range(opbox);
  set_plot_y_fixed_range(opbox);

  opbox->iopt |= NOAXES;
  opbox->op_idle_action = bou_3D_op_idle_action;
  do_one_plot(pr);
  broadcast_dialog_message(MSG_DRAW,0);
  return 0;
}



int set_bou_live(void)
{
  pltreg *pr;
  O_p *op;
  boussole *bou = NULL;

  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
    return D_O_K;

  if(updating_menu_state != 0)
    {
      remove_short_cut_from_dialog(0, KEY_L);
      add_keyboard_short_cut(0, KEY_L, 0, set_bou_live);
      return D_O_K;
    }
  bou = find_boussole_in_pr(pr);
  if (bou == NULL) return D_O_K;

  bou->live = (bou->live) ? 0 : 1;
  win_printf("live %d",bou->live);
  return refresh_plot(pr, UNCHANGED);
}


MENU *Boussole_plot_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"Trajectories", do_Boussole_trajectories_plot,NULL,0,NULL);
  add_item_to_menu(mn,"3D ", create_3D_plot,NULL,0,NULL);
  add_item_to_menu(mn, "\0",  NULL,   NULL,   0, NULL);
  add_item_to_menu(mn,"Poincare (N)", do_Boussole_poicare,NULL,0,NULL);
  add_item_to_menu(mn,"Advance phase (riht)",do_move_phase,NULL,0,NULL);
  add_item_to_menu(mn, "\0",  NULL,   NULL,   0, NULL);
  add_item_to_menu(mn,"Small Inc S (up)",do_inc_S,NULL,0,NULL);
  add_item_to_menu(mn,"Small dec (dwn)S",do_dec_S,NULL,0,NULL);
  add_item_to_menu(mn,"Big Inc S (PgUp)",do_big_inc_S,NULL,0,NULL);
  add_item_to_menu(mn,"Big Dec S (PgDwn)",do_big_dec_S,NULL,0,NULL);
  add_item_to_menu(mn,"Set limits (B)",set_limits_to_op,NULL,0,NULL);
  add_item_to_menu(mn,"Set live (L)",set_bou_live,NULL,0,NULL);
  add_item_to_menu(mn,"Toggle M (O,M)",do_toggle_M,NULL,0,NULL);
  add_item_to_menu(mn,"Toggle P (P)",do_toggle_P,NULL,0,NULL);





  return mn;
}

int	Boussole_main(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  add_plot_treat_menu_item ( "Boussole", NULL, Boussole_plot_menu(), 0, NULL);
  return D_O_K;
}

int	Boussole_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  remove_item_to_menu(plot_treat_menu, "Boussole", NULL, NULL);
  return D_O_K;
}
#endif
