/*
*    Plug-in program for image treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _PIV_C_
#define _PIV_C_

/* If you include other regular header do it here*/

# include "stdlib.h"
# include "string.h"
# include "XVin/platform.h"
# include "color.h"
# include "plot_op.h"

# include "allegro.h"
# include "winalleg.h"
# include "xvin.h"
//# include "../trmov/trmov.h"
# include "fftl32n.h"
# include "fillib.h"
# include "float.h"
# include "../fft2d/fft2d.h"


# include "largeint.h"
# include "windows.h"
# include "../nrutil/nrutil.h" 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "piv.h"
//place here other headers of this plugin 


# define 	WRONG_ARGUMENT		-1
# define 	OUT_MEMORY			-2
# define 	FFT_NOT_SUPPORTED	-15
# define	PB_WITH_EXTREMUM	2
# define	TOO_MUCH_NOISE		4
# define	GETTING_REF_PROFILE	1

int deplace(float *x, int nx)
{
	register int i, j;
	float tmp;
	
	for (i = 0, j = nx/2; j < nx ; i++, j++)
	{
		tmp = x[j];
		x[j] = x[i];
		x[i] = tmp;
	}
	return 0;
}


int	correlate_2_1d_sig(float *x1, int nx, float *x2, int filter, int remove_dc, float *corr, int norm)
{
	register int i;
	float moy;
	float m1 = 0, m2 = 0, re1, re2, im1, im2;
	
	if (x1 == NULL)				return WRONG_ARGUMENT;
	if (fft_init(nx) || filter_init(nx))	return FFT_NOT_SUPPORTED;

	/*	compute fft of x1 */
	/*for(i = 0, moy = 0; i < nx/4; i++)	             
	  {
	    moy += x1[i];	
	    moy += x1[nx-1-i];	
	    }
	    for(i = 0, moy = 2*moy/nx; remove_dc && i < nx; i++)   x1[i] = x1[i] - moy;


	//realtr1(nx, x1);
	//fft(nx, x1, 1);
	//realtr2(nx, x1, 1);
	for(i = 0, moy = 0; i < nx/4; i++)	             
	  {
	    moy += x2[i];	
	    moy += x2[nx-1-i];	
	  }
	  for(i = 0, moy = 2*moy/nx; remove_dc && i < nx; i++)   x2[i] = x2[i] - moy;*/
	//realtr1(nx, x2);
	//fft(nx, x2, 1);
	//realtr2(nx, x2, 1);

	/*if (remove_dc)
	  { x1[0] = 0; 
	  x2[0] = 0;}*/
	if (remove_dc > 1) 
	  {
	    x1[2] = x1[3] = 0; // we remove the mode 1 high pass filtering  
	    x2[2] = x2[3] = 0; // we remove the mode 1 high pass filtering  
	  }
	
	/*	compute normalization */
	if (norm)
	  {
	    for (i=0, m1 = m2 = 0; i< nx; i+=2)
	      {
		m1 += x1[i] * x1[i] + x1[i+1] * x1[i+1];
		m2 += x2[i] * x2[i] + x2[i+1] * x2[i+1];
	      }
	    m1 /= 2;	
	    m2 /= 2;	
	    m1 = sqrt(m1*m2);
	  }

	/*	compute correlation in Fourier space */
	for (i=2; i< nx; i+=2)
	{
		re1 = x1[i];		re2 = x2[i];
		im1 = x1[i+1];		im2 = x2[i+1];
		corr[i] 	= (re1 * re2 + im1 * im2);
		corr[i+1] 	= (im1 * re2 - re1 * im2);
	}
	corr[0]	= 2*(x1[0] * x2[0]);	/* these too mode are special */
	corr[1]	= 2*(x1[1] * x2[1]);
	for (i=0; norm && i< nx && m1 != 0; i++) corr[i] /= m1;
	//corr[2] = corr[3] = 0;
	if (filter> 0)		lowpass_smooth_half (nx, corr, filter);
	
	/*	get back to real world */
	realtr2(nx, corr, -1);
	fft(nx, corr, -1);
	realtr1(nx, corr);
	deplace(corr, nx);
	return 0;
}	



/*	Find maximum position in a 1d array, the array is supposed periodic
 *	return 0 -> everything fine, TOO_MUCH_NOISE or PB_WITH_EXTREMUM
 *
 */
int	find_max1(float *x, int nx, float *Max_pos, float *Max_val)
{
	register int i;
	int  nmax, ret = 0, delta;
	double a, b, c, d, f_2, f_1, f0, f1, f2, xm = 0;

	for (i = 0, nmax = 0; i < nx; i++) nmax = (x[i] > x[nmax]) ? i : nmax;
	//win_printf("max at %d val %g",nmax,x[nmax]);
	f_2 = x[(nx + nmax - 2) % nx];
	f_1 = x[(nx + nmax - 1) % nx];
	f0 = x[nmax];
	f1 = x[(nmax + 1) % nx];
	f2 = x[(nmax + 2) % nx];
	if (f_1 < f1)
	{
		a = -f_1/6 + f0/2 - f1/2 + f2/6;
		b = f_1/2 - f0 + f1/2;
		c = -f_1/3 - f0/2 + f1 - f2/6;
		d = f0;
		delta = 0;
	}	
	else
	{
		a = b = c = 0;	
		a = -f_2/6 + f_1/2 - f0/2 + f1/6;
		b = f_2/2 - f_1 + f0/2;
		c = -f_2/3 - f_1/2 + f0 - f1/6;
		d = f_1;
		delta = -1;
	}
	if (fabs(a) < 1e-8)
	{
		if (b != 0)	xm =  - c/(2*b) - (3 * a * c * c)/(4 * b * b * b);
		else
		{
			xm = 0;
			ret |= PB_WITH_EXTREMUM;
		}
	}
	else if ((b*b - 3*a*c) < 0)
	{
		ret |= PB_WITH_EXTREMUM;
		xm = 0;
	}
	else
		xm = (-b - sqrt(b*b - 3*a*c))/(3*a);
	/*
	for (p = -2; p < 1; p++)
	{
		if (6*a*p+b > 0) ret |= TOO_MUCH_NOISE;
	}	
	*/
	*Max_pos = (float)(xm + nmax + delta); 
	*Max_val = (a * xm * xm * xm) + (b * xm * xm) + (c * xm) + d;
	return ret;
}

int do_piv_on_avg_im_along_x(void)
 {
   register int i, j, k, l, m;
   static int delta = 4, x = 20;
   O_i *ois = NULL, *oim = NULL , *oicp = NULL, *oicv = NULL;
   O_p *op = NULL, *opc = NULL, *opcmoy = NULL;
   float *tmp, l_s, l_e, *tmpmoy, nbre_of_windows, pos, val, tmpp, tmpv, valm, posm, moy;
   int nf,n_black = 0;
   imreg *imr = NULL;
   d_s  *dse, *dsc, *dscpmoy, *dscpe, *dscpa;
 
 
         if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
         {
                 active_menu->flags |=  D_DISABLED;
                 return D_O_K;   
         }               
         if(updating_menu_state != 0)    
         {
                 if (ois->im.n_f == 1 || ois->im.n_f == 0)       
                         active_menu->flags |=  D_DISABLED;
                 else active_menu->flags &= ~D_DISABLED;
                 return D_O_K;   
         }       
         nf = abs(ois->im.n_f);          
         if (nf <= 0)    
         {
                 win_printf("This is not a movie");
                 return D_O_K;
         }
         i = ois->im.c_f;
         
         i = win_scanf("largeur du moyennage %6d\nvalmax %6d\nnumber of black frames %6d\n", &delta, &x, &n_black);
	 if (i == CANCEL) return D_O_K;

	 nf = nf - n_black;
	     
	 nbre_of_windows = ois->im.nx/delta;
	     
	 oim = create_and_attach_movie_to_imr(imr, ois->im.nx/delta, ois->im.ny, IS_FLOAT_IMAGE, 2);
	 if (oim == NULL) return (win_printf_OK("cant create oim array!"));

	 oicp = create_and_attach_oi_to_imr(imr, ois->im.nx/delta, nf, IS_FLOAT_IMAGE);
	 if (oicp == NULL) return (win_printf_OK("cant create oicp array!"));

	 oicv = create_and_attach_oi_to_imr(imr, ois->im.nx/delta, nf, IS_FLOAT_IMAGE);
	 if (oicv == NULL) return (win_printf_OK("cant create oicv array!"));

	     tmpmoy = (float*)calloc(ois->im.ny,sizeof(float));
	     if (tmpmoy == NULL) return (win_printf_OK("cant create tmpmoy array!"));

	     tmp = (float*)calloc(ois->im.ny,sizeof(float));
	     if (tmp == NULL) return (win_printf_OK("cant create tmp array!"));

	     op = create_and_attach_op_to_oi(oim, ois->im.ny, ois->im.ny, 0, IM_SAME_Y_AXIS);
	     if (op == NULL) return (win_printf_OK("I can't create op!"));
	     dse = op->dat[0];
	     dsc = create_and_attach_one_ds(op, ois->im.ny, ois->im.ny, 0);

	     opc = create_and_attach_op_to_oi(oicp, oicp->im.ny, oicp->im.ny, 0, IM_SAME_Y_AXIS);
	     if (opc == NULL) return (win_printf_OK("I can't create opc!"));
	     dscpmoy = opc->dat[0];
	     dscpe = create_and_attach_one_ds(opc, oicp->im.ny, oicp->im.ny, 0);

	     opcmoy = create_and_attach_op_to_oi(oicp, oicp->im.nx, oicp->im.nx, 0, IM_SAME_X_AXIS);
	     if (opc == NULL) return (win_printf_OK("I can't create opc!"));
	     dscpa = opcmoy->dat[0];

   /*average movie + correlation*/
	     for (k = 0; k < nf; k++)
             {
	       switch_frame(ois,k);
	       display_title_message("image %d",k);
	       switch_frame(oim,k%2);
	       for (l=0; l < nbre_of_windows; l++)
		 {
		   l_s = l*delta;
		   l_e = l_s + delta;
		   for (j=0 ; j< ois->im.ny; j++) tmpmoy[j] = 0;  
		   for (m = l_s; m < l_e && m < ois->im.nx; m++)
		     {
		       extract_raw_row(ois, m, tmp);
		       for (j=0 ; j< ois->im.ny; j++) tmpmoy[j] += tmp[j];  
		     }
		   for(j = 0, moy = 0; j < ois->im.ny/4; j++)	             
		     {
		       moy += tmpmoy[j];	
		       moy += tmpmoy[ois->im.ny-1-j];	
		     }
		   for(j = 0, moy = 2*moy/ois->im.ny; j < ois->im.ny; j++)   tmpmoy[j] = tmpmoy[j] - moy;
		   realtr1(ois->im.ny, tmpmoy);
		   fft(ois->im.ny, tmpmoy, 1);
		   realtr2(ois->im.ny, tmpmoy, 1);
		   for (j=0, tmpmoy[0] = 0; j < ois->im.ny && i != 0; j++) oim->im.pixel[j].fl[l] = tmpmoy[j]/delta;
		 }
	       if (k > 0)
		 {
		   for (l=0; l < nbre_of_windows; l++)
		   
		     {
		       switch_frame(oim,(k-1)%2);
		       extract_raw_row(oim, l, dse->xd);
		       switch_frame(oim,k%2);
		       extract_raw_row(oim, l, dse->yd);
		       correlate_2_1d_sig(dse->xd,ois->im.ny,dse->yd,(ois->im.ny)/4,1,dsc->xd,0);
		       find_max1(dsc->xd, dsc->nx, &pos, &val);
		       oicp->im.pixel[k].fl[l] = pos - (ois->im.ny)/2;
		       oicv->im.pixel[k].fl[l] = val;
		     }
		 }
	     }

  /*average velocity*/
	     for (l=0; l < oicv->im.nx; l++)
	       {
		 extract_raw_row(oicv, l, dscpe->xd);
		 find_max1(dscpe->xd, dscpe->nx, &posm, &valm);
		 for (k = 0, tmpv = tmpp = 0; k < oicv->im.ny; k++)
		   {
		     tmpp = tmpp*(x*valm - oicv->im.pixel[k].fl[l])/(x*valm) +  oicp->im.pixel[k].fl[l]*oicv->im.pixel[k].fl[l];
		     tmpv = tmpv*(x*valm - oicv->im.pixel[k].fl[l])/(x*valm) +  oicv->im.pixel[k].fl[l];
		 if (tmpv != 0) dscpmoy->yd[k] = tmpp/tmpv;
		 else dscpmoy->yd[k] = dscpmoy->yd[k-1];
		 dscpmoy->xd[k] = k;
		   }
		 for(k = 0; k < oicv->im.ny; k++) dscpa->yd[l] += dscpmoy->yd[k];
		    dscpa->yd[l] /= nf;
		    dscpa->xd[l] = l;
	       }


   /*autocorrelation movie	  
	    for (k = 0; k < nf-1; k++)
	      {
		for (l=0; l < oim->im.nx; l++)
		  { 
		    switch_frame(oim,k);
		    extract_raw_row(oim, l, dse->xd);
		    switch_frame(oim,k+1);
		    extract_raw_row(oim, l, dse->yd);
		    correlate_2_1d_sig(dse->xd,ois->im.ny,dse->yd,(ois->im.ny)/4,1,dsc->xd,0);
		    for (j=0; j < oim->im.ny; j++)
		      {
			dsc->yd[j] =  dse->yd[j] = j;
			switch_frame(oic,k);
			oic->im.pixel[j].fl[l] = dsc->xd[j];
			}
		  }
		  }*/

  /*plot average speed profile along x
	 for (l=0; l < oim->im.nx; l++)
		  {
		    for (k = 0; k < nf-1; k++)
		      {
			switch_frame(oic,k);
			extract_raw_row(oic,l,dsctmp->xd);
			find_max1(dsctmp->xd, dscp->nx, &pos, &val);
			dscp->yd[k] = pos - (oic->im.ny)/2;
			dscv->yd[k] = val;
			dscv->xd[k] = dscp->xd[k] = dscpa->xd[k] = k;
		      }
		    find_max1(dscv->yd, dscv->ny, &posm, &valm);
		    for (k = 0, tmpv = tmpp = 0 ; k < nf-1; k++)
		      {
			tmpp = tmpp*(x*valm - dscv->yd[k])/(x*valm) + dscp->yd[k]*dscv->yd[k];
			tmpv = tmpv*(x*valm - dscv->yd[k])/(x*valm) + dscv->yd[k];
			if (tmpv != 0) dscpa->yd[k] = tmpp/tmpv;
			else dscpa->yd[k] = dscpa->yd[k-1];
		      }
		    for(k = 0; k < oim->im.n_f; k++) dscvmoy->yd[l] += dscpa->yd[k]/nf;
		    dscvmoy->yd[l] = dscvmoy->yd[l]/oim->im.n_f;
		    dscvmoy->xd[l] = l;
		    }*/
              
	 find_zmin_zmax(oim);
	 find_zmin_zmax(oicp);
	 find_zmin_zmax(oicv);
	 return refresh_im_plot(imr, oicp->cur_op);                    
 }



MENU *piv_image_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	//add_item_to_menu(mn,"average profile", do_piv_along_x,NULL,0,NULL);
	add_item_to_menu(mn,"average profile", do_piv_on_avg_im_along_x,NULL,0,NULL);


	return mn;
}

int	piv_main(int argc, char **argv)
{
	add_image_treat_menu_item ( "piv", NULL, piv_image_menu(), 0, NULL);
	return D_O_K;
}

int	piv_unload(int argc, char **argv)
{
	remove_item_to_menu(image_treat_menu, "piv", NULL, NULL);
	return D_O_K;
}
#endif

