#ifndef _INFORS_H_
#define _INFORS_H_

#define DEBUG       1

#define READ_ONLY   0
#define WRITE_ONLY  1
#define WRITE_READ  2

#define MAX_STEP    16

#define MINITRON    00
#define MULTITRON   31

typedef struct one_temp_step
{
  float ti;
  float tf;
  int duration;
} ots;

# ifndef _INFORS_C_
PXV_VAR(HANDLE, hCom);
PXV_VAR(COMMTIMEOUTS, lpTo);
PXV_VAR(COMMCONFIG, lpCC);
PXV_VAR(int, source_running);
PXV_VAR(char, str_com[32]);

PXV_VAR(O_p*, top);
PXV_VAR(ots, step[MAX_STEP]);
# endif

# ifdef _INFORS_C_

HANDLE hThread = NULL;
int source_running = 0;

HANDLE hCom = NULL;
COMMTIMEOUTS lpTo;
COMMCONFIG lpCC;
UINT timeout = 500;  // ms
char str_com[32];

char *debug_file = "c:\\serial_debug.txt";
int debug = DEBUG;
FILE *fp_debug = NULL;

static int main_id = MINITRON; // param
//static int main_id = MULTITRON; // param
static int sub_id = 0x80; // param
#ifndef FAKE_RS232
static int serial_port = 1; // param
static int serial_baud = 9600;
#endif
static int freq = 20;        // param: points per minute (<= 20)
char save_path[512];
char save_file[512];

O_p* top;
ots step[MAX_STEP];
static int cycDuration = 0, iCyc = 0, iSt = 0;
static int nSteps = 3, nCyc = 2;
float cur_ref = 0;

# endif
PXV_FUNC(HANDLE,init_serial_port,(unsigned short port_number, int baudrate, int CTSRTS, int verbose));
PXV_FUNC(int, CloseSerialPort,(HANDLE hCom));
PXV_FUNC(int, close_serial_port,(void));
PXV_FUNC(int,Write_serial_port,(HANDLE hCom_port, char *ch, DWORD NumberOfBytesToWrite));
PXV_FUNC(int,Read_serial_port,(HANDLE hCom_port, char *ch, DWORD nNumberOfBytesToRead, unsigned long *lpNumberOfBytesWritten));
PXV_FUNC(int, write_command_on_serial_port,(void));
PXV_FUNC(int, read_on_serial_port,(void));
PXV_FUNC(int,talk_serial_port,(HANDLE hCom_port, char *command, int wait_answer, char * answer, DWORD NumberOfBytesToWrite, DWORD nNumberOfBytesToRead));

PXV_FUNC(DWORD WINAPI, StepThreadProc, (LPVOID lpParam)); 

PXV_FUNC(MENU*, serialw_menu, (void));
PXV_FUNC(MENU*, infors_plot_menu, (void));
PXV_FUNC(int, infors_main, (int argc, char **argv));

#endif

