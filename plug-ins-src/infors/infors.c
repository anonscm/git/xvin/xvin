/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _INFORS_C_
#define _INFORS_C_

# include "allegro.h"
# include "winalleg.h"
# include "xvin.h"

/* If you include other regular header do it here*/ 
# include <windowsx.h>
# include <windows.h>

/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "infors.h"
//place here other headers of this plugin 

#define BPM  60000
// beats per min (1ms*1000*60 = 60 000ms = 1min)
#define DELAY Sleep(100)

HANDLE init_serial_port(unsigned short port_number, int baudrate, int RTSCTS, int verbose)
{
    HANDLE hCom_port = NULL;
        
    sprintf( str_com, "\\\\.\\COM%d", port_number);
    hCom_port = CreateFile(str_com,GENERIC_READ|GENERIC_WRITE,0,NULL,
        OPEN_EXISTING,/*FILE_FLAG_OVERLAPPED*/0,NULL);//I start with non overlapped
    
    if (GetCommState( hCom_port, &lpCC.dcb) == 0) 
    {
        win_printf("GetCommState FAILED");
        return NULL;
    }    

    /* Initialisation des parametres par defaut */
    lpCC.dcb.BaudRate = baudrate;
    if (verbose)
      win_printf("lpCC.dcb.BaudRate = %d",lpCC.dcb.BaudRate);
    lpCC.dcb.ByteSize = 8;
    lpCC.dcb.StopBits = TWOSTOPBITS; //ONESTOPBIT
    lpCC.dcb.Parity = NOPARITY;
    lpCC.dcb.fOutX = FALSE;
    lpCC.dcb.fInX = FALSE;


    if (RTSCTS == 0)
      {
	lpCC.dcb.fDtrControl = DTR_CONTROL_DISABLE;//DTR_CONTROL_HANDSHAKE or DTR_CONTROL_DISABLE;
	lpCC.dcb.fRtsControl = RTS_CONTROL_DISABLE;//RTS_CONTROL_HANDSHAKE
      }
    else
      {
	lpCC.dcb.fDtrControl = DTR_CONTROL_HANDSHAKE;// or DTR_CONTROL_DISABLE;
	lpCC.dcb.fRtsControl = RTS_CONTROL_HANDSHAKE;
      }
 
 
    if (SetCommState( hCom_port, &lpCC.dcb )== 0) 
    {
        win_printf("SetCommState FAILED");
        return NULL;
    }    

    if (GetCommTimeouts(hCom_port,&lpTo)== 0) 
    {
        win_printf("GetCommTimeouts FAILED");
        return NULL;
    }
        
    lpTo.ReadIntervalTimeout = 0;
    lpTo.ReadTotalTimeoutMultiplier = 1;
    lpTo.ReadTotalTimeoutConstant = 1;
    lpTo.WriteTotalTimeoutMultiplier = 1;
    lpTo.WriteTotalTimeoutConstant = 1;
    
    if (SetCommTimeouts(hCom_port,&lpTo)== 0) 
    {
        win_printf("SetCommTimeouts FAILED");
        return NULL;
    }    
    
    if (SetupComm(hCom_port,2048,2048) == 0) 
    {
        win_printf("Init Serial port %d FAILED",port_number+1);
        return NULL;
    }
    if (verbose)
      win_printf("Init Serial port %d OK",port_number);
    return hCom_port;
}

int purge_com(HANDLE hCom)
{
  if (hCom == NULL) return 1; 
  return PurgeComm(hCom, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);
}

int init_port(void)
{
  int i;
  static int port_number = 1, sbaud = 0, hand = 0;

  if(updating_menu_state != 0)	return D_O_K;

  if (hCom != NULL) return  win_printf_OK("Init Serial port in use"); 
  i = win_scanf("Port number ?%5d\nDump in and out in a debug file\n"
		"No %R yes %r\n"
		"Baudrate 57.6k %R 115.2k %r 230.4k %r 460.8k %r 921.6k %r\n"
		"HandShaking None %R Cts/Rts %r"
		,&port_number,&debug,&sbaud,&hand);
  if (i == CANCEL) return D_O_K;
  hCom = init_serial_port(port_number, pow(2,sbaud)*57600, hand, 1);
  if (hCom == NULL) return 1;
  if (debug)
    {
      fp_debug = fopen(debug_file,"w");
      if (fp_debug != NULL) fclose(fp_debug);
    }
  purge_com(hCom);
  
  return D_O_K;
}

int CloseSerialPort(HANDLE hComl)
{
    if (CloseHandle(hComl) == 0) return win_printf("Close Handle FAILED!");
    hComl = NULL;
    return D_O_K;
}

int close_serial_port(void)
{
        if(updating_menu_state != 0)	return D_O_K;
        CloseSerialPort(hCom);
        return D_O_K;
}


int Write_serial_port(HANDLE handle, char *data, DWORD length)
{
  int success = 0, pos = 0;
  DWORD dwWritten = 0;
  OVERLAPPED o= {0};


  if (handle == NULL) return -2;

  o.hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
  for ( ;pos < (int)length; )
    {
      dwWritten = 0;
      if (!WriteFile(handle, (LPCVOID)(data + pos), length - pos, &dwWritten, &o))
	{
	  if (GetLastError() == ERROR_IO_PENDING)
	    if (WaitForSingleObject(o.hEvent, timeout) == WAIT_OBJECT_0)
	      if (GetOverlappedResult(handle, &o, &dwWritten, FALSE))
		success = 1;
	}
      else	success = 1;
      pos += dwWritten;
    }
  // if (dwWritten != length)     success = false;
  CloseHandle(o.hEvent);

  if (debug)
    {
      fp_debug = fopen(debug_file,"a");
      if (fp_debug != NULL)
	{
	  if (dwWritten != length)    
	    fprintf (fp_debug,">pb writting %d char instead of %d\n",(int)dwWritten,(int)length);
	  fprintf (fp_debug,">\t %s\n",data);
	  fclose(fp_debug);
	}
    }

  DELAY;

  return (success == 0) ? -1 : dwWritten;
}


int ReadData(HANDLE handle, char *data, DWORD length, DWORD* dwRead)
{
  int success = 0;
  OVERLAPPED o= {0};

  if (handle == NULL) return -2;
  o.hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
  if (!ReadFile(handle, data, length, dwRead, &o))
    {
      if (GetLastError() == ERROR_IO_PENDING)
	if (WaitForSingleObject(o.hEvent, timeout) == WAIT_OBJECT_0)
	  success = 1;
      GetOverlappedResult(handle, &o, dwRead, FALSE);
    }
  else
    success = 1;
  CloseHandle(o.hEvent);
  return success;
}


int Read_serial_port(HANDLE handle, char *stuff, DWORD max_size, DWORD *dwRead)
{
  register int i, j;
  
  if (handle == NULL) return -2;
  char lch[2];

  for (i = j = 0; j == 0 && i < max_size-1; )
    {
      *dwRead = 0;
      if (ReadData(handle, lch, 1, dwRead) == 0)
	{
	  stuff[i] = 0;
	  *dwRead = i;
	  if (debug)
	    {
	      fp_debug = fopen(debug_file,"a");
	      if (fp_debug != NULL)
		{
		  fprintf (fp_debug,"<-- ReadData error\n");
		  fprintf (fp_debug,"<\t %s\n",stuff);
		  fclose(fp_debug);
		}
	    }
	  return -1;
	}
      if (*dwRead == 1) 
	{
	  stuff[i] = lch[0];
	  j = (lch[0] == '\n') ? 1 : 0;
	  i++;
	}
    }
  stuff[i] = 0;  // end of string
  *dwRead = i;

  if (debug)
    {
      fp_debug = fopen(debug_file,"a");
      if (fp_debug != NULL)
	{
	  fprintf (fp_debug,"<\t %s\n",stuff);
	  fclose(fp_debug);
	}
    }

  return i;
}

int write_command_on_serial_port(void)
{
    static char Command[128]="test";
    
    if(updating_menu_state != 0)	return D_O_K;
    
    win_scanf("Command to send? %s",&Command);
    strcat(Command,"\r");  
    Write_serial_port(hCom,Command,strlen(Command));

    return D_O_K;
}


int read_on_serial_port(void)
{
    char Command[128];
    unsigned long lpNumberOfBytesWritten = 0;
    int ret = 0;
    DWORD nNumberOfBytesToRead = 127;
    unsigned long t0;
    
    if(updating_menu_state != 0)	return D_O_K;

    t0 = my_uclock();        
    ret = Read_serial_port(hCom,Command,nNumberOfBytesToRead,&lpNumberOfBytesWritten);
    t0 = my_uclock() - t0;
    Command[lpNumberOfBytesWritten] = 0;

    win_printf("command read = %s \n ret %d lpNumberOfBytesWritten = %d\n in %g mS",Command,ret,lpNumberOfBytesWritten,1000*(double)(t0)/get_my_uclocks_per_sec());

    return D_O_K;
}


int write_read_serial_port(void)
{
    if(updating_menu_state != 0)	return D_O_K;
    if (hCom == NULL) return win_printf_OK("No serial port init");
    
        write_command_on_serial_port();

        read_on_serial_port();

        return D_O_K;
}


int n_write_read_serial_port(void)
{
    static char Command[128]="dac16?";
    static int ntimes = 1, i;
    unsigned long t0;
    double dt, dtm, dtmax;
    char resu[128], *ch;
    int ret = 0;
    unsigned long lpNumberOfBytesWritten = 0;
    DWORD nNumberOfBytesToRead = 127;
    
    if(updating_menu_state != 0)	return D_O_K;

    if (hCom == NULL) return win_printf_OK("No serial port init�");    
    for (ch = Command; *ch != 0; ch++) 
      if (*ch == '\r') *ch = 0;
    win_scanf("Command to send? %snumber of times %d",&Command,&ntimes);
       
    strcat(Command,"\r");  

    for(i = 0, dtm = dtmax = 0; i < ntimes; i++)
      {
	t0 = my_uclock();    
	Write_serial_port(hCom,Command,strlen(Command));
	ret = Read_serial_port(hCom,resu,nNumberOfBytesToRead,&lpNumberOfBytesWritten);
	for  (; ret <= 0; )
	  ret = Read_serial_port(hCom,resu,nNumberOfBytesToRead,&lpNumberOfBytesWritten);

	t0 = my_uclock() - t0;
	dt = 1000*(double)(t0)/get_my_uclocks_per_sec();
	resu[lpNumberOfBytesWritten] = 0;
	dtm += dt;
	if (dt > dtmax) dtmax = dt;
      }
    dtm /= ntimes;
    win_printf("command read = %s \n ret %d Number Of Bytes  = %d\nin %g mS in avg %g max",resu,ret,lpNumberOfBytesWritten,dtm, dtmax);
    return D_O_K;
}



/********************************************************************************************************/
/********************** Functions for infors control ***********************************************/
/********************************************************************************************************/

#define SWITCH_REMOTE_MODE   sprintf(command, "%c%02d%cMRA%c", 2, main_id, sub_id, 3);  Write_serial_port(hCom, command, strlen(command))
#define SWITCH_LOCAL_MODE    sprintf(command, "%c%02d%cMLA%c", 2, main_id, sub_id, 3);  Write_serial_port(hCom, command, strlen(command))

int set_infors_temp(float temperature)
{
  static char command[128];

/*   if ((int) (temperature*10) == cur_ref) */
/*     return 0; */

#ifndef FAKE_RS232 
  if (hCom != NULL) return my_set_window_title("Cannot open connexion with incubator");
  hCom = init_serial_port(serial_port, serial_baud, 0, 0);
  if (hCom == NULL) return 1;
  purge_com(hCom);

  // Set tref
  SWITCH_REMOTE_MODE;
#endif
  sprintf(command, "%c%02d%cS%dA%c", 2, main_id, sub_id, (int)(temperature*10), 3);
  my_set_window_title(command);

#ifdef FAKE_RS232 
  my_set_window_title("command: %s", command);
#endif

#ifndef FAKE_RS232 
  Write_serial_port(hCom, command, strlen(command));
  SWITCH_LOCAL_MODE;

  CloseSerialPort(hCom);
  hCom = NULL;
#endif

  return 0;
}

int set_infors_temp_gui(void)
{
  int i;
  float temp = 0;

  if(updating_menu_state != 0)
    return D_O_K;

  if (source_running == 1)
    win_printf_OK("There is already a cycle running");   

  i = win_scanf("Reference temperature %4f", &temp);
  if (i == CANCEL) return D_O_K;

  set_infors_temp(temp);

  return 0;
}

float read_infors_temp(void)
{
  int temperature = 0;
  static char command[128];

#ifndef FAKE_RS232 
  char resu[128], resu2[16];
  int ret = 0;
  unsigned long lpNumberOfBytesWritten = 0;
  DWORD nNumberOfBytesToRead = 10;

  if (hCom != NULL) return my_set_window_title("Cannot open connexion with incubator");
  hCom = init_serial_port(serial_port, serial_baud, 0, 0);
  if (hCom == NULL) return 1;
  purge_com(hCom);

  // Read tsensor
#endif
  sprintf(command, "%c%02d%cRA%c", 2, main_id, sub_id, 3);
#ifdef FAKE_RS232 
  my_set_window_title("command: %s", command);
#else
  Write_serial_port(hCom, command, strlen(command));
  ret = Read_serial_port(hCom,resu,nNumberOfBytesToRead,&lpNumberOfBytesWritten);
  for  (; ret <= 0; )
    ret = Read_serial_port(hCom,resu,nNumberOfBytesToRead,&lpNumberOfBytesWritten);
  CloseSerialPort(hCom);
  hCom = NULL;

  resu[11] = 0;
  my_set_window_title(resu);
  //  win_printf_OK("resu: %s", resu);  
  //template: 2 00 128 331 E3
  sprintf(resu2, "%c%02d%c%%dE", 2, main_id, sub_id);
  sscanf(resu, resu2, &temperature);
  //  temperature /= 10;
#endif

  return (float)temperature/10;
}

int read_infors_temp_gui(void)
{
#ifndef FAKE_RS232
  float temp;
#endif

  if(updating_menu_state != 0)
    return D_O_K;

  if (source_running == 1)
    win_printf_OK("There is already a cycle running");   

#ifndef FAKE_RS232
  temp = read_infors_temp();
  win_printf_OK("Sensor temperature: %f", temp);
#else
  win_printf_OK("You have to compile with FAKE_RS232 disabled");
# endif

  return 0;
}


/********************************************************************************************************/
/********************** Functions for cycles thread *****************************************************/
/********************************************************************************************************/

// Idle action for OD profile plot
int infors_op_idle_action(O_p *op, DIALOG *d)
{
  if (d == NULL || d->dp == NULL || d->proc == NULL)    return 1;
  if (op->need_to_refresh)      d->proc(MSG_DRAW,d,0);	
  return 0;
}

// Define a timer to acquire OD every second
VOID CALLBACK dummyAPCProc(LPVOID lpArgToCompletionRoutine, DWORD dwTimerLowValue, DWORD dwTimerHighValue)
{
  HANDLE hTimer = (HANDLE) lpArgToCompletionRoutine;
  d_s *ds1, *ds2;
  float ti = 0, tf = 0, tref = 0, tsensor = 0;
  static int i, nx_i = 0, nx_dur = 0, nx_max = 0;
  char fullname[1024];

  my_set_window_title("coucou!");

  // Access to dataset
  ds1 = top->dat[0];
/*   if (ds1 == NULL) */
/*     return 0; */
  ds2 = top->dat[1];
/*   if (ds2 == NULL) */
/*     return 0; */

  ti = step[iSt].ti;
  tf = step[iSt].tf;
  
  nx_i = iCyc * cycDuration * freq;
  for (i = 0; i < iSt; i++)
    nx_i += step[i].duration * freq;

  nx_dur = step[iSt].duration * freq;   // nb of acq for this step
  nx_max = nx_dur + nx_i;               // final index at the end of this step
  
  my_set_window_title("iSt: %d  iCyc: %d  nx_i: %d  nx_dur: %d  nx_max: %d, nx: %d  %f %f %f %f", iSt, iCyc, nx_i, nx_dur, nx_max, ds1->nx, ti, tf, tref, tsensor);

  // To be executed at each timer overflow
  tref = (float) (ds1->nx - nx_i);
  tref /= (float) (nx_dur-1);
  tref *= (tf - ti);
  tref += ti;

#ifndef FAKE_RS232 
  // Set tref
  set_infors_temp(tref);
  // Read tsensor
  tsensor = read_infors_temp();
#endif
#ifdef FAKE_RS232 
  tsensor = 10*cos((M_PI * ds1->nx)/40); 
#endif
  ds1->xd[ds1->nx] = ds1->nx;
  ds1->yd[ds1->nx] = tref;
  ds2->xd[ds2->nx] = ds2->nx;
  ds2->yd[ds2->nx] = tsensor;
  ds1->nx++;
  ds1->ny = ds1->nx;
  ds2->nx++;
  ds2->ny = ds2->nx;

  top->need_to_refresh = 1;

  if (ds1->nx%10 == 0)
    {
      append_filename(fullname, top->dir, top->filename, 1024);
      save_one_plot(top, fullname);
    }
   
  if (ds1->nx >= nx_max || source_running != 1)
    {
      CancelWaitableTimer(hTimer);
      source_running = 0;
      //   CloseHandle(hThread);
    }

  return;
}

DWORD WINAPI StepThreadProc( LPVOID lpParam ) 
{
  HANDLE hTimer = NULL;
  LARGE_INTEGER liDueTime;
  LONG period;
  DIALOG *starting_one = NULL;
  static int nx_dur = 0;

  nx_dur = step[iSt].duration * freq;   // nb of acq for this step

  // Create a waitable timer.
  if (freq == 0 || nx_dur == 0)
    return 1;

  liDueTime.QuadPart = 0; // start without delay
#ifndef FAKE_RS232
  period = BPM / freq;
#else
  period = BPM / freq / 10; // for debugging (10x faster)
#endif

  my_set_window_title("period: %ld", period);
  //Sleep(1000); // first call ok with this sleep

  hTimer = CreateWaitableTimer(NULL, TRUE, "WaitableTimer2");
  if (!hTimer)   win_printf_OK("CreateWaitableTimer failed (%d)\n", GetLastError());
  starting_one = active_dialog; // to check if windows change
  source_running = 1;

  // Set a timer to wait for (1min / freq) indefinitely.
  if (!SetWaitableTimer(hTimer, &liDueTime, period, dummyAPCProc, (LPVOID *) hTimer, 0))
    win_printf_OK("SetWaitableTimer failed (%d)\n", GetLastError());

  // Wait for the timer.
  SleepEx (INFINITE, TRUE);
 
  return 0;
}

int create_step_thread(void)
{
  DWORD dwThreadId;
    
  hThread = CreateThread( 
			 NULL,              // default security attributes
			 0,                 // use default stack size  
			 StepThreadProc,    // thread function 
			 NULL,              // argument to thread function 
			 0,                 // use default creation flags 
			 &dwThreadId);      // returns the thread identifier 

  if (hThread == NULL) 	win_printf_OK("No thread created");
  SetThreadPriority(hThread,  THREAD_PRIORITY_NORMAL);

  return 0;
}

int infors_idle_action(DIALOG *d)
{
  char title[1024];

  if (source_running) return 0;
  if (iSt >= nSteps)  // new cycle
  {
    iSt = -1;
    iCyc++;
    if (iCyc >= nCyc)
    {
      general_idle_action = NULL;
      return 0;
    }
  }

  // start new step in current cycle
  win_printf_OK("New step");
  iSt++;
  sprintf(title, "iSt: %d  iCyc: %d", iSt, iCyc);
  my_set_window_title(title);
  create_step_thread();

  return 0;
}

int start_temp_cycle(void)
{
  int do_save(void);
  
  pltreg *pr;
  d_s *ds1, *ds2;
  int  i, n;
  int nx;
  char mes[128];

  if(updating_menu_state != 0)
    {
      add_keyboard_short_cut(0, KEY_N, 0, start_temp_cycle);
      return D_O_K;
    }

  // User prompt
  sprintf(mes, "Number of cycle steps: %%3d\n[max is %d]\n\nNumber of cycles %%3d", MAX_STEP);
  i = win_scanf(mes, &nSteps, &nCyc);
  if (i == CANCEL) return D_O_K;

  cycDuration = 0;
  for (n = 0; n < nSteps; n++)
  {
    step[n].ti = 37;
    step[n].tf = 42;
    step[n].duration = 1;
    sprintf(mes, "step %d:\n\nTinit %%4f\nTend %%4f\nDuration %%3d min", n);
    i = win_scanf(mes,&(step[n].ti),&(step[n].tf),&(step[n].duration)); 
    //win_printf_OK("step %d:\n\nTinit %4f\nTend %4f\nDuration %3d min", n, step[n].ti, step[n].tf, step[n].duration );
    if (i == CANCEL) return D_O_K;
    cycDuration += step[n].duration;
  }

  // Create and allocate plot
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return 1;
  nx = cycDuration * nCyc;    // total duration (minutes)
  nx *= freq;                 // nb of points per minute
  top = create_and_attach_one_plot(pr, nx, nx, 0);
  if (top == NULL)    return 2;
  ds1 = top->dat[0];            // temperature reference
  if (ds1 == NULL)    return 3;
  ds2 = create_and_attach_one_ds(top, nx, nx, 0);  //temperature sensor: ds2 = top->dat[1]
  if (ds2 == NULL)   return 4;
  top->op_idle_action = infors_op_idle_action;

  //set_plot_symb(ds1, "\\pt5\\oc");
  set_plot_x_title(top,"Time (min)");
  set_plot_y_title(top,"Temperature (^oC)");
  //  top->xu[top->c_xu]->dx = (float) 1 / (float) freq;  // Set x unit set with accurate scale
  create_attach_select_x_un_to_op (top, IS_OTHER, 0, (float) 1/freq, 0, 0, "\'");
  set_plt_x_unit_set(pr, top->c_xu);
  strftime(mes, 31, "%d.%m.%Y %H:%M", localtime(&(ds1->time)));
  set_plot_title(top, "Temperature profile on %s", mes);
  strftime(mes, 31, "%Y%m%d_%H%M", localtime(&(ds1->time)));
  set_op_filename(top, "Tprofile_%s.gr", mes);

  ds1->nx = 0;
  ds1->ny = 0;
  ds2->nx = 0;
  ds2->ny = 0;

  //Save plot and store saving infos
  do_save();
  strcpy(save_path, top->dir);
  strcpy(save_file, top->filename);

  //Start temperature cycles
  iCyc = 0;
  iSt = -1;
  general_idle_action = infors_idle_action;

  return 0;
}

int killThread(void)
{
/*   DWORD lpExitCode; */
  
  source_running = 0;

/*   GetExitCodeThread(hThread, &lpExitCode); */
/*   TerminateThread(hThread, lpExitCode); */
/*   CloseHandle(hThread); */

  return 0;
}

int stop_infors()
{
 
  if(updating_menu_state != 0)
    {
      add_keyboard_short_cut(0, KEY_S, 0, stop_infors);
      return D_O_K;
    }

  killThread();
  general_idle_action = NULL;

  return D_O_K;
}


/********************************************************************************************************/
/*********************************** Menu Functions *****************************************************/
/********************************************************************************************************/

MENU *serialw_menu(void) 
{
  static MENU mn[32];
 
    
  if (mn[0].text != NULL)	return mn;
  
  add_item_to_menu(mn,"Init serial port", init_port,NULL,0,NULL);
  add_item_to_menu(mn,"Write serial port", write_command_on_serial_port,NULL,0,NULL);
  add_item_to_menu(mn,"Read serial port", read_on_serial_port,NULL,0,NULL);
  add_item_to_menu(mn,"Write read serial port", write_read_serial_port,NULL,0,NULL);
  add_item_to_menu(mn,"Write read n times", n_write_read_serial_port,NULL,0,NULL);
  add_item_to_menu(mn,"Close serial port", close_serial_port,NULL,0,NULL);
  return mn;
}


MENU *infors_plot_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"Start temperature profile", start_temp_cycle,NULL,0,NULL);
//  add_item_to_menu(mn,"Start monitoring temperature", start_monitor,NULL,0,NULL);
  add_item_to_menu(mn,"Stop current profile", stop_infors,NULL,0,NULL);
  add_item_to_menu(mn,"Set reference temperature", set_infors_temp_gui,NULL,0,NULL);
  add_item_to_menu(mn,"Read current temperature", read_infors_temp_gui,NULL,0,NULL);

  return mn;
}

int	infors_main(int argc, char **argv)
{
  add_plot_treat_menu_item("Serial Port", NULL, serialw_menu(), 0, NULL);
  add_plot_treat_menu_item("INFORS", NULL, infors_plot_menu(), 0, NULL);
  return D_O_K;
}

int	infors_unload(int argc, char **argv)
{
  remove_item_to_menu(plot_treat_menu, "Serial Port", NULL, NULL);
  remove_item_to_menu(plot_treat_menu, "INFORS", NULL, NULL);
  return D_O_K;
}
#endif

