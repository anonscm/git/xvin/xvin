/*
*    Plug-in program for simple serial comm 
*
*   
*      JF Allemand
*/
#ifndef _SERIALW32_C_
#define _SERIALW32_C_


# include "allegro.h"
# include "winalleg.h"
# include <windowsx.h>
# include "ctype.h"
# include "xvin.h"

#define BUILDING_PLUGINS_DLL
# include "serialw32.h"



int Write_serial_port(HANDLE hCom_port, char *ch, DWORD NumberOfBytesToWrite)
{
    unsigned long NumberOfBytesWritten = 0;
    
    
    if ( !WriteFile( hCom_port, ch, NumberOfBytesToWrite, &NumberOfBytesWritten, NULL ) )  
    return win_printf("WriteFile FAILED\n Bytes to be written %d\n Bytes effectively written %d",NumberOfBytesToWrite,NumberOfBytesWritten);
       
    return NumberOfBytesWritten;   
}

int Read_serial_port(HANDLE hCom_port, char *ch, DWORD nNumberOfBytesToRead, unsigned long *lpNumberOfBytesWritten)
{
    
    if ( !ReadFile( hCom_port, ch, nNumberOfBytesToRead, lpNumberOfBytesWritten, NULL ) )  
    return win_printf("ReadFile FAILED\n Bytes to be written %d\n Bytes effectively written %d",strlen(ch),*lpNumberOfBytesWritten);
    
    return *lpNumberOfBytesWritten;   
}


int talk_serial_port(HANDLE hCom_port, char *command, int wait_answer, char * answer, DWORD NumberOfBytesToWrite, DWORD nNumberOfBytesToRead)
{
    int NumberOfBytesWrittenOnPort = -1;
    unsigned long NumberOfBytesWritten = -1;
    
    if (wait_answer != READ_ONLY)
    {
        NumberOfBytesWrittenOnPort = Write_serial_port(hCom_port, command, NumberOfBytesToWrite);
        //draw_bubble(screen,0,650,75,"NumberOfBytesWrittenOnPort = %d",NumberOfBytesWrittenOnPort);
    }    
		
    if (wait_answer != WRITE_ONLY) 
    {
        Read_serial_port(hCom_port,answer,nNumberOfBytesToRead,&NumberOfBytesWritten);
    
        //draw_bubble(screen,0,650,75,"number bytes read = %d",NumberOfBytesWritten);
    }	
    return 0;
}

int init_port(void)
{
    int port_number = 1;
    
    if(updating_menu_state != 0)	return D_O_K;
    
    win_scanf("Port number ?%d",&port_number);
    hCom = init_serial_port(port_number);
    return D_O_K;
}


HANDLE init_serial_port(unsigned short port_number)
{
    HANDLE hCom_port;
        
    sprintf( str_com, "\\\\.\\COM%d\0", port_number);
    //win_printf("PORT%d",port_number);
    hCom_port = CreateFile(str_com,GENERIC_READ|GENERIC_WRITE,0,NULL,
        OPEN_EXISTING,/*FILE_FLAG_OVERLAPPED*/0,NULL);//I start with non overlapped
    /*if ( hCom_port== INVALID_HANDLE_VALUE);
    {
        win_printf("INIT FAILED");
        return NULL;
    }   */ 
    
    if (GetCommState( hCom_port, &lpCC.dcb) == 0) 
    {
        win_printf("GetCommState FAILED");
        return NULL;
    }    

    /* Initialisation des parametres par defaut */
    /*http://msdn.microsoft.com/library/default.asp?url=/library/en-us/devio/base/dcb_str.asp*/
    //win_printf("lpCC.dcb.BaudRate = %d",lpCC.dcb.BaudRate);
    lpCC.dcb.BaudRate = CBR_9600;
    lpCC.dcb.ByteSize = 8;
    lpCC.dcb.StopBits = ONESTOPBIT;
    lpCC.dcb.Parity = NOPARITY;

    lpCC.dcb.fDtrControl = DTR_CONTROL_ENABLE;//DTR_CONTROL_HANDSHAKE;
    lpCC.dcb.fRtsControl = RTS_CONTROL_ENABLE;//RTS_CONTROL_HANDSHAKE
 
 
    if (SetCommState( hCom_port, &lpCC.dcb )== 0) 
    {
        win_printf("SetCommState FAILED");
        return NULL;
    }    

    if (GetCommTimeouts(hCom_port,&lpTo)== 0) 
    {
        win_printf("GetCommTimeouts FAILED");
        return NULL;
    }
        
    lpTo.ReadIntervalTimeout = 0;
    lpTo.ReadTotalTimeoutMultiplier = 10;
    lpTo.ReadTotalTimeoutConstant = 10;
    lpTo.WriteTotalTimeoutMultiplier = 10;
    lpTo.WriteTotalTimeoutConstant = 100;
    
    if (SetCommTimeouts(hCom_port,&lpTo)== 0) 
    {
        win_printf("SetCommTimeouts FAILED");
        return NULL;
    }    
    
    if (SetupComm(hCom_port,2048,2048) == 0) 
    {
        win_printf("Init Serial port %d FAILED",port_number+1);
        return NULL;
    }//win_printf("Init Serial port %d OK",port_number+1);
    return hCom_port;
}

int write_command_on_serial_port(void)
{
    char Command[128];
    //char *test="\r";
    
    if(updating_menu_state != 0)	return D_O_K;
    
    win_scanf("Command to send? %s",&Command);
       
     //strcat(Command,"\r");  
     //win_printf("deja cela %d\n Command %s",strlen(Command), Command);
    Write_serial_port(hCom,Command,strlen(Command));
   //sprintf(Command,"?nm\r");
   //strcat(Command,'\r');
    //win_printf("deja cela %d\n Command %s",strlen(Command), Command);
    
    //win_printf("%s \n lpNumberOfBytesWritten = %d",Command,lpNumberOfBytesWritten);
    return D_O_K;
}


int read_on_serial_port(void)
{
    char Command[128];
    unsigned long lpNumberOfBytesWritten = 0;
    DWORD nNumberOfBytesToRead = 127;
    
    if(updating_menu_state != 0)	return D_O_K;
    
    //win_scanf("Command to send? %s",Command);
    Read_serial_port(hCom,Command,nNumberOfBytesToRead,&lpNumberOfBytesWritten);

    win_printf("command read = %s \n lpNumberOfBytesWritten = %d",Command,lpNumberOfBytesWritten);
    return D_O_K;
}



int write_read_serial_port(void)
{
        if(updating_menu_state != 0)	return D_O_K;
    
        write_command_on_serial_port();
        read_on_serial_port();
        return D_O_K;
}
        
int CloseSerialPort(HANDLE hCom)
{
    if (CloseHandle(hCom) == 0) return win_printf("Close Handle FAILED!");
    return D_O_K;
}

int close_serial_port(void)
{
        if(updating_menu_state != 0)	return D_O_K;
        CloseSerialPort(hCom);
        return D_O_K;
}

/*
To monitor communications events :


       SetCommMask(hCom, dwEvtMask );

dwEvtMask is logical or with : 
EV_BREAK, EV_CTS, EV_DSR, EV_ERR, EV_RING, EV_RLSD , EV_RXCHAR, EV_RXFLAG, EV_TXEMPTY values

 and used WaitCommEvent function

  BOOL WaitCommEvent(
     HANDLE hFile,
     LPDWORD lpEvtMask,
     LPOVERLAPPED lpOverlapped
   );
*/
MENU *serialw_menu(void) 
{
  static MENU mn[32];
 
    
  if (mn[0].text != NULL)	return mn;
  
  
    add_item_to_menu(mn,"Init serial port", init_port,NULL,0,NULL);
    add_item_to_menu(mn,"Write serial port", write_command_on_serial_port,NULL,0,NULL);
    add_item_to_menu(mn,"Read serial port", read_on_serial_port,NULL,0,NULL);
    add_item_to_menu(mn,"Write read serial port", write_read_serial_port,NULL,0,NULL);
    
    add_item_to_menu(mn,"Close serial port", close_serial_port,NULL,0,NULL);
    
    
    return mn;
}



int	serialw32_main(int argc, char **argv)
{
  add_plot_treat_menu_item("SERIAL PORT", NULL, serialw_menu(), 0, NULL);
	
  
  return D_O_K;
}

int	serialw32_unload(int argc, char **argv)
{
	remove_item_to_menu(plot_treat_menu, "SERIAL PORT", NULL, NULL);
	return D_O_K;
}
#endif
