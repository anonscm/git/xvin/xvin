#ifndef _RUNSPECTRUM_C_
#define _RUNSPECTRUM_C_

# include "allegro.h"
# include "winalleg.h"
# include <windowsx.h>
# include "ctype.h"
# include "xvin.h"

#define BUILDING_PLUGINS_DLL

# include "serialw32.h"
# include "pm.h"
# include "spectro.h"
# include "RunSpectrum.h"
# include "pmQE.h"

	
void	find_correction_in_lambda_for_pm_d(float lambda,float *cor)

{		register int i;
		int n_lambda_pm_a=0;

		for (i=0;i<1000/*because interpol with 1000 pts*/;i++)
			n_lambda_pm_a = (pm_d_x[i] <= lambda) ? i : n_lambda_pm_a;
				
		*cor=pm_d_y[n_lambda_pm_a]*.01;
		return;
}	


int aquire_spectrum(void)
{
    static float StartLambda = 500.0;
    static float EndLambda = 550.0;
    //int NumberSteps = 10;
    static float Resolution = 5;
    int direction = 0;
    d_s *ds = NULL;
    O_p *op = NULL;
    pltreg *pr = NULL;
    int i = 0, j, flag = 0;
    float correction_factor = 1;
    int npoints = 1024;
    
    if(updating_menu_state != 0)	return D_O_K;
    
    if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
		return win_printf_OK("cannot find data");

    
    j = win_scanf("You will aquire a spectrum on the Hamatsu HC135-01 with the ACTON 150\n Did you set the parameters for the PM?(else press CANCEL)\n What is the starting wavelength (in nm)?%f\n  What is the final wavelength?%f\n What resolution do you want?%f",&StartLambda,&EndLambda,&Resolution);
    if (j == CANCEL) return D_O_K;
    npoints = (int)(abs(StartLambda - EndLambda)/Resolution+1);
    op = create_and_attach_one_plot(pr,npoints, npoints, 0);//Affiner la taille du dataset
    if (op == NULL) return 1;
    
    ds = op->dat[0];

    
    SetLambda(StartLambda);
    
    Setjog(Resolution);
    
    direction = ((StartLambda - EndLambda) > 0) ? 2: 1;
    Lambda = StartLambda;
    set_plot_x_title(op, "Wavelength");	
    set_plot_y_title(op, "Number of photons");
    set_ds_plain_line(ds);
    
    while ( (keypressed() != TRUE) && ((direction == 1) ? (Lambda - Resolution -EndLambda)< 0 : (Lambda + Resolution -EndLambda)> 0))
    {
        ds->xd[i] = Lambda;//Ajuster pour gain variable?
        find_correction_in_lambda_for_pm_d(Lambda,&correction_factor);
        //draw_bubble(screen,0,450,75,"correction_factor = %f ",correction_factor);
        ds->yd[i] = AquireFromPM()/ ((correction_factor == 0) ? 1 : correction_factor);
        if ( flag != 0)
        {
               Lambda = (direction == 1) ? Lambda + Resolution : Lambda - Resolution;
               SetLambda(Lambda);
               i++;
        }
        flag = 1;
        op->need_to_refresh = 1;
        refresh_plot(pr, UNCHANGED);
        broadcast_dialog_message(MSG_DRAW,0);
        
    }
    
    
    	    
    broadcast_dialog_message(MSG_DRAW,0);
    	    
    
        
   return D_O_K;
}      

float AquireFromPM(void)
{
	unsigned char *command="S\r",answer[5];
	static int k;
	DWORD nNumberOfBytesToRead = 4;
	DWORD NumberOfBytesToWrite = 2;
	float PhotonNumber = 0;
	
	if(updating_menu_state != 0)	return D_O_K;
	
	talk_serial_port(hCom_PM_port, command, WRITE_ONLY, NULL, NumberOfBytesToWrite, nNumberOfBytesToRead);
	
	
	
    k = talk_serial_port(hCom_PM_port, NULL, READ_ONLY, answer, NumberOfBytesToWrite, nNumberOfBytesToRead);
    
	
	//if (k != 4)	win_printf("%d char read instead of 4",k);
	if (/*answer[0] > 0 && */answer[0] < 128)PhotonNumber = (float)((int)(answer[0]&0x7f) * 16777216 + (int)answer[1] * (256*256) + (int)answer[2] * 256 + (int)answer[3]);
/*		if (tst == OK)	tst = win_printf("%d x %g y %g %d",i,x[i],y[i],k);*/
		if (((int)answer[0])>128)
		{	
			reset_HV_PM();
			//win_printf("ReSalut");
			win_printf("Overflow detected, Voltage reset to 0V");
		}
		
		draw_bubble(screen,0,650,75,"Nphotons = %f ",PhotonNumber);

	return PhotonNumber;
}

int test_bouton(void)
{
    static int b1 = 1, b2 = 1, b3 = 1;
    
    if(updating_menu_state != 0)	return D_O_K;
	
    win_scanf("B1 %b\n ",&b1);
    
    win_scanf("B1 %b\n B2 %R B2B %r B3B %r\n B3 %b",&b1,&b2,&b3);
    win_printf ("B1 %d\n B2 %d\n B3 %d",b1,b2,b3);
    return 0;
    
}

         
MENU *RunSpectrum_menu(void) 
{
  static MENU mn[32];
  //extern int integration(void);
 
    
  if (mn[0].text != NULL)	return mn;
  
  
    add_item_to_menu(mn,"RunSpectrum", aquire_spectrum,NULL,0,NULL);
    add_item_to_menu(mn,"Integration Time", integration,NULL,0,NULL);
    add_item_to_menu(mn,"Plateau  Voltage ", set_plateau,NULL,0,NULL);
    add_item_to_menu(mn,"Reset Voltage", reset_voltage_PM,NULL,0,NULL);
    add_item_to_menu(mn,"Continuous reading", continuous_reading,NULL,0,NULL);
    add_item_to_menu(mn,"Set Lambda", Setlambda,NULL,0,NULL);
    add_item_to_menu(mn,"Check Lambda", Checklambda,NULL,0,NULL);
    //add_item_to_menu(mn,"Test bouton", test_bouton,NULL,0,NULL);
    
    
    return mn;
}

int	RunSpectrum_main(int argc, char **argv)
{
  //extern MENU *spectro_menu(void) ;
  //extern MENU *pm__menu(void)   ;
    
  add_plot_treat_menu_item("RunSpectrum", NULL, RunSpectrum_menu(), 0, NULL);
  //add_plot_treat_menu_item("PM HAMAMATSU", NULL, pm_menu(), 0, NULL);
  
  //add_plot_treat_menu_item("SPECTRO ACTON", NULL, spectro_menu(), 0, NULL);
  
  open_serial_port_Spectro();
  open_serial_port_PM();
  
  
  
  return D_O_K;
}

int	RunSpectrum_unload(int argc, char **argv)
{
	remove_item_to_menu(plot_treat_menu, "RunSpectrum", NULL, NULL);
	return D_O_K;
}
        




#endif
