#ifndef _SPECTRO_H_
#define _SPECTRO_H




/*definition des fonctions*/	


/*definition des variables globales*/
#ifdef _SPECTRO_C_
HANDLE hComSpectroPort = 0;

float ScanningSpeed = 100.0;
float Lambda = 0.0;
float Jogvalue = 50.00;
unsigned short Spectro_port_number = 3;


#else
PXV_VAR( float,ScanningSpeed);
PXV_VAR(float,Lambda);
PXV_VAR(float, Jogvalue);

PXV_VAR(unsigned short, Spectro_port_number); 
PXV_VAR(HANDLE, hComSpectroPort );

#endif


PXV_FUNC(int, 	SetScanningSpeed,(void));
PXV_FUNC(int, 	Setlambda,(void));
PXV_FUNC(int, 	Checklambda,(void));
PXV_FUNC(int, 	Scan,(void));
PXV_FUNC(int, 	SetJog, (void));
PXV_FUNC(int, Jog,(void));
PXV_FUNC(int, open_serial_port_Spectro,(void));
PXV_FUNC(int, close_serial_port_Spectro,(void));
PXV_FUNC(int,	SetScanSpeed,(float scanspeed));
PXV_FUNC(int,	CheckScanningSpeed,(float *scanspeed));
PXV_FUNC(int, Checkscanningspeed,(void));
PXV_FUNC(int, SetLambda,(float lambda));
PXV_FUNC(int, 	StartScan,(float scan_destination));
PXV_FUNC(int, 	JoG,(int direction));
PXV_FUNC(int,   Setjog,(float jogvalue));
PXV_FUNC(float, CheckLambda,(float *lambda));


#endif



	
			
	
	
       



