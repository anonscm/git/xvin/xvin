#ifndef _PM_H_
#define _PM_H

//HANDLE hCom_PM = 0;
//unsigned short pm_port_number = 1;


/*definition des fonctions*/	

PXV_FUNC(int, 	set_voltage,(void));
PXV_FUNC(int, 	set_edge,(void));
PXV_FUNC(int, 	set_level,(void));

PXV_FUNC(void, 	test_overflow,(void));
PXV_FUNC(int, 	numb_readings,(void));
PXV_FUNC(int, 	integration, (void));
PXV_FUNC(int, open_serial_port_PM,(void));
PXV_FUNC(int, close_serial_port_PM,(void));
PXV_FUNC(int, continuous_reading,(void));
PXV_FUNC(int,	set_PM_integration_time,(int periods));
PXV_FUNC(int,	set_the_number_of_integrations,(int readings));

PXV_FUNC(void,	reset_HV_PM,(void));
PXV_FUNC(int, SetVoltage,(int Voltage));
PXV_FUNC(int, check_answer_valid,(char *ch, char * answer));

PXV_FUNC(int, 	read_PM,(int npoints, float *y, float *x));
PXV_FUNC(int, 	set_plateau,(void));
PXV_FUNC(int,   reset_voltage_PM,(void));
PXV_FUNC(int,   set_PM_integration_time,(int periods));

/*definition des variables globales*/
#ifdef _PM_C_
int 	Voltage=0;
int 	periods=10;
int 	npoints_PM = 256;
int		readings =128;
HANDLE hCom_PM_port = 0;
unsigned short pm_port_number = 1; 


#else
PXV_VAR( int,Voltage);
PXV_VAR(int,periods);
PXV_VAR(int, npoints_PM);
PXV_VAR(int	,readings);
PXV_VAR(unsigned short, pm_port_number); 
PXV_VAR(HANDLE, hCom_PM_port );

#endif
#endif



	
			
	
	
       



