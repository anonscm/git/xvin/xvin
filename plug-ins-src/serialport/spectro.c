/*
*    Plug-in program for using spectro ACTON on serial port 
*
*   
*      JF Allemand
*/
#ifndef _SPECTRO_C_
#define _SPECTRO_C_


# include "allegro.h"
# include "winalleg.h"
# include <windowsx.h>
# include "ctype.h"
# include "xvin.h"

#define BUILDING_PLUGINS_DLL
# include "serialw32.h"
# include "spectro.h"


///mettre turret et grating

int close_serial_port_Spectro(void)
{
    if(updating_menu_state != 0)	return D_O_K;
	
    CloseSerialPort(hComSpectroPort);
    return 0;
}

int open_serial_port_Spectro(void)
{
    if(updating_menu_state != 0)	return D_O_K;
	
    win_scanf("Serial Port for the ACTON Spectro? %d",&Spectro_port_number);
    hComSpectroPort = init_serial_port(Spectro_port_number);
    return 0;
}
/******************************SCANNING SPEED******************************/
int SetScanSpeed(float scanspeed)
{
    char command[32],answer[256];
	DWORD nNumberOfBytesToRead = 255;
	DWORD NumberOfBytesToWrite;
	//unsigned long lpNumberOfBytesWritten= -1;
	
	sprintf(command,"%.1f nm/min\r",scanspeed);
	NumberOfBytesToWrite = strlen(command);
	
    
	talk_serial_port(hComSpectroPort, command, WRITE_READ, answer, NumberOfBytesToWrite, nNumberOfBytesToRead);
	CheckScanningSpeed(&ScanningSpeed);
	
    return D_O_K;
}

int CheckScanningSpeed(float *scanspeed)
{
    char command[32],answer[256],result[32];
    DWORD NumberOfBytesToWrite;
	DWORD nNumberOfBytesToRead = 255;
	//unsigned long lpNumberOfBytesWritten= -1;
	char *numberchar = "0123456789.";
    char * pch = NULL;
    
    
	sprintf(command,"?nm/min\r");
    NumberOfBytesToWrite = strlen(command);
	
    
	talk_serial_port(hComSpectroPort, command, WRITE_READ, answer, NumberOfBytesToWrite, nNumberOfBytesToRead);
//	win_printf("command %s\n answer %s",command,answer);
	
	
	pch = strpbrk (answer, numberchar);
    while (pch != NULL)
    {
          //win_printf ("%c " , *pch);
          strncat(result,pch,1);
          pch = strpbrk (pch+1,numberchar);
    }
	
//	win_printf("command %s\n answer %s \n result %s",command,answer,result);
	
	
	*scanspeed = (float) atof(result);
	win_printf("scanspeed = %f",*scanspeed);
    return D_O_K;
}
    
int Checkscanningspeed(void)
{
	if(updating_menu_state != 0)	return D_O_K;
	
	CheckScanningSpeed(&ScanningSpeed);
	return 0;
}

int SetScanningSpeed(void)
{
    
	
	int i;
	
	if(updating_menu_state != 0)	return D_O_K;
    
    i = win_scanf("What scanning speed do you want? %f",&ScanningSpeed);
    if (i == CANCEL) return 0;
    if (SetScanSpeed(ScanningSpeed) != D_O_K) return win_printf("Problem");
    
	
	//VERIFIER OK
	return D_O_K;
}
	
/************************************LAMBDA**********************************/
int SetLambda(float lambda)
{
    char command[32],answer[256];
	DWORD nNumberOfBytesToRead = 255;
	DWORD NumberOfBytesToWrite;
	//unsigned long lpNumberOfBytesWritten = -1;
	
	sprintf(command,"%.1f goto\r",lambda);
//	win_printf("lambda command %s",command);
	
	NumberOfBytesToWrite = strlen(command);

	talk_serial_port(hComSpectroPort, command, WRITE_READ, answer, NumberOfBytesToWrite, nNumberOfBytesToRead);
	CheckLambda(&Lambda);
	
    return D_O_K;
}

float CheckLambda(float *lambda)
{
    char command[32],answer[256],result[8]="\0";
    
    char *numberchar = "0123456789.";
    char * pch = NULL;
    DWORD NumberOfBytesToWrite;
	DWORD nNumberOfBytesToRead = 255;
	int begin = 0 , end = 0 ;
	//unsigned long lpNumberOfBytesWritten = -1;
	
	sprintf(command,"?nm\r");
    NumberOfBytesToWrite = strlen(command);
	talk_serial_port(hComSpectroPort, command, WRITE_READ, answer, NumberOfBytesToWrite, nNumberOfBytesToRead);

	
	//strspn ( const char * string1, const char * string2 );
//	strcspn ( const char * string1, const char * string2 );
    begin = strcspn ( answer, numberchar );
    end = strspn ( answer + begin, numberchar );
	strncat(result,answer+begin ,end);
//	pch = strpbrk (answer, numberchar);
//    while (pch != NULL)
//    {
//          //win_printf ("%c " , *pch);
//          strncat(result,pch,1);
//          pch = strpbrk (pch+1,numberchar);
//    }
	
	
	
	/*start_lambda_char = strcspn ( answer, "0123456789" );
	win_printf("answer %s\n longueur %d \n debut %d",answer,strlen (strchr(answer,'.')+5),start_lambda_char);
	if ((num_characters = strlen (strchr(answer,'.')+5)-start_lambda_char)>5) return win_printf("Problem in finding the value");
	strncpy(result,answer+start_lambda_char,num_characters);*/
	//win_printf("answer %s\n result %s",answer,result);
	*lambda = (float) atof(result);
	draw_bubble(screen,0,550,50,"Lambda = %f nm",*lambda);
	//win_printf("command %s\n answer %s\nLambda = %f\n begin %d \n end %d \n result %s",command,answer,*lambda,begin,end,result);
    return *lambda;
}
    
int Checklambda(void)
{
	if(updating_menu_state != 0)	return D_O_K;
	
	CheckLambda(&Lambda);
	return 0;
}

int Setlambda(void)
{
    
	
	int i;
	
	if(updating_menu_state != 0)	return D_O_K;
    
    i = win_scanf("What position do you want (in nm)? \n%f",&Lambda);
    if (i == CANCEL) return 0;
    if (SetLambda(Lambda) != D_O_K) return win_printf("Problem");
    
	
	//VERIFIER OK
	return D_O_K;
}
/*************************START SCANNING***************************/
int Scan(void)
{
    int i;
    float scan_destination = 500.0;
    
    if(updating_menu_state != 0)	return D_O_K;
    
    i = win_scanf("The present position is %1f\n You want to scan to which position (in nm)? %1f",CheckLambda(&Lambda),&scan_destination);
    if (i == CANCEL) return 0;
    StartScan(scan_destination);
    return 0;
}
  
int StartScan(float scan_destination)
{
    char command[32],answer[256];
    //int num_characters = -1;
    DWORD NumberOfBytesToWrite;
	DWORD nNumberOfBytesToRead = 255;
	//unsigned long lpNumberOfBytesWritten;
	
	sprintf(command,"%1f nm\r",scan_destination);
    NumberOfBytesToWrite = strlen(command);
	talk_serial_port(hComSpectroPort, command, WRITE_READ, answer, NumberOfBytesToWrite, nNumberOfBytesToRead);
	return 0;
}
/********************************************JOGGING**********************************/
/*int JoG(int direction)
{
      
      char command[32],answer[256],result[6];
      int num_characters = -1;
      DWORD nNumberOfBytesToRead = 255;
      //unsigned long lpNumberOfBytesWritten;
	
	if (direction > 0) sprintf(command,"jog-up\r");

	if (direction < 0) sprintf(command,"jog-dn\r");

	talk_serial_port(hComSpectroPort, command, WRITE_READ, answer, nNumberOfBytesToRead);
	return 0;
}
*/
int Jog(void)
{
  
    
    if(updating_menu_state != 0)	return D_O_K;
    
    JoG (RETRIEVE_MENU_INDEX) ;
    return D_O_K;
}

int JoG(int direction)
{
      
      char command[32],answer[256];
    //  int num_characters = -1;
    DWORD NumberOfBytesToWrite;
      DWORD nNumberOfBytesToRead = 255;
  //    unsigned long lpNumberOfBytesWritten;
  
//  win_printf("direction %d",direction);
	
	if (direction == 1) sprintf(command,"jog-up\r");

	if (direction == 2) sprintf(command,"jog-dn\r");
    NumberOfBytesToWrite = strlen(command);
	talk_serial_port(hComSpectroPort, command, WRITE_READ, answer, NumberOfBytesToWrite, nNumberOfBytesToRead);
	return 0;
}

int SetJog(void)
{
    int i;
//    float jog = 50.00;
    
    if(updating_menu_state != 0)	return D_O_K;
    
    i = win_scanf("Step wanted (in nm)%f",&Jogvalue);
    if (i == CANCEL) return 0;
    Setjog(Jogvalue);
    return D_O_K;
}
int Setjog(float jogvalue)
{
    char command[32],answer[256];
    DWORD nNumberOfBytesToRead = 255;
    DWORD NumberOfBytesToWrite;
    
    sprintf(command,"%.2f nm/jog\r",jogvalue);
    //win_printf("%s", command);
      
    NumberOfBytesToWrite = strlen(command);
	talk_serial_port(hComSpectroPort, command, WRITE_READ, answer, NumberOfBytesToWrite, nNumberOfBytesToRead);
	
    return 0;
}

/***************************MENU*******************************************************/
MENU *spectro_menu(void) 
{
  static MENU mn[32];
 
    
  if (mn[0].text != NULL)	return mn;
  
  
    add_item_to_menu(mn,"Scanning speed", SetScanningSpeed,NULL,0,NULL);
    add_item_to_menu(mn,"Set Scanning", Checkscanningspeed,NULL,0,NULL);
    add_item_to_menu(mn,"Set Lambda", Setlambda,NULL,0,NULL);
    add_item_to_menu(mn,"Check Lambda", Checklambda,NULL,0,NULL);
    add_item_to_menu(mn,"Start scanning", Scan,NULL,0,NULL);
    add_item_to_menu(mn,"Set jogging step", SetJog,NULL,0,NULL);
    add_item_to_menu(mn,"Jog up", Jog,NULL,MENU_INDEX(1),NULL);
    add_item_to_menu(mn,"Jog down", Jog,NULL,MENU_INDEX(2),NULL);
    
    add_item_to_menu(mn,"Open serial port", open_serial_port_Spectro,NULL,0,NULL);
    add_item_to_menu(mn,"Close serial port", close_serial_port_Spectro,NULL,0,NULL);
    
    
    return mn;
}


int	spectro_main(int argc, char **argv)
{
    
  add_plot_treat_menu_item("SPECTRO ACTON", NULL, spectro_menu(), 0, NULL);
  
  open_serial_port_Spectro();
  
  
  
  return D_O_K;
}

int	pm_unload(int argc, char **argv)
{
	remove_item_to_menu(plot_treat_menu, "SPECTRO ACTON", NULL, NULL);
	return D_O_K;
}
#endif

