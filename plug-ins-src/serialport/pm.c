#ifndef _PM_C_
#define _PM_C_

# include "allegro.h"
# include "winalleg.h"
# include <windowsx.h>
# include "ctype.h"
# include "xvin.h"

#define BUILDING_PLUGINS_DLL

# include "serialw32.h"
# include "pm.h"






/*****************GENERAL COMMAND*****************************************/
int check_answer_valid(char *ch, char * answer)
{
        //win_printf("answer %s",answer);
        if (strlen(answer) < 2) return win_printf("Can not be valid");   
		
        if ((answer[0] !='V' ) || (answer[1]!='A')) 
        {
			win_printf("error in setting %s parameter \%s",ch,answer);
			return -1;
		}
		return D_O_K;

}

/*************************************SET HIGH VOLTAGE*****************************/
int SetVoltage(int voltage)
{	
    char command[5],answer[32];
	DWORD nNumberOfBytesToRead = 32;
	DWORD NumberOfBytesToWrite = 4;
	
    //win_printf("voltage %d\n voltage / 256 %d \n (voltage %  256) %d",voltage,voltage / 256,(voltage %  256));
	
	command[0]='V';
	command[1]= (unsigned char) (voltage / 256) ;             
	command[2] = (unsigned char)  (voltage %  256)  ;            
	command[3]='\r';
	/*command[0]=0x56;
	command[1]=0x00;
	command[2]=0x00;
	command[3]=0x0D;*/
	//win_printf("Command %s\n strlen(command) = %d ",command,strlen(command));
	talk_serial_port(hCom_PM_port, command, WRITE_READ, answer, NumberOfBytesToWrite, nNumberOfBytesToRead);
	check_answer_valid(command,answer);
    
    return 0;
}   
    
    
    
    
int set_voltage(void)
{
		int i;
		

		if(updating_menu_state != 0)	return D_O_K;
    

        i=win_scanf( "Enter Desired Value of High Voltage (0 - 1200):%d\n",&Voltage);
		if (Voltage > 1200 || Voltage < 0)
		{
          win_printf("Play it again!");
          return 0;
		}
		
		i=win_printf("Are you sure that you want to set the Voltage to %d !",Voltage);         
		if (i == CANCEL)	return 0;
		
        SetVoltage(Voltage);	
		
        return 0;
}


/********************************RESET THE VOLTAGE TO 0 FOR SAFETY**********************************/
 
void reset_HV_PM(void)
{
    Voltage = 0;  
	SetVoltage(Voltage);
	win_printf("Voltage set to %dV ", Voltage);
 	  
}

/**************************Set Number of readings *************************/

int	set_the_number_of_integrations(int readings)
{	
	char command[3],answer[32];
	DWORD nNumberOfBytesToRead = 32;
	DWORD NumberOfBytesToWrite = 3;
	
	//sprintf(command,"R%d",readings);	
	command[0]='R' ;
	command[1]=(unsigned char)readings;
	strcat(command,"\r");
    
 
    win_printf("command %s",command);
	talk_serial_port(hCom_PM_port, command, WRITE_READ, answer, NumberOfBytesToWrite, nNumberOfBytesToRead);
	//win_printf("answer %s",answer);
	if (check_answer_valid(command,answer)!= -1)    npoints_PM=readings;
    
	
	return 0;
}

int numb_readings(void)
{
	int i;

	if(updating_menu_state != 0)	return D_O_K;
    i=win_scanf( "Number of readings desired (1--255):%d\n ",&readings);
			
	if (i == CANCEL)	return 0;
 	if (readings > 255 || readings < 1)
	{
		win_printf("Play it again!");
		return 0;
	}
	set_the_number_of_integrations(readings);
    return 0;                             
}  


           


/**************************Set Number of intergation periods (10ms)*************************/
int	set_PM_integration_time(int periods)
{
	char command[3],answer[32];
	DWORD nNumberOfBytesToRead = 32;
	DWORD NumberOfBytesToWrite = 3;
	

    command[0]='P'; 
	command[1]= (unsigned char)periods;
	strcat(command,"\r");
	
	talk_serial_port(hCom_PM_port, command, WRITE_READ, answer, NumberOfBytesToWrite, nNumberOfBytesToRead);
	check_answer_valid(command,answer);
	return 0;		
}
    

int integration (void)
{	   	
	int i;
	
	
		
	if(updating_menu_state != 0)	return D_O_K;
    
	i=win_scanf( "number of periods of 10ms desired:%d\n e.g. 100 Periods Is Equivalent to 1-sec Integration Time ",&periods);
			
	if (i == CANCEL)	return 0;
			
	if (periods > 100 || periods < 1)
	{
		win_printf("Play it again!");
		return 0;
	}
	
    set_PM_integration_time(periods);
   	
    return D_O_K;	
    
}   

/*********************************TEST IF OVERFLOW DETECTED***********************************/

       
void test_overflow(void)
{
	unsigned char *command="S\r",answer[5];
	DWORD nNumberOfBytesToRead = 4;
	DWORD NumberOfBytesToWrite = 2;
	

	set_the_number_of_integrations(1);
		
	
	
    talk_serial_port(hCom_PM_port, command, WRITE_READ, answer, NumberOfBytesToWrite, nNumberOfBytesToRead);
	check_answer_valid(command,answer);
		
		if ((int)(answer[0]) >= 128) 
			{
				reset_HV_PM();
        		win_printf(" OVERFLOW !!!\n PM VOLTAGE RESET TO 0V ");
				return;
				
	  
			}
		set_the_number_of_integrations(readings);	
		
		
		return;
}

     

int read_PM(int npoints,float *y, float *x)
{
	unsigned char *command="S\r",answer[5];
	static int i, k;
	DWORD nNumberOfBytesToRead = 4;
	DWORD NumberOfBytesToWrite = 2;
	
	
	
	talk_serial_port(hCom_PM_port, command, WRITE_ONLY, NULL, NumberOfBytesToWrite, nNumberOfBytesToRead);
	
	
	
    for (i = 0; i < npoints; i++)
	{		
	    
		k = talk_serial_port(hCom_PM_port, NULL, READ_ONLY, answer, NumberOfBytesToWrite, nNumberOfBytesToRead);
    
	
		if (k != 4)	win_printf("%d char read instead of 4",k);
		x[i] = i;
   		y[i] = (float)((int)(answer[0]&0x7f) * 16777216 + (int)answer[1] * (256*256) + (int)answer[2] * 256 + (int)answer[3]);
/*		if (tst == OK)	tst = win_printf("%d x %g y %g %d",i,x[i],y[i],k);*/
		if (((int)answer[0])>128)
		{	
			reset_HV_PM();
			//win_printf("ReSalut");
			win_printf("Overflow detected, Voltage reset to 0V");
		}
	}
	return 0;
}


/**********************SET THE trigger mode to level mode **********************************/

int set_level(void)
{
	unsigned char *command="L\r";
	DWORD nNumberOfBytesToRead = 0;
	DWORD NumberOfBytesToWrite = 2;
	int i;
	
			if(updating_menu_state != 0)	return D_O_K;
    	
			i=win_printf("You will read while the trigger signal is high");
			if (i == CANCEL)	return 0;		
			
			
			talk_serial_port(hCom_PM_port, command, WRITE_ONLY, NULL, NumberOfBytesToWrite, nNumberOfBytesToRead);
			return 0;
        
}

/**********************SET THE trigger mode to edge mode **********************************/

int set_edge(void)
{
	unsigned char *command="E\r";
	DWORD nNumberOfBytesToRead = 0;
	DWORD NumberOfBytesToWrite = 2;
	int i;
	
			if(updating_menu_state != 0)	return D_O_K;
    
            i=win_printf("You will read a number of readings when the trigger signal will reach high");		
			if (i == CANCEL)	return 0;	
			
			talk_serial_port(hCom_PM_port, command, WRITE_ONLY, NULL, NumberOfBytesToWrite, nNumberOfBytesToRead);
			return 0;
        
}

/**********************SET THE VOLTAGE TO THE PLATEAU **********************************/

int set_plateau(void)
{
	char *command = "D\r",answer[3];
	DWORD nNumberOfBytesToRead = 2;
	DWORD NumberOfBytesToWrite = 2;
	int i;
	
	if(updating_menu_state != 0)	return D_O_K;
			
    i=win_printf("You will set High voltage to plateau value!");	
	if (i == CANCEL)	return D_O_K;	
			
	//sprintf(command,"D\r");			
	
	talk_serial_port(hCom_PM_port, command, WRITE_READ, answer, NumberOfBytesToWrite, nNumberOfBytesToRead);
	//check_answer_valid(command,answer);
			
	//test_overflow();    A REMTTRE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	return 0;
       
}

int continuous_reading(void)
{
    unsigned char *command="C\r",answer[3];
	DWORD nNumberOfBytesToRead = 4;
	DWORD NumberOfBytesToWrite = 2;
	int k;
	float Nphotons = 0;
	
	if(updating_menu_state != 0)	return D_O_K;
			
    		
	//sprintf(command,"C\r");
    
    talk_serial_port(hCom_PM_port, command, WRITE_ONLY, NULL, NumberOfBytesToWrite, nNumberOfBytesToRead);
    
    while (keypressed() != TRUE)//tester delai
    {
            k = talk_serial_port(hCom_PM_port, command, READ_ONLY, answer, NumberOfBytesToWrite, nNumberOfBytesToRead);
    
	
		    //if (k != 4)	win_printf("%d char read instead of 4",k);
		
  		    Nphotons = (float)((int)(answer[0]&0x7f) * 16777216 + (int)answer[1] * (256*256) + (int)answer[2] * 256 + (int)answer[3]);
  			if (((int)answer[0])>128)
  			{	
                         
                         talk_serial_port(hCom_PM_port, "\r", WRITE_ONLY, NULL, 1, nNumberOfBytesToRead);
                         reset_HV_PM();
                         return 0;
			//win_printf("ReSalut");
			            win_printf("Overflow detected, Voltage reset to 0V");
            }
            draw_bubble(screen,0,550,75,"Nphotons = %f in %d ms",Nphotons, periods*10);
    }    

            
    //sprintf(command,"\r");	

	talk_serial_port(hCom_PM_port, "\r"/*command*/, WRITE_ONLY, NULL, 1, nNumberOfBytesToRead);
    

    return 0;
    

}
int close_serial_port_PM(void)
{
    if(updating_menu_state != 0)	return D_O_K;
	
    CloseSerialPort(hCom_PM_port);
    return 0;
}

int open_serial_port_PM(void)
{
    if(updating_menu_state != 0)	return D_O_K;
	
    win_scanf("Serial Port for the PM? %d",&pm_port_number);
    hCom_PM_port = init_serial_port(pm_port_number);
    //win_printf("hCom_PM_port %d",hCom_PM_port);
    return 0;
}

int reset_voltage_PM(void)
{
    if(updating_menu_state != 0)	return D_O_K;
	
    reset_HV_PM();
    return 0;
}             
/***************************************************************************************/

MENU *pm_menu(void) 
{
  static MENU mn[32];
 
    
  if (mn[0].text != NULL)	return mn;
  
  
    add_item_to_menu(mn,"Integration Time", integration,NULL,0,NULL);
    add_item_to_menu(mn,"Number of Readings", numb_readings,NULL,0,NULL);
    add_item_to_menu(mn,"Plateau  Voltage ", set_plateau,NULL,0,NULL);
    add_item_to_menu(mn,"Reset Voltage", reset_voltage_PM,NULL,0,NULL);
    add_item_to_menu(mn,"Continuous reading", continuous_reading,NULL,0,NULL);
    add_item_to_menu(mn,"Set Voltage", set_voltage,NULL,0,NULL);
    add_item_to_menu(mn,"Open serial port for PM", open_serial_port_PM,NULL,0,NULL);
    add_item_to_menu(mn,"Close serial port for PM", close_serial_port_PM,NULL,0,NULL);
    
    
    return mn;
}



int	pm_main(int argc, char **argv)
{
    
  add_plot_treat_menu_item("PM HAMAMATSU", NULL, pm_menu(), 0, NULL);
  
  open_serial_port_PM();
  
  //set_PM_integration_time(periods);
  //set_the_number_of_integrations(readings);	
  //set_plateau();	
  
  atexit(reset_HV_PM);
  return D_O_K;
}

int	pm_unload(int argc, char **argv)
{
	remove_item_to_menu(plot_treat_menu, "PM HAMAMATSU", NULL, NULL);
	return D_O_K;
}
#endif
