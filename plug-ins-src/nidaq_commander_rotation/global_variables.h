/** Global variables declarations for the plug-in

This file contains the declaration of all global variables in the plug-in, 
sorted by the .c file where they appear.
This allows all these variables to be used in any .c file in the plug-in 
as long as <<#define global_variables.h>> is declared in the .c file.

\author Adrien Meglio
*/

#ifndef GLOBAL_VARIABLES_H_
#define GLOBAL_VARIABLES_H_

// From NIDAQ.C
int CURRENT_MODE;
int CURRENT_LINE; 

// From menu_plug_in.c

//MENU dynamic_submenu[32];
//MENU switch_on_submenu[32];
//MENU switch_off_submenu[32];


/* #ifndef NIDAQ_COMMANDER_ROTATION_C_ */
/* // From PIFOC_functions.c */
/* PXV_VAR(float, PIFOC_POSITION); */
/* PXV_VAR(float, PIFOC_FINESSE_MICRONS); */
/* // From AOTF_NIDAQ_base.c */
/* PXV_VAR(float64, g_frequency) ; */
/* PXV_VAR(int, g_num_points) ; */
/* PXV_VAR(float, g_level) ; */
/* PXV_VAR(l_s*, lines); */
/* PXV_VAR(TaskHandle, CURRENTtaskHandle); */
/* // From AOTF_functions.c */
/* PXV_VAR(pltreg*, ptrg); */
/* PXV_VAR(int, DURATION); */
/* PXV_VAR(int, AOTF_FINESSE); */
/* #else */
float PIFOC_POSITION; /* The percentage of the total course (between 0 and 1) */float PIFOC_FINESSE_MICRONS; /* The finesse of the displacement in microns */
float PIFOC_FINESSE_MICRONS;
float64 g_frequency ; // The rotation frequency
int g_num_points ; // The resolution of the curve (points per period)
float g_level ; // The level or amplitude (gives the force and the direction)
l_s* lines;
TaskHandle CURRENTtaskHandle;

pltreg* ptrg;
int DURATION;
int AOTF_FINESSE;

float EPSILON_FINESSE;
/* #endif */

// From ROTATION_functions.c

//r_s rotation;
//FILE *rotation_fp;

#endif
