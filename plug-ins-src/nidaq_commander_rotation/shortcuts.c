/** \file shortcuts.c
    
    These functions allow shortcut control through keyboard.
    
    \author Adrien Meglio
    
    \version 26/01/07
*/
#ifndef SHORTCUTS_C_
#define SHORTCUTS_C_

#include <allegro.h>
#include <winalleg.h>
#include <xvin.h>

/* If you include other plug-ins header do it here*/ 
#include <NIDAQmx.h>
#include "../logfile/logfile.h"
#include "nidaq_commander_rotation.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL

extern rfp grf;

int display_values_to_screen(void)
{
  int v = 150;
  int o = 50;
  if(updating_menu_state != 0)  return D_O_K;
  draw_bubble(screen,0,o+800, v+40,"Rotation Parameters Setting:");
  draw_bubble(screen,0,o+800, v+60,"rf.num turns: %d" , grf.num_turns);  
  draw_bubble(screen,0,o+800, v+80,"rf.direction: %d" , grf.direction);  
  draw_bubble(screen,0,o+800, v+100,"rf.mean value: %d",	grf.meanvalue);  
  draw_bubble(screen,0,o+800, v+120,"rf.frequency %6f",	grf.frequency); 
  draw_bubble(screen,0,o+800, v+140,"rf.num points: %6d",grf.num_points);       
  draw_bubble(screen,0,o+800, v+160,"rf.level : %6f" ,	grf.level);
  draw_bubble(screen,0,o+800, v+180,"Phase = %4d/%4d*PI",	grf.phasenum,	grf.phaseden);	  
  draw_bubble(screen,0,o+800, v+200,"stepped? %d",	grf.stepflag);	  
  draw_bubble(screen,0,o+800, v+220,"rf.epsilon %6f",	grf.epsilon);
  draw_bubble(screen,0,o+800, v+240,"rf.N_1 %6d, rf.N_2 %6d",	grf.N_1,grf.N_2 );
  draw_bubble(screen,0,o+800, v+260,"rf.switch on[0] %d",grf.switch_on[0]);
  draw_bubble(screen,0,o+800, v+280,"rf.switch on[1] %d",grf.switch_on[1]);
  draw_bubble(screen,0,o+800, v+300,"rf.switch on[2] %d",grf.switch_on[2]) ;
  draw_bubble(screen,0,o+800, v+320,"CURRENT_MODE = %d",CURRENT_MODE);
  draw_bubble(screen,0,o+800, v+340,"PIFOC_POSITION = %f",PIFOC_POSITION);
  return D_O_K;
}


//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
// Keyboard control functions

/** Defines a shortcut for key KEY_PLUS_PAD.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_PLUS_PAD(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_PLUS_PAD, 0, short__KEY_PLUS_PAD);
        return D_O_K; 
    }
     
    if (CURRENT_MODE == PIFOC_MODE)
    {   
        PIFOC__up();
    }
    else if (CURRENT_MODE == LINE_MODE)
    {
        AOTF__set_LINE_intensity(CURRENT_LINE,lines[CURRENT_LINE].level+AOTF_FINESSE);
    }
    return display_values_to_screen();
}

/** Defines a shortcut for key KEY_MINUS_PAD.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_MINUS_PAD(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_MINUS_PAD, 0, short__KEY_MINUS_PAD);
        return D_O_K;
    }
     
    if (CURRENT_MODE == PIFOC_MODE)
    {   
        PIFOC__down();
    }
    else if (CURRENT_MODE == LINE_MODE)
    {
        AOTF__set_LINE_intensity(CURRENT_LINE,lines[CURRENT_LINE].level-AOTF_FINESSE);
    }
    
    return display_values_to_screen();
}

/** Defines a shortcut for key KEY_P.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_P(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_P, 0, short__KEY_P);
        return D_O_K;
    }
    
    CURRENT_MODE = PIFOC_MODE;
        
    return display_values_to_screen();
}

/** Defines a shortcut for key KEY_L.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_L(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_L, 0, short__KEY_L);
        return D_O_K;
    }
    
    CURRENT_MODE = LINE_MODE;
        
    return display_values_to_screen();
}

/** Defines a shortcut for key KEY_R.

\author Francesco Mosconi
\version 26/01/07
*/
int short__KEY_R(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_R, 0, short__KEY_R);
        return D_O_K;
    }
    
    CURRENT_MODE = ROTATION_MODE;
        
    return display_values_to_screen();
}


/** Defines a shortcut for key KEY_1_PAD.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_1_PAD(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_1_PAD, 0, short__KEY_1_PAD);
        return D_O_K;
    }
    
    if (CURRENT_MODE == LINE_MODE)
    {
        CURRENT_LINE = LINE_1;
    }
    
    return display_values_to_screen();
}

/** Defines a shortcut for key KEY_2_PAD.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_2_PAD(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_2_PAD, 0, short__KEY_2_PAD);
        return D_O_K;
    }
    
    if (CURRENT_MODE == LINE_MODE)
    {
        CURRENT_LINE = LINE_2;
    }
    
    return display_values_to_screen();
}
/** Defines a shortcut for key KEY_3_PAD.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_3_PAD(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_3_PAD, 0, short__KEY_3_PAD);
        return D_O_K;
    }
    
    if (CURRENT_MODE == LINE_MODE)
    {
        CURRENT_LINE = LINE_3;
    }
    
    return display_values_to_screen();
}


/** Defines a shortcut for key KEY_1_PAD.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_4_PAD(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_4_PAD, 0, short__KEY_4_PAD);
        return D_O_K;
    }
    
    if (CURRENT_MODE == ROTATION_MODE)
    {
      grf.epsilon = grf.epsilon + EPSILON_FINESSE;
      AOTF__rotating_B_all_fixlink_gparam_rf(&grf);
    }
     
    return display_values_to_screen();
}

/** Defines a shortcut for key KEY_7_PAD.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_7_PAD(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_7_PAD, 0, short__KEY_7_PAD);
        return D_O_K;
    }
     if (CURRENT_MODE == ROTATION_MODE)
    {
      grf.epsilon = grf.epsilon - EPSILON_FINESSE;
      AOTF__rotating_B_all_fixlink_gparam_rf(&grf);
    }
    
    return display_values_to_screen();
}

/** Defines a shortcut for key KEY_5_PAD.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_5_PAD(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_5_PAD, 0, short__KEY_5_PAD);
        return D_O_K;
    }
    
    else if (CURRENT_MODE == ROTATION_MODE)
    {   
      grf.phasenum = grf.phasenum + 1;
      AOTF__rotating_B_all_fixlink_gparam_rf(&grf);
    }
    
    return display_values_to_screen();
}

/** Defines a shortcut for key KEY_8_PAD.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_8_PAD(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_8_PAD, 0, short__KEY_8_PAD);
        return D_O_K;
    }
    
    else if (CURRENT_MODE == ROTATION_MODE)
    {   
      grf.phasenum = grf.phasenum - 1;
      AOTF__rotating_B_all_fixlink_gparam_rf(&grf);
    }
    
    return display_values_to_screen();
}

/** Defines a shortcut for key KEY_5_PAD.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_6_PAD(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_6_PAD, 0, short__KEY_6_PAD);
        return D_O_K;
    }
    
    else if (CURRENT_MODE == ROTATION_MODE)
    {   
      grf.N_1 = grf.N_1 + 1;
      grf.N_2 = grf.N_2 + 1;
      AOTF__rotating_B_all_fixlink_gparam_rf(&grf);
    }
    
    return display_values_to_screen();
}

/** Defines a shortcut for key KEY_6_PAD.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_9_PAD(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_9_PAD, 0, short__KEY_9_PAD);
        return D_O_K;
    }
    
    else if (CURRENT_MODE == ROTATION_MODE)
    {   
      grf.N_1 = (grf.N_1 - 1 > 0) ? grf.N_1 - 1 : grf.N_1;
      grf.N_2 = (grf.N_2 - 1 > 0) ? grf.N_2 - 1 : grf.N_2;
      AOTF__rotating_B_all_fixlink_gparam_rf(&grf);
    }
    
    return display_values_to_screen();
}

/** Defines a shortcut for key KEY_G.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_G(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_G, 0, short__KEY_G);
        return D_O_K;
    }
     
    if (CURRENT_MODE == PIFOC_MODE)
    {   
        PIFOC__set_MICRONS();
    }
    else if (CURRENT_MODE == LINE_MODE)
    {
        AOTF__get_LINE_intensity();
    }
    
    return display_values_to_screen();
}

/** Defines a shortcut for key KEY_O.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_O(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_O, 0, short__KEY_O);
        return D_O_K;
    }
    
    if (CURRENT_MODE == LINE_MODE)
    {
        AOTF__switch_LINE();
    }
    
    return display_values_to_screen();
}

/** Defines a shortcut for key KEY_F.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_F(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_F, 0, short__KEY_F);
        return D_O_K;
    }
     
    if (CURRENT_MODE == PIFOC_MODE)
    {   
        PIFOC_FINESSE_MICRONS /= 10;
    }
    else if (CURRENT_MODE == LINE_MODE)
    {   
        AOTF_FINESSE = 0.01;
    }
    else if (CURRENT_MODE == ROTATION_MODE)
    {   
        EPSILON_FINESSE /= 10;
    }
    
    return display_values_to_screen();
}

/** Defines a shortcut for key KEY_C.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_C(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_C, 0, short__KEY_C);
        return D_O_K;
    }
     
    if (CURRENT_MODE == PIFOC_MODE)
    {   
        PIFOC_FINESSE_MICRONS *= 10;
    }
    else if (CURRENT_MODE == LINE_MODE)
    {   
        AOTF_FINESSE = 0.1;
    }
    else if (CURRENT_MODE == ROTATION_MODE)
    {   
        EPSILON_FINESSE *= 10;
    }
    
    return display_values_to_screen();
}
/** Defines a shortcut for key KEY_0_PAD.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_0_PAD(void)
{
  if(updating_menu_state != 0)      
    {
      add_keyboard_short_cut(0, KEY_0_PAD, 0, short__KEY_0_PAD);
      return D_O_K;
    }
  return display_values_to_screen();
}

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
// Base functions

/** Main function of shortcuts capabilities

\author Adrien Meglio
\version 15/11/06
*/
int	shortcuts_main(void)
{
    if(updating_menu_state != 0) return D_O_K;
    
    AOTF_FINESSE = 0.1;
    EPSILON_FINESSE = 0.01;
    CURRENT_MODE = PIFOC_MODE;
    
    return D_O_K;
}

#endif



