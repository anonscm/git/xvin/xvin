/** Define and typedef declarations for the plug-in

This file contains the declaration of all define and typedef in the plug-in, 
sorted by the .c file where they appear.
This allows all these define to be used in any .c file in the plug-in 
as long as <<#define global_define.h>> is declared in the .c file.

\author Adrien Meglio
*/

#ifndef GLOBAL_DEFINE_H_
#define GLOBAL_DEFINE_H_

//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
// DEFINEs

// From AOTF_NIDAQ_base.c

#define PIFOC_MODE 1
#define LINE_MODE 2
#define ROTATION_MODE 3

// From NIDAQ_functions.c

#define AOTF_MAX_VOLT 10.0 // Max possible voltage
/* ATTENTION : Check that figure !!! */

#define PLAY_FOREVER DAQmx_Val_ContSamps
#define PLAY_ONCE DAQmx_Val_FiniteSamps


#define ON 1
#define OFF 0

// From PIFOC_functions.c

#define NUMBER_OF_AO_LINES 3
#define PIFOC_LINE LINE_4
#define PIFOC_MAX_VOLT 10.0 // Max possible voltage
#define PIFOC_MIN_VOLT 0.0 // Min possible voltage
#define PIFOC_TOTAL_COURSE 10.0 // The percentage-of-course to input-volts multiplication factor
#define PIFOC_COURSE_MICRONS 100 // The total PIFOC displacement range

// From ROTATION_functions.c

#define BOBINES 6

//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////

typedef struct ROTATION_STATUS
{
  float a; // Percentage of max value (1 is maximum)
  int p; // Number of data points (usually its a power of 2)
  int f; // Finesse or number of angular positions (use 6 for the moment)
  int t; //Number of turns to execute
  int r; //Rotation reference (current rotation position);
  float w; // angular frequency (periods per second)
} r_s ;

typedef struct rotating_field_parameters
{
  int num_turns; //number of periods the whole sequence must be repeated
  int direction; //direction of rotation (only used in continous rotation)
  int meanvalue; //mean value for oscillating amplitude and fixed direction field
  float frequency; //frequency (in turns per second)
  int num_points; //spatial frequency (points per turn)
  float level; //field amplitude
  int phasenum; //phase numerator
  int phaseden; //phase denominator
  int stepflag; //flag for stepped motion
  float epsilon; //epsilon parameter for doing a bit more than a turn
  int N_1; //number of turns in one direction
  int N_2; //number of turns in opposite direction
  int switch_on[3]; //switch that tells which coils to switch on
} rfp;




#endif
