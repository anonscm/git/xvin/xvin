/** \file AOTF_functions.c
    \brief Sub-plug-in program for AOTF control with NIDAQ-M 6229 in Xvin.
    
    Controls function generator for different AOTF control situations.
    BEWARE : requests the NIDAQ plugin !
    
    \sa NIDAQ.c
    \author Adrien Meglio
    \version 01/11/2006
*/
#ifndef AOTF_FUNCTIONS_C_
#define AOTF_FUNCTIONS_C_

#include <allegro.h>
#include <winalleg.h>
#include <xvin.h>

/* If you include other plug-ins header do it here*/ 
#include <NIDAQmx.h>
#include "../logfile/logfile.h"
#include "nidaq_commander_rotation.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL


rfp grf ;
//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
//rfp structure
int init_rfp(rfp *rf)
{
  rf->num_turns = 0;
  rf->direction = 0;
  rf->meanvalue = 0;
  rf->frequency = 0.1;
  rf->num_points = 256;
  rf->level = 0.1;
  rf->phasenum = 0;
  rf->phaseden = 1;
  rf->stepflag = 0;
  rf->epsilon = 0.;
  rf->N_1 = 2;
  rf->N_2 = 2;
  rf->switch_on[0] = 1;
  rf->switch_on[1] = 1;
  rf->switch_on[2] = 1;
  
  return 0;;
}

int set_rfp(rfp *rf)
{
  int i=0;
  if(win_scanf("Rotation Parameters Setting:\n"
	       "rf->num turns: %R infinite %r one\n"
	       "rf->direction: %R CCW %r CW\n"
	       "rf->mean value(only useful for alternating): %R 0 %r 1/2\n"
	       "rf->frequency (periods per s): %6f\n"
	       "rf->num points: %6d\n"
	       "rf->level [-0.3:0.3]: %6f\n"
	       "Phase = %4d/%4d*PI\n"
	       "stepped? %R NO %r YES %r new apolisation\n"
	       "rf->epsilon %6f\n"
	       "rf->N_1 %6d, rf->N_2 %6d\n"
	       "rf->switch on[0] %R OFF %r ON\n"
	       "rf->switch on[1] %R OFF %r ON\n"
	       "rf->switch on[2] %R OFF %r ON\n",
	       &(rf->num_turns),
	       &(rf->direction),
	       &(rf->meanvalue),
	       &(rf->frequency),
	       &(rf->num_points),
	       &(rf->level),
	       &(rf->phasenum),
	       &(rf->phaseden),
	       &(rf->stepflag),
	       &(rf->epsilon),
	       &(rf->N_1),
	       &(rf->N_2),
	       &(rf->switch_on[0]),
	       &(rf->switch_on[1]),
	       &(rf->switch_on[2])) == CANCEL) return CANCEL;
  if((i = check_rfp(rf))!=0) {
    win_printf("something is wrong!\ncheck rf returned %d",i);
    i =CANCEL;
  }
  return i;
}

int check_rfp(rfp *rf)
{
  int ret = 0;
  float max_frequency =0;
  if(rf->num_turns < 0) return ret = -1;
  if(rf->direction != 0 && rf->direction != 1) return ret = -2;
  if(rf->meanvalue != 0 && rf->meanvalue != 1) return ret = -22;
  if(rf->frequency < 0) return ret = -3;
  if(rf->num_points < 0) return ret = -4;
  if(rf->level > 0.3 && rf->level < -0.3) return ret = -5;
  if(rf->stepflag != 0 && rf->stepflag != 1 && rf->stepflag !=2) return ret = -23;
  if(rf->N_1 < 0 || rf->N_2 < 0) return ret = -24;
  if(rf->switch_on[0] != 0 && rf->switch_on[0] != 1) return ret = -6;
  if(rf->switch_on[1] != 0 && rf->switch_on[1] != 1) return ret = -7;
  if(rf->switch_on[2] != 0 && rf->switch_on[2] != 1) return ret = -8;
  IO__NIDAQ_max_frequency(&max_frequency);
  if(rf->frequency*rf->num_points*NUMBER_OF_AO_LINES > max_frequency) return ret = -9;
  return ret;
}


//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
// Control sequences

/*
NOTE : The configuration is such that the user may change the frequency independently of the resolution. 
The user creates a 'lines' structure with a given number of points per period. It is the resolution of the signal. 
Then the user can change the signal frequency via the #define FREQUENCY.
*/

/** Varying magnetic field. Not rotating!

All lines are sinus-modulated, but field direction is kept constant.

\author Francesco Mosconi
\version 12/02/2007
*/
int AOTF__alternating_B_all_interface(void)
{
  int ret = 0;
  if(updating_menu_state != 0)	return D_O_K;
  if((ret = set_rfp(&grf)) != 0) return win_printf("ret = %d", ret);
  return AOTF__alternating_B_all_gparam_rf(&grf);
}

int AOTF__alternating_B_all_gparam_rf(rfp *rf)
{
  g_frequency = (float64) rf->frequency;
  g_level = rf->level;
  g_num_points = rf->num_points;
  return AOTF__alternating_B_all(rf->num_turns, rf->meanvalue, rf->phasenum, rf->phaseden, rf->switch_on);

}

int AOTF__alternating_B_all(int num_turns,int  meanvalue,int phasenum,int phaseden,int* switch_on)
{
    int i,j;

    if (lines == NULL) 
    { 
        lines__create_lines_structure();
        dump_to_xv_log_file_with_date_and_time("lines structure was re-created");
    }
    
    /* Creates the data sequence for each line*/ 
    for (j = 0 ; j < NUMBER_OF_AO_LINES ; j++)
    {
        lines[j].level = g_level;
        lines[j].status = switch_on[j];
        
        lines[j].num_points = g_num_points;
                
        lines[j].data = (float64*) realloc(lines[j].data,lines[j].num_points*sizeof(float64));
        if (lines[j].data == NULL) return dump_to_xv_log_file_with_date_and_time("memory allocation error");
        
        for (i = 0 ; i < lines[j].num_points ; i++)
        {
            lines[j].data[i] = (float64) (
					  0.5*meanvalue +
					  0.5*(2-meanvalue)*AOTF_MAX_VOLT*
	      (sin((double) 2*PI*i/lines[j].num_points) *
	       sin((double) PI*j/NUMBER_OF_AO_LINES + //phase shift  between lines is PI/3
		   (double) PI/(2*NUMBER_OF_AO_LINES)+ //add also a phase of PI/6 so that central line is line number 2
		   (double) phasenum*PI/phaseden )));
	}
    }
    //dump_to_xv_log_file_with_date_and_time("lines structure updated");  
      
    /* Recreates the complete sequence (all lines merged) to be sent to the card */  
    
    if(num_turns == 0)    DURATION = PLAY_FOREVER;
    else                  DURATION = PLAY_ONCE;

    lines__refresh_all_lines();
    //lines__sequence__plot();

    return D_O_K;
}


/** Rotating magnetic field.

All lines are sinus-modulated each with a phase shift of 2PI/3.

\author Francesco Mosconi
\version 12/02/2007
*/
int AOTF__rotating_B_all_interface(void)
{
  int ret = 0;
  if(updating_menu_state != 0)	return D_O_K;
  
  if((ret = set_rfp(&grf)) != 0) return win_printf("ret = %d", ret);
  
  return AOTF__rotating_B_all_gparam_rf(&grf);
}

int AOTF__rotating_B_all_gparam_rf(rfp *rf)
{
  g_frequency = (float64) rf->frequency;
  g_level = rf->level;
  g_num_points = rf->num_points;
  return AOTF__rotating_B_all(rf->num_turns, rf->direction, rf->phasenum, rf->phaseden, rf->stepflag, rf->switch_on);

}

int AOTF__rotating_B_all(int num_turns,int direction,int phasenum,int phaseden,int stepflag,int* switch_on)
{
    int i,j,k,n,K,phase=0;
 
    if (lines == NULL) 
    {
        lines__create_lines_structure();
        dump_to_xv_log_file_with_date_and_time("lines structure was re-created");
    }
    
    /* Creates the data sequence for each line*/ 
    for (j = 0 ; j < NUMBER_OF_AO_LINES ; j++)
    {
      phase = 0;
      lines[j].level = g_level;
      lines[j].status = switch_on[j];
      lines[j].num_points = g_num_points;
      
      lines[j].data = (float64*) realloc(lines[j].data,lines[j].num_points*sizeof(float64));
      if (lines[j].data == NULL) return dump_to_xv_log_file_with_date_and_time("memory allocation error");
      if(stepflag == 0){
	for (i = 0 ; i < lines[j].num_points ; i++)
	  {
	    lines[j].data[i] = (float64) AOTF_MAX_VOLT*
	      (sin(
		   (double) 2*(direction-0.5)*2*PI*i/lines[j].num_points + //
		   (double) PI*j/NUMBER_OF_AO_LINES + //phase shift  between lines is PI/3
		   (double) PI/(2*NUMBER_OF_AO_LINES))); //add also a phase of PI/6 so that central line is line number 2
	  }
      }
      else if(stepflag == 1){
	K = 2*phaseden/phasenum; //number of steps
	n = g_num_points/K;
	k = 0;
	for (i = 0 ; i < lines[j].num_points ; i++)
	  {
	    if(k>=n){
	      k = 0;
	      phase = i;
	    }
	    lines[j].data[i] = (float64) AOTF_MAX_VOLT*
	      (sin(
		   (double) 2*(direction-0.5)*2*PI*phase/lines[j].num_points + //
		   (double) PI*j/NUMBER_OF_AO_LINES + //phase shift  between lines is PI/3
		   (double) PI/(2*NUMBER_OF_AO_LINES)));//add also a phase of PI/6 so that central line is line number 2
	    k++;
	  }
      }
    }
      
    //dump_to_xv_log_file_with_date_and_time("lines structure updated");  
      
    /* Recreates the complete sequence (all lines merged) to be sent to the card */  
    
    if(num_turns == 0)    DURATION = PLAY_FOREVER;
    else                  DURATION = PLAY_ONCE;

    lines__refresh_all_lines();
    //lines__sequence__plot();

    return D_O_K;
}

/** Rotating magnetic field, alternating directions.

All lines are sinus-modulated each with a phase shift of 2PI/3.
The field does one turn in one direction and the following in the opposite so
the linking number is not changed.

\author Francesco Mosconi
\version 12/02/2007
*/
int AOTF__rotating_B_all_fixlink_interface(void)
{

  int ret = 0;
  if(updating_menu_state != 0)	return D_O_K;
  if((ret = set_rfp(&grf)) != 0) return win_printf("ret = %d", ret);
  return AOTF__rotating_B_all_fixlink_gparam_rf(&grf);
}

int AOTF__rotating_B_all_fixlink_gparam_rf(rfp *rf)
{
  int e =0;
  int np = 0;
  //questo serve per fare un po' piu' di un giro
  e = (int) (g_num_points*(rf->epsilon/PI));
  np = (rf->N_1 + rf->N_2)*g_num_points + 2*e;
  
  g_frequency = (float64) ((rf->frequency*rf->num_points)/np);//normalizzo cosi', perche' poi moltiplico la freq per il numero di punti
  g_level = rf->level;
  g_num_points = rf->num_points;
  return AOTF__rotating_B_all_fixlink(rf->num_turns, rf->phasenum, rf->phaseden, rf->stepflag, rf->switch_on, rf->epsilon, rf->N_1, rf->N_2);

}

int AOTF__rotating_B_all_fixlink(int num_turns,int phasenum,int phaseden,int apolisationflag,int* switch_on, float epsilon, int N_1, int N_2)
{
   int i,j,phase=0,e=0;

   if (lines == NULL) 
     {
       lines__create_lines_structure();
       dump_to_xv_log_file_with_date_and_time("lines structure was re-created");
     }
   //questo serve per fare un po' piu' di un giro
   e = (int) (g_num_points*(epsilon/PI));
   
   /* Creates the data sequence for each line*/ 
   for (j = 0 ; j < NUMBER_OF_AO_LINES ; j++)
     {
       phase = 0;
       lines[j].level = g_level;
       lines[j].status = switch_on[j];
       lines[j].num_points = (N_1 + N_2)*g_num_points + 2*e;
       
       lines[j].data = (float64*) realloc(lines[j].data,lines[j].num_points*sizeof(float64));
       if (lines[j].data == NULL) return dump_to_xv_log_file_with_date_and_time("memory allocation error");

       if(apolisationflag == 0){
	 for (i = 0 ; i < N_1*g_num_points + e ; i++)
	   {
	     lines[j].data[i] = (float64) AOTF_MAX_VOLT*
	       (sin((double) i*2*PI/g_num_points - epsilon + //
		    (double) PI*j/NUMBER_OF_AO_LINES + //phase shift  between lines is PI/3
		    (double) phasenum*PI/phaseden +
		    (double) PI/(2*NUMBER_OF_AO_LINES))); //add also a phase of PI/6 so that central line is line number 2
	   }
	 for (i = 0 ; i < N_2*g_num_points + e /*lines[j].num_points*/ ; i++)
	   {
	     lines[j].data[i + N_1*g_num_points + e] = (float64) AOTF_MAX_VOLT*
	       (sin((double) (i)*2*PI/g_num_points - 3*epsilon + PI - //
		    (double) PI*j/NUMBER_OF_AO_LINES - //phase shift  between lines is PI/3
		    (double) phasenum*PI/phaseden -
		    (double) PI/(2*NUMBER_OF_AO_LINES))); //add also a phase of PI/6 so that central line is line number 2
	     //	   lines[j].data[lines[j].num_points/2+i] = lines[j].data[lines[j].num_points/2-i-1];
	   }
       }
       else if(apolisationflag == 1){
	 for (i = 0 ; i < N_1*g_num_points + e ; i++)
	   {
	     lines[j].data[i] = (float64) AOTF_MAX_VOLT*
	       (double)(1-cos((double)i*2*PI/(N_1*g_num_points + e)))*
	       (sin((double) i*2*PI/g_num_points - epsilon + //
		    (double) PI*j/NUMBER_OF_AO_LINES + //phase shift  between lines is PI/3
		    (double) phasenum*PI/phaseden +
		    (double) PI/(2*NUMBER_OF_AO_LINES))); //add also a phase of PI/6 so that central line is line number 2
	   }
	 for (i = 0 ; i < N_2*g_num_points + e /*lines[j].num_points*/ ; i++)
	   {
	     lines[j].data[i + N_1*g_num_points + e] = (float64) AOTF_MAX_VOLT*
	       (double)(1-cos((double)i*2*PI/(N_2*g_num_points + e)))*
	       (sin((double) (i)*2*PI/g_num_points - 3*epsilon + PI - //
		    (double) PI*j/NUMBER_OF_AO_LINES - //phase shift  between lines is PI/3
		    (double) phasenum*PI/phaseden -
		    (double) PI/(2*NUMBER_OF_AO_LINES))); //add also a phase of PI/6 so that central line is line number 2
	     //	   lines[j].data[lines[j].num_points/2+i] = lines[j].data[lines[j].num_points/2-i-1];
	   }
	 if(i + N_1*g_num_points + e != lines[j].num_points ) win_printf("i %d, j %d",i + N_1*g_num_points + e,lines[j].num_points );

       }
        else if(apolisationflag == 2){
	 for (i = 0 ; i < N_1*g_num_points + e ; i++)
	   {
	     lines[j].data[i] = (float64) AOTF_MAX_VOLT*
	       (double)4*(1/(1+pow(cos((double)i*PI/(N_1*g_num_points + e)),6))-0.5)*
	       (sin((double) i*2*PI/g_num_points - epsilon + //
		    (double) PI*j/NUMBER_OF_AO_LINES + //phase shift  between lines is PI/3
		    (double) phasenum*PI/phaseden +
		    (double) PI/(2*NUMBER_OF_AO_LINES))); //add also a phase of PI/6 so that central line is line number 2
	   }
	 for (i = 0 ; i < N_2*g_num_points + e /*lines[j].num_points*/ ; i++)
	   {
	     lines[j].data[i + N_1*g_num_points + e] = (float64) AOTF_MAX_VOLT*
	       (double)4*(1/(1+pow(cos((double)i*PI/(N_1*g_num_points + e)),6))-0.5)*
	       (sin((double) (i)*2*PI/g_num_points - 3*epsilon + PI - //
		    (double) PI*j/NUMBER_OF_AO_LINES - //phase shift  between lines is PI/3
		    (double) phasenum*PI/phaseden -
		    (double) PI/(2*NUMBER_OF_AO_LINES))); //add also a phase of PI/6 so that central line is line number 2
	     //	   lines[j].data[lines[j].num_points/2+i] = lines[j].data[lines[j].num_points/2-i-1];
	   }
	 if(i + N_1*g_num_points + e != lines[j].num_points ) win_printf("i %d, j %d",i + N_1*g_num_points + e,lines[j].num_points );

       }
       
     }
   
   //dump_to_xv_log_file_with_date_and_time("lines structure updated");  
   
   /* Recreates the complete sequence (all lines merged) to be sent to the card */  
   
   if(num_turns == 0)    DURATION = PLAY_FOREVER;
   else                  DURATION = PLAY_ONCE;
   
   lines__refresh_all_lines();
   //lines__sequence__plot();
   
   return D_O_K;
}

/** Static magnetic field.

All lines are on with a constant I.

\author Francesco Mosconi
\version 12/02/2007
*/
int AOTF__static_B_all_interface(void)
{
    static int phasenum = 2, phaseden = 1;
    static int loopflag = NO;
    int ret = OK;

    if(updating_menu_state != 0)	return D_O_K;
    
    ret = win_scanf("This function switches on the magnetic field\n"
	      "on the 3 OUTPUT lines\n"
	      "Please set: \n"
	      "central line intensity [-1:1] %f\n"
	      "Phase = %4d/%4d*PI\n"
	      "do you want to loop? %R No %r Yes",
	      &g_level,&phasenum,&phaseden, &loopflag);
    if(ret == CANCEL) return D_O_K;
    return AOTF__static_B_all(phasenum, phaseden, loopflag);
}

int AOTF__static_B_all_gparam(float level, int phasenum,int phaseden,int stepflag,int loopflag)
{
  g_level = level;
  return AOTF__static_B_all(phasenum, phaseden, loopflag);

}

int AOTF__static_B_all(int phasenum,int phaseden,int loopflag)
{
    int i,j;
    int loopnumber = 0;
   float mylocfreq = g_frequency; 
   int switch_on[3] = {1,1,1};

    if(g_level > 1 || g_level < -1)
      {
	g_level = 1;
	return win_printf("error! level should be between -1 and 1! ");
      }

    if (lines == NULL) 
    {
        lines__create_lines_structure();
        dump_to_xv_log_file_with_date_and_time("lines structure was re-created");
    }
    
    do{
      /* Creates the data sequence for each line*/ 
      for (j = 0 ; j < NUMBER_OF_AO_LINES ; j++)
	{
	  lines[j].level = g_level;
	  lines[j].status = switch_on[j];
	  
	  lines[j].num_points = 5;
	  
	  lines[j].data = (float64*) realloc(lines[j].data,lines[j].num_points*sizeof(float64));
	  if (lines[j].data == NULL) return dump_to_xv_log_file_with_date_and_time("memory allocation error");
	  
	  for (i = 0 ; i < lines[j].num_points ; i++)
	    {
	      lines[j].data[i] = (float64) AOTF_MAX_VOLT*(sin((double) PI*j/NUMBER_OF_AO_LINES +
							      (double) PI/(2*NUMBER_OF_AO_LINES) + 
							      (double) loopnumber*phasenum*PI/phaseden ));
	    }
	}
      
      //dump_to_xv_log_file_with_date_and_time("lines structure updated");  
      
      /* Recreates the complete sequence (all lines merged) to be sent to the card */  
      DURATION = PLAY_ONCE;
      lines__refresh_all_lines();
      if(loopflag == YES)	{
	if(win_printf("done loop with phase = %d/%d PI! continue?",loopnumber*phasenum, phaseden) == OK)loopnumber++;
	else loopflag = NO;
      }
    }
    while(loopflag == YES);
    
    return D_O_K;
}


/** Switches a line on or off.

this is a switch for lines. The result is either 0 (OFF) or lines.level (ON)

\param int line The number of the line. Beware Line 1 has number 0, etc.
\param int state The state to which switch the line. 0 is OFF and 1 is ON.

\author Adrien Meglio
\version 01/11/2006
*/
int AOTF__sequence__switch_line(int line,int state)
{
    int i;
    
    if(updating_menu_state != 0)	return D_O_K;

    if (lines == NULL) 
    {
        lines__create_lines_structure();
        dump_to_xv_log_file_with_date_and_time("lines structures was re-created");
    }
    
    /* Creates the data sequence for the line*/   
    lines[line].num_points = 10; 
    
    lines[line].data = (float64*) realloc(lines[line].data,lines[line].num_points*sizeof(float64));
    if (lines[line].data == NULL) return dump_to_xv_log_file_with_date_and_time("memory allocation error");
    
    for (i = 0 ; i < ((int)(lines[line].num_points)) ; i++)
    {
        lines[line].data[i] = (float64) AOTF_MAX_VOLT*lines[line].level; 
    }
    
    /* Recreates the complete sequence (all lines merged) to be sent to the card */  
    DURATION = PLAY_ONCE;
    lines__refresh_all_lines();

    /* Prints into the log file */
    switch (state)
    {
        case OFF :
        win_event("Switched line LINE %d off",line);
        break;
        case ON :
        win_event("Switched line LINE %d on",line);
        break;
    }
    
    return D_O_K;
}


//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
// Shortcut functions

/** Defines a shortcut to switch the current line.

\author Adrien Meglio
\version 26/01/07
*/
int AOTF__switch_LINE(void)
{
    if(updating_menu_state != 0) return D_O_K;
    
    if (lines[CURRENT_LINE].status == ON)
    {   
        lines[CURRENT_LINE].status = OFF;
        AOTF__sequence__switch_line(CURRENT_LINE,OFF);
    } else
    {
        lines[CURRENT_LINE].status = ON;
        AOTF__sequence__switch_line(CURRENT_LINE,ON);
    }
    
    return D_O_K;
} 

/** Sets the line intensity to a given value (between 0 and 1).

\author Adrien Meglio
\version 29/01/07
*/
int AOTF__set_LINE_intensity(int line, float level)
{
    if(updating_menu_state != 0) return D_O_K;
    
    lines[line].level = (float) level;
    win_event("Changed line LINE %d level to %d/100",line,100*level);
    
    DURATION = PLAY_ONCE;
    lines__refresh_all_lines();
    
    return D_O_K;
} 

/** Defines a shortcut to get the current line intensity.

\author Adrien Meglio
\version 26/01/07
*/
int AOTF__get_LINE_intensity(void)
{
    int level = 0;
    
    if(updating_menu_state != 0) return D_O_K;
    
    win_scanf("Set line LINE to what percentage of maximal intensity ? %d",&level);
    
    AOTF__set_LINE_intensity(CURRENT_LINE,level/100);
    
    return D_O_K;
} 

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
// Base functions

int	AOTF_functions_main(void)
{
    if(updating_menu_state != 0) return D_O_K;
    
    return D_O_K;
}

#endif
