#ifndef _ROTATION_THREADS_C_
#define _ROTATION_THREADS_C_

#include "allegro.h"
#include "winalleg.h"
#include <windowsx.h>

#include "xvin.h"
//#include "global_define.h"
//#include "global_variables.h"
//#include "global_functions.h"


#define BUILDING_PLUGINS_DLL

/** General display thread main operation.

This function is periodically called by the display control thread. 
It monitors acquisition status and reports them to the screen.
\param int None.
\return int Error code.
\return Screen output of acquisition status.
\author Adrien Meglio
\version 26/01/07
*/ 
/* DWORD WINAPI StatusThreadProc(LPVOID lpParam)  */
/* { */
/*     int current_temperature; */
/*     float pifoc_position; */
/*     int line_status; */
/*     float line_level; */
/*     BITMAP *temp_button; */
    
/*     temp_button = new_button(2,40,9,"TEMPERATURE : -69 C ; TARGET : -70 C"); */
    
/*     while(1) */
/*     { */

/*         IO__give_PIFOC_microns(&pifoc_position);  */
/*         textprintf_ex(temp_button,font,0,4*text_height(font),GREY_160,-1,"PIFOC POSITION %.3f um",pifoc_position); */
        
/*         IO__give_line_status(0,&line_status); */
/*         IO__give_line_level(0,&line_level); */
/*         if (line_status == 1) */
/*         { */
/*             textprintf_ex(temp_button,font,0,5*text_height(font),BLUE,-1,"LINE 1 : %.0f/100",line_level*100); */
/*         } else */
/*         { */
/*             textprintf_ex(temp_button,font,0,5*text_height(font),GREY_128,-1,"LINE 1 : %.0f/100",line_level*100); */
/*         } */
        
/*         IO__give_line_status(1,&line_status); */
/*         IO__give_line_level(1,&line_level); */
/*         if (line_status == 1) */
/*         { */
/*             textprintf_ex(temp_button,font,0,6*text_height(font),RED,-1,"LINE 2 : %.0f/100",line_level*100); */
/*         } else */
/*         { */
/*             textprintf_ex(temp_button,font,0,6*text_height(font),GREY_128,-1,"LINE 2 : %.0f/100",line_level*100); */
/*         } */
        
/*         IO__give_line_status(2,&line_status); */
/*         IO__give_line_level(2,&line_level); */
/*         if (line_status == 1) */
/*         { */
/*             textprintf_ex(temp_button,font,0,7*text_height(font),GREEN,-1,"LINE 3 : %.0f/100",line_level*100); */
/*         } else */
/*         { */
/*             textprintf_ex(temp_button,font,0,7*text_height(font),GREY_128,-1,"LINE 3 : %.0f/100",line_level*100); */
/*         } */
/*     }   */
    
/*     destroy_bitmap(temp_button);     */
    
/*     return D_O_K; */
/* }  */

/* /\** Display control thread. */

/* Initializes the display control thread. */
/* \param int None. */
/* \return int Error code. */
/* \author Adrien Meglio */
/* \version 26/01/07 */
/* *\/  */
/* int launcher__rotation_display_thread(void) */
/* { */
/*     HANDLE hThread = NULL; */
/*     DWORD dwThreadId; */

/*     hThread = CreateThread(  */
/*             NULL,              // default security attributes */
/*             0,                 // use default stack size   */
/*             StatusThreadProc,// thread function  */
/*             NULL,        // argument to thread function  */
/*             0,                 // use default creation flags  */
/*             &dwThreadId);      // returns the thread identifier  */

/*     if (hThread == NULL) 	dump_to_xv_log_file_with_date_and_time("No thread created"); */
/*     SetThreadPriority(hThread,  THREAD_PRIORITY_LOWEST); */

/*     return D_O_K; */
/* } */

#endif




