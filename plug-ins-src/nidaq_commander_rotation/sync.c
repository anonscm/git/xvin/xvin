/** \file sync.c

    \author Francesco Mosconi
    
    \version 19/04/07
*/
#ifndef SYNC_C_
#define SYNC_C_

#include <allegro.h>
#include <winalleg.h>
#include <xvin.h>


/* If you include other plug-ins header do it here*/ 
#include <NIDAQmx.h>
#include "../logfile/logfile.h"
#include "nidaq_commander_rotation.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL


//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
//sync functions

int sync__set_sync(void)
{
  if(updating_menu_state != 0) return D_O_K;
  SYNC_flag = 1;    
  return D_O_K;
}

int sync__unset_sync(void)
{
  if(updating_menu_state != 0) return D_O_K;
  SYNC_flag = 0;    
  return D_O_K;
}

int ia(O_p *op, DIALOG *d)
{
  pltreg *pr = NULL;
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return(win_printf("can't find plot region"));
  
  return refresh_plot(pr, UNCHANGED);
}
int sync_continuous_plot_AO_AI_interface(void)
{
  register int i;
  pltreg *pr;
  O_p *op[NUMBER_OF_AI_LINES];
  d_s *dsi[NUMBER_OF_AI_LINES], *dso[NUMBER_OF_AI_LINES];

  if(updating_menu_state != 0) return D_O_K;

  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return(win_printf("can't find plot region"));

  if(AO_data == NULL)
    return win_printf("cannot find AO data");
  if(AI_data == NULL)
    return win_printf("cannot find AI data");

  //creating 3 plots with one period each
  for(i=0 ; i<NUMBER_OF_AI_LINES ; i++){
    op[i] = create_and_attach_one_plot(pr,num_points_per_period,num_points_per_period,0);
    dso[i] = op[i]->dat[0];
    dsi[i] = create_and_attach_one_ds(op[i],num_points_per_period,num_points_per_period,0);
    op[i]->op_idle_action = ia;
    sync_gop[i] = op[i];
    create_attach_select_x_un_to_op (op[i], IS_SECOND, 0, 1./(g_frequency*num_points_per_period), 1, 0, "s");
    create_attach_select_y_un_to_op (op[i], IS_VOLT, 0, 1, 1, 0, "V");
  }

  SYNC_plot_flag = 1;

  return D_O_K;
}

int sync_stop_continuous_plot_AO_AI_interface(void)
{
  if(updating_menu_state != 0) return D_O_K;

  SYNC_plot_flag = 0;
  return D_O_K;
}


int	sync_main(void)
{
    if(updating_menu_state != 0) return D_O_K;
    return D_O_K;
}

#endif

