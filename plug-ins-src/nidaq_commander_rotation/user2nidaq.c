/** \file NIDAQ_user2nidaq.c

    Contains all "user to NIDAQ" conversion functions via the "lines" structure.
    
    \author Adrien Meglio
    
    \version 26/01/07
*/
#ifndef NIDAQ_USER2NIDAQ_C_
#define NIDAQ_USER2NIDAQ_C_

#include <allegro.h>
#include <winalleg.h>
#include <xvin.h>


/* If you include other plug-ins header do it here*/ 
#include <NIDAQmx.h>
#include "../logfile/logfile.h"
#include "nidaq_commander_rotation.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
// Plot functions

/** Plots the data in the lines structure.

\author Adrien Meglio
\version 09/02/07
*/
int lines__sequence__plot(void)
{
    int i,j;
    pltreg *current_pr;
    O_p* current_plot;
    d_s** lines_dataset;
    int* nb_points;
        
    if(updating_menu_state != 0) return D_O_K;
    
    if (lines == NULL) 
    {
        lines__create_lines_structure();
        dump_to_xv_log_file_with_date_and_time("lines structures was re-created");
    }
    
    /* Finds the current plot region, or recreates it */
    current_pr = get_pr();
    if (current_pr == NULL)
    {
        current_pr = create_and_register_new_plot_project(0,0,100,100);
    }
    
    /* Datasets creation */
    nb_points = (int*) calloc(NUMBER_OF_AO_LINES,sizeof(int));
    for (i=0 ; i<NUMBER_OF_AO_LINES ; i++)
    {
        nb_points[i] = lines[i].num_points;
    }
    lines_dataset = give_me_a__set_of_datasets_in_pr(NUMBER_OF_AO_LINES,nb_points,"Rotating field sequences");

    /* Feeds the datasets with the data from lines */
    for (i=0 ; i<NUMBER_OF_AO_LINES ; i++)
    {
        for (j=0 ; j<lines[i].num_points ; j++)
        {
            lines_dataset[i]->xd[j] = (float) j/(lines[i].num_points*g_frequency); /* Converts points to time */
            lines_dataset[i]->yd[j] = (float) (lines[i].data[j])*(lines[i].level)*(lines[i].status); 
        }
    }
    
    /* Labels */
    current_plot = current_pr->o_p[current_pr->cur_op];
    current_plot->x_title = "Time (s)";
    current_plot->y_title = "Input tension (V)";
    
    return D_O_K;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
// lines structure functions    

/*
NOTE : The configuration is such that the user may change the frequency independently of the resolution. 
The user creates a 'lines' structure with a given number of points per period. It is the resolution of the signal. 
Then the user can change the signal frequency via the global float g_frequency.
*/

int lines__create_lines_structure(void)
{
    int i,j;
    
    if(updating_menu_state != 0)	return D_O_K;
    
    lines = (l_s*) calloc(NUMBER_OF_LINES,sizeof(l_s));
    if (lines == NULL) return dump_to_xv_log_file_with_date_and_time("memory allocation error");
    
    for (j = 0 ; j < NUMBER_OF_LINES ; j++)
    {
        lines[j].level = 1;
        
        lines[j].num_points = 1;
        lines[j].data = (float64*) realloc(lines[j].data,lines[j].num_points*sizeof(float64));
        if (lines[j].data == NULL) return dump_to_xv_log_file_with_date_and_time("memory allocation error");
        
        for (i = 0 ; i < lines[j].num_points ; i++)
        {
            lines[j].data[i] = (float64) 0.0; 
        }
    }
    
    return D_O_K;
}     
  
/** Refreshes the lines data.

This is a refresher for the lines structure, useful e.g. if lines.devel has been changed.

\author Adrien Meglio
\version 26/01/07
*/
int lines__refresh_all_lines(void)
{
    int i;
    
    if(updating_menu_state != 0)	return D_O_K;
    
    /* If a task is already running : stop it */
    task__all_close();

    if (lines == NULL) 
    {
        lines__create_lines_structure();
        dump_to_xv_log_file_with_date_and_time("lines structures was re-created");
    }
    
    /* Recreates the complete sequence (all lines merged) to be sent to the card */  
    lines__merge_data_and_launch(lines,DURATION,g_frequency);  
    
    return D_O_K;  
}   

////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
// Initialization

/* Initializes the card.

\author Adrien Meglio
\version 26/01/07
*/
int lines__initialize_NIDAQ(void)
{
    int i,j;
    
    if(updating_menu_state != 0)	return D_O_K;
    
    CURRENTtaskHandle = 0; // No task is running at the beginning
    
    /* Creates the structure */
    lines__create_lines_structure();
    
    for (j = 0 ; j < NUMBER_OF_LINES ; j++)
    {
        lines[j].level = 1;
        
        lines[j].num_points = 10;
        
        lines[j].data = (float64*) realloc(lines[j].data,lines[j].num_points*sizeof(float64));
        if (lines[j].data == NULL) return dump_to_xv_log_file_with_date_and_time("memory allocation error");
        
        for (i = 0 ; i < lines[j].num_points ; i++)
        {
            lines[j].data[i] = (float64) 0.0; 
        }
    }
      
    /* Recreates the complete sequence (all lines merged) to be sent to the card */  
    DURATION = PLAY_ONCE;
    lines__refresh_all_lines(); 

    return D_O_K;
} 

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
// Base functions

int	user2nidaq_main(void)
{
    if(updating_menu_state != 0) return D_O_K;
    
	return D_O_K;
}

#endif

