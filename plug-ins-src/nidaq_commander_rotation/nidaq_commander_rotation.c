/** \file nidaq_commander_rotation.c
    \brief Sub-plug-in program for AOTF control with NIDAQ-M 6229 in Xvin.
    
    Controls function generator for different AOTF control situations.
    BEWARE : requests the NIDAQ plugin !
    
    \sa NIDAQ.c
    \author Adrien Meglio
    \version 01/11/2006
*/
#ifndef NIDAQ_COMMANDER_ROTATION_C_
#define NIDAQ_COMMANDER_ROTATION_C_

#include <allegro.h>
#include <winalleg.h>
#include <xvin.h>

/* If you include other plug-ins header do it here*/ 
#include <NIDAQmx.h>
#include "../logfile/logfile.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL
#include "nidaq_commander_rotation.h"
extern rfp grf;
//////////////////////////////////////////////////////////////////////////////////////////////
// Base functions

int	nidaq_commander_rotation_main(void)
{
    if(updating_menu_state != 0) return D_O_K;
    
    menu_plug_in_main();

    g_frequency = 1;
    g_num_points = 256;
    g_level = 0.1;
    init_rfp(&grf);
    lines__initialize_NIDAQ(); // Creates the writing task

    //dump_to_xv_log_file_with_date_and_time("NIDAQ capabilities loaded");
    
    PIFOC_functions_main();
    AOTF_functions_main();
    shortcuts_main();
    IO_main();
    user2nidaq_main();

	return D_O_K;
}
int	nidaq_commander_rotation_unload(void)
{
  
  return D_O_K;
}
#endif
