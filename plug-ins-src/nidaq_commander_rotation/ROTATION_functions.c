/** \file ROTATION_functions.c
    \brief Sub-plug-in program for ROTATION control with NIDAQmx 6229 in Xvin.
    
    Controls function generator to drive magnetic field.
    BEWARE : requests the NIDAQ plugin !
    
    \sa NIDAQ.c
    \author Francesco Mosconi
    \version 01/03/07
*/
#ifndef ROTATION_FUNCTIONS_C_
#define ROTATION_FUNCTIONS_C_

#include <allegro.h>
#include <winalleg.h>
#include <xvin.h>

/* If you include other plug-ins header do it here*/ 
#include <NIDAQmx.h>
#include "../logfile/logfile.h"
#include "nidaq_commander_rotation.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL

////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////

/** Directly sets the ROTATION to a given position.

\author Francesco Mosconi
\version 01/03/07
*/
int ROTATION__set_RADIANS(void)
{
    float ROTATION_temp = 0;
    int nt = 0 ;
        
    if(updating_menu_state != 0) return D_O_K;
    
    win_scanf("ROTATION angle: %f rad"
	      "number of turns %d",&ROTATION_temp, &nt);
       
    ROTATION__move(ROTATION_temp/2*PI - ROTATION_position);
    
    return D_O_K;
}


/** Moves the PIFOC of a given quantity.

\param float pifoc_move The displacement in percent (between 0 and 1) of the max course.

\author Francesco Mosconi
\version 01/03/07
*/
int ROTATION__move(float pifoc_move)
{
    int i; 
    //float ROTATION_temp;  
        
    if(updating_menu_state != 0) return D_O_K;
    
    /* Checks that the value is not off-limits */
    ROTATION_POSITION += pifoc_move;
    ROTATION_POSITION = (ROTATION_POSITION*ROTATION_TOTAL_COURSE > ROTATION_MAX_VOLT) ? ROTATION_MAX_VOLT/ROTATION_TOTAL_COURSE : ROTATION_POSITION;
    ROTATION_POSITION = (ROTATION_POSITION*ROTATION_TOTAL_COURSE < ROTATION_MIN_VOLT) ? ROTATION_MIN_VOLT/ROTATION_TOTAL_COURSE : ROTATION_POSITION;
    
    /* Creates the data sequence for the ROTATION line*/ 
    lines[ROTATION_LINE].level = 1;
    
    lines[ROTATION_LINE].num_points = 10;
            
    lines[ROTATION_LINE].data = (float64*) realloc(lines[ROTATION_LINE].data,lines[ROTATION_LINE].num_points*sizeof(float64));
    if (lines[ROTATION_LINE].data == NULL) return dump_to_xv_log_file_with_date_and_time("memory allocation error");
    
    for (i = 0 ; i < lines[ROTATION_LINE].num_points ; i++)
    {
        lines[ROTATION_LINE].data[i] = (float64) ROTATION_POSITION*ROTATION_TOTAL_COURSE; 
    }
      
    /* Recreates the complete sequence (all lines merged) to be sent to the card */  
    DURATION = PLAY_FOREVER;
    lines__refresh_all_lines();
    
    /* Prints into the log file */
    //win_event("Changed ROTATION position to %f �m (really %f/100)\n",(1-ROTATION_POSITION)*ROTATION_COURSE_MICRONS,100*ROTATION_POSITION);
    
	return D_O_K;
}

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
// Bead search functions

/** A ROTATION reference setting mode.

Allows the user to dynamically select 2 ROTATION references by ctrl+click (left or right) 
then go back to them just by clicking. Program exit is provided by the escape key.

\author Francesco Mosconi
\version 01/03/07
*/
DWORD WINAPI thread__ROTATION_reference(LPVOID lpParam) 
{
    int pressed_key;
    int exit_status = 0;
    float pifoc_position_percent_1 = 0;
    float pifoc_position_percent_2 = 1;

    
    while (exit_status == 0)MAGNETIC_FIELD
      {
      
        if (key[KEY_A]) /* Q is KEY_A on qwerty keyboards */
        {
            exit_status = 1;
            //dump_to_xv_log_file_with_date_and_time("Exiting reference mode");
        }
        
        if (key[KEY_F1]) 
        {
             if (key_shifts & KB_CTRL_FLAG) 
             {
                pifoc_position_percent_1 = ROTATION_POSITION;
                //dump_to_xv_log_file_with_date_and_time("Set reference 1 to : %.0f",pifoc_position_percent_1);
             } 
             else 
             {
                ROTATION__move(pifoc_position_percent_1 - ROTATION_POSITION);
                //dump_to_xv_log_file_with_date_and_time("Moved to reference 1");
             }
        }
        
        if (key[KEY_F2]) 
        {
             if (key_shifts & KB_CTRL_FLAG) 
             {
                pifoc_position_percent_2 = ROTATION_POSITION;
                //dump_to_xv_log_file_with_date_and_time("Set reference 2 to : %.0f",pifoc_position_percent_2);
             } 
             else 
             {
                ROTATION__move(pifoc_position_percent_2 - ROTATION_POSITION);
                //dump_to_xv_log_file_with_date_and_time("Moved to reference 2");
             }
        }
   }

    return D_O_K;
}

/** ROTATION reference control thread launcher.

Initializes the ROTATION reference control thread.
\param int None.MAGNETIC_FIELD
\return int Error code.
\author Adrien Meglio
\version 2/02/07
*/ 
int launcher__ROTATION_reference(void)
{
    HANDLE hThread = NULL;
    DWORD dwThreadId;

    hThread = CreateThread( 
            NULL,              // default security attributes
            0,                 // use default stack size  
            thread__ROTATION_reference,// thread function 
            NULL,        // argument to thread function 
            0,                 // use default creation flags 
            &dwThreadId);      // returns the thread identifier 

    if (hThread == NULL) 	dump_to_xv_log_file_with_date_and_time("No thread created");
    SetThreadPriority(hThread,  THREAD_PRIORITY_LOWEST);

    return D_O_K;
}


//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
// Shortcut functions

/** Defines a shortcut to move the focal plane upwards.

\author Adrien Meglio
\version 26/01/07
*/
int ROTATION__cw(void)
{
    if(updating_menu_state != 0) return D_O_K;
     
    ROTATION__move(ROTATION_FINESSE_MICRONS/ROTATION_COURSE_MICRONS);

//    dump_to_xv_log_file_with_date_and_time("Move up");
    
    return D_O_K;
}

/** Defines a shortcut to move the focal plane downwards.

\author Adrien Meglio
\version 26/01/07
*/
int ROTATION__ccw(void)
{
    if(updating_menu_state != 0) return D_O_K;
      
    ROTATION__move(-ROTATION_FINESSE_MICRONS/ROTATION_COURSE_MICRONS);

//    dump_to_xv_log_file_with_date_and_time("Move up");
    
    return D_O_K;
}

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
/** Rotation to lines conversion.

All lines are sinus-modulated each with a phase shift of 2PI/3.

\author Francesco Mosconi
\version 12/02/2007
*/
int ROTATION_to_lines(void)
{
  register int i,j;

  if (lines == NULL) 
    {
      lines__create_lines_structure();
      dump_to_xv_log_file_with_date_and_time("lines structure was re-created");
    }
  
  /* Creates the data sequence for each line*/ 
  for (j = 0 ; j < NUMBER_OF_AO_LINES ; j++)
    {
      phase = 0;
      lines[j].level = rotation.a;
      lines[j].status = switch_on[j];
      if(rotation.t = o){
	lines[j].num_periods = 1;
	lines[j].num_points = 5;
      }
      else{
	lines[j].num_periods = rotation.t;
	lines[j].num_points = rotation.p;
      }

      lines[j].data = (float64*) realloc(lines[j].data,lines[j].num_points*lines[j].num_periods*sizeof(float64));
      if (lines[j].data == NULL) return dump_to_xv_log_file_with_date_and_time("memory allocation error");
      for (i = 0 ; i < lines[j].num_points ; i++)
        {
	  lines[j].data[i] = (float64) (
					0.5*meanvalue +
					0.5*(2-meanvalue)*AOTF_MAX_VOLT*
					(sin((double) 2*PI*i/lines[j].num_points) *
					 sin((double) PI*j/NUMBER_OF_AO_LINES + //phase shift  between lines is PI/3
					     (double) PI/(2*NUMBER_OF_AO_LINES)+ //add also a phase of PI/6 so that central line is line number 2
					     (double) phasenum*PI/ )));
	}
      
      
    }
  
  //dump_to_xv_log_file_with_date_and_time("lines structure updated");  
  
  /* Recreates the complete sequence (all lines merged) to be sent to the card */  
  
  DURATION = PLAY_ONCE;
  
  lines__refresh_all_lines();
  //lines__sequence__plot();
  
  return D_O_K;
}


//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
//ROTATION IO functions:

/** Opens file for reading. It creates the file if it does not exist
    and it sets the rotation value to the .

\author Francesco Mosconi
\version 01/03/07
*/
int ROTATION_f_init(void)
{
  if(rotation_fp == NULL)  
    if((rotation_fp = fopen("rotation.dat","r+")) == NULL)
      if((rotation_fp = fopen("rotation.dat","w+")) == NULL)
	return win_printf("cannot open rotation file");
      else ROTATION_reset();
  
  return 0;
}

/** Reads from rotation binary file the current rotation status.

\author Francesco Mosconi
\version 01/03/07
*/
int ROTATION_f_read(r_s *rp)
{
  if(rotation_fp == NULL)
    ROTATION_f_init();

  fread(rp, sizeof(r_s), 1, rotation_fp);
  rewind(rotation_fp);
  
  return 0;
}

/** Writes to rotation binary file the current rotation status.

\author Francesco Mosconi
\version 01/03/07
*/
int ROTATION_f_write(r_s *rp)
{
  if(updating_menu_state != 0) return D_O_K;
  
  if(rotation_fp == NULL)
    ROTATION_f_init();
  
  fwrite(rp, sizeof(r_s), 1, rotation_fp);
  fflush(rotation_fp);
  rewind(rotation_fp);

  return 0;
}

/** Closes the rotation binary file.

\author Francesco Mosconi
\version 01/03/07
*/
int ROTATION_f_close(void)
{
  
  if(rotation_fp == NULL)
    ROTATION_f_init();
  
  fclose(rotation_fp);
  
  return 0;
}

/** Outputs rotation value to screen.File 

\author Francesco Mosconi
\version 01/03/07
*/
int ROTATION_fprintf(void)
{
  r_s temprot;

  if(updating_menu_state != 0) return D_O_K;
  
  ROTATION_f_read(&temprot);
  
  win_printf("rotation.a = %5f\n"
	     "rotation.p = %5d\n"
	     "rotation.f = %5d\n"
	     "rotation.t = %5d\n"
	     "rotation.r = %5d\n"
	     "rotation.w = %5f'n"
	     "temprot.a = %5f\n"
	     "temprot.p = %5d\n"
	     "temprot.f = %5d\n"
	     "temprot.t = %5d\n"
	     "temprot.r = %5d\n"
	     "temprot.w = %5f'n",
	     rotation.a, rotation.p, rotation.f,
	     rotation.t, rotation.r, rotation.w,
	     temprot.a, temprot.p, temprot.f,
	     temprot.t, temprot.r, temprot.w);	     
  return 0;
}

/** Closes the rotation binary file.

\author Francesco Mosconi
\version 01/03/07
*/
int ROTATION_f_close(void)
{
  
  if(rotation_fp == NULL)
    ROTATION_f_init();
  
  fclose(rotation_fp);
  
  return 0;
}



//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
//Rotation Initialisation

/** Rotation reset to default values

\author Francesco Mosconi
\version 01/03/07
*/
int ROTATION_reset(void)
{
  
  rotation.a = 0; //0V initial amplitude
  rotation.p = 256;
  rotation.f = BOBINES;
  rotation.t = 0;
  rotation.r = 0;
  rotation.w = 0;

  f_rotation_write(&rotation);

  return D_O_K;
}

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
// Base functions

/** Main function of NIDAQ ROTATION

\author Francesco Mosconi
\version 01/03/07
*/
int  ROTATION_functions_main(void)
{
  if(updating_menu_state != 0) return D_O_K;
  
  ROTATION_f_init();//initialize rotation structure. read file values if they exist, otherwise set to default
  
  rotation_to_lines();
  
  launcher__ROTATION_reference(); /* Enters ROTATION reference mode */
  return D_O_K;
}

#endif



