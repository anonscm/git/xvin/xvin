/** \file menu_plug_in.c
    \brief Plug-in program for menu settings and display.
    
    This is a subprogram that creates, displays and interfaces the menus with the plug-in functions.
    
    \author Adrien Meglio
*/
#ifndef MENU_PLUG_IN_C
#define MENU_PLUG_IN_C

#include <allegro.h>
#include <winalleg.h>
#include <xvin.h>

/* If you include other plug-ins header do it here*/ 
#include <NIDAQmx.h>
#include "../logfile/logfile.h"
#include "nidaq_commander_rotation.h"

#define BUILDING_PLUGINS_DLL

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Menu functions

/** A menu containing a clickable item and a static submenu */
MENU *commander_base_menu(void)
{
  static MENU mn[32];
  
  if (mn[0].text != NULL)	return mn;
  
  add_item_to_menu(mn,"Alternating B",AOTF__alternating_B_all_interface,NULL,0,NULL);
  add_item_to_menu(mn,"Rotating B",AOTF__rotating_B_all_interface,NULL,0,NULL);
  add_item_to_menu(mn,"Rotating B fixlink",AOTF__rotating_B_all_fixlink_interface,NULL,0,NULL);
  add_item_to_menu(mn,"Static B",AOTF__static_B_all_interface,NULL,0,NULL);
  add_item_to_menu(mn,"sync set sync",sync__set_sync,NULL,0,NULL);
  add_item_to_menu(mn,"sync unset sync",sync__unset_sync,NULL,0,NULL);
  add_item_to_menu(mn,"sync continuous plot AO AI interface",sync_continuous_plot_AO_AI_interface,NULL,0,NULL);
  add_item_to_menu(mn,"sync stop continuous plot AO AI interface",sync_stop_continuous_plot_AO_AI_interface,NULL,0,NULL);

  add_item_to_menu(mn,"Plot 'lines'",lines__sequence__plot,NULL,0,NULL);
  add_item_to_menu(mn,"Stop tasks",task__all_close,NULL,0,NULL);
  
  //add_item_to_menu(mn,"Stop",task__all_close,NULL,0,NULL);
  add_item_to_menu(mn,"Set PIFOC",PIFOC__set_MICRONS,NULL,0,NULL);
  
  /* Useless : only here to be constantly evaluated */
  add_item_to_menu(mn,"Shortcut",short__KEY_PLUS_PAD,NULL,0,NULL); 
  add_item_to_menu(mn,"Shortcut",short__KEY_MINUS_PAD,NULL,0,NULL); 
  add_item_to_menu(mn,"Shortcut",short__KEY_P,NULL,0,NULL); 
  add_item_to_menu(mn,"Shortcut",short__KEY_L,NULL,0,NULL); 
  add_item_to_menu(mn,"Shortcut",short__KEY_1_PAD,NULL,0,NULL); 
  add_item_to_menu(mn,"Shortcut",short__KEY_2_PAD,NULL,0,NULL); 
  add_item_to_menu(mn,"Shortcut",short__KEY_3_PAD,NULL,0,NULL); 
  add_item_to_menu(mn,"Shortcut",short__KEY_4_PAD,NULL,0,NULL); 
  add_item_to_menu(mn,"Shortcut",short__KEY_5_PAD,NULL,0,NULL); 
  add_item_to_menu(mn,"Shortcut",short__KEY_6_PAD,NULL,0,NULL); 
  add_item_to_menu(mn,"Shortcut",short__KEY_7_PAD,NULL,0,NULL); 
  add_item_to_menu(mn,"Shortcut",short__KEY_8_PAD,NULL,0,NULL); 
  add_item_to_menu(mn,"Shortcut",short__KEY_9_PAD,NULL,0,NULL); 
  add_item_to_menu(mn,"Shortcut",short__KEY_G,NULL,0,NULL); 
  add_item_to_menu(mn,"Shortcut",short__KEY_O,NULL,0,NULL); 
  add_item_to_menu(mn,"Shortcut",short__KEY_F,NULL,0,NULL); 
  add_item_to_menu(mn,"Shortcut",short__KEY_C,NULL,0,NULL); 
  add_item_to_menu(mn,"Shortcut",short__KEY_R,NULL,0,NULL);
  add_item_to_menu(mn,"Shortcut",short__KEY_0_PAD,NULL,0,NULL);
  
  return mn;
}

/** Main menu for plug_in */     
int menu_plug_in_main(void)
{   
    if(updating_menu_state != 0) return D_O_K;
    
    //dump_to_xv_log_file_with_date_and_time("Menu capabilities loaded");
    
    add_plot_treat_menu_item("AOTF control",NULL,commander_base_menu(),0,NULL);
    add_image_treat_menu_item("AOTF control",NULL,commander_base_menu(),0,NULL);
	
	return D_O_K;    
}
int menu_plug_in_unload(void)
{   
  if(updating_menu_state != 0) return D_O_K;
  
  remove_item_to_menu(plot_treat_menu,"AOTF control",NULL,NULL);    
  remove_item_to_menu(image_treat_menu,"AOTF control",NULL,NULL);
  return D_O_K;    
}

#endif

