#include "methylstat.hh"
#include "../stat/stat.h"
#include "define.hh"
#include "p_treat_basic.h"
#include <cassert>
#include <vector>
#include <cfloat>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <map>

std::map<int, float> beadNmBaseRateMap;

class comma_numpunct : public std::numpunct<char>
{
  protected:
    virtual char do_decimal_point() const
    {
        return ',';
    }
};


bool extract_cycle_info_from_cycle_dataset(d_s *cycle_ds, int *cycle_id)
{
    bool ret = false;

    if (cycle_ds && cycle_ds->source)
    {
        ret = sscanf(cycle_ds->source, CYCLE_DATASET_SOURCE, cycle_id) == 1;
    }

    return ret;
}

bool extract_bead_info_from_cycle_plot(O_p *cycle_plot, int *bead_id, int *track_id)
{
    bool ret = false;

    if (cycle_plot && cycle_plot->filename)
    {
        ret = sscanf(cycle_plot->filename, CYCLE_PLOT_FILENAME, bead_id, track_id) == 2;
    }

    return ret;
}

bool extract_bead_info_from_phase_cycle_plot(O_p *cycle_phase_plot, int *bead_id, int *track_id, int *phase_id)
{
    bool ret = false;

    if (cycle_phase_plot && cycle_phase_plot->filename)
    {
        ret = (sscanf(cycle_phase_plot->filename, CYCLE_PHASE_PLOT_FILENAME, bead_id, track_id, phase_id) == 3);
    }

    return ret;
}
bool extract_bead_info_from_hyb_pattern_plot(O_p *hyb_pattern_plot, int *bead_id, int *track_id, int *phase_id)
{
    bool ret = false;

    if (hyb_pattern_plot && hyb_pattern_plot->filename)
    {
        ret = (sscanf(hyb_pattern_plot->filename, HYB_PATTERN_PLOT_FILENAME, bead_id, track_id, phase_id) == 3);
    }

    return ret;
}

/**
 * @brief Get the size and the position of a phase in a cycle for a given record.
 *
 * @param record_track the input record
 * @param phase the choosen phase
 * @param cycle the cycle from which extract the data
 * @param out_begin the begin of the phase in the cycle
 * @param out_end the end of the phase in the cycle
 *
 * @return 0 on success a negative value otherwise
 */
// not sure it will work quite well
int get_phase_indexes(g_record *record_track, int phase, int cycle, int *out_begin, int *out_end)
{
    int done = 0;
    int ref_cycle_zero = 0;
    int begin_index = 0;
    int end_index = 0;
    int param_cst = 0;
    assert(out_begin || out_end);
    // Getting cycle size
    done = retrieve_image_index_of_next_point(record_track, cycle, &begin_index, &end_index);

    if (done < 0)
    {
        return -1;
    }

    ref_cycle_zero = begin_index;
    done = retrieve_image_index_of_next_point_phase(record_track, cycle, phase, &begin_index, &end_index, &param_cst);

    if (done < 0)
    {
        return -1;
    }

    if (out_begin)
    {
        *out_begin = begin_index - ref_cycle_zero;
    }

    if (out_end)
    {
        *out_end = end_index - ref_cycle_zero;
    }

    return 0;
}

// FIXME : Work in progress ...
int suppress_points_for_reference_phase_range(g_record *record_track, int ref_cycle, int ref_phase, int phase,
        float keep_percentage)
{
    int begin_ref_idx;
    int end_ref_idx;
    int begin_idx;
    int end_idx;

    if (get_phase_indexes(record_track, ref_phase, ref_cycle, &begin_ref_idx, &end_ref_idx) != 0 &&
            get_phase_indexes(record_track, phase, ref_cycle, &begin_idx, &end_idx) != 0)
    {
        return -1;
    }

    for (int i = begin_ref_idx; i < end_ref_idx; i++)
    {
    }
}


std::vector<float> *get_median_offset_in_phase(g_record *record_track, O_p *plot, int phase,
        int cutted_points)
{
    std::vector<float> globVect;
    std::vector<float> locVect;
    int begin_index = 0;
    int end_index = 0;
    std::vector<float> *medians = new std::vector<float>();
    float globalMedian = 0;

    for (int ds_idx = 0; ds_idx < plot->n_dat; ++ds_idx)
    {
        int cur_cycle = -1;
        extract_cycle_info_from_cycle_dataset(plot->dat[0], &cur_cycle);

        if (cur_cycle == -1)
        {
            medians->push_back(0);
            continue;
        }

        get_phase_indexes(record_track, phase, cur_cycle, &begin_index, &end_index);
        begin_index += cutted_points;
        end_index -= cutted_points;

        for (int i = 0; i < plot->dat[ds_idx]->nx ; i++)
        {
            if (plot->dat[ds_idx]->xd[i] >= begin_index && plot->dat[ds_idx]->xd[i] <= end_index)
            {
                globVect.push_back(plot->dat[ds_idx]->yd[i]);
                locVect.push_back(plot->dat[ds_idx]->yd[i]);
            }
        }

        if (!locVect.empty())
        {
            std::sort(locVect.begin(), locVect.end());
            medians->push_back(locVect[locVect.size() / 2]);
            locVect.clear();
        }
        else
        {
            medians->push_back(0);
        }
    }

    std::sort(globVect.begin(), globVect.end());

    if (globVect.size() > 0)
    {
        globalMedian = globVect[globVect.size() / 2];
    }

    for (unsigned int i = 0; i < medians->size(); ++i)
    {
        (*medians)[i] = globalMedian - (*medians)[i];
    }

    return medians;
}

/**
 * @brief Align all cycle on phase, based on the center (median) of the reference phase distribution.
 *
 * @param record_track the track from which as been extracted cycles
 * @param plot the cycles plot
 * @param phase the phase from which align data
 * @param cutted points at the begining and end of the cycle (avoid bad alignement cycle transition
 * is too big)
 *
 * @return 0 on success, a negative value on fail.
 */
int align_cycles_on_phase(g_record *record_track, O_p *plot, int phase, int cutted_points)
{
    pltreg *pr = NULL;
    ac_grep(cur_ac_reg, "%pr", &pr);
    std::vector<float> vect;
    //O_p *op = create_and_attach_one_plot(pr, ref_ds->nx, ref_ds->ny, 0);
    std::vector<float> *mediansOffset = get_median_offset_in_phase(record_track, plot, phase, cutted_points);

    // appling shift to the other dataset
    for (int ds_idx = 0; ds_idx < plot->n_dat; ds_idx++)
    {
        d_s *ds = plot->dat[ds_idx]; // we work on dataset k

        for (int i = 0; i < ds->ny; i++)
        {
            ds->yd[i] += (*mediansOffset)[ds_idx];
        }

        set_ds_treatement(ds, "Data shifted in Y by %g", (*mediansOffset)[ds_idx]);
    }

    plot->need_to_refresh = 1;
    return 0;
}

// NOT TESTED
int align_cycle_phase_to_zero(g_record *record_track, O_p *plot, int phase, int cutted_points)
{
    pltreg *pr = NULL;
    ac_grep(cur_ac_reg, "%pr", &pr);
    std::vector<float> vect;
    int begin_index = 0;
    int end_index = 0;
    //O_p *op = create_and_attach_one_plot(pr, ref_ds->nx, ref_ds->ny, 0);

    // appling shift to the other dataset
    for (int ds_idx = 0; ds_idx < plot->n_dat; ds_idx++)
    {
        d_s *ds = plot->dat[ds_idx]; // we work on dataset k
        vect.clear();
        int cur_cycle = -1;
        extract_cycle_info_from_cycle_dataset(plot->dat[0], &cur_cycle);

        if (cur_cycle == -1)
        {
            continue;
        }

        get_phase_indexes(record_track, phase, cur_cycle, &begin_index, &end_index);
        begin_index += cutted_points;
        end_index -= cutted_points;

        for (int i = 0; i < plot->dat[ds_idx]->nx ; i++)
        {
            if (plot->dat[ds_idx]->xd[i] >= begin_index && plot->dat[ds_idx]->xd[i] <= end_index)
            {
                vect.push_back(plot->dat[ds_idx]->yd[i]);
            }
        }

        std::sort(vect.begin(), vect.end());

        for (int i = 0; i < ds->ny; i++)
        {
            ds->yd[i] -=  vect[vect.size() / 2];
        }
    }

    plot->need_to_refresh = 1;
    return 0;
}

/**
 * @brief Create histogram based on the value of cycle for a given phase
 *
 * @param record_track the record
 * @param pr current plot region (to create new plot in the right project)
 * @param cycles_plot the plot with on dataset for each cycle
 * @param phase the phase to look at
 * @param biny sigma of the histogram gaussian
 *
 * @return the plot containing the histogram.
 */

O_p *create_hybridization_pattern_from_phase(g_record *record_track, pltreg *pr, O_p *cycles_plot, int begin_phase,
        int end_phase,
        float biny, int cutted_points, const char *save_folder, bool norm_histo)
{
    O_p *phase_cycles_plot = NULL;
    O_p *hyb_pattern_plot = NULL;
    int bead_id = 0;
    int track_id = 0;
    char file[512] = {0};
    char buf[1024] = {0};
    phase_cycles_plot = get_phase_plot(record_track, pr, cycles_plot, begin_phase, end_phase, cutted_points);

    // building the histogram
    if (norm_histo)
    {
        hyb_pattern_plot = build_histo_with_exponential_convolution_norm_for_each_ds(pr, phase_cycles_plot, biny);
    }
    else
    {
        hyb_pattern_plot = build_histo_with_exponential_convolution(pr, phase_cycles_plot, NULL, biny);
    }

    if (hyb_pattern_plot == NULL)
    {
        return NULL;
    }

    extract_bead_info_from_phase_cycle_plot(phase_cycles_plot, &bead_id, &track_id, &end_phase);
    snprintf(buf, sizeof(buf), "Bead%d - Track%d phase[%d;%d] Histogram", bead_id, track_id, begin_phase, end_phase);
    hyb_pattern_plot->dat[0]->source = strdup(buf);
    set_plot_title(hyb_pattern_plot, "\\stack{{Hybridization pattern of}{%s}}\n", phase_cycles_plot->title);
    set_plot_file(hyb_pattern_plot, HYB_PATTERN_PLOT_FILENAME, bead_id, track_id, &end_phase);

    if (save_folder)
    {
        set_op_path(phase_cycles_plot, save_folder);
        build_full_file_name(file, sizeof(file), phase_cycles_plot->dir, phase_cycles_plot->filename);
        save_one_plot(phase_cycles_plot, file);
        set_op_path(hyb_pattern_plot, save_folder);
        build_full_file_name(file, sizeof(file), hyb_pattern_plot->dir, hyb_pattern_plot->filename);
        save_one_plot(hyb_pattern_plot, file);
        remove_data_from_pltreg(pr, IS_ONE_PLOT, (void *)phase_cycles_plot);
        //free_one_plot(phase_cycles_plot);
        phase_cycles_plot = NULL;
    }

    return hyb_pattern_plot;
}

O_p *get_phase_plot(g_record *record_track, pltreg *pr, O_p *cycles_plot, int begin_phase, int end_phase,
                    int cutted_points)
{
    int begin = 0;
    int end = 0;
    int point_count = 0;
    O_p *phase_cycles_plot = NULL;
    int bead_id = 0;
    int track_id = 0;
    int cycle_id = 0;
    // get index of our phase

    if (!cycles_plot || cycles_plot->n_dat < 1)
    {
        return NULL;
    }

    extract_cycle_info_from_cycle_dataset(cycles_plot->dat[0], &cycle_id);
    get_phase_indexes(record_track, begin_phase, cycle_id, &begin, NULL);
    begin += cutted_points;
    get_phase_indexes(record_track, end_phase, cycle_id, NULL, &end);
    end -= cutted_points;
    point_count  = end - begin;

    if (point_count <= 0)
    {
        return nullptr;
    }

    // creating and init the plot only phase points
    phase_cycles_plot = create_and_attach_one_plot(pr, point_count, point_count, 0);
    assert(phase_cycles_plot != nullptr);
    uns_op_2_op_by_type(cycles_plot, IS_Y_UNIT_SET, phase_cycles_plot, IS_Y_UNIT_SET);
    uns_op_2_op_by_type(cycles_plot, IS_X_UNIT_SET, phase_cycles_plot, IS_X_UNIT_SET);
    change_decade(phase_cycles_plot->yu[1], IS_NANO);

    for (int i = 1; i < cycles_plot->n_dat; i++)
    {
        extract_cycle_info_from_cycle_dataset(cycles_plot->dat[i], &cycle_id);
        get_phase_indexes(record_track, begin_phase, cycle_id, &begin, NULL);
        begin += cutted_points;
        get_phase_indexes(record_track, end_phase, cycle_id, NULL, &end);
        end -= cutted_points;
        point_count  = end - begin;
        create_and_attach_one_ds(phase_cycles_plot, point_count, point_count, 0);
    }

    // filling  plot
    for (int ds_idx = 0; ds_idx < cycles_plot->n_dat; ds_idx++)
    {
        extract_cycle_info_from_cycle_dataset(cycles_plot->dat[ds_idx], &cycle_id);
        d_s *cur_phase_ds = phase_cycles_plot->dat[ds_idx];
        d_s *cur_cycle_ds = cycles_plot->dat[ds_idx];
        get_phase_indexes(record_track, begin_phase, cycle_id, &begin, NULL);
        begin += cutted_points;
        get_phase_indexes(record_track, end_phase, cycle_id, NULL, &end);
        end -= cutted_points;

        for (int i = begin; i < end; i++)
        {
            cur_phase_ds->xd[i - begin] = cur_cycle_ds->xd[i];
            cur_phase_ds->yd[i - begin] = cur_cycle_ds->yd[i];
        }
    }

    extract_bead_info_from_cycle_plot(cycles_plot, &bead_id, &track_id);
    set_plot_title(phase_cycles_plot, "Bead %d Z(t) - track %d - phase %d to phase %d", bead_id, track_id, begin_phase,
                   end_phase);
    set_plot_file(phase_cycles_plot, CYCLE_PHASE_PLOT_FILENAME, bead_id, track_id, end_phase);
    uns_op_2_op_by_type(cycles_plot, IS_X_UNIT_SET, phase_cycles_plot, IS_X_UNIT_SET);
    return phase_cycles_plot;
}

# ifdef ALREADY_DEFINE
int stretch_histogram_max_visible_peak_to_pos(O_p *src_op, d_s *pattern_ds, int k_min, int k_max, float min_value,
        float ref_x)
{
    float max_pos = -1;
    float y_val = 0;
    float max_y_val = -1;
    int max_index = -1;
    float pos = 0;
    int find_max_ret = 0;
    char buf[1024] = {0};
    k_min = k_min > 0 ? k_min : 1;

    if (k_max < k_min)
    {
        return -1;
    }

    for (int k = k_min; k < k_max; ++k)
    {
        if ((pattern_ds->yd[k] > pattern_ds->yd[k - 1]) && (pattern_ds->yd[k] >= pattern_ds->yd[k + 1])
                && (pattern_ds->yd[k] > min_value))
        {
            find_max_ret = find_max_around(pattern_ds->yd, pattern_ds->nx, k, &pos, &y_val, NULL);

            if (find_max_ret == 0 && y_val > max_y_val)
            {
                max_y_val = y_val;
                max_pos = pos;
                max_index = k;
            }
        }
    }

    if (max_index >= 0)
    {
        // ax = offset, dx = multi, rx = div
        float ax =  0;
        float dx = ref_x;
        float rx = pattern_ds->xd[max_index] * (src_op->dx == 0 ? 1 : src_op->dx);
        float drx, tmp;
        drx = dx / rx;
        tmp = (src_op->dx != 0) ? ax / src_op->dx : ax;

        for (int i = 0; i < pattern_ds->nx ; i++)
        {
            pattern_ds->xd[i] =  tmp + drx * pattern_ds->xd[i];

            if (pattern_ds->xe)
            {
                pattern_ds->xe[i] = drx * pattern_ds->xe[i];
            }
        }

        snprintf(buf, sizeof(buf), "Bp->Nm: %.3f - %s", 1. / drx, pattern_ds->source);
        free(pattern_ds->source);
        pattern_ds->source = strdup(buf);
    }

    return 0;
}
# endif
int align_hybridization_pattern_peak(d_s *pattern_ds, int peak_index, float peak_min_threshold,
                                     O_p *phase_cycle_plot)
{
    float max_pos = 0;
    float x_val_of_y_max = 0;
    float max_y_val = 0;
    int find_max_ret = 0;
    assert(pattern_ds);
    int k = 1;

    for (k = 1; k < pattern_ds->nx - 1; k++)
    {
        if ((pattern_ds->yd[k] > pattern_ds->yd[k - 1]) && (pattern_ds->yd[k] >= pattern_ds->yd[k + 1])
                && (pattern_ds->yd[k] > peak_min_threshold))
        {
            //win_printf("k = %d y %g",k,dsi->yd[k]);
            find_max_ret = find_max_around(pattern_ds->yd, pattern_ds->nx, k, &max_pos, &max_y_val, NULL);

            if (find_max_ret == 0)
            {
                if (peak_index == 0)
                {
                    break;
                }

                peak_index--;
            }
        }
    }

    if (peak_index == 0 && k < pattern_ds->nx - 1)
    {
        int pk = (int)max_pos;
        int pk1 = pk + 1;
        float p = (float)pk1 - max_pos;
        pk = (pk < 0) ? 0 : pk;
        pk = (pk < pattern_ds->nx) ? pk : pattern_ds->nx - 1;
        pk1 = (pk1 < 0) ? 0 : pk1;
        pk1 = (pk1 < pattern_ds->nx) ? pk1 : pattern_ds->nx - 1;
        x_val_of_y_max = p * pattern_ds->xd[pk] + (1 - p) * pattern_ds->xd[pk1];

        for (int i = 0; i < pattern_ds->nx; i++)
        {
            pattern_ds->xd[i] -= x_val_of_y_max;
        }

        if (phase_cycle_plot)
        {
            d_s *ds = nullptr;

            for (int i = 0; i < phase_cycle_plot->n_dat; i++)
            {
                d_s *ds = phase_cycle_plot->dat[i];

                for (int j = 0; j < ds->ny; ++j)
                {
                    ds->yd[j] -= x_val_of_y_max;
                }
            }

            phase_cycle_plot->iopt2 &= ~X_LIM;
            phase_cycle_plot->iopt2 &= ~Y_LIM;
            phase_cycle_plot->x_lo = 0;
            phase_cycle_plot->x_hi = 0;
            phase_cycle_plot->y_lo = 0;
            phase_cycle_plot->y_hi = 0;
            phase_cycle_plot->need_to_refresh = 1;
        }

        return 0;
    }
    else
    {
        return -1;
    }
}
d_s *get_methylation_time_by_cycle(O_p *cycle_phase_plot, O_p *time_by_cycle_histo_op, float
                                   min_y, float max_y, int peak_id, int min_num_point)
{
    d_s *time_by_cycle_histo_ds = time_by_cycle_histo_op->dat[0];
    float min = 0;
    float max = 0;
    int mini = -1;
    int maxi = -1;
    int k = 0;
    int bead_id = 0;
    int track_id = 0;
    int phase_id = 0;
    int num_pt = 0;

    for (int cycle_idx = 0; cycle_idx < cycle_phase_plot->n_dat; cycle_idx++)
    {
        min = FLT_MAX;
        max = -FLT_MAX;
        mini = -1;
        maxi = -1;
        num_pt = 0;
        d_s *cycle_ds = cycle_phase_plot->dat[cycle_idx];

        // eliminate too small ds
        if (cycle_ds->nx < 2 || cycle_ds->ny < 2)
        {
            continue;
        }

        if (cycle_ds->xe && time_by_cycle_histo_ds->ye == NULL)
        {
            alloc_data_set_y_error(time_by_cycle_histo_ds);
        }

        for (int i = 0; i < cycle_ds->nx ; i++)
        {
            //eliminating point out of the current peak
            if (cycle_ds->yd[i] < min_y || cycle_ds->yd[i] > max_y)
            {
                continue;
            }

            if (cycle_ds->xd[i] < min)
            {
                min = cycle_ds->xd[i];
                mini = i;
            }

            if (cycle_ds->xd[i] > max)
            {
                max = cycle_ds->xd[i];
                maxi = i;
                num_pt++;
            }
        }

        if (num_pt >= min_num_point && mini != -1 && mini != maxi)
        {
            time_by_cycle_histo_ds->yd[k] = max - min;
            time_by_cycle_histo_ds->xd[k] = cycle_idx;

            if (cycle_ds->xe)
            {
                time_by_cycle_histo_ds->ye[k] = sqrt(cycle_ds->xe[mini] * cycle_ds->xe[mini] + cycle_ds->xe[maxi] * cycle_ds->xe[maxi]);
            }

            k++;
        }
    }

    time_by_cycle_histo_ds->nx = k;
    time_by_cycle_histo_ds->ny = k;
    extract_bead_info_from_phase_cycle_plot(cycle_phase_plot, &bead_id, &track_id, &phase_id);
    set_plot_title(time_by_cycle_histo_op, "\\stack{{Bead %d - track %d - phase %d - peak %d}{"
                   "Methylation Time by Cycle}{y=[%f, %f]}}",
                   bead_id,
                   track_id, phase_id, peak_id, min_y, max_y);
    set_plot_file(time_by_cycle_histo_op, HYB_TIME_BY_CYCLE_PLOT_FILENAME, bead_id, track_id,
                  phase_id, peak_id);
    uns_op_2_op_by_type(cycle_phase_plot, IS_X_UNIT_SET, time_by_cycle_histo_op, IS_Y_UNIT_SET);
    time_by_cycle_histo_op->need_to_refresh = 1;
    return time_by_cycle_histo_ds;
}

MaxPeaksVector *get_local_maxima_x_sorted_data(d_s *data, float mthres)
{
    float max_pos = 0;
    float x_val_of_y_max = 0;
    float max_y_val = 0;
    int find_max_ret = 0;
    MaxPeaksVector *maxvect = new MaxPeaksVector();

    for (int k = 1; k < data->nx - 1; k++)
    {
        if ((data->yd[k] > data->yd[k - 1]) && (data->yd[k] >= data->yd[k + 1])
                && (data->yd[k] > mthres))
        {
            //win_printf("k = %d y %g",k,dsi->yd[k]);
            find_max_ret = find_max_around(data->yd, data->nx, k, &max_pos, &max_y_val, NULL);

            if (find_max_ret == 0)
            {
                int pk = (int)max_pos;
                int pk1 = pk + 1;
                float p = (float)pk1 - max_pos;
                pk = (pk < 0) ? 0 : pk;
                pk = (pk < data->nx) ? pk : data->nx - 1;
                pk1 = (pk1 < 0) ? 0 : pk1;
                pk1 = (pk1 < data->nx) ? pk1 : data->nx - 1;
                x_val_of_y_max = p * data->xd[pk] + (1 - p) * data->xd[pk1];
                maxvect->push_back(std::pair<float, float>(x_val_of_y_max, max_y_val));
            }
        }
    }

    if (maxvect->size() == 0)
    {
        delete maxvect;
        return 0;
    }

    return maxvect;
}
int get_methylation_time_histograms(pltreg *pr,
                                    O_p *cycle_phase_plot,
                                    d_s *hyb_pattern_ds,
                                    float histogram_bin,
                                    float sigma,
                                    float mthres,
                                    MethylDataVector& metdatas,
                                    const char *save_folder)
{
    MaxPeaksVector *maxPosVect = 0;
    std::vector<O_p *> time_histograms;
    O_p *methylation_time_by_cycle_op = NULL;
    O_p *cur_time_histogram = NULL;
    d_s *cur_met_time_by_cycle = NULL;
    double af = 0;
    double tau = 0.1;
    double tau_err_rel = 0.001;
    float peak_pos = 0;
    float peak_height = 0.0;
    int bead_id = 0;
    int track_id = 0;
    int phase_id = 0;
    double chi2 = 0;
    char file[512] = {0};
    extract_bead_info_from_phase_cycle_plot(cycle_phase_plot, &bead_id, &track_id, &phase_id);
    maxPosVect = get_local_maxima_x_sorted_data(hyb_pattern_ds, mthres);

    for (auto cur_hyb : *maxPosVect)
    {
        float cur_hyb_pos = cur_hyb.first;
        peak_height = cur_hyb.second;
        // resetting tau
        peak_pos = cur_hyb_pos * 10e2; // TODO : Verify unit before conversion in nanometer.
        methylation_time_by_cycle_op = create_and_attach_one_plot(pr, cycle_phase_plot->n_dat, cycle_phase_plot->n_dat, 0);
        // we take point around the average of the hybridation peak, using the fact that the noise
        // has a gaussian distribution.
        float min_point_pos = cur_hyb_pos - 3 * sigma;
        float max_point_pos = cur_hyb_pos + 3 * sigma;
        cur_met_time_by_cycle = get_methylation_time_by_cycle(cycle_phase_plot,
                                methylation_time_by_cycle_op,
                                min_point_pos, max_point_pos,
                                peak_pos, 4);
        QuickSort_double(cur_met_time_by_cycle->yd, cur_met_time_by_cycle->xd, 0,
                         cur_met_time_by_cycle->ny -1);
        cur_time_histogram = histogram_vc(pr, methylation_time_by_cycle_op, cur_met_time_by_cycle, histogram_bin, 0, 0, 2);

        if (cur_time_histogram)
        {
            tau = 0.1;
            tau_err_rel = 0.001;
            fit_a_exp_t_over_tau_with_er_full(cur_time_histogram, cur_time_histogram->dat[2], &af, &tau, tau_err_rel, 0, &chi2,
                                              false);
            tau = 0.1;
            tau_err_rel = 0.001;
            fit_a_exp_t_over_tau_with_er_full(cur_time_histogram, cur_time_histogram->dat[1], &af, &tau, tau_err_rel, 0, &chi2,
                                              false);
            set_plot_title(cur_time_histogram, "\\stack{{Bead %d - track %d - phase %d - peak %d}{"
                           "Methylation time histogram}{y=[%f, %f]}}",
                           bead_id,
                           track_id, phase_id, peak_pos, min_point_pos, max_point_pos);
            set_plot_file(cur_time_histogram, METHYL_HISTOGRAM_PLOT_FILENAME, bead_id, track_id,
                          phase_id, peak_pos, tau);

            if (tau < 0)
            {
                printf("lol");
            }

            auto search = beadNmBaseRateMap.find(bead_id);
            float nmBaseRate = (search != beadNmBaseRateMap.end() ? search->second : 1);
            float avg = 0;

            for (int i = 0; i < cur_met_time_by_cycle->ny; ++i)
            {
                avg += cur_met_time_by_cycle->yd[i];
            }

            avg /= cur_met_time_by_cycle->ny;
            MethylationData metdata;
            metdata.bead_id = bead_id;
            metdata.track_id = track_id;
            metdata.phase_id = phase_id;
            metdata.peak_pos = peak_pos;
            metdata.peak_height = peak_height;
            metdata.cycle_count = cycle_phase_plot->n_dat;
            metdata.cycle_with_hyb_count = cur_met_time_by_cycle->nx;
            metdata.hyb_min_y = min_point_pos;
            metdata.hyb_max_y = max_point_pos;
            metdata.exp_a = af;
            metdata.exp_tau = tau;
            metdata.exp_chi2 = chi2;
            metdata.median_time = cur_met_time_by_cycle->yd[cur_met_time_by_cycle->ny / 2] * methylation_time_by_cycle_op->dy;
            metdata.firstquartile_time = cur_met_time_by_cycle->yd[cur_met_time_by_cycle->ny / 4] * methylation_time_by_cycle_op->dy;
            metdata.lastquartile_time = cur_met_time_by_cycle->yd[(int) (cur_met_time_by_cycle->ny*(3. / 4.))] * methylation_time_by_cycle_op->dy;
            metdata.max_time = cur_met_time_by_cycle->yd[cur_met_time_by_cycle->ny - 1] * methylation_time_by_cycle_op->dy;
            metdata.min_time = cur_met_time_by_cycle->yd[0] * methylation_time_by_cycle_op->dy;

            metdata.avg_time = avg * methylation_time_by_cycle_op->dy;
            metdata.nmBaseRate = nmBaseRate;
            metdatas.push_back(metdata);
            printf("dy : %f\n", methylation_time_by_cycle_op->dy);

            if (save_folder)
            {
                set_op_path(cur_time_histogram, save_folder);
                build_full_file_name(file, sizeof(file), cur_time_histogram->dir, cur_time_histogram->filename);
                save_one_plot(cur_time_histogram, file);
            }
        }

        if (save_folder)
        {
            set_op_path(methylation_time_by_cycle_op, save_folder);
            build_full_file_name(file, sizeof(file), methylation_time_by_cycle_op->dir, methylation_time_by_cycle_op->filename);
            save_one_plot(methylation_time_by_cycle_op, file);
            remove_data_from_pltreg(pr, IS_ONE_PLOT, (void *) methylation_time_by_cycle_op);
            //free_one_plot(methylation_time_by_cycle_op);
            methylation_time_by_cycle_op = NULL;
        }
    }

    return 0;
}
int export_methylation_data_to_csv(const char *filename, MethylDataVector& mdv)
{
    std::ofstream ofile;
    std::locale comma_locale(std::locale(), new comma_numpunct());
    ofile.open(filename, std::ios_base::out);
    ofile.imbue(comma_locale);
    int cur_line = 2;
    ofile << "Bead Id;Track Id;Used Phase;Peak Pos;Peak Height;Hybridization minimum Y;Hybridization maximum Y;"
          << "Total Number of cycle;Cycle with hybridization;Hybridization rate;"
          << "exp a param;exp tau param;chi square;Median Time;FirstQuartileTime;LastQuartileTime;MinTime;MaxTime;Avg Time;nmBaseRate" << std::endl;

    for (MethylationData met : mdv)
    {
        ofile << met.bead_id << ";" << met.track_id << ";" << met.phase_id << ";"
              << met.peak_pos << ";"
              << met.peak_height << ";"
              << met.hyb_min_y << ";" << met.hyb_max_y << ";"
              << met.cycle_count << ";" << met.cycle_with_hyb_count
              << ";=I" << cur_line << "/H" << cur_line << ";" // FORMULA
              << met.exp_a << ";"
              << met.exp_tau << ";" << met.exp_chi2 << ";"
              << met.median_time << ";" 
              << met.firstquartile_time << ";"
              << met.lastquartile_time << ";"
              << met.min_time << ";"
              << met.max_time << ";"
              << met.avg_time << ";"
              << met.nmBaseRate << std::endl;
        cur_line++;
    }

    ofile.close();
    return 0;
}

void import_beads_stetchings(std::istream& is)
{
    int bead_id = 0;
    float nmBaseRate = 0;

    while (is.good())
    {
        is >> bead_id;
        is >> nmBaseRate;
        beadNmBaseRateMap[bead_id] = nmBaseRate;
    }
}
