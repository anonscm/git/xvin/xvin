#include "gui.hh"
#include "methylstat.hh"
#include "iostream"
#include "fstream"

PXV_ARRAY(MENU, quicklink_menu);

MENU *methylstat_plot_menu(void)
{
    static MENU mn[32];

    if (mn[0].text != NULL)
    {
        return mn;
    }

    add_item_to_menu(mn, "align cycles on phase", do_align_cycle_on_phase, NULL, 0, NULL);
    add_item_to_menu(mn, "align cycle phase to zero", do_align_cycle_phase_to_zero, NULL, 0, NULL);
    add_item_to_menu(mn, "create hybridization histogram from phase", do_create_hybridization_pattern_from_phase, NULL, 0,
                     NULL);
    add_item_to_menu(mn, "align hybridization histogram to a peak", do_align_hybridization_pattern_peak, NULL, 0, NULL);
    add_item_to_menu(mn, "stretch histogram peak to a value", do_stretch_histograms, NULL, 0, NULL);
    add_item_to_menu(mn, "get metylation histogram for one hybridization",
                     do_get_methylation_time_by_cycle_for_one_hybridization, NULL, 0, NULL);
    add_item_to_menu(mn, "get metylation histograms", do_get_methylation_time_histograms, NULL, 0, NULL);
    add_item_to_menu(mn, "import bead stretching", do_import_beads_stretchings, NULL, 0, NULL);
    add_item_to_menu(quicklink_menu, "get metylation histograms", do_get_methylation_time_histograms, NULL, 0, NULL);
    return mn;
}

int methylstat_main(int argc, char **argv)
{
    (void) argc;
    (void) argv;
    add_plot_treat_menu_item("methylstat", NULL, methylstat_plot_menu(), 0, NULL);
    return D_O_K;
}

int methylstat_unload(int argc, char **argv)
{
    (void) argc;
    (void) argv;
    remove_item_to_menu(plot_treat_menu, "methylstat", NULL, NULL);
    return D_O_K;
}
int do_import_beads_stretchings(void)
{
    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    /* we first find the data that we need to transform */
    char *nmbr_filename = open_one_file_config("Open nmbaserate file ", NULL, "txt File\0*.txt\0All file\0*.*\0\0",
                          "METHYLSTAT",
                          "NMBR-FILE-LOAD");
    std::ifstream ofile;
    ofile.open(nmbr_filename, std::ios_base::out);
    import_beads_stetchings(ofile);
    ofile.close();
    return 0;
}

int do_align_cycle_on_phase(void)
{
    int i;
    int allegro_return = 0;
    g_record *g_r = NULL;
    O_p *op = NULL;
    d_s *dsi;
    pltreg *pr = NULL;
    int bead_id = 0;
    int track_id = 0;
    int all_plot = 1;
    int phase = 3;
    int cutted_points = 3;
    int plot_count = 0;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
    {
        return win_printf_OK("cannot find data");
    }

    allegro_return =
        win_scanf("%b all plot\n Alignement phase %5d\n \n number of eliminated edge point %5d", &all_plot,
                  &phase, &cutted_points);

    if (allegro_return == WIN_CANCEL)
    {
        return OFF;
    }

    g_r = find_g_record_in_pltreg(pr);

    if (g_r == NULL)
    {
        win_printf_OK("no opened record\n");
        return -1;
    }

    if (all_plot)
    {
        i = 0;
        plot_count = pr->n_op;
    }
    else
    {
        i = pr->cur_op;
        plot_count = i + 1;
    }

    do
    {
        O_p *cur_op = pr->o_p[i];

        if (cur_op->filename != NULL && sscanf(cur_op->filename, "Z(t)bd%dtrack%d.gr", &bead_id, &track_id) == 2
                && bead_id < g_r->n_bead && track_id == g_r->n_rec)
        {
            align_cycles_on_phase(g_r, cur_op, phase, cutted_points);
        }

        ++i;
    }
    while (all_plot && i < pr->n_op);

    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}

int do_align_cycle_phase_to_zero(void)
{
    int i;
    int allegro_return = 0;
    g_record *g_r = NULL;
    O_p *op = NULL;
    d_s *dsi;
    pltreg *pr = NULL;
    int bead_id = 0;
    int track_id = 0;
    int all_plot = 1;
    int phase = 3;
    int cutted_points = 3;
    int plot_count = 0;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
    {
        return win_printf_OK("cannot find data");
    }

    allegro_return =
        win_scanf("%b all plot\n Alignement phase %5d\n \n number of eliminated edge point %5d", &all_plot,
                  &phase, &cutted_points);

    if (allegro_return == WIN_CANCEL)
    {
        return OFF;
    }

    g_r = find_g_record_in_pltreg(pr);

    if (g_r == NULL)
    {
        win_printf_OK("no opened record\n");
        return -1;
    }

    if (all_plot)
    {
        i = 0;
        plot_count = pr->n_op;
    }
    else
    {
        i = pr->cur_op;
        plot_count = i + 1;
    }

    do
    {
        O_p *cur_op = pr->o_p[i];

        if (cur_op->filename != NULL && sscanf(cur_op->filename, "Z(t)bd%dtrack%d.gr", &bead_id, &track_id) == 2
                && bead_id < g_r->n_bead && track_id == g_r->n_rec)
        {
            align_cycle_phase_to_zero(g_r, cur_op, phase, cutted_points);
        }

        ++i;
    }
    while (all_plot && i < pr->n_op);

    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}

int do_create_hybridization_pattern_from_phase(void)
{
    int i;
    int allegro_return = 0;
    g_record *g_r = NULL;
    O_p *op = NULL;
    O_p *merged_histogram = nullptr;
    d_s *dsi;
    pltreg *pr = NULL;
    int bead_id = 0;
    int track_id = 0;
    int phase_id = 0;
    int all_plot = 1;
    int begin_phase = 4;
    int end_phase = 5;
    int cutted_points = 3;
    float biny = 0.003;
    int plot_count = 0;
    int merge_histogram = 0;
    int peak_index = 0;
    int align_peak = 0;
    float peak_min_threshold = 1;
    static int norm_histo = 1;
    static int propagate_phase_cycle_plot = 0;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
    {
        return win_printf_OK("cannot find data");
    }

    allegro_return =
        win_scanf("%b all plot\n phase used to create hybridization pattern from %5d to %5d\n bin size %5f \n %b norm histo\n number of eliminated edge point %5d \n %b merge histogram in one plot\n"
                  "\t %b align histogram to a hyb peak \n\t\t index %d min value %f, \n\t %b propagate alignement to phase cycle plot",
                  &all_plot, &begin_phase, &end_phase, &biny, &norm_histo, &cutted_points, &merge_histogram, &align_peak, &peak_index,
                  &peak_min_threshold, &propagate_phase_cycle_plot);

    if (allegro_return == WIN_CANCEL)
    {
        return OFF;
    }

    g_r = find_g_record_in_pltreg(pr);

    if (g_r == NULL)
    {
        win_printf_OK("no opened record\n");
        return -1;
    }

    if (all_plot)
    {
        i = 0;
        plot_count = pr->n_op;
    }
    else
    {
        i = pr->cur_op;
        plot_count = i + 1;
    }

    if (merge_histogram)
    {
        merged_histogram = create_and_attach_one_plot(pr, 1, 1, 0);
        merged_histogram->title = strdup("Merged histograms");
        merged_histogram->filename = strdup("merged-histograms.gr");
    }

    do
    {
        O_p *cur_op = pr->o_p[i];

        if (cur_op->filename != NULL &&
                extract_bead_info_from_cycle_plot(cur_op, &bead_id, &track_id) &&
                bead_id < g_r->n_bead && track_id == g_r->n_rec)
        {
            O_p *out_op = create_hybridization_pattern_from_phase(g_r, pr, cur_op,
                          begin_phase, end_phase, biny,
                          cutted_points, NULL, norm_histo);

            if (out_op && align_peak && out_op->filename != NULL
                    && extract_bead_info_from_hyb_pattern_plot(out_op, &bead_id, &track_id, &phase_id))
            {
                O_p *prop_op = propagate_phase_cycle_plot ? cur_op : nullptr;
                align_hybridization_pattern_peak(out_op->dat[0], peak_index, peak_min_threshold, prop_op);
                out_op->need_to_refresh = 1;
            }

            if (merge_histogram && out_op)
            {
                d_s *cur_ds = create_and_attach_one_ds(merged_histogram, 1, 1, 0);
                duplicate_data_set(out_op->dat[0], cur_ds);
                uns_op_2_op_by_type(out_op, IS_X_UNIT_SET, merged_histogram, IS_X_UNIT_SET);
            }
        }

        ++i;
    }
    while (i < plot_count);

    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}
# ifdef ALREADY_DEFINE
int do_stretch_histograms(void)
{
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    float ref_x = 0;
    int allegro_return = WIN_OK;
    int k_min = -1;
    int k_max = -1;
    int specific_color = 0;
    int color = 0;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2)
    {
        return win_printf_OK("cannot find data");
    }

    allegro_return = win_scanf("ref x value %5f\n %b only a specified color ",
                               &ref_x, &specific_color);

    if (allegro_return == WIN_CANCEL)
    {
        return OFF;
    }

    if (specific_color)
    {
        color = color_chooser();
    }

    if (color == -1)
    {
        return OFF;
    }

    for (int i = 0; i < op->n_dat; ++i)
    {
        ds = op->dat[i];

        if (ds->color == color)
        {
            for (int j = 0; j  < ds->nx; ++j)
            {
                if (k_min == -1 && ds->xd[j] > op->x_lo)
                {
                    k_min = j;
                }

                if (k_max == -1 && ds->xd[j] > op->x_hi)
                {
                    k_max = j - 1;
                }
            }

            if (k_min == -1)
            {
                continue;
            }

            if (k_max == -1)
            {
                k_max = ds->nx - 1;
            }

            stretch_histogram_max_visible_peak_to_pos(op, ds, k_min, k_max, op->y_lo, ref_x);
        }
    }

    refresh_plot(pr, UNCHANGED);
    return 0;
}
# endif
int do_align_hybridization_pattern_peak(void)
{
    int i;
    int allegro_return = 0;
    O_p *op = NULL;
    d_s *dsi;
    pltreg *pr = NULL;
    int bead_id = 0;
    int track_id = 0;
    int phase_id = 0;
    int all_plot = 0;
    int peak_index = 0;
    int plot_count = 0;
    float peak_min_threshold = 0.1;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
    {
        return win_printf_OK("cannot find data");
    }

    allegro_return = win_scanf("%b all plot\n peak index %5d\n peak minimum threashold %5f\n ", &all_plot, &peak_index,
                               &peak_min_threshold);

    if (allegro_return == WIN_CANCEL)
    {
        return OFF;
    }

    if (all_plot)
    {
        i = 0;
        plot_count = pr->n_op;
    }
    else
    {
        i = pr->cur_op;
        plot_count = i + 1;
    }

    do
    {
        O_p *cur_op = pr->o_p[i];

        if (cur_op->filename != NULL && extract_bead_info_from_hyb_pattern_plot(cur_op, &bead_id, &track_id, &phase_id))
        {
            align_hybridization_pattern_peak(cur_op->dat[0], peak_index, peak_min_threshold, nullptr);
            cur_op->need_to_refresh = 1;
        }

        ++i;
    }
    while (i < plot_count);

    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}

int do_get_methylation_time_by_cycle_for_one_hybridization(void)
{
    int i;
    int allegro_return = 0;
    g_record *g_r = NULL;
    O_p *cur_op = NULL;
    O_p *time_histo_op = NULL;
    d_s *dsi;
    pltreg *pr = NULL;
    int bead_id = 0;
    int track_id = 0;
    int all_plot = 1;
    int begin_phase = 4;
    int end_phase = 5;
    int cutted_points = 3;
    float min_y = 0.25;
    float max_y = 0.30;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr", &pr) != 1)
    {
        return win_printf_OK("cannot find data");
    }

    g_r = find_g_record_in_pltreg(pr);

    if (g_r == NULL)
    {
        win_printf_OK("no opened record\n");
        return -1;
    }

    allegro_return =
        win_scanf("begin phase %5d\n end phase %5d\n discarded border points %5d \n min y %5f \n max y %5f"/*, &all_plot*/,
                  &begin_phase, &end_phase, &cutted_points, &min_y, &max_y);

    if (allegro_return == WIN_CANCEL)
    {
        return OFF;
    }

    //    if (all_plot)
    //        i = 0;
    //    else
    i = pr->cur_op;
    //    do
    //    {
    cur_op = pr->o_p[i];
    cur_op = get_phase_plot(g_r, pr, cur_op, begin_phase, end_phase, cutted_points);
    time_histo_op = create_and_attach_one_plot(pr, cur_op->n_dat, cur_op->n_dat, 0);
    //        if (cur_op->filename != NULL && sscanf(cur_op->filename, "Z(t)bd%dtrack%d.gr", &bead_id, &track_id) == 2
    //            && bead_id < g_r->n_bead && track_id == g_r->n_rec)
    //        {
    get_methylation_time_by_cycle(cur_op, time_histo_op, min_y, max_y, 0, cutted_points + 1);
    //        }
    //        ++i;
    //    }
    //    while (all_plot && i < pr->n_op);
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}

int do_get_methylation_time_histograms(void)
{
    int i;
    int allegro_return = 0;
    g_record *g_r = NULL;
    O_p *op = NULL;
    d_s *dsi;
    pltreg *pr = NULL;
    int bead_id = 0;
    int track_id = 0;
    static int all_plot = get_config_int("METHYLSTAT", "TIME-HISTO-ALL-PLOT", 1);
    static int begin_phase = get_config_int("METHYLSTAT", "TIME-HISTO-BEGIN-PHASE", 4);
    static int end_phase = get_config_int("METHYLSTAT", "TIME-HISTO-END-PHASE", 5);
    static int cutted_points = get_config_int("METHYLSTAT", "TIME-HISTO-ALL-CUTTED-POINTS", 3);
    static int do_align = get_config_int("METHYLSTAT", "TIME-HISTO-DO-ALIGN", 0);
    static int align_phase = get_config_int("METHYLSTAT", "TIME-HISTO-ALIGN-PHASE", 3);
    static float histogram_bin = get_config_int("METHYLSTAT", "TIME-HISTO-HISTO-BIN", -1);
    static float mthres = get_config_float("METHYLSTAT", "TIME-HISTO-MTHRES", 0.0015);
    static int save_to_csv = get_config_int("METHYLSTAT", "TIME-HISTO-SAVE-TO-CSV", 1);
    static int save_interm_plots = get_config_int("METHYLSTAT", "TIME-HISTO-SAVE-INTERM-PLOTS", 1);
    static int align_on_peak = 0;
    static int align_peak_idx = 0;
    static float align_peak_thres = 10.;
    static int norm_histo = 1;
    float sigma = 0.006;
    int  num_plot = 0;
    char *csv_filename = NULL;
    char *result_folder = NULL;
    char file[512] = {0};
    MethylDataVector metdatas;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
    {
        return win_printf_OK("cannot find data");
    }

    allegro_return = win_scanf("%b all plot\n %b"
                               "align to a phase : phase on which align %5d \n"
                               "phase used to create histogram from %5d to %5d\n"
                               "gaussian sigma %5f\n"
                               "%b align histo to a peak\n"
                               "\t peak index %d minimum threshold %5f\n"
                               "number of eliminated edge point %5d\n"
                               "minimum hibridization histogram threshold %5f\n"
                               "time histogram bin %5f\n"
                               "%b norm histo"
                               "%b save intermediate plots\n"
                               "%b save data to csv\n",
                               &all_plot, &do_align, &align_phase, &begin_phase, &end_phase,
                               &sigma, &align_on_peak, &align_peak_idx, &align_peak_thres, &cutted_points, &mthres, &histogram_bin,
                               &norm_histo,
                               &save_interm_plots,
                               &save_to_csv);

    if (allegro_return == WIN_CANCEL)
    {
        return OFF;
    }

    set_config_int("METHYLSTAT", "TIME-HISTO-ALL-PLOT", all_plot);
    set_config_int("METHYLSTAT", "TIME-HISTO-BEGIN_PHASE", begin_phase);
    set_config_int("METHYLSTAT", "TIME-HISTO-END_PHASE", end_phase);
    set_config_int("METHYLSTAT", "TIME-HISTO-ALL-CUTTED-POINTS", cutted_points);
    set_config_int("METHYLSTAT", "TIME-HISTO-DO-ALIGN", do_align);
    set_config_int("METHYLSTAT", "TIME-HISTO-ALIGN-PHASE", align_phase);
    set_config_int("METHYLSTAT", "TIME-HISTO-HISTO-BIN", histogram_bin);
    set_config_float("METHYLSTAT", "TIME-HISTO-MTHRES", mthres);
    set_config_int("METHYLSTAT", "TIME-HISTO-SAVE-TO-CSV", save_to_csv);
    set_config_int("METHYLSTAT", "TIME-HISTO-SAVE-INTERM-PLOTS", save_interm_plots);
    g_r = find_g_record_in_pltreg(pr);

    if (g_r == NULL)
    {
        win_printf_OK("no opened record\n");
        return -1;
    }

    if (save_interm_plots)
    {
        result_folder = select_folder_config("Select Folder to save intermediate plots", NULL, "METHYLSTAT",
                                             "INTERMED-PLOTS-PATH");
    }

    if (save_to_csv)
    {
        //TOD filename
        strncpy(file, g_r->filename, sizeof(file));
        replace_extension(file, file, "csv", sizeof(file));
        csv_filename = save_one_file_config("Save Methyl data in CSV ", NULL, file, "csv File\0*.csv\0\0", "METHYLSTAT",
                                            "CSV-LAST-SAVE");
    }

    if (all_plot)
    {
        i = 0;
    }
    else
    {
        i = pr->cur_op;
    }

    num_plot = pr->n_op;

    do
    {
        O_p *cur_op = pr->o_p[i];

        if (cur_op->filename != NULL && extract_bead_info_from_cycle_plot(cur_op, &bead_id, &track_id)
                && bead_id < g_r->n_bead && track_id == g_r->n_rec)
        {
            if (do_align)
            {
                align_cycles_on_phase(g_r, cur_op, align_phase, cutted_points);
            }

            O_p *hyb_pattern_op = create_hybridization_pattern_from_phase(g_r, pr, cur_op, begin_phase, end_phase,  sigma,
                                  cutted_points,
                                  result_folder, norm_histo);

            if (hyb_pattern_op)
            {
                if (align_on_peak)
                {
                    align_hybridization_pattern_peak(hyb_pattern_op->dat[0], align_peak_idx, align_peak_thres, cur_op);
                }

                cur_op = get_phase_plot(g_r, pr, cur_op, begin_phase, end_phase, cutted_points);
                get_methylation_time_histograms(pr, cur_op, hyb_pattern_op->dat[0], histogram_bin, sigma, mthres, metdatas,
                                                result_folder);
                export_methylation_data_to_csv(csv_filename, metdatas);

                if (result_folder)
                {
                    remove_data_from_pltreg(pr, IS_ONE_PLOT, (void *)hyb_pattern_op);
                    //free_one_plot(hyb_pattern_op);
                    hyb_pattern_op = NULL;
                }

                remove_data_from_pltreg(pr, IS_ONE_PLOT, (void *)cur_op);
                //free_one_plot(cur_op);
                cur_op = NULL;
            }
        }

        ++i;
    }
    while (all_plot && i < num_plot);

    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}
