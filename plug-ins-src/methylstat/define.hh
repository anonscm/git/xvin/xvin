#pragma once
#include <vector>

typedef struct
{
    int bead_id;
    int track_id;
    int phase_id;
    float peak_pos;
    float peak_height;
    int cycle_count;
    int cycle_with_hyb_count;
    float hyb_min_y;
    float hyb_max_y;
    double exp_a;
    double exp_tau;
    double exp_chi2;
    float median_time;
    float firstquartile_time;
    float lastquartile_time;
    float min_time;
    float max_time;
    float avg_time;
    float nmBaseRate;
} MethylationData;

typedef std::vector<MethylationData> MethylDataVector;
typedef std::vector<std::pair<float,float>> MaxPeaksVector;
