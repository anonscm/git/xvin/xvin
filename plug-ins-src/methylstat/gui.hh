#pragma once
#include "xvin.h"

PXV_FUNC(MENU, *methylstat_plot_menu, (void));
PXV_FUNC(int, methylstat_main, (int argc, char **argv));
PXV_FUNC(int, methylstat_unload, (int argc, char **argv));
PXV_FUNC(int, do_align_cycle_on_phase, (void));
PXV_FUNC(int, do_align_cycle_phase_to_zero, (void));
PXV_FUNC(int, do_create_hybridization_pattern_from_phase, (void));
PXV_FUNC(int, do_align_hybridization_pattern_peak, (void));
PXV_FUNC(int, do_get_methylation_time_by_cycle_for_one_hybridization, (void));
PXV_FUNC(int, do_get_methylation_time_histograms, (void));
PXV_FUNC(int, do_stretch_histograms,(void));
PXV_FUNC(int, do_import_beads_stretchings, (void));
