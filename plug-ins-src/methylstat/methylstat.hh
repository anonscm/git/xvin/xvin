#pragma once
#include <allegro.h>
#include "xvin.h"
#include "../trackBead/record.h"
#include "define.hh"
#include <iostream>

#define CYCLE_PLOT_FILENAME "Z(t)bd%dtrack%d.gr"
#define CYCLE_PHASE_PLOT_FILENAME "Z(t)bd%dtrack%dphase%d.gr"
#define HYB_PATTERN_PLOT_FILENAME  "HybProb(Z)bd%dtrack%dphase%d-hyb-pattern.gr"
#define HYB_TIME_BY_CYCLE_PLOT_FILENAME "MethylTime(cycle)bd%dtrack%dphase%dpeak%d.gr"
#define METHYL_HISTOGRAM_PLOT_FILENAME  "Count(MethylTime(cycle))bd%dtrack%dphase%dpeak%d-tau%lf-meth-histo.gr"
#define CYCLE_DATASET_SOURCE "Bead Cycle %d phase(s):"


PXV_FUNC(bool, extract_cycle_info_from_cycle_dataset, (d_s *cycle_ds, int *cycle_id));
PXV_FUNC(bool, extract_bead_info_from_cycle_plot, (O_p *cycle_plot, int *bead_id, int *track_id));
PXV_FUNC(bool, extract_bead_info_from_phase_cycle_plot, (O_p *cycle_phase_plot, int *bead_id, int *track_id, int *phase_id));
PXV_FUNC(bool, extract_bead_info_from_hyb_pattern_plot, (O_p *hyb_pattern_plot, int *bead_id, int *track_id, int *phase_id));

PXV_FUNC(int, align_cycles_on_phase,(g_record *record_track, O_p *plot, int phase, int cutted_points));
PXV_FUNC(int, align_cycle_phase_to_zero, (g_record *record_track, O_p *plot, int phase, int cutted_points));
PXV_FUNC(O_p, *create_hybridization_pattern_from_phase, (g_record *record_track, pltreg *pr, O_p *cycles_plot, int begin_phase, int end_phase, float biny, int cutted_points, const char *save_folder, bool norm_histo));

PXV_FUNC(int, align_hybridization_pattern_peak, (d_s *histo_ds, int peak_index, float peak_min_threshold,O_p* phase_cycle_plot));

PXV_FUNC(d_s, *get_methylation_time_by_cycle, (O_p *cycle_phase_plot, O_p *time_histogram_op, float min_y, float max_y, int peak_id, int min_num_point));
PXV_FUNC(O_p, *get_phase_plot, (g_record *record_track, pltreg *pr, O_p *cycles_plot, int begin_phase,int end_phase, int cutted_points));
PXV_FUNC(int, get_methylation_time_histograms,(pltreg *pr, O_p *cycle_phase_plot,
                                    d_s *histo_ds,
                                    float histogram_bin,
                                    float sigma,
                                    float mthres,
                                    MethylDataVector& metdatas, const char *save_folder));

PXV_FUNC(int, export_methylation_data_to_csv, (const char *filename, MethylDataVector& mdv));

std::vector<float>* get_median_offset_in_phase(g_record *record_track, O_p *plot, int phase,
        int cutted_points);
PXV_FUNC(int, stretch_histogram_max_visible_peak_to_pos, (O_p *src_op, d_s *pattern_ds, int k_min, int k_max, float min_value, float ref_x));
PXV_FUNC(void, import_beads_stetchings, (std::istream& is));
