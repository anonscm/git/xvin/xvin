#include "xvin.h"

static	char x_root_filename[128] = "L_dep";
static	char f_root_filename[128] = "L_M";
static	char noise_root_filename[128] = "L_noise";
static	char file_extension[16] = ".dat";
static 	int  bool_save_x=1, bool_save_f=1, bool_save_noise=0;

static  int  npts_max_in_file = 1e6;
		int	 current_file_number;
		
FILE *file_x, *file_noise, *file_f;

char 	filename[512];	// tmp for filenaming
float	*data;			// tmp float data

// arrays containing for one cycle/window:
extern double	*f, 	// deterministic forcing
				*noise, // noise
				*x, 	// solution
				*df, 	// derivative of forcing
				*dx;	// derivative of solution

				
/********************************************************/
/* to open the files 									*/
/********************************************************/				
int init_Langevin_files(int ny)
{	int i;
	
	i=win_scanf("{\\pt14\\color{yellow}Saving Langevin data in files}\n\n"
		"%b save forcing, files prefix  : %18s\n"
		"%b save noise, files prefix    : %18s\n"
		"%b save solution, files prefix : %18s\n\n"
		"files extension                : %8s\n"
		"max number of points in a file : %8d",
		&bool_save_f, &f_root_filename, &bool_save_noise, &noise_root_filename, &bool_save_x, &x_root_filename,
		&file_extension, &npts_max_in_file);
	if (i==CANCEL) return(CANCEL);
	
	if (bool_save_f==1)
	{	sprintf(filename, "%s%d%s", f_root_filename, 0, file_extension);
		file_f = fopen(filename,"wb");
	}
		
	if (bool_save_noise==1)
	{	sprintf(filename, "%s%d%s", noise_root_filename, 0, file_extension);
		file_noise = fopen(filename,"wb");
	}
	
	if (bool_save_x==1)
	{	sprintf(filename, "%s%d%s", x_root_filename, 0, file_extension);
		file_x = fopen(filename,"wb");
	}
	
	data = (float*)calloc(ny, sizeof(float));
	
	return(D_O_K);
}




/********************************************************/
/* to save data in the files 							*/
/********************************************************/
int save_Langevin_files(int ny, int j)
{	int k;

	// first, we eventually change file (if the first is full):
	if (current_file_number < (j*ny)/npts_max_in_file)
	{	current_file_number++;

		if (bool_save_f==1)
		{	fclose(file_f);
			sprintf(filename, "%s%d%s", f_root_filename, current_file_number, file_extension);
			file_f = fopen(filename,"wb");
		}
		
		if (bool_save_noise==1)
		{	fclose(file_noise);
			sprintf(filename, "%s%d%s", noise_root_filename, current_file_number, file_extension);
			file_noise = fopen(filename,"wb");
		}
	
		if (bool_save_x==1)
		{	fclose(file_x);
			sprintf(filename, "%s%d%s", x_root_filename, current_file_number, file_extension);
			file_x = fopen(filename,"wb");
		}
	}
	
	// then we save:
	if (bool_save_f==1)
	{	for (k=0; k<ny; k++) data[k]=(float)f[k];
		fwrite(data, sizeof(float), ny, file_f);
	}
	if (bool_save_noise==1)
	{	for (k=0; k<ny; k++) data[k]=(float)noise[k];
		fwrite(data, sizeof(float), ny, file_noise);
	}
	if (bool_save_x==1)
	{	for (k=0; k<ny; k++) data[k]=(float)x[k];
		fwrite(data, sizeof(float), ny, file_x);
	}
		
	return(current_file_number);
}


/********************************************************/
/* to close the files 									*/
/********************************************************/
int free_Langevin_files(void)
{
	if (bool_save_x==1)		fclose(file_x);
	if (bool_save_f==1) 	fclose(file_f);
	if (bool_save_noise==1) fclose(file_noise);
	
	free(data);
	
	return(0);
}

