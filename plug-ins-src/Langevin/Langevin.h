#ifndef _LANGEVIN_H_
#define _LANGEVIN_H_

#define LANGEVIN_JARZYNSKI  0x0100 // to compute Jarzynsi stuff
#define LANGEVIN_MULTIPLE   0x0200 // to integrate several times
#define LANGEVIN_SAVE		0x0400 // to save data

PXV_FUNC(MENU*, Langevin_plot_menu, (void));
PXV_FUNC(int, Langevin_main, 			(int argc, char **argv));
PXV_FUNC(int, Langevin_unload,		(int argc, char **argv));
PXV_FUNC(int, do_integrate_langevin_equation, (void));

							
void spit(char *message);
#endif

