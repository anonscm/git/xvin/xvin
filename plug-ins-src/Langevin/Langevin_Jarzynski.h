int ask_for_Jarzynski_parameters(int ny, float f_acq);
int autodetect_Jarzynski_values(int ny, double *f, double *df, double *x, double *dx, double dt, double k, double amp_f, double amp_noise);
int init_Langevin_Jarzynski(void);
int accumulate_data_for_Jarzynski(int ny, int bool_Fourier, double *f, double *df, double *x, double *dx, double dt);
int save_Jarzynski_data_in_datasets_and_free(O_p *op1, int n_cycles);


