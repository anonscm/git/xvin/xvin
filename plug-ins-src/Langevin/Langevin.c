#ifndef _LANGEVIN_C_
#define _LANGEVIN_C_

#include "allegro.h"
#include "xvin.h"
#include "fft_filter_lib.h"


#include <gsl/gsl_spline.h>
#include <gsl/gsl_statistics.h>
#include <fftw3.h>		
#include <gsl/gsl_rng.h>			// for Langevin equation
#include <gsl/gsl_randist.h>		// for Langevin equation
// #include <gsl/gsl_histogram.h>

/* If you include other plug-ins header do it here*/ 
#include "Langevin_Jarzynski.h" // here, because it includes 'hist.h' from plug'in 'hist'

/* But not below this define */
#define BUILDING_PLUGINS_DLL
#include "Langevin.h"
#include "Langevin_math.h"
#include "Langevin_files.h"

// for the Langevin equation:
const gsl_rng_type *random_generator_type;		/* type of the random generator 	*/

/*************************************************************************************/
// nombre de pas d'integrations dans 1 boucle de Runge-Kutta:
extern int nbpas_RK4;	// defined in Langevin_math.c
/*************************************************************************************/
// parameters of the equation:
static double 	m=3e-10, nu=1e-7, k=5.5e-4, amp_f=1, amp_noise=0.02;
/*************************************************************************************/
// arrays containing for one cycle/window:
double			*f, 	// deterministic forcing
				*noise, // noise
				*x, 	// solution
				*df, 	// derivative of forcing
				*dx;	// derivative of solution

int    n_variables=1;
				

int do_integrate_langevin_equation(void)
{
	pltreg 	*pr = NULL;
	O_p 	*op1=NULL, 		// input plot : contains the forcing over 1 cycle, and will contain the solution over 1 cycle
			*op_long=NULL; 	// output plot : will contain the slution over several cycles, if asked for.
	d_s 	*ds_f, *ds_noise=NULL, *ds2=NULL;
	gsl_rng *r=NULL;  			/* random generator 			*/
	double	dt;
	float	float_m=(float)m, float_nu=(float)nu, float_k=(float)k, float_amp_f=(float)amp_f, float_amp_noise=(float)amp_noise;
	float	f_acq;
register int 	i,j;
	int		ny, n_cycles=1000;
	int		n_fft;
static int 	bool_hanning=0, bool_filter_Fourier=0, bool_generate_noise=1, do_derivate_Fourier=0, 
			bool_seed_random_with_clock=1,		// random generator for the noise is set with clock, or with 0
			do_plot_d_forcing=0, do_plot_d_x=0;
	int		bool_do_Jarzynski=0, 
			bool_do_save_data=0,
			bool_do_multiple_cycles=0, 
			bool_plot_multiple_cycles=0;
static int	bool_Fourier=0;			
	int		index, i_f, i_noise;
	char	s[256];
	

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;
	
	if (index & LANGEVIN_JARZYNSKI) bool_do_Jarzynski=1; // we'll do more than just integrating...
	if (index & LANGEVIN_MULTIPLE)  bool_do_multiple_cycles=1;
	if (index & LANGEVIN_SAVE)	{	bool_do_save_data=1;
									bool_do_multiple_cycles=1;
								}
	
	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine integrates a Langevin eq. with forcing\n"
        				"in Fourier space or with RK4\n"
					"{\\pt14 m.d^2x/dt^2 + \\nu .dx/dt + k.x = f(t) + \\eta(t)}\n\n"
					"the forcing f(t) is read from a dataset.\n"
					"the noise \\eta(t) is either read from a dataset or generated.\n"
					"a new dataset is created for the solution x\n"
					"several treatments can be applied automatically.");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds_f) != 3)	
									return(win_printf_OK("cannot plot region"));
	i_f     = op1->cur_dat;
	ny = ds_f->ny;	// number of points in time series 
	if (ny<4) 						return(win_printf_OK("not enough points !"));
	
	dt = (double)(ds_f->xd[ny-1]-ds_f->xd[0])/(ny-1);
	if ( (ds_f->xd[2]-ds_f->xd[1])!=(dt) ) 	return(win_printf_OK("dt is not constant ?!"));
	if (dt==0)						return(win_printf_OK("dt is zero !"));
	n_fft=ny;
	f_acq=(float)(1./dt);

	i=win_scanf("{\\pt14\\color{yellow}Langevin equation}\n"
			  "integration with %R RK4 or %r in Fourier space\n\n"
			  "{\\pt14 m.d^2x/dt^2 + \\nu .dx/dt + k.x = f(t) + \\eta(t)}\n\n"
			  "the forcing f(t) is read from active dataset.\n"
			  "%R read the noise from preceeding dataset (active-1)\n"
			  "%r generate the noise (gaussian, \\sigma=1) (%b seed with clock)\n\n"				
	 		  "mass m %6f     damping \\nu  %6f    spring constant k %6f \n"
			  "deterministic forcing amplitude %6f   noise amplitude %6f \n\n"
			  "%b use Hanning window\n"
			  "%b filter Fourier transforms (to avoid Gibbs oscillations) (not working!)\n"
			  "%R derivate f using dinite differences or %r using Fourier\n"
			  "%b plot df, derivative of the forcing\n"
			  "%b plot dx, derivative of the solution", 
			  	&bool_Fourier,
				&bool_generate_noise, &bool_seed_random_with_clock, 
				&float_m, &float_nu, &float_k, &float_amp_f, &float_amp_noise, 
				&bool_hanning, &bool_filter_Fourier, &do_derivate_Fourier, &do_plot_d_forcing, &do_plot_d_x);
	if (i==CANCEL) return(D_O_K);
	m=(double)float_m;
	nu=(double)float_nu;
	k=(double)float_k;
	amp_f=(double)float_amp_f;;
	amp_noise=(double)float_amp_noise;
				
	
	init_Langevin_equation_common(ny, 1);	// single Langevin eq.
	if (bool_Fourier==1)
	{	init_Langevin_equation_Fourier(ny, bool_filter_Fourier);
	}
	else
	{	i=CANCEL+1;
		if ( (m/nu) < dt/2.) i=win_printf("relaxation time is too short compared to dt!\n"
					"m / \\nu = %6g < dt/2 = %6g", m/nu, dt/2.);
		if (i==CANCEL) return(D_O_K);
		if (m!=0)
		{ 	if ( 2*M_PI*sqrt(m/k) < dt/4.) i=win_printf("caracteristic time is too short compared to dt!\n"
					"\\sqrt{m/k} = %6g < dt/2 = %6g", sqrt(m/k), dt/2.);
			if (i==CANCEL) return(D_O_K);
		}
		
		nbpas_RK4 = 1;
	}

	if (bool_do_Jarzynski==1) ask_for_Jarzynski_parameters(ny, f_acq); // ask for indices of startin (s) and ending (e) points
	
	if (bool_generate_noise==0)	
	{	if (op1->n_dat<2)				return(win_printf_OK("not enough datasets\n"));

		i_noise = (i_f>0) ? (i_f-1) : (op1->n_dat-1);
		ds_noise = op1->dat[i_noise];
		if (ny != ds_noise->ny)			return(win_printf_OK("datasets %d and %d do not have the same number of points!", i_f, i_noise));
		i=win_printf_OK("forcing f(t) is read from dataset %d (active).\n"
						 "noise \\eta(t) is read from dataset %d.\n", 
						 i_f, i_noise);
		if(i==CANCEL) 					return(D_O_K);
	}
	
   	// we load or create the noise:
	if (bool_generate_noise==1)	
	{	random_generator_type = gsl_rng_default;
		gsl_rng_env_setup();	// 3 lines to init the random number generator
	  	r = gsl_rng_alloc(random_generator_type);
	  	if (bool_seed_random_with_clock==1) gsl_rng_set(r, (unsigned long int)clock());
	  	else								gsl_rng_set(r, gsl_rng_default_seed);
	  	
	  	for (i=0; i<ny; i++) 
		{	noise[i] = (double)gsl_ran_gaussian(r, (double)(1.0))*(double)amp_noise; 
		}
	}
	else
	{	for (i=0; i<ny; i++) noise[i]=(double)(amp_noise*ds_noise->yd[i]);
	}

	// we load the forcing:
	for (i=0; i<ny; i++) f[i] = (double)(amp_f*ds_f->yd[i]);		// deterministic forcing
	
	
	// initial conditions: (they will relax, so it doesn"t matter that much)
	x[0]  = (f[0]+noise[0])/k;
	dx[0] = 0; 
	
	// integration:
	if (bool_Fourier==1)
	{	integrate_langevin_equation_Fourier(ny, (double)dt, m, nu, k, bool_hanning, bool_filter_Fourier, f_acq);
	}
	else 
	{	integrate_langevin_equation_RK4(ny, (double)dt, m, nu, k);
	}	
	
	
	// save the solution in a new dataste:
	ds2 = create_and_attach_one_ds (op1, ny, ny, 0);
	for (i=0; i<ny; i++)
	{	ds2->xd[i] = ds_f->xd[i];
		ds2->yd[i] = (float)(x[i]);			
	}
	inherit_from_ds_to_ds(ds2, ds_f);
	ds2->treatement = my_sprintf(ds2->treatement,"x, solution of Langevin (%s)", (bool_Fourier==1) ? "Fourier" : "RK4");

		
	if ( (bool_Fourier==0) || (do_derivate_Fourier==0) ) // we derivate using finite differences (order 2)
	{	compute_derivative_periodic(ny, dt, f, df);		
	//	compute_derivative(ny, dt, x, dx); // this was already computed... we overwrite it assuming periodicity
	}
	
	// computes some estimates for the ranges of histograms of powers:	
	if (bool_do_Jarzynski==1) autodetect_Jarzynski_values(ny, f, df, x, dx, dt, k, amp_f, amp_noise); 
	
	
	// plot the derivative of the forcing, if asked for:
	if (do_plot_d_forcing==1)
	{	ds2 = create_and_attach_one_ds (op1, ny, ny, 0);
		for (i=0; i<ny; i++)
		{	ds2->xd[i] = ds_f->xd[i];
			ds2->yd[i] = (float)(df[i]);
		}
		inherit_from_ds_to_ds(ds2, ds_f);
		ds2->treatement = my_sprintf(ds2->treatement,"df/dt, derivative of the forcing");
	}
	
	// plot the derivative of the solution, if asked for:
	if (do_plot_d_x==1)
	{	ds2 = create_and_attach_one_ds (op1, ny, ny, 0);
		for (i=0; i<ny; i++)
		{	ds2->xd[i] = ds_f->xd[i];
			ds2->yd[i] = (float)(dx[i]);
		}
		inherit_from_ds_to_ds(ds2, ds_f);
		ds2->treatement = my_sprintf(ds2->treatement,"dx/dt, derivative of the solution");
	}
		
	refresh_plot(pr, UNCHANGED);
	
	// now perform several experiments (in a loop) to have some statistics:
	if ( (bool_generate_noise==1) && ( (bool_do_multiple_cycles==1) || (bool_do_Jarzynski==1) ) )
	{	
		i=win_scanf("{\\pt14\\color{yellow}Integrate Langevin eq. over several cycles}\n"
				"%8d cycles\n"
				"%b plot complete solution",
				&n_cycles, &bool_plot_multiple_cycles);
		if (i==CANCEL) return(D_O_K);

		if (bool_do_save_data==1)
		{	i=init_Langevin_files(ny);
			if (i==CANCEL) return(D_O_K);
		}
		
		if (bool_plot_multiple_cycles==1)
		{	if (ny*n_cycles>1e6) return(win_printf_OK("You asked to plot the complete solution\n"
													  "over %d cycles of %d points\n"
													  "this is too large for me and my memory...",
													  n_cycles, ny));
			if ((op_long = create_and_attach_one_plot(pr, ny*n_cycles, ny*n_cycles, 0)) == NULL)
				return win_printf_OK("cannot create plot !");
			ds2 = op_long->dat[0];
			ds2->treatement = my_sprintf(ds2->treatement,"Langevin solution over %d cycles of forcing", n_cycles);
		}
		
		if (bool_do_Jarzynski==1) init_Langevin_Jarzynski();			
		
		for (j=0; j<n_cycles; j++)
		{	// prepare a new noise:
			for (i=0; i<ny; i++) noise[i] = (double)(gsl_ran_gaussian(r, (double)(1.0)) * amp_noise);
			
			// print where we are:
			if (((100*j)%n_cycles)==0)
			{	sprintf(s, "cycling... %d%% done", (100*j)/n_cycles);
				spit(s);
			}
			
			// Integrate langevin equation:
			if (bool_Fourier==1) 
			{ 	integrate_langevin_equation_Fourier(ny, dt, m, nu, k, 
						bool_hanning, bool_filter_Fourier, f_acq);
			}
			else
			{	// initial conditions:
				x[0] = x[ny-1];     dx[0] = dx[ny-1]; // we take the last point of previous integration, and advance it
				integrate_langevin_equation_RK4(2, dt,	m, nu, k);
				x[0] = x[1];		dx[0] = dx[1]; // we copy it to be the initial condition of next run
				// integration:
				integrate_langevin_equation_RK4(ny, dt,	m, nu, k);
			} 		
				
			if (bool_do_save_data==1)
			{	sprintf(s, "saving, file(s) %d", save_Langevin_files(ny, j) );
			}
			
			if (bool_plot_multiple_cycles==1)
			{	for (i=0; i<ny; i++) 
				{	ds2->xd[j*ny+i] = (float)dt*(j*ny+i);
					ds2->yd[j*ny+i] = x[i];
				}
			}	
			
			if (bool_do_Jarzynski==1) accumulate_data_for_Jarzynski(ny, bool_Fourier, f, df, x, dx, dt);
						
		}// end of loop over j (cycles)

		if (bool_do_Jarzynski==1) save_Jarzynski_data_in_datasets_and_free(op1, n_cycles); // saving histograms
	

	}
	
	if (bool_do_save_data==1) 
		free_Langevin_files();
	gsl_rng_free(r);
	free_Langevin_equation_common();
	if (bool_Fourier==1)	
		free_Langevin_equation_Fourier();
		
	return( refresh_plot(pr, UNCHANGED) );
} // end of the do_integrate_langevin_equation function






/*********************************************************************/
/* following function integrates coupled Langevin equations          */
/*********************************************************************/
int do_integrate_langevin_equation_n_variables(void)
{
	pltreg 	*pr = NULL;
	O_p 	*op1=NULL, 		// input plot : contains the forcing over 1 cycle, and will contain the solution over 1 cycle
			*op_long=NULL; 	// output plot : will contain the slution over several cycles, if asked for.
	d_s 	*ds_f, *ds2=NULL;
	gsl_rng *r=NULL;  			/* random generator 			*/
	double	dt;
	float	float_m=(float)m, float_nu=(float)nu, float_k=(float)k, float_amp_f=(float)amp_f, float_amp_noise=(float)amp_noise;
	float	f_acq;
register int 	i,j;
	int		ny, n_cycles=1000;
	int		n_fft;
static int 	bool_hanning=0, // do_derivate_Fourier=0, 
			bool_seed_random_with_clock=1,		// random generator for the noise is set with clock, or with 0
			do_plot_d_forcing=0, do_plot_d_x=0;
	int	 //	bool_do_Jarzynski=0, 
			bool_do_save_data=0,
			bool_do_multiple_cycles=0, 
			bool_plot_multiple_cycles=0;
static int	bool_Fourier=0;			
	int		index, i_f;
	char	s[256];
	

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;
	
//	if (index & LANGEVIN_JARZYNSKI) bool_do_Jarzynski=1; // we'll do more than just integrating...
	if (index & LANGEVIN_MULTIPLE)  bool_do_multiple_cycles=1;
	if (index & LANGEVIN_SAVE)	{	bool_do_save_data=1;
									bool_do_multiple_cycles=1;
								}
	
	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine integrates coupled Langevin equations with forcing\n"
        				"with RK4\n"
					"{\\pt14 m.d^2x/dt^2 + \\nu .dx/dt + k.x = f(t) + \\eta(t)}\n\n"
					"the forcing f(t) is read from a dataset and is the same for all modes.\n"
					"the noise \\eta(t) is generated (white noise).\n"
					"a new dataset is created for the solution x");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds_f) != 3)	
									return(win_printf_OK("cannot find plot or dataset"));
	i_f     = op1->cur_dat;
	ny = ds_f->ny;	// number of points in time series 
	if (ny<4) 						return(win_printf_OK("not enough points !"));
	
	dt = (double)(ds_f->xd[ny-1]-ds_f->xd[0])/(ny-1);
	if ( (ds_f->xd[2]-ds_f->xd[1])!=(dt) ) 	return(win_printf_OK("dt is not constant ?!"));
	if (dt==0)						return(win_printf_OK("dt is zero !"));
	n_fft=ny;
	f_acq=(float)(1./dt);

	i=win_scanf("{\\pt14\\color{yellow}Coupled Langevin equations}\n"
			  "integration with RK4\n\n"
			  "{\\pt14 m.d^2x/dt^2 + \\nu .dx/dt + k.x = f(t) + \\eta(t)}\n\n"
			  "number of equations : %4d\n\n"
			  "the forcing f(t) is read from active dataset. It's the same for all modes.\n"
			  "the noise (gaussian, \\sigma=1) is generated (%b seed with clock)\n\n"				
	 		  "mass m %6f     damping \\nu  %6f    spring constant k %6f \n"
			  "deterministic forcing amplitude %6f   noise amplitude %6f \n\n"
			  "%b use Hanning window\n"
			  "%b plot df, derivative of the forcing\n"
			  "%b plot dx, derivative of the solution", 
				&n_variables, &bool_seed_random_with_clock, 
				&float_m, &float_nu, &float_k, &float_amp_f, &float_amp_noise, 
				&bool_hanning, &do_plot_d_forcing, &do_plot_d_x);
	if (i==CANCEL) return(D_O_K);
	if (n_variables>2) return(win_printf_OK("Only 2 equations for 2 modes up to now..."));
	
	m=(double)float_m;
	nu=(double)float_nu;
	k=(double)float_k;
	amp_f=(double)float_amp_f;;
	amp_noise=(double)float_amp_noise;
				
	
	init_Langevin_equation_common(ny, n_variables);	// 2 coupled Langevin equations
	{	i=CANCEL+1;
		if ( (m/nu) < dt/2.) i=win_printf("relaxation time is too short compared to dt!\n"
					"m / \\nu = %6g < dt/2 = %6g", m/nu, dt/2.);
		if (i==CANCEL) return(D_O_K);
		if (m!=0)
		{ 	if ( 2*M_PI*sqrt(m/k) < dt/4.) i=win_printf("caracteristic time is too short compared to dt!\n"
					"\\sqrt{m/k} = %6g < dt/2 = %6g", sqrt(m/k), dt/2.);
			if (i==CANCEL) return(D_O_K);
		}

	}

   	// we generate the noise:
	{	random_generator_type = gsl_rng_default;
		gsl_rng_env_setup();	// 3 lines to init the random number generator
	  	r = gsl_rng_alloc(random_generator_type);
	  	if (bool_seed_random_with_clock==1) gsl_rng_set(r, (unsigned long int)clock());
	  	else								gsl_rng_set(r, gsl_rng_default_seed);
	  	
	  	for (i=0; i<ny*n_variables; i++) 
		{	noise[i] = (double)gsl_ran_gaussian(r, (double)(1.0))*(double)amp_noise; 
		}
	}

	// we load the forcing:
	for (i=0; i<ny; i++) f[i] = (double)(amp_f*ds_f->yd[i]);		// deterministic forcing
		
	// initial conditions: (they will relax, so it doesn"t matter that much)
	for (j=0; j<n_variables; j++)
	{   x[j+0]  = (f[j+0]+noise[0*n_variables+j])/k;
	    dx[j+0] = 0.; 
    }
win_printf("ready to integrate\nis it OK ?");			
	// integration: we work inplicitly on variables x and dx (ny long, n_variables large)
	integrate_langevin_equation_RK4_n_variables(ny, (double)dt, m, nu, k);
		
win_printf("integration done.\nis it OK ?");		
	
	// save the solution in a new dataste:
    for (j=0; j<n_variables; j++)
	{   ds2 = create_and_attach_one_ds (op1, ny, ny, 0);
	    for (i=0; i<ny; i++)
	    {	ds2->xd[i] = ds_f->xd[i];
            ds2->yd[i] = (float)(x[i*n_variables+j]);			
	    }
	    inherit_from_ds_to_ds(ds2, ds_f);
	    ds2->treatement = my_sprintf(ds2->treatement,"x, solution %d/%d of Langevin (%s)", 
                          j+1, n_variables, (bool_Fourier==1) ? "Fourier" : "RK4");
    }

win_printf("datasets created and filled\nis it OK ?");		

	// plot the derivative of the forcing, if asked for:
	if (do_plot_d_forcing==1)
	{	ds2 = create_and_attach_one_ds (op1, ny, ny, 0);
		for (i=0; i<ny; i++)
		{	ds2->xd[i] = ds_f->xd[i];
			ds2->yd[i] = (float)(df[i]);
		}
		inherit_from_ds_to_ds(ds2, ds_f);
		ds2->treatement = my_sprintf(ds2->treatement,"df/dt, derivative of the forcing");
	}
	
	// plot the derivative of the solution, if asked for:
	if (do_plot_d_x==1)
	{	ds2 = create_and_attach_one_ds (op1, ny, ny, 0);
		for (i=0; i<ny; i++)
		{	ds2->xd[i] = ds_f->xd[i];
			ds2->yd[i] = (float)(dx[i]);
		}
		inherit_from_ds_to_ds(ds2, ds_f);
		ds2->treatement = my_sprintf(ds2->treatement,"dx/dt, derivative of the solution");
	}
		
	refresh_plot(pr, UNCHANGED);
	
	// now perform several experiments (in a loop) to have some statistics:
	if (bool_do_multiple_cycles==1)
	{	
		i=win_scanf("{\\pt14\\color{yellow}Integrate Langevin eq. over several cycles}\n"
				"%8d cycles\n"
				"%b plot complete solution",
				&n_cycles, &bool_plot_multiple_cycles);
		if (i==CANCEL) return(D_O_K);

		if (bool_do_save_data==1)
		{	i=init_Langevin_files(ny);
			if (i==CANCEL) return(D_O_K);
		}
		
		if (bool_plot_multiple_cycles==1)
		{	if (ny*n_cycles>1e6) return(win_printf_OK("You asked to plot the complete solution\n"
													  "over %d cycles of %d points\n"
													  "this is too large for me and my memory...",
													  n_cycles, ny));
			if ((op_long = create_and_attach_one_plot(pr, ny*n_cycles, ny*n_cycles, 0)) == NULL)
				return win_printf_OK("cannot create plot !");
			ds2 = op_long->dat[0];
			ds2->treatement = my_sprintf(ds2->treatement,"Langevin solution over %d cycles of forcing", n_cycles);
		}
		
//		if (bool_do_Jarzynski==1) init_Langevin_Jarzynski();			
		
		for (j=0; j<n_cycles; j++)
		{	// prepare a new noise:
			for (i=0; i<ny*n_variables; i++) noise[i] = (double)(gsl_ran_gaussian(r, (double)(1.0)) * amp_noise);
			
			// print where we are:
			if (((100*j)%n_cycles)==0)
			{	sprintf(s, "cycling... %d%% done", (100*j)/n_cycles);
				spit(s);
			}
			
			// Integrate langevin equation:
			{	// initial conditions:
                for (i=0; i<n_variables; i++)
                {   x[i+0]  = x[(ny-1)*n_variables+i];     
                    dx[i+0] = dx[(ny-1)*n_variables+i]; // we take the last point of previous integration, and advance it
                }
				integrate_langevin_equation_RK4_n_variables(2, dt,	m, nu, k);
				for (i=0; i<n_variables; i++)
                {	x[i+0]  = x[1*n_variables+i];		
                    dx[i+0] = dx[1*n_variables+i]; // we copy it to be the initial condition of next run
                }
				// integration:
				integrate_langevin_equation_RK4_n_variables(ny, dt,	m, nu, k);
			} 		
				
			if (bool_do_save_data==1)
			{	sprintf(s, "saving, file(s) %d", save_Langevin_files(ny, j) );
			}
			
			if (bool_plot_multiple_cycles==1)
			{	for (i=0; i<ny; i++) 
				{	ds2->xd[j*ny+i] = (float)dt*(j*ny+i);
					ds2->yd[j*ny+i] = x[i];
				}
			}	
			
//			if (bool_do_Jarzynski==1) accumulate_data_for_Jarzynski(ny, bool_Fourier, f, df, x, dx, dt);
						
		}// end of loop over j (cycles)

//		if (bool_do_Jarzynski==1) save_Jarzynski_data_in_datasets_and_free(op1, n_cycles); // saving histograms
	

	}
	
	if (bool_do_save_data==1) 
		free_Langevin_files();
	gsl_rng_free(r);
	free_Langevin_equation_common();
		
	return( refresh_plot(pr, UNCHANGED) );
} // end of the do_integrate_langevin_equation_n_variables function





/* following is to intergate a Langevin eq, using as the noise the solution of another Langevin equation */
int do_integrate_langevin_equation_with_filtred_noise(void)
{
	pltreg 	*pr = NULL;
	O_p 	*op1=NULL, 		// input plot : contains the forcing over 1 cycle, and will contain the solution over 1 cycle
			*op_long=NULL; 	// output plot : will contain the slution over several cycles, if asked for.
			
	d_s 	*ds_f, *ds2=NULL;
	gsl_rng *r=NULL;  			/* random generator 			*/
	double	dt;
	double  m_noise, nu_noise, k_noise, amp_noise_noise;
	float	float_m=(float)m, float_nu=(float)nu, float_k=(float)k, float_amp_f=(float)amp_f, float_amp_noise=(float)amp_noise;
static float float_m_noise=0., float_nu_noise=0.1, float_k_noise=100., float_amp_noise_noise=0.01;
	float	f_acq;
register int 	i,j;
	int		ny, n_cycles=1000;
	int		n_fft;
static int 	bool_seed_random_with_clock=1,		// random generator for the noise is set with clock, or with 0
			do_plot_noise=1;
	int		bool_do_Jarzynski=0, 
			bool_do_save_data=0,
			bool_do_multiple_cycles=0, 
			bool_plot_multiple_cycles=0;
static int	bool_Fourier=0;			
	int		index, i_f;
	char	s[256];
	double  last_x, last_d_x, last_noise, last_d_noise;
	

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;
	
	if (index & LANGEVIN_JARZYNSKI) bool_do_Jarzynski=1; // we'll do more than just integrating...
	if (index & LANGEVIN_MULTIPLE)  bool_do_multiple_cycles=1;
	if (index & LANGEVIN_SAVE)	{	bool_do_save_data=1;
									bool_do_multiple_cycles=1;
								}
	
	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine integrates a Langevin eq. with forcing\n"
        				"in Fourier space or with RK4\n"
					"{\\pt14 m.d^2x/dt^2 + \\nu .dx/dt + k.x = f(t) + \\eta(t)}\n\n"
					"the forcing f(t) is read from a dataset.\n"
					"the noise \\eta(t) is the solution of another Langevin equation.\n"
					"a new dataset is created for the solution x\n"
					"several treatments can be applied automatically.");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds_f) != 3)	
									return(win_printf_OK("cannot plot region"));
	i_f     = op1->cur_dat;
	ny = ds_f->ny;	// number of points in time series 
	if (ny<4) 						return(win_printf_OK("not enough points !"));
	
	dt = (double)(ds_f->xd[ny-1]-ds_f->xd[0])/(ny-1);
	if ( (ds_f->xd[2]-ds_f->xd[1])!=(dt) ) 	return(win_printf_OK("dt is not constant ?!"));
	if (dt==0)						return(win_printf_OK("dt is zero !"));
	n_fft=ny;
	f_acq=(float)(1./dt);

	i=win_scanf("{\\pt14\\color{yellow}Langevin equation (RK4)}\n"
			  "{\\pt14 m.d^2x/dt^2 + \\nu .dx/dt + k.x = f(t) + \\eta(t)}\n\n"
			  "the forcing f(t) is read from active dataset.\n\n"
			  "noise \\eta(t) is solution of another Langevin eq. (with gaussian noise, \\sigma=1) (%b seed with clock)\n\n"				
	 		  "for the main equation:\n"
	 		  "mass m %6f     damping \\nu  %6f    spring constant k %6f \n"
	 		  "deterministic forcing amplitude %6f   noise amplitude %6f \n\n"
	 		  "for the noise equation:\n"
	 		  "mass m %6f     damping \\nu  %6f    spring constant k %6f \n"
			  "noise amplitude in the noise equation %6f \n\n"
			  "%b plot the noise \\eta, solution of the noise equation", 
			  	&bool_seed_random_with_clock, 
				&float_m, &float_nu, &float_k, &float_amp_f, &float_amp_noise, 
				&float_m_noise, &float_nu_noise, &float_k_noise, &float_amp_noise_noise, 
				&do_plot_noise);
	if (i==CANCEL) return(D_O_K);
	m=(double)float_m;
	nu=(double)float_nu;
	k=(double)float_k;
	amp_f=(double)float_amp_f;;
	amp_noise=(double)float_amp_noise;
	m_noise=(double)float_m_noise;
	nu_noise=(double)float_nu_noise;
	k_noise=(double)float_k_noise;
	amp_noise_noise=(double)float_amp_noise_noise;
				 
	
	init_Langevin_equation_common(ny, 1);	// single variable Langevin equation
	i=CANCEL+1;
	if ( (m/nu) < dt/2.) i=win_printf("relaxation time is too short compared to dt!\n"
				"m / \\nu = %6g < dt/2 = %6g", m/nu, dt/2.);
	if (i==CANCEL) return(D_O_K);
	if (m!=0)
	{ 	if ( 2*M_PI*sqrt(m/k) < dt/4.) i=win_printf("caracteristic time is too short compared to dt!\n"
				"\\sqrt{m/k} = %6g < dt/2 = %6g", sqrt(m/k), dt/2.);
		if (i==CANCEL) return(D_O_K);
	}
	if ( (m_noise/nu_noise) < dt/2.) i=win_printf("equation for the noise : relaxation time is too short compared to dt!\n"
				"(noise) m / \\nu = %6g < dt/2 = %6g", m_noise/nu_noise, dt/2.);
	if (i==CANCEL) return(D_O_K);
	if (m_noise!=0)
	{ 	if ( 2*M_PI*sqrt(m_noise/k_noise) < dt/4.) i=win_printf("equation for the noise : caracteristic time is too short compared to dt!\n"
				"(noise) \\sqrt{m/k} = %6g < dt/2 = %6g", sqrt(m_noise/k_noise), dt/2.);
		if (i==CANCEL) return(D_O_K);
	}
	
	nbpas_RK4 = 1;
	
	if (bool_do_Jarzynski==1) ask_for_Jarzynski_parameters(ny, f_acq); // ask for indices of startin (s) and ending (e) points
		
   	// we create the noise for the noise (white):
	random_generator_type = gsl_rng_default;
	gsl_rng_env_setup();	// 3 lines to init the random number generator
	r = gsl_rng_alloc(random_generator_type);
	if (bool_seed_random_with_clock==1) gsl_rng_set(r, (unsigned long int)clock());
	else								gsl_rng_set(r, gsl_rng_default_seed);
	  	
	for (i=0; i<ny; i++) noise[i] = (double)gsl_ran_gaussian(r, (double)(1.0))*(double)amp_noise_noise; 

	// we set the forcing to 0:
	for (i=0; i<ny; i++) f[i] = (double)0.;		// no deterministic forcing for the Langevin for the noise
	
	// initial conditions for the noise: (same as done below for the real solution)
	x[0]  = (f[0]+noise[0])/k_noise;
	dx[0] = 0;

	// integration of the noise:
	integrate_langevin_equation_RK4(ny, (double)dt, m_noise, nu_noise, k_noise);
	last_noise   = x[ny-1];
	last_d_noise = dx[ny-1];
			
	// then copying this solution as the noise for the next Langevin equation:
	for (i=0; i<ny; i++) noise[i] = x[i]*amp_noise; 
	
	// we load the forcing:
	for (i=0; i<ny; i++) f[i] = (double)(amp_f*ds_f->yd[i]);		// deterministic forcing
	
	// initial conditions for the real solution: (they will relax, so it doesn"t matter that much)
	x[0]  = (f[0]+noise[0])/k;
	dx[0] = 0; 
	
	// integration of the real Langevin equation:
	integrate_langevin_equation_RK4(ny, (double)dt, m, nu, k);
	last_x   =  x[ny-1];
	last_d_x = dx[ny-1];
		
	// save the solution in a new dataste:
	ds2 = create_and_attach_one_ds (op1, ny, ny, 0);
	for (i=0; i<ny; i++)
	{	ds2->xd[i] = ds_f->xd[i];
		ds2->yd[i] = (float)(x[i]);			
	}
	inherit_from_ds_to_ds(ds2, ds_f);
	ds2->treatement = my_sprintf(ds2->treatement,"x, solution of Langevin with a filtred noise (RK4)");

	compute_derivative_periodic(ny, dt, f, df);		
	
	if (bool_do_Jarzynski==1) autodetect_Jarzynski_values(ny, f, df, x, dx, dt, k, amp_f, amp_noise); 

	// plot the noise, if asked for:
	if (do_plot_noise==1)
	{	ds2 = create_and_attach_one_ds (op1, ny, ny, 0);
		for (i=0; i<ny; i++)
		{	ds2->xd[i] = ds_f->xd[i];
			ds2->yd[i] = (float)(noise[i]);
		}
		inherit_from_ds_to_ds(ds2, ds_f);
		ds2->treatement = my_sprintf(ds2->treatement,"filtred noise, from a Langevin");
	}
		
	refresh_plot(pr, UNCHANGED);
	
	// now perform several experiments (in a loop) to have some statistics:
	if ( ( (bool_do_multiple_cycles==1) || (bool_do_Jarzynski==1) ) )
	{	
		i=win_scanf("{\\pt14\\color{yellow}Integrate Langevin eq. over several cycles}\n"
				"%8d cycles\n"
				"%b plot complete solution",
				&n_cycles, &bool_plot_multiple_cycles);
		if (i==CANCEL) return(D_O_K);

		if (bool_do_save_data==1)
		{	i=init_Langevin_files(ny);
			if (i==CANCEL) return(D_O_K);
		}
		
		if (bool_plot_multiple_cycles==1)
		{	if (ny*n_cycles>1e6) return(win_printf_OK("You asked to plot the complete solution\n"
													  "over %d cycles of %d points\n"
													  "this is too large for me and my memory...",
													  n_cycles, ny));
			if ((op_long = create_and_attach_one_plot(pr, ny*n_cycles, ny*n_cycles, 0)) == NULL)
				return win_printf_OK("cannot create plot !");
			ds2 = op_long->dat[0];
			ds2->treatement = my_sprintf(ds2->treatement,"Langevin solution over %d cycles of forcing", n_cycles);
		}

		if (bool_do_Jarzynski==1) init_Langevin_Jarzynski();			
		
		for (j=0; j<n_cycles; j++)
		{	// print where we are:
			if (((100*j)%n_cycles)==0)
			{	sprintf(s, "cycling... %d%% done", (100*j)/n_cycles);
				spit(s);
			}
			
			// integrate the Langevin eq. for the noise:
			for (i=0; i<ny; i++) noise[i] = (double)(gsl_ran_gaussian(r, (double)(1.0)) * amp_noise_noise);
			for (i=0; i<ny; i++) f[i]     = (double)0.;
			// initial conditions:	
			x[0] = last_noise;     dx[0] = last_d_noise; // we take the last point of previous integration, and advance it
			integrate_langevin_equation_RK4(2, dt,	m_noise, nu_noise, k_noise);
			x[0] = x[1];		   dx[0] = dx[1]; // we copy it to be the initial condition of next run
			// integration:
			integrate_langevin_equation_RK4(ny, dt,	m_noise, nu_noise, k_noise);
			last_noise   = x[ny-1];
			last_d_noise = dx[ny-1];
			
			// Integrate langevin equation:
			for (i=0; i<ny; i++) noise[i] = (double)x[i]*amp_noise;
			for (i=0; i<ny; i++) f[i]     = (double)(amp_f*ds_f->yd[i]);
			// initial conditions:
			x[0] = last_x;     dx[0] = last_d_x; // we take the last point of previous integration, and advance it
			integrate_langevin_equation_RK4(2, dt,	m, nu, k);
			x[0] = x[1];		dx[0] = dx[1]; // we copy it to be the initial condition of next run
			// integration:
			integrate_langevin_equation_RK4(ny, dt,	m, nu, k);		
			last_x   =  x[ny-1];
			last_d_x = dx[ny-1]; 
				
			if (bool_do_save_data==1)
			{	sprintf(s, "saving, file(s) %d", save_Langevin_files(ny, j) );
			}
			
			if (bool_plot_multiple_cycles==1)
			{	for (i=0; i<ny; i++) 
				{	ds2->xd[j*ny+i] = (float)dt*(j*ny+i);
					ds2->yd[j*ny+i] = x[i];
				}
			}	
			
			if (bool_do_Jarzynski==1) accumulate_data_for_Jarzynski(ny, bool_Fourier, f, df, x, dx, dt);
						
		}// end of loop over j (cycles)

		if (bool_do_Jarzynski==1) save_Jarzynski_data_in_datasets_and_free(op1, n_cycles); // saving histograms
	
	}
	
	
	if (bool_do_save_data==1) free_Langevin_files();
	gsl_rng_free(r);
	free_Langevin_equation_common();
	
		
	return( refresh_plot(pr, UNCHANGED) );
} // end of the do_integrate_langevin_equation_with_filtered_noise function







void spit(char *message)
{	size_t	l_max=55;
	char 	white_string[64]="                                              ";
	
	if (strlen(message)<l_max) 	strncat (message, white_string, l_max-strlen(message));
	textout_ex(screen, font, message, 40, 40, makecol(100,155,155), makecol(2,1,1));
	return;
}




MENU Langevin_simple_menu[16] = 
{	{"1 cycle",				do_integrate_langevin_equation, 	NULL, 0, 					NULL},
	{"N cycles",			do_integrate_langevin_equation, 	NULL, LANGEVIN_MULTIPLE,  	NULL},
	{"save in files",		do_integrate_langevin_equation, 	NULL, LANGEVIN_SAVE,	  	NULL},
	{"compute Jarzynski",	do_integrate_langevin_equation, 	NULL, LANGEVIN_JARZYNSKI| LANGEVIN_MULTIPLE,  NULL}
};

MENU Langevin_double_menu[16] = 
{	{"1 cycle",				do_integrate_langevin_equation_n_variables, 	NULL, 0, 					NULL},
	{"N cycles",			do_integrate_langevin_equation_n_variables, 	NULL, LANGEVIN_MULTIPLE,  	NULL},
	{"save in files",		do_integrate_langevin_equation_n_variables, 	NULL, LANGEVIN_SAVE,	  	NULL}
};

MENU Langevin_filtred_noise_menu[16] =
{	{"1 cycle",				do_integrate_langevin_equation_with_filtred_noise, 	NULL, 0,	 				NULL},
	{"N cycles",			do_integrate_langevin_equation_with_filtred_noise, 	NULL, LANGEVIN_MULTIPLE, 	NULL},
	{"save in files",		do_integrate_langevin_equation_with_filtred_noise, 	NULL, LANGEVIN_SAVE,	 	NULL},
	{"compute Jarzynski",	do_integrate_langevin_equation_with_filtred_noise, 	NULL, LANGEVIN_JARZYNSKI| LANGEVIN_MULTIPLE,  NULL}
};


MENU *Langevin_plot_menu(void)
{	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"ext. forcing + white noise",	NULL, Langevin_simple_menu, 0, NULL);
	add_item_to_menu(mn,"\0",	 			 0,						NULL, 0, NULL);	
	add_item_to_menu(mn,"ext. forcing + white noise, 2 variables",	NULL, Langevin_double_menu, 0, NULL);
	add_item_to_menu(mn,"\0",	 			 0,						NULL, 0, NULL);	
	add_item_to_menu(mn,"ext. forcing + filtred noise",	NULL, Langevin_filtred_noise_menu, 0, NULL);

	return mn;
}

int	Langevin_main(int argc, char **argv)
{
	add_plot_treat_menu_item ("Langevin", NULL, Langevin_plot_menu(), 0, NULL);
	return D_O_K;
}


int	Langevin_unload(int argc, char **argv)
{ 
	remove_item_to_menu(plot_treat_menu,"Langevin",	NULL, NULL);
	return D_O_K;
}
#endif

