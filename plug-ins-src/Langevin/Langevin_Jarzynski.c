#include <gsl/gsl_histogram.h>
#include "xvin.h"
#include "../hist/hist.h"


// below are global variables visible by all the function in this file, but not further:
gsl_histogram  *hist_W_classic_AB=NULL,		*hist_W_classic_BA=NULL, 
				*hist_W_Jarzynski_AB=NULL,	*hist_W_Jarzynski_BA=NULL, 
				*hist_P_classic=NULL,		*hist_P_Jarzynski=NULL;
double 	W_classic_AB=0, W_Jarzynski_AB=0, W_classic_BA=0, W_Jarzynski_BA=0, P_classic=0, P_Jarzynski=0;
float 	P_c_min=-2., P_J_min=-2., P_c_max=2., P_J_max=2.;
float	W_c_min, W_c_max, W_J_min, W_J_max, mean, sigma_c, sigma_J;
static float   x_up, x_down, f_up, f_down, D_F_x, D_F_f;
int		i_A_s, i_A_e, i_B_s, i_B_e;
static int n_bins=100;
				
				


// ask for indices of startin (s) and ending (e) points :
int ask_for_Jarzynski_parameters(int ny, float f_acq)
{	char *info=NULL;

	i_A_s=0; 		i_A_e = ny/2;
	i_B_s=ny/2; 	i_B_e = ny;
	info = my_sprintf(info,"From A to B will be the interval\n [ %%4d , %%4d [\n"
				  "From B to A will be the interval\n [ %%4d , %%4d [\n\n"
				  "(f_{ec}=%g Hz, %d points in excitation, total time is %g s)",
				  f_acq, ny, (float)ny/f_acq);
	return(win_scanf(info, &i_A_s, &i_A_e, &i_B_s, &i_B_e));
}	




// computes some estimates for the ranges of histograms of powers:				
int autodetect_Jarzynski_values(int ny, double *f, double *df, double *x, double *dx, double dt, double k, double amp_f, double amp_noise)
{	int i;

	P_c_min=0; P_c_max=0;
	P_J_min=0; P_J_max=0;
	W_classic_AB = 0;		W_classic_BA = 0;
	W_Jarzynski_AB = 0;		W_Jarzynski_BA = 0;

	for (i=0; i<ny; i++)
	{	
		P_classic   = -(dx[i] * f[i]);
		P_Jarzynski = -(x[i] * df[i]);
		if (P_classic < P_c_min) P_c_min=P_classic;
		if (P_classic > P_c_max) P_c_max=P_classic;
		if (P_Jarzynski < P_J_min) P_J_min=P_Jarzynski;
		if (P_Jarzynski > P_J_max) P_J_max=P_Jarzynski;
		
		if ( (i_A_s<=i) && (i<i_A_e) ) 
		{	W_classic_AB   += P_classic*dt;
			W_Jarzynski_AB += P_Jarzynski*dt; 
		}
		if ( (i_B_s<=i) && (i<i_B_e) ) 
		{	W_classic_BA   += P_classic*dt;
			W_Jarzynski_BA += P_Jarzynski*dt; 
		}
	}
		
	// we estimate the variation of free-energy:
	x_up   = (float)(1./2.)*( x[i_A_e] + x[i_B_s]);	// we average over 2 points
	x_down = (float)(1./2.)*( x[i_A_s] + x[i_B_e]); // we average over 2 points
	f_up   = (float)f[i_A_e];
	f_down = (float)f[i_A_s];
	D_F_x = (float)((0.5*k*x_up*x_up) - (0.5*k*x_down*x_down));
	D_F_f = (float)((0.5*f_up*f_up/k) - (0.5*k*f_down*f_down/k));

	win_printf("Work given {\\color{yellow}to} the system\n"
			 "W_^{classic}_{AB}   =   %6g   W_^{classic}_{BA}   =   %6g\n"
			 "W_^{Jarzynski}_{AB}   = %6g   W_^{Jarzynski}_{BA}   = %6g\n"
			 "\\Delta F_{AB} = \\frac{1}{2}kx^2 = %6g\n"
			 "\\Delta F_{AB} = \\frac{f^2}{2k} = %6g", 
			(float)W_classic_AB, (float)W_classic_BA, (float)W_Jarzynski_AB, (float)W_Jarzynski_BA, D_F_x, D_F_f);
	if (amp_f!=0)
	{ 		W_c_min = fabs(D_F_f)*(1-0.1-10.*amp_noise/amp_f);
			W_c_max = fabs(D_F_f)*(1+0.1+10.*amp_noise/amp_f);
	}
	else { 	W_c_min = -5.*D_F_f; 
			W_c_max=+5.*D_F_f; 
		 }

	mean  = (P_c_min+P_c_max)/2.; 	sigma_c = (P_c_max-P_c_min)/2.;
		P_c_min = mean-2.*sigma_c;
		P_c_max = mean+2.*sigma_c;
	mean  = (P_J_min+P_J_max)/2.; 	sigma_J = (P_J_max-P_J_min)/2.;
		P_J_min = mean-2.*sigma_J;
		P_J_max = mean+2.*sigma_J;
	W_J_min = fabs(D_F_f) - (W_c_max-W_c_min) * sigma_J/sigma_c;
	W_J_max = fabs(D_F_f) + (W_c_max-W_c_min) * sigma_J/sigma_c;

	return(0);
} // end of function 'autodetect_Jarzynski_values'

					
				
				
				
				
				
				
				
int init_Langevin_Jarzynski(void)
{	int i;

	i=win_scanf("Compute statistics over cycles ?\n\n"
				"histograms with %6d bins\n\n"
				"Powers (classic)   between %6f and %6f\n"
				"Powers (Jarzynski) between %6f and %6f\n"
				"Works (classic)    between %6f and %6f\n"
				"Works (classic)    between %6f and %6f", 
				&n_bins, &P_c_min, &P_c_max, &P_J_min, &P_J_max, &W_c_min, &W_c_max, &W_J_min, &W_J_max);
	if (i==CANCEL) return(D_O_K);
	if (n_bins<=1) return(win_printf_OK("number of bins (%d) is not correct",n_bins));
	if (P_c_min>=P_c_max) return(win_printf_OK("P_c : min (%g) > max (%g)", P_c_min, P_c_max));
	if (P_J_min>=P_J_max) return(win_printf_OK("P_J : min (%g) > max (%g)", P_J_min, P_J_max));
	if (W_c_min>=W_c_max) return(win_printf_OK("W_c : min (%g) > max (%g)", W_c_min, W_c_max));
	if (W_J_min>=W_J_max) return(win_printf_OK("W_J : min (%g) > max (%g)", W_J_min, W_J_max));
			
	(gsl_histogram *)hist_W_classic_AB = gsl_histogram_alloc(n_bins);
	gsl_histogram_set_ranges_uniform (hist_W_classic_AB, (double)W_c_min, (double)W_c_max);

	(gsl_histogram *)hist_W_classic_BA = gsl_histogram_alloc(n_bins);
	gsl_histogram_set_ranges_uniform (hist_W_classic_BA, (double)W_c_min, (double)W_c_max);

	(gsl_histogram *)hist_W_Jarzynski_AB = gsl_histogram_alloc(n_bins);
	gsl_histogram_set_ranges_uniform (hist_W_Jarzynski_AB, (double)W_J_min, (double)W_J_max);
				
	(gsl_histogram *)hist_W_Jarzynski_BA = gsl_histogram_alloc(n_bins);
	gsl_histogram_set_ranges_uniform (hist_W_Jarzynski_BA, (double)W_J_min, (double)W_J_max);
			
	(gsl_histogram *)hist_P_classic = gsl_histogram_alloc(n_bins);
	gsl_histogram_set_ranges_uniform (hist_P_classic, (double)P_c_min, (double)P_c_max);

	(gsl_histogram *)hist_P_Jarzynski = gsl_histogram_alloc(n_bins);
	gsl_histogram_set_ranges_uniform (hist_P_Jarzynski, (double)P_J_min, (double)P_J_max);
			
	return(0);
}






int accumulate_data_for_Jarzynski(int ny, int bool_Fourier, double *f, double *df, double *x, double *dx, double dt)
{	int i;

	// we compute the powers and works:
	W_classic_AB=0;			W_classic_BA=0;
	W_Jarzynski_AB=0;		W_Jarzynski_BA=0;
	for (i=0; i<ny-1+bool_Fourier; i++) // if Fourier, then periodicty, so one more point for derivatives
	{	P_classic   = -f[i]*dx[i];
		P_Jarzynski = -df[i]*x[i];

		if ( (i_A_s<=i) && (i<i_A_e) ) 
		{	W_classic_AB   += P_classic*dt;
			W_Jarzynski_AB += P_Jarzynski*dt; 
		}
		if ( (i_B_s<=i) && (i<i_B_e) ) 
		{	W_classic_BA   += P_classic*dt;
			W_Jarzynski_BA += P_Jarzynski*dt; 
		}
											
		// we build the histograms of powers:
		gsl_histogram_increment(hist_P_classic,   (double)P_classic);
		gsl_histogram_increment(hist_P_Jarzynski, (double)P_Jarzynski);
	}
	// we build the histograms of works:
	gsl_histogram_increment(hist_W_classic_AB,   (double)W_classic_AB);
	gsl_histogram_increment(hist_W_classic_BA,   -(double)W_classic_BA);
	gsl_histogram_increment(hist_W_Jarzynski_AB, (double)W_Jarzynski_AB);
	gsl_histogram_increment(hist_W_Jarzynski_BA, -(double)W_Jarzynski_BA);
	
	return(0);
}		





int save_Jarzynski_data_in_datasets_and_free(O_p *op1, int n_cycles)
{	pltreg 	*pr = NULL;
	O_p 	*op_P=NULL, *op_W=NULL;
	d_s 	*ds2=NULL;

	if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)	return(win_printf_OK("cannot find plot region"));
									
	// create a new plot for the histograms of W and put a line for \\Delta F as the first dataset:		
	if ((op_W = create_and_attach_one_plot(pr, 2, 2, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	ds2 = op_W->dat[0];
	ds2->xd[0]=D_F_f; ds2->xd[1]=D_F_f;
	ds2->yd[0]=1;     ds2->yd[1]=10*n_cycles/n_bins;
	set_ds_plot_label(ds2, D_F_f, n_cycles/n_bins, USR_COORD, "\\fbox{\\Delta F_{AB} = %2.3g }", D_F_f);

	// then add a dataset for the histogram of W_classic_AB:
	if ((ds2 = create_and_attach_one_ds(op_W, n_bins, n_bins, 0)) == NULL)	
		return win_printf_OK("cannot create dataset !");
	histogram_to_ds(hist_W_classic_AB, ds2, n_bins);
	gsl_histogram_free(hist_W_classic_AB);
	ds2->treatement = my_sprintf(ds2->treatement,"histogram of W^{classic}_{AB}");
			
	// then add a dataset for the histogram of W_classic_BA:
	if ((ds2 = create_and_attach_one_ds(op_W, n_bins, n_bins, 0)) == NULL)	
		return win_printf_OK("cannot create dataset !");
	histogram_to_ds(hist_W_classic_BA, ds2, n_bins);
	gsl_histogram_free(hist_W_classic_BA);
	ds2->treatement = my_sprintf(ds2->treatement,"histogram of -W^{classic}_{BA}");
			
	// then add a dataset for the histogram of W_Jarzynski_AB:
	if ((ds2 = create_and_attach_one_ds(op_W, n_bins, n_bins, 0)) == NULL)	
		return win_printf_OK("cannot create dataset !");
	histogram_to_ds(hist_W_Jarzynski_AB, ds2, n_bins);
	gsl_histogram_free(hist_W_Jarzynski_AB);
	ds2->treatement = my_sprintf(ds2->treatement,"histogram of W^{Jarzynski}_{AB}");

	// then add a dataset for the histogram of W_Jarzynski_BA:
	if ((ds2 = create_and_attach_one_ds(op_W, n_bins, n_bins, 0)) == NULL)	
		return win_printf_OK("cannot create dataset !");
	histogram_to_ds(hist_W_Jarzynski_BA, ds2, n_bins);
	gsl_histogram_free(hist_W_Jarzynski_BA);
	ds2->treatement = my_sprintf(ds2->treatement,"histogram of -W^{Jarzynski}_{BA}");
			
	// create a new plot for the histograms of P and put the histogram of P_classic as the first dataset:		
	if ((op_P = create_and_attach_one_plot(pr, n_bins, n_bins, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	ds2 = op_P->dat[0];
	histogram_to_ds(hist_P_classic, ds2, n_bins);
	gsl_histogram_free(hist_P_classic);
	ds2->treatement = my_sprintf(ds2->treatement,"histogram of P_{classic}");
			
	// then add a dataset for the histogram of power P_Jarzynski:
	if ((ds2 = create_and_attach_one_ds(op_P, n_bins, n_bins, 0)) == NULL)	
		return win_printf_OK("cannot create dataset !");
	histogram_to_ds(hist_P_Jarzynski, ds2, n_bins);
	gsl_histogram_free(hist_P_Jarzynski);
	ds2->treatement = my_sprintf(ds2->treatement,"histogram of P_{Jarzynski}");
			
	set_plot_title(op_W,"histograms of W^c_{AB}, -W^c_{BA}, W^J_{AB}, -W^J_{BA}");
	set_plot_x_title(op_W, "W");
	set_plot_y_title(op_W, "pdf");
	op_W->filename = Transfer_filename(op1->filename);
    	op_W->dir = Mystrdup(op1->dir);
	op_W->iopt |= YLOG;
			
	set_plot_title(op_P,"histograms of P_c and P_J");
	set_plot_x_title(op_P, "P");
	set_plot_y_title(op_P, "pdf");
	op_P->filename = Transfer_filename(op1->filename);
    	op_P->dir = Mystrdup(op1->dir);
	op_P->iopt |= YLOG;
	
	return(0);
}


