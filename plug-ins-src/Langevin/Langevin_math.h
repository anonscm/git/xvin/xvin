
int compute_derivative_periodic(int ny, double dt, double *y, double *dy_dt);

int init_Langevin_equation_common (int ny, int n_variables);
int init_Langevin_equation_Fourier(int ny, int bool_filter_Fourier);
int free_Langevin_equation_common (void);
int free_Langevin_equation_Fourier(void);
int integrate_langevin_equation_RK4            (int ny, double dt, double m, double nu, double k);
int integrate_langevin_equation_RK4_n_variables(int ny, double dt, double m, double nu, double k);
int integrate_langevin_equation_Fourier        (int ny, double dt, double m, double nu, double k, 
							int bool_hanning, int bool_filter_Fourier, double f_acq);

// internal functions:
double time_derivative_o1(double A_tmp, double nu, double k, double f, double dt);
int    time_derivative_o2(double *A_tmp, double m, double nu, double k, double f, double dt, double *dA);
int    time_derivative_o1_n_variables(double *A_tmp, double nu, double k, double *f, double dt, double *dA);
int    time_derivative_o2_n_variables(double *A_tmp, double m, double nu, double k, double *f, double dt, double *dA);
double integrate_RK4_Langevin_o1(double A_old, double nu, double k, double f_old, double f_new, double dt);
int    integrate_RK4_Langevin_o2(double *A_old, double m, double nu, double k, double f_old, double f_new, double dt, double *A_new);
int    integrate_RK4_Langevin_o1_n_variables(double *A_old, double nu, double k, double *f_old, double *f_new, double dt, double *A);
int    integrate_RK4_Langevin_o2_n_variables(double *A_old, double m, double nu, double k, double *f_old, double *f_new, double dt, double *A_new);

