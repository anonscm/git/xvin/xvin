#include "fft_filter_lib.h"

#include <fftw3.h>

#include "Langevin_math.h"

#ifndef M_PI
#define M_PI 3.141592653589793238462643383
#endif

/*************************************************************************************/
// nombre de pas d'integrations dans 1 boucle de Runge-Kutta:
int   nbpas_RK4=1;
/*************************************************************************************/
// number of variables (ie, number of Langevin equations:
extern int   n_variables;
// arrays containing for one cycle/window:
extern double	*f, 	// deterministic forcing
				*noise, // noise
				*x, 	// solution
				*df, 	// derivative of forcing
				*dx;	// derivative of solution
// and their Fourier transforms computed on the same time-window:
fftw_complex 	*ft_f,
				*ft_noise,
				*ft_dx,
				*ft_df;
fftw_plan  plan_f_tf, plan_n_tn, plan_tdf_df, plan_tdx_dx, plan_tx_x;



// some variables, used in the case of multiple Langevin equations:
double *A_tmp, *dA_tmp1, *dA_tmp2, *dA_tmp3, *dA_tmp4,
       *f_intermed, *f_old_tmp, *f_new_tmp;



/********************************************************************************/
/* systeme differentiel correspondant a 1 equation de Langevin (for RK4)		*/
/* ordre 1 (overdamped Langevin eq.)											*/
/* f contient la partie deterministe + le bruit, au temps initial				*/
/********************************************************************************/
double time_derivative_o1(double A_tmp, double nu, double k, double f, double dt)
{	double dA;

	dA = dt*( - k*A_tmp + f )/nu;
	return(dA);
}

/********************************************************************************/
/* systeme differentiel correspondant a 1 equation de Langevin (for RK4)		*/
/* ordre 2 (regular Langevin eq.)											*/
/* f contient la partie deterministe + le bruit, au temps initial				*/
/********************************************************************************/
int time_derivative_o2(double *A_tmp, double m, double nu, double k, double f, double dt, double *dA)
{
	dA[0] = (double)dt*( A_tmp[1] );
	dA[1] = (double)dt*( -k*A_tmp[0] -nu*A_tmp[1] + f )/m;
	return(0);
}


/********************************************************************************/
/* systeme differentiel correspondant a n equations de Langevin (for RK4)		*/
/* ordre 1 (overdamped Langevin eqs.)											*/
/* f contient la partie deterministe + le bruit, au temps initial				*/
/********************************************************************************/
int time_derivative_o1_n_variables(double *A_tmp, double nu, double k, double *f, double dt, double *dA)
{   register int i;

    // linear part:
	for (i=0; i<n_variables; i++)
        dA[i] = dt*( - k*A_tmp[i] + f[i] )/nu;
    // coupling :
/*    for (i=0; i<n_variables; i++) // not written yet !
        dA[i] += dt*( A_tmp[i-1])/nu;
*/
	return(0);
}

/********************************************************************************/
/* systeme differentiel correspondant a n equations de Langevin (for RK4)		*/
/* ordre 2 (regular Langevin eqs.)									 		    */
/* f contient la partie deterministe + le bruit, au temps initial				*/
/********************************************************************************/
int time_derivative_o2_n_variables(double *A_tmp, double m, double nu, double k, double *f, double dt, double *dA)
{	register int j;

    for (j=0; j<n_variables; j++)
	{   dA[2*j+0] = (double)dt*( A_tmp[2*j+1] );
	    dA[2*j+1] = (double)dt*( -k*A_tmp[2*j+0] -nu*A_tmp[2*j+1] + f[j] )/m;
    }
	return(0);
}





/********************************************************************************/
/* Runge-Kutta method with order 4												*/
/* NG 	28.04.2005 for Runge-Kutta 4th-order									*/
/********************************************************************************/
/* ordre 1																		*/
/* la valeur de la solution au temps final = temps inital + dt est retourn�e	*/
/********************************************************************************/
double integrate_RK4_Langevin_o1(double A_old, double nu, double k, double f_old, double f_new, double dt)
{	double  A=0;
	double  A_tmp;
	double  dA_tmp1, dA_tmp2, dA_tmp3, dA_tmp4;
 	register int i=0;			/* compteur des pas intermediaires	*/

	while (i<nbpas_RK4)
	{
		A_tmp = A_old;		  		dA_tmp1 = time_derivative_o1(A_tmp, nu, k, f_old, 		     dt);
		A_tmp = A_old + dA_tmp1/2.;	dA_tmp2 = time_derivative_o1(A_tmp, nu, k, (f_old+f_new)/2., dt);
		A_tmp = A_old + dA_tmp2/2.;	dA_tmp3 = time_derivative_o1(A_tmp, nu, k, (f_old+f_new)/2., dt);
		A_tmp = A_old + dA_tmp3;	dA_tmp4 = time_derivative_o1(A_tmp, nu, k, f_new,            dt);

		A     = A_old + (dA_tmp1/2. + dA_tmp2 + dA_tmp3 + dA_tmp4/2.)/3.;
		A_old = A;
		i++;
	}
	return(A);
}/* end of the function "integrate_RK4_Langevin"								*/
/********************************************************************************/





/********************************************************************************/
/* Runge-Kutta method with order 4												*/
/* NG 	28.04.2005 for Runge-Kutta 4th-order									*/
/********************************************************************************/
/* ordre 2																		*/
/* la valeur A_new  au temps final = temps inital + dt est retourn�e			*/
/********************************************************************************/
int integrate_RK4_Langevin_o2(double *A_old, double m, double nu, double k, double f_old, double f_new, double dt, double *A_new)
{	double  A_tmp[2];
	double  dA_tmp1[2], dA_tmp2[2], dA_tmp3[2], dA_tmp4[2];
 	register int i=0;			/* compteur des pas intermediaires	*/

	while (i<nbpas_RK4)
	{
		A_tmp[0] = A_old[0];
		A_tmp[1] = A_old[1];	time_derivative_o2(A_tmp, m, nu, k, f_old, dt, dA_tmp1);
		A_tmp[0] = A_old[0] + dA_tmp1[0]/2.;
		A_tmp[1] = A_old[1] + dA_tmp1[1]/2.;
								time_derivative_o2(A_tmp, m, nu, k, (f_old+f_new)/2., dt, dA_tmp2);
		A_tmp[0] = A_old[0] + dA_tmp2[0]/2.;
		A_tmp[1] = A_old[1] + dA_tmp2[1]/2.;
								time_derivative_o2(A_tmp, m, nu, k, (f_old+f_new)/2., dt, dA_tmp3);
		A_tmp[0] = A_old[0] + dA_tmp3[0];
		A_tmp[0] = A_old[0] + dA_tmp3[0];
								time_derivative_o2(A_tmp, m, nu, k, f_new, dt, dA_tmp4);

		A_new[0] = A_old[0] + (dA_tmp1[0]/2. + dA_tmp2[0] + dA_tmp3[0] + dA_tmp4[0]/2.)/3.;
		A_new[1] = A_old[1] + (dA_tmp1[1]/2. + dA_tmp2[1] + dA_tmp3[1] + dA_tmp4[1]/2.)/3.;

		A_old[0] = A_new[0];
		A_old[1] = A_new[1];

		i++;
	}
	return(0);
}/* end of the function "integrate_RK4_Langevin_o2"				                */
/********************************************************************************/




/********************************************************************************/
/* Runge-Kutta method with order 4, for multipe coupled Langevin eqs.			*/
/* NG 	23/10/2006 for coupled Langevin equations        						*/
/********************************************************************************/
/* ordre 1																		*/
/* la valeur de la solution au temps final = temps inital + dt est retourn�e	*/
/********************************************************************************/
int integrate_RK4_Langevin_o1_n_variables(double *A_old, double nu, double k, double *f_old, double *f_new, double dt, double *A_new)
{	register int i=0,j;			/* compteur des pas intermediaires	*/

	while (i<nbpas_RK4)
	{
        for (j=0; j<n_variables; j++) f_intermed[j] = (f_old[j]+f_new[j])/2.;

		for (j=0; j<n_variables; j++) A_tmp[j] = A_old[j];
            time_derivative_o1_n_variables(A_tmp, nu, k, f_old, 		   dt, dA_tmp1);
		for (j=0; j<n_variables; j++) A_tmp[j] = A_old[j] + dA_tmp1[j]/2.;
            time_derivative_o1_n_variables(A_tmp, nu, k, f_intermed, dt, dA_tmp2);
		for (j=0; j<n_variables; j++) A_tmp[j] = A_old[j] + dA_tmp2[j]/2.;
            time_derivative_o1_n_variables(A_tmp, nu, k, f_intermed, dt, dA_tmp3);
		for (j=0; j<n_variables; j++) A_tmp[j] = A_old[j] + dA_tmp3[j];
            time_derivative_o1_n_variables(A_tmp, nu, k, f_new,            dt, dA_tmp4);

        for (j=0; j<n_variables; j++) A_tmp[j] = A_old[j] + dA_tmp3[j];
        {	A_new[j] = A_old[j] + (dA_tmp1[j]/2. + dA_tmp2[j] + dA_tmp3[j] + dA_tmp4[j]/2.)/3.;
		    A_old[j] = A_new[j];
        }

		i++;
	}
	return(0);
}/* end of the function "integrate_RK4_Langevin"								*/
/********************************************************************************/





/********************************************************************************/
/* Runge-Kutta method with order 4												*/
/* NG 	28.04.2005 for Runge-Kutta 4th-order									*/
/********************************************************************************/
/* ordre 2																		*/
/* la valeur A_new  au temps final = temps inital + dt est retourn�e			*/
/********************************************************************************/
int integrate_RK4_Langevin_o2_n_variables(double *A_old, double m, double nu, double k, double *f_old, double *f_new, double dt, double *A_new)
{	register int i=0, j;			/* compteur des pas intermediaires	*/

	while (i<nbpas_RK4)
	{
        for (j=0; j<n_variables; j++) f_intermed[j] = (f_old[j]+f_new[j])/2.;

		for (j=0; j<2*n_variables; j++)
        {   A_tmp[j] = A_old[j];
        }
        time_derivative_o2_n_variables(A_tmp, m, nu, k, f_old, dt, dA_tmp1);

		for (j=0; j<2*n_variables; j++)
        {   A_tmp[j] = A_old[j] + dA_tmp1[j]/2.;
        }
        time_derivative_o2_n_variables(A_tmp, m, nu, k, f_intermed, dt, dA_tmp2);
		for (j=0; j<2*n_variables; j++)
        {   A_tmp[j] = A_old[j] + dA_tmp2[j]/2.;
        }
		time_derivative_o2_n_variables(A_tmp, m, nu, k, f_intermed, dt, dA_tmp3);
        for (j=0; j<2*n_variables; j++)
        {   A_tmp[j] = A_old[j] + dA_tmp3[j];
        }
		time_derivative_o2_n_variables(A_tmp, m, nu, k, f_new, dt, dA_tmp4);

		for (j=0; j<2*n_variables; j++)
        {   A_new[j] = A_old[j] + (dA_tmp1[j]/2. + dA_tmp2[j] + dA_tmp3[j] + dA_tmp4[j]/2.)/3.;
            A_old[j] = A_new[j];
        }

		i++;
	}
	return(0);
}/* end of the function "integrate_RK4_Langevin_o2"				                */
/********************************************************************************/




/********************************************************************************/
/* this will computed the solution, and save it in variables x anbd dx          */
/********************************************************************************/
int integrate_langevin_equation_RK4(int ny, double dt, double m, double nu, double k)
{
	double	A, Av_old[2], Av_new[2];
	register int 	i;

	// initialization of initial conditions:
	A = x[0];
	Av_old[0] = A;
	Av_old[1] = dx[0];

	// integration:
	if (m==0)		// first order Langevin equation
	for (i=1; i<ny; i++)
	{ 	  A = integrate_RK4_Langevin_o1(A, nu, k, f[i-1]+noise[i-1], f[i]+noise[i], dt);
		  x[i]  = A;
		  dx[i] = (x[i]-x[i-1])/dt;
	}
	else			// first order Langevin equation
	for (i=1; i<ny; i++)
	{ 	  integrate_RK4_Langevin_o2(Av_old, m, nu, k, f[i-1]+noise[i-1], f[i]+noise[i], dt, Av_new);
		  Av_old[0] = Av_new[0];
		  Av_old[1] = Av_new[1];
		  x[i]  = Av_new[0];
		  dx[i] = Av_new[1];
	}

	return( 0 );
} // end of function "integrate_Langevin_equation_RK4"





/********************************************************************************/
/* this will computed the solution, and save it in variables x anbd dx          */
/********************************************************************************/
int integrate_langevin_equation_RK4_n_variables(int ny, double dt, double m, double nu, double k)
{
	double	*A, *Av_old, *Av_new;
	register int i,j;

	A      = fftw_malloc(  n_variables*sizeof(double));
	Av_old = fftw_malloc(2*n_variables*sizeof(double));
	Av_new = fftw_malloc(2*n_variables*sizeof(double));

	// initialization of initial conditions: (first slot of the pointer)
	for (j=0; j<n_variables; j++)
	{   A[j] = x[j];
	    Av_old[2*j+0] = x[j];   // mode x
	    Av_old[2*j+1] = dx[j];  // mode dx/dt
    }

	// integration:
	if (m==0)		// first order Langevin equation
	for (i=1; i<ny; i++)
	{ 	  for (j=0; j<n_variables; j++) // forcing term = deterministic part + noise part
          {   f_old_tmp[j] = f[i-1] + noise[(i-1)*n_variables + j];
              f_new_tmp[j] = f[i]   + noise[(i)*n_variables   + j];
          }
          integrate_RK4_Langevin_o1_n_variables(A, nu, k, f_old_tmp, f_new_tmp, dt, Av_new);
	      for (j=0; j<n_variables; j++)
		  {   x[i*n_variables+j]  = Av_new[j];
		      dx[i*n_variables+j] = (x[i*n_variables+j]-x[(i-1)*n_variables+j])/dt;
          }
	}
	else			// second order Langevin equation
	for (i=1; i<ny; i++)
	{ 	  for (j=0; j<n_variables; j++) // forcing term = deterministic part + noise part
          {   f_old_tmp[j] = f[i-1] + noise[(i-1)*n_variables + j];
              f_new_tmp[j] = f[i]   + noise[(i)*n_variables   + j];
          }
          integrate_RK4_Langevin_o2_n_variables(Av_old, m, nu, k, f_old_tmp, f_new_tmp, dt, Av_new);
		  for (j=0; j<n_variables; j++)
		  {   Av_old[2*j+0] = Av_new[2*j+0];
		      Av_old[2*j+1] = Av_new[2*j+1];
    		  x[i*n_variables+j]  = Av_new[2*j+0];
		      dx[i*n_variables+j] = Av_new[2*j+1];
          }
	}

    fftw_free(A);
    fftw_free(Av_new);
    fftw_free(Av_old);

	return(0);
} // end of function "integrate_Langevin_equation_RK4_n_variables"





/********************************************************************************/
/* derivate (the forcing), order 2 in time, assuming periodicity				*/
/********************************************************************************/
int compute_derivative_periodic(int ny, double dt, double *y, double *dy_dt)
{	register int i;

	for (i=1; i<ny-1; i++)
	{	dy_dt[i] = (y[i+1]-y[i-1])/(2.*dt);
	}
	dy_dt[0]    = (y[1]-y[ny-1])/(2.*dt);
	dy_dt[ny-1] = (y[0]-y[ny-2])/(2.*dt);

	return(0);
}




/********************************************************************************/
/* allocate global variables, containing time series of forcing and solution	*/
/* for a regular , 1 variable Langevin eq., n_variables=1                       */
/* for 2 coupled Langevin eq., n_variables=2                                    */
/********************************************************************************/
int init_Langevin_equation_common(int ny, int n_var)
{
	f        = fftw_malloc((ny)*sizeof(double));		// to store initial data
    noise    = fftw_malloc((ny*n_variables)*sizeof(double));
    dx       = fftw_malloc((ny*n_variables)*sizeof(double));
    df       = fftw_malloc((ny)*sizeof(double));
    x        = fftw_malloc((ny*n_variables)*sizeof(double));

    nbpas_RK4   = 1;
    n_variables = n_var;

    if (n_variables>=1)
    {  A_tmp   = fftw_malloc(n_variables*2);
       dA_tmp1 = fftw_malloc(n_variables*2);
       dA_tmp2 = fftw_malloc(n_variables*2);
       dA_tmp3 = fftw_malloc(n_variables*2);
       dA_tmp4 = fftw_malloc(n_variables*2);
       f_intermed = fftw_malloc(n_variables);
       f_old_tmp  = fftw_malloc(n_variables);
       f_new_tmp  = fftw_malloc(n_variables);
    }

    return(0);
}




/********************************************************************************/
/* allocate global variables, containing Fourier transform of					*/
/* time series of forcing and solution, and fft_plans							*/
/********************************************************************************/
int init_Langevin_equation_Fourier(int ny, int bool_filter_Fourier)
{
	if (bool_filter_Fourier==1)
	{	// 'current_filter' is defined in fft_filter_lib.c and .h
		if (current_filter!=NULL) 		free_filter(current_filter);
			current_filter=construct_filter(ny/2+1, (REAL_INPUT | REAL_OUTPUT | SYM),1 );
//		if (current_filter==NULL)		return win_printf_OK("filter is NULL!!");
//		if (current_filter->n==0)		return win_printf_OK("filter has 0 points!!");
//		if (current_filter->f==NULL)	return win_printf_OK("filter data is NULL!!");
		// win_printf("filter is tested");
	}

	ft_f     = fftw_malloc((ny/2+1)   *sizeof(fftw_complex));
    ft_noise = fftw_malloc((ny/2+1)   *sizeof(fftw_complex));
    ft_dx    = fftw_malloc((ny/2+1)   *sizeof(fftw_complex));
    ft_df    = fftw_malloc((ny/2+1)   *sizeof(fftw_complex));

    plan_f_tf   = fftw_plan_dft_r2c_1d(ny, f, ft_f, FFTW_ESTIMATE);
    plan_n_tn   = fftw_plan_dft_r2c_1d(ny, noise, ft_noise, FFTW_ESTIMATE);
	plan_tdf_df = fftw_plan_dft_c2r_1d(ny, ft_df, df, FFTW_ESTIMATE);
	plan_tdx_dx = fftw_plan_dft_c2r_1d(ny, ft_dx, dx, FFTW_ESTIMATE);
    plan_tx_x   = fftw_plan_dft_c2r_1d(ny, ft_noise, x, FFTW_ESTIMATE);

	return(0);
}




/* after intergation, we free the space used by temporary variables */
int free_Langevin_equation_common(void)
{
	fftw_free(f);
	fftw_free(noise);
    fftw_free(dx);
    fftw_free(df);
    fftw_free(x);

    if (n_variables>=1)
    {  fftw_free(A_tmp);
       fftw_free(dA_tmp1);    fftw_free(dA_tmp2);
       fftw_free(dA_tmp3);    fftw_free(dA_tmp4);
       fftw_free(f_intermed); fftw_free(f_old_tmp); fftw_free(f_new_tmp);
    }

    return(0);
}



/* after intergation, we free the space used by temporary variables */
int free_Langevin_equation_Fourier(void)
{
	fftw_free(ft_f);
	fftw_free(ft_noise);
	fftw_free(ft_dx);
	fftw_free(ft_df);

	fftw_destroy_plan(plan_f_tf);
	fftw_destroy_plan(plan_n_tn);
	fftw_destroy_plan(plan_tdf_df);
	fftw_destroy_plan(plan_tdx_dx);
	fftw_destroy_plan(plan_tx_x);

	return(0);
}






void complex_divide(fftw_complex a, fftw_complex b, fftw_complex c)
{	double modulus_squared;

	modulus_squared = b[0]*b[0] + b[1]*b[1];

	c[0] = ( a[0]*b[0] + a[1]*b[1])/modulus_squared;
	c[1] = (-a[0]*b[1] + a[1]*b[0])/modulus_squared;

	return;
}



int integrate_langevin_equation_Fourier(int ny, double dt, double m, double nu, double k,
							int bool_hanning, int bool_filter_Fourier, double f_acq)
{
	fftw_complex IH, tmp_1, tmp_2;
	double	omega;
	double	factor;
register int 	i;
	int		n_fft=ny;

	if (bool_hanning==1)
	{	fft_window_real(ny, f, 0.001);
		fft_window_real(ny, noise, 0.001);
	}

	fftw_execute(plan_f_tf);
	fftw_execute(plan_n_tn);

	for (i=0; i<(ny+1)/2; i++)	// the fft has ny/2+1 complex points // loop was initially with (ny+1)/2
     {	omega = (i*f_acq/n_fft*2*M_PI); // pulsation

     	// derivate of the forcing
     	ft_df[i][0] = -omega*ft_f[i][1];
     	ft_df[i][1] =  omega*ft_f[i][0];

     	// transfert function IH = 1/H :
     	IH[0]  =   (k-m*omega*omega);
     	IH[1]  =  -nu*(-omega);

     	// total forcing :
     	tmp_1[0] = ft_f[i][0] + ft_noise[i][0];
     	tmp_1[1] = ft_f[i][1] + ft_noise[i][0];

     	// solution :
     	complex_divide  (tmp_1, IH, tmp_2);  // now, tmp_2 contains the FT of the solution x
     	ft_noise[i][0]=tmp_2[0];
     	ft_noise[i][1]=tmp_2[1];

     	// derivate of the solution
     	ft_dx[i][0] = -omega*ft_noise[i][1];
     	ft_dx[i][1] =  omega*ft_noise[i][0];

	}
	if (ny%2==0) // the Nyquist frequency is represented by a mode which is purely real (like mode 0)
	{ 	ft_noise[ny/2][0]=0;
		ft_noise[ny/2][1]=0;
		ft_dx[ny/2][0]=0;
		ft_dx[ny/2][1]=0;
		ft_df[ny/2][0]=0;
		ft_df[ny/2][1]=0;
	}

	if (bool_filter_Fourier==1)
	{	apply_filter_complex(current_filter, ft_noise, (ny/2+1));	// the fft has ny/2+1 complex points
		apply_filter_complex(current_filter, ft_dx,    (ny/2+1));	// the fft has ny/2+1 complex points
		apply_filter_complex(current_filter, ft_df,    (ny/2+1));	// the fft has ny/2+1 complex points
	}


	// we Fourier-transform back:
	{	fftw_execute(plan_tx_x);
		fftw_execute(plan_tdf_df);
		fftw_execute(plan_tdx_dx);

		if (bool_hanning==1)
		{	fft_unwindow_real(ny, x, 0.001);
			fft_unwindow_real(ny, df, 0.001);
			fft_unwindow_real(ny, dx, 0.001);
		}
	}

	// we normalize the iffts:
	factor = (double)(1./ny);
	for (i=0; i<ny; i++)
	{	x[i] *= factor;
		df[i]    *= factor;
		dx[i]    *= factor;
	}

	return(0);
} // end of the integrate_langevin_equation_Fourier function
