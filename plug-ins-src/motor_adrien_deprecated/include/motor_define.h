#ifndef motor_H
#define motor_H

// From pifoc.c

#define CNA_1   1
#define CNA_2   2
#define CNA_3   3
#define CAN_1   3 /*1*/

#define CANAL_PIEZZO_READ CAN_1
#define CANAL_PIEZZO_WRITE 3

#ifndef PORT_A_AD
#define PORT_A_AD     0x00
#endif

#ifndef PORT_B_AD
#define PORT_B_AD     0x01
#endif

#ifndef PORT_C_AD
#define PORT_C_AD     0x02
#endif

#define PIEZZO_100	100.0
#define	PIEZZO_250	250.0

//

#define PCI_6503   256
#define PCI_6602   232 

/* #define MOTOR_P_MODE 2 */
/* #define MOTOR_R_MODE 3 */
/* #define MOTOR_T_MODE 4 */

#define MOTOR_T_POINTS_PER_MM 8000
#define MOTOR_R_POINTS_PER_TURN 4000

#define PORT_A 0
#define PORT_B 1
#define PORT_C 2
#define NO_HANDSHAKING 0
#define HANDSHAKING 1
#define IN_SIGNAL 0
#define OUT_SIGNAL 1
#define PCI6503                     256

/****************************INTERFACE CONTROLER REGISTERS*********************/

#define MOTOR_1                        0
#define MOTOR_2                        1
#define MOTOR_1_2                      2  /********************A verifier******************/
#define READ_V_MAX_1 0x00
#define READ_V_MAX_2 0x08

#define LECT_POS_1_1 0x10
#define LECT_POS_1_2 0x18
#define LECT_POS_1_3 0x20
#define LECT_POS_1_4 0x28

#define LECT_POS_2_1 0x30
#define LECT_POS_2_2 0x38
#define LECT_POS_2_3 0x40
#define LECT_POS_2_4 0x48

#define CONSIGNE_POS_1_1 0x50
#define CONSIGNE_POS_1_2 0x58
#define CONSIGNE_POS_1_3 0x60
#define CONSIGNE_POS_1_4 0x68

#define CONSIGNE_POS_2_1 0x70
#define CONSIGNE_POS_2_2 0x78
#define CONSIGNE_POS_2_3 0x80
#define CONSIGNE_POS_2_4 0x88

#define TEST_COMMAND_PID      0xA0
#define COMMAND_PID      0x90
#define PARAMETER_PID    0x98
#define SPEED_FEEDBACK_VMAX_MOT_1  		0xC0
#define SPEED_FEEDBACK_VMAX_MOT_2  		0xC8
#define POS_FEEDBACK_LIM_1_MOT_1     	0xD0
#define POS_FEEDBACK_LIM_2_MOT_1     	0xD8
#define POS_FEEDBACK_LIM_1_MOT_2     	0xE0
#define POS_FEEDBACK_LIM_2_MOT_2     	0xE8
#define ACCELERATION_1   0x01
#define ACCELERATION_2   0x02
#define P_1              0x10
#define I_1              0x11
#define D_1              0x12
#define R_1              0x13
#define P_2              0x20
#define I_2              0x21
#define D_2              0x22
#define R_2              0x23
#define RESET_MOT_1    0xB1
#define RESET_MOT_2    0xB2
#define RESET_MOT_1_2  0xB3

#define INITIALISATION       0xF8

#define CARD_TYPE            0xF0
#define CARD_ADRESS			 0X02

#endif
