#ifndef motor_function_H
#define motor_function_H

// From pifoc.c

/* PXV_FUNC(int, what_is_piezzo, (void)); */
PXV_FUNC(int, init_CAN, (void));
PXV_FUNC(int, IO_write, (unsigned char canal, short int data));
PXV_FUNC(int, IO_read, (unsigned char canal));
/* int Read_ADC_value(void) */
/* int Write_DAC_value(void) */
PXV_FUNC(int, set_piezzo, (unsigned short int piezzo_pos));
/* PXV_FUNC(int, set_piezzo_in_micron, (float piezzo_pos)); */
/* PXV_FUNC(float, read_piezzo_in_micron, (void)); */
PXV_FUNC(unsigned short int, read_piezzo, (void));
PXV_FUNC(int, convert__microns_to_unsigned_points, (float microns, unsigned short int* points));
PXV_FUNC(int, convert__microns_to_signed_points, (float microns, short int* points));
PXV_FUNC(int, convert__points_to_microns, (unsigned short int points, float* microns));
//PXV_FUNC(float, convert__points_to_microns, (unsigned short int points));
PXV_FUNC(int, IO__whatis__pifoc_type, (int* piezzo_type));
PXV_FUNC(int, IO__whatis__motor_p_position_in_microns, (float* position_in_microns));
PXV_FUNC(int, IO__whatis__motor_p_consigne_in_microns, (float* position_in_microns));
PXV_FUNC(int, IO__whatis__motor_p_consigne_in_points, (unsigned short int* position_in_points));
PXV_FUNC(int, IO__whatis__motor_p_position_in_percent, (float* position_in_percent));

PXV_FUNC(MENU, *pifoc_menu,(void));
PXV_FUNC(int, pifoc_main, (void));

// From motor.c

PXV_FUNC(int, detect_NI_boards,(void));
PXV_FUNC(int,  read_card_type,(void));
PXV_FUNC(int, new_res_motor_1,(void));
PXV_FUNC(int, new_res_motor_2,(void));
PXV_FUNC(int, init_motors,(void));
PXV_FUNC(int, motor_main,(void));
PXV_FUNC(int, IO__whatis__motor_t_position_in_points, (long* position_in_points));
PXV_FUNC(int, IO__whatis__motor_t_position_in_points, (long* position_in_points));

// From user2card.c

PXV_FUNC(int, send_command_to_controller, (unsigned char command,unsigned char card_adress,unsigned char data));
PXV_FUNC(unsigned char, read_data_from_controller, (unsigned char command,unsigned char card_adress));
PXV_FUNC(int, set_Vmax_moteur_1,(unsigned char v_max_1));
PXV_FUNC(int, set_Vmax_moteur_2,(unsigned char v_max_2));
PXV_FUNC(int, set_speed_feedback_1,(unsigned char feedback_speed));
PXV_FUNC(int, set_speed_feedback_2,(unsigned char feedback_speed));
PXV_FUNC(int, set_acc_pos_feedback_1,(unsigned char acc));
PXV_FUNC(int, set_acc_pos_feedback_2,(unsigned char acc));
PXV_FUNC(int, set_consigne_pos_moteur_1,(long cons_pos_1));
PXV_FUNC(int, set_consigne_pos_moteur_2,(long cons_pos_2));
PXV_FUNC(int, set_lim_dec_feedback_pos_1,(short int lim_pos_feedback));
PXV_FUNC(int, set_lim_dec_feedback_pos_2,(short int lim_pos_feedback));
PXV_FUNC(long int, read_pos_motor_1,(void));
PXV_FUNC(long int, read_pos_motor_2,(void));
PXV_FUNC(int, new_P_motor_1,(unsigned char new_P_1));
PXV_FUNC(int, new_P_motor_2,(unsigned char new_P_2));
PXV_FUNC(int, new_I_motor_1,(unsigned char new_I_1));
PXV_FUNC(int, new_I_motor_2,(unsigned char new_I_2));
PXV_FUNC(int, new_D_motor_1,(unsigned char new_D_1));
PXV_FUNC(int, new_D_motor_2,(unsigned char new_D_2));
PXV_FUNC(int, new_R_motor_1,(unsigned char new_R_1));
PXV_FUNC(int, new_R_motor_2,(unsigned char new_R_2));

PXV_FUNC(int, IO__whatis__motor_t_points_per_mm, (int* motor_t_points_per_mm ));
PXV_FUNC(int, IO__whatis__motor_r_points_per_turn, (int* motor_r_points_per_turn));

PXV_FUNC(int, IO__whatis__motor_r_consigne_in_turns, (float* position_in_turns));
PXV_FUNC(int, IO__whatis__motor_t_consigne_in_mm, (float* position_in_mm));

// From menu.c

PXV_FUNC(int, new_set_P_1,(void));
PXV_FUNC(int, new_set_P_2,(void));
PXV_FUNC(int, new_set_I_1,(void));
PXV_FUNC(int, new_set_I_2,(void));
PXV_FUNC(int, new_set_D_1,(void));
PXV_FUNC(int, new_set_D_2,(void));
PXV_FUNC(int, new_set_ratio_motor_1,(void));
PXV_FUNC(int, new_set_ratio_motor_2,(void));
PXV_FUNC(int, new_reset_motor_1,(void));
PXV_FUNC(int, new_reset_motor_2,(void));
PXV_FUNC(int, set_cons_max_speed_pos_1,(void));
PXV_FUNC(int, set_cons_max_speed_pos_2,(void));
PXV_FUNC(int, Read_pos_mot_1,(void));
PXV_FUNC(int, Read_pos_mot_2,(void));
PXV_FUNC(int, set_cons_pos_mot_1,(void));
PXV_FUNC(int, set_cons_pos_mot_2,(void));
PXV_FUNC(int, set_vitesse_mot_1,(void));
PXV_FUNC(int, set_vitesse_mot_2,(void));
PXV_FUNC(int, set_acc_pos_1,(void));
PXV_FUNC(int, set_acc_pos_2,(void));
PXV_FUNC(int, set_cons_pos_lim_1,(void));
PXV_FUNC(int, set_cons_pos_lim_2,(void));
PXV_FUNC(MENU, *motor_menu,(void));
PXV_FUNC(int,  menu_main,(void));

// From shortcut.c

PXV_FUNC(int, MOTOR_P__move, (float amount_in_mum));
PXV_FUNC(int, MOTOR_T__move, (float amount_in_mum));
PXV_FUNC(int, MOTOR_R__move, (float amount_in_mum));

/* PXV_FUNC(int, MOTOR_P__up, (float amount_in_mum)); */
/* PXV_FUNC(int, MOTOR_P__down, (float amount_in_mum)); */
/* PXV_FUNC(int, MOTOR_T__up, (float amount_in_mm)); */
/* PXV_FUNC(int, MOTOR_T__down, (float amount_in_mm)); */
/* PXV_FUNC(int, MOTOR_R__up, (float amount_in_turns)); */
/* PXV_FUNC(int, MOTOR_R__down, (float amount_in_turns)); */
PXV_FUNC(int, MOTOR_P__set, (void));
PXV_FUNC(int, MOTOR_R__set, (void));
PXV_FUNC(int, MOTOR_T__set, (void));
PXV_FUNC(int, shortcuts_main, (void));


//PXV_FUNC(int, set_cons_max_speed_lim_1,(void));
//PXV_FUNC(int, set_cons_max_speed_lim_2,(void));
//PXV_FUNC(int, do_detect_card_type,(void));
//PXV_FUNC(int, read_status,(void));
//PXV_FUNC(void, reset_pos,(unsigned char motor));

//XV_ARRAY(MENU,  project_menu);
//XV_FUNC(int, do_quit, (void));

//PXV_FUNC(int, Write,(unsigned char port, unsigned char val));
//PXV_FUNC(unsigned char, Read,(unsigned char port));

#endif
