#ifndef motor_variable_H
#define motor_variable_H

int NI_6602, NI_6503;

int CURRENT_MODE;

// From shortcut.c
float MOTOR_R_FINESSE;
float MOTOR_T_FINESSE_MM;

// From pifoc.c
int small_step;
int big_step;
float obj_position;
float Z_step;
extern float n_rota;
extern float n_magnet_z;
float PIEZZO_TYPE;
float MOTOR_P_FINESSE_MICRONS;

//float GLOBAL__consigne__p_in_mum;
unsigned short int GLOBAL__consigne__p_in_points;
float GLOBAL__consigne__t_in_mm;
float GLOBAL__consigne__r_in_turns;

#endif
