/**
\file motor.c

\brief Program for driving DC motors with C Herrmann controller
\version 11/03
\author JF Allemand	
*/				 				
 
#ifndef motor_C
#define motor_C

#include <allegro.h>
#include <winalleg.h>
#include <xvin.h>
#include <windows.h>
#include <stdio.h>

#include "motor_define.h"
#include "motor_variable.h"
#include "motor_function.h"

#include "nidaqcns.h"
#include "nidaq.h"

/*****************************RESET THE MOTORS (LOW-LEVEL)******************************/

int new_res_motor_1(void)
{		int i;
    	clock_t start;
    	
    	if(updating_menu_state != 0)	return D_O_K;	
        send_command_to_controller(COMMAND_PID,CARD_ADRESS,RESET_MOT_1);
        send_command_to_controller(PARAMETER_PID,CARD_ADRESS,RESET_MOT_1);
        
    for (i=0,start=clock();(clock()-start<(2*CLOCKS_PER_SEC))&&(read_data_from_controller(TEST_COMMAND_PID,CARD_ADRESS)!=255);i++);
    return 0;

}

int new_res_motor_2(void)
{			int i;
    		clock_t start;
    		
    		if(updating_menu_state != 0)	return D_O_K;	
           	send_command_to_controller(COMMAND_PID,CARD_ADRESS,RESET_MOT_2);
        	send_command_to_controller(PARAMETER_PID,CARD_ADRESS,RESET_MOT_2);
        
    for (i=0,start=clock();(clock()-start<(2*CLOCKS_PER_SEC))&&(read_data_from_controller(TEST_COMMAND_PID,CARD_ADRESS)!=255);i++);
    return 0;
}	
  
/****************************PID PARAMETERS (LOW-LEVEL)**************************/

int set_Vmax_moteur_1(unsigned char v_max_1)
{  
  send_command_to_controller(READ_V_MAX_1,CARD_ADRESS,v_max_1);
   
  return 0;
}

int set_Vmax_moteur_2(unsigned char v_max_2)
{   
  send_command_to_controller(READ_V_MAX_2,CARD_ADRESS,v_max_2);
  
  return 0;
}

int set_speed_feedback_1(unsigned char feedback_speed)
{
  send_command_to_controller(SPEED_FEEDBACK_VMAX_MOT_1,CARD_ADRESS,feedback_speed);
    
  return 0;
}

int set_speed_feedback_2(unsigned char feedback_speed)
{	
  send_command_to_controller(SPEED_FEEDBACK_VMAX_MOT_2,CARD_ADRESS,feedback_speed);
  
  return 0;
}

int set_acc_pos_feedback_1(unsigned char acc)
{   
  int i;
  clock_t start;
  send_command_to_controller(COMMAND_PID,CARD_ADRESS,ACCELERATION_1);
  send_command_to_controller(PARAMETER_PID,CARD_ADRESS,acc);
    
  for (i=0,start=clock();(clock()-start<(2*CLOCKS_PER_SEC))&&(read_data_from_controller(TEST_COMMAND_PID,CARD_ADRESS)!=255);i++);
        
  return 0;
}

int set_acc_pos_feedback_2(unsigned char acc)
{  
  int i;
  clock_t start;

  send_command_to_controller(COMMAND_PID,CARD_ADRESS,ACCELERATION_2);
  send_command_to_controller(PARAMETER_PID,CARD_ADRESS,acc);
    
  for (i=0,start=clock();(clock()-start<(2*CLOCKS_PER_SEC))&&(read_data_from_controller(TEST_COMMAND_PID,CARD_ADRESS)!=255);i++);
  return 0;
}

/*int set_speed_feedback_v_lim_1(unsigned char vlim)
  {
  send_command_to_controller(SPEED_FEEDBACK_VLIM_MOT_1,CARD_ADRESS,vlim);
  return 0;
  }

  int set_speed_feedback_v_lim_2(unsigned char vlim)
  {
  send_command_to_controller(SPEED_FEEDBACK_VLIM_MOT_2,CARD_ADRESS,vlim);
  return 0;
  }
*/

int set_lim_dec_feedback_pos_1(short int lim_pos_feedback)
{  
  unsigned char pos0=0;
  unsigned char pos1=0;
	
  pos0=(unsigned char)(lim_pos_feedback&0xFF);
  pos1=(unsigned char)((lim_pos_feedback&0xFF00)>>8);
   
  send_command_to_controller(POS_FEEDBACK_LIM_1_MOT_1,CARD_ADRESS,pos1);
  send_command_to_controller(POS_FEEDBACK_LIM_2_MOT_1,CARD_ADRESS,pos0);
    
  return 0;
}

int set_lim_dec_feedback_pos_2(short int lim_pos_feedback)
{  	
  unsigned char pos0=0;
  unsigned char pos1=1;

  pos0=(unsigned char)(lim_pos_feedback&0xFF);
  pos1=(unsigned char)((lim_pos_feedback&0xFF00)>>8);
  send_command_to_controller(POS_FEEDBACK_LIM_1_MOT_2,CARD_ADRESS,pos1);
  send_command_to_controller(POS_FEEDBACK_LIM_2_MOT_2,CARD_ADRESS,pos0);
   
  return 0;
}

long int read_pos_motor_1(void)
{    
  unsigned char pos1=0;
  unsigned char pos2=0;
  unsigned char pos3=0;
  unsigned char pos4=0;
  long int position_motor_1=0;

  pos1=   read_data_from_controller(LECT_POS_1_1,CARD_ADRESS);
  pos2=   read_data_from_controller(LECT_POS_1_2,CARD_ADRESS);
  pos3=   read_data_from_controller(LECT_POS_1_3,CARD_ADRESS);
  pos4=   read_data_from_controller(LECT_POS_1_4,CARD_ADRESS);
  //draw_bubble(screen,0,750,50,"1 %d pos2 %d  3 %d  4 %d ",(int)pos1, (int)pos2, (int)pos3,(int)pos4);
    
  position_motor_1=pos4+(pos3<<8)+(pos2<<16)+(pos1<<24);

  return position_motor_1;
}

long int read_pos_motor_2(void)
{    
  unsigned char pos1=0;
  unsigned char pos2=0;
  unsigned char pos3=0;
  unsigned char pos4=0;
  long int position_motor_2=0;

  pos1=   read_data_from_controller(LECT_POS_2_1,CARD_ADRESS);
  pos2=   read_data_from_controller(LECT_POS_2_2,CARD_ADRESS);
  pos3=   read_data_from_controller(LECT_POS_2_3,CARD_ADRESS);
  pos4=   read_data_from_controller(LECT_POS_2_4,CARD_ADRESS);
  position_motor_2=pos4+(pos3<<8)+(pos2<<16)+(pos1<<24);
    
  return position_motor_2;
}

int new_P_motor_1(unsigned char new_P_1)
{		
  int i;
  clock_t start;
  send_command_to_controller(COMMAND_PID,CARD_ADRESS,P_1);
  send_command_to_controller(PARAMETER_PID,CARD_ADRESS,new_P_1); 
    
    
  for (i=0,start=clock();(clock()-start<(2*CLOCKS_PER_SEC))&&(read_data_from_controller(TEST_COMMAND_PID,CARD_ADRESS)!=255);i++);
    
  return 0;
}

int new_P_motor_2(unsigned char new_P_2)
{		   
  int i;
  clock_t start;
  send_command_to_controller(COMMAND_PID,CARD_ADRESS,P_2);
  send_command_to_controller(PARAMETER_PID,CARD_ADRESS,new_P_2); 
         
  for (i=0,start=clock();(clock()-start<(2*CLOCKS_PER_SEC))&&(read_data_from_controller(TEST_COMMAND_PID,CARD_ADRESS)!=255);i++);
   
  return 0;
}

int new_I_motor_1(unsigned char new_I_1)
{        int i;
  clock_t start;
  send_command_to_controller(COMMAND_PID,CARD_ADRESS,I_1);
  send_command_to_controller(PARAMETER_PID,CARD_ADRESS,new_I_1); 
       
  for (i=0,start=clock();(clock()-start<(2*CLOCKS_PER_SEC))&&(read_data_from_controller(TEST_COMMAND_PID,CARD_ADRESS)!=255);i++);
  return 0;
}

int new_I_motor_2(unsigned char new_I_2)
{
  int i;
  clock_t start;
  send_command_to_controller(COMMAND_PID,CARD_ADRESS,I_2);
  send_command_to_controller(PARAMETER_PID,CARD_ADRESS,new_I_2);
            
  for (i=0,start=clock();(clock()-start<(2*CLOCKS_PER_SEC))&&(read_data_from_controller(TEST_COMMAND_PID,CARD_ADRESS)!=255);i++);
  return 0;
}

int new_D_motor_1(unsigned char new_D_1)
{
  int i;
  clock_t start;
  send_command_to_controller(COMMAND_PID,CARD_ADRESS,D_1);
  send_command_to_controller(PARAMETER_PID,CARD_ADRESS,new_D_1);
        
  for (i=0,start=clock();(clock()-start<(2*CLOCKS_PER_SEC))&&(read_data_from_controller(TEST_COMMAND_PID,CARD_ADRESS)!=255);i++);
  return 0;
}

int new_D_motor_2(unsigned char new_D_2)
{		int i;
  clock_t start;
  send_command_to_controller(COMMAND_PID,CARD_ADRESS,D_2);
  send_command_to_controller(PARAMETER_PID,CARD_ADRESS,new_D_2);
      
  for (i=0,start=clock();(clock()-start<(2*CLOCKS_PER_SEC))&&(read_data_from_controller(TEST_COMMAND_PID,CARD_ADRESS)!=255);i++);
  return 0;
}

int new_R_motor_1(unsigned char new_R_1)
{	int i;
  clock_t start;

  send_command_to_controller(COMMAND_PID,CARD_ADRESS,R_1);
  send_command_to_controller(PARAMETER_PID,CARD_ADRESS,new_R_1);
        
  for (i=0,start=clock();(clock()-start<(2*CLOCKS_PER_SEC))&&(read_data_from_controller(TEST_COMMAND_PID,CARD_ADRESS)!=255);i++);
  return 0;
}

int new_R_motor_2(unsigned char new_R_2)
{		int i;
  clock_t start;
  send_command_to_controller(COMMAND_PID,CARD_ADRESS,R_2);
  send_command_to_controller(PARAMETER_PID,CARD_ADRESS,new_R_2);
       
  for (i=0,start=clock();(clock()-start<(2*CLOCKS_PER_SEC))&&(read_data_from_controller(TEST_COMMAND_PID,CARD_ADRESS)!=255);i++);
  return 0;
}




/****************************PID PARAMETERS (LOW-LEVEL)**************************/

int init_motors(void)
{
  /* Reset and init motor 1 */
  new_res_motor_1();

  set_lim_dec_feedback_pos_1(200);
  new_P_motor_1(20);
  new_I_motor_1(0);
  new_D_motor_1(255);
  new_R_motor_1(6);
  set_acc_pos_feedback_1(1);
  set_speed_feedback_1(25); /* The smaller, the shorter the time to stop */
  set_Vmax_moteur_1(128);

  /* Reset and init motor 2 */
  new_res_motor_2();

  set_lim_dec_feedback_pos_2(200); 
  new_P_motor_2(20);
  new_I_motor_2(0);
  new_D_motor_2(255);
  new_R_motor_2(6);
  set_acc_pos_feedback_2(1);
  set_speed_feedback_2(25);
  set_Vmax_moteur_2(128);

/*   set_lim_dec_feedback_pos_1(200); */
/*   new_P_motor_1(20); */
/*   new_I_motor_1(0); */
/*   new_D_motor_1(255); */
/*   new_R_motor_1(6); */
/*   set_acc_pos_feedback_1(1); */
/*   set_speed_feedback_1(75); */
/*   set_Vmax_moteur_1(128); */

/*   set_lim_dec_feedback_pos_2(200);  */
/*   new_P_motor_2(40); */
/*   new_I_motor_2(3); */
/*   new_D_motor_2(255); */
/*   new_R_motor_2(6); */
/*   set_acc_pos_feedback_2(1); */
/*   set_speed_feedback_2(127); */
/*   set_Vmax_moteur_2(128); */

  return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
// Motors Control

int set_consigne_pos_moteur_1(long cons_pos_1)
{    
  unsigned char pos1=0;
  unsigned char pos2=0;
  unsigned char pos3=0;
  unsigned char pos4=0;

  pos4=(unsigned char)(cons_pos_1&0xFF);
  pos3=(unsigned char)((cons_pos_1&0xFF00)>>8);
  pos2=(unsigned char)((cons_pos_1&0xFF0000)>>16);
  pos1=(unsigned char)((cons_pos_1&0xFF000000)>>24);
  send_command_to_controller(CONSIGNE_POS_1_1,CARD_ADRESS,pos1);
  send_command_to_controller(CONSIGNE_POS_1_2,CARD_ADRESS,pos2);
  send_command_to_controller(CONSIGNE_POS_1_3,CARD_ADRESS,pos3);
  send_command_to_controller(CONSIGNE_POS_1_4,CARD_ADRESS,pos4);

  GLOBAL__consigne__t_in_mm = ((float) cons_pos_1/MOTOR_T_POINTS_PER_MM);

  return 0;
}

int set_consigne_pos_moteur_2(long cons_pos_2)
{     
  unsigned char pos1=0;
  unsigned char pos2=0;
  unsigned char pos3=0;
  unsigned char pos4=0;
	
  pos4=(unsigned char)(cons_pos_2&0xFF);
  pos3=(unsigned char)((cons_pos_2&0xFF00)>>8);
  pos2=(unsigned char)((cons_pos_2&0xFF0000)>>16);
  pos1=(unsigned char)((cons_pos_2&0xFF000000)>>24);
  send_command_to_controller(CONSIGNE_POS_2_1,CARD_ADRESS,pos1);
  send_command_to_controller(CONSIGNE_POS_2_2,CARD_ADRESS,pos2);
  send_command_to_controller(CONSIGNE_POS_2_3,CARD_ADRESS,pos3);
  send_command_to_controller(CONSIGNE_POS_2_4,CARD_ADRESS,pos4);

  GLOBAL__consigne__r_in_turns = ((float) cons_pos_2/MOTOR_R_POINTS_PER_TURN);

  return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
// I/O

/* Returns points to mm conversion in translation

\author Adrien Meglio
\version 14/05/07
*/
int IO__whatis__motor_t_points_per_mm(int* motor_t_points_per_mm )
{
  if(updating_menu_state != 0)	return D_O_K;

  *motor_t_points_per_mm = MOTOR_T_POINTS_PER_MM;

  return D_O_K;
}

/* Returns points to turns conversion in rotation

\author Adrien Meglio
\version 14/05/07
*/
int IO__whatis__motor_r_points_per_turn(int* motor_r_points_per_turn)
{
  if(updating_menu_state != 0)	return D_O_K;

  *motor_r_points_per_turn = MOTOR_R_POINTS_PER_TURN;

  return D_O_K;
}

/* Returns rotation consigne in turns

\author Adrien Meglio
\version 16/05/07
*/
int IO__whatis__motor_r_consigne_in_turns(float* position_in_turns)
{
  if(updating_menu_state != 0)	return D_O_K;

  *position_in_turns= GLOBAL__consigne__r_in_turns; 
  
  return D_O_K;
}

/* Returns translation consigne in mm

\author Adrien Meglio
\version 16/05/07
*/
int IO__whatis__motor_t_consigne_in_mm(float* position_in_mm)
{
  if(updating_menu_state != 0)	return D_O_K;

  *position_in_mm = GLOBAL__consigne__t_in_mm; 
  
  return D_O_K;
}

/* Returns rotation position in pointsconsigne in turns

\author Adrien Meglio
\version 28/05/07
*/
int IO__whatis__motor_r_position_in_points(long* position_in_points)
{
  long pos;

  if(updating_menu_state != 0)	return D_O_K;

  /* For some unknown reason, the position has to be read twice (if read/written on another canal previsously, eg pifoc, 
   communications parameters may have changed) */
  read_pos_motor_2();
  pos = read_pos_motor_2();

  *position_in_points= pos; 
  
  return D_O_K;
}

/* Returns translation position in mm

\author Adrien Meglio
\version 28/05/07
*/
int IO__whatis__motor_t_position_in_points(long* position_in_points)
{
  long pos;

  if(updating_menu_state != 0)	return D_O_K;

  /* For some unknown reason, the position has to be read twice (if read/written on another canal previsously, eg pifoc, 
   communications parameters may have changed) */
  read_pos_motor_1();
  pos = read_pos_motor_1();

  *position_in_points = pos; 
  
  return D_O_K;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Main functions

int motorfile_main(void)
{
  if(updating_menu_state != 0)	return D_O_K;	

  //win_report("Motors initialization");
  detect_NI_boards();
  init_motors();
  GLOBAL__consigne__t_in_mm = (float) read_pos_motor_1()*MOTOR_T_POINTS_PER_MM;
  GLOBAL__consigne__r_in_turns = (float) read_pos_motor_2()*MOTOR_R_POINTS_PER_TURN;

  return D_O_K;    
}

#endif








