/** \file pifoc.c
PIFOC piezzo handling with the NI 6503 card and C. Herrmann controller.

\author JF Allemand
\author Adrien Meglio

\version 21/05/07
*/

#ifndef PIFOC_C
#define PIFOC_C

#include <allegro.h>
#include <winalleg.h>
#include <xvin.h>
#include <windows.h>
#include <stdio.h>

#include "motor_define.h"
#include "motor_variable.h"
#include "motor_function.h"

#include "nidaqcns.h"
#include <NIDAQmx.h>
#include "nidaq.h"

/*********************************Set positions of the PIFOC (microns)*****************************/

/** Test function
*/
int test_piezzo(void)
{
   if(updating_menu_state != 0)	return D_O_K; 
   
/*    position_micron = read_piezzo_in_micron(); */

  /*  win_scanf("Position %f",&position_micron); */
   
/*    position_point = (int) (65535*(position_micron/PIEZZO_TYPE)); */
   
/*    win_report("Position demand�e : %d/65535 (soit %.2f \mu m)",position_point,position_micron); */
   
/*    position_micron += 1.0; */
   
/*    position_point = (int) (65535*(position_micron/PIEZZO_TYPE)); */
   
/*    win_report("Position incr�ment�e : %d/65535 (soit %.2f \mu m)",position_point,position_micron); */

/*    position_micron += -1.0; */
   
/*    position_point = (int) (65535*(position_micron/PIEZZO_TYPE)); */
   
/*    win_report("Position incr�ment�e : %d/65535 (soit %.2f \mu m)",position_point,position_micron); */
   
  /*  set_piezzo_in_micron(position_micron); */

/*    win_report("Position lue %.2f pour une position demand�e %.2f microns",read_piezzo_in_micron(),position_micron); */

/*    set_piezzo_in_micron(read_piezzo_in_micron()); */
/*    win_report("Position du pifoc %.2f",read_piezzo_in_micron()); */

   unsigned short int read_points;
   float read_microns;
   float read_points_in_microns;
   unsigned short int read_microns_in_points;

   read_points = IO_read(CANAL_PIEZZO_READ);
   read_microns = 0;//read_piezzo_in_micron();
   convert__points_to_microns(read_points,&read_points_in_microns);
   convert__microns_to_unsigned_points(read_microns,&read_microns_in_points);
   
   win_report("Read exactly %d, interpreted as %.2f microns \nPure conversion would yield %.2f microns \nand retroconversion gives %d",read_points,read_microns,read_points_in_microns,read_microns_in_points);  
   
   return D_O_K;
}

/** Sets the piezzo to the required position.

\param unsigned short int piezzo_pos The target position between 0 and 65535.

\author Adrien Meglio
\version 18/05/07
*/
int set_piezzo(unsigned short int piezzo_pos)
{
  if(updating_menu_state != 0)	return D_O_K;	

  IO_write(CANAL_PIEZZO_WRITE,piezzo_pos);

  //win_report("Pifoc set to %d from %.2f",(int)((piezzo_pos/PIEZZO_TYPE)*65535),piezzo_pos);

  GLOBAL__consigne__p_in_points = piezzo_pos;

  return D_O_K;
}

/* int set_piezzo_in_micron(float piezzo_pos) */
/* { */
/*   if(updating_menu_state != 0)	return D_O_K;	 */

/*   IO_write((unsigned char)CANAL_PIEZZO_WRITE,convert__microns_to_points(piezzo_pos)); */

/*   //win_report("Pifoc set to %d from %.2f",(int)((piezzo_pos/PIEZZO_TYPE)*65535),piezzo_pos); */

/*   GLOBAL__consigne__p_in_mum = piezzo_pos; */

/*  return D_O_K; */
/* } */

/*********************************Read positions of the PIFOC (microns)************/
/* float read_piezzo_in_micron(void) */
/* { */
/*   float microns = 0.0; */

/*   if(updating_menu_state != 0)	return microns; */

/*   microns = convert__points_to_microns(read_piezzo()); */
  
/*   return microns; */
/* } */

/** Returns piezzo position in points.

\author Adrien Meglio
\version 18/05/07
*/
unsigned short int read_piezzo(void)
{  
  int points = 0;
 
  if(updating_menu_state != 0)	return points;
  
  points = IO_read(CANAL_PIEZZO_READ);
  
  return points;
}

/** Conversion of microns equivalent (float) to piezzo points (unsigned short int).

\param float microns The argument to be converted (must be positive)
\return unsigned short int The result

\author Adrien Meglio
\version 18/05/07
*/
/* unsigned short int convert__microns_to_unsigned_points(float microns) */
/* { */
/*   int points = 0; */

/*   if(updating_menu_state != 0)	return points; */
  
/*   points = (unsigned short int) ((microns/PIEZZO_TYPE)*65535); */
  
/*   return points; */
/* } */
int convert__microns_to_unsigned_points(float microns, unsigned short int* points)
{
  if(updating_menu_state != 0)	return D_O_K;
  
  //win_report("%d",(unsigned short int) ((microns/PIEZZO_TYPE)*65535));

  *points = (unsigned short int) ((microns/PIEZZO_TYPE)*65535);
  
  return D_O_K;
}

/** Conversion of microns equivalent (float) to piezzo points (short int).

\param float microns The argument to be converted (may be negative)
\return short int The result

\author Adrien Meglio
\version 18/05/07
*/
/* short int convert__microns_to_signed_points(float microns) */
/* { */
/*   int points = 0; */

/*   if(updating_menu_state != 0)	return points; */
  
/*   points = (short int) ((microns/PIEZZO_TYPE)*65535); */
  
/*   return points; */
/* } */
int convert__microns_to_signed_points(float microns, short int* points)
{
  if(updating_menu_state != 0)	return D_O_K;
  
  *points = (short int) ((microns/PIEZZO_TYPE)*65535);
  
  return D_O_K;
}

/** Conversion of piezzo points (short int) to microns equivalent (float).

\param unsigned short int points The argument to be converted (must be positive)
\return float The result

\author Adrien Meglio
\version 18/05/07
*/
/* float convert__points_to_microns(unsigned short int points) */
/* { */
/*   float microns = 0.0; */

/*   if(updating_menu_state != 0)	return microns; */
  
/*   microns = ((float) points)*PIEZZO_TYPE/65535; */
  
/*   return microns; */
/* } */

int convert__points_to_microns(unsigned short int points, float* microns)
{
  if(updating_menu_state != 0)	return D_O_K;
  
  *microns = ((float) points)*PIEZZO_TYPE/65535;
  
  return D_O_K;
}

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
// IO Functions

/* Returns piezzo type.

\param int* piezzo_type The function will return the value in piezzo_type

\author Adrien Meglio
\version 14/05/07
*/
int IO__whatis__pifoc_type(int* piezzo_type)
{
  if(updating_menu_state != 0)	return D_O_K;

  *piezzo_type = PIEZZO_TYPE;

  return D_O_K;
}

/* Returns PIFOC position in microns.

\param float* position_in_microns The function will return the value in position_in_microns

\author Adrien Meglio
\version 4/05/07
*/
int IO__whatis__motor_p_position_in_microns(float* position_in_microns)
{
  float temp_microns;

  if(updating_menu_state != 0)	return D_O_K;

  convert__points_to_microns(read_piezzo(),&temp_microns); 

  *position_in_microns = temp_microns;

  return D_O_K;
}

/* Returns PIFOC consigne in microns.

\param float* position_in_microns The function will return the value in position_in_microns

\author Adrien Meglio
\version 16/05/07
*/
int IO__whatis__motor_p_consigne_in_microns(float* position_in_microns)
{
  float temp_microns;

  if(updating_menu_state != 0)	return D_O_K;

  convert__points_to_microns(GLOBAL__consigne__p_in_points,&temp_microns); 

  *position_in_microns = temp_microns;    
  
  return D_O_K;
}

/* Returns PIFOC consigne in points.

\param float* position_in_points The function will return the value in position_in_points

\author Adrien Meglio
\version 28/05/07
*/
int IO__whatis__motor_p_consigne_in_points(unsigned short int* position_in_points)
{
  if(updating_menu_state != 0)	return D_O_K;

  *position_in_points = GLOBAL__consigne__p_in_points;    
  
  return D_O_K;
}

/* Returns PIFOC position in percent.

\param float* position_in_percent The function will return the value in position_in_percent

\author Adrien Meglio
\version 4/05/07
*/
int IO__whatis__motor_p_position_in_percent(float* position_in_percent)
{
  if(updating_menu_state != 0)	return D_O_K;

  *position_in_percent = 100.0*read_piezzo()/65535; 

  return D_O_K;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Main functions

/** Menu function for file \a pifoc.c

\author Adrien Meglio
\version 18/05/07
*/
MENU *pifoc_menu(void)
{
  static MENU mn[32];
  
  if (mn[0].text != NULL)	return mn;
  
  add_item_to_menu(mn,"Test PIFOC",test_piezzo,NULL,0,NULL);

  return mn;
}

/** Main function for file \a pifoc.c

\author JF Allemand
\author Adrien Meglio

\version 21/05/07
*/
int pifoc_main(void)
{
  PIEZZO_TYPE = PIEZZO_250;
  
  if (updating_menu_state != 0) return D_O_K;
  
  init_CAN(); 
  //init_CNA();
   
  /* Initialize piezzo at startup */
  convert__microns_to_unsigned_points(150.0,&GLOBAL__consigne__p_in_points);
  //win_report("Initialisation piezzo : %d points",GLOBAL__consigne__p_in_points);
  set_piezzo(GLOBAL__consigne__p_in_points);

  //win_report("Reading pifoc");

  IO_read(CANAL_PIEZZO_READ);
  IO_write(CANAL_PIEZZO_WRITE,10000);
  IO_read(CANAL_PIEZZO_READ);
  
  add_plot_treat_menu_item("PIFOC menu", NULL, pifoc_menu(), 0, NULL);
  add_image_treat_menu_item("PIFOC menu", NULL, pifoc_menu(), 0, NULL);
  
  return 0;    
}

#endif









