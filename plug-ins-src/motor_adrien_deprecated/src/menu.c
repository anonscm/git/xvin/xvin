#ifndef menu_C_
#define menu_C_

#include <allegro.h>
#include <winalleg.h>
#include <xvin.h>
#include <windows.h>
#include <stdio.h>

#include "motor_define.h"
#include "motor_variable.h"
#include "motor_function.h"

#include "nidaqcns.h"
#include "nidaq.h"

/**********************TRADUCTION EN BOUTONS **************************/

int new_set_P_1(void)
{       
  static int new_P_1;
          
  if(updating_menu_state != 0)	return D_O_K;	
  win_scanf("Proportional factor motor 1 %d ?",&new_P_1);
  new_P_motor_1((unsigned char)new_P_1);

  return 0;
}

int new_set_P_2(void)
{       
  static int new_P_2;
        
  if(updating_menu_state != 0)	return D_O_K;	
  win_scanf("Proportional factor motor 2 %d ?",&new_P_2);
  new_P_motor_2((unsigned char)new_P_2);

  return 0;
}


int new_set_I_1(void)
{
  static int new_I_1;
  	    
  if(updating_menu_state != 0)	return D_O_K;	
  win_scanf("Integral factor motor 1 %d ?",&new_I_1);
  new_I_motor_1((unsigned char)new_I_1);
  
  return 0;
}

int new_set_I_2(void)
{ 
  static int new_I_2;
           
  if(updating_menu_state != 0)	return D_O_K;	
  win_scanf("Integral factor motor 2 %d ?",&new_I_2);
  new_I_motor_2((unsigned char)new_I_2);
  
  return 0;
}

int new_set_D_1(void)
{ 
  static int new_D_1;
        
  if(updating_menu_state != 0)	return D_O_K;	
  win_scanf("Derivative factor motor 1 %d ?",&new_D_1);
  new_D_motor_1((unsigned char)new_D_1);
  
  return 0;
}

int new_set_D_2(void)
{ 
  static int new_D_2;
         
  if(updating_menu_state != 0)	return D_O_K;	
  win_scanf("Derivative factor motor 2 %d ?",&new_D_2);
  new_D_motor_2((unsigned char)new_D_2);
  
  return 0;
}

int new_set_ratio_motor_1(void)
{ 
  static int new_R_1;
        
  if(updating_menu_state != 0)	return D_O_K;	
  win_scanf("Ratio factor motor 1 %d ?",&new_R_1);
  new_R_motor_1((unsigned char)new_R_1);
  
  return 0;
}

int new_set_ratio_motor_2(void)
{ 
  static int new_R_2;
  	    
  if(updating_menu_state != 0)	return D_O_K;	
  win_scanf("Ratio factor motor 2 %d ?",&new_R_2);
  new_R_motor_2((unsigned char)new_R_2);
  
  return 0;
}

int new_reset_motor_1(void)
{
  if(updating_menu_state != 0)	return D_O_K;	
  new_res_motor_1();
  
  return 0;
}

int new_reset_motor_2(void)
{
  if(updating_menu_state != 0)	return D_O_K;	
  new_res_motor_2();

  return 0;
}	

int set_cons_max_speed_pos_1(void)
{
  static int v_max=0;

  if(updating_menu_state != 0)	return D_O_K;	
      
  win_scanf("Max Speed %d ?",&v_max);
  set_speed_feedback_1((unsigned char)v_max);

  return 0;
}

int set_cons_max_speed_pos_2(void)
{ 
  static int v_max=0;

  if(updating_menu_state != 0)	return D_O_K;	
      
  win_scanf("Max Speed %d ?",&v_max);
  set_speed_feedback_2((unsigned char)v_max);

  return 0;
}
	
int Read_pos_mot_1(void)
{	
  if(updating_menu_state != 0)	return D_O_K;		
  win_printf("Pos Mot 1 %ld",read_pos_motor_1());
	
  return 0;
}

int Read_pos_mot_2(void)
{	
  if(updating_menu_state != 0)	return D_O_K;		
  win_printf("Pos Mot 2 %ld",read_pos_motor_2());
	
  return 0;
}

int set_cons_pos_mot_1(void)
{
  static  long cons1=0;
		
  if(updating_menu_state != 0)	return D_O_K;	
  win_scanf("Position %d ?",&cons1);
  set_consigne_pos_moteur_1(cons1);
	
  return 0;
}
int set_cons_pos_mot_2(void)
{
  static  long  cons2=0;
		
  if(updating_menu_state != 0)	return D_O_K;	
  win_scanf("Position %d ?",&cons2);
  set_consigne_pos_moteur_2(cons2);
	
  return 0;
}

int set_vitesse_mot_1(void)
{	 
  static int vmax=0;
  
  if(updating_menu_state != 0)	return D_O_K;	
  win_scanf("Speed %d ?(0-255)",&vmax);
  set_Vmax_moteur_1((unsigned char)vmax);
  
  return 0;
}

int set_vitesse_mot_2(void)
{
  static int vmax=0;
     	 
  if(updating_menu_state != 0)	return D_O_K;	
  win_scanf("Speed %d ?",&vmax);
  set_Vmax_moteur_2((unsigned char)vmax);
  
  return 0;
}

int set_acc_pos_1(void)
{ 
  static int acc=0;
  
  if(updating_menu_state != 0)	return D_O_K;	
  win_scanf("Acceleration position feedback %d ?",&acc);
  set_acc_pos_feedback_1((unsigned char)acc);
  
  return 0;
}

int set_acc_pos_2(void)
{  
  static int acc=0;
	 
  if(updating_menu_state != 0)	return D_O_K;	
  win_scanf("Acceleration position feedback %d ?",&acc);
  set_acc_pos_feedback_2((unsigned char)acc);
  
  return 0;
}

int set_cons_pos_lim_1(void)
{  
  static int pos_lim=0;
     
  if(updating_menu_state != 0)	return D_O_K;	
  win_scanf("Pos lim %d ?",&pos_lim);
  set_lim_dec_feedback_pos_1((short int)pos_lim);
	
  return 0;
}

int set_cons_pos_lim_2(void)
{   
  static int pos_lim=0;
     
  if(updating_menu_state != 0)	return D_O_K;	
  win_scanf("Pos lim %d ?",&pos_lim);
  set_lim_dec_feedback_pos_2((short int)pos_lim);
	
  return 0;
}


MENU *motor_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
	
  add_item_to_menu(mn,"Read_pos_mot_1",Read_pos_mot_1,NULL,0,NULL);
  add_item_to_menu(mn,"set_cons_pos_mot_1",set_cons_pos_mot_1,NULL,0,NULL);
  add_item_to_menu(mn,"Reset motor 1",new_reset_motor_1,NULL,0,NULL);
  add_item_to_menu(mn,"set_cons_max_speed_pos_1",set_cons_max_speed_pos_1,NULL,0,NULL);
  add_item_to_menu(mn,"set_acc_pos_1",set_acc_pos_1,NULL,0,NULL);
  add_item_to_menu(mn,"set_cons_pos_lim_1",set_cons_pos_lim_1,NULL,0,NULL);
  add_item_to_menu(mn,"P motor 1",new_set_P_1,NULL,0,NULL);
  add_item_to_menu(mn,"I motor 1",new_set_I_1,NULL,0,NULL);
  add_item_to_menu(mn,"D motor 1",new_set_D_1,NULL,0,NULL);
  add_item_to_menu(mn,"Ratio motor 1",new_set_ratio_motor_1,NULL,0,NULL);
  add_item_to_menu(mn,"set_vitesse_mot_1",set_vitesse_mot_1,NULL,0,NULL);
  add_item_to_menu(mn,"Read_pos_mot_2",Read_pos_mot_2,NULL,0,NULL);
  add_item_to_menu(mn,"set_cons_pos_mot_2",set_cons_pos_mot_2,NULL,0,NULL);
  add_item_to_menu(mn,"Reset motor 2",new_reset_motor_2,NULL,0,NULL);
  add_item_to_menu(mn,"set_cons_max_speed_pos_2",set_cons_max_speed_pos_2,NULL,0,NULL);
  add_item_to_menu(mn,"set_acc_pos_2",set_acc_pos_2,NULL,0,NULL);
  add_item_to_menu(mn,"set_cons_pos_lim_2",set_cons_pos_lim_2,NULL,0,NULL);
  add_item_to_menu(mn,"P motor 2",new_set_P_2,NULL,0,NULL);
  add_item_to_menu(mn,"I motor 2",new_set_I_2,NULL,0,NULL);
  add_item_to_menu(mn,"D motor 2",new_set_D_2,NULL,0,NULL);
  add_item_to_menu(mn,"Ratio motor 2",new_set_ratio_motor_2,NULL,0,NULL);
  add_item_to_menu(mn,"set_vitesse_mot_2",set_vitesse_mot_2,NULL,0,NULL);
  add_item_to_menu(mn,"read_card_type",read_card_type,NULL,0,NULL);
  add_item_to_menu(mn,"do_initialisation",init_motors ,NULL,0,NULL);
	
  return mn;
}

int  menu_main(void)
{
  if(updating_menu_state != 0) return D_O_K;
    
  add_image_treat_menu_item("Motor functions",NULL,motor_menu(),0,NULL);  
  add_plot_treat_menu_item("Motor functions",NULL,motor_menu(),0,NULL);  

  return D_O_K;
}

#endif

