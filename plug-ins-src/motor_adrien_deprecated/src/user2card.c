#ifndef user2card_C 
#define user2card_C

#include <allegro.h>
#include <winalleg.h>
#include <xvin.h>
#include <windows.h>
#include <stdio.h>

#include "motor_define.h"
#include "motor_variable.h"
#include "motor_function.h"

#include "nidaqcns.h"
#include <NIDAQmx.h>
#include "nidaq.h"

#define DAQmxErrChk(functionCall) if( DAQmxFailed(error=(functionCall)) ) goto Error; else

/************************CARD INITIALIZATION (LOW-LEVEL)*************************/

int detect_NI_boards(void)
{	
  short int infovalue;
  int i;
  int status;
  
  if(updating_menu_state != 0)	return D_O_K;	
  for (i=0;i<3;i++)
    {
      status = Init_DA_Brds (i,&infovalue);
      //win_printf("Init board number %d",infovalue);
      if (infovalue==PCI_6503)
	{
	  NI_6503=i;
	  //win_printf(" Board %d is DAQ6503",i);
	}
      else if (infovalue==PCI_6602)
	{
	  NI_6602=i;
	  //win_printf(" Board %d is Counter 6602",i);
	}
      
      else 
	{ 
	  //win_printf("No board number %d",i);
	  //NI_6602 = 1;
	}
    }
  if (NI_6503 == 0) 
    {
      win_printf("No motor available");
      return 0;
    }
  /*status = Get_DAQ_Device_Info (NI_6602, ND_DEVICE_TYPE_CODE		, &infoValue);
    win_printf("Counter on board=%d (ND_AM9513 ?)",infovalue);*/
  
  return 0;
}

int  read_card_type(void)
{        
  unsigned char card_type[4];
  unsigned char i;
  
  if(updating_menu_state != 0)	return D_O_K;	
  
  for (i=0;i<4;i++)
    {
      card_type[i]=read_data_from_controller(CARD_TYPE,CARD_ADRESS+i);
      win_printf ("card type %0x i=%0x ",card_type[i],i);
      if (card_type[0]!=0x02) win_printf("T'as un souci ! Je ne suis pas qui tu crois");
      
    }
  return (int)card_type[0];
}

/************************MOTOR INTERFACE(LOW-LEVEL)*************************/
int send_command_to_controller (unsigned char commande,unsigned char adr,unsigned char data)
{
  char n=0;
  short int hands= 0;
  /*char *value=NULL;*/
  int status;
  
  status= DIG_Prt_Config(NI_6503,PORT_A,NO_HANDSHAKING,OUT_SIGNAL);

  if (adr < 4)
    {
   
      status = DIG_Out_Prt (NI_6503,PORT_A,data);
   		
      status = DIG_Out_Prt (NI_6503,PORT_B,commande+adr);
      while ((hands!=1)& (n < 100)) 
      	{
	  n = n + 1;
	  status = DIG_Prt_Status (NI_6503,PORT_B,&hands);
        }
      if (hands == 1) 
        {
	  status = DIG_Out_Prt (NI_6503,PORT_B,0xFF);/**1 donc on �crit sur B pour avoir un accuse de reception je crois**/
	  hands = 0;
	  n=0;
	  while ((hands != 1) & (n < 100))
	    {
	      n = n + 1;
	      status = DIG_Prt_Status (NI_6503,PORT_B,&hands);
	    }
        }
    } 
        
  status = DIG_Prt_Config(NI_6503,PORT_A,NO_HANDSHAKING,IN_SIGNAL);/***on remet la ligne en etat***/
   
  return 0;
}

unsigned char read_data_from_controller (unsigned char commande,unsigned char  adr)
{
  int n=0;

  long int value;
  int status;
  short int hands=0;
        
  if (adr < 4) 
    {
      status = DIG_Out_Prt (NI_6503,PORT_B,commande+adr);
      while ((hands != 1) & (n < 1000))
	{
	  n = n + 1;
	  status = DIG_Prt_Status (NI_6503,PORT_B,&hands);
	}
      if (hands == 1)
	{
	  status = DIG_In_Prt (NI_6503,PORT_A, &value);
	  status = DIG_Out_Prt (NI_6503,PORT_B,0xFF);
	}
      status = DIG_Prt_Config(NI_6503,PORT_B,HANDSHAKING,OUT_SIGNAL);
    }

  return  value;
}


/*********************************PIFOC WRITE INTERFACE(LOW-LEVEL)**************************************/
int IO_write_future(unsigned char canal, short int data)
{
  long int voie;

  TaskHandle taskHandle_A=0;
  TaskHandle taskHandle_B=0;
  TaskHandle taskHandle_C_6=0;

  uInt16 write_data_port;
  uInt8 write_data_line[8]={0,0,0,0,0,0,0,0};

  int32 error=0;
  char errBuff[2048]={'\0'};
  int32 written;
  
  voie=canal*64;

  /* Create the task */
  DAQmxErrChk (DAQmxCreateTask("",&taskHandle_A));
  DAQmxErrChk (DAQmxCreateTask("",&taskHandle_B));
  DAQmxErrChk (DAQmxCreateTask("",&taskHandle_C_6));

  /* Associates ports to the task */ 
  
  /* Ports NI6503/0, 1 and 2 will be named "PORT_A_AD" (resp. B) and will be used as output ports */ 
  DAQmxErrChk (DAQmxCreateDOChan(taskHandle_A,"Dev2/port0","PORT_A_AD",DAQmx_Val_ChanForAllLines)); 
  DAQmxErrChk (DAQmxCreateDOChan(taskHandle_B,"Dev2/port1","PORT_B_AD",DAQmx_Val_ChanForAllLines)); 
  /* Line NI6503/PORT_C/Line_6 will be named 'PORT_C_AD_LINE_6" and will be used as an output line */ 
  DAQmxErrChk (DAQmxCreateDOChan(taskHandle_C_6,"Dev2/port2/line6","PORT_C_AD_LINE_6",DAQmx_Val_ChanForAllLines)); 
  
  /* DAQmx Read Code */

  /* Write on port A */ 
  DAQmxErrChk (DAQmxStartTask(taskHandle_A));
  write_data_port = 0xFF&data;
  DAQmxErrChk (DAQmxWriteDigitalU16(taskHandle_A,1,1,10.0,DAQmx_Val_GroupByChannel,&write_data_port,&written,NULL));
  DAQmxStopTask(taskHandle_A);

  /* Write on port B */ 
  DAQmxErrChk (DAQmxStartTask(taskHandle_B));
  write_data_port = 63+voie;
  DAQmxErrChk (DAQmxWriteDigitalU16(taskHandle_B,1,1,10.0,DAQmx_Val_GroupByChannel,&write_data_port,&written,NULL));
  DAQmxStopTask(taskHandle_B);

  /* Write on line C/6 */ 
  DAQmxErrChk (DAQmxStartTask(taskHandle_C_6));
  write_data_line[7] = 1;
  DAQmxErrChk (DAQmxWriteDigitalLines(taskHandle_C_6,1,1,10.0,DAQmx_Val_GroupByChannel,write_data_line,NULL,NULL));
  DAQmxStopTask(taskHandle_C_6);

  /* Write on port B */ 
  DAQmxErrChk (DAQmxStartTask(taskHandle_B));
  write_data_port = 9+voie;
  DAQmxErrChk (DAQmxWriteDigitalU16(taskHandle_B,1,1,10.0,DAQmx_Val_GroupByChannel,&write_data_port,&written,NULL));
  write_data_port = 12+voie;
  DAQmxErrChk (DAQmxWriteDigitalU16(taskHandle_B,1,1,10.0,DAQmx_Val_GroupByChannel,&write_data_port,&written,NULL));
  DAQmxStopTask(taskHandle_B);

  /* Write on port A */ 
  DAQmxErrChk (DAQmxStartTask(taskHandle_A));
  write_data_port = ((0xFF00&data)>>8);
  DAQmxErrChk (DAQmxWriteDigitalU16(taskHandle_A,1,1,10.0,DAQmx_Val_GroupByChannel,&write_data_port,&written,NULL));
  DAQmxStopTask(taskHandle_B);

  /* Write on port B */ 
  DAQmxErrChk (DAQmxStartTask(taskHandle_B));
  write_data_port = 5+voie;
  DAQmxErrChk (DAQmxWriteDigitalU16(taskHandle_B,1,1,10.0,DAQmx_Val_GroupByChannel,&write_data_port,&written,NULL));
  DAQmxStopTask(taskHandle_B);

  /* Write on line C/6 */ 
  DAQmxErrChk (DAQmxStartTask(taskHandle_C_6));
  write_data_line[7] = 0;
  DAQmxErrChk (DAQmxWriteDigitalLines(taskHandle_C_6,1,1,10.0,DAQmx_Val_GroupByChannel,write_data_line,NULL,NULL));
  DAQmxStopTask(taskHandle_C_6);

  //DIG_Prt_Config (NI_6503,PORT_A_AD,NO_HANDSHAKING,OUT_SIGNAL);
  //DIG_Prt_Config (NI_6503,PORT_B_AD,NO_HANDSHAKING,OUT_SIGNAL);
  //DIG_Prt_Config (NI_6503,PORT_C_AD,NO_HANDSHAKING,OUT_SIGNAL);
  //DIG_Out_Prt (NI_6503,PORT_A_AD,(0xFF&data));
  //DIG_Out_Prt (NI_6503,PORT_B_AD,63+voie);
  //DIG_Out_Line (NI_6503,PORT_C_AD,6,1);
  //DIG_Out_Prt (NI_6503,PORT_B_AD,9+voie);
  //DIG_Out_Prt (NI_6503,PORT_B_AD,12+voie);
  //DIG_Out_Prt (NI_6503,PORT_A_AD,((0xFF00&data)>>8));
  //DIG_Out_Prt (NI_6503,PORT_B_AD,5+voie);
  //DIG_Out_Line (NI_6503,PORT_C_AD,6,0);
  // ?? DIG_Prt_Config (NI_6503,PORT_A_AD,NO_HANDSHAKING,IN_SIGNAL);
  
  Error:
  if( DAQmxFailed(error) )
    DAQmxGetExtendedErrorInfo(errBuff,2048);
  if( (taskHandle_A!=0) || (taskHandle_B!=0) || (taskHandle_C_6!=0) ) {
    /*********************************************/
    // DAQmx Stop Code
    /*********************************************/
    DAQmxStopTask(taskHandle_A);
    DAQmxStopTask(taskHandle_B);
    DAQmxStopTask(taskHandle_C_6);
    
    DAQmxClearTask(taskHandle_A);
    DAQmxClearTask(taskHandle_B);
    DAQmxClearTask(taskHandle_C_6);
  }
  if( DAQmxFailed(error) )
    win_report("DAQmx Error: %s\n",errBuff);
  return 0;
}

int IO_write(unsigned char canal, short int data)
{
  unsigned char voie;
  
  voie=canal*64;
  DIG_Prt_Config (NI_6503,PORT_A_AD,NO_HANDSHAKING,OUT_SIGNAL);
  DIG_Prt_Config (NI_6503,PORT_B_AD,NO_HANDSHAKING,OUT_SIGNAL);
  DIG_Prt_Config (NI_6503,PORT_C_AD,NO_HANDSHAKING,OUT_SIGNAL);
  DIG_Out_Prt (NI_6503,PORT_A_AD,(0xFF&data));
  DIG_Out_Prt (NI_6503,PORT_B_AD,63+voie);
  DIG_Out_Line (NI_6503,PORT_C_AD,6,1);
  DIG_Out_Prt (NI_6503,PORT_B_AD,9+voie);
  DIG_Out_Prt (NI_6503,PORT_B_AD,12+voie);
  DIG_Out_Prt (NI_6503,PORT_A_AD,((0xFF00&data)>>8));
  DIG_Out_Prt (NI_6503,PORT_B_AD,5+voie);
  DIG_Out_Line (NI_6503,PORT_C_AD,6,0);
  DIG_Prt_Config (NI_6503,PORT_A_AD,NO_HANDSHAKING,IN_SIGNAL);
  
  return 0;
}

/********************************PIFOC READ INTERFACE(LOW-LEVEL)***************************/
int IO_read(unsigned char canal)
{
  long int voie;
  int i;
  unsigned long mesure=0;
  unsigned long read_1=0;
  unsigned long read_2=0;
    
  voie = (canal-1) * 2;
  
  for (i=0;i<2;i++)/* we need to read twice!!!!!!*/
    {
      DIG_Prt_Config (NI_6503,PORT_A_AD,NO_HANDSHAKING,IN_SIGNAL);
      DIG_Prt_Config (NI_6503,PORT_B_AD,NO_HANDSHAKING,OUT_SIGNAL);
      DIG_Prt_Config (NI_6503,PORT_C_AD,NO_HANDSHAKING,OUT_SIGNAL);
      
      DIG_Out_Prt (NI_6503,PORT_B_AD,voie+57);
      DIG_Out_Line (NI_6503,PORT_C_AD,6,1);
      DIG_Out_Prt (NI_6503,PORT_B_AD,voie+0);
      DIG_Out_Prt (NI_6503,PORT_B_AD,voie+8);
      DIG_In_Prt (NI_6503,PORT_A_AD,&read_1);
      
      DIG_Out_Prt (NI_6503,PORT_B_AD,voie+9);
      DIG_In_Prt (NI_6503,PORT_A_AD,&read_2);
      
      DIG_Out_Prt (NI_6503,PORT_B_AD,voie+57);
      DIG_Out_Line (NI_6503,PORT_C_AD,6,0);
      DIG_Prt_Config (NI_6503,PORT_B_AD,HANDSHAKING,OUT_SIGNAL);
    }
  
  mesure = ((((0xff&read_1)<<8)+(0xff&read_2)+32768) & 0xffff);
  //draw_bubble(screen,0,750,50,"canal %d read 1 %d  read 2 %d  mesure %d ",(int) canal, read_1, read_2,mesure);
  
  return (unsigned int)mesure;
}

int IO_read_future(unsigned char canal)
{
  long int voie;
  int i;

  TaskHandle taskHandle_A=0;
  TaskHandle taskHandle_B=0;
  TaskHandle taskHandle_C_6=0;

  uInt16 write_data_port;
  uInt16 read_data_port1, read_data_port2;
  uInt8 write_data_line[8]={0,0,0,0,0,0,0,0};

  int32 error=0;
  char errBuff[2048]={'\0'};
  int32 written;
  int32 read;

  voie = (canal-1) * 2;

  /* Create the task */
  DAQmxErrChk (DAQmxCreateTask("",&taskHandle_A));
  DAQmxErrChk (DAQmxCreateTask("",&taskHandle_B));
  DAQmxErrChk (DAQmxCreateTask("",&taskHandle_C_6));

  /* Associates ports to the task */ 
  
  /* Port NI6503/0 will be named "PORT_A_AD" and will be used as an input port */
  DAQmxErrChk (DAQmxCreateDIChan(taskHandle_A,"Dev2/port0","PORT_A_AD",DAQmx_Val_ChanForAllLines)); 
  /* Port NI6503/1 will be named "PORT_B_AD" and will be used as an output port */ 
  DAQmxErrChk (DAQmxCreateDOChan(taskHandle_B,"Dev2/port1","PORT_B_AD",DAQmx_Val_ChanForAllLines)); 
  /* Line NI6503/2/Line_6 will be named 'PORT_C_AD_LINE_6" and will be used as an output line */ 
  DAQmxErrChk (DAQmxCreateDOChan(taskHandle_C_6,"Dev2/port2/line6","PORT_C_AD_LINE_6",DAQmx_Val_ChanForAllLines)); 
  
  /* DAQmx Read Code */
  /* The card is read twice */
  for (i = 0 ; i < 2 ; i++)
    {
      /* Write on port B */ 
      DAQmxErrChk (DAQmxStartTask(taskHandle_B));
      write_data_port = voie+57;
      DAQmxErrChk (DAQmxWriteDigitalU16(taskHandle_B,1,1,10.0,DAQmx_Val_GroupByChannel,&write_data_port,&written,NULL));
      DAQmxStopTask(taskHandle_B);

      /* Write on line C/6 */ 
      DAQmxErrChk (DAQmxStartTask(taskHandle_C_6));
      write_data_line[7] = 1;
      DAQmxErrChk (DAQmxWriteDigitalLines(taskHandle_C_6,1,1,10.0,DAQmx_Val_GroupByChannel,write_data_line,NULL,NULL));
      DAQmxStopTask(taskHandle_C_6);

      /* Write on port B */ 
      DAQmxErrChk (DAQmxStartTask(taskHandle_B));
      write_data_port = voie+0;
      DAQmxErrChk (DAQmxWriteDigitalU16(taskHandle_B,1,1,10.0,DAQmx_Val_GroupByChannel,&write_data_port,&written,NULL));
      write_data_port = voie+8;
      DAQmxErrChk (DAQmxWriteDigitalU16(taskHandle_B,1,1,10.0,DAQmx_Val_GroupByChannel,&write_data_port,&written,NULL));
      DAQmxStopTask(taskHandle_B);

      /* Read on port A */ 
      DAQmxErrChk (DAQmxStartTask(taskHandle_A));
      DAQmxErrChk (DAQmxReadDigitalU16(taskHandle_A,1,10.0,DAQmx_Val_GroupByChannel,&read_data_port1,1,&read,NULL));
      DAQmxStopTask(taskHandle_A);

      /* Write on port B */ 
      DAQmxErrChk (DAQmxStartTask(taskHandle_B));
      write_data_port = voie+9;
      DAQmxErrChk (DAQmxWriteDigitalU16(taskHandle_B,1,1,10.0,DAQmx_Val_GroupByChannel,&write_data_port,&written,NULL));
      DAQmxStopTask(taskHandle_B);

      /* Read on port A */ 
      DAQmxErrChk (DAQmxStartTask(taskHandle_A));
      DAQmxErrChk (DAQmxReadDigitalU16(taskHandle_A,1,10.0,DAQmx_Val_GroupByChannel,&read_data_port2,1,&read,NULL));
      DAQmxStopTask(taskHandle_A);

      /* Write on port B */ 
      DAQmxErrChk (DAQmxStartTask(taskHandle_B));
      write_data_port = voie+57;
      DAQmxErrChk (DAQmxWriteDigitalU16(taskHandle_B,1,1,10.0,DAQmx_Val_GroupByChannel,&write_data_port,&written,NULL));
      DAQmxStopTask(taskHandle_B);

      /* Write on line C/6 */ 
      DAQmxErrChk (DAQmxStartTask(taskHandle_C_6));
      write_data_line[7] = 0;
      DAQmxErrChk (DAQmxWriteDigitalLines(taskHandle_C_6,1,1,10.0,DAQmx_Val_GroupByChannel,write_data_line,NULL,NULL));
      DAQmxStopTask(taskHandle_C_6);
    }
	
  win_report("Mesure %d",((((0xff&read_data_port1)<<8)+(0xff&read_data_port2)+32768) & 0xffff));

 Error:
  if( DAQmxFailed(error) )
    DAQmxGetExtendedErrorInfo(errBuff,2048);
  if( (taskHandle_A!=0) || (taskHandle_B!=0) || (taskHandle_C_6!=0) ) {
    /*********************************************/
    // DAQmx Stop Code
    /*********************************************/
    DAQmxStopTask(taskHandle_A);
    DAQmxStopTask(taskHandle_B);
    DAQmxStopTask(taskHandle_C_6);
    
    DAQmxClearTask(taskHandle_A);
    DAQmxClearTask(taskHandle_B);
    DAQmxClearTask(taskHandle_C_6);
  }
  if( DAQmxFailed(error) )
    win_report("DAQmx Error: %s\n",errBuff);
  return 0;
}


/****************************CONVERTERS INITIALIZATION (LOW-LEVEL)********************************/
int init_CNA(void)
{
  if(updating_menu_state != 0)	return D_O_K;	
  DIG_Prt_Config (NI_6503,PORT_A_AD,NO_HANDSHAKING,IN_SIGNAL);
  DIG_Prt_Config (NI_6503,PORT_B_AD,NO_HANDSHAKING,OUT_SIGNAL);
  DIG_Prt_Config (NI_6503,PORT_C_AD,NO_HANDSHAKING,OUT_SIGNAL);
  DIG_Out_Prt (NI_6503,PORT_C_AD,0xFF);
  IO_write(CNA_1,0);
  IO_write(CNA_2,0);
  IO_write(CNA_3,0);
  IO_read(CAN_1);
  IO_read(CAN_1);
  /*win_printf("CNA_1 %d CNA_2 %d CNA_3 %d CAN_1%d",CNA_1,CNA_2,CNA_3,CAN_1);*/  
  return 0;
}

int init_CAN_future(void)
{	
  TaskHandle taskHandle_C=0;
  uInt16 write_data_port;
  int32 error=0;
  char errBuff[2048]={'\0'};
  int32 written;

  if(updating_menu_state != 0)	return D_O_K;	

  /* Create the task */
  DAQmxErrChk (DAQmxCreateTask("",&taskHandle_C));

  /* Associates ports to the task */ 

  /* Port NI6503/2 will be named "PORT_C_AD" and will be used as an output port */ 
  DAQmxErrChk (DAQmxCreateDOChan(taskHandle_C,"Dev2/port2","PORT_C_AD",DAQmx_Val_ChanForAllLines)); 

  /* Write on port B */ 
  DAQmxErrChk (DAQmxStartTask(taskHandle_C));
  write_data_port = 0xFF;
  DAQmxErrChk (DAQmxWriteDigitalU16(taskHandle_C,1,1,10.0,DAQmx_Val_GroupByChannel,&write_data_port,&written,NULL));
  DAQmxStopTask(taskHandle_C);

 Error:
  if( DAQmxFailed(error) )
    DAQmxGetExtendedErrorInfo(errBuff,2048);
  if( (taskHandle_C!=0) ) {
    /*********************************************/
    // DAQmx Stop Code
    /*********************************************/
    DAQmxStopTask(taskHandle_C);
    DAQmxClearTask(taskHandle_C);
  }
  if( DAQmxFailed(error) ) win_report("DAQmx Error: %s\n",errBuff);

  //DIG_Prt_Config (NI_6503,PORT_A_AD,NO_HANDSHAKING,IN_SIGNAL);
  //DIG_Prt_Config (NI_6503,PORT_B_AD,NO_HANDSHAKING,OUT_SIGNAL);
  //DIG_Prt_Config (NI_6503,PORT_C_AD,NO_HANDSHAKING,OUT_SIGNAL);
  //DIG_Out_Prt (NI_6503,PORT_C_AD,0xFF);

  IO_write (CNA_1, 0);
  IO_write (CNA_2, 0);
  IO_write (CNA_3, 0);
  IO_read (CAN_1);
  IO_read (CAN_1);
  
  return 0;
}

int init_CAN(void)
{	
  if(updating_menu_state != 0)	return D_O_K;	
  DIG_Prt_Config (NI_6503,PORT_A_AD,NO_HANDSHAKING,IN_SIGNAL);
  DIG_Prt_Config (NI_6503,PORT_B_AD,NO_HANDSHAKING,OUT_SIGNAL);
  DIG_Prt_Config (NI_6503,PORT_C_AD,NO_HANDSHAKING,OUT_SIGNAL);
  DIG_Out_Prt (NI_6503,PORT_C_AD,0xFF);
  IO_write (CNA_1, 0);
  IO_write (CNA_2, 0);
  IO_write (CNA_3, 0);
  IO_read (CAN_1);
  IO_read (CAN_1);
  
  return 0;
}

#endif

