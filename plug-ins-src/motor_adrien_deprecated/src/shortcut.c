/** \file shortcut.c
    
    These functions allow shortcut control through keyboard.
    
    \author Adrien Meglio
    
    \version 24/04/07
*/
#ifndef shortcut_C_
#define shortcut_C_

#include <allegro.h>
#include <winalleg.h>
#include <xvin.h>
#include <windows.h>
#include <stdio.h>

#include "motor_define.h"
#include "motor_variable.h"
#include "motor_function.h"

#include "nidaqcns.h"
#include "nidaq.h"

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
// Shortcut functions

int MOTOR_P__move(float amount_in_mum)
{
  unsigned short int position_P;
  short int move_in_points;

  if(updating_menu_state != 0) return D_O_K;

  //position_P = read_piezzo();
  position_P = GLOBAL__consigne__p_in_points;

  convert__microns_to_signed_points(-amount_in_mum,&move_in_points); /* The motor is mounted upside-down */

  position_P += move_in_points; 

  //win_report("Pifoc at position %d will be moved by %d points to %d",GLOBAL__consigne__p_in_points,move_in_points,position_P);

  set_piezzo(position_P);

  return D_O_K;
}

/* int MOTOR_P__up(float amount_in_mum) */
/* { */
/*   float position_P; */

/*   if(updating_menu_state != 0) return D_O_K; */

/*   position_P = read_piezzo_in_micron(); */

/*   position_P += (float) -amount_in_mum; /* The motor is mounted upside-down */ 

/*   set_piezzo_in_micron(position_P); */

/*   return D_O_K; */

/*  } */

/* int MOTOR_P__down(float amount_in_mum) */
/* { */
/*   float position_P; */

/*   if(updating_menu_state != 0) return D_O_K; */
  
/*   position_P = read_piezzo_in_micron(); */
  
/*   position_P += (float) amount_in_mum; /* The motor is mounted upside-down */ 
  
/*   set_piezzo_in_micron(position_P); */
  
/*   return D_O_K; */

/*  } */

int MOTOR_T__move(float amount_in_mm)
{
  long position_T;

  if(updating_menu_state != 0) return D_O_K;

  read_pos_motor_1(); /* Need to read twice, if another canal (eg pifoc) has been read/written just before */
  position_T = read_pos_motor_1();

  position_T += (long) (-amount_in_mm*MOTOR_T_POINTS_PER_MM); /* The motor is mounted upside-down */

  set_consigne_pos_moteur_1(position_T);

  return D_O_K; 
}

/* int MOTOR_T__up(float amount_in_mm) */
/* { */
/*   long position_T; */

/*   if(updating_menu_state != 0) return D_O_K; */

/*   position_T = read_pos_motor_1(); */

/*   position_T += (long) -amount_in_mm*MOTOR_T_POINTS_PER_MM; /* The motor is mounted upside-down */ 

/*   set_consigne_pos_moteur_1(position_T); */

/*   return D_O_K; */

/*  } */

/* int MOTOR_T__down(float amount_in_mm) */
/* { */
/*  long position_T; */

/*   if(updating_menu_state != 0) return D_O_K; */

/*   position_T = read_pos_motor_1(); */

/*   position_T += (long) amount_in_mm*MOTOR_T_POINTS_PER_MM; /* The motor is mounted upside-down */ 

/*   set_consigne_pos_moteur_1(position_T); */

/*   return D_O_K; */

/* }*/

int MOTOR_R__move(float amount_in_turns)
{
  long position_R;

  if(updating_menu_state != 0) return D_O_K;

  read_pos_motor_2(); /* Need to read twice, if another canal (eg pifoc) has been read/written just before */
  position_R = read_pos_motor_2();

  position_R += (long) (amount_in_turns*MOTOR_R_POINTS_PER_TURN);

  set_consigne_pos_moteur_2(position_R);

  return D_O_K;
 }

/* int MOTOR_R__up(float amount_in_turns) */
/* { */
/*   long position_R; */

/*   if(updating_menu_state != 0) return D_O_K; */

/*   position_R = read_pos_motor_2(); */

/*   position_R += (long) amount_in_turns*MOTOR_R_POINTS_PER_TURN; */

/*   set_consigne_pos_moteur_2(position_R); */

/*   return D_O_K; */

/*  } */

/* int MOTOR_R__down(float amount_in_turns) */
/* { */
/*   long position_R; */

/*   if(updating_menu_state != 0) return D_O_K; */

/*   position_R = read_pos_motor_2(); */

/*   position_R += (long) -amount_in_turns*MOTOR_R_POINTS_PER_TURN; */

/*   set_consigne_pos_moteur_2(position_R); */

/*   return D_O_K; */

/*  } */

int MOTOR_P__set(void)
{
  float consigne_float = 0;    
  unsigned short int consigne_points;
  
  if(updating_menu_state != 0) return D_O_K;
  
  if(win_scanf("Position du PIFOC : %f �m",&consigne_float) == CANCEL) return D_O_K;
  
  convert__microns_to_unsigned_points(consigne_float,&consigne_points);

  set_piezzo(consigne_points);

 return D_O_K;
}

int MOTOR_R__set(void)
{	
  float consigne=0;
	 	
  if(updating_menu_state != 0)	return D_O_K;	
	
  if (win_scanf("Position : %f (tours) ?",&consigne) == CANCEL) return D_O_K;

  set_consigne_pos_moteur_2((long) consigne*MOTOR_R_POINTS_PER_TURN);

  return D_O_K;
}

int MOTOR_T__set(void)
{	
 float consigne=0;
	 	
  if(updating_menu_state != 0)	return D_O_K;	
	
  if (win_scanf("Position : %f (mm) ?",&consigne) == CANCEL) return D_O_K;

  set_consigne_pos_moteur_1((long) consigne*MOTOR_T_POINTS_PER_MM);

  return D_O_K;
}

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
// Base functions

/** Main function of shortcuts capabilities

\author Adrien Meglio
\version 15/11/06
*/
int  shortcuts_main(void)
{
    if(updating_menu_state != 0) return D_O_K;
    
    return D_O_K;
}

#endif
