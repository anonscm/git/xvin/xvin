/**
\file main.c

\brief Program for driving DC motors with C Herrmann controller
\version 22/05/07
\author Adrien Meglio	
*/				 				
 
#ifndef main_C
#define main_C

#include <allegro.h>
#include <winalleg.h>
#include <xvin.h>
#include <windows.h>
#include <stdio.h>

#include "motor_define.h"
#include "motor_variable.h"
#include "motor_function.h"

#include "nidaqcns.h"
#include "nidaq.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Main functions

int motor_main(void)
{
  if(updating_menu_state != 0)	return D_O_K;	

  //win_report("Motors initialization");
  motorfile_main();

  /* Loads pifoc */
  //win_report("PIFOC initialization");
  pifoc_main();

  /* Loads shortcuts */
  //win_report("Shortcuts initialization");
  shortcuts_main();

  /* Loads menu functions */
  //win_report("Menus initialization");
  menu_main();
    
  //win_report("Motor plugin loaded");

  //broadcast_dialog_message(MSG_DRAW,0);
	
  return 0;    
}

#endif

