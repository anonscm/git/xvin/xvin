#pragma once
PXV_FUNC(int, do_demitour_find_extrema, (void));
PXV_FUNC(int, do_demitour_find_extrema_all_ds, (void));
PXV_FUNC(int, do_demitour_delete_data_set, (void));
PXV_FUNC(MENU*, demitour_plot_menu, (void));
PXV_FUNC(int, do_demitour_rescale_data_set, (void));
PXV_FUNC(int, demitour_main, (int argc, char **argv));
