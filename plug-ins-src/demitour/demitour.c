/*
 *    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
 */
#ifndef _DEMITOUR_C_
#define _DEMITOUR_C_

#include <allegro.h>
# include "xvin.h"

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "demitour.h"


int do_demitour_hello(void)
{
	int i;

	if(updating_menu_state != 0)	return D_O_K;

	i = win_printf("Hello from demitour");
	if (i == WIN_CANCEL) win_printf_OK("you have press WIN_CANCEL");
	else win_printf_OK("you have press OK");
	return 0;
}

int do_demitour_rescale_data_set(void)
{
	int i, j;
	O_p *op = NULL;
	int nf;
	static float factor = 1;
	d_s *ds, *dsi;
	pltreg *pr = NULL;


	if(updating_menu_state != 0)	return D_O_K;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine multiply the y coordinate"
				"of a data set by a number");
	}
	/* we first find the data that we need to transform */
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
		return win_printf_OK("cannot find data");
	nf = dsi->nx;	/* this is the number of points in the data set */
	i = win_scanf("By what factor do you want to multiply y %f",&factor);
	if (i == WIN_CANCEL)	return OFF;

	if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
		return win_printf_OK("cannot create plot !");

	for (j = 0; j < nf; j++)
	{
		ds->yd[j] = factor * dsi->yd[j];
		ds->xd[j] = dsi->xd[j];
	}

	/* now we must do some house keeping */
	inherit_from_ds_to_ds(ds, dsi);
	set_ds_treatement(ds,"y multiplied by %f",factor);
	/* refisplay the entire plot */
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
}

int do_demitour_delete_data_set(void)
{
  int i, j;
  O_p *op = NULL, *opd;
  int nf, n_dat;
  d_s *ds, *dsi, *dsd;
  pltreg *pr = NULL;
  static int lim = 0;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  i = win_scanf("Delete ds with number of points lower than %d",&lim);
  if (i == WIN_CANCEL)	return OFF;

  i = op->n_dat;
  op->n_dat = 0;
  opd = duplicate_plot_data(op, NULL);
  op->n_dat = i;

  if (opd == NULL)
		return (win_printf_OK("can't create plot"));

  add_data_to_pltreg(pr, IS_ONE_PLOT, (void *)opd);
  opd->need_to_refresh = 1;

  n_dat = op->n_dat;

  for (j = 0; j < n_dat; j++)
  {
    ds = op->dat[j];
    nf = ds->nx;

	 if (nf>lim)
    {
	   dsd = create_and_attach_one_ds(opd, nf, nf, 0);

		if(dsd == NULL)
        return win_printf("Cannot allocate plot");


		for(i = 0; i < nf; i++)
		{
		  dsd->xd[i] = ds->xd[i];
		  dsd->yd[i] = ds->yd[i];
		}

		inherit_from_ds_to_ds(dsd, ds);
	}
  }

  /* redisplay the entire plot */
  refresh_plot(pr, pr->n_op - 1);
  return D_O_K;
}

int do_demitour_find_extrema(void)
{
	int i, j;
	O_p *op = NULL, *opn = NULL, *opn2 = NULL;
	int nf;
	static float factor = 1;
	d_s *ds = NULL, *dsi;
	pltreg *pr = NULL;


	if(updating_menu_state != 0)	return D_O_K;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine finds the extrema"
				"of a data set and saves increasing and decreasing phases");
	}
	/* we first find the data that we need to transform */
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
		return win_printf_OK("cannot find data");
	nf = dsi->nx;	/* this is the number of points in the data set */
	/*i = win_scanf("By what factor do you want to multiply y %f",&factor);
	  if (i == WIN_CANCEL)	return OFF;*/

	if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
		return win_printf_OK("cannot create plot !");

	if ((opn2 = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
		return win_printf_OK("cannot create plot !");

	int crois = 0;
	float seuil = op->y_lo;

	for(j = 1; j < nf-1; j++)
	{
		if (dsi->yd[j] > op->y_lo && fabsf(dsi->xd[j+1]-dsi->xd[j])/fabsf(dsi->xd[j]-dsi->xd[j-1]) < (float)2 ) // Tant qu'on est au-dessus du seuil	
		{	
			if (crois == 0)
			{
				if (dsi->yd[j-1] < dsi->yd[j] && dsi->yd[j] < dsi->yd[j+1]) // demarrage croissant
				{
					crois = 1;
					if (ds == NULL) // creation du ds
					{
						if ((ds = build_data_set(16, 16)) == NULL)
							return win_printf_OK("cannot create plot !");
						ds->nx = ds->ny = 0;
					}
				}	
				else if (dsi->yd[j-1] > dsi->yd[j] && dsi->yd[j] > dsi->yd[j+1]) // demarrage decroissant apres une zone croissante
				{
					crois = -1;
					if (ds == NULL) // creation du ds
					{
						if ((ds = build_data_set(16, 16)) == NULL)
							return win_printf_OK("cannot create plot !");
						ds->nx = ds->ny = 0;
					}
				}
				// Si aucun de ces cas n'est realise, on reste a crois = 0 et on attend le prochain point
			}
			if (dsi->yd[j-1] < dsi->yd[j] && dsi->yd[j] < dsi->yd[j+1] && crois == 1) // etait croissant, et le reste
			{
				add_new_point_to_ds(ds, dsi->xd[j], dsi->yd[j]);	
			}

			if (dsi->yd[j-1] > dsi->yd[j] && dsi->yd[j] > dsi->yd[j+1] && crois == -1) // etait décroissant, et le reste
			{
				add_new_point_to_ds(ds, dsi->xd[j], dsi->yd[j]);
			}

			if (dsi->yd[j-1] > dsi->yd[j] && dsi->yd[j] < dsi->yd[j+1] && crois == -1) // minimum local
			{
				add_new_point_to_ds(ds, dsi->xd[j], dsi->yd[j]);
				if (add_one_plot_data(opn, IS_DATA_SET, (void*)ds))
					return win_printf_OK("cannot attach ds !");
				ds = NULL;
				crois = 0;
			}

			if (dsi->yd[j-1] < dsi->yd[j] && dsi->yd[j] > dsi->yd[j+1] && crois == 1) // maximum local
			{
				add_new_point_to_ds(ds, dsi->xd[j], dsi->yd[j]);
				if (add_one_plot_data(opn2, IS_DATA_SET, (void*)ds))
					return win_printf_OK("cannot attach ds !");
				ds = NULL;
				crois = 0;
			}
		}

		else // Si on est en-dessous on réinitialise
		{
			if (ds != NULL)
			{
				add_new_point_to_ds(ds, dsi->xd[j], dsi->yd[j]);
				if (add_one_plot_data(opn, IS_DATA_SET, (void*)ds)) // Ajoute a opn car forcement minimum
					return win_printf_OK("cannot attach ds !");
				ds = NULL;
			}
			crois = 0;
		}
	}


	/* now we must do some house keeping */
	if (opn != NULL)
	{
		set_plot_title(opn, "Minima and decreasing phases");
		if (op->x_title != NULL) set_plot_x_title(opn, op->x_title);
		if (op->y_title != NULL) set_plot_y_title(opn, op->y_title);
		opn->filename = Transfer_filename(op->filename);
		uns_op_2_op(opn, op);
	}

	if (opn2 != NULL)
	{
		set_plot_title(opn2, "Maxima and increasing phases");
		if (op->x_title != NULL) set_plot_x_title(opn2, op->x_title);
		if (op->y_title != NULL) set_plot_y_title(opn2, op->y_title);
		opn2->filename = Transfer_filename(op->filename);
		uns_op_2_op(opn2, op);
	}

	/* refisplay the entire plot */
	refresh_plot(pr, pr->n_op-1);
	return D_O_K;
}

int do_demitour_find_extrema_all_ds(void)
{
	int i, j, k;
	O_p *op = NULL, *opn = NULL, *opn2 = NULL;
	int nf;
	static float factor = 1;
	d_s *ds = NULL, *dsi;
	pltreg *pr = NULL;
	
	if(updating_menu_state != 0)	return D_O_K;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine finds the extrema"
				"of all data sets and saves increasing and decreasing phases");
	}
	/* we first find the data that we need to transform */
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
		return win_printf_OK("cannot find data");
	nf = dsi->nx;	/* this is the number of points in the data set */
	/*i = win_scanf("By what factor do you want to multiply y %f",&factor);
	  if (i == WIN_CANCEL)	return OFF;*/

	if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
		return win_printf_OK("cannot create plot !");

	if ((opn2 = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
		return win_printf_OK("cannot create plot !");


	// Initialisation du plot avec nombre d extrema
	d_s *dsmin = NULL, *dsmax = NULL;
	O_p *opn3 = NULL;
	int n_min = 0, n_max=0;
	if ((dsmin = build_data_set(16, 16)) == NULL)
		return win_printf_OK("cannot create plot !");
	dsmin->nx = dsmin->ny = 0;
	if ((dsmax = build_data_set(16, 16)) == NULL)
		return win_printf_OK("cannot create plot !");
	dsmax->nx = dsmax->ny = 0;
	if ((opn3 = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
		return win_printf_OK("cannot create plot !");

	
	for(k=0; k<op->n_dat; k++) // Iteration sur les datasets
	{
		dsi = op->dat[k];

		n_min = 0;
		n_max = 0;

		int crois = 0;
		float seuil = op->y_lo;

		for(j = 1; j < nf-1; j++)
		{
			if (dsi->yd[j] > op->y_lo && fabsf(dsi->xd[j+1]-dsi->xd[j])/fabsf(dsi->xd[j]-dsi->xd[j-1]) < (float)2 ) // Tant qu'on est au-dessus du seuil et que le point suivant n est pas trop loin	
			{	
				if (crois == 0)
				{
					if (dsi->yd[j-1] < dsi->yd[j] && dsi->yd[j] < dsi->yd[j+1]) // demarrage croissant
					{
						crois = 1;
						if (ds == NULL) // creation du ds
						{
							if ((ds = build_data_set(16, 16)) == NULL)
								return win_printf_OK("cannot create plot !");
							ds->nx = ds->ny = 0;
						}
					}	
					else if (dsi->yd[j-1] > dsi->yd[j] && dsi->yd[j] > dsi->yd[j+1]) // demarrage decroissant apres une zone croissante
					{
						crois = -1;
						if (ds == NULL) // creation du ds
						{
							if ((ds = build_data_set(16, 16)) == NULL)
								return win_printf_OK("cannot create plot !");
							ds->nx = ds->ny = 0;
						}
					}
					// Si aucun de ces cas n'est realise, on reste a crois = 0 et on attend le prochain point
				}
				if (dsi->yd[j-1] < dsi->yd[j] && dsi->yd[j] < dsi->yd[j+1] && crois == 1) // etait croissant, et le reste
				{
					add_new_point_to_ds(ds, dsi->xd[j], dsi->yd[j]);	
				}

				if (dsi->yd[j-1] > dsi->yd[j] && dsi->yd[j] > dsi->yd[j+1] && crois == -1) // etait décroissant, et le reste
				{
					add_new_point_to_ds(ds, dsi->xd[j], dsi->yd[j]);
				}

				if (dsi->yd[j-1] > dsi->yd[j] && dsi->yd[j] < dsi->yd[j+1] && crois == -1) // minimum local
				{
					add_new_point_to_ds(ds, dsi->xd[j], dsi->yd[j]);
					if (add_one_plot_data(opn, IS_DATA_SET, (void*)ds))
						return win_printf_OK("cannot attach ds !");
					ds = NULL;
					crois = 0;
					n_min++;
				}

				if (dsi->yd[j-1] < dsi->yd[j] && dsi->yd[j] > dsi->yd[j+1] && crois == 1) // maximum local
				{
					add_new_point_to_ds(ds, dsi->xd[j], dsi->yd[j]);
					if (add_one_plot_data(opn2, IS_DATA_SET, (void*)ds))
						return win_printf_OK("cannot attach ds !");
					ds = NULL;
					crois = 0;
					n_max++;
				}
			}

			else // Si on est en-dessous on réinitialise
			{
				if (ds != NULL)
				{
					add_new_point_to_ds(ds, dsi->xd[j], dsi->yd[j]);
					if (add_one_plot_data(opn, IS_DATA_SET, (void*)ds)) // Ajoute a opn car forcement minimum
						return win_printf_OK("cannot attach ds !");
					ds = NULL;
				}
				crois = 0;
			}
		}

		add_new_point_to_ds(dsmin, (float)k, (float)n_min);
		add_new_point_to_ds(dsmax, (float)k, (float)n_max);
	}

	if (add_one_plot_data(opn3, IS_DATA_SET, (void*)dsmin))
		return win_printf_OK("cannot attach ds !");
	
	if (add_one_plot_data(opn3, IS_DATA_SET, (void*)dsmax))
		return win_printf_OK("cannot attach ds !");
	
	/* now we must do some house keeping */
	if (opn != NULL)
	{
		set_plot_title(opn, "Minima and decreasing phases");
		if (op->x_title != NULL) set_plot_x_title(opn, op->x_title);
		if (op->y_title != NULL) set_plot_y_title(opn, op->y_title);
		opn->filename = Transfer_filename(op->filename);
		uns_op_2_op(opn, op);
	}

	if (opn2 != NULL)
	{
		set_plot_title(opn2, "Maxima and increasing phases");
		if (op->x_title != NULL) set_plot_x_title(opn2, op->x_title);
		if (op->y_title != NULL) set_plot_y_title(opn2, op->y_title);
		opn2->filename = Transfer_filename(op->filename);
		uns_op_2_op(opn2, op);
	}
	
	if (opn3 != NULL)
	{
		set_plot_title(opn3, "Number of extrema in all ds");
		set_plot_x_title(opn3, "Dataset index");
		set_plot_y_title(opn3, "Number of extrema");
	}

	/* refisplay the entire plot */
	refresh_plot(pr, pr->n_op-1);
	return D_O_K;
}

MENU *demitour_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"Find extrema of ds (helicase action)", do_demitour_find_extrema,NULL,0,NULL);
	add_item_to_menu(mn,"Find extrema of all ds", do_demitour_find_extrema_all_ds,NULL,0,NULL);
   add_item_to_menu(mn,"Delete ds with few points", do_demitour_delete_data_set,NULL,0,NULL);
	return mn;
}

int	demitour_main(int argc, char **argv)
{
	add_plot_treat_menu_item ( "demitour", NULL, demitour_plot_menu(), 0, NULL);
	return D_O_K;
}

int	demitour_unload(int argc, char **argv)
{
	remove_item_to_menu(plot_treat_menu, "demitour", NULL, NULL);
	return D_O_K;
}
#endif

