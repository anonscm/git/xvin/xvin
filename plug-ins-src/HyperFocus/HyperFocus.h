#ifndef _HYPERFOCUS_H_
#define _HYPERFOCUS_H_

PXV_FUNC(int, do_HyperFocus_average_along_y, (void));
PXV_FUNC(MENU*, HyperFocus_image_menu, (void));
PXV_FUNC(int, HyperFocus_main, (int argc, char **argv));
#endif

