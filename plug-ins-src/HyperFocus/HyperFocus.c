/*
 *    Plug-in program for image treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _HYPERFOCUS_C_
#define _HYPERFOCUS_C_

# include "allegro.h"
# include "xvin.h"

/* If you include other regular header do it here*/


/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled
# include "../fft2d/fft2d.h"

#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "HyperFocus.h"
//place here other headers of this plugin





int do_HyperFocus_average_along_y(void)
{
	register int i, j;
	int l_s, l_e;
	O_i *ois = NULL;
	O_p *op = NULL;
	d_s *ds;
	imreg *imr = NULL;


	if(updating_menu_state != 0)	return D_O_K;

	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine average the image intensity"
			"of several lines of an image");
	}
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	return D_O_K;
	/* we grap the data that we need, the image and the screen region*/
	l_s = 0; l_e = ois->im.ny;
	/* we ask for the limit of averaging*/
	i = win_scanf("Average Between lines%d and %d",&l_s,&l_e);
	if (i == WIN_CANCEL)	return D_O_K;
	/* we create a plot to hold the profile*/
	op = create_one_plot(ois->im.nx,ois->im.nx,0);
	if (op == NULL) return (win_printf_OK("cant create plot!"));
	ds = op->dat[0]; /* we grab the data set */
	op->y_lo = ois->z_black;	op->y_hi = ois->z_white;
	op->iopt2 |= Y_LIM;	op->type = IM_SAME_X_AXIS;
	inherit_from_im_to_ds(ds, ois);
	op->filename = Transfer_filename(ois->filename);
	set_plot_title(op,"HyperFocus averaged profile from line %d to %d",l_s, l_e);
	set_formated_string(&ds->treatement,"averaged profile from line %d to %d",l_s, l_e);
	uns_oi_2_op(ois, IS_Z_UNIT_SET, op, IS_Y_UNIT_SET);
	uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
	for (i=0 ; i< ds->nx ; i++)	ds->yd[i] = 0;
	for (i=l_s ; i< l_e ; i++)
	{
		extract_raw_line(ois, i, ds->xd);
		for (j=0 ; j< ds->nx ; j++)	ds->yd[j] += ds->xd[j];
	}
	for (i=0, j = l_e - l_s ; i< ds->nx && j !=0 ; i++)
	{
		ds->xd[i] = i;
		ds->yd[i] /= j;
	}
	add_one_image (ois, IS_ONE_PLOT, (void *)op);
	ois->cur_op = ois->n_op - 1;
	broadcast_dialog_message(MSG_DRAW,0);
	return D_REDRAWME;
}

O_i	*HyperFocus_image_multiply_by_a_scalar(O_i *ois, float factor)
{
	register int i, j;
	O_i *oid;
	float tmp;
	int onx, ony, data_type;
	union pix *ps, *pd;

	onx = ois->im.nx;	ony = ois->im.ny;	data_type = ois->im.data_type;
	oid =  create_one_image(onx, ony, data_type);
	if (oid == NULL)
	{
		win_printf("Can't create dest image");
		return NULL;
	}
	if (data_type == IS_CHAR_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].ch[j];
				pd[i].ch[j] = (tmp < 0) ? 0 :
					((tmp > 255) ? 255 : (unsigned char)tmp);
			}
		}
	}
	else if (data_type == IS_INT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].in[j];
				pd[i].in[j] = (tmp < -32768) ? -32768 :
					((tmp > 32767) ? 32767 : (short int)tmp);
			}
		}
	}
	else if (data_type == IS_UINT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].ui[j];
				pd[i].ui[j] = (tmp < 0) ? 0 :
					((tmp > 65535) ? 65535 : (unsigned short int)tmp);
			}
		}
	}
	else if (data_type == IS_LINT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].li[j];
				pd[i].li[j] = (tmp < 0x10000000) ? 0x10000000 :
					((tmp > 0x7FFFFFFF) ? 0x7FFFFFFF : (int)tmp);
			}
		}
	}
	else if (data_type == IS_FLOAT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].fl[j] = factor * ps[i].fl[j];
			}
		}
	}
	else if (data_type == IS_DOUBLE_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].db[j] = factor * ps[i].db[j];
			}
		}
	}
	else if (data_type == IS_COMPLEX_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].fl[2*j] = factor * ps[i].fl[2*j];
				pd[i].fl[2*j+1] = factor * ps[i].fl[2*j+1];
			}
		}
	}
	else if (data_type == IS_COMPLEX_DOUBLE_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].db[2*j] = factor * ps[i].db[2*j];
				pd[i].db[2*j+1] = factor * ps[i].db[2*j+1];
			}
		}
	}
	else if (data_type == IS_RGB_PICTURE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< 3*onx; j++)
			{
				tmp = factor * ps[i].ch[j];
				pd[i].ch[j] = (tmp < 0) ? 0 :
					((tmp > 255) ? 255 : (unsigned char)tmp);
			}
		}
	}
	else if (data_type == IS_RGBA_PICTURE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< 4*onx; j++)
			{
				tmp = factor * ps[i].ch[j];
				pd[i].ch[j] = (tmp < 0) ? 0 :
					((tmp > 255) ? 255 : (unsigned char)tmp);
			}
		}
	}
	inherit_from_im_to_im(oid,ois);
	uns_oi_2_oi(oid,ois);
	oid->im.win_flag = ois->im.win_flag;
	if (ois->title != NULL)	set_im_title(oid, "%s rescaled by %g",
		 ois->title,factor);
	set_formated_string(&oid->im.treatement,
		"Image %s rescaled by %g", ois->filename,factor);
	return oid;
}

int extract_avg_z_profile_RGB_image(O_i *ois, int col, int xc, int yc, float *zp)
{
   int im, ix, iy, nfi, onx, ony;
   int onx3;
   int ix1, iy1, ix_1, iy_1;
   double tmp;

  if (ois->im.data_type != IS_RGB_PICTURE || zp == NULL) return 1;

   onx = ois->im.nx;	ony = ois->im.ny;
   nfi = abs(ois->im.n_f);
   nfi = (nfi == 0) ? 1 : nfi;
   onx3 = 3 * onx;

   xc = (xc < 0) ? 0 : xc;
   xc = (xc < onx) ? xc : onx - 1;
   yc = (yc < 0) ? 0 : yc;
   yc = (yc < ony) ? yc : ony - 1;
   ix = 3 * xc + col;
   iy = yc;
   for (im = 0, tmp = 0; im < nfi; im++)
     {
        tmp = 4*ois->im.pxl[im][iy].ch[ix];
        ix1 = ix + 3;
        ix1 = (ix1 < onx3) ? ix1 : ix;
        iy1 = iy + 1;
        iy1 = (iy1 < ony) ? iy1 : iy;
        ix_1 = ix - 3;
        ix_1 = (ix_1 < 0) ? 0 : ix_1;
        iy_1 = iy - 1;
        iy_1 = (iy_1 < 0) ? 0 : iy_1;
        tmp += 2*ois->im.pxl[im][iy].ch[ix1];
        tmp += 2*ois->im.pxl[im][iy].ch[ix_1];
        tmp += 2*ois->im.pxl[im][iy1].ch[ix];
        tmp += 2*ois->im.pxl[im][iy_1].ch[ix];
        tmp += ois->im.pxl[im][iy1].ch[ix1];
        tmp += ois->im.pxl[im][iy1].ch[ix_1];
        tmp += ois->im.pxl[im][iy_1].ch[ix_1];
        tmp += ois->im.pxl[im][iy_1].ch[ix1];
        zp[im] = tmp/16;
     }
   return 0;
}

int extract_avg2_z_profile_RGB_image(O_i *ois, int col, int xc, int yc, float *zp)
{
   int im, ix, iy, nfi, onx, ony;
   int onx3;
   int ix1, iy1, ix_1, iy_1, ix2, iy2, ix_2, iy_2;
   double tmp, tmp2;

  if (ois->im.data_type != IS_RGB_PICTURE || zp == NULL) return 1;

   onx = ois->im.nx;	ony = ois->im.ny;
   nfi = abs(ois->im.n_f);
   nfi = (nfi == 0) ? 1 : nfi;
   onx3 = 3 * onx;

   xc = (xc < 0) ? 0 : xc;
   xc = (xc < onx) ? xc : onx - 1;
   yc = (yc < 0) ? 0 : yc;
   yc = (yc < ony) ? yc : ony - 1;
   ix = 3 * xc + col;
   iy = yc;
   for (im = 0, tmp = 0; im < nfi; im++)
     {
        tmp = ois->im.pxl[im][iy].ch[ix];
        ix1 = ix + 3;
        ix1 = (ix1 < onx3) ? ix1 : ix;
        iy1 = iy + 1;
        iy1 = (iy1 < ony) ? iy1 : iy;
        ix_1 = ix - 3;
        ix_1 = (ix_1 < 0) ? 0 : ix_1;
        iy_1 = iy - 1;
        iy_1 = (iy_1 < 0) ? 0 : iy_1;
        ix2 = ix1 + 3;
        ix2 = (ix2 < onx3) ? ix2 : ix1;
        iy2 = iy1 + 1;
        iy2 = (iy2 < ony) ? iy2 : iy1;
        ix_2 = ix_1 - 3;
        ix_2 = (ix_2 < 0) ? 0 : ix_2;
        iy_2 = iy1 - 1;
        iy_2 = (iy_2 < 0) ? 0 : iy_2;
        tmp2 = 1;
        tmp += 0.6*ois->im.pxl[im][iy].ch[ix1];
        tmp += 0.6*ois->im.pxl[im][iy].ch[ix_1];
        tmp += 0.6*ois->im.pxl[im][iy1].ch[ix];
        tmp += 0.6*ois->im.pxl[im][iy_1].ch[ix];
        tmp2 += 4*0.6;
        tmp += 0.36*ois->im.pxl[im][iy1].ch[ix1];
        tmp += 0.36*ois->im.pxl[im][iy1].ch[ix_1];
        tmp += 0.36*ois->im.pxl[im][iy_1].ch[ix_1];
        tmp += 0.36*ois->im.pxl[im][iy_1].ch[ix1];
        tmp2 += 4*0.36;
        tmp += 0.13*ois->im.pxl[im][iy].ch[ix2];
        tmp += 0.13*ois->im.pxl[im][iy].ch[ix_2];
        tmp += 0.13*ois->im.pxl[im][iy2].ch[ix];
        tmp += 0.13*ois->im.pxl[im][iy_2].ch[ix];
        tmp2 += 4*0.13;
        tmp += 0.07*ois->im.pxl[im][iy1].ch[ix2];
        tmp += 0.07*ois->im.pxl[im][iy1].ch[ix_2];
        tmp += 0.07*ois->im.pxl[im][iy_1].ch[ix_2];
        tmp += 0.07*ois->im.pxl[im][iy_1].ch[ix2];
        tmp += 0.07*ois->im.pxl[im][iy2].ch[ix1];
        tmp += 0.07*ois->im.pxl[im][iy2].ch[ix_1];
        tmp += 0.07*ois->im.pxl[im][iy_2].ch[ix_1];
        tmp += 0.07*ois->im.pxl[im][iy_2].ch[ix1];
        tmp2 += 8*0.07;
        zp[im] = tmp/tmp2;
     }
   return 0;
}


int do_image_extract_z_avg_profiles_in_box(void)
{
  O_i *ois;
  O_p *op;
  d_s *ds;
  imreg *imr;
  int i, j, k, nfi, onx, ony, ver = 0;;
  static int w = 32, h = 32,xc = 0 ,yc = 0, col = 1;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
      return D_O_K;
  nfi = abs(ois->im.n_f);
  nfi = (nfi == 0) ? 1 : nfi;

  if(updating_menu_state != 0)
    {
      if (nfi == 1) 	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the image intensity"
			   "by a user defined scalar factor");
    }
  onx = ois->im.nx;	ony = ois->im.ny;
  i = win_scanf("Define the box width %5d and height %5d\n"
                "center xc %5d and yc %5d\nSelect color:\n%R->R\n%r->G\n%r->B\n"
                ,&w,&h,&xc,&yc,&col);
  if (i == WIN_CANCEL) return 0;

  op = create_and_attach_op_to_oi(ois, nfi, nfi, 0, 0);
  if (op == NULL)	return win_printf_OK("Could not create plot data");
  ds = op->dat[0];

  for (i = (yc - h/2); i < (yc + h/2); i++)
    {
       if (i < 0 || i >= ony)
           {
               ver =  win_printf("excluded line = %d not in [0,%d[",i,ony);
               continue;
           }
       for (j = (xc - w/2); j < (xc + w/2); j++)
          {
              if (j < 0 || j >= onx)
                 {
                     ver =  win_printf("excluded col = %d  not in [0,%d[",j,onx);
                     continue;
                 }
              if (ds == NULL) ds = create_and_attach_one_ds(op, nfi, nfi, 0);
              if (ds == NULL) continue;
              if (ver != WIN_CANCEL)  ver =  win_printf("z prof in line = %d col %d",i,j);
              extract_avg2_z_profile_RGB_image(ois, col, j, i, ds->yd);
              set_ds_source(ds,"Z profile avg at x = %d y = %d",j,i);
              for (k = 0; k < ds->nx; k++) ds->xd[k] = k;
              ds = NULL;
           }
    }
  ois->need_to_refresh |= PLOT_NEED_REFRESH  | EXTRA_PLOT_NEED_REFRESH;
  return refresh_image(imr, UNCHANGED);
}


int do_extract_significant_min_of_z_avg_profiles(void)
{
  O_i *ois, *oid;
  O_p *op;
  d_s *ds;
  imreg *imr;
  int i, j, k, nfi, onx, ony, imin;
  float tmp, min;
  static float thres = 6;
  static int  col = 1;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
      return D_O_K;
  nfi = abs(ois->im.n_f);
  nfi = (nfi == 0) ? 1 : nfi;

  if(updating_menu_state != 0)
    {
      if (nfi == 1) 	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the image intensity"
			   "by a user defined scalar factor");
    }
  onx = ois->im.nx;	ony = ois->im.ny;
  i = win_scanf("Define the threshold of strong min %5f\nSelect color:\n%R->R\n%r->G\n%r->B\n"
                ,&thres,&col);
  if (i == WIN_CANCEL) return 0;

  op = create_and_attach_op_to_oi(ois, nfi, nfi, 0, 0);
  if (op == NULL)	return win_printf_OK("Could not create plot data");
  ds = op->dat[0];

  oid = create_and_attach_oi_to_imr(imr,onx,ony,(nfi < 256) ? IS_CHAR_IMAGE : IS_UINT_IMAGE);
  if (oid == NULL)	return win_printf_OK("Could not create image");

  for (i = 0; i < ony; i++)
    {
       for (j = 0; j < onx; j++)
          {
              extract_avg2_z_profile_RGB_image(ois, col, j, i, ds->yd);
              for (k = 0, tmp = 0; k < ds->nx; k++) tmp += ds->yd[k];
              for (k = 0, tmp /= ds->nx, imin = 0, min = ds->yd[0]; k < ds->nx; k++)
                      min = (ds->yd[k] < min) ? ds->yd[imin = k] : min;
              if (tmp - min > thres)
                 {
                         if (nfi < 256) oid->im.pixel[i].ch[j] = imin;
                         else oid->im.pixel[i].ui[j] = imin;
                 }
           }
    }
  oid->need_to_refresh |= ALL_NEED_REFRESH;
  return refresh_image(imr, imr->n_oi -1);
}


int do_image_extract_z_profiles_in_box(void)
{
  O_i *ois;
  O_p *op;
  d_s *ds;
  imreg *imr;
  int i, j, k, nfi, onx, ony, ver = 0;;
  static int w = 32, h = 32,xc = 0 ,yc = 0;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
      return D_O_K;
  nfi = abs(ois->im.n_f);
  nfi = (nfi == 0) ? 1 : nfi;

  if(updating_menu_state != 0)
    {
      if (nfi == 1) 	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the image intensity"
			   "by a user defined scalar factor");
    }
  onx = ois->im.nx;	ony = ois->im.ny;
  i = win_scanf("Define the box width %5d and height %5d\n"
                "center xc %5d and yc %5d\n",&w,&h,&xc,&yc);
  if (i == WIN_CANCEL) return 0;

  op = create_and_attach_op_to_oi(ois, nfi, nfi, 0, 0);
  if (op == NULL)	return win_printf_OK("Could not create plot data");
  ds = op->dat[0];

  for (i = (yc - h/2); i < (yc + h/2); i++)
    {
       if (i < 0 || i >= ony)
           {
               ver =  win_printf("excluded line = %d not in [0,%d[",i,ony);
               continue;
           }
       for (j = (xc - w/2); j < (xc + w/2); j++)
          {
              if (j < 0 || j >= onx)
                 {
                     ver =  win_printf("excluded col = %d  not in [0,%d[",j,onx);
                     continue;
                 }
              if (ds == NULL) ds = create_and_attach_one_ds(op, nfi, nfi, 0); 
              if (ds == NULL) continue;
              if (ver != WIN_CANCEL)  ver =  win_printf("z prof in line = %d col %d",i,j);
              extract_z_profile_from_movie(ois, j, i, ds->yd);
              set_ds_source(ds,"Z profile at x = %d y = %d",j,i);
              for (k = 0; k < ds->nx; k++) ds->xd[k] = k;
              ds = NULL;
           }
    }
  ois->need_to_refresh |= PLOT_NEED_REFRESH  | EXTRA_PLOT_NEED_REFRESH;
  return refresh_image(imr, UNCHANGED);
}

int find_image_mean_sigma_sigmap_sigmam(O_i *ois, double *mean, double *sigma, double *sigm, double *sigp)
{
  int i, j,  onx, ony, isigm, isigp;
  double meanl, sigmal, val, sigml, sigpl, tmp;

  if (ois == NULL) return 1;
  onx = ois->im.nx;	ony = ois->im.ny;  
  for (i = 0, meanl = 0; i < ony ; i++)
    {
      for (j=0; j< onx; j++)
	{
	  get_raw_pixel_value(ois, j, i, &val);
	  meanl += val;
	}
    }
  meanl /= onx*ony;
  for (i = 0, sigmal = sigml = sigpl = 0, isigm = isigp = 0; i < ony ; i++)
    {
      for (j=0; j< onx; j++)
	{
	  get_raw_pixel_value(ois, j, i, &val);
	  tmp = val - meanl;
	  tmp *= tmp;
	  sigmal += tmp;
	  if (val > meanl)
	    {
	      sigpl += tmp*tmp;;
	      isigp++;
	    }
	  else
	    {
	      sigml += tmp*tmp;
	      isigm++;
	    }
	}
    }
  sigmal /= (onx*ony - 1);
  sigml = (isigm > 0) ? sigml/isigm : sigml;
  sigpl = (isigp > 0) ? sigpl/isigp : sigpl;
  if (mean) *mean = meanl;
  if (sigma) *sigma = sigmal;
  if (sigm) *sigm = sigml;
  if (sigp) *sigp = sigpl;
  return 0;
}

int do_HyperFocus_image_draw_sigma_vs_z(void)
{
  O_i *ois = NULL;
  O_p *op = NULL;
  d_s *ds = NULL, *dsm = NULL, *dsp = NULL;
  imreg *imr = NULL;
  int i, j, nfi, onx, ony, im, isigm, isigp;
  double mean, sigma, val, sigm, sigp;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
      return D_O_K;
  nfi = abs(ois->im.n_f);
  nfi = (nfi == 0) ? 1 : nfi;

  if(updating_menu_state != 0)
    {
      if (nfi == 1) 	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the image intensity"
			   "by a user defined scalar factor");
    }
  onx = ois->im.nx;	ony = ois->im.ny;
  op = create_and_attach_op_to_oi(ois, nfi, nfi, 0, 0);
  if (op == NULL)	return win_printf_OK("Could not create plot data");
  dsm = create_and_attach_one_ds(op, nfi, nfi, 0);
  dsp = create_and_attach_one_ds(op, nfi, nfi, 0);
  ds = op->dat[0];
  inherit_from_im_to_ds(ds, ois);
  update_ds_treatement(ds, "Simple Variance (arround mean of rach image) <(Z-Z_m)^2>");
  inherit_from_im_to_ds(dsm, ois);
  update_ds_treatement(dsp, "Positive square Variance (arround mean of rach image) \\Sigma (<(Z-Z_m)^4>) for Z > Z_m");
  inherit_from_im_to_ds(dsp, ois);
  update_ds_treatement(dsm, "Negative square Variance (arround mean of rach image) \\Sigma (<(Z-Z_m)^4>) for Z < Z_m");  
  for (im = 0; im < nfi; im++)
    {
      switch_frame(ois,im);
      for (i = 0, mean = 0; i < ony ; i++)
	{
	  for (j=0; j< onx; j++)
	    {
	      get_raw_pixel_value(ois, j, i, &val);
	      mean += val;
	    }
	}
      mean /= onx*ony;
      for (i = 0, sigma = sigm = sigp = 0, isigm = isigp = 0; i < ony ; i++)
	{
	  for (j=0; j< onx; j++)
	    {
	      get_raw_pixel_value(ois, j, i, &val);
	      sigma += (val - mean)*(val-mean);
              if (val > mean)
                 {
                    sigp += (val - mean)*(val-mean) *(val - mean)*(val-mean);
                    isigp++;
                 }
              else
                 {
                    sigm += (val - mean)*(val-mean) * (val - mean)*(val-mean);
                    isigm++;
                 }
	    }
	}
      sigma /= (onx*ony - 1);
      sigm = (isigm > 0) ? sigm/isigm : sigm;
      sigp = (isigp > 0) ? sigp/isigp : sigp;
      ds->xd[im] = dsp->xd[im] = dsm->xd[im] = im;
      ds->yd[im] = sigma;
      ds->xd[im] = im;
      dsp->yd[im] = sigp;
      dsm->yd[im] = sigm;
    }
  ois->need_to_refresh |= PLOT_NEED_REFRESH  | EXTRA_PLOT_NEED_REFRESH;
  return (refresh_image(imr, UNCHANGED));
}


int get_complex_raw_pixel_value(O_i *oi, int ix, int iy, double *re, double *im)
{
    double zr = 0, zi = 0;

    if (oi == NULL || ix < 0 || iy < 0 || ix >= oi->im.nx || iy >= oi->im.ny)
    {
        return 1;
    }
    if (oi->im.data_type == IS_CHAR_IMAGE || oi->im.data_type == IS_RGB_PICTURE ||
	oi->im.data_type == IS_RGBA_PICTURE || oi->im.data_type == IS_RGB16_PICTURE ||
	oi->im.data_type == IS_RGBA16_PICTURE || oi->im.data_type == IS_UINT_IMAGE ||
	oi->im.data_type == IS_LINT_IMAGE || oi->im.data_type == IS_FLOAT_IMAGE  ||
	oi->im.data_type == IS_DOUBLE_IMAGE)
      return 1;
    
    switch (oi->im.data_type)
      {
      case  IS_COMPLEX_IMAGE:
        zr = oi->im.pixel[iy].cp[ix].re;
        zi = oi->im.pixel[iy].cp[ix].im;
        break;
      case  IS_COMPLEX_DOUBLE_IMAGE:
        zr = oi->im.pixel[iy].dcp[ix].dre;
        zi = oi->im.pixel[iy].dcp[ix].dim;
        break;
      default:
        return 1;
      }
    if (re) *re = zr;
    if (im) *im = zi;    
    return 0;
}

d_s *project_mode_on_ds(O_i *ois, d_s *dsd)
{
    int nt;
    int i, j, onx, ony;
    double  rep = 0, imp = 0;
    d_s *ds;
    
    if (ois == NULL) return NULL;
  
    onx = ois->im.nx;	ony = ois->im.ny;
    nt = onx * (ony - 1);  // special mode at k = 0 not used
    if (dsd != NULL)
      {
	ds = dsd;
	if (dsd->nx < nt || dsd->ny < nt)
	  {ds = build_adjust_data_set(ds,nt,nt);}
      }
    else
      {
	ds = build_data_set(nt,nt);
      }
    if (ds == NULL)	return NULL;
    inherit_from_im_to_ds(ds, ois);
    update_ds_treatement(ds, "Power in one image \\Sigma (Re^2+ Im^2) on all pixels");  

    for (i = 1, ds->nx = 0; i < ony ; i++)
      {
	for (j=0; j< onx; j++)
	  {
	    get_complex_raw_pixel_value(ois, j, i, &rep, &imp);
	    ds->yd[ds->nx] = imp;
	    ds->xd[ds->nx] = rep;
	    ds->nx = ((ds->nx+1) < ds->mx) ? ds->nx+1: ds->nx; 
	  }
      }
    ds->nx = ds->ny = nt;
    return ds;
}



int do_project_complex_image_on_ds(void)
{
  O_i *ois = NULL;
  O_p *op = NULL;
  d_s *ds = NULL;
  imreg *imr = NULL;
  int  nfi, onx, ony;
  double rep, imp;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
      return D_O_K;
  nfi = abs(ois->im.n_f);
  nfi = (nfi == 0) ? 1 : nfi;

  if(updating_menu_state != 0)
    {
      if (get_complex_raw_pixel_value(ois, 0, 0, &rep, &imp))
	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the image intensity"
			   "by a user defined scalar factor");
    }
  
  onx = ois->im.nx;	ony = ois->im.ny;
  op = create_and_attach_op_to_oi(ois, onx*ony, onx*ony, 0, 0);
  if (op == NULL)	return win_printf_OK("Could not create plot data");
  ds = op->dat[0];
  project_mode_on_ds(ois, ds);
  ois->need_to_refresh |= PLOT_NEED_REFRESH  | EXTRA_PLOT_NEED_REFRESH;
  return (refresh_image(imr, UNCHANGED));
}


int do_HyperFocus_image_draw_power_vs_z(void)
{
  O_i *ois = NULL;
  O_p *op = NULL;
  d_s *ds = NULL;
  imreg *imr = NULL;
  int i, j, nfi, onx, ony, im;
  double mean, rep, imp;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
      return D_O_K;
  nfi = abs(ois->im.n_f);
  nfi = (nfi == 0) ? 1 : nfi;

  if(updating_menu_state != 0)
    {
      if (nfi == 1 || get_complex_raw_pixel_value(ois, 0, 0, &rep, &imp))
	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the image intensity"
			   "by a user defined scalar factor");
    }
  onx = ois->im.nx;	ony = ois->im.ny;
  op = create_and_attach_op_to_oi(ois, nfi, nfi, 0, 0);
  if (op == NULL)	return win_printf_OK("Could not create plot data");
  ds = op->dat[0];
  inherit_from_im_to_ds(ds, ois);
  update_ds_treatement(ds, "Power in one image \\Sigma (Re^2+ Im^2) on all pixels");  

  for (im = 0; im < nfi; im++)
    {
      switch_frame(ois,im);
      for (i = 0, mean = 0; i < ony ; i++)
	{
	  for (j=0; j< onx; j++)
	    {
	      get_complex_raw_pixel_value(ois, j, i, &rep, &imp);
	      mean += rep*rep+imp*imp;
	    }
	}
      ds->yd[im] = mean;
      ds->xd[im] = im;
    }
  ois->need_to_refresh |= PLOT_NEED_REFRESH  | EXTRA_PLOT_NEED_REFRESH;
  return (refresh_image(imr, imr->n_oi - 1));
}


int do_HyperFocus_image_draw_im_cor_n_n_1_vs_z(void)
{
  O_i *ois;
  O_p *op;
  d_s *ds, *ds2;
  imreg *imr;
  int i, j, k, nfi, onx, ony;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
      return D_O_K;
  nfi = abs(ois->im.n_f);
  nfi = (nfi == 0) ? 1 : nfi;

  if(updating_menu_state != 0)
    {
      if (nfi == 1) 	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the image intensity"
			   "by a user defined scalar factor");
    }
  onx = ois->im.nx;	ony = ois->im.ny;
  op = create_and_attach_op_to_oi(ois, nfi, nfi, 0, 0);
  if (op == NULL)	return win_printf_OK("Could not create plot data");
  ds = op->dat[0];
  ds2 = create_and_attach_one_ds(op, nfi, nfi, 0);

  for (i = 0; i < ony ; i++)
     {
	for (j=0; j< onx; j++)
	   {
              extract_z_profile_from_movie(ois, j, i, ds->xd);
              for (k = 0; k < ds->nx-1; k++)
                 {
                     ds->yd[k] += ds->xd[k] * ds->xd[k+1];
                     if (ds->xd[k] < 0 ||  ds->xd[k+1] < 0)
                        {
                            ds2->yd[k] += ds->xd[k] * ds->xd[k+1];
                            ds2->xd[k] += 1;
                        }
                 }
           }
     }
  for (k = 0; k < ds->nx-1; k++)
     {
         ds->yd[k] /= onx*ony;
         ds2->yd[k] = (ds2->xd[k] > 0) ? ds2->yd[k]/ds2->xd[k] : ds2->yd[k];
         ds->xd[k] = ds2->xd[k] = 0.5 + k;
     }
  ds->nx = ds->ny = nfi - 1;

  ois->need_to_refresh |= PLOT_NEED_REFRESH  | EXTRA_PLOT_NEED_REFRESH;
  return refresh_image(imr, UNCHANGED);
}

int do_HyperFocus_image_draw_im_cor_n_n_1_vs_z_pond(void)
{
  O_i *ois = NULL;
  O_p *op = NULL;
  d_s *ds = NULL, *ds2 = NULL, *dst = NULL;
  imreg *imr = NULL;
  int i, j, k, nfi, onx, ony;
  float w, tmp, noise2;
  static float noise = 1.75;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
      return D_O_K;
  nfi = abs(ois->im.n_f);
  nfi = (nfi == 0) ? 1 : nfi;

  if(updating_menu_state != 0)
    {
      if (nfi == 1) 	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the image intensity"
			   "by a user defined scalar factor");
    }
  onx = ois->im.nx;	ony = ois->im.ny;

  i = win_scanf("Indiquate noise level for ponderation %4f",&noise);
  if (i == WIN_CANCEL) return 0;
  op = create_and_attach_op_to_oi(ois, nfi, nfi, 0, 0);
  if (op == NULL)	return win_printf_OK("Could not create plot data");
  ds = op->dat[0];
  ds2 = create_and_attach_one_ds(op, nfi, nfi, 0);
  dst = build_data_set(nfi, nfi);
  noise2 = noise*noise;
  for (i = 0; i < ony ; i++)
     {
	for (j=0; j< onx; j++)
	   {
              extract_z_profile_from_movie(ois, j, i, dst->xd);
              for (k = 0; k < ds->nx-1; k++)
                 {
		   tmp = dst->xd[k] * dst->xd[k+1];
		   w = 1 - exp(-tmp/noise2);
		   ds->yd[k] += w * tmp;
		   ds->xd[k] += w;
		   if (dst->xd[k] < 0 ||  dst->xd[k+1] < 0)
                        {
			  ds2->yd[k] += w * tmp;
			  ds2->xd[k] += w;
                        }
                 }
           }
     }
  for (k = 0; k < ds->nx-1; k++)
     {
         ds->yd[k] = (ds->xd[k] > 0) ? ds->yd[k]/ds->xd[k] : ds->yd[k];	 
         ds2->yd[k] = (ds2->xd[k] > 0) ? ds2->yd[k]/ds2->xd[k] : ds2->yd[k];
         ds->xd[k] = ds2->xd[k] = 0.5 + k;
     }
  ds->nx = ds->ny = nfi - 1;
  free_data_set(dst);
  ois->need_to_refresh |= PLOT_NEED_REFRESH  | EXTRA_PLOT_NEED_REFRESH;
  return refresh_image(imr, UNCHANGED);
}




# ifdef DOUBLON

int do_HyperFocus_image_draw_sigma_vs_z(void)
{
  O_i *ois;
  O_p *op;
  d_s *ds;
  imreg *imr;
  int i, j, nfi, onx, ony, im;
  double mean, sigma, val;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
      return D_O_K;
  nfi = abs(ois->im.n_f);
  nfi = (nfi == 0) ? 1 : nfi;

  if(updating_menu_state != 0)
    {
      if (nfi == 1) 	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the image intensity"
			   "by a user defined scalar factor");
    }
  onx = ois->im.nx;	ony = ois->im.ny;
  op = create_and_attach_op_to_oi(ois, nfi, nfi, 0, 0);
  if (op == NULL)	return win_printf_OK("Could not create plot data");
  ds = op->dat[0];

  for (im = 0; im < nfi; im++)
    {
      switch_frame(ois,im);
      for (i = 0, mean = 0; i < ony ; i++)
	{
	  for (j=0; j< onx; j++)
	    {
	      get_raw_pixel_value(ois, j, i, &val);
	      mean += val;
	    }
	}
      mean /= onx*ony;
      for (i = 0, sigma = 0; i < ony ; i++)
	{
	  for (j=0; j< onx; j++)
	    {
	      get_raw_pixel_value(ois, j, i, &val);
	      sigma += (val - mean)*(val-mean);
	    }
	}
      sigma /= (onx*ony - 1);
      ds->xd[im] = im;
      ds->yd[im] = sigma;
    }
  ois->need_to_refresh |= PLOT_NEED_REFRESH  | EXTRA_PLOT_NEED_REFRESH;
  return (refresh_image(imr, imr->n_oi - 1));
}

# endif


int HP_image(O_i *oid, O_i *ois)
{
  int i, j, onx, ony;
  double val;
  if (ois == NULL || oid == NULL) return 1;

  onx = ois->im.nx;	ony = ois->im.ny;
  for (i = 0; i < ony ; i++)
    {
      for (j=0; j< onx; j++)
	oid->im.pixel[i].fl[j] = 0;
    }
  for (i = 1; i < ony-1 ; i++)
    {
      for (j=1; j< onx-1; j++)
	{
	  get_raw_pixel_value(ois, j, i, &val);
	  if (i != 0 && j != 0 && i < ony-1 && j < onx-1)
	    oid->im.pixel[i].fl[j] += (float)8*val;
	  get_raw_pixel_value(ois, j-1, i-1, &val);
	  oid->im.pixel[i].fl[j] -= (float)val;
	  get_raw_pixel_value(ois, j, i-1, &val);
	  oid->im.pixel[i].fl[j] -= (float)val;
	  get_raw_pixel_value(ois, j-1, i, &val);
	  oid->im.pixel[i].fl[j] -= (float)val;
	  get_raw_pixel_value(ois, j+1, i-1, &val);
	  oid->im.pixel[i].fl[j] -= (float)val;
	  get_raw_pixel_value(ois, j-1, i+1, &val);
	  oid->im.pixel[i].fl[j] -= (float)val;
	  get_raw_pixel_value(ois, j+1, i, &val);
	  oid->im.pixel[i].fl[j] -= (float)val;
	  get_raw_pixel_value(ois, j, i+1, &val);
	  oid->im.pixel[i].fl[j] -= (float)val;
	  get_raw_pixel_value(ois, j+1, i+1, &val);
	  oid->im.pixel[i].fl[j] -= (float)val;
	}
    }
  return 0;
}



int Correlation_pixel_pixel_1_image(O_i *oid, O_i *ois)
{
  int i, j, onx, ony;
  double val00, val10, val01, val11;
  if (ois == NULL || oid == NULL) return 1;

  onx = ois->im.nx;	ony = ois->im.ny;
  onx = (oid->im.nx < onx) ? oid->im.nx : onx;
  ony = (oid->im.ny < ony) ? oid->im.ny : ony;
  for (i = 0; i < ony ; i++)
    {
      for (j=0; j< onx; j++)
	oid->im.pixel[i].fl[j] = 0;
    }
  for (i = 0; i < ony-1 ; i++)
    {
      for (j=0; j< onx-1; j++)
	{
	  get_raw_pixel_value(ois, j, i+1, &val10);
	  get_raw_pixel_value(ois, j, i, &val00);
	  get_raw_pixel_value(ois, j+1, i+1, &val11);
	  get_raw_pixel_value(ois, j+1, i, &val01);
          oid->im.pixel[i].fl[j] = (float) (val00 * val11 + val01 * val10);
          oid->im.pixel[i].fl[j] *= (float)val00;
	}
    }
  return 0;
}


int do_HyperFocus_image_draw_sigma_vs_z_HP(void)
{
  O_i *ois, *oid;
  O_p *op;
  d_s *ds;
  imreg *imr;
  int i, j, nfi, onx, ony, im;
  double mean, sigma, val;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
      return D_O_K;
  nfi = abs(ois->im.n_f);
  nfi = (nfi == 0) ? 1 : nfi;

  if(updating_menu_state != 0)
    {
      if (nfi == 1) 	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the image intensity"
			   "by a user defined scalar factor");
    }
  onx = ois->im.nx;	ony = ois->im.ny;
  oid = create_one_image(onx, ony, IS_FLOAT_IMAGE);
  if (oid == NULL)	return win_printf_OK("Could not create temp image");
  op = create_and_attach_op_to_oi(ois, nfi, nfi, 0, 0);
  if (op == NULL)	return win_printf_OK("Could not create plot data");
  ds = op->dat[0];

  for (im = 0; im < nfi; im++)
    {
      switch_frame(ois,im);
      HP_image(oid, ois);
      for (i = 0, mean = 0; i < ony ; i++)
	{
	  for (j=0; j< onx; j++)
	    {
	      mean += oid->im.pixel[i].fl[j];
	    }
	}
      mean /= onx*ony;
      for (i = 0, sigma = 0; i < ony ; i++)
	{
	  for (j=0; j< onx; j++)
	    {
	      val = oid->im.pixel[i].fl[j];
	      sigma += (val - mean)*(val-mean);
	    }
	}
      sigma /= (onx*ony - 1);
      ds->xd[im] = im;
      ds->yd[im] = sigma;
    }
  ois->need_to_refresh |= PLOT_NEED_REFRESH  | EXTRA_PLOT_NEED_REFRESH;
  free_one_image(oid);
  return (refresh_image(imr, imr->n_oi - 1));
}

O_i *remove_in_RGB_image_dc_from_profile(O_i *ois, int col, int xc, int yc, int w, int h)
{
   O_i *oid;
   int i, j, ix, iy, nfi, onx, ony, x0, y0, x1, y1, itmp, im, pval;
   double tmp;

   if (ois->im.data_type != IS_RGB_PICTURE) return NULL;

   onx = ois->im.nx;	ony = ois->im.ny;
   nfi = abs(ois->im.n_f);
   nfi = (nfi == 0) ? 1 : nfi;
   x0 = xc - w/2;
   x0 = (x0 < 0) ? 0 : x0;
   x1 = x0 + w;
   if (x1 > onx)
      {
          x1 = onx;
          x0 = x1 - w;
          x0 = (x0 < 0) ? 0 : x0;
      }
   y0 = yc - h/2;
   y0 = (y0 < 0) ? 0 : y0;
   y1 = y0 + h;
   if (y1 > ony)
      {
          y1 = ony;
          y0 = y1 - h;
          y0 = (y0 < 0) ? 0 : y0;
      }
   oid = create_one_movie(w, h, IS_CHAR_IMAGE, nfi);
   if (oid == NULL)	return NULL;
   int x13 = 3 *  x1 + col;
   for (iy = y0, i = 0; iy < y1 ; iy++, i++)
     {
       for (ix = 3 * x0 + col, j = 0; ix < x13; ix += 3, j++)
         {
             for (im = 0, tmp = 0; im < nfi; im++)
                 tmp += ois->im.pxl[im][iy].ch[ix];
             tmp /= (nfi > 0) ? nfi : 0;
             itmp = (int)(0.5 + tmp);
             for (im = 0; im < nfi; im++)
                {
                    pval = 128 + ois->im.pxl[im][iy].ch[ix] - itmp;
                    pval = (pval < 0) ? 0 : pval;
                    oid->im.pxl[im][i].ch[j] = (pval < 255) ? pval : 255;
                }
          }
     }
   inherit_from_im_to_im(oid, ois);
   return oid;
}


O_i *remove_in_RGB_image_dc_from_avg_profile(O_i *ois, int col, int xc, int yc, int w, int h)
{
   O_i *oid;
   int i, j, ix, iy, nfi, onx, ony, x0, y0, x1, y1, itmp, im, pval;
   double tmp;

   if (ois->im.data_type != IS_RGB_PICTURE) return NULL;

   onx = ois->im.nx;	ony = ois->im.ny;
   nfi = abs(ois->im.n_f);
   nfi = (nfi == 0) ? 1 : nfi;
   x0 = xc - w/2;
   x0 = (x0 < 0) ? 0 : x0;
   x1 = x0 + w;
   if (x1 > onx)
      {
          x1 = onx;
          x0 = x1 - w;
          x0 = (x0 < 0) ? 0 : x0;
      }
   y0 = yc - h/2;
   y0 = (y0 < 0) ? 0 : y0;
   y1 = y0 + h;
   if (y1 > ony)
      {
          y1 = ony;
          y0 = y1 - h;
          y0 = (y0 < 0) ? 0 : y0;
      }
   oid = create_one_movie(w, h, IS_CHAR_IMAGE, nfi);
   if (oid == NULL)	return NULL;
   int x13 = 3 *  x1 + col, onx3 = 3* onx;
   int ix1, iy1, ix_1, iy_1;
   for (iy = y0, i = 0; iy < y1 ; iy++, i++)
     {
       for (ix = 3 * x0 + col, j = 0; ix < x13; ix += 3, j++)
         {
             for (im = 0, tmp = 0; im < nfi; im++)
                {
                    tmp += ois->im.pxl[im][iy].ch[ix];
                    ix1 = ix + 3;
                    ix1 = (ix1 > onx3) ? ix : ix1;
                    iy1 = iy + 1;
                    iy1 = (iy1 > ony) ? iy : iy1;
                    ix_1 = ix - 3;
                    ix_1 = (ix_1 < 0) ? 0 : ix_1;
                    iy_1 = iy - 1;
                    iy_1 = (iy_1 < 0) ? 0 : iy_1;
                    tmp += ois->im.pxl[im][iy].ch[ix1];
                    tmp += ois->im.pxl[im][iy].ch[ix_1];
                    tmp += ois->im.pxl[im][iy1].ch[ix];
                    tmp += ois->im.pxl[im][iy_1].ch[ix];
                    tmp += ois->im.pxl[im][iy1].ch[ix1];
                    tmp += ois->im.pxl[im][iy1].ch[ix_1];
                    tmp += ois->im.pxl[im][iy_1].ch[ix_1];
                    tmp += ois->im.pxl[im][iy_1].ch[ix1];
                }
             tmp /= (nfi > 0) ? 9*nfi : 0;
             itmp = (int)(0.5 + tmp);
             for (im = 0; im < nfi; im++)
                {
                    pval = 128 + ois->im.pxl[im][iy].ch[ix] - itmp;
                    pval = (pval < 0) ? 0 : pval;
                    oid->im.pxl[im][i].ch[j] = (pval < 255) ? pval : 255;
                }
          }
     }
   inherit_from_im_to_im(oid, ois);
   return oid;
}



O_i *extract_small_image_in_movie(O_i *dest, int nx, int ny, O_i *ois, int x_c, int y_c)
{
  int nfi, nfo, im;
  int x0, y0, onx, ony, odnx, odny;

  if (ois == NULL) return NULL;
  onx = ois->im.nx;	ony = ois->im.ny;
  nfi = abs(ois->im.n_f);
  nfi = (nfi == 0) ? 1 : nfi;
  if (nx <= 1 || ny <= 1) return NULL;
  if (dest != NULL)
    {
      odnx = dest->im.nx;	odny = dest->im.ny;
      nfo = abs(dest->im.n_f);
      nfo = (nfo == 0) ? 1 : nfo;
      if (odnx != nx || odny != ny || nfi != nfo) return NULL;
      if (dest->im.data_type != ois->im.data_type) return NULL;
    }
  if (dest == NULL)
    {
      if (nfi < 2) dest = create_one_image(nx, ny, ois->im.data_type);
      else         dest = create_one_movie(nx, ny, ois->im.data_type, nfi);
      if (dest == NULL)
	{
	  win_printf("Can't create dest image");
	  return NULL;
	}
    }
  
  for (im = 0; im < nfi; im++)
    {
      if (nfi > 1)
	{
	  switch_frame(ois,im);
	  switch_frame(dest,im);
	}
      x0 = x_c-nx/2;
      x0 = (x0 < 0) ? 0 : x0;
      x0 = (x0 + nx < onx) ? x0 : onx - nx - 1;
      y0 = y_c-ny/2;
      y0 =  (y0 < 0) ? 0 : y0;
      y0 = (y0 + ny < ony) ? y0 : ony - ny - 1;

      copy_one_subset_of_image_in_a_bigger_one(dest, 0, 0, ois, x0, y0, nx, ny);
    }
  return dest;
}




int do_HyperFocus_image_grab_small_movie_flatten(void)
{
  O_i *ois, *oid;
  imreg *imr;
  int i;
  static int nx = 8, ny = 8, x_c = 10, y_c = 20, col = 1;


  if(updating_menu_state != 0)	return D_O_K;

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the image intensity"
			   "by a user defined scalar factor");
    }
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      win_printf("Cannot find image");
      return D_O_K;
    }
  i = win_scanf("define the small image size %5dx%5d\n"
		"Center %5d %5d\n%R-> use R component\n"
                "%r-> use G component\n%r-> use B component\n"
                ,&nx, &ny, &x_c, &y_c, &col);
  if (i == WIN_CANCEL)	return D_O_K;

  oid = remove_in_RGB_image_dc_from_profile(ois, col, x_c, y_c, nx, ny);
  if (oid == NULL)	return win_printf_OK("unable to create image!");
  add_image(imr, oid->type, (void*)oid);
  find_zmin_zmax(oid);
  return (refresh_image(imr, imr->n_oi - 1));
}


int do_HyperFocus_image_grab_small_movie(void)
{
  O_i *ois, *oid, *oid2;
  imreg *imr;
  int i, nfi;
  static int nx = 8, ny = 8, x_c = 10, y_c = 20, hp = 0;


  if(updating_menu_state != 0)	return D_O_K;

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the image intensity"
			   "by a user defined scalar factor");
    }
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      win_printf("Cannot find image");
      return D_O_K;
    }
  i = win_scanf("define the small image size %5dx%5d\n"
		"Center %5d %5d\n%b High Pass Filter\n",&nx, &ny, &x_c, &y_c, &hp);
  if (i == WIN_CANCEL)	return D_O_K;
  oid = extract_small_image_in_movie(NULL, nx, ny, ois, x_c, y_c);
  if (oid == NULL)	return win_printf_OK("unable to create image!");
  add_image(imr, oid->type, (void*)oid);
  find_zmin_zmax(oid);
  if (hp)
    {
      nfi = abs(ois->im.n_f);
      nfi = (nfi == 0) ? 1 : nfi;
      oid2 = create_one_movie(nx, ny, IS_FLOAT_IMAGE, nfi);
      if (oid2 == NULL)	return win_printf_OK("unable to create image!");
      for (i = 0; i < nfi; i++)
	{
	  switch_frame(oid,i);
	  switch_frame(oid2,i);
	  HP_image(oid2, oid);
	}
      add_image(imr, oid2->type, (void*)oid2);
      find_zmin_zmax(oid2);
    }
  return (refresh_image(imr, imr->n_oi - 1));
}



int do_HyperFocus_image_grab_small_movie_correlate_n_n_1(void)
{
    O_i *ois, *oid, *apo = NULL;
    imreg *imr;
    int i, nfi;
    static int nx = 32, ny = 32, x_c = 10, y_c = 20, col = 1;
    static int lc = 4, lw = 2, hc = 10, hw = 4, norm = 0, fir = 16;


    if(updating_menu_state != 0)	return D_O_K;

    if (key[KEY_LSHIFT])
      {
	return win_printf_OK("This routine multiply the image intensity"
			     "by a user defined scalar factor");
      }
    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
      {
	win_printf("Cannot find image");
	return D_O_K;
      }
    nfi = abs(ois->im.n_f);
    nfi = (nfi == 0) ? 1 : nfi;
    
    if (is_oi_a_color_image(ois))
      {
	i = win_scanf("define the small image size %5dx%5d\n"
		      "Center %5d %5d\n"
		      "Select color to filter:\n"
		      "%R-> use R component\n"
		      "%r-> use G component\n"
		      "%r-> use B component\n"
		      "Band pass filter:\n"
		      "Low pass cutoff frequency %4d width %4d\n"
		      "high pass cutoff frequency %4d width %4d\n"
		      "%b Normalize\nAverage size for profile %4d\n"
		      ,&nx, &ny, &x_c, &y_c,&col, &lc,&lw,&hc,&hw,&norm,&fir);
	if (i == WIN_CANCEL)	return D_O_K;
	oid = extract_small_image_in_movie(NULL, nx, ny, ois, x_c, y_c);
	if (oid == NULL)	return win_printf_OK("unable to create image!");
	if (col == 0) oid->im.mode = R_LEVEL;
	else if (col == 1) oid->im.mode = G_LEVEL;
	else oid->im.mode = B_LEVEL;
      }
    else
      {
	i = win_scanf("define the small image size %5dx%5d\n"
		      "Center %5d %5d\n"
		      "Band pass filter:\n"
		      "Low pass cutoff frequency %4d width %4d\n"
		      "high pass cutoff frequency %4d width %4d\n"
		      "%b Normalize\nAverage size for profile %4d\n"
		      ,&nx, &ny, &x_c, &y_c,&lc,&lw,&hc,&hw,&norm,&fir);
	if (i == WIN_CANCEL)	return D_O_K;
	oid = extract_small_image_in_movie(NULL, nx, ny, ois, x_c, y_c);
	if (oid == NULL)	return win_printf_OK("unable to create image!");
      }
    //set_oi_periodic_in_x_and_y(oid);
    set_oi_not_periodic(oid);
    add_image(imr, oid->type, (void*)oid);
    find_zmin_zmax(oid);
    set_zmin_zmax_values(oid,0,255);
    
    apo = create_appodisation_float_image(oid->im.nx, oid->im.ny, 0.05, 0);
    if (apo == NULL)	return win_printf_OK("Could not create apodisation image!");
    
    
    O_i *oidf =  forward_fftbt_2d_im_appo(NULL, oid, 0, screen, apo, 1);
    if (oidf == NULL)	return win_printf_OK("Could not create image!");
    oidf->height *= 0.5;
    oidf->need_to_refresh = ALL_NEED_REFRESH;
    add_image(imr, oidf->type, (void*)oidf);
    find_zmin_zmax(oidf);
    set_oi_periodic_in_x_and_y(oidf);
    
    O_i *oidfbp = band_pass_filter_fft_2d_im(NULL, oidf,  0, lc,  lw,  hc,  hw, -1, 1);
    if (oidfbp == NULL)	return win_printf_OK("Could not filter image");
    add_image(imr, oidfbp->type, (void*)oidfbp);
    find_zmin_zmax(oidfbp);
    
    O_i *oidfc = corel_2d_movie_in_time(NULL, oidfbp,-1,norm);
    if (oidfc == NULL)	return win_printf_OK("Could not create image!");
    add_image(imr, oidfc->type, (void*)oidfc);
    oidfc->need_to_refresh = ALL_NEED_REFRESH;
    find_zmin_zmax(oidfc);

    O_i *oidc = backward_fftbt_2d_im(NULL, oidfc, 0, screen, .05);
    if (oidc == NULL)	return win_printf_OK("Could not create image!");
    oidc->height *= 2;
    oidc->need_to_refresh = ALL_NEED_REFRESH;
    add_image(imr, oidc->type, (void*)oidc);
    find_zmin_zmax(oidc);
    
    O_i *oidp = shift_2d_im(NULL, oidc);
    if (oidp == NULL)	return win_printf_OK("unable to create image!");
    add_image(imr, oidp->type, (void*)oidp);
    oidp->need_to_refresh = ALL_NEED_REFRESH;
    find_zmin_zmax(oidp);
    
    O_p* op = create_and_attach_op_to_oi(oidp, nfi-1, nfi-1, 0, 0);
    if (op == NULL)	return win_printf_OK("Could not create plot data");
    d_s *ds = op->dat[0];
    int im;
    for (im = 0; im < nfi-1; im++)
      {
	switch_frame(oidc,im);
	ds->xd[im] = im;
	ds->yd[im] = oidc->im.pixel[0].fl[0];
	ds->yd[im] += oidc->im.pixel[0].fl[1];
	ds->yd[im] += oidc->im.pixel[1].fl[1];
	ds->yd[im] += oidc->im.pixel[1].fl[0];
	ds->yd[im] += oidc->im.pixel[0].fl[nx-1];
	ds->yd[im] += oidc->im.pixel[1].fl[nx-1];
	ds->yd[im] += oidc->im.pixel[ny-1].fl[nx-1];
	ds->yd[im] += oidc->im.pixel[ny-1].fl[0];
	ds->yd[im] += oidc->im.pixel[ny-1].fl[1];
      }
    d_s *dsm = duplicate_data_set(ds, NULL);
    add_one_plot_data(op, IS_DATA_SET, (void*)dsm);
    for (im = 0; im < dsm->nx-fir; im++)
      {
	for (i = 0, dsm->xd[im] = dsm->yd[im] = 0; i < fir; i++)
	  {
	    dsm->xd[im] += ds->xd[im+i];
	    dsm->yd[im] += ds->yd[im+i];
	  }
	dsm->xd[im] /= (fir > 0) ? fir : 1;
	dsm->yd[im] /= (fir > 0) ? fir : 1;
      }
    dsm->nx = dsm->ny = im;
    return (refresh_image(imr, imr->n_oi - 1));
}

int do_HyperFocus_image_scan_grab_small_movie_correlate_n_n_1(void)
{
  O_i *ois = NULL;    // the initial movie for hyperfocus
  O_i *oid = NULL;    // A small movie corresponding to a small image
  O_i *oid2 = NULL;   // the image containing the index of the good focus 
  O_i *oid2a = NULL;  // the image containing the amplitude of the good focus 
  O_i *oif = NULL;    // the final hyperfocus image
  O_i *apo = NULL;    // the apodisation pattern
  imreg *imr = NULL;
  int i, nfi, onx, ony, im, imax, nxe, nye;
  float max;
  static int nx = 64, ny = 64, x_c = 10, y_c = 20, col = 1;
  static int lc = 6, lw = 3, hc = 30, hw = 8, norm = 0, fir = 20;
  O_i *oidf = NULL;   // the forward fft of oid  
  O_i *oidfbp = NULL; // the bandpass of oidf
  O_i *oidfc = NULL;  // the correlation of oidfbp
  O_i *oidc = NULL;   // the fft-1 de oidfc
  O_i *oim = NULL;    // the image to compute the average in z
  double val[4] = {0};

  if(updating_menu_state != 0)	return D_O_K;

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the image intensity"
			   "by a user defined scalar factor");
    }
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      win_printf("Cannot find image");
      return D_O_K;
    }
  nfi = abs(ois->im.n_f);
  nfi = (nfi == 0) ? 1 : nfi;
  onx = ois->im.nx;
  ony = ois->im.ny;
  
  if (is_oi_a_color_image(ois))
    {
      i = win_scanf("define the small image size %5dx%5d\n"
		    "Select color to filter:\n"
		    "%R-> use R component\n"
		    "%r-> use G component\n"
		    "%r-> use B component\n"
		    "Band pass filter:\n"
		    "Low pass cutoff frequency %4d width %4d\n"
		    "high pass cutoff frequency %4d width %4d\n"
		    "%b Normalize\n"
		    "Average size for profile %4d\n"
		    ,&nx, &ny,&col, &lc,&lw,&hc,&hw,&norm,&fir);
      if (i == WIN_CANCEL)	return D_O_K;
    }
  else
    {
      i = win_scanf("define the small image size %5dx%5d\n"
		    "Band pass filter:\n"
		    "Low pass cutoff frequency %4d width %4d\n"
		    "high pass cutoff frequency %4d width %4d\n"
		    "%b Normalize\n"
		    "Average size for profile %4d\n"
		    ,&nx, &ny,&lc,&lw,&hc,&hw,&norm,&fir);
      if (i == WIN_CANCEL)	return D_O_K;
    }

    nxe = (2*onx/nx);
    nxe += ((2*onx - nxe*nx) > 0) ? 1 : 0;
    nye = (2*ony/ny);
    nye += ((2*ony - nye*ny) > 0) ? 1 : 0;
    
    oid2 = create_and_attach_oi_to_imr(imr,nxe,nye,(nfi < 256) ? IS_CHAR_IMAGE : IS_UINT_IMAGE);
    if (oid2 == NULL)	return win_printf_OK("Could not create image");

    oid2a = create_and_attach_oi_to_imr(imr,nxe,nye,IS_FLOAT_IMAGE);
    if (oid2a == NULL)	return win_printf_OK("Could not create image");
    
    oif = create_and_attach_oi_to_imr(imr,onx,ony,IS_FLOAT_IMAGE);
    if (oif == NULL)	return win_printf_OK("Could not create image");

    smart_map_pixel_ratio_of_image_and_screen(oif);
    set_zmin_zmax_values(oif, 0, fir*255);
    set_z_black_z_white_values(oif, 0, fir*255);
    
    int ix, iy, dxs, dxe, dys, dye;
    d_s *ds = build_data_set(nfi-1, nfi-1);
    d_s *dsm = build_data_set(nfi-1, nfi-1);
    if (ds == NULL || dsm == NULL)
      return win_printf_OK("Could not create plot data");
    for (iy = 0, y_c = ny/2; y_c < ony && iy < nye; iy++, y_c = ny/2 + ony*iy/nye)
      {
	for (ix = 0, x_c = nx/2; x_c < onx && ix < nxe; ix++, x_c = nx/2 + onx*ix/nxe)
	  {
	    oid = extract_small_image_in_movie(oid, nx, ny, ois, x_c, y_c);
	    if (oid == NULL)	return win_printf_OK("unable to create image!");
	    if (is_oi_a_color_image(ois))
	      {
		if (col == 0) oid->im.mode = R_LEVEL;
		else if (col == 1) oid->im.mode = G_LEVEL;
		else oid->im.mode = B_LEVEL;
	      }
	    //set_oi_periodic_in_x_and_y(oid);
	    set_oi_not_periodic(oid);
	    if (apo == NULL)
	      {
		apo = create_appodisation_float_image(oid->im.nx, oid->im.ny, 0.05, 0);
		if (apo == NULL)	return win_printf_OK("Could not create apodisation image!");
	      }
	    oidf =  forward_fftbt_2d_im_appo(oidf, oid, 0, screen, apo, 1);
	    if (oidf == NULL)	return win_printf_OK("Could not create image!");
	    oidfbp = band_pass_filter_fft_2d_im(oidfbp, oidf,  0, lc,  lw,  hc,  hw, -1, 1);
	    if (oidfbp == NULL)	return win_printf_OK("Could not filter image");
	    
	    oidfc = corel_2d_movie_in_time(oidfc, oidfbp, -1, norm);
	    if (oidfc == NULL)	return win_printf_OK("Could not create image!");
	    oidc = backward_fftbt_2d_im(oidc, oidfc, 0, screen, .05);
	    for (im = 0; im < nfi-1; im++)
	      {
		switch_frame(oidc,im);
		ds->xd[im] = im;
		ds->yd[im] = oidc->im.pixel[0].fl[0];
		ds->yd[im] += oidc->im.pixel[0].fl[1];
		ds->yd[im] += oidc->im.pixel[1].fl[1];
		ds->yd[im] += oidc->im.pixel[1].fl[0];
		ds->yd[im] += oidc->im.pixel[0].fl[nx-1];
		ds->yd[im] += oidc->im.pixel[1].fl[nx-1];
		ds->yd[im] += oidc->im.pixel[ny-1].fl[nx-1];
		ds->yd[im] += oidc->im.pixel[ny-1].fl[0];
		ds->yd[im] += oidc->im.pixel[ny-1].fl[1];
	      }
	    for (im = imax = 0; im < ds->nx-fir; im++)
	      {
		for (i = 0, dsm->xd[im] = dsm->yd[im] = 0; i < fir; i++)
		  {
		    dsm->xd[im] += ds->xd[im+i];
		    dsm->yd[im] += ds->yd[im+i];
		  }
		dsm->xd[im] /= (fir > 0) ? fir : 1;
		dsm->yd[im] /= (fir > 0) ? fir : 1;
		if (im == 0 || dsm->yd[im] > max)
		  {
		    max = dsm->yd[im];
		    imax = (int)(0.5 + dsm->xd[im]);
		  }
	      }
	    //dsm->nx = dsm->ny = im;
	    val[0] = imax;
	    set_raw_pixel_value(oid2, (2*x_c)/nx, (2*y_c)/ny, val);
	    val[0] = max;
	    set_raw_pixel_value(oid2a, (2*x_c)/nx, (2*y_c)/ny, val);
	    my_set_window_title("Treating region in xc %d in yc %d imax at %d val %g", x_c, y_c,imax, max);
	    oim = average_partial_movie(oid, oim, imax-fir/2, fir);
	    switch_frame(oid,imax);
	    dxs = (ix == 0) ? 0 :nx/4;
	    dxe = (ix == nxe - 1) ? nx : 3*nx/4;
	    dys = (iy == 0) ? 0 :ny/4;
	    dye = (iy == nye - 1) ? ny : 3*ny/4;	    	    
	    copy_one_subset_of_image_in_a_bigger_one(oif, x_c - nx/2, y_c - ny/2, oim, dxs, dys, dxe,  dye);
	    oif->need_to_refresh = ALL_NEED_REFRESH;
	    if (ix%8 == 0) refresh_image(imr, imr->n_oi - 1);
	  }
	
      }
    free_one_image(oid);
    free_one_image(oidf);
    free_one_image(oidfbp);
    free_one_image(oidfc);
    free_one_image(oidc);
    free_one_image(oim);
    return (refresh_image(imr, imr->n_oi - 1));
}

# ifdef KEEP

int do_HyperFocus_image_scan_grab_small_movie_correlate_n_n_1(void)
{
  O_i *ois, *oid = NULL, *oid2, *oid2a, *oif;
  imreg *imr;
  int i, nfi, onx, ony, im, imax, nxe, nye;
  float max, tmp;
  static int nx = 32, ny = 32, x_c = 10, y_c = 20, col = 1;
  static int lc = 4, lw = 2, hc = 10, hw = 4, norm = 0;
  O_i *oidf = NULL;
  O_i *oidfbp = NULL;
  O_i *oidfc = NULL;
  O_i *oidc = NULL;
  double val[4];

  if(updating_menu_state != 0)	return D_O_K;

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the image intensity"
			   "by a user defined scalar factor");
    }
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      win_printf("Cannot find image");
      return D_O_K;
    }
  nfi = abs(ois->im.n_f);
  nfi = (nfi == 0) ? 1 : nfi;
  onx = ois->im.nx;
  ony = ois->im.ny;
  i = win_scanf("define the small image size %5dx%5d\n"
                "Select color to filter:\n"
                "%R-> use R component\n"
                "%r-> use G component\n"
                "%r-> use B component\n"
                "Band pass filter:\n"
                "Low pass cutoff frequency %4d width %4d\n"
                "high pass cutoff frequency %4d width %4d\n"
                "%b Normalize\n",&nx, &ny,&col, &lc,&lw,&hc,&hw,&norm);
  if (i == WIN_CANCEL)	return D_O_K;

  oid2 = create_and_attach_oi_to_imr(imr,(int)(0.5+(float)onx/nx),(int)(0.5+(float)ony/ny),(nfi < 256) ? IS_CHAR_IMAGE : IS_UINT_IMAGE);
  if (oid2 == NULL)	return win_printf_OK("Could not create image");

  oid2a = create_and_attach_oi_to_imr(imr,(int)(0.5+(float)onx/nx),(int)(0.5+(float)ony/ny),IS_FLOAT_IMAGE);
  if (oid2a == NULL)	return win_printf_OK("Could not create image");

  oif = create_and_attach_oi_to_imr(imr,onx,ony,ois->im.data_type);
  if (oif == NULL)	return win_printf_OK("Could not create image");

  nxe = (onx/nx);
  nxe += ((onx - nxe*nx) > 0) ? 1 : 0;
  nye = (ony/ny);
  nye += ((ony - nye*ny) > 0) ? 1 : 0;
  int ix, iy;
  //d_s *ds = build_data_set(nfi-1, nfi-1);
  //if (ds == NULL)	return win_printf_OK("Could not create plot data");
  for (iy = 0, y_c = ny/2; y_c < ony && iy < nye; iy++, y_c = ny/2 + ony*iy/nye)
     {
         for (ix = 0, x_c = nx/2; x_c < onx && ix < nxe; ix++, x_c = nx/2 + onx*ix/nxe)
            {

               oid = extract_small_image_in_movie(oid, nx, ny, ois, x_c, y_c);
               if (oid == NULL)	return win_printf_OK("unable to create image!");
               if (col == 0) oid->im.mode = R_LEVEL;
               else if (col == 1) oid->im.mode = G_LEVEL;
               else oid->im.mode = B_LEVEL;
               set_oi_periodic_in_x_and_y(oid);

               oidf = forward_fftbt_2d_im(oidf, oid, 0, screen, .05);
               if (oidf == NULL)	return win_printf_OK("Could not create image!");
               oidfbp = band_pass_filter_fft_2d_im(oidfbp, oidf,  0, lc,  lw,  hc,  hw, -1, 1);
               if (oidfbp == NULL)	return win_printf_OK("Could not filter image");

               oidfc = corel_2d_movie_in_time(oidfc, oidfbp, -1, norm);
               if (oidfc == NULL)	return win_printf_OK("Could not create image!");
               oidc = backward_fftbt_2d_im(oidc, oidfc, 0, screen, .05);
               for (im = imax = 0; im < nfi-1; im++)
                 {
                      switch_frame(oidc,im);
                      //s->xd[im] = im;
                      tmp = oidc->im.pixel[0].fl[0];
                      tmp += oidc->im.pixel[0].fl[1];
                      tmp += oidc->im.pixel[1].fl[1];
                      tmp += oidc->im.pixel[1].fl[0];
                      tmp += oidc->im.pixel[0].fl[nx-1];
                      tmp += oidc->im.pixel[1].fl[nx-1];
                      tmp += oidc->im.pixel[ny-1].fl[nx-1];
                      tmp += oidc->im.pixel[ny-1].fl[0];
                      tmp += oidc->im.pixel[ny-1].fl[1];
                      if (im == 0 || tmp > max)
                        {
                            max = tmp;
                            imax = im;
                        }
                 }
               val[0] = imax;
               set_raw_pixel_value(oid2, x_c/nx, y_c/ny, val);
               val[0] = max;
               set_raw_pixel_value(oid2a, x_c/nx, y_c/ny, val);
               switch_frame(oid,imax);
               copy_one_subset_of_image_in_a_bigger_one(oif, x_c - nx/2, y_c - ny/2, oid, 0, 0, nx,  ny);

            }
     }
  return (refresh_image(imr, imr->n_oi - 1));
}

# endif

int do_HyperFocus_by_sigma(void)
{
  O_i *ois, *oim = NULL, *oid = NULL;
  O_p *op = NULL;
  //d_s *ds = NULL;
  imreg *imr;
  int i, j, nfi, onx, ony, x0, y0, maxi, im;
  static int nx = 8, ny = 8;
  double mean, sigma, max, val;


  if(updating_menu_state != 0)	return D_O_K;

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the image intensity"
			   "by a user defined scalar factor");
    }
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      win_printf("Cannot find image");
      return D_O_K;
    }
  i = win_scanf("define the small image size %5dx%5d\n",&nx, &ny);
  if (i == WIN_CANCEL)	return D_O_K;
  if (ois == NULL) return 1;
  onx = ois->im.nx;	ony = ois->im.ny;
  nfi = abs(ois->im.n_f);
  nfi = (nfi == 0) ? 1 : nfi;

  oim = extract_small_image_in_movie(oim, nx, ny, ois, 0, 0);
  if (oim == NULL)	return win_printf_OK("unable to create image!");
  if (op == NULL)
    {
      op = create_and_attach_op_to_oi(ois, nfi, nfi, 0, 0);
      if (op == NULL)	return win_printf_OK("Could not create plot data");
      //ds = op->dat[0];
    }
  if (oid == NULL)  oid = create_one_image(onx, ony, ois->im.data_type);


  for (y0 = 0; y0 < ony ; y0 += nx)
    {
      for (x0 = 0; x0 < onx; x0 += ny)
	{
	  oim = extract_small_image_in_movie(oim, nx, ny, ois, x0, y0);
	  for (im = 0, maxi = 0, max = 0; im < nfi; im++)
	    {
	      switch_frame(oim,im);
	      for (i = 0, mean = 0; i < ny ; i++)
		{
		  for (j=0; j< nx; j++)
		    {
		      get_raw_pixel_value(oim, j, i, &val);
		      mean += val;
		    }
		}
	      mean /= nx*ny;
	      for (i = 0, sigma = 0; i < ny ; i++)
		{
		  for (j=0; j< nx; j++)
		    {
		      get_raw_pixel_value(oim, j, i, &val);
		      sigma += (val - mean)*(val-mean);
		    }
		}
	      sigma /= (nx*ny - 1);
	      if (sigma > max)
		{
		  max = sigma;
		  maxi = im;
		}
	    }
	  switch_frame(oim,maxi);
	  copy_one_subset_of_image_in_a_bigger_one(oid, x0, y0, oim, 0, 0, nx,  ny);
	}
    }

  add_image(imr, oid->type, (void*)oid);
  find_zmin_zmax(oid);
  return (refresh_image(imr, imr->n_oi - 1));
}


int find_RGB_image_green_vs_red_affine_param(O_i *ois, double *a1, double *a0)
{
   d_s *ds = NULL, *dsf = NULL;
   int nfi, onx, ony, x0, y0;
   double out_a = 0, out_b = 0;

   if (ois->im.data_type != IS_RGB_PICTURE) return 1;

   onx = ois->im.nx;	ony = ois->im.ny;
   nfi = onx * ony;
   ds = build_data_set(nfi, nfi);
   if (ds == NULL)	return 2;
   ds->nx = ds->ny = 0;
   for (y0 = 0; y0 < ony ; y0++)
     {
       for (x0 = 0; x0 < onx; x0++)
         add_new_point_to_ds(ds, ois->im.pixel[y0].rgb[x0].r, ois->im.pixel[y0].rgb[x0].g);
     }
   dsf = least_square_fit_on_ds_between_indexes(ds, true, 0, ds->nx, &out_a, &out_b);
   if (dsf == NULL) return 3;
   if (a1) *a1 = out_a;
   if (a0) *a0 = out_b;
   free_data_set(dsf);
   return 0;
}

int do_RGB_image_green_vs_red_plot(void)
{
   O_i *ois;
   O_p *op = NULL;
   d_s *ds = NULL;
   imreg *imr;
   int nfi, onx, ony, x0, y0;

  if(updating_menu_state != 0)	return D_O_K;

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the image intensity"
			   "by a user defined scalar factor");
    }
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      win_printf("Cannot find image");
      return D_O_K;
    }
  if (ois->im.data_type != IS_RGB_PICTURE)
          return win_printf("This routine haadles RVB picture only!");


  onx = ois->im.nx;	ony = ois->im.ny;
  nfi = onx * ony;
  op = create_and_attach_op_to_oi(ois, nfi, nfi, 0, 0);
  if (op == NULL)	return win_printf_OK("Could not create plot data");
  ds = op->dat[0];
  ds->nx = ds->ny = 0;
  for (y0 = 0; y0 < ony ; y0++)
    {
      for (x0 = 0; x0 < onx; x0++)
        add_new_point_to_ds(ds, ois->im.pixel[y0].rgb[x0].r, ois->im.pixel[y0].rgb[x0].g);
    }
  set_plot_title(op, "Green versus Red");
  /*
  double a1, a0;
  find_RGB_image_green_vs_red_affine_param(ois, &a1, &a0);
  win_printf("Affine values a = %g b = %g",a1,a0);
  */
  return refresh_image(imr, UNCHANGED);
}


int do_convert_RGB_image_in_BW_green_minus_red(void)
{
   O_i *ois, *oid = NULL, *oidf = NULL, *oidfc = NULL;
   imreg *imr;
   int i, nfi, onx, ony, x0, y0, im, d2;
   static int avg = 0;

  if(updating_menu_state != 0)	return D_O_K;

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the image intensity"
			   "by a user defined scalar factor");
    }
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      win_printf("Cannot find image");
      return D_O_K;
    }
  if (ois->im.data_type != IS_RGB_PICTURE)
          return win_printf("This routine haadles RVB picture only!");


  i = win_scanf("%b 2x2 avg ?",&avg);
  if (i == WIN_CANCEL) return 0;
  d2 = (avg) ? 2 : 1;
  onx = ois->im.nx;	ony = ois->im.ny;
  nfi = abs(ois->im.n_f);
  nfi = (nfi == 0) ? 1 : nfi;
  oid = create_and_attach_movie_to_imr(imr, onx/d2, ony/d2, IS_FLOAT_IMAGE, nfi);
  if (oid == NULL)	return win_printf_OK("Could not BW movie");
  oidf = create_and_attach_movie_to_imr(imr, onx/d2, ony/d2, IS_FLOAT_IMAGE, nfi);
  if (oidf == NULL)	return win_printf_OK("Could not BW movie");
  oidfc = create_and_attach_movie_to_imr(imr, onx/d2, ony/d2, IS_FLOAT_IMAGE, nfi);
  if (oidfc == NULL)	return win_printf_OK("Could not BW movie");

  double a1, a0, tmp;
  for (im = 0; im < nfi; im++)
    {
      switch_frame(ois,im);
      switch_frame(oid,im);
      switch_frame(oidf,im);
      switch_frame(oidfc,im);
      find_RGB_image_green_vs_red_affine_param(ois, &a1, &a0);
      for (y0 = 0; y0 < ony ; y0++)
        {
           for (x0 = 0; x0 < onx; x0++)
             {
                tmp = ois->im.pixel[y0].rgb[x0].g - a0 - a1*ois->im.pixel[y0].rgb[x0].r;
                tmp += 128;
                tmp = (tmp <= 0) ? 0 : tmp;
                tmp = (tmp > 255) ? 255 : tmp;
                oid->im.pixel[y0/d2].fl[x0/d2] += tmp;         
             }
        }
      HP_image(oidf, oid);
      Correlation_pixel_pixel_1_image(oidfc, oidf);
    }
  find_zmin_zmax(oid);
  find_zmin_zmax(oidf);
  find_zmin_zmax(oidfc);
  inherit_from_im_to_im(oidf, oid);
  inherit_from_im_to_im(oidfc, oidf);
  return (refresh_image(imr, imr->n_oi - 1));
}

int do_HyperFocus_by_HP_filter(void)
{
  O_i *ois, *oim = NULL, *oid = NULL, *oif = NULL, *oih = NULL;
  O_p *op = NULL;
  //d_s *ds = NULL;
  imreg *imr;
  int i, j, nfi, onx, ony, x0, y0, maxi, im;
  static int nx = 8, ny = 8;
  double mean, sigma, max, val;


  if(updating_menu_state != 0)	return D_O_K;

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the image intensity"
			   "by a user defined scalar factor");
    }
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      win_printf("Cannot find image");
      return D_O_K;
    }
  i = win_scanf("define the small image size %5dx%5d\n",&nx, &ny);
  if (i == WIN_CANCEL)	return D_O_K;
  if (ois == NULL) return 1;
  onx = ois->im.nx;	ony = ois->im.ny;
  nfi = abs(ois->im.n_f);
  nfi = (nfi == 0) ? 1 : nfi;

  oim = extract_small_image_in_movie(oim, nx, ny, ois, 0, 0);
  if (oim == NULL)	return win_printf_OK("unable to create image!");
  if (op == NULL)
    {
      op = create_and_attach_op_to_oi(ois, nfi, nfi, 0, 0);
      if (op == NULL)	return win_printf_OK("Could not create plot data");
      //ds = op->dat[0];
    }
  if (oid == NULL)  oid = create_one_image(onx, ony, ois->im.data_type);
  oih = create_one_image(onx, ony, IS_FLOAT_IMAGE);
  oif = create_one_image(nx, ny, IS_FLOAT_IMAGE);
  if (oif == NULL) return 1;
  for (y0 = 0; y0 < ony ; y0 += nx)
    {
      for (x0 = 0; x0 < onx; x0 += ny)
	{
	  oim = extract_small_image_in_movie(oim, nx, ny, ois, x0, y0);
	  for (im = 0, maxi = 0, max = 0; im < nfi; im++)
	    {
	      switch_frame(oim,im);
              HP_image(oif, oim);
              for (i = 0, mean = 0; i < ny ; i++)
        	{
	          for (j=0; j< nx; j++)
		    {
                      mean += oif->im.pixel[i].fl[j];
                    }
	        }
              mean /= onx*ony;
              for (i = 0, sigma = 0; i < ny ; i++)
	        {
	          for (j=0; j< nx; j++)
	           {
	             val = oif->im.pixel[i].fl[j];
	             sigma += (val - mean)*(val-mean);
	          }
	       }
             sigma /= (nx*ny - 1);
	      if (sigma > max)
		{
		  max = sigma;
		  maxi = im;
		}
	    }
	  switch_frame(oim,maxi);
          oih->im.pixel[y0].fl[x0] = (float)maxi;
	  copy_one_subset_of_image_in_a_bigger_one(oid, x0, y0, oim, 0, 0, nx,  ny);
	}
    }
  free_one_image(oif);
  add_image(imr, oid->type, (void*)oid);
  find_zmin_zmax(oid);
  add_image(imr, oih->type, (void*)oih);
  find_zmin_zmax(oih);
  return (refresh_image(imr, imr->n_oi - 1));
}

int do_find_best_z_by_cutting_in_small_parts(void)
{
    imreg *imr = NULL;
    O_i *ois = NULL; //input big movie
    O_i *oim = NULL; // small subset of movie to be treated
    O_i *oip = NULL; // Orthogonal polynomial movie
    O_i *oidp = NULL; // small image minus orthogonal poynomial
    O_i *apo = NULL; // appodisation image
    O_i *oid = NULL; // appodised movie
    O_i *oidf = NULL; // FFT movie
    O_i *oidz = NULL; // image containing dz
    O_i *oiz = NULL; // image containing z index   
    O_i *oitmp = NULL; // pointer on image to copy
    O_i *oidu = NULL;  // undersample image
    O_i *oimaxq = NULL; // image containing the quality of max
    O_i *oimaxqf = NULL; // image containing the quality of max filtered
    O_i *oimax4 = NULL; // image containing the quality of negative sigma4        
    O_i *oidc = NULL; // correlation image
    O_i *oibf = NULL; // image containing band pass
    O_i *oibfc = NULL; // image containing band pass copy    
    O_i *oiq = NULL; // image with wuality of the max
    O_i *oire = NULL; //movie final with homogeneous focus    
    int i, j, k, x = 0, y = 0, nf, nfi, nxs = 0, nys = 0, im, X0 = 0, X1 = 0, X_1 = 0, Y0 = 0, Y1 = 0, Y_1 = 0;
    int sizex = 0, sizey = 0;
    int jx, jy;

    float max_val = 0, x_fmax = 0, y_fmax = 0, max_val_bg, max_z2_bg, fmax, tmp;
    static int size = 128, norm = 0;
    static unsigned long t_c, to = 0;
    static int keep_dc = 1, sub = 1, bin = 1, yonly = 0, treat0 = -1, keepi = -1, zavg = 32;
    static float sm = 0.0;
    static int useBP = 1, lp = 6, lw = 4, hp = 64, hw = 16, startre = 0,sizere = 64;

    O_p *opoi = NULL; // the plot with ds synchrone with movie
    d_s *dsop = NULL;
    d_s *dsop2 = NULL;
    d_s *dsop3 = NULL;        
    double mean, sigma, sigm, sigp;

    
    if(updating_menu_state != 0)	return D_O_K;
    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
    i = win_scanf("This routine will cut an image in smaller ones\n"
                  "with 1/2 covering edge for correlation\n"
                  "define the size of the small image %8d\n"
		  "%R->undersample in X and Y or %r->in Y only\n"
		  "Define your undersampling %4d\n"
		  "%R->projection or %r->substract projection\n"
		  "For appodisation: %bkeep DC component\n"
		  "Decrease appodisation to %6f \n"
		  "Averaging size in Z %5d\n"
		  "%bApply BP filter\n"
		  "If yes LP %4d w %4d HP %4d w %4d\n"
		  "%bNormalize correlation\n"
		  "Reconstruct movie without motion"
		  "starting at frame %4d over %4d\n"
		  "treat only %4d keep image %4d\n"
		  ,&size,&yonly,&bin,&sub,&keep_dc,&sm,&zavg,&useBP,&lp,&lw,&hp,&hw
		  ,&norm,&startre,&sizere,&treat0,&keepi);
    if (i == WIN_CANCEL) return 0;


    if (ois == NULL)      win_printf_OK("No image found");

    nfi = nb_of_frames_in_movie(ois);
    if (nfi < 2)      win_printf_OK("Not a movie %d frames",nfi);    
    nf = compute_nb_of_subset(ois, size, size,&nxs,&nys);
    win_printf("New image %d x %d x %d",nxs,nys,nfi);
    oim = create_and_attach_movie_to_imr(imr, size, size, ois->im.data_type, nfi);
    if (oim == NULL) return win_printf_OK("cannot create tmp movie");
    if (keepi < 0)
      {
	oimaxq = create_and_attach_movie_to_imr(imr, nxs, nys, IS_FLOAT_IMAGE, nfi-1);
	if (oimaxq == NULL) return win_printf_OK("cannot create result movie");
	oimaxqf = create_and_attach_movie_to_imr(imr, nxs, nys, IS_FLOAT_IMAGE, nfi-1);
	if (oimaxqf == NULL) return win_printf_OK("cannot create result movie");
	oimax4 = create_and_attach_movie_to_imr(imr, nxs, nys, IS_FLOAT_IMAGE, nfi-1);
	if (oimax4 == NULL) return win_printf_OK("cannot create result movie");    	
	oidz = create_and_attach_movie_to_imr(imr, nxs, nys, IS_FLOAT_IMAGE, nfi-1);
	if (oidz == NULL) return win_printf_OK("cannot create result movie");
	oiz = create_and_attach_oi_to_imr(imr, nxs, nys, IS_INT_IMAGE);
	if (oidz == NULL) return win_printf_OK("cannot create result movie");
	oiq = create_and_attach_oi_to_imr(imr, nxs, nys, IS_FLOAT_IMAGE);    
	if (oiq == NULL) return win_printf_OK("cannot create result movie");
      }
    if (sizere > 0 && keepi < 0)
      {
	oire = create_and_attach_movie_to_imr(imr, ois->im.nx, ois->im.ny, ois->im.data_type, sizere);
	if (oire == NULL) return win_printf_OK("cannot create result movie");
      }
    sizex = (yonly) ? size : size/bin;
    sizey = size/bin;     
    oip = build_ortho_poly2x2(sizex, sizey, NULL);
    if (oip == NULL) return win_printf_OK("cannot build polynomial");    
    apo = create_appodisation_float_image(sizex, sizey, sm, 0);
    if (apo == NULL)	return win_printf_OK("Could not create apodisation image!");

    
    t_c = my_uclock();
    for (j = 0; j < nf; j++)
    {
        X_1 = (j != 0 && x != X_1)? x : X_1;
        Y_1 = (j != 0 && y != Y_1)? y : Y_1;
	find_subset_pos(ois, size, size, j, &x, &y);
	X0 = (j == 0 || x < X0) ? x : X0;
	X1 = (j == 0 || x > X1) ? x : X1;
	Y0 = (j == 0 || y < Y0) ? y : Y0;
	Y1 = (j == 0 || y > Y1) ? y : Y1;		
	if (treat0 >= 0 && treat0 != j) continue;
	if (keepi >= 0 && keepi != j) continue;	
	display_title_message("small image=%d x=%d y=%d",j,x,y);
	for (im = 0; im < nfi; im++)
	  {
	    switch_frame(oim,im);
	    switch_frame(ois,im);
	    copy_one_subset_of_image_and_border(oim, ois, x, y);
	  }
	if (oim == NULL) win_printf("Small movie %p",(void*)oim);
	oitmp = oidu;
	if (j == keepi)
	  add_image(imr, oim->im.data_type, duplicate_image_or_movie(oim, NULL, 0));
	  
	if (bin > 1)
	  {
	    if (yonly) oidu = undersample_image_of_char_in_y(oidu, oim, bin);
	    else oidu = undersample_image_of_char(oidu, oim, bin);
	    if (oitmp != NULL && oitmp != oidu)
	      win_printf("mem leak in undersample");
	    if (oidu == NULL) win_printf("undersampled  %p",(void*)oidu);
	  }
	if (j == keepi && bin > 1)
	  add_image(imr, oidu->im.data_type, duplicate_image_or_movie(oidu, NULL, 0));	
	oitmp = oidp;
	oidp = project_on_first_poly(oidp, (bin > 1) ? oidu : oim, oip, sub, NULL);
	if (j == keepi)
	  add_image(imr, oidp->im.data_type, duplicate_image_or_movie(oidp, NULL, 0)); 
	if (oitmp != NULL && oitmp != oidp)
	  win_printf("mem leak in project");	
	if (oidp == NULL) win_printf("project %p ",(void*)oidp);
	oitmp = oid;
	set_oi_not_periodic(oidp);
	oid = d2_im_appo(oid, oidp, 0, NULL, apo, 1, keep_dc);
	if (j == keepi)
	  add_image(imr, oid->im.data_type, duplicate_image_or_movie(oid, NULL, 0)); 	
	if (oitmp != NULL && oitmp != oid)
	  win_printf("mem leak in apo");
	if (oid == NULL) win_printf("apo %p",(void*)oid);
	set_oi_periodic_in_x_and_y(oid);
	
	oitmp = oidf;
	oidf = forward_fftbt_2d_im(oidf, oid, 0, screen, 0);
	if (j == keepi)
	  add_image(imr, oidf->im.data_type, duplicate_image_or_movie(oidf, NULL, 0)); 		
	if (oitmp != NULL && oitmp != oidf)
	  win_printf("mem leak in fft");	
	if (oidf == NULL) win_printf("Fft movie");
	if (useBP)
	  oidf = band_pass_filter_fft_2d_im(oidf, oidf,  0, lp,  lw,  hp,  hw, 0, 0);
	oidc = corel_2d_movie_in_time(oidc, oidf, -1, norm);
	if (j == keepi)
	  add_image(imr, oidc->im.data_type, duplicate_image_or_movie(oidc, NULL, 0));	  
	oibf = backward_fftbt_2d_im(oibf, oidc, 0, screen, 0);
	oibf = shift_2d_im(oibf, oibf);
	if (j == keepi)
	  add_image(imr, oibf->im.data_type, oibfc = duplicate_image_or_movie(oibf, NULL, 0));
	// we find the max
	if (keepi >= 0)
	  {
	    opoi = create_and_attach_op_to_oi(oibfc, nfi, nfi, 0, 0);
	    if (opoi == NULL)   return win_printf_OK("Could not create tmp opoi!");
	    dsop = opoi->dat[0];
	    if (dsop == NULL)	return win_printf_OK("Could not create tmp dsop!");
	    dsop2 = create_and_attach_one_ds(opoi, nfi, nfi, 0);
	    dsop3 = create_and_attach_one_ds(opoi, nfi, nfi, 0);	    
	  }
	  
	jy = j/nxs;    jx = j%nxs;
	for(im = 0; im < nb_of_frames_in_movie(oibf); im++)
	  {
	    switch_frame(oibf,im);
	    oibf->im.nxs = 3*oibf->im.nx/8;
	    oibf->im.nxe = 5*oibf->im.nx/8;
	    oibf->im.nys = 3*oibf->im.ny/8;
	    oibf->im.nye = 5*oibf->im.ny/8;	    
	    find_zmax_and_pos_interpolate(oibf, &max_val, &x_fmax, &y_fmax);
	    find_zmax_excluding_an_area(oibf, x_fmax, oibf->im.nx/8, y_fmax, oibf->im.ny/8,&max_val_bg, &max_z2_bg);
	    max_z2_bg = 3*sqrt(max_z2_bg);
	    if (oimaxq != NULL)
	      oimaxq->im.pxl[im][jy].fl[jx] = (max_z2_bg > 0) ? max_val/max_z2_bg : max_val;
	    if (dsop != NULL)
	      {
		dsop->xd[im] = im;
		dsop->yd[im] = (max_z2_bg > 0) ? max_val/max_z2_bg : max_val;
	      }
	    switch_frame(oid,im);	    
	    find_image_mean_sigma_sigmap_sigmam(oid, &mean, &sigma, &sigm, &sigp);
	    if (dsop3 != NULL)
	      {
		dsop3->xd[im] = im;
		dsop3->yd[im] = (sigma > 0) ? sigm/(sigma*sigma) : sigm;
	      }
	    if (oimax4 != NULL)
	      oimax4->im.pxl[im][jy].fl[jx] = (sigma > 0) ? sigm/(sigma*sigma) : sigm;
	    
	  }
	for(im = 0; im < nb_of_frames_in_movie(oibf) -zavg; im++)
	  {
	    if (oidz != NULL) oidz->im.pxl[im+zavg/2][jy].fl[jx] = 0;
	    for(k = 0; k < zavg; k++)
	      {
		if (oidz != NULL)
		  oidz->im.pxl[im+zavg/2][jy].fl[jx] += oimaxq->im.pxl[im+k][jy].fl[jx]/zavg;
		if (dsop2 != NULL)
		  {
		    dsop2->xd[im] = im + zavg/2;
		    dsop2->yd[im] += dsop->yd[im+k]/zavg;
		  }
	      }
	  }
	for(im = zavg/2, fmax = 0; im < nb_of_frames_in_movie(oibf) -zavg/2; im++)
	  {
	    tmp = 0;
	    if (oidz != NULL) tmp = oidz->im.pxl[im][jy].fl[jx];
	    else if (dsop2 != NULL) tmp = dsop2->yd[im];
	    if (im == zavg/2 || tmp  > fmax)
	      {
		fmax = tmp;
		if (oiz != NULL)  oiz->im.pixel[jy].in[jx] = im;
		if (oiq != NULL)  oiq->im.pixel[jy].fl[jx] = fmax;
		if (opoi != NULL)
		  set_plot_title(opoi,"Max at %d val %g",im, fmax);	
	      }
	  }
	

    }

    if (oimaxq != NULL && oimaxqf != NULL)
      {
	d_s *ds = NULL;
	int ix, iy;
	ds = build_data_set(nb_of_frames_in_movie(oibf),nb_of_frames_in_movie(oibf));
	for (ix = 0; ix < nb_of_frames_in_movie(oibf); ix++) ds->xd[ix] = ix;
	for (iy = 0; iy < nys; iy++)
	  {
	    for (ix = 0; ix < nxs; ix++)
	      {
		extract_z_profile_from_movie(oimaxq, ix, iy, ds->yd);
		for(i = 0; i < ds->nx; i++)    ds->yd[i] *= ds->yd[i]/5;
		for(i = 0; i < ds->nx; i++)    ds->yd[i] *= ds->yd[i]/5;	    
		if (ix > 0)
		  {
		    extract_z_profile_from_movie(oimaxq, ix-1, iy, ds->xd); 
			for(i = 0; i < ds->nx; i++)    ds->yd[i] *= ds->xd[i]/5;
		  }
		if (ix > 0 && iy > 0)
		  {	    
		    extract_z_profile_from_movie(oimaxq, ix-1, iy-1, ds->xd);
		    for(i = 0; i < ds->nx; i++)    ds->yd[i] *= ds->xd[i]/5;
		  }
		if (ix > 0 && iy < nys-1)
		  {	    
		    extract_z_profile_from_movie(oimaxq, ix-1, iy+1, ds->xd);
		    for(i = 0; i < ds->nx; i++)    ds->yd[i] *= ds->xd[i]/5;
		  }
		if (ix > nxs-1)
		  {	    	    
		    extract_z_profile_from_movie(oimaxq, ix+1, iy, ds->xd);	    
		    for(i = 0; i < ds->nx; i++)    ds->yd[i] *= ds->xd[i]/5;
		  }
		if (ix > nxs-1 && iy > 0)
		  {	    	    	    
		    extract_z_profile_from_movie(oimaxq, ix+1, iy-1, ds->xd);	    
		    for(i = 0; i < ds->nx; i++)    ds->yd[i] *= ds->xd[i]/5;
		  }
		if (ix > nxs-1 && iy < nys-1)
		  {	    	    	    
		    extract_z_profile_from_movie(oimaxq, ix+1, iy+1, ds->xd);
		    for(i = 0; i < ds->nx; i++)    ds->yd[i] *= ds->xd[i]/5;
		  }
		if (iy < nys-1)
		  {	    	    	    	    
		    extract_z_profile_from_movie(oimaxq, ix, iy+1, ds->xd);
		    for(i = 0; i < ds->nx; i++)    ds->yd[i] *= ds->yd[i]/5;
		  }
		if (iy > 0)
		  {	    	    	    	    
		    extract_z_profile_from_movie(oimaxq, ix, iy-1, ds->xd);
		    for(i = 0; i < ds->nx; i++)    ds->yd[i] *= ds->xd[i]/5;
		  }
		for(i = 0; i < ds->nx; i++) 		    
		  oimaxqf->im.pxl[i][iy].fl[ix] = ds->yd[i];
	      }
	  }
	free_data_set(ds);
      }
    

    
    //we create movie around good focus
    float xp, yp;
    int s_8 = size/8, s_2 = size/2, s_3 = (3*size)/4, ki, jx1, jy1, s_ys, s_ye, s_xs, s_xe;
    

    for (j = 0; keepi < 0 && j < nf; j++)
      {
	find_subset_pos(ois, size, size, j, &x, &y);
	jy1 = j/nxs;
	jx1 = j%nxs;
	s_ys = (jy1 == 0) ? 0 : s_8;
	s_ye = (jy1 == nys-1) ? size : size - s_8;
	for (jy = s_ys; jy < s_ye; jy++)
	  {
	    if ((jy + y ) < 0 || (jy + y) >= oire->im.ny) continue;
	    s_xs = (jx1 == 0) ? 0 : s_8;
	    s_xe = (jx1 == nxs-1) ? size : size - s_8;	    
	    for (jx = s_xs; jx < s_xe; jx++)
	      {		
		if ((jx + x) < 0 || (jx + x) >= oire->im.nx) continue;
		xp = ((float)(jx-s_2)/s_3)+j%nxs;
		yp = ((float)(jy-s_2)/s_3)+j/nxs;
		ki = (int)interpolate_image_point(oiz, xp, yp, NULL, NULL);
		for (im = 0; im < sizere; im++)
		  {
		    switch_frame(oire,im);
		    k = ki+im-sizere/2;//oiz->im.pixel[jy1].in[jx1]+im-sizere/2;
		    k = (k < 0) ? 0 : k;
		    k = (k < nfi) ? k : nfi-1;
		    switch_frame(ois,k);
		    oire->im.pixel[jy+y].ch[jx+x] = ois->im.pixel[jy+y].ch[jx+x];
		  }    
	      }
	  }
      }
    
    
    t_c = my_uclock() - t_c;
    oim->need_to_refresh = ALL_NEED_REFRESH;
    find_zmin_zmax(oim);
    if (oidz)
      {
	find_zmin_zmax(oidz);
	set_oi_horizontal_extend(oidz, (float)(X1+size-X0)/1024);
	set_oi_vertical_extend(oidz, (float)(Y1+size-Y0)/1024);    	
      }
    if (oiz)
      {
	find_zmin_zmax(oiz);
	set_oi_horizontal_extend(oiz, (float)(X1+size-X0)/1024);
	set_oi_vertical_extend(oiz, (float)(Y1+size-Y0)/1024);	
      }
    if (oiq)
      {
	find_zmin_zmax(oiq);
	set_oi_horizontal_extend(oiq, (float)(X1+size-X0)/1024);
	set_oi_vertical_extend(oiq, (float)(Y1+size-Y0)/1024);		
      }
    if (oimaxq)
      {
	find_zmin_zmax(oimaxq);
	set_oi_horizontal_extend(oimaxq, (float)(X1+size-X0)/1024);
	set_oi_vertical_extend(oimaxq, (float)(Y1+size-Y0)/1024);
      }
    if (oimaxqf)
      {
	find_zmin_zmax(oimaxqf);
	set_oi_horizontal_extend(oimaxqf, (float)(X1+size-X0)/1024);
	set_oi_vertical_extend(oimaxqf, (float)(Y1+size-Y0)/1024);
      }
    if (oimax4)
      {
	find_zmin_zmax(oimax4);
	set_oi_horizontal_extend(oimax4, (float)(X1+size-X0)/1024);
	set_oi_vertical_extend(oimax4, (float)(Y1+size-Y0)/1024);
      }
    if (oire)
      {
	find_zmin_zmax(oire);
	set_oi_horizontal_extend(oire, (float)(X1+size-X0)/1024);
	set_oi_vertical_extend(oire, (float)(Y1+size-Y0)/1024);    	
      }
    to = get_my_uclocks_per_sec();
    win_printf("Process took %g S\nx0 %d x1 %d dx %d\ny0 %d y1 %d dy %d\n"
	       ,((double)t_c)/to,X0,X1,X1-X_1,Y0,Y1,Y1-Y_1);
    //add_image(imr, oid->im.data_type, (void*)oid);
    //add_image(imr, oidp->im.data_type, (void*)oidp);
    //add_image(imr, oidf->im.data_type, (void*)oidf);


    free_one_image(oip);
    free_one_image(oid);
    //free_one_image(oidft);    
    free_one_image(oidp);        
    free_one_image(apo);
    return refresh_image(imr, imr->n_oi - 1);
}

	



	
int do_find_NB_modes_above_noise_by_cutting_in_small_parts(void)
{
    imreg *imr = NULL;
    O_i *ois = NULL; //input big movie
    O_i *oim = NULL; // small subset of movie to be treated
    O_i *oip = NULL; // Orthogonal polynomial movie
    O_i *oidp = NULL; // small image minus orthogonal poynomial
    O_i *apo = NULL; // appodisation image
    O_i *oid = NULL; // appodised movie
    O_i *oidf = NULL; // FFT movie
    O_i *oidz = NULL; // image containing dz
    O_i *oitmp = NULL; // pointer on image to copy
    O_i *oidu = NULL;  // undersample image
    O_i *oidx = NULL; // image containing dx
    O_i *oidy = NULL; // image containing dy
    O_i *oimaxq = NULL; // image containing the quality of max    
    O_i *oidc = NULL; // correlation image
    O_i *oibf = NULL; // image containing band pass
    O_i *oire = NULL; // image final with motion removed
    int i, j, k, x = 0, y = 0, nf, nfi, nxs = 0, nys = 0, im, X0 = 0, X1 = 0, X_1 = 0, Y0 = 0, Y1 = 0, Y_1 = 0;
    int sizex = 0, sizey = 0, immax = 0, itmp;
    O_p *opoi = NULL; // the plot with ds synchrone with movie
    O_p *opmax = NULL; // plot containing max position value over +/- 2zavg
    d_s *dsmax = NULL;
    O_p *opcor = NULL; // plot containing max correlation value over +/- 4zavg
    d_s *dscor = NULL;
    O_p *opq = NULL;  // the quality of the maximum
    O_p *oppro = NULL; // the plot with ds synchrone with movie displaying x,y local move
    d_s *ds = NULL;
    d_s *dsop = NULL;
    O_p *opmeandxy = NULL;
    d_s *dsmeandxy = NULL;

    float max_val = 0, x_fmax = 0, y_fmax = 0, max_val_bg, max_z2_bg, tmp;
    static int size = 192, seedref = 48, norm = 0;
    static unsigned long t_c, to = 0;
    static int keep_dc = 1, sub = 1, bin = 3, yonly = 0, treat0 = -1, keepi = -1, zavg = 32;
    static float sm = 0.0;
    static int useBP = 0, lp = 6, lw = 4, hp = 64, hw = 16, polyapo = 0, startre = 0,sizere = 128, outcolor = 1;
    double a = 0, b = 0, E = 0;
    
    if(updating_menu_state != 0)	return D_O_K;
    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
    i = win_scanf("This routine will cut an image in smaller ones\n"
                  "with 1/2 covering edge for correlation\n"
                  "define the size of the small image %8d\n"
		  "%R->undersample in X and Y or %r->in Y only\n"
		  "Define your undersampling %4d\n"
		  "%R->projection or %r->substract projection\n"
		  "For polynome projection %buse apodisation\n"
		  "For appodisation: %bkeep DC component\n"
		  "Decrease appodisation to %6f \n"
		  "Averaging size in Z %5d\n"
		  "%bApply BP filter\n"
		  "If yes LP %4d w %4d HP %4d w %4d\n"
		  "%bNormalize correlation\n"
		  "reference index %4d\n"
		  "Reconstruct movie without motion"
		  "starting at frame %4d over %4d;\n"
		  "For pixels extrapolated outside the image \n"
		  "%R->impose black %r->or use nearest neighbor\n"
		  "treat only %4d keep image %4d\n"
		  ,&size,&yonly,&bin,&sub,&polyapo,&keep_dc,&sm,&zavg,&useBP,&lp,&lw,&hp,&hw
		  ,&norm,&seedref,&startre,&sizere,&outcolor,&treat0,&keepi);
    if (i == WIN_CANCEL) return 0;

    if (ois == NULL)      win_printf_OK("No image found");

    nfi = nb_of_frames_in_movie(ois);
    if (nfi < 2)      win_printf_OK("Not a movie %d frames",nfi);    
    nf = compute_nb_of_subset(ois, size, size,&nxs,&nys);
    win_printf("New image %d x %d x %d",nxs,nys,nfi);
    oim = create_and_attach_movie_to_imr(imr, size, size, ois->im.data_type, nfi);
    if (oim == NULL) return win_printf_OK("cannot create tmp movie");
    oidx = create_and_attach_movie_to_imr(imr, nxs, nys, IS_FLOAT_IMAGE, nfi);
    if (oidx == NULL) return win_printf_OK("cannot create result movie");
    oidy = create_and_attach_movie_to_imr(imr, nxs, nys, IS_FLOAT_IMAGE, nfi);
    if (oidy == NULL) return win_printf_OK("cannot create result movie");
    oimaxq = create_and_attach_movie_to_imr(imr, nxs, nys, IS_FLOAT_IMAGE, nfi);
    if (oimaxq == NULL) return win_printf_OK("cannot create result movie");    
    oidz = create_and_attach_movie_to_imr(imr, nxs, nys, IS_FLOAT_IMAGE, nfi);
    if (oidz == NULL) return win_printf_OK("cannot create result movie");
    sizex = (yonly) ? size : size/bin;
    sizey = size/bin;     
    oip = build_ortho_poly2x2(sizex, sizey, (polyapo)? apo : NULL);
    if (oip == NULL) return win_printf_OK("cannot build polynomial");    
    apo = create_appodisation_float_image(sizex, sizey, sm, 0);
    if (apo == NULL)	return win_printf_OK("Could not create apodisation image!");
    ds = build_data_set(nfi,nfi);
    if (ds == NULL)	return win_printf_OK("Could not create tmp ds!");
    opoi = create_and_attach_op_to_oi(oidz, nf, nf, 0, 0);
    if (opoi == NULL)   return win_printf_OK("Could not create tmp opoi!");
    dsop = opoi->dat[0];
    if (dsop == NULL)	return win_printf_OK("Could not create tmp dsop!");

    opmax = create_and_attach_op_to_oi(oidz, 4*zavg, 4*zavg, 0, 0);
    if (opmax == NULL)   return win_printf_OK("Could not create tmp opmax!");
    create_and_attach_n_ds(nf, opmax, 4*zavg, 4*zavg, 0);
    set_ds_point_symbol(opmax->dat[nf],"\\oc");

    opcor = create_and_attach_op_to_oi(oidz, 4*zavg, 4*zavg, 0, 0);
    if (opcor == NULL)   return win_printf_OK("Could not create opcor!");
    create_and_attach_n_ds(nf, opcor, 8*zavg, 8*zavg, 0);
    set_ds_point_symbol(opcor->dat[nf],"\\oc");

    opq = create_and_attach_op_to_oi(oidz, nf, nf, 0, 0);
    if (opq == NULL)   return win_printf_OK("Could not create opq!");
    create_and_attach_n_ds(2, opq, nf, nf, 0);

    opoi = create_and_attach_op_to_oi(ois, 2*nf, 2*nf, 0, IM_SAME_X_AXIS | IM_SAME_Y_AXIS);
    if (opoi == NULL)   return win_printf_OK("Could not create tmp opoi!");
    create_and_attach_n_ds(nfi-1, opoi, 2*nf, 2*nf, 0);

    oppro = create_and_attach_op_to_oi(oidz, nfi, nfi, 0, 0);
    if (oppro == NULL)   return win_printf_OK("Could not create opq!");
    create_and_attach_n_ds(nf-1, oppro, nfi, nfi, 0);    

    set_oi_plot_axes(ois, DS_OF_OP_SYNCHRONIZE_WITH_MOVIE);

    opmeandxy = create_and_attach_op_to_oi(oidz, nfi, nfi, 0, 0);
    if (opmeandxy == NULL)   return win_printf_OK("Could not create opq!");
    dsmeandxy = opmeandxy->dat[0];
    dsmeandxy = alloc_data_set_y_error(dsmeandxy);
    
    for (im = 0; im < opoi->n_dat; im++)
      {
	set_ds_dash_line(opoi->dat[im]);
	set_ds_line_color(opoi->dat[im], Lightred);
	//set_ds_point_symbol(opoi->dat[im],"\\oc");
      }
    t_c = my_uclock();
    for (j = 0; j < nf; j++)
      {   // we scan small images covering the big one
        X_1 = (j != 0 && x != X_1)? x : X_1;
        Y_1 = (j != 0 && y != Y_1)? y : Y_1;
	find_subset_pos(ois, size, size, j, &x, &y);
	X0 = (j == 0 || x < X0) ? x : X0;
	X1 = (j == 0 || x > X1) ? x : X1;
	Y0 = (j == 0 || y < Y0) ? y : Y0;
	Y1 = (j == 0 || y > Y1) ? y : Y1;		
	display_title_message("small image=%d x=%d y=%d",j,x,y);
	if (treat0 >= 0 && treat0 != j) continue;
	for (im = 0; im < nfi; im++)
	  {  // we copy the small image movie
	    switch_frame(oim,im);
	    switch_frame(ois,im);
	    copy_one_subset_of_image_and_border(oim, ois, x, y);
	  }
	if (oim == NULL) win_printf("Small movie %p",(void*)oim);
	oitmp = oidu;
	if (j == keepi)
	  add_image(imr, oim->im.data_type, duplicate_image_or_movie(oim, NULL, 0));
	  
	if (bin > 1)
	  {   // if undersampling was selected we do it
	    if (yonly) oidu = undersample_image_of_char_in_y(oidu, oim, bin);
	    else oidu = undersample_image_of_char(oidu, oim, bin);
	    if (oitmp != NULL && oitmp != oidu)
	      win_printf("mem leak in undersample");
	    if (oidu == NULL) win_printf("undersampled  %p",(void*)oidu);
	  }
	if (j == keepi && bin > 1)
	  add_image(imr, oidu->im.data_type, duplicate_image_or_movie(oidu, NULL, 0));	
	oitmp = oidp;
	// we project on or substract orthogonal first modes to flatten 
	oidp = project_on_first_poly(oidp, (bin > 1) ? oidu : oim, oip, sub, (polyapo)? apo : NULL);
	if (j == keepi)
	  add_image(imr, oidp->im.data_type, duplicate_image_or_movie(oidp, NULL, 0)); 
	if (oitmp != NULL && oitmp != oidp)
	  win_printf("mem leak in project");	
	if (oidp == NULL) win_printf("project %p ",(void*)oidp);
	oitmp = oid;
	// we apply appodisation
	oid = d2_im_appo(oid, oidp, 0, NULL, apo, 1, keep_dc);
	if (j == keepi)
	  add_image(imr, oid->im.data_type, duplicate_image_or_movie(oid, NULL, 0)); 	
	if (oitmp != NULL && oitmp != oid)
	  win_printf("mem leak in apo");
	if (oid == NULL) win_printf("apo %p",(void*)oid);
	// we switch periodic boundary conditions because we have already applied appodisation
	set_oi_periodic_in_x_and_y(oid);
	oitmp = oidf;
	//oidf = forward_fft_2d_im(oidf, oid, screen, 0);
	oidf = forward_fftbt_2d_im(oidf, oid, 0, screen, 0);
	if (j == keepi)
	  add_image(imr, oidf->im.data_type, duplicate_image_or_movie(oidf, NULL, 0)); 		
	if (oitmp != NULL && oitmp != oidf)
	  win_printf("mem leak in fft");	
	if (oidf == NULL) win_printf("Fft movie");
	// we computes how many modes are above noise in TF
	ds = Nb_modes_above_noise_by_histo_small_amp_of_fft_2d_movie(oidf, ds);
	for (im = 0; ds != NULL && im < nfi; im++)
	  {  // we copy this data
	    switch_frame(oidz,im);
	    oidz->im.pixel[j/nxs].fl[j%nxs] = ds->yd[im];
	  }
	for (im = 0; im < ds->nx-zavg; im++)
	  {
	    for(k = 0, ds->xd[im] = 0; k < zavg; k++)
	      ds->xd[im] += ds->yd[im+k];
	  }
	for (im = immax = 0; im < ds->nx-zavg; im++)
	  immax = (ds->xd[im] > ds->xd[immax]) ? im : immax;
	immax += zavg/2;  // we find the place with the maximum correspondind to the sharpest image
	

	if (useBP)  // we apply band pass if selected
	  oidf = band_pass_filter_fft_2d_im(oidf, oidf,  0, lp,  lw,  hp,  hw, 0, 0);
	// we correlate small images
	oidc = corel_2d_movie_in_time(oidc, oidf, immax, norm);
	if (j == keepi)
	  add_image(imr, oidc->im.data_type, duplicate_image_or_movie(oidc, NULL, 0));
	// we go back in real space
	oibf = backward_fftbt_2d_im(oibf, oidc, 0, screen, 0);
	oibf = shift_2d_im(oibf, oibf);
	if (j == keepi)
	  add_image(imr, oibf->im.data_type, duplicate_image_or_movie(oibf, NULL, 0));
	// we find the max starting at immax
	switch_frame(oibf,immax);
	// this the max
	find_zmax_and_pos_interpolate(oibf, &max_val, &x_fmax, &y_fmax); 
	// this is the noise max	
	find_zmax_excluding_an_area(oibf, x_fmax, oibf->im.nx/4, y_fmax, oibf->im.ny/4,&max_val_bg, &max_z2_bg);
	// we copy the max position in images
	oidx->im.pxl[immax][j/nxs].fl[j%nxs] = x_fmax;
	oidy->im.pxl[immax][j/nxs].fl[j%nxs] = y_fmax;
	// we copy the quality factor in an image
	oimaxq->im.pxl[immax][j/nxs].fl[j%nxs] = (max_val_bg > 0) ? max_val/max_val_bg : max_val;
	// we draw the shift vector in image
	opoi->dat[immax]->xd[2*j] = (size/2) + x;
	opoi->dat[immax]->yd[2*j] = (size/2) + y;
	opoi->dat[immax]->xd[2*j+1] = (x_fmax*bin) + x;
	opoi->dat[immax]->yd[2*j+1] = (y_fmax*bin) + y;
	// we keep max
	dsop->xd[j] = immax;
	ds->yd[immax] = dsop->yd[j] = max_val;

	for(im = 0, dsmax = opmax->dat[j]; im < dsmax->nx; im++)
	  {
	    k = immax + im - 5*zavg/2;
	    k = (k < 0) ? 0 : k;
	    k = (k < ds->nx-zavg) ? k : ds->nx-zavg-1;
	    dsmax->yd[im] = ds->xd[k]/zavg;
	    opmax->dat[nf]->yd[im] += ds->xd[k]/zavg;
	    opmax->dat[nf]->xd[im] = dsmax->xd[im] = im - 2*zavg;
	  }
	// we find the max after immax and repeat the process
	for (im = immax+1; im < nfi; im++)
	  { // we restrict max search close to previous max
	    itmp = (int)(0.5+y_fmax);
	    if (itmp < oibf->im.ny/4)
	      {	oibf->im.nys = 0; oibf->im.nye = oibf->im.ny/2;  }
	    else if (itmp > 3*oibf->im.ny/4)
	      {	oibf->im.nys = oibf->im.ny/2;  oibf->im.nye = oibf->im.ny;}
	    else
	      {	oibf->im.nys = itmp - oibf->im.ny/4;
		oibf->im.nye = itmp + oibf->im.ny/4; }
	    itmp = (int)(0.5+x_fmax);
	    if (itmp < oibf->im.nx/4)
	      {	oibf->im.nxs = 0;
		oibf->im.nxe = oibf->im.nx/2;  }
	    else if (itmp > 3*oibf->im.nx/4)
	      {	oibf->im.nxs = oibf->im.nx/2;	    
		oibf->im.nxe = oibf->im.nx;    }
	    else
	      {	oibf->im.nxs = itmp - oibf->im.nx/4;
		oibf->im.nxe = itmp + oibf->im.nx/4;       }
	    switch_frame(oibf,im);
	    find_zmax_and_pos_interpolate(oibf, &max_val, &x_fmax, &y_fmax);
	    find_zmax_excluding_an_area(oibf, x_fmax, oibf->im.nx/4, y_fmax, oibf->im.ny/4,&max_val_bg, &max_z2_bg);		    
	    oidx->im.pxl[im][j/nxs].fl[j%nxs] = x_fmax;
	    oidy->im.pxl[im][j/nxs].fl[j%nxs] = y_fmax;
	    oimaxq->im.pxl[im][j/nxs].fl[j%nxs] = (max_val_bg > 0) ? max_val/max_val_bg : max_val;		
	    opoi->dat[im]->xd[2*j] = (size/2) + x;
	    opoi->dat[im]->yd[2*j] = (size/2) + y;
	    opoi->dat[im]->xd[2*j+1] = (x_fmax*bin) + x;
	    opoi->dat[im]->yd[2*j+1] = (y_fmax*bin) + y;
	    ds->yd[im] = max_val;	    
	  }
	x_fmax = oidx->im.pxl[immax][j/nxs].fl[j%nxs];
	y_fmax = oidy->im.pxl[immax][j/nxs].fl[j%nxs];
	// we find the max before immax	 and repeat the process
	for (im = immax-1; im >= 0; im--)
	  {  // we restrict max search close to previous max
	    itmp = (int)(0.5+y_fmax);
	    if (itmp < oibf->im.ny/4)
	      {	oibf->im.nys = 0; oibf->im.nye = oibf->im.ny/2;  }
	    else if (itmp > 3*oibf->im.ny/4)
	      {	oibf->im.nys = oibf->im.ny/2;  oibf->im.nye = oibf->im.ny;}
	    else
	      {	oibf->im.nys = itmp - oibf->im.ny/4;
		oibf->im.nye = itmp + oibf->im.ny/4; }
	    itmp = (int)(0.5+x_fmax);
	    if (itmp < oibf->im.nx/4)
	      {	oibf->im.nxs = 0;
		oibf->im.nxe = oibf->im.nx/2;  }
	    else if (itmp > 3*oibf->im.nx/4)
	      {	oibf->im.nxs = oibf->im.nx/2;	    
		oibf->im.nxe = oibf->im.nx;    }
	    else
	      {	oibf->im.nxs = itmp - oibf->im.nx/4;
		oibf->im.nxe = itmp + oibf->im.nx/4;       }
	    switch_frame(oibf,im);
	    find_zmax_and_pos_interpolate(oibf, &max_val, &x_fmax, &y_fmax);
	    find_zmax_excluding_an_area(oibf, x_fmax, oibf->im.nx/4, y_fmax, oibf->im.ny/4,&max_val_bg, &max_z2_bg);		    	    
	    oidx->im.pxl[im][j/nxs].fl[j%nxs] = x_fmax;
	    oidy->im.pxl[im][j/nxs].fl[j%nxs] = y_fmax;
	    oimaxq->im.pxl[im][j/nxs].fl[j%nxs] = (max_val_bg > 0) ? max_val/max_val_bg : max_val;			    
	    opoi->dat[im]->xd[2*j] = (size/2) + x;
	    opoi->dat[im]->yd[2*j] = (size/2) + y;
	    opoi->dat[im]->xd[2*j+1] = (x_fmax*bin) + x;
	    opoi->dat[im]->yd[2*j+1] = (y_fmax*bin) + y;
	    ds->yd[im] = max_val;	    	    
	  }
	for(im = 0, dscor = opcor->dat[j]; im < dscor->nx; im++)
	  {
	    k = immax + im - dscor->nx/2;
	    k = (k < 0) ? 0 : k;
	    k = (k < ds->nx) ? k : ds->nx-1;
	    dscor->yd[im] = ds->yd[k];
	    dscor->yd[im] /= (ds->yd[immax] > 0) ? ds->yd[immax] : 1;
	    opcor->dat[nf]->yd[im] += dscor->yd[im];
	    opcor->dat[nf]->xd[im] = dscor->xd[im] = im - dscor->nx/2;
	  }
	
	oibf->im.nxs = 0;	 oibf->im.nxe = oibf->im.nx;
	oibf->im.nys = 0;	 oibf->im.nye = oibf->im.ny;
    }
    for(im = 0, dsmax = opmax->dat[nf]; im < dsmax->nx; im++)
      dsmax->yd[im] /= (nf > 1) ? nf : 1;
    for(im = 0, dscor = opcor->dat[nf]; im < dscor->nx; im++)
      dscor->yd[im] /= (nf > 1) ? nf : 1;
    alloc_data_set_y_error(ds);
    for(j = 0, dsmax = opmax->dat[nf]; j < nf; j++)
      {
	for(im = 0; im < dsmax->nx; im++)
	  {
	    k = (int)(dsop->xd[j]) + im - 2*zavg;
	    k = (k < 0) ? 0 : k;
	    k = (k < nfi) ? k : nfi-1;
	    ds->yd[im] = oidz->im.pxl[k][j/nxs].fl[j%nxs];
	    ds->ye[im] = sqrt(1 + ds->yd[im]);
	    ds->xd[im] = im - 2*zavg;
	  }
	ds->nx = ds->ny = dsmax->nx;
	//find_best_a_and_b_of_2_ds(ds, dsmax, 0, 0, &a, &b, &E);
	find_best_a_of_2_ds(ds, dsmax, 0, &a, &E);
	opq->dat[0]->yd[j] = a;
	opq->dat[0]->xd[j] = j;
	opq->dat[1]->yd[j] = b;
	opq->dat[1]->xd[j] = j;
	opq->dat[2]->yd[j] = E;
	opq->dat[2]->xd[j] = j;
	if (j == seedref)
	  {
	    add_data_to_one_plot(opmax, IS_DATA_SET, (void*)duplicate_data_set(ds, NULL));
	  }
      }
    for(j = 0; j < nf; j++)
      {
	for(im = 0; im < nfi; im++)
	  {
    	    oppro->dat[j]->xd[im] = bin*oidx->im.pxl[im][j/nxs].fl[j%nxs];
    	    oppro->dat[j]->yd[im] = bin*oidy->im.pxl[im][j/nxs].fl[j%nxs];
	    oppro->dat[j]->user_ispare[0]=j%nxs;
	    oppro->dat[j]->user_ispare[1]=j/nxs;	    
	  }
      }
    // we synchronize dx and dy of different subsets
    int jx, jy;
    float dx, dy;
    for (j = 0; j < nf; j++)
    {
      jy = j/nxs; jx = j%nxs;
      find_subset_pos(ois, size, size, j, &x, &y);
      immax = dsop->xd[seedref];
      dx = oidx->im.pxl[immax][jy].fl[jx] - (size/(2*bin));
      dy = oidy->im.pxl[immax][jy].fl[jx] - (size/(2*bin));
      for(im = 0; im < nfi; im++)
	{
	  oidx->im.pxl[im][jy].fl[jx] -= dx;
	  oidy->im.pxl[im][jy].fl[jx] -= dy;
	  opoi->dat[im]->xd[2*j] = (size/2) + x;
	  opoi->dat[im]->yd[2*j] = (size/2) + y;
	  opoi->dat[im]->xd[2*j+1] = (oidx->im.pxl[im][jy].fl[jx]*bin) + x;
	  opoi->dat[im]->yd[2*j+1] = (oidy->im.pxl[im][jy].fl[jx]*bin) + y;
	  
	}
    }
    for (j = 0; j < nf; j++)
      {
	for(im = 0; im < dsmeandxy->nx; im++)
	  {
	    tmp = oimaxq->im.pxl[im][j/nxs].fl[j%nxs];
	    if (tmp > 3)
	      {
		tmp = exp(tmp/2);
		dsmeandxy->xd[im] += tmp*oidx->im.pxl[im][j/nxs].fl[j%nxs];
		dsmeandxy->yd[im] += tmp*oidy->im.pxl[im][j/nxs].fl[j%nxs];
		dsmeandxy->ye[im] += tmp;		
	      }
	  }
      }
    for(im = 0; im < dsmeandxy->nx; im++)
      {
	tmp = dsmeandxy->ye[im];
	dsmeandxy->xd[im] /= (tmp > 0) ? tmp : 1;
	dsmeandxy->yd[im] /= (tmp > 0) ? tmp : 1;	
      }


    for (j = 0; j < nf; j++)
    {
      jy = j/nxs; jx = j%nxs;
      find_subset_pos(ois, size, size, j, &x, &y);
      for(im = 0; im < nfi; im++)
	{
	  tmp = oimaxq->im.pxl[im][jy].fl[jx];
	  if (tmp < 3)
	    {  // when signal too low we replace by the mean
	      oidx->im.pxl[im][jy].fl[jx] = dsmeandxy->xd[im];
	      oidy->im.pxl[im][jy].fl[jx] = dsmeandxy->yd[im];
	      opoi->dat[im]->xd[2*j] = (size/2) + x;
	      opoi->dat[im]->yd[2*j] = (size/2) + y;
	      opoi->dat[im]->xd[2*j+1] = (oidx->im.pxl[im][jy].fl[jx]*bin) + x;
	      opoi->dat[im]->yd[2*j+1] = (oidy->im.pxl[im][jy].fl[jx]*bin) + y;
	    }
	}
    }

    
    //we create the new image with motion removed
    float xp, yp;
    int s_8 = size/8, s_2 = size/2, s_3 = (3*size)/4, dx0m, dx1m, dy0m, dy1m;

    sizere = ((startre + sizere) <= nfi) ? sizere : nfi- startre;     
    dx0m = 0; dx1m = ois->im.nx; dy0m = 0; dy1m = ois->im.ny;
    for (im = 0; im < sizere; im++)
      {
	switch_frame(ois,startre+im);
	switch_frame(oidx,startre+im);
	switch_frame(oidy,startre+im);		
	for (j = 0; j < nf; j++)
	  {
	    if ((j%nxs > 0) && (j%nxs < nxs-1)
		&& (j/nxs > 0) && (j/nxs < nxs-1)) continue;
	    find_subset_pos(ois, size, size, j, &x, &y);
	    for (jy = s_8; jy < size - s_8; jy++)
	      {
		if ((jy + y) < 0 || (jy + y) >= ois->im.ny) continue; 
		for (jx = s_8; jx < size - s_8; jx++)
		  {
		    if ((jx + x) < 0 || (jx + x) >= ois->im.nx) continue;
		    xp = ((float)(jx-s_2)/s_3)+j%nxs;
		    yp = ((float)(jy-s_2)/s_3)+j/nxs;
		    dx = bin*interpolate_image_point(oidx, xp, yp, NULL, NULL);
		    dy = bin*interpolate_image_point(oidy, xp, yp, NULL, NULL);
		    if (dx+jx+x-s_2 < 0 && jx+x > dx0m) dx0m = jx+x;
		    if (dx+jx+x-s_2 >= ois->im.nx && jx+x < dx1m) dx1m = jx+x;
		    if (dy+jy+y-s_2 < 0 && jy+y > dy0m) dy0m = jy+y;
		    if (dy+jy+y-s_2 >= ois->im.ny && jy+y < dy1m) dy1m = jy+y;		    
		  }
	      }
	  }
      }
    if (sizere > 0)
      {
	if (outcolor == 0)
	  oire = create_and_attach_movie_to_imr(imr, dx1m-dx0m, dy1m-dy0m, ois->im.data_type, sizere);
	else oire = create_and_attach_movie_to_imr(imr, ois->im.nx, ois->im.ny, ois->im.data_type, sizere);
	if (oire == NULL) return win_printf_OK("cannot create result movie");
      }


    for (im = 0; im < sizere; im++)
      {
	switch_frame(oire,im);
	switch_frame(ois,startre+im);
	switch_frame(oidx,startre+im);
	switch_frame(oidy,startre+im);		
	for (j = 0; j < nf; j++)
	  {
	    find_subset_pos(ois, size, size, j, &x, &y);
	    for (jy = s_8; jy < size - s_8; jy++)
	      {
		if (outcolor == 0)
		  {
		    if ((jy + y ) < dy0m || (jy + y) >= dy1m) continue; 
		    for (jx = s_8; jx < size - s_8; jx++)
		      {
						if ((jx + x) < dx0m || (jx + x) >= dx1m) continue;
			xp = ((float)(jx-s_2)/s_3)+j%nxs;
			yp = ((float)(jy-s_2)/s_3)+j/nxs;
			dx = bin*interpolate_image_point(oidx, xp, yp, NULL, NULL);
			dy = bin*interpolate_image_point(oidy, xp, yp, NULL, NULL);
			oire->im.pixel[jy+y-dy0m].ch[jx+x-dx0m] = fast_interpolate_image_point(ois, dx+jx+x-s_2, dy+jy+y-s_2,0)/256;
		      }
		  }
		else
		  {
		    if ((jy + y ) < 0 || (jy + y) >= ois->im.ny) continue; 
		    for (jx = s_8; jx < size - s_8; jx++)
		      {
			if ((jx + x) < 0 || (jx + x) >= ois->im.nx) continue;
			xp = ((float)(jx-s_2)/s_3)+j%nxs;
			yp = ((float)(jy-s_2)/s_3)+j/nxs;
			dx = bin*interpolate_image_point(oidx, xp, yp, NULL, NULL);
			dy = bin*interpolate_image_point(oidy, xp, yp, NULL, NULL);
			oire->im.pixel[jy+y].ch[jx+x] = fast_interpolate_image_point(ois, dx+jx+x-s_2, dy+jy+y-s_2,-1)/256;
		      }
		  }
		  
	      }
	  }
      }
    
    t_c = my_uclock() - t_c;
    oim->need_to_refresh = ALL_NEED_REFRESH;
    find_zmin_zmax(oim);
    find_zmin_zmax(oidx);
    find_zmin_zmax(oidy);        
    find_zmin_zmax(oidz);
    find_zmin_zmax(oire);            
    to = get_my_uclocks_per_sec();
    win_printf("Process took %g S\nx0 %d x1 %d dx %d\ny0 %d y1 %d dy %d\n"
	       ,((double)t_c)/to,X0,X1,X1-X_1,Y0,Y1,Y1-Y_1);
    //add_image(imr, oid->im.data_type, (void*)oid);
    //add_image(imr, oidp->im.data_type, (void*)oidp);
    //add_image(imr, oidf->im.data_type, (void*)oidf);
    set_oi_horizontal_extend(oidx, (float)(X1+size-X0)/1024);
    set_oi_vertical_extend(oidx, (float)(Y1+size-Y0)/1024);
    set_oi_horizontal_extend(oidy, (float)(X1+size-X0)/1024);
    set_oi_vertical_extend(oidy, (float)(Y1+size-Y0)/1024);
    set_oi_horizontal_extend(oidz, (float)(X1+size-X0)/1024);
    set_oi_vertical_extend(oidz, (float)(Y1+size-Y0)/1024); 
    set_oi_horizontal_extend(oimaxq, (float)(X1+size-X0)/1024);
    set_oi_vertical_extend(oimaxq, (float)(Y1+size-Y0)/1024);   
    if (oire != NULL)
      {
	set_oi_horizontal_extend(oire, (float)(X1+size-X0)/1024);
	set_oi_vertical_extend(oire, (float)(Y1+size-Y0)/1024);
      }
    free_one_image(oip);
    free_one_image(oid);
    //free_one_image(oidft);    
    free_one_image(oidp);        
    free_one_image(apo);
    free_data_set(ds);
    return refresh_image(imr, imr->n_oi - 1);
}

int do_find_displacement_of_movie(void)
{
    imreg *imr = NULL;
    O_i *ois = NULL;
    O_i *oidc = NULL; // correlation image
    O_i *oibf = NULL; // image containing band pass
    O_p *opoi = NULL; // the plot for results    
    d_s *dsop = NULL;
    d_s *dsq = NULL;
    d_s *dsm = NULL;
    d_s *dsn = NULL;    
    int j, nfi, im;
    float max_val = 0, x_fmax = 0, y_fmax = 0, max_val_bg, max_z2_bg;
    static int size, norm = 1;    
    static int n0 = 50; // size over which correlation is computed
    int *im1 = NULL, *im2 = NULL, bin = 1;
    float dx = 0, dy = 0, dxx = 0, dyy = 0;
    
    if(updating_menu_state != 0)	return D_O_K;
    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;

    if (ois == NULL)      win_printf_OK("No image found");
    size = ois->im.nx;
    nfi = nb_of_frames_in_movie(ois);
    if (nfi < 2)      win_printf_OK("Not a movie %d frames",nfi);
    im1 = (int*)calloc(nfi,sizeof(int));
    im2 = (int*)calloc(nfi,sizeof(int));
    if (im1 == NULL || im2 == NULL) return win_printf_OK("cannot create tmp int arrays");

    opoi = create_and_attach_op_to_oi(ois, nfi, nfi, 0, IM_SAME_X_AXIS | IM_SAME_Y_AXIS);
    if (opoi == NULL)   return win_printf_OK("Could not create tmp opoi!");
    dsop = opoi->dat[0];
    dsq = create_and_attach_one_ds(opoi, nfi, nfi, 0);
    if (dsq == NULL)   return win_printf_OK("Could not create tmp opoi!");
    dsm = create_and_attach_one_ds(opoi, nfi, nfi, 0);
    if (dsm == NULL)   return win_printf_OK("Could not create tmp opoi!");
    dsn = create_and_attach_one_ds(opoi, nfi, nfi, 0);
    if (dsn == NULL)   return win_printf_OK("Could not create tmp opoi!");    


    for (im = 0, j = 0; im < nfi; im++)
      {
	im1[im] = ((n0/2) - 1) + n0*(im/n0);
	im2[im] = (j == im1[im]) ? (n0 + n0*(im/n0)): j;
	j++;
	dsn->xd[im] = im1[im];
	dsn->yd[im] = im2[im]; 	
      }

    
    oidc = corel_2d_movie_in_time_from_arrays(oidc, ois, im1, im2, nfi, norm);
    add_image(imr, oidc->im.data_type, duplicate_image_or_movie(oidc, NULL, 0));
    // we go back in real space
    oibf = backward_fftbt_2d_im(oibf, oidc, 0, screen, 0);
    oibf = shift_2d_im(oibf, oibf);
    add_image(imr, oibf->im.data_type, duplicate_image_or_movie(oibf, NULL, 0));

    

    for (im = 0; im < nfi; im++)
      { // we find the max 
	switch_frame(oibf,im);
	// this the max
	find_zmax_and_pos_interpolate(oibf, &max_val, &x_fmax, &y_fmax); 
	// this is the noise max	
	find_zmax_excluding_an_area(oibf, x_fmax, oibf->im.nx/4, y_fmax, oibf->im.ny/4,&max_val_bg, &max_z2_bg);
	// we copy the max position in images
	dsop->xd[im] = x_fmax;
	dsop->yd[im] = y_fmax;
	// we copy the quality factor in an image
	dsq->yd[im] = (max_val_bg > 0) ? max_val/max_val_bg : max_val;
	dsq->xd[im] = im;
      }
    for (im = 0, dx = dy = 0, dxx = dyy = size/(2*bin); im < nfi; im++)
      { // we correct correlations with different reference
	dsm->xd[im] = dsop->xd[im];
	dsm->yd[im] = dsop->yd[im];	
	if (im == im1[im])
	  {
	    dxx = dsm->xd[im];
	    dsm->xd[im] = size/(2*bin);
	    dyy = dsm->yd[im];
	    dsm->yd[im] = size/(2*bin);
	  }
	if (((im % n0) == 0) && (im > 0))
	  {
	    dx += dxx - dsm->xd[im];
	    dy += dyy - dsm->yd[im];
	  }
	dsm->xd[im] += dx;
	dsm->yd[im] += dy;
      }
# ifdef NEW
    
    dx = oidx->im.pxl[seedref][j/nxs].fl[j%nxs] - size/(2*bin);
    dy = oidy->im.pxl[seedref][j/nxs].fl[j%nxs] - size/(2*bin);
    for (im = 0; im < nfi; im++)
      { // we set zeo drift at seedref
	oidx->im.pxl[im][j/nxs].fl[j%nxs] -= dx;
	oidy->im.pxl[im][j/nxs].fl[j%nxs] -= dy;
	opoi->dat[im]->xd[2*j] = (size/2) + x;
	opoi->dat[im]->yd[2*j] = (size/2) + y;
	opoi->dat[im]->xd[2*j+1] = (oidx->im.pxl[im][j/nxs].fl[j%nxs]*bin) + x;
	opoi->dat[im]->yd[2*j+1] = (oidy->im.pxl[im][j/nxs].fl[j%nxs]*bin) + y;
      }
# endif
    if (im1) free(im1);
    if (im2) free(im2);    
    return refresh_image(imr, imr->n_oi - 1);
    
}

int do_find_NB_modes_above_noise_by_cutting_in_small_parts_ver2(void)
{
    imreg *imr = NULL;
    O_i *ois = NULL; //input big movie
    O_i *oim = NULL; // small subset of movie to be treated
    O_i *oip = NULL; // Orthogonal polynomial movie
    O_i *oidp = NULL; // small image minus orthogonal poynomial
    O_i *apo = NULL; // appodisation image
    O_i *oid = NULL; // appodised movie
    O_i *oidf = NULL; // FFT movie
    O_i *oidz = NULL; // image containing dz
    O_i *oitmp = NULL; // pointer on image to copy
    O_i *oidu = NULL;  // undersample image
    O_i *oidx = NULL; // image containing dx
    O_i *oidy = NULL; // image containing dy
    O_i *oimaxq = NULL; // image containing the quality of max    
    O_i *oidc = NULL; // correlation image
    O_i *oibf = NULL; // image containing band pass
    O_i *oire = NULL; // image final with motion removed
    int i, j, k, x = 0, y = 0, nf, nfi, nxs = 0, nys = 0, im, X0 = 0, X1 = 0, X_1 = 0, Y0 = 0, Y1 = 0, Y_1 = 0;
    int sizex = 0, sizey = 0, immax = 0;
    O_p *opoi = NULL; // the plot with ds synchrone with movie
    O_p *opmax = NULL; // plot containing max position value over +/- 2zavg
    d_s *dsmax = NULL;
    O_p *opcor = NULL; // plot containing max correlation value over +/- 4zavg
    d_s *dscor = NULL;
    O_p *opq = NULL;  // the quality of the maximum
    O_p *oppro = NULL; // the plot with ds synchrone with movie displaying x,y local move
    d_s *ds = NULL;
    d_s *dsop = NULL;
    O_p *opmeandxy = NULL;
    d_s *dsmeandxy = NULL;

    float max_val = 0, x_fmax = 0, y_fmax = 0, max_val_bg, max_z2_bg, tmp;
    static int size = 192, seedref = 48, norm = 0;
    static unsigned long t_c, to = 0;
    static int keep_dc = 1, sub = 1, bin = 3, yonly = 0, treat0 = -1, keepi = -1, zavg = 32;
    static float sm = 0.0;
    static int useBP = 0, lp = 6, lw = 4, hp = 64, hw = 16, polyapo = 0, startre = 0,sizere = 128, outcolor = 1;
    double a = 0, b = 0, E = 0;
    int *im1 = NULL;  // array containing index of first correlation
    int *im2 = NULL;  // array containing index of second correlation  
    float dx = 0, dy = 0, dxx = 0, dyy = 0;
    static int n0 = 50; // size over which correlation is computed
    int jx, jy;
    
    if(updating_menu_state != 0)	return D_O_K;
    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
    i = win_scanf("This routine will cut an image in smaller ones\n"
                  "with 1/2 covering edge for correlation\n"
                  "define the size of the small image %8d\n"
		  "%R->undersample in X and Y or %r->in Y only\n"
		  "Define your undersampling %4d\n"
		  "%R->projection or %r->substract projection\n"
		  "For polynome projection %buse apodisation\n"
		  "For appodisation: %bkeep DC component\n"
		  "Decrease appodisation to %6f \n"
		  "Averaging size in Z %5d\n"
		  "%bApply BP filter\n"
		  "If yes LP %4d w %4d HP %4d w %4d\n"
		  "%bNormalize correlation\n"
		  "reference index %4d size to compute correlation %4d\n"
		  "Reconstruct movie without motion"
		  "starting at frame %4d over %4d;\n"
		  "For pixels extrapolated outside the image \n"
		  "%R->impose black %r->or use nearest neighbor\n"
		  "treat only %4d keep image %4d\n"
		  ,&size,&yonly,&bin,&sub,&polyapo,&keep_dc,&sm,&zavg,&useBP,&lp,&lw,&hp,&hw
		  ,&norm,&seedref,&n0,&startre,&sizere,&outcolor,&treat0,&keepi);
    if (i == WIN_CANCEL) return 0;

    if (ois == NULL)      win_printf_OK("No image found");

    nfi = nb_of_frames_in_movie(ois);
    if (nfi < 2)      win_printf_OK("Not a movie %d frames",nfi);
    im1 = (int*)calloc(nfi,sizeof(int));
    im2 = (int*)calloc(nfi,sizeof(int));
    if (im1 == NULL || im2 == NULL) return win_printf_OK("cannot create tmp int arrays");
    nf = compute_nb_of_subset(ois, size, size,&nxs,&nys);
    win_printf("New image %d x %d x %d",nxs,nys,nfi);
    oim = create_and_attach_movie_to_imr(imr, size, size, ois->im.data_type, nfi);
    if (oim == NULL) return win_printf_OK("cannot create tmp movie");
    oidx = create_and_attach_movie_to_imr(imr, nxs, nys, IS_FLOAT_IMAGE, nfi);
    if (oidx == NULL) return win_printf_OK("cannot create result movie");
    oidy = create_and_attach_movie_to_imr(imr, nxs, nys, IS_FLOAT_IMAGE, nfi);
    if (oidy == NULL) return win_printf_OK("cannot create result movie");
    oimaxq = create_and_attach_movie_to_imr(imr, nxs, nys, IS_FLOAT_IMAGE, nfi);
    if (oimaxq == NULL) return win_printf_OK("cannot create result movie");    
    oidz = create_and_attach_movie_to_imr(imr, nxs, nys, IS_FLOAT_IMAGE, nfi);
    if (oidz == NULL) return win_printf_OK("cannot create result movie");
    sizex = (yonly) ? size : size/bin;
    sizey = size/bin;     
    oip = build_ortho_poly2x2(sizex, sizey, (polyapo)? apo : NULL);
    if (oip == NULL) return win_printf_OK("cannot build polynomial");    
    apo = create_appodisation_float_image(sizex, sizey, sm, 0);
    if (apo == NULL)	return win_printf_OK("Could not create apodisation image!");
    ds = build_data_set(nfi,nfi);
    if (ds == NULL)	return win_printf_OK("Could not create tmp ds!");
    opoi = create_and_attach_op_to_oi(oidz, nf, nf, 0, 0);
    if (opoi == NULL)   return win_printf_OK("Could not create tmp opoi!");
    dsop = opoi->dat[0];
    if (dsop == NULL)	return win_printf_OK("Could not create tmp dsop!");

    opmax = create_and_attach_op_to_oi(oidz, 4*zavg, 4*zavg, 0, 0);
    if (opmax == NULL)   return win_printf_OK("Could not create tmp opmax!");
    create_and_attach_n_ds(nf, opmax, 4*zavg, 4*zavg, 0);
    set_ds_point_symbol(opmax->dat[nf],"\\oc");

    opcor = create_and_attach_op_to_oi(oidz, 4*zavg, 4*zavg, 0, 0);
    if (opcor == NULL)   return win_printf_OK("Could not create opcor!");
    create_and_attach_n_ds(nf, opcor, 8*zavg, 8*zavg, 0);
    set_ds_point_symbol(opcor->dat[nf],"\\oc");

    opq = create_and_attach_op_to_oi(oidz, nf, nf, 0, 0);
    if (opq == NULL)   return win_printf_OK("Could not create opq!");
    create_and_attach_n_ds(2, opq, nf, nf, 0);

    opoi = create_and_attach_op_to_oi(ois, 2*nf, 2*nf, 0, IM_SAME_X_AXIS | IM_SAME_Y_AXIS);
    if (opoi == NULL)   return win_printf_OK("Could not create tmp opoi!");
    create_and_attach_n_ds(nfi-1, opoi, 2*nf, 2*nf, 0);

    oppro = create_and_attach_op_to_oi(oidz, nfi, nfi, 0, 0);
    if (oppro == NULL)   return win_printf_OK("Could not create opq!");
    create_and_attach_n_ds(nf-1, oppro, nfi, nfi, 0);    

    set_oi_plot_axes(ois, DS_OF_OP_SYNCHRONIZE_WITH_MOVIE);

    opmeandxy = create_and_attach_op_to_oi(oidz, nfi, nfi, 0, 0);
    if (opmeandxy == NULL)   return win_printf_OK("Could not create opq!");
    dsmeandxy = opmeandxy->dat[0];
    dsmeandxy = alloc_data_set_y_error(dsmeandxy);

    for (im = 0, j = 0; im < nfi; im++)
      {
	im1[im] = ((n0/2) - 1) + n0*(im/n0);
	im2[im] = (j == im1[im]) ? (n0 + n0*(im/n0)): j;
	j++;
      }
    
    for (im = 0; im < opoi->n_dat; im++)
      {
	set_ds_dash_line(opoi->dat[im]);
	set_ds_line_color(opoi->dat[im], Lightred);
	//set_ds_point_symbol(opoi->dat[im],"\\oc");
      }
    t_c = my_uclock();
    for (j = 0; j < nf; j++)
      {   // we scan small images covering the big one
        X_1 = (j != 0 && x != X_1)? x : X_1;
        Y_1 = (j != 0 && y != Y_1)? y : Y_1;
	find_subset_pos(ois, size, size, j, &x, &y);
	X0 = (j == 0 || x < X0) ? x : X0;
	X1 = (j == 0 || x > X1) ? x : X1;
	Y0 = (j == 0 || y < Y0) ? y : Y0;
	Y1 = (j == 0 || y > Y1) ? y : Y1;		
	display_title_message("small image=%d x=%d y=%d",j,x,y);
	if (treat0 >= 0 && treat0 != j) continue;
	for (im = 0; im < nfi; im++)
	  {  // we copy the small image movie
	    switch_frame(oim,im);
	    switch_frame(ois,im);
	    copy_one_subset_of_image_and_border(oim, ois, x, y);
	  }
	if (oim == NULL) win_printf("Small movie %p",(void*)oim);
	oitmp = oidu;
	if (j == keepi)
	  add_image(imr, oim->im.data_type, duplicate_image_or_movie(oim, NULL, 0));
	  
	if (bin > 1)
	  {   // if undersampling was selected we do it
	    if (yonly) oidu = undersample_image_of_char_in_y(oidu, oim, bin);
	    else oidu = undersample_image_of_char(oidu, oim, bin);
	    if (oitmp != NULL && oitmp != oidu)
	      win_printf("mem leak in undersample");
	    if (oidu == NULL) win_printf("undersampled  %p",(void*)oidu);
	  }
	if (j == keepi && bin > 1)
	  add_image(imr, oidu->im.data_type, duplicate_image_or_movie(oidu, NULL, 0));	
	oitmp = oidp;
	// we project on or substract orthogonal first modes to flatten 
	oidp = project_on_first_poly(oidp, (bin > 1) ? oidu : oim, oip, sub, (polyapo)? apo : NULL);
	if (j == keepi)
	  add_image(imr, oidp->im.data_type, duplicate_image_or_movie(oidp, NULL, 0)); 
	if (oitmp != NULL && oitmp != oidp)
	  win_printf("mem leak in project");	
	if (oidp == NULL) win_printf("project %p ",(void*)oidp);
	oitmp = oid;
	// we apply appodisation
	oid = d2_im_appo(oid, oidp, 0, NULL, apo, 1, keep_dc);
	if (j == keepi)
	  add_image(imr, oid->im.data_type, duplicate_image_or_movie(oid, NULL, 0)); 	
	if (oitmp != NULL && oitmp != oid)
	  win_printf("mem leak in apo");
	if (oid == NULL) win_printf("apo %p",(void*)oid);
	// we switch periodic boundary conditions because we have already applied appodisation
	set_oi_periodic_in_x_and_y(oid);
	oitmp = oidf;
	//oidf = forward_fft_2d_im(oidf, oid, screen, 0);
	oidf = forward_fftbt_2d_im(oidf, oid, 0, screen, 0);
	if (j == keepi)
	  add_image(imr, oidf->im.data_type, duplicate_image_or_movie(oidf, NULL, 0)); 		
	if (oitmp != NULL && oitmp != oidf)
	  win_printf("mem leak in fft");	
	if (oidf == NULL) win_printf("Fft movie");
	// we computes how many modes are above noise in TF
	ds = Nb_modes_above_noise_by_histo_small_amp_of_fft_2d_movie(oidf, ds);
	for (im = 0; ds != NULL && im < nfi; im++)
	  {  // we copy this data
	    switch_frame(oidz,im);
	    oidz->im.pixel[j/nxs].fl[j%nxs] = ds->yd[im];
	  }
	for (im = 0; im < ds->nx-zavg; im++)
	  {
	    for(k = 0, ds->xd[im] = 0; k < zavg; k++)
	      ds->xd[im] += ds->yd[im+k];
	  }
	for (im = immax = 0; im < ds->nx-zavg; im++)
	  immax = (ds->xd[im] > ds->xd[immax]) ? im : immax;
	immax += zavg/2;  // we find the place with the maximum correspondind to the sharpest image
	

	if (useBP)  // we apply band pass if selected
	  oidf = band_pass_filter_fft_2d_im(oidf, oidf,  0, lp,  lw,  hp,  hw, 0, 0);
	// we correlate small images
	oidc = corel_2d_movie_in_time_from_arrays(oidc, oidf, im2, im1, nfi, norm);
	//oidc = corel_2d_movie_in_time(oidc, oidf, immax, norm);
	if (j == keepi)
	  add_image(imr, oidc->im.data_type, duplicate_image_or_movie(oidc, NULL, 0));
	// we go back in real space
	oibf = backward_fftbt_2d_im(oibf, oidc, 0, screen, 0);
	oibf = shift_2d_im(oibf, oibf);
	if (j == keepi)
	  add_image(imr, oibf->im.data_type, duplicate_image_or_movie(oibf, NULL, 0));

	for (im = 0; im < nfi; im++)
	  { // we find the max 
	    switch_frame(oibf,im);
	    // this the max
	    find_zmax_and_pos_interpolate(oibf, &max_val, &x_fmax, &y_fmax); 
	    // this is the noise max	
	    find_zmax_excluding_an_area(oibf, x_fmax, oibf->im.nx/4, y_fmax, oibf->im.ny/4,&max_val_bg, &max_z2_bg);
	    // we copy the max position in images
	    oidx->im.pxl[im][j/nxs].fl[j%nxs] = x_fmax;
	    oidy->im.pxl[im][j/nxs].fl[j%nxs] = y_fmax;
	    // we copy the quality factor in an image
	    oimaxq->im.pxl[im][j/nxs].fl[j%nxs] = (max_val_bg > 0) ? max_val/max_val_bg : max_val;
	    // we draw the shift vector in image
	    opoi->dat[im]->xd[2*j] = (size/2) + x;
	    opoi->dat[im]->yd[2*j] = (size/2) + y;
	    opoi->dat[im]->xd[2*j+1] = (x_fmax*bin) + x;
	    opoi->dat[im]->yd[2*j+1] = (y_fmax*bin) + y;
	    // we keep max
	    dsop->xd[j] = immax;
	    ds->yd[immax] = dsop->yd[j] = max_val;
	  }
	for (im = 0, dx = dy = 0, dxx = dyy = size/(2*bin); im < nfi; im++)
	  { // we correct correlations with different reference
	    if (im == im1[im])
	      {
		dxx = oidx->im.pxl[im][j/nxs].fl[j%nxs];
		oidx->im.pxl[im][j/nxs].fl[j%nxs] = size/(2*bin);
		dyy = oidy->im.pxl[im][j/nxs].fl[j%nxs];
		oidy->im.pxl[im][j/nxs].fl[j%nxs] = size/(2*bin);
		//if (j == 0) win_printf("replacing point %d ",im);
	      }
	    if (((im % n0) == 0) && (im > 0))
	      {
		dx += dxx - oidx->im.pxl[im][j/nxs].fl[j%nxs];
		dy += dyy - oidy->im.pxl[im][j/nxs].fl[j%nxs];
		//if (j == 0) win_printf("shifing point %d ",im);
	      }
	    oidx->im.pxl[im][j/nxs].fl[j%nxs] += dx;
	    oidy->im.pxl[im][j/nxs].fl[j%nxs] += dy;
	  }
	dx = oidx->im.pxl[seedref][j/nxs].fl[j%nxs] - size/(2*bin);
	dy = oidy->im.pxl[seedref][j/nxs].fl[j%nxs] - size/(2*bin);
	for (im = 0; im < nfi; im++)
	  { // we set zeo drift at seedref
	    oidx->im.pxl[im][j/nxs].fl[j%nxs] -= dx;
	    oidy->im.pxl[im][j/nxs].fl[j%nxs] -= dy;
	    opoi->dat[im]->xd[2*j] = (size/2) + x;
	    opoi->dat[im]->yd[2*j] = (size/2) + y;
	    opoi->dat[im]->xd[2*j+1] = (oidx->im.pxl[im][j/nxs].fl[j%nxs]*bin) + x;
	    opoi->dat[im]->yd[2*j+1] = (oidy->im.pxl[im][j/nxs].fl[j%nxs]*bin) + y;
	  }

	
	for(im = 0, dsmax = opmax->dat[j]; im < dsmax->nx; im++)
	  {
	    k = immax + im - 5*zavg/2;
	    k = (k < 0) ? 0 : k;
	    k = (k < ds->nx-zavg) ? k : ds->nx-zavg-1;
	    dsmax->yd[im] = ds->xd[k]/zavg;
	    opmax->dat[nf]->yd[im] += ds->xd[k]/zavg;
	    opmax->dat[nf]->xd[im] = dsmax->xd[im] = im - 2*zavg;
	  }
	for(im = 0, dscor = opcor->dat[j]; im < dscor->nx; im++)
	  {
	    k = immax + im - dscor->nx/2;
	    k = (k < 0) ? 0 : k;
	    k = (k < ds->nx) ? k : ds->nx-1;
	    dscor->yd[im] = ds->yd[k];
	    dscor->yd[im] /= (ds->yd[immax] > 0) ? ds->yd[immax] : 1;
	    opcor->dat[nf]->yd[im] += dscor->yd[im];
	    opcor->dat[nf]->xd[im] = dscor->xd[im] = im - dscor->nx/2;
	  }
	
	oibf->im.nxs = 0;	 oibf->im.nxe = oibf->im.nx;
	oibf->im.nys = 0;	 oibf->im.nye = oibf->im.ny;
    }
    for(im = 0, dsmax = opmax->dat[nf]; im < dsmax->nx; im++)
      dsmax->yd[im] /= (nf > 1) ? nf : 1;
    for(im = 0, dscor = opcor->dat[nf]; im < dscor->nx; im++)
      dscor->yd[im] /= (nf > 1) ? nf : 1;
    alloc_data_set_y_error(ds);
    for(j = 0, dsmax = opmax->dat[nf]; j < nf; j++)
      {
	for(im = 0; im < dsmax->nx; im++)
	  {
	    k = (int)(dsop->xd[j]) + im - 2*zavg;
	    k = (k < 0) ? 0 : k;
	    k = (k < nfi) ? k : nfi-1;
	    ds->yd[im] = oidz->im.pxl[k][j/nxs].fl[j%nxs];
	    ds->ye[im] = sqrt(1 + ds->yd[im]);
	    ds->xd[im] = im - 2*zavg;
	  }
	ds->nx = ds->ny = dsmax->nx;
	//find_best_a_and_b_of_2_ds(ds, dsmax, 0, 0, &a, &b, &E);
	find_best_a_of_2_ds(ds, dsmax, 0, &a, &E);
	opq->dat[0]->yd[j] = a;
	opq->dat[0]->xd[j] = j;
	opq->dat[1]->yd[j] = b;
	opq->dat[1]->xd[j] = j;
	opq->dat[2]->yd[j] = E;
	opq->dat[2]->xd[j] = j;
	if (j == seedref)
	  {
	    add_data_to_one_plot(opmax, IS_DATA_SET, (void*)duplicate_data_set(ds, NULL));
	  }
      }
    for(j = 0; j < nf; j++)
      {
	for(im = 0; im < nfi; im++)
	  {
    	    oppro->dat[j]->xd[im] = bin*oidx->im.pxl[im][j/nxs].fl[j%nxs];
    	    oppro->dat[j]->yd[im] = bin*oidy->im.pxl[im][j/nxs].fl[j%nxs];
	    oppro->dat[j]->user_ispare[0]=j%nxs;
	    oppro->dat[j]->user_ispare[1]=j/nxs;	    
	  }
      }
    // we synchronize dx and dy of different subsets
    for (j = 0; j < nf; j++)
      {
	for(im = 0; im < dsmeandxy->nx; im++)
	  {
	    tmp = oimaxq->im.pxl[im][j/nxs].fl[j%nxs];
	    if (tmp > 3)
	      {
		tmp = exp(tmp/2);
		dsmeandxy->xd[im] += tmp*oidx->im.pxl[im][j/nxs].fl[j%nxs];
		dsmeandxy->yd[im] += tmp*oidy->im.pxl[im][j/nxs].fl[j%nxs];
		dsmeandxy->ye[im] += tmp;		
	      }
	  }
      }
    for(im = 0; im < dsmeandxy->nx; im++)
      {
	tmp = dsmeandxy->ye[im];
	dsmeandxy->xd[im] /= (tmp > 0) ? tmp : 1;
	dsmeandxy->yd[im] /= (tmp > 0) ? tmp : 1;	
      }


    for (j = 0; j < nf; j++)
    {
      jy = j/nxs; jx = j%nxs;
      find_subset_pos(ois, size, size, j, &x, &y);
      for(im = 0; im < nfi; im++)
	{
	  tmp = oimaxq->im.pxl[im][jy].fl[jx];
	  if (tmp < 3)
	    {  // when signal too low we replace by the mean
	      oidx->im.pxl[im][jy].fl[jx] = dsmeandxy->xd[im];
	      oidy->im.pxl[im][jy].fl[jx] = dsmeandxy->yd[im];
	      opoi->dat[im]->xd[2*j] = (size/2) + x;
	      opoi->dat[im]->yd[2*j] = (size/2) + y;
	      opoi->dat[im]->xd[2*j+1] = (oidx->im.pxl[im][jy].fl[jx]*bin) + x;
	      opoi->dat[im]->yd[2*j+1] = (oidy->im.pxl[im][jy].fl[jx]*bin) + y;
	    }
	}
    }

    
    //we create the new image with motion removed
    float xp, yp;
    int s_8 = size/8, s_2 = size/2, s_3 = (3*size)/4, dx0m, dx1m, dy0m, dy1m;

    sizere = ((startre + sizere) <= nfi) ? sizere : nfi- startre;     
    dx0m = 0; dx1m = ois->im.nx; dy0m = 0; dy1m = ois->im.ny;
    for (im = 0; im < sizere; im++)
      {
	switch_frame(ois,startre+im);
	switch_frame(oidx,startre+im);
	switch_frame(oidy,startre+im);		
	for (j = 0; j < nf; j++)
	  {
	    if ((j%nxs > 0) && (j%nxs < nxs-1)
		&& (j/nxs > 0) && (j/nxs < nxs-1)) continue;
	    find_subset_pos(ois, size, size, j, &x, &y);
	    for (jy = s_8; jy < size - s_8; jy++)
	      {
		if ((jy + y) < 0 || (jy + y) >= ois->im.ny) continue; 
		for (jx = s_8; jx < size - s_8; jx++)
		  {
		    if ((jx + x) < 0 || (jx + x) >= ois->im.nx) continue;
		    xp = ((float)(jx-s_2)/s_3)+j%nxs;
		    yp = ((float)(jy-s_2)/s_3)+j/nxs;
		    dx = bin*interpolate_image_point(oidx, xp, yp, NULL, NULL);
		    dy = bin*interpolate_image_point(oidy, xp, yp, NULL, NULL);
		    if (dx+jx+x-s_2 < 0 && jx+x > dx0m) dx0m = jx+x;
		    if (dx+jx+x-s_2 >= ois->im.nx && jx+x < dx1m) dx1m = jx+x;
		    if (dy+jy+y-s_2 < 0 && jy+y > dy0m) dy0m = jy+y;
		    if (dy+jy+y-s_2 >= ois->im.ny && jy+y < dy1m) dy1m = jy+y;		    
		  }
	      }
	  }
      }
    if (sizere > 0)
      {
	if (outcolor == 0)
	  oire = create_and_attach_movie_to_imr(imr, dx1m-dx0m, dy1m-dy0m, ois->im.data_type, sizere);
	else oire = create_and_attach_movie_to_imr(imr, ois->im.nx, ois->im.ny, ois->im.data_type, sizere);
	if (oire == NULL) return win_printf_OK("cannot create result movie");
      }


    for (im = 0; im < sizere; im++)
      {
	switch_frame(oire,im);
	switch_frame(ois,startre+im);
	switch_frame(oidx,startre+im);
	switch_frame(oidy,startre+im);		
	for (j = 0; j < nf; j++)
	  {
	    find_subset_pos(ois, size, size, j, &x, &y);
	    for (jy = s_8; jy < size - s_8; jy++)
	      {
		if (outcolor == 0)
		  {
		    if ((jy + y ) < dy0m || (jy + y) >= dy1m) continue; 
		    for (jx = s_8; jx < size - s_8; jx++)
		      {
						if ((jx + x) < dx0m || (jx + x) >= dx1m) continue;
			xp = ((float)(jx-s_2)/s_3)+j%nxs;
			yp = ((float)(jy-s_2)/s_3)+j/nxs;
			dx = bin*interpolate_image_point(oidx, xp, yp, NULL, NULL);
			dy = bin*interpolate_image_point(oidy, xp, yp, NULL, NULL);
			oire->im.pixel[jy+y-dy0m].ch[jx+x-dx0m] = fast_interpolate_image_point(ois, dx+jx+x-s_2, dy+jy+y-s_2,0)/256;
		      }
		  }
		else
		  {
		    if ((jy + y ) < 0 || (jy + y) >= ois->im.ny) continue; 
		    for (jx = s_8; jx < size - s_8; jx++)
		      {
			if ((jx + x) < 0 || (jx + x) >= ois->im.nx) continue;
			xp = ((float)(jx-s_2)/s_3)+j%nxs;
			yp = ((float)(jy-s_2)/s_3)+j/nxs;
			dx = bin*interpolate_image_point(oidx, xp, yp, NULL, NULL);
			dy = bin*interpolate_image_point(oidy, xp, yp, NULL, NULL);
			oire->im.pixel[jy+y].ch[jx+x] = fast_interpolate_image_point(ois, dx+jx+x-s_2, dy+jy+y-s_2,-1)/256;
		      }
		  }
		  
	      }
	  }
      }
    
    t_c = my_uclock() - t_c;
    oim->need_to_refresh = ALL_NEED_REFRESH;
    find_zmin_zmax(oim);
    find_zmin_zmax(oidx);
    find_zmin_zmax(oidy);        
    find_zmin_zmax(oidz);
    find_zmin_zmax(oire);            
    to = get_my_uclocks_per_sec();
    win_printf("Process took %g S\nx0 %d x1 %d dx %d\ny0 %d y1 %d dy %d\n"
	       ,((double)t_c)/to,X0,X1,X1-X_1,Y0,Y1,Y1-Y_1);
    //add_image(imr, oid->im.data_type, (void*)oid);
    //add_image(imr, oidp->im.data_type, (void*)oidp);
    //add_image(imr, oidf->im.data_type, (void*)oidf);
    set_oi_horizontal_extend(oidx, (float)(X1+size-X0)/1024);
    set_oi_vertical_extend(oidx, (float)(Y1+size-Y0)/1024);
    set_oi_horizontal_extend(oidy, (float)(X1+size-X0)/1024);
    set_oi_vertical_extend(oidy, (float)(Y1+size-Y0)/1024);
    set_oi_horizontal_extend(oidz, (float)(X1+size-X0)/1024);
    set_oi_vertical_extend(oidz, (float)(Y1+size-Y0)/1024); 
    set_oi_horizontal_extend(oimaxq, (float)(X1+size-X0)/1024);
    set_oi_vertical_extend(oimaxq, (float)(Y1+size-Y0)/1024);   
    if (oire != NULL)
      {
	set_oi_horizontal_extend(oire, (float)(X1+size-X0)/1024);
	set_oi_vertical_extend(oire, (float)(Y1+size-Y0)/1024);
      }
    free_one_image(oip);
    free_one_image(oid);
    //free_one_image(oidft);    
    free_one_image(oidp);        
    free_one_image(apo);
    free_data_set(ds);
    if (im1) free(im1);
    if (im2) free(im2);    
    return refresh_image(imr, imr->n_oi - 1);
}



int do_find_negative_4_order_variance_by_cutting_in_small_parts(void)
{
    imreg *imr = NULL;
    O_i *ois = NULL, *oim = NULL, *oip = NULL, *oidp = NULL, *apo = NULL, *oid = NULL;
    O_i *oidz = NULL, *oitmp = NULL, *oidu = NULL;
    int i, j, k, x = 0, y = 0, nf, nfi, nxs = 0, nys = 0, im, X0 = 0, X1 = 0, X_1 = 0, Y0 = 0, Y1 = 0, Y_1 = 0;
    int sizex = 0, sizey = 0;
    static int size = 256;
    static unsigned long t_c, to = 0;
    static int keep_dc = 1, sub = 1, bin = 2, yonly = 0, treat0 = -1;
    static float sm = 0.0;
    float mean, sigma, sigp, sigm, val, mean2, sig3;
    int isigp, isigm, iabove, ibelow, imean2;
    
    if(updating_menu_state != 0)	return D_O_K;
    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
    i = win_scanf("This routine will cut an image in smaller ones\n"
                  "with 1/2 covering edge for correlation\n"
                  "define the size of the small image %8d\n"
		  "%R->undersample in X and Y or %r->in Y only\n"
		  "Define your undersampling %4d\n"
		  "%R->projection or %r->substract projection\n"
		  "For appodisation: %bkeep DC component\n"
		  "Decreasea appodisation to %6f \n"
		  "treat only %4d\n"
		  ,&size,&yonly,&bin,&sub,&keep_dc,&sm,&treat0);
    if (i == WIN_CANCEL) return 0;

    if (ois == NULL)      win_printf_OK("No image found");

    nfi = nb_of_frames_in_movie(ois);
    if (nfi < 2)      win_printf_OK("Not a movie %d frames",nfi);    
    nf = compute_nb_of_subset(ois, size, size,&nxs,&nys);
    win_printf("New image %d x %d x %d",nxs,nys,nfi);
    oim = create_and_attach_movie_to_imr(imr, size, size, ois->im.data_type, nfi);
    if (oim == NULL) return win_printf_OK("cannot create tmp movie");
    oidz = create_and_attach_movie_to_imr(imr, nxs, nys, IS_FLOAT_IMAGE, nfi);
    if (oidz == NULL) return win_printf_OK("cannot create result movie");
    sizex = (yonly) ? size : size/bin;
    sizey = size/bin;     
    oip = build_ortho_poly2x2(sizex, sizey, NULL);
    if (oip == NULL) return win_printf_OK("cannot build polynomial");    
    apo = create_appodisation_float_image(sizex, sizey, sm, 0);
    if (apo == NULL)	return win_printf_OK("Could not create apodisation image!");
    t_c = my_uclock();
    for (j = 0; j < nf; j++)
    {
        X_1 = (j != 0 && x != X_1)? x : X_1;
        Y_1 = (j != 0 && y != Y_1)? y : Y_1;
	find_subset_pos(ois, size, size, j, &x, &y);
	X0 = (j == 0 || x < X0) ? x : X0;
	X1 = (j == 0 || x > X1) ? x : X1;
	Y0 = (j == 0 || y < Y0) ? y : Y0;
	Y1 = (j == 0 || y > Y1) ? y : Y1;		
	//display_title_message("small image=%d x=%d y=%d",j,x,y);
	if (treat0 >= 0 && treat0 != j) continue;
	for (im = 0; im < nfi; im++)
	  {
	    switch_frame(oim,im);
	    switch_frame(ois,im);
	    copy_one_subset_of_image_and_border(oim, ois, x, y);
	  }
	if (oim == NULL) win_printf("Small movie %p",(void*)oim);
	oitmp = oidu;
	if (bin > 1)
	  {
	    if (yonly) oidu = undersample_image_of_char_in_y(oidu, oim, bin);
	    else oidu = undersample_image_of_char(oidu, oim, bin);
	    if (oitmp != NULL && oitmp != oidu)
	      win_printf("mem leak in undersample");
	    if (oidu == NULL) win_printf("undersampled  %p",(void*)oidu);
	  }
	oitmp = oidp;
	oidp = project_on_first_poly(oidp, (bin > 1) ? oidu : oim, oip, sub, NULL);
	if (oitmp != NULL && oitmp != oidp)
	  win_printf("mem leak in project");	
	if (oidp == NULL) win_printf("project %p ",(void*)oidp);
	oitmp = oid;
	oid = d2_im_appo(oid, oidp, 0, NULL, apo, 1, keep_dc);
	if (oitmp != NULL && oitmp != oid)
	  win_printf("mem leak in apo");
	if (oid == NULL) win_printf("apo %p",(void*)oid);
	for (im = 0;im < nfi; im++)
	  {
	    switch_frame(oid,im);
	    for (i = 0, mean = 0; i < oid->im.ny ; i++)
	      {
		for (k=0; k< oid->im.nx; k++)
		    mean += oid->im.pixel[i].fl[k];
	      }
	    mean /= oid->im.nx*oid->im.ny;
	    for (i = 0, sigma = sigm = sigp = 0, isigm = isigp = 0; i < oid->im.ny ; i++)
	      {
		for (k=0; k< oid->im.nx; k++)
		  {
		    val = oid->im.pixel[i].fl[k];
		    sigma += (val - mean)*(val-mean);
		    if (val > mean)
		      {
			sigp += (val - mean)*(val-mean) *(val - mean)*(val-mean);
			isigp++;
		      }
		    else
		      {
			sigm += (val - mean)*(val-mean) * (val - mean)*(val-mean);
			isigm++;
		      }
		  }
	      }
	    sigma /= (oid->im.nx*oid->im.ny - 1);
	    sigma = sqrt(sigma);
	    for (i = 0, mean2 = 0, imean2 = 0; i < oid->im.ny ; i++)
	      {
		for (k=0; k< oid->im.nx; k++)
		  {
		    val = oid->im.pixel[i].fl[k];
		    if (fabs(val - mean) < (2 *sigma))
		      {
			mean2 += val;
			imean2++;
		      }
		  }
	      }
	    mean2 = (imean2 > 0) ? mean2/imean2 : mean2;
	    for (i = 0, ibelow = 0, iabove = 0, imean2 = 0, sig3 = 0; i < oid->im.ny ; i++)
	      {
		for (k=0; k< oid->im.nx; k++)
		  {
		    val = oid->im.pixel[i].fl[k];
		    sig3 = (val -mean2)*(val -mean2)*(val -mean2);
		    if ((val - mean2) > 2*sigma)
		      {
			iabove++;
		      }
		    else if ((val - mean2) < (-2*sigma))
		      {
			ibelow++;
		      }
		    else imean2++;
		  }
	      }
	    sig3 /= (oid->im.nx*oid->im.ny - 1);
	    display_title_message("small image=%d x=%d y=%d mean %g sigma %g mean2 %g iabove %d ibelow %d imean2 %d",j,x,y,mean,sigma,mean2,iabove,ibelow,imean2);
	    sigm = (isigm > 0) ? sigm/isigm : sigm;
	    sigp = (isigp > 0) ? sigp/isigp : sigp;
	    switch_frame(oidz,im);
	    oidz->im.pixel[j/nxs].fl[j%nxs] = (sigma > 0) ? sig3/(sigma*sigma*sigma) : sig3;//ibelow-iabove;
	  }
    }
    t_c = my_uclock() - t_c;
    oim->need_to_refresh = ALL_NEED_REFRESH;
    find_zmin_zmax(oim);
    find_zmin_zmax(oidz);    
    to = get_my_uclocks_per_sec();
    win_printf("Process took %g S\nx0 %d x1 %d dx %d\ny0 %d y1 %d dy %d\n"
	       ,((double)t_c)/to,X0,X1,X1-X_1,Y0,Y1,Y1-Y_1);

    free_one_image(oip);
    free_one_image(oid);
    free_one_image(oidp);        
    free_one_image(apo);
    return refresh_image(imr, imr->n_oi - 1);
}


int do_max_NB_modes_profile(void)
{
    register int i, j;
    static int start = -1, end = -1, min = 0, fir = 16, maxval = 0;
    O_i *ois = NULL, *oid = NULL;
    int nf;
    d_s *ds = NULL;
    imreg *imr = NULL;
    int onx, ony, ix, iy;
    union pix *pd = NULL;
    float ypos = 0, Max_val = 0;


    if (ac_grep(cur_ac_reg, "%im%oi", &imr, &ois) != 2)
    {
        active_menu->flags |=  D_DISABLED;
        return D_O_K;
    }

    if (updating_menu_state != 0)
    {
        if (ois->im.n_f == 1 || ois->im.n_f == 0)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }

    nf = abs(ois->im.n_f);
    ony = ois->im.ny;
    onx = ois->im.nx;

    if (nf <= 0)
    {
        win_printf("This is not a movie");
        return D_O_K;
    }

    start = (start == -1) ? 0 : start;
    end = (end == -1 || nf < end) ? nf : end;
    i = win_scanf("I shall build an image with each pixel corresponding to:\n"
		  "Frame index of the max value\n"
		  "using a FIR filter, if yes averaging %4d points\n"
		  "%bAdd max value",&fir,&maxval);

    if (i == WIN_CANCEL)  return D_O_K;

    start = (start < 0) ? 0 : start;
    end = (nf < end) ? nf : end;

    if (maxval == 0) oid = create_and_attach_oi_to_imr(imr, onx, ony, IS_FLOAT_IMAGE);
    else             oid = create_and_attach_movie_to_imr(imr, onx, ony, IS_FLOAT_IMAGE,2);
    if (oid == NULL)  return win_printf_OK("cannot create profile !");

    set_im_title(oid, "%s taken in frame [%d,%d[", ((min) ? "Min" : "Max"), start, end);
    set_oi_treatement(oid, "%s taken in frame [%d,%d[", ((min) ? "Min" : "Max"), start, end);
    inherit_from_im_to_im(oid, ois);
    uns_oi_2_oi(oid, ois);

    ds = build_data_set(nf,nf);
    pd = oid->im.pixel;
    for (ix = 0; ix < nf; ix++) ds->xd[ix] = ix;
    for (iy = 0; iy < ony; iy++)
      {
	for (ix = 0; ix < onx; ix++)
	  {
	    // 0, 0
	    extract_z_profile_from_movie(ois, ix, iy, ds->yd);
	    for(i = 0; i < ds->nx; i++)    ds->yd[i] *= ds->yd[i]/5;
	    for(i = 0; i < ds->nx; i++)    ds->yd[i] *= ds->yd[i]/5;	    
	    // -1, 0
	    if (ix > 0)
	      {
		extract_z_profile_from_movie(ois, (ix > 0) ? ix-1 : ix, iy, ds->xd);	    
		for(i = 0; i < ds->nx; i++)    ds->yd[i] *= ds->xd[i]/5;
	      }
	    // -1, -1
	    if (ix > 0 && iy > 0)
	      {	    
		extract_z_profile_from_movie(ois, (ix > 0) ? ix-1 : ix, (iy > 0) ? iy-1 : iy, ds->xd);
		for(i = 0; i < ds->nx; i++)    ds->yd[i] *= ds->xd[i]/5;
	      }
	    // -1, +1
	    if (ix > 0 && iy < ony-1)
	      {	    
		extract_z_profile_from_movie(ois, (ix > 0) ? ix-1 : ix, (iy < ony-1) ? iy+1 : ony-1, ds->xd);
		for(i = 0; i < ds->nx; i++)    ds->yd[i] *= ds->xd[i]/5;
	      }
	    // 1, 0
	    if (ix > onx-1)
	      {	    	    
		extract_z_profile_from_movie(ois, (ix < onx-1) ? ix+1 : onx-1, iy, ds->xd);	    
		for(i = 0; i < ds->nx; i++)    ds->yd[i] *= ds->xd[i]/5;
	      }
	    // 1, -1
	    if (ix > onx-1 && iy > 0)
	      {	    	    	    
		extract_z_profile_from_movie(ois, (ix < onx-1) ? ix+1 : onx-1, (iy > 0) ? iy-1 : iy, ds->xd);	    
		for(i = 0; i < ds->nx; i++)    ds->yd[i] *= ds->xd[i]/5;
	      }
	    // 1, 1
	    if (ix > onx-1 && iy < ony-1)
	      {	    	    	    
		extract_z_profile_from_movie(ois, (ix < onx-1) ? ix+1 : ix, (iy < ony-1) ? iy+1 : ony-1, ds->xd);
		for(i = 0; i < ds->nx; i++)    ds->yd[i] *= ds->xd[i]/5;
	      }
	    // 0, 1
	    if (iy < ony-1)
	      {	    	    	    	    
		extract_z_profile_from_movie(ois, ix, (iy < ony-1) ? iy+1 : ony-1, ds->xd);
		for(i = 0; i < ds->nx; i++)    ds->yd[i] *= ds->yd[i]/5;
	      }
	    if (iy > 0)
	      {	    	    	    	    
		// 0, -1
		extract_z_profile_from_movie(ois, ix, (iy > 0) ? iy-1 : 0, ds->xd);
		for(i = 0; i < ds->nx; i++)    ds->yd[i] *= ds->xd[i]/5;
	      }	    
	    
	    
	    for(i = 0; i < ds->nx-fir; i++)
	      {
		ds->xd[i] = 0;
		for(j = 0; j < fir; j++)
		  {
		    ds->xd[i] += ds->yd[i+j];
		  }
	      }
	    for(i = j = 0; i < ds->nx-fir; i++)
	      {
		if (i == 0 || ds->xd[i] > ypos)
		  {
		    j = i;
		    ypos = ds->xd[i];
		  }
	      }
	    find_max_around(ds->xd, ds->nx-fir, j, &ypos, &Max_val, NULL);
	    pd[iy].fl[ix] = ypos + (fir/2);
	    if (maxval == 1)
	      {
		oid->im.pxl[1][iy].fl[ix] = 100*pow(Max_val/fir,(double)1/9);
	      }
	  }
      }
    if (ds != NULL) free_data_set(ds);
    map_pixel_ratio_of_image_and_screen(oid, 1, 1);
    oid->need_to_refresh = ALL_NEED_REFRESH;
    return (refresh_image(imr, imr->n_oi - 1));
}


MENU *HyperFocus_image_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"average profile", do_HyperFocus_average_along_y,NULL,0,NULL);
  add_item_to_menu(mn,"grab small image", do_HyperFocus_image_grab_small_movie,NULL,0,NULL);
  add_item_to_menu(mn,"grab flatten movie", do_HyperFocus_image_grab_small_movie_flatten,NULL,0,NULL);

  add_item_to_menu(mn,"variance of movie", do_HyperFocus_image_draw_sigma_vs_z,NULL,0,NULL);
  add_item_to_menu(mn,"Power of movie frames", do_HyperFocus_image_draw_power_vs_z,NULL,0,NULL);  
  add_item_to_menu(mn,"variance of HP movie", do_HyperFocus_image_draw_sigma_vs_z_HP,NULL,0,NULL);
  add_item_to_menu(mn,"Correlation k = 0 between n and n+1",do_HyperFocus_image_draw_im_cor_n_n_1_vs_z ,NULL,0,NULL);
  add_item_to_menu(mn,"Correlation k = 0 between n and n+1 weighted",do_HyperFocus_image_draw_im_cor_n_n_1_vs_z_pond ,NULL,0,NULL);  


  add_item_to_menu(mn,"Z profile in x,y box",do_image_extract_z_profiles_in_box ,NULL,0,NULL);
  add_item_to_menu(mn,"Z profiles averaged in x,y box",do_image_extract_z_avg_profiles_in_box ,NULL,0,NULL);
  add_item_to_menu(mn,"Z profiles min in image", do_extract_significant_min_of_z_avg_profiles,NULL,0,NULL);

  add_item_to_menu(mn,"Z profiles min in image by FFT", do_HyperFocus_image_grab_small_movie_correlate_n_n_1,NULL,0,NULL);

  add_item_to_menu(mn,"HyperFocus in image by FFT", do_HyperFocus_image_scan_grab_small_movie_correlate_n_n_1,NULL,0,NULL);


  add_item_to_menu(mn,"Plot green versus red", do_RGB_image_green_vs_red_plot,NULL,0,NULL);
  add_item_to_menu(mn,"Filter green - red",do_convert_RGB_image_in_BW_green_minus_red,NULL,0,NULL);



  add_item_to_menu(mn,"Hyperfocus by variance",do_HyperFocus_by_sigma ,NULL,0,NULL);
  add_item_to_menu(mn,"Hyperfocus by variance of HP",do_HyperFocus_by_HP_filter ,NULL,0,NULL);

  add_item_to_menu(mn,"Project complex image on ds",do_project_complex_image_on_ds ,NULL,0,NULL);  

  add_item_to_menu(mn,"Image of Nb. of modes above noise",do_find_NB_modes_above_noise_by_cutting_in_small_parts,NULL,0,NULL);
  add_item_to_menu(mn,"Image of Nb. of modes above noise V2",do_find_NB_modes_above_noise_by_cutting_in_small_parts_ver2,NULL,0,NULL);
  add_item_to_menu(mn,"Find displacement of FT-BP image",  do_find_displacement_of_movie,NULL,0,NULL);  



  add_item_to_menu(mn,"Image of Neg variance^2",do_find_negative_4_order_variance_by_cutting_in_small_parts,NULL,0,NULL);
  add_item_to_menu(mn,"Max Nb. of modes above noise",do_max_NB_modes_profile,NULL,0,NULL);
  add_item_to_menu(mn,"Find best Z ",do_find_best_z_by_cutting_in_small_parts,NULL,0,NULL);    


  
  return mn;
}

int	HyperFocus_main(int argc, char **argv)
{
  (void)argc;
  (void)argv;

  add_image_treat_menu_item ( "HyperFocus", NULL, HyperFocus_image_menu(), 0, NULL);
  return D_O_K;
}

int	HyperFocus_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  remove_item_to_menu(image_treat_menu, "HyperFocus", NULL, NULL);
  return D_O_K;
}
#endif

