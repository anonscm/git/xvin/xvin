/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _OLI95_C_
#define _OLI95_C_

# include "allegro.h"
# include "xvin.h"

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
//place here other headers of this plugin 
# include "allegro.h"
# include "xvin.h"

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "oli95.h"


int do_oli95_hello(void)
{
  register int i;

  if(updating_menu_state != 0)	return D_O_K;

  i = win_printf("Hello from oli95");
  if (i == CANCEL) win_printf_OK("you have press CANCEL");
  else win_printf_OK("you have press OK");
  return 0;
}

int do_oli95_fit_data_set_to_a_step(void)
{
  register int i, j, k;
  O_p *op = NULL, *opn = NULL;
  int nf, besti, inrange;
  double xi2b, xi2, avg1, avg2, avg1b, avg2b;
  static float sigma = 0.003, smin = 0.005, smax = 0.020, ximin = 30, ximax = 65;
  static int minsize = 8, sdraw = 0, filter = 0;
  d_s *ds, *dsi;
  pltreg *pr = NULL;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  nf = dsi->nx;	/* this is the number of points in the data set */
  i = win_scanf("I shall fit a single step to all the data set\n"
		"minimal size of the step %8d\n"
		"Noise \\sigma  =%8f\n"
		"And draw:\n""%R->Step position\n"
		"%r->step size\n%r->\\chi^2\n"
		"You can keep all points %b or\n"
		"Keep points if step size in [%8f,%8f]\n"
		"Keep points if \\chi^2 in [%8f,%8f]\n"
		,&minsize,&sigma,&sdraw,&filter,&smin,&smax,&ximin,&ximax);
  if (i == CANCEL)	return OFF;

  if ((opn = create_and_attach_one_plot(pr, op->n_dat, op->n_dat, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  ds = opn->dat[0];

  for (k = 0; k < op->n_dat; k++)
    {
      dsi = op->dat[k];
      nf = dsi->nx;
      for (i = minsize; i < nf - minsize; i++)
	{
	  for (j = 0, avg1 = 0; j < i; j++) avg1 += dsi->yd[j];
	  for (j = i, avg2 = 0; j < nf; j++) avg2 += dsi->yd[j];
	  avg1 /= i;
	  avg2 /= (nf-i);
	  xi2 = 0;
	  for (j = 0; j < i; j++)   xi2 += (dsi->yd[j]-avg1)*(dsi->yd[j]-avg1);
	  for (j = i; j < nf; j++)  xi2 += (dsi->yd[j]-avg2)*(dsi->yd[j]-avg2);
	  xi2 /= (sigma*sigma);
	  if ((i == minsize) || (xi2 < xi2b)) 
	    {
	      besti = i;
	      xi2b = xi2;
	      avg1b = avg1;
	      avg2b = avg2;
	    }
	}
      ds->xd[k] = k;
      inrange = (xi2b > ximin && xi2b < ximax) ? 1 : 0;
      inrange = (((avg1b-avg2b) > smin) && ((avg1b-avg2b) < smax)) ? inrange : 0;
      if (filter || inrange)
	{
	  if (sdraw == 0) ds->yd[k] = besti;
	  else if (sdraw == 1) ds->yd[k] = avg1b-avg2b;
	  else if (sdraw == 2) ds->yd[k] = xi2b;
	}
    }

  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"Step found with \\sigma  = %f",sigma);
  set_plot_title(opn, "Step on ds");
  set_plot_x_title(opn, "Index of ds");
  if (sdraw == 0)      set_plot_y_title(opn, "Step position");
  else if (sdraw == 1) set_plot_y_title(opn, "Step size");
  else if (sdraw == 2) set_plot_y_title(opn, "\\chi^2");
  
  opn->filename = Transfer_filename(op->filename);

  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);


  //win_printf("Step at %d size %g\n\\chi^2 = %g\n",besti,avg1b-avg2b,xi2b/(sigma*sigma));
  /*
  for (j = 0; j < nf; j++)
  {
    ds->yd[j] = dsi->yd[j];
    ds->xd[j] = dsi->xd[j];
  }

  // now we must do some house keeping
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"y multiplied by");
  // refisplay the entire plot 
  refresh_plot(pr, UNCHANGED);
  */
  return D_O_K;
}

int do_oli95_rescale_plot(void)
{
  register int i, j;
  O_p *op = NULL, *opn = NULL;
  int nf;
  static float factor = 1;
  d_s *ds, *dsi;
  pltreg *pr = NULL;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number and place it in a new plot");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  nf = dsi->nx;	/* this is the number of points in the data set */
  i = win_scanf("By what factor do you want to multiply y %f",&factor);
  if (i == CANCEL)	return OFF;

  if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  ds = opn->dat[0];

  for (j = 0; j < nf; j++)
  {
    ds->yd[j] = factor * dsi->yd[j];
    ds->xd[j] = dsi->xd[j];
  }

  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"y multiplied by %f",factor);
  set_plot_title(opn, "Multiply by %f",factor);
  if (op->x_title != NULL) set_plot_x_title(opn, op->x_title);
  if (op->y_title != NULL) set_plot_y_title(opn, op->y_title);
  opn->filename = Transfer_filename(op->filename);
  uns_op_2_op(opn, op);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}

MENU *oli95_plot_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
  //add_item_to_menu(mn,"Hello example", do_oli95_hello,NULL,0,NULL);
  add_item_to_menu(mn,"Fit step in Y", do_oli95_fit_data_set_to_a_step,NULL,0,NULL);
  //add_item_to_menu(mn,"plot rescale in Y", do_oli95_rescale_plot,NULL,0,NULL);
  return mn;
}

int	oli95_main(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  add_plot_treat_menu_item ( "oli95", NULL, oli95_plot_menu(), 0, NULL);
  return D_O_K;
}

int	oli95_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  remove_item_to_menu(plot_treat_menu, "oli95", NULL, NULL);
  return D_O_K;
}
#endif

