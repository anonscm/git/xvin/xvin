#ifndef _OLI95_H_
#define _OLI95_H_

PXV_FUNC(int, do_oli95_rescale_plot, (void));
PXV_FUNC(MENU*, oli95_plot_menu, (void));
PXV_FUNC(int, do_oli95_rescale_data_set, (void));
PXV_FUNC(int, oli95_main, (int argc, char **argv));
#endif

