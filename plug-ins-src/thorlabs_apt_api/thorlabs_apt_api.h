/* This file was automatically generated.  Do not edit! */
#pragma once

#include "thorlabs_apt_struct.h"
#include "thorlabs_apt_commands.h"
#include <stdbool.h>
#include <stddef.h>

//data from http://www.thorlabs.com/software/apt/APT_Communications_Protocol_Rev_14.pdf
#define POS_ENC_CONV_FACTOR 34304.0
#define SPEED_ENC_CONV_FACTOR 767367.490218667
#define ACC_ENS_CONV_FACTOR 261.93

#define TL_APT_HOSTCTRL_DEST_MODULE 0x01
#define TL_APT_RACKCTRL_DEST_MODULE 0x11
#define TL_APT_BAY0_DEST_MODULE 0x21
#define TL_APT_BAY1_DEST_MODULE 0x22
#define TL_APT_BAY2_DEST_MODULE 0x23
#define TL_APT_BAY3_DEST_MODULE 0x24
#define TL_APT_BAY4_DEST_MODULE 0x25
#define TL_APT_BAY5_DEST_MODULE 0x26
#define TL_APT_BAY6_DEST_MODULE 0x27
#define TL_APT_BAY7_DEST_MODULE 0x28
#define TL_APT_BAY8_DEST_MODULE 0x29
#define TL_APT_BAY9_DEST_MODULE 0x2A
#define TL_APT_GENERIC_DEST_MODULE 0x50



int tl_apt_mot_get_velocity(tl_apt_context *context,int *min_velocity,int *acceleration,int *max_velocity);
int t1_apt_mot_get_bldist_l(tl_apt_context *context,float *bldist);
int tl_apt_mot_get_backlash_distance(tl_apt_context *context,int32_t *backlash_dist);
int tl_apt_mot_set_backlash_distance_l(tl_apt_context *context,float backlash_distance);
int tl_apt_mot_set_backlash_distance(tl_apt_context *context,int32_t backlash_distance);
int tl_apt_mot_set_velocity(tl_apt_context *context,int32_t min_velocity,int32_t acceleration,int32_t max_velocity);
int tl_apt_mot_move_rel(tl_apt_context *context);
int tl_apt_mot_move_absolute(tl_apt_context *context);
int tl_apt_mot_move_absolute_with_pos(tl_apt_context *context,int32_t absolute_pos);
int tl_apt_mot_get_move_abspos(tl_apt_context *context,int *absolute_pos);
int tl_apt_mot_get_PID(tl_apt_context *context,int *Proportionnal,int *Integral,int *Differential,int *Integral_Limit,unsigned short int *FilterControl);
int tl_apt_mot_set_PID(tl_apt_context *context,int32_t Proportionnal,int32_t Integral,int32_t Differential,int32_t Integral_Limit,uint16_t FilterControl);
int tl_apt_mot_set_mov_rel(tl_apt_context *context,int32_t rel_move);
int tl_apt_mot_set_move_abspos(tl_apt_context *context,int32_t absolute_pos);
int t1_apt_mot_get_statusbits(tl_apt_context *context,uint32_t *statusbits);
int tl_apt_mot_get_poscounter(tl_apt_context *context,int32_t *poscounter);
int t1_apt_mot_get_pos(tl_apt_context *context,float *pos);
int tl_apt_mot_set_poscounter(tl_apt_context *context,int32_t poscounter);
int tl_apt_mot_home(tl_apt_context *context);
int tl_apt_mot_set_homeparam(tl_apt_context *context,uint16_t home_dir,uint16_t limit_switch,int32_t home_velocity,int32_t offset_distance);
int t1_apt_mot_get_homeparam(tl_apt_context *context,uint16_t *home_dir,uint16_t *limit_switch,int32_t *home_velocity,int32_t *offset_distance);
int tl_apt_mot_move_stop(tl_apt_context *context);
int tl_apt_mod_get_channel_enablestate(tl_apt_context *context,uchar *state);
int tl_apt_mod_set_channel_enablestate(tl_apt_context *context,const uchar state);
int tl_apt_mot_no_end_of_move(tl_apt_context *context);
int tl_apt_mod_identify(tl_apt_context *context);
int tl_apt_mod_disconnect(tl_apt_context *context);
int tl_apt_close(tl_apt_context *context);
int tl_apt_cleanup(tl_apt_context *context);
int tl_apt_fill_data_header(tl_apt_context *context, tl_apt_header_t *outHeader, ushort command, ushort data_length);
int tl_apt_fill_params_header(tl_apt_context *context, tl_apt_header_t *outHeader, uint16_t command, char param1,
                              char param2);
int tl_apt_generic_get(tl_apt_context *context, uint16_t req_command, uint16_t get_command, tl_apt_header_t *out_header,
                       uchar *out_raw_data,
                       size_t raw_data_size);
int tl_apt_header_copy(tl_apt_header_t *dest, tl_apt_header_t *src);

tl_apt_context *tl_apt_init(int vendor,int product,uchar *dest_module,uint16_t *channel_id,int *err);
tl_apt_context *tl_apt_init_ws(int vendor,int product,uchar *dest_module,uint16_t *channel_id,int *err,char *APT_serial);
int tl_apt_cleanup(tl_apt_context *context);
int tl_apt_generic_get(tl_apt_context *context,uint16_t req_command,uint16_t get_command,tl_apt_header_t *out_header,uchar *out_raw_data,size_t raw_data_size);
int tl_apt_header_print(tl_apt_header_t *header);
int tl_apt_mod_disconnect(tl_apt_context *context);


int tl_apt_mod_identify(tl_apt_context *context);

int tl_apt_mod_set_channel_enablestate(tl_apt_context *context, const uchar state);
int tl_apt_mod_get_channel_enablestate(tl_apt_context *context, uchar *state);

int tl_apt_mot_move_stop(tl_apt_context *context);

int tl_apt_mot_set_poscounter(tl_apt_context *context, const int poscounter);
int tl_apt_mot_get_poscounter(tl_apt_context *context, int *poscounter);

int tl_apt_mot_set_move_abspos(tl_apt_context *context, const int abspos);
int tl_apt_mot_get_move_abspos(tl_apt_context *context, int *abspos);

int tl_apt_mot_move_absolute_with_pos(tl_apt_context *context, const int absolute_pos);
int tl_apt_mot_move_absolute(tl_apt_context *context);

void print_message(uchar *msg, int size);
tl_apt_context *tl_apt_init_ws(int vendor, int product, uchar *dest_module, uint16_t *channel_id, int *err,
                               char *APT_serial);
int tl_apt_mot_no_end_of_move(tl_apt_context *context);
int t1_apt_mot_get_homeparam(tl_apt_context *context, uint16_t *home_dir, uint16_t *limit_switch,
                             int32_t *home_velocity, int32_t *offset_distance);
int tl_apt_mot_set_homeparam(tl_apt_context *context, uint16_t home_dir, uint16_t limit_switch, int32_t home_velocity,
                             int32_t offset_distance);
int t1_apt_mot_get_pos(tl_apt_context *context, float *pos);
int t1_apt_mot_get_statusbits(tl_apt_context *context, uint32_t *statusbits);
int tl_apt_mot_get_pmdstageaxis_l(tl_apt_context *context, tl_apt_mot_pmdstageaxis_t *pmdstageaxis);
int tl_apt_mot_set_pmdstageaxis_l(tl_apt_context *context, tl_apt_mot_pmdstageaxis_t *pmdstageaxis);
int tl_apt_mot_set_backlash_distance(tl_apt_context *context, int32_t backlash_distance);
int tl_apt_mot_set_backlash_distance_l(tl_apt_context *context,			    float backlash_distance);
int tl_apt_mot_get_backlash_distance(tl_apt_context *context, int32_t *backlash_dist);
int t1_apt_mot_get_bldist_l(tl_apt_context *context, float *bldist);








//int tl_apt_mot_get_encposcount_per_unit(tl_apt_context *context, uint encposcount_per_unit);

//int tl_apt_mot_set_pmdstageaxis(tl_apt_context *context, tl_apt_mot_pmdstageaxis_t);

int tl_apt_mot_set_velocity(tl_apt_context *context, const int min_velocity, const int acceleration,
                            const int max_velocity);
int tl_apt_mot_get_velocity(tl_apt_context *context, int *min_velocity, int *acceleration, int *max_velocity);

int grab_zmag_plot_from_serial_port(void);

int tl_apt_mot_get_PID(tl_apt_context *context,
                       int *Proportionnal,
                       int *Integral,
                       int *Differential,
                       int *Integral_Limit,
                       unsigned short int *FilterControl);
int tl_apt_mot_set_PID(tl_apt_context *context,
                       const int Proportionnal,
                       const  int Integral,
                       const int Differential,
                       const int Integral_Limit,
                       const unsigned short int FilterControl);

int tl_apt_mot_home(tl_apt_context *context);
