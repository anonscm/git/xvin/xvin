//	Defines APT.DLL application programmers interface for accessing the APT system.
//	This interface can be included by C or C++ code.


//#include <Windows.h>	// This may need uncommenting in some environments.

#include "thorlabs_apt_api.h"
#include "xvin.h"

uchar g_current_channel = 0;


#ifdef XV_WIN32
#include "allegro.h"

#ifdef XV_WIN32
#include "winalleg.h"
#endif
#include <windows.h>

void psleep(unsigned long int milliseconds)
{
    Sleep(milliseconds);
}
#else
#include <unistd.h>

// void psleep(unsigned long int milliseconds)
// {
//     usleep(milliseconds * 1000); // takes microseconds
// }
#endif

void print_message(uchar *msg, int size)
{
    for (int i = 0; i < size; i++)
    {
        printf("%02X ", *(msg + i));
    }

    printf("\n");
}


int tl_apt_fill_data_header(tl_apt_context *context, tl_apt_header_t *outHeader, uint16_t command, uint16_t data_length)
{
    if (context == NULL || outHeader == NULL)
    {
        return 1;
    }

    outHeader->command = command;
    outHeader->dest_module = context->dest_module | 0x80;
    outHeader->src_module = context->src_module;
    outHeader->data.length = data_length;
    return 0;
}

int tl_apt_fill_params_header(tl_apt_context *context, tl_apt_header_t *outHeader, uint16_t command, char param1,
                              char param2)
{
    if (context == NULL || outHeader == NULL)
    {
        return 1;
    }

    outHeader->command = command;
    outHeader->dest_module = context->dest_module;
    outHeader->src_module = context->src_module;
    outHeader->data.params.first = param1;
    outHeader->data.params.second = param2;
    return 0;
}

int tl_apt_header_copy(tl_apt_header_t *dest, tl_apt_header_t *src)
{
    dest->command =     src->command;
    dest->data.length = src->data.length;
    dest->src_module =  src->src_module;
    dest->dest_module = src->dest_module;
    return 0;
}

int tl_apt_header_print(tl_apt_header_t *header)
{
    if (header)
    {
        if (header->dest_module & 0x80)
        {
            printf("header: command n0x%hx, data length: %hu, src: 0x%hhx, dest 0x%hhx\n", header->command, header->data.length,
                   header->src_module, header->dest_module);
        }
        else
        {
            printf("header: command n0x%hx, param1: 0x%hhx, param2: 0x%hhx, src: 0x%hhx, dest 0x%hhx\n", header->command,
                   header->data.params.first, header->data.params.second, header->src_module, header->dest_module);
        }
    }
    else
    {
        printf("header: NULL\n");
    }

    return 0;
}

int tl_apt_generic_get(tl_apt_context *context, uint16_t req_command, uint16_t get_command, tl_apt_header_t *out_header,
                       uchar *out_raw_data,
                       size_t raw_data_size)
{

    int ret = 0;
    tl_apt_header_t header = {0};
    tl_apt_hw_richresponse_t *richresponse = {0};
    uchar raw_data[255] = {0}; // by specification : a packages can't exceed 255 bytes
    uint t0 = 0, t1 = 0;
    ret = tl_apt_fill_params_header(context, &header, req_command, 0x01, 0x00);

    if (ret != 0)
    {
        warning_message("thorlabs_apt: can't purge buffers error: %d, %s\n", ret, ftdi_get_error_string(context->ftdi));
        tl_apt_cleanup(context);
        return -1;
    }

    ret = ftdi_write_data(context->ftdi, (uchar *) &header, sizeof(header));

    if (ret != sizeof(header))
    {
        warning_message("thorlabs_apt: can't send data, error: %d, %s\n", ret, ftdi_get_error_string(context->ftdi));
        tl_apt_cleanup(context);
        return -1;
    }

    t0 = t1 = my_uclock();

    while (ret >= 0 && (header.command != get_command))
    {
        t1 = my_uclock();

        if (t1 - t0 > get_my_uclocks_per_sec() * 2)
        {
            error_message("thorlabs_apt: no response to req command: 0x%hx -  timeout !\n", req_command);
            tl_apt_cleanup(context);
            return -1;
        }

        ret = ftdi_read_data(context->ftdi, (uchar *) &header, sizeof(header));

        if (ret > 0)
        {
            if (header.dest_module & 0x80)
            {
                ret = ftdi_read_data(context->ftdi, raw_data, header.data.length);

                if (ret < 0)
                {
                    warning_message("thorlabs_apt: can't read data, error: %d, %s\n", ret, ftdi_get_error_string(context->ftdi));
                }
            }

            if (header.command != get_command)
            {
                if (header.command == MGMSG_HW_RESPONSE)
                {
                    error_message("thorlabs_apt: hardware notified an error\n");
                }
                else if (header.command == MGMSG_HW_RICHRESPONSE)
                {
                    richresponse = (tl_apt_hw_richresponse_t *) &raw_data;
                    error_message("thorlabs_apt: hardware notified an error! \n"
                                  "message_id: %hx \n"
                                  "code: %hx \n"
                                  "notes: %s\n", richresponse->message_id, richresponse->code, richresponse->notes);
                }

                debug_message("thorlabs_apt: received event not matching with %hx\n", get_command);
                tl_apt_header_print(&header);
            }
        }
        else if (ret < 0)
        {
            warning_message("thorlabs_apt: can't read data, error: %d, %s\n", ret, ftdi_get_error_string(context->ftdi));
        }
    }

    if (header.command == get_command)
    {
        if (!(header.dest_module & 0x80) && raw_data_size == 0)
        {
            if (out_header)
            {
                tl_apt_header_copy(out_header, &header);
            }

            return 0;
        }
        else if (header.dest_module & 0x80 &&
                 header.data.length == raw_data_size)
        {
            if (ret == (int) raw_data_size)
            {
                if (out_header)
                {
                    tl_apt_header_copy(out_header, &header);
                }

                if (out_raw_data)
                {
                    for (size_t i = 0; i < raw_data_size; ++i)
                    {
                        out_raw_data[i] =  raw_data[i];
                    }
                }

                return 0;
            }
        }
    }


    error_message("thorlabs_apt: response from controller does not fit the specifications !\n");
    tl_apt_header_print(&header);
    tl_apt_cleanup(context);
    return -1;
}

tl_apt_context *tl_apt_init_ws(int vendor, int product, uchar *dest_module, uint16_t *channel_id, int *err,
                               char *APT_serial)
{
    tl_apt_context *context = (tl_apt_context *) calloc(1, sizeof(tl_apt_context));
    context->src_module = TL_APT_HOSTCTRL_DEST_MODULE; // src is always the host computer
    context->dest_module = dest_module ? *dest_module :
                           TL_APT_GENERIC_DEST_MODULE; // if dest not defined, used the generic USB unit
    context->channel_id = channel_id ? *channel_id : 0x01;
    int ret = 0;
    context->ftdi = ftdi_new();
    ret = ftdi_usb_open_desc(context->ftdi, vendor, product, NULL, APT_serial);

    if (ret != 0)
    {
        error_message("thorlabs_apt: can't open usb %x:%x, err: %d : %s\n", vendor, product, ret,
                      ftdi_get_error_string(context->ftdi));
        ftdi_free(context->ftdi);
        free(context);
        context = 0;

        if (err)
        {
            *err = ret;
        }

        return NULL;
    }

    ret = ftdi_set_baudrate(context->ftdi, 115200);

    if (ret != 0)
    {
        warning_message("thorlabs_apt: can't set baudrate, %d : %s\n", ret, ftdi_get_error_string(context->ftdi));
    }

    ret = ftdi_set_line_property(context->ftdi, BITS_8, STOP_BIT_1, NONE);

    if (ret != 0)
    {
        warning_message("thorlabs_apt: can't set line parameters %d : %s\n", ret, ftdi_get_error_string(context->ftdi));
    }

    // sleep can be necessary
    ret = ftdi_usb_purge_buffers(context->ftdi);

    if (ret != 0)
    {
        warning_message("thorlabs_apt: can't purge buffer %d : %s\n", ret, ftdi_get_error_string(context->ftdi));
    }

    //ret = ftdi_usb_reset(context->ftdi);

    if (ret != 0)
    {
        warning_message("thorlabs_apt: can't reset device %d : %s\n", ret, ftdi_get_error_string(context->ftdi));
    }

    ftdi_setflowctrl(context->ftdi,  SIO_RTS_CTS_HS);

    if (ret != 0)
    {
        warning_message("thorlabs_apt: can't set flow control %d : %s\n", ret, ftdi_get_error_string(context->ftdi));
    }

    ftdi_setrts(context->ftdi, 1);

    if (ret != 0)
    {
        warning_message("thorlabs_apt: can't set rts %d : %s\n", ret, ftdi_get_error_string(context->ftdi));
    }

    return context;
}

tl_apt_context *tl_apt_init(int vendor, int product, uchar *dest_module, uint16_t *channel_id, int *err)
{
    tl_apt_context *context = (tl_apt_context *) calloc(1, sizeof(tl_apt_context));
    context->src_module = TL_APT_HOSTCTRL_DEST_MODULE; // src is always the host computer
    context->dest_module = dest_module ? *dest_module :
                           TL_APT_GENERIC_DEST_MODULE; // if dest not defined, used the generic USB unit
    context->channel_id = channel_id ? *channel_id : 0x01;
    int ret = 0;
    context->ftdi = ftdi_new();
    ret = ftdi_usb_open(context->ftdi, vendor, product);

    if (ret != 0)
    {
        error_message("thorlabs_apt: can't open usb %x:%x, err: %d : %s\n", vendor, product, ret,
                      ftdi_get_error_string(context->ftdi));
        ftdi_free(context->ftdi);
        free(context);
        context = 0;

        if (err)
        {
            *err = ret;
        }

        return 0;
    }

    ret = ftdi_set_baudrate(context->ftdi, 115200);

    if (ret != 0)
    {
        warning_message("thorlabs_apt: can't set baudrate, %d : %s\n", ret, ftdi_get_error_string(context->ftdi));
    }

    ret = ftdi_set_line_property(context->ftdi, BITS_8, STOP_BIT_1, NONE);

    if (ret != 0)
    {
        warning_message("thorlabs_apt: can't set line parameters %d : %s\n", ret, ftdi_get_error_string(context->ftdi));
    }

    // sleep can be necessary
    ret = ftdi_usb_purge_buffers(context->ftdi);

    if (ret != 0)
    {
        warning_message("thorlabs_apt: can't purge buffer %d : %s\n", ret, ftdi_get_error_string(context->ftdi));
    }

    //ret = ftdi_usb_reset(context->ftdi);

    if (ret != 0)
    {
        warning_message("thorlabs_apt: can't reset device %d : %s\n", ret, ftdi_get_error_string(context->ftdi));
    }

    ftdi_setflowctrl(context->ftdi,  SIO_RTS_CTS_HS);

    if (ret != 0)
    {
        warning_message("thorlabs_apt: can't set flow control %d : %s\n", ret, ftdi_get_error_string(context->ftdi));
    }

    ftdi_setrts(context->ftdi, 1);

    if (ret != 0)
    {
        warning_message("thorlabs_apt: can't set rts %d : %s\n", ret, ftdi_get_error_string(context->ftdi));
    }

    return context;
}

int tl_apt_close(tl_apt_context *context)
{
    if (context != NULL)
    {
        tl_apt_mod_disconnect(context);

        if (context->ftdi)
        {
            ftdi_free(context->ftdi);
        }

        free(context);
    }

    return 0;
}

int tl_apt_cleanup(tl_apt_context *context)
{
    int ret = 0;
    if (context==NULL) return 0;
    ret = ftdi_usb_purge_buffers(context->ftdi);

    if (ret != 0)
    {
        warning_message("thorlabs_apt: can't purge buffers %d : %s\n", ret, ftdi_get_error_string(context->ftdi));
    }

    ftdi_usb_reset(context->ftdi);

    if (ret != 0)
    {
        warning_message("thorlabs_apt: can't reset %d : %s\n", ret, ftdi_get_error_string(context->ftdi));
    }

    return 0;
}

int tl_apt_mod_identify(tl_apt_context *context)
{
    tl_apt_header_t header = {0};
    int ret = 0;
    tl_apt_fill_params_header(context, &header, MGMSG_MOD_IDENTIFY, 0x0, 0x0);
    ret = ftdi_write_data(context->ftdi, (uchar *) &header, sizeof(header));

    if (ret < 0)
    {
        warning_message("thorlabs_apt: can't write data %d : %s\n", ret, ftdi_get_error_string(context->ftdi));
    }

    return ret != sizeof(header) ? ret : 0;
}

int tl_apt_mot_no_end_of_move(tl_apt_context *context)
{
    tl_apt_header_t header = {0};
    int ret = 0;
    tl_apt_fill_params_header(context, &header, MGMSG_MOT_SUSPEND_ENDOFMOVEMSGS, 0x0, 0x0);
    ret = ftdi_write_data(context->ftdi, (uchar *) &header, sizeof(header));

    if (ret < 0)
    {
        warning_message("thorlabs_apt: can't write data %d : %s\n", ret, ftdi_get_error_string(context->ftdi));
    }

    return ret != sizeof(header) ? ret : 0;
}




int tl_apt_mod_disconnect(tl_apt_context *context)
{
    tl_apt_header_t header = {0};
    int ret = 0;
    tl_apt_fill_params_header(context, &header, MGMSG_HW_DISCONNECT, 0x0, 0x0);
    ret = ftdi_write_data(context->ftdi, (uchar *) &header, sizeof(header));

    if (ret < 0)
    {
        warning_message("thorlabs_apt: can't write data %d : %s\n", ret, ftdi_get_error_string(context->ftdi));
    }

    return ret != sizeof(header) ? ret : 0;
}


int tl_apt_mod_set_channel_enablestate(tl_apt_context *context, const uchar state)
{
    tl_apt_header_t header = {0};
    int ret = 0;
    tl_apt_fill_params_header(context, &header, MGMSG_MOD_SET_CHANENABLESTATE, 0x01, state);
    ret = ftdi_write_data(context->ftdi, (uchar *) &header, sizeof(header));

    if (ret < 0)
    {
        warning_message("thorlabs_apt: can't write data %d : %s\n", ret, ftdi_get_error_string(context->ftdi));
    }

    return ret == sizeof(tl_apt_header_t) ? 0 : -1;
}

int tl_apt_mod_get_channel_enablestate(tl_apt_context *context, uchar *state)
{
    int ret = 0;
    tl_apt_header_t header = {0};
    ret = tl_apt_generic_get(context, MGMSG_MOD_REQ_CHANENABLESTATE,
                             MGMSG_MOD_GET_CHANENABLESTATE, &header, NULL, 0);

    if (ret == 0 && state)
    {
        *state = header.data.params.second;
    }

    return ret;
}

int tl_apt_mot_move_stop(tl_apt_context *context)
{
    tl_apt_header_t header = {0};
    int ret = 0;
    tl_apt_fill_params_header(context, &header, MGMSG_MOT_MOVE_STOP, 0x01, 0x02);
    ret = ftdi_write_data(context->ftdi, (uchar *) &header, sizeof(header));

    if (ret < 0)
    {
        warning_message("thorlabs_apt: can't write data %d : %s\n", ret, ftdi_get_error_string(context->ftdi));
    }

    return ret != sizeof(header) ? ret : 0;
}



int t1_apt_mot_get_homeparam(tl_apt_context *context, uint16_t *home_dir, uint16_t *limit_switch,
                             int32_t *home_velocity, int32_t *offset_distance)

{
    tl_apt_mot_homeparam_t homeparam_msg = {0};
    int ret = 0;
    ret = tl_apt_generic_get(context,
                             MGMSG_MOT_REQ_HOMEPARAMS,
                             MGMSG_MOT_GET_HOMEPARAMS,
                             NULL,
                             (uchar *) &homeparam_msg,
                             sizeof(homeparam_msg));

    if (ret == 0)
    {
        *home_dir = homeparam_msg.home_dir;
        *limit_switch = homeparam_msg.limit_switch;
        *home_velocity = homeparam_msg.home_velocity;
        *offset_distance = homeparam_msg.offset_distance;
    }

    return ret;
}


int tl_apt_mot_set_homeparam(tl_apt_context *context, uint16_t home_dir, uint16_t limit_switch, int32_t home_velocity,
                             int32_t offset_distance)
{
    int ret = 0;
    uchar msg[sizeof(tl_apt_header_t) + sizeof(tl_apt_mot_homeparam_t)] = {0};
    tl_apt_header_t *header = (tl_apt_header_t *) msg;
    tl_apt_mot_homeparam_t *homeparam_msg = (tl_apt_mot_homeparam_t *)(msg + sizeof(tl_apt_header_t));
    tl_apt_fill_data_header(context, header, MGMSG_MOT_SET_HOMEPARAMS, sizeof(tl_apt_mot_homeparam_t));
    homeparam_msg->channel_id = context->channel_id;
    homeparam_msg->home_dir = home_dir;
    homeparam_msg->limit_switch = limit_switch;
    homeparam_msg->home_velocity = home_velocity;
    homeparam_msg->offset_distance = offset_distance;
    ret = ftdi_write_data(context->ftdi, msg, sizeof(msg));


    if (ret < 0)
    {
        warning_message("thorlabs_apt: can't write data %d : %s\n", ret, ftdi_get_error_string(context->ftdi));
    }

    return ret == sizeof(msg) ? 0 : -1;
}


int tl_apt_mot_home(tl_apt_context *context)
{
    tl_apt_header_t header = {0};
    int ret = 0;
    tl_apt_fill_params_header(context, &header, MGMSG_MOT_MOVE_HOME, 0x01, 0x02);
    ret = ftdi_write_data(context->ftdi, (uchar *) &header, sizeof(header));

    if (ret < 0)
    {
        warning_message("thorlabs_apt: can't write data %d : %s\n", ret, ftdi_get_error_string(context->ftdi));
    }

    return ret != sizeof(header) ? ret : 0;
}



int tl_apt_mot_set_poscounter(tl_apt_context *context, int32_t poscounter)
{
    int ret = 0;
    uchar msg[sizeof(tl_apt_header_t) + sizeof(tl_apt_mot_poscounter_t)] = {0};
    tl_apt_header_t *header = (tl_apt_header_t *) msg;
    tl_apt_mot_poscounter_t *poscounter_msg = (tl_apt_mot_poscounter_t *)(msg + sizeof(tl_apt_header_t));
    tl_apt_fill_data_header(context, header, MGMSG_MOT_SET_POSCOUNTER, sizeof(tl_apt_mot_poscounter_t));
    poscounter_msg->channel_id = context->channel_id;
    poscounter_msg->poscounter = poscounter;
    ret = ftdi_write_data(context->ftdi, msg, sizeof(msg));

    if (ret < 0)
    {
        warning_message("thorlabs_apt: can't write data %d : %s\n", ret, ftdi_get_error_string(context->ftdi));
    }

    return ret == sizeof(msg) ? 0 : -1;
}
int t1_apt_mot_get_pos(tl_apt_context *context, float *pos)
{
    int i = 0;
    int poscounter = 0;
    i = tl_apt_mot_get_poscounter(context, &poscounter);
    *pos = (float) poscounter / POS_ENC_CONV_FACTOR;
    return i;
}


int t1_apt_mot_get_statusbits(tl_apt_context *context, uint32_t *statusbits)
{
    tl_apt_mot_statusbits_t statusbits_msg = {0};
    int ret = 0;
    ret = tl_apt_generic_get(context,
                             MGMSG_MOT_REQ_STATUSBITS,
                             MGMSG_MOT_GET_STATUSBITS,
                             NULL,
                             (uchar *) &statusbits_msg,
                             sizeof(statusbits_msg));

    if (ret == 0)
    {
        *statusbits = statusbits_msg.statusbits;
    }

    return ret;
}


int tl_apt_mot_get_poscounter(tl_apt_context *context, int32_t *poscounter)
{
    tl_apt_mot_poscounter_t poscounter_msg = {0};
    int ret = 0;
    ret = tl_apt_generic_get(context,
                             MGMSG_MOT_REQ_POSCOUNTER,
                             MGMSG_MOT_GET_POSCOUNTER,
                             NULL,
                             (uchar *) &poscounter_msg,
                             sizeof(poscounter_msg));

    if (ret == 0 && poscounter)
    {
        *poscounter = poscounter_msg.poscounter;
    }

    return ret;
}

int tl_apt_mot_set_move_abspos(tl_apt_context *context, int32_t absolute_pos)
{
    int ret = 0;
    uchar msg[sizeof(tl_apt_header_t) + sizeof(tl_apt_mot_move_abspos_t)] = {0};
    tl_apt_header_t *header = (tl_apt_header_t *) msg;
    tl_apt_mot_move_abspos_t *absolute_pos_msg = (tl_apt_mot_move_abspos_t *)(msg + sizeof(tl_apt_header_t));
    tl_apt_fill_data_header(context, header, MGMSG_MOT_SET_MOVEABSPARAMS, sizeof(tl_apt_mot_move_abspos_t));
    absolute_pos_msg->channel_id = context->channel_id;
    absolute_pos_msg->absolute_pos = absolute_pos;
    ret = ftdi_write_data(context->ftdi, msg, sizeof(msg));

    if (ret < 0)
    {
        warning_message("thorlabs_apt: can't write data %d : %s\n", ret, ftdi_get_error_string(context->ftdi));
    }

    return ret == sizeof(msg) ? 0 : -1;
}

int tl_apt_mot_set_mov_rel(tl_apt_context *context, int32_t rel_move)
{
    int ret = 0;
    uchar msg[sizeof(tl_apt_header_t) + sizeof(tl_apt_mot_move_abspos_t)] = {0};
    tl_apt_header_t *header = (tl_apt_header_t *) msg;
    tl_apt_mot_move_abspos_t *absolute_pos_msg = (tl_apt_mot_move_abspos_t *)(msg + sizeof(tl_apt_header_t));

    tl_apt_fill_data_header(context, header, MGMSG_MOT_SET_MOVERELPARAMS, sizeof(tl_apt_mot_move_abspos_t));

    absolute_pos_msg->channel_id = context->channel_id;
    absolute_pos_msg->absolute_pos = rel_move;

    ret = ftdi_write_data(context->ftdi, msg, sizeof(msg));
    if (ret < 0)
    {
        warning_message("thorlabs_apt: can't write data %d : %s\n", ret, ftdi_get_error_string(context->ftdi));
    }

    return ret == sizeof(msg) ? 0 : -1;
}


int tl_apt_mot_set_PID(tl_apt_context *context,
                       int32_t Proportionnal,
                       int32_t Integral,
                       int32_t Differential,
                       int32_t Integral_Limit,
                       uint16_t FilterControl)
{
    int ret = 0;
    uchar msg[sizeof(tl_apt_header_t) + sizeof(tl_apt_mot_PID_t)] = {0};
    tl_apt_header_t *header = (tl_apt_header_t *) msg;
    tl_apt_mot_PID_t *mot_pid_msg = (tl_apt_mot_PID_t *)(msg + sizeof(tl_apt_header_t));
    tl_apt_fill_data_header(context, header, MGMSG_MOT_SET_DCPIDPARAMS, sizeof(tl_apt_mot_PID_t));
    mot_pid_msg->channel_id = context->channel_id;
    mot_pid_msg->Proportionnal = Proportionnal;
    mot_pid_msg->Integral = Integral;
    mot_pid_msg->Differential = Differential;
    mot_pid_msg->Integral_Limit = Integral_Limit;
    mot_pid_msg->FilterControl = FilterControl;
    ret = ftdi_write_data(context->ftdi, msg, sizeof(msg));


    if (ret < 0)
    {
        warning_message("thorlabs_apt: can't write data %d : %s\n", ret, ftdi_get_error_string(context->ftdi));
    }

    return ret == sizeof(msg) ? 0 : -1;
}


int tl_apt_mot_get_PID(tl_apt_context *context,
                       int *Proportionnal,
                       int *Integral,
                       int *Differential,
                       int *Integral_Limit,
                       unsigned short int *FilterControl)
{
    int ret = 0;
    tl_apt_mot_PID_t pid_msg = {0};
    ret = tl_apt_generic_get(context,
                             MGMSG_MOT_REQ_DCPIDPARAMS,
                             MGMSG_MOT_GET_DCPIDPARAMS,
                             NULL,
                             (uchar *) &pid_msg,
                             sizeof(tl_apt_mot_PID_t));

    if (ret == 0)
    {
        if (Proportionnal)
        {
            *Proportionnal = pid_msg.Proportionnal;
        }

        if (Integral)
        {
            *Integral = pid_msg.Integral;
        }

        if (Differential)
        {
            *Differential = pid_msg.Differential;
        }

        if (Integral_Limit)
        {
            *Integral_Limit = pid_msg.Integral_Limit;
        }

        if (FilterControl)
        {
            *FilterControl = pid_msg.FilterControl;
        }
    }

    return ret;
}


int tl_apt_mot_get_move_abspos(tl_apt_context *context, int *absolute_pos)
{
    int ret = 0;
    tl_apt_mot_move_abspos_t absolute_pos_msg = {0};
    ret = tl_apt_generic_get(context,
                             MGMSG_MOT_REQ_MOVEABSPARAMS,
                             MGMSG_MOT_GET_MOVEABSPARAMS,
                             NULL,
                             (uchar *) &absolute_pos_msg,
                             sizeof(absolute_pos_msg));

    if (ret == 0 && absolute_pos)
    {
        *absolute_pos = absolute_pos_msg.absolute_pos;
    }

    return ret;
}

int tl_apt_mot_move_absolute_with_pos(tl_apt_context *context, int32_t absolute_pos)
{
    int ret = 0;
    uchar msg[sizeof(tl_apt_header_t) + sizeof(tl_apt_mot_move_abspos_t)] = {0};
    tl_apt_header_t *header = (tl_apt_header_t *) msg;
    tl_apt_mot_move_abspos_t *absolute_pos_msg = (tl_apt_mot_move_abspos_t *)(msg + sizeof(tl_apt_header_t));
    tl_apt_fill_data_header(context, header, MGMSG_MOT_MOVE_ABSOLUTE, sizeof(tl_apt_mot_move_abspos_t));
    absolute_pos_msg->channel_id = context->channel_id;
    absolute_pos_msg->absolute_pos = absolute_pos;
    ret = ftdi_write_data(context->ftdi, msg, sizeof(msg));


    if (ret < 0)
    {
        warning_message("thorlabs_apt: can't write data %d : %s\n", ret, ftdi_get_error_string(context->ftdi));
    }

    return ret == sizeof(msg) ? 0 : -1;
}

int tl_apt_mot_move_absolute(tl_apt_context *context)
{
    int ret = 0;
    tl_apt_header_t header = {0};
    tl_apt_fill_params_header(context, &header, MGMSG_MOT_MOVE_ABSOLUTE, (uint8_t)0x01, (uint8_t)0x00);
    ret = ftdi_write_data(context->ftdi, (uchar *) &header, sizeof(header));

    if (ret < 0)
    {
        warning_message("thorlabs_apt: can't write data %d : %s\n", ret, ftdi_get_error_string(context->ftdi));
    }

    return ret == sizeof(tl_apt_header_t) ? 0 : -1;
}

int tl_apt_mot_move_rel(tl_apt_context *context)
{
    int ret = 0;
    tl_apt_header_t header = {0};

    tl_apt_fill_params_header(context, &header, MGMSG_MOT_MOVE_RELATIVE, (uint8_t)0x01, (uint8_t)0x00);

    ret = ftdi_write_data(context->ftdi, (uchar *) &header, sizeof(header));
    if (ret < 0)
    {
        warning_message("thorlabs_apt: can't write data %d : %s\n", ret, ftdi_get_error_string(context->ftdi));
    }

    return ret == sizeof(tl_apt_header_t) ? 0 : -1;
}





int tl_apt_mot_get_pmdstageaxis_l(tl_apt_context *context, tl_apt_mot_pmdstageaxis_t *pmdstageaxis)
{
    int ret = 0;
    ret = tl_apt_generic_get(context,
                             MGMSG_MOT_REQ_PMDSTAGEAXISPARAMS,
                             MGMSG_MOT_GET_PMDSTAGEAXISPARAMS,
                             NULL,
                             (uchar *) &pmdstageaxis,
                             sizeof(tl_apt_mot_pmdstageaxis_t));
    return ret;
}


int tl_apt_mot_set_pmdstageaxis_l(tl_apt_context *context, tl_apt_mot_pmdstageaxis_t *pmdstageaxis)
{
    int ret = 0;
    uchar msg[sizeof(tl_apt_header_t) + sizeof(tl_apt_mot_pmdstageaxis_t)] = {0};
    tl_apt_header_t *header = (tl_apt_header_t *) msg;
    tl_apt_fill_data_header(context, header, MGMSG_MOT_SET_PMDSTAGEAXISPARAMS, sizeof(tl_apt_mot_pmdstageaxis_t));
    tl_apt_mot_pmdstageaxis_t *pmdstage_msg = (tl_apt_mot_pmdstageaxis_t *)(msg + sizeof(tl_apt_header_t));
    *pmdstage_msg = *pmdstageaxis;
    ret = ftdi_write_data(context->ftdi, msg, sizeof(msg));

    if (ret < 0)
    {
        warning_message("thorlabs_apt: can't write data %d : %s\n", ret, ftdi_get_error_string(context->ftdi));
    }

    return ret == sizeof(msg) ? 0 : -1;
}

int tl_apt_mot_set_velocity(tl_apt_context *context,
                            int32_t min_velocity,
                            int32_t acceleration,
                            int32_t max_velocity)
{
    int ret = 0;
    uchar msg[sizeof(tl_apt_header_t) + sizeof(tl_apt_mot_velocity_t)] = {0};
    tl_apt_header_t *header = (tl_apt_header_t *) msg;
    tl_apt_mot_velocity_t *velocity_msg = (tl_apt_mot_velocity_t *)(msg + sizeof(tl_apt_header_t));
    tl_apt_fill_data_header(context, header, MGMSG_MOT_SET_VELPARAMS, sizeof(tl_apt_mot_velocity_t));
    velocity_msg->channel_id = context->channel_id;
    velocity_msg->min_velocity = min_velocity;
    velocity_msg->acceleration = acceleration;
    velocity_msg->max_velocity = max_velocity;
    ret = ftdi_write_data(context->ftdi, msg, sizeof(msg));

    if (ret < 0)
    {
        warning_message("thorlabs_apt: can't write data %d : %s\n", ret, ftdi_get_error_string(context->ftdi));
    }

    return ret == sizeof(msg) ? 0 : -1;
}


int tl_apt_mot_set_backlash_distance(tl_apt_context *context,
                                     int32_t backlash_distance)
{
    int ret = 0;
    uchar msg[sizeof(tl_apt_header_t) + sizeof(tl_apt_mot_backlash_t)] = {0};
    tl_apt_header_t *header = (tl_apt_header_t *) msg;
    tl_apt_mot_backlash_t *backlash_msg = (tl_apt_mot_backlash_t *)(msg + sizeof(tl_apt_header_t));
    tl_apt_fill_data_header(context, header, MGMSG_MOT_SET_GENMOVEPARAMS, sizeof(tl_apt_mot_backlash_t));
    backlash_msg->channel_id = context->channel_id;
    backlash_msg->backlash_distance = backlash_distance;
    printf("bldistance=%d\n", backlash_distance);
    ret = ftdi_write_data(context->ftdi, msg, sizeof(msg));

    if (ret < 0)
    {
        warning_message("thorlabs_apt: can't write data %d : %s\n", ret, ftdi_get_error_string(context->ftdi));
    }

    return ret == sizeof(msg) ? 0 : -1;
}
int tl_apt_mot_set_backlash_distance_l(tl_apt_context *context,
                                       float backlash_distance)
{
    int ret = tl_apt_mot_set_backlash_distance(context,
              (uint32_t)(backlash_distance * POS_ENC_CONV_FACTOR));
    return ret;
}


int tl_apt_mot_get_backlash_distance(tl_apt_context *context, int32_t *backlash_dist)
{
    int ret = 0;
    tl_apt_mot_backlash_t backlash_distance = {0};
    ret = tl_apt_generic_get(context,
                             MGMSG_MOT_REQ_GENMOVEPARAMS,
                             MGMSG_MOT_GET_GENMOVEPARAMS,
                             NULL,
                             (uchar *) &backlash_distance,
                             sizeof(tl_apt_mot_backlash_t));

    if (ret == 0)
    {
        *backlash_dist = backlash_distance.backlash_distance;
    }

    return ret;
}

int t1_apt_mot_get_bldist_l(tl_apt_context *context, float *bldist)
{
    int i = 0;
    int32_t backlash_distint = 0;
    i = tl_apt_mot_get_backlash_distance(context, &backlash_distint);
    *bldist = backlash_distint / POS_ENC_CONV_FACTOR;
    return i;
}


int tl_apt_mot_get_velocity(tl_apt_context *context,
                            int *min_velocity,
                            int *acceleration,
                            int *max_velocity)
{
    int ret = 0;
    tl_apt_mot_velocity_t velocity_msg = {0};
    ret = tl_apt_generic_get(context,
                             MGMSG_MOT_REQ_VELPARAMS,
                             MGMSG_MOT_GET_VELPARAMS,
                             NULL,
                             (uchar *) &velocity_msg,
                             sizeof(tl_apt_mot_velocity_t));

    if (ret == 0)
    {
        if (min_velocity)
        {
            *min_velocity = velocity_msg.min_velocity;
        }

        if (max_velocity)
        {
            *max_velocity = velocity_msg.max_velocity;
        }

        if (acceleration)
        {
            *acceleration = velocity_msg.acceleration;
        }
    }

    return ret;
}
