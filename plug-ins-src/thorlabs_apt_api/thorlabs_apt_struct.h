#pragma once
#include <libftdi1/ftdi.h>

typedef unsigned char uchar;
typedef unsigned short ushort;
typedef unsigned int uint;

typedef struct
{
    struct ftdi_context *ftdi;
    char dest_module;
    char src_module;
    uint16_t channel_id;
} __attribute__((__packed__))
tl_apt_context;

typedef struct
{
    uint16_t command;
    union
    {
        uint16_t length;
        struct
        {
            char first;
            char second;
        } __attribute__((__packed__)) params;
    } data;
    char dest_module;
    char src_module;
} __attribute__((__packed__))
tl_apt_header_t;

typedef struct
{
    uint16_t message_id;
    uint16_t code;
    char notes[64];

} __attribute__((__packed__))
tl_apt_hw_richresponse_t;

typedef struct
{
    uint16_t channel_id;
    int32_t poscounter;

} __attribute__((__packed__))
tl_apt_mot_poscounter_t;

typedef struct
{
    uint16_t channel_id;
    uint32_t statusbits;

} __attribute__((__packed__))
tl_apt_mot_statusbits_t;

typedef struct
{
    uint16_t channel_id;
    uint16_t home_dir;
    uint16_t limit_switch;
    int32_t home_velocity;
    int32_t offset_distance;
} __attribute__((__packed__))
tl_apt_mot_homeparam_t;

typedef struct
{
    ushort channel_id;
    uint16_t stage_id;
    uint16_t axis_id;
    char partno_axis[16];
    uint32_t serial_num;
    uint32_t count_per_unit;
    int32_t min_pos;
    int32_t max_pos;
    int32_t max_accn;
    int32_t max_dec;
    int32_t max_vel;
    uint16_t reserved_word[4];
    uint32_t reserved_dword[4];
} __attribute__((__packed__))
tl_apt_mot_pmdstageaxis_t;



typedef struct
{
    uint16_t channel_id;
    int32_t backlash_distance;

} __attribute__((__packed__))
tl_apt_mot_backlash_t;

typedef struct
{
    uint16_t channel_id;
    int32_t min_velocity;
    int32_t acceleration;
    int32_t max_velocity;

}  __attribute__((__packed__))
tl_apt_mot_velocity_t;

typedef struct
{
    uint16_t channel_id;
    int32_t absolute_pos;

}  __attribute__((__packed__))
tl_apt_mot_move_abspos_t;

typedef struct
{
    uint16_t channel_id;
    int32_t Proportionnal;
    int32_t Integral;
    int32_t Differential;
    int32_t Integral_Limit;
    uint16_t FilterControl;

}  __attribute__((__packed__))
tl_apt_mot_PID_t;
