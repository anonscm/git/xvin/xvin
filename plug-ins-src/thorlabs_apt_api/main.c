/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */

# include "allegro.h"
# include "xvin.h"

/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "main.h"
#include "thorlabs_apt_api.h"

//to be adapted for other syringes
#define TRAVEL_SYRINGE_VOLUME 20000
#define TRAVEL_SYRINGE_RANGE 65.


tl_apt_context *g_context = NULL;

int do_init(void)
{
    if(updating_menu_state != 0)
      {
	if (g_context == NULL) active_menu->flags &= ~D_DISABLED;
	//else active_menu->flags |=  D_DISABLED;
	return D_O_K;
      }

    if (g_context)
    {
        win_printf("please close your device before init again");

    }

    g_context = tl_apt_init(0x0403, 0xfaf0, NULL, NULL, NULL);

    if (g_context == NULL)
    {
        g_context = tl_apt_init(0x1569, 0x2001, NULL, NULL, NULL);
    }
    do_update_menu();
    return 0;
}
int do_close(void)
{
    if(updating_menu_state != 0)
      {
	if (g_context == NULL) active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }
    tl_apt_close(g_context);
    g_context = NULL;

    return 0;
}

int do_cleanup(void)
{
    if(updating_menu_state != 0)
      {
	if (g_context == NULL) active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }


    if (g_context == NULL)
    {
        return win_printf_OK("please init before use");
    }

    tl_apt_cleanup(g_context);

    return 0;
}

int do_identify(void)
{
    int ret;

    if(updating_menu_state != 0)
      {
	if (g_context == NULL) active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }
    if (g_context == NULL)
    {
        return win_printf_OK("please init before use");
    }

    ret = tl_apt_mod_identify(g_context);

    if (ret != 0)
        win_printf("can't load identify, err: %d", ret);


    return 0;

}
int do_get_count_per_unit(void)
{
    tl_apt_mot_pmdstageaxis_t pmdstageaxis;

    if(updating_menu_state != 0)
      {
	if (g_context == NULL) active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }
    if (g_context == NULL)
    {
        return win_printf_OK("please init before use");
    }

    if (tl_apt_mot_get_pmdstageaxis_l(g_context, &pmdstageaxis) == 0)
    {
        win_printf("poscount per unit: %d", pmdstageaxis.count_per_unit);
    }
    else
        warning_message("unable to get pmdstageaxis\n");

    return 0;

}
int do_get_state(void)
{
    uchar state = 0;
    int istate = 0;

    if(updating_menu_state != 0)
      {
	if (g_context == NULL) active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }

    if (g_context == NULL)
    {
        return win_printf_OK("please init before use");
    }

    if (tl_apt_mod_get_channel_enablestate(g_context, &state) == 0)
    {
        istate = state;
        win_printf("state : %d", istate);
    }
    else
        warning_message("unable to get state\n");

    return 0;

}
int do_stop_move(void)
{
  //int ret = 0;

    if(updating_menu_state != 0)
      {
	if (g_context == NULL) active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }

    if (g_context == NULL)
    {
        return win_printf_OK("please init before use");
    }

    if (tl_apt_mot_move_stop(g_context) != 0)
    {
        warning_message("unable to stop move\n");
    }

    return D_O_K;
}

int do_set_state(void)
{
    uchar state = 0;
    int istate = 0;
    int ret = 0;

    if(updating_menu_state != 0)
      {
	if (g_context == NULL) active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }


    if (g_context == NULL)
    {
        return win_printf_OK("please init before use");
    }

    if (tl_apt_mod_get_channel_enablestate(g_context, &state) != 0)
        warning_message("unable to get state\n");

    istate = state;
    ret = win_scanf("state: %d", &istate);
    state = istate;

    if (ret == WIN_CANCEL)         return D_O_K;


    if (tl_apt_mod_set_channel_enablestate(g_context, state) != 0)
    {
        warning_message("unable to set state\n");
    }

    return D_O_K;

}

int do_get_poscounter(void)
{
    int poscounter = 0;

    if(updating_menu_state != 0)
      {
	if (g_context == NULL) active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }

    if (g_context == NULL)
    {
        return win_printf_OK("please init before use");
    }

    if (tl_apt_mot_get_poscounter(g_context, &poscounter) == 0)
    {
        win_printf("poscounter: %d = %g mm", poscounter
		   ,(float)poscounter/POS_ENC_CONV_FACTOR);
    }
    else
        warning_message("unable to get state\n");

    return 0;

}
int do_set_poscounter(void)
{
    int poscounter = 0;
    int ret = 0;

    if(updating_menu_state != 0)
      {
	if (g_context == NULL) active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }

    if (g_context == NULL)
    {
        return win_printf_OK("please init before use");
    }

    if (tl_apt_mot_get_poscounter(g_context, &poscounter) != 0)
        warning_message("unable to get state\n");

    ret = win_scanf("poscounter: %d", &poscounter);

    if (ret == WIN_CANCEL)        return D_O_K;


    if (tl_apt_mot_set_poscounter(g_context, poscounter) != 0)
    {
        warning_message("unable to set state\n");
    }

    return D_O_K;
}


int do_mot_home(void)
{
    int poscounter = 0;
    int ret = 0;

    if(updating_menu_state != 0)
      {
	if (g_context == NULL) active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }

    if (g_context == NULL)
    {
        return win_printf_OK("please init before use");
    }

    if (tl_apt_mot_home(g_context) != 0)
        warning_message("unable to home\n");

    return D_O_K;
}

int do_get_move_abspos(void)
{
    int absolute_pos = 0;

    if(updating_menu_state != 0)
      {
	if (g_context == NULL) active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }

    if (g_context == NULL)
    {
        return win_printf_OK("please init before use");
    }

    if (tl_apt_mot_get_move_abspos(g_context, &absolute_pos) == 0)
    {
        win_printf("absolute_pos: %d", absolute_pos);
    }
    else
        warning_message("unable to get\n");

    return 0;

}
int do_set_move_abspos(void)
{
    int absolute_pos = 0;
    int ret = 0;

    if(updating_menu_state != 0)
      {
	if (g_context == NULL) active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }

    if (g_context == NULL)
    {
        return win_printf_OK("please init before use");
    }

    if (tl_apt_mot_get_move_abspos(g_context, &absolute_pos) != 0)
        warning_message("unable to get\n");

    ret = win_scanf("absolute_pos: %d", &absolute_pos);

    if (ret == WIN_CANCEL)        return D_O_K;


    if (tl_apt_mot_set_move_abspos(g_context, absolute_pos) != 0)
    {
        warning_message("unable to set \n");
    }

    return D_O_K;
}

int do_move_abspos(void)
{
    int absolute_pos = 0;
    int ret = 0;

    if(updating_menu_state != 0)
      {
	if (g_context == NULL) active_menu->flags  |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }

    if (g_context == NULL)
      {        win_printf_OK("please init before use");    }

    if (tl_apt_mot_get_move_abspos(g_context, &absolute_pos) != 0)
        warning_message("unable to get\n");

    ret = win_scanf("absolute_pos: %d", &absolute_pos);

    if (ret == WIN_CANCEL)         return D_O_K;


    if (tl_apt_mot_move_absolute_with_pos(g_context, absolute_pos) != 0)
    {
        warning_message("unable to set \n");
    }

    return D_O_K;
}
int do_get_velocity(void)
{
    int min_velocity = 0;
    int max_velocity = 0;
    int acceleration = 0;

    if(updating_menu_state != 0)
      {
	if (g_context == NULL) active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }

    if (g_context == NULL)
    {
        return win_printf_OK("please init before use");
    }

    if (tl_apt_mot_get_velocity(g_context, &min_velocity, &acceleration, &max_velocity) == 0)
    {
        win_printf(" min velocity: %d\n acceleration: %d\n max velocity %d\n", min_velocity, acceleration, max_velocity);
    }
    else
        warning_message("unable to get velocity\n");

    return 0;

}

int do_set_velocity(void)
{
    int min_velocity = 0;
    int max_velocity = 0;
    int acceleration = 0;
    int ret = 0;

    if(updating_menu_state != 0)
      {
	if (g_context == NULL) active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }

    if (g_context == NULL)
    {
        win_printf("please init before use");
        return D_O_K;
    }

    if (tl_apt_mot_get_velocity(g_context, &min_velocity, &acceleration, &max_velocity) != 0)
        warning_message("unable to get state\n");

    ret = win_scanf(" min velocity: %d\n acceleration: %d\n max velocity %d\n", &min_velocity, &acceleration,
                    &max_velocity);

    if (ret == WIN_CANCEL)        return D_O_K;


    if (tl_apt_mot_set_velocity(g_context, min_velocity, acceleration, max_velocity) != 0)
    {
        warning_message("unable to set velocity\n");
    }

    return D_O_K;
}


int do_move_abspos_mm(void)
{
    int absolute_pos = 0;
    int ret = 0;
    float pos = 0;

    if(updating_menu_state != 0)
      {
	if (g_context == NULL) active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }

    if (g_context == NULL)
      {        win_printf_OK("please init before use");    }

    if (tl_apt_mot_get_move_abspos(g_context, &absolute_pos) != 0)
        warning_message("unable to get\n");

    pos = (float)absolute_pos/POS_ENC_CONV_FACTOR;
    ret = win_scanf("absolute_pos: %5f (mm)", &pos);

    if (ret == WIN_CANCEL)         return D_O_K;

    absolute_pos = (int)(0.5 + pos*POS_ENC_CONV_FACTOR);

    if (tl_apt_mot_move_absolute_with_pos(g_context, absolute_pos) != 0)
    {
        warning_message("unable to set \n");
    }
    return D_O_K;
}

int do_set_velocity_mm_s(void)
{
    int min_velocity = 0;
    int max_velocity = 0;
    int acceleration = 0;
    int ret = 0;
    float v_min = 0, v_max = 0, acc = 0;

    if(updating_menu_state != 0)
      {
	if (g_context == NULL) active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }

    if (g_context == NULL)
    {
        win_printf("please init before use");
        return D_O_K;
    }

    if (tl_apt_mot_get_velocity(g_context, &min_velocity, &acceleration, &max_velocity) != 0)
        warning_message("unable to get state\n");

    v_min = (float)min_velocity/SPEED_ENC_CONV_FACTOR;
    v_max = (float)max_velocity/SPEED_ENC_CONV_FACTOR;
    acc = (float)acceleration/ACC_ENS_CONV_FACTOR;
    ret = win_scanf(" min velocity: %5f (mm/s)\n acceleration: %5f\n max velocity %5f (mm/s)\n"
		    , &v_min, &acc,&v_max);

    if (ret == WIN_CANCEL)        return D_O_K;

    min_velocity = (int)(0.5+(v_min*SPEED_ENC_CONV_FACTOR));
    max_velocity = (int)(0.5+(v_max*SPEED_ENC_CONV_FACTOR));
    acceleration = (int)(0.5+(acc*ACC_ENS_CONV_FACTOR));

    if (tl_apt_mot_set_velocity(g_context, min_velocity, acceleration, max_velocity) != 0)
    {
        warning_message("unable to set velocity\n");
    }

    return D_O_K;
}


int do_set_motor_PID(void)
{
    int ret = 0;
    int Proportionnal = 0;
    int Integral = 0;
    int Differential = 0;
    int Integral_Limit = 0;
    int FC;
    unsigned short int FilterControl = 0;


    if(updating_menu_state != 0)
      {
	if (g_context == NULL) active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }

    if (g_context == NULL)
    {
        win_printf("please init before use");
        return D_O_K;
    }

    if (tl_apt_mot_get_PID(g_context, &Proportionnal, &Integral, &Differential,
		       &Integral_Limit, &FilterControl) != 0)
        warning_message("unable to get state\n");
    FC = (int)FilterControl;
    ret = win_scanf("Proportionnal: %5d \nIntegral: %5d\n"
		    "Differential: %5d\nIntegral Limit %5d\n"
		    "FilterControl %5d\n"
		    ,&Proportionnal, &Integral, &Differential,
		       &Integral_Limit, &FC);

    if (ret == WIN_CANCEL)        return D_O_K;

    FilterControl = (unsigned short int)FC;
    if (tl_apt_mot_set_PID(g_context, Proportionnal, Integral, Differential,
		       Integral_Limit, FilterControl) != 0)
        warning_message("unable to get state\n");


    return D_O_K;
}

int do_flow_control_sent_MOT_vol(void)
{
    register int i;
    int acceleration, min_velocity, max_velocity;
    static int microliter = 300, duration = 300;
    float tmp, vel, vol, vol_range;
    int poscounter = 0;


    if(updating_menu_state != 0)
      {
	if (g_context == NULL) active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }

    if (g_context == NULL)
    {
        return win_printf_OK("please init before use");
    }

    if (tl_apt_mot_get_poscounter(g_context, &poscounter) != 0)
        warning_message("unable to get state\n");

    i = win_scanf("You will inject %5d microliters \n in %5d seconds", &microliter, &duration);

    if (i == WIN_CANCEL) return D_O_K;

    if (fabs(microliter) > 1)
    {
        // do speed !
        vol = (float) TRAVEL_SYRINGE_VOLUME;
        vol_range = TRAVEL_SYRINGE_RANGE;
        tmp = microliter * vol_range / vol; //the movement in mm required

        //win_printf("tmp %f",tmp);
        if (tl_apt_mot_get_velocity(g_context, &min_velocity, &acceleration, &max_velocity) != 0)
        {
            warning_message("unable to get state\n");
            return D_O_K;
        }

        duration = fabs(duration);

        if (duration < 1.0) duration  = 1;

        vel = fabs(tmp / duration); //in mm per sec

        if (vel > 2.5) vel = 2.5;

        if (vel < 0.0001) vel = 0.0001;

        max_velocity = (int)(SPEED_ENC_CONV_FACTOR * vel);
        min_velocity = 0;
        //win_printf("Vel %f \n Max vel %d ",vel, max_velocity);

        if (tl_apt_mot_set_velocity(g_context, min_velocity, acceleration, max_velocity) != 0)
        {
            warning_message("unable to set velocity\n");
        }

        tmp = poscounter - (int)(tmp * POS_ENC_CONV_FACTOR); //from mm to encoder position

        if (tl_apt_mot_move_absolute_with_pos(g_context, tmp) != 0)
        {
            warning_message("unable to set \n");
        }
    }

    return 0;

}

int grab_zmag_plot_from_serial_port(void)
{
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    static int wait_ms = 25;
    int  i,  nx = 400;
    static float step = 1, zmag; // , sgn = 1
    unsigned long t0, tt, tn, dt;
    char message[2048] = {0};
    float  tmp = 0;
    int min_velocity = 0;
    int max_velocity = 0;
    int acceleration = 0;
    float v_min = 0, v_max = 0, acc = 0;
    int absolute_pos = 0;
    float pos = 0;



    if(updating_menu_state != 0)
      {
	if (g_context == NULL) active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }

    if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
      return(win_printf("can't find plot"));

    if (g_context == NULL) win_printf("APT not initiated");
    message[0] = 0;



    if (tl_apt_mot_get_velocity(g_context, &min_velocity, &acceleration, &max_velocity) != 0)
        warning_message("unable to get state\n");

    v_min = (float)min_velocity/SPEED_ENC_CONV_FACTOR;
    v_max = (float)max_velocity/SPEED_ENC_CONV_FACTOR;
    acc = (float)acceleration/ACC_ENS_CONV_FACTOR;

    if (tl_apt_mot_get_poscounter(g_context, &absolute_pos) != 0) //tl_apt_mot_get_move_abspos
        warning_message("unable to get\n");

    pos = (float)absolute_pos/POS_ENC_CONV_FACTOR;


    snprintf(message,sizeof(message),"Moving motor and recording it\n"
	     "V_{min} (%g), %%5f \n"
	     "V_{max} (%g), %%5f \n"
	     "Acc (%g), %%5f \n"
	     "Starting position %g\n"
	     "I am going to do a step of %%6f mm\n"
	     "Define the nb of points to record %%8d\n"
	     "Define the waiting time between reads %%8d ms\n"
	     ,v_min, v_max,acc
	     ,pos);


    i = win_scanf(message,&v_min, &v_max, &acc,&step,&nx,&wait_ms);
    if (i == WIN_CANCEL) return D_O_K;

    min_velocity = (int)(0.5+(v_min*SPEED_ENC_CONV_FACTOR));
    max_velocity = (int)(0.5+(v_max*SPEED_ENC_CONV_FACTOR));
    acceleration = (int)(0.5+(acc*ACC_ENS_CONV_FACTOR));

    if (tl_apt_mot_set_velocity(g_context, min_velocity, acceleration, max_velocity) != 0)
    {
        warning_message("unable to set velocity\n");
    }


    pr = create_pltreg_with_op(&op, nx, nx, 0);
    if (op == NULL)    return win_printf("Cannot allocate plot");
    ds = op->dat[0];
    if (ds == NULL)    return win_printf("Cannot allocate plot");

    zmag = pos;
    pos += step;

    absolute_pos = (int)(0.5 + pos*POS_ENC_CONV_FACTOR);
    if (tl_apt_mot_move_absolute_with_pos(g_context, absolute_pos) != 0)
    {
        warning_message("unable to set \n");
    }

    for (i = 0, tt = 0, t0 = my_uclock(); i < nx; i++)
      {
	tn = t0 + i*(wait_ms*get_my_uclocks_per_sec()/1000);
	for (; (tt = my_uclock()) < tn; );
	tt = my_uclock();
	if (tl_apt_mot_get_poscounter(g_context, &absolute_pos) != 0)
	  warning_message("unable to get\n");
	dt = my_uclock() - tt;
	ds->yd[i] = (float)absolute_pos/POS_ENC_CONV_FACTOR;
	ds->xd[i] =  (1000*(tt-t0))/get_my_uclocks_per_sec();
	tmp += (float)(1000*dt)/get_my_uclocks_per_sec();
	my_set_window_title("iter %d %g",i, ds->yd[i]);
      }
    tmp /= nx;
    win_printf("Mean read time %g ms",tmp);

    set_plot_title(op,"\\stack{{Zmag timing}{v_{max} %g v_{min} %g acc %g}}"
		   ,v_min, v_max, acc);
    create_attach_select_y_un_to_op(op, IS_METER, 0 ,(float)1, -3, 0, "mm");
    create_attach_select_x_un_to_op(op, IS_SECOND, 0,(float)1, -3, 0, "ms");
    set_plot_x_title(op, "Time");
    set_plot_y_title(op, "Zmag");

    absolute_pos = (int)(0.5 + zmag*POS_ENC_CONV_FACTOR);
    if (tl_apt_mot_move_absolute_with_pos(g_context, absolute_pos) != 0)
    {
        warning_message("unable to set \n");
    }
    refresh_plot(pr, pr->n_op-1);
    return D_O_K;
}



MENU *thorlabs_apt_api_plot_menu(void)
{
    static MENU mn[32], mne[32] = {0};

    if (mn[0].text != NULL)   return mn;

    add_item_to_menu(mn, "init", do_init, NULL, 0, NULL);
    add_item_to_menu(mn, "close", do_close, NULL, 0, NULL);
    add_item_to_menu(mn, "cleanup", do_cleanup, NULL, 0, NULL);
    add_item_to_menu(mn, "identify", do_identify, NULL, 0, NULL);
    add_item_to_menu(mn, "Home", do_mot_home, NULL, 0, NULL);
    add_item_to_menu(mn, "\0",  NULL,   NULL,   0, NULL);
    add_item_to_menu(mn, "extra fuction", NULL, mne, 0, NULL);
    add_item_to_menu(mne, "stop move", do_stop_move, NULL, 0, NULL);
    add_item_to_menu(mne, "get count per units", do_get_count_per_unit, NULL, 0, NULL);
    add_item_to_menu(mne, "get state", do_get_state, NULL, 0, NULL);
    add_item_to_menu(mne, "set state", do_set_state, NULL, 0, NULL);
    add_item_to_menu(mn, "get poscounter", do_get_poscounter, NULL, 0, NULL);
    add_item_to_menu(mne, "set poscounter", do_set_poscounter, NULL, 0, NULL);
    add_item_to_menu(mne, "get move absolute pos", do_get_move_abspos, NULL, 0, NULL);
    add_item_to_menu(mne, "set move absolute pos", do_set_move_abspos, NULL, 0, NULL);
    add_item_to_menu(mne, "move absolute pos", do_move_abspos, NULL, 0, NULL);
    add_item_to_menu(mn, "move absolute pos (mm)", do_move_abspos_mm, NULL, 0, NULL);

    add_item_to_menu(mne, "get velocity", do_get_velocity, NULL, 0, NULL);
    add_item_to_menu(mne, "set velocity", do_set_velocity, NULL, 0, NULL);
    add_item_to_menu(mn, "set velocity (mm/s)", do_set_velocity_mm_s, NULL, 0, NULL);
    add_item_to_menu(mn, "set motor PID",do_set_motor_PID , NULL, 0, NULL);
    add_item_to_menu(mn, "\0",  NULL,   NULL,   0, NULL);
    add_item_to_menu(mn, "Inject volume", do_flow_control_sent_MOT_vol, NULL, 0, NULL);

    add_item_to_menu(mn, "Record motor move",grab_zmag_plot_from_serial_port , NULL, 0, NULL);



    return mn;
}

int thorlabs_apt_api_main(int argc, char **argv)
{
    /* if (g_context) */
    /*   { */
    /*     win_printf("please close your device before init again"); */

    /*   } */

    /* g_context = tl_apt_init(0x0403, 0xfaf0, NULL, NULL, NULL); */

    /* if (g_context == NULL) */
    /*   { */
    /*     g_context = tl_apt_init(0x1569, 0x2001, NULL, NULL, NULL); */
    /*   } */

    /* usleep(1000); */
    /* if ( tl_apt_mod_identify(g_context) != 0) */
    /*   win_printf("can't load identify"); */
    (void)argc;
    (void)argv;
    add_plot_treat_menu_item("thorlabs_apt_api", NULL, thorlabs_apt_api_plot_menu(), 0, NULL);

    //atexit(do_close);
    //atexit(do_cleanup);
    return D_O_K;
}

int thorlabs_apt_api_unload(int argc, char **argv)
{
    (void)argc;
    (void)argv;
    remove_item_to_menu(plot_treat_menu, "thorlabs_apt_api", NULL, NULL);
    return D_O_K;
}
