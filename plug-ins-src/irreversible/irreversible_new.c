#ifndef _IRREVERSIBLE_NEW_C_
#define _IRREVERSIBLE_NEW_C_

#include "allegro.h"
#include "xvin.h"

/* If you include other plug-ins header do it here*/ 
#include "../inout/inout.h"
/* But not below this define */
#define BUILDING_PLUGINS_DLL

#include "irreversible.h"
#include "irreversible_new.h"
#include "utils.h"


// out is the output pointer, it must be allocated before!!!
int irrev_binarize_data(float *in, lettre *to, int n_pts, int n_bin, float min, float max)
{	register int i;

	for (i=0; i<n_pts; i++)
	{	to[i] = (int)( (in[i] - min)/(max-min)*n_bin );	
    }
		// maybe use floor() or some other way to get the integer part...
		
     return(0);
}


int QuickSort_i_lettre(int *xd, lettre *yd, int l, int r)
{	int register  i, j, x, y;
	lettre	  	  k;

    	i = l;  j = r;
    	x = xd[ (int)((l+r)/2) ];
    	while (i<j)
	{	while (xd[i] < x) { i += 1; }
          	while (x < xd[j]) { j -= 1; }
          	if (i <= j)
          	{  	y = xd[i]; xd[i] = xd[j]; xd[j] = y;
			    k = yd[i]; yd[i] = yd[j]; yd[j] = k;
             		i += 1;
             		j -= 1;
       		}
    	}/*while*/
    	if (l<j) QuickSort_i_lettre(xd, yd, l, j);
    	if (i<r) QuickSort_i_lettre(xd, yd, i, r);
    	return 0;
} /* end of the "QuickSort_i_lettre" function */


// returns 1 if words are identical, or 0 if they are different
// 2006-06-26
int is_identical(lettre *mot_1, lettre *mot_2, unsigned char taille)
{   unsigned char i=0, idem=1;

    while ( (i<taille) && (idem==1) )
    {     if (mot_1[i]!=mot_2[i]) idem=0;
          i++;
    }
    return(idem);
}



// slower version than "find_word_in_ordered_data"
// 2007-06-26
int find_word_in_data(lettre *data, int n_pts, int i_start, lettre *mot, unsigned char taille)
{   register int i;
    int p=0;

    if (i_start>=n_pts-taille) return(0);

    for (i=i_start; i<n_pts-taille; i++)
    {  // if (data[i]==mot[0])
        {   p += is_identical(data+i, mot, taille);
        }
    }
    
    return(p);
}


// data is ordered according to perm
// data_0 is the original data
// 2007-06-26
int find_word_in_ordered_data(int *perm, lettre *data, lettre *data_0, int n_pts, lettre *mot, unsigned char taille)
{   register int i;
    int p=0;

    i=0;
    while ( (data[i]<mot[0]) && (i<n_pts-taille) ) i++;
    
    while ( (data[i]==mot[0]) && (i<n_pts-taille) ) 
    {     p += is_identical(data_0+perm[i], mot, taille);
          i ++;
    }
       
    return(p);
}


/*******************************************************/
/* domain of study is: [i_start; i_end[ (C convention) */
/* but domain for QuickSort is [i_start; i_end]        */
/* d : size of words to study, so d>0                  */
/*******************************************************/
int scan_for_entropy(int *perm, lettre *data, lettre *data_0, int i_start, int i_end, 
                                float *CI, unsigned char d, unsigned char d_max, float normalization)
{       // then we compute S1+ and S1- (words of 1 letter only):
        register int i, k;
        lettre       c;
        unsigned char p;
        int      *perm_tmp;
        
        i=i_start+1; // i is an index that will travel along all the data
        c=data[i_start]; // c is the letter of the predecessor
        p=1; // p will count the occurence of c
        while (i<i_end)
        {   if (data[i]==c) p++;
            else 
            {  // we have isolated p words of 1 letter with the same letter
               // they are such that their index in the permuted data are
               // [ i-p;  i-1]
                   
               // what are the following letters in the original data ?
               // it is data_0[perm[k]+1] for k in [i-1-p; i-1]
                   
               if (p>1) // we continue if there is more than one possible word
               if (d<d_max) // and if we need to continue!
               {  // we backup perm:
                  perm_tmp = (int*)calloc(p, sizeof(int));
                  // for (k=0; k<p; k++) perm_tmp[k] = perm[k+i-p];
                        
                  // we search for the next letters:      
                  for (k=i-p; k<i; k++)
                  {   data[k]           = data_0[perm[k]+d];
                      perm_tmp[k-(i-p)] = perm[k];
                  }
               
                  QuickSort_i_lettre(perm_tmp, data-(i-p), 0, p-1);
                  for (k=i-p; k<i; k++)
                  {   perm[k] = perm[perm_tmp[k-(i-p)]];
                  }
                  
                  scan_for_entropy(perm, data, data_0, i-p, i, CI, d+1, d_max, normalization);
                  
                  free(perm_tmp);
               }
               
               // we increment the entropy:
               CI[d-1] += (p>0) ? (float)p*normalization*log((float)p*normalization) : (float)0.;
               p=1;
            }
            c=data[i];
            i++;
        }
        return(0);
} // end of the function "scan_for_entropy"




/*********************************************************************/
/* to compute the correlations integrals with Eckmann-Ruelle method  */
/* we use the function "scan_for_entropy" above                      */
/*********************************************************************/
int do_irrev_ER_CI(void)
{   register int i, i_F;
    pltreg   *pr = NULL;
	O_p 	  *op1=NULL, *op2=NULL;
	d_s 	  *ds1,      *ds2=NULL;
	int	   n_pts;
	int    index;
	lettre *data, *data_0;
	float  y_min, y_max, epsilon;
	int    N_lettres_alphabet;
	int    *perm=NULL, *perm_0=NULL, *pern=NULL;
    int    F_min, F_max, F_step, epsilon_number, F;
#define d_min  1 
#define d_step 1
    int    d_max, d;
    int    p;
    float  R;
    float  *Cde_plnp, normalization;
static int bool_method=2;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine...");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)		return win_printf_OK("cannot find data");
	n_pts = ds1->ny; // number of points in time series

    F_min=F_min_default; F_max=F_max_default; F_step=F_step_default; epsilon_number=epsilon_number_default;
    d_max=d_max_default;
    R=R_default;
    i=win_scanf("{\\pt14\\color{yellow}Correlation integrals (Eckmann Ruelle, new 2007)}\n\n"
			"Use spheres with radius {\\pt12\\epsilon=R^{-F}}\n\n"
			"with R=%5f and F in the (compact) interval\n"
			"%3d and %3d (with step \\Delta F = %3d).\n\n"
			"Use d (length of word) in the (compact) interval [1 ; %3d]\n\n"
            "%R direct (bourrin), %r ordered (better), %r ",
			&R, &F_min, &F_max, &F_step, &d_max, &bool_method );
	if (i==CANCEL) return D_O_K;
	epsilon_number = (F_max - F_min)/F_step +1;

    // sanity tests : 
    if ( (n_pts<=d_max*2) || (epsilon_number<1) || (d_max<1) ) return(win_printf_OK("error with your values! "));   

	F_min_default=F_min; F_max_default=F_max; F_step_default=F_step; epsilon_number_default=epsilon_number;
    d_max_default=d_max; R_default=R;
   
    // min and max of the data, used to build accessible alphabets:
    y_min = irrev_find_min(ds1->yd, n_pts);
	y_max = irrev_find_max(ds1->yd, n_pts);
    data  =(lettre*)calloc(n_pts, sizeof(lettre));
	data_0=(lettre*)calloc(n_pts, sizeof(lettre));
        
	// we allocate memory for output: one dataset for any word length, epsilon in X
	op2 = create_and_attach_one_plot (pr, epsilon_number, epsilon_number, 0);
	ds2 = op2->dat[0];
    inherit_from_ds_to_ds(ds2, ds1);
    ds2->treatement=my_sprintf(ds2->treatement, "Correlation integrals, Eckmann Ruelle, words of %d letters", 1);
    for (d=1; d<d_max; d++)
    {	ds2 = create_and_attach_one_ds(op2, epsilon_number, epsilon_number, 0);
        if (ds2==NULL)    return(win_printf_OK("can't create data set"));
       
        inherit_from_ds_to_ds(ds2, ds1);
        ds2->treatement=my_sprintf(ds2->treatement, "Correlation integrals, Eckmann Ruelle, words of %d letters", d+1);
    }
    
    Cde_plnp = (float*)calloc(d_max, sizeof(float));
	   	    
	for (F=F_min, i_F=0; F<=F_max; F+=F_step, i_F++)
	{	epsilon=(float)exp(log(R)*(-(float)F));	// this will then define an alphabet
	    N_lettres_alphabet = (lettre)((y_max-y_min)/epsilon);
	    
	    for (d=1; d<=d_max; d++)
        {	op2->dat[d-1]->xd[i_F] = (float)log((double)epsilon);
            op2->dat[d-1]->yd[i_F] = 0.;
        }
	    
		// first, we project the data onto letters (small integers):
	    irrev_binarize_data(ds1->yd, data, n_pts, N_lettres_alphabet, y_min, y_max);

        // we then save the original data in data_0:
        for (i=0; i<n_pts; i++) data_0[i]=data[i];

	    // then we sort the data and keep the permutation pointer:
	    perm = (int*)calloc(n_pts, sizeof(int));
	    for (i=0; i<n_pts; i++) perm[i]=i;
	    QuickSort_i_lettre(perm, data, 0, n_pts-1-d_max); 
        // note about the range we choose : we discard the last letters, 
        // because they can never constitute a word with enough letters
        
        // we then save the original perm in perm_0:
        perm_0 = (int*)calloc(n_pts, sizeof(int));
	    for (i=0; i<n_pts; i++) perm_0[i]=perm[i];

        // we build the inverse permutation:
        pern = (int*)calloc(n_pts, sizeof(int));
        for (i=0; i<n_pts; i++) pern[perm[i]]=i;
	    
	    normalization=(float)1./(float)(n_pts-d_max);
        
        scan_for_entropy(perm, data, data_0, 0 /*starting index*/, n_pts-d_max /*ending index*/, 
                                Cde_plnp, 1, d_max, normalization);
                                
        if (bool_method==0) // bourrin
        { for (d=1; d<=d_max; d++)
          {	for (i=0; i<n_pts-d_max; i++)
            {   p = find_word_in_data(data_0, n_pts-d_max+d, i, data_0+i, d);
                if (p==0) win_printf("0 mots!");
                Cde_plnp[d-1] += (float)( ((double)p*normalization)*log((double)p*normalization) ); 
            }
          }  
        }
        else if (bool_method==1) // un peu mieux
        {
        }
        else // the real stuff  
        { for (d=1; d<=d_max; d++)
          {	op2->dat[d-1]->yd[i_F] = Cde_plnp[d-1];
          }  
        }
          
        draw_bubble(screen, B_RIGHT, SCREEN_W-64, 50, "F = -ln(epsilon)/ln(2) = %d (%d %% done)\n"
                            "%d letters in alphabet", 
						F, (int)(100*(F-F_min)/epsilon_number), (int)N_lettres_alphabet);

        
    }
        
	free(data);
	free(perm);
	
	refresh_plot(pr, UNCHANGED);
	return(D_O_K);
}






/*********************************************************************/
/* to compute the correlations integrals with Eckmann-Ruelle method  */
/* we use the function "scan_for_entropy" above                      */
/*********************************************************************/
int do_irrev_ER_CI_bourrin(void)
{   register int i, i_F;
    pltreg   *pr = NULL;
	O_p 	  *op1=NULL, *op2=NULL;
	d_s 	  *ds1,      *ds2=NULL;
	int	   n_pts;
	int    index;
	lettre *data, *data_0;
	float  y_min, y_max, epsilon;
	int    N_lettres_alphabet;
	int    *perm=NULL, *perm_0=NULL, *pern=NULL;
    int    F_min, F_max, F_step, epsilon_number, F;
#define d_min  1 
#define d_step 1
    int    d_max, d;
    int    p;
    float  R;
    float  *Cde_plnp, normalization;
    

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine...");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)		return win_printf_OK("cannot find data");
	n_pts = ds1->ny; // number of points in time series

    F_min=F_min_default; F_max=F_max_default; F_step=F_step_default; epsilon_number=epsilon_number_default;
    d_max=d_max_default;
    R=R_default;
    i=win_scanf("{\\pt14\\color{yellow}Correlation integrals (Eckmann Ruelle, new 2007)}\n\n"
			"Use spheres with radius {\\pt12\\epsilon=R^{-F}}\n\n"
			"with R=%5f and F in the (compact) interval\n"
			"%3d and %3d (with step \\Delta F = %3d).\n\n"
			"Use d (length of word) in the (compact) interval [1 ; %3d]",
			&R, &F_min, &F_max, &F_step, &d_max );
	if (i==CANCEL) return D_O_K;
	epsilon_number = (F_max - F_min)/F_step +1;

    // sanity tests : 
    if ( (n_pts<=d_max*2) || (epsilon_number<1) || (d_max<1) ) return(win_printf_OK("error with your values! "));   

	F_min_default=F_min; F_max_default=F_max; F_step_default=F_step; epsilon_number_default=epsilon_number;
    d_max_default=d_max; R_default=R;
   
    // min and max of the data, used to build accessible alphabets:
    y_min = irrev_find_min(ds1->yd, n_pts);
	y_max = irrev_find_max(ds1->yd, n_pts);
    data  =(lettre*)calloc(n_pts, sizeof(lettre));
	data_0=(lettre*)calloc(n_pts, sizeof(lettre));
        
	// we allocate memory for output: one dataset for any word length, epsilon in X
	op2 = create_and_attach_one_plot (pr, epsilon_number, epsilon_number, 0);
	ds2 = op2->dat[0];
    inherit_from_ds_to_ds(ds2, ds1);
    ds2->treatement=my_sprintf(ds2->treatement, "Correlation integrals, Eckmann Ruelle, words of %d letters", 1);
    for (d=1; d<d_max; d++)
    {	ds2 = create_and_attach_one_ds(op2, epsilon_number, epsilon_number, 0);
        if (ds2==NULL)    return(win_printf_OK("can't create data set"));
       
        inherit_from_ds_to_ds(ds2, ds1);
        ds2->treatement=my_sprintf(ds2->treatement, "Correlation integrals, Eckmann Ruelle, words of %d letters", d+1);
    }
    
    Cde_plnp = (float*)calloc(d_max, sizeof(float));
	   	    
	for (F=F_min, i_F=0; F<=F_max; F+=F_step, i_F++)
	{	epsilon=(float)exp(log(R)*(-(float)F));	// this will then define an alphabet
	    N_lettres_alphabet = (lettre)((y_max-y_min)/epsilon);
	    
	    for (d=1; d<=d_max; d++)
        {	op2->dat[d-1]->xd[i_F] = (float)log((double)epsilon);
            op2->dat[d-1]->yd[i_F] = 0.;
        }
	    
		// first, we project the data onto letters (small integers):
	    irrev_binarize_data(ds1->yd, data, n_pts, N_lettres_alphabet, y_min, y_max);

        // we then save the original data in data_0:
        for (i=0; i<n_pts; i++) data_0[i]=data[i];

	    // then we sort the data and keep the permutation pointer:
	    perm = (int*)calloc(n_pts, sizeof(int));
	    for (i=0; i<n_pts; i++) perm[i]=i;
	    QuickSort_i_lettre(perm, data, 0, n_pts-1-d_max); 
        // note about the range we choose : we discard the last letters, 
        // because they can never constitute a word with enough letters
        
        // we then save the original perm in perm_0:
        perm_0 = (int*)calloc(n_pts, sizeof(int));
	    for (i=0; i<n_pts; i++) perm_0[i]=perm[i];

        // we build the inverse permutation:
        pern = (int*)calloc(n_pts, sizeof(int));
        for (i=0; i<n_pts; i++) pern[perm[i]]=i;
	    
	    normalization=(float)1./(float)(n_pts-d_max);
        
        for (d=1; d<=d_max; d++)
        {	for (i=0; i<n_pts-d_max; i++)
            {   p = find_word_in_data(data_0, n_pts-d_max+d, i, data_0+i, d);
                if (p==0) win_printf("0 mots!");
                Cde_plnp[d-1] += (float)( ((double)p*normalization)*log((double)p*normalization) ); 
            }
        }  
        
        for (d=1; d<=d_max; d++)
        {	op2->dat[d-1]->yd[i_F] = Cde_plnp[d-1];
        }  
        
        draw_bubble(screen, B_RIGHT, SCREEN_W-64, 50, "F = -ln(epsilon)/ln(2) = %d (%d %% done)\n"
                            "%d letters in alphabet", 
						F, (int)(100*(F-F_min)/epsilon_number), (int)N_lettres_alphabet);

    }
        
	free(data);
	free(perm);
	
	refresh_plot(pr, UNCHANGED);
	return(D_O_K);
}



#endif

