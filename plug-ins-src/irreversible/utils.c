/*
 *  utils.c
 *  entropy
 *
 *  Created by Emmanuel Leveque on 23/12/07.
 *  Copyright 2007 CNRS. All rights reserved.
 *
 */
#include <stdio.h>
#include <math.h>

#include "utils.h"


float irrev_find_min(float *in, int n_pts)
{	register int i;
	float	   min = in[0];
	int		   a   = 0;
	
	for (i=1; i<n_pts; i++)
	{	if (in[i]<=min)
		{	min = in[i];
			a   = i;
		}
	} 
	
	return(min);
}


float irrev_find_max(float *in, int n_pts)
{	register int i;
	float	   max = in[0];
	int		   a   = 0;
	
	for (i=1; i<n_pts; i++)
	{	if (in[i]>=max)
		{	max = in[i];
			a   = i;
		}
	} 
	
	return(max);
}


void binarize_data(float *input, int *output, int nb_pts, int nb_bins)
{	
	int i;
	float minv, maxv;
	
	// get minimum and maximum values of input
    	minv = irrev_find_min(input, nb_pts);
   	maxv = irrev_find_max(input, nb_pts);
   
	for (i=0; i<nb_pts; i++)
	{	output[i] = floorf( ((input[i] - minv)/(maxv - minv)) * (float) nb_bins);	
		if(output[i]==nb_bins) output[i]--; // maximum value
    	}
		
    	return;
}


void irrev_QuickSort(int * xd, int * yd, int l, int r)
{	
	int i, j, x, y;
	int	k;
	
	i = l;  j = r;
	x = xd[ (int)((l+r)/2) ];
	while (i<j)
	{	while (xd[i] < x) { i ++; }
		while (x < xd[j]) { j --; }
		if (i <= j)
		{  	y = xd[i]; xd[i] = xd[j]; xd[j] = y;
			k = yd[i]; yd[i] = yd[j]; yd[j] = k;
			i ++;
			j --;
		}
	}
	
	if (l<j) irrev_QuickSort(xd, yd, l, j);
	if (i<r) irrev_QuickSort(xd, yd, i, r);
	
	return;
} 


