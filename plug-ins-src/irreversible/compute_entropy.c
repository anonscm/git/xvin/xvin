/*
 *  compute_entropy.c
 *  entropy
 *
 *  Created by Emmanuel Leveque on 26/12/07.
 *  Copyright 2007 CNRS. All rights reserved.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "defs.h"
#include "compute_entropy.h"


// following function computes sum of p.ln(p)
double *compute_PlogP(word_info *catalog_fwd[DEPTH], int *nb_words_fwd)
{
	int i, j;
	double sum, P;
	static double Splus[DEPTH];
	
	for(i=0; i<DEPTH; i++){
		Splus[i] = 0.0;
		
		sum = 0.0;
		for(j=0; j<nb_words_fwd[i]; j++) sum += (double) catalog_fwd[i][j].how_many;
		
		for(j=0; j<nb_words_fwd[i]; j++){
			P = (double) catalog_fwd[i][j].how_many / sum;
			Splus[i] += - P*log(P);  
		}
	}
	return Splus;
}

// following function computes sum of p.ln(p-)
double *compute_PlogPm(word_info *catalog_fwd[DEPTH],   int *nb_words_fwd, 
                       word_info *catalog_bckwd[DEPTH], int *nb_words_bckwd,
			double *Splus)
{
	int i, j, jj;
	int k;
	int u;
static  double Sminus[DEPTH];
	double P, sum[DEPTH];
	double Pm, sum_inv[DEPTH];
	double *tmp_P, *tmp_Pm;

	Sminus[0] = 0.0;
	sum[0] = 0.0;
	for(j=0; j<nb_words_bckwd[0]; j++) sum[0] += (double) catalog_bckwd[0][j].how_many;
	for(j=0; j<nb_words_bckwd[0]; j++){
		P = (double) catalog_bckwd[0][j].how_many / sum[0];
		Sminus[0] += - P*log(P);  
	}
	Splus[0] = Sminus[0]; // for words of 1 letters, P=Pm, so at this level, it is the same 
	
	for(i=1; i<DEPTH; i++){
		Splus[i] = 0.0;
		Sminus[i] = 0.0;	
		sum[i] = 0.0;
		sum_inv[i] = 0.0;

//		fprintf(stdout, "scaning forward and backward catalogs for words of length %d...\n", i+1);
		
		tmp_P = malloc(sizeof(double)*nb_words_fwd[i]);
		tmp_Pm = malloc(sizeof(double)*nb_words_fwd[i]);
		u=0;
		
		for(j=0; j<nb_words_fwd[i]; j++)
			if((jj=search_bckwd(i, j, catalog_fwd, catalog_bckwd, nb_words_bckwd)) >= 0)
            {
				
				sum[i] += (double) catalog_fwd[i][j].how_many;
				sum_inv[i] += (double) catalog_bckwd[i][jj].how_many;
				
				/*				for(k=0; k<i; k++)
				 fprintf(stdout, "%d-", catalog_bckwd[i][jj].value[k]);
				 k=i; fprintf(stdout, "%d", catalog_bckwd[i][jj].value[k]);
				 fprintf(stdout, " is found %d times in fwd and %d times in bckwd\n", 
				 catalog_fwd[i][j].how_many, catalog_bckwd[i][jj].how_many);
				 */			
				
				tmp_P[u] = (double) catalog_fwd[i][j].how_many;
				tmp_Pm[u] = (double) catalog_bckwd[i][jj].how_many;
				u++;
			} 
		
		for(k=0; k<u; k++){
			P = (double) tmp_P[k] / sum[i];
			Pm = (double) tmp_Pm[k] / sum_inv[i];
			Splus[i]  += -P*log(P); 
			Sminus[i] += -P*log(Pm); 
		}
		free(tmp_P); free(tmp_Pm);
	}
		
//	fprintf(stdout, "------------------------------------\n");		

	return Sminus;
}



int search_bckwd(int i, int j, word_info *catalog_fwd[DEPTH],   
                       word_info *catalog_bckwd[DEPTH], int *nb_words_bckwd)
{
	int k;
	static int jj; // start from the last index because words are ingeniously sorted !
	static int ilast=-1;
	int value[i+1];	
	int stop;
		
	int jj0;
	
	// start searching in a new vector
	if(ilast!=i){ 
		ilast=i;
		jj=0;
	}
	
	jj0=jj;
	
	// value to be found
	for(k=0; k<i+1; k++) 
		value[k] = catalog_fwd[i][j].value[k];
	
	if(jj == nb_words_bckwd[i]) return -1; // already at the end of the vector
	
	stop=0; // start searching
	while(!stop){
		
		if(jj == nb_words_bckwd[i]){ // word is not in the catalog_backward
			jj=jj0;
			return -1;
		}
		
		stop = 1;	// unless a word is found	
		
		for(k=0; k<i+1; k++)
			if(value[k]!=catalog_bckwd[i][jj].value[k]) 
			{
			stop=0;
			k=i+1; // escape
			}

		jj++;
	}
	
	return jj-1;
}
