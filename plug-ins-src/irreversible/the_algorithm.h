/*
 *  the_algorithm.h
 *  entropy
 *
 *  Created by Emmanuel Leveque on 26/12/07.
 *  Copyright 2007 CNRS. All rights reserved.
 *
 */

extern int icount[DEPTH]; 



// function declaration : 

void the_algorithm(ItemsPtr X, int again, int direct,
                            int *the_msg, int nb_pts,
                            word_info *catalog_fwd[DEPTH],   int *nb_words_fwd, 
                            word_info *catalog_bckwd[DEPTH], int *nb_words_bckwd,
                            int *icount);

void the_algorithm_2(ItemsPtr X, int again,
                            int *the_msg, int nb_pts, int d_max,
                            word_info *catalog[DEPTH], int *nb_words, int *icount);


void create_catalog(int *the_msg, int nb_pts, int d_max,
                            word_info *catalog[DEPTH], int *nb_words);
