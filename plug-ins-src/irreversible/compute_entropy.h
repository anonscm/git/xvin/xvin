/*
 *  compute_entropy.h
 *  entropy
 *
 *  Created by Emmanuel Leveque on 26/12/07.
 *  modified NG, 2008/01/08
 *  Copyright 2007 CNRS. All rights reserved.
 *
 */

double *compute_PlogP (        word_info *catalog_fwd[DEPTH],   int *nb_words_fwd);
double *compute_PlogPm(        word_info *catalog_fwd[DEPTH],   int *nb_words_fwd, 
                               word_info *catalog_bckwd[DEPTH], int *nb_words_bckwd,
			       double *Splus);
int search_bckwd(int i, int j, word_info *catalog_fwd[DEPTH],   
                               word_info *catalog_bckwd[DEPTH], int *nb_words_bckwd);
