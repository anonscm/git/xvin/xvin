#ifndef _IRREVERSIBLE_H_
#define _IRREVERSIBLE_H_

#define ENTROPY_P2   0x0100	// entropy derived from sum of $p^2$
#define ENTROPY_PLNP 0x0200	// entropy derived from sum of $p.ln(p)$




extern int	F_min_default, F_max_default, F_step_default, epsilon_number_default;
extern int	d_min_default, d_max_default, d_step_default, d_number_default;
extern float R_default;


// #include "../../include/gsl/gsl_histogram.h"

PXV_FUNC(MENU*, irreversible_legacy_plot_menu,	(void));
PXV_FUNC(MENU*, irreversible_new_plot_menu,		(void));
PXV_FUNC(int,   irreversible_main, 			(int argc, char **argv));
PXV_FUNC(int,   irreversible_unload, 			(int argc, char **argv));

#endif

