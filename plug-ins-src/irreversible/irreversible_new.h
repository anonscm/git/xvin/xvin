#ifndef _IRREVERSIBLE_NEW_H_
#define _IRREVERSIBLE_NEW_H_

typedef unsigned char lettre;

PXV_FUNC(int,	irrev_binarize_data,(float *in, lettre *to, int n_pts, int n_bin, float min, float max) );
PXV_FUNC(int,   QuickSort_i_lettre, (int   *xd, lettre *yd, int l, int r) );

// following functions are accessible from menu:
PXV_FUNC(int,	do_irrev_ER_CI,				(void) );
PXV_FUNC(int,	do_irrev_ER_CI_bourrin,		(void) );
#endif

