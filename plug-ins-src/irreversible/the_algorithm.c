/*
 *  the_algorithm.c
 *  entropy
 *
 *  Created by Emmanuel Leveque on 26/12/07.
 *  Copyright 2007 CNRS. All rights reserved.
 *
 */
#include <stdio.h>
#include <stdlib.h>

#include "defs.h"
#include "utils.h"
#include "the_algorithm.h"


void the_algorithm(ItemsPtr X, int again, int direct,
                            int *the_msg, int nb_pts,   
                            word_info *catalog_fwd[DEPTH],   int *nb_words_fwd, 
                            word_info *catalog_bckwd[DEPTH], int *nb_words_bckwd,
                            int *icount)
{
	
	int i, j;
	int u;
	int *tmp_perm;
	
	int nb_items;
	int *value_of_item;
	int *size_of_item;
	Word *words;

	int *tmp_index;
	
	if(X->depth>0){
		if(again==0){
/*			for(i=0; i<X->depth; i++)
				fprintf(stdout, "%d", X->root[i]);
			fprintf(stdout, " is found %d times \n", X->msg_length); 
*/
			// how many words in the catalog?
			if(direct==1)
				nb_words_fwd[X->depth-1] ++; 
			if(direct==-1)
				nb_words_bckwd[X->depth-1] ++;
		}
		else{ // fill the catalog
			if(direct==1){
				catalog_fwd[X->depth-1][icount[X->depth-1]].how_many=X->msg_length;
				catalog_fwd[X->depth-1][icount[X->depth-1]].value = malloc(X->depth*sizeof(int));
				for(i=0; i<X->depth; i++) catalog_fwd[X->depth-1][icount[X->depth-1]].value[i] = X->root[i];
				icount[X->depth-1] ++;
			}	
			if(direct==-1){
				catalog_bckwd[X->depth-1][icount[X->depth-1]].how_many=X->msg_length;
				catalog_bckwd[X->depth-1][icount[X->depth-1]].value = malloc(X->depth*sizeof(int));
				for(i=0; i<X->depth; i++) catalog_bckwd[X->depth-1][icount[X->depth-1]].value[i] = X->root[i];
				icount[X->depth-1] ++;
			}
		}			
	}
	
	if(X->depth==DEPTH) return; // stop the recursion if depth is attained
		
/*	fprintf(stdout, "message to analyse:\t");
	for (i=0; i<X->msg_length; i++)
		fprintf(stdout, "%d\t", X->msg[i]); 
	fprintf(stdout, "\n");
*/

	tmp_perm  = calloc(X->msg_length, sizeof(int)); // temporary permutation vector
	for (i=0; i<X->msg_length; i++) 
		tmp_perm[i]=i;
	irrev_QuickSort(X->msg, tmp_perm, 0, X->msg_length-1); // sort the message

	// how many items in the message?
	u = X->msg[0];
	nb_items=1;
	for(i=1; i<X->msg_length; i++)
	{
		if(X->msg[i] != u){
			u = X->msg[i];
			nb_items++;
		}
	}
	// fprintf(stdout, "number of items in the message : %d\n", nb_items);
	
	words = malloc(nb_items*sizeof(Word));
	
	value_of_item = malloc(nb_items*sizeof(int));
	size_of_item = malloc(nb_items*sizeof(int));

	// compute value and size of items
	value_of_item[0] = X->msg[0];
	size_of_item[0] = 1;
	u=0;
	
	for(i=1; i<X->msg_length; i++)
		if(X->msg[i] != value_of_item[u]){
			// fprintf(stdout, "value = %d, size = %d\n", value_of_item[u], size_of_item[u]); 
			u++;
			value_of_item[u] = X->msg[i];
			size_of_item[u] = 1;
		}
		else
			size_of_item[u]++;
	// fprintf(stdout, "value = %d, size = %d\n", value_of_item[u], size_of_item[u]);

	for(i=0; i<nb_items; i++){
		for(j=0; j<X->depth; j++)
			words[i][j] = X->root[j];
		words[i][X->depth]=value_of_item[i];
	}


	tmp_index = malloc(X->msg_length*sizeof(int)); // index in the original message

	u=0;
	for(i=0; i<nb_items; i++)
		for(j=0; j<size_of_item[i]; j++)
		{
			tmp_index[u] = X->index[tmp_perm[u]];
			u++;
		}
	
	for(u=0; u<X->msg_length; u++)
		X->index[u] = tmp_index[u];
		
	free(tmp_index); // free memory

	u=0;
	for(i=0; i<nb_items; i++){
		irrev_QuickSort(X->index, tmp_perm, u, u+size_of_item[i]-1); 
		u += size_of_item[i];
	}
	free(tmp_perm); // free memry


	// initialize next_items for next recursion
	X->next_items = malloc(nb_items*sizeof(Items));	
	
	u=0;
	for(i=0; i<nb_items; i++){
		X->next_items[i].msg_length = size_of_item[i];
		
		X->next_items[i].msg = malloc(X->next_items[i].msg_length*sizeof(int));
		X->next_items[i].depth = X->depth+1;
	
		X->next_items[i].index = malloc(X->next_items[i].msg_length*sizeof(int));
		
		for(j=0; j<X->next_items[i].msg_length; j++){ 
			X->next_items[i].msg[j] = the_msg[(X->index[u]+X->next_items[i].depth) % nb_pts]; // periodic boundary conditions
			X->next_items[i].index[j] = X->index[u];
			u++;
		}		
		
		for(j=0; j<X->next_items[i].depth; j++)	X->next_items[i].root[j] = words[i][j]; // previous values
		
		the_algorithm(&(X->next_items[i]), again, direct,
		              the_msg, nb_pts,
                      catalog_fwd, nb_words_fwd, catalog_bckwd, nb_words_bckwd, icount); // iterate the algorithm with for each item
	}
	
	return;
}










void the_algorithm_2(ItemsPtr X, int again, int *the_msg, int nb_pts, int d_max,  
                     word_info *catalog[DEPTH],   int *nb_words, int *icount)
{
	int i, j;
	int u;
	int *tmp_perm;
	
	int nb_items;
	int *value_of_item;
	int *size_of_item;
	Word *words;

	int *tmp_index;
	

	if(X->depth>0)
	{	if(again==0)
		{
/*			for(i=0; i<X->depth; i++)
				fprintf(stdout, "%d", X->root[i]);
			fprintf(stdout, " is found %d times \n", X->msg_length); 
*/
			// how many words in the catalog?
			nb_words[X->depth-1] ++;
		}
		else
		{   // fill the catalog
			catalog[X->depth-1][icount[X->depth-1]].how_many=X->msg_length;
			catalog[X->depth-1][icount[X->depth-1]].value = malloc(X->depth*sizeof(int));
			for(i=0; i<X->depth; i++) catalog[X->depth-1][icount[X->depth-1]].value[i] = X->root[i];
			icount[X->depth-1] ++;
		}			
	}
	
	if(X->depth==DEPTH) return; // stop the recursion if depth is attained
		
/*	fprintf(stdout, "message to analyse:\t");
	for (i=0; i<X->msg_length; i++)
		fprintf(stdout, "%d\t", X->msg[i]); 
	fprintf(stdout, "\n");
*/


	tmp_perm  = calloc(X->msg_length, sizeof(int)); // temporary permutation vector
	for (i=0; i<X->msg_length; i++)	tmp_perm[i]=i;
	irrev_QuickSort(X->msg, tmp_perm, 0, X->msg_length-1); // sort the message


	// how many items in the message?
	u = X->msg[0];
	nb_items=1;
	for(i=1; i<X->msg_length; i++)
	{
		if(X->msg[i] != u){
			u = X->msg[i];
			nb_items++;
		}
	}
	// fprintf(stdout, "number of items in the message : %d\n", nb_items);
	
	words = malloc(nb_items*sizeof(Word));
	
	value_of_item = malloc(nb_items*sizeof(int));
	size_of_item = malloc(nb_items*sizeof(int));

	// compute value and size of items
	value_of_item[0] = X->msg[0];
	size_of_item[0] = 1;
	u=0;
	
	for(i=1; i<X->msg_length; i++)
		if(X->msg[i] != value_of_item[u]){
			// fprintf(stdout, "value = %d, size = %d\n", value_of_item[u], size_of_item[u]); 
			u++;
			value_of_item[u] = X->msg[i];
			size_of_item[u] = 1;
		}
		else
			size_of_item[u]++;
	// fprintf(stdout, "value = %d, size = %d\n", value_of_item[u], size_of_item[u]);

	for(i=0; i<nb_items; i++){
		for(j=0; j<X->depth; j++)
			words[i][j] = X->root[j];
		words[i][X->depth]=value_of_item[i];
	}


	tmp_index = malloc(X->msg_length*sizeof(int)); // index in the original message

	u=0;
	for(i=0; i<nb_items; i++)
		for(j=0; j<size_of_item[i]; j++)
		{
			tmp_index[u] = X->index[tmp_perm[u]];
			u++;
		}
	
	for(u=0; u<X->msg_length; u++)
		X->index[u] = tmp_index[u];
		
	free(tmp_index); // free memory

	u=0;
	for(i=0; i<nb_items; i++){
		irrev_QuickSort(X->index, tmp_perm, u, u+size_of_item[i]-1); 
		u += size_of_item[i];
	}
	free(tmp_perm); // free memry


	// initialize next_items for next recursion
	X->next_items = malloc(nb_items*sizeof(Items));	
	
	u=0;
	for(i=0; i<nb_items; i++){
		X->next_items[i].msg_length = size_of_item[i];
		
		X->next_items[i].msg = malloc(X->next_items[i].msg_length*sizeof(int));
		X->next_items[i].depth = X->depth+1;
	
		X->next_items[i].index = malloc(X->next_items[i].msg_length*sizeof(int));
		
		for(j=0; j<X->next_items[i].msg_length; j++){ 
			X->next_items[i].msg[j] = the_msg[(X->index[u]+X->next_items[i].depth) % nb_pts]; // periodic boundary conditions
			X->next_items[i].index[j] = X->index[u];
			u++;
		}		
		
		for(j=0; j<X->next_items[i].depth; j++)	X->next_items[i].root[j] = words[i][j]; // previous values
		
		// then iterate the algorithm for each item :
		the_algorithm_2(&(X->next_items[i]), again, 
		              the_msg, nb_pts, d_max, catalog, nb_words, icount); 
	}
	
	return;
}







void create_catalog(int *the_msg, int nb_pts, int d_max,
                            word_info *catalog[DEPTH],   int *nb_words) 
{	int i, u, again;
	int icount[DEPTH];
	Items *X;

    // pre-initialisation:
	for(i=0; i<d_max; i++) nb_words[i] = 0; // on 2008-01-11, replaced DEPTH by d_max
	for(i=0; i<d_max; i++) icount[i] = 0;   // on 2008-01-11, replaced DEPTH by d_max

        for(again=0; again<2; again++)
	{
	    // initialize algorithm
		X = malloc(sizeof(Items));

		X->msg_length = nb_pts;
		X->msg = malloc(X->msg_length*sizeof(int));	
		for(i=0; i<X->msg_length; i++) X->msg[i]=the_msg[i];
		
		X->index = malloc(X->msg_length*sizeof(int));
		for(u=0; u<X->msg_length; u++) X->index[u] = u;
		
		X->depth = 0; 
		
		the_algorithm_2(X, again, the_msg, nb_pts, d_max,                    
                		catalog, nb_words, icount); 
	
		if(again==0)
            	{	// for(i=0; i<DEPTH; i++) fprintf(stdout, "%d words of %d characters\n",  nb_words[i], i+1);
			for(i=0; i<d_max; i++) catalog[i] = malloc(nb_words[i]*sizeof(word_info));
			         // on 2008-01-11, replaced DEPTH by d_max
		}

		free(X);
	}
	
        return;
} 
 
 

