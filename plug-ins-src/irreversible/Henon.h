#ifndef _HENON_H_
#define _HENON_H_

// following functions are accessible from menu:
PXV_FUNC(int,	my_integrate_Henon,		(double *x, double a, double b) );
PXV_FUNC(int,	do_integrate_Henon, 		(void) );

#endif

