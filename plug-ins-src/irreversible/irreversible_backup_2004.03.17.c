#ifndef _IRREVERSIBLE_C_
#define _IRREVERSIBLE_C_

#include "allegro.h"
#include "xvin.h"

/* If you include other plug-ins header do it here*/ 
#include "../inout/inout.h"
/* But not below this define */
#define BUILDING_PLUGINS_DLL

#include "irreversible.h"


/****************************************************************/
/* D2 distance between two vectors in a space of dimension N	*/
/****************************************************************/
double D2_N(int N, float *x1, float *x2)
{	register int i;
	double d=0;

	for (i=0; i<N; i++)
	{	d += (x1[i]-x2[i])*(x1[i]-x2[i]);
	}

	return sqrt(d);
}




/****************************************************************/
/* D2 distance between two vectors in a space of dimension N	*/
/****************************************************************/
double D2_N_inverted(int N, float *x1, float *x2)
{	register int i;
	double d=0;

	for (i=0; i<N; i++)
	{	d += (x1[i]-x2[N-1-i])*(x1[i]-x2[N-1-i]);
	}

	return sqrt(d);
}




int do_correlation_integrals_Grassberger_Procaccia(void)
{
	register int d, F, i, j;
	O_p 	*op2, *op1 = NULL;
	int	ny;
	int	F_min=1, F_max=10,	epsilon_number;
	int	d_min=1, d_max=1, 	d_number;
	d_s 	*ds2, *ds1;
	pltreg *pr = NULL;
	static int shift=1;
	int	index;
	double epsilon; 
	float	*x1, *x2;
	int	Cde;
	float	R=2;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine computes the correlation integral C_d(\\epsilon)\n"
					"using the algorithm from Grassberger and Procaccia\n"
					"for a given range of d and \\epsilon.\n\n"
					"This is the first step in estimating the Kolmogorov-Sinai entropy,");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)		return win_printf_OK("cannot plot region");
	ny=ds1->ny;	// number of points in time series

	i=win_scanf("{\\pt14\\color{yellow}Correlation integral}\n\n"
			"Use spheres with radius {\\pt12\\epsilon=R^{-F}}\n\n"
			"with R=%5f and F in the (compact) interval\n"
			"%3d and %3d (with step \\Delta F = 1).\n\n"
			"Use d (length of word) in the (compact) interval\n"
			"%3d and %3d.",
			&R, &F_min, &F_max, &d_min, &d_max);
	if (i==CANCEL) return D_O_K;
	epsilon_number	= F_max - F_min +1;
	d_number 	= d_max - d_min +1;

	if ((op2 = create_and_attach_one_plot(pr, epsilon_number, epsilon_number, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	ds2 = op2->dat[0];
	if ( (ds2==NULL) || (ny<d_number) || (epsilon_number<1) || (d_number<1) || (ny-d_max-1<1) ) return(win_printf("error ! "));

	for (d=d_min; d<=d_max; d++)
	{	if (d!=d_min) ds2=create_and_attach_one_ds (op2, epsilon_number, epsilon_number, 0);


		for (F=F_min; F<=F_max; F++)
		{	epsilon=exp(log(R)*(-F));	

			Cde=0;
			for (i=0; i<ny-d+1; i+=shift)
        		{	x1 = ds1->yd + i;

				for (j=i+1; j<ny-d+1; j+=shift)
				{	x2 = ds1->yd + j;
					if (D2_N(d, x1, x2)<epsilon) 
					{	Cde++;
					}
				}
			}
			Cde *=2;	// because we forced j>i, only half the couples were measured 
			ds2->xd[F-F_min] = log(epsilon);
			ds2->yd[F-F_min] = (Cde > 0) ? log(Cde) : 0;
			ds2->yd[F-F_min]-= log(((ny-d)*(ny-d-1))) ;	// the total number of couple of words of size d

			draw_bubble(screen, B_RIGHT, SCREEN_W-64, 50, "d = %d  (%d %% done)", 
								d, (int)(100*(d-d_min)/d_number));	
			draw_bubble(screen, B_RIGHT, SCREEN_W-64, 70, "F = -ln(epsilon)/ln(2) = %d (%d %% done)", 
								F, (int)(100*(F-F_min)/epsilon_number));

		//	win_printf("essai : %d -> %f", Cde, (1+sqrt(1+Cde*4))/2 );

		}// end of loop over F and epsilon

		ds2->treatement = my_sprintf(ds2->treatement,"Correlation integral for d=%d",d);

	} // end of loop over d

	ds2=op2->dat[0];
	set_ds_plot_label(ds2, ds2->xd[(int)(epsilon_number/2)], ds2->yd[(int)(epsilon_number/2)], USR_COORD, "d=%d", d_min);
	ds2=op2->dat[op2->n_dat-1];
	set_ds_plot_label(ds2, ds2->xd[(int)(epsilon_number/2)], ds2->yd[(int)(epsilon_number/2)], USR_COORD, "d=%d", d_max);

	set_plot_title(op2,"Correlation integrals");
	set_plot_x_title(op2, "ln \\epsilon");
	set_plot_y_title(op2, "ln C_d (\\epsilon) for d in [%d, %d]", d_min, d_max);
	op2->filename = Transfer_filename(op1->filename);
   	op2->dir = Mystrdup(op1->dir);
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the 'do_correlation_integrals_Grassberger_Procaccia' function





int do_correlation_integrals_direct(void)
{
	register int d, F, i, j;
	O_p 	*op1, *op2, *op3 = NULL;
	int	ny;
	int	F_min=1, F_max=10,	epsilon_number;
	int	d_min=1, d_max=1, 	d_number;
	d_s 	*ds1, *ds2, *ds3;
	pltreg *pr = NULL;
	static int shift=1;
	int	index;
	double epsilon; 
	float	*x1, *x2;
	int	Cde_forward, Cde_backward;
	int	Cde_forward_plnp, Cde_backward_plnp;
	int	Cde_tmp_f, Cde_tmp_b;
	float	R=2;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine computes the correlation integrals C_d(\\epsilon)\n"
					"using the classical definition of the entropy (\\Sigma p.ln(p))"
					"for a given range of d and \\epsilon.\n\n"
					"Forward and Backward integrals are computed, to measure the irreversibility.\n\n"
					"This is the first step in estimating the Kolmogorov-Sinai entropy,");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)		return win_printf_OK("cannot plot region");
	ny=ds1->ny;	// number of points in time series

	i=win_scanf("{\\pt14\\color{yellow}Correlation integral}\n\n"
			"Use spheres with radius \\epsilon=R^{-F}\n"
			"with R=%5f and F in the (compact) interval\n"
			"%3d and %3d (with step \\Delta F = 1).\n\n"
			"Use d (length of word) in the (compact) interval\n"
			"%3d and %3d.",
			&R, &F_min, &F_max, &d_min, &d_max);
	if (i==CANCEL) return D_O_K;
	epsilon_number	= F_max - F_min +1;
	d_number 	= d_max - d_min +1;

	if ((op2 = create_and_attach_one_plot(pr, epsilon_number, epsilon_number, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	if ((op3 = create_and_attach_one_plot(pr, epsilon_number, epsilon_number, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	ds2 = op2->dat[0];
	ds3 = op3->dat[0];
	if ( (ds2==NULL) || (ds3==NULL) || (ny<d_number) || (epsilon_number<1) || (d_number<1) || (ny-d_max-1<1) ) 
		return(win_printf("error ! "));

	for (d=d_min; d<=d_max; d++)
	{	if (d!=d_min) 
		{	ds2=create_and_attach_one_ds (op2, epsilon_number, epsilon_number, 0);
			ds3=create_and_attach_one_ds (op3, epsilon_number, epsilon_number, 0);
		}

		for (F=F_min; F<=F_max; F++)
		{	epsilon=exp(log(R)*(-F));	

			Cde_forward_plnp = Cde_backward_plnp 	= 0;
			Cde_forward	 = Cde_backward	 	= 0;
			for (i=0; i<ny-d+1; i+=shift)
        		{	x1 = ds1->yd + i;
			
				Cde_tmp_f = 0;
				Cde_tmp_b = 0;
				for (j=0; j<ny-d+1; j+=shift)
				{	x2 = ds1->yd + j;
					if (D2_N	 (d, x1, x2)<epsilon) 	Cde_tmp_f++;
					if (D2_N_inverted(d, x1, x2)<epsilon) 	Cde_tmp_b++;
				}
				Cde_forward  += Cde_tmp_f;	// Grassberger-Procaccia estimate
				Cde_backward += Cde_tmp_b;	// Grassberger-Procaccia estimate

				Cde_forward_plnp  += (Cde_tmp_f>0) ? Cde_tmp_f/(ny-d)*log(Cde_tmp_f/(ny-d)) : 0; // usual p.ln(p) estimate
				Cde_backward_plnp += (Cde_tmp_b>0) ? Cde_tmp_f/(ny-d)*log(Cde_tmp_b/(ny-d)) : 0; // usual p.ln(p) estimate
			}

			Cde_forward *=2;	// because we forced j>i, only half the couples were measured 
			Cde_backward *=2;

			ds2->xd[F-F_min] = log(epsilon);
			ds2->yd[F-F_min] = (Cde_forward_plnp > 0) ? log(Cde_forward_plnp) : -2*log(ny*ny);	// in case of a zero probability...

			ds3->xd[F-F_min] = log(epsilon);
			ds3->yd[F-F_min] = (Cde_backward_plnp > 0) ? log(Cde_backward_plnp) : -2*log(ny*ny);	// in case of a zero probability...

			draw_bubble(screen, B_RIGHT, SCREEN_W-64, 50, "d = %d  (%d %% done)", 
								d, (int)(100*(d-d_min)/d_number));	
			draw_bubble(screen, B_RIGHT, SCREEN_W-64, 70, "F = -ln(epsilon)/ln(2) = %d (%d %% done)", 
								F, (int)(100*(F-F_min)/epsilon_number));

			win_printf("essai : C_{d,\\epsilon}^+ = %d -> %f", Cde_forward, (1+sqrt(1+Cde_forward*4))/2 );

		}// end of loop over F and epsilon

		ds2->treatement = my_sprintf(ds2->treatement,"Correlation integral (forward) for d=%d",d);
		ds3->treatement = my_sprintf(ds3->treatement,"Correlation integral (backward) for d=%d",d);

	} // end of loop over d

	ds2=op2->dat[0];
	set_ds_plot_label(ds2, ds2->xd[(int)(epsilon_number/2)], ds2->yd[(int)(epsilon_number/2)], USR_COORD, "d=%d", d_min);
	ds2=op2->dat[op2->n_dat-1];
	set_ds_plot_label(ds2, ds2->xd[(int)(epsilon_number/2)], ds2->yd[(int)(epsilon_number/2)], USR_COORD, "d=%d", d_max);

	set_plot_title(op2,"Correlation integrals (forward)");
	set_plot_x_title(op2, "ln \\epsilon");
	set_plot_y_title(op2, "ln C_d^{+} (\\epsilon) for d in [%d, %d]", d_min, d_max);
	op2->filename = Transfer_filename(op1->filename);
   	op2->dir = Mystrdup(op1->dir);

	set_plot_title(op3,"Correlation integrals (backward)");
	set_plot_x_title(op3, "ln \\epsilon");
	set_plot_y_title(op3, "ln C_d^{-} (\\epsilon) for d in [%d, %d]", d_min, d_max);
	op3->filename = Transfer_filename(op1->filename);
   	op3->dir = Mystrdup(op1->dir);

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the 'do_correlation_integrals_direct' function





int do_entropy_measurement(void)
{
	register int d, F, i;
	O_p 	*op2, *op1 = NULL;
	int	ny;
	int	F_min=1, F_max=10,	epsilon_number;
	int	d_min=1, d_max=1, 	d_number;
	float	lne_min, lne_max;
	d_s 	*ds2, *ds1;
	pltreg *pr = NULL;
	int	index;
	float	tmp;	
	float 	dt=1;	// time flag between two points in the initial signal (1/f_acq).

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;


	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine computes the Kolmogorov-Sinai entropy,\n"
					"using previously computed correlation integrals.");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)		return win_printf_OK("cannot plot region");
	if (op1->title==NULL) return(win_printf_OK("bad plot\n I need correlation integrals"));
	if (strcmp(op1->title,"Correlation integrals")!=0) return(win_printf_OK("bad plot\n I need correlation integrals"));

	ny=ds1->ny;	// number of points in time series
	d_number=op1->n_dat;
	if (d_number<2) return(win_printf_OK("I need more than one correlation integral !"));

/*
	if (op1->dat[0]->n_lab!=0)	
	{	sscanf((op1->dat[0]->lab[0])+2, "%d", &d_min);
	}
win_printf("d=%d",d_min);
*/
	i=win_scanf("{\\pt14\\color{yellow}Kolmogorov-Sinai estimate}\n"
			"{\\pt14\\color{yellow}using Grassberger-Procaccia algorithm}\n\n"
			"Use ln\\epsilon=-F*ln(2) in the (compact) interval\n"
			"%5f and %5f.\n\n"
			"recall me what were d_{min}: %3d and d_{max}: %3d\n"
			"and dt between 2 consecutive points in the initial signal: %5f",
			&lne_min, &lne_max, &d_min, &d_max, &dt);
	if (i==CANCEL) return D_O_K;
	d_number 	= d_max - d_min +1;
	F_min=(int)(-lne_max/log(2));
	F_max=(int)(-lne_min/log(2));
	epsilon_number	= F_max - F_min +1;

	if ((op2 = create_and_attach_one_plot(pr,d_number-1, d_number-1, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	ds2 = op2->dat[0];
	if ( (ds2==NULL) || (epsilon_number<1) || (d_number<1) ) return(win_printf("error ! "));

	for (d=0; d<d_number-1; d++)
	{	tmp=0;

		for (F=F_min; F<=F_max; F++)
		{	
			tmp += op2->dat[d]-op2->dat[d+1];
		}

		tmp /= epsilon_number;

		ds2->xd[d] = d_min+d;
		ds2->yd[d] = tmp/dt;

	} // end of loop over d

	set_plot_title(op2,"\\stack{{Kolmogorov-Sinai entropy estimate}{by Grassberger and Procaccia method}}");
	set_plot_x_title(op2, "dimension d");
	set_plot_y_title(op2, "K_{2,d}");
	op2->filename = Transfer_filename(op1->filename);
   	op2->dir = Mystrdup(op1->dir);
	ds2->treatement = my_sprintf(ds2->treatement,"KS entropy estimate");
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the do_entropy_measurement function





MENU *irreversible_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;

	add_item_to_menu(mn,"Correlation integrals (G-P)", 		do_correlation_integrals_Grassberger_Procaccia, NULL, 0, NULL);
	add_item_to_menu(mn,"Correlation integrals (direct)", 		do_correlation_integrals_direct, 		NULL, 0, NULL);
	add_item_to_menu(mn,"Kolmogorov-Sinai entropy",			do_entropy_measurement,				NULL, 0, NULL);
	
	add_item_to_menu(mn,"\0", 					NULL,						NULL, 0, NULL);
		
	return mn;
}

int	irreversible_main(int argc, char **argv)
{
	add_plot_treat_menu_item ( "irreversibility", NULL, irreversible_plot_menu(), 0, NULL);
	return D_O_K;
}
#endif

