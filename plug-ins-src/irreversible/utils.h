/*
 *  irrev_utils.h
 *  entropy
 *
 *  Copyright 2007 CNRS. All rights reserved.
 *
 */


float irrev_find_min  (float *in, int n_pts);
float irrev_find_max  (float *in, int n_pts);
void  binarize_data   (float *input, int *output, int nb_pts, int nb_bins);
void  irrev_QuickSort (int   * xd,   int * yd, int l, int r);


