#ifndef _IRREVERSIBLE_LEGACY_H_
#define _IRREVERSIBLE_LEGACY_H_

PXV_FUNC(int,	 copy_float_N,		(int N, float *from, float *to) );
PXV_FUNC(int,	 QuickSort_f_i,	(float *xd, int *yd, int l, int r) );
PXV_FUNC(int, 	 QuickSort_i_f,	(int *xd, float *yd, int l, int r) );
PXV_FUNC(double, D2_N,			(int N, float *x1, float *x2) );
PXV_FUNC(double, D2_N_inverted, 	(int N, float *x1, float *x2) );

PXV_FUNC(int, do_correlation_integrals_Grassberger_Procaccia,	(void));
PXV_FUNC(int, do_correlation_integrals_direct, 			(void));
PXV_FUNC(int, do_entropy_measurement, 				(void));
PXV_FUNC(int, do_measure_entropy_production, 			(void));
PXV_FUNC(int, do_normalize_by_embedding_dimension,		(void));
PXV_FUNC(int, do_fit_slopes,					(void));
PXV_FUNC(int, do_Eckmann_Ruelle, 				(void));

#endif

