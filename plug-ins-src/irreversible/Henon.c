#ifndef _HENON_C_
#define _HENON_C_

#include "allegro.h"
#include "xvin.h"

/* If you include other plug-ins header do it here*/ 

/* But not below this define */
#define BUILDING_PLUGINS_DLL

#include "Henon.h"



int my_integrate_Henon(double *x, double a, double b)
{
  double tempx, tempy;

  tempx = x[0];
  tempy = x[1];

  x[0] = tempy + (double)1.0 - a*tempx*tempx;
  x[1] = b*tempx;
      
  return(0);
}


int do_integrate_Henon(void)
{
	pltreg *pr = NULL;
	O_p 	*op;
	d_s	*ds_x=NULL, *ds_y=NULL;
static	double a=1.4, b=0.3; // values from Grassberger & Procaccia, PRA 28, 4, p2591 (1983)
static  double x[2]={0.0, 0.2};
	float a_f, b_f, x_f, y_f;
	register int i;
static	int bool_plot_x=1, bool_plot_y=1, n_pts=16384;

	if(updating_menu_state != 0)	return D_O_K;	
	
	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine integrates Henon map.");
	}
	
	if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)		return win_printf_OK("cannot find plot region");

	a_f = (float)a; b_f=(float)b; x_f=(float)x[0]; y_f=(float)x[1];
	i = win_scanf("{\\pt14\\color{Yellow}Henon map}\n\n"
			"x_{n+1} = y_n + 1 - a x_n^2\n\n"
			"y_{n+1} = b x_n\n\n"
			"a = %6f, b = %6f\n\n"
			"%8d points.\n"
			"initial conditions : (%6f, %6f)\n"
			"%b plot x, %b plot y.", &a_f, &b_f, &n_pts, &x_f, &y_f, &bool_plot_x, &bool_plot_y);
	if (i==CANCEL) return(D_O_K);
	a=(double)a_f; b=(double)b_f;
	x[0]=x_f;      x[1]=y_f; 
	if ( (bool_plot_x+bool_plot_y) < 1 ) return(win_printf_OK("nothing to plot ? Cool, I abort.")); 

	op = create_and_attach_one_plot (pr, n_pts, n_pts, 0);
	          
	if (bool_plot_x==1) 
	{ ds_x = op->dat[0];
	  ds_x->treatement=my_sprintf(ds_x->treatement, "Henon map, x component, a=%5f, b=%5f", (float)a, (float)b);
	  if (bool_plot_y==1) ds_y = create_and_attach_one_ds(op, n_pts, n_pts, 0);
	}
	else if (bool_plot_y==1) ds_y = op->dat[0];
	if (bool_plot_y==1) ds_y->treatement=my_sprintf(ds_y->treatement, "Henon map, y component, a=%5f, b=%5f", (float)a, (float)b);
  
	for (i=0; i<n_pts; i++)
	{	my_integrate_Henon(x,a,b);

		if (bool_plot_x==1)
		{	ds_x->xd[i] = (float)i;
			ds_x->yd[i] = (float)x[0];
		}
		if (bool_plot_y==1)
		{	ds_y->xd[i] = (float)i;
			ds_y->yd[i] = (float)x[1];
		}
    	}    

	set_plot_title  (op, "Henon map");
	set_plot_x_title(op, "(x,y)");
	set_plot_y_title(op, "time (iteration nb)");
	
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the do_divide_by_d function


#endif

