#ifndef _IRREVERSIBLE_C_
#define _IRREVERSIBLE_C_

#include "allegro.h"
#include "xvin.h"

/* If you include other plug-ins header do it here*/ 
#include "../inout/inout.h"
/* But not below this define */
#define BUILDING_PLUGINS_DLL

#include "irreversible.h"



// #define D1_1((x1),(x2)) abs((x1)-(x2))



int	F_min_default=1, 
	F_max_default=100,	
	epsilon_number_default=1;
int	d_min_default=1, 
	d_max_default=5, 	
	d_step_default=1,
	d_number_default=1;
float	R_default=1.2;


int	copy_float_N(int N, float *from, float *to)
{	register int i;

	for(i=0; i<N; i++)
	{	to[i] = from[i];
	}

	return i;
}



/****************************************************************/
/* this function operates on a double array,			*/
/* sorting with the first one 					*/
/* the second array is for the indices (permutations)		*/
/****************************************************************/
int QuickSort_f_i(float *xd, int *yd, int l, int r)
{	int register  i, j, k;
    	float         x, y;

    	i = l;  j = r;
    	x = xd[ (int)((l+r)/2) ];
    	while (i<j)
	{	while (xd[i] < x) { i += 1; }
          	while (x < xd[j]) { j -= 1; }
          	if (i <= j)
          	{  	y = xd[i]; xd[i] = xd[j]; xd[j] = y;
			k = yd[i]; yd[i] = yd[j]; yd[j] = k;
             		i += 1;
             		j -= 1;
       		}
    	}/*while*/
    	if (l<j) QuickSort_f_i(xd, yd, l, j);
    	if (i<r) QuickSort_f_i(xd, yd, i, r);
    	return 0;
} /* end of the "QuickSort_f_i" function */

int QuickSort_i_f(int *xd, float *yd, int l, int r)
{	int register  i, j, x, y;
	float		k;

    	i = l;  j = r;
    	x = xd[ (int)((l+r)/2) ];
    	while (i<j)
	{	while (xd[i] < x) { i += 1; }
          	while (x < xd[j]) { j -= 1; }
          	if (i <= j)
          	{  	y = xd[i]; xd[i] = xd[j]; xd[j] = y;
			k = yd[i]; yd[i] = yd[j]; yd[j] = k;
             		i += 1;
             		j -= 1;
       		}
    	}/*while*/
    	if (l<j) QuickSort_i_f(xd, yd, l, j);
    	if (i<r) QuickSort_i_f(xd, yd, i, r);
    	return 0;
} /* end of the "QuickSort_i_f" function */




/****************************************************************/
/* D2 distance between two vectors in a space of dimension N	*/
/****************************************************************/
double D2_N(int N, float *x1, float *x2)
{	register int i;
	double d=0;

	for (i=0; i<N; i++)
	{	d += (x1[i]-x2[i])*(x1[i]-x2[i]);
	}

	return sqrt(d);
}




/****************************************************************/
/* D2 distance between two vectors in a space of dimension N	*/
/****************************************************************/
double D2_N_inverted(int N, float *x1, float *x2)
{	register int i;
	double d=0;

	for (i=0; i<N; i++)
	{	d += (x1[i]-x2[N-1-i])*(x1[i]-x2[N-1-i]);
	}

	return sqrt(d);
}




int do_correlation_integrals_Grassberger_Procaccia(void)
{
	register int d, F, i, j;
	O_p 	*op2, *op1 = NULL;
	int	ny;
	int	F_min=F_min_default, F_max=F_max_default, epsilon_number=epsilon_number_default;
	int	d_min=d_min_default, d_max=d_max_default, d_number=d_number_default;
	int	d_step=d_step_default;
	d_s 	*ds2, *ds1;
	pltreg *pr = NULL;
	static int shift=1;
	int	index;
	double epsilon; 
	float	*x1, *x2;
	int	Cde;
	float	R=R_default;
	int	bool_adapt=1;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine computes the correlation integral C_d(\\epsilon)\n"
					"using the algorithm from Grassberger and Procaccia\n"
					"for a given range of d and \\epsilon.\n\n"
					"This is the first step in estimating the Kolmogorov-Sinai entropy,");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)		return win_printf_OK("cannot plot region");
	ny=ds1->ny;	// number of points in time series

	i=win_scanf("{\\pt14\\color{yellow}Correlation integral}\n\n"
			"Use spheres with radius {\\pt12\\epsilon=R^{-F}}\n\n"
			"with R=%5f and F in the (compact) interval\n"
			"%3d and %3d (with step \\Delta F = 1).\n\n"
			"Use d (length of word) in the (compact) interval\n"
			"%3d and %3d, with step %3d.\n\n%b select increment to be current d (faster but low stats.)",
			&R, &F_min, &F_max, &d_min, &d_max, &d_step, &bool_adapt);
	if (i==CANCEL) return D_O_K;
	epsilon_number	= F_max - F_min +1;
	d_number 	= (d_max - d_min)/d_step +1;

	if ((op2 = create_and_attach_one_plot(pr, epsilon_number, epsilon_number, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	ds2 = op2->dat[0];
	if ( (ds2==NULL) || (ny<d_number) || (epsilon_number<1) || (d_number<1) || (ny-d_max-1<1) ) return(win_printf("error ! "));

	for (d=d_min; d<=d_max; d+=d_step)
	{	if (d!=d_min) ds2=create_and_attach_one_ds (op2, epsilon_number, epsilon_number, 0);

		if (bool_adapt==1) shift=d; // added to compare with other (direct) method
		else		   shift=1;

		for (F=F_min; F<=F_max; F++)
		{	epsilon=exp(log(R)*(-F));	

			Cde=0;
			for (i=0; i<ny-d+1; i+=shift)
        		{	x1 = ds1->yd + i;

				for (j=i+1; j<ny-d+1; j+=shift)
				{	x2 = ds1->yd + j;
					if (D2_N(d, x1, x2)<epsilon) 
					{	Cde++;
					}
				}
			}
			Cde *=2;	// because we forced j>i, only half the couples were measured 
			ds2->xd[F-F_min] = log(epsilon);
			ds2->yd[F-F_min] = (Cde > 0) ? log(Cde) : 0;
			ds2->yd[F-F_min]-= log(((ny-d)/shift*(ny-d-1)/shift)) ;	// the total number of couple of words of size d
		
			draw_bubble(screen, B_RIGHT, SCREEN_W-64, 50, "d = %d  (%d %% done)", 
								d, (int)(100*(d-d_min)/d_number));	
			draw_bubble(screen, B_RIGHT, SCREEN_W-64, 70, "F = -ln(epsilon)/ln(2) = %d (%d %% done)", 
								F, (int)(100*(F-F_min)/epsilon_number));

		//	win_printf("essai : %d -> %f", Cde, (1+sqrt(1+Cde*4))/2 );

		}// end of loop over F and epsilon

		ds2->treatement = my_sprintf(ds2->treatement,"Correlation integral for d=%d",d);

	} // end of loop over d

	ds2=op2->dat[0];
	set_ds_plot_label(ds2, ds2->xd[(int)(epsilon_number/2)], ds2->yd[(int)(epsilon_number/2)], USR_COORD, "d=%d", d_min);
	set_ds_plot_label(ds2, ds2->xd[epsilon_number/2], 0, USR_COORD, "\\stack{{d_{min}=%d d_{max}=%d, d_{step}=%d}"
				"{F_{min}=%d, F_{max}=%d, R=%g}"
				"{%d pts in d, %d pts in F}"
				"{Grassberger - Procaccia method}}", d_min,d_max,d_step,F_min,F_max,R,epsilon_number,d_number);

	ds2=op2->dat[op2->n_dat-1];
	set_ds_plot_label(ds2, ds2->xd[(int)(epsilon_number/2)], ds2->yd[(int)(epsilon_number/2)], USR_COORD, "d=%d", d_max);

	set_plot_title(op2,"Correlation integrals");
	set_plot_x_title(op2, "ln \\epsilon");
	set_plot_y_title(op2, "ln C_d (\\epsilon) for d in [%d, %d]", d_min, d_max);
	op2->filename = Transfer_filename(op1->filename);
   	op2->dir = Mystrdup(op1->dir);

	F_min_default=F_min;
	F_max_default=F_max;
	epsilon_number_default=epsilon_number;
	d_min_default=d_min; 
	d_max_default=d_max;
	d_number_default=d_number;
	d_step_default=d_step;
	R_default=R;

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the 'do_correlation_integrals_Grassberger_Procaccia' function





int do_correlation_integrals_direct(void)
{
	register int d, F, i, j, i2_f, i2_b, k;
	O_p 	*op1, *op2, *op3 = NULL;
	int	ny;
	int	F_min=F_min_default, F_max=F_max_default, epsilon_number=epsilon_number_default;
	int	d_min=d_min_default, d_max=d_max_default, d_number=d_number_default;
	int	d_step=d_step_default;
	d_s 	*ds1, *ds2, *ds3;
	pltreg *pr = NULL;
	static int shift=1;
	int	index;
	double epsilon; 
	float	*x1, *x2;
	double	Cde_forward, Cde_backward;
	float	*tmp_f, *tmp_b;
	int	*p, *q;
	float	pp, qq;
	float	R=R_default;
	int	n_different_bins_f, n_different_bins_b, n_different_bins;
	int	n_f, n_b;
	int	norm, norm_b;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;
	if ( (index!=ENTROPY_P2) && (index!=ENTROPY_PLNP) ) return(win_printf_OK("bad call!"));

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine computes the correlation integrals C_d(\\epsilon)\n"
					"using the classical definition of the entropy (\\Sigma p.ln(p))"
					"for a given range of d and \\epsilon.\n\n"
					"Forward and Backward integrals are computed, to measure the irreversibility.\n\n"
					"This is the first step in estimating the Kolmogorov-Sinai entropy,");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)		return win_printf_OK("cannot plot region");
	ny=ds1->ny;	// number of points in time series

	i=win_scanf("{\\pt14\\color{yellow}Correlation integral}\n\n"
			"Use spheres with radius \\epsilon=R^{-F}\n"
			"with R=%5f and F in the (compact) interval\n"
			"%3d and %3d (with step \\Delta F = 1).\n\n"
			"Use d (length of word) in the (compact) interval\n"
			"%3d and %3d, with step %3d.",
			&R, &F_min, &F_max, &d_min, &d_max, &d_step);
	if (i==CANCEL) return D_O_K;
	epsilon_number	= F_max - F_min +1;
	d_number 	= (d_max - d_min)/d_step +1;

	if ((op2 = create_and_attach_one_plot(pr, epsilon_number, epsilon_number, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	if ((op3 = create_and_attach_one_plot(pr, epsilon_number, epsilon_number, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	ds2 = op2->dat[0];
	ds3 = op3->dat[0];
	if ( (ds2==NULL) || (ds3==NULL) || (ny<d_number) || (epsilon_number<1) || (d_number<1) || (ny-d_max-1<1) ) 
		return(win_printf("error ! "));

	for (d=d_min; d<=d_max; d+=d_step)
	{	if (d!=d_min) 
		{	ds2=create_and_attach_one_ds (op2, epsilon_number, epsilon_number, 0);
			ds3=create_and_attach_one_ds (op3, epsilon_number, epsilon_number, 0);
		}

		x1 = (float*)calloc(d, sizeof(float));

		tmp_f=(float*)calloc((ny),sizeof(float));
		tmp_b=(float*)calloc((ny),sizeof(float));
		p    =(int*)calloc(((ny/d)+1),sizeof(int));
		q    =(int*)calloc(((ny/d)+1),sizeof(int));

		for (F=F_min; F<=F_max; F++)
		{	epsilon=exp(log(R)*(-F));	

			n_different_bins_f = 0;
			n_different_bins_b = 0;
			n_different_bins   = 0;
			n_f  =ny;
			n_b  =ny; // nb of points in the backward looked data series

			for (i=0; i<ny; i++)
			{	tmp_f[i] = ds1->yd[i];
				tmp_b[i] = ds1->yd[i];
			}

			shift=d;
			while ( (n_f>=d) /* || (n_b>=d) */)
        		{	p[n_different_bins_f]=0;
				q[n_different_bins_b]=0;

				copy_float_N(d, tmp_f, x1);

				i2_f = 0;
				if (n_f>=d)
				{	for (j=0; j<n_f-d+1; j+=shift)
					{
						x2 = tmp_f + j;

						if (D2_N	 (d, x1, x2)<epsilon) 	
						{	p[n_different_bins] += 1;		}
						else
						{	for (k=0; k<d; k++) tmp_f[i2_f+k] = x2[k];
							i2_f += d;
						//	n_f -=d;
						}
					}
					n_different_bins_f++;
				}
				n_f=i2_f;	// nb of points in the new forward signal
				
				i2_b = 0;
				if (n_b>=d)	
				{
					 for (j=0; j<n_b-d+1; j+=shift)
					{
						x2 = tmp_b + j;

						if (D2_N_inverted (d, x2, x1)<epsilon) 	
						{	q[n_different_bins] += 1;		}
						else
						{	for (k=0; k<d; k++) tmp_b[i2_b+k] = x2[k];
							i2_b += d;
						//	n_b -=d;
						}
					}
					n_different_bins_b++;
				}
				n_b=i2_b;	// nb of points in the new backward signal
				
				n_different_bins++;

			}

			norm	     = 0;
			norm_b	     = 0;

			for (i=0; i<n_different_bins; i++)
			{	norm	     += p[i];
				norm_b	     += q[i];
			}

			Cde_forward  = 0;
			Cde_backward = 0;

// win_printf("norm forward : %d    norm backward %d   n bins %d", norm, norm_b, ny/d);			

			for (i=0; i<n_different_bins; i++)
			{	pp = (float)((float)p[i])/(ny/d); 	/* /((ny-d+1)/d); */
				qq = (float)((float)q[i])/(ny/d); 	/* ((ny-d+1)/d); */
				if (index==ENTROPY_P2)
				{	Cde_forward  += pp*pp;
			/* if G-P, then it is += pp*pp;
			and then, outside the loop :
				if (Cde_backward>0) Cde_backward = log(Cde_backward);
				else 			Cde_backward = -log(ny);
			*/
					Cde_backward += qq*pp;
				}
				else
				{	Cde_forward  += (pp>0) ? (double)(pp)*(double)log(pp) : 0; // forward entropy
					Cde_backward += (qq>0) ? (double)(pp)*(double)log(qq) : 0;	// Gaspard estimate of reverse entropy
				}
			}

/*
i=win_printf("%d points in initial dataset\n%d points in final forward dataset\n"
	"%d points in final backward dataset\n\n"
	"\\epsilon=%f -> %d different bins forward (and %d backward)\nn_{max} reel : %d    max(+,-) = %d \n\n"
	"sum of positive probabilities %d -> %f\n"
	"sum of negative probabilities %d -> %f\n"
	"C_{d,\\epsilon}^+ = %f\n"
	"C_{d,\\epsilon}^- = %f", 
	ny,n_f,n_b, epsilon, n_different_bins_f, n_different_bins_b, n_different_bins, n_max2,
	norm,(float)(norm*d)/((float)(ny-d+1)), 
	norm_b,(float)(norm_b*d)/((float)(ny-d+1)), Cde_forward, Cde_backward);

if (i==CANCEL) return D_O_K;*/
			ds2->xd[F-F_min] = log(epsilon);
			ds3->xd[F-F_min] = log(epsilon);
			if (index==ENTROPY_PLNP)
			{	ds2->xd[F-F_min] = Cde_forward;
				ds3->xd[F-F_min] = Cde_backward;
			}
			else {	ds2->yd[F-F_min] = (Cde_forward>0) ? log(Cde_forward) : -2*log(ny);	// in case of a zero probability...
				ds3->yd[F-F_min] = (Cde_backward>0) ? log(Cde_backward) : -2*log(ny);	// in case of a zero probability...
			}


			draw_bubble(screen, B_RIGHT, SCREEN_W-64, 50, "d = %d  (%d %% done)", 
								d, (int)(100*(d-d_min)/d_number));	
			draw_bubble(screen, B_RIGHT, SCREEN_W-64, 70, "F = %d (%d %% done)", 
								F, (int)(100*(F-F_min)/epsilon_number));

			if (norm_b==0)	// finished for this value of d ! we end the loop on F (epsilon) by just filling the ds with 0 :
			for (i=F; i<F_max; i++)
			{	F++;
				epsilon=exp(log(R)*(-F));
				ds2->xd[F-F_min] = log(epsilon);
				ds2->yd[F-F_min] = -2*log(ny);	// in case of a zero probability...

				ds3->xd[F-F_min] = log(epsilon);
				ds3->yd[F-F_min] = -2*log(ny);
			}

		}// end of loop over F and epsilon

		free(tmp_f);	free(tmp_b);
		free(p);	free(q);

		ds2->treatement = my_sprintf(ds2->treatement,"Correlation integral (forward) for d=%d",d);
		ds3->treatement = my_sprintf(ds3->treatement,"Correlation integral (backward) for d=%d",d);

		free(x1);

	} // end of loop over d

	ds2=op2->dat[0];
	set_ds_plot_label(ds2, ds2->xd[(int)(epsilon_number/2)], ds2->yd[(int)(epsilon_number/2)], USR_COORD, "d=%d", d_min);
	ds2=op2->dat[op2->n_dat-1];
	set_ds_plot_label(ds2, ds2->xd[(int)(epsilon_number/2)], ds2->yd[(int)(epsilon_number/2)], USR_COORD, "d=%d", d_max);

	set_ds_plot_label(ds3, ds3->xd[epsilon_number/2], 0, USR_COORD, "\\fbox{\\stack{{d_{min}=%d d_{max}=%d, d_{step}=%d}"
				"{F_{min}=%d, F_{max}=%d, R=%g}"
				"{%d points in d, %d pts in F}"
				"{using \\Sigma %s}"
				"{direct method}}",
				d_min,d_max,d_step,F_min,F_max,R,epsilon_number,d_number,
				(index==ENTROPY_P2) ? "p^2" : "p.ln(p)" );

	set_plot_title(op2,"Correlation integrals (forward)");
	set_plot_x_title(op2, "ln \\epsilon");
	set_plot_y_title(op2, "ln C_d^{+} (\\epsilon) for d in [%d, %d]", d_min, d_max);
	op2->filename = Transfer_filename(op1->filename);
   	op2->dir = Mystrdup(op1->dir);

	set_plot_title(op3,"Correlation integrals (backward)");
	set_plot_x_title(op3, "ln \\epsilon");
	set_plot_y_title(op3, "ln C_d^{-} (\\epsilon) for d in [%d, %d]", d_min, d_max);
	op3->filename = Transfer_filename(op1->filename);
   	op3->dir = Mystrdup(op1->dir);

	F_min_default=F_min;
	F_max_default=F_max;
	epsilon_number_default=epsilon_number;
	d_min_default=d_min; 
	d_max_default=d_max;
	d_number_default=d_number;
	d_step_default=d_step;
	R_default=R;

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the 'do_correlation_integrals_direct' function





int do_entropy_measurement(void)
{
	register int d, F, i;
	O_p 	*op2, *op1 = NULL;
	int	ny;
	int	F_min=F_min_default, F_max=F_max_default, epsilon_number=epsilon_number_default;
	int	d_min=d_min_default, d_max=d_max_default, d_number=d_number_default;
	int	d_step=d_step_default;
	float	lne_min=-5, lne_max=-2;
	d_s 	*ds2, *ds1;
	pltreg *pr = NULL;
	int	index;
	float	R=R_default;
	float 	dt=1;	// time flag between two points in the initial signal (1/f_acq).

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;


	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine computes the Kolmogorov-Sinai entropy,\n"
					"using previously computed correlation integrals.");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)		return win_printf_OK("cannot plot region");
	if (op1->title==NULL) return(win_printf_OK("bad plot\n I need correlation integrals"));
	if (strncmp(op1->title,"Correlation integrals",20)!=0) return(win_printf_OK("bad plot\n I need correlation integrals"));

	ny=ds1->ny;	// number of points in time series
	d_number=op1->n_dat;
	if (d_number<2) return(win_printf_OK("I need more than one correlation integral !"));

	lne_min = op1->x_lo;
	lne_max = op1->x_hi;

	i=win_scanf("{\\pt14\\color{yellow}Kolmogorov-Sinai estimate}\n"
			"{\\pt14\\color{yellow}using Grassberger-Procaccia algorithm}\n\n"
			"Use ln\\epsilon=-F*ln(R) in the (compact) interval\n"
			"%5f and %5f.\nwith R=%5f\n\n"
			"recall me what were d_{min}: %3d, d_{max}: %3d, and d step: %3d\n"
			"and dt between 2 consecutive points in the initial signal: %5f",
			&lne_min, &lne_max, &R, &d_min, &d_max, &d_step, &dt);
	if (i==CANCEL) return D_O_K;
	d_number 	= (d_max - d_min)/d_step +1;
	F_min=(int)floor(-lne_max/log(R));
	F_max=(int)floor(-lne_min/log(R));
	epsilon_number	= F_max - F_min +1;

// i=win_printf("F is in [%d,  %d]", F_min, F_max);	if (i==CANCEL) return D_O_K;

	if ((op2 = create_and_attach_one_plot(pr,d_number-1, d_number-1, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	ds2 = op2->dat[0];
	if ( (ds2==NULL) || (epsilon_number<1) || (d_number<1) ) return(win_printf("error ! "));

	for (F=F_min; F<=F_max; F++)
	{	if (F!=F_min) ds2 = create_and_attach_one_ds (op2, d_number-1, d_number-1, 0);

		for (d=0; d<d_number-1; d++)
		{	ds2->xd[d] = d_min+d*d_step;
			ds2->yd[d] = op1->dat[d]->yd[F] - op1->dat[d+1]->yd[F];
			ds2->yd[d] /= dt;
		}

	} // end of loop over F (epsilon)

	set_plot_title(op2,"\\stack{{Kolmogorov-Sinai entropy estimate}{by Grassberger and Procaccia method}}");
	set_plot_x_title(op2, "dimension d");
	set_plot_y_title(op2, "K_{2,d}");
	op2->filename = Transfer_filename(op1->filename);
   	op2->dir = Mystrdup(op1->dir);
	ds2->treatement = my_sprintf(ds2->treatement,"KS entropy estimate");
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the do_entropy_measurement function










int do_measure_entropy_production(void)
{
	register int d, F, i;
	O_p 	*op_f, *op_b, *op_entropy = NULL;
	int	ny;
	int	F_min=F_min_default, F_max=F_max_default, epsilon_number=epsilon_number_default;
	int	d_min=d_min_default, d_max=d_max_default, d_number=d_number_default;
	int	d_step=d_step_default;
	float	lne_min=-5, lne_max=-2;
	d_s 	*ds2;
	pltreg *pr = NULL;
	int	index;
	float	tmp;	
	float	R=R_default;
	float 	dt=1;	// time flag between two points in the initial signal (1/f_acq).

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;


	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine computes the entropy production,\n"
					"as defined by P. Gaspard.\n"
					"using previously computed correlation integrals.\n\n"
					"active plot is to contain backward correlation integrals\n"
					"and previous plot forward correlation integrals.");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op_b) != 2)		return win_printf_OK("cannot plot region");
	if (pr->n_op<2)							return win_printf_OK("not enough plots");
	op_f = pr->o_p[ ((pr->cur_op-1)<0) ? pr->n_op-1 : (pr->cur_op-1) ];
	if ( (op_b->title==NULL) || (op_f->title==NULL) )		return(win_printf_OK("bad plot\n I need correlation integrals"));
	if (strcmp(op_b->title,"Correlation integrals (backward)")!=0) 	return(win_printf_OK("bad plot (current)\n I need backward correlation integrals"));
	if (strcmp(op_f->title,"Correlation integrals (forward)")!=0) 	return(win_printf_OK("bad plot (previous)\n I need forward correlation integrals"));

	ny=op_b->dat[op_b->cur_dat]->ny;	// number of points in time series
	d_number=op_b->n_dat;	if (op_f->n_dat!=d_number)		return(win_printf_OK("not the same nb of points in d !"));
	if (d_number<2) 						return(win_printf_OK("I need more than one correlation integral !"));

	lne_min = op_b->x_lo;
	lne_max = op_b->x_hi;

	i=win_scanf("{\\pt14\\color{yellow}Kolmogorov-Sinai estimate}\n"
			"{\\pt14\\color{yellow}using Grassberger-Procaccia algorithm}\n\n"
			"Use ln\\epsilon=-F*ln(R) in the (compact) interval\n"
			"%8f and %8f.\nwith R=%5f\n\n"
			"recall me what were d_{min}: %3d, d_{max}: %3d, and d step: %3d\n"
			"and dt between 2 consecutive points in the initial signal: %5f",
			&lne_min, &lne_max, &R, &d_min, &d_max, &d_step, &dt);
	if (i==CANCEL) return D_O_K;
	d_number 	= (d_max - d_min)/d_step +1;
	F_min=(int)floor(-lne_max/log(R));
	F_max=(int)floor(-lne_min/log(R));
	epsilon_number	= F_max - F_min +1;

// i=win_printf("F is in [%d,  %d]", F_min, F_max);	if (i==CANCEL) return D_O_K;

	if ((op_entropy = create_and_attach_one_plot(pr,d_number-1, d_number-1, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	ds2 = op_entropy->dat[0];
	if ( (ds2==NULL) || (epsilon_number<1) || (d_number<1) ) return(win_printf("error ! "));

	for (F=F_min; F<=F_max; F++)
	{	if (F!=F_min) 
		{	ds2=create_and_attach_one_ds (op_entropy, d_number-1, d_number-1, 0);
		}
		for (d=0; d<d_number-1; d++)
		{	
			tmp=0;
			tmp += (op_f->dat[d]->yd[F_min]   - op_b->dat[d]->yd[F_min]);
			tmp -= (op_f->dat[d+1]->yd[F_min] - op_b->dat[d+1]->yd[F_min]);
			
			ds2->xd[d] = d_min+d*d_step;
			ds2->yd[d] = tmp/(dt*d_step);

		} // end of loop over d
	} // end of loop over F (epsilon)

	set_plot_title(op_entropy,"\\stack{{entropy production (Gaspard)}{by unit time}}");
	set_plot_x_title(op_entropy, "dimension d");
	set_plot_y_title(op_entropy, "K_{2,d}");
	op_entropy->filename = Transfer_filename(op_b->filename);
   	op_entropy->dir = Mystrdup(op_b->dir);
	ds2->treatement = my_sprintf(ds2->treatement,"KS entropy estimate");
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the do_measure_entropy_production function







int do_normalize_by_embedding_dimension(void)
{
	register int d, i;
	O_p 	*op1, *op2 = NULL;
	int	ny;
	int	d_min=d_min_default, d_max=d_max_default, d_number=d_number_default;
	int	d_step=d_step_default;
	d_s 	*ds2;
	pltreg *pr = NULL;
	int	index;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;


	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine divides all correlation integrals by their\n"
					"corresponding embedding dimension\n\n"
					"a new plot is created.");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op1) != 2)		return win_printf_OK("cannot plot region");
	if (op1->title==NULL)					return(win_printf_OK("bad plot\n I need correlation integrals"));
	if (strncmp(op1->title,"Correlation integrals",20)!=0) 	return(win_printf_OK("bad plot (current)\n I need backward correlation integrals"));

	ny=op1->dat[0]->ny;	// number of points in time series
	
	i=win_scanf("{\\pt14\\color{yellow}Normalization of Correlation Integrals}\n\n"
			"recall me what were d_{min}: %3d, d_{max}: %3d\n and step in d: %3d",
			&d_min, &d_max, &d_step);
	if (i==CANCEL) return D_O_K;
	d_number 	= (d_max - d_min)/d_step +1;
	if (d_number!=op1->n_dat) return(win_printf_OK("bad number of datasets!\n(not consistent with what you indicated)"));

	if ((op2 = create_and_attach_one_plot(pr, ny, ny, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	ds2 = op2->dat[0];
	if ( (ds2==NULL) || (d_min<1) ) return(win_printf("error ! "));

	for (d=0; d<d_number; d++)
	{	if (op1->dat[d]->nx != ny) return(win_printf("error, nb of points differ in inital datasets"));
		if (d!=0)
		{	ds2=create_and_attach_one_ds (op2, ny, ny, 0);
		}
		for (i=0; i<ny; i++)
		{	ds2->xd[i] = op1->dat[d]->xd[i];
			ds2->yd[i] = op1->dat[d]->yd[i] /( (d*d_step)+d_min) ;
		}
		inherit_from_ds_to_ds(ds2, op1->dat[d]);
		ds2->treatement = my_sprintf(ds2->treatement,"divided by d");
	} // end of loop over d

	set_plot_title(op2, op1->title);
	set_plot_x_title(op2, op1->x_title);
	set_plot_y_title(op2, "\\frac{1}{d} %s", op1);
	op2->filename = Transfer_filename(op1->filename);
   	op2->dir = Mystrdup(op1->dir);
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the do_normalize_by_embedding_dimension










int do_fit_slopes(void)
{
	register int d, i;
	O_p 	*op1 = NULL;
	int	ny;
	int	d_min=d_min_default, d_max=d_max_default, d_number=d_number_default;
	int	d_step=d_step_default;
	float	lne_min, lne_max;
	d_s 	*ds1, *ds2;
	pltreg *pr = NULL;
	int	index;
	int	x_min, x_max, N;
	float	sx, sx2, sy, sxy, delta;
	float	a,b;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;


	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine firs all correlation integrals\n"
					"assumlingg a power law");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op1) != 2)		return win_printf_OK("cannot plot region");
	if (op1->title==NULL)					return(win_printf_OK("bad plot\n I need correlation integrals"));
	if (strncmp(op1->title,"Correlation integrals",20)!=0) 	return(win_printf_OK("bad plot (current)\n I need backward correlation integrals"));

	ny=op1->dat[0]->ny;	// number of points in time series
	
	i=win_scanf("{\\pt14\\color{yellow}Normalization of Correlation Integrals}\n\n"
			"recall me what were d_{min}: %3d, d_{max}: %3d\n and step in d: %3d",
			&d_min, &d_max, &d_step);
	if (i==CANCEL) return D_O_K;
	d_number 	= (d_max - d_min)/d_step +1;
	if (d_number!=op1->n_dat) return(win_printf_OK("bad number of datasets!\n(not consistent with what you indicated)"));

	if ( (d_min<1) ) return(win_printf("error ! "));

	lne_min = op1->x_lo;
	lne_max = op1->x_hi;

	ds1=op1->dat[0];	// intervall where to fit ??? !!! dataset is ordered in reverse !!!
	x_min = ny-1; 	while (ds1->xd[x_min]<lne_min) x_min--;
	x_max = 0; 	while (ds1->xd[x_max]>lne_max) x_max++;

	win_printf("I will fit in compact interval [%d, %d]",x_min,x_max);

	for (d=0; d<d_number; d++)
	{	ds1 = op1->dat[d]; 
		if (ds1->nx != ny) return(win_printf("error, nb of points differ in inital datasets"));
		ds2 = create_and_attach_one_ds (op1, ny, ny, 0);
				
		N=(x_min-x_max)+1;
		sx=0; sx2=0; sy=0; sxy=0;
		for (i=x_max; i<=x_min; i++)
		{	sx  += ds1->xd[i];
			sx2 += ds1->xd[i]*ds1->xd[i];
			sy  += ds1->yd[i];
			sxy += ds1->xd[i]*ds1->yd[i];
		}
		delta = sx*sx - N*sx2;	if (delta==0) return(win_printf("slope is infinite !"));
		a = (sy*sx  - N*sxy)/delta;
		b = (sx*sxy - sx2*sy)/delta;

		for (i=0; i<ny; i++)
		{	ds2->xd[i] = ds1->xd[ny-1-i];
			ds2->yd[i] = ds1->xd[ny-1-i]*a + b ;
		}
		
		inherit_from_ds_to_ds(ds2, ds1);
		ds2->treatement = my_sprintf(ds2->treatement,"linear fit in [%d %d]",x_min,x_max);
		ds2->color = ds1->color;
	} // end of loop over d

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the do_fit_slopes





/* npts : number of points in the signal */
/* ref  : index of the point to study the neighbors of which */
/* epsilon : radius of the ball */
int count_neighbors_1d(int npts, float *data, int ref, float epsilon)
{	register int N=0, i;

	i=1;
	while ( (ref+i<npts) && ( abs( data[ref+i]-data[ref] ) < epsilon) )
	{	N++;
		i++;
	}

	i=1;
	while ( (ref-i>0) && ( abs( data[ref-i]-data[ref] ) < epsilon) )
	{	N++;
		i++;
	}
	return(N);
}






int do_Eckmann_Ruelle(void)
{
	register int d, F, i, j;
	O_p 	*op2, *op1 = NULL;
	int	npts;
	int	F_min=F_min_default, F_max=F_max_default, epsilon_number=epsilon_number_default;
	int	d_min=1, 	d_max=d_max_default, d_number=d_number_default;
	int	d_step=1;
	d_s 	*ds2, *ds1;
	pltreg *pr = NULL;
	int	index;
	double epsilon; 
	float	*Cde_plnp, *Cde_p2;
	float	R=R_default;
	int	*perm, *pern, *tmp;
	float	*data;
	float	p;
	register int N, N2, i_max, d_c;
	FILE 	*fic;
	int	i_ds1;
	float	discretization;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;
	if ( (index!=ENTROPY_P2) && (index!=ENTROPY_PLNP) ) return(win_printf_OK("bad call!"));


	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine....,");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)		return win_printf_OK("cannot plot region");
	npts=ds1->ny;	// number of points in time series

	i=win_scanf("{\\pt14\\color{yellow}Correlation integral}\n\n"
			"Use spheres with radius {\\pt12\\epsilon=R^{-F}}\n\n"
			"with R=%5f and F in the (compact) interval\n"
			"%3d and %3d (with step \\Delta F = 1).\n\n"
			"Use d (length of word) in the (compact) interval\n"
			"[ 1 ;  %3d ], with step 1.",
			&R, &F_min, &F_max, &d_max);
	if (i==CANCEL) return D_O_K;
	epsilon_number	= F_max - F_min +1;
	d_number 	= (d_max - d_min)/d_step +1; 	// d_number id d_max indeed !

fic = fopen("log.log", "wt");

	draw_bubble(screen, B_RIGHT, SCREEN_W-64, 50, "Sorting data...");	

	perm     =(int*)  calloc(npts,	  sizeof(int));
	pern     =(int*)  calloc(npts,	  sizeof(int));
	tmp      =(int*)  calloc(npts,	  sizeof(int));
	Cde_plnp =(float*)calloc(d_number,sizeof(int));	
	Cde_p2   =(float*)calloc(d_number,sizeof(int));	

	data = ds1->yd;

	for (i=0; i<npts; i++)
	{	perm[i]=i;
		pern[i]=i;
	}

	QuickSort_f_i(data, perm, 0, npts-1);
	
	for (i=0; i<npts; i++)
	{	pern[perm[i]] = i;
		tmp[i] = perm[i];
	}

	draw_bubble(screen, B_RIGHT, SCREEN_W-64, 50, "Sorting done");	

	if ( (npts<d_number) || (epsilon_number<1) || (d_number<1) || (npts-d_max-1<1) ) return(win_printf("error ! "));

	if ((op2 = create_and_attach_one_plot(pr, epsilon_number, epsilon_number, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	ds2 = op2->dat[0]; ds2->treatement = my_sprintf(ds2->treatement,"Correlation integral for d=%d",d_min);
	for (d=d_min+d_step; d<=d_max; d+=d_step) 
	{	ds2=create_and_attach_one_ds (op2, epsilon_number, epsilon_number, 0);
		ds2->treatement = my_sprintf(ds2->treatement,"Correlation integral for d=%d",d);
	}

	for (F=F_min; F<=F_max; F++)
	{	epsilon=exp(log(R)*(-F));	
		discretization = (data[npts-1]-data[0])/epsilon;

//win_printf("discretization %g for epsilon = %g", discretization, epsilon);

		for (d_c=0; d_c<d_number; d_c++)
		{	Cde_plnp[d_c]=0;
			Cde_p2  [d_c]=0;
		}

		i=0;			// i is the index of a given point in the ordered 'data'
		do
		{	N=0;		// N is the number of points distant from data[i] by less than epsilon
			i_ds1 = perm[i];//  i    is travelling over data
					// i_ds1 is travelling over ds1

			i_max=i;	// i_max is the index of the point the farest from data[i], but still less than epsilon

			N2=0;		// N2 is a tmp variable for N
			do 
			{ if (perm[i_max] < (npts-d_max+1))
				{ tmp[N2] = perm[i_max];	// tmp contains all the indexes of points in sphere of radius epsilon around data[i]
				  N2++;
				}
				  i_max++;
			}
			while ( ((data[i_max]-data[i])<epsilon) && (i_max<npts) );

			N = N2;
			p = ((float)N2 / (float)((npts-d_max+1) ));
			Cde_plnp[0] += (p>0) ? p*log(p) : 0;
			Cde_p2  [0] += N2*N2;



			if (i_ds1<npts-d_max+1)
			for (d_c=1; d_c<d_number; d_c++)
			{
				N2= 0;
				for (j=0; j<N; j++)
				{ 	if ( fabs(data[pern[ tmp[j] + d_c ]] - data[pern[ i_ds1  + d_c ]] ) < epsilon) 
					{	tmp[N2] = tmp[j];
						N2++;
					}
				}

				p = ((float)(N2-1) / ((float)(npts-d_max-1)) );
				Cde_plnp[d_c] += (p>0) ? p*log(p) : 0;
				Cde_p2  [d_c] += (N2-1)*(N2-1);

fprintf(fic, "i=%d   i_ds1=%d d_c=%d N=%d N2=%d\n",i,i_ds1,d_c,N,N2);

				N = N2;

			} // end of loop over d
		
			i = i_max; // we jump to the next intervall (no overlapping)		

		} 
		while (i<npts-d_max+1); // end of loop over i (initial point)

		for (d_c=0; d_c<d_number; d_c++)
		{	op2->dat[d_c]->xd[F-F_min] = log(epsilon);
		
			if (index==ENTROPY_PLNP)
				op2->dat[d_c]->yd[F-F_min] = Cde_plnp[d_c];
			else
			{	op2->dat[d_c]->yd[F-F_min] = (Cde_p2[d_c]>0) ? log(Cde_p2[d_c]) : 0; 
				op2->dat[d_c]->yd[F-F_min]-=2*log(npts-d_c-1);		// the total number of couple of words of size d
			}
		}

		draw_bubble(screen, B_RIGHT, SCREEN_W-64, 50, "F = -ln(epsilon)/ln(2) = %d (%d %% done)", 
						F, (int)(100*(F-F_min)/epsilon_number));

	} // end of loop over F and epsilon

	draw_bubble(screen, B_RIGHT, SCREEN_W-64, 50, "Sorting back data...");	
	QuickSort_i_f(perm, data, 0, npts-1);

	free(perm); free(pern); free(tmp); free(Cde_plnp); free(Cde_p2);
fclose(fic);

	ds2=op2->dat[0];
//	set_ds_plot_label(ds2, ds2->xd[(int)(epsilon_number/2)], ds2->yd[(int)(epsilon_number/2)], USR_COORD, "d=%d", d_min);
	set_ds_plot_label(ds2, ds2->xd[epsilon_number/2], 0, USR_COORD, "\\stack{{d_{min}=%d d_{max}=%d, d_{step}=%d}"
				"{F_{min}=%d, F_{max}=%d, R=%g}"
				"{%d pts in d, %d pts in F}"
				"{using \\Sigma %s}"
				"{Eckmann - Ruelle scheme}}", 
				d_min,d_max,d_step,F_min,F_max,R, d_number, epsilon_number,
				(index==ENTROPY_P2) ? "p^2" : "p.ln(p)");

//	ds2=op2->dat[op2->n_dat-1];
//	set_ds_plot_label(ds2, ds2->xd[(int)(epsilon_number/2)], ds2->yd[(int)(epsilon_number/2)], USR_COORD, "d=%d", d_max);

	set_plot_title(op2,"Correlation integrals");
	set_plot_x_title(op2, "ln \\epsilon");
	set_plot_y_title(op2, "ln C_d (\\epsilon) for d in [%d, %d]", d_min, d_max);
	op2->filename = Transfer_filename(op1->filename);
   	op2->dir = Mystrdup(op1->dir);

	F_min_default=F_min;
	F_max_default=F_max;
	epsilon_number_default=epsilon_number;
	d_min_default=d_min; 
	d_max_default=d_max;
	d_number_default=d_number;
	d_step_default=d_step;
	R_default=R;

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the 'do_Eckmann_Ruelle' function












MENU *irreversible_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;

	add_item_to_menu(mn,"Correlation integrals (G-P)", 		do_correlation_integrals_Grassberger_Procaccia, NULL, 0, NULL);
	add_item_to_menu(mn,"Kolmogorov-Sinai entropy",			do_entropy_measurement,				NULL, 0, NULL);
	add_item_to_menu(mn,"\0", 					NULL,						NULL, 0, NULL);
	add_item_to_menu(mn,"Correlation integrals (p.ln(p))", 		do_correlation_integrals_direct, 		NULL, ENTROPY_PLNP, NULL);
	add_item_to_menu(mn,"Correlation integrals (p^2)", 		do_correlation_integrals_direct, 		NULL, ENTROPY_P2, NULL);
	add_item_to_menu(mn,"Entropy production",			do_measure_entropy_production,			NULL, 0, NULL);
	add_item_to_menu(mn,"\0", 					NULL,						NULL, 0, NULL);
	add_item_to_menu(mn,"Normalise corr. int. by d", 		do_normalize_by_embedding_dimension,		NULL, 0, NULL);	
	add_item_to_menu(mn,"Linear fit in selected region", 		do_fit_slopes,					NULL, 0, NULL);	
	add_item_to_menu(mn,"\0", 					NULL,						NULL, 0, NULL);
	add_item_to_menu(mn,"Eckmann-Ruelle (p.ln(p))",			do_Eckmann_Ruelle,				NULL, ENTROPY_PLNP, NULL);	
	add_item_to_menu(mn,"Eckmann-Ruelle (p^2)",			do_Eckmann_Ruelle,				NULL, ENTROPY_P2, NULL);	

	return mn;
}

int	irreversible_main(int argc, char **argv)
{
	add_plot_treat_menu_item ( "irreversibility", NULL, irreversible_plot_menu(), 0, NULL);
	return D_O_K;
}


int	irreversible_unload(int argc, char **argv)
{ 
	remove_item_to_menu(plot_treat_menu,"irreversibility",	NULL, NULL);
	return D_O_K;
}
#endif

