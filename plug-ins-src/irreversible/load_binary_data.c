#include <stdlib.h>
#include <stdio.h>

/************************************************************************/
int load_data_bin(char *filename, float **x1)
/* to load an array of floats as a variable								*/
/************************************************************************/
{	FILE 	*data_file;
	int	i,N;
	float 	*x;
	
	data_file=fopen(filename, "rb");
	if (data_file==NULL) printf("load_data_bin: error opening file %s",filename);
	
	x =(float*)calloc(1,sizeof(float)); 
	N=-1;
	while (!feof(data_file)) 
	{ 	i=fread(x,sizeof(float),1,data_file); 
		N++; 
	}
	free(x);
	rewind(data_file);
	*x1=(float*)calloc(N,sizeof(float));
	i=fread(*x1,sizeof(float),N,data_file);
	
	fclose(data_file);
	return(i);
}


int main(int argc, char **argv)
{	char filename_angle[64] = "dep"; // le nom de fichier pour les angles
	char filename_M[64] = "M"; // le nom de fichier pour les moments
	char nom_complet[128];
	char extension[12] = ".dat"; // l'extension
	int  i, j, npts_angle, npts_M, n_files=10; 
	float *y1, *y2; // contiendra les donnees d'un fichier
	FILE *ascii_file;
	
	for (i=1; i<n_files; i++)
	{	sprintf(nom_complet, "%s%d%s", filename_angle, i, extension); // nous construisons le nom complet
		npts_angle=load_data_bin(nom_complet, &y1);
		printf("fichier binaire d'angles %d : %s, contient %d valeurs.\n", i, nom_complet, npts_angle);
			
		sprintf(nom_complet, "%s%d%s", filename_M, i, extension); // nous construisons le nom complet
		npts_M=load_data_bin(nom_complet, &y2);
		printf("fichier binaire de moments %d : %s, contient %d valeurs.\n", i, nom_complet, npts_M);
		
		if (npts_M!=npts_angle) return(printf("pas le meme nombre de points!"));
		
		sprintf(nom_complet, "%s%d%s", "data", i, ".txt");
		ascii_file=fopen(nom_complet, "wt"); // texte
		for (j=0; j<npts_M; j++) fprintf(ascii_file, "%g %g\n", y2[j], y1[j]);
		fclose(ascii_file);
		
		printf("moments et angles sauves (ascii) dans %s.\n\n", nom_complet);
		
		free(y1);
		free(y2);
	}		
	
	return(0);
}

