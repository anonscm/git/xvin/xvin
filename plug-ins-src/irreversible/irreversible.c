#ifndef _IRREVERSIBLE_C_
#define _IRREVERSIBLE_C_

#include "allegro.h"
#include "xvin.h"

/* If you include other plug-ins header do it here*/ 
#include "../inout/inout.h"
/* But not below this define */
#define BUILDING_PLUGINS_DLL

#include "irreversible.h"
#include "irreversible_legacy.h"
#include "irreversible_new.h"
#include "irreversible_main.h"
#include "Henon.h"

// #define D1_1((x1),(x2)) abs((x1)-(x2))



int	F_min_default = 1, 
	F_max_default = 40,	
	F_step_default= 1,
	epsilon_number_default=1;
int	d_min_default    = 1, 
	d_max_default    = 5, 	
	d_step_default   = 1,
	d_number_default = 1;
float	R_default    = 1.2;








MENU *irreversible_legacy_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;

	add_item_to_menu(mn,"Correlation integrals (G-P)", 		do_correlation_integrals_Grassberger_Procaccia, NULL, 0, NULL);
	add_item_to_menu(mn,"Kolmogorov-Sinai entropy",			do_entropy_measurement,				NULL, 0, NULL);
	add_item_to_menu(mn,"\0", 					NULL,						NULL, 0, NULL);
	add_item_to_menu(mn,"Correlation integrals (p.ln(p))", 		do_correlation_integrals_direct, 		NULL, ENTROPY_PLNP, NULL);
	add_item_to_menu(mn,"Correlation integrals (p^2)", 		do_correlation_integrals_direct, 		NULL, ENTROPY_P2, NULL);
	add_item_to_menu(mn,"Entropy production",			do_measure_entropy_production,			NULL, 0, NULL);
	add_item_to_menu(mn,"\0", 					NULL,						NULL, 0, NULL);
	add_item_to_menu(mn,"Normalise corr. int. by d", 		do_normalize_by_embedding_dimension,		NULL, 0, NULL);	
	add_item_to_menu(mn,"Linear fit in selected region", 		do_fit_slopes,					NULL, 0, NULL);	
	add_item_to_menu(mn,"\0", 					NULL,						NULL, 0, NULL);
	add_item_to_menu(mn,"Eckmann-Ruelle (p.ln(p))",			do_Eckmann_Ruelle,				NULL, ENTROPY_PLNP, NULL);	
	add_item_to_menu(mn,"Eckmann-Ruelle (p^2)",			do_Eckmann_Ruelle,				NULL, ENTROPY_P2, NULL);	

	return mn;
}


MENU *irreversible_new_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;

	add_item_to_menu(mn,"Correlation integrals (Eckmann-Ruelle)", 		do_irrev_ER_CI, NULL, 0, NULL);
	add_item_to_menu(mn,"idem, bourrin (Eckmann-Ruelle)", 		do_irrev_ER_CI_bourrin, NULL, 0, NULL);

	return mn;
}


MENU *irreversible_EL_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;

	add_item_to_menu(mn,"Correlation integrals (v1)", 	do_measure_correlation_integrals_2008_v1,	NULL, 0, NULL);
	add_item_to_menu(mn,"Correlation integrals (v2)", 	do_measure_correlation_integrals_2008_v2,	NULL, 0, NULL);
	add_item_to_menu(mn,"\0", 				NULL,		NULL, 0, NULL);
	add_item_to_menu(mn,"divide by d", 	    		do_divide_by_d, NULL, 0, NULL);
	add_item_to_menu(mn,"linear fit in visible zone",	do_irrev_linear_fits, NULL, 0, NULL);
	add_item_to_menu(mn,"measure entropy", 	    		do_entropy_measurement_2008, NULL, 0, NULL);
	add_item_to_menu(mn,"\0", 				NULL,		NULL, 0, NULL);
	add_item_to_menu(mn,"Henon map", 	    		do_integrate_Henon, NULL, 0, NULL);
	add_item_to_menu(mn,"binarize signal", 	    		do_irrev_binarize, NULL, 0, NULL);
	
	return mn;
}


int	irreversible_main(int argc, char **argv)
{
	add_plot_treat_menu_item ( "irreversibility (old)", NULL, irreversible_legacy_plot_menu(), 0, NULL);
	add_plot_treat_menu_item ( "irreversibility (new)", NULL, irreversible_new_plot_menu(), 0, NULL);
	add_plot_treat_menu_item ( "irreversibility (EL)",  NULL, irreversible_EL_plot_menu(), 0, NULL);
	return D_O_K;
}


int	irreversible_unload(int argc, char **argv)
{ 
	remove_item_to_menu(plot_treat_menu,"irreversibility (old)",	NULL, NULL);
	remove_item_to_menu(plot_treat_menu,"irreversibility (new)",	NULL, NULL);
    	remove_item_to_menu(plot_treat_menu,"irreversibility (EL)",	NULL, NULL);
	return D_O_K;
}
#endif

