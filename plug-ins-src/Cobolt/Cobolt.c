/*
*    Plug-in program for simple serial comm 
*
*   
*      JF Allemand
*/
#ifndef _COBOLT_SERIAL_PORT_C_
#define _COBOLT_SERIAL_PORT_C_


# include "allegro.h"
# include "winalleg.h"
//# include <windowsx.h>
//# include "ctype.h"
# include "xvin.h"

#define BUILDING_PLUGINS_DLL

# include "Cobolt_serial_port.h"



// request definition


# define SET_ROT_REQUEST             1
# define SET_ZMAG_REQUEST            2
# define READ_ROT_REQUEST            3
# define READ_ZMAG_REQUEST           4
# define SET_ZMAG_REF_REQUEST        5
# define SET_ROT_REF_REQUEST         6
# define READ_TEMPERATURE_REQUEST    7
# define READ_FOCUS_REQUEST          8
# define SET_FOCUS_REQUEST           9


// third group is errors state
# define PICO_COM_ERROR      0x01000000 
# define ZMAG_TOP_LIMIT      0x02000000 
# define ZMAG_BOTTOM_LIMIT   0x02000000 
# define ZMAG_VCAP_LIMIT     0x04000000 
# define PIFOC_OVERFLOW      0x08000000 
# define PIFOC_OUT_OF_RANGE  0x10000000 

#define SBAUD_300       1
#define SBAUD_600       2
#define SBAUD_1200      3
#define SBAUD_2400      4
#define SBAUD_4800      5
#define SBAUD_9600      6
#define SBAUD_19200     7
#define SBAUD_38400     8
#define SBAUD_57600     9
#define SBAUD_115200   10
#define SBAUD_230400   11
#define SBAUD_460800   12
#define SBAUD_921600   13

unsigned long t_rs = 0, dt_rs;
int request_pending = 0;
float request_parameter = 0;
int request_image_n = 0;
char *debug_file = "c:\\serial_debug.txt";
int debug = 0;
FILE *fp_debug = NULL;
unsigned long grab_start, grab_time;
int n_grab = 0;
UINT timeout = 200;  // ms
char last_answer_2[128];

int sent_cmd_and_read_answer(HANDLE hCom, char *Command, char *answer, DWORD n_answer, DWORD *n_written, DWORD *dwErrors, unsigned long *to);


int generate_bauderate_value(int n)
{
  if (n ==  SBAUD_300)        return 300;
  else if (n == SBAUD_600)    return 600;
  else if (n == SBAUD_1200)   return 1200;
  else if (n == SBAUD_2400)   return 2400;
  else if (n == SBAUD_4800)   return 4800;
  else if (n == SBAUD_9600)   return 9600;
  else if (n == SBAUD_19200)  return 19200;
  else if (n == SBAUD_38400)  return 38400;
  else if (n == SBAUD_57600)  return 57600;
  else if (n == SBAUD_115200) return 115200;
  else if (n == SBAUD_230400) return 230400;
  else if (n == SBAUD_460800) return 460800;
  else if (n == SBAUD_921600) return 921600;
  else return 9600;
}

int retrieve_sbaud(int baudrate)
{
  if (baudrate == 300)        return  SBAUD_300;
  else if (baudrate == 600)    return  SBAUD_600;
  else if (baudrate == 1200)   return  SBAUD_1200;
  else if (baudrate == 2400)   return  SBAUD_2400;
  else if (baudrate == 4800)   return  SBAUD_4800;
  else if (baudrate == 9600)   return  SBAUD_9600;
  else if (baudrate == 19200)  return  SBAUD_19200;
  else if (baudrate == 38400)  return  SBAUD_38400;
  else if (baudrate == 57600)  return  SBAUD_57600;
  else if (baudrate == 115200) return  SBAUD_115200;
  else if (baudrate == 230400) return  SBAUD_230400;
  else if (baudrate == 460800) return  SBAUD_460800;
  else if (baudrate == 921600) return  SBAUD_921600;
  else return SBAUD_9600;
}


// http://msdn.microsoft.com/en-us/library/ms810467.aspx

HANDLE init_serial_port(unsigned short port_number, int baudrate, int RTSCTS)
{
    HANDLE hCom_port;
        
    sprintf( str_com, "\\\\.\\COM%d\0", port_number);
    hCom_port = CreateFile(str_com,GENERIC_READ|GENERIC_WRITE,0,NULL,
        OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED,NULL);
    PurgeComm(hCom_port, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);
    if (GetCommState( hCom_port, &lpCC.dcb) == 0) 
    {
        win_printf("GetCommState FAILED");
        return NULL;
    }    

    /* Initialisation des parametres par defaut */
    /*http://msdn.microsoft.com/library/default.asp?url=/library/en-us/devio/base/dcb_str.asp*/
    //win_printf("lpCC.dcb.BaudRate = %d",lpCC.dcb.BaudRate);
    lpCC.dcb.BaudRate = baudrate;//CBR_57600;//CBR_115200;
    //lpCC.dcb.BaudRate = CBR_115200;
    //    win_printf("lpCC.dcb.BaudRate = %d",lpCC.dcb.BaudRate);
    lpCC.dcb.ByteSize = 8;
    lpCC.dcb.StopBits = ONESTOPBIT;
    lpCC.dcb.Parity = NOPARITY;
    lpCC.dcb.fOutX = FALSE;
    lpCC.dcb.fInX = FALSE;


    if (RTSCTS == 0)
      {
	lpCC.dcb.fDtrControl = DTR_CONTROL_DISABLE;//DTR_CONTROL_HANDSHAKE or DTR_CONTROL_DISABLE;
	lpCC.dcb.fRtsControl = RTS_CONTROL_DISABLE;//RTS_CONTROL_HANDSHAKE
      }
    else
      {
	lpCC.dcb.fDtrControl = DTR_CONTROL_HANDSHAKE;// or DTR_CONTROL_DISABLE;
	lpCC.dcb.fRtsControl = RTS_CONTROL_HANDSHAKE;
	lpCC.dcb.fOutxCtsFlow = TRUE;
      }
 
 
    if (SetCommState( hCom_port, &lpCC.dcb )== 0) 
    {
        win_printf("SetCommState FAILED");
        return NULL;
    }    

    //Delay(60);

    if (GetCommTimeouts(hCom_port,&lpTo)== 0) 
    {
        win_printf("GetCommTimeouts FAILED");
        return NULL;
    }
        
    lpTo.ReadIntervalTimeout = 100;   // 100 ms max entre characters
    lpTo.ReadTotalTimeoutMultiplier = 1;
    lpTo.ReadTotalTimeoutConstant = 1;
    lpTo.WriteTotalTimeoutMultiplier = 1;
    lpTo.WriteTotalTimeoutConstant = 1;
    
    if (SetCommTimeouts(hCom_port,&lpTo)== 0) 
    {
        win_printf("SetCommTimeouts FAILED");
        return NULL;
    }    
    
    if (SetupComm(hCom_port,2048,2048) == 0) 
    {
        win_printf("Init Serial port %d FAILED",port_number+1);
        return NULL;
    }
    win_printf("Init Serial port %d OK",port_number);
    t_rs = my_uclock();
    dt_rs = get_my_uclocks_per_sec()/1000;
    return hCom_port;
}


int Write_serial_port(HANDLE handle, char *data, DWORD length)
{
  int success = 0;
  DWORD dwWritten = 0;
  OVERLAPPED o= {0};

  o.hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
  if (!WriteFile(handle, (LPCVOID)data, length, &dwWritten, &o))
    {
      if (GetLastError() == ERROR_IO_PENDING)
	if (WaitForSingleObject(o.hEvent, timeout) == WAIT_OBJECT_0)
	  if (GetOverlappedResult(handle, &o, &dwWritten, FALSE))
	    success = 1;
    }
  else    success = 1;
  if (dwWritten != length)     success = 0;
  CloseHandle(o.hEvent);
  if (debug)
    {
      fp_debug = fopen(debug_file,"a");
      if (fp_debug != NULL)
	{
	  if (dwWritten != length)    
	    fprintf (fp_debug,">pb writting %d char instead of %d\n",(int)dwWritten,(int)length);
	  fprintf (fp_debug,">\t %s (t = %g)\n",data,(double)(my_uclock()-t_rs)/dt_rs);
	  fclose(fp_debug);
	}
    }
  return (success == 0) ? -(dwWritten+1)  : dwWritten;
}



int ReadData(HANDLE handle, BYTE* data, DWORD length, DWORD* dwRead)
{
  int success = 0;
  OVERLAPPED o ={0};

  o.hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
  if (!ReadFile(handle, data, length, dwRead, &o))
    {
      if (GetLastError() == ERROR_IO_PENDING)
	if (WaitForSingleObject(o.hEvent, timeout) == WAIT_OBJECT_0)
	  success = 1;
      GetOverlappedResult(handle, &o, dwRead, FALSE);
    }
  else    success = 1;
  CloseHandle(o.hEvent);
  return success;
}


int check_for_rs232_error(HANDLE hComm)
{
  COMSTAT comStat;
  DWORD   dwErrors;
  int    fOOP, fOVERRUN, fPTO, fRXOVER, fRXPARITY, fTXFULL;
  int    fBREAK, fDNS, fFRAME, fIOE, fMODE;

    // Get and clear current errors on the port.
    if (!ClearCommError(hComm, &dwErrors, &comStat))
        // Report error in ClearCommError.
        return 0;

    // Get error flags.
    fDNS = dwErrors & CE_DNS;
    fIOE = dwErrors & CE_IOE;
    fOOP = dwErrors & CE_OOP;
    fPTO = dwErrors & CE_PTO;
    fMODE = dwErrors & CE_MODE;
    fBREAK = dwErrors & CE_BREAK;
    fFRAME = dwErrors & CE_FRAME;
    fRXOVER = dwErrors & CE_RXOVER;
    fTXFULL = dwErrors & CE_TXFULL;
    fOVERRUN = dwErrors & CE_OVERRUN;
    fRXPARITY = dwErrors & CE_RXPARITY;
    if (dwErrors)   win_printf("Com error %x",dwErrors);

    // COMSTAT structure contains information regarding
    // communications status.
    if (comStat.fCtsHold)
      win_printf(" Tx waiting for CTS signal");

    if (comStat.fDsrHold)
      win_printf(" Tx waiting for DSR signal");

    if (comStat.fRlsdHold)
      win_printf(" Tx waiting for RLSD signal");

    if (comStat.fXoffHold)
      win_printf(" Tx waiting, XOFF char rec'd");

    if (comStat.fXoffSent)
      win_printf(" Tx waiting, XOFF char sent");
    
    if (comStat.fEof)
      win_printf(" EOF character received");
    
    if (comStat.fTxim)
      win_printf(" Character waiting for Tx; char queued with TransmitCommChar");

    if (comStat.cbInQue)
      win_printf(" comStat.cbInQue bytes have been received, but not read");

    if (comStat.cbOutQue)
      win_printf(" comStat.cbOutQue bytes are awaiting transfer");
    return 0;
}

int Read_serial_port(HANDLE handle, char *stuff, DWORD max_size, DWORD *dwRead)
{
  register int i, j;
  unsigned long t0;
  char lch[2];
  int timeout = 0;
  
  if (handle == NULL) return -2;


  t0 = get_my_uclocks_per_sec()/20; // we cannot wait more than 50 ms
  t0 += my_uclock();
  for (i = j = 0; j == 0 && i < max_size-1 && timeout == 0; )
    {
      *dwRead = 0;
      if (ReadData(handle, lch, 1, dwRead) == 0)
	{
	  stuff[i] = 0;
	  *dwRead = i;
	  if (debug)
	    {
	      fp_debug = fopen(debug_file,"a");
	      if (fp_debug != NULL)
		{
		  fprintf (fp_debug,"<-- ReadData error\n");
		  fprintf (fp_debug,"<\t %s (t = %g)\n",stuff,(double)(my_uclock()-t_rs)/dt_rs);
		  fclose(fp_debug);
		}
	    }
	  return -1;
	}

      if (*dwRead == 1) 
	{
	  stuff[i] = lch[0];
	  j = (lch[0] == '\n') ? 1 : 0;
	  i++;
	}
      timeout = (t0 > my_uclock()) ? 0 : 1;
    }
  stuff[i] = 0;
  *dwRead = i;

  if (debug)
    {
      fp_debug = fopen(debug_file,"a");
      if (fp_debug != NULL)
	{
	  if (timeout) fprintf (fp_debug,"<\t timeout %s (t = %g)\n",stuff,(double)(my_uclock()-t_rs)/dt_rs);
	  else fprintf (fp_debug,"<\t %s (t = %g)\n",stuff,(double)(my_uclock()-t_rs)/dt_rs);
	  fclose(fp_debug);
	}
    }
  return (timeout) ? -2 : i;
}


int talk_serial_port(HANDLE hCom_port, char *command, int wait_answer, char *answer, DWORD NumberOfBytesToWrite, DWORD nNumberOfBytesToRead)
{
    int NumberOfBytesWrittenOnPort = -1;
    unsigned long NumberOfBytesWritten = -1;
    

    if (hCom == NULL) return win_printf_OK("No serial port init!");
    if (wait_answer != READ_ONLY)
    {
        NumberOfBytesWrittenOnPort = Write_serial_port(hCom_port, command, NumberOfBytesToWrite);
    }    
		
    if (wait_answer != WRITE_ONLY) 
    {
        Read_serial_port(hCom_port,answer,nNumberOfBytesToRead,&NumberOfBytesWritten);
    
    }	
    return 0;
}




int write_command_on_serial_port(void)
{
    static char Command[128]="test";
    int ret = 0;
    unsigned long t0;
    double dt;
    //char *test="\r";
    
    if(updating_menu_state != 0)	return D_O_K;
    
    win_scanf("Command to send? %s",&Command);
       
    strcat(Command,"\r");  
     //win_printf("deja cela %d\n Command %s",strlen(Command), Command);
    t0 = my_uclock();    
    ret = Write_serial_port(hCom,Command,strlen(Command));
    t0 = my_uclock() - t0;
    dt = 1000*(double)(t0)/get_my_uclocks_per_sec();
    win_printf("Command sent ret %d in %g mS",ret,dt);


    return D_O_K;
}


int read_on_serial_port(void)
{
    char Command[128];
    unsigned long lpNumberOfBytesWritten = 0;
    int ret = 0;
    DWORD nNumberOfBytesToRead = 127;
    unsigned long t0;
    
    if(updating_menu_state != 0)	return D_O_K;

    t0 = my_uclock();        
    //win_scanf("Command to send? %s",Command);
    ret = Read_serial_port(hCom,Command,nNumberOfBytesToRead,&lpNumberOfBytesWritten);
    t0 = my_uclock() - t0;
    Command[lpNumberOfBytesWritten] = 0;

    win_printf("Read = %s \n ret %d lpNumberOfBytesRead = %d\n in %g mS",Command,ret,lpNumberOfBytesWritten,1000*(double)(t0)/get_my_uclocks_per_sec());

    //win_printf("command read = %s \n lpNumberOfBytesWritten = %d",Command,lpNumberOfBytesWritten);
    return D_O_K;
}




int write_read_serial_port(void)
{
    
    if(updating_menu_state != 0)	return D_O_K;
    if (hCom == NULL) return win_printf_OK("No serial port init§");
    
        write_command_on_serial_port();

        read_on_serial_port();


        return D_O_K;
}

int do_check_for_rs232_error(void)
{
    
    if(updating_menu_state != 0)	return D_O_K;
    if (hCom == NULL) return win_printf_OK("No serial port init§");

    check_for_rs232_error(hCom);
    return D_O_K;
}


int sent_cmd_and_read_answer(HANDLE hCom, char *Command, char *answer, DWORD n_answer, DWORD *n_written, DWORD *dwErrors, unsigned long *t0)
{
  int rets, ret, i, j;
  COMSTAT comStat;
  char l_command[128], ch[2], chtmp[128];
  DWORD len = 0;

  if (hCom == NULL || Command == NULL || answer == NULL || strlen(Command) < 1) return 1;

    for (i = 0, j = 1; i < 5 && j != 0; i++)
      {
	j = ClearCommError(hCom, dwErrors, &comStat);
	if (j && comStat.cbInQue)   Read_serial_port(hCom,chtmp,127,&len);
	if (j && comStat.fCtsHold)  my_set_window_title(" Tx waiting for CTS signal");
	if (j && comStat.fDsrHold)  my_set_window_title(" Tx waiting for DSR signal");
	if (j && comStat.fRlsdHold) my_set_window_title(" Tx waiting for RLSD signal");
	if (j && comStat.fXoffHold) my_set_window_title(" Tx waiting, XOFF char rec'd");
	if (j && comStat.fXoffSent) my_set_window_title(" Tx waiting, XOFF char sent");
	if (j && comStat.fEof)      my_set_window_title(" EOF character received");
	if (j && comStat.fTxim)     my_set_window_title(" Character waiting for Tx; char queued with TransmitCommChar");
	//if (j && comStat.cbInQue)   my_set_window_title(" comStat.cbInQue bytes have been received, but not read");
	if (j && comStat.cbOutQue)  my_set_window_title(" comStat.cbOutQue bytes are awaiting transfer");
      }


  for (i = 0; i < 126 && Command[i] != 0 && Command[i] != 13; i++)
    l_command[i] = Command[i]; // we copy until CR
  l_command[i++] = 13;         // we add it
  l_command[i] = 0;            // we end string
  if (t0) *t0 = my_uclock();

  rets = Write_serial_port(hCom,l_command,strlen(l_command));
  if (rets <= 0)
    {
      if (t0) *t0 = my_uclock() - *t0;
      // Get and clear current errors on the port.
      if (ClearCommError(hCom, dwErrors, &comStat)) 	
	{
	  if (comStat.cbInQue)  Read_serial_port(hCom,chtmp,127,&len);
	  if (comStat.fCtsHold)  my_set_window_title(" Tx waiting for CTS signal");
	  if (comStat.fDsrHold)  my_set_window_title(" Tx waiting for DSR signal");
	  if (comStat.fRlsdHold) my_set_window_title(" Tx waiting for RLSD signal");
	  if (comStat.fXoffHold) my_set_window_title(" Tx waiting, XOFF char rec'd");
	  if (comStat.fXoffSent) my_set_window_title(" Tx waiting, XOFF char sent");
	  if (comStat.fEof)      my_set_window_title(" EOF character received");
	  if (comStat.fTxim)     my_set_window_title(" Character waiting for Tx; char queued with TransmitCommChar");
	  //if (comStat.cbInQue)   my_set_window_title(" comStat.cbInQue bytes have been received, but not read");
	  if (comStat.cbOutQue)  my_set_window_title(" comStat.cbOutQue bytes are awaiting transfer");
	  return -2;
	}
      return -1;
    }

  *n_written = 0;
  ret = Read_serial_port(hCom,answer,n_answer,n_written);
  for  (; ret == 0; )
    ret = Read_serial_port(hCom,answer,n_answer,n_written);
  if (t0) *t0 = my_uclock() - *t0;
  answer[*n_written] = 0;
  strncpy(last_answer_2,answer,127);
  if (ret < 0)
    {
      // Get and clear current errors on the port.
      if (ClearCommError(hCom, dwErrors, &comStat)) 	
	{
	  if (comStat.cbInQue)  Read_serial_port(hCom,chtmp,127,&len);
	  if (comStat.fCtsHold)  my_set_window_title(" Tx waiting for CTS signal");
	  if (comStat.fDsrHold)  my_set_window_title(" Tx waiting for DSR signal");
	  if (comStat.fRlsdHold) my_set_window_title(" Tx waiting for RLSD signal");
	  if (comStat.fXoffHold) my_set_window_title(" Tx waiting, XOFF char rec'd");
	  if (comStat.fXoffSent) my_set_window_title(" Tx waiting, XOFF char sent");
	  if (comStat.fEof)      my_set_window_title(" EOF character received");
	  if (comStat.fTxim)     my_set_window_title(" Character waiting for Tx; char queued with TransmitCommChar");
	  //if (comStat.cbInQue)   my_set_window_title(" comStat.cbInQue bytes have been received, but not read");
	  if (comStat.cbOutQue)  my_set_window_title(" comStat.cbOutQue bytes are awaiting transfer");
	  return 2;
	}
      return 1;      
    }
  return 0;
}


int n_write_read_serial_port(void)
{
  pltreg *pr;
  O_p *op = NULL;
  d_s *ds = NULL, *ds2 = NULL;
  static char Command[128]="dac16?";
  static int ntimes = 1, i, j, n_ms = 1, d_plot = 1;
  unsigned long t0 = 0, tn = 0;
  double dt = 0, dtm = 0, dtmax = 0;
  char resu[128], *ch, lch[2], chtmp[128];
  DWORD len = 0;
  int ret = 0, n_er_r = 0, n_er_s = 0, rets = 0;
  unsigned long lpNumberOfBytesWritten = 0, nNumberOfBytesToRead = 128;
  COMSTAT comStat;
  DWORD   dwErrors;


    
  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return(win_printf("can't find plot"));



  if (hCom == NULL) return win_printf_OK("No serial port init§");    
  for (ch = Command; *ch != 0; ch++) 
    if (*ch == '\r') *ch = 0;
  win_scanf("Command to send? %snumber of times %dNb of ms to sleep between exchange %dDraw timing plot %b\n"
	    ,&Command,&ntimes,&n_ms,&d_plot);
       
  strcat(Command,"\r");  
  
  if (d_plot)
    {
      op = create_and_attach_one_plot(pr, ntimes,ntimes, 0);
      if (op == NULL)    return win_printf("Cannot allocate plot");
      ds = op->dat[0];
      if (ds == NULL)    return win_printf("Cannot allocate plot");
      ds2 = create_and_attach_one_ds(op, ntimes, ntimes, 0);
      if (ds2 == NULL)    return win_printf("Cannot allocate plot");

      create_attach_select_y_un_to_op(op, IS_SECOND, 0 ,(float)1, -3, 0, "ms");
      set_plot_x_title(op, "N");
      set_plot_y_title(op, "Time");			

    }

    for(i = 0, dtm = dtmax = 0; i < ntimes; i++)
      {
	t0 = my_uclock();
	tn = t0 + (n_ms*get_my_uclocks_per_sec()/1000);
	for (j = 0, rets = 1; j < 126 && Command[j] != 0; j++)
	  {
	    lch[0] = Command[j];
	    lch[1] = 0;
	    if (ClearCommError(hCom, &dwErrors, &comStat)) // Report error in ClearCommError.
	      {
		if (comStat.cbInQue)  Read_serial_port(hCom,chtmp,127,&len);
		if (comStat.fCtsHold)  my_set_window_title(" Tx waiting for CTS signal");
		if (comStat.fDsrHold)  my_set_window_title(" Tx waiting for DSR signal");
		if (comStat.fRlsdHold) my_set_window_title(" Tx waiting for RLSD signal");
		if (comStat.fXoffHold) my_set_window_title(" Tx waiting, XOFF char rec'd");
		if (comStat.fXoffSent) my_set_window_title(" Tx waiting, XOFF char sent");
		if (comStat.fEof)      my_set_window_title(" EOF character received");
		if (comStat.fTxim)     my_set_window_title(" Character waiting for Tx; char queued with TransmitCommChar");
		//if (comStat.cbInQue)   my_set_window_title(" comStat.cbInQue bytes have been received, but not read");
		if (comStat.cbOutQue)  my_set_window_title(" comStat.cbOutQue bytes are awaiting transfer");
		display_title_message("%d before command clearerror rets %d in %g mS error %d",i,rets,dt, (int)dwErrors);
	      }
	    rets = Write_serial_port(hCom,lch,1);
	    if (rets <= 0)
	      {
		// Get and clear current errors on the port.
		if (ClearCommError(hCom, &dwErrors, &comStat)) // Report error in ClearCommError.
		  {
		    if (comStat.cbInQue)  Read_serial_port(hCom,chtmp,127,&len);
		    if (comStat.fCtsHold)  my_set_window_title(" Tx waiting for CTS signal");
		    if (comStat.fDsrHold)  my_set_window_title(" Tx waiting for DSR signal");
		    if (comStat.fRlsdHold) my_set_window_title(" Tx waiting for RLSD signal");
		    if (comStat.fXoffHold) my_set_window_title(" Tx waiting, XOFF char rec'd");
		    if (comStat.fXoffSent) my_set_window_title(" Tx waiting, XOFF char sent");
		    if (comStat.fEof)      my_set_window_title(" EOF character received");
		    if (comStat.fTxim)     my_set_window_title(" Character waiting for Tx; char queued with TransmitCommChar");
		    //if (comStat.cbInQue)   my_set_window_title(" comStat.cbInQue bytes have been received, but not read");
		    if (comStat.cbOutQue)  my_set_window_title(" comStat.cbOutQue bytes are awaiting transfer");
		    display_title_message("%d command sent failed rets %d in %g mS error %d",i,rets,dt, (int)dwErrors);
		  }
	      }
	  }
	if (rets <= 0)
	  {
	    t0 = my_uclock() - t0;
	    dt = 1000*(double)(t0)/get_my_uclocks_per_sec();
	    dtm += dt;
	    if (dt > dtmax) dtmax = dt;
	    if (ds != NULL)
	      {
		ds->yd[i] = dt;
		ds->xd[i] = ds2->xd[i] = i;
		ds2->yd[i] = rets;
	      }
	    //purge_com();
	    // Get and clear current errors on the port.
	    if (ClearCommError(hCom, &dwErrors, &comStat)) // Report error in ClearCommError.
	      {
		if (comStat.cbInQue)  Read_serial_port(hCom,chtmp,127,&len);
		if (comStat.fCtsHold)  my_set_window_title(" Tx waiting for CTS signal");
		if (comStat.fDsrHold)  my_set_window_title(" Tx waiting for DSR signal");
		if (comStat.fRlsdHold) my_set_window_title(" Tx waiting for RLSD signal");
		if (comStat.fXoffHold) my_set_window_title(" Tx waiting, XOFF char rec'd");
		if (comStat.fXoffSent) my_set_window_title(" Tx waiting, XOFF char sent");
		if (comStat.fEof)      my_set_window_title(" EOF character received");
		if (comStat.fTxim)     my_set_window_title(" Character waiting for Tx; char queued with TransmitCommChar");
		//if (comStat.cbInQue)   my_set_window_title(" comStat.cbInQue bytes have been received, but not read");
		if (comStat.cbOutQue)  my_set_window_title(" comStat.cbOutQue bytes are awaiting transfer");
		display_title_message("%d command sent failed rets %d in %g mS error %d",i,rets,dt, (int)dwErrors);
	      }
	    n_er_s++;
	    continue;
	  }
	
	ret = Read_serial_port(hCom,resu,nNumberOfBytesToRead,&lpNumberOfBytesWritten);
	for  (; ret == 0; )
	  ret = Read_serial_port(hCom,resu,nNumberOfBytesToRead,&lpNumberOfBytesWritten);
	t0 = my_uclock() - t0;
	dt = 1000*(double)(t0)/get_my_uclocks_per_sec();
	resu[lpNumberOfBytesWritten] = 0;
	if (ds != NULL)
	  {
	    ds->yd[i] = dt;
	    ds->xd[i] = i;
	    ds->xd[i] = ds2->xd[i] = i;
	    ds2->yd[i] = 0;
	  }
	dtm += dt;
	if (dt > dtmax) dtmax = dt;
	if (ret < 0)
	  {
	    // Get and clear current errors on the port.
	    if (ClearCommError(hCom, &dwErrors, &comStat))	      // Report error in ClearCommError.
	      {
		if (comStat.cbInQue)  Read_serial_port(hCom,chtmp,127,&len);
		if (comStat.fCtsHold)  my_set_window_title(" Tx waiting for CTS signal");
		if (comStat.fDsrHold)  my_set_window_title(" Tx waiting for DSR signal");
		if (comStat.fRlsdHold) my_set_window_title(" Tx waiting for RLSD signal");
		if (comStat.fXoffHold) my_set_window_title(" Tx waiting, XOFF char rec'd");
		if (comStat.fXoffSent) my_set_window_title(" Tx waiting, XOFF char sent");
		if (comStat.fEof)      my_set_window_title(" EOF character received");
		if (comStat.fTxim)     my_set_window_title(" Character waiting for Tx; char queued with TransmitCommChar");
		//if (comStat.cbInQue)   my_set_window_title(" comStat.cbInQue bytes have been received, but not read");
		if (comStat.cbOutQue)  my_set_window_title(" comStat.cbOutQue bytes are awaiting transfer");
		display_title_message("%d command read failed = %s  ret %d Number Of Bytes = %d in %g mS Err %d",i,resu,ret,lpNumberOfBytesWritten,dt, (int)dwErrors);
	      }
	    ds2->yd[i] = 2;
	    purge_com();
	    n_er_r++;
	    continue;
	  }
	for ( ; my_uclock() < tn; );
	if ((ds != NULL) && ((i%1000) == 0))
	  {
	      op->need_to_refresh = 1;
	      refresh_plot(pr, pr->n_op-1);		
	  }	
      }
    dtm /= ntimes;
    win_printf("%d command read = %s \n ret %d Number Of Bytes  = %d\n"
	       "in %g mS in avg %g max\n %d read er %d sent er"
	       ,i,resu,ret,lpNumberOfBytesWritten,dtm, dtmax,n_er_r,n_er_s);
    refresh_plot(pr, pr->n_op-1);		
    return D_O_K;
}

int purge_com(void)
{
   if (hCom == NULL) return 1; 
  return PurgeComm(hCom, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);
}
int init_port(void)
{
  int i;
  static int port_number = 1, sbaud = 0, hand = 0;
  char command[128], answer[128], *gr;
  int  ret, iret;
  unsigned long len = 0;
  
  if (hCom != NULL) return 0; 
  i = win_scanf("Port number ?%5d\nDump in and out in a debug file\n"
		"No %R yes %r\n"
		"HandShaking None %R Cts/Rts %r"
		,&port_number, &debug,&sbaud,&hand);
  if (i == CANCEL) return D_O_K;
  
  hCom = init_serial_port(port_number, SBAUD_115200/*generate_bauderate_value(sbaud+1)*/, hand);
  if (hCom == NULL) return 1;
  if (debug)
    {
      fp_debug = fopen(debug_file,"w");
      if (fp_debug != NULL) fclose(fp_debug);
    }
  purge_com();

  sprintf(command,"echo=0\r");
  if (Write_serial_port(hCom,command,strlen(command)) < 0)
    win_printf("error timeout in write echo");
  for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);
  answer[len] = 0;
  win_printf("After readind response to echo");
  gr = strstr(answer,"Echo off");
  return (gr != NULL) ? 0 : 1;
}

int my_sleep(long ms)
{
  unsigned long t0;

  for (t0 = my_uclock() + ms*(get_my_uclocks_per_sec()/1000); my_uclock() < t0; );
  return 0;
}


int CloseSerialPort(HANDLE hCom)
{
    if (CloseHandle(hCom) == 0) return win_printf("Close Handle FAILED!");
    return D_O_K;
}

int close_serial_port(void)
{
        if(updating_menu_state != 0)	return D_O_K;
        CloseSerialPort(hCom);
        return D_O_K;
}






/*
Attemp to read the pico response from serial port
if a full answer is available then transfer it
otherwise grab characters and respond that full message is not yet there

*/

char *catch_pending_request_answer(int *size, int im_n, int *error, unsigned long *t0)
{
  static char answer[128], request_answer[128];
  unsigned long dwRead = 0;
  static int i = 0;
  int j, max_size = 128, lf_found = 0;
  char lch[2];

  if (t0 != NULL) *t0 = my_uclock();
  if (hCom == NULL) 
   {
     *error = -3;
     if (t0 != NULL) *t0 = my_uclock()- *t0;
     return NULL;   // no serial port !
   }

  for (j = 0; j == 0 && i < max_size-1; )
    {// we grab rs232 char already arrived
      dwRead = 0;
      if (ReadData(hCom, lch, 1, &dwRead) == 0)
	{  // something wrong
	  answer[i] = 0;
	  dwRead = i;
	  if (debug)
	    {
	      fp_debug = fopen(debug_file,"a");
	      if (fp_debug != NULL)
		{
		  fprintf (fp_debug,"<-- ReadData error at im %d\n", im_n);
		  fprintf (fp_debug,"<\t %s\n",answer);
		  fclose(fp_debug);
		}
	    }
	  *error = -2;
	  if (t0 != NULL) *t0 = my_uclock()- *t0;
	  return NULL;    // serial port read error !
	}
      if (dwRead == 1) 
	{
	  answer[i] = lch[0];
	  j = lf_found = (lch[0] == '\n') ? 1 : 0;
	  i++;
	}
      else if (dwRead == 0) j = 1; 
    }
  answer[i] = 0;
  *size = i;
  if (lf_found)
    {
      *error = 0;
      strncpy(request_answer, answer,i);
      if (debug)
	{
	  fp_debug = fopen(debug_file,"a");
	  if (fp_debug != NULL)
	    {
	      fprintf (fp_debug,"Im %d : <\t %s\n",im_n,request_answer);
	      fclose(fp_debug);
	    }
	}
      if (t0 != NULL) *t0 = my_uclock()- *t0;
      i = 0;
      return request_answer;
    }
  *error = 1;
  if (debug)
    {
      fp_debug = fopen(debug_file,"a");
      if (fp_debug != NULL)
	{
	  fprintf (fp_debug,"Im %d : <\t %s...\n",im_n,answer);
	  fclose(fp_debug);
	}
    }
  if (t0 != NULL) *t0 = my_uclock()- *t0;
  return NULL;
}


ilk? Get interlock state 0 = OK, 1 = interlock open
@cobas
Enable/disable autostart
See sect 7.1 for description
0 = disable, 1 = enable
@cobas? Get autostart enable state 0 = disabled, 1 = enabled
l? Get laser ON/OFF state 0 = OFF, 1 = ON
l
Laser ON/OFF
Requires autostart disabled. Use this command
for manual ON/OFF of OEM models.
0 = OFF, 1 = ON

p Set output power
Float (W)
(e.g. p 0.050 for 50 mW)
pa? Read output power Float (W)


leds? Status of 4 LEDs
Int [0:15]
Bit 0 = “POWER ON”
Bit 1 = “LASER ON”
Bit 2 = “LASER LOCK”
Bit 3 = “ERROR”
1 = LED on
0 = LED off
f? Get operating fault
0 = no fault
1 = temperature error
3 = open interlock
4 = constant power fault
cf Clear fault
@cobasdr
Enable/disable direct control
See sect 8.4 for description
0 = disable, 1 = enable

@cobasdr? Get direct control enable state 0 = disabled 1 = enabled

@cob1
Laser ON after interlock Forces the laser into
Autostart without checking if autostart is
enabled


  int get_control_enable_state(void)
{
  int i;
  unsigned int result;
  char command[128], answer[128], *gr;
  unsigned long len = 0;
  int set = -1, ret, iret;
  DWORD   dwErrors;
  HANDLE hCom;

  if(updating_menu_state != 0)	return D_O_K;
  sprintf(command,"@cobasdr?\r");

  i = RETRIEVE_MENU_INDEX;


  if (i == 1) (hCom = hCom1)
    else if (i ==2) (hCom = hCom2)
      else return win_printf("Can not find the port");
  if (hCom != NULL)
    {
      ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);  
      result = atof(answer);
      win_printf("Laser power = %f (W)",result);
    }
  else win_printf("hCom not initialized");


  return D_O_K;
}


int get_laser_power(void)
{
  int i;
  unsigned int result;
  char command[128], answer[128], *gr;
  unsigned long len = 0;
  int set = -1, ret, iret;
  DWORD   dwErrors;
  HANDLE hCom;

  if(updating_menu_state != 0)	return D_O_K;
  sprintf(command,"p?\r");

  i = RETRIEVE_MENU_INDEX;


  if (i == 1) (hCom = hCom1)
    else if (i ==2) (hCom = hCom2)
      else return win_printf("Can not find the port");
  if (hCom != NULL)
    {
      ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);  
      result = atof(answer);
      win_printf("Laser power = %f (W)",result);
    }
  else win_printf("hCom not initialized");


  return D_O_K;
}

int laser_working_hours(void)
{
  int i;
  unsigned int result;
  char command[128], answer[128], *gr;
  unsigned long len = 0;
  int set = -1, ret, iret;
  DWORD   dwErrors;
  HANDLE hCom;

  if(updating_menu_state != 0)	return D_O_K;
  sprintf(command,"hrs?\r");

  i = RETRIEVE_MENU_INDEX;


  if (i == 1) (hCom = hCom1)
    else if (i ==2) (hCom = hCom2)
      else return win_printf("Can not find the port");
  if (hCom != NULL)
    {
      ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);  
      result = atof(answer);
      win_printf("Laser hours = %f",result);
    }
  else win_printf("hCom not initialized");


  return D_O_K;
}


int laser_serial_number(void)
{
  int i;
  unsigned int result;
  char command[128], answer[128], *gr;
  unsigned long len = 0;
  int set = -1, ret, iret;
  DWORD   dwErrors;

  if(updating_menu_state != 0)	return D_O_K;

  i = RETRIEVE_MENU_INDEX;
  sprintf(command,"sn?\r");
  if (i == 1) (hCom = hCom1)
    else if (i ==2) (hCom = hCom2)
      else return win_printf("Can not find the port");
  if (hCom != NULL)
    {
      ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);  
      result = atoi(answer);
      win_printf("Laser serial number = %d",result);
    }
  else win_printf("hCom not initialized");
  return D_O_K;
}

MENU *cobolt_serial_menu(void)
{
  static MENU mn[32];
  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"Get serial Number 514", laser_serial_number,NULL,MENU_INDEX(GET_SERIAL_NUMBER_514),NULL);
  add_item_to_menu(mn,"Get serial Number 561", laser_serial_nulber,NULL,MENU_INDEX(GET_SERIAL_NUMBER_561),NULL);
  add_item_to_menu(mn,"Laser working hours 514", laser_working_hours,NULL,MENU_INDEX(GET_SERIAL_NUMBER_514),NULL);
  add_item_to_menu(mn,"Laser working hours 561", laser_working_hours,NULL,MENU_INDEX(GET_SERIAL_NUMBER_561),NULL);
  add_item_to_menu(mn,"GET Laser power 514", get_laser_power,NULL,MENU_INDEX(GET_SERIAL_NUMBER_514),NULL);
  add_item_to_menu(mn,"GET Laser power 561", get_laser_power,NULL,MENU_INDEX(GET_SERIAL_NUMBER_561),NULL);





MENU *serialw_menu(void) 
{
  static MENU mn[32];
 
    
  if (mn[0].text != NULL)	return mn;
  
    add_item_to_menu(mn,"Init serial port", init_port,NULL,0,NULL);
    add_item_to_menu(mn,"Write serial port", write_command_on_serial_port,NULL,0,NULL);
    add_item_to_menu(mn,"Read serial port", read_on_serial_port,NULL,0,NULL);
    add_item_to_menu(mn,"Write read serial port", write_read_serial_port,NULL,0,NULL);
    add_item_to_menu(mn,"Write read n times", n_write_read_serial_port,NULL,0,NULL);
    add_item_to_menu(mn,"Close serial port", close_serial_port,NULL,0,NULL);
    add_item_to_menu(mn,"Clear error", do_check_for_rs232_error,NULL,0,NULL);


    return mn;
}



int	Pico_serial_port_unload(int argc, char **argv)
{
    if (CloseHandle(hCom) == 0) return win_printf("Close Handle FAILED!");
	remove_item_to_menu(plot_treat_menu, "SERIAL PORT", NULL, NULL);
	return D_O_K;
}

int GetPortNumXP2000Vista(WORD vid, WORD pid, char* ser)
{
  //Variables used for Registry access
  HKEY tmpKey, tmpSubKey, tmpPortKey;
  char portKeyString[256];
  DWORD valtype;
  DWORD length = 100;
  char portString[101];
  //Set portnum to -1, so if there is an error we will
  //know by returning a negative port value
  int portNum = -1;
  //  Open keys to get to the key where the port number is located. This key is:
  //  HKLM\System\CurrentControlSet\Enum\USB\Vid_xxxx&Pid_yyyy&Mi_00\zzzz_00\Device  Parameters\PortName
    if (ERROR_SUCCESS == RegOpenKeyEx(HKEY_LOCAL_MACHINE, "SYSTEM\\CurrentControlSet\\", 0,
				      KEY_READ, &tmpKey))
      {
	if (ERROR_SUCCESS == RegOpenKey(tmpKey, "Enum\\USB\\", &tmpSubKey))
	  {
	    //Loop through and replace spaces for WinXP2000Vista
	    int i = 0;
	    while (ser[i] != '\0')
	      {
		if (ser[i] == 0x20)
		  ser[i] = '_';
		i++;
	      }
	    //The portkey string should look like this
	    //"Vid_XXXX&Pid_XXXX&MI_00\\XXXX_00" where the XXXX's are Vid, Pid and serial string  - version less than 5.0
	   
	      //"Vid_XXXX&Pid_XXXX\\XXXX" where the XXXX's are Vid, Pid and serial string - version greater than or equal to 5.0
	      snprintf(portKeyString,256,"Vid_%04x&Pid_%04x&Mi_00\\%s_00\\Device Parameters\\", vid, pid, ser);
	    //If the portkey string is in the registry, then go ahead and open the portname
	    if (ERROR_SUCCESS == RegOpenKeyEx(tmpSubKey, portKeyString, 0, KEY_READ,
					      &tmpPortKey))
	      {
		if (ERROR_SUCCESS == RegQueryValueEx(tmpPortKey, "PortName", NULL, &valtype,
						     (unsigned char *)portString, &length))
		  {
		    // When we obtain this key, it will be in string format of
		    // "COMXX" where XX is the port. Simply make the first three
		    // elements of the string 0, and call the atoi function to obtain
		    // the number of the port.
		    portString[0] = '0';
		    portString[1] = '0';
		    portString[2] = '0';
		    portNum = atoi(portString);
		  }
		//Make sure to close all open keys for cleanup
		RegCloseKey(tmpPortKey);
	      }
	    else
	      {
		snprintf(portKeyString,256,"Vid_%04x&Pid_%04x\\%s\\Device Parameters\\", vid, pid,ser);
		//If the portkey string is in the registry, then go ahead and open the portname
		if (ERROR_SUCCESS == RegOpenKeyEx(tmpSubKey, portKeyString, 0, KEY_READ,
						  &tmpPortKey))
		  {
		    if (ERROR_SUCCESS == RegQueryValueEx(tmpPortKey, "PortName", NULL, &valtype,
							 (unsigned char *)portString, &length))
		      {
			// When we obtain this key, it will be in string format of
			// "COMXX" where XX is the port. Simply make the first three
			// elements of the string 0, and call the atoi function to obtain
			// the number of the port.
			portString[0] = '0';
			portString[1] = '0';
			portString[2] = '0';
			portNum = atoi(portString);
		      }
		    //Make sure to close all open keys for cleanup
		    RegCloseKey(tmpPortKey);
		  }
	      }
	    RegCloseKey(tmpSubKey);
	  }
	RegCloseKey(tmpKey);
      }
  RegCloseKey(HKEY_LOCAL_MACHINE);
    // Return the number of the port the device is connected too
  return portNum;
}


# ifdef C2103
int GetPortNumXP2000Vista(WORD vid, WORD pid, char* ser)
{
  //Variables used for Registry access
  HKEY tmpKey, tmpSubKey, tmpPortKey;
  CString portKeyString;
  DWORD valtype;
  char* portString;
  DWORD length = 100;
  portString = new char[101];
  //Set portnum to -1, so if there is an error we will
  //know by returning a negative port value
  int portNum = -1;
  //  Open keys to get to the key where the port number is located. This key is:
  //  HKLM\System\CurrentControlSet\Enum\USB\Vid_xxxx&Pid_yyyy&Mi_00\zzzz_00\Device
  Parameters\PortName
    if (ERROR_SUCCESS == RegOpenKeyEx(HKEY_LOCAL_MACHINE, "SYSTEM\\CurrentControlSet\\", 0,
				      KEY_READ, &tmpKey))
      {
	if (ERROR_SUCCESS == RegOpenKey(tmpKey, "Enum\\USB\\", &tmpSubKey))
	  {
	    //Loop through and replace spaces for WinXP2000Vista
	    int i = 0;
	    while (ser[i] != '\0')
	      {
		if (ser[i] == 0x20)
		  ser[i] = '_';
		i++;
	      }
	    //The portkey string should look like this
	    //"Vid_XXXX&Pid_XXXX&MI_00\\XXXX_00" where the XXXX's are Vid, Pid and serial string
	    - version less than 5.0
	      //"Vid_XXXX&Pid_XXXX\\XXXX" where the XXXX's are Vid, Pid and serial string -
	      version greater than or equal to 5.0
	      portKeyString.Format("Vid_%04x&Pid_%04x&Mi_00\\%s_00\\Device Parameters\\", vid,
				   pid, ser);
	    //If the portkey string is in the registry, then go ahead and open the portname
	    if (ERROR_SUCCESS == RegOpenKeyEx(tmpSubKey, portKeyString, 0, KEY_READ,
					      &tmpPortKey))
	      {
		if (ERROR_SUCCESS == RegQueryValueEx(tmpPortKey, "PortName", NULL, &valtype,
						     (unsigned char *)portString, &length))
		  {
		    // When we obtain this key, it will be in string format of
		    // "COMXX" where XX is the port. Simply make the first three
		    // elements of the string 0, and call the atoi function to obtain
		    // the number of the port.
		    portString[0] = '0';AN197
					  8 Rev. 0.7
					  portString[1] = '0';
		    portString[2] = '0';
		    portNum = atoi(portString);
		  }
		//Make sure to close all open keys for cleanup
		RegCloseKey(tmpPortKey);
	      }
	    else
	      {
		portKeyString.Format("Vid_%04x&Pid_%04x\\%s\\Device Parameters\\", vid, pid,
				     ser);
		//If the portkey string is in the registry, then go ahead and open the portname
		if (ERROR_SUCCESS == RegOpenKeyEx(tmpSubKey, portKeyString, 0, KEY_READ,
						  &tmpPortKey))
		  {
		    if (ERROR_SUCCESS == RegQueryValueEx(tmpPortKey, "PortName", NULL, &valtype,
							 (unsigned char *)portString, &length))
		      {
			// When we obtain this key, it will be in string format of
			// "COMXX" where XX is the port. Simply make the first three
			// elements of the string 0, and call the atoi function to obtain
			// the number of the port.
			portString[0] = '0';
			portString[1] = '0';
			portString[2] = '0';
			portNum = atoi(portString);
		      }
		    //Make sure to close all open keys for cleanup
		    RegCloseKey(tmpPortKey);
		  }
	      }
	    RegCloseKey(tmpSubKey);
	  }
	RegCloseKey(tmpKey);
      }
  RegCloseKey(HKEY_LOCAL_MACHINE);
  delete portString;
  // Return the number of the port the device is connected too
  return portNum;
}
#endif
#endif
