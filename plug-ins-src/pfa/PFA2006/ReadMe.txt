	=================================================
	Phase Function Approach (Version 0.8)

	--- a software for reconstruction of rate
	constant distribution in complex decay data


	Released May 22, 2006
	Zhuang Lab, Harvard University
	=================================================

This software serves the following reference:
	_________________________________________________

	Y. Zhou & X. Zhuang, Robust Reconstruction
	of the Rate Constant Distribution Using the Phase
	Function Method, Biophys. J., 91:pageno. (2006)
	_________________________________________________

Note: Page number information will be updated upon the publication of this paper.

The online supporting material of the above reference is available at http://www.biophysj.org 

# Contents of the software package #

	* THE SOFTWARE KERNEL

		- The C language source code PFA2006.C (compiled by Turbo C 2.0, compatible with WinXP)

		- The executable file PFA2006.EXE (generated from the above C file, compatible with WinXP)

		- Graphic support and fonts from Turbo C 2.0: EGAVGA.BGI, GOTH.CHR, LITT.CHR and TRIP.CHR (also freely available from Borland Developer Network http://bdn.borland.com/article/20841)

	* THE SUPPLEMENTARY FILES

		- The related paper in Biophysical Journal (BJ2006.PDF)	and its supplementary material(SUPPMAT.PDF)	[Will be updated upon the publication of the paper.]

		- The Read-Me file README.TXT

		- The sample input data files in TXT format: TESTin.TXT, fig3c.TXT, fig3d.TXT, fig4a.TXT, fig4b.TXT, fig4c.TXT, fig5a.TXT (Here, figure numbers refer to the journal reference in the file BJ2006.PDF, TESTin.txt represents a Mittag-Leffler function with exponent 1/2.)

		- The sample output data files in TXT format: TESTout.TXT, OUT.TXT

# Using the software Phase Function Approach (Version 0.8) #

	* To Run the "Phase Function Approach" program, double click the executable file PFA2006.exe.

	* Enter the desired input and output file names in the 'FILENAME.EXT' format. A TXT extension is preferred. 

		- The input file (TESTin.txt, fig3c.txt, etc.) are in the format of a NON-DECREASING integer sequence with fewer than 8000 entries. In other words, the input file is the "CUMULATIVE counts with dwell time shorter than time t", where the maximum measurable time is shorter than 8000x time resolution limit.

	* Then the program enters a graphics mode. Within 10 seconds (measured on IBM T42 laptop), the results are plotted on the screen and written to the output file. The program exits naturally after a key strike.

		- The sample output file TESTout.txt is related to TESTin.txt, and the file OUT.TXT results from fig3c.TXT. You may recover also Figures 3d, 4a, 4b and 5a in the Biophysical Journal paper using the current software and the related input files.

	! You are free to use and distribute the ORIGINAL form of this software. Please contact zhou3@fas.harvard.edu if you need to modify the software source code (PFA2006.C). Please cite the aforementioned journal reference if you are using the output of this software for an academic publication.



