#ifndef _PFA_REWRITTEN_C_
#define _PFA_REWRITTEN_C_


# include "allegro.h"
# include "xvin.h"
//#include <math.h>

/* If you include other plug-ins header do it here*/ 

#include <complex.h> // Include BEFORE fftw3
//bug found! see this page:
//      http://www.fftw.org/fftw3_doc/Complex-numbers.html#Complex-numbers
// two options:
       // either we include complex.h and then treat out[i] as a built in complex number
       // in this case the real part is obtained via the function creal(out[i])
       //
       // or we do not include complex.h and then out[i] is allocated as a double[2].
// ciao 
// francesco
#include <fftw3.h>

/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "pfa.h"

//#define PI 3.14159265358979323846264338
#define LC 0.88137358701954302523260932 /*Lattice constant h=log(1+sqrt(2))*/
#define LOG10 1
#define LINEAR 0
#define MAXOCTAVE 2048 /*Partition number for fast Fourier transform*/
#define MAXDATASIZE 8000 /*Maximum bins in the t-domain*/
#define RESOLUTION 1 /*Time resolution limit of t-domain data*/
#define LOGKBINS 416 /*Maximum bins in the lnk-domain (activation energy-domain)*/


int do_initialize(void)
{   
    pltreg *pr =NULL;
    O_p *op =NULL;
    
    if(updating_menu_state != 0) return D_O_K;
    
    /* Grabs the data from the current project */
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
	  return win_printf_OK("cannot find data");
	  
	/* Gets size of data */
    datasize = dsi->nx;  
	  
    /* Allocating datasets */
    if ((dsF = create_and_attach_one_ds(op, datasize, datasize, 0)) == NULL)
	  return win_printf_OK("cannot create dataset !");
	if ((dsF = alloc_data_set_y_error(dsF)) == NULL)
	  return win_printf_OK("cannot allocate error !");
	/*ds for output function phi*/
	if ((ds_phase = create_and_attach_one_ds(op, datasize, datasize, 0)) == NULL)
	  return win_printf_OK("cannot create dataset !");
	/*ds for output function rho*/
	if ((dsrho = create_and_attach_one_ds(op, datasize, datasize, 0)) == NULL)
	  return win_printf_OK("cannot create dataset !");
	/*ds for real part of FFT*/
	if ((ds_fft_real = create_and_attach_one_ds(op, datasize, datasize, 0)) == NULL)
	  return win_printf_OK("cannot create dataset !");
      /*ds for imaginary part of FFT*/
	if ((ds_fft_imaginary = create_and_attach_one_ds(op, datasize, datasize, 0)) == NULL)
	  return win_printf_OK("cannot create dataset !");  
	  
    return D_O_K;
}

int do_fft(void)
{
    fftw_complex *out; // Output
    double temp_complex[2]; // A temporary complex number
    double *in; // Input
    fftw_plan p; // FFT task handle
    
    int i;
    
    if(updating_menu_state != 0) return D_O_K;
    
    /* Allocating I/O sets of fftw */
    in = (double*) fftw_malloc(sizeof(double) * datasize);
    out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * datasize);
    
    /* Copies the dataset into "in" */
    for (i=0 ; i<datasize ; i++)
    {
        in[i] = dsi->yd[i];
    }
    
    /* Creates the handle to the specified fft */
    p = fftw_plan_dft_r2c_1d(datasize, in, out, FFTW_ESTIMATE); //Backward is fftw_plan_dft_r2c_1d
    
    /* Executes the fft */
    fftw_execute(p); /* repeat as needed */
        
    /* Copies "out" into the dataset */
    for (i=0 ; i<datasize ; i++)
    {
        //temp_complex = out[i];
        ds_fft_real->yd[i] = creal(out[i]);
        ds_fft_imaginary->yd[i] = cimag(out[i]);
    }    
        
    /*Memory care */
    fftw_destroy_plan(p);
    fftw_free(in); fftw_free(out);
    
    return D_O_K;
}

int do_retrieve_phase(void)
{
    int i_re, i_im;
    
    if(updating_menu_state != 0) return D_O_K;
    
    /* Attention : we are now in 2 dimensions (Re W & Im W) ! */
    for (i_re = 0 ; i_re < datasize ; i_re++)
    {
        for (i_im = 0 ; i_im < datasize ; i_im++)
        {
            //ds_phase->data[i_re]->data[i_im] = carg();
        }
    }
    
    return D_O_K;
}

MENU *pfa_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	//add_item_to_menu(mn,"do pfa", do_pfa,NULL,0,NULL);
	add_item_to_menu(mn,"do fft", do_fft,NULL,0,NULL);
	return mn;
}

int	pfa_main(int argc, char **argv)
{
	add_plot_treat_menu_item ( "pfa", NULL, pfa_plot_menu(), 0, NULL);
	return D_O_K;
}

int	pfa_unload(int argc, char **argv)
{
	remove_item_to_menu(plot_treat_menu, "pfa", NULL, NULL);
	return D_O_K;
}

#endif
