#ifndef _PFA_H_
#define _PFA_H_

PXV_FUNC(MENU*, pfa_plot_menu, (void));
PXV_FUNC(int, pfa_main, (int argc, char **argv));

PXV_FUNC(int, do_fft, (void));

// Defines global variables
int datasize;
d_s *dsF;
d_s *ds_phase;
d_s *dsrho;
d_s *dsi;
d_s *ds_fft_real;
d_s *ds_fft_imaginary;

#endif

