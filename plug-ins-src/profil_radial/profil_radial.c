/*
*    Plug-in program for image treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _PROFIL_RADIAL_C_
#define _PROFIL_RADIAL_C_

# include "allegro.h"
# include "xvin.h"
#include <immintrin.h>

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "profil_radial.h"
//place here other headers of this plugin 

//# define SKIP_IM
# define PIAS

/*  take a radial image profile of an image with a different scaling in
 *  x and y, real x = i_image * ax,  y = i_image * ay
 *
 *
 *
 */


float *radial_non_pixel_square_image_sym_profile_in_array(float *rz, O_i *ois, float xc, float yc, int r_max, float ax,
        float ay)
{
    int i, j;
    union pix *ps = NULL;
    int onx, ony, ri, type;
    float z = 0, z1, x, y, r, p, p0 = 0, pr0 = 0;
    int imin, imax, jmin, jmax, npr0 = 0; //  xci, yci
# if !defined(PLAYITSAM) && !defined(PIAS)
    float eva_black = 0, eva_level = 0, eva_norm = 0, thres_w = 0.001, eva_w;
    int n_eva_black = 0;
# endif

    //for (i = 0; i < 2*r_max && i < 256; i++)  radial_dx[i] = 0;
    //for (i = 0; i < 4096; i++)      evanescent_black_level_histo[i] = 0;
    if (ois == NULL)    return NULL;

    if (rz == NULL)     rz = (float *)calloc(2 * r_max, sizeof(float));

    if (rz == NULL)     return NULL;

    ps = ois->im.pixel;
    ony = ois->im.ny;
    onx = ois->im.nx;
    type = ois->im.data_type;

    for (i = 0; i < 2 * r_max; i++)   rz[i] = 0;

    //xci = (int)xc;
    //yci = (int)yc;

    imin = (int)(yc - ((1+r_max) / ay));
    imax = (int)(0.5 + yc + ((1+r_max) / ay));
    jmin = (int)(xc -  ((1+r_max) / ax));
    jmax = (int)(0.5 + xc + ((1+r_max) / ax));
    imin = (imin < 0) ? 0 : imin;
    imax = (imax > ony) ? ony : imax;
    jmin = (jmin < 0) ? 0 : jmin;
    jmax = (jmax > onx) ? onx : jmax;

    if (type == IS_COMPLEX_IMAGE)
    {
        for (i = imin; i < imax; i++)
        {
            y = ay * ((float)i - yc);
            y *= y;

            for (j = jmin; j < jmax; j++)
            {
                x = ax * ((float)j - xc);
                x *= x;
                r = (float)sqrt(x + y);
                ri = (int)r;

                if (ri > r_max) continue;   /* bug break;*/

                if (ois->im.mode == RE)
                    z = ps[i].fl[2 * j];
                else if (ois->im.mode == IM)
                    z = ps[i].fl[2 * j + 1];
                else if (ois->im.mode == AMP)
                {
                    z = ps[i].fl[2 * j];
                    z1 = ps[i].fl[2 * j + 1];
                    z = sqrt(z * z + z1 * z1);
                }
                else if (ois->im.mode == AMP_2)
                {
                    z = ps[i].fl[2 * j];
                    z1 = ps[i].fl[2 * j + 1];
                    z = z * z + z1 * z1;
                }
                else if (ois->im.mode == LOG_AMP)
                {
                    z = ps[i].fl[2 * j];
                    z1 = ps[i].fl[2 * j + 1];
                    z = z * z + z1 * z1;
                    z = (z > 0) ? log10(z) : -40.0;
                }
                else
                {
                    win_printf("Unknown mode for complex image");
                    return NULL;
                }

                p = r - ri;

                if (ri == r_max)
                {
                    p0 += (1 - p) * z;
                    pr0 += (1 - p);
		    npr0++;
                    //radial_dx[r_max] -= 1-p;
                }
                else
                {
                    rz[ri] += (1 - p) * z;
                    rz[ri + r_max] += (1 - p);
                    //radial_dx[ri] += p;
                }

# if !defined(PLAYITSAM) && !defined(PIAS)
                eva_w = (1 - p) * evanescent_spot_weighting[ri];
# endif
                ri++;

                if (ri < r_max)
                {
                    rz[ri] += p * z;
                    rz[ri + r_max] += p;
                    //radial_dx[ri] -= 1-p;
                }

# if !defined(PLAYITSAM) && !defined(PIAS)
                eva_w += p * evanescent_spot_weighting[ri];

                if (eva_w < thres_w)
                {
                    eva_black += z;
                    n_eva_black++;
                }
                else
                {
                    eva_level += eva_w * z;
                    eva_norm += eva_w;
                }

# endif
            }
        }
    }
    else if (type == IS_FLOAT_IMAGE)
    {
        for (i = imin; i < imax; i++)
        {
            y = ay * ((float)i - yc);
            y *= y;

            for (j = jmin; j < jmax; j++)
            {
                x = ax * ((float)j - xc);
                x *= x;
                r = (float)sqrt(x + y);
                ri = (int)r;

                if (ri > r_max) continue;   /* bug break;*/

                z = ps[i].fl[j];
                p = r - ri;
# if !defined(PLAYITSAM) && !defined(PIAS)
                eva_w = (1 - p) * evanescent_spot_weighting[ri];
# endif

                if (ri == r_max)
                {
                    p0 += (1 - p) * z;
                    pr0 += (1 - p);
		    npr0++;		    
                    //radial_dx[r_max] -= 1-p;
                }
                else
                {
                    rz[ri] += (1 - p) * z;
                    rz[ri + r_max] += (1 - p);
                    //radial_dx[ri] += p;
                }

                ri++;

                if (ri < r_max)
                {
                    rz[ri] += p * z;
                    rz[ri + r_max] += p;
                    //radial_dx[ri] -= 1-p;
                }

# if !defined(PLAYITSAM) && !defined(PIAS)
                eva_w += p * evanescent_spot_weighting[ri];

                if (eva_w < thres_w)
                {
                    eva_black += z;
                    n_eva_black++;
                }
                else
                {
                    eva_level += eva_w * z;
                    eva_norm += eva_w;
                }

# endif
            }
        }
    }
    else if (type == IS_INT_IMAGE)
    {
        for (i = imin; i < imax; i++)
        {
            y = ay * ((float)i - yc);
            y *= y;

            for (j = jmin; j < jmax; j++)
            {
                x = ax * ((float)j - xc);
                x *= x;
                r = (float)sqrt(x + y);
                ri = (int)r;

                if (ri > r_max) continue;   /* bug break;*/

                z = (float)ps[i].in[j];
                p = r - ri;
# if !defined(PLAYITSAM) && !defined(PIAS)
                eva_w = (1 - p) * evanescent_spot_weighting[ri];
# endif

                if (ri == r_max)
                {
                    p0 += (1 - p) * z;
                    pr0 += (1 - p);
		    npr0++;
                    //radial_dx[r_max] -= 1-p;
                }
                else
                {
                    rz[ri] += (1 - p) * z;
                    rz[ri + r_max] += (1 - p);
                    //radial_dx[ri] += p;
                }

                ri++;

                if (ri < r_max)
                {
                    rz[ri] += p * z;
                    rz[ri + r_max] += p;
                    //radial_dx[ri] -= 1-p;
                }

# if !defined(PLAYITSAM) && !defined(PIAS)
                eva_w += p * evanescent_spot_weighting[ri];

                if (eva_w < thres_w)
                {
                    eva_black += z;
                    n_eva_black++;
                }
                else
                {
                    eva_level += eva_w * z;
                    eva_norm += eva_w;
                }

# endif
            }
        }
    }
    else if (type == IS_UINT_IMAGE)
    {
        for (i = imin; i < imax; i++)
        {
            y = ay * ((float)i - yc);
            y *= y;

            for (j = jmin; j < jmax; j++)
            {
                x = ax * ((float)j - xc);
                x *= x;
                r = (float)sqrt(x + y);
                ri = (int)r;

                if (ri > r_max) continue;   /* bug break;*/

                z = (float)ps[i].ui[j];
                p = r - ri;
# if !defined(PLAYITSAM) && !defined(PIAS)
                eva_w = (1 - p) * evanescent_spot_weighting[ri];
# endif

                if (ri == r_max)
                {
                    p0 += (1 - p) * z;
                    pr0 += (1 - p);
		    npr0++;		    
                    //radial_dx[r_max] -= 1-p;
                }
                else
                {
                    rz[ri] += (1 - p) * z;
                    rz[ri + r_max] += (1 - p);
                    //radial_dx[ri] += p;
                }

                ri++;

                if (ri < r_max)
                {
                    rz[ri] += p * z;
                    rz[ri + r_max] += p;
                    //radial_dx[ri] -= 1-p;
                }

# if !defined(PLAYITSAM) && !defined(PIAS)
                eva_w += p * evanescent_spot_weighting[ri];

                if (eva_w < thres_w)
                {
                    eva_black += z;
                    n_eva_black++;
                }
                else
                {
                    eva_level += eva_w * z;
                    eva_norm += eva_w;
                }

# endif
            }
        }
    }
    else if (type == IS_CHAR_IMAGE)
    {
        for (i = imin; i < imax; i++)
        {
            y = ay * ((float)i - yc);
            y *= y;

            for (j = jmin; j < jmax; j++)
            {
                x = ax * ((float)j - xc);
                x *= x;
                r = (float)sqrt(x + y);
                ri = (int)r;
		//		if (j == jmin || j == jmax-1)
		//  ps[i].ch[j] = 240;
                if (ri > r_max) continue;   /* bug break;*/
#ifdef SKIP_IM 
	    if (ri < r_max)
	      rz[ri] += r;
#else
                z = (float)ps[i].ch[j];		
                p = r - ri;
# if !defined(PLAYITSAM) && !defined(PIAS)
                eva_w = (1 - p) * evanescent_spot_weighting[ri];
# endif

                if (ri == r_max)
                {
                    p0 += (1 - p) * z;
                    pr0 += (1 - p);
		    npr0++;
		    //ps[i].ch[j] = 255;//3*ri;
		    //win_printf("ri == rmax\ni = %d jmin = %d j = %d jmax = %d",i,jmin,j,jmax);
                    //radial_dx[r_max] -= 1-p;
                }
                else
                {
                    rz[ri] += (1 - p) * z;
                    rz[ri + r_max] += (1 - p);
		    //ps[i].ch[j] = ri;
		    
                    //radial_dx[ri] += p;
                }

                ri++;

                if (ri < r_max)
                {
                    rz[ri] += p * z;
                    rz[ri + r_max] += p;
                    //radial_dx[ri] -= 1-p;
                }

# if !defined(PLAYITSAM) && !defined(PIAS)
                eva_w += p * evanescent_spot_weighting[ri];

                if (eva_w < thres_w)
                {
                    eva_black += z;
                    n_eva_black++;
                }
                else
                {
                    eva_level += eva_w * z;
                    eva_norm += eva_w;
                }

# endif
# endif

            }
        }
    }

    for (i = 0; i < r_max; i++)
    {
        rz[i] = (rz[i + r_max] == 0) ? 0 : rz[i] / rz[i + r_max];
        //      radial_dx[i] = (rz[i+r_max] == 0) ? 0 : radial_dx[i]/rz[i+r_max];
    }

    p0 = (pr0 == 0) ? 0 : p0 / pr0;

    for (i = 0; i < r_max; i++)     rz[i + r_max] = rz[i];

    for (i = 1; i < r_max; i++)     rz[r_max - i] = rz[r_max + i];

    rz[0] = p0;
# if !defined(PLAYITSAM) && !defined(PIAS)

    if (n_eva_black > 0)    eva_black /= n_eva_black;

    evanescent_black_avg = eva_black;
    evanescent_black_n_avg = n_eva_black;

    if (eva_norm > 0.0) eva_level /= eva_norm;

    evanescent_avg_intensity = eva_level;
    evanescent_avg_n_appo = eva_norm;
# endif
    return rz;
}




typedef float v8sf __attribute__((vector_size(32)));   // vector of four single floats
union f8vector
{
    v8sf v;
    float f[8];
};
union f8vector f8, f8ax, f8ay, f80, f8x, f8y, f8xc, f8yc, f8y2, f8r[256], f8x2[256];





// not yet satisfactory !

float *radial_non_pixel_square_image_sym_profile_in_array_avx2(float *rz, O_i *ois, float xc, float yc, int r_max,
        float ax, float ay)
{
    int i, j;
    union pix *ps = NULL;
    int onx, ony, ri, type, onx_8;
    float z = 0, *r, p, p0 = 0, pr0 = 0, rmax2, tmp;//, x, y; // , z1,
    int imin, imax, jmin, jmax, jmin0, jmin_8, jmin_80, jmax_8, j_nxmax, npr0 = 0;
    //int win_res = 0;
    //, jmax0, jmax_80


    //for (i = 0; i < 2*r_max && i < 256; i++)  radial_dx[i] = 0;
    if (ois == NULL)    return NULL;

    if (rz == NULL)     rz = (float *)calloc(2 * r_max, sizeof(float));

    if (rz == NULL)     return NULL;

    ps = ois->im.pixel;
    ony = ois->im.ny;
    onx = ois->im.nx;
    onx_8 = onx >> 3;
    type = ois->im.data_type;

    for (i = 0; i < 2 * r_max; i++)   rz[i] = 0; 

    rmax2 = (r_max+1) * (r_max+1);
    imin = (int)(yc - ((r_max+1) / ay));
    imax = (int)(0.5 + yc + ((r_max+1) / ay));
    jmin = (int)(xc -  ((r_max+1) / ax));
    jmax = (int)(0.5 + xc + ((r_max+1) / ax));
    imin = (imin < 0) ? 0 : imin;
    imax = (imax > ony) ? ony : imax;
    jmin = (jmin < 0) ? 0 : jmin;
    jmax = (jmax > onx) ? onx : jmax;

    jmin_8 = jmin >> 3;
    jmin_8 -= (jmin & 0x0007) ? 1 : 0;
    jmin_80 = jmin_8 = (jmin_8 < 0) ? 0 : jmin_8;
    jmin0 = jmin_80 << 3;
    jmax_8 = (jmax & 0x0007) ? 1 + (jmax >> 3) : (jmax >> 3);
    //jmax_80 = jmax_8 = (jmax_8 > onx_8) ? onx_8 : jmax_8;


    
    /*win_printf("Imin %d imax %d, jmin %d jmax %d\njmin_4 %d jmax_4 %d"
      ,imin,imax,jmin,jmax,jmin_4,jmax_4); */

    //f8xc.f[7] = xc;
    f8xc.v =_mm256_broadcast_ss(&xc);
    //f8yc.f[7] = yc;
    f8yc.v =_mm256_broadcast_ss(&yc);
    //f8ax.f[7] = ax;
    f8ax.v =_mm256_broadcast_ss(&ax);
    //f8ay.f[7] = ay;
    f8ay.v =_mm256_broadcast_ss(&ay);    
    //f8.f[7] = 8;
    tmp = 8;
    f8.v =_mm256_broadcast_ss(&tmp);
    r = f8r[0].f;
    
    j = jmin0;
    f80.f[0] = j;    f80.f[1] = j+1;    f80.f[2] = j+2;    f80.f[3] = j+3;  // pixel index
    f80.f[4] = j+4;  f80.f[5] = j+5;    f80.f[6] = j+6;    f80.f[7] = j+7;  // pixel index 

    f8x.v = _mm256_sub_ps(f80.v, f8xc.v);            // distance in pixel from center
    f8x.v = _mm256_mul_ps(f8ax.v, f8x.v);            // rescaling in x
    f8.v = _mm256_mul_ps(f8ax.v, f8.v);            // we prepare rescaled substraction

    j_nxmax = ((jmax_8 - jmin_80) < (int)sizeof(f8x2)) ? (jmax_8 - jmin_80) : (int)sizeof(f8x2); 
    for (j = 0; j < j_nxmax; j++) //for (j = 0; j < onx_8; j++)
      {
	f8x2[j].v = _mm256_mul_ps(f8x.v, f8x.v);     // we compute X2
	f8x.v = _mm256_add_ps(f8x.v, f8.v);         // we move index
      }

    //f80.f[7] = imin;        // starting y position
    tmp = imin;
    f80.v =_mm256_broadcast_ss(&tmp);
    f8y.v = _mm256_sub_ps(f80.v, f8yc.v);            // distance in pixel to yc
    f8y.v = _mm256_mul_ps(f8ay.v, f8y.v);            // rescaled in y


    for (i = imin; i < imax; i++)
      {
	f8y2.v = _mm256_mul_ps(f8y.v, f8y.v);        // we compute y2
	f8y.v = _mm256_add_ps(f8y.v, f8ay.v);        // move by one rescaled pixel in y
	tmp = rmax2 - f8y2.f[0];

	if (tmp < 0) continue;

	tmp = sqrt(tmp);
	tmp = (tmp + 1) / ax;
	jmin = (int)(xc - tmp);
	jmax = (int)(xc + tmp + 1);
	jmin = (jmin < 0) ? 0 : jmin;
	jmax = (jmax > onx) ? onx : jmax;
	jmin_8 = jmin >> 3;
	jmin_8 -= (jmin & 0x0007) ? 1 : 0;
	jmin_8 = (jmin_8 < 0) ? 0 : jmin_8;
	jmax_8 = (jmax & 0x0007) ? 1 + (jmax >> 3) : (jmax >> 3);
	jmax_8 = (jmax_8 > onx_8) ? onx_8 : jmax_8;

	for (j = jmin_8; j < jmax_8; j++)
	  {
	    if (j < 0) continue;
	    f8r[j-jmin_80].v = _mm256_add_ps(f8y2.v, f8x2[j- jmin_80].v);  // we compute x2 + y2
	    
	    f8r[j-jmin_80].v = _mm256_sqrt_ps(f8r[j-jmin_80].v);         // we get the distance
	    //f8ri[j-jmin_80].v = _mm256_floor_ps(f8r[j-jmin_80].v);
	    //f8p[j-jmin_80].v = _mm256_sub_ps(f8r[j-jmin_80].v, f8ri[j-jmin_80].v);
	  }
	for (j = jmin; j < jmax; j++)
	  {
	    ri = (int)r[j-jmin0];
	    /*
            y = ay * ((float)i - yc);
            y *= y;
	    x = ax * ((float)j - xc);
	    x *= x;
	    if ((fabs((float)sqrt(x + y)-r[j-jmin0]) > 0.01) && (win_res != WIN_CANCEL))
	      win_res = win_printf("point at i = %d and j = %d\n"
				   "differs normal = %g and r[%d] = %g f8r[%d][%d] = %g\n"
				   "normal x2 = %g y2 = %g\n"
				   "avx2   x2[%d][%d] = %g y2[%d] = %g\n"
				   "jmin0 %d jmin %d, jmax %d,\n"
				   "jmin_8 %d jmax_8 %d jmin_80 %d jmax_80 %d\n"
				   ,i,j,sqrt(x + y),j-jmin0,r[j-jmin0],(j/8)-jmin_80,j%8, f8r[(j/8)-jmin_80].f[j%8]
				   ,x,y,(j/8)-jmin_80,j%8, f8x2[(j/8)-jmin_80].f[j%8],j%8,f8y2.f[j%8]
				   ,jmin0,jmin,jmax,jmin_8,jmax_8,jmin_80,jmax_80);
	    */
	    
	    if (ri > r_max) continue;   /* bug break;*/
#ifdef SKIP_IM
	    if (ri < r_max)
	      rz[ri] += r[j-jmin0];
#else
	    if (type == IS_INT_IMAGE)	      z = (float)ps[i].in[j];
	    else if (type == IS_CHAR_IMAGE)   z = (float)ps[i].ch[j];
	    else if (type == IS_UINT_IMAGE)   z = (float)ps[i].ui[j];
	    else if (type == IS_FLOAT_IMAGE)   z = (float)ps[i].fl[j];	    
	    p = r[j-jmin0] - ri;

	    if (ri == r_max)
	      {
		p0 += (1 - p) * z;
		pr0 += (1 - p);
		npr0++;
		//radial_dx[r_max] -= 1-p;
	      }
	    else
	      {
		rz[ri] += (1 - p) * z;
		rz[ri + r_max] += (1 - p);
		//radial_dx[ri] += p;
	      }
	    ri++;
	    if (ri < r_max)
	      {
		rz[ri] += p * z;
		rz[ri + r_max] += p;
		//radial_dx[ri] -= 1-p;
	      }
# endif	    
	  }
      }
    for (i = 0; i < r_max; i++)
      {
        rz[i] = (rz[i + r_max] == 0) ? 0 : rz[i] / rz[i + r_max];
        //radial_dx[i] = (rz[i+r_max] == 0) ? 0 : radial_dx[i]/rz[i+r_max];
      }
    p0 = (pr0 == 0) ? 0 : p0 / pr0;

    for (i = 0; i < r_max; i++)     rz[i + r_max] = rz[i];

    for (i = 1; i < r_max; i++)     rz[r_max - i] = rz[r_max + i];

    if (rz[r_max] == 0) rz[r_max] = rz[r_max + 1];

    rz[0] = p0;
    return rz;
}

int do_radial(void)
{
  register int i;
  imreg *imr;
  O_i *ois;
  O_p *op;
  d_s *ds;//, *ds2;		
  static float xc = 256 , yc = 256, ax = 1, ay = 1;
  static int r_max = 64;
  unsigned long u_micro;

  
  if(updating_menu_state != 0)	return D_O_K;
  
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;

  i = win_scanf("center xc %f yc %f r_{max} %dax %f ay %f",&xc,&yc,&r_max,&ax,&ay);
  if (i == WIN_CANCEL)	return OFF;


  
  op = create_and_attach_op_to_oi(ois, 2*(r_max), 2*(r_max), 0, 0);
  ds = op->dat[0];

  u_micro = my_uclock();
  radial_non_pixel_square_image_sym_profile_in_array(ds->yd, ois, xc, yc, r_max, ax, ay);
  //radial_non_pixel_square_image_sym_profile_in_array_sse(ds->yd, ois, xc, yc, r_max, ax, ay);


  u_micro = my_uclock() - u_micro;
  win_printf("radial profil dt = %g \\mu s", 
	     1000000*((double)u_micro/((int)MY_UCLOCKS_PER_SEC)));

  
  /*  
  ds2 = create_and_attach_one_ds(op, 2*r_max, 2*r_max, 0);
  for (i = 0; i < r_max; i++)		
    ds2->yd[r_max-i] = ds2->yd[r_max+i] = radial_dx[i];
  */
  set_plot_title(op,"do radial");
  //ds->source = Mystrdupre(ds->source,op->title);	
  //for (i = 0; i < 2*r_max; i++) ds2->xd[i] = ds->xd[i] = (i-r_max)/ay;
  for (i = 0; i < 2*r_max; i++) ds->xd[i] = (i-r_max)/ay;
  broadcast_dialog_message(MSG_DRAW,0);
  return refresh_im_plot(imr, ois->n_op - 1);	
}

int do_radial_avx(void)
{
  register int i;
  imreg *imr;
  O_i *ois;
  O_p *op;
  d_s *ds;//, *ds2;		
  static float xc = 256 , yc = 256, ax = 1, ay = 1;
  static int r_max = 64;
  unsigned long u_micro;

  
  if(updating_menu_state != 0)	return D_O_K;
  
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;

  i = win_scanf("center xc %f yc %f r_{max} %dax %f ay %f",&xc,&yc,&r_max,&ax,&ay);
  if (i == WIN_CANCEL)	return OFF;


  
  op = create_and_attach_op_to_oi(ois, 2*r_max, 2*r_max, 0, 0);
  ds = op->dat[0];

  u_micro = my_uclock();
  radial_non_pixel_square_image_sym_profile_in_array_avx2(ds->yd, ois, xc, yc, r_max, ax, ay);

  u_micro = my_uclock() - u_micro;
  win_printf("radial profil avx dt = %g \\mu s", 
	     1000000*((double)u_micro/((int)MY_UCLOCKS_PER_SEC)));
  
  /*
  ds2 = create_and_attach_one_ds(op, 2*r_max, 2*r_max, 0);
  for (i = 0; i < r_max; i++)		
    ds2->yd[r_max-i] = ds2->yd[r_max+i] = radial_dx[i];
  */
  set_plot_title(op,"do radial");
  //ds->source = Mystrdupre(ds->source,op->title);	
  //for (i = 0; i < 2*r_max; i++) ds2->xd[i] = ds->xd[i] = (i-r_max)/ay;
  for (i = 0; i < 2*r_max; i++) ds->xd[i] = (i-r_max)/ay;  
  broadcast_dialog_message(MSG_DRAW,0);
  return refresh_im_plot(imr, ois->n_op - 1);	
}



MENU *profil_radial_image_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"Radial profile", do_radial, NULL, 0, NULL);
	add_item_to_menu(mn,"Radial profile avx", do_radial_avx, NULL, 0, NULL);	
	
	return mn;
}

int	profil_radial_main(int argc, char **argv)
{
  (void)argc;
  (void)argv;
	add_image_treat_menu_item ( "profil_radial", NULL, profil_radial_image_menu(), 0, NULL);
	return D_O_K;
}

int	profil_radial_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;
	remove_item_to_menu(image_treat_menu, "profil_radial", NULL, NULL);
	return D_O_K;
}
#endif

