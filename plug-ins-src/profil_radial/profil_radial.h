#ifndef _PROFIL_RADIAL_H_
#define _PROFIL_RADIAL_H_

PXV_FUNC(int, do_profil_radial_average_along_y, (void));
PXV_FUNC(MENU*, profil_radial_image_menu, (void));
PXV_FUNC(int, profil_radial_main, (int argc, char **argv));
#endif

