#ifndef _BHP_H_
#define _BHP_H_

#define	BHP_SKEWNESS 0x010000
#define	BHP_KURTOSIS 0x100000
#define BHP_SK_JOINT 0x001000

PXV_FUNC(MENU*, BHP_plot_menu, 	(void));
PXV_FUNC(int, BHP_main, 		(int argc, char **argv));
PXV_FUNC(int, BHP_unload,	(int argc, char **argv));

PXV_FUNC(int, do_BHP_create_a, (void));
PXV_FUNC(int, do_BHP_moment,   (void));

#endif

