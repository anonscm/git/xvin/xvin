
struct BHP_params
{  double S, sign, K;
};
     
double BHP_skewness       (double x, void *params);
double BHP_skewness_deriv (double x, void *params);
void   BHP_skewness_fdf   (double x, void *params, double *y, double *dy);
                         
double BHP_kurtosis       (double x, void *params);
double BHP_kurtosis_deriv (double x, void *params);
void   BHP_kurtosis_fdf   (double x, void *params, double *y, double *dy);

double BHP_SKjoint        (double x, void *params);
double BHP_SKjoint_deriv  (double x, void *params);
void   BHP_SKjoint_fdf    (double x, void *params, double *y, double *dy);

int BHP_find_zero(gsl_function_fdf FDF, double *result, double *error);

