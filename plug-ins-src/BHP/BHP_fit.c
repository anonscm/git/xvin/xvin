#include <math.h>

#include <gsl/gsl_math.h>       // for integer powers
#include <gsl/gsl_sf_psi.h>     // for di-gamma et al
#include <gsl/gsl_roots.h>
#include <gsl/gsl_errno.h>

#include "BHP_fit.h"

double BHP_skewness(double x, void *params)
{      double psi_1, psi_2;
       struct BHP_params *p = (struct BHP_params*)params;
       double S    = p->S;
       double sign = (double)p->sign;
       
       // trigamma function, derivate of digamma function
       psi_1 = gsl_sf_psi_1(x);
       // its derivate is calculated using the general polygamma function from gsl:
       psi_2 = gsl_sf_psi_n (2, x);
       
       return( sign*psi_2 / (psi_1 * sqrt(psi_1)) - S); 
}

double BHP_skewness_deriv (double x, void *params)
{      double psi_1, psi_2, psi_3;
       struct BHP_params *p = (struct BHP_params *) params;
       double sign = (double)p->sign;
     
       // trigamma function, derivate of digamma function
       psi_1 = gsl_sf_psi_1(x);
       // its derivate is calculated using the general polygamma function from gsl:
       psi_2 = gsl_sf_psi_n (2, x);
       psi_3 = gsl_sf_psi_n (3, x);
       
       return( sign*( psi_3*psi_1 - (double)(3.0/2.0)*psi_2*psi_2 ) / (psi_1*psi_1*sqrt(psi_1)));
}
     
void BHP_skewness_fdf (double x, void *params, double *y, double *dy)
{ //     struct BHP_params *p = (struct BHP_params *) params;
     
       *y  = BHP_skewness      (x, params);
       *dy = BHP_skewness_deriv(x, params);
}





double BHP_kurtosis(double x, void *params)
{      double psi_1, psi_3;
       struct BHP_params *p = (struct BHP_params *) params;
       double K = p->K;

       // trigamma function, derivate of digamma function
       psi_1 = gsl_sf_psi_1(x);
       psi_3 = gsl_sf_psi_n (3, x);
       
       return( psi_3/(psi_1*psi_1) - K); 
}

double BHP_kurtosis_deriv (double x, void *params)
{      double psi_1, psi_2, psi_3, psi_4;
//       struct BHP_params *p = (struct BHP_params *) params;

       psi_1 = gsl_sf_psi_1(x);
       psi_2 = gsl_sf_psi_n (2, x);
       psi_3 = gsl_sf_psi_n (3, x);
       psi_4 = gsl_sf_psi_n (4, x);
       
       return( (psi_4*psi_1 - (double)2.0*psi_3*psi_2) / gsl_pow_3(psi_1) );
}

void BHP_kurtosis_fdf (double x, void *params, double *y, double *dy)
{   //   struct BHP_params *p = (struct BHP_params *) params;
     
       *y  = BHP_kurtosis      (x, params);
       *dy = BHP_kurtosis_deriv(x, params);
}





double BHP_SKjoint(double x, void *params)
{      
       return( gsl_pow_2(BHP_skewness(x, params)) + gsl_pow_2(BHP_kurtosis(x, params)) ); 
}

double BHP_SKjoint_deriv (double x, void *params)
{            
       return( (double)2.0*( BHP_skewness(x, params)*BHP_skewness_deriv(x, params) 
                           + BHP_kurtosis(x, params)*BHP_kurtosis_deriv(x, params) ));
}

void BHP_SKjoint_fdf (double x, void *params, double *y, double *dy)
{   //   struct BHP_params *p = (struct BHP_params *) params;
     
       *y  = BHP_SKjoint      (x, params);
       *dy = BHP_SKjoint_deriv(x, params);
}





int BHP_find_zero(gsl_function_fdf FDF, double *result, double *error)
{   int status;
    int iter = 0, max_iter = 100;
    const gsl_root_fdfsolver_type *T;
    gsl_root_fdfsolver *s;
    double x0, x = 5.0;
    
    T = gsl_root_fdfsolver_newton;
    s = gsl_root_fdfsolver_alloc (T);
    gsl_root_fdfsolver_set (s, &FDF, x);
     
//    printf ("using %s method\n", gsl_root_fdfsolver_name (s));
//    printf ("%-5s %10s %10s %10s\n", "iter", "root", "err", "err(est)");
    do
    {      iter++;
           status = gsl_root_fdfsolver_iterate (s);
           x0 = x;
           x = gsl_root_fdfsolver_root (s);
           status = gsl_root_test_delta (x, x0, 0, 1e-3);
     
//           if (status == GSL_SUCCESS) printf ("Converged:\n");
//           printf ("%5d %10.7f %+10.7f %10.7f\n", iter, x, x - r_expected, x - x0);
    }
    while ( (status==GSL_CONTINUE) && (iter<max_iter) );
  
    *result = x;
    *error  = x-x0;
     
    gsl_root_fdfsolver_free (s);
    return(status); 
}
     
