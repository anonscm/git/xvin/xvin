#ifndef _BHP_C_
#define _BHP_C_

#include "xvin.h"
#include "fft_filter_lib.h"

#include <gsl/gsl_sf_psi.h>
#include <gsl/gsl_roots.h>
#include <gsl/gsl_errno.h>

/* If you include other plug-ins header do it here*/ 

/* But not below this define */
#define BUILDING_PLUGINS_DLL
#include "BHP.h"
#include "BHP_fit.h"




//(2007/12/05)
int do_BHP_create_a(void)
{	O_p    *op = NULL;
	d_s    *ds = NULL;
	pltreg *pr = NULL;
//	double precision=1.0e-5;
	int		index;
	int     j;
static int ny=512;
static float a_min=M_PI/2., a_max=29.;
	
	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])	
		return win_printf_OK("This routine plots the skewness of a BHP as a function of a.\n\n"
							"a new dataset is created.");
	
	if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)	return win_printf_OK("cannot find plot region");

    j=win_scanf("{\\pt14\\color{Yellow}Create a dataset of a}\n\n"
            "dataset with %5d points\n"
            "from %8f  to  %8f", &ny, &a_min, &a_max);
    if (j==CANCEL) return(D_O_K);

    if ((op = create_and_attach_one_plot(pr, ny, ny, 0)) == NULL)
			           return(win_printf_OK("cannot create plot !"));
    set_plot_x_title(op, "a");
	op->filename = Transfer_filename(pr->one_p->filename);
	op->dir = Mystrdup(pr->one_p->dir);

    ds=op->dat[0];
	for (j=0; j<ny; j++)
	{    ds->xd[j] = a_min + (a_max-a_min)/(ny-1)*j;
	     ds->yd[j] = 1.;
    }

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the do_BHP_create_a







//(2007/12/05)
int do_BHP_moment(void)
{	O_p 	*op1 = NULL;
	d_s 	*ds1, *ds_full=NULL, *ds_esti=NULL;
	pltreg *pr = NULL;
//	double precision=1.0e-5;
	int		index;
static int bool_sign=0, bool_full=1, bool_estimate=0;
	int     ny, j, sign=+1;
	double  x, psi_1, psi_2, psi_3;
	
	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])	
		return win_printf_OK("This routine plots the skewness of a BHP as a function of a.\n\n"
							"a new dataset is created.");
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)	return win_printf_OK("cannot plot region");
	ny = ds1->ny;	// number of points in time series 


    j=win_scanf("{\\pt14\\color{Yellow}Moment of order N of a BHP vs a}\n\n"
            "sign of the BHP : %R >0 or %r <0\n"
            "%b plot real function (polygammas)\n"
            "%b plot asymptotic estimate", &bool_sign, &bool_full, &bool_estimate);
    if (j==CANCEL) return(D_O_K);
    if (bool_sign==0) sign=+1; else sign=-1;

    if (bool_full==1)     
    {  ds_full = create_and_attach_one_ds (op1, ny, ny, 0);
       inherit_from_ds_to_ds(ds_full, ds1);
	   ds_full->treatement = my_sprintf(ds_full->treatement,"exact %s of BHP vs a",
                           (index==BHP_SKEWNESS) ? "skewness" : "kurtosis");
    }
    if (bool_estimate==1) 
    {  ds_esti = create_and_attach_one_ds (op1, ny, ny, 0);
	   inherit_from_ds_to_ds(ds_esti, ds1);
	   ds_esti->treatement = my_sprintf(ds_esti->treatement,"asympt. dev. of %s of BHP vs a",
                                  (index==BHP_SKEWNESS) ? "skewness" : "kurtosis");
    }
    for (j=0; j<ny; j++)
	{    x = (double)ds1->xd[j];
          
         if (bool_estimate==1)
         { ds_esti->xd[j] = ds1->xd[j];
           if (index==BHP_SKEWNESS) ds_esti->yd[j] = -(float)sign/(float)sqrt(x);
           if (index==BHP_KURTOSIS) ds_esti->yd[j] = (float)2./(float)x;
         }
         
         if (bool_full==1)
         { // trigamma function, derivate of digamma function
           psi_1 = gsl_sf_psi_1(x);
           // its derivate is calculated using the general polygamma function from gsl:
           psi_2 = gsl_sf_psi_n (2, x);
           // and so on:
           psi_3 = gsl_sf_psi_n (3, x);
           
           if (index==BHP_SKEWNESS)
           {  // skewness from these de Baptiste Portelli, p178 :
             ds_full->yd[j] = (float)((double)sign * psi_2 / (psi_1 * sqrt(psi_1)) ); 
           }
           if (index==BHP_KURTOSIS)
           {  // skewness from these de Baptiste Portelli, p178 :
             ds_full->yd[j] = (float)((double)psi_3 / (psi_1*psi_1) ); 
           }
           
           ds_full->xd[j] = ds1->xd[j];
         }
    }

	
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the do_BHP_moment





//(2007/12/10)
int do_BHP_find_zero(void)
{	O_p 	*op1 = NULL;
	d_s 	*ds1, *ds2=NULL;
	pltreg *pr = NULL;
//	double precision=1.0e-5;
	int		index;
static int bool_sign=0;
	int     ny, j, sign=+1;
	double  x;
	gsl_function_fdf FDF; // what to fit ?
	struct BHP_params params = {1.0, 0.0, -5.0};
	double result, error;
       
	
	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])	
		return win_printf_OK("This routine finds the value of a such that the skewness or kurtosis"
		                    "of the BHP distribution with a is the one given in the active dataset.\n" 
                            "(it is an inversion, with Newton's method).\n\n"
							"a new dataset is created.");
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)	return win_printf_OK("cannot plot region");
	ny = ds1->ny;	// number of points in time series 

    j=win_scanf("{\\pt14\\color{Yellow}Invert S(a) or K(a) for a BHP and find a}\n\n"
            "sign of the BHP : %R >0 or %r <0\n", &bool_sign);
    if (j==CANCEL) return(D_O_K);
    if (bool_sign==0) sign=+1; else sign=-1;
    params.sign = sign;

    ds2 = create_and_attach_one_ds (op1, ny, ny, 0);
    inherit_from_ds_to_ds(ds2, ds1);
	ds2->treatement = my_sprintf(ds2->treatement,"inversion of %s of BHP to find a",
                           (index==BHP_SKEWNESS) ? "skewness" : "kurtosis");
    
    for (j=0; j<ny; j++)
	{    x = (double)ds1->xd[j];
         ds2->xd[j] = ds1->xd[j];
         
       if (index==BHP_SKEWNESS)
       {  params.S  = (double)ds1->yd[j]; // value of the skewness to fit
          FDF.f     = &BHP_skewness;
          FDF.df    = &BHP_skewness_deriv;
          FDF.fdf   = &BHP_skewness_fdf;
          FDF.params = &params;
       }
       if (index==BHP_KURTOSIS)
       {  params.K  = (double)ds1->yd[j]; // value of the skewness to fit
          FDF.f     = &BHP_skewness;
          FDF.df    = &BHP_skewness_deriv;
          FDF.fdf   = &BHP_skewness_fdf;
          FDF.params = &params;
       }
       if (index==BHP_SK_JOINT)
       {  params.S  = (double)ds1->yd[j]; // value of the skewness to fit
          FDF.f     = &BHP_skewness;
          FDF.df    = &BHP_skewness_deriv;
          FDF.fdf   = &BHP_skewness_fdf;
          FDF.params = &params;
       }
       if (BHP_find_zero(FDF, &result, &error)==GSL_CONTINUE) win_printf("not converged...");
         
       ds2->yd[j] = (float)result;  
       
       // add error bars:
    }

	
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the do_BHP_find_zero


MENU *BHP_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;

	add_item_to_menu(mn,"create a dataset of a",  do_BHP_create_a,	NULL, 0, NULL);	
	add_item_to_menu(mn,"plot skewness vs a",	  do_BHP_moment,	NULL, BHP_SKEWNESS, NULL);	
	add_item_to_menu(mn,"plot kurtosis vs a",	  do_BHP_moment,	NULL, BHP_KURTOSIS, NULL);	
	add_item_to_menu(mn,"\0",					  0,				NULL, 0, NULL);	
	add_item_to_menu(mn,"fit skewness in a",	  do_BHP_find_zero, NULL, BHP_SKEWNESS, NULL);	
	add_item_to_menu(mn,"fit kurtosis in a",	  do_BHP_find_zero, NULL, BHP_KURTOSIS, NULL);	
	add_item_to_menu(mn,"fit both S and K in a",  do_BHP_find_zero, NULL, BHP_SK_JOINT, NULL);	
	
	return mn;
}




int	BHP_main(int argc, char **argv)
{
	add_plot_treat_menu_item ("BHP", NULL, BHP_plot_menu(), 0, NULL);
	return D_O_K;
}


int	BHP_unload(int argc, char **argv)
{ 
	remove_item_to_menu(plot_treat_menu,"BHP",	NULL, NULL);
	return D_O_K;
}
#endif
