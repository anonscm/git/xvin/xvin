/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _THORLABS_APT_C_
#define _THORLABS_APT_C_

#include "allegro.h"
#include "winalleg.h"
#include "xvin.h"
#include <pthread.h>

/* If you include other regular header do it here*/


/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "thorlabs_APT.h"
//place here other headers of this plugin
# include "allegro.h"
# include "xvin.h"

/* If you include other regular header do it here*/


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "APTAPI.h"
 # include "thorlabs_APT.h"


char warning_in_motor_thread[256] = {0};

pthread_t thread_apt = 0;



 float small_step = 0.05;

float zmag_pos = 0, zmag_target = 0, prev_zmag_target = 0; 
float zmag_speed_target = 2.5,  prev_zmag_speed_target = 2.5;

FILE *apt_zmag_fp = NULL;

char zamagfilename[1024] = {0};

//# define FLOW
// or
# define MOTOR_ONLY


void stop_apt(void)
{
  APTCleanUp();
}
# ifdef FLOW
long plNumUnits = -1, plSerialNum[16];
float pfPosition[16];
float pf_offset[16];
float pvel[16] = {0.1,0.1,0.1,0.1,0.1,0.1,0.1};

// define the seringhe full volume in ml
float ser_vol[16] = {20,1,1,1,1,1,1};
// define the seringhe full range mm per full vol
float ser_range[16] = {65,60,60,60,60,60,60};
// define the seringhe motor range
float ser_mot_range[16] = {50,50,50,50,50,50,50,50};
char mot_name[16][64];


int do_flow_control_start(void)
{
  register int i, j;
  int ret, er = 0, prog_restart = 0;
  char message[2048];
  char szModel[256], szSWVer[256], szHWNotes[256], cfg_nb[128], *nm;
  float pfMinVel, pfAccn, pfMaxVel;
  float pfMinPos, pfMaxPos, pfPitch, vel, pos, vol;
  long plUnits;

  if(updating_menu_state != 0)
    {
      if (plNumUnits > 0 ) active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }


  
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine test Thorlabs APT lib");
    }
  //  i = win_scanf("Do you want RS232 error window on %b",&dlg);
  //if (i == CANCEL) return D_O_K;
  i = APTInit();
  APTCleanUp();
  i = APTInit();
  if (i != 0)
    {
      APTCleanUp();
      i = APTInit();
    }
  if (i != 0)
    {
      win_printf("APTinit failed !");
      APTCleanUp();
      return 0;
    }
  //if (dlg == 0)
  EnableEventDlg(FALSE);

  ret = GetNumHWUnitsEx(HWTYPE_TDC001, &plNumUnits);
  //win_printf("GetNumHWUnitsEx Ret %d num -> %d",ret, (int)plNumUnits);
  if (plNumUnits < 1)
    {
      win_printf("No device connected! You may have to reinitiate your Cube\n"
		 "by unpluging and repluging DC power supply\n");
      APTCleanUp();
      return 0;
    }
  atexit(stop_apt);
  snprintf(message,sizeof(message),"You have %d TDC001 cube connected\n",(int)plNumUnits);
  for (i = 0; i < plNumUnits && i < 16; i++)
    {
      er = 0;
      ret = GetHWSerialNumEx(HWTYPE_TDC001, i, plSerialNum + i);
      //win_printf("GetHWSerialNumEx Ret %d num %d -> %d",ret, i,(int)plSerialNum[i]);
      er += InitHWDevice(plSerialNum[i]);
      er += MOT_GetPosition(plSerialNum[i], pfPosition + i);
      if (er == 0) prog_restart += (fabs(pfPosition[i]) < 0.005) ? 0 : 1;
      er += GetHWInfo(plSerialNum[i], szModel, 256, szSWVer, 256, szHWNotes, 256);
      for (j = 0; j < sizeof(szHWNotes) && szHWNotes[j] != 0; j++)
	if (szHWNotes[j] < 32) szHWNotes[j] = 0;
      snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_name",(int)plSerialNum[i]);
      nm = (char*)get_config_string("FLOW_CONTROL",cfg_nb,NULL);

      //win_printf("Mot num %d -> %g",plSerialNum[i],pfPosition[i]);
      if (er == 0)
	snprintf(message+strlen(message),sizeof(message)-strlen(message),
		 "Cube %d with serial # %d (%s) at %g model %s ver.%s\n"
		 ,i,(int)plSerialNum[i],(nm)?nm:"",pfPosition[i],szModel, szSWVer);
      else
	snprintf(message+strlen(message),sizeof(message)-strlen(message),
		 "Com error on Cube %d with serial # %d '%s) at %g model %s ver. %s\n"
		 ,i,(int)plSerialNum[i],(nm)?nm:"",pfPosition[i],szModel, szSWVer);
      //GetHWInfo(long lSerialNum, TCHAR *szModel, long lModelLen, TCHAR *szSWVer, long lSWVerLen, TCHAR *szHWNotes, long lHWNotesLen);
      if (MOT_GetStageAxisInfo(plSerialNum[i], &pfMinPos, &pfMaxPos, &plUnits, &pfPitch))
	win_printf_OK("Error reading axis info\non Cube with serial # %d\n",(int)plSerialNum[i]);
      pfMaxPos = 100;
      pfMinPos = -100;
      if (MOT_SetStageAxisInfo(plSerialNum[i], pfMinPos, pfMaxPos, plUnits, pfPitch))
	win_printf_OK("Error setting axis info\non Cube with serial # %d\n",(int)plSerialNum[i]);
      if (MOT_SetBLashDist(plSerialNum[i], 0.0))
	win_printf_OK("Error setting  blash distance\non Cube with serial # %d\n",(int)plSerialNum[i]);

      snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_max_vel",(int)plSerialNum[i]);
      vel = get_config_float("FLOW_CONTROL",cfg_nb,0.1);
      snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_position",(int)plSerialNum[i]);
      pos = get_config_float("FLOW_CONTROL",cfg_nb,0);
      pf_offset[i] = pos - pfPosition[i];
      snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_volume",(int)plSerialNum[i]);
      vol = get_config_float("FLOW_CONTROL",cfg_nb,ser_vol[i]);
      ser_vol[i] = vol;
      snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_volume_range",(int)plSerialNum[i]);
      vol = get_config_float("FLOW_CONTROL",cfg_nb,ser_range[i]);
      ser_range[i] = vol;
      if (MOT_GetVelParams(plSerialNum[i], &pfMinVel, &pfAccn, &pfMaxVel))
	win_printf_OK("Error reading axis velocity\non Cube with serial # %d\n",(int)plSerialNum[i]);
      pvel[i] = vel;
      if (MOT_SetVelParams(plSerialNum[i], pfMinVel, pfAccn, vel))
	win_printf_OK("Error setting axis velocity\non Cube with serial # %d\n",(int)plSerialNum[i]);
      //      snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_name",(int)plSerialNum[i]);
      //vol = get_config_string("FLOW_CONTROL",cfg_nb,NULL);
    }
  flush_config_file(); // write_Pico_config_file();
  win_printf(message);
  do_update_menu();
  return 0;
}

int do_flow_control_stop(void)
{
  if(updating_menu_state != 0)	return D_O_K;
  APTCleanUp();
  return 0;
}

int do_flow_control_read_MOT_pos(void)
{
  register int i, j;
  char message[2048], cfg_nb[128], *nm;

  if(updating_menu_state != 0)	return D_O_K;
  if (plNumUnits < 0) win_printf("APT not initiated");
  message[0] = 0;
  for (i = 0; i < plNumUnits && i < 16; i++)
    {
      j = MOT_GetPosition(plSerialNum[i], pfPosition + i);
      snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_name",(int)plSerialNum[i]);
      nm = (char*)get_config_string("FLOW_CONTROL",cfg_nb,NULL);
      if (j == 0)
	snprintf(message+strlen(message),sizeof(message)-strlen(message),
		 "Cube %d with serial # %d (%s)at %g\n"
		 ,i,(int)plSerialNum[i],(nm)?nm:"",pfPosition[i]);
      else snprintf(message+strlen(message),sizeof(message)-strlen(message),
		 "error on Cube %d with serial # %d (%s)at %g\n"
		    ,i,(int)plSerialNum[i],(nm)?nm:"",pfPosition[i]);
    }
  win_printf(message);
  return 0;
}

int do_flow_control_set_MOT_pos(void)
{
  register int i, j, k;
  char message[2048], status[16], cfg_nb[128], *nm;
  float newPosition[16];
  long plStatusBits[16];

  if(updating_menu_state != 0)	return D_O_K;
  if (plNumUnits < 0) return win_printf_OK("APT not initiated");
  status[0] = message[0] = 0;
  for (i = 0; i < plNumUnits && i < 16; i++)
    {
      j = MOT_GetPosition(plSerialNum[i], newPosition + i);
      MOT_GetStatusBits(plSerialNum[i], plStatusBits + i);
      snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_name",(int)plSerialNum[i]);
      nm = (char*)get_config_string("FLOW_CONTROL",cfg_nb,NULL);
      k = (int)plStatusBits[i]&0x07;
      if (k == 1) sprintf(status,"Rev. limit On!");
      else if (k == 2) sprintf(status,"Fwd. limit On!");
      else if (k == 3) sprintf(status," Rev. soft limit On!");
      else if (k == 4) sprintf(status," Fwd. soft limit On!");
      else if (k == 5) sprintf(status," Mot. moving Rev.!");
      else if (k == 6) sprintf(status," Mot. moving Fwd.!");
      else if (k == 7) sprintf(status," Shaft jogging Rev.!");
      else if (k == 8) sprintf(status," Shaft jogging Fwd.!");
      if (j == 0)
	{
	  pfPosition[i] = newPosition[i];
	  snprintf(message+strlen(message),sizeof(message)-strlen(message),
		   "Cube %d with serial # %d (%s)\nmove to %%12f %s\n"
		   ,i,(int)plSerialNum[i],(nm)?nm:"",status);
	}
      else
	{
	  snprintf(message+strlen(message),sizeof(message)-strlen(message),
		   "Error on Cube %d with serial # %d (%s)\nmove to %%12f %s\n"
		   ,i,(int)plSerialNum[i],(nm)?nm:"",status);
	}
    }
  if (plNumUnits == 0) return 0;
  if (plNumUnits == 1) i = win_scanf(message,newPosition);
  else if (plNumUnits == 2) i = win_scanf(message,newPosition,newPosition+1);
  else if (plNumUnits == 3) i = win_scanf(message,newPosition,newPosition+1,newPosition+2);
  else if (plNumUnits == 4) i = win_scanf(message,newPosition,newPosition+1,newPosition+2,newPosition+3);
  else i = win_scanf(message,newPosition,newPosition+1,newPosition+2,newPosition+3,newPosition+4);
  if (i == WIN_CANCEL) return D_O_K;
  for (i = 0; i < plNumUnits && i < 16; i++)
    {
      if (pfPosition[i] != newPosition[i])
	{
	  j = MOT_MoveAbsoluteEx(plSerialNum[i], newPosition[i], 0);
	  if (j) win_printf_OK("Error setting position \non Cube %d with serial # %d\n",i,(int)plSerialNum[i]);
	}
    }
  return 0;
}



int do_flow_control_sent_MOT_vol(void)
{
  register int i, j, k;
  char message[2048], status[16], cfg_nb[128], *nm;
  float newPosition[16], duration[16];
  long plStatusBits[16];
  float pfMinVel, pfAccn, pfMaxVel;
  float tmp, vel, vol, vol_range;

  if(updating_menu_state != 0)	return D_O_K;
  if (plNumUnits < 0) return win_printf_OK("APT not initiated");
  status[0] = message[0] = 0;
  for (i = 0; i < plNumUnits && i < 16; i++)
    {
      j = MOT_GetPosition(plSerialNum[i], pfPosition + i);
      newPosition[i] = 0;
      duration[i] = 1;
      MOT_GetStatusBits(plSerialNum[i], plStatusBits + i);
      snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_name",(int)plSerialNum[i]);
      nm = (char*)get_config_string("FLOW_CONTROL",cfg_nb,NULL);
      k = (int)plStatusBits[i]&0x07;
      if (k == 1) sprintf(status,"Rev. limit On!");
      else if (k == 2) sprintf(status,"Fwd. limit On!");
      else if (k == 3) sprintf(status," Rev. soft limit On!");
      else if (k == 4) sprintf(status," Fwd. soft limit On!");
      else if (k == 5) sprintf(status," Mot. moving Rev.!");
      else if (k == 6) sprintf(status," Mot. moving Fwd.!");
      else if (k == 7) sprintf(status," Shaft jogging Rev.!");
      else if (k == 8) sprintf(status," Shaft jogging Fwd.!");
      if (j == 0)
	{
	  //pfPosition[i] = newPosition[i];
	  snprintf(message+strlen(message),sizeof(message)-strlen(message),
		   "Cube %d with serial # %d %s\nsent %%12f \\mu l \nin %%12f S %s\n"
		   ,i,(int)plSerialNum[i],(nm)?nm:"",status);
	}
      else
	{
	  snprintf(message+strlen(message),sizeof(message)-strlen(message),
		   "Error on Cube %d with serial # %d %s\nsent %%12f \\mu l \nin %%12f S %s\n"
		   ,i,(int)plSerialNum[i],(nm)?nm:"",status);
	}
    }
  if (plNumUnits == 0) return 0;
  if (plNumUnits == 1) i = win_scanf(message,newPosition,duration);
  else if (plNumUnits == 2)
    i = win_scanf(message,newPosition,duration,newPosition+1,duration+1);
  else if (plNumUnits == 3)
    i = win_scanf(message,newPosition,duration,newPosition+1,duration+1
		  ,newPosition+2,duration+2);
  else if (plNumUnits == 4)
    i = win_scanf(message,newPosition,duration,newPosition+1,duration+1
		  ,newPosition+2,duration+2,newPosition+3,duration+3);
  else if (plNumUnits == 5)
    i = win_scanf(message,newPosition,duration,newPosition+1,duration+1
		  ,newPosition+2,duration+2,newPosition+3,duration+3
		  ,newPosition+4,duration+4);
  else if (plNumUnits == 6)
    i = win_scanf(message,newPosition,duration,newPosition+1,duration+1
		  ,newPosition+2,duration+2,newPosition+3,duration+3
		  ,newPosition+4,duration+4,newPosition+5,duration+5);
  else if (plNumUnits == 7)
    i = win_scanf(message,newPosition,duration,newPosition+1,duration+1
		  ,newPosition+2,duration+2,newPosition+3,duration+3
		  ,newPosition+4,duration+4,newPosition+5,duration+5
		  ,newPosition+6,duration+6);
  else return win_printf_OK("Too many cubes !");
  if (i == WIN_CANCEL) return D_O_K;
  for (i = 0; i < plNumUnits && i < 16; i++)
    {
      if (fabs(newPosition[i]) > 0.01)
	{
	  // do speed !
	  snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_volume",(int)plSerialNum[i]);
	  vol = get_config_float("FLOW_CONTROL",cfg_nb,ser_vol[i]);
	  snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_volume_range",(int)plSerialNum[i]);
	  vol_range = get_config_float("FLOW_CONTROL",cfg_nb,ser_range[i]);
	  tmp = newPosition[i]*vol_range/(1000*vol);
	  if (MOT_GetVelParams(plSerialNum[i], &pfMinVel, &pfAccn, &pfMaxVel))
	    win_printf_OK("Error reading axis velocity\non Cube with serial # %d\n",(int)plSerialNum[i]);
	  duration[i] = fabs(duration[i]);
	  if (duration[i] < 1.0) duration[i]  = 1;
	  vel = fabs(tmp/duration[i]);
	  if (vel > 2.5) vel = 2.5;
	  if (vel < 0.0001) vel = 0.0001;
	  if (MOT_SetVelParams(plSerialNum[i], pfMinVel, pfAccn, vel))
	    win_printf_OK("Error setting axis velocity\non Cube with serial # %d\n",(int)plSerialNum[i]);
	  tmp += pfPosition[i];
	  j = MOT_MoveAbsoluteEx(plSerialNum[i], tmp, 0);
	  if (j) win_printf_OK("Error setting position \non Cube %d with serial # %d\n",i,(int)plSerialNum[i]);
	  snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_position",(int)plSerialNum[i]);
	  set_config_float("FLOW_CONTROL",cfg_nb,pfPosition[i]);
	}
    }
  flush_config_file(); //write_Pico_config_file
  return 0;
}


int do_flow_control_set_Cube_properties(void)
{
  register int i;
  static int devid = 0;
  char message[2048], name[128], cfg_nb[128], *nm;
  float vol = 0, vol_range = 0, mot_range = 0;

  if(updating_menu_state != 0)	return D_O_K;
  if (plNumUnits < 0) win_printf("APT not initiated");
  message[0] = 0;
  if (plNumUnits > 1)
    {
      snprintf(message,sizeof(message),"Choose your device: \n");
      for (i = 0; i < plNumUnits && i < 6; i++)
	{
	  snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_name",(int)plSerialNum[i]);
	  nm = (char*)get_config_string("FLOW_CONTROL",cfg_nb,NULL);
	  if (nm != NULL)
	    snprintf(message+strlen(message),sizeof(message)-strlen(message)
			,"Cube %d with name %s (%d)\n",i,nm,(int)plSerialNum[i]);
	  else snprintf(message+strlen(message),sizeof(message)-strlen(message)
		     ,"Cube %d with serial # %d \n",i,(int)plSerialNum[i]);
	}
      snprintf(message+strlen(message),sizeof(message),"Device index %%d \n");
      i = win_scanf(message,&devid);
      if (i == WIN_CANCEL) return D_O_K;
    }
  else devid = 0;
  snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_name",(int)plSerialNum[devid]);
  nm = (char*)get_config_string("FLOW_CONTROL",cfg_nb,NULL);
  if (nm != NULL)
      strncpy(name,nm,sizeof(name));
  else name[0] = 0;
  snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_volume",(int)plSerialNum[devid]);
  vol = get_config_float("FLOW_CONTROL",cfg_nb,0);
  snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_volume_range",(int)plSerialNum[devid]);
  vol_range = get_config_float("FLOW_CONTROL",cfg_nb,0);
  snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_stage_range",(int)plSerialNum[devid]);
  mot_range = get_config_float("FLOW_CONTROL",cfg_nb,0);

  snprintf(message,sizeof(message),"Device %d with serial # %d\n"
	   "Define name %%20ls\n"
	   "Seringhe volume (%g ml), %%12f \n"
	   "Piston travel (%g mm), %%12f \n"
	   "stage range (%g mm), %%12f \n"
	   "(1ml : 65mm), (20ml : 60mm)\n"
	   ,devid,(int)plSerialNum[devid],vol,vol_range,mot_range);

  i = win_scanf(message,name,&vol,&vol_range,&mot_range);
  if (i == WIN_CANCEL) return D_O_K;
  snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_name",(int)plSerialNum[devid]);
  set_config_string("FLOW_CONTROL",cfg_nb,name);
  snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_volume",(int)plSerialNum[devid]);
  set_config_float("FLOW_CONTROL",cfg_nb,vol);
  snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_volume_range",(int)plSerialNum[devid]);
  set_config_float("FLOW_CONTROL",cfg_nb,vol_range);
  snprintf(cfg_nb,sizeof(cfg_nb),"Cube_%d_stage_range",(int)plSerialNum[devid]);
  set_config_float("FLOW_CONTROL",cfg_nb,mot_range);
  flush_config_file(); // write_Pico_config_file();
  return 0;
}




MENU *apt_flow_control_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	//add_item_to_menu(mn,"data set rescale in Y", do_apt_flow_control_rescale_data_set,NULL,0,NULL);
	//add_item_to_menu(mn,"plot rescale in Y", do_apt_flow_control_rescale_plot,NULL,0,NULL);
	return mn;
}



int	thorlabs_APT_main(int argc, char **argv)
{
  //do_thorlabs_APT_start();
	add_plot_treat_menu_item ( "thorlabs_APT", NULL, thorlabs_APT_plot_menu(), 0, NULL);
	return D_O_K;
}

int	thorlabs_APT_unload(int argc, char **argv)
{
  stop_apt(); // do_thorlabs_APT_stop();
  remove_item_to_menu(plot_treat_menu, "thorlabs_APT", NULL, NULL);
  return D_O_K;
}



MENU *thorlabs_APT_plot_menu(void)
{
  static MENU mn[32];
  static int init = 0;

  if (mn[0].text != NULL)	return mn;
  if (init == 0)
    {
      add_item_to_menu(mn,"start", do_flow_control_start,NULL,0,NULL);
      add_item_to_menu(mn,"stop", do_flow_control_stop,NULL,0,NULL);
      add_item_to_menu(mn,"Read moteur position", do_flow_control_read_MOT_pos,NULL,0,NULL);
      add_item_to_menu(mn,"Set cube properties", do_flow_control_set_Cube_properties,NULL,0,NULL);
      add_item_to_menu(mn,"Inject volume", do_flow_control_sent_MOT_vol,NULL,0,NULL);
      add_item_to_menu(mn,"Move absolute position", do_flow_control_set_MOT_pos,NULL,0,NULL);

      init = 1;
    }
  return mn;
}


# endif

# ifdef OLD
int	apt_flow_control_unload(int argc, char **argv)
{
  //remove_item_to_menu(plot_treat_menu, "apt_flow_control", NULL, NULL);
	return D_O_K;
}
# endif




# ifdef MOTOR_ONLY

long plNumUnits = -1, plSerialNum[64];
float pfPosition[64];





int do_thorlabs_APT_start(void)
{
  register int i, j;
  int ret, er = 0, dlg = 0;
  char message[2048];
  char szModel[256], szSWVer[256], szHWNotes[256];
  pltreg *pr = NULL;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine test Thorlabs APT lib");
    }
  i = win_scanf("Do you want RS232 error window on %b",&dlg);
  if (i == WIN_CANCEL) return D_O_K;
  i = APTInit();
  APTCleanUp();
  i = APTInit();
  if (i != 0)
    {
      APTCleanUp();
      i = APTInit();
    }
  if (i != 0)
    {
      win_printf("APTinit failed !");
      APTCleanUp();
      return 0;
    }
  if (dlg == 0) EnableEventDlg(FALSE);

  ret = GetNumHWUnitsEx(HWTYPE_TDC001, &plNumUnits);
  //win_printf("GetNumHWUnitsEx Ret %d num -> %d",ret, (int)plNumUnits);
  if (plNumUnits < 1)
    {
      win_printf("No device connected");
      APTCleanUp();
      return 0;
    }
  snprintf(message,sizeof(message),"You have %d TDC001 cube connected\n",(int)plNumUnits);
  for (i = 0; i < plNumUnits && i < 64; i++)
    {
      er = 0;
      ret = GetHWSerialNumEx(HWTYPE_TDC001, i, plSerialNum + i);
      if (ret) win_printf("GetHWSerialNumEx Ret %d num %d -> %d",ret, i,(int)plSerialNum[i]);
      er += InitHWDevice(plSerialNum[i]);
      er += MOT_GetPosition(plSerialNum[i], pfPosition + i);
      er += GetHWInfo(plSerialNum[i], szModel, 256, szSWVer, 256, szHWNotes, 256);
      for (j = 0; j < (int)sizeof(szHWNotes) && szHWNotes[j] != 0; j++)
              if ((int)szHWNotes[j] < 32) szHWNotes[j] = 0;
      //win_printf("Mot num %d -> %g",plSerialNum[i],pfPosition[i]);
      if (er == 0)
	snprintf(message+strlen(message),sizeof(message)-strlen(message),
		 "Cube %d with serial # %d at %g mdel %s ver. %s notes %s\n"
		 ,i,(int)plSerialNum[i],pfPosition[i],szModel, szSWVer, szHWNotes);
      else
	snprintf(message+strlen(message),sizeof(message)-strlen(message),
		 "Com error on Cube %d with serial # %d at %g mdel %s ver. %s notes %s\n"
		 ,i,(int)plSerialNum[i],pfPosition[i],szModel, szSWVer, szHWNotes);
      //GetHWInfo(long lSerialNum, TCHAR *szModel, long lModelLen, TCHAR *szSWVer, long lSWVerLen, TCHAR *szHWNotes, long lHWNotesLen);
    }
  win_printf(message);
  do_update_menu();
  if (ac_grep(cur_ac_reg,"%pr",&pr) == 1)
    refresh_plot(pr, UNCHANGED);		
  return 0;
}

int do_thorlabs_APT_stop(void)
{
  if(updating_menu_state != 0)
    {
      if (plNumUnits < 0) active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }
  
  APTCleanUp();
  do_update_menu();
  return 0;
}

int do_thorlabs_APT_read_MOT_pos(void)
{
  register int i, j;
  char message[2048];

  if(updating_menu_state != 0)
    {
      if (plNumUnits < 0 || grabbing > 0) active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }
  if (plNumUnits < 0) win_printf("APT not initiated");
  message[0] = 0;
  for (i = 0; i < plNumUnits && i < 64; i++)
    {
      j = MOT_GetPosition(plSerialNum[i], pfPosition + i);
      if (j == 0)
	snprintf(message+strlen(message),sizeof(message)-strlen(message),
		 "Cube %d with serial # %d at %g\n",i,(int)plSerialNum[i],pfPosition[i]);
      else snprintf(message+strlen(message),sizeof(message)-strlen(message),
		 "error on Cube %d with serial # %d at %g\n",i,(int)plSerialNum[i],pfPosition[i]);
    }
  win_printf(message);
  return 0;
}

int do_thorlabs_APT_set_MOT_pos(void)
{
  register int i, j, k;
  char message[2048], status[64];
  float newPosition[64];
  long plStatusBits[64];

  if(updating_menu_state != 0)
    {
      if (plNumUnits < 0 || grabbing > 0) active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }
  
  if (plNumUnits < 0) return win_printf_OK("APT not initiated");
  status[0] = message[0] = 0;
  for (i = 0; i < plNumUnits && i < 6; i++)
    {
      j = MOT_GetPosition(plSerialNum[i], newPosition + i);
      MOT_GetStatusBits(plSerialNum[i], plStatusBits + i);
      k = (int)plStatusBits[i]&0x07;
      if (k == 1) sprintf(status,"Rev. limit On!");
      else if (k == 2) sprintf(status,"Fwd. limit On!");
      else if (k == 3) sprintf(status," Rev. soft limit On!");
      else if (k == 4) sprintf(status," Fwd. soft limit On!");
      else if (k == 5) sprintf(status," Mot. moving Rev.!");
      else if (k == 6) sprintf(status," Mot. moving Fwd.!");
      else if (k == 7) sprintf(status," Shaft jogging Rev.!");
      else if (k == 8) sprintf(status," Shaft jogging Fwd.!");
      if (j == 0)
	{
	  pfPosition[i] = newPosition[i];
	  snprintf(message+strlen(message),sizeof(message)-strlen(message),
		   "Cube %d with serial # %d move to %%12f %s\n"
		   ,i,(int)plSerialNum[i],status);
	}
      else
	{
	  snprintf(message+strlen(message),sizeof(message)-strlen(message),
		   "Error on Cube %d with serial # %d move to %%12f %s\n"
		   ,i,(int)plSerialNum[i],status);
	}
    }
  if (plNumUnits == 0) return 0;
  if (plNumUnits == 1) i = win_scanf(message,newPosition);
  else if (plNumUnits == 2) i = win_scanf(message,newPosition,newPosition+1);
  else if (plNumUnits == 3) i = win_scanf(message,newPosition,newPosition+1,newPosition+2);
  else if (plNumUnits == 4) i = win_scanf(message,newPosition,newPosition+1,newPosition+2,newPosition+3);
  else i = win_scanf(message,newPosition,newPosition+1,newPosition+2,newPosition+3,newPosition+4);
  if (i == WIN_CANCEL) return D_O_K;
  for (i = 0; i < plNumUnits && i < 6; i++)
    {
      if (pfPosition[i] != newPosition[i])
	{
	  j = MOT_MoveAbsoluteEx(plSerialNum[i], newPosition[i], 0);
	  if (j) win_printf_OK("Error setting position \non Cube %d with serial # %d\n",i,(int)plSerialNum[i]);
	}
    }
  return 0;
}

int do_thorlabs_APT_set_MOT_pos_relative(void)
{
  register int i, j;
  char message[2048];
  float newPosition[64];
  long plRevLimSwitch[64], plFwdLimSwitch[64];

  if(updating_menu_state != 0)
    {
      if (plNumUnits < 0 || grabbing > 0) active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }
  if (plNumUnits < 0) return win_printf_OK("APT not initiated");
  message[0] = 0;
  for (i = 0; i < plNumUnits && i < 6; i++)
    {
      j = MOT_GetPosition(plSerialNum[i], newPosition + i);
      j += MOT_GetHWLimSwitches(plSerialNum[i], plRevLimSwitch + i , plFwdLimSwitch + i);
      if (j == 0)
	{
	  pfPosition[i] = newPosition[i];
	  snprintf(message+strlen(message),sizeof(message)-strlen(message),
		   "Cube %d with serial # %d RevL %d FwdL %d move %%f \n",i
		   ,(int)plSerialNum[i], (int)plRevLimSwitch[i] , (int)plFwdLimSwitch[i]);
	}
      else
	{
	  snprintf(message+strlen(message),sizeof(message)-strlen(message),
		   "Error on Cube %d with serial # %d RevL %d FwdL %d move %%f \n",i
		   ,(int)plSerialNum[i], (int)plRevLimSwitch[i] , (int)plFwdLimSwitch[i]);
	}
    }
  if (plNumUnits == 0) return 0;
  if (plNumUnits == 1) i = win_scanf(message,newPosition);
  else if (plNumUnits == 2) i = win_scanf(message,newPosition,newPosition+1);
  else if (plNumUnits == 3) i = win_scanf(message,newPosition,newPosition+1,newPosition+2);
  else if (plNumUnits == 4) i = win_scanf(message,newPosition,newPosition+1,newPosition+2,newPosition+3);
  else i = win_scanf(message,newPosition,newPosition+1,newPosition+2,newPosition+3,newPosition+4);
  if (i == WIN_CANCEL) return D_O_K;
  for (i = 0; i < plNumUnits && i < 6; i++)
    {
      if (pfPosition[i] != newPosition[i])
	{
	  j = MOT_MoveRelativeEx(plSerialNum[i], newPosition[i]-pfPosition[i], 0);
	  if (j) win_printf_OK("Error setting position \non Cube %d with serial # %d\n",i,(int)plSerialNum[i]);	
	}
    }
  return 0;
}


int do_thorlabs_APT_small_inc_MOT_pos(void)
{
  register int i, j;
  float s_step = small_step;

  if(updating_menu_state != 0)
    {
      if (plNumUnits < 0 || grabbing > 0) active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }
  
  if (plNumUnits < 0) return win_printf_OK("APT not initiated");
  if (plNumUnits != 1) return win_printf_OK("You have several motors");
  if (key[KEY_LSHIFT])
    {
       i = win_scanf("Small step size %6f",&s_step);
       if (i == WIN_CANCEL) return 0;
       small_step = s_step;
    }
  j = MOT_GetPosition(plSerialNum[0], pfPosition);
  if (j == 0) my_set_window_title("Aiming position %g",pfPosition[0]+small_step);
  j = MOT_MoveRelativeEx(plSerialNum[0], small_step, 0);
  if (j) win_printf_OK("Error setting position \non Cube 0 with serial # %d\n"
                       ,(int)plSerialNum[0]);	
  return 0;
}

int do_thorlabs_APT_small_dec_MOT_pos(void)
{
  register int i, j;
  float s_step = small_step;

  if(updating_menu_state != 0)
    {
      if (plNumUnits < 0 || grabbing > 0 ) active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }
  if (plNumUnits < 0) return win_printf_OK("APT not initiated");
  if (plNumUnits != 1) return win_printf_OK("You have several motors");
  if (key[KEY_LSHIFT])
    {
       i = win_scanf("Small step size %6f",&s_step);
       if (i == WIN_CANCEL) return 0;
       small_step = s_step;
    }
  j = MOT_GetPosition(plSerialNum[0], pfPosition);
  if (j == 0) my_set_window_title("Aiming position %g",pfPosition[0]-small_step);
  j = MOT_MoveRelativeEx(plSerialNum[0], -small_step, 0);
  if (j) win_printf_OK("Error setting position \non Cube 0 with serial # %d\n"
                       ,(int)plSerialNum[0]);	
  return 0;
}





int do_thorlabs_APT_get_MOT_home(void)
{
  register int i;
  static int devid = 0;
  char message[2048];
  long plDirection, plLimSwitch;
  float pfHomeVel, pfZeroOffset;
  int dir = 0, do_home = 1;

  if(updating_menu_state != 0)
    {
      if (plNumUnits < 0 || grabbing > 0) active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }
  if (plNumUnits < 0) win_printf("APT not initiated");
  message[0] = 0;
  if (plNumUnits > 1)
    {
      snprintf(message,sizeof(message),"Choose your device: \n");
      for (i = 0; i < plNumUnits && i < 6; i++)
	  snprintf(message+strlen(message),sizeof(message)-strlen(message),"Cube %d with serial # %d \n",i,(int)plSerialNum[i]);
      snprintf(message+strlen(message),sizeof(message),"Device index %%d \n");
      i = win_scanf(message,&devid);
      if (i == WIN_CANCEL) return D_O_K;
    }
  else devid = 0;
  i = MOT_GetHomeParams(plSerialNum[devid], &plDirection, &plLimSwitch, &pfHomeVel, &pfZeroOffset);

  if (i) return win_printf_OK("Error reading home params\non Cube %d with serial # %d\n"
			      ,devid,(int)plSerialNum[devid]);	
  if (plDirection == HOME_REV) dir = 1;
  snprintf(message,sizeof(message),"Device %d with serial # %d\n"
	   "Homing in %%R forward or %%r backward direction\n"
	   "pfHomeVel (%g) %%12f \npfZeroOffset (%g) %%12f\nDo home setting %%b\n"
	   ,devid,(int)plSerialNum[devid], pfHomeVel, pfZeroOffset);


  i = win_scanf(message, &dir, &pfHomeVel, &pfZeroOffset, &do_home);
  if (i == WIN_CANCEL) return D_O_K;
  if (dir == 0)
    {
      plDirection = HOME_FWD;
      plLimSwitch = HOMELIMSW_FWD;
    }
  else
    {
      plDirection = HOME_REV;
      plLimSwitch = HOMELIMSW_REV;
    }
  i = MOT_SetHomeParams(plSerialNum[devid], plDirection, plLimSwitch, pfHomeVel, pfZeroOffset);
  if (i) return win_printf_OK("Error reading home params\non Cube %d with serial # %d\n"
			      ,devid,(int)plSerialNum[devid]);	
  if (do_home)
    {
      i = MOT_MoveHome(plSerialNum[devid], 0);
      if (i) return win_printf_OK("Error stting home\non Cube %d with serial # %d\n"
				  ,devid,(int)plSerialNum[devid]);	
    }
  return 0;
}

int do_thorlabs_APT_get_MOT_VelParams(void)
{
  register int i, j;
  static int devid = 0;
  char message[2048];
  float pfMinVel, pfAccn, pfMaxVel;

  if(updating_menu_state != 0)
    {
      if (plNumUnits < 0 || grabbing > 0) active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }
  if(updating_menu_state != 0)	return D_O_K;
  if (plNumUnits < 0) win_printf("APT not initiated");
  message[0] = 0;
  if (plNumUnits > 1)
    {
      snprintf(message,sizeof(message),"Choose your device: \n");
      for (i = 0; i < plNumUnits && i < 6; i++)
	  snprintf(message+strlen(message),sizeof(message)-strlen(message),"Cube %d with serial # %d \n",i,(int)plSerialNum[i]);
      snprintf(message+strlen(message),sizeof(message),"Device index %%d \n");
      i = win_scanf(message,&devid);
      if (i == WIN_CANCEL) return D_O_K;
    }
  else devid = 0;
  j = MOT_GetVelParams(plSerialNum[devid], &pfMinVel, &pfAccn, &pfMaxVel);
  if (j) return win_printf_OK("Error reading velocity params\non Cube %d with serial # %d\n"
			      ,devid,(int)plSerialNum[devid]);
  snprintf(message,sizeof(message),"Device %d with serial # %d\n"
	     "Min velocity (%g), %%12f \n"
	     "Max velocity (%g), %%12f \n"
	     "Acceleration (%g), %%12f \n"
	   ,devid,(int)plSerialNum[devid],pfMinVel, pfMaxVel, pfAccn);

  i = win_scanf(message,&pfMinVel, &pfMaxVel, &pfAccn);
  if (i == WIN_CANCEL) return D_O_K;
  j = MOT_SetVelParams(plSerialNum[devid], pfMinVel, pfAccn, pfMaxVel);
  if (j) return win_printf_OK("Error reading velocity params\non Cube %d with serial # %d\n"
			      ,devid,(int)plSerialNum[devid]);
  return 0;
}

int do_thorlabs_APT_get_MOT_AxisInfo(void)
{
  register int i, j;
  static int devid = 0;
  char message[2048];
  float pfMinPos, pfMaxPos, pfPitch;
  long plUnits;

  if(updating_menu_state != 0)
    {
      if (plNumUnits < 0 || grabbing > 0) active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }
  if (plNumUnits < 0) win_printf("APT not initiated");
  message[0] = 0;
  if (plNumUnits > 1)
    {
      snprintf(message,sizeof(message),"Choose your device: \n");
      for (i = 0; i < plNumUnits && i < 6; i++)
	  snprintf(message+strlen(message),sizeof(message)-strlen(message),"Cube %d with serial # %d \n",i,(int)plSerialNum[i]);
      snprintf(message+strlen(message),sizeof(message),"Device index %%d \n");
      i = win_scanf(message,&devid);
      if (i == WIN_CANCEL) return D_O_K;
    }
  else devid = 0;
  j = MOT_GetStageAxisInfo(plSerialNum[devid], &pfMinPos, &pfMaxPos, &plUnits, &pfPitch);
  if (j) return win_printf_OK("Error reading axis params\non Cube %d with serial # %d\n"
			      ,devid,(int)plSerialNum[devid]);	
  snprintf(message,sizeof(message),"Device %d with serial # %d\n"
	     "Min position (%g), %%12f \n"
	     "Max position (%g), %%12f \n"
	     "Unit (%d), %%12d \n"
	     "Pitch (%g), %%12f \n"
	   ,devid,(int)plSerialNum[devid],pfMinPos, pfMaxPos, (int)plUnits, pfPitch);

  i = win_scanf(message,&pfMinPos, &pfMaxPos, &plUnits, &pfPitch);
  if (i == WIN_CANCEL) return D_O_K;
  j = MOT_SetStageAxisInfo(plSerialNum[devid], pfMinPos, pfMaxPos, plUnits, pfPitch);
  if (j) return win_printf_OK("Error reading axis params\non Cube %d with serial # %d\n"
			      ,devid,(int)plSerialNum[devid]);	
  return 0;
}

int do_thorlabs_APT_chg_MOT_PID(void)
{
  register int i, j;
  static int devid = 0;
  char message[2048];
  long lProp, lInt, lDeriv, lIntLimit;
  int ilProp, ilInt, ilDeriv, ilIntLimit;


  if(updating_menu_state != 0)
    {
      if (plNumUnits < 0  || grabbing > 0) active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }
  if (plNumUnits < 0) win_printf("APT not initiated");
  message[0] = 0;
  if (plNumUnits > 1)
    {
      snprintf(message,sizeof(message),"Choose your device: \n");
      for (i = 0; i < plNumUnits && i < 6; i++)
	  snprintf(message+strlen(message),sizeof(message)-strlen(message),"Cube %d with serial # %d \n",i,(int)plSerialNum[i]);
      snprintf(message+strlen(message),sizeof(message),"Device index %%d \n");
      i = win_scanf(message,&devid);
      if (i == WIN_CANCEL) return D_O_K;
    }
  else devid = 0;
  j = MOT_GetPIDParams(plSerialNum[devid], &lProp, &lInt, &lDeriv, &lIntLimit);
  if (j) return win_printf_OK("Error reading PID params\non Cube %d with serial # %d\n"
			      ,devid,(int)plSerialNum[devid]);	
  snprintf(message,sizeof(message),"Device %d with serial # %d\n"
	     "Proportionnal (%d), %%12d \n"
	     "Integral (%d), %%12d \n"
	     "Derivative (%d), %%12d \n"
	     "Limit (%d), %%12d \n"
	   ,devid,(int)plSerialNum[devid],(int)lProp, (int)lInt, (int)lDeriv, (int)lIntLimit);
  ilProp = (int)lProp; ilInt = (int)lInt; ilDeriv = (int)lDeriv; ilIntLimit = (int)lIntLimit;
  i = win_scanf(message,&ilProp, &ilInt, &ilDeriv, &ilIntLimit);
  if (i == WIN_CANCEL) return D_O_K;
  j = MOT_SetPIDParams(plSerialNum[devid], (long)ilProp, (long)ilInt, (long)ilDeriv, (long)ilIntLimit);
  if (j) return win_printf_OK("Error reading PID params\non Cube %d with serial # %d\n"
			      ,devid,(int)plSerialNum[devid]);	

  return 0;
}


int do_thorlabs_APT_set_MOT_home(void)
{
  register int i, j;
  static int devid = 0;
  char message[2048];

  if(updating_menu_state != 0)
    {
      if (plNumUnits < 0  || grabbing > 0) active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }
  if (plNumUnits < 0) win_printf("APT not initiated");
  message[0] = 0;
  if (plNumUnits > 1)
    {
      snprintf(message,sizeof(message),"Choose your device: \n");
      for (i = 0; i < plNumUnits && i < 6; i++)
	  snprintf(message+strlen(message),sizeof(message)-strlen(message),"Cube %d with serial # %d \n",i,(int)plSerialNum[i]);
      snprintf(message+strlen(message),sizeof(message),"Device index %%d \n");
      i = win_scanf(message,&devid);
      if (i == WIN_CANCEL) return D_O_K;
    }
  else devid = 0;
  i = win_printf("Do you want to set Device %d with serial # %d\n at home position ?\n"
		 ,devid,plSerialNum[devid]);
  if (i == WIN_CANCEL) return D_O_K;
  j = MOT_MoveHome(plSerialNum[devid], 0);
  if (j) return win_printf_OK("Error stting home\non Cube %d with serial # %d\n"
			      ,devid,(int)plSerialNum[devid]);	

  return 0;
}




int do_thorlabs_APT_get_MOT_BlashDistance(void)
{
  register int i, j;
  static int devid = 0;
  char message[2048];
  float pfBLashDist;

  if(updating_menu_state != 0)
    {
      if (plNumUnits < 0  || grabbing > 0) active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }
  if(updating_menu_state != 0)	return D_O_K;
  if (plNumUnits < 0) win_printf("APT not initiated");
  message[0] = 0;
  if (plNumUnits > 1)
    {
      snprintf(message,sizeof(message),"Choose your device: \n");
      for (i = 0; i < plNumUnits && i < 6; i++)
	  snprintf(message+strlen(message),sizeof(message)-strlen(message),"Cube %d with serial # %d \n",i,(int)plSerialNum[i]);
      snprintf(message+strlen(message),sizeof(message),"Device index %%d \n");
      i = win_scanf(message,&devid);
      if (i == WIN_CANCEL) return D_O_K;
    }
  else devid = 0;
  j = MOT_GetBLashDist(plSerialNum[devid], &pfBLashDist);
  if (j) return win_printf_OK("Error reading blash distance\non Cube %d with serial # %d\n"
			      ,devid,(int)plSerialNum[devid]);	
  snprintf(message,sizeof(message),"Device %d with serial # %d\n"
	     "Blash distance (%g), %%12f \n"
	   ,devid,(int)plSerialNum[devid],pfBLashDist);

  i = win_scanf(message,&pfBLashDist);
  if (i == WIN_CANCEL) return D_O_K;
  j = MOT_SetBLashDist(plSerialNum[devid], pfBLashDist);
  if (j) return win_printf_OK("Error setting blash distance\non Cube %d with serial # %d\n"
			      ,devid,(int)plSerialNum[devid]);	
  return 0;
}



int grab_zmag_plot_from_serial_port(void)
{
  pltreg *pr;
  O_p *op;
  d_s *ds;
  static int wait_ms = 25;
  int  i, j, nx = 400;
  static float step = 1, zmag; // , sgn = 1
  unsigned long t0, tt, tn, dt;
  static int devid = 0;
  char message[2048];
  float newPosition[64], tmp = 0;
  long lProp, lInt, lDeriv, lIntLimit;
  int ilProp, ilInt, ilDeriv, ilIntLimit;



  if(updating_menu_state != 0)
    {
      if (plNumUnits < 0 || grabbing > 0) active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }

  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return(win_printf("can't find plot"));

  if (plNumUnits < 0) win_printf("APT not initiated");
  message[0] = 0;
  if (plNumUnits > 1)
    {
      snprintf(message,sizeof(message),"Choose your device: \n");
      for (i = 0; i < plNumUnits && i < 6; i++)
	  snprintf(message+strlen(message),sizeof(message)-strlen(message),"Cube %d with serial # %d \n",i,(int)plSerialNum[i]);
      snprintf(message+strlen(message),sizeof(message),"Device index %%d \n");
      i = win_scanf(message,&devid);
      if (i == WIN_CANCEL) return D_O_K;
    }
  else devid = 0;
  j = MOT_GetPIDParams(plSerialNum[devid], &lProp, &lInt, &lDeriv, &lIntLimit);
  if (j) return win_printf_OK("Error reading PID params\non Cube %d with serial # %d\n"
			      ,devid,(int)plSerialNum[devid]);
  j = MOT_GetPosition(plSerialNum[devid], newPosition + devid);
  if (j) return win_printf_OK("Error reading position off Cube %d with serial # %d\n"
			      ,devid,(int)plSerialNum[devid]);

  snprintf(message,sizeof(message),"Device %d with serial # %d\n"
	   "Proportionnal (%d), %%12d \n"
	   "Integral (%d), %%12d \n"
	   "Derivative (%d), %%12d \n"
	   "Limit (%d), %%12d \n"
	   "Starting position %g\n"
	   "I am going to do a step of %%6f mm\n"
	   "Define the nb of points to record %%8d\n"
	   "Define the waiting time between reads %%8d ms\n"
	   ,devid,(int)plSerialNum[devid],(int)lProp, (int)lInt
	   , (int)lDeriv, (int)lIntLimit,newPosition[devid]);

  ilProp = (int)lProp; ilInt = (int)lInt; ilDeriv = (int)lDeriv; ilIntLimit = (int)lIntLimit;
  i = win_scanf(message,&ilProp, &ilInt, &ilDeriv, &ilIntLimit,&step,&nx,&wait_ms);
  if (i == WIN_CANCEL) return D_O_K;
  j = MOT_SetPIDParams(plSerialNum[devid], (long)ilProp, (long)ilInt, (long)ilDeriv, (long)ilIntLimit);
  if (j) return win_printf_OK("Error reading PID params\non Cube %d with serial # %d\n"
			      ,devid,(int)plSerialNum[devid]);
  //sgn = (step < 0) ? -1 : 1;

  pr = create_pltreg_with_op(&op, nx, nx, 0);
  if (op == NULL)    return win_printf("Cannot allocate plot");
  ds = op->dat[0];
  if (ds == NULL)    return win_printf("Cannot allocate plot");

  zmag = newPosition[devid];
  newPosition[devid] = zmag + step;
  j = MOT_MoveAbsoluteEx(plSerialNum[devid], newPosition[devid], 0);
  if (j) return win_printf_OK("Error setting position off Cube %d with serial # %d\n"
			      ,devid,(int)plSerialNum[devid]);



  for (i = 0, tt = 0, t0 = my_uclock(); i < nx; i++)
    {
      tn = t0 + i*(wait_ms*get_my_uclocks_per_sec()/1000);
      for (; (tt = my_uclock()) < tn; );
      tt = my_uclock();
      j = MOT_GetPosition(plSerialNum[devid], newPosition + devid);
      dt = my_uclock() - tt;
      if (j) return win_printf_OK("Error reading position off Cube %d with serial # %d\n"
			      ,devid,(int)plSerialNum[devid]);
      ds->yd[i] = newPosition[devid];
      ds->xd[i] =  (1000*(tt-t0))/get_my_uclocks_per_sec();
      tmp += (float)(1000*dt)/get_my_uclocks_per_sec();
      my_set_window_title("iter %d %g",i, ds->yd[i]);
    }
  tmp /= nx;
  win_printf("Mean read time %g ms",tmp);

  set_plot_title(op,"\\stack{{Zmag timing}{P %d I %d D %d}}",ilProp, ilInt, ilDeriv);
  create_attach_select_y_un_to_op(op, IS_METER, 0 ,(float)1, -3, 0, "mm");
  create_attach_select_x_un_to_op(op, IS_SECOND, 0,(float)1, -3, 0, "ms");
  set_plot_x_title(op, "Time");
  set_plot_y_title(op, "Zmag");

  zmag = newPosition[devid];
  newPosition[devid] = zmag;
  j = MOT_MoveAbsoluteEx(plSerialNum[devid], newPosition[devid], 0);
  if (j) return win_printf_OK("Error setting position off Cube %d with serial # %d\n"
			      ,devid,(int)plSerialNum[devid]);	


  refresh_plot(pr, pr->n_op-1);		
  return D_O_K;
}


static void *grab_motor_data(void* data)
{
    struct timespec tim, tim2;
    float tmp = 0;
    int iter = 0, j, apt_zmag_cycle_start = -1, apt_zmag_cycle_index = 0;
    tim.tv_sec = 0;
    tim.tv_nsec = 100000000;
    static float pfMinVel = 1, pfAccn, pfMaxVel = 2.5;	    

    (void)data;
    for (iter = 0; grabbing > 0; iter++)
      {
	if (apt_zmag_cycle == 0) apt_zmag_cycle_start = -1;
	else
	  {
	    apt_zmag_cycle_start = (apt_zmag_cycle_start < 0) ? iter : apt_zmag_cycle_start;
	    apt_zmag_cycle_index  = iter - apt_zmag_cycle_start;
	    apt_zmag_cycle_index %= (dur_zmag_hi + dur_zmag_lo);
	  }
	if (iter%32 == 0 && apt_zmag_cycle == 0)
	  {
	    j = MOT_GetVelParams(plSerialNum[0], &pfMinVel, &pfAccn, &pfMaxVel);
	    if (j == 0) prev_zmag_speed_target = pfMaxVel;
	    apt_thread_action = 1;
	  }
	else if (apt_zmag_cycle > 0 && apt_zmag_cycle_index == 0)
	  {
	    j = MOT_MoveAbsoluteEx(plSerialNum[0], zmag_target = zmag_hi, 0);
	    if (j == 0) prev_zmag_target = zmag_hi;
	    apt_thread_action = 2;
	  }
	else if (apt_zmag_cycle > 0 && apt_zmag_cycle_index == dur_zmag_hi)
	  {
	    j = MOT_MoveAbsoluteEx(plSerialNum[0], zmag_target = zmag_lo, 0);
	    if (j == 0) prev_zmag_target = zmag_lo;
	    apt_thread_action = 2;
	  }	
	else if (fabs(zmag_target - prev_zmag_target) > 0.005)
	  {
	    j = MOT_MoveAbsoluteEx(plSerialNum[0], zmag_target, 0);
	    if (j == 0) prev_zmag_target = zmag_target;
	    apt_thread_action = 2;
	  }
	else if (fabs(zmag_speed_target - prev_zmag_speed_target) > 0.005)
	  {
	    zmag_speed_target = (zmag_speed_target > 2.5) ? 2.5 : zmag_speed_target;
	    zmag_speed_target = (zmag_speed_target < pfMinVel) ? pfMinVel : zmag_speed_target;
	    j = MOT_SetVelParams(plSerialNum[0], pfMinVel, pfAccn, zmag_speed_target);
	    if (j == 0) prev_zmag_speed_target = zmag_speed_target;
	    apt_thread_action = 3;
	  }
	else
	  {
	    j = MOT_GetPosition(plSerialNum[0], &tmp );
	    if (j == 0) zmag_pos = tmp;
	    apt_thread_action = 4;
	  }
	if (zmag_saving)
	  {	
	    apt_zmag_fp = fopen(zamagfilename, "a");
	    if (apt_zmag_fp != NULL)
	      {
		if (zmag_saving == 2)
		  fprintf(apt_zmag_fp,"%d\t%f\t%d\n",iter,zmag_pos,apt_thread_action);
		else fprintf(apt_zmag_fp,"%d\t%f\n",iter,zmag_pos);
		
		fclose(apt_zmag_fp);	
	      }

	  }
	nanosleep(&tim , &tim2);
      }
   return 0;
}



int launch_zmag_thread(void)
{
    int i;
    if(updating_menu_state != 0)
      {
	if (plNumUnits < 0) active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }
    i = win_scanf("Do you want to save Zmag position in a file:\n"
		  "%R->no %r->simple %r->complete\n",&zmag_saving);
    if (i == WIN_CANCEL) return 0;
    if (zmag_saving)
      {
	if (do_select_file(zamagfilename, sizeof(zamagfilename), "TXT-FILE", "*.txt", "File to save debug stuff\0", 0))
	  return win_printf_OK("Cannot select input file");

	apt_zmag_fp = fopen(zamagfilename, "r");
	if (apt_zmag_fp != NULL)
	  {
	    i = win_printf("File already exist:\n%s\nReady to overwrite!"
		       , backslash_to_slash(zamagfilename));
	    if (i == WIN_CANCEL)    return 0;
	  }
	fclose(apt_zmag_fp);
	apt_zmag_fp = fopen(zamagfilename, "w");
	if (apt_zmag_fp == NULL)
	  {
	    win_printf("Cannot open File:\n%s\n"
		       , backslash_to_slash(zamagfilename));
	    return 0;
	  }
	if (zmag_saving == 2)
	  fprintf(apt_zmag_fp,"%%Iter\tZmag\tAction\n");
	else fprintf(apt_zmag_fp,"%%Iter\tZmag\n");
	fclose(apt_zmag_fp);	
      }
    
    if (pthread_create (&thread_apt, NULL, grab_motor_data, NULL))
      win_printf_OK("Could not start APT grabbing thread!");
    grabbing = 1;
    do_update_menu();
    return 0;
}    

int move_zmag_on_grabbing(void)
{
  int i;
  float tmp;
  char message[256] = {0};
  
  if(updating_menu_state != 0)
    {
      if (plNumUnits < 0 || grabbing <= 0) active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }
  snprintf(message,sizeof(message),"Zmag position %g (target %g)\nNew position %%6f\n"
	   ,zmag_pos,zmag_target);
  tmp = zmag_target;
  i = win_scanf(message,&tmp);
  if (i == WIN_CANCEL) return 0;
  zmag_target = tmp;
  do_update_menu();
  return 0;
}    

int start_zmag_cycle(void)
{
  int i;
  float lo, hi;
  int dlo, dhi;
  char message[512] = {0};
  
  if(updating_menu_state != 0)
    {
      if (plNumUnits < 0 || grabbing <= 0 || apt_zmag_cycle > 0)
	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }
  snprintf(message,sizeof(message),"Zmag cycles between two positions\n"
	   "actual position %g (target %g)\n"
	   "High force position %%6f\n"
	   "High force duration (in 0.1S) %%4d\n"
	   "Low force position %%6f\n"
	   "Low force duration (in 0.1S) %%4d\n"	   
	   ,zmag_pos,zmag_target);
  hi = zmag_hi;
  lo = zmag_lo;
  dhi = dur_zmag_hi;
  dlo = dur_zmag_lo;

  i = win_scanf(message,&hi,&dhi,&lo,&dlo);
  if (i == WIN_CANCEL) return 0;
  zmag_hi = hi;
  zmag_lo = lo;
  dur_zmag_hi = dhi;
  dur_zmag_lo = dlo;
  apt_zmag_cycle = 1;
  do_update_menu();
  return 0;
}    

int stop_zmag_cycle(void)
{
  if(updating_menu_state != 0)
    {
      if (plNumUnits < 0 || grabbing <= 0 || apt_zmag_cycle == 0)
	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }
  apt_zmag_cycle = 0;
  do_update_menu();
  return 0;
}    



int set_zmag_speed_on_grabbing(void)
{
  int i;
  float tmp;
  char message[256] = {0};
  
  if(updating_menu_state != 0)
    {
      if (plNumUnits < 0 || grabbing <= 0) active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }
  snprintf(message,sizeof(message),"Zmag speed %g \nNew speed %%6f\n"
	   ,zmag_speed_target);
  tmp = prev_zmag_speed_target;
  i = win_scanf(message,&tmp);
  if (i == WIN_CANCEL) return 0;
  zmag_speed_target = tmp;
  do_update_menu();
  return 0;
}

 
MENU *thorlabs_APT_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"start APT", do_thorlabs_APT_start,NULL,0,NULL);
	add_item_to_menu(mn,"stop APT", do_thorlabs_APT_stop,NULL,0,NULL);
	add_item_to_menu(mn,"read position", do_thorlabs_APT_read_MOT_pos,NULL,0,NULL);
	add_item_to_menu(mn,"set position", do_thorlabs_APT_set_MOT_pos,NULL,0,NULL);
	add_item_to_menu(mn,"set position 2", do_thorlabs_APT_set_MOT_pos_relative,NULL,0,NULL);
	add_item_to_menu(mn,"get home", do_thorlabs_APT_get_MOT_home,NULL,0,NULL);
	add_item_to_menu(mn,"set home", do_thorlabs_APT_set_MOT_home,NULL,0,NULL);
	add_item_to_menu(mn,"Motor velocity params", do_thorlabs_APT_get_MOT_VelParams,NULL,0,NULL);
	add_item_to_menu(mn,"Motor PID params", do_thorlabs_APT_chg_MOT_PID,NULL,0,NULL);
	add_item_to_menu(mn,"Motor axis info", do_thorlabs_APT_get_MOT_AxisInfo,NULL,0,NULL);
	add_item_to_menu(mn,"Motor blash distance",do_thorlabs_APT_get_MOT_BlashDistance,NULL,0,NULL);
	add_item_to_menu(mn,"Transient",grab_zmag_plot_from_serial_port,NULL,0,NULL);
	add_item_to_menu(mn,"Launch APT thread",launch_zmag_thread,NULL,0,NULL);

	add_item_to_menu(mn,"Move Zmag in thread",move_zmag_on_grabbing,NULL,0,NULL);
	add_item_to_menu(mn,"Change Zmag speed in thread",set_zmag_speed_on_grabbing,NULL,0,NULL);	

	




	//add_item_to_menu(mn,"plot rescale in Y", do_thorlabs_APT_rescale_plot,NULL,0,NULL);
	return mn;
}

int	thorlabs_APT_main(int argc, char **argv)
{
  //do_thorlabs_APT_start();
        (void)argc;
        (void)argv;
	add_plot_treat_menu_item ( "thorlabs_APT", NULL, thorlabs_APT_plot_menu(), 0, NULL);
	return D_O_K;
}

int	thorlabs_APT_unload(int argc, char **argv)
{
        (void)argc;
        (void)argv;
  do_thorlabs_APT_stop();
  remove_item_to_menu(plot_treat_menu, "thorlabs_APT", NULL, NULL);
  return D_O_K;
}


# endif

#endif

