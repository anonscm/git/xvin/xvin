#ifndef _THORLABS_APT_H_
#define _THORLABS_APT_H_




PXV_FUNC(int, do_thorlabs_APT_rescale_plot, (void));
PXV_FUNC(MENU*, thorlabs_APT_plot_menu, (void));
PXV_FUNC(int, do_thorlabs_APT_rescale_data_set, (void));
PXV_FUNC(int, thorlabs_APT_main, (int argc, char **argv));
PXV_FUNC(int, do_flow_control_start, (void));
PXV_FUNC(int, do_flow_control_stop, (void));
PXV_FUNC(int, do_thorlabs_APT_start, (void));
PXV_FUNC(int, do_thorlabs_APT_stop, (void));
PXV_FUNC(int, do_thorlabs_APT_read_MOT_pos, (void));
PXV_FUNC(int, do_thorlabs_APT_set_MOT_pos, (void));
PXV_FUNC(int, do_thorlabs_APT_set_MOT_pos_relative, (void));
PXV_FUNC(int, do_thorlabs_APT_get_MOT_home, (void));
PXV_FUNC(int, do_thorlabs_APT_set_MOT_home, (void));
PXV_FUNC(int, do_thorlabs_APT_small_inc_MOT_pos, (void));
PXV_FUNC(int, do_thorlabs_APT_small_dec_MOT_pos, (void));
PXV_FUNC(int, set_zmag_speed_on_grabbing, (void));
PXV_FUNC(int, move_zmag_on_grabbing, (void));
PXV_FUNC(int, launch_zmag_thread, (void));
PXV_FUNC(int, start_zmag_cycle, (void));
PXV_FUNC(int, stop_zmag_cycle, (void));
#ifndef _THORLABS_APT_C_
PXV_VAR(long, plNumUnits);
XV_ARRAY(long, plSerialNum);
XV_ARRAY(float, pfPosition);
PXV_VAR(int, grabbing);
PXV_VAR(int, apt_thread_action);
XV_VAR(float, zmag_pos);
XV_VAR(float, zmag_target);
XV_VAR(float, prev_zmag_target);

#endif
#ifdef _THORLABS_APT_C_
int grabbing = 0;
int  apt_thread_action = 0, apt_zmag_cycle = 0;
float zmag_hi = 0, zmag_lo = 0;
int dur_zmag_hi = 40, dur_zmag_lo = 100, zmag_saving = 0;

#endif
#endif

