/*    Simple Plug-in program for Continuous Synchronous Analog and Digital input in Xvin winth NIDAQMX.
 *
 *    F. Mosconi
  */
/*********************************************************************
*
* ANSI C Example program:
*    ContAIAO.c
*
* Example Category:
*    Sync
*
* Description:
*    This example demonstrates how to continuously acquire analog and
*    digital data at the same time, synchronized with one another on
*    the same device.
*
* Instructions for Running:
*    1. Select the physical channel to correspond to where your
*       analog signal is input on the DAQ device. Also, select the
*       channel to correspond to where your digital signal is input
*       on the DAQ device.
*    2. Enter the minimum and maximum voltage ranges.
*    Note: For better accuracy try to match the input range to the
*          expected voltage level of the measured signal.
*    3. Set the sample rate of the acquisition.
*    Note: The rate should be at least twice as fast as the maximum
*          frequency component of the signal being acquired.

*    Note: This example requires two DMA channels to run. If your
*          hardware does not support two DMA channels, you need to
*          set the Data Transfer Mechanism attribute for the Digital
*          Input Task to use "Interrupts".
*
*    Refer to your device documentation to determine how many DMA
*    channels are supported for your hardware.
*
* Steps:
*    1. Create a task.
*    2. Create an analog input voltage channel. Also, create a
*       Digital Input channel.
*    3. Set the rate for the sample clocks. Additionally, define the
*       sample modes to be continuous.

*    3a. Call the GetTerminalNameWithDevPrefix function. This will
*    take a Task and a terminal and create a properly formatted
*    device + terminal name to use as the source of the digital
*    sample clock.
*    4. Call the Start function to arm the two tasks. Make sure the
*       digital input task is armed before the analog input task.
*       This will ensure both will start at the same time.
*    5. Read the waveform data continuously until the user hits the
*       stop button or an error occurs.
*    6. Call the Stop function to stop the acquisition.
*    7. Call the Clear Task function to clear the task.
*    8. Display an error if any.
*
* I/O Connections Overview:
*    Make sure your signal input terminals match the Physical Channel
*    I/O controls.
*
*********************************************************************/

#ifndef _CONTAIAO_C_
#define _CONTAIAO_C_

# include "allegro.h"
# include "xvin.h"
# include "../logfile/logfile.h"
#include <stdio.h>
#include <NIDAQmx.h>

/* If you include other plug-ins header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "ContAIAO.h"

static TaskHandle  AOtaskHandle=0,AItaskHandle=0;
float64     *AOdata;
float64     *AIdata;
unsigned int t0 = 0;
O_p *op = NULL;
d_s *dsi[3] , *dso[3];
int np = 100;
float64 smplfreq = 100; //250kHz is maximum allowed with one channel


#define DAQmxErrChk(functionCall) if( DAQmxFailed(error=(functionCall)) ) goto Error; else

static int32 GetTerminalNameWithDevPrefix(TaskHandle taskHandle, const char terminalName[], char triggerName[]);
int32 CVICALLBACK EveryNCallback(TaskHandle taskHandle, int32 everyNsamplesEventType, uInt32 nSamples, void *callbackData);
int32 CVICALLBACK DoneCallback(TaskHandle taskHandle, int32 status, void *callbackData);


int ContAIAO()
{
  int32   error=0;
  char    errBuff[2048]={'\0'};
  char    trigName[256];
  t0 =my_uclock();
  win_event("start:\t%d\t%d\n",t0,my_uclock() - t0);
  
  /*********************************************/
  // DAQmx Configure Code
  /*********************************************/
  
  DAQmxErrChk (DAQmxCreateTask("",&AOtaskHandle));
  DAQmxErrChk (DAQmxCreateAOVoltageChan(AOtaskHandle,"Dev1/ao0:2","",-10.0,10.0,DAQmx_Val_Volts,NULL));
  DAQmxErrChk (DAQmxCfgSampClkTiming(AOtaskHandle,"",smplfreq,DAQmx_Val_Rising,DAQmx_Val_ContSamps,np));
  DAQmxErrChk (GetTerminalNameWithDevPrefix(AOtaskHandle,"ao/SampleClock",trigName));
  DAQmxErrChk (DAQmxCreateTask("",&AItaskHandle));
  DAQmxErrChk (DAQmxCreateAIVoltageChan(AItaskHandle,"Dev1/ai0,Dev1/ai1,Dev1/ai8","",DAQmx_Val_RSE,-10.0,10.0,DAQmx_Val_Volts,NULL));
  DAQmxErrChk (DAQmxCfgSampClkTiming(AItaskHandle,trigName,smplfreq,DAQmx_Val_Rising,DAQmx_Val_ContSamps,np));
  
  DAQmxErrChk (DAQmxRegisterEveryNSamplesEvent(AItaskHandle,DAQmx_Val_Acquired_Into_Buffer,np,0,EveryNCallback,NULL));
  DAQmxErrChk (DAQmxRegisterDoneEvent(AOtaskHandle,0,DoneCallback,NULL));
  
  /*********************************************/
  // DAQmx Write Code
  /*********************************************/
  DAQmxErrChk (DAQmxWriteAnalogF64(AOtaskHandle,np,0,10.0,DAQmx_Val_GroupByScanNumber,AOdata,NULL,NULL));

  /*********************************************/
  // DAQmx Start Code
  /*********************************************/
  DAQmxErrChk (DAQmxStartTask(AItaskHandle));
  DAQmxErrChk (DAQmxStartTask(AOtaskHandle));
  win_event("task started:\t%d\t%d\n",my_uclock(),my_uclock() - t0);
  win_event("Acquiring samples continuously. Press Enter to interrupt\n");
  win_event("Read:\tcount\tclock\tdt\tAO\tAI\tTotal:\tAO\tAI\n");

Error:
  if( DAQmxFailed(error) ){
    DAQmxGetExtendedErrorInfo(errBuff,2048);
    if( AOtaskHandle ) {
      /*********************************************/
      // DAQmx Stop Code
      /*********************************************/
      DAQmxStopTask(AOtaskHandle);
      DAQmxClearTask(AOtaskHandle);
      AOtaskHandle = 0;
    }
    if( AItaskHandle ) {
      /*********************************************/
      // DAQmx Stop Code
      /*********************************************/
      DAQmxStopTask(AItaskHandle);
      DAQmxClearTask(AItaskHandle);
      AItaskHandle = 0;
    }
    if( DAQmxFailed(error) )
      win_event("DAQmx Error: %s\n",errBuff);
  }
	return 0;
}

int Stop(void)
{
  int32   error=0;
  char    errBuff[2048]={'\0'};
  
  if(updating_menu_state != 0)	return D_O_K;

  if( DAQmxFailed(error) )
    DAQmxGetExtendedErrorInfo(errBuff,2048);
  if( AOtaskHandle ) {
    /*********************************************/
    // DAQmx Stop Code
    /*********************************************/
    DAQmxStopTask(AOtaskHandle);
    DAQmxClearTask(AOtaskHandle);
    AOtaskHandle = 0;
  }
  if( AItaskHandle ) {
    /*********************************************/
    // DAQmx Stop Code
    /*********************************************/
    DAQmxStopTask(AItaskHandle);
    DAQmxClearTask(AItaskHandle);
    AItaskHandle = 0;
  }
  if( DAQmxFailed(error) )
    win_event("DAQmx Error: %s\n",errBuff);

  return 0;
}

static int32 GetTerminalNameWithDevPrefix(TaskHandle taskHandle, const char terminalName[], char triggerName[])
{
	int32	error=0;
	char	device[256];
	int32	productCategory;
	uInt32	numDevices,i=1;

	DAQmxErrChk (DAQmxGetTaskNumDevices(taskHandle,&numDevices));
	while( i<=numDevices ) {
		DAQmxErrChk (DAQmxGetNthTaskDevice(taskHandle,i++,device,256));
		DAQmxErrChk (DAQmxGetDevProductCategory(device,&productCategory));
		if( productCategory!=DAQmx_Val_CSeriesModule && productCategory!=DAQmx_Val_SCXIModule )
			*triggerName++ = '/';
			strcat(strcat(strcpy(triggerName,device),"/"),terminalName);
			break;
	}

Error:
	return error;
}


int idle_action(O_p *op, DIALOG *d)
{
  pltreg *pr = NULL;
  int channel;
  
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return(win_printf("can't find plot region in idle action"));

  return refresh_plot(pr, UNCHANGED);
}

int32 CVICALLBACK EveryNCallback(TaskHandle taskHandle, int32 everyNsamplesEventType, uInt32 nSamples, void *callbackData)
{
	int32       error=0;
	char        errBuff[2048]={'\0'};
	static int  totalAO=0,totalAI=0;
	int32       readAO = 0,readAI = 0;
	int32 i = 0 , j = 0 ;
	static int count =0;
	count++;
	t0 = my_uclock();

	win_event("1\t%d\t%d\t%d\t%d\t%d\t\t%d\t%d\n",count,t0,my_uclock() - t0,readAO,readAI,totalAO+=readAO,totalAI+=readAI);
	/*********************************************/
	// DAQmx Read Code
	/*********************************************/
	DAQmxErrChk (DAQmxReadAnalogF64(AItaskHandle,np,10.0,DAQmx_Val_GroupByScanNumber,AIdata,3*np,&readAI,NULL));
	win_event("2\t%d\t%d\t%d\t%d\t%d\t\t%d\t%d\n",count,my_uclock(),my_uclock() - t0,readAO,readAI,totalAO+=readAO,totalAI+=readAI);

	//fill_plot
	for(j = 0 ; j < 3 ; j++){
	  for(i = 0 ; i < np ; i++){
	  dso[j]->xd[i] = dsi[j]->xd[i] = (float) i/np;
	  dso[j]->yd[i] = (float) AOdata[3*i+j];
	  dsi[j]->yd[i] = (float) AIdata[3*i+j];
	  }
	}
	
	op->need_to_refresh |=1;

Error:
	if( DAQmxFailed(error) ) {
		DAQmxGetExtendedErrorInfo(errBuff,2048);
		/*********************************************/
		// DAQmx Stop Code
		/*********************************************/
		if( AOtaskHandle ) {
			DAQmxStopTask(AOtaskHandle);
			DAQmxClearTask(AOtaskHandle);
			AOtaskHandle = 0;
		}
		if( AItaskHandle ) {
			DAQmxStopTask(AItaskHandle);
			DAQmxClearTask(AItaskHandle);
			AItaskHandle = 0;
		}
		win_event("DAQmx Error: %s\n",errBuff);
	}
	return 0;
}

int32 CVICALLBACK DoneCallback(TaskHandle taskHandle, int32 status, void *callbackData)
{
	int32   error=0;
	char    errBuff[2048]={'\0'};

	// Check to see if an error stopped the task.
	DAQmxErrChk (status);

Error:
	if( DAQmxFailed(error) ) {
		DAQmxGetExtendedErrorInfo(errBuff,2048);
		DAQmxClearTask(taskHandle);
		if( AItaskHandle ) {
			DAQmxStopTask(AItaskHandle);
			DAQmxClearTask(AItaskHandle);
			AItaskHandle = 0;
		}
		win_event("DAQmx Error: %s\n",errBuff);
	}
	return 0;
}

int do_ContAIAO(void)
{
  register int i,j;
  pltreg *pr = NULL;
  
  if(updating_menu_state != 0)	return D_O_K;
  
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return D_O_K;
  
  AOdata = (float64*) calloc(3*np,sizeof(float64));
  AIdata = (float64*) calloc(3*np,sizeof(float64));
  for(j = 0;j<3;j++){
    for(i = 0;i<np;i++){
      AOdata[3*i+j] = .995*sin((double)i*2.0*PI/np+(double)j*PI/3);
    }
  }
  op = create_and_attach_one_plot(pr,np,np,0);
  dso[0] = op->dat[0];
  dsi[0] = create_and_attach_one_ds(op,np,np,0);
  dso[1] = create_and_attach_one_ds(op,np,np,0);
  dsi[1] = create_and_attach_one_ds(op,np,np,0);
  dso[2] = create_and_attach_one_ds(op,np,np,0);
  dsi[2] = create_and_attach_one_ds(op,np,np,0);
  
  op->op_idle_action = idle_action;
  
  ContAIAO();
  
  return D_O_K;
}


MENU *ContAIAO_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"ContAIAO", do_ContAIAO,NULL,0,NULL);
	add_item_to_menu(mn,"Stop ContAIAO", Stop,NULL,0,NULL);
	return mn;
}

int	ContAIAO_main(int argc, char **argv)
{
	add_plot_treat_menu_item ( "ContAIAO", NULL, ContAIAO_menu(), 0, NULL);
	return D_O_K;
}

int	ContAIAO_unload(int argc, char **argv)
{
	remove_item_to_menu(plot_treat_menu, "ContAIAO", NULL, NULL);
	return D_O_K;
}
#endif

