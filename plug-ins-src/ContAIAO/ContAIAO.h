#ifndef _CONTAIAO_H_
#define _CONTAIAO_H_

PXV_FUNC(int, do_ContAIAO, (void));
PXV_FUNC(MENU*, ContAIAO_menu, (void));
PXV_FUNC(int, ContAIAO_main, (int argc, char **argv));
#endif

