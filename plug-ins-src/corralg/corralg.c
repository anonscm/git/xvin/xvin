#ifndef _CORRALG_C_
#define _CORRALG_C_


# include "allegro.h"
# include "xvin.h"
//#include <math.h>

/* If you include other plug-ins header do it here*/ 

/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "corralg.h"

/* int corr_algorithm_one(unsigned long long *a1,unsigned long long *a2,int sz1,int sz2,unsigned long long *cfa,int szcf,int ncasc) */
/* { */
/*   mt = (unsigned long long *)calloc(Nphot,sizeof(unsigned long long)); */
/*   for(i=0;i<Nphot;i++){ */
/*     status = SPC_get_photon (stream_hndl,&phot_info ); */
/*     if(status != 0) {show_bh_error(status); break;} */
/*     if(phot_info.rout_chan == 0 ){ */
/*       mt[i] = (((unsigned long long)(phot_info.mtime_hi)) << 32 | (unsigned long long)phot_info.mtime_lo); */
/*       //mt[i] = (unsigned long long)(phot_info.mtime_lo); */
/*       if(mt[i]>limit){ */
/* 	//here insert algorithm for corralg function. */
/* 	k=i-1; */
/* 	AL[0]++; */
/* 	for(j=1;j<=n;j++) */
/* 	  { */
/* 	    d=mt[i]-DL[j]; */
/* 	    for(l=k;mt[k]>d;k--); */
/* 	    AL[j] += l-k; */
/* 	  } */
/* 	/\*if(i%Nphref == 0){ */
/* 	  T = mt[i]; */
/* 	  for(j=1;j<=n;j++){ */
/* 	    ds->yd[j] = (T-limit)*AL[j]/AL[0]/AL[0]/(DL[j]-DL[j-1]); */
/* 	    } */
/* 	  op->need_to_refresh = 1; */
/* 	  broadcast_dialog_message(MSG_DRAW,0); */
/* 	  win_printf("mt[%d] = %d",i,mt[i]); */
/* 	  }*\/ */
/*       } */
/*       else */
/* 	i--; */
/*     } */
/*     else */
/*       i--; */
/*     //    else if (phot_info.rout_chan == 1) */
/*   } */
/* } */

/* int generate_random_toa(unsigned long long t,int szt){ */

/*   register int i=0; */
/*   for (i=0; i<szt; i++) { */
/*     t[i] = (unsigned long long) gsl_ran_flat(r, (double)x1, (double)x2); */
/*   } */

/* } */

int corr_algorithm_two(unsigned long long *t1,int sz1,
		       unsigned long long *t2,int sz2,
		       unsigned long long *cfa,int szcfa,
		       unsigned long long *tc,
		       int maxcorrtime)
{

  register int i=0,k=0, K=0;
  int Nc = 8; //Number of bins in a cascade
  int cost = 1;
  int tmax = 0; //verificare se e' un int!
  unsigned long long diff=0;
  
  unsigned int *f1=NULL,*f2=NULL;
  f1 = (unsigned int *)calloc(sz1,sizeof(unsigned int));
  f2 = (unsigned int *)calloc(sz2,sizeof(unsigned int));
  //set all elements to 1 in frequency arrays
  for(i=0;i<sz1;i++){
    f1[i] = 1;
  }
  for(i=0;i<sz2;i++){
    f2[i] = 1;
  }

  //need allocate time array!!!!
  //need to allocate it in bunches of Nc
  K=0;
  tc[K] = 1;
  do{
    if(K==0){
      tc[K*Nc+1] = tc[K*Nc]+1;
      tc[K*Nc+2] = tc[K*Nc]+2;
      tc[K*Nc+3] = tc[K*Nc]+3;
      tc[K*Nc+4] = tc[K*Nc]+4;
      tc[K*Nc+5] = tc[K*Nc]+5;
      tc[K*Nc+6] = tc[K*Nc]+6;
      tc[K*Nc+7] = tc[K*Nc]+7;
    }
    else if(K == 1){
      tc[K*Nc] = tc[K*Nc-1]+1;
      tc[K*Nc+1] = tc[K*Nc]+1;
      tc[K*Nc+2] = tc[K*Nc]+2;
      tc[K*Nc+3] = tc[K*Nc]+3;
      tc[K*Nc+4] = tc[K*Nc]+4;
      tc[K*Nc+5] = tc[K*Nc]+5;
      tc[K*Nc+6] = tc[K*Nc]+6;
      tc[K*Nc+7] = tc[K*Nc]+7;
    }
    else{
      cost <<= 1;
      tc[K*Nc] = tc[K*Nc-1]+ cost;
      tc[K*Nc+1] = tc[K*Nc]+ cost;
      tc[K*Nc+2] = tc[K*Nc]+ 2*cost;
      tc[K*Nc+3] = tc[K*Nc]+ 3*cost;
      tc[K*Nc+4] = tc[K*Nc]+ 4*cost;
      tc[K*Nc+5] = tc[K*Nc]+ 5*cost;
      tc[K*Nc+6] = tc[K*Nc]+ 6*cost;
      tc[K*Nc+7] = tc[K*Nc]+ 7*cost;
    } 
    K++;
  }
  while(tc[(K-1)*Nc+7]< maxcorrtime);



  //try first cascade K = 0:
  for(i=0;i<sz1;i++){
    while((diff=t2[k]-t1[i])<Nc && k < sz2){
      cfa[diff]+=f1[i]*f2[k];
      k++;
    }
  }

  //now from second cascade on:
  K = 1;
  tmax = 16;
  while(tmax < maxcorrtime){
    for(i=0;i<sz1;i++){
      while((diff=t2[k]-t1[i])<2*Nc && (diff=t2[k]-t1[i])>= Nc && k < sz2 ){
	cfa[diff+(K-1)*Nc-1]+=f1[i]*f2[k];
	k++;
      }
    }
    
    //divide all by two
    for(i=0;i<sz1;i++){
      t1[i] >>= 1;
      if((i!=0) && (t1[i]==t1[i-1])){
	f1[i]=f1[i-1]+f1[i];
	f1[i-1]=0;
      }
    }
    for(i=0;i<sz2;i++){
      t2[i] >>= 1;
      if((i!=0) && (t2[i]==t2[i-1])){
	f2[i]=f2[i-1]+f2[i];
	f2[i-1]=0;
      }
    }
    tmax <<= 1;
  }

  //need to normalize!
  free(f1);
  free(f2);
  return D_O_K;
}

int do_corralg(void)
{ 
  O_p *op = NULL, *opcorr = NULL;
  d_s *ds = NULL, *dscorr = NULL;
  pltreg *pr = NULL;
  
  int sz1 = 1000;
  int sz2 = 1000;
  unsigned long long maxcorrtime = 200;
  unsigned long long total_aq_time = 0;
  unsigned long long t = 8;
  int szcfa = 0;
  unsigned long long *t1 = NULL, *t2 =NULL, *cfa = NULL, *tc = NULL;

  register int i=0;

  if(updating_menu_state != 0) return D_O_K;


  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
    return win_printf_OK("cannot find pltreg");

  sz1 = ds->nx;
  sz2 = ds->nx;

  //total acquisition time:
  //(here I am assuming I measure macro times and not intervals)
  total_aq_time = ds->yd[ds->nx-1] - ds->yd[0];
  maxcorrtime = total_aq_time/5;

  if(maxcorrtime<8){
    win_printf("this dataset is not good!!!\n"
	       "set maxcorrtime to 200");
    maxcorrtime = 200;
  }
  
  //need to calculate szcfa:
  while(t<maxcorrtime){
    t <<=1;
    szcfa +=8;
  }
  
  win_printf_OK("2");
  t1 = (unsigned long long *)calloc(sz1,sizeof(unsigned long long));
  t2 = (unsigned long long *)calloc(sz2,sizeof(unsigned long long));
  tc = (unsigned long long *)calloc(szcfa,sizeof(unsigned long long));
  cfa = (unsigned long long *)calloc(szcfa,sizeof(unsigned long long));

 
  opcorr = create_and_attach_one_plot(pr,szcfa,szcfa,0);
  dscorr = opcorr->dat[0];
  
  win_printf_OK("3");

  for(i = 0; i<sz1; i++){
    t1[i] = (unsigned long long) ds->yd[i];
  }
 
  for(i = 0; i<sz2; i++){
    t2[i] = (unsigned long long) ds->yd[i];
  }
 
  corr_algorithm_two(t1,sz1,t2,sz2,cfa,szcfa,tc,maxcorrtime);
  
  for(i = 0; i<szcfa; i++){
    dscorr->xd[i] = (float) tc[i];
    dscorr->yd[i] = (float) cfa[i];
  }
 
  opcorr->need_to_refresh = 1;	
  /* redisplay the entire plot */
  refresh_plot(pr, UNCHANGED);

  

  return 0;
}


MENU *corralg_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"do corralg", do_corralg,NULL,0,NULL);
	return mn;
}

int	corralg_main(int argc, char **argv)
{
	add_plot_treat_menu_item ( "corralg", NULL, corralg_plot_menu(), 0, NULL);
	return D_O_K;
}

int	corralg_unload(int argc, char **argv)
{
	remove_item_to_menu(plot_treat_menu, "corralg", NULL, NULL);
	return D_O_K;
}

#endif
