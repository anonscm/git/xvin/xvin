#ifndef _POLYMERWLC_H_
#define _POLYMERWLC_H_

PXV_FUNC(int, do_polymerWLC_rescale_plot, (void));
PXV_FUNC(MENU*, polymerWLC_plot_menu, (void));
PXV_FUNC(int, do_polymerWLC_rescale_data_set, (void));
PXV_FUNC(int, polymerWLC_main, (int argc, char **argv));
#endif

