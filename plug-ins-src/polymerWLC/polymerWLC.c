/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _POLYMERWLC_C_
#define _POLYMERWLC_C_

# include "allegro.h"
# include "xvin.h"

/* If you include other regular header do it here*/


/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "../nrutil/nrutil.h"

//place here all headers from plugins already compiled


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
//place here other headers of this plugin
# include "allegro.h"
# include "xvin.h"
# include "float.h"
#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multimin.h>

/* If you include other regular header do it here*/


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "polymerWLC.h"


// mFJC z = L0 (coth(F*2*xi/kBT) - (kBT/F*2xi))(1+F/S)

# if !defined(PLAYITSAM) && !defined(PIAS) && !defined(PICOTWIST)
double marko_siggia_improved( double x)
{
	double tmp;
	double a_2 = -5.164228e-01, a_3 = -2.737418e+00, a_4 = 1.607497e+01;
	double a_5 = -3.887607e+01, a_6 = 3.949944e+01, a_7 = -1.417718e+01;


	tmp = (x -.25 + .25*(1/((1-x)*(1-x))));
	tmp += a_2 * x * x;
	tmp += a_3 * x * x * x;
	tmp += a_4 * x * x * x * x;
	tmp += a_5 * x * x * x * x * x;
	tmp += a_6 * x * x * x * x * x * x;
	tmp += a_7 * x * x * x * x * x * x * x;
	return tmp;
}



float	find_best_xi_at_fixed_L(d_s *dsi, float T, float xi, float L, float *E)
{
  register int i, j;
  static int no_more = 0;
  double fy, f, x, y, fx, er;

  for (i = 0; i < dsi->nx; i++)
    {
      x = dsi->xd[i];
      if (x < 0 || x > L)
	{
	  if (no_more != WIN_CANCEL) no_more = win_printf("cannot fit \n"
			  "L = %g at %d is too small or negative!",x,i);
	  return -FLT_MAX;
	}
      y = dsi->yd[i];
      if (y <= 0)
	{
	  if (no_more != WIN_CANCEL) no_more = win_printf("cannot fit \nforce  = %g at %d is negative!",y,i);
	  return -FLT_MAX;
	}

    }

  for (i = 0, f = fy = 0; i < dsi->nx; i++)
    {
      er = (dsi->ye != NULL) ? dsi->ye[i] : dsi->yd[i]/10;
      x = dsi->xd[i]/L;
      y = dsi->yd[i] * 1e-12;
      y /=  1.38e-23 * T/(xi*1e-9);
      er *= 1e-12;
      er /= 1.38e-23 * T/(xi*1e-9);
      fx = marko_siggia_improved(x);
      if (y < 0)	return win_printf("cannot treat negative force");
      if (er > 0)
	{
	  fy += y*fx/(er*er);
	  f += (fx*fx)/(er*er);
	}
    }
  xi = xi * f/fy;
  for (i = 0, j = 0, *E = 0; i < dsi->nx; i++)
    {
      x = dsi->xd[i]/L;
      y = dsi->yd[i];
      fx = marko_siggia_improved(x);
      fx *= 1.38e-11 * T/(xi*1e-9);
      er = (dsi->ye != NULL) ? dsi->ye[i] : dsi->yd[i]/10;
      f = y - fx;
      if (er > 0)
	{
	  *E += (f*f)/(er*er);
	  j++;
	}
    }
  return (float)xi;
}


int	do_fit_xi_auto_with_er(void)
{
  register int i;
  char st[256] = {0};
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *dsi = NULL, *dsd = NULL;
  float x, y, tmp, E = 0, E1 = 0, dl = .1, lmin, lmax;
  int nf = 1024;
  static float T = 298;
  static float xi = 50;	/* nm*/
  static float L = 16.2;	/* microns */



  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  i = win_scanf("Fit WLC (Marko-Siggia improved)\nthe pesistence length and\n"
		"Length of molecule %12f \npossible \\xi  value %12f\n",&L,&xi);
  if (i == WIN_CANCEL)	return OFF;
  for (i = 0, lmin = 0, lmax = fabs(dsi->xd[0]); i < dsi->nx; i++)
    {
      x = fabs(dsi->xd[i]);
      lmin = (x > lmin) ? x : lmin;
      lmax = (x > lmax) ? x : lmax;
    }
  L = (L < lmin) ? lmin +.01 : L;

  xi = find_best_xi_at_fixed_L(dsi, T,  xi, L, &E);
  if (xi == -FLT_MAX) return win_printf("could not fit");
  for (i = 0; fabs(dl) > .000125 && i < 1024; i++)
    {
      L += dl;
      L = (L < lmin) ? lmin +.01 : L;
      xi = find_best_xi_at_fixed_L(dsi,	T,  xi, L, &E1);
      if (xi == -FLT_MAX) return win_printf("could not fit");
      if (E1 > E)   dl = -dl/2;
      E = E1;
    }

  i = win_printf("%d iter \\xi = %g, the energy is %g",i,xi,E);
  if (i == WIN_CANCEL)	return OFF;
  if ((dsd = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create data set");
  for (i = 0, tmp = lmax/nf ; i < dsd->nx; i++, tmp += lmax/nf)
    {
      dsd->xd[i] = (float)(tmp);
      y = marko_siggia_improved(tmp/L);
      y *= 1.38e-11 * T/(xi*1e-9);
      dsd->yd[i] = y;
    }


  sprintf(st,"\\fbox{\\pt8\\stack{{\\sl Excact WLM model}"
	  "{L =  %g \\mu m}{\\xi = %g nm}{\\chi^2 = %g, n = %d}}}",L,xi,E,dsi->nx-2);
  push_plot_label(op, op->x_lo + (op->x_hi - op->x_lo)/4,
		  op->y_hi - (op->y_hi - op->y_lo)/4, st, USR_COORD);
  return refresh_plot(pr,pr->cur_op);
}


int	do_fit_xi_auto_with_er_show_E(void)
{
  register int i;
  char st[256] = {0};
  pltreg *pr = NULL;
  O_p *op = NULL, *opn = NULL;
  d_s *dsi = NULL, *dsd = NULL, *dsn = NULL;
  float x, y, tmp, E = 0, E1 = 0, lmin, lmax;
  int nf = 1024;
  static float T = 298;
  static float xi = 50;	/* nm*/
  static float L = 16.2;	/* microns */



  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  i = win_scanf("Fit WLC model to data y->Force, x-> extension\n"
		"find the pesistence length \\xi and\n"
		"the Length of molecule %12f \npossible \\xi  value %12f\n",&L,&xi);
  if (i == WIN_CANCEL)	return OFF;
  for (i = 0, lmin = 0, lmax = fabs(dsi->xd[0]); i < dsi->nx; i++)
    {
      x = fabs(dsi->xd[i]);
      lmin = (x > lmin) ? x : lmin;
      lmax = (x > lmax) ? x : lmax;
    }
  L = (L < lmin) ? lmin +.01 : L;

  if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  dsn = opn->dat[0];

  /* now we must do some house keeping */
  set_ds_source(dsn,"minimum");
  set_plot_x_title(opn, "L");
  set_plot_y_title(opn, "\\chi^2");


  xi = find_best_xi_at_fixed_L(dsi, T,  xi, L, &E);
  for (i = 0; i < nf; i++)
    {
      L = lmax + 4*(i+1)*lmax/nf;
      xi = find_best_xi_at_fixed_L(dsi,	T,  xi, L, &E1);
      if (E1 < E)
	{
	  E = E1;
	  lmin = L;
	}
      dsn->xd[i] = L;
      dsn->yd[i] = E1;
    }
  L = lmin;
  i = win_printf("%d iter \\xi = %g, the energy is %g",i,xi,E);
  if (i == WIN_CANCEL)	return OFF;
  if ((dsd = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create data set");
  for (i = 0, tmp = lmax/nf ; i < dsd->nx; i++, tmp += lmax/nf)
    {
      dsd->xd[i] = (float)(tmp);
      y = marko_siggia_improved(tmp/L);
      y *= 1.38e-11 * T/(xi*1e-9);
      dsd->yd[i] = y;
    }


  sprintf(st,"\\fbox{\\pt8\\stack{{\\sl Mako-Siggia improved WLC model}"
	  "{L =  %g \\mu m}{\\xi = %g nm}{\\chi^2 = %g, n = %d}}}",L,xi,E,dsi->nx-2);
  push_plot_label(op, op->x_lo + (op->x_hi - op->x_lo)/4,
		  op->y_hi - (op->y_hi - op->y_lo)/4, st, USR_COORD);
  return refresh_plot(pr,pr->cur_op);
}

# endif  // # if !defined(PLAYITSAM) && !defined(PIAS) && !defined(PICOTWIST)

// FJC z = L0 (coth(F*2*xi/kBT)
// F along x, extension along y
float	find_best_L0_at_fixed_xi_for_FJC_simple(d_s *dsi, float T, float xi, float L, float *E)
{
  register int i, j;
  double fy, f, x, y, fx, er, tmp;
  static int verbose = WIN_CANCEL;

  for (i = 0; i < dsi->nx; i++)
    {
      x = dsi->xd[i];
      y = dsi->yd[i];
      if (x <= 0)
	return win_printf("cannot fit \nforce  = %g at %d is negative!",y,i);

    }

  for (i = 0, f = fy = 0; i < dsi->nx; i++)
    {
      er = (dsi->ye != NULL) ? dsi->ye[i] : dsi->yd[i]/10;
      x = dsi->xd[i] * 1e-12;
      tmp = x * 2 * xi * 1e-9;
      tmp /=  1.38e-23 * T;
      fx = ((cosh(tmp)/sinh(tmp)) - ((double)1/tmp));
      y = dsi->yd[i];
      if (y < 0)	return win_printf("cannot treat negative force");
      if (er > 0)
	{
	  fy += y*fx/(er*er);
	  f += (fx*fx)/(er*er);
	}
    }
  if (f != 0)    L = fy/f;
  for (i = 0, j = 0, *E = 0; i < dsi->nx; i++)
    {
      x = dsi->xd[i] * 1e-12;
      y = dsi->yd[i];
      tmp = x * 2 * xi * 1e-9;
      tmp /=  1.38e-23 * T;
      fx = L*((cosh(tmp)/sinh(tmp)) - ((double)1/tmp));
      er = (dsi->ye != NULL) ? dsi->ye[i] : dsi->yd[i]/10;
      f = y - fx;
      if (er > 0)
	{
	  *E += (f*f)/(er*er);
	  j++;
	}
    }
  if (verbose != WIN_CANCEL) verbose = win_printf("xi = %g l = %g, E = %g",xi,L,*E);
  return (float)L;
}



int	do_fit_xL_auto_with_er_FJC_simple(void)
{
  register int i;
  char st[256] = {0};
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *dsi = NULL, *dsd = NULL;
  float x, tmp, E = 0, E1 = 0,  dl = .1, fmin, fmax = 1, fx;
  int nf = 1024;
  static float T = 298;
  static float xi = 50;	/* nm*/
  static float L = 16.2;	/* microns */



  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  i = win_scanf("Fit FJC model wi\n"
		"z = L(coth(F*2*\\xi /kBT) - (kBT/F*2\\xi ))\n"
		"x->Force, y->Extension, error on extension\n"
		"Fit the pesistence length \\xi and the\n"
		"Length L of molecule \n"
		"possible \\xi  value %7f\n"
		,&xi);
  if (i == WIN_CANCEL)	return OFF;

  for (i = 0; i < dsi->nx; i++)
    {
      x = fabs(dsi->xd[i]);
      fmin = (i == 0 || x < fmin) ? x : fmin;
      fmax = (i == 0 || x > fmax) ? x : fmax;
    }

  L = find_best_L0_at_fixed_xi_for_FJC_simple(dsi, T,  xi, L, &E);
  dl = L/10;
  for (i = 0; fabs(dl) > .0000125*L && i < 1024; i++)
    {
      xi += dl;
      L = find_best_L0_at_fixed_xi_for_FJC_simple(dsi, T,  xi, L, &E1);
      if (E1 > E)   dl = -dl/2;
      E = E1;
    }

  i = win_printf("%d iter, \\xi  = %g, L = %g\n"
		 " the energy is %g\nfmax = %g\n",i,xi,L,E,fmax);
  if (i == WIN_CANCEL)	return OFF;
  if ((dsd = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create data set");
  for (i = 0, tmp = fmax/nf ; i < dsd->nx; i++, tmp += fmax/nf)
    {
      dsd->xd[i] = (float)(tmp);
      x = tmp * 1e-12;
      x = x * 2 * xi * 1e-9;
      x /=  1.38e-23 * T;
      fx = L*((cosh(x)/sinh(x)) - ((double)1/x));
      dsd->yd[i] = fx;
    }


  sprintf(st,"\\fbox{\\pt8\\stack{{\\sl simple FJC model}"
	  "{y = L.[coth(f) - f^{-1}]}"
	  "{f = F*2*\\xi /kBT}"
	  "{L =  %g \\mu m}{\\xi = %g nm}{\\chi^2 = %g, n = %d}}}",L,xi,E,dsi->nx-2);
  set_ds_plot_label(dsd, op->x_lo + (op->x_hi - op->x_lo)/4,
		    op->y_hi - (op->y_hi - op->y_lo)/4, USR_COORD, "%s", st);
  return refresh_plot(pr,pr->cur_op);
}


// mFJC z = L0 (coth(F*2*xi/kBT) - (kBT/F*2xi))(1+F/S)
// F along x, extension along y
float	find_best_L0_at_fixed_xi_for_FJCm(d_s *dsi, float T, float xi, float L, float S, float *E)
{
  register int i, j;
  double fy, f, x, y, fx, er, xs, tmp;
  static int verbose = WIN_CANCEL;

  for (i = 0; i < dsi->nx; i++)
    {
      x = dsi->xd[i];
      y = dsi->yd[i];
      if (y <= 0)
	return win_printf("cannot fit \nforce  = %g at %d is negative!",y,i);

    }

  for (i = 0, f = fy = 0; i < dsi->nx; i++)
    {
      er = (dsi->ye != NULL) ? dsi->ye[i] : dsi->yd[i]/10;
      xs = dsi->xd[i]/S;
      x = dsi->xd[i] * 1e-12;
      tmp = x * 2 * xi * 1e-9;
      tmp /=  1.38e-23 * T;
      fx = ((cosh(tmp)/sinh(tmp)) - ((double)1/tmp))*(1+xs);
      y = dsi->yd[i];
      if (y < 0)	return win_printf("cannot treat negative force");
      if (er > 0)
	{
	  fy += y*fx/(er*er);
	  f += (fx*fx)/(er*er);
	}
    }
  if (f != 0)    L = fy/f;
  for (i = 0, j = 0, *E = 0; i < dsi->nx; i++)
    {
      x = dsi->xd[i] * 1e-12;
      xs = dsi->xd[i]/S;
      y = dsi->yd[i];
      tmp = x * 2 * xi * 1e-9;
      tmp /=  1.38e-23 * T;
      fx = L*((cosh(tmp)/sinh(tmp)) - ((double)1/tmp))*(1+xs);
      er = (dsi->ye != NULL) ? dsi->ye[i] : dsi->yd[i]/10;
      f = y - fx;
      if (er > 0)
	{
	  *E += (f*f)/(er*er);
	  j++;
	}
    }
  if (verbose != WIN_CANCEL) verbose = win_printf("xi = %g l = %g, E = %g",xi,L,*E);
  return (float)L;
}



int	do_fit_xL_auto_with_er_FJC(void)
{
  register int i, k;
  char st[256] = {0};
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *dsi = NULL, *dsd = NULL;
  float x, tmp, E = 0, E1 = 0, E2 = 0, dl = .1, fmin, fmax = 1, xs, fx, ds;
  int nf = 1024;
  static float T = 298;
  static float xi = 50;	/* nm*/
  static float L = 16.2;	/* microns */
  static float S = 300;



  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  i = win_scanf("Fit FJC model with elastic correction\n"
		"z = L(coth(F*2*\\xi /kBT) - (kBT/F*2\\xi ))(1+F/S)\n"
		"x->Force, y->Extension, error on extension\n"
		"Fit the pesistence length \\xi the\n"
		"Length L of molecule and the elastic correction S \n"
		"possible \\xi  value %7f\n"
		"S = %7f\n",&xi,&S);
  if (i == WIN_CANCEL)	return OFF;

  for (i = 0; i < dsi->nx; i++)
    {
      x = fabs(dsi->xd[i]);
      fmin = (i == 0 || x < fmin) ? x : fmin;
      fmax = (i == 0 || x > fmax) ? x : fmax;
    }

  L = find_best_L0_at_fixed_xi_for_FJCm(dsi, T,  xi, L, S, &E);
  dl = L/10;
  for (i = 0; fabs(dl) > .0000125*L && i < 1024; i++)
    {
      xi += dl;
      L = find_best_L0_at_fixed_xi_for_FJCm(dsi, T,  xi, L, S, &E1);
      if (E1 > E)   dl = -dl/2;
      E = E1;
    }
  for (k = 0, E2 = E, ds = S/5; fabs(ds) > .0001*S && k < 1024; k++)
    {
      S += ds;
      L = find_best_L0_at_fixed_xi_for_FJCm(dsi, T,  xi, L, S, &E);
      dl = L/10;
      for (i = 0; fabs(dl) > .0000125*L && i < 1024; i++)
	{
	  xi += dl;
	  L = find_best_L0_at_fixed_xi_for_FJCm(dsi, T,  xi, L, S, &E1);
	  if (E1 > E)   dl = -dl/2;
	  E = E1;
	}
      //win_printf("%d-%d iter, S = %g ds %g \n\\xi = %g, L = %g\n the energy is %g\nfmax = %g\n",k,i,S,ds,xi,L,E,fmax);
      if (E > E2)   ds = -ds/2;
      E2 = E;

    }

  i = win_printf("%d-%d iter, S = %g ds %g \n\\xi  = %g, L = %g\n"
		 " the energy is %g\nfmax = %g\n",k,i,S,ds,xi,L,E,fmax);
  if (i == WIN_CANCEL)	return OFF;
  if ((dsd = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create data set");
  for (i = 0, tmp = fmax/nf ; i < dsd->nx; i++, tmp += fmax/nf)
    {
      dsd->xd[i] = (float)(tmp);
      xs = dsd->xd[i]/S;
      x = tmp * 1e-12;
      x = x * 2 * xi * 1e-9;
      x /=  1.38e-23 * T;
      fx = L*((cosh(x)/sinh(x)) - ((double)1/x))*(1+xs);
      dsd->yd[i] = fx;
    }


  sprintf(st,"\\fbox{\\pt8\\stack{{\\sl Extensible FJC model}"
	  "{y = L.[coth(f) - f^{-1})](1+F/S)}"
	  "{f = 2.F.\\xi /k_BT}"
	  "{L =  %g \\mu m}{\\xi = %g nm}{S = %g pN}"
	  "{\\chi^2 = %g, n = %d}}}",L,xi,S,E,dsi->nx-2);
  set_ds_plot_label(dsd, op->x_lo + (op->x_hi - op->x_lo)/4,
		    op->y_hi - (op->y_hi - op->y_lo)/4, USR_COORD, "%s", st);
  return refresh_plot(pr,pr->cur_op);
}



// mFJC z = L0 [(coth(F*2*xi/kBT) - (kBT/F*2xi))+F/S]
// F along x, extension along y
float	find_best_L0_at_fixed_xi_for_FJCm_SC(d_s *dsi, float T, float xi, float L, float S, float *E)
{
  register int i, j;
  double fy, f, x, y, fx, er, xs, tmp;
  static int verbose = WIN_CANCEL;

  for (i = 0; i < dsi->nx; i++)
    {
      x = dsi->xd[i];
      y = dsi->yd[i];
      if (y <= 0)
	return win_printf("cannot fit \nforce  = %g at %d is negative!",y,i);

    }

  for (i = 0, f = fy = 0; i < dsi->nx; i++)
    {
      er = (dsi->ye != NULL) ? dsi->ye[i] : dsi->yd[i]/10;
      xs = dsi->xd[i]/S;
      x = dsi->xd[i] * 1e-12;
      tmp = x * 2 * xi * 1e-9;
      tmp /=  1.38e-23 * T;
      fx = ((cosh(tmp)/sinh(tmp)) - ((double)1/tmp))+(xs);
      y = dsi->yd[i];
      if (y < 0)	return win_printf("cannot treat negative force");
      if (er > 0)
	{
	  fy += y*fx/(er*er);
	  f += (fx*fx)/(er*er);
	}
    }
  if (f != 0)    L = fy/f;
  for (i = 0, j = 0, *E = 0; i < dsi->nx; i++)
    {
      x = dsi->xd[i] * 1e-12;
      xs = dsi->xd[i]/S;
      y = dsi->yd[i];
      tmp = x * 2 * xi * 1e-9;
      tmp /=  1.38e-23 * T;
      fx = L*(((cosh(tmp)/sinh(tmp)) - ((double)1/tmp))+(xs));
      er = (dsi->ye != NULL) ? dsi->ye[i] : dsi->yd[i]/10;
      f = y - fx;
      if (er > 0)
	{
	  *E += (f*f)/(er*er);
	  j++;
	}
    }
  if (verbose != WIN_CANCEL) verbose = win_printf("xi = %g l = %g, E = %g",xi,L,*E);
  return (float)L;
}



int	do_fit_xL_auto_with_er_FJC_SC(void)
{
  register int i, k;
  char st[256] = {0};
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *dsi = NULL, *dsd = NULL;
  float x, tmp, E = 0, E1 = 0, E2 = 0, dl = .1, fmin, fmax = 1, xs, fx, ds;
  int nf = 1024;
  static float T = 298;
  static float xi = 50;	/* nm*/
  static float L = 16.2;	/* microns */
  static float S = 300;



  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  i = win_scanf("Fit FJC model with elastic correction\n"
		"z = L[(coth(F*2*\\xi /kBT) - (kBT/F*2\\xi ))+F/S]\n"
		"x->Force, y->Extension, error on extension\n"
		"Fit the pesistence length \\xi the\n"
		"Length L of molecule and the elastic correction S \n"
		"possible \\xi  value %7f\n"
		"S = %7f\n",&xi,&S);
  if (i == WIN_CANCEL)	return OFF;

  for (i = 0; i < dsi->nx; i++)
    {
      x = fabs(dsi->xd[i]);
      fmin = (i == 0 || x < fmin) ? x : fmin;
      fmax = (i == 0 || x > fmax) ? x : fmax;
    }

  L = find_best_L0_at_fixed_xi_for_FJCm_SC(dsi, T,  xi, L, S, &E);
  dl = L/10;
  for (i = 0; fabs(dl) > .0000125*L && i < 1024; i++)
    {
      xi += dl;
      L = find_best_L0_at_fixed_xi_for_FJCm_SC(dsi, T,  xi, L, S, &E1);
      if (E1 > E)   dl = -dl/2;
      E = E1;
    }
  for (k = 0, E2 = E, ds = S/5; fabs(ds) > .0001*S && k < 1024; k++)
    {
      S += ds;
      L = find_best_L0_at_fixed_xi_for_FJCm_SC(dsi, T,  xi, L, S, &E);
      dl = L/10;
      for (i = 0; fabs(dl) > .0000125*L && i < 1024; i++)
	{
	  xi += dl;
	  L = find_best_L0_at_fixed_xi_for_FJCm_SC(dsi, T,  xi, L, S, &E1);
	  if (E1 > E)   dl = -dl/2;
	  E = E1;
	}
      //win_printf("%d-%d iter, S = %g ds %g \n\\xi = %g, L = %g\n the energy is %g\nfmax = %g\n",k,i,S,ds,xi,L,E,fmax);
      if (E > E2)   ds = -ds/2;
      E2 = E;

    }

  i = win_printf("%d-%d iter, S = %g ds %g \n\\xi  = %g, L = %g\n"
		 " the energy is %g\nfmax = %g\n",k,i,S,ds,xi,L,E,fmax);
  if (i == WIN_CANCEL)	return OFF;
  if ((dsd = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create data set");
  for (i = 0, tmp = fmax/nf ; i < dsd->nx; i++, tmp += fmax/nf)
    {
      dsd->xd[i] = (float)(tmp);
      xs = dsd->xd[i]/S;
      x = tmp * 1e-12;
      x = x * 2 * xi * 1e-9;
      x /=  1.38e-23 * T;
      fx = L*(((cosh(x)/sinh(x)) - ((double)1/x))+(xs));
      dsd->yd[i] = fx;
    }


  sprintf(st,"\\fbox{\\pt8\\stack{{\\sl Extensible FJC model}"
	  "{y = L.[(coth(f) - f^{-1})+F/S]}"
	  "{f = 2.F.\\xi /k_BT}"
	  "{L =  %g \\mu m}{\\xi = %g nm}{S = %g pN}"
	  "{\\chi^2 = %g, n = %d}}}",L,xi,S,E,dsi->nx-2);
  set_ds_plot_label(dsd, op->x_lo + (op->x_hi - op->x_lo)/4,
		    op->y_hi - (op->y_hi - op->y_lo)/4, USR_COORD, "%s", st);
  return refresh_plot(pr,pr->cur_op);
}



int	fjc_curve_elastic(void)
{
  register int i;
  static int nx = 1024;
  double  tmp, y, y1;
  static float T = 298;
  static float S = 600;
  static float xi = 50;	/* nm*/
  static float L = 16.2;	/* microns */
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *ds = NULL;

  if(updating_menu_state != 0)	return D_O_K;

  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return win_printf("cannot find stuff in acreg!");

  i = win_scanf("Draw FJC with elasticity\n\n"
		"with \\xi  (in nm) = %10f\n"
		"T (in ^{\\pt5 \\oc}) = %10f\n"
		"L (in \\mu m) = %10f\n"
		"S (in pN) = %10f\n"
		"with %10d number of points\n",&xi,&T,&L,&S,&nx);

  if (i == WIN_CANCEL)	return OFF;

  op = create_and_attach_one_plot(pr, nx, nx, 0);
  if (op == NULL)		return win_printf("cannot create plot !");
  ds = op->dat[0];

  for (i = 0, tmp = 0.001 ; i < nx; i++, tmp *= 1.01)
    {
      y = 1.38e-23 * T/(2*xi*1e-9);
      y /= tmp;
      y *= 1e12;
      ds->yd[i] = tmp;
      y1 = (double)1/y;
      //if ((i*20)%nx == 0)	win_printf("y = %f y1 = %f",y,y1);
      ds->xd[i] = L * (cosh(y1)/sinh(y1) - y);
      if (S > 0) ds->xd[i] *= (1+tmp/S);
    }



  set_plot_title(op, "\\stack{{FJC model}"
		 "{\\pt8 \\xi  = %g nm, T = %g,}"
		 "{L = %g \\mu m, S = %g pN}}",xi,T,L,S);
  set_plot_x_title(op, "Allongement (\\mu m)");
  set_plot_y_title(op, "Force (pN)");
  return refresh_plot(pr,pr->n_op-1);
}

int	fjc_curve_elastic_SC(void)
{
  register int i;
  static int nx = 1024;
  double  tmp, y, y1;
  static float T = 298;
  static float S = 600;
  static float xi = 50;	/* nm*/
  static float L = 16.2;	/* microns */
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *ds = NULL;

  if(updating_menu_state != 0)	return D_O_K;

  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return win_printf("cannot find stuff in acreg!");

  i = win_scanf("Draw FJC with elasticity SC\n\n"
		"z = L0 [(coth(F*2*xi/kBT) - (kBT/F*2xi))+F/S]\n"
		"with \\xi  (in nm) = %10f\n"
		"T (in ^{\\pt5 \\oc}) = %10f\n"
		"L (in \\mu m) = %10f\n"
		"S (in pN) = %10f\n"
		"with %10d number of points\n",&xi,&T,&L,&S,&nx);

  if (i == WIN_CANCEL)	return OFF;

  op = create_and_attach_one_plot(pr, nx, nx, 0);
  if (op == NULL)		return win_printf("cannot create plot !");
  ds = op->dat[0];

  for (i = 0, tmp = 0.001 ; i < nx; i++, tmp *= 1.01)
    {
      y = 1.38e-23 * T/(2*xi*1e-9);
      y /= tmp;
      y *= 1e12;
      ds->yd[i] = tmp;
      y1 = (double)1/y;
      //if ((i*20)%nx == 0)	win_printf("y = %f y1 = %f",y,y1);
      ds->xd[i] = L * (cosh(y1)/sinh(y1) - y);
      if (S > 0) ds->xd[i] += L*(tmp/S);
    }



  set_plot_title(op, "\\stack{{FJC model SC}"
		 "{z = L0 [(coth(F*2*xi/kBT) - (kBT/F*2xi))+F/S]}"
		 "{\\pt8 \\xi  = %g nm, T = %g,}"
		 "{L = %g \\mu m, S = %g pN}}",xi,T,L,S);
  set_plot_x_title(op, "Allongement (\\mu m)");
  set_plot_y_title(op, "Force (pN)");
  return refresh_plot(pr,pr->n_op-1);
}

# ifdef PB
// mFJC_sc z = L0 (coth(2*F*xi/kBT) - (kBT/2*F*xi))(F/S)
// f = 2*F*xi/kBT
// (z/L0) = (coth(f) - (1/f)) * (f.kBT/2*S*xi)
// (z/L0) = (coth(f) - (1/f)) * (f.b)
// b = kBT/2*S*xi

double invert_SC_elastic_FJC(double zr, double b)
{
  register int i;
  double z, z1, z2, f, f1, f2;
  int wasneg = 0;

  if (zr == 0) return (double)0;
  if (zr < 0)
    {
      zr = -zr;
      wasneg = 1;
    }
  f1 = 3*zr; // we use linear approx to start
  z1 = (cosh(f1)/sinh(f1) - (double)1/f1)*(1+f1*b);
  for (i = 0 ;z1 > zr && i < 64; f1/=2, i++)
    z1 = (cosh(f1)/sinh(f1) - (double)1/f1)*(f1*b);
  f2 = 2*f1;
  z2 = (cosh(f2)/sinh(f2) - (double)1/f2)*(f2*b);
  for (i = 0;z2 < zr && i < 64; f2*=2, i++)
    z2 = (cosh(f2)/sinh(f2) - (double)1/f2)*(f2*b);
  // now we have z1 < zr < z2
  for (i = 0; i < 64; i++)
    {
      f = (f1+f2)/2;
      z = (cosh(f)/sinh(f) - (double)1/f)*(f*b);
      if (z < zr)
	{
	  z1 = z;
	  f1 = f;
	}
      else
	{
	  z2 = z;
	  f2 = f;
	}
    }
  f = (f1+f2)/2;
  z = (cosh(f)/sinh(f) - (double)1/f)*(f*b);
  //win_printf("zr %g f %g z-zr %g",zr,f,abs(z-zr));
  return (wasneg) ? -f : f;
}


float	find_best_xi_SC_elastic_Brilluoin(d_s *dsi, float T, float xi, float L, float S, float *E)
{
  register int i, j;
  double fy, f, x, y, fx, er, b, Normf;

  for (i = 0; i < dsi->nx; i++)
    {
      x = dsi->xd[i];
      if (x < 0 )
	return win_printf("cannot fit \n"
			  "L = %g at %d is too small or negative!",x,i);
      y = dsi->yd[i];
      if (y <= 0)
	return win_printf("cannot fit \nforce  = %g at %d is negative!",y,i);

    }
// b = kBT/2*S*xi
  b = 1.38e-2 * T/(2*S*xi);
  Normf = 1.38e-2 * T/(2*xi);
  for (i = 0, f = fy = 0; i < dsi->nx; i++)
    {
      er = (dsi->ye != NULL) ? dsi->ye[i] : dsi->yd[i]/10;
      x = dsi->xd[i]/L;
      y = dsi->yd[i];
      y /=  Normf;
      er /= Normf;
      fx = invert_SC_elastic_FJC(x, b);
      if (y < 0)	return win_printf("cannot treat negative force");
      if (er > 0)
	{
	  fy += y*fx/(er*er);
	  f += (fx*fx)/(er*er);
	}
    }
  xi = xi * f/fy;
  b = 1.38e-2 * T/(2*S*xi);
  Normf = 1.38e-2 * T/(2*xi);
  for (i = 0, j = 0, *E = 0; i < dsi->nx; i++)
    {
      x = dsi->xd[i]/L;
      y = dsi->yd[i];
      fx = invert_SC_elastic_FJC(x, b);
      fx *= Normf;
      er = (dsi->ye != NULL) ? dsi->ye[i] : dsi->yd[i]/10;
      f = y - fx;
      if (er > 0)
	{
	  *E += (f*f)/(er*er);
	  j++;
	}
    }
  return (float)xi;
}


 int	do_fit_xi_auto_with_er_SC_Brillouin(void)
{
  register int i, k;
  char st[256] = {0};
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *dsi = NULL, *dsd = NULL;
  float x, y, tmp, E = 0, E1 = 0, E2 = 0, dl = .1, ds = 0, lmin = 0, lmax = 0;
  int nf = 1024;
  static int fitxi = 1, fitl = 1, fits = 1;
  static float T = 298;
  static float S = 500; // pN
  static float xi = 1;	/* nm*/
  static float L = 1;	/* microns */
  double b, Normf;


  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  i = win_scanf("Fit SC Elastic FJC :\n"
		"x = L0.(coth(2*F*\\xi /kBT) - (kBT/2*F*\\xi ))(F/S)\n"
		"x->extension, y->force, error on force\n"
		"Starting value of persitence length \\xi  %8f (nm), Fit \\xi %b\n"

		"Starting value of Length of molecule L %8f Fit L %b\n"
		"starting Value of enthalpic elasticity S %8f (pN) Fit S %b\n"
		,&xi,&fitxi,&L,&fitl,&S,&fits);
  if (i == WIN_CANCEL)	return OFF;
  for (i = 0; i < dsi->nx; i++)
    {
      x = fabs(dsi->xd[i]);
      lmin = (i == 0 || x < lmin) ? x : lmin;
      lmax = (i == 0 || x > lmax) ? x : lmax;
    }
  L = (L < lmin) ? lmin +.01 : L;

  if (fitxi)
    {
      xi = find_best_xi_SC_elastic_Brilluoin(dsi, T, xi, L, S, &E);
      dl = L/10;
      for (i = 0; fitl > 0 && fabs(dl) > .0000125*L && i < 1024; i++)
	{
	  L += dl;
	  L = (L < lmin) ? lmin +.01 : L;
	  xi = find_best_xi_SC_elastic_Brilluoin(dsi, T, xi, L, S, &E1);
	  if (E1 > E)   dl = -dl/2;
	  //win_printf("Iter %d: xi %g L %g E %g dl %g",i,xi,L,E1,dl);
	  E = E1;
	}

      for (k = 0, E2 = E, ds = S/5;fits > 0 && fabs(ds) > .0001*S && k < 1024; k++)
	{
	  S += ds;
	  xi = find_best_xi_SC_elastic_Brilluoin(dsi, T, xi, L, S, &E);
	  dl = L/10;
	  for (i = 0;fitl > 0 && fabs(dl) > .0000125*L && i < 1024; i++)
	    {
	      L += dl;
	      L = (L < lmin) ? lmin +.01 : L;
	      xi = find_best_xi_SC_elastic_Brilluoin(dsi, T, xi, L, S, &E1);
	      if (E1 > E)   dl = -dl/2;
	      E = E1;
	    }
	  //win_printf("%d-%d iter, S = %g ds %g \n\\xi = %g, L = %g\n the energy is %g\nfmax = %g\n",k,i,S,ds,xi,L,E,fmax);
	  if (E > E2)   ds = -ds/2;
	  E2 = E;
	}
    }
      //  i = win_printf("%d-%d iter, S = %g ds %g \n\\xi = %g, L = %g\n"
      //		 " the energy is %g\nfmax = %g\n",k,i,S,ds,xi,L,E,fmax);




      //i = win_printf("%d iter \\xi = %g, L = %g the energy is %g",i,xi,L,E);
      //if (i == WIN_CANCEL)	return OFF;
  if ((dsd = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create data set");
  b = 1.38e-2 * T/(2*S*xi);
  Normf = 1.38e-2 * T/(2*xi);
  for (i = 0, tmp = lmax/nf ; i < dsd->nx; i++, tmp += lmax/nf)
    {
      dsd->xd[i] = (float)(tmp);
      y = invert_SC_elastic_FJC(tmp/L, b);
      y *= Normf;
      dsd->yd[i] = y;
    }


  sprintf(st,"\\fbox{\\pt8\\stack{{\\sl SC Extensible FJC model}"
	  "{L =  %g \\mu m}{\\xi = %g nm}{S = %g pN}{\\chi^2 = %g, n = %d}}}"
	  ,L,xi,S,E,dsi->nx-fits-fitl-fitxi);
  set_ds_plot_label(dsd, op->x_lo + (op->x_hi - op->x_lo)/4,
		  op->y_hi - (op->y_hi - op->y_lo)/4, USR_COORD, st);

  return refresh_plot(pr,pr->cur_op);
}

# endif  // pb
// mFJC z = L0 (coth(2*F*xi/kBT) - (kBT/2*F*xi))(1+F*xi/S*xi)
// f = 2*F*xi/kBT
// (z/L0) = (coth(f) - (1/f)) * (1 + f.kBT/2*S*xi)
// (z/L0) = (coth(f) - (1/f)) * (1 + f.b)
// b = kBT/2*S*xi

double invert_elastic_FJC(double zr, double b)
{
  register int i;
  double z, z1, z2, f, f1, f2;
  int wasneg = 0;

  if (zr == 0) return (double)0;
  if (zr < 0)
    {
      zr = -zr;
      wasneg = 1;
    }
  f1 = 3*zr; // we use linear approx to start
  z1 = (cosh(f1)/sinh(f1) - (double)1/f1)*(1+f1*b);
  for (i = 0 ;z1 > zr && i < 64; f1/=2, i++)
    z1 = (cosh(f1)/sinh(f1) - (double)1/f1)*(1+f1*b);
  f2 = 2*f1;
  z2 = (cosh(f2)/sinh(f2) - (double)1/f2)*(1+f2*b);
  for (i = 0;z2 < zr && i < 64; f2*=2, i++)
    z2 = (cosh(f2)/sinh(f2) - (double)1/f2)*(1+f2*b);
  // now we have z1 < zr < z2
  for (i = 0; i < 64; i++)
    {
      f = (f1+f2)/2;
      z = (cosh(f)/sinh(f) - (double)1/f)*(1+f*b);
      if (z < zr)
	{
	  z1 = z;
	  f1 = f;
	}
      else
	{
	  z2 = z;
	  f2 = f;
	}
    }
  f = (f1+f2)/2;
  z = (cosh(f)/sinh(f) - (double)1/f)*(1+f*b);
  //win_printf("zr %g f %g z-zr %g",zr,f,abs(z-zr));
  return (wasneg) ? -f : f;
}


float	find_best_xi_elastic_Brilluoin(d_s *dsi, float T, float xi, float L, float S, float *E)
{
  register int i, j;
  double fy, f, x, y, fx, er, b, Normf;

  for (i = 0; i < dsi->nx; i++)
    {
      x = dsi->xd[i];
      if (x < 0 )
	return win_printf("cannot fit \n"
			  "L = %g at %d is too small or negative!",x,i);
      y = dsi->yd[i];
      if (y <= 0)
	return win_printf("cannot fit \nforce  = %g at %d is negative!",y,i);

    }
// b = kBT/2*S*xi
  b = 1.38e-2 * T/(2*S*xi);
  Normf = 1.38e-2 * T/(2*xi);
  for (i = 0, f = fy = 0; i < dsi->nx; i++)
    {
      er = (dsi->ye != NULL) ? dsi->ye[i] : dsi->yd[i]/10;
      x = dsi->xd[i]/L;
      y = dsi->yd[i];
      y /=  Normf;
      er /= Normf;
      fx = invert_elastic_FJC(x, b);
      if (y < 0)	return win_printf("cannot treat negative force");
      if (er > 0)
	{
	  fy += y*fx/(er*er);
	  f += (fx*fx)/(er*er);
	}
    }
  xi = xi * f/fy;
  b = 1.38e-2 * T/(2*S*xi);
  Normf = 1.38e-2 * T/(2*xi);
  for (i = 0, j = 0, *E = 0; i < dsi->nx; i++)
    {
      x = dsi->xd[i]/L;
      y = dsi->yd[i];
      fx = invert_elastic_FJC(x, b);
      fx *= Normf;
      er = (dsi->ye != NULL) ? dsi->ye[i] : dsi->yd[i]/10;
      f = y - fx;
      if (er > 0)
	{
	  *E += (f*f)/(er*er);
	  j++;
	}
    }
  return (float)xi;
}


 int	do_fit_xi_auto_with_er_Brillouin(void)
{
  register int i, k;
  char st[256] = {0};
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *dsi = NULL, *dsd = NULL;
  float x, y, tmp, E = 0, E1 = 0, E2 = 0, dl = .1, ds = 0, lmin = 0, lmax = 0;
  int nf = 1024;
  static int fitxi = 1, fitl = 1, fits = 1;
  static float T = 298;
  static float S = 500; // pN
  static float xi = 1;	/* nm*/
  static float L = 1;	/* microns */
  double b, Normf;


  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  i = win_scanf("Fit Elastic FJC :\n"
		"x = L0.(coth(2*F*\\xi /kBT) - (kBT/2*F*\\xi ))(1+F/S)\n"
		"x->extension, y->force, error on force\n"
		"Starting value of persitence length \\xi  %8f (nm), Fit \\xi %b\n"

		"Starting value of Length of molecule L %8f Fit L %b\n"
		"starting Value of enthalpic elasticity S %8f (pN) Fit S %b\n"
		,&xi,&fitxi,&L,&fitl,&S,&fits);
  if (i == WIN_CANCEL)	return OFF;
  for (i = 0; i < dsi->nx; i++)
    {
      x = fabs(dsi->xd[i]);
      lmin = (i == 0 || x < lmin) ? x : lmin;
      lmax = (i == 0 || x > lmax) ? x : lmax;
    }
  L = (L < lmin) ? lmin +.01 : L;

  if (fitxi)
    {
      xi = find_best_xi_elastic_Brilluoin(dsi, T, xi, L, S, &E);
      dl = L/10;
      for (i = 0; fitl > 0 && fabs(dl) > .0000125*L && i < 1024; i++)
	{
	  L += dl;
	  L = (L < lmin) ? lmin +.01 : L;
	  xi = find_best_xi_elastic_Brilluoin(dsi, T, xi, L, S, &E1);
	  if (E1 > E)   dl = -dl/2;
	  //win_printf("Iter %d: xi %g L %g E %g dl %g",i,xi,L,E1,dl);
	  E = E1;
	}

      for (k = 0, E2 = E, ds = S/5;fits > 0 && fabs(ds) > .0001*S && k < 1024; k++)
	{
	  S += ds;
	  xi = find_best_xi_elastic_Brilluoin(dsi, T, xi, L, S, &E);
	  dl = L/10;
	  for (i = 0;fitl > 0 && fabs(dl) > .0000125*L && i < 1024; i++)
	    {
	      L += dl;
	      L = (L < lmin) ? lmin +.01 : L;
	      xi = find_best_xi_elastic_Brilluoin(dsi, T, xi, L, S, &E1);
	      if (E1 > E)   dl = -dl/2;
	      E = E1;
	    }
	  //win_printf("%d-%d iter, S = %g ds %g \n\\xi = %g, L = %g\n the energy is %g\nfmax = %g\n",k,i,S,ds,xi,L,E,fmax);
	  if (E > E2)   ds = -ds/2;
	  E2 = E;
	}
    }
      //  i = win_printf("%d-%d iter, S = %g ds %g \n\\xi = %g, L = %g\n"
      //		 " the energy is %g\nfmax = %g\n",k,i,S,ds,xi,L,E,fmax);




      //i = win_printf("%d iter \\xi = %g, L = %g the energy is %g",i,xi,L,E);
      //if (i == WIN_CANCEL)	return OFF;
  if ((dsd = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create data set");
  b = 1.38e-2 * T/(2*S*xi);
  Normf = 1.38e-2 * T/(2*xi);
  for (i = 0, tmp = lmax/nf ; i < dsd->nx; i++, tmp += lmax/nf)
    {
      dsd->xd[i] = (float)(tmp);
      y = invert_elastic_FJC(tmp/L, b);
      y *= Normf;
      dsd->yd[i] = y;
    }


  sprintf(st,"\\fbox{\\pt8\\stack{{\\sl Extensible FJC model}"
	  "{L =  %g \\mu m}{\\xi = %g nm}{S = %g pN}{\\chi^2 = %g, n = %d}}}"
	  ,L,xi,S,E,dsi->nx-fits-fitl-fitxi);
  set_ds_plot_label(dsd, op->x_lo + (op->x_hi - op->x_lo)/4,
		    op->y_hi - (op->y_hi - op->y_lo)/4, USR_COORD, "%s", st);

  return refresh_plot(pr,pr->cur_op);
}



int	langevin_curve_elastic(void)
{
  register int i;
  static int nx = 1024;
  double  tmp, y, y1;
  static float b = 1;
  double zr = 0.5;
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *ds = NULL;

  if(updating_menu_state != 0)	return D_O_K;

  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return win_printf("cannot find stuff in acreg!");

  i = win_scanf("Draw Langevin curve with elasticity\n\n"
		"z = (coth(f) - (1/f)) * (1 + f.b)\n"
		"b  = %10f\n"
		"with %10d number of points\n",&b,&nx);

  if (i == WIN_CANCEL)	return OFF;

  op = create_and_attach_one_plot(pr, nx, nx, 0);
  if (op == NULL)		return win_printf("cannot create plot !");
  ds = op->dat[0];

  for (i = 0, tmp = 0.001 ; i < nx && tmp < 1000; i++, tmp *= 1.01)
    {
      y = tmp;
      ds->yd[i] = tmp;
      y1 = (double)1/y;
      ds->xd[i] = (cosh(y)/sinh(y) - y1);
      ds->xd[i] *= (1+b*y);
    }



  set_plot_title(op, "\\stack{{Langevin model}"
		"{z = (coth(f) - (1/f)) * (1 + f.b)}"
		 "{b = %g}}",b);
  set_plot_x_title(op, "Extension");
  set_plot_y_title(op, "normalized force");
  do
    {
      i = win_scanf("Invert brillouin zr %10lf b %10f",&zr,&b);
      invert_elastic_FJC(zr, b);
    }while(i != WIN_CANCEL);
  return refresh_plot(pr,pr->n_op-1);
}



# if !defined(PLAYITSAM) && !defined(PIAS) && !defined(PICOTWIST)
/*
  fit parameter a in y = a*exp(-x/tau)
  by minimisation of chi^2

 */

int	find_best_a_of_exp_fit(d_s *dsi,    // input data set
			       int verbose, // issue explicit error message
			       double tau,  // the imposed tau value
			       double *a,   // the best a value
			       double *E)   // the chi^2
{
  register int i, j;
  double fy, f, x, y, fx, er, la = 0;


  if (dsi == NULL)
    {
      if (verbose) win_printf("No valid data \n");
      return 1;
    }
  if (dsi->ye == NULL)
    {
      if (verbose) win_printf("No valid data error in Y \n");
      return 2;
    }
  for (i = 0, f = fy = 0; i < dsi->nx; i++)
    {
      er = dsi->ye[i];
      x = dsi->xd[i];
      y = dsi->yd[i];
      fx = exp(-x/tau);
      if (er > 0)
	{
	  fy += y*fx/(er*er);
	  f += (fx*fx)/(er*er);
	}
    }
  if (f != 0)    la = fy/f;
  if (a) *a = la;
  else return 2;
  for (i = 0, j = 0, *E = 0; i < dsi->nx; i++)
    {
      x = dsi->xd[i];
      y = dsi->yd[i];
      fx = la*exp(-x/tau);
      er = dsi->ye[i];
      f = y - fx;
      if (er > 0)
	{
	  *E += (f*f)/(er*er);
	  j++;
	}
    }
  return 0;
}

int	do_fit_a_exp_t_over_tau_with_er(void)
{
  register int i;
  char st[256] = {0};
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *dsi = NULL, *dsd = NULL;
  float x, tmp, xmin, xmax;
  int nf = 1024;
  double a, dt, E = 0, E1 = 0;
  static int match_x = 0;
  static double tau = 0.1, err = 0.001;

  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  i = win_scanf("Fit y = a.exp (-x/\\tau) \n"
		"\\tau  starting value %12lf\n"
		"Error on \\tau  %12lf\n"
		"fitted curve x matching data %b\n"
		,&tau,&err,&match_x);
  if (i == WIN_CANCEL)	return OFF;
  for (i = 0, xmin = xmax = dsi->xd[0]; i < dsi->nx; i++)
    {
      x = dsi->xd[i];
      xmin = (x < xmin) ? x : xmin;
      xmax = (x > xmax) ? x : xmax;
    }

  i = find_best_a_of_exp_fit(dsi, 1, tau, &a, &E);
  if (i)
    {
      win_printf("Error %d",i);
      return D_O_K;
    }
  dt = tau/10;
  for (i = 0; fabs(dt) > err && i < 1024; i++)
    {
      //if (i < 10) win_printf("iter %d bef \\tau = %g dt -> %g\n"
      //		     "a = %g \\chi^2 %g\n",i,tau,dt,a,E);
      tau += dt;
      find_best_a_of_exp_fit(dsi, 1, tau, &a, &E1);
      if (E1 > E)   dt = -dt/2;
      //if (i < 10) win_printf("iter %d bef \\tau = %g dt -> %g\n"
      //		     "a = %g \\chi^2 %g\n",i,tau,dt,a,E1);
      E = E1;
    }

  if (i > 1023) win_printf("%d iter \\tau = %g, \\chi^2 = %g",i,tau,E);
  if (match_x) nf = dsi->nx;
  if ((dsd = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create data set");
  if (match_x)
    {
      for (i = 0; i < dsi->nx; i++)
	{
	  x = dsi->xd[i];
	  dsd->xd[i] = x;
	  dsd->yd[i] = a*exp(-x/tau);
	}
    }
  else
    {
      tmp = (xmax-xmin)/40;
      xmin -= tmp;
      xmax += tmp;
      for (i = 0, tmp = xmin ; i < dsd->nx; i++, tmp += (xmax-xmin)/dsd->nx)
	{
	  dsd->xd[i] = (float)(tmp);
	  dsd->yd[i] = a*exp(-tmp/tau);
	}
    }


  sprintf(st,"\\fbox{\\pt8\\stack{{\\sl y = a.exp(-t/\\tau)}"
	  "{a =  %g}{\\tau  = %g}{1/\\tau  = %g}{\\chi^2 = %g, n = %d}}}"
	  ,a,tau,((tau!=0)?(double)1/tau:0),E,dsi->nx-2);
  set_ds_plot_label(dsd, op->x_lo + (op->x_hi - op->x_lo)/4,
		    op->y_hi - (op->y_hi - op->y_lo)/4, USR_COORD,"%s",st);
  return refresh_plot(pr,pr->cur_op);
}

# endif


/*
  fit parameter a in y = a*exp(-x/tau)
  by minimisation of chi^2

 */

int	find_best_a_of_1_minus_exp_fit(d_s *dsi,    // input data set
			       int verbose, // issue explicit error message
			       double tau,  // the imposed tau value
			       double *a,   // the best a value
			       double *E)   // the chi^2
{
  register int i, j;
  double fy, f, x, y, fx, er, la = 0;


  if (dsi == NULL)
    {
      if (verbose) win_printf("No valid data \n");
      return 1;
    }
  if (dsi->ye == NULL)
    {
      if (verbose) win_printf("No valid data error in Y \n");
      return 2;
    }
  for (i = 0, f = fy = 0; i < dsi->nx; i++)
    {
      er = dsi->ye[i];
      x = dsi->xd[i];
      y = dsi->yd[i];
      fx = 1;
      fx -= exp(-x/tau);
      if (er > 0)
	{
	  fy += y*fx/(er*er);
	  f += (fx*fx)/(er*er);
	}
    }
  if (f != 0)    la = fy/f;
  if (a) *a = la;
  else return 2;
  for (i = 0, j = 0, *E = 0; i < dsi->nx; i++)
    {
      x = dsi->xd[i];
      y = dsi->yd[i];
      fx = la*((double)1-exp(-x/tau));
      er = dsi->ye[i];
      f = y - fx;
      if (er > 0)
	{
	  *E += (f*f)/(er*er);
	  j++;
	}
    }
  return 0;
}


int	do_fit_a_1_minus_exp_t_over_tau_with_er(void)
{
  register int i;
  char st[256] = {0};
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *dsi = NULL, *dsd = NULL;
  float x, tmp, xmin, xmax;
  int nf1;
  double a, dt, E = 0, E1 = 0;
  static int match_x = 0, nf = 1024;
  static double tau = 0.1, err = 0.001;

  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  for (i = 0, xmin = xmax = dsi->xd[0]; i < dsi->nx; i++)
    {
      x = dsi->xd[i];
      xmin = (x < xmin) ? x : xmin;
      xmax = (x > xmax) ? x : xmax;
    }
  tmp = (xmax-xmin)/40;
  xmin -= tmp;
  xmax += tmp;

  i = win_scanf("Fit y = a.(1-exp (-x/\\tau)) \n"
		"\\tau  starting value %12lf\n"
		"Error on \\tau  %12lf\n"
		"fitted curve x matching data %b\n"
		"otherwise draw the function with %8d pts\n"
		"From %10f to %10f\n"
		,&tau,&err,&match_x,&nf,&xmin,&xmax);
  if (i == WIN_CANCEL)	return OFF;



  i = find_best_a_of_1_minus_exp_fit(dsi, 1, tau, &a, &E);
  if (i)
    {
      win_printf("Error %d",i);
      return D_O_K;
    }
  dt = tau/10;
  for (i = 0; fabs(dt) > err && i < 1024; i++)
    {
      //if (i < 10) win_printf("iter %d bef \\tau = %g dt -> %g\n"
      //		     "a = %g \\chi^2 %g\n",i,tau,dt,a,E);
      tau += dt;
      find_best_a_of_1_minus_exp_fit(dsi, 1, tau, &a, &E1);
      if (E1 > E)   dt = -dt/2;
      //if (i < 10) win_printf("iter %d bef \\tau = %g dt -> %g\n"
      //		     "a = %g \\chi^2 %g\n",i,tau,dt,a,E1);
      E = E1;
    }

  if (i > 1023) win_printf("%d iter \\tau = %g, \\chi^2 = %g",i,tau,E);
  nf1 = (match_x) ? dsi->nx : nf;
  if ((dsd = create_and_attach_one_ds(op, nf1, nf1, 0)) == NULL)
    return win_printf_OK("cannot create data set");
  if (match_x)
    {
      for (i = 0; i < dsi->nx; i++)
	{
	  x = dsi->xd[i];
	  dsd->xd[i] = x;
	  dsd->yd[i] = a*((double)1-exp(-x/tau));
	}
    }
  else
    {
      for (i = 0, tmp = xmin ; i < dsd->nx; i++, tmp += (xmax-xmin)/dsd->nx)
	{
	  dsd->xd[i] = (float)(tmp);
	  dsd->yd[i] = a*((double)1-exp(-tmp/tau));
	}
    }


  sprintf(st,"\\fbox{\\pt8\\stack{{\\sl y = a.[1-exp(-t/\\tau)]}"
	  "{a =  %g}{\\tau  = %g}{1/\\tau  = %g}{\\chi^2 = %g, n = %d}}}"
	  ,a,tau,((tau!=0)?(double)1/tau:0),E,dsi->nx-2);
  set_ds_plot_label(dsd, op->x_lo + (op->x_hi - op->x_lo)/4,
		    op->y_hi - (op->y_hi - op->y_lo)/4, USR_COORD, "%s", st);
  return refresh_plot(pr,pr->cur_op);
}


/*
  fit parameter a in y = a*x^2
  by minimisation of chi^2

 */

int	find_best_a_of_x2_fit(d_s *dsi,    // input data set
			       int verbose, // issue explicit error message
			       double *a,   // the best a value
			       double *E)   // the chi^2
{
  register int i, j;
  double fy, f, x, y, fx, er, la = 0;


  if (dsi == NULL)
    {
      if (verbose) win_printf("No valid data \n");
      return 1;
    }
  if (dsi->ye == NULL)
    {
      if (verbose) win_printf("No valid data error in Y \n");
      return 2;
    }
  for (i = 0, f = fy = 0; i < dsi->nx; i++)
    {
      er = dsi->ye[i];
      x = dsi->xd[i];
      y = dsi->yd[i];
      fx = x * x;
      if (er > 0)
	{
	  fy += y*fx/(er*er);
	  f += (fx*fx)/(er*er);
	}
    }
  if (f != 0)    la = fy/f;
  if (a) *a = la;
  else return 2;
  for (i = 0, j = 0, *E = 0; i < dsi->nx; i++)
    {
      x = dsi->xd[i];
      y = dsi->yd[i];
      fx = la*x*x;
      er = dsi->ye[i];
      f = y - fx;
      if (er > 0)
	{
	  *E += (f*f)/(er*er);
	  j++;
	}
    }
  return 0;
}

int	do_fit_a_x2_with_er(void)
{
  register int i;
  char st[256] = {0};
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *dsi = NULL, *dsd = NULL;
  float x, tmp, xmin, xmax;
  int nf1;
  double a, E = 0;
  static int match_x = 0, nf = 1024;
  //static double err = 0.001;

  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  for (i = 0, xmin = xmax = dsi->xd[0]; i < dsi->nx; i++)
    {
      x = dsi->xd[i];
      xmin = (x < xmin) ? x : xmin;
      xmax = (x > xmax) ? x : xmax;
    }
  tmp = (xmax-xmin)/40;
  xmin -= tmp;
  xmax += tmp;

  i = win_scanf("Fit y = a.x^2 \n"
		"fitted curve x matching data %b\n"
		"otherwise draw the function with %8d pts\n"
		"From %10f to %10f\n"
		,&match_x,&nf,&xmin,&xmax);
  if (i == WIN_CANCEL)	return OFF;


  i = find_best_a_of_x2_fit(dsi, 1, &a, &E);
  if (i)
    {
      win_printf("Error %d",i);
      return D_O_K;
    }
  nf1 = (match_x) ? dsi->nx : nf;
  if ((dsd = create_and_attach_one_ds(op, nf1, nf1, 0)) == NULL)
    return win_printf_OK("cannot create data set");
  if (match_x)
    {
      for (i = 0; i < dsi->nx; i++)
	{
	  x = dsi->xd[i];
	  dsd->xd[i] = x;
	  dsd->yd[i] = a*x*x;
	}
    }
  else
    {
      for (i = 0, tmp = xmin ; i < dsd->nx; i++, tmp += (xmax-xmin)/dsd->nx)
	{
	  x = dsd->xd[i] = (float)(tmp);
	  dsd->yd[i] = a*x*x;
	}
    }


  sprintf(st,"\\fbox{\\pt8\\stack{{\\sl y = a.x^2}"
	  "{a =  %g}{\\chi^2 = %g, n = %d}}}"
	  ,a,E,dsi->nx-1);
  set_ds_plot_label(dsd, op->x_lo + (op->x_hi - op->x_lo)/4,
		    op->y_hi - (op->y_hi - op->y_lo)/4, USR_COORD,"%s",st);
  return refresh_plot(pr,pr->cur_op);
}


/*
  fit parameter a in y = a*x
  by minimisation of chi^2

 */



int	find_best_a_of_x_fit(d_s *dsi,    // input data set
			       int verbose, // issue explicit error message
			       double *a,   // the best a value
			       double *E)   // the chi^2
{
  register int i, j;
  double fy, f, x, y, fx, er, la = 0;


  if (dsi == NULL)
    {
      if (verbose) win_printf("No valid data \n");
      return 1;
    }
  if (dsi->ye == NULL)
    {
      if (verbose) win_printf("No valid data error in Y \n");
      return 2;
    }
  for (i = 0, f = fy = 0; i < dsi->nx; i++)
    {
      er = dsi->ye[i];
      x = dsi->xd[i];
      y = dsi->yd[i];
      fx = x ;
      if (er > 0)
	{
	  fy += y*fx/(er*er);
	  f += (fx*fx)/(er*er);
	}
    }
  if (f != 0)    la = fy/f;
  if (a) *a = la;
  else return 2;
  for (i = 0, j = 0, *E = 0; i < dsi->nx; i++)
    {
      x = dsi->xd[i];
      y = dsi->yd[i];
      fx = la*x;
      er = dsi->ye[i];
      f = y - fx;
      if (er > 0)
	{
	  *E += (f*f)/(er*er);
	  j++;
	}
    }
  return 0;
}


int	do_fit_a_x_with_er(void)
{
  register int i;
  char st[256] = {0};
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *dsi = NULL, *dsd = NULL;
  float x, tmp, xmin, xmax;
  int nf1;
  double a, E = 0;
  static int match_x = 0, nf = 1024;
  //static double err = 0.001;

  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  for (i = 0, xmin = xmax = dsi->xd[0]; i < dsi->nx; i++)
    {
      x = dsi->xd[i];
      xmin = (x < xmin) ? x : xmin;
      xmax = (x > xmax) ? x : xmax;
    }
  tmp = (xmax-xmin)/40;
  xmin -= tmp;
  xmax += tmp;

  i = win_scanf("Fit y = a.x^2 \n"
		"fitted curve x matching data %b\n"
		"otherwise draw the function with %8d pts\n"
		"From %10f to %10f\n"
		,&match_x,&nf,&xmin,&xmax);
  if (i == WIN_CANCEL)	return OFF;


  i = find_best_a_of_x_fit(dsi, 1, &a, &E);
  if (i)
    {
      win_printf("Error %d",i);
      return D_O_K;
    }
  nf1 = (match_x) ? dsi->nx : nf;
  if ((dsd = create_and_attach_one_ds(op, nf1, nf1, 0)) == NULL)
    return win_printf_OK("cannot create data set");
  if (match_x)
    {
      for (i = 0; i < dsi->nx; i++)
	{
	  x = dsi->xd[i];
	  dsd->xd[i] = x;
	  dsd->yd[i] = a*x;
	}
    }
  else
    {
      for (i = 0, tmp = xmin ; i < dsd->nx; i++, tmp += (xmax-xmin)/dsd->nx)
	{
	  x = dsd->xd[i] = (float)(tmp);
	  dsd->yd[i] = a*x;
	}
    }


  sprintf(st,"\\fbox{\\pt8\\stack{{\\sl y = a.x}"
	  "{a =  %g}{\\chi^2 = %g, n = %d}}}"
	  ,a,E,dsi->nx-1);
  set_ds_plot_label(dsd, op->x_lo + (op->x_hi - op->x_lo)/4,
		    op->y_hi - (op->y_hi - op->y_lo)/4, USR_COORD,"%s",st);
  return refresh_plot(pr,pr->cur_op);
}


# if !defined(PLAYITSAM) && !defined(PIAS) && !defined(PICOTWIST)
int	do_cumulative_histogram(void)
{
  register int i;
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *dsi = NULL, *dsd = NULL;


  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  dsd = duplicate_data_set(dsi,NULL);
  for (i = dsd->nx - 2; i >= 0; i--)
    {
      dsd->yd[i] = dsd->yd[i+1] + dsd->yd[i];
      if (dsd->ye)  dsd->ye[i] = sqrt(dsd->ye[i+1]*dsd->ye[i+1] + dsd->ye[i]*dsd->ye[i]);
    }
  inherit_from_ds_to_ds(dsd, dsi);
  add_one_plot_data (op, IS_DATA_SET, (void *)dsd);
  return refresh_plot(pr,pr->cur_op);
}


/*
  fit parameter a in y = a*exp(-x/tau) + b
  by minimisation of chi^2

 */


int	find_best_a_an_b_of_exp_fit(d_s *dsi,    // input data set
				    int verbose, // issue explicit error message
				    int error_type, // the type of error in data
				    double tau,  // the imposed tau value
				    double *a,   // the best a value
				    double *b,   // the best b value
				    double *E)   // the chi^2
{
  register int i, j;
  double fy, f, x, y, fx, er, la, lb;
  double sy = 0, sy2 = 0, sfx = 0, sfx2 = 0, sfxy = 0, sig_2, ny = 0;

  if (dsi == NULL)
    {
      if (verbose) win_printf("No valid data \n");
      return 1;
    }
  for (i = 0, f = fy = 0; i < dsi->nx; i++)
    {
      x = dsi->xd[i];
      y = dsi->yd[i];
      fx = exp(-x/tau);
      if (error_type == 0 && dsi->ye != NULL)   er = dsi->ye[i];
      else if (error_type == 1) er = fx;
      else er = 1;
      if (er > 0)
	{
	  sig_2 = (double)1/(er*er);
	  sy += y*sig_2;
	  sy2 += y*y*sig_2;
	  sfx += fx*sig_2;
	  sfx2 += fx*fx*sig_2;
	  sfxy += y*fx*sig_2;
	  ny += sig_2;
	}
    }
  lb = (sfx2 * ny) - sfx * sfx;
  if (lb == 0)
    {
      if (verbose) win_printf_OK("\\Delta  = 0 \n can't fit that!");
      return 3;
    }
  la = sfxy * ny - sfx * sy;
  la /= lb;
  lb = (sfx2 * sy - sfx * sfxy)/lb;
  if (a) *a = la;
  if (b) *b = lb;
  for (i = 0, j = 0, *E = 0; i < dsi->nx; i++)
    {
      x = dsi->xd[i];
      y = dsi->yd[i];
      fx = la*exp(-x/tau) + lb;
      if (error_type == 0 && dsi->ye != NULL)   er = dsi->ye[i];
      else if (error_type == 1) er = fx;
      else er = 1;
      f = y - fx;
      if (er > 0)
	{
	  *E += (f*f)/(er*er);
	  j++;
	}
    }
  return 0;
}



int	do_fit_a_exp_t_over_tau_plus_b_with_er(void)
{
  register int i;
  char st[256] = {0};
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *dsi = NULL, *dsd = NULL;
  float x, tmp, xmin, xmax;
  int nf = 1024;
  double a, b, dt, E = 0, E1 = 0, ert, tauu;
  static int match_x = 0, error_type = 0;
  static double tau = 0.1, err = 0.0001;

  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  if (dsi->ye != NULL)
    {
      error_type = 0;
      i = win_scanf("Fit y = a.exp (-x/\\tau) +b \n"
		    "\\tau  starting value %12lf\n"
		    "Relative error on \\tau  %12lf\n"
		    "Error type in data: error bars %R f(x) %r constant %r\n"
		    "fitted curve x matching data %b\n"
		    ,&tau,&err,&error_type,&match_x);
        if (i == WIN_CANCEL)	return OFF;
    }
  else
    {
      i = win_scanf("Fit y = a.exp (-x/\\tau) +b \n"
		    "\\tau  starting value %12lf\n"
		    "Relative error on \\tau  %12lf\n"
		    "Error type in data: f(x) %R constant %r\n"
		    "fitted curve x matching data %b\n"
		    ,&tau,&err,&error_type,&match_x);
      if (i == WIN_CANCEL)	return OFF;
      error_type++;
    }


  for (i = 0, xmin = xmax = dsi->xd[0]; i < dsi->nx; i++)
    {
      x = dsi->xd[i];
      xmin = (x < xmin) ? x : xmin;
      xmax = (x > xmax) ? x : xmax;
    }

  tauu = (op->dx > 0) ? tau/op->dx : tau;


  i = find_best_a_an_b_of_exp_fit(dsi, 1, error_type, tauu, &a, &b, &E);
  if (i)
    {
      win_printf("Error %d",i);
      return D_O_K;
    }
  dt = tauu/10;
  ert = tauu*err;
  for (i = 0; fabs(dt) > ert && i < 1024; i++)
    {
      tauu += dt;
      find_best_a_an_b_of_exp_fit(dsi, 1, error_type, tauu, &a, &b, &E1);
      if (E1 > E)   dt = -dt/2;
      E = E1;
    }
  if (i > 1023)
    win_printf("%d iter \\tau = %g, a = %g b = %g \\chi^2 = %g",i,tauu,a,b,E);
  if (match_x) nf = dsi->nx;
  if ((dsd = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create data set");
  tau = tauu*op->dx;
  win_printf("\\tau = %g, \\tau_u %g",tau, tauu);
  if (match_x)
    {
      for (i = 0; i < dsi->nx; i++)
	{
	  x = dsi->xd[i];
	  dsd->xd[i] = x;
	  dsd->yd[i] = a*exp(-x/tauu) +b;
	}
    }
  else
    {
      tmp = (xmax-xmin)/40;
      xmin -= tmp;
      xmax += tmp;
      for (i = 0, tmp = xmin ; i < dsd->nx; i++, tmp += (xmax-xmin)/dsd->nx)
	{
	  dsd->xd[i] = (float)(tmp);
	  dsd->yd[i] = a*exp(-tmp/tauu) +b;
	}
    }
  b = op->ay+(op->dy*b);
  a = a*op->dy;

  sprintf(st,"\\fbox{\\pt8\\stack{{\\sl y = a.exp(-t/\\tau)}"
	  "{a =  %g%s}{b = %g%s}{\\tau  = %g%s}{1/\\tau  = %g}"
	  "{\\chi^2 = %g, n = %d}}}"
	  ,a,(op->y_unit) ? op->y_unit:"",b,(op->y_unit) ? op->y_unit:" ",tau
	  ,(op->x_unit) ? op->x_unit:"",((tau!=0)?(double)1/tau:0),E,dsi->nx-3);
  set_ds_plot_label(dsd, op->x_lo + (op->x_hi - op->x_lo)/4,
		    op->y_hi - (op->y_hi - op->y_lo)/4, USR_COORD,"%s",st);
  return refresh_plot(pr,pr->cur_op);
}

#endif

/*
  fit parameter a in a Lorentzian
  y = a * /(1 + (fc/f)^2)
  by minimisation of chi^2
  return the # of fitted pts or an error if < 0
 */

 static double a1d=0.00001;
 static double fc1d=10;



 double Spql(d_s *dsi, int p, int q)
 {
   if (dsi==NULL) return 0;
   int nx=dsi->nx;
   double r=0;
   for (int i=0;i<nx;i++)
   {
     r+=pow(dsi->xd[i],2*p)*pow(dsi->yd[i],q);
   }
   return r;
 }

 int find_best_lorentian_swedish_trick(d_s *ds, double *a, double *fc)
 {
     double S01=Spql(ds,0,1);
     double S02=Spql(ds,0,2);
     double S11=Spql(ds,1,1);
     double S22=Spql(ds,2,2);
     double S12=Spql(ds,1,2);
     double fac=2.0/(S02*S22-S12*S12);
     double aa=fac*(S01*S22-S11*S12);
     double bb=fac*(S11*S02-S01*S12);
     *a=1.0/aa;
     *fc=sqrt(aa/bb);
     return 0;

 }

  double mle_lorentz (double a, double f, void *params)
  {
    d_s *ds = (d_s *) params;
    double mle=0;
    double pf=0;
    for (int i=0;i<ds->nx;i++)
    {
      pf=a/(1+(ds->xd[i]/f)*(ds->xd[i]/f));
      mle+=log(pf)+ds->yd[i]/pf;
    }
    printf("mle = %lf \n",mle);

    return mle;


  };

  double mle_1d_fc (double f, void *params)
  {
    d_s *ds = (d_s *) params;
    double mle=0;
    double pf=0;
    for (int i=0;i<ds->nx;i++)
    {
      pf=a1d/(1+(ds->xd[i]/f)*(ds->xd[i]/f));
      mle+=log(pf)+ds->yd[i]/pf;
    }
    printf("mle = %lf \n",mle);

    return mle;


  };


  double mle_1d_a (double a, void *params)
  {
    d_s *ds = (d_s *) params;
    double mle=0;
    double pf=0;
    for (int i=0;i<ds->nx;i++)
    {
      pf=a/(1+(ds->xd[i]/fc1d)*(ds->xd[i]/fc1d));
      mle+=log(pf)+ds->yd[i]/pf;
    }
    printf("mle = %lf \n",mle);

    return mle;


  };


int minimization_1d (d_s *ds, double *my_f_1d,double m, double a, double b,double *fc)
{
  int status;
  int iter = 0, max_iter = 100;
  const gsl_min_fminimizer_type *T;
  gsl_min_fminimizer *s;
  gsl_function F;

  F.function = my_f_1d;
  F.params = ds;

  T = gsl_min_fminimizer_brent;
  s = gsl_min_fminimizer_alloc (T);
  gsl_min_fminimizer_set (s, &F, m, a, b);

  printf ("using %s method\n",
          gsl_min_fminimizer_name (s));

  printf ("%5s [%9s, %9s] %9s \n",
          "iter", "lower", "upper", "min");

  printf ("%5d [%.7f, %.7f] %.7f \n",
          iter, a, b,
          m);

  do
    {
      iter++;
      status = gsl_min_fminimizer_iterate (s);

      m = gsl_min_fminimizer_x_minimum (s);
      a = gsl_min_fminimizer_x_lower (s);
      b = gsl_min_fminimizer_x_upper (s);

      status = gsl_min_test_interval (a, b, 0, 0.01);

      if (status == GSL_SUCCESS)
        printf ("Converged:\n");

      printf ("%5d [%.7f, %.7f] "
              "%.7f \n",
              iter, a, b,
              m);
    }
  while (status == GSL_CONTINUE && iter < max_iter);

  gsl_min_fminimizer_free (s);
  *fc = m;

  return status;
}

int	do_fit_lorentzian_mle(void)
{
  register int i;
  char st[256] = {0};
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *dsi = NULL, *dsd = NULL;
  float x, tmp, xmin, xmax;
  int nf = 1024;
  double  dt, E = 0, E1 = 0;
  static int match_x = 0, error_type = 0;
  static double fc = 20, err = 0.001,a=0.0001;
  static int iter_max=1000;
  static double fc_old=0,a_old=0,mle=0,mle_old=0;
  static float rel_tol=0.01;


  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  i = win_scanf("Warning ! This algorithm is only valid if the spectrum that you fit is not the \n"
  "mean of statistically independent spectrum. If it is, you should cancel.\n"
  "Relative tolerance criterium : %9f \n",&rel_tol);

  if (i == WIN_CANCEL)	return OFF;

  for (i = 0, xmin = xmax = dsi->xd[0]; i < dsi->nx; i++)
    {
      x = dsi->xd[i];
      xmin = (x < xmin) ? x : xmin;
      xmax = (x > xmax) ? x : xmax;
    }

  find_best_lorentian_swedish_trick(dsi, &a, &fc);
  mle=mle_lorentz(a,fc,(void *) dsi);
  a1d=a;
  
  for (int ii=0;ii<iter_max;ii++)
      {
          fc_old=fc;
          a_old=a;
          mle_old=mle;
          minimization_1d (dsi, &mle_1d_fc, fc, 0.5*fc,  2*fc, &fc);
          fc1d=fc;
          minimization_1d (dsi, &mle_1d_a, a,  0.5*a,  2*a, &a);
          a1d=a;
          mle=mle_lorentz(a,fc,(void *) dsi);
          if ((fabs(fc_old-fc)<rel_tol*fabs(fc)) && (fabs(a_old-a)<rel_tol*fabs(a)) && (fabs(mle_old-mle)<rel_tol*fabs(mle))) break;
      }
  
  nf = dsi->nx;
  if ((dsd = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
      return win_printf_OK("cannot create data set");
  
  
  for (i = 0; i < dsi->nx; i++)
      {
          x = dsi->xd[i];
          dsd->xd[i] = x;
          dsd->yd[i] = a/(1 + (x*x)/(fc*fc));
      }
  
  sprintf(st,"\\fbox{\\pt8\\stack{{\\sl f(x) = \\frac{a}{1 +f_c^2/f^2}}"
          "{a =  %g}{f_c  = %g}{n = %d}}}"
          ,a,fc,dsi->nx-2);
  set_ds_plot_label(dsd, op->x_lo + (op->x_hi - op->x_lo)/4,
                    op->y_hi - (op->y_hi - op->y_lo)/4, USR_COORD,"%s", st);
  return refresh_plot(pr,pr->cur_op);
}







int	find_best_a_of_lorentzian(d_s *dsi,       // input data set
			       int verbose,    // issue explicit error message
			       int error_type, // the type of error in data
			       double fc,      // the imposed cut-off freq
			       double *a,      // the best a value
			       double *E)      // the chi^2
{
  register int i, j;
  double fy, f, x, y, fx, er, la = 0;


  if (dsi == NULL)
    {
      if (verbose) win_printf("No valid data \n");
      return -1;
    }
  if (dsi->ye == NULL && error_type == 0)
    {
      if (verbose) win_printf("No valid data error in Y \n");
      return -2;
    }
  for (i = 0, f = fy = 0; i < dsi->nx; i++)
    {
      x = dsi->xd[i];
      y = dsi->yd[i];
      fx = (double)1/(1 + (x*x)/(fc*fc));
      if (error_type == 0 && dsi->ye != NULL)   er = dsi->ye[i];
      else if (error_type == 1) er = fx;
      else er = 1;
      if (er > 0)
	{
	  fy += y*fx/(er*er);
	  f += (fx*fx)/(er*er);
	}
    }
  if (f != 0)    la = fy/f;
  else return -3;
  if (a) *a = la;
  for (i = 0, j = 0, *E = 0; i < dsi->nx; i++)
    {
      x = dsi->xd[i];
      y = dsi->yd[i];
      fx = la/(1 + (x*x)/(fc*fc));
      if (error_type == 0 && dsi->ye != NULL)   er = dsi->ye[i];
      else if (error_type == 1) er = fx;
      else er = 1;
      f = y - fx;
      if (er > 0)
	{
	  *E += (f*f)/(er*er);
	  j++;
	}
    }
  return j;
}




int	do_fit_lorentzian_with_er(void)
{
  register int i;
  char st[256] = {0};
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *dsi = NULL, *dsd = NULL;
  float x, tmp, xmin, xmax;
  int nf = 1024;
  double a, dt, E = 0, E1 = 0;
  static int match_x = 0, error_type = 0;
  static double fc = 20, err = 0.001;

  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  i = win_scanf("Fit f(x) = \\frac{a}{1 +f_c^2/f^2} \n"
		"f_c  starting value %12lf\n"
		"Error type in data: error bars %R f(x) %r constant %r\n"
		"Error on f_c  %12lf\n"
		"fitted curve x matching data %b\n"

		,&fc,&error_type,&err,&match_x);
  if (i == WIN_CANCEL)	return OFF;
  if (error_type == 0 && dsi->ye == NULL)
    return win_printf_OK("You cannot select error bars if they do not exists!");
  for (i = 0, xmin = xmax = dsi->xd[0]; i < dsi->nx; i++)
    {
      x = dsi->xd[i];
      xmin = (x < xmin) ? x : xmin;
      xmax = (x > xmax) ? x : xmax;
    }

  i = find_best_a_of_lorentzian(dsi, 1, error_type, fc, &a, &E);
  if (i < 0)
    {
      win_printf("Error %d",i);
      return D_O_K;
    }
  dt = fc/10;
  for (i = 0; fabs(dt) > err && i < 1024; i++)
    {
      //if (i < 10) win_printf("iter %d bef \\tau = %g dt -> %g\n"
      //		     "a = %g \\chi^2 %g\n",i,tau,dt,a,E);
      fc += dt;
      find_best_a_of_lorentzian(dsi, 1, error_type, fc, &a, &E1);
      if (E1 > E)   dt = -dt/2;
      //if (i < 10) win_printf("iter %d bef \\tau = %g dt -> %g\n"
      //		     "a = %g \\chi^2 %g\n",i,tau,dt,a,E1);
      E = E1;
    }

  if (i > 1023) win_printf("%d iter fc = %g, \\chi^2 = %g",i,fc,E);
  if (match_x) nf = dsi->nx;
  if ((dsd = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create data set");
  if (match_x)
    {
      for (i = 0; i < dsi->nx; i++)
	{
	  x = dsi->xd[i];
	  dsd->xd[i] = x;
	  dsd->yd[i] = a/(1 + (x*x)/(fc*fc));
	}
    }
  else
    {
      tmp = (xmax-xmin)/40;
      //xmin -= tmp;
      xmax += tmp;
      for (i = 0, tmp = xmin ; i < dsd->nx; i++, tmp += (xmax-xmin)/dsd->nx)
	{
	  dsd->xd[i] = (float)(tmp);
	  dsd->yd[i] = a/(1 + (tmp*tmp)/(fc*fc));
	}
    }


  sprintf(st,"\\fbox{\\pt8\\stack{{\\sl f(x) = \\frac{a}{1 +f_c^2/f^2}}"
	  "{a =  %g}{f_c  = %g}{\\chi^2 = %g, n = %d}}}"
	  ,a,fc,E,dsi->nx-2);
  set_ds_plot_label(dsd, op->x_lo + (op->x_hi - op->x_lo)/4,
		    op->y_hi - (op->y_hi - op->y_lo)/4, USR_COORD,"%s", st);
  return refresh_plot(pr,pr->cur_op);
}



/*
  fit parameter a in a 1/f noise + white noise
  y = a * (1 + (fc/f)^2)
  by minimisation of chi^2
  return the # of fitted pts or an error if < 0
  the point of index 0 (f = 0) is diregarded
 */

int	find_best_a_of_1_over_f2_plus_white_noise(d_s *dsi,       // input data set
						  int verbose,    // issue explicit error message
						  int error_type, // the type of error in data
						  double fc,      // the imposed cut-off freq
						  double *a,      // the best a value
						  double *E)      // the chi^2
{
  register int i, j;
  double fy, f, x, y, fx, er, la = 0;


  if (dsi == NULL)
    {
      if (verbose) win_printf("No valid data \n");
      return -1;
    }
  if (dsi->ye == NULL && error_type == 0)
    {
      if (verbose) win_printf("No valid data error in Y \n");
      return -2;
    }
  for (i = 1, f = fy = 0; i < dsi->nx; i++)
    {
      x = dsi->xd[i];
      if (x <= 0) continue;
      y = dsi->yd[i];
      fx = (1 + (fc*fc)/(x*x));
      if (error_type == 0 && dsi->ye != NULL)   er = dsi->ye[i];
      else if (error_type == 1) er = fx;
      else er = 1;
      if (er > 0)
	{
	  fy += y*fx/(er*er);
	  f += (fx*fx)/(er*er);
	}
    }
  if (f != 0)    la = fy/f;
  else return -3;
  if (a) *a = la;
  for (i = 1, j = 0, *E = 0; i < dsi->nx; i++)
    {
      x = dsi->xd[i];
      y = dsi->yd[i];
      fx = la*(1 + (fc*fc)/(x*x));
      if (error_type == 0 && dsi->ye != NULL)   er = dsi->ye[i];
      else if (error_type == 1) er = fx;
      else er = 1;
      f = y - fx;
      if (er > 0)
	{
	  *E += (f*f)/(er*er);
	  j++;
	}
    }
  return j;
}


int	do_fit_a_x_1_over_f2_plus_white_noise(void)
{
  register int i, j;
  char st[256] = {0};
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *dsi = NULL, *dsd = NULL;
  float x, tmp, xmin = 0.01, xmax = 100;
  int nf = 1024;
  double a, dt, E = 0, E1 = 0;
  static int match_x = 0, error_type = 0, fitfc = 1;
  static double fc = 20, err = 0.001;

  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  i = win_scanf("Fit f(x) = a.(1 +f_c^2/f^2) \n"
		"f_c  starting value %12lf fit f_c %b\n"
		"Error type in data: error bars %R f(x) %r constant %r\n"
		"Error on f_c  %12lf\n"
		"fitted curve x matching data %b\n"
		,&fc,&fitfc,&error_type,&err,&match_x);
  if (i == WIN_CANCEL)	return OFF;
  if (error_type == 0 && dsi->ye == NULL)
    return win_printf_OK("You cannot select error bars if they do not exists!");
  for (i = 0, j = 0; i < dsi->nx; i++)
    {
      x = dsi->xd[i];
      if (x <= 0) continue;
      if (j == 0)
	{
	  xmin = xmax = dsi->xd[i];
	  j = 1;
	}
      xmin = (x < xmin) ? x : xmin;
      xmax = (x > xmax) ? x : xmax;
    }

  i = find_best_a_of_1_over_f2_plus_white_noise(dsi, 1, error_type, fc, &a, &E);
  if (i < 0)
    {
      win_printf("Error %d",i);
      return D_O_K;
    }
  if (fitfc)
    {
      dt = fc/10;
      for (i = 0; fabs(dt) > err && i < 1024; i++)
	{
	  //if (i < 10) win_printf("iter %d bef \\tau = %g dt -> %g\n"
	  //		     "a = %g \\chi^2 %g\n",i,tau,dt,a,E);
	  fc += dt;
	  find_best_a_of_1_over_f2_plus_white_noise(dsi, 1, error_type, fc, &a, &E1);
	  if (E1 > E)   dt = -dt/2;
	  //if (i < 10) win_printf("iter %d bef \\tau = %g dt -> %g\n"
	  //		     "a = %g \\chi^2 %g\n",i,tau,dt,a,E1);
	  E = E1;
	}
      if (i > 1023) win_printf("%d iter fc = %g, \\chi^2 = %g",i,fc,E);
    }
  if (match_x) nf = dsi->nx;
  if ((dsd = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create data set");
  if (match_x)
    {
      for (i = 0, j = 0; i < dsi->nx; i++)
	{
	  x = dsi->xd[i];
	  if (x <= 0) continue;
	  dsd->xd[j] = x;
	  dsd->yd[j++] = a*(1 + (fc*fc)/(x*x));
	}
      dsd->nx = dsd->ny = j;
    }
  else
    {
      tmp = (xmax-xmin)/40;
      //xmin -= tmp;
      xmax += tmp;
      for (i = 0, tmp = xmin ; i < dsd->nx; i++, tmp += (xmax-xmin)/dsd->nx)
	{
	  dsd->xd[i] = (float)(tmp);
	  dsd->yd[i] = a*(1 + (fc*fc)/(tmp*tmp));
	}
    }


  sprintf(st,"\\fbox{\\pt8\\stack{{\\sl f(x) = a \\times (1 +f_c^2/f^2)}"
	  "{a =  %g}{f_c  = %g}{\\chi^2 = %g, n = %d}}}"
	  ,a,fc,E,dsi->nx-1-fitfc);
  set_ds_plot_label(dsd, op->x_lo + (op->x_hi - op->x_lo)/4,
		    op->y_hi - (op->y_hi - op->y_lo)/4, USR_COORD,"%s",st);
  return refresh_plot(pr,pr->cur_op);
}




double Michaelis_Mentel(double km, double x)
{
  double tmp;

  tmp = km + x;
  tmp = (tmp != 0) ? (double)1/tmp : tmp;
  return x*tmp;
}

double Michaelis_Mentel_NN(double km, double x, int NN)
{
    double tmp, Xp = 1;
    int i;

    for(i = 0; i < NN; i++)
        Xp *= x;
    tmp = km + Xp;
    tmp = (tmp != 0) ? (double)1/tmp : tmp;
    return Xp*tmp;
}



int	find_best_MM_at_fixed_km(d_s *dsi, double km, double *a, float *E, int NN)
{
  register int i, j;
  double fy, f, x, y, fx, er, la;

  for (i = 0; i < dsi->nx; i++)
    {
      x = dsi->xd[i];
      y = dsi->yd[i];
      if (y <= 0)
	return win_printf("cannot fit \nrate  = %g at %d is negative!",y,i);

    }

  for (i = 0, f = fy = 0; i < dsi->nx; i++)
    {
      er = (dsi->ye != NULL) ? dsi->ye[i] : dsi->yd[i]/10;
      x = dsi->xd[i];
      y = dsi->yd[i] ;
      fx = Michaelis_Mentel_NN(km, x, NN);
      if (y < 0)	return win_printf("cannot treat negative force");
      if (er > 0)
	{
	  fy += y*fx/(er*er);
	  f += (fx*fx)/(er*er);
	}
    }
  if (f != 0)    la = fy/f;
  else return -3;
  if (a) *a = la;
  for (i = 0, j = 0, *E = 0; i < dsi->nx; i++)
    {
      x = dsi->xd[i];
      y = dsi->yd[i];
      fx = la*Michaelis_Mentel_NN(km, x, NN);
      er = (dsi->ye != NULL) ? dsi->ye[i] : dsi->yd[i]/10;
      f = y - fx;
      if (er > 0)
	{
	  *E += (f*f)/(er*er);
	  j++;
	}
    }
  return j;
}



int	find_chi2_MM(d_s *dsi, double km, double a, float *E, int NN)
{
  register int i, j;
  double  f, x, y, fx, er;

  for (i = 0; i < dsi->nx; i++)
    {
      x = dsi->xd[i];
      y = dsi->yd[i];
      if (y <= 0)
	return win_printf("cannot fit \nrate  = %g at %d is negative!",y,i);

    }
  for (i = 0, j = 0, *E = 0; i < dsi->nx; i++)
    {
      x = dsi->xd[i];
      y = dsi->yd[i];
      fx = a*Michaelis_Mentel_NN(km, x, NN);
      er = (dsi->ye != NULL) ? dsi->ye[i] : dsi->yd[i]/10;
      f = y - fx;
      if (er > 0)
	{
	  *E += (f*f)/(er*er);
	  j++;
	}
    }
  return j;
}


int	do_fit_MM_auto_with_er(void)
{
  register int i;
  char st[256] = {0};
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *dsi = NULL, *dsd = NULL;
  float x, y, tmp, E = 0, E1 = 0, dl = .1, lmin, lmax;
  static int fita = 1,  nf = 1024, NN = 1;
  static double km = 50, a = 1;
 



  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  for (i = 0, lmin = lmax = fabs(dsi->xd[0]); i < dsi->nx; i++)
    {
      x = fabs(dsi->xd[i]);
      lmin = (x < lmin) ? x : lmin;
      lmax = (x > lmax) ? x : lmax;
    }

  i = win_scanf("Fit V = \\frac{a.x^n}{k_m + x^n}\n\n\n"
                "k_m possible value %12lf \n"
                "Fit a %b else impose value %8lf\n"
                "Impose n value to %4d\n"
                ,&km,&fita,&a,&NN);
  if (i == WIN_CANCEL)	return OFF;
  dl = km/10;
  if (fita)    i = find_best_MM_at_fixed_km(dsi, km, &a, &E, NN);
  else find_chi2_MM(dsi, km, a, &E, NN);
  for (i = 0; (fabs(dl) > .000125*km) && i < 1024; i++)
    {
      km += dl;
      if (fita)    i = find_best_MM_at_fixed_km(dsi, km, &a, &E1, NN);
      else find_chi2_MM(dsi, km, a, &E1, NN);
      if (E1 > E)   dl = -dl/2;
      //win_printf("i = %d km = %g; dk = %g; a = %g; E = %g",i,km,dl,a,E1);
      E = E1;
    }

  i = win_printf("%d iter km = %g, a = %g, the energy is %g",i,km,a,E);
  if (i == WIN_CANCEL)	return OFF;
  float kmin = 0;
  for (i = 0, kmin = dsi->xd[0]; i < dsi->nx; i++)
      kmin = (dsi->xd[i] < kmin) ? dsi->xd[i] : kmin;
      
  tmp = km/100;
  tmp = kmin/2;
  for (nf = 0; nf < 10000 && tmp < lmax; nf++, tmp *= 1.025);
  if ((dsd = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create data set");
  for (i = 0, tmp = kmin/2 ; i < dsd->nx; i++, tmp *= 1.025)
    {
      dsd->xd[i] = (float)(tmp);
      y = a*Michaelis_Mentel_NN(km, tmp, NN);
      dsd->yd[i] = y;
    }


  sprintf(st,"\\fbox{\\pt8\\stack{{\\sl Fit V = \\frac{a.x^%d}{k_m + x^%d}}"
          "{km =  %g \\mu m}{a = %g}{\\chi^2 = %g, n = %d}}}"
          ,NN,NN,km,a,E,dsi->nx-2);
  push_plot_label(op, op->x_lo + (op->x_hi - op->x_lo)/4,
		  op->y_hi - (op->y_hi - op->y_lo)/4, st, USR_COORD);
  return refresh_plot(pr,pr->cur_op);
}

int bootstrap_ds_with_er_in_y(d_s *ds, d_s *dsd, int *idum)
{
  register int i, j, nx;
  float ran;

  nx = ds->nx;
  for (i = 0; i < nx; i++)
    {
      ran = ran1(idum);
      j = (int)(nx*ran);
      j = (j < 0) ? 0 : j;
      j = (j < nx) ? j : nx-1;
      dsd->xd[i] = ds->xd[j];
      dsd->yd[i] = ds->yd[j];
      dsd->ye[i] = ds->ye[j];
    }
  return 0;
}




int	do_fit_Y_equal_a_MM_boostrap(void)
{
  register int i, j;
  pltreg *pr = NULL;
  O_p *op = NULL, *opb = NULL;
  d_s *dsi = NULL, *dsib = NULL;
  d_s *dskm = NULL, *dsa = NULL, *dschi2 = NULL;
  float xmin, xmax;
  //int nf;
  static int  niter = 512;
  static int idum = 458;
  float  E = 0, E1 = 0, dl = .1;//, lmin, lmax;
  static int fita = 1, NN = 1;
  static double km = 50, a = 1;



  if(updating_menu_state != 0)	return D_O_K;
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine fits a Michaelis Menten function to a data set\n"
			   "using a Levenberg Marquart algorithm and boostraps input data\n"
			   "Fit V = \\frac{a.x^n}{k_m + x^n}\n\n\n"
			   "The boostrap allows to estimate the errors in the fitted\n"
			   "parameters");
    }

  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)	return 1;
  for (i = 0, xmin = xmax = dsi->xd[0]; i < dsi->nx; i++)
    {
      xmin = (dsi->xd[i] < xmin) ? dsi->xd[i] : xmin;
      xmax = (dsi->xd[i] > xmax) ? dsi->xd[i] : xmax;
    }

  i = win_scanf("This routine bootstrat a fit to a Michaelis Menten function to a data set\n"
                "using a Levenberg Marquart algorithm and boostraps input data\n"
                "Fit V = \\frac{a.x^n}{k_m + x^n}\n\n\n"
                "k_m possible value %6lf \n"
                "Fit a %b else impose value %6lf\n"
                "Imposed n value %4d\n"
                "the parameter k_m is fitted, a may be fitted or imposed\n"
                "The boostrap allows to estimate the errors in the fitted\n"
                "parameters. Nb. of boostrap %5d random seed %8d\n"
                ,&km,&fita,&a,&niter,&NN,&idum);


  if (i == WIN_CANCEL)	return OFF;


  if (dsi->ye == NULL) return win_printf_OK("your data sets has no error bars!");


  //nf = dsi->nx;
  if ((dsib = duplicate_data_set(dsi, NULL)) == NULL)
    return win_printf_OK("cannot create plot");


  if ((opb = create_and_attach_one_plot(pr, niter, niter, 0)) == NULL)
    return win_printf_OK("cannot create plot");
  dskm = opb->dat[0];
  if ((dsa = create_and_attach_one_ds(opb, niter, niter, 0)) == NULL)
    return win_printf_OK("cannot create data set");
  if ((dschi2 = create_and_attach_one_ds(opb, niter, niter, 0)) == NULL)
    return win_printf_OK("cannot create data set");

  for (j = 0; j < niter; j++)
    {
      bootstrap_ds_with_er_in_y(dsi, dsib, &idum);
      dl = km/10;
      if (fita)    i = find_best_MM_at_fixed_km(dsib, km, &a, &E, NN);
      else find_chi2_MM(dsib, km, a, &E, NN);
      for (i = 0; (fabs(dl) > .000125*km) && i < 1024; i++)
	{
	  km += dl;
	  if (fita)    i = find_best_MM_at_fixed_km(dsib, km, &a, &E1, NN);
	  else find_chi2_MM(dsib, km, a, &E1, NN);
	  if (E1 > E)   dl = -dl/2;
	  //win_printf("i = %d km = %g; dk = %g; a = %g; E = %g",i,km,dl,a,E1);
	  E = E1;
	}

      //i = win_printf("%d iter km = %g, a = %g, the energy is %g",i,km,a,E);
      //if (i == WIN_CANCEL)	return OFF;
      dskm->yd[j] = op->ax+(op->dx*km);
      dsa->yd[j] = a*op->dy;
      dschi2->yd[j] = E;
      dskm->xd[j] = dsa->xd[j] = dschi2->xd[j] = j;

      //display_title_message("%03d x_0 = %06.2g, a = %06.2g  b = %06.2g"
      //		    " \\chi^2  is %06.2g",j,dsx->yd[j],dsa->yd[j],dsb->yd[j],chisq);
    }
  return refresh_plot(pr,pr->cur_op);
}




MENU *polymerWLC_plot_menu(void)
{
  static MENU mn[32] = {0};

  if (mn[0].text != NULL)	return mn;

# if !defined(PLAYITSAM) && !defined(PIAS) && !defined(PICOTWIST)  
  add_item_to_menu(mn,"Fit y = a.exp(t/T)", do_fit_a_exp_t_over_tau_with_er,NULL,0,NULL);
  add_item_to_menu(mn,"Fit y = a.exp(t/T) + b",do_fit_a_exp_t_over_tau_plus_b_with_er,NULL,0,NULL);
  add_item_to_menu(mn,"Fit y = a.[1-exp(t/T)]", do_fit_a_1_minus_exp_t_over_tau_with_er,NULL,0,NULL);
  add_item_to_menu(mn, "\0",   NULL,   NULL,   0, NULL);
# endif
  add_item_to_menu(mn,"Fit Lorentzian",do_fit_lorentzian_with_er,NULL,0,NULL);
  add_item_to_menu(mn,"Fit Lorentzian MLE",do_fit_lorentzian_mle,NULL,0,NULL);

  add_item_to_menu(mn,"Fit a(1+fc^2/f^2)",do_fit_a_x_1_over_f2_plus_white_noise ,NULL,0,NULL);
  add_item_to_menu(mn, "\0",   NULL,   NULL,   0, NULL);
  add_item_to_menu(mn,"Fit V = ax^n/(x^n+km)",do_fit_MM_auto_with_er,NULL,0,NULL);
  add_item_to_menu(mn,"Boostrap V = ax/(x+km)",do_fit_Y_equal_a_MM_boostrap,NULL,0,NULL);
  add_item_to_menu(mn, "\0",   NULL,   NULL,   0, NULL);
# if !defined(PLAYITSAM) && !defined(PIAS) && !defined(PICOTWIST)  
  add_item_to_menu(mn,"Cumulative histogram",do_cumulative_histogram,NULL,0,NULL);
# endif
  add_item_to_menu(mn,"Fit y = a.x^2", do_fit_a_x2_with_er,NULL,0,NULL);
  add_item_to_menu(mn,"Fit y = a.x", do_fit_a_x_with_er,NULL,0,NULL);
  add_item_to_menu(mn, "\0",   NULL,   NULL,   0, NULL);
# if !defined(PLAYITSAM) && !defined(PIAS) && !defined(PICOTWIST)
  add_item_to_menu(mn,"WLC: Fit xi and L", do_fit_xi_auto_with_er,NULL,0,NULL);
  add_item_to_menu(mn,"WLC: Fit xi and L show", do_fit_xi_auto_with_er_show_E,NULL,0,NULL);
# endif
  add_item_to_menu(mn,"Fit simple FJC Z(F)",do_fit_xL_auto_with_er_FJC_simple,NULL,0,NULL);
  add_item_to_menu(mn,"Fit elastic FJC Z(F)",do_fit_xL_auto_with_er_FJC ,NULL,0,NULL);



  add_item_to_menu(mn,"Fit Cocco elastic FJC Z(F)",do_fit_xL_auto_with_er_FJC_SC,NULL,0,NULL);
  // do_fit_xi_auto_with_er_SC_Brillouin
  add_item_to_menu(mn,"Draw elastic FJC",fjc_curve_elastic ,NULL,0,NULL);
  //add_item_to_menu(mn,"Draw elastic FJC SC",fjc_curve_elastic_SC ,NULL,0,NULL);
  add_item_to_menu(mn,"Draw Brillouin model",langevin_curve_elastic ,NULL,0,NULL);
  add_item_to_menu(mn,"Fit Brillouin model F(Z)",do_fit_xi_auto_with_er_Brillouin ,NULL,0,NULL);



  return mn;
}

int	polymerWLC_main(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  add_plot_treat_menu_item ( "polymerWLC", NULL, polymerWLC_plot_menu(), 0, NULL);
  return D_O_K;
}

int	polymerWLC_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  remove_item_to_menu(plot_treat_menu, "polymerWLC", NULL, NULL);
  return D_O_K;
}
#endif
