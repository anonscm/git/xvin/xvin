#include <utility>
#include <fstream>
#include <boost/python/dict.hpp>
#include "memorized_win_scan.hh"
#include "../pywrapper/core.h"
#include "../pywrapper/core.hpp"
#include "hybridstat.hh"
#include "gui.hh"

namespace hybridstat { namespace all
{
    namespace
    {
        auto _input(Input const & bead)
        {
            distance::Input tmp;
            for(auto const & peak: bead.probabilities)
                tmp.emplace_back(peak.xp, peak.events.size()*1.f);

            return tmp;
        }

        auto _inputs(Inputs const & inp)
        {
            std::vector<distance::Input> peaks;
            peaks.reserve(inp.size());

            for(auto const & bead: inp)
                peaks.emplace_back(_input(bead.second));
            return peaks;
        }

        auto _keys(Inputs const & inp)
        {
            std::vector<Key>  keys;
            keys.reserve(inp.size());
            for(auto const & bead: inp)
                keys.push_back(bead.first);
            return keys;
        }

        template <typename T0, typename T1>
        void _move(T0 & peaks, T1 & inp)
        {
            peaks.resize(inp.size());
            for(size_t k = 0, e = inp.size(); k < e; ++k)
                static_cast<decltype((inp[k]))>(peaks[k]) = std::move(inp[k]);
        }

        auto _read_oligos(std::string oligos)
        {
            constexpr char  diff = 'a'-'A';
            Oligos          volig;
            std::regex      patt("[^;, ]+");
            for(auto i = std::sregex_iterator(oligos.begin(), oligos.end(), patt)
                   , e = std::sregex_iterator(); i != e; ++i)
            {
                std::string x = i->str();
                for(auto & c: x)
                   if(c >= 'A' && c <= 'Z')
                        c = c+diff;

                volig.push_back(x);
            }
            return volig;
        }

        template <typename T>
        auto _read_hairpins(T & cf, bool ask4hp, bool ask4id, Oligos const & oligos)
        {
            auto find = [&](char const * title,
                            char const * files,
                            char const * key,
                            bool         bdo,
                            bool         bask,
                            std::string &save) -> std::string
                {
                    if(!bdo)
                        return "";

                    if(bask || save.size() == 0
                            || save == "unknown"
                            || std::ifstream(save.c_str()).bad())
                        save = openfile(cf, key, title, files);
                    return save;
                };

            auto vhpin = theoretical::read(find("Open hairpin sequence data",
                                                "any file\0*.*\0\0",
                                                "HAIRPIN-DATA",
                                                cf.usehairpins, ask4hp,
                                                cf.hairpinpath));
            auto found = theoretical::find(vhpin, oligos);
            auto mprev = identification::read(find("Open previous report",
                                                   "xlsx file\0*.xlsx\0\0",
                                                   "IDENTIFICATION-DATA",
                                                   cf.usemanualids, ask4id,
                                                   cf.idpath));

            if(cf.dist.useparams && cf.usemanualids)
                cf.dist.params = identification::readParameters(cf.idpath);

            // remove peak-less hairpins
            auto tmp   = std::move(vhpin);
            for(auto     & peaks: found)
                for(auto & hp:    tmp)
                    if(hp.name == peaks.name)
                    {
                        auto it = mprev.find(hp.name);
                        if(it != mprev.end())
                        {
                            hp.beads    = it->second;
                            peaks.beads = it->second;
                        }
                        vhpin.emplace_back(hp);

                        break;
                    }

            return std::make_pair(found, vhpin);
        }

        void _match( SmallArgs       const  & cf
                   , bool                     isref
                   , distance::Input const  & ref
                   , distance::Input        & peaks
                   , Input                  & inp
                   , Result                 & res
                   )
        {
            res.ncycles     = inp.ncycles;
            res.uncertainty = inp.uncertainty;
            _move(res.peaks, inp.probabilities);
            if(isref)
                for(auto & peak: res.peaks)
                    peak.refpos = peak.xp;
            else
            {
                auto prec = cf.uncertainty <= 0. ? inp.uncertainty : cf.uncertainty;
                prec     *= cf.peakdistratio;
                for(auto && m: distance::matched(true, prec, res.dist, peaks, ref))
                    res.peaks[m.first].refpos = ref[m.second].first;
            }
        }

        template <typename T0, typename T1>
        bool _cluster_query(bool bpy, T0 & cf, bool & dontask4hp, bool & dontask4id, T1 fcn)
        {
            char const * unknown = "unknown";
            auto fileinfo = [&](char const *& file, bool & dontask, char const * id)
                        {
                            file = unknown;
                            file = config::get<char const *>(MS_CKEY, id, file);
                            if(file == nullptr || strlen(file) == 0)
                                file = unknown;

                            dontask = strcmp(file, unknown) != 0;
                            if(dontask && !std::ifstream(file).good())
                            {
                                file    = unknown;
                                dontask = false;
                            }
                        };

            char const * hpfile = unknown;
            char const * idfile = unknown;
            fileinfo(hpfile, dontask4hp, "HAIRPIN-DATA");
            fileinfo(idfile, dontask4id, "IDENTIFICATION-DATA");
            if(bpy)
            {
                if(cf.hairpinpath.size())
                    hpfile = cf.hairpinpath.c_str();
                if(cf.idpath.size())
                    idfile = cf.idpath.c_str();
            }

            char        fmt[2048] = "";
            std::string fname     = indexing::filename(indexing::record());
            snprintf(fmt, sizeof(fmt),
                     "File: {\\raw '%s'}\n"
                     "{\\color{yellow}Report style}\n"
                     "\t -Group beads into %%d clusters\n"
                     "{\\color{yellow}Or}\n"
                     "\t -Group beads by hairpins %%b\n"
                     "\t  Use Hairpin file {\\raw '%s'} %%b (requested later if OFF)\n"
                     "\t  Use an ID File %%b (use {\\raw '%s'} %%b)\n"
                     "\t  Use ID File's stretch and bias parameters %%b\n"
                     "\t  Extensions (\\mu/base): in phase 3 %%f, in phase 5 %%f\n"
                     "\t  Use oligos %%s  (comma-separated list)\n",
                     fname.c_str(), hpfile, idfile);

            return fcn( fmt
                      , config::item("CLUSTERS",      cf.nclusters)
                      , config::item("HAIRPINS",      cf.usehairpins)
                      , &dontask4hp, &cf.usemanualids, &dontask4id
                      , config::item("USE-PARAMS",    cf.dist.useparams)
                      , config::item("HAIRPIN-EXT-3", cf.dist.ext3)
                      , config::item("HAIRPIN-EXT-5", cf.dist.ext5)
                      , config::item("OLIGOS",        cf.oligolist)
                      );
        }

        template <typename T>
        void _add(boost::python::dict & dico, config::Item<T> const & x)
        { dico[x.title] = *x; }

        void _add(boost::python::dict &, bool const *) {}

        auto _update_identifiedbeads(SmallArgs const & cf, Inputs const & inp)
        {
            auto tmp = cf.expectedpeaks;
            for(auto & ref: tmp)
                ref.beads.clear();

            size_t i = 0, e = inp.size();
            for(auto it = inp.begin(); i < e; ++i, ++it)
                for(size_t k = 0, ke = cf.expectedpeaks.size(); k < ke; ++k)
                    if(cf.expectedpeaks[k].beads.find(it->first.bead)
                            != cf.expectedpeaks[k].beads.end())
                    {
                        tmp[k].beads.insert(i);
                        break;
                    }

            return tmp;
        }

        auto _update_params(SmallArgs const & cf, Inputs const & inp)
        {
            decltype(cf.dist.params) tmp;

            size_t i = 0, e = inp.size();
            for(auto it = inp.begin(); i < e; ++i, ++it)
                if(cf.dist.params.find(it->first.bead) != cf.dist.params.end())
                    tmp[i] = cf.dist.params.at(it->first.bead);

            return tmp;
        }

        template <typename T>
        bool        _reportinput(T & cf, std::string grname)
        {
            char file[1024] = {0};
            strncpy(file, grname.c_str(), sizeof(file));
            replace_extension(file, file, "xlsx", sizeof(file));
            auto f = savefile(cf, file);
            if(f.size() == 0)
                return true;

            bool dontask4hp, dontask4id;
            if(_cluster_query(false, cf, dontask4hp, dontask4id,
                 [&](auto && ... args)
                 { return reportquery(cf, args...); }))
                return true;

            auto volig = _read_oligos(cf.oligolist);
            auto vhpin = _read_hairpins(cf, !dontask4hp, !dontask4id, volig);
            if(vhpin.second.size() == 0 && cf.usehairpins)
            {
                win_printf_OK("No hairpin data was found");
                return true;
            }

            cf.reportpath       = f;
            cf.oligos           = std::move(volig);
            cf.sequences        = std::move(vhpin.second);
            if(cf.usehairpins)
                cf.expectedpeaks = std::move(vhpin.first);
            else
                cf.expectedpeaks = {};
            return false;
        }
    }

    template<typename T0>
    void  _reportguitopython(bool bpy, T0 const & cf, boost::python::dict dico)
    {
        bool _1, _2;
         _cluster_query(bpy, cf, _1, _2,
                 [&](char const * form, auto && ... args)
                 {
                     dico["help"] = pywrapper::format(form, args...);

                     auto fcn = [&](auto const & x) {  _add(dico, x); return 0; };
                     [](...){}(fcn(args)...);
                     return true;
                 });
    }

    void        reportguitopython(SmallArgs const & cf, boost::python::dict dico)
    { return _reportguitopython(false, cf, dico); }
    void        reportguitopython(BatchArgs const & cf, boost::python::dict dico)
    { return _reportguitopython(true, cf, dico); }

    std::string openfile(SmallArgs const &, char const * key,
                                            char const * title,
                                            char const * files)
    {
        char * f = open_one_file_config(title, nullptr, files, MS_CKEY, key);
        if(f != nullptr && strlen(f) != 0)
            return f;
        return "";
    }

    std::string savefile(SmallArgs const &, char const * file)
    {
        char * f = save_one_file_config("Save hybridization data ",
                                        NULL, file,
                                        "xlsx File\0*.xlsx\0\0",
                                        MS_CKEY, "CSV-LAST-SAVE");
        if(f != nullptr && strlen(f) != 0)
            return f;
        return "";
    }

    bool        reportinput(SmallArgs & cf, std::string grname)
    {   return _reportinput(cf, grname); }
    bool        reportinput(BatchArgs & cf, std::string grname)
    {   return _reportinput(cf, grname); }

    Results     cluster(SmallArgs const & cf, Inputs & inp)
    {
        auto beads     = _inputs(inp);
        auto dist      = distance::run(cf.dist,  beads);

        auto clcf      = cf.clust;
        clcf.nclusters = cf.nclusters;
        auto cl        = cluster ::run(clcf, dist);

        auto keys      = _keys(inp);

        Results res;
        res.reserve(cl.groups.size());
        for(auto const & grp: cl.groups)
        {
            decltype(res.begin()->second) tmp;
            tmp.reserve(grp.second.size());

            auto const & ref = beads[grp.first];
            for(auto item: grp.second)
            {
                distance::Key k(grp.first,item.id);
                auto & bead = inp.at(keys[item.id]);

                Result r;
                r.key        = keys[item.id];
                r.dist       = distance::convert(grp.first, item.id, dist.at(k));
                r.silhouette = item.silhouette;
                _match(cf, item.id == grp.first, ref, beads[item.id], bead, r);
                tmp.emplace_back(r);
            }

            res.emplace_back(keys[grp.first], tmp);
        }
        return res;
    }

    Results     match(SmallArgs const & cf, Inputs & inp)
    {
        auto const   keys  = _keys(inp);
        auto         beads = _inputs(inp);
        auto         mcf   = cf.dist;
        auto const   delta = cf.dist.ext3/cf.dist.ext5;
        if(cf.firstphase > int(phases::pull))
        {
            mcf.height3    = -1.; // discard length info
            mcf.ext3       = 0.;
        }

        auto expectedpeaks = _update_identifiedbeads(cf, inp);
        mcf.params         = _update_params(cf, inp);

        auto         out   = bestmatch::run(mcf, expectedpeaks, beads);
        auto const & cl    = out.second;
        auto const & dist  = out.first;

        auto add = [&](auto & r, auto xp, auto yp, auto & base)
                    {
                        base.xp     = xp;
                        base.yp     = yp;
                        base.refpos = base.xp;
                        base.events = {base};
                        r.peaks.push_back(base);
                        r.peaks.back().good = false; // invalidate probabilities
                    };

        Results res;
        res.reserve(cl.groups.size());
        for(auto const & grp: cl.groups)
        {
            decltype(res.begin()->second) tmp;
            tmp.reserve(grp.second.size());

            auto const & ref = cf.expectedpeaks[grp.first].peaks;
            for(auto item: grp.second)
            {
                Result r;
                if(grp.first == item.id)
                {
                    r.key = {-1, int(grp.first)};
                    Peak base;
                    add(r, 0., 0., base);
                    for(auto const & peak: ref)
                        add(r, peak.first, peak.second, base);
                    if(cf.firstphase <= int(phases::pull))
                        add(r, cf.expectedpeaks[item.id].peaks.length*delta, 0., base);
                }
                else
                {
                    auto id     = item.id-cf.expectedpeaks.size();
                    distance::Key k(grp.first,item.id);
                    auto & bead = inp.at(keys[id]);

                    r.key  = keys[id];
                    r.dist = distance::convert(grp.first, item.id, dist.at(k));
                    _match(cf, false, ref, beads[id], bead, r);
                }

                r.silhouette = item.silhouette;
                tmp.emplace_back(r);
            }

            res.emplace_back(tmp[0].key, tmp);
        }
        return res;
    }

    void updatereport()
    {
        char fmt[2048] = "{\\color{yellow}Update report}\n"
                         "You have changed the stretch and bias for beads.\n"
                         "You can now update peak data accordingly.\n\n"
                         "Use a different file for stretches and biases %b\n"
                         "Maximum peak number of bases to reference peak: %d\n";
        bool ref   = false;
        int  sigma = 20;
        if(config::query(MS_CKEY, fmt,
                         config::item("DIFFERENT-REF", ref),
                         config::item("MAX-BASES",     sigma)))
            return;

        auto get = [](auto title, auto key) -> std::string
            {
                char * f = nullptr;
                f = open_one_file_config(title, nullptr,
                                         "xlsx file\0*.xlsx\0\0",
                                         MS_CKEY, key);
                return f != nullptr && strlen(f) != 0 ? f : "";
            };

        std::string rfile;
        if(ref)
        {

            rfile = get("Open file with biases and stretches",
                        "BIASES-AND-STRETCHES");
            if(rfile.size() == 0)
                return;
        }

        std::string ffile = get("Open file to update", "FILE-TO-UPDATE");
        if(ffile.size() == 0)
            return;

        auto     mod  = pywrapper::import("hybridstat.report");

        boost::python::dict d;
        d["reference"] = rfile;
        d["filename"]  = ffile;
        d["sigma"]     = sigma;
        try
        {
            boost::python::object rep = mod.attr("repair");
            rep(d);
        } catch (boost::python::error_already_set &)
        {
            std::string msg = "{\\color{yellow}CRASH OCCURRED!}\n";
            msg += pywrapper::exception();
            win_printf_OK(msg.c_str());
        }
        startfile(ffile);
    }
}}
