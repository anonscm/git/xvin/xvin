#include <regex>
#include "memorized_win_scan.hh"
#include "../pywrapper/core.h"
#include "../pywrapper/core.hpp"
#include "../trackBead/indexing.h"
#include "../filters/filter_core.h"
#include "../trackBead/draw_track_event_phase_average.hh"
#include "cordrift_core.h"
#include "menu_template.hh"
#include "hybridstat.hh"
#include "gui.hh"

int do_align_cycle_on_phase(void)
{
    if (updating_menu_state != 0)
        return D_O_K;

    /* we first find the data that we need to transform */
    char    tmp[512] = "%pr";
    pltreg *pr       = nullptr;
    if (ac_grep(cur_ac_reg, tmp, &pr) != 1)
        return win_printf_OK("cannot find data");

    auto gr = indexing::record(pr);
    if(gr == nullptr)
    { win_printf_OK("no opened record\n"); return -1; }

    using namespace hybridstat::alignment;
    static Args cf;
    if(query(cf, hybridstat::MS_CKEY))
        return OFF;

    for(auto i = cf.allplots ? 0        : pr->cur_op
           , e = cf.allplots ? pr->n_op : pr->cur_op+1; i < e; ++i)
        if(indexing::belongs(pr, pr->o_p[i]))
        {
            auto plot  = pr->o_p[i];
            auto items = cordrift::cycleddataset(plot);
            auto meds  = compute(cf, gr, items);
            apply(meds, items);

            char tmp [512] = "Data shifted in Y by %g";
            for (int ds_idx = 0; ds_idx < plot->n_dat; ++ds_idx)
            {
                auto cid = indexing::cycleid(plot->dat[ds_idx]);
                if(cid)
                    set_ds_treatement(plot->dat[ds_idx], tmp, meds[*cid]);
            }


            plot->need_to_refresh = 1;
        }

    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}

namespace hybridstat { namespace alignment
{ CONFIG_IMPLEMENT(Args, HYBRIDSTAT_ALIGNMENT_ARGS(),form) }}

namespace hybridstat { namespace hybridization
{ CONFIG_IMPLEMENT(Args, HYBRIDSTAT_HYBRID_ARGS(),form) }}

int do_create_hybridization_pattern_from_phase(void)
{
    using namespace hybridstat;
    auto fcn = [&](auto const cf, auto pr)
    {
        auto g_r = indexing::record(pr);
        if(g_r == nullptr)
        { win_printf_OK("no opened record\n"); return true; }

        std::vector<O_p*> allops = {pr->o_p[pr->cur_op]};
        if(cf.allplots)
            allops.assign(pr->o_p, pr->o_p+pr->n_op);

        for(auto cur_op: allops)
            if(indexing::belongs(pr, cur_op))
            {
                auto cycles = cycleddataset(cur_op);
                auto evts   = intervals::run(intervals::args(cf), g_r, cycles);
                hybridization::run(cf, pr, cur_op, evts);
            }

        refresh_plot(pr, UNCHANGED);
        return false;
    };

    return menu::run_pr<hybridization::Args>(fcn, MS_CKEY,
            "Create time histogram of hybridization time");
}

int do_align_hybridization_pattern_peak(void)
{
    if (updating_menu_state != 0)
        return D_O_K;

    /* we first find the data that we need to transform */
    char    tmp[512] = "%pr";
    pltreg *pr       = nullptr;
    if (ac_grep(cur_ac_reg, tmp, &pr) != 1)
        return win_printf_OK("cannot find data");

    int   all_plot           = 0;
    int   peak_index         = 0;
    float peak_min_threshold = 0.1;
    if(WIN_CANCEL == win_scanf("%b all plot\n"
                               "peak index %5d\n"
                               "peak minimum threashold %5f\n",
                               &all_plot, &peak_index,
                               &peak_min_threshold))
        return OFF;

    for(auto i = all_plot ? 0        : pr->cur_op
           , e = all_plot ? pr->n_op : pr->cur_op+1; i < e; ++i)
    {
        auto cur_op   = pr->o_p[i];
        int  bead_id  = 0;
        int  track_id = 0;
        int  phase_id = 0;
        if (cur_op->filename != NULL
                && sscanf(cur_op->filename, HYB_PATTERN_PLOT_FILENAME,
                          &bead_id, &track_id, &phase_id) == 3)
        {
            align_hybridization_pattern_peak(cur_op->dat[0], peak_index, peak_min_threshold);
            cur_op->need_to_refresh = 1;
        }
    }

    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}

namespace hybridstat { namespace all {
    namespace _detail1
    {
        CONFIG_IMPLEMENT(SmallArgs, HYBRIDSTAT_ALL_SMALLARGS(),smallform)
        CONFIG_IMPLEMENT(Args, HYBRIDSTAT_ARGS(),form)
    }

    namespace _detail2
    {
#       define _MISSING                             \
            ((CLUSTERS,      , nclusters,))         \
            ((HAIRPINS,      , usehairpins,))       \
            ((HAIRPIN-EXT-3, , dist.ext3,))         \
            ((HAIRPIN-EXT-5, , dist.ext5,))         \
            ((OLIGOS,        , oligolist,))         \
            ((STRATEGY,      , dist.strategy,))     \
            ((HAIRPIN-DATA,  , hairpinpath,))       \
            ((IDENTIFICATION-DATA,, idpath,))       \
            ((CSV-LAST-SAVE,      , reportpath,))

        CONFIG_IMPLEMENT(SmallArgs, HYBRIDSTAT_ALL_SMALLARGS() _MISSING,smallform)
        CONFIG_IMPLEMENT(Args,      HYBRIDSTAT_ARGS()          _MISSING,form)
    }

    void load (SmallArgs &cf, char const * key) { _detail2::load(cf, key); }
    void load (Args &cf, char const * key)      { _detail2::load(cf, key); }
    void save (SmallArgs const &cf, char const * key) { _detail2::save(cf, key); }
    void save (Args const &cf, char const * key)      { _detail2::save(cf, key); }

    bool query(SmallArgs &cf, char const * key)
    {
        _detail2::load(cf, key);
        auto r = _detail1::query(cf, key);
        _detail2::save(cf, key);
        return r;
    }

    bool query(Args &cf, char const * key)
    {
        _detail2::load(cf, key);
        auto r = _detail1::query(cf, key);
        _detail2::save(cf, key);
        return r;
    }

    struct ColorSwitcher
    {
        ~ColorSwitcher() { reset(); }
        void add(d_s *ds, bool sameasprev)
        {
            if(ds == nullptr)
                return;

            if(sameasprev && items.size() != 0 && items.back().first->color == scale[cur])
                items.emplace_back(ds, items.back().second);
            else
                items.emplace_back(ds, ds->color);

            set_ds_line_color(ds, scale[cur]);
        }

        void update()
        {
            auto e     = items.size();
            auto last  = scale[scale.size()-1];
            if(move)
            {
                auto nlast = scale[scale.size()-2];
                for(; index < e; ++index)
                {
                    if(items[index].first->color != nlast)
                        break;

                    set_ds_line_color(items[index].first, last);
                }

                for(int i = int(e)-1, k = 0, ie = int(index); i >= ie; --i)
                {
                    if(items[i].first->color != scale[k])
                        ++k;
                    set_ds_line_color(items[i].first, scale[k+1]);
                }
            }
            else
            {
                cur = cur == scale.size()-2 ? size_t(0) : cur+1;
                if(items.size() && items[index].first->color == scale[cur])
                {
                    auto col = items[index].first->color;
                    for(auto e = items.size()
                           ; index < e && items[index].first->color == col
                           ; ++index)
                        set_ds_line_color(items[index].first, last);
                }
            }

        }

        void reset()
        {
            for(auto & pair: items)
                set_ds_line_color(pair.first, pair.second);
            items.resize(0);
            index = 0;
        }

        std::vector<std::pair<d_s*, int>> items;
        std::vector<int>                  scale = {Green, Yellow, Lightred, Lightblue, Lightgray};
        size_t                            index = 0;
        size_t                            cur   = 0;
        bool                              move  = false;
    };

    template <typename CF>
    struct FuncObj
    {
        struct _Item
        {
            Key      key;
            pltreg * pr;
            O_p    * op;
        };
        std::string         _grname;
        CF                  _cf;
        std::vector<_Item>  _inputs;

        bool operator()(CF & cf, pltreg * pr, O_p * cur_op)
        {
            if(indexing::record(pr) == nullptr)
            { win_printf_OK("Could not find track!\n"); return false; }
            if(!indexing::belongs(pr, cur_op))
                return false;

            Key key = {*indexing::trackid(cur_op), *indexing::beadid(cur_op)};
            _inputs.emplace_back(_Item{key, pr, cur_op});
            _grname = indexing::filename(pr);
            _cf     = cf;
            cur_op  = NULL;
            return false;
        }

        void _cordrift(cordrift::all::SmallArgs &  cf,
                       cordrift::DataSet        &  res,
                       pltreg                   *  pr,
                       O_p                      *  op,
                       char                        title[3][256],
                       int                         id,
                       O_p                      *& beadop,
                       ColorSwitcher             & colors)
        {
            if(int(res.size()) < cf.nmin)
                return;

            auto profiles = cordrift::all::run(cf, pr, res);
            colors.update();

            auto  add  = [&](auto const & cor)
                        {
                            int le = int(cor.profile.size());
                            if(le <= 0)
                                return;

                            if(beadop == nullptr)
                            {
                                beadop = create_and_attach_one_plot(pr, le, le, 0);
                                set_plot_file (beadop, title[2]);
                                set_plot_title(beadop, title[1]);
                                if(op != nullptr)
                                    uns_op_2_op_by_type(op,     IS_X_UNIT_SET,
                                                        beadop, IS_X_UNIT_SET);
                            }
                            else
                                create_and_attach_one_ds(beadop, le, le, 0);

                            auto & ds = *beadop->dat[beadop->n_dat-1];
                            set_ds_source(&ds, title[0], id);
                            colors.add(&ds, true);

                            for(size_t i = 0, e = cor.profile.size(); i < e; ++i)
                            {
                                ds.xd[i] = cor.minx+float(i);
                                ds.yd[i] = cor.profile[i];
                            }
                        };

            if(cf.continuity)
                add(profiles.upstream);

            add(profiles.within);
            if(profiles.within.profile.size())
            {
                Data dd(beadop->dat[beadop->n_dat-1]);
                auto uncert = cordrift::intervals::hfsigma(dd);
                char tmp[512] = "%s \\sigma_{HF} = %f";
                set_plot_title(beadop, tmp, title[1], uncert);
            }

            if(cf.continuity)
                add(profiles.downstream);
            menu::refresh(pr, beadop);
        }

        void _alignments()
        {
            auto acf = alignment::args(_cf);
            if(acf.refphase == int(phases::measure))
                return;

            for(auto const & item: _inputs)
            {
                auto g_r    = indexing ::record(item.pr);
                auto cycles = cycleddataset(item.op);
                auto deltas = alignment::compute(acf, g_r, cycles);
                alignment::apply(deltas, cycles);
            }
        }

        void _drifts()
        {
            std::set<void *> done;
            auto check = [&](auto * x)
                         {
                             if(done.find(static_cast<void*>(x)) != done.end())
                                 return true;
                             done.insert(static_cast<void*>(x));
                             return false;
                         };
            O_p * beadop   = nullptr;
            O_p * cycleop  = nullptr;
            char  btitle[][256] = { "Bead %d drift",
                                    "Correlated drifts per bead",
                                    "CordriftPerBead" };
            char  ctitle[][256] = { "Cycle %d remaining drift",
                                    "Correlated drifts per cycle",
                                    "CordriftPerCycle" };
            auto bcf       = cordrift::all::args(_cf);
            bcf.lookdown   = std::numeric_limits<size_t>::max();
            bcf.continuity = _cf.continuityperbead;
            bcf.zero       = cordrift::collapse::ZeroPolicy::min;
            if(_cf.firstphase == _cf.lastphase && _cf.firstphase == int(phases::measure))
                bcf.zero   = cordrift::collapse::ZeroPolicy::measright;

            auto ccf       = cordrift::all::args(_cf);
            ccf.lookdown   = std::numeric_limits<size_t>::max();
            ccf.continuity = _cf.continuitypercycle;
            if(!_cf.driftperbead)
                ccf.continuity = true;

            ccf.zero       = cordrift::collapse::ZeroPolicy::mean;
            if(_cf.firstphase == _cf.lastphase && _cf.firstphase == int(phases::measure))
                ccf.zero   = cordrift::collapse::ZeroPolicy::measright;

            ColorSwitcher bcol, ccol;
            for(auto item: _inputs)
            {
                if(check(item.pr))
                    continue;

                std::map<int, cordrift::DataSet> curr;
                for(auto x: _inputs)
                {
                    if(x.pr != item.pr || check(x.op))
                        continue;

                    auto bid = indexing::beadid(x.op);
                    if(!bid)
                        continue;

                    auto res = cordrift::cycleddataset(x.op);
                    res      = cordrift::clean::run(res);
                    for(auto const & ds: res)
                        if(ds.cycle)
                            curr[ds.cycle.value()].push_back(ds);

                    if(_cf.driftperbead)
                        _cordrift(bcf, res, x.pr, x.op, btitle, bid.value(), beadop, bcol);
                }

                if(_cf.driftpercycle)
                    for(auto & cycle: curr)
                        _cordrift(ccf, cycle.second, item.pr, nullptr, ctitle,
                                  cycle.first, cycleop, ccol);
            }

            auto utitle = [&](auto op, char const * t)
                          {
                              if(op == nullptr)
                                  return;
                              auto vect     = dataset(op->dat, op->dat + op->n_dat);
                              auto uncert   = cordrift::intervals::hfsigma(vect);
                              char tmp[512] = "%s \\sigma_{HF} = %f";
                              set_plot_title(op, tmp, t, uncert);
                              op->need_to_refresh = 1;
                          };
            utitle(beadop,  btitle[1]);
            utitle(cycleop, ctitle[1]);
        }

        void _peak_detection(_Item item, Inputs & inp, std::string & msg)
        {
            auto g_r    = indexing ::record(item.pr);
            auto cycles = cycleddataset(item.op);
            cycles      = cordrift::clean::run(cycles);

            auto evtcf       = intervals::args(_cf);
            evtcf.confidence = _cf.evtconfidence;
            auto evts        = intervals::run(evtcf, g_r, cycles);

            auto acf = alignment::args(_cf);
            if(acf.refphase == int(phases::measure))
            {
                auto deltas = alignment::compute(acf, g_r, evts);
                alignment::apply(deltas, cycles);
            }

            evts        = cordrift::select::run(_cf, item.pr, evts).within;
            auto hres   = hybridization ::run(hybridization::args(_cf),
                                              item.pr, item.op, evts);
            auto probas = probability   ::run(_cf, g_r, evts, hres.second);
            if(probas.size())
            {
                menu::refresh(item.pr, item.pr->o_p[item.pr->n_op-1]);
                auto & res        = inp[item.key];
                res.ncycles       = size_t(item.op->n_dat);
                res.uncertainty   = hres.first;
                res.probabilities = std::move(probas);
            } else
                msg += std::string("\n   - ")+item.op->filename;
        }

        ~FuncObj()
        {
            if(_inputs.size() == 0)
                return;

            _alignments();

            _drifts();

            Inputs      inp;
            std::string msg;
            for(auto const & x: _inputs)
                _peak_detection(x, inp, msg);
            if(msg.size())
            {
                msg = "{\\color{Beads without peaks}}\n" + msg;
                win_printf_OK(msg.c_str());
            }
            if(inp.size() == 0)
                return;

            bool good = true;
            auto pr   = _inputs.front().pr;
            for(auto x: _inputs)
                if(pr != x.pr)
                    good = false;

            if(good && size_t(pr->n_op) == _inputs.size()*3+3)
            {
                std::valarray<O_p*> ops(pr->n_op);
                std::copy(pr->o_p, pr->o_p+pr->n_op, std::begin(ops));
                pr->o_p[1] = ops[_inputs.size()+1];
                pr->o_p[2] = ops[_inputs.size()+2];
                for(size_t k = 0; k < _inputs.size(); ++k)
                {
                    pr->o_p[3*k+3] = ops[k+1];
                    pr->o_p[3*k+4] = ops[_inputs.size()+3+2*k];
                    pr->o_p[3*k+5] = ops[_inputs.size()+3+2*k+1];
                }
                for(auto x: ops)
                    x->need_to_refresh = 1;
                menu::refresh(pr, pr->o_p[pr->n_op-1]);
            }

            if(reportinput(_cf, _grname))
                return;

            for(auto const & expected: _cf.expectedpeaks)
                for(auto const & op: _inputs)
                    if(expected.beads.find(op.key.bead) != expected.beads.end())
                        identification::title(op.op, expected.name);

            auto cl = _cf.expectedpeaks.size() ? match  (_cf, inp)
                                               : cluster(_cf, inp);
            topython(_cf, cl);
            startfile(_cf);
        }
    };

    void run(BatchArgs & cf, pltreg * pr)
    {
        if(pr == nullptr)
            pr = indexing::currentpltreg();

        FuncObj<BatchArgs> obj;
        for(int i = 0; i < pr->n_op; ++i)
            obj(cf, pr, pr->o_p[i]);
    }

    void run(BatchArgs & cf, pltreg * pr, O_p * op)
    {
        if(op == nullptr)
            op = indexing::currentplot();
        FuncObj<BatchArgs>()(cf, pr, op);
    }
}}


int do_get_methylation_time_histograms(void)
{
    using namespace hybridstat::all;
    return trackrenorm::run<Args>(FuncObj<Args>(), hybridstat::MS_CKEY,
                                  "compute hybridstat histogram");
}

int do_renormalize_and_hybridstat(void)
{
    using namespace hybridstat::all;
    return trackrenorm::run<SmallArgs>(FuncObj<SmallArgs>(), hybridstat::MS_CKEY,
                        "renormalize beads and compute hybridstat histogram");
}

int do_identify_beads(void)
{
    if (updating_menu_state != 0)
        return D_O_K;

    auto lst = hybridstat::identification::querykeys();
    if(lst.size() == 0)
        return D_O_K;

    auto plt = indexing::currentpltreg();
    auto res = hybridstat::identification::queryplots(lst, plt);
    hybridstat::identification::titles(res, plt);

    auto sfile = indexing::filename(plt);
    if(sfile.rfind('.') != std::string::npos)
        sfile.resize(sfile.rfind('.'));
    sfile += "_identification.xlsx";

    char file[2048];
    strcpy(file, sfile.c_str());
    auto fname = save_one_file_config("Save identification list",
                                      nullptr, file,
                                      "xlsx File\0*.xlsx\0\0",
                                      hybridstat::MS_CKEY,
                                      "IDENTIFICATION-DATA");

    if(fname != nullptr && strlen(fname))
        hybridstat::identification::write(fname, res);
    return D_O_K;
}

int do_update_report(void)
{
    if (updating_menu_state != 0)
        return D_O_K;

    hybridstat::all::updatereport();
    return D_O_K;
}
PYMENU_IMPL_ALL(hybridstat,HYBRIDSTAT_MENUS)
