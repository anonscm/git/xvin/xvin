#!/usr/bin/env python3
"""
Defines basic hybridstat related report objects and functions
"""
from typing                 import Tuple, Iterator
from typing                 import Callable, cast
from abc                    import abstractmethod

import numpy

from ._type                 import Peak, Key, Bead
from ._genericreport        import Reporter
from ._recursiveimports     import PeaksClass

PULL_PHASE  = 3

def _isref(bead:Bead):
    return bead.key.track == -1

class _Base(Reporter):
    def __init__(self, arg, **kwargs):
        super().__init__(arg)
        if isinstance(arg, _Base):
            base        = cast(_Base, arg)
            self.groups = base.groups
            self.hpins  = base.hpins
            self.oligos = base.oligos
            self.config = base.config
            self.keys   = base.keys
            self.haslengthpeak = base.haslengthpeak
        else:
            self.groups = kwargs['groups']             # type: Groups
            self.hpins  = kwargs['hairpins']           # type: Hairpins
            self.oligos = kwargs['oligos']             # type: Oligos
            self.config = kwargs['config']             # type: dict
            self.keys   = dict()                       # type: Dict[Key,str]
            self.haslengthpeak = self.config['firstphase'] <= PULL_PHASE

            if len(self.keys) == 0:
                for _, bead in self.beads():
                    if _isref(bead):
                        self.keys[bead.key] = self.hpins[bead.key.bead].name
                    else:
                        self.keys[bead.key] = u"T{0}B{1}".format(*bead.key)

    def beads(self) -> Iterator[Tuple[Key,Bead]]:
        u"iterates through groups"
        for k, beads     in self.groups:
            for bead     in beads:
                yield k, bead

    def groupbeads(self, spe = None) -> Iterator[Bead]:
        u"iterates through beads in one group"
        for k, beads     in self.groups:
            if k == spe:
                for bead in beads:
                    if bead.key != spe:
                        yield bead
                break

    def peaks(self, ref:Key, ypos:float) -> Iterator[Tuple[Bead,Peak]]:
        u"iterate over reference peaks in the group"
        for bead in self.groupbeads(ref):
            for peak in bead.peaks:
                if peak.ref is None:
                    continue
                if abs(peak.ref-ypos) < 1.:
                    yield bead, peak

    def baseunits(self)-> str:
        u"Returns the unit value for bases"
        return u"µm" if self.nohairpin() else u"base"

    def basefmt(self)-> type:
        u"Returns the format type for bases"
        return float if self.nohairpin() else int

    def nohairpin(self)-> bool:
        u"returns true if no hairpin was provided"
        return len(self.hpins) == 0

    def uncertainties(self):
        u"returns uncertainties for all beads"
        if len(self.hpins):
            sigmas = iter(bead for ref, bead in self.beads() if ref != bead.key)
            sigmas = iter(bead.uncertainty*bead.distance.stretch for bead in sigmas)
            sigmas = numpy.fromiter(sigmas, numpy.float)
        else:
            sigmas = numpy.fromiter((bead.uncertainty for _, bead in self.beads()),
                                    numpy.float)
        return sigmas

    @abstractmethod
    def iterate(self):
        u"Iterates through sheet's base objects and their hierarchy"

class _ChartCreator(object):
    _ERR    = .015
    _WIDTH  = 400
    _HUNIT  = 18
    def __init__(self,
                 parent     : _Base,
                 height     : Callable,
                ) -> None:
        if isinstance(parent, PeaksClass()):
            peaks = parent
        else:
            peaks = PeaksClass()(parent)
        self._pos    = tuple(peaks.columnindex(u'Peak Position in Reference',
                                               u'Hybridisation Rate'))
        self._book   = parent.book
        self._height = height
        self._rseries= None      # type: dict
        self._row    = peaks.tablerow()+1

    def _args(self, nrows:int, tpe:str, color:str) -> dict:
        u"return args for creating a chart"
        base   = PeaksClass().sheet_name
        def _pos(i):
            return [base, self._row, self._pos[i], self._row+nrows-1, self._pos[i]]
        return dict(name         = [base, self._row, 0],
                    categories   = _pos(0),
                    values       = _pos(1),
                    marker       = dict(type  = tpe,
                                        #size  = 0.1,
                                        fill  = dict(color = color)),
                    x_error_bars = dict(type  = 'fixed',
                                        value = self._ERR,
                                        line  = dict(color = color)))

    def bead(self, ref: Key, bead: Bead):
        u"returns a chart for this bead"
        return self.peaks(ref, bead, bead.peaks[0])

    def peaks(self, ref: Key, bead: Bead, peak: Peak):
        u"returns a chart for this bead if peak is peaks zero"
        if bead.peaks[0] != peak:
            return

        chart = self._book.add_chart(dict(type = 'scatter'))

        if ref != bead.key:
            series = self._args(len(bead.peaks), 'circle', 'green')
            chart.add_series(series)
        else:
            self._rseries = self._args(len(bead.peaks), 'square', 'blue')

        chart.add_series(self._rseries)
        chart.set_title (dict(none  = True))
        chart.set_legend(dict(none  = True))
        axis = dict(major_gridlines = dict(visible = False),
                    label_position  = "none",
                    visible         = True)
        chart.set_x_axis(axis)
        chart.set_y_axis(axis)
        chart.set_size  (dict(width  = self._WIDTH,
                              height = self._HUNIT*self._height(bead)))
        self._row += len(bead.peaks)
        return chart
