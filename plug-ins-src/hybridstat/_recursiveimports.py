#!/usr/bin/env python3
u"Deals with recursive imports"

def SummaryClass():
    from ._summary import SummarySheet
    return SummarySheet

def PeaksClass():
    from ._peaks import PeaksSheet
    return PeaksSheet

def EventsClass():
    from ._events import EventsSheet
    return EventsSheet
