#include <regex>
#include "hybridstat.hh"
#include "menu_template.hh"
#include "gui.hh"
namespace hybridstat { namespace identification {
    Flags querykeys()
    {
        std::string keys;
        if(config::query( MS_CKEY
                        , "{\\color{yellow}Identify plots}\n"
                          "Flags (comma-separated list):\n%s\n"
                        , config::item("FLAGS", keys)))
            return {};

        if(keys.size() == 0)
            return {};

        decltype(querykeys()) lst;
        std::regex patt("[^;,]+");
        for(auto i = std::sregex_iterator(keys.begin(), keys.end(), patt)
               , e = std::sregex_iterator(); i != e; ++i)
        {
            auto x = trim(i->str());
            if(x.size())
                lst.push_back(x);
        }
        return lst;
    }


    Identifiers queryplots(Flags const & lst, plot_region *plt)
    {
        std::string dialog;
        for(auto const & name: lst)
            dialog   += "%r " + name + "\n";

        int val = 0;
        Identifiers res;
        for(auto op: indexing::plots(indexing::BEADPATTERN, plt))
        {
            auto id = indexing::beadid(indexing::BEADPATTERN, op);

            char tmp[2024];
            snprintf(tmp, sizeof(tmp), "{\\color{yellow}Define flag for bead %d}\n"
                                       "%%R No flag\n%s", *id, dialog.c_str());
            menu::refresh(plt, op);
            if(config::query("", tmp, val))
                return {};

            if(val > 0)
                res[lst[val-1]].insert(*id);
        }
        return res;
    }

    void title(one_plot * op, std::string name)
    {
        std::string t(op->title);
        std::regex  r("[^{]*Bead");
        std::string x(name + ": Bead");
        std::string n(std::regex_replace(t, r, x.c_str()));
        op->need_to_refresh = 1;
        set_plot_title(op, n.c_str());
    }

    void titles(Identifiers const & res, plot_region * plt)
    {
        for(auto op: indexing::plots(indexing::BEADPATTERN, plt))
        {
            auto id = indexing::beadid(indexing::BEADPATTERN, op);
            for(auto x: res)
                if(x.second.find(*id) != x.second.end())
                {
                    title(op, x.first);
                    break;
                }
        }
        refresh_plot(plt, UNCHANGED);
    }
}}
