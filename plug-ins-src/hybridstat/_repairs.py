#!/usr/bin/env python3
# -*- coding: utf-8 -*-
u"read-write existing files"
from    typing          import Optional, Dict, Any # pylint: disable=unused-import
import  os
import  re
import  shutil
import  tempfile

from    zipfile         import ZipFile, ZIP_STORED, ZipInfo
from    xml.etree       import ElementTree
from    pandas          import DataFrame

import  numpy
import  openpyxl
from    openpyxl.utils  import column_index_from_string as getcolindex

class UpdateableZipFile(ZipFile):
    """
    Add delete (via remove_file) and update (via writestr and write methods) To
    enable update features use UpdateableZipFile with the 'with statement',
    Upon  __exit__ (if updates were applied) a new zip file will override the
    exiting one with the updates
    """

    DeleteMarker = type('DeleteMarker', tuple(), dict())

    def __init__(self, file, mode="r", compression=ZIP_STORED, allowZip64=False):
        # Init base
        super(UpdateableZipFile, self).__init__(file, mode=mode,
                                                compression=compression,
                                                allowZip64=allowZip64)
        # track file to override in zip
        self._replace = {}
        # Whether the with statement was called
        self._allow_updates = False

    def writestr(self, zinfo_or_arcname, byts, compress_type=None):
        if isinstance(zinfo_or_arcname, ZipInfo):
            name = zinfo_or_arcname.filename
        else:
            name = zinfo_or_arcname
        # If the file exits, and needs to be overridden,
        # mark the entry, and create a temp-file for it
        # we allow this only if the with statement is used
        if self._allow_updates and name in self.namelist():
            temp_file = self._replace[name] = self._replace.get(name,
                                                                tempfile.TemporaryFile())
            temp_file.write(byts)
        # Otherwise just act normally
        else:
            super(UpdateableZipFile, self).writestr(zinfo_or_arcname,
                                                    byts, compress_type=compress_type)

    def write(self, filename, arcname=None, compress_type=None):
        arcname = arcname or filename
        # If the file exits, and needs to be overridden,
        # mark the entry, and create a temp-file for it
        # we allow this only if the with statement is used
        if self._allow_updates and arcname in self.namelist():
            temp_file = self._replace[arcname] = self._replace.get(arcname,
                                                                   tempfile.TemporaryFile())
            with open(filename, "rb") as source:
                shutil.copyfileobj(source, temp_file)
        # Otherwise just act normally
        else:
            super(UpdateableZipFile, self).write(filename,
                                                 arcname=arcname, compress_type=compress_type)

    def __enter__(self):
        # Allow updates
        self._allow_updates = True
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        # call base to close zip file, organically
        try:
            super(UpdateableZipFile, self).__exit__(exc_type, exc_val, exc_tb)
            if len(self._replace) > 0:
                self._rebuild_zip()
        finally:
            # In case rebuild zip failed,
            # be sure to still release all the temp files
            self._close_all_temp_files()
            self._allow_updates = False

    def _close_all_temp_files(self):
        for temp_file in self._replace.values():
            if hasattr(temp_file, 'close'):
                temp_file.close()

    def remove_file(self, path):
        u"removes a file from the archive"
        self._replace[path] = self.DeleteMarker()

    def _rebuild_zip(self):
        tempdir = tempfile.mkdtemp()
        try:
            temp_zip_path = os.path.join(tempdir, 'new.zip')
            with ZipFile(self.filename, 'r') as zip_read:
                # Create new zip with assigned properties
                with ZipFile(temp_zip_path, 'w', compression=self.compression,
                             allowZip64=self._allowZip64) as zip_write:
                    for item in zip_read.infolist():
                        # Check if the file should be replaced / or deleted
                        replacement = self._replace.get(item.filename, None)
                        # If marked for deletion, do not copy file to new zipfile
                        if isinstance(replacement, self.DeleteMarker):
                            del self._replace[item.filename]
                            continue
                        # If marked for replacement, copy temp_file, instead of old file
                        elif replacement is not None:
                            del self._replace[item.filename]
                            # Write replacement to archive,
                            # and then close it (deleting the temp file)
                            replacement.seek(0)
                            data = replacement.read()
                            replacement.close()
                        else:
                            data = zip_read.read(item.filename)
                        zip_write.writestr(item, data)
            # Override the archive with the updated one
            shutil.move(temp_zip_path, self.filename)
        finally:
            shutil.rmtree(tempdir)

class ColumnData(DataFrame):
    u"read-write existing files"
    def __init__(self, fname, sheet, args):
        book = openpyxl.load_workbook(fname, read_only = True)#, data_only = True)
        rows = enumerate(book[sheet].iter_rows())
        for _, row in rows:
            cols = tuple((col.value, i) for i, col in enumerate(row) if col.value in args)
            if len(cols):
                break

        if len(cols) != len(args):
            raise KeyError('missing columns %s in file'
                           % (set(name for name, _ in cols) - set(args)))

        vals = dict((name, []) for name , iC in cols)
        inds = []
        for i, row in rows:
            if all(col.value is None for col in row):
                break

            for name, j in cols:
                vals[name].append(args[name](row[j].value))
            inds.append(i+1)

        super().__init__(data  = vals, index = inds)
        self._fname = fname
        self._sheet = sheet
        self._args  = args
        self._cols  = cols

    def paste(self, filename, key):
        u"pastes values into a file"
        if filename == self._fname:
            return

        other = ColumnData(filename, self._sheet, self._args)
        keys  = frozenset(self[key].unique()) & frozenset(other[key].unique())

        cols  = list(name        for name, _    in  self._cols)
        dcols = list(name        for name       in cols if name != key)
        icols = dict((name, ind) for name, ind  in self._cols)
        diffs = []
        equ   = lambda i, j: i == j or all((numpy.isnan(i), numpy.isnan(j)))
        for elem in keys:
            vother = other.loc[other[key] == elem, dcols]
            vself  = self .loc[self [key] == elem, dcols]
            for icol, name in enumerate(dcols):
                if equ(vother.values[0][icol], vself.values[0][icol]):
                    continue

                diffs.append((vother.index[0], icols[name]))

            other.loc[other[key] == elem, dcols] = self .loc[self [key] == elem, cols]

        return other.replaceinfile(*dcols, diffs = frozenset(diffs))

    def getcolindex(self, name):
        u"returns the column index"
        for col, icol in self._cols:
            if col == name:
                return icol

    def replaceinfile(self, *cols, diffs = None):
        u"replaces values in a file"
        if diffs is not None and len(diffs) == 0:
            return
        if isinstance(diffs, dict):
            diffs = frozenset(diffs.items())

        args  = dict(self._cols)
        icols = tuple(args[col] for col in cols)
        cols  = dict((j,i) for i,j in self._cols)

        def _getid():
            book = openpyxl.load_workbook(self._fname,
                                          read_only = True,
                                          data_only = True)
            return book.get_sheet_names().index(self._sheet)+1
        sheet = "xl/worksheets/sheet%d.xml" % _getid()


        def _itercells(tree):
            recol = re.compile(r"([A-Z]+)(\d+)")
            data  = next(item for item in tree if item.tag.endswith("sheetData"))

            for col in iter(col for row in data for col in row):
                match = recol.match(col.get("r"))
                irow  = int(match.group(2))

                icol  = getcolindex(match.group(1))-1
                if diffs is not None:
                    if (irow, icol) not in diffs:
                        continue
                elif irow not in self.index or icol not in icols:
                    continue

                yield (icol, irow, col)

        with UpdateableZipFile(self._fname) as stream:
            tree = ElementTree.fromstring(stream.read(sheet))

            for icol, irow, col in  _itercells(tree):
                val = self[cols[icol]][irow]
                if numpy.isnan(val):
                    if len(col):
                        col.remove(col[0])
                else:
                    if len(col) == 0:
                        ElementTree.SubElement(col, "s:v")

                    col[0].text = str(val)

            stream.writestr(sheet, ElementTree.tostring(tree))

class _RunRepairs:
    PPOS     = 'Peak Position'
    RPEAKCOL = 'Reference Peak'
    UFO      = u'Unidentified Peak Count'
    def __init__(self, args):
        self._sigma = args['sigma']
        self._path  = args['filename']
        self._ref   = args.get('reference', self._path)

    @staticmethod
    def _int(xxx):
        if xxx is None:
            return None
        try:
            return int(xxx)
        except ValueError:
            return None

    @staticmethod
    def _float(xxx):
        if xxx is None:
            return None
        try:
            return float(xxx)
        except ValueError:
            return None

    def pasteStreches(self):
        u"pastes stretches from one file to another"
        if self._ref != self._path:
            summ = ColumnData(self._ref, "Summary",
                              dict(Bead    = str,
                                   Stretch = self._float,
                                   Bias    = self._float))
            summ.paste(self._path, 'Bead')

    @classmethod
    def getDefaultSummaryColumns(cls):
        u"returns default summary columns"
        return dict(Bead    = str,
                    Stretch = cls._float,
                    Bias    = cls._float)

    def getSummary(self, *args, cols: 'Optional[Dict[str,Any]]' = None, **kwargs):
        u"Opens and returns the summary"
        if cols is None:
            cols = self.getDefaultSummaryColumns()

        cols = dict(cols)
        cols.update(kwargs)
        cols.update(dict(args))
        return ColumnData(self._path, "Summary", cols)

    summary = property(getSummary)

    @classmethod
    def getDefaultPeaksColumns(cls):
        u"returns default peak columns"
        return {'Bead': str, 'Reference': str,
                cls.PPOS:     cls._float,
                cls.RPEAKCOL: cls._float}

    def getPeaks(self, *args, cols = None, **kwargs):
        u"Opens and returns the peaks"
        if cols is None:
            cols = self.getDefaultPeaksColumns()

        cols = dict(cols)
        cols.update(kwargs)
        cols.update(dict(args))
        return ColumnData(self._path, "Peaks", cols)

    peaks = property(getPeaks)

    def changeReferencePeaks(self):
        u"Computes and updates reference peaks"
        # import pyxvin so as to initialize numpy
        # pylint: disable=import-error
        import  pyxvin              # pylint: disable=unused-import
        from ._core import batch    # pylint: disable=no-name-in-module
        refpeaks = dict()
        changed  = set()
        peaks    = self.peaks
        icol     = peaks.getcolindex(self.RPEAKCOL)
        for bead in peaks.Bead.unique():
            data = peaks[peaks.Bead == bead]
            ref  = data.Reference.unique()[0]
            if ref == bead:
                continue

            if ref not in refpeaks:
                tmp           = peaks[peaks.Bead == ref][self.PPOS]
                refpeaks[ref] = tmp[tmp > 0.].values

            params   = (self.summary
                        [['Stretch', 'Bias']]
                        [self.summary.Bead == bead].values[0])
            match    = batch.matchpeaks(True, self._sigma, refpeaks[ref],
                                        params[0] * data[self.PPOS].values + params[1])

            peaks.loc[data.index[1:], self.RPEAKCOL] = numpy.nan
            changed.update((irow, icol) for irow in data.index[1:])
            if len(match):
                inds = data[self.PPOS].index[list(j for i, j in match)]
                peaks.loc[inds, self.RPEAKCOL] = [[refpeaks[ref][i]] for i, j in match]

        peaks.replaceinfile(self.RPEAKCOL, diffs = changed)

    def changeRefVariables(self):
        u"Computes and updates times and rates for the reference peaks"
        cols  = tuple(u'Hybridisation ' + name
                      for name in (u'Rate', u'Time',
                                   u'Time Probability',
                                   u'Time Uncertainty'))
        peaks = self.getPeaks(*((col, self._float) for col in cols))
        def _med(ite):
            if len(ite) == 0 or all(numpy.isnan(val) for val in ite.values):
                return numpy.nan
            elif len(ite.values) == 1:
                return ite.values[0]
            else:
                return numpy.median(ite.values)

        vals  = (peaks
                 .loc[peaks.Bead != peaks.Reference]
                 .loc[lambda df: df[self.RPEAKCOL] != numpy.nan]
                 .groupby(['Reference', self.RPEAKCOL])
                 .aggregate(dict.fromkeys(cols, _med)))

        peaks.loc[peaks.Bead == peaks.Reference, cols] = numpy.nan

        for ref in vals.index.levels[0].values:
            ind = peaks.loc[peaks.Bead == ref].index
            if len(ind) == 0:
                continue

            for col in cols:
                cur = dict(vals.loc[ref, col].items())
                peaks.loc[ind, col] = (peaks.loc[ind, self.RPEAKCOL]
                                       .map(lambda val: cur.get(val, numpy.nan)))

                icol = peaks.getcolindex(col)
                peaks.replaceinfile(col, diffs = dict.fromkeys(ind.values, icol))

    def changeUnidentifiedPeaks(self):
        u"""Counts the number of unidentified peaks"""
        try:
            summ = self.getSummary(cols = {u'Bead': str, self.UFO: self._int})
        except KeyError:
            return # no Unidentified Peaks column

        peaks = self.getPeaks(cols = {u'Bead': str, self.RPEAKCOL: self._int})

        fcn   = lambda x: sum(1 for i in x if numpy.isnan(i))
        count = dict(peaks.groupby(u'Bead').agg({self.RPEAKCOL: fcn})[self.RPEAKCOL].items())

        inds = []
        for i in range(len(summ)):
            cnt = count[summ.Bead.values[i]]
            if cnt != summ[self.UFO].values[i]:
                inds.append(summ.index.values[i])
                summ.loc[inds[-1], self.UFO] = cnt

        diffs = dict.fromkeys(inds, summ.getcolindex(self.UFO))
        summ.replaceinfile(self.UFO, diffs = diffs)

    def run(self):
        u""" repairs peak matching """
        self.pasteStreches()
        self.changeReferencePeaks()
        self.changeRefVariables()
        self.changeUnidentifiedPeaks()

def run(args):
    u"Repairs a file"
    _RunRepairs(args).run()
