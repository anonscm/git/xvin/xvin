#pragma once
#include <boost/optional.hpp>
#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include <type_traits>
#include "cordrift_core.h"
#include "hybridstat_theoretical.h"
#include "hybridstat_cluster.h"
struct data_set;
namespace hybridstat
{
    constexpr char const * MS_CKEY = "HYBRIDSTAT";
    using cordrift::phases;
    using cordrift::Data;
    using cordrift::DataSet;
    using cordrift::dataset;
    using cordrift::cycleddataset;
    using cordrift::data_t;

    namespace localmax
    {
        struct Args
        {
            float  sigma      = 1.f;
            size_t ninc       = 2lu;
            float  xthreshold = -1.;
            float  ythreshold = -1.;
        };

        using Results = std::vector<int>;
        Results  run(Args,  std::vector<float> const &);
        Results  run(float, std::vector<float> const &);
    }

    namespace maxcoordinates
    {
        using localmax::Args;

        using Results = std::vector<std::pair<float, float>>;
        Results run(Args,  size_t, float const*, float const*, float const*);
        Results run(float, size_t, float const*, float const*, float const*);
        Results run(float, float, size_t, float const*, float const*);
        Results run(float, float, size_t, float const*, float const*, float const*);
    }

    namespace zeroclip
    {
        struct Args
        {
            float wMax;
            float wZero;
            float rPeak = 1.1f;
            size_t ninc = 2lu;
        };

        struct Results
        {
            std::pair<float,float> zero       = {0., 0.};
            std::pair<float,float> max        = {0., 0.};

            float   maxPosition ()        const { return max.second;  }
            float   zeroPosition()        const { return zero.second; }
            float   clip(Args const & cf) const { return max.first*cf.rPeak; }
            bool    good(Args const & cf) const { return clip(cf) < zero.first; }

            bool    operator == (zeroclip::Results const & b) const
            { return zero == b.zero && max == b.max; }
        };

        inline  Args args(float cf)     { return {cf*1.5f, cf*2.5f, 1.1f}; }
        template <typename T>
        inline  decltype(&T::sigma, Args{}) args(T const & cf) { return args(cf.sigma); }

        Results compute(Args cf, size_t nx, float const * xs, float const * ys);
        bool    apply  (Args cf, Results const & res, size_t nx, float * ys);
        bool    run    (Args cf, size_t nx, float const * xs, float * ys);
    }

    namespace histogram
    {
        struct Args
        {
            enum class Type { kGaussian, kSquare };

            bool   reg      = true;
            float  sz       = 1.f;
            float  bin      = .2;
            float  sigma    = 1.;
            float  maxabs   = 20.;
            Type   type     = Type::kGaussian;
        };

        struct Results
        {
            float               ymin, ymax, bin;
            std::vector<float>  data;
        };

        Results run(Args cf, std::vector<data_set const*> phases);
        Results run(Args cf, DataSet const    & phases);

#       define MS_HIST_HELPER(A,B,C,D)                          \
            inline Args A(float cf)                             \
            { return { B, D, cf/5.f, cf, 20., Args::Type::C}; } \
            template<typename T>                                \
            inline decltype(&T::sigma, Args{}) A(T const & cf)  \
            { return A(cf.sigma); }

        MS_HIST_HELPER(gaussian,     true,  kGaussian, 5.f)
        MS_HIST_HELPER(square,       true,  kSquare,   2.f)
        MS_HIST_HELPER(timegaussian, false, kGaussian, 5.f)
        MS_HIST_HELPER(timesquare,   false, kSquare,   2.f)
#       undef MS_HIST_HELPER
    }

    namespace intervals
    {
#       define HYBRIDSTAT_INTERVALS_SMALLARGS(X)            \
            CORDRIFT_INTERVAL_SMALLARGS(X)                  \
            CORDRIFT_SELECT_SMALLARGS(X)

#       define HYBRIDSTAT_INTERVALS_FORM                    \
            "{\\color{yellow}Flat stretch detection}\n"     \
            CORDRIFT_INTERVAL_SMALLFORM                     \
            CORDRIFT_SELECT_FORM

#       define HYBRIDSTAT_INTERVALS_ARGS(X)                 \
            ((ALL-PLOT,  int   ,  X allplots,  1))          \
            HYBRIDSTAT_INTERVALS_SMALLARGS(X)

        // [lookup, lookdown] range within which events are
        // considered to be in decreasing Z
#       define HYBRIDSTAT_INTERVALS_HIDDEN_ARGS(X)          \
            (( , size_t, lookdown, 4))                      \
            (( , size_t, lookup,   5))

        char const form [] = "On all plots: %b\n\n" HYBRIDSTAT_INTERVALS_FORM;

        CONFIG_DECLARE(Args, HYBRIDSTAT_INTERVALS_ARGS() HYBRIDSTAT_INTERVALS_HIDDEN_ARGS())
        CONFIG_COPY_FUNC(Args,args,HYBRIDSTAT_INTERVALS_SMALLARGS())

        using Results = DataSet;
        Results run(Args, gen_record  const *, DataSet const &);
        Results run(Args, plot_region const *, DataSet const &);
    }

    namespace alignment
    {
#       define HYBRIDSTAT_ALIGNMENT_HIDDENARGS(X)   \
            ((, int ,  X refrange,   3))            \
            ((, int ,  X refprec,    5))            \
            ((, int ,  X refgau,     5))            \
            ((, int ,  X refrepeats, 6))

#       define HYBRIDSTAT_ALIGNMENT_SMALLARGS(X)                        \
            ((ALIGN-PHASE       ,  int   ,  X refphase,  int(phases::pull)))

#       define HYBRIDSTAT_ALIGNMENT_ARGS(X)                             \
            ((ALL-PLOT          ,  int   ,  X allplots,  1))            \
            HYBRIDSTAT_ALIGNMENT_SMALLARGS(X)

#       define HYBRIDSTAT_ALIGNMENT_FORM                                \
            "\n{\\color{yellow}Align on phase} %05d (-1 for no alignment)\n"

        char const form [] = "On all plots: %b" HYBRIDSTAT_ALIGNMENT_FORM;

        CONFIG_DECLARE(Args, HYBRIDSTAT_ALIGNMENT_ARGS() HYBRIDSTAT_ALIGNMENT_HIDDENARGS())
        CONFIG_COPY_FUNC(Args,args,HYBRIDSTAT_ALIGNMENT_SMALLARGS())

        using Results = std::vector<double>;
        Results compute(Args, gen_record  const *, DataSet const &);
        Results compute(Args, plot_region const *, DataSet const &);
        void    apply  (Results const &,  DataSet &);
    }

    namespace hybridization
    {
#       define HYBRIDSTAT_HYBRID_SPECIFIC_ARGS(X)                       \
            ((EVENTS-CONFIDENCE ,  float ,  X evtconfidence ,  0.01))   \
            ((SIGMA             ,  float ,  X sigma         ,  0.003))  \
            ((CLIP-ZERO-PEAK    ,  int   ,  X clip          ,  1))      \
            ((MTHRES            ,  int   ,  X threshold     ,  5))

#       define HYBRIDSTAT_HYBRID_SMALL_ARGS(X)      \
            HYBRIDSTAT_INTERVALS_SMALLARGS(X)       \
            HYBRIDSTAT_ALIGNMENT_SMALLARGS(X)       \
            HYBRIDSTAT_HYBRID_SPECIFIC_ARGS(X)

#       define HYBRIDSTAT_HYBRID_ARGS(X)                                    \
            ((ALL-PLOT          ,  int   ,  X allplots,  1))                \
            HYBRIDSTAT_HYBRID_SMALL_ARGS(X)

#       define HYBRIDSTAT_HYBRID_HIDDEN_ARGS(X)             \
            ((, float, rate, 30.f)) // rate of time frames (30Hz)

#       define HYBRIDSTAT_HYBRID_SPECIFIC_FORM               \
            HYBRIDSTAT_ALIGNMENT_FORM                        \
            "\n{\\color{yellow}Create histograms}\n"         \
            "Select events with a confidence level of %6f\n" \
            "Smearing: %5f. Clip peak at x = 0: %b\n"    \
            "Discard peaks with less than %5d events\n"

#       define HYBRIDSTAT_HYBRID_FORM                       \
            HYBRIDSTAT_INTERVALS_FORM                       \
            HYBRIDSTAT_HYBRID_SPECIFIC_FORM

        char const form [] = "On all plots: %b\n\n" HYBRIDSTAT_HYBRID_FORM;
        CONFIG_DECLARE(Args, HYBRIDSTAT_HYBRID_ARGS() HYBRIDSTAT_HYBRID_HIDDEN_ARGS())
        CONFIG_COPY_FUNC(Args,args,HYBRIDSTAT_HYBRID_SMALL_ARGS())
    }

    namespace probability
    {

       /* Computing the MLE for the probability of an hybridization such that:
        * #. It was observed at during at least conf.nmin time
        * #. It disappeared after a time nmax
        *
        * Consider the de-hibridization probability rho between measures n and n+1.
        * Then:
        * #. The probability of hybridization lasting K measures is
        * proportional to: p (1-p)^K
        * #. The probability of hybridization lasting until the end of the cycle
        * is proportional to: (1-p)^N
        *
        * Normalizing them, we find: P(K) = p (1-p)^(K-D), P(N) = (1-p)^(N-D)
        * where: D = conf.nmin
        *
        * Where: N_k is the number of events wich lasted k, reaching the cycle end
        *        n_k is the number of events wich lasted k, not reaching the cycle end
        * the maximum likelihood estimation (MLE) is:
        *  max_p(\product_k (p^{n_k} (1-p)^{(k-D) (n_k+N_k)}))
        *  <=> max(\sum_k n_k log(p) + \sum_k (k-D) (n_k+N_k) log(1-p))
        *  <=> max((1-\rho) log(p) + (lambda-D) log(1-p))
        *      where \rho    = \sum_k N_k / \sum_k (n_k + N_k)
        *            \lambda = \sum_k (n_k + N_k) k / \sum_k (n_k + N_k)
        *  <=> p = (1-\rho)/(1-\rho-D + \lambda), after taking the derivative.
        *
        * For estimating the standard error, \rho and \lambda are considered
        * to be independant variables, \rho following a binomial law and
        * \lambda being the average of a geometric law.
        *
        * we then have : \sigma^2 ~   (p^2/(1-\rho))^2 \sigma_\lambda^2
        *                          + (p(1-p)/(1-\rho))^2 \sigma_\rho^2
        * i.e.
        *      \sigma^2 ~ p^2/(1-\rho)^2 (1-p) + \rho/(1-\rho)^2 p^2 (1-p)^{N-D+2}(1-(1-p))^{N-D}
        *
        * as (1-p)^{N-D}(1-(1-p))^{N-D} <= 1/2^{N-D}  and N-D >> 1
        *      \sigma   ~ p/(1-\rho) x sqrt(1-p)
        *
        * Moving back to time, with F the frame rate,
        * we have (1-p)^{t*F} = exp(t * F ln(1-p)) => T = 1/(F ln(1-p))
        */
        struct Probability
        {
            Probability(size_t n) : nmin(n) {}
            template <typename T>
            Probability(std::enable_if_t<std::is_class<T>::value, T> const & c)
                : nmin(c.nmin) {}

            void operator() (float, bool);

            bool  good       ()             const;
            float probability()             const;
            float stddev     ()             const;
            float uncertainty(float rate)   const;
            float time       (float rate)   const;

            size_t nmin;
            private:
                size_t _count = 0;
                size_t _big   = 0;
                float  _sum   = 0.0;
        };

        struct Position
        {
            float xp     = 0.f;
            float yp     = 0.f;
            bool  good   = true;
        };

        struct Result: public Position
        {
            float prob   = 0.f;
            float time   = 0.f;
            float uncert = 0.f;
            std::vector<Position> events;
        };

        using Results = std::vector<Result>;

        struct _PPImpl;
        struct PeakProbabilities
        {
            PeakProbabilities (size_t nmin, float sigma);
            ~PeakProbabilities();

            void    peak    (float xp, float yp);
            void    event   (float const *, size_t, size_t, size_t);
            Results results (float rate) const;
            private:
                friend struct _PPImpl;
                _PPImpl * _impl;
        };
    }

    namespace all
    {
        using Sequences     = theoretical::Sequences;
        using SequencePeaks = theoretical::Results;
        using Oligos        = theoretical::Oligos;
#       define HYBRIDSTAT_ALL_SMALLARGS(X)                                  \
            BOOST_PP_SEQ_POP_BACK(CORDRIFT_ALL_SMALLARGS(X))                \
            ((PER-BEAD          ,  int   ,  X driftperbead      ,  1))      \
            ((PER-CYCLE         ,  int   ,  X driftpercycle     ,  1))      \
            HYBRIDSTAT_ALIGNMENT_SMALLARGS(X)                               \
            HYBRIDSTAT_HYBRID_SPECIFIC_ARGS(X)                              \

#       define HYBRIDSTAT_REPORT_ARGS(X)                                \
            ((,  std::string,    X reportpath,      {}))                \
            ((,  Sequences,      X sequences,       {}))                \
            ((,  SequencePeaks,  X expectedpeaks,   {}))                \
            ((,  Oligos,         X oligos,          {}))                \
            ((,  int   ,         X nclusters,       1))

#       define HYBRIDSTAT_ARGS(X)                                       \
            ((ALL-PLOT          ,  int   ,  X allplots,  1))            \
            HYBRIDSTAT_ALL_SMALLARGS(X)

#       define HYBRIDSTAT_ALL_SMALL_FORM                                            \
            CORDRIFT_ALL_SMALLFORM                                                  \
            "Remove drift: per beads across cycles %b, per cycle across beads %b\n" \
            HYBRIDSTAT_HYBRID_SPECIFIC_FORM

        char const smallform [] = HYBRIDSTAT_ALL_SMALL_FORM;
        struct SmallArgs
        {
            CONFIG_STRUCT_DECLARE(HYBRIDSTAT_ALL_SMALLARGS())
            CONFIG_STRUCT_DECLARE(HYBRIDSTAT_REPORT_ARGS())
            cluster  ::Args clust;
            bestmatch::Args dist;

            bool  continuityperbead  = true;
            bool  continuitypercycle = false;
            float peakdistratio      = 8.;
            float rate               = 30.f;     // rate of time frames (30Hz)

            bool         usehairpins  = true;
            bool         usemanualids = false;
            std::string  oligolist    = "";
            std::string  hairpinpath  = "";
            std::string  idpath       = "";
        };
        CONFIG_FUNC_DECLARE(SmallArgs)

        struct BatchArgs: public SmallArgs {};

        void run(BatchArgs & cf, plot_region * pr = nullptr);
        void run(BatchArgs & cf, plot_region * pr, one_plot * op = nullptr);

        char const form [] = "%b all plot\n" HYBRIDSTAT_ALL_SMALL_FORM;
        struct Args : public SmallArgs
        { int allplots = 1; };
        CONFIG_FUNC_DECLARE(Args)
    }
}
