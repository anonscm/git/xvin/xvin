#include <regex>
#include "../pywrapper/core.h"
#include "../pywrapper/core.hpp"
#include <boost/python/stl_iterator.hpp>
#include "hybridstat.hh"
#include "gui.hh"

namespace hybridstat { namespace all
{
    namespace bp = boost::python;
    void reportguitopython(SmallArgs const &, bp::dict);
    void reportguitopython(BatchArgs const &, bp::dict);

    namespace
    {
        constexpr char const NAME[] = "hybridstat.report";
        using pywrapper::topython;
        template <typename T0>
        auto _topython(T0 const & cf)
        {
            bp::dict items;
            CONFIG_PYWRAPPER(cf, items, HYBRIDSTAT_ALL_SMALLARGS(),smallform)
            reportguitopython(cf, items);
            return items;
        }

        bp::object topython(Sequences const & out)
        {
            auto mod     = pywrapper::import(NAME);
            auto Hairpin = mod.attr("Hairpin");
            auto hpfcn   = [&](auto const & hp)
                           {
                               auto beads = topython(hp.beads);
                               return Hairpin(hp.name, hp.seq, beads);
                           };

            return topython(out, hpfcn);
        }

        bp::object topython(Results const & out)
        {
            auto mod      = pywrapper::import(NAME);
            auto Proba    = mod.attr("Probability");
            auto Peak     = mod.attr("Peak");
            auto Distance = mod.attr("Distance");
            auto Position = mod.attr("Position");
            auto Bead     = mod.attr("Bead");
            auto Id       = mod.attr("Key");
            auto Group    = mod.attr("Group");
            auto None     = bp::object();

            std::map<hybridstat::all::Key, bp::object, hybridstat::all::Cmp> keys;
            auto get_key = [&](hybridstat::all::Key const & k)
                {
                    auto it = keys.find(k);
                    if(it != keys.end())
                        return it->second;

                    return (keys[k] = Id(k.track, k.bead));
                };

            auto eventfcn = [&](auto const & i)
                { return Position(i.xp, i.yp, i.good); };

            auto probfcn  = [&](auto const & i)
                {
                    auto prob = i.good   ? Proba(i.prob, i.time, i.uncert): None;
                    auto rp   = i.refpos ? bp::object(i.refpos.get())     : None;
                    auto evts = topython(i.events, eventfcn);
                    return Peak(eventfcn(i), prob, rp, evts);
                };

            auto resfcn   = [&](auto const & i)
                {
                    auto key   = get_key(i.key);
                    auto dist  = Distance(i.dist.dist, i.dist.dyn, i.dist.bias);
                    auto peaks = topython(i.peaks, probfcn);
                    return Bead(key, i.ncycles, i.uncertainty, i.silhouette, peaks, dist);
                };

            auto medfcn  = [&](auto const & med)
                { return Group(get_key(med.first), topython(med.second, resfcn)); };

            bp::object obj = topython(out, medfcn);
            return obj;
        }
    }

    template <typename T0>
    void _topython(T0 const & cf, Results const & res)
    {
        bp::dict d;
        pywrapper::runandprinterr([&]{ d["filename"] = cf.reportpath; });
        pywrapper::runandprinterr([&]{ d["config"]   = _topython(cf); });
        pywrapper::runandprinterr([&]{ d["hairpins"] = topython(cf.sequences); });
        pywrapper::runandprinterr([&]{ d["oligos"]   = topython(cf.oligos); });
        pywrapper::runandprinterr([&]{ d["groups"]   = topython(res); });
        pywrapper::runandprinterr([&]{ d["git"]      = pywrapper::gitinfo(); });
        pywrapper::guirunmodule(NAME, d);
    }
    void topython(SmallArgs const & cf, Results const & res)
    { return _topython(cf, res); }
    void topython(BatchArgs const & cf, Results const & res)
    { return _topython(cf, res); }

    void startfile(std::string path)
    { pywrapper::import("hybridstat.report").attr("startfile")(path); }
}}

namespace hybridstat { namespace identification {
    namespace bp = boost::python;
    using pywrapper::topython;

    Identifiers read(std::string file)
    {
        if(file.size() == 0)
            return {};
        auto        mod  = pywrapper::import("hybridstat.identification");
        bp::object  pyrt = mod.attr("read")(file);
        Identifiers rt;
        for(bp::stl_input_iterator<bp::object> it(pyrt), e; it != e; ++it)
        {
            std::string name = bp::extract<std::string>((*it)[0]);

            std::set<int> beads;
            for(bp::stl_input_iterator<int> iB((*it)[1]), eB; iB != eB; ++iB)
                beads.insert(*iB);

            rt[name] = beads;
        }
        return rt;
    }

    Params readParameters(std::string file)
    {
        if(file.size() == 0)
            return {};
        auto        mod  = pywrapper::import("hybridstat.identification");
        bp::object  pyrt = mod.attr("readparams")(file);
        Params rt;
        for(bp::stl_input_iterator<bp::object> it(pyrt), e; it != e; ++it)
            rt[bp::extract<int>((*it)[0])] = { bp::extract<double>((*it)[1]),
                                               bp::extract<double>((*it)[2]) };
        return rt;
    }

    void write(std::string file, Identifiers const & items)
    {
        if(file.size() == 0)
            return;

        auto     mod  = pywrapper::import("hybridstat.identification");
        bp::list res;
        for(auto const & it: items)
            res.append(bp::make_tuple(it.first, topython(it.second)));
        mod.attr("write")(file, res);
    }
}}
