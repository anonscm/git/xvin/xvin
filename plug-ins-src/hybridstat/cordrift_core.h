#pragma once
#include <map>
#include <vector>
#include <valarray>
#include <boost/preprocessor/seq/pop_back.hpp>
#include "memorized_win_scan.hh"
#include "ompbase.h"
#include "../trackBead/indexing.h"
#include "../filters/filter_core.h"

#   define CORDRIFT_COPY(X) CONFIG_COPY(cf, args, BOOST_PP_SEQ_POP_BACK(X()));
#   define NEW_PLOT_FORM "Place result in a new plot %b\n"

extern "C" { struct data_set; struct plot_region; struct gen_record; };
namespace cordrift
{
    enum class phases: int
    {
        pull    = 3,
        measure = 5
    };

    using data_t = float;
    struct Data
    {
        using cid_t = indexing::opt_t<int>;
        Data()              = default;
        Data(Data const &)  = default;
        Data(Data      &&)  = default;
        Data(data_set &x) : Data(&x) {}
        Data(data_set *x);

        Data & operator =  (Data const &) = default;
        Data & operator =  (Data      &&) = default;
        template <typename T>
        Data & operator = (T & x) { return operator=(Data(x)); }
        template <typename T>
        Data & operator = (T * x) { return operator=(Data(x)); }

        bool   operator <  (Data const & r) const;
        Data & operator += (std::pair<size_t, size_t> const & p);
        Data   operator +  (std::pair<size_t, size_t> const & r) const { return Data(*this) += r; }

        data_t   * xd     = nullptr;
        data_t   * yd     = nullptr;
        size_t     nx     = 0;
        data_set * parent = nullptr;
        cid_t      cycle;
    };

    struct MemData: public Data
    {
        MemData()                 = default;
        MemData(MemData const &)  = default;
        MemData(MemData      &&)  = default;

        MemData(data_set const *x) : MemData(*x) {}
        MemData(data_set const &x) : MemData(Data(const_cast<data_set&>(x))) {}
        MemData(Data     const &x);

        MemData & operator =  (MemData const &) = default;
        MemData & operator =  (MemData      &&) = default;
        template <typename T>
        MemData & operator = (T & x) { return operator=(MemData(x)); }
        template <typename T>
        MemData & operator = (T * x) { return operator=(MemData(x)); }
        private:
            std::valarray<data_t> _xd;
            std::valarray<data_t> _yd;
    };

    using DataSet    = std::vector<Data>;
    using MemDataSet = std::vector<MemData>;

    template <typename T>
    inline DataSet dataset(T b, T e)
    {
        DataSet tmp;
        std::for_each(b, e, [&](auto x) { tmp.emplace_back(Data(x)); });
        return tmp;
    }

    template <typename T>
    inline auto dataset(T * b)
    -> decltype(b->dat, DataSet())
    { return dataset(b->dat, b->dat+b->n_dat); }

    template <typename T>
    inline auto dataset(T & b)
    -> decltype(std::begin(b), DataSet())
    { return dataset(std::begin(b), std::end(b)); }

    template <typename T>
    inline DataSet cycleddataset(T b, T e)
    {
        DataSet tmp;
        std::for_each(b, e, [&](auto x)
                            {
                                tmp.emplace_back(Data(x));
                                tmp.back().cycle = indexing::cycleid(x);
                            });
        return tmp;
    }

    template <typename T>
    inline auto cycleddataset(T * b)
    -> decltype(b->dat, DataSet())
    { return cycleddataset(b->dat, b->dat+b->n_dat); }

    template <typename T>
    inline auto cycleddataset(T & b)
    -> decltype(std::begin(b), DataSet())
    { return cycleddataset(std::begin(b), std::end(b)); }

    namespace clean
    {
        struct Args
        {
            float minval = -10.f;
            float maxval = 10.f;
        };

        using Ranges = std::vector<std::pair<data_t*, data_t*>>;
        bool    check  (Args const &, data_t);
        Ranges  compute(Args const &, Data const &);
        void    apply  (Ranges const &, Data       &);

        DataSet run(Args cf, DataSet const & ds);
        inline DataSet run(DataSet const & ds) { Args cf; return run(cf, ds); }
    }

    namespace intervals
    {
#       define CORDRIFT_INTERVAL_SMALLARGS(X)                            \
            ((intervals_filter,         int,    X bfilter,      1))      \
            ((intervals_uncertainty,    data_t, X uncertainty,  .0))     \
            ((intervals_confidence,     data_t, X confidence,   .1))     \
            ((intervals_delta,          int,    X dn,           1))      \
            ((intervals_nmin,           int,    X nmin,         5))      \
            ((intervals_nsides,         int,    X nei,          0))

#       define CORDRIFT_INTERVAL_HIDDEN_ARGS(X)                          \
            ((, size_t, X lookdown, std::numeric_limits<size_t>::max())) \
            ((, size_t, X lookup,   std::numeric_limits<size_t>::max()))

#       define CORDRIFT_INTERVAL_ARGS(X)                               \
            CORDRIFT_INTERVAL_SMALLARGS(X)                             \
            ((intervals_all,        int,   X all_ds,            1))    \
            ((intervals_new,        int,   X new_plt,           1))

        CONFIG_DECLARE(Args, CORDRIFT_INTERVAL_ARGS() CORDRIFT_INTERVAL_HIDDEN_ARGS())
        CONFIG_COPY_FUNC(Args, args, CORDRIFT_INTERVAL_SMALLARGS() CORDRIFT_INTERVAL_HIDDEN_ARGS())

#       define CORDRIFT_INTERVAL_SMALLFORM                                      \
            "Filter %b for a noise level of %6f (0. = automated estimation)\n"  \
            "Confidence level: %6f\n (lower = longer events)"                   \
            "Derivate averaged over %4d measures (lower = noisier)\n"           \
            "Keep events with >= %4d measure, after removing %4d from both sides.\n"

#       define CORDRIFT_INTERVAL_FORM           \
            CORDRIFT_INTERVAL_SMALLFORM         \
            "Proceed on all data sets %b\n"

        static char const form [] = CORDRIFT_INTERVAL_FORM NEW_PLOT_FORM;

        using rng_t     = std::pair<size_t, size_t>;
        using Intervals = std::vector<rng_t>;
        using Results   = std::vector<rng_t>;
        Results run     (Args const &, size_t, size_t, Data const &);

        double  hfsigma (Data    const &);
        double  hfsigma (DataSet const &);
        DataSet run     (Args, plot_region const *, DataSet const &);
        DataSet run     (Args, gen_record  const *, DataSet const &);
    }

    namespace select
    {
#       define CORDRIFT_SELECT_SMALLARGS(X)                                 \
            ((first_phase, int,     X firstphase, int(phases::pull)))       \
            ((last_phase,  int,     X lastphase,  int(phases::measure)))

#       define CORDRIFT_SELECT_HIDDEN_ARGS(X)           \
            ((,            data_t,   X minratio,   .5f))

#       define CORDRIFT_SELECT_FORM "Phase used: from %5d to %5d\n"
        static char const form [] = CORDRIFT_SELECT_FORM;

        CONFIG_DECLARE  (Args, CORDRIFT_SELECT_SMALLARGS() CORDRIFT_SELECT_HIDDEN_ARGS())
        inline Args args(int x) { return  Args{x, x}; }

        CONFIG_COPY_FUNC(Args, args, CORDRIFT_SELECT_SMALLARGS())

        struct Results { DataSet upstream, within, downstream; };

        int compare(std::pair<int,int>, Data const &, data_t ratio = .5f);
        int compare(int, int, Data const &, data_t ratio = .5f);

        Results run(Args const &, plot_region const *, DataSet const &);
        Results run(Args const &, gen_record  const *, DataSet const &);

        template <typename T0, typename T1>
        inline auto run(T0 const & a, T1 const * plt, DataSet const & rng)
        { Args cf = select::args(a); return run(cf, plt, rng); }

        using PhaseIntervals = std::vector<std::pair<int,int>>;
        using PhaseResults   = std::vector<DataSet>;
        PhaseResults run(PhaseIntervals, gen_record  const *, DataSet const &, data_t = 0.5f);
        PhaseResults run(PhaseIntervals, plot_region const *, DataSet const &, data_t = 0.5f);

        DataSet unique(int, plot_region const *, DataSet const &);
        DataSet unique(int, gen_record  const *, DataSet const &);
    };

    namespace collapse
    {
#       define CORDRIFT_COLLAPSE_SMALLARGS(X)                   \
            ((collapse_useintervals, int, X useintervals,  0))  \
            ((collapse_driftsmear,   int, X nsmear,  0))

#       define CORDRIFT_COLLAPSE_ARGS(X)                        \
            CORDRIFT_COLLAPSE_SMALLARGS(X)                      \
            ((collapse_new,         int, X new_plt, 1))

        enum class ZeroPolicy { mean, min, measmin, measmean, measright };
#       define CORDRIFT_COLLAPSE_HIDDEN_SMALLARGS(X)                \
            (( , data_t,      X rho,        .5))                    \
            (( , data_t,      X endthr,     0.3f))                  \
            (( , bool,        X continuity, true))                  \
            (( , float,       X meddev,     25.f))                  \
            (( , ZeroPolicy,  X zero,       ZeroPolicy::measright)) \
            (( , int,         X estlength,  10))

#       define CORDRIFT_COLLAPSE_HIDDEN_ARGS(X)     \
            CORDRIFT_COLLAPSE_HIDDEN_SMALLARGS(X)   \
            ((, int,        X nmin,         5))     \
            ((, int,        X dn,           1))     \
            ((, data_t,     X confidence,   .1))    \
            ((, int,        X bfilter,      1))     \
            ((, data_t,     X uncertainty,  .0))

        CONFIG_DECLARE(Args, CORDRIFT_COLLAPSE_ARGS() CORDRIFT_COLLAPSE_HIDDEN_ARGS())

#       define CORDRIFT_COLLAPSE_FORM                                                   \
         "Estimate using median derivates %R, mean derivates %r or flat intervals %r\n" \
         "Smooth the drift with a gaussian kernel size of %d\n"

        static char const form [] =
            "This routine collapse along y datasets to generate an average\n"
            CORDRIFT_COLLAPSE_FORM NEW_PLOT_FORM;

        CONFIG_COPY_FUNC(Args, args, CORDRIFT_COLLAPSE_SMALLARGS() CORDRIFT_COLLAPSE_HIDDEN_ARGS())

        struct Profile
        {
            std::valarray<data_t> profile;
            std::valarray<data_t> counts;
            int                   minx, maxx;
        };
        using Results = Profile;

        Profile run(Args const &, DataSet const &);
        template <typename T>
        inline Profile run(T const & a, DataSet const & x)
        { auto cf = collapse::args(a); return run(cf, x); }

        struct Profiles { Profile upstream, within, downstream; };
    }

    namespace remove
    {
#       define CORDRIFT_REMOVE_ARGS ((remove_new, int, new_plt, 1))

        CONFIG_DECLARE(Args, CORDRIFT_REMOVE_ARGS)

        static char const form [] = NEW_PLOT_FORM;
        using collapse::Profile;

        void            run(Profile const & a, Data & b);

        template <typename T>
        inline void     run(Profile const & res, T & items)
        {
            int e = int(items.size());
            PRAGMA_OMP(parallel for)
            for(int i = 0; i < e; ++i)
                run(res, items[i]);
        }
    }

    namespace all
    {
#       define CORDRIFT_ALL_SMALLARGS(X)                        \
            CORDRIFT_INTERVAL_SMALLARGS(X)                      \
            CORDRIFT_SELECT_SMALLARGS(X)                        \
            BOOST_PP_SEQ_POP_BACK(CORDRIFT_COLLAPSE_ARGS(X))    \
            ((all_new, int, X new_plt,   1))

#       define CORDRIFT_ALL_SMALLFORM                           \
            "\n{\\color{yellow}Flat stretch detection:}\n"      \
            CORDRIFT_INTERVAL_SMALLFORM                         \
            CORDRIFT_SELECT_FORM                                \
            "\n{\\color{yellow}Drift profile:}\n"               \
            CORDRIFT_COLLAPSE_FORM                              \

#       define CORDRIFT_ALL_HIDDEN_ARGS(X)                      \
            CORDRIFT_INTERVAL_HIDDEN_ARGS(X)                    \
            CORDRIFT_SELECT_HIDDEN_ARGS(X)                      \
            CORDRIFT_COLLAPSE_HIDDEN_SMALLARGS(X)

        static char const smallform [] =
            CORDRIFT_ALL_SMALLFORM "\n" NEW_PLOT_FORM;

        using ZeroPolicy = collapse::ZeroPolicy;
        CONFIG_DECLARE(SmallArgs, CORDRIFT_ALL_SMALLARGS() CORDRIFT_ALL_HIDDEN_ARGS())

#       define CORDRIFT_ALL_ARGS()                      \
            ((all_full, int, allplots,      0))         \
            CORDRIFT_ALL_SMALLARGS()

        static char const form      [] =
            "{\\color{yellow}Apply:} to all plots %b.\n\n"
            CORDRIFT_ALL_SMALLFORM "\n" NEW_PLOT_FORM;

        struct Args : public SmallArgs
        { int allplots = 0; };

        using Results = DataSet;
        using collapse::Profiles;

        Profiles run(SmallArgs const &, DataSet &);
        Profiles run(SmallArgs const &, plot_region const *, DataSet &);
        Profiles run(SmallArgs const &, gen_record  const *, DataSet &);

        CONFIG_COPY_FUNC(SmallArgs, args, BOOST_PP_SEQ_POP_BACK(CORDRIFT_ALL_SMALLARGS()))

        template <typename T>
        inline void run(T const & a, DataSet &res)
        { auto cf = all::args(a); run(cf, res); }

        template <typename T>
        inline void run(T const & a, DataSet &res, Profiles & cor)
        { auto cf = all::args(a); run(cf, res, cor); }
    }
}
