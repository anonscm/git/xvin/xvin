#include <list>
#include "../filters/accumulators.h"
#include "../filters/stattests.h"
#include "ompbase.h"

#include "xvin.h"
#include "cordrift_core.h"
namespace cordrift
{
    Data::Data(data_set *x) : xd(x->xd), yd(x->yd), nx(x->nx), parent(x) {}
    bool  Data::operator < (Data const & r) const
    {
        return xd[nx-1] == r.xd[r.nx-1]
             ? nx > r.nx : xd[nx-1] >  r.xd[r.nx-1];
    }

    Data &  Data::operator += (std::pair<size_t, size_t> const & r)
    {
        xd += r.first;
        yd += r.first;
        nx  = r.second-r.first;
        return *this;
    }

    MemData::MemData(Data const & ds)
        : Data(ds)
        , _xd (ds.xd, ds.nx)
        , _yd (ds.yd, ds.nx)
    {
        xd = &_xd[0];
        yd = &_yd[0];
    };

    namespace
    {
        using items_t = std::vector<struct data_set *>;
        using namespace stats;

        constexpr auto none  = std::numeric_limits<size_t>::max();

        size_t _index(data_t a, data_t b) { return floor(std::max(0.f, a-b)+0.5); }
    }

    namespace clean
    {
        bool check(Args const & cf, data_t y)
        { return y >= cf.minval && y <= cf.maxval; }

        Ranges  compute(Args const & cf, Data const & ds)
        {
            Ranges rngs;
            bool looking = true;
            for(auto y0 = ds.yd, y = y0, e = y+ds.nx; y != e; ++y)
                if(check(cf, y[0]) ^ looking)
                {
                    if(looking)
                        rngs.emplace_back(y, e);
                    else
                        rngs.back().second = y;
                    looking = !looking;
                }
            return rngs;
        }

        void apply(Ranges const & rngs, Data & ds)
        {
            for(auto rng: rngs)
            {
                if(rng.first == ds.yd)
                {
                    if(rng.second == ds.yd+ds.nx)
                        ds  = Data();
                    else
                        ds += std::make_pair(0, rng.first-ds.yd);

                } else if(rng.second == ds.yd+ds.nx)
                    ds.nx -= rng.second-rng.first;
                else
                    std::fill(rng.first, rng.second, rng.second[0]);
            }
        }

        DataSet run(Args cf, DataSet const & items)
        {
            DataSet res(items);
            int e = int(res.size());

            PRAGMA_OMP(parallel for)
            for(int i = 0; i < e; ++i)
            {
                Data & ds = res[i];
                auto rngs = compute(cf, ds);
                apply(rngs, ds);
            }

            auto it = std::remove_if(res.begin(), res.end(),
                                     [](auto & ds) { return ds.nx == 0; });
            res.resize(it-res.begin());
            return res;
        }
    }

    namespace intervals
    {
        CONFIG_IMPLEMENT(Args,CORDRIFT_INTERVAL_ARGS(), form)
        namespace
        {
            namespace sn = samples::normal;
            namespace ks = samples::normal::knownsigma;


            struct _Rng: public sn::Input
            {
                using sn::Input::Input;
                _Rng(data_t const * ptr, size_t i1, size_t i2)
                {
                    acc_t<bat::count, bat::mean> vals(i2-i1, ptr+i1);
                    count = size_t(ba::count(vals));
                    mean  = ba::mean (vals);
                    first = i1;
                    last  = i2;
                }

                size_t  first;
                size_t  last;
            };

            struct _Pairs: std::vector<_Rng>
            {
                _Pairs(Args const & cf, size_t lookdown, size_t lookup)
                    : _args(cf)
                    , _eq_thr(ks::threshold(true,  cf.confidence, cf.uncertainty))
                    , _lt_thr(ks::threshold(false, cf.confidence, cf.uncertainty))
                    , _len_thr(size_t(cf.nmin+2*cf.nei))
                    , _lookdown(cf.lookdown ? lookdown : none)
                    , _lookup  (cf.lookdown ? lookup   : none)
                {}

                void add(data_t const * raw, data_t const * filt, size_t & iFirst, size_t iLast)
                {
                    if(iFirst == none)
                        return;

                    _Rng rng(filt, iFirst, iLast);
                    auto isbad = [&](auto ind)
                                 { return std::abs(raw[ind]-rng.mean) > _eq_thr; };

                    auto merge = [&](auto const & now)
                                 {
                                     if(size() == 0)
                                         return false;
                                     if(_lookdown < now.first && now.first < _lookup)
                                          return ks::value(false, now, back())
                                               > _lt_thr;
                                     else
                                          return ks::value(true,  now, back())
                                               < _eq_thr;
                                 };

                    while(rng.first < rng.last   && isbad(rng.first))
                        ++rng.first;

                    while(rng.first < rng.last-1 && isbad(rng.last-1))
                        --rng.last;

                    iFirst = none;
                    if(rng.first +1 >= rng.last)
                        return;

                    _Rng now(filt, rng.first, rng.last);
                    if(merge(now))
                    {
                        if(_lookdown > now.first)
                            back() = _Rng(filt, back().first, rng.last);
                        else
                        {
                            back().last = now.last;
                            if(now.mean < back().mean)
                                static_cast<sn::Input&>(back()) = now;
                        }
                    } else
                        emplace_back(now);
                }

                Results filter() const
                {
                    Results res;
                    for(auto const & x: *this)
                        if(x.last-x.first >= _len_thr)
                            res.push_back({x.first+_args.nei, x.last-_args.nei});
                    return res;
                }

                private:
                    Args    const & _args;
                    data_t  const   _eq_thr;
                    data_t  const   _lt_thr;
                    size_t  const   _len_thr;
                    size_t  const   _lookdown;
                    size_t  const   _lookup;
            };

            struct _Filter: std::valarray<data_t>
            {
                data_t const * get(Args const & args, size_t sz, data_t const *p)
                {
                    namespace kn  = samples::normal::knownsigma;
                    if(args.bfilter == 0)
                        return p;
                    resize(sz);
                    std::copy(p, p+sz, std::begin(*this));

                    filter::xvnonlin::Args cfilt;
                    cfilt.precision = args.uncertainty;
                    filter::xvnonlin::run(cfilt, *this);
                    return &(*this)[0];
                }
            };

            template <typename T1, typename T2>
            auto _apply(Args    const &  args,
                        size_t           lookdown,
                        size_t           lookup,
                        Data    const &  ds,
                        data_t  const *  filt,
                        T1            && left,
                        T2            && right
                       )
            {
                _Pairs pairs(args, lookdown, lookup);
                auto       cur       = none;
                auto const e         = ds.nx - (size_t(args.dn) < ds.nx ? args.dn: ds.nx);
                auto const threshold = ks::threshold(true, args.confidence,
                                                           args.uncertainty,
                                                           args.dn, args.dn);

                for(size_t i = args.dn; i < e; ++i)
                {
                    auto l = left (filt+i-1);
                    auto r = right(filt+i+args.dn-1);
                    if(std::abs(l-r) < threshold)
                    {
                        if(cur == none)
                            cur = i-1;
                    } else
                        pairs.add(ds.yd, filt, cur, i);
                }

                pairs.add(ds.yd, filt, cur, e);

                return pairs.filter();
            }

            template <typename T>
            inline DataSet _run(Args cf, T const * plt, DataSet const & resa)
            {
                DataSet res;
                for(auto & ds: resa)
                {
                    size_t j = 0, i = 1;
                    for(; i < ds.nx; ++i)
                        if(ds.xd[i]-ds.xd[i-1] > 1.1)
                        {
                            if(int(i-1-j) > cf.nmin)
                                res.emplace_back(ds+std::make_pair(j, i-1));
                            j = i;
                        }
                    if(int(i-1-j) > cf.nmin)
                        res.emplace_back(ds+std::make_pair(j, i-1));
                }

                if(cf.uncertainty <= 0.)
                    cf.uncertainty = hfsigma(res);
                assert(cf.uncertainty > 0. && cf.uncertainty < 100.);

                DataSet mem;
                omp::mapreduce( mem, res,
                                [&](auto & cur, auto const & ds)
                                {
                                    int  id       = ds.cycle ? *ds.cycle : -1;
                                    auto lookdown = none;
                                    auto lookup   = none;
                                    if(cf.lookdown != none && ds.cycle)
                                    {
                                        auto phr = indexing::phaserange
                                                       (plt, id, int(cf.lookdown));
                                        if(phr)
                                            lookdown = _index(phr->first, ds.xd[0]);
                                    }
                                    if(cf.lookup != none && ds.cycle)
                                    {
                                        auto phr = indexing::phaserange
                                                       (plt, id, int(cf.lookup));
                                        if(phr)
                                            lookup = _index(phr->second, ds.xd[0]);
                                    }
                                    auto cutRes = run(cf, lookdown, lookup, ds);
                                    cur.reserve(cur.size()+cutRes.size());

                                    for(auto const & rng: cutRes)
                                        cur.emplace_back(ds+rng);
                                },
                                [](auto & a, auto && b)
                                {
                                    a.reserve(a.size()+b.size());
                                    std::copy(b.begin(), b.end(), std::back_inserter(a));
                                }
                              );

                std::sort(mem.begin(), mem.end());
                return mem;
            }
        }

        Results run(Args const & args, size_t lookdown, size_t lookup, Data const & ds)
        {
            _Filter mem;
            auto filt = mem.get(args, ds.nx, ds.yd);

            if(args.dn <= 1)
                return _apply(args, lookdown, lookup, ds, filt,
                       [](data_t const * x) { return x[0]; },
                       [](data_t const * x) { return x[0]; });
            else
            {
                RARolling<bat::mean> lm(args.dn, filt)
                                   , rm(args.dn, filt+args.dn);

                return _apply(args, lookdown, lookup, ds, filt,
                       [&lm](data_t const * x) { lm(x); return ba::mean(lm); },
                       [&rm](data_t const * x) { rm(x); return ba::mean(rm); });
            }
        }

        template <typename T>
        typename T::value_type hfsigma(T const & res)
        {
            acc_t<bat::mediandeviation> quant;
            for(size_t i = 1; i < res.size(); ++i)
                quant(res[i]-res[i-1]);
            return stats::compute(quant);
        }

        double hfsigma(Data const & res)
        {
            acc_t<bat::mediandeviation> quant;
            for(size_t i = 1; i < res.nx; ++i)
                quant(res.yd[i]-res.yd[i-1]);
            return stats::compute(quant);
        }

        double hfsigma(DataSet const & res)
        {
            boost::array<double,2> qt = {1./3., 2./3.};
            ba::accumulator_set<double,
                                ba::stats<bat::count,
                                          bat::extended_p_square_quantile>>
                quant(ba::extended_p_square_probabilities = qt);
            auto mfcn = [&](auto & cur, auto const & ds)
                        {
                            for(size_t k = 1; k < ds.nx; ++k)
                                cur(ds.yd[k]-ds.yd[k-1]);
                        };

            wacc_t<bat::sum_of_weights, bat::weighted_mean> asig;
            auto rfcn = [&](auto const & var)
                        {
                            auto q0    = ba::quantile(var, ba::quantile_probability = qt[0]);
                            auto q1    = ba::quantile(var, ba::quantile_probability = qt[1]);
                            auto stdev = (q1-q0)*.5;
                            asig(stdev*stdev, ba::weight = ba::count(var));
                        };

            omp::mapreduce(quant, res, mfcn, rfcn);
            return std::sqrt(ba::weighted_mean(asig));
        }

        DataSet run(Args cf, gen_record  const * plt, DataSet const & res)
        { return _run(cf, plt, res); }

        DataSet run(Args cf, plot_region const * plt, DataSet const & res)
        { return _run(cf, plt, res); }
    }

    namespace select
    {
        namespace
        {
            template <typename T>
            auto _run(Args const & cf, T const * plt, DataSet const & items)
            {
                indexing::PhaseRange firstphase(plt, cf.firstphase),
                                     lastphase (plt, cf.lastphase);
                Results res;
                for(auto const & ds: items)
                {
                    auto pfirst = firstphase[ds.cycle];
                    auto plast  = lastphase [ds.cycle];
                    if(pfirst && plast)
                        switch(compare(pfirst->first, plast->second, ds, cf.minratio))
                        {
                            case -1: res.upstream  .emplace_back(ds);   break;
                            case 0:  res.within    .emplace_back(ds);   break;
                            case 1:  res.downstream.emplace_back(ds);   break;
                        }
                }
                return res;
            }

            template <typename T0, typename T1>
            auto _run(T0 const & phvalues, data_t minratio, T1 const * plt, DataSet const & items)
            {
                ba::accumulator_set<int, ba::stats<bat::min, bat::max>> phextr;
                auto old = -std::numeric_limits<int>::max();
                for(auto x: phvalues)
                {
                    phextr(x.first);
                    phextr(x.second);
                    if(x.first > x.second || x.first < old)
                        throw std::range_error("pairs must be sorted");
                    old = x.first;
                }

                auto minp = ba::min(phextr);
                auto maxp = ba::max(phextr)+1;
                std::vector<indexing::PhaseRange> phranges;
                for(int i = minp; i < maxp; ++i)
                    phranges.emplace_back(indexing::PhaseRange(plt, i));

                PhaseResults res(phvalues.size()+2);
                auto e = phvalues.size();
                for(auto const & ds: items)
                {
                    size_t cmp = e+1;
                    size_t i   = 0;
                    for(; i < e && cmp > e; ++i)
                    {
                        auto pF = phranges[phvalues[i].first -minp][ds.cycle];
                        auto pL = phranges[phvalues[i].second-minp][ds.cycle];
                        if(!pF || !pL)
                            break;

                        switch(compare(pF->first, pL->second, ds, minratio))
                        {
                            case -1: cmp = i;   break;
                            case  0: cmp = i+1; break;
                            case  1:            break;
                        }
                    }
                    res[cmp].emplace_back(ds);
                }
                return res;
            }

            template <typename T>
            DataSet _unique(int ph, T const * rec,  DataSet const & items)
            {
                stats::acc_t<stats::bat::max> ncycles;
                for(auto & ds: items)
                    ncycles(*ds.cycle);

                std::valarray<bool> keep(true, items.size());
                std::valarray<std::pair<double, size_t>>
                    cymax(std::make_pair(-std::numeric_limits<double>::max(),
                                          std::numeric_limits<size_t>::max()),
                          stats::compute(ncycles)+1);
                size_t found = 0;
                indexing::PhaseRange prng(rec, ph);
                for(size_t i = 0, e = items.size(); i < e; ++i)
                {
                    auto const & ds = items[i];
                    if(compare(*prng[*ds.cycle], ds) != 0)
                        continue;

                    auto   max  = stats::compute<stats::bat::median>(ds.nx, ds.yd);
                    auto & pair = cymax[*ds.cycle];
                    if(max <= pair.first)
                    {
                        ++found;
                        keep[i] = false;
                    } else
                    {
                        if(pair.second < items.size())
                        {
                            ++found;
                            keep[pair.second] = false;
                        }
                        pair = {max, i};
                    }
                }

                if(!found)
                    return items;

                size_t k = 0;
                DataSet res(items.size()-found);
                for(size_t i = 0, e = items.size(); i < e; ++i)
                    if(keep[i])
                        res[k++]= items[i];
                return res;
            }
        }

        int compare(int first, int last, Data const & ds, data_t ratio)
        {
            auto x0    = int(ds.xd[0]);
            auto x1    = int(ds.xd[ds.nx-1]);
            if(last <= x0)
                return 1;
            else if(first >= x1)
                return -1;
            else
            {
                auto npre = x0 < first ? first-x0 : 0;
                auto naft = x1 < last  ? 0        : x1-last;
                auto len  = x1 - x0 - npre - naft;
                return len >= (npre+naft)*ratio ? 0 : npre > naft ? -1 : 1;
            }
        }

        int compare(std::pair<int,int> pair, Data const & ds, data_t ratio)
        { return compare(pair.first, pair.second, ds, ratio); }

        int     compare(Args const &, std::pair<int,int> const &, Data const &);
        Results run(Args const & cf, plot_region const * plt, DataSet const & items)
        { return _run(cf, plt, items); }
        Results run(Args const & cf, gen_record  const * plt, DataSet const & items)
        { return _run(cf, plt, items); }

        PhaseResults run( PhaseIntervals        phvalues
                        , gen_record    const * plt
                        , DataSet       const & items
                        , data_t                 minratio
                        )
        { return _run(phvalues, minratio, plt, items); }

        PhaseResults run( PhaseIntervals        phvalues
                        , plot_region   const * plt
                        , DataSet       const & items
                        , data_t                 minratio
                        )
        { return _run(phvalues, minratio, plt, items); }

        DataSet unique(int phase, plot_region const * plt, DataSet const & in)
        { return _unique(phase, plt, in); }

        DataSet unique(int phase, gen_record const * plt, DataSet const & in)
        { return _unique(phase, plt, in); }
    }

    namespace collapse
    {
        CONFIG_IMPLEMENT(Args,CORDRIFT_COLLAPSE_ARGS(), form)
        namespace
        {
            template <typename T>
            void _range(Results & res, T const & items)
            {
                acc_t<ba::tag::min> minx;
                for(auto const & ds : items) minx(ds.xd[0]);
                res.minx = round(ba::min(minx));

                acc_t<ba::tag::max> maxx;
                for(auto const & ds : items) maxx(ds.xd[ds.nx-1]);
                res.maxx = round(ba::max(maxx))+1;
            }

            void _update_profile(Data const & ds, data_t avg, Results & res)
            {
                if(std::isnan(avg))
                    return;

                for(size_t i = 0, e = ds.nx;  i < e; ++i)
                {
                    auto j   = ds.xd[i]-res.minx;
                    if(j < 0)
                        continue;

                    auto rho = res.counts[j]/(res.counts[j]+1.f);
                    res.counts [j] += 1u;
                    res.profile[j]  = (ds.yd[i] - avg) * (1.f-rho)
                                    +  res.profile[j]  * rho;
                }
            }

            using  _rng_t  = std::vector<std::pair<size_t,size_t>>;
            struct _Ranges : public _rng_t
            {
                _Ranges(Results const & res)
                    : _min      (res.minx)
                    , _profile  (res.profile)
                {}

                auto operator()(Data const & ds)
                {
                    assert(     ds.xd[0] >= _min
                           &&   "res.minx must be the min x value");
                    assert(     ds.xd[ds.nx-1] >= _min
                           &&   "res.minx must be the min x value");

                    size_t xmin   = _index(ds.xd[0], _min);
                    size_t xmax   = _index(ds.xd[ds.nx-1], _min)+1;
                    for(auto & rng: *this)
                    {
                        if(xmax <= rng.first || xmin >= rng.second)
                            continue;

                        size_t iFirst = 0;
                        if(xmin < rng.first)
                        {
                            iFirst     = rng.first - xmin;
                            rng.first  = xmin;
                        }
                        assert(xmax <= rng.second && "should be sorted");

                        // fit to current profile
                        acc_t<bat::mean> delta;
                        for(size_t k = iFirst; k < ds.nx; ++k)
                            delta(ds.yd[k] - _profile[_index(ds.xd[k], _min)]);
                        return ba::mean(delta);
                    }

                    push_back(std::pair<data_t,data_t>(xmin, xmax));
                    return 0.;
                }

                private:
                    data_t                               _min;
                    decltype(Results::profile) const &  _profile;
            };

            template <typename T0, typename T1>
            inline auto _threshold(T0 const & cf, T1 const & res)
            {
                return std::max(1.*cf.nmin,
                                cf.endthr * ba::max(acc_t<bat::max>(res.counts)));
            }

            namespace _collapse
            {
                namespace ks = samples::normal::knownsigma;

                template <typename T0, typename T1>
                auto intervals(T0 const & cf, Results & res, T1 const & items)
                {
                    _Ranges ranges(res);
                    for(auto const & ds: items)
                        _update_profile(ds, ranges(ds), res);

                    std::vector<std::pair<size_t, size_t>> splitranges;
                    auto thr = _threshold(cf, res);
                    for(auto rng: ranges)
                    {
                        splitranges.push_back(rng);
                        bool looking = false;
                        auto i1      = rng.first;
                        auto i2      = rng.second;
                        while(i1 < i2 && res.counts[i1]   < thr)
                            ++i1;
                        while(i1 < i2 && res.counts[i2-1] < thr)
                            --i2;
                        for(auto i = i1; i < i2; ++i)
                            if(looking ^ (res.counts[i] < thr))
                            {
                                if(looking)
                                    splitranges.emplace_back(i, rng.second);
                                else
                                    splitranges.back().second = i;
                                looking = !looking;
                            }
                    }
                    return splitranges;
                }

                namespace ci = cordrift::intervals;

                struct _Yds
                {
                    template <typename T0, typename T1>
                    _Yds(T0 const & cf, T1 const & items)
                    : _ptrs(items.size())
                    , _mem (items.size())
                    , _sz  (items.size())
                    , _dn  (std::max(1, cf.dn))
                    , _thr (ks::threshold(true, cf.confidence,
                                                cf.uncertainty,
                                                _dn, _dn))

                    {
                        int n = int(items.size());
                        for(int i = 0; i < n; ++i)
                        {
                            _ptrs[i] = items[i].yd;
                            _sz  [i] = items[i].nx;
                        }

                        ci::Args icf;
                        icf.uncertainty = cf.uncertainty;
                        _ints.resize(n);


                        filter::forwardbackward::Args cfilt;
                        cfilt.precision = cf.uncertainty;

                        if(cf.bfilter)
                        {
                            for(int i = 0; i < n; ++i)
                                _mem[i].resize(items[i].nx);
                        }

                        PRAGMA_OMP(parallel for)
                        for(int i = 0; i < n; ++i)
                        {
                            Data ds(items[i]);
                            if(cf.bfilter)
                            {
                                ds.yd    = _mem[i].data();
                                _ptrs[i] = ds.yd;
                                std::copy(items[i].yd, items[i].yd+_sz[i], _mem[i].begin());
                                filter::forwardbackward::run(cfilt, ds.nx, ds.yd);
                            }
                            _ints[i] = std::move(ci::run(icf, none, none, ds));
                        }
                    }

                    void operator()(int i, int ind, std::vector<float> & values)
                    {
                        if(ind < 1 || ind >= _sz[i])
                            return;

                        auto const * yd    = _ptrs[i];
                        auto         left  = yd[ind-1];
                        auto         right = yd[ind];

                        if(_dn > 1)
                        {
                            for(int k = std::max(0, ind-2), n = std::max(0, ind-_dn-1); k != n; --k)
                                left  += yd[k];
                            left /=  ind-std::max(0, ind-_dn);

                            for(int k = ind+1, n = std::min(_sz[i], ind+_dn); k != n; ++k)
                                right += yd[k];
                            right /=  std::min(_sz[i], ind+_dn)-ind;
                        }

                        auto val = right-left;
                        for(auto const & rng: this->_ints[i])
                            if(int(rng.first) <= ind-_dn-1 && ind+_dn < int(rng.second))
                            {
                                values.push_back(val);
                                return;
                            }

                        if(std::abs(val) < _thr)
                            values.push_back(val);
                    }

                    private:
                        std::vector<ci::Results>        _ints;
                        std::vector<float const *>      _ptrs;
                        std::vector<std::vector<float>> _mem;
                        std::vector<int>                _sz;
                        int                       const _dn;
                        float                           _thr;
                };

                template <typename T0, typename T1>
                auto derivate(T0 cf, Results & res, T1 const & items)
                {
                    if(cf.uncertainty <= 0.)
                        cf.uncertainty = intervals::hfsigma(items);

                    _Yds yds(cf, items);

                    PRAGMA_OMP(parallel)
                    {
                        auto const n   = items.size();
                        std::vector<float> values;
                        values.reserve(items.size());

                        std::vector<size_t> inds(n, 0);
                        PRAGMA_OMP(for)
                        for(int k = res.minx+1; k < res.maxx; ++k)
                        {
                            values.resize(0);
                            for(size_t i = 0; i < n; ++i)
                                for( ; inds[i] < items[i].nx; ++inds[i])
                                {
                                    auto x = items[i].xd[inds[i]];
                                    if(x < k-.1f)
                                       continue;

                                    else if (x < k+.1f)
                                        yds(i, inds[i], values);
                                    break;
                                }

                            auto j = k-res.minx;
                            res.counts [j] = values.size();
                            if(res.counts[j])
                            {
                                if(cf.useintervals == 1)
                                    res.profile[j] = stats::compute<bat::mean>(values);
                                else
                                    res.profile[j] = stats::median(values);
                            }
                        }
                    }

                    res.profile[0] = res.counts[0] = 0;

                    // clip derivate
                    float last = 0;
                    auto  dev  = cf.meddev * stats::compute<bat::mediandeviation>(res.profile);
                    for(size_t iS = 0, nS = res.profile.size()-1; iS < nS; ++iS)
                        if(std::abs(res.profile[iS]-res.profile[iS+1]) > dev)
                            res.profile[iS] = last;
                        else
                            last = res.profile[iS];

                    for(int k = 1, n = res.counts.size(); k < n; ++k)
                        res.profile[k] += res.profile[k-1];
                    res.profile -= res.profile[res.profile.size()-1];

                    return std::vector<std::pair<size_t, size_t>>{};
                }
            }

            struct _Values
            {
                template <typename T>
                _Values(T const & res, size_t i, size_t di, size_t j, size_t dj)
                {
                    using m2_t = wacc_t<bat::weighted_mean, bat::weighted_moment<2>>;
                    auto nS = res.profile.size();
                    auto i0 = i    < di ?  0 : i-di;
                    auto i1 = j+dj > nS ? nS : j+dj;

                    m2_t  var(i1-i0, i0, &res.counts[i0]);

                    auto y = stats::median(&res.profile[i0], &res.profile[i1]);
                    auto x = ba::weighted_mean(var);
                    auto v = ba::weighted_moment<2>(var)-x*x;
                    std::vector<float> cov(i1-i0);
                    for(auto i = i0; i < i1; ++i)
                        cov[i-i0] = (res.profile[i]-y)*(i-x);

                    first  = v <= 0. ? 0. : stats::median(cov)/v;
                    second = y - first * x;
                }

                template <typename T>
                _Values(T const & res, size_t i, size_t di, bool bup)
                    : _Values(res, i, bup ? di-1 : 0, i, bup ? 1 : di)
                {}

                template <typename T>
                double operator()(T j) const { return first * j + second; };

                double first, second;
            };

            template <typename T>
            inline auto _value(T const & res, size_t i, size_t di, bool bup)
            { return _Values(res, i, di, bup)(i); }

            template <typename T>
            auto _value(T const & cf, Profile const & res, bool upstream)
            {
                auto thr  = _threshold(cf, res);
                int nS    = int(res.profile.size());
                int iS    = upstream ? nS-1 : 0;
                if(upstream)
                {
                    for(; iS > 0 && res.counts[iS] <= thr; --iS)
                        ;
                    return _value(res, iS, cf.estlength, true);

                } else
                {
                    for(; iS < nS && res.counts[iS] <= thr; ++iS)
                        ;
                    return _value(res, iS,  cf.estlength, false);
                }
            }

            template <typename T>
            auto _value(T const & cf, DataSet  const & vect, Profile & res, bool upstream)
            {
                if(vect.size() == 0)
                    return 0.;

                res = run(cf, vect);
                return _value(cf, res, upstream);
            }

            template <typename T>
            void _continuity(T const & cf, _rng_t & rng, Results & res)
            {
                auto thr = _threshold(cf, res);
                int  nS  = int(res.profile.size());

                int iFirst  = nS-1;
                for(; iFirst > 0 && res.counts[iFirst] <= thr; --iFirst)
                    ;

                auto delta = 0.;
                auto iPrev = iFirst+1;
                for(auto const & pair: rng)
                {
                    if(pair.first == 0)
                        continue;

                    int iLast  = pair.first;
                    iFirst     = pair.first-1;
                    while(iFirst > 0 && res.counts[iFirst] < thr)
                        --iFirst;
                    assert(iFirst >= 0 && iFirst < nS);

                    while(iLast  < iPrev && res.counts[iLast] < thr)
                        ++iLast;
                    assert(iLast >= 0 && iLast < nS);

                    assert(iPrev <= nS);
                    for(auto k = iLast; k < iPrev; ++k)
                        res.profile[k] += delta;

                    _Values rR(res, iLast, cf.estlength, false);
                    auto    mid = (iFirst+iLast)/2+1;

                    for(auto k = mid; k < iLast; ++k)
                        res.profile[k] = rR(k);

                    _Values rL(res, iFirst, cf.estlength, true);
                    delta = res.profile[mid] - rL(mid);

                    for(auto k = iFirst; k < mid; ++k)
                        res.profile[k] = rL(k) + delta;

                    iPrev = iFirst;
                }
                for(auto k = 0; k < iPrev; ++k)
                    res.profile[k] += delta;

                rng = {{size_t(0), res.profile.size()}};
            }

            template <typename T>
            void _smooth(T const & cf, Results & res)
            {
                if(cf.nsmear/2 <= 0) return;

                size_t ws = size_t(cf.nsmear/2);

                std::valarray<data_t> wgts(0.,2*ws+1);
                for(size_t i = 0, e = wgts.size(); i < e; ++i)
                {
                    data_t x = (data_t(i)-ws)/(2.*ws*cf.rho);
                    wgts[i]  = exp(-x*x);
                }
                wgts /= wgts.sum();

                decltype(res.profile) prof(0., res.profile.size());
                decltype(res.counts)  cnt (0., res.profile.size());
                auto inner = [&](auto const & arr, size_t i)
                { return std::inner_product(&arr[i-ws], &arr[i+ws+1], &wgts[0], 0.f); };

                for(size_t i = 0; i < ws; ++i)
                {
                    prof[i] = res.profile[i];
                    cnt [i] = res.counts [i];
                    prof[prof.size()-ws+i] = res.profile[prof.size()-ws+i];
                    cnt [prof.size()-ws+i] = res.counts [prof.size()-ws+i];
                }

                for(size_t i = ws, e = prof.size()-ws; i < e; ++i)
                {
                    prof[i] = inner(res.profile, i);
                    cnt [i] = inner(res.counts,  i);
                }

                std::swap(prof, res.profile);
                std::swap(cnt,  res.counts);
            }

            template <typename T0, typename T1>
            void _zero_min(T0 const & cf, T1 rng, Results & res)
            {
                res.profile -= stats::compute<bat::median>(res.profile);

                auto const thr  = std::max(0., cf.endthr
                                              *stats::compute<bat::max>(res.counts));
                auto const le   = size_t(cf.nmin);
                std::vector<float> medvals(le);

                auto const lem1 = size_t(cf.nmin)-1;
                auto const hle  = cf.nmin/2;

                size_t const iSmin = rng ? rng->first : 0;
                size_t const iSmax = rng ? rng->second: res.profile.size();

                ba::accumulator_set<int, ba::stats<bat::sum>> sum1, sum2;
                auto ptr   = &res.profile[iSmin];
                auto delta = 0.;
                auto init  = [&,lem1](auto val)
                             {
                                 delta = val;
                                 sum1  = {};
                                 for(size_t i = 0; i <= lem1; ++i)
                                     sum1(ptr[i] <= delta ? 1 : 0);

                                 sum2  = {};
                             };

                init(0.);

                RARolling<bat::mean> wgt(cf.nmin);
                for(size_t iS = iSmin; iS < iSmin+lem1; ++iS)
                    wgt (&res.counts[iS]);

                for(size_t iS = iSmin+lem1, nS = iSmax; iS < nS; ++iS, ++ptr)
                {
                    wgt (&res.counts[iS]);
                    sum1(ptr[lem1] <= delta ? 1 : 0);

                    auto cnt = ba::sum(sum1) - ba::sum(sum2);

                    sum2(ptr[0] <= delta ? 1 : 0);

                    if(cnt < hle)
                        continue;

                    if(ba::mean(wgt) < thr)
                        continue;

                    std::copy(ptr, ptr+le, medvals.begin());
                    auto val = stats::median(medvals);
                    if(val < delta)
                        init(val);
                }

                if((!rng) || delta < 0.)
                    res.profile -= delta;
            }

            template <typename T0, typename T1>
            void _zero_mean(T0 const & cf, _rng_t const & rng, T1 pha, Results & res)
            {
                auto rem = [&](auto iFirst, auto iLast)
                            {
                                auto sz    = iLast-iFirst;
                                auto ptr   = &res.profile[iFirst];
                                auto delta = -stats::compute<bat::mean>(sz, ptr);
                                for(size_t k = 0; k < sz; ++k)
                                    ptr[k] += delta;
                            };

                for(auto const & pair: rng)
                    rem(pair.first, pair.second);

                if(cf.useintervals == 2)
                {
                    auto  dev  = cf.meddev * intervals::hfsigma(res.profile);
                    float last = 0;
                    size_t iSmin = pha ? pha->first : 0,
                           iSmax = pha ? pha->second: res.profile.size();

                    for(size_t iS = iSmin, nS = iSmax; iS < nS; ++iS)
                        if(   res.counts[iS] < cf.nmin)
                            res.profile[iS] = last;
                        else if(iS < nS-1 && std::abs(res.profile[iS]-res.profile[iS+1]) > dev)
                            res.profile[iS] = last;
                        else
                            last = res.profile[iS];
                }
            }

            template <typename T0, typename T1>
            void _zero_right(T0 const & cf, T1 rng, Results & res)
            {
                size_t const iSmax = rng ? rng->second: res.profile.size();
                size_t const iSmin = rng ? rng->first : 0;
                size_t const iS    = iSmin + cf.nmin > iSmax ? iSmin : iSmax - cf.nmin;
                std::valarray<float> tmp(res.profile[std::slice(iS, iSmax-iS, 1)]);
                res.profile -= stats::median(std::begin(tmp), std::end(tmp));
            }

            template <typename T>
            auto _adjust_pair(T const & cf, Profile const & left, Profile const & right)
            {
                return std::make_pair(_value(cf, left,  true),
                                      _value(cf, right, false));
            }

            template <typename T>
            void _adjust_left(T const & cf, Profile & left, Profile const & right)
            {
                auto p = _adjust_pair(cf, left, right);
                if(p.first < p.second)
                    left.profile += p.second - p.first;
            }

            template <typename T>
            void _adjust_right(T const & cf, Profile const & left, Profile & right)
            {
                auto p = _adjust_pair(cf, left, right);
                if(p.first > p.second)
                    right.profile += p.first - p.second;
            }

            template <typename T>
            Results _merge(T const & cf, Profile const & left, Profile const & right)
            {
                assert(right.minx >= left.minx && "Not implemented");

                auto const lsz = left .profile.size();
                auto const rsz = right.profile.size();
                auto const tot = _index(right.maxx, left.minx);
                auto const sl  = [](auto a, auto b){ return std::slice(a, b-a, 1); };
                auto const lsl = sl(std::max(lsz, tot-rsz), tot);
                auto const rsl = sl(_index(left.maxx, right.minx), rsz);

                if(lsz == 0 || rsz == 0)
                    return lsz == 0 ? right : left;

                Results res;
                res.profile.resize(tot);
                res.counts .resize(tot);

                res.profile[sl(0,lsz)] = left.profile;
                res.profile[lsl]       = right.profile[rsl];
                res.counts [sl(0,lsz)] = left.counts;
                res.counts [lsl]       = right.counts [rsl];

                size_t x1 = _index(left .maxx, left.minx);
                size_t x2 = _index(right.minx, left.minx);
                if(x2 < x1)
                    std::swap(x2, x1);

                _rng_t rng = {{x2, tot}, {0, x1}};

                res.minx = left .minx;
                res.maxx = right.maxx;
                if(x2-x1 <= size_t(cf.nmin))
                    _continuity(cf, rng, res);
                return res;
            }
        }

        Results run   (Args const & cf, DataSet const & items)
        {
            Results res;
            if(items.size() == 0)
                return res;
            _range   (res, items);

            res.profile.resize(res.maxx-res.minx);
            res.counts .resize(res.maxx-res.minx);

            auto rng = cf.useintervals == 2 ? _collapse::intervals(cf, res, items)
                                            : _collapse::derivate (cf, res, items);
            if(cf.continuity)
                _continuity(cf, rng, res);

            _smooth(cf, res);

            decltype(indexing::phaserange(0, 0)) tmp;
            switch(cf.zero)
            {
                case ZeroPolicy::min:
                    _zero_min (cf, tmp,  res);
                    break;

                case ZeroPolicy::measmin:
                    tmp = indexing::phaserange(items[0].parent, int(phases::measure));
                    _zero_min (cf, tmp,  res);
                    break;

                case ZeroPolicy::measright:
                    tmp = indexing::phaserange(items[0].parent, int(phases::measure));
                    _zero_right (cf, tmp,  res);
                    break;

                case ZeroPolicy::mean:
                    _zero_mean(cf, rng, tmp, res);
                    break;

                case ZeroPolicy::measmean:
                    tmp = indexing::phaserange(items[0].parent, int(phases::measure));
                    _zero_mean(cf, rng, tmp, res);
                    break;
                default:
                    break;
            }

            return res;
        }

        template <typename T0, typename T1>
        Profiles run(T0 const & cf, T1 const * plt, DataSet & mem)
        {
            Args ccf = collapse::args(cf);
            Profiles cor;
            if(cf.useintervals != 2)
                cor.within = run(ccf, mem);
            else if(!cf.continuity)
            {
                if(plt != nullptr)
                    mem = select::run(cf, plt, mem).within;
                cor.within = run(ccf, mem);
            } else if(plt == nullptr)
                cor.within = run(ccf, mem);
            else
            {
                select::Results sel;
                select::PhaseIntervals phranges =
                    {{int(phases::pull),    int(phases::pull)},
                     {int(phases::measure), int(phases::measure)}};

                auto sets      = select::run(phranges, plt, mem, cf.minratio);
                sel.upstream   = std::move(sets[0]);
                sel.downstream = std::move(sets[phranges.size()+1]);

                cor.upstream   = run(ccf, sel.upstream);
                cor.downstream = run(ccf, sel.downstream);
                auto pull      = run(ccf, sets[1]);
                auto meas      = run(ccf, sets[2]);

                _adjust_right(ccf, cor.upstream, pull);
                _adjust_left (ccf, meas, cor.downstream);

                cor.within = _merge(ccf, pull, meas);

                _adjust_right(ccf, cor.upstream, cor.within);
                _adjust_left (ccf, cor.within,   cor.downstream);
            }
            return cor;
        }
    }

    namespace remove
    {
        CONFIG_IMPLEMENT(Args,CORDRIFT_REMOVE_ARGS, form)

        void run(collapse::Results const & res, Data & ds)
        {
            for(size_t k = 0, nr = res.profile.size(); k < ds.nx; ++k)
            {
                size_t i = _index(ds.xd[k], res.minx);
                if(i >= nr)
                    i = nr-1;
                ds.yd[k] -= res.profile[i];
            }
        }
    }

    namespace all
    {
        CONFIG_IMPLEMENT(SmallArgs,CORDRIFT_ALL_SMALLARGS(), smallform)
        CONFIG_IMPLEMENT(Args,CORDRIFT_ALL_ARGS(), form)
        namespace
        {
            template <typename T>
            Profiles _run(SmallArgs const & cf, T const * plt, DataSet & res)
            {
                if(!cf.continuity && (int(res.size()) < cf.nmin))
                    return {};

                collapse ::Profiles cor;
                if(cf.useintervals == 2)
                {
                    auto mem = intervals::run(intervals::args(cf), plt, res);
                    cor = collapse::run(cf, plt, mem);
                } else
                    cor = collapse::run(cf, plt, res);

                remove::run(cor.within, res);
                return cor;
            }
        }

        Profiles run(SmallArgs const & cf, plot_region const * plt, DataSet & res)
        { return _run(cf, plt, res); }

        Profiles run(SmallArgs const & cf, gen_record const * plt, DataSet & res)
        { return _run(cf, plt, res); }

        Profiles run(SmallArgs const & cf, DataSet & res)
        { return _run(cf, (gen_record*) nullptr, res); }
    }
}
