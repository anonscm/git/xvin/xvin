#include "cordrift_core.h"
#include "hybridstat_core.h"
#include "../pywrapper/core.hpp"
#include <boost/python/stl_iterator.hpp>
#include <allegro.h>
#ifdef XV_WIN32
#   include <winalleg.h>
#endif

extern "C" {
#   include "../trackBead/record.h"
#   include "plot_ds.h"
}

namespace bp = boost::python;
using cordrift::collapse::ZeroPolicy;

namespace pywrapper
{
    constexpr int signature(ZeroPolicy x) { return int(x); };
}

namespace
{
    bp::object _submodule(std::string subname)
    {
        std::string name = bp::extract<std::string>(bp::scope().attr("__name__"));
        name += "." + subname;

        bp::object  mod(bp::handle<>(bp::borrowed(PyImport_AddModule(name.c_str()))));
        bp::scope().attr(subname.c_str()) = mod;

        return mod;
    }
}


namespace cordrift
{

    namespace intervals
    {
#       define INTERVAL_PYARGS CORDRIFT_INTERVAL_SMALLARGS()
        bp::object _run(bp::object iter PYWRAPPER_ARGS(INTERVAL_PYARGS))
        {
            Args cf; PYWRAPPER_COPY_ARGS(cf, INTERVAL_PYARGS)

            std::vector<float> dt;
            pywrapper::extract_vector(iter, dt);
            Data ds;
            ds.nx  = dt.size();
            ds.yd  = dt.data();
            auto res = run(cf, std::numeric_limits<size_t>::max()
                             , std::numeric_limits<size_t>::max()
                             , ds);
            return pywrapper::topython(res);
        }

        auto _hfsigma(bp::object iter)
        {
            std::vector<data_set *> dt;
            pywrapper::extract_vector(iter, dt);

            auto vect = dataset(dt.begin(), dt.end());
            return hfsigma(vect);
        }
    }

    namespace collapse
    {
        bp::dict topython(Results & res)
        {
            bp::dict pyr;
            pyr["minx"]    = res.minx;
            pyr["maxx"]    = res.maxx;
            pyr["profile"] = pywrapper::topython(res.profile);
            pyr["counts"]  = pywrapper::topython(res.counts);
            return pyr;
        };

#       define COLLAPSE_PYARGS CORDRIFT_COLLAPSE_SMALLARGS() CORDRIFT_COLLAPSE_HIDDEN_ARGS()
        bp::object _run(bp::object iter PYWRAPPER_ARGS(COLLAPSE_PYARGS))
        {
            Args cf; PYWRAPPER_COPY_ARGS(cf, COLLAPSE_PYARGS)

            std::vector<data_set *> dt;
            pywrapper::extract_vector(iter, dt);

            DataSet cdt = dataset(dt.begin(), dt.end());
            auto    cor = run(cf, cdt);
            return topython(cor);
        }
    }

    namespace all
    {
        bp::dict topython(Profiles & res)
        {
            bp::dict pyr;
            pyr["upstream"]     = collapse::topython(res.upstream);
            pyr["within"]       = collapse::topython(res.within);
            pyr["downstream"]   = collapse::topython(res.downstream);
            return pyr;
        };

#       define ALL_PYARGS CORDRIFT_ALL_SMALLARGS()
        bp::object _run(bp::object iter PYWRAPPER_ARGS(ALL_PYARGS))
        {
            SmallArgs cf; PYWRAPPER_COPY_ARGS(cf, ALL_PYARGS)

            std::vector<data_set *> dt;
            pywrapper::extract_vector(iter, dt);

            char    tmp[512] = "%pr";
            plot_region *pr  = nullptr;
            if (ac_grep(cur_ac_reg, tmp, &pr) != 1)
            {
                win_printf_OK("cannot find data");
                return bp::object();
            }

            auto cdt = cycleddataset(dt.begin(), dt.end());
            auto cor = run(cf, pr, cdt);
            return topython(cor);
        }
    }

    void _module()
    {
        bp::scope guard = _submodule("cordrift");
        bp::def( "intervals"
               , intervals::_run
               , (bp::args("data") PYWRAPPER_SIGNATURE(INTERVAL_PYARGS))
               , CORDRIFT_INTERVAL_SMALLFORM
               );

        bp::def( "sigma"
               , intervals::_hfsigma
               , bp::args("data")
               , "This is the standard deviation computed without taking"
                 "jumps into account"
               );

        bp::def( "collapse"
               , collapse::_run
               , (bp::args("data") PYWRAPPER_SIGNATURE(COLLAPSE_PYARGS))
               , CORDRIFT_COLLAPSE_FORM
               );

        bp::def( "cordrift"
               , all::_run
               , (bp::args("data") PYWRAPPER_SIGNATURE(ALL_PYARGS))
               , CORDRIFT_ALL_SMALLFORM
               );
    }
};

namespace hybridstat { namespace all
{
    float  _get_ext3(all::BatchArgs const & args) { return args.dist.ext3; }
    float  _get_ext5(all::BatchArgs const & args) { return args.dist.ext5; }
    void   _set_ext3(all::BatchArgs & args, float a) { args.dist.ext3 = a; }
    void   _set_ext5(all::BatchArgs & args, float a) { args.dist.ext5 = a; }

    bp::object _matchpeaks(bool bzero, float sigma, bp::object ref, bp::object bead)
    {
        auto extract = [](bp::object dt)
            {
                std::vector<float> tmp;
                pywrapper::extract_vector(dt, tmp);

                hybridstat::distance::Input inp;
                inp.resize(tmp.size());
                for(size_t i = 0; i < tmp.size(); ++i)
                    inp[i] = std::make_pair(float(tmp[i]), 0.f);

                return inp;
            };

        auto iref  = extract(ref);
        auto ibead = extract(bead);

        hybridstat::distance::Result _;
        auto res = hybridstat::distance::matched(bzero, sigma, _, iref, ibead);
        return pywrapper::topython(res);
    }

    void _load(all::BatchArgs & cf) { load(static_cast<SmallArgs&>(cf), MS_CKEY); };
    void _run (all::BatchArgs & cf) { run(cf); }

    void _module()
    {
        bp::scope guard = _submodule("batch");

#       define _PY_ATTR_DECLARE__(X) .def_readwrite(#X, &BatchArgs::X)
#       define _PY_ATTR_DECLARE_(X) _PY_ATTR_DECLARE__(X)
#       define _PY_ATTR_DECLARE(A,K,C) _PY_ATTR_DECLARE_(BOOST_PP_TUPLE_ELEM(4,2,C))
        using all::BatchArgs;
        bp::class_<BatchArgs>("Args")
           BOOST_PP_SEQ_FOR_EACH(_PY_ATTR_DECLARE,_,                    \
                                 HYBRIDSTAT_ALL_SMALLARGS()             \
                                 ((,,usehairpins,)) ((,,usemanualids,)) \
                                 ((,,oligolist,))   ((,,reportpath,))   \
                                 ((,,hairpinpath,)) ((,,idpath,)))
           .add_property("ext3", _get_ext3, _set_ext3)
           .add_property("ext5", _get_ext5, _set_ext5)
           .def("load", _load)
           .def("run",  _run)
           ;
#       undef _PY_ATTR_DECLARE__
#       undef _PY_ATTR_DECLARE_
#       undef _PY_ATTR_DECLARE

        bp::def("matchpeaks", _matchpeaks);
    }
}}

BOOST_PYTHON_MODULE(_core)
{
    bp::scope().attr("__path__") = "hybridstat";
    cordrift::_module();
    hybridstat::all::_module();
}
