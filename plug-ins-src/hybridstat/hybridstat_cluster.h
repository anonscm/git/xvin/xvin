#pragma once
#include <limits>
#include <vector>
#include <valarray>
#include <map>
#include "hybridstat_theoretical.h"
namespace hybridstat { namespace distance
{
    using Input = theoretical::Peaks;
    struct Measure
    {
        double val      = 0.;
        double grad[2]  = {0., 0.};
    };

    enum class Strategy {hairpin, blind, automatic};

    struct Args
    {
        struct Grid { float first, last, step; };
        std::valarray<float> sigmas     = {0.03, .015};
        Grid                 griddyn    = Grid{ .85f,  1.15f, .005f  };
        Grid                 gridbias   = Grid{-.025f, .025f, .0025f };

        int                  strategy   = int(Strategy::blind);
        float                xrel       = 1e-4;
        float                frel       = 1e-4;
        float                xabs       = 1e-8;
        float                stopval    = 1e-8;
        size_t               maxeval    = 100;
    };

    struct Result
    {
        float dist  = std::numeric_limits<float>::max();
        float dyn   = 1.;
        float bias  = 0.;
    };

    typedef std::pair<size_t,size_t> Key;
    struct Less
    {
        constexpr bool operator()(Key const & a, Key const & b) const
        {
            bool ab = a.first < a.second;
            bool bb = b.first < b.second;
            auto a1 = ab ? a.first : a.second;
            auto b1 = bb ? b.first : b.second;
            if(a1 != b1) return a1 < b1;

            auto a2 = ab ? a.second : a.first ;
            auto b2 = bb ? b.second : b.first ;
            return a2 < b2;
        }
    };

    typedef std::map<Key, Result, Less> Results;

    Measure convolve(float const sigma, float dyn, float bias,
                     Input const & b1, Input const & b2);

    Result  run(Args const &, Input const &, Input const &);
    Results run(Args const &, std::vector<Input> const &);

    /* Returns dyn and bias so as to convert second bead's x axis to the first's;
     */
    Result  convert(size_t, size_t, Result const &);

    typedef std::vector<std::pair<size_t, size_t>> Matches;
    /* Returns the index of matched peaks (distance < sigma)
     */
    Matches matched(bool, float, Result const &, Input const &, Input const &);
    inline Matches matched(float a, Result const & b, Input const & c, Input const & d)
    { return matched(false, a, b, c, d); }
}}

namespace hybridstat { namespace cluster
{
    typedef distance::Results Input;

    struct Args
    {
        size_t nclusters;
        float  tol          = 1e-5;
        size_t maxiter      = 100;
        size_t maxsmalliter = 10;
        float  ratio        = .1;
    };

    struct Item
    {
        size_t id;
        float  silhouette = 0.f;
    };

    typedef std::vector<Item>           Items;
    typedef std::pair  <size_t, Items>  Group;
    struct Results
    {
        std::vector<size_t> values;
        std::vector<Group>  groups;
        float               total;
    };

    Results run(Args const &, Input const &);
}}

namespace hybridstat { namespace bestmatch
{
    using Input    = theoretical::Peaks;
    using Item     = distance   ::Result;
    using Peaks    = theoretical::Peaks;
    using Hairpin  = theoretical::Result;
    using Hairpins = theoretical::Results;
    using Params   = std::map<int, std::pair<double, double>>;

    typedef std::pair<distance::Results, cluster::Results> Results;
    struct Result
    {
        size_t              id;
        std::vector<Item>   dist;
    };

    struct Args : public distance::Args
    {
        float ext5       = 8.8e-4f;   // conversion base -> nm: phase 3
        float ext3       = 1.e-3f;    // conversion base -> nm: phase 5
        float height3    = .5f;       // height of last peak
        float matchwidth = 20.f;
        int   useparams  = 0;
        std::vector<int> strategies = {};

        Params params;
    };

    Result  run(Args,         Hairpins const &, Input const &);
    Result  run(Args,         Hairpins const &, Input const &, int);
    Results run(Args const &, Hairpins, std::vector<Input> const &);
}}
