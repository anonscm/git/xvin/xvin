#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Outputs hybridstat plugin's data to an xlsx file
"""
# pylint: disable=locally-disabled,unused-import,wrong-import-order
from contextlib         import closing
from xlsxwriter         import Workbook

# pylint: disable=import-error
import  pyxvin   # needed for access to numpy

from ._genericreport    import fileobj, startfile
from ._repairs          import run as repair
from ._summary          import summary
from ._peaks            import PeaksSheet
from ._events           import EventsSheet
from ._type             import Probability, Position, Events, Peak, Peaks, Key
from ._type             import Distance, Bead, Group, Groups, Hairpin, Hairpins, Oligos

def _debug_save(args, fname = "/tmp/_pickle"):
    try:
        import pickle
        with open(fname, 'wb') as stream:
            pickle.dump(args, stream)
    except Exception: # pylint: disable=locally-disabled,broad-except
        pass

def _debug_run(fname = '/tmp/_pickle'):
    import pickle
    with open(fname, 'rb') as stream:
        run(pickle.load(stream))

def save(filename : str, **args):
    """ saves groups data to filename """
    with closing(fileobj(filename)) as book:
        summ = summary(book, **args)

        PeaksSheet(summ).table()
        EventsSheet(summ).table()

def _run(args):
    u""" saves groups data to filename """
    #_debug_save(args)
    save(args.pop('filename'), **args)

def run(args):
    u""" saves groups data to filename """
    #_debug_save(args)
    filepath = args.pop('filename')
    save(filepath, **args)
