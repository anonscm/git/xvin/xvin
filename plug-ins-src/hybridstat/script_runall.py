#!/usr/bin/env python3
# -*- coding: utf-8 -*-
u"Run all"
import os
import sys
import re
import shutil
import subprocess

if sys.platform == 'win32':
    _CMD  = 'c:/msys64/usr/bin/bash --login -c "cd %s; ./pias _script.py"'
    _CMD %= os.path.abspath(".")
else:
    _CMD  = './pias _script.py'

_OLS   = re.compile(r"^.*?("
                    +r"([\[]?[ATGC][\]]?){4}" +r"((\+|_and_)([\[]?[ATGC][\]]?){4})?"
                    +r").*?5_HPs.*$")
_BEADS = re.compile(r"^Z\(t\)bd\d+track\d+.gr$").match
def getoligo(path):
    u"returns the oligo parsed from a file name"
    val = _OLS.match(path)
    if not val:
        print("Not an oligo ?", path[path.rfind('/')+1:])
    return None if val is None else val.group(1)

def runone(kmer, trpath, grpath, idf, rpt):
    u"runs one file"
    # pylint: disable=no-member
    import pyxvin
    from hybridstat._core import batch

    pyxvin.batchmode(True)
    pyxvin.loadtrack(trpath)
    pyxvin.loadplots([grpath+name for name in os.listdir(grpath) if _BEADS(name)])

    args = batch.Args()
    args.load()
    args.oligolist     = ','.join(kmer)
    args.useintervals  = 1
    args.driftpercycle = False
    args.driftperbead  = False
    args.hairpinpath   = "hairpins.fasta"
    args.usehairpins   = True
    args.usemanualids  = len(idf) > 0
    args.refphase      = 5
    args.idpath        = idf
    args.useparams     = 0
    args.reportpath    = rpt
    args.run()
    pyxvin.close()

def _testpath(path, nam):
    return ('5_HPs' in nam
            and os.path.isdir(path+nam)
            and getoligo(nam) is not None)

def _getdirs(path):
    return ((getoligo(nam), path+nam+"/") for nam in os.listdir(path)
            if _testpath(path, nam))

def _getgrs(grs):
    def _getcgrs(path):
        for name in os.listdir(path):
            if _testpath(path, name):
                yield (getoligo(name), path+name+"/")
            elif os.path.isdir(path+name):
                yield from _getcgrs(path+name+'/')

    ret = dict()
    for i in (grs,) if isinstance(grs, str) else grs:
        ret.update(_getcgrs(i))
    return ret

def _gettrs(trs):
    trdirs  = dict()
    for name, path in _getdirs(trs):
        trdirs.setdefault(name, []).append(path)
    return trdirs

def _intersect(grdirs, trdirs):
    print(len(grdirs), len(trdirs), len(frozenset(trdirs) & frozenset(grdirs)))
    if len(frozenset(grdirs) - frozenset(trdirs)):
        print("Not in gr: ", frozenset(grdirs) - frozenset(trdirs))

    for oligo in frozenset(trdirs) ^ frozenset(grdirs):
        grdirs.pop(oligo, None)
        trdirs.pop(oligo, None)

    print(len(grdirs), len(trdirs))
    for oligo in tuple(trdirs):
        kmer = oligo.replace('[', '').replace(']', '')
        kmer = kmer.replace('+', ',').replace('_and_', ',').split(',')
        if len(kmer) not in (1, 2) or any(len(i) != 4 for i in kmer):
            print("Not a kmer: "+kmer)
            grdirs.pop(oligo, None)
            trdirs.pop(oligo, None)
    print(len(grdirs), len(trdirs))

def _getitems(grdirs, trdirs, withids:bool):
    items = []
    idcnt = 0
    for oligo in grdirs:
        for pot in ('--checked', '--corrected', ''):
            grpath = grdirs[oligo]+"cgr_project"+pot+'/'
            if (os.path.exists(grpath)
                    and next((1 for name in os.listdir(grpath) if _BEADS(name)), 0)):
                break
        else:
            print("Missing cgr_project for: "+grdirs[oligo])
            continue

        fil = next((i[:-3]+"trk" for i in os.listdir(grpath) if i.endswith(".cgr")),
                   None)
        if fil is None:
            print("Missing .cgr in cgr_project:", grpath)
            return

        trpath = next((i+fil for i in trdirs[oligo] if os.path.exists(i+fil)),
                      None)
        if trpath is None:
            print("trk ? ", trdirs[oligo], grpath)
            continue

        if withids:
            idf   = next((grdirs[oligo]+name for name in os.listdir(grdirs[oligo])
                         if name.startswith("hairpin_identification")), "")
            if idf == "":
                idcnt += 1
                print(("(%d) id ? " % idcnt)+grpath)
                continue
        else:
            idf = ""

        rpt   = "reports/"+trpath[trpath.rfind("/")+1:-3]+"xlsx"
        if os.path.exists(rpt):
            print("Report already exists for: "+grpath[grpath.rfind('/')+1:])
            continue

        kmer = oligo.replace('[', '').replace(']', '')
        kmer = kmer.replace('+', ',').replace('_and_', ',').split(',')
        items.append((kmer, trpath, grpath, idf, rpt))
    return items

def runall(grs, trs:str, withids:bool):
    "runs on all files"
    trdirs = _gettrs(trs)
    grdirs = _getgrs(grs)
    _intersect(grdirs, trdirs)
    items  = _getitems(grdirs, trdirs, withids)

    if not os.path.exists("reports"):
        os.mkdir("reports")

    print(len(items))
    for i, (kmer, trpath, grpath, idf, rpt) in enumerate(items):
        tpath = trpath[trpath.rfind('/')+1:]
        with open("_script.py", "w") as stream:
            print("from script_runall import runone", file = stream)
            print("runone("+str(kmer)+", '"+"', '".join((tpath, grpath, idf, rpt))+"')",
                  file = stream)

        print("Running {} [{}/{}]: {}".format(kmer, i+1, len(items), tpath))
        shutil.copy(trpath, tpath)
        proc = subprocess.Popen(_CMD, shell = True, cwd = os.path.abspath("."))
        while proc.returncode is None and not os.path.exists(rpt):
            try:
                proc.wait(10)
            except subprocess.TimeoutExpired:
                pass
        if proc.returncode is None:
            proc.terminate()
            try:
                proc.wait(10)
            except subprocess.TimeoutExpired:
                if proc.returncode is None:
                    if os.sys.plaform =="win32":
                        subprocess.Popen("taskkill /F /T /PID %i" % proc.pid,
                                         shell = True)
                    else:
                        proc.kill()
        os.remove(tpath)

if __name__ == '__main__':
    if os.sys.platform == "win32":
        TRPTH = "Z:/helicon/sylwia/"
    else:
        TRPTH = "/media/data/helicon/sylwia/"

    GRPTH = (TRPTH+"/backup/cgr_projects_ready_for_hybridstat_backup/",
             TRPTH+"/backup/corrected_cgr_projects_and_reference_stretch/")
    runall(GRPTH, TRPTH, False)
