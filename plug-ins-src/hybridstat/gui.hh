#pragma once
#include "memorized_win_scan.hh"
#include "xvin.h"
#include <allegro.h>
#ifdef XV_WIN32
#   include <winalleg.h>
#endif
#include "hybridstat_core.h"

#define HYBRIDSTAT_MENUS                                                        \
((false, "align cycles on phase",                     do_align_cycle_on_phase)) \
((false, "create hybridization histogram from phase", do_create_hybridization_pattern_from_phase))\
((false, "align hybridization histogram to a peak",   do_align_hybridization_pattern_peak))       \
((false, "dump phase and get metylation histograms",  do_renormalize_and_hybridstat))             \
((false, "identify beads",                            do_identify_beads))                         \
((false, "get metylation histograms",                 do_get_methylation_time_histograms))        \
((false, "update xlsx report",                        do_update_report))
MENU_DECLARE_ALL(hybridstat,HYBRIDSTAT_MENUS)
