#ifndef HYBRIDSTAT_HH
#   define HYBRIDSTAT_HH
#include <string>
#include <boost/optional.hpp>
#include <allegro.h>
#include "xvin.h"
#include "../trackBead/record.h"

#define HYB_TIME_BY_CYCLE_PLOT_FILENAME "MethylTime(cycle)bd%dtrack%dphase%dpeak%d.gr"
#define METHYL_HISTOGRAM_PLOT_FILENAME  "Count(MethylTime(cycle))bd%dtrack%dphase%dpeak%d-tau%lf-meth-histo.gr"

#ifdef __cplusplus
#   include <map>
#   include <vector>
#   include "cordrift_core.h"
#   include "hybridstat_core.h"
#   include "hybridstat_cluster.h"
namespace hybridstat
{
    inline std::string trim(std::string reas)
    {
        auto x = reas.find_last_not_of(" \t\n");
        if(x == std::string::npos)
            return "";
        reas.resize(x+1);
        x = reas.find_first_not_of(" \t\n");
        return x != std::string::npos ? reas.substr(x) : reas;
    }

    namespace probability
    {
        using all::SmallArgs;
        Results run(all::SmallArgs const &,
                    gen_record     const *,
                    DataSet        const &,
                    Data           const &);
    }

    namespace hybridization
    {
        using Results = std::pair<double, Data>;
        Results run( Args                cf
                   , plot_region       * pr
                   , one_plot    const * cycles
                   , DataSet     const & events);
    }

    namespace identification
    {
        using Flags       = std::vector<std::string>;
        using Identifiers = std::map<std::string, std::set<int>>;
        using Params      = bestmatch::Params;
        Identifiers     read        (std::string);
        Params          readParameters(std::string);
        void            write       (std::string, Identifiers const & items);
        void            title       (one_plot *, std::string);
        void            titles      (Identifiers const &, plot_region *);
        Flags           querykeys   ();
        Identifiers     queryplots  (Flags const &, plot_region *);
    }

    namespace all
    {
        struct Key
        {
            int track;
            int bead;
        };

        struct Cmp
        {
            bool operator ()(Key const & a, Key const & b)
            { return a.track == b.track ? a.bead < b.bead : a.track < b.track; }
        };

        using Probabilities = probability::Results;
        struct Input
        {
            size_t          ncycles     = 0;
            double          uncertainty = std::numeric_limits<double>::max();
            Probabilities   probabilities;
        };

        struct Peak : public probability::Result
        {
            boost::optional<float> refpos;
        };

        struct Result: public Input
        {
            Key               key;
            size_t            ncycles    = 0;
            float             silhouette = -2.f;
            float             uncertainty  = 0.f;
            distance::Result  dist;
            std::vector<Peak> peaks;
        };

        using Inputs  = std::map<Key, Input, Cmp>;
        using Group   = std::pair<Key,std::vector<Result>>;
        using Results = std::vector<Group>;

        bool        reportinput (SmallArgs &, std::string);
        bool        reportinput (BatchArgs &, std::string);
        Results     cluster     (SmallArgs const &, Inputs &);
        Results     match       (SmallArgs const &, Inputs &);
        void        topython    (SmallArgs const &, Results const &);
        void        topython    (BatchArgs const &, Results const &);
        void        startfile   (std::string);

        std::string        openfile(SmallArgs const &,    char const *,
                                    char const *, char const *);
        inline std::string openfile(BatchArgs const & cf, char const * key,
                                    char const*, char const*)
        { return strcmp(key, "HAIRPIN-DATA") == 0 ? cf.hairpinpath : cf.idpath; }

        std::string savefile(SmallArgs const &, char const *);
        template <typename ... Args>
        inline std::string savefile(BatchArgs const & cf, Args ...)
        { return cf.reportpath; }

        template <typename ... Args>
        inline bool reportquery(SmallArgs & cf, Args && ... items)
        { cf.usemanualids = false; return config::query(MS_CKEY, items...); }

        template <typename ... Args>
        inline bool reportquery(BatchArgs const &, Args && ... els)
        {   auto t = std::make_tuple(els...); *std::get<3>(t) = false; *std::get<5>(t) = false;
            return false;
        }

        inline bool query(BatchArgs &, char const *) { return false; }

        inline void startfile(SmallArgs & cf) { startfile(cf.reportpath); }
        inline void startfile(BatchArgs &) {}

        void updatereport();
    }
}
#endif

PXV_FUNC(bool, extract_bead_info_from_cycle_plot, (O_p const *cycle_plot, int *bead_id, int *track_id));
PXV_FUNC(int, align_cycles_on_phase,(g_record *record_track, O_p *plot, int phase, int cutted_points));

PXV_FUNC(int, align_hybridization_pattern_peak, (d_s *histo_ds, int peak_index, float peak_min_threshold));
#endif
