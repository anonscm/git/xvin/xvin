#include <boost/container/stable_vector.hpp>
#include "../filters/accumulators.h"
#include <fstream>
#include <deque>
#include <map>

#include "hybridstat_core.h"
#define BUILDING_PLUGINS_DLL
#include "hybridstat.hh"
XV_FUNC(int, find_max_around,(float *x, int nx, int nmax, float *Max_pos, float *Max_val, float *Max_deriv));
namespace
{
    struct RollingMax
    {
        typedef float                       data_t;
        typedef std::pair<data_t,data_t>    pair_t;
        typedef std::deque<pair_t>          list_t;
        RollingMax(data_t w, size_t n) : _window(w), _ninc(n) {}
        RollingMax(RollingMax const &) = default;

        private:
            data_t _window;
            size_t _ninc;
            list_t _items;

        public:
            void operator () (data_t x, data_t const * y)
            {
                while(_items.size() > 0 && y[0] >= _items.back().first)
                    _items.pop_back();

                bool good = true;
                switch(_ninc)
                {
                    case 2:
                        if(y[-2] >= y[-1] || y[2] >= y[1])
                            good = false;
                    case 1:
                        if(y[-1] >= y[0] || y[1] >= y[0])
                            good = false;
                    case 0: break;
                    default:
                    {
                        for(decltype(_ninc) i = 0; i < _ninc && good; ++i)
                            if(y[-i-1] > y[-i] || y[i+1] > y[i])
                                good = false;
                    }
                }
                if(good)
                    _items.push_back({y[0],x});

                x -= _window;
                while(_items.size() && x > _items.front().second)
                    _items.pop_front();
            }

            void operator () (data_t x, data_t y)
            {
                while(_items.size() > 0 && y >= _items.back().first)
                    _items.pop_back();

                _items.push_back({y,x});

                x -= _window;
                while(x > _items.front().second)
                    _items.pop_front();
            }

            auto            ninc            () const { return _ninc;              }
            auto            window          () const { return _window;            }
            auto const &    operator*       () const { return _items.front();     }
            explicit        operator bool   () const { return _items.size() > 0;  }
    };
}

namespace hybridstat
{
    namespace theoretical
    {
        Sequences read (std::string fname)
        {
            if(fname.size() == 0)
                return {};
            std::ifstream stream(fname);
            return read(stream);
        }

        Sequences read (std::istream & stream)
        {
            Sequences    hairpins;
            std::string line;
            int         id = 1;
            while(std::getline(stream, line))
            {
                size_t i = 0, e = line.size();
                bool   isnew = false;
                while(i < e && (line[i] == ' ' || line[i] == '\t' || line[i] == '>'))
                {
                    if(line[i] == '>')
                        isnew = true;
                    ++i;
                }
                if(line[i] == '#')
                    continue;

                while(i < e && (line[e-1] == ' ' || line[e-1] == '\t' || line[e-1] == '\r'))
                    --e;

                if(isnew)
                {
                    hairpins.resize(hairpins.size()+1);
                    if(e <= i)
                    {
                        char tmp[64];
                        snprintf(tmp, sizeof(tmp), "Hairpin %d", id);
                        hairpins.back().name = tmp;
                    }
                    else
                        hairpins.back().name = line.substr(i, e-i);

                    id += 1;
                    continue;
                } else if(hairpins.size() == 0)
                {
                    hairpins.resize(1);
                    hairpins.back().name = "Hairpin 1";
                    id += 1;
                }

                if(e <= i)
                    continue;

                constexpr char diff = 'a'-'A';
                for(size_t k = i; k < e; ++k)
                    if(line[k] >= 'A' && line[k] <= 'Z')
                        line[k] = line[k]+diff;
                hairpins.back().seq += line.substr(i, e-i);
            }
            return hairpins;
        }

        Results find(Sequences const & hairpins, Oligos const & argoli)
        {
            Results out;
            std::map<std::string, float> oligos;
            for(auto const & kmer: argoli)
            {
                auto rev = kmer;
                for(size_t i = 0, e = kmer.size(); i < e; ++i)
                    switch(kmer[i])
                    {
                        case 'a': rev[e-i-1] = 't'; break;
                        case 't': rev[e-i-1] = 'a'; break;
                        case 'g': rev[e-i-1] = 'c'; break;
                        case 'c': rev[e-i-1] = 'g'; break;
                        default: assert(false && "unknown base");
                    }

               oligos[kmer] = 1.f;
               if(oligos.find(rev) == oligos.end())
                   oligos[rev] = -1.f;
            }

            for(auto const & hairpin: hairpins)
            {
                decltype(out)::value_type cur;

                cur.name         = hairpin.name;
                cur.beads        = hairpin.beads;
                cur.peaks.length = hairpin.seq.size();
                for(int i = hairpin.seq.size()-1; i >= 0; --i)
                    for(auto const & pkmer: oligos)
                    {
                        auto const & kmer = pkmer.first;
                        if(int(kmer.size()) > i)
                            continue;
                        bool good = true;
                        for(int j = kmer.size()-1, k = i-j-1; good && j >= 0; --j)
                            good = kmer[j] == hairpin.seq[k+j];

                        if(good)
                        {
                            cur.peaks.push_back({i*1.f+1.f, pkmer.second});
                            break;
                        }
                    }
                for(size_t i = 0, e = cur.peaks.size(); 2*i < e; ++i)
                    std::swap(cur.peaks[i], cur.peaks[e-1-i]);

                out.emplace_back(cur);
            }
            return out;
        }
    }

    namespace localmax
    {
        Results run(Args cf, size_t sz, float const * ys)
        {
            RollingMax rmax(cf.sigma, cf.ninc);
            Results res;
            for(auto i = rmax.ninc(), e = sz-rmax.ninc(); i < e; ++i)
            {
                rmax(i,ys+i);
                if(!rmax)
                    continue;

                auto cur = *rmax;
                if(cf.xthreshold > cur.first)
                    continue;
                if(res.size() == 0 || res.back() != cur.second)
                    res.push_back(cur.second);
            }
            return res;
        }

        Results run(Args cf, std::vector<float> const & ys)
        { return run(cf, ys.size(), &ys[0]); }
        Results  run(float s, std::vector<float> const & y)
        { return run(Args{s}, y); }
    }

    namespace maxcoordinates
    {
        Results run(Args cf, size_t        sz
                           , float const * ys
                           , float const * xd
                           , float const * yd)
        {
            auto minmax = [sz](float ix)
                          { return ix < 0 ? 0 : ix > sz ? sz: floor(ix); };

            auto lmax = localmax::run(cf,sz,ys);
            Results res;
            res.reserve(lmax.size());
            for(auto k: lmax)
            {
                auto xp = 0.f, yp = 0.f;
                if(find_max_around(const_cast<float *>(ys), sz, k, &xp, &yp, nullptr) != 0)
                    continue;

                int  pk  = minmax(xp), pk1 = minmax(xp+1.);
                auto rho = xp-pk;
                auto sppos  = [pk,pk1,rho](float const * xx)
                              { return xx[pk] * (1.-rho) + xx[pk1] * rho; };
                res.push_back({sppos(xd), sppos(yd)});
                if(res.back().second < cf.ythreshold)
                    res.pop_back();
            }
            return res;
        }

        Results run(float s, size_t sz, float const *ys
                           , float const *xd, float const *yd)
        { return run(Args{s}, sz, ys, xd, yd); }

        Results run(float s1, float s2 , size_t sz, float const *ys
                                       , float const *xd, float const *yd)
        {
            Args c;
            c.sigma      = s1;
            c.xthreshold = s2;
            return run(c, sz, ys, xd, yd);
        }

        Results run(float s1, float s2 , size_t sz, float const *ys
                                       , float const *xd)
        {
            Args c;
            c.sigma      = s1;
            c.xthreshold = s2;
            return run(c, sz, ys, xd, ys);
        }
    }

    namespace zeroclip
    {
        Results compute(Args cf, size_t nx, float const * xs, float const * ys)
        {
            RollingMax rmax(cf.wMax, cf.ninc);

            Results res;
            for(auto i = rmax.ninc(), e = nx-rmax.ninc(); i < e; ++i)
            {
                rmax(xs[i],ys+i);
                if(!rmax)
                    continue;

                auto cur = *rmax;
                if(std::abs(cur.second) < cf.wZero)
                {
                    if(res.zero.first < cur.first )
                    {
                        if(res.zero.first > res.max.first)
                            res.max = res.zero;
                        res.zero = cur;
                    }
                }
                else if(res.max.first < cur.first)
                    res.max = cur;
            }

            return res;
        }

        bool apply(Args cf, Results const & res, size_t nx, float * ys)
        {
            if(!res.good(cf))
                return false;

            bool bfound = false;
            auto max    = res.clip(cf);
            for(decltype(nx) i = 0u; i < nx; ++i)
                if(ys[i] > max)
                {
                    bfound = true;
                    ys[i]  = max;
                }

            return bfound;
        }

        bool run(Args cf, size_t nx, float const * xs, float * ys)
        {
            auto res = compute(cf, nx, xs, ys);
            return apply(cf, res, nx, ys);
        };
    }

    namespace histogram
    {
        namespace
        {
            inline data_set       const * _get(data_set       const * ds) { return ds; }
            inline cordrift::Data const * _get(cordrift::Data const & ds) { return &ds; }

            template <typename T>
            Results _run(Args cf, T const & phases)
            {
                using namespace stats;
                acc_t<bat::min, bat::max> rng;

                std::map<decltype(_get(phases[0])), std::pair<float,float>> dsinfo;
                auto const s2 = cf.sigma*cf.sigma;
                for(auto const & tds: phases)
                {
                    decltype(auto) ds = _get(tds);
                    acc_t<bat::count, bat::median, bat::variance> info;
                    for(auto x = ds->yd; x != ds->yd+ds->nx; ++x)
                        if(std::abs(*x) < cf.maxabs)
                            info(*x);

                    auto cnt = ba::count(info);
                    if(cnt <= 0)
                        continue;

                    auto var = ba::variance(info)/cnt;
                    if(var != 0 && cnt <= 4)
                        continue; // student-t's variance is infinite

                    if(cnt >= 4)
                        var *= (ds->nx-2)/(cnt-4); // student-t's variance
                    dsinfo.insert({ds, {ba::mean(info), std::sqrt(s2+var)}});
                    rng(ba::mean(info));
                }
                if(dsinfo.size() == 0)
                    return {0, 0, 0, {}};

                auto const hz    = floor(cf.sigma*cf.sz/cf.bin);
                auto const ymin  = ba::min(rng)-cf.bin*5;
                auto const ymax  = ba::max(rng)+cf.bin*5;
                int  const hmax  = int(ceil((ymax-ymin)/cf.bin))+1;

                decltype(Results::data) hist(hmax);
                for(auto const & pds: dsinfo)
                {
                    auto       ds   = pds.first;
                    auto const y0   = pds.second.first-ymin;
                    auto const sig  = pds.second.second;
                    int  const ibin = floor(y0/cf.bin);
                    auto const w    = cf.reg ? 1 : ds->nx;

                    auto       k    = ibin      < hz   ? 0    : ibin-hz;
                    auto const e    = ibin+hz+1 > hmax ? hmax : ibin+hz+1;
                    assert(e <= hmax);
                    if(cf.type == Args::Type::kGaussian)
                        for(; k < e; ++k)
                        {
                            auto x   = (k*cf.bin-y0)/sig;
                            hist[k] += w*std::exp(-x*x*.5);
                        }
                    else
                        for(; k < e; ++k)
                            hist[k] += w;
                }

                Results res{float(ymin), float(ymax), cf.bin, {}};
                std::swap(res.data, hist);
                return res;
            }
        }

        Results run(Args cf, std::vector<data_set const*> phases)
        { return _run(cf, phases); }

        Results run(Args cf, cordrift::DataSet const & phases)
        { return _run(cf, phases); }
    }

    namespace intervals
    {
        namespace
        {
            template <typename T>
            Results _run(Args const & cf, T const * rec, DataSet const & items)
            {
                auto icf     = cordrift::intervals::args(cf);
                icf.lookup   = cf.lookup;
                icf.lookdown = cf.lookdown;
                auto res     = cordrift::intervals::run(icf, rec, items);

                if(cf.firstphase != int(phases::pull))
                    return res;

                // now keep only one event in phase 3:
                return cordrift::select::unique(int(phases::pull), rec, res);
            }
        }

        Results run(Args cf, gen_record  const * gr, DataSet const & inp)
        { cf.nei = 0; return _run(cf, gr, inp); }
        Results run(Args cf, plot_region const * gr, DataSet const & inp)
        { cf.nei = 0; return _run(cf, gr, inp); }
    }

    namespace probability
    {
        void Probability::operator()(float sz, bool ismax)
        {
            if(sz < nmin)
                return;
            ++_count;
            if(ismax)
                ++_big;

            _sum += sz;
        }

        bool Probability::good() const
        { return _count > 0 && nmin < (1.f-float(_big)/_count+_sum/_count);  }

        float Probability::probability() const
        {
            if(_count <= _big)
                return 0.;

            typedef decltype(probability()) real;
            real mrho = real(1)-real(_big)/_count;
            real tmp  = mrho+_sum/_count;
            if(tmp <= nmin)
                return 0.;
            return mrho/(tmp-nmin);
        }

        float Probability::time(float rate) const
        {
            //  we want the average time knowing that the
            //  probability is p (1-p)^{t * F} = p exp(t * F * ln(1-p))
            //  Thus the time is : -1/(ln(1-p)*F)
            typedef decltype(time(1.f)) real;
            real p = 1.-probability();
            if(p <= 0.)
                return 0.;

            real lnp = -std::log(p)*rate;
            if(lnp <= 0.)
                return std::numeric_limits<real>::max();
            else if(!std::isfinite(lnp))
                return 0.;
            else
                return 1./lnp;
        }

        float Probability::stddev() const
        {
            if(_count <= 0)
                return 0.0;
            auto mrho = 1.f-float(_big)/_count;
            auto tmp  = mrho+_sum/_count;
            if(tmp <= nmin)
                return 0.f;
            auto p = mrho/(tmp-nmin);
            return p /mrho * sqrt(1-p);
        }

        float Probability::uncertainty(float rate) const
        {
            typedef decltype(uncertainty(1.)) real;
            if(_count <= _big)
                return std::numeric_limits<real>::max();

            return time(rate) / std::sqrt(_count-_big);
        }

        /* pimpl pattern */
        class _PPImpl: public Probability
        {
            using Probability::Probability;
            friend struct PeakProbabilities;
            struct _Item
            {
                Probability prob;
                float       maxx;
                float       minx;
                float       xp, yp;
                boost::container::stable_vector<Position> events;
            };

            struct _Cmp
            {
                bool operator()(_Item const & a, float b) const
                { return a.minx < b; };
            };


            float               _sigma;
            std::vector<_Item>  _items;
        };

        PeakProbabilities::PeakProbabilities (size_t nmin, float sigma)
            : _impl(new _PPImpl(nmin))
        { _impl->_sigma = sigma; }

        PeakProbabilities::~PeakProbabilities() { delete _impl; }

        void    PeakProbabilities::peak    (float xp, float yp)
        {
            auto &      items = _impl->_items;
            auto        sigma = _impl->_sigma;
            auto const  prob  = *_impl;

            auto const  rho   = hybridstat::histogram::square(sigma).sz;
            items.push_back({prob, xp-rho*sigma, xp+rho*sigma, xp, yp, {}});
        }

        void    PeakProbabilities::event(float const * yd, size_t r1, size_t r2, size_t rmax)
        {
            auto & items = _impl->_items;
            auto cmp     = _PPImpl::_Cmp();
            auto xp      = stats::compute<stats::bat::median>(r2-r1, yd+r1);
            auto yp      = r2 >= rmax ? rmax-r1 : r2-r1;
            for(auto e = items.end()
                   , i = std::lower_bound(items.begin(), e, xp, cmp)
                   ; i != e && i->maxx < xp; ++i)
            {
                i->prob(yp, r2 >= rmax);
                i->events.push_back({float(xp), float(yp), r2 < rmax});
            }
        }

        Results PeakProbabilities::results (float rate) const
        {
            auto const & items = _impl->_items;
            Results r(items.size());
            for(decltype(r.size()) i = 0u, n = r.size(); i < n; ++i)
            {
                auto        & it = items[i];
                r[i].xp     = it.xp;
                r[i].yp     = it.yp;
                r[i].good   = it.prob.good       ();
                r[i].prob   = it.prob.probability();
                r[i].time   = it.prob.time       (rate);
                r[i].uncert = it.prob.uncertainty(rate);
                r[i].events.assign(it.events.begin(), it.events.end());
            }
            return r;
        }
    }
}
