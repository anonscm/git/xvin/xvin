#include <random>
#include <algorithm>
#include <utility>
#include <map>
#include <list>
#include <valarray>
#include <nlopt.hpp>
#include "hybridstat_cluster.h"
#include "../trackBead/indexing.h"
#include "../filters/accumulators.h"
#include "ompbase.h"
namespace hybridstat
{
    namespace distance
    {
        namespace
        {
            template <typename K0, typename K1, typename T>
            void _matched( K0             add
                         , K1             discard
                         , bool           bzero
                         , float          sigma
                         , Result const & res
                         , T      const & beadX
                         , T      const & beadY
                         )
            {
                struct _MInfo
                {
                    bool   color;
                    size_t ind;
                    float  pos;
                };

                using list_t  = std::list<_MInfo>;

                auto pX   = [&](size_t i) -> _MInfo
                            { return {true,  i, float(beadX[i].first)}; };
                auto pY   = [&](size_t i) -> _MInfo
                            { return {false, i, float((beadY[i].first-res.bias)/res.dyn)}; };

                auto endoflist  = [&](auto const &lst, auto const & minc)
                                {
                                    if(lst.size() == 0)
                                        return false;

                                    auto const & back = lst.back();
                                    return minc.color == back.color
                                        || back.pos < minc.pos - sigma;
                                };
                auto findbest   = [](auto const & cur)
                                {
                                    auto val  = std::numeric_limits<float>::max();
                                    auto best = cur.end();
                                    auto lpos = cur.front().pos;
                                    for(auto it = std::next(cur.begin())
                                           , e  = cur.end(); it != e; ++it)
                                    {
                                        if(it->pos-lpos < val)
                                        {
                                            best = it;
                                            val  = it->pos-lpos;
                                        }
                                        lpos = it->pos;
                                    }
                                    return best;
                                };
                auto empty      = [&](auto && cur)
                                {
                                    auto left = std::move(cur);

                                    std::list<list_t> stack;
                                    while(left.size() > 1 || stack.size() > 0)
                                    {
                                        if(left.size() <= 1)
                                        {
                                            if(     left.size() == 1
                                                &&  discard(left.front())
                                              ) return true;

                                            left = std::move(stack.back());
                                            stack.pop_back();
                                        }

                                        auto best = findbest(left);
                                        auto prev = std::prev(best);
                                        if(best->color)
                                            add(*best, *prev);
                                        else
                                            add(*prev, *best);

                                        list_t right;
                                        right.splice(right.end(), left, ++best, left.end());
                                        if(right.size() > 1)
                                            stack.emplace_back(right);

                                        left.pop_back();
                                        left.pop_back();
                                    }
                                    return false;
                                };

                size_t       iX   = bzero ? 1 : 0, iY   = 0;
                size_t const eX   = beadX.size(),  eY   = beadY.size();
                _MInfo       minc{true, 0, 0.f};

                list_t curlist;
                for(auto maxc = pY(0); iX < eX && iY < eY; minc.color ? ++iX : ++iY)
                {
                    minc = minc.color ? pX(iX) : pY(iY);
                    if(minc.pos > maxc.pos)
                        std::swap(minc, maxc);

                    if(endoflist(curlist, minc) && empty(curlist))
                        return;

                    curlist.push_back(minc);
                }

                minc = iX == eX ? pY(iY++) : pX(iX++);
                if(!endoflist(curlist, minc))
                    curlist.push_back(minc);

                while(iX < eX)
                    if(discard(pX(iX++)))
                        return;
                while(iY < eY)
                    if(discard(pY(iY++)))
                        return;

                empty(curlist);
            }

            struct _Info
            {
                using real    = double;
                struct _Input : public std::valarray<std::pair<real,real>>
                {
                    using std::valarray<std::pair<real,real>>::valarray;
                    real length = 0;
                };

                using _Sigmas = std::valarray<real>;
                using _Pair   = std::pair<_Info const *, real>;

                template <typename T>
                static auto input(T const & x)
                -> decltype(std::begin(x)->first,x.length, _Input())
                {
                    _Input r(x.size());
                    r.length = x.length;
                    std::copy(std::begin(x), std::end(x), std::begin(r));
                    return r;
                }

                template <typename T>
                static auto input(T const & x)
                -> decltype(*std::begin(x)+1., _Sigmas())
                {
                    _Sigmas r(x.size());
                    std::copy(std::begin(x), std::end(x), std::begin(r));
                    return r;
                }

                static _Sigmas && input(_Sigmas && x) { return std::move(x); }
                static _Input  && input(_Input  && x) { return std::move(x); }
                static size_t     input(size_t     x) { return x; }

                template <typename T0, typename T1, typename T2>
                _Info(T0 && b1, T1 && b2, T2 && s)
                    : b1(input(std::forward<T0>(b1)))
                    , b2(input(std::forward<T1>(b2)))
                    , sigmas(input(std::forward<T2>(s)))
                {}

                void normalize()
                {
                    if(_normalized)
                        return;
                    _normalized = true;
                    // The normalization tries to subdue the effect of having
                    // a different number of peaks and peak heights for each
                    // bead.
                    auto norm = [=](auto & b)
                                {
                                    _Info::real s = 0.;
                                    for(auto const & x: b) s += x.second*x.second;

                                    s  = std::sqrt(s);
                                    for(auto & v: b)
                                        v.second /= s;
                                };

                    norm(b1);
                    norm(b2);
                    for(auto &v: b1) v.second =  std::abs(v.second);
                    for(auto &v: b2) v.second = -std::abs(v.second);
                }

                static double run(unsigned, double const *, double *, void *);
                Measure _compute(real alpha, real beta, real sig, bool ord) const;
                Measure operator()(double const *, real) const;

                _Input  b1;
                _Input  b2;
                _Sigmas sigmas;
                bool    sym = false;
                bool    _normalized = false;
            };

            Measure _Info::_compute(real alpha, real beta, real sig, bool ord) const
            {
                real sum   = 0.0f;
                real norm1 = 0.0f;

                real grsum [2] = {0.f, 0.f};
                real grnorm    = 0.f;
                auto const & _b1 = ord ? b1 : b2;
                auto const & _b2 = ord ? b2 : b1;
                for(auto const & v2: _b2)
                {
                    for(auto const & v1: _b1)
                    {
                        auto d = (v1.first-alpha*v2.first-beta)/sig;
                        auto e = std::exp(-.5*d*d);
                        auto c = e*d/sig;

                        sum      += e;
                        grsum[0] += c*v2.first;
                        grsum[1] += c;
                    }

                    for(auto const & v1: _b2)
                    {
                        auto d = (v1.first-v2.first)*alpha/sig;
                        auto e = std::exp(-.5*d*d);

                        norm1  += e;
                        grnorm += e*d/sig*(v2.first-v1.first);
                    }
                }

                real norm2 = 0.0f;
                for(auto const & v1: _b1)
                    for(auto const & v2: _b1)
                    {
                        auto d = (v1.first-v2.first)/sig;
                        norm2 += std::exp(-.5*d*d);
                    }

                auto c = std::sqrt(norm1*norm2);
                auto x = sum/c;
                return {1.-x, {(.5*grnorm*sum/norm1-grsum[0])/c, -grsum[1]/c}};

            }

            Measure _Info::operator()(double const *x_, real sig) const
            {
                real alpha = (real) x_[0];
                real beta  = (real) x_[1];
                auto r1    = _compute(alpha, beta, sig, true);
                auto r2    = _compute(1./alpha, -beta/alpha, sig, false);
                return { r1.val+r2.val,
                        { r1.grad[0]-(r2.grad[0]-r2.grad[1]*beta)/(alpha*alpha),
                          r1.grad[1]-r2.grad[1]/alpha}};
            }

            double _Info::run(unsigned, double const *x, double * g, void * d)
            {
                auto const & pair = *(_Info::_Pair*)(d);
                auto const & info = *pair.first;
                auto         r    = info(x, pair.second);
                if(g != nullptr) { g[0] = r.grad[0]; g[1] = r.grad[1]; }
                return r.val;
            }

            Result _run_hairpin(Args const & cf, _Info const  & info)
            {
                auto const & b1  = info.b1;
                auto const & b2  = info.b2;
                auto const   e1  = b1.size();
                auto const   e2  = b2.size();
                auto const   sig = info.sigmas[0];
                Result best{std::numeric_limits<float>::max(), 1., 0. };

                auto compute = [&,e1,e2](auto const & res)
                {
                    size_t cnt  = 0;
                    auto   sum  = 0.;
                    _matched([&](auto const & a, auto const & b)
                                { sum += std::abs(a.pos-b.pos); ++cnt; },
                             [&](auto const & a)
                                {
                                    if(!a.color && b1.length != 0 && a.pos > b1.length)
                                        sum += 5*sig;
                                    sum += sig;
                                    return sum > best.dist;
                                },
                             true, sig, res, b2, b1);
                    sum /= sig * cnt;
                    return sum;
                };

                Result cur;
                auto const & dyn   = cf.griddyn;
                auto const & bias  = cf.gridbias;
                auto const   edyn  = dyn .last + dyn .step *.5;
                auto const   ebias = bias.last + bias.step *.5;
                for(cur.bias = bias.first; cur.bias < ebias; cur.bias += bias.step)
                    for(cur.dyn = dyn.first; cur.dyn < edyn; cur.dyn += dyn.step)
                    {
                        cur.dist = compute(cur);
                        if(cur.dist < best.dist)
                            best = cur;
                    }
                return best;
            }

            Result _run_blind(Args const & cf, _Info & info)
            {
                double                minf = std::numeric_limits<float>::max();
                std::vector<double>   cur;
                std::valarray<double> best {std::numeric_limits<float>::max(), 1., 0. };
                auto                  pair = _Info::_Pair(&info, info.sigmas[0]);

                info.normalize();
                if(info.b1.size() <= 1 || info.b2.size() <= 1)
                    return { float(minf), 1.f, 0.f };

                auto const & dyn   = cf.griddyn;
                auto const & bias  = cf.gridbias;

                auto optimize = [&](auto d, auto b)
                                {
                                    nlopt::opt opt(nlopt::LD_LBFGS, 2);
                                    opt.set_xtol_rel(cf.xrel);
                                    opt.set_ftol_rel(cf.frel);
                                    opt.set_xtol_abs(cf.xabs);
                                    opt.set_stopval (cf.stopval);
                                    opt.set_maxeval (cf.maxeval);
                                    opt.set_min_objective(_Info::run, &pair);

                                    std::vector<double>
                                        lb  = {dyn.first-dyn.step,  bias.first-bias.step},
                                        ub  = {dyn.last +dyn.step,  bias.last +bias.step};
                                    opt.set_lower_bounds(lb);
                                    opt.set_upper_bounds(ub);

                                    cur = {d, b};

                                    std::vector<double> tmp = {d, b};
                                    try
                                    {
                                        opt.optimize(tmp, minf);
                                        cur = tmp;
                                        return true;
                                    }
                                    catch(std::runtime_error &)
                                    {   return false; }
                                };

                auto const edyn  = dyn .last + dyn .step *.5;
                auto const ebias = bias.last + bias.step *.5;
                for(double d = dyn.first; d < edyn; d += dyn.step)
                    for(double b = bias.first; b < ebias; b += bias.step)
                        if(optimize(d, b) && minf < best[0])
                            best = {minf, cur[0], cur[1]};

                if(best[0] == std::numeric_limits<float>::max())
                    return {std::numeric_limits<float>::max(), 1.0f, 0.f};

                for(auto sig: info.sigmas)
                    if(pair.second != sig)
                    {
                        pair.second = sig;
                        if(optimize(best[1], best[2]))
                            best = {minf, cur[0], cur[1]};
                        else
                            best[0] = _Info::run(2, &best[1], nullptr, &pair);
                    }

                return {float(best[0]), float(best[1]), float(best[2])};
            }

            Result _run(Args const &  cf, _Info & info)
            {
                if(cf.strategy == int(Strategy::blind))
                    return _run_blind(cf, info);
                else
                    return _run_hairpin(cf, info);
            }

            Result _run( Args const  & cf
                       , Input const & b1
                       , Input const & b2
                       )
            {
                _Info info(b1, b2, cf.sigmas);
                return _run(cf, info);
            }
        }

        Measure convolve(float const sigma, float dyn, float bias,
                         Input const & b1, Input const & b2)
        {
            auto s = _Info::_Sigmas{sigma};
            _Info info(b1, b2, s);
            double x[2] = { dyn, bias };
            info.normalize();
            return info(x, sigma);
        }

        Result run(Args const & cf, Input const & b1, Input const & b2)
        { return _run(cf, b1, b2); }

        Results run(Args const & mcf, std::vector<Input> const & b)
        {
            auto const n = b.size();

            std::vector<std::pair<Key, Result>> tmp;
            for(decltype(b.size()) i = 0; i < n; ++i)
                for(decltype(b.size()) j = i; j < n; ++j)
                    tmp.emplace_back(Key(i,j), Result{});

            auto cf     = mcf;
            cf.griddyn  = {.85, 1.15, .05};
            cf.gridbias = {-.01, .01, .01};
            cf.sigmas   = {.1};

            auto const e = int(tmp.size());
            PRAGMA_OMP(parallel)
            {
                PRAGMA_OMP(for)
                for(int k = 0; k < e; ++k)
                {
                    _Info info(b[tmp[k].first.first],
                               b[tmp[k].first.second],
                               cf.sigmas);
                    info.sym = true;
                    for(int i = info.b1.size()-1; i >= 0; --i)
                        info.b1[i].first -= info.b1[0].first;

                    for(int i = info.b2.size()-1; i >= 0; --i)
                        info.b2[i].first -= info.b2[0].first;

                    auto r  = _run(cf, info);
                    r.bias += b[tmp[k].first.first] [0].first;
                    r.bias -= b[tmp[k].first.second][0].first *r.dyn;
                    tmp[k].second = r;
                }
            }

            Results r;
            for(int k = 0; k < e; ++k)
                r.emplace(tmp[k]);
            return r;
        }

        Result convert(size_t a, size_t b, Result const & r)
        {
            if(a < b)
                return r;
            else if(a == b)
                return {r.dist, 1.f, 0.f};
            else
                return {r.dist, 1.f/r.dyn, -r.bias/r.dyn};
        }

        Matches matched( bool           bzero
                       , float          sigma
                       , Result const & res
                       , Input  const & beadX
                       , Input  const & beadY
                       )
        {
            Matches m;
            _matched([&](auto const & a, auto const & b)
                        { m.emplace_back(a.ind, b.ind); },
                     [](...) { return false; },
                     bzero, sigma, res, beadX, beadY);
            std::sort(m.begin(), m.end());
            return m;
        }
    }

    namespace cluster
    {
        namespace
        {
            size_t _size(Input const & c)
            { return floor(std::sqrt(1.+8.*c.size())*.5+.4); }

            auto _at(Input const & c, size_t i, size_t j)
            { return c.at({i, j}).dist; }

            auto _at(Input const & c, Item const & i, Item const & j)
            { return c.at({i.id, j.id}).dist; }

            using ddist_t = std::discrete_distribution<size_t>;

            void _seed(Input const & costs, Results & as, std::mt19937 & gen)
            {
                auto const n  = _size(costs);
                auto const k  = as.groups.size();

                std::valarray<double> mincosts(n);

                // randomly pick the first center
                auto p = std::uniform_int_distribution<size_t>(0,n-1)(gen);
                as.groups[0].first = p;
                for(size_t i = 0; i < n; ++i)
                    if(i != p) mincosts[i] = _at(costs, p, i);

                // pick remaining (with a chance proportional to mincosts)
                for(size_t j = 1; j < k; ++j)
                {
                    auto p = ddist_t(std::begin(mincosts), std::end(mincosts))(gen);
                    as.groups[j].first = p;

                    mincosts[p]        = 0.f;
                    for(size_t i = 0; i < n; ++i)
                        if(mincosts[i] > 0.f)
                        {
                            double d = _at(costs, p, i);
                            if(d < mincosts[i])
                                mincosts[i] = d;
                        }
                }
            }

            size_t _find_medoid(Input const & costs, Items const & grp)
            {
                std::valarray<float> sums(grp.size());
                for(size_t i = 0, n = grp.size(); i < n; ++i)
                    for(size_t j = i; j < n; ++j)
                    {
                        auto s = _at(costs, grp[i], grp[j]);
                        sums[i] += s;
                        sums[j] += s;
                    }

                auto i = std::min_element(std::begin(sums), std::end(sums))-std::begin(sums);
                return grp[i].id;
            }

            auto _silhouette(float v1, float v2)
            { return ((v2-v1)/(v1 < v2 ? v2 : v1)-.5f)*2.f; }

            size_t _update(Input    const & costs,
                           Results        & as)
            {
                size_t const nclust = as.groups.size();
                size_t const nitems = _size(costs);
                for(auto & grp: as.groups)
                    grp.second.resize(0);

                as.total   = 0.0;
                size_t cnt = 0;
                for(size_t j = 0; j < nitems; ++j)
                {
                    auto   v1   = std::numeric_limits<float> ::max();
                    auto   v2   = std::numeric_limits<float> ::max();
                    size_t sz   = std::numeric_limits<size_t>::max();
                    size_t igrp = 0;
                    for(size_t k = 0; k < nclust; ++k)
                    {
                        auto x = _at(costs, as.groups[k].first, j);
                        if(x < v1 || (x == v1 && sz > as.groups[k].second.size()))
                        {
                            sz   = as.groups[k].second.size();
                            igrp = k;
                            v2   = v1;
                            v1   = x;
                        } else if(x < v2)
                            v2   = x;
                    }

                    auto & grp = as.groups[igrp];
                    if(grp.first != as.values[j])
                        cnt += 1;
                    as.values[j] = grp.first;
                    as.total    += v1;

                    grp.second.emplace_back(Item{j, _silhouette(v1, v2)});
                }
                return cnt;
            }

            bool _disband_smallest(Args const & cf, Results & res, std::mt19937 & gen)
            {
                auto       & groups = res.groups;
                auto         ms     = [](auto const & a, auto const & b)
                                      { return a.second.size() < b.second.size(); };
                auto       & small  = *std::min_element(groups.begin(), groups.end(), ms);
                auto const & big    = *std::max_element(groups.begin(), groups.end(), ms);
                if(small.second.size() < size_t(floor(cf.ratio * big.second.size()+1)))
                {
                    std::valarray<float> silh(res.values.size());
                    for(auto const & grp: groups)
                        if(grp.first != small.first)
                        {
                            for(auto const & it: grp.second)
                                silh[it.id] = 1.-it.silhouette;
                            silh[grp.first] = 0.;
                        }
                    small.first = ddist_t(std::begin(silh), std::end(silh))(gen);
                    return true;
                }
                return false;
            }

            void _sort(Results & as)
            {
                for(auto & g: as.groups)
                {
                    auto i = g.first;
                    for(auto const & x: g.second)
                        as.values[x.id] = i;

                    auto cmp = [&](auto const & a, auto const & b)
                               {
                                   constexpr auto fm = std::numeric_limits<float>::max();
                                   float xa = a.id == i ? fm : a.silhouette;
                                   float xb = b.id == i ? fm : b.silhouette;
                                   return xa > xb;
                               };
                    std::sort(g.second.begin(), g.second.end(), cmp);
                }
            }
        }

        Results run(Args const & cf, Input const & costs)
        {
            size_t const n = floor(std::sqrt(1.+8.*costs.size())*.5+.4);
            // prepare storage
            Results as;
            as.values.assign(n, std::numeric_limits<size_t>::max());
            as.groups.resize(cf.nclusters < n ? cf.nclusters : n);
            as.total = 0.0f;

            std::random_device rd;
            std::mt19937 gen(rd());
            _seed(costs, as, gen);

            // initialize assignments
            _update(costs, as);

            bool    converged = false;
            size_t  nsmall    = 0;
            for(size_t t = 0; !converged &&  t < cf.maxiter; ++t)
            {
                // update medoids
                for(auto & o: as.groups)
                    o.first = _find_medoid(costs, o.second);

                // update assignments
                auto tcost_pre = as.total;
                auto cnt       = _update(costs, as);

                if(nsmall < cf.maxsmalliter && _disband_smallest(cf, as, gen))
                    // smallest group was disbanded.
                    // A new medoid was selected based on silhouette values
                    ++nsmall;
                else
                    // check convergence
                    converged = (cnt == 0 || std::abs(as.total - tcost_pre) < cf.tol);
            }
            _sort(as);

            auto cmp = [](auto const & a, auto const & b)
                       { return a.second.size() == b.second.size()
                              ? a.first         <  b.first
                              : a.second.size() <  b.second.size(); };
            std::sort(as.groups.begin(), as.groups.end(), cmp);
            return as;
        }
    }

    namespace bestmatch
    {
        namespace
        {
            template <typename T0, typename T1>
            auto _transform(T0 const & x, T1 y)
            -> decltype(x.size(),
                        std::valarray<typename std::remove_cv<decltype(y(x[0]))>::type>{})
            {
                size_t i = 0, e = x.size();
                std::valarray<typename std::remove_cv<decltype(y(x[0]))>::type> ans(e);
                for(; i < e; ++i)
                    ans[i] = y(x[i]);
                return ans;
            }

            template <int I, typename T>
            auto _acc(T x, T y)
            {
                return std::accumulate(x, y, 1.f,
                                      [](auto a, auto b)
                                      { return a+std::get<I>(b); });
            }

            template <int I, typename T0>
            auto _mean(T0 const & x)
            { return _acc<I>(std::begin(x), std::end(x)) / x.size(); }

        }

        bestmatch::Result  run( Args             cf
                              , Hairpins const & refs
                              , Input    const & bead
                              , int              ibead)
        {
            auto addzero = [=](auto & b)
                            {
                                auto x = std::move(b);
                                b.resize(x.size()+1);
                                b[0] = {0., 1.};
                                for(size_t i = 0; i < x.size(); ++i)
                                    b[i+1] = x[i];
                            };

            Result res;
            auto dyn = cf.griddyn;
            for(size_t i = 0, e = refs.size(); i < e; ++i)
            {
                distance::_Info info(refs[i].peaks, bead, cf.sigmas);
                auto rho  = cf.ext5 <= 0.f ? _mean<0>(info.b2)/_mean<0>(info.b1)
                                           : cf.ext5;
                addzero(info.b1);
                info.normalize();
                for(int i = info.b2.size()-1; i >= 0; --i)
                    info.b2[i].first -= info.b2[0].first;

                if(cf.params.find(ibead) != cf.params.end())
                {
                    auto  r = cf.params.at(ibead).first;
                    cf.griddyn.first = r;
                    cf.griddyn.last  = r;
                    cf.griddyn.step  = dyn.step/rho/4.;
                } else
                {
                    cf.griddyn = dyn;
                    cf.griddyn.first /= rho;
                    cf.griddyn.last  /= rho;
                    cf.griddyn.step  /= rho;
                }

                int nsteps = (cf.griddyn.last-cf.griddyn.first)/cf.griddyn.step+.5;
                if(cf.griddyn.last*info.b2[info.b2.size()-1].first > info.b1.length)
                {
                    cf.griddyn.last = info.b1.length/info.b2[info.b2.size()-1].first
                                    + cf.gridbias.last;
                    if(cf.griddyn.last < cf.griddyn.first)
                        cf.griddyn.first = cf.griddyn.last;
                    else
                        cf.griddyn.step = (cf.griddyn.last-cf.griddyn.first)/nsteps;
                }

                //info.b1.length = info.b1.length + cf.gridbias.last;
                //info.sigmas /= rho;

                auto r = distance::_run(cf, info);
                if(res.dist.size() == 0 || r.dist < res.dist[res.id].dist)
                    res.id = i;


                res.dist.emplace_back(
                        distance::Result{ r.dist,
                                          float(r.dyn),
                                          float(r.bias-bead[0].first*r.dyn)
                                         });
            }
            return res;
        }

        bestmatch::Result  run( Args             cf
                              , Hairpins const & refs
                              , Input    const & bead)
        { return run(cf, refs, bead, -std::numeric_limits<int>::max()); }

        Results  onerun(Args const & cf_, Hairpins           const & refs
                                        , std::vector<Input> const & beads)
        {
            Args cf = cf_;
            if(cf.strategy == int(distance::Strategy::hairpin))
            {
                cf.sigmas   = {.015};
                cf.griddyn  = {.8,  1.2, .001};
            } else
            {
                cf.sigmas   = {15.};
                cf.griddyn  = {.7, 1.3, .15};
                cf.gridbias = {-20, 20, 10.};
            }

            Results res;
            auto &  dist = res.first;
            auto &  grp  = res.second;

            grp.groups.resize(refs.size());
            grp.values.resize(beads.size()+refs.size());
            for(size_t i = 0, e = refs.size(); i < e; ++i)
            {
                grp.groups[i].first = i;
                grp.groups[i].second.push_back({i, 1.});
                dist.emplace(distance::Key(i, i), distance::Result{0., 1., 0.});
            }

            size_t nref  = refs.size();
            auto   cpy   = refs;
            if(cf.ext3 > 0.f)
            {
                auto const delta = cf.ext3/cf.ext5;
                for(auto & hpin: cpy)
                    hpin.peaks.emplace_back(hpin.peaks.length*delta, 1.f);
            }

            auto e = int(beads.size());
            PRAGMA_OMP(parallel for)
            for(int i = 0; i < e; ++i)
            {
                auto bead = beads[i];
                auto sz   = bead.size();
                if(cf.ext3 > 0.f && sz > 2)
                    bead[sz-1].second = cf.height3 * _acc<1>(&bead[1], &bead[sz-2])/(sz-2);

                auto cur = run(cf, cpy, bead, i);

                // change id to match one already found
                for(size_t k = 0, ke = cpy.size(); k < ke; ++k)
                    if(refs[k].beads.find(i) != refs[k].beads.end())
                    {
                        cur.id = k;
                        break;
                    }

                auto v1  = cur.dist[cur.id].dist;
                auto v2  = std::numeric_limits<float>::max();
                for(size_t k = 0; k < nref; ++k)
                {
                    dist.emplace(distance::Key(k, i+nref), cur.dist[k]);
                    if(cur.id != k && v2 > cur.dist[k].dist)
                        v2 = cur.dist[k].dist;
                }

                PRAGMA_OMP(critical)
                { grp.groups[cur.id].second.push_back({i+nref, cluster::_silhouette(v1, v2)}); }
            }
            cluster::_sort(grp);
            return res;
        }

        Results  run(Args const & cf_, Hairpins                   refs
                                     , std::vector<Input> const & beads)
        {
            auto  out     = onerun(cf_, refs, beads);
            if(beads.size() == 1)
                return out;

            float ncycles = indexing::ncycles(indexing::record());
            for(auto strat: cf_.strategies)
            {
                auto cf     = cf_;
                if(strat != int(distance::Strategy::automatic))
                    cf.strategy = strat;

                auto const & cl   = out.second;
                auto const & dist = out.first;
                for(auto const & grp: cl.groups)
                {
                    auto & ref = refs[grp.first].peaks;
                    std::map<float, std::vector<float>> peaks;
                    for(auto const & peak: ref)
                        peaks[peak.first] = {};

                    for(auto item: grp.second)
                        if(grp.first != item.id)
                        {
                            auto  id   = item.id-refs.size();
                            distance::Key k(grp.first,item.id);
                            auto &bead = beads[id];
                            auto  tmp  = distance::convert(grp.first, item.id, dist.at(k));
                            float prec = cf.matchwidth;
                            for(auto && m: distance::matched(true, prec, tmp, bead, ref))
                                peaks.at(ref[m.second].first).push_back(bead[m.first].second);
                        }

                    stats::acc_t<stats::bat::min> minv;
                    for(auto & peak: ref)
                    {
                        auto   norm = std::copysign(1.f, peak.second)/ncycles;
                        auto & vals = peaks.at(peak.first);
                        if(vals.size() != 0)
                        {
                            peak.second = stats::median(vals)*norm;
                            minv(peak.second);
                        }
                    }

                    auto val = stats::compute(minv);
                    for(auto & peak: ref)
                    {
                        auto norm = std::copysign(1.f, peak.second);
                        auto & vals = peaks.at(peak.first);
                        if(vals.size() == 0)
                            peak.second = norm*val;
                    }
                }

                out = onerun(cf_, refs, beads);
            }

            return out;
        }
    }
}
