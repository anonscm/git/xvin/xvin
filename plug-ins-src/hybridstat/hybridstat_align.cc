#include "../filters/accumulators.h"

#include "hybridstat_core.h"
namespace hybridstat { namespace alignment
{
    namespace
    {
        template <typename T>
        auto _computestat(DataSet const & dset)
        {
            using namespace stats;
            stats::acc_t<T> acc;
            omp::mapreduce(acc, dset,
                           [](auto & cur, auto const & ds)
                           { cur(stats::compute<T>(ds.nx, ds.yd)); },
                           [](auto & cur,   auto const & b)
                           { cur(stats::compute(b)); });
            return stats::compute(acc);
        }

        struct _MinDistInfo
        {
            DataSet items;
            float   cnt   = 0.f;
            float   delta = 0.f;

            void push_back(Data const & ds)
            {
                items.push_back(ds);
                cnt += ds.nx;
            }
        };

        template <typename T>
        Results _mindist(Args const & args, T const * rec, DataSet const & plot)
        {
            if(args.refphase <= 0)
                return {};

            auto inphase = cordrift::select::run(args.refphase, rec, plot).within;
            if(inphase.size() == 0)
                return {};

            auto sigma   = cordrift::intervals::hfsigma(inphase);
            auto rng     = sigma * args.refrange;
            auto prec    = sigma / args.refprec;

            std::map<int, _MinDistInfo> items;
            for(auto const & ds: inphase)
                items[*indexing::cycleid(ds.parent)].push_back(ds);

            auto miny = _computestat<stats::bat::min>  (inphase)-rng;
            auto maxy = _computestat<stats::bat::max>  (inphase)+rng;

            auto add  = [&](auto const & info, float delta, float cnt, auto & hist)
                {
                    delta   -= miny;
                    int   sz = hist.size()-1;
                    for(auto const & ds: info.items)
                    {
                        auto r = cnt * float(ds.nx)/info.cnt;
                        for(size_t j = 0; j < ds.nx; ++j)
                        {
                            int pos = std::floor((ds.yd[j]+delta)/prec+0.5);
                            for(int k = -args.refgau; k < 1+args.refgau; ++k)
                                hist[std::max(0, std::min(sz, k+pos))] += r*std::exp(-k*k*.5);
                        }
                    }
                };

            std::vector<int> keys;
            for(auto const & el: items)
                keys.push_back(el.first);

            for(int i = 0; i < args.refrepeats; ++i)
            {
                std::valarray<float> full(0.f, int((maxy-miny)/prec));
                for(auto const & el: items)
                    add(el.second, el.second.delta, 1.f/items.size(),  full);

                int e = int(items.size());
                PRAGMA_OMP(parallel for)
                for(int i = 0; i < e; ++i)
                {
                    auto & el = items[keys[i]];
                    float maxdiff = 0.f;
                    for(auto del = -rng; del < rng+prec*.1; del += prec)
                    {
                        std::valarray<float> curr(full.size());
                        add(el, del, 1.f, curr);

                        float diff = (full*curr).sum();
                        if(diff > maxdiff)
                        {
                            maxdiff  = diff;
                            el.delta = del;
                        }
                    }
                }
            }

            Results res;
            for(auto const & el: items)
            {
                res.resize(std::max(res.size(), size_t(el.first+1)));
                res[el.first] = -el.second.delta;
            }
            return res;
        }

        template <typename T>
        Results _minmax(Args const & args, T const * rec, DataSet const & plot)
        {
            if(args.refphase <= 0)
                return {};
            auto inphase = cordrift::select::run(args.refphase, rec, plot).within;
            if(inphase.size() == 0)
                return {};

            auto e = int(inphase.size());
            std::vector<double> meds(e);

            PRAGMA_OMP(parallel for)
            for (int i = 0; i < e; ++i)
            {
                auto const & ds = inphase[i];
                meds[i] = stats::compute<stats::bat::median>(ds.nx, ds.yd);
            }

            auto fcn = [&meds, &inphase, &e](auto tag)
                       {
                           std::map<int, stats::acc_t<decltype(tag)>> values;
                           for(int i = 0; i < e; ++i)
                           {
                               assert(*inphase[i].cycle >= 0 &&
                                      "I don't deal with negative cycles");
                               values[*inphase[i].cycle](meds[i]);
                           }

                           stats::acc_t<stats::bat::median> ref;
                           stats::acc_t<stats::bat::max>    ncycles;
                           for(auto const & pair: values)
                           {
                               ref   (stats::compute(pair.second));
                               ncycles(pair.first);
                           }
                           auto r = stats::compute(ref);

                           Results res(stats::compute(ncycles)+1);
                           for(auto const & pair: values)
                               res[pair.first] = stats::compute(pair.second)-r;
                           return res;
                       };

            if(args.refphase == int(phases::pull))
                return fcn(stats::bat::max{});
            else
                return fcn(stats::bat::min{});
        }
    }

    Results compute(Args args, gen_record const * rec, DataSet const & plot)
    {
        if(args.refphase == int(phases::measure))
            return _mindist(args, rec, plot);
        else
            return _minmax(args, rec, plot);
    }

    Results compute(Args args, plot_region const * rec, DataSet const & plot)
    {
        if(args.refphase == int(phases::measure))
            return _mindist(args, rec, plot);
        else
            return _minmax(args, rec, plot);
    }

    void apply(Results const & medians, DataSet & plot)
    {
        if(medians.size() == 0)
            return;

        int e = int(plot.size());
        PRAGMA_OMP(parallel for)
        for (int i = 0; i < e; ++i)
        {
            auto & ds = plot[i];
            if(!ds.cycle)
                continue;

            auto const & med = medians[*ds.cycle];
            if(med != 0.)
                for(size_t i = 0; i < ds.nx; i++)
                    ds.yd[i] -= med;
        }
    }
}}
