#!/usr/bin/env python3
"""
Outputs hybridstat plugin's data to an xlsx file
"""
from typing import Sequence, Optional, NamedTuple

Probability = NamedTuple('Probability', [('probability', float),
                                         ('time',        float),
                                         ('uncertainty', float)])
Position    = NamedTuple('Position',    [('x', float), ('y', float), ('completed', bool)])
Events      = Sequence[Position]
Peak        = NamedTuple('Peak', [('pos',    Position),
                                  ('prob',   Optional[Probability]),
                                  ('ref',    Optional[float]),
                                  ('events', Events)])
Peaks       = Sequence[Peak]
Key         = NamedTuple('Key',      [('track',         int),
                                      ('bead',          int)])
Distance    = NamedTuple('Distance', [('val',           float),
                                      ('stretch',       float),
                                      ('bias',          float)])
Bead        = NamedTuple('Bead',     [('key',           Key),
                                      ('ncycles',       int),
                                      ('uncertainty',   float),
                                      ('silhouette',    float),
                                      ('peaks',         Peaks),
                                      ('distance',      Distance)])
Group       = NamedTuple('Group',    [('key',           Key),
                                      ('beads',         Sequence[Bead])])
Groups      = Sequence[Group]
Hairpin     = NamedTuple('Hairpin', [('name', str), ('value', str), ('beads', Sequence[int])])
Hairpins    = Sequence[Hairpin]
Oligos      = Sequence[str]
