#pragma once
#include <string>
#include <vector>
#include <set>
namespace hybridstat
{
    namespace theoretical
    {
        struct Sequence
        {
            std::string     name;
            std::string     seq;
            std::set<int>   beads;
        };
        using Sequences = std::vector<Sequence>;
        using Oligos    = std::vector<std::string>;

        struct Peaks: public std::vector<std::pair<float,float>>
        {
            float length = 0;
        };

        struct Result
        {
            std::string     name;
            Peaks           peaks;
            std::set<int>   beads;
        };

        using Results = std::vector<Result>;

        Sequences   read (std::string);
        Sequences   read (std::istream &);
        Results     find (Sequences const &, Oligos const &);
    }
}
