#include <boost/container/stable_vector.hpp>
#include "../filters/accumulators.h"
#include "hybridstat.hh"
#include "hybridstat_core.h"
#include "menu_template.hh"
#include "../stat/stat.h"
#include "../trackBead/indexing.h"
#include "ompbase.h"
#include "plot_ds.h"
#include <cassert>
#include <vector>
#include <cfloat>
#include <fstream>
#include <iostream>
#include <algorithm>
using namespace stats;
int align_hybridization_pattern_peak(d_s *pattern_ds, int peak_index, float peak_min_threshold)
{
    float max_pos = 0;
    float x_val_of_y_max = 0;
    float max_y_val = 0;
    int find_max_ret = 0;
    assert(pattern_ds);
    int k = 1;

    for (k = 1; k < pattern_ds->nx - 1; k++)
    {
        if ((pattern_ds->yd[k] > pattern_ds->yd[k - 1]) && (pattern_ds->yd[k] >= pattern_ds->yd[k + 1])
                && (pattern_ds->yd[k] > peak_min_threshold))
        {
            //win_printf("k = %d y %g",k,dsi->yd[k]);
            find_max_ret = find_max_around(pattern_ds->yd, pattern_ds->nx, k, &max_pos, &max_y_val, NULL);

            if (find_max_ret == 0)
            {
                if (peak_index == 0)
                {
                    break;
                }

                peak_index--;
            }
        }
    }

    if (peak_index == 0 && k < pattern_ds->nx - 1)
    {
        int pk = (int)max_pos;
        int pk1 = pk + 1;
        float p = (float)pk1 - max_pos;
        pk = (pk < 0) ? 0 : pk;
        pk = (pk < pattern_ds->nx) ? pk : pattern_ds->nx - 1;
        pk1 = (pk1 < 0) ? 0 : pk1;
        pk1 = (pk1 < pattern_ds->nx) ? pk1 : pattern_ds->nx - 1;
        x_val_of_y_max = p * pattern_ds->xd[pk] + (1 - p) * pattern_ds->xd[pk1];

        for (int i = 0; i < pattern_ds->nx; i++)
            pattern_ds->xd[i] -= x_val_of_y_max;

        return 0;
    }
    else
        return -1;
}

namespace hybridstat { namespace hybridization {
    namespace
    {
        auto _xvin_events( Args     const & cf
                         , pltreg         * pr
                         , O_p      const * cycles
                         , double           sig
                         , DataSet  const & events)
        {
            one_plot * phases = nullptr;
            for(auto const & elem: events)
            {
                int len = int(elem.nx);
                if(phases == nullptr)
                    phases = create_and_attach_one_plot(pr, len, len, 0);
                else
                    create_and_attach_one_ds(phases, len, len, 0);

                auto ds = phases->dat[phases->n_dat-1];
                inherit_from_ds_to_ds(ds, elem.parent);
            }
            std::vector<data_set*> created(phases->dat, phases->dat+phases->n_dat);

            int e = int(created.size());

            PRAGMA_OMP(parallel for)
            for(int i = 0; i < e; ++i)
            {
                auto       & ds   = *created[i];
                auto const & elem = events  [i];
                std::copy(elem.xd, elem.xd+elem.nx, ds.xd);
                std::copy(elem.yd, elem.yd+elem.nx, ds.yd);

                set_ds_line_color(&ds, get_ds_line_color(elem.parent));
                set_ds_source(&ds, get_ds_source(elem.parent));
            };

            auto  bid = *indexing::beadid(cycles), tid = *indexing::trackid(cycles);
            set_plot_file(phases, CYCLE_PHASE_PLOT_FILENAME, bid, tid, cf.lastphase);
            uns_op_2_op_by_type(cycles, IS_X_UNIT_SET, phases, IS_X_UNIT_SET);
            set_plot_title(phases, "Bead %d Z(t) - track %d (\\sigma: %.05f)",
                           bid, tid, sig);
            menu::refresh(pr, phases);
            return phases;
        }

        template <typename T>
        auto _compute_histo(Args const & cf, pltreg const * pr, DataSet const & in, T dsvect)
        {
            std::vector<float> mem;
            float pullpos[3];

            assert(cf.sigma > 0 && "Smearing must have value above 0");
            auto fill = [](histogram::Results const & mine, auto & his)
                {
                    for(size_t i = 0,  e = mine.data.size(); i < e; ++i)
                    {
                        his.xd[i] = mine.ymin+i*mine.bin;
                        his.yd[i] = mine.data[i];
                    }
                };

            auto create_time_gaussian = [&]()
                {
                    auto   gau    = histogram::run(histogram::timegaussian(cf), in);
                    auto & htg    = dsvect(gau.data.size());
                    fill(gau, htg);

                    auto zccf     = zeroclip::args(cf);
                    auto zcres    = zeroclip::compute(zccf, htg.nx, htg.xd, htg.yd);
                    if(cf.clip == 1 && gau.data.size() > 1)
                        zeroclip::apply(zccf, zcres, htg.nx, htg.yd);
                    return zcres;
                };

            auto create_normalized_gaussian = [&]()
                {
                    auto   gau = histogram::run(histogram::gaussian(cf), in);
                    auto & hga = dsvect(gau.data.size());
                    fill(gau, hga);
                    return Data(hga);
                };

            auto create_normalized_gaussian_without_pull_phase = [&]()
                {
                    // make sure events from phase 3 don't pollute events elsewhere
                    int  ph   = int(phases::pull);
                    auto vect = cordrift::select::run(ph, pr, in);
                    auto gau  = histogram::run(histogram::gaussian(cf), vect.downstream);
                    mem.resize(gau.data.size()*2);

                    acc_t<bat::approx_median> med;
                    for(auto const & ds: vect.within)
                        med(stats::compute<bat::approx_median>(ds.nx, ds.yd));
                    pullpos[0] = stats::compute(med);
                    pullpos[1] = vect.within.size();

                    Data hga;
                    hga.nx = mem.size()/2;
                    hga.xd = mem.data();
                    hga.yd = mem.data()+hga.nx;
                    fill(gau, hga);

                    return hga;
                };

            auto compute_peaks_xpos = [&](auto const & lmax)
                {
                    std::valarray<float> xvals(lmax.size());
                    for(size_t i = 0, e = lmax.size(); i < e; ++i)
                        xvals[i] = lmax[i].first;
                    return xvals;
                };

            auto compute_peaks_ypos = [&](auto const & lmax, auto const & xvals)
                {
                    std::valarray<int>   cnt  (lmax.size());
                    auto const rho = histogram::square(cf.sigma).sz*cf.sigma;
                    for(auto const & ds: in)
                    {
                        auto xp = stats::compute<bat::approx_median>(ds.nx, ds.yd);
                        for(size_t i = 0, e = cnt.size(); i < e; ++i)
                            cnt[i] +=     (xvals[i] <= xp+rho)
                                      &&  (xp-rho <= xvals[i]) ? 1 : 0;
                    };
                    return cnt;
                };

            auto compute_peaks = [&](auto const & xvals, auto const & cnt)
                {
                    auto sz = mem.size() == 0 ? 0 : 1;
                    for(auto x: cnt)
                        if(x >= cf.threshold) ++sz;

                    auto & hpeaks = dsvect(sz);
                    for(size_t ix = 0, iy = 0, j = 0, e = cnt.size(); j < e; ++j)
                        if(cnt[j] >= cf.threshold)
                        {
                            hpeaks.xd[ix++] = xvals[j];
                            hpeaks.yd[iy++] = cnt  [j];
                        }

                    if(mem.size() > 0)
                    {
                        hpeaks.xd[sz-1] = pullpos[0];
                        hpeaks.yd[sz-1] = pullpos[1];
                    }
                };

            auto zcres = create_time_gaussian();
            auto hga   = create_normalized_gaussian();
            if(hga.xd == nullptr)
                return;

            if(cf.firstphase == int(phases::pull))
                hga    = create_normalized_gaussian_without_pull_phase();

            auto sig  = 3.f*cf.sigma/histogram::gaussian(cf).bin;
            auto lmax = maxcoordinates::run(sig, zcres.zeroPosition()-1e-5,
                                            hga.nx, hga.yd, hga.xd);

            auto xvals = compute_peaks_xpos(lmax);
            auto cnt   = compute_peaks_ypos(lmax, xvals);
            compute_peaks(xvals, cnt);
        }

        void _xvin_histo(Args const & cf, O_p const* cycles, O_p const* phases, O_p * histo)
        {
            if(histo->n_dat < 3)
                return;

            auto hds = histo->dat[histo->n_dat-1];
            set_ds_dot_line(hds);
            auto set = [](auto ds, auto fcn, auto x)
                       { char tmp[512]; strcpy(tmp, x); (*fcn)(ds, tmp); };
            set(hds, set_plot_symb, "\\pt5\\oc ");
            inherit_from_ds_to_ds(hds, histo->dat[1]);
            set(hds, set_ds_source, "Detected Peaks (x = position, y = number of events)");

            auto fmax    = [](d_s const & ds)
                         { return *std::max_element(ds.yd, ds.yd+ds.nx); };
            auto max2    = fmax(*hds);
            auto rescale = [&fmax,max2](d_s const & ds)
                         {
                             auto max0 = fmax(ds);
                             if(max0 != 0.)
                                 for(auto i = 0; i < ds.nx; ++i)
                                     ds.yd[i] *= max2/max0;
                         };
            rescale(histo->dat[0][0]);
            set(histo->dat[0], set_ds_source, "Average event duration");
            rescale(histo->dat[1][0]);
            set(histo->dat[1], set_ds_source, "Average number of events");

            auto tmp = cf.lastphase;
            auto bid = *indexing::beadid(cycles), tid = *indexing::trackid(cycles);
            set_plot_title(histo, "\\stack{{Hybridization pattern of}{%s}}\n", phases->title);
            set_plot_file (histo, HYB_PATTERN_PLOT_FILENAME, bid, tid, &tmp);
            histo->need_to_refresh = 1;
        }
    }

    Results run( Args                cf
               , pltreg            * pr
               , O_p         const * cycles
               , DataSet     const & events)
    {
        if(events.size() == 0)
            return {};
        auto sig  = stats::parallel<bat::variance>
                                   (events,
                                    [](auto & cur, Data const & ds)
                                    {
                                        for(size_t i = 0; i < ds.nx; ++i)
                                            cur(ds.yd[i]);
                                    }
                                   );
        sig = std::sqrt(sig);

        O_p * xvevts = _xvin_events(cf, pr, cycles, sig, events);
        O_p * xvhist = nullptr;
        auto newds   = [&](size_t len) -> decltype(auto)
                    {
                        if(xvhist == nullptr)
                            xvhist = create_and_attach_one_plot(pr, len, len, 0);
                        else
                            create_and_attach_one_ds(xvhist, len, len, 0);
                        return *xvhist->dat[xvhist->n_dat-1];
                    };

        _compute_histo(cf, pr, events, newds);
        _xvin_histo   (cf, cycles, xvevts, xvhist);
        if(xvhist->n_dat < 3)
            return {sig, Data()};

        return {sig, Data(xvhist->dat[xvhist->n_dat-1])};
    }
}}

namespace hybridstat { namespace probability {
    Results run(all::SmallArgs const & cf,
                gen_record     const * gr,
                DataSet        const & vect,
                Data           const & peaks)
    {
        PeakProbabilities pk(cf.nmin, cf.sigma);
        for(size_t i = 0; i < peaks.nx; ++i)
            pk.peak(peaks.xd[i], peaks.yd[i]);

        indexing::PhaseRange ranges(gr, cf.lastphase);
        for(auto ds: vect)
        {
            auto rng = ranges[ds.cycle];
            if(!rng)
                continue;
            pk.event(ds.yd, 0, ds.nx, rng->second);
        }
        return pk.results(cf.rate);
    }
}}
