/*
 *    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
 */
#ifndef _FINDSEQ_C_
#define _FINDSEQ_C_

# include "allegro.h"
# include "xvin.h"
# include "float.h"
#include <ctype.h>

/* If you include other regular header do it here*/


/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "findSeq.h"

/* If you include other regular header do it here*/
# include "../nrutil/nrutil.h"
# include "../Tm_DNA/Tm_DNA.h"


extern char fullfile1[16384];

// This is the data structure where DNA sequences are stored in memory
// sequence is stored in a paginated structure


// Several sequences may be load simultaneously (from the same file or from several files)
// One of them is the current one

extern int n_asqd, m_asqd, c_asqd;
extern sqd *lsqd, * *asqd;
char oligo[512] = {"\0"};


// Hybridization is stored in the following structure

typedef struct _hybrid_oligo
{
    int seqid;               // this is the ID of the oligo
    int pos;                 // this is his position
} hyo;


typedef struct _dyn_array
{
    struct _hybrid_oligo *hyo;
    int n_hy;
    int m_hy;
    int valid;
} dya;


dya ** *oligoReg = NULL;
int noligoReg = -1;
int *oligo_nhib_invalid = NULL;

// This is the data structure of a fingerprint

typedef struct _fingerprint
{
    unsigned int olisel[32];  // space to contain the oligos ID
    int n_sel;                // this is the number of oligos in the set
    int order[32];            // this array specify the index of oligos ordered according to their position
    float mean_pos[32];       // for each oligo this array contains the averaged position over all sequences
    unsigned int
    *seq;        // space to contain the sequence fingerprint (the 0 and 1 of the number represent hybridization)
    int n_seq;                // this is the number of sequences
    int **seqhyb;             // space to contain the hybridization positions +1 space indicating non unique code
    double E1, E2, E3;        // this are energies used in MC
} figpt;


// this this the data structure for multiplexing hybridization

typedef struct _oligo_multiplex
{
    int o_size;                     // the size of the oligo AGCT -> 4
    int set_nb;                     // the number of oligos sets  = the nb of bits for setID
    int n_oligo;                    // the total number of oligos
    int m_oligo;                    // the size of memary allocated for oligos
    int *oligo_nb;                  // the oligo nb in [0,n_oligo[, at beginning 0,1, ... ,n_oligo-1
    int *ioligo;                    // an array containing the oligos ID
    int *ioligo_trial;              // an array containing the oligos ID for a possible new config
    unsigned long long *setID;      // an array containing the oligo setID (size n_setID) these number have set_nb of bits
    int n_setID;                    // the number of oligo identifier used n_setID <= n_oligo
    int *set_n;                     // an array of set_nb elements each containing the number of oligos in each set
    int ** *set;                    // a double array set[set_nb][j] of pointers aiming to ioligo element
    int *overlap;                   // array storing overlap state of sets
    int *partial_hybridized;        // array storing partial hybridizin state of sets
    double Es;                      // Energy of set
    int *overlap_trial;             // array storing overlap state of trial sets
    int *partial_hybridized_trial;  // array storing partial hybridizin state of trial sets
    double Es_trial;                // Energy of trial set
} omul;


omul *oli_mul_set = NULL;

int seql_min = 1300, seql_max = 1800;

figpt *bfgpt = NULL, *tfgpt = NULL;

# define MAXLINE 256
extern char lineBuf[MAXLINE];
int idum = 4587;
int itern = 0, flip = 0, flips = 0;
int lastolinb = 0, prevoliid = 0, newoliid = 0;
float ture = 4;

//int findOligoInSeq(sqd *msqd, char *oligo, int start, int noNs);


int convert_oligoID_to_seq(char *message, int npid, int size)
{
    int i, j;

    message[0] = 0;

    for (i = 0; i < size; i++)
    {
        j = (npid & 0x03);

        if (j == 0) message[size - i - 1] = 'A';
        else if (j == 1) message[size - i - 1] = 'C';
        else if (j == 2) message[size - i - 1] = 'G';
        else if (j == 3) message[size - i - 1] = 'T';

        npid >>= 2;
    }

    message[i] = 0;
    return 0;
}

int convert_oligo_seq_to_ID(char *seq, int size)
{
    int i, j;
    int npid;

    for (i = 0, npid = 0; i < size; i++)
    {
        if ((seq[i] & 0x5F) == 'A') j = 0;
        else if ((seq[i] & 0x5F) == 'C') j = 1;
        else if ((seq[i] & 0x5F) == 'G') j = 2;
        else if ((seq[i] & 0x5F) == 'T') j = 3;

        npid <<= 2;
        npid |= j;
    }

    return npid;
}

int grab_oligos_from_file(char ***loligo)
{
    int  size;
    char seqfilename[2048], oligol[64];
    int n_oligo = 0, m_oligo = 0, read, ic;
    FILE *fp;

    if (loligo == NULL) return -1;

    seqfilename[0] = 0;

    if (do_select_file(seqfilename, sizeof(seqfilename), "TXT-FILE", "*.txt", "File to load oligo\0", 1))
        return win_printf_OK("Cannot select input file");

    fp = fopen(seqfilename, "r");

    if (fp == NULL)
    {
        win_printf("Cannot open file:\n%s", backslash_to_slash(seqfilename));
        return -1;
    }

    (*loligo) = (char **)calloc(m_oligo = 256, sizeof(char *));

    if ((*loligo) == NULL)
    {
        win_printf("Cannot allocate loligo");
        return -1;
    }

    for (n_oligo = 0, read = 1, size = -1; read == 1;)
    {
        read = fscanf(fp, "%s", oligol);

        if (read == 1)
        {
            if (n_oligo >= m_oligo)
            {
                m_oligo += 256;
                (*loligo) = (char **)realloc(loligo, m_oligo * sizeof(char *));

                if ((*loligo) == NULL)
                {
                    win_printf("Cannot allocate loligo");
                    return -1;
                }
            }

            ic = strlen(oligol);

            if (ic > 0)
            {
                if (size < 0) size = ic;

                //if (size != ic) { win_printf_OK("Your oligos differ in size"); return -1;}
                (*loligo)[n_oligo] = strdup(oligol);
                n_oligo++;
            }
        }
    }

    fclose(fp);
    return n_oligo;
}

// construct an array containing the number of oligo being present in a sequence
int countOligo(sqd *msqd, int oligoSize, unsigned int **oligoId, int start, int end)
{
    int i, j, k;
    char b;
    int st, nd, noli, keep, id, ip, ic, val;

    if ((msqd == NULL) || (msqd->seq == NULL) || (msqd->totalSize <= 0)) return -1;

    st = (start < 0) ? 0 : start;
    nd = (end > msqd->totalSize || end <= 0) ? msqd->totalSize : end;

    if (oligoSize > 10) return -win_printf_OK("There is more than 1 million of oligos!");

    for (noli = 4, i = 1; i < oligoSize; i++, noli *= 4);

    *oligoId = (unsigned int *)calloc(noli, sizeof(unsigned int));

    if (*oligoId == NULL) return -win_printf_OK("cannot allocate counting space!");

    for (i = st; i < nd - oligoSize; i++)
    {
        for (j = 0, keep = 1, id = 0; j < oligoSize; j++)
        {
            k = i + j;
            ip = k >> msqd->ln2PageSize;
            ic = k & msqd->pageMask;
            b = msqd->seq[ip][ic] & 0x5F;

            if (b == 'N') keep = 0;

            val = 0;

            if (b == 'A')       val = 0;
            else if (b == 'C')  val = 1;
            else if (b == 'G')  val = 2;
            else if (b == 'T')  val = 3;

            id <<= 2;
            id |= val;
        }

        if (keep == 0) continue;

        (*oligoId)[id]++;
    }

    return noli;
}

// construct an array containing the number of oligo being present in a sequence
int registerOligo(sqd *msqd, int seqid, int oligoSize, dya ****loligoReg, int start, int end)
{
    int i, j, k;
    char b;
    int st, nd, noli, keep, id, ip, ic, val, ie, exist;
    dya *olre;

    if ((msqd == NULL) || (msqd->seq == NULL) || (msqd->totalSize <= 0)) return -1;

    st = (start < 0) ? 0 : start;
    nd = (end > msqd->totalSize || end <= 0) ? msqd->totalSize : end;

    if (oligoSize > 10) return -win_printf_OK("There is more than 1 million of oligos!");

    for (noli = 4, i = 1; i < oligoSize; i++, noli *= 4);

    if (*loligoReg == NULL)
    {
        *loligoReg = (dya ** *)calloc(noli, sizeof(dya **));
        oligo_nhib_invalid = (int *)calloc(noli, sizeof(int));
    }

    if (*loligoReg == NULL || oligo_nhib_invalid == NULL)
        return -win_printf_OK("cannot allocate counting space!");

    noligoReg = noli;

    for (i = st; i < nd - oligoSize; i++)
    {
        for (j = 0, keep = 1, id = 0; j < oligoSize; j++)
        {
            k = i + j;
            ip = k >> msqd->ln2PageSize;
            ic = k & msqd->pageMask;
            b = msqd->seq[ip][ic] & 0x5F;

            if (b == 'N') keep = 0;

            val = 0;

            if (b == 'A')       val = 0;
            else if (b == 'C')  val = 1;
            else if (b == 'G')  val = 2;
            else if (b == 'T')  val = 3;

            id <<= 2;
            id |= val;
        }

        if (keep == 0) continue;

        if ((*loligoReg)[id] == NULL)
        {
            (*loligoReg)[id] = (dya **)calloc(n_asqd, sizeof(dya *));

            if ((*loligoReg)[id] == NULL)
                return -win_printf_OK("cannot allocate counting spot 1!");
        }

        if ((*loligoReg)[id][seqid] == NULL)
            (*loligoReg)[id][seqid] = (dya *)calloc(1, sizeof(dya));

        if ((*loligoReg)[id][seqid] == NULL)
            return -win_printf_OK("cannot allocate counting spot 1!");

        olre = (*loligoReg)[id][seqid];

        if (olre->hyo == NULL)
        {
            olre->hyo = (hyo *)calloc(1, sizeof(hyo));

            if (olre->hyo == NULL)    return -win_printf_OK("cannot allocate counting spot 2!");

            olre->m_hy = 1;
            olre->n_hy = 0;
        }

        for (ie = 0, exist = 0; (exist == 0) && (ie < olre->n_hy); ie++)
            if (olre->hyo[ie].pos == i)         exist = 1;

        if (exist == 0)
        {
            if (olre->n_hy + 1 >= olre->m_hy)
            {
                olre->hyo = (hyo *)realloc(olre->hyo, (olre->m_hy + 1) * sizeof(hyo));

                if (olre->hyo == NULL)  return -win_printf_OK("cannot allocate counting spot 3!");

                olre->m_hy += 1;
            }

            olre->hyo[olre->n_hy].seqid = seqid;
            olre->hyo[olre->n_hy].pos = i;
            olre->n_hy++;
        }
    }

    return noli;
}



char *findSeq_idle_point_add_display(struct one_plot *op, DIALOG *d, int nearest_ds, int nearest_point)
{
    static char message[1024];
    d_s *ds;
    int ok = 0, oligoSize = 0, npid;

    message[0] = 0;

    (void)d;
    if (op == NULL) return message;

    if (nearest_ds >= op->n_dat) return message;

    ds = op->dat[nearest_ds];
    //"Oligosize: %d", noligo);
    ok = sscanf(ds->source, "Oligo repeat: %d", &oligoSize);

    if (ok < 1)    ok = sscanf(ds->source, "Oligosize: %d", &oligoSize);

    if (ok < 1)    ok = sscanf(ds->source, "Common Oligos with size: %d", &oligoSize);

    if (ok < 1)    ok = sscanf(ds->source, "Specific Oligos with size: %d", &oligoSize);

    if (ok < 1) return message;

    if (oligoSize > 15) return message;

    npid = (int)ds->xd[nearest_point];

    convert_oligoID_to_seq(message, npid, oligoSize);
    return message;
}
// int registerOligo(sqd *msqd, int seqid, int oligoSize, dya ***oligoReg, int start, int end)
// en cours

int countOligoSpecificAmongSeqInPr(int oligoSize, pltreg *pr, int start, int end, int min, int max, char *oligocenter)
{
    int i, j, k;
    int noli, nseq, common, off;
    O_p *opn = NULL, *opl = NULL, *ops = NULL;
    d_s *ds = NULL, *dsl = NULL, *dss = NULL, **dst = NULL;
    int *seqhy = NULL;
    sqd *msqd;

    if (oligoSize > 10) return -win_printf_OK("There is more than 1 million of oligos!");

    for (noli = 4, i = 1; i < oligoSize; i++, noli *= 4);

    if (n_asqd < 2) return win_printf_OK("Not enough sequences !");

    seqhy = (int *)calloc(n_asqd, sizeof(int));

    if (seqhy == NULL) return win_printf_OK("cannot allocate sequence !");

    dst = (d_s **)calloc(n_asqd, sizeof(d_s *));

    if (dst == NULL) return win_printf_OK("cannot allocate sequence !");

    for (i = 0, nseq = 0; i < n_asqd; i++)
    {
        seqhy[i] = -1;
        msqd = asqd[i];

        if ((msqd != NULL) && (msqd->seq != NULL) && (msqd->totalSize > min) && (msqd->totalSize < max))
        {
            //countOligo(msqd, oligoSize, oligoIds + i, start, end);
            registerOligo(msqd, i, oligoSize, &oligoReg, start, end);
            seqhy[i] = 0; // we mark good sequences
            nseq++;
        }
    }

    if ((opn = create_and_attach_one_plot(pr, 16, 16, 0)) == NULL)
        return win_printf_OK("cannot create plot at %s:%d", __FILE__, __LINE__);

    ds = opn->dat[0];
    ds->nx = ds->ny = 0;
    opn->op_idle_point_add_display = findSeq_idle_point_add_display;

    if ((opl = create_and_attach_one_plot(pr, 16, 16, 0)) == NULL)
        return win_printf_OK("cannot create plot at %s:%d", __FILE__, __LINE__);

    dsl = opl->dat[0];
    dsl->nx = dsl->ny = 0;
    opl->op_idle_point_add_display = findSeq_idle_point_add_display;

    for (j = 0, dss = NULL, ops = NULL; j < n_asqd; j++)
    {
        if (seqhy[j] < 0) continue;

        if (ops == NULL)
        {
            if ((ops = create_and_attach_one_plot(pr, asqd[j]->totalSize, asqd[j]->totalSize, 0)) == NULL)
                return win_printf_OK("cannot create plot at %s:%d", __FILE__, __LINE__);

            dss = ops->dat[0];
        }

        if (dss == NULL)
        {

            if ((dss = create_and_attach_one_ds(ops, asqd[j]->totalSize, asqd[j]->totalSize, 0)) == NULL)
                return win_printf_OK("cannot create plot at %s:%d", __FILE__, __LINE__);
        }

        dst[j] = dss;
        set_ds_source(dss, "Common Oligos cover of seq %s with size: %d", asqd[j]->filename, oligoSize);

        for (k = 0; k < dss->nx; k++) dss->xd[k] = k;

        dss = NULL;
    }

    for (i = 0; i < noli; i++)
    {
        if (oligoReg[i] != NULL)
        {
            // this oligo was found to hybridize
            for (j = 0; j < n_asqd; j++) // we reset counter for all sequences
            {
                k = (oligoReg[i][j] == NULL) ? 0 : oligoReg[i][j]->n_hy;
                seqhy[j] = (seqhy[j] >= 0) ? k : seqhy[j];
            }

            for (j = 0, common = 0, k = -1; j < n_asqd; j++)
                if (seqhy[j] > 0)
                {
                    common++;    // we count how sequences where hybridized
                    k = j;
                }

            add_new_point_to_ds(ds, i, common);

            if (common == 1 && k >= 0) // we plot oligos specific to one sequence
                add_new_point_to_ds(dsl, i, k);

            for (j = 0; common > 0 && j < n_asqd; j++)
            {
                for (k = 0; (oligoReg[i][j] != NULL) && (k < oligoReg[i][j]->n_hy); k++)
                    if (oligoReg[i][j]->hyo[k].pos >= 0 && oligoReg[i][j]->hyo[k].pos < dst[j]->nx)
                        dst[j]->yd[oligoReg[i][j]->hyo[k].pos] = common;
            }
        }
    }

    for (j = 0; j < n_asqd; j++)
    {
        // this loop feels the oligosize
        if (seqhy[j] < 0) continue;

        for (k = 0; k < dst[j]->nx; k++) dst[j]->xd[k] = 0;

        for (k = dst[j]->nx - oligoSize; k >= 0; k--)
        {
            if (dst[j]->yd[k] > 0)
            {
                for (i = k; i < k + oligoSize && i < dst[j]->nx; i++)
                    dst[j]->xd[i] = (dst[j]->yd[k] > dst[j]->xd[i]) ? dst[j]->yd[k] : dst[j]->xd[i];
            }
        }

        for (k = 0; k < dst[j]->nx; k++)
        {
            dst[j]->yd[k] =  dst[j]->xd[k];
            dst[j]->xd[k] = k;
        }

        if (strlen(oligocenter) > 2)
        {
            off = findOligoInSeq(asqd[j], oligocenter, 0, 1);

            for (k = 0; k < dst[j]->nx; k++)
                dst[j]->xd[k] -= off;
        }
    }

    set_ds_source(ds, "Common Oligos with size: %d", oligoSize);
    set_plot_title(opn, "Common Oligos with size # %d", oligoSize);
    set_plot_x_title(opn, "Oligo index");
    set_plot_y_title(opn, "Number of common seq.");

    set_ds_source(dsl, "Specific Oligos with size: %d", oligoSize);
    set_plot_title(opl, "Specific Oligos with size: %d", oligoSize);
    set_plot_x_title(opl, "Oligo index");
    set_plot_y_title(opl, "oligo valid");

    set_plot_title(ops, "Common Oligos covering with size # %d", oligoSize);
    set_plot_x_title(ops, "Position along sequence");
    set_plot_y_title(ops, "Cover.");
    free(dst);
    free(seqhy);
    return noli;
}

/*
   int countOligoSpecificAmongSeqInPr(int oligoSize, pltreg *pr, int start, int end)
   {
   int i, j, k;
   int noli, nseq, common;
   O_p *opn = NULL, *opl = NULL;
   d_s *ds = NULL, *dsl = NULL;
   unsigned int **oligoIds = NULL;
   sqd *msqd;

   if (oligoSize > 10) return -win_printf_OK("There is more than 1 million of oligos!");
   for (noli = 4, i = 1; i < oligoSize; i++, noli *= 4);
   if (n_asqd < 2) return win_printf_OK("Not enough sequences !");

   oligoIds = (unsigned int **)calloc(n_asqd,sizeof(unsigned int *));
   if (oligoIds == NULL) return win_printf_OK("cannot allocate sequence !");
   for (i = 0, nseq = 0; i < n_asqd; i++)
   {
   oligoIds[i] = NULL;
   msqd = asqd[i];
   if ((msqd != NULL) && (msqd->seq != NULL) && (msqd->totalSize > 0))
   {
   countOligo(msqd, oligoSize, oligoIds + i, start, end);
   nseq++;
   }
   }

   if ((opn = create_and_attach_one_plot(pr, noli, noli, 0)) == NULL)
   return win_printf_OK("cannot create plot at %s:%s", __FILE__, __LINE__);
   ds = opn->dat[0];
   opn->op_idle_point_add_display = findSeq_idle_point_add_display;
   if ((opl = create_and_attach_one_plot(pr, noli, noli, 0)) == NULL)
   return win_printf_OK("cannot create plot at %s:%s", __FILE__, __LINE__);
   dsl = opl->dat[0];
   opl->op_idle_point_add_display = findSeq_idle_point_add_display;

   for (i = 0; i < noli; i++)
   {
   for (j = 0, common = 0; j < n_asqd; j++)
   {
   msqd = asqd[j];
   if ((msqd != NULL) && (msqd->seq != NULL) && (msqd->totalSize > 0))
   {
   common += (oligoIds[j][i]) ? 1 : 0;
   k = (oligoIds[j][i]) ? j : -1;
   }
   }
   ds->yd[i] = common;
   if (common == 1) dsl->yd[i] = k;
   dsl->xd[i] = i;
   ds->xd[i] = i;
   }

   set_ds_source(ds,"Common Oligos with size: %d",oligoSize);
   set_plot_title(opn, "Common Oligos with size # %d",oligoSize);
   set_plot_x_title(opn, "Oligo index");
   set_plot_y_title(opn, "Number of common seq.");

   set_ds_source(dsl,"Specific Oligos with size: %d",oligoSize);
   set_plot_title(opl, "Specific Oligos with size: %d",oligoSize);
   set_plot_x_title(opl, "Oligo index");
   set_plot_y_title(opl, "oligo valid");
// refisplay the entire plot

for (i = 0; i < n_asqd; i++)
if (oligoIds[i] != NULL)  free(oligoIds[i]);
free(oligoIds);
return noli;
}
*/



int countOligoInPr(sqd *msqd, int oligoSize, pltreg *pr, int start, int end, int nhyb, FILE *fp)
{
    int i, j, k;
    char b;
    float dx;
    int st, nd, noli, keep, id, ip, ic, val, last_found_N = 0, iseq;
    O_p *opn = NULL, *opl = NULL;
    d_s *ds = NULL, *dsl = NULL;
    char oligol[64];

    if ((msqd == NULL) || (msqd->seq == NULL) || (msqd->totalSize <= 0)) return -1;

    st = (start < 0) ? 0 : start;
    nd = (end > msqd->totalSize || end <= 0) ? msqd->totalSize : end;

    if (oligoSize > 10) return -win_printf_OK("There is more than 1 million of oligos!");

    for (noli = 4, i = 1; i < oligoSize; i++, noli *= 4);


    if ((opn = create_and_attach_one_plot(pr, noli, noli, 0)) == NULL)
        return win_printf_OK("cannot create plot at %s:%d", __FILE__, __LINE__);

    ds = opn->dat[0];
    opn->op_idle_point_add_display = findSeq_idle_point_add_display;

    if ((opl = create_and_attach_one_plot(pr, noli, noli, 0)) == NULL)
        return win_printf_OK("cannot create plot at %s:%d", __FILE__, __LINE__);

    dsl = opl->dat[0];
    opl->op_idle_point_add_display = findSeq_idle_point_add_display;

    //for (i = 0; i < dsl->nx; i++) dsl->xd[i] = -1;
    for (i = st, last_found_N = 0; i < nd - oligoSize; i++)
    {
        for (j = 0, keep = 1, id = 0; j < oligoSize; j++)
        {
            k = i + j;
            ip = k >> msqd->ln2PageSize;
            ic = k & msqd->pageMask;
            b = msqd->seq[ip][ic] & 0x5F;

            if (b == 'N') keep = 0;

            val = 0;

            if (b == 'A')       val = 0;
            else if (b == 'C')  val = 1;
            else if (b == 'G')  val = 2;
            else if (b == 'T')  val = 3;

            id <<= 2;
            id |= val;
        }

        if (keep == 0)
        {
            last_found_N = i;
            continue;
        }

        if (last_found_N < (int)dsl->xd[id])
        {
            dx = i - dsl->xd[id];
            dsl->yd[id] += dx;
        }

        ds->yd[id]++;
        dsl->xd[id] = i;
    }

    for (i = 0; i < dsl->nx; i++)
    {
        dsl->yd[i] = (ds->yd[i] > 0) ? dsl->yd[i] / ds->yd[i] : dsl->yd[i];
        dsl->xd[i] = ds->xd[i] = i;

        if (fp != NULL && ds->yd[i] >= nhyb)
        {
            for (k = 0, iseq = i; k < oligoSize; k++)
            {
                j = (iseq & 0x03);

                if (j == 0) oligol[oligoSize - k - 1] = 'A';
                else if (j == 1) oligol[oligoSize - k - 1] = 'C';
                else if (j == 2) oligol[oligoSize - k - 1] = 'G';
                else if (j == 3) oligol[oligoSize - k - 1] = 'T';

                iseq >>= 2;
            }

            oligol[k] = 0;
            fprintf(fp, "Oligo %s hybridize %d times\n", oligol, (int)ds->yd[i]);
        }
    }

    set_ds_source(ds, "Oligosize: %d", oligoSize);
    set_plot_title(opn, "Oligo # %d", oligoSize);
    set_plot_x_title(opn, "Oligo number");
    set_plot_y_title(opn, "Number of oligos");

    set_ds_source(dsl, "Oligo repeat: %d", oligoSize);
    set_plot_title(opl, "Oligo # %d", oligoSize);
    set_plot_x_title(opl, "Oligo number");
    set_plot_y_title(opl, "Mean distance between oligos");
    /* refisplay the entire plot */

    return noli;
}

int findOligoInSeq_norepeat(sqd *msqd, const char *oligol, int start, int noNs)
{
    int i, j, k;
    int st, loli, match, ip, ic;
    //  static int n=0;

    if ((msqd == NULL) || (msqd->seq == NULL) || (msqd->totalSize <= 0) || (oligol == NULL)) return -1;

    //q=win_scanf("Do you want the program to match oligo with N...N in the sequence ? \n Yes %R  No %r",&n);
    //if (q==WIN_CANCEL) return OFF;

    st = (start < 0) ? 0 : start;
    loli = strlen(oligol);

    for (i = st; i < msqd->totalSize - loli; i++)
    {
        for (j = 0, match = 1; j < loli && match > 0; j++)
        {
            k = i + j;
            ip = k >> msqd->ln2PageSize;
            ic = k & msqd->pageMask;

            if ((msqd->seq[ip][ic] & 0x5F) == 'N')
                match = (noNs == 0) ? match : 0;
            else if ((oligol[j] & 0x5F) == 'R')
                match = (((msqd->seq[ip][ic] & 0x5F) == 'A') || ((msqd->seq[ip][ic] & 0x5F) == 'G')) ? match : 0;
            else if ((oligol[j] & 0x5F) == 'Y')
                match = (((msqd->seq[ip][ic] & 0x5F) == 'C') || ((msqd->seq[ip][ic] & 0x5F) == 'T')
                         || ((msqd->seq[ip][ic] & 0x5F) == 'U')) ? match : 0;
            else if ((oligol[j] & 0x5F) == 'S')
                match = (((msqd->seq[ip][ic] & 0x5F) == 'G') || ((msqd->seq[ip][ic] & 0x5F) == 'C')) ? match : 0;
            else if ((oligol[j] & 0x5F) == 'W')
                match = (((msqd->seq[ip][ic] & 0x5F) == 'A') || ((msqd->seq[ip][ic] & 0x5F) == 'T')) ? match : 0;
            else if ((oligol[j] & 0x5F) == 'M')
                match = (((msqd->seq[ip][ic] & 0x5F) == 'A') || ((msqd->seq[ip][ic] & 0x5F) == 'C')) ? match : 0;
            else if ((oligol[j] & 0x5F) == 'K')
                match = (((msqd->seq[ip][ic] & 0x5F) == 'G') || ((msqd->seq[ip][ic] & 0x5F) == 'T')) ? match : 0;
            else  match = (((msqd->seq[ip][ic] & 0x5F) != (oligol[j] & 0x5F)) && ((oligol[j] & 0x5F) != 'N')) ? 0 : match;
        }

        if (match > 0)
        {
            if (start >= 0)
                return i;
        }
    }

    return -1;
}

int findOligoInSeq(sqd *msqd, const char *oligol, int start, int noNs)
{
    int i, j, k;
    int st, loli, repeat, match, ip, ic;
    //  static int n=0;

    if ((msqd == NULL) || (msqd->seq == NULL) || (msqd->totalSize <= 0) || (oligol == NULL)) return -1;

    //q=win_scanf("Do you want the program to match oligo with N...N in the sequence ? \n Yes %R  No %r",&n);
    //if (q==WIN_CANCEL) return OFF;

    st = (start < 0) ? 0 : start;
    loli = strlen(oligol);

    for (i = st, repeat = 0; i < msqd->totalSize - loli; i++)
    {
        for (j = 0, match = 1; j < loli && match > 0; j++)
        {
            k = i + j;
            ip = k >> msqd->ln2PageSize;
            ic = k & msqd->pageMask;

            if ((msqd->seq[ip][ic] & 0x5F) == 'N')
                match = (noNs == 0) ? match : 0;
            else if ((oligol[j] & 0x5F) == 'R')
                match = (((msqd->seq[ip][ic] & 0x5F) == 'A') || ((msqd->seq[ip][ic] & 0x5F) == 'G')) ? match : 0;
            else if ((oligol[j] & 0x5F) == 'Y')
                match = (((msqd->seq[ip][ic] & 0x5F) == 'C') || ((msqd->seq[ip][ic] & 0x5F) == 'T')
                         || ((msqd->seq[ip][ic] & 0x5F) == 'U')) ? match : 0;
            else if ((oligol[j] & 0x5F) == 'S')
                match = (((msqd->seq[ip][ic] & 0x5F) == 'G') || ((msqd->seq[ip][ic] & 0x5F) == 'C')) ? match : 0;
            else if ((oligol[j] & 0x5F) == 'W')
                match = (((msqd->seq[ip][ic] & 0x5F) == 'A') || ((msqd->seq[ip][ic] & 0x5F) == 'T')) ? match : 0;
            else if ((oligol[j] & 0x5F) == 'M')
                match = (((msqd->seq[ip][ic] & 0x5F) == 'A') || ((msqd->seq[ip][ic] & 0x5F) == 'C')) ? match : 0;
            else if ((oligol[j] & 0x5F) == 'K')
                match = (((msqd->seq[ip][ic] & 0x5F) == 'G') || ((msqd->seq[ip][ic] & 0x5F) == 'T')) ? match : 0;
            else  match = (((msqd->seq[ip][ic] & 0x5F) != (oligol[j] & 0x5F)) && ((oligol[j] & 0x5F) != 'N')) ? 0 : match;
        }

        if (match > 0)
        {
            if (start >= 0) return i;

            repeat++;
        }
    }

    return repeat;
}
int findNNNBackwardInSeq(sqd *msqd, int start, int stop)
{
    int i;
    int  ip, ic;

    if ((msqd == NULL) || (msqd->seq == NULL) || (msqd->totalSize <= 0)) return -1;

    if (stop > start) return start;

    stop = (stop < 0) ? 0 : stop;

    for (i = start; i > stop; i--)
    {
        ip = i >> msqd->ln2PageSize;
        ic = i & msqd->pageMask;

        if ((msqd->seq[ip][ic] & 0x5F) == 'N') return i;
    }

    return i;
}


/*
   int main(int argc, char **argv)
   {
   FILE *fp = NULL;                 // the file descriptor
   int len, i;
   char ch, **seq = NULL;

   if (argc < 3)
   {
   fprintf(stderr,"Usage: loadSeq file.fasta sequence\n"
   "file.fasta being the sequence file\n"
   "sequence being a ATGC... string to search for");
   return 0;
   }

   fp = fopen(argv[1],"r");      // open file  for text reading
   if (fp == NULL)               // we test if the file opens ok
   {
   fprintf(stderr,"Cannot open %s\n",argv[1]);
   return 1;
   }
   fclose(fp);
   for (i = 0, len = strlen(argv[2]); i < len; i++)   // we loop on the sequence
   {
   ch = argv[2][i];
   if (ch != 'a' && ch != 'A' && ch != 'c' && ch != 'C' &&
   ch != 'g' && ch != 'G' && ch != 't' && ch != 'T')
   {
   fprintf(stderr,"Invalid base letter in sequence: %s\n",argv[2]);
   return 1;
   }
   }
   readFastaSeq(argv[1], &seq, &len);
   i = findOligoInSeq(seq, len, argv[2], 0);
   printf("Found %s at position %d\n",argv[2],i);
   i = findOligoInSeq(seq, len, argv[2], -1);
   printf("Found %s %d times\n",argv[2],i);
   return 0;
   }
   */
//int find_complementary_with_woobble(char *oli, char *comp, int size_c)
int do_find_oligo(void)
{
    int i = 0, j, k;
    int len = 0, ip, ic, q;//, len2,len3,len4;
    char ch, seqmatch[64];
    static int noNs = 1, more = 0, range = 100, hp = 0, all_seq = 0;
    char oligo2[128], oligo3[128], oligo4[128];//, comp1[128],comp2[128], comp3[128], comp4[128];
    bool display_each_occurence = true;
    int idx = 0;
    int max_idx = n_asqd;
    int count = 0;

    if (updating_menu_state != 0)
    {
        // we wait for a valid sequence to be loaded
        if (lsqd == NULL && asqd == NULL) active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }

    if (key[KEY_LSHIFT])
        return win_printf_OK("This routine finds an oligo in a fasta sequence");

    i = win_scanf("Define the sequence of oligo to find:\n%64s\n"
                  "%Rsearch direct sequence or %rcomplementary or %rboth(hairpin)\n"
                  "Forbid matching with Ns in sequence %b\n"
                  "extend search to 3 more oligos close by %b\n"
                  "find on all sequences %b", oligo, &hp, &noNs, &more, &all_seq);

    if (i == WIN_CANCEL) return OFF;

    if (more)
    {
        i = win_scanf("Define the sequences of 3 oligos to find near by:\n"
                      "%64s\n%64s\n%64s\n"
                      "define range size %8d\n", oligo2, oligo3, oligo4, &range);

        if (i == WIN_CANCEL) return OFF;

    }

    for (i = 0, len = strlen(oligo); i < len; i++)   // we loop on the sequence
    {
        ch = oligo[i];

        if (ch != 'a' && ch != 'A' && ch != 'c' && ch != 'C' &&
                ch != 'g' && ch != 'G' && ch != 't' && ch != 'T' &&
                ch != 'r' && ch != 'R' && ch != 'y' && ch != 'Y' &&
                ch != 's' && ch != 'S' && ch != 'w' && ch != 'W' &&
                ch != 'n' && ch != 'N'
           )
            return win_printf_OK("Invalid base letter in sequence: %s\n", oligo);
    }

    if (!all_seq)
    {
        idx = c_asqd;
        max_idx = c_asqd + 1;
    }

    for (; idx < max_idx; ++idx)
    {
        while (display_each_occurence)
        {
            i = findOligoInSeq(asqd[idx], oligo, i, noNs);

            for (j = 0; j < len; j++)
            {
                k = i + j;
                ip = k >> asqd[idx]->ln2PageSize;
                ic = k & lsqd->pageMask;
                seqmatch[j] = asqd[idx]->seq[ip][ic];
            }

            seqmatch[len] = 0;

            q = win_printf("Found %s at position %d\nmatching %s\nGo to next match -> OK else count hits\n", oligo, i, seqmatch);
            i++;

            if (q == WIN_CANCEL)
            {
                display_each_occurence = false;
            }
        }

        int cur_count = findOligoInSeq(asqd[idx], oligo, -1, noNs);
        //win_printf("Found %s in %d %d times\n", oligo, idx, cur_count);
        count += cur_count;
    }

    win_printf("Found %s %d times\n", oligo, count);
    return 0;
}


int do_find_oligo_triplex(void)
{
    int i = 0, j, k;
    int len = 0, ip, ic, q, match, msize, i_1;
    char seqmatch[2048];
    char seqfilename[2048];
    static int stretch = 1, nYorR = 12, isR = 0, dfile = 0, go;
    FILE *fp;

    if (updating_menu_state != 0)
    {
        // we wait for a valid sequence to be loaded
        if (lsqd == NULL && asqd == NULL) active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }

    if (key[KEY_LSHIFT])
        return win_printf_OK("This routine finds an oligo in a fasta sequence");

    i = win_scanf("Looking for homopurine or homopiridine stretches\n"
                  "Define %8d the minimum number of Y->%R or R->%r to find:\n"
                  "Find stretch length %b"
                  "dump to file %b"
                  , &nYorR, &isR, &stretch, &dfile);

    if (i == WIN_CANCEL) return OFF;

    msize = 63;

    if (dfile)
    {
        seqfilename[0] = 0;
        msize = sizeof(seqmatch) - 1;

        if (do_select_file(seqfilename, sizeof(seqfilename), "TXT-FILE", "*.txt", "File to save triplex\0", 0))
            return win_printf_OK("Cannot select output file");

        fp = fopen(seqfilename, "w");

        if (fp == NULL)
        {
            win_printf("Cannot open file:\n%s", backslash_to_slash(seqfilename));
            return -1;
        }

    }

    for (i = 0, len = nYorR; i < len; i++)   // we loop on the sequence
        oligo[i] = (isR) ? 'R' : 'Y';

    oligo[nYorR] = 0;

    for (i = i_1 = 0, go = 1; i >= 0 && go == 1;)
    {
        i = findOligoInSeq(lsqd, oligo, i, 1);

        if (i_1 > i)
        {
            go = 0;
            break;
        }

        i_1 = i;

        for (j = 0; j < len; j++)
        {
            k = i + j;
            ip = k >> lsqd->ln2PageSize;
            ic = k & lsqd->pageMask;
            seqmatch[j] = lsqd->seq[ip][ic];
        }

        seqmatch[len] = 0;
        k = len;

        if (stretch)
        {
            for (j = i + len, match = 1; match == 1 && j < lsqd->totalSize; j++)
            {
                ip = j >> lsqd->ln2PageSize;
                ic = j & lsqd->pageMask;

                if (isR)
                    match = (((lsqd->seq[ip][ic] & 0x5F) == 'A') || ((lsqd->seq[ip][ic] & 0x5F) == 'G')) ? match : 0;
                else if (isR == 0)
                    match = (((lsqd->seq[ip][ic] & 0x5F) == 'C') || ((lsqd->seq[ip][ic] & 0x5F) == 'T')) ? match : 0;

                if (match && k < msize) seqmatch[k++] = lsqd->seq[ip][ic];

            }

            seqmatch[k] = 0;
        }

        if (dfile)
        {
            fprintf(fp, "%s\t%d\t%d\n", seqmatch, i, j - i);
        }
        else
        {
            q = win_printf("Found %s at position %d\n"
                           "matching %s\n"
                           "Extending over %d\n"
                           "Go to next match -> OK else count hits\n"
                           , oligo, i, seqmatch, j - i);
        }

        if (j >= lsqd->totalSize) i = -1;
        else if (stretch) i = j + 1;
        else       i++;

        if (q == WIN_CANCEL) i = -1;
    }

    if (dfile) fclose(fp);

    i = findOligoInSeq(lsqd, oligo, -1, 1);
    win_printf("Found %s %d times\n", oligo, i);
    return 0;
}


int do_findSeq_plot_NN_in_fasta(void)
{
    int i;
    O_p *opn = NULL;
    int ip, ic, state, lstate;
    d_s *ds = NULL;
    pltreg *pr = NULL;


    if (updating_menu_state != 0)
    {
        // we wait for a valid sequence to be loaded
        if (lsqd == NULL) active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }


    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine multiply the y coordinate"
                             "of a data set by a number and place it in a new plot");
    }

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr", &pr) != 1) // we get the plot region
        return win_printf_OK("cannot find data at %s:%d", __FILE__, __LINE__);

    if ((opn = create_and_attach_one_plot(pr, 16, 16, 0)) == NULL)
        return win_printf_OK("cannot create plot at %s:%d", __FILE__, __LINE__);

    ds = opn->dat[0];
    ds->nx = ds->ny = 0;
    opn->need_to_refresh = 1;


    state = ((lsqd->seq[0][0] & 0x5F) == 'N') ? 1 : 0;
    add_new_point_to_ds(ds, 0, state);

    for (i = 0; i < lsqd->totalSize; i++)
    {
        ip = i >> lsqd->ln2PageSize;
        ic = i & lsqd->pageMask;
        lstate = ((lsqd->seq[ip][ic] & 0x5F) == 'N') ? 1 : 0;

        if (state != lstate)
        {
            add_new_point_to_ds(ds, (i > 0) ? i - 1 : i, state);
            add_new_point_to_ds(ds, i, lstate);
            state = lstate;
        }
    }

    add_new_point_to_ds(ds, (i > 0) ? i - 1 : i, state);
    set_plot_title(opn, "Seq: %s", lsqd->description);
    set_plot_x_title(opn, "Position along sequence");
    set_plot_y_title(opn, "Presence of 'N'");
    /* refisplay the entire plot */
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}


int line_least_square_fit_on_ds_between_indexes_chi2(const d_s *src_ds, bool is_affine_fit, int min_idx, int max_idx, float error_in_y_if_needed,
						double *out_a, double *out_b, double *chi2)
{
  int nx, i;
  bool has_good_error_bars = true;
  float minx = FLT_MAX;
  float maxx = -FLT_MAX;
  float miny = FLT_MAX;
  float maxy = -FLT_MAX;
  double sy2 = 0, sy = 0, sx = 0, sx2 = 0, sxy = 0, sig2 = 0, sig_2 = 0, ny = 0;
  double a = 0, b = 0, lchi2 = 0;

  //assert(src_ds);

    min_idx = (min_idx < 0) ? 0 : min_idx;
    max_idx = (max_idx < 0) ? src_ds->nx : max_idx;
    nx = max_idx - min_idx;
    error_in_y_if_needed = (error_in_y_if_needed <= 0) ? 1 : error_in_y_if_needed;
    if (max_idx - min_idx < 2)
    {
        (win_printf_OK("no fit on less than 2 points"));
        return 1;
    }
    if (src_ds->ye == NULL)
    {
        has_good_error_bars = false;
    }
    else
    {
        for (i = min_idx, has_good_error_bars = true; i < max_idx && has_good_error_bars == true; i++)
	  has_good_error_bars = (src_ds->ye[i] == 0) ? false : has_good_error_bars;
    }
    minx = miny = FLT_MAX;
    maxx = maxy = -FLT_MAX;
    if (has_good_error_bars == 0)
    {
        for (i = min_idx; i < max_idx ; i++)
        {
            sy += src_ds->yd[i];
            sy2 += src_ds->yd[i] * src_ds->yd[i];
            sx += src_ds->xd[i];
            sx2 += src_ds->xd[i] * src_ds->xd[i];
            sxy += src_ds->xd[i] * src_ds->yd[i];
            minx = (src_ds->xd[i] < minx) ? src_ds->xd[i] : minx;
            maxx = (src_ds->xd[i] > maxx) ? src_ds->xd[i] : maxx;
            miny = (src_ds->yd[i] < miny) ? src_ds->yd[i] : miny;
            maxy = (src_ds->yd[i] > maxy) ? src_ds->yd[i] : maxy;
        }
        if (!is_affine_fit)
        {
            if (sx2 == 0)
            {
                win_printf_OK("\\Sigma x_i^2 = 0   => cannot perform fit.");
                return 2;
            }
            a = (sxy / sx2);
            b = 0.;
        }
        else
        {
            b = (sx2 * nx) - sx * sx;
            if (b == 0)
            {
                win_printf_OK("\\Delta  = 0 \n can't fit that!");
                return 3;
            }
            a = sxy * nx - sx * sy;
            a /= b;
            b = (sx2 * sy - sx * sxy) / b;
        }
        for (i = 0, lchi2 = 0; i < nx; i++)
        {
            sig2 = error_in_y_if_needed * error_in_y_if_needed;
            sig_2 = (double)1 / sig2;
            lchi2 += (a * src_ds->xd[i] + b - src_ds->yd[i]) * (a * src_ds->xd[i] + b - src_ds->yd[i]) * sig_2;
        }
    }
    else  // with error bars in y
    {
      for (i = 0; i < nx ; i++)
        {
	  sig2 = src_ds->ye[i] * src_ds->ye[i];
	  sig_2 = (double)1 / sig2;
	  sy += src_ds->yd[i] * sig_2;
	  sy2 += src_ds->yd[i] * src_ds->yd[i] * sig_2;
	  sx += src_ds->xd[i] * sig_2;
	  sx2 += src_ds->xd[i] * src_ds->xd[i] * sig_2;
	  sxy += src_ds->xd[i] * src_ds->yd[i] * sig_2;
	  ny += sig_2;
	  minx = (src_ds->xd[i] < minx) ? src_ds->xd[i] : minx;
	  maxx = (src_ds->xd[i] > maxx) ? src_ds->xd[i] : maxx;
	  miny = (src_ds->yd[i] < miny) ? src_ds->yd[i] : miny;
	  maxy = (src_ds->yd[i] > maxy) ? src_ds->yd[i] : maxy;
        }
      if (!is_affine_fit)
        {
	  if (sx2 == 0)
            {
	      win_printf_OK("\\Sigma x_i^2 = 0   => cannot perform fit.");
	      return 4;
            }
	  a = (sxy / sx2);
	  b = 0.;
        }
      else
        {
	  b = (sx2 * ny) - sx * sx;

	  if (b == 0)
            {
                win_printf_OK("\\Delta  = 0 \n can't fit that!");
                return 5;
            }
            a = sxy * ny - sx * sy;
            a /= b;
            b = (sx2 * sy - sx * sxy) / b;
        }
        for (i = 0, lchi2 = 0; i < nx; i++)
        {
	  sig2 = src_ds->ye[i] * src_ds->ye[i];
	  sig_2 = (double)1 / sig2;
	  lchi2 += (a * src_ds->xd[i] + b - src_ds->yd[i]) * (a * src_ds->xd[i] + b - src_ds->yd[i]) * sig_2;
        }
    }

    if (out_a) *out_a = a;
    if (out_b) *out_b = b;
    if (chi2)  *chi2 = lchi2;
    return 0;
}


int find_possible_seq_swap(char *seq, int nseq, // the sequence and its size
			   int olis,  // the oligo size
			   int start, // starting position of deb1
			   int start2, // starting position of deb2
			   int start3, // starting position of end1
			   int max_s, // the maximum distance between deb2 and deb1
			   int *deb1, int *end1,  // the beginning and end of first sequence
			   int *deb2, int *end2, // the beginning and end of first sequence
			   int verbose)
{
  int oi, match, i, k, kk, found;
  char seed1[32] = {0}, seed2[32] = {0};

  for (found = 0 ; (found == 0) && (start < nseq - olis); start++)
    {// we grab the first sead on the left
      for (oi = 0; (oi < olis-1) && (start+oi < nseq); oi++)
	seed1[oi] = seq[start+oi];
      seed1[oi] = 0;
      for (i = 1+start2; (found == 0) && (i < max_s) && ((start+olis-2+i) < nseq); i++)
	{// is there a maching possibility nearby ?
	  for (oi = 0, match = 0; (oi < olis-1) && (start+oi+i < nseq); oi++)
	    match += (seed1[oi] == seq[start+oi+i]) ? 1 : 0;
	  if (match == olis-1) break;
	}
      if (i == max_s) continue; // no matching stuff in range
      if (match < olis-1) continue;
      *deb1 = start;
      *deb2 = start+i;// we save position of  matching stuff
      *deb2 = (*deb2 < 0) ? 0 : *deb2;
      *deb2 = (*deb2 < nseq) ? *deb2 : nseq-1;
      if (verbose != WIN_CANCEL)
	verbose = win_printf("found seed1 %s in deb1=%d and deb2=%d\n"
		 "%c%c and %c%c\n"
		 ,seed1,*deb1,*deb2,seq[*deb1],seq[*deb1+1]
		 ,seq[*deb2],seq[*deb2+1]);
      for (i = *deb1+1+start3; (found == 0) && (i <= *deb2); i++)
	{// we grab the second sead on the right
	  for (oi = 0; oi < olis-1 && (i+oi < nseq); oi++)
	    seed2[oi] = seq[i+oi];
	  seed2[oi] = 0;
	  if (verbose != WIN_CANCEL)
	    verbose = win_printf("i = %d seed2 %s, deb2 %d",i,seed2,*deb2);
	  for (k = olis-1; (found == 0) && (k < max_s) && ((olis-2+*deb2+k) < nseq); k++)
	    {// is there a second maching possibility
	      for (oi = 0, match = 0; (oi < olis-1) && (oi+*deb2+k < nseq); oi++)
		{
		  kk = oi+*deb2+k;
		  if (kk < 0 || kk >= nseq)
		    {
		      win_printf("kk = %d,  oi %d deb2 %d k %d",kk,oi,*deb2,k);
		      continue;
		    }
		  match += (seed2[oi] == seq[kk]) ? 1 : 0;
		}
	      if (match == olis-1)
		{
		  if (verbose != WIN_CANCEL)
		    {
		      verbose = win_printf("found seed1 %s in deb1=%d and deb2=%d\n"
					   "%c%c and %c%c\n"
					   "match %d seed2 %s found in end1=%d and %c%c at end2=%d"
					   ,seed1,*deb1,*deb2,seq[*deb1],seq[*deb1+1]
					   ,seq[*deb2],seq[*deb2+1]
					   ,match,seed2,i,seq[*deb2+k],seq[1+*deb2+k],*deb2+k
					   );
		    }
		  found = 1;
		  break;
		}
	    }
	  if (k == max_s) continue; // no matching stuff in range
	  if (match < olis-1) continue;
	  *end1 = i;
	  *end2 = *deb2 + k;
	  if (*end1 - *deb1 < 1) continue;
	  if (*end2 - *deb2 < 1) continue;
	  if (*end2 >= nseq) continue;
	}
    }
  return (found) ? 0 : 1;
}


int do_swap(char *seq, int nseq, // the sequence and its size
	    float *olipos, //the array with oligo position
	    float *olipos_t, //the array with oligo position swapped
	    int noligos, //the number of oligos
	    int olis,  // the oligo size
	    int deb1, int end1,  // the beginning and end of first sequence
	    int deb2, int end2, // the beginning and end of first sequence
	    float *olipos_er, // the error in position
	    float *olipos_t_er, // the swapped error in position
	    int pos_has_error) // a flag saying if error is valid

{
  int oi, len, k, match, il, il2;
  float tmp;
  char  wrongseq[256];

  (void)pos_has_error;
  for (oi = 0; (oi < end2 - deb2+olis-1) && (oi < (int)sizeof(wrongseq)); oi++)
    wrongseq[oi] = seq[deb2+oi];
  k = oi;
  for (oi = 0; (oi < deb2 - end1) && (end1+olis-1+oi < nseq) && (k+oi < (int)sizeof(wrongseq)); oi++)
    wrongseq[k+oi] = seq[end1+olis-1+oi];
  k += oi;
  for (oi = 0; (oi < end1 - deb1) && (k+oi < (int)sizeof(wrongseq)); oi++)
    wrongseq[k+oi] = seq[deb1+oi+olis-1];
  wrongseq[k+oi] = 0;
  len = strlen(wrongseq);

  for (oi = 0; (oi < len) && (deb1+oi < nseq); oi++)
    seq[deb1+oi] = wrongseq[oi];

  for(il = 0; il < deb1; il++)
    {
      olipos_t[il] = olipos[il];
      olipos_t_er[il] = olipos_er[il];
    }
  for(il = deb1; il < end1; il++)
    {
      olipos_t[end2-end1+il] = olipos[il];
      olipos_t_er[end2-end1+il] = olipos_er[il];
    }
  for(il = end1; il < deb2; il++)
    {
      olipos_t[end2-end1-deb2+deb1+il] = olipos[il];
      olipos_t_er[end2-end1-deb2+deb1+il] = olipos_er[il];
    }
  for(il = deb2; il < end2; il++)
    {
      olipos_t[deb1-deb2+il] = olipos[il];
      olipos_t_er[deb1-deb2+il] = olipos_er[il];
    }
  for(il = end2; il < noligos; il++)
    {
      olipos_t[il] = olipos[il];
      olipos_t_er[il] = olipos_er[il];
    }
  //for(il = deb1; il < end2; il++)
  for(il = 0; il < noligos; il++)
    {
      //for(il2 = deb1+1; il2 < end2; il2++)
      for(il2 = il+1; il2 < noligos; il2++)
	{
	  for (oi = 0, match = 0; oi < olis; oi++)
	    match += (seq[il+oi] == seq[il2+oi]) ? 1 : 0;
	  if ((match == olis) && (olipos_t[il] > olipos_t[il2]))
	    {
	      tmp = olipos_t[il];
	      olipos_t[il] = olipos_t[il2];
	      olipos_t[il2] = tmp;
	      tmp = olipos_t_er[il];
	      olipos_t_er[il] = olipos_t_er[il2];
	      olipos_t_er[il2] = tmp;
	    }
	}
    }
  for(il = 0; il < noligos; il++)
    {
      olipos[il] = olipos_t[il];
      olipos_er[il] = olipos_t_er[il];
    }
  return 0;
}


int compute_swap_energy(int deb1, int end1,  // the beginning and end of first sequence
			int deb2, int end2) // the beginning and end of first sequence
{
  int E;
  E = (end1 - deb1) * (end2 - end1) * (end2 - end1);
  E += (deb2 - end1) * (end2 - deb2 - end1 + deb1) * (end2 - deb2 - end1 + deb1);
  E += (end2 - deb2) * (deb1 - deb2) * (deb1 - deb2);
  return E;
}


char *describe_swap(char *seq, int nseq, // the sequence and its size
		    float *olipos, //the array with oligo position
		    float *olipos_t, //the array with oligo position swapped
		    int noligos, //the number of oligos
		    int olis,  // the oligo size
		    int deb1, int end1,  // the beginning and end of first sequence
		    int deb2, int end2) // the beginning and end of first sequence
{
  int oi, bper, k, match, E, idem, il, il2, len;
  float corr = 0, corr2 = 0, tmp;
  char pa1[32], inter[32], pa2[32], wrongseq[256], goodseq[256];
  double mv_a = 0, mv_b = 0, mv_chi2 = 0, a = 0, b = 0, chi2 = 0;
  static float ex_noise = 3;
  char seed1[32] = {0}, seed2[32] = {0}, message[4096] = {0};
  d_s *dsi = NULL, *dsfit = NULL;
  static char *swseq = NULL;
  static int nswseq = 0;

  if (nswseq < nseq)
    {
      if (swseq != NULL) free(swseq);
      swseq = (char*)calloc(1+nseq,sizeof(char));
      if (swseq == NULL) return win_printf_ptr("Cannot allocate swseq at %d",1+nseq);
      nswseq = nseq+1;
    }

  for (oi = 0; (oi < end1 - deb1+olis-1) && (oi < (int)sizeof(pa1)); oi++)
    pa1[oi] = (oi < olis-1) ? 0x20 | seq[deb1+oi] : 0xDF&seq[deb1+oi];
  pa1[oi] = 0;
  for (oi = 0; (oi < deb2 - end1) && (end1+olis-1+oi < nseq) && (oi < (int)sizeof(inter)); oi++)
    inter[oi] = seq[end1+olis-1+oi];
  inter[oi] = 0;
  for (oi = 0; (oi < end2 - deb2+olis-1) && (oi < (int)sizeof(pa2)); oi++)
    pa2[oi] = (oi < olis-1) ? 0x20 | seq[deb2+oi] : 0xDF&seq[deb2+oi];
  pa2[oi] = 0;
  for (oi = 0; (oi < end2 - deb2+olis-1) && (oi < (int)sizeof(wrongseq)); oi++)
    wrongseq[oi] = seq[deb2+oi];
  k = oi;
  for (oi = 0; (oi < deb2 - end1) && (end1+olis-1+oi < nseq) && (k+oi < (int)sizeof(wrongseq)); oi++)
    wrongseq[k+oi] = seq[end1+olis-1+oi];
  k += oi;
  for (oi = 0; (oi < end1 - deb1) && (k+oi < (int)sizeof(wrongseq)); oi++)
    wrongseq[k+oi] = seq[deb1+oi+olis-1];
  wrongseq[k+oi] = 0;

  len = strlen(wrongseq);

  for (oi = 0; oi < nseq; oi++)
    swseq[oi] = seq[oi];
  swseq[oi] = 0;
  for (oi = 0; (oi < len) && (deb1+oi < nseq); oi++)
    swseq[deb1+oi] = wrongseq[oi];


  for (oi = 0; oi < end2 - deb1+olis-1 && (oi < (int)sizeof(goodseq)); oi++)
    goodseq[oi] = seq[deb1+oi];
  goodseq[oi] = 0;
  for (oi = 0, bper = 0; goodseq[oi] != 0 && wrongseq[oi] != 0; oi++)
    bper += (goodseq[oi] != wrongseq[oi]) ? 1 : 0;
  if (strlen(pa1) != strlen(pa2))
    {
      match = 0;
    }
  else
    {
      for (oi = 0, match = 0; pa1[oi] > 0 && pa2[oi] > 0; oi++)
	match += (pa2[oi] == pa1[oi]) ? 1 : 0;
    }
  idem = (match == (int)strlen(pa1))? 1 : 0;
  for (oi = 0; (oi < olis-1) && (oi < (int)sizeof(seed1)); oi++)
    seed1[oi] = pa1[oi];
  seed1[oi] = 0;

  for (oi = 0; (oi < olis-1) && (oi < (int)sizeof(seed2)); oi++)
    seed2[oi] = pa1[end1-deb1+oi];
  seed2[oi] = 0;

  E = (end1 - deb1) * (end2 - end1) * (end2 - end1);
  E += (deb2 - end1) * (end2 - deb2 - end1 + deb1) * (end2 - deb2 - end1 + deb1);
  E += (end2 - deb2) * (deb1 - deb2) * (deb1 - deb2);

  if (olipos != NULL && olipos_t != NULL)
    {
      dsi = build_data_set(noligos,noligos);
      dsfit = build_data_set(noligos,noligos);
      if (dsi == NULL || dsfit == NULL)
	return win_printf_ptr("Cannot create temp ds");
      for(il = 0; il < noligos; il++)
	{
	  dsi->xd[il] = il;
	  dsi->yd[il] = olipos[il];
	}
      dsi->nx = dsi->ny = noligos;
      line_least_square_fit_on_ds_between_indexes_chi2(dsi, 1, 0, noligos, ex_noise, &a, &b, &chi2);

      for(il = 0; il < deb1; il++)
	olipos_t[il] = olipos[il];
      for(il = deb1; il < end1; il++)
	olipos_t[end2-end1+il] = olipos[il];
      for(il = end1; il < deb2; il++)
	olipos_t[end2-end1-deb2+deb1+il] = olipos[il];
      for(il = deb2; il < end2; il++)
	olipos_t[deb1-deb2+il] = olipos[il];
      for(il = end2; il < noligos; il++)
	olipos_t[il] = olipos[il];

      for(il = 0; il < noligos; il++)
	{
	  //for(il2 = deb1+1; il2 < end2; il2++)
	  for(il2 = il+1; il2 < noligos; il2++)
	    {
	      for (oi = 0, match = 0; oi < olis; oi++)
		match += (swseq[il+oi] == swseq[il2+oi]) ? 1 : 0;
	      if ((match == olis) && (olipos_t[il] > olipos_t[il2]))
		{
		  tmp = olipos_t[il];
		  olipos_t[il] = olipos_t[il2];
		  olipos_t[il2] = tmp;
		}
	    }
	}
      for(il = 0; il < noligos; il++)
	{
	  dsfit->xd[il] = il;
	  dsfit->yd[il] = olipos_t[il];
	}
      dsfit->nx = dsfit->ny = noligos;
      line_least_square_fit_on_ds_between_indexes_chi2(dsfit, 1, 0, noligos, ex_noise, &mv_a, &mv_b, &mv_chi2);

      for(il = deb1, corr = corr2 = 0; il < end1; il++)
	{
	  tmp = a * il + b;
	  corr += (olipos[il] - tmp) * (end2 - end1);
	  corr2 += (olipos_t[il] - tmp) * (end2 - end1);
	}
      for(il = end1; il < deb2; il++)
	{
	  tmp = a * il + b;
	  corr += (olipos[il] - tmp) * (end2 - deb2 - end1 + deb1);
	  corr2 += (olipos_t[il] - tmp) * (end2 - deb2 - end1 + deb1);
	}
      for(il = deb2; il < end2; il++)
	{
	  tmp = a * il + b;
	  corr += (olipos[il] - tmp) * (deb1 - deb2);
	  corr2 += (olipos_t[il] - tmp) * (deb1 - deb2);
	}

    }

  snprintf(message,sizeof(message),"match %d found ambiguous sequence at [%d, %d[ and [%d, %d[\n"
	   "seed1 %s seed2 %s; deb1=%d; end1=%d; deb2=%d; end2=%d\n"
	   "sequence %s-%s-%s E = %d\n"
	   "%dx(E1=%d) + %dx(E_{inter}=%d) + %dx(E2=%d)\n"
	   "%s ->good\n%s ->bad %d errors\n"
	   "%s sequences\n"
	   "original seq a = %g b = %g \\chi^2 = %g; coorelation %g\n"
	   "swapped seq a = %g b = %g \\chi^2 = %g; coorelation %g\n"
	   ,match,deb1,end1+olis-1,deb2, end2+olis-1
	   ,seed1,seed2,deb1,end1,deb2,end2
	   , pa1,inter,pa2, E
	   ,(end1 - deb1), (end2 - end1) * (end2 - end1)
	   ,(deb2 - end1), (end2 - deb2 - end1 + deb1) * (end2 - deb2 - end1 + deb1)
	   ,(end2 - deb2), (deb1 - deb2) * (deb1 - deb2),goodseq,wrongseq,bper
	   ,(idem)?"Identical ":"Different "
	   ,a,b,chi2,corr,mv_a,mv_b,mv_chi2,corr2);

    free_data_set(dsi);
    free_data_set(dsfit);
    return strdup(message);
}



d_s *convert_swap_to_ds(char *seq, int nseq, // the sequence and its size
			float *olipos, //the array with oligo position
			float *olipos_t, //the array with oligo position swapped
			int noligos, //the number of oligos
			int olis,  // the oligo size
			int deb1, int end1,  // the beginning and end of first sequence
			int deb2, int end2, // the beginning and end of first sequence
			float *olipos_er, // the error in position
			float *olipos_t_er, // the swapped error in position
			int pos_has_error) // a flag saying if error is valid

{
  int oi, match, il, il2, swp, k, len;
  float tmp;
  d_s *dsfit = NULL;
  static char  wrongseq[256], *swseq = NULL;
  static int nswseq = 0;

  if (nswseq < nseq)
    {
      if (swseq != NULL) free(swseq);
      swseq = (char*)calloc(1+nseq,sizeof(char));
      if (swseq == NULL) return win_printf_ptr("Cannot allocate swseq at %d",1+nseq);
      nswseq = nseq+1;
    }

  for (oi = 0; (oi < end2 - deb2+olis-1) && (oi < (int)sizeof(wrongseq)); oi++)
    wrongseq[oi] = seq[deb2+oi];
  k = oi;
  for (oi = 0; (oi < deb2 - end1) && (end1+olis-1+oi < nseq) && (k+oi < (int)sizeof(wrongseq)); oi++)
    wrongseq[k+oi] = seq[end1+olis-1+oi];
  k += oi;
  for (oi = 0; (oi < end1 - deb1) && (k+oi < (int)sizeof(wrongseq)); oi++)
    wrongseq[k+oi] = seq[deb1+oi+olis-1];
  wrongseq[k+oi] = 0;
  len = strlen(wrongseq);

  for (oi = 0; oi < nseq; oi++)
    swseq[oi] = seq[oi];
  swseq[oi] = 0;
  for (oi = 0; (oi < len) && (deb1+oi < nseq); oi++)
    swseq[deb1+oi] = wrongseq[oi];

  if (olipos != NULL && olipos_t != NULL)
    {
      dsfit = build_data_set(noligos,noligos);
      if (dsfit == NULL)
	return win_printf_ptr("Cannot create temp ds");

      if (pos_has_error)
	alloc_data_set_y_error(dsfit);


      for(il = 0; il < deb1; il++)
	{
	  olipos_t[il] = olipos[il];
	  olipos_t_er[il] = olipos_er[il];
	}
      for(il = deb1; il < end1; il++)
	{
	  olipos_t[end2-end1+il] = olipos[il];
	  olipos_t_er[end2-end1+il] = olipos_er[il];
	}
      for(il = end1; il < deb2; il++)
	{
	  olipos_t[end2-end1-deb2+deb1+il] = olipos[il];
	  olipos_t_er[end2-end1-deb2+deb1+il] = olipos_er[il];
	}
      for(il = deb2; il < end2; il++)
	{
	  olipos_t[deb1-deb2+il] = olipos[il];
	  olipos_t_er[deb1-deb2+il] = olipos_er[il];
	}
      for(il = end2; il < noligos; il++)
	{
	  olipos_t[il] = olipos[il];
	  olipos_t_er[il] = olipos_er[il];
	}

      for(il = 0, swp = 0; il < noligos; il++)
	{
	  for(il2 = il+1; il2 < noligos; il2++)
	    {
	      for (oi = 0, match = 0; (oi < olis) && (il+oi < nseq) && (il2+oi < nseq); oi++)
		match += (swseq[il+oi] == swseq[il2+oi]) ? 1 : 0;
	      if ((match == olis) && (olipos_t[il] > olipos_t[il2]))
		{
		  tmp = olipos_t[il];
		  olipos_t[il] = olipos_t[il2];
		  olipos_t[il2] = tmp;
		  tmp = olipos_t_er[il];
		  olipos_t_er[il] = olipos_t_er[il2];
		  olipos_t_er[il2] = tmp;
		  swp++;
		}
	    }
	}
      for(il = 0; il < noligos; il++)
	{
	  dsfit->xd[il] = il;
	  dsfit->yd[il] = olipos_t[il];
	  if (dsfit->ye != NULL)
	    dsfit->ye[il] = olipos_t_er[il];

	}
      dsfit->nx = dsfit->ny = noligos;
      set_ds_treatement(dsfit,"deb1 %d end1 %d deb2 %d end2 %d swp %d"
		       ,deb1, end1, deb2, end2,swp);
      return dsfit;
    }
    return NULL;
}

d_s *convert_seq_to_ds(float *olipos, //the array with oligo position
		       int noligos, //the number of oligos
		       float *olipos_er, // the error in position
		       int pos_has_error) // a flag saying if error is valid
{
  int il;
  d_s *dsi = NULL;

  if (olipos != NULL)
    {
      dsi = build_data_set(noligos,noligos);
      if (dsi == NULL)
	return win_printf_ptr("Cannot create temp ds");
      if (pos_has_error)
	alloc_data_set_y_error(dsi);
      for(il = 0; il < noligos; il++)
	{
	  dsi->xd[il] = il;
	  dsi->yd[il] = olipos[il];
	  if (dsi->ye != NULL)
	    dsi->ye[il] = olipos_er[il];
	}
      dsi->nx = dsi->ny = noligos;

      return dsi;
    }
    return NULL;
}


int line_least_square_fit_slope_impose_on_ds(d_s *dsi, float ex_noise, double a, double *b, double *chi2)
{
  int i;
  double yi_axi = 0, bt = 0, tmp, chi, sig2, ny;

  for (i = 0, yi_axi = 0, ny = 0; i < dsi->nx; i++)
    {
      sig2 = (dsi->ye != NULL) ? dsi->ye[i] * dsi->ye[i] : ex_noise * ex_noise;
      sig2 = (double)1 / sig2;
      yi_axi += (dsi->yd[i] - a * dsi->xd[i]) * sig2;
      ny += sig2;
    }
  bt = (ny > 0) ? yi_axi/ny : yi_axi;
  for (i = 0, chi = 0; i < dsi->nx; i++)
    {
      tmp = dsi->yd[i] - a * dsi->xd[i] - bt;
      sig2 = (dsi->ye != NULL) ? dsi->ye[i] * dsi->ye[i] : ex_noise * ex_noise;
      chi += (tmp*tmp)/sig2;
    }
  if (b) *b = bt;
  if (chi2) *chi2 = chi;
  return 0;
}


int do_findSeq_MT_ambiguous_oligo_assemble_plot(void)
{
  static int start = 0, start2 = 16, start3 = 8, max_s = 32, olis = 3, verb = 0, Emax = 1000, savefile = 0,  improv = 1, niter = 1000000, fix_a = 0;
  int end, deb1, deb2, end1, end2, i, j, k, li, oi, match, noligos = 0;
  char  olild[16] = {0}, olild_1[16] = {0};
  static char seq[128] = {0};
  char bestseq[10][128] = {0};
  double mv_a = 0, mv_b = 0, mv_chi2 = 0, a = 0, b = 0, chi2 = 0;
  float olipos[128] = {0}, olipos_t[128] = {0}, oliposb[10][128] = {0};
  float olipos_er[128] = {0}, olipos_t_er[128] = {0}, oliposb_er[10][128] = {0};
  int il;
  static char seqfilename[2048] = {0};
  FILE *fp = NULL;
  static float ex_noise = 3, tempe = 1, th_chi2 = 45, impose_stretching = 0.9;
  pltreg *pr = NULL;
  int il2, verbose, E =0, bchi2_i[10] = {0}, position_er_available = 1;
  float tmp, tempt;
  d_s *dsfit = NULL, *dsi = NULL, *dsc = NULL, *dss = NULL;
  O_p *op = NULL, *opc = NULL;


  if (updating_menu_state != 0)
    {
      return D_O_K;
    }
  if (ac_grep(cur_ac_reg, "%pr", &pr) != 1) // we get the plot region
    return win_printf_OK("cannot find data at %s:%d", __FILE__, __LINE__);

  i = win_scanf("Look for ambiguous sequence in oligonucleodide assembly in\n"
		"loading sequence from a file containing oligo positions\n"
		"Noise expected %5f\n"
		"Oligomer size %4d, search size %4d\n"
		"For least square fit, Impose the stretching %b at value %5f\n"
		"Display event if their energy is bigger than %5d\n"
		"%b if oligo position, display only \\chi^2 favorable to move\n"
		"Temperature %5f Nb. of iter %6d\n"
		"%b->Verbose; %b->save result in a file\n"
		"Threshold to Save stuff in \\chi^2 %5f\n"
		,&ex_noise,&olis,&max_s,&fix_a,&impose_stretching,&Emax,&improv,&tempe,&niter,&verb,&savefile,&th_chi2);
  if (i == WIN_CANCEL) return 0;

  verbose = (verb == 0) ? WIN_CANCEL : 0;

  if (do_select_file(seqfilename, sizeof(seqfilename), "TXT-FILE", "*.txt", "File to load oligos and position", 1))
    return win_printf_OK("Cannot select output file");
  fp = fopen (seqfilename,"r");
  if (fp == NULL)
    return win_printf_OK("Impossible to open file %s",backslash_to_slash(seqfilename));
  position_er_available = 1;
  for (i = 0, j = 2; i < (int)sizeof(seq) && j >= 2; )
    {
      olipos[i] = 0;
      olipos_er[i] = 0;
      j = fscanf(fp,"%s %f %f\n",olild,olipos+i,olipos_er+i);
      //win_printf("Oligo %d ->%d read %s %g %g",i,j,olild,olipos[i],olipos_er[i]);
      if (j > 1)
	position_er_available = (j <= 2) ? 0 : position_er_available;
      if (j >= 2)
	{

	  if (i == 0)
	    {
	      for (li = 0; olild[li] != 0 && li < (int)sizeof(olild); li++)
		seq[li] = olild[li];
	      if (li != olis) win_printf("Line %d:\noligo size %d differ from %d proposed",i,li,olis);
	      k = li;
	      for (li = 0; olild[li] != 0 && li < (int)sizeof(olild); li++)
		olild_1[li] = olild[li];
	      olild_1[li] = 0;
	    }
	  else
	    {
	      for (li = 0; olild[li] != 0 && li < (int)sizeof(olild); li++);
	      if (li != olis) win_printf("Line %d:\noligo size %d differ from %d proposed",i,li,olis);
	      for (oi = 0, match = 0; (oi < olis - 1); oi++)
		match += (olild_1[oi+1] == olild[oi]) ? 1 : 0;
	      if (match != olis - 1)
		win_printf("Line %d: match %d\noligo %s does not continue oligo %s"
			   ,i,match,olild,olild_1);
	      li = (li > 0) ? li - 1: 0;
	      seq[k++] = olild[li];
	      for (li = 0; olild[li] != 0 && li < (int)sizeof(olild); li++)
		olild_1[li] = olild[li];
	      olild_1[li] = 0;
	    }
	  i++;
	  noligos++;
	}
    }
  //win_printf("n oligos %d seq length %d",noligos,k);

  for(il = 0; il < noligos; il++)
    {
      for(il2 = il+1; il2 < noligos; il2++)
	{
	  for (oi = 0, match = 0; oi < olis; oi++)
	    match += (seq[il+oi] == seq[il2+oi]) ? 1 : 0;
	  /*
	  if (match == 3)
	    win_printf("match %d olis %d\noligo %c%c%c at %d->%g \nand oligo2 %c%c%c at %d->%g"
		       ,match,olis,seq[il],seq[il+1],seq[il+2],il,olipos[il]
		       ,seq[il2],seq[il2+1],seq[il2+2],il2,olipos[il2]);
	  */
	  if ((match == olis) && (olipos[il] > olipos[il2]))
	    {
	      win_printf("swapp oligo %c%c%c at %d->%g and %d->%g"
		  	 ,seq[il],seq[il+1],seq[il+2],il,olipos[il]
		  	 ,il2,olipos[il2]);
	      tmp = olipos[il];
	      olipos[il] = olipos[il2];
	      olipos[il2] = tmp;
	      tmp = olipos_er[il];
	      olipos_er[il] = olipos_er[il2];
	      olipos_er[il2] = tmp;
	    }
	}
    }

  seq[k] = 0;
  fclose(fp);
  win_printf("Seq:\n%s\nLength %d and %d oligos error %d",seq,k,noligos,position_er_available);
  end = k;


  if (savefile)
    {
      if (do_select_file(seqfilename, sizeof(seqfilename), "TXT-FILE", "*.txt", "File to save ambiguous sequence", 0))
	return win_printf_OK("Cannot select output file");
      fp = fopen (seqfilename,"w");
      if (fp == NULL)  win_printf_OK("Cannot open file:\n%s",backslash_to_slash(seqfilename));
    }


  if ((opc = create_and_attach_one_plot(pr, 16, 16, 0)) == NULL)
    return win_printf_OK("cannot create plot at %s:%d", __FILE__, __LINE__);

  dsc = opc->dat[0];
  dsc->nx = dsc->ny = 0;
  opc->need_to_refresh = 1;

  op = create_one_empty_plot();
  if (op == NULL) return win_printf_OK("Cannot allocate plot");
  add_data_to_pltreg(pr, IS_ONE_PLOT, (void*)op);

  //add_one_plot_data(op, IS_DATA_SET, (void*)dsi);
  int mvd = 0, mvu = 0, noamb = 0, ebig = 0, iter;
  float chi2_min[10] = {0}, prob, testp;

  dsi = convert_seq_to_ds(olipos, noligos, olipos_er, position_er_available);
  if (dsi == NULL)
    return win_printf("cannot convert to ds");
  if (fix_a)
    {
      line_least_square_fit_slope_impose_on_ds(dsi, ex_noise, impose_stretching, &b, &chi2);
      a = impose_stretching;
    }
  else
    line_least_square_fit_on_ds_between_indexes_chi2(dsi, 1, 0, noligos, ex_noise, &a, &b, &chi2);
  win_printf("initial chi2 = %g",chi2);
  for (i = 0; i < 10; i++) chi2_min[i] = chi2;
  add_one_plot_data(op, IS_DATA_SET, (void*)dsi);
  set_plot_title(op,"\\chi^2 = %g",*chi2_min);
  op->need_to_refresh = 1;
  refresh_plot(pr, pr->n_op - 1);

  O_p *opn = NULL; d_s *dstmp = NULL; char *mes = NULL;
  if (fp != NULL) dss = build_data_set(16,16);
  dss->nx = dss->ny = 0;
  for (iter = 0; iter < niter; iter++)
    {
      //i = win_scanf("choose start %5d\nstart2 %5d\n start3 %5d\n%b->Verbose\nlast E %5d\n"
      //	    ,&start, &start2, &start3,&verb,&E);
      // verbose = (verb == 0) ? WIN_CANCEL : 0;
      // if (i != WIN_OK) break;
      tempt = (tempe * (niter - iter))/niter;
      start = (int)(ran1(&idum)*noligos);
      start2 = (int)(ran1(&idum)*(max_s/2));
      start3 = (int)(ran1(&idum)*(max_s/4));
      if (find_possible_seq_swap(seq, end, olis, start, start2, start3, max_s, &deb1, &end1, &deb2, &end2, verbose) == 0)
	{
	  E = compute_swap_energy(deb1, end1, deb2, end2);
	  if (E < Emax)
	    {
	      mes = describe_swap(seq, end,  olipos, olipos_t, noligos, olis, deb1, end1, deb2, end2);
	      dsfit = convert_swap_to_ds(seq, end, olipos, olipos_t, noligos, olis, deb1, end1, deb2, end2, olipos_er, olipos_t_er, position_er_available);
	      dsi = convert_seq_to_ds(olipos, noligos, olipos_er, position_er_available);
	      if (dsi)
		{
		  if (fix_a)
		    {
		      line_least_square_fit_slope_impose_on_ds(dsi, ex_noise, impose_stretching, &b, &chi2);
		      a = impose_stretching;
		    }
		  else
		    line_least_square_fit_on_ds_between_indexes_chi2(dsi, 1, 0, noligos, ex_noise, &a, &b, &chi2);
		}
	      if (dsfit)
		{
		  if (fix_a)
		    {
		      line_least_square_fit_slope_impose_on_ds(dsfit, ex_noise, impose_stretching, &mv_b, &mv_chi2);
		      mv_a = impose_stretching;
		    }
		  else
		    line_least_square_fit_on_ds_between_indexes_chi2(dsfit, 1, 0, noligos, ex_noise, &mv_a, &mv_b, &mv_chi2);
		}
	      set_ds_source(dsi,"chi2 %g \n%s",chi2,mes);
	      set_ds_source(dsfit,"chi2 %g",mv_chi2);

	      //win_printf("iter %d chi2 %g chi2 mv %g",iter,chi2,mv_chi2);
	      if (mv_chi2 >= chi2)
		{
		  testp = ran1(&idum);
		  prob = (tempt > 0) ? exp(-(mv_chi2-chi2)/(noligos*tempt)) : 0;
		  if (prob > testp)
		    {
		      //win_printf("Start %d start2 %d start3 %d\n"
		      //	 "Deb1 %d end1 %d deb2 %d end2 %d\n"
		      //	 ,start, start2, start3, deb1, end1, deb2, end2);

		      do_swap(seq, end, olipos, olipos_t, noligos, olis, deb1, end1, deb2, end2, olipos_er, olipos_t_er, position_er_available);
		      mvu++;

		      dstmp = convert_seq_to_ds(olipos, noligos, olipos_er, position_er_available);
		      if (dstmp)
			{
			  if (fix_a)
			    {
			      line_least_square_fit_slope_impose_on_ds(dstmp, ex_noise, impose_stretching, &b, &chi2);
			      a = impose_stretching;
			    }
			  else
			    line_least_square_fit_on_ds_between_indexes_chi2(dstmp, 1, 0, noligos, ex_noise, &a, &b, &chi2);
			}
		      if (mv_chi2 != chi2)
			{
			  win_printf("Chi2 differs mv %g and %g",mv_chi2,chi2);
			  opn = create_one_empty_plot();
			  if (opn == NULL) return win_printf_OK("Cannot allocate plot");
			  add_data_to_pltreg(pr, IS_ONE_PLOT, (void*)opn);
			  add_one_plot_data(opn, IS_DATA_SET, (void*)dsi);
			  add_one_plot_data(opn, IS_DATA_SET, (void*)dsfit);
			  add_one_plot_data(opn, IS_DATA_SET, (void*)dstmp);
			  set_ds_source(dstmp,"chi2 %g",chi2);
			  return 0;
			}
		      free_data_set(dstmp);

		      if (op->n_dat > 0)
			{
			  free_data_set(op->dat[0]);
			  op->n_dat = (op->n_dat > 0) ? op->n_dat-1 : op->n_dat;
			}
		      add_one_plot_data(op, IS_DATA_SET, (void*)dsfit);
		      set_ds_source(dsfit,"\\chi^2=%g",mv_chi2);
		      set_plot_title(op,"\\stack{{Iter %d \\chi^2 = %g, min %g}"
				     "{\\pt7 up %d dwn %d noamb %d ebig %d T %g}}"
				     ,iter,mv_chi2,chi2_min[0],mvu,mvd,noamb,ebig,tempt);
		      op->need_to_refresh = 1;
		      refresh_plot(pr, pr->n_op - 1);
		      add_new_point_to_ds(dsc, iter, mv_chi2);
		    }
		  else
		    {
		      free_data_set(dsfit);
		      dsfit = NULL;
		    }
		}
	      else
		{
		  //win_printf("Start %d start2 %d start3 %d\n"
		  //	     "Deb1 %d end1 %d deb2 %d end2 %d\n"
		  //	     ,start, start2, start3, deb1, end1, deb2, end2);

		  do_swap(seq, end, olipos, olipos_t, noligos, olis, deb1, end1, deb2, end2, olipos_er, olipos_t_er, position_er_available);
		  mvd++;
		  dstmp = convert_seq_to_ds(olipos, noligos, olipos_er, position_er_available);
		  if (dstmp)
		    {
		      if (fix_a)
			{
			  line_least_square_fit_slope_impose_on_ds(dstmp, ex_noise, impose_stretching, &b, &chi2);
			  a = impose_stretching;
			}
		      else
			line_least_square_fit_on_ds_between_indexes_chi2(dstmp, 1, 0, noligos, ex_noise, &a, &b, &chi2);
		    }

		  if (mv_chi2 != chi2)
		    {
		      win_printf("Chi2 differs mv %g and %g",mv_chi2,chi2);
		      opn = create_one_empty_plot();
		      if (opn == NULL) return win_printf_OK("Cannot allocate plot");
		      add_data_to_pltreg(pr, IS_ONE_PLOT, (void*)opn);
		      add_one_plot_data(opn, IS_DATA_SET, (void*)dsi);
		      add_one_plot_data(opn, IS_DATA_SET, (void*)dsfit);
		      add_one_plot_data(opn, IS_DATA_SET, (void*)dstmp);
		      set_ds_source(dstmp,"chi2 %g",chi2);
		      return 0;
		    }
		  free_data_set(dstmp);
		  if ((dss != NULL) && (mv_chi2 < th_chi2))
		    {
		      for (i = 0; i < dss->ny; i++)
			{
			  if (((float)mv_chi2) == dss->yd[i]) break;
			}
		      if (i == 0 || i >= dss->nx)
			{
			  add_new_point_to_ds(dss, iter, mv_chi2);
			  if (fp) fprintf(fp,"Chi2 = %g; iter %d Seq: %s\n",mv_chi2,iter,seq);
			}
		    }
		  add_new_point_to_ds(dsc, iter, mv_chi2);
		  for (i = 0; i < 10; i++)
		    {
		      if (((float)mv_chi2) <= chi2_min[i])
			  break;
		    }
		  k = i;
		  if (k < 10 &&  ((float)mv_chi2) < chi2_min[k])
		    {
		      for (i = 9; i > k; i--)
			{
			  for (oi = 0; bestseq[i-1][oi] != 0; oi++)
			    bestseq[i][oi] = bestseq[i-1][oi];
			  bestseq[i][oi] = 0;
			  for (oi = 0; oi < noligos; oi++)
			    {
			      oliposb[i][oi] = oliposb[i-1][oi];
			      oliposb_er[i][oi] = oliposb_er[i-1][oi];
			    }
			  chi2_min[i] = chi2_min[i-1];
			  bchi2_i[i] = bchi2_i[i-1];
			}
		      for (oi = 0; seq[oi] != 0; oi++)
			bestseq[k][oi] = seq[oi];
		      bestseq[k][oi] = 0;
		      for (oi = 0; oi < noligos; oi++)
			{
			  oliposb[k][oi] = olipos[oi];
			  oliposb_er[k][oi] = olipos_er[oi];
			}
		      chi2_min[k] = mv_chi2;
		      bchi2_i[k] = iter;;
		    }
		  if (op->n_dat > 0)
		    {
		      free_data_set(op->dat[0]);
		      op->n_dat = (op->n_dat > 0) ? op->n_dat-1 : op->n_dat;
		    }
		  add_one_plot_data(op, IS_DATA_SET, (void*)dsfit);
		  set_ds_source(dsfit,"\\chi^2=%g",mv_chi2);
		  set_plot_title(op,"\\stack{{Iter %d \\chi^2 = %g, min %g}"
				 "{\\pt7 up %d dwn %d noamb %d ebig %d T %g}}"
				 ,iter,mv_chi2,chi2_min[0],mvu,mvd,noamb,ebig,tempt);
		  op->need_to_refresh = 1;
		  refresh_plot(pr, pr->n_op - 1);
		}
	      free_data_set(dsi);
	      dsi = NULL;
	      if (mes != NULL)	  free(mes);
	    }
	  else ebig++;
	}
      else noamb++; //win_printf("Could not find ambiguous sequences\n");
    }
   for (i = 0; i < 10; i++)
     {
       dsi = convert_seq_to_ds(oliposb[i], noligos, oliposb_er[i], position_er_available);
       add_one_plot_data(op, IS_DATA_SET, (void*)dsi);
       set_ds_source(dsi,"iter %d \\chi^2=%g Seq:\n%s\n",bchi2_i[k],chi2_min[i],bestseq[i]);
     }
   if (fp) fclose(fp);
  return 0;
}

	      //describe_swap(seq, end,  olipos, olipos_t, noligos, olis, deb1, end1, deb2, end2);
	      //if (win_scanf("Do this move ?") == WIN_OK)
	      //do_swap(seq, end, olipos, olipos_t, noligos, olis, deb1, end1, deb2, end2);


int do_fit_line_with_impose_slope(void)
{
    int i;
    pltreg *pr = NULL;
    O_p *op;
    d_s *dsi = NULL, *ds = NULL;
    static float ex_noise = 3, impose_stretching = 0.9;
    double b = 0, chi2 = 0;

    if (updating_menu_state != 0)
      {
	return D_O_K;
      }
    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr,&op,&dsi) != 3)
      return win_printf_OK("cannot find data at %s:%d", __FILE__, __LINE__);

    i = win_scanf("Give impose slope %5f and noise %5f\n"
		  ,&impose_stretching,&ex_noise);

    line_least_square_fit_slope_impose_on_ds(dsi, ex_noise, impose_stretching, &b, &chi2);
    if ((ds = create_and_attach_one_ds(op, dsi->nx, dsi->nx, 0)) == NULL)
      return win_printf_OK("cannot create plot at %s:%d", __FILE__, __LINE__);
    for (i = 0; i < ds->nx; i++)
      {
	ds->xd[i] = dsi->xd[i];
	ds->yd[i] = b + impose_stretching * dsi->xd[i];
      }
    set_ds_source(ds,"Linear fit with impose slope %f b = %f \\chi2 = %g"
		  ,impose_stretching,b,chi2);

    refresh_plot(pr, UNCHANGED);
    return 0;
}

int do_findSeq_ambiguous_oligo_assemble_plot(void)
{
  static int start = 0, max_s = 32, olis = 3, verb = 0, Emax = 100, savefile = 0, specific = 0, improv = 1, fix_a = 0;
  int end, deb1, deb2, end1, end2, i, j, k, li, oi, match, verbose, noligos = 0;
  char seed1[32] = {0}, seed2[32] = {0}, olild[16] = {0}, olild_1[16] = {0};
  static char seq[128] = {0};
  float olipos[128] = {0}, olipos_t[128] = {0};
  d_s *dsfit = NULL, *dsi = NULL;
  char pa1[32], inter[32], pa2[32], wrongseq[256], goodseq[256];
  int E, bper, il;
  static char seqfilename[2048] = {0};
  FILE *fp = NULL;
  double mv_a = 0, mv_b = 0, mv_chi2 = 0, a = 0, b = 0, chi2 = 0;
  static float ex_noise = 3, impose_stretching = 0.9;
  pltreg *pr = NULL;
  O_p *op;
  int kk, il2;
  float corr = 0, corr2 = 0, tmp;

  if (updating_menu_state != 0)
    {
      return D_O_K;
    }
  if (ac_grep(cur_ac_reg, "%pr", &pr) != 1) // we get the plot region
    return win_printf_OK("cannot find data at %s:%d", __FILE__, __LINE__);

  if (strlen(seq) == 0) sprintf(seq,"CTGACAGGGACCCTCTTGTATAGCAGCAGTTGTGCATTTGTTGCCACTCA");
  i = win_scanf("Look for ambiguous sequence in oligonucleodide assembly in\n"
		"%R->Use sequence below,\n%r->or load sequence from a file,\n"
		"%r->or load sequence from a file containing oligo position\n"
		"%s\n"
		"Noise expected %5f\n"
		"Oligomer size %4d, search size %4d\n"
		"Display event if their energy is bigger than %5d\n"
		"Impose the stretching %b at value %5f\n"
		"%b if oligo position, display only \\chi^2 favorable to move\n"
		"%b->Verbose; %b->save result in a file\n"
		,&specific,seq,&ex_noise,&olis,&max_s,&Emax,&fix_a,&impose_stretching, &improv,&verb,&savefile);
  if (i == WIN_CANCEL) return 0;


  if (specific == 1)
    {
      if (do_select_file(seqfilename, sizeof(seqfilename), "TXT-FILE", "*.txt", "File to load exact sequence", 1))
	return win_printf_OK("Cannot select output file");
      fp = fopen (seqfilename,"r");
      if (fp == NULL)
	return win_printf_OK("Impossible to open file %s",backslash_to_slash(seqfilename));
      for (i = 0, j = fgetc(fp); i < (int)sizeof(seq) && j != EOF; j = fgetc(fp))
	{
	  seq[i++] = (char)j;
	}
      seq[i] = 0;
      fclose(fp);
      win_printf("Seq:\n%s",seq);
    }
  if (specific == 2)
    {
      if (do_select_file(seqfilename, sizeof(seqfilename), "TXT-FILE", "*.txt", "File to load oligos and position", 1))
	return win_printf_OK("Cannot select output file");
      fp = fopen (seqfilename,"r");
      if (fp == NULL)
	return win_printf_OK("Impossible to open file %s",backslash_to_slash(seqfilename));
      for (i = 0, j = 2; i < (int)sizeof(seq) && j == 2; )
	{
	  j = fscanf(fp,"%s %f\n",olild,olipos+i);
	  //win_printf("Oligo %d ->%d read %s %g",i,j,olild,olipos[i]);
	  if (j == 2)
	    {
	      if (i == 0)
		{
		  for (li = 0; olild[li] != 0 && li < (int)sizeof(olild); li++)
		    seq[li] = olild[li];
		  if (li != olis) win_printf("Line %d:\noligo size %d differ from %d proposed",i,li,olis);
		  k = li;
		  for (li = 0; olild[li] != 0 && li < (int)sizeof(olild); li++)
		    olild_1[li] = olild[li];
		  olild_1[li] = 0;
		}
	      else
		{
		  for (li = 0; olild[li] != 0 && li < (int)sizeof(olild); li++);
		  if (li != olis) win_printf("Line %d:\noligo size %d differ from %d proposed",i,li,olis);
		  for (oi = 0, match = 0; (oi < olis - 1); oi++)
		    match += (olild_1[oi+1] == olild[oi]) ? 1 : 0;
		  if (match != olis - 1)
		    win_printf("Line %d: match %d\noligo %s does not continue oligo %s"
			       ,i,match,olild,olild_1);
		  li = (li > 0) ? li - 1: 0;
		  seq[k++] = olild[li];
		  for (li = 0; olild[li] != 0 && li < (int)sizeof(olild); li++)
		    olild_1[li] = olild[li];
		  olild_1[li] = 0;
		}
	      i++;
	      noligos++;
	    }
	}
      //win_printf("n oligos %d seq length %d",noligos,k);

      for(il = 0; il < noligos; il++)
	{
	  for(il2 = il+1; il2 < noligos; il2++)
	    {
	      for (oi = 0, match = 0; oi < olis; oi++)
		match += (seq[il+oi] == seq[il2+oi]) ? 1 : 0;
	      if ((match == olis) && (olipos[il] > olipos[il2]))
		{
		  win_printf("swapp oligo %c%c%c at %d->%g and %d->%g"
		  	 ,seq[il],seq[il+1],seq[il+2],il,olipos[il]
		  	 ,il2,olipos[il2]);
		  tmp = olipos[il];
		  olipos[il] = olipos[il2];
		  olipos[il2] = tmp;
		}
	    }
	}


      dsi = build_data_set(noligos,noligos);
      dsfit = build_data_set(noligos,noligos);
      if (dsi == NULL || dsfit == NULL)
	return win_printf("Cannot create temp ds");
      for(il = 0; il < noligos; il++)
	{
	  dsi->xd[il] = il;
	  dsi->yd[il] = olipos[il];
	}
      dsi->nx = dsi->ny = noligos;
      op = create_one_empty_plot();
      if (op == NULL) return win_printf_OK("Cannot allocate plot");
      add_data_to_pltreg(pr, IS_ONE_PLOT, (void*)op);

      add_one_plot_data(op, IS_DATA_SET, (void*)dsi);
      if (fix_a)
	{
	  line_least_square_fit_slope_impose_on_ds(dsi, ex_noise, impose_stretching, &b, &chi2);
	  a = impose_stretching;
	}
      else
	line_least_square_fit_on_ds_between_indexes_chi2(dsi, 1, 0, noligos, ex_noise, &a, &b, &chi2);
      seq[k] = 0;
      fclose(fp);
      win_printf("Seq:\n%s\nLength %d and %d oligos ",seq,k,noligos);
    }

  if (savefile)
    {
      if (do_select_file(seqfilename, sizeof(seqfilename), "TXT-FILE", "*.txt", "File to save ambiguous sequence", 0))
	return win_printf_OK("Cannot select output file");
      fp = fopen (seqfilename,"w");
      if (fp == NULL)  win_printf_OK("Cannot open file:\n%s",backslash_to_slash(seqfilename));
      fprintf(fp,"%s original sequence\n",seq);
    }


  verbose = (verb == 0) ? WIN_CANCEL : 0;
  end = strlen(seq);
  for (start = 0; start < end - olis; start++)
    {
      // we grab the first sead on the left
      for (oi = 0; (oi < olis-1) && (start+oi < end); oi++)
	seed1[oi] = seq[start+oi];
      seed1[oi] = 0;
      //if (verbose != WIN_CANCEL)
      //verbose = win_printf("start = %d seed1 %s len %d",start,seed1,end);
      for (i = 1; (i < max_s) && ((start+olis-2+i) < end); i++)
	{// is there a maching possibility nearby ?
	  for (oi = 0, match = 0; (oi < olis-1) && (start+oi+i < end); oi++)
	    match += (seed1[oi] == seq[start+oi+i]) ? 1 : 0;
	  if (match == olis-1) break;
	}
      if (i == max_s) continue; // no matching stuff in range
      if (match < olis-1) continue;
      deb1 = start;
      deb2 = start+i;// we save position of  matching stuff
      deb2 = (deb2 < 0) ? 0 : deb2;
      deb2 = (deb2 < end) ? deb2 : end-1;
      if (verbose != WIN_CANCEL)
	verbose = win_printf("found seed1 %s in deb1=%d and deb2=%d\n"
		 "%c%c and %c%c\n"
		 ,seed1,deb1,deb2,seq[deb1],seq[deb1+1]
		 ,seq[deb2],seq[deb2+1]);
      for (i = deb1+1; i <= deb2; i++)
	{
	  // we grab the second sead on the right
	  for (oi = 0; oi < olis-1 && (i+oi < end); oi++)
	    seed2[oi] = seq[i+oi];
	  seed2[oi] = 0;
	  if (verbose != WIN_CANCEL)
	    verbose = win_printf("i = %d seed2 %s, deb2 %d",i,seed2,deb2);
	  //deb2 = (deb2 < 0) ? 0 : deb2;
	  //deb2 = (deb2 < end) ? deb2 : end-1;
	  for (k = olis-1; (k < max_s) && ((olis-2+deb2+k) < end); k++)
	    {// is there a second maching possibility
	      for (oi = 0, match = 0; (oi < olis-1) && (oi+deb2+k < end); oi++)
		{
		  kk = oi+deb2+k;
		  if (kk < 0 || kk >= end)
		    {
		      win_printf("kk = %d,  oi %d deb2 %d k %d",kk,oi,deb2,k);
		      continue;
		    }
		  match += (seed2[oi] == seq[kk]) ? 1 : 0;
		}
	      if (match == olis-1)
		{
		  if (verbose != WIN_CANCEL)
		    {
		      verbose = win_printf("found seed1 %s in deb1=%d and deb2=%d\n"
					   "%c%c and %c%c\n"
					   "match %d seed2 %s found in end1=%d and %c%c at end2=%d"
					   ,seed1,deb1,deb2,seq[deb1],seq[deb1+1]
					   ,seq[deb2],seq[deb2+1]
					   ,match,seed2,i,seq[deb2+k],seq[1+deb2+k],deb2+k
					   );
		    }
		  break;
		}
	    }
	  if (k == max_s) continue; // no matching stuff in range
	  if (match < olis-1) continue;
	  end1 = i;
	  end2 = deb2 + k;
	  if (end1 - deb1 < 1) continue;
	  if (end2 - deb2 < 1) continue;
	  if (end2 >= end) continue;
	  for (oi = 0; oi < end1 - deb1+olis-1; oi++)
	    pa1[oi] = (oi < olis-1) ? 0x20 | seq[deb1+oi] : 0xDF&seq[deb1+oi];
	  pa1[oi] = 0;
	  for (oi = 0; (oi < deb2 - end1) && (end1+olis-1+oi < end); oi++)
	    inter[oi] = seq[end1+olis-1+oi];
	  inter[oi] = 0;
	  for (oi = 0; oi < end2 - deb2+olis-1; oi++)
	    pa2[oi] = (oi < olis-1) ? 0x20 | seq[deb2+oi] : 0xDF&seq[deb2+oi];
	  pa2[oi] = 0;
	  for (oi = 0; oi < end2 - deb2+olis-1; oi++)
	    wrongseq[oi] = seq[deb2+oi];
	  k = oi;
	  for (oi = 0; (oi < deb2 - end1) && (end1+olis-1+oi < end); oi++)
	    wrongseq[k+oi] = seq[end1+olis-1+oi];
	  k += oi;
	  for (oi = 0; oi < end1 - deb1; oi++)
	    wrongseq[k+oi] = seq[deb1+oi+olis-1];
	  wrongseq[k+oi] = 0;

	  for (oi = 0; oi < end2 - deb1+olis-1; oi++)
	    goodseq[oi] = seq[deb1+oi];
	  goodseq[oi] = 0;
	  for (oi = 0, bper = 0; goodseq[oi] != 0 && wrongseq[oi] != 0; oi++)
	    bper += (goodseq[oi] != wrongseq[oi]) ? 1 : 0;
	  if (strlen(pa1) != strlen(pa2))
	    {
	      match = 0;
	    }
	  else
	    {
	      for (oi = 0, match = 0; pa1[oi] > 0 && pa2[oi] > 0; oi++)
		match += (pa2[oi] == pa1[oi]) ? 1 : 0;
	    }
	  if (match == (int)strlen(pa1)) continue;
	  E = (end1 - deb1) * (end2 - end1) * (end2 - end1);
	  E += (deb2 - end1) * (end2 - deb2 - end1 + deb1) * (end2 - deb2 - end1 + deb1);
	  E += (end2 - deb2) * (deb1 - deb2) * (deb1 - deb2);
	  if (E < Emax)
	    {
 	      if (noligos > 0)
		{
		  for(il = 0; il < deb1; il++)
		    olipos_t[il] = olipos[il];
		  for(il = deb1; il < end1; il++)
		    olipos_t[end2-end1+il] = olipos[il];
		  for(il = end1; il < deb2; il++)
		    olipos_t[end2-end1-deb2+deb1+il] = olipos[il];
		  for(il = deb2; il < end2; il++)
		    olipos_t[deb1-deb2+il] = olipos[il];
		  for(il = end2; il < noligos; il++)
		    olipos_t[il] = olipos[il];
		  for(il = deb1; il < end2; il++)
		    {
		      for(il2 = il+1; il2 < end2; il2++)
			{
			  for (oi = 0, match = 0; oi < olis; oi++)
			    match += (seq[il+oi] == seq[il2+oi]) ? 1 : 0;
			  if ((match == olis) && (olipos_t[il] > olipos_t[il2]))
			    {
			      //win_printf("swapp oligo %c%c%c at %d->%g and %d->%g"
			      //	 ,seq[il],seq[il+1],seq[il+2],il,olipos_t[il]
			      //	 ,il2,olipos_t[il2]);
			      tmp = olipos_t[il];
			      olipos_t[il] = olipos_t[il2];
			      olipos_t[il2] = tmp;
			    }
			}
		    }


		  for(il = 0; il < noligos; il++)
		    {
		      dsfit->xd[il] = il;
		      dsfit->yd[il] = olipos_t[il];
		    }
		  dsfit->nx = dsfit->ny = noligos;
		  if (fix_a)
		    {
		      line_least_square_fit_slope_impose_on_ds(dsfit, ex_noise, impose_stretching, &mv_b, &mv_chi2);
		      mv_a = impose_stretching;
		    }
		  else
		    line_least_square_fit_on_ds_between_indexes_chi2(dsfit, 1, 0, noligos, ex_noise, &mv_a, &mv_b, &mv_chi2);
		}
	      int res = WIN_CANCEL; d_s *dstmp = NULL;

	      for(il = deb1, corr = corr2 = 0; il < end1; il++)
		{
		  tmp = a * il + b;
		  corr += (olipos[il] - tmp) * (end2 - end1);
		  corr2 += (olipos_t[il] - tmp) * (end2 - end1);
		}
	      for(il = end1; il < deb2; il++)
		{
		   tmp = a * il + b;
		   corr += (olipos[il] - tmp) * (end2 - deb2 - end1 + deb1);
		   corr2 += (olipos_t[il] - tmp) * (end2 - deb2 - end1 + deb1);
		}
	      for(il = deb2; il < end2; il++)
		{
		   tmp = a * il + b;
		   corr += (olipos[il] - tmp) * (deb1 - deb2);
		   corr2 += (olipos_t[il] - tmp) * (deb1 - deb2);
		}



	      if ((noligos == 0) || (mv_chi2*improv <= chi2*improv) || (corr2*improv <= corr*improv))
		res = win_printf("match %d found ambiguous sequence at [%d, %d[ and [%d, %d[\n"
			   "seed1 %s seed2 %s; start=%d; deb1=%d; end1=%d; deb2=%d; end2=%d\n"
			   "sequence %s-%s-%s E = %d\n"
			   "%dx(E1=%d) + %dx(E_{inter}=%d) + %dx(E2=%d)\n"
			   "%s ->good\n%s ->bad %d errors\n"
			   "original seq a = %g b = %g \\chi^2 = %g; coorelation %g\n"
			   "swapped seq a = %g b = %g \\chi^2 = %g; coorelation %g\n"
			   ,match,deb1,end1+olis-1,deb2, end2+olis-1
			   , seed1,seed2,start,deb1,end1,deb2,end2
			   , pa1,inter,pa2, E
			   ,(end1 - deb1), (end2 - end1) * (end2 - end1)
			   ,(deb2 - end1), (end2 - deb2 - end1 + deb1) * (end2 - deb2 - end1 + deb1)
			   ,(end2 - deb2), (deb1 - deb2) * (deb1 - deb2),goodseq,wrongseq,bper
				 ,a,b,chi2,corr,mv_a,mv_b,mv_chi2,corr2);
	      if (res == WIN_OK)
		{
		  dstmp = duplicate_data_set(dsfit,NULL);
		  add_one_plot_data(op, IS_DATA_SET, (void*)dstmp);
		  set_ds_source(dstmp,"match %d found ambiguous sequence at [%d, %d[ and [%d, %d[\n"
				"seed1 %s seed2 %s; start=%d; deb1=%d; end1=%d; deb2=%d; end2=%d\n"
				"sequence %s-%s-%s E = %d\n"
				"%dx(E1=%d) + %dx(E_{inter}=%d) + %dx(E2=%d)\n"
				"%s ->good\n%s ->bad %d errors\n"
				"original seq a = %g b = %g \\chi^2 = %g; coorelation %g\n"
				"swapped seq a = %g b = %g \\chi^2 = %g; coorelation %g\n"
				,match,deb1,end1+olis-1,deb2, end2+olis-1
				, seed1,seed2,start,deb1,end1,deb2,end2
				, pa1,inter,pa2, E
				,(end1 - deb1), (end2 - end1) * (end2 - end1)
				,(deb2 - end1), (end2 - deb2 - end1 + deb1) * (end2 - deb2 - end1 + deb1)
				,(end2 - deb2), (deb1 - deb2) * (deb1 - deb2),goodseq,wrongseq,bper
				,a,b,chi2,corr,mv_a,mv_b,mv_chi2,corr2);

		  for(il = 0; il < deb1; il++)
		    olipos_t[il] = il;
		  for(il = deb1; il < end1; il++)
		    olipos_t[end2-end1+il] = il;
		  for(il = end1; il < deb2; il++)
		    olipos_t[end2-end1-deb2+deb1+il] = il;
		  for(il = deb2; il < end2; il++)
		    olipos_t[deb1-deb2+il] = il;
		  for(il = end2; il < noligos; il++)
		    olipos_t[il] = il;
		  for(il = 0; il < noligos; il++)
		    {
		      dsfit->xd[il] = il;
		      dsfit->yd[il] = olipos_t[il];
		    }
		  dsfit->nx = dsfit->ny = noligos;
		  dstmp = duplicate_data_set(dsfit,NULL);
		  add_one_plot_data(op, IS_DATA_SET, (void*)dstmp);
		  res = WIN_CANCEL;
		}
	      if (fp != NULL)
		{
		  for(oi = 0; oi < start; oi++) fprintf(fp,"%c",seq[oi]);
		  fprintf(fp," %s-%s-%s ", pa1,inter,pa2);
		  for(oi = end2+olis-1; oi < end; oi++) fprintf(fp,"%c",seq[oi]);
		  fprintf(fp," %d-%d-%d E=%d\n",(end1 - deb1),(deb2 - end1),(end2 - deb2),E);
		}

	    }
	}

    }
  if (fp != NULL) fclose(fp);
  return 0;
}


int do_findSeq_ambiguous_oligo_assemble_plot_of_fasta(void)
{
    static int start = 0, max_s = 32, olis = 3, verb = 0, Emax = 100, savefile = 0, usecor = 0, NbAlt = 0;
  int end, deb1, deb2, end1, end2, i, k, oi, match, verbose;
  char seed1[32] = {0}, seed2[32] = {0};
  static char seq[128] = {0};
  char pa1[32], inter[32], pa2[32], wrongseq[256], goodseq[256];
  int E, bper;
  static char seqfilename[2048] = {0};
  FILE *fp = NULL;
  O_p *opn = NULL;
  d_s *ds = NULL, *ds2 = NULL;
  pltreg *pr = NULL;
  static float erbp = 2, thres = 0.05;
  float tmp, ercor, erquad;

  if (updating_menu_state != 0)
    {
        // we wait for a valid sequence to be loaded
        if (lsqd == NULL) active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }
  i = win_scanf("Look for ambiguous sequence in oligonucleodide assembly in\n"
                "recorded fasta sequence\n"
                "Oligomer size %4d, search size %4d\n"
                "Specify error in Z in bp %5f\n"
                "Error detection %R->quadratic %r->correlation\n"
                "Display event if their energy is bigger than %5d\n"
                "%b->Verbose; %b->save result in a file\n"
                "%b Add a plot with number of alternatives with proba > %4f\n"
                ,&olis,&max_s,&erbp,&usecor,&Emax,&verb,&savefile,&NbAlt,&thres);
  if (i == WIN_CANCEL) return 0;

  if (savefile)
    {
      if (do_select_file(seqfilename, sizeof(seqfilename), "TXT-FILE", "*.txt", "File to save ambiguous sequence", 0))
	return win_printf_OK("Cannot select output file");
      fp = fopen (seqfilename,"w");
      if (fp == NULL)  win_printf_OK("Cannot open file:\n%s",backslash_to_slash(seqfilename));
      fprintf(fp,"%s original sequence\n",seq);
    }



  int ia, l2, pm;
  verbose = (verb == 0) ? WIN_CANCEL : 0;
  end = lsqd->totalSize;
  l2 = lsqd->ln2PageSize;
  pm = lsqd->pageMask;

  if (ac_grep(cur_ac_reg, "%pr", &pr) != 1) // we get the plot region
    return win_printf_OK("cannot find data at %s:%d", __FILE__, __LINE__);

  if ((opn = create_and_attach_one_plot(pr, end, end, 0)) == NULL)
    return win_printf_OK("cannot create plot at %s:%d", __FILE__, __LINE__);

  ds = opn->dat[0];
  opn->need_to_refresh = 1;
  if (NbAlt)
      {
          if ((ds2 = create_and_attach_one_ds(opn, end, end, 0)) == NULL)
              return win_printf_OK("cannot create plot !");
          set_ds_source(ds2,"Number of alternative paths with Proba > %g",thres);
      }

  int type;
  for (start = 0; start < end - olis; start++)
    {
      // we grab the first sead on the left
      for (oi = 0; oi < olis-1; oi++)
          {
              ia = start+oi;
              seed1[oi] = lsqd->seq[ia >> l2][ia & pm];
          }
      seed1[oi] = 0;
      if (verbose != WIN_CANCEL)
          verbose = win_printf("start = %d seed1 %s len %d",start,seed1,end);
      for (i = 1; (i < max_s) && ((start+olis-2+i) < end); i++)
          {// is there a maching possibility nearby ?
              for (oi = 0, match = 0; oi < olis-1; oi++)
                  {
                      ia = start+oi+i;
                      match += (seed1[oi] == lsqd->seq[ia >> l2][ia & pm]) ? 1 : 0;
                  }
              if (match == olis-1) break;
          }
      if (i == max_s) continue; // no matching stuff in range
      if (match < olis-1) continue;
      deb1 = 0;
      deb2 = i;// we save position of  matching stuff
      if (verbose != WIN_CANCEL)
          verbose = win_printf("found seed1 %s in deb1=%d and deb2=%d\n"
                               "%c%c and %c%c\n"
                               ,seed1,start+deb1,start+deb2
                               ,lsqd->seq[(start+deb1)>>l2][(start+deb1) & pm]
                               ,lsqd->seq[(start+deb1+1) >> l2][(start+deb1+1) & pm]
                               ,lsqd->seq[(start+deb2) >> l2][(start+deb2) & pm]
                               ,lsqd->seq[(start+deb2+1) >> l2][(start+deb2+1) & pm]);
      for (i = 1; i <= deb2; i++)
          {
              // we grab the second sead on the right
              for (oi = 0; oi < olis-1; oi++)
                  {
                      ia = start+i+oi;
                      seed2[oi] = lsqd->seq[ia >> l2][ia & pm];
                  }
              seed2[oi] = 0;
              if (verbose != WIN_CANCEL)
                  verbose = win_printf("i = %d seed2 %s, deb2 %d",i,seed2,deb2);
              for (k = olis-1; (k < max_s) && ((start+olis-2+deb2+k) < end); k++)
                  {// is there a second maching possibility
                      for (oi = 0, match = 0; oi < olis-1; oi++)
                          {
                              ia = start+oi+deb2+k;
                              match += (seed2[oi] == lsqd->seq[ia >> l2][ia & pm]) ? 1 : 0;
                          }
                      if (match == olis-1)
                          {
                              if (verbose != WIN_CANCEL)
                                  verbose = win_printf("match %d found %c%c at %d",match
                                                       ,lsqd->seq[(start+deb2+k) >> l2][(start+deb2+k) & pm]
                                                       ,lsqd->seq[(start+1+deb2+k) >> l2][(start+1+deb2+k) & pm]
                                                       ,start+deb2+k);
                              break;
                          }
                  }
              if (k == max_s) continue; // no matching stuff in range
              if (match < olis-1) continue;
              end1 = i;
              end2 = deb2 + k;
              if (start + end2 >= end) continue;
              for (oi = 0; oi < end1 - deb1+olis-1; oi++)
                  {
                      ia = start+deb1+oi;
                      pa1[oi] = (oi < olis-1) ? 0x20 | lsqd->seq[ia>>l2][ia&pm] : lsqd->seq[ia>>l2][ia&pm];
                  }
              pa1[oi] = 0;
              for (oi = 0; (oi < deb2 - end1) && (start+end1+olis-1+oi < end); oi++)
                  {
                      ia = start+end1+olis-1+oi;
                      inter[oi] = lsqd->seq[ia>>l2][ia&pm];
                  }
              inter[oi] = 0;
              for (oi = 0; oi < end2 - deb2+olis-1; oi++)
                  {
                      ia = start+deb2+oi;
                      pa2[oi] = (oi < olis-1) ? 0x20 | lsqd->seq[ia>>l2][ia&pm] : lsqd->seq[ia>>l2][ia&pm];
                  }
              pa2[oi] = 0;
              for (oi = 0; oi < end2 - deb2+olis-1; oi++)
                  {
                      ia = start+deb2+oi;
                      wrongseq[oi] = lsqd->seq[ia>>l2][ia&pm];
                  }
              k = oi;
              for (oi = 0; (oi < deb2 - end1) && (start+end1+olis-1+oi < end); oi++)
                  {
                      ia = start+end1+olis-1+oi;
                      wrongseq[k+oi] = lsqd->seq[ia>>l2][ia&pm];
                  }
              k += oi;
              for (oi = 0; oi < end1 - deb1; oi++)
                  {
                      ia = start+deb1+oi+olis-1;
                      wrongseq[k+oi] = lsqd->seq[ia>>l2][ia&pm];
                  }
              wrongseq[k+oi] = 0;
              for (oi = 0; oi < end2 - deb1+olis-1; oi++)
                  {
                      ia = start+deb1+oi;
                      goodseq[oi] = lsqd->seq[ia>>l2][ia&pm];
                  }
              goodseq[oi] = 0;
              for (oi = 0, bper = 0; goodseq[oi] != 0 && wrongseq[oi] != 0; oi++)
                  bper += (goodseq[oi] != wrongseq[oi]) ? 1 : 0;
              if (strlen(pa1) != strlen(pa2))
                  {
                      match = 0;
                  }
              else
                  {
                      for (oi = 0, match = 0; pa1[oi] > 0 && pa2[oi] > 0; oi++)
                          match += (pa2[oi] == pa1[oi]) ? 1 : 0;
                  }
              if (match == (int)strlen(pa1)) continue;
              E = (end1 - deb1) * (end2 - end1) * (end2 - end1);
              E += (deb2 - end1) * (end2 - deb2 - end1 + deb1) * (end2 - deb2 - end1 + deb1);
              E += (end2 - deb2) * (deb1 - deb2) * (deb1 - deb2);
              if (E < Emax)
                  {
                      if (verbose != WIN_CANCEL)
                          verbose = win_printf("match %d found ambiguous sequence at [%d, %d[ and [%d, %d[\n"
                                               "seed1 %s seed2 %s; start=%d; deb1=%d; end1=%d; deb2=%d; end2=%d\n"
                                               "sequence %s-%s-%s E = %d\n"
                                               "%dx(E1=%d) + %dx(E_{inter}=%d) + %dx(E2=%d)\n"
                                               "%s ->good\n%s ->bad %d errors\n"
                                               ,match,start+deb1,start+end1+olis-1,start+deb2, start+end2+olis-1
                                               , seed1,seed2,start,deb1,end1,deb2,end2
                                               , pa1,inter,pa2, E
                                               ,(end1 - deb1), (end2 - end1) * (end2 - end1)
                                               ,(deb2 - end1), (end2 - deb2 - end1 + deb1) * (end2 - deb2 - end1 + deb1)
                                               ,(end2 - deb2), (deb1 - deb2) * (deb1 - deb2),goodseq,wrongseq,bper);
                      type = ((end1 - deb1) < 9) ? (end1 - deb1) : 9;
                      type += 10 * (((deb2 - end1) < 9) ? (deb2 - end1) : 9);
                      type += 100 * (((end2 - deb2) < 9) ? (end2 - deb2) : 9);

                      tmp = sqrt(E);
                      erquad = (1 - erf(fabs(tmp)/(2*erbp*sqrt(2))))/2;
                      ercor = (1 - erf(fabs(tmp)/(erbp*sqrt(2))))/2;

                      for (oi = 0; goodseq[oi] != 0 && wrongseq[oi] != 0; oi++)
                          {
                              if (goodseq[oi] != wrongseq[oi])
                                  {
                                      ds->yd[start+oi] += (usecor) ? ercor : erquad;
                                      /*
                                        if ((int)ds->xd[start+oi] == 0 || E < (int)ds->xd[start+oi])
                                        {
                                        ds->xd[start+oi] = E;
                                        ds->yd[start+oi] = type;
                                        }
                                      */
                                  }
                              if (ds2 != NULL)
                                  {
                                      tmp = (usecor) ? ercor : erquad;
                                      if (tmp > thres)
                                          ds2->yd[start+oi] += 1;
                                  }
                          }

                      if (fp != NULL)
                          {
                              for(oi = (start > 5) ? start -5 : 0; oi < start; oi++) fprintf(fp,"%c",lsqd->seq[oi>>l2][oi&pm]);
                              fprintf(fp," %s-%s-%s ", pa1,inter,pa2);
                              for(oi = start+end2+olis-1; oi < end && oi < start+end2+olis+4; oi++) fprintf(fp,"%c",lsqd->seq[oi>>l2][oi&pm]);
                              fprintf(fp," %d-%d-%d E=%d\n",(end1 - deb1),(deb2 - end1),(end2 - deb2),E);
                          }

                  }
          }

    }
  set_ds_treatement(ds,"Using %s detection",(usecor)?"correlation":"quadratique");
  set_plot_title(opn,"\\pt7\\stack{{For seq. %s error vs noise plot}"
                 "{noise %g oligo size %d, max search size %d}"
                 "{Using %s detection, Emax = %d}}"
                 ,lsqd->description,erbp,olis,max_s
                 ,(usecor)?"correlation":"quadratique",Emax);
  for (start = 0; start < end; start++)
      {
          ds->xd[start] = start;
          if (ds2 != NULL) ds2->xd[start] = start;
      }
  if (fp != NULL) fclose(fp);
  refresh_plot(pr, pr->n_op-1);
  return 0;
}


int do_findSeq_ambiguous_2_strands_oligo_assemble_plot_of_fasta(void)
{
    static int start = 0, max_s = 32, olis = 3, verb = 0, Emax = 100, savefile = 0, usecor = 0, NbAlt = 0;
  int end, deb1, end1, i,  oi, match, verbose;
  char seed1[32] = {0}, comp_rev_seed1[32] = {0};
  static char seq[128] = {0};
  char wrongseq[256], goodseq[256];
  int bper;
  float E;
  static char seqfilename[2048] = {0};
  FILE *fp = NULL;
  O_p *opn = NULL;
  d_s *ds = NULL, *ds2 = NULL;
  pltreg *pr = NULL;
  static float erbp = 2, thres = 0.05;
  float tmp, ercor, erquad;

  if (updating_menu_state != 0)
    {
        // we wait for a valid sequence to be loaded
        if (lsqd == NULL) active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }
  i = win_scanf("Look for ambiguous sequence in 2 strand oligonucleodide assembly in\n"
                "recorded fasta sequence\n"
                "Oligomer size %4d, search size %4d\n"
                "Specify error in Z in bp %5f\n"
                "Error detection %R->quadratic %r->correlation\n"
                "Display event if their energy is bigger than %5d\n"
                "%b->Verbose; %b->save result in a file\n"
                "%b Add number of path > %4f\n"
                ,&olis,&max_s,&erbp,&usecor,&Emax,&verb,&savefile,&NbAlt,&thres);
  if (i == WIN_CANCEL) return 0;

  if (savefile)
    {
      if (do_select_file(seqfilename, sizeof(seqfilename), "TXT-FILE", "*.txt", "File to save ambiguous sequence", 0))
	return win_printf_OK("Cannot select output file");
      fp = fopen (seqfilename,"w");
      if (fp == NULL)  win_printf_OK("Cannot open file:\n%s",backslash_to_slash(seqfilename));
      fprintf(fp,"%s original sequence\n",seq);
    }



  int ia, l2, pm;
  verbose = (verb == 0) ? WIN_CANCEL : 0;
  end = lsqd->totalSize;
  l2 = lsqd->ln2PageSize;
  pm = lsqd->pageMask;

  if (ac_grep(cur_ac_reg, "%pr", &pr) != 1) // we get the plot region
    return win_printf_OK("cannot find data at %s:%d", __FILE__, __LINE__);

  if ((opn = create_and_attach_one_plot(pr, end, end, 0)) == NULL)
    return win_printf_OK("cannot create plot at %s:%d", __FILE__, __LINE__);

  ds = opn->dat[0];
  opn->need_to_refresh = 1;
  if (NbAlt)
      {
          if ((ds2 = create_and_attach_one_ds(opn, end, end, 0)) == NULL)
              return win_printf_OK("cannot create plot !");
          set_ds_source(ds2,"Number of alternative paths with Proba > %g",thres);
      }

  for (start = 0; start < end - olis; start++)
    {
      // we grab the first sead on the left
      for (oi = 0; oi < olis-1; oi++)
          {
              ia = start+oi;
              seed1[oi] = lsqd->seq[ia >> l2][ia & pm];
          }
      seed1[oi] = 0;
      for (oi = 0; oi < olis-1; oi++)
          {
              if (seed1[olis-2-oi] == 'A')             comp_rev_seed1[oi] = 'T';
              else if (seed1[olis-2-oi] == 'C')        comp_rev_seed1[oi] = 'G';
              else if (seed1[olis-2-oi] == 'G')        comp_rev_seed1[oi] = 'C';
              else if (seed1[olis-2-oi] == 'T')        comp_rev_seed1[oi] = 'A';
              else win_printf("wrong base at %d ->%c",start+olis-2-oi,seed1[olis-2-oi]);
          }
      comp_rev_seed1[oi] = 0;
      if (verbose != WIN_CANCEL)
          verbose = win_printf("start = %d seed1 %s len %d looking for %s",start,seed1,end,comp_rev_seed1);
      for (i = 1; (i < max_s) && ((start+olis-2+i) < end); i++)
          {// is there a maching possibility nearby ?
              for (oi = 0, match = 0; oi < olis-1; oi++)
                  {
                      ia = start+oi+i;
                      match += (comp_rev_seed1[oi] == lsqd->seq[ia >> l2][ia & pm]) ? 1 : 0;
                  }
              if (match == olis-1) break;
          }
      if (i == max_s) continue; // no matching stuff in range
      if (match < olis-1) continue;
      deb1 = 0;
      end1 = i;// we save position of  matching stuff
      if (verbose != WIN_CANCEL)
          verbose = win_printf("found seed1 %s in deb1=%d and %s in end1=%d\n"
                               "%c%c and %c%c\n"
                               ,seed1,start+deb1,comp_rev_seed1,start+end1
                               ,lsqd->seq[(start+deb1)>>l2][(start+deb1) & pm]
                               ,lsqd->seq[(start+deb1+1) >> l2][(start+deb1+1) & pm]
                               ,lsqd->seq[(start+end1) >> l2][(start+end1) & pm]
                               ,lsqd->seq[(start+end1+1) >> l2][(start+end1+1) & pm]);


      for (oi = 0; oi < end1 - deb1+olis-1; oi++)
          {
              ia = start+deb1+oi;
              wrongseq[oi] = goodseq[oi] = lsqd->seq[ia>>l2][ia&pm];
          }
      wrongseq[oi] = goodseq[oi] = 0;

      for (oi = 2; oi < end1 - deb1; oi++)
          {
              if (goodseq[oi] == 'A')             wrongseq[oi] = 'T';
              else if (goodseq[oi] == 'C')        wrongseq[oi] = 'G';
              else if (goodseq[oi] == 'G')        wrongseq[oi] = 'C';
              else if (goodseq[oi] == 'T')        wrongseq[oi] = 'A';
              else win_printf("wrong base at %d ->%c",oi,goodseq[oi]);
          }
      for (oi = match = 0; oi < end1 - deb1+olis-1; oi++)
          {
              match += (goodseq[oi] == wrongseq[oi]) ? 1 : 0;
          }
      if (match == oi)
          {
              //if (verbose != WIN_CANCEL)
              //    verbose = win_printf("wrong %s and good %s equal !",wrongseq,goodseq);

              continue;
          }
      bper = oi - match;
      for (oi = 0, E = 0; oi < end1 - deb1; oi++)
          E += ((end1 - deb1 -1 - oi) - oi) * ((end1 - deb1 -1 - oi) - oi);
      //if (verbose != WIN_CANCEL)
      //  verbose = win_printf("wrong %s and good %s => E = %g !",wrongseq,goodseq,E);
      if (E < Emax)
          {
              if (verbose != WIN_CANCEL)
                  verbose = win_printf("match %d found ambiguous 2 strand sequence at [%d, %d[\n"
                                       "Good seq. %s\n"
                                       " Bad seq. %s\n"
                                       "E = %g and %d errors"
                                       ,match,start+deb1,start+end1+olis-1
                                       ,goodseq,wrongseq,E,bper);
              tmp = sqrt(E);
              erquad = (1 - erf(fabs(tmp)/(2*erbp*sqrt(2))))/2;
              ercor = (1 - erf(fabs(tmp)/(erbp*sqrt(2))))/2;
              for (oi = 0; goodseq[oi] != 0 && wrongseq[oi] != 0; oi++)
                  {
                      if (goodseq[oi] != wrongseq[oi])
                          {
                              ds->yd[start+oi] += (usecor) ? ercor : erquad;
                              /*
                                if ((int)ds->xd[start+oi] == 0 || E < (int)ds->xd[start+oi])
                                {
                                ds->xd[start+oi] = E;
                                ds->yd[start+oi] = type;
                                }
                              */
                          }
                      if (ds2 != NULL)
                          {
                              tmp = (usecor) ? ercor : erquad;
                              if (tmp > thres)
                                  ds2->yd[start+oi] += 1;
                          }
                      if (fp != NULL)
                          {
                              for(oi = (start > 5) ? start -5 : 0; oi < start; oi++) fprintf(fp,"%c",lsqd->seq[oi>>l2][oi&pm]);
                              fprintf(fp," %s/%s", goodseq, wrongseq);
                          }

                  }
          }

    }
  set_ds_treatement(ds,"Using %s detection",(usecor)?"correlation":"quadratique");
  set_plot_title(opn,"\\pt7\\stack{{For seq. %s error 2 strands vs noise plot}"
                 "{noise %g oligo size %d, max search size %d}"
                 "{Using %s detection, Emax = %d}}"
                 ,lsqd->description,erbp,olis,max_s
                 ,(usecor)?"correlation":"quadratique",Emax);
  for (start = 0; start < end; start++)
      {
          ds->xd[start] = start;
          if (ds2 != NULL) ds2->xd[start] = start;
      }
  if (fp != NULL) fclose(fp);
  refresh_plot(pr, pr->n_op-1);
  return 0;
}


int do_findSeq_oligo_plot(void)
{
    int i, j, i_1;
    O_p *opn = NULL, *op = NULL;
    int len, in, iseq, k, f1 = 0, f0 = 0, fn = 0, fi, nmax = 0, smax, st = 0, total, tmpi, n_oli, i_oli, rn_oli;
    static int start = 0, size = 16384, pairwise = 0, hits = 0;
    d_s *ds = NULL;
    pltreg *pr = NULL;
    char ch, complementary[128], *desc = NULL;
    static int all = 0, addplt = 0, seg_or_complementary = 0, hairpin = 0, noNs = 1, oliFromFile = 0, simu = 0, inv = 0;
    static float noise = 4.5, stretching = 0.91;
    char **loligo = NULL, *c_oligo = NULL, seqfilename[2048], good_oli[1024] = {0};
    FILE *fp = NULL;
    float dx;


    if (updating_menu_state != 0)
    {
        // we wait for a valid sequence to be loaded
        if (lsqd == NULL) active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }


    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine multiply the y coordinate"
                             "of a data set by a number and place it in a new plot");
    }

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr", &pr) != 1) // we get the plot region
        return win_printf_OK("cannot find data at %s:%d", __FILE__, __LINE__);

    i = win_scanf("Load oligos from file %b or define the sequence of oligo to find:\n5'-%64s-3'\n"
                  "Do you want to find this sequence%R or its complementary%r\n"
                  "Click here %b to find hybridization on both strands (hairpin),\n"
                  "%R->Display covering; %r just hits; %r hits +/- (differenciating strands)\n"
                  "Click here %b to display pair wise distribution\n"
                  "Forbid matching with Ns in sequence %b\n"
                  "Limit the plot size to %8d bases starting at %8d\n"
                  "Apply to all fasta sequences loaded %b\n"
                  "%R->Add to present data set; %r->add a new data set; %r->create a new plot\n"
                  "Chick here %b to simulate fingerprint with noise\n"
                  , &oliFromFile, oligo, &seg_or_complementary, &hairpin, &hits, &pairwise, &noNs, &size, &start, &all, &addplt, &simu);

    if (i == WIN_CANCEL) return OFF;

    if (oliFromFile)
    {
        n_oli = grab_oligos_from_file(&loligo);

        if (n_oli <= 0) return win_printf_OK("Pb loading oligos!");
    }
    else n_oli = 1;

    if (simu)
    {
        i = win_scanf("Specify the noise in bp to add to position %8f\n"
                      "Direct or %b inverted sequence; stretching %8f\n"
                      , &noise, &inv, &stretching);

        if (i == WIN_CANCEL) return OFF;

        seqfilename[0] = 0;

        if (do_select_file(seqfilename, sizeof(seqfilename), "FINGERPRINT File", "sfgp\0", "File to save fingerprint\0", 0))
            return win_printf_OK("Cannot select output file");

        fp = fopen(seqfilename, "w");

        if (fp == NULL)  win_printf_OK("Cannot open file:\n%s", backslash_to_slash(seqfilename));
    }

    if (addplt < 2)
    {
        op = pr->one_p; // we grab the current plot

        if (op == NULL) return win_printf_OK("cannot find plot");

        op->need_to_refresh = 1;

        if (addplt < 1 && all == 1 && op->n_dat < n_asqd)
            return win_printf_OK("Plot has less data sets than the nb of sequence!");

        if (addplt < 1) ds = op->dat[op->cur_dat];
    }

    if (all)
    {
        for (iseq = 0, nmax = 0; iseq < n_asqd; iseq++)
            smax = nmax = (nmax < asqd[iseq]->totalSize) ? asqd[iseq]->totalSize : nmax;
    }
    else smax = nmax = lsqd->totalSize;

    st = 0;

    if (nmax > size)
    {
        nmax = size;
        st = start;
    }

    if (simu && fp != NULL && all == 0)
    {
        fprintf(fp, "Barcode de %s; starting at %d over %d; noise %g inv %d stretching %g\n\n", lsqd->description, start, size,
                noise, inv, stretching);
        fprintf(fp, "%d\n", (int)(0.5 + stretching * nmax));
    }


    if (oliFromFile)
      {
	rn_oli = n_oli;
	i = win_scanf("If you want a random subset from your oligo series\n"
		      "specify the number of oligo s to test %6d",&rn_oli);
	if (i == WIN_CANCEL) return 0;
	if (rn_oli < n_oli)
	  {
	    for (i = 0; i < rn_oli; )
	      {
		i_oli = (int)(ran1(&idum) * n_oli);
		if (good_oli[i_oli] == 1) continue;
		good_oli[i_oli] = 1;
		i++;
	      }
	  }
	else for (i = 0; i < n_oli; i++) good_oli[i] = 1;
      }
    else good_oli[0] = 1;
    for (i_oli = 0; i_oli < n_oli; i_oli++)
    {
        c_oligo = (oliFromFile) ? loligo[i_oli] : oligo;
	if (good_oli[i_oli] == 0) continue;
        for (i = 0, len = strlen(c_oligo); i < len; i++)   // we loop on the sequence
        {
            ch = c_oligo[i];

            if (ch != 'a' && ch != 'A' && ch != 'c' && ch != 'C' &&
                    ch != 'g' && ch != 'G' && ch != 't' && ch != 'T' &&
                    ch != 'r' && ch != 'R' && ch != 'y' && ch != 'Y' &&
                    ch != 's' && ch != 'S' && ch != 'w' && ch != 'W' &&
                    ch != 'm' && ch != 'M' && ch != 'k' && ch != 'K' &&
                    ch != 'n' && ch != 'N')
                return win_printf_OK("Invalid base letter in sequence: %s\n", c_oligo);

            if (ch == 'a' || ch == 'A') ch = 'T';
            else if (ch == 'c' || ch == 'C') ch = 'G';
            else if (ch == 'g' || ch == 'G') ch = 'C';
            else if (ch == 't' || ch == 'T') ch = 'A';
            else if (ch == 'm' || ch == 'M') ch = 'K';
            else if (ch == 'k' || ch == 'K') ch = 'M';

            complementary[len - i - 1] = ch;
        }

        complementary[len] = 0;

        if (all) lsqd = asqd[0];

        if (op == NULL && opn == NULL)
        {
            if ((opn = create_and_attach_one_plot(pr, nmax, nmax, 0)) == NULL)
                return win_printf_OK("cannot create plot at %s:%d", __FILE__, __LINE__);

            ds = opn->dat[0];
            opn->need_to_refresh = 1;
            //op = opn;
            //ds->nx = ds->ny = 0;
        }

        if (pairwise == 0) smax = st + nmax;

        for (iseq = 0; iseq < n_asqd; iseq++)
        {
            if (all == 0 && iseq > 0) break;

            if (addplt == 1)
            {
                if (ds == NULL)
                {
                    if ((ds = create_and_attach_one_ds(op, nmax, nmax, 0)) == NULL)
                        return win_printf_OK("cannot create plot at %s:%d", __FILE__, __LINE__);

                    //ds->nx = ds->ny = 0;
                }

                set_ds_source(ds, "Oligo: %s, seq %d", c_oligo, iseq);
            }
            else if (addplt == 0) ds = op->dat[iseq];
            else if (addplt == 2)
            {
                if (ds == NULL)
                {
                    if ((ds = create_and_attach_one_ds(opn, nmax, nmax, 0)) == NULL)
                        return win_printf_OK("cannot create plot at %s:%d", __FILE__, __LINE__);

                    //ds->nx = ds->ny = 0;
                }

                set_ds_source(ds, "Oligo: %s, seq %d", c_oligo, iseq);
            }

            for (i_1 = st - 1, j = 0, i = st + 1, fi = 0; i != 0 && i < smax; j++)
            {
                if (all) lsqd = asqd[iseq];

                if (seg_or_complementary == 0)
                    i = findOligoInSeq(lsqd, c_oligo, i_1 + 1, noNs);
                else i = findOligoInSeq(lsqd, complementary, i_1 + 1, noNs);

                if (i > 0 && i < smax)
                {
                    fi++;
                    in = findNNNBackwardInSeq(lsqd, i, i_1);
                    in = (i_1 < in) ? in : i_1;

                    if (pairwise == 0)
                    {
                        if (hits == 0)
                        {
                            for (k = i - st; k < ds->nx && k < i + len; k++)
                                ds->yd[k] = 1;
                        }
                        else
                        {
                            k = i - st;

                            if (k >= 0 && k < ds->nx) ds->yd[k] = 1;
                        }

                        if (simu && fp != NULL && all == 0)
                        {
                            k = i - st;
                            dx = noise * gasdev(&idum);

                            if (inv) fprintf(fp, "%d\t%s\t%d\n", (int)(0.5 + stretching * (dx + nmax - k)), c_oligo, k);
                            else     fprintf(fp, "%d\t%s\t%d\n", (int)(0.5 + stretching * (dx + k)), c_oligo, k);
                        }

                        //add_new_point_to_ds(ds, i, i - in);
                    }
                    else if (i_1 >= 0)
                    {
                        if ((i - i_1) > 0 && (i - i_1) < nmax)
                        {
                            ds->yd[i - i_1] += 1;
                            total++;
                        }
                    }

                    i_1 = i;
                }

                tmpi = asqd[iseq]->totalSize - asqd[iseq]->nnumber;
                desc = asqd[iseq]->description;
            }

            if (hairpin)
            {
                for (i_1 = st - 1, j = 0, i = st + 1; i != 0 && i < smax; j++)
                {
                    if (all) lsqd = asqd[iseq];

                    if (seg_or_complementary == 1)
                        i = findOligoInSeq(lsqd, c_oligo, i_1 + 1, noNs);
                    else i = findOligoInSeq(lsqd, complementary, i_1 + 1, noNs);

                    if (i > 0 && i < smax)
                    {
                        fi++;
                        in = findNNNBackwardInSeq(lsqd, i, i_1);
                        in = (i_1 < in) ? in : i_1;

                        if (pairwise == 0)
                        {
                            if (hits == 0)
                            {
                                for (k = i - st; k < ds->nx && k < i + len; k++)
                                    ds->yd[k] = 1;
                            }
                            else if (hits == 1)
                            {
                                k = i - st;

                                if (k >= 0 && k < ds->nx) ds->yd[k] = 1;
                            }

                            else if (hits == 2)
                            {
                                k = i - st;

                                if (k >= 0 && k < ds->nx) ds->yd[k] = 1;
                            }

                            if (simu && fp != NULL && all == 0)
                            {
                                k = i - st;
                                dx = noise * gasdev(&idum);

                                if (inv) fprintf(fp, "%d\t%s\t%d\n", (int)(0.5 + stretching * (dx + nmax - k)), c_oligo, k);
                                else     fprintf(fp, "%d\t%s\t%d\n", (int)(0.5 + stretching * (dx + k)), c_oligo, k);
                            }

                            //add_new_point_to_ds(ds, i, i - in);
                        }
                        else if (i_1 >= 0)
                        {
                            if ((i - i_1) > 0 && (i - i_1) < nmax)
                            {
                                ds->yd[i - i_1] += 1;
                                total++;
                            }
                        }

                        i_1 = i;
                    }
                }
            }

            if (fi == 0) f0++;
            else if (fi == 1) f1++;
            else fn++;

            for (k = 0; k < ds->nx; k++)
            {
                ds->xd[k] = k;

                if (pairwise == 0)
                {
                    if (k >= asqd[iseq]->totalSize) ds->yd[k] = -0.01;
                }
                else if (tmpi > 0)
                {
                    ds->yd[k] /= total;//tmpi;
                }
            }

            ds = NULL;
        }
    }

    if (simu && fp != NULL && all == 0)
    {
        if (op == NULL) create_attach_select_x_un_to_op(opn, IS_METER, 0, stretching, -9, 0, "nm");
        else create_attach_select_x_un_to_op(op, IS_METER, 0, stretching, -9, 0, "nm");

        for (j = 0, i = st; i < smax; j++, i++)
        {
            if (j % 60 == 0)       fprintf(fp, "\n");

            fprintf(fp, "%c", lsqd->seq[i >> lsqd->ln2PageSize][i & lsqd->pageMask]);
        }

        fprintf(fp, "\n");
    }


    if (fp != NULL) fclose(fp);


    /* now we must do some house keeping */
    if (op == NULL)
    {
        if (pairwise == 0)
        {
            set_plot_title(opn, "\\stack{{%d Oligo matching 0nce,}{\\pt7 %d -> 0, %d several times) %d total}}", f1, f0, fn,
                           n_asqd);
            set_plot_x_title(opn, "Position along sequence");
            set_plot_y_title(opn, "matching");
        }
        else
        {
            set_plot_title(opn, "\\stack{{Oligo %s matching %s}{\\pt7 %d -> hits, %d bases (without N)}}", c_oligo, desc, total,
                           tmpi);
            set_plot_x_title(opn, "Distance between hit");
            set_plot_y_title(opn, "Probability");
        }
    }
    else win_printf("\\stack{{%d hits}{%d Oligo matching 0nce,}{\\pt7 %d -> 0, %d several times) %d total}}", fi, f1, f0,
                        fn, n_asqd);

    /* refisplay the entire plot */
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}






int do_findSeq_retrieve_oligo_finger_print_in_plot(void)
{
    int i, j;
    O_p *opn = NULL, *op;
    int len,  ok;
    d_s *ds, *dsn = NULL;
    pltreg *pr = NULL;
    char ch, oligo_l[1024];
    static int n_match = 4, nstart = 0;
    static float sigma = 2.5, tmp;

    if (updating_menu_state != 0)
    {
        // we wait for a valid sequence to be loaded
        if (lsqd == NULL) active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine multiply the y coordinate"
                             "of a data set by a number and place it in a new plot");
    }

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &ds) != 3)
        return win_printf_OK("cannot find data at %s:%d", __FILE__, __LINE__);

    if (ds->source == NULL || strlen(ds->source) > 1024)
        win_printf_OK("cannot find right data source!");

    oligo_l[0] = 0;
    ok = sscanf(ds->source, "Oligo: %s", oligo_l);

    if (ok < 1)    win_printf_OK("cannot find right data source!");

    for (i = 0, len = strlen(oligo_l); i < len; i++)   // we loop on the sequence
    {
        ch = oligo_l[i];

        if (ch != 'a' && ch != 'A' && ch != 'c' && ch != 'C' &&
                ch != 'g' && ch != 'G' && ch != 't' && ch != 'T')
            return win_printf_OK("Invalid base letter in sequence: %s\n", oligo_l);
    }

    i = win_scanf("I am going to look for a fingerprint in the plot\n"
                  "Define the number of matchs %8d\n"
                  "Define the starting point %10d\n"
                  "Specify the \\sigma of error %8f\n"
                  , &n_match, &nstart, &sigma);

    if (i == WIN_CANCEL) return D_O_K;

    if (nstart < 0 || nstart >= ds->nx - n_match)
        return win_printf_OK("nstart out of range %d not in [0,%d[", nstart, ds->nx - n_match);

    if (sigma <= 0) win_printf_OK("\\sigma  inapropriate %g", sigma);

    if ((opn = create_and_attach_one_plot(pr, ds->nx - n_match, ds->nx - n_match, 0)) == NULL)
        return win_printf_OK("cannot create plot at %s:%d", __FILE__, __LINE__);

    dsn = opn->dat[0];

    for (i = 0; i < dsn->nx; i++)
    {
        for (j = 0; j < n_match; j++)
        {
            tmp = (ds->yd[i + j] - ds->yd[nstart + j]) / sigma;
            dsn->yd[i] -= tmp * tmp;
        }

        dsn->yd[i] = exp(dsn->yd[i] / n_match);
        dsn->xd[i] = ds->xd[i];
    }

    /* now we must do some house keeping */
    set_ds_source(dsn, "Oligo correl matching: %s", oligo_l);
    set_plot_title(opn, "Oligo correl matching");
    set_plot_x_title(opn, "Position along sequence");
    set_plot_y_title(opn, "Square of distance between match");
    /* refisplay the entire plot */
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}



int do_findSeq_count_oligo2(void)
{
    int i;
    int len;
    static int noligo = 3, ndump = 0, nbhyb = 5, first = 1;
    pltreg *pr = NULL;
    FILE *fp = NULL;
    static char file[256];


    if (updating_menu_state != 0)
    {
        // we wait for a valid sequence to be loaded
        if (lsqd == NULL) active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }

    if (first == 1)
    {
        file[0] = 0;
        first = 0;
    }

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine multiply the y coordinate"
                             "of a data set by a number and place it in a new plot");
    }

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr", &pr) != 1)
        return win_printf_OK("cannot find data at %s:%d", __FILE__, __LINE__);

    i = win_scanf("Define the size of oligo to find: %8d\n"
                  "Dump most represented oligo to ascii file %b\n"
                  "if yes dump oligo if they hybridized more than %8d\n"
                  "Specify Filename\n%s\n"
                  , &noligo, &ndump, &nbhyb, file);

    if (i == WIN_CANCEL) return OFF;

    if (ndump)     fp = fopen(file, "w");

    len = countOligoInPr(lsqd, noligo, pr, 0, 0, nbhyb, fp);

    if (fp) fclose(fp);

    if (len <= 0) win_printf_OK("Pb counting oligos");

    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}



int do_findSeq_count_common_oligos(void)
{
    int i;
    int len;
    static int noligo = 10, first = 1, olictr = 0;
    pltreg *pr = NULL;
    char oligocenter[64];

    if (updating_menu_state != 0)
    {
        // we wait for a valid sequence to be loaded
        if (lsqd == NULL) active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine multiply the y coordinate"
                             "of a data set by a number and place it in a new plot");
    }

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr", &pr) != 1)
        return win_printf_OK("cannot find data at %s:%d", __FILE__, __LINE__);

    if (first == 1)
    {
        oligocenter[0] = 0;
        first = 0;
    }

    i = win_scanf("Define the size of oligo to find: %8d\n"
                  "define minimum sequence size %8d\n"
                  "define maximum sequence size %8d\n"
                  "Use an oligo to center sequence %b"
                  , &noligo, &seql_min, &seql_max, &olictr);

    if (i == WIN_CANCEL) return OFF;

    if (olictr == 0)  oligocenter[0] = 0;
    else
    {
        i = win_scanf("with oligo %s \n", oligocenter);

        if (i == WIN_CANCEL) return OFF;
    }

    len = countOligoSpecificAmongSeqInPr(noligo, pr, 0, 0, seql_min, seql_max, oligocenter);

    if (len <= 0) win_printf_OK("Pb counting oligos");

    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}



int do_findSeq_count_oligo(void)
{
    int i, j;
    O_p *opn = NULL;
    int len;
    static int noligo = 3;
    d_s *ds;
    pltreg *pr = NULL;
    unsigned int *oligoId = NULL;


    if (updating_menu_state != 0)
    {
        // we wait for a valid sequence to be loaded
        if (lsqd == NULL) active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }


    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine multiply the y coordinate"
                             "of a data set by a number and place it in a new plot");
    }

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr", &pr) != 1)
        return win_printf_OK("cannot find data at %s:%d", __FILE__, __LINE__);

    i = win_scanf("Define the size of oligo to find:\n%8d\n", &noligo);

    if (i == WIN_CANCEL) return OFF;

    len = countOligo(lsqd, noligo, &oligoId, 0, 0);

    if (len <= 0) win_printf_OK("Pb counting oligos");

    if ((opn = create_and_attach_one_plot(pr, len, len, 0)) == NULL)
        return win_printf_OK("cannot create plot at %s:%d", __FILE__, __LINE__);

    ds = opn->dat[0];
    opn->op_idle_point_add_display = findSeq_idle_point_add_display;

    //ds->nx = ds->ny = 0;
    for (j = 0; j < ds->nx; j++)
    {
        ds->yd[j] = oligoId[j];
        ds->xd[j] = j;
    }

    /* now we must do some house keeping */
    set_ds_source(ds, "Oligosize: %d", noligo);
    set_plot_title(opn, "Oligo #");
    set_plot_x_title(opn, "Oligo number");
    set_plot_y_title(opn, "Number of oligos");
    /* refisplay the entire plot */
    free(oligoId);
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}

/*
int do_fetch_fasta_file(void)
{
    char path[512];
    static int keep_Ns = 1;

    if(updating_menu_state != 0)    return D_O_K;
    if (key[KEY_LSHIFT])
        return win_printf_OK("This routine loads a fasta sequence");

    win_scanf("Do you accept sequences containing Ns %b",&keep_Ns);

    file_list_t *file_list = open_files_config("Load plot (*.gr)", NULL, "Plot Files (*.gr)\0*.gr\0All Files (*.*)\0*.*\0\0", "FASTA-FILE", "last_loaded");
    file_list_t *cursor = file_list;

    while (cursor != NULL)
    {

        lsqd = readFastaSeq(cursor->data,keep_Ns);
        extract_file_path(path, sizeof(path), cursor->data);
        reorder_visited_path_list(path);
        cursor = cursor->next;
    }
    free_file_list(file_list);
    win_printf("%d sequences loaded",n_asqd);
    do_update_menu();
    return 0;
}
*/

char *findSeq_idle_point_add_display_blast(struct one_plot *op, DIALOG *d, int nearest_ds, int nearest_point)
{
    static char message[1024];
    d_s *ds;
    int ok = 0, oligoSize = 0, seq1 = 0, seq2 = 0, i, npid, ic, ip;
    sqd *msqd;

    message[0] = 0;
    (void)d;
    if (op == NULL) return message;

    if (nearest_ds >= op->n_dat) return message;

    ds = op->dat[nearest_ds];
    ok = sscanf(ds->source, "Common Oligos with size: %d between sequence %d and %d", &oligoSize, &seq1, &seq2);

    if (ok < 3) return message;

    if (oligoSize > 15) return message;

    if (asqd == NULL) return message;

    if (seq1 < 0 || seq1 >= n_asqd) return message;

    msqd = asqd[seq1];
    npid = (int)ds->xd[nearest_point];

    if (npid < 0 || npid >= msqd->totalSize) return message;

    for (i = 0; i < oligoSize; i++)
    {
        ip = (npid + i) >> msqd->ln2PageSize;
        ic = (npid + i) & msqd->pageMask;
        message[i] = msqd->seq[ip][ic] & 0x5F;
    }

    return message;
}



int do_draw_common_oligos_2_seq(void)
{
    int i = 0, j1, j2;
    int seq1, seq2, noli, k1, k2, seq, imax, common = 0, k, nxst;
    static int oligoSize = 10, all = 0, align = 0;
    char message[512];
    d_s *ds, *dst = NULL, *dsd = NULL, *dsh = NULL;
    O_p *opn, *opd;// , *oph
    float  tmp = 0;// Max_pos, Max_val,
    pltreg *pr;
    dya *olr1, *olr2;


    if (updating_menu_state != 0)
    {
        // we wait for a valid sequence to be loaded
        if (asqd == NULL || oligoReg == NULL) active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }

    if (key[KEY_LSHIFT])
        return win_printf_OK("This routine display  a blast plot between two sequences");

    if (ac_grep(cur_ac_reg, "%pr", &pr) != 1)
        return win_printf_OK("cannot find data at %s:%d", __FILE__, __LINE__);

    seq1 = c_asqd;
    seq2 = c_asqd + 1;
    snprintf(message, sizeof(message), "There is %d fasta sequences loaded\n"
             "current one is %d -> %s\n"
             "choose sequence 1 %%8d\n"
             "choose sequence 2 %%8d\n"
             "Or select all sequences %%b\n"
             "Shift to align %%b\n"
             , n_asqd, seq1, backslash_to_slash(asqd[seq1]->filename));
    i = win_scanf(message, &seq1, &seq2, &all, &align);

    if (i == WIN_CANCEL) return D_O_K;

    if (seq1 < 0 || seq2 >= n_asqd)  win_printf_OK("value %d for sequence 1 out of range [0,%d[", seq1, n_asqd);

    if (seq2 < 0 || seq2 >= n_asqd)  win_printf_OK("value %d for sequence 2 out of range [0,%d[", seq2, n_asqd);

    oligoSize = noli = noligoReg;

    for (i = 1; oligoSize > 4; i++, oligoSize /= 4);

    oligoSize = i;

    nxst = asqd[seq1]->totalSize;

    for (i = 2; i < nxst; i *= 2);

    nxst = 2 * i;

    if ((opn = create_and_attach_one_plot(pr, nxst, nxst, 0)) == NULL)
        return win_printf_OK("cannot create plot at %s:%d", __FILE__, __LINE__);

    ds = opn->dat[0];
    ds->nx = ds->ny = 0;
    set_ds_dash_line(ds);

    if (align)
    {
        dst = build_data_set(16, 16);

        if (dst == NULL) return win_printf_OK("cannot create plot at %s:%d", __FILE__, __LINE__);

        dst->nx = dst->ny = 0;

        if (all)
        {
            if ((opd = create_and_attach_one_plot(pr, n_asqd, n_asqd, 0)) == NULL)
                return win_printf_OK("cannot create plot at %s:%d", __FILE__, __LINE__);

            dsd = opd->dat[0];
            set_plot_title(opd, "Offset between sequences");
            set_plot_x_title(opd, "Sequence nb");
            set_plot_y_title(opd, "Offset versurs sequence %d", seq1);
        }

        dsh = build_data_set(nxst, nxst);

        if (dsh == NULL) return win_printf_OK("cannot create ds !");
    }

    opn->op_idle_point_add_display = findSeq_idle_point_add_display_blast;

    for (seq = 0; seq < n_asqd; seq++)
    {
        if (seq == seq1) continue;

        if (all == 0 && seq != seq2) continue;

        if (ds == NULL)
        {
            if ((ds = create_and_attach_one_ds(opn, nxst, nxst, 0)) == NULL)
                return win_printf_OK("cannot create plot at %s:%d", __FILE__, __LINE__);

            ds->nx = ds->ny = 0;
            set_ds_dash_line(ds);
        }

        //set_ds_source(ds,"Common Oligos with size: %d between sequence %d and %d",oligoSize,seq1,seq);
        for (i = 0; oligoReg != NULL && i < noli; i++)
        {
            if (oligoReg[i] != NULL)
            {
                for (j1 = 0, k1 = -1, olr1 = oligoReg[i][seq1]; (olr1 != NULL) && (j1 < olr1->n_hy); j1++)
                {
                    k1 = olr1->hyo[j1].pos;

                    for (j2 = 0, k2 = -1, olr2 = oligoReg[i][seq]; (olr2 != NULL) && (j2 < olr2->n_hy); j2++)
                    {
                        if (k2 < 0 || (abs(olr2->hyo[j2].pos - k1) < abs(olr2->hyo[j2].pos - k2)))
                            k2 = olr2->hyo[j2].pos;
                    }

                    if (k2 > 0)
                    {
                        add_new_point_to_ds(ds, k1, k2);
                        add_new_point_to_ds(ds, k1 + oligoSize, k2 + oligoSize);

                        if (align)  add_new_point_to_ds(dst, dst->nx, k2 - k1);
                    }
                }
            }
        }

        if (align)
        {
            /*
               oph = build_histo_with_exponential_convolution(NULL, NULL, dst, 1.0);
               if (oph != NULL && oph->n_dat > 0)
               {
               imax = find_index_of_max_in_y(oph->dat[0]);
               if (imax >= 0)
               {
               nxh = oph->dat[0]->nx;
               ret = find_max_around(oph->dat[0]->yd, nxh, imax, &Max_pos, &Max_val, NULL);
               if (ret == 0)
               {
               tmp = oph->dat[0]->xd[0] + (Max_pos * (oph->dat[0]->xd[nxh-1] - oph->dat[0]->xd[0])/(nxh-1));
               for (j2 = 0; j2 < ds->nx; j2++)
               ds->yd[j2] -= tmp;
               for (j2 = 0; j2 < dst->nx; j2++)
               dst->yd[j2] -= (int)tmp;
               }
               */
            for (i = 0; i < dsh->nx; i++) dsh->yd[i] = dsh->xd[i] = 0;

            for (i = 0; i < dst->nx; i++)
            {
                k = (int)dst->yd[i] + dsh->nx / 2;

                if ((k >= 0) && (k < dsh->nx))
                {
                    dsh->xd[k] = (int)dst->yd[i];
                    dsh->yd[k] += 1;
                }
            }

            for (i = 0, imax = 0, tmp = 0; i < dsh->nx; i++)
            {
                if (dsh->yd[i] > 1)
                {
                    imax += dsh->yd[i];
                    tmp += dsh->yd[i] * dsh->xd[i];
                }

                //imax = (dsh->yd[i] > dsh->yd[imax]) ? i :imax;
            }

            if (imax > 0) tmp /= imax;

            for (j2 = 0; j2 < ds->nx; j2++)
                ds->yd[j2] -= tmp;

            for (j2 = 0; j2 < dst->nx; j2++)
                dst->yd[j2] -= (int)tmp;

            //if ((dsh = create_and_attach_one_ds(opn, 51, 51, 0)) == NULL)
            //  return win_printf_OK("cannot create plot at %s:%s", __FILE__, __LINE__);
            //for (i = 0; i < dst->nx; i++)
            //{
            //        k = (int)dst->yd[i] +25;
            //        if (k < 51 && k >= 0) dsh->yd[k] += 1;
            //      }
            for (i = 0, common = 0; i < dsh->nx; i++)
                if ((dsh->yd[i] > 1) || (fabs(dsh->xd[i] - tmp) < 30)) common += dsh->yd[i];

            dst->nx = dst->ny = 0;

            if (all == 0)
            {
                //add_data_to_pltreg(pr, IS_ONE_PLOT, (void*)oph);
                //oph->need_to_refresh = 1;
                win_printf("shift by %g", tmp);
            }
            else
            {
                dsd->yd[seq] = tmp;
                dsd->xd[seq] = seq;
                //free_one_plot(oph);
                //oph = NULL;
            }
        }

        set_ds_source(ds, "Common Oligos %d with size: %d between sequence %d and %d", common, oligoSize, seq1, seq);
        ds = NULL;
    }

    if (dsh) free_data_set(dsh);

    set_plot_title(opn, "Common Oligos with size # %d", oligoSize);
    set_plot_x_title(opn, "%s",backslash_to_slash(asqd[seq1]->filename));
    set_plot_y_title(opn, "%s",backslash_to_slash(asqd[seq1]->filename));
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}


int do_draw_common_oligos_2_seq_im(void)
{
    int i = 0, j1, j2;
    int seq1, seq2, noli, k1, k2;
    static int oligoSize = 10;
    O_i *oi;
    pltreg *pr;
    imreg *imr;
    dya *olr1 = NULL, *olr2 = NULL;

    if (updating_menu_state != 0)
    {
        // we wait for a valid sequence to be loaded
        if (asqd == NULL || oligoReg == NULL) active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }

    if (key[KEY_LSHIFT])
        return win_printf_OK("This routine display  a blast plot between two sequences");

    if (ac_grep(cur_ac_reg, "%pr", &pr) != 1)
        return win_printf_OK("cannot find data at %s:%d", __FILE__, __LINE__);

    seq1 = c_asqd;
    seq2 = c_asqd + 1;

    if (seq1 < 0 || seq2 >= n_asqd)  win_printf_OK("value %d for sequence 1 out of range [0,%d[", seq1, n_asqd);

    if (seq2 < 0 || seq2 >= n_asqd)  win_printf_OK("value %d for sequence 2 out of range [0,%d[", seq2, n_asqd);

    oligoSize = noli = noligoReg;

    for (i = 1; oligoSize > 4; i++, oligoSize /= 4);

    oligoSize = i;


    imr = create_and_register_new_image_project(0,   32,  900,  668);

    if (imr == NULL) return (win_printf_OK("could not create imreg"));

    oi = create_and_attach_oi_to_imr(imr, n_asqd, n_asqd, IS_INT_IMAGE);
    remove_from_image(imr, imr->o_i[0]->im.data_type, (void *)imr->o_i[0]);
    select_image_of_imreg(imr, imr->n_oi - 1);

    for (i = 0; oligoReg != NULL && i < noli; i++)
    {
        if (oligoReg[i] != NULL)
        {
            for (seq1 = 0; seq1 < n_asqd; seq1++)
            {
                olr1 = oligoReg[i][seq1];

                if (olr1 == NULL) continue;

                for (j1 = 0, k1 = -1; j1 < olr1->n_hy; j1++)
                {
                    k1 = olr1->hyo[j1].pos;

                    for (seq2 = 0; seq2 < n_asqd; seq2++)
                    {
                        olr2 = oligoReg[i][seq2];

                        if (olr2 == NULL) continue;

                        for (j2 = 0, k2 = -1; j2 < olr2->n_hy; j2++)
                        {
                            if (k2 < 0 || (abs(olr2->hyo[j2].pos - k1) < abs(olr2->hyo[j2].pos - k2)))
                                k2 = olr2->hyo[j2].pos;
                        }

                        if (k2 > 0) oi->im.pixel[seq2].in[seq1] += 1;
                    }
                }
            }
        }
    }

    find_zmin_zmax(oi);
    set_im_x_title(oi, "Sequence index 1");
    set_im_y_title(oi, "Sequence index 2");
    oi->need_to_refresh = ALL_NEED_REFRESH;
    set_im_title(oi, "Common oligos");
    return refresh_image(imr, imr->n_oi - 1);
}




int do_draw_common_oligos_between_N_seq(void)
{
    int i = 0, j1, j;
    int seq1, noli, k1, k2, nseq, k;
    static int oligoSize = 10,  nhitmin = 25, nhitmax = 250, *seqhit = NULL;
    int *seqhy = NULL;
    d_s *ds, **dst = NULL;
    O_p *opn;
    pltreg *pr;
    sqd *msqd;


    if (updating_menu_state != 0)
    {
        // we wait for a valid sequence to be loaded
        if (asqd == NULL || oligoReg == NULL) active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }

    if (key[KEY_LSHIFT])
        return win_printf_OK("This routine display  a blast plot between two sequences");

    if (ac_grep(cur_ac_reg, "%pr", &pr) != 1)
        return win_printf_OK("cannot find data at %s:%d", __FILE__, __LINE__);

    seq1 = c_asqd;
    i = win_scanf("Draw oligos along sequences common to multiple sequences\n"
                  "having %d minimum and %d maximum hits ", &nhitmin, &nhitmax);

    if (i == WIN_CANCEL) return D_O_K;

    oligoSize = noli = noligoReg;

    for (i = 1; oligoSize > 4; i++, oligoSize /= 4);

    oligoSize = i;


    seqhit = (int *)calloc(n_asqd, sizeof(int));

    if (seqhit == NULL) return win_printf_OK("cannot create array !");

    seqhy = (int *)calloc(n_asqd, sizeof(int));

    if (seqhy == NULL) return win_printf_OK("cannot allocate sequence !");

    dst = (d_s **)calloc(n_asqd, sizeof(d_s *));

    if (dst == NULL) return win_printf_OK("cannot allocate sequence !");

    for (i = 0, nseq = 0; i < n_asqd; i++)
    {
        seqhy[i] = -1;
        msqd = asqd[i];

        if ((msqd != NULL) && (msqd->seq != NULL) && (msqd->totalSize > seql_min) && (msqd->totalSize < seql_max))
        {
            seqhy[i] = 0;
            nseq++;
        }
    }

    for (j = 0, ds = NULL, opn = NULL; j < n_asqd; j++)
    {
        if (seqhy[j] < 0) continue;

        if (opn == NULL)
        {
            if ((opn = create_and_attach_one_plot(pr, asqd[j]->totalSize, asqd[j]->totalSize, 0)) == NULL)
                return win_printf_OK("cannot create plot at %s:%d", __FILE__, __LINE__);

            ds = opn->dat[0];
        }

        if (ds == NULL)
        {

            if ((ds = create_and_attach_one_ds(opn, asqd[j]->totalSize, asqd[j]->totalSize, 0)) == NULL)
                return win_printf_OK("cannot create plot at %s:%d", __FILE__, __LINE__);
        }

        dst[j] = ds;
        set_ds_source(ds, "Common Oligos with size: %d for sequence %d -> %s", oligoSize, j, asqd[j]->filename);

        for (k = 0; k < ds->nx; k++) ds->xd[k] = k;

        ds = NULL;
    }

    opn->op_idle_point_add_display = findSeq_idle_point_add_display_blast;

    for (i = 0; oligoReg != NULL && i < noli; i++)
    {
        if (oligoReg[i] != NULL)
        {
            for (j1 = 0, k1 = 0; j1 < n_asqd; j1++)
                k1 += (oligoReg[i][j1] != NULL && oligoReg[i][j1]->n_hy > 0) ? 1 : 0;

            if (k1 < nhitmin || k1 > nhitmax) continue;

            for (j1 = 0; j1 < n_asqd; j1++)
            {
                ds = dst[j1];

                for (j = 0; oligoReg[i][j1] != NULL && j < oligoReg[i][j1]->n_hy; j++)
                {
                    for (k2 = 0; k2 < oligoSize && (k2 + oligoReg[i][j1]->hyo[j].pos) < ds->nx ; k2++)
                        ds->yd[k2 + oligoReg[i][j1]->hyo[j].pos] += 1;
                }
            }
        }
    }

    set_plot_title(opn, "Common Oligos with size # %d", oligoSize);
    set_plot_x_title(opn, "%s",backslash_to_slash(asqd[seq1]->filename));
    set_plot_y_title(opn, "%s",backslash_to_slash(asqd[seq1]->filename));
    refresh_plot(pr, UNCHANGED);
    free(dst);
    free(seqhy);
    free(seqhit);
    return D_O_K;
}


int find_valid_oligos(void)
{
    int i, j1;
    int invalid = 0, k1, k2, nv, nk2;

    if (noligoReg < 0) return 1;

    for (i = 0, nv = 0, nk2 = 0; oligoReg != NULL && i < noligoReg; i++)
    {
        if (oligoReg[i] != NULL)
        {
            for (j1 = 0, k2 = 0, invalid = 0; j1 < n_asqd; j1++)
            {
                k1 = (oligoReg[i][j1] != NULL) ? oligoReg[i][j1]->n_hy : 0;

                if (k1 > 1) invalid = 0x1000000; // invalid oligo

                k2 += (k1 > 0) ? 1 : 0;
            }

            if (invalid == 0)
            {
                nv++;
                nk2 += k2;
            }

            oligo_nhib_invalid[i] = k2 | invalid;
        }
    }

    win_printf("%d oligos %d valid avg hyb %g", noligoReg, nv, (float)nk2 / nv);
    return 0;
}


unsigned int *get_oligos_freq_binder(void)
{
    int i, j1;
    unsigned int *nh = NULL;
    int  nv;

    if (noligoReg < 0) return NULL;

    for (i = 0, nv = 0; oligoReg != NULL && i < noligoReg; i++)
    {
        if (oligo_nhib_invalid[i] >  n_asqd) continue;

        if (oligo_nhib_invalid[i] < (n_asqd / 4)) continue;

        if (oligo_nhib_invalid[i] > (3 * n_asqd / 4)) continue;

        nv++;
    }

    win_printf("nv = %d", nv);
    nh = (unsigned int *)calloc((nv + 1), sizeof(unsigned int));

    if (nh == NULL) return NULL;

    nh[0] = nv;

    for (i = 0, j1 = 1; oligoReg != NULL && i < noligoReg; i++)
    {
        if (oligo_nhib_invalid[i] >  n_asqd) continue;

        if (oligo_nhib_invalid[i] < (n_asqd / 4)) continue;

        if (oligo_nhib_invalid[i] > (3 * n_asqd / 4)) continue;

        if (j1 <= nv)   nh[j1++] = i;
    }

    return nh;
}


int QuickSort_float_int(float *xd, int *yd, int l, int r)
{
    int  i, j;
    float   x, y;
    int iy;

    i = l;
    j = r;
    x = xd[(int)((l + r) / 2) ];

    while (i < j)
    {
        while (xd[i] < x)
        {
            i += 1;
        }

        while (x < xd[j])
        {
            j -= 1;
        }

        if (i <= j)
        {
            y = xd[i];
            xd[i] = xd[j];
            xd[j] = y;
            iy = yd[i];
            yd[i] = yd[j];
            yd[j] = iy;
            i += 1;
            j -= 1;
        }
    }/*while*/

    if (l < j) QuickSort_float_int(xd, yd, l, j);

    if (i < r) QuickSort_float_int(xd, yd, i, r);

    return 0;
} /* end of the "QuickSort_float_int" function */


int allocate_fingerprint(figpt *fgp, int nsel, int nseq)
{
    int i;

    win_printf("allocating fingerprint %d and %d", nsel, nseq);

    if (fgp == NULL || nsel <= 0 || nseq <= 0) return 1;

    if (nsel > 32) return 2;

    win_printf("allocating fingerprint %d and %d bef calloc", nsel, nseq);
    fgp->seq = (unsigned int *)calloc(nseq, sizeof(unsigned int));

    if (fgp->seq == NULL)
    {
        win_printf("cannot allocate seq in figerprint");
        return 3;
    }

    fgp->n_seq = nseq;
    win_printf("allocating fingerprint %d and %d bef calloc seqhyb", nsel, nseq);
    fgp->seqhyb = (int **)calloc(nseq, sizeof(int *));

    if (fgp->seqhyb == NULL)
    {
        win_printf("cannot allocate seqhyb double array");
        return 4;
    }

    fgp->n_sel = nsel;
    win_printf("allocating fingerprint %d and %d bef calloc seqhyb i ", nsel, nseq);

    for (i = 0; i < nseq; i++)
    {
        fgp->seqhyb[i] = (int *)calloc(nsel + 1, sizeof(int));

        if (fgp->seqhyb[i] == NULL)
        {
            win_printf("cannot allocate seqhyb[%d] double array", i);
            return 1;
        }
    }

    return 0;
}



int fill_fingerprint(figpt *fgp, int iolisel, unsigned int oli)
{
    int j, pos, npos;
    unsigned int k1, l;
    float mpos;

    if (fgp == NULL) return 1;

    if (iolisel >= fgp->n_sel) return 2;

    if (oligoReg == NULL || oligoReg[oli] == NULL) return 3;

    for (j = 0, l = ~(1 << iolisel), mpos = 0, npos = 0; j < fgp->n_seq; j++)
    {
        k1 = (oligoReg[oli][j] != NULL) ? oligoReg[oli][j]->n_hy : 0;

        if (k1 > 1) return 3; // we want one hybridization only

        fgp->seq[j] &= l;
        fgp->seq[j] |= k1 << iolisel;

        if (j == 0) win_printf("iolisel %d l %x k1 %x val %d", iolisel, l, (k1 << iolisel), fgp->seq[j]);

        pos = (oligoReg[oli][j] != NULL) ? oligoReg[oli][j]->hyo[0].pos : -1;
        fgp->seqhyb[j][iolisel] = pos;

        if (pos >= 0)
        {
            npos++;
            mpos += pos;
        }
    }

    fgp->olisel[iolisel] = oli;

    if (npos)
    {
        mpos /= npos;
        fgp->mean_pos[iolisel] = pos;
    }

    return 0;
}

int find_energy_fingerprint(figpt *fgp)
{
    int i, j, k;
    static unsigned int *oc = NULL;
    static int ocsize = 0;
    float tmp_pos[32];
    int i_pos[32], d_min, i_1, pos, hyb;

    if (fgp == NULL) return 1;

    if (oc == NULL)
    {
        for (i = 0, ocsize = 1; i < fgp->n_sel; i++)  ocsize *= 2;

        win_printf("ocsize %d, nsel %d", ocsize, fgp->n_sel);
        oc = (unsigned int *)calloc(ocsize, sizeof(unsigned int));

        if (oc == NULL) return win_printf_OK("cannot create unsigned array of size %d!", ocsize);
    }
    else
    {
        for (i = 0, j = 1; i < fgp->n_sel; i++)  j *= 2;

        if (j != ocsize) return win_printf_OK("incompatible unsigned array of size %d %d!", j, ocsize);
    }

    // we compute how many sequences have the same fingerprint
    for (i = 0; i < ocsize; i++) oc[i] = 0;

    for (i = 0; i < fgp->n_seq; i++)
    {
        k = fgp->seq[i];

        if (k >= 0 && k < ocsize) oc[k] += 1;
        else win_printf("Wrong oligo index");
    }

    for (i = 0, j = 0; i < fgp->n_seq; i++)
    {
        k = fgp->seq[i];

        if (k >= 0 && k < ocsize)
        {
            fgp->seqhyb[i][fgp->n_sel] = (int)oc[k];

            if (oc[k] > 1) j++;
        }
    }

    k = j;

    for (i = 0, j = 0; i < ocsize; i++)
        j += (oc[i] > 1) ? oc[i] : 0;

    if (k != j) win_printf("inconsistency j %d k %d", j, k);

    fgp->E1 = j * 1000; // we put a high energy when same code is used several times

    for (i = 0; i < fgp->n_sel; i++)
    {
        tmp_pos[i] = fgp->mean_pos[i];
        i_pos[i] = i;
    } // we find the order of oligos along sequence

    QuickSort_float_int(tmp_pos, i_pos, 0, fgp->n_sel - 1);

    for (i = 0; i < fgp->n_sel; i++) fgp->order[i] = i_pos[i];

    for (j = 0, fgp->E2 = 0; j < fgp->n_seq; j++)
    {
        for (i = 0, i_1 = -1, d_min = -1; i < fgp->n_sel; i++)
        {
            k = i_pos[i];

            if (k >= 0 && k < fgp->n_sel)
            {
                pos = fgp->seqhyb[j][k];

                if (pos >= 0)
                {
                    if (i_1 >= 0)
                    {
                        if (d_min < 0) d_min = fabs(pos - i_1);
                        else d_min = (d_min > fabs(pos - i_1)) ? fabs(pos - i_1) : d_min;
                    }

                    i_1 = pos;
                }
            }
        }

        if (d_min >= 0)
        {
            if (d_min < 1) fgp->E2 += 100;
            else fgp->E2 += ((float)50) / d_min;
        }
    }

    for (i = 0, fgp->E3 = 0; i < fgp->n_sel; i++)
    {
        for (j = 0, hyb = 0; j < fgp->n_seq; j++)
            hyb += (fgp->seqhyb[j][i] < 0) ? 0 : 1;

        if ((hyb < 1) || (hyb >= fgp->n_seq)) fgp->E3 += 3 * fgp->n_seq;
        else fgp->E3 += (float)(fgp->n_seq * fgp->n_seq) / (hyb * (fgp->n_seq - hyb));
    }

    return 0;
}

O_i *fill_hybrid_sequences_in_im(imreg *imr, O_i *oi, figpt *fgp)
{
    int nmax, i, j, k, l, pos, oligoSize, alloc = 0;
    char col[16][3];

    col[0][0] = 0;
    col[0][1] = 0;
    col[0][2] = 128;
    col[1][0] = 0;
    col[1][1] = 128;
    col[1][2] = 0;
    col[2][0] = 0;
    col[2][1] = 128;
    col[2][2] = 128;
    col[3][0] = 128;
    col[3][1] = 0;
    col[3][2] = 0;
    col[4][0] = 128;
    col[4][1] = 0;
    col[4][2] = 128;
    col[5][0] = 128;
    col[5][1] = 128;
    col[5][2] = 0;
    col[6][0] = 0;
    col[6][1] = 0;
    col[6][2] = 85;
    col[7][0] = 0;
    col[7][1] = 85;
    col[7][2] = 0;
    col[8][0] = 0;
    col[8][1] = 85;
    col[8][2] = 85;
    col[9][0] = 85;
    col[9][1] = 85;
    col[9][2] = 0;
    col[10][0] = 85;
    col[10][1] = 0;
    col[10][2] = 85;
    col[11][0] = 64;
    col[11][1] = 64;
    col[11][2] = 255;
    col[12][0] = 64;
    col[12][1] = 255;
    col[12][2] = 64;
    col[13][0] = 0;
    col[13][1] = 255;
    col[13][2] = 255;
    col[14][0] = 255;
    col[14][1] = 0;
    col[14][2] = 255;
    col[15][0] = 255;
    col[15][1] = 255;
    col[15][2] = 0;

    if (fgp == NULL || imr == NULL) return NULL;

    if (oi == NULL) alloc = 1;

    for (i = 0, nmax = 0; i < n_asqd; i++)
        nmax = (nmax < asqd[i]->totalSize) ? asqd[i]->totalSize : nmax;

    if (oi == NULL)    oi = create_and_attach_oi_to_imr(imr, nmax, fgp->n_seq, IS_RGB_PICTURE);
    else if (nmax != oi->im.nx) return (O_i *) win_printf_ptr("Wrong image dim in x");
    else if (n_asqd != oi->im.ny) return (O_i *) win_printf_ptr("Wrong image dim in y");

    oligoSize = noligoReg;

    for (i = 1; oligoSize > 4; i++, oligoSize /= 4);

    oligoSize = i;


    for (j = 0; j < oi->im.ny && j < fgp->n_seq; j++)
    {
        for (i = 0; i < 3 * oi->im.nx; i++)
        {
            k = fgp->seqhyb[j][fgp->n_sel];
            oi->im.pixel[j].ch[i] = (k > 1) ? 32 : 0;
        }

        for (i = 0; i < fgp->n_sel; i++)
        {
            l = fgp->order[i];

            if (l >= 0 && l < fgp->n_sel)
            {
                pos = fgp->seqhyb[j][l];

                if (pos >= 0)
                {
                    for (k = pos; k < pos + oligoSize && k < oi->im.nx; k++)
                    {
                        oi->im.pixel[j].ch[3 * k] = col[l % 16][0];
                        oi->im.pixel[j].ch[3 * k + 1] = col[l % 16][1];
                        oi->im.pixel[j].ch[3 * k + 2] = col[l % 16][2];
                    }
                }
            }
        }
    }

    if (alloc) find_zmin_zmax(oi);

    set_im_x_title(oi, "Bp along Sequence");
    set_im_y_title(oi, "Sequence index");
    oi->need_to_refresh = ALL_NEED_REFRESH;
    set_im_title(oi, "\\stack{{Specific oligos (Energy %g)}{\\pt7 E1 %g, E2 %g, E3 %g}"
                 "{\\pt7 olinb %d exchange %d with %d}{\\pt7 iter %d flip %d flips %d}}"
                 , fgp->E1 + fgp->E2 + fgp->E3, fgp->E1, fgp->E2, fgp->E3, lastolinb, prevoliid, newoliid, itern, flip, flips);
    return oi;
}


figpt *copy_figpt(figpt *src, figpt *dest)
{
    figpt *ldest = NULL;
    int i, j;


    if (src == NULL) return NULL;

    if (dest == NULL)
    {
        win_printf("Entering copying figrt dest = NULL src sel %d seq %d", src->n_sel, src->n_seq);
        ldest = (figpt *)calloc(1, sizeof(figpt));

        if (ldest == NULL)
        {
            win_printf("calloc pb for figpt");
            return NULL;
        }

        allocate_fingerprint(ldest, src->n_sel, src->n_seq);
        dest = ldest;
    }
    else
    {
        if ((src->n_sel != dest->n_sel) || (src->n_seq != dest->n_seq))
            return NULL;
    }

    win_printf("copy figrt 1");

    for (i = 0; i < src->n_sel; i++)
    {
        dest->olisel[i] = src->olisel[i];
        dest->order[i] = src->order[i];
        dest->mean_pos[i] = src->mean_pos[i];
    }

    win_printf("copy figrt 2");

    for (i = 0; i < src->n_seq; i++)
    {
        dest->seq[i] = src->seq[i];

        for (j = 0; j <= src->n_sel; j++)
            dest->seqhyb[i][j] = src->seqhyb[i][j];
    }

    win_printf("copy figrt 3");
    dest->E1 = src->E1;
    dest->E2 = src->E2;
    dest->E3 = src->E3;
    return dest;
}

//# ifdef ENCOURS

int fingerprint_idle_action(O_i *oi, DIALOG *d)
{
    int i, j;
    static unsigned int *subset = NULL, *sbs = NULL;
    int noli, no, invalid, nsbs = 0, dosub, oliid;
    double E, Et;
    imreg *imr;
    float testp, prob;
    static int nidle = 0;

    my_set_window_title("entering idle %d", nidle++);

    if (d->dp == NULL)    return 1;

    imr = (imreg *)d->dp;       /* the image region is here */

    if (bfgpt == NULL) return 0;

    if (tfgpt == NULL) return 0;

    if (subset == NULL)
    {
        subset = get_oligos_freq_binder();
        win_printf("Find binder");
    }

    nsbs = (subset == NULL) ? 0 : subset[0];

    if (nsbs < 0) win_printf("nsns %d", nsbs);

    sbs = (subset == NULL) ? NULL : subset + 1;
    E = bfgpt->E1 + bfgpt->E2 + bfgpt->E3;
    noli = noligoReg;
    my_set_window_title("entering idle %d nsbs %d", nidle, nsbs);
    return 0;

    for (j = 0, dosub = 0; j < 256; j++)
    {
        if (copy_figpt(bfgpt, tfgpt) == NULL) return 0;

        for (i = (int)(ran1(&idum) * bfgpt->n_sel); i < 0 && i >= bfgpt->n_sel;  i = (int)(ran1(&idum) * bfgpt->n_sel));

        oliid = tfgpt->olisel[i];

        if ((subset == NULL) || ((j & 0x0f) == 0) || nsbs < 0)
        {
            for (no = (int)(ran1(&idum) * noli), invalid = 1; invalid > 0; no = (int)(ran1(&idum) * noli))
            {
	      if (((int)tfgpt->olisel[i] != no) && (no >= 0) && (no < noli))
                {
                    my_set_window_title("entering idle %d n0 %d was %d", nidle, no, tfgpt->olisel[i]);
                    invalid = (oligo_nhib_invalid[no] > tfgpt->n_seq) ? 1 : 0; //invalid;

                    if (invalid == 0) invalid = (fill_fingerprint(tfgpt, i, no)) ? 1 : 0;
                }
            }

            dosub = 0;
        }
        else
        {
            for (no = (int)ran1(&idum) * nsbs, invalid = 1; invalid > 0; no = (int)ran1(&idum) * nsbs)
            {
                if ((no >= 0) && (no < nsbs))
                {
                    no = sbs[no];

                    if (((int)tfgpt->olisel[i] != no) && (no >= 0) && (no < noli))
                    {
                        my_set_window_title("entering idle %d n0 %d was %d", nidle, no, tfgpt->olisel[i]);
                        invalid = (oligo_nhib_invalid[no] > tfgpt->n_seq) ? 1 : 0; //invalid;

                        if (invalid == 0) invalid = (fill_fingerprint(tfgpt, i, no)) ? 1 : 0;
                    }
                }
            }

            dosub = 1;
        }

        find_energy_fingerprint(tfgpt);
        Et = tfgpt->E1 + tfgpt->E2 + tfgpt->E3;

        if (Et < E)
        {
            if (dosub == 0) flip++;
            else flips++;

            lastolinb = i;
            prevoliid = oliid;
            newoliid = tfgpt->olisel[i];

            //if ((flips&0xf) == 0) my_set_window_title("Flips i %d exchange %d with %d",i,(int)bfgpt->olisel[i],(int)tfgpt->olisel[i]);
            if (copy_figpt(tfgpt, bfgpt) == NULL) return 0;

            E = bfgpt->E1 + bfgpt->E2 + bfgpt->E3;
        }
        else if (Et >= E)
        {
            testp = ran1(&idum);
            prob = exp(-(Et - E) / (tfgpt->n_seq * ture));

            if (prob > testp)
            {
                if (dosub == 0) flip++;
                else flips++;

                lastolinb = i;
                prevoliid = oliid;
                newoliid = tfgpt->olisel[i];

                //if ((flips&0xf) == 0) my_set_window_title("Flips i %d exchange %d with %d",i,(int)bfgpt->olisel[i],(int)tfgpt->olisel[i]);
                if (copy_figpt(tfgpt, bfgpt) == NULL) return 0;

                E = bfgpt->E1 + bfgpt->E2 + bfgpt->E3;
            }
        }
    }

    //if (256-dosub != 16) win_printf("dosub %d",dosub);
    itern += 256;
    fill_hybrid_sequences_in_im(imr, oi, bfgpt);
    oi->need_to_refresh = ALL_NEED_REFRESH;
    return refresh_image(imr, imr->n_oi - 1);
}


int do_chg_param(void)
{
    int i;
    float t;

    if (updating_menu_state != 0)    return D_O_K;

    t = ture;
    i = win_scanf("Modify parameter\n"
                  "temperature %8f\n", &t);

    if (i == WIN_CANCEL) return 0;

    ture = t;
    return 0;
}


int optimizing_common_oligos_seq_im(void)
{
    int i = 0;
    int noli;
    static int nsel = 12;
    int oligoSize = 10, no, nmax, invalid;
    pltreg *pr;
    imreg *imr;

    if (updating_menu_state != 0)
    {
        // we wait for a valid sequence to be loaded
        if (asqd == NULL || oligoReg == NULL) active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }

    if (key[KEY_LSHIFT])
        return win_printf_OK("This routine display  a blast plot between two sequences");

    if (ac_grep(cur_ac_reg, "%pr", &pr) != 1)
        return win_printf_OK("cannot find data at %s:%d", __FILE__, __LINE__);

    for (i = 0, nmax = 0; i < n_asqd; i++)
        nmax = (nmax < asqd[i]->totalSize) ? asqd[i]->totalSize : nmax;

    oligoSize = noli = noligoReg;

    for (i = 1; oligoSize > 4; i++, oligoSize /= 4);

    oligoSize = i;

    i = win_scanf("How many oligos %d", &nsel);

    if (i == WIN_CANCEL) return 0;

    find_valid_oligos();
    bfgpt = (figpt *)calloc(1, sizeof(bfgpt));

    if (bfgpt == NULL)
        win_printf_OK("could not allocate fingerprint structure");

    if ((i = allocate_fingerprint(bfgpt, nsel, n_asqd)))
        return (win_printf_OK("could not allocate fingerprint ret %d nsel %d nseq %d", i, nsel, n_asqd));

    for (i = 0; i < nsel; i++)
    {
        for (no = (int)(ran1(&idum) * noli), invalid = 1; invalid > 0; no = (int)(ran1(&idum) * noli))
        {
            if ((no < 0) || (no >= noli)) continue;

            invalid = (oligo_nhib_invalid[no] > n_asqd) ? 1 : 0; //invalid;

            if (invalid == 0) invalid = (fill_fingerprint(bfgpt, i, no)) ? 1 : 0;
        }
    }

    win_printf("put selection\n");
    find_energy_fingerprint(bfgpt);
    tfgpt = copy_figpt(bfgpt, NULL);
    win_printf("found energy\n");
    imr = create_and_register_new_image_project(0,   32,  900,  668);

    if (imr == NULL) return (win_printf_OK("could not create imreg"));

    win_printf("seq to im\n");
    fill_hybrid_sequences_in_im(imr, NULL, bfgpt);
    remove_from_image(imr, imr->o_i[0]->im.data_type, (void *)imr->o_i[0]);
    win_printf("Remov im 0\n");
    select_image_of_imreg(imr, imr->n_oi - 1);
    imr->o_i[0]->oi_idle_action = fingerprint_idle_action;
    return refresh_image(imr, imr->n_oi - 1);
}

//# endif


int convert_oligo_seq(char *message, int npid, int size)
{
    int i, j;

    message[0] = 0;

    for (i = 0; i < size; i++)
    {
        j = (npid & 0x03);

        if (j == 0) message[size - i - 1] = 'A';
        else if (j == 1) message[size - i - 1] = 'C';
        else if (j == 2) message[size - i - 1] = 'G';
        else if (j == 3) message[size - i - 1] = 'T';

        npid >>= 2;
    }

    message[i] = 0;
    return 0;
}


// should work for woobbles and oligos of different size
int  does_oligos_seq_overlap(char *oli1, char *oli2)
{
    int i, j, k;
    int over = 0, size1, size2;
    char ch1, ch2;

    if (oli1 == NULL || oli2 == NULL) return -1000;

    size1 = strlen(oli1);
    size2 = strlen(oli2);


    for (i = k = 0, over = 0; over == 0 && i < size1; i++)
    {
        for (j = 0, over = 1; over > 0 && j < size2 && j < (size1 - i); j++)
        {
            ch1 = oli1[i + j] & 0x5f;
            ch2 = oli2[j] & 0x5f;

            if (ch1 == 'W')
            {
                over = ((ch2 == 'W') || (ch2 == 'A') || (ch2 == 'T')) ? (over + 1) : 0;
            }
            else if (ch1 == 'S')
            {
                over = ((ch2 == 'S') || (ch2 == 'C') || (ch2 == 'G')) ? (over + 1) : 0;
            }
            else if (ch1 == 'M')
            {
                over = ((ch2 == 'K') || (ch2 == 'C') || (ch2 == 'A')) ? (over + 1) : 0;
            }
            else if (ch1 == 'K')
            {
                over = ((ch2 == 'M') || (ch2 == 'T') || (ch2 == 'G')) ? (over + 1) : 0;
            }
            else if (ch1 == 'A')
            {
                over = ((ch2 == 'W') || (ch2 == 'A')) ? (over + 1) : 0;
            }
            else if (ch1 == 'T')
            {
                over = ((ch2 == 'W') || (ch2 == 'T')) ? (over + 1) : 0;
            }
            else if (ch1 == 'C')
            {
                over = ((ch2 == 'S') || (ch2 == 'C')) ? (over + 1) : 0;
            }
            else if (ch1 == 'G')
            {
                over = ((ch2 == 'S') || (ch2 == 'G')) ? (over + 1) : 0;
            }
            else if (ch1 == 'N' || ch2 == 'N')
            {
                over++;
            }
            else over = 0;
        }
    }

    k = (over) ? over - 1 : 0;

    for (i = 0, over = 0; over == 0 && i < size2; i++)
    {
        for (j = 0, over = 1; over > 0 && j < size1 && j < (size2 - i); j++)
        {
            ch1 = oli2[i + j] & 0x5f;
            ch2 = oli1[j] & 0x5f;

            if (ch1 == 'W')
            {
                over = ((ch2 == 'W') || (ch2 == 'A') || (ch2 == 'T')) ? (over + 1) : 0;
            }
            else if (ch1 == 'S')
            {
                over = ((ch2 == 'S') || (ch2 == 'C') || (ch2 == 'G')) ? (over + 1) : 0;
            }
            else if (ch1 == 'M')
            {
                over = ((ch2 == 'K') || (ch2 == 'C') || (ch2 == 'A')) ? (over + 1) : 0;
            }
            else if (ch1 == 'K')
            {
                over = ((ch2 == 'M') || (ch2 == 'T') || (ch2 == 'G')) ? (over + 1) : 0;
            }
            else if (ch1 == 'A')
            {
                over = ((ch2 == 'W') || (ch2 == 'A')) ? (over + 1) : 0;
            }
            else if (ch1 == 'T')
            {
                over = ((ch2 == 'W') || (ch2 == 'T')) ? (over + 1) : 0;
            }
            else if (ch1 == 'C')
            {
                over = ((ch2 == 'S') || (ch2 == 'C')) ? (over + 1) : 0;
            }
            else if (ch1 == 'G')
            {
                over = ((ch2 == 'S') || (ch2 == 'G')) ? (over + 1) : 0;
            }
            else if (ch1 == 'N' || ch2 == 'N')
            {
                over++;
            }
            else over = 0;
        }
    }

    over = (over > 0) ? over - 1 : 0;
    k = (k > over) ? k : -over;
    return k;
}
// test selfoverlapping except on 0 !
int  does_oligo_self_overlap(char *oli1)
{
    int i, j;
    int over = 0, size1;
    char ch1, ch2;

    if (oli1 == NULL) return -1000;

    size1 = strlen(oli1);


    for (i = 1, over = 0; over == 0 && i < size1; i++)
    {
        for (j = 0, over = 1; over > 0 && j < size1 && j < (size1 - i); j++)
        {
            ch1 = oli1[i + j] & 0x5f;
            ch2 = oli1[j] & 0x5f;

            if (ch1 == 'W')
            {
                over = ((ch2 == 'W') || (ch2 == 'A') || (ch2 == 'T')) ? (over + 1) : 0;
            }
            else if (ch1 == 'S')
            {
                over = ((ch2 == 'S') || (ch2 == 'C') || (ch2 == 'G')) ? (over + 1) : 0;
            }
            else if (ch1 == 'M')
            {
                over = ((ch2 == 'K') || (ch2 == 'C') || (ch2 == 'A')) ? (over + 1) : 0;
            }
            else if (ch1 == 'K')
            {
                over = ((ch2 == 'M') || (ch2 == 'T') || (ch2 == 'G')) ? (over + 1) : 0;
            }
            else if (ch1 == 'A')
            {
                over = ((ch2 == 'W') || (ch2 == 'A')) ? (over + 1) : 0;
            }
            else if (ch1 == 'T')
            {
                over = ((ch2 == 'W') || (ch2 == 'T')) ? (over + 1) : 0;
            }
            else if (ch1 == 'C')
            {
                over = ((ch2 == 'S') || (ch2 == 'C')) ? (over + 1) : 0;
            }
            else if (ch1 == 'G')
            {
                over = ((ch2 == 'S') || (ch2 == 'G')) ? (over + 1) : 0;
            }
            else if (ch1 == 'N' || ch2 == 'N')
            {
                over++;
            }
            else over = 0;

            //win_printf("Self overlap on %s, i %d j %d ch1 %c ch2 %c over %d",oli1,i,j,ch1,ch2,over);
        }
    }

    return (over) ? over - 1 : 0;
}



int find_complementary_with_woobble(char *oli, char *comp, int size_c)
{
    int i;
    int size;
    char ch1;

    if (oli == NULL) return 2;

    size = strlen(oli);

    if (comp == NULL)
    {
        comp = strdup(oli);

        if (comp == NULL) return 3;

        size_c = strlen(comp);
    }

    if (size_c < size || size <= 1) return 1;

    for (i = 0; i < size; i++)
    {
        ch1 = oli[i] & 0x5f;

        if (ch1 == 'W')
        {
            comp[size - i - 1] = 'W';
        }
        else if (ch1 == 'S')
        {
            comp[size - i - 1] = 'S';
        }
        else if (ch1 == 'M')
        {
            comp[size - i - 1] = 'K';
        }
        else if (ch1 == 'K')
        {
            comp[size - i - 1] = 'M';
        }
        else if (ch1 == 'A')
        {
            comp[size - i - 1] = 'T';
        }
        else if (ch1 == 'T')
        {
            comp[size - i - 1] = 'A';
        }
        else if (ch1 == 'C')
        {
            comp[size - i - 1] = 'G';
        }
        else if (ch1 == 'G')
        {
            comp[size - i - 1] = 'C';
        }
        else if (ch1 == 'N')
        {
            comp[size - i - 1] = 'N';
        }
        else win_printf("Wrong char %c", ch1);
    }

    comp[size] = 0;
    return 0;
}

int  does_oligos_with_woobble_partially_hybridized(char *oli1, char *oli2)
{
    char comp[32];

    if (oli1 == NULL || oli2 == NULL) return -1000;

    find_complementary_with_woobble(oli2, comp, sizeof(comp));
    return does_oligos_seq_overlap(oli1, comp);
}



int test_ovelap_and_hybridized_with_woobbles_on_file(void)
{
    int i, j, k;
    int  size;
    char seqfilename[2048],  line[256], oligol[64], ch, **loligo = NULL;
    int n_oligo = 0, m_oligo = 0, read, ic, cancel = 0, thres = 2, in_file = 0;
    FILE *fp;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    seqfilename[0] = 0;

    if (do_select_file(seqfilename, sizeof(seqfilename), "TXT-FILE", "*.txt", "File to load oligo set\0", 1))
        return win_printf_OK("Cannot select input file");

    fp = fopen(seqfilename, "r");

    if (fp == NULL)  win_printf_OK("Cannot open file:\n%s", backslash_to_slash(seqfilename));

    loligo = (char **)calloc(m_oligo = 256, sizeof(char *));

    if (loligo == NULL) return win_printf_OK("Cannot allocate loligo");

    for (n_oligo = 0, read = 1, size = -1; read == 1;)
    {
        read = fscanf(fp, "%s", line);

        if (read == 1)
        {
            if (n_oligo >= m_oligo)
            {
                m_oligo += 256;
                loligo = (char **)realloc(loligo, m_oligo * sizeof(char *));

                if (loligo == NULL) return win_printf_OK("Cannot allocate loligo");
            }

            for (j = 0, ic = 0; (ch = line[j]) != 0; j++)
            {
                // loop scanning characters in line
                switch (ch)
                {
                // we test character
                case 'a':
                case 't':
                case 'g':
                case 'c':
                case 'n':
                case 'A':
                case 'T':
                case 'G':
                case 'C':
                case 'N':
                case 's':
                case 'S':
                case 'w':
                case 'W':
                case 'm':
                case 'M':
                case 'k':
                case 'K':
                    oligol[ic++] = ch;
                    break;

                default:
                    break;
                }
            } // end of loop scanning characters in line

            if (ic > 0)
            {
                oligol[ic] = 0;

                if (size < 0) size = ic;

                if (size != ic) return win_printf_OK("Your oligos differ in size");

                loligo[n_oligo] = strdup(oligol);
                n_oligo++;
            }
        }
    }

    fclose(fp);
    win_printf("%d oligos loaded size %d\n"
               "First: %s\nLast %s\n", n_oligo, size, loligo[0], loligo[n_oligo - 1]);

    i = win_scanf("I am going to scan oligos for overlapping or partailly hybridization\n"
                  "Do you want to display result on screen %R or dump result in a file %r\n"
                  "For partailly hybridization specify the threshold above witch\n this is a pb %5d\n"
                  , &in_file, &thres);

    if (in_file)
    {
        if (do_select_file(seqfilename, sizeof(seqfilename), "TXT-FILE", "*.txt", "File to save analysis\0", 0))
            return win_printf_OK("Cannot select output file");

        fp = fopen(seqfilename, "w");

        if (fp == NULL)  win_printf_OK("Cannot open file:\n%s", backslash_to_slash(seqfilename));
    }



    for (i = 0; i < n_oligo; i++)
    {
        for (j = 0; j < n_oligo; j++)
        {
            k = (j == i) ? does_oligo_self_overlap(loligo[i])
                : does_oligos_seq_overlap(loligo[i], loligo[j]);

            if (k && cancel != WIN_CANCEL)
            {
                if (in_file)    fprintf(fp, "Oligos %d-> %s and %d -> %s overlap by %d\n"
                                            , i, loligo[i], j, loligo[j], k);
                else cancel = win_printf("Oligos %d-> %s and %d -> %s overlap by %d\n"
                                             "To suppress futher message press WIN_CANCEL"
                                             , i, loligo[i], j, loligo[j], k);
            }
        }
    }

    cancel = 0;

    for (i = 0; i < n_oligo; i++)
    {
        for (j = 0; j < n_oligo; j++)
        {
            k = does_oligos_with_woobble_partially_hybridized(loligo[i], loligo[j]);

            if ((abs(k) > thres) && (cancel != WIN_CANCEL))
            {
                if (in_file)
                    fprintf(fp, "Oligos %d-> %s and %d -> %s partially hybridized by %d\n"
                            , i, loligo[i], j, loligo[j], k);
                else cancel = win_printf("Oligos %d-> %s and %d -> %s partially hybridized by %d\n"
                                             "To suppress futher message press WIN_CANCEL", i, loligo[i], j, loligo[j], k);
            }
        }
    }

    if (in_file) fclose(fp);

    return 0;
}








int  does_oligos_overlap(int oli1, int oli2, int size)
{
    int i;
    int over = 0, dx, mask, b, size2;

    size2 = 2 * size;

    for (i = 0, mask = 0; i < size2; i += 2) mask |= (0x3 << i);

    b = mask;

    for (i = 0; over == 0 && i < size2; i += 2)
    {
        over = ((mask & oli1) == (oli2 >> i)) ? 1 : over;
        mask >>= 2;
        dx = (size2 - i) / 2;
    }

    if (over) return dx;

    mask = b;

    for (i = 0; over == 0 && i < size2; i += 2)
    {
        over = ((mask & oli2) == (oli1 >> i)) ? 1 : over;
        mask >>= 2;
        dx = (size2 - i) / 2;
    }

    return (over) ? -dx : 0;
}


int  does_oligos_partially_hybridized(int oli1, int oli2, int size)
{
    int i, k;
    int  mask, size2, invert; // over = 0, b,
    //  char oli[32], comp[32];

    size2 = 2 * size;

    for (i = 0, mask = 0; i < size2; i += 2) mask |= (0x3 << i);

    //b = mask;
    for (i = 0, invert = 0, k = oli2; i < size2; i += 2, k >>= 2)
        invert |= ((k & 0x3) ^ 0x3) << (size2 - 2 - i);

    invert &= mask;
    //convert_oligoID_to_seq(oli, oli2, size);
    //convert_oligoID_to_seq(comp, invert, size);
    //win_printf("Oligo %s complementary %s",oli,comp);
    return does_oligos_overlap(oli1, invert, size);
    /*
       for (i = 0; over == 0 && i < size2; i += 2)
       {
       over = (((mask & oli1)^(invert >> i)) == mask) ? 1 : over;
       mask >>= 2;
       }
       if (over) return i/2;
       mask = b;
       for (i = 0; over == 0 && i < size2; i += 2)
       {
       over = (((mask & invert)^(oli1 >> i)) == mask) ? 1 : over;
       mask >>= 2;
       }
       return -(i/2);
       */
}



int test_ovelap_and_hybridized(void)
{
    int i;
    int  size,  ioli1, ioli2, selfo1, selfo2, selfh1, selfh2, intero, interh;
    static int first = 1;
    static char oli1[32], oli2[32];


    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (first == 1)
    {
        oli1[0] = oli2[0] = 0;
        first = 0;
    }

    if (key[KEY_LSHIFT])
        return win_printf_OK("This is internal test over overlapping and hybridization\n");

    i = win_scanf("This is internal test over overlapping and hybridization\n"
                  "Oligo 1 %24s\n"
                  "Oligo 2 %24s\n", oli1, oli2);

    if (i == WIN_CANCEL) return 0;

    selfo1 = does_oligo_self_overlap(oli1);
    selfo2 = does_oligo_self_overlap(oli2);
    selfh1 = does_oligos_with_woobble_partially_hybridized(oli1, oli1);
    selfh2 = does_oligos_with_woobble_partially_hybridized(oli2, oli2);
    intero = does_oligos_seq_overlap(oli1, oli2);
    interh = does_oligos_with_woobble_partially_hybridized(oli1, oli2);

    win_printf_OK("Oligo 1 %s\n: selfoverlap by %d and self hybridized by %d\n"
                  "Oligo 2 %s\n: selfoverlap by %d and self hybridized by %d\n"
                  "Oligo 1 and 2 overlap = %d \npartially hybridize %d"
                  , oli1, selfo1, selfh1, oli2, selfo2, selfh2, intero, interh);


    size = strlen(oli2);

    if ((int)strlen(oli1) != size) return win_printf_OK("Your oligos differ in size");

    ioli1 = convert_oligo_seq_to_ID(oli1, size);
    ioli2 = convert_oligo_seq_to_ID(oli2, size);
    return win_printf_OK("Oligos %s and %s\noverlap = %d \npartially hybridize %d"
                         , oli1, oli2, does_oligos_overlap(ioli1, ioli2, size)
                         , does_oligos_partially_hybridized(ioli1, ioli2, size));
}



int test_ovelap_and_hybridized_on_file(void)
{
    int i, j, k;
    int  size;
    static char oli1[32], oli2[32];
    char seqfilename[2048],  line[256], oligol[64], ch;
    int n_oligo = 0, m_oligo = 0, *ioligo = NULL, read, ic, cancel = 0, thres = 2, in_file = 0;
    FILE *fp;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    seqfilename[0] = 0;

    if (do_select_file(seqfilename, sizeof(seqfilename), "TXT-FILE", "*.txt", "File to load oligo set\0", 1))
        return win_printf_OK("Cannot select input file");

    fp = fopen(seqfilename, "r");

    if (fp == NULL)  win_printf_OK("Cannot open file:\n%s", backslash_to_slash(seqfilename));

    ioligo = (int *)calloc(m_oligo = 256, sizeof(int));

    if (ioligo == NULL) return win_printf_OK("Cannot allocate ioligo");

    for (n_oligo = 0, read = 1, size = -1; read == 1;)
    {
        read = fscanf(fp, "%s", line);

        if (read == 1)
        {
            if (n_oligo >= m_oligo)
            {
                m_oligo += 256;
                ioligo = (int *)realloc(ioligo, m_oligo * sizeof(int));

                if (ioligo == NULL) return win_printf_OK("Cannot allocate ioligo");
            }

            for (j = 0, ic = 0; (ch = line[j]) != 0; j++)
            {
                // loop scanning characters in line
                switch (ch)
                {
                // we test character
                case 'a':
                case 't':
                case 'g':
                case 'c':
                case 'n':
                case 'A':
                case 'T':
                case 'G':
                case 'C':
                case 'N':
                    oligol[ic++] = ch;
                    break;

                default:
                    break;
                }
            } // end of loop scanning characters in line

            if (ic > 0)
            {
                oligol[ic] = 0;

                if (size < 0) size = ic;

                if (size != ic) return win_printf_OK("Your oligos differ in size");

                ioligo[n_oligo] = convert_oligo_seq_to_ID(oligol, size);
                /*
                   if (n_oligo == 0)
                   {
                   convert_oligoID_to_seq(oli1, ioligo[0], size);
                   win_printf("line %s\noligo %s -> ID 0x%X\n->%s",line,oligo,ioligo[0],oli1);
                   }
                   */
                n_oligo++;
            }
        }
    }

    fclose(fp);
    convert_oligoID_to_seq(oli1, ioligo[0], size);
    convert_oligoID_to_seq(oli2, ioligo[n_oligo - 1], size);
    win_printf("%d oligos loaded size %d\n"
               "First: %s\nLast %s\n", n_oligo, size, oli1, oli2);

    i = win_scanf("I am going to scan oligos for overlapping or partailly hybridization\n"
                  "Do you want to display result on screen %R or dump result in a file %r\n"
                  "For partailly hybridization specify the threshold above witch\n this is a pb %5d\n"
                  , &in_file, &thres);

    if (in_file)
    {
        if (do_select_file(seqfilename, sizeof(seqfilename), "TXT-FILE", "*.txt", "File to save analysis\0", 0))
            return win_printf_OK("Cannot select output file");

        fp = fopen(seqfilename, "w");

        if (fp == NULL)  win_printf_OK("Cannot open file:\n%s", backslash_to_slash(seqfilename));
    }



    for (i = 0; i < n_oligo; i++)
    {
        for (j = 0; j < n_oligo; j++)
        {
            if (j == i) break;

            k = does_oligos_overlap(ioligo[i], ioligo[j], size);

            if (k && cancel != WIN_CANCEL)
            {
                convert_oligoID_to_seq(oli1, ioligo[i], size);
                convert_oligoID_to_seq(oli2, ioligo[j], size);

                if (in_file)    fprintf(fp, "Oligos %d-> %s and %d -> %s overlap by %d\n", i, oli1, j, oli2, k);
                else cancel = win_printf("Oligos %d-> %s and %d -> %s overlap by %d\n"
                                             "To suppress futher message press WIN_CANCEL", i, oli1, j, oli2, k);
            }
        }
    }

    cancel = 0;

    for (i = 0; i < n_oligo; i++)
    {
        for (j = 0; j < n_oligo; j++)
        {
            k = does_oligos_partially_hybridized(ioligo[i], ioligo[j], size);

            if ((abs(k) > thres) && (cancel != WIN_CANCEL))
            {
                convert_oligoID_to_seq(oli1, ioligo[i], size);
                convert_oligoID_to_seq(oli2, ioligo[j], size);

                if (in_file)    fprintf(fp, "Oligos %d-> %s and %d -> %s partially hybridized by %d\n", i, oli1, j, oli2, k);
                else cancel = win_printf("Oligos %d-> %s and %d -> %s partially hybridized by %d\n"
                                             "To suppress futher message press WIN_CANCEL", i, oli1, j, oli2, k);
            }
        }
    }

    if (in_file) fclose(fp);

    return 0;
}



omul *load_multiplex_oligo_set_from_file(char *filename)
{
    int i, j;
    int  size, set_nb;
    unsigned long long kl;
    char seqfilename[2048],  line[256], oligol[64], set[32], ch;
    int n_oligo = 0, read, ic, sc, n_setID = 0;
    FILE *fp;
    omul *loli_mul_set = NULL;

    seqfilename[0] = 0;

    if (filename != NULL && strlen(filename) > 3)
    {
        for (i = 0; filename[i] != 0 && i < (int) sizeof(seqfilename); i++) seqfilename[i] = filename[i];
    }
    else
    {
        if (do_select_file(seqfilename, sizeof(seqfilename), "TXT-FILE", "*.txt", "File to load oligo set\0", 1))
            return (omul *)win_printf_ptr("Cannot select input file");
    }

    fp = fopen(seqfilename, "r");

    if (fp == NULL)  win_printf_ptr("Cannot open file:\n%s", backslash_to_slash(seqfilename));

    // first reading to test file format
    for (n_oligo = 0, read = 1, set_nb = size = -1; read == 1;)
    {
        read = (fgets(line, sizeof(line), fp) == NULL) ? 0 : 1;

        //read = fscanf(fp,"%s",line);
        if (read == 1)
        {
            for (j = 0, ic = sc = 0; (ch = line[j]) != 0; j++)
            {
                // loop scanning characters in line
                switch (ch)
                {
                // we test character
                case 'a':
                case 't':
                case 'g':
                case 'c':
                case 'n':
                case 'A':
                case 'T':
                case 'G':
                case 'C':
                case 'N':
                    oligol[ic++] = ch;
                    break;

                case '0':
                case '1':
                    set[sc++] = ch;
                    break;

                default:
                    break;
                }
            } // end of loop scanning characters in line

            if (ic > 0 && sc > 0)
            {
                if (size < 0) size = ic;

                if (size != ic) return (omul *) win_printf_ptr("Your oligos differ in size");

                if (set_nb < 0) set_nb = sc;

                if (set_nb != sc) return (omul *) win_printf_ptr("Your oligos setID differ in size");

                n_oligo++;
                n_setID++;
            }
            else if (ic > 0)
            {
                if (size < 0) size = ic;

                if (size != ic) return (omul *) win_printf_ptr("Your oligos differ in size");

                n_oligo++;
            }
            else win_printf("pb at line %s", line);
        }
    }

    fclose(fp);

    if (n_oligo < 0 || set_nb < 0) return (omul *) win_printf_ptr("Set_nb %d n_oligo %d!", set_nb, n_oligo);

    loli_mul_set = (omul *)calloc(1, sizeof(struct _oligo_multiplex));

    if (loli_mul_set == NULL) return (omul *) win_printf_ptr("Cannot allocate omul");

    loli_mul_set->o_size = size;
    loli_mul_set->set_nb = set_nb;
    loli_mul_set->n_oligo = n_oligo;
    loli_mul_set->m_oligo = n_oligo;
    loli_mul_set->n_setID = n_setID;
    loli_mul_set->oligo_nb = (int *)calloc(n_oligo, sizeof(int));

    if (loli_mul_set->oligo_nb == NULL) return (omul *) win_printf_ptr("Cannot allocate omul %d oligo_nb", n_oligo);

    loli_mul_set->ioligo = (int *)calloc(n_oligo, sizeof(int));

    if (loli_mul_set->ioligo == NULL) return (omul *) win_printf_ptr("Cannot allocate omul %d ioligo", n_oligo);

    loli_mul_set->ioligo_trial = (int *)calloc(n_oligo, sizeof(int));

    if (loli_mul_set->ioligo_trial == NULL) return (omul *) win_printf_ptr("Cannot allocate omul %d ioligo_trial", n_oligo);

    loli_mul_set->setID = (unsigned long long *)calloc(n_setID, sizeof(unsigned long long));

    if (loli_mul_set->setID == NULL) return (omul *) win_printf_ptr("Cannot allocate omul %d setID", n_setID);

    loli_mul_set->set_n = (int *)calloc(set_nb, sizeof(int));

    loli_mul_set->overlap = (int *)calloc(size + 1, sizeof(int));

    if (loli_mul_set->overlap == NULL) return (omul *) win_printf_ptr("Cannot allocate omul %d overlap", size + 1);

    loli_mul_set->partial_hybridized = (int *)calloc(size + 1, sizeof(int));

    if (loli_mul_set->partial_hybridized == NULL) return (omul *)
                win_printf_ptr("Cannot allocate omul %d partial_hybridized", size + 1);

    loli_mul_set->overlap_trial = (int *)calloc(size + 1, sizeof(int));

    if (loli_mul_set->overlap_trial == NULL) return (omul *) win_printf_ptr("Cannot allocate omul %d overlap trial",
                size + 1);

    loli_mul_set->partial_hybridized_trial = (int *)calloc(size + 1, sizeof(int));

    if (loli_mul_set->partial_hybridized_trial == NULL) return (omul *)
                win_printf_ptr("Cannot allocate omul %d partial_hybridized trial", size + 1);

    if (loli_mul_set->set_n == NULL) return (omul *) win_printf_ptr("Cannot allocate omul %d set_n", set_nb);

    loli_mul_set->set = (int ** *)calloc(set_nb, sizeof(int **));

    if (loli_mul_set->set == NULL) return (omul *) win_printf_ptr("Cannot allocate omul %d set ", set_nb);

    for (i = 0, j = 1; i < set_nb; i++, j *= 2);

    for (i = 0; i < set_nb; i++)
    {
        loli_mul_set->set[i] = (int **)calloc(j, sizeof(int *));

        if (loli_mul_set->set[i] == NULL) return (omul *) win_printf_ptr("Cannot allocate omul set %d size %d", i, j);
    }

    fp = fopen(seqfilename, "r");

    if (fp == NULL)  win_printf_ptr("Cannot open file:\n%s", backslash_to_slash(seqfilename));

    // no we read data
    for (read = 1, n_oligo = 0, n_setID = 0; read == 1;)
    {
        read = (fgets(line, sizeof(line), fp) == NULL) ? 0 : 1;

        //read = fscanf(fp,"%s",line);
        if (read == 1)
        {
            for (j = 0, ic = sc = 0; (ch = line[j]) != 0; j++)
            {
                // loop scanning characters in line
                switch (ch)
                {
                // we test character
                case 'a':
                case 't':
                case 'g':
                case 'c':
                case 'n':
                case 'A':
                case 'T':
                case 'G':
                case 'C':
                case 'N':
                    oligol[ic++] = ch;
                    break;

                case '0':
                case '1':
                    set[sc++] = ch;
                    break;

                default:
                    break;
                }
            } // end of loop scanning characters in line

            if (ic > 0 && sc > 0)
            {
                loli_mul_set->oligo_nb[n_oligo] = n_oligo;
                loli_mul_set->ioligo[n_oligo] = convert_oligo_seq_to_ID(oligol, loli_mul_set->o_size);

                for (j = 0, kl = 0; j < set_nb; j++)
                {
                    if (set[set_nb - j - 1] == '1')
                    {
                        loli_mul_set->set[j][loli_mul_set->set_n[j]] = loli_mul_set->oligo_nb + n_oligo;
                        (loli_mul_set->set_n[j])++;
                        kl |= (0x1) << (set_nb - j - 1);
                    }
                }

                n_oligo++;
                loli_mul_set->setID[n_setID++] = kl;
            }
            else if (ic > 0)
            {
                loli_mul_set->oligo_nb[n_oligo] = n_oligo;
                loli_mul_set->ioligo[n_oligo++] = convert_oligo_seq_to_ID(oligol, loli_mul_set->o_size);
            }
            else win_printf("pb at line %s", line);
        }
    }

    fclose(fp);
    convert_oligoID_to_seq(oligol, loli_mul_set->ioligo[0], loli_mul_set->o_size);
    win_printf("Loaded %d oligos of size %d with %d labels in %d sets\n"
               "Oligo 0 ->%s ID 0x%I64X\n"
               "Set 1 has %d oligos\n"
               "Set 2 has %d oligos\n"
               "Set 3 has %d oligos\n"
               "Set 4 has %d oligos\n"
               "Set 5 has %d oligos\n"
               "Set 6 has %d oligos\n"
               , n_oligo, loli_mul_set->o_size, n_setID, set_nb, oligol, loli_mul_set->setID[0], loli_mul_set->set_n[0]
               , loli_mul_set->set_n[1]
               , loli_mul_set->set_n[2], loli_mul_set->set_n[3], loli_mul_set->set_n[4], loli_mul_set->set_n[5]);
    return loli_mul_set;
}



int dump_multiplex_oligo_set_analysis_to_file(omul *loli_mul_set, int thres, char *filename)
{
    int i, j, k;
    int l, m, over;
    unsigned long long bin;
    char seqfilename[2048], oli1[32], oli2[32];
    FILE *fp;

    if (loli_mul_set == NULL || loli_mul_set->set_nb <= 0 || loli_mul_set->n_oligo <= 0) return 1;

    seqfilename[0] = 0;

    if (filename != NULL && strlen(filename) > 3)
    {
        for (i = 0; filename[i] != 0 && i < (int) sizeof(seqfilename); i++) seqfilename[i] = filename[i];
    }
    else
    {
        if (do_select_file(seqfilename, sizeof(seqfilename), "OLI-SET-ANALYSIS", "*.txt", "File to save oligos set analysis\0",
                           0))
            return win_printf_OK("Cannot select output file");
    }

    fp = fopen(seqfilename, "w");

    if (fp == NULL)  win_printf_OK("Cannot open file:\n%s", backslash_to_slash(seqfilename));

    for (i = 0; i <= loli_mul_set->o_size; i++) loli_mul_set->overlap[i] = loli_mul_set->partial_hybridized[i] = 0;

    for (i = 0; i < loli_mul_set->set_nb; i++)
    {
        fprintf(fp, "\nOligo set %d\n\n", i);

        for (j = 0; j < loli_mul_set->set_n[i]; j++)
        {
            k = *(loli_mul_set->set[i][j]);
            convert_oligoID_to_seq(oli1, loli_mul_set->ioligo[k], loli_mul_set->o_size);
            bin = loli_mul_set->setID[k];

            for (l = 0; l < loli_mul_set->set_nb; l++)
                oli2[l] = (bin & (0x1 << l)) ? '1' : '0';

            oli2[loli_mul_set->set_nb] = 0;
            fprintf(fp, "%s\t%s\n", oli2, oli1);
        }

        fprintf(fp, "\n");

        for (j = 0; j < loli_mul_set->set_n[i]; j++)
        {
            k = *(loli_mul_set->set[i][j]);

            for (l = 0; l < loli_mul_set->set_n[i]; l++)
            {
                if (j == l) break;

                m = *(loli_mul_set->set[i][l]);
                over = does_oligos_overlap(loli_mul_set->ioligo[k], loli_mul_set->ioligo[m], loli_mul_set->o_size);

                if (abs(over) <= loli_mul_set->o_size) loli_mul_set->overlap[abs(over)]++;

                if (over)
                {
                    convert_oligoID_to_seq(oli1, loli_mul_set->ioligo[k], loli_mul_set->o_size);
                    convert_oligoID_to_seq(oli2, loli_mul_set->ioligo[m], loli_mul_set->o_size);
                    fprintf(fp, "Oligos %d-> %s and %d -> %s overlap by %d\n", k, oli1, m, oli2, over);
                }
            }
        }

        fprintf(fp, "\n");

        for (j = 0; j < loli_mul_set->set_n[i]; j++)
        {
            k = *(loli_mul_set->set[i][j]);

            for (l = 0; l < loli_mul_set->set_n[i]; l++)
            {
                m = *(loli_mul_set->set[i][l]);
                over = does_oligos_partially_hybridized(loli_mul_set->ioligo[k], loli_mul_set->ioligo[m], loli_mul_set->o_size);

                if (abs(over) <= loli_mul_set->o_size) loli_mul_set->partial_hybridized[abs(over)]++;

                if (abs(over) > thres)
                {
                    convert_oligoID_to_seq(oli1, loli_mul_set->ioligo[k], loli_mul_set->o_size);
                    convert_oligoID_to_seq(oli2, loli_mul_set->ioligo[m], loli_mul_set->o_size);
                    fprintf(fp, "Oligos %d-> %s and %d -> %s partially hybridized by %d\n", k, oli1, m, oli2, over);
                }
            }
        }
    }

    fclose(fp);
    return 0;
}

int dump_multiplex_oligo_set_analysis_to_screen(omul *loli_mul_set, int thres)
{
    int i, j, k;
    int l, m, over;
    char message[2048], oli1[32], oli2[32];

    if (loli_mul_set == NULL || loli_mul_set->set_nb <= 0 || loli_mul_set->n_oligo <= 0) return 1;

    for (i = 0; i <= loli_mul_set->o_size; i++) loli_mul_set->overlap[i] = loli_mul_set->partial_hybridized[i] = 0;

    for (i = 0; i < loli_mul_set->set_nb; i++)
    {
        snprintf(message, sizeof(message), "Oligo set %d\n\n", i);

        /*
           for (j = 0; j < loli_mul_set->set_n[i]; j++)
           {
           k = *(loli_mul_set->set[i][j]);
           convert_oligoID_to_seq(oli1, loli_mul_set->ioligo[k], loli_mul_set->o_size);
           bin = loli_mul_set->setID[k];
           for (l = 0; l < loli_mul_set->set_nb; l++)
           oli2[l] = (bin&(0x1<<l)) ? '1' : '0';
           oli2[loli_mul_set->set_nb] = 0;
           snprintf(message+strlen(message)+strlen(message),sizeof(message)-strlen(message)
           ,"%s\t%s\n",oli2,oli1);
           }
           snprintf(message+strlen(message),sizeof(message)-strlen(message),"\n");
           */
        for (j = 0; j < loli_mul_set->set_n[i]; j++)
        {
            k = *(loli_mul_set->set[i][j]);

            for (l = 0; l < loli_mul_set->set_n[i]; l++)
            {
                if (j == l) break;

                m = *(loli_mul_set->set[i][l]);
                over = does_oligos_overlap(loli_mul_set->ioligo[k], loli_mul_set->ioligo[m], loli_mul_set->o_size);

                if (abs(over) <= loli_mul_set->o_size) loli_mul_set->overlap[abs(over)]++;

                if (over)
                {
                    convert_oligoID_to_seq(oli1, loli_mul_set->ioligo[k], loli_mul_set->o_size);
                    convert_oligoID_to_seq(oli2, loli_mul_set->ioligo[m], loli_mul_set->o_size);
                    snprintf(message + strlen(message) + strlen(message), sizeof(message) - strlen(message)
                             , "Oligos %d-> %s and %d -> %s overlap by %d\n", k, oli1, m, oli2, over);
                }
            }
        }

        snprintf(message + strlen(message), sizeof(message) - strlen(message), "\n");

        for (j = 0; j < loli_mul_set->set_n[i]; j++)
        {
            k = *(loli_mul_set->set[i][j]);

            for (l = 0; l < loli_mul_set->set_n[i]; l++)
            {
                m = *(loli_mul_set->set[i][l]);
                over = does_oligos_partially_hybridized(loli_mul_set->ioligo[k], loli_mul_set->ioligo[m], loli_mul_set->o_size);

                if (abs(over) <= loli_mul_set->o_size) loli_mul_set->partial_hybridized[abs(over)]++;

                if (abs(over) > thres)
                {
                    convert_oligoID_to_seq(oli1, loli_mul_set->ioligo[k], loli_mul_set->o_size);
                    convert_oligoID_to_seq(oli2, loli_mul_set->ioligo[m], loli_mul_set->o_size);
                    snprintf(message + strlen(message), sizeof(message) - strlen(message)
                             , "Oligos %d-> %s and %d -> %s partially hybridized by %d\n", k, oli1, m, oli2, over);
                }
            }
        }

        win_printf("%s",message);
    }

    return 0;
}




int dump_multiplex_oligo_set_inter_analysis_to_screen(omul *loli_mul_set, int thres)
{
    int i, j, k;
    int l, m, over, i2;
    char message[2048], oli1[32], oli2[32];

    if (loli_mul_set == NULL || loli_mul_set->set_nb <= 0 || loli_mul_set->n_oligo <= 0) return 1;

    for (i = 0; i < loli_mul_set->set_nb; i++)
    {
        for (i2 = i + 1; i2 < loli_mul_set->set_nb; i2++)
        {
            snprintf(message, sizeof(message), "Oligo set %d inter with %d\n\n", i, i2);

            for (j = 0; j < loli_mul_set->set_n[i]; j++)
            {
                k = *(loli_mul_set->set[i][j]);

                for (l = 0; l < loli_mul_set->set_n[i2]; l++)
                {
                    m = *(loli_mul_set->set[i2][l]);
                    over = does_oligos_overlap(loli_mul_set->ioligo[k], loli_mul_set->ioligo[m], loli_mul_set->o_size);

                    if (abs(over) <= loli_mul_set->o_size) loli_mul_set->overlap[abs(over)]++;

                    if (over)
                    {
                        convert_oligoID_to_seq(oli1, loli_mul_set->ioligo[k], loli_mul_set->o_size);
                        convert_oligoID_to_seq(oli2, loli_mul_set->ioligo[m], loli_mul_set->o_size);
                        snprintf(message + strlen(message) + strlen(message), sizeof(message) - strlen(message)
                                 , "Oligos %d-%d-> %s and %d-%d -> %s overlap by %d\n", i, k, oli1, i2, m, oli2, over);
                    }
                }
            }

            snprintf(message + strlen(message), sizeof(message) - strlen(message), "\n");

            for (j = 0; j < loli_mul_set->set_n[i]; j++)
            {
                k = *(loli_mul_set->set[i][j]);

                for (l = 0; l < loli_mul_set->set_n[i2]; l++)
                {
                    m = *(loli_mul_set->set[i2][l]);
                    over = does_oligos_partially_hybridized(loli_mul_set->ioligo[k], loli_mul_set->ioligo[m], loli_mul_set->o_size);

                    if (abs(over) <= loli_mul_set->o_size) loli_mul_set->partial_hybridized[abs(over)]++;

                    if (abs(over) > thres)
                    {
                        convert_oligoID_to_seq(oli1, loli_mul_set->ioligo[k], loli_mul_set->o_size);
                        convert_oligoID_to_seq(oli2, loli_mul_set->ioligo[m], loli_mul_set->o_size);
                        snprintf(message + strlen(message), sizeof(message) - strlen(message)
                                 , "Oligos %d-%d-> %s and %d-%d -> %s partially hybridized by %d\n", i, k, oli1, i2, m, oli2, over);
                    }
                }
            }

            win_printf("%s",message);
        }
    }

    return 0;
}

int dump_multiplex_oligo_set_inter_analysis_to_file(omul *loli_mul_set, int thres, char *filename)
{
    int i, j, k;
    int l, m, over, i2;
    char seqfilename[2048], oli1[32], oli2[32];
    FILE *fp;

    if (loli_mul_set == NULL || loli_mul_set->set_nb <= 0 || loli_mul_set->n_oligo <= 0) return 1;

    seqfilename[0] = 0;

    if (filename != NULL && strlen(filename) > 3)
    {
      for (i = 0; filename[i] != 0 && i < (int)sizeof(seqfilename); i++) seqfilename[i] = filename[i];
    }
    else
    {
        if (do_select_file(seqfilename, sizeof(seqfilename), "OLI-SET-ANALYSIS", "*.txt", "File to save oligos set analysis\0",
                           0))
            return win_printf_OK("Cannot select output file");
    }

    fp = fopen(seqfilename, "w");

    if (fp == NULL)  win_printf_OK("Cannot open file:\n%s", backslash_to_slash(seqfilename));

    for (i = 0; i < loli_mul_set->set_nb; i++)
    {
        for (i2 = i + 1; i2 < loli_mul_set->set_nb; i2++)
        {
            fprintf(fp, "Oligo set %d inter with %d\n\n", i, i2);

            for (j = 0; j < loli_mul_set->set_n[i]; j++)
            {
                k = *(loli_mul_set->set[i][j]);

                for (l = 0; l < loli_mul_set->set_n[i2]; l++)
                {
                    m = *(loli_mul_set->set[i2][l]);
                    over = does_oligos_overlap(loli_mul_set->ioligo[k], loli_mul_set->ioligo[m], loli_mul_set->o_size);

                    if (abs(over) <= loli_mul_set->o_size) loli_mul_set->overlap[abs(over)]++;

                    if (over)
                    {
                        convert_oligoID_to_seq(oli1, loli_mul_set->ioligo[k], loli_mul_set->o_size);
                        convert_oligoID_to_seq(oli2, loli_mul_set->ioligo[m], loli_mul_set->o_size);
                        fprintf(fp, "Oligos %d-%d-> %s and %d-%d -> %s overlap by %d\n", i, k, oli1, i2, m, oli2, over);
                    }
                }
            }

            fprintf(fp, "\n");

            for (j = 0; j < loli_mul_set->set_n[i]; j++)
            {
                k = *(loli_mul_set->set[i][j]);

                for (l = 0; l < loli_mul_set->set_n[i2]; l++)
                {
                    m = *(loli_mul_set->set[i2][l]);
                    over = does_oligos_partially_hybridized(loli_mul_set->ioligo[k], loli_mul_set->ioligo[m], loli_mul_set->o_size);

                    if (abs(over) <= loli_mul_set->o_size) loli_mul_set->partial_hybridized[abs(over)]++;

                    if (abs(over) > thres)
                    {
                        convert_oligoID_to_seq(oli1, loli_mul_set->ioligo[k], loli_mul_set->o_size);
                        convert_oligoID_to_seq(oli2, loli_mul_set->ioligo[m], loli_mul_set->o_size);
                        fprintf(fp, "Oligos %d-%d-> %s and %d-%d -> %s partially hybridized by %d\n", i, k, oli1, i2, m, oli2, over);
                    }
                }
            }
        }
    }

    fclose(fp);
    return 0;
}





int multiplex_oligo_set_analysis(omul *loli_mul_set, int trial)
{
    int i, j, k;
    int l, m, over;

    if (loli_mul_set == NULL || loli_mul_set->set_nb <= 0 || loli_mul_set->n_oligo <= 0) return 1;

    if (trial)
    {
        for (i = 0; i <= loli_mul_set->o_size; i++) loli_mul_set->overlap_trial[i] = loli_mul_set->partial_hybridized_trial[i] = 0;
    }
    else
    {
        for (i = 0; i <= loli_mul_set->o_size; i++) loli_mul_set->overlap[i] = loli_mul_set->partial_hybridized[i] = 0;
    }

    for (i = 0; i < loli_mul_set->set_nb; i++)
    {
        for (j = 0; j < loli_mul_set->set_n[i]; j++)
        {
            k = *(loli_mul_set->set[i][j]);

            for (l = 0; l < loli_mul_set->set_n[i]; l++)
            {
                if (j == l) break;

                m = *(loli_mul_set->set[i][l]);

                if (trial)
                {
                    over = does_oligos_overlap(loli_mul_set->ioligo_trial[k], loli_mul_set->ioligo_trial[m], loli_mul_set->o_size);

                    if (abs(over) <= loli_mul_set->o_size) loli_mul_set->overlap_trial[abs(over)]++;
                }
                else
                {
                    over = does_oligos_overlap(loli_mul_set->ioligo[k], loli_mul_set->ioligo[m], loli_mul_set->o_size);

                    if (abs(over) <= loli_mul_set->o_size) loli_mul_set->overlap[abs(over)]++;
                }
            }
        }

        for (j = 0; j < loli_mul_set->set_n[i]; j++)
        {
            k = *(loli_mul_set->set[i][j]);

            for (l = 0; l < loli_mul_set->set_n[i]; l++)
            {
                m = *(loli_mul_set->set[i][l]);

                if (trial)
                {
                    over = does_oligos_partially_hybridized(loli_mul_set->ioligo_trial[k], loli_mul_set->ioligo_trial[m],
                                                            loli_mul_set->o_size);

                    if (abs(over) <= loli_mul_set->o_size) loli_mul_set->partial_hybridized_trial[abs(over)]++;
                }
                else
                {
                    over = does_oligos_partially_hybridized(loli_mul_set->ioligo[k], loli_mul_set->ioligo[m], loli_mul_set->o_size);

                    if (abs(over) <= loli_mul_set->o_size) loli_mul_set->partial_hybridized[abs(over)]++;
                }
            }
        }
    }

    return 0;
}



int dump_multiplex_oligo_set_to_file(omul *loli_mul_set, char *filename)
{
    int i, j, k;
    int l;
    unsigned long long bin;
    char seqfilename[2048], oligol[32], ch[32];
    FILE *fp;

    if (loli_mul_set == NULL || loli_mul_set->set_nb <= 0 || loli_mul_set->n_oligo <= 0) return 1;

    seqfilename[0] = 0;

    if (filename != NULL && strlen(filename) > 3)
    {
      for (i = 0; filename[i] != 0 && i < (int)sizeof(seqfilename); i++) seqfilename[i] = filename[i];
    }
    else
    {
        if (do_select_file(seqfilename, sizeof(seqfilename), "OLI-SETS", "*.txt", "File to save oligos set\0", 0))
            return win_printf_OK("Cannot select output file");
    }

    fp = fopen(seqfilename, "w");

    if (fp == NULL)  win_printf_OK("Cannot open file:\n%s", backslash_to_slash(seqfilename));

    for (i = 0; i < loli_mul_set->set_nb; i++)
    {
        fprintf(fp, "\nOligo set %d\n\n", i);

        for (j = 0; j < loli_mul_set->set_n[i]; j++)
        {
            k = *(loli_mul_set->set[i][j]);
            convert_oligoID_to_seq(oligol, loli_mul_set->ioligo[k], loli_mul_set->o_size);
            bin = loli_mul_set->setID[k];

            for (l = 0; l < loli_mul_set->set_nb; l++)
                ch[l] = (bin & (0x1 << l)) ? '1' : '0';

            ch[loli_mul_set->set_nb] = 0;
            fprintf(fp, "%s\t%s\n", ch, oligol);
        }
    }

    fclose(fp);
    return 0;
}

int test_load_multiplex(void)
{
    int i, j;
    static int dump_set = 1, dump_analysis = 1, thres = 3, do_screen = 1, do_inter = 1;
    int over, bscore, score; // , par_0, par_1, par_2 , bpar_0, bpar_1, bpar_2
    static int sw1 = 0, sw2 = 0, was1, was2, max = 1000;

    if (updating_menu_state != 0)          return D_O_K;

    oli_mul_set = load_multiplex_oligo_set_from_file(NULL);

    i = win_scanf("Do you want to save oligos sets in a file %b\n"
                  "Do you want to dump oligo sets analysis in a file %b\n"
                  "Specify threshod for partially hybridized %8d\n"
                  "Do you want to dump oligo sets analysis to screen %b\n"
                  "Do you want to dump oligo sets inter group analysis to screen %b\n"
                  , &dump_set, &dump_analysis, &thres, &do_screen, &do_inter);

    if (i == WIN_CANCEL) return D_O_K;

    if (dump_set)       dump_multiplex_oligo_set_to_file(oli_mul_set, NULL);

    if (dump_analysis) dump_multiplex_oligo_set_analysis_to_file(oli_mul_set, thres, NULL);

    if (do_screen) dump_multiplex_oligo_set_analysis_to_screen(oli_mul_set, thres);

    if (do_inter) dump_multiplex_oligo_set_inter_analysis_to_screen(oli_mul_set, thres);

    for (i = 0; i != WIN_CANCEL;)
    {
        win_scanf("Select first olgo to swap %d with %d", &sw1, &sw2);
        was1 = oli_mul_set->ioligo[sw1];
        was2 = oli_mul_set->ioligo[sw2];
        oli_mul_set->ioligo[sw1] = was2;
        oli_mul_set->ioligo[sw2] = was1;

        multiplex_oligo_set_analysis(oli_mul_set, 0);

        for (j = 1, over = 0; j <= oli_mul_set->o_size; j++)
            over += oli_mul_set->overlap[j];

        j = win_printf("Overlapping %d, partial hybridization\n"
                       "of size %d -> %d; %d -> %d; %d -> %d; %d -> %d\n"
                       "To obtain detail press OK", over
                       , oli_mul_set->o_size, oli_mul_set->partial_hybridized[oli_mul_set->o_size]
                       , oli_mul_set->o_size - 1, oli_mul_set->partial_hybridized[oli_mul_set->o_size - 1]
                       , oli_mul_set->o_size - 2, oli_mul_set->partial_hybridized[oli_mul_set->o_size - 2]
                       , oli_mul_set->o_size - 3, oli_mul_set->partial_hybridized[oli_mul_set->o_size - 3]);


        if (j != WIN_CANCEL) dump_multiplex_oligo_set_analysis_to_screen(oli_mul_set, thres);

        j = win_printf("keep move ?");

        if (j == WIN_CANCEL)
        {
            oli_mul_set->ioligo[sw1] = was1;
            oli_mul_set->ioligo[sw2] = was2;
        }

        multiplex_oligo_set_analysis(oli_mul_set, 0);

        for (j = 1, over = 0; j <= oli_mul_set->o_size; j++)
            over += oli_mul_set->overlap[j];

        j = win_printf("Overlapping %d, partial hybridization\n"
                       "of size %d -> %d; %d -> %d; %d -> %d; %d -> %d\n"
                       "To obtain detail press OK", over
                       , oli_mul_set->o_size, oli_mul_set->partial_hybridized[oli_mul_set->o_size]
                       , oli_mul_set->o_size - 1, oli_mul_set->partial_hybridized[oli_mul_set->o_size - 1]
                       , oli_mul_set->o_size - 2, oli_mul_set->partial_hybridized[oli_mul_set->o_size - 2]
                       , oli_mul_set->o_size - 3, oli_mul_set->partial_hybridized[oli_mul_set->o_size - 3]);

        if (j != WIN_CANCEL) dump_multiplex_oligo_set_analysis_to_screen(oli_mul_set, thres);

        i = win_printf("continue ?");
    }

    i = win_scanf("Do you want to save oligos sets in a file %b\n"
                  "Do you want to dump oligo sets analysis in a file %b\n"
                  , &dump_set, &dump_analysis);

    if (i == WIN_CANCEL) return D_O_K;

    if (dump_set)       dump_multiplex_oligo_set_to_file(oli_mul_set, NULL);

    if (dump_analysis) dump_multiplex_oligo_set_analysis_to_file(oli_mul_set, thres, NULL);

    /*
       bpar_0 = oli_mul_set->partial_hybridized[oli_mul_set->o_size];
       bpar_1 = oli_mul_set->partial_hybridized[oli_mul_set->o_size-1];
       bpar_2 = oli_mul_set->partial_hybridized[oli_mul_set->o_size-2];
       */
    bscore = (over * 100000000) + 1000000 * oli_mul_set->partial_hybridized[oli_mul_set->o_size]
             + 10000 * oli_mul_set->partial_hybridized[oli_mul_set->o_size - 1]
             + 100 * oli_mul_set->partial_hybridized[oli_mul_set->o_size - 2]
             + oli_mul_set->partial_hybridized[oli_mul_set->o_size - 3];
    i = win_scanf("How many steps do you want %d", &max);

    if (i == WIN_CANCEL) return D_O_K;

    for (i = 0; i < max; i++)
    {
        sw1 = (int)(ran1(&idum) * oli_mul_set->n_oligo);

        if (sw1 < 0 || sw1 >= oli_mul_set->n_oligo) continue;

        sw2 = (int)(ran1(&idum) * oli_mul_set->n_oligo);

        if (sw2 < 0 || sw2 >= oli_mul_set->n_oligo) continue;

        was1 = oli_mul_set->ioligo[sw1];
        was2 = oli_mul_set->ioligo[sw2];
        oli_mul_set->ioligo[sw1] = was2;
        oli_mul_set->ioligo[sw2] = was1;


        multiplex_oligo_set_analysis(oli_mul_set, 0);

        for (j = 1, over = 0; j <= oli_mul_set->o_size; j++)
            over += oli_mul_set->overlap[j];

        score = (over * 100000000) + 1000000 * oli_mul_set->partial_hybridized[oli_mul_set->o_size]
                + 10000 * oli_mul_set->partial_hybridized[oli_mul_set->o_size - 1]
                + 100 * oli_mul_set->partial_hybridized[oli_mul_set->o_size - 2]
                + oli_mul_set->partial_hybridized[oli_mul_set->o_size - 3];

        if (score <= bscore)
        {
            if (score < bscore) win_printf("Overlapping %d, partial hybridization\n"
                                               "of size %d -> %d; %d -> %d; %d -> %d; %d -> %d\n"
                                               "To obtain detail press OK", over
                                               , oli_mul_set->o_size, oli_mul_set->partial_hybridized[oli_mul_set->o_size]
                                               , oli_mul_set->o_size - 1, oli_mul_set->partial_hybridized[oli_mul_set->o_size - 1]
                                               , oli_mul_set->o_size - 2, oli_mul_set->partial_hybridized[oli_mul_set->o_size - 2]
                                               , oli_mul_set->o_size - 3, oli_mul_set->partial_hybridized[oli_mul_set->o_size - 3]);

            bscore = score;
            /*
               bpar_0 = oli_mul_set->partial_hybridized[oli_mul_set->o_size];
               bpar_1 = oli_mul_set->partial_hybridized[oli_mul_set->o_size-1];
               bpar_2 = oli_mul_set->partial_hybridized[oli_mul_set->o_size-2];
               */
        }
        else
        {
            oli_mul_set->ioligo[sw1] = was1;
            oli_mul_set->ioligo[sw2] = was2;
        }
    }

    i = win_scanf("Do you want to save oligos sets in a file %b\n"
                  "Do you want to dump oligo sets analysis in a file %b\n"
                  , &dump_set, &dump_analysis);

    if (i == WIN_CANCEL) return D_O_K;

    if (dump_set)       dump_multiplex_oligo_set_to_file(oli_mul_set, NULL);

    if (dump_analysis) dump_multiplex_oligo_set_analysis_to_file(oli_mul_set, thres, NULL);




    return 0;
}


int findSeq_plot_oligos_with_minimal_Tm(void)
{
    int i, j, k;
    O_p *opn = NULL;
    int  found, is, nmax, gc, l, ci;
    d_s *ds = NULL, *dst = NULL, *dsgc = NULL;
    pltreg *pr = NULL;
    float Tm, Tm_1, dh, dS, dg;
    char ch, loligo[128], complementary[128], label[150];
    static int min = 5, max = 32;
    static float Tmmin = 40, CO = 1.0, Na = 100;


    if (updating_menu_state != 0)
    {
        // we wait for a valid sequence to be loaded
        if (lsqd == NULL) active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }


    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine multiply the y coordinate"
                             "of a data set by a number and place it in a new plot");
    }

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr", &pr) != 1) // we get the plot region
        return win_printf_OK("cannot find data at %s:%d", __FILE__, __LINE__);

    i = win_scanf("make a plot of oligo length having a Tm larger than a threshold\n"
                  "adjust length so that Tm exceeds a specific value %8f \n"
                  "Oligo concentration %8f \\mu M [Na^+] %8f mM\n"
                  "minimum size %5d maximum size %5d\n"
                  , &Tmmin, &CO, &Na, &min, &max);

    if (i == WIN_CANCEL) return OFF;

    nmax = lsqd->totalSize;

    if ((opn = create_and_attach_one_plot(pr, nmax, nmax, 0)) == NULL)
        return win_printf_OK("cannot create plot at %s:%d", __FILE__, __LINE__);

    ds = opn->dat[0];
    set_ds_source(ds, "Oligo length");
    opn->need_to_refresh = 1;
    set_plot_title(opn, "\\stack{{Oligos with Tm greater than %g}{\\pt7 [oligo] %g \\mu M and [Na^+] %g mM}}", Tmmin, CO,
                   Na);
    set_plot_x_title(opn, "Position along sequence");
    set_plot_y_title(opn, "Length and Tm of Oligos");

    if ((dst = create_and_attach_one_ds(opn, nmax, nmax, 0)) == NULL)
        return win_printf_OK("cannot create plot at %s:%d", __FILE__, __LINE__);

    set_ds_source(dst, "Oligo Tm");
    alloc_data_set_y_error(dst);

    if ((dsgc = create_and_attach_one_ds(opn, nmax, nmax, 0)) == NULL)
        return win_printf_OK("cannot create plot at %s:%d", __FILE__, __LINE__);

    set_ds_source(dsgc, "Oligo GC content");

    for (i = 0; i < ds->nx - max; i++)
    {
        for (k = min - 1, found = 0; k < max && found == 0; k++)
        {
            for (j = 0, gc = 0; (j + i) < ds->nx && j < k; j++)
            {
                is = i + j;
                ch = lsqd->seq[is >> lsqd->ln2PageSize][is & lsqd->pageMask];
                loligo[j] = ch;
                gc += ((ch & 0x5F) == 'G' || (ch & 0x5F) == 'C') ? 1 : 0;
            }

            loligo[k] = 0;

            if (k >= min) Tm_1 = Tm;

            if (compute_Tm_unified(loligo, CO, Na, &Tm, &dh, &dS, &dg)) // 1 micoM
                win_printf_OK("error in computing Tm");

            if ((Tm > Tmmin) || (k == max - 1))
            {
                found = 1;
                ds->xd[i] = dst->xd[i] = dsgc->xd[i] = i;
                ds->yd[i] = k;
                dst->yd[i] = Tm;

                if (dst->ye) dst->ye[i] = Tm - Tm_1;

                dsgc->yd[i] = ((float)(100 * gc)) / k;

                for (l = 0; l < k; l++)   // we loop on the sequence
                {
                    ch = loligo[l];

                    if (ch != 'a' && ch != 'A' && ch != 'c' && ch != 'C' &&
                            ch != 'g' && ch != 'G' && ch != 't' && ch != 'T' &&
                            ch != 'r' && ch != 'R' && ch != 'y' && ch != 'Y' &&
                            ch != 's' && ch != 'S' && ch != 'w' && ch != 'W' &&
                            ch != 'n' && ch != 'N')
                        return win_printf_OK("Invalid base letter in sequence: %s\n", loligo);

                    if (ch == 'a' || ch == 'A') ch = 'T';
                    else if (ch == 'c' || ch == 'C') ch = 'G';
                    else if (ch == 'g' || ch == 'G') ch = 'C';
                    else if (ch == 't' || ch == 'T') ch = 'A';

                    complementary[k - l - 1] = ch;
                }

                complementary[k] = 0;

                ci = findOligoInSeq(lsqd, complementary, 0, 1);

                if (ci > 0)
                {
                    snprintf(label, sizeof(label), "\\pt5 %s", loligo);
                    push_plot_label_in_ds(ds, ci, dst->yd[i], label, VERT_LABEL_USR);
                }
            }
        }
    }

    ds->nx = ds->ny = dst->nx = dst->ny = dsgc->nx = dsgc->ny = i;
    /* refisplay the entire plot */
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}






int findSeq_plot_multiplex_oligo_covering_on_seq(void)
{
    int i, j, i_1;
    O_p *opn = NULL;
    int  in, k, f1 = 0, f0 = 0, fn = 0, fi, nmax = 0, jo, ig, l, n_ones, is, igc, il;
    float val;
    unsigned long long bin;
    d_s *ds = NULL;
    pltreg *pr = NULL;
    float Tm, dh, dS, dg;
    float C0 = 1.0, Na = 100;  // C0 = 1.0 micoM, Na+ = 100 mM;
    char ch, complementary[128], oligol[128], lseq[32], label[64], cnbef[32], cnaft[32], cx[32];
    static int plt_per_group = 0, covering = 0, peak = 0, vlabel = 0, adjTm = 0, Nbef = 2, Naft = 2;
    static float Tmmin = 40;


    if (updating_menu_state != 0)
    {
        // we wait for a valid sequence to be loaded
        if (lsqd == NULL || oli_mul_set == NULL) active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }


    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine multiply the y coordinate"
                             "of a data set by a number and place it in a new plot");
    }

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr", &pr) != 1) // we get the plot region
        return win_printf_OK("cannot find data at %s:%d", __FILE__, __LINE__);

    i = win_scanf("make a single plot with each dataset corresponding to a group%R\n"
                  "Or make as many plots as there are groups with one dataset per oligo%r\n"
                  "Or make a plot with one dataset per oligo%r\n"
                  "Display oligo ID%R or histo peak%r GC count%r TM %r or peak of oligo size %r\n"
                  "add matching sequence as vertical label %b\n"
                  "%b adjust Tm to exceed a specific value %8f \n"
                  "In the first case display covering %b\n"
                  "Number of Ns in 5' %4d in 3' %4d\n"
                  "[Oligo] %5f \\mu M; [Na^+] %5f mM\n"
                  , &plt_per_group, &peak, &vlabel, &adjTm, &Tmmin, &covering, &Nbef, &Naft, &C0, &Na);

    if (i == WIN_CANCEL) return OFF;

    nmax = lsqd->totalSize;


    for (ig = 0; ig < oli_mul_set->set_nb; ig++)
    {
        // loop on group
        //fprintf(fp,"\nOligo set %d\n\n",i);
        if (opn == NULL || plt_per_group == 1)
        {
            if ((opn = create_and_attach_one_plot(pr, nmax, nmax, 0)) == NULL)
                return win_printf_OK("cannot create plot at %s:%d", __FILE__, __LINE__);

            ds = opn->dat[0];
            opn->need_to_refresh = 1;
            set_plot_title(opn, "\\stack{{Group %d having %d oligos}}", ig, oli_mul_set->set_n[ig]);
            set_plot_x_title(opn, "Position along sequence");
            set_plot_y_title(opn, "matching");
            convert_oligoID_to_seq(oligol, oli_mul_set->ioligo[0], oli_mul_set->o_size);
            set_ds_source(ds, "Oligo: %s", oligol);

            for (i = 1; plt_per_group == 2 && i < oli_mul_set->n_oligo; i++)
            {
                if ((ds = create_and_attach_one_ds(opn, nmax, nmax, 0)) == NULL)
                    return win_printf_OK("cannot create plot at %s:%d", __FILE__, __LINE__);

                convert_oligoID_to_seq(oligol, oli_mul_set->ioligo[i], oli_mul_set->o_size);
                set_ds_source(ds, "Oligo: %s", oligol);
            }
        }

        for (jo = 0; jo < oli_mul_set->set_n[ig]; jo++)
        {
            // loop on oligos in a group
            k = *(oli_mul_set->set[ig][jo]);

            if (plt_per_group == 2 && k < opn->n_dat) ds = opn->dat[k];

            convert_oligoID_to_seq(oligol, oli_mul_set->ioligo[k], oli_mul_set->o_size);
            bin = oli_mul_set->setID[k];

            for (l = 0, n_ones = 0; l < oli_mul_set->set_nb; l++)
                n_ones += (bin & (((unsigned long long)0x1) << l)) ? 1 : 0;

            for (l = 0; l < oli_mul_set->o_size; l++)   // we loop on the sequence
            {
                ch = oligol[l];

                if (ch != 'a' && ch != 'A' && ch != 'c' && ch != 'C' &&
                        ch != 'g' && ch != 'G' && ch != 't' && ch != 'T' &&
                        ch != 'r' && ch != 'R' && ch != 'y' && ch != 'Y' &&
                        ch != 's' && ch != 'S' && ch != 'w' && ch != 'W' &&
                        ch != 'm' && ch != 'M' && ch != 'k' && ch != 'K' &&
                        ch != 'n' && ch != 'N')
                    return win_printf_OK("Invalid base letter in sequence: %s\n", oligol);

                if (ch == 'a' || ch == 'A') ch = 'T';
                else if (ch == 'c' || ch == 'C') ch = 'G';
                else if (ch == 'g' || ch == 'G') ch = 'C';
                else if (ch == 't' || ch == 'T') ch = 'A';
                else if (ch == 'm' || ch == 'M') ch = 'K';
                else if (ch == 'k' || ch == 'K') ch = 'M';

                complementary[oli_mul_set->o_size - l - 1] = ch;
            }

            complementary[oli_mul_set->o_size] = 0;

            if (ds == NULL)
            {
                if ((ds = create_and_attach_one_ds(opn, nmax, nmax, 0)) == NULL)
                    return win_printf_OK("cannot create plot at %s:%d", __FILE__, __LINE__);
            }

            if (plt_per_group != 2) set_ds_source(ds, "Oligo: %s, group %d", oligol, ig);

            for (i_1 = -1, j = 0, i = 1, fi = 0; i != 0 ; j++)
            {
                for (is = 0; is < Nbef; is++) cnbef[is] = 0;

                for (is = 0; is < Naft; is++) cnaft[is] = 0;

                i = findOligoInSeq(lsqd, complementary, i_1 + 1, 1);

                if (i > 0)
                {
                    for (igc = 0, k = 0, is = (i >= Naft) ? i - Naft : i; is < i + oli_mul_set->o_size + Nbef
                            && is < lsqd->totalSize; is++, k++)
                    {
                        ch = lsqd->seq[is >> lsqd->ln2PageSize][is & lsqd->pageMask];
                        lseq[k] = ch;

                        if (is < i)
                        {
                            cnaft[is - i + Naft] = ch;
                        }
                        else if (is >= i + oli_mul_set->o_size)
                        {
                            cnbef[is - i - oli_mul_set->o_size] = ch;
                        }
                        else
                        {
                            cx[is - i] = ch;
                        }

                        if (ch == 'g' || ch == 'G' || ch == 'c' || ch == 'C') igc++;
                    }

                    lseq[k] = 0;
                    cnaft[Naft] = 0;
                    cnbef[Nbef] = 0;
                    cx[oli_mul_set->o_size] = 0;
                    /*
                       for (k = 0, is = (i > 1) ? i - 3 : i; is < i + oli_mul_set->o_size + 2 && is < lsqd->totalSize; is++, k++)
                       {
                       ch = lsqd->seq[is >> lsqd->ln2PageSize][is & lsqd->pageMask];
                       lseq[k] = ch;
                       if (ch == 'g' || ch == 'G' || ch == 'c' || ch == 'C') igc++;
                       }
                       lseq[k] = 0;
                       */
                    fi++;
                    in = findNNNBackwardInSeq(lsqd, i, i_1);
                    in = (i_1 < in) ? in : i_1;

                    if (peak == 3)
                    {
                        //if (compute_Tm(lseq, C0, &Tm, &dh, &dS, &dg)) // 1 micoM
                        if (compute_Tm_unified(lseq, C0, Na, &Tm, &dh, &dS, &dg)) // 1 micoM
                            win_printf_OK("error in computing Tm");

                        /*
                           if (ig == 0 && jo < 5)
                           win_printf("Tm of %s (G %d j %d)\nat %f \\mu M equals %6.2f degrees\n"
                           "\\Delta G = %g\n"
                           "\\Delta H = %g\n"
                           "\\Delta S = %g\n"
                           ,lseq,ig,jo,C0,Tm,dg,dh,dS);
                           */
                    }

                    for (il = 0, (k = (peak == 1) ? i + oli_mul_set->o_size - 1 : i); k < ds->nx && k < i + oli_mul_set->o_size; k++, il++)
                    {
                        val = (peak == 1) ? 1 : (float)bin;
                        val = (peak == 2) ? 0.1 + igc : val;

                        if (peak == 3)    val = Tm;

                        if (peak == 4)    val = 1;

                        ds->yd[k] = (covering) ? (ds->yd[k] + ((float)1) / n_ones) : val;

                        if (vlabel && il == 0)
                        {
                            snprintf(label, sizeof(label), "\\pt5 %s{\\color{lightblue}%s}%s", cnaft, cx, cnbef);
                            push_plot_label_in_ds(ds, k, ds->yd[k] + 1, label, VERT_LABEL_USR);
                        }
                    }

                    i_1 = i;
                }
            }

            for (i_1 = -1, j = 0, i = 1, fi = 0; i != 0 ; j++)
            {
                for (is = 0; is < Nbef; is++) cnbef[is] = 0;

                for (is = 0; is < Naft; is++) cnaft[is] = 0;

                i = findOligoInSeq(lsqd, oligol, i_1 + 1, 1);

                if (i > 0)
                {
                    for (igc = 0, k = 0, is = (i >=  Nbef) ? i - Nbef : i; is < i + oli_mul_set->o_size + Naft
                            && is < lsqd->totalSize; is++, k++)
                    {
                        ch = lsqd->seq[is >> lsqd->ln2PageSize][is & lsqd->pageMask];
                        lseq[k] = ch;

                        if (is < i)
                        {
                            cnbef[is - i + Nbef] = ch;
                        }
                        else if (is >= i + oli_mul_set->o_size)
                        {
                            cnaft[is - i - oli_mul_set->o_size] = ch;
                        }
                        else
                        {
                            cx[is - i] = ch;
                        }

                        if (ch == 'g' || ch == 'G' || ch == 'c' || ch == 'C') igc++;
                    }

                    lseq[k] = 0;
                    cnaft[Naft] = 0;
                    cnbef[Nbef] = 0;
                    cx[oli_mul_set->o_size] = 0;
                    fi++;
                    in = findNNNBackwardInSeq(lsqd, i, i_1);
                    in = (i_1 < in) ? in : i_1;

                    if (peak == 3)
                    {
                        //if (compute_Tm(lseq, C0, &Tm, &dh, &dS, &dg))
                        if (compute_Tm_unified(lseq, C0, Na, &Tm, &dh, &dS, &dg)) // 1 micoM
                            win_printf_OK("error in computing Tm");
                    }

                    for (il = 0, (k = (peak == 1) ? i + oli_mul_set->o_size - 1 : i); k < ds->nx && k < i + oli_mul_set->o_size; k++, il++)
                    {
                        val = (peak == 1) ? 1 : (float)bin;
                        val = (peak == 2) ? 0.1 + igc : val;

                        if (peak == 3)  val = Tm;

                        if (peak == 4)    val = 1;

                        ds->yd[k] = (covering) ? (ds->yd[k] + ((float)1) / n_ones) : val;

                        if (vlabel && il == 0)
                        {
                            snprintf(label, sizeof(label), "\\pt5 %s{\\color{lightgreen}%s}%s", cnbef, cx, cnaft);
                            //snprintf(label,sizeof(label),"\\pt5 %s",lseq);
                            push_plot_label_in_ds(ds, k, ds->yd[k] + 1, label, VERT_LABEL_USR);
                        }
                    }

                    i_1 = i;
                }
            }

            if (fi == 0) f0++;
            else if (fi == 1) f1++;
            else fn++;

            for (k = 0; k < ds->nx; k++)        ds->xd[k] = k;

            if (plt_per_group == 1) ds = NULL;
        } // end of loop on oligos in a group

        if (covering == 0) ds = NULL;
    } // end of loop on groups

    /* refisplay the entire plot */
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}





int count_non_overlapping_oligos(void)
{
    int i, j, k;
    int  noli, setp, *set = NULL, size = 3, ok;
    char seqfilename[2048], message[64];
    FILE *fp;


    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT])
        return win_printf_OK("This routine display  a blast plot between two sequences");

    i = win_scanf("Define oligo size %d < 16", &size);

    if (i == WIN_CANCEL) return 0;


    if (size > 10) return -win_printf_OK("There is more than 1 million of oligos!");

    for (noli = 4, i = 1; i < size; i++, noli *= 4);

    set = (int *)calloc(noli, sizeof(int));

    if (set == NULL) return win_printf_OK("cannot allocate counting space!");

    for (i = 0, setp = 1; i < noli; i++)
    {
        if (set[i]) continue;

        set[i] = setp;  // this is the identifier of the non-overlapping subset

        for (j = 0; j < noli; j++)
        {
            if (i == j) continue;

            if (set[j] > 0 && set[j] < setp) continue;

            if (does_oligos_overlap(i, j, size)) continue;

            for (k = 0, ok = 1; k < j; k++)
            {
                if (set[k] != setp) continue;

                if (does_oligos_overlap(k, j, size)) ok = 0;
            }

            if (ok) set[j] = setp;
        }

        setp++;
    }

    seqfilename[0] = 0;

    if (do_select_file(seqfilename, sizeof(seqfilename), "TXT-FILE", "*.txt", "File to save sequence\0", 0))
        return win_printf_OK("Cannot select output file");

    fp = fopen(seqfilename, "w");

    if (fp == NULL)  win_printf_OK("Cannot open file:\n%s", backslash_to_slash(seqfilename));

    for (k = 1; k < setp; k++)
    {
        fprintf(fp, "pool %d\n", k);

        for (j = 0, i = 0; j < noli; j++)
        {
            if (set[j] == k)
            {
                convert_oligo_seq(message, j, size);
                fprintf(fp, "%d\t%s\n", i++, message);
            }
        }
    }

    fclose(fp);
    return 0;
}


int count_non_overlapping_non_partial_hybridizing_oligos(void)
{
    int i, j, k;
    int  noli, setp, maxi, max, *set = NULL, ok, *maxs = NULL, gri, *used = NULL, maxm;
    static int minoverlap = 1, min_hyb = 1, size = 5, grm = 10;
    char seqfilename[2048], message[64];
    FILE *fp;


    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT])
        return win_printf_OK("This routine display  a blast plot between two sequences");

    i = win_scanf("Define oligo size %8d < 16\n"
                  "Minimum overlapping %6d\n"
                  "Minimum complementary hybridization %6d\n"
                  , &size, &minoverlap, &min_hyb);

    if (i == WIN_CANCEL) return 0;


    if (size > 10) return -win_printf_OK("There is more than 1 million of oligos!");

    for (noli = 4, i = 1; i < size; i++, noli *= 4);

    set = (int *)calloc(noli, sizeof(int));

    if (set == NULL) return win_printf_OK("cannot allocate counting space!");

    maxs = (int *)calloc(noli, sizeof(int));

    if (maxs == NULL) return win_printf_OK("cannot allocate counting space!");

    used = (int *)calloc(noli, sizeof(int));

    if (used == NULL) return win_printf_OK("cannot allocate counting space!");


    for (i = 0, max = maxm = 0; i < noli; i++)
    {
        for (k = 0; k < noli; k++) set[k] = 0;

        for (j = 0, setp = 0; j < noli; j++)
        {
            if ((i != j) && (abs(does_oligos_overlap(i, j, size)) > minoverlap)) continue;

            if (abs(does_oligos_partially_hybridized(i, j, size)) > min_hyb) continue;

            for (k = 0, ok = 1; k < j; k++)
            {
                if (set[k] == 0) continue;

                if (abs(does_oligos_overlap(k, j, size)) > minoverlap) ok = 0;

                if (abs(does_oligos_partially_hybridized(k, j, size)) > min_hyb) ok = 0;
            }

            if (abs(does_oligos_partially_hybridized(j, j, size)) > min_hyb) ok = 0;

            if (ok)
            {
                set[j] = 1;
                setp++;
            }
        }

        maxs[i] = setp;
        maxm += setp;

        //if (i%32 == 0) win_printf("I %d setp %d",i,setp);
        if (setp > max)
        {
            maxi = i;
            max = setp;
        }
    }

    i = win_printf("Max group size %d of oligos %d bases spanning %d mean setp %g\n"
                   "with %d minimum overlapping\n"
                   "and %d partial hybridization\n"
                   "Press OK to save"
                   , max, size, noli, (float)maxm / noli, minoverlap, min_hyb);

    if (i == WIN_CANCEL)
    {
        free(set);
        free(maxs);
        free(used);
        return D_O_K;
    }

    i = win_scanf("Define the number of group to print %5d", &grm);

    if (i == WIN_CANCEL)
    {
        free(set);
        free(maxs);
        free(used);
        return D_O_K;
    }

    seqfilename[0] = 0;

    if (do_select_file(seqfilename, sizeof(seqfilename), "TXT-FILE", "*.txt", "File to save sequence\0", 0))
        return win_printf_OK("Cannot select output file");

    fp = fopen(seqfilename, "w");

    if (fp == NULL)
    {
        free(set);
        free(maxs);
        free(used);
        return win_printf_OK("Cannot open file:\n%s", backslash_to_slash(seqfilename));
    }


    for (gri = 0; gri < grm; gri++)
    {
        for (i = 0, max = 0, maxi = 0; i < noli; i++)
        {
            if (maxs[i] >= max)
            {
                maxi = i;
                max = maxs[i];
            }
        }

        maxs[maxi] = 0;
        fprintf(fp, "%d group from  %d size %d of oligos with %d bases spanning %d "
                "with %d minimum overlapping "
                "and %d partial hybridization \n"
                , gri, maxi, max, size, noli, minoverlap, min_hyb);

        for (k = 0; k < noli; k++) set[k] = 0;

        for (i = maxi, j = 0, setp = 0; j < noli; j++)
        {
            if ((i != j) && (abs(does_oligos_overlap(i, j, size)) > minoverlap)) continue;

            if (abs(does_oligos_partially_hybridized(i, j, size)) > min_hyb) continue;

            for (k = 0, ok = 1; k < j; k++)
            {
                if (set[k] == 0) continue;

                if (abs(does_oligos_overlap(k, j, size)) > minoverlap) ok = 0;

                if (abs(does_oligos_partially_hybridized(k, j, size)) > min_hyb) ok = 0;
            }

            if (abs(does_oligos_partially_hybridized(j, j, size)) > min_hyb) ok = 0;

            if (ok)
            {
                set[j] = 1;
                maxs[j] = 0;
                convert_oligo_seq(message, j, size);
                fprintf(fp, "%d\t%s\t%d\n", setp++, message, j);
                used[j]++;
            }
        }
    }

    for (k = 0, max = 0; k < noli; k++) max += (used[k]) ? 1 : 0;

    fprintf(fp, "\n%d oligos used\n", max);

    for (k = 0, max = 0; k < noli; k++)
    {
        if (used[k])
        {
            convert_oligo_seq(message, k, size);
            fprintf(fp, "%d\t%s\t%d\n", k, message, used[k]);
        }
    }

    free(set);
    free(maxs);
    free(used);
    fclose(fp);
    return 0;
}


int count_non_overlapping_non_partial_hybridizing_oligos_with_woobbles(void)
{
    int i, j, k;
    int  noli, nbits, setp, maxi, max, *set = NULL, ok, *maxs = NULL, gri, *used = NULL, maxm, gc;
    int over, hyb, sover[4], shyb[4], sovhy[4], *lngc, **grel = NULL, maxmax = 0, mgrel = 0;
    static int minoverlap = 1, min_hyb = 1, size = 5, grm = 10, first = 1, mingc = 5;
    char seqfilename[2048], pattern[32], bits[32], pos[32], **loligo = NULL;
    FILE *fp;


    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT])
        return win_printf_OK("This routine display  a blast plot between two sequences");

    if (first) pattern[0] = 0;

    i = win_scanf("Define oligo pattern (W woobble S or W), (X a single base)\n"
                  "(N a degenerate base) (M woobble M or K)\n %16s size < 16\n"
                  "Minimum overlapping %6d\n"
                  "Minimum complementary hybridization %6d\n"
                  "Minimum number of GC bases %6d\n"
                  , pattern, &minoverlap, &min_hyb, &mingc);

    if (i == WIN_CANCEL) return 0;

    size = strlen(pattern);

    for (i = 0, j = 0; i < size; i++)
    {
        if ((pattern[i] & 0x5F) == 'X')
        {
            bits[i] = 2;
            pos[i] = j;
            j += 2;
        }
        else if ((pattern[i] & 0x5F) == 'W')
        {
            bits[i] = 1;
            pos[i] = j;
            j += 1;
        }
        else if ((pattern[i] & 0x5F) == 'M')
        {
            bits[i] = 1;
            pos[i] = j;
            j += 1;
        }
        else if ((pattern[i] & 0x5F) == 'N')
        {
            bits[i] = 0;
            pos[i] = 0;
        }
        else return win_printf_OK("Unalloaded char %c in pattern %s", pattern[i], pattern);
    }

    nbits = j;

    if (nbits > 20) return -win_printf_OK("There is more than 1 million of oligos!");

    for (noli = 1, i = 0; i < nbits; i++, noli *= 2);

    i = win_printf("# bits %d; noli %d pattern %s", nbits, noli, pattern);

    if (i == WIN_CANCEL) return 0;

    set = (int *)calloc(noli, sizeof(int));

    if (set == NULL) return win_printf_OK("cannot allocate counting space!");

    maxs = (int *)calloc(noli, sizeof(int));

    if (maxs == NULL) return win_printf_OK("cannot allocate counting space!");

    used = (int *)calloc(noli, sizeof(int));

    if (used == NULL) return win_printf_OK("cannot allocate counting space!");

    loligo = (char **)calloc(noli, sizeof(char *));

    if (loligo == NULL) return win_printf_OK("cannot allocate list of oligos!");

    lngc = (int *)calloc(noli, sizeof(int));

    if (lngc == NULL) return win_printf_OK("cannot allocate list of oligos!");

    for (i = 0; i < noli; i++)
    {
        loligo[i] = (char *)calloc(size + 1, sizeof(char));

        if (loligo[i] == NULL) return win_printf_OK("cannot allocate oligos in list!");
    }

    for (i = 0; i < 4; i++) sover[i] = shyb[i] = sovhy[i] = 0;

    // we generate the oligo list
    for (i = 0; i < noli; i++)
    {
        for (j = 0; j < size; j++)
        {
            if (bits[j] == 0)       loligo[i][j] = 'N';

            if (bits[j] == 1)
            {
                if ((pattern[i] & 0x5F) == 'M')
                    loligo[i][j] = ((i >> pos[j]) & 0x1) ? 'M' : 'K';
                else
                {
                    loligo[i][j] = ((i >> pos[j]) & 0x1) ? 'S' : 'W';
                    lngc[i] += (i >> pos[j]) & 0x1;
                }
            }

            if (bits[j] == 2)
            {
                k = ((i >> pos[j]) & 0x3);

                if (k == 0)  loligo[i][j] = 'A';
                else if (k == 1)
                {
                    loligo[i][j] = 'C';
                    lngc[i]++;
                }
                else if (k == 2)
                {
                    loligo[i][j] = 'G';
                    lngc[i]++;
                }
                else if (k == 3)  loligo[i][j] = 'T';
            }
        }

        loligo[i][j] = 0;
        over = does_oligo_self_overlap(loligo[i]);
        over = abs(over);
        hyb = does_oligos_with_woobble_partially_hybridized(loligo[i], loligo[i]);
        hyb = abs(hyb);

        if (over < 4) sover[over]++;

        if (hyb < 4) shyb[hyb]++;

        over = (over > hyb) ? over : hyb;

        if (over < 4) sovhy[over]++;
    }

    win_printf("For pattern %s\n"
               "Self-overlapping %d oligos overlapp by 0, %d by 1, %d by 2, %d by 3\n"
               "Self-hybridization %d oligos hybrid by 0, %d by 1, %d by 2, %d by 3\n"
               "Both conditions    %d oligos match  by 0, %d by 1, %d by 2, %d by 3\n"
               , pattern, sover[0], sover[1], sover[2], sover[3], shyb[0], shyb[1], shyb[2], shyb[3]
               , sovhy[0], sovhy[1], sovhy[2], sovhy[3]);

    if (i == WIN_CANCEL) return 0;

    for (i = 0, max = maxm = 0; i < noli; i++)
    {
        for (k = 0; k < noli; k++) set[k] = 0;

        over = does_oligo_self_overlap(loligo[i]);
        hyb = does_oligos_with_woobble_partially_hybridized(loligo[i], loligo[i]);
        setp = 0;

        if ((abs(over) <= minoverlap) && (abs(hyb) <= min_hyb))
        {
            for (j = 0, setp = 0; j < noli; j++)
            {
                if (lngc[j] < mingc) continue;

                over = does_oligo_self_overlap(loligo[j]);

                if (abs(over) > minoverlap) continue;

                hyb = does_oligos_with_woobble_partially_hybridized(loligo[j], loligo[j]);

                if (abs(hyb) > min_hyb) continue;

                over = (i == j) ? 0 : does_oligos_seq_overlap(loligo[i], loligo[j]);

                if (abs(over) > minoverlap) continue;

                hyb = does_oligos_with_woobble_partially_hybridized(loligo[i], loligo[j]);

                if (abs(hyb) > min_hyb) continue;

                for (k = 0, ok = 1; k < j; k++)
                {
                    if (set[k] == 0) continue;

                    over = does_oligos_seq_overlap(loligo[k], loligo[j]);

                    if (abs(over) > minoverlap) ok = 0;

                    hyb = does_oligos_with_woobble_partially_hybridized(loligo[k], loligo[j]);

                    if (abs(hyb) > min_hyb)  ok = 0;
                }

                hyb = does_oligos_with_woobble_partially_hybridized(loligo[i], loligo[j]);

                if (abs(hyb) > min_hyb)  ok = 0;

                if (ok)
                {
                    set[j] = 1;
                    setp++;
                }
            }

            maxs[i] = setp;
            maxm += setp;
        }

        my_set_window_title("treating %d -> %s -> %d", i, loligo[i], setp);

        //if (i%32 == 0) win_printf("I %d setp %d",i,setp);
        if (setp > max)
        {
            maxi = i;
            max = setp;
        }
    }

    i = win_printf("Max group size %d of oligos %d bases spanning %d mean setp %g\n"
                   "with %d minimum overlapping\n"
                   "and %d partial hybridization\n"
                   "Press OK to save"
                   , max, size, noli, (float)maxm / noli, minoverlap, min_hyb);
    maxmax = max;

    if (i == WIN_CANCEL)
    {
        free(set);
        free(maxs);
        free(used);

        for (i = 0; i < noli; i++)
            if (loligo[i] != NULL) free(loligo[i]);

        if (loligo == NULL) free(loligo);

        return D_O_K;
    }

    i = win_scanf("Define the minimum size of group to be print %5d", &grm);

    if (i == WIN_CANCEL)
    {
        free(set);
        free(maxs);
        free(used);

        for (i = 0; i < noli; i++)
            if (loligo[i] != NULL) free(loligo[i]);

        if (loligo == NULL) free(loligo);

        return D_O_K;
    }

    seqfilename[0] = 0;

    if (do_select_file(seqfilename, sizeof(seqfilename), "TXT-FILE", "*.txt", "File to save sequence\0", 0))
        return win_printf_OK("Cannot select output file");

    fp = fopen(seqfilename, "w");

    if (fp == NULL)
    {
        free(set);
        free(maxs);
        free(used);

        for (i = 0; i < noli; i++)
            if (loligo[i] != NULL) free(loligo[i]);

        if (loligo == NULL) free(loligo);

        return win_printf_OK("Cannot open file:\n%s", backslash_to_slash(seqfilename));
    }


    grel = (int **)calloc(256, sizeof(int *));

    if (grel == NULL) return win_printf_OK("cannot allocate list of oligos!");

    mgrel = 256;

    for (gri = 0; max > grm; gri++)
    {
        for (i = 0, max = 0, maxi = 0; i < noli; i++)
        {
            if (maxs[i] >= max)
            {
                maxi = i;
                max = maxs[i];
            }
        }

        maxs[maxi] = 0;

        if (max < grm) break;

        if (gri >= mgrel)
        {
            mgrel += 256;
            grel = (int **)realloc(grel, mgrel * sizeof(int *));

            if (grel == NULL) return win_printf_OK("cannot allocate list of oligos!");
        }

        grel[gri] = (int *)calloc(maxmax, sizeof(int));

        if (grel[gri] == NULL) return win_printf_OK("cannot allocate list of oligos!");

        fprintf(fp, "%d group from  %d size %d of oligos with %d bases spanning %d "
                "with %d minimum overlapping "
                "and %d partial hybridization \n"
                , gri, maxi, max, size, noli, minoverlap, min_hyb);


        for (k = 0; k < noli; k++) set[k] = 0;

        for (i = maxi, j = 0, setp = 0; j < noli; j++)
        {
            /*
               over = (j == i) ? does_oligo_self_overlap(loligo[i])
               : does_oligos_seq_overlap(loligo[i], loligo[j]);
               if (abs(over) > minoverlap) continue;
               hyb = does_oligos_with_woobble_partially_hybridized(loligo[i], loligo[j]);
               if (abs(hyb) > min_hyb) continue;
               */
            if (lngc[j] < mingc) continue;

            over = does_oligo_self_overlap(loligo[j]);

            if (abs(over) > minoverlap) continue;

            hyb = does_oligos_with_woobble_partially_hybridized(loligo[j], loligo[j]);

            if (abs(hyb) > min_hyb) continue;

            over = (i == j) ? 0 : does_oligos_seq_overlap(loligo[i], loligo[j]);

            if (abs(over) > minoverlap) continue;

            hyb = does_oligos_with_woobble_partially_hybridized(loligo[i], loligo[j]);

            if (abs(hyb) > min_hyb) continue;

            for (k = 0, ok = 1; k < j; k++)
            {
                if (set[k] == 0) continue;

                over = does_oligos_seq_overlap(loligo[k], loligo[j]);

                if (abs(over) > minoverlap) ok = 0;

                hyb = does_oligos_with_woobble_partially_hybridized(loligo[k], loligo[j]);

                if (abs(hyb) > min_hyb)  ok = 0;
            }

            hyb = does_oligos_with_woobble_partially_hybridized(loligo[i], loligo[j]);

            if (abs(hyb) > min_hyb)  ok = 0;

            if (ok)
            {
                set[j] = 1;
                maxs[j] = 0;

                for (k = 0, gc = 0; k < size; k++)
                    gc += ((loligo[j][k] == 'C') || (loligo[j][k] == 'G') || (loligo[j][k] == 'S')) ? 1 : 0;

                grel[gri][setp] = j;
                fprintf(fp, "%d\t%s\t%d\t%d\n", setp++, loligo[j], j, gc);
                used[j]++;
            }
        }
    }

    for (k = 0, max = 0; k < noli; k++) max += (used[k]) ? 1 : 0;

    fprintf(fp, "\n%d oligos used\n", max);

    for (k = 0, max = 0; k < noli; k++)
    {
        if (used[k])
        {
            fprintf(fp, "%d\t%s\t%d\n", k, loligo[k], used[k]);
        }
    }



    fprintf(fp, "\noligo#\tsequence\tused");

    for (k = 0; k < gri; k++) fprintf(fp, "\tG%d", k);

    fprintf(fp, "\n");

    for (k = 0, max = 0; k < noli; k++)
    {
        if (used[k])
        {
            fprintf(fp, "%d\t%s\t%d", k, loligo[k], used[k]);

            for (j = 0; j < gri; j++)
            {
                for (i = 0; i < maxmax; i++)
                    if (grel[j][i] == k) break;

                fprintf(fp, "\t%d", (grel[j][i] == k) ? 1 : 0);
            }

            fprintf(fp, "\n");
        }
    }

    free(set);
    free(maxs);
    free(used);

    for (i = 0; i < noli; i++)
        if (loligo[i] != NULL) free(loligo[i]);

    if (loligo == NULL) free(loligo);

    fclose(fp);
    return 0;
}



int count_binary_number(void)
{
    int i, j, k;
    int  nnum, find = 0;
    char seqfilename[2048], ch[64];
    FILE *fp;
    static int size = 8, min = 1, max = 3;


    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT])
        return win_printf_OK("This routine display  a blast plot between two sequences");

    i = win_scanf("Define the maximum number of bits %8d\n"
                  "minimum number of num 1 %8d\n max number of 1 %8d\n", &size, &min, &max);

    if (i == WIN_CANCEL) return 0;


    if (size > 10) return -win_printf_OK("There is more than 1000 codes!");

    for (nnum = 2, i = 1; i < size; i++, nnum *= 2);

    seqfilename[0] = 0;

    if (do_select_file(seqfilename, sizeof(seqfilename), "TXT-BIN", "*.txt", "File to save binary numbers\0", 0))
        return win_printf_OK("Cannot select output file");

    fp = fopen(seqfilename, "w");

    if (fp == NULL)  win_printf_OK("Cannot open file:\n%s", backslash_to_slash(seqfilename));

    for (k = 1, find = 0; k < nnum; k++)
    {
        for (i = 0, j = 0; i < size; i++)
        {
            j += (k & (0x1 << i)) ? 1 : 0;
            ch[size - i - 1] = (k & (0x1 << i)) ? '1' : '0';
        }

        ch[size] = 0;

        if (j > min && j < max)
        {
            fprintf(fp, "%d\t%s %d\n", ++find, ch, k);
        }
    }

    fclose(fp);
    return 0;
}


int grab_oligos_and_pos_from_file(char ***loligo, float **pos)
{
    int  size;
    char seqfilename[2048], oligol[64];
    int n_oligo = 0, m_oligo = 0, read, ic;
    float lpos = 0;
    FILE *fp;

    if (loligo == NULL || pos == NULL) return -1;

    seqfilename[0] = 0;

    if (do_select_file(seqfilename, sizeof(seqfilename), "TXT-FILE", "*.txt", "File to load oligo and position\0", 1))
        return win_printf_OK("Cannot select input file");

    fp = fopen(seqfilename, "r");

    if (fp == NULL)
    {
        win_printf("Cannot open file:\n%s", backslash_to_slash(seqfilename));
        return -1;
    }

    (*loligo) = (char **)calloc(m_oligo = 256, sizeof(char *));

    if ((*loligo) == NULL)
    {
        win_printf("Cannot allocate loligo");
        return -1;
    }

    (*pos) = (float *)calloc(m_oligo = 256, sizeof(float));

    if ((*pos) == NULL)
    {
        win_printf("Cannot allocate loligo");
        return -1;
    }

    for (n_oligo = 0, read = 2, size = -1; read == 2;)
    {
        read = fscanf(fp, "%s\t%f", oligol, &lpos);

        //win_printf("Read %d, %s %g",read,oligo,lpos);
        if (read == 2)
        {
            if (n_oligo >= m_oligo)
            {
                m_oligo += 256;
                (*loligo) = (char **)realloc(loligo, m_oligo * sizeof(char *));

                if ((*loligo) == NULL)
                {
                    win_printf("Cannot allocate loligo");
                    return -1;
                }

                (*pos) = (float *)realloc(pos, m_oligo * sizeof(float));

                if ((*pos) == NULL)
                {
                    win_printf("Cannot allocate loligo");
                    return -1;
                }
            }

            ic = strlen(oligol);

            if (ic > 0)
            {
                if (size < 0) size = ic;

                //if (size != ic) { win_printf_OK("Your oligos differ in size"); return -1;}
                (*loligo)[n_oligo] = strdup(oligol);
                (*pos)[n_oligo] = lpos;
                n_oligo++;
            }
        }

        //else win_printf("Read pb %d, %s %g",read,oligo,lpos);
    }

    fclose(fp);
    return n_oligo;
}


int test_oligo_pos(void)
{
    int i, j;
    int  over = 0, n_oligo = 0, cli[4] = {0};//, maxj;
    char **loligo = NULL;
    float *pos = NULL;
    static float conv_bp_nm = 1.0, err = 2.5;
    pltreg *pr;
    O_p *opn = NULL;
    d_s *ds = NULL;
    char label[512] = {0};
    char *cl[4] = {"lightmagenta","lightgreen","lightred","lightblue"};


    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT])
        return win_printf_OK("This routine display  a blast plot between two sequences");

    if (ac_grep(cur_ac_reg, "%pr", &pr) != 1) // we get the plot region
        return win_printf_OK("cannot find data at %s:%d", __FILE__, __LINE__);

    n_oligo = grab_oligos_and_pos_from_file(&loligo, &pos);

    if (n_oligo <= 0) return 0;

    i = win_scanf("Define the bp to nm conversion %8f\nError %8f\n", &conv_bp_nm, &err);

    if (i == WIN_CANCEL) return 0;

    if ((opn = create_and_attach_one_plot(pr, 2*n_oligo, 2*n_oligo, 0)) == NULL)
        return win_printf_OK("cannot create plot at %s:%d", __FILE__, __LINE__);

    ds = opn->dat[0];
    opn->need_to_refresh = 1;
    set_plot_x_title(opn, "Position");
    set_plot_y_title(opn, "Distance");
    set_ds_source(ds, "Oligo: %s", loligo[0]);

    /*
    for (i = 1; i < n_oligo; i++)
    {
        if ((ds = create_and_attach_one_ds(opn, n_oligo, n_oligo, 0)) == NULL)
            return win_printf_OK("cannot create plot at %s:%d", __FILE__, __LINE__);

        set_ds_source(ds, "Oligo: %s", loligo[i]);
    }
    */
    float prevx = 0, prevy = 0;
    for (i = 0; i < n_oligo; i++)
    {
      //ds = opn->dat[i];
	/*
        for (j = 0, max = 0; j < n_oligo; j++)
        {
            over = does_oligos_seq_overlap(loligo[i], loligo[j]);

            if (over == 0) ds->yd[j] = 0;
            else if (abs(over) == (int)strlen(loligo[i])) ds->yd[j] = 0;
            else if (abs(over) == (int)strlen(loligo[j])) ds->yd[j] = 0;
            else if (i == j) ds->yd[j] = 0;
            else
            {
                tmp = pos[j] - pos[i] - conv_bp_nm * (strlen(loligo[i]) - over);
                tmp = -((tmp * tmp) / (err * err));
                ds->yd[j] = over * over * exp(tmp);

                if (ds->yd[j] > max)
                {
                    //maxj = j;
                    max = ds->yd[j];
                }
            }

            ds->xd[j] = j;

        }
	*/

        /*
           over = does_oligos_seq_overlap(loligo[i], loligo[maxj]);
           tmp = pos[maxj] - pos[i] - conv_bp_nm*(strlen(loligo[i]) - over);
           win_printf("I %d->%s at %g J %d->%s at %g \n\\delta z = %g over %d size_i %d size_j %d tmp %g",
           i,loligo[i],pos[i],maxj,loligo[maxj],pos[maxj],pos[maxj] - pos[i],over,strlen(loligo[i]), strlen(loligo[maxj]),tmp);
           */
        for (j = 0; j < 3 && j < (int)strlen(loligo[i]); j++)
	  {
	    if (loligo[i][j] == 'A') cli[j] = 0;
	    else if (loligo[i][j] == 'C') cli[j] = 1;
	    else if (loligo[i][j] == 'G') cli[j] = 2;
	    else if (loligo[i][j] == 'T') cli[j] = 3;
	  }
	char oligostr[32];
	int over_1 = 0;
	snprintf(oligostr,sizeof(oligostr),"%s",loligo[i]);
	if (i+1 < n_oligo)
	  over = does_oligos_seq_overlap(loligo[i], loligo[i+1]);
	over = abs(over);
	if (i > 0)
	  over_1 = does_oligos_seq_overlap(loligo[i-1], loligo[i]);
	over_1 = abs(over_1);
	if (over < 2 && over_1 < 2)
	  {
	    oligostr[0] = tolower(oligostr[0]);
	    oligostr[1] = tolower(oligostr[1]);
	    oligostr[2] = tolower(oligostr[2]);
	  }
	snprintf(label, sizeof(label),
		 "\\center{\\pt8\\stack{{\\color{%s}%c{      }}{\\color{%s}{   }%c{   }}{\\color{%s}{      }%c}}}"
		 ,cl[cli[0]],oligostr[0], cl[cli[1]],oligostr[1], cl[cli[2]],oligostr[2]);
	push_plot_label_in_ds(opn->dat[0], i, -5, label, USR_COORD);
	//if (i < 5) win_printf("Label %s\n%d %g",label,i, pos[i] - i*conv_bp_nm);

	if (over < 2)
	  {
	    ds->xd[2*i] = i;
	    prevx = ds->xd[2*i+1] = i;
	    ds->yd[2*i] = pos[i] - (i*conv_bp_nm) - pos[0];
	    prevy = ds->yd[2*i+1] = pos[i] - (i*conv_bp_nm) - pos[0];
	  }
	else
	  {
	    ds->xd[2*i] = prevx;
	    prevx = ds->xd[2*i+1] = i;
	    ds->yd[2*i] = prevy;
	    prevy = ds->yd[2*i+1] = pos[i] - (i*conv_bp_nm) - pos[0];
	  }

    }
    set_ds_dash_line(ds);
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}

int test_oligo_compatibility(void)
{
    int i, k;
    int   n_oligo = 0;//, maxj;
    char **loligo = NULL;
    float *pos = NULL, tmp;
    static float conv_bp_nm = 1.0, err = 2.5, maxmul= 3;
    pltreg *pr;


    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT])
        return win_printf_OK("This routine display  a blast plot between two sequences");

    if (ac_grep(cur_ac_reg, "%pr", &pr) != 1) // we get the plot region
        return win_printf_OK("cannot find data at %s:%d", __FILE__, __LINE__);

    n_oligo = grab_oligos_and_pos_from_file(&loligo, &pos);

    if (n_oligo <= 0) return 0;

    i = win_scanf("Define the bp to nm conversion %8f\n"
		  "Error %8f\n"
		  "Max error multiplier %6f", &conv_bp_nm, &err,&maxmul);

    if (i == WIN_CANCEL) return 0;
    float min_prev = 0, min_next = 0;
    int k_min_prev = 0, k_min_next = 0;
    for (i = 0; i < n_oligo; i++)
    {
      min_prev = -1;
      min_next = -1;
      k_min_prev = -1;
      k_min_next = -1;
      for (k = 0; k < n_oligo; k++)
	{
	  if (k == i) continue;
	  if (loligo[i][1] == loligo[k][0] && loligo[i][2] == loligo[k][1])
	    {
	      tmp = fabs(pos[i] - pos[k]);
	      if (min_prev < 0 || min_prev > tmp)
		{
		  k_min_prev = k;
		  min_prev = tmp;
		}
	    }
	  if (loligo[i][0] == loligo[k][1] && loligo[i][1] == loligo[k][2])
	    {
	      tmp = fabs(pos[i] - pos[k]);
	      if (min_next < 0 || min_next > tmp)
		{
		  k_min_next = k;
		  min_next = tmp;
		}
	    }
	}
      if ((min_prev > maxmul*err) || (min_next > maxmul*err))
	win_printf("Hybridization %d (%s at %g) is suspicious\n"
		   "Previous %d %s at distance %g\n"
		   "Next %d %s at distance %g\n"
		   ,i,loligo[i],pos[i]
		   ,k_min_prev,(k_min_prev>=0)?loligo[k_min_prev]:"none"
		   ,(k_min_prev >= 0) ? pos[i]-pos[k_min_prev] : -1
		   ,k_min_next,(k_min_next >= 0) ? loligo[k_min_next]:"none"
		   ,(k_min_next>=0)?pos[k_min_next]-pos[i]:-1);

    }
    return D_O_K;
}

int test_oligo_align(void)
{
    int i, k;
    int   n_oligo = 0, *used = NULL;//, maxj;
    char **loligo = NULL;
    float *pos = NULL, tmp;
    static float conv_bp_nm = 1.0, err = 2.5, maxmul= 3;
    pltreg *pr;
    O_p *op = NULL;
    d_s *ds = NULL;


    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT])
        return win_printf_OK("This routine display  a blast plot between two sequences");

    if (ac_grep(cur_ac_reg, "%pr", &pr) != 1) // we get the plot region
        return win_printf_OK("cannot find data at %s:%d", __FILE__, __LINE__);

    n_oligo = grab_oligos_and_pos_from_file(&loligo, &pos);

    if (n_oligo <= 0) return 0;

    used = (int*)calloc(n_oligo,sizeof(int));
    if (used == NULL) return win_printf_OK("cannot alloc used");
    i = win_scanf("Define the bp to nm conversion %8f\n"
		  "Error %8f\n"
		  "Max error multiplier %6f", &conv_bp_nm, &err,&maxmul);

    if (i == WIN_CANCEL) return 0;
    float min_prev = 0, min_next = 0;
    int k_min_prev = 0, k_min_next = 0, finish = 0;
    op = create_and_attach_one_empty_plot(pr, 0);
    if (op == NULL) return win_printf_OK("cannot alloc op");
    for (i = 0; i < n_oligo; i++)
    {
      min_prev = -1;
      min_next = -1;
      k_min_prev = -1;
      k_min_next = -1;
      for (k = 0; k < n_oligo; k++) used[k] = 0;
      ds = create_and_attach_one_ds(op,16, 16, 0);
      if (ds == NULL) return win_printf_OK("cannot alloc ds");
      ds->nx = ds->ny = 0;
      for (finish = 0; finish == 0; )
	{
	  for (k = 0; k < n_oligo; k++)
	    {
	      if (k == i) continue;
	      if (used[k]) continue;
	      if (loligo[i][1] == loligo[k][0] && loligo[i][2] == loligo[k][1])
		{
		  tmp = fabs(pos[i] - pos[k]);
		  if (min_prev < 0 || min_prev > tmp)
		    {
		      k_min_prev = k;
		      min_prev = tmp;
		    }
		}
	      if (loligo[i][0] == loligo[k][1] && loligo[i][1] == loligo[k][2])
		{
		  tmp = fabs(pos[i] - pos[k]);
		  if (min_next < 0 || min_next > tmp)
		    {
		      k_min_next = k;
		      min_next = tmp;
		    }
		}
	    }
	  if ((min_prev > maxmul*err) || (min_next > maxmul*err))
	    win_printf("Hybridization %d (%s at %g) is suspicious\n"
		       "Previous %d %s at distance %g\n"
		       "Next %d %s at distance %g\n"
		       ,i,loligo[i],pos[i]
		       ,k_min_prev,(k_min_prev>=0)?loligo[k_min_prev]:"none"
		       ,(k_min_prev >= 0) ? pos[i]-pos[k_min_prev] : -1
		       ,k_min_next,(k_min_next >= 0) ? loligo[k_min_next]:"none"
		       ,(k_min_next>=0)?pos[k_min_next]-pos[i]:-1);
	}

    }
    return D_O_K;
}



// construct an array containing the number of oligo being present in a sequence



MENU *findSeq_plot_menu(void)
{
    static MENU mn[32], emn[32];

    if (mn[0].text != NULL) return mn;

    add_item_to_menu(mn, "Load fasta file", do_load_fasta_file, NULL, 0, NULL);
    add_item_to_menu(mn, "select fasta sequence", do_select_fasta_seq, NULL, 0, NULL);
    add_item_to_menu(mn, "Clear fasta sequences", do_clear_sequence_list, NULL, 0, NULL);
    add_item_to_menu(mn, "Find oligo", do_find_oligo, NULL, 0, NULL);
    add_item_to_menu(mn, "Find triplex", do_find_oligo_triplex, NULL, 0, NULL);
    add_item_to_menu(mn, "Find oligo plot", do_findSeq_oligo_plot, NULL, 0, NULL);
    add_item_to_menu(mn, "Displays N islands", do_findSeq_plot_NN_in_fasta, NULL, 0, NULL);



    add_item_to_menu(mn, "Correlation in oligo plot", do_findSeq_retrieve_oligo_finger_print_in_plot, NULL, 0, NULL);
    add_item_to_menu(mn, "Find # of oligo of size n", do_findSeq_count_oligo, NULL, 0, NULL);
    add_item_to_menu(mn, "Find # of oligo of size n ++", do_findSeq_count_oligo2, NULL, 0, NULL);
    add_item_to_menu(mn, "Find common oligos", do_findSeq_count_common_oligos, NULL, 0, NULL);
    add_item_to_menu(mn, "Draw common oligos in 2 seq", do_draw_common_oligos_2_seq, NULL, 0, NULL);
    add_item_to_menu(mn, "Draw common oligos having N hits", do_draw_common_oligos_between_N_seq, NULL, 0, NULL);
    add_item_to_menu(mn, "Draw image common oligos in 2 seq", do_draw_common_oligos_2_seq_im, NULL, 0, NULL);
    add_item_to_menu(mn, "dump non overlap", count_non_overlapping_oligos, NULL, 0, NULL);
    add_item_to_menu(mn, "dump non overlap non hyb", count_non_overlapping_non_partial_hybridizing_oligos, NULL, 0, NULL);
    add_item_to_menu(mn, "dump non overlap non hyb woobble",
                     count_non_overlapping_non_partial_hybridizing_oligos_with_woobbles, NULL, 0, NULL);


    add_item_to_menu(mn, "dump binary code", count_binary_number, NULL, 0, NULL);
    add_item_to_menu(mn, "Oligo length for Tm", findSeq_plot_oligos_with_minimal_Tm, NULL, 0, NULL);
    add_item_to_menu(mn, "Oligo pre viterbi", test_oligo_pos, NULL, 0, NULL);
    add_item_to_menu(mn, "Test Oligo compatibility", test_oligo_compatibility, NULL, 0, NULL);



    add_item_to_menu(mn, "Create optimizing stuff", optimizing_common_oligos_seq_im, NULL, 0, NULL);
    add_item_to_menu(mn, "Chg params", do_chg_param, NULL, 0, NULL);
    add_item_to_menu(mn, "Test overlap", test_ovelap_and_hybridized, NULL, 0, NULL);
    add_item_to_menu(mn, "Test overlap file", test_ovelap_and_hybridized_on_file, NULL, 0, NULL);
    add_item_to_menu(mn, "Test overlap with woobbles in file", test_ovelap_and_hybridized_with_woobbles_on_file, NULL, 0,
                     NULL);
    add_item_to_menu(mn, "Test load multiplex", test_load_multiplex, NULL, 0, NULL);
    add_item_to_menu(mn, "Plot multiplex oligos on seq", findSeq_plot_multiplex_oligo_covering_on_seq, NULL, 0, NULL);
    add_item_to_menu(mn, "Ambiguous sequences fct.", NULL, emn, 0, NULL);
    add_item_to_menu(emn, "test ambiguous seq",do_findSeq_ambiguous_oligo_assemble_plot, NULL, 0, NULL);
    add_item_to_menu(emn, "test ambiguous fasta seq",do_findSeq_ambiguous_oligo_assemble_plot_of_fasta, NULL, 0, NULL);
    add_item_to_menu(emn, "test ambiguous 2 strands fasta seq",do_findSeq_ambiguous_2_strands_oligo_assemble_plot_of_fasta, NULL, 0, NULL);

    add_item_to_menu(emn, "test MT of ambiguous seq",do_findSeq_MT_ambiguous_oligo_assemble_plot, NULL, 0, NULL);
    add_item_to_menu(emn, "Fit line with imposed slope",do_fit_line_with_impose_slope, NULL, 0, NULL);




    //add_item_to_menu(mn,"data set rescale in Y", do_findSeq_rescale_data_set,NULL,0,NULL);
    //add_item_to_menu(mn,"plot rescale in Y", do_findSeq_rescale_plot,NULL,0,NULL);

    return mn;
}

MENU *findSeq_image_menu(void)
{
    static MENU mn[32];

    if (mn[0].text != NULL) return mn;

    add_item_to_menu(mn, "Chg params", do_chg_param, NULL, 0, NULL);
    return mn;
}

int findSeq_main(void)
{
    add_plot_treat_menu_item("findSeq", NULL, findSeq_plot_menu(), 0, NULL);
    add_plot_treat_menu_item("Tm_DNA", NULL, Tm_DNA_plot_menu(), 0, NULL);
    add_image_treat_menu_item("findSeq", NULL, findSeq_image_menu(), 0, NULL);
    return D_O_K;
}

int findSeq_unload(void)
{
    remove_item_to_menu(plot_treat_menu, "findSeq", NULL, NULL);
    remove_item_to_menu(plot_treat_menu, "Tm_DNA", NULL, NULL);
    return D_O_K;
}
#endif

// http://pimprenelle.lps.ens.fr/wwwXvin/Pico_setup.exe
