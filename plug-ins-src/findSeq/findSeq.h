#pragma once
#include <xvin.h>
#include "../fasta_utils/fasta_utils.h"
PXV_FUNC(int, find_complementary_with_woobble, (char *oli, char *comp, int size_c));
PXV_FUNC(int, do_findSeq_rescale_plot, (void));
PXV_FUNC(MENU*, findSeq_plot_menu, (void));
PXV_FUNC(int, do_findSeq_rescale_data_set, (void));
PXV_FUNC(int, findSeq_main, (void));
PXV_FUNC(int, findOligoInSeq, (sqd *msqd, const char *oligo, int start, int noNs));
PXV_FUNC(int, findOligoInSeq_norepeat, (sqd *msqd, const char *oligo, int start, int noNs));

// TODO : get all prototypes.

int convert_oligoID_to_seq(char *message, int npid, int size);
int convert_oligo_seq_to_ID(char *seq, int size);
int grab_oligos_from_file(char ***loligo);
int countOligo(sqd *msqd, int oligoSize, unsigned int **oligoId, int start, int end);
//int registerOligo(sqd *msqd, int seqid, int oligoSize, dya ** **oligoReg, int start, int end);
char *findSeq_idle_point_add_display(struct one_plot *op, DIALOG *d, int nearest_ds, int nearest_point);
int countOligoSpecificAmongSeqInPr(int oligoSize, pltreg *pr, int start, int end, int min, int max, char *oligocenter);
int countOligoInPr(sqd *msqd, int oligoSize, pltreg *pr, int start, int end, int nhyb, FILE *fp);
int findNNNBackwardInSeq(sqd *msqd, int start, int stop);
int do_find_oligo(void);
int do_find_oligo_triplex(void);
int do_findSeq_plot_NN_in_fasta(void);
