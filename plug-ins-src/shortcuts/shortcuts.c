#include <allegro.h>
#include "xvin.h"

#include "shortcuts.h"
#include "form_builder.h"

#ifdef PIAS
#include "../trackBead/trackBead.h"
#include "../trackBead/record.h"
#include "../cordrift/cordrift.h"
#endif
#include "p_treat_basic.h"
#include "p_treat_math.h"
#include "plot_reg_gui.h"


typedef struct
{
    char *name;
    int key;
    int (*callback)(void);
    bool enable;
} keybinding_t;

#define XV_SCKB_NKEYS 28
const char *key_names[XV_SCKB_NKEYS] ={ "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "PGUP", "PGDN"};
const int key_vals[XV_SCKB_NKEYS] = {KEY_A, KEY_B, KEY_C, KEY_D, KEY_E, KEY_F, KEY_G, KEY_H, KEY_I, KEY_J, KEY_K, KEY_L, KEY_M, KEY_N, KEY_O, KEY_P, KEY_Q, KEY_R, KEY_S, KEY_T, KEY_U, KEY_V, KEY_W, KEY_X, KEY_Y, KEY_Z, KEY_PGUP, KEY_PGDN};
#ifdef PIAS
int g_shortcuts_size = 13;
#else
int g_shortcuts_size = 10;
#endif
keybinding_t g_keybinding[32] =
{
    {"Kill_plot", KEY_A, op_menu_kill_op, true},
    {"Kill_nearest_ds", KEY_U, rm_nearest_ds, true},
    {"Go_prev_plot", KEY_I, do_prev_plot, true},
    {"Go_next_plot", KEY_E, do_next_plot, true},
    {"Select next ds",KEY_Q,do_next_ds,true},
    {"Select prev ds",KEY_W,do_prev_ds,true},
    {"Remove_invisible_pts_all_ds", KEY_P, do_rm_invisible, true},
    {"Remove_visible_pts_all_ds", KEY_O, do_rm_visible, true},
    {"Adjust_y", KEY_Y, adjust_ds_mean_y_according_to_visible_points, true},
    {"Auto_XY_Limits", KEY_B, auto_x_y_limit, true},
    #ifdef PIAS
    {"Erase visible events",KEY_K,erase_bead_visible_events,true},
    {"Switch current bead fixed status",KEY_F,switch_current_bead_fixed_status,true},
    {"erase data of that bead in G4",KEY_R,do_erase_corresponding_points_in_g4_data,true},
    #endif
    {NULL, 0, NULL, false}
};
#ifdef PIAS
int do_treat_bead(void)
{
    if (updating_menu_state != 0)
    {
        active_menu->flags &= ~D_DISABLED;
        return D_O_K;
    }

    do_cordrift_reproduce_decreasing_steps_duration();
    do_build_histo_with_exponential_convolution();
    find_peaks_in_Y_of_equally_spaced_ds_all_ds();
    return D_O_K;
}
#endif

int do_next_plot(void)
{
    if (updating_menu_state != 0)
    {
        active_menu->flags &= ~D_DISABLED;
        return D_O_K;
    }

    pltreg *pr = 0;
    ac_grep(cur_ac_reg, "%pr", &pr); // we get the plot region
    int nextop = pr->cur_op + 1 % pr->n_op;
    switch_plot(pr, nextop);
    pr->one_p->need_to_refresh = 1;
    DIALOG *di = find_dialog_associated_to_pr(pr, NULL);

    if (di)
    {
        di->proc(MSG_DRAW, di, 0);
    }

    return D_O_K;
}
int do_prev_plot(void)
{
    if (updating_menu_state != 0)
    {
        active_menu->flags &= ~D_DISABLED;
        return D_O_K;
    }

    pltreg *pr = 0;
    ac_grep(cur_ac_reg, "%pr", &pr); // we get the plot region
    int prevop = pr->cur_op - 1;

    if (prevop < 0)
    {
        prevop = pr->n_op - 1;
    }

    switch_plot(pr, prevop);
    pr->one_p->need_to_refresh = 1;
    DIALOG *di = find_dialog_associated_to_pr(pr, NULL);

    if (di)
    {
        di->proc(MSG_DRAW, di, 0);
    }

    return D_O_K;
}

int do_prev_ds(void)
{
    if (updating_menu_state != 0)
    {
        active_menu->flags &= ~D_DISABLED;
        return D_O_K;
    }
    O_p *op = NULL;
    pltreg *pr = 0;
    ac_grep(cur_ac_reg, "%pr%op", &pr,&op); // we get the plot region


    int curdat = op->cur_dat;
    if (curdat == 0 )op->cur_dat = op->n_dat - 1;
    else op->cur_dat = curdat - 1;
    pr->one_p->need_to_refresh = 1;
    DIALOG *di = find_dialog_associated_to_pr(pr, NULL);

    if (di)
    {
        di->proc(MSG_DRAW, di, 0);
    }

    return D_O_K;
}

int do_next_ds(void)
{
    if (updating_menu_state != 0)
    {
        active_menu->flags &= ~D_DISABLED;
        return D_O_K;
    }

    O_p *op = NULL;

    pltreg *pr = 0;
    ac_grep(cur_ac_reg, "%pr%op", &pr,&op); // we get the plot region


    int curdat = op->cur_dat;
    op->cur_dat = (curdat + 1) % op->n_dat;
    pr->one_p->need_to_refresh = 1;
    DIALOG *di = find_dialog_associated_to_pr(pr, NULL);

    if (di)
    {
        di->proc(MSG_DRAW, di, 0);
    }

    return D_O_K;
}

int rm_nearest_ds(void)
{
    last_data_mouse_x = mouse_x;
    last_data_mouse_y = mouse_y;
    do_remove_nearest_ds(false);
    return 0;
}
int search_id_from_key(int ky)
{
    for (int i = 0; i < XV_SCKB_NKEYS; ++i)
    {
        if (key_vals[i] == ky)
        {
            return i;
        }
    }
    return 0;
}
int push_key_option(form_window_t *form, int *ky)
{
    *ky = search_id_from_key(*ky);

    for (int i = 0; i < XV_SCKB_NKEYS; ++i)
    {
        form_push_combo_option(form, key_names[i], ky);
    }

    return 0;
}

int do_setup_keybindings(void)
{
    if (updating_menu_state == 0)
    {
        form_window_t *form = form_create("Keybindings setup");

        for (int i = 0; i < g_shortcuts_size; ++i)
        {
            keybinding_t *kb = &g_keybinding[i];
            form_push_bool(form, &(kb->enable));
            form_push_label(form, " %s ", kb->name);
            push_key_option(form, &(kb->key));
            form_newline(form);
        }

        form_run(form);
        form_free(form);

        // Converting back from combo value
        for (int i = 0; i < g_shortcuts_size; ++i)
        {
            keybinding_t *kb = &g_keybinding[i];
            kb->key = key_vals[kb->key];

            set_config_int("KEYBINDINGS-KEYS", kb->name, kb->key);
            set_config_int("KEYBINDINGS-ENABLE", kb->name, kb->enable);
        }
	    flush_config_file();
        remove_item_to_menu(plot_treat_menu, "keybindings", NULL, NULL);
        add_plot_treat_menu_item("keybindings", NULL, keybindings_plot_menu(), 0, NULL);
        do_update_menu();
    }

    for (int i = 0; i < g_shortcuts_size; ++i)
    {
        keybinding_t *sc = &g_keybinding[i];

        if (sc->enable)
        {
            remove_short_cut_from_dialog(0, sc->key);
            add_keyboard_short_cut(0, sc->key, 0, sc->callback);
        }
    }

    return D_O_K;
}

MENU *keybindings_plot_menu(void)
{
    static MENU mn[32] = {0};
    char buf[1024] = {0};

    if (mn[0].text != NULL)
    {
        return mn;
    }

    add_item_to_menu(mn, "Setup keybindings", do_setup_keybindings, NULL, 0, NULL);


    for (int i = 0; i < g_shortcuts_size; ++i)
    {
        keybinding_t *sc = &g_keybinding[i];

        if (sc->enable)
        {
            int ky = search_id_from_key(sc->key);
            snprintf(buf, 1024, "%s [%s]", sc->name, key_names[ky]);
            add_item_to_menu(mn, strdup(buf), NULL, NULL, 0, NULL);
        }
    }

    return mn;
}
int do_rm_invisible(void)
{
    active_menu->flags = DS_ALL;
    active_menu->dp = "INVISIBLE";
    cheatting();
    return 0;

}
int do_rm_visible(void)
{
    active_menu->flags = DS_ALL;
    active_menu->dp = "VISIBLE";
    cheatting();
    return 0;
}
int	shortcuts_main(int argc, char **argv)
{
    for (int i = 0; i < g_shortcuts_size; ++i)
    {
        keybinding_t *sc = &g_keybinding[i];
        sc->key = get_config_int("KEYBINDINGS-KEYS", sc->name, sc->key);
        sc->enable = get_config_int("KEYBINDINGS-ENABLE", sc->name, sc->enable);
    }

    add_plot_treat_menu_item("keybindings", NULL, keybindings_plot_menu(), 0, NULL);
    // QuickLinks
#ifdef PIAS
    add_item_to_menu(quicklink_menu, "Adjust y",        adjust_ds_mean_y_according_to_visible_points,          NULL,   0,
                     NULL);
    add_item_to_menu(quicklink_menu, "Remove Invisible points (all ds)",	cheatting,	NULL,	DS_ALL, (char *) "INVISIBLE");
    add_item_to_menu(quicklink_menu, "Remove Visible points (all ds)",	cheatting,	NULL,	DS_ALL, (char *) "VISIBLE");
    add_item_to_menu(quicklink_menu, "Make binding histogram", do_treat_bead, NULL, 0, NULL);
#endif
    return D_O_K;
}
int	shortcuts_unload(int argc, char **argv)
{
    remove_item_to_menu(plot_treat_menu, "keybindings", NULL, NULL);
    return D_O_K;
}
