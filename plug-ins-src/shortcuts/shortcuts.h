#pragma once
#include "xvin.h"
#include "form_builder.h"

PXV_FUNC(int, shortcuts_main, (int argc, char **argv));
PXV_FUNC(int,	shortcuts_unload, (int argc, char **argv));
MENU *keybindings_plot_menu(void);

int do_treat_bead(void);
int do_next_plot(void);
int do_prev_plot(void);
int do_next_ds(void);
int do_prev_ds(void);
int do_rm_invisible(void);
int do_rm_visible(void);
int rm_nearest_ds(void);
int do_setup_keybindings(void);
int push_key_option(form_window_t *form, int *ky);
int search_id_from_key(int ky);
