/*
*    Plug-in program for image treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _ISING_C_
#define _ISING_C_

# include "allegro.h"
# include "xvin.h"

/* If you include other regular header do it here*/ 
# include "../nrutil/nrutil.h"
//# include "markov_heatbath.h"
# include "time.h"

/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "ising.h"
//place here other headers of this plugin 


long idum; // the random seed
int flip = 0;
float temperature = 1;
int heatbath = 0;
float field_ext = 0;
d_s *ds = NULL; // pointer to dataset
O_p *op = NULL; // pointer to plot

// we put in this routine all the simulation and the screen display 
// this routine is executed when the ising image is selected and 
// when nothing else is activated. This routine should execute in a small
// fraction of a second so that the system will have a good response time
// to the user action 

// this dummy routine select randomly pixel and then flip them (0->255) 
// or (255->0)
int dummy_ising_oi_idle_action(struct  one_image *oi, DIALOG *d)
{
  imreg *imr;
  unsigned long t0, t1;
  int x, y, j;
  double randomnr;
  // we recover usefull data to display the image
  if (d->dp == NULL)    return 1;
  imr = (imreg*)d->dp;        /* the image region is here */
  if (imr->one_i == NULL || imr->n_oi == 0)        return 1;
 
  if (heatbath == 0)
  {
    for (t0 = my_uclock(), t1 = t0 + get_my_uclocks_per_sec()/30; t0 < t1; t0 = my_uclock()) 
    {// we repeat action for 1/30 of a second
      for (j = 0; j < 1000; j++, flip++)
	  {
        x = oi->im.nx *ran1(&idum); // oi->im.nx is the image size in x -> take a random x position 
        y = oi->im.ny * ran1(&idum); // oi->im.ny is the image size in x -> take a random x position
        randomnr = ran1(&idum);
        float probability;
        probability = proba(oi, oi->im.nx, x, y);
        if (randomnr < probability) { flip_spin(oi, x, y); }
	  }
    }
  }
  if (heatbath == 1)
  {
    for (t0 = my_uclock(), t1 = t0 + get_my_uclocks_per_sec()/30; t0 < t1; t0 = my_uclock()) 
    {// we repeat action for 1/30 of a second
      for (j = 0; j < 1000; j++, flip++)
	  {
        x = oi->im.nx *ran1(&idum); // oi->im.nx is the image size in x -> take a random x position 
        y = oi->im.ny * ran1(&idum); // oi->im.ny is the image size in x -> take a random x position
        randomnr = ran1(&idum);
        float probability;
        probability = proba_hb(oi, oi->im.nx, x, y);
        if (randomnr < probability) { flip_spin_hb(oi, x, y, 255); }
		else {flip_spin_hb(oi, x, y, 0);}
	  }
    }
  }
  add_new_point_to_ds(ds, flip, magnetization(oi,oi->im.nx)); // adds datapoint to graph
  set_plot_title(op, "Ising model %dx%d flips %d",oi->im.nx,oi->im.ny,flip);
  oi->need_to_refresh |= ALL_NEED_REFRESH; // data has changed and image must be redrawn 
  return refresh_image(imr, UNCHANGED);
}


int create_ising_image(void)
{
  idum = time(NULL);
  O_i *oi;
  imreg *imr;
  int i,k;
  int alg = 1;
  static int size = 512;
  
  // keep this following line
  if(updating_menu_state != 0)	return D_O_K;
  
  // this is a description message obtained when pressing shift left 
  if (key[KEY_LSHIFT])  
    return win_printf_OK("This routine simulate a 2D ising model");

  // we retrieve the image region
  if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
      return win_printf_OK("Cannot find image");

  // we ask the user his choice
  i = win_scanf("This routine simulate the 2D Ising model\n"
		"define the size of the 2D matrix size %8d",&size);
  if (i == CANCEL)	return D_O_K;
  // we create an image of unsigned char which will be the ising matrix
  oi = create_and_attach_oi_to_imr(imr, size, size, IS_CHAR_IMAGE);
  if (oi == NULL) return win_printf_OK("Cannot create image");

  get_temperature();

  k = win_scanf("Use Markov or heatbath algorithm (1/2) %8d",&alg);
  if (k == CANCEL)	return D_O_K;
  if (alg == 1||2){ heatbath = alg -1;}
  else{ return win_printf_OK("Input not correct");}

  get_field_ext();

  construct(oi, size);

  op = create_and_attach_op_to_oi(oi, 1, 1, 0, 0);
  if (op == NULL)    return win_printf_OK("cannot create calibration plot !");

  ds = op->dat[0];
  set_ds_source(ds, "Calibration nearest profile -1       ");

  ds->nx = ds->ny = 0;   

  set_plot_title(op, "Ising model %dx%d",size,size);
  oi->oi_idle_action = dummy_ising_oi_idle_action;
  find_zmin_zmax(oi);
  return (refresh_image(imr, imr->n_oi - 1));
}


MENU *ising_image_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"Create 2D ising", create_ising_image,NULL,0,NULL);
	add_item_to_menu(mn,"Change temperature", get_temperature,NULL,0,NULL);
	add_item_to_menu(mn,"Change external field", get_field_ext,NULL,0,NULL);
	return mn;
}

int	ising_main(int argc, char **argv)
{
	add_image_treat_menu_item ( "ising", NULL, ising_image_menu(), 0, NULL);
	return D_O_K;
}

int	ising_unload(int argc, char **argv)
{
	remove_item_to_menu(image_treat_menu, "ising", NULL, NULL);
	return D_O_K;
}
#endif

void construct(struct  one_image *oi, int length) //Generates a field of length times length random orientated spins
{
    int i, j;
	for (i = 0; i < length; i++)
	{
		for (j = 0; j < length; j++)
		{
			int temp = rand()%2; // Chooses randomly between 0 and 1
			temp *= 255;
			UCHAR_PIXEL(oi,i,j) = temp;
		}
	}
}

int neighbours(struct  one_image *oi, int length, int x, int y) // Calculates the sum of the neighbours of spin[x][y], accounting for the boudaries of the field
{
	int temp = 0 ;
	if ( y > 0) { temp += (2 * UCHAR_PIXEL(oi,x,y-1) / 255) - 1; } // Maps 255/0 to +1/-1 and adds the value of spin[x][y-1] to temp
	if (y < (length-1)) { temp += (2 * UCHAR_PIXEL(oi,x,y+1) / 255) - 1; }
	if ( x > 0 ) { temp += (2 * UCHAR_PIXEL(oi,x-1,y) / 255) - 1; }
	if (x < (length-1)) { temp += (2 * UCHAR_PIXEL(oi,x+1,y) / 255) - 1; }
	return temp;
}

void flip_spin(struct  one_image *oi, int x, int y) // Flips spin[x][y]
{
	UCHAR_PIXEL(oi,x,y) = (UCHAR_PIXEL(oi,x,y)) ? 0 : 255;
	return;
}

float proba(struct  one_image *oi, int length, int x, int y) // Calculates the probability of a spin flip for the Markov algorithm
{
	float energy;
	energy = 2 * ((float)neighbours(oi, length, x, y) + field_ext) * ((2 * UCHAR_PIXEL(oi,x,y) / 255) - 1);
	float temp;
	temp = energy/temperature;
	return (exp(-temp));
}

void flip_spin_hb(struct  one_image *oi, int x, int y, int sigma) // Changes spin[x][y] into sigma
{
	UCHAR_PIXEL(oi,x,y) = sigma;
}

float proba_hb(struct  one_image *oi, int length, int x, int y) // Calculates the probability that spin[m][n] will become +1 for the heatbath algorithm
{
	float energy;
	energy = 2 * ((float) neighbours(oi, length, x, y) + field_ext);
	float temp;
	temp = energy/temperature;
	return (1/(1+exp(-temp)));
}

int get_temperature(void)
{
  // keep this following line
  if(updating_menu_state != 0)	return D_O_K;

  int j;
  j = win_scanf("Enter a temperature T > 0  %8f",&temperature);
  if (j == CANCEL)	return D_O_K;
  if (temperature <= 0) return win_printf_OK("Temperature must be larger than 0");
  return D_O_K;
}

int get_field_ext(void)
{
  // keep this following line
  if(updating_menu_state != 0)	return D_O_K;

  win_scanf("Enter energy of spin in external field %8f",&field_ext);
  return D_O_K;
}

int magnetization(struct  one_image *oi, int length)
{
	int temp = 0;
	int i,j;
	for (i = 0; i < length; i++)
	{
		for (j = 0; j < length; j++)
		{
			temp += (2 * UCHAR_PIXEL(oi,i,j) / 255) - 1;
		}
	}
	return temp;
}

# ifdef OLD

/*
*    Plug-in program for image treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _ISING_C_
#define _ISING_C_

# include "allegro.h"
# include "xvin.h"

/* If you include other regular header do it here*/ 
# include "../nrutil/nrutil.h"

/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "ising.h"
//place here other headers of this plugin 


int idum = 4587; // the random seed
int flip = 0;

// we put in this routine all the simulation and the screen display 
// this routine is executed when the ising image is selected and 
// when nothing else is activated. This routine should execute in a small
// fraction of a second so that the system will have a good response time
// to the user action 

// this dummy routine select randomly pixel and then flip them (0->255) 
// or (255->0)
int dummy_ising_oi_idle_action(struct  one_image *oi, DIALOG *d)
{
  imreg *imr;
  unsigned long t0, t1;
  int x, y, j;


  // we recover usefull data to display the image
  if (d->dp == NULL)    return 1;
  imr = (imreg*)d->dp;        /* the image region is here */
  if (imr->one_i == NULL || imr->n_oi == 0)        return 1;

  
  for (t0 = my_uclock(), t1 = t0 + get_my_uclocks_per_sec()/30; t0 < t1; t0 = my_uclock()) 
    {// we repeat action for 1/30 of a second
      for (j = 0; j < 1000; j++, flip++)
	{
	  x = oi->im.nx * ran1(&idum); // oi->im.nx is the image size in x -> take a random x position 
	  y = oi->im.ny * ran1(&idum); // oi->im.ny is the image size in x -> take a random x position 
	  UCHAR_PIXEL(oi,x,y) = (UCHAR_PIXEL(oi,x,y)) ? 0 : 255;
	}
    }
  set_im_title(oi, "Ising model %dx%d flips %d",oi->im.nx,oi->im.ny,flip);
  oi->need_to_refresh |= ALL_NEED_REFRESH; // data has changed and image must be redrawn 
  return refresh_image(imr, UNCHANGED);
}


int compute_M_of_ising_image(void)
{
  O_i *oi = NULL;
  imreg *imr = NULL;
  int i, j, M, N;

  
  // keep this following line
  if(updating_menu_state != 0)	return D_O_K;
  
  // this is a description message obtained when pressing shift left 
  if (key[KEY_LSHIFT])  
    return win_printf_OK("This routine compute M of a 2D ising model");

  // we retrieve the image region
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)
      return win_printf_OK("Cannot find image");

  if (oi == NULL) return 0;
  if (oi->im.data_type != IS_CHAR_IMAGE)
      return win_printf_OK("Wrong image type");
  for(i = 0, M = N = 0; i < oi->im.ny; i++)
    {
      for(j = 0; j < oi->im.nx; j++)
	{
	  M += (UCHAR_PIXEL(oi,j,i)) ? 1 : -1;
	  N++;
	}
    }
  win_printf("Image %dx%d N = %d M = %d\nm = %g",oi->im.nx,oi->im.ny,N,M,(double)M/N);
  return 0;
}


int draw_computed_M_of_ising_image(void)
{
  O_i *oi = NULL;
  imreg *imr = NULL;
  int i, j, M, N;

  
  // keep this following line
  if(updating_menu_state != 0)	return D_O_K;
  
  // this is a description message obtained when pressing shift left 
  if (key[KEY_LSHIFT])  
    return win_printf_OK("This routine compute M of a 2D ising model");

  // we retrieve the image region
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)
      return win_printf_OK("Cannot find image");

  if (oi == NULL) return 0;
  if (oi->im.data_type != IS_CHAR_IMAGE)
      return win_printf_OK("Wrong image type");
  for(i = 0, M = N = 0; i < oi->im.ny; i++)
    {
      for(j = 0; j < oi->im.nx; j++)
	{
	  M += (UCHAR_PIXEL(oi,j,i)) ? 1 : -1;
	  N++;
	}
    }
  win_printf("Image %dx%d N = %d M = %d\nm = %g",oi->im.nx,oi->im.ny,N,M,(double)M/N);
  return 0;
}


int create_ising_image(void)
{
  O_i *oi;
  imreg *imr;
  int i;
  static int size = 512;
  
  // keep this following line
  if(updating_menu_state != 0)	return D_O_K;
  
  // this is a description message obtained when pressing shift left 
  if (key[KEY_LSHIFT])  
    return win_printf_OK("This routine simulate a 2D ising model");

  // we retrieve the image region
  if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
      return win_printf_OK("Cannot find image");

  // we ask the user his choice
  i = win_scanf("This routine simulate the 2D Ising model\n"
		"define the size of the 2D matrix size %8d",&size);
  if (i == CANCEL)	return D_O_K;
  // we create an image of unsigned char which will be the ising matrix
  oi = create_and_attach_oi_to_imr(imr, size, size, IS_CHAR_IMAGE);
  if (oi == NULL) return win_printf_OK("Cannot create image");

  // to read or write a pixel value use this macro : UCHAR_PIXEL(oi,line,col)
  // example :
  UCHAR_PIXEL(oi,0,0) = 255;
  UCHAR_PIXEL(oi,0,size-1) = 255;
  UCHAR_PIXEL(oi,size-1,0) = 255;
  UCHAR_PIXEL(oi,size-1,size-1) = 255;
  UCHAR_PIXEL(oi,size/2,size/2) = 255;
  set_im_title(oi, "Ising model %dx%d",size,size);
  oi->oi_idle_action = dummy_ising_oi_idle_action;
  find_zmin_zmax(oi);
  return (refresh_image(imr, imr->n_oi - 1));

}


MENU *ising_image_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"Create 2D ising", create_ising_image,NULL,0,NULL);
	add_item_to_menu(mn,"Compute M", compute_M_of_ising_image,NULL,0,NULL);


	return mn;
}

int	ising_main(int argc, char **argv)
{
  (void)argc;
  (void)argv;
	add_image_treat_menu_item ( "ising", NULL, ising_image_menu(), 0, NULL);
	return D_O_K;
}

int	ising_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;
	remove_item_to_menu(image_treat_menu, "ising", NULL, NULL);
	return D_O_K;
}
#endif

#endif
