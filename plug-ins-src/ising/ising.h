#ifndef _ISING_H_
#define _ISING_H_

PXV_FUNC(int, create_ising_image, (void));
PXV_FUNC(MENU*, ising_image_menu, (void));
PXV_FUNC(int, ising_main, (int argc, char **argv));
#endif

