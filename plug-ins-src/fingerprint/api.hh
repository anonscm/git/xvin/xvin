/**
 * @file api.hh
 * @brief 
 * @author François-Xavier Lyonnet du Moutier
 *
 * Contains library API functions for xvin
 * WIP : migrate functions from menu.cc file.
 *
 */

#pragma once

#include "sequence/fingerprint.hh"
#include "sequence/dist-oligo-sequence.hh"
#include "search/dist-oligo-sequence-finder.hh"
#include "results/dist-oligo-solution.hh"
#include "benchmark/benchmark-generator.hh"


//int benchmark_oligo_sequence_algo(benchmark::BenchmarkGenerator& bengen, O_p *op, FingerprintVector *poorResults);
//int benchmark_rank_per_hyb_count(benchmark::BenchmarkGenerator *bengen, O_p *data_op, O_p *timing_op, int sequence_size, bool use_quartile, FingerprintVector *poorResults);
