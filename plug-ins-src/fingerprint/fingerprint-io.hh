#pragma once
#include "../fasta_utils/fasta_utils.h"
#include "sequence/fingerprint.hh"
#include "sequence/pos-fingerprint.hh"
#include "sequence/dist-fingerprint.hh"
#include <stdio.h>
#include <iostream>
#include <string>
#define BUFFER_SIZE 16384

void dump_clusters(FILE *fd, FingerprintVector& fgps, int **clusters, int nb_clusters, int *cluster_size);

std::ostream& operator<<(std::ostream& os, PosFingerprint& fgprint);
std::ostream& operator<<(std::ostream& os, DistFingerprint& fgprint);
std::vector<Fingerprint *> loadFingerprintFile(std::istream& is);
PosFingerprint *loadSimplePosFingerprint(std::istream& is);
DistFingerprint *loadSimpleDistFingerprint(std::istream& is);
DistFingerprint *histogramToDistFingerprint(d_s *histogram, Oligo oligo, float threashold);


bool json2oligo(json js, oligo_t ***oligos, int *nb_oligos);
json oligo2json(oligo_t **oligos, int nb_oligos);
