#include "unit_test.hh"
#include "../fasta_utils/fasta_utils.h"
#include "xvin.h"
#include "gui/menu.hh"
#include "search/dist-class-generator.hh"
#include "gui/file-menu.hh"
#include "storage/environnement.hh"
int unit_test_load_seq_and_oligo(const char* seq_file, const char* oligo_file)
{
    if (seq_file == NULL) {
        seq_file = get_config_string("FASTA-FILE", "last_loaded", "");
        if (seq_file == NULL || strlen(seq_file) == 0 || !file_exists(seq_file, 0, NULL))
        {
            seq_file = open_one_file_config("Open fasta file", NULL, "Fasta file\0*.fa*\0All files\0*.*\0", "FASTA-FILE", "last_loaded");
        }
        if (seq_file == NULL)
        {
            return 0;
        }
    }

    if (oligo_file == NULL) {
        oligo_file = get_config_string("OLIGO-FILE", "last_loaded", "");
        printf("oligo file: $%s$\n", oligo_file);
        if (oligo_file == NULL || strlen(oligo_file) == 0 || !file_exists(oligo_file, 0, NULL))
        {
        printf("oligo file: $%s$\n", oligo_file);
            oligo_file = open_one_file_config("Open oligo file", NULL, "Oligo file\0*.oligo\0All files\0*.*\0","OLIGO-FILE", "last_loaded");
        }
        if (oligo_file == NULL)
        {
            return 0;
        }
    }

    read_fasta_seq(seq_file, true);    // read fasta file accepting "N" nucléotides
    load_oligo_file(oligo_file, true); // read oligo file loading also reverse oligo

    return D_O_K;
}

int unit_test_import_posfingerprint_from_fasta_and_oligo_and_needle(
                                                                    const char* fingerprint_file, const char* needle_file)
{

    if (fingerprint_file) {
        // load a fingerprint as reference, as pos fingerprint
        load_fingerprint(fingerprint_file, false, false, FGP_REPR_POS, false);
    }
    else {
        import_from_sequence(true, 0, false, FGP_REPR_POS, 3, false);
    }

    if (needle_file == NULL) {
        needle_file = get_config_string("SFGP-FILE", "last_loaded", "");
        if (needle_file == NULL || strlen(needle_file) == 0 || !file_exists(needle_file, 0, NULL))
        {
            needle_file = open_one_file_config("Open sfgp file", NULL, "Simple Fingerprint file\0*.sfgp*\0All files\0*.*\0","SFGP-FILE", "last_loaded");
        }
        if (needle_file == NULL)
        {
            return 0;
        }
    }

    load_fingerprint(needle_file, true, true, FGP_REPR_POS, false);

    return D_O_K;
}

int unit_test_create_dist_class_and_convert_fingerprints()
{

    fingerprint::Environnement::instance().distClasses(*fingerprint_distance_distribution(0, 0.1, MIN_BASE_NM_RATE, MAX_BASE_NM_RATE, 2.5, 3000, false));
    get_dist_oligo_sequence_from_fingerprints(FGP_LIST_HAYSTACK, -1);
    //get_dist_oligo_sequence_from_fingerprints(FGP_LIST_NEEDLE, -1);
    return D_O_K;
}
