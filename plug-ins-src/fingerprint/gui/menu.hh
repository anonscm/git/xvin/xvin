#pragma once

#ifndef XV_WIN32
# include "config.h"
#endif

/* If you include other regular header do it here*/
# include "../../findSeq/findSeq.h"
# include "../../fasta_utils/fasta_utils.h" // /!\ include in this order, to get overwritten findSeq function nam include in this order, to get overwritten findSeq function namee

/* But not below this define */
# define BUILDING_PLUGINS_DLL
#include "../define.hh"
# include "../sequence/fingerprint.hh"
#include "../sequence/pos-fingerprint.hh"
#include "../fingerprint-io.hh"
#include "../search.hh"
#include "form_builder.h"
#include "../results/matching-data.hh"
#include "../sequence/dist-oligo-sequence.hh"
#include "../view/dist-oligo-solution-viewer.hh"
#include "../search/basic-finder.hh"
# include <allegro.h>
# include <xvin.h>
#include <vector>
#include <map>
#include <iostream>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/string.hpp>
//template class boost::serialization::detail::singleton_wrapper<boost::serialization::extended_type_info_typeid<std::map<int,int,std::less<int>,std::allocator<std::pair<int,int>>>>>;

/*
 * Base plugin functions
 */
PXV_FUNC(int, fingerprint_main, (int argc, char **argv));
MENU *fingerprint_plot_menu(void);
int fingerprint_unload(int /*argc*/, char ** /*argv*/);

/*
 * Utils Functions
 */
PXV_FUNC(int, set_dosv, (DistOligoSolutionViewer *dosv));


/*
 * GUI Functions
 */
PXV_FUNC(int, do_ouput_random_subfgp, (void));
PXV_FUNC(int, do_check_boxplot, (void));
PXV_FUNC(int, do_fingerprint_search, (void));
PXV_FUNC(int, do_fingerprint_search_rank, (void));
PXV_FUNC(int, do_fingerprint_dist_oligo_sequence_search, (void));
PXV_FUNC(int, do_get_dist_oligo_sequence_from_fingerprints, (void));
PXV_FUNC(int, do_export_last_solutions, (void));
PXV_FUNC(int, do_fingerprint_multiple_match, (void));
PXV_FUNC(int, do_fingerprint_coperture, (void));
PXV_FUNC(int, do_fingerprint_solviewer_configure, (void));
PXV_FUNC(int, do_solviewer_go_next_pos, (void));
PXV_FUNC(int, do_solviewer_go_prev_pos, (void));
PXV_FUNC(int, do_matchdata_load, (void));
PXV_FUNC(int, do_fingerprint_efficency_by_size, (void));
PXV_FUNC(int, do_fingerprint_distance_distribution, (void));
PXV_FUNC(int, do_display_oligo_index,(void));
PXV_FUNC(int, do_create_stats,(void));
PXV_FUNC(int, do_unit_test,(void));
PXV_FUNC(int, do_fingerprint_plot_search_algo_performance,(void));
PXV_FUNC(int, do_fingerprint_distribution_for_all_oligos_and_generate_class,(void));
PXV_FUNC(int, do_fingerprint_solviewer_configure, (void));
PXV_FUNC(int, do_solviewer_go_next_pos, (void));
PXV_FUNC(int, do_solviewer_go_prev_pos, (void));
PXV_FUNC(int, do_solviewer_dec_ref_peak, (void));
PXV_FUNC(int, do_solviewer_inc_ref_peak, (void));


/*
 * Treatement
 */
PXV_FUNC(int, get_dist_oligo_sequence_from_fingerprints, (FgpList fgplist, int index));
int fingerprint_info(std::shared_ptr<Fingerprint> fgp);
PXV_FUNC(int, fingerprint_solviewer_display, ());
DistClasses *fingerprint_distance_distribution(int haystack_idx, float fraction_in_class, float minBaseNmRate,
        float maxBaseNmRate, float classWidthMultiplifier, int maxDistance,
        bool in_current_plot);

PXV_FUNC(Fingerprint *, create_random_subfgp, (
    std::vector<Fingerprint *> fgps,
    int *fgp_idx,
    int numSample,
    int fgpSize,
    int fgpstddevSize,
    float noiseDeviation,
    float avgBaseNmRate,
    float stddevBaseNmRate,
    float meanPoissonDistMissingHyb,
    ReverseSetting revSetting,
    int minHyb,
    int *locus));
int fingerprint_distribution_for_all_oligos_and_generate_class(const char *seq_file, int oligo_size,
        float fraction_in_class, float minBaseNmRate, float maxBaseNmRate, float classWidthMultiplifier,
        int upper_threshold);
const char *generate_oligo(int size);
const char *generate_oligo_from_seq(sqd *sequence, int size);

/*
 * Idle action
 */
PXV_FUNC(int, average_efficency_plot_idle_action, (O_p *op, DIALOG *d));
PXV_FUNC(int, hybridation_efficency_plot_idle_action, (O_p *op, DIALOG *d));

char *fingerprint_idle_view(struct one_plot *op, DIALOG * /*d*/, int nearest_ds, int nearest_point);
char *fingerprint_idle_distribution(O_p *op, DIALOG * /*d*/, int nearest_ds, int nearest_point);


/*
 * Print functions
 */
std::ostream& print_search_match(
    std::ostream& os, DistFingerprint& haystack, DistFingerprint& needle, SearchMatch& searchMatch);
std::ostream& print_solution(
    std::ostream& os, Fingerprint& haystack, Fingerprint& needle, SearchMatch& searchMatch);

void draw_search_match(
    d_s *dataset, DistFingerprint& haystack, DistFingerprint& needle, SearchMatch& searchMatch);
void draw_im_search_match(
    DistFingerprint& haystack, DistFingerprint& needle, SearchMatch& searchMatch);

/*
 * Other
 */
int plot_matchingDatas(std::vector<MatchingData>& datas, MatchingData::GetterFunc xgetter,
                       MatchingData::GetterFunc ygetter, d_s *dataset, d_s *count_dataset);
int mDataMultiplePlot(O_p *op, std::vector<MatchingData>& mds);
int mData_plot_idle_action(O_p *op, DIALOG *);
int computeEfficencyAsync(int minFgpSize, int maxFgpSize, int stepSize, int nbIteration, int seqIdx,
                          bool allowReverse, double minBaseNmRate, double maxBaseNmRate, bool
#ifndef NDEBUG
                          stopOnFail
#endif
                          ,
                          double noiseDeviation, BasicFinder refFinder);
