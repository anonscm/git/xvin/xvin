#pragma once
#include "form_builder.h"
#include "../sequence/fingerprint.hh"
#include "../sequence/dist-oligo-sequence.hh"

int generate_dos_combo_box(form_window_t *form, int *var, std::vector<std::shared_ptr<DistOligoSequence>>& vect);
int generate_combo_box(form_window_t *form, int *var, FingerprintVector& vect);
