/**
 * @file cssv-menu.hh
 * @brief ChiSquare-Candidate-Viewer Menu
 * @author François-Xavier Lyonnet du Moutier
 * @version 
 * @date 2016-04-21
 */
#pragma once
#include "xvin.h"
#include "../controller/chisquare-solution-controller.hh"
MENU *fingerprint_cssv_plot_menu(void);
int do_cssv_search();
int do_cssv_search_origin();
int do_cssv_next_candidate(void);
int do_cssv_prev_candidate(void);
int do_cssv_select_candidate(void);
int do_cssv_select_match_candidate(void);
int do_cssv_add_needle_to_needles(void);
int fingerprint_cssv_set_controller(chiSquareMethod::ChiSquareSolutionController* ct);
int do_cssv_search_all();
