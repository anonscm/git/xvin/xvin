#include "cssv-menu.hh"
#include "../controller/chisquare-solution-controller.hh"
#include "../storage/environnement.hh"
#include "form_builder.h"
#include "combo-box.hh"
#include <memory>

typedef fingerprint::Environnement Env;

chiSquareMethod::ChiSquareSolutionController *cssc = 0;

MENU *fingerprint_cssv_plot_menu(void)
{
    static MENU cssv_menu[32];

    if (cssv_menu[0].text != NULL)
    {
        return cssv_menu;
    }

    add_item_to_menu(cssv_menu, "Search", do_cssv_search, NULL, 0, NULL);
    add_item_to_menu(cssv_menu, "Search Origin", do_cssv_search_origin, NULL, 0, NULL);
    add_item_to_menu(cssv_menu, "Search All reference", do_cssv_search_all, NULL, 0, NULL);
    add_item_to_menu(cssv_menu, "Next Candidate", do_cssv_next_candidate, NULL, 0, NULL);
    add_item_to_menu(cssv_menu, "Prev Candidate", do_cssv_prev_candidate, NULL, 0, NULL);
    add_item_to_menu(cssv_menu, "Select Candidate", do_cssv_select_candidate, NULL, 0, NULL);
    add_item_to_menu(cssv_menu, "Select and view Match of Candidate", do_cssv_select_match_candidate, NULL, 0, NULL);
    add_item_to_menu(cssv_menu, "Add current expt. data to global needle", do_cssv_add_needle_to_needles, NULL, 0, NULL);
    return cssv_menu;
}

// FIXME Q&D fix
int fingerprint_cssv_set_controller(chiSquareMethod::ChiSquareSolutionController *ct)
{
    cssc = ct;
}

int do_cssv_add_needle_to_needles(void)
{
    if (updating_menu_state != 0)
    {
        return 0;
    }

    if (!cssc)
    {
        return 0;
    }

    Env& env = Env::instance();
    auto expt = cssc->expt();

    if (!expt.expired())
    {
        auto exptl = std::shared_ptr<PosFingerprint>(new PosFingerprint(*expt.lock()));
        env.experimentals().push_back(exptl);
    }
    return 0;
}

int do_cssv_select_candidate(void)
{
    if (updating_menu_state != 0)
    {
        return 0;
    }

    if (!cssc)
    {
        return 0;
    }

    cssc->selectAndViewCandidate();
    return 0;
}

int do_cssv_select_match_candidate(void)
{
    if (updating_menu_state != 0)
    {
        return 0;
    }

    if (!cssc)
    {
        return 0;
    }

    cssc->selectAndListMatchingOfCandidate();
    return 0;
}

static int ref_idx = 0;
static int expt_idx = 0;

int do_cssv_search_origin()
{
    if (updating_menu_state != 0)
    {
        return 0;
    }

    Env& env = Env::instance();

    if (cssc == 0)
    {
        cssc = new chiSquareMethod::ChiSquareSolutionController();
    }

    form_window_t *form = form_create("X^2 Search");
    form_push_label(form, "Reference ");
    generate_combo_box(form, &ref_idx, env.references());
    form_newline(form);
    form_push_label(form, "Expt ");
    generate_combo_box(form, &expt_idx, env.experimentals());

    if (form_run(form) != WIN_OK)
    {
        form_free(form);
        return D_O_K;
    }

    form_free(form);
    cssc->config();

    if (env.references().size() > 0 && env.experimentals().size() > 0)
    {
        std::shared_ptr<PosFingerprint> refce = std::dynamic_pointer_cast<PosFingerprint>(env.references()[ref_idx]);
        std::shared_ptr<PosFingerprint> expt = std::dynamic_pointer_cast<PosFingerprint>(env.experimentals()[expt_idx]);

        if (refce && expt)
        {
            cssc->searchOrigin(refce, expt);
            cssc->selectAndViewCandidate();
        }
        else
        {
            warning_message("refce or expt are not posfgp");
        }
    }

    return 0;
}
int do_cssv_search()
{
    if (updating_menu_state != 0)
    {
        remove_short_cut_from_dialog(0, KEY_S);
        add_keyboard_short_cut(0, KEY_S, 0, do_cssv_search);
        return 0;
    }

    Env& env = Env::instance();

    if (cssc == 0)
    {
        cssc = new chiSquareMethod::ChiSquareSolutionController();
    }

    form_window_t *form = form_create("X^2 Search");
    form_push_label(form, "Reference ");
    generate_combo_box(form, &ref_idx, env.references());
    form_newline(form);
    form_push_label(form, "Expt ");
    generate_combo_box(form, &expt_idx, env.experimentals());

    if (form_run(form) != WIN_OK)
    {
        form_free(form);
        return D_O_K;
    }

    form_free(form);
    cssc->config();

    if (env.references().size() > 0 && env.experimentals().size() > 0)
    {
        std::shared_ptr<PosFingerprint> refce = std::dynamic_pointer_cast<PosFingerprint>(env.references()[ref_idx]);
        std::shared_ptr<PosFingerprint> expt = std::dynamic_pointer_cast<PosFingerprint>(env.experimentals()[expt_idx]);

        if (refce && expt)
        {
            cssc->search(refce, expt);
            cssc->selectAndViewCandidate();
        }
        else
        {
            warning_message("refce or expt are not posfgp");
        }
    }

    return 0;
}

int do_cssv_search_all()
{
    if (updating_menu_state != 0)
    {
        remove_short_cut_from_dialog(0, KEY_S);
        add_keyboard_short_cut(0, KEY_S, 0, do_cssv_search);
        return 0;
    }

    Env& env = Env::instance();

    if (cssc == 0)
    {
        cssc = new chiSquareMethod::ChiSquareSolutionController();
    }

    form_window_t *form = form_create("X^2 Search All ref" );
    form_newline(form);
    form_push_label(form, "Expt ");
    generate_combo_box(form, &expt_idx, env.experimentals());

    if (form_run(form) != WIN_OK)
    {
        form_free(form);
        return D_O_K;
    }

    form_free(form);
    cssc->config();

    if (env.references().size() > 0 && env.experimentals().size() > 0)
    {
        std::shared_ptr<PosFingerprint> expt = std::dynamic_pointer_cast<PosFingerprint>(env.experimentals()[expt_idx]);

        if (expt)
        {
            cssc->searchAll(expt);
            cssc->selectAndViewCandidate();
        }
        else
        {
            warning_message("refce or expt are not posfgp");
        }
    }

    return 0;
}

int do_cssv_next_candidate(void)
{
    O_p *op = NULL;
    pltreg *pr = NULL;

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2) // we get the plot region
    {
        return win_printf_OK("cannot find data");
    }

    if (updating_menu_state != 0)
    {
        if (op != NULL && op->user_id == 0xCAE)
        {
            remove_short_cut_from_dialog(0, KEY_RIGHT);
            add_keyboard_short_cut(0, KEY_RIGHT, 0, do_cssv_next_candidate);
        }

        return 0;
    }

    if (cssc != 0)
    {
        cssc->nextCandidate();
    }

    return 0;
}
int do_cssv_prev_candidate(void)
{
    O_p *op = NULL;
    pltreg *pr = NULL;

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2) // we get the plot region
    {
        return win_printf_OK("cannot find data");
    }

    if (updating_menu_state != 0)
    {
        if (op != NULL && op->user_id == 0xCAE)
        {
            remove_short_cut_from_dialog(0, KEY_LEFT);
            add_keyboard_short_cut(0, KEY_LEFT, 0, do_cssv_prev_candidate);
        }

        return 0;
    }

    if (cssc != 0)
    {
        cssc->prevCandidate();
    }

    return 0;
}
