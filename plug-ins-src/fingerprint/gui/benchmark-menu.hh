#pragma once
#include "xvin.h"

PXV_FUNC(int, do_benchmark_oligo_sequence_algo_dosf,(void));
PXV_FUNC(int, do_benchmark_oligo_sequence_algo_css,(void));
PXV_FUNC(int, do_benchmark_css_poor_results, ());
PXV_FUNC(int, do_benchmark_dosf_poor_results, ());
PXV_FUNC(int, do_benchmark_basf_poor_results, ());
PXV_FUNC(int, do_benchmark_rank_per_hyb_count, (void));
PXV_FUNC(MENU *, fingerprint_benchmark_plot_menu,(void));

void checkCssContr(void);
void checkDosContr(void);
void checkBasContr(void);
int auto_benchmark_from_file(const char *benchmark_file);
int do_auto_benchmark_from_file(void);
