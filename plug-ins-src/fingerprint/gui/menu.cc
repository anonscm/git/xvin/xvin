/*
 *    Plug-in program for plot treatement in Xvin.
 *
 *    François-Xavier Lyonnet du Moutier
 */
#include "menu.hh"
#include "file_picker_gui.h"
#include "../fingerprint-io.hh"
#include "../results/matching-data.hh"
#include "../search/basic-finder.hh"
#include "../search/rank-finder.hh"
#include "../search/dist-class-generator.hh"
#include "../search/dist-oligo-sequence-finder.hh"
//#include "../serialization.hh"
#include "../colortab.h"
#include "../unit_test.hh"
#include "../sequence/dist-oligo-sequence.hh"
#include "../view/basic-solution-viewer.hh"
#include "../api.hh"
#include "../view/dist-oligo-solution-viewer.hh"
#include "form_builder.h"
#include "progress_builder.h"
#include "../../ImOliSeq/ImOliSeq.h"
#include "../storage/environnement.hh"
#include "../sequence/fingerprint-sample-generator.hh"
#include "cssv-menu.hh"
#include "fgp-view-menu.hh"
#include "../view/data-list.hh"
#include "combo-box.hh"
#include "file-menu.hh"
#include "benchmark-menu.hh"
#include "search-menu.hh"
#include "../../cordrift/cordrift.h"
#include "p_treat_math.h"
#ifdef PIAS
#include "../../trackBead/trackBead.h"
#include "p_treat_basic.h"
#endif

#include <fstream>
#include <string>
#include <sstream>
#include <cassert>
#include <future>
#include <clocale>
#include <boost/serialization/vector.hpp>


typedef fingerprint::Environnement Env;

std::vector<SearchMatch> lastSolutions;
int mDataMatchBySize = 1;
int mDataHybBySize = 1;
int mDataFoundOrigBySize = 1;
int mDataChrBySize = 1;
int mDataOrigPosRankBySize = 1;
int mDataNumSampleBySize = 1;
int mDataMatchByHyb = 0;
int mDataChrByHyb = 0;
int mDataOrigPosRankByHyb = 0;
int mDataFoundOrigByHyb = 0;
int mDataNumSampleByHyb = 0;
int mDataNumSampleByMatch = 0;
int mDataHybByMatch = 0;
int mDataUniqueMatchByCoperture = 0;

int mDataFilterSmallNeedle = 0;
int mDataFilterMinNeedleSize = 200;
int mDataFilterBigNeedle = 0;
int mDataFilterMaxNeedleSize = 200;

std::future<int> mDataFuture;
std::mutex matchingDatasMutex;
std::vector<MatchingData> matchingDatas;




int do_check_boxplot(void)
{
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *dataset = NULL;

    if (updating_menu_state != 0)
    {
        active_menu->flags &= ~D_DISABLED;
        return D_O_K;
    }

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2)
    {
        return D_O_K;
    }

    if ((op = create_and_attach_one_plot(pr, 2, 2, 0)) == NULL)
    {
        return D_O_K;
    }

    dataset = op->dat[0];
    alloc_data_set_y_error(dataset);
    alloc_data_set_y_down_error(dataset);
    alloc_data_set_y_box(dataset);

    for (int i = 0; i < 2; ++i)
    {
        dataset->xd[i] = i * 3;
        dataset->yd[i] = i * 3;
        dataset->ye[i] = 4;
        dataset->yed[i] = 3;
        dataset->ybu[i] = 3;
        dataset->ybd[i] = 3;
    }

    op->need_to_refresh = ALL_NEED_REFRESH;
    refresh_plot(pr, pr->n_op - 1);
    return D_O_K;
}


DistOligoSolutionViewer *g_dosv = 0;

int set_dosv(DistOligoSolutionViewer *dosv)
{
    if (g_dosv)
    {
        delete g_dosv;
    }

    g_dosv = dosv;
}

int do_dist_oligo_solution_viewer_open(void)
{
    form_window_t *form = NULL;
    static int ref_idx = 0;
    static int needle_idx = 0;
    int form_ret = 0;
    bool mergeFeature = true;
    pltreg *pr = 0;
    O_p *op = 0;
    DistOligoSearchParams dosp;

    if (updating_menu_state != 0)
    {
        active_menu->flags &= ~D_DISABLED;
        ac_grep(cur_ac_reg, "%pr%op", &pr, &op);

        if (op != NULL && op->user_id == 4343)
        {
            remove_short_cut_from_dialog(0, KEY_O);
            add_keyboard_short_cut(0, KEY_O, 0, do_dist_oligo_solution_viewer_open);
        }

        return D_O_K;
    }

    form = form_create("Display solutions");
    form_push_label(form, "Ref index");
    generate_combo_box(form, &ref_idx, Env::instance().references());
    form_newline(form);
    form_push_label(form, "Needle");
    generate_combo_box(form, &needle_idx, Env::instance().experimentals());
    form_newline(form);
    form_push_label(form, "\t Allow merge distances");
    form_push_bool(form, &dosp.allowGap);
    form_newline(form);
    form_push_label(form, "\t MaxSkip");
    form_push_int(form, &dosp.maxGapSize);
    form_newline(form);
    form_push_label(form, "\t Reject Thres");
    form_push_float(form, 0.1, &dosp.rejectThreshold);
    form_newline(form);
    form_push_label(form, "\t Accept Thres");
    form_push_float(form, 0.1, &dosp.acceptThreshold);
    form_newline(form);
    form_ret = form_run(form);

    if (form_ret != WIN_OK)
    {
        form_free(form);
        return D_O_K;
    }

    form_free(form);
    std::shared_ptr<Fingerprint> needle = Env::instance().experimentals()[needle_idx];
    std::shared_ptr<Fingerprint> haystack = Env::instance().references()[ref_idx];
    DistOligoSequence *needle_dos = new DistOligoSequence(needle, Env::instance().distClasses());
    DistOligoSequence *haystack_dos = new DistOligoSequence(haystack, Env::instance().distClasses());
    DistOligoSequenceFinder dosf = DistOligoSequenceFinder(dosp);
    DistSearchResult result = dosf.find(*haystack_dos, *needle_dos);
    DistOligoSolutionViewer *dosv = new DistOligoSolutionViewer(Env::instance().distClasses(), haystack, needle,
            std::move(result), 2);

    if (g_dosv)
    {
        delete g_dosv;
    }

    g_dosv = dosv;
    return D_O_K;
}

int do_dist_oligo_solution_viewer_next(void)
{
    O_p *op = NULL;
    pltreg *pr = NULL;
    int sol = 0;

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2) // we get the plot region
    {
        return win_printf_OK("cannot find data");
    }

    if (updating_menu_state != 0)
    {
        if (op != NULL && op->user_id == 0xDEE)
        {
            remove_short_cut_from_dialog(0, KEY_RIGHT);
            add_keyboard_short_cut(0, KEY_RIGHT, 0, do_dist_oligo_solution_viewer_next);
        }

        return D_O_K;
    }

    if (op->cur_dat >= 2)
    {
        sol = op->cur_dat - 2;
    }

    g_dosv->nextSolution(sol);
    return D_O_K;
}
int do_dist_oligo_solution_viewer_prev(void)
{
    O_p *op = NULL;
    pltreg *pr = NULL;
    int sol = 0;

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2) // we get the plot region
    {
        return win_printf_OK("cannot find data");
    }

    if (updating_menu_state != 0)
    {
        if (op != NULL && op->user_id == 0xDEE)
        {
            remove_short_cut_from_dialog(0, KEY_LEFT);
            add_keyboard_short_cut(0, KEY_LEFT, 0, do_dist_oligo_solution_viewer_prev);
        }

        return D_O_K;
    }

    if (op->cur_dat >= 2)
    {
        sol = op->cur_dat - 2;
    }

    g_dosv->prevSolution(sol);
    return D_O_K;
}

int do_dist_oligo_solution_viewer_goto_true_positive(void)
{
    O_p *op = NULL;
    pltreg *pr = NULL;
    int sol = 0;

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2) // we get the plot region
    {
        return win_printf_OK("cannot find data");
    }

    if (updating_menu_state != 0)
    {
        if (op != NULL && op->user_id == 4343)
        {
            remove_short_cut_from_dialog(0, KEY_T);
            add_keyboard_short_cut(0, KEY_T, 0, do_dist_oligo_solution_viewer_goto_true_positive);
        }

        return D_O_K;
    }

    if (op->cur_dat >= 2)
    {
        sol = op->cur_dat - 2;
    }

    g_dosv->gotoTruePositiveSolution(sol);
    return D_O_K;
}


int do_fingerprint_solviewer_configure()
{
    static int hayidx = 0;
    static int neeidx = 0;
    static int startPos = 0;
    static float minNmBaseRate = MIN_NM_BASE_RATE;
    static float maxNmBaseRate = MAX_NM_BASE_RATE;
    static int dir = 1;
    static bool onlyMatch = true;
    form_window_t *form = NULL;
    int form_ret = 0;

    if (updating_menu_state != 0)
    {
        if (Env::instance().references().size() > 0 || Env::instance().experimentals().size() > 0)
        {
            active_menu->flags &= ~D_DISABLED;
        }
        else
        {
            active_menu->flags |= D_DISABLED;
        }

        return D_O_K;
    }

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("View a fingerprint");
    }

    form = form_create("Configure fingerprint viewer");
    form_push_label(form, "haystack :");
    generate_combo_box(form, &hayidx, Env::instance().references());
    form_newline(form);
    form_push_label(form, "needle :");
    generate_combo_box(form, &neeidx, Env::instance().experimentals());
    form_newline(form);
    form_push_label(form, "starting at: ");
    form_push_int(form, &startPos);
    form_newline(form);
    form_push_label(form, "min Nm Base Rate: ");
    form_push_float(form, .05, &minNmBaseRate);
    form_push_label(form, "max Nm Base Rate: ");
    form_push_float(form, .05, &maxNmBaseRate);
    form_newline(form);
    form_push_label(form, "direction ( positive or negative ): ");
    form_push_int(form, &dir);
    form_newline(form);
    form_push_label(form, "view only last solutions ");
    form_push_bool(form, &onlyMatch);
    form_ret = form_run(form);

    if (form_ret != WIN_OK)
    {
        form_free(form);
        return D_O_K;
    }

    form_free(form);
    return fingerprint_solviewer_configure(Env::instance().references(), hayidx, Env::instance().experimentals(), neeidx,
                                           lastSolutions, dir, onlyMatch);
}

int do_solviewer_go_next_pos(void)
{
    O_p *op = NULL;
    pltreg *pr = NULL;

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2) // we get the plot region
    {
        return win_printf_OK("cannot find data");
    }

    if (updating_menu_state != 0)
    {
        if (op != NULL && op->user_id == 4242)
        {
            remove_short_cut_from_dialog(0, KEY_RIGHT);
            add_keyboard_short_cut(0, KEY_RIGHT, 0, do_solviewer_go_next_pos);
        }

        return D_O_K;
    }

    fingerprint_solviewer_go_next_pos();
    return D_O_K;
}

int do_solviewer_go_prev_pos(void)
{
    O_p *op = NULL;
    pltreg *pr = NULL;

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2) // we get the plot region
    {
        return win_printf_OK("cannot find data");
    }

    if (updating_menu_state != 0)
    {
        if (op != NULL && op->user_id == 4242)
        {
            remove_short_cut_from_dialog(0, KEY_LEFT);
            add_keyboard_short_cut(0, KEY_LEFT, 0, do_solviewer_go_prev_pos);
        }

        return D_O_K;
    }

    fingerprint_solviewer_go_prev_pos();
    return D_O_K;
}

int do_solviewer_inc_ref_peak(void)
{
    O_p *op = NULL;
    pltreg *pr = NULL;

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2) // we get the plot region
    {
        return win_printf_OK("cannot find data");
    }

    if (updating_menu_state != 0)
    {
        if (op != NULL && op->user_id == 4242)
        {
            remove_short_cut_from_dialog(0, KEY_UP);
            add_keyboard_short_cut(0, KEY_UP, 0, do_solviewer_inc_ref_peak);
        }

        return D_O_K;
    }

    fingerprint_solviewer_inc_ref_peak();
    return D_O_K;
}

int do_solviewer_dec_ref_peak(void)
{
    O_p *op = NULL;
    pltreg *pr = NULL;

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2) // we get the plot region
    {
        return win_printf_OK("cannot find data");
    }

    if (updating_menu_state != 0)
    {
        if (op != NULL && op->user_id == 4242)
        {
            remove_short_cut_from_dialog(0, KEY_DOWN);
            add_keyboard_short_cut(0, KEY_DOWN, 0, do_solviewer_dec_ref_peak);
        }

        return D_O_K;
    }

    fingerprint_solviewer_dec_ref_peak();
    return D_O_K;
}

int do_generate_pseudo_experimental_fingerprint(void)
{
    int allegro_win_return = 0;
    static int allHaystack = 1;
    static int fgp_idx = 0;
    static int fgpSize = 3000;
    static int fgpstddevSize = 1500;
    static int numSample = 1;
    static float noiseDeviation = 3.;
    static float hybProba = 0.2;
    static float exptCycle = 100;
    static float detectionThres = 0;
    static float avgAddedHyb = 0;
    static float reverseProb = 0;
    static int minHyb = 0;
    static float avgBaseNmRate = AVG_BASE_NM_RATE;
    static float stddevBaseNmRate = STDDEV_BASE_NM_RATE;
    static int saveToFile = 0;
    static int loadNeedle = 1;
    static int isFixedLocus = 0;
    static int randomSeed = -1;
    static int locus = 0;
    std::shared_ptr<Fingerprint> resFgp = 0;
    static char path[1024] = {0};
    FingerprintVector samples;
    std::uniform_int_distribution<int> fgpDistribution;
    FingerprintSampleGenerator fgpsg;
    FingerprintVector ref;
    FingerprintVector& haystacks = Env::instance().references();
    int retrieve = 0;

    if (updating_menu_state != 0)
    {
        if (Env::instance().references().size() > 0 || Env::instance().experimentals().size() > 0)
        {
            active_menu->flags &= ~D_DISABLED;
            remove_short_cut_from_dialog(0, KEY_G);
            add_keyboard_short_cut(0, KEY_G, 0, do_generate_pseudo_experimental_fingerprint);
        }
        else
        {
            active_menu->flags |= D_DISABLED;
        }

        return D_O_K;
    }

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("Create an random subsequence of a barcode");
    }

    allegro_win_return = win_scanf("all haystack %b or index: %5d\n"
                                   "number of samples %5d\n"
                                   "Size average %5d"
                                   "Size deviation %5d\n"
                                   "Noise deviation %5f\n"
                                   "Base -> nm rate Average %5f"
                                   "Base -> nm rate Deviation %5f\n"
                                   "Hybridization Probability %5f\n"
                                   "Number of cycle %5f\n"
                                   "Hybridization detection threshold %5f\n"
                                   "Added hybridization Average %5f\n"
                                   "Reverse probability: %5f\n"
                                   "use a specified locus %b locus: %5d\n"
                                   "min hybridation count %5d\n"
                                   "random seed (-1 for auto) %5d\n"
                                   "save in file %b"
                                   "Load needle %b",
                                   &allHaystack,
                                   &fgp_idx,
                                   &numSample,
                                   &fgpSize,
                                   &fgpstddevSize,
                                   &noiseDeviation,
                                   &avgBaseNmRate,
                                   &stddevBaseNmRate,
                                   &hybProba,
                                   &exptCycle,
                                   &detectionThres,
                                   &avgAddedHyb,
                                   &reverseProb,
                                   &isFixedLocus,
                                   &locus,
                                   &minHyb,
                                   &randomSeed,
                                   &saveToFile,
                                   &loadNeedle);

    if (allegro_win_return == WIN_CANCEL)
    {
        return OFF;
    }

    if (!isFixedLocus)
    {
        locus = -1;
    }

    if (!allHaystack)
    {
        ref = FingerprintVector();
        ref.push_back(Env::instance().references()[fgp_idx]);
        haystacks = ref;
        fgp_idx = 0;
    }

    fgpsg.references(haystacks);
    fgpsg.sizeDistribution(fgpSize, fgpstddevSize);
    fgpsg.noiseDistribution(0, noiseDeviation);
    fgpsg.baseNmRateDistribution(avgBaseNmRate, stddevBaseNmRate);
    fgpsg.params().missingHybDistribution = MissingHybDistribution(exptCycle, hybProba);
    fgpsg.params().missingHybDetectionThreshold = detectionThres;
    fgpsg.addedHybDistribution(avgAddedHyb);
    fgpsg.directionDistribution(1 - reverseProb);
    fgpsg.minHybridization(minHyb);

    if (randomSeed >= 0)
    {
        fgpsg.randomSeed(randomSeed);
    }

    if (numSample > 1)
    {
        samples = fgpsg.generate(numSample);
    }
    else
    {
        fgpDistribution = std::uniform_int_distribution<int> (0, haystacks.size() - 1);
        samples = FingerprintVector();
        LocusDistribution locusDistribution(0, haystacks[fgp_idx]->size() - 1);
        SizeDistribution sizeDistribution = fgpsg.sizeDistribution();
        DirectionDistribution directionDistribution = DirectionDistribution(1. - fgpsg.probReverse());
        UniformRandomEngine randomEngine = UniformRandomEngine(randomSeed);
        BaseNmRateDistribution baseNmRateDistribution = fgpsg.baseNmRateDistribution();

        do
        {
            if (!isFixedLocus)
            {
                locus = locusDistribution(randomEngine);
            }

            int size = sizeDistribution(randomEngine);
            bool isReverse = !directionDistribution(randomEngine);
            float baseNmRate = baseNmRateDistribution(randomEngine);
            resFgp = fgpsg.generateOne(*haystacks[fgp_idx], locus, size, isReverse, baseNmRate);
            retrieve++;
        }
        while (retrieve < 1 && (!resFgp || resFgp->nbHybridation() < minHyb));

        if (resFgp)
        {
            samples.push_back(resFgp);
        }
    }

    if (saveToFile)
    {
        char *file = save_one_file_config("Save pseudo experimental Env::instance().references()", path, NULL,
                                          "Simple Fingerprint\0*.sfgp\0All\0*.*\0", "SFGP-FILE", "last_saved");
        std::ofstream ofs;
        ofs.open(file, std::ofstream::out);

        for (int i = 0; i < (int) samples.size(); ++i)
        {
            (samples)[i]->dumpSfgp(ofs) << std::endl;
        }

        ofs.close();
    }

    // Fingerprint* resFgpOrig = genExpBarcodeFromFgp(fgp, locus, fgpSize, isReversed, 0, 1);
    // ofs.open(outputFileOrig, std::ofstream::out);
    // resFgpOrig->dumpSfgp(ofs) << std::endl;
    // ofs.close();

    if (loadNeedle)
    {
        Env::instance().experimentals().insert(Env::instance().experimentals().end(), samples.begin(), samples.end());
        // Env::instance().experimentals().push_back(resFgpOrig);
        do_update_menu();
    }

    return D_O_K;
}




std::ostream& print_search_match(
    std::ostream& os, DistFingerprint& haystack, DistFingerprint& needle, SearchMatch& searchMatch)
{
    os << "In sequence: " << haystack.description() << std::endl
       << "Found: " << needle.description() << std::endl
       << "needle Hybridation Count: " << needle.nbHybridation() << std::endl
       << "haystack Hybridation Count: "
       << std::distance(haystack.hybridationMap().lower_bound(searchMatch.firstReferencePosition()),
                        haystack.hybridationMap().upper_bound(searchMatch.lastReferencePosition()))
       << std::endl
       << "Solution: " << searchMatch << std::endl;
    return os;
}


void draw_search_match(
    d_s *dataset, DistFingerprint& haystack, DistFingerprint& needle, SearchMatch& searchMatch)
{
    int curPoint = 0;
    int dsSize = searchMatch.haystackNeedleMatchMap().size();
    dataset = build_adjust_data_set(dataset, dsSize, dsSize);
    dataset->nx = dsSize;
    dataset->ny = dsSize;

    for (auto corresp : searchMatch.haystackNeedleMatchMap())
    {
        dataset->xd[curPoint] = needle.stretchedPosition(
                                    corresp.second - searchMatch.originNeedlePosition(), searchMatch.nmBaseRate());
        dataset->yd[curPoint] = corresp.first - searchMatch.firstReferencePosition();
        curPoint++;
    }

    std::ostringstream ss;
    print_search_match(ss, haystack, needle, searchMatch);
    std::cout << std::endl
              << ss.str();
    dataset->source = strdup(ss.str().c_str());
}

void draw_im_search_match(
    DistFingerprint& haystack, DistFingerprint& needle, SearchMatch& searchMatch)
{
    O_i *oi = 0;
    imreg *imr = 0;
    int width = needle.stretchedPosition(needle.lastHyb(), searchMatch.nmBaseRate());
    int height = width;
    imr = create_and_register_new_image_project(0, 32, 900, 668);

    if (imr == NULL)
    {
        return;
    }

    oi = create_and_attach_oi_to_imr(imr, width, height, IS_RGB_PICTURE);
    create_attach_select_x_un_to_oi(oi, IS_METER, 0, 1 / searchMatch.nmBaseRate(), -9, 0, "Nm");
    remove_from_image(imr, imr->o_i[0]->im.data_type, (void *)imr->o_i[0]);
    select_image_of_imreg(imr, imr->n_oi - 1);

    for (int i = 0; i < width; ++i)
    {
        for (int j = 0; j < height; ++j)
        {
            oi->im.pixel[j].rgb[i].r = 255;
            oi->im.pixel[j].rgb[i].g = 255;
            oi->im.pixel[j].rgb[i].b = 255;
        }
    }

    int lastPos = 0;

    for (auto hyb : needle.hybridationMap())
    {
        int oligoKey = hyb.second.theoricalOligo().get_key();
        int oligoSize = hyb.second.theoricalOligo().get().oligo().size();
        int j = hyb.first;

        for (j = (hyb.first > lastPos) ? hyb.first : lastPos;
                j - hyb.first < oligoSize && j < width; ++j)
        {
            for (int i = 0; i < height; ++i)
            {
                rgb_t color = { (unsigned char)EXTRACT_R(colors[oligoKey]),
                                (unsigned char)EXTRACT_G(colors[oligoKey]),
                                (unsigned char)EXTRACT_B(colors[oligoKey])
                              };
                oi->im.pixel[i].rgb[j].r -= color.r;
                oi->im.pixel[i].rgb[j].g -= color.g;
                oi->im.pixel[i].rgb[j].b -= color.b;
                // std::cout << "pixel-- " << "y: " << i << "x: " << hyb.first + j << std::endl;
            }
        }

        lastPos = j;
    }

    lastPos = 0;
    HybridationMap::const_iterator begin
        = haystack.hybridationMap().lower_bound(searchMatch.firstReferencePosition());
    HybridationMap::const_iterator end
        = haystack.hybridationMap().upper_bound(searchMatch.firstReferencePosition() + width);

    for (HybridationMap::const_iterator hyb = begin; hyb != end; ++hyb)
    {
        int oligoKey = hyb->second.theoricalOligo().get_key();
        int oligoSize = hyb->second.theoricalOligo().get().oligo().size();
        int hybPos = hyb->first - searchMatch.firstReferencePosition();
        int j = hybPos;

        for (j = (hybPos > lastPos) ? hybPos : lastPos; j - hybPos < oligoSize && j < height; ++j)
        {
            for (int i = 0; i < width; ++i)
            {
                rgb_t color = { (unsigned char)(254 - EXTRACT_R(colors[oligoKey])),
                                (unsigned char)(254 - EXTRACT_G(colors[oligoKey])),
                                (unsigned char)(254 - EXTRACT_B(colors[oligoKey]))
                              };
                //                std::cout <<
                //                    (int)oi->im.pixel[j].rgb[i].r  << " + " << (int)color.r <<
                //                    std::endl <<
                //                    (int)oi->im.pixel[j].rgb[i].g  << " + " << (int)color.g<<
                //                    std::endl <<
                //                    (int)oi->im.pixel[j].rgb[i].b  << " + " << (int)color.b<<
                //                    std::endl<< std::endl;
                oi->im.pixel[j].rgb[i].r -= color.r;
                oi->im.pixel[j].rgb[i].g -= color.g;
                oi->im.pixel[j].rgb[i].b -= color.b;
                //                std::cout <<
                //                    (int)oi->im.pixel[j].rgb[i].r  << std::endl <<
                //                    (int)oi->im.pixel[j].rgb[i].g  << std::endl <<
                //                    (int)oi->im.pixel[j].rgb[i].b  << std::endl<< std::endl;
                // std::cout << "pixel-- " << "y: " << i << "x: " << hyb.first + j << std::endl;
            }
        }

        lastPos = j;
    }

    oi->need_to_refresh = ALL_NEED_REFRESH;
    refresh_image(imr, imr->n_oi - 1);
}
int do_fingerprint_search_rank(void)
{
    static int haystack_idx = 0;
    static int needle_idx = 0;
    // static int start = 0;
    static int all_haystack = 1;
    static int all_needle = 1;
    static float maxError = MAX_HYBRIDATION_ERROR;
    static int noiseDeviation = NOISE_DEVIATION;
    static float minNmBaseRate = MIN_NM_BASE_RATE;
    static float maxNmBaseRate = MAX_NM_BASE_RATE;
    static int iteration = RANKING_ITERATION;
    static int min_correl_peak = MIN_CORRELATED_PEAK;
    char *title = "Search using ranking";
    // bool shutdown = false;
    int allegro_win_return = 0;

    if (updating_menu_state != 0)
    {
        if (Env::instance().references().size() > 0 && Env::instance().experimentals().size() > 0)
        {
            active_menu->flags &= ~D_DISABLED;
        }
        else
        {
            active_menu->flags |= D_DISABLED;
        }

        return D_O_K;
    }

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine try to match a fingerprint into another");
    }

    allegro_win_return = win_scanf("%t"
                                   "All haystack %b \n or"
                                   "Index of the haystack fingerprint %d\n"
                                   "All Env::instance().experimentals() %b \n or"
                                   "Index of the needle fingerprint %d\n"
                                   "max error [XXXX]\n" //%f"
                                   "minimum correlatedPeak %d\n"
                                   "nb iterations %d\n"
                                   "noise deviation %d\n"
                                   "min nm -> base rate %f\n"
                                   "max nm -> base rate %f\n",
                                   //"Draw results %b\n",
                                   title, &all_haystack, &haystack_idx, &all_needle, &needle_idx, &min_correl_peak, &iteration,
                                   //&maxError,
                                   &noiseDeviation, &minNmBaseRate, &maxNmBaseRate
                                   //&searchOneOligoAtTime,
                                   //&skipMissingOligoInHaystack,
                                   //&skipMissingOligoInNeedle,
                                   //&draw_result
                                  );

    if (allegro_win_return == WIN_CANCEL)
    {
        return OFF;
    }

    if (haystack_idx < 0 || haystack_idx >= (int)Env::instance().references().size() || needle_idx < 0
            || needle_idx >= (int)Env::instance().experimentals().size())
    {
        win_printf("invalid index choose");
        return OFF;
    }

    RankFinder rfinder;
    rfinder.maxSearchHybridationError(maxError);
    rfinder.noiseDeviation(noiseDeviation);
    rfinder.minNmBaseRate(minNmBaseRate);
    rfinder.maxNmBaseRate(maxNmBaseRate);
    rfinder.rankingIteration(iteration);
    rfinder.minCorrelatedPeak(min_correl_peak);
    int size = all_haystack ? Env::instance().references().size() : haystack_idx + 1;
    int needles_size = all_needle ? Env::instance().experimentals().size() : needle_idx + 1;
    #pragma omp parallel for

    for (int i = all_haystack ? 0 : haystack_idx; i < size; ++i)
    {
        RankFinder localFinder = rfinder;
        std::shared_ptr<PosFingerprint> curHaystack = std::dynamic_pointer_cast<PosFingerprint>
                (Env::instance().references()[i]);

        if (curHaystack == 0)
        {
            warning_message("Invalid haystack format");
            continue;
        }

        BasicSearchResult matchSet;

        for (int j = all_needle ? 0 : needle_idx; j < needles_size; ++j)
        {
            std::shared_ptr<PosFingerprint> needle = std::dynamic_pointer_cast<PosFingerprint>(Env::instance().experimentals()[j]);

            if (needle == 0)
            {
                warning_message("Invalid needle format");
                continue;
            }

            rfinder.buildRankVector(*curHaystack, *needle);
        }
    }

    return D_O_K;
}

int do_fingerprint_search(void)
{
    static int haystack_idx = 0;
    static int needle_idx = 0;
    // static int start = 0;
    static int all_haystack = 1;
    static float maxError = MAX_HYBRIDATION_ERROR;
    static int noiseDeviation = NOISE_DEVIATION;
    static float minNmBaseRate = MIN_NM_BASE_RATE;
    static float maxNmBaseRate = MAX_NM_BASE_RATE;
    static int skipMissingOligoInHaystack = SKIP_MISSING_IN_HAYSTACK;
    static int skipMissingOligoInNeedle = SKIP_MISSING_IN_NEEDLE;
    static int draw_result = 1;
    static int searchOneOligoAtTime = 0;
    bool isFound = false;
    // bool shutdown = false;
    pltreg *pr = 0;
    O_p *op = 0;
    int allegro_win_return = 0;

    if (updating_menu_state != 0)
    {
        if (Env::instance().references().size() > 0 && Env::instance().experimentals().size() > 0)
        {
            active_menu->flags &= ~D_DISABLED;
        }
        else
        {
            active_menu->flags |= D_DISABLED;
        }

        return D_O_K;
    }

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine try to match a fingerprint into another");
    }

    allegro_win_return = win_scanf("All haystack %b \n or"
                                   "Index of the haystack fingerprint %d\n"
                                   "Index of the needle fingerprint %d\n"
                                   "max error %f\n"
                                   "noise deviation %d\n"
                                   "min nm -> base rate %f\n"
                                   "max nm -> base rate %f\n"
                                   "search one oligo at the time %b\n"
                                   "skip Missing Oligo In Haystack %b \n"
                                   "skip Missing Oligo In Needle %b \n"
                                   "Draw results %b\n",
                                   &all_haystack, &haystack_idx, &needle_idx, &maxError, &noiseDeviation, &minNmBaseRate,
                                   &maxNmBaseRate, &searchOneOligoAtTime, &skipMissingOligoInHaystack,
                                   &skipMissingOligoInNeedle, &draw_result);

    if (allegro_win_return == WIN_CANCEL)
    {
        return OFF;
    }

    if (!(haystack_idx >= 0 && haystack_idx < (int)Env::instance().references().size() && needle_idx >= 0
            && needle_idx < (int)Env::instance().experimentals().size()))
    {
        win_printf("invalid index choose");
        return OFF;
    }

    if (draw_result)
    {
        if (ac_grep(cur_ac_reg, "%pr", &pr) != 1) // we get the plot region
        {
            return win_printf_OK("cannot find data");
        }

        if ((op = create_and_attach_one_plot(pr, 10, 10, 0)) == NULL)
        {
            return win_printf_OK("cannot create plot!");
        }

        set_plot_y_title(op, "reference barcode");
        set_plot_x_title(op, "solution barcode");
    }

    auto needle = std::dynamic_pointer_cast<DistFingerprint>(Env::instance().experimentals()[needle_idx]);
    BasicFinder ffinder;
    ffinder.maxSearchHybridationError(maxError);
    ffinder.noiseDeviation(noiseDeviation);
    ffinder.minNmBaseRate(minNmBaseRate);
    ffinder.maxNmBaseRate(maxNmBaseRate);
    ffinder.skipMissingOligoInHaystack(skipMissingOligoInHaystack);
    ffinder.skipMissingOligoInNeedle(skipMissingOligoInNeedle);
    int size = all_haystack ? Env::instance().references().size() : haystack_idx + 1;
    #pragma omp parallel for

    for (int i = all_haystack ? 0 : haystack_idx; i < size; ++i)
    {
        BasicFinder localFinder = ffinder;
        auto curHaystack = std::dynamic_pointer_cast<DistFingerprint>(Env::instance().references()[i]);
        BasicSearchResult matchSet;

        if (searchOneOligoAtTime)
        {
            matchSet = localFinder.findAllByOligo(*curHaystack, *needle);
        }
        else
        {
            matchSet = localFinder.findAll(*curHaystack, *needle);
        }

        if (!matchSet.solutions().empty())
        {
            isFound = true;
        }

        lastSolutions.clear();

        for (SearchMatch ms : matchSet.solutions())
        {
            ms.chromosome(i);
            ms.needle(needle_idx);
            lastSolutions.push_back(ms);
            print_search_match(
                std::cout, *curHaystack, ms.reversed() ? * (needle->reverse()) : *needle, ms);
            //    #pragma omp critical
            //    {
            //        if (cur_ds_idx > 0)
            //        {
            //            if ((create_and_attach_one_ds(op, 10, 10 , 0)) == NULL)
            //            {
            //                std::cerr << __FILE__ << ":" << __LINE__ << " Fatal error, create one
            //                des failed." << std::endl;
            //                throw std::exception();
            //            }
            //        }
            //        d_s *dataset = op->dat[cur_ds_idx];
            //        draw_search_match(dataset, *curHaystack, ms.reversed() ? * (needle->reverse())
            //        : *needle, ms);
            //        draw_im_search_match(*curHaystack, ms.reversed() ? * (needle->reverse()) :
            //        *needle, ms);
            //        op->need_to_refresh = 1;
            //        refresh_plot(pr, UNCHANGED);
            //        cur_ds_idx++;
            //    }
        }

        std::cout << "search finished for: " << curHaystack->description() << std::endl;
    }

    if (!isFound)
    {
        win_printf_OK("needle %i not found\n", needle_idx);
    }

    return D_O_K;
}

int do_fingerprint_dist_oligo_sequence_search(void)
{
    form_window_t *form = NULL;
    static int hayIdx = 0;
    static int neeIdx = 0;
    int form_ret = 0;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    DistOligoSearchParams dosp;

    if (updating_menu_state != 0)
    {
        if (Env::instance().referenceDsos().size() > 0 && Env::instance().experimentalDsos().size() > 0)
        {
            active_menu->flags &= ~D_DISABLED;
        }
        else
        {
            active_menu->flags |= D_DISABLED;
        }

        return D_O_K;
    }

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine try to match a  dist oligo sequence into another");
    }

    form = form_create("Search using distance oligo sequence");
    form_push_label(form, "haystack :");
    generate_dos_combo_box(form, &hayIdx, Env::instance().referenceDsos());
    form_newline(form);
    form_push_label(form, "needle :");
    generate_dos_combo_box(form, &neeIdx, Env::instance().experimentalDsos());
    form_newline(form);
    form_push_label(form, "\t Allow merge distances");
    form_push_bool(form, &dosp.allowGap);
    form_newline(form);
    form_push_label(form, "\t MaxSkip");
    form_push_int(form, &dosp.maxGapSize);
    form_newline(form);
    form_push_label(form, "\t Reject Thres");
    form_push_float(form, 0.1, &dosp.rejectThreshold);
    form_newline(form);
    form_push_label(form, "\t Accept Thres");
    form_push_float(form, 0.1, &dosp.acceptThreshold);
    form_newline(form);
    form_ret = form_run(form);

    if (form_ret != WIN_OK)
    {
        form_free(form);
        return D_O_K;
    }

    form_free(form);
    DistOligoSequenceFinder dosf = DistOligoSequenceFinder(dosp);
    DistSearchResult result = dosf.find(*Env::instance().referenceDsos()[hayIdx],
                                        *Env::instance().experimentalDsos()[neeIdx]);
    Env::instance().referenceDsos()[hayIdx]->dump(std::cout);
    Env::instance().experimentalDsos()[neeIdx]->dump(std::cout);

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2)
    {
        return D_O_K;
    }

    const DistOligoSolutions& solutions = result.solutions();

    if (solutions.size() > 0)
    {
        op = create_and_attach_one_plot(pr, solutions.size() * 2, solutions.size() * 2, 0);
        ds = op->dat[0];
        int solutionCount = solutions.size();

        for (int i = 0; i < solutionCount; ++i)
        {
            const DistOligoSolution& dos = solutions[i];
            ds->xd[i * 2] = dos.firstReferencePosition();
            ds->yd[i * 2] = 0;
            ds->xd[i * 2 + 1] = dos.firstReferencePosition();
            ds->yd[i * 2 + 1] = dos.score();
        }
    }
    else
    {
        win_printf("sequence not find");
    }

    op->need_to_refresh = ALL_NEED_REFRESH;
    set_dash_line(ds);
    refresh_plot(pr, pr->n_op - 1);
    return D_O_K;
}

int do_get_dist_oligo_sequence_from_fingerprints(void)
{
    get_dist_oligo_sequence_from_fingerprints(FGP_LIST_HAYSTACK, -1);
    return D_O_K;
}

/**
 * @brief
 *
 * @param fgplist
 * @param index index of the fgp, -1 for all
 *
 * @return
 */
int get_dist_oligo_sequence_from_fingerprints(FgpList fgplist, int index)
{
    int i = 0;
    int lend = 0;
    FingerprintVector *fgpVector = &Env::instance().references();
    std::vector<std::shared_ptr<DistOligoSequence >> *dosVector = &Env::instance().referenceDsos();

    switch (fgplist)
    {
    case FGP_LIST_HAYSTACK:
        fgpVector = &Env::instance().references();
        dosVector = &Env::instance().referenceDsos();
        break;

    case FGP_LIST_NEEDLE:
        fgpVector = &Env::instance().experimentals();
        dosVector = &Env::instance().experimentalDsos();
        break;
    }

    if (index != -1)
    {
        i = index;
        lend = index + 1;
    }
    else
    {
        lend = fgpVector->size();
    }

    for (; i < lend; ++i)
    {
        dosVector->push_back(std::shared_ptr<DistOligoSequence>(new DistOligoSequence(((*fgpVector)[i]),
                             Env::instance().distClasses())));
    }

    do_update_menu();
    return D_O_K;
}
int do_export_last_solutions(void)
{
    static int allegro_win_return = D_O_K;
    static int offsetBefore = 1000;
    static int offsetAfter = 1000;
    static int format = 1;
    static char filePrefix[1024] = "solution";
    int nb_sequences = -1;
    sqd **sequences = NULL;
    get_loaded_sequences(&sequences, &nb_sequences);

    if (updating_menu_state != 0)
    {
        if (lastSolutions.size() > 0 && nb_sequences > 0)
        {
            active_menu->flags &= ~D_DISABLED;
        }
        else
        {
            active_menu->flags |= D_DISABLED;
        }

        return D_O_K;
    }

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This function export a solution into a file.");
    }

    allegro_win_return = win_scanf("offset: before %5d - after %5d\n"
                                   "%R fasta file\n"
                                   "%r viewer file\n"
                                   "file prefix %s",
                                   &offsetBefore, &offsetAfter, &format, &filePrefix);

    if (allegro_win_return == WIN_CANCEL)
    {
        return OFF;
    }

    int fileNum = 0;

    if (format == 0) // FASTA file
    {
        for (auto solution : lastSolutions)
        {
            std::stringstream ss;
            std::ofstream ofs;
            ss << filePrefix << fileNum << ".fasta";
            ofs.open(ss.str());
            ofs << Env::instance().references()[solution.chromosome()]->description() << "|"
                << Env::instance().experimentals()[solution.needle()]->description()
                << " position: " << solution.firstReferencePosition() << "-"
                << solution.lastReferencePosition() << "reverted: " << solution.reversed()
                << " error: " << solution.error() << " chiSquare: " << solution.chiSquare()
                << std::endl;

            for (int i = 0; i < nb_sequences; ++i)
            {
                sqd *seq = sequences[i];

                if (std::string(seq->description)
                        .find(Env::instance().references()[solution.chromosome()]->description())
                        != std::string::npos)
                {
                    int start = solution.firstReferencePosition() - offsetBefore;
                    int end = solution.lastReferencePosition() + offsetAfter;
                    start = start >= 0 ? start : 0;
                    end = end <= seq->totalSize ? end : seq->totalSize;

                    for (int j = start; j < end; ++j)
                    {
                        if ((j - start) % 80 == 0)
                        {
                            ofs << std::endl;
                        }

                        ofs << seq->seq[j / seq->pageSize][j % seq->pageSize];
                    }

                    break;
                }
            }

            ofs.close();
            ++fileNum;
        }
    }
    else
    {
        for (auto solution : lastSolutions)
        {
            std::stringstream ss;
            std::ofstream ofs;
            ss << filePrefix << fileNum << ".solview";
            ofs.open(ss.str());
            ofs << Env::instance().references()[solution.chromosome()]->description() << std::endl
                << Env::instance().experimentals()[solution.needle()]->description() << std::endl
                << solution;
            int i = 0;

            for (; i < nb_sequences; ++i)
            {
                sqd *seq = sequences[i];

                if (std::string(seq->description)
                        .find(Env::instance().references()[solution.chromosome()]->description())
                        != std::string::npos)
                {
                    int start = solution.firstReferencePosition() - offsetBefore;
                    int end = solution.lastReferencePosition() + offsetAfter;
                    std::stringstream seqline;
                    std::stringstream cursorline;
                    start = start >= 0 ? start : 0;
                    end = end <= seq->totalSize ? end : seq->totalSize;
                    int j = start;

                    for (; j < end; ++j)
                    {
                        if ((j - start) % 80 == 0)
                        {
                            ofs << seqline.str() << " [" << j << "]" << std::endl;
                            seqline.str("");
                            ofs << cursorline.str() << std::endl;
                            cursorline.str("");
                        }

                        seqline << seq->seq[j / seq->pageSize][j % seq->pageSize];

                        if (solution.haystackNeedleMatchMap().find(j)
                                != solution.haystackNeedleMatchMap().end())
                        {
                            cursorline << "^";
                        }
                        else
                        {
                            cursorline << " ";
                        }
                    }

                    ofs << seqline.str() << " [" << j << "]" << std::endl;
                    seqline.str("");
                    ofs << cursorline.str() << std::endl;
                    cursorline.str("");
                    break;
                }
            }

            if (i == nb_sequences)
            {
                win_printf_OK("no sequence available");
            }

            ofs.close();
            ++fileNum;
        }
    }

    return D_O_K;
}
int do_fingerprint_multiple_match(void)
{
    int nb_match = 0;
    int **clusters = NULL;
    int *clusters_size = NULL;
    int nb_clusters = 0;
    static int group_sequences = 1;
    int allegro_win_return = 0;
    BasicFinder ffinder;

    if (updating_menu_state != 0)
    {
        if (Env::instance().references().size() > 0)
        {
            active_menu->flags &= ~D_DISABLED;
        }
        else
        {
            active_menu->flags |= D_DISABLED;
        }

        return D_O_K;
    }

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine try to match a fingerprint into another");
    }

    allegro_win_return = win_scanf("group matching sequences %b", &group_sequences);

    if (allegro_win_return == WIN_CANCEL)
    {
        return OFF;
    }

    FingerprintVector fingerprints = Env::instance().references();
    #pragma omp parallel for shared(fingerprints) reduction(+ : nb_match)

    for (int i = 0; i < (int)fingerprints.size(); ++i)
    {
        for (int j = i + 1; j < (int)fingerprints.size(); ++j)
        {
            // std::cout << i << " " << j << std::endl;
            BasicFinder localFinder = ffinder;
            SearchMatch *fingerprintMatch = 0;

            if (localFinder.isFingerprintSignificant(*fingerprints[i])
                    && localFinder.isFingerprintSignificant(*fingerprints[j]))
            {
                fingerprintMatch
                    = localFinder.find(*(std::dynamic_pointer_cast<DistFingerprint>(fingerprints[i])),
                                       *(std::dynamic_pointer_cast<DistFingerprint>(fingerprints[j])), 0);
            }
            else
            {
                std::cout << i << "+" << j << "not significant" << std::endl;
            }

            if (fingerprintMatch)
            {
                nb_match++;

                if (group_sequences)
                {
                    #pragma omp critical
                    {
                        cluster_fingerprint(i, j, &clusters, &nb_clusters, &clusters_size);
                    }
                }
            }

            delete fingerprintMatch;
            fingerprintMatch = 0;
        }

        cluster_alone_fingerprint(i, &clusters, &nb_clusters, &clusters_size);
    }

    win_printf("number of fingerprint matching : %d\n", nb_match);
    win_printf("clusters :%d", nb_clusters);
    dump_clusters(stdout, Env::instance().references(), clusters, nb_clusters, clusters_size);
    return D_O_K;
}

int do_fingerprint_coperture(void)
{
    int hybridationCount = 0;
    int seq_id = 0;
    double coperture = 0.;
    bool computeAll = true;
    int allegro_win_return = 0;

    if (updating_menu_state != 0)
    {
        if (Env::instance().references().size() > 0 || Env::instance().experimentals().size() > 0)
        {
            active_menu->flags &= ~D_DISABLED;
        }
        else
        {
            active_menu->flags |= D_DISABLED;
        }

        return D_O_K;
    }

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine calculate the coperture of a fingerprint");
    }

    allegro_win_return
        = win_scanf("fingerprint id %d or all Env::instance().references() %b", &seq_id, &computeAll);

    if (allegro_win_return == WIN_CANCEL)
    {
        return OFF;
    }

    if (!(seq_id >= 0 && seq_id < (int)Env::instance().references().size()))
    {
        win_printf("invalid index choose");
        return OFF;
    }

    if (computeAll)
    {
        for (auto fingerprint : Env::instance().references())
        {
            coperture += fingerprint->coperture();
            hybridationCount += fingerprint->nbHybridation();
        }

        coperture /= Env::instance().references().size();
    }
    else
    {
        coperture = Env::instance().references()[seq_id]->coperture();
        hybridationCount = Env::instance().references()[seq_id]->nbHybridation();
    }

    win_printf_OK("coperture: %f\nhybridations: %d", coperture, hybridationCount);
    return D_O_K;
}



int plot_matchingDatas(std::vector<MatchingData>& datas, MatchingData::GetterFunc xgetter,
                       MatchingData::GetterFunc ygetter, d_s *dataset, d_s *count_dataset)
{
    std::map<int, int> indexMap;
    assert(dataset);
    matchingDatasMutex.lock();

    for (auto matchingData : datas)
    {
        if (!mDataFilterSmallNeedle || matchingData.needleSize() > mDataFilterMinNeedleSize)
        {
            int idx = std::round((matchingData.*xgetter)());
            indexMap[idx] = 0;
        }
    }

    matchingDatasMutex.unlock();

    if (indexMap.size() == 0)
    {
        return D_O_K;
    }

    int cur_idx = 0;

    for (std::map<int, int>::const_iterator it = indexMap.begin(); it != indexMap.end(); ++it)
    {
        indexMap[it->first] = cur_idx;
        cur_idx++;
    }

    dataset = build_adjust_data_set(dataset, cur_idx, cur_idx);

    if (count_dataset)
    {
        count_dataset = build_adjust_data_set(count_dataset, cur_idx, cur_idx);
    }

    for (int i = 0; i < cur_idx; ++i)
    {
        dataset->xd[i] = i;
        dataset->yd[i] = 0;

        if (dataset->ye != NULL)
        {
            dataset->ye[i] = 0;
        }

        if (count_dataset)
        {
            count_dataset->xd[i] = i;
            count_dataset->yd[i] = 0;
        }
    }

    std::vector<int> squareValues = std::vector<int>(cur_idx, 0);
    std::vector<int> nbDataByStep = std::vector<int>(cur_idx, 0);
    matchingDatasMutex.lock();

    for (auto matchingData : datas)
    {
        if ((!mDataFilterSmallNeedle || matchingData.needleSize() > mDataFilterMinNeedleSize)
                && (!mDataFilterBigNeedle || matchingData.needleSize() < mDataFilterMaxNeedleSize))
        {
            int curPoint = std::round(indexMap[(matchingData.*xgetter)()]);
            dataset->xd[curPoint] = (matchingData.*xgetter)();
            dataset->yd[curPoint] += (matchingData.*ygetter)();

            if (dataset->ye != NULL)
            {
                squareValues[curPoint] += (matchingData.*ygetter)() * (matchingData.*ygetter)();
            }

            if (count_dataset)
            {
                count_dataset->xd[curPoint] = (matchingData.*xgetter)();
            }

            nbDataByStep[curPoint]++;
        }
    }

    matchingDatasMutex.unlock();

    for (int i = 0; i < cur_idx; ++i)
    {
        if (nbDataByStep[i] != 0)
        {
            // std::cout << i;
            dataset->yd[i] /= nbDataByStep[i];

            if (dataset->ye != NULL)
            {
                dataset->ye[i] = std::sqrt(
                                     squareValues[i] / nbDataByStep[i] - (dataset->yd[i] * dataset->yd[i]));
            }

            if (count_dataset)
            {
                count_dataset->yd[i] = nbDataByStep[i];
            }
        }
    }

    dataset->nx = cur_idx;
    dataset->ny = cur_idx;

    if (count_dataset)
    {
        count_dataset->nx = cur_idx;
        count_dataset->ny = cur_idx;
    }

    return D_O_K;
}

int mDataMultiplePlot(O_p *op, std::vector<MatchingData>& mds)
{
    int cur_ds_idx = 0;
    d_s *numSampleBySizeDs = 0;
    d_s *numSampleByHybDs = 0;
    d_s *numSampleByMatchDs = 0;

    if (mDataNumSampleByHyb)
    {
        numSampleByHybDs = op->dat[cur_ds_idx];
        numSampleByHybDs->source = strdup("sample count by hybridation count");
        cur_ds_idx++;
    }

    if (mDataNumSampleBySize)
    {
        numSampleBySizeDs = op->dat[cur_ds_idx];
        numSampleBySizeDs->source = strdup("sample count by needle size");
        cur_ds_idx++;
    }

    if (mDataNumSampleByMatch)
    {
        numSampleByMatchDs = op->dat[cur_ds_idx];
        numSampleByMatchDs->source = strdup("sample count by match size");
        cur_ds_idx++;
    }

    if (mDataMatchBySize)
    {
        plot_matchingDatas(mds, &MatchingData::needleSize, &MatchingData::matchCount,
                           op->dat[cur_ds_idx], numSampleBySizeDs);
        op->dat[cur_ds_idx]->source = strdup("Matching count by needle size");
        numSampleBySizeDs = 0;
        cur_ds_idx++;
    }

    if (mDataHybBySize)
    {
        plot_matchingDatas(mds, &MatchingData::needleSize, &MatchingData::needleHybridationCount,
                           op->dat[cur_ds_idx], numSampleBySizeDs);
        op->dat[cur_ds_idx]->source = strdup("Hybridation count by needle size");
        numSampleBySizeDs = 0;
        cur_ds_idx++;
    }

    if (mDataFoundOrigBySize)
    {
        plot_matchingDatas(mds, &MatchingData::needleSize, &MatchingData::foundAtOrigin,
                           op->dat[cur_ds_idx], numSampleBySizeDs);
        op->dat[cur_ds_idx]->source
            = strdup("Is the needle has been found at his original position by needle size");
        numSampleBySizeDs = 0;
        cur_ds_idx++;
    }

    if (mDataChrBySize)
    {
        plot_matchingDatas(mds, &MatchingData::needleSize, &MatchingData::chromosomeCount,
                           op->dat[cur_ds_idx], numSampleBySizeDs);
        op->dat[cur_ds_idx]->source
            = strdup("In how many chromosome the needle has been found by needle size");
        numSampleBySizeDs = 0;
        cur_ds_idx++;
    }

    if (mDataOrigPosRankBySize)
    {
        plot_matchingDatas(mds, &MatchingData::needleSize, &MatchingData::originalPosRank,
                           op->dat[cur_ds_idx], numSampleBySizeDs);
        op->dat[cur_ds_idx]->source = strdup(
                                          "rank of the original position solution in the best solution liste by needle size");
        numSampleBySizeDs = 0;
        cur_ds_idx++;
    }

    if (mDataMatchByHyb)
    {
        plot_matchingDatas(mds, &MatchingData::needleHybridationCount, &MatchingData::matchCount,
                           op->dat[cur_ds_idx], numSampleByHybDs);
        op->dat[cur_ds_idx]->source = strdup("Matching count by hybridation count");
        numSampleByHybDs = 0;
        cur_ds_idx++;
    }

    if (mDataChrByHyb)
    {
        plot_matchingDatas(mds, &MatchingData::needleHybridationCount,
                           &MatchingData::chromosomeCount, op->dat[cur_ds_idx], numSampleByHybDs);
        op->dat[cur_ds_idx]->source = strdup("Chromosome count by hybridation count");
        numSampleByHybDs = 0;
        cur_ds_idx++;
    }

    if (mDataOrigPosRankByHyb)
    {
        plot_matchingDatas(mds, &MatchingData::needleHybridationCount,
                           &MatchingData::originalPosRank, op->dat[cur_ds_idx], numSampleByHybDs);
        op->dat[cur_ds_idx]->source = strdup("rank of the original position solution in the best "
                                             "solution liste by  hybridation count");
        numSampleByHybDs = 0;
        cur_ds_idx++;
    }

    if (mDataFoundOrigByHyb)
    {
        plot_matchingDatas(mds, &MatchingData::needleHybridationCount, &MatchingData::foundAtOrigin,
                           op->dat[cur_ds_idx], numSampleByHybDs);
        op->dat[cur_ds_idx]->source
            = strdup("Is the needle has been found at his original position by hybridation count");
        numSampleByHybDs = 0;
        cur_ds_idx++;
    }

    if (mDataHybByMatch)
    {
        plot_matchingDatas(mds, &MatchingData::matchCount, &MatchingData::needleHybridationCount,
                           op->dat[cur_ds_idx], numSampleByMatchDs);
        op->dat[cur_ds_idx]->source = strdup("hybridation count by match count");
        numSampleByMatchDs = 0;
        cur_ds_idx++;
    }

    if (mDataUniqueMatchByCoperture)
    {
        plot_matchingDatas(
            mds, &MatchingData::coperture, &MatchingData::uniqueMatch, op->dat[cur_ds_idx], 0);
        op->dat[cur_ds_idx]->source = strdup("unique match by coperture");
        cur_ds_idx++;
    }

    op->need_to_refresh = 1;
    return D_O_K;
}

int mData_plot_idle_action(O_p *op, DIALOG *)
{
    pltreg *pr = find_pr_in_current_dialog(NULL);

    if (!pr)
    {
        return WIN_CANCEL;
    }

    if (mDataFuture.valid()
            && mDataFuture.wait_for(std::chrono::seconds(0)) == std::future_status::timeout)
    {
        mDataMultiplePlot(op, matchingDatas);
    }
    else
    {
        mDataMultiplePlot(op, matchingDatas);
        op->op_idle_action = 0;
    }

    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}
int computeEfficencyAsync(int minFgpSize, int maxFgpSize, int stepSize, int nbIteration, int seqIdx,
                          bool allowReverse, double minBaseNmRate, double maxBaseNmRate, bool
#ifndef NDEBUG
                          stopOnFail
#endif
                          ,
                          double noiseDeviation, BasicFinder refFinder)
{
    matchingDatasMutex.lock();
    matchingDatas.clear();
    matchingDatasMutex.unlock();
    srand(time(NULL));
    int nb_step = (maxFgpSize - minFgpSize) / stepSize + 1;
    int i;

    for (i = 0; i < nb_step; ++i)
    {
        int needle_size = minFgpSize + i * stepSize;
        #pragma omp parallel for

        for (int j = 0; j < nbIteration; j++)
        {
            SearchMatchSet matchs;
            int randfgp = seqIdx < 0 ? std::rand() % Env::instance().references().size() : seqIdx;
            int chrCount = 0;
            std::shared_ptr<Fingerprint> needle;
            auto fgp = std::dynamic_pointer_cast<DistFingerprint>(Env::instance().references()[randfgp]);
            int locus = rand() % (fgp->size() - needle_size);
            bool isReversed = allowReverse && std::rand() % 2;
            double baseNmRate = minBaseNmRate != 1 || maxBaseNmRate != 1
                                ? minBaseNmRate + ((maxBaseNmRate - minBaseNmRate) * rand()) / (RAND_MAX)
                                : 1;
            FingerprintSampleGenerator fgpsg;
            fgpsg.sizeDistribution((maxFgpSize + minFgpSize) / 2, maxFgpSize - minFgpSize);
            fgpsg.addedHybDistribution(0);
            fgpsg.missingHybDistribution(0);
            fgpsg.noiseDistribution(0, noiseDeviation);

            if (allowReverse)
            {
                fgpsg.directionDistribution(.5);
            }
            else
            {
                fgpsg.directionDistribution(1);
            }

//    std::cerr << currentStep << ": "<< cur_locus << std::endl;
            auto cur_fgp = std::shared_ptr<Fingerprint>(fgpsg.generateOne(*fgp, locus, needle_size, isReversed, baseNmRate));
            BasicFinder ffinder = refFinder;

            if (ffinder.isFingerprintSignificant(*needle))
            {
                int k;
                int kend;

                if (seqIdx >= 0)
                {
                    k = seqIdx;
                    kend = k + 1;
                }
                else
                {
                    k = 0;
                    kend = Env::instance().references().size();
                }

                for (; k < kend; ++k)
                {
                    BasicSearchResult chrMatchs = ffinder.findAll(dynamic_cast<const DistFingerprint&>(*(Env::instance().references()[k])),
                                                  dynamic_cast<DistFingerprint&>(*needle));

                    if (chrMatchs.solutions().size() > 0)
                    {
                        chrCount++;
                    }

                    for (auto match : chrMatchs.solutions())
                    {
                        match.chromosome(k);
                        matchs.insert(match);
                    }
                }

                bool foundAtOrigin = false;
                int originalPosRank = -1;

                for (auto It = matchs.cbegin(); !foundAtOrigin && It != matchs.cend(); ++It)
                {
                    foundAtOrigin = It->chromosome() == randfgp
                                    && It->firstReferencePosition()
                                    == fgp->hybridationMap().lower_bound(locus)->first;
                    ++originalPosRank;
                }

#ifndef NDEBUG
                #pragma omp critical

                if (!foundAtOrigin && stopOnFail)
                {
                    std::ofstream ofs;
                    ofs.open("../dataset/notFound.sfgp", std::ofstream::out);
                    needle->dumpSfgp(ofs) << std::endl;
                    ofs.close();
                    ofs.open("../dataset/notFound-orig.sfgp", std::ofstream::out);
                    fgpsg.noiseDistribution(0, 0);
                    fgpsg.generateOne(*fgp, locus, needle_size, false, 1)->dumpSfgp(ofs)
                            << std::endl;
                    ofs.close();
                    assert(false);
                }

#endif
                #pragma omp critical(affectMatchingDatas)
                {
                    matchingDatasMutex.lock();
                    matchingDatas.push_back(MatchingData(matchs.size(), needle_size,
                                                         needle->nbHybridation(), needle->coperture(), foundAtOrigin,
                                                         originalPosRank, chrCount, needle->oligosCount()));
                    matchingDatasMutex.unlock();
                }
            }

            needle = 0;
            #pragma omp critical(affectMatchingDatas)
            {
                matchingDatasMutex.lock();

                if (matchingDatas.size() % 30 == 0)
                {
                    std::ofstream ofs;
                    std::stringstream ss;
                    ss << "matchingdata-" << matchingDatas.size() << "-samples"
                       << ".matchdata";
                    ofs.open(ss.str(), std::ifstream::out);
                    error_message("no more boost archive");
                    //boost::archive::text_oarchive oa(ofs);
                    //oa << matchingDatas;
                    ofs.close();
                }

                my_set_window_title("number of samples: %d", (int)matchingDatas.size());
                matchingDatasMutex.unlock();
            }
        }

        #pragma omp critical(affectMatchingDatas)
        {
            std::ofstream ofs;
            std::stringstream ss;
            ss << "matchingdata-" << minFgpSize << "to" << needle_size << ".matchdata";
            ofs.open(ss.str(), std::ifstream::out);
            //boost::archive::text_oarchive oa(ofs);
            error_message("no more boost archive");
            matchingDatasMutex.lock();
            //oa << matchingDatas;
            matchingDatasMutex.unlock();
            ofs.close();
            /*if (mDataRunningOp)
              {
              mData_plot_idle_action(mDataRunningOp, 0);
              }*/
            std::cout << "step " << i << " / " << nb_step << std::endl;
        }
    }

    std::ofstream ofs;
    ofs.open("matchingdata.matchdata", std::ifstream::out);
    error_message("no more boost archive");
    //boost::archive::text_oarchive oa(ofs);
    matchingDatasMutex.lock();
    //oa << matchingDatas;
    matchingDatasMutex.unlock();
    ofs.close();
    return D_O_K;
}
int do_matchdata_load(void)
{
    char *file = 0;
    std::ifstream ifs;
    pltreg *pr = 0;
    O_p *op = 0;
    int allegro_win_return = D_O_K;
    int datasetCount = 0;
    static int mode = 2;
    static int add_error = 0;
    std::vector<MatchingData> datas;

    if (updating_menu_state != 0)
    {
        active_menu->flags &= ~D_DISABLED;
        return D_O_K;
    }

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("Load a fingerprint file");
    }

    allegro_win_return = win_scanf("mode: %R file %r current data %r attach\n"
                                   "filter: \n"
                                   "%b filter needle smaller than %5d\n"
                                   "%b filter needle bigger than %5d\n"
                                   "%b add y standard deviation \n"
                                   "display: \n"
                                   "%b sample count by barcode size \n"
                                   "%b match count by barcode size \n"
                                   "%b hybridation count by barcode size \n"
                                   "%b found at origin by barcode size \n"
                                   "%b chromosome count by barcode size \n"
                                   "%b original position rank by barcode size \n"
                                   "--\n"
                                   "%b sample count by hybridation count \n"
                                   "%b match count by hybridation count \n"
                                   "%b chromosome count by hybridation count \n"
                                   "%b original position rank by hybridation count \n"
                                   "%b found at origin by hybridation count \n"
                                   "--\n"
                                   "%b sample count by match count\n"
                                   "%b hybridation count by match count\n"
                                   "--\n"
                                   "%b unique match by coperture\n",
                                   &mode, &mDataFilterSmallNeedle, &mDataFilterMinNeedleSize, &mDataFilterBigNeedle,
                                   &mDataFilterMaxNeedleSize, &add_error, &mDataNumSampleBySize, &mDataMatchBySize,
                                   &mDataHybBySize, &mDataFoundOrigBySize, &mDataChrBySize, &mDataOrigPosRankBySize,
                                   &mDataNumSampleByHyb, &mDataMatchByHyb, &mDataChrByHyb, &mDataOrigPosRankByHyb,
                                   &mDataFoundOrigByHyb, &mDataNumSampleByMatch, &mDataHybByMatch,
                                   &mDataUniqueMatchByCoperture);

    if (allegro_win_return == WIN_CANCEL)
    {
        return OFF;
    }

    if (mode == 0)
    {
        if (!(file = open_one_file_config("Open matchdata File", NULL,
                                          "Matching Data Files\0*.matchdata\0\0", "MATCHDATA-FILE", "last_loaded")))
        {
            return OFF;
        }

        ifs.open(file, std::ifstream::in);

        if (ifs.good())
        {
            error_message("no more boost archive");
            //boost::archive::text_iarchive ia(ifs);
            //ia >> datas;
            win_printf_OK("%d matchingData loaded\n", datas.size());
        }

        ifs.close();
    }

    if (ac_grep(cur_ac_reg, "%pr", &pr) != 1) // we get the plot region
    {
        return win_printf_OK("cannot find data");
    }

    if ((op = create_and_attach_one_plot(pr, 10, 10, 0)) == NULL)
    {
        return win_printf_OK("cannot create plot!");
    }

    if (add_error)
    {
        alloc_data_set_y_error(op->dat[0]);
    }

    datasetCount = mDataMatchBySize + mDataHybBySize + mDataFoundOrigBySize + mDataMatchByHyb
                   + mDataChrByHyb + mDataChrBySize + mDataOrigPosRankByHyb + mDataOrigPosRankBySize
                   + mDataFoundOrigByHyb + mDataHybByMatch + mDataNumSampleBySize + mDataNumSampleByHyb
                   + mDataNumSampleByMatch + mDataUniqueMatchByCoperture;

    for (int i = 1; i < datasetCount; ++i)
    {
        if ((create_and_attach_one_ds(op, 10, 10, 0)) == NULL)
        {
            return win_printf_OK("cannot create plot!");
        }

        if (add_error)
        {
            alloc_data_set_y_error(op->dat[i]);
        }
    }

    set_plot_title(op, "matching stat");

    if (mode == 0)
    {
        mDataMultiplePlot(op, datas);
        refresh_plot(pr, UNCHANGED);
    }
    else if (mode == 1)
    {
        mDataMultiplePlot(op, matchingDatas);
        refresh_plot(pr, UNCHANGED);
    }
    else
    {
        op->op_idle_action = mData_plot_idle_action;
    }

    do_update_menu();
    return D_O_K;
}
int do_fingerprint_efficency_by_size(void)
{
    int allegro_win_return = 0;
    int sequence_idx = 0;
    int all_sequences = get_config_int("BARCODE-TEST", "USE-ALL-BARCODE", 1);
    float error = get_config_float("BARCODE-TEST", "MAX-ERROR", MAX_HYBRIDATION_ERROR);
    int nb_iteration = get_config_int("BARCODE-TEST", "NB-ITERATION", 256);
    int searchNoiseDeviation
        = get_config_int("BARCODE-TEST", "SEARCH-NOISE-DEVIATION", THEORICAL_STANDARD_DEVIATION);
    float minNmBaseRate = get_config_float("BARCODE-TEST", "MIN-NM-BASE-RATE", MIN_NM_BASE_RATE);
    float maxNmBaseRate = get_config_float("BARCODE-TEST", "MAX-NM-BASE-RATE", MAX_NM_BASE_RATE);
    int allowReverse = get_config_int("BARCODE-TEST", "NEEDLE-ALLOW-REVERSE", 1);
    int noiseDeviation = get_config_float(
                             "BARCODE-TEST", "NEEDLE-ALLOW-REVERSE", THEORICAL_STANDARD_DEVIATION - 1);
    float minBaseNmRate
        = get_config_float("BARCODE-TEST", "NEEDLE-MIN-BASE-NM-RATE", MIN_BASE_NM_RATE);
    float maxBaseNmRate
        = get_config_float("BARCODE-TEST", "NEEDLE-MAX-BASE-NM-RATE", MAX_BASE_NM_RATE);
    int max_fingerp_size = get_config_int("BARCODE-TEST", "MAX-BARCODE-SIZE", 2000);
    int min_fingerp_size = get_config_int("BARCODE-TEST", "MIN-BARCODE-SIZE", 100);
    int cuttingMethod = get_config_int("BARCODE-TEST", "NEEDLE-CUTTING-METHOD", 0);
    int stepSize = get_config_int("BARCODE-TEST", "BARCODE-SIZE-STEP", 100);
    char restrictionSite[1024];
    strcpy(restrictionSite, get_config_string("BARCODE-TEST", "NEEDLE-RESTRICTION-SITE", "AAATTT"));
    int stopOnFail = 0;
    BasicFinder ffinder;

    if (updating_menu_state != 0)
    {
        if (Env::instance().references().size() > 0)
        {
            active_menu->flags &= ~D_DISABLED;
        }
        else
        {
            active_menu->flags |= D_DISABLED;
        }

        return D_O_K;
    }

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine calculate statistics of a fingerprint");
    }

    if (mDataFuture.valid()
            && mDataFuture.wait_for(std::chrono::seconds(0)) == std::future_status::timeout)
    {
        return win_printf_OK("A instance is already running.\n"
                             "Current parameters are :\n"
                             "Search: \n"
                             "match on all sequences %d\n"
                             "sequence index %3d\n"
                             "iteration by step %5d\n"
                             "max error %5f \n"
                             "search noise deviation %5d\n"
                             "nm -> base rate: min %5f max %5f\n"
                             "Env::instance().experimentals(): \n"
                             "size: min %5d max %5d\n"
                             "cuttingMethod : %d\n"
                             "cut by size: step %5d\n"
                             "cut by enzym: site 5'-%5s-3'\n"
                             "allow reverse %d\n"
                             "noise deviation %5d\n"
                             "base -> nm rate: min %5f max %5f\n",
                             all_sequences, sequence_idx, nb_iteration, error, searchNoiseDeviation, minNmBaseRate,
                             maxNmBaseRate, min_fingerp_size, max_fingerp_size, cuttingMethod, stepSize,
                             restrictionSite, allowReverse, noiseDeviation, minBaseNmRate, maxBaseNmRate);
    }

    allegro_win_return = win_scanf("Search: \n"
                                   " sequence index %3d\n"
                                   "%b match on all sequences \n"
                                   "iteration by step %5d\n"
                                   "max error %5f \n"
                                   "search noise deviation %5d\n"
                                   "nm -> base rate: min %5f max %5f\n"
                                   "Env::instance().experimentals(): \n"
                                   "size: min %5d max %5d\n"
                                   " cut by size: step %5d\n"
                                   "%b cut by enzym: site 5'-%5s-3'\n"
                                   "allow reverse %b\n"
                                   "noise deviation %5d\n"
                                   "base -> nm rate: min %5f max %5f\n"
#ifndef NDEBUG
                                   "stop on fail %b"
#endif
                                   ,
                                   &sequence_idx, &all_sequences, &nb_iteration, &error, &searchNoiseDeviation, &minNmBaseRate,
                                   &maxNmBaseRate, &min_fingerp_size, &max_fingerp_size, &stepSize, &cuttingMethod,
                                   &restrictionSite, &allowReverse, &noiseDeviation, &minBaseNmRate, &maxBaseNmRate
#ifndef NDEBUG
                                   ,
                                   &stopOnFail
#endif
                                  );

    if (allegro_win_return == WIN_CANCEL)
    {
        return OFF;
    }

    set_config_int("BARCODE-TEST", "USE-ALL-BARCODE", all_sequences);
    set_config_float("BARCODE-TEST", "MAX-ERROR", error);
    set_config_int("BARCODE-TEST", "NB-ITERATION", nb_iteration);
    set_config_int("BARCODE-TEST", "MAX-BARCODE-SIZE", max_fingerp_size);
    set_config_int("BARCODE-TEST", "MIN-BARCODE-SIZE", min_fingerp_size);
    set_config_int("BARCODE-TEST", "BARCODE-SIZE-STEP", stepSize);
    set_config_int("BARCODE-TEST", "SEARCH-NOISE-DEVIATION", searchNoiseDeviation);
    set_config_int("BARCODE-TEST", "NEEDLE-ALLOW-REVERSE", allowReverse);
    set_config_float("BARCODE-TEST", "NEEDLE-ALLOW-REVERSE", noiseDeviation);
    set_config_float("BARCODE-TEST", "NEEDLE-MIN-BASE-NM-RATE", minBaseNmRate);
    set_config_float("BARCODE-TEST", "NEEDLE-MAX-BASE-NM-RATE", maxBaseNmRate);
    set_config_string("BARCODE-TEST", "NEEDLE-RESTRICTION-SITE", restrictionSite);
    set_config_int("BARCODE-TEST", "NEEDLE-CUTTING-METHOD", cuttingMethod);

    if (!all_sequences && !(sequence_idx >= 0 && sequence_idx < (int)Env::instance().references().size()))
    {
        win_printf("invalid index choose");
        return OFF;
    }

    ffinder.maxSearchHybridationError(error);
    ffinder.noiseDeviation(searchNoiseDeviation);
    ffinder.minNmBaseRate(minNmBaseRate);
    ffinder.maxNmBaseRate(maxNmBaseRate);
    ffinder.skipMissingOligoInHaystack(false);
    ffinder.skipMissingOligoInNeedle(false);
    mDataFuture = std::async(std::launch::async, computeEfficencyAsync, min_fingerp_size,
                             max_fingerp_size, stepSize, nb_iteration, all_sequences ? -1 : sequence_idx, allowReverse,
                             (double)minBaseNmRate, (double)maxBaseNmRate, stopOnFail, (double)noiseDeviation, ffinder);
    do_matchdata_load();
    return D_O_K;
}

int do_fingerprint_distance_distribution(void)
{
    form_window_t *form = NULL;
    int form_ret = 0;
    int haystack_idx = 0;
    static float fraction_in_class = 0.1;
    static int maxdist = MAX_DISTANCE_BETWEEN_PEAK;
    static float minBaseNmRate = MIN_BASE_NM_RATE;
    static float maxBaseNmRate = MAX_BASE_NM_RATE;
    static float classWidthMultiplifier = 3;
    static bool in_current_plot = false;
    static bool save_as_ref = false;
    pltreg *pr = NULL;
    O_p *op = NULL;

    if (updating_menu_state != 0)
    {
        if (Env::instance().references().size() <= 0)
        {
            active_menu->flags |= D_DISABLED;
        }
        else
        {
            active_menu->flags &= ~D_DISABLED;
        }

        return D_O_K;
    }

    form = form_create("Plot distance distribution: ");
    form_push_label(form, "haystack :");
    generate_combo_box(form, &haystack_idx, Env::instance().references());
    form_newline(form);
    form_push_label(form, "max fraction in class: ");
    form_push_float(form, 0.05, &fraction_in_class);
    form_newline(form);
    form_push_label(form, "min Bp to Nm Rate: ");
    form_push_float(form, 0.05, &minBaseNmRate);
    form_newline(form);
    form_push_label(form, "max Bp to Nm Rate: ");
    form_push_float(form, 0.05, &maxBaseNmRate);
    form_newline(form);
    form_push_label(
        form, "create new classe when class width > (X x stretching amplitude at max distance");
    form_newline(form);
    form_push_label(form, "X =");
    form_push_float(form, 0.5, &classWidthMultiplifier);
    form_newline(form);
    form_push_label(form, "maximum distance between peaks: ");
    form_push_int(form, &maxdist);
    form_newline(form);
    form_push_label(form, "put in current plot");
    form_push_bool(form, &in_current_plot);
    form_newline(form);
    form_push_bool(form, &save_as_ref);
    form_push_label(form, "save as reference classes");
    form_ret = form_run(form);
    form_free(form);

    if (form_ret != WIN_OK)
    {
        return D_O_K;
    }

    DistClasses *dcv = fingerprint_distance_distribution(haystack_idx, fraction_in_class,
                       minBaseNmRate, maxBaseNmRate, classWidthMultiplifier, maxdist, in_current_plot);

    if (save_as_ref)
    {
        Env::instance().distClasses(*dcv);
    }

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2)
    {
        return D_O_K;
    }

    do_update_menu();
    return refresh_plot(pr, pr->n_op - 1);
}

DistClasses *fingerprint_distance_distribution(int haystack_idx, float fraction_in_class,
        float minBaseNmRate, float maxBaseNmRate, float classWidthMultiplifier, int maxDistance,
        bool in_current_plot)
{
    DistClassGenerator *dcg = NULL;
    DistClasses *dcv = NULL;
    std::vector<int> *dists = NULL;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    float max_y_val = 0;
    dcg = new DistClassGenerator(*Env::instance().references()[haystack_idx], fraction_in_class, minBaseNmRate,
                                 maxBaseNmRate, classWidthMultiplifier, maxDistance);
    dists = dcg->computeDistDistribution();
    dcv = dcg->computeDistClasses(dists);

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2)
    {
        return D_O_K;
    }

    if (!in_current_plot)
    {
        op = create_and_attach_one_plot(pr, dists->size(), dists->size(), 0);

        if (!op)
        {
            return NULL;
        }

        ds = op->dat[0];
    }
    else
    {
        ds = create_and_attach_one_ds(op, dists->size(), dists->size(), 0);
    }

    for (uint i = 0; i < dists->size(); ++i)
    {
        ds->xd[i] = i;
        ds->yd[i] = (*dists)[i];

        if (ds->yd[i] > max_y_val)
        {
            max_y_val = ds->yd[i];
        }
    }

    set_ds_source(
        ds, "%s", (Env::instance().references()[haystack_idx]->oligoSet().begin())->get().oligo().c_str());
    ds = create_and_attach_one_ds(op, dcv->range.size() * 2, dcv->range.size() * 2, 0);

    for (uint i = 0; i < dcv->range.size(); ++i)
    {
        // ds->xd[i * 4] = (*dcv)[i].first;
        // ds->yd[i * 4] = 0;
        // ds->xd[i * 4 + 1] = (*dcv)[i].first;
        // ds->yd[i * 4 + 1] = 10000;
        ds->xd[i * 2] = (*dcv).range[i].second;
        ds->yd[i * 2] = 0;
        ds->xd[i * 2 + 1] = (*dcv).range[i].second;
        ds->yd[i * 2 + 1] = max_y_val;
    }

    set_dash_line(ds);
    std::stringstream ss;
    ss << (Env::instance().references()[haystack_idx]->oligoSet().begin())->get().oligo()
       << "- hybridations :" << Env::instance().references()[haystack_idx]->nbHybridation();
    set_plot_title(op, ss.str().c_str());
    set_plot_x_title(op, "distance in Bp");
    set_plot_y_title(op, "num of elements");
    op->need_to_refresh = ALL_NEED_REFRESH;
    Env::instance().distClasses(*dcv);
    return dcv;
}

const char *generate_oligo(int size)
{
    char *oligo = (char *)calloc((size + 1), sizeof(char));
    int nbNucleo = 4;
    char nucleo[4] = { 'A', 'T', 'C', 'G' };
    std::srand(time(0));

    for (int i = 0; i < size; ++i)
    {
        oligo[i] = nucleo[std::rand() % nbNucleo];
    }

    oligo[size] = '\0';
    return oligo;
}
const char *generate_oligo_from_seq(sqd *sequence, int size)
{
    char *oligo = (char *)calloc((size + 1), sizeof(char));
    std::srand(time(0));
    int pos = std::rand() % sequence->totalSize - size;
    pos = pos >= 0 ? pos : 0;

    for (int i = 0; i < size; ++i)
    {
        oligo[i] = sequence->seq[(pos + i) / sequence->pageSize][(pos + i) % sequence->pageSize];
    }

    oligo[size] = '\0';
    return oligo;
}

int do_create_stats(void)
{
    int minOligoSize = 1;
    int maxOligoSize = 23;
    int iterations = 10000;
    sqd **sequences = 0;
    int nb_sequences = 0;
    pltreg *pr = 0;
    O_p *op = 0;
    d_s *unique_ds = 0;
    d_s *found_ds = 0;
    int mode = 0;
    get_loaded_sequences(&sequences, &nb_sequences);

    if (updating_menu_state != 0)
    {
        if (nb_sequences <= 0)
        {
            active_menu->flags |= D_DISABLED;
        }
        else
        {
            active_menu->flags &= ~D_DISABLED;
        }

        return D_O_K;
    }

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("stat on Env::instance().references()");
    }

    int allegro_win_return = win_scanf("%R therorical %r from sequence \n"
                                       "From %d to %d\n"
                                       "number of iterations %d\n",
                                       &mode, &minOligoSize, &maxOligoSize, &iterations);

    if (allegro_win_return == WIN_CANCEL)
    {
        return D_O_K;
    }

    if (ac_grep(cur_ac_reg, "%pr", &pr) != 1) // we get the plot region
    {
        return win_printf_OK("cannot find data");
    }

    if ((op = create_and_attach_one_plot(
                  pr, maxOligoSize - minOligoSize, maxOligoSize - minOligoSize, 0)) == NULL)
    {
        return win_printf_OK("cannot create plot!");
    }

    unique_ds = op->dat[0];

    if (mode == 0)
    {
        unique_ds->nx = 0;
        unique_ds->ny = 0;

        for (int i = minOligoSize; i < maxOligoSize; ++i)
        {
            double denum = (double)sequences[0]->totalSize / std::pow(4, i);
            unique_ds->xd[i] = i;
            double expmdenum = std::exp(-denum);
            unique_ds->yd[i] = expmdenum + expmdenum * denum;
            unique_ds->nx++;
            unique_ds->ny++;
            op->need_to_refresh = 1;
            refresh_plot(pr, UNCHANGED);
        }
    }
    else
    {
        if ((found_ds = create_and_attach_one_ds(
                            op, maxOligoSize - minOligoSize, maxOligoSize - minOligoSize, 0)) == NULL)
        {
            return win_printf_OK("cannot create dataset!");
        }

        set_plot_title(op, "matching stat");
        unique_ds->source = "unique match in sequence";
        found_ds->source = "found in sequence";
        unique_ds->nx = 0;
        found_ds->nx = 0;
        unique_ds->ny = 0;
        found_ds->ny = 0;

        for (int i = minOligoSize; i < maxOligoSize; ++i)
        {
            unique_ds->xd[i - minOligoSize] = i;
            unique_ds->yd[i - minOligoSize] = 0;
            found_ds->xd[i - minOligoSize] = i;
            found_ds->yd[i - minOligoSize] = 0;
            unique_ds->nx++;
            found_ds->nx++;
            unique_ds->ny++;
            found_ds->ny++;
            #pragma omp parallel for

            for (int j = 0; j < iterations; ++j)
            {
                const char *oligo
                    = generate_oligo_from_seq(sequences[std::rand() % nb_sequences], i);
                int nbFound = 0;

                for (int k = 0; k < nb_sequences && nbFound < 2; ++k)
                {
                    int pos = -1;

                    while (nbFound < 2
                            && (pos = findOligoInSeq_norepeat(sequences[k], oligo, pos + 1, 1)) != -1)
                    {
                        nbFound++;
                    }
                }

                unique_ds->yd[i - minOligoSize] += (nbFound == 1);
                found_ds->yd[i - minOligoSize] += (nbFound > 0);
            }

            unique_ds->yd[i - minOligoSize] = unique_ds->yd[i - minOligoSize]
                                              / found_ds->yd[i - minOligoSize];
            found_ds->yd[i - minOligoSize] = found_ds->yd[i - minOligoSize] / iterations;
            #pragma omp critical
            {
                op->need_to_refresh = 1;
                refresh_plot(pr, UNCHANGED);
            }
        }
    }

    return D_O_K;
}



int g_fgp_dist_oligo_size = 0;
char *fingerprint_idle_distribution(O_p *op, DIALOG * /*d*/, int nearest_ds, int nearest_point)
{
    static char message[1024];
    d_s *ds;
    message[0] = '\0';

    if (op == NULL)
    {
        return message;
    }

    if (nearest_ds >= op->n_dat)
    {
        return message;
    }

    ds = op->dat[nearest_ds];
    int oligo_index = (int)ds->xd[nearest_point];
    get_oligo_by_index(message, g_fgp_dist_oligo_size, oligo_index);
    std::stringstream ss;
    ss << "--" << op->dat[op->n_dat - 1]->yd[oligo_index] * 100 << "%% over the threshold";
    my_strncat(message, ss.str().c_str(), sizeof(message));
    return message;
}

int fingerprint_distribution_for_all_oligos_and_generate_class(const char *seq_file, int oligo_size,
        float fraction_in_class, float minBaseNmRate, float maxBaseNmRate, float classWidthMultiplifier,
        int upper_threshold)
{
    char *oligo = (char *)malloc(oligo_size * sizeof(char) + 1);
    oligo[oligo_size] = 0;
    int oligo_nb = std::pow(4, oligo_size);
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    d_s *over_upper_threshold = NULL;
    DistClasses *dcv = 0;
    DistClassGenerator *dcg = NULL;
    std::vector<int> *dists = NULL;

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2)
    {
        return -1;
    }

    op = create_and_attach_one_plot(pr, oligo_nb, oligo_nb, 0);
    set_ds_source(op->dat[0], "class %d", 0);
    // for (int i = 1; i < nb_classes; ++i)
    //{
    //    create_and_attach_one_ds(op, oligo_nb, oligo_nb, 0);
    //    set_ds_source(op->dat[i], "class %d", i);
    //}
    over_upper_threshold = create_and_attach_one_ds(op, oligo_nb, oligo_nb, 0);

    // hyb_count_ds = create_and_attach_one_ds(op, oligo_nb, oligo_nb, 0);

    if (seq_file == NULL)
    {
        seq_file = get_config_string("FASTA-FILE", "last_loaded", "");
    }

    if (!read_fasta_seq(seq_file, true)) // read fasta file accepting "N" nucléotides
    {
        return -1;
    }

    progress_window_t *progress = progress_create("Progress");
    progress_run(progress);

    for (int i = 0; i < oligo_nb; ++i)
    {
        clear_oligo_list();
        get_oligo_by_index(oligo, oligo_size, i);
        progress_set_info_text(progress, oligo);
        oligo_t *oligot = (oligo_t *)malloc(sizeof(oligo_t));
        oligot->seq = strdup(oligo);
        oligot->probability = 1;
        oligot->behaviors_size = 0;
        oligot->behaviors = 0;
        oligot->behaviors_proba = 0;
        fasta_utils_push_oligo(oligot);
        import_from_sequence(true, 0, false, FGP_REPR_DIST, 3, false);
        auto cur_fgp = Env::instance().references()[Env::instance().references().size() - 1];
        const std::string desc = cur_fgp->description();
        std::stringstream ss;
        ss << "[" << oligo << "]" << desc;
        cur_fgp->description(ss.str());
        dcg = new DistClassGenerator(*cur_fgp, fraction_in_class, minBaseNmRate, maxBaseNmRate,
                                     classWidthMultiplifier, upper_threshold);
        dists = dcg->computeDistDistribution();
        dcv = dcg->computeDistClasses(dists);
        progress_set_fraction(progress, (float)i / oligo_nb);

        if (progress_is_canceled(progress))
        {
            break;
        }

        for (int j = 1; j < (int) dcv->range.size() + 1; ++j)
        {
            if (op->n_lab >= j)
            {
                create_and_attach_one_ds(op, oligo_nb, oligo_nb, 0);
            }

            ds = op->dat[j];
            ds->xd[i] = i;
            ds->yd[i] = dcv->range[j].second;
        }

        over_upper_threshold->xd[i] = i;
        over_upper_threshold->yd[i] = (*dists)[upper_threshold - 1]
                                      / (float)Env::instance().references()[Env::instance().references().size() - 1]->nbHybridation();
        // hyb_count_ds->xd[i] = i;
        // hyb_count_ds->yd[i] = Env::instance().references()[Env::instance().references().size() - 1]->nbHybridation();
        delete dcv;
        delete dists;
        delete dcg;
    }

    g_fgp_dist_oligo_size = oligo_size;
    progress_free(progress);
    set_plot_x_title(op, "index of oligo");
    set_plot_y_title(op, "distance in Bp");
    op->op_idle_point_add_display = fingerprint_idle_distribution;
    op->need_to_refresh = ALL_NEED_REFRESH;
    refresh_plot(pr, pr->n_op - 1);
    return 0;
}

int do_fingerprint_distribution_for_all_oligos_and_generate_class(void)
{
    int res = 0;

    if (updating_menu_state != 0)
    {
        active_menu->flags &= ~D_DISABLED;
        return D_O_K;
    }

    static int oligo_size = get_config_int("FINGERPRINT-CLASS", "oligo_size", 4);
    static float fraction_in_class
        = get_config_float("FINGERPRINT-CLASS", "fraction_in_class", 0.1);
    static float minBaseNmRate
        = get_config_float("FINGERPRINT-CLASS", "minBaseNmRate", MIN_BASE_NM_RATE);
    static float maxBaseNmRate
        = get_config_float("FINGERPRINT-CLASS", "maxBaseNmRate", MAX_BASE_NM_RATE);
    static float classWidthMultiplifier
        = get_config_float("FINGERPRINT-CLASS", "classWidthMultiplifier", 3);
    static int upper_threshold = get_config_int("FINGERPRINT-CLASS", "upper_threshold", 500);
    res = win_scanf("oligo size %d \n max fraction in class %f \n"
                    "min base nm rate %f"
                    "max base nm rate %f"
                    "class width multiplifier %f"
                    "distance upper streshold %d \n",
                    &oligo_size, &fraction_in_class, &minBaseNmRate, &maxBaseNmRate, &classWidthMultiplifier,
                    &upper_threshold);

    if (res != D_O_K)
    {
        return -1;
    }

    set_config_int("FINGERPRINT-CLASS", "oligo_size", oligo_size);
    set_config_int("FINGERPRINT-CLASS", "fraction_in_class", fraction_in_class);
    set_config_float("FINGERPRINT-CLASS", "minBaseNmRate", minBaseNmRate);
    set_config_float("FINGERPRINT-CLASS", "maxBaseNmRate", maxBaseNmRate);
    set_config_float("FINGERPRINT-CLASS", "classWidthMultiplifier", classWidthMultiplifier);
    set_config_int("FINGERPRINT-CLASS", "upper_threshold", upper_threshold);
    fingerprint_distribution_for_all_oligos_and_generate_class(NULL, oligo_size, fraction_in_class,
            minBaseNmRate, maxBaseNmRate, classWidthMultiplifier, upper_threshold);
    return 0;
}

int do_fingerprint_plot_search_algo_performance(void)
{
    if (updating_menu_state != 0)
    {
        active_menu->flags &= ~D_DISABLED;
        return D_O_K;
    }

    return D_O_K;
}


MENU *fingerprint_plot_menu(void)
{
    static MENU mn[32];
    static MENU search_mn[32];
    static MENU oligo_seq_search_mn[32];
    static MENU fgp_search_mn[32];

    if (mn[0].text != NULL)
    {
        return mn;
    }

    // do_load_fasta_file();
    // do_load_oligo_file();
    // do_import_from_sequence();
    // do_fingerprint_load();
    // do_fingerprint_load();
    // do_fingerprint_multiple_match();
    // do_fingerprint_search();
    // do_fingerprint_efficency_by_size();
    // do_matchdata_load();
    // do_fingerprint_view();
    //
    // File
    // Display
    // Manual search
    //
//add_item_to_menu(quicklink_menu, "fgp - Benchmark - auto", do_auto_benchmark_from_file, NULL, 0, NULL);
//    add_item_to_menu(quicklink_menu, "fgp - Search - auto", do_auto_search_from_file, NULL, 0, NULL);
//    add_item_to_menu(quicklink_menu, "", NULL, NULL, 0, NULL);
    add_item_to_menu(mn, "Benchmark - auto", do_auto_benchmark_from_file, NULL, 0, NULL);
    add_item_to_menu(mn, "Search - auto", do_auto_search_from_file, NULL, 0, NULL);
    add_item_to_menu(fgp_search_mn, " ", NULL, NULL, 0, NULL);
    add_item_to_menu(
        search_mn, "Visual search", do_fingerprint_solviewer_configure, NULL, 0, NULL);
    add_item_to_menu(search_mn, "Next Solution", do_solviewer_go_next_pos, NULL, 0, NULL);
    add_item_to_menu(search_mn, "Prev Solution", do_solviewer_go_prev_pos, NULL, 0, NULL);
    add_item_to_menu(search_mn, "Increase Reference peak position", do_solviewer_inc_ref_peak, NULL, 0, NULL);
    add_item_to_menu(search_mn, "Decrease Reference peak position", do_solviewer_dec_ref_peak, NULL, 0, NULL);
    // fingerprint search menu
    add_item_to_menu(
        fgp_search_mn, "Search fingerprint into sequence", do_fingerprint_search, NULL, 0, NULL);
    add_item_to_menu(fgp_search_mn, "Search fingerprint into sequence by rank",
                     do_fingerprint_search_rank, NULL, 0, NULL);
    add_item_to_menu(fgp_search_mn, "Search Match for all loaded fingerprint ",
                     do_fingerprint_multiple_match, NULL, 0, NULL);
    add_item_to_menu(fgp_search_mn, " ", NULL, NULL, 0, NULL);
    add_item_to_menu(fgp_search_mn, "oligo coperture of a sequence", do_fingerprint_coperture, NULL, 0, NULL);
    add_item_to_menu(fgp_search_mn, "Test fingerprint efficency on a sequence",
                     do_fingerprint_efficency_by_size, NULL, 0, NULL);
    add_item_to_menu(fgp_search_mn, "create stats", do_create_stats, NULL, 0, NULL);
    add_item_to_menu(fgp_search_mn, "Export last solutions", do_export_last_solutions, NULL, 0, NULL);
    add_item_to_menu(fgp_search_mn, "Load/Attach Matchdata", do_matchdata_load, NULL, 0, NULL);
    // oligo sequence search
    add_item_to_menu(oligo_seq_search_mn, "Plot distance between peaks distribution",
                     do_fingerprint_distance_distribution, NULL, 0, NULL);
    add_item_to_menu(oligo_seq_search_mn, "Search match oligo sequence",
                     do_fingerprint_dist_oligo_sequence_search, NULL, 0, NULL);
    add_item_to_menu(oligo_seq_search_mn, " ", NULL, NULL, 0, NULL);
    add_item_to_menu(mn, "Plot class for all n-mer",
                     do_fingerprint_distribution_for_all_oligos_and_generate_class, NULL, 0, NULL);
    add_item_to_menu(mn, " ", NULL, NULL, 0, NULL);
    add_item_to_menu(oligo_seq_search_mn, " ", NULL, NULL, 0, NULL);
    add_item_to_menu(oligo_seq_search_mn, "Create Oligo solution viewer", do_dist_oligo_solution_viewer_open, NULL, 0,
                     NULL);
    add_item_to_menu(oligo_seq_search_mn, "Next - Oligo solution viewer", do_dist_oligo_solution_viewer_next, NULL, 0,
                     NULL);
    add_item_to_menu(oligo_seq_search_mn, "Prev - Oligo solution viewer", do_dist_oligo_solution_viewer_prev, NULL, 0,
                     NULL);
    add_item_to_menu(oligo_seq_search_mn, "Goto true positive - Oligo solution viewer",
                     do_dist_oligo_solution_viewer_goto_true_positive, NULL, 0,
                     NULL);
    add_item_to_menu(mn, "File", NULL, fingerprint_plot_file_menu(), 0, NULL);
    add_item_to_menu(mn, "Print", NULL, fingerprint_plot_fgp_view_menu(), 0, NULL);
    add_item_to_menu(mn, "Benchmark", NULL, fingerprint_benchmark_plot_menu(), 0, NULL);
    add_item_to_menu(mn, "Chisquare Search", NULL, fingerprint_cssv_plot_menu(), 0, NULL);
    add_item_to_menu(mn, "Dist Oligo Sequence Search", NULL, oligo_seq_search_mn, 0, NULL);
    add_item_to_menu(mn, "Basic Search", NULL, fgp_search_mn, 0, NULL);
    add_item_to_menu(mn, "Manual Search (ImOliSeq)", NULL, search_mn, 0, NULL);
    add_item_to_menu(mn, " ", NULL, NULL, 0, NULL);
    add_item_to_menu(mn, "Generate pseudo experimental fingerprint", do_generate_pseudo_experimental_fingerprint, NULL, 0,
                     NULL);
    //add_item_to_menu(mn, "check boxplot", do_check_boxplot, NULL, 0, NULL);
    setlocale(LC_ALL, "C");
    // do_fingerprint_load();
    // do_fingerprint_efficency_by_size();
    do_update_menu();
    return mn;
}

int fingerprint_main(int argc, char **argv)
{
    fasta_utils_main(argc, argv);
    add_plot_treat_menu_item("fingerprint", NULL, fingerprint_plot_menu(), 0, NULL);
    //unit_test_load_seq_and_oligo(NULL, NULL);
    //unit_test_import_posfingerprint_from_fasta_and_oligo_and_needle(NULL, NULL);
    //unit_test_create_dist_class_and_convert_fingerprints();
    //do_auto_benchmark_from_file();
    //do_cssv_search();
    //do_benchmark_oligo_sequence_algo_css();
    //do_benchmark_oligo_sequence_algo_css();
    return D_O_K;
}

int fingerprint_unload(int /*argc*/, char ** /*argv*/)
{
    remove_item_to_menu(plot_treat_menu, "fingerprint", NULL, NULL);
    return D_O_K;
}

// template
// boost::serialization::singleton<boost::serialization::extended_type_info_typeid<std::map<int,
// std::pair<double, int>>>>::get_instance(void);
