#include "fgp-view-menu.hh"
#include "../sequence/fingerprint.hh"
#include "../sequence/dist-oligo-sequence.hh"
#include "../view/data-list.hh"
#include "../storage/environnement.hh"
#include "form_builder.h"
#include "combo-box.hh"
#include <fstream>
#include <string>
#include <sstream>

typedef fingerprint::Environnement Env;

char *fingerprint_idle_view(struct one_plot *op, DIALOG * /*d*/, int nearest_ds, int nearest_point)
{
    static char message[1024];
    d_s *ds;
    message[0] = 0;

    if (op == NULL)
    {
        return message;
    }

    if (nearest_ds >= op->n_dat)
    {
        return message;
    }

    ds = op->dat[nearest_ds];
    int oligo_index = (int)ds->yd[nearest_point];
    Oligo o = Oligo();
    o.set_key(oligo_index);
    BiDirOligo bdo = o.get();
    strcpy(message, bdo.oligo().c_str());
    return message;
}

int fingerprint_info(std::shared_ptr<Fingerprint> fgp)
{
    if (fgp == NULL)
    {
        return 1;
    }

    int is_ref_fgp = fgp->isReferenceFingerprint();
    std::stringstream ss;

    for (Oligo oligo : fgp->oligoSet())
    {
        ss << oligo.get().oligo() << std::endl;
    }

    std::string oligostring = ss.str();
    return win_printf_OK("description: %s\n"
                         "size: %d\n"
                         "hybridationCount: %d\n"
                         "coperture: %d\n"
                         "isReferenceFingerprint: %d\n"
                         "origin %d\n"
                         "oligosCount: %d\n"
                         "%s\n",
                         fgp->description().c_str(), fgp->size(), fgp->nbHybridation(), fgp->coperture(), is_ref_fgp,
                         fgp->originPosition(), fgp->oligosCount(), oligostring.c_str());
}

int do_fingerprint_info(void)
{
    int allegro_win_return = 0;
    int list_choice = 0;
    int ref_idx = 0;
    int expt_idx = 0;
    DataList listView;
    int curPos = -2;
    int id = 0;
    auto env = Env::instance();

    if (updating_menu_state != 0)
    {
        if (Env::instance().references().size() > 0 || Env::instance().experimentals().size() > 0)
        {
            active_menu->flags &= ~D_DISABLED;
        }
        else
        {
            active_menu->flags |= D_DISABLED;
        }

        return D_O_K;
    }

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine try to match a fingerprint into another");
    }

    form_window_t *form = form_create("Fgp info");
    form_push_option(form, "Reference ", &list_choice);
    generate_combo_box(form, &ref_idx, env.references());
    form_newline(form);
    form_push_option(form, "Experiment ", &list_choice);
    generate_combo_box(form, &expt_idx, env.experimentals());
    int form_ret = form_run(form);

    if (form_ret != WIN_OK)
    {
        form_free(form);
        return D_O_K;
    }

    form_free(form);

    if (allegro_win_return == WIN_CANCEL)
    {
        return OFF;
    }

    if ((list_choice == 0 && (ref_idx) >= (int)Env::instance().references().size())
            || ((list_choice == 1) && expt_idx >= (int)Env::instance().experimentals().size()))
    {
        return win_printf_OK("invalid index!");
    }

    std::shared_ptr<Fingerprint> fgp = list_choice == 0 ? Env::instance().references()[ref_idx] :
                                       Env::instance().experimentals()[expt_idx];
    fingerprint_info(fgp);
    std::string title = std::string(fgp->description());
    std::vector<std::string> headerTexts = {"Id", "Position", "Oligos", "Behavior", "Probability", "Empty ?"};
    std::vector<GType> headerTypes = {G_TYPE_INT, G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_FLOAT, G_TYPE_INT} ;
    listView.setTitle(title);
    listView.setHeaders(headerTexts, headerTypes);

    for (int i = 0; i < fgp->nbHybridation(); ++i)
    {
        curPos = fgp->nextHybridizationPos(++curPos);
        std::cout << curPos << std::endl;
        Hybridation hyb = fgp->hybridationValue(curPos);
        listView.addRow(
            0, i,
            1, curPos,
            2, hyb.theoricalOligo().get().oligo().c_str(),
            3, hyb.actualOligo().get().oligo().c_str(),
            4, hyb.probability(),
            5, hyb.empty(),
            -1);
    }

    id = listView.run();
    return 0;
}

int do_fingerprint_plot()
{
    pltreg *pr = 0;
    O_p *op = 0;
    d_s *fingerprint_dataset = 0;
    static bool add_to_plot = false;
    static bool all_seq = false;
    static int ref_idx = 0;
    static int expt_idx = 0;
    static int list_choice = 0;
    static int partBarcode = 0;
    static int barcodeBegin = 0;
    static int barcodeEnd = 0;
    static int seeProbability = 1;
    static float nmBaseRate = 1.;
    static bool reverse = false;
    std::shared_ptr<Fingerprint> fgp = 0;
    // Fingerprint *fgprev = 0;
    auto env = Env::instance();

    if (updating_menu_state != 0)
    {
        if (Env::instance().references().size() > 0 || Env::instance().experimentals().size() > 0)
        {
            active_menu->flags &= ~D_DISABLED;
        }
        else
        {
            active_menu->flags |= D_DISABLED;
        }

        return D_O_K;
    }

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("View a fingerprint");
    }

    if (ac_grep(cur_ac_reg, "%pr", &pr) != 1) // we get the plot region
    {
        return win_printf_OK("cannot find data");
    }

    form_window_t *form = form_create("Fgp info");
    form_push_option(form, "Reference ", &list_choice);
    generate_combo_box(form, &ref_idx, env.references());
    form_push_label(form, " or ");
    form_push_option(form, "Experiment ", &list_choice);
    generate_combo_box(form, &expt_idx, env.experimentals());
    form_newline(form);
    form_push_label(form, "Take all sequences ");
    form_push_bool(form, &all_seq);
    form_newline(form);
    form_push_label(form, "nm->base rate");
    form_push_float(form, 0.05, &nmBaseRate);
    form_newline(form);
    form_push_label(form, "reverse: ");
    form_push_bool(form, &reverse);
    form_newline(form);
    form_push_option(form, "Full barcode ", &partBarcode);
    form_push_option(form, "or ", &partBarcode);
    form_push_label(form, "begin: ");
    form_push_int(form, &barcodeBegin);
    form_push_label(form, "end: ");
    form_push_int(form, &barcodeEnd);
    form_newline(form);
    form_push_option(form, "View oligo index", &seeProbability);
    form_push_option(form, "or hybridization probability", &seeProbability);
    form_push_label(form, "add to plot: ");
    form_push_bool(form, &add_to_plot);
    form_newline(form);
    int form_ret = form_run(form);

    if (form_ret != WIN_OK)
    {
        form_free(form);
        return D_O_K;
    }

    form_free(form);

    if ((list_choice == 0 && ref_idx >= (int)Env::instance().references().size())
            || (list_choice == 1 && expt_idx >= (int)Env::instance().experimentals().size()))
    {
        return win_printf_OK("invalid index! %d, max index %d", list_choice == 0 ? ref_idx : expt_idx,
                             list_choice == 0 ? Env::instance().references().size() - 1 : Env::instance().experimentals().size() - 1);
    }

    if (!add_to_plot)
    {
        if ((op = create_and_attach_one_plot(
                      pr, 1, 1, 0)) == NULL) /// TODO
        {
            return win_printf_OK("cannot create plot !");
        }
    }
    else
    {
        op = pr->one_p;
    }

    auto fgps = list_choice == 0 ? Env::instance().references() : Env::instance().experimentals();

    for (uint k = 0; k < fgps.size(); ++k)
    {
        fgp =  reverse ? std::shared_ptr<Fingerprint> (fgps[k]->reverse()) : fgps[k];
        fingerprint_dataset = create_and_attach_one_ds(op, fgp->nbHybridation() * 3 + 1, fgp->nbHybridation() * 3 + 1, 0);
        int curPoint = 0;
        int offset = !partBarcode || barcodeBegin < 0 ? 0 : barcodeBegin;
        int max = !partBarcode || barcodeEnd > fgp->size() ? fgp->size() : barcodeEnd;

        for (int i = offset; i < max; i++)
        {
            Hybridation hyb = fgp->hybridationValue(i);

            if (!hyb.empty())
            {
                if (seeProbability)
                {
                    if (curPoint == 0
                            || ((fingerprint_dataset->xd[curPoint - 1]
                                 < Fingerprint::stretchedPositiond(i - 2 - offset, nmBaseRate))))
                    {
                        fingerprint_dataset->xd[curPoint]
                            = Fingerprint::stretchedPositiond(i - 2 - offset, nmBaseRate);
                        fingerprint_dataset->yd[curPoint] = 0;
                        curPoint++;
                    }

                    fingerprint_dataset->xd[curPoint]
                        = Fingerprint::stretchedPositiond(i - offset, nmBaseRate);
                    fingerprint_dataset->yd[curPoint] = hyb.probability();
                    curPoint++;
                    fingerprint_dataset->xd[curPoint]
                        = Fingerprint::stretchedPositiond(i + 2 - offset, nmBaseRate);
                    ;
                    fingerprint_dataset->yd[curPoint] = 0;
                    curPoint++;
                }
                else
                {
                    fingerprint_dataset->xd[curPoint]
                        = Fingerprint::stretchedPositiond(i - offset, nmBaseRate);
                    fingerprint_dataset->yd[curPoint] = hyb.theoricalOligo().get_key();
                    curPoint++;
                }

                // std::cout << hyb << std::endl;
            }
        }

        fingerprint_dataset->nx = curPoint;
        fingerprint_dataset->ny = curPoint;
        set_ds_point_symbol(fingerprint_dataset, "\\fd");
        fingerprint_dataset->source = strdup(fgp->description().c_str());
    }

    std::stringstream st;

    if (!add_to_plot)
    {
        set_plot_title(op, "Fingeprint plot");
        set_plot_x_title(op, "position");
        set_plot_y_title(op, seeProbability ? "hybridation probability" : "hybridated oligo");
    }

    // set_ds_point_symbol(fingerprintrev_dataset, "\\fd");
    op->op_idle_point_add_display = fingerprint_idle_view;
    op->need_to_refresh = 1;
    auto *un = build_unit_set(IS_METER, 1, 1, IS_NANO, IS_X_UNIT_SET, "nm");
    add_x_unit_set_to_op(op, un);
    set_op_x_unit_set(op, 1);
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}

int do_fingerprint_list(void)
{
    int allegro_win_return = 0;
    DataList listView;
    int id = 0;

    if (updating_menu_state != 0)
    {
        if (Env::instance().references().size() > 0 || Env::instance().experimentals().size() > 0)
        {
            active_menu->flags &= ~D_DISABLED;
        }
        else
        {
            active_menu->flags |= D_DISABLED;
        }

        return D_O_K;
    }

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine try to match a fingerprint into another");
    }

    if (allegro_win_return == WIN_CANCEL)
    {
        return OFF;
    }

    std::string title = std::string("Fingerprint List");
    std::vector<std::string> headerTexts = {"Id", "Name", "Oligos", "Size", "Hybridizations", "Origin", "Type"};
    std::vector<GType> headerTypes = {G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_INT, G_TYPE_INT, G_TYPE_INT, G_TYPE_STRING} ;
    listView.setTitle(title);
    listView.setHeaders(headerTexts, headerTypes);
    auto fgpNbHyb = [](const Fingerprint & fgp)
    {
        return fgp.nbHybridation();
    };
    auto zeroFcn = [](auto x)
    {
        (void) x;
        return 0;
    };
    auto tableGo = [&listView](auto vect, auto nbHybFcn)
    {
        for (uint i = 0; i < vect.size(); ++i)
        {
            const auto curSol = vect[i];
            listView.addRow(
                0, i,
                1, curSol->description().c_str(),
//TODO extract oligos
                2, "Not Impl", //curSol->oligoSet().begin()->get().oligo().c_str(),
                3, curSol->size(),
                4, nbHybFcn(*curSol),
                5, curSol->originPosition(),
                6, typeid(*curSol).name(),
                -1);
        }
    };
    tableGo(Env::instance().references(), fgpNbHyb);
    tableGo(Env::instance().referenceDsos(), zeroFcn);
    tableGo(Env::instance().experimentals(), fgpNbHyb);
    tableGo(Env::instance().experimentalDsos(), zeroFcn);
    id = listView.run();
    return 0;
}

int do_display_oligo_index(void)
{
    if (updating_menu_state != 0)
    {
        active_menu->flags &= ~D_DISABLED;
        return D_O_K;
    }

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("Display Global oligo index");
    }

    std::stringstream ss;
    ss << OligoGlobalIndex<BiDirOligo>::instance() << std::endl;
    ss << OligoGlobalIndex<Hybridation>::instance() << std::endl;
    std::cout << ss.str() << std::endl;
    win_printf_OK("%s", ss.str().c_str());
    do_update_menu();
    return D_O_K;
}

MENU *fingerprint_plot_fgp_view_menu(void)
{
    static MENU fv_mn[32];

    if (fv_mn[0].text != NULL)
    {
        return fv_mn;
    }

    add_item_to_menu(fv_mn, "List fingerprint", do_fingerprint_list, NULL, 0, NULL);
    add_item_to_menu(fv_mn, "Plot fingerprint", do_fingerprint_plot, NULL, 0, NULL);
    add_item_to_menu(fv_mn, "Info fingerprint", do_fingerprint_info, NULL, 0, NULL);
    add_item_to_menu(fv_mn, "Display OligoGlobalIndex", do_display_oligo_index, NULL, 0, NULL);
    return fv_mn;
}
