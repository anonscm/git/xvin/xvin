#pragma once
#ifndef XV_WIN32
# include "config.h"
#endif

#include "json.hpp"
using json = nlohmann::json;

int auto_search_from_file(const char *json_file);
int do_auto_search_from_file(void);
int auto_search_one_config(json &jsonConfig);
