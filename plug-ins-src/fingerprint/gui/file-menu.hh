#pragma once
#ifndef XV_WIN32
# include "config.h"
#endif

#include "xvin.h"
#include "../sequence/pos-fingerprint.hh"

bool are_sequences_oligos_loaded();
PXV_FUNC(int, load_fingerprint, (const char *files, bool is_simple_fingerprint, bool is_needle, FgpRepr fgprepr,
                                 bool create_dos));
PXV_FUNC(int, import_from_sequence, (bool import_all_sequences, int sequence_idx, bool is_needle, FgpRepr fgprepr,
                                     int resolution, bool create_dos));

PXV_FUNC(int, do_fingerprint_load,(void));
PXV_FUNC(int, do_fingerprint_save,(void));
PXV_FUNC(int, do_clears_fingerprints,(void));
PXV_FUNC(MENU *, fingerprint_plot_file_menu, (void));
PXV_FUNC(int, do_extract_fingerprint_from_histogram,(void));
PXV_FUNC(int, do_import_from_sequence,(void));

