#include "search-menu.hh"
#include "../sequence/haystack-importer.hh"
#include "../search/chisquare-search.hh"
#include "../search/dist-oligo-sequence-finder.hh"
#include "../search/basic-finder.hh"

#include "xvin.h"
#include "json.hpp"
#include <iostream>
#include <fstream>
#include <boost/filesystem.hpp>
#include "gitinfo.hh"

int json_add_infos(json& j)
{
    time_t t;
    char timec[100] = {0};
    time(&t);
    strftime(timec, sizeof(timec), "%c %Z", std::gmtime(&t));
    j["date"] = timec;
    j["git"] = {git::hashtag(), git::branch(), git::version(), git::author(), git::date()};
    return 0;
}
int auto_search_from_file(const char *json_file)
{
    std::ifstream istm;
    istm.open(json_file);
    json jsonConfig;
    istm >> jsonConfig;
    istm.close();

    if (jsonConfig.is_array())
    {
        //#pragma omp parallel for
        for (uint i = 0; i < jsonConfig.size(); i++)
        {
            auto_search_one_config(jsonConfig[i]);
        }
    }
    else
    {
        auto_search_one_config(jsonConfig);
    }
}

int auto_search_one_config(json& jsonConfig)
{
    std::ofstream myfile;
    json jsonResults;

    if (jsonConfig["searchEngine"] == "ChiSquare")
    {
        HaystackImporter<chiSquareMethod::ChiSquareSearch> hi;
        hi.sequencePath = jsonConfig["refSequencePath"];

        if (!jsonConfig["oligos"].is_null())
        {
            if (!json2oligo(jsonConfig["oligos"], &hi.oligoSet, &hi.oligoSetSize) || hi.oligoSetSize <= 0)
            {
                error_message("oligo loading failed.");
                return 0;
            }
        }
        else if (jsonConfig["oligoSetPath"].is_string())
        {
            hi.oligoSetPath = jsonConfig["oligoSetPath"];
        }
        else
        {
            error_message("no oligo to load");
            return 0;
        }

        hi.resolution = jsonConfig["resolution"];
        std::shared_ptr<PosFingerprint> haystack = hi.importHaystack(false);
        std::shared_ptr<PosFingerprint> expt = std::shared_ptr<PosFingerprint>(new PosFingerprint(jsonConfig["hybridizations"],
                                               hi.oligoSet, hi.oligoSetSize));
        ChiSquareSearchParams cssp;
        chiSquareMethod::ChiSquareSearch css;
        cssp.loadJson(jsonConfig["searchParams"]);
        cssp.generateMissingParams(*haystack);
        css.params(cssp);
        auto results = css.search(haystack, expt);
        json_add_infos(jsonResults);
        jsonResults["results"] = results.dumpJson("", 0);
        jsonResults["params"] = jsonConfig;
        jsonResults["haystack"] = haystack->dumpJson(false);
        results.needle(expt);
        myfile.open(jsonConfig["output"]);
        myfile << std::setprecision(4) << jsonResults.dump()  << std::endl;
        myfile.flush();
        myfile.close();
    }
    else if (jsonConfig["searchEngine"] == "DistOligoSequence")
    {
        HaystackImporter<chiSquareMethod::ChiSquareSearch> hipos;
        HaystackImporter<DistOligoSequenceFinder> hi;
        hi.sequencePath = jsonConfig["refSequencePath"];

        if (!jsonConfig["oligos"].is_null())
        {
            if (!json2oligo(jsonConfig["oligos"], &hi.oligoSet, &hi.oligoSetSize) || hi.oligoSetSize <= 0)
            {
                error_message("oligo loading failed.");
                return 0;
            }
        }
        else if (jsonConfig["oligoSetPath"].is_string())
        {
            hi.oligoSetPath = jsonConfig["oligoSetPath"];
        }
        else
        {
            error_message("no oligo to load");
            return 0;
        }

        hi.resolution = jsonConfig["resolution"];
        auto haystack = hi.importHaystack(false);
        DistOligoSearchParams dosp;
        DistOligoSequenceFinder dosf;
        dosp.loadJson(jsonConfig["searchParams"]);
        dosp.generateMissingParams(*haystack);
        std::shared_ptr<PosFingerprint> expt = std::shared_ptr<PosFingerprint>(new PosFingerprint(jsonConfig["hybridizations"],
                                               hi.oligoSet, hi.oligoSetSize));


        std::shared_ptr<DistOligoSequence> cur_dos = std::shared_ptr<DistOligoSequence> (new DistOligoSequence(
                    expt,
                    haystack->distClasses(), dosp.distClassBaseNmRate));
        dosf.params(dosp);
        auto results = dosf.find(*haystack, *cur_dos);
        json_add_infos(jsonResults);
        jsonResults["results"] = results.dumpJson("", 0);
        jsonResults["params"] = jsonConfig;
        jsonResults["haystack"] = haystack->dumpJson(false);
        results.needle(cur_dos);
        myfile.open(jsonConfig["output"]);
        myfile << std::setprecision(4) << jsonResults.dump()  << std::endl;
        myfile.flush();
        myfile.close();
    }
    else if (jsonConfig["searchEngine"] == "Basic")
    {
    }
    else
    {
        warning_message("Invalid algorithm name");
    }
}

int do_auto_search_from_file(void)
{
    file_list_t *search_files = nullptr;
    file_list_t *tmp = nullptr;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    search_files = open_files_config("Load configuration files", "", "Json file\0*.json\0All file\0*.*\0",
                                     "FINGERPRINT",
                                     "search_config_file");

    while (search_files != nullptr)
    {
        printf("%s\n", search_files->data);
        auto_search_from_file(search_files->data);
        tmp = search_files;
        search_files = search_files->next;
        free(tmp->data);
        free(tmp);
    }
}
