#pragma once
#include "../sequence/fingerprint.hh"
#include "form_builder.h"
#include <vector>

int generate_combo_box(form_window_t *form, int *var, std::vector<Fingerprint *>& vect);
