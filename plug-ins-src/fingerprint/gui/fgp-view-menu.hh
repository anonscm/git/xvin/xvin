#pragma once
#ifndef XV_WIN32
# include "config.h"
#endif
#include "xvin.h"
 
PXV_FUNC(int, do_fingerprint_list, (void));
PXV_FUNC(int, do_fingerprint_info, (void));
PXV_FUNC(int, do_fingerprint_plot, ());
PXV_FUNC(MENU *, fingerprint_plot_fgp_view_menu, (void));
