#include "file-menu.hh"

#include "../sequence/dist-oligo-sequence.hh"
#include "../search/dist-class-generator.hh"
#include "../storage/environnement.hh"
#include "../fingerprint-io.hh"

# include "../../findSeq/findSeq.h"
# include "../../fasta_utils/fasta_utils.h" // /!\ include in this order, to get overwritten findSeq function nam include in this order, to get overwritten findSeq function namee

#include <sstream>
#include <fstream>

typedef fingerprint::Environnement Env;

int do_fingerprint_load(void)
{
    file_list_t *files = 0;
    int allegro_win_return;
    int format_choice = 0;
    int list_choice = 0;
    int fgprepr_choice = 0;
    int create_dos = 0;

    if (updating_menu_state != 0)
    {
        active_menu->flags &= ~D_DISABLED;
        return D_O_K;
    }

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("Load a fingerprint file");
    }

    allegro_win_return = win_scanf("Load Fingerprint: \n"
                                   "from archive %R from simple fgp format %r \n"
                                   "as haystack %R as needle %r \n"
                                   "%R use pos representation \n"
                                   "%r create dist fingerprint\n"
                                   "%b create dist oligo sequence \n",
                                   &format_choice, &list_choice, &fgprepr_choice, &create_dos);

    if (allegro_win_return == WIN_CANCEL
            || (format_choice == 0
                && !(files = open_files_config("Open fingerprint File", NULL,
                             "Fingerprint\0*.fgp\0\0", "FGP-FILE", "last_loaded")))
            || (format_choice == 1
                && !(files = open_files_config("Open fingerprint File", NULL,
                             "Simple Fingerprint\0*.sfgp\0\0", "SFGP-FILE", "last_loaded"))))
    {
        return OFF;
    }

    while (files != NULL)
    {
        const char *file = files->data;
        printf("listchoices : %d\n", list_choice);
        load_fingerprint(
            file, format_choice == 1, list_choice == 1, (FgpRepr)fgprepr_choice, !!create_dos);
        files = files->next;
    }

    free_file_list(files);
    do_update_menu();
    return D_O_K;
}
int load_fingerprint(
    const char *file, bool is_simple_fingerprint, bool is_needle, FgpRepr fgpRepr, bool create_dos)
{
    DistOligoSequence *dos = 0;
    std::ifstream ifs;
    ifs.open(file, std::ifstream::in);

    if (ifs.good())
    {
        if (is_simple_fingerprint)
        {
            std::shared_ptr<Fingerprint> fgp = NULL;

            switch (fgpRepr)
            {
            case FGP_REPR_POS:
            {
                fgp = std::shared_ptr<PosFingerprint>(loadSimplePosFingerprint(ifs));
                break;
            }

            case FGP_REPR_DIST:
            {
                fgp = std::shared_ptr<DistFingerprint>(loadSimpleDistFingerprint(ifs));
                break;
            }
            }

            if (create_dos && !Env::instance().distClasses().range.empty())
            {
                dos = new DistOligoSequence(fgp, Env::instance().distClasses());
            }

            if (is_needle)
            {
                Env::instance().experimentals().push_back(fgp);

                if (dos)
                {
                    Env::instance().experimentalDsos().push_back(std::shared_ptr<DistOligoSequence>(dos));
                }
            }
            else
            {
                Env::instance().references().push_back(fgp);

                if (dos)
                {
                    Env::instance().referenceDsos().push_back(std::shared_ptr<DistOligoSequence>(dos));
                }
            }
        }
        else
        {
            error_message("no more boost archive");
            //boost::archive::text_iarchive ia(ifs);
            // read class state from archive
            // ia.register_type(static_cast<PosFingerprint *>(NULL));
            //ia.register_type(static_cast<DistFingerprint *>(NULL));
            // FIXME shared pointer serialize
            //try
            //{
            //    if (is_needle)
            //    {
            //        ia >> Env::instance().experimentals();
            //    }
            //    else
            //    {
            //        ia >> Env::instance().references();
            //    }
            //}
            //catch (...)
            //{
            //    error_message("invalid fingerprint file %s", file);
            //}
        }

        warning_message(
            "%d haystack loaded\n %d Env::instance().experimentals() in memory", Env::instance().references().size(),
            Env::instance().experimentals().size());
    }
    else
    {
        error_message("invalid file %s\n", file);
    }

    ifs.close();
    return D_O_K;
}

int do_fingerprint_save(void)
{
    char *file = 0;
    std::ofstream ofs;

    if (updating_menu_state != 0)
    {
        if (Env::instance().references().size() > 0)
        {
            active_menu->flags &= ~D_DISABLED;
        }
        else
        {
            active_menu->flags |= D_DISABLED;
        }

        return D_O_K;
    }

    error_message("SAVE DISABLED");

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("Save a fingerprint file");
    }

    if (!(file = save_one_file_config("Save fingerprint File", NULL, NULL,
                                      "Fingerprint\0*.fgp*\0\0", "FGP-FILE", "last_saved")))
    {
        return OFF;
    }

    ofs.open(file, std::ifstream::out);
    //boost::archive::text_oarchive oa(ofs);
    // oa.register_type(static_cast<PosFingerprint *>(NULL));
    // FIXME NO MORE SERIALIZE
    //oa.register_type(static_cast<DistFingerprint *>(NULL));
    //oa << Env::instance().references();
    ofs.close();
    return D_O_K;
}
int do_clear_fingerprints(void)
{
    int clearHaystacks = 1;
    int clearNeedles = 1;

    if (updating_menu_state != 0)
    {
        if (Env::instance().references().size() > 0 || Env::instance().experimentals().size() > 0)
        {
            active_menu->flags &= ~D_DISABLED;
        }
        else
        {
            active_menu->flags |= D_DISABLED;
        }

        return D_O_K;
    }

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("Clear Env::instance().references()");
    }

    int allegro_win_return = win_scanf("Clear :\n"
                                       "Haystacks: %b\n"
                                       "Experimentals: %b\n",
                                       &clearHaystacks, &clearNeedles);

    if (allegro_win_return == WIN_CANCEL)
    {
        return OFF;
    }

    if (clearHaystacks)
    {
        while (Env::instance().references().size() > 0)
        {
            auto fgp = Env::instance().references().back();
            Env::instance().references().pop_back();
        }
    }

    if (clearNeedles)
    {
        while (Env::instance().experimentals().size() > 0)
        {
            auto fgp = Env::instance().experimentals().back();
            Env::instance().experimentals().pop_back();
        }
    }

    do_update_menu();
    return D_O_K;
}
int do_extract_fingerprint_from_histogram(void)
{
    int allegro_win_return;
    int update_fingerprint = 0;
    float threas = 0.15;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    char oligo_str[1024] = { 0 };

    if (updating_menu_state != 0)
    {
        active_menu->flags &= ~D_DISABLED;
        return D_O_K;
    }

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("Import fingerprint from histogram");
    }

    allegro_win_return = win_scanf("threashold %f \n"
                                   "oligo %10s"
                                   "%R as new fingerprint [X] update a fingerprint\n"
                                   "[XXX] index of the fingerprint to update",
                                   &threas, &oligo_str, &update_fingerprint
                                   //,&fgp_idx
                                  );

    if (allegro_win_return == WIN_CANCEL)
    {
        return OFF;
    }

    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &ds) != 3)
    {
        return win_printf_OK("cannot find data");
    }

    DistFingerprint *fgprint = histogramToDistFingerprint(ds, Oligo(BiDirOligo(oligo_str)), threas);

    if (!update_fingerprint)
    {
        Env::instance().experimentals().push_back(std::shared_ptr<Fingerprint>(fgprint));
        //fingerprint_info(std::shared_ptr<Fingerprint>(fgprint));
    }

    return D_O_K;
}

bool are_sequences_oligos_loaded()
{
    int nb_sequences = -1;
    int nb_oligos = 0;
    sqd **sequences = NULL;
    oligo_t **oligos = NULL;
    get_loaded_oligos(&oligos, &nb_oligos);
    get_loaded_sequences(&sequences, &nb_sequences);

    if (nb_oligos < 1 || nb_sequences < 1)
    {
        return false;
    }

    return true;
}
int do_load_then_import_from_sequence(void)
{
    static int clear_fasta = true;
    static int clear_oligo = true;
    static int clear_haystack = true;
    static int clear_expt = true;
    static int oligo_choice = 0;
    char seq[1024] = "";
    float proba = 1.;

    if (updating_menu_state != 0)
    {
        active_menu->flags &= ~D_DISABLED;
        return D_O_K;
    }

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("Import fingerprint from sequence");
    }

    int allegro_win_return = win_scanf("Clear :\n"
                                       "fasta: %b\n"
                                       "oligo: %b\n"
                                       "Haystacks: %b\n"
                                       "Experimentals: %b\n"
                                       "%R for oligo file\n"
                                       "%r oligo seq: %s %f",
                                       &clear_fasta, &clear_oligo,
                                       &clear_haystack, &clear_expt,
                                       &oligo_choice, &seq, &proba);

    if (allegro_win_return == WIN_CANCEL)
    {
        return OFF;
    }

    if (clear_oligo)
    {
        clear_oligo_list();
    }

    if (clear_fasta)
    {
        do_clear_sequence_list();
    }

    if (clear_haystack)
    {
        while (Env::instance().references().size() > 0)
        {
            auto fgp = Env::instance().references().back();
            Env::instance().references().pop_back();
        }
    }

    if (clear_expt)
    {
        while (Env::instance().experimentals().size() > 0)
        {
            auto fgp = Env::instance().experimentals().back();
            Env::instance().experimentals().pop_back();
        }
    }

    do_load_fasta_file();

    if (oligo_choice == 0)
    {
        do_load_oligo_file();
    }
    else
    {
        if (strlen(seq) > 0 && is_dna(seq))
        {
            oligo_t *cur_oligo  = (oligo_t *) malloc(sizeof(oligo_t));
            cur_oligo->seq = strdup(seq);
            cur_oligo->probability = proba;
            cur_oligo->behaviors = NULL;
            cur_oligo->behaviors_proba = NULL;
            cur_oligo->behaviors_size = 0;
            fasta_utils_push_oligo(cur_oligo);
        }
        else
        {
            warning_message("%s is not DNA\n", seq);
        }
    }

    do_import_from_sequence();

    return D_O_K;
}
int do_import_from_sequence(void)
{
    int allegro_win_return = 0;
    static int sequence_idx = 0;
    static int import_all_sequences = 1;
    static int fingerprint_repr_choice = 0;
    static int list_choice = 0;
    static int resolution = 3;
    static int create_dos = 0;

    if (updating_menu_state != 0)
    {
        if (!are_sequences_oligos_loaded())
        {
            active_menu->flags |= D_DISABLED;
        }
        else
        {
            active_menu->flags &= ~D_DISABLED;
        }

        return D_O_K;
    }

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("Import fingerprint from sequence");
    }

    allegro_win_return = win_scanf("%b match on all sequences\n"
                                   "%d or sequence index\n"
                                   "%R as haystack %r as needle\n"
                                   "%R use pos representation \n"
                                   "%r create dist fingerprint\n"
                                   "resolution %d\n"
                                   "%b create dist oligo sequence \n",
                                   &import_all_sequences, &sequence_idx, &list_choice, &fingerprint_repr_choice, &resolution,
                                   &create_dos);

    if (allegro_win_return == WIN_CANCEL)
    {
        return OFF;
    }

    import_from_sequence(import_all_sequences, sequence_idx, list_choice == 1,
                         (FgpRepr)fingerprint_repr_choice, resolution, (bool)create_dos);
    win_printf_OK("%d haystack loaded\n %d experimentals loaded", Env::instance().references().size(),
                  Env::instance().experimentals().size());
    do_update_menu();
    return D_O_K;
}

int import_from_sequence(bool import_all_sequences, int sequence_idx, bool is_needle,
                         FgpRepr fgprepr, int resolution, bool create_dos)
{
    int nb_sequences = 0;
    int nb_oligos = 0;
    sqd **sequences = 0;
    oligo_t **oligos = 0;
    DistOligoSequence *dos = 0;
    get_loaded_oligos(&oligos, &nb_oligos);
    get_loaded_sequences(&sequences, &nb_sequences);
    int i = 0;
    int nb_seq = nb_sequences;

    if (!import_all_sequences)
    {
        i = sequence_idx;
        nb_seq = sequence_idx + 1;
    }

    // boost::bimap is not threadsafe for insertion. we insert all oligo before processing any
    // fingerprint creation
    for (int j = 0; j < nb_oligos; ++j)
    {
        Oligo o = Oligo(BiDirOligo(*(oligos[j])));
    }

    #pragma omp parallel for

    for (i = 0; i < nb_seq; i++)
    {
        std::shared_ptr<Fingerprint>fgp = 0;

        switch (fgprepr)
        {
        case FGP_REPR_POS:
        {
            fgp = std::shared_ptr<PosFingerprint>(new PosFingerprint(sequences[i], oligos, nb_oligos, resolution));
            break;
        }

        case FGP_REPR_DIST:
        {
            fgp = std::shared_ptr<DistFingerprint>(new DistFingerprint(sequences[i], oligos, nb_oligos, resolution));
            break;
        }
        }

        if (create_dos && !Env::instance().distClasses().range.empty())
        {
            dos = new DistOligoSequence(fgp, Env::instance().distClasses());
        }

        #pragma omp critical
        {
            if (is_needle)
            {
                Env::instance().experimentals().push_back(fgp);

                if (dos)
                {
                    Env::instance().experimentalDsos().push_back(std::shared_ptr<DistOligoSequence>(dos));
                }
            }
            else
            {
                Env::instance().references().push_back(fgp);

                if (dos)
                {
                    Env::instance().referenceDsos().push_back(std::shared_ptr<DistOligoSequence>(dos));
                }
            }

            std::cout << "loaded sequence: " << sequences[i]->description << std::endl;
        }
    }

    return D_O_K;
}

MENU *fingerprint_plot_file_menu(void)
{
    static MENU file_mn[32];

    if (file_mn[0].text != NULL)
    {
        return file_mn;
    }

    add_item_to_menu(file_mn, "Load fingerprint file", do_fingerprint_load, NULL, 0, NULL);
    add_item_to_menu(file_mn, "Extract fingerprint from histogram",
                     do_extract_fingerprint_from_histogram, NULL, 0, NULL);
    add_item_to_menu(
        file_mn, "Import fingerprint from sequence", do_import_from_sequence, NULL, 0, NULL);
    add_item_to_menu(
        file_mn, "Load and Import fingerprint from sequence", do_load_then_import_from_sequence, NULL, 0, NULL);
    add_item_to_menu(file_mn, "Save fingerprint file", do_fingerprint_save, NULL, 0, NULL);
    add_item_to_menu(file_mn, "Clear fingerprint file", do_clear_fingerprints, NULL, 0, NULL);
    return file_mn;
}
