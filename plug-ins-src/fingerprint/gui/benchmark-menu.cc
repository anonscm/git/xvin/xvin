#include "benchmark-menu.hh"

#include "../define.hh"
#include "../search/dist-oligo-sequence-finder.hh"
#include "../search/chisquare-search.hh"
#include "../search/basic-finder.hh"
#include "../api.hh"
#include "../storage/environnement.hh"
#include "form_builder.h"
#include "progress_builder.h"
#include "combo-box.hh"
#include "../params/benchmark-params.hh"
#include "../controller/benchmark-controller.hh"
#include "cssv-menu.hh"
#include <iostream>
#include <fstream>

#include "json.hpp"
using json = nlohmann::json;


typedef fingerprint::Environnement Env;

namespace benchmark
{
Controller<chiSquareMethod::ChiSquareSearch> *cssController_ = nullptr;
Controller<DistOligoSequenceFinder> *dosfController_ = nullptr;
Controller<BasicFinder> *basfController_ = nullptr;
}

void checkCssContr(void)
{
    if (!benchmark::cssController_)
    {
        benchmark::cssController_ = new benchmark::Controller<chiSquareMethod::ChiSquareSearch>();
    }
}

void checkDosContr(void)
{
    if (!benchmark::dosfController_)
    {
        benchmark::dosfController_ = new benchmark::Controller<DistOligoSequenceFinder>();
    }
}
void checkBasContr(void)
{
    if (!benchmark::basfController_)
    {
        benchmark::basfController_ = new benchmark::Controller<BasicFinder>();
    }
}

int auto_benchmark_from_file(const char *benchmark_file)
{
    std::ifstream istm;
    istm.open(benchmark_file);
    json jsonConfig;
    //try
    //{
    istm >> jsonConfig;

    if (jsonConfig["searchEngine"] == "ChiSquare")
    {
        checkCssContr();
        benchmark::Params<chiSquareMethod::ChiSquareSearch> params = benchmark::cssController_->config(benchmark_file);

        if (params.haystack == nullptr)
        {
            return D_O_K;
        }

        benchmark::cssController_->benchmark(params);
    }
    else if (jsonConfig["searchEngine"] == "Basic")
    {
        checkBasContr();
        benchmark::Params<BasicFinder> params = benchmark::basfController_->config(benchmark_file);

        if (params.haystack == nullptr)
        {
            return D_O_K;
        }

        benchmark::basfController_->benchmark(params);
    }
    else if (jsonConfig["searchEngine"] == "DistOligoSequence")
    {
        checkDosContr();
        benchmark::Params<DistOligoSequenceFinder> params = benchmark::dosfController_->config(benchmark_file);

        if (params.haystack == nullptr)
        {
            return D_O_K;
        }

        benchmark::dosfController_->benchmark(params);
    }
    else
    {
        warning_message("Invalid algorithm name");
    }

    //}
    //catch (std::exception& e)
    //{
    //    error_message("Exception: %s", e.what());
    //    istm.close();
    //    return -1;
    //}
    istm.close();
}
int do_auto_benchmark_from_file(void)
{
    char *benchmark_file = nullptr;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    benchmark_file = open_one_file_config("Load configuration file", "", "Json file\0*.json\0All file\0*.*\0",
                                          "FINGERPRINT",
                                          "benchmark_config_file");

    if (benchmark_file != nullptr)
    {
        auto_benchmark_from_file(benchmark_file);
    }

    free(benchmark_file);
}


int do_benchmark_css_dump_json(void)
{
    char *filepath = nullptr;

    if (updating_menu_state != 0)
    {
        if (!Env::instance().references().empty() && !Env::instance().distClasses().range.empty())
        {
            active_menu->flags &= ~D_DISABLED;
        }
        else
        {
            active_menu->flags |= D_DISABLED;
        }

        return D_O_K;
    }

    checkCssContr();
    filepath = select_folder_config("Choose where to save Json (Big number of files)", "NULL", "DATALIST",
                                    "last_saved_json_folder");
    benchmark::cssController_->dumpJson(filepath);
    return 0;
}
int do_benchmark_dosf_dump_json(void)
{
    std::ofstream myfile;
    char file[512] = "data.json";
    char *filepath = nullptr;

    if (updating_menu_state != 0)
    {
        if (!Env::instance().references().empty() && !Env::instance().distClasses().range.empty())
        {
            active_menu->flags &= ~D_DISABLED;
        }
        else
        {
            active_menu->flags |= D_DISABLED;
        }

        return D_O_K;
    }

    checkDosContr();
    filepath = select_folder_config("Choose where to save Json (Big number of files)", "NULL", "DATALIST",
                                    "last_saved_json_folder");
    benchmark::dosfController_->dumpJson(filepath);
    return 0;
}
int do_benchmark_basf_dump_json(void)
{
    std::ofstream myfile;
    char file[512] = "data.json";
    char *filepath = nullptr;

    if (updating_menu_state != 0)
    {
        if (!Env::instance().references().empty() && !Env::instance().distClasses().range.empty())
        {
            active_menu->flags &= ~D_DISABLED;
        }
        else
        {
            active_menu->flags |= D_DISABLED;
        }

        return D_O_K;
    }

    checkBasContr();
    filepath = select_folder_config("Choose where to save Json (Big number of files)", "NULL", "DATALIST",
                                    "last_saved_json_folder");
    benchmark::basfController_->dumpJson(filepath);
    return 0;
}
int do_benchmark_oligo_sequence_algo_dosf(void)
{
    if (updating_menu_state != 0)
    {
        if (!Env::instance().referenceDsos().empty() && !Env::instance().distClasses().range.empty())
        {
            active_menu->flags &= ~D_DISABLED;
        }
        else
        {
            active_menu->flags |= D_DISABLED;
        }

        return D_O_K;
    }

    checkDosContr();
    benchmark::Params<DistOligoSequenceFinder> params = benchmark::dosfController_->config();

    if (params.haystack == nullptr)
    {
        return D_O_K;
    }

    benchmark::dosfController_->benchmark(params);
    return 0;
}
int do_benchmark_oligo_sequence_algo_css(void)
{
    if (updating_menu_state != 0)
    {
        if (!Env::instance().references().empty() && !Env::instance().distClasses().range.empty())
        {
            active_menu->flags &= ~D_DISABLED;
        }
        else
        {
            active_menu->flags |= D_DISABLED;
        }

        return D_O_K;
    }

    checkCssContr();
    benchmark::Params<chiSquareMethod::ChiSquareSearch> params = benchmark::cssController_->config();

    if (params.haystack == nullptr)
    {
        return D_O_K;
    }

    benchmark::cssController_->benchmark(params);
    return 0;
}
int do_benchmark_oligo_sequence_algo_basf(void)
{
    if (updating_menu_state != 0)
    {
        if (!Env::instance().references().empty() && !Env::instance().distClasses().range.empty())
        {
            active_menu->flags &= ~D_DISABLED;
        }
        else
        {
            active_menu->flags |= D_DISABLED;
        }

        return D_O_K;
    }

    checkBasContr();
    benchmark::Params<BasicFinder> params = benchmark::basfController_->config();

    if (params.haystack == nullptr)
    {
        return D_O_K;
    }

    benchmark::basfController_->benchmark(params);
    return 0;
}

int do_benchmark_basf_poor_results()
{
    if (updating_menu_state != 0)
    {
        if (!Env::instance().references().empty() && !Env::instance().distClasses().range.empty())
        {
            active_menu->flags &= ~D_DISABLED;
        }
        else
        {
            active_menu->flags |= D_DISABLED;
        }

        return D_O_K;
    }

    checkBasContr();
    int id = benchmark::basfController_->listPoorResults();

    if (id >= 0)
    {
        benchmark::basfController_->displayPoorResult(id);
    }

    return 0;
}
int do_benchmark_css_poor_results()
{
    if (updating_menu_state != 0)
    {
        if (!Env::instance().references().empty() && !Env::instance().distClasses().range.empty())
        {
            active_menu->flags &= ~D_DISABLED;
        }
        else
        {
            active_menu->flags |= D_DISABLED;
        }

        return D_O_K;
    }

    checkCssContr();
    int id = benchmark::cssController_->listPoorResults();

    if (id >= 0)
    {
        benchmark::cssController_->displayPoorResult(id);
    }

    return 0;
}

int do_benchmark_dosf_poor_results()
{
    if (updating_menu_state != 0)
    {
        if (!Env::instance().references().empty() && !Env::instance().distClasses().range.empty())
        {
            active_menu->flags &= ~D_DISABLED;
        }
        else
        {
            active_menu->flags |= D_DISABLED;
        }

        return D_O_K;
    }

    checkDosContr();
    int id = benchmark::dosfController_->listPoorResults();

    if (id >= 0)
    {
        benchmark::dosfController_->displayPoorResult(id);
    }

    return 0;
}
MENU *fingerprint_benchmark_plot_menu(void)
{
    static MENU menu[32];

    if (menu[0].text != NULL)
    {
        return menu;
    }

    add_item_to_menu(menu, "Benchmark - auto", do_auto_benchmark_from_file, NULL, 0, NULL);
    add_item_to_menu(menu, " ", NULL, NULL, 0, NULL);
    add_item_to_menu(menu, "Benchmark X^2 Search", do_benchmark_oligo_sequence_algo_css, NULL, 0, NULL);
    add_item_to_menu(menu, "Poor result X^2 Search", do_benchmark_css_poor_results, NULL, 0, NULL);
    add_item_to_menu(menu, "Dump json X^2 Search", do_benchmark_css_dump_json, NULL, 0, NULL);
    add_item_to_menu(menu, "Benchmark Dist Oligo Search", do_benchmark_oligo_sequence_algo_dosf, NULL, 0, NULL);
    add_item_to_menu(menu, "Poor result Dist Oligo Search", do_benchmark_dosf_poor_results, NULL, 0, NULL);
    add_item_to_menu(menu, "Dump json Dist Oligo Search", do_benchmark_dosf_dump_json, NULL, 0, NULL);
    add_item_to_menu(menu, "Benchmark Basic Search", do_benchmark_oligo_sequence_algo_basf, NULL, 0, NULL);
    add_item_to_menu(menu, "Poor result Basic Search", do_benchmark_basf_poor_results, NULL, 0, NULL);
    add_item_to_menu(menu, "Dump json Oligo Search", do_benchmark_basf_dump_json, NULL, 0, NULL);
    return menu;
}
