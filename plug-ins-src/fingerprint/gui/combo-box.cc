#include "combo-box.hh"

int generate_dos_combo_box(form_window_t *form, int *var, std::vector<std::shared_ptr<DistOligoSequence>>& vect)
{
    for (uint i = 0; i < vect.size(); ++i)
    {
        std::stringstream ss;
        ss << i << " - " << vect[i]->description();
        form_push_combo_option(form, ss.str().c_str(), var);
    }

    return 0;
}

int generate_combo_box(form_window_t *form, int *var, FingerprintVector& vect)
{
    for (uint i = 0; i < vect.size(); ++i)
    {
        std::stringstream ss;
        ss << i << " - " << vect[i]->description();
        form_push_combo_option(form, ss.str().c_str(), var);
    }

    return 0;
}
