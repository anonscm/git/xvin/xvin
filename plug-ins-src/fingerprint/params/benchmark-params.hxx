#include "../fingerprint-io.hh"
#include "../sequence/haystack-importer.hh"
template<typename T>
json benchmark::Params<T>::dumpJson() const
{
    json j;

    switch (aggregator)
    {
    case Aggregator::Step:
        j["aggr"] = "step";
        break;

    case Aggregator::HybCount:
        j["aggr"] = "hybCount";
        break;

    case Aggregator::None:
        j["aggr"] = "None";
    }

    j["samplesGenParamsPerStep"] = json::array();
    j["searchParamsPerStep"] = json::array();

    for (auto genParams : generatorParamsPerStep)
    {
        j["samplesGenParamsPerStep"].push_back(genParams.dumpJson());
    }

    for (auto searchParams : searchParamsPerStep)
    {
        j["searchParamsPerStep"].push_back(searchParams.dumpJson());
    }

    j["samplesPerStep"] = samplesPerStep;

    if (randomGenerated)
    {
        j["randSeed"] = randSeed;
    }
    else
    {
        j["locusInterval"] = locusInterval;
    }

    j["searchEngine"] = T::engine_name;
    j["mapGenom"] = mapGenom;
    j["truncateRes"] = truncateRes;
    j["keepScores"] = keepScores;
    j["infos"] = infos;

    if (!output.empty())
    {
        j["output"] = output;
    }

    if (!sequencePath.empty())
    {
        j["sequencePath"] = sequencePath;
    }

    if (oligoSetSize > 0)
    {
        j["oligos"] = oligo2json(oligoSet, oligoSetSize);
    }
    if (!oligoSetPath.empty())
    {
        j["oligoSetPath"] = oligoSetPath;
    }

    return j;
}

template<typename T>
bool benchmark::Params<T>::loadJson(json j)
{
    if (j["aggr"] == "step")
    {
        aggregator = Aggregator::Step;
        loadedJson_["aggr"] = j["aggr"];
    }
    else if (j["aggr"] == "hybCount")
    {
        aggregator = Aggregator::HybCount;
        loadedJson_["aggr"] = j["aggr"];
    }
    else if (j["aggr"] == "None")
    {
        aggregator = Aggregator::None;
        loadedJson_["aggr"] = j["aggr"];
    }
    else
    {
    }

    if (j["sampleGenParamsPerStep"].is_array())
    {
        for (auto genParams : j["sampleGenParamsPerStep"])
        {
            SampleGeneratorParams sgp;
            sgp.loadJson(genParams);
            generatorParamsPerStep.push_back(sgp);
        }
    }

    if (j["searchParamsPerStep"].is_array())
    {
        for (auto searchParams : j["searchParamsPerStep"])
        {
            typename T::params_type sp;
            sp.loadJson(searchParams);
            searchParamsPerStep.push_back(sp);
        }
    }

    if (j["samplesPerStep"].is_number())
    {
        samplesPerStep = j["samplesPerStep"];
    }

    if (j["randSeed"].is_number())
    {
        randomGenerated = true;
        randSeed = j["randSeed"];
    }
    else
    {
        randomGenerated = false;

        if (j["locusInterval"].is_number())
        {
            locusInterval = j["locusInterval"];
        }
    }

    if (j["mapGenom"].is_boolean())
    {
        mapGenom = j["mapGenom"];
    }
    else
    {
        if (j["samplesPerStep"].is_number())
        {
            samplesPerStep = j["samplesPerStep"];
        }
    }

    if (j["truncateRes"].is_boolean())
    {
        truncateRes = j["truncateRes"];
    }

    if (j["keepScores"].is_boolean())
    {
        keepScores = j["keepScores"];
    }

    if (j["infos"].is_string())
    {
        infos = j["infos"];
    }

    if (j["searchEngine"].is_string())
    {
        searchEngineName = j["searchEngine"];
        //if (searchEngineName == "DistOligoSequence")
        //{
        //    assert(SearchEngine == DistOligoSequenceFinder);
        //}
        //if (searchEngineName == "ChiSquare")
        //{
        //    assert(SearchEngine == chiSquare::ChiSquareSearch);
        //}
        //if (searchEngineName == "Basic")
        //{
        //    assert(SearchEngine == BasicFinder);
        //}
    }

    if (j["output"].is_string())
    {
        output = j["output"];
    }

    if (j["sequencePath"].is_string())
    {
        sequencePath = j["sequencePath"];
    }

    if (!j["oligos"].is_null())
    {
        json2oligo(j["oligos"], &oligoSet, &oligoSetSize);
    }
    else if (j["oligoSetPath"].is_string())
    {
        oligoSetPath = j["oligoSetPath"];
    }

    if (!sequencePath.empty() && (!oligoSetPath.empty() || oligoSetSize > 0))
    {
        importHaystack();
    }
    else
    {
        error_message("unable to load haystack");
        return false;
    }
    return true;
}

template<typename T>
bool benchmark::Params<T>::importHaystack()
{
    HaystackImporter<T> hi;

    hi.sequencePath = sequencePath;
    hi.oligoSet = oligoSet;
    hi.oligoSetSize = oligoSetSize;
    hi.oligoSetPath = oligoSetPath;

    haystack =  hi.importHaystack(true);
}
