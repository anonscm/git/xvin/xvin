#include "chisquare-search-params.hh"

ChiSquareSearchParams::ChiSquareSearchParams()
{
}

ChiSquareSearchParams::~ChiSquareSearchParams()
{
}
void ChiSquareSearchParams::generateMissingParams(PosFingerprint& dos)
{
}
json ChiSquareSearchParams::dumpJson(void)
{
    json j;
    j["nmBaseRateDist"]["avg"] = nmBaseRateDistribution.mean();
    j["nmBaseRateDist"]["stdev"] = nmBaseRateDistribution.standard_deviation();
    j["nmBaseRateDist"]["confidence"] = stretchingConfidenceLevel;
    j["noiseDist"]["stdev"] = expectedNoiseDeviation;
    j["noiseDist"]["confidence"] = noiseConfidenceLevel;
    j["minUmDegreeOfFreedom"] = minUmDegreeOfFreedom;
    j["minFoundRate"] = minFoundRate;
    j["chiSquarePValue"] = gammaSignificanceLevel;
    return j;
}

json ChiSquareSearchParams::dumpLoadedJson() const
{
    return loadedJson_;
}

void ChiSquareSearchParams::loadJson(json jso)
{
    if (jso["nmBaseRateDist"].is_object())
    {
        float nmmean = jso["nmBaseRateDist"]["avg"];
        float nmstdev = jso["nmBaseRateDist"]["stdev"];
        nmBaseRateDistribution = NmBaseRateDistribution(nmmean, nmstdev);
        stretchingConfidenceLevel = jso["nmBaseRateDist"]["confidence"];
    }

    if (jso["noiseDist"].is_object())
    {
        expectedNoiseDeviation = jso["noiseDist"]["stdev"];
        noiseConfidenceLevel = jso["noiseDist"]["confidence"];
    }

    if (jso["minUmDegreeOfFreedom"].is_number())
    {
        minUmDegreeOfFreedom = jso["minUmDegreeOfFreedom"];
    }

    if (jso["minFoundRate"].is_number_float())
    {
        minFoundRate = jso["minFoundRate"];
    }

    if (jso["gammaSignificanceLevel"].is_number_float())
    {
        gammaSignificanceLevel = jso["gammaSignificanceLevel"];
    }

    if (jso["direction"].is_string())
    {
        if (strcmp(jso["direction"].get<std::string>().c_str(), "forward") != 0
                &&  strcmp(jso["direction"].get<std::string>().c_str(), "both") != 0)
        {
            dirForward = false;
        }
        if (strcmp(jso["direction"].get<std::string>().c_str(), "backward") != 0
                &&  strcmp(jso["direction"].get<std::string>().c_str(), "both") != 0)
        {
            dirBackward = false;
        }
    }

    loadedJson_ = jso;
}
