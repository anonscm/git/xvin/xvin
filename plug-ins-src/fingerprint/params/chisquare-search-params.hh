#pragma once
#include "../sequence/pos-fingerprint.hh"
#include "../define.hh"

struct ChiSquareSearchParams
{
  public:
    ChiSquareSearchParams();
    virtual ~ChiSquareSearchParams();

    NmBaseRateDistribution nmBaseRateDistribution = NmBaseRateDistribution(AVG_NM_BASE_RATE, STDDEV_NM_BASE_RATE);
    double stretchingConfidenceLevel = STRETCHING_CONFIDENCE;
    int minUmDegreeOfFreedom = 2;

    double minFoundRate = 3. / 5.;

    /**
     * @brief Expected noise stddeviation
     */
    double expectedNoiseDeviation = NOISE_DEVIATION;

    double noiseConfidenceLevel = NOISE_CONFIDENCE;

    /**
     * @brief significance level allowed to select good candidat solutions
     */
    double gammaSignificanceLevel = 0.05;

    bool dirBackward = true;
    bool dirForward = true;

    json dumpJson(void);
    void loadJson(json j);
    json dumpLoadedJson() const;
    void generateMissingParams(PosFingerprint& dos);
  private:
    json loadedJson_;
};
