#include "dist-oligo-search-params.hh"

DistOligoSearchParams::DistOligoSearchParams()
{
    //buildDefaultScoreMatrix();
    //build2DExpScoreMatrix();
    //buildSumMatrix(); // TODO check
}
DistOligoSearchParams::~DistOligoSearchParams()
{
}

json DistOligoSearchParams::dumpJson()
{
    json j;
    j["distClass"] = json::array();

    for (uint i = 0; i < distClasses.range.size(); ++i)
    {
        j["distClass"][i].push_back({(distClasses.range)[i].first, (distClasses.range)[i].second});
    }

    j["scoreMatrix"] = json::array();

    for (int i = 0; i < scoreMatrix.rows(); ++i)
    {
        j["scoreMatrix"][i] = json::array();

        for (int k = 0; k < scoreMatrix.cols(); ++k)
        {
            j["scoreMatrix"][i][k] = (scoreMatrix)(i, k);
        }
    }

    j["sumMatrix"] = json::array();

    for (int i = 0; i < sumMatrix.rows(); ++i)
    {
        j["sumMatrix"][i] = json::array();

        for (int k = 0; k < sumMatrix.cols(); ++k)
        {
            j["sumMatrix"][i][k] = sumMatrix(i, k);
        }
    }

    j["allowGap"] = allowGap;
    j["maxGapSize"] = maxGapSize;
    j["globalMaxGapSize"] = globalMaxGapSize;
    j["gapCost"] = gapCost;
    j["rejectThreshold"] = rejectThreshold;
    j["acceptThreshold"] = acceptThreshold;
    j["distClassBaseNmRate"] = distClassBaseNmRate;

    if (dirForward)
    {
        if (dirBackward)
        {
            j["direction"] = "both";
        }
        else
        {
            j["direction"] = "forward";
        }
    }
    else if (dirBackward)
    {
            j["direction"] = "backward";
    }
    else
    {
            j["direction"] = "none";
    
    }
    

    return j;
}
json DistOligoSearchParams::dumpLoadedJson() const
{
    return loadedJson_;
}

void DistOligoSearchParams::loadJson(json j)
{
    if (j["distClass"].is_array())
    {
        error_message("disabled distClass loading");
        // distClassVector.clear();
        // for (uint i = 0; i < j["distClass"].size(); ++i)
        // {
        //     distClassVector.push_back(std::pair<int, int>(j["distClass"][i]["first"], j["distClass"][i]["second"]));
        // }
    }

    if (j["scoreMatrix"].is_array())
    {
        scoreMatrix.resize(j["scoreMatrix"].size(), j["scoreMatrix"].size());

        for (uint i = 0; i < j["scoreMatrix"].size(); ++i)
        {
            for (uint k = 0; k < j["scoreMatrix"][i].size(); ++k)
            {
                scoreMatrix(i, k) = j["scoreMatrix"][i][k];
            }
        }
    }

    if (j["sumMatrix"].is_array())
    {
        warning_message("sumMatrix loading not work");
    }

    if (j["allowGap"].is_boolean())
    {
        allowGap = j["allowGap"];
    }
    else if (j["merge"].is_boolean())
    {
        allowGap = j["merge"];
    }
    else
    {
        error_message("Wrong allowGap value");
    }

    if (j["maxGapSize"].is_number_integer())
    {
        maxGapSize = j["maxGapSize"];
    }
    else if (j["maxGapSize"].is_number_integer())
    {
        maxGapSize = j["maxGapSize"];
    }
    else
    {
        error_message("Wrong maxGapSize value");
    }

    if (j["gapCost"].is_number_integer())
    {
        gapCost = j["gapCost"];
    }

    if (j["globalMaxGapSize"].is_number_integer())
    {
        globalMaxGapSize = j["globalMaxGapSize"];
    }

    rejectThreshold = j["rejectThreshold"];
    acceptThreshold = j["acceptThreshold"];

    if (j["distClassBaseNmRate"].is_number_float())
    {
        distClassBaseNmRate = j["distClassBaseNmRate"];
    }

    if (j["direction"].is_string())
    {
        dirForward = (strcmp(j["direction"].get<std::string>().c_str(), "forward") == 0
                ||  strcmp(j["direction"].get<std::string>().c_str(), "both") == 0);

        dirBackward = (strcmp(j["direction"].get<std::string>().c_str(), "backward") == 0
                ||  strcmp(j["direction"].get<std::string>().c_str(), "both") == 0);
    }

    loadedJson_ = j;
}

void DistOligoSearchParams::buildDefaultScoreMatrix()
{
    int size = distClasses.range.size();
    scoreMatrix = ScoreMatrix<float>(size, size);

    for (int i = 0; i < (int) size; ++i)
    {
        for (int j = 0; j < (int) size; ++j)
        {
            int gap = abs(i - j);
            (scoreMatrix)(i, j) = 1 / std::pow(2, gap);
        }
    }

    //std::cout << *scoreMatrix_ << std::endl;
}

void DistOligoSearchParams::build2DExpScoreMatrix()
{
    int size = distClasses.range.size();
    scoreMatrix = ScoreMatrix<float>(size, size);

    for (int i = 0; i < (int) distClasses.range.size(); ++i)
    {
        for (int j = 0; j < (int) distClasses.range.size(); ++j)
        {
            scoreMatrix(i, j) = 1;

            if (i != j)
            {
                int gap = abs(i - j) * 2 - 1;
                scoreMatrix(i, j) = 1 / pow(2, gap);
                scoreMatrix(i, j) += 1 / pow(2, ((i + j) / 2) + 1);
            }
        }
    }

    //std::cout << *scoreMatrix_ << std::endl;
}

void DistOligoSearchParams::buildSumMatrix()
{
    int size = distClasses.range.size();
    sumMatrix = DistClassSumMatrix<char>(size, size);
    //TO REMOVE
//    for (uint i = 0; i < distClasses.range.size(); ++i)
//    {
//        for (uint j = 0; j < distClasses.range.size(); ++j)
//        {
//            int dist = (distClasses.range[i].first + (distClasses.range)[i].second) / 2
//                       + (distClassVector[j].first + distClass.range[j].second) / 2;
//            bool found = false;
//
//            for (int k = 0; k < (int) distClassVector.size() && !found; ++k)
//            {
//                auto interval = distClassVector[k];
//
//                if (dist >= interval.first && dist <= interval.second)
//                {
//                    sumMatrix(i, j) =  k;
//                    found = true;
//                }
//            }
//
//            if (!found)
//            {
//                sumMatrix(i, j) = distClassVector.size() - 1;
//            }
//        }
//    }
}

void DistOligoSearchParams::generateMissingParams(DistOligoSequence& dos)
{
    if (distClasses.range.size() == 0)
    {
        distClasses = dos.distClasses();
    }

    if (scoreMatrix.size() == 0)
    {
        buildDefaultScoreMatrix();
    }

    if (sumMatrix.size() == 0)
    {
        buildSumMatrix();
    }
}
