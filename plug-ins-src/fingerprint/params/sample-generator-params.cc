#include "sample-generator-params.hh"
#include "../fingerprint-io.hh"

SampleGeneratorParams::SampleGeneratorParams()
{
}

SampleGeneratorParams::~SampleGeneratorParams()
{
}


void SampleGeneratorParams::loadJson(json j)
{
    float sizeAvg = j["sizeDist"]["avg"];
    float sizeStDev = j["sizeDist"]["stdev"];
    sizeDistribution = SizeDistribution(sizeAvg, sizeStDev);
    float noiseAvg = j["noiseDist"]["avg"];
    float noiseStDev = j["noiseDist"]["stdev"];
    noiseDistribution = NoiseDistribution(noiseAvg, noiseStDev);
    float baseNmRateAvg = j["baseNmRateDist"]["avg"];
    float baseNmRateStDev = j["baseNmRateDist"]["stdev"];
    baseNmRateDistribution = BaseNmRateDistribution(baseNmRateAvg, baseNmRateStDev);

    if (j["missingHybDist"].is_object())
    {
        int missingHybCycleCount = j["missingHybDist"]["cycleCount"];
        double missingHybProb = j["missingHybDist"]["hybProba"];
        missingHybDistribution = MissingHybDistribution(missingHybCycleCount, missingHybProb);
        missingHybDetectionThreshold = j["missingHybDist"]["thres"];
    }

    if (j["orientDist"].is_object())
    {
        directionDistribution = DirectionDistribution(j["orientDist"]["proba"].get<float>());
    }

    //if (j["orientDist"].is_object() && j["orientDist"]["proba"].is_number_float())
    //{
    //    double directionProb = j["orientDist"]["proba"];
    //    directionDistribution = DirectionDistribution(directionProb);
    //}
    //if (j["minHyb"].is_number_integer())
    //{
    //    minHybridization = j["minHyb"];
    //}
    loadedJson_ = j;

    if (!j["oligos"].is_null())
    {
        json2oligo(j["oligos"], &oligoSet, &oligoSetSize);
    }
}

json SampleGeneratorParams::dumpJson() const
{
    json j;
    j["sizeDist"]["avg"] = sizeDistribution.mean();
    j["sizeDist"]["stdev"] = sizeDistribution.stddev();
    j["noiseDist"]["avg"] = noiseDistribution.mean();
    j["noiseDist"]["stdev"] = noiseDistribution.stddev();
    j["baseNmRateDist"]["avg"] = baseNmRateDistribution.mean();
    j["baseNmRateDist"]["stdev"] = baseNmRateDistribution.stddev();
    j["missingHybDist"]["cycleCount"] = missingHybDistribution.t();
    j["missingHybDist"]["hybProba"] = missingHybDistribution.p();
    j["missingHybDist"]["thres"] = missingHybDetectionThreshold;
    j["orientDist"]["proba"] = directionDistribution.p();
    j["minHyb"] = minHybridization;

    if (oligoSetSize > 0)
    {
        j["oligos"] = oligo2json(oligoSet, oligoSetSize);
    }

    return j;
}
json SampleGeneratorParams::dumpLoadedJson() const
{
    return loadedJson_;
}
