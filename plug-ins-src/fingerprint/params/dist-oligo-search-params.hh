#pragma once
#include "../define.hh"
#include "../sequence/dist-oligo-sequence.hh"

struct DistOligoSearchParams
{
  public:
    DistOligoSearchParams();
    virtual ~DistOligoSearchParams();

    DistClasses distClasses;
    ScoreMatrix<float> scoreMatrix;
    DistClassSumMatrix<char> sumMatrix;
    std::vector<char> distClassMapping;
    bool allowGap = true;
    float gapCost = -1;
    int maxGapSize = 1;
    int globalMaxGapSize = 6;
    float rejectThreshold = 0.5;
    float acceptThreshold = 0.8;
    float distClassBaseNmRate = 1;

    bool dirForward = true;
    bool dirBackward = true;

    void buildDefaultScoreMatrix();
    void build2DExpScoreMatrix();
    void buildSumMatrix();

    json dumpJson(void);

    void loadJson(json j);
    json dumpLoadedJson() const;
    void generateMissingParams(DistOligoSequence& dos);
  private:
    json loadedJson_;
};

