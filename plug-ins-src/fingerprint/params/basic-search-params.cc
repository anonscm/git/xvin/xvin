#include "basic-search-params.hh"

void BasicSearchParams::generateMissingParams(DistFingerprint& fgp)
{
}
json BasicSearchParams::dumpJson(void)
{
    json j;

    j["maxSearchHybridationError"] = maxSearchHybridationError;
    j["noiseDeviation"] = noiseDeviation;
    j["localSearchPrecision"] = localSearchPrecision;
    j["skipMissingOligoInHaystack"] = skipMissingOligoInHaystack;
    j["skipMissingOligoInNeedle"] = skipMissingOligoInNeedle;

    return j;
}

void BasicSearchParams::loadJson(json j)
{
    maxSearchHybridationError = j["maxSearchHybridationError"];
    noiseDeviation = j["noiseDeviation"];
    localSearchPrecision = j["localSearchPrecision"];
    skipMissingOligoInHaystack = j["skipMissingOligoInHaystack"];
    skipMissingOligoInNeedle = j["skipMissingOligoInNeedle"];

    loadedJson_ = j;
}


BasicSearchParams::BasicSearchParams()
{
}

BasicSearchParams::~BasicSearchParams()
{
}

json BasicSearchParams::dumpLoadedJson() const
{
    return loadedJson_;
}
