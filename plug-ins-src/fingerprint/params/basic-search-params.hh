#pragma once
#include "../define.hh"
#include "../sequence/dist-fingerprint.hh"

class BasicSearchParams
{
  public:
    BasicSearchParams();
    virtual ~BasicSearchParams();

    /**
     * @brief
     */
    double maxSearchHybridationError = MAX_HYBRIDATION_ERROR;

    /**
     * @brief
     */
    double noiseDeviation = NOISE_DEVIATION;
    double localSearchPrecision = NOISE_DEVIATION * 3;


    bool skipMissingOligoInHaystack = SKIP_MISSING_IN_HAYSTACK;
    bool skipMissingOligoInNeedle = SKIP_MISSING_IN_NEEDLE;
    /**
     * @brief the minimum possible stretching of DNA with picoSeq
     */
    double minNmBaseRate = MIN_NM_BASE_RATE;
    /**
     * @brief the maximum possible stretching of DNA with picoSeq
     */
    double maxNmBaseRate = MAX_NM_BASE_RATE;

    json dumpJson(void);
    void loadJson(json j);
    json dumpLoadedJson() const;
    void generateMissingParams(DistFingerprint& dos);
  private:
    json loadedJson_;
};
