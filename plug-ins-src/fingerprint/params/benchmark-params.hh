#pragma once
#ifndef XV_WIN32
# include "config.h"
#endif
#include "../sequence/fingerprint.hh"
#include "../sequence/dist-oligo-sequence.hh"
#include "../search/abstract-search.hh"
#include "../define.hh"
#include<memory>
#include "json.hpp"
using json = nlohmann::json;

namespace benchmark
{
enum Aggregator
{
    Step = 0,
    HybCount = 1,
    None = 2
};

template<typename SearchEngine>
struct Params
{
    Aggregator aggregator = Aggregator::None;
    std::shared_ptr<typename SearchEngine::input_type> haystack;

    std::vector<SampleGeneratorParams> generatorParamsPerStep;
    std::vector<typename SearchEngine::params_type> searchParamsPerStep;

    int samplesPerStep = 1500;
    bool randomGenerated = false;
    int randSeed = 42;
    int locusInterval = 2000;

    bool mapGenom = true;
    bool truncateRes = true;
    bool keepScores = true;

    std::string infos = "";
    std::string searchEngineName = "";
    std::string output = "";
    std::string sequencePath = "";
    oligo_t **oligoSet = nullptr;
    int oligoSetSize = 0;
    std::string oligoSetPath = "";

    // Not dumped
    bool keepAllResults = false;
    bool getFullData = false;

    json dumpJson() const;
    bool loadJson(json j);
    json dumpLoadedJson() const;

    bool importHaystack();


    private:
    json loadedJson_;
};
}

#include "benchmark-params.hxx"
