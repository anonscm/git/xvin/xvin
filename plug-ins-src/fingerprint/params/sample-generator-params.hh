#pragma once
#include "../define.hh"
#include "json.hpp"
using json = nlohmann::json;

class SampleGeneratorParams
{
  public:
    SampleGeneratorParams();
    virtual ~SampleGeneratorParams();

    SizeDistribution sizeDistribution = SizeDistribution(3000, 1500);
    NoiseDistribution noiseDistribution = NoiseDistribution(0, NOISE_DEVIATION);
    BaseNmRateDistribution baseNmRateDistribution = BaseNmRateDistribution(AVG_BASE_NM_RATE, STDDEV_BASE_NM_RATE);
    MissingHybDistribution missingHybDistribution = MissingHybDistribution(100, 0.2);
    AddedHybDistribution addedHybDistribution = AddedHybDistribution(0.5);
    DirectionDistribution directionDistribution = DirectionDistribution(1);
    oligo_t **oligoSet = nullptr;
    int oligoSetSize = 0;
    int minHybridization = 0;
    double missingHybDetectionThreshold = 0;


    void loadJson(json j);
    json dumpLoadedJson() const;
    json dumpJson() const;
    

  private:
    json loadedJson_;
};
