#pragma once
#include "../define.hh"
class SampleGenerationParams
{
public:
    SampleGenerationParams ();
    virtual ~SampleGenerationParams ();
  
    std::string fromDesc = ""; 
    int size = 0; 
    int locus = 0;
    NoiseDistribution noiseDistribution = NoiseDistribution(0, NOISE_DEVIATION);
    float baseNmRate = 1.;
    bool isReverse = false;
    int missingHybs = 0;
};
