/**
 * @file define.hh
 * @brief Plugin wild constants, structs, enum, typedef.
 * @author François-Xavier Lyonnet du Moutier
 */

#pragma once
#ifndef XV_WIN32
# include "config.h"
#endif

#define AVG_BASE_NM_RATE 0.88
#define STDDEV_BASE_NM_RATE 0.04
#define AVG_NM_BASE_RATE 1 / AVG_BASE_NM_RATE
#define STDDEV_NM_BASE_RATE STDDEV_BASE_NM_RATE
#define STRETCHING_CONFIDENCE 0.99
#define NOISE_CONFIDENCE 0.999

#define MIN_BASE_NM_RATE AVG_BASE_NM_RATE - 2.9 * STDDEV_BASE_NM_RATE 
#define MAX_BASE_NM_RATE AVG_BASE_NM_RATE + 2.9 * STDDEV_BASE_NM_RATE
#define MIN_NM_BASE_RATE 1 / MAX_BASE_NM_RATE
#define MAX_NM_BASE_RATE 1 / MIN_BASE_NM_RATE



//#define MIN_STRETCHING_RATE 1
//#define MAX_STRETCHING_RATE 1
#define MAX_HYBRIDATION_ERROR 0.05
#define SKIP_MISSING_IN_HAYSTACK 0
#define SKIP_MISSING_IN_NEEDLE 0
#define THEORICAL_STANDARD_DEVIATION 3.0
#define NOISE_DEVIATION THEORICAL_STANDARD_DEVIATION
#define RANKING_ITERATION 4
#define MIN_CORRELATED_PEAK 3
#define MAX_DISTANCE_BETWEEN_PEAK 3000

#undef PS // CORRECT INCLUDES ERRORS
#include "sequence/bidirectional-oligo.hh"
#include "sequence/hybridation.hh"
#include "results/search-match.hh"
#include "results/dist-oligo-solution.hh"
#include "results/candidate-solution.hh"
//#include <boost/flyweight.hpp>
//#include <boost/flyweight/key_value.hpp>
//#include <boost/flyweight/no_racking.hpp>
//#include <boost/flyweight/set_factory.hpp>
#include <map>
#include <unordered_set>
#include <vector>
#include <string>
#include <random>
#include <boost/bimap.hpp>
#include <boost/icl/split_interval_set.hpp>
#include <boost/icl/split_interval_map.hpp>
#include <boost/icl/separate_interval_set.hpp>
#include <boost/icl/interval_map.hpp>
#include <boost/serialization/unordered_set.hpp>
#include <boost/math/distributions/normal.hpp>

#include <Eigen/Dense>

enum FgpRepr
{
    FGP_REPR_POS = 0,
    FGP_REPR_DIST = 1
};
enum FgpList
{
    FGP_LIST_HAYSTACK,
    FGP_LIST_NEEDLE
};
enum ReverseSetting
{
    REV_SETTING_FORBID,
    REV_SETTING_ALLOW,
    REV_SETTING_FORCE
};
//typedef boost::flyweights::flyweight<boost::flyweights::key_value<int, BiDirOligo, OligoKeyExtractor<BiDirOligo>>, boost::flyweights::no_tracking> Oligo;
typedef std::unordered_set<Oligo> OligoSet;
typedef unsigned int uint;
/**
 * @brief position / hybridization map
 */
typedef std::map<int, Hybridation> HybridationMap;

/**
 * @brief Position Hybridization pair
 */
typedef std::pair<int, Hybridation> HybridationPair;
/**
 * @brief Vector of Hybridization
 */
typedef std::vector<Hybridation> HybridationVector;
typedef std::pair<double, int> StretchingPair;
typedef std::vector<StretchingPair> StretchingVector;


/**
 * @brief A set of SearchMatch
 */
typedef std::set<SearchMatch> SearchMatchSet;
typedef std::vector<int> PositionVector;
typedef std::pair<int, int> DistClass;
//typedef std::vector<DistClass> DistClassVector;
typedef struct {
    std::vector<DistClass> range;
    std::vector<char> map;
    float maxFractionInClass;
    float minBaseNmRate;
    float maxBaseNmRate;
    float classWidthMultiplifier;
    int maxDistance;
} DistClasses;


template<typename T> using DistMatrix = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>;
template<typename T> using RankMatrix = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>;
template<typename T> using ScoreMatrix = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>;
template<typename T> using DistClassSumMatrix = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>;

typedef std::pair<const Oligo, const Oligo> OligoPair;
//typedef boost::bimap< std::string, int > DistRangeBimap;
//typedef DistRangeBimap::value_type DistRange;
typedef boost::icl::discrete_interval<int> DistInterval;
typedef boost::icl::interval_map<int, std::set<std::pair<int,int>>> DistIntervalSet;
typedef std::map<OligoPair, DistIntervalSet> OligoPosDistMap;

template<typename SolutionType> using SearchSolutions = std::vector<SolutionType>;

typedef SearchSolutions<DistOligoSolution> DistOligoSolutions;
typedef std::vector<CandidateSolution> CandidateSolutions;

typedef std::default_random_engine UniformRandomEngine;
typedef std::normal_distribution<float> NoiseDistribution;
typedef std::uniform_int_distribution<int> LocusDistribution;
typedef std::normal_distribution<double> BaseNmRateDistribution;
typedef boost::math::normal_distribution<double> NmBaseRateDistribution;
typedef std::normal_distribution<float> SizeDistribution;
typedef std::binomial_distribution<int> MissingHybDistribution;
typedef std::poisson_distribution<int> AddedHybDistribution;
typedef std::bernoulli_distribution DirectionDistribution;


typedef std::map<char, Oligo> CharOligoMap;


namespace chiSquareMethod
{
    typedef std::vector<std::vector<int>> MatchingHybVector;
    typedef std::vector<std::pair<int,int>> MatchingPairVector;
    typedef std::pair<double, double> LinearParams;
} /* chiSquareMethod */ 

namespace benchmark
{
}
