#pragma once

int unit_test_load_seq_and_oligo(const char *seq_file, const char *oligo_file);
int unit_test_import_posfingerprint_from_fasta_and_oligo_and_needle(const char *fingerprint_file, const char *needle_file);
int unit_test_create_dist_class_and_convert_fingerprints();
