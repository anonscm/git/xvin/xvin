#include "api.hh"
#include "progress_builder.h"
#include "../boxplot/boxplot.hh"
#include "../stat/stat.h"

using namespace benchmark;

//TODO boxplot !
//int benchmark_oligo_sequence_algo(BenchmarkGenerator& bengen, O_p *op, FingerprintVector* poorResults)
//{
//    char buf[1024] = {0};
//    progress_window_t *progress = NULL;
//    d_s *boxplot_ds = NULL;
//    d_s *notfound_ds = NULL;
//    boxplot_ds = create_and_attach_one_ds(op, bengen.lastStep(), bengen.lastStep(), 0);
//    alloc_data_set_y_error(boxplot_ds);
//    alloc_data_set_y_down_error(boxplot_ds);
//    alloc_data_set_y_box(boxplot_ds);
//    notfound_ds = create_and_attach_one_ds(op, bengen.lastStep(), bengen.lastStep(), 0);
//    boxplot_ds->nx = 0;
//    notfound_ds->nx = 0;
//    set_ds_source(boxplot_ds, "Boxs");
//    set_ds_source(notfound_ds, "Not found");
//    boxplot_ds->boxplot_width = (bengen.stepValue(bengen.lastStep()) - bengen.stepValue(0)) / (bengen.lastStep() * 2);
//    progress = progress_create("Benchmark progress");
//    progress_run(progress);
//
//    if (poorResults)
//    {
//        bengen.poorResults(poorResults);
//    }
//
//    for (int i = 0; i < bengen.lastStep(); ++i)
//    {
//        int not_found_count = 0;
//        int found_count = 0;
//        int cur_min = INT_MAX;
//        int cur_max = INT_MIN;
//        float cur_sum = 0;
//        float cur_sq_sum = 0;
//        snprintf(buf, sizeof(buf), "current step %f", bengen.stepValue(i));
//        progress_set_info_text(progress, buf);
//
//        //#pragma omp parallel for
//        for (int j = 0; j < bengen.samplesPerStep(); ++j)
//        {
//            DistSearchResult *result = 0;
//            result = bengen.run(i, j);
//            int cur_val = result->firstTruePositiveRank();
//
//            if (cur_val == -1)
//            {
//                not_found_count++;
//            }
//            else
//            {
//                cur_min = cur_min > cur_val ? cur_val : cur_min;
//                cur_max = cur_max < cur_val ? cur_val : cur_max;
//                cur_sum += cur_val;
//                cur_sq_sum += std::pow(cur_val, 2);
//            }
//
//            progress_set_fraction(progress, (i * bengen.samplesPerStep() + j) / (float)(bengen.lastStep() *
//                                  bengen.samplesPerStep()));
//
//            if (progress_is_canceled(progress))
//            {
//                progress_free(progress);
//                return -1;
//            }
//        }
//
//        found_count = bengen.samplesPerStep() - not_found_count;
//        boxplot_ds->xd[i] = bengen.stepValue(i);
//        boxplot_ds->yd[i] = cur_sum / (found_count);
//        boxplot_ds->ybu[i] = std::sqrt((cur_sq_sum / found_count - std::pow(cur_sum / found_count, 2)));
//        boxplot_ds->ybd[i] = boxplot_ds->ybu[i];
//        boxplot_ds->ye[i] = cur_max - boxplot_ds->yd[i];
//        boxplot_ds->yed[i] = boxplot_ds->yd[i] - cur_min;
//        boxplot_ds->nx++;
//        notfound_ds->xd[i] = bengen.stepValue(i);
//        notfound_ds->yd[i] = not_found_count;
//        notfound_ds->nx++;
//        op->need_to_refresh = ALL_NEED_REFRESH;
//    }
//
//    progress_free(progress);
//    progress = NULL;
//    return 0;
//}







//int benchmark_rank_per_hyb_count_prepare(BenchmarkGenerator *bengen, O_p *data_op,O_p *timing_op, int sequence_size)
//{
//    d_s *ds = NULL;
//    d_s *timing_per_nbhyb_ds = NULL;
//
//    d_s *not_found_ds = NULL;
//    d_s *count_ds = NULL;
//    d_s *not_found_count_ds = NULL;
//    int samples_count = bengen->lastStep() * bengen->samplesPerStep();
//
//    if (data_op->n_dat > 1)
//    {
//        count_ds = build_adjust_data_set(data_op->dat[1], sequence_size, sequence_size);
//        count_ds->nx = sequence_size;
//        count_ds->ny = sequence_size;
//    }
//    else
//    {
//        count_ds = create_and_attach_one_ds(data_op, sequence_size, sequence_size, 0);
//    }
//
//    if (data_op->n_dat > 2)
//    {
//        not_found_ds = build_adjust_data_set(data_op->dat[2], samples_count, samples_count);
//        not_found_ds->nx = samples_count;
//        not_found_ds->ny = samples_count;
//    }
//    else
//    {
//        not_found_ds = create_and_attach_one_ds(data_op, samples_count,  samples_count, 0);
//    }
//
//    if (data_op->n_dat > 3)
//    {
//        not_found_count_ds = build_adjust_data_set(data_op->dat[3], sequence_size, sequence_size);
//        not_found_count_ds->nx = sequence_size;
//        not_found_count_ds->ny = sequence_size;
//    }
//    else
//    {
//        not_found_count_ds = create_and_attach_one_ds(data_op, sequence_size, sequence_size, 0);
//    }
//
//
//    ds = build_adjust_data_set(data_op->dat[0], samples_count, samples_count);
//    ds->nx = samples_count;
//    ds->ny = samples_count;
//
//
//    timing_per_nbhyb_ds = build_adjust_data_set(timing_op->dat[0], samples_count, samples_count);
//    timing_per_nbhyb_ds->nx = samples_count;
//    timing_per_nbhyb_ds->ny = samples_count;
//
//    set_ds_source(ds, "Solution rank per hybridization count");
//    set_ds_source(not_found_ds, "not found solution rank per hybridization count");
//    set_ds_source(count_ds, "count of found solution per hybridations count");
//    set_ds_source(not_found_count_ds, "count of not found solution per hybridations count");
//    set_plot_x_title(data_op, "nb hybridation");
//    set_plot_y_title(data_op, "rank of the solution");
//    set_ds_dot_line(ds);
//    set_ds_point_symbol(ds, "\\fd");
//    set_ds_dot_line(not_found_ds);
//    set_ds_point_symbol(not_found_ds, "\\fd");
//
//    un_s *un = build_unit_set(IS_SECOND, 1, 1, IS_NANO, IS_T_UNIT_SET, "ns");
//
//    change_decade(un, IS_MILLI);
//
//    add_y_unit_set_to_op(timing_op, un);
//
//    set_op_y_unit_set(timing_op, 1); 
//    set_ds_source(timing_per_nbhyb_ds, "Timing");
//    set_plot_x_title(timing_op, "nb hybridation");
//    set_plot_y_title(timing_op, "time");
//    set_ds_dot_line(timing_per_nbhyb_ds);
//    set_ds_point_symbol(timing_per_nbhyb_ds, "\\fd");
//
//    for (int i = 0; i < sequence_size; ++i)
//    {
//        count_ds->xd[i] = i;
//        count_ds->yd[i] = 0;
//        not_found_count_ds->xd[i] = i;
//        not_found_count_ds->yd[i] = 0;
//    }
//
//    return 0;
//}
//
//int benchmark_rank_per_hyb_count(BenchmarkGenerator *bengen, O_p *data_op,O_p *timing_op, int sequence_size, bool use_quartile,
//                                 FingerprintVector *poorResults)
//{
//    d_s *ds = NULL;
//    d_s *count_ds = NULL;
//    d_s *not_found_ds = NULL;
//    d_s *not_found_count_ds = NULL;
//    d_s *timing_ds = NULL;
//    char buf[1024] = {0};
//    benchmark_rank_per_hyb_count_prepare(bengen, data_op, timing_op, sequence_size);
//    ds = data_op->dat[0];
//    count_ds = data_op->dat[1];
//    not_found_ds = data_op->dat[2];
//    not_found_count_ds = data_op->dat[3];
//    timing_ds = timing_op->dat[0];
//    progress_window_t *progress = progress_create("Global Progress");
//    progress_run(progress);
//    int maxHybridationCount = 0;
//    bengen->poorResults(poorResults);
//    int cur_ds_idx = 0;
//    int cur_not_found_ds_idx = 0;
//
//    for (int i = 0; i < bengen->lastStep(); ++i)
//    {
//        for (int j = 0; j < bengen->samplesPerStep(); ++j)
//        {
//            //progress_set_info_text(progress, buf);
//            progress_set_fraction(progress, (i * bengen->samplesPerStep() + j) / ((float)(bengen->lastStep() *
//                                  bengen->samplesPerStep())));
//            DistSearchResult *result = bengen->run(i, j);
//            int cur_rank = result->firstTruePositiveRank();
//            const std::shared_ptr<Fingerprint> cur_fgp = bengen->lastFingerprint();
//            int cur_hyb_count = cur_fgp->nbHybridation();
//
//            if (cur_rank >= 0)
//            {
//                ds->xd[cur_ds_idx] = cur_hyb_count;
//                ds->yd[cur_ds_idx] = cur_rank;
//                count_ds->yd[cur_hyb_count] += 1;
//                cur_ds_idx++;
//            }
//            else
//            {
//                not_found_ds->xd[cur_not_found_ds_idx] = cur_hyb_count;
//                not_found_ds->yd[cur_not_found_ds_idx] = cur_rank;
//                not_found_count_ds->yd[cur_hyb_count] += 1;
//                cur_not_found_ds_idx++;
//            }
//
//            timing_ds->xd[i * bengen->samplesPerStep() + j ] = cur_hyb_count;
//            timing_ds->yd[i * bengen->samplesPerStep() + j ] = std::chrono::duration_cast<std::chrono::nanoseconds> (result->duration()).count();
//
//            if (cur_hyb_count > maxHybridationCount)
//            {
//                maxHybridationCount = cur_hyb_count;
//            }
//
//            if (progress_is_canceled(progress))
//            {
//                break;
//            }
//        }
//
//        if (progress_is_canceled(progress))
//        {
//            break;
//        }
//    }
//
//    ds->nx = cur_ds_idx;
//    ds->ny = cur_ds_idx;
//
//    not_found_ds->nx = cur_not_found_ds_idx;
//    not_found_ds->ny = cur_not_found_ds_idx;
//
//    boxplot_from_ds(data_op, 0, NULL, use_quartile);
//    boxplot_from_ds(timing_op, 0, NULL, use_quartile);
//    histogram_vc(NULL, timing_op, timing_ds, 0, 1, 0, 0);
//    progress_free(progress);
//    count_ds->nx = maxHybridationCount + 1;
//    count_ds->ny = maxHybridationCount + 1;
//    not_found_count_ds->nx = maxHybridationCount + 1;
//    not_found_count_ds->ny = maxHybridationCount + 1;
//    // BUGGY
//    //build_adjust_data_set(sum_ds, maxHybridationCount + 1, maxHybridationCount + 1);
//    //build_adjust_data_set(count_ds, maxHybridationCount + 1, maxHybridationCount + 1);
//    //build_adjust_data_set(not_found_count_ds, maxHybridationCount + 1, maxHybridationCount + 1);
//    data_op->need_to_refresh = ALL_NEED_REFRESH;
//    return D_O_K;
//}
