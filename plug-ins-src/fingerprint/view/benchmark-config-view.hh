#pragma once
#include "../params/benchmark-params.hh"
#include "form_builder.h"
#include "../gui/combo-box.hh"

namespace benchmark
{

enum ConfigMode
{
    JsonFile = 0,
    Form = 1
};

template<typename SearchEngine>
class BenchmarkConfigView
{
  public:
    BenchmarkConfigView();
    virtual ~BenchmarkConfigView();

    int draw(Params<SearchEngine>& param, const char *benchmark_file = nullptr);

  private:

    form_window_t *form_ = 0;
    Params<SearchEngine> *params_ = nullptr;
    int fgpIdx_ = 0;
    bool genSeed_ = false;
    float stretchingMean_ = AVG_NM_BASE_RATE;
    float stretchingDev_ = STDDEV_NM_BASE_RATE;
    ConfigMode configMode_ = JsonFile;
    int prepareNoiseVariant();
    int prepareStretchVariant();
    int prepareNoiseStretchVariant();
    int prepareSearchEngine(typename SearchEngine::params_type& params);
    int prepareGeneral();
    std::shared_ptr<typename SearchEngine::input_type> getHaystack();
    int drawVariantForm();
    int postTreatSearchEngine(typename SearchEngine::params_type& params);
};
}

#include "benchmark-config-view.hxx"
