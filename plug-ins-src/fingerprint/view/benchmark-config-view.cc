#include "benchmark-config-view.hh"
#include "../search/dist-oligo-sequence-finder.hh"
#include "../search/chisquare-search.hh"
#include "../search/basic-finder.hh"
#include "../storage/environnement.hh"
#include "form_builder.h"


typedef fingerprint::Environnement Env;
namespace benchmark
{

template<>
int BenchmarkConfigView<DistOligoSequenceFinder>::postTreatSearchEngine(typename DistOligoSequenceFinder::params_type& params)
{
}
template<>
int BenchmarkConfigView<BasicFinder>::postTreatSearchEngine(typename BasicFinder::params_type& params)
{
}
template<>
int BenchmarkConfigView<chiSquareMethod::ChiSquareSearch>::postTreatSearchEngine(typename chiSquareMethod::ChiSquareSearch::params_type& params)
{
    params.nmBaseRateDistribution = (NmBaseRateDistribution(stretchingMean_, stretchingDev_));
    //params.noiseConfidenceLevel(params_.noiseConfidenceLevel);

}

template<>
int BenchmarkConfigView<DistOligoSequenceFinder>::prepareSearchEngine(typename DistOligoSequenceFinder::params_type& params)
{
    form_push_label(form_, "Haystack");
    generate_dos_combo_box(form_, &fgpIdx_, Env::instance().referenceDsos());
    form_newline(form_);
    form_push_label(form_, "\t Allow gaps");
    form_push_bool(form_, &params.allowGap);
    form_newline(form_);
    form_push_label(form_, "\t max gap size");
    form_push_int(form_, &params.maxGapSize);
    form_newline(form_);
    form_push_label(form_, "\t accept threshold");
    form_push_float(form_, 0.1, &params.acceptThreshold);
    form_newline(form_);
    form_push_label(form_, "\t reject threshold");
    form_push_float(form_, 0.1, &params.rejectThreshold);
    form_newline(form_);
    return 0;
}

template<>
int BenchmarkConfigView<chiSquareMethod::ChiSquareSearch>::prepareSearchEngine(typename chiSquareMethod::ChiSquareSearch::params_type& params)
{
    form_push_label(form_, "Haystack");
    generate_combo_box(form_, &fgpIdx_, Env::instance().references());
    form_newline(form_);
    form_push_label(form_, "\t NmBaseRate mean value");
    form_push_float(form_, 0.1, &stretchingMean_);
    form_newline(form_);
    form_push_label(form_, "\t NmbaseRate standard deviation");
    form_push_float(form_, 0.1, &stretchingDev_);
    form_newline(form_);
    form_push_label(form_, "\t Confidence Level (stetching)");
    form_push_double(form_, 0.1, &params.stretchingConfidenceLevel);
    form_newline(form_);
    form_push_label(form_, "\t Min Univoque match degree of freedom");
    form_push_int(form_, &params.minUmDegreeOfFreedom);
    form_newline(form_);
    form_push_label(form_, "\t Min found rate");
    form_push_double(form_, 0.1, &params.minFoundRate);
    form_newline(form_);
    form_push_label(form_, "\t chiSquare(gamma) significance level");
    form_push_double(form_, 0.1, &params.gammaSignificanceLevel);
    form_newline(form_);
    form_push_label(form_, "\t expected noise deviation");
    form_push_double(form_, 0.1, &params.expectedNoiseDeviation);
    form_newline(form_);
    form_push_label(form_, "\t noise confidence interval");
    form_push_double(form_, 0.1, &params.noiseConfidenceLevel);
    form_newline(form_);
    return 0;
}
template<>
int BenchmarkConfigView<BasicFinder>::prepareSearchEngine(typename BasicFinder::params_type& params)
{
    form_push_label(form_, "Haystack");
    generate_combo_box(form_, &fgpIdx_, Env::instance().references());
    form_newline(form_);
    form_push_label(form_, "\t Max Error");
    form_push_double(form_, 0.1,&params.maxSearchHybridationError);
    form_newline(form_);
    form_push_label(form_, "\t Noise Deviation");
    form_push_double(form_,0.1, &params.noiseDeviation);
    form_newline(form_);
    form_push_label(form_, "\t Local search precision");
    form_push_double(form_, 0.1, &params.localSearchPrecision);
    form_newline(form_);
    form_push_label(form_, "\t skip Missing Oligo in Haystack");
    form_push_bool(form_, &params.skipMissingOligoInHaystack);
    form_newline(form_);
    form_push_label(form_, "\t skip Missing Oligo in Needle");
    form_push_bool(form_, &params.skipMissingOligoInNeedle);
    form_newline(form_);
    form_push_label(form_, "\t minNmBaseRate");
    form_push_double(form_, 0.1, &params.minNmBaseRate);
    form_newline(form_);
    form_push_label(form_, "\t max NmBase Rate");
    form_push_double(form_, 0.1, &params.maxNmBaseRate);
    form_newline(form_);

    return 0;
}

template<>
std::shared_ptr<typename chiSquareMethod::ChiSquareSearch::input_type>
BenchmarkConfigView<chiSquareMethod::ChiSquareSearch>::getHaystack()
{
    auto ptr = std::dynamic_pointer_cast<chiSquareMethod::ChiSquareSearch::input_type>
               (Env::instance().references()[fgpIdx_]);

    if (ptr == nullptr)
    {
        error_message("dynamic pointer cast failed %s != %s", typeid(chiSquareMethod::ChiSquareSearch::input_type).name(),
                        typeid(Env::instance().references()).name());
    }

    return ptr;
}

template<>
std::shared_ptr<typename BasicFinder::input_type>
BenchmarkConfigView<BasicFinder>::getHaystack()
{
    auto ptr = std::dynamic_pointer_cast<BasicFinder::input_type>
               (Env::instance().references()[fgpIdx_]);

    if (ptr == nullptr)
    {
        error_message("dynamic pointer cast failed %s != %s", typeid(BasicFinder::input_type).name(),
                        typeid(Env::instance().references()).name());
    }

    return ptr;
}

template<>
std::shared_ptr<typename DistOligoSequenceFinder::input_type>
BenchmarkConfigView<DistOligoSequenceFinder>::getHaystack()
{
    return Env::instance().referenceDsos()[fgpIdx_];
}
}
