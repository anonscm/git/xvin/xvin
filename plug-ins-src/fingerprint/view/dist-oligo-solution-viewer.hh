#pragma once
#include "../results/dist-oligo-solution.hh"
#include "../sequence/fingerprint.hh"
#include "../results/search-result.hh"
#include <vector>
#include <memory>
#include<queue>

/**
 * @brief
 */
enum DistOligoSolutionViewerMode
{
    DIFF,
    GLOBAL
};

/**
 * @brief A Graphical tool to display solution from DistOligoFinder
 */
class DistOligoSolutionViewer
{
  public:
    DistOligoSolutionViewer(DistClasses& classes, std::shared_ptr<const Fingerprint> haystack,
                            std::shared_ptr<const Fingerprint >refSolution,
                            DistSearchResult result, int viewedSolutionsCount);
    virtual ~DistOligoSolutionViewer();

    int nextSolution(uint solutionIdx);
    int prevSolution(uint solutionIdx);
    int gotoTruePositiveSolution(uint solutionIdx);

  private:
    const DistClasses& distClasses_;
    std::shared_ptr<const Fingerprint> reference_;
    std::shared_ptr<const Fingerprint> refSolution_;
    const DistSearchResult result_;
    std::vector<int> solutionIdxs_;
    pltreg *outputPr_;
    O_p *outputOp_;
    DistOligoSolutionViewerMode viewerMode_;
    int truePositiveRank_;
    bool found_;


    //std::weak_ptr<O_p> *outputOp_;
    int allocDiffMode();
    int drawDiffClasses();
    int drawDiffSolution(const Fingerprint& fgp, int solrank, const DistOligoSolution *solution, d_s *dataset);
    int drawDiffMode();

    int updateDisplay();
};
