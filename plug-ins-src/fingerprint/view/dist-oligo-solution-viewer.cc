#include "dist-oligo-solution-viewer.hh"
#include "../sequence/dist-oligo-sequence.hh"
#include "../api.hh"
#include "../sequence/fingerprint-sample-generator.hh"
DistOligoSolutionViewer::DistOligoSolutionViewer(DistClasses& classes, std::shared_ptr <
        const Fingerprint > haystack,
        std::shared_ptr<const Fingerprint> refSolution, DistSearchResult result, int viewedSolutionsCount)
    : distClasses_(classes),
      reference_(haystack),
      refSolution_(refSolution),
      result_(result),
      solutionIdxs_(viewedSolutionsCount, 0),
      viewerMode_(DistOligoSolutionViewerMode::DIFF),
      truePositiveRank_(-1),
      found_(false)
{
    outputPr_ = NULL;
    ac_grep(cur_ac_reg, "%pr", &outputPr_);
    outputOp_ = create_and_attach_one_plot(outputPr_, 1, 1, 0);
    outputOp_->read_only = true;
    outputOp_->user_id = 0xDEE;
    const std::vector<DistOligoSolution>&  solutions = result.solutions();

    for (uint i = 0; i < solutions.size(); ++i)
    {
        if (i < solutionIdxs_.size())
        {
            solutionIdxs_[i] = i;
        }

        bool truePositive = abs(solutions[i].firstReferencePosition() - refSolution_->originPosition()) <
                            refSolution_->size();

        if (truePositiveRank_ == -1 && truePositive)
        {
            truePositiveRank_ = i;
            found_ = true;
        }
    }

    if (!found_)
    {
        DistOligoSolution dos = DistOligoSolution(refSolution->originPosition());
        truePositiveRank_ = solutions.size() - 1;
    }

    solutionIdxs_[solutionIdxs_.size() - 1] = truePositiveRank_;
    updateDisplay();

    if (viewedSolutionsCount > 0)
    {
        outputOp_->cur_dat = 2;
    }
}

DistOligoSolutionViewer::~DistOligoSolutionViewer()
{
    remove_data(outputPr_, IS_ONE_PLOT, outputOp_);
}

int DistOligoSolutionViewer::nextSolution(uint solutionIdx)
{
    if (solutionIdx < solutionIdxs_.size())
    {
        solutionIdxs_[solutionIdx] = (solutionIdxs_[solutionIdx] + 1) % result_.solutions().size();
        updateDisplay();
        return 0;
    }

    return -1;
}

int DistOligoSolutionViewer::prevSolution(uint solutionIdx)
{
    if (solutionIdx < solutionIdxs_.size())
    {
        solutionIdxs_[solutionIdx] = (solutionIdxs_[solutionIdx] == 0) ? result_.solutions().size() - 1 :
                                     solutionIdxs_[solutionIdx] - 1;
        updateDisplay();
    }

    return 0;
}
int DistOligoSolutionViewer::gotoTruePositiveSolution(uint solutionIdx)
{
    if (solutionIdx < solutionIdxs_.size())
    {
        solutionIdxs_[solutionIdx] = truePositiveRank_;
        updateDisplay();
        return 0;
    }

    return -1;
}

int DistOligoSolutionViewer::updateDisplay()
{
    if (viewerMode_ == DIFF)
    {
        drawDiffMode();
    }

    outputOp_->need_to_refresh = 1;
    // assuming that current method is called when looking at the plot.
    refresh_plot(outputPr_, UNCHANGED);
    return 0;
}
int DistOligoSolutionViewer::allocDiffMode()
{
    int datCount = 1/*classes_ds*/ + 1/*ref_ds*/ + solutionIdxs_.size();

    if (outputOp_->n_dat < datCount)
    {
        for (int i = outputOp_->n_dat; i < datCount; ++i)
        {
            create_and_attach_one_ds(outputOp_, 1, 1, 0);
        }
    }

    build_adjust_data_set(outputOp_->dat[0], distClasses_.range.size() * 2, distClasses_.range.size() * 2);
    outputOp_->dat[0]->nx = distClasses_.range.size() * 2;
    outputOp_->dat[0]->ny = distClasses_.range.size() * 2;
    build_adjust_data_set(outputOp_->dat[1], refSolution_->nbHybridation(), refSolution_->nbHybridation());
    outputOp_->dat[1]->nx = refSolution_->nbHybridation();
    outputOp_->dat[1]->ny = refSolution_->nbHybridation();

    for (int i = outputOp_->n_lab - 1; i >= 0; --i)
    {
        remove_plot_label_from_op(outputOp_, outputOp_->lab[i]);
    }

    return 0;
}

int DistOligoSolutionViewer::drawDiffClasses()
{
    d_s *classes_ds = outputOp_->dat[0];
    char buf[128] = {0};
    snprintf(buf, sizeof(buf), "%s", found_ ? "FOUND" : "NOTFOUND");
    set_plot_title(outputOp_,
                   "\\stack{{Distance Oligo solutions diff}"
                   "{Needle: %s - %d Hybridization}"
                   "{Number of solutions: %d}"
                   "{ [%s] Origin position at rank: %d}"
                   "{Current display %d:%d}}",
                   refSolution_->description().c_str(),
                   refSolution_->nbHybridation(),
                   result_.solutions().size(),
                   buf,
                   truePositiveRank_,
                   solutionIdxs_[0], solutionIdxs_[1]);
    set_plot_x_title(outputOp_, "Relative position in nm");
    set_plot_y_title(outputOp_, "Distance between hybridization");
    set_dash_line(classes_ds);
    set_ds_line_color(classes_ds, Brown);

    for (uint i = 0; i < distClasses_.range.size(); ++i)
    {
        // ds->xd[i * 4] = (*dcv)[i].first;
        // ds->yd[i * 4] = 0;
        // ds->xd[i * 4 + 1] = (*dcv)[i].first;
        // ds->yd[i * 4 + 1] = 10000;
        classes_ds->xd[i * 2] = 0;
        classes_ds->yd[i * 2] = distClasses_.range[i].second;
        classes_ds->xd[i * 2 + 1] = 3000;
        classes_ds->yd[i * 2 + 1] = distClasses_.range[i].second;
    }

    classes_ds->need_to_refresh = 1;
    outputOp_->width = 2;
    return 0;
}
int DistOligoSolutionViewer::drawDiffSolution(const Fingerprint& fgp, int solrank, const DistOligoSolution *solution,
        d_s *dataset)
{
    //DistOligoSequence *dos = new DistOligoSequence(fgp, distClasses_);
    int cur_hyb = -1;
    int dist = 0;
    dataset->xd[0] = 0;
    dataset->yd[0] = 0;
    bool true_positive = solrank == truePositiveRank_;
    build_adjust_data_set(dataset, fgp.nbHybridation(), fgp.nbHybridation());

    for (int i = 1; i < fgp.nbHybridation(); ++i)
    {
        cur_hyb = fgp.nextHybridizationPos(cur_hyb);
        dist = fgp.nextHybridizationPos(cur_hyb) - cur_hyb;
        dataset->xd[i] = cur_hyb - fgp.firstHyb();
        dataset->yd[i] = dist;
    }

    dataset->need_to_refresh = 1;

    if (solrank >= 0)
    {
        if (true_positive)
        {
            set_ds_source(dataset, "Solution rank: %d - Score: %f - Locus: %d/%d - True positive", solrank,
                          solution->score(), solution->firstReferencePosition(), fgp.originPosition());
            set_ds_line_color(dataset, Lightgreen);
        }
        else
        {
            set_ds_source(dataset, "Solution rank: %d - Score: %f - Locus: %d/%d - False positive", solrank,
                          solution->score(), solution->firstReferencePosition(), fgp.originPosition());
            set_ds_line_color(dataset, Lightred);
        }
    }
    else
    {
        set_ds_source(dataset, "Searched Needle");
        set_ds_line_color(dataset, Lightblue);
    }

    return 0;
}

int DistOligoSolutionViewer::drawDiffMode()
{
    const std::vector<DistOligoSolution>& solutions = result_.solutions();
    allocDiffMode();
    drawDiffClasses();
    drawDiffSolution(*refSolution_, -1, NULL, outputOp_->dat[1]);
    DistOligoSequence doseq = DistOligoSequence(refSolution_, distClasses_);
    std::cout << " needle:\t";
    doseq.dump(std::cout);

    for (uint i = 0; i < solutionIdxs_.size(); ++i)
    {
        if (solutionIdxs_[i] < (int) solutions.size())
        {
            int startPos = (solutionIdxs_[i] != -1) ?
                           solutions[solutionIdxs_[i]].firstReferencePosition() :
                           refSolution_->originPosition();
            FingerprintSampleGenerator fgpsg;
            fgpsg.sizeDistribution(refSolution_->size() + 1000, 0);
            fgpsg.addedHybDistribution(0);
            fgpsg.missingHybDistribution(0);
            fgpsg.noiseDistribution(0, 0);
            fgpsg.directionDistribution(1);
            std::shared_ptr <Fingerprint> solutionfgp = fgpsg.generateOne(*reference_, startPos, refSolution_->size() + 1000, false,
                    1);

            if (solutionfgp != nullptr)
            {
                doseq = DistOligoSequence(solutionfgp, distClasses_);
                std::cout << ((solutionIdxs_[i] == truePositiveRank_) ? "!" : " ") << "rank " << solutionIdxs_[i] << ":\t";
                doseq.dump(std::cout);
                drawDiffSolution(*solutionfgp, solutionIdxs_[i],
                                 solutionIdxs_[i] != -1 ? & solutions[solutionIdxs_[i]] : 0,
                                 outputOp_->dat[i + 2]);
                solutionfgp.reset();
            }
        }
    }

    return 0;
}
